<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";

	$programName='New Item Allocation';
	$programCode='P0449';
	$invoiceApproveLevel = (int)getApproveLevel($programName);
	

 if($requestType=='getValidation')
	{
		$allocationNo  = $_REQUEST['allocationNo'];
		$allocationNoArray=explode("/",$allocationNo);
		
		            $sql = "SELECT 
ware_itemcreationdetails.intMainItemId, 
ware_itemcreationdetails.intSubItemId,
ware_itemcreationdetails.dblQty,
ware_itemcreationheader.intStatus,
ware_itemcreationheader.intApproveLevels,
mst_item.strName AS itemName, 
ware_itemcreationheader.intCompanyId AS location,
mst_locations.intCompanyId AS companyId 
FROM
ware_itemcreationdetails
Inner Join ware_itemcreationheader ON ware_itemcreationdetails.intAllocationNo = ware_itemcreationheader.intAllocationNo AND ware_itemcreationdetails.intAllocationYear = ware_itemcreationheader.intAllocationYear 
Inner Join mst_locations ON ware_itemcreationheader.intCompanyId = mst_locations.intId
Inner Join mst_item ON ware_itemcreationdetails.intSubItemId = mst_item.intId
WHERE
ware_itemcreationdetails.intAllocationNo =  '$allocationNoArray[0]' AND
ware_itemcreationdetails.intAllocationYear =  '$allocationNoArray[1]'";
				
		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$msg ="Maximum Qtys for items"; 
		$msg1 =""; 
		$totTosaveQty=0;
		$totSavableQty=0;
		while($row=mysqli_fetch_array($result))
		{	
			 $Qty=$row['dblQty'];
			 
			$location 	= $_SESSION['CompanyID'];
			$company 	= $_SESSION['headCompanyId'];
			
			$stockBalQty=getStockBalance_bulk($location,$row['intSubItemId']);
			$allocatedQty=getAllocatedQty($location,$row['intSubItemId']);
		$confirmatonMode=loadConfirmatonMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);
			
			
			 if($row['intStatus']==1){
				$errorFlg=1;
				$msg1 ="This Allocation is already confirmed"; 
			 }
			 else if($row['intStatus']==0){
				$errorFlg=1;
				$msg1 ="This Allocation is rejected"; 
			 }
			else if($confirmatonMode==0){//no confirmation has been raised
				$errorFlg = 1;
				$msg ="No Permission to Approve this Gate Pass Note"; 
			}
			else{//confirmation has been raised
				$errorFlg = 0;
			}
				
			if($row['intStatus']==($row['intApproveLevels']+1)){//no confirmation has been raised
				$balQtyToAllocate = $stockBalQty-$allocatedQty;
			}
			else{//confirmation has been raised
				$balQtyToAllocate = $stockBalQty-$allocatedQty+$Qty;
			}

		if($errorFlg!=1){
				$rowI=loadItemDetails($row['intMainItemId']);
				$grnRateNew=$rowI['dblLastPrice'];
				$currenNew=$rowI['intCurrency'];
			
				if($grnRateNew<=0){
					$rollBackFlag=1;
					$rollBackMsg="Price Not Added for items";	
				}
				else if($currenNew<=0){
					$rollBackFlag=1;
					$rollBackMsg="Currency Not Added for items";	
				}

				$rowO=loadItemDetails($row['intSubItemId']);
				$grnRateOld=$rowO['dblLastPrice'];
				$currenOld=$rowO['intCurrency'];
			if($errorFlg!=1){
				if($grnRateOld<=0){
					$rollBackFlag=1;
					$rollBackMsg="Price Not Added for items";	
				}
				else if($currenOld<=0){
					$rollBackFlag=1;
					$rollBackMsg="Currency Not Added for items";	
				}
			}

			if($errorFlg!=1){
			if($Qty>$balQtyToAllocate){
				$errorFlg=1;
					$msg .="</br>".$row['itemName']." =".$balQtyToAllocate;
			}
			}
		}
		
			$totTosaveQty +=$Qty;
			$totSavableQty +=$balQtyToAllocate;
		}//end of while
	
	if($msg1!=''){
		$msg=$msg1;
	}
	
	if(($totTosaveQty > $totSavableQty) && ($errorFlg!=1)){
		$errorFlg=1;
		$msg ="Balance to Allocate Qty is not tally with This Qtys.";
	}
	
	//$errorFlg=1;
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}
//---------------------------
else if($requestType=='validateRejecton')
	{
		$allocationNo  = $_REQUEST['allocationNo'];
		$allocationNoArray=explode("/",$allocationNo);
		
		$sql = "SELECT
		ware_itemcreationheader.intStatus, 
		ware_itemcreationheader.intApproveLevels 
		FROM ware_itemcreationheader
		WHERE
		ware_itemcreationheader.intAllocationNo =  '$allocationNoArray[0]' AND
		ware_itemcreationheader.intAllocationYear =  '$allocationNoArray[1]'";

		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$msg ="";
		$row=mysqli_fetch_array($result);
		
		$rejectionMode=loadRejectionMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);

		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this Allocation is already raised.So cant reject"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This Allocation is already Rejected"; 
		}
		else if($rejectionMode==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="No Permission to reject this Allocation"; 
		}
		else{//confirmation has been raised
			$errorFlg = 0;
			
		}
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}

	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
	//--------------------------------------------------------------
function loadItemDetails($mainItem)
{
	global $db;
	  $sql = "SELECT
			mst_item.dblLastPrice,
			mst_item.intCurrency 
			FROM mst_item 
			WHERE
			mst_item.intId =  '$mainItem'";

	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);	
	return $row;	
}
//--------------------------------------------------------------
	//--------------------------------------------------------------
function getAllocatedQty($location,$itemId)
{
	global $db;
	  $sql = "SELECT
Sum(ware_itemcreationdetails.dblQty) as Qty 
FROM
ware_itemcreationdetails
Inner Join ware_itemcreationheader ON ware_itemcreationdetails.intAllocationNo = ware_itemcreationheader.intAllocationNo AND ware_itemcreationdetails.intAllocationYear = ware_itemcreationheader.intAllocationYear
WHERE
ware_itemcreationdetails.intSubItemId =  '$itemId' AND
ware_itemcreationheader.intCompanyId =  '$location' AND
ware_itemcreationheader.intStatus >1  AND 
ware_itemcreationheader.intStatus <=ware_itemcreationheader.intApproveLevels  ";

	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);	
		return val($row['Qty']);	
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//--------------------------------------------------------

?>

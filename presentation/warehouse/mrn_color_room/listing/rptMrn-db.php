<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";

	$programName='Material Request Note';
	$programCode='P0229';
	$mrnApproveLevel = (int)getApproveLevel($programName);
	


//---------------------------
 if($requestType=='getValidation')
	{
		$mrnNo  = $_REQUEST['mrnNo'];
		$mrnNoArray=explode("/",$mrnNo);
		$mrnNo=$mrnNoArray[0];
		$mrnYear=$mrnNoArray[1];
		
		$sql = "SELECT
		ware_mrnheader.intStatus, 
		ware_mrnheader.intApproveLevels 
		FROM ware_mrnheader
		WHERE
		ware_mrnheader.intMrnNo =  '$mrnNo' AND
		ware_mrnheader.intMrnYear =  '$mrnYear'";

		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$msg ="Final confirmation of this MRN is already raised"; 
		$row=mysqli_fetch_array($result);
		$status=$row['intStatus'];
		$approveLevels=$row['intApproveLevels'];
		
		$confirmatonMode=loadConfirmatonMode($programCode,$status,$approveLevels,$userId);
		
		
		if($status==1){// 
			$errorFlg = 1;
			$msg ="Final confirmation of this GRN is already raised"; 
		}
		else if($confirmatonMode==0){// 
			$errorFlg = 1;
			$msg ="No Permission to Approve"; 
		}
		
		
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}

//---------------------------
else if($requestType=='validateRejecton')
	{
		$mrnNo  = $_REQUEST['mrnNo'];
		$mrnNoArray=explode("/",$mrnNo);
		$mrnNo=$mrnNoArray[0];
		$mrnYear=$mrnNoArray[1];
		
		$sql = "SELECT
		ware_mrnheader.intStatus, 
		ware_mrnheader.intApproveLevels 
		FROM ware_mrnheader
		WHERE
		ware_mrnheader.intMrnNo =  '$mrnNo' AND
		ware_mrnheader.intMrnYear =  '$mrnYear'";

		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$msg ="";
		$row=mysqli_fetch_array($result);
		
		$rejectionMode=loadRejectionMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);

		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this MRN is already raised.So cant reject"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This MRN is already Rejected"; 
		}
		else if($rejectionMode==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="No Permission to reject this MRN"; 
		}
		else{//confirmation has been raised
			$errorFlg = 0;
			
		}
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}

//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//--------------------------------------------------------

?>

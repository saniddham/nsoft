//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
// JavaScript Document

$(document).ready(function() {
	$('#frmMrnReport').validationEngine();
	
	$('#frmMrnReport #imgApprove').click(function(){
	var val = $.prompt('Are you sure you want to approve this MRN ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					if(validateQuantities()==0){
					///////////
					var url = "rptMrn-db-set.php"+window.location.search+'&status=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmMrnReport #imgApprove').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmMrnReport #imgApprove').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
 					   hideWaiting();
					}
				}});
	});
	
	$('#frmMrnReport #imgReject').click(function(){
	var val = $.prompt('Are you sure you want to reject this MRN ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
					showWaiting();
					if(validateRejecton()==0){
					///////////
					var url = "rptMrn-db-set.php"+window.location.search+'&status=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmMrnReport #imgReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmMrnReport #imgReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
						hideWaiting();
			}
		}});
	});
	
});
//---------------------------------------------------
function validateQuantities(){
		var ret=0;	
	    var mrnNo = document.getElementById('divMrnNo').innerHTML;
		var url 		= "rptMrn-db-get.php?requestType=getValidation";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"mrnNo="+mrnNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",3000);
					  ret= 1;
					}
				}
			});
			return ret;

}
//---------------------------------------------------
function validateRejecton(){
		var ret=0;	
	    var mrnNo = document.getElementById('divMrnNo').innerHTML;
		var url 		= "rptMrn-db-get.php?requestType=validateRejecton";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"mrnNo="+mrnNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",3000);
					  ret= 1;
					}
				}
			});
			return ret;

}
//------------------------------------------------
function alertx()
{
	$('#frmMrnReport #imgApprove').validationEngine('hide')	;
}
//--------------------------------------------
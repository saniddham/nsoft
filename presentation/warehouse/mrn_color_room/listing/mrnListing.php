<?php
date_default_timezone_set('Asia/Colombo');
//ini_set('allow_url_include',1);
//ini_set('display_errors',1);
session_start();

$backwardseperator 	= "../../../../";
$thisFilePath 		=  $_SERVER['PHP_SELF'];

require_once $_SESSION['ROOT_PATH']."dataAccess/Connector.php";
include_once "../../../../libraries/jqgrid2/inc/jqgrid_dist.php";
require_once $_SESSION['ROOT_PATH']."class/cls_permisions.php";
	
$objpermisionget= new cls_permisions($db);

$company 	= $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];

$programName='Material Request Note';
$programCode='P0229';
$intUser  = $_SESSION["userId"];

$userDepartment=getUserDepartment($intUser);
$approveLevel = (int)getMaxApproveLevel();

$option=1;
$option=getOption($objpermisionget);

$strOption3=getOption3ComboValues();

 $sql = "select * from(SELECT DISTINCT 
							if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected','Pending')) as Status,
							tb1.intMrnNo as `MRN_No`,
							tb1.intMrnYear as `MRN_Year`,
							mst_department.strName as `Department`,
							substr(tb1.datdate,1,10) as `Date`,
							sys_users.strUserName as `Create_User`, 
							  
							IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_mrnheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_mrnheader_approvedby
								Inner Join sys_users ON ware_mrnheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_mrnheader_approvedby.intMrnNo  = tb1.intMrnNo AND
								ware_mrnheader_approvedby.intYear =  tb1.intMrnYear AND
								ware_mrnheader_approvedby.intApproveLevelNo =  '1' AND 
							    ware_mrnheader_approvedby.intStatus='0'  
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.intStatus>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_mrnheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_mrnheader_approvedby
								Inner Join sys_users ON ware_mrnheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_mrnheader_approvedby.intMrnNo  = tb1.intMrnNo AND
								ware_mrnheader_approvedby.intYear =  tb1.intMrnYear AND
								ware_mrnheader_approvedby.intApproveLevelNo =  '$i' AND 
							    ware_mrnheader_approvedby.intStatus='0'  
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.intStatus>1) AND (tb1.intStatus<=tb1.intApproveLevels) AND ((SELECT
								max(ware_mrnheader_approvedby.dtApprovedDate) 
								FROM
								ware_mrnheader_approvedby
								Inner Join sys_users ON ware_mrnheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_mrnheader_approvedby.intMrnNo  = tb1.intMrnNo AND
								ware_mrnheader_approvedby.intYear =  tb1.intMrnYear AND
								ware_mrnheader_approvedby.intApproveLevelNo =  ($i-1) AND 
							    ware_mrnheader_approvedby.intStatus='0'  )<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*fase part*/
								 if($i>tb1.intApproveLevels,'-----',''))
								
								/*end IFNULL false part*/
								) as `".$approval."`, "; 
									
								}
							
							
						$sql .= "IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_mrnheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_mrnheader_approvedby
								Inner Join sys_users ON ware_mrnheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_mrnheader_approvedby.intMrnNo  = tb1.intMrnNo AND
								ware_mrnheader_approvedby.intYear =  tb1.intMrnYear AND
								ware_mrnheader_approvedby.intApproveLevelNo =  '0' AND 
							    ware_mrnheader_approvedby.intStatus='0' AND 
								tb1.intStatus<>1
							),'') as `RejectedBy` , "; 
							
								
							$sql .= "
							(SELECT Sum(ware_mrndetails.dblQty) FROM ware_mrndetails where tb1.intMrnNo=ware_mrndetails.intMrnNo AND tb1.intMrnYear=ware_mrndetails.intMrnYear ) as mrnQty, 
							
							(SELECT
							Sum(ware_issuedetails.dblQty)
							FROM
							ware_issuedetails
							Inner Join ware_issueheader ON ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo AND ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear
							WHERE
							ware_issueheader.intStatus =  '1' AND tb1.intMrnNo=ware_issuedetails.intMrnNo AND tb1.intMrnYear=ware_issuedetails.intMrnYear ) as issueQty, 
							
							round((SELECT
							Sum(ware_issuedetails.dblQty)
							FROM
							ware_issuedetails
							Inner Join ware_issueheader ON ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo AND ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear
							WHERE
							ware_issueheader.intStatus =  '1' AND tb1.intMrnNo=ware_issuedetails.intMrnNo AND tb1.intMrnYear=ware_issuedetails.intMrnYear )/(SELECT Sum(ware_mrndetails.dblQty) FROM ware_mrndetails where tb1.intMrnNo=ware_mrndetails.intMrnNo AND tb1.intMrnYear=ware_mrndetails.intMrnYear )*100)as `Issue_Progress`,
							
							'View' as `View`   
							
							FROM
							
							ware_mrnheader as tb1
							Inner Join sys_users ON tb1.intUser = sys_users.intUserId 
							Inner Join mst_department ON tb1.intDepartment = mst_department.intId
							WHERE  tb1.intCompanyId = '$location'  ";
							if($option==3)//all loging user  department's mrns
							$sql .= " AND tb1.intCompanyId = '$location'" ; 
							else if($option==2)//all loging user  department's mrns
							$sql .= " AND sys_users.intDepartmentId = '$userDepartment'" ; 
							else if($option==1)//only loging user's mrns
							$sql .= " AND tb1.intUser = '$intUser'" ; 
							
							$sql .= "  )  as t where 1=1
						";
					   	//echo $sql;
$col = array();

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist

$cols[] = $col;	$col=NULL;
//MRN No
$col["title"] 	= "MRN No"; // caption of column
$col["name"] 	= "MRN_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= "../addNew/mrn.php?mrnNo={MRN_No}&year={MRN_Year}";	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

$reportLink  = "rptMrn.php?mrnNo={MRN_No}&year={MRN_Year}";
$reportLinkApprove  = "rptMrn.php?mrnNo={MRN_No}&year={MRN_Year}&approveMode=1";

$cols[] = $col;	$col=NULL;


//MRN Year
$col["title"] = "MRN Year"; // caption of column
$col["name"] = "MRN_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Department
$col["title"] = "Department"; // caption of column
$col["name"] = "Department"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
if($option==3)//all in loging user  department's mrns
{
$col["stype"] 	= "select";
$col["editoptions"] 	=  array("value"=> $strOption3);
}
$col["width"] = "4";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//DATE
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Create User
$col["title"] = "Create User"; // caption of column
$col["name"] = "Create_User"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Create User
$col["title"] = "Issue Progress%"; // caption of column
$col["name"] = "Issue_Progress"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//FIRST APPROVAL
/*$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;*/

for($i=1; $i<=$approveLevel; $i++){
	if($i==1){
	$ap="1st Approval";
	$ap1="1st_Approval";
	}
	else if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}

$col["title"] = "Rejected By"; // caption of column
$col["name"] = "RejectedBy"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid();

$grid["caption"] 		= "MRN Listing";
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'MRN_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes

$grid["export"]["range"] = "filtered"; // or "all"
// initialize search, 'name' field equal to (eq) 'Client 1'
/*$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"}
     ]
}
SEARCH_JSON;

$grid["search"] = true; 
$grid["postData"] = array("filters" => $sarr ); */

// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

// params are array(<function-name>,<class-object> or <null-if-global-func>,<continue-default-operation>)
$e["on_render_excel"] = array("custom_excel_layout", null, true);
$jq->set_events($e);
				
function custom_excel_layout($param)
{
	$grid = $param["grid"];
	$xls = $param["xls"];
	$arr = $param["data"];
	
	$xls->addHeader(array('MRN LISTING'));
	$xls->addHeader(array());
	$xls->addHeader($arr[0]);
	$summary_arr = array(0);
	
	for($i=1;$i<count($arr);$i++)
	{
		$xls->addRow($arr[$i]);
		$summary_arr[0] += $arr[$i]["id"];
	}
	//$xls->addRow($summary_arr);
	return $xls;
}


$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);



$out = $jq->render("list1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MRN Listing<?php /*?>(<?php echo $option; ?>)<?php */?></title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />


</head>

<body>	
<form id="frmlisting" name="frmlisting" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
	<link rel="stylesheet" type="text/css" media="screen" href="../../../../libraries/jqdrid/js/themes/smoothness/jquery-ui.custom.css"></link>	
	<link rel="stylesheet" type="text/css" media="screen" href="../../../../libraries/jqdrid/js/jqgrid/css/ui.jqgrid.css"></link>	
	
	<script src="../../../../libraries/jqdrid/js/jquery.min.js" type="text/javascript"></script>
	<script src="../../../../libraries/jqdrid/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script src="../../../../libraries/jqdrid/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="../../../../libraries/jqdrid/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
	<script src="../../../../libraries/javascript/script.js" type="text/javascript"></script>
	
    <td><table width="100%" border="0">
      <tr>
        <td>
        <div align="center" style="margin:10px">
        
			<?php echo $out?>
        </div>
        </td>
      </tr>

    </table></td>
    </tr>


</form>
</body>
</html>
<?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	  $sqlp = "SELECT
			Max(ware_mrnheader.intApproveLevels) AS appLevel
			FROM ware_mrnheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
//------------------------------function load User Department---------------------
function getUserDepartment($intUser){
	global $db;

	$sqlp = "SELECT
			sys_users.intDepartmentId
			FROM
			sys_users
			WHERE
			sys_users.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['intDepartmentId'];

}

//------------------------------function load User Department---------------------
function getOption3ComboValues(){
	global $db;

	$sql = "SELECT
			mst_department.intId,
			mst_department.strName
			FROM
			mst_department
			WHERE
			mst_department.intStatus =  '1'";
	$result = $db->RunQuery($sql);
	$strOption3 = ":All";
	while($row=mysqli_fetch_array($result))
	{
		$strOption3 .= ";".$row['strName'].":".$row['strName'] ;
	}
			 
	return $strOption3;

}
//------------------------------function load User Department---------------------
function getOption($objpermisionget){
	
	$option2 	= $objpermisionget->boolSPermision(3);
	$option3 	= $objpermisionget->boolSPermision(4);
	
	if($option3)
	$option=3;
	else if($option2)
	$option=2;
	else
	$option=1;
		
	return $option;


}
?>


<?php
///////////////////////////////
// H B G KORALA   ////////////
//////////////////////////////
//ini_set('display_errors',1);
session_start();
$backwardseperator	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$location 			= $_SESSION['CompanyID'];
$company 			= $_SESSION['headCompanyId'];
$thisFilePath 		=  $_SERVER['PHP_SELF'];
$intUser  			= $_SESSION["userId"];

include  	"{$backwardseperator}dataAccess/permisionCheck.inc";
include_once ("../../../../class/warehouse/color_room_mrn/cls_color_room_mrn_get.php");
include_once ("../../../../class/cls_commonErrorHandeling_get.php");
include_once ("../../../../class/warehouse/cls_warehouse_get.php");
include_once ("../../../../class/customerAndOperation/cls_textile_stores.php");

 
$mrnNo 				= $_REQUEST['mrnNo'];
$year 				= $_REQUEST['year'];
$editMode			=0;
$confirmatonMode	=0;

$programName		='Stores Transfer Material Request Note';
$programCode		='P0784';

$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_mrn_get		= new Cls_color_room_mrn_Get($db);
$obj_warehouse_get	= new cls_warehouse_get($db);

$header_array 		= $obj_mrn_get->get_header_array($serialYear,$serialNo,'RunQuery');
$detail_result		= $obj_mrn_get->get_details_result($serialYear,$serialNo,'RunQuery');

$permition_arr		= $obj_commonErr->get_permision_withApproval_save($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_save		= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_cancel($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_cancel	= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_reject($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_reject	= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_confirm	= $permition_arr['permision'];

 if(!isset($header_array["MRN_NO"]))
	$MRNDate	= date("Y-m-d");
else
	$MRNDate	= $header_array["MRN_DATE"];
	
?> 
      
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Stores Transfer Material Requisition Note</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="mrn-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body>
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>


<form id="frmMrn" name="frmMrn" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Stores Transfer Materials Requisition Note</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF"><tr><td>
   <table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="22" class="normalfnt">MRN No</td>
            <td width="22%"><input name="txtMrnNo" type="text" disabled="disabled" class="txtText" id="txtMrnNo" style="width:60px" value="<?php echo $mrnNo ?>" /><input name="txtYear" type="text" disabled="disabled" class="txtText" id="txtYear" style="width:40px"value="<?php echo $year ?>"  /></td>
            <td width="17%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="8%" class="normalfnt">Date</td>
            <td width="18%"><input name="dtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="dtDate" style="width:120px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="27" class="normalfnt">Stores</td>
            <td width="39%"><select name="cboStores" id="cboStores" style="width:250px"  class="validate[required] txtText" <?php if($permision_save==0){  ?> disabled="disabled"<?php } ?>>
                  <?php
				  echo $obj_warehouse_get->get_sub_stores_options($location,'RunQuery');
				?>
            </select></td>
            <td width="25%">&nbsp;</td>
            <td width="11%" class="normalfnt">&nbsp;</td>
            <td width="15%">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
<td width="10%" height="27" rowspan="2" class="normalfnt">Note</td>
            <td width="41%" rowspan="2"><textarea name="txtRemarks" id="txtRemarks" cols="35" rows="3" <?php if($permision_save==0){  ?> disabled="disabled"<?php } ?>><?php echo $remarks; ?></textarea></td>
            <td width="27%" rowspan="2"><div id="divPrice" style="display:none"><?php echo $priceEdit; ?></div></td>
            <td width="9%" rowspan="2" valign="top" class="normalfnt">&nbsp;</td>            
            <td width="13%" valign="bottom"><a class="button green medium" id="butAddItems">Add Items</a></td>
          </tr>
        </table></td>
      </tr>
      <tr> <td><div style="width:900px;height:300px;overflow:scroll" >
        	<table width="100%" class="bordered" id="tblMain">
            <tr>
              <th width="6%" height="22" >Del</th>
              <th width="8%" >Order No</th>
              <th width="10%" >Sales Order</th>
              <th width="23%" >Item Description</th>
              <th width="4%">UOM</th>
              <th width="6%">Order Qty</th>
              <th width="7%">Samp Con Pc</th>
              <th width="8%">Required Qty</th>
              <th width="8%">Allocated Qty</th>
              <th width="10%">Qty</th>
              <th width="10%"> Extra Qty</th>
              </tr>
            <?php
			     $sql = "SELECT
				ware_mrndetails.dblQty, 
				mst_item.strName as itemName,
				mst_item.intBomItem,
				 mst_item.intId as intItemNo, 
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,
				ware_mrndetails.intOrderNo,
				ware_mrndetails.intOrderYear,
				ware_mrndetails.strStyleNo as strSalesOrderId,  
				trn_orderdetails.strSalesOrderNo, 
				mst_item.strCode, 
				mst_units.strCode as uom , 
				mst_part.strName as part
				FROM
				ware_mrndetails 
				left Join trn_orderdetails ON ware_mrndetails.intOrderNo = trn_orderdetails.intOrderNo AND ware_mrndetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_mrndetails.strStyleNo = trn_orderdetails.intSalesOrderId
				Inner Join mst_item ON ware_mrndetails.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				left Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
				WHERE 
				/*mst_item.intStatus = '1' AND*/ 
				ware_mrndetails.intMrnNo =  '$mrnNo' AND
				ware_mrndetails.intMrnYear =  '$year'  
				ORDER BY mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc 
";
				$result = $db->RunQuery($sql);
				$totAmm=0;
				while($row=mysqli_fetch_array($result))
				{
					
					$mainCatName=$row['mainCatName'];
					$subCatName=$row['subCatName'];
					$mainCatId=$row['intMainCategory'];
					$subCatId=$row['intSubCategory'];
					$itemId=$row['intItemNo'];
					$itemName=$row['itemName'];
					$uom=$row['uom'];
					$part=$row['part'];
					$allocatedQty=$row['dblAllocatedQty'];
					$mrnQty=$row['dblMrnQty'];
					$balToMrnQty=$allocatedQty-$mrnQty;
					if($row['intBomItem']==1){
						$balToMrnQty=10000;
					}
					$Qty=$row['dblQty'];
					
					$orderNo=$row['intOrderNo']."/".$row['intOrderYear'];
					$salesOrderId=$row['strSalesOrderId'];
					$salesOrderNo=$row['strSalesOrderNo']."/".$part;
					if($row['intOrderNo']==0){
						$orderNo='';
						$salesOrderId='';
						$salesOrderNo='';
					}
					
				?>
<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><?php if($editMode==1){  ?><img class="delImg" src="../../../../images/del.png" width="15" height="15" /><?php } ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $orderNo ?>" class="orderNo"><?php echo $uom ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $itemId ?>" class="item"><span class="orderNo"><?php echo $uom ?></span></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $itemId ?>" class="item"><?php echo $uom ?></td>
			<td align="center" bgcolor="#FFFFFF" id=""><?php echo $uom ?></td>
			<td align="left" bgcolor="#FFFFFF" id=""><?php echo $uom ?></td>
			<td align="left" bgcolor="#FFFFFF" id=""><?php echo $uom ?></td>
			<td align="left" bgcolor="#FFFFFF" id=""><?php echo $uom ?></td>
			<td align="left" bgcolor="#FFFFFF" id=""><?php echo $uom ?></td>
			<td align="center" bgcolor="#FFFFFF" id=""><input  id="<?php echo $Qty ?>2" class=" Qty" style="width:80px;text-align:center" type="text" value="<?php echo $Qty ?>"  disabled="disabled" /></td>
			<td align="center" bgcolor="#FFFFFF" id=""><input  id="<?php echo $Qty ?>" class="validate[required,custom[number]] calculateValue extQty" style="width:80px;text-align:center" type="text" value="<?php echo $Qty ?>" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/></td>
			</tr>            <?php
	}

			   ?>
</table>
        </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound">
            <a class="button white medium" id="butNew">New</a>
            <a class="button white medium" id="butSave">Save</a>
            <a class="button white medium" id="butConfirm">Confirm</a>
            <a href="../../../main.php" class="button white medium" id="butSave">Close</a>
    </td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>


</body>
</html>
<?php
//------------------------------function load Header---------------------
function loadHeader($mrnNo,$year)
{
	global $db;
	  $sql = "SELECT
			ware_mrnheader.datdate,
			ware_mrnheader.intStatus,
			ware_mrnheader.intApproveLevels,
			ware_mrnheader.intUser,
			ware_mrnheader.strRemarks, 
			sys_users.strUserName, 
			ware_mrnheader.intDepartment as department 
			FROM
			ware_mrnheader 
			Inner Join mst_locations ON ware_mrnheader.intCompanyId = mst_locations.intId
			Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
			Inner Join sys_users ON ware_mrnheader.intUser = sys_users.intUserId
			WHERE
			ware_mrnheader.intMrnNo =  '$mrnNo' AND
			ware_mrnheader.intMrnYear =  '$year' 
			GROUP BY
			ware_mrnheader.intMrnNo,
			ware_mrnheader.intMrnYear"; 
			 $result = $db->RunQuery($sql);
			 return $result;
}
//------------------------------function load Default Department---------------------
function loadDefaultDepartment($intUser){
	global $db;
	 $sql1 = "SELECT
				sys_users.intDepartmentId
				FROM
				sys_users
				WHERE
				sys_users.intUserId =  '$intUser'";	
	$result1 = $db->RunQuery($sql1);
	$row1=mysqli_fetch_array($result1);
	$department=$row1['intDepartmentId'];
	return $department;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser,$depatment){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	  $sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId 
		Inner Join sys_users ON sys_users.intUserId = menupermision.intUserId
		WHERE 
		sys_users.intDepartmentId =  '$depatment' 
		AND menus.strCode =  '$programCode' 
		AND menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//------------------------------function load User Department---------------------
function getUserDepartment($intUser){
	global $db;

	$sqlp = "SELECT
			sys_users.intDepartmentId
			FROM
			sys_users
			WHERE
			sys_users.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['intDepartmentId'];

}
?>

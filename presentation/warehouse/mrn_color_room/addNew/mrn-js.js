			
$(document).ready(function() {
	
 //-------------------------------------------- 
 	$('#frmMrn #butAddItems').live('click',loadPopup);
 //-------------------------------------------- 
 	$('#frmMrnPopup #butAdd').live('click',addClickedRows);
 //-------------------------------------------- 
 	$('#frmMrnPopup #butClose1').live('click',disablePopup);
 //-------------------------------------------- 
 	$('#frmMrnPopup #cboOrderNo').live('change',load_salesOrders);
 //-------------------------------------------- 
 	$('#frmMrnPopup #cboMainCategory').live('change',load_sub_category);
 //-------------------------------------------- 
 	$('#frmMrnPopup #imgSearchItems').live('click',load_items);
 //-------------------------------------------- 
 	$('#frmMrn #butSave').live('click',save_data);
 //-------------------------------------------- 
 	$('#frmMrn #butConfirm').live('click',approve_mrn);
 //-------------------------------------------- 
	
	
	});
	
	
function loadPopup()
{
		popupWindow3('1');
		$('#popupContact1').load('mrnPopup.php',function(){
	});
}

function closePopUp(){
	
}

function load_salesOrders()
{
 		var orderNo = $('#cboOrderNo').val();
		var url 		= "mrn-db.php?requestType=loadSalesOrder&orderNo="+orderNo;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmMrnPopup #cboSalesOrderNo').html(httpobj.responseText)
}

function load_sub_category()
{
		var mainCategory = $('#cboMainCategory').val();
		var url 		= "mrn-db.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmMrnPopup #cboSubCategory').html(httpobj.responseText)
}

function load_items()
{
	var orderNo = $('#cboOrderNo').val();
	var salesOrderId = $('#cboSalesOrderNo').val();
	var salesOrderNo = $('#cboSalesOrderNo option:selected').text();
	var mainCategory = $('#cboMainCategory').val();
	var subCategory = $('#cboSubCategory').val();
	var description = $('#txtItmDesc').val();
	
	if(salesOrderId==''){
		alert("Please select the Sales Order No");
		return false;
	}
	else if(mainCategory==''){
		alert("Please select the main category");
		return false;
	}
	
	var url 		= "mrn-db.php?requestType=loadItems";
	
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:"orderNo="+orderNo+"&mainCategory="+mainCategory+"&subCategory="+subCategory+"&salesOrderNo="+salesOrderId+"&description="+description, 
		async:false,
		success:function(json){

			var length = json.arrCombo.length;
			var arrCombo = json.arrCombo;


			for(var i=0;i<length;i++)
			{
				var itemId		=arrCombo[i]['itemId'];	
				var maincatId	=arrCombo[i]['maincatId'];	
				var subCatId	=arrCombo[i]['subCatId'];	
				var code		=arrCombo[i]['code'];	
				var itemName	=arrCombo[i]['itemName'];
				var uom			=arrCombo[i]['uom'];
				var conPC		=arrCombo[i]['consum'];
				
				var orderQty	=arrCombo[i]['orderQty'];	
				var stockBal	=arrCombo[i]['stockBal'];	
				var required	=arrCombo[i]['requiredQty'];	
				var allocated	=arrCombo[i]['issued_colRoom'];	
				
				var mainCatName	=arrCombo[i]['mainCatName'];
				var subCatName	=arrCombo[i]['subCatName'];	
					
				var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" id=""><input type="checkbox" id="chkDisp" /></td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatNameP">'+mainCatName+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatNameP">'+subCatName+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="itemP">'+itemName+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uomP">'+uom+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+orderNo+'" style="display:none"  class="orderNoP">'+orderNo+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+orderQty+'" style="display:none"  class="orderQtyP">'+orderQty+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+stockBal+'" style="display:none"  class="stockBalP">'+stockBal+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+allocated+'" style="display:none"  class="allocatedP">'+allocated+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+conPC+'" style="display:none"  class="conPCP">'+conPC+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderId+'" style="display:none"  class="salesOrderNoP">'+salesOrderNo+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+required+'" class="requiredP">'+required+'</td></tr>';

 				add_new_row('#frmMrnPopup #tblItemsPopup',content);
				
			}
				//checkAlreadySelected();

		}
	});
	

}

function addClickedRows()
{
	var rowCount = document.getElementById('tblItemsPopup').rows.length;
	var totRetAmmount=0;
	var itemType = $('#cboItemType').val();

	var i=0;
	$('#tblItemsPopup .itemP').each(function(){
		i++;
		
		if((document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked==true) && (document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled==false)){
			

			var itemId		=$(this).attr('id');
			var maincatId	=$(this).parent().find(".mainCatNameP").attr('id');
			var subCatId	=$(this).parent().find(".subCatNameP").attr('id');
			var code		=$(this).parent().find(".code").html();
			var uom			=$(this).parent().find(".uomP").html();
			var itemName	=$(this).html();
			var mainCatName	=$(this).parent().find(".mainCatNameP").html();
			var subCatName	=$(this).parent().find(".subCatNameP").html();
		
			var stockBal	=$(this).parent().find(".stockBalP").html();
			var orderQty	=$(this).parent().find(".orderQtyP").html();
			var required	=$(this).parent().find(".requiredP").html();
			var allocated	=$(this).parent().find(".allocatedP").html();
			var conPC		=$(this).parent().find(".conPCP").html();
			
			var orderNo		=$(this).parent().find(".orderNoP").html();
			var salesOrderNo=$(this).parent().find(".salesOrderNoP").html();
			var salesOrderId=$(this).parent().find(".salesOrderNoP").attr('id');
		
			var qty='';

			var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="../../../../images/del.png" width="15" height="15" /></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+orderNo+'" class="orderNo">'+orderNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderId+'" class="salesOrderNo">'+salesOrderNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="item">'+itemName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uom">'+uom+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+orderQty+'" class="orderQty">'+orderQty+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+conPC+'" class="conPC">'+conPC+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+required+'" class="required">'+required+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+allocated+'" class="allocated">'+allocated+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="'+required+'" class="validate[required,custom[number]] calculateValue Qty" style="width:80px;text-align:center" type="text" value="'+required+'"/></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="exQty" class="validate[required,custom[number]] calculateValue exQty" style="width:80px;text-align:center" type="text" value="0"/></td>';
			content +='</tr>';

			add_new_row('#frmMrn #tblMain',content);
		//----------------------------	
		$('.delImg').click(function(){
			$(this).parent().parent().remove();
		});
		
		}
	});
	disablePopup();
	closePopUp();
}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblMrn').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMrn').deleteRow(1);
			
	}
}
//----------------------------------------------- 
function clearPopupRows()
{
	var rowCount = document.getElementById('tblItemsPopup').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblItemsPopup').deleteRow(1);
			
	}
}
//------------------------------------------------------------
function checkAlreadySelected(){
	$('#tblMrn .item').each(function(){
			var itemId	= $(this).attr('id');
			var orderNo = 	$(this).parent().find(".orderNo").html();
			var salesOrderNo = 	$(this).parent().find(".salesOrderNo").html();
			
			var j=0;
			$('#tblItemsPopup .itemP').each(function(){
				j++;
				var itemIdP = 	$(this).attr('id');
				var orderNoP=$(this).parent().find(".orderNoP").html();
				var salesOrderNoP=$(this).parent().find(".salesOrderNoP").html();
				
				if((orderNo==orderNoP) && (salesOrderNo==salesOrderNoP) && (itemId==itemIdP)){
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].checked=true;
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].disabled=true;
				}
			});
	});
	
	
	var rowCount = document.getElementById('tblMrn').rows.length;
	var rowCount1 = document.getElementById('tblItemsPopup').rows.length;
	
}
//---------------------------------------------------
function save_data(){
	alert('ok');
	showWaiting();

	if(!Validate_save()){
		hideWaiting();
		setTimeout("alertx('')",4000);
		return;
	}
	
	var data 	= "mrn_no="+$('#txtMrnNo').val();
		data   += "&mrn_year="+$('#txtYear').val();
		data   += "&stores="+$('#cboStores').val();
		data   += "&date="+$('#dtDate').val();
		data   += "&note="	+	URLEncode_json($('#txtRemarks').val());
 
	var arr		= "[";
	$('#tblMain .orderNo').each(function(){
			arr += "{";
			arr += '"orderNo":"'+$(this).parent().find('.orderNo').attr('id')+'",' ;
			arr += '"salesOrderNo":"'+$(this).parent().find('.salesOrderNo').attr('id')+'",';
 			arr += '"item":"'+($(this).parent().find('.item').attr('id'))+'",';
			arr += '"qty":"'+($(this).parent().find('.Qty').val())+'",';
			arr += '"extraQty":"'+$(this).parent().find('.exQty').val()+'"';
 
  			arr +=  '},';
	});

	arr 		 = arr.substr(0,arr.length-1);
	arr 		+= " ]"; 
 
 	var url 	= "mrn-db.php?requestType=save";
		data 	+= "&arr="+arr;
   		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	async:false,
	success:function(json)
		{
			$('#frmMrn #butSave').validationEngine('showPrompt', json.msg,json.type );
			if(json.type=='pass')
			{
				$('#frmMrn #txtMrnNo').val(json.serialNo);
				$('#frmMrn #txtYear').val(json.serialYear);
				if(json.permision_confirm==1){
 					$('#frmMrn #butConfirm').css('display', 'inline'); 
				}
				$('#frmMrn #butReport').css('display','inline'); 
			}
		},
	error:function(xhr,status)
		{
			$('#frmMrn #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
		}
	});
	var t = setTimeout("alertx('#butSave')",4000);
	hideWaiting();
}
	
function Validate_save()
{

	return true;
}
function alertx(but)
{
	$('#frmJernalEntry '+but).validationEngine('hide')	;
}

function approve_mrn(){
	if($('#txtMrnNo').val()!=''){
		window.open('../listing/rptMrn.php?mrnNo='+$('#txtMrnNo').val()+'&year='+$('#txtYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no MRN to confirm");
	}
}
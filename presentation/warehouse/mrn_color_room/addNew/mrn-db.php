<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
// ini_set('display_errors',1);
	session_start();
	$backwardseperator 	= "../../../../";
	$mainPath 			= $_SESSION['mainPath'];
	$userId 			= $_SESSION['userId'];
	$requestType 		= $_REQUEST['requestType'];
	$location 			= $_SESSION['CompanyID'];
	$company 			= $_SESSION['headCompanyId'];
	
	$programCode		='P0784';
	
	include_once "{$backwardseperator}dataAccess/Connector.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
	include_once ("../../../../class/cls_commonErrorHandeling_get.php");
	include_once ("../../../../class/warehouse/color_room_mrn/cls_color_room_mrn_get.php");
	include_once ("../../../../class/warehouse/color_room_mrn/cls_color_room_mrn_set.php");
	include_once ("../../../../class/warehouse/cls_warehouse_get.php");
	
	$obj_mrn_get			= new Cls_color_room_mrn_Get($db);
	$obj_mrn_set			= new Cls_color_room_mrn_Set($db);
	$obj_warehouse_get		= new cls_warehouse_get($db);
	$obj_comfunc_get		= new cls_commonFunctions_get($db);
	$obj_commonErr			= new cls_commonErrorHandeling_get($db);
	
	if($requestType=='loadSalesOrder')
	{
		$orderNo		= $_REQUEST['orderNo'];
		$orderNoArray	= explode('/',$orderNo);
		$html			= $obj_warehouse_get->get_sales_order_nos_options($orderNoArray[0],$orderNoArray[1],'RunQuery');
		echo $html;
	}
	else if($requestType=='loadSubCategory')
	{
		$mainCategory  	= $_REQUEST['mainCategory'];
		
		$html			= $obj_warehouse_get->get_sub_category_options($mainCategory,'RunQuery');
		echo $html;
	}
	else if($requestType=='loadItems')
	{
		$orderNo  		= $_REQUEST['orderNo'];
		$orderNoArray	= explode('/',$orderNo);
		$salesOrderNo	= $_REQUEST['salesOrderNo'];
		$mainCategory  	= $_REQUEST['mainCategory'];
		$subCategory  	= $_REQUEST['subCategory'];
		$description  	= $_REQUEST['description'];
		
		$result = $obj_warehouse_get->get_stores_trans_items($orderNoArray[0],$orderNoArray[1],$salesOrderNo,$mainCategory,$subCategory,$description,'RunQuery');
		while($row=mysqli_fetch_array($result))
		{
			$data['itemId']			= $row['intId'];
			$data['maincatId']		= $row['intMainCategory'];
			$data['subCatId']		= $row['intSubCategory'];
			$data['mainCatName']	= $row['mainCatName'];
			$data['subCatName']		= $row['subCatName'];
			$data['code'] 			= $row['strCode'];
			$data['uom'] 			= $row['uom'];
			$data['itemName'] 		= $row['itemName'];
			
			$data['requiredQty'] 	= $row['consum'];
			$data['allocatedQty'] 	= '';
			$data['mrnQty'] 		= '';
			$data['issueQty'] 		= '';
			$data['orderQty'] 		= $row['orderQty'];
			$data['consum'] 		= $row['consum'];
			$data['required'] 		= $row['required'];
			$data['issued_colRoom']	= $obj_mrn_get->get_issued_to_color_room_item_amount($orderNoArray[0],$orderNoArray[1],$salesOrderNo,$location,$row['intId'],'RunQuery');
			$data['stockBal'] 		= $obj_warehouse_get->get_bulk_stock_balance($location,$row['intId'],'RunQuery');
			
			$arrCombo[] = $data;
		}
 		$response['arrCombo'] 		= $arrCombo;
		echo json_encode($response);

	}
	else if($requestType=='save')
	{
		
		$db->begin();
		
		$rollBackFlag	=0;
		$saved_records	=0;
	
		$serialNo	=$_REQUEST["mrn_no"];
		$serialYear	=$_REQUEST["mrn_year"];
		$stores		=$_REQUEST["stores"];
		$date		=$_REQUEST["date"];
		$note		=/*$obj_comfunc_get->*/replace($_REQUEST["note"]);
		// $note		=/*$obj_comfunc_get->replace*/($_REQUEST["note"]);
 		$arr				= json_decode($_REQUEST["arr"],true);
 
		$header_array 		= $obj_mrn_get->get_header_array($serialYear,$serialNo,'RunQuery2');
		
		if($rollBackFlag==0){
			$permition_arr		= $obj_commonErr->get_permision_withApproval_save($header_array['STATUS'],$header_array['LEVELS'],$userId,$progrmCode,'RunQuery2');
			$permision_save		= $permition_arr['permision'];
			if($permision_save!=1){
				$rollBackFlag	=1;
				$response		=$permition_arr;
			}
		}
		
		if($serialNo==''){
			$editMode=0;
			$journal_array 	= $obj_comfunc_get->GetSystemMaxNo('MRN_COLOR_ROOM',$location);
			$serialNo		= $journal_array["max_no"];
			$serialYear		= date('Y');
			$approveLevels 	= $obj_comfunc_get->getApproveLevels('Stores Transfer Material Request Note');
 			$status			=$approveLevels+1;
		}
		else{
			$editMode=1;
			$header_array 		= $obj_mrn_get->get_header_array($serialYear,$serialNo,'RunQuery2');
			
 			$status=$header_array['STATUS'];
			$approveLevels 	= $obj_comfunc_get->getApproveLevels('Stores Transfer Material Request Note');
 			$status			=$approveLevels+1;
		}
		
		
		
		
		if($rollBackFlag==0){
			
			if($editMode==0){
				$response_sh	= $obj_mrn_set->save_header($serialYear,$serialNo,$stores,$date,$note,$userId,$location,$approveLevels,$status,'RunQuery2');
			}
			else{
				$response_sh	= $obj_mrn_set->update_header($serialYear,$serialNo,$stores,$date,$note,$userId,$location,$approveLevels,$status,'RunQuery2');
			}
			
			if($response_sh['type']=='fail'){
				$rollBackFlag	=1;
				$response		=$response_sh;
			}
			else{
				$saved_records++;	
			}
		}
		

		if($rollBackFlag==0){
			$maxAppByStatus=$obj_mrn_get->get_MaxAppByStatus($serialYear,$serialNo,'RunQuery2');
			$maxAppByStatus=(int)$maxAppByStatus+1;
 			
/*			$response_ap	= $obj_mrn_set->approved_by_update($serialYear,$serialNo,$maxAppByStatus,'RunQuery2');

			if($response_ap['type']=='fail'){
				$rollBackFlag	=1;
				$response		=$response_ap;
			}
			else{
				$saved_records++;	
			}
*/		}
					

		if($rollBackFlag==0){
			$response_dt	= $obj_mrn_set->delete_details($serialYear,$serialNo,'RunQuery2');
			if($response_dt['type']=='fail'){
				$rollBackFlag	=1;
				$response		=$response_dt;
			}
		}
		foreach($arr as $array_loop)
		{
			$orderNo			= $array_loop["orderNo"];
			$orderNoArray		= explode('/',$orderNo);
			$orderNo			= $orderNoArray[0];
			$orderYear			= $orderNoArray[1];
			$salesOrderNo		= $array_loop["salesOrderNo"];
			$item				= $array_loop["item"];
			$qty				= $array_loop["qty"];
			$extraQty			= $array_loop["extraQty"];
			
			$requiredQty		= $obj_mrn_get->get_required_to_color_room_item_amount($orderNo,$orderYear,$salesOrderNo,$location,$item,'RunQuery2');
			$color_room_issued	= $obj_mrn_get->get_issued_to_color_room_item_amount($orderNo,$orderYear,$salesOrderNo,$location,$item,'RunQuery2');
			$bulk_stock_balance	= $obj_warehouse_get->get_bulk_stock_balance($location,$item,'RunQuery2');

			if($rollBackFlag==0){
				$response_dt	= $obj_mrn_set->save_details($serialYear,$serialNo,$orderNo,$orderYear,$salesOrderNo,$item,$qty,$extraQty,'RunQuery2');
				if($response_dt['type']=='fail'){
					$rollBackFlag	=1;
					$response		=$response_dt;
				}
				else{
					$saved_records++;	
				}
			}
		}
		//print_r($response);
		
		if($saved_records==0){
			$db->rollback();
			$response['type'] 		= 'fail';
			$response['msg'] 		= 'Detials Not Saved';
			$response['q'] 			= '';
		}
		else if($rollBackFlag==0){
			$db->commit();
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved Successfully';
			$response['q'] 			= '';
			$response['serialNo'] 	= $serialNo;
			$response['serialYear']	= $serialYear;
		}
		else
		{
			$db->rollback();
 		}
	echo json_encode($response);
		
	}
	
//--------------------------------------------------------getMaxAppByStatus
function replace($str)
{
	  $str 	= 	str_replace('\\', "\\\\",	$str);
	  $str 	= 	str_replace("'","\'",	$str);
	  $str	=	str_replace("\n", "<br>",$str);  
	  
	  return $str;
}	

//--------------------------------------------------------getMaxAppByStatus
 
	?>
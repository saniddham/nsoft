<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>

<?php
die();
session_start();
$backwardseperator = "../../../";
$companyId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
//include  	"{$backwardseperator}dataAccess/permisionCheck.inc";

$programName='Style Utilization Report';
$programCode='P0496';

$locationId = $_SESSION['CompanyID'];//this locationId use in report header(reportHeader.php)--------------------

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Style Utilization Report</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="rptStyleUtilization-js.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>

<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:273px;
	top:170px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<form id="frmStyleUtilizationRpt" name="frmStyleUtilizationRpt" method="post" action="rptReturnToSupplier.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>STYLE UTILIZATION REPORT</strong><strong></strong></div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="gridHeader">
              <td colspan="4" >&nbsp;</td>
              <?php
				 $numberOfOrders=getNumberOfOrders();
				 $width=47/$numberOfOrders;
				$resultO = getOrderDetails();
				while($rowO=mysqli_fetch_array($resultO))
				{
			   ?>
              <td colspan="2" >Order No <img class="delImg" width="15" height="15" src="../../../images/del.png" style="display:none" id="<?php echo $rowO['intOrderNo']."/".$rowO['intOrderYear'] ?>"></td>
              <?php
				}
			   ?>
              <td colspan="3" >&nbsp;</td>
              </tr>
            <tr class="gridHeader">
              <td colspan="4" >&nbsp;</td>
              <?php
				$resultO = getOrderDetails();
				while($rowO=mysqli_fetch_array($resultO))
				{
			   ?>
              <td colspan="2" ><span class="normalfntCenter"><?php echo $rowO['intOrderNo']."/".$rowO['intOrderYear'] ?></span></td>
              <?php
				}
			   ?>
              <td colspan="3" >&nbsp;</td>
              </tr>
            <tr class="gridHeader">
              <td width="9%" >Main Category</td>
              <td width="8%" >Sub Category</td>
              <td width="21%" >Item</td>
              <td width="3%" >UOM</td>
              <?php
				$resultO = getOrderDetails();
				while($rowO=mysqli_fetch_array($resultO))
				{
			   ?>
              <td width="<?php echo $width/2; ?>%" >Order Qty</td>
              <td width="<?php echo $width/2; ?>%" >Alloc Qty</td>
              <?php
				}
			   ?>
              <td width="6%" >Total</td>
              <td width="8%" >Stock Balance</td>
              <td width="9%" >Required Qty</td>
            </tr>
              <?php 
			$resultI = getItemDetails();
			$totQty=0;
			$totAmmount=0;
			while($row=mysqli_fetch_array($resultI))
			{
				$item=$row['intItem'];
				$totOrderQty=0;
				$totAllocQty=0;
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" >&nbsp;<?php echo $row['mainCat']?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['subCat'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['item'] ?>&nbsp;</td>
              <td class="normalfntMid" >&nbsp;<?php echo $row['uom'] ?>&nbsp;</td>
              <?php
				$resultO = getOrderDetails();
				while($rowO=mysqli_fetch_array($resultO))
				{ 
					$x_orderQty=getOrderQty($rowO['intOrderNo'],$rowO['intOrderYear']);
					$orderQty=getOrderWiseItemQtys($rowO['intOrderNo'],$rowO['intOrderYear'],$item);
					$orderQty=$orderQty*$x_orderQty;
					$allocatedQty=getAllocatedQty($rowO['intOrderNo'],$rowO['intOrderYear'],$item);
					
					if(($orderQty!='') && ($allocatedQty=='')){
						$allocatedQty=0;
					}
					if($allocatedQty!=''){
					$totAllocQty+=$allocatedQty;
					}
					
					if($orderQty!=''){
					$totOrderQty+=$orderQty;
					}
			   ?>
              <td class="normalfntRight" >&nbsp;<?php echo $orderQty; ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $allocatedQty ?>&nbsp;</td>
              <?php
				}
				$total=$totOrderQty-$totAllocQty;
				$stockBalance=getStockBalance_bulk($item,$companyId);
				$requiredQty=0;
				if($total>$stockBalance){ 
				$requiredQty=$total-$stockBalance;
				}
				
			   ?>
              <td class="normalfntRight" >&nbsp;<?php echo $total ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $stockBalance ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $requiredQty ?>&nbsp;</td>
              </tr>
      <?php 
			$totQty+=$row['dblQty'];
			}
	  ?>
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<?php  
 	if($intStatus!=0)
	{

				for($i=1; $i<=$savedLevels; $i++)
				{
					  $sqlc = "SELECT
							ware_returntosupplierheader_approvedby.intApproveUser,
							ware_returntosupplierheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_returntosupplierheader_approvedby.intApproveLevelNo
							FROM
							ware_returntosupplierheader_approvedby
							Inner Join sys_users ON ware_returntosupplierheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_returntosupplierheader_approvedby.intReturnNo =  '$retSupNo' AND
							ware_returntosupplierheader_approvedby.intYear =  '$year'  AND
							ware_returntosupplierheader_approvedby.intApproveLevelNo =  '$i'  order by intApproveLevelNo asc
";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
						if($i==1)
						$desc="1st ";
						else if($i==2)
						$desc="2nd ";
						else if($i==3)
						$desc="3rd ";
						else
						$desc=$i."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
				?>
      <?php
			}
	}
	else{
					 $sqlc = "SELECT
							ware_returntosupplierheader_approvedby.intApproveUser,
							ware_returntosupplierheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_returntosupplierheader_approvedby.intApproveLevelNo
							FROM
							ware_returntosupplierheader_approvedby
							Inner Join sys_users ON ware_returntosupplierheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_returntosupplierheader_approvedby.intReturnNo =  '$retSupNo' AND
							ware_returntosupplierheader_approvedby.intYear =  '$year'  AND
							ware_returntosupplierheader_approvedby.intApproveLevelNo =  '0'";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
					  ?>
      <?php
	}
?>
</table>
</div>        
</form>
</body>
</html>
<?php
	function getItemDetails()
	{
		global $db;
		 $sql = "select 
				tb1.intMainCategory,
				tb1.mainCat,
				tb1.intSubCategory,
				tb1.subCat,
				tb1.intItem,
				tb1.item,
				tb1.intUOM,
				tb1.strCode, 
				sum(tb1.qty) as qty,
				tb1.dblLastPrice, 
				mst_units.strCode as uom 
								   from (SELECT
				mst_item.intMainCategory,
				mst_maincategory.strName AS mainCat,
				mst_item.intSubCategory,
				mst_subcategory.strName AS subCat,
				trn_sample_color_recipes.intItem,
				mst_item.strName AS item,
				mst_item.intUOM,
				mst_units.strCode, 
				trn_sample_color_recipes.dblWeight/1000 AS qty,
				mst_item.dblLastPrice,
				trn_sample_color_recipes.intSampleNo,
				trn_sample_color_recipes.intSampleYear,
				trn_sample_color_recipes.intRevisionNo,
				trn_sample_color_recipes.strCombo,
				trn_sample_color_recipes.strPrintName  
				FROM 
				trn_sample_color_recipes
				Inner Join mst_item ON trn_sample_color_recipes.intItem = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId  
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				UNION 
				SELECT
				mst_item.intMainCategory,
				mst_maincategory.strName AS mainCat,
				mst_item.intSubCategory,
				mst_subcategory.strName AS subCat,
				trn_sample_foil_consumption.intItem, 
				mst_item.strName AS item,
				mst_item.intUOM,
				mst_units.strCode, 
				trn_sample_foil_consumption.dblFoilQty  AS qty , 
				mst_item.dblLastPrice,
				trn_sample_foil_consumption.intSampleNo,
				trn_sample_foil_consumption.intSampleYear,
				trn_sample_foil_consumption.intRevisionNo,
				trn_sample_foil_consumption.strCombo,
				trn_sample_foil_consumption.strPrintName 
				FROM
				trn_sample_foil_consumption
				Inner Join mst_item ON trn_sample_foil_consumption.intItem = mst_item.intId 
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId  
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId
				UNION 
				SELECT
				mst_item.intMainCategory,
				mst_maincategory.strName AS mainCat,
				mst_item.intSubCategory,
				mst_subcategory.strName AS subCat,
				trn_sample_spitem_consumption.intItem,
				mst_item.strName AS item,
				mst_item.intUOM,
				mst_units.strCode, 
				trn_sample_spitem_consumption.dblQty as qty,
				mst_item.dblLastPrice,
				trn_sample_spitem_consumption.intSampleNo,
				trn_sample_spitem_consumption.intSampleYear,
				trn_sample_spitem_consumption.intRevisionNo,
				trn_sample_spitem_consumption.strCombo,
				trn_sample_spitem_consumption.strPrintName
				FROM
				trn_sample_spitem_consumption 
				Inner Join mst_item ON trn_sample_spitem_consumption.intItem = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				) as tb1  
				left Join trn_orderdetails ON trn_orderdetails.intSampleNo = tb1.intSampleNo AND trn_orderdetails.intSampleYear = tb1.intSampleYear AND trn_orderdetails.intRevisionNo = tb1.intRevisionNo AND trn_orderdetails.strCombo = tb1.strCombo AND trn_orderdetails.strPrintName = tb1.strPrintName
				left Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear 
				Inner Join mst_units ON tb1.intUOM = mst_units.intId 
				WHERE
				trn_orderheader.intAllocatedStatus <>  '1' AND
				trn_orderheader.intStatus =  '1'
				GROUP BY tb1.item 
				Order by   
				tb1.mainCat ASC,
				tb1.subCat ASC,
				tb1.item ASC 
";

		$result = $db->RunQuery($sql);
		return $result;	
	}
	
	function getOrderDetails()
	{
		global $db;
		$sql = "SELECT
				trn_orderheader.intOrderNo,
				trn_orderheader.intOrderYear
				FROM
				trn_orderheader
				WHERE
				trn_orderheader.intAllocatedStatus <>  '1' AND
				trn_orderheader.intStatus =  '1'";
		$result = $db->RunQuery($sql);
		return $result;	
	}
	function getNumberOfOrders()
	{
		global $db;
		$sql = "SELECT 
				Count(concat(trn_orderheader.intOrderNo,'/',trn_orderheader.intOrderYear)) as orders 
				FROM
				trn_orderheader
				WHERE
				trn_orderheader.intAllocatedStatus <>  '1' AND
				trn_orderheader.intStatus =  '1'";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		return $row['orders'];
	}
	
	function getOrderWiseItemQtys($orderNo,$orderYear,$item)
	{
		global $db;
		   $sql = "select 
				sum(tb1.qty) as qty 
								   from (SELECT
				mst_item.intMainCategory,
				mst_maincategory.strName AS mainCat,
				mst_item.intSubCategory,
				mst_subcategory.strName AS subCat,
				trn_sample_color_recipes.intItem,
				mst_item.strName AS item,
				mst_item.intUOM,
				mst_units.strCode, 
				trn_sample_color_recipes.dblWeight/1000 AS qty,
				mst_item.dblLastPrice,
				trn_sample_color_recipes.intSampleNo,
				trn_sample_color_recipes.intSampleYear,
				trn_sample_color_recipes.intRevisionNo,
				trn_sample_color_recipes.strCombo,
				trn_sample_color_recipes.strPrintName  
				FROM 
				trn_sample_color_recipes
				Inner Join mst_item ON trn_sample_color_recipes.intItem = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId  
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				UNION 
				SELECT
				mst_item.intMainCategory,
				mst_maincategory.strName AS mainCat,
				mst_item.intSubCategory,
				mst_subcategory.strName AS subCat,
				trn_sample_foil_consumption.intItem, 
				mst_item.strName AS item,
				mst_item.intUOM,
				mst_units.strCode, 
				trn_sample_foil_consumption.dblFoilQty  AS qty , 
				mst_item.dblLastPrice,
				trn_sample_foil_consumption.intSampleNo,
				trn_sample_foil_consumption.intSampleYear,
				trn_sample_foil_consumption.intRevisionNo,
				trn_sample_foil_consumption.strCombo,
				trn_sample_foil_consumption.strPrintName 
				FROM
				trn_sample_foil_consumption
				Inner Join mst_item ON trn_sample_foil_consumption.intItem = mst_item.intId 
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId  
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId
				UNION 
				SELECT
				mst_item.intMainCategory,
				mst_maincategory.strName AS mainCat,
				mst_item.intSubCategory,
				mst_subcategory.strName AS subCat,
				trn_sample_spitem_consumption.intItem,
				mst_item.strName AS item,
				mst_item.intUOM,
				mst_units.strCode, 
				trn_sample_spitem_consumption.dblQty as qty,
				mst_item.dblLastPrice,
				trn_sample_spitem_consumption.intSampleNo,
				trn_sample_spitem_consumption.intSampleYear,
				trn_sample_spitem_consumption.intRevisionNo,
				trn_sample_spitem_consumption.strCombo,
				trn_sample_spitem_consumption.strPrintName
				FROM
				trn_sample_spitem_consumption 
				Inner Join mst_item ON trn_sample_spitem_consumption.intItem = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				) as tb1  
				left Join trn_orderdetails ON trn_orderdetails.intSampleNo = tb1.intSampleNo AND trn_orderdetails.intSampleYear = tb1.intSampleYear AND trn_orderdetails.intRevisionNo = tb1.intRevisionNo AND trn_orderdetails.strCombo = tb1.strCombo AND trn_orderdetails.strPrintName = tb1.strPrintName
				left Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderheader.intAllocatedStatus <>  '1' AND
				trn_orderheader.intStatus =  '1' AND 
				trn_orderheader.intOrderNo ='$orderNo' AND 
				trn_orderheader.intOrderYear ='$orderYear' AND 
				tb1.intItem='$item' 
				GROUP BY tb1.item";

		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		return $row['qty'];
	}
	
	function getAllocatedQty($orderNo,$orderYear,$item)
	{
		global $db;
		$sql="SELECT
			Sum(ware_stocktransactions.dblQty) AS qty
			FROM ware_stocktransactions
			WHERE
			ware_stocktransactions.strType IN  ('ALLOCATE','UNALLOCATE') AND
			ware_stocktransactions.intOrderNo =  '$orderNo' AND
			ware_stocktransactions.intOrderYear =  '$orderYear' AND
			ware_stocktransactions.intItemId =  '$item'
			GROUP BY
			ware_stocktransactions.intOrderNo,
			ware_stocktransactions.intOrderYear,
			ware_stocktransactions.intItemId";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		return $row['qty'];
	}
	
	function getStockBalance_bulk($item,$companyId)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$companyId'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";
				
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		return round($row['stockBal'],4);
	}
	
	function getOrderQty($orderNo,$orderYear)
	{
		global $db;
		$sql = "SELECT
		Sum(trn_orderdetails.intQty) AS qty 
		FROM trn_orderdetails 
		WHERE
		trn_orderdetails.intOrderNo='$orderNo' 
		AND trn_orderdetails.intOrderYear='$orderYear'   
		GROUP BY
		trn_orderdetails.intOrderNo,
		trn_orderdetails.intOrderYear"; 
				
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		return round($row['qty'],4);
	}
?>
//////////////////////////////////////
//Created By: H.B.G Korala
/////////////////////////////////////
$(document).ready(function() {
	
  		$("#frmStyleUtilizationRpt").validationEngine();
		$('#frmStyleUtilizationRpt #cboIssueNo').focus();
		
		$("#butAddItems").click(function(){
			if(!$('#frmStyleUtilizationRpt #cboIssueNo').val()){
				alert("Please select the Issue No");
				return false;				
			}
			else if(!$('#frmStyleUtilizationRpt #cboIssueYear').val()){
				alert("Please select the Year");
				return false;				
			}
			//clearRows();
			loadPopup();
		});
		
		$("#cboIssueNo").change(function(){
			clearRows();
		});
		$("#cboIssueYear").change(function(){
			clearRows();
		});
	//--------------------------------------------
	$('#frmStyleUtilizationRpt .delImg').click(function(){
	var val = $.prompt('Are you sure you want to remove this order no from report ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
						var orderNos =  $('.delImg').attr('id');

						var url = "rptStyleUtilization-db-set.php?"+'status=remove'+"&orderNos="+orderNos;
						var obj = $.ajax({url:url,async:false});
						window.location.href = window.location.href;
					}
				}});
	});
	//--------------------------------------------
		
  //permision for delete
  if(intDeletex)
  {
   	$('#frmStyleUtilizationRpt .delImg').css("display",'');
  }
  
  
});//end of ready

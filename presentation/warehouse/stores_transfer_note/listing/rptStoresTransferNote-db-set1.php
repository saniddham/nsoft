<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	  = $_SESSION['mainPath'];
	$userId 	  = $_SESSION['userId'];
	$requestType  = $_REQUEST['status'];
	
	include "{$backwardseperator}dataAccess/Connector.php";
	
	$transfNo	= $_REQUEST['transfNo'];
	$year	  	= $_REQUEST['year'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	
if($requestType=="approve")
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	$rollBackFlag 	= 0;
	$rollBackMsg 	= "Maximum Qtys for items are...";
	
	$sql 	= "SELECT intApproveLevels,intStatus FROM ware_storestransferheader WHERE intTransfNo='$transfNo' AND intTransfYear='$year' ";
	$result = $db->RunQuery2($sql);
	$row 	= mysqli_fetch_array($result);
	
	if($row['intStatus']>$row['intApproveLevels'])
	{
		$sql = "SELECT
				STD.intItemId,
				STD.dblQty,
				STD.intTransfNo,
				STD.intTransfYear, 
				STH.intRequisitionNo,
				STH.intRequisitionYear
				FROM
				ware_storestransferdetails STD
				INNER JOIN ware_storestransferheader STH ON STD.intTransfNo = STH.intTransfNo AND 
				STD.intTransfYear = STH.intTransfYear
				WHERE
				STD.intTransfNo =  '$transfNo' AND
				STD.intTransfYear =  '$year' ";
		
		$result = $db->RunQuery2($sql);
		while($row=mysqli_fetch_array($result))
		{
			$SRNNo 		= $row['intRequisitionNo'];
			$SRNYear 	= $row['intRequisitionYear'];
			$itemId 	= $row['intItemId'];
			$qty	 	= $row['dblQty'];
			
			$sqlChk = " SELECT  
						tb1.intItemId as itemID, 
						mst_item.strName as itemName,
						tb1.intRequisitionNo,
						tb1.intRequisitionYear,
						ware_storesrequesitionheader.intReqFromStores as stores,
						ware_storesrequesitionheader.intOrderYear,
						ware_storesrequesitionheader.strOrderNos,
						tb1.dblQty,
						tb1.dblExQty,
						IFNULL((SELECT
						Sum(ware_storestransferdetails.dblQty)
						FROM
						ware_storestransferheader
						Inner Join ware_storestransferdetails ON ware_storestransferheader.intTransfNo = ware_storestransferdetails.intTransfNo AND ware_storestransferheader.intTransfYear = ware_storestransferdetails.intTransfYear
						WHERE
						ware_storestransferheader.intRequisitionNo =  tb1.intRequisitionNo AND
						ware_storestransferheader.intRequisitionYear =  tb1.intRequisitionYear AND
						ware_storestransferdetails.intItemId =  tb1.intItemId AND
						ware_storestransferheader.intStatus > '0' AND 
						ware_storestransferheader.intStatus <= ware_storestransferheader.intApproveLevels),0) as dblTransferQty, 
						IFNULL((SELECT
						Sum(ware_storestransferdetails.dblExQty)
						FROM
						ware_storestransferheader
						Inner Join ware_storestransferdetails ON ware_storestransferheader.intTransfNo = ware_storestransferdetails.intTransfNo AND ware_storestransferheader.intTransfYear = ware_storestransferdetails.intTransfYear
						WHERE
						ware_storestransferheader.intRequisitionNo =  tb1.intRequisitionNo AND
						ware_storestransferheader.intRequisitionYear =  tb1.intRequisitionYear AND
						ware_storestransferdetails.intItemId =  tb1.intItemId AND
						ware_storestransferheader.intStatus > '0' AND 
						ware_storestransferheader.intStatus <= ware_storestransferheader.intApproveLevels),0) as dblExTransferQty  
						FROM
						ware_storesrequesitiondetails as tb1 
						Inner Join ware_storesrequesitionheader ON tb1.intRequisitionNo = ware_storesrequesitionheader.intRequisitionNo 
						AND tb1.intRequisitionYear = ware_storesrequesitionheader.intRequisitionYear
						Inner Join mst_item ON tb1.intItemId = mst_item.intId
						Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
						Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
						WHERE 
						ware_storesrequesitionheader.intStatus = '1' AND 
						ware_storesrequesitionheader.intRequisitionNo =  '$SRNNo' AND 
						ware_storesrequesitionheader.intRequisitionYear =  '$SRNYear' AND 
						tb1.intItemId =  '$itemId' ";
						
			$resultsc 		= $db->RunQuery2($sqlChk);
			$rowc 			= mysqli_fetch_array($resultsc);
			
			$orderNos		= $rowc['strOrderNos'];
			$orderYear		= $rowc['intOrderYear'];
			$itemID			= $rowc['itemID'];
			$item			= $rowc['itemName'];
			$exQty	 		= $row['dblExQty'];
			$stores			= $rowc['stores'];

 			$balQtyToTransf 	= $rowc['dblQty']-$rowc['dblTransferQty'];
			$balQtyToTExransf 	= $rowc['dblExQty']-$rowc['dblExTransferQty'];
			$balQtyToTransf 	= $balQtyToTransf+$qty;
			$balQtyToTExransf 	= $balQtyToTExransf+$exQty;
			$stockBalExQty 		= getStockBalance_bulk($stores,$itemID);
			$stockBalQty 		= getStockBalance($stores,$orderYear,$orderNos,$itemID);
			if($balQtyToTExransf>$stockBalExQty)
			{
				$balQtyToTExransf=$stockBalExQty;
			}
			if($stockBalExQty>$balQtyToTExransf)
			{
				$rollBackFlag = 1;
				$rollBackMsg .= "</br> "."-".$item." =".$balQtyToTExransf;
			}
			else
			{
				$sqlUpd = " UPDATE ware_storesrequesitiondetails 
							SET
							dblTransferQty = dblTransferQty+$qty
							WHERE
							intRequisitionNo = '$SRNNo' AND 
							intRequisitionYear = '$SRNYear' AND 
							intItemId = '$itemId' ";
				$resultUpd = $db->RunQuery2($sqlUpd);
				if(!$resultUpd)
				{
					$rollBackFlag 	= 1;
					$rollBackMsg	= $db->errormsg;
				}
			}
		}
	}
	
	
	$sqlUpd = "UPDATE ware_storestransferheader 
				SET
				intStatus = intStatus-1
				WHERE
				intTransfNo = '$transfNo' AND 
				intTransfYear = '$year' ";
	$resultUpd = $db->RunQuery2($sqlUpd);
	if($resultUpd)
	{
		$sql = "SELECT intStatus, intApproveLevels 
				FROM ware_storestransferheader 
				WHERE intTransfNo = '$transfNo' AND 
				intTransfYear = '$year' ";
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		
		$approveLevel = $row['intApproveLevels'];
		$updStatus    = $row['intStatus'];
		$newApprLevel = ($approveLevel+1)-$updStatus;
		
		$sqlIns = "INSERT INTO ware_storestransferheader_approvedby 
					(
					intTransfNo, 
					intYear, 
					intApproveLevelNo, 
					intApproveUser, 
					dtApprovedDate, 
					intStatus
					)
					VALUES
					(
					'$transfNo', 
					'$year', 
					'$newApprLevel', 
					'$userId', 
					now(), 
					0
					) ";
		$resultIns = $db->RunQuery2($sqlIns);
		if($resultIns)
		{
			//$rollBackMsg			= "Approval raised sucessfully";
		}
		else
		{
			$rollBackFlag			= 1;
			$rollBackMsg			= $db->errormsg;
		}
		
		if($updStatus==1)
		{
			$sql = "SELECT
					STD.intItemId,
					STD.dblQty,
					STD.intTransfNo,
					STD.intTransfYear, 
					STH.intRequisitionNo,
					STH.intRequisitionYear,
					SRH.intReqFromStores,
					SRD.dblQty as SRNQty,
					SRD.dblExQty as SRNExcess 
					FROM
					ware_storestransferdetails STD
					INNER JOIN ware_storestransferheader STH ON STD.intTransfNo = STH.intTransfNo AND 
					STD.intTransfYear = STH.intTransfYear
					INNER JOIN ware_storesrequesitionheader SRH ON STH.intRequisitionNo = SRH.intRequisitionNo AND 
					STH.intRequisitionYear = SRH.intRequisitionYear
					INNER JOIN ware_storesrequesitiondetails SRD ON SRH.intRequisitionNo = SRD.intRequisitionNo AND SRH.intRequisitionYear = SRD.intRequisitionYear AND STD.intItemId = SRD.intItemId
 					WHERE
					STD.intTransfNo = '$transfNo' AND
					STD.intTransfYear = '$year' ";
			
			$result 		= $db->RunQuery2($sql);
			$toSavedQty		= 0;
			$transSavedQty	= 0;
			while($row=mysqli_fetch_array($result))
			{
				
				$toSavedQty+=round($row['dblQty'],4);
				$item 		= $row['intItemId'];
				$Qty	 	= $row['dblQty'];
				$fromStores = $row['intReqFromStores'];
				
				
				$resultG 	= getGrnWiseStockBalance_bulk($location,$item);
				while($rowG=mysqli_fetch_array($resultG))
				{
					if(($Qty>0) && ($rowG['stockBal']>0))
					{
						if($Qty<=$rowG['stockBal'])
						{
							$saveQty = $Qty;
							$Qty	 = 0;
						}
						else if($Qty>$rowG['stockBal'])
						{
							$saveQty = $rowG['stockBal'];
							$Qty	 = $Qty-$saveQty;
						}
						$grnNo		 = $rowG['intGRNNo'];
						$grnYear	 = $rowG['intGRNYear'];
						$grnDate	 = $rowG['dtGRNDate'];
						$grnRate	 = $rowG['dblGRNRate'];	
						$currency	 = loadCurrency($grnNo,$grnYear,$item);
						$saveQty	 = round($saveQty,4);		
						if($saveQty>0)
						{	
							$sqlI 	 = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
						VALUES ('$company','$location','$transfNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','-$saveQty','STORESTRANSFER','$userId',now())";
							$resultI = $db->RunQuery2($sqlI);
							if($resultI)
							{
								$sqlJ 	 = "INSERT INTO `ware_sub_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,intSubStores,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
							VALUES ('$company','$location','$fromStores','$transfNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','$saveQty','STORESTRANSFER','$userId',now())";
								$resultJ = $db->RunQuery2($sqlJ);
								if($resultJ)
								{
									$transSavedQty+=$saveQty;
								}
							}
							if((!$resultI) || (!$resultJ))
							{
								$rollBackFlag 	= 1;
								$sqlM			= $sqlI;
								$rollBackMsg 	= $db->errormsg;
							}
						}
					}
				}
			}
		}
	}
	else
	{
		$rollBackFlag 	= 1;
		$rollBackMsg	= $db->errormsg;
	}
	//echo $rollBackFlag;
	
 	if(($toSavedQty!=$transSavedQty) && ($rollBackFlag!=1))
	{
		$rollBackFlag	= 1;
		$rollBackMsg 	= "Approval Error".$toSavedQty.'!='.$transSavedQty;
		$sqlM			= $toSavedQty."-".$transSavedQty;
	}
	if(($rollBackFlag==0) &&($status==1))
	{
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		$response['msg'] 		= "Final Approval raised sucessfully";
	}
	else if($rollBackFlag==1)
	{		
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
	}
	else if($rollBackFlag==0)
	{		
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		$response['msg'] 		= $rollBackMsg;
	}
	else
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= "Approval fail";
	}
	
	$db->CloseConnection();
	echo json_encode($response);
}
function getStockBalance_bulk($stores,$item)
{
	global $db;
	   $sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
			FROM ware_stocktransactions_bulk
			WHERE
			ware_stocktransactions_bulk.intItemId =  '$item' AND 
			ware_stocktransactions_bulk.intLocationId = (SELECT intLocation FROM mst_substores WHERE intId='$stores')
			GROUP BY
			ware_stocktransactions_bulk.intItemId";

	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	return val($row['stockBal']);	
}
function getGrnWiseStockBalance_bulk($location,$item)
{
	global $db;
	  $sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) AS stockBal,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear,
			ware_stocktransactions_bulk.dtGRNDate,
			ware_stocktransactions_bulk.dblGRNRate
			FROM ware_stocktransactions_bulk
			WHERE
			ware_stocktransactions_bulk.intItemId =  '$item' AND
			ware_stocktransactions_bulk.intLocationId =  '$location'
			GROUP BY
			ware_stocktransactions_bulk.intItemId,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear, 
			ware_stocktransactions_bulk.dblGRNRate 
			ORDER BY
			ware_stocktransactions_bulk.dtGRNDate ASC";

	$result = $db->RunQuery2($sql);
	return $result;	
}
function loadCurrency($grnNo,$grnYear,$item)
{
	global $db;
	
	if(($grnNo!=0) && ($grnYear!=0))
	{
		$sql = "SELECT
				trn_poheader.intCurrency as currency 
				FROM
				ware_grnheader
				Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
				WHERE
				ware_grnheader.intGrnNo =  '$grnNo' AND
				ware_grnheader.intGrnYear =  '$grnYear'";
	}
	else
	{
		$sql = "SELECT
				mst_item.intCurrency as currency 
				FROM mst_item
				WHERE
				mst_item.intId =  '$item'";
	}
	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
	return $row['currency'];;	
}

?>
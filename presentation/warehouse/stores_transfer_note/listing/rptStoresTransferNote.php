<?php
	session_start();
	$backwardseperator = "../../../../";
	$companyId 		= $_SESSION['headCompanyId'];
	$locationId 	= $_SESSION['CompanyID'];
	
	$intUser  		= $_SESSION["userId"];
	$mainPath 	  	= $_SESSION['mainPath'];
	$thisFilePath 	=  $_SERVER['PHP_SELF'];
	include  	"{$backwardseperator}dataAccess/Connector.php";
	
	$transfNo 		= $_REQUEST['transfNo'];
	$year 			= $_REQUEST['year'];
	$approveMode	= $_REQUEST['approveMode'];
	$programCode	= 'P0252';
	$userPermission = 0;
	
	$sql = "SELECT STH.intTransfNo,STH.intTransfYear,STH.intRequisitionNo,STH.intRequisitionYear,SSRT.strName AS srnTo,
			SSRF.strName AS srnFrom,DATE(STH.datdate) AS dtDate,STH.strNote,STH.intStatus,STH.intApproveLevels
			FROM ware_storestransferheader STH
			INNER JOIN ware_storesrequesitionheader SRH ON SRH.intRequisitionNo=STH.intRequisitionNo AND
			SRH.intRequisitionYear=STH.intRequisitionYear
			INNER JOIN mst_substores SSRT ON SSRT.intId=SRH.intReqToStores
			INNER JOIN mst_substores SSRF ON SSRF.intId=SRH.intReqFromStores
			WHERE STH.intTransfNo = '$transfNo' AND
			STH.intTransfYear = '$year' /*AND
			STH.intCompanyId = '$locationId'*/ ";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{	
		$SRNNo 			= $row['intRequisitionNo'];
		$SRNYear 		= $row['intRequisitionYear'];
		$SRNFrom 		= $row['srnFrom'];
		$SRNTo			= $row['srnTo'];
		$Date			= $row['dtDate'];
		$Note 			= $row['strNote'];
		$intStatus 		= $row['intStatus'];
		$approveLevel 	= $row['intApproveLevels'];
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Stores Transfer Note Report</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="rptStoresTransferNote-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position: absolute;
	left: 309px;
	top: 191px;
	width: 650px;
	height: 322px;
	z-index: 1;
}
.APPROVE {
	font-size: 16px;
	font-weight: bold;
	font-family: "Arial Black", Gadget, sans-serif;
	color: #36F;
}
</style>
</head>

<body>
<?php
$rpApproveLevel = (int)getMainApproveLevel('Stores Transfer Note');
 
if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="../../../../images/pending.png"  /></div>
<?php
}
?>
<form id="frmSTNReport" name="frmSTNReport" method="post" action="rptStoresTransferNote.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>
<tr>
<td colspan="3"></td>
</tr>
</table>

<div align="center">
<div style="background-color:#FFF" ><strong>STORES TRANSFER NOTE REPORT</strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td colspan="2">
  <table width="100%">
  <tr>
    <td colspan="8" align="center" bgcolor="#FFFFFF">
    <?php
	if($intStatus>1 && $approveMode==1)
	{
		$k=$rpApproveLevel+2-$intStatus;
		$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser' ";	
		
		$resultp = $db->RunQuery($sqlp);
		$rowp=mysqli_fetch_array($resultp);
		$userPermission=0;
		if($rowp['int'.$k.'Approval']==1)
		{	
		?>
    		<img src="../../../../images/approve.png" align="middle" class="noPrint mouseover" id="imgApprove" />
   		 <?php
		}
		
		$sqlp = "SELECT
				 menupermision.intReject
				 FROM menupermision 
				 INNER JOIN menus ON menupermision.intMenuId = menus.intId
				 WHERE
				 menus.strCode = '$programCode' AND
				 menupermision.intUserId = '$intUser' ";	
		
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 $userPermission=0;
		 if($rowp['intReject']==1)
		 {
			?>
			<img src="../../../../images/reject.png" align="middle" class="noPrint mouseover" id="imgReject" />
            <?php
		 }
	}
	?>
    </td>
  </tr>
  <tr>
  <?php
 	if($intStatus==1)
	{
	?>
   <td colspan="8" align="center" class="APPROVE" style="color:#6C6">CONFIRMED</td>
   <?PHP
	}
	else if($intStatus==0)
	{
   ?>
   <td colspan="9" align="center" class="APPROVE" style="color:#F00">REJECTED</td>
   <?php
	}
	else
	{
   ?>
   <td width="15%" colspan="9" align="center" class="APPROVE">PENDING</td>
   <?php
	}
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="10%"><span class="normalfnt">STN No</span></td>
    <td width="1%" align="center" valign="middle">:</td>
    <td width="19%"><span class="normalfnt"><?php echo $transfNo.'/'.$year; ?></span></td>
    <td width="2%" align="center" valign="middle">&nbsp;</td>
    <td width="12%"><span class="normalfnt">SRN No</span></td>
    <td width="1%" align="center" valign="middle">:</td>
    <td width="21%"><span class="normalfnt"><?php echo $SRNNo.'/'.$SRNYear; ?></span></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">SRN From</td>
    <td align="center" valign="middle">:</td>
    <td><span class="normalfnt"><?php echo $SRNFrom; ?></span></td>
    <td align="center" valign="middle">&nbsp;</td>
    <td><span class="normalfnt">Date</span></td>
    <td align="center" valign="top">:</td>
    <td valign="top" class="normalfnt"><?php echo $Date; ?></td>
    </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">SRN To</td>
    <td align="center" valign="middle">:</td>
    <td><span class="normalfnt"><?php echo $SRNTo; ?></span></td>
    <td align="center" valign="middle">&nbsp;</td>
    <td class="normalfnt">Note</td>
    <td align="center" valign="middle"></td>
    <td rowspan="2" valign="top" class="normalfnt"><?php echo $Note; ?></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td class="normalfnt"></td>
    <td align="center" valign="middle"></td>
    </tr>
  </table>
  </td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td colspan="2">
    <table width="100%">
       <tr>
        <td width="1%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
            <table width="100%" class="bordered tblMainGrid" id="tbl1" >
            	<tr class="">
                    <th width="12%" >Main Category</th>
                    <th width="13%" >Sub Category</th>
                    <th width="24%" >Item Description</th>
                    <th width="5%">UOM</th>
                    <th width="9%">SRN Qty</th>
                    <th width="10%">Excess Qty</th>
                    <th width="7%">Bal Qty</th>
                    <th width="14%">Total Transf Qty</th>
                    <th width="6%">Qty</th>
                  <th width="9%" >Color Room Stock</th> 
                  <th width="9%" >Style Stock</th> 
                  <th width="9%" >Bulk Stock</th> 
                </tr>
                <?php
				$result1 = getGridDetails($transfNo,$year,$locationId);
				while($row1=mysqli_fetch_array($result1))
				{
					
				$stockBal_bulk_Qty=getStockBalance_bulk($row1['intReqToStores'],$row1['intItemId']);
 				$stockBal_style_Qty=getStockBalance($row1['intReqToStores'],$row1['intOrderYear'],$row1['strOrderNos'],$row1['intItemId']);
 				$stockBal_color_Qty=getExcess_color_stock($row1['intReqToStores'],$row1['intItemId']);
 					
				?>
                <tr class="normalfnt">
                 <td align="left" class="normalfnt"><?php echo $row1['mainCatName']; ?></td>
                 <td align="left" class="normalfnt"><?php echo $row1['subCatName']; ?></td>
                 <td align="left" class="normalfnt" ><?php echo $row1['itemName']; ?></td>
                 <td style="text-align:center" class="normalfnt" ><?php echo $row1['unitName']; ?></td>
                 <td style="text-align:center" class="normalfnt"><?php echo $row1['SRNQty']; ?></td>
                 <td style="text-align:center" class="normalfnt"><?php echo $row1['excessQty']; ?></td>
                 <td style="text-align:center" class="normalfnt"><?php echo $row1['SRNQty']+$row1['excessQty']-$row1['totTransQty']; ?></td>
                 <td style="text-align:center" class="normalfnt"><?php echo $row1['totTransQty']; ?></td>
                 <td style="text-align:center" class="normalfnt"><?php echo ($row1['dblQty']+$row1['dblExcessQty']); ?></td>
                 <td style="text-align:center" class="normalfnt"><?php echo $stockBal_bulk_Qty; ?></td>
                 <td style="text-align:center" class="normalfnt"><?php echo $stockBal_style_Qty; ?></td>
                 <td style="text-align:center" class="normalfnt"><?php echo $stockBal_color_Qty; ?></td>
               
                </tr>
                <?php
				}
				?>
            </table>
        </td>
        <td width="3%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<tr>
  <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
  <td width="884" bgcolor="#FFFFFF">&nbsp;</td>
</tr>
<?php  
 	if($intStatus!=0)
	{

				for($i=1; $i<=$rpApproveLevel; $i++)
				{
					 $sqlc = "SELECT
								ware_storestransferheader_approvedby.intApproveUser,
								ware_storestransferheader_approvedby.dtApprovedDate,
								sys_users.strUserName,
								ware_storestransferheader_approvedby.intApproveLevelNo
								FROM
								ware_storestransferheader_approvedby
								INNER JOIN sys_users ON ware_storestransferheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_storestransferheader_approvedby.intTransfNo = '$transfNo' AND
								ware_storestransferheader_approvedby.intYear = '$year' AND
								ware_storestransferheader_approvedby.intApproveLevelNo = '$i'
								";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
						if($i==1)
						$desc="1st ";
						else if($i==2)
						$desc="2nd ";
						else if($i==3)
						$desc="3rd ";
						else
						$desc=$i."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['strUserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['strUserName']=='')
					 $desc2='---------------------------------';
				?>

            <tr>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><?php echo $desc; ?>Approved By - <?php echo $desc2;?></span></td>
            </tr>
<?php
			}
	}
?>

<tr height="40">
  <td colspan="2" align="center" class="normalfntMid">Printed Date: <?php echo date("Y/m/d") ?></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php
function getMainApproveLevel($program)
{
	global $db;
	$sql = "SELECT intApprovalLevel FROM sys_approvelevels WHERE strName='$program' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	return $row['intApprovalLevel'];
}
function getGridDetails($transfNo,$year,$locationId)
{
	global $db;
	$sql = "SELECT STD.intTransfNo,
			STD.intTransfYear,
			STD.intItemId,
			MI.strName AS itemName,
			MC.strName AS mainCatName,
			MC.intId AS mainCatId,
			SC.strName AS subCatName,
			SC.intId AS subCatId,
			STD.dblQty,
			STD.dblExcessQty,
			(SELECT SUM(dblQty) FROM ware_storesrequesitiondetails SRD WHERE SRD.intRequisitionNo=STH.intRequisitionNo AND 
			SRD.intRequisitionYear=STH.intRequisitionYear AND SRD.intItemId=STD.intItemId ) AS SRNQty,
			
			(SELECT SUM(dblExQty) FROM ware_storesrequesitiondetails SRD WHERE SRD.intRequisitionNo=STH.intRequisitionNo AND 
			SRD.intRequisitionYear=STH.intRequisitionYear AND SRD.intItemId=STD.intItemId ) AS excessQty,
		
			(SELECT SUM(dblTransferQty+dblExTransferQty) FROM ware_storesrequesitiondetails SRD WHERE SRD.intRequisitionNo=STH.intRequisitionNo AND 
			SRD.intRequisitionYear=STH.intRequisitionYear AND SRD.intItemId=STD.intItemId ) AS totTransQty,
			
			MU.intId AS unitId,MU.strName AS unitName,
			
			ware_storesrequesitionheader.intOrderYear,
			ware_storesrequesitionheader.strOrderNos,
			ware_storesrequesitionheader.intReqFromStores, 
			ware_storesrequesitionheader.intReqToStores 
			
			
			FROM ware_storestransferdetails STD
			INNER JOIN ware_storestransferheader STH ON STH.intTransfNo=STD.intTransfNo AND STH.intTransfYear=STD.intTransfYear

			INNER JOIN ware_storesrequesitionheader ON STH.intRequisitionNo = ware_storesrequesitionheader.intRequisitionNo AND STH.intRequisitionYear = ware_storesrequesitionheader.intRequisitionYear
			
			INNER JOIN mst_item MI ON MI.intId=STD.intItemId
			INNER JOIN mst_maincategory MC ON MC.intId=MI.intMainCategory
			INNER JOIN mst_subcategory SC ON SC.intId=MI.intSubCategory
			INNER JOIN mst_units MU ON MU.intId=MI.intUOM
			WHERE STD.intTransfNo='$transfNo' AND
			STD.intTransfYear='$year' ";
			
	$result = $db->RunQuery($sql);
	return $result;
}
	function getStockBalance($stores,$orderYear,$orderNos,$item)
	{
		global $db;
		global $db;
		 $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intOrderYear = '$orderYear' AND 
				FIND_IN_SET(ware_stocktransactions.intOrderNo,'$orderNos') AND 
 				ware_stocktransactions.intLocationId = (SELECT intLocation FROM mst_substores WHERE intId='$stores') AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($reqFromStores,$item)
	{
		global $db;
		   $sql = "SELECT
				ROUND(IFNULL(SUM(ware_stocktransactions_bulk.dblQty),0),2) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND 
				ware_stocktransactions_bulk.intLocationId = (SELECT intLocation FROM mst_substores WHERE intId='$reqFromStores')
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return ($row['stockBal']==''?0:$row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getExcess_color_stock($stores,$item)
	{
		global $db;
		   $sql = "SELECT
					SUM(ware_sub_stocktransactions_bulk.dblQty) AS stockBal
					FROM ware_sub_stocktransactions_bulk
					WHERE
					ware_sub_stocktransactions_bulk.intItemId =  '$item' AND 
					ware_sub_stocktransactions_bulk.intLocationId = (SELECT intLocation FROM mst_substores WHERE intId='$stores') AND 
					ware_sub_stocktransactions_bulk.intOrderNo IS NULL 
					GROUP BY
					ware_sub_stocktransactions_bulk.intItemId";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	
?>
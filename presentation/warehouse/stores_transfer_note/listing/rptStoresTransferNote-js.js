// JavaScript Document
$(document).ready(function() {
	
	$("#frmSTNReport").validationEngine();
	$('#frmSTNReport #imgApprove').live('click',urlApprove);
	$('#frmSTNReport #imgReject').live('click',urlReject);
});
function urlApprove()
{
	var val = $.prompt('Are you sure you want to approve this Transfer note ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "rptStoresTransferNote-db-set.php"+window.location.search+'&status=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmSTNReport #imgApprove').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSTNReport #imgApprove').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}
function urlReject()
{
	var val = $.prompt('Are you sure you want to reject this Transfer Note ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "rptStoresTransferNote-db-set.php"+window.location.search+'&status=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmSTNReport #imgReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx1()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSTNReport #imgReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx1()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}
function alertx()
{
	$('#frmSTNReport #imgApprove').validationEngine('hide')	;
}
function alertx1()
{
	$('#frmSTNReport #imgReject').validationEngine('hide')	;
}
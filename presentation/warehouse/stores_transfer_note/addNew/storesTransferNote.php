<?php
///////////////////////////////
// H B G KORALA   ////////////
//////////////////////////////
session_start();
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$location 			= $_SESSION['CompanyID'];
$company 			= $_SESSION['headCompanyId'];
$thisFilePath 		= $_SERVER['PHP_SELF'];
$intUser  			= $_SESSION["userId"];
$intStatus			= '';
$approveLevels		= '';
//include "../add_new/{$backwardseperator}dataAccess/permisionCheck.inc";
include_once  	"{$backwardseperator}dataAccess/Connector.php";
require_once $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";

$obj_commonErr		= new cls_commonErrorHandeling_get($db);

$serialNo			= $_REQUEST['transfNo'];
$transfNo 			= $_REQUEST['transfNo'];
$year 				= $_REQUEST['year'];

$programName		= 'Stores Transfer Note';
$programCode		= 'P0258';
	
if($transfNo!='' && $year!='')
{
	$result = loadHeader($transfNo,$year,$location);
	while($row=mysqli_fetch_array($result))
	{
		$SRNNo 			= $row['intRequisitionNo'];
		$SRNYear 		= $row['intRequisitionYear'];
		$Date			= $row['dtDate'];
		$tranferFrom 	= $row['intReqFromStores'];
		$tranferTo 		= $row['intReqToStores'];
		$Note 			= $row['strNote'];
		$approveLevels 	= $row['intApproveLevels'];
		$intStatus 		= $row['intStatus'];
		$orderYear 		= $row['intOrderYear'];
		$graphicNo 		= $row['strGraphicNo'];
	}
}

 	$editPermition		=$obj_commonErr->get_permision_withApproval_save($intStatus,$approveLevels,$intUser,$programCode,'RunQuery');
	$editMode			=$editPermition['permision'];
 	$confirnPermition	=$obj_commonErr->get_permision_withApproval_confirm($intStatus,$approveLevels,$intUser,$programCode,'RunQuery');
	$confirmMode		=$confirnPermition['permision'];
			 
?>
<script type="text/javascript" >
	var intStatus 		= '<?php echo $intStatus; ?>' ;
	var approvalLevels 	= '<?php echo $approveLevels; ?>' ;
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Stores Transfer Note</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="storesTransferNote-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body>
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
<form id="frmStoresTransferNote" name="frmStoresTransferNote" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text"> Stores Transfer Note</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableBorder_allRound">
          <tr>
            <td width="6%" height="22" class="normalfnt">&nbsp;STN No</td>
            <td width="19%"><input name="txtTransNo" type="text" disabled="disabled" class="txtText" id="txtTransNo" style="width:60px" value="<?php echo $transfNo; ?>" />&nbsp;<input name="txtTransYear" type="text" disabled="disabled" class="txtText" id="txtTransYear" style="width:40px" value="<?php echo $year; ?>" /></td>
            <td width="9%">&nbsp;</td>
            <td width="37%">&nbsp;</td>
            <td width="5%" class="normalfnt">Date</td>
            <td width="24%" align="left">&nbsp;<input name="dtDate" type="text" value="<?php echo(($Date)?$Date:date('Y-m-d')); ?>" class="txtbox" id="dtDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/></td>
            </tr>
          
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="2" class="tableBorder_allRound">
<tr>
            <td height="22" class="normalfnt">&nbsp;SRN No</td>
            <td><select name="cboSRNYear" id="cboSRNYear" style="width:60px" <?php echo($transfNo!=''?'disabled="disabled"':''); ?>>
                  <option value=""></option>
                  <?php
					 $sql = "SELECT 
							distinct 
							ware_storesrequesitionheader.intRequisitionYear
							FROM ware_storesrequesitionheader
							WHERE
							ware_storesrequesitionheader.intLocationId =  '$location' AND
							ware_storesrequesitionheader.intStatus =  '1' 
							order by ware_storesrequesitionheader.intRequisitionYear desc";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intRequisitionYear']==$SRNYear)
							echo "<option value=\"".$row['intRequisitionYear']."\" selected=\"selected\">".$row['intRequisitionYear']."</option>";	
						else
							echo "<option value=\"".$row['intRequisitionYear']."\">".$row['intRequisitionYear']."</option>";	
					}
				?>
              </select>
              <select name="cboSRNno" id="cboSRNno" style="width:103px" <?php echo($transfNo!=''?'disabled="disabled"':''); ?>>
                  <option value=""></option>
                  <?php
					if($transfNo!='')
					{
						$sql = "SELECT intRequisitionNo FROM ware_storesrequesitionheader WHERE intLocationId='$location' ";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							if($row['intRequisitionNo']==$SRNNo)
								echo "<option value=\"".$row['intRequisitionNo']."\" selected=\"selected\">".$row['intRequisitionNo']."</option>";	
							else
								echo "<option value=\"".$row['intRequisitionNo']."\">".$row['intRequisitionNo']."</option>";	
						}
					}
				?>
            </select></td>
            <td class="normalfnt">Transfer To</td>
            <td><select style="width:203px" name="cboTrnFrom" id="cboTrnFrom" disabled="disabled">
            <option value=""></option>
            <?php
			$sql=" SELECT
					mst_substores.intId,
					mst_substores.strName
					FROM `mst_substores`
					WHERE
					mst_substores.intParentId = 0 AND
					mst_substores.intMainStoresType = 0 AND
					mst_substores.intLocation = $location AND
					mst_substores.intStatus = 1
					ORDER BY
					mst_substores.strName ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$tranferFrom)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
			?>
            </select></td>
            <td class="normalfnt">Transfer From</td>
            <td width="23%" align="left"><select style="width:203px" name="cboTrnTo" id="cboTrnTo" disabled="disabled">
            <option value=""></option>
            <?php
			$sql=" SELECT
					mst_substores.intId,
					mst_substores.strName
					FROM `mst_substores`
					WHERE
					mst_substores.intParentId = 0 AND
					mst_substores.intMainStoresType = 1 AND
					mst_substores.intLocation = $location AND
					mst_substores.intStatus = 1
					ORDER BY
					mst_substores.strName ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$tranferTo)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
			?>
            </select></td>
            <td width="0%" align="left">&nbsp;</td>
          </tr>          <tr>
            <td height="27" valign="top" rowspan="2" class="normalfnt">&nbsp;Note</td>
            <td rowspan="2"><textarea name="txtNote" id="txtNote" cols="31" rows="3"><?php echo $Note; ?></textarea></td>
            <td  class="normalfnt">Graphic No</td>
            <td class="normalfnt"><input id="txtOrderYear" value="<?php echo $graphicNo; ?>"  style="width:203px" disabled="disabled"></td>
            <td  class="normalfnt">&nbsp;</td>
            <td  class="normalfnt">&nbsp;</td>
            <td align="right" valign="top">&nbsp;</td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td valign="top" class="normalfnt">&nbsp;</td>
            <td align="right" valign="top">&nbsp;</td>
            <td align="right" valign="bottom">
                 <a id="butAddItems" class="button green small" style="" name="butAddItems"> Add New </a>
            </td>
            <td align="right" valign="bottom">&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr><td height="10"></td></tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="bordered" id="tblMain" >
            <tr>
              <th width="4%" height="22" >Del</th>
              <th width="8%" >Order No</th>
              <th width="10%" >Sales Order</th>
              <th width="12%" >Main Category</th>
              <th width="11%" >Sub Category</th>
              <th width="22%" >Item Description</th>
              <th width="4%">UOM</th>
              <th width="7%">SRN Qty</th>
              <th width="6%">Extra </th>
              <th width="7%">Bal Qty</th>
              <th width="9%">Qty</th>
              </tr>
              <?php
			  if($transfNo!="" && $year!="")
				{
					$sql = " SELECT STD.intTransfNo,STD.intTransfYear,STD.intItemId,MI.strName AS itemName,
								MC.strName AS mainCatName,MC.intId AS mainCatId,SC.strName AS subCatName,SC.intId AS subCatId,STD.dblQty,
								(SELECT SUM(dblQty) FROM ware_storesrequesitiondetails SRD WHERE SRD.intRequisitionNo=STH.intRequisitionNo AND 
								SRD.intRequisitionYear=STH.intRequisitionYear AND SRD.intItemId=STD.intItemId ) AS SRNQty,
								(SELECT SUM(dblExQty) FROM ware_storesrequesitiondetails SRD WHERE SRD.intRequisitionNo=STH.intRequisitionNo AND 
								SRD.intRequisitionYear=STH.intRequisitionYear AND SRD.intItemId=STD.intItemId ) AS excessQty,
								(SELECT SUM(dblTransferQty+dblExTransferQty) FROM ware_storesrequesitiondetails SRD WHERE SRD.intRequisitionNo=STH.intRequisitionNo AND 
								SRD.intRequisitionYear=STH.intRequisitionYear AND SRD.intItemId=STD.intItemId ) AS totTransQty,
								MU.intId AS unitId,MU.strName AS unitName
								FROM ware_storestransferdetails STD
								INNER JOIN ware_storestransferheader STH ON STH.intTransfNo=STD.intTransfNo AND STH.intTransfYear=STD.intTransfYear
								INNER JOIN mst_item MI ON MI.intId=STD.intItemId
								INNER JOIN mst_maincategory MC ON MC.intId=MI.intMainCategory
								INNER JOIN mst_subcategory SC ON SC.intId=MI.intSubCategory
								INNER JOIN mst_units MU ON MU.intId=MI.intUOM
								WHERE STD.intTransfNo='$transfNo' AND
								STD.intTransfYear='$year' ";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$stockBalQty=getStockBalance_bulk($location,$tranferFrom,$row['intItemId']);
						$balToTransfQty=$row['SRNQty']-$row['totTransQty'];
						$maxTrnsfQty=$balToTransfQty;
						if($stockBalQty<$maxTrnsfQty)
						{
							$maxTrnsfQty=$stockBalQty;
						}
					?>
                    	<tr class="normalfnt">
                        <td align="center" bgcolor="#FFFFFF"><img class="delImg" src="../../../../images/del.png" width="15" height="15" /></td>
                        <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['mainCatId']; ?>" class="mainCatName"><?php echo $row['mainCatName']; ?></td>
                        <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['mainCatId']; ?>" class="mainCatName"><?php echo $row['mainCatName']; ?></td>
                        <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['mainCatId']; ?>" class="mainCatName"><?php echo $row['mainCatName']; ?></td>
                        <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['subCatId']; ?>" class="subCatName"><?php echo $row['subCatName']; ?></td>
                        <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['intItemId']; ?>" class="item"><?php echo $row['itemName']; ?></td>
                        <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['unitId']; ?>" class="uom"><?php echo $row['unitName']; ?></td>
                        <td align="center" bgcolor="#FFFFFF" class="srnQty"><?php echo $row['SRNQty']; ?></td>
                        <td align="center" bgcolor="#FFFFFF" class="excessQty"><?php echo $row['excessQty']; ?></td>
                        <td align="center" bgcolor="#FFFFFF" class="balToTransfQty"><?php echo $row['SRNQty']+$row['excessQty']-$row['totTransQty']; ?></td>
                        <td align="center" bgcolor="#FFFFFF" id=""><input  id="maxTrnsfQty" class="validate[required,custom[number],max[<?php echo $maxTrnsfQty; ?>]] calculateValue Qty" style="width:70px;text-align:center" type="text" value="<?php echo $row['dblQty']; ?>" onKeyUp="chekQty(this);"/></td></tr>
                    <?php
					}
				}
			 ?>
            </table>
          </div></td>
      </tr>
        <tr>
          <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
            <tr>
              <td width="100%" align="center" bgcolor=""><a id="butNew"  class="button white medium" style="" name="butAddNewTask"> New </a>
                <?php if($editMode==1){ ?>
                <a id="butSave" class="button white medium" style="" name="butSave"> Save </a>
                <?php } ?>
                 <a id="butConfirm" class="button white medium"  <?php if($confirmMode!=1){ ?>style="display:none"<?php } ?> name="butConfirm"> Confirm </a>
                 <a id="butReport" class="button white medium" <?php if($serialNo==''){ ?>style="display:none"<?php } ?> name="butReport"  > Report </a>
                <a id="butClose" class="button white medium" name="butClose" href="../../../../main.php">Close</a></td>
            </tr>
          </table></td>
        </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>


</body>
</html>
<?php 
	//--------------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$style,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.strStyleNo =  '$style' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$reqFromStores,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND 
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}

//------------------------------function load Header---------------------
function loadHeader($transfNo,$year,$location)
{
	global $db;
	$sql = "SELECT ST.intTransfNo,ST.intTransfYear,ST.intRequisitionNo,
			ST.intRequisitionYear,ST.strNote,ST.intStatus,
			ST.intApproveLevels,DATE(ST.datdate) AS dtDate,SR.intReqFromStores,SR.intReqToStores,
			SR.intOrderYear,
			SR.strGraphicNo 
 			FROM ware_storestransferheader ST
			INNER JOIN ware_storesrequesitionheader SR ON SR.intRequisitionNo=ST.intRequisitionNo AND 
			SR.intRequisitionYear=ST.intRequisitionYear
			WHERE intTransfNo='$transfNo' AND
			intTransfYear='$year' AND
			intCompanyId='$location' ";
	$result = $db->RunQuery($sql);
	return $result;
}
//------------------------------function load Default Department---------------------
function loadDefaultDepartment($intUser){
	global $db;
	 $sql1 = "SELECT
				sys_users.intDepartmentId
				FROM
				sys_users
				WHERE
				sys_users.intUserId =  '$intUser'";	
	$result1 = $db->RunQuery($sql1);
	$row1=mysqli_fetch_array($result1);
	$department=$row1['intDepartmentId'];
	return $department;
}
//------------------------------function load loadEditMode---------------------
function loadRejectionMode($programCode,$status,$approveLevels,$userId)
{
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
			 menupermision.intReject 
			 FROM menupermision 
			 Inner Join menus ON menupermision.intMenuId = menus.intId
			 WHERE
			 menus.strCode = '$programCode' AND
			 menupermision.intUserId = '$userId'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1)
	{
		 if($status!=0)
		 {
			 $rejectMode=1;
		 }
	}
	return $rejectMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$status,$approveLevels,$userId)
{
	global $db;
	$confirmatonMode = 0;
	$k = $approveLevels+2-$status;
	
	$sqlp = "SELECT
			 menupermision.int".$k."Approval 
			 FROM menupermision 
			 Inner Join menus ON menupermision.intMenuId = menus.intId
			 WHERE
			 menus.strCode =  '$programCode' AND
			 menupermision.intUserId = '$userId' ";	
	
	$resultp = $db->RunQuery($sqlp);
	$rowp	 = mysqli_fetch_array($resultp);
	if($rowp['int'.$k.'Approval']==1)
	{
		 if($intStatus!=1)
		 {
		 	$confirmatonMode=1;
		 }
	} 
	return $confirmatonMode;
}
?>

var basePath	="presentation/warehouse/reports/";
ViewReport_excel_new

$(document).ready(function(){
	//------------------------
	jQuery("#formID").validationEngine();
	$('#tblPOSummeryHead').click(function(){
		
		headShowHide('tblPOSummeryHead','tblPOSummeryBody');
		
	});
	
	$('#tblPOSummeryBody thead').click(function(){
		
		bodyShowHide('tblPOSummeryHead','tblPOSummeryBody');
		
	});
	//----------------------
	
	$('#tblPOGRNSummeryHead').click(function(){

		headShowHide('tblPOGRNSummeryHead','tblPOGRNSummeryBody');
		
	});
	
	$('#tblPOGRNSummeryBody thead').click(function(){
		
		bodyShowHide('tblPOGRNSummeryHead','tblPOGRNSummeryBody');
		
	});
	//----------------------
	
	$('#tblPOItemSummeryHead').click(function(){

		headShowHide('tblPOItemSummeryHead','tblPOItemSummeryBody');

	});
	
	$('#tblPOItemSummeryBody thead').click(function(){
		
		bodyShowHide('tblPOItemSummeryHead','tblPOItemSummeryBody');
			
	});
	
	//-------------------------
	$('#tblSubCatPOGRNSummeryHead').click(function(){

		headShowHide('tblSubCatPOGRNSummeryHead','tblSubCatPOGRNSummeryBody');
		
	});
	
	$('#tblSubCatPOGRNSummeryBody thead').click(function(){
		
		bodyShowHide('tblSubCatPOGRNSummeryHead','tblSubCatPOGRNSummeryBody');
		
	});
	
	//-------------------------
	$('#tblSubCatGRNPOSummeryHead').click(function(){

		headShowHide('tblSubCatGRNPOSummeryHead','tblSubCatGRNPOSummeryBody');
		
	});
	
	$('#tblSubCatGRNPOSummeryBody thead').click(function(){
		
		bodyShowHide('tblSubCatGRNPOSummeryHead','tblSubCatGRNPOSummeryBody');
		
	});
	//-------------------------
	
	$('#tblPOPendingSummeryHead').click(function(){

		headShowHide('tblPOPendingSummeryHead','tblPOPendingSummeryBody');

	});
	
	$('#tblPOPendingSummeryBody thead').click(function(){
		
		bodyShowHide('tblPOPendingSummeryHead','tblPOPendingSummeryBody');
			
	});
	
	//-------------------------
	
	$('#tblStockBalanceHead').click(function(){

		headShowHide('tblStockBalanceHead','tblStockBalanceBody');

	});
	
	$('#tblStockBalanceBody thead').click(function(){
		
		bodyShowHide('tblStockBalanceHead','tblStockBalanceBody');
		
	});
	
	//-------------------------
	
	$('#tblStkBalExcellHead').click(function(){

		headShowHide('tblStkBalExcellHead','tblStkBalExcellBody');

	});
	
	$('#tblStkBalExcellBody thead').click(function(){
		
		bodyShowHide('tblStkBalExcellHead','tblStkBalExcellBody');
			
	});
	
	//-------------------------
	
	$('#tblGeneralStockMovementHead').click(function(){

		headShowHide('tblGeneralStockMovementHead','tblGeneralStockMovementBody');

	});
	
	$('#tblGeneralStockMovementBody thead').click(function(){
		
		bodyShowHide('tblGeneralStockMovementHead','tblGeneralStockMovementBody');
			
	});
	
	//-------------------------
	
	$('#tblStyleStockMovementHead').click(function(){

		headShowHide('tblStyleStockMovementHead','tblStyleStockMovementBody');

	});
	
	$('#tblStyleStockMovementBody thead').click(function(){
		
		bodyShowHide('tblStyleStockMovementHead','tblStyleStockMovementBody');
			
	});

	//-------------------------
	
	$('#tblDepWiseIssuesHead').click(function(){

		headShowHide('tblDepWiseIssuesHead','tblDepWiseIssuesBody');

	});
	
	$('#tblDepWiseIssuesBody thead').click(function(){
		
		bodyShowHide('tblDepWiseIssuesHead','tblDepWiseIssuesBody');
			
	});

	//-------------------------
	
	$('#tblLocationItemsTransHead').click(function(){

		headShowHide('tblLocationItemsTransHead','tblLocationItemsTransBody');

	});
	
	$('#tblLocationItemsTransBody thead').click(function(){
		
		bodyShowHide('tblLocationItemsTransHead','tblLocationItemsTransBody');
			
	});
	//-------------------------
	
	$('#tblInternalOnloanTrnsActHead').click(function(){

		headShowHide('tblInternalOnloanTrnsActHead','tblInternalOnloanTrnsActBody');

	});
	
	$('#tblInternalOnloanTrnsActBody thead').click(function(){
		
		bodyShowHide('tblInternalOnloanTrnsActHead','tblInternalOnloanTrnsActBody');
			
	});
	//-------------------------
	
	$('#tblInkStockHead').click(function(){

		headShowHide('tblInkStockHead','tblInkStockBody');

	});
	
	$('#tblInkStockBody thead').click(function(){
		
		bodyShowHide('tblInkStockHead','tblInkStockBody');
			
	});


	//-------------------------
	
	$('#tblInkItemStockMovementHead').click(function(){

		headShowHide('tblInkItemStockMovementHead','tblInkItemStockMovementBody');

	});
	
	$('#tblInkItemStockMovementBody thead').click(function(){
		
		bodyShowHide('tblInkItemStockMovementHead','tblInkItemStockMovementBody');
			
	});
	
	$('#tblGrnSummaryHead').click(function(){

		headShowHide('tblGrnSummaryHead','tblGrnSummaryBody');

	});
	
	$('#tblGrnSummaryBody thead').click(function(){
		
		bodyShowHide('tblGrnSummaryHead','tblGrnSummaryBody');
			
	});
	
	$('#tblItemsExpiryHead').click(function(){

		headShowHide('tblItemsExpiryHead','tblItemsExpiryBody');

	});
	
	$('#tblItemsExpiryBody thead').click(function(){
		
		bodyShowHide('tblItemsExpiryHead','tblItemsExpiryBody');
			
	});
	
	$('#tblItemsAgingHead').click(function(){

		headShowHide('tblItemsAgingHead','tblItemsAgingBody');

	});
	
	$('#tblItemsAgingBody thead').click(function(){
		
		bodyShowHide('tblItemsAgingHead','tblItemsAgingBody');
			
	});

	$('#tblmonthlyGPHead').click(function(){

		headShowHide('tblmonthlyGPHead','tblmonthlyGPBody');

	});
	
	$('#tblmonthlyGPBody thead').click(function(){
		
		bodyShowHide('tblmonthlyGPHead','tblmonthlyGPBody');
			
	});
	//----------------------

	$('#tblmonthlyGPTranferinHead').click(function(){

		headShowHide('tblmonthlyGPTranferinHead','tblmonthlyGPTranferinBody');

	});

	$('#tblmonthlyGPTranferinBody thead').click(function(){

		bodyShowHide('tblmonthlyGPTranferinHead','tblmonthlyGPTranferinBody');

	});
	
	$('#tblLocWiseGRNHead').click(function(){

		headShowHide('tblLocWiseGRNHead','tblLocWiseGRNBody');

	});
	
	$('#tblLocWiseGRNBody thead').click(function(){
		
		bodyShowHide('tblLocWiseGRNHead','tblLocWiseGRNBody');
			
	});//////
	//----------------------
	///edit nuwan----------------------
	
	$('#stcMoveRepHead').click(function(){

		headShowHide('stcMoveRepHead','stcMoveRepBody');

	});
	
	$('#stcMoveRepBody thead').click(function(){
		
		bodyShowHide('stcMoveRepHead','stcMoveRepBody');
			
	});
	//end
	
	$('#tblStockBalanceBulkAllHead').click(function(){

		headShowHide('tblStockBalanceBulkAllHead','tblStockBalanceBulkAllBody');

	});
	
	$('#tblStockBalanceBulkAllBody thead').click(function(){
		
		bodyShowHide('tblStockBalanceBulkAllHead','tblStockBalanceBulkAllBody');
			
	});
	//------------------------
	

	
	
	
	
////////////////////////////////////////////////////////////////
 //-------------------------------------------- 
  $('#frmPOItemSummery #cboMainCategory').change(function(){
		var mainCategory = $('#frmPOItemSummery #cboMainCategory').val();
		var url 		= basePath+"report-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmPOItemSummery #cboSubCategory').html(httpobj.responseText);
		
  });
 //-------------------------------------------- 
  $('#frmPOItemSummery #cboSubCategory').change(function(){
		var mainCategory = $('#frmPOItemSummery #cboMainCategory').val();
		var subCategory = $('#frmPOItemSummery #cboSubCategory').val();
		var url 		= basePath+"report-db-get.php?requestType=loadItems&mainCategory="+mainCategory+"&subCategory="+subCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmPOItemSummery #cboItem').html(httpobj.responseText);
		
  });
 //-------------------------------------------- 
  $('#frmSubCatPOGRNSummery #cboMainCategory').change(function(){
		var mainCategory = $('#frmSubCatPOGRNSummery #cboMainCategory').val();
		var url 		= basePath+"report-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmSubCatPOGRNSummery #cboSubCategory').html(httpobj.responseText);
		
  });
 //-------------------------------------------- 
  $('#frmSubCatGRNPOSummery #cboMainCategory').change(function(){
		var mainCategory = $('#frmSubCatGRNPOSummery #cboMainCategory').val();
		var url 		= basePath+"report-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmSubCatGRNPOSummery #cboSubCategory').html(httpobj.responseText);
		
  });
	
 ///////////////////////////////////////////////////////// 
 //-------------------------------------------- 
  $('#frmDepWiseIssues #cboMainCategory').change(function(){
		var mainCategory = $('#frmDepWiseIssues #cboMainCategory').val();
		var url 		= basePath+"report-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmDepWiseIssues #cboSubCategory').html(httpobj.responseText);
		
  });
 //-------------------------------------------- 
  $('#frmDepWiseIssues #cboSubCategory').change(function(){
		var mainCategory = $('#frmDepWiseIssues #cboMainCategory').val();
		var subCategory = $('#frmDepWiseIssues #cboSubCategory').val();
		var url 		= basePath+"report-db-get.php?requestType=loadItems&mainCategory="+mainCategory+"&subCategory="+subCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmDepWiseIssues #cboItem').html(httpobj.responseText);
		
  });
 //-------------------------------------------- 
 //-------------------------------------------- 
  $('#frmLocWiseGRN #cboMainCategory').change(function(){
		var mainCategory = $('#frmLocWiseGRN #cboMainCategory').val();
		var url 		= basePath+"report-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmLocWiseGRN #cboSubCategory').html(httpobj.responseText);
		
  });
 //-------------------------------------------- 
  $('#frmLocWiseGRN #cboSubCategory').change(function(){
		var mainCategory = $('#frmLocWiseGRN #cboMainCategory').val();
		var subCategory = $('#frmLocWiseGRN #cboSubCategory').val();
		var url 		= basePath+"report-db-get.php?requestType=loadItems&mainCategory="+mainCategory+"&subCategory="+subCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmLocWiseGRN #cboItem').html(httpobj.responseText);
		
  });
  ///...nuwan....sub...main...
  
   $('#frmStoMoveReport #cboMainCategory').change(function(){
		var mainCategory = $('#frmStoMoveReport #cboMainCategory').val();
		var url 		= basePath+"report-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmStoMoveReport #cboSubCategory').html(httpobj.responseText);
		
  });
 //-------------------------------------------- 
  $('#frmStoMoveReport #cboSubCategory').change(function(){
		var mainCategory = $('#frmStoMoveReport #cboMainCategory').val();
		var subCategory = $('#frmStoMoveReport #cboSubCategory').val();
		var url 		= basePath+"report-db-get.php?requestType=loadItems&mainCategory="+mainCategory+"&subCategory="+subCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmStoMoveReport #cboItem').html(httpobj.responseText);
		
  });////end nuwan
 //-------------------------------------------- 
  /////////////////////stock balance//////////////////
 //--------------------------------------------- 
$('#frmStockBalance #cboCompany').change(function(){
	var company = $('#frmStockBalance #cboCompany').val();
	var url 		= basePath+"../stockBalance/stockBalance-db-get.php?requestType=loadLocations&company="+company;
	var httpobj 	= $.ajax({url:url,async:false})
	$('#frmStockBalance #cboLocation').html(httpobj.responseText);
	
});
//--------------------------------------------------------
$('#frmStockBalance #cboCompany').change(function(){
	var company = $('#frmStockBalance #cboCompany').val();
	var url 		= basePath+"../stockBalance/stockBalance-db-get.php?requestType=loadCurrency&company="+company;
	var httpobj 	= $.ajax({url:url,async:false});
	var curr=httpobj.responseText.trim();
	$('#frmStockBalance #cboCurrency').val(curr);
	
});
//--------------------------------------------------------
  $('#frmStockBalance #cboOrderNo').change(function(){
		var orderNo = $('#frmStockBalance #cboOrderNo').val();
		var url 		= basePath+"../stockBalance/stockBalance-db-get.php?requestType=loadStyleNo&orderNo="+orderNo;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmStockBalance #cbStyleNo').html(httpobj.responseText);
  });
 //-------------------------------------------- 
  $('#frmStockBalance #cboMainCategory').change(function(){
		var mainCategory = $('#frmStockBalance #cboMainCategory').val();
		var url 		= basePath+"../stockBalance/stockBalance-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmStockBalance #cboSubCategory').html(httpobj.responseText);
  });
 //-------------------------------------------- 
  $('#frmStockBalance #cboSubCategory').change(function(){
		var mainCategory = $('#frmStockBalance #cboMainCategory').val();
		var subCategory = $('#frmStockBalance #cboSubCategory').val();
		var url 		= basePath+"../stockBalance/stockBalance-db-get.php?requestType=loadItems&mainCategory="+mainCategory+"&subCategory="+subCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmStockBalance #cboItems').html(httpobj.responseText);
  });
 //-------------------------------------------- 
  $('#frmStockBalance #chkStyle').click(function(){
		document.getElementById('rwOrder').style.display='';
		document.getElementById('rwStyle').style.display='';
  });
 //-------------------------------------------- 
  $('#frmStockBalance #chkGeneral').click(function(){
		document.getElementById('rwOrder').style.display='none';
		document.getElementById('rwStyle').style.display='none';
  });
 //-------------------------------------------- 
  $('#frmStockBalance #chkDate').click(function(){
	  var val = $(this).attr('checked');
	  if(!val)
	  {
		$('#frmStockBalance #dtDateFrom7').val('');  
		$('#frmStockBalance #dtDateFrom7').attr('disabled','true');
		$('#frmStockBalance #dtDateTo7').val('');  
		$('#frmStockBalance #dtDateTo7').attr('disabled','true');
	  }
	  else
	  {
		$('#frmStockBalance #dtDateFrom7').removeAttr('disabled');
		$('#frmStockBalance #dtDateTo7').removeAttr('disabled');
	  }
  });
 //-------------------------------------------- 
 $('#frmStockBalance .subcheck').live('click',function(){
	loadItems();	 
 });
  $('#frmStockBalance #imgSearchItems').click(function(){
	  
	if ($('#frmStockBalance').validationEngine('validate'))   
    { 
	//	document.frmStockBalance.submit();
	//alert($('#frmStockBalance #cboCompany').val());
	//alert($('#cboCompany').val());
	
	var dateFrom=$('#frmStockBalance #dtDateFrom7').val();
	var dateTo=$('#frmStockBalance #dtDateTo7').val();
	
	var string="";
	$('.subcheck:checked').each(function(){
		var subCatId=$(this).val();	
				string += subCatId  +',' ;
	});
	string = string.substr(0,string.length-1);
	
	var reportId	="954";
	
	var url='?q='+reportId;
		url+='&company='+$('#frmStockBalance #cboCompany').val();
		url+='&currency='+$('#frmStockBalance #cboCurrency').val();
		url+='&location='+$('#frmStockBalance #cboLocation').val();
		url+='&chkStyle='+$('input[name=radio]:checked').val();
		url+='&reptType='+$('input[name=radioReptType]:checked').val();
		url+='&itemType='+$('input[name=radioItemType]:checked').val();
		url+='&orderNo='+$('#frmStockBalance #cboOrderNo').val();
		url+='&styleNo='+$('#frmStockBalance #cbStyleNo').val();
		url+='&mainCat='+$('#frmStockBalance #cboMainCategory').val();
		url+='&subCat='+string;
		url+='&item='+$('#frmStockBalance #cboItems').val();
		url+='&dateTo='+dateTo;
		window.open(url);
	}
  });
 //---------------------------------------------
 //---------------------------------
  $('#frmStockBalance #chkAll2').click(function(){
		if(document.getElementById('chkAll2').checked==true)
		 var chk=true;
		else
		 var chk=false;
		
		var rowCount = document.getElementById('tblMainCategory').rows.length;
		for(var i=1;i<rowCount;i++)
		{
	    	document.getElementById('tblMainCategory').rows[i].cells[0].childNodes[0].checked=chk;
			
			if(chk==true){
				$('#div'+i).show();
				$('#div'+i+' input:checkbox').attr('checked','checked');
			}
			else{
				$('#div'+i).hide();
				$('#div'+i+' input:checkbox').removeAttr('checked');
			}
		}
		loadItems();
		
    });
	
	
  	$('#frmStockBalance .mainCateg').click(function(){
	 	var id = $(this).val();
		if($(this).attr('checked'))
		{
			$('#div'+id).show();
			$('#div'+id+' input:checkbox').attr('checked','checked');
		}
		else
		{
			$('#div'+$(this).val()).hide();
			$('#div'+id+' input:checkbox').removeAttr('checked');
		}
		loadItems();
});

 $('#frmStockBalance .subcheckAll').live('click',function(){
	 	var id = $(this).val();
		var cl ='#frmStockBalance .subcheckAll'+id;
		var c2 ='#frmStockBalance .subcheck'+id;
		
		var chk=$(cl).attr('checked');

		if(chk==true)
		{
			$(c2).each(function(){
			$(this).attr('checked','checked');
			});
		}
		else
		{
			$(c2).each(function(){
			$(this).removeAttr('checked');
			});
		}
	 
	// alert(chk);
		loadItems();	 
 });

 //-------------------------------------------- 
  /////////////////////stock balance BULK ALL//////////////////
 //--------------------------------------------- 
$('#frmStockBalanceBulkAll #cboCompany').change(function(){
	var company = $('#frmStockBalanceBulkAll #cboCompany').val();
	var url 		= basePath+"../stockBalance/stockBalance-db-get.php?requestType=loadLocations&company="+company;
	var httpobj 	= $.ajax({url:url,async:false})
	$('#frmStockBalanceBulkAll #cboLocation').html(httpobj.responseText);
	
});
//--------------------------------------------------------
$('#frmStockBalanceBulkAll #cboCompany').change(function(){
	var company = $('#frmStockBalanceBulkAll #cboCompany').val();
	var url 		= basePath+"../stockBalance/stockBalance-db-get.php?requestType=loadCurrency&company="+company;
	var httpobj 	= $.ajax({url:url,async:false});
	var curr=httpobj.responseText.trim();
	$('#frmStockBalanceBulkAll #cboCurrency').val(curr);
	
});
//--------------------------------------------------------
  $('#frmStockBalanceBulkAll #cboMainCategory').change(function(){
		var mainCategory = $('#frmStockBalanceBulkAll #cboMainCategory').val();
		var url 		= basePath+"../stockBalance/stockBalance-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmStockBalanceBulkAll #cboSubCategory').html(httpobj.responseText);
  });
 //-------------------------------------------- 
  $('#frmStockBalanceBulkAll #cboSubCategory').change(function(){
		var mainCategory = $('#frmStockBalanceBulkAll #cboMainCategory').val();
		var subCategory = $('#frmStockBalanceBulkAll #cboSubCategory').val();
		var url 		= basePath+"../stockBalance/stockBalance-db-get.php?requestType=loadItems&mainCategory="+mainCategory+"&subCategory="+subCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmStockBalanceBulkAll #cboItems').html(httpobj.responseText);
  });
 //-------------------------------------------- 
  $('#frmStockBalanceBulkAll #chkDate').click(function(){
	  var val = $(this).attr('checked');
	  if(!val)
	  {
		$('#frmStockBalanceBulkAll #dtDateFrom9').val('');  
		$('#frmStockBalanceBulkAll #dtDateFrom9').attr('disabled','true');
		$('#frmStockBalanceBulkAll #dtDateTo9').val('');  
		$('#frmStockBalanceBulkAll #dtDateTo9').attr('disabled','true');
	  }
	  else
	  {
		$('#frmStockBalanceBulkAll #dtDateFrom9').removeAttr('disabled');
		$('#frmStockBalanceBulkAll #dtDateTo9').removeAttr('disabled');
	  }
  });
 //-------------------------------------------- 
 $('#frmStockBalanceBulkAll .subcheck').live('click',function(){
	loadItems_sub_all();	 
 });
  $('#frmStockBalanceBulkAll #imgSearchItems').click(function(){
	  
	if ($('#frmStockBalanceBulkAll').validationEngine('validate'))   
    { 
	//	document.frmStockBalanceBulkAll.submit();
	//alert($('#frmStockBalanceBulkAll #cboCompany').val());
	//alert($('#cboCompany').val());
	
	var dateFrom=$('#frmStockBalanceBulkAll #dtDateFrom9').val();
	var dateTo=$('#frmStockBalanceBulkAll #dtDateTo9').val();
	if(dateFrom>dateTo){
		alert("Invalid Date Range");
		return false;
	}
	
	var string="";
	$('#frmStockBalanceBulkAll .subcheck:checked').each(function(){
		var subCatId=$(this).val();	
				string += subCatId  +',' ;
	});
	string = string.substr(0,string.length-1);
	
	var reportId	="1137";
	
	var url='?q='+reportId;
		url+='&company='+$('#frmStockBalanceBulkAll #cboCompany').val();
		url+='&currency='+$('#frmStockBalanceBulkAll #cboCurrency').val();
		url+='&location='+$('#frmStockBalanceBulkAll #cboLocation').val();
		url+='&mainCat='+$('#frmStockBalanceBulkAll #cboMainCategory').val();
		url+='&subCat='+string;
		url+='&item='+$('#frmStockBalanceBulkAll #cboItems').val();
		url+='&dateFrom='+dateFrom;
		url+='&dateTo='+dateTo;
		window.open(url);
	}
  });
 //---------------------------------------------
 //---------------------------------
  $('#frmStockBalanceBulkAll #chkAll2').click(function(){
		if(document.getElementById('chkAll2').checked==true)
		 var chk=true;
		else
		 var chk=false;
		
		var rowCount = document.getElementById('frmStockBalanceBulkAll tblMainCategory').rows.length;
		for(var i=1;i<rowCount;i++)
		{
	    	document.getElementById('frmStockBalanceBulkAll tblMainCategory').rows[i].cells[0].childNodes[0].checked=chk;
			
			if(chk==true){
				$('#frmStockBalanceBulkAll #div'+i).show();
				$('#frmStockBalanceBulkAll #div'+i+' input:checkbox').attr('checked','checked');
			}
			else{
				$('#frmStockBalanceBulkAll #div'+i).hide();
				$('#frmStockBalanceBulkAll #div'+i+' input:checkbox').removeAttr('checked');
			}
		}
		loadItems_sub_all();
		
    });
	
	
  	$('#frmStockBalanceBulkAll .mainCateg').click(function(){
	 	var id = $(this).val();
		if($(this).attr('checked'))
		{
			$('#frmStockBalanceBulkAll #divBulk'+id).show();
			$('#frmStockBalanceBulkAll #divBulk'+id+' input:checkbox').attr('checked','checked');
		}
		else
		{
			$('#frmStockBalanceBulkAll #divBulk'+$(this).val()).hide();
			$('#frmStockBalanceBulkAll #divBulk'+id+' input:checkbox').removeAttr('checked');
		}
		loadItems_sub_all();
});

 $('#frmStockBalanceBulkAll .subcheckAll').live('click',function(){
	 	var id = $(this).val();
		var cl ='#frmStockBalanceBulkAll .subcheckAll'+id;
		var c2 ='#frmStockBalanceBulkAll .subcheck'+id;
		
		var chk=$(cl).attr('checked');

		if(chk==true)
		{
			$(c2).each(function(){
			$(this).attr('checked','checked');
			});
		}
		else
		{
			$(c2).each(function(){
			$(this).removeAttr('checked');
			});
		}
	 
	// alert(chk);
		loadItems_sub_all();	 
 });

 //---------------------------------
 /////////////////////END OF STOCK BALANCE BULK ALL////////////////////////////
 
 /////////////////////STOCK BALANCE EXCELL/////////////////////
$('#frmCompanyStockBalance #cboCompany').change(function(){
	var company = $('#frmCompanyStockBalance #cboCompany').val();
	var url 		= basePath+"../reports/excellReports/companyStockBalance/companyStockBalance-db-get.php?requestType=loadLocations&company="+company;
	var httpobj 	= $.ajax({url:url,async:false})
	$('#frmCompanyStockBalance #cboLocation').html(httpobj.responseText);  
	
});
 //-------------------------------------------- 
  $('#frmCompanyStockBalance #chkDate').click(function(){
	  var val = $(this).attr('checked');
	  if(!val)
	  {
		$('#frmCompanyStockBalance #dtDateToS').val('');  
		$('#frmCompanyStockBalance #dtDateToS').attr('disabled','true');
	  }
	  else
	  {
		$('#frmCompanyStockBalance #dtDateToS').removeAttr('disabled');
	  }
  });
 //-------------------------------------------- 
 
 
 
		$("#frmCompanyStockBalance #butDownload").click(function(){
			if($("#frmCompanyStockBalance").validationEngine('validate')){
			//clearRows();
			downloadFile();
			}

		});
 ///////////////////////////////////
	
  ///////////////GENERAL STOCK MOVEMENT////////////////
	$('#frmGeneralStockMovement #cboCompany').change(function(){
	var company = $('#frmGeneralStockMovement #cboCompany').val();
	var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadLocations&company="+company;
	var httpobj 	= $.ajax({url:url,async:false})
	$('#frmGeneralStockMovement #cboLocation').html(httpobj.responseText);
	
	});
//---------------------------------------------------------
	$('#frmDepWiseIssues #cboCompany').change(function(){
	var company = $('#frmDepWiseIssues #cboCompany').val();
	var url 		= basePath+"report-db-get.php?requestType=loadLocations&company="+company;
	var httpobj 	= $.ajax({url:url,async:false})
	$('#frmDepWiseIssues #cboLocation').html(httpobj.responseText);
	
	});
//---------------------------------------------------------
	$('#frmLocWiseGRN #cboCompany').change(function(){
	var company = $('#frmLocWiseGRN #cboCompany').val();
	var url 		= basePath+"report-db-get.php?requestType=loadLocations&company="+company;
	var httpobj 	= $.ajax({url:url,async:false})
	$('#frmLocWiseGRN #cboLocation').html(httpobj.responseText);
	
	});
///nuwan-------------------------------------
	$('#frmStoMoveReport #cboCompany').change(function(){
	var company = $('#frmStoMoveReport #cboCompany').val();
	var url 		= basePath+"report-db-get.php?requestType=loadLocations&company="+company;
	var httpobj 	= $.ajax({url:url,async:false})
	$('#frmStoMoveReport #cboLocation').html(httpobj.responseText);
	
	});//end
//--------------------------------------------------------
/*$('#frmStockBalance #cboCompany').change(function(){
	var company = $('#frmStockBalance #cboCompany').val();
	var url 		= "stockMovement-db-get.php?requestType=loadCurrency&company="+company;
	var httpobj 	= $.ajax({url:url,async:false});
	var curr=httpobj.responseText.trim();
	document.getElementById('cboCurrency').value=curr;
	
});*/
//--------------------------------------------------------
  $('#frmGeneralStockMovement #cboOrderNo').change(function(){
		var orderNo = $('#frmGeneralStockMovement #cboOrderNo').val();
		var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadStyleNo&orderNo="+orderNo;
		var httpobj 	= $.ajax({url:url,async:false})
	   $('#frmGeneralStockMovement #cbStyleNo').html(httpobj.responseText);
		
  });
 //-------------------------------------------- 
  $('#frmGeneralStockMovement #cboMainCategory').change(function(){
		var mainCategory = $('#frmGeneralStockMovement #cboMainCategory').val();
		var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
	   $('#frmGeneralStockMovement #cboSubCategory').html(httpobj.responseText);
  });
 //-------------------------------------------- 
  $('#frmGeneralStockMovement #cboSubCategory').change(function(){
		var mainCategory = $('#frmGeneralStockMovement #cboMainCategory').val();
		var subCategory = $('#frmGeneralStockMovement #cboSubCategory').val();
		var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadItems&mainCategory="+mainCategory+"&subCategory="+subCategory;
		var httpobj 	= $.ajax({url:url,async:false})
	   $('#frmGeneralStockMovement #cboItems').html(httpobj.responseText);
  });
 //-------------------------------------------- 
  $('#frmGeneralStockMovement #chkStyle').click(function(){
		document.getElementById('rwOrder').style.display='';
		document.getElementById('rwStyle').style.display='';
  });
 //-------------------------------------------- 
  $('#frmGeneralStockMovement #chkGeneral').click(function(){
		document.getElementById('rwOrder').style.display='none';
		document.getElementById('rwStyle').style.display='none';
  });
 //-------------------------------------------- 
  $('#frmGeneralStockMovement #imgSearchItems').click(function(){
  
	if ($('#frmGeneralStockMovement').validationEngine('validate'))   
    { 
	//alert($('#chkNormal').attr('checked'));
	if($('#frmGeneralStockMovement #chkNormal').attr('checked'))
		var url='?q='+959;
	else
		var url='?q='+960;
		
		url+='&company='+	$('#frmGeneralStockMovement #cboCompany').val();
		url+='&location='+	$('#frmGeneralStockMovement #cboLocation').val();
		url+='&mainCat='+	$('#frmGeneralStockMovement #cboMainCategory').val();
		url+='&subCat='+	$('#frmGeneralStockMovement #cboSubCategory').val();
		url+='&item='+		$('#frmGeneralStockMovement #cboItems').val();
		url+='&fromDate='+	$('#frmGeneralStockMovement #dtDateFrom8').val();
		url+='&toDate='+	$('#frmGeneralStockMovement #dtDateTo8').val();
		window.open(url);
	}
  });
  
/*  $('#frmGeneralStockMovement #chkDate').click(function(){
	  var val = $(this).attr('checked');
	  if(!val)
	  {
		$('#frmGeneralStockMovement #txtFromDate').val('');  
		$('#frmGeneralStockMovement #txtFromDate').attr('disabled','true');
		$('#frmGeneralStockMovement #txtToDate').val('');  
		$('#frmGeneralStockMovement #txtToDate').attr('disabled','true');
	  }
	  else
	  {
		//$('#txtFromDate').val('');  
		$('#frmGeneralStockMovement #txtFromDate').removeAttr('disabled');
		//$('#txtToDate').val('');  
		$('#frmGeneralStockMovement #txtToDate').removeAttr('disabled');
	  }
  });
  
*/  $('#frmStockBalance #chkDate').click(function(){
	  var val = $(this).attr('checked');
	  if(!val)
	  {
		$('#frmStockBalance #dtDateFrom7').val('');  
		$('#frmStockBalance #dtDateFrom7').attr('disabled','true');
		$('#frmStockBalance #dtDateTo7').val('');  
		$('#frmStockBalance #dtDateTo7').attr('disabled','true');
	  }
	  else
	  {
		//$('#txtFromDate').val('');  
		$('#frmStockBalance #dtDateFrom7').removeAttr('disabled');
		//$('#txtToDate').val('');  
		$('#frmStockBalance #dtDateTo7').removeAttr('disabled');
	  }
  });
  
  $('#frmGeneralStockMovement #chkDate').click(function(){
	  var val = $(this).attr('checked');
	  if(!val)
	  {
		$('#frmGeneralStockMovement #dtDateFrom8').val('');  
		$('#frmGeneralStockMovement #dtDateFrom8').attr('disabled','true');
		$('#frmGeneralStockMovement #dtDateTo8').val('');  
		$('#frmGeneralStockMovement #dtDateTo8').attr('disabled','true');
	  }
	  else
	  {
		//$('#txtFromDate').val('');  
		$('#frmGeneralStockMovement #dtDateFrom8').removeAttr('disabled');
		//$('#txtToDate').val('');  
		$('#frmGeneralStockMovement #dtDateTo8').removeAttr('disabled');
	  }
  });

/////////////////ENDD OF GENERAL STOCK MOVEMENT//////
  $('#frmPOSummery #chkDate').click(function(){
	  var val = $(this).attr('checked');
	  if(!val)
	  {
		$('#frmPOSummery #dtFromDate').val('');  
		$('#frmPOSummery #dtFromDate').attr('disabled','true');
		$('#frmPOSummery #dtToDate').val('');  
		$('#frmPOSummery #dtToDate').attr('disabled','true');
	  }
	  else
	  {
		//$('#txtFromDate').val('');  
		$('#frmPOSummery #dtFromDate').removeAttr('disabled');
		//$('#txtToDate').val('');  
		$('#frmPOSummery #dtToDate').removeAttr('disabled');
	  }
  });
//-----------------------------------------------------------
  $('#frmPOGRNSummery #chkDate').click(function(){
	  var val = $(this).attr('checked');
	  if(!val)
	  {
		$('#frmPOGRNSummery #dtFromDate2').val('');  
		$('#frmPOGRNSummery #dtFromDate2').attr('disabled','true');
		$('#frmPOGRNSummery #dtToDate2').val('');  
		$('#frmPOGRNSummery #dtToDate2').attr('disabled','true');
	  }
	  else
	  {
		//$('#txtFromDate').val('');  
		$('#frmPOGRNSummery #dtFromDate2').removeAttr('disabled');
		//$('#txtToDate').val('');  
		$('#frmPOGRNSummery #dtToDate2').removeAttr('disabled');
	  }
  });
//-----------------------------------------------------------
  $('#frmPOItemSummery #chkDate').click(function(){
	  var val = $(this).attr('checked');
	  if(!val)
	  {
		$('#frmPOItemSummery #dtFromDate3').val('');  
		$('#frmPOItemSummery #dtFromDate3').attr('disabled','true');
		$('#frmPOItemSummery #dtToDate3').val('');  
		$('#frmPOItemSummery #dtToDate3').attr('disabled','true');
	  }
	  else
	  {
		//$('#txtFromDate').val('');  
		$('#frmPOItemSummery #dtFromDate3').removeAttr('disabled');
		//$('#txtToDate').val('');  
		$('#frmPOItemSummery #dtToDate3').removeAttr('disabled');
	  }
  });
//-----------------------------------------------------------
  $('#frmSubCatPOGRNSummery #chkDate').click(function(){
	  var val = $(this).attr('checked');
	  if(!val)
	  {
		$('#frmSubCatPOGRNSummery #dtFromDate4').val('');  
		$('#frmSubCatPOGRNSummery #dtFromDate4').attr('disabled','true');
		$('#frmSubCatPOGRNSummery #dtToDate4').val('');  
		$('#frmSubCatPOGRNSummery #dtToDate4').attr('disabled','true');
	  }
	  else
	  {
		//$('#txtFromDate').val('');  
		$('#frmSubCatPOGRNSummery #dtFromDate4').removeAttr('disabled');
		//$('#txtToDate').val('');  
		$('#frmSubCatPOGRNSummery #dtToDate4').removeAttr('disabled');
	  }
  });
//-----------------------------------------------------------
  $('#frmSubCatGRNPOSummery #chkDate').click(function(){
	  var val = $(this).attr('checked');
	  if(!val)
	  {
		$('#frmSubCatGRNPOSummery #dtFromDateGRNPO').val('');  
		$('#frmSubCatGRNPOSummery #dtFromDateGRNPO').attr('disabled','true');
		$('#frmSubCatGRNPOSummery #dtToDateGRNPO').val('');  
		$('#frmSubCatGRNPOSummery #dtToDateGRNPO').attr('disabled','true');
	  }
	  else
	  {
		//$('#txtFromDate').val('');  
		$('#frmSubCatGRNPOSummery #dtFromDateGRNPO').removeAttr('disabled');
		//$('#txtToDate').val('');  
		$('#frmSubCatGRNPOSummery #dtToDateGRNPO').removeAttr('disabled');
	  }
  });
//-----------------------------------------------------------
  $('#frmPOPendingSummery #chkDate').click(function(){
	  var val = $(this).attr('checked');
	  if(!val)
	  {
		$('#frmPOPendingSummery #dtFromDate5').val('');  
		$('#frmPOPendingSummery #dtFromDate5').attr('disabled','true');
		$('#frmPOPendingSummery #dtToDate5').val('');  
		$('#frmPOPendingSummery #dtToDate5').attr('disabled','true');
	  }
	  else
	  {
		//$('#txtFromDate').val('');  
		$('#frmPOPendingSummery #dtFromDate5').removeAttr('disabled');
		//$('#txtToDate').val('');  
		$('#frmPOPendingSummery #dtToDate5').removeAttr('disabled');
	  }
  });
//-----------------------------------------------------------
  $('#frmDepWiseIssues #chkDate').click(function(){
	  var val = $(this).attr('checked');
	  if(!val)
	  {
		$('#frmDepWiseIssues #dtFromDate6').val('');  
		$('#frmDepWiseIssues #dtFromDate6').attr('disabled','true');
		$('#frmDepWiseIssues #dtToDate6').val('');  
		$('#frmDepWiseIssues #dtToDate6').attr('disabled','true');
	  }
	  else
	  {
		//$('#txtFromDate').val('');  
		$('#frmDepWiseIssues #dtFromDate6').removeAttr('disabled');
		//$('#txtToDate').val('');  
		$('#frmDepWiseIssues #dtToDate6').removeAttr('disabled');
	  }
  });
//-----------------------------------------------------------
  $('#frmLocWiseGRN #chkDate').click(function(){
	  var val = $(this).attr('checked');
	  if(!val)
	  {
		$('#frmLocWiseGRN #dtFromDate11').val('');  
		$('#frmLocWiseGRN #dtFromDate11').attr('disabled','true');
		$('#frmLocWiseGRN #dtToDate11').val('');  
		$('#frmLocWiseGRN #dtToDate11').attr('disabled','true');
	  }
	  else
	  {
		//$('#txtFromDate').val('');  
		$('#frmLocWiseGRN #dtFromDate11').removeAttr('disabled');
		//$('#txtToDate').val('');  
		$('#frmLocWiseGRN #dtToDate11').removeAttr('disabled');
	  }
  });
  
 //---nuwan----
 
 $('#frmStoMoveReport #chkDate').click(function(){
	  var val = $(this).attr('checked');
	  if(!val)
	  {
		$('#frmStoMoveReport #dtFromStrDate11').val('');  
		$('#frmStoMoveReport #dtFromStrDate11').attr('disabled','true');
		$('#frmStoMoveReport #dtToStrDate11').val('');  
		$('#frmStoMoveReport #dtToStrDate11').attr('disabled','true');
	  }
	  else
	  {
		//$('#txtFromDate').val('');  
		$('#frmStoMoveReport #dtFromStrDate11').removeAttr('disabled');
		//$('#txtToDate').val('');  
		$('#frmStoMoveReport #dtToStrDate11').removeAttr('disabled');
	  }
  });   //---end nuwan
  
//-----------------------------------------------------------
  $('#frmLocationItemsTrans #chkDate').click(function(){
	  var val = $(this).attr('checked');
	  if(!val)
	  {
		$('#frmLocationItemsTrans #dtFromDate9').val('');  
		$('#frmLocationItemsTrans #dtFromDate9').attr('disabled','true');
		$('#frmLocationItemsTrans #dtToDate9').val('');  
		$('#frmLocationItemsTrans #dtToDate9').attr('disabled','true');
	  }
	  else
	  {
		//$('#txtFromDate').val('');  
		$('#frmLocationItemsTrans #dtFromDate9').removeAttr('disabled');
		//$('#txtToDate').val('');  
		$('#frmLocationItemsTrans #dtToDate9').removeAttr('disabled');
	  }
  });
//-----------------------------------------------------------
  $('#frmInternalOnloanTrnsAct #chkDate').click(function(){
	  var val = $(this).attr('checked');
	  if(!val)
	  {
		$('#frmInternalOnloanTrnsAct #dtFromDate10').val('');  
		$('#frmInternalOnloanTrnsAct #dtFromDate10').attr('disabled','true');
		$('#frmInternalOnloanTrnsAct #dtToDate10').val('');  
		$('#frmInternalOnloanTrnsAct #dtToDate10').attr('disabled','true');
	  }
	  else
	  {
		//$('#txtFromDate').val('');  
		$('#frmInternalOnloanTrnsAct #dtFromDate10').removeAttr('disabled');
		//$('#txtToDate').val('');  
		$('#frmInternalOnloanTrnsAct #dtToDate10').removeAttr('disabled');
	  }
  });
//-----------------------------------------------------------
////////////////STYLE STOCK MOVEMENT/////////////////
	$('#frmStyleStockMovement #imgSearchItems').click(function(){
				var path='?q='+965;

		window.open(path);
	});


////////////////END OF STYLE STOCK MOVEMENT/////////////////



 //-------------------------------------------- 
  $('#frmLocationItemsTrans #cboMainCategory').change(function(){
		var mainCategory = $('#frmLocationItemsTrans #cboMainCategory').val();
		var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
	   $('#frmLocationItemsTrans #cboSubCategory').html(httpobj.responseText);
  });
 //-------------------------------------------- 
  $('#frmLocationItemsTrans #cboSubCategory').change(function(){
		var mainCategory = $('#frmLocationItemsTrans #cboMainCategory').val();
		var subCategory = $('#frmLocationItemsTrans #cboSubCategory').val();
		var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadItems&mainCategory="+mainCategory+"&subCategory="+subCategory;
		var httpobj 	= $.ajax({url:url,async:false})
	   $('#frmLocationItemsTrans #cboItem').html(httpobj.responseText);
  });
 //-------------------------------------------- 
	$('#frmLocationItemsTrans #cboCompany').change(function(){
	var company = $('#frmLocationItemsTrans #cboCompany').val();
	var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadLocations&company="+company;
	var httpobj 	= $.ajax({url:url,async:false})
	$('#frmLocationItemsTrans #cboLocation').html(httpobj.responseText);
	
	});
 //-------------------------------------------- 
  $('#frmInternalOnloanTrnsAct #cboMainCategory').change(function(){
		var mainCategory = $('#frmInternalOnloanTrnsAct #cboMainCategory').val();
		var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
	   $('#frmInternalOnloanTrnsAct #cboSubCategory').html(httpobj.responseText);
  });
 //-------------------------------------------- 
  $('#frmInternalOnloanTrnsAct #cboSubCategory').change(function(){
		var mainCategory = $('#frmInternalOnloanTrnsAct #cboMainCategory').val();
		var subCategory = $('#frmInternalOnloanTrnsAct #cboSubCategory').val();
		var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadItems&mainCategory="+mainCategory+"&subCategory="+subCategory;
		var httpobj 	= $.ajax({url:url,async:false})
	   $('#frmInternalOnloanTrnsAct #cboItem').html(httpobj.responseText);
  });
 //-------------------------------------------- 
	$('#frmInternalOnloanTrnsAct #cboCompany').change(function(){
	var company = $('#frmInternalOnloanTrnsAct #cboCompany').val();
	var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadLocations&company="+company;
	var httpobj 	= $.ajax({url:url,async:false})
	$('#frmInternalOnloanTrnsAct #cboLocation').html(httpobj.responseText);
	
	});
 //-------------------------------------------- 
 //---------------INK STOCK------------------------------------------------------------------ 
	//-----
	$('#frmInkStock #cboCompany').change(function(){
	var company = $('#frmInkStock #cboCompany').val();
	var url 		= basePath+"report-db-get.php?requestType=loadLocations&company="+company;
	var httpobj 	= $.ajax({url:url,async:false})
	$('#frmInkStock #cboLocation').html(httpobj.responseText);
	});
	
	//-----
	$('#frmInkStock #cboLocation').change(function(){
	var company = $('#frmInkStock #cboCompany').val();
	var location = $('#frmInkStock #cboLocation').val();
	var url 		= basePath+"report-db-get.php?requestType=loadSubStores&company="+company+"&location="+location;
	var httpobj 	= $.ajax({url:url,async:false})
	$('#frmInkStock #cboSubStores').html(httpobj.responseText);
	});
	
	//-----
	$('#frmInkStock #cboSearch').click(function(){
	var orderYear 	= $('#frmInkStock #cboOrderYear').val();
	var orderNo 	= $('#frmInkStock #cboOrderNo').val();
	var url 		= basePath+"report-db-get.php?requestType=loadSalesOrders&orderYear="+orderYear+"&orderNo="+orderNo;
	var httpobj 	= $.ajax({url:url,async:false})
	$('#frmInkStock #cboSalesOrder').html(httpobj.responseText);
	});
	
	//-----
	$('#frmInkStock #butReport').click(function(){
	var company 	= $('#frmInkStock #cboCompany').val();
	var location 	= $('#frmInkStock #cboLocation').val();
	var subStores 	= $('#frmInkStock #cboSubStores').val();
	var orderYear 	= $('#frmInkStock #cboOrderYear').val();
	var orderNo 	= $('#frmInkStock #cboOrderNo').val();
	var salesOrder 	= $('#frmInkStock #cboSalesOrder').val();
	var rptType	 	= $('input[name="chkRptType"]:checked').val();
 
	if(rptType==1)
	var url 		= "?q=967&company="+company+"&location="+location+"&subStores="+subStores+"&orderYear="+orderYear+"&orderNo="+orderNo+"&salesOrder="+salesOrder+"&rptType="+rptType;
	if(rptType==2)
	var url 		= "?q=971&company="+company+"&location="+location+"&subStores="+subStores+"&orderYear="+orderYear+"&orderNo="+orderNo+"&salesOrder="+salesOrder+"&rptType="+rptType;
	if(rptType==3)
	var url 		= "?q=960&company="+company+"&location="+location+"&subStores="+subStores+"&orderYear="+orderYear+"&orderNo="+orderNo+"&salesOrder="+salesOrder+"&rptType="+rptType;
	
		window.open(url);
	});
 
 
	//-----
	$('#frmInkStock .clsChkRpt').click(function(){
		
		var rptType	 	= $('input[name="chkRptType"]:checked').val();
		if(rptType==3){
			$('#frmInkStock #cboOrderYear').val('');
			$('#frmInkStock #cboOrderNo').val('');
			$('#frmInkStock #cboSalesOrder').val('');
		}
 	});

 
 
 //---------------INK STOCK------------------------------------------------------------------ 
 
 
 //---------------STYLE ALLOVATED ITEM STOCK MOVEMENT------------------------------------------------------------------ 
	//-----
	$('#frmInkItemStockMovement #cboCompany').change(function(){
	var company = $('#frmInkItemStockMovement #cboCompany').val();
	var url 		= basePath+"report-db-get.php?requestType=loadLocations&company="+company;
	var httpobj 	= $.ajax({url:url,async:false})
	$('#frmInkItemStockMovement #cboLocation').html(httpobj.responseText);
	});
	
	//-----
	$('#frmInkItemStockMovement #cboLocation').change(function(){
	var company = $('#frmInkItemStockMovement #cboCompany').val();
	var location = $('#frmInkItemStockMovement #cboLocation').val();
	var url 		= basePath+"report-db-get.php?requestType=loadSubStores&company="+company+"&location="+location;
	var httpobj 	= $.ajax({url:url,async:false})
	$('#frmInkItemStockMovement #cboSubStores').html(httpobj.responseText);
	});
	
	//-----
	$('#frmInkItemStockMovement #cboSearch').click(function(){
	var orderYear 	= $('#frmInkItemStockMovement #cboOrderYear').val();
	var orderNo 	= $('#frmInkItemStockMovement #cboOrderNo').val();
	var url 		= basePath+"report-db-get.php?requestType=loadSalesOrders&orderYear="+orderYear+"&orderNo="+orderNo;
	var httpobj 	= $.ajax({url:url,async:false})
	$('#frmInkItemStockMovement #cboSalesOrder').html(httpobj.responseText);
	});
	
 //------- 
  $('#frmInkItemStockMovement #cboMainCategory').change(function(){
		var mainCategory = $('#frmInkItemStockMovement #cboMainCategory').val();
		var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
	   $('#frmInkItemStockMovement #cboSubCategory').html(httpobj.responseText);
  });
 //--------- 
  $('#frmInkItemStockMovement #cboSubCategory').change(function(){
		var mainCategory = $('#frmInkItemStockMovement #cboMainCategory').val();
		var subCategory = $('#frmInkItemStockMovement #cboSubCategory').val();
		var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadItems&mainCategory="+mainCategory+"&subCategory="+subCategory;
		var httpobj 	= $.ajax({url:url,async:false})
	   $('#frmInkItemStockMovement #cboItems').html(httpobj.responseText);
  });
	
	
	
	$('#frmInkItemStockMovement #butReport').click(function(){
	var company 	= $('#frmInkItemStockMovement #cboCompany').val();
	var location 	= $('#frmInkItemStockMovement #cboLocation').val();
	var orderYear 	= $('#frmInkItemStockMovement #cboOrderYear').val();
	var orderNo 	= $('#frmInkItemStockMovement #cboOrderNo').val();
	var salesOrder 	= $('#frmInkItemStockMovement #cboSalesOrder').val();
	var item 	= $('#frmInkItemStockMovement #cboItems').val();


	var url 		= "?q=975&company="+company+"&location="+location+"&orderYear="+orderYear+"&orderNo="+orderNo+"&salesOrder="+salesOrder+"&item="+item;
	
		window.open(url);
	});
 
 
	//-----
	$('#frmInkStock .clsChkRpt').click(function(){
		
		var rptType	 	= $('input[name="chkRptType"]:checked').val();
		if(rptType==3){
			$('#frmInkStock #cboOrderYear').val('');
			$('#frmInkStock #cboOrderNo').val('');
			$('#frmInkStock #cboSalesOrder').val('');
		}
 	});

 
 
 //---------------INK STOCK------------------------------------------------------------------ 
 //--------------------------------------------------------
	$('#frmGrnSummary #cboCompany').change(function(){
		var company = $('#frmGrnSummary #cboCompany').val();
		var url 		= basePath+"report-db-get.php?requestType=loadLocations&company="+company;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmGrnSummary #cboLocation').html(httpobj.responseText);
	});
	
	$('#frmGrnSummary #butReport').click(function(){
		var company 	= $('#frmGrnSummary #cboCompany').val();
		var location 	= $('#frmGrnSummary #cboLocation').val();
		var supplier 	= $('#frmGrnSummary #cboSupplier').val();
		
		var formLink			= "?q=883";	 

		var url 		= formLink+"&company="+company+"&location="+location+"&supplier="+supplier;
		window.open(url);
	});
	
	//Item Expire----------------------------------------
	$('#frmItemsExpiry #cboCompany').change(function(){
		var company = $('#frmItemsExpiry #cboCompany').val();
		var url 		= basePath+"report-db-get.php?requestType=loadLocations&company="+company;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmItemsExpiry #cboLocation').html(httpobj.responseText);
	});

  $('#frmItemsExpiry #cboMainCategory').change(function(){
		var mainCategory = $('#frmItemsExpiry #cboMainCategory').val();
		var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
	   $('#frmItemsExpiry #cboSubCategory').html(httpobj.responseText);
  });

  $('#frmItemsExpiry #cboSubCategory').change(function(){
		var mainCategory = $('#frmItemsExpiry #cboMainCategory').val();
		var subCategory = $('#frmItemsExpiry #cboSubCategory').val();
		var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadItems&mainCategory="+mainCategory+"&subCategory="+subCategory;
		var httpobj 	= $.ajax({url:url,async:false})
	   $('#frmItemsExpiry #cboItems').html(httpobj.responseText);
  });
  
/*	$('#frmItemsExpiry #butReport').click(function(){
	var company 		= $('#frmItemsExpiry #cboCompany').val();
	var location 		= $('#frmItemsExpiry #cboLocation').val();
	var mainCategory 	= $('#frmItemsExpiry #cboMainCategory').val();
	var subCategory 	= $('#frmItemsExpiry #cboSubCategory').val();
	var itemId 			= $('#frmItemsExpiry #cboItems').val();
	var year 			= $('#frmItemsExpiry #cboYear').val();
	var url 			= "rpt_item_aging.php?company="+company+"&location="+location+"&mainCategory="+mainCategory+"&subCategory="+subCategory+"&itemId="+itemId+"&year="+year;
	window.open(url);
  });*/
 	//-------------------------------------------------------
	//Item Expiry----------------------------------------
	$('#frmItemsExpiry #cboCompany').change(function(){
		var company = $('#frmItemsExpiry #cboCompany').val();
		var url 		= basePath+"report-db-get.php?requestType=loadLocations&company="+company;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmItemsExpiry #cboLocation').html(httpobj.responseText);
	});

  $('#frmItemsExpiry #cboMainCategory').change(function(){
		var mainCategory = $('#frmItemsExpiry #cboMainCategory').val();
		var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
	   $('#frmItemsExpiry #cboSubCategory').html(httpobj.responseText);
  });

  $('#frmItemsExpiry #cboSubCategory').change(function(){
		var mainCategory = $('#frmItemsExpiry #cboMainCategory').val();
		var subCategory = $('#frmItemsExpiry #cboSubCategory').val();
		var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadItems&mainCategory="+mainCategory+"&subCategory="+subCategory;
		var httpobj 	= $.ajax({url:url,async:false})
	   $('#frmItemsExpiry #cboItems').html(httpobj.responseText);
  });
  
	$('#frmItemsExpiry #butReport').click(function(){
	var company 		= $('#frmItemsExpiry #cboCompany').val();
	var location 		= $('#frmItemsExpiry #cboLocation').val();
	var mainCategory 	= $('#frmItemsExpiry #cboMainCategory').val();
	var subCategory 	= $('#frmItemsExpiry #cboSubCategory').val();
	var itemId 			= $('#frmItemsExpiry #cboItems').val();
	var year 			= $('#frmItemsExpiry #cboYear').val();
	
	var rptLink		= "?q=977";	 
	var url 			= rptLink+"&company="+company+"&location="+location+"&mainCategory="+mainCategory+"&subCategory="+subCategory+"&itemId="+itemId+"&year="+year;
	window.open(url);
  });
 	//-------------------------------------------------------
	//Item Aging----------------------------------------
	$('#frmItemsAging #cboCompany').change(function(){
		var company = $('#frmItemsAging #cboCompany').val();
		var url 		= basePath+"report-db-get.php?requestType=loadLocations&company="+company;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmItemsAging #cboLocation').html(httpobj.responseText);
	});

  $('#frmItemsAging #cboMainCategory').change(function(){
		var mainCategory = $('#frmItemsAging #cboMainCategory').val();
		var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
	   $('#frmItemsAging #cboSubCategory').html(httpobj.responseText);
  });

  $('#frmItemsAging #cboSubCategory').change(function(){
		var mainCategory = $('#frmItemsAging #cboMainCategory').val();
		var subCategory = $('#frmItemsAging #cboSubCategory').val();
		var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadItems&mainCategory="+mainCategory+"&subCategory="+subCategory;
		var httpobj 	= $.ajax({url:url,async:false})
	   $('#frmItemsAging #cboItems').html(httpobj.responseText);
  });
  
	$('#frmItemsAging #butReport').click(function(){
	var company 		= $('#frmItemsAging #cboCompany').val();
	var location 		= $('#frmItemsAging #cboLocation').val();
	var mainCategory 	= $('#frmItemsAging #cboMainCategory').val();
	var subCategory 	= $('#frmItemsAging #cboSubCategory').val();
	var itemId 			= $('#frmItemsAging #cboItems').val();
	var toDate 			= $('#frmItemsAging #dtDateToAging').val();
	
	var rptLink		= "?q=981";	 
	var url 			= rptLink+"&company="+company+"&location="+location+"&mainCategory="+mainCategory+"&subCategory="+subCategory+"&itemId="+itemId+"&toDate="+toDate;
	window.open(url);
  });
 	//-------------------------------------------------------

	//Monthly GP----------------------------------------
	$('#frmmonthlyGP #cboCompany').change(function(){
		var company = $('#frmmonthlyGP #cboCompany').val();
		var url 		= basePath+"report-db-get.php?requestType=loadLocations&company="+company;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmmonthlyGP #cboLocation').html(httpobj.responseText);
	});

  $('#frmmonthlyGP #cboMainCategory').change(function(){
		var mainCategory = $('#frmmonthlyGP #cboMainCategory').val();
		var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
	   $('#frmmonthlyGP #cboSubCategory').html(httpobj.responseText);
  });

  $('#frmmonthlyGP #cboSubCategory').change(function(){
		var mainCategory = $('#frmmonthlyGP #cboMainCategory').val();
		var subCategory = $('#frmmonthlyGP #cboSubCategory').val();
		var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadItems&mainCategory="+mainCategory+"&subCategory="+subCategory;
		var httpobj 	= $.ajax({url:url,async:false})
	   $('#frmmonthlyGP #cboItems').html(httpobj.responseText);
  });
  
  
  
	$('#frmmonthlyGP #butReport').click(function(){
	var company 		= $('#frmmonthlyGP #cboCompany').val();
	var location 		= $('#frmmonthlyGP #cboLocation').val();
	var mainCategory 	= $('#frmmonthlyGP #cboMainCategory').val();
	var subCategory 	= $('#frmmonthlyGP #cboSubCategory').val();
	var itemId 			= $('#frmmonthlyGP #cboItems').val();
	var fromDate 		= $('#frmmonthlyGP #dtGPDateFrom').val();
	var toDate 			= $('#frmmonthlyGP #dtGPDateTo').val();
	
	var rptLink			= "?q=985";	 
	var url 			= rptLink+"&company="+company+"&location="+location+"&mainCategory="+mainCategory+"&subCategory="+subCategory+"&itemId="+itemId+"&fromDate="+fromDate+"&toDate="+toDate;
	window.open(url);
  });
 	//-------------------------------------------------------
	$('#frmmonthlyGPTranferin #cboCompany').change(function(){
		var company = $('#frmmonthlyGPTranferin #cboCompany').val();
		var url 		= basePath+"report-db-get.php?requestType=loadLocations&company="+company;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmmonthlyGPTranferin #cboLocation').html(httpobj.responseText);
	});

	$('#frmmonthlyGPTranferin #cboMainCategory').change(function(){
		var mainCategory = $('#frmmonthlyGPTranferin #cboMainCategory').val();
		var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmmonthlyGPTranferin #cboSubCategory').html(httpobj.responseText);
	});

	$('#frmmonthlyGPTranferin #cboSubCategory').change(function(){
		var mainCategory = $('#frmmonthlyGPTranferin #cboMainCategory').val();
		var subCategory = $('#frmmonthlyGPTranferin #cboSubCategory').val();
		var url 		= basePath+"../stockMovement/stockMovement-db-get.php?requestType=loadItems&mainCategory="+mainCategory+"&subCategory="+subCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmmonthlyGPTranferin #cboItems').html(httpobj.responseText);
	});
	$('#frmmonthlyGPTranferin #butReport').click(function(){
		var company 		= $('#frmmonthlyGPTranferin #cboCompany').val();
		var location 		= $('#frmmonthlyGPTranferin #cboLocation').val();
		var mainCategory 	= $('#frmmonthlyGPTranferin #cboMainCategory').val();
		var subCategory 	= $('#frmmonthlyGPTranferin #cboSubCategory').val();
		var itemId 			= $('#frmmonthlyGPTranferin #cboItems').val();
		var fromDate 		= $('#frmmonthlyGPTranferin #dtGPTDateFrom').val();
		var toDate 			= $('#frmmonthlyGPTranferin #dtGPTDateTo').val();

		var rptLink			= "?q=1300";
		var url 			= rptLink+"&company="+company+"&location="+location+"&mainCategory="+mainCategory+"&subCategory="+subCategory+"&itemId="+itemId+"&fromDate="+fromDate+"&toDate="+toDate;
		window.open(url);
	});
	



});




function ViewReport(type,formId)
{
	
	var reportId	="927";
	if ($('#'+formId).validationEngine('validate'))  { 
	//var url = "report.php?q="+reportId+"&type="+type+"&"+$('#'+formId).serialize();
	var url = "presentation/warehouse/reports/report.php?type="+type+"&"+$('#'+formId).serialize();
	window.open(url)
	}
}


function ViewReport_pdf(type,formId)
{
	/*
 	if ($('#'+formId).validationEngine('validate'))  { 
	var url = basePath+"report.php?type="+type+"&"+$('#'+formId).serialize();
	window.open(url)
	}*/
	var reportId	="927";
	if ($('#'+formId).validationEngine('validate'))  { 
	var url = "presentation/warehouse/reports/report.php?type="+type+"&"+$('#'+formId).serialize();
	//var url = "report.php?q="+reportId+"&type="+type+"&"+$('#'+formId).serialize();
	window.open(url)
	}
}


function ViewReport1(formId)
{
	reportName = $('#'+formId+' #cboReportType').val();
	var url = reportName+"?"+$('#'+formId).serialize();
	window.open(url,reportName)
}

function ViewReport_excel(type,formId)
{
//	if ($('#'+formId).validationEngine('validate'))  { 
	var url = "presentation/warehouse/reports/report.php?type="+type+"&"+$('#'+formId).serialize()+"&report_type=excel";
	//var url = basePath+"report.php?type="+type+"&"+$('#'+formId).serialize()+"&report_type=excel";
	window.open(url)
//	}
}

function ViewReport_excel_new1(type,formId,format)//=================================================================================================================
{
//	if ($('#'+formId).validationEngine('validate'))  { 
	var url = "presentation/warehouse/reports/report.php?type="+type+"&report_type="+format+"&"+$('#'+formId).serialize()+"&report_type=excel";
	//var url = basePath+"report.php?type="+type+"&"+$('#'+formId).serialize()+"&report_type=excel";
	window.open(url)
//	}
}


function ViewReport_excel_new(type,formId)
{
	
	var reportId	="927";
	if ($('#'+formId).validationEngine('validate'))  { 
	//var url = "report.php?q="+reportId+"&type="+type+"&"+$('#'+formId).serialize()+"&report_type=excel";
	var url = "presentation/warehouse/reports/report.php?type="+type+"&"+$('#'+formId).serialize()+"&report_type=excel";
	window.open(url)
	}
}



function loadItems(){
/*	var noOfTbls = document.getElementById('txtNoOfTables').value;
	for(var i=1;i<=noOfTbls;i++)
	{
		var node='#tbl'+i+' .chkRow';
		$('#tbl'+i+' .chkRow').each(function(){
			
		}
	}*/
	var data = "requestType=loadItems";
	var string="";
	
	
	
	
	$('#frmStockBalance .subcheck:checked').each(function(){
		var subCatId=$(this).val();	
			//	arr2 += "{";
			//	arr2 += '"subCatId":"'+		subCatId  +'"' ;
				string += subCatId  +',' ;
			//	arr2 +=  '},';
	});

				string = string.substr(0,string.length-1);
				data+="&string="	+	string;
	var url 		= basePath+"../stockBalance/stockBalance-db-get.php?"+data;
	var httpobj 	= $.ajax({url:url,async:false});
	//alert(httpobj.responseText);
	$('#frmStockBalance #cboItems').html(httpobj.responseText);
}

function loadItems_sub_all(){
/*	var noOfTbls = document.getElementById('txtNoOfTables').value;
	for(var i=1;i<=noOfTbls;i++)
	{
		var node='#tbl'+i+' .chkRow';
		$('#tbl'+i+' .chkRow').each(function(){
			
		}
	}*/
	var data = "requestType=loadItems";
	var string="";
	
	
	
	
	$('#frmStockBalanceBulkAll .subcheck:checked').each(function(){
		var subCatId=$(this).val();	
			//	arr2 += "{";
			//	arr2 += '"subCatId":"'+		subCatId  +'"' ;
				string += subCatId  +',' ;
			//	arr2 +=  '},';
	});

				string = string.substr(0,string.length-1);
				data+="&string="	+	string;
	var url 		= basePath+"../stockBalance/stockBalance-db-get.php?"+data;
	var httpobj 	= $.ajax({url:url,async:false});
	//alert(httpobj.responseText);
	$('#frmStockBalanceBulkAll #cboItems').html(httpobj.responseText);
}

///////////////////////////////////	
function downloadFile(){
	
	var reportId	="957";
	
	//var path='report.php?q='+reportId;
	var path='presentation/warehouse/reports/excellReports/companyStockBalance/companyStockBalance-xlsx.php';
		var date=$('#frmCompanyStockBalance #dtDateToS').val();
		window.open(path+'?company='+$('#frmCompanyStockBalance #cboCompany').val()+'&date='+date+'&location='+$('#frmCompanyStockBalance #cboLocation').val()+'&currency='+$('#frmCompanyStockBalance #cboCurrency').val());	

}

////////////////////////////////////
function headShowHide(headId,bodyId){
	
		$('#tblPOSummeryBody').hide('slow');
		$('#tblPOGRNSummeryBody').hide('slow');		
		$('#tblPOItemSummeryBody').hide('slow');		
		$('#tblSubCatPOGRNSummeryBody').hide('slow');		
		$('#tblSubCatGRNPOSummeryBody').hide('slow');		
		$('#tblPOPendingSummeryBody').hide('slow');	
		$('#tblStockBalanceBody').hide('slow');
		$('#tblStkBalExcellBody').hide('slow');
		$('#tblGeneralStockMovementBody').hide('slow');	
		$('#tblStyleStockMovementBody').hide('slow');
		$('#tblDepWiseIssuesBody').hide('slow');
		$('#tblLocationItemsTransBody').hide('slow');
		$('#tblInternalOnloanTrnsActBody').hide('slow');
		$('#tblGrnSummaryBody').hide('slow');	
		$('#tblItemsExpiryBody').hide('slow');	
		$('#tblItemsAgingBody').hide('slow');
		$('#tblmonthlyGPBody').hide('slow');
		$('#tblmonthlyGPTranferinBody').hide('slow');
		$('#tblInkStockBody').hide('slow');	
		$('#tblInkItemStockMovementBody').hide('slow');	
		$('#tblLocWiseGRNBody').hide('slow');
		$('#tblStockBalanceBulkAllBody').hide('slow');
	
		$('#tblPOSummeryHead').show('slow');
		$('#tblPOGRNSummeryHead').show('slow');
		$('#tblPOItemSummeryHead').show('slow');
		$('#tblSubCatPOGRNSummeryHead').show('slow');
		$('#tblSubCatGRNPOSummeryHead').show('slow');
		$('#tblPOPendingSummeryHead').show('slow');
		$('#tblStockBalanceHead').show('slow');
		$('#tblStkBalExcellHead').show('slow');
		$('#tblGeneralStockMovementHead').show('slow');
		$('#tblStyleStockMovementHead').show('slow');
		$('#tblDepWiseIssuesHead').show('slow');
		$('#tblLocationItemsTransHead').show('slow');
		$('#tblInternalOnloanTrnsActHead').show('slow');

		$('#tblGrnSummaryHead').show('slow');
		$('#tblItemsExpiryHead').show('slow');
		$('#tblItemsAgingHead').show('slow');
		$('#tblmonthlyGPHead').show('slow');
		$('#tblInkStockHead').show('slow');	
		$('#tblInkItemStockMovementHead').show('slow');	
		$('#tblLocWiseGRNHead').show('slow');
		////nuwan
		$('#tblStockBalanceBulkAllHead').hide('slow');
	
	
		$('#'+headId).hide('slow');
		$('#'+bodyId).show('slow');
		
}
//------------------------------------
function bodyShowHide(headId,bodyId){
		$('#'+headId).show('slow');
		$('#'+bodyId).hide('slow');
}

//................................nuwan






////////////////////////////////////////
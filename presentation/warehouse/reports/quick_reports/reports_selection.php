<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];
//include  "{$backwardseperator}dataAccess/permisionCheck.inc";
include  "{$backwardseperator}dataAccess/Connector.php";
//include 	"../../../../class/cls_getData.php";

//BEGIN - CLASS OBJECTS	{
//$obj_getData	= new clsGetData;
//END - CLASS OBJECTS	}

$year			= date('Y');
$month			= $_SESSION["Month"];

$option='';
for($i=$year; $i>=2012 ; $i--){
		$option.="<option value=\"".$i."\">".$i."</option>";	
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Qpet | Quick Reports</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="reports.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">
</head>
<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<!--<form id="frmPayrollReport" name="frmPayrollReport" autocomplete="off">
--><div align="center">
		<div class="trans_layoutL" style="width:700px">
		  <div class="trans_text">Quick Reports</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
          
<!--BEGIN - Procument-->
		    <tr>
		      <td><table align="left" width="600" class="main_header_table" id="tblProcumentHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="../../../../images/report_go.png" class="mouseover" />&nbsp;<u>Procument</u></td>
	            </tr>
		        </table></td>
	        </tr>
		    <tr>
		      <td><form name="frmProcument" id="frmProcument" autocomplete="off">
              <table width="700" align="left" border="0" id="tblProcumentBody" class="main_table" cellspacing="0" style="display:none;">
		        <thead>
		          <tr>
		            <td colspan="4" style="text-align:left">Procument - Quick reports View.<img src="../../../../images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
		          <tr>
		            <td width="209" class="normalfnt">PRN Report</td>
		            <td width="119"><select name="year" style="width:80px" id="year" class="documentYear">
		              <?php echo $option; ?>
	                </select></td>
		            <td width="138"><input type="text" name="doNo" id="doNo"  style="width:120px" class="documentNo"/></td>
		            <td width="226"><img src="../../../../images/view.jpg" width="96" height="24" alt="view report" id="prn" class="view" /></td>
	              </tr>
		          <tr>
		            <td width="209" class="normalfnt">PO Report</td>
		            <td><select name="year" style="width:80px" id="year" class="documentYear">
		              <?php echo $option; ?>
	                </select></td>
		            <td><input type="text" name="doNo2" id="doNo2"  style="width:120px" class="documentNo"/></td>
		            <td><img src="../../../../images/view.jpg" width="96" height="24" alt="view report" id="po" class="view"/></td>
	              </tr>
                </tbody>
	          </table></form></td>
	        </tr>
<!--END - Procument--> 
<!--BEGIN - Warehouse-->
		    <tr>
		      <td><table align="left" width="600" class="main_header_table" id="tblWarehouseHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="../../../../images/report_go.png" class="mouseover" />&nbsp;<u>Warehouse</u></td>
	            </tr>
		        </table></td>
	        </tr>
		    <tr>
		      <td><form name="frmWarehouse" id="frmWarehouse" autocomplete="off">
              <table width="700" align="left" border="0" id="tblWarehouseBody" class="main_table" cellspacing="0" style="display:none;">
		        <thead>
		          <tr>
		            <td colspan="4" style="text-align:left">Warehouse - Quick reports View.<img src="../../../../images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
		          <tr>
		            <td width="209" class="normalfnt">GRN Report</td>
		            <td width="119"><select name="year" style="width:80px" id="year" class="documentYear">
		              <?php echo $option; ?>
	                </select></td>
		            <td width="138"><input type="text" name="doNo" id="doNo"  style="width:120px" class="documentNo"/></td>
		            <td width="226"><img src="../../../../images/view.jpg" width="96" height="24" alt="view report"  id="grn" class="view"/></td>
	              </tr>
		          <tr>
		            <td width="209" class="normalfnt">Suplier Return Note Report</td>
		            <td><select name="year" style="width:80px" id="year" class="documentYear">
		              <?php echo $option; ?>
	                </select></td>
		            <td><input type="text" name="doNo3" id="doNo3"  style="width:120px" class="documentNo"/></td>
		            <td><img src="../../../../images/view.jpg" width="96" height="24" alt="view report"  id="srn" class="view"/></td>
	              </tr>
		          <tr>
		            <td width="209" class="normalfnt">MRN Report</td>
		            <td><select name="year" style="width:80px" id="year" class="documentYear">
		              <?php echo $option; ?>
	                </select></td>
		            <td><input type="text" name="doNo3" id="doNo3"  style="width:120px" class="documentNo"/></td>
		            <td><img src="../../../../images/view.jpg" width="96" height="24" alt="view report"  id="mrn" class="view"/></td>
	              </tr>
		          <tr>
		            <td width="209" class="normalfnt">Material Issue Note Report</td>
		            <td><select name="year" style="width:80px" id="year" class="documentYear">
		              <?php echo $option; ?>
	                </select></td>
		            <td><input type="text" name="doNo3" id="doNo3"  style="width:120px" class="documentNo"/></td>
		            <td><img src="../../../../images/view.jpg" width="96" height="24" alt="view report"  id="min" class="view"/></td>
	              </tr>
		          <tr>
		            <td width="209" class="normalfnt">Return To Stores Report</td>
		            <td><select name="year" style="width:80px" id="year" class="documentYear">
		              <?php echo $option; ?>
	                </select></td>
		            <td><input type="text" name="doNo3" id="doNo3"  style="width:120px" class="documentNo"/></td>
		            <td><img src="../../../../images/view.jpg" width="96" height="24" alt="view report"  id="retSt" class="view"/></td>
	              </tr>
		          <tr>
		            <td width="209" class="normalfnt">Gate Pass Report</td>
		            <td><select name="year" style="width:80px" id="year" class="documentYear">
		              <?php echo $option; ?>
	                </select></td>
		            <td><input type="text" name="doNo3" id="doNo3"  style="width:120px" class="documentNo"/></td>
		            <td><img src="../../../../images/view.jpg" width="96" height="24" alt="view report"   id="gp" class="view"/></td>
	              </tr>
		          <tr>
		            <td width="209" class="normalfnt">Gate Pass Transfer IN Report</td>
		            <td><select name="year" style="width:80px" id="year" class="documentYear">
		              <?php echo $option; ?>
	                </select></td>
		            <td><input type="text" name="doNo3" id="doNo3"  style="width:120px" class="documentNo"/></td>
		            <td><img src="../../../../images/view.jpg" width="96" height="24" alt="view report"   id="gpTIN" class="view" /></td>
	              </tr>
		          <tr>
		            <td width="209" class="normalfnt">Inter Company Transfer Report</td>
		            <td><select name="year" style="width:80px" id="year" class="documentYear">
		              <?php echo $option; ?>
	                </select></td>
		            <td><input type="text" name="doNo3" id="doNo3"  style="width:120px" class="documentNo"/></td>
		            <td><img src="../../../../images/view.jpg" width="96" height="24" alt="view report"   id="interComp" class="view"/></td>
	              </tr>
		          <tr>
		            <td width="209" class="normalfnt">New Item Allocation Report</td>
		            <td><select name="year" style="width:80px" id="year" class="documentYear">
		              <?php echo $option; ?>
	                </select></td>
		            <td><input type="text" name="doNo3" id="doNo3"  style="width:120px" class="documentNo"/></td>
		            <td><img src="../../../../images/view.jpg" width="96" height="24" alt="view report"   id="newItmAlloc" class="view"/></td>
	              </tr>
                </tbody>
	          </table></form></td>
	        </tr>
<!--END - Warehouse--> 

	      </table>
    </div>
  </div>
<!--</form>
-->
</body>
</html>

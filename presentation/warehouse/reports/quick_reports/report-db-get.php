<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
if($requestType=='loadSubCategory')
	{
		$mainCategory  = $_REQUEST['mainCategory'];
		$sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strCode,
				mst_subcategory.strName
				FROM mst_subcategory
				WHERE
				mst_subcategory.intMainCategory =  '$mainCategory' 
				ORDER BY mst_subcategory.strName ASC";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
if($requestType=='loadItems')
	{
		$mainCategory  = $_REQUEST['mainCategory'];

		$subCategory  = $_REQUEST['subCategory'];
		$sql = "SELECT
				mst_item.intId,
				mst_item.strName
				FROM mst_item
				WHERE 
				1=1 ";
		if($subCategory!='')		
		$sql .= " AND mst_item.intSubCategory =  '$subCategory'  ";
		if($mainCategory!='')
			$sql .= " AND mst_item.intMainCategory =  '$mainCategory'  ";
		
		$sql .= " ORDER BY mst_item.strName ASC";
		
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadLocations')
	{
		$company  = $_REQUEST['company'];
		$sql = "SELECT
				mst_locations.intId,
				mst_locations.strName
				FROM mst_locations
				WHERE
				mst_locations.intCompanyId =  '$company'";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	/*
	 * Author: Hasitha  Charaka
	 * Date: 2017/09/28
	 * Load Multiple SubCat
	 */
	else if($requestType=='loadSubCategoryMultiple'){

		$mainCategory  = $_REQUEST['mainCategory'];

		if($mainCategory !='null') {
			$sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strCode,
				mst_subcategory.strName
				FROM mst_subcategory
				WHERE
				";
			$sql .= "mst_subcategory.intMainCategory IN  ($mainCategory) ";
		}elseif ($mainCategory == 'null'){
			$sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strCode,
				mst_subcategory.strName
				FROM mst_subcategory
				WHERE
				1=1 ";
		}

		$sql.="ORDER BY mst_subcategory.strName ASC";

		$result = $db->RunQuery($sql);

		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);

	}else if($requestType == 'loadItemsMultiple') {
		$mainCategory  = $_REQUEST['mainCategory'];
		$subCategory  = $_REQUEST['subCategory'];

		$sql = "SELECT
				mst_item.intId,
				mst_item.strName
				FROM mst_item
				WHERE 
				1=1 ";
	if($subCategory!='' && $subCategory != 'null') {
		$sql .= " AND mst_item.intSubCategory in ($subCategory)  ";
	}
	if($mainCategory !='' && $mainCategory !='null') {
		$sql .= " AND mst_item.intMainCategory in  ($mainCategory) ";
	}
	$sql .= " ORDER BY mst_item.strName ASC";

	$result = $db->RunQuery($sql);
	$html = "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
	}
	echo $html;
	echo json_encode($response);
}

if($requestType=='loadOrderNumberToCompany') {
	$company_r=$_GET['cboCompany_r'];
	$sql ="SELECT
trn_orderheader.intOrderNo AS OrderNo
FROM
trn_orderheader
INNER JOIN mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
WHERE mst_companies.intId ='$company_r' AND trn_orderheader.intStatus ='1'
ORDER BY
	trn_orderheader.intOrderNo ASC";

	$result = $db->RunQuery($sql);
	$html = "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$html .= "<option value=\"".$row['OrderNo']."\">".$row['OrderNo']."</option>";
	}
	echo $html;
	echo json_encode($response);

}
	?>

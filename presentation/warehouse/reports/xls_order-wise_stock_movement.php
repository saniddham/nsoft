<?php
//suvini

//include_once "../../../dataAccess/Connector.php";

//$result2 = $db->RunQuery($report_sql);
//ini_set("display_errors",0);

header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=report.xls" );
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");

$totQty		= 0;
$totAmount 	= 0;
$report_cat        = $_REQUEST['report_cat'];
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<th width="2%">Date</th>
<th width="10%">Order</th>
<th width="6%">Graphic No</th>
<th width="6%">Location</th>
<th width="8%">Main Category</th>
<th width="8%">Sub Category</th>
<th width="4%"> Code</th>
<th width="10%">Item</th>
<th width="10%">Sales Order</th>
<th width="8%">UOM</th>
<th width="3%" height="22">PRI</th>
<th width="3%" height="22">PRN</th>
<th width="3%">PURCHASED</th>
<th width="3%">MRN</th>
<th width="3%">ISSUED</th>
<th width="3%">RETURNED</th>
<th width="5%">Gp out (From HO)</th>
<th width="6%">Gp in (To HO)</th>
<th width="6%">Issued (To HO)</th>
<th width="3%">Issued From non-HO</th>
<th width="3%">Bal To Issue</th>
<th width="3%">HO Stock</th>
</tr>
<?php

if($report_cat ==1)

{
    
	$orderNo_temp 	='';
	$orderYear_temp ='';
	$item_temp 		='';
    $rowspanPRn 	= 0;
while($row = mysqli_fetch_array($result))
{
	
	$so_count	  = get_number_of_sos($row['ORDER_NO'],$row['ORDER_YEAR'],$row['ITEM']);
	$MRN_QTY      = $row['MRN_QTY'] + $row['MRN_QTY_OTHER_LOC']; //Total
    if($row['type'] == '2')
	 {
     $bal_to_issue = round((($MRN_QTY) - ($row['ISSUE_QTY'] + MAX($row['ISSUE_QTY_OTHER_LOC'], ($row['GP_QTY_OTHER_LOC'])))), 4);
     }
   else
     {
    $bal_to_issue = round((($row['REQUIRED']) - ($row['ISSUE_QTY'] + MAX($row['ISSUE_QTY_OTHER_LOC'], ($row['GP_QTY_OTHER_LOC'])))), 4);
     }

if ($bal_to_issue < 0)
$bal_to_issue = 0;
$ISSUE_QTY = $row['ISSUE_QTY'] + $row['ISSUE_QTY_OTHER_LOC']; // Total
$returnToStores = $row['RET_TO_STORE_QTY'];
?>
  <tr>
    <td><?php echo $row['date']; ?></td>
    <td><?php echo $row['ORDER_NO']; ?></td>
    <td><?php echo $row['graphic']; ?></td>
    <td><?php echo $row['location']; ?></td>
    <td><?php echo $row['mainCatName']; ?></td>
    <td><?php echo $row['subCatName']; ?></td>
    <td><?php  if($row['SUP_ITEM_CODE']!=null){ echo $row['SUP_ITEM_CODE'];} else {echo $row['strCode'];} ?></td>
    <td><?php echo $row['itemName']; ?></td>
    <td><?php echo $row['salesOrderStr']; ?></td>
    <td><?php echo $row['UOM']; ?></td>
    <td valign="top" title="PRI" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round($row['REQUIRED'], 4); ?>&nbsp;</td>
<?php if($orderNo_temp != $row['ORDER_NO'] || $orderYear_temp != $row['ORDER_YEAR'] || $item_temp != $row['ITEM']  ){  ?>
<td valign="top" title="PRN" class="normalfnt" <?php if($so_count > 0) { $rowspanPRn = $so_count; ?>     rowspan="<?php echo $so_count; ?>" <?php } ?> style="text-align: right; ">
&nbsp;<?php echo round($row['PRN_QTY'], 4); ?>&nbsp;</td>
<td valign="top" title="PURCHASED" class="normalfnt" <?php if($so_count > 0) { ?> rowspan="<?php echo $so_count; ?>"  <?php } ?> style="text-align: right; ">
&nbsp;<?php echo round($row['PO_QTY'], 4); ?>&nbsp;</td>

<?php } else {   ?>
<?php if($rowspanPRn < 0) { ?>
<td title="PRN"></td>
<?php } if($rowspanPRn < 0 ) { ?>
<td title="PURCHASED"></td>
<?php } ?>
<?php }  $rowspanPRn--; // substracting data from rowspanPrn  ?>
<td valign="top" title="MRN" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round($MRN_QTY, 4); ?>&nbsp;</td>
<td valign="top" title="ISSUED" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round($ISSUE_QTY, 4); ?>&nbsp;</td>
<td valign="top" title="RETURNED" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round($returnToStores, 4); ?>&nbsp;</td>
<td valign="top" title="Gp out (From HO)" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round($row['GP_QTY'], 4); ?>&nbsp;</td>
<td valign="top" title="Gp in (To HO)" class="normalfnt"
style="text-align: right"><?php echo round($row['GP_IN_QTY'], 4); ?></td>
<td valign="top" title="Issued (To HO)" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round($row['ISSUE_QTY'], 4); ?>&nbsp;</td>
<td valign="top" title="Issued From non-HO" class="normalfnt" style="text-align: right">
<span class="normalfnt" style="text-align: right">
<?php echo round($row['ISSUE_QTY_OTHER_LOC'], 4); ?></span>
</td>
<td valign="top" title="Bal To Issue" class="normalfnt" style="text-align: right">
&nbsp;<?php echo $bal_to_issue ?>&nbsp;</td>
<td valign="top" title="HO Stock" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(getHOStock($row['itemId']), 4); ?>&nbsp;</td>
    
</tr>
<?php 
$orderNo_temp 	=$row['ORDER_NO'];
$orderYear_temp =$row['ORDER_YEAR'];
$item_temp 		=$row['ITEM'];
?>

<?php
	
	
}
}
else if($report_cat ==2)
{
	$orderNo_temp 	='';
	$orderYear_temp ='';
	$item_temp 		='';
    $rowspanPRn 	= 0;
while ($row = mysqli_fetch_array($result))
{
	
$sql_currency = "SELECT 
mst_item.dblLastPrice, 
mst_item.intCurrency,
mst_financecurrency.strCode,
mst_financeexchangerate.dblBuying as rate
FROM mst_item
INNER JOIN mst_financecurrency 
on mst_item.intCurrency = mst_financecurrency.intId
INNER JOIN mst_financeexchangerate on  mst_item.intCurrency = mst_financeexchangerate.intCurrencyId
WHERE 
mst_item.intId ='".$row['itemId']."' 
AND mst_financeexchangerate.intCompanyId = '$companyId' 
AND mst_financeexchangerate.dtmDate = (select MAX(mst_financeexchangerate.dtmDate) 
FROM mst_financeexchangerate 
WHERE mst_financeexchangerate.intCompanyId = '$companyId') ";

//echo $sql_currency;
$result5 = $db->RunQuery($sql_currency);

while ($row_1 = mysqli_fetch_array($result5))
{
$unit_price     = $row_1['dblLastPrice'];
$currency_id    = $row_1['intCurrency'];                          
$exchange_rate  = $row_1['rate']; 

								
$so_count	     = get_number_of_sos($row['ORDER_NO'],$row['ORDER_YEAR'],$row['ITEM']);
$MRN_QTY         = $row['MRN_QTY'] + $row['MRN_QTY_OTHER_LOC']; //Total

if($row['type'] == '2')
{
$bal_to_issue    = round((($MRN_QTY) - ($row['ISSUE_QTY'] + MAX($row['ISSUE_QTY_OTHER_LOC'], ($row['GP_QTY_OTHER_LOC'])))), 4);
 }
 else
 {
$bal_to_issue    = round((($row['REQUIRED']) - ($row['ISSUE_QTY'] + MAX($row['ISSUE_QTY_OTHER_LOC'], ($row['GP_QTY_OTHER_LOC'])))), 4);
 }

if ($bal_to_issue < 0)
$bal_to_issue = 0;
$ISSUE_QTY 		= $row['ISSUE_QTY'] + $row['ISSUE_QTY_OTHER_LOC']; // Total
$returnToStores = $row['RET_TO_STORE_QTY']; //Get the return to head office qty not the all the return Qty
   ?>
<tr style="background-color:
<?php if($row['type'] == '2'){$color = '#FFE6E6'; echo $color; }else { $color = '#FFFFFF'; echo $color;} ?>">

<td title="Date" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['date'];?>&nbsp;</td>

<td title="Order" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['ORDER_NO'].'/'.$row['ORDER_YEAR']; ?>&nbsp;</td>

<td title="graphic" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['graphic']; ?>&nbsp;</td>

<td title="Location" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['location']; ?>&nbsp;</td>

<td title="Main Category" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['mainCatName']; ?>&nbsp;</td>

<td title="Sub Category" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['subCatName']; ?>&nbsp;</td>

<td title="Code" class="normalfnt" style="text-align: center">
&nbsp;<?php if($row['SUP_ITEM_CODE']!=null) {echo $row['SUP_ITEM_CODE'];} else { echo $row['strCode'];} ?>&nbsp;</td>

<td title="Item" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['itemName']; ?>&nbsp;</td>

<td title="Sales Order" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['salesOrderStr']; ?>&nbsp;</td>

<td title="UOM" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['UOM']; ?>&nbsp;</td>

<?php
if($currency_id!=1)
{          
                               
$sql_rate_usd = "SELECT 
mst_financeexchangerate.dblBuying
FROM
mst_financeexchangerate
WHERE 
mst_financeexchangerate.intCompanyId 	  = '$companyId' 
AND mst_financeexchangerate.intCurrencyId = '1'
AND
mst_financeexchangerate.dtmDate = (select MAX(mst_financeexchangerate.dtmDate)
FROM
mst_financeexchangerate
WHERE
mst_financeexchangerate.intCompanyId = '$companyId') ";

$result_rateUSD = $db->RunQuery($sql_rate_usd);
while($row_rateUSD=mysqli_fetch_array($result_rateUSD))
{
$rate_usd = $row_rateUSD['dblBuying'];

?>
<td valign="top" title="PRI" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round( ($row['REQUIRED']*(($unit_price)* $exchange_rate /$rate_usd)), 4); ?>&nbsp;</td>

<?php 
if($orderNo_temp != $row['ORDER_NO'] || $orderYear_temp != $row['ORDER_YEAR'] || $item_temp != $row['ITEM']  )
{  ?>
<td valign="top" title="PRN" class="normalfnt" <?php if($so_count > 0) { $rowspanPRn = $so_count; ?>     rowspan="<?php echo $so_count; ?>" <?php } ?> style="text-align: right; ">
&nbsp;<?php echo round( ( $row['PRN_QTY']*(($unit_price)* $exchange_rate /$rate_usd)), 4); ?>&nbsp;</td>

<td valign="top" title="PURCHASED" class="normalfnt" <?php if($so_count > 0) { ?> rowspan="<?php echo $so_count; ?>"  <?php } ?> style="text-align: right; ">
&nbsp;<?php echo round( ( $row['PO_QTY']*(($unit_price)* $exchange_rate /$rate_usd)), 4); ?>&nbsp;</td>

<?php } 
else {   ?>
<?php 
if($rowspanPRn < 0) { ?>
<td title="PRN"></td>
<?php } 
if($rowspanPRn < 0 ) { ?>
<td title="PURCHASED"></td>
<?php } ?>
<?php }  $rowspanPRn--; // substracting data from rowspanPrn  ?>

  <td valign="top" title="MRN" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round( ( $MRN_QTY*(($unit_price)* $exchange_rate/$rate_usd)), 4); ?>&nbsp;</td>
    
<td valign="top" title="ISSUED" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(( $ISSUE_QTY*(($unit_price)* $exchange_rate/$rate_usd)), 4); ?>&nbsp;</td>
    
 
<td valign="top" title="RETURNED" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($returnToStores*(($unit_price)* $exchange_rate/$rate_usd)), 4); ?>&nbsp;</td> 
    
   
<td valign="top" title="Gp out (From HO)" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($row['GP_QTY']*(($unit_price)* $exchange_rate/$rate_usd)), 4); ?>&nbsp;</td>
    
  
<td valign="top" title="Gp in (To HO)" class="normalfnt"
style="text-align: right"><?php echo round(($row['GP_IN_QTY']*(($unit_price)* $exchange_rate/$rate_usd)), 4); ?></td>
    
    
<td valign="top" title="Issued (To HO)" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($row['ISSUE_QTY']*(($unit_price)* $exchange_rate/$rate_usd)), 4); ?>&nbsp;</td>
    
   
<td valign="top" title="Issued From non-HO" class="normalfnt" style="text-align: right">
<span class="normalfnt" style="text-align: right">
<?php echo round(( $row['ISSUE_QTY_OTHER_LOC']*(($unit_price)* $exchange_rate/$rate_usd)), 4);?></span>
</td>

<td valign="top" title="Bal To Issue" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($bal_to_issue*(($unit_price)* $exchange_rate/$rate_usd)), 4); ?>&nbsp;</td>

   
<td valign="top" title="HO Stock" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round((getHOStock($row['itemId'])*(($unit_price)* $exchange_rate/$rate_usd)), 4); ?>&nbsp;</td>

<?php 
$orderNo_temp 	=$row['ORDER_NO'];
$orderYear_temp =$row['ORDER_YEAR'];
$item_temp 		=$row['ITEM'];
?>
<?php 

}
}//currencu id = another

elseif($currency_id==1)
{
//echo "hgg";										 
?>			
  
<td valign="top" title="PRI" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($row['REQUIRED']*$unit_price), 4); ?>&nbsp;</td>

<?php 
if($orderNo_temp != $row['ORDER_NO'] || $orderYear_temp != $row['ORDER_YEAR'] || $item_temp != $row['ITEM']  )
{  ?>
<td valign="top" title="PRN" class="normalfnt" <?php if($so_count > 0) { $rowspanPRn = $so_count; ?> rowspan="<?php echo $so_count; ?>" 
<?php 
} 
?> style="text-align: right; ">
&nbsp;<?php echo round(($row['PRN_QTY']*$unit_price), 4); ?>&nbsp;</td>

<td valign="top" title="PURCHASED" class="normalfnt" <?php if($so_count > 0) { ?> rowspan="<?php echo $so_count; ?>"  <?php } ?> style="text-align: right; ">
&nbsp;<?php echo round(($row['PO_QTY']*$unit_price), 4); ?>&nbsp;</td>

<?php } 
else {   ?>
<?php 
if($rowspanPRn < 0)
 { ?>
<td title="PRN"></td>
<?php 
} 
if($rowspanPRn < 0 )
 { ?>
<td title="PURCHASED"></td>
<?php } ?>
<?php }  $rowspanPRn--; // substracting data from rowspanPrn  ?>

<td valign="top" title="MRN" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($MRN_QTY*$unit_price),4); ?>&nbsp;</td>

<td valign="top" title="ISSUED" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($ISSUE_QTY*$unit_price),4); ?>&nbsp;</td>

<td valign="top" title="RETURNED" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($returnToStores*$unit_price),4); ?>&nbsp;</td>

<td valign="top" title="Gp out (From HO)" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($row['GP_QTY']*$unit_price),4); ?>&nbsp;</td>

<td valign="top" title="Gp in (To HO)" class="normalfnt"
style="text-align: right"><?php echo round(($row['GP_IN_QTY']*$unit_price),4); ?></td>

<td valign="top" title="Issued (To HO)" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($row['ISSUE_QTY']*$unit_price),4); ?>&nbsp;</td>

<td valign="top" title="Issued From non-HO" class="normalfnt" style="text-align: right">
<span class="normalfnt" style="text-align: right">
<?php echo round(($row['ISSUE_QTY_OTHER_LOC']*$unit_price),4); ?></span>
</td>
<td valign="top" title="Bal To Issue" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($bal_to_issue *$unit_price),4); ?>&nbsp;</td>

<td valign="top" title="HO Stock" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(((getHOStock($row['itemId'])*$unit_price)),4); ?>&nbsp;</td>
                                                    
<?php 
$orderNo_temp 	=$row['ORDER_NO'];
$orderYear_temp =$row['ORDER_YEAR'];
$item_temp 		=$row['ITEM'];

} //currency usd
}

?>
                                  				 
<?php 
// }//usd_currency
}//dblprice
}
?>	
</table>
<?php 

function getHOStock($item)
{

    global $db;

    $sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) as qty 
			FROM `ware_stocktransactions_bulk`
			WHERE
			ware_stocktransactions_bulk.intItemId = '$item' AND
			ware_stocktransactions_bulk.intLocationId = '2'";

    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['qty'];

}

function get_approved_non_grn_pos($item, $location)
{

    global $db;

    $sql = "SELECT
			sum(trn_podetails.dblQty-trn_podetails.dblGRNQty) as qty
			FROM
			trn_podetails
			INNER JOIN trn_poheader ON trn_podetails.intPONo = trn_poheader.intPONo AND trn_podetails.intPOYear = trn_poheader.intPOYear
			WHERE
			trn_poheader.intStatus = 1 AND
			trn_poheader.intDeliveryTo = '$location' AND
			trn_podetails.intItem = '$item'
			";

    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['qty'];


}



function get_number_of_sos($no,$year,$item){
    global $db;
    $sql="SELECT
			Count(trn_po_prn_details_sales_order.SALES_ORDER) AS so_count
			FROM `trn_po_prn_details_sales_order`
			WHERE
			trn_po_prn_details_sales_order.ORDER_NO = '$no' AND
			trn_po_prn_details_sales_order.ORDER_YEAR = '$year' AND
			trn_po_prn_details_sales_order.ITEM = '$item'
			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['so_count'];
	
}

?>
<?php
	session_start();
	
	//ini_set("display_errors",1);
	
	include "../../../libraries/pdf-php/class.ezpdf.php";
	include "../../../libraries/fpdf/fpdf.php";
	include "../../../dataAccess/Connector.php";
	$pdf = new Cezpdf('a4');
	
	$type = $_REQUEST['type'];
//	$type = 'POSummery';
/*
 * AUTHOR : HASITHA CHARAKA
 *
 */
if($type =='order_wise_stock_order_forcast') {

    if(isset($_GET['order_no'])){
        //$order_number = array();
        $order_number = $_GET['order_no'];
//       echo $order_number.'<br>';
    }
    if(($_GET['cboMainCategory'])){

        $main_category= $_GET['cboMainCategory'];
//        echo $main_category.'<br>';
    }
    if(isset($_GET['cboSubCategory'])){
        $sub_category = $_GET['cboSubCategory'];
//        echo $sub_category.'<br>';
    }
    if(isset($_GET['cboItem'])){

        $item = $_GET['cboItem'];
//        echo $item.'<br>';
    }else {
        $item = '';
    }

    if(isset($_GET['balance_mrn'])){
        $balance_mrn = $_GET['balance_mrn'];
//        echo  $balance_mrn .'<br>';
    }
    if(isset($_GET['balance_issues'])){
        $balance_issues = $_GET['balance_issues'];

//        echo $balance_issues.'<br>';
    }
    if(isset($_GET['report_so_wise'])){
        $so_wise =$_GET['report_so_wise'];
//        echo $so_wise.'<br>';
    }
    if(isset($_GET['pending_mrn'])== 1){
        $pending_mrn = $_GET['pending_mrn'];
    }else {
        $pending_mrn= '';
    }

//     echo $pending_mrn.'<br>';

    if(isset($_GET['cboCompany_r'])){
        $company= $_GET['cboCompany_r'];
        //$head_office_loc =get_ho_loc($company,$conn);
//        echo $company.'<br>';
    }

    $companyId         = $_SESSION["headCompanyId"];
    $locationId        = $_SESSION["CompanyID"];

    $head_office_loc   = get_ho_loc($companyId);
    $pdfLandscape      = new Cezpdf('a4','landscape');
    $sql = "SELECT 
TB_ALL .type,
TB_ALL .date as DATES_TODATE, 
TB_ALL .date as DATES_OTHER, 
TB_ALL .ORDER_COMPANY,
TB_ALL .location AS location,
TB_ALL .mainCatId,
TB_ALL .subCatId, 
TB_ALL .ITEM, 
TB_ALL .re_order_policy,
TB_ALL .mainCatName, 
TB_ALL .subCatName,
TB_ALL .strCode,
TB_ALL .itemName, 
TB_ALL .otherOrders AS TODATE_ORDERS,
TB_ALL .REQUIRED AS REQUIRED_TODATE,
TB_ALL .PRN_QTY AS PRN_QTY_TODATE,
TB_ALL .PO_QTY AS PO_QTY_TODATE, 
(
ifnull(TB_ALL.MRN_QTY,0) + ifnull(TB_ALL.MRN_QTY_OTHER_LOC,0)
) AS MRN_QTY_TODATE,

(
ifnull(TB_ALL.ISSUE_QTY,0) + ifnull(TB_ALL.ISSUE_QTY_OTHER_LOC,0) 
) AS ISSUE_QTY_TODATE,

(
ifnull(TB_ALL.RET_TO_STORE_QTY,0) 
+ ifnull(TB_ALL.RET_TO_STORE_QTY_OTHER_LOC,0) 
) AS RET_TO_STORE_QTY_TODATE,

TB_ALL.GP_QTY AS GP_FROM_HO_TODATE,
TB_ALL.GP_IN_QTY AS GP_IN_QTY_TO_HO_TODATE,
TB_ALL.UOM,
TB_ALL.otherOrders AS OTHER_ORDERS, 

(
ifnull(TB_ALL.ISSUE_QTY,0) 
) AS ISSUE_QTY_ALL_HO,

(
ifnull(TB_ALL.GP_QTY,0)  
) AS GP_FROM_HO_ALL,

(
ifnull(TB_ALL.ISSUE_QTY_OTHER_LOC,0)  
) AS ISSUE_QTY_ALL_NON_HO,

TB_ALL.PO_QTY AS PO_QTY_OTHER,
TB_ALL.ORDER_NO,
TB_ALL.ORDER_STATUS,
TB_ALL.ORDER_TYPE,
TB_ALL.CREATE_DATE,
TB_ALL.SO

from 
/*tb_all*/	
(select tb.type,
tb.ORDER_COMPANY, 
tb.location AS location, 
tb.date,
tb.ITEM, 
GROUP_CONCAT(DISTINCT tb.ORDER_NO,'/',tb.ORDER_YEAR) as otherOrders,
 
SUM(tb.REQUIRED) as REQUIRED,
tb.TRANSACTION_LOCATION, 
sum(tb.PO_QTY) as PO_QTY, 
sum(tb.PRN_QTY) as PRN_QTY, 
tb.mainCatId,
tb.subCatId,
tb.mainCatName,
tb.subCatName, 
tb.strCode, 
tb.itemName, 
tb.re_order_policy,
tb.UOM, 
SUM(tb.MRN_QTY) as MRN_QTY, 
SUM(tb.MRN_EXTRA_QTY) as MRN_EXTRA_QTY, 
SUM(tb.ISSUE_QTY) as ISSUE_QTY, 
SUM(tb.GP_QTY) as GP_QTY, 
SUM(tb.GP_IN_QTY) as GP_IN_QTY, 
SUM(tb.RET_TO_STORE_QTY) as RET_TO_STORE_QTY, 
SUM(tb.MRN_QTY_OTHER_LOC) as MRN_QTY_OTHER_LOC, 
SUM(tb.MRN_EXTRA_QTY_OTHER_LOC) as MRN_EXTRA_QTY_OTHER_LOC, 
SUM(tb.ISSUE_QTY_OTHER_LOC) as ISSUE_QTY_OTHER_LOC, 
SUM(tb.GP_QTY_OTHER_LOC) as GP_QTY_OTHER_LOC, 
SUM(tb.GP_IN_QTY_OTHER_LOC) as GP_IN_QTY_OTHER_LOC, 
SUM(tb.RET_TO_STORE_QTY_OTHER_LOC) as RET_TO_STORE_QTY_OTHER_LOC ,
tb.ORDER_NO,
tb.ORDER_STATUS,
tb.ORDER_TYPE,
tb.CREATE_DATE,
tb.SO 
/*tb*/	
from( 
SELECT 	TB3.type, 
TB3.ORDER_COMPANY, 
TB3.location AS location, 
TB3.date, 
TB3.ITEM, 
TB3.ORDER_NO, 
TB3.ORDER_YEAR, 
-- SUM(TB3.REQUIRED) AS REQUIRED,

(
SELECT
sum(
trn_po_prn_details.REQUIRED
)
FROM
trn_po_prn_details
WHERE
trn_po_prn_details.ORDER_NO = TB3.ORDER_NO
AND trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR
AND trn_po_prn_details.ITEM = TB3.ITEM
) AS REQUIRED,
(
SELECT
sum(
trn_po_prn_details.PURCHASED_QTY) 
FROM trn_po_prn_details 
WHERE trn_po_prn_details.ORDER_NO = TB3.ORDER_NO 
AND trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR 
AND trn_po_prn_details.ITEM = TB3.ITEM ) AS PO_QTY, 

(SELECT SUM(trn_po_prn_details.PRN_QTY)
FROM trn_po_prn_details 
WHERE trn_po_prn_details.ORDER_NO = TB3.ORDER_NO AND
 trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR AND 
trn_po_prn_details.ITEM = TB3.ITEM ) AS PRN_QTY, 

TB3.TRANSACTION_LOCATION, 
 
TB3.mainCatId,
TB3.subCatId,
TB3.mainCatName, 
TB3.subCatName, 
TB3.strCode, 
TB3.itemName, 
TB3.re_order_policy,
TB3.UOM, 
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY, 
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY, 
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY, 
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_QTY,0)) as GP_QTY, 
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY, 
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY, 
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY_OTHER_LOC, 
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY_OTHER_LOC, 
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY_OTHER_LOC, 
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_QTY,0)) as GP_QTY_OTHER_LOC, 
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY_OTHER_LOC, 
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY_OTHER_LOC,
TB3.ORDER_STATUS,
TB3.ORDER_TYPE,
TB3.CREATE_DATE,
TB3.SO
FROM 
/*tb3*/	(SELECT TB2.ORDER_COMPANY, 
TB2.ORDER_NO, 
TB2.ORDER_YEAR, 
TB2.location, 
TB2.ITEM,
TB2.SALES_ORDER, 
TB2.type, 
CONCAT(trn_orderdetails.strSalesOrderNo,'/',trn_orderdetails.strCombo,'/',trn_orderdetails.strPrintName) AS SALES_ORDER_String, 
'' AS date,
mst_maincategory.intId as mainCatId,
mst_subcategory.intId AS subCatId,  
mst_maincategory.strName AS mainCatName, 
mst_subcategory.strName AS subCatName, 
mst_item.strCode, 
mst_item.intId, 
mst_item.strName AS itemName, 
mst_item.intReorderPolicy AS re_order_policy,
mst_units.strName AS UOM, 
-- trn_po_prn_details_sales_order.REQUIRED, 
trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID AS TRANSACTION_LOCATION, 



SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN', 
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS MRN_QTY, 
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN_EXTRA', 
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS MRN_EXTRA_QTY, 
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'ISSUE', 
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS ISSUE_QTY, 
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS', 
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS GP_QTY, 
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS_IN',
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS GP_IN_QTY, 
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'RETURNED_TO_STORES', 
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS RET_TO_STORE_QTY,
TB2.ORDER_STATUS,
TB2.ORDER_TYPE,
TB2.CREATE_DATE,
(
trn_orderdetails.strSalesOrderNo
) AS SO
FROM 
/*TB2*/( SELECT * FROM 
/*tb1*/	( SELECT 
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO, 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR, 
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER, 
trn_po_prn_details_sales_order_stock_transactions.ITEM, 
mst_locations.intCompanyId AS ORDER_COMPANY, 
mst_locations.strName AS location, 
( SELECT GROUP_CONCAT(mst_locations.intCompanyId) 
FROM ware_fabricreceivedheader 
INNER JOIN mst_locations 
ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId 
WHERE 
ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO 
AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR 
)AS FABRIC_IN_COMPANIES, 
1 AS type,
trn_orderheader.intStatus AS ORDER_STATUS,
trn_orderheader.PO_TYPE AS ORDER_TYPE,
trn_orderheader.dtDate AS CREATE_DATE
FROM trn_po_prn_details_sales_order_stock_transactions 
INNER JOIN trn_po_prn_details_sales_order 
ON 
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO 
AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR 
AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER 
AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM 
INNER JOIN trn_orderheader 
ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND 
trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR 
AND trn_orderheader.intStatus = 1
INNER JOIN mst_locations 
ON mst_locations.intId = trn_orderheader.intLocationId 

GROUP BY trn_po_prn_details_sales_order_stock_transactions.ORDER_NO, 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR, 
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER, 
trn_po_prn_details_sales_order_stock_transactions.ITEM 
HAVING FIND_IN_SET('$company', FABRIC_IN_COMPANIES) OR ORDER_COMPANY = '$company' 

UNION ALL 

SELECT 
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO, 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR, 
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER, 
trn_po_prn_details_sales_order_stock_transactions.ITEM, 
mst_locations.intCompanyId AS ORDER_COMPANY, 
mst_locations.strName AS location, 
( SELECT GROUP_CONCAT( DISTINCT mst_locations.intCompanyId ) 
FROM ware_fabricreceivedheader 
INNER JOIN mst_locations 
ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId 
WHERE 
ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND 
ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR 
) AS FABRIC_IN_COMPANIES, 
2 AS type,
trn_orderheader.intStatus AS ORDER_STATUS,
trn_orderheader.PO_TYPE AS ORDER_TYPE,
trn_orderheader.dtDate AS CREATE_DATE
FROM trn_po_prn_details_sales_order_stock_transactions 

LEFT JOIN trn_po_prn_details_sales_order 
ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO AND 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR AND 
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER AND 
trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM 
INNER JOIN trn_orderheader ON 
trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND 
trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR 
AND trn_orderheader.intStatus = 1
INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId 

WHERE trn_po_prn_details_sales_order.ORDER_NO IS NULL 

GROUP BY 
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO, 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR, 
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER, 
trn_po_prn_details_sales_order_stock_transactions.ITEM 
HAVING ( FIND_IN_SET('$company', FABRIC_IN_COMPANIES) OR ORDER_COMPANY = '$company' ) 


UNION ALL 

SELECT 
trn_po_prn_details_sales_order.ORDER_NO, 
trn_po_prn_details_sales_order.ORDER_YEAR, 
trn_po_prn_details_sales_order.SALES_ORDER, 
trn_po_prn_details_sales_order.ITEM, 
mst_locations.intCompanyId AS ORDER_COMPANY, 
mst_locations.strName AS location, 
( SELECT GROUP_CONCAT( DISTINCT mst_locations.intCompanyId ) 
FROM ware_fabricreceivedheader 
INNER JOIN mst_locations 
ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId 
WHERE ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO 
AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR 
) AS FABRIC_IN_COMPANIES, 
1 AS type,
trn_orderheader.intStatus AS ORDER_STATUS,
trn_orderheader.PO_TYPE AS ORDER_TYPE,
trn_orderheader.dtDate AS CREATE_DATE
FROM trn_po_prn_details_sales_order_stock_transactions

RIGHT JOIN 
trn_po_prn_details_sales_order 
ON 
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO    = trn_po_prn_details_sales_order.ORDER_NO AND 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR  = trn_po_prn_details_sales_order.ORDER_YEAR AND 
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER AND 
trn_po_prn_details_sales_order_stock_transactions.ITEM        = trn_po_prn_details_sales_order.ITEM 
INNER JOIN trn_orderheader 
ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO AND 
trn_orderheader.intOrderYear  = trn_po_prn_details_sales_order.ORDER_YEAR 
AND trn_orderheader.intStatus = 1
INNER JOIN mst_locations 
ON mst_locations.intId = trn_orderheader.intLocationId 

WHERE trn_po_prn_details_sales_order_stock_transactions.ORDER_NO IS NULL 

GROUP BY 
trn_po_prn_details_sales_order.ORDER_NO, 
trn_po_prn_details_sales_order.ORDER_YEAR, 
trn_po_prn_details_sales_order.SALES_ORDER, 
trn_po_prn_details_sales_order.ITEM 

HAVING ( FIND_IN_SET('$company', FABRIC_IN_COMPANIES) OR ORDER_COMPANY = '$company' ) ) AS TB1 
GROUP BY 
TB1.ORDER_NO, 
TB1.ORDER_YEAR, 
TB1.SALES_ORDER,
TB1.ITEM ) AS TB2 


LEFT JOIN 
trn_po_prn_details_sales_order_stock_transactions 
ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = TB2.ORDER_NO AND 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = TB2.ORDER_YEAR AND 
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = TB2.SALES_ORDER AND 
trn_po_prn_details_sales_order_stock_transactions.ITEM = TB2.ITEM 
INNER JOIN trn_orderheader 
ON trn_orderheader.intOrderNo = TB2.ORDER_NO AND 
trn_orderheader.intOrderYear = TB2.ORDER_YEAR AND
trn_orderheader.intStatus = 1
LEFT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order.ORDER_NO = TB2.ORDER_NO
AND trn_po_prn_details_sales_order.ORDER_YEAR = TB2.ORDER_YEAR
AND trn_po_prn_details_sales_order.SALES_ORDER = TB2.SALES_ORDER
AND trn_po_prn_details_sales_order.ITEM = TB2.ITEM
INNER JOIN trn_orderdetails 
ON TB2.ORDER_NO     = trn_orderdetails.intOrderNo 
AND TB2.ORDER_YEAR  = trn_orderdetails.intOrderYear 
AND TB2.SALES_ORDER = trn_orderdetails.intSalesOrderId 
INNER JOIN mst_item 
ON TB2.ITEM = mst_item.intId 
INNER JOIN mst_units 
ON mst_item.intUOM = mst_units.intId 
INNER JOIN mst_maincategory 
ON mst_item.intMainCategory = mst_maincategory.intId 
INNER JOIN mst_subcategory 
ON mst_item.intSubCategory = mst_subcategory.intId 
GROUP BY 
TB2.ORDER_NO, 
TB2.ORDER_YEAR, 
TB2.SALES_ORDER, 
TB2.ITEM,
trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID
) AS TB3 ";
    if($so_wise == '1') {

        $sql .=" GROUP BY
TB3.ORDER_NO,
TB3.ORDER_YEAR,
TB3.ITEM,
TB3.SO ) 
as 
tb 
GROUP BY 
tb.ORDER_NO,
tb.ORDER_YEAR,
tb.ITEM,
tb.SO
 ) as TB_ALL 
WHERE
1 = 1 ";

    }else {
        $sql .= " GROUP BY
TB3.ORDER_NO,
TB3.ORDER_YEAR,
TB3.ITEM ) 
as 
tb 
GROUP BY 
tb.ORDER_NO,
tb.ORDER_YEAR,
tb.ITEM
 ) as TB_ALL 
WHERE
1 = 1 ";
    }

    // $sql .=  " AND TB_ALL .ORDER_COMPANY IN (2)";
    $sql .=  " AND TB_ALL .ORDER_COMPANY  = $company ";
    if($order_number !='null'){
        $sql .= "AND TB_ALL.ORDER_NO IN ($order_number)";
    }
    if($main_category !='null') {

        $sql .= " AND TB_ALL.mainCatId IN ($main_category)";

    }

    if($sub_category !='null') {
        $sql .= "AND TB_ALL.subCatId IN ($sub_category)";

    }

    if($item !='' && $item !='null') {

        $sql .= "AND TB_ALL.ITEM IN ($item)";

    }
    $sql .=   " ORDER BY TB_ALL.date ASC";

    $result = $db->RunQuery($sql);
    if($_GET['report_type']=='excel_order'){
        include "xls_order_wise_stock_forcast.php";
        return;
    }



}
else if($type=='POSummery')
{

        $currency = $_REQUEST['cboCurrency'];
        $dtFrom = $_REQUEST['dtFromDate'];
        $dtTo = $_REQUEST['dtToDate'];
        $date = date("Y-m-d");
        //$date = "2012-09-25";
        $dtStr='';

        if(($dtFrom!='') || ($dtTo!='')){
            $dtStr.=" - ";
        }

        if($dtFrom!=''){
            $dtStr.="Date From : ".$dtFrom;
        }
        if($dtTo!=''){
            $dtStr .="Date To : ".$dtTo;
        }

        $company = $_SESSION['headCompanyId'];

        $rate = loadCurrency($company,$date,$currency);
        $currencyName= loadCurrencyName($currency);
        if($currencyName!='')
        $currencyName= " (Currency : ".$currencyName."$dtStr)";


        $sql = "(SELECT 
                mst_supplier.strName as SUPPLIER, 
                round(Sum(if(mst_shipmentterms.intId = '1' ,trn_podetails.dblQty*trn_podetails.dblUnitPrice*mst_financeexchangerate.dblBuying,0))/$rate,2) AS `LOCAL`,
                round(Sum(if(mst_shipmentterms.intId = '2' ,trn_podetails.dblQty*mst_financeexchangerate.dblBuying,0))/$rate,2) AS IMPORT,
                round(Sum(if(mst_shipmentterms.intId = '3' ,trn_podetails.dblQty*trn_podetails.dblUnitPrice*mst_financeexchangerate.dblBuying,0))/$rate,2) AS `COMPANY TRANSFER`,
                round(Sum(if(mst_shipmentterms.intId = '4' ,trn_podetails.dblQty*trn_podetails.dblUnitPrice*mst_financeexchangerate.dblBuying,0))/$rate,2) AS `LOAN IN`,
                round(Sum(trn_podetails.dblQty*trn_podetails.dblUnitPrice*mst_financeexchangerate.dblBuying)/$rate ,2)   AS `TOTAL`   
                FROM
                trn_podetails
                Inner Join trn_poheader ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
                Inner Join mst_shipmentterms ON trn_poheader.intShipmentTerm = mst_shipmentterms.intId
Inner Join mst_financeexchangerate ON trn_poheader.intCurrency = mst_financeexchangerate.intCurrencyId AND trn_poheader.dtmPODate = mst_financeexchangerate.dtmDate
Inner Join mst_locations ON trn_poheader.intCompany = mst_locations.intId AND mst_financeexchangerate.intCompanyId = mst_locations.intCompanyId
                Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId 
                WHERE trn_poheader.intStatus=1 ";
            if($dtFrom!='')
            $sql .= " AND trn_poheader.dtmPODate>='$dtFrom' ";
            if($dtTo!='')
            $sql .= " AND trn_poheader.dtmPODate<='$dtTo' ";
            $sql .= " GROUP BY
                trn_poheader.intSupplier )
                UNION 
                (SELECT 
                'TOTAL', 
                round(Sum(if(mst_shipmentterms.intId = '1' ,trn_podetails.dblQty*trn_podetails.dblUnitPrice*mst_financeexchangerate.dblBuying,0))/$rate,2) AS `LOCAL`,
                round(Sum(if(mst_shipmentterms.intId = '2' ,trn_podetails.dblQty*trn_podetails.dblUnitPrice*mst_financeexchangerate.dblBuying,0))/$rate,2) AS IMPORT,
                round(Sum(if(mst_shipmentterms.intId = '3' ,trn_podetails.dblQty*trn_podetails.dblUnitPrice*mst_financeexchangerate.dblBuying,0))/$rate,2) AS `COMPANY TRANSFER`,
                round(Sum(if(mst_shipmentterms.intId = '4' ,trn_podetails.dblQty*trn_podetails.dblUnitPrice*mst_financeexchangerate.dblBuying,0))/$rate,2) AS `LOAN IN`,
                round(Sum(trn_podetails.dblQty*trn_podetails.dblUnitPrice*mst_financeexchangerate.dblBuying)/$rate ,2)   AS `TOTAL` 
                FROM
                trn_podetails
                Inner Join trn_poheader ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
                Inner Join mst_shipmentterms ON trn_poheader.intShipmentTerm = mst_shipmentterms.intId
Inner Join mst_financeexchangerate ON trn_poheader.intCurrency = mst_financeexchangerate.intCurrencyId AND trn_poheader.dtmPODate = mst_financeexchangerate.dtmDate
Inner Join mst_locations ON trn_poheader.intCompany = mst_locations.intId AND mst_financeexchangerate.intCompanyId = mst_locations.intCompanyId
Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
                WHERE trn_poheader.intStatus=1 ";
            if($dtFrom!='')
            $sql .= " AND trn_poheader.dtmPODate>='$dtFrom' ";
            if($dtTo!='')
            $sql .= " AND trn_poheader.dtmPODate<='$dtTo' ";
            $sql .= ")";

            //echo $sql;
        $result = $db->RunQuery($sql);
        while($data[] = mysqli_fetch_array($result, MYSQLI_ASSOC)) {}

        $pdf->ezImage('../../../images/logo.jpg',5,70,800,150);
        $pdf->selectFont('../../../libraries/pdf-php/fonts/Helvetica.afm');
        //$this->ezStartPageNumbers(580,10,10,'','',1);
        $pdf->ezStartPageNumbers(580,10,10,'','',1);
        //print_r($data);

        //$cols = array('Main'=>"Main",'Sub'=>'Sub','Code'=>'Code','Name'=>'Name');
        $currencyName= "SUPPLIER PO SUMMERY$currencyName";
        $pdf->ezText($currencyName,$size=0,$options=array(),$test=0);
        $pdf->ezText('',$size=0,$options=array(),$test=0);
        $pdf->ezTable($data,'','',
array('xPos'=>0,'xOrientation'=>'right','width'=>550,'fontSize' => 7
,'cols'=>array('IMPORT'=>array('justification'=>'right'),'LOAN IN'=>array('justification'=>'right'),'TOTAL'=>array('justification'=>'right'),'LOCAL'=>array('justification'=>'right'),'COMPANY TRANSFER'=>array('justification'=>'right'))));


        $pdf->ezStream();
    }
else if($type=='POGRNSummery')
    {
        $currency = $_REQUEST['cboCurrency'];
        $supplier = $_REQUEST['cboSupplier'];
        $dtFrom = $_REQUEST['dtFromDate2'];
        $dtTo = $_REQUEST['dtToDate2'];
        $date = date("Y-m-d");
        // $date = "2012-09-25";
        $dtStr='';

        if(($dtFrom!='') || ($dtTo!='')){
            $dtStr.=" - ";
        }

        if($dtFrom!=''){
            $dtStr.="Date From : ".$dtFrom;
        }
        if($dtTo!=''){
            $dtStr .=" Date To : ".$dtTo;
        }


        if($supplier!=''){
            $supp=loadSupplierName($supplier);
            $supStr .=" - Supplier : ".$supp;
        }

        $company = $_SESSION['headCompanyId'];

        $rate = loadCurrency($company,$date,$currency);
        $currencyName= loadCurrencyName($currency);
        if($currencyName!='')
        $currencyName= " (Currency : ".$currencyName.$supStr."  -  ".$dtStr.")";

        //$rate =1;


        $sql = "select 
                `SUPPLIER`,
                `Date`,
                `Invoice No`,
                `Invoice Date`,
                `PO No`,
                `PO Type`,
                `GRN No`,
                `Return No`,
                `Amount`  	
                 from (SELECT 
                mst_supplier.strName as SUPPLIEROrder,
                mst_supplier.strName as SUPPLIER,
                trn_poheader.dtmPODate as` Date`,
                ware_grnheader.strInvoiceNo as` Invoice No`,
                ware_grnheader.SUPPLIER_INVOICE_DATE AS ` Invoice Date`,
                concat(tb1.intPONo,'/',tb1.intPOYear) as `PO No`,
                mst_shipmentterms.strName as `PO Type`,
                concat(ware_grnheader.intGrnNo,'/',ware_grnheader.intGrnYear) as `GRN No`,
                '' as `Return No`,
                'a' as showOrderSupTot, 
                'a' as showOrderTot, 
                'a' as  rwOrder, 
                'grn' as type, 
                round(sum( tb2.dblGrnQty*tb2.dblGrnRate*ware_grnheader.dblExchangeRate/mst_financeexchangerate.dblBuying) ,2)  as `Amount` 
                FROM
                trn_poheader 
                Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId 
                Inner Join mst_shipmentterms ON trn_poheader.intShipmentTerm = mst_shipmentterms.intId
                Inner Join (select * from trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem ) as tb1 ON trn_poheader.intPONo = tb1.intPONo AND trn_poheader.intPOYear = tb1.intPOYear
                inner Join ware_grnheader ON tb1.intPONo = ware_grnheader.intPoNo AND tb1.intPOYear = ware_grnheader.intPoYear AND ware_grnheader.intStatus='1' 
                inner Join ware_grndetails as tb2 ON ware_grnheader.intGrnNo = tb2.intGrnNo AND ware_grnheader.intGrnYear = tb2.intGrnYear AND tb1.intItem = tb2.intItemId  
                inner join mst_financeexchangerate ON mst_financeexchangerate.dtmDate = ware_grnheader.SUPPLIER_INVOICE_DATE AND mst_financeexchangerate.intCurrencyId = $currency AND mst_financeexchangerate.intCompanyId = $company
                WHERE 
                trn_poheader.intStatus='1' ";
            if($dtFrom!='')
            $sql .= " AND trn_poheader.dtmPODate>='$dtFrom' ";
            if($dtTo!='')
            $sql .= " AND trn_poheader.dtmPODate<='$dtTo' ";
            if($supplier!='')
            $sql .= " AND trn_poheader.intSupplier='$supplier' ";
            $sql .= "  
                GROUP BY
                trn_poheader.intSupplier,
                tb1.intPONo,
                tb1.intPOYear,
                ware_grnheader.intGrnNo,
                ware_grnheader.intGrnYear 
                
                UNION 
                
                SELECT
                mst_supplier.strName as SUPPLIEROrder,
                mst_supplier.strName as SUPPLIER,
                ware_returntosupplierheader.datdate as` Date`,
                '' as` Invoice No`,
                '' AS `Invoice Date`,
                concat(trn_poheader.intPONo,'/',trn_poheader.intPOYear) as `PO No`,
                ''  as `PO Type`,
                concat(ware_grnheader.intGrnNo,'/',ware_grnheader.intGrnYear) as `GRN No`,
                concat(ware_returntosupplierheader.intReturnNo,'/',ware_returntosupplierheader.intReturnYear) as `Return No`,
                'a' as showOrderTot,
                'a' as showOrderSupTot, 
                'a' as  rwOrder, 
                'return' as type, 
                round(sum(ware_returntosupplierdetails.dblQty*tb2.dblGrnRate*ware_grnheader.dblExchangeRate*-1/$rate),2) as `Amount` 
                FROM
                trn_poheader 
                Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
                Inner Join (select * from trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem ) as tb1 ON trn_poheader.intPONo = tb1.intPONo AND trn_poheader.intPOYear = tb1.intPOYear
                Inner Join ware_grnheader ON tb1.intPONo = ware_grnheader.intPoNo AND tb1.intPOYear = ware_grnheader.intPoYear AND ware_grnheader.intStatus='1' 
                Inner Join ware_grndetails as tb2 ON ware_grnheader.intGrnNo = tb2.intGrnNo AND ware_grnheader.intGrnYear = tb2.intGrnYear AND tb1.intItem = tb2.intItemId 
                Inner Join ware_returntosupplierheader ON tb2.intGrnNo = ware_returntosupplierheader.intGrnNo AND tb2.intGrnYear = ware_returntosupplierheader.intGrnYear AND ware_returntosupplierheader.intStatus='1' 
                Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear AND tb2.intItemId = ware_returntosupplierdetails.intItemId 
                WHERE 
                trn_poheader.intStatus='1' AND 
                ware_returntosupplierheader.intStatus='1'  ";
            if($dtFrom!='')
            $sql .= " AND trn_poheader.dtmPODate>='$dtFrom' ";
            if($dtTo!='')
            $sql .= " AND trn_poheader.dtmPODate<='$dtTo' ";
            if($supplier!='')
            $sql .= " AND trn_poheader.intSupplier='$supplier' ";
            $sql .= " 
                GROUP BY
                trn_poheader.intSupplier,
                tb1.intPONo,
                tb1.intPOYear,
                ware_grnheader.intGrnNo,
                ware_grnheader.intGrnYear,
                ware_returntosupplierheader.intReturnNo,
                ware_returntosupplierheader.intReturnYear 
                
                UNION 
                
                SELECT
                mst_supplier.strName as SUPPLIEROrder,
                '' as SUPPLIER,
                '' as ` Date`,
                '' as ` Invoice No`,
                '' AS `Invoice Date`,
                '' as `PO No`,
                ''  as `PO Type`,
                ''  as `GRN No`,
                '' as `Return No`,
                'b' as showOrderTot,
                'x' as showOrderSupTot, 
                'b' as  rwOrder, 
                'supWiseTotal' as type, 
                round(IFNULL(Sum(ware_grndetails.dblGrnQty*ware_grndetails.dblGrnRate*ware_grnheader.dblExchangeRate/$rate),0)+ IFNULL((SELECT
                Sum(ware_returntosupplierdetails.dblQty*ware_grndetails.dblGrnRate*ware_grnheader.dblExchangeRate/$rate)
                FROM
                trn_poheader
                Inner Join (select * from trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem) AS trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear  AND trn_poheader.intStatus =  '1'"; if($supplier!=''){ $sql .=" AND trn_poheader.intSupplier='$supplier' "; }  $sql .= "
                Inner Join ware_grnheader ON trn_podetails.intPONo = ware_grnheader.intPoNo AND trn_podetails.intPOYear = ware_grnheader.intPoYear  AND ware_grnheader.intStatus =  '1' 
                Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND trn_podetails.intItem = ware_grndetails.intItemId
                Inner Join ware_returntosupplierheader ON ware_grndetails.intGrnNo = ware_returntosupplierheader.intGrnNo AND ware_grndetails.intGrnYear = ware_returntosupplierheader.intGrnYear  AND ware_returntosupplierheader.intStatus =  '1' 
                Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo 
                AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear AND ware_grndetails.intItemId = ware_returntosupplierdetails.intItemId 
                WHERE tb1.intSupplier=trn_poheader.intSupplier AND ware_grnheader.intStatus =  '1'";
            if($dtFrom!='')
            $sql .= " AND trn_poheader.dtmPODate>='$dtFrom' ";
            if($dtTo!='')
            $sql .= " AND trn_poheader.dtmPODate<='$dtTo' ";
            $sql .= "   ),0)*-1,2) as `Amount`
                FROM
                trn_poheader as tb1
                Inner Join (select * from trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem) AS trn_podetails ON tb1.intPONo = trn_podetails.intPONo AND tb1.intPOYear = trn_podetails.intPOYear
                Inner Join ware_grnheader ON trn_podetails.intPONo = ware_grnheader.intPoNo AND trn_podetails.intPOYear = ware_grnheader.intPoYear  AND ware_grnheader.intStatus =  '1' 
                Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND trn_podetails.intItem = ware_grndetails.intItemId 
                Inner Join mst_supplier ON tb1.intSupplier = mst_supplier.intId 
                WHERE
                ware_grnheader.intStatus =  '1' 
                 AND tb1.intStatus =  '1'  ";
            if($dtFrom!='')
            $sql .= " AND tb1.dtmPODate>='$dtFrom' ";
            if($dtTo!='')
            $sql .= " AND tb1.dtmPODate<='$dtTo' ";
            if($supplier!='')
            $sql .= " AND tb1.intSupplier='$supplier' ";
            $sql .= "  
                GROUP BY
                tb1.intSupplier
                
                UNION 
                
                SELECT
                'z' as SUPPLIEROrder,
                '' as SUPPLIER,
                '' as ` Date`,
                '' as ` Invoice No`,
                '' AS `Invoice Date`,
                '' as `PO No`,
                ''  as `PO Type`,
                ''  as `GRN No`,
                'Total' as `Return No`,
                'z' as showOrderSupTot, 
                'c' as showOrderTot, 
                'c' as  rwOrder, 
                'Total' as type, 
                round(IFNULL(Sum(ware_grndetails.dblGrnQty*ware_grndetails.dblGrnRate*ware_grnheader.dblExchangeRate/$rate),0)+ IFNULL((SELECT
                Sum(ware_returntosupplierdetails.dblQty*ware_grndetails.dblGrnRate*ware_grnheader.dblExchangeRate/$rate)
                FROM
                trn_poheader
                Inner Join (select * from trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem) AS trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear  AND trn_poheader.intStatus =  '1' "; if($supplier!=''){ $sql .=" AND trn_poheader.intSupplier='$supplier' "; }  $sql .= "
                Inner Join ware_grnheader ON trn_podetails.intPONo = ware_grnheader.intPoNo AND trn_podetails.intPOYear = ware_grnheader.intPoYear  AND ware_grnheader.intStatus =  '1' 
                Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND trn_podetails.intItem = ware_grndetails.intItemId
                Inner Join ware_returntosupplierheader ON ware_grndetails.intGrnNo = ware_returntosupplierheader.intGrnNo AND ware_grndetails.intGrnYear = ware_returntosupplierheader.intGrnYear  AND ware_returntosupplierheader.intStatus =  '1' 
                Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo 
                AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear AND ware_grndetails.intItemId = ware_returntosupplierdetails.intItemId  
where ware_grnheader.intStatus =  '1'    ";
            if($dtFrom!='')
            $sql .= " AND trn_poheader.dtmPODate>='$dtFrom' ";
            if($dtTo!='')
            $sql .= " AND trn_poheader.dtmPODate<='$dtTo' ";
            $sql .= "   ),0)*-1,2) as `Amount`
                FROM
                trn_poheader as tb1
                Inner Join (select * from trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem) AS trn_podetails ON tb1.intPONo = trn_podetails.intPONo AND tb1.intPOYear = trn_podetails.intPOYear  AND tb1.intStatus =  '1'
                Inner Join ware_grnheader ON trn_podetails.intPONo = ware_grnheader.intPoNo AND trn_podetails.intPOYear = ware_grnheader.intPoYear  AND ware_grnheader.intStatus =  '1' 
                Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND trn_podetails.intItem = ware_grndetails.intItemId 
                Inner Join mst_supplier ON tb1.intSupplier = mst_supplier.intId 
                WHERE
                ware_grnheader.intStatus =  '1' 
                 AND tb1.intStatus =  '1'  ";
            if($dtFrom!='')
            $sql .= " AND tb1.dtmPODate>='$dtFrom' ";
            if($dtTo!='')
            $sql .= " AND tb1.dtmPODate<='$dtTo' ";
            if($supplier!='')
            $sql .= " AND tb1.intSupplier='$supplier' ";
            $sql .= "  
                
                UNION 
                
                SELECT
                'y' as SUPPLIEROrder,
                '' as SUPPLIER,
                '' as ` Date`,
                '' as ` Invoice No`,
                '' AS  `Invoice Date`,
                '' as `PO No`,
                ''  as `PO Type`,
                ''  as `GRN No`,
                '' as `Return No`,
                'y' as showOrderSupTot, 
                'c' as showOrderTot, 
                'c' as  rwOrder, 
                'blankLineBeforeTotal' as type, 
                ''  as `Amount`  
                
                ) as tb1  
                order by  tb1.SUPPLIEROrder asc , 
                tb1.`rwOrder` asc,
                tb1.`PO No` asc,
                tb1.`GRN No` asc,
                tb1.`Return No` asc,
                tb1.`showOrderSupTot` asc,
                tb1.`showOrderTot` asc,
                 tb1.`Invoice date` DESC";

             // echo $sql;die;
        $result = $db->RunQuery($sql);

        if($_REQUEST['report_type']=='excel')
        {
            include "xls_po_grn_summary.php";
            return;
        }

        while($data[] = mysqli_fetch_array($result, MYSQLI_ASSOC)) {}

        $pdf->ezImage('../../../images/logo.jpg',5,70,800,150);
        $pdf->selectFont('../../../libraries/pdf-php/fonts/Helvetica.afm');
        //$this->ezStartPageNumbers(580,10,10,'','',1);
        $pdf->ezStartPageNumbers(580,10,10,'','',1);
        //print_r($data);

        //$cols = array('Main'=>"Main",'Sub'=>'Sub','Code'=>'Code','Name'=>'Name');
        $currencyName	="SUPPLIER PO - GRN SUMMERY$currencyName";
        $pdf->ezText($currencyName,$size=0,$options=array(),$test=0);
        $pdf->ezText('',$size=0,$options=array(),$test=0);

        $pdf->ezTable($data,'','',
array('xPos'=>0,'xOrientation'=>'right','width'=>550,'fontSize' => 7
,'cols'=>array('PO Qty'=>array('justification'=>'right'),'GRN Qty'=>array('justification'=>'right'),'Return Qty'=>array('justification'=>'right'),'Amount'=>array('justification'=>'right'))));


        $pdf->ezStream();
    }
else if($type=='POItemSubCategorySummery')
    {
        $currency = $_REQUEST['cboCurrency'];
        $dtFrom = $_REQUEST['dtFromDate3'];
        $dtTo = $_REQUEST['dtToDate3'];

        $mainCategory = $_REQUEST['cboMainCategory'];
        $subCategory = $_REQUEST['cboSubCategory'];
        $item = $_REQUEST['cboItem'];

        $date = date("Y-m-d");
        //$date = "2012-09-25";
        $dtStr='';

        if(($dtFrom!='') && ($dtTo!='')){
            $dtStr.=" - ";
        }

        if($dtFrom!=''){
            $dtStr.="Date From : ".$dtFrom;
        }
        if($dtTo!=''){
            $dtStr .="Date To : ".$dtTo;
        }

        $company = $_SESSION['headCompanyId'];

        $rate = loadCurrency($company,$date,$currency);
        $currencyName= loadCurrencyName($currency);
        if($currencyName!='')
        $currencyName= "SUPPLIER ITEM SUB CATEGORY SUMMERY (Currency : ".$currencyName."$dtStr)";
        //$currencyName	= wordwrap($currencyName,15,"<br>\n",TRUE);

        $sql = "SELECT
                mst_subcategory.strName AS `Sub Category`, ";
        if($item!=''){
        $sql .= "mst_item.strName AS `Item`, ";
        }
        $sql .= "round(Sum(ware_stocktransactions_bulk.dblQty*ware_stocktransactions_bulk.dblGRNRate*mst_financeexchangerate.dblBuying/$rate),2) AS `Value`
                FROM
                ware_stocktransactions_bulk
                Inner Join mst_item ON ware_stocktransactions_bulk.intItemId = mst_item.intId
                Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
                Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
                left Join mst_financeexchangerate ON ware_stocktransactions_bulk.intCurrencyId = mst_financeexchangerate.intCurrencyId AND ware_stocktransactions_bulk.dtGRNDate = mst_financeexchangerate.dtmDate AND ware_stocktransactions_bulk.intCompanyId = mst_financeexchangerate.intCompanyId 
                where 1=1   ";
        if($dtFrom!='')
        $sql .= " AND ware_stocktransactions_bulk.dtGRNDate>='$dtFrom' ";
        if($dtTo!='')
        $sql .= " AND ware_stocktransactions_bulk.dtGRNDate<='$dtTo' ";
        if($mainCategory!='')
        $sql .= "AND mst_item.intMainCategory='$mainCategory' ";
        if($subCategory!='')
        $sql .= "AND mst_item.intSubCategory='$subCategory'  ";
        if($item!='')
        $sql .= "AND mst_item.intId='$item'  ";
        $sql .= "GROUP BY
                mst_item.intSubCategory";
        if($item!='')
        $sql .= " ,mst_item.intId ";

            //echo $sql;
        $result = $db->RunQuery($sql);
        while($data[] = mysqli_fetch_array($result, MYSQLI_ASSOC)) {}

        $pdf->ezImage('../../../images/logo.jpg',5,70,800,150);
        $pdf->selectFont('../../../libraries/pdf-php/fonts/Helvetica.afm');
        //$this->ezStartPageNumbers(580,10,10,'','',1);
        $pdf->ezStartPageNumbers(580,10,10,'','',1);
        //print_r($data);

        //$cols = array('Main'=>"Main",'Sub'=>'Sub','Code'=>'Code','Name'=>'Name');

        //$pdf->Cell(0,10,$currencyName,0,1);
        $pdf->ezText($currencyName,$size=0,$options=array(),$test=0);
        $pdf->ezText('',$size=0,$options=array(),$test=0);
        $pdf->ezTable($data,'','',
array('xPos'=>0, 'xOrientation'=>'right','width'=>550,'fontSize' => 7
,'cols'=>array('Sub Category'=>array('justification'=>'center'),'Item'=>array('justification'=>'center'),'Value'=>array('justification'=>'right'))));


        $pdf->ezStream();
    }
else if($type=='SubCatWisePOGRNSummery')
    {
        $currency = $_REQUEST['cboCurrency'];
        $mainCategory = $_REQUEST['cboMainCategory'];
        $subCategory = $_REQUEST['cboSubCategory'];
        $dtFrom = $_REQUEST['dtFromDate4'];
        $dtTo = $_REQUEST['dtToDate4'];
        $date = date("Y-m-d");
        // $date = "2012-09-25";
        $dtStr='';

        if(($dtFrom!='') || ($dtTo!='')){
            $dtStr.=" - ";
        }

        if($dtFrom!=''){
            $dtStr.="Date From : ".$dtFrom;
        }
        if($dtTo!=''){
            $dtStr .="  Date To : ".$dtTo;
        }


        if($supplier!=''){
            $supp=loadSupplierName($supplier);
            $supStr .=" - ".$supp;
        }

        $company = $_SESSION['headCompanyId'];

        $rate = loadCurrency($company,$date,$currency);
        $currencyName= loadCurrencyName($currency);
        if($currencyName!='')
        $currencyName= " (Currency : ".$currencyName.$supStr.$dtStr.")";

        //$rate =1;


        $sql = "select  
                `Sub Category`,
                `Date`,
                `SUPPLIER`,
                `Invoice No`,
                `Invoice Date`,
                `PO No`,
                `PO Type`,
                `GRN No`,
                `Return No`,`Amount` ";
        if($_REQUEST['report_type']=='excel')
        {
            $sql .=",SubCatOrder ";
        }
        $sql .=" from (SELECT 
                mst_subcategory.strName as SubCatOrder,
                mst_supplier.strName as SUPPLIER,
                trn_poheader.dtmPODate as` Date`,
                ware_grnheader.strInvoiceNo as` Invoice No`,
                ware_grnheader.SUPPLIER_INVOICE_DATE AS ` Invoice Date`,
                concat(tb1.intPONo,'/',tb1.intPOYear) as `PO No`,
                mst_shipmentterms.strName as `PO Type`,
                concat(ware_grnheader.intGrnNo,'/',ware_grnheader.intGrnYear) as `GRN No`,
                '' as `Return No`,
                mst_subcategory.strName as `Sub Category`, 
                'a' as showOrderSupTot, 
                'a' as showOrderTot, 
                'a' as  rwOrder, 
                'grn' as type, 
                round(sum( tb2.dblGrnQty*tb2.dblGrnRate*ware_grnheader.dblExchangeRate/$rate) ,2)  as `Amount` 
                FROM
                trn_poheader 
                Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId 
                Inner Join mst_shipmentterms ON trn_poheader.intShipmentTerm = mst_shipmentterms.intId
                Inner Join (select * from trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem) as tb1 ON trn_poheader.intPONo = tb1.intPONo AND trn_poheader.intPOYear = tb1.intPOYear
                inner Join ware_grnheader ON tb1.intPONo = ware_grnheader.intPoNo AND tb1.intPOYear = ware_grnheader.intPoYear AND ware_grnheader.intStatus='1' 
                inner Join ware_grndetails as tb2 ON ware_grnheader.intGrnNo = tb2.intGrnNo AND ware_grnheader.intGrnYear = tb2.intGrnYear AND tb1.intItem = tb2.intItemId   
                Inner Join mst_item ON tb2.intItemId = mst_item.intId
                Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
                WHERE 
                trn_poheader.intStatus='1' ";
            if($dtFrom!='')
            $sql .= " AND trn_poheader.dtmPODate>='$dtFrom' ";
            if($dtTo!='')
            $sql .= " AND trn_poheader.dtmPODate<='$dtTo' ";
            if($mainCategory!='')
            $sql .= "AND mst_item.intMainCategory='$mainCategory' ";
            if($subCategory!='')
            $sql .= "AND mst_item.intSubCategory='$subCategory'  ";
            $sql .= "  
                GROUP BY 
                mst_item.intSubCategory,
                trn_poheader.intSupplier,
                tb1.intPONo,
                tb1.intPOYear,
                ware_grnheader.intGrnNo,
                ware_grnheader.intGrnYear 
                
                UNION 
                
                SELECT
                mst_subcategory.strName as SubCatOrder,
                mst_supplier.strName as SUPPLIER,
                ware_returntosupplierheader.datdate as` Date`,
                '' as` Invoice No`,
                '' as` Invoice Date`,
                concat(trn_poheader.intPONo,'/',trn_poheader.intPOYear) as `PO No`,
                ''  as `PO Type`,
                concat(ware_grnheader.intGrnNo,'/',ware_grnheader.intGrnYear) as `GRN No`,
                concat(ware_returntosupplierheader.intReturnNo,'/',ware_returntosupplierheader.intReturnYear) as `Return No`,
                mst_subcategory.strName as `Sub Category`, 
                'a' as showOrderTot,
                'a' as showOrderSupTot, 
                'a' as  rwOrder, 
                'return' as type, 
                round(sum(ware_returntosupplierdetails.dblQty*tb2.dblGrnRate*ware_grnheader.dblExchangeRate*-1/$rate),2) as `Amount` 
                FROM
                trn_poheader 
                Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
                Inner Join (select * from trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem) as tb1 ON trn_poheader.intPONo = tb1.intPONo AND trn_poheader.intPOYear = tb1.intPOYear
                Inner Join ware_grnheader ON tb1.intPONo = ware_grnheader.intPoNo AND tb1.intPOYear = ware_grnheader.intPoYear AND ware_grnheader.intStatus='1' 
                Inner Join ware_grndetails as tb2 ON ware_grnheader.intGrnNo = tb2.intGrnNo AND ware_grnheader.intGrnYear = tb2.intGrnYear AND tb1.intItem = tb2.intItemId 
                Inner Join ware_returntosupplierheader ON tb2.intGrnNo = ware_returntosupplierheader.intGrnNo AND tb2.intGrnYear = ware_returntosupplierheader.intGrnYear AND ware_returntosupplierheader.intStatus='1' 
                Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear AND tb2.intItemId = ware_returntosupplierdetails.intItemId  
                Inner Join mst_item ON ware_returntosupplierdetails.intItemId = mst_item.intId
                Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
                WHERE 
                trn_poheader.intStatus='1' AND 
                ware_returntosupplierheader.intStatus='1'  ";
            if($dtFrom!='')
            $sql .= " AND trn_poheader.dtmPODate>='$dtFrom' ";
            if($dtTo!='')
            $sql .= " AND trn_poheader.dtmPODate<='$dtTo' ";
            if($mainCategory!='')
            $sql .= "AND mst_item.intMainCategory='$mainCategory' ";
            if($subCategory!='')
            $sql .= "AND mst_item.intSubCategory='$subCategory'  ";
            $sql .= "  
                GROUP BY 
                mst_item.intSubCategory,
                trn_poheader.intSupplier,
                tb1.intPONo,
                tb1.intPOYear,
                ware_grnheader.intGrnNo,
                ware_grnheader.intGrnYear,
                ware_returntosupplierheader.intReturnNo,
                ware_returntosupplierheader.intReturnYear 
                
                UNION 
                
                SELECT
                mst_subcategory.strName as SubCatOrder,
                '' as SUPPLIER,
                '' as ` Date`,
                '' as ` Invoice No`,
                '' as ` Invoice Date`,
                '' as `PO No`,
                ''  as `PO Type`,
                ''  as `GRN No`,
                '' as `Return No`,
                '' as  `Sub Category`, 
                'b' as showOrderTot,
                'x' as showOrderSupTot, 
                'b' as  rwOrder, 
                'subCatWiseTotal' as type, 
                round(IFNULL(Sum(ware_grndetails.dblGrnQty*ware_grndetails.dblGrnRate*ware_grnheader.dblExchangeRate/$rate),0)+ IFNULL((SELECT
                Sum(ware_returntosupplierdetails.dblQty*ware_grndetails.dblGrnRate*ware_grnheader.dblExchangeRate/$rate)
                FROM
                trn_poheader
                Inner Join (select * from trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem) AS trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear  AND trn_poheader.intStatus =  '1'
                Inner Join mst_item ON trn_podetails.intItem = mst_item.intId"; if($mainCategory!=''){ $sql .=" AND mst_item.intMainCategory='$mainCategory' "; }
                if($subCategory!=''){ $sql .=" AND mst_item.intSubCategory='$subCategory' "; }
                $sql .= " 
                Inner Join ware_grnheader ON trn_podetails.intPONo = ware_grnheader.intPoNo AND trn_podetails.intPOYear = ware_grnheader.intPoYear  AND ware_grnheader.intStatus =  '1' 
                Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND trn_podetails.intItem = ware_grndetails.intItemId
                Inner Join ware_returntosupplierheader ON ware_grndetails.intGrnNo = ware_returntosupplierheader.intGrnNo AND ware_grndetails.intGrnYear = ware_returntosupplierheader.intGrnYear  AND ware_returntosupplierheader.intStatus =  '1' 
                Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo 
                AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear AND ware_grndetails.intItemId = ware_returntosupplierdetails.intItemId 
                WHERE tb2.intSubCategory=mst_item.intSubCategory AND ware_grnheader.intStatus =  '1'   ";
            if($dtFrom!='')
            $sql .= " AND trn_poheader.dtmPODate>='$dtFrom' ";
            if($dtTo!='')
            $sql .= " AND trn_poheader.dtmPODate<='$dtTo' ";
            $sql .= "   ),0)*-1,2) as `Amount`
                FROM
                trn_poheader as tb1
                Inner Join (select * from trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem) AS trn_podetails ON tb1.intPONo = trn_podetails.intPONo AND tb1.intPOYear = trn_podetails.intPOYear
                Inner Join ware_grnheader ON trn_podetails.intPONo = ware_grnheader.intPoNo AND trn_podetails.intPOYear = ware_grnheader.intPoYear  AND ware_grnheader.intStatus =  '1' 
                Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND trn_podetails.intItem = ware_grndetails.intItemId 
                Inner Join mst_supplier ON tb1.intSupplier = mst_supplier.intId 
                Inner Join mst_item as tb2  ON ware_grndetails.intItemId = tb2.intId
                Inner Join mst_subcategory ON tb2.intSubCategory = mst_subcategory.intId
                WHERE
                ware_grnheader.intStatus =  '1' 
                 AND tb1.intStatus =  '1'  ";
            if($dtFrom!='')
            $sql .= " AND tb1.dtmPODate>='$dtFrom' ";
            if($dtTo!='')
            $sql .= " AND tb1.dtmPODate<='$dtTo' ";
            if($mainCategory!='')
            $sql .= "AND tb2.intMainCategory='$mainCategory' ";
            if($subCategory!='')
            $sql .= "AND tb2.intSubCategory='$subCategory'  ";
            $sql .= "  
                GROUP BY
                tb2.intSubCategory
                
                UNION 
                
                SELECT
                'z' as SubCatOrder,
                '' as SUPPLIER,
                '' as ` Date`,
                '' as ` Invoice No`,
                '' as ` Invoice Date`,
                '' as `PO No`,
                ''  as `PO Type`,
                ''  as `GRN No`,
                'Total' as `Return No`,
                '' as `Sub Category`, 
                'z' as showOrderSupTot, 
                'c' as showOrderTot, 
                'c' as  rwOrder, 
                'Total' as type, 
                round(IFNULL(Sum(ware_grndetails.dblGrnQty*ware_grndetails.dblGrnRate*ware_grnheader.dblExchangeRate/$rate),0)+ IFNULL((SELECT
                Sum(ware_returntosupplierdetails.dblQty*ware_grndetails.dblGrnRate*ware_grnheader.dblExchangeRate/$rate)
                FROM
                trn_poheader
                Inner Join (select * from trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem) AS trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear  AND trn_poheader.intStatus =  '1'
                Inner Join mst_item ON trn_podetails.intItem = mst_item.intId"; if($mainCategory!=''){ $sql .=" AND mst_item.intMainCategory='$mainCategory' "; }
                if($subCategory!=''){ $sql .=" AND mst_item.intSubCategory='$subCategory' "; }
                $sql .= " 
                Inner Join ware_grnheader ON trn_podetails.intPONo = ware_grnheader.intPoNo AND trn_podetails.intPOYear = ware_grnheader.intPoYear  AND ware_grnheader.intStatus =  '1' 
                Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND trn_podetails.intItem = ware_grndetails.intItemId
                Inner Join ware_returntosupplierheader ON ware_grndetails.intGrnNo = ware_returntosupplierheader.intGrnNo AND ware_grndetails.intGrnYear = ware_returntosupplierheader.intGrnYear  AND ware_returntosupplierheader.intStatus =  '1' 
                Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo 
                AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear AND ware_grndetails.intItemId = ware_returntosupplierdetails.intItemId  
where ware_grnheader.intStatus =  '1'   ";
            if($dtFrom!='')
            $sql .= " AND trn_poheader.dtmPODate>='$dtFrom' ";
            if($dtTo!='')
            $sql .= " AND trn_poheader.dtmPODate<='$dtTo' ";
            $sql .= "   ),0)*-1,2) as `Amount`
                FROM
                trn_poheader as tb1
                Inner Join (select * from trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem) AS trn_podetails ON tb1.intPONo = trn_podetails.intPONo AND tb1.intPOYear = trn_podetails.intPOYear  AND tb1.intStatus =  '1'
                Inner Join ware_grnheader ON trn_podetails.intPONo = ware_grnheader.intPoNo AND trn_podetails.intPOYear = ware_grnheader.intPoYear  AND ware_grnheader.intStatus =  '1' 
                Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND trn_podetails.intItem = ware_grndetails.intItemId 
                Inner Join mst_supplier ON tb1.intSupplier = mst_supplier.intId 
                Inner Join mst_financeexchangerate as tb3 ON tb1.intCurrency = tb3.intCurrencyId AND tb1.dtmPODate = tb3.dtmDate
                Inner Join mst_locations ON tb1.intCompany = mst_locations.intId AND tb3.intCompanyId = mst_locations.intCompanyId
                Inner Join mst_item ON ware_grndetails.intItemId = mst_item.intId
                Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
                WHERE
                ware_grnheader.intStatus =  '1' 
                 AND tb1.intStatus =  '1'  ";
            if($dtFrom!='')
            $sql .= " AND tb1.dtmPODate>='$dtFrom' ";
            if($dtTo!='')
            $sql .= " AND tb1.dtmPODate<='$dtTo' ";
            if($mainCategory!='')
            $sql .= "AND mst_item.intMainCategory='$mainCategory' ";
            if($subCategory!='')
            $sql .= "AND mst_item.intSubCategory='$subCategory'  ";
            $sql .= "  
                
                UNION 
                
                SELECT
                'y' as SubCatOrder,
                '' as SUPPLIER,
                '' as ` Date`,
                '' as ` Invoice No`,
                '' as ` Invoice Date`,
                '' as `PO No`,
                ''  as `PO Type`,
                ''  as `GRN No`,
                '' as `Return No`,
                '' as `Sub Category`, 
                'y' as showOrderSupTot, 
                'c' as showOrderTot, 
                'c' as  rwOrder, 
                'blankLineBeforeTotal' as type, 
                ''  as `Amount`  
                
                ) as tb1  
                order by  tb1.SubCatOrder asc , 
                tb1.`rwOrder` asc,
                tb1.`PO No` asc,
                tb1.`GRN No` asc,
                tb1.`Return No` asc,
                tb1.`showOrderSupTot` asc,
                tb1.`showOrderTot` asc,
                tb1.`Invoice Date` DESC";

             // echo $sql;
        $result = $db->RunQuery($sql);

        if($_REQUEST['report_type']=='excel')
        {
        include "xls_sub_category_wise_po_grn_summary.php";

        return;
        }

        while($data[] = mysqli_fetch_array($result, MYSQLI_ASSOC)) {}

        $pdf->ezImage('../../../images/logo.jpg',5,70,800,150);
        $pdf->selectFont('../../../libraries/pdf-php/fonts/Helvetica.afm');
        //$this->ezStartPageNumbers(580,10,10,'','',1);
        $pdf->ezStartPageNumbers(580,10,10,'','',1);
        //print_r($data);

        //$cols = array('Main'=>"Main",'Sub'=>'Sub','Code'=>'Code','Name'=>'Name');
        $currencyName="SUB CATEGORY PO - GRN SUMMERY$currencyName";
        $pdf->ezText($currencyName,$size=0,$options=array(),$test=0);
        $pdf->ezText('',$size=0,$options=array(),$test=0);

        $pdf->ezTable($data,'','',
array('xPos'=>0,'xOrientation'=>'right','width'=>550,'fontSize' => 7
,'cols'=>array('PO Qty'=>array('justification'=>'right'),'GRN Qty'=>array('justification'=>'right'),'Return Qty'=>array('justification'=>'right'),'Amount'=>array('justification'=>'right'))));


        $pdf->ezStream();
    }
else if($type=='SubCatWiseGRNPOSummery')
	{
		$currency = $_REQUEST['cboCurrency'];
		$mainCategory = $_REQUEST['cboMainCategory'];
		$subCategory = $_REQUEST['cboSubCategory'];
		$dtFrom = $_REQUEST['dtFromDateGRNPO'];
		$dtTo = $_REQUEST['dtToDateGRNPO'];
		$date = date("Y-m-d");
		// $date = "2012-09-25";
		$dtStr='';

		if(($dtFrom!='') || ($dtTo!='')){
			$dtStr.=" - ";
		}

		if($dtFrom!=''){
			$dtStr.="Date From : ".$dtFrom;
		}
		if($dtTo!=''){
			$dtStr .="  Date To : ".$dtTo;
		}


		if($supplier!=''){
			$supp=loadSupplierName($supplier);
			$supStr .=" - ".$supp;
		}

		$company = $_SESSION['headCompanyId'];

		$rate = loadCurrency($company,$date,$currency);
		$currencyName= loadCurrencyName($currency);
		if($currencyName!='')
		$currencyName= " (Currency : ".$currencyName.$supStr.$dtStr.")";

		//$rate =1;


		$sql = "select  
				`Sub Category`,
				`Date`,
				`GRNDate`,
				`SUPPLIER`,
				`Invoice No`,
				`Invoice Date`,
				`PO No`,
				`PO Type`,
				`GRN No`,
				`Return No`,`Amount` ";
		if($_REQUEST['report_type']=='excel')
		{
			$sql .=",SubCatOrder ";
		}
		$sql .=" from (SELECT 
				mst_subcategory.strName as SubCatOrder,
				mst_supplier.strName as SUPPLIER,
				trn_poheader.dtmPODate as `Date`,
				ware_grnheader.datdate as `GRNDate`,
				ware_grnheader.strInvoiceNo as` Invoice No`,
				ware_grnheader.SUPPLIER_INVOICE_DATE AS ` Invoice Date`,
				concat(tb1.intPONo,'/',tb1.intPOYear) as `PO No`,
				mst_shipmentterms.strName as `PO Type`,
				concat(ware_grnheader.intGrnNo,'/',ware_grnheader.intGrnYear) as `GRN No`,
				'' as `Return No`,
				mst_subcategory.strName as `Sub Category`, 
				'a' as showOrderSupTot, 
				'a' as showOrderTot, 
				'a' as  rwOrder, 
				'grn' as type, 
				round(sum( tb2.dblGrnQty*tb2.dblGrnRate*ware_grnheader.dblExchangeRate/$rate) ,2)  as `Amount` 
				FROM
				trn_poheader 
				Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId 
				Inner Join mst_shipmentterms ON trn_poheader.intShipmentTerm = mst_shipmentterms.intId
				Inner Join (select * from trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem) as tb1 ON trn_poheader.intPONo = tb1.intPONo AND trn_poheader.intPOYear = tb1.intPOYear
				inner Join ware_grnheader ON tb1.intPONo = ware_grnheader.intPoNo AND tb1.intPOYear = ware_grnheader.intPoYear AND ware_grnheader.intStatus='1' 
				inner Join ware_grndetails as tb2 ON ware_grnheader.intGrnNo = tb2.intGrnNo AND ware_grnheader.intGrnYear = tb2.intGrnYear AND tb1.intItem = tb2.intItemId   
				Inner Join mst_item ON tb2.intItemId = mst_item.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				WHERE 
				trn_poheader.intStatus='1' ";
			if($dtFrom!='')
			$sql .= " AND ware_grnheader.datdate>='$dtFrom' ";
			if($dtTo!='')
			$sql .= " AND ware_grnheader.datdate<='$dtTo' ";
			if($mainCategory!='')
			$sql .= "AND mst_item.intMainCategory='$mainCategory' ";
			if($subCategory!='')
			$sql .= "AND mst_item.intSubCategory='$subCategory'  ";
			$sql .= "  
				GROUP BY 
				mst_item.intSubCategory,
				trn_poheader.intSupplier,
				tb1.intPONo,
				tb1.intPOYear,
				ware_grnheader.intGrnNo,
				ware_grnheader.intGrnYear 
				
				UNION 
				
				SELECT
				mst_subcategory.strName as SubCatOrder,
				mst_supplier.strName as SUPPLIER,
				ware_returntosupplierheader.datdate as `Date`,
				ware_grnheader.datdate as `GRNDate`,
				'' as` Invoice No`,
				'' as` Invoice Date`,
				concat(trn_poheader.intPONo,'/',trn_poheader.intPOYear) as `PO No`,
				''  as `PO Type`,
				concat(ware_grnheader.intGrnNo,'/',ware_grnheader.intGrnYear) as `GRN No`,
				concat(ware_returntosupplierheader.intReturnNo,'/',ware_returntosupplierheader.intReturnYear) as `Return No`,
				mst_subcategory.strName as `Sub Category`, 
				'a' as showOrderTot,
				'a' as showOrderSupTot, 
				'a' as  rwOrder, 
				'return' as type, 
				round(sum(ware_returntosupplierdetails.dblQty*tb2.dblGrnRate*ware_grnheader.dblExchangeRate*-1/$rate),2) as `Amount` 
				FROM
				trn_poheader 
				Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
				Inner Join (select * from trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem) as tb1 ON trn_poheader.intPONo = tb1.intPONo AND trn_poheader.intPOYear = tb1.intPOYear
				Inner Join ware_grnheader ON tb1.intPONo = ware_grnheader.intPoNo AND tb1.intPOYear = ware_grnheader.intPoYear AND ware_grnheader.intStatus='1' 
				Inner Join ware_grndetails as tb2 ON ware_grnheader.intGrnNo = tb2.intGrnNo AND ware_grnheader.intGrnYear = tb2.intGrnYear AND tb1.intItem = tb2.intItemId 
				Inner Join ware_returntosupplierheader ON tb2.intGrnNo = ware_returntosupplierheader.intGrnNo AND tb2.intGrnYear = ware_returntosupplierheader.intGrnYear AND ware_returntosupplierheader.intStatus='1' 
				Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear AND tb2.intItemId = ware_returntosupplierdetails.intItemId  
				Inner Join mst_item ON ware_returntosupplierdetails.intItemId = mst_item.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				WHERE 
				trn_poheader.intStatus='1' AND 
				ware_returntosupplierheader.intStatus='1'  ";
			if($dtFrom!='')
			$sql .= " AND ware_grnheader.datdate>='$dtFrom' ";
			if($dtTo!='')
			$sql .= " AND ware_grnheader.datdate<='$dtTo' ";
			if($mainCategory!='')
			$sql .= "AND mst_item.intMainCategory='$mainCategory' ";
			if($subCategory!='')
			$sql .= "AND mst_item.intSubCategory='$subCategory'  ";
			$sql .= "  
				GROUP BY 
				mst_item.intSubCategory,
				trn_poheader.intSupplier,
				tb1.intPONo,
				tb1.intPOYear,
				ware_grnheader.intGrnNo,
				ware_grnheader.intGrnYear,
				ware_returntosupplierheader.intReturnNo,
				ware_returntosupplierheader.intReturnYear 
				
				UNION 
				
				SELECT
				mst_subcategory.strName as SubCatOrder,
				'' as SUPPLIER,
				'' as `Date`,
				'' as `GRNDate`,
				'' as ` Invoice No`,
				'' as ` Invoice Date`,
				'' as `PO No`,
				''  as `PO Type`,
				''  as `GRN No`,
				'' as `Return No`,
				'' as  `Sub Category`, 
				'b' as showOrderTot,
				'x' as showOrderSupTot, 
				'b' as  rwOrder, 
				'subCatWiseTotal' as type, 
				round(IFNULL(Sum(ware_grndetails.dblGrnQty*ware_grndetails.dblGrnRate*ware_grnheader.dblExchangeRate/$rate),0)+ IFNULL((SELECT
				Sum(ware_returntosupplierdetails.dblQty*ware_grndetails.dblGrnRate*ware_grnheader.dblExchangeRate/$rate)
				FROM
				trn_poheader
				Inner Join (select * from trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem) AS trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear  AND trn_poheader.intStatus =  '1'
				Inner Join mst_item ON trn_podetails.intItem = mst_item.intId"; if($mainCategory!=''){ $sql .=" AND mst_item.intMainCategory='$mainCategory' "; }
				if($subCategory!=''){ $sql .=" AND mst_item.intSubCategory='$subCategory' "; }
				$sql .= " 
				Inner Join ware_grnheader ON trn_podetails.intPONo = ware_grnheader.intPoNo AND trn_podetails.intPOYear = ware_grnheader.intPoYear  AND ware_grnheader.intStatus =  '1' 
				Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND trn_podetails.intItem = ware_grndetails.intItemId
				Inner Join ware_returntosupplierheader ON ware_grndetails.intGrnNo = ware_returntosupplierheader.intGrnNo AND ware_grndetails.intGrnYear = ware_returntosupplierheader.intGrnYear  AND ware_returntosupplierheader.intStatus =  '1' 
				Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo 
				AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear AND ware_grndetails.intItemId = ware_returntosupplierdetails.intItemId 

				WHERE tb2.intSubCategory=mst_item.intSubCategory AND ware_grnheader.intStatus =  '1'   ";
			if($dtFrom!='')
			$sql .= " AND ware_grnheader.datdate>='$dtFrom' ";
			if($dtTo!='')
			$sql .= " AND ware_grnheader.datdate<='$dtTo' ";
			$sql .= "   ),0)*-1,2) as `Amount`
				FROM
				trn_poheader as tb1
				Inner Join (select * from trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem) AS trn_podetails ON tb1.intPONo = trn_podetails.intPONo AND tb1.intPOYear = trn_podetails.intPOYear
				Inner Join ware_grnheader ON trn_podetails.intPONo = ware_grnheader.intPoNo AND trn_podetails.intPOYear = ware_grnheader.intPoYear  AND ware_grnheader.intStatus =  '1' 
				Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND trn_podetails.intItem = ware_grndetails.intItemId 
				Inner Join mst_supplier ON tb1.intSupplier = mst_supplier.intId 
				Inner Join mst_item as tb2  ON ware_grndetails.intItemId = tb2.intId
				Inner Join mst_subcategory ON tb2.intSubCategory = mst_subcategory.intId
				WHERE
				ware_grnheader.intStatus =  '1' 
				 AND tb1.intStatus =  '1'  ";
			if($dtFrom!='')
			$sql .= " AND ware_grnheader.datdate>='$dtFrom' ";
			if($dtTo!='')
			$sql .= " AND ware_grnheader.datdate<='$dtTo' ";
			if($mainCategory!='')
			$sql .= "AND tb2.intMainCategory='$mainCategory' ";
			if($subCategory!='')
			$sql .= "AND tb2.intSubCategory='$subCategory'  ";
			$sql .= "  
				GROUP BY
				tb2.intSubCategory
				
				UNION 
				
				SELECT
				'z' as SubCatOrder,
				'' as SUPPLIER,
				'' as `Date`,
				'' as `GRNDate`,
				'' as ` Invoice No`,
				'' as ` Invoice Date`,
				'' as `PO No`,
				''  as `PO Type`,
				''  as `GRN No`,
				'Total' as `Return No`,
				'' as `Sub Category`, 
				'z' as showOrderSupTot, 
				'c' as showOrderTot, 
				'c' as  rwOrder, 
				'Total' as type, 
				round(IFNULL(Sum(ware_grndetails.dblGrnQty*ware_grndetails.dblGrnRate*ware_grnheader.dblExchangeRate/$rate),0)+ IFNULL((SELECT
				Sum(ware_returntosupplierdetails.dblQty*ware_grndetails.dblGrnRate*ware_grnheader.dblExchangeRate/$rate)
				FROM
				trn_poheader
				Inner Join (select * from trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem) AS trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear  AND trn_poheader.intStatus =  '1'
				Inner Join mst_item ON trn_podetails.intItem = mst_item.intId"; if($mainCategory!=''){ $sql .=" AND mst_item.intMainCategory='$mainCategory' "; }
				if($subCategory!=''){ $sql .=" AND mst_item.intSubCategory='$subCategory' "; }
				$sql .= " 
				Inner Join ware_grnheader ON trn_podetails.intPONo = ware_grnheader.intPoNo AND trn_podetails.intPOYear = ware_grnheader.intPoYear  AND ware_grnheader.intStatus =  '1' 
				Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND trn_podetails.intItem = ware_grndetails.intItemId
				Inner Join ware_returntosupplierheader ON ware_grndetails.intGrnNo = ware_returntosupplierheader.intGrnNo AND ware_grndetails.intGrnYear = ware_returntosupplierheader.intGrnYear  AND ware_returntosupplierheader.intStatus =  '1' 
				Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo 
				AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear AND ware_grndetails.intItemId = ware_returntosupplierdetails.intItemId  
where ware_grnheader.intStatus =  '1'   ";
			if($dtFrom!='')
			$sql .= " AND ware_grnheader.datdate>='$dtFrom' ";
			if($dtTo!='')
			$sql .= " AND ware_grnheader.datdate<='$dtTo' ";
			$sql .= "   ),0)*-1,2) as `Amount`
				FROM
				trn_poheader as tb1
				Inner Join (select * from trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem) AS trn_podetails ON tb1.intPONo = trn_podetails.intPONo AND tb1.intPOYear = trn_podetails.intPOYear  AND tb1.intStatus =  '1'
				Inner Join ware_grnheader ON trn_podetails.intPONo = ware_grnheader.intPoNo AND trn_podetails.intPOYear = ware_grnheader.intPoYear  AND ware_grnheader.intStatus =  '1' 
				Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND trn_podetails.intItem = ware_grndetails.intItemId 
				Inner Join mst_supplier ON tb1.intSupplier = mst_supplier.intId 
				Inner Join mst_financeexchangerate as tb3 ON tb1.intCurrency = tb3.intCurrencyId AND tb1.dtmPODate = tb3.dtmDate
				Inner Join mst_locations ON tb1.intCompany = mst_locations.intId AND tb3.intCompanyId = mst_locations.intCompanyId
				Inner Join mst_item ON ware_grndetails.intItemId = mst_item.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				WHERE
				ware_grnheader.intStatus =  '1' 
				 AND tb1.intStatus =  '1'  ";
			if($dtFrom!='')
			$sql .= " AND ware_grnheader.datdate>='$dtFrom' ";
			if($dtTo!='')
			$sql .= " AND ware_grnheader.datdate<='$dtTo' ";
			if($mainCategory!='')
			$sql .= "AND mst_item.intMainCategory='$mainCategory' ";
			if($subCategory!='')
			$sql .= "AND mst_item.intSubCategory='$subCategory'  ";
			$sql .= "  
				
				UNION 
				
				SELECT
				'y' as SubCatOrder,
				'' as SUPPLIER,
				'' as ` Date`,
				'' as `GRNDate`,
				'' as ` Invoice No`,
				'' as ` Invoice Date`,
				'' as `PO No`,
				''  as `PO Type`,
				''  as `GRN No`,
				'' as `Return No`,
				'' as `Sub Category`, 
				'y' as showOrderSupTot, 
				'c' as showOrderTot, 
				'c' as  rwOrder, 
				'blankLineBeforeTotal' as type, 
				''  as `Amount`  
				
				) as tb1  
				order by  tb1.SubCatOrder asc , 
				tb1.`rwOrder` asc,
				tb1.`PO No` asc,
				tb1.`GRN No` asc,
				tb1.`Return No` asc,
				tb1.`showOrderSupTot` asc,
				tb1.`showOrderTot` asc,
				tb1.`Invoice Date` DESC";

			 // echo $sql;
		$result = $db->RunQuery($sql);

		if($_REQUEST['report_type']=='excel')
		{
		include "xls_sub_category_wise_grn_po_summary.php";

		return;
		}

		while($data[] = mysqli_fetch_array($result, MYSQLI_ASSOC)) {}

		$pdf->ezImage('../../../images/logo.jpg',5,70,800,150);
		$pdf->selectFont('../../../libraries/pdf-php/fonts/Helvetica.afm');
		//$this->ezStartPageNumbers(580,10,10,'','',1);
		$pdf->ezStartPageNumbers(580,10,10,'','',1);
		//print_r($data);

		//$cols = array('Main'=>"Main",'Sub'=>'Sub','Code'=>'Code','Name'=>'Name');
	    $currencyName="SUB CATEGORY GRN - PO SUMMERY$currencyName";
		$pdf->ezText($currencyName,$size=0,$options=array(),$test=0);
		$pdf->ezText('',$size=0,$options=array(),$test=0);

		$pdf->ezTable($data,'','',
array('xPos'=>0,'xOrientation'=>'right','width'=>550,'fontSize' => 7
,'cols'=>array('PO Qty'=>array('justification'=>'right'),'GRN Qty'=>array('justification'=>'right'),'Return Qty'=>array('justification'=>'right'),'Amount'=>array('justification'=>'right'))));


		$pdf->ezStream();
	}

else if($type=='POPendingSummery')
        {
            $currency = $_REQUEST['cboCurrency'];
            $dtFrom = $_REQUEST['dtFromDate5'];
            $dtTo = $_REQUEST['dtToDate5'];
            $date = date("Y-m-d");
            // $date = "2012-09-25";
            $dtStr='';

            if(($dtFrom!='') ||($dtTo!='')){
                $dtStr.="";
            }

            if($dtFrom!=''){
                $dtStr.="Date From : ".$dtFrom;
            }
            if($dtTo!=''){
                $dtStr .="Date To : ".$dtTo;
            }


            if($supplier!=''){
                $supp=loadSupplierName($supplier);
                $supStr .=" - ".$supp;
            }

            $company = $_SESSION['headCompanyId'];

            $rate = loadCurrency($company,$date,$currency);
            $currencyName= loadCurrencyName($currency);
            if(($dtFrom!='') ||($dtTo!=''))
            $currencyName= " (".$dtStr.")";

            //$rate =1;


            $sql = "select 
                    `SUPPLIER`,
                    `Date`,
                    `PO No`,
                    `PO Qty`,
                    `GRN Qty`,
                    `Balance Qty` 
                     from (SELECT 
                    mst_supplier.strName as SUPPLIER,
                    tb1.dtmPODate as` Date`,
                    concat(tb1.intPONo,'/',tb1.intPOYear) as `PO No`,
                    round(sum( trn_podetails.dblQty) ,2)  as `PO Qty` , 
                    
                    round((SELECT
                            Sum(ware_grndetails.dblGrnQty)
                            FROM
                            ware_grnheader
                            Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear
                            WHERE
                            ware_grnheader.intStatus =  '1' AND tb1.intPONo=ware_grnheader.intPONo AND tb1.intPOYear=ware_grnheader.intPOYear) ,2)  as `GRN Qty` ,
                            
                    round((SELECT
                            Sum(ware_returntosupplierdetails.dblQty)
                            FROM
                            ware_grnheader
                            Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear 
                            Inner Join ware_returntosupplierheader ON ware_grndetails.intGrnNo = ware_returntosupplierheader.intGrnNo AND ware_grndetails.intGrnYear = ware_returntosupplierheader.intGrnYear  AND ware_returntosupplierheader.intStatus =  '1' 
                            Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo 
                            WHERE
                            ware_returntosupplierheader.intStatus =  '1' AND tb1.intPONo=ware_grnheader.intPONo AND tb1.intPOYear=ware_grnheader.intPOYear) ,2)  as `Return Qty` , 
                            
                    round(((SELECT
                            Sum(ware_grndetails.dblGrnQty)
                            FROM
                            ware_grnheader
                            Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear
                            WHERE
                            ware_grnheader.intStatus =  '1' AND tb1.intPONo=ware_grnheader.intPONo AND tb1.intPOYear=ware_grnheader.intPOYear)-(SELECT
                            Sum(ware_returntosupplierdetails.dblQty)
                            FROM
                            ware_grnheader
                            Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear 
                            Inner Join ware_returntosupplierheader ON ware_grndetails.intGrnNo = ware_returntosupplierheader.intGrnNo AND ware_grndetails.intGrnYear = ware_returntosupplierheader.intGrnYear  AND ware_returntosupplierheader.intStatus =  '1' 
                            Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo 
                            WHERE
                            ware_returntosupplierheader.intStatus =  '1' AND tb1.intPONo=ware_grnheader.intPONo AND tb1.intPOYear=ware_grnheader.intPOYear)) ,2)  as `Balance Qty` 
                            
                    FROM
                    trn_poheader as tb1  
                    Inner Join mst_supplier ON tb1.intSupplier = mst_supplier.intId 
                    Inner Join mst_shipmentterms ON tb1.intShipmentTerm = mst_shipmentterms.intId
                    Inner Join trn_podetails ON tb1.intPONo = trn_podetails.intPONo AND tb1.intPOYear = trn_podetails.intPOYear 
                    WHERE 
                    tb1.intStatus='1' ";
                if($dtFrom!='')
                $sql .= " AND tb1.dtmPODate>='$dtFrom' ";
                if($dtTo!='')
                $sql .= " AND tb1.dtmPODate<='$dtTo' ";
                $sql .= "  
                    GROUP BY
                    trn_podetails.intPONo,
                    trn_podetails.intPOYear 
                    
                    ) as tb1   
                    Order By 
                    tb1.`PO No` asc";

                //   echo $sql;
            $result = $db->RunQuery($sql);
            while($data[] = mysqli_fetch_array($result, MYSQLI_ASSOC)) {}

            $pdf->ezImage('../../../images/logo.jpg',5,70,800,150);
            $pdf->selectFont('../../../libraries/pdf-php/fonts/Helvetica.afm');
            //$this->ezStartPageNumbers(580,10,10,'','',1);
            $pdf->ezStartPageNumbers(580,10,10,'','',1);
            //print_r($data);

            //$cols = array('Main'=>"Main",'Sub'=>'Sub','Code'=>'Code','Name'=>'Name');
            $currencyName="PENDING PO SUMMERY$currencyName";
            $pdf->ezText($currencyName,$size=0,$options=array(),$test=0);
            $pdf->ezText('',$size=0,$options=array(),$test=0);
            $pdf->ezTable($data,'','',
    array('xPos'=>0,'xOrientation'=>'right','width'=>550,'fontSize' => 7
    ,'cols'=>array('PO Qty'=>array('justification'=>'right'),'GRN Qty'=>array('justification'=>'right'),'Return Qty'=>array('justification'=>'right'),'Balance Qty'=>array('justification'=>'right'),'Amount'=>array('justification'=>'right'))));


            $pdf->ezStream();
        }
else if($type=='itemForcast')
{
			    $company           = $_REQUEST['company'];
				$maincategory	   = $_REQUEST['maincategory'];
				$subcategory       = $_REQUEST['subcategory'];
				$item  			   = $_REQUEST['item'];
				$BalToPurchase     = $_REQUEST['BalToPurchase'];
				$mrn  			   = $_REQUEST['mrn'];
				$curYear 		   = date('Y');
				$companyId         = $_SESSION["headCompanyId"];
                $locationId        = $_SESSION["CompanyID"];

	$head_office_loc   = get_ho_loc($company);
	$pdfLandscape      = new Cezpdf('a4','landscape');
	$sql 			   = "SELECT 
										TB_ALL .type,
										TB_ALL .ORDER_COMPANY,
										TB_ALL .location AS location,
										TB_ALL.mainCatId,
										TB_ALL.subCatId, 
										TB_ALL .ITEM, 
										TB_ALL.re_order_policy,
										TB_ALL .mainCatName, 
										TB_ALL .subCatName,
										TB_ALL .strCode,
                                                                                TB_ALL.supItemCode,
										TB_ALL .itemName, 
										TB_ALL .otherOrders AS TODATE_ORDERS,
										TB_ALL .REQUIRED AS REQUIRED_TODATE,
										TB_ALL .PRN_QTY AS PRN_QTY_TODATE,
										TB_ALL .PO_QTY AS PO_QTY_TODATE, 
										(
										ifnull(TB_ALL.MRN_QTY,0) + ifnull(TB_ALL.MRN_QTY_OTHER_LOC,0)
										) AS MRN_QTY_TODATE,
										
										(
										ifnull(TB_ALL.ISSUE_QTY,0) + ifnull(TB_ALL.ISSUE_QTY_OTHER_LOC,0) 
										) AS ISSUE_QTY_TODATE,
										
										(
										ifnull(TB_ALL.RET_TO_STORE_QTY,0) 
										+ ifnull(TB_ALL.RET_TO_STORE_QTY_OTHER_LOC,0) 
										) AS RET_TO_STORE_QTY_TODATE,
										
										TB_ALL.GP_QTY AS GP_FROM_HO_TODATE,
										TB_ALL.GP_IN_QTY AS GP_IN_QTY_TO_HO_TODATE,
										TB_ALL.UOM,
										TB_ALL.otherOrders AS OTHER_ORDERS, 
										
										(
										ifnull(TB_ALL.ISSUE_QTY,0) 
										) AS ISSUE_QTY_ALL_HO,
										
										(
										ifnull(TB_ALL.GP_QTY,0)  
										) AS GP_FROM_HO_ALL,
										
										(
										ifnull(TB_ALL.ISSUE_QTY_OTHER_LOC,0)  
										) AS ISSUE_QTY_ALL_NON_HO
										
										from 
										/*tb_all*/	
										(select tb.type,
										tb.ORDER_COMPANY, 
										tb.location AS location, 
										tb.ITEM, 
										GROUP_CONCAT(DISTINCT tb.ORDER_NO,'/',tb.ORDER_YEAR) as otherOrders,
										
										SUM(tb.REQUIRED) as REQUIRED,
										tb.TRANSACTION_LOCATION, 
										sum(tb.PO_QTY) as PO_QTY, 
										sum(tb.PRN_QTY) as PRN_QTY, 
										tb.mainCatId,
										tb.subCatId,
										tb.mainCatName,
										tb.subCatName, 
										tb.strCode,
                                                                                tb.supItemCode,
										tb.itemName, 
										tb.re_order_policy,
										tb.UOM, 
										SUM(tb.MRN_QTY) as MRN_QTY, 
										SUM(tb.MRN_EXTRA_QTY) as MRN_EXTRA_QTY, 
										SUM(tb.ISSUE_QTY) as ISSUE_QTY, 
										SUM(tb.GP_QTY) as GP_QTY, 
										SUM(tb.GP_IN_QTY) as GP_IN_QTY, 
										SUM(tb.RET_TO_STORE_QTY) as RET_TO_STORE_QTY, 
										SUM(tb.MRN_QTY_OTHER_LOC) as MRN_QTY_OTHER_LOC, 
										SUM(tb.MRN_EXTRA_QTY_OTHER_LOC) as MRN_EXTRA_QTY_OTHER_LOC, 
										SUM(tb.ISSUE_QTY_OTHER_LOC) as ISSUE_QTY_OTHER_LOC, 
										SUM(tb.GP_QTY_OTHER_LOC) as GP_QTY_OTHER_LOC, 
										SUM(tb.GP_IN_QTY_OTHER_LOC) as GP_IN_QTY_OTHER_LOC, 
										SUM(tb.RET_TO_STORE_QTY_OTHER_LOC) as RET_TO_STORE_QTY_OTHER_LOC 
										/*tb*/	
										from( 
										SELECT 	TB3.type, 
										TB3.ORDER_COMPANY, 
										TB3.location AS location, 
										TB3.ITEM, 
										TB3.ORDER_NO, 
										TB3.ORDER_YEAR, 
										
										-- SUM(TB3.REQUIRED) AS REQUIRED,
										(
										SELECT
										sum(
										trn_po_prn_details.REQUIRED
										)
										FROM
										trn_po_prn_details
										WHERE
										trn_po_prn_details.ORDER_NO = TB3.ORDER_NO
										AND trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR
										AND trn_po_prn_details.ITEM = TB3.ITEM
										) AS REQUIRED,
										
										(SELECT trn_po_prn_details.PURCHASED_QTY 
										FROM trn_po_prn_details 
										WHERE trn_po_prn_details.ORDER_NO = TB3.ORDER_NO 
										AND trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR 
										AND trn_po_prn_details.ITEM = TB3.ITEM ) AS PO_QTY, 
										
										(SELECT trn_po_prn_details.PRN_QTY
										FROM trn_po_prn_details 
										WHERE trn_po_prn_details.ORDER_NO = TB3.ORDER_NO AND
										trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR AND 
										trn_po_prn_details.ITEM = TB3.ITEM ) AS PRN_QTY,
										
										TB3.TRANSACTION_LOCATION, 
										TB3.mainCatId,
										TB3.subCatId,
										TB3.mainCatName, 
										TB3.subCatName, 
										TB3.strCode,
                                                                                TB3.supItemCode,
										TB3.itemName, 
										TB3.re_order_policy,
										TB3.UOM, 
										SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY, 
										SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY, 
										SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY, 
										SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_QTY,0)) as GP_QTY, 
										SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY, 
										SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY, 
										SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY_OTHER_LOC, 
										SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY_OTHER_LOC, 
										SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY_OTHER_LOC, 
										SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_QTY,0)) as GP_QTY_OTHER_LOC, 
										SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY_OTHER_LOC, 
										SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY_OTHER_LOC 
										FROM 
										/*tb3*/	
										(SELECT TB2.ORDER_COMPANY, 
										TB2.ORDER_NO, 
										TB2.ORDER_YEAR, 
										TB2.location, 
										TB2.ITEM,
										TB2.SALES_ORDER, 
										TB2.type, 
										CONCAT(trn_orderdetails.strSalesOrderNo,'/',trn_orderdetails.strCombo,'/',trn_orderdetails.strPrintName) AS SALES_ORDER_String, 
										
										mst_maincategory.intId as mainCatId,
										mst_subcategory.intId AS subCatId,  
										mst_maincategory.strName AS mainCatName, 
										mst_subcategory.strName AS subCatName, 
										mst_item.strCode,
                                                                                CONCAT(mst_supplier.strCode,mst_item.strCode) as supItemCode,
										mst_item.intId, 
										mst_item.strName AS itemName, 
										mst_item.intReorderPolicy AS re_order_policy,
										mst_units.strName AS UOM, 
										-- trn_po_prn_details_sales_order.REQUIRED, 
										trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID AS TRANSACTION_LOCATION, 
										
										SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN', 
										trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS MRN_QTY, 
										SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN_EXTRA', 
										trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS MRN_EXTRA_QTY, 
										SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'ISSUE', 
										trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS ISSUE_QTY, 
										SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS', 
										trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS GP_QTY, 
										SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS_IN', 
										trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS GP_IN_QTY, 
										SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'RETURNED_TO_STORES', 
										trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS RET_TO_STORE_QTY 
										FROM 
										/*TB2*/( SELECT * FROM 
										/*tb1*/	( SELECT 
										trn_po_prn_details_sales_order_stock_transactions.ORDER_NO, 
										trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR, 
										trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER, 
										trn_po_prn_details_sales_order_stock_transactions.ITEM, 
										mst_locations.intCompanyId AS ORDER_COMPANY, 
										mst_locations.strName AS location, 
										( SELECT GROUP_CONCAT(mst_locations.intCompanyId) 
										FROM ware_fabricreceivedheader 
										INNER JOIN mst_locations 
										ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId 
										WHERE 
										ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO 
										AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR 
										)AS FABRIC_IN_COMPANIES, 
										1 AS type 
										FROM trn_po_prn_details_sales_order_stock_transactions 
										INNER JOIN trn_po_prn_details_sales_order 
										ON 
										trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO 
										AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR 
										AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER 
										AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM 
										INNER JOIN trn_orderheader 
										ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND 
										trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR 
										AND trn_orderheader.intStatus = 1
										INNER JOIN mst_locations 
										ON mst_locations.intId = trn_orderheader.intLocationId 
										
										
										GROUP BY 
										trn_po_prn_details_sales_order_stock_transactions.ORDER_NO, 
										trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR, 
										trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER, 
										trn_po_prn_details_sales_order_stock_transactions.ITEM 
										HAVING FIND_IN_SET('$company', FABRIC_IN_COMPANIES) OR ORDER_COMPANY = '$company' 
										
										UNION ALL 
										
										SELECT 
										trn_po_prn_details_sales_order_stock_transactions.ORDER_NO, 
										trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR, 
										trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER, 
										trn_po_prn_details_sales_order_stock_transactions.ITEM, 
										mst_locations.intCompanyId AS ORDER_COMPANY, 
										mst_locations.strName AS location, 
										( SELECT GROUP_CONCAT( DISTINCT mst_locations.intCompanyId ) 
										FROM ware_fabricreceivedheader 
										INNER JOIN mst_locations 
										ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId 
										WHERE 
										ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND 
										ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR 
										) AS FABRIC_IN_COMPANIES, 
										2 AS type 
										FROM trn_po_prn_details_sales_order_stock_transactions 
										
										LEFT JOIN trn_po_prn_details_sales_order 
										ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO AND 
										trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR AND 
										trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER AND 
										trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM 
										INNER JOIN trn_orderheader ON 
										trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND 
										trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR 
										AND trn_orderheader.intStatus = 1
										INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId 
										
										WHERE trn_po_prn_details_sales_order.ORDER_NO IS NULL 
										
										GROUP BY 
										trn_po_prn_details_sales_order_stock_transactions.ORDER_NO, 
										trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR, 
										trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER, 
										trn_po_prn_details_sales_order_stock_transactions.ITEM 
										HAVING ( FIND_IN_SET('$company', FABRIC_IN_COMPANIES) OR ORDER_COMPANY = '$company' ) 
										
										
										UNION ALL 
										
										SELECT 
										trn_po_prn_details_sales_order.ORDER_NO, 
										trn_po_prn_details_sales_order.ORDER_YEAR, 
										trn_po_prn_details_sales_order.SALES_ORDER, 
										trn_po_prn_details_sales_order.ITEM, 
										mst_locations.intCompanyId AS ORDER_COMPANY, 
										mst_locations.strName AS location, 
										( SELECT GROUP_CONCAT( DISTINCT mst_locations.intCompanyId ) 
										FROM ware_fabricreceivedheader 
										INNER JOIN mst_locations 
										ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId 
										WHERE ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO 
										AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR 
										) AS FABRIC_IN_COMPANIES, 
										1 AS type 
										FROM trn_po_prn_details_sales_order_stock_transactions
										
										RIGHT JOIN 
										trn_po_prn_details_sales_order 
										ON 
										trn_po_prn_details_sales_order_stock_transactions.ORDER_NO    = trn_po_prn_details_sales_order.ORDER_NO AND 
										trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR  = trn_po_prn_details_sales_order.ORDER_YEAR AND 
										trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER AND 
										trn_po_prn_details_sales_order_stock_transactions.ITEM        = trn_po_prn_details_sales_order.ITEM 
										INNER JOIN trn_orderheader 
										ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO AND 
										trn_orderheader.intOrderYear  = trn_po_prn_details_sales_order.ORDER_YEAR
										AND trn_orderheader.intStatus = 1 
										INNER JOIN mst_locations 
										ON mst_locations.intId = trn_orderheader.intLocationId 
										
										WHERE trn_po_prn_details_sales_order_stock_transactions.ORDER_NO IS NULL 
										
										GROUP BY 
										trn_po_prn_details_sales_order.ORDER_NO, 
										trn_po_prn_details_sales_order.ORDER_YEAR, 
										trn_po_prn_details_sales_order.SALES_ORDER, 
										trn_po_prn_details_sales_order.ITEM 
										
										HAVING ( FIND_IN_SET('$company', FABRIC_IN_COMPANIES) OR ORDER_COMPANY = '$company' ) ) AS TB1 
										GROUP BY 
										TB1.ORDER_NO, 
										TB1.ORDER_YEAR, 
										TB1.SALES_ORDER,
										TB1.ITEM ) AS TB2 
										
										LEFT JOIN 
										trn_po_prn_details_sales_order_stock_transactions 
										ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = TB2.ORDER_NO AND 
										trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = TB2.ORDER_YEAR AND 
										trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = TB2.SALES_ORDER AND 
										trn_po_prn_details_sales_order_stock_transactions.ITEM = TB2.ITEM 
										INNER JOIN trn_orderheader 
										ON trn_orderheader.intOrderNo = TB2.ORDER_NO AND 
										trn_orderheader.intOrderYear = TB2.ORDER_YEAR  AND
										trn_orderheader.intStatus = 1
										
										LEFT JOIN 
										trn_po_prn_details_sales_order 
										ON 
										trn_po_prn_details_sales_order.ORDER_NO =  TB2.ORDER_NO 
										AND trn_po_prn_details_sales_order.ORDER_YEAR =  TB2.ORDER_YEAR 
										AND trn_po_prn_details_sales_order.SALES_ORDER =  TB2.SALES_ORDER 
										AND trn_po_prn_details_sales_order.ITEM =  TB2.ITEM 
										INNER JOIN trn_orderdetails 
										ON TB2.ORDER_NO     = trn_orderdetails.intOrderNo 
										AND TB2.ORDER_YEAR  = trn_orderdetails.intOrderYear 
										AND TB2.SALES_ORDER = trn_orderdetails.intSalesOrderId 
										INNER JOIN mst_item 
										ON TB2.ITEM = mst_item.intId 
                                                                                LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
										INNER JOIN mst_units 
										ON mst_item.intUOM = mst_units.intId 
										INNER JOIN mst_maincategory 
										ON mst_item.intMainCategory = mst_maincategory.intId 
										INNER JOIN mst_subcategory 
										ON mst_item.intSubCategory = mst_subcategory.intId 
										GROUP BY 
										TB2.ORDER_NO, 
										TB2.ORDER_YEAR, 
										TB2.SALES_ORDER, 
										TB2.ITEM,
										trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID 
										) AS TB3 
										GROUP BY
										
										TB3.ORDER_NO,
										TB3.ORDER_YEAR,
										TB3.ITEM) 
										as 
										tb 
										GROUP BY tb.ITEM ) as TB_ALL 
										WHERE
										1 = 1";

										$sql .=  "  AND TB_ALL .ORDER_COMPANY IN ($company)";


										if($maincategory !='null')
										{
										$sql .=  "  AND TB_ALL.mainCatId IN ($maincategory)";
										}
										if($subcategory !='null')
										{
										$sql .=  "  AND TB_ALL.subCatId IN ($subcategory)";
										}
										if($item !='null')
										{
										$sql .=  "  AND TB_ALL .ITEM IN ($item)";
										}
										$sql .=   "
												   
												  ORDER BY  
												   TB_ALL .mainCatId,
												   TB_ALL .subCatId,
												   TB_ALL .ITEM 
												    ASC";

										$result = $db->RunQuery($sql);

										if($_REQUEST['report_type']=='excel')
										{
										include "xls_item-wise_stock_forecast.php";

										return;
										}

		while($data[] = mysqli_fetch_array($result, MYSQLI_ASSOC)) {}

		$pdfLandscape->ezImage('../../../images/logo.png',5,70,800,150);
		$pdfLandscape->selectFont('../../../libraries/pdf-php/fonts/Helvetica.afm');
		//$this->ezStartPageNumbers(580,10,10,'','',1);
		$pdfLandscape->ezStartPageNumbers(580,10,10,'','',1);
		//print_r($data);

		//$cols = array('Main'=>"Main",'Sub'=>'Sub','Code'=>'Code','Name'=>'Name');
		$currencyName="Department Wise Issue Notes$dtStr";
		$pdfLandscape->ezText($currencyName,$size=0,$options=array(),$test=0);
		$pdfLandscape->ezText('',$size=0,$options=array(),$test=0);
		$pdfLandscape->ezTable($data,'','',
array('xPos'=>0,'xOrientation'=>'right','width'=>800,'fontSize' => 7
,'cols'=>array('location'=>array('justification'=>'left'),'Company'=>array('justification'=>'left'),'Issue No'=>array('justification'=>'left'),'Order No'=>array('justification'=>'left'),'Style No'=>array('justification'=>'left'),'Main Category'=>array('justification'=>'left'),'Sub Category'=>array('justification'=>'left'),'UOM'=>array('justification'=>'left'),'Item'=>array('justification'=>'left'),'Price'=>array('justification'=>'right'),'Qty'=>array('justification'=>'right'),'Amount'=>array('justification'=>'right'))));

/*		$pdf->ezTable($data,'',"PENDING PO SUMMERY$currencyName",
array('xPos'=>0,'xOrientation'=>'right','width'=>550,'fontSize' => 7
,'cols'=>array('PO Qty'=>array('justification'=>'right'),'GRN Qty'=>array('justification'=>'right'),'Return Qty'=>array('justification'=>'right'),'Balance Qty'=>array('justification'=>'right'),'Amount'=>array('justification'=>'right'))));

*/
		$pdfLandscape->ezStream();
}


else if($type=='returnItems') //excel report for orderwise stock movement suvini
    {
                        $year              = $_REQUEST['year'];
                        $order		       = $_REQUEST['order'];
                        $report_type       = $_REQUEST['report_type'];
                        $report_cat        = $_REQUEST['report_cat'];
                        $companyId         = $_SESSION["headCompanyId"];
                        $locationId        = $_SESSION["CompanyID"];
                        $head_office_loc   = get_ho_loc($companyId);
                        $pdfLandscape      = new Cezpdf('a4','landscape');

                        $sql4 = "SELECT
                        TB3.ORDER_COMPANY,
                        TB3.location AS location,
                        TB3.date,
                        TB3.ORDER_NO,
                        TB3.ORDER_YEAR,
                        TB3.SALES_ORDER,
                        TB3.SALES_ORDER_String AS salesOrderStr,
                        TB3.graphic,
                        TB3.ITEM,
                        TB3.REQUIRED,
                        TB3.TRANSACTION_LOCATION,
                        TB3.PO_QTY,
                        TB3.PRN_QTY,
                        TB3.mainCatName,
                        TB3.subCatName,
                        TB3.strCode,
                        TB3.SUP_ITEM_CODE,
                        TB3.itemName,
                        TB3.itemId AS itemId,
                        TB3.UOM,
                        TB3.type,
                        SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY,
                        SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY,
                        SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY,
                        SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_QTY,0)) as GP_QTY,
                        SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY,
                        SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY,
                        SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY_OTHER_LOC,
                        SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY_OTHER_LOC,
                        SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY_OTHER_LOC,
                        SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_QTY,0)) as GP_QTY_OTHER_LOC,
                        SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY_OTHER_LOC,
                        SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY_OTHER_LOC
                        FROM
                        (
                        SELECT
                        trn_orderheader.intLocationId AS ORDER_COMPANY,
                        TB2.ORDER_NO,
                        TB2.ORDER_YEAR,
                        TB2.location,
                        TB2.ITEM,
                        TB2.SALES_ORDER,
                        TB2.type,
                        CONCAT (trn_orderdetails.strSalesOrderNo,'/',trn_orderdetails.strCombo,'/',trn_orderdetails.strPrintName) AS SALES_ORDER_String,
                        trn_sampleinfomations.strGraphicRefNo as graphic,
                        GROUP_CONCAT(DISTINCT date(trn_orderheader_approvedby.dtApprovedDate ))AS date,
                        mst_maincategory.strName AS mainCatName,
                        mst_subcategory.strName AS subCatName,
                        mst_item.strCode,
                        mst_item.strCode as SUP_ITEM_CODE,
                        mst_item.intId AS itemId,
                        mst_item.strName AS itemName,
                        mst_units.strName AS UOM,
                        TB2.REQUIRED,
                        trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID AS TRANSACTION_LOCATION,
                        
                        (
                        SELECT
                        trn_po_prn_details.PURCHASED_QTY
                        FROM
                        trn_po_prn_details
                        WHERE
                        trn_po_prn_details.ORDER_NO = TB2.ORDER_NO
                        AND trn_po_prn_details.ORDER_YEAR = TB2.ORDER_YEAR
                        AND trn_po_prn_details.ITEM = TB2.ITEM
                        ) AS PO_QTY,
                        (
                        SELECT
                        trn_po_prn_details.PRN_QTY
                        FROM
                        trn_po_prn_details
                        WHERE
                        trn_po_prn_details.ORDER_NO = TB2.ORDER_NO
                        AND trn_po_prn_details.ORDER_YEAR = TB2.ORDER_YEAR
                        AND trn_po_prn_details.ITEM = TB2.ITEM
                        ) AS PRN_QTY,
                        SUM(
                        
                        IF (
                        trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN',
                        trn_po_prn_details_sales_order_stock_transactions.QTY,
                        0
                        )
                        ) AS MRN_QTY,
                        SUM(
                        
                        IF (
                        trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN_EXTRA',
                        trn_po_prn_details_sales_order_stock_transactions.QTY,
                        0
                        )
                        ) AS MRN_EXTRA_QTY,
                        SUM(
                        
                        IF (
                        trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'ISSUE',
                        trn_po_prn_details_sales_order_stock_transactions.QTY,
                        0
                        )
                        ) AS ISSUE_QTY,
                        SUM(
                        
                        IF (
                        trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS',
                        trn_po_prn_details_sales_order_stock_transactions.QTY,
                        0
                        )
                        ) AS GP_QTY,
                        SUM(
                        
                        IF (
                        trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS_IN',
                        trn_po_prn_details_sales_order_stock_transactions.QTY,
                        0
                        )
                        ) AS GP_IN_QTY,
                        SUM(
                        
                        IF (
                        trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'RETURNED_TO_STORES',
                        trn_po_prn_details_sales_order_stock_transactions.QTY,
                        0
                        )
                        ) AS RET_TO_STORE_QTY
                        FROM
                        (
                        SELECT
                        *
                        FROM
                        (
                        SELECT
                        trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
                        trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
                        trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
                        trn_po_prn_details_sales_order_stock_transactions.ITEM,
                        trn_po_prn_details_sales_order.REQUIRED,
                        mst_locations.intCompanyId AS ORDER_COMPANY,
                        mst_locations.strName AS location,
                        (
                        SELECT
                        GROUP_CONCAT(mst_locations.intCompanyId)
                        FROM
                        ware_fabricreceivedheader
                        INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
                        WHERE
                        ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
                        AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
                        ) AS FABRIC_IN_COMPANIES,
                        1 AS type
                        FROM
                        trn_po_prn_details_sales_order_stock_transactions
                        INNER JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
                        AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
                        AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
                        AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
                        INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
                        AND trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
                        INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
                        WHERE trn_orderheader.intOrderNo IN ($order)
                        AND trn_orderheader.intOrderYear = '$year'
                        GROUP BY
                        trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
                        trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
                        trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
                        trn_po_prn_details_sales_order_stock_transactions.ITEM
                        HAVING
                        FIND_IN_SET('$companyId', FABRIC_IN_COMPANIES)
                        OR ORDER_COMPANY = '$companyId'
                        UNION ALL
                        SELECT
                        trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
                        trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
                        trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
                        trn_po_prn_details_sales_order_stock_transactions.ITEM,
                        trn_po_prn_details_sales_order.REQUIRED,
                        mst_locations.intCompanyId AS ORDER_COMPANY,
                        mst_locations.strName AS location,
                        
                        (
                        SELECT
                        GROUP_CONCAT(
                        DISTINCT mst_locations.intCompanyId
                        )
                        FROM
                        ware_fabricreceivedheader
                        INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
                        WHERE
                        ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
                        AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
                        ) AS FABRIC_IN_COMPANIES,
                        2 AS type
                        FROM
                        trn_po_prn_details_sales_order_stock_transactions
                        LEFT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
                        AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
                        AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
                        AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
                        INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
                        AND trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
                        INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
                        WHERE
                        trn_po_prn_details_sales_order.ORDER_NO IS NULL
                        AND trn_orderheader.intOrderNo IN ($order)
                        AND trn_orderheader.intOrderYear = '$year'
                        GROUP BY
                        trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
                        trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
                        trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
                        trn_po_prn_details_sales_order_stock_transactions.ITEM
                        HAVING
                        (
                        FIND_IN_SET('$companyId', FABRIC_IN_COMPANIES)
                        OR ORDER_COMPANY = '$companyId'
                        )
                        UNION ALL
                        SELECT
                        trn_po_prn_details_sales_order.ORDER_NO,
                        trn_po_prn_details_sales_order.ORDER_YEAR,
                        trn_po_prn_details_sales_order.SALES_ORDER,
                        trn_po_prn_details_sales_order.ITEM,
                        trn_po_prn_details_sales_order.REQUIRED,
                        mst_locations.intCompanyId AS ORDER_COMPANY,
                        mst_locations.strName AS location,
                        (
                        SELECT
                        GROUP_CONCAT(
                        DISTINCT mst_locations.intCompanyId
                        )
                        FROM
                        ware_fabricreceivedheader
                        INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
                        WHERE
                        ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO
                        AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
                        ) AS FABRIC_IN_COMPANIES,
                        1 AS type
                        FROM
                        trn_po_prn_details_sales_order_stock_transactions
                        RIGHT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
                        AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
                        AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
                        AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
                        INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO
                        AND trn_orderheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
                        INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
                        WHERE
                        trn_po_prn_details_sales_order_stock_transactions.ORDER_NO IS NULL
                        AND trn_orderheader.intOrderNo IN ($order)
                        AND trn_orderheader.intOrderYear = '$year'
                        GROUP BY
                        trn_po_prn_details_sales_order.ORDER_NO,
                        trn_po_prn_details_sales_order.ORDER_YEAR,
                        trn_po_prn_details_sales_order.SALES_ORDER,
                        trn_po_prn_details_sales_order.ITEM
                        HAVING
                        (
                        FIND_IN_SET('$companyId', FABRIC_IN_COMPANIES)
                        OR ORDER_COMPANY = '$companyId'
                        )
                        ) AS TB1
                        GROUP BY
                        TB1.ORDER_NO,
                        TB1.ORDER_YEAR,
                        TB1.SALES_ORDER,
                        TB1.ITEM
                        ) AS TB2
                        LEFT JOIN trn_po_prn_details_sales_order_stock_transactions ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = TB2.ORDER_NO
                        AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = TB2.ORDER_YEAR
                        AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = TB2.SALES_ORDER
                        AND trn_po_prn_details_sales_order_stock_transactions.ITEM = TB2.ITEM
                        INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = TB2.ORDER_NO
                        AND trn_orderheader.intOrderYear = TB2.ORDER_YEAR
                        INNER JOIN trn_orderheader_approvedby ON trn_orderheader_approvedby.intOrderNo = TB2.ORDER_NO
                        AND trn_orderheader_approvedby.intYear = TB2.ORDER_YEAR 
                        AND trn_orderheader_approvedby.intStatus = 0
                        AND trn_orderheader_approvedby.intApproveLevelNo = 1
                        LEFT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order.ORDER_NO = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
                        AND trn_po_prn_details_sales_order.ORDER_YEAR = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
                        AND trn_po_prn_details_sales_order.SALES_ORDER = trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER
                        AND trn_po_prn_details_sales_order.ITEM = trn_po_prn_details_sales_order_stock_transactions.ITEM
                        INNER JOIN trn_orderdetails ON TB2.ORDER_NO = trn_orderdetails.intOrderNo
                        AND TB2.ORDER_YEAR = trn_orderdetails.intOrderYear
                        AND TB2.SALES_ORDER = trn_orderdetails.intSalesOrderId
                        INNER JOIN trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
                        INNER JOIN mst_item ON TB2.ITEM = mst_item.intId
                        LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
                        INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
                        INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
                        INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId	
                        GROUP BY
                        TB2.ORDER_NO,
                        TB2.ORDER_YEAR,
                        TB2.SALES_ORDER,
                        TB2.ITEM,
                        trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID
                        ) AS TB3
                        GROUP BY
                        TB3.ORDER_NO,
                        TB3.ORDER_YEAR,
                        TB3.SALES_ORDER,
                        TB3.ITEM 
                        ORDER BY 
                        TB3.ORDER_NO,
                        TB3.ORDER_YEAR,
                        TB3.ITEM,
                        TB3.SALES_ORDER 
    ";


    $result = $db->RunQuery($sql4);

    if($_REQUEST['report_type']=='excel')
    {
    include "xls_order-wise_stock_movement.php";
    return;
    }

            while($data[] = mysqli_fetch_array($result, MYSQLI_ASSOC)) {}

            $pdfLandscape->ezImage('../../../images/logo.png',5,70,800,150);
            $pdfLandscape->selectFont('../../../libraries/pdf-php/fonts/Helvetica.afm');
            //$this->ezStartPageNumbers(580,10,10,'','',1);
            $pdfLandscape->ezStartPageNumbers(580,10,10,'','',1);
            //print_r($data);

            //$cols = array('Main'=>"Main",'Sub'=>'Sub','Code'=>'Code','Name'=>'Name');
            $currencyName="Department Wise Issue Notes$dtStr";
            $pdfLandscape->ezText($currencyName,$size=0,$options=array(),$test=0);
            $pdfLandscape->ezText('',$size=0,$options=array(),$test=0);
            $pdfLandscape->ezTable($data,'','',
    array('xPos'=>0,'xOrientation'=>'right','width'=>800,'fontSize' => 7
    ,'cols'=>array('location'=>array('justification'=>'left'),'Company'=>array('justification'=>'left'),'Issue No'=>array('justification'=>'left'),'Order No'=>array('justification'=>'left'),'Style No'=>array('justification'=>'left'),'Main Category'=>array('justification'=>'left'),'Sub Category'=>array('justification'=>'left'),'UOM'=>array('justification'=>'left'),'Item'=>array('justification'=>'left'),'Price'=>array('justification'=>'right'),'Qty'=>array('justification'=>'right'),'Amount'=>array('justification'=>'right'))));

    /*		$pdf->ezTable($data,'',"PENDING PO SUMMERY$currencyName",
    array('xPos'=>0,'xOrientation'=>'right','width'=>550,'fontSize' => 7
    ,'cols'=>array('PO Qty'=>array('justification'=>'right'),'GRN Qty'=>array('justification'=>'right'),'Return Qty'=>array('justification'=>'right'),'Balance Qty'=>array('justification'=>'right'),'Amount'=>array('justification'=>'right'))));

    */
            $pdfLandscape->ezStream();
        }
else if($type=='departmentWiseIssues')
	{
	$pdfLandscape = new Cezpdf('a4','landscape');

		$company = $_REQUEST['cboCompany'];
		$location = $_REQUEST['cboLocation'];
		$department = $_REQUEST['cboDepartment'];
		$mainCat = $_REQUEST['cboMainCategory'];
		$subCat = $_REQUEST['cboSubCategory'];
		$item = $_REQUEST['cboItem'];
		$currencyId = $_REQUEST['cboCurrency'];
		$checkDate	= $_REQUEST['chkDate'];

		$dtFrom = $_REQUEST['dtFromDate6'];
		$dtTo = $_REQUEST['dtToDate6'];
		$date = date("Y-m-d");

		//have to confirm from rakshitha and anable below code
		/*if($dtFrom=='' && $dtTo=='')
		{
			$dtFrom = date('d-m-Y', strtotime("-1 year"));
			$dtTo	= date("Y-m-d");
		}*/


		// $date = "2012-09-25";
		$dtStr='';

		if(($dtFrom!='') || ($dtTo!='')){
			$dtStr.=" - ( ";
		}

		if($dtFrom!=''){
			$dtStr.="Date From : ".$dtFrom;
		}
		if($dtTo!=''){
			$dtStr .=" Date To : ".$dtTo;
		}
		if(($dtFrom!='') || ($dtTo!='')){
		$dtStr .=")";
		}

		$locationName= loadLocationName($location);
		$departmentName= loadDepartmentName($department);

		if($currencyId=='')
		$currencyId = 2;

		$sql = "SELECT
					mst_financeexchangerate.dblBuying, 
					mst_financecurrency.strCode as currency 
				FROM
					mst_financeexchangerate 
					Inner Join mst_financecurrency ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId
				WHERE
					mst_financeexchangerate.intCurrencyId =  '$currencyId' AND
					mst_financeexchangerate.intCompanyId =  '$company'
				ORDER BY
					mst_financeexchangerate.dtmDate DESC
				LIMIT 1
				";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);

		$currency = $row['currency'];
		$toCurrencyRate = $row['dblBuying'];

		if($toCurrencyRate<=0 || $toCurrencyRate<='')
			$toCurrencyRate=1;


		if($currencyId=='')
			$para1 = ' mst_financecurrency.strCode';
		else
			$para1 = $currency;

		if($currencyId==''){
			$para2 = 1;
			$toCurrencyRate=1;
		}
		else
			$para2 = 'ware_grnheader.dblExchangeRate';


			$para3 = "(SELECT
					mst_financeexchangerate.dblBuying
				FROM
					mst_financeexchangerate 
					Inner Join mst_financecurrency ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId
				WHERE
					mst_financeexchangerate.intCurrencyId =  ware_stocktransactions_bulk.intCurrencyId AND
					mst_financeexchangerate.intCompanyId =  '$company' 
					AND mst_financeexchangerate.dtmDate = ware_stocktransactions_bulk.dtGRNDate 
				ORDER BY
					mst_financeexchangerate.dtmDate DESC
				LIMIT 1)";


		$sql = " (
				

SELECT 
				mst_department.strName as Department,
				mst_companies.strName AS Company , 
				mst_locations.strName AS Location,
				concat(tb1.intIssueNo,'/',tb1.intIssueYear) as `Issue No`,
								     if(tb1.strOrderNo=0,'',concat(tb1.strOrderNo,'/',tb1.intOrderYear)) as `Order No`,
				tb1.strStyleNo as `Style No`,
				mst_maincategory.strName as `Main Category`,
				mst_subcategory.strName as `Sub Category`,
				mst_item.strName as `Item` , 
				mst_units.strCode as `UOM` , ";

				/*if($currencyId=='')
				$sql .="$para1 as Currency,";
				else
				$sql .="'$para1' as Currency,";
				*/
				$sql .=" (SELECT mst_financecurrency.strCode FROM mst_financecurrency WHERE mst_financecurrency.intId = '$currencyId') AS Currency , ";

			$sql .= "(SELECT
						ROUND(SUM((IF(ware_grnheader.intGrnNo ='*',
						$para2,
						(SELECT
						mst_financeexchangerate.dblBuying
						FROM
						mst_financeexchangerate
						INNER JOIN
						mst_financecurrency ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId
						WHERE
						mst_financeexchangerate.intCurrencyId = ware_stocktransactions_bulk.intCurrencyId
						AND mst_financeexchangerate.intCompanyId = '$company' 
						AND mst_financeexchangerate.dtmDate = ware_stocktransactions_bulk.dtGRNDate 
						ORDER BY mst_financeexchangerate.dtmDate DESC
						LIMIT 1)) * ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate * - 1) / (SELECT
					mst_financeexchangerate.dblBuying 
				FROM
					mst_financeexchangerate 
					Inner Join mst_financecurrency ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId
				WHERE
					mst_financeexchangerate.intCurrencyId =  '$currencyId' AND
					mst_financeexchangerate.intCompanyId =  '$company' 
					AND mst_financeexchangerate.dtmDate = ware_stocktransactions_bulk.dtGRNDate 
				ORDER BY
					mst_financeexchangerate.dtmDate DESC
				LIMIT 1)),
						2)
						FROM
						ware_stocktransactions_bulk
						LEFT JOIN
						ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo
						AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear
						WHERE
						ware_stocktransactions_bulk.intDocumentNo = tb1.intIssueNo
						AND ware_stocktransactions_bulk.intDocumntYear = tb1.intIssueYear
						AND ware_stocktransactions_bulk.intItemId = tb1.intItemId
						AND ware_stocktransactions_bulk.strType = 'ISSUE') AS Amount,
						
						((SELECT
						ROUND(SUM(ware_stocktransactions_bulk.dblQty * - 1),
						2)
						FROM
						ware_stocktransactions_bulk
						WHERE
						ware_stocktransactions_bulk.intDocumentNo = tb1.intIssueNo
						AND ware_stocktransactions_bulk.intDocumntYear = tb1.intIssueYear
						AND ware_stocktransactions_bulk.intItemId = tb1.intItemId
						AND ware_stocktransactions_bulk.strType = 'ISSUE')) AS Qty,
						
						ROUND(('Amount'/'Qty'),4) AS Price	
	  					   
						FROM
						ware_issuedetails  as tb1 
						Inner Join ware_mrnheader ON tb1.intMrnNo = ware_mrnheader.intMrnNo AND tb1.intMrnYear = ware_mrnheader.intMrnYear
						Inner Join mst_item ON tb1.intItemId = mst_item.intId
						Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
						Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
						Inner Join ware_issueheader ON tb1.intIssueNo = ware_issueheader.intIssueNo AND tb1.intIssueYear = ware_issueheader.intIssueYear 
						left Join ware_issueheader_approvedby as ap ON ap.intIssueNo = ware_issueheader.intIssueNo AND ap.intYear = ware_issueheader.intIssueYear AND ap.intApproveLevelNo= ware_issueheader.intApproveLevels AND ap.intStatus=0 
						Inner Join mst_locations ON ware_issueheader.intCompanyId = mst_locations.intId  
						Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId 
						inner join mst_department ON mst_department.intId = ware_mrnheader.intDepartment 
						Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
						Inner Join mst_financecurrency ON mst_item.intCurrency = mst_financecurrency.intId 
						WHERE
						ware_issueheader.intStatus =  '1' AND
						mst_locations.intCompanyId =  '$company' ";

			if($location!='')
			$sql .= " AND ware_issueheader.intCompanyId =  '$location' ";
			if($department!='')
			$sql .= " AND ware_mrnheader.intDepartment =  '$department' ";

			if($mainCat!='')
			$sql .= " AND mst_item.intMainCategory =  '$mainCat' ";
			if($subCat!='')
			$sql .= " AND mst_item.intSubCategory =  '$subCat' ";
			if($item!='')
			$sql .= " AND mst_item.intId =  '$item' ";


			if($dtFrom!='')
			$sql .= " AND ware_issueheader.datdate>='$dtFrom' ";
			if($dtTo!='')
			$sql .= " AND ware_issueheader.datdate<='$dtTo' ";
			$sql .= "  
				GROUP BY
				tb1.intIssueNo,
				tb1.intIssueYear,
				mst_item.intId 
				Order By 
				tb1.intIssueNo asc,
				tb1.intIssueYear asc,
				tb1.intOrderYear asc,
				tb1.strOrderNo asc,
				tb1.strStyleNo asc,
				mst_maincategory.strName asc,
				mst_subcategory.strName asc,
				mst_item.strName asc )";
		//echo $sql;
		//die();
		$result = $db->RunQuery($sql);
		if($_REQUEST['report_type']=='excel')
		{
			include "xls_department_wise_issue_notes.php";
			return;
		}

		while($data[] = mysqli_fetch_array($result, MYSQLI_ASSOC)) {}

		$pdfLandscape->ezImage('../../../images/logo.png',5,70,800,150);
		$pdfLandscape->selectFont('../../../libraries/pdf-php/fonts/Helvetica.afm');
		//$this->ezStartPageNumbers(580,10,10,'','',1);
		$pdfLandscape->ezStartPageNumbers(580,10,10,'','',1);
		//print_r($data);

		//$cols = array('Main'=>"Main",'Sub'=>'Sub','Code'=>'Code','Name'=>'Name');
		$currencyName="Department Wise Issue Notes$dtStr";
		$pdfLandscape->ezText($currencyName,$size=0,$options=array(),$test=0);
		$pdfLandscape->ezText('',$size=0,$options=array(),$test=0);
		$pdfLandscape->ezTable($data,'','',
array('xPos'=>0,'xOrientation'=>'right','width'=>800,'fontSize' => 7
,'cols'=>array('location'=>array('justification'=>'left'),'Company'=>array('justification'=>'left'),'Issue No'=>array('justification'=>'left'),'Order No'=>array('justification'=>'left'),'Style No'=>array('justification'=>'left'),'Main Category'=>array('justification'=>'left'),'Sub Category'=>array('justification'=>'left'),'UOM'=>array('justification'=>'left'),'Item'=>array('justification'=>'left'),'Price'=>array('justification'=>'right'),'Qty'=>array('justification'=>'right'),'Amount'=>array('justification'=>'right'))));

/*		$pdf->ezTable($data,'',"PENDING PO SUMMERY$currencyName",
array('xPos'=>0,'xOrientation'=>'right','width'=>550,'fontSize' => 7
,'cols'=>array('PO Qty'=>array('justification'=>'right'),'GRN Qty'=>array('justification'=>'right'),'Return Qty'=>array('justification'=>'right'),'Balance Qty'=>array('justification'=>'right'),'Amount'=>array('justification'=>'right'))));

*/
		$pdfLandscape->ezStream();
	}
else if($type=='locationItemsTransactions')
	{
		$company = $_REQUEST['cboCompany'];
		$location = $_REQUEST['cboLocation'];
		$mainCat = $_REQUEST['cboMainCategory'];
		$subCat = $_REQUEST['cboSubCategory'];
		$item = $_REQUEST['cboItem'];
		$dtFrom = $_REQUEST['dtFromDate9'];
		$dtTo = $_REQUEST['dtToDate9'];
		$date = date("Y-m-d");
		// $date = "2012-09-25";
		$dtStr='';

		if(($dtFrom!='') || ($dtTo!='')){
			$dtStr.=" - (";
		}

		if($dtFrom!=''){
			$dtStr.=" Date From : ".$dtFrom;
		}
		if($dtTo!=''){
			$dtStr .=" Date To : ".$dtTo;
		}
		if(($dtFrom!='') || ($dtTo!='')){
		$dtStr .=")";
		}

		$locationName= loadLocationName($location);
		$departmentName= loadDepartmentName($department);




		$sql = "SELECT distinct
				mst_companies.strName AS Company , 
				mst_locations.strName AS Location,
				concat(ware_stocktransactions_bulk.intDocumentNo,'/',ware_stocktransactions_bulk.intDocumntYear) as `Document No`,
				mst_maincategory.strName as `Main Category`,
				mst_subcategory.strName as `Sub Category`,
				mst_item.strName as `Item` , 
				mst_units.strCode as `UOM` ,
				ware_stocktransactions_bulk.dtDate as Date, 
				
				IF(ware_stocktransactions_bulk.strType='GRN','GRN',
				IF(ware_stocktransactions_bulk.strType='ISSUE','ISSUE',
				IF(ware_stocktransactions_bulk.strType='GATEPASS','GATE PASS',
				IF(ware_stocktransactions_bulk.strType='RETSTORES','RETURN TO STORES',
				IF(ware_stocktransactions_bulk.strType='RTSUP','RETURN TO SUPPLIER',
				IF(ware_stocktransactions_bulk.strType='TRANSFERIN','GATE PASS TRANSFER IN',
				IF(ware_stocktransactions_bulk.strType='ALLOCATE','STYLE ALLOCATE',
				IF(ware_stocktransactions_bulk.strType='UNALLOCATE','STYLE UN-ALLOCATE',
				IF(ware_stocktransactions_bulk.strType='INVOICE','INVOICE',
				IF(ware_stocktransactions_bulk.strType='RTSUP_RCV','RETURN INVOICE',
				IF(ware_stocktransactions_bulk.strType='Adjust Q+','STOCK ADJUSTMENT+',
				IF(ware_stocktransactions_bulk.strType='Adjust R+','STOCK ADJUSTMENT+',
				IF(ware_stocktransactions_bulk.strType='Adjust RQ+','STOCK ADJUSTMENT+',
				IF(ware_stocktransactions_bulk.strType='Adjust Q-','STOCK ADJUSTMENT-',
				IF(ware_stocktransactions_bulk.strType='ITEMCREATION-','ALLOCATE TO CREATE ITEM',
				IF(ware_stocktransactions_bulk.strType='ITEMCREATION+','CREATE NEW',ware_stocktransactions_bulk.strType 
				))))))))))))))))
				as Transaction, 
				ware_stocktransactions_bulk.dblGRNRate as Price, 
				mst_financecurrency.strCode as Currency, 
				abs(ware_stocktransactions_bulk.dblQty) as Qty   
				FROM
				ware_stocktransactions_bulk
				Inner Join mst_item ON ware_stocktransactions_bulk.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				Inner Join mst_locations ON ware_stocktransactions_bulk.intLocationId = mst_locations.intId  
				Inner Join mst_companies ON ware_stocktransactions_bulk.intCompanyId = mst_companies.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				Inner Join mst_financecurrency ON ware_stocktransactions_bulk.intCurrencyId = mst_financecurrency.intId
				WHERE
				ware_stocktransactions_bulk.intLocationId =  '$location' AND
				ware_stocktransactions_bulk.intCompanyId =  '$company' ";

			if($item!='')
			$sql .= " AND ware_stocktransactions_bulk.intItemId =  '$item' ";


			if($dtFrom!='')
			$sql .= " AND date(ware_stocktransactions_bulk.dtDate) >='$dtFrom' ";
			if($dtTo!='')
			$sql .= " AND date(ware_stocktransactions_bulk.dtDate) <='$dtTo' ";
			$sql .= "  
				GROUP BY
				ware_stocktransactions_bulk.intLocationId,
				ware_stocktransactions_bulk.intCompanyId,
				ware_stocktransactions_bulk.intDocumentNo,
				ware_stocktransactions_bulk.intDocumntYear,
				ware_stocktransactions_bulk.strType,
				ware_stocktransactions_bulk.intItemId , 
				mst_item.intId 
				Order By 
				mst_item.strName asc, 
				ware_stocktransactions_bulk.dtDate asc
				"
				 ;

			 //  echo $sql;
		$result = $db->RunQuery($sql);
		while($data[] = mysqli_fetch_array($result, MYSQLI_ASSOC)) {}

		$pdf->ezImage('../../../images/logo.jpg',5,70,800,150);
		$pdf->selectFont('../../../libraries/pdf-php/fonts/Helvetica.afm');
		//$this->ezStartPageNumbers(580,10,10,'','',1);
		$pdf->ezStartPageNumbers(580,10,10,'','',1);
		//print_r($data);

		//$cols = array('Main'=>"Main",'Sub'=>'Sub','Code'=>'Code','Name'=>'Name');
		$currencyName = "Location Wise Transactions$dtStr";
		$pdf->ezText($currencyName,$size=0,$options=array(),$test=0);
		$pdf->ezText('',$size=0,$options=array(),$test=0);

		$pdf->ezTable($data,'','',
array('xPos'=>0,'xOrientation'=>'right','width'=>550,'fontSize' => 7
,'cols'=>array('location'=>array('justification'=>'left'),'Company'=>array('justification'=>'left'),'Issue No'=>array('justification'=>'left'),'Main Category'=>array('justification'=>'left'),'Sub Category'=>array('justification'=>'left'),'UOM'=>array('justification'=>'left'),'Price'=>array('justification'=>'right'),'Item'=>array('justification'=>'left'),'Transaction'=>array('justification'=>'left'),'Date'=>array('justification'=>'left'),'Qty'=>array('justification'=>'right'),'Currency'=>array('justification'=>'right'))));

/*		$pdf->ezTable($data,'',"PENDING PO SUMMERY$currencyName",
array('xPos'=>0,'xOrientation'=>'right','width'=>550,'fontSize' => 7
,'cols'=>array('PO Qty'=>array('justification'=>'right'),'GRN Qty'=>array('justification'=>'right'),'Return Qty'=>array('justification'=>'right'),'Balance Qty'=>array('justification'=>'right'),'Amount'=>array('justification'=>'right'))));

*/
		$pdf->ezStream();
}

else if($type=='InternalOnloanTrnsActions_loanIn')
{
	ini_set("session.auto_start", 0);
	$rptType= $_REQUEST['cboRptType'];
	$company = $_REQUEST['cboCompany'];
	$location = $_REQUEST['cboLocation'];
	$mainCat = $_REQUEST['cboMainCategory'];
	$subCat = $_REQUEST['cboSubCategory'];
	$item = $_REQUEST['cboItem'];
	$dtFrom = $_REQUEST['dtFromDate10'];
	$dtTo = $_REQUEST['dtToDate10'];
	$date = date("Y-m-d");
	// $date = "2012-09-25";
	$dtStr='';

	if(($dtFrom!='') || ($dtTo!='')){
		$dtStr.=" - (";
	}

	if($dtFrom!=''){
		$dtStr.=" Date From : ".$dtFrom;
	}
	if($dtTo!=''){
		$dtStr .=" Date To : ".$dtTo;
	}
	if(($dtFrom!='') || ($dtTo!='')){
	$dtStr .=")";
	}

	$sql="SELECT
mst_companies.strName
FROM mst_companies
WHERE
mst_companies.intId =  '$company'";
	$result=$db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	$comp=$row["strName"];

	$sql="SELECT
mst_locations.strName
FROM
mst_locations
WHERE
mst_locations.intId =  '$location'";
	$result=$db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	$loc=$row["strName"];

	$sql="SELECT
mst_item.strName
FROM
mst_item
WHERE
mst_item.intId =  '$item'
";
	$result=$db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	$itm=$row["strName"];
class PDF extends FPDF
{
	function Header()
	{
		global $rowCount;
		global $title;
		global $dtTo;
		global $dtFrom;
		global $comp;
		global $loc;
		global $itm;


		$h = 5;
		$this->SetFont('Times','B',9);
		$this->Cell(140,$h,$title,'0',0,'L');
		$this->SetFont('Times','',9);
		$this->Cell(120,$h,"PAGE NO",'0',0,'R');
		$this->Cell(0,$h,$this->PageNo(),'1',1,'C');
		$this->SetFont('Times','B',9);
		$this->setX(100);
		$this->Cell(140,$h,"INTERNAL ONLOAN TRANSACTION - LOAN IN",'0',0,'L');
		$this->SetFont('Times','B',9);
		$this->setXY(20,20);
		$this->Cell(340,$h,"(Company - ".$comp."/ Location - ".$loc."/ Item - ".$itm."/ Date From - ".$dtFrom." To - ".$dtTo." )",'0',0,'L');
		$this->SetFont('Times','B',9);
		$this->Table_header1(10,25,215);
		$this->Table_header2(235,25,60);
		$this->Table_header3();
		$this->SetFont('Times','',9);
	}

	function CreateTable($result,$rowCount,$fieldCount)
	{
		$count 			= 3;
		$booFirst		= true;

/*		$this->Table_header1(10,25,215);
		$this->Table_header2(235,25,60);
		$this->Table_header3();
*/		$temp='';
		$itemTmp='';
		$flg=0;

		while($row=mysqli_fetch_array($result))
		{
			if(($temp==$row['grnNo']) && ($itemTmp==$row['item'])){
				$flg=1;
			}
			else{
				$flg=0;
			}


			$this->Table_Body($row,$count,$flg);

			if($count == 30){

				//$this->Table_Footer();
				$this->AddPage();


				$count = 4;
				$booFirst = false;
			}
			if(($temp==$row['grnNo']) && ($itemTmp==$row['item'])){
				$count++;
			}
			else{
				$count=$count+2;
			}

			$temp=$row['grnNo'];
			$itemTmp=$row['item'];

		}
			// echo $count;

		// $this->Table_Footer();
	}

	function Table_header1($x,$y,$l)
	{
		global $obj_getData;
		global $periodArray;
		$this->SetY($y);
		$this->SetX($x);
		$h = 4;
		$this->SetFont('Times','B',8);
		$this->Cell($l,12,"Loan In",'1',0,'C');

		$this->Ln();

	}

	function Table_header2($x,$y,$l)
	{
		global $obj_getData;
		global $periodArray;
		$this->SetY($y);
		$this->SetX($x);
		$h = 4;
		$this->SetFont('Times','B',8);
		$this->Cell($l,12,"Loan Settelement",'1',0,'C');

		$this->Ln();

	}

	function Table_header3()
	{
		global $obj_getData;
		global $periodArray;
		$h = 4;
		$this->SetFont('Times','B',8);
		$this->Cell(15,$h,'Rcv Date','1',0,'L');
		$this->Cell(50,$h,'Company','1',0,'L');
		$this->Cell(20,$h,'GRN No','1',0,'L');
		$this->Cell(90,$h,'Item','1',0,'L');
		$this->Cell(10,$h,'UOM','1',0,'L');
		$this->Cell(15,$h,'Qty','1',0,'L');
		$this->Cell(15,$h,'Value','1',0,'L');
		$this->SetX(235);
		$this->Cell(15,$h,'Date','1',0,'L');
	//	$this->Cell(25,$h,'Company','1',0,'L');
		$this->Cell(20,$h,'Return No','1',0,'L');
	//	$this->Cell(30,$h,'Item','1',0,'L');
	//	$this->Cell(15,$h,'UOM','1',0,'L');
		$this->Cell(10,$h,'Qty','1',0,'L');
		$this->Cell(15,$h,'Value','1',0,'L');

		$this->Ln();

	}


	function Table_Body($row,$count,$dup)
	{

		//------

		if(($dup!=1) && ($count!=1)){
			$h	= 4.4;
			$this->SetFont('Times','',7);

			$this->Cell(15,$h,'','1',0,'L');
			$this->Cell(50,$h,'','1',0,'L');
			$this->Cell(20,$h,'','1',0,'L');
			$this->Cell(90,$h,'','1',0,'L');
			$this->Cell(10,$h,'','1',0,'L');
			$this->Cell(15,$h,'','1',0,'L');
			$this->Cell(15,$h,'','1',0,'L');

			$this->SetX(235);

			$this->Cell(15,$h,'','1',0,'L');
		//	$this->Cell(25,$h,'','1',0,'L');
			$this->Cell(20,$h,'','1',0,'L');
		//	$this->Cell(30,$h,'','1',0,'L');
		//	$this->Cell(15,$h,'','1',0,'L');
			$this->Cell(10,$h,'','1',0,'L');
			$this->Cell(15,$h,'','1',0,'L');
			$this->Ln();
		}
		//------

		$h	= 4.4;
		$this->SetFont('Times','',7);

		if($dup==1){
		$this->Cell(15,$h,'','1',0,'L');
		$this->Cell(50,$h,'','1',0,'L');
		$this->Cell(20,$h,'','1',0,'L');
		$this->Cell(90,$h,'','1',0,'L');
		$this->Cell(10,$h,'','1',0,'L');
		$this->Cell(15,$h,'','1',0,'L');
		$this->Cell(15,$h,'','1',0,'L');
		}
		else{
		$this->Cell(15,$h,$row["grnDate"],'1',0,'L');
		$this->Cell(50,$h,$row["grnCompany"],'1',0,'L');
		$this->Cell(20,$h,$row["grnNo"],'1',0,'L');
		$this->Cell(90,$h,$row["item"],'1',0,'L');
		$this->Cell(10,$h,$row["uom"],'1',0,'L');
		$this->Cell(15,$h,$row["dblGrnQty"],'1',0,'L');
		$this->Cell(15,$h,$row["dblGrnQty"]*$row["dblGrnRate"],'1',0,'L');
		}

		$this->SetX(235);

		if($row["retNo"]==''){
		$this->Cell(15,$h,'','1',0,'L');
	//	$this->Cell(25,$h,'','1',0,'L');
		$this->Cell(20,$h,'','1',0,'L');
	//	$this->Cell(30,$h,'','1',0,'L');
	//	$this->Cell(15,$h,'','1',0,'L');
		$this->Cell(10,$h,'','1',0,'L');
		$this->Cell(15,$h,'','1',0,'L');
		}
		else{
		$this->Cell(15,$h,$row["returnDate"],'1',0,'L');
	//	$this->Cell(25,$h,$row["grnCompany"],'1',0,'L');
		$this->Cell(20,$h,$row["retNo"],'1',0,'L');
	//	$this->Cell(30,$h,$row["item"],'1',0,'L');
	//	$this->Cell(15,$h,$row["uom"],'1',0,'L');
		$this->Cell(10,$h,$row["retQty"],'1',0,'L');
		$this->Cell(15,$h,$row["dblGrnRate"]*$row["retQty"],'1',0,'L');
		}


		$this->Ln();


	}

	function Table_Footer()
	{
		$h	= 6;
		$this->SetFont('Times','B',7);

		$this->Cell(91,$h,"SUB TOTAL - PAGE ( ".$this->PageNo()." )",'1',0,'C');
		$this->Cell(24,$h,number_format($totContri,2),'1',0,'R');
		$this->Ln();
	}

	function Footer()
	{

	}

}



///////////////////////////////////////

class PDF2 extends FPDF
{
	function Header()
	{
		global $rowCount;
		global $title;
		global $dtTo;
		global $dtFrom;
		global $comp;
		global $loc;
		global $itm;
		$h = 5;
		$this->SetFont('Times','B',9);
		$this->Cell(140,$h,$title,'0',0,'L');
		$this->SetFont('Times','',9);
		$this->Cell(120,$h,"PAGE NO",'0',0,'R');
		$this->Cell(0,$h,$this->PageNo(),'1',1,'C');
		$this->SetFont('Times','B',9);
		$this->setX(100);
		$this->Cell(140,$h,"INTERNAL ONLOAN TRANSACTION - LOAN OUT",'0',0,'L');
		$this->SetFont('Times','B',9);
		$this->setXY(20,20);
		$this->Cell(340,$h,"(Company - ".$comp."/ Location - ".$loc."/ Item - ".$itm."/ Date From - ".$dtFrom." To - ".$dtTo." )",'0',0,'L');
		$this->SetFont('Times','',9);
		$this->Table_header1(10,25,210);
		$this->Table_header2(230,25,65);
		$this->Table_header3(10,25,135);
		$this->SetFont('Times','',9);
	}

	function CreateTable($result,$rowCount,$fieldCount)
	{

		$count 			= 3;
		$booFirst		= true;

		$temp='';
		$itemTmp='';
		$flg=0;
		while($row=mysqli_fetch_array($result))
		{
			if(($row['rcds']>1 && ($row['intReturnNo']==''))){

			}
			else{
			if(($temp==$row['intPONo']) && ($itemTmp==$row['item'])){
				$flg=1;
			}
			else{
				$flg=0;
			}
			$this->Table_Body($row,$count,$flg);
			if($count == 30){
				//$this->Table_Footer();
				$this->AddPage();
				//$this->Table_header3();
				$count = 4;
				$booFirst = false;
			}

			if(($temp==$row['intPONo']) && ($itemTmp==$row['item'])){
				$count++;
			}
			else{
				$count=$count+2;
			}
			$temp=$row['intPONo'];
			$itemTmp=$row['item'];
		}
		}
		//$this->Table_Footer();
	}

	function Table_header1($x,$y,$l)
	{
		global $obj_getData;
		global $periodArray;
		$this->SetY($y);
		$this->SetX($x);
		$h = 4;
		$this->SetFont('Times','B',8);
		$this->Cell($l,12,"Loan Out",'1',0,'C');

		$this->Ln();

	}

	function Table_header2($x,$y,$l)
	{
		global $obj_getData;
		global $periodArray;
		$this->SetY($y);
		$this->SetX($x);
		$h = 4;
		$this->SetFont('Times','B',8);
		$this->Cell($l,12,"Loan Settelement",'1',0,'C');

		$this->Ln();

	}

	function Table_header3($x,$y,$l)
	{
		global $obj_getData;
		global $periodArray;
		$h = 4;
		$this->SetFont('Times','B',8);
		$this->Cell(15,$h,'Sent Date','1',0,'L');
		$this->Cell(50,$h,'Company','1',0,'L');
		$this->Cell(20,$h,'PO No','1',0,'L');
		$this->Cell(80,$h,'Item','1',0,'L');
		$this->Cell(15,$h,'UOM','1',0,'L');
		$this->Cell(15,$h,'Qty','1',0,'L');
		$this->Cell(15,$h,'Value','1',0,'L');
		$this->SetX(230);
		$this->Cell(15,$h,'Date','1',0,'L');
	//	$this->Cell(25,$h,'Company','1',0,'L');
		$this->Cell(20,$h,'Return No','1',0,'L');
	//	$this->Cell(30,$h,'Item','1',0,'L');
	//	$this->Cell(15,$h,'UOM','1',0,'L');
		$this->Cell(15,$h,'Qty','1',0,'L');
		$this->Cell(15,$h,'Value','1',0,'L');

		$this->Ln();

	}


	function Table_Body($row,$count,$dup)
	{

		//------

		if(($dup!=1) && ($count!=1)){
			$h	= 4.4;
			$this->SetFont('Times','',7);

			$this->Cell(15,$h,'','1',0,'L');
			$this->Cell(50,$h,'','1',0,'L');
			$this->Cell(20,$h,'','1',0,'L');
			$this->Cell(80,$h,'','1',0,'L');
			$this->Cell(15,$h,'','1',0,'L');
			$this->Cell(15,$h,'','1',0,'L');
			$this->Cell(15,$h,'','1',0,'L');

			$this->SetX(230);

			$this->Cell(15,$h,'','1',0,'L');
		//	$this->Cell(25,$h,'','1',0,'L');
			$this->Cell(20,$h,'','1',0,'L');
		//	$this->Cell(30,$h,'','1',0,'L');
		//	$this->Cell(15,$h,'','1',0,'L');
			$this->Cell(15,$h,'','1',0,'L');
			$this->Cell(15,$h,'','1',0,'L');
			$this->Ln();
		}
		//------


		$h	= 4.4;
		$this->SetFont('Times','',7);

		if($dup==1){
		$this->Cell(15,$h,'','1',0,'L');
		$this->Cell(50,$h,'','1',0,'L');
		$this->Cell(20,$h,'','1',0,'L');
		$this->Cell(80,$h,'','1',0,'L');
		$this->Cell(15,$h,'','1',0,'L');
		$this->Cell(15,$h,'','1',0,'L');
		$this->Cell(15,$h,'','1',0,'L');
		}
		else{
		$this->Cell(15,$h,$row["dtmPODate"],'1',0,'L');
		$this->Cell(50,$h,$row["poCompany"],'1',0,'L');
		$this->Cell(20,$h,$row["intPONo"]."/".$row["intPOYear"],'1',0,'L');
		$this->Cell(80,$h,$row["item"],'1',0,'L');
		$this->Cell(15,$h,$row["uom"],'1',0,'L');
		$this->Cell(15,$h,$row["poQty"],'1',0,'L');
		$this->Cell(15,$h,$row["poQty"]*$row["dblGRNRate"],'1',0,'L');
		}

		$this->SetX(230);

		if($row["intReturnNo"]==''){
		$this->Cell(15,$h,'','1',0,'L');
		//$this->Cell(25,$h,'','1',0,'L');
		$this->Cell(20,$h,'','1',0,'L');
		//$this->Cell(30,$h,'','1',0,'L');
		//$this->Cell(15,$h,'','1',0,'L');
		$this->Cell(15,$h,'','1',0,'L');
		$this->Cell(15,$h,'','1',0,'L');
		}
		else{
		$this->Cell(15,$h,$row["datdate"],'1',0,'L');
	//	$this->Cell(25,$h,$row["poCompany"],'1',0,'L');
		$this->Cell(20,$h,$row["intReturnNo"]."/".$row["intReturnYear"],'1',0,'L');
/*		$this->Cell(30,$h,$row["item"],'1',0,'L');
		$this->Cell(15,$h,$row["uom"],'1',0,'L');
*/		$this->Cell(15,$h,$row["dblQty"],'1',0,'L');
		$this->Cell(15,$h,$row["dblGRNRate"]*$row["dblQty"],'1',0,'L');
		}


		$this->Ln();

	}

	function Table_Footer()
	{
		$h	= 6;
		$this->SetFont('Times','B',7);

		$this->Cell(91,$h,"SUB TOTAL - PAGE ( ".$this->PageNo()." )",'1',0,'C');
		$this->Cell(24,$h,number_format($totContri,2),'1',0,'R');
		$this->Ln();
	}

	function Footer()
	{

	}
}

//////////////////////////////

		if($rptType==1){
		$pdf 		= new PDF('L','mm','A4');
		$result 	= GetMainDetails_loanIn($company,$location,$item,$dtFrom,$dtTo);
		$rowCount 	= mysqli_num_rows($result);
		$fieldCount = mysqli_num_fields($result);

		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->CreateTable($result,$rowCount,$fieldCount);

		$pdf->Output('','I');
		}
		else{
		$pdf2 		= new PDF2('L','mm','A4');
		$result 	= GetMainDetails_loanOut($company,$location,$item,$dtFrom,$dtTo);
		$rowCount 	= mysqli_num_rows($result);
		$fieldCount = mysqli_num_fields($result);

		$pdf2->AliasNbPages();
		$pdf2->AddPage();
		$pdf2->CreateTable($result,$rowCount,$fieldCount);

		$pdf2->Output('','I');
		}
///////////////////////////////
	}

else if($type=='Locations')
{
	$sql = "SELECT
				mst_companies.strCode AS `Comapny Code`,
				mst_companies.strName AS `Company Name`,
				mst_locations.strCode AS `Location Code`,
				mst_locations.strName AS `Location Name`,
				mst_locations.strAddress,
				mst_locations.strPhoneNo,
				mst_locations.strFaxNo,
				mst_locations.strEmail
			FROM
			mst_locations
				Inner Join mst_companies ON mst_companies.intId = mst_locations.intCompanyId
			ORDER BY
				mst_companies.intId ASC,
				mst_locations.intId ASC
			";
	$pdf->Report($sql,'LOCATIONS LIST');
}

else if($type=='locationWiseGRN')
{
	$pdfLandscape = new Cezpdf('a4','landscape');

	$company = $_REQUEST['cboCompany'];
	$location = $_REQUEST['cboLocation'];
	$mainCat = $_REQUEST['cboMainCategory'];
	$subCat = $_REQUEST['cboSubCategory'];
	$item = $_REQUEST['cboItem'];
	$currencyId = $_REQUEST['cboCurrency'];

	$dtFrom = $_REQUEST['dtFromDate11'];
	$dtTo = $_REQUEST['dtToDate11'];
	$date = date("Y-m-d");
	// $date = "2012-09-25";
	$dtStr='';

	if(($dtFrom!='') || ($dtTo!='')){
		$dtStr.=" - ( ";
	}

	if($dtFrom!=''){
		$dtStr.="Date From : ".$dtFrom;
	}
	if($dtTo!=''){
		$dtStr .=" Date To : ".$dtTo;
	}
	if(($dtFrom!='') || ($dtTo!='')){
	$dtStr .=")";
	}

	$locationName= loadLocationName($location);

	$sql = "SELECT
				mst_financeexchangerate.dblBuying, 
				mst_financecurrency.strCode as currency 
			FROM
				mst_financeexchangerate 
				Inner Join mst_financecurrency ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId
			WHERE
				mst_financeexchangerate.intCurrencyId =  '$currencyId' AND
				mst_financeexchangerate.intCompanyId =  '$company'
			ORDER BY
				mst_financeexchangerate.dtmDate DESC
			LIMIT 1
			";
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);

	$currency = $row['currency'];
	$toCurrencyRate = $row['dblBuying'];

	if($toCurrencyRate<=0 || $toCurrencyRate<='')
		$toCurrencyRate=1;


	if($currencyId=='')
		$para1 = ' mst_financecurrency.strCode';
	else
		$para1 = $currency;

	if($currencyId==''){
		$para2 = 1;
		$toCurrencyRate=1;
	}
	else
		$para2 = 'ware_grnheader.dblExchangeRate';


		$para3 = "(SELECT
				mst_financeexchangerate.dblBuying
			FROM
				mst_financeexchangerate 
				Inner Join mst_financecurrency ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId
			WHERE
				mst_financeexchangerate.intCurrencyId =  ware_stocktransactions_bulk.intCurrencyId AND
				mst_financeexchangerate.intCompanyId =  '$company'
			ORDER BY
				mst_financeexchangerate.dtmDate DESC
			LIMIT 1)";


	$sql = "/* ( */
			

SELECT distinct
			mst_supplier.strName as supplier,
			mst_companies.strName AS Company , 
			mst_locations.strName AS Location,
			concat(tb1.intGrnNo,'/',tb1.intGrnYear) as `GRN No`,
			ware_grnheader.datdate as `GRN_DATE`,
			concat(ware_grnheader.intPoNo,'/',ware_grnheader.intPoYear) as `PO No`,
			mst_maincategory.strName as `Main Category`,
			mst_subcategory.strName as `Sub Category`,
			mst_item.strName as `Item` , 
			mst_units.strCode as `UOM` , ";

			if($currencyId=='')
			$sql .="$para1 as Currency,";
			else
			$sql .="'$para1' as Currency,";



		$sql .= " round(((SELECT
				round(  sum( (if(ware_grnheader.intGrnNo<>0,$para2,$para3)*ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate)/$toCurrencyRate),2   ) 
				FROM
				ware_stocktransactions_bulk 
				left Join ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear 
				where ware_stocktransactions_bulk.intDocumentNo = tb1.intGrnNo AND ware_stocktransactions_bulk.intDocumntYear = tb1.intGrnYear AND ware_stocktransactions_bulk.intItemId = tb1.intItemId  AND ware_stocktransactions_bulk.strType =  'GRN' ))/((SELECT
				round(sum(ware_stocktransactions_bulk.dblQty),2) 
				FROM
				ware_stocktransactions_bulk 
				where  ware_stocktransactions_bulk.intDocumentNo = tb1.intGrnNo AND ware_stocktransactions_bulk.intDocumntYear = tb1.intGrnYear AND ware_stocktransactions_bulk.intItemId = tb1.intItemId AND ware_stocktransactions_bulk.strType =  'GRN'  )),4) as Price ,
				
				
((SELECT
				round(sum(ware_stocktransactions_bulk.dblQty),2) 
				FROM
				ware_stocktransactions_bulk 
				where  ware_stocktransactions_bulk.intDocumentNo = tb1.intGrnNo AND ware_stocktransactions_bulk.intDocumntYear = tb1.intGrnYear AND ware_stocktransactions_bulk.intItemId = tb1.intItemId AND ware_stocktransactions_bulk.strType =  'GRN'  )) as Qty , 
				
((SELECT
				round(  sum( (if(ware_grnheader.intGrnNo<>0,$para2,$para3)*ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate)/$toCurrencyRate),2   )  
				FROM
				ware_stocktransactions_bulk 
				left Join ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear 
			where ware_stocktransactions_bulk.intDocumentNo = tb1.intGrnNo AND ware_stocktransactions_bulk.intDocumntYear = tb1.intGrnYear AND ware_stocktransactions_bulk.intItemId = tb1.intItemId  AND ware_stocktransactions_bulk.strType =  'GRN' )) as Amount,
			ware_grnheader.strInvoiceNo AS InvoiceNo,
			ware_grnheader.SUPPLIER_INVOICE_DATE
			FROM
			ware_grndetails  as tb1 
			Inner Join ware_grnheader ON tb1.intGrnNo = ware_grnheader.intGrnNo AND tb1.intGrnYear = ware_grnheader.intGrnYear
			Inner Join mst_item ON tb1.intItemId = mst_item.intId
			Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
			Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
			Inner Join mst_locations ON ware_grnheader.intCompanyId = mst_locations.intId  
			Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId 
			Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
			Inner Join mst_financecurrency ON mst_item.intCurrency = mst_financecurrency.intId 
			INNER JOIN trn_poheader ON trn_poheader.intPONo = ware_grnheader.intPoNo AND trn_poheader.intPOYear = ware_grnheader.intPoYear
			INNER JOIN mst_supplier ON mst_supplier.intId = trn_poheader.intSupplier
			WHERE
			ware_grnheader.intStatus =  '1' AND
			mst_locations.intCompanyId =  '$company' ";

		if($location!='')
		$sql .= " AND ware_grnheader.intCompanyId =  '$location' ";

		if($mainCat!='')
		$sql .= " AND mst_item.intMainCategory =  '$mainCat' ";
		if($subCat!='')
		$sql .= " AND mst_item.intSubCategory =  '$subCat' ";
		if($item!='')
		$sql .= " AND mst_item.intId =  '$item' ";


		if($dtFrom!='')
		$sql .= " AND ware_grnheader.datdate>='$dtFrom' ";
		if($dtTo!='')
		$sql .= " AND ware_grnheader.datdate<='$dtTo' ";
		$sql .= "  
			GROUP BY
			tb1.intGrnNo,
			tb1.intGrnYear,
			mst_item.intId 
			Order By 
			mst_locations.strName asc, 
			tb1.intGrnNo asc,
			tb1.intGrnYear asc,
			mst_maincategory.strName asc,
			mst_subcategory.strName asc,
			mst_item.strName asc /*)*/";

	/*	$sql .= " UNION
			(select
			'' as Company,
			'' as Location,
			'' as supplier,
			'' as `GRN No`,
			'' AS `GRN_DATE`,
			'' as `PO No`,
			'' as `Main Category`,
			'' as `Sub Category`,
			'' as `Item` ,
			'' as `UOM` ,
			'' as Currency,
			'' as Price ,
			sum(Qty) ,
			sum(Amount)


			from (select DISTINCT
			'' as Company,
			'' as Location,
			'' as supplier,
			'' as `GRN No`,
			'' AS `GRN_DATE`,
			'' as `PO No`,
			'' as `Main Category`,
			'' as `Sub Category`,
			'' as `Item` ,
			'' as `UOM` ,
			'' as Currency,
			'' as Price ,


((SELECT
				round(sum(ware_stocktransactions_bulk.dblQty),2)
				FROM
				ware_stocktransactions_bulk
				where  ware_stocktransactions_bulk.intDocumentNo = tb1.intGrnNo AND ware_stocktransactions_bulk.intDocumntYear = tb1.intGrnYear AND ware_stocktransactions_bulk.intItemId = tb1.intItemId AND ware_stocktransactions_bulk.strType =  'GRN'  )) as Qty ,

((SELECT
				round(  sum( (if(ware_grnheader.intGrnNo<>0,$para2,$para3)*ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate)/$toCurrencyRate),2   )
				FROM
				ware_stocktransactions_bulk
				left Join ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear
				where ware_stocktransactions_bulk.intDocumentNo = tb1.intGrnNo AND ware_stocktransactions_bulk.intDocumntYear = tb1.intGrnYear AND ware_stocktransactions_bulk.intItemId = tb1.intItemId  AND ware_stocktransactions_bulk.strType =  'GRN' )) as Amount
			FROM
			ware_grndetails  as tb1
			Inner Join ware_grnheader ON tb1.intGrnNo = ware_grnheader.intGrnNo AND tb1.intGrnYear = ware_grnheader.intGrnYear
			Inner Join mst_item ON tb1.intItemId = mst_item.intId
			Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
			Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
			Inner Join mst_locations ON ware_grnheader.intCompanyId = mst_locations.intId
			Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
			Inner Join mst_units ON mst_item.intUOM = mst_units.intId
			Inner Join mst_financecurrency ON mst_item.intCurrency = mst_financecurrency.intId
			WHERE
			ware_grnheader.intStatus =  '1' AND
			mst_locations.intCompanyId =  '$company' ";

		if($location!='')
		$sql .= " AND ware_grnheader.intCompanyId =  '$location' ";

		if($mainCat!='')
		$sql .= " AND mst_item.intMainCategory =  '$mainCat' ";
		if($subCat!='')
		$sql .= " AND mst_item.intSubCategory =  '$subCat' ";
		if($item!='')
		$sql .= " AND mst_item.intId =  '$item' ";


		if($dtFrom!='')
		$sql .= " AND ware_grnheader.datdate>='$dtFrom' ";
		if($dtTo!='')
		$sql .= " AND ware_grnheader.datdate<='$dtTo' ";
		$sql .= "
			GROUP BY
			tb1.intGrnNo,
			tb1.intGrnYear,
			mst_item.intId
			Order By
			mst_locations.strName asc,
			tb1.intGrnNo asc,
			tb1.intGrnYear asc,
			mst_maincategory.strName asc,
			mst_subcategory.strName asc,
			mst_item.strName asc ";
		$sql .=" /*group by mst_locations.intCompanyId,tb1.intGrnNo,tb1.intGrnYear,ware_grnheader.intPoNo,ware_grnheader.intPoYear*//* ) as tbx  ";

		$sql .= ")";*/





		  //echo $sql;
	$result = $db->RunQuery($sql);
	if($_REQUEST['report_type']=='excel')
	{
		include "xls_location_wise_grn_notes.php";
		return;
	}

	while($data[] = mysqli_fetch_array($result, MYSQLI_ASSOC)) {}

	$pdfLandscape->ezImage('../../../images/logo.jpg',5,70,800,150);
	$pdfLandscape->selectFont('../../../libraries/pdf-php/fonts/Helvetica.afm');
	//$this->ezStartPageNumbers(580,10,10,'','',1);
	$pdfLandscape->ezStartPageNumbers(580,10,10,'','',1);
	//print_r($data);

	//$cols = array('Main'=>"Main",'Sub'=>'Sub','Code'=>'Code','Name'=>'Name');
	$currencyName = "Location Wise GRN Notes$dtStr";
	$pdfLandscape->ezText($currencyName,$size=0,$options=array(),$test=0);
	$pdfLandscape->ezText('',$size=0,$options=array(),$test=0);
	$pdfLandscape->ezTable($data,'','',
array('xPos'=>0,'xOrientation'=>'right','width'=>800,'fontSize' => 7
,'cols'=>array('location'=>array('justification'=>'left'),'Company'=>array('justification'=>'left'),'Issue No'=>array('justification'=>'left'),'Order No'=>array('justification'=>'left'),'Style No'=>array('justification'=>'left'),'Main Category'=>array('justification'=>'left'),'Sub Category'=>array('justification'=>'left'),'UOM'=>array('justification'=>'left'),'Item'=>array('justification'=>'left'),'Price'=>array('justification'=>'right'),'Qty'=>array('justification'=>'right'),'Amount'=>array('justification'=>'right'))));

/*		$pdf->ezTable($data,'',"PENDING PO SUMMERY$currencyName",
array('xPos'=>0,'xOrientation'=>'right','width'=>550,'fontSize' => 7
,'cols'=>array('PO Qty'=>array('justification'=>'right'),'GRN Qty'=>array('justification'=>'right'),'Return Qty'=>array('justification'=>'right'),'Balance Qty'=>array('justification'=>'right'),'Amount'=>array('justification'=>'right'))));

*/
	$pdfLandscape->ezStream();
}


//edit by nuwan

else if($type=='stoMoveReport')
{
	$pdfLandscape = new Cezpdf('a4','landscape');

	$company 					= $_REQUEST['cboCompany'];
	$location 					= $_REQUEST['cboLocation'];
	$mainCat 					= $_REQUEST['cboMainCategory'];
	$subCat 					= $_REQUEST['cboSubCategory'];

	$dtFrom 					= $_REQUEST['dtFromStrDate11'];
	$dtTo 						= $_REQUEST['dtToStrDate11'];
	$date 						= date("Y-m-d");

	$header_data				= get_selection_details($company,$location,$mainCat,$subCat);
	$header_data['currency']	= 'LKR';
	$header_data['range']		= 'FROM '.$dtFrom.' TO '.$dtTo;

	// $date = "2012-09-25";
	/*$dtStr='';

	if(($dtFrom!='') || ($dtTo!='')){
		$dtStr.=" - ( ";
	}

	if($dtFrom!=''){
		$dtStr.="Date From : ".$dtFrom;
	}
	if($dtTo!=''){
		$dtStr .=" Date To : ".$dtTo;
	}
	if(($dtFrom!='') || ($dtTo!='')){
	$dtStr .=")";
	}

	*/

 $sql = "SELECT
			date(ware_stocktransactions_bulk.dtDate) AS datez,
			mst_companies.strName AS company,
			mst_locations.strName AS location,
			mst_maincategory.strName AS main_category,
			mst_subcategory.strName AS sub_category,
			mst_item.strCode AS `code`,
            mst_item.strCode as SUP_ITEM_CODE,
			mst_item.strName AS item,
                        mst_item.ITEM_HIDE,
			mst_units.strCode AS uom,
			Sum(if(strType='GRN',dblQty,0)) AS GRN,
			Sum(if(strType='GRN',dblQty*mst_financeexchangerate.dblBuying*dblGRNRate,0)) AS GRN_VALUE,
			GROUP_CONCAT(DISTINCT if(strType='GRN',CONCAT(intDocumentNo,'/',intDocumntYear),'')) AS GRN_NOS,
			Sum(if(strType='Adjust Q+',dblQty,0)) AS Adjust_qty_plus,
			Sum(if(strType='Adjust Q+',dblQty*mst_financeexchangerate.dblBuying*dblGRNRate,0)) AS Adjust_qty_plus_VALUE,
			GROUP_CONCAT(DISTINCT if(strType='Adjust Q+',CONCAT(intDocumentNo,'/',intDocumntYear),'')) AS Adjust_qty_plus_NOS,
			Sum(if(strType='OPENSTOCK' || strType='OPENSTOCK-FROM-EMBELISH-HEAD-OFFICE',dblQty,0)) AS OPENSTOCK,
			Sum(if(strType='OPENSTOCK' || strType='OPENSTOCK-FROM-EMBELISH-HEAD-OFFICE',dblQty*mst_financeexchangerate.dblBuying*dblGRNRate,0)) AS OPENSTOCK_VALUE,
			GROUP_CONCAT(DISTINCT if(strType='OPENSTOCK',CONCAT(intDocumentNo,'/',intDocumntYear),'')) AS OPENSTOCK_NOS,
			Sum(if(strType='TRANSFERIN',dblQty,0)) AS TRANSFERIN,
			Sum(if(strType='TRANSFERIN',dblQty*mst_financeexchangerate.dblBuying*dblGRNRate,0)) AS TRANSFERIN_VALUE,
			GROUP_CONCAT(DISTINCT if(strType='TRANSFERIN',CONCAT(intDocumentNo,'/',intDocumntYear),'')) AS TRANSFERIN_NOS,
			Sum(if(strType='RETSTORES',dblQty,0)) AS RETSTORES,
			Sum(if(strType='RETSTORES',dblQty*mst_financeexchangerate.dblBuying*dblGRNRate,0)) AS RETSTORES_VALUE,
			GROUP_CONCAT(DISTINCT if(strType='RETSTORES',CONCAT(intDocumentNo,'/',intDocumntYear),'')) AS RETSTORES_NOS,
			Sum(if(strType='RTSUP_RCV',dblQty,0)) AS RTSUP_RCV,
			Sum(if(strType='RTSUP_RCV',dblQty*mst_financeexchangerate.dblBuying*dblGRNRate,0)) AS RTSUP_RCV_VALUE,
			GROUP_CONCAT(DISTINCT if(strType='RTSUP_RCV',CONCAT(intDocumentNo,'/',intDocumntYear),'')) AS RTSUP_RCV_NOS,
			Sum(if(strType='ITEMCREATION+',dblQty,0)) AS ITEMCREATION_plus,
			Sum(if(strType='ITEMCREATION+',dblQty*mst_financeexchangerate.dblBuying*dblGRNRate,0)) AS ITEMCREATION_plus_VALUE,
			GROUP_CONCAT(DISTINCT if(strType='ITEMCREATION+',CONCAT(intDocumentNo,'/',intDocumntYear),'')) AS ITEMCREATION_plus_NOS,
			Sum(if(strType='Adjust RQ+',dblQty,0)) AS Adjust_rate_and_qty,
			Sum(if(strType='Adjust RQ+',dblQty*mst_financeexchangerate.dblBuying*dblGRNRate,0)) AS Adjust_rate_and_qty_VALUE,
			GROUP_CONCAT(DISTINCT if(strType='Adjust RQ+',CONCAT(intDocumentNo,'/',intDocumntYear),'')) AS Adjust_rate_and_qty_NOS,
			Sum(if(strType='UNALLOCATE',dblQty,0)) AS UNALLOCATE,
			Sum(if(strType='UNALLOCATE',dblQty*mst_financeexchangerate.dblBuying*dblGRNRate,0)) AS UNALLOCATE_VALUE,
			GROUP_CONCAT(DISTINCT if(strType='UNALLOCATE',CONCAT(intDocumentNo,'/',intDocumntYear),'')) AS UNALLOCATE_NOS,
			Sum(if(strType='Adjust R+',dblQty,0)) AS Adjust_rate_plus,
			Sum(if(strType='Adjust R+',dblQty*mst_financeexchangerate.dblBuying*dblGRNRate,0)) AS Adjust_rate_plus_VALUE,
			GROUP_CONCAT(DISTINCT if(strType='Adjust R+',CONCAT(intDocumentNo,'/',intDocumntYear),'')) AS Adjust_rate_plus_NOS,
			Sum(if(strType='ALLOCATE',dblQty,0)) AS ALLOCATE,
			Sum(if(strType='ALLOCATE',dblQty*mst_financeexchangerate.dblBuying*dblGRNRate,0)) AS ALLOCATE_VALUE,
			GROUP_CONCAT(DISTINCT if(strType='ALLOCATE',CONCAT(intDocumentNo,'/',intDocumntYear),'')) AS ALLOCATE_NOS,
			Sum(if(strType='ITEMCREATION-',dblQty*-1,0)) AS ITEMCREATION_minus,
			Sum(if(strType='ITEMCREATION-',dblQty*mst_financeexchangerate.dblBuying*dblGRNRate*-1,0)) AS ITEMCREATION_minus_VALUE,
			GROUP_CONCAT(DISTINCT if(strType='ITEMCREATION-',CONCAT(intDocumentNo,'/',intDocumntYear),'')) AS ITEMCREATION_minus_NOS,
			Sum(if(strType='STORESTRANSFER',dblQty*-1,0)) AS STORESTRANSFER,
			Sum(if(strType='STORESTRANSFER',dblQty*mst_financeexchangerate.dblBuying*dblGRNRate*-1,0)) AS STORESTRANSFER_VALUE,
			GROUP_CONCAT(DISTINCT if(strType='STORESTRANSFER',CONCAT(intDocumentNo,'/',intDocumntYear),'')) AS STORESTRANSFER_NOS,
			Sum(if(strType='RTSUP',dblQty*-1,0)) AS RTSUP,
			Sum(if(strType='RTSUP',dblQty*mst_financeexchangerate.dblBuying*dblGRNRate*-1,0)) AS RTSUP_VALUE,
			GROUP_CONCAT(DISTINCT if(strType='RTSUP',CONCAT(intDocumentNo,'/',intDocumntYear),'')) AS RTSUP_NOS,
			Sum(if(strType='Dispose',dblQty*-1,0)) AS Dispose,
			Sum(if(strType='Dispose',dblQty*mst_financeexchangerate.dblBuying*dblGRNRate*-1,0)) AS Dispose_VALUE,
			GROUP_CONCAT(DISTINCT if(strType='Dispose',CONCAT(intDocumentNo,'/',intDocumntYear),'')) AS Dispose_NOS,
			Sum(if(strType='CLOSESTOCK',dblQty*-1,0)) AS CLOSESTOCK,
			Sum(if(strType='CLOSESTOCK',dblQty*mst_financeexchangerate.dblBuying*dblGRNRate*-1,0)) AS CLOSESTOCK_VALUE,
			GROUP_CONCAT(DISTINCT if(strType='CLOSESTOCK',CONCAT(intDocumentNo,'/',intDocumntYear),'')) AS CLOSESTOCK_NOS,
			Sum(if(strType='GATEPASS',dblQty*-1,0)) AS GATEPASS,
			Sum(if(strType='GATEPASS',dblQty*mst_financeexchangerate.dblBuying*dblGRNRate*-1,0)) AS GATEPASS_VALUE,
			GROUP_CONCAT(DISTINCT if(strType='GATEPASS',CONCAT(intDocumentNo,'/',intDocumntYear),'')) AS GATEPASS_NOS,
			Sum(if(strType='INVOICE',dblQty*-1,0)) AS INVOICE,
			Sum(if(strType='INVOICE',dblQty*mst_financeexchangerate.dblBuying*dblGRNRate*-1,0)) AS INVOICE_VALUE,
			GROUP_CONCAT(DISTINCT if(strType='INVOICE',CONCAT(intDocumentNo,'/',intDocumntYear),'')) AS INVOICE_NOS,
			Sum(if(strType='Adjust Q-',dblQty*-1,0)) AS Adjust_Q_minus,
			Sum(if(strType='Adjust Q-',dblQty*mst_financeexchangerate.dblBuying*dblGRNRate*-1,0)) AS Adjust_Q_minus_VALUE,
			GROUP_CONCAT(DISTINCT if(strType='Adjust Q-',CONCAT(intDocumentNo,'/',intDocumntYear),'')) AS Adjust_Q_minus_NOS,
			Sum(if(strType='ISSUE',dblQty*-1,0)) AS ISSUE,
			Sum(if(strType='ISSUE',dblQty*mst_financeexchangerate.dblBuying*dblGRNRate*-1,0)) AS ISSUE_VALUE,
			GROUP_CONCAT(DISTINCT if(strType='ISSUE',CONCAT(intDocumentNo,'/',intDocumntYear),'')) AS ISSUE_NOS
			FROM
			ware_stocktransactions_bulk
			LEFT JOIN mst_financeexchangerate on mst_financeexchangerate.dtmDate = ware_stocktransactions_bulk.dtGRNDate and mst_financeexchangerate.intCompanyId = ware_stocktransactions_bulk.intCompanyId
			and mst_financeexchangerate.intCurrencyId = ware_stocktransactions_bulk.intCurrencyId
			INNER JOIN mst_item ON mst_item.intId = ware_stocktransactions_bulk.intItemId
			INNER JOIN mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
			INNER JOIN mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
			INNER JOIN mst_companies ON ware_stocktransactions_bulk.intCompanyId = mst_companies.intId
			INNER JOIN mst_locations ON ware_stocktransactions_bulk.intLocationId = mst_locations.intId
			INNER JOIN mst_units ON mst_units.intId = mst_item.intUOM
                        LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
			LEFT JOIN ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear
			
								
	    WHERE
					
			date(ware_stocktransactions_bulk.dtDate) BETWEEN ('$dtFrom') AND ('$dtTo')
					
					AND
					mst_companies.intId =('$company ')
					AND
					mst_locations.intId =('$location ')
					AND
					mst_maincategory.intId =('$mainCat ') ";

					if($subCat!='')
		            $sql .= "AND mst_subcategory.intId  =  '$subCat' ";


					$sql .= "GROUP BY
					ware_stocktransactions_bulk.intItemId,
					ware_stocktransactions_bulk.intLocationId,
					date(ware_stocktransactions_bulk.dtDate)
					
					";




		 $sql;
			$result = $db->RunQuery($sql);
			if($_REQUEST['report_type']=='excel')
			{
				include "xls_stock_movement_report.php";
				return;
			}

	while($data[] = mysqli_fetch_array($result, MYSQLI_ASSOC)) {}

	/*$pdfLandscape->ezImage('../../../images/logo.jpg',5,70,800,150);
	$pdfLandscape->selectFont('../../../libraries/pdf-php/fonts/Helvetica.afm');
	//$this->ezStartPageNumbers(580,10,10,'','',1);
	$pdfLandscape->ezStartPageNumbers(580,10,10,'','',1);
	//print_r($data);

	//$cols = array('Main'=>"Main",'Sub'=>'Sub','Code'=>'Code','Name'=>'Name');
	$currencyName = "Location Wise GRN Notes$dtStr";
	$pdfLandscape->ezText($currencyName,$size=0,$options=array(),$test=0);
	$pdfLandscape->ezText('',$size=0,$options=array(),$test=0);
	$pdfLandscape->ezTable($data,'','',
array('xPos'=>0,'xOrientation'=>'right','width'=>800,'fontSize' => 7
,'cols'=>array('location'=>array('justification'=>'left'),'Company'=>array('justification'=>'left'),'Issue No'=>array('justification'=>'left'),'Order No'=>array('justification'=>'left'),'Style No'=>array('justification'=>'left'),'Main Category'=>array('justification'=>'left'),'Sub Category'=>array('justification'=>'left'),'UOM'=>array('justification'=>'left'),'Item'=>array('justification'=>'left'),'Price'=>array('justification'=>'right'),'Qty'=>array('justification'=>'right'),'Amount'=>array('justification'=>'right'))));
	
	$pdfLandscape->ezStream();*/
	
	}
//// end nuwan
	

function GetMainDetails_loanIn($company,$location,$item,$dtFrom,$dtTo)
{
	global $db;
	
		
	$sql = "SELECT
ware_grnheader.datdate AS grnDate,
mst_companies.strName as grnCompany,
concat(ware_grnheader.intGrnNo,'/',ware_grnheader.intGrnYear) as grnNo,
mst_item.strName as item,
mst_units.strName as uom,
ware_grndetails.dblGrnQty,
ware_grndetails.dblGrnRate,
substring(ware_returntosupplierheader.datdate,1,10) as returnDate,
concat(ware_returntosupplierheader.intReturnNo,'/',ware_grnheader.intGrnYear) as retNo,
ware_returntosupplierdetails.dblQty as retQty 
FROM
trn_poheader
Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
Inner Join ware_grnheader ON trn_poheader.intPONo = ware_grnheader.intPoNo AND trn_poheader.intPOYear = ware_grnheader.intPoYear
Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear
Inner Join mst_item ON ware_grndetails.intItemId = mst_item.intId
Inner Join mst_locations ON mst_supplier.intInterLocation = mst_locations.intId
Inner Join mst_companies ON mst_supplier.intCompanyId = mst_companies.intId
Inner Join mst_units ON mst_item.intUOM = mst_units.intId
left Join ware_returntosupplierheader ON ware_grndetails.intGrnNo = ware_returntosupplierheader.intGrnNo AND ware_grndetails.intGrnYear = ware_returntosupplierheader.intGrnYear and ware_returntosupplierheader.intStatus='1' 
left Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear AND ware_grndetails.intItemId = ware_returntosupplierdetails.intItemId
WHERE
mst_supplier.intInterCompany =  '1' AND
ware_grnheader.intStatus =  '1' AND 
ware_grnheader.intCompanyId =  '$location' ";
if($item!='')
$sql .= " AND ware_grndetails.intItemId =  '$item' ";


if($dtFrom!='')
$sql .= " AND date(ware_grnheader.datdate) >='$dtFrom' ";
if($dtTo!='')
$sql .= " AND date(ware_grnheader.datdate) <='$dtTo' "; 
$sql .= " 
ORDER BY
ware_grnheader.intGrnNo ASC,
ware_grnheader.intGrnYear ASC,
mst_item.strName ASC,
ware_returntosupplierheader.intReturnNo ASC,
ware_returntosupplierheader.intReturnYear ASC
";

//echo $sql;
	return $db->RunQuery($sql);
}
//-----------------------------------------------------------
function GetMainDetails_loanOut($company,$location,$item,$dtFrom,$dtTo)
{
	global $db;
	
		
	$sql = "select 
 tb1.dtmPODate, 
tb1.poCompany, 
tb1.intPONo,
tb1.intPOYear, 
tb1.item, 
tb1.uom, 
tb1.dblGRNRate, 
tb1.intItemId, 
tb1.poQty , 
tb1.rcds , 
ware_returntosupplierheader.intReturnNo,
ware_returntosupplierheader.intReturnYear , 
substr(ware_returntosupplierheader.datdate,1,10) as datdate, 
ware_returntosupplierdetails.dblQty 
 from (SELECT 
substr(trn_poheader.dtmPODate,1,10) as dtmPODate,
mst_companies.strName as poCompany,
trn_poheader.intPONo,
trn_poheader.intPOYear,
mst_item.strName as item,
ware_invoicedetails.intItemId,
mst_units.strName as uom,
ware_stocktransactions_bulk.dblGRNRate,
Sum(ware_invoicedetails.dblQty) as poQty, 
count(ware_invoicedetails.intItemId) as rcds 
FROM
trn_poheader  
Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
Inner Join trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
Inner Join ware_invoiceheader ON trn_podetails.intPONo = ware_invoiceheader.intPONo AND trn_podetails.intPOYear = ware_invoiceheader.intPOYear
Inner Join ware_invoicedetails ON ware_invoiceheader.intInvoiceNo = ware_invoicedetails.intInvoiceNo AND ware_invoiceheader.intInvoiceYear = ware_invoicedetails.intInvoiceYear AND trn_podetails.intItem = ware_invoicedetails.intItemId
Inner Join mst_item on ware_invoicedetails.intItemId = mst_item.intId  
Inner Join mst_locations ON trn_poheader.intCompany = mst_locations.intId
Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
Inner Join mst_units ON mst_item.intUOM = mst_units.intId
Inner Join ware_stocktransactions_bulk ON ware_invoiceheader.intCompanyId = ware_stocktransactions_bulk.intLocationId AND ware_invoicedetails.intInvoiceNo = ware_stocktransactions_bulk.intDocumentNo AND ware_invoicedetails.intInvoiceYear = ware_stocktransactions_bulk.intDocumntYear AND ware_invoicedetails.intItemId = ware_stocktransactions_bulk.intItemId 
Inner Join ware_grnheader ON trn_poheader.intPONo = ware_grnheader.intPoNo AND trn_poheader.intPOYear = ware_grnheader.intPoYear
Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND trn_podetails.intItem = ware_grndetails.intItemId 
WHERE 
mst_supplier.intInterCompany =  '1' AND
mst_supplier.intCompanyId =  '$company' AND
mst_supplier.intInterLocation =  '$location' AND
trn_poheader.intStatus =  '1' AND   
ware_invoiceheader.intStatus =  '1' 
GROUP BY
trn_poheader.intPONo,
trn_poheader.intPOYear,
ware_invoicedetails.intItemId  ) as tb1 
Inner Join ware_grnheader ON tb1.intPONo = ware_grnheader.intPoNo AND tb1.intPOYear = ware_grnheader.intPoYear
Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND tb1.intItemId = ware_grndetails.intItemId
Left Join ware_returntosupplierheader ON ware_grndetails.intGrnNo = ware_returntosupplierheader.intGrnNo AND ware_grndetails.intGrnYear = ware_returntosupplierheader.intGrnYear
Left Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear AND ware_grndetails.intItemId = ware_returntosupplierdetails.intItemId
where 1=1  ";
if($item!='')
$sql .= " AND tb1.intItemId =  '$item' ";
if($dtFrom!='')
$sql .= " AND date(tb1.dtmPODate) >='$dtFrom' ";
if($dtTo!='')
$sql .= " AND date(tb1.dtmPODate) <='$dtTo' "; 
$sql .= " 
ORDER BY
tb1.intPONo ASC,
tb1.intPOYear ASC,
tb1.item ASC,
ware_returntosupplierheader.intReturnNo ASC,
ware_returntosupplierheader.intReturnYear ASC
";
//  echo   $sql;

	return $db->RunQuery($sql);
}
//------------------------------function load Header---------------------
function loadCurrency($company,$date,$currency) 
{
	global $db;
		  $sql = "SELECT
					mst_financeexchangerate.dblBuying,
					mst_financecurrency.strCode
					FROM
					mst_financeexchangerate
					Inner Join mst_financecurrency ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId
					WHERE
					mst_financeexchangerate.intCompanyId =  '$company' AND
					mst_financeexchangerate.intCurrencyId =  '$currency' AND
					mst_financeexchangerate.dtmDate =  '$date'";
				$result = $db->RunQuery($sql);
				$row=mysqli_fetch_array($result);
				return (float)($row['dblBuying']);
}
//------------------------------function load Header---------------------
function loadCurrencyName($currency) 
{
	global $db;
		    $sql = "SELECT mst_financecurrency.strCode
					FROM
					mst_financecurrency
					WHERE
					mst_financecurrency.intId =  '$currency'";
				$result = $db->RunQuery($sql);
				$row=mysqli_fetch_array($result);
				return $row['strCode'];
}
//-----------------------------------------------
function loadSupplierName($supplier){
	global $db;
		    $sql = "SELECT
						mst_supplier.intId,
						mst_supplier.strName
						FROM mst_supplier
						WHERE
					mst_supplier.intId =  '$supplier'";
				$result = $db->RunQuery($sql);
				$row=mysqli_fetch_array($result);
				return $row['strName'];
}
//-----------------------------------------------
function loadLocationName($location){
	global $db;
		    $sql = "SELECT
					mst_locations.strName
					FROM mst_locations
					WHERE
					mst_locations.intId =  '$location'";
				$result = $db->RunQuery($sql);
				$row=mysqli_fetch_array($result);
				return $row['strName'];
}
//-----------------------------------------------
function loadDepartmentName($department){
	global $db;
		    $sql = "SELECT
					mst_department.strName
					FROM mst_department
					WHERE
					mst_department.intId =  '$department'";
				$result = $db->RunQuery($sql);
				$row=mysqli_fetch_array($result);
				return $row['strName'];
}

function get_selection_details($company,$location,$mainCat,$subCat){
	
	global $db;
	$sql 	= "SELECT
			mst_companies.strName as company 
			FROM mst_companies
			WHERE
			mst_companies.intId =  '$company'";
	$result = $db->RunQuery($sql);
	$row	=mysqli_fetch_array($result);
	$data['company']=$row['company'];
	
	$sql 	= "SELECT
			mst_locations.strName as location
			FROM mst_locations
			WHERE
			mst_locations.intId =  '$location'";
	$result = $db->RunQuery($sql);
	$row	=mysqli_fetch_array($result);
	$data['location']=$row['location'];

	
		$sql 	= "SELECT
			mst_maincategory.strName as main_category
			FROM mst_maincategory
			WHERE
			mst_maincategory.intId =  '$mainCat'";
	$result = $db->RunQuery($sql);
	$row	=mysqli_fetch_array($result);
	$data['main_category']=$row['main_category'];

	
		$sql 	= "SELECT
			mst_subcategory.strName as sub_category
			FROM mst_subcategory
			WHERE
			mst_subcategory.intId =  '$subCat'";
	$result = $db->RunQuery($sql);
	$row	=mysqli_fetch_array($result);
	$data['sub_category']=$row['sub_category'];

	return $data;

}
function get_ho_loc($companyId){
    global $db;
    $sql="SELECT
			mst_locations.intId
			FROM `mst_locations`
			WHERE
			mst_locations.COMPANY_MAIN_STORES_LOCATION = 1 AND
			mst_locations.intCompanyId = '$companyId'
			";
			
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['intId'];

}

?>
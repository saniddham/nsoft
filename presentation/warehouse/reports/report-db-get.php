<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
	session_start();
	$backwardseperator = "../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
if($requestType=='loadSubCategory')
	{
		$mainCategory  = $_REQUEST['mainCategory'];
		$sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strCode,
				mst_subcategory.strName
				FROM mst_subcategory
				WHERE
				mst_subcategory.intMainCategory =  '$mainCategory' 
				ORDER BY mst_subcategory.strName ASC";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	
	if($requestType=='loadSubCategory_multi')
	{
		
		$mainCategory  			= $_REQUEST['mainCategory'];
		
		$sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strCode,
				mst_subcategory.strName
				FROM mst_subcategory
				WHERE
				mst_subcategory.intMainCategory  IN ($mainCategory)
				ORDER BY mst_subcategory.strName ASC";
				
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{ 
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	if($requestType=='loadItem_multiple')
	{ 
	    
		$mainCategory  			= $_REQUEST['mainCategory'];
		$subCategory  			= $_REQUEST['subCategory'];
		
		
		$sql = "SELECT
				mst_item.intId,
				mst_item.strName
				FROM mst_item
				WHERE 
				1=1 ";
		if($mainCategory!='')		
		$sql .= " AND mst_item.intMainCategory in ($mainCategory)";		
		if($subCategory!='')		
		$sql .= " AND mst_item.intSubCategory in ($subCategory)";
		
		$sql .= " ORDER BY mst_item.strName ASC";
		
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		
	}
	
	else if($requestType == 'loadOrder') //suvini 2017.12.14
	{

						$year           = $_REQUEST['year'];
						$html		   = "<option value=\"\"></option>";
						$result_order   = "select 
					  DISTINCT (trn_orderheader.intOrderNo) as intOrderNo,
						trn_orderheader.intOrderYear
						FROM
						trn_orderheader
						INNER  JOIN trn_orderdetails on 
						trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND 
						trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
						WHERE

						trn_orderheader.intOrderYear = '".$year."'
						AND trn_orderheader.intStatus in (1,-10,3,2)
						ORDER BY
						trn_orderheader.intOrderNo";
                        
						$result = $db->RunQuery($result_order);
						$html = "<option value=\"\"></option>";
						while($row=mysqli_fetch_array($result))
		{
			
	                    $html	.= "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
		}
		 $response['orderHTML']	= $html;	
		 echo json_encode($response);
 	}
	
	
	
if($requestType=='loadItems')
	{
		$mainCategory  = $_REQUEST['mainCategory'];
		$subCategory  = $_REQUEST['subCategory'];
		$sql = "SELECT
				mst_item.intId,
				mst_item.strName
				FROM mst_item
				WHERE 
				1=1 ";
		if($subCategory!='')		
		$sql .= " AND mst_item.intSubCategory =  '$subCategory'  ";
		if($mainCategory!='')		
		$sql .= " AND mst_item.intMainCategory =  '$mainCategory'  ";

		$sql .= " ORDER BY mst_item.strName ASC";
		
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadLocations')
	{
		$company  = $_REQUEST['company'];
		$sql = "SELECT
				mst_locations.intId,
				mst_locations.strName
				FROM mst_locations
				WHERE
				mst_locations.intCompanyId =  '$company'";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadSubStores')
	{
		$company  = $_REQUEST['company'];
		$location  = $_REQUEST['location'];
		$sql = "SELECT
				mst_substores.intId,
				mst_substores.strName
				FROM `mst_substores`
				WHERE
				mst_substores.intLocation = '$location'
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadSalesOrders')
	{
		$orderYear  = $_REQUEST['orderYear'];
		$orderNo  = $_REQUEST['orderNo'];
		$sql = "SELECT
				trn_orderdetails.intSalesOrderId,
				trn_orderdetails.strSalesOrderNo,
				trn_orderdetails.strCombo,
				trn_orderdetails.strPrintName,
				mst_part.strName, 
				concat(strSalesOrderNo,'/',strCombo,'/',strPrintName,'/',strName) as salesOrder 
				FROM
				trn_orderdetails
				INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
				WHERE
				trn_orderdetails.intOrderNo = '$orderNo' AND
				trn_orderdetails.intOrderYear = '$orderYear'
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intSalesOrderId']."\">".$row['salesOrder']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}


	?>

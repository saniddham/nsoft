<?php
session_start();
 $companyId 			= $_SESSION['CompanyID'];
$locationId 		= $_SESSION['CompanyID'];//this locationId use in report header(reportHeader.php)-------
$intUser  			= $_SESSION["userId"];
$mainPath 			= $_SESSION['mainPath'];
$thisFilePath 		=  $_SERVER['PHP_SELF'];
 require_once 		"class/cls_commonFunctions_get.php";
  
$obj_common	= new cls_commonFunctions_get($db);


$company 	= $_REQUEST['company'];
$location 	= $_REQUEST['location'];
$subStores 	= $_REQUEST['subStores'];
$orderNo 	= $_REQUEST['orderNo'];
$orderYear 	= $_REQUEST['orderYear'];
$salesOrder = $_REQUEST['salesOrder'];

$result			= $obj_common->loadCompany_result($company,'RunQuery');
$row 			= mysqli_fetch_array($result);
$company_desc 	= $row['strName'];

$result			= $obj_common->loadLocationName($location,'RunQuery');
$row 			= mysqli_fetch_array($result);
$location_desc	= $row['strName'];

$result			= $obj_common->load_sub_stores_result($subStores,'RunQuery');
$row 			= mysqli_fetch_array($result);
$subStores_desc	= $row['strName'];

$result			= $obj_common->load_sales_order_result($orderNo,$orderYear,$salesOrder,'RunQuery');
$row 			= mysqli_fetch_array($result);
$salesOrder_desc= $row['salesOrder'];

 //----------------------------------
?>
 <head>
 <title>Ink Stock Balance Report</title>
 <style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:264px;
	top:247px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<form id="frmMrnReport" name="frmMrnReport" method="post"  >
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>INK STOCK BALANCE REPORT</strong></div>
<table width="868" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td width="905">
  <table width="100%">
   <tr>
    <td width="9%"></td>
    <td width="9%" class="normalfnt" align="right"><strong style="text-align:right">Company</strong></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="31%" align="left"><span class="normalfnt" style="text-align:left"><?php echo $company_desc  ?></span></td>
    <td width="13%">&nbsp;</td>
    <td width="1%" align="center" valign="middle">&nbsp;</td>
    <td width="22%">&nbsp;</td>
    <td width="6%" class="normalfnt"></td>
    <td width="7%"></td>
  </tr>
   <tr>
    <td></td>
    <td class="normalfnt" align="right"><strong>Location</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $location_desc  ?></span></td>
    <td class="normalfnt" align="right"><strong style="text-align:right">Sub Stores</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt" style="text-align:left"><?php echo $subStores_desc  ?></span></td>
    <td class="normalfnt"></td>
    <td></td>
  </tr>
   <tr>
    <td></td>
    <td class="normalfnt"><strong>Order No</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php if($orderNo !='' ){echo $orderYear."/".$orderNo;}  ?></span></td>
    <td class="normalfnt" align="right"><strong style="text-align:right">Sales Order</strong></td>
    <td align="center" valign="middle"></td>
    <td><span class="normalfnt" style="text-align:left"><?php echo $salesOrder_desc  ?></span></td>
    <td></td>
    <td></td>
  </tr>
   </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr>
              <th width="14%" >Order No</th>
              <th width="17%" >Sales Order</th>
              <th width="16%" >Color</th>
              <th width="19%" >Technique</th>
              <th width="28%" <?php if($reptType=='1'){ ?> style="display:none"<?php } ?>>Ink Type</th>
              <th width="6%" >Weight</th>
              </tr>
            <?php 
			
			$sql = "SELECT
					DISTINCT 
					trn_sampleinfomations_details_technical.intColorId,
					trn_sampleinfomations_details_technical.intInkTypeId AS intInkType,
					mst_inktypes.strName AS INK,
					mst_colors.strName AS COLOR,
					trn_sampleinfomations_details.intTechniqueId AS intTechnique,
					mst_techniques.strName AS TECHNIQUE,
					trn_orderheader.intOrderNo,
					trn_orderheader.intOrderYear,
					CONCAT(trn_orderheader.intOrderNo,'/',trn_orderheader.intOrderYear) AS ORDER_NO,
					trn_orderdetails.strSalesOrderNo as SALES_ORDER,
					trn_orderdetails.intSalesOrderId,
					(SELECT
					sum(ware_sub_color_stocktransactions_bulk.dblQty)
					FROM `ware_sub_color_stocktransactions_bulk`
					WHERE
					ware_sub_color_stocktransactions_bulk.intOrderNo = trn_orderheader.intOrderNo AND
					ware_sub_color_stocktransactions_bulk.intOrderYear = trn_orderheader.intOrderYear AND
					ware_sub_color_stocktransactions_bulk.intSalesOrderId = trn_orderdetails.intSalesOrderId AND
					ware_sub_color_stocktransactions_bulk.intColorId = trn_sampleinfomations_details_technical.intColorId AND
					ware_sub_color_stocktransactions_bulk.intTechnique = trn_sampleinfomations_details.intTechniqueId AND
					ware_sub_color_stocktransactions_bulk.intInkType = trn_sampleinfomations_details_technical.intInkTypeId AND 
					ware_sub_color_stocktransactions_bulk.intLocationId = '$location' AND 
					ware_sub_color_stocktransactions_bulk.intSubStores = '$subStores'
					) AS INK_STOCK  
					FROM
					trn_orderheader
					INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					INNER JOIN trn_sampleinfomations_details_technical ON trn_orderdetails.intSampleNo = trn_sampleinfomations_details_technical.intSampleNo AND trn_orderdetails.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear AND trn_orderdetails.strCombo = trn_sampleinfomations_details_technical.strComboName AND trn_orderdetails.strPrintName = trn_sampleinfomations_details_technical.strPrintName AND trn_orderdetails.intRevisionNo = trn_sampleinfomations_details_technical.intRevNo
					Inner Join mst_inktypes ON mst_inktypes.intId = trn_sampleinfomations_details_technical.intInkTypeId
					Inner Join mst_colors ON mst_colors.intId = trn_sampleinfomations_details_technical.intColorId
					Inner Join trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_details_technical.intSampleNo AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_details_technical.intRevNo AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_details_technical.strPrintName AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_details_technical.strComboName AND trn_sampleinfomations_details.intColorId = trn_sampleinfomations_details_technical.intColorId
					Inner Join mst_techniques ON mst_techniques.intId = trn_sampleinfomations_details.intTechniqueId
					WHERE
					trn_orderheader.intStatus = '1'  ";
					if($orderNo != '')
					$sql.=" AND
					trn_orderheader.intOrderNo = '$orderNo' ";
					if($orderYear != '')
					$sql.=" AND
					trn_orderheader.intOrderYear = '$orderYear' ";
					if($salesOrder != '')
					$sql.=" AND
					trn_orderdetails.intSalesOrderId = '$salesOrder' ";
					
					$sql.=" GROUP BY
					trn_orderheader.intOrderNo,
					trn_orderheader.intOrderYear,
					trn_orderdetails.strSalesOrderNo,
					trn_orderdetails.intSalesOrderId,
					trn_sampleinfomations_details_technical.intColorId,
					trn_sampleinfomations_details_technical.intInkTypeId,
					trn_sampleinfomations_details.intTechniqueId
					/*having INK_STOCK >0*/
					ORDER BY 
					trn_orderheader.intOrderNo asc ,
					trn_orderheader.intOrderYear asc,
					trn_orderdetails.strSalesOrderNo asc,
					trn_orderdetails.intSalesOrderId asc,
					trn_sampleinfomations_details_technical.intColorId asc,
					trn_sampleinfomations_details_technical.intInkTypeId asc,
					trn_sampleinfomations_details.intTechniqueId asc
					
						";

			//echo $sql;

			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				
				$result_s			= $obj_common->load_sales_order_result($row['intOrderNo'],$row['intOrderYear'],$row['intSalesOrderId'],'RunQuery');
				$row_s 				= mysqli_fetch_array($result_s);
				$salesOrder_desc	= $row_s['salesOrder'];
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $row['ORDER_NO']?></td>
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $salesOrder_desc ?></td>
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $row['COLOR'] ?></td>
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $row['TECHNIQUE'] ?></td>
              <td class="normalfntMid" ><?php echo $row['INK'] ?></td>
              <td class="normalfntRight" ><?php echo number_format($row['WEIGHT'],4) ?></td>
              </tr>
            <?php 
			}
	  ?>
            </table>
        </td>
        </tr>
      
      </table>
    </td>
</tr>

<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
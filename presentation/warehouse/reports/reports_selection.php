<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];
//include  "{$backwardseperator}dataAccess/permisionCheck.inc";
//include 	"class/cls_getData.php";

//BEGIN - CLASS OBJECTS	{
//$obj_getData	= new clsGetData;
//END - CLASS OBJECTS	}
$userId 		= $_SESSION['userId'];
$year			= $_SESSION["Year"];
$month			= $_SESSION["Month"];
?>
<title>nsoft | Warehouse - Reports</title>
<!--<script type="text/javascript" src="presentation/warehouse/reports/reports.js"></script>
-->

<!--<form id="frmPayrollReport" name="frmPayrollReport" autocomplete="off">
--><div align="center">
		<div class="trans_layoutL" style="width:700px">
		  <div class="trans_text">Warehouse Reports</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
          
<!--BEGIN - PO Summery-->
		    <tr>
		      <td><table align="left" width="600" class="main_header_table" id="tblPOSummeryHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Supplier wise PO Summery -(PDF).</u></td>
	            </tr>
		        </table></td>
	        </tr>
		    <tr>
		      <td><form name="frmPOSummery" id="frmPOSummery" autocomplete="off">
              <table width="700" align="left" border="0" id="tblPOSummeryBody" class="main_table" cellspacing="0" style="display:none;">
		        <thead>
		          <tr>
		            <td colspan="4" style="text-align:left">Supplier wise PO Summery Selection Criteria -(PDF)<img src="images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
		          <tr>
		            <td width="89" class="normalfnt">Currency</td>
		            <td colspan="3" ><select id="cboCurrency" name="cboCurrency" style="width:120px" class="validate[required]">
	                <?php 
					$sql="SELECT
							mst_financecurrency.intId,
							mst_financecurrency.strCode
							FROM mst_financecurrency
							WHERE
							mst_financecurrency.intStatus =  '1'
							ORDER BY
							mst_financecurrency.strCode ASC";
					$result=$db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row["intId"]."\">".$row["strCode"]."</option>";
					}
					?></select></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">Date From&nbsp;&nbsp;<input name="chkDate" type="checkbox" id="chkDate"  /></td>
		            <td width="144" ><input name="dtFromDate" type="text" value="<?php   //echo date("Y-m-d"); ?>" class="txtbox" id="dtFromDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
		            <td width="16" ><span class="normalfnt">To</span></td>
		            <td width="443" ><input name="dtToDate" type="text" value="<?php  //echo date("Y-m-d"); ?>" class="txtbox" id="dtToDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"  disabled="disabled" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">&nbsp;</td>
		            <td >&nbsp;</td>
		            <td >&nbsp;</td>
		            <td >&nbsp;</td>
	              </tr>
		          <tr>
		            <td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
		              <tr>
		                <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport('POSummery','frmPOSummery')"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
	                  </tr>
	                </table></td>
	              </tr>
	            </tbody>
	          </table></form></td>
	        </tr>
<!--END - PO Summery--> 
<!--BEGIN - PO-GRN Summery-->
		    <tr>
		      <td><table align="left" width="600" class="main_header_table" id="tblPOGRNSummeryHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Supplier wise PO - GRN Summery -(PDF/Excel).</u></td>
	            </tr>
		        </table></td>
	        </tr>
		    <tr>
		      <td><form name="frmPOGRNSummery" id="frmPOGRNSummery" autocomplete="off">
              <table width="700" align="left" border="0" id="tblPOGRNSummeryBody" class="main_table" cellspacing="0" style="display:none;">
		        <thead>
		          <tr>
		            <td colspan="4" style="text-align:left">Supplier wise PO - GRN Summery Selection Criteria -(PDF/EXCEL)<img src="images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
		          <tr>
		            <td width="89" class="normalfnt">Currency</td>
		            <td colspan="3" ><select id="cboCurrency" name="cboCurrency" style="width:120px" class="validate[required]">
	                <?php 
					$sql="SELECT
							mst_financecurrency.intId,
							mst_financecurrency.strCode
							FROM mst_financecurrency
							WHERE
							mst_financecurrency.intStatus =  '1'
							ORDER BY
							mst_financecurrency.strCode ASC";
					$result=$db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row["intId"]."\">".$row["strCode"]."</option>";
					}
					?></select></td>
	              </tr>
		          <tr>
		            <td width="89" class="normalfnt">Supplier</td>
		            <td colspan="3" ><select id="cboSupplier" name="cboSupplier" style="width:290px" class="validate[required]">
		              <option value=""></option>
	                <?php 
					$sql="SELECT
						mst_supplier.intId,
						mst_supplier.strName
						FROM mst_supplier
						WHERE
						mst_supplier.intStatus =  '1'
						ORDER BY
						mst_supplier.strName ASC";
					$result=$db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row["intId"]."\">".$row["strName"]."</option>";
					}
					?></select></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">Date From&nbsp;&nbsp;<input name="chkDate" type="checkbox" id="chkDate"  /></td>
		            <td width="144" ><input name="dtFromDate2" type="text" value="<?php   //echo date("Y-m-d"); ?>" class="txtbox" id="dtFromDate2" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
		            <td width="16" ><span class="normalfnt">To</span></td>
		            <td width="443" ><input name="dtToDate2" type="text" value="<?php  //echo date("Y-m-d"); ?>" class="txtbox" id="dtToDate2" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"  /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">&nbsp;</td>
		            <td >&nbsp;</td>
		            <td >&nbsp;</td>
		            <td >&nbsp;</td>
	              </tr>
		          <tr>
		            <td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
		              <tr>
		                <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport('POGRNSummery','frmPOGRNSummery')"/><img  style="display:inline" border="0" src="images/Treport_exel.png" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport_excel_new1('POGRNSummery','frmPOGRNSummery','excel')"/><a href="main.php"><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
	                  </tr>
	                </table></td>
	              </tr>
	            </tbody>
	          </table></form></td>
	        </tr>
<!--END - PO-GRN Summery--> 
<!--BEGIN - PO-Item Summery-->
<!--		    <tr>-->
<!--		      <td><table align="left" width="600" class="main_header_table" id="tblPOItemSummeryHead" cellpadding="2" cellspacing="0">-->
<!--		        <tr>-->
<!--		          <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Sub Category  Value -(PDF).</u></td>-->
<!--	            </tr>-->
<!--		        </table></td>-->
<!--	        </tr>-->
<!--		    <tr>-->
<!--		      <td><form name="frmPOItemSummery" id="frmPOItemSummery" autocomplete="off">-->
<!--              <table width="700" align="left" border="0" id="tblPOItemSummeryBody" class="main_table" cellspacing="0" style="display:none;">-->
<!--		        <thead>-->
<!--		          <tr>-->
<!--		            <td colspan="4" style="text-align:left">Sub Category Value Selection Criteria -(PDF)<img src="images/close_small.png" align="right" class="mouseover"/></td>-->
<!--	              </tr>-->
<!--	            </thead>-->
<!--		        <tbody>-->
<!--		          <tr>-->
<!--		            <td width="89" class="normalfnt">Currency</td>-->
<!--		            <td colspan="3" ><select id="cboCurrency" name="cboCurrency" style="width:120px" class="validate[required]">-->
<!--	                --><?php //
//					$sql="SELECT
//							mst_financecurrency.intId,
//							mst_financecurrency.strCode
//							FROM mst_financecurrency
//							WHERE
//							mst_financecurrency.intStatus =  '1'
//							ORDER BY
//							mst_financecurrency.strCode ASC";
//					$result=$db->RunQuery($sql);
//					while($row=mysqli_fetch_array($result))
//					{
//						echo "<option value=\"".$row["intId"]."\">".$row["strCode"]."</option>";
//					}
//					?><!--</select></td>-->
<!--	              </tr>-->
<!--		          <tr>-->
<!--		            <td class="normalfnt">Main Category</td>-->
<!--		            <td colspan="3" ><select name="cboMainCategory" style="width:120px" id="cboMainCategory">-->
<!--		              <option value=""></option>-->
<!--		              --><?php
//					$sql = "SELECT
//							mst_maincategory.intId,
//							mst_maincategory.strName
//							FROM mst_maincategory";
//					$result = $db->RunQuery($sql);
//					while($row=mysqli_fetch_array($result))
//					{
//						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
//					}
//				?>
<!--	                </select></td>-->
<!--	              </tr>-->
<!--		          <tr>-->
<!--		            <td class="normalfnt">Sub Category</td>-->
<!--		            <td colspan="3" ><select name="cboSubCategory" style="width:120px" id="cboSubCategory">-->
<!--		              <option value=""></option>-->
<!--	                </select></td>-->
<!--	              </tr>-->
<!--		          <tr>-->
<!--		            <td width="89" class="normalfnt">Item</td>-->
<!--		            <td colspan="3" ><select id="cboItem" name="cboItem" style="width:290px" class="validate[required]">-->
<!--		              <option value=""></option>-->
<!--					  </select></td>-->
<!--	              </tr>-->
<!--		          <tr>-->
<!--		            <td class="normalfnt">Date From&nbsp;&nbsp;<input name="chkDate" type="checkbox" id="chkDate"  /></td>-->
<!--		            <td width="144" ><input name="dtFromDate3" type="text" value="--><?php //  //echo date("Y-m-d"); ?><!--" class="txtbox" id="dtFromDate3" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>-->
<!--		            <td width="16" ><span class="normalfnt">To</span></td>-->
<!--		            <td width="443" ><input name="dtToDate3" type="text" value="--><?php // //echo date("Y-m-d"); ?><!--" class="txtbox" id="dtToDate3" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"  /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>-->
<!--	              </tr>-->
<!--		          <tr>-->
<!--		            <td class="normalfnt">&nbsp;</td>-->
<!--		            <td >&nbsp;</td>-->
<!--		            <td >&nbsp;</td>-->
<!--		            <td >&nbsp;</td>-->
<!--	              </tr>-->
<!--		          <tr>-->
<!--		            <td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">-->
<!--		              <tr>-->
<!--		                <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport('POItemSubCategorySummery','frmPOItemSummery')"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>-->
<!--	                  </tr>-->
<!--	                </table></td>-->
<!--	              </tr>-->
<!--	            </tbody>-->
<!--	          </table></form></td>-->
<!--	        </tr>-->
<!--END - PO-GRN Summery--> 
<!--BEGIN -Sub Cat Wise  PO-GRN Summery-->
		    <tr>
		      <td><table align="left" width="600" class="main_header_table" id="tblSubCatPOGRNSummeryHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Sub Category wise PO - GRN Summery -(PDF/Excel). </u></td>
	            </tr>
		        </table></td>
	        </tr>
		    <tr>
		      <td><form name="frmSubCatPOGRNSummery" id="frmSubCatPOGRNSummery" autocomplete="off">
              <table width="700" align="left" border="0" id="tblSubCatPOGRNSummeryBody" class="main_table" cellspacing="0" style="display:none;">
		        <thead>
		          <tr>
		            <td colspan="4" style="text-align:left">Sub Category wise PO - GRN Summery Selection Criteria -(PDF/EXCEL)<img src="images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
		          <tr>
		            <td width="89" class="normalfnt">Currency</td>
		            <td colspan="3" ><select id="cboCurrency" name="cboCurrency" style="width:120px" class="validate[required]">
	                <?php 
					$sql="SELECT
							mst_financecurrency.intId,
							mst_financecurrency.strCode
							FROM mst_financecurrency
							WHERE
							mst_financecurrency.intStatus =  '1'
							ORDER BY
							mst_financecurrency.strCode ASC";
					$result=$db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row["intId"]."\">".$row["strCode"]."</option>";
					}
					?></select></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">Main Category</td>
		            <td colspan="3" ><select name="cboMainCategory" style="width:120px" id="cboMainCategory">
		              <option value=""></option>
		              <?php
					$sql = "SELECT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
	                </select></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">Sub Category</td>
		            <td colspan="3" ><select name="cboSubCategory" style="width:120px" id="cboSubCategory">
		              <option value=""></option>
	                </select></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">GRN Date From&nbsp;&nbsp;
		              <input name="chkDate" type="checkbox" id="chkDate"  /></td>
		            <td width="154" ><input name="dtFromDate4" type="text" value="<?php   //echo date("Y-m-d"); ?>" class="txtbox" id="dtFromDate4" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
		            <td width="26" ><span class="normalfnt">To</span></td>
		            <td width="372" ><input name="dtToDate4" type="text" value="<?php  //echo date("Y-m-d"); ?>" class="txtbox" id="dtToDate4" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"  /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">&nbsp;</td>
		            <td >&nbsp;</td>
		            <td >&nbsp;</td>
		            <td >&nbsp;</td>
	              </tr>
		          <tr>
		            <td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
		              <tr>
		                <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport('SubCatWisePOGRNSummery','frmSubCatPOGRNSummery')"/><img  style="display:inline" border="0" src="images/Treport_exel.png" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport_excel_new1('SubCatWisePOGRNSummery','frmSubCatPOGRNSummery','excel')"/><a href="main.php"><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
	                  </tr>
	                </table></td>
	              </tr>
	            </tbody>
	          </table></form></td>
	        </tr>
<!--END - PO-GRN Summery--> 

<!--BEGIN -Sub Cat Wise  GRN - PO Summery-->
		    <tr>
		      <td><table align="left" width="600" class="main_header_table" id="tblSubCatGRNPOSummeryHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Sub Category wise GRN - PO Summery -(PDF/Excel). </u></td>
	            </tr>
		        </table></td>
	        </tr>
		    <tr>
		      <td><form name="frmSubCatGRNPOSummery" id="frmSubCatGRNPOSummery" autocomplete="off">
              <table width="700" align="left" border="0" id="tblSubCatGRNPOSummeryBody" class="main_table" cellspacing="0" style="display:none;">
		        <thead>
		          <tr>
		            <td colspan="4" style="text-align:left">Sub Category wise GRN - PO Summery Selection Criteria -(PDF/EXCEL)<img src="images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
		          <tr>
		            <td width="154" class="normalfnt">Currency</td>
		            <td colspan="3" ><select id="cboCurrency" name="cboCurrency" style="width:120px" class="validate[required]">
	                <?php 
					$sql="SELECT
							mst_financecurrency.intId,
							mst_financecurrency.strCode
							FROM mst_financecurrency
							WHERE
							mst_financecurrency.intStatus =  '1'
							ORDER BY
							mst_financecurrency.strCode ASC";
					$result=$db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row["intId"]."\">".$row["strCode"]."</option>";
					}
					?></select></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">Main Category</td>
		            <td colspan="3" ><select name="cboMainCategory" style="width:120px" id="cboMainCategory">
		              <option value=""></option>
		              <?php
					$sql = "SELECT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
	                </select></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">Sub Category</td>
		            <td colspan="3" ><select name="cboSubCategory" style="width:120px" id="cboSubCategory">
		              <option value=""></option>
	                </select></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">GRN Date From&nbsp;&nbsp;
		              <input name="chkDate" type="checkbox" id="chkDate"  /></td>
		            <td width="144" ><input name="dtFromDateGRNPO" type="text" value="<?php   //echo date("Y-m-d"); ?>" class="txtbox" id="dtFromDateGRNPO" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
		            <td width="37" ><span class="normalfnt">To</span></td>
		            <td width="357" ><input name="dtToDateGRNPO" type="text" value="<?php  //echo date("Y-m-d"); ?>" class="txtbox" id="dtToDateGRNPO" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"  /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">&nbsp;</td>
		            <td >&nbsp;</td>
		            <td >&nbsp;</td>
		            <td >&nbsp;</td>
	              </tr>
		          <tr>
		            <td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
		              <tr>
		                <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport('SubCatWiseGRNPOSummery','frmSubCatGRNPOSummery')"/><img  style="display:inline" border="0" src="images/Treport_exel.png" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport_excel_new1('SubCatWiseGRNPOSummery','frmSubCatGRNPOSummery','excel')"/><a href="main.php"><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
	                  </tr>
	                </table></td>
	              </tr>
	            </tbody>
	          </table></form></td>
	        </tr>
<!--END - PO-GRN Summery--> 



















<!--PENDING - PO Summery-->
		    <tr>
		      <td><table align="left" width="600" class="main_header_table" id="tblPOPendingSummeryHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Pending PO Summery -(PDF).</u></td>
	            </tr>
		        </table></td>
	        </tr>
		    <tr>
		      <td><form name="frmPOPendingSummery" id="frmPOPendingSummery" autocomplete="off">
              <table width="700" align="left" border="0" id="tblPOPendingSummeryBody" class="main_table" cellspacing="0" style="display:none;">
		        <thead>
		          <tr>
		            <td colspan="4" style="text-align:left">Pendinge PO Summery Selection Criteria -(PDF)<img src="images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
		          <tr>
		            <td class="normalfnt">Date From&nbsp;&nbsp;<input name="chkDate" type="checkbox" id="chkDate"  /></td>
		            <td width="144" ><input name="dtFromDate5" type="text" value="<?php   //echo date("Y-m-d"); ?>" class="txtbox" id="dtFromDate5" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
		            <td width="16" ><span class="normalfnt">To</span></td>
		            <td width="443" ><input name="dtToDate5" type="text" value="<?php  //echo date("Y-m-d"); ?>" class="txtbox" id="dtToDate5" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"  disabled="disabled" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">&nbsp;</td>
		            <td >&nbsp;</td>
		            <td >&nbsp;</td>
		            <td >&nbsp;</td>
	              </tr>
		          <tr>
		            <td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
		              <tr>
		                <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport('POPendingSummery','frmPOPendingSummery')"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
	                  </tr>
	                </table></td>
	              </tr>
	            </tbody>
	          </table></form></td>
	        </tr>
<!--END - PO Summery--> 

<!--Department wise Issue Details-->
		    <tr>
		      <td><table align="left" width="600" class="main_header_table" id="tblDepWiseIssuesHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Department Wise Issue Notes -(PDF/Excel).</u></td>
	            </tr>
		        </table></td>
	        </tr>
		    <tr>
		      <td><form name="frmDepWiseIssues" id="frmDepWiseIssues" autocomplete="off">
              <table width="700" align="left" border="0" id="tblDepWiseIssuesBody" class="main_table" cellspacing="0" style="display:none;">
		        <thead>
		          <tr>
		            <td colspan="7" style="text-align:left">Department Wise Issue Notes Selection Criteria -(PDF)<img src="images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
          <tr>
            <td width="15%" class="normalfnt">Company <span class="compulsoryRed">*</span></td>
            <td colspan="3	"><select name="cboCompany" id="cboCompany" style="width:220px"  class="txtText validate[required]"  >
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_companies.intId,
							mst_companies.strName
							FROM mst_companies
							WHERE
							mst_companies.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td width="15%" class="normalfnt">Main Category</td>
            <td width="32%" class="normalfnt"><select name="cboMainCategory" style="width:120px" id="cboMainCategory">
		              <option value=""></option>
		              <?php
					$sql = "SELECT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
	                </select></td>
            <td width="1%" class="normalfnt"></td>
          </tr> 
          <tr>
            <td class="normalfnt">Location <span class="compulsoryRed">*</span></td>
            <td colspan="3"><select name="cboLocation" id="cboLocation" style="width:220px"  class="txtText validate[required]"  >
                  <option value=""></option>
            </select></td>
            <td class="normalfnt">Sub Category</td>
            <td class="normalfnt"><select name="cboSubCategory" style="width:120px" id="cboSubCategory">
		              <option value=""></option>
	                </select></td>
            <td class="normalfnt"></td>
          </tr> 
          <tr>
            <td class="normalfnt">Department<?php /*?> <span class="compulsoryRed">*</span><?php */?></td>
            <td colspan="3"><select name="cboDepartment" id="cboDepartment" style="width:220px"  class="txtText<?php /*?> validate[required]<?php */?>"  >
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_department.intId,
							mst_department.strName
							FROM
							mst_department
							WHERE
							mst_department.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td class="normalfnt">Item</td>
            <td class="normalfnt"><select id="cboItem" name="cboItem" style="width:210px">
		              <option value=""></option>
					  </select></td>
            <td class="normalfnt"></td>
          </tr> 
		          <tr>
		            <td class="normalfnt">Date From&nbsp;&nbsp;<input name="chkDate" type="checkbox" id="chkDate"  /></td>
		            <td><input name="dtFromDate6" type="text" value="<?php   //echo date("Y-m-d"); ?>" class="txtbox" id="dtFromDate6" style="width:80px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
		            <td><span class="normalfnt">To</span></td>
		            <td><input name="dtToDate6" type="text" value="<?php  //echo date("Y-m-d"); ?>" class="txtbox" id="dtToDate6" style="width:80px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"  /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
            <td class="normalfnt">Currency</td>
            <td class="normalfnt"><select id="cboCurrency" name="cboCurrency" style="width:120px" >
                  <option value=""></option>
	                <?php 
					$sql="SELECT
							mst_financecurrency.intId,
							mst_financecurrency.strCode
							FROM mst_financecurrency
							WHERE
							mst_financecurrency.intStatus =  '1'
							ORDER BY
							mst_financecurrency.strCode ASC";
					$result=$db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row["intId"]."\">".$row["strCode"]."</option>";
					}
					?></select></td>
            <td class="normalfnt"></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">&nbsp;</td>
		            <td >&nbsp;</td>
		            <td >&nbsp;</td>
		            <td >&nbsp;</td>
            <td class="normalfnt"></td>
            <td class="normalfnt"></td>
            <td class="normalfnt"></td>
	              </tr>
		          <tr>
		            <td colspan="7" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
		              <tr>
		                <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport('departmentWiseIssues','frmDepWiseIssues')"/><img  style="display:inline" border="0" src="images/Treport_exel.png" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport_excel_new1('departmentWiseIssues','frmDepWiseIssues','excel')"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
	                  </tr>
	                </table></td>
	              </tr>
	            </tbody>
	          </table></form></td>
	        </tr>
<!--END - Department wise Issue Details--> 

<!--location wise item transactionss--><!--
		    <tr>
		      <td><table align="left" width="600" class="main_header_table" id="tblLocationItemsTransHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Location Wise General Item Transactions -(GRID).</u></td>
	            </tr>
		        </table></td>
	        </tr>
		    <tr>
		      <td><form name="frmLocationItemsTrans" id="frmLocationItemsTrans" autocomplete="off">
              <table width="700" align="left" border="0" id="tblLocationItemsTransBody" class="main_table" cellspacing="0" style="display:none;">
		        <thead>
		          <tr>
		            <td colspan="4" style="text-align:left">Location Item Transactions Criteria -(PDF)<img src="images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
          <tr>
            <td width="15%" class="normalfnt">Company</td>
            <td colspan="3	"><select name="cboCompany" id="cboCompany" style="width:250px"  class="txtText validate[required]"  >
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_companies.intId,
							mst_companies.strName
							FROM mst_companies
							WHERE
							mst_companies.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
          </tr> 
          <tr>
            <td class="normalfnt">Location</td>
            <td colspan="3"><select name="cboLocation" id="cboLocation" style="width:250px"  class="txtText validate[required]"  >
                  <option value=""></option>
            </select></td>
          </tr> 
		          <tr>
		            <td class="normalfnt">Main Category</td>
		            <td colspan="3" ><select name="cboMainCategory" style="width:120px" id="cboMainCategory">
		              <option value=""></option>
		              <?php
					$sql = "SELECT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
	                </select></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">Sub Category</td>
		            <td colspan="3" ><select name="cboSubCategory" style="width:120px" id="cboSubCategory">
		              <option value=""></option>
	                </select></td>
	              </tr>
		          <tr>
		            <td width="89" class="normalfnt">Item</td>
		            <td colspan="3" ><select id="cboItem" name="cboItem" style="width:290px" class="validate[required]">
		              <option value=""></option>
					  </select></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">Date From&nbsp;&nbsp;<input name="chkDate" type="checkbox" id="chkDate"  /></td>
		            <td width="22%"><input name="dtFromDate9" type="text" value="<?php   //echo date("Y-m-d"); ?>" class="txtbox" id="dtFromDate9" style="width:120px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
		            <td width="3%"><span class="normalfnt">To</span></td>
		            <td width="60%"><input name="dtToDate9" type="text" value="<?php  //echo date("Y-m-d"); ?>" class="txtbox" id="dtToDate9" style="width:120px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"  /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">&nbsp;</td>
		            <td >&nbsp;</td>
		            <td >&nbsp;</td>
		            <td >&nbsp;</td>
	              </tr>
		          <tr>
		            <td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
		              <tr>
		                <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onclick="ViewReport('locationItemsTransactions','frmLocationItemsTrans')"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
	                  </tr>
	                </table></td>
	              </tr>
	            </tbody>
	          </table></form></td>
	        </tr>-->
<!--END - Department wise Issue Details--> 

<!--location wise item transactionss-->
		    <tr>
		      <td><table align="left" width="600" class="main_header_table" id="tblLocationItemsTransHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>General Item Movement -(Excel).</u></td>
	            </tr>
		        </table></td>
	        </tr>
		    <tr>
		      <td><form name="frmLocationItemsTrans" id="frmLocationItemsTrans" autocomplete="off">
              <table width="700" align="left" border="0" id="tblLocationItemsTransBody" class="main_table" cellspacing="0" style="display:none;">
		        <thead>
		          <tr>
		            <td width="270%" colspan="4" style="text-align:left">General Item Movement -(Excel)<img src="images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
          <tr>
	              <td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
		              <tr>
                      <?php		 

 						$formLink		= "?q=885";	
					  ?>
		                <td width="100%" align="center" bgcolor=""><a href="<?php echo $formLink; ?>" target="_blank"><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" /></a><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
	                  </tr>
                  </table></td>
	              </tr>
	            </tbody>
	          </table></form></td>
	        </tr>
<!--END - Department wise Issue Details--> 

<!--STOCK BALANCE Summery-->
		    <tr>
		      <td><table align="left" width="600" class="main_header_table" id="tblStockBalanceHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Stock Balance.</u></td>
	            </tr>
		        </table></td>
	        </tr>
		    <tr>
		      <td><form name="frmStockBalance" id="frmStockBalance" autocomplete="off">
              <table width="700" align="left" border="0" id="tblStockBalanceBody" class="main_table" cellspacing="0" style="display:none;">
		        <thead>
		          <tr>
		            <td colspan="4" style="text-align:left">Stock Balance Selection Criteria<img src="images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
          <tr>
          <td width="17%"></td>
            <td width="15%" height="27" class="normalfnt">Company</td>
            <td width="30%"><select name="cboCompany" id="cboCompany" style="width:250px"  class="txtText" >
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_companies.intId,
							mst_companies.strName
							FROM mst_companies
							WHERE
							mst_companies.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td width="14%">&nbsp;</td>
            <td width="24%">&nbsp;</td>
          </tr>
<tr>
          <td width="17%"></td>
            <td width="15%" height="27" class="normalfnt">Currency</td>
            <td width="30%"><select name="cboCurrency" id="cboCurrency" style="width:75px"  class="validate[required] txtText" >
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_financecurrency.intId,
							mst_financecurrency.strCode
							FROM mst_financecurrency
							WHERE
							mst_financecurrency.intStatus =  '1'
							and mst_financecurrency.intId = 2
							";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";	
					}
				?>
            </select></td>
            <td width="14%">&nbsp;</td>
            <td width="24%">&nbsp;</td>
          </tr> 
          <tr>
          <td></td>
            <td width="15%" height="27" class="normalfnt">Location</td>
            <td width="30%"><select name="cboLocation" id="cboLocation" style="width:250px"  class="txtText" >
                  <option value=""></option>
            </select></td>
            <td></td>
            <td></td>
          </tr>

          
<tr>
          <td></td>
            <td colspan="2">
<div id="divTable1" style="overflow:scroll;width:100%;height:150px"  >
<table width="97%" id="tblMainCategory" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
        <tr class="" >
          <td width="44" bgcolor="#FAD163" class="normalfntMid"><input type="checkbox" name="chkAll2" id="chkAll2" />
          </td>
          <td width="234"  height="24" bgcolor="#FAD163" class="normalfntMid"><strong>Main Categories</strong></td>
          </tr>
        
                  <?php
					$sql = "SELECT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory";
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						$i++;
						$categoryId[$i]=$row['intId'];
						$category[$i]=$row['strName'];
						?>
<tr class="normalfnt">
          <td bgcolor="#FFFFFF" class="normalfntMid"><input name="chkSelect" type="checkbox" value="<?php echo $i; ?>" class="mainCateg" /></td>
          <td bgcolor="#FFFFFF" class="normalfntMid" id="<?php echo $row['intId']; ?>"><?php echo $row['strName']; ?></td>
                  <?php
					}
				?>
 </table> </div></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
          <td colspan="2" align="center"><table width="100%" id="sbCategory" border="0" cellpadding="0" cellspacing="1"> 
          <tr>
            <td colspan="5"><div class="normalfnt" style="text-align:center; font-size:11px; background-color:#FAD163" ><b>Sub Categories</b></div>
</td>
          </tr> 
                <?php
					$sql = "SELECT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory";
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						$i++;
						$categoryId[$i]=$row['intId'];
						$category[$i]=$row['strName'];
					//	$height=QueryCount($sql)*10;
					
				$sql1 = "SELECT
						mst_subcategory.intId,
						mst_subcategory.strCode,
						mst_subcategory.strName
						FROM mst_subcategory
						WHERE
						mst_subcategory.intMainCategory =  '$categoryId[$i]'";
						$result2 = $db->RunQuery($sql1);
						$no =  	mysqli_num_rows($result2);
									
						 $height=$no*30+18;
						 if($height>150){
							$height	=150; 
						 }
						 
						 if($no==0){
							$width=0; 
							$height=10;
						 }
						 else{
							$width=600; 
						 }
					//	$height=100;
						?>
          <tr>
            <td colspan="5"><div id="<?php echo "div".$i; ?>" style="display:none">
  <div class="normalfnt subcategory" style="text-align:left; font-size:11px; background-color:#FFD8B0" ><b><input name="subcheckAll" id="<?php echo $categoryId[$i]; ?>" type="checkbox" value="<?php echo $categoryId[$i]; ?>" class="subcheckAll subcheckAll<?php echo $categoryId[$i]; ?>"/>&nbsp;<?php echo $category[$i]; ?></b></div>
<div id="<?php echo "divTable".$i; ?>" style="overflow:scroll;width:<?php echo $width; ?>px;height:<?php echo $height; ?>px"  >
  <table width="100%" id="<?php echo "tbl".$i; ?>" >
                  <?php
				$result1 = $db->RunQuery($sql1);
				while($row1=mysqli_fetch_array($result1))
				{
				?>
        <tr class="chkRow">
          <td class="normalfnt" align="left"><input name="chkPayMethod" id="<?php echo $row1['intId']; ?>" type="checkbox" value="<?php echo $row1['intId']; ?>"  class="subcheck subcheck<?php echo $categoryId[$i]; ?>"/>&nbsp;<?php echo $row1['strName']; ?></td>
        </tr>
                  <?php
					}
				?>
        
      </table></div></div> <!--divBnkDepositFields-->
</td>
          </tr>
                  <?php
					}
				?>
          </table></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
          <td></td>
            <td width="15%" height="27" class="normalfnt">Item </td>
            <td width="30%"><select name="cboItems" id="cboItems" style="width:250px"  class="txtText" >
                  <option value=""></option>
            </select></td>
        <td>&nbsp;</td>
            <td></td>
          </tr>
<tr>
          <td></td>
            <td width="15%" height="27" class="normalfnt">Date Up To&nbsp;&nbsp;<input name="chkDate" type="checkbox" id="chkDate"  /></td>
            <td width="30%"><input name="dtDateTo7" type="text"  disabled="disabled" value="" class="txtbox" id="dtDateTo7" style="width:100px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /><strong class="normalfnt"></strong>&nbsp;&nbsp;
        <td><input type="text" name="txtNoOfTables" id="txtNoOfTables" value="<?php echo $i?>" style="display:none" />&nbsp;</td>
            <td></td>
          </tr>

          <tr>
              <td colspan="5"><table width="100%" border="0" cellpadding="0" cellspacing="0"><tr>
                          <td width="32%" height="27" class="normalfnt"></td>
                          <td width="13%" height="27" class="normalfnt"><input type="radio" name="radioItemType" id="chkActive" checked="checked" value="1"/>
                              Active Items</td>
                          <td width="0%" height="27" class="normalfnt"></td>
                          <td width="17%" height="27" class="normalfnt"><input type="radio" name="radioItemType" id="chkInactive" value="0"/>
                              Inactive Items</td>
                          <td width="0%" height="27" class="normalfnt"></td>
                          <td width="17%" height="27" class="normalfnt"><input type="radio" name="radioItemType" id="chkAllItem" value="2"/>
                              All Items</td>
                          <td width="15%" height="27" class="normalfnt">&nbsp;</td>
                      </tr>
                  </table></td>
          </tr>
              <tr>
          <td colspan="5"><table width="100%" border="0" cellpadding="0" cellspacing="0"><tr>
          <td width="32%" height="27" class="normalfnt"></td>
          <td width="13%" height="27" class="normalfnt"><input type="radio" name="radioReptType" id="chkNormal" checked="checked" value="1"/>
            QTY Report</td>
          <td width="0%" height="27" class="normalfnt"></td>
          <td width="17%" height="27" class="normalfnt"><input type="radio" name="radioReptType" id="chkValue" value="2"/>
            Value Report</td>
          <td width="23%" height="27" class="normalfnt"><img src="images/smallSearch.png" width="24" height="24" class="mouseover" id="imgSearchItems" name="imgSearchItems"  /></td>
          <td width="15%" height="27" class="normalfnt">&nbsp;</td>
          </tr>
          </table></td>
          </tr>
        	            </tbody>
	          </table></form></td>
	        </tr>
<!--END - STOCK BALANCE  --> 
<!--STOCK BALANCE BULK All Summery-->
<?php
if(loadViewMode('P1137',$userId)==1){
?>
		    <tr>
		      <td><table align="left" width="600" class="main_header_table" id="tblStockBalanceBulkAllHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Bulk Stock Balance(Multiple Companies/Locations).</u></td>
	            </tr>
		        </table></td>
	        </tr>
		    <tr>
		      <td><form name="frmStockBalanceBulkAll" id="frmStockBalanceBulkAll" autocomplete="off">
              <table width="700" align="left" border="0" id="tblStockBalanceBulkAllBody" class="main_table" cellspacing="0"  style="display:none;">
		        <thead>
		          <tr>
		            <td colspan="4" style="text-align:left">Bulk Stock Balance (Multiple Companies/Locations) Selection Criteria<img src="images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
          <tr>
          <td width="17%"></td>
            <td width="15%" height="27" class="normalfnt">Company</td>
            <td width="30%"><select name="cboCompany" id="cboCompany" style="width:250px"  class="txtText" >
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_companies.intId,
							mst_companies.strName
							FROM mst_companies
							WHERE
							mst_companies.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td width="14%">&nbsp;</td>
            <td width="24%">&nbsp;</td>
          </tr>
<tr>
          <td width="17%"></td>
            <td width="15%" height="27" class="normalfnt">Currency</td>
            <td width="30%"><select name="cboCurrency" id="cboCurrency" style="width:75px"  class="validate[required] txtText" >
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_financecurrency.intId,
							mst_financecurrency.strCode
							FROM mst_financecurrency
							WHERE
							mst_financecurrency.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";	
					}
				?>
            </select></td>
            <td width="14%">&nbsp;</td>
            <td width="24%">&nbsp;</td>
          </tr> 
          <tr>
          <td></td>
            <td width="15%" height="27" class="normalfnt">Location</td>
            <td width="30%"><select name="cboLocation" id="cboLocation" style="width:250px"  class="txtText" >
                  <option value=""></option>
            </select></td>
            <td></td>
            <td></td>
          </tr>
          
<tr>
  <td></td>
  <td colspan="2">
  <div id="divTable1" style="overflow:scroll;width:100%;height:150px"  >
  <table width="97%" id="tblMainCategory" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
    <tr class="" >
      <td width="44" bgcolor="#FAD163" class="normalfntMid"><input type="checkbox" name="chkAll2" id="chkAll2" />
        </td>
      <td width="234"  height="24" bgcolor="#FAD163" class="normalfntMid"><strong>Main Categories</strong></td>
      </tr>
    
    <?php
					$sql = "SELECT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory";
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						$i++;
						$categoryId[$i]=$row['intId'];
						$category[$i]=$row['strName'];
						?>
  <tr class="normalfnt">
    <td bgcolor="#FFFFFF" class="normalfntMid"><input name="chkSelect" type="checkbox" value="<?php echo $i; ?>" class="mainCateg" /></td>
    <td bgcolor="#FFFFFF" class="normalfntMid" id="<?php echo $row['intId']; ?>"><?php echo $row['strName']; ?></td>
    <?php
					}
				?>
    </table> </div></td>
  <td></td>
  <td></td>
</tr>
          <tr>
            <td></td>
          <td colspan="2" align="center"><table width="100%" id="sbCategory" border="0" cellpadding="0" cellspacing="1"> 
          <tr>
            <td colspan="5"><div class="normalfnt" style="text-align:center; font-size:11px; background-color:#FAD163" ><b>Sub Categories</b></div></td>
          </tr> 
                <?php
					$sql = "SELECT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory";
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						$i++;
						$categoryId[$i]=$row['intId'];
						$category[$i]=$row['strName'];
					//	$height=QueryCount($sql)*10;
					
				$sql1 = "SELECT
						mst_subcategory.intId,
						mst_subcategory.strCode,
						mst_subcategory.strName
						FROM mst_subcategory
						WHERE
						mst_subcategory.intMainCategory =  '$categoryId[$i]'";
						$result2 = $db->RunQuery($sql1);
						$no =  	mysqli_num_rows($result2);
									
						 $height=$no*30+18;
						 if($height>150){
							$height	=150; 
						 }
						 
						 if($no==0){
							$width=0; 
							$height=10;
						 }
						 else{
							$width=600; 
						 }
					//	$height=100;
						?>
          <tr>
            <td colspan="5"><div id="<?php echo "divBulk".$i; ?>" style="display:none">
  <div class="normalfnt subcategory" style="text-align:left; font-size:11px; background-color:#FFD8B0" ><b><input name="subcheckAll" id="<?php echo $categoryId[$i]; ?>" type="checkbox" value="<?php echo $categoryId[$i]; ?>" class="subcheckAll subcheckAll<?php echo $categoryId[$i]; ?>"/>&nbsp;<?php echo $category[$i]; ?></b></div>
<div id="<?php echo "divTable".$i; ?>" style="overflow:scroll;width:<?php echo $width; ?>px;height:<?php echo $height; ?>px"  >
  <table width="100%" id="<?php echo "tbl".$i; ?>" >
                  <?php
				$result1 = $db->RunQuery($sql1);
				while($row1=mysqli_fetch_array($result1))
				{
				?>
        <tr class="chkRow">
          <td class="normalfnt" align="left"><input name="chkPayMethod" id="<?php echo $row1['intId']; ?>" type="checkbox" value="<?php echo $row1['intId']; ?>"  class="subcheck subcheck<?php echo $categoryId[$i]; ?>"/>&nbsp;<?php echo $row1['strName']; ?></td>
        </tr>
                  <?php
					}
				?>
        
      </table></div></div> <!--divBnkDepositFields-->
</td>
          </tr>
                  <?php
					}
				?>
          </table></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
          <td></td>
            <td width="15%" height="27" class="normalfnt">Item </td>
            <td width="30%"><select name="cboItems" id="cboItems" style="width:250px"  class="txtText" >
                  <option value=""></option>
            </select></td>
        <td>&nbsp;</td>
            <td></td>
          </tr>
<tr>
          <td></td>
            <td width="15%" height="27" class="normalfnt">Date Range From&nbsp;&nbsp;<input name="chkDate" type="checkbox" id="chkDate"  /></td>
            <td width="30%"><input name="dtDateFrom9" type="text"  disabled="disabled" value="" class="txtbox" id="dtDateFrom9" style="width:100px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /><strong class="normalfnt">To</strong>&nbsp;&nbsp;
<input name="dtDateTo9" type="text"  disabled="disabled" value="" class="txtbox" id="dtDateTo9" style="width:100px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" />            </td>
        <td><input type="text" name="txtNoOfTables" id="txtNoOfTables" value="<?php echo $i?>" style="display:none" />&nbsp;<img src="images/smallSearch.png" width="24" height="24" class="mouseover" id="imgSearchItems" name="imgSearchItems"  /></td>
            <td align="right"></td>
          </tr>  
              <tr>
          <td colspan="5"></td>
          </tr>
        	            </tbody>
	          </table></form></td>
	        </tr>
<!--END - STOCK BALANCE BULK ALL  --> 
<?php
}
?>

<!--BEGIN - STOCK BALANCE EXCELL-->
<!--		    <tr>-->
<!--		      <td><table align="left" width="600" class="main_header_table" id="tblStkBalExcellHead" cellpadding="2" cellspacing="0">-->
<!--		        <tr>-->
<!--		          <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Company Wise Stock Balance -(Excel).</u></td>-->
<!--	            </tr>-->
<!--		        </table></td>-->
<!--	        </tr>-->
<!--		    <tr>-->
<!--		      <td><form name="frmCompanyStockBalance" id="frmCompanyStockBalance" autocomplete="off">-->
<!--              <table width="700" align="left" border="0" id="tblStkBalExcellBody" class="main_table" cellspacing="0" style="display:none;">-->
<!--		        <thead>-->
<!--		          <tr>-->
<!--		            <td colspan="4" style="text-align:left">Company Wise Stock Balance -(Excell) Selection Criteria<img src="images/close_small.png" align="right" class="mouseover"/></td>-->
<!--	              </tr>-->
<!--	            </thead>-->
<!--		        <tbody>-->
<!--<tr>-->
<!--        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">-->
<!--          <tr>-->
<!--            <td width="15%" class="normalfnt">Company</td>-->
<!--            <td width="40%"><select name="cboCompany" id="cboCompany" style="width:250px"  class="txtText "  >-->
<!--                  <option value=""></option>-->
<!--                  --><?php
//					$sql = "SELECT
//							mst_companies.intId,
//							mst_companies.strName
//							FROM mst_companies
//							WHERE
//							mst_companies.intStatus =  '1'";
//					$result = $db->RunQuery($sql);
//					while($row=mysqli_fetch_array($result))
//					{
//						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
//					}
//				?>
<!--            </select></td>-->
<!--            <td width="9%" class="normalfnt"></td>-->
<!--            <td width="36%"></td>-->
<!--        <td></td>-->
<!--          </tr> -->
<!--          <tr>-->
<!--            <td width="15%" class="normalfnt">Location</td>-->
<!--            <td width="40%"><select name="cboLocation" id="cboLocation" style="width:250px"  class="txtText"  >-->
<!--                  <option value=""></option>-->
<!--            </select></td>-->
<!--            <td width="9%" class="normalfnt">Date&nbsp;&nbsp;<input id="chkDate" type="checkbox" name="chkDate"></td>-->
<!--            <td width="36%"><input name="dtDateToS" type="text"  disabled="disabled" value="" class="txtbox" id="dtDateToS" style="width:100px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" />            </td>-->
<!--        <td></td>-->
<!--          </tr> -->
<!--          <tr>-->
<!--            <td width="15%" class="normalfnt">Currency</td>-->
<!--            <td width="40%"><select id="cboCurrency" name="cboCurrency" style="width:120px" >-->
<!--                  <option value=""></option>-->
<!--	                --><?php //
//					$sql="SELECT
//							mst_financecurrency.intId,
//							mst_financecurrency.strCode
//							FROM mst_financecurrency
//							WHERE
//							mst_financecurrency.intStatus =  '1'
//							ORDER BY
//							mst_financecurrency.strCode ASC";
//					$result=$db->RunQuery($sql);
//					while($row=mysqli_fetch_array($result))
//					{
//						echo "<option value=\"".$row["intId"]."\">".$row["strCode"]."</option>";
//					}
//					?><!--</select></td>-->
<!--            <td width="9%" class="normalfnt"></td>-->
<!--            <td width="36%"></td>-->
<!--        <td></td>-->
<!--          </tr> -->
<!--              <tr>-->
<!--                <td class="normalfnt"></td>-->
<!--                <td class="normalfnt"></td>-->
<!--                <td class="normalfnt">&nbsp;</td>-->
<!--                <td class="normalfnt"><img src="images/Treport.jpg" style="display:inline" border="0"  alt="download excell" id="butDownload" name="butDownload" /></td>-->
<!--              </tr>-->
<!---->
<!--        </table></td>-->
<!--      </tr>	            </tbody>-->
<!--	          </table></form></td>-->
<!--	        </tr>-->
<!--END - STOCK BALANCE EXCELL--> 
<!--BEGIN - GENERAL STOCK MOVEMENT-->
		    <tr>
		      <td><table align="left" width="600" class="main_header_table" id="tblGeneralStockMovementHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>General Stock Movement.</u></td>
	            </tr>
		        </table></td>
	        </tr>
		    <tr>
		      <td><form name="frmGeneralStockMovement" id="frmGeneralStockMovement" autocomplete="off">
              <table width="700" align="left" border="0" id="tblGeneralStockMovementBody" class="main_table" cellspacing="0" style="display:none;">
		        <thead>
		          <tr>
		            <td colspan="4" style="text-align:left">General Stock Movement Selection Criteria<img src="images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
<tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
          <td width="17%" height="27" class="normalfnt">Company</td>
            <td width="64%"><select name="cboCompany" id="cboCompany" style="width:250px"  class="validate[required] txtText" >
              <option value=""></option>
              <?php
					$sql = "SELECT
							mst_companies.intId,
							mst_companies.strName
							FROM mst_companies
							WHERE
							mst_companies.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
					}
				?>
            </select></td>
            <td width="4%">&nbsp;</td>
            <td width="15%">&nbsp;</td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Location</td>
            <td width="64%"><select name="cboLocation" id="cboLocation" style="width:250px"  class="validate[required] txtText" >
              <option value=""></option>
              </select></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Main Category</td>
            <td width="64%"><select name="cboMainCategory" style="width:250px" id="cboMainCategory">
              <option value=""></option>
              <?php
					$sql = "SELECT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
					}
				?>
              </select></td>
            <td></td>
            <td></td>
          </tr>          <tr>
          <td height="27" class="normalfnt">Sub Category</td>
            <td width="64%"><select name="cboSubCategory" id="cboSubCategory" style="width:250px"  class="validate[required] txtText" >
              <option value=""></option>
            </select></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Item </td>
            <td><select name="cboItems" id="cboItems" style="width:250px"  class="validate[required] txtText" >
              <option value=""></option>
            </select></td>
            <td>&nbsp;</td>
            <td></td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Date</td>
            <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="4%" class="normalfnt"><input name="chkDate" type="checkbox" id="chkDate"  /></td>
                <td width="6%" class="normalfnt">From</td>
                <td width="19%"><input name="dtDateFrom8" type="text" disabled="disabled"  class="txtbox" id="dtDateFrom8" style="width:80px;"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                <td width="3%" class="normalfnt">To</td>
                <td width="68%"><input name="dtDateTo8" type="text" disabled="disabled"  class="txtbox" id="dtDateTo8" style="width:80px;"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              </tr>
            </table></td>
          </tr>
          <tr>
          <td height="27" class="normalfnt">&nbsp;</td>
            <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="16%" class="normalfnt">Normal Report</td>
                <td width="10%"><input name="radio" type="radio" class="mouseover" id="chkNormal" value="1" checked="checked" /></td>
                <td width="16%" class="normalfnt">Details Report</td>
                <td width="43%"><input name="radio" type="radio" class="mouseover" id="chkDetails" value="radio" /></td>
                <td width="7%">&nbsp;</td>
                <td width="8%">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
          <tr>
          <td colspan="4"><table width="100%" border="0" cellpadding="0" cellspacing="0"><tr>
          <td width="30%" height="27" class="normalfnt"></td>
          <td height="27" align="center" ><img src="images/Treport.jpg" width="92" height="24" class="mouseover" id="imgSearchItems" name="imgSearchItems"  /></td>
          <td width="26%" height="27" class="normalfnt">&nbsp;</td>
          <td width="15%" height="27" class="normalfnt">&nbsp;</td>
          </tr></table></td>
          </tr>
        </table></td>
      </tr>                </tbody>
	          </table></form></td>
	        </tr>
<!--END - GENERAL STOCK MOVEMENT-->
<!--BEGIN - STYLE STOCK MOVEMENT-->
<!--		    <tr>-->
<!--		      <td><table align="left" width="600" class="main_header_table" id="tblStyleStockMovementHead" cellpadding="2" cellspacing="0">-->
<!--		        <tr>-->
<!--		          <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Style Stock Movement.</u></td>-->
<!--	            </tr>-->
<!--		        </table></td>-->
<!--	        </tr>-->
<!--		    <tr>-->
<!--		      <td><form name="frmStyleStockMovement" id="frmStyleStockMovement" autocomplete="off">-->
<!--              <table width="700" align="left" border="0" id="tblStyleStockMovementBody" class="main_table" cellspacing="0" style="display:none;">-->
<!--		        <thead>-->
<!--		          <tr>-->
<!--		            <td colspan="4" style="text-align:left">Style Stock Movement<img src="images/close_small.png" align="right" class="mouseover"/></td>-->
<!--	              </tr>-->
<!--	            </thead>-->
<!--		        <tbody>-->
<!--<tr>-->
<!--        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">-->
<!--          <tr>-->
<!--            <td width="100%"><table width="100%" border="0" cellpadding="0" cellspacing="0"><tr>-->
<!--              <td width="30%" height="27" class="normalfnt"></td>-->
<!--              <td height="27" align="center" ><img src="images/Treport.jpg" width="92" height="24" class="mouseover" id="imgSearchItems" name="imgSearchItems"  /></td>-->
<!--              <td width="26%" height="27" class="normalfnt">&nbsp;</td>-->
<!--              <td width="15%" height="27" class="normalfnt">&nbsp;</td>-->
<!--              </tr></table></td>-->
<!--          </tr>-->
<!--        </table></td>-->
<!--      </tr>       			</tbody>-->
<!--	          </table></form></td>-->
<!--	        </tr>-->
<!--END - STYLE STOCK BALANCE MOVEMENT--> 
<!--BEGIN - INTERNAL ONLOAN TRANSACTON-->
		    <tr>
		      <td><table align="left" width="600" class="main_header_table" id="tblInternalOnloanTrnsActHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Internal Onloan Transaction -(PDF).</u></td>
	            </tr>
		        </table></td>
	        </tr>
		    <tr>
		      <td><form name="frmInternalOnloanTrnsAct" id="frmInternalOnloanTrnsAct" autocomplete="off">
                            <table width="700" align="left" border="0" id="tblInternalOnloanTrnsActBody" class="main_table" cellspacing="0" style="display:none;">
		        <thead>
		          <tr>
		            <td colspan="4" style="text-align:left">Internal Onloan Transaction Criteria -(PDF)<img src="images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
          <tr>
            <td width="15%" class="normalfnt">Company</td>
            <td colspan="3	"><select name="cboCompany" id="cboCompany" style="width:250px"  class="txtText validate[required]"  >
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_companies.intId,
							mst_companies.strName
							FROM mst_companies
							WHERE
							mst_companies.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
          </tr> 
          <tr>
            <td class="normalfnt">Location</td>
            <td colspan="3"><select name="cboLocation" id="cboLocation" style="width:250px"  class="txtText validate[required]"  >
                  <option value=""></option>
            </select></td>
          </tr> 
		          <tr>
		            <td class="normalfnt">Main Category</td>
		            <td colspan="3" ><select name="cboMainCategory" style="width:120px" id="cboMainCategory">
		              <option value=""></option>
		              <?php
					$sql = "SELECT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
	                </select></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">Sub Category</td>
		            <td colspan="3" ><select name="cboSubCategory" style="width:120px" id="cboSubCategory">
		              <option value=""></option>
	                </select></td>
	              </tr>
		          <tr>
		            <td width="89" class="normalfnt">Item</td>
		            <td colspan="3" ><select id="cboItem" name="cboItem" style="width:290px" class="">
		              <option value=""></option>
					  </select></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">Date From&nbsp;&nbsp;<input name="chkDate" type="checkbox" id="chkDate"  /></td>
		            <td width="22%"><input name="dtFromDate10" type="text" value="<?php   //echo date("Y-m-d"); ?>" class="txtbox" id="dtFromDate10" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
		            <td width="3%"><span class="normalfnt">To</span></td>
		            <td width="60%"><input name="dtToDate10" type="text" value="<?php  //echo date("Y-m-d"); ?>" class="txtbox" id="dtToDate10" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"  /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
	              </tr>
		          <tr>
		            <td width="89" class="normalfnt">Type</td>
		            <td colspan="3" ><select id="cboRptType" name="cboRptType" style="width:290px" class="">
		              <option value="1">Loan-In</option>
		              <option value="2">Loan-Out</option>
					  </select></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">&nbsp;</td>
		            <td >&nbsp;</td>
		            <td >&nbsp;</td>
		            <td >&nbsp;</td>
	              </tr>
		          <tr>
		            <td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
		              <tr>
		                <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport_pdf('InternalOnloanTrnsActions_loanIn','frmInternalOnloanTrnsAct')"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
	                  </tr>
	                </table></td>
	              </tr>
	            </tbody>
	          </table></form></td>
	        </tr>
<!--END - INTERNAL ONLOAN TRANSACTON-->

<!--BEGIN - GRN Summary List-->
		    <tr>
		      <td><table align="left" width="600" class="main_header_table" id="tblGrnSummaryHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>GRN Summary List.</u></td>
	            </tr>
		        </table></td>
	        </tr>
		    <tr>
		      <td><form name="frmGrnSummary" id="frmGrnSummary" autocomplete="off">
              <table width="100%" align="left" border="0" id="tblGrnSummaryBody" class="main_table" cellspacing="0" style="display:none;"  >
		        <thead>
		          <tr>
		            <td colspan="4" style="text-align:left">GRN Summary List Criteria<img src="images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
<tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="23%" class="normalfnt">Company</td>
            <td width="77%" colspan="2"><select name="cboCompany" id="cboCompany" class="txtText" style="width:250px" >
              <option value=""></option>
              <?php
					$sql = "SELECT
							mst_companies.intId,
							mst_companies.strName
							FROM mst_companies
							WHERE
							mst_companies.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            </tr> 
          <tr>
            <td width="23%" class="normalfnt">Location</td>
            <td colspan="2"><select name="cboLocation" id="cboLocation" class="txtText" style="width:250px" >
              <option value=""></option>
            </select></td>
            </tr> 
         
          <tr>
            <td class="normalfnt">Supplier</td>
            <td colspan="2" ><select name="cboSupplier" id="cboSupplier" class="txtText" style="width:250px" >
              <option value=""></option>
              <?php
			  	$sql = "SELECT intId,strName
						FROM mst_supplier
						WHERE intStatus = 1
						ORDER BY strName"; 
				
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
				}
			  ?>
            </select></td>
            </tr>
          
          <tr><td colspan="3">&nbsp;</td>
          </tr> 
              <tr>
                <td colspan="3" class="normalfntMid"><img src="images/Treport.jpg" style="display:inline" border="0"  alt="view report" id="butReport" name="butReport" class="mouseover" /></td>
                </tr>

         </table></td>
      </tr>       			</tbody>
	          </table></form></td>
	        </tr>
<!--END - Allocated items STOCK BALANCE--> 
   
<!--BEGIN - ITEMS EXPIRY REPORT-->
		    <tr>
		      <td><table align="left" width="600" class="main_header_table" id="tblItemsExpiryHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>RM Expiry Report.</u></td>
	            </tr>
		        </table></td>
	        </tr>
		    <tr>
		      <td><form name="frmItemsExpiry" id="frmItemsExpiry" autocomplete="off">
              <table width="100%" align="left" border="0" id="tblItemsExpiryBody" class="main_table" cellspacing="0"  style="display:none;"   >
		        <thead>
		          <tr>
		            <td colspan="4" style="text-align:left">RM Expiry Report Selection List Criteria<img src="images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
<tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="18%" class="normalfnt">Company</td>
            <td colspan="2"><select name="cboCompany" id="cboCompany" class="txtText" style="width:250px" >
              <option value=""></option>
              <?php
					$sql = "SELECT
							mst_companies.intId,
							mst_companies.strName
							FROM mst_companies
							WHERE
							mst_companies.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td width="1%"></td>
            <td width="1%"></td>
            </tr> 
          <tr>
            <td width="18%" class="normalfnt">Location</td>
            <td colspan="2"><select name="cboLocation" id="cboLocation" class="txtText" style="width:250px" >
              <option value=""></option>
            </select></td>
            <td></td>
            <td></td>
            </tr> 
         
          <tr>
            <td class="normalfnt">Main Category</td>
            <td colspan="2" ><select name="cboMainCategory" style="width:250px" id="cboMainCategory">
              <option value=""></option>
              <?php
					$sql = "SELECT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory";
					//$sql .= " WHERE mst_maincategory.intId IN ('1','2') ";
					$sql	.= " ORDER BY mst_maincategory.strName ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select></td>
            <td>&nbsp;</td>
            <td >&nbsp;</td>
            <td width="1%"></td>
          </tr>
          <tr>
            <td class="normalfnt">Sub Category</td>
            <td colspan="2"><select name="cboSubCategory" id="cboSubCategory" style="width:250px"  class="validate[required] txtText" >
              <option value=""></option>
            </select></td>
            <td>&nbsp;</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td class="normalfnt">Item </td>
            <td colspan="2"><select name="cboItems" id="cboItems" style="width:250px"  class="validate[required] txtText" >
              <option value=""></option>
              </select></td>
            <td>&nbsp;</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td class="normalfnt">Year </td>
            <td colspan="2"><select name="cboYear" id="cboYear" style="width:250px"  class="validate[required] txtText" >
              <option value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option>
              <option value="<?php echo date('Y')-1; ?>"><?php echo date('Y')-1; ?></option>
              <option value="<?php echo date('Y')-2; ?>"><?php echo date('Y')-2; ?></option>
              <option value="<?php echo date('Y')-3; ?>"><?php echo date('Y')-3; ?></option>
              <option value="<?php echo date('Y')-4; ?>"><?php echo date('Y')-4; ?></option>
              <option value="<?php echo date('Y')-5; ?>"><?php echo date('Y')-5; ?></option>
              </select></td>
            <td>&nbsp;</td>
            <td></td>
            <td></td>
          </tr>
          <tr><td colspan="5">&nbsp;</td>
          </tr> 
              <tr>
                <td colspan="2" class="normalfntMid"><img src="images/Treport.jpg" style="display:inline" border="0"  alt="view report" id="butReport" name="butReport" class="mouseover" /></td>
                </tr>

         </table></td>
      </tr>       			</tbody>
	          </table></form></td>
	        </tr>
<!--END - Allocated items expiry--> 

<!--BEGIN - ITEMS AGING REPORT-->
 		    <tr>
		      <td><table align="left" width="600" class="main_header_table" id="tblItemsAgingHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>RM Aging Report.</u></td>
	            </tr>
		        </table></td>
	        </tr>
 	    <tr>
		      <td><form name="frmItemsAging" id="frmItemsAging" autocomplete="off">
              <table width="100%" align="left" border="0" id="tblItemsAgingBody" class="main_table" cellspacing="0"  style="display:none;"   >
		        <thead>
		          <tr>
		            <td colspan="4" style="text-align:left">RM Aging Report Selection List Criteria<img src="images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
<tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="18%" class="normalfnt">Company</td>
            <td colspan="2"><select name="cboCompany" id="cboCompany" class="txtText" style="width:250px" >
              <option value=""></option>
              <?php
					$sql = "SELECT
							mst_companies.intId,
							mst_companies.strName
							FROM mst_companies
							WHERE
							mst_companies.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td width="1%"></td>
            <td width="1%"></td>
            </tr> 
          <tr>
            <td width="18%" class="normalfnt">Location</td>
            <td colspan="2"><select name="cboLocation" id="cboLocation" class="txtText" style="width:250px" >
              <option value=""></option>
            </select></td>
            <td></td>
            <td></td>
            </tr> 
         
          <tr>
            <td class="normalfnt">Main Category</td>
            <td colspan="2" ><select name="cboMainCategory" style="width:250px" id="cboMainCategory">
              <option value=""></option>
              <?php
					$sql = "SELECT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory";
					//$sql .= " WHERE mst_maincategory.intId IN ('1','2') ";
					$sql	.= " ORDER BY mst_maincategory.strName ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select></td>
            <td>&nbsp;</td>
            <td >&nbsp;</td>
            <td width="1%"></td>
          </tr>
          <tr>
            <td class="normalfnt">Sub Category</td>
            <td colspan="2"><select name="cboSubCategory" id="cboSubCategory" style="width:250px"  class="validate[required] txtText" >
              <option value=""></option>
            </select></td>
            <td>&nbsp;</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td class="normalfnt">Item </td>
            <td colspan="2"><select name="cboItems" id="cboItems" style="width:250px"  class="validate[required] txtText" >
              <option value=""></option>
              </select></td>
            <td>&nbsp;</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td class="normalfnt">To Date </td>
            <td colspan="2"><input name="dtDateToAging" type="text" value="" class="txtbox" id="dtDateToAging" style="width:100px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" />            </td>
            <td>&nbsp;</td>
            <td></td>
            <td></td>
          </tr>
          <tr><td colspan="5">&nbsp;</td>
          </tr> 
              <tr>
                <td colspan="2" class="normalfntMid"><img src="images/Treport.jpg" style="display:inline" border="0"  alt="view report" id="butReport" name="butReport" class="mouseover" /></td>
                </tr>

         </table></td>
      </tr>       			</tbody>
	          </table></form></td>
	        </tr> 
<!--END - Allocated items STOCK BALANCE--> 
    
<!--BEGIN - monthly GP REPORT-->
		    <tr>
		      <td><table align="left" width="600" class="main_header_table" id="tblmonthlyGPHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Monthly Gate Pass Report.</u></td>
	            </tr>
		        </table></td>
	        </tr>
		    <tr>
		      <td><form name="frmmonthlyGP" id="frmmonthlyGP" autocomplete="off">
              <table width="100%" align="left" border="0" id="tblmonthlyGPBody" class="main_table" cellspacing="0"  style="display:none;"   >
		        <thead>
		          <tr>
		            <td colspan="4" style="text-align:left">Monthly Gate Pass Report Selection List Criteria<img src="images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
<tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="18%" class="normalfnt">Company</td>
            <td colspan="2"><select name="cboCompany" id="cboCompany" class="txtText" style="width:250px" >
              <option value=""></option>
              <?php
					$sql = "SELECT
							mst_companies.intId,
							mst_companies.strName
							FROM mst_companies
							WHERE
							mst_companies.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td width="1%"></td>
            <td width="1%"></td>
            </tr> 
          <tr>
            <td width="18%" class="normalfnt">Location</td>
            <td colspan="2"><select name="cboLocation" id="cboLocation" class="txtText" style="width:250px" >
              <option value=""></option>
            </select></td>
            <td></td>
            <td></td>
            </tr> 
         
          <tr>
            <td class="normalfnt">Main Category</td>
            <td colspan="2" ><select name="cboMainCategory" style="width:250px" id="cboMainCategory">
              <option value=""></option>
              <?php
					$sql = "SELECT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory";
					//$sql .= " WHERE mst_maincategory.intId IN ('1','2') ";
					$sql	.= " ORDER BY mst_maincategory.strName ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select></td>
            <td>&nbsp;</td>
            <td >&nbsp;</td>
            <td width="1%"></td>
          </tr>
          <tr>
            <td class="normalfnt">Sub Category</td>
            <td colspan="2"><select name="cboSubCategory" id="cboSubCategory" style="width:250px"  class="validate[required] txtText" >
              <option value=""></option>
            </select></td>
            <td>&nbsp;</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td class="normalfnt">Item </td>
            <td colspan="2"><select name="cboItems" id="cboItems" style="width:250px"  class="validate[required] txtText" >
              <option value=""></option>
              </select></td>
            <td>&nbsp;</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Date</td>
            <td colspan="5"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="6%" class="normalfnt">From</td>
                <td width="44%"><input name="dtGPDateFrom" type="text" class="txtbox" id="dtGPDateFrom" style="width:80px;"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                <td width="6%" class="normalfnt">To</td>
                <td width="44%"><input name="dtGPDateTo" type="text"  class="txtbox" id="dtGPDateTo" style="width:80px;"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              </tr>
            </table></td>
          </tr>
          <tr><td colspan="5">&nbsp;</td>
          </tr> 
              <tr>
                <td colspan="2" class="normalfntMid"><img src="images/Treport.jpg" style="display:inline" border="0"  alt="view report" id="butReport" name="butReport" class="mouseover" /></td>
                </tr>

         </table></td>
      </tr>       			</tbody>
	          </table></form></td>
	        </tr>

              <!--BEGIN - monthly GP TRANFERIN REPORT-->
              <tr>
                  <td><table align="left" width="600" class="main_header_table" id="tblmonthlyGPTranferinHead" cellpadding="2" cellspacing="0">
                          <tr>
                              <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Monthly Gate Pass Tranferin Report.</u></td>
                          </tr>
                      </table></td>
              </tr>
              <tr>
                  <td><form name="frmmonthlyGPTranferin" id="frmmonthlyGPTranferin" autocomplete="off">
                          <table width="100%" align="left" border="0" id="tblmonthlyGPTranferinBody" class="main_table" cellspacing="0"  style="display:none;"   >
                              <thead>
                              <tr>
                                  <td colspan="4" style="text-align:left">Monthly Gate Pass Tranferin Report Selection List Criteria<img src="images/close_small.png" align="right" class="mouseover"/></td>
                              </tr>
                              </thead>
                              <tbody>
                              <tr>
                                  <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                          <tr>
                                              <td width="18%" class="normalfnt">Company</td>
                                              <td colspan="2"><select name="cboCompany" id="cboCompany" class="txtText" style="width:250px" >
                                                      <option value=""></option>
                                                      <?php
                                                      $sql = "SELECT
							mst_companies.intId,
							mst_companies.strName
							FROM mst_companies
							WHERE
							mst_companies.intStatus =  '1'";
                                                      $result = $db->RunQuery($sql);
                                                      while($row=mysqli_fetch_array($result))
                                                      {
                                                          echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
                                                      }
                                                      ?>
                                                  </select></td>
                                              <td width="1%"></td>
                                              <td width="1%"></td>
                                          </tr>
                                          <tr>
                                              <td width="18%" class="normalfnt">Location</td>
                                              <td colspan="2"><select name="cboLocation" id="cboLocation" class="txtText" style="width:250px" >
                                                      <option value=""></option>
                                                  </select></td>
                                              <td></td>
                                              <td></td>
                                          </tr>

                                          <tr>
                                              <td class="normalfnt">Main Category</td>
                                              <td colspan="2" ><select name="cboMainCategory" style="width:250px" id="cboMainCategory">
                                                      <option value=""></option>
                                                      <?php
                                                      $sql = "SELECT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory";
                                                      //$sql .= " WHERE mst_maincategory.intId IN ('1','2') ";
                                                      $sql	.= " ORDER BY mst_maincategory.strName ASC";
                                                      $result = $db->RunQuery($sql);
                                                      while($row=mysqli_fetch_array($result))
                                                      {
                                                          echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
                                                      }
                                                      ?>
                                                  </select></td>
                                              <td>&nbsp;</td>
                                              <td >&nbsp;</td>
                                              <td width="1%"></td>
                                          </tr>
                                          <tr>
                                              <td class="normalfnt">Sub Category</td>
                                              <td colspan="2"><select name="cboSubCategory" id="cboSubCategory" style="width:250px"  class="validate[required] txtText" >
                                                      <option value=""></option>
                                                  </select></td>
                                              <td>&nbsp;</td>
                                              <td></td>
                                              <td></td>
                                          </tr>
                                          <tr>
                                              <td class="normalfnt">Item </td>
                                              <td colspan="2"><select name="cboItems" id="cboItems" style="width:250px"  class="validate[required] txtText" >
                                                      <option value=""></option>
                                                  </select></td>
                                              <td>&nbsp;</td>
                                              <td></td>
                                              <td></td>
                                          </tr>
                                          <tr>
                                              <td height="27" class="normalfnt">Date</td>
                                              <td colspan="5"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                      <tr>
                                                          <td width="6%" class="normalfnt">From</td>
                                                          <td width="44%"><input name="dtGPTDateFrom" type="text" class="txtbox" id="dtGPTDateFrom" style="width:80px;"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                                          <td width="6%" class="normalfnt">To</td>
                                                          <td width="44%"><input name="dtGPTDateTo" type="text"  class="txtbox" id="dtGPTDateTo" style="width:80px;"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                                      </tr>
                                                  </table></td>
                                          </tr>
                                          <tr><td colspan="5">&nbsp;</td>
                                          </tr>
                                          <tr>
                                              <td colspan="2" class="normalfntMid"><img src="images/Treport.jpg" style="display:inline" border="0"  alt="view report" id="butReport" name="butReport" class="mouseover" /></td>
                                          </tr>

                                      </table></td>
                              </tr>       			</tbody>
                          </table></form></td>
              </tr>
<!--END - Allocated items STOCK BALANCE--> 
  
<!--Location wise GRN--(2014-06-13)--->
		    <tr>
		      <td><table align="left" width="600" class="main_header_table" id="tblLocWiseGRNHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="images/report_go.png" class="mouseover" /><u>Location Wise GRN -(Excel).</u></td>
	            </tr>
		        </table></td>
	        </tr>
		    <tr>
		      <td><form name="frmLocWiseGRN" id="frmLocWiseGRN" autocomplete="off">
              <table width="700" align="left" border="0" id="tblLocWiseGRNBody" class="main_table" cellspacing="0" style="display:none;">
		        <thead>
		          <tr>
		            <td colspan="7" style="text-align:left">Location Wise GRN Selection Criteria -(Excel)<img src="images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
          <tr>
            <td width="15%" class="normalfnt">Company</td>
            <td colspan="3	"><select name="cboCompany" id="cboCompany" style="width:220px"  class="txtText validate[required]"  >
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_companies.intId,
							mst_companies.strName
							FROM mst_companies
							WHERE
							mst_companies.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td width="15%" class="normalfnt">Main Category</td>
            <td width="32%" class="normalfnt"><select name="cboMainCategory" style="width:120px" id="cboMainCategory">
		              <option value=""></option>
		              <?php
					$sql = "SELECT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
	                </select></td>
            <td width="1%" class="normalfnt"></td>
          </tr> 
          <tr>
            <td class="normalfnt">Location</td>
            <td colspan="3"><select name="cboLocation" id="cboLocation" style="width:220px"  class="txtText"  >
                  <option value=""></option>
            </select></td>
            <td class="normalfnt">Sub Category</td>
            <td class="normalfnt"><select name="cboSubCategory" style="width:120px" id="cboSubCategory">
		              <option value=""></option>
	                </select></td>
            <td class="normalfnt"></td>
          </tr> 
          <tr>
            <td class="normalfnt">Currency</td>
            <td colspan="3"><span class="normalfnt">
              <select id="cboCurrency" name="cboCurrency" style="width:120px" >
                <option value=""></option>
                <?php 
					$sql="SELECT
							mst_financecurrency.intId,
							mst_financecurrency.strCode
							FROM mst_financecurrency
							WHERE
							mst_financecurrency.intStatus =  '1'
							ORDER BY
							mst_financecurrency.strCode ASC";
					$result=$db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row["intId"]."\">".$row["strCode"]."</option>";
					}
					?>
              </select>
            </span></td>
            <td class="normalfnt">Item</td>
            <td class="normalfnt"><select id="cboItem" name="cboItem" style="width:210px">
		              <option value=""></option>
					  </select></td>
            <td class="normalfnt"></td>
          </tr> 
		          <tr>
		            <td class="normalfnt">Date From&nbsp;&nbsp;<input name="chkDate" type="checkbox" id="chkDate"  /></td>
		            <td><input name="dtFromDate11" type="text" value="<?php   //echo date("Y-m-d"); ?>" class="txtbox" id="dtFromDate11" style="width:80px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
		            <td><span class="normalfnt">To</span></td>
		            <td><input name="dtToDate11" type="text" value="<?php  //echo date("Y-m-d"); ?>" class="txtbox" id="dtToDate11" style="width:80px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"  /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt"></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">&nbsp;</td>
		            <td >&nbsp;</td>
		            <td >&nbsp;</td>
		            <td >&nbsp;</td>
            <td class="normalfnt"></td>
            <td class="normalfnt"></td>
            <td class="normalfnt"></td>
	              </tr>
		          <tr>
		            <td colspan="7" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
		              <tr>
		                <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport('locationWiseGRN','frmLocWiseGRN')"/><img  style="display:inline" border="0" src="images/Treport_exel.png" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport_excel_new('locationWiseGRN','frmLocWiseGRN')"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
	                  </tr>
	                </table></td>
	              </tr>
	            </tbody>
	          </table></form></td>
	        </tr>
<!-- END---------Location wise GRN-->
<!----Stock movement report endit by nuwan-----> <tr>
		      <td><table align="left" width="600" class="main_header_table" id="stcMoveRepHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Stock movement report -(Excel).</u></td>
	            </tr>
		        </table></td>
	        </tr>
		   		    <tr>
		      <td><form name="frmStoMoveReport" id="frmStoMoveReport" autocomplete="off">
              <table width="700" align="left" border="0" id="stcMoveRepBody" class="main_table" cellspacing="0" style="display:none;">
		        <thead>
		          <tr>
		            <td colspan="7" style="text-align:left">Stock movement report Criteria -(Excel)<img src="images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
          <tr>
            <td width="15%" class="normalfnt">Company</td>
            <td colspan="3	"><select name="cboCompany" id="cboCompany" style="width:220px"   class="validate[required]" >
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_companies.intId,
							mst_companies.strName
							FROM mst_companies
							WHERE
							mst_companies.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td width="15%" class="normalfnt">Main Category</td>
            <td width="32%" class="normalfnt"><select name="cboMainCategory" style="width:120px" id="cboMainCategory" class="validate[required]">
		              <option value=""></option>
		              <?php
						$sql = "SELECT
								mst_maincategory.intId,
								mst_maincategory.strName
								FROM mst_maincategory";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
						}
				     ?>
	                </select></td>
            <td width="1%" class="normalfnt"></td>
          </tr> 
          <tr>
            <td class="normalfnt">Location</td>
            <td colspan="3"> <select name="cboLocation" id="cboLocation" style="width:220px"  class="validate[required] txtText" >
                  <option value=""></option>
            </select></td>
            <td class="normalfnt">Sub Category</td>
            <td class="normalfnt"><select name="cboSubCategory" style="width:120px" id="cboSubCategory">
		              <option value=""></option>
	                </select></td>
            <td class="normalfnt"></td>
          </tr> 
          
		          <tr>
		            <td class="normalfnt">Date From&nbsp;&nbsp;<input name="chkDate" type="checkbox" id="chkDate" class="validate[required]" /></td>
		            <td><input name="dtFromStrDate11" type="text" value="<?php   //echo date("Y-m-d"); ?>" class="txtbox" id="dtFromStrDate11" style="width:80px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""   class="validate[dateRange[grp1]] txtbox"style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
		            <td><span class="normalfnt">To</span></td>
		            <td><input name="dtToStrDate11" type="text" value="<?php  //echo date("Y-m-d"); ?>" class="txtbox " id="dtToStrDate11" style="width:80px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"  /><input type="reset" value=""  class="validate[dateRange[grp1]] txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt"></td>
	              </tr>
		          <tr>
		            <td class="normalfnt">&nbsp;</td>
		            <td >&nbsp;</td>
		            <td >&nbsp;</td>
		            <td >&nbsp;</td>
            <td class="normalfnt"></td>
            <td class="normalfnt"></td>
            <td class="normalfnt"></td>
	              </tr>
		          <tr>
		            <td colspan="7" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
		              <tr>
		                <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28" onclick="reSet()" /><img  style="display:none" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport('stoMoveReport','frmStoMoveReport')"/><img  style="display:inline" border="0" src="images/Treport_exel.png" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport_excel_new('stoMoveReport','frmStoMoveReport')"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                        <script>function reSet() {document.getElementById("frmStoMoveReport").reset();}</script>
	                  </tr>
	                </table></td>
	              </tr>
	            </tbody>
	          </table></form></td>
	        </tr>
            <!---end nuwan----->
<!--END - PO Summery--> 

   
          </table>
    </div>
  </div>
<!--</form>
-->
<?php
function loadViewMode($programCode,$intUser){
	global $db;
	
	$editMode	=0;
	$sqlp 		= "SELECT
					menupermision.intView  
					FROM menupermision 
					Inner Join menus ON menupermision.intMenuId = menus.intId
					WHERE
					menus.strCode =  '$programCode' AND
					menupermision.intUserId =  '$intUser'";	
				
	$resultp 	= $db->RunQuery($sqlp);
	$rowp		= mysqli_fetch_array($resultp);
		 
	return $rowp['intView'];
}
?>
<?php
//ini_set('display_errors',1);
 $companyId 			= $_SESSION['CompanyID'];
$locationId 		= $_SESSION['CompanyID'];//this locationId use in report header(reportHeader.php)-------
$intUser  			= $_SESSION["userId"];
$mainPath 			= $_SESSION['mainPath'];
$thisFilePath 		=  $_SERVER['PHP_SELF'];
 require_once 		"class/cls_commonFunctions_get.php";
require_once 		"class/warehouse/cls_warehouse_get.php";
  
$obj_common			= new cls_commonFunctions_get($db);
$obj_wh_get			= new cls_warehouse_get($db);

$company 			= $_REQUEST['company'];
$location 			= $_REQUEST['location'];
$mainCategory 		= $_REQUEST['mainCategory'];
$subCategory		= $_REQUEST['subCategory'];
$itemId 			= $_REQUEST['itemId'];
$originalMainCat    = $mainCategory;
$originalSubCat     = $subCategory;
$originalItem       = $itemId;
$toDate	 			= $_REQUEST['toDate'];
$currunt_month		= date('m');
//$currunt_month		= 8;
if($year != date('Y')){
	$currunt_month = 12;
}

$result				= $obj_common->loadCompany_result($company,'RunQuery');
$row 				= mysqli_fetch_array($result);
$company_desc 		= $row['strName'];
$base_currency_id	= $row['intBaseCurrencyId'];
$base_currency		= $row['currency'];

$result				= $obj_common->loadLocationName($location,'RunQuery');
$row 				= mysqli_fetch_array($result);
$location_desc		= $row['strName'];

$result				= $obj_common->loadMainCatName($mainCategory,'RunQuery');
$row 				= mysqli_fetch_array($result);
$main_cat_desc		= $row['strName'];

$result				= $obj_common->loadSubCatName($subCategory,'RunQuery');
$row 				= mysqli_fetch_array($result);
$sub_cat_desc		= $row['strName'];

$result				= $obj_common->get_item_name($itemId,'RunQuery');
$row 				= mysqli_fetch_array($result);
$item_desc			= $row['strName'];

 //----------------------------------
?>
 <head>
 <title>RAW MATERIAL AGING Report</title>
 <style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:264px;
	top:247px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<form id="frmRM_Aging_Report" name="frmRM_Aging_Report" method="post" >
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>RAW MATERIAL AGING REPORT</strong></div>
<table width="1200" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td width="1200">
  <table width="100%">
   <tr>
     <td width="4%"></td>
     <td width="10%" class="normalfnt" align="right"><strong style="text-align:right">Company</strong></td>
     <td width="1%" align="center" valign="middle"><strong>:</strong></td>
     <td width="29%" align="left"><span class="normalfnt" style="text-align:left"><?php echo $company_desc  ?></span></td>
     <td width="9%" class="normalfnt">&nbsp;</td>
     <td width="1%" align="center" valign="middle">&nbsp;</td>
     <td width="17%">&nbsp;</td>
     <td width="8%" class="normalfnt"></td>
     <td width="1%"></td>
     <td width="20%"></td>
   </tr>
   <tr>
     <td></td>
     <td class="normalfnt" align="right"><strong>To Date</strong></td>
     <td align="center" valign="middle"><strong>:</strong></td>
     <td><span class="normalfnt" style="text-align:left"><?php echo $toDate  ?></span></td>
     <td class="normalfnt"><strong>Currency</strong></td>
     <td class="normalfnt"><strong>:</strong></td>
     <td><span class="normalfnt" style="text-align:left"><?php echo $base_currency  ?></span></td>
     <td class="normalfnt">&nbsp;</td>
     <td class="normalfnt">&nbsp;</td>
     <td>&nbsp;</td>
   </tr>
  <tr>
    <td colspan="10">  <table width="100%">
   <tr>
     <td width="4%">&nbsp;</td>
    <td width="5%" >&nbsp;</td>
    <td width="11%" class="normalfnt">&nbsp;</td>
    <td width="21%">&nbsp;</td>
    <td width="10%">&nbsp;</td>
    <td width="11%">&nbsp;</td>
    <td width="11%">&nbsp;</td>
    <td width="11%">&nbsp;</td>
    <td width="16%">&nbsp;</td>
  </tr>
</table>
</td>
  </tr>
   </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr>
              <th width="5%" nowrap="nowrap" >Location</th>
              <th width="9%" nowrap="nowrap" >Main Category</th>
              <th width="8%" nowrap="nowrap" >Sub Category</th>
              <th width="6%" nowrap="nowrap" >Item Code</th>
              <th width="10%" nowrap="nowrap" >Item</th>
              <th width="5%" nowrap="nowrap" >UOM</th>
              <th width="5%" >Unit Price</th>
              <th width="5%" >To Date Stock Balance</th>
              <th width="7%" >To Date Stock Value</th>
             <?php
			 $cols		=0;
			 for ($i=0; $i <=360; $i=$i+30){
			 	$cols=$cols+2;
			 }
			$cols=$cols+2;
			
			 for ($i=0; $i <=330; $i=$i+30){
				 $width			=70/$cols;   
			 ?>
              <th width="7%" >Stock <?php echo $i.'-'.($i+30) ; ?> </th>
              <th width="9%" >Value  <?php echo $i.'-'.($i+30); ?> </th>
              <?php
			 }
			  ?>
              <th width="11%" >Stock Over <?php echo $i; ?> </th>
              <th width="13%" >Value Over <?php echo $i; ?> </th>
              </tr>
              <?php
 			$result_loc			= $obj_common->loadLocation_with_selected_val($company,$location,'RunQuery');
			while($row_loc=mysqli_fetch_array($result_loc))
			{
				$location 		= $row_loc['intId'];
				$location_name 	= $row_loc['strName'];
			  ?>
              
            <?php 
 			$sql			= $obj_wh_get->get_expiry_items_sql($originalMainCat,$originalSubCat,$originalItem);

 			$result 		= $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				//$mainCategory	= $row['MAIN_CAT'];
				//$subCategory	= $row['SUB_CAT'];
				$itemId			= $row['ITEM_ID'];
				$curr_stock_bal	= $obj_wh_get->get_bulk_stock_balance_to_given_date($location,$itemId,$toDate,'RunQuery');
				$curr_stock_val	= $obj_wh_get->get_bulk_stock_val_to_given_date($location,$itemId,$toDate,$base_currency_id,'RunQuery');
			 ?>
             
            <tr height="17" class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" nowrap="nowrap" ><?php echo $location_name?></td>
              <td class="normalfnt" nowrap="nowrap" ><?php echo $row['MAIN_CAT']?></td>
              <td class="normalfnt" nowrap="nowrap" ><?php echo $row['SUB_CAT']?></td>
              <?php
              if($row['SUP_ITEM_CODE']!=null)
              {
              ?>
              <td class="normalfnt" nowrap="nowrap" ><?php echo $row['SUP_ITEM_CODE']?></td>
              <?php
              }else{
              ?>    
              
              <td class="normalfnt" nowrap="nowrap" ><?php echo $row['ITEM_CODE']?></td>
              <?php
              }
              ?>
              <td class="normalfnt" nowrap="nowrap" ><?php echo $row['ITEM']?></td>
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $row['UOM']?></td>
              <td align="right" valign="middle"><?php echo number_format((round($curr_stock_val,2)/round($curr_stock_bal,2)),2); ?></td>
              <td align="right" valign="middle" bgcolor="#D5FCBA" ><?php echo number_format($curr_stock_bal,2);$sum_curr_stock_bal +=$curr_stock_bal;  ?></td>
              <td align="right" bgcolor="#CCFBAC" style="color:#009"><?php echo number_format($curr_stock_val,2);$sum_curr_stock_val +=$curr_stock_val; ?></td>
              <?php
			 for ($i=0; $i <=330; $i=$i+30){
					$range_from	= $i;
					$range_to	= $i+30;
					//$toDate	= $i+30;
					//echo $range_from."-".$range_to."/";
					$sql_stk	= $obj_wh_get->get_aging_data_sql($company,$location,$itemId,$toDate,$range_from,$range_to,$base_currency_id);
					if($intUser==2){
					//echo $sql_stk;
					}
					$result_stk	= $db->RunQuery($sql_stk);
					$row_stk	= mysqli_fetch_array($result_stk);
					//print_r($row_stk);
					$stockBal	= $row_stk['stock'];
					$stockVal	= $row_stk['val'];
					if($stockBal == '')
						$stockBal	= 0;
					?>
              <td align="right"><?php echo number_format($stockBal,2);$sum_stockBal[$i]+=$stockBal;  ?> </td>
              <td align="right" style="color:#009"><?php echo number_format($stockVal,2);$sum_stockVal[$i]+= $stockVal; ?> </td>
              <?php
                }
			  ?>
			  <?php
					$range_from	= $i;
					$range_to	= '';
					$sql_stk	= $obj_wh_get->get_aging_data_sql($company,$location,$itemId,$toDate,$range_from,$range_to,$base_currency_id);
					$result_stk	= $db->RunQuery($sql_stk);
					$row_stk	= mysqli_fetch_array($result_stk);
					//print_r($row_stk);
					$stockBal	= $row_stk['stock'];
					$stockVal	= $row_stk['val'];
					if($stockBal == '')
						$stockBal	= 0;
              ?>
              <td align="right"><?php echo number_format($stockBal,2);$sum_ageOverBal+=$stockBal;  ?> </td>
              <td align="right" style="color:#009"><?php echo number_format($stockVal,2);$sum_ageOverVal+=$stockVal;  ?> </td>
            </tr>
    
            <?php 
			}
	  ?>
      <?php
			}
	  
	  ?>
      		<tr>
            	<th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th style="text-align:right"><?php echo number_format($sum_curr_stock_bal,2); ?></th>
                <th  style="text-align:right"><?php echo number_format($sum_curr_stock_val,2); ?></th>
                <?php
               		for ($i=0; $i <=330; $i=$i+30){
			 	?>
              <th  style="text-align:right"><?php echo number_format($sum_stockBal[$i],2); ?></th>
              <th  style="text-align:right"><?php echo number_format($sum_stockVal[$i],2); ?></th>
              	<?php
			 		}
				?>
               <th  style="text-align:right"><?php echo number_format($sum_ageOverBal,2); ?></th>
               <th  style="text-align:right"><?php echo number_format($sum_ageOverVal,2); ?></th>
            </tr>
            </table>
        </td>
        </tr>
      
      </table>
    </td>
</tr>

<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
 
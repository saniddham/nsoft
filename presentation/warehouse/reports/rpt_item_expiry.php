<?php
$companyId 			= $_SESSION['CompanyID'];
$locationId 		= $_SESSION['CompanyID'];//this locationId use in report header(reportHeader.php)-------
$intUser  			= $_SESSION["userId"];
$mainPath 			= $_SESSION['mainPath'];
$thisFilePath 		=  $_SERVER['PHP_SELF'];
 require_once 		"class/cls_commonFunctions_get.php";
require_once 		"class/warehouse/cls_warehouse_get.php";
  
$obj_common			= new cls_commonFunctions_get($db);
$obj_wh_get			= new cls_warehouse_get($db);

$company 			= $_REQUEST['company'];
$location 			= $_REQUEST['location'];
$mainCategory 		= $_REQUEST['mainCategory'];
$subCategory		= $_REQUEST['subCategory'];
$itemId 			= $_REQUEST['itemId'];
$year	 			= $_REQUEST['year'];
$currunt_month		= date('m');
//$currunt_month		= 8;
if($year != date('Y')){
	$currunt_month = 12;
}

$result				= $obj_common->loadCompany_result($company,'RunQuery');
$row 				= mysqli_fetch_array($result);
$company_desc 		= $row['strName'];

$result				= $obj_common->loadLocationName($location,'RunQuery');
$row 				= mysqli_fetch_array($result);
$location_desc		= $row['strName'];

$result				= $obj_common->loadMainCatName($mainCategory,'RunQuery');
$row 				= mysqli_fetch_array($result);
$main_cat_desc		= $row['strName'];

$result				= $obj_common->loadSubCatName($subCategory,'RunQuery');
$row 				= mysqli_fetch_array($result);
$sub_cat_desc		= $row['strName'];

$result				= $obj_common->get_item_name($itemId,'RunQuery');
$row 				= mysqli_fetch_array($result);
$item_desc			= $row['strName'];

 //----------------------------------
?>
 <head>
 <title>RAW MATERIAL Expiry Report</title>
 <style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:264px;
	top:247px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<form id="frmRM_expiry_Report" name="frmRM_expiry_Report" method="post" action="rpt_item_aging.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>RAW MATERIAL EXPITY REPORT</strong></div>
<table width="968" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td width="905">
  <table width="100%">
   <tr>
    <td width="5%"></td>
    <td width="12%" class="normalfnt" align="right"><strong style="text-align:right">Company</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="23%" align="left"><span class="normalfnt" style="text-align:left"><?php echo $company_desc  ?></span></td>
    <td width="13%" class="normalfnt"><strong>Location</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="32%"><span class="normalfnt"><?php echo $location_desc  ?></span></td>
    <td width="6%" class="normalfnt"></td>
    <td width="7%"></td>
  </tr>
   <tr>
    <td></td>
    <td class="normalfnt" align="right"><strong>Main Category</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt" style="text-align:left"><?php echo $main_cat_desc  ?></span></td>
    <td class="normalfnt" align="right"><strong style="text-align:right">Sub Category</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt" style="text-align:left"><?php echo $sub_cat_desc  ?></span></td>
    <td class="normalfnt"></td>
    <td></td>
  </tr>
   <tr>
    <td></td>
    <td class="normalfnt"><strong>Item</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php $item_desc;  ?></span></td>
    <td class="normalfnt" align="right"><strong>Year</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt" style="text-align:left"><?php echo $year  ?></span></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="9">  <table width="100%">
   <tr>
     <td width="5%">&nbsp;</td>
    <td width="4%" bgcolor="#FFD9D9">&nbsp;</td>
    <td width="11%" class="normalfnt">Expired</td>
    <td width="23%">&nbsp;</td>
    <td width="8%">&nbsp;</td>
    <td width="11%">&nbsp;</td>
    <td width="11%">&nbsp;</td>
    <td width="11%">&nbsp;</td>
    <td width="16%">&nbsp;</td>
  </tr>
</table>
</td>
  </tr>
   </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr>
              <th width="10%" nowrap="nowrap" >Main Category</th>
              <th width="10%" nowrap="nowrap" >Sub Category</th>
              <th width="20%" nowrap="nowrap" >Item</th>
              <th width="5%" nowrap="nowrap" >UOM</th>
              <th width="5%" >Currunt Stock Balance</th>
             <?php
			 for ($i=1; $i <=$currunt_month; $i++){
				 $month			= $i;
				 $month_name	= $obj_common->getMonthName($month);
				 $width			=60/$currunt_month;   
			 ?>
              <th width="<?php echo $currunt_month.'%';  ?>" ><?php echo $year.' '.$month_name    ?> </th>
              <?php
			 }
			  ?>
              </tr>
            <?php 
 			$sql			= $obj_wh_get->get_expiry_items_sql($mainCategory,$subCategory,$itemId);
 			$result 		= $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				$mainCategory	= $row['MAIN_CAT'];
				$subCategory	= $row['SUB_CAT'];
				$itemId			= $row['ITEM_ID'];
				$curr_stock_bal	= $obj_wh_get->get_bulk_stock_balance($location,$itemId,'RunQuery');
			 ?>
             
            <tr height="17" class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $row['MAIN_CAT']?></td>
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $row['SUB_CAT']?></td>
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $row['ITEM']?></td>
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $row['UOM']?></td>
              <td rowspan="2" align="right" valign="middle"><?php echo number_format($curr_stock_bal,2); ?></td>
				<?php
                for ($i=1; $i <=$currunt_month; $i++){
					$month		= $i;
					$sql_stk	= $obj_wh_get->get_expiry_data_sql($company,$location,$itemId,$year,$month);
					$result_stk	= $db->RunQuery($sql_stk);
					$row_stk	= mysqli_fetch_array($result_stk);
					//print_r($row_stk);
					$diff		= $row_stk['DIFF'];
					$stockBal	= $row_stk['none_expired'];
					if($stockBal == '' || (date('m') <$month  && date('Y') == $year))
						$stockBal	= 0;
					?>
					<td align="right"><?php echo number_format($stockBal,2);  ?> </td>
                <?php
                }
			  ?>
              </tr>
    
            <tr height="17" class="normalfnt"  bgcolor="#FFD9D9">
              <td class="normalfntMid" nowrap="nowrap" ><?php //echo $row['MAIN_CAT']?></td>
              <td class="normalfntMid" nowrap="nowrap" ><?php //echo $row['SUB_CAT']?></td>
              <td class="normalfntMid" nowrap="nowrap" ><?php //echo $row['ITEM']?></td>
              <td class="normalfntMid" nowrap="nowrap" ><?php //echo $row['UOM']?></td>
              <?php
                for ($i=1; $i <=$currunt_month; $i++){
					$month		= $i;
					$sql_stk	= $obj_wh_get->get_expiry_data_sql($company,$location,$itemId,$year,$month);
					$result_stk	= $db->RunQuery($sql_stk);
					$row_stk	= mysqli_fetch_array($result_stk);
					$diff		= $row_stk['DIFF'];
					$stockBal	= $row_stk['expired'];
					if($stockBal == '' || (date('m') <$month  && date('Y') == $year))
						$stockBal	= 0;
					?>
					<td align="right" ><?php  echo number_format($stockBal,2)   ?> </td>
                <?php
                }
			  ?>
              </tr>
              
            <?php 
			}
	  ?>
            </table>
        </td>
        </tr>
      
      </table>
    </td>
</tr>

<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
 
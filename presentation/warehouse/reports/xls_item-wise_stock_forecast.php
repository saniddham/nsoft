<?php
//suvini

//include_once "../../../dataAccess/Connector.php";

//$result2 = $db->RunQuery($report_sql);
 				$maincategory	   = $_REQUEST['maincategory'];
  				$company           = $_REQUEST['company'];
				$maincategory	   = $_REQUEST['maincategory'];
				$subcategory       = $_REQUEST['subcategory'];
				$item  			   = $_REQUEST['item'];
				$BalToPurchase     = $_REQUEST['BalToPurchase'];
				$mrn  			   = $_REQUEST['mrn'];
				$curYear 		   = date('Y');
				$companyId         = $_SESSION["headCompanyId"];
                $locationId        = $_SESSION["CompanyID"];
				
				$head_office_loc   = get_ho_loc1($companyId);
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=report.xls" );
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");

?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<caption><strong><font size="3">ITEM WISE STOCK ORDER FORECAST REPORT</font></strong></caption>


<tr>
<th width="8%">Main Category</th>
<th width="8%">Sub Category</th>
<th width="4%"> Code</th>
<th width="10%">Item </th>
<th width="10%">UOM </th>
<th width="3%" >PRI </th>
<th width="3%" >PRN </th>
<th width="3%">ORDER BASED PO</th>
<th width="3%">MRN </th>
<th width="3%">ISSUED </th>
<th width="3%">RETURNED</th>
<th width="3%">GP OUT (FROM HO)</th>
<th width="3%">GP IN (TO HO)</th> 
<th width="3%">Available stock</th> 
<th width="3%">Bal To MRN</th>
<th width="3%">Bal To Issue</th>
<th width="3%">Rolling stock</th>

<?php



if($BalToPurchase==1)
{
?>
<th width="3%">Bal to purchase</th>
<?php
}
?>
<th width="3%">Re-order Policy</th>
                                                
</tr>
 <?php
												 


while ($row = mysqli_fetch_array($result))
{
//echo '(('.$row['REQUIRED'].'+'.$row['PRI_OTHER_ORDERS'].')-('.$row['TOT_HO_ISSUED'].'+MAX('.$row['TOT_NON_HO_ISSUED'].',('.$row['GP_OUT_FROM_HO'].'+'.$row['GP_OTHER_ORDERS'].'))))';
			//echo '<br>';


$MRN_Qty 	=  round($row['MRN_QTY_TODATE'],4);
$bal_to_MRN = (round(($row['REQUIRED_TODATE'] - $MRN_Qty),4)); 


if($row['type'] == 2)
{
$bal_to_issue_for_all = round((($MRN_Qty)-($row['ISSUE_QTY_ALL_HO']+MAX($row['ISSUE_QTY_ALL_NON_HO'],($row['GP_FROM_HO_ALL'])))),4);
}
else
{
$bal_to_issue_for_all = round((($row['REQUIRED_TODATE'])-($row['ISSUE_QTY_ALL_HO']+MAX($row['ISSUE_QTY_ALL_NON_HO'],($row['GP_FROM_HO_ALL'])))),4);
}

if($bal_to_issue_for_all < 0)
$bal_to_issue_for_all = 0;

$bal_to_grn	= ((round((($row['REQUIRED_TODATE'])-($row['ISSUE_QTY_ALL_HO']+MAX($row['ISSUE_QTY_ALL_NON_HO'],($row['GP_FROM_HO_ALL'])))),4))-(round(getHOStock($row['ITEM'],$head_office_loc),4))) ;
if($bal_to_grn < 0)
$bal_to_grn = 0;
$bal_to_purchase = $bal_to_grn-get_approved_non_grn_pos($row['ITEM'],2);
if($bal_to_purchase < 0)
$bal_to_purchase = 0;

if($row['type'] == 1)
$col	='#FFFFFF';
else 
$col	='#FF99CC';

$stockbal = get_stock_balance($row['ITEM'],$head_office_loc);
$rolling  = getRollingStock($row['ITEM'],$curYear,$locationId);
?>
<tr bgcolor="<?php echo $col; ?>">
					
<td class="normalfnt" style="text-align: center">&nbsp;<?php echo $row['mainCatName']; ?>&nbsp;</td>
<td class="normalfnt" style="text-align: center">&nbsp;<?php echo $row['subCatName']; ?>&nbsp;</td>

<td class="normalfnt" style="text-align: center">&nbsp;<?php if($row['supItemCode']!=null){ echo $row['supItemCode'];} else {echo $row['strCode']; } ?>&nbsp;</td>
<td class="normalfnt" style="text-align: center">&nbsp;<?php echo $row['itemName']; ?>&nbsp;</td>
<td class="normalfnt" style="text-align: center">&nbsp;<?php echo $row['UOM']; ?>&nbsp;</td>
<td class="normalfnt" style="text-align: right">&nbsp;<?php  echo round($row['REQUIRED_TODATE'],4); ?>&nbsp;
</td>
<td class="normalfnt" style="text-align: right">&nbsp;<?php echo round($row['PRN_QTY_TODATE'],4); ?>&nbsp;
</td>
<td class="normalfnt" style="text-align: right">&nbsp;<?php echo round($row['PO_QTY_TODATE'],4); ?>&nbsp;</td>
<td class="normalfnt" style="text-align: right">&nbsp;<?php echo $MRN_Qty; ?>&nbsp;</td>
<td class="normalfnt" style="text-align: right">&nbsp;<?php echo round($row['ISSUE_QTY_TODATE'],4); ?>&nbsp;
</td>
<td class="normalfnt" style="text-align: right">&nbsp;<?php echo round($row['RET_TO_STORE_QTY_TODATE'],4); ?>&nbsp;</td>
<td class="normalfnt" style="text-align: right">&nbsp;<?php echo round($row['GP_FROM_HO_TODATE'],4); ?>&nbsp;</td>
<td class="normalfnt" style="text-align: right"><?php echo round($row['GP_IN_QTY_TO_HO_TODATE'],4); ?></td>
<td class="normalfnt" style="text-align: right" bgcolor="<?php if ($stockbal<$rolling) echo '#bf4080'; ?>">&nbsp; 
<?php echo round(get_stock_balance($row['ITEM'],$head_office_loc),4);


?>
&nbsp;</td>
<td class="normalfnt" style="text-align: right">&nbsp;<?php if($bal_to_MRN>0)
echo $bal_to_MRN;
else
echo 0; 
?>&nbsp;</td>
<td class="normalfnt" style="text-align: right">&nbsp;<?php echo $bal_to_issue_for_all ?>&nbsp;</td>
<td class="normalfnt" style="text-align: right">&nbsp;<?php echo round(getRollingStock($row['ITEM'],$curYear,$locationId),4); ?>&nbsp;</td>

<?php
if($BalToPurchase == 1)
{ 
?>
<td class="normalfnt" style="text-align: right">&nbsp;<?php echo $bal_to_purchase ?>&nbsp;</td>
<?php
}
?>
<td class="normalfnt" style="text-align: right"> 
<?php

 if($row['re_order_policy'] == 1)
 {
	 echo "Fixed Reorder Qty";
 } 
 else if($row['re_order_policy'] == 2)
 {
	 echo "Maximum Qty";
 }
 else if($row['re_order_policy'] == 3)
 {
	 echo "Order";
 }
  else if($row['re_order_policy'] == 4)
 {
	 echo "Lot for Lot";
 }
 
 
 
 ?>
</td>
</tr>

<?php
 }
 
 if($mrn==1)//include prnding MRn to thr report. - suvini
{
?>

<tr></tr>
<?php
	
$sql6					= get_pending_mrn_details($companyId,$head_office_loc,$locationId,$company,$maincategory,$subcategory,$item);
$result6 				= $db->RunQuery($sql6);
while ($row 			= mysqli_fetch_array($result6))
{
//$MRN_Qty 				= round($row['mrn_qty'],4);
$bal_to_MRN 			= round(($row['PRI_QTY'] - $row['MRN_QTY']),4); 

$bal_to_issue_for_all 	= (($row['MRN_QTY'])-($row['ISSUE_QTY']));

$stockbal 				= get_stock_balance($row['intItemId'],$head_office_loc);
$rolling  				= getRollingStock($row['intItemId'],$curYear,$locationId);
//if($bal_to_issue_for_all < 0)
//{
//$bal_to_issue_for_all = 0;
//
/*$bal_to_grn	= ((round((($row['REQUIRED_TODATE']+$row['PRI_OTHER'])-($row['ISSUE_QTY_ALL_HO']+MAX($row['ISSUE_QTY_ALL_NON_HO'],($row['GP_FROM_HO_ALL'])))),4))-(round(getHOStock($row['ITEM'],$head_office_loc),4))) ;
if($bal_to_grn < 0)
$bal_to_grn = 0;
$bal_to_purchase = $bal_to_grn-get_approved_non_grn_pos($row['ITEM'],2);
/*/
///$bal_to_purchase = ($row['PRI_QTY']- $row['PO_QTY']);
//if($bal_to_purchase < 0)
//{
//$bal_to_purchase = 0;
//}
/*if($row['type'] == 1)
$col	='#FFFFFF';
else 
$col	='#FF99CC';	*/
?>

<tr bgcolor="#71ede4">
					
<td class="normalfnt" style="text-align: center">&nbsp;<?php echo $row['mainCat']; ?>&nbsp;</td>
<td class="normalfnt" style="text-align: center">&nbsp;<?php echo $row['subCat']; ?>&nbsp;</td>
<td class="normalfnt" style="text-align: center">&nbsp;<?php echo $row['strCode']; ?>&nbsp;</td>
<td class="normalfnt" style="text-align: center">&nbsp;<?php echo $row['item']; ?>&nbsp;</td>
<td class="normalfnt" style="text-align: center">&nbsp;<?php echo $row['unit']; ?>&nbsp;</td>
<td class="normalfnt" style="text-align: right">&nbsp;<?php  echo round($row['PRI_QTY'],4); ?>&nbsp;
</td>
<td class="normalfnt" style="text-align: right">&nbsp;<?php echo round($row['PRN_QTY'],4); ?>&nbsp;
</td>
<td class="normalfnt" style="text-align: right">&nbsp;<?php echo round($row['PO_QTY'],4); ?>&nbsp;</td>
<td class="normalfnt" style="text-align: right">&nbsp;<?php echo round($row['MRN_QTY'],4); ?>&nbsp;</td>
<td class="normalfnt" style="text-align: right">&nbsp;<?php echo round($row['ISSUE_QTY'],4); ?>&nbsp;
</td>
<td class="normalfnt" style="text-align: right">&nbsp;<?php echo round($row['RET_TO_STORES'],4); ?>&nbsp;</td>
<td class="normalfnt" style="text-align: right">&nbsp;<?php echo round($row['GP_OUT'],4); ?>&nbsp;</td>
<td class="normalfnt" style="text-align: right"><?php echo round($row['GP_IN'],4); ?></td>
<td class="normalfnt" style="text-align: right" bgcolor="<?php if ($stockbal<$rolling) echo '#bf4080'; ?>">&nbsp; 
<?php echo round(get_stock_balance($row['ITEM'],$head_office_loc),4);


?>
&nbsp;</td>
<td class="normalfnt" style="text-align: right">&nbsp;<?php if ($bal_to_MRN>0)echo $bal_to_MRN; 
else 
echo 0; ?>&nbsp;</td>
<td class="normalfnt" style="text-align: right">&nbsp;<?php echo $bal_to_issue_for_all ?>&nbsp;</td>
<td class="normalfnt" style="text-align: right">&nbsp;<?php echo round(getRollingStock($row['intItemId'],$curYear,$locationId),4); ?>&nbsp;</td>
<?php
if($BalToPurchase == 1)
{ 
?>
<td class="normalfnt" style="text-align: right">&nbsp;<?php echo round(($row['PRI_QTY']-$row['PO_QTY']),4); ?>&nbsp;</td>
<?php

}
?>
<td class="normalfnt" style="text-align: right">
<?php

 if($row['re_order_policy'] == 1)
 {
	 echo "Fixed Reorder Qty";
 } 
 else if($row['re_order_policy'] == 2)
 {
	 echo "Maximum Qty";
 }
 else if($row['re_order_policy'] == 3)
 {
	 echo "Order";
 }
  else if($row['re_order_policy'] == 4)
 {
	 echo "Lot for Lot";
 }
 
 
 
 ?>
</td>
</tr>
<?php
	

}
}


function get_pending_mrn_details($companyId,$head_office_loc,$locationId,$company,$maincategory,$subcategory,$item)
{
$sql = "
SELECT
TB_ALL.intCompanyId,
TB_ALL.intItemId,
TB_ALL.MRN_QTY,
TB_ALL.item,
TB_ALL.strCode,
TB_ALL.re_order_policy,
TB_ALL.mainCat,
TB_ALL.mainCat_id,
TB_ALL.subCat,
TB_ALL.subCat_id,
TB_ALL.unit,
TB_ALL.GP_IN,
TB_ALL.GP_OUT,
TB_ALL.ISSUE_QTY,
TB_ALL.PO_QTY,
TB_ALL.PRI_QTY,
TB_ALL.PRN_QTY,
TB_ALL.RET_TO_STORES
FROM (SELECT
TB2.intCompanyId,
TB2.intItemId,
TB2.item,
TB2.strCode,
TB2.re_order_policy,
TB2.mainCat,
TB2.mainCat_id,
TB2.subCat,
TB2.subCat_id,
TB2.unit,
(TB2.MRN_QTY) AS MRN_QTY ,
(TB2.ISSUE_QTY) AS ISSUE_QTY,
(TB2.PRI_QTY) AS PRI_QTY,
(TB2.RET_TO_STORES) AS RET_TO_STORES,
(TB2.GP_OUT) AS GP_OUT,
(TB2.GP_IN) AS GP_IN,

(TB2.PRN_QTY) AS PRN_QTY,
(TB2.PO_QTY) AS PO_QTY
FROM(select 
TB1.intOrderNo,
TB1.intOrderYear,
TB1.intItemId,
TB1.intCompanyId,
TB1.item,
TB1.strCode,
TB1.re_order_policy,
TB1.mainCat,
TB1.mainCat_id,
TB1.subCat,
TB1.subCat_id,
TB1.unit,
SUM(TB1.mrn_qty) AS MRN_QTY,
SUM(TB1.issue_qty)as ISSUE_QTY,
SUM(TB1.PRI_QTY) as PRI_QTY,
SUM(TB1.ret_to_stores) AS RET_TO_STORES,
SUM(TB1.GP_Out) AS GP_OUT,
SUM(TB1.GP_IN_QTY) AS GP_IN,
( SELECT SUM(trn_po_prn_details.PRN_QTY) as prn_qty 
FROM trn_po_prn_details 
WHERE trn_po_prn_details.ORDER_NO = TB1.intOrderNo AND 
trn_po_prn_details.ORDER_YEAR = TB1.intOrderYear AND 
trn_po_prn_details.ITEM = TB1.intItemId) AS PRN_QTY,

(SELECT SUM(trn_po_prn_details.PURCHASED_QTY) as po_qty 
FROM trn_po_prn_details WHERE 
trn_po_prn_details.ORDER_NO = TB1.intOrderNo AND 
trn_po_prn_details.ORDER_YEAR = TB1.intOrderYear AND 
trn_po_prn_details.ITEM = TB1.intItemId ) AS PO_QTY

FROM(

select  

ware_mrndetails.intOrderNo,  

ware_mrndetails.intOrderYear,
ware_mrndetails.intItemId,
mst_locations.intCompanyId,
mst_maincategory.intId AS mainCat_id,
mst_maincategory.strName AS mainCat,
mst_subcategory.intId AS subCat_id,
mst_subcategory.strName As subCat,
mst_item.strName AS item,
mst_item.strCode,
mst_item.intReorderPolicy as re_order_policy,
mst_units.strName AS unit,
	
sum(ware_mrndetails.dblQty) as mrn_qty, 
sum(ware_mrndetails.dblIssudQty) as issue_qty, 

(select SUM(trn_po_prn_details_sales_order.REQUIRED) 
from trn_po_prn_details_sales_order
WHERE 
trn_po_prn_details_sales_order.ORDER_NO = ware_mrndetails.intOrderNo AND 
trn_po_prn_details_sales_order.ORDER_YEAR = ware_mrndetails.intOrderYear AND
trn_po_prn_details_sales_order.SALES_ORDER = ware_mrndetails.strStyleNo AND  
trn_po_prn_details_sales_order.ITEM = ware_mrndetails.intItemId ) AS PRI_QTY, 


(select SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'RETURNED_TO_STORES',
 trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS RET_TO_STORE_QTY 
FROM trn_po_prn_details_sales_order_stock_transactions 
WHERE trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = ware_mrndetails.intOrderNo 
AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = ware_mrndetails.intOrderYear 
AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = ware_mrndetails.strStyleNo
AND trn_po_prn_details_sales_order_stock_transactions.ITEM = ware_mrndetails.intItemId 
AND trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID = '$locationId' ) as ret_to_stores,

 (select SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS',
 trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS GP_QTY 
FROM trn_po_prn_details_sales_order_stock_transactions 
WHERE trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = ware_mrndetails.intOrderNo AND 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = ware_mrndetails.intOrderYear AND 
 trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = ware_mrndetails.strStyleNo AND 
trn_po_prn_details_sales_order_stock_transactions.ITEM = ware_mrndetails.intItemId AND 
trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID = '$locationId' )as GP_Out, 

(select SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS_IN', 
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS GP_IN_QTY 
FROM trn_po_prn_details_sales_order_stock_transactions 
WHERE trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = ware_mrndetails.intOrderNo AND 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = ware_mrndetails.intOrderYear AND 
 trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = ware_mrndetails.strStyleNo AND
trn_po_prn_details_sales_order_stock_transactions.ITEM = ware_mrndetails.intItemId AND 
trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID = '$locationId' )GP_IN_QTY 

from ware_mrndetails 
INNER JOIN ware_mrnheader on ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND 
ware_mrndetails.intMrnYear= ware_mrnheader.intMrnYear 
INNER JOIN mst_item ON ware_mrndetails.intItemId = mst_item.intId 
INNER JOIN mst_locations on ware_mrnheader.intCompanyId = mst_locations.intId
INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId 
INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId 
INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
WHERE 
((ware_mrnheader.intStatus <= (ware_mrnheader.intApproveLevels+1) && ware_mrnheader.intStatus >1) 
-- || ware_mrnheader.intStatus=0
)

AND 
mst_locations.intCompanyId IN ($company)
GROUP BY 
ware_mrndetails.intOrderNo,
ware_mrndetails.intOrderYear,
ware_mrndetails.strStyleNo,
ware_mrndetails.intItemId
) as  TB1



GROUP BY 
TB1.intOrderNo,
TB1.intOrderYear,
TB1.intItemId 
)AS TB2

GROUP BY 
TB2.intItemId 
) 

AS TB_ALL

where
1=1
";

$sql .=  "  AND TB_ALL.intCompanyId IN ($company)";


if($maincategory !='null')
{
$sql .=  "  AND TB_ALL.mainCat_id IN ($maincategory)";
}

if($subcategory !='null')
{
$sql .=  " AND TB_ALL.subCat_id IN ($subcategory)";
}
if($item !='null')
{
$sql .=  " AND TB_ALL.intItemId IN ($item)";
}
$sql .=   "ORDER BY
		   
	       TB_ALL.mainCat_id,
	       TB_ALL.subCat_id,
		   TB_ALL.intItemId 
		   ASC";

return $sql;	
}

function getRollingStock($item,$curYear,$locationId)
{        
		global $db;
		$sql = "select item_rolling_stock.qty AS qty
		FROM item_rolling_stock
		WHERE
		item_rolling_stock.locationId = '$locationId'
		AND
		item_rolling_stock.intItemId = '$item'
		AND
		item_rolling_stock.`year`   = '$curYear'";
		
        $result  = $db->RunQuery($sql);
	    $row     = mysqli_fetch_array($result);
	    return $row['qty'];
	
}


function get_stock_balance($item,$head_office_loc)
{
	
	global $db;
	
	$sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) as qty 
			FROM `ware_stocktransactions_bulk`
			WHERE
			ware_stocktransactions_bulk.intItemId = '$item' AND
			ware_stocktransactions_bulk.intLocationId = '$head_office_loc'
			";
	
	$result_stock = $db->RunQuery($sql);
	$row_stock    = mysqli_fetch_array($result_stock);
	$stock_bal 	  = round (($row_stock['qty']),4);
	return $stock_bal;



	
}

function getHOStock($item,$head_office_loc)
{

	global $db;
	
	$sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) as qty 
			FROM `ware_stocktransactions_bulk`
			WHERE
			ware_stocktransactions_bulk.intItemId = '$item' AND
			ware_stocktransactions_bulk.intLocationId = '$head_office_loc' 
			";
	
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	return $row['qty'];

}
function get_approved_non_grn_pos($item,$location){
	
	global $db;
	
	$sql = "SELECT
			sum(trn_podetails.dblQty-trn_podetails.dblGRNQty) as qty
			FROM
			trn_podetails
			INNER JOIN trn_poheader ON trn_podetails.intPONo = trn_poheader.intPONo AND trn_podetails.intPOYear = trn_poheader.intPOYear
			WHERE
			trn_poheader.intStatus = 1 AND
			trn_poheader.intDeliveryTo = '$location' AND
			trn_podetails.intItem = '$item'
			";
	
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	return $row['qty'];
	
	
}
function get_ho_loc1($companyId){
    global $db;
    $sql="SELECT
			mst_locations.intId
			FROM `mst_locations`
			WHERE
			mst_locations.COMPANY_MAIN_STORES_LOCATION = 1 AND
			mst_locations.intCompanyId = '$companyId'
			";
			
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['intId'];

}

?>
<?php
ini_set('display_errors',0);
$company 			= $_SESSION['headCompanyId'];
$locationId 		= $_SESSION['CompanyID'];//this locationId use in report header(reportHeader.php)-------
$intUser  			= $_SESSION["userId"];
$mainPath 			= $_SESSION['mainPath'];
$thisFilePath 		=  $_SERVER['PHP_SELF'];
require_once 		"class/cls_commonFunctions_get.php";
require_once 		"class/warehouse/cls_warehouse_get.php";

$obj_common			= new cls_commonFunctions_get($db);
$obj_wh_get			= new cls_warehouse_get($db);

$company_selected 	= $_REQUEST['company'];
$location_selected	= $_REQUEST['location'];
$mainCategory 		= $_REQUEST['mainCategory'];
$subCategory		= $_REQUEST['subCategory'];
$itemId 			= $_REQUEST['itemId'];
$from	 			= $_REQUEST['fromDate'];
$to		 			= $_REQUEST['toDate'];


$sql = "SELECT 
			mst_companies.intId,
			mst_companies.strName,
			mst_companies.intBaseCurrencyId,
			mst_financecurrency.strCode AS currency 
			FROM mst_companies 
			LEFT JOIN mst_financecurrency ON mst_companies.intBaseCurrencyId = mst_financecurrency.intId 
			WHERE mst_companies.intId='$company' AND
			mst_companies.intStatus = 1 
			ORDER BY mst_companies.strName";

$res_comp = 	$db->RunQuery($sql);
//$res_comp		= $obj_common->loadCompany_result($company,'RunQuery');
$row_comp		= mysqli_fetch_array($res_comp);
$baseCurrency 	= $row_comp['intBaseCurrencyId'];


$sqlp = "SELECT
			mst_financecurrency.strCode
			FROM
			mst_financecurrency
			WHERE
			mst_financecurrency.intId = '$baseCurrency'
			";
$resultp = $db->RunQuery($sqlp);
$rowp=mysqli_fetch_array($resultp);
$currency_desc	=$rowp['strCode'];
//$currency_desc	= $obj_common->getCurrencyName($baseCurrency);

//$month_desc	= $obj_common->get_month_name($month,'RunQuery');

?>
<head>
    <title>Monthly Gate Pass Report</title>
    <style>
        .break { page-break-before: always; }

        @media print {
            .noPrint
            {
                display:none;
            }
        }
        #apDiv1 {
            position:absolute;
            left:264px;
            top:247px;
            width:650px;
            height:322px;
            z-index:1;
        }
        .APPROVE {
            font-size: 18px;
            font-weight: bold;
        }
    </style>
</head>

<body>
<form id="frmRM_expiry_Report" name="frmRM_expiry_Report" method="post" action="rpt_item_aging.php">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td width="20%"></td>
            <td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
            <td width="20%"></td>
        </tr>

        <tr>
            <td colspan="3"></td>
        </tr>
    </table>
    <div align="center">
        <div style="background-color:#FFF" ><strong>MONTHLY TRANSFERIN  REPORT</strong></div>
        <table width="968" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td width="905">
                    <table width="100%">
                        <tr>
                            <td width="4%"></td>
                            <td width="11%" class="normalfnt" align="right"><strong>Date from</strong></td>
                            <td width="1%" align="center" valign="middle"><strong>:</strong></td>
                            <td width="8%" align="left"><span class="normalfnt" style="text-align:left"><?php echo $from  ?></span></td>
                            <td width="10%" class="normalfnt"><strong>Date To</strong></td>
                            <td width="1%" align="center" valign="middle"><strong>:</strong></td>
                            <td width="12%"><span class="normalfnt"><?php echo $to  ?></span></td>
                            <td width="7%" class="normalfnt"><strong>Currncy</strong></td>
                            <td width="1%" align="center" valign="middle"><strong>:</strong></td>
                            <td width="45%"><span class="normalfnt"><?php echo $currency_desc  ?></span></td>
                        </tr>
                        <tr>
                            <td colspan="10">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td colspan="7" class="normalfnt">
                                <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
                                    <thead>
                                    <tr>
                                        <th width="10%" nowrap="nowrap" >Company</th>
                                        <th width="10%" nowrap="nowrap" >Location</th>
                                        <th width="10%" nowrap="nowrap" >Gate Pass No</th>
                                        <th width="10%" nowrap="nowrap" >Gate Pass Tranferin No</th>
                                        <th width="10%" nowrap="nowrap" >Date</th>
                                        <th width="10%" nowrap="nowrap" >Main Category</th>
                                        <th width="10%" nowrap="nowrap" >Sub Category</th>
                                        <th width="20%" nowrap="nowrap" >Item</th>
                                        <th width="5%" nowrap="nowrap" >UOM</th>
                                        <th width="5%" >Qty</th>
                                        <th width="5%" >Value</th>
                                    </tr>
                                    </thead>
                                    <?php
                                    $tmp_serial ="";
                                    $arr_serial_sum = NULL;
                                    $tot_sum		=0;
                                    $rcds			=0;
                                    $sql		= $obj_wh_get->get_bulk_tranferin_details($from,$to,$company_selected,$location_selected,$mainCategory,$subCategory,$itemId,$baseCurrency);
                                    $result		= $db->RunQuery($sql);
                                    while($row=mysqli_fetch_array($result))
                                    {
                                    $rcds++;
                                    $company		= $row['COMPANY'];
                                    $location		= $row['LOCATION'];
                                    $tolocation		= $row['TOLOCATION'];
                                    $serial			= $row['SERIAL_NO'];
                                    $date			= $row['GP_DATE'];
                                    $mainCategory	= $row['MAIN_CAT'];
                                    $subCategory	= $row['SUB_CAT'];
                                    $item   		= $row['ITEM'];
                                    $uom			= $row['UNIT'];
                                    $qty			= $row['QTY'];
                                    $value			= $row['VALUE'];
                                    ?>
                                    <?php
                                    if($serial !=$tmp_serial  && $tmp_serial !='' ){
                                    ?>
                                    <tbody>
                                    <tr height="17" class="normalfnt"  bgcolor="#CCCCCC">
                                        <td colspan="9">
                                        <td class="normalfntRight" valign="middle"><?php echo number_format($arr_serial_sum[$tmp_serial],2); ?></td>
                                    </tr>
                                    <?php
                                    }
                                    ?>

                                    <tr height="17" class="normalfnt"  bgcolor="#FFFFFF">
                                        <td class="normalfntMid" nowrap="nowrap" ><?php echo $company  ?></td>
                                        <td class="normalfntMid" nowrap="nowrap" ><?php echo $location  ?></td>
                                        <td class="normalfntMid" nowrap="nowrap" ><?php echo $tolocation  ?></td>
                                        <td class="normalfntMid" nowrap="nowrap" ><?php echo $serial  ?></td>
                                        <td class="normalfntMid" nowrap="nowrap" ><?php echo $date?></td>
                                        <td class="normalfntMid" nowrap="nowrap" ><?php echo $mainCategory?></td>
                                        <td class="normalfntMid" nowrap="nowrap" ><?php echo $subCategory ?></td>
                                        <td class="normalfntMid" nowrap="nowrap" ><?php echo $item ?></td>
                                        <td class="normalfntMid" nowrap="nowrap" ><?php echo $uom ?></td>
                                        <td  class="normalfntRight" valign="middle"><?php echo number_format($qty,2); ?></td>
                                        <td   class="normalfntRight" valign="middle"><?php echo number_format($value,2); ?></td>
                                    </tr>


                                    <?php
                                    $arr_serial_sum[$serial] +=round($value,2);
                                    $tmp_serial				= $serial;
                                    $tot_sum				+=round($value,2);
                                    }
                                    ?>
                                    <?php
                                    if($rcds >0  ){
                                        ?>
                                        <tr height="17" class="normalfnt"  bgcolor="#CCCCCC">
                                            <td colspan="10">
                                            <td class="normalfntRight" valign="middle"><?php echo number_format($arr_serial_sum[$tmp_serial],2); ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    <tr height="17" class="normalfnt"  bgcolor="#999999">
                                        <td colspan="10">
                                        <td class="normalfntRight" valign="middle"><?php echo number_format($tot_sum,2); ?></td>
                                    </tr>
                                    </tbody>
                                </table>

                            </td>
                        </tr>

                    </table>
                </td>
            </tr>

            <tr height="40">
                <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
            </tr>
        </table>
    </div>
</form>
</body>




</html>


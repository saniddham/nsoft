<?php

//include_once "../../../dataAccess/Connector.php";

//$result2 = $db->RunQuery($report_sql);

header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=report.xls" );
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");

$totQty		= 0;
$totAmount 	= 0;
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="#99CCFF">Department</td>
    <td bgcolor="#99CCFF">Company</td>
    <td bgcolor="#99CCFF">Location</td>
    <td bgcolor="#99CCFF">Issue No</td>
    <td bgcolor="#99CCFF">Order Nor</td>
    <td bgcolor="#99CCFF">Order Year</td>
    <td bgcolor="#99CCFF">Main Category</td>
    <td bgcolor="#99CCFF">Sub Category</td>
    <td bgcolor="#99CCFF">Item</td>
    <td bgcolor="#99CCFF">UOM</td>
    <td bgcolor="#99CCFF">Currency</td>
    <td bgcolor="#99CCFF">Price</td>
    <td bgcolor="#99CCFF">Qty</td>
    <td bgcolor="#99CCFF">Amount</td>
  </tr>
<?php


while($row = mysqli_fetch_array($result))
{
?>
  <tr>
    <td><?php echo $row['Department']; ?></td>
    <td><?php echo $row['Company']; ?></td>
    <td><?php echo $row['Location']; ?></td>
    <td><?php echo $row['Issue No']; ?></td>
    <td><?php echo $row['Order No']; ?></td>
    <td><?php echo $row['Style No']; ?></td>
    <td><?php echo $row['Main Category']; ?></td>
    <td><?php echo $row['Sub Category']; ?></td>
    <td><?php echo $row['Item']; ?></td>
    <td><?php echo $row['UOM']; ?></td>
    <td><?php echo $row['Currency']; ?></td>
    <td><?php echo $row['Price']; ?></td>
    <td><?php echo $row['Qty']; ?></td>
    <td><?php echo $row['Amount']; ?></td>
  </tr>
<?php
	
	$totQty 	= $totQty+$row['Qty'];
	$totAmount 	= $totAmount+$row['Amount'];
}
?>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><?php echo round($totQty,4); ?></td>
    <td><?php echo round($totAmount,4); ?></td>
  </tr>
</table>

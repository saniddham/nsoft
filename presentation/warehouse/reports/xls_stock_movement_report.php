<?php

//include_once "../../../dataAccess/Connector.php";

//$result2 = $db->RunQuery($report_sql);
//
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=report.xls" );
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");

$fields		="#8A8787";
$plus_qty	="#5CDFEF";
$plus_val	="#F59CC0";
$minus_qty	="#5CDFEF";
$minus_val	="#F59CC0";

$fields1	="#D4D4D4";
$plus_qty1	="#BBE0ED";
$plus_val1	="#D8A7A8";
$minus_qty1	="#BBE0ED";
$minus_val1	="#D8A7A8";

$docNos		='#E06A6C';

?>

<table width="100%" border="1" cellspacing="0" cellpadding="0">
 
 <tr>
 <td><strong>Company</strong></td>
 <td><strong><?php echo $header_data['company']; ?></strong></td>
 <td><strong>Location</strong></td>
 <td><strong><?php echo $header_data['location']; ?></strong></td>
 </tr>
 <tr>
 <td><strong>Main Category</strong></td>
 <td><strong><?php echo $header_data['main_category']; ?></strong></td>
 <td><strong>Sub Category</strong></td>
 <td><strong><?php echo $header_data['sub_category']; ?></strong></td>
 </tr>
 <tr>
 <td><strong>Date Range</strong></td>
 <td><strong><?php echo $header_data['range']; ?></strong></td>
 <td><strong>Currency</strong></td>
 <td><strong><?php echo $header_data['currency']; ?></strong></td>
 </tr>
 <tr height="20">
 <td colspan="37"></td>	
 </tr>
 
 
  <tr>
    <td bgcolor="<?php echo $fields;?>"><strong>DATE</strong></td>
    <td bgcolor="<?php echo $fields;?>"><strong>COMPANY</strong></td>
    <td bgcolor="<?php echo $fields;?>"><strong>LOCATION</strong></td>
    <td bgcolor="<?php echo $fields;?>"><strong>MAIN CATEGORY</strong></td>
    <td bgcolor="<?php echo $fields;?>"><strong>SUB CATEGORY</strong></td>
    <td bgcolor="<?php echo $fields;?>"><strong>CODE</strong></td>
     
    <td bgcolor="<?php echo $fields;?>"><strong>ITEM</strong></td>
    <td bgcolor="<?php echo $fields;?>"><strong>UOM</strong></td>
    <td bgcolor="<?php echo $plus_qty;?>"><strong>GRN QTY (+)</strong></td>
    <td bgcolor="<?php echo $plus_val;?>"><strong>GRN VALUE (+)</strong></td>
    <td bgcolor="<?php echo $docNos;?>"><strong>GRN NOS</strong></td>
    <td bgcolor="<?php echo $plus_qty;?>"><strong>TRANSFERED IN QTY (+)</strong></td>
    <td bgcolor="<?php echo $plus_val;?>"><strong>TRANSFERED IN VALUE (+)</strong></td>
    <td bgcolor="<?php echo $docNos;?>"><strong>TRANSFERED IN NOS</strong></td>
    <td bgcolor="<?php echo $minus_qty;?>"><strong>GATE PASS TRANSFERED OUT QTY (-)</strong></td>
    <td bgcolor="<?php echo $minus_val;?>"><strong>GATE PASS TRANSFERED OUT VALUE (-)</strong></td>
    <td bgcolor="<?php echo $docNos;?>"><strong>GATE PASS TRANSFERED OUT NOS</strong></td>
    <td bgcolor="<?php echo $minus_qty;?>"><strong>ISSUED QTY (-)</strong></td>
    <td bgcolor="<?php echo $minus_val;?>"><strong>ISSUE VALUE (-)</strong></td>
    <td bgcolor="<?php echo $docNos;?>"><strong>ISSUED NOS</strong></td>
    <td bgcolor="<?php echo $plus_qty;?>"><strong>RETURNED ISSUE QTY (+)</strong></td>
    <td bgcolor="<?php echo $plus_val;?>"><strong>RETURNED ISSUE VALUE (+)</strong></td>
    <td bgcolor="<?php echo $docNos;?>"><strong>RETURNED ISSUE NOS</strong></td>
    <td bgcolor="<?php echo $minus_qty;?>"><strong>RETURNED TO SUPPLIER QTY (-)</strong></td>
    <td bgcolor="<?php echo $minus_val;?>"><strong>RETURNED TO SUPPLIER VALUE (-)</strong></td>
    <td bgcolor="<?php echo $docNos;?>"><strong>RETURNED TO SUPPLIER NOS</strong></td>
    <td bgcolor="<?php echo $minus_qty;?>"><strong>DISPOSED QTY (-)</strong></td>
    <td bgcolor="<?php echo $minus_val;?>"><strong>DISPOSED VALUE (-)</strong></td>
    <td bgcolor="<?php echo $docNos;?>"><strong>DISPOSED NOS</strong></td>
    <td bgcolor="<?php echo $plus_qty;?>"><strong>ITEM CREATION QTY (+)</strong></td>
    <td bgcolor="<?php echo $plus_val;?>"><strong>ITEM CREATION QTY VALUE (+)</strong></td>
    <td bgcolor="<?php echo $docNos;?>"><strong>ITEM CREATION NOS</strong></td>
    <td bgcolor="<?php echo $minus_qty;?>"><strong>ITEM CREATION QTY (-)</strong></td>
    <td bgcolor="<?php echo $minus_val;?>"><strong>ITEM CREATION QTY VALUE (-)</strong></td>
    <td bgcolor="<?php echo $docNos;?>"><strong>ITEM CREATION NOS</strong></td>
    <td bgcolor="<?php echo $plus_qty;?>"><strong>OPENNING STOCK (+)</strong></td>
    <td bgcolor="<?php echo $plus_val;?>"><strong>OPENNING STOCK VALUE (+)</strong></td>
    <td bgcolor="<?php echo $docNos;?>"><strong>OPENNING STOCK NOS</strong></td>
    <td bgcolor="<?php echo $minus_qty;?>"><strong>CLOSING STOCK QTY (-)</strong></td>
    <td bgcolor="<?php echo $minus_val;?>"><strong>CLOSING STOCK VALUE (-)</strong></td>
    <td bgcolor="<?php echo $docNos;?>"><strong>CLOSING STOCK NOS</strong></td>
    <td bgcolor="<?php echo $plus_qty;?>"><strong>STOCK ADJUSTED VIA QTY (+)</strong></td>
    <td bgcolor="<?php echo $plus_val;?>"><strong>STOCK ADJUSTED VIA QTY VALUE (+)</strong></td>
    <td bgcolor="<?php echo $minus_qty;?>"><strong>STOCK ADJUSTED VIA QTY (-)</strong></td>
    <td bgcolor="<?php echo $minus_val;?>"><strong>STOCK ADJUSTED VIA VALUE (-)</strong></td>
    <td bgcolor="<?php echo $plus_qty;?>"><strong>STOCK ADJUSTMENT via QTY & RATE (+)</strong></td>
    <td bgcolor="<?php echo $plus_val;?>"><strong>STOCK ADJUSTMENT via QTY & RATE VALUE (+)</strong></td>
    <td bgcolor="<?php echo $plus_val;?>"><strong>STOCK ADJUSTMENT via RATE VALUE (+)</strong></td>
  
    
    
    
    
  </tr>
<?php


while($row = mysqli_fetch_array($result))
{
?>
  <tr>
    <td bgcolor="<?php echo $fields1;?>"><?php echo $row['datez']; ?></td>
    <td bgcolor="<?php echo $fields1;?>"><?php echo $row['company']; ?></td>
    <td bgcolor="<?php echo $fields1;?>"><?php echo $row['location']; ?></td>
    <td bgcolor="<?php echo $fields1;?>"><?php echo $row['main_category']; ?></td>
    <td bgcolor="<?php echo $fields1;?>"><?php echo $row['sub_category']; ?></td>
    <?php
    if($row['SUP_ITEM_CODE']!=null){
        ?>
    <td bgcolor="<?php echo $fields1;?>"><?php echo $row['SUP_ITEM_CODE']; ?></td>
    <?php
    }else{
        ?>
    
    <td bgcolor="<?php echo $fields1;?>"><?php echo $row['code']; ?></td>
    <?php
    }
    ?>
    
    <td bgcolor="<?php echo $fields1;?>"><?php if($row['ITEM_HIDE']==1) echo "*****"; else echo $row['item']; ?></td>
    <td bgcolor="<?php echo $fields1;?>"><?php echo $row['uom']; ?></td>
    <td bgcolor="<?php echo $plus_qty1;?>"><?php echo $row['GRN']; ?></td>
    <td bgcolor="<?php echo $plus_val1;?>"><?php echo $row['GRN_VALUE']; ?></td>
    <td bgcolor="<?php echo $docNos;?>"><?php echo $row['GRN_NOS']; ?></td>
    <td bgcolor="<?php echo $plus_qty1;?>"><?php echo $row['TRANSFERIN']; ?></td>
    <td bgcolor="<?php echo $plus_val1;?>"><?php echo $row['TRANSFERIN_VALUE']; ?></td>
    <td bgcolor="<?php echo $docNos;?>"><?php echo $row['TRANSFERIN_NOS']; ?></td>
    <td bgcolor="<?php echo $minus_qty1;?>"><?php echo $row['GATEPASS']; ?></td>
    <td bgcolor="<?php echo $minus_val1;?>"><?php echo $row['GATEPASS_VALUE']; ?></td>
    <td bgcolor="<?php echo $docNos;?>"><?php echo $row['GATEPASS_NOS']; ?></td>
    <td bgcolor="<?php echo $minus_qty1;?>"><?php echo $row['ISSUE']; ?></td>
    <td bgcolor="<?php echo $minus_val1;?>"><?php echo $row['ISSUE_VALUE']; ?></td>
    <td bgcolor="<?php echo $docNos;?>"><?php echo $row['ISSUE_NOS']; ?></td>
    <td bgcolor="<?php echo $plus_qty1;?>"><?php echo $row['RETSTORES']; ?></td>
    <td bgcolor="<?php echo $plus_val1;?>"><?php echo $row['RETSTORES_VALUE']; ?></td>
    <td bgcolor="<?php echo $docNos;?>"><?php echo $row['RETSTORES_NOS']; ?></td>
    <td bgcolor="<?php echo $minus_qty1;?>"><?php echo $row['RTSUP']; ?></td>
    <td bgcolor="<?php echo $minus_val1;?>"><?php echo $row['RTSUP_VALUE']; ?></td>
    <td bgcolor="<?php echo $docNos;?>"><?php echo $row['RTSUP_NOS']; ?></td>
    <td bgcolor="<?php echo $minus_qty1;?>"><?php echo $row['Dispose']; ?></td>
    <td bgcolor="<?php echo $minus_val1;?>"><?php echo $row['Dispose_VALUE']; ?></td>
    <td bgcolor="<?php echo $docNos;?>"><?php echo $row['Dispose_NOS']; ?></td>
    <td bgcolor="<?php echo $plus_qty1;?>"><?php echo $row['ITEMCREATION_plus']; ?></td>
    <td bgcolor="<?php echo $plus_val1;?>"><?php echo $row['ITEMCREATION_plus_VALUE']; ?></td>
    <td bgcolor="<?php echo $docNos;?>"><?php echo $row['ITEMCREATION_plus_NOS']; ?></td>
    <td bgcolor="<?php echo $minus_qty1;?>"><?php echo $row['ITEMCREATION_minus']; ?></td>
    <td bgcolor="<?php echo $minus_val1;?>"><?php echo $row['ITEMCREATION_minus_VALUE']; ?></td>
    <td bgcolor="<?php echo $docNos;?>"><?php echo $row['ITEMCREATION_minus_NOS']; ?></td>
    <td bgcolor="<?php echo $plus_qty1;?>"><?php echo $row['OPENSTOCK']; ?></td>
    <td bgcolor="<?php echo $plus_val1;?>"><?php echo $row['OPENSTOCK_VALUE']; ?></td>
    <td bgcolor="<?php echo $docNos;?>"><?php echo $row['OPENSTOCK_NOS']; ?></td>
    <td bgcolor="<?php echo $minus_qty1;?>"><?php echo $row['CLOSESTOCK']; ?></td>
    <td bgcolor="<?php echo $minus_val1;?>"><?php echo $row['CLOSESTOCK_VALUE']; ?></td>
    <td bgcolor="<?php echo $docNos;?>"><?php echo $row['CLOSESTOCK_NOS']; ?></td>
     <td bgcolor="<?php echo $plus_qty1;?>"><?php echo $row['Adjust_qty_plus']; ?></td>
    <td bgcolor="<?php echo $plus_val1;?>"><?php echo $row['Adjust_qty_plus_VALUE']; ?></td>
    <td bgcolor="<?php echo $minus_qty1;?>"><?php echo $row['Adjust_Q_minus']; ?></td>
    <td bgcolor="<?php echo $minus_val1;?>"><?php echo $row['Adjust_Q_minus_VALUE']; ?></td>
     <td bgcolor="<?php echo $plus_qty1;?>"><?php echo $row['Adjust_rate_and_qty']; ?></td>
    <td bgcolor="<?php echo $plus_val1;?>"><?php echo $row['Adjust_rate_and_qty_VALUE']; ?></td>
    <td bgcolor="<?php echo $plus_val1;?>"><?php echo $row['Adjust_rate_plus_VALUE']; ?></td>
    
    
    
   
    
    
    			
  </tr>
<?php

}
?>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    
  </tr>
</table>

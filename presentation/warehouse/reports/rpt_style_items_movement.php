<?php
 $companyId 			= $_SESSION['CompanyID'];
$locationId 		= $_SESSION['CompanyID'];//this locationId use in report header(reportHeader.php)-------
$intUser  			= $_SESSION["userId"];
$mainPath 			= $_SESSION['mainPath'];
$thisFilePath 		=  $_SERVER['PHP_SELF'];
 require_once 		"class/cls_commonFunctions_get.php";
  
$obj_common	= new cls_commonFunctions_get($db);


$company 	= $_REQUEST['company'];
$location 	= $_REQUEST['location'];
$subStores 	= $_REQUEST['subStores'];
$orderNo 	= $_REQUEST['orderNo'];
$orderYear 	= $_REQUEST['orderYear'];
$salesOrder = $_REQUEST['salesOrder'];
$item 		= $_REQUEST['item'];
//$item		= 703;

$result			= $obj_common->loadCompany_result($company,'RunQuery');
$row 			= mysqli_fetch_array($result);
$company_desc 	= $row['strName'];

$result			= $obj_common->loadLocationName($location,'RunQuery');
$row 			= mysqli_fetch_array($result);
$location_desc	= $row['strName'];

$result			= $obj_common->load_sub_stores_result($subStores,'RunQuery');
$row 			= mysqli_fetch_array($result);
$subStores_desc	= $row['strName'];

$result			= $obj_common->load_sales_order_result($orderNo,$orderYear,$salesOrder,'RunQuery');
$row 			= mysqli_fetch_array($result);
$salesOrder_desc= $row['salesOrder'];

$arr_item			= $obj_common->get_item_details($item,'RunQuery');
 
 //----------------------------------
?>
 <head>
 <title>Order allocated Items Stock Movement Report</title>
 <style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:264px;
	top:247px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<form id="frmMrnReport" name="frmMrnReport" method="post" >
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>ORDER ALLOCATED ITEM STOCK MOVEMENT REPORT</strong></div>
<table width="1090" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td width="1085">
  <table width="100%">
   <tr>
     <td width="5%"></td>
     <td width="13%" class="normalfnt" align="right"><strong style="text-align:right">Company</strong></td>
     <td width="2%" align="center" valign="middle"><strong>:</strong></td>
     <td width="29%" align="left"><span class="normalfnt" style="text-align:left"><?php echo $company_desc  ?></span></td>
     <td width="15%" class="normalfnt"><strong>Location</strong></td>
     <td width="1%" align="center" valign="middle"><strong>:</strong></td>
     <td width="22%"><span class="normalfnt"><?php echo $location_desc  ?></span></td>
     <td width="6%" class="normalfnt"></td>
     <td width="7%"></td>
   </tr>
   <tr>
     <td></td>
     <td class="normalfnt"><strong>Order No</strong></td>
     <td align="center" valign="middle"><strong>:</strong></td>
     <td><span class="normalfnt"><?php if($orderNo !='' ){echo $orderYear."/".$orderNo;}  ?></span></td>
     <td class="normalfnt" align="right"><strong style="text-align:right">Sales Order</strong></td>
     <td align="center" valign="middle"><strong>:</strong></td>
     <td><span class="normalfnt" style="text-align:left"><?php echo $salesOrder_desc  ?></span></td>
     <td></td>
     <td></td>
   </tr>
<tr>
    <td></td>
    <td class="normalfnt"><strong>Main Category</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $arr_item['main_category'] ?></span></td>
    <td class="normalfnt" align="right"><strong style="text-align:right">Sub Category</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $arr_item['sub_category'] ?></span></td>
    <td></td>
    <td></td>
  </tr>
<tr>
    <td></td>
    <td class="normalfnt"><strong>Item</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $arr_item['item'] ?></span></td>
    <td class="normalfnt" align="right">&nbsp;</td>
    <td align="center" valign="middle"></td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
  </tr>     </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
    <?php
	  $sqlM	= "
				SELECT 
				ware_stocktransactions.intGRNNo,
				ware_stocktransactions.intGRNYear,
				ware_stocktransactions.dtGRNDate,
				ware_stocktransactions.dblGRNRate,
				mst_locations.strName AS location ,
				mst_financecurrency.strCode as currency  ,
				ware_stocktransactions.intCurrencyId 
 				FROM `ware_stocktransactions` 
				INNER JOIN mst_locations ON ware_stocktransactions.intLocationId = mst_locations.intId
				INNER JOIN mst_financecurrency ON ware_stocktransactions.intCurrencyId = mst_financecurrency.intId
				WHERE
				ware_stocktransactions.intOrderNo = '$orderNo' AND
				ware_stocktransactions.intOrderYear = '$orderYear' AND
				ware_stocktransactions.intSalesOrderId = '$salesOrder' AND
				ware_stocktransactions.intItemId = '$item' AND
				ware_stocktransactions.intLocationId = '$location'
				GROUP BY
				ware_stocktransactions.dtGRNDate,
				ware_stocktransactions.intGRNNo,
				ware_stocktransactions.intGRNYear,
				ware_stocktransactions.dblGRNRate,
				ware_stocktransactions.intCurrencyId
	";
	//echo $sqlM;
	$resultM = $db->RunQuery($sqlM);
	while($rowM	=	mysqli_fetch_array($resultM))
	{
		$grnNo		= $rowM['intGRNNo'];
		$grnYear	= $rowM['intGRNYear'];
		$grnRate	= $rowM['dblGRNRate'];
		$grnDate	= $rowM['dtGRNDate']; 
		$currency	= $rowM['intCurrencyId'];
	?>
      <tr>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr>
              <th align="right"  >GRN No : </th>
              <th align="left" colspan="2" ><span class="normalfnt"><?php echo $rowM['intGRNNo'].'/'.$rowM['intGRNYear']?></span></th>
              <th colspan="3" >Date : <span class="normalfnt"><?php echo $rowM['dtGRNDate']?></span></th>
              <th colspan="2" >Rate : <span class="normalfnt"><?php echo $rowM['dblGRNRate'].' : '.$rowM['currency'];?></span></th>
              <th colspan="5" >GRN Location : <span class="normalfnt"><?php echo $rowM['location']?></span></th>
              </tr>
            <tr>
              <th width="9%" >Document No</th>
              <th width="13%" >Transaction Type</th>
              <th width="10%" >Main Stores Qty</th>
              <th width="9%" >Color Room Qty</th>
              <th width="4%" >By</th>
              <th width="6%" >Date</th>
              <th width="11%" > Approved By</th>
              <th width="9%" >Related Document No</th>
              <th width="4%" >Type</th>
              <th width="4%" >Qty</th>
              <th width="4%" >By</th>
              <th width="5%" >Date</th>
              <th width="11%" > Approved By</th>
              </tr>
            <?php 
            
            $sql = "SELECT * FROM ((SELECT
						ware_stocktransactions.strType,
						ware_stocktransactions.intDocumentNo,
						ware_stocktransactions.intDocumntYear,
						round(ware_stocktransactions.dblQty,4) as dblQty,
						ware_stocktransactions.dtDate,
						ware_stocktransactions.intUser,
						sys_users.strUserName
						FROM
						ware_stocktransactions
						INNER JOIN mst_locations ON ware_stocktransactions.intLocationId = mst_locations.intId
						INNER JOIN mst_financecurrency ON ware_stocktransactions.intCurrencyId = mst_financecurrency.intId
						INNER JOIN sys_users ON ware_stocktransactions.intUser = sys_users.intUserId
						WHERE
						ware_stocktransactions.intOrderNo = '$orderNo' AND
						ware_stocktransactions.intOrderYear = '$orderYear' AND
						ware_stocktransactions.intSalesOrderId = '$salesOrder' AND
						ware_stocktransactions.intItemId = '$item' AND
						ware_stocktransactions.intLocationId = '$location' AND
						ware_stocktransactions.intGRNNo = '$grnNo' AND
						ware_stocktransactions.intGRNYear = '$grnYear' AND
						ware_stocktransactions.dtGRNDate = '$grnDate' AND
						ware_stocktransactions.dblGRNRate = '$grnRate' AND
						ware_stocktransactions.intCurrencyId = '$currency'
						/*GROUP BY
						ware_stocktransactions.strType,
						ware_stocktransactions.intDocumentNo,
						ware_stocktransactions.intDocumntYear*/
						ORDER BY
						ware_stocktransactions.dtDate ASC)
						UNION
						(SELECT
						ware_sub_stocktransactions_bulk.strType,
						ware_sub_stocktransactions_bulk.intDocumentNo,
						ware_sub_stocktransactions_bulk.intDocumntYear,
						ware_sub_stocktransactions_bulk.dblQty,
						ware_sub_stocktransactions_bulk.dtDate,
						ware_sub_stocktransactions_bulk.intUser,
						sys_users.strUserName
						FROM
						ware_sub_stocktransactions_bulk
						INNER JOIN mst_locations ON ware_sub_stocktransactions_bulk.intLocationId = mst_locations.intId
						INNER JOIN mst_financecurrency ON ware_sub_stocktransactions_bulk.intCurrencyId = mst_financecurrency.intId
						INNER JOIN sys_users ON ware_sub_stocktransactions_bulk.intUser = sys_users.intUserId
						WHERE 
						ware_sub_stocktransactions_bulk.strType = 'INK_ALLOC' AND
						ware_sub_stocktransactions_bulk.intOrderNo = '$orderNo' AND
						ware_sub_stocktransactions_bulk.intOrderYear = '$orderYear' AND
						ware_sub_stocktransactions_bulk.intSalesOrderId = '$salesOrder' AND
						ware_sub_stocktransactions_bulk.intItemId = '$item' AND
						ware_sub_stocktransactions_bulk.intLocationId = '$location' AND
						ware_sub_stocktransactions_bulk.intGRNNo = '$grnNo' AND
						ware_sub_stocktransactions_bulk.intGRNYear = '$grnYear' AND
						ware_sub_stocktransactions_bulk.dtGRNDate = '$grnDate' AND
						ware_sub_stocktransactions_bulk.dblGRNRate = '$grnRate' AND
						ware_sub_stocktransactions_bulk.intCurrencyId = '$currency'
						/*GROUP BY
						ware_sub_stocktransactions_bulk.strType,
						ware_sub_stocktransactions_bulk.intDocumentNo,
						ware_sub_stocktransactions_bulk.intDocumntYear*/
						ORDER BY
						ware_sub_stocktransactions_bulk.dtDate ASC
						) ) AS tb1 ORDER BY tb1.dtDate ASC
                         ";
    
            // echo $sql;
    
            $result = $db->RunQuery($sql);
			$i=0;
			$balMain	=0;
			$balColor	=0;
            while($row=mysqli_fetch_array($result))
            {
                $i++;
                 $doc_no		= $row['intDocumentNo'].'/'.$row['intDocumntYear'];
				 if($row['intDocumentNo'] == '')
				 $doc_no		= '';
                 $transaction_type	= $row['strType'];
                 $main_stores_qty	= $row['dblQty'];
				
				 if($transaction_type == 'STORESTRANSFER'){
                 $color_stores_qty	= $row['dblQty']*-1;
                 $main_stores_qty	= '';
				 $balColor			+=$color_stores_qty;
				 }
				 else if($transaction_type == 'INK_ALLOC'){
                 $color_stores_qty	= $row['dblQty'];
                 $main_stores_qty	= '';
				 $balColor			+=$color_stores_qty;
				 }
				 else{
                 $color_stores_qty	= '';
				 $balMain			+=$main_stores_qty;
				 }
				 
			    
				 $arr_by			= get_create_by_data($row['intDocumentNo'],$row['intDocumntYear'],$transaction_type,$row['intUser'],$row['dtDate'],$item);
				 $doc_by			= $arr_by['doc_user'];
                 $doc_date			= $arr_by['doc_create_date'];
				 if($doc_by == '')
				 {
					 $doc_by		= $approved_by;
					 $doc_date		= $approved_date;
				 }
                 $par_doc_type		= $arr_by['par_type'];
			     $par_doc_no		= $arr_by['par_doc_no'].'/'.$arr_by['par_doc_year'];
				 $par_doc_qty		= $arr_by['qty'];
                 if($arr_by['par_doc_no'] == ''){
					$par_doc_no		= ''; 
				 }
				 $par_doc_date		= $arr_by['par_doc_create_date'];
                 $par_doc_by		= $arr_by['par_doc_user'];
				 
                 $approved_by		= $row['strUserName'];
                 $approved_date		= $row['dtDate'];
				
				 if($transaction_type == 'ISSUE'){
					 
					 $resIss	=	get_mrn_records($doc_no,$item,'limit 1','');
					 $row_issue=mysqli_fetch_array($resIss);
					 //print_r($row_issue);
					 $par_doc_type		= 'MRN';
					 $par_doc_no		= $row_issue['intMrnNo'].'/'.$row_issue['intMrnYear'];
					 $par_doc_date		= $row_issue['dtmCreateDate'];
					 $par_doc_by		= $row_issue['strUserName'];
				 	$par_doc_qty		= $row_issue['qty'];
				 }
		
      ?>
            <tr class="normalfnt"  <?php if($i%2 == 0){  ?> bgcolor="#FFD7D7" <?php } else {?> bgcolor="#FFFFFF" <?php } ?> >
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $doc_no; ?></td>
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $transaction_type; ?></td>
              <td class="normalfntRight" nowrap="nowrap" ><?php echo $main_stores_qty; ?></td>
              <td class="normalfntRight" nowrap="nowrap" ><?php echo $color_stores_qty; ?></td>
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $doc_by ?></td>
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $doc_date ?></td>
              <td class="normalfntMid" nowrap="nowrap" ><table width="100%" id="tblMainGrid" cellspacing="0" cellpadding="0" border="0">
                <?php
			 $SQL_A		=get_approved_by($doc_no, $transaction_type);
			 $resultA 	= $db->RunQuery($SQL_A);
			 while($rowA=mysqli_fetch_array($resultA)){
			 ?>
                <tr>
                  <td><?php echo $rowA['desc_']  ; ?></td>
                  <td><?php echo $rowA['by_']  ; ?></td>
                  </tr>
                <?php
			 }

			 ?>
                </table>
              </td>
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $par_doc_no ?></td>
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $par_doc_type ?></td>
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $par_doc_qty ?></td>
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $par_doc_by ?></td>
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $par_doc_date?></td>
              <td class="normalfntMid" nowrap="nowrap" ><table width="100%" id="tblMainGrid" cellspacing="0" cellpadding="0" border="0">
             <?php
			 $SQL_A		=get_approved_by($par_doc_no, $par_doc_type);
			 $resultA 	= $db->RunQuery($SQL_A);
			 while($rowA=mysqli_fetch_array($resultA)){
			 ?>
              <tr>
                <td><?php echo $rowA['desc_']  ; ?></td>
                <td><?php echo $rowA['by_']  ; ?></td>
              </tr>
             <?php
			 }
			 ?>
            </table>
            </td>
              </tr>
      
      <?php	 
				 if($transaction_type == 'ISSUE'){
					// echo "okkkkkk";
					 $resIss2	=	get_mrn_records($doc_no,$item,'',$par_doc_no);
					 
					 while($row_issue2=mysqli_fetch_array($resIss2)){
						 $par_doc_type		= 'MRN';
						 $par_doc_no		= $row_issue2['intMrnNo'].'/'.$row_issue2['intMrnYear'];
						 $par_doc_date		= $row_issue2['dtmCreateDate'];
						 $par_doc_by		= $row_issue2['strUserName'];
						 $par_doc_qty		= $row_issue['qty'];
						 ?>
                        <tr class="normalfnt"  <?php if($i%2 == 0){  ?> bgcolor="#FFD7D7" <?php } else {?> bgcolor="#FFFFFF" <?php } ?> >
                          <td class="normalfntMid" nowrap="nowrap" ><?php //echo $doc_no; ?></td>
                          <td class="normalfntMid" nowrap="nowrap" ><?php //echo $transaction_type; ?></td>
                          <td class="normalfntMid" nowrap="nowrap" ><?php //echo $main_stores_qty; ?></td>
                          <td class="normalfntMid" nowrap="nowrap" ><?php //echo $color_stores_qty; ?></td>
                          <td class="normalfntMid" nowrap="nowrap" ><?php //echo $doc_by ?></td>
                          <td class="normalfntMid" nowrap="nowrap" ><?php //echo $doc_date ?></td>
                          <td class="normalfntMid" nowrap="nowrap" > 
                          </td>
                          <td class="normalfntMid" nowrap="nowrap" ><?php echo $par_doc_no ?></td>
                          <td class="normalfntMid" nowrap="nowrap" ><?php echo $par_doc_type ?></td>
             			 <td class="normalfntMid" nowrap="nowrap" ><?php echo $par_doc_qty ?></td>
                          <td class="normalfntMid" nowrap="nowrap" ><?php echo $par_doc_by ?></td>
                          <td class="normalfntMid" nowrap="nowrap" ><?php echo $par_doc_date?></td>
                          <td class="normalfntMid" nowrap="nowrap" ><table width="100%" id="tblMainGrid" cellspacing="0" cellpadding="0" border="0">
                         <?php
                         $SQL_A		=get_approved_by($par_doc_no, $par_doc_type);
                         $resultA 	= $db->RunQuery($SQL_A);
                         while($rowA=mysqli_fetch_array($resultA)){
                         ?>
                          <tr>
                            <td><?php echo $rowA['desc_']  ; ?></td>
                            <td><?php echo $rowA['by_']  ; ?></td>
                          </tr>
                         <?php
                         }
                         ?>
                        </table>
                        </td>
                          </tr>
                         <?php
					 }
				 }
      ?>
            <?php 
            }
      ?>
    <tr bgcolor="#CCCCCC">
    <td></td>
    <td></td>
    <td align="right" class="normalfntRight"><?php echo $balMain; ?></td>
    <td align="right" class="normalfntRight"><?php echo $balColor; ?></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    </tr>
            </table>
        </td>
      </tr>
    <?php
	}
	?>
      </table>
    </td>
</tr>

<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php
function get_create_by_data($doc_no, $doc_year, $trns_type,$by,$date,$item){
	global $db;
	
	if($trns_type=='STORESTRANSFER'){
		//SRN DATA
		$SQL	= "SELECT
					'STORES REQUESITION' AS par_type,
					ware_storestransferheader.intRequisitionNo AS par_doc_no,
					ware_storestransferheader.intRequisitionYear AS par_doc_year,
					ware_storestransferheader.dtmCreateDate AS doc_create_date,
					U1.strUserName AS doc_user,
					ware_storesrequesitionheader.dtDate AS par_doc_create_date,
					U2.strUserName AS par_doc_user,
					ware_storesrequesitiondetails.dblQty,
					ware_storesrequesitiondetails.dblExQty,
					sum(ware_storesrequesitiondetails.dblQty+ware_storesrequesitiondetails.dblExQty) as qty 
					FROM
					ware_storestransferheader
					INNER JOIN sys_users AS U1 ON ware_storestransferheader.intUser = U1.intUserId
					INNER JOIN ware_storesrequesitionheader ON ware_storestransferheader.intRequisitionNo = ware_storesrequesitionheader.intRequisitionNo AND ware_storestransferheader.intRequisitionYear = ware_storesrequesitionheader.intRequisitionYear
					INNER JOIN sys_users AS U2 ON ware_storesrequesitionheader.intUser = U2.intUserId
					INNER JOIN ware_storesrequesitiondetails ON ware_storesrequesitionheader.intRequisitionNo = ware_storesrequesitiondetails.intRequisitionNo AND ware_storesrequesitionheader.intRequisitionYear = ware_storesrequesitiondetails.intRequisitionYear
					WHERE
					ware_storestransferheader.intTransfNo = '$doc_no' AND
					ware_storestransferheader.intTransfYear = '$doc_year' AND
					ware_storesrequesitiondetails.intItemId = '$item' 
					GROUP BY 
					ware_storestransferheader.intRequisitionNo,
					ware_storestransferheader.intRequisitionYear 
					";
	
	}
	else if($trns_type=='GATEPASS'){
	
		$SQL	= "SELECT 
					'' AS par_type, 
					'' as par_doc_no,
					'' as par_doc_year,
					ware_gatepassheader.dtmCreateDate as doc_create_date,
					U1.strUserName AS doc_user,
					'' as par_doc_create_date,
					'' as par_doc_user 
					FROM
					ware_gatepassheader
					INNER JOIN sys_users as U1 ON ware_gatepassheader.intUser = U1.intUserId
					WHERE
					ware_gatepassheader.intGatePassNo = '$doc_no' AND
					ware_gatepassheader.intGatePassYear = '$doc_year'
					";
	
		
	}
	else if($trns_type=='ISSUE'){
		
		$SQL	= "SELECT 
					'MRN' AS par_type, 
					ware_issuedetails.intMrnNo as par_doc_no,
					ware_issuedetails.intMrnYear as par_doc_year,
					ware_issueheader.dtmCreateDate as doc_create_date,
					U1.strUserName AS doc_user,
					ware_mrnheader.dtDate as par_doc_create_date,
					U2.strUserName as par_doc_user 
					FROM
					ware_issueheader
					INNER JOIN ware_issuedetails ON ware_issueheader.intIssueNo = ware_issuedetails.intIssueNo AND ware_issueheader.intIssueYear = ware_issuedetails.intIssueYear
					INNER JOIN ware_mrnheader ON ware_issuedetails.intMrnNo = ware_mrnheader.intMrnNo AND ware_issuedetails.intMrnYear = ware_mrnheader.intMrnYear
					WHERE
					ware_issuedetails.intItemId = '$item'
					INNER JOIN sys_users as U1 ON ware_issueheader.intUser = U1.intUserId
					INNER JOIN sys_users AS U2 ON ware_mrnheader.intUser = U2.intUserId
					WHERE
					ware_issueheader.intIssueNo = '$doc_no' AND
					ware_issueheader.intIssueYear = '$doc_year'
					";
		
	}
	else if($trns_type=='RETSTORES'){
		//MRN DATA
		$SQL	= "SELECT
					'ISSUE' AS par_type,
					ware_returntostoresheader.intIssueNo AS par_doc_no,
					ware_returntostoresheader.intIssueYear AS par_doc_year,
					ware_returntostoresheader.dtmCreateDate AS doc_create_date,
					U1.strUserName AS doc_user,
					ware_issueheader.dtmCreateDate AS par_doc_create_date,
					U2.strUserName AS par_doc_user,
					ware_issuedetails.dblQty as qty 
					FROM
					ware_returntostoresheader
					INNER JOIN ware_issueheader ON ware_returntostoresheader.intIssueNo = ware_issueheader.intIssueNo AND ware_returntostoresheader.intIssueYear = ware_issueheader.intIssueYear
					INNER JOIN sys_users AS U1 ON ware_returntostoresheader.intUser = U1.intUserId
					INNER JOIN sys_users AS U2 ON ware_issueheader.intUser = U2.intUserId  
					INNER JOIN ware_issuedetails ON ware_issueheader.intIssueNo = ware_issuedetails.intIssueNo AND ware_issueheader.intIssueYear = ware_issuedetails.intIssueYear
					WHERE
					ware_returntostoresheader.intReturnNo = '$doc_no' AND
					ware_returntostoresheader.intReturnYear = '$doc_year' AND
					ware_issuedetails.intItemId = '$item'
					GROUP BY
					ware_returntostoresheader.intIssueNo,
					ware_returntostoresheader.intIssueYear
					";
	}
	else if($trns_type=='TRANSFERIN'){
		//GP DATA
		  $SQL	= "SELECT 
					'GATEPASS' AS par_type, 
					ware_gatepasstransferinheader.intGatePassNo as par_doc_no,
					ware_gatepasstransferinheader.intGatePassYear as par_doc_year,
					ware_gatepasstransferinheader.dtmCreateDate as doc_create_date,
					U1.strUserName AS doc_user,
					ware_gatepassheader.dtmCreateDate as par_doc_create_date,
					U2.strUserName as par_doc_user,
					ware_gatepassdetails.dblQty AS qty 
					FROM
					ware_gatepasstransferinheader
					INNER JOIN ware_gatepassheader ON ware_gatepasstransferinheader.intGatePassNo = ware_gatepassheader.intGatePassNo AND ware_gatepasstransferinheader.intGatePassYear = ware_gatepassheader.intGatePassYear
					INNER JOIN sys_users AS U1 ON ware_gatepassheader.intUser = U1.intUserId
					INNER JOIN sys_users AS U2 ON ware_gatepasstransferinheader.intUser = U2.intUserId  
					INNER JOIN ware_gatepassdetails ON ware_gatepassheader.intGatePassNo = ware_gatepassdetails.intGatePassNo AND ware_gatepassheader.intGatePassYear = ware_gatepassdetails.intGatePassYear
					WHERE
					ware_gatepasstransferinheader.intGpTransfNo = '$doc_no' AND
					ware_gatepasstransferinheader.intGpTransfYear = '$doc_year' AND
					ware_gatepassdetails.intItemId = '$item'
					GROUP BY
					ware_gatepassdetails.intGatePassNo,
					ware_gatepassdetails.intGatePassYear

					";
		
	}
	
	$result = $db->RunQuery($SQL);
	$row=mysqli_fetch_array($result);
 	
	return $row;
}

function get_approved_by($doc_no, $transaction_type){
	if($transaction_type == 'STORESTRANSFER'){
		$sql	= "SELECT
					if(ware_storestransferheader_approvedby.intApproveLevelNo=1,'1st Approved By',if(ware_storestransferheader_approvedby.intApproveLevelNo=2,'2nd Approved By',if(ware_storestransferheader_approvedby.intApproveLevelNo=3,'3rd Approved By',if(ware_storestransferheader_approvedby.intApproveLevelNo>3,concat(ware_storestransferheader_approvedby.intApproveLevelNo,'th Approved By'),if(ware_storestransferheader_approvedby.intApproveLevelNo=0,'Rejected By',if(ware_storestransferheader_approvedby.intApproveLevelNo=-1,'Revised By','')))))) as desc_,
					concat('(',sys_users.strUserName,')','-',ware_storestransferheader_approvedby.dtApprovedDate) as by_
					FROM
					ware_storestransferheader_approvedby
					INNER JOIN sys_users ON ware_storestransferheader_approvedby.intApproveUser = sys_users.intUserId
					WHERE
					concat(ware_storestransferheader_approvedby.intTransfNo,'/',ware_storestransferheader_approvedby.intYear) = '$doc_no'
					ORDER BY
					ware_storestransferheader_approvedby.dtApprovedDate ASC";
	}
	if($transaction_type == 'STORES REQUESITION'){
		$sql	= "SELECT
					if(ware_storesrequesitionheader_approvedby.intApproveLevelNo=1,'1st Approved By',if(ware_storesrequesitionheader_approvedby.intApproveLevelNo=2,'2nd Approved By',if(ware_storesrequesitionheader_approvedby.intApproveLevelNo=3,'3rd Approved By',if(ware_storesrequesitionheader_approvedby.intApproveLevelNo>3,concat(ware_storesrequesitionheader_approvedby.intApproveLevelNo,'th Approved By'),if(ware_storesrequesitionheader_approvedby.intApproveLevelNo=0,'Rejected By',if(ware_storesrequesitionheader_approvedby.intApproveLevelNo=-1,'Revised By','')))))) as desc_,
					concat('(',sys_users.strUserName,')','-',ware_storesrequesitionheader_approvedby.dtApprovedDate) as by_
					FROM
					ware_storesrequesitionheader_approvedby
					INNER JOIN sys_users ON ware_storesrequesitionheader_approvedby.intApproveUser = sys_users.intUserId
					WHERE
					concat(ware_storesrequesitionheader_approvedby.intRequisitionNo,'/',ware_storesrequesitionheader_approvedby.intYear) = '$doc_no'
					ORDER BY
					ware_storesrequesitionheader_approvedby.dtApprovedDate ASC";
	}
	
 	
	if($transaction_type == 'MRN'){
		$sql	= "SELECT
					if(ware_mrnheader_approvedby.intApproveLevelNo=1,'1st Approved By',if(ware_mrnheader_approvedby.intApproveLevelNo=2,'2nd Approved By',if(ware_mrnheader_approvedby.intApproveLevelNo=3,'3rd Approved By',if(ware_mrnheader_approvedby.intApproveLevelNo>3,concat(ware_mrnheader_approvedby.intApproveLevelNo,'th Approved By'),if(ware_mrnheader_approvedby.intApproveLevelNo=0,'Rejected By',if(ware_mrnheader_approvedby.intApproveLevelNo=-1,'Revised By','')))))) as desc_,
					concat('(',sys_users.strUserName,')','-',ware_mrnheader_approvedby.dtApprovedDate) as by_
					FROM
					ware_mrnheader_approvedby
					INNER JOIN sys_users ON ware_mrnheader_approvedby.intApproveUser = sys_users.intUserId
					WHERE
					concat(ware_mrnheader_approvedby.intMrnNo,'/',ware_mrnheader_approvedby.intYear) = '$doc_no'
					ORDER BY
					ware_mrnheader_approvedby.dtApprovedDate ASC";
	}
	
 
	if($transaction_type == 'ISSUE'){
		$sql	= "SELECT
					if(ware_issueheader_approvedby.intApproveLevelNo=1,'1st Approved By',if(ware_issueheader_approvedby.intApproveLevelNo=2,'2nd Approved By',if(ware_issueheader_approvedby.intApproveLevelNo=3,'3rd Approved By',if(ware_issueheader_approvedby.intApproveLevelNo>3,concat(ware_issueheader_approvedby.intApproveLevelNo,'th Approved By'),if(ware_issueheader_approvedby.intApproveLevelNo=0,'Rejected By',if(ware_issueheader_approvedby.intApproveLevelNo=-1,'Revised By','')))))) as desc_,
					concat('(',sys_users.strUserName,')','-',ware_issueheader_approvedby.dtApprovedDate) as by_
					FROM
					ware_issueheader_approvedby
					INNER JOIN sys_users ON ware_issueheader_approvedby.intApproveUser = sys_users.intUserId
					WHERE
					concat(ware_issueheader_approvedby.intIssueNo,'/',ware_issueheader_approvedby.intYear) = '$doc_no'
					ORDER BY
					ware_issueheader_approvedby.dtApprovedDate ASC";
	}
	
 	
	if($transaction_type == 'GATEPASS'){
		$sql	= "SELECT
					if(ware_gatepassheader_approvedby.intApproveLevelNo=1,'1st Approved By',if(ware_gatepassheader_approvedby.intApproveLevelNo=2,'2nd Approved By',if(ware_gatepassheader_approvedby.intApproveLevelNo=3,'3rd Approved By',if(ware_gatepassheader_approvedby.intApproveLevelNo>3,concat(ware_gatepassheader_approvedby.intApproveLevelNo,'th Approved By'),if(ware_gatepassheader_approvedby.intApproveLevelNo=0,'Rejected By',if(ware_gatepassheader_approvedby.intApproveLevelNo=-1,'Revised By','')))))) as desc_,
					concat('(',sys_users.strUserName,')','-',ware_gatepassheader_approvedby.dtApprovedDate) as by_
					FROM
					ware_gatepassheader_approvedby
					INNER JOIN sys_users ON ware_gatepassheader_approvedby.intApproveUser = sys_users.intUserId
					WHERE
					concat(ware_gatepassheader_approvedby.intGatePassNo,'/',ware_gatepassheader_approvedby.intYear) = '$doc_no'
					ORDER BY
					ware_gatepassheader_approvedby.dtApprovedDate ASC";
	}
	
 	
	if($transaction_type == 'TRANSFERIN'){
		$sql	= "SELECT
					if(ware_gatepasstransferinheader_approvedby.intApproveLevelNo=1,'1st Approved By',if(ware_gatepasstransferinheader_approvedby.intApproveLevelNo=2,'2nd Approved By',if(ware_gatepasstransferinheader_approvedby.intApproveLevelNo=3,'3rd Approved By',if(ware_gatepasstransferinheader_approvedby.intApproveLevelNo>3,concat(ware_gatepasstransferinheader_approvedby.intApproveLevelNo,'th Approved By'),if(ware_gatepasstransferinheader_approvedby.intApproveLevelNo=0,'Rejected By',if(ware_gatepasstransferinheader_approvedby.intApproveLevelNo=-1,'Revised By','')))))) as desc_,
					concat('(',sys_users.strUserName,')','-',ware_gatepasstransferinheader_approvedby.dtApprovedDate) as by_
					FROM
					ware_gatepasstransferinheader_approvedby
					INNER JOIN sys_users ON ware_gatepasstransferinheader_approvedby.intApproveUser = sys_users.intUserId
					WHERE
					concat(ware_gatepasstransferinheader_approvedby.intGpTransfNo,'/',ware_gatepasstransferinheader_approvedby.intYear) = '$doc_no'
					ORDER BY
					ware_gatepasstransferinheader_approvedby.dtApprovedDate ASC";
	}
	
	if($transaction_type == 'RETSTORES'){
		  $sql	= "SELECT
					if(ware_returntostoresheader_approvedby.intApproveLevelNo=1,'1st Approved By',if(ware_returntostoresheader_approvedby.intApproveLevelNo=2,'2nd Approved By',if(ware_returntostoresheader_approvedby.intApproveLevelNo=3,'3rd Approved By',if(ware_returntostoresheader_approvedby.intApproveLevelNo>3,concat(ware_returntostoresheader_approvedby.intApproveLevelNo,'th Approved By'),if(ware_returntostoresheader_approvedby.intApproveLevelNo=0,'Rejected By',if(ware_returntostoresheader_approvedby.intApproveLevelNo=-1,'Revised By','')))))) as desc_,
					concat('(',sys_users.strUserName,')','-',ware_returntostoresheader_approvedby.dtApprovedDate) as by_
					FROM
					ware_returntostoresheader_approvedby
					INNER JOIN sys_users ON ware_returntostoresheader_approvedby.intApproveUser = sys_users.intUserId
					WHERE
					concat(ware_returntostoresheader_approvedby.intReturnNo,'/',ware_returntostoresheader_approvedby.intYear) = '$doc_no'
					ORDER BY
					ware_returntostoresheader_approvedby.dtApprovedDate ASC";
	}
	
	return $sql;
	

}

function get_mrn_records($doc_no,$item,$limit,$mrnNo){
	global $db;
	$SQL	= "SELECT
				ware_issuedetails.intMrnNo,
				ware_issuedetails.intMrnYear,
				ware_mrnheader.dtmCreateDate,
				ware_mrnheader.intUser,
				sys_users.strUserName,
				sum(ware_mrndetails.dblQty) as qty 
				FROM
				ware_issuedetails
				INNER JOIN ware_mrnheader ON ware_issuedetails.intMrnNo = ware_mrnheader.intMrnNo AND ware_issuedetails.intMrnYear = ware_mrnheader.intMrnYear
				INNER JOIN sys_users ON ware_mrnheader.intUser = sys_users.intUserId
				INNER JOIN ware_mrndetails ON ware_mrnheader.intMrnNo = ware_mrndetails.intMrnNo AND ware_mrnheader.intMrnYear = ware_mrndetails.intMrnYear AND ware_issuedetails.strOrderNo = ware_mrndetails.intOrderNo AND ware_issuedetails.intOrderYear = ware_mrndetails.intOrderYear AND ware_issuedetails.strStyleNo = ware_mrndetails.strStyleNo AND ware_issuedetails.intItemId = ware_mrndetails.intItemId
				WHERE
				ware_issuedetails.intItemId = '$item'  AND
				concat(ware_issuedetails.intIssueNo,'/',ware_issuedetails.intIssueYear) = '$doc_no'  AND 
				concat(ware_issuedetails.intMrnNo,'/',ware_issuedetails.intMrnYear) <> '$mrnNo'
				GROUP BY  
				ware_issuedetails.intMrnNo,
				ware_issuedetails.intMrnYear
				ORDER BY 
				ware_issuedetails.intMrnNo ASC,
				ware_issuedetails.intMrnYear ASC
				" ;
	if($limit!=''){
		$SQL .= $limit ;
	}
		//echo $SQL;		
				
	$result = $db->RunQuery($SQL);
	return $result;
	
}

?>
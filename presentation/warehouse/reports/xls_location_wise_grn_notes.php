<?php

//include_once "../../../dataAccess/Connector.php";

//$result2 = $db->RunQuery($report_sql);
//
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=report.xls" );
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="#99CCFF">Company</td>
    <td bgcolor="#99CCFF">Location</td>
    <td bgcolor="#99CCFF">Supplier</td>
    <td bgcolor="#99CCFF">GRN No</td>
    <td bgcolor="#99CCFF">GRN Date</td>
    <td bgcolor="#99CCFF">PO No</td>
    <td bgcolor="#99CCFF">Main Category</td>
    <td bgcolor="#99CCFF">Sub Category</td>
    <td bgcolor="#99CCFF">Item</td>
    <td bgcolor="#99CCFF">UOM</td>
    <td bgcolor="#99CCFF">Currency</td>
    <td bgcolor="#99CCFF">Price</td>
    <td bgcolor="#99CCFF">Qty</td>
    <td bgcolor="#99CCFF">Amount</td>
    <td bgcolor="#99CCFF">Invoice No</td>
    <td bgcolor="#99CCFF">Invoice Date</td>
  </tr>
<?php
$totQty=0;
$totAmount=0;

while($row = mysqli_fetch_array($result))
{
?>
  <tr>
    <td><?php echo $row['Company']; ?></td>
    <td><?php echo $row['Location']; ?></td>
    <td><?php echo $row['supplier']; ?></td>
    <td><?php echo $row['GRN No']; ?></td>
    <td><?php echo $row['GRN_DATE']; ?></td>
    <td><?php echo $row['PO No']; ?></td>
    <td><?php echo $row['Main Category']; ?></td>
    <td><?php echo $row['Sub Category']; ?></td>
    <td><?php echo $row['Item']; ?></td>
    <td><?php echo $row['UOM']; ?></td>
    <td><?php echo $row['Currency']; ?></td>
    <td><?php echo $row['Price']; ?></td>
    <td><?php echo $row['Qty']; ?></td>
    <td><?php echo $row['Amount']; ?></td>
    <td><?php echo $row['InvoiceNo']; ?></td>
    <td><?php echo $row['SUPPLIER_INVOICE_DATE']; ?></td>
  </tr>
<?php
$totQty		+=$row['Qty'];
$totAmount	+=$row['Amount'];
}
?>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><?php echo $totQty; ?></td>
    <td><?php echo $totAmount; ?></td>
  </tr>
</table>

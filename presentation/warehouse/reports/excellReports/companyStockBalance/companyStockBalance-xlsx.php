<?php
//echo "ddd";
//die();
////////////////////////////////
/////  H. B. G. KORALA  ////////
////     16-10-2012  ///////////
///////////////////////////////
ini_set('display_errors',0);
ini_set('memory_limit', '-1');
session_start();
ini_set('max_execution_time',6000000);
//$backwardseperator = "../../../../../";

$company 	= $_REQUEST['company'];
$location 	= $_REQUEST['location'];
$date	= $_REQUEST['date'];
$currencyId	= $_REQUEST['currency'];

include "../../../../../dataAccess/Connector.php";


/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2012 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2012 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.7, 2012-05-19
 */

/** Error reporting */
//error_reporting(E_ALL);

date_default_timezone_set('Europe/London');

/** Include PHPExcel */
require_once "../../../../../libraries/excel/Classes/PHPExcel.php";
//include  	"{$backwardseperator}dataAccess/Connector.php";






//-----------------------------
	 $SQL = "SELECT
			mst_companies.intId as compId,
			mst_companies.strName as company,
			mst_locations.intId as locId,
			mst_locations.strName as location 
			FROM
			mst_locations
			Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
			WHERE 
			1=1 ";
			if($company!=''){
			$SQL .= " AND mst_companies.intId =  '$company'";
			}
			if($location!=''){
			$SQL .= " AND mst_locations.intId =  '$location'";
			}
	
	$result = $db->RunQuery($SQL);
	$i=0;
	while($row=mysqli_fetch_array($result))
	{
		$arrCompanyId[$i]	= $row["compId"];
		$arrCompanyName[$i]	= $row["company"];
		$arrLocationId[$i]	= $row["locId"];
		$arrLocationName[$i] = $row["location"];
		
		$i++;
	}
	$lengthLocations=$i;
//-----------------------------
	 $SQL = "SELECT
			mst_companies.strName
			FROM mst_companies
			WHERE
			mst_companies.intId =  '$company';";
	
	$result = $db->RunQuery($SQL);
	$row = mysqli_fetch_array($result);
	$companyName		= $row["strName"];

//-----------------------------
	 $SQL = "SELECT
			mst_locations.strName
			FROM mst_locations
			WHERE
			mst_locations.intId =  '$location';";
	
	$result = $db->RunQuery($SQL);
	$row = mysqli_fetch_array($result);
	$locationName		= $row["strName"];
//-----------------------------
		$sql = "SELECT
					mst_financeexchangerate.dblBuying, 
					mst_financecurrency.strCode as currency 
				FROM
					mst_financeexchangerate 
					Inner Join mst_financecurrency ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId
				WHERE
					mst_financeexchangerate.intCurrencyId =  '$currencyId' AND
					mst_financeexchangerate.intCompanyId =  '$company'
				ORDER BY
					mst_financeexchangerate.dtmDate DESC
				LIMIT 1
				";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		
		$currency = $row['currency'];
		$toCurrencyRate = $row['dblBuying'];
		
//------------------------------------------------------
		

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

$alphas = range('A', 'Z');

$style_header = array(                  
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb'=>'FFDDDD'),
                ),
                'font' => array(
                    'bold' => true,
                )
                );


// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

//$objPHPExcel->getDefaultStyle()->getFont()->setSize(15);
// Add some data
	if($location!='')
	{
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue("$alphas[0]1", 'Company Wise General Stock Balance') 
				->setCellValue("$alphas[0]2", 'Company'.' - '.$companyName) 
				->setCellValue("$alphas[0]3", 'Location'.' - '.$locationName) 
				->setCellValue("$alphas[0]4", 'To Date'.' - '.$date) 
				->setCellValue("$alphas[3]4", 'Currency'.' - '.$currency);
				$i=4;
	}
	else {
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue("$alphas[0]1", 'Company Wise General Stock Balance') 
				->setCellValue("$alphas[0]2", 'Company'.' - '.$companyName) 
				->setCellValue("$alphas[0]3", 'To Date'.' - '.$date) 
				->setCellValue("$alphas[3]3", 'Currency'.' - '.$currency);
				$i=3;
	}
			
			
			
$objPHPExcel->getActiveSheet()->getStyle("$alphas[3]$i")->getFont()->setSize(13);
$objPHPExcel->getActiveSheet()->getStyle("$alphas[3]$i")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("$alphas[3]$i")->getFont()->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
			
$objPHPExcel->getActiveSheet()->getStyle("$alphas[0]1:$alphas[0]$i")->getFont()->setSize(13);
$objPHPExcel->getActiveSheet()->getStyle("$alphas[0]1:$alphas[0]$i")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("$alphas[0]1:$alphas[0]$i")->getFont()->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);

//$objPHPExcel->getActiveSheet()->getStyle("$alphas[0]1:$alphas[0]$i")->getFont()->setColor(COLOR_GREEN,true);

  
  $i+=2;
  $j=9;
	
	
	//Company name for header
	for($j=9;$j<$lengthLocations+9; $j++){
		if($arrCompanyId[$j-9]!=$arrCompanyId[$j-9]){
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue($alphas[$j].$i, $arrCompanyName[$j-9]);  
		}
					
		$objPHPExcel->getActiveSheet()->getStyle("$alphas[9]$i:$alphas[$j]$i")->getFont()->setBold(true);
	}
	
	$i++;
	//location name for header
	for($j=9;$j<$lengthLocations+9; $j++){
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue($alphas[$j].$i, $arrLocationName[$j-9]);  
					
		//$objPHPExcel->getActiveSheet()->getStyle("$alphas[8]$i:$alphas[$j]$i")->getFont()->setBold(true);
	}
	$l=$j;

$j=0;
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($alphas[$j++].$i, 'No')
            ->setCellValue($alphas[$j++].$i, 'Main Category')
            ->setCellValue($alphas[$j++].$i, 'Sub Category')
            ->setCellValue($alphas[$j++].$i, 'Item Code')
            ->setCellValue($alphas[$j++].$i, 'Item Name') 
            ->setCellValue($alphas[$j++].$i, 'UOM')  
            ->setCellValue($alphas[$j++].$i, 'Currency')  
            ->setCellValue($alphas[$j++].$i, 'Price') 
            ->setCellValue($alphas[$j++].$i, 'Re Order Level') 
            ->setCellValue($alphas[$l++].$i, 'Ammount');
			
$objPHPExcel->getActiveSheet()->getStyle("$alphas[0]$i:$alphas[$l]$i")->getFont()->setBold(true);

$p=$i;

//$objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
		
/*		if($toCurrencyRate<=0 || $toCurrencyRate<='')
			$toCurrencyRate=1;
		
		if($currencyId=='')
			$para1 = ' tb2.strCode';
		else	
			$para1 = $currency;
			
		if($currencyId==''){
			$para2 = 1;
			$toCurrencyRate=1;
		}
		else	
			$para2 = 'ware_grnheader.dblExchangeRate';

$sql ="SELECT  
		@rowid:=@rowid+1 as No, 
		tb2.`Main_Category`,
		tb2.`Sub_Category`,
		tb2.`Item_Code`,
		tb2.`Item_Name`,
		tb2.`Location`,
		tb2.`Stock_Balance`, 
		tb2.`Price`, 
		tb2.UOM , ";
				
		if($currencyId=='')
		$sql .="$para1 as Currency,";
		else
		$sql .="'$para1' as Currency,";
		
		$sql .= " tb2.`Ammount`    
		FROM (
		SELECT 
		tb1.Item_Name,
		tb1.Location  ,
		mst_maincategory.strName AS `Main_Category`, 
		mst_subcategory.strName AS `Sub_Category`, 
		tb1.strCode AS `Item_Code`,
 		IFNULL(round(Sum(ware_stocktransactions_bulk.dblQty),4),0) AS `Stock_Balance`, 
		mst_units.strName as UOM ,
 		mst_financecurrency.strCode, 
 		((round(  sum( ($para2 *(ware_stocktransactions_bulk.dblQty) * ware_stocktransactions_bulk.dblGRNRate)/$toCurrencyRate),4   ))/(round(Sum(ware_stocktransactions_bulk.dblQty),4))) as Price  , 
		mst_financecurrency.strCode as Currency, 
		round(  sum( ($para2 *(ware_stocktransactions_bulk.dblQty) * ware_stocktransactions_bulk.dblGRNRate)/$toCurrencyRate),4   ) AS `Ammount`    
		FROM 
		(SELECT 
		mst_item.intId, 
		 mst_item.strCode, 
		 mst_item.dblLastPrice, 
		mst_locations.intId as intLocId, 
		mst_locations.intCompanyId, 
		mst_item.strName as Item_Name,
		mst_locations.strName as  Location, 
		mst_item.intMainCategory, 
		mst_item.intSubCategory, 
		mst_item.intCurrency, 
		mst_item.intUOM 
		FROM 
		mst_item ,
		mst_locations) as tb1 
		left Join mst_maincategory ON tb1.intMainCategory = mst_maincategory.intId 
		left Join mst_subcategory ON tb1.intSubCategory = mst_subcategory.intId 
		left Join mst_units ON tb1.intUOM = mst_units.intId 
		left Join mst_financecurrency ON tb1.intCurrency = mst_financecurrency.intId  
		left Join ware_stocktransactions_bulk ON ware_stocktransactions_bulk.intItemId = tb1.intId AND ware_stocktransactions_bulk.intLocationId =tb1.intLocId ";
		if($date!='')
		$sql .=" AND date(ware_stocktransactions_bulk.dtDate) <=  '$date' "; 
		  
		  
		$sql .=" left Join ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear   
		left Join mst_companies ON ware_stocktransactions_bulk.intCompanyId = mst_companies.intId  
		
		WHERE  tb1.intCompanyId='$company'";
		if($location!='')
		$sql .=" AND tb1.intLocId =  '$location' "; 
		
		
		$sql .="GROUP BY tb1.intCompanyId,tb1.intLocId, tb1.intId  
		 ORDER BY Item_Name ASC,tb1.intCompanyId,Location ASC limit 1200
		) AS tb2 JOIN (SELECT @rowid:=0) as init 
		";
	  // 	 echo $sql;
*/	  
	  
	  
/////////////////////////////////////	  


$tempCompany='';
$tempLocation='';
for($k=0;$k<$lengthLocations;$k++){	
	
$company=$arrCompanyId[$k];
$location=$arrLocationId[$k];
//-----------------------------
$sql = "SELECT
			mst_financeexchangerate.dblBuying, 
			mst_financecurrency.strCode as currency 
		FROM
			mst_financeexchangerate 
			Inner Join mst_financecurrency ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId
		WHERE
			mst_financeexchangerate.intCurrencyId =  '$currencyId' AND
			mst_financeexchangerate.intCompanyId =  '$company'
		ORDER BY
			mst_financeexchangerate.dtmDate DESC
		LIMIT 1
		";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
//-----------------------------------------
$currency = $row['currency'];
$toCurrencyRate = $row['dblBuying'];

	if($toCurrencyRate<=0 || $toCurrencyRate<='')
		$toCurrencyRate=1;
	
	if($currencyId=='')
		$para1 = ' mst_financecurrency.strCode';
	else	
		$para1 = $currency;
		
	if($currencyId==''){
		$para2 = 1;
		$toCurrencyRate=1;
	}
	else	
		$para2 = 'ware_grnheader.dblExchangeRate';
//-----------------------------
  
	if($date!=''){
	$datePara =" AND date(ware_stocktransactions_bulk.dtDate) <=  '$date' " ;
	}
	  
	
$sql=" 
   
SELECT @rowid:=@rowid+1 as No, tb2.`Main_Category`, tb2.`Sub_Category`, tb2.`Item_Code`, tb2.`Item_Name`, tb2.`Location`, tb2.`Stock_Balance`, tb2.`Re_OrderQty`, tb2.`Price`, tb2.UOM , tb2.Currency, tb2.`Ammount` 
FROM (
SELECT 
tb1.Item_Name,
tb1.Location  , 
tb1.dblReorderQty as Re_OrderQty, 
mst_maincategory.strName AS `Main_Category`, mst_subcategory.strName AS `Sub_Category`, tb1.strCode AS `Item_Code`,
 IFNULL(round(Sum(ware_stocktransactions_bulk.dblQty),4),0) AS `Stock_Balance`, 
mst_units.strName as UOM , ";

if($currencyId=='')
$sql .="$para1 as Currency,";
else
$sql .="'$para1' as Currency,";
	
$sql .="((round(  sum( ($para2 *(ware_stocktransactions_bulk.dblQty) * ware_stocktransactions_bulk.dblGRNRate)/$toCurrencyRate),4   ))/(round(Sum(ware_stocktransactions_bulk.dblQty),4))) as Price  , 
round(  sum( ($para2 *(ware_stocktransactions_bulk.dblQty) * ware_stocktransactions_bulk.dblGRNRate)/$toCurrencyRate),4   ) AS `Ammount`  

FROM 
(SELECT 
mst_item.intId, 
mst_item.strCode, 
mst_item.dblLastPrice, 
mst_item.dblReorderQty, 
mst_locations.intId as intLocId, 
mst_locations.intCompanyId, 
mst_item.strName as Item_Name,
mst_locations.strName as  Location, 
mst_item.intMainCategory, 
mst_item.intSubCategory, 
mst_item.intCurrency, 
mst_item.intUOM 
FROM 
mst_item ,
mst_locations) as tb1 
left Join mst_maincategory ON tb1.intMainCategory = mst_maincategory.intId 
left Join mst_subcategory ON tb1.intSubCategory = mst_subcategory.intId 
left Join mst_units ON tb1.intUOM = mst_units.intId 
left Join mst_financecurrency ON tb1.intCurrency = mst_financecurrency.intId  
left Join ware_stocktransactions_bulk ON ware_stocktransactions_bulk.intItemId = tb1.intId AND ware_stocktransactions_bulk.intLocationId =tb1.intLocId   $datePara   
left Join ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear 
left Join mst_companies ON ware_stocktransactions_bulk.intCompanyId = mst_companies.intId  
WHERE  1=1";

if($company!='')
$sql .=" AND tb1.intCompanyId='$company'";
if($location!='')
$sql .=" AND tb1.intLocId =  '$location' "; 

		
		$sql .="
GROUP BY tb1.intLocId, tb1.intId  
 ORDER BY Main_Category ASC,Sub_Category ASC, Item_Name ASC, Location ASC
) AS tb2 JOIN (SELECT @rowid:=0) as init";	  
	  
// echo $sql;	  
	  
////////////////////////	  
		
$result = $db->RunQuery($sql);
$tempMain_Category='';
$tempSub_Category='';
$r=0;
$i=$p;
$arrAmmount = array();
while($row=mysqli_fetch_array($result))
{
	$i++; 
	$r++;

  if($k==0){
	if(($row['Main_Category']!=$tempMain_Category) || ($row['Sub_Category']!=$tempSub_Category)){
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue("B$i", 'Main Category : '.$row['Main_Category'])
					->setCellValue("D$i", 'Sub Category : '.$row['Sub_Category']);
					
		$objPHPExcel->getActiveSheet()->getStyle("A$i:J$i")->getFont()->setSize(10);
		$objPHPExcel->getActiveSheet()->getStyle("A$i:J$i")->getFont()->setBold(true);
					
		$i++; 
	}
	
	// Miscellaneous glyphs, UTF-8
  $j=0;
  $l=9;
  
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue($alphas[$j++].$i, $r)
				->setCellValue($alphas[$j++].$i, $row['Main_Category'])
				->setCellValue($alphas[$j++].$i, $row['Sub_Category']) 
				->setCellValue($alphas[$j++].$i, $row['Item_Code']) 
				->setCellValue($alphas[$j++].$i, $row['Item_Name']) 
				->setCellValue($alphas[$j++].$i, $row['UOM'])
				->setCellValue($alphas[$j++].$i, $row['Currency'])  
				->setCellValue($alphas[$j++].$i, $row['Price'])
				->setCellValue($alphas[$j++].$i, $row['Re_OrderQty'])
				->setCellValue($alphas[$j++].$i, $row['Stock_Balance']) ;
			//	->setCellValue($alphas[$j++].$i, $row['Ammount']); 
			
		$arrAmmount[$i] += ($row['Ammount']==''?0:$row['Ammount']);
				
		if($row['Re_OrderQty']>=$row['Stock_Balance']){
		   $objPHPExcel->getActiveSheet()->getStyle($alphas[$j-1].$i)->applyFromArray( $style_header );
		}
			
}
else{
//$l=$l+2;
	if(($row['Main_Category']!=$tempMain_Category) || ($row['Sub_Category']!=$tempSub_Category)){
		$i++; 
	}
	$l=9+$k;	
 	$objPHPExcel->setActiveSheetIndex(0)
 				->setCellValue($alphas[$l].$i, $row['Stock_Balance']) ;
//				->setCellValue($alphas[$j++].$i, $row['Ammount']); 
		$arrAmmount[$i] +=$row['Ammount'];
		
		if($row['Re_OrderQty']>=$row['Stock_Balance']){
		   $objPHPExcel->getActiveSheet()->getStyle($alphas[$l].$i)->applyFromArray( $style_header );
		}
}


if($k==$lengthLocations-1){
	$l=$l+1;
 	$objPHPExcel->setActiveSheetIndex(0)
 				->setCellValue($alphas[$l].$i, $arrAmmount[$i]) ;
}

	$tempMain_Category=$row['Main_Category'];
	$tempSub_Category=$row['Sub_Category'];
}
//$objPHPExcel->getActiveSheet()->getStyle("E4:E$i")->getFont()->setBold(true);

$tempCompany=$company;
$tempLocation=$location;
}


$j=0;
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(9);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(12);
for($k=0;$k<$lengthLocations;$k++){	
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(24);
}
$objPHPExcel->getActiveSheet()->getColumnDimension($alphas[$j++])->setWidth(12);

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Company Wise Stock Balance');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client's web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Stock Balance Report.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

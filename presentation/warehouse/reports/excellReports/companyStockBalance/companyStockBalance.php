<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];
$intUser  = $_SESSION["userId"];

include  	"{$backwardseperator}dataAccess/permisionCheck.inc";
//include  	"{$backwardseperator}dataAccess/Connector.php";


	  $sql = "SELECT
ware_mrnheader.datdate,
ware_mrnheader.intStatus,
ware_mrnheader.intApproveLevels,
ware_mrnheader.intUser,
sys_users.strUserName, 
ware_mrnheader.intDepartment as department 
FROM
ware_mrnheader 
Inner Join mst_locations ON ware_mrnheader.intCompanyId = mst_locations.intId
Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
Inner Join sys_users ON ware_mrnheader.intUser = sys_users.intUserId
WHERE
ware_mrnheader.intMrnNo =  '$mrnNo' AND
ware_mrnheader.intMrnYear =  '$year' 
GROUP BY
ware_mrnheader.intMrnNo,
ware_mrnheader.intMrnYear"; 
			 $result = $db->RunQuery($sql);
			 while($row=mysqli_fetch_array($result))
			 {
				$department=$row['department'];
				$date = $row['datdate'];
				$intStatus = $row['intStatus'];
			 }
					//default user department
					if($mrnNo==''){
					
					 $sql1 = "SELECT
								sys_users.intDepartmentId
								FROM
								sys_users
								WHERE
								sys_users.intUserId =  '$intUser'";	
					$result1 = $db->RunQuery($sql1);
					$row1=mysqli_fetch_array($result1);
					$department=$row1['intDepartmentId'];
					}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Company Wise Stock Balance</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="companyStockBalance-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../../libraries/calendar/theme.css" />
<script src="../../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body>
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>


<form id="frmCompanyStockBalance" name="frmCompanyStockBalance" method="get" action="companyStockBalance.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Company Wise Stock Balance</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
          <td width="17%"></td>
            <td width="15%" height="27" class="normalfnt">Company</td>
            <td width="30%"><select name="cboCompany" id="cboCompany" style="width:250px"  class="txtText validate[required]"  >
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_companies.intId,
							mst_companies.strName
							FROM mst_companies
							WHERE
							mst_companies.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td width="14%">&nbsp;</td>
            <td width="24%">&nbsp;</td>
          </tr> 
           
                <?php
					$sql = "SELECT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory";
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						$i++;
						$categoryId[$i]=$row['intId'];
						$category[$i]=$row['strName'];
						?>
          <tr>
          <td></td>
            <td colspan="2"><div id="<?php echo "div".$i; ?>" style="display:none">
  <div class="normalfnt subcategory" style="text-align:left; font-size:11px; background-color:#FFD8B0" ><?php echo $category[$i]; ?></div>
<div id="<?php echo "divTable".$i; ?>" style="overflow:scroll;width:100%;height:130px"  >
  <table width="100%" id="<?php echo "tbl".$i; ?>" >
                  <?php
		$sql1 = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strCode,
				mst_subcategory.strName
				FROM mst_subcategory
				WHERE
				mst_subcategory.intMainCategory =  '$categoryId[$i]'";
				$result1 = $db->RunQuery($sql1);
				while($row1=mysqli_fetch_array($result1))
				{
				?>
        <tr class="chkRow">
          <td class="normalfnt" align="left"><input name="chkPayMethod" id="<?php echo $row1['intId']; ?>" type="checkbox" value="<?php echo $row1['intId']; ?>"  class="subcheck"/>&nbsp;<?php echo $row1['strName']; ?></td>
        </tr>
                  <?php
					}
				?>
        
      </table></div></div> <!--divBnkDepositFields-->
</td>
            <td></td>
            <td></td>
          </tr>
                  <?php
					}
				?>
          <tr>
          <td colspan="5"><table width="100%" border="0" cellpadding="0" cellspacing="0"><tr>
          <td width="32%" height="27" class="normalfnt"></td>
          <td width="13%" height="27" class="normalfnt">&nbsp;</td>
          <td width="0%" height="27" class="normalfnt"></td>
          <td width="17%" height="27" class="normalfnt" align="right"><img src="../../../../../images/download.png" width="115" height="24" alt="download excell" id="butDownload" name="butDownload" /></td>
          <td width="23%" height="27" class="normalfnt">&nbsp;</td>
          <td width="15%" height="27" class="normalfnt">&nbsp;</td>
          </tr></table></td>
          </tr>
        </table></td>
      </tr>
<tr>
      </tr>      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="92%">&nbsp;</td>
            <td width="8%">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td></td>
      </tr>
  </table></td></table></div></div>
</form>
	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>


</body>
</html>

<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
date_default_timezone_set('Asia/Kolkata');

require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";
 
$company			= $_REQUEST['company'];
$location			= $_REQUEST['location'];
$supplier			= $_REQUEST['supplier'];

 $thisFilePath 		=  $_SERVER['PHP_SELF'];

 
	$sql = "SELECT SUB_1.* FROM
			(
			SELECT * 
			FROM view_grn_summary_list_new
			WHERE 1=1 ";
		
			if($company!='')
				$sql .= "AND companyId = '$company' ";
			if($location!='')
				$sql .= "AND locationId = '$location' "; 
			if($supplier!='')
				$sql .= "AND supplierId = '$supplier' ";
	
	$sql.= " ) AS SUB_1 WHERE 1=1"; 
 
$jq = new jqgrid('',$db);	
$col = array();

$col["title"] 			= "GRN No"; 
$col["name"] 			= "conGrnNo";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 
$col["search"] 			= true; 
$cols[] 				= $col;
$col					= NULL;

$col["title"] 			= "GRN Date";
$col["name"] 			= "GRNDATE";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "PO No";
$col["name"] 			= "conPONo";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Invoice Date";
$col["name"] 			= "InvoiceDate";
$col["width"] 			= "5";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 
$cols[] 				= $col;	
$col					= NULL;


$col["title"] 			= "Invoice No";
$col["name"] 			= "strInvoiceNo";
$col["width"] 			= "5";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Supplier";
$col["name"] 			= "supplier";
$col["width"] 			= "6";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Location";
$col["name"] 			= "location";
$col["width"] 			= "5";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Company";
$col["name"] 			= "companyName";
$col["width"] 			= "5";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Currency";
$col["name"] 			= "strCode";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "PO Amount";
$col["name"] 			= "PO_amount";
$col["width"] 			= "3";
$col["align"] 			= "right";
$col["sortable"] 		= true; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "GRN Amount";
$col["name"] 			= "GRN_value";
$col["width"] 			= "3";
$col["align"] 			= "right";
$col["sortable"] 		= true; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "RTS Amount";
$col["name"] 			= "returntosupplier";
$col["width"] 			= "3";
$col["align"] 			= "right";
$col["sortable"] 		= true; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Inv Booking Serial ";
$col["name"] 			= "invoice_booking_serial";
$col["width"] 			= "4";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Payment Completed";
$col["name"] 			= "PAYMENT_COMPLETED";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "GRN Summary Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'GRNDATE,intGrnYear,intGrnNo'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);




$out = $jq->render("list1");
?>
<title>GRN Listing</title>

	<?php 
		//include "include/listing.html";
	?>
<form id="frmlisting" name="frmlisting" method="post" action="">

        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
</form>

<?php
session_start();
 $companyId 			= $_SESSION['CompanyID'];
$locationId 		= $_SESSION['CompanyID'];//this locationId use in report header(reportHeader.php)-------
$intUser  			= $_SESSION["userId"];
$mainPath 			= $_SESSION['mainPath'];
$thisFilePath 		=  $_SERVER['PHP_SELF'];
 require_once 		"class/cls_commonFunctions_get.php";
  
$obj_common	= new cls_commonFunctions_get($db);


$company 	= $_REQUEST['company'];
$location 	= $_REQUEST['location'];
$subStores 	= $_REQUEST['subStores'];
$orderNo 	= $_REQUEST['orderNo'];
$orderYear 	= $_REQUEST['orderYear'];
$salesOrder = $_REQUEST['salesOrder'];

$result			= $obj_common->loadCompany_result($company,'RunQuery');
$row 			= mysqli_fetch_array($result);
$company_desc 	= $row['strName'];

$result			= $obj_common->loadLocationName($location,'RunQuery');
$row 			= mysqli_fetch_array($result);
$location_desc	= $row['strName'];

$result			= $obj_common->load_sub_stores_result($subStores,'RunQuery');
$row 			= mysqli_fetch_array($result);
$subStores_desc	= $row['strName'];

$result			= $obj_common->load_sales_order_result($orderNo,$orderYear,$salesOrder,'RunQuery');
$row 			= mysqli_fetch_array($result);
$salesOrder_desc= $row['salesOrder'];

 //----------------------------------
?>
 <head>
 <title>Order allocated Ink Items Stock Balance Report</title>
 <style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:264px;
	top:247px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<form id="frmMrnReport" name="frmMrnReport" method="post" >
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>ORDER ALLOCATED INK ITEM STOCK BALANCE REPORT</strong></div>
<table width="868" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td width="905">
  <table width="100%">
   <tr>
    <td width="9%"></td>
    <td width="9%" class="normalfnt" align="right"><strong style="text-align:right">Company</strong></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="31%" align="left"><span class="normalfnt" style="text-align:left"><?php echo $company_desc  ?></span></td>
    <td width="13%">&nbsp;</td>
    <td width="1%" align="center" valign="middle">&nbsp;</td>
    <td width="22%">&nbsp;</td>
    <td width="6%" class="normalfnt"></td>
    <td width="7%"></td>
  </tr>
   <tr>
    <td></td>
    <td class="normalfnt" align="right"><strong>Location</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $location_desc  ?></span></td>
    <td class="normalfnt" align="right"><strong style="text-align:right">Sub Stores</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt" style="text-align:left"><?php echo $subStores_desc  ?></span></td>
    <td class="normalfnt"></td>
    <td></td>
  </tr>
   <tr>
    <td></td>
    <td class="normalfnt"><strong>Order No</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php if($orderNo !='' ){echo $orderYear."/".$orderNo;}  ?></span></td>
    <td class="normalfnt" align="right"><strong style="text-align:right">Sales Order</strong></td>
    <td align="center" valign="middle"></td>
    <td><span class="normalfnt" style="text-align:left"><?php echo $salesOrder_desc  ?></span></td>
    <td></td>
    <td></td>
  </tr>
   </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr>
              <th width="14%" >Order No</th>
              <th width="17%" >Sales Order</th>
              <th width="16%" >Main Category</th>
              <th width="19%" >Sub Category</th>
              <th width="28%" >Item</th>
              <th width="5%" >UOM</th>
              <th width="6%" >Qty</th>
              </tr>
            <?php 
			
			$sql = "SELECT
					DISTINCT 
					trn_orderheader.intOrderNo,
					trn_orderheader.intOrderYear,
					CONCAT(trn_orderheader.intOrderNo,'/',trn_orderheader.intOrderYear) AS ORDER_NO,
					trn_orderdetails.strSalesOrderNo as SALES_ORDER,
					trn_orderdetails.intSalesOrderId, 
					mst_item.strName itemName,
					mst_units.strName as unitName,
					mst_units.intId as unitId,
					mst_units.dblNoOfPcs ,
					mst_maincategory.strName AS mainCat,
					mst_maincategory.intId AS mainId,
					mst_subcategory.strName AS subCat,
					mst_subcategory.intId AS subId,

					(SELECT
					sum(ware_sub_stocktransactions_bulk.dblQty)
					FROM `ware_sub_stocktransactions_bulk`
					WHERE
					ware_sub_stocktransactions_bulk.intOrderNo = trn_orderheader.intOrderNo AND
					ware_sub_stocktransactions_bulk.intOrderYear = trn_orderheader.intOrderYear AND
					ware_sub_stocktransactions_bulk.intSalesOrderId = trn_orderdetails.intSalesOrderId AND
					ware_sub_stocktransactions_bulk.intItemId = trn_sample_color_recipes.intItem AND
					ware_sub_stocktransactions_bulk.intLocationId = '$location' AND 
					ware_sub_stocktransactions_bulk.intSubStores = '$subStores'
					) AS INK_STOCK  
					FROM
					trn_orderheader
					INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_orderdetails.intRevisionNo AND trn_sampleinfomations.strGraphicRefNo = trn_orderdetails.strGraphicNo
					Inner Join trn_sample_color_recipes ON trn_sample_color_recipes.intSampleNo = trn_orderdetails.intSampleNo AND trn_sample_color_recipes.intSampleYear = trn_orderdetails.intSampleYear AND trn_sample_color_recipes.intRevisionNo = trn_orderdetails.intRevisionNo AND trn_sample_color_recipes.strCombo = trn_orderdetails.strCombo AND trn_sample_color_recipes.strPrintName = trn_orderdetails.strPrintName
					Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = trn_sample_color_recipes.intSampleNo AND trn_sampleinfomations_details_technical.intSampleYear = trn_sample_color_recipes.intSampleYear AND trn_sampleinfomations_details_technical.intRevNo = trn_sample_color_recipes.intRevisionNo AND trn_sampleinfomations_details_technical.strComboName = trn_sample_color_recipes.strCombo AND trn_sampleinfomations_details_technical.strPrintName = trn_sample_color_recipes.strPrintName AND trn_sampleinfomations_details_technical.intColorId = trn_sample_color_recipes.intColorId AND trn_sampleinfomations_details_technical.intInkTypeId = trn_sample_color_recipes.intInkTypeId
					Inner Join mst_item ON mst_item.intId = trn_sample_color_recipes.intItem
					Inner Join mst_locations ON trn_sampleinfomations.intCompanyId = mst_locations.intId 
					left Join mst_financeexchangerate ON mst_item.intCurrency = mst_financeexchangerate.intCurrencyId AND trn_sampleinfomations.dtDate = mst_financeexchangerate.dtmDate AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId 
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId
					Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
					Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
					WHERE
					trn_orderheader.intStatus = '1'  
					AND  trn_orderheader.intOrderNo IS NOT NULL ";
					if($orderNo != '')
					$sql.=" AND
					trn_orderheader.intOrderNo = '$orderNo' ";
					if($orderYear != '')
					$sql.=" AND
					trn_orderheader.intOrderYear = '$orderYear' ";
					if($salesOrder != '')
					$sql.=" AND
					trn_orderdetails.intSalesOrderId = '$salesOrder' ";
					
					$sql.=" GROUP BY
					trn_orderheader.intOrderNo,
					trn_orderheader.intOrderYear,
					trn_orderdetails.strSalesOrderNo,
					trn_orderdetails.intSalesOrderId,
					mst_item.intId 
					/*having INK_STOCK >0*/
					ORDER BY 
					trn_orderheader.intOrderNo asc ,
					trn_orderheader.intOrderYear asc,
					trn_orderdetails.strSalesOrderNo asc,
					trn_orderdetails.intSalesOrderId asc,
					mst_item.strName asc 
					
						";

			//echo $sql;

			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				
				$result_s			= $obj_common->load_sales_order_result($row['intOrderNo'],$row['intOrderYear'],$row['intSalesOrderId'],'RunQuery');
				$row_s 				= mysqli_fetch_array($result_s);
				$salesOrder_desc	= $row_s['salesOrder'];
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $row['ORDER_NO']?></td>
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $salesOrder_desc ?></td>
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $row['mainCat'] ?></td>
              <td class="normalfntMid" nowrap="nowrap" ><?php echo $row['subCat'] ?></td>
              <td class="normalfntMid" ><?php echo $row['itemName'] ?></td>
              <td class="normalfntMid" ><?php echo $row['unitName'] ?></td>
              <td class="normalfntRight" ><?php echo number_format($row['INK_STOCK'],4) ?></td>
              </tr>
            <?php 
			}
	  ?>
            </table>
        </td>
        </tr>
      
      </table>
    </td>
</tr>

<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
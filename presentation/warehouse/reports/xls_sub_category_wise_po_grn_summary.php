<?php

header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=report.xls" );
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");

?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<th width="100%" colspan="9"><?php echo "SUPPLIER PO - GRN SUMMERY$currencyName"; ?></th>
</tr>
<tr>
<th width="20%">Sub Category</th>
<th width="10%">Date</th>
<th width="10%"> Supplier</th>
<th width="10%"> Invoice No</th>
<th width="10%">Invoice Date </th>
<th width="10%">PO No </th>
<th width="10%" >PO Type </th>
<th width="10%" >GRN No </th>
<th width="10%">Return No </th>
<th width="10%">Amount </th>
</tr>
<?php

while($row = mysqli_fetch_array($result))
{
	if(($row['SubCatOrder'] !='y')){
?>
<tr <?php if($row['SUPPLIER']!=''){ ?>bgcolor="#FDFDFD" <?php }else if($row['Return No']=='Total'){ ?>bgcolor="#595959"<?php } else { ?> bgcolor="#A5A5A5"<?php } ?>>

<td class="normalfnt" style="text-align: center"><?php echo $row['Sub Category']; ?></td>
<td class="normalfnt" style="text-align: center"><?php echo $row['Date']; ?></td>
<td class="normalfnt" style="text-align: center"><?php echo $row['SUPPLIER']; ?></td>
<td class="normalfnt" style="text-align: center"><?php echo $row['Invoice No']; ?></td>
<td class="normalfnt" style="text-align: center"><?php echo $row['Invoice Date']; ?></td>
<td class="normalfnt" style="text-align: center"><?php echo $row['PO No']; ?></td>
<td class="normalfnt" style="text-align: center"><?php echo $row['PO Type']; ?></td>
<td class="normalfnt" style="text-align: right"><?php  echo ($row['GRN No']); ?></td>
<?php
if($row['Return No']=="Total"){ //bold the text
	?>
<td class="normalfnt" style="text-align: right"><b><?php echo ($row['Return No']); ?></b></td>
<td class="normalfnt" style="text-align: right; "><b><?php echo round($row['Amount'],4); ?></b></td>
<?php
}
else{
	?>
<td class="normalfnt" style="text-align: right"><?php echo ($row['Return No']); ?></td>
<td class="normalfnt" style="text-align: right"><?php echo round($row['Amount'],4); ?></td>
<?php
}
?>
</tr>
<?php
	}
}
?>
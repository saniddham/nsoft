<?php
date_default_timezone_set('Asia/Colombo');
ini_set('display_errors',0);
 
 $thisFilePath 		=  $_SERVER['PHP_SELF'];

require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";

	

$company 	= $_SESSION['headCompanyId'];
$location 	= $_SESSION['CompanyID'];
$intUser  	= $_SESSION["userId"];
$programName='STOCK MOVEMENT DETAILS';



 $sql = "select * from(	SELECT
				mst_companies.strName AS companyName,
				mst_locations.strName AS locationName,
				mst_maincategory.strName AS mainName,
				mst_subcategory.strName AS subName,
				mst_item.strName AS itemName,
				mst_units.strName AS uom,
				ware_stocktransactions_bulk.intDocumntYear,
				ware_stocktransactions_bulk.intDocumentNo,
				date(ware_stocktransactions_bulk.dtDate) AS cdate,
				ware_stocktransactions_bulk.strType,
				mst_financecurrency.strCode AS currencyCode,
				ware_stocktransactions_bulk.dblGRNRate AS rate,
				ware_stocktransactions_bulk.dblQty
			FROM
			ware_stocktransactions_bulk
				Inner Join mst_locations ON mst_locations.intId = ware_stocktransactions_bulk.intLocationId
				Inner Join mst_companies ON mst_companies.intId = mst_locations.intCompanyId
				Inner Join mst_item ON mst_item.intId = ware_stocktransactions_bulk.intItemId
				Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
				Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
				Inner Join mst_units ON mst_units.intId = mst_item.intUOM
				Inner Join mst_financecurrency ON ware_stocktransactions_bulk.intCurrencyId = mst_financecurrency.intId
					) as t where 1=1				";
					   	//echo $sql;
$col = array();

$col["title"] 		= "Company"; 
$col["name"] 		= "companyName"; 
$col["width"] 		= "2";
$col["align"] 		= "center";
$col["stype"] 		= "select";
$col["editoptions"] 	=  array("value"=> loadSelectBox('SELECT strName AS name FROM mst_companies where intStatus=1 ORDER BY name ASC'));
$cols[] = $col;	$col=NULL;

$col["title"] = "Location"; 
$col["name"] = "locationName"; 
$col["width"] = "2";
$col["align"] = "center";
$col["stype"] 		= "select";
$col["editoptions"] 	=  array("value"=> loadSelectBox('SELECT strName AS name FROM mst_locations where intStatus=1  ORDER BY name ASC'));
$cols[] = $col;	$col=NULL;

$col["title"] = "Main Category"; 
$col["name"] = "mainName"; 
$col["width"] = "2";
$col["stype"] 			= "select";
$col["editoptions"] 	=  array("value"=> loadSelectBox('SELECT strName as name FROM mst_maincategory where intStatus=1  ORDER BY mst_maincategory.strName ASC'));
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

$col["title"] = "Sub Category"; 
$col["name"] = "subName"; 
$col["width"] = "2";
$col["align"] = "center";
$col["stype"] 			= "select";
$col["editoptions"] 	=  array("value"=> loadSelectBox('SELECT strName AS name FROM mst_subcategory where intStatus=1  ORDER BY name ASC'));
$cols[] = $col;	$col=NULL;

$col["title"] = "Item"; 
$col["name"] = "itemName"; 
$col["width"] = "2";
$col["align"] = "center";
$col["stype"] 			= "select";
$col["editoptions"] 	=  array("value"=> loadSelectBox('SELECT strName AS name FROM  mst_item where intStatus=1   ORDER BY name ASC'));
$cols[] = $col;	$col=NULL;

$col["title"] = "UOM"; 
$col["name"] = "uom"; 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

$col["title"] = "Document Year"; 
$col["name"] = "intDocumntYear"; 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

$col["title"] = "Document No"; 
$col["name"] = "intDocumentNo"; 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

$col["title"] = "Date";
$col["name"] = "cdate";  
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

$col["title"] = "Type"; 
$col["name"] = "strType"; 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

$col["title"] = "Currency"; 
$col["name"] = "currencyCode"; 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

$col["title"] = "Rate"; 
$col["name"] = "rate"; 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

$col["title"] = "Qty"; 
$col["name"] = "dblQty"; 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

$jq = new jqgrid();

$grid["caption"] 		= "STOCK MOVEMENT DETAILS";
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'cdate'; // by default sort grid by this field
$grid["sortorder"] 		= "ASC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes

$grid["export"]["range"] = "filtered"; // or "all"
// initialize search, 'name' field equal to (eq) 'Client 1'
/*$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"}
     ]
}
SEARCH_JSON;

$grid["search"] = true; 
$grid["postData"] = array("filters" => $sarr ); */

// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

// params are array(<function-name>,<class-object> or <null-if-global-func>,<continue-default-operation>)
$e["on_render_excel"] = array("custom_excel_layout", null, true);
$jq->set_events($e);
				
function custom_excel_layout($param)
{
	$grid = $param["grid"];
	$xls = $param["xls"];
	$arr = $param["data"];
	
	$xls->addHeader(array('STOCK MOVEMENT'));
	$xls->addHeader(array());
	$xls->addHeader($arr[0]);
	$summary_arr = array(0);
	
	for($i=1;$i<count($arr);$i++)
	{
		$xls->addRow($arr[$i]);
		$summary_arr[0] += $arr[$i]["id"];
	}
	//$xls->addRow($summary_arr);
	return $xls;
}


$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);



$out = $jq->render("list1");
?>
<head>
	<?php 
		include "include/listing.html";
	?>
</head>
<body>	
<form id="frmlisting" name="frmlisting" method="post" action="">

        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
</form>
</body>

</html>
<?php
function loadSelectBox($sql){
	global $db;
	$result = $db->RunQuery($sql);
	$strOption3 = ":All";
	while($row=mysqli_fetch_array($result))
	{
		$strOption3 .= ";".$row['name'].":".$row['name'] ;
	}
			 
	return $strOption3;

}
?>
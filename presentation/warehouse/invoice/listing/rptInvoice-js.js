// JavaScript Document
var basePath = "presentation/warehouse/invoice/listing/";

$(document).ready(function() {
	$('#frmInvoiceReport').validationEngine();
	
	$('#frmInvoiceReport #imgApprove').click(function(){
	var val = $.prompt('Are you sure you want to approve this Inter Company Transfer ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
					showWaiting();
					if(validateQuantities()==0){
					///////////
					var url = basePath+"rptInvoice-db-set.php"+window.location.search+'&status=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmInvoiceReport #imgApprove').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmInvoiceReport #imgApprove').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
						hideWaiting();
					}
				}});
	});
	
	$('#frmInvoiceReport #imgReject').click(function(){
	var val = $.prompt('Are you sure you want to reject this Inter Company Transfer ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
				if(v)
				{
					if(validateRejecton()==0){
					///////////
					var url = basePath+"rptInvoice-db-set.php"+window.location.search+'&status=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmInvoiceReport #imgReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmInvoiceReport #imgReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
					}
				}
			}});
	});
	
});
//---------------------------------------------------
function validateQuantities(){
		var ret=0;	
	    var invoiceNo = document.getElementById('divInvNo').innerHTML;
		var url 		= basePath+"rptInvoice-db-get.php?requestType=getValidation";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"invoiceNo="+invoiceNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",1000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//---------------------------------------------------
function validateRejecton(){
		var ret=0;	
	    var invoiceNo = document.getElementById('divInvNo').innerHTML;
		var url 		= basePath+"rptInvoice-db-get.php?requestType=validateRejecton";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"invoiceNo="+invoiceNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",3000);
					  ret= 1;
					}
				}
			});
			return ret;

}
//------------------------------------------------
function alertx()
{
	$('#frmInvoiceReport #imgApprove').validationEngine('hide')	;
}
//--------------------------------------------
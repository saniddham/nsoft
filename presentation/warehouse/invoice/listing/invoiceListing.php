<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
date_default_timezone_set('Asia/Kolkata');
//ini_set('allow_url_include',1);
// ini_set('display_errors',1);
session_start();

$thisFilePath 		=  $_SERVER['PHP_SELF'];
require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";

$company 	= $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];

$programName='Invoice';
$programCode='P0484';
$intUser  = $_SESSION["userId"];

$reportMenuId	='909';
$menuId			='484';

$userDepartment=getUserDepartment($intUser);
$approveLevel = (int)getMaxApproveLevel();

################################################################
################## new part ####################################
################################################################
///// create where part ///////////////////
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'Status'=>'tb1.intStatus',
				'Invoice_No'=>'tb1.intInvoiceNo',
				'Invoice_Year'=>'tb1.intInvoiceYear',
				'PO_No'=>'tb1.intPONo',
				'PO_Year'=>'tb1.intPOYear',
				'Date'=>'tb1.datdate',
				'User'=>'sys_users.strUserName'
				);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND substr(tb1.datdate,1,10) = '".date('Y-m-d')."'";
	
################## end code ####################################

$sql = "select * from(SELECT DISTINCT 
							if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected','Pending')) as Status,
							tb1.intInvoiceNo as `Invoice_No`,
							tb1.intInvoiceYear as `Invoice_Year`,
							tb1.intPONo as `PO_No`, 
							tb1.intPOYear as `PO_Year`, 
							tb1.datdate as `Date`,
							sys_users.strUserName as `User`,

							IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(ware_invoiceheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_invoiceheader_approvedby
								Inner Join sys_users ON ware_invoiceheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_invoiceheader_approvedby.intInvoiceNo  = tb1.intInvoiceNo AND
								ware_invoiceheader_approvedby.intYear =  tb1.intInvoiceYear AND
								ware_invoiceheader_approvedby.intApproveLevelNo =  '1' AND
								ware_invoiceheader_approvedby.intStatus =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.intStatus>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_invoiceheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_invoiceheader_approvedby
								Inner Join sys_users ON ware_invoiceheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_invoiceheader_approvedby.intInvoiceNo  = tb1.intInvoiceNo AND
								ware_invoiceheader_approvedby.intYear =  tb1.intInvoiceYear AND
								ware_invoiceheader_approvedby.intApproveLevelNo =  '$i' AND
								ware_invoiceheader_approvedby.intStatus =  '0'
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.intStatus>1) AND (tb1.intStatus<=tb1.intApproveLevels) AND ((SELECT
								max(ware_invoiceheader_approvedby.dtApprovedDate) 
								FROM
								ware_invoiceheader_approvedby
								Inner Join sys_users ON ware_invoiceheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_invoiceheader_approvedby.intInvoiceNo  = tb1.intInvoiceNo AND
								ware_invoiceheader_approvedby.intYear =  tb1.intInvoiceYear AND
								ware_invoiceheader_approvedby.intApproveLevelNo =  ($i-1) AND
								ware_invoiceheader_approvedby.intStatus =  '0')<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*fase part*/
								 if($i>tb1.intApproveLevels,'-----',''))
								
								/*end IFNULL false part*/
								) as `".$approval."`, "; 
									
								}
								
							$sql .= "'View' as `View`   
						FROM
							ware_invoiceheader as tb1 
							Inner Join sys_users ON tb1.intUser = sys_users.intUserId 
							Inner Join mst_locations ON tb1.intCompanyId = mst_locations.intId 
							WHERE
							mst_locations.intCompanyId =  '$company' AND 
							tb1.intCompanyId =  '$location'
							$where_string
							)  as t where 1=1
						";
					  //	    echo $sql;

	
$formLink			= "?q=$menuId&invoiceNo={Invoice_No}&year={Invoice_Year}";	 
$reportLink  		= "?q=$reportMenuId&invoiceNo={Invoice_No}&year={Invoice_Year}";
$reportLinkApprove  = "?q=$reportMenuId&invoiceNo={Invoice_No}&year={Invoice_Year}&approveMode=1";
$reportLinkPrint  	= "?q=$reportMenuId&invoiceNo={Invoice_No}&year={Invoice_Year}&mode=print";
						 
$col = array();

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;	$col=NULL;

//PO No
$col["title"] 	= "Invoice No"; // caption of column
$col["name"] 	= "Invoice_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= $formLink;	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

 
$cols[] = $col;	$col=NULL;



//PO Year
$col["title"] = "Invoice Year"; // caption of column
$col["name"] = "Invoice_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//PO Year
$col["title"] = "PO No"; // caption of column
$col["name"] = "PO_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//PO Year
$col["title"] = "PO Year"; // caption of column
$col["name"] = "PO_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//Supplier
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//User
$col["title"] = "User"; // caption of column
$col["name"] = "User"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//FIRST APPROVAL
$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}



//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Invoice (Loan In Settlement) Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'Invoice_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes

$grid["export"]["range"] = "filtered"; 

// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$jq["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

// params are array(<function-name>,<class-object> or <null-if-global-func>,<continue-default-operation>)
$e["on_render_excel"] = array("custom_excel_layout", null, true);
$jq->set_events($e);
				
function custom_excel_layout($param)
{
	$grid = $param["grid"];
	$xls = $param["xls"];
	$arr = $param["data"];
	
	$xls->addHeader(array('INTERCOMPANY TRANSFER LIST'));
	$xls->addHeader(array());
	$xls->addHeader($arr[0]);
	$summary_arr = array(0);
	
	for($i=1;$i<count($arr);$i++)
	{
		$xls->addRow($arr[$i]);
		$summary_arr[0] += $arr[$i]["id"];
	}
	//$xls->addRow($summary_arr);
	return $xls;
}


$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);




$out = $jq->render("list1");
?>
 	<?php 
		//include "include/listing.html";
	?>
    <title>Invoice Listing</title>
 <form id="frmlisting" name="frmlisting" method="post" action="">

        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
</form>
 <?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(ware_invoiceheader.intApproveLevels) AS appLevel
			FROM ware_invoiceheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
//------------------------------function load User Department---------------------
function getUserDepartment($intUser){
	global $db;

	$sqlp = "SELECT
			sys_users.intDepartmentId
			FROM
			sys_users
			WHERE
			sys_users.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['intDepartmentId'];

}
?>


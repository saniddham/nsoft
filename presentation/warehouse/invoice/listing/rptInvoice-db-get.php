<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once $backwardseperator."class/warehouse/cls_warehouse_get.php";

	$objwhouseget= new cls_warehouse_get($db);

	$programName='Invoice';
	$programCode='P0484';
	$invoiceApproveLevel = (int)getApproveLevel($programName);
	
		$db->OpenConnection();
		$db->RunQuery2('Begin');

 if($requestType=='getValidation')
	{
		$invoiceNo  = $_REQUEST['invoiceNo'];
		$invoiceNoArray=explode("/",$invoiceNo);
		
		$sql = "SELECT
		ware_invoiceheader.intPONo,
		ware_invoiceheader.intPOYear,
		ware_invoiceheader.intStatus, 
		ware_invoiceheader.intApproveLevels,  
		ware_invoiceheader.intCompanyId as location,
		mst_locations.intCompanyId as companyId 
		FROM
		ware_invoiceheader
		Inner Join mst_locations ON ware_invoiceheader.intCompanyId = mst_locations.intId
		WHERE
		ware_invoiceheader.intInvoiceNo =  '$invoiceNoArray[0]' AND
		ware_invoiceheader.intInvoiceYear =  '$invoiceNoArray[1]'";

		$result = $db->RunQuery2($sql);
		$errorFlg=0;
		$msg ="Final confirmation of this Invoice is already raised"; 
		$row=mysqli_fetch_array($result);
		$status=$row['intStatus'];
		$company = $row['companyId'];
		$location = $row['location'];
		$approveLevels=$row['intApproveLevels'];	
		$poNo=$row['intPONo'];
		$poYear=$row['intPOYear'];
		
		//--------------------------
		$response['arrData'] = $objwhouseget->getLocationValidation('INVOICE',$companyId,$poNo,$poYear);//check for grn location
		//--------------------------
		$confirmatonMode=loadConfirmatonMode($programCode,$status,$approveLevels,$userId);
		
		/*if($response['arrData']['type']=='fail'){
			$errorFlg = 1;
			$msg=$response['arrData']['msg'];
		}
		else */if($status==1){// 
			$errorFlg = 1;
			$msg ="Final confirmation of this Invoice is already raised"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This Gate Pass No is Rejected"; 
		}
		else if($confirmatonMode==0){// 
			$errorFlg = 1;
			$msg ="No Permission to Approve"; 
		}
		else{// 
			$errorFlg = 0;
		}
		////////////////////////
	
		if($errorFlg == 0){
		       $sql = "SELECT
ware_invoicedetails.intLocation,
ware_invoicedetails.intItemId,
ware_invoicedetails.dblQty,
ware_invoiceheader.intStatus,
ware_invoiceheader.intApproveLevels,
mst_item.strName AS itemName
FROM
ware_invoicedetails
Inner Join ware_invoiceheader ON ware_invoicedetails.intInvoiceNo = ware_invoiceheader.intInvoiceNo AND ware_invoicedetails.intInvoiceYear = ware_invoiceheader.intInvoiceYear
Inner Join mst_item ON ware_invoicedetails.intItemId = mst_item.intId
WHERE
ware_invoicedetails.intInvoiceNo =  '$invoiceNoArray[0]' AND
ware_invoicedetails.intInvoiceYear =  '$invoiceNoArray[1]'";
				
		$result = $db->RunQuery2($sql);
		$errorFlg=0;
		$msg ="Maximum Qtys for items"; 
		$totTosaveQty=0;
		$totSavableQty=0;
		while($row=mysqli_fetch_array($result))
		{	
			 $Qty=$row['dblQty'];
			 
			 
			//$location 	= $_SESSION['CompanyID'];
			$invlocationId 	= $row['intLocation'];
			//$company 	= $_SESSION['headCompanyId'];
			
			$stockBalQty=getStockBalance_bulk($location,$row['intItemId']);
			$balToInvQty=getBalToInvQty($poNo,$poYear,$row['intItemId']);
			$stockBalQty=round($stockBalQty,4);
			$balToInvQty=round($balToInvQty,4);
				
			if($row['intStatus']==($row['intApproveLevels']+1)){//no confirmation has been raised
				if($stockBalQty < $balToInvQty)
					$balToInvQty = $stockBalQty;
			}
			else{//confirmation has been raised
				$balToInvQty = $balToInvQty+$Qty;
				if($stockBalQty < $balToInvQty)
					$balToInvQty = $stockBalQty;
			}
			
			

			if($Qty>$balToInvQty){
				$errorFlg=1;
					$msg .="</br> "."-".$row['itemName']." =".round($balToInvQty,4);
			}
			
			$totTosaveQty +=$Qty;
			$totSavableQty +=$balToInvQty;
			
		}//end of while
		}
	
	
	if(($totTosaveQty > $totSavableQty) && ($errorFlg!=1)){
		$errorFlg=1;
		$msg ="No Stock balance to confirm.";
	}
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}
//---------------------------
else if($requestType=='validateRejecton')
	{
		$invoiceNo  = $_REQUEST['invoiceNo'];
		$invoiceNoArray=explode("/",$invoiceNo);
		
		$sql = "SELECT
		ware_invoiceheader.intStatus, 
		ware_invoiceheader.intApproveLevels 
		FROM ware_invoiceheader
		WHERE
		ware_invoiceheader.intInvoiceNo =  '$invoiceNoArray[0]' AND
		ware_invoiceheader.intInvoiceYear =  '$invoiceNoArray[1]'";

		$result = $db->RunQuery2($sql);
		$errorFlg=0;
		$msg ="";
		$row=mysqli_fetch_array($result);
		
		$rejectionMode=loadRejectionMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);

		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this Invoice is already raised.So cant reject"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This Invoice is already Rejected"; 
		}
		else if($rejectionMode==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="No Permission to reject this Invoice"; 
		}
		else{//confirmation has been raised
			$errorFlg = 0;
			
		}
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}

		$db->RunQuery2('Commit');
		$db->CloseConnection();		

	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery2($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
	//-----------------------------------------------------------------
	function getBalToInvQty($poNo,$poYear,$itemId)
	{
		global $db;
		$sql = "SELECT
				mst_item.strName AS itemName,
				mst_item.intId,
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName AS mainCatName,
				mst_subcategory.strName AS subCatName,
				mst_supplier.intCompanyId,
				tb1.dblQty,
				tb1.dblGRNQty, 
				mst_units.strCode as uom , 
				
				IFNULL((SELECT
				Sum(ware_invoicedetails.dblQty)
				FROM
				ware_invoiceheader
				Inner Join ware_invoicedetails ON ware_invoiceheader.intInvoiceNo = ware_invoicedetails.intInvoiceNo AND ware_invoiceheader.intInvoiceYear = ware_invoicedetails.intInvoiceYear
				WHERE
				ware_invoiceheader.intPONo =  tb1.intPONo AND
				ware_invoiceheader.intPOYear =  tb1.intPOYear AND
				ware_invoicedetails.intItemId =  tb1.intItem AND
				ware_invoiceheader.intStatus <=  ware_invoiceheader.intApproveLevels AND
				ware_invoiceheader.intStatus >  '0'),0) as dblInvoiceQty  , 
				
			 	IFNULL((SELECT
				Sum(ware_returntosupplierdetails.dblQty)
				FROM
				ware_returntosupplierheader
				Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear
				Inner Join ware_grnheader ON ware_returntosupplierheader.intGrnNo = ware_grnheader.intGrnNo AND ware_returntosupplierheader.intGrnYear = ware_grnheader.intGrnYear
				WHERE
				ware_grnheader.intPoNo =  tb1.intPONo AND
				ware_grnheader.intPoYear =  tb1.intPOYear AND
				ware_returntosupplierdetails.intItemId =  tb1.intItem AND
				ware_returntosupplierheader.intStatus = 1),0) as dblRetunSupplierQty  
				
				FROM
				trn_poheader
				Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
				Inner Join trn_podetails as tb1 ON trn_poheader.intPONo = tb1.intPONo AND trn_poheader.intPOYear = tb1.intPOYear 
				Left Join ware_grnheader ON tb1.intPONo = ware_grnheader.intPoNo AND tb1.intPOYear = ware_grnheader.intPoYear and ware_grnheader.intStatus<=ware_grnheader.intApproveLevels  and ware_grnheader.intStatus>0
				Left Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND tb1.intItem = ware_grndetails.intItemId
				Left Join ware_invoiceheader ON tb1.intPONo = ware_invoiceheader.intPONo AND tb1.intPOYear = ware_invoiceheader.intPOYear and ware_invoiceheader.intStatus<=ware_invoiceheader.intApproveLevels  and ware_invoiceheader.intStatus>0
				Left Join ware_invoicedetails ON ware_invoiceheader.intInvoiceNo = ware_invoicedetails.intInvoiceNo AND ware_invoiceheader.intInvoiceYear = ware_invoicedetails.intInvoiceYear AND tb1.intItem = ware_invoicedetails.intItemId
				Inner Join mst_item ON tb1.intItem = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				WHERE
				trn_poheader.intPONo =  '$poNo' AND
				trn_poheader.intPOYear =  '$poYear' AND 
				tb1.intItem =  '$itemId' 
				group by mst_item.intId";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			
			$poQty=$row['dblQty'];
			$confirmedInvQty=$row['dblInvoiceQty'];
			$retSupQty=$row['dblRetunSupplierQty'];
			$invBal=$poQty-$confirmedInvQty+$retSupQty;
			
		return $invBal;
	}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery2($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//--------------------------------------------------------

?>

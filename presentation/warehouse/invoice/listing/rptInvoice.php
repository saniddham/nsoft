<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
session_start();
 $companyId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
 
$invoiceNo = $_REQUEST['invoiceNo'];
$year = $_REQUEST['year'];
$approveMode = $_REQUEST['approveMode'];

$programName='Invoice';
$programCode='P0484';
 
/*$gatePassNo = '100000';
$year = '2012';
$approveMode=1;
*/
   $sql = "select 
tb1.intPONo,
tb1.intPOYear,
tb1.strNote,
tb1.intStatus,
tb1.datdate,
tb1.intApproveLevels,
tb1.intUser,
tb1.invoiceRaisedLocationId,
tb1.poRaisedCompanyId, 
tb1. delToCompany,
tb1.strUserName, 
mst_companies.strName AS invFrmCompany  
    from (SELECT 
ware_invoiceheader.intPONo,
ware_invoiceheader.intPOYear,
ware_invoiceheader.strNote,
ware_invoiceheader.intStatus,
ware_invoiceheader.datdate,
ware_invoiceheader.intApproveLevels,
ware_invoiceheader.intUser,
ware_invoiceheader.intCompanyId AS invoiceRaisedLocationId,
mst_supplier.intCompanyId as poRaisedCompanyId, 
mst_companies.strName as delToCompany,
sys_users.strUserName
FROM
ware_invoiceheader  
Inner Join trn_poheader ON ware_invoiceheader.intPONo = trn_poheader.intPONo AND ware_invoiceheader.intPOYear = trn_poheader.intPOYear 
Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
Inner Join mst_companies ON mst_supplier.intCompanyId = mst_companies.intId
Inner Join sys_users ON ware_invoiceheader.intUser = sys_users.intUserId
WHERE
ware_invoiceheader.intInvoiceNo =  '$invoiceNo' AND
ware_invoiceheader.intInvoiceYear =  '$year' 
GROUP BY
ware_invoiceheader.intInvoiceNo,
ware_invoiceheader.intInvoiceYear) as tb1  
Inner Join mst_locations ON tb1.invoiceRaisedLocationId = mst_locations.intId
Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$poNo = $row['intPONo'];
					$poYear = $row['intPOYear'];
					$company = $row['delToCompany'];
					$locationId = $row['invoiceRaisedLocationId'];//this locationId use in report header(reportHeader.php)--------------------
					$note=$row['strNote'];
					$invDate = $row['datdate'];
					$invBy = $row['strUserName'];
					$intStatus = $row['intStatus'];
					$savedLevels = $row['intApproveLevels'];
					$user = $row['strUserName'];
					$invByCompany = $row['invFrmCompany'];
					$invToCompany = $row['delToCompany'];
					$createUserId= $row['intUser'];
				 }
				 
$confirmationMode=loadConfirmatonMode($programCode,$intStatus,$savedLevels,$intUser);
$rejectionMode=loadRejectionMode($programCode,$intStatus,$savedLevels,$intUser);
?>
 <head>
 <title>Invoice (Loan In Settlement) Report</title>
 <script type="application/javascript" src="presentation/warehouse/invoice/listing/rptInvoice-js.js"></script>
 <style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:282px;
	top:172px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="images/pending.png"  /></div>
<?php
}
?>
<form id="frmInvoiceReport" name="frmInvoiceReport" method="post" action="rptInvoice.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>INVOICE (LOAN IN SETTLEMENT) REPORT</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
    <?php
	if($intStatus>1)
	{
	?>
    <?php if($approveMode==1){
			if($confirmationMode==1) { 
			?>
			<img src="images/approve.png" align="middle" class="mouseover noPrint" id="imgApprove" />
			<?php
			}
			if($rejectionMode==1) { 
			?>
            <img src="images/reject.png" align="middle" class="mouseover noPrint" id="imgReject" />
                <?php
                }
            }
	}
	?>
    </td>
  </tr>
  <tr>
  <?php
 	if($intStatus==1)
	{
	?>
   <td colspan="9" class="APPROVE" >CONFIRMED</td>
   <?PHP
	}
	else if($intStatus==0)
	{
   ?>
   <td colspan="9" class="APPROVE" style="color:#F00">REJECTED</td>
   <?php
	}
	else
	{
   ?>
   <td width="10%" colspan="9" class="APPROVE">PENDING</td>
   <?php
	}
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="10%"><span class="normalfnt"><strong>Invoice No</strong></span></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="27%"><span class="normalfnt"><?php echo $invoiceNo ?>/<?php echo $year ?></span></td>
    <td width="9%" class="normalfnt"><strong>Invoice From</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="28%"><span class="normalfnt"><?php echo $invByCompany."<strong> To: </strong>".$invToCompany ?></span></td>
    <td width="1%"><div id="divInvNo" style="display:none"><?php echo $invoiceNo ?>/<?php echo $year ?></div></td>
  <td width="3%"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Invoice  By</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $invBy  ?></span></td>
    <td><span class="normalfnt"><strong>PO No</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $poNo."/".$poYear ?></span></td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Date</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $invDate  ?></span></td>
    <td class="normalfnt"><strong>Note</strong></td>
    <td align="center"  valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $note  ?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr >
              <th width="14%" height="14" >Main Category</th>
              <th width="17%" >Sub Category</th>
              <th width="11%" >Item Code</th>
              <th width="37%" >Item</th>
              <th width="7%" >UOM</th>
              <th width="14%" >Qty</th>
              </tr>
              <?php 
	  	   $sql1 = "SELECT
ware_invoicedetails.intInvoiceNo,
ware_invoicedetails.intInvoiceYear,
ware_invoicedetails.intItemId,
Sum(ware_invoicedetails.dblQty) as dblQty,
mst_item.strCode,
mst_item.strCode as SUP_ITEM_CODE,
mst_item.strName,
mst_item.intUOM, 
mst_units.strCode as uom , 
mst_maincategory.strName AS mainCategory,
mst_subcategory.strName AS subCategory
FROM ware_invoicedetails
Inner Join mst_item ON ware_invoicedetails.intItemId = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
WHERE
ware_invoicedetails.intInvoiceNo =  '$invoiceNo' AND
ware_invoicedetails.intInvoiceYear =  '$year' 
GROUP BY
ware_invoicedetails.intItemId 
";
                 //  echo $sql1;
			$result1 = $db->RunQuery($sql1);
			$totQty=0;
			$totAmmount=0;
			while($row=mysqli_fetch_array($result1))
			{
                           
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" >&nbsp;<?php echo $row['mainCategory']?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['subCategory'] ?>&nbsp;</td>
              <?php
              if($row['SUP_ITEM_CODE']!=null)
              {
               ?>
               <td class="normalfnt" >&nbsp;<?php echo $row['SUP_ITEM_CODE']?>&nbsp;</td>
              <?php
              }else{
               ?>   
              
              <td class="normalfnt" >&nbsp;<?php echo $row['strCode'] ?>&nbsp;</td>
              <?php
              }
              ?>
              <td class="normalfnt" >&nbsp;<?php echo $row['strName'] ?>&nbsp;</td>
              <td class="normalfntMid" ><?php echo $row['uom'] ?></td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblQty'] ?>&nbsp;</td>
              </tr>
      <?php 
			$totQty+=$row['dblQty'];
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $totQty ?>&nbsp;</td>
              </tr>
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<?php  
					   $flag=0;
					   $sqlc = "SELECT
							ware_invoiceheader_approvedby.intApproveUser,
							ware_invoiceheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName, 
							ware_invoiceheader_approvedby.intApproveLevelNo
							FROM
							ware_invoiceheader_approvedby
							Inner Join sys_users ON ware_invoiceheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_invoiceheader_approvedby.intInvoiceNo =  '$invoiceNo' AND
							ware_invoiceheader_approvedby.intYear =  '$year'  order by dtApprovedDate asc
";
					 $resultc = $db->RunQuery($sqlc);
					while($rowc=mysqli_fetch_array($resultc)){
						if($rowc['intApproveLevelNo']==1)
						$desc="1st ";
						else if($rowc['intApproveLevelNo']==2)
						$desc="2nd ";
						else if($rowc['intApproveLevelNo']==3)
						$desc="3rd ";
						else
						$desc=$rowc['intApproveLevelNo']."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
					 
					 if($rowc['intApproveLevelNo']>0){
						 $flag=1;
				?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
            </tr>
<?php
	//}
	//else{			
					}
					 if($rowc['intApproveLevelNo']==0){
						 $flag=2;
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Rejected By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
					 }
					 if($rowc['intApproveLevelNo']==-1){
						 $flag=3;
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Revised By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
					 }
					 
		 if($rowc['intApproveLevelNo']>0){
			 $finalSavedLevel=$rowc['intApproveLevelNo'];
		 }
	 }//end of while
	
	
	//echo $flag;
	if($flag>1){
		$finalSavedLevel=0;
	}
	
	if(($flag<=1) || ($intStatus>0)){
	for($j=$finalSavedLevel+1; $j<=$savedLevels; $j++)
	{ 
		if($j==1)
		$desc="1st ";
		else if($j==2)
		$desc="2nd ";
		else if($j==3)
		$desc="3rd ";
		else
		$desc=$j."th ";
		$desc2='---------------------------------';
	?>
        <tr>
            <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
        </tr>
<?php
	}
	}
?>
<tr height="110" >
  <td align="center" class="normalfntMid"></td>
</tr>
<tr height="40">
<?php
	//$locationId = '';
	$sql = "SELECT mst_locations.intCompanyId FROM mst_locations WHERE mst_locations.intId =  '$locationId'";
	$result 	= $db->RunQuery($sql);
	$row		= mysqli_fetch_array($result);
	$createCompanyId = $row['intCompanyId'];
?>
<?Php 
	$url  = "{$backwardseperator}presentation/sendToApproval.php";		// * file name
	$url .= "?status=$intStatus";										// * set recent status
	$url .= "&approveLevels=$savedLevels";								// * set approve levels in order header
	$url .= "&programCode=$programCode";								// * program code (ex:P10001)
	$url .= "&program=INVOICE(LOAN IN SETTELEMENT)";									// * program name (ex:Purchase Order)
	$url .= "&companyId=$createCompanyId";									// * created company id
	$url .= "&createUserId=$createUserId";	
	
	$url .= "&field1=Invoice No";												 
	$url .= "&field2=Invoice Year";	
	$url .= "&value1=$invoiceNo";												 
	$url .= "&value2=$year";	
	
	$url .= "&subject=INVOICE(LOAN IN SETTELEMENT) FOR APPROVAL ('$gatePassNo'/'$year')";	
	
	$url .= "&statement1=Please Approve this";	
	$url .= "&statement2=to Approve this";	
							// * doc year
	$url .= "&link=".urlencode(base64_encode($mainPath."presentation/warehouse/invoice/listing/rptInvoice.php?invoiceNo=$invoiceNo&year=$year&approveMode=1"));
?>
  <td align="center" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:150px;border:none"  ></iframe></td>
</tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
	
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//-------------------------------------------------------------------------
?>	

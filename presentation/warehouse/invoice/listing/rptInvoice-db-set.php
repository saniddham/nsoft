<?php 

	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];

	include "{$backwardseperator}dataAccess/Connector.php";
	include "{$backwardseperator}libraries/mail/mail.php";
	require_once $backwardseperator."class/cls_mail.php";
	
	$objMail = new cls_create_mail($db);
	
	$programName='Invoice';
	$programCode='P0484';
	$issueApproveLevel = (int)getApproveLevel($programName);
	
	/////////// parameters /////////////////////////////

	
	$invoiceNo = $_REQUEST['invoiceNo'];
	$year = $_REQUEST['year'];
	
	if($_REQUEST['status']=='approve')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
		$sql = "UPDATE `ware_invoiceheader` SET `intStatus`=intStatus-1 WHERE (intInvoiceNo='$invoiceNo') AND (`intInvoiceYear`='$year') and intStatus not in(0,1)";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Approval error!";
		}
		
		if($result){
			$sql = "SELECT  ware_invoiceheader.intUser, 
			ware_invoiceheader.intStatus, 
			ware_invoiceheader.intApproveLevels,
			ware_invoiceheader.intCompanyId as location,
			mst_locations.intCompanyId as companyId 
			FROM
			ware_invoiceheader
			Inner Join mst_locations ON ware_invoiceheader.intCompanyId = mst_locations.intId
			WHERE (intInvoiceNo='$invoiceNo') AND (`intInvoiceYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$createdUser = $row['intUser'];
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			$company = $row['companyId'];
			$location = $row['location'];
			
			$approveLevel=$savedLevels+1-$status;
			$sqlI = "INSERT INTO `ware_invoiceheader_approvedby` (`intInvoiceNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
				VALUES ('$invoiceNo','$year','$approveLevel','$userId',now(),0)";
			$resultI = $db->RunQuery2($sqlI);
			if((!$resultI) && ($rollBackFlag!=1)){
				$rollBackFlag=1;
				$sqlM=$sqlI;
				$rollBackMsg = "Approval error!";
			}
			
			if($status==1)
			{
				if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
				////////////// send mail to creater //////////////////
				sendFinlConfirmedEmailToUser($invoiceNo,$year,$objMail,$mainPath,$root_path);
				}
			}
		//--------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
				$sql1 = "SELECT 
 						ware_invoicedetails.dblQty,
						ware_invoicedetails.intLocation,   
						ware_invoicedetails.intItemId, 
						mst_item.strName,  
						mst_item.intBomItem 
						FROM 
						ware_invoicedetails 
						Inner Join mst_item ON ware_invoicedetails.intItemId = mst_item.intId
						WHERE
						ware_invoicedetails.intInvoiceNo =  '$invoiceNo' AND
						ware_invoicedetails.intInvoiceYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				$toSavedQty=0;
				$transSavedQty=0;
				while($row=mysqli_fetch_array($result1))
				{
					$toSavedQty+=round($row['dblQty'],4);
					//$invlocationId=$row['intLocation'];
					$item=$row['intItemId'];
					$itemName=$row['strName'];
					$Qty=$row['dblQty'];
					
						$resultG = getGrnWiseStockBalance_bulk($location,$item);
						while($rowG=mysqli_fetch_array($resultG)){//save grn wise items
						
					//	echo "</br>".$itemName."=".$Qty;

								if(($Qty>0) && ($rowG['stockBal']>0)){
								if($rowG['stockBal']>=$Qty){
									$saveQty=$Qty;
									$Qty=0;
									}
									else if($Qty>$rowG['stockBal']){
									$saveQty=$rowG['stockBal'];
									$Qty=$Qty-$saveQty;
									}
									$grnNo=$rowG['intGRNNo'];
									$grnYear=$rowG['intGRNYear'];
									$grnDate=$rowG['dtGRNDate'];
									$grnRate=$rowG['dblGRNRate'];	
									$currency=loadCurrency($grnNo,$grnYear,$item);
									$saveQty = round($saveQty,4);
									if($saveQty>0){	
									$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
										VALUES ('$company','$location','$invoiceNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','-$saveQty','INVOICE','$userId',now())";
									$resultI = $db->RunQuery2($sqlI);
										if($resultI){
											$transSavedQty+=$saveQty;
										}
										if((!$resultI) && ($rollBackFlag!=1)){
											$rollBackFlag=1;
											$sqlM=$sqlI;
											$rollBackMsg = "Approval error!";
										}
									
									}
								}
					}
				}
			}
		//--------------------------------------------------------------------
			if($status==$savedLevels){//if first confirmation raised,requisition table shld update
			}
		//---------------------------------------------------------------------------
		}
		
		$toSavedQty=round($toSavedQty,4);
		$transSavedQty=round($transSavedQty,4);
		
		if(($toSavedQty!=$transSavedQty) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$rollBackMsg = "Approval Error";
			$sqlM=$toSavedQty."-".$transSavedQty;
		}
		
		if(($rollBackFlag==0) &&($status==1)){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Final Approval raised sucessfully";
			$response['q'] 			= $sql;
		}
		else if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Approval raised sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		//----------------------------------------
			echo json_encode($response);
		//-----------------------------------------	
		
	}
	
	else if($_REQUEST['status']=='reject')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
		//$grnApproveLevel = (int)getApproveLevel($programName);
		$sql = "SELECT  ware_invoiceheader.intUser, ware_invoiceheader.intStatus,ware_invoiceheader.intApproveLevels FROM ware_invoiceheader WHERE  (intInvoiceNo='$invoiceNo') AND (`intInvoiceYear`='$year')";
		$results = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($results);
		$createdUser = $row['intUser'];
		$status = $row['intStatus'];
		$savedLevels = $row['intApproveLevels'];
		
		$sql = "UPDATE `ware_invoiceheader` SET `intStatus`=0 WHERE (intInvoiceNo='$invoiceNo') AND (`intInvoiceYear`='$year')";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Rejection error!";
		}
		
		//$sql = "DELETE FROM `ware_invoiceheader_approvedby` WHERE (`intInvoiceNo`='$invoiceNo') AND (`intYear`='$year')";
		//$result2 = $db->RunQuery($sql);
		
		$sqlI = "INSERT INTO `ware_invoiceheader_approvedby` (`intInvoiceNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$invoiceNo','$year','0','$userId',now(),0)";
		$resultI = $db->RunQuery2($sqlI);
		if((!$resultI) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sqlI;
			$rollBackMsg = "Rejection error!";
		}
		
		if($resultI)
		{
			if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
			sendRejecedEmailToUser($invoiceNo,$year,$objMail,$mainPath,$root_path);
			}
		}
		//---------------------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
/*				$sql1 = "SELECT 
						ware_invoicedetails.intLocation,
						ware_invoicedetails.intItemId,
						ware_invoicedetails.dblQty,
						mst_item.intBomItem 
						FROM 
						ware_invoicedetails 
						Inner Join mst_item ON ware_invoicedetails.intItemId = mst_item.intId
						WHERE
						ware_invoicedetails.intInvoiceNo =  '$invoiceNo' AND
						ware_invoicedetails.intInvoiceYear =  '$year'";
				$result1 = $db->RunQuery($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$invlocationId=$row['intLocation'];
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					$Qty=round($Qty,4);
					$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intItemId,dblQty,strType,intUser,dtDate) 
						VALUES ('$company','$invlocationId','$invoiceNo','$year','$item','$Qty','CINVOICE','$userId',now())";
					$resultI = $db->RunQuery($sqlI);
				}
*/			}
		//------------------------------------
		if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Rejection raised sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		//----------------------------------------
			echo json_encode($response);
		//-----------------------------------------	
		
	}
//--------------------------------------------------------------
function getGrnWiseStockBalance_bulk($location,$item)
{
	
	global $db;
	  $sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) AS stockBal,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear,
			ware_stocktransactions_bulk.dtGRNDate,
			ware_stocktransactions_bulk.dblGRNRate
			FROM ware_stocktransactions_bulk
			WHERE
			ware_stocktransactions_bulk.intItemId =  '$item' AND
			ware_stocktransactions_bulk.intLocationId =  '$location'
			GROUP BY
			ware_stocktransactions_bulk.intItemId,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear
			ORDER BY
			ware_stocktransactions_bulk.dtGRNDate ASC";

	$result = $db->RunQuery2($sql);
	return $result;	
}
//--------------------------------------------------------------
function loadCurrency($grnNo,$grnYear,$item)
{
	global $db;
	
	if(($grnNo!=0) && ($grnYear!=0)){
	  $sql = "SELECT
			trn_poheader.intCurrency as currency 
			FROM
			ware_grnheader
			Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
			WHERE
			ware_grnheader.intGrnNo =  '$grnNo' AND
			ware_grnheader.intGrnYear =  '$grnYear'";
	}
	else{
	$sql = "SELECT
			mst_item.intCurrency as currency 
			FROM mst_item
			WHERE
			mst_item.intId =  '$item'";
	}

	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);	
	return $row['currency'];;	
}
//--------------------------------------------------------
function sendFinlConfirmedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_invoiceheader
			Inner Join sys_users ON ware_invoiceheader.intUser = sys_users.intUserId
			WHERE
			ware_invoiceheader.intInvoiceNo =  '$serialNo' AND
			ware_invoiceheader.intInvoiceYear =  '$year'";

				
		 $result = $db->RunQuery2($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="CONFIRMED INVOICE(LOAN IN SETTELEMENT) ('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='INVOICE(LOAN IN SETTELEMENT)';
			$_REQUEST['field1']='Invoice No';
			$_REQUEST['field2']='Invoice Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="CONFIRMED INVOICE(LOAN IN SETTELEMENT) ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Approved this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/invoice/listing/rptInvoice.php?invoiceNo=$serialNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
function sendRejecedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_invoiceheader
			Inner Join sys_users ON ware_invoiceheader.intUser = sys_users.intUserId
			WHERE
			ware_invoiceheader.intInvoiceNo =  '$serialNo' AND
			ware_invoiceheader.intInvoiceYear =  '$year'";

				
		 $result = $db->RunQuery2($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="REJECTED INVOICE(LOAN IN SETTELEMENT) ('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='INVOICE(LOAN IN SETTELEMENT)';
			$_REQUEST['field1']='Invoice No';
			$_REQUEST['field2']='Invoice Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="REJECTED INVOICE(LOAN IN SETTELEMENT) ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Rejected this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/invoice/listing/rptInvoice.php?invoiceNo=$serialNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
?>
<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];

include $backwardseperator."dataAccess/Connector.php";

$po  = $_REQUEST['po'];
$year  = $_REQUEST['year'];
$itemId  = $_REQUEST['itemId'];
$itemName  = $_REQUEST['itemName'];
$balToInvQty  = $_REQUEST['balToInvQty'];
$stockQty  = $_REQUEST['stockQty'];

if($stockQty>$balToInvQty){
	$maxQty=$balToInvQty;
}
else{
	$maxQty=$stockQty;
}
//----------------------------------
$sql = "SELECT
		mst_item.strName
		FROM mst_item
		WHERE
		mst_item.intId =  '$itemId'";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$itemName=$row['strName'];
//---------------------------------------
$sql = "SELECT
		mst_supplier.intInterLocation,
		mst_supplier.intCompanyId
		FROM
		trn_poheader
		Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
		WHERE
		trn_poheader.intPONo =  '$po' AND
		trn_poheader.intPOYear =  '$year'";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$intLocation=$row['intInterLocation'];
$intCompany=$row['intCompanyId'];
if(($intCompany!=$company) || ($intLocation!=$location)){
	$company='';
	$location='';
}
//-------------------------------------
?>
<title>Items</title>

</head>

<body>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>

    
<form id="frmInvoicePopup" name="frmInvoicePopup" method="post" action="">
<table width="500" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutD" style="width:510">
		  <div class="trans_text"> Item Allocation</div>
		  <table width="400" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="350" border="0">
      <tr>
        <td><table width="350" border="0" cellpadding="0" cellspacing="0" class="tableBorder">
<tr>
            <td width="7%" height="22" class="normalfnt">&nbsp;</td>
            <td width="31%" class="normalfnt">Item </td>
            <td width="3%" class="normalfnt">: </td>
            <td width="56%" class="normalfnt"><div id="item"><?php echo $itemName; ?></div></td>
            <td width="1%" align="right">&nbsp;</td>
            <td width="2%" align="right"><div id="itemId" style="display:none"><?php echo $itemId; ?></div></td>
            </tr>  
            <tr id="rw1">
            <td width="7%" height="22" class="normalfnt">&nbsp;</td>
            <td width="31%" class="normalfnt">Bal To Invoice Qty</td>
            <td width="3%" class="normalfnt">: </td>
            <td width="56%" class="normalfnt"><div id="balToInvoice"><?php echo $balToInvQty; ?></div></td>
            <td width="1%" class="normalfnt"></td>
            <td width="2%" align="right">&nbsp;</td>
            </tr>
<tr id="rw2">
            <td width="7%" height="22" class="normalfnt">&nbsp;</td>
            <td width="31%" class="normalfnt">Stock Qty</td>
            <td width="3%" class="normalfnt">: </td>
            <td width="56%" class="normalfnt"><div id="stockQty"><?php echo $stockQty; ?></div></td>
            <td width="1%" class="normalfnt"><input type="text" id="txtPopGrnNo" value="<?php echo $grnNo; ?>" style="display:none" /></td>
            <td width="2%" align="right">&nbsp;</td>
            </tr>         
            
          
          </table></td>
      </tr>
      <?php //echo $pp
	  ?>
      
      <tr>
        <td><div style="width:420px;height:300px;overflow:scroll" >
          <table width="400" class="bordered" id="tblItemsPopup" >
            <tr >
              <th width="46%" height="22" >Location</th>
              <th width="30%" >Qty</th>
              <th width="24%">Stock Bal</th>
              </tr>
              <?php

					 $sql = "SELECT
							mst_locations.strName,
							mst_locations.intId, 
							Sum(ware_stocktransactions_bulk.dblQty) as stkQty 
							FROM
							ware_stocktransactions_bulk
							Inner Join mst_locations ON ware_stocktransactions_bulk.intLocationId = mst_locations.intId
							WHERE
							ware_stocktransactions_bulk.intCompanyId =  '$company' AND 
							ware_stocktransactions_bulk.intLocationId =  '$location' AND 
							ware_stocktransactions_bulk.intItemId =  '$itemId'
							GROUP BY
							mst_locations.strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$location=$row['strName'];
						$locationId=$row['intId'];
						$stkQty=$row['stkQty'];
					}
			  ?>
                  <tr class="normalfnt" >
                  <td width="46%" height="22" class="normalfnt"  bgcolor="#FFFFFF" id="<?php echo $locationId ; ?>" ><?php echo $location ; ?></td>
                  <td width="30%" bgcolor="#FFFFFF"  ><input type="text" id="txtQty" value="0" style="width:100; text-align:right"  class="validate[required,custom[number],max[<?php echo $stkQty ?>]] locQty" /></td>
                  <td width="24%" class="normalfntRight"  bgcolor="#FFFFFF" align="right"><?php echo $stkQty ; ?></td>
                  </tr>
              <?php
			  
			  ?>
            </table>
          </div></td>
      </tr>
        <td align="center" class="tableBorder_allRound"><img src="images/Tadd.jpg" width="92" height="24"  alt="add" id="butAdd" name="butAdd" class="mouseover"/><img src="planning/img/Tclose.jpg" width="92" height="24" id="butClose1" name="butClose1" class="mouseover"  /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>

</body>
</html>

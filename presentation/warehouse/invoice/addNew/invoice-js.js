var basePath	="presentation/warehouse/invoice/addNew/";
var reportId	="909";

$(document).ready(function() {
	
  		$("#frmInvoice").validationEngine();
		
		
		$("#frmInvoice #cboPONo").change(function(){
			clearRows();
			loadDetails();
		});
		$("#frmInvoice #cboPOYear").change(function(){
			loadPONos();
			clearRows();
			loadDetails();
		});
		$("#frmInvoice #butAllocate").click(function(){
			loadLocationWiseStocks(this);
		});
		
	//--------------------------------------------
		
   
/* //-------------------------------------------- 
  $('#frmInvoice #cboPONo').change(function(){
	    var po = $('#cboPONo').val();
	    var year = $('#cboPOYear').val();
		var url 		= "invoice-db-get.php?requestType=loadExchangeRate";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"po="+po+"&year="+year,
			async:false,
			success:function(json){

					document.getElementById("divExchRate").innerHTML=json.excRate;
			}
		});
		
  });
 //-------------------------------------------- 
  $('#frmInvoice #cboPOYear').change(function(){
	    var po = $('#cboPONo').val();
	    var year = $('#cboPOYear').val();
		var url 		= "invoice-db-get.php?requestType=loadExchangeRate";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"po="+po+"&year="+year,
			async:false,
			success:function(json){

					document.getElementById("divExchRate").innerHTML=json.excRate;
			}
		});
		
  });
*/  //-------------------------------------------------------
   $('#frmInvoice #cboSupplier').change(function(){
	    var supplier = $('#cboSupplier').val();
		var url 		= basePath+"invoice-db-get.php?requestType=loadSupplierDetails";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"supplier="+supplier,
			async:false,
			success:function(json){

					$('#frmInvoice #cboCurrency').val(json.currency);
					$('#frmInvoice #cboPayTerm').val(json.payTerm);
					$('#frmInvoice #cboPayMode').val(json.payMode);
					$('#frmInvoice #cboShipmentMode').val(json.shipmentMode);
					document.getElementById("divExchRate").innerHTML=json.excRate;
			}
		});
		
  });
  //-------------------------------------------------------
 
  $('#frmInvoice #butSave').click(function(){
	var requestType = '';
	if ($('#frmInvoice').validationEngine('validate'))   
    { 

		var data = "requestType=save";
/*		
			data+="&serialNo="		+	$('#txtInvoiceNo').val();
			data+="&Year="	+	$('#txtYear').val();
			data+="&PoNo="	+	$('#cboPONo').val();
			data+="&PoYear="	+	$('#cboPOYear').val();
			data+="&note="	+	$('#txtNote').val();
			data+="&date="			+	$('#dtDate').val();
*/
		var arrHeader = "{";
							arrHeader += '"serialNo":"'+$('#txtInvoiceNo').val()+'",' ;
							arrHeader += '"Year":"'+$('#txtYear').val()+'",' ;
							arrHeader += '"PoNo":"'+$('#cboPONo').val()+'",' ;
							arrHeader += '"PoYear":"'+$('#cboPOYear').val()+'",' ;
							arrHeader += '"note":'+URLEncode_json($('#txtNote').val())+',' ;
							arrHeader += '"date":"'+$('#dtDate').val()+'"' ;
			arrHeader += ' }';
		
		
		
			var rowCount = document.getElementById('tblInvoice').rows.length;
			if(rowCount==1){
				alert("items not selected to Invoice");
				return false;				
			}
			var row = 0;
			var errorFlag=0;
			
			var arr="[";
			var totalQty = 0;
			$('#tblInvoice .item').each(function(){
				
				var itemId	= $(this).attr('id');
				var itemName	= $(this).html();
				var Qty	= $(this).parent().find(".invoQty").val();
				totalQty +=parseFloat((Qty==''?0:Qty));
					if(Qty>0){
				        arr += "{";
						arr += '"itemId":"'+	itemId +'",' ;
						arr += '"itemName":"'+	URLEncode(itemName) +'",' ;
						arr += '"Qty":"'+		Qty +'"' ;
						arr +=  '},';
					}
					else{
						errorFlag=1;
					}
			});
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
		
			data+="&arrHeader="	+	arrHeader;
			data+="&arr="	+	arr;
			
			if(!totalQty>0){
				alert("Plese enter qty at least one item.");
				return false;
			}

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basePath+"invoice-db-set.php";
     	var obj = $.ajax({
			url:url,
			type:'post',
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmInvoice #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						//loadCombo_frmSampleInfomations();
						var t=setTimeout("alertx()",1000);
						$('#txtInvoiceNo').val(json.serialNo);
						$('#txtYear').val(json.year);
						if(json.confirmationMode==1){
							$('#butConfirm').show();
						}
						else{
							$('#butConfirm').hide();
						}
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmInvoice #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
	
	//--------------refresh the form----------
	$('#frmInvoice #butNew').click(function(){
		window.location.href = "?q=484";
		$('#frmInvoice').get(0).reset();
		clearRows();
		$('#frmInvoice #txtGrnNo').val('');
		$('#frmInvoice #txtGrnYear').val('');
		$('#frmInvoice #txtInvioceNo').val('');
		$('#frmInvoice #cboPO').val('');
		$('#frmInvoice #txtDeliveryNo').val('');
		$('#frmInvoice #txtTotGrnAmmount').val('');
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmInvoice #dtDate').val(d);
	});
	//----------------------------------------
	
/*    $('#frmInvoice #butDelete').click(function(){
		if($('#frmInvoice #cboSearch').val()=='')
		{
			$('#frmInvoice #butDelete').validationEngine('showPrompt', 'Please select Type.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmInvoice #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "typeOfPrint-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmInvoice #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmInvoice #butDelete').validationEngine('showPrompt', json.msg,json.type );
												
												if(json.type=='pass')
												{
													$('#frmInvoice').get(0).reset();
													loadCombo_frmInvoice();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
	
*/	
//----------------------------	
$('.delImg').click(function(){
	$(this).parent().parent().remove();
});

//-----------------------------------
$('#frmInvoice #butReport').click(function(){
	if($('#txtPoNo').val()!=''){
		window.open('?q='+reportId+'&invoiceNo='+$('#txtInvoiceNo').val()+'&year='+$('#txtYear').val());	
	}
	else{
		alert("There is no Invoice No to view");
	}
});
//----------------------------------	
$('#frmInvoice #butConfirm').click(function(){
	if($('#txtPoNo').val()!=''){
		window.open('?q='+reportId+'&invoiceNo='+$('#txtInvoiceNo').val()+'&year='+$('#txtYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no Invoice No to confirm");
	}
});
//-----------------------------------------------------
$('#frmInvoice #butClose').click(function(){
		//load('main.php');	
});
//--------------refresh the form-----------------------
	$('#frmInvoice #butNew').click(function(){
		window.location.href = "?q=484";
		$('#frmInvoice').get(0).reset();
		clearRows();
		$('#frmInvoice #txtGrnNo').val('');
		$('#frmInvoice #txtGrnYear').val('');
		$('#frmInvoice #txtInvioceNo').val('');
		$('#frmInvoice #cboPO').val('');
		$('#frmInvoice #cboPO').focus();
		$('#frmInvoice #txtDeliveryNo').val('');
		$('#frmInvoice #txtTotGrnAmmount').val('');
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmInvoice #dtDate').val(d);
	});
//-----------------------------------------------------
});

//----------end of ready -------------------------------
//-------------------------------------
function alertx()
{
	$('#frmInvoice #butSave').validationEngine('hide')	;
}
//----------------------------------------------- 
function alertDelete()
{
	$('#frmInvoice #butDelete').validationEngine('hide')	;
}
//----------------------------------------------- 
function closePopUp(){
	
}
//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblInvoice').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblInvoice').deleteRow(1);
			
	}
}
//----------------------------------------------- 
function loadDetails(){
	    var po = $('#cboPONo').val();
	    var year = $('#cboPOYear').val();
		var url 		= basePath+"invoice-db-get.php?requestType=loadDetails";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"po="+po+"&year="+year,
			async:false,
			success:function(json){

						var length = json.arrCombo.length;
						var arrCombo = json.arrCombo;
						document.getElementById("cboGatePassTo").value=arrCombo[0]['gpTo'];
						//alert(arrCombo[0]['gpTo']);
						for(var i=0;i<length;i++)
						{
								var itemId=arrCombo[i]['itemId'];	
								var maincatId=arrCombo[i]['maincatId'];	
								var subCatId=arrCombo[i]['subCatId'];	
								var itemName=arrCombo[i]['itemName'];	
								var mainCatName=arrCombo[i]['mainCatName'];
								var subCatName=arrCombo[i]['subCatName'];	
								var uom=arrCombo[i]['uom'];	
								var poQty=arrCombo[i]['poQty'];
								var grnQty=arrCombo[i]['grnQty'];
								var retQty=arrCombo[i]['retSupQty'];
								var invoiceQty=arrCombo[i]['invoiceQty'];
								var stockQty=arrCombo[i]['stockQty'];
								var poBal=parseFloat(poQty)-parseFloat(invoiceQty)+parseFloat(retQty);
								poBal	= Math.round(poBal * 10000) / 10000
								
								var maxQty=0;
								if(poBal>stockQty){
									maxQty=stockQty
								}
								else{
									maxQty=poBal
								}
								maxQty	= Math.round(maxQty * 10000) / 10000
									
								var content='<tr class="normalfnt">';
								content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'">'+mainCatName+'</td>';
								content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'">'+subCatName+'</td>';
								content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'"  class="item">'+itemName+'</td>';
								content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'"  class="uom">'+uom+'</td>';
								content +='<td align="center" bgcolor="#FFFFFF" id="'+poQty+'" >'+poQty+'</td>';
								content +='<td align="center" bgcolor="#FFFFFF" id="'+poBal+'" class="balToInvQty">'+poBal+'</td>';
								content +='<td align="center" bgcolor="#FFFFFF" id="'+stockQty+'" class="stockQty">'+stockQty+'</td>';
								content +='<td align="center" bgcolor="#FFFFFF" id=""  ><input id="'+maxQty+'" class="validate[required,custom[number],max['+maxQty+']] invoQty" style="width:80px;text-align:center" type="text" value="0" /></td></tr>';
							
								if(poBal>0)
								add_new_row('#frmInvoice #tblInvoice',content);
						}
					
					
			}
		});
				$("#butAllocate").click(function(){
				loadLocationWiseStocks(this);
		});

}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function loadLocationWiseStocks(obj){
		popupWindow3('1');
	    var po = $('#cboPONo').val();
	    var year = $('#cboPOYear').val();
	 	var itemId = $(obj).parent().parent().find('.item').attr('id');
	 	var itemName = $(obj).parent().parent().find('.item').text();
	 	var balToInvQty =$(obj).parent().parent().find('.balToInvQty').attr('id');
	 	var stockQty =$(obj).parent().parent().find('.stockQty').attr('id');
		
		$('#popupContact1').load(basePath+'invoicePopup.php?itemId='+itemId+'&balToInvQty='+balToInvQty+'&stockQty='+stockQty+'&po='+po+'&year='+year,function(){
				selectAlreadySelected();
			    //-------------------------------------------------------
				$('#butAdd').click(function(){
					addClickedRows(obj);
				});	
				$('#butClose1').click(disablePopup);
			    //-------------------------------------------------------
		});	
}
//----------------------------------------------------
function addClickedRows(obj)
{
  		$("#frmInvoicePopup").validationEngine();
		var xx=validateQty();
		if(xx==false){
			return xx;
		}
		//return false;
	
	var rowCount = document.getElementById('tblItemsPopup').rows.length;
	var totQty=0;
	for(var i=1;i<rowCount;i++)
	{
		var itemId=document.getElementById('itemId').innerHTML;
		var itemName=document.getElementById('item').innerHTML;
		var locationId=document.getElementById('tblItemsPopup').rows[i].cells[0].id;
		var location=document.getElementById('tblItemsPopup').rows[i].cells[0].innerHTML;
		var Qty=parseFloat(document.getElementById('tblItemsPopup').rows[i].cells[1].childNodes[0].value);
	//	alert(location);
			if(Qty==''){
			 Qty=0;
			}
		totQty+=Qty;
		var content='<tr class="normalfnt">';
		content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+itemName+'</td>';
		content +='<td align="center" bgcolor="#FFFFFF" id="'+locationId+'">'+location+'</td>';
		content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+Qty+'</td>';
		content +='</tr>';
	//	alert(content);
		
		deleteDuplicate(itemId);
		if(Qty>0){
			
		add_new_row('#frmInvoice #tblLocationWiseallocations',content);
		}
	}
 //	$(obj).parent().parent().find('.invoQty').val(totQty);
	rw=obj.parentNode.parentNode.rowIndex;
	document.getElementById('tblInvoice').rows[rw].cells[6].childNodes[0].value=Qty;
	disablePopup();
	
}
//-------------------------------------
function validateQty(){
	
	var rowCount = document.getElementById('tblItemsPopup').rows.length;
	var qty=0;
	for(var i=1;i<rowCount;i++)
	{
		 qty += parseFloat(document.getElementById('tblItemsPopup').rows[i].cells[1].childNodes[0].value);
	}
	var stockQty=parseFloat(document.getElementById('stockQty').innerHTML)
	var balToInvQty=parseFloat(document.getElementById('balToInvoice').innerHTML)

	var maxQty=0;
	if(stockQty>balToInvQty){
		maxQty=balToInvQty;
	}
	else{
		maxQty=stockQty;
	}
	
	if(qty>maxQty){
		alert("Invalid Qty");
		return false;
	}
	else {
		return true;
	}
	
}
//--------------------------------------------
function deleteDuplicate(itemId){
	
	var rowCount = document.getElementById('tblLocationWiseallocations').rows.length;
	for(var i=1;i<rowCount;i++)
	{
		 if(document.getElementById('tblLocationWiseallocations').rows[i].cells[0].id==itemId){
			document.getElementById('tblLocationWiseallocations').deleteRow(i);
			rowCount--; 
		 }
	}
	
}
//------------------------------------------------------------
function selectAlreadySelected(){
	
	var rowCount = document.getElementById('tblLocationWiseallocations').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			var itemId = 	document.getElementById('tblLocationWiseallocations').rows[i].cells[0].id;
			var location = 	document.getElementById('tblLocationWiseallocations').rows[i].cells[1].id;
			var Qty = 	document.getElementById('tblLocationWiseallocations').rows[i].cells[2].innerHTML;
		
			var rowCount1 = document.getElementById('tblLocationWiseallocations').rows.length;
			for(var j=1;j<rowCount1;j++)
			{
				var itemIdP = 	document.getElementById('itemId').innerHTML;
				var locationP = 	document.getElementById('tblItemsPopup').rows[j].cells[0].id;
				
				if((location==locationP) && (itemId==itemIdP)){
					document.getElementById('tblItemsPopup').rows[j].cells[1].childNodes[0].value=Qty;
				}
			}
	}
}
//------------------------------------------------------------
function loadPONos(){
	var poYear = $('#cboPOYear').val();
	var url 		= basePath+"invoice-db-get.php?requestType=loadPONos&poYear="+poYear;
	var httpobj 	= $.ajax({url:url,async:false})
	document.getElementById('cboPONo').innerHTML=httpobj.responseText;
}
//------------------------------------------------------------

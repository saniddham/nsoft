<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
///////////////////////////////
// H B G KORALA   ////////////
//////////////////////////////
session_start();
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];
$intUser  = $_SESSION["userId"];

$invoiceNo = $_REQUEST['invoiceNo'];
$year = $_REQUEST['year'];

$programName='Invoice';
$programCode='P0484';

if(($invoiceNo=='')&&($year=='')){
	$savedStat = '';
	$intStatus=$savedStat+1;
	$date='';
	$poNo='';
	$poYear='';
	$compId='';
	$note='';
}
else{
	$result = loadHeader($invoiceNo,$year);
	while($row=mysqli_fetch_array($result))
	{
		$poNo = $row['intPONo'];
		$poYear = $row['intPOYear'];
		$compId = $row['compId'];
		$note=$row['strNote'];
		$date = $row['datdate'];
		$savedStat=$row['intApproveLevels'];
		$intStatus=$row['intStatus'];
	}
}
			 
$editMode=loadEditMode($programCode,$intStatus,$savedStat,$intUser);
$confirmatonMode=loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser);
if($invoiceNo==''){
	$confirmatonMode=0;	
}
$mainPage=$backwardseperator."main.php";
	
?>
 <head>
 <title>Invoice (Loan In Settlement)</title>
<!-- <script type="text/javascript" src="presentation/warehouse/invoice/addNew/invoice-js.js"></script>
--> </head>

<body>


<form id="frmInvoice" name="frmInvoice" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Invoice (Loan In Settlement)</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="22" class="normalfnt">Invoice No</td>
            <td width="22%"><input name="txtInvoiceNo" type="text" disabled="disabled" class="txtText" id="txtInvoiceNo" style="width:60px" value="<?php echo $invoiceNo ?>" /><input name="txtYear" type="text" disabled="disabled" class="txtText" id="txtYear" style="width:40px"  value="<?php echo $year ?>"/></td>
            <td width="31%">&nbsp;</td>
            <td width="8%" class="normalfnt">Date</span></td>
            <td width="1%" class="normalfnt">&nbsp;</td>
            <td width="28%" align="left"><input name="dtDate" type="text" value="<?php if($invoiceNo){ echo substr($date,0,10); }else { echo date("Y-m-d"); }?>" class="txtbox" id="dtDate" style="width:130px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
<tr>
            <td height="22" class="normalfnt">PO No</td>
            <td><select name="cboPOYear" id="cboPOYear" style="width:60px">
                  <?php
					$sql = "SELECT
							DISTINCT trn_poheader.intPOYear
							FROM
							trn_poheader
							Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
							WHERE
							mst_supplier.intInterCompany =  '1' AND
							mst_supplier.intCompanyId =  '$company' 
							Order by trn_poheader.intPOYear desc";
					$result = $db->RunQuery($sql);
					$i=0;

					while($row=mysqli_fetch_array($result))
					{
						if(($poYear=='') && ($i==0)){
							$poYear = $row['intPOYear'];
						}
						
						if($row['intPOYear']==$poYear)
						echo "<option value=\"".$row['intPOYear']."\" selected=\"selected\">".$row['intPOYear']."</option>";	
						else
						echo "<option value=\"".$row['intPOYear']."\">".$row['intPOYear']."</option>";	
						$i++;
					}
				?>
              </select>
              <select name="cboPONo" id="cboPONo" style="width:103px">
                  <option value=""></option>
                  <?php
					 $sql = "SELECT
							trn_poheader.intPONo,
							trn_poheader.intPOYear
							FROM
							trn_poheader
							Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
							WHERE
							mst_supplier.intInterCompany =  '1' AND
							mst_supplier.intCompanyId =  '$company' AND 
							trn_poheader.intPOYear =  '$poYear' AND 
							mst_supplier.intInterLocation =  '$location' 
							order by  trn_poheader.intPONo desc
							";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intPONo']==$poNo)
						echo "<option value=\"".$row['intPONo']."\" selected=\"selected\">".$row['intPONo']."</option>";	
						else
						echo "<option value=\"".$row['intPONo']."\">".$row['intPONo']."</option>";	
					}
				?>
            </select>
              </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td align="right">&nbsp;</td>
          </tr>        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="27" rowspan="2" class="normalfnt">Note</td>
            <td width="38%" rowspan="2"><textarea name="txtNote" id="txtNote" cols="35" rows="3" class="validate[required]"><?php echo $note; ?></textarea></td>
            <td width="15%">&nbsp;</td>
            <td width="9%" class="normalfnt" valign="top">Gate Pass To</td>
            <td width="28%" align="left" valign="top"><select style="width:240px" name="cboGatePassTo" id="cboGatePassTo"   class="validate[required]" disabled="disabled">
              <option value=""></option>
              <?php
					$sql = "SELECT
							mst_locations.intId,
							mst_locations.strName,
							mst_companies.strName as company 
							FROM
							mst_locations
							Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
							WHERE
							mst_locations.intStatus =  '1'
							ORDER BY
							mst_locations.strName ASC
							";
							$pp=$sql;
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$compId)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['company']."-".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['company']."-".$row['strName']."</option>";	
					}
				?>
              </select></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="92%">&nbsp;</td>
            <td width="8%"></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="bordered" id="tblInvoice" >
            <tr >
              <th width="11%" >Main Category</th>
              <th width="12%" >Sub Category</th>
              <th width="31%" >Item Description</th>
              <th width="5%">UOM</th>
              <th width="8%">PO Qty</th>
              <th width="14%">Bal to Invoice Qty</th>
              <th width="8%">Stock Qty</th>
              <th width="11%">Qty</th>
              </tr>
              <?php
			     $sql = "SELECT 
ware_invoiceheader.intPONo,
ware_invoiceheader.intPOYear,
ware_invoicedetails.intItemId,
mst_item.intMainCategory,
mst_item.strName AS item,
mst_item.intSubCategory,
mst_subcategory.strName AS subcat,
mst_maincategory.strName AS maincat,
ware_invoicedetails.intItemId,

IFNULL((SELECT
Sum(ware_invoicedetails.dblQty)
FROM
ware_invoiceheader
Inner Join ware_invoicedetails ON ware_invoiceheader.intInvoiceNo = ware_invoicedetails.intInvoiceNo AND ware_invoiceheader.intInvoiceYear = ware_invoicedetails.intInvoiceYear
WHERE
ware_invoiceheader.intPONo =  tb1.intPONo AND
ware_invoiceheader.intPOYear =  tb1.intPOYear AND
ware_invoicedetails.intItemId =  tb1.intItem AND
ware_invoiceheader.intStatus <=  ware_invoiceheader.intApproveLevels AND
ware_invoiceheader.intStatus >  '0'),0) as invoiceQty, 

IFNULL((SELECT
Sum(ware_returntosupplierdetails.dblQty)
FROM
ware_returntosupplierheader
Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear
Inner Join ware_grnheader ON ware_returntosupplierheader.intGrnNo = ware_grnheader.intGrnNo AND ware_returntosupplierheader.intGrnYear = ware_grnheader.intGrnYear
WHERE
ware_grnheader.intPoNo =  tb1.intPONo AND
ware_grnheader.intPoYear =  tb1.intPOYear AND
ware_returntosupplierdetails.intItemId =  tb1.intItem AND
ware_returntosupplierheader.intStatus = 1),0) as retSupQty,  

ware_invoicedetails.dblQty, 
tb1.dblQty as poQty , 
mst_units.strCode as uom  
FROM
ware_invoicedetails 
Inner Join ware_invoiceheader ON ware_invoicedetails.intInvoiceNo = ware_invoiceheader.intInvoiceNo AND ware_invoicedetails.intInvoiceYear = ware_invoiceheader.intInvoiceYear
Inner Join trn_poheader ON ware_invoiceheader.intPONo = trn_poheader.intPONo AND ware_invoiceheader.intPOYear = trn_poheader.intPOYear
Inner Join trn_podetails as tb1 ON trn_poheader.intPONo = tb1.intPONo AND trn_poheader.intPOYear = tb1.intPOYear AND ware_invoicedetails.intItemId = tb1.intItem
Inner Join mst_item ON ware_invoicedetails.intItemId = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
WHERE  
/*mst_item.intStatus = '1' AND*/ 
ware_invoicedetails.intInvoiceNo =  '$invoiceNo' AND
ware_invoicedetails.intInvoiceYear =  '$year'  
GROUP BY  ware_invoicedetails.intItemId 
Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc";
				$result = $db->RunQuery($sql);
				$totAmm=0;
				while($row=mysqli_fetch_array($result))
				{
					$stockBalQty=getStockBalance_bulk($location,$row['intItemId']);
					$maincatId=$row['intMainCategory'];
					$subCatId=$row['intSubCategory'];
					$mainCatName=$row['maincat'];
					$subCatName=$row['subcat'];
					$itemId=$row['intItemId'];
					$itemName=$row['item'];
					$uom=$row['uom'];
					$poQty=$row['poQty'];
					$invoiceQty=$row['dblQty'];
					$confirmedInvQty=$row['invoiceQty'];
					$retSupQty=$row2['retSupQty'];
					$totAmm+=$Qty;
					
/*					$row2=confirmedInvQtyAndretSupQty($row['intPONo'],$row['intPOYear']);
					$confirmedInvQty=$row2['dblInvoiceQty'];
					$retSupQty=$row2['dblRetunSupplierQty'];
					if($row2['dblInvoiceQty']==''){ 
					$confirmedInvQty =0;
					}
					if($row['dblRetunSupplierQty']==''){ 
					$retSupQty =0;
					}
*/	
				$poBal=$poQty-$confirmedInvQty+$retSupQty;
					$maxQty=0;
					if($poBal>$stockBalQty){
						$maxQty=$stockBalQty;
					}
					else{
						$maxQty=$poBal;
					}
					
					
			  ?>
<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" id="<?php echo $maincatId; ?>"><?php echo $mainCatName; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $subCatId; ?>" ><?php echo $subCatName; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $itemId; ?>" class="item"><?php echo $itemName; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $uom; ?>"><?php echo $uom; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $poQty; ?>"><?php echo $poQty; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $poBal; ?>"  class="balToInvQty"><?php echo $poBal; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $stockBalQty; ?>" class="stockQty"><?php echo $stockBalQty; ?></td>
        	<td align="center" bgcolor="#FFFFFF" id=""><input  id="<?php echo $stockBalQty ?>" class="validate[required,custom[number],max[<?php echo $maxQty ?>]] invoQty" style="width:80px;text-align:right" type="text" value="<?php echo $invoiceQty ?>" /></td>
        	</tr>             <?php
				}
			  ?>            </table>
        </div></td>
      </tr>
<tr>
        <td><div style="width:500px;height:300px;overflow:scroll; display:none" >
          <table width="100%" class="grid" id="tblLocationWiseallocations" >
            <tr class="gridHeader">
              <td width="41%" >Item</td>
              <td width="42%">location</td>
              <td width="17%">Qty</td>
              </tr>
              <?php
			     $sql = "SELECT
ware_invoicedetails.intItemId,
mst_item.strName as item,
ware_invoicedetails.intLocation,
mst_locations.strName as location,
sum(ware_invoicedetails.dblQty) as dblQty 
FROM
ware_invoicedetails
Inner Join mst_item ON ware_invoicedetails.intItemId = mst_item.intId
Inner Join mst_locations ON ware_invoicedetails.intLocation = mst_locations.intId
WHERE 
mst_item.intStatus = '1' AND 
ware_invoicedetails.intInvoiceNo =  '$invoiceNo' AND
ware_invoicedetails.intInvoiceYear =  '$year' 
GROUP BY
ware_invoicedetails.intLocation,
ware_invoicedetails.intItemId
ORDER BY
mst_item.strName ASC,
ware_invoicedetails.intLocation ASC
";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					$itemId=$row['intItemId'];
					$itemName=$row['item'];
					$locationId=$row['intLocation'];
					$location=$row['location'];
					$invoiceQty=$row['dblQty'];
					
			  ?>
<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" id="<?php echo $itemId; ?>"><?php echo $itemName; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $locationId; ?>" ><?php echo $location; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $itemId; ?>"><?php echo $invoiceQty; ?></td>
        	</tr>             <?php
				}

			  ?>            </table>
        </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound">
        <a name="butNew"  class="button white medium" id="butNew">New</a>
         <?php if(($form_permision['add']||$form_permision['edit']) && $editMode==1){ ?>
         <a id="butSave" name="butSave"  class="button white medium">Save</a>
		 <?php } ?>
         <a id="butConfirm" name="butConfirm" style=" <?php if(($intStatus>1) and ($confirmatonMode==1)){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>" class="button white medium">Approve</a>
         <a id="butReport" name="butReport"  class="button white medium">Report</a>
         <a href="main.php"  class="button white medium">Close</a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>


</body>
</html>
<?php
	//--------------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$style,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.strStyleNo =  '$style' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return (float)($rows['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				round(Sum(ware_stocktransactions_bulk.dblQty),4) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return (float)($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function confirmedInvQtyAndretSupQty($poNo,$year)
	{
		global $db;
		       $sql="SELECT
				mst_item.strName AS itemName,
				mst_item.intId,
				Sum(ware_invoicedetails.dblQty) as dblInvoiceQty  , 
				Sum(ware_grndetails.dblRetunSupplierQty) as dblRetunSupplierQty  
				FROM
				trn_poheader
				Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
				Inner Join trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear 
				Left Join ware_grnheader ON trn_podetails.intPONo = ware_grnheader.intPoNo AND trn_podetails.intPOYear = ware_grnheader.intPoYear and ware_grnheader.intStatus<=ware_grnheader.intApproveLevels  and ware_grnheader.intStatus>0
				Left Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND trn_podetails.intItem = ware_grndetails.intItemId
				Left Join ware_invoiceheader ON trn_podetails.intPONo = ware_invoiceheader.intPONo AND trn_podetails.intPOYear = ware_invoiceheader.intPOYear and ware_invoiceheader.intStatus<=ware_invoiceheader.intApproveLevels  and ware_invoiceheader.intStatus>0
				Left Join ware_invoicedetails ON ware_invoiceheader.intInvoiceNo = ware_invoicedetails.intInvoiceNo AND ware_invoiceheader.intInvoiceYear = ware_invoicedetails.intInvoiceYear AND trn_podetails.intItem = ware_invoicedetails.intItemId

				Inner Join mst_item ON trn_podetails.intItem = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				WHERE 
				mst_item.intStatus = '1' AND 
				trn_poheader.intPONo =  '$poNo' AND
				trn_poheader.intPOYear =  '$year' 
				group by mst_item.intId";
		$result = $db->RunQuery($sql);
		return mysqli_fetch_array($result);
	}
//------------------------------function load Header---------------------
function loadHeader($serialNo,$year)
{
	global $db;
		 $sql = "SELECT
				ware_invoiceheader.datdate,
				ware_invoiceheader.intStatus,
				ware_invoiceheader.intApproveLevels,
				ware_invoiceheader.intUser,
				ware_invoiceheader.intPONo,
				ware_invoiceheader.intPOYear,
				ware_invoiceheader.strNote,
				mst_supplier.intCompanyId,
				trn_poheader.intCompany as compId,
				ware_invoiceheader.intStatus, 
				sys_users.strUserName  
				FROM
				ware_invoiceheader 
				Inner Join trn_poheader ON ware_invoiceheader.intPONo = trn_poheader.intPONo AND ware_invoiceheader.intPOYear = trn_poheader.intPOYear 
				Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
				Inner Join mst_companies ON mst_supplier.intCompanyId = mst_companies.intId
				Inner Join sys_users ON ware_invoiceheader.intUser = sys_users.intUserId
				WHERE
				ware_invoiceheader.intInvoiceNo =  '$serialNo' AND
				ware_invoiceheader.intInvoiceYear =  '$year' 
				GROUP BY
				ware_invoiceheader.intInvoiceNo,
				ware_invoiceheader.intInvoiceYear";
				 $result = $db->RunQuery($sql);
			 return $result;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
?>

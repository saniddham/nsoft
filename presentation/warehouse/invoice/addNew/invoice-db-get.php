<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	/////////// type of print load part /////////////////////
if($requestType=='loadPONos')
	{
		$poYear  = $_REQUEST['poYear'];
		$sql = "SELECT DISTINCT
				trn_poheader.intPONo 
				FROM
				trn_poheader
				Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
				WHERE
				mst_supplier.intInterCompany =  '1' AND
				mst_supplier.intCompanyId =  '$company' AND  
				mst_supplier.intInterLocation =  '$location' ";
		if($poYear!=''){		
		$sql .= " AND trn_poheader.intPOYear =  '$poYear' ";	
		}
		$sql .= " order by intPONo desc, intPOYear desc ";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intPONo']."\">".$row['intPONo']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
else if($requestType=='loadDetails')
	{
		$po  = $_REQUEST['po'];
		$year  = $_REQUEST['year'];
		
		       $sql="SELECT
				mst_item.strName AS itemName,
				mst_item.intId,
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName AS mainCatName,
				mst_subcategory.strName AS subCatName,
				trn_poheader.intDeliveryTo as intCompanyId,
				tb1.dblQty,
				tb1.dblGRNQty, 
				mst_units.strCode as uom , 
				
				IFNULL((SELECT
				Sum(ware_invoicedetails.dblQty)
				FROM
				ware_invoiceheader
				Inner Join ware_invoicedetails ON ware_invoiceheader.intInvoiceNo = ware_invoicedetails.intInvoiceNo AND ware_invoiceheader.intInvoiceYear = ware_invoicedetails.intInvoiceYear
				WHERE
				ware_invoiceheader.intPONo =  tb1.intPONo AND
				ware_invoiceheader.intPOYear =  tb1.intPOYear AND
				ware_invoicedetails.intItemId =  tb1.intItem AND
				ware_invoiceheader.intStatus <=  ware_invoiceheader.intApproveLevels AND
				ware_invoiceheader.intStatus >  '0'),0) as dblInvoiceQty  , 
				
			 	IFNULL((SELECT
				Sum(ware_returntosupplierdetails.dblQty)
				FROM
				ware_returntosupplierheader
				Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear
				Inner Join ware_grnheader ON ware_returntosupplierheader.intGrnNo = ware_grnheader.intGrnNo AND ware_returntosupplierheader.intGrnYear = ware_grnheader.intGrnYear
				WHERE
				ware_grnheader.intPoNo =  tb1.intPONo AND
				ware_grnheader.intPoYear =  tb1.intPOYear AND
				ware_returntosupplierdetails.intItemId =  tb1.intItem AND
				ware_returntosupplierheader.intStatus = 1),0) as dblRetunSupplierQty  
				
				FROM
				trn_poheader
				Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
				Inner Join trn_podetails as tb1 ON trn_poheader.intPONo = tb1.intPONo AND trn_poheader.intPOYear = tb1.intPOYear 
				Left Join ware_grnheader ON tb1.intPONo = ware_grnheader.intPoNo AND tb1.intPOYear = ware_grnheader.intPoYear and ware_grnheader.intStatus<=ware_grnheader.intApproveLevels  and ware_grnheader.intStatus>0
				Left Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND tb1.intItem = ware_grndetails.intItemId
				Left Join ware_invoiceheader ON tb1.intPONo = ware_invoiceheader.intPONo AND tb1.intPOYear = ware_invoiceheader.intPOYear and ware_invoiceheader.intStatus<=ware_invoiceheader.intApproveLevels  and ware_invoiceheader.intStatus>0
				Left Join ware_invoicedetails ON ware_invoiceheader.intInvoiceNo = ware_invoicedetails.intInvoiceNo AND ware_invoiceheader.intInvoiceYear = ware_invoicedetails.intInvoiceYear AND tb1.intItem = ware_invoicedetails.intItemId
				Inner Join mst_item ON tb1.intItem = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				WHERE 
				mst_item.intStatus = '1' AND 
				trn_poheader.intPONo =  '$po' AND
				trn_poheader.intPOYear =  '$year' 
				group by mst_item.intId";
				
			//	echo $sql;
				
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$data['gpTo'] 	= $row['intCompanyId'];
			$data['itemId'] 	= $row['intId'];
			$data['maincatId'] = $row['intMainCategory'];
			$data['subCatId'] 	= $row['intSubCategory'];
			$data['itemName'] = $row['itemName'];
			$data['uom'] = $row['uom'];
			$data['mainCatName'] = $row['mainCatName'];
			$data['subCatName'] = $row['subCatName'];
			$data['poQty'] = round($row['dblQty'],4);
			$data['grnQty'] = round($row['dblGRNQty'],4);
			$data['invoiceQty'] =round($row['dblInvoiceQty'],4);
			$data['retSupQty'] =$row['dblRetunSupplierQty'];
			if($row['dblInvoiceQty']==''){ 
			$data['invoiceQty'] =0;
			}
			if($row['dblRetunSupplierQty']==''){ 
			$data['retSupQty'] =0;
			}
			$data['stockQty'] = getStockBalance_bulk($company,$location,$row['intId']);
			
			$arrCombo[] = $data;
		}
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);

	}
	//-----------------------------------------------------------
	function getStockBalance_bulk($company,$location,$item)
	{
		global $db;
		 		$sql = "SELECT
				round(Sum(ware_stocktransactions_bulk.dblQty),4) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intCompanyId =  '$company' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
?>
<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once $backwardseperator."class/warehouse/cls_warehouse_get.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$objwhouseget= new cls_warehouse_get($db);
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	/*$serialNo 	 = $_REQUEST['serialNo'];
	$year 		 = $_REQUEST['Year'];
	$poNo  = $_REQUEST['PoNo'];
	$poYear 		 = $_REQUEST['PoYear'];
	$note 	 = $_REQUEST['note'];
	$date 		 = $_REQUEST['date'];*/
	
	
	$arrHeader	= json_decode($_REQUEST['arrHeader'],true);
	$serialNo	= $arrHeader['serialNo'];
	$year		= $arrHeader['Year'];
	$poNo		= $arrHeader['PoNo'];
	$poYear		= $arrHeader['PoYear'];
	$note		= replace1($arrHeader['note']);
	$date		= $arrHeader['date'];

	$arr 		= json_decode($_REQUEST['arr'], true);
	
	$programName='Invoice';
	$programCode='P0484';

	$ApproveLevels = (int)getApproveLevel($programName);
	$invoiceApproveLevel = $ApproveLevels+1;
	
//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$savableFlag= 1;
		
		if($serialNo==''){
			$serialNo 	= getNextInvoiceNo();
			$year = date('Y');
			$editMode=0;
			$savedStatus=$invoiceApproveLevel;
			$savedLevels=$ApproveLevels;
		}
		else{
			$editMode=1;
			$savableFlag=getSaveStatus($serialNo,$year);
			$sql = "SELECT
			ware_invoiceheader.intStatus, 
			ware_invoiceheader.intApproveLevels 
			FROM ware_invoiceheader 
			WHERE
			ware_invoiceheader.intInvoiceNo =  '$serialNo' AND
			ware_invoiceheader.intInvoiceYear =  '$year'";

			$result = $db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			$savedStatus=$row['intStatus'];
			$savedLevels=$row['intApproveLevels'];
		}

		//--------------------------
		$response['arrData'] = $objwhouseget->getLocationValidation('INVOICE',$location,$poNo,$poYear);//check for grn location
		//--------------------------
		$editPermission=loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
		$locationEditFlag=getEditLocationValidation('INVOICE',$location,$serialNo,$year);
		//--------------------------
		
		
		if($response['arrData']['type']=='fail'){
			 $rollBackFlag=1;
			 $rollBackMsg=$response['arrData']['msg'];
		 }
		else if(( $locationEditFlag==1) && ($editMode==1)){
			 $rollBackFlag=1;
			 $rollBackMsg="Invalid Edit Location";
		 }
		else if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This Invoice No is already confirmed.cant edit";
		 }
		 else if($editPermission==0){
			 $rollBackFlag=1;
			 $rollBackMsg="No Permission to save.";
		 }
		else if($editMode==1){//edit existing grn
		$sql = "UPDATE `ware_invoiceheader` SET intStatus ='$invoiceApproveLevel', 
												       intApproveLevels ='$ApproveLevels', 
													  intPONo ='$poNo',
													  intPOYear ='$poYear',
													  strNote ='$note', 
													  intModifiedBy ='$userId', 
													  dtmModifiedDate =now() 
				WHERE (`intInvoiceNo`='$serialNo') AND (`intInvoiceYear`='$year')";
		$result = $db->RunQuery2($sql);
		}
		else{
			//$sql = "DELETE FROM `ware_invoiceheader` WHERE (`intInvoiceNo`='$serialNo') AND (`intInvoiceYear`='$year')";
			//$result1 = $db->RunQuery2($sql);

			$sql = "INSERT INTO `ware_invoiceheader` (`intInvoiceNo`,`intInvoiceYear`,intPONo,intPOYear,strNote,intStatus,intApproveLevels,datdate,dtmCreateDate,intUser,intCompanyId) 
					VALUES ('$serialNo','$year','$poNo','$poYear','$note','$invoiceApproveLevel','$ApproveLevels',now(),now(),'$userId','$location')";
			$result = $db->RunQuery2($sql);
			$maxAppByStatus=(int)getMaxAppByStatus($serialNo,$year)+1;
			$sql = "UPDATE `ware_invoiceheader_approvedby` SET intStatus ='$maxAppByStatus' 
					WHERE (`intInvoiceNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
			$result1 = $db->RunQuery2($sql);
		}
		//-----------delete and insert to detail table-----------------------
		if(($result) && ($rollBackFlag!=1)){
		$sql = "DELETE FROM `ware_invoicedetails` WHERE (`intInvoiceNo`='$serialNo') AND (`intInvoiceYear`='$year')";
		$result2 = $db->RunQuery2($sql);
			$toSave=0;
			$saved=0;
			$rollBackFlag=0;
			$rollBackMsg ="Maximum Invoice Qtys for items are..."; 
			foreach($arr as $arrVal)
			{
				$location 	= $_SESSION['CompanyID'];
				$company 	= $_SESSION['headCompanyId'];
				//$allocLocationId 	= $arrVal['location'];
				$allocLocationId 	= $location;
				$itemId 	 = $arrVal['itemId'];
				$itemName 		 = $arrVal['itemName'];
				$Qty 		 = $arrVal['Qty'];
		
				$stockBalQty=getStockBalance_bulk($allocLocationId,$itemId);
				$balToInvQty=getBalToInvQty($poNo,$poYear,$itemId);
				$stockBalQty=round($stockBalQty,4);
				$balToInvQty=round($balToInvQty,4);
				
				if($stockBalQty < $balToInvQty)
				$balToInvQty = $stockBalQty;
				
				
				if($Qty>$balToInvQty){
					$rollBackFlag=1;
					$rollBackMsg .="</br> ".$itemName." =".$balToInvQty;
				}

				//----------------------------
				if($rollBackFlag!=1){
				if($orderNoArray[0]==''){
					$orderNoArray[0]=0;
				}
				if($orderNoArray[1]==''){
					$orderNoArray[1]=0;
				}
				$Qty=round($Qty,4);
					$sql = "INSERT INTO `ware_invoicedetails` (`intInvoiceNo`,`intInvoiceYear`,intLocation,`intItemId`,`dblQty`) 
					VALUES ('$serialNo','$year','$location','$itemId','$Qty')";
					$result3 = $db->RunQuery2($sql);
					if($result3==1){
					$saved++;
					}
				}
				$toSave++;
			}
		}
		//echo $rollBackFlag;
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$confirmationMode=getConfirmationMode($programCode,$userId,$ApproveLevels,$invoiceApproveLevel);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
//----------------------------------------
	echo json_encode($response);
//-----------------------------------------	
	function getNextInvoiceNo()
	{
		global $db;
		global $location;
		$sql = "SELECT
				sys_no.intWareInvoiceNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$location'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextInvoiceNo = $row['intWareInvoiceNo'];
		
		$sql = "UPDATE `sys_no` SET intWareInvoiceNo=intWareInvoiceNo+1 WHERE (`intCompanyId`='$location')  ";
		$db->RunQuery2($sql);
		
		return $nextInvoiceNo;
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		       $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getSaveStatus($serialNo,$year)
	{
		global $db;
		$sql = "SELECT ware_invoiceheader.intStatus, ware_invoiceheader.intApproveLevels FROM ware_invoiceheader WHERE (intInvoiceNo='$serialNo') AND (`intInvoiceYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			
		return $editableFlag;
	}
	//-----------------------------------------------------------------
	function getBalToInvQty($poNo,$poYear,$itemId)
	{
		global $db;
		$sql = "SELECT
				mst_item.strName AS itemName,
				mst_item.intId,
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName AS mainCatName,
				mst_subcategory.strName AS subCatName,
				mst_supplier.intCompanyId,
				tb1.dblQty,
				tb1.dblGRNQty, 
				mst_units.strCode as uom , 
				
				IFNULL((SELECT
				Sum(ware_invoicedetails.dblQty)
				FROM
				ware_invoiceheader
				Inner Join ware_invoicedetails ON ware_invoiceheader.intInvoiceNo = ware_invoicedetails.intInvoiceNo AND ware_invoiceheader.intInvoiceYear = ware_invoicedetails.intInvoiceYear
				WHERE
				ware_invoiceheader.intPONo =  tb1.intPONo AND
				ware_invoiceheader.intPOYear =  tb1.intPOYear AND
				ware_invoicedetails.intItemId =  tb1.intItem AND
				ware_invoiceheader.intStatus <=  ware_invoiceheader.intApproveLevels AND
				ware_invoiceheader.intStatus >  '0'),0) as dblInvoiceQty  , 
				
			 	IFNULL((SELECT
				Sum(ware_returntosupplierdetails.dblQty)
				FROM
				ware_returntosupplierheader
				Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear
				Inner Join ware_grnheader ON ware_returntosupplierheader.intGrnNo = ware_grnheader.intGrnNo AND ware_returntosupplierheader.intGrnYear = ware_grnheader.intGrnYear
				WHERE
				ware_grnheader.intPoNo =  tb1.intPONo AND
				ware_grnheader.intPoYear =  tb1.intPOYear AND
				ware_returntosupplierdetails.intItemId =  tb1.intItem AND
				ware_returntosupplierheader.intStatus = 1),0) as dblRetunSupplierQty  
				
				FROM
				trn_poheader
				Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
				Inner Join trn_podetails as tb1 ON trn_poheader.intPONo = tb1.intPONo AND trn_poheader.intPOYear = tb1.intPOYear 
				Left Join ware_grnheader ON tb1.intPONo = ware_grnheader.intPoNo AND tb1.intPOYear = ware_grnheader.intPoYear and ware_grnheader.intStatus<=ware_grnheader.intApproveLevels  and ware_grnheader.intStatus>0
				Left Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND tb1.intItem = ware_grndetails.intItemId
				Left Join ware_invoiceheader ON tb1.intPONo = ware_invoiceheader.intPONo AND tb1.intPOYear = ware_invoiceheader.intPOYear and ware_invoiceheader.intStatus<=ware_invoiceheader.intApproveLevels  and ware_invoiceheader.intStatus>0
				Left Join ware_invoicedetails ON ware_invoiceheader.intInvoiceNo = ware_invoicedetails.intInvoiceNo AND ware_invoiceheader.intInvoiceYear = ware_invoicedetails.intInvoiceYear AND tb1.intItem = ware_invoicedetails.intItemId
				Inner Join mst_item ON tb1.intItem = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				WHERE
				trn_poheader.intPONo =  '$poNo' AND
				trn_poheader.intPOYear =  '$poYear' AND 
				tb1.intItem =  '$itemId' 
				group by mst_item.intId";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			
			$poQty=$row['dblQty'];
			$confirmedInvQty=$row['dblInvoiceQty'];
			$retSupQty=$row['dblRetunSupplierQty'];
			$invBal=$poQty-$confirmedInvQty+$retSupQty;
			
		return $invBal;
	}
	//-----------------------------------------------------------------
//-----------------------------------------------------------
	function getEditLocationValidation($type,$location,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
		  $sql = "SELECT
					ware_invoiceheader.intCompanyId
					FROM
					ware_invoiceheader
					Inner Join mst_locations ON ware_invoiceheader.intCompanyId = mst_locations.intId
					WHERE
					ware_invoiceheader.intInvoiceNo =  '$serialNo' AND
					ware_invoiceheader.intInvoiceYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			if($location == $row['intCompanyId'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function loadConfirmatonMode-------------------
function getConfirmationMode($programCode,$intUser,$approveLevels,$status){
		global $db;
		$k=$approveLevels+2-$status;
		$sqlp = "SELECT 
		menupermision.intEdit, 
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		if($rowp['int'.$k.'Approval']==1) 
		 $confirmatonMode=1; 
		else
		 $confirmatonMode=0; 
	 
	return $confirmatonMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		$resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
		 
		// $editMode;
		 
	return $editMode;
}
//------------------------------function get MaxAppByStatus---------------------
function getMaxAppByStatus($serialNo,$year){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	  $sql = "SELECT
				max(ware_invoiceheader_approvedby.intStatus) as status 
				FROM
				ware_invoiceheader_approvedby
				WHERE
				ware_invoiceheader_approvedby.intInvoiceNo =  '$serialNo' AND
				ware_invoiceheader_approvedby.intYear =  '$year'";
				
		$resultm = $db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm['status'];

}
//--------------------------------------------------------
function replace1($str)
{
	  $str 	= 	str_replace('\\', "\\\\",	$str);
	  $str 	= 	str_replace("'","\'",	$str);
	  $str	=	str_replace("\n", "<br>",$str);  
	  
	  return $str;
}	
//------------------------------------------------------

?>




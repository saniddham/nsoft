<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_permisions.php";
	require_once $_SESSION['ROOT_PATH']."class/warehouse/cls_warehouse_get.php";

	//---------------------------
	$objpermisionget= new cls_permisions($db);
	$obj_wh_get		= new cls_warehouse_get($db);
	//---------------------------
	
	//---------LOAD HEADER DATA--------------------------------------
	if($requestType=='loadHeaderData')
	{
		$grnNo  = $_REQUEST['grnNo'];
		$grnNoArray = explode('/',$grnNo);
		$grnNo 	 = $grnNoArray[0];
		$grnYear = $grnNoArray[1];
		
		$sql = "SELECT
				ware_grnheader.strInvoiceNo,
				trn_poheader.intSupplier,
				mst_supplier.strName
				FROM
				ware_grnheader
				Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
				Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
				WHERE
				ware_grnheader.intGrnNo =  '$grnNo' AND
				ware_grnheader.intGrnYear =  '$grnYear'";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['invoiceNo'] 	= $row['strInvoiceNo'];
			$response['supplier'] = $row['strName'];
		}
		echo json_encode($response);
	}
	//---------LOAD SUB CATEGORIES--------------------------------------
	else if($requestType=='loadSubCategory')
	{
		$mainCategory  = $_REQUEST['mainCategory'];
		$sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strCode,
				mst_subcategory.strName
				FROM mst_subcategory
				WHERE
				mst_subcategory.intMainCategory =  '$mainCategory' 
				ORDER BY mst_subcategory.strName ASC";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	//------LOAD SALES ORDER NOS--------------------------------------
	else if($requestType=='loadStyleNo')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderNoArray 	 = explode('/',$orderNo);
		 $sql = "SELECT DISTINCT 
		 		trn_orderdetails.intSalesOrderId, 
				trn_orderdetails.strSalesOrderNo,
				mst_part.strName
				FROM
				trn_orderdetails
				Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
				WHERE 
				trn_orderdetails.intOrderNo =  '$orderNoArray[0]' AND
				trn_orderdetails.intOrderYear =  '$orderNoArray[1]'";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intSalesOrderId']."\">".$row['strSalesOrderNo']."/".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	//------LOAD GENERAL ITEMS--------------------------------------
	else if($requestType=='loadGeneralItems')
	{
		$mainCategory  = $_REQUEST['mainCategory'];
		$subCategory  = $_REQUEST['subCategory'];
		$description  = $_REQUEST['description'];
		$bomItemPermission= getBomItemPermission($userId);
		
		   $sql="SELECT
				mst_item.strName as itemName,
				mst_item.intId, 
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,
				mst_item.strCode , 
				mst_units.strCode as uom 
				FROM 
				mst_item  
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				WHERE  
				mst_item.intStatus = '1' "; 
				if($bomItemPermission==1){
					$sql.=" AND mst_item.intBomItem IN  ('0','1') "; 
				}
				else{
					$sql.=" AND mst_item.intBomItem =  '0' "; 
				}
				
				if($mainCategory!='')
					$sql.=" AND mst_item.intMainCategory =  '$mainCategory' ";
				if($subCategory!='')
				$sql.=" AND mst_item.intSubCategory =  '$subCategory'";
				if($description!=''){
				$sql.=" AND
				mst_item.strName LIKE  '%$description%'";
				}
				$sql.=" Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc";
		//echo $sql;
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			 $stockBalQty=getStockBalance_bulk($location,$row['intId']);

			$data['itemId'] 	= $row['intId'];
			$data['maincatId'] = $row['intMainCategory'];
			$data['subCatId'] 	= $row['intSubCategory'];
			$data['mainCatName'] = $row['mainCatName'];
			$data['subCatName'] = $row['subCatName'];
			$data['code'] = $row['strCode'];
			$data['uom'] = $row['uom'];
			$data['itemName'] = $row['itemName'];
			
			$data['requiredQty'] 	= '';
			$data['allocatedQty'] 	= '';
			$data['mrnQty'] 	= '';
			$data['issueQty'] 	= '';
			
			$data['stockBal'] = round($stockBalQty,4);
			
			$arrCombo[] = $data;
		}
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);
	}

	//------LOAD BOM ITEMS--------------------------------------
	else if($requestType=='loadBomItems')
	{
		$itemType  = $_REQUEST['itemType'];
		$orderNo  = $_REQUEST['orderNo'];
		$orderNoArray 	 = explode('/',$orderNo);
		$salesOrderNo  = $_REQUEST['salesOrderNo'];
		$mainCategory  = $_REQUEST['mainCategory'];
		$subCategory  = $_REQUEST['subCategory'];
		$description  = $_REQUEST['description'];
		//---------------------
		if(($orderNoArray[0]!='') && ($salesOrderNo!='')){
			
			$result = getBomItems($orderNoArray[0],$orderNoArray[1],$salesOrderNo,$mainCategory,$subCategory,$description);
			while($row=mysqli_fetch_array($result))
			{
				$data['itemId'] 		= $row['intId'];
				$data['maincatId'] 		= $row['intMainCategory'];
				$data['subCatId'] 		= $row['intSubCategory'];
				$data['mainCatName'] 	= $row['mainCatName'];
				$data['subCatName'] 	= $row['subCatName'];
				$data['code'] 			= $row['strCode'];
				$data['uom'] 			= $row['uom'];
				$data['itemName'] 		= $row['itemName'];
				
				$data['requiredQty'] 	= '';
				$data['allocatedQty'] 	= '';
				$data['mrnQty'] 		= '';
				$data['issueQty'] 		= '';
				
				$data['orderQty'] 		= $obj_wh_get->get_order_qty($orderNoArray[0],$orderNoArray[1],$salesOrderNo,'RunQuery');
				$data['sampleConpC'] 	= $obj_wh_get->get_order_item_sample_consumption($orderNoArray[0],$orderNoArray[1],$salesOrderNo,$row['intId'],'RunQuery');
				$data['actualConPC'] 	= $obj_wh_get->get_order_item_first_second_consumption($orderNoArray[0],$orderNoArray[1],$salesOrderNo,$row['intId'],'RunQuery');
				
				$data['stockBal'] 		= round(getStockBalance($company,$location,$orderNoArray[0],$orderNoArray[1],$salesOrderNo,$row['intId']),4);
				
				$arrCombo[] = $data;
			}
		}
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);

	}
//---------------------------------------------------------------------------

//----------------------------FUNCTIONS--------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$salesOrderNo,$item)
	{
		global $db;
		  $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.intSalesOrderId =  '$salesOrderNo' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getBomItems($orderNo,$orderYear,$salesOrderNo,$mainCategory,$subCategory,$description)
	{
		global $db;
			  	$sql = "select 
						mst_item.strName as itemName,
						mst_item.intId, 
						mst_item.intMainCategory,
						mst_item.intSubCategory,
						mst_maincategory.strName as mainCatName,
						mst_subcategory.strName as subCatName,
						mst_item.strCode , 
						mst_units.strCode as uom 
						from (
						/*(SELECT
							trn_sample_color_recipes.intItem,
							Sum(trn_sample_color_recipes.dblWeight/1000) AS qty,
							mst_item.strName,
							mst_units.strName as unitName, 
							trn_sample_color_recipes.intSampleNo, 
							trn_sample_color_recipes.intSampleYear, 
							trn_sample_color_recipes.intRevisionNo, 
							trn_sample_color_recipes.strCombo, 
							trn_sample_color_recipes.strPrintName 
						FROM
							trn_sample_color_recipes
							Inner Join mst_item ON mst_item.intId = trn_sample_color_recipes.intItem
							inner join mst_units on mst_item.intUOM = mst_units.intId
						GROUP BY
							trn_sample_color_recipes.intItem,trn_sample_color_recipes.intSampleNo,trn_sample_color_recipes.intSampleNo, 
							trn_sample_color_recipes.intSampleYear, 
							trn_sample_color_recipes.intRevisionNo, 
							trn_sample_color_recipes.strCombo, 
							trn_sample_color_recipes.strPrintName
						)	
						UNION */
						(
						SELECT
							trn_sample_foil_consumption.intItem,
							Sum(trn_sample_foil_consumption.dblMeters) AS qty,
							mst_item.strName,
							'Meter' as unitName, 
							trn_sample_foil_consumption.intSampleNo, 
							trn_sample_foil_consumption.intSampleYear, 
							trn_sample_foil_consumption.intRevisionNo, 
							trn_sample_foil_consumption.strCombo, 
							trn_sample_foil_consumption.strPrintName 
						FROM
							trn_sample_foil_consumption
							Inner Join mst_item ON mst_item.intId = trn_sample_foil_consumption.intItem
						GROUP BY
							trn_sample_foil_consumption.intItem,trn_sample_foil_consumption.intSampleNo, 
							trn_sample_foil_consumption.intSampleYear, 
							trn_sample_foil_consumption.intRevisionNo, 
							trn_sample_foil_consumption.strCombo, 
							trn_sample_foil_consumption.strPrintName 
						)
						UNION
						(
						SELECT
							trn_sample_spitem_consumption.intItem,
							Sum(trn_sample_spitem_consumption.dblQty),
							mst_item.strName,
							mst_units.strName AS unitName, 
							trn_sample_spitem_consumption.intSampleNo, 
							trn_sample_spitem_consumption.intSampleYear, 
							trn_sample_spitem_consumption.intRevisionNo, 
							trn_sample_spitem_consumption.strCombo, 
							trn_sample_spitem_consumption.strPrintName 
						FROM
							trn_sample_spitem_consumption
							Inner Join mst_item ON mst_item.intId = trn_sample_spitem_consumption.intItem
							Inner Join mst_units ON mst_units.intId = mst_item.intUOM
						GROUP BY
							trn_sample_spitem_consumption.intItem,trn_sample_spitem_consumption.intSampleNo, 
							trn_sample_spitem_consumption.intSampleYear, 
							trn_sample_spitem_consumption.intRevisionNo, 
							trn_sample_spitem_consumption.strCombo, 
							trn_sample_spitem_consumption.strPrintName
						)	
						) as MAIN  
						
						INNER JOIN trn_orderdetails ON trn_orderdetails.intSampleNo=MAIN.intSampleNo AND 
						trn_orderdetails.intSampleYear=MAIN.intSampleYear AND 
						trn_orderdetails.intRevisionNo=MAIN.intRevisionNo AND 
						trn_orderdetails.strCombo=MAIN.strCombo AND 
						trn_orderdetails.strPrintName=MAIN.strPrintName  
						INNER JOIN mst_item ON mst_item.intId=MAIN.intItem 
						Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
						Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
						WHERE  
						trn_orderdetails.intOrderNo='$orderNo' AND 
						trn_orderdetails.intOrderYear='$orderYear' AND 
						trn_orderdetails.intSalesOrderId='$salesOrderNo' AND 
						mst_item.intMainCategory =  '$mainCategory' AND 
						mst_item.intStatus='1'";
						if($subCategory!='')
						$sql.=" AND mst_item.intSubCategory =  '$subCategory'";
						if($description!=''){
						$sql.=" AND
						mst_item.strName LIKE  '%$description%'";
						}
						$sql.=" Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc";
					//	echo $sql;

		$result = $db->RunQuery($sql);
		return $result;
	}
	//-------------------------------------------------------------
	function getBomItemPermission($user){
		global $db;
		   $sql = "SELECT *
				FROM sys_bomitem_users
				WHERE
				sys_bomitem_users.intUserId =  '$user'
				";
		$result = $db->RunQuery($sql);
		if($result){
			return 1;
		}
		else{
			return 0;
		}
	}
	//--------------------------------------------------------------

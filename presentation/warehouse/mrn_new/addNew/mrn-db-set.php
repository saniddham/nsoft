<?php 
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
	//ini_set('display_errors',1);
	
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	include_once "{$backwardseperator}dataAccess/Connector.php";
	require_once "{$backwardseperator}libraries/mail/mail.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_mail.php";
	require_once $_SESSION['ROOT_PATH']."class/warehouse/cls_warehouse_get.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_permisions.php";
	
	
	$objMail = new cls_create_mail($db);
	$obj_wh_get		= new cls_warehouse_get($db);
	$obj_common		= new cls_commonFunctions_get($db);
	$obj_permision		= new cls_permisions($db);
	
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];

	$arrHeader 	 = json_decode($_REQUEST['arrHeader'],true);
	$serialNo 	 = $arrHeader['serialNo'];
	$year 		 = $arrHeader['Year'];
	$department	 = $arrHeader['department'];
	$date 		 = $arrHeader['date'];
	$remarks 	 = replace1($arrHeader['remarks']);
	
	$arr 		 = json_decode($_REQUEST['arr'], true);
	
	$ApproveLevels = (int)getApproveLevel('Material Request Note');
	$mrnApproveLevel = $ApproveLevels+1;
	
	$programName='Material Request Note';
	$programCode='P0229';
	
//------------SAVE---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		$savableFlag= 1;
		
		if($serialNo==''){
			$serialNo 	= getNextMrnNo();
			$year = date('Y');
			$editMode=0;
			$savedStatus=$mrnApproveLevel;
			$savedLevels=$ApproveLevels;
		}
		else{
			$savableFlag=getSaveStatus($serialNo,$year);
			$editMode=1;
			$sql = "SELECT
			ware_mrnheader.intStatus, 
			ware_mrnheader.intApproveLevels 
			FROM ware_mrnheader 
			WHERE
			ware_mrnheader.intMrnNo =  '$serialNo' AND
			ware_mrnheader.intMrnYear =  '$year'";

			$result = $db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			$savedStatus=$row['intStatus'];
			$savedLevels=$row['intApproveLevels'];
		}
		
		//--------------------------
		$editPermission=loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
		$locationEditFlag=getEditLocationValidation('MRN',$companyId,$serialNo,$year);
		//--------------------------
		
		//-----------delete and insert to header table-----------------------
		 if(( $locationEditFlag==1) && ($editMode==1)){
			 $rollBackFlag=1;
			 $rollBackMsg="Invalid Edit Location";
		 }
		 else if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This MRN No is already confirmed.cant edit";
		 }
		 else if($editPermission==0){
			 $rollBackFlag=1;
			 $rollBackMsg="No Permission to save.";
		 }
		else if($editMode==1){
			$sql = "UPDATE `ware_mrnheader` SET intStatus ='$mrnApproveLevel', 
														   intDepartment ='$department', 
															 strRemarks ='$remarks',
														   intApproveLevels ='$ApproveLevels', 
														   datdate ='$date', 
														  intModifiedBy ='$userId', 
														  int25ExceedingApproved = NULL,
														  int25ExceedingApproveLevels = NULL,
														  int100ExceedingApproved = NULL,
 														  int100ExceedingApproveLevels = NULL,
														  dtmModifiedDate =now() 
					WHERE (`intMrnNo`='$serialNo') AND (`intMrnYear`='$year')";
			$result = $db->RunQuery2($sql);
			
			//inactive previous recordrs in approvedby table
			$maxAppByStatus=(int)getMaxAppByStatus($serialNo,$year)+1;
			$sql = "UPDATE `ware_mrnheader_approvedby` SET intStatus ='$maxAppByStatus' 
					WHERE (`intMrnNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
			$result1 = $db->RunQuery2($sql);
			if(!$result1){
				$rollBackFlag=1;
				$rollBackMsg ="Header Saving Error";
				$rollBackSql =$sql;
			}
		}
		else{
			//$sql = "DELETE FROM `ware_mrnheader` WHERE (`intMrnNo`='$serialNo') AND (`intMrnYear`='$year')";
			//$result1 = $db->RunQuery2($sql);

			$sql = "INSERT INTO `ware_mrnheader` (`intMrnNo`,`intMrnYear`,intDepartment,intStatus,intApproveLevels,strRemarks,datdate,dtmCreateDate,intUser,intCompanyId) 
					VALUES ('$serialNo','$year','$department','$mrnApproveLevel','$ApproveLevels','$remarks','$date',now(),'$userId','$companyId')";
			$result = $db->RunQuery2($sql);
		}
		//-----------delete and insert to detail table-----------------------
		if(($result) && ($rollBackFlag!=1)){
		$sql = "DELETE FROM `ware_mrndetails` WHERE (`intMrnNo`='$serialNo') AND (`intMrnYear`='$year')";
		$result2 = $db->RunQuery2($sql);
			$toSave=0;
			$saved=0;
			$rollBackFlag=0;
			$rollBackMsg ="Maximum MRN Qtys for items are..."; 
			foreach($arr as $arrVal)
			{
				$orderNo 	 = $arrVal['orderNo'];
				$orderNoArray 	 = explode('/',$orderNo);
				$salesOrderId 	 = $arrVal['salesOrderId'];
				$itemId 	 	= $arrVal['itemId'];
				$itemName		= $obj_common->get_item_name($itemId,'RunQuery2');
				$Qty 		 = $arrVal['Qty'];

				//-----------Validate MRN Qty-----------------
				//2013-12-12  - bom mrn validation
				if($orderNoArray[0] >0 ){
					$orderQty 		= $obj_wh_get->get_order_qty($orderNoArray[0],$orderNoArray[1],$salesOrderId,'RunQuery2');
					$sampleConpC	= $obj_wh_get->get_order_item_sample_consumption($orderNoArray[0],$orderNoArray[1],$salesOrderId,$itemId,'RunQuery2');
					$actualConPC 	= $obj_wh_get->get_order_item_first_second_consumption($orderNoArray[0],$orderNoArray[1],$salesOrderId,$itemId,'RunQuery2');
					$issuedQty	 	= $obj_wh_get->get_order_item_issued_qty($orderNoArray[0],$orderNoArray[1],$salesOrderId,$itemId,'RunQuery2');
					
					if($actualConPC > 0){
						$consump	=	$actualConPC;
					}
					else{
						$consump	=	$sampleConpC;
					}
					
/*					if(($actualConPC > 0) && (($Qty+$issuedQty) >round($orderQty*$actualConPC) )){
						$rollBackFlag = 1;
						$rollBackMsg  = 'Requested Qty Greater than expected Actual Qty for Item : </br>'.$itemName.'</br> (Expected ='.$orderQty.'*'.$actualConPC.' ='.round($orderQty*$actualConPC).')'.'</br> Already Issued Qty ='.$issuedQty;
					}
					
					else*/ 
					//requestd 25%<qty<=100%(actual not raised)
					if((($Qty+$issuedQty) > round($orderQty*$sampleConpC*25/100) && (($Qty+$issuedQty) <= round($orderQty*$sampleConpC))) && ($actualConPC<=0)){
						//$rollBackFlag=1;
						$appMsg ='Actual Consumptions not raised.Exceeds 25% MRN Qty :'.round($orderQty*$sampleConpC*25/100).'</br> Already Issued Qty ='.$issuedQty.'</br>'.'This has been sent to approve';
						send_25_ExceedingMRNForApproval($serialNo,$year,$obj_permision,$objMail,$mainPath,$root_path);
 						
						$appLevels	=(int)$obj_common->get_special_approve_levels('Permision to approve 25%  exceeded Quantities','RunQuery2');	
						$exStatus		=$appLevels+1;
					
						$sql = "UPDATE `ware_mrnheader` SET int25ExceedingApproved ='$exStatus' ,int25ExceedingApproveLevels ='$appLevels'
								WHERE (`intMrnNo`='$serialNo') AND (`intMrnYear`='$year')";
						$result1 = $db->RunQuery2($sql);
						if(!$result1){
							$rollBackFlag=1;
							$rollBackMsg ="Header Saving Error";
							$rollBackSql =$sql;
						}
						else{
							$sql = "UPDATE `ware_mrnheader` SET int100ExceedingApproved =NULL  ,int100ExceedingApproveLevels =NULL
									WHERE (`intMrnNo`='$serialNo') AND (`intMrnYear`='$year')";
							$result1 = $db->RunQuery2($sql);
							if(!$result1){
								$rollBackFlag=1;
								$rollBackMsg ="Header Saving Error";
								$rollBackSql =$sql;
							}
						}
						if($rollBackFlag != 1){
							//inactive previous recordrs in approvedby table
							$maxAppByStatus=(int)get_25_MaxAppByStatus($serialNo,$year)+1;
							$sql = "UPDATE `ware_mrn_25_exceeding_approval` SET intStatus ='$maxAppByStatus' 
									WHERE (`intMrnNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
							$result1 = $db->RunQuery2($sql);
							if(!$result1){
								$rollBackFlag=1;
								$rollBackMsg ="Header Saving Error";
								$rollBackSql =$sql;
							}
						}
						
					}
					//requestd qty>100%(actual not raised)
					else if(((($Qty+$issuedQty) > round($orderQty*$sampleConpC))) && ($actualConPC<=0)){
						//$rollBackFlag=1;
						$appMsg ='Actual Consumptions not raised.Exceeds 100% MRN Qty :'.round($orderQty*$sampleConpC).'</br> Already Issued Qty ='.$issuedQty.'</br>'.'This has been sent to approve';
						send_100_ExceedingMRNForApproval($serialNo,$year,$obj_permision,$objMail,$mainPath,$root_path);
 						
						$appLevels	=(int)$obj_common->get_special_approve_levels('Permision to approve 100%  exceeded Quantities','RunQuery2');	
						$exStatus		=$appLevels+1;
						
						$sql = "UPDATE `ware_mrnheader` SET int100ExceedingApproved ='$exStatus' ,int100ExceedingApproveLevels ='$appLevels'
								WHERE (`intMrnNo`='$serialNo') AND (`intMrnYear`='$year')";
						$result1 = $db->RunQuery2($sql);
						if(!$result1){
							$rollBackFlag=1;
							$rollBackMsg ="Header Saving Error";
							$rollBackSql =$sql;
						}
						else{
							$sql = "UPDATE `ware_mrnheader` SET int25ExceedingApproved =NULL ,int25ExceedingApproveLevels =NULL 
									WHERE (`intMrnNo`='$serialNo') AND (`intMrnYear`='$year')";
							$result1 = $db->RunQuery2($sql);
							if(!$result1){
								$rollBackFlag=1;
								$rollBackMsg ="Header Saving Error";
								$rollBackSql =$sql;
							}
						}
						if($rollBackFlag != 1){
							//inactive previous recordrs in approvedby table
							$maxAppByStatus=(int)get_100_MaxAppByStatus($serialNo,$year)+1;
							$sql = "UPDATE `ware_mrn_100_exceeding_approval` SET intStatus ='$maxAppByStatus' 
									WHERE (`intMrnNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
							$result1 = $db->RunQuery2($sql);
							if(!$result1){
								$rollBackFlag=1;
								$rollBackMsg ="Header Saving Error";
								$rollBackSql =$sql;
							}
						}
						
					}
					//requestd qty>100%(actual raised)
					else if(((($Qty+$issuedQty) > round($orderQty*$sampleConpC))) && ($actualConPC>0)){
						//$rollBackFlag=1;
						$appMsg ='Actual Consumptions raised.Exceeds 100% MRN Qty :'.round($orderQty*$actualConPC).'</br> Already Issued Qty ='.$issuedQty.'</br>'.'This has been sent to approve';
						send_100_ExceedingMRNForApproval($serialNo,$year,$obj_permision,$objMail,$mainPath,$root_path);
 						
						$appLevels	=(int)$obj_common->get_special_approve_levels('Permision to approve 100%  exceeded Quantities','RunQuery2');	
						$exStatus		=$appLevels+1;
						
						  $sql = "UPDATE `ware_mrnheader` SET `int100ExceedingApproved` ='$exStatus'  ,`int100ExceedingApproveLevels` ='$appLevels' 
								WHERE (`intMrnNo`='$serialNo') AND (`intMrnYear`='$year')";
						  $result1 = $db->RunQuery2($sql);
 						if(!$result1){
							$rollBackFlag=1;
							$rollBackMsg ="Header Saving Error";
							$rollBackSql =$sql;
						}
						else{
							$sql = "UPDATE `ware_mrnheader` SET int25ExceedingApproved =NULL ,int25ExceedingApproveLevels =NULL
									WHERE (`intMrnNo`='$serialNo') AND (`intMrnYear`='$year')";
							$result1 = $db->RunQuery2($sql);
							if(!$result1){
								$rollBackFlag=1;
								$rollBackMsg ="Header Saving Error";
								$rollBackSql =$sql;
							}
						}
						if($rollBackFlag != 1){
							//inactive previous recordrs in approvedby table
							$maxAppByStatus=(int)get_100_MaxAppByStatus($serialNo,$year)+1;
							$sql = "UPDATE `ware_mrn_100_exceeding_approval` SET intStatus ='$maxAppByStatus' 
									WHERE (`intMrnNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
							$result1 = $db->RunQuery2($sql);
							if(!$result1){
								$rollBackFlag=1;
								$rollBackMsg ="Header Saving Error";
								$rollBackSql =$sql;
							}
						}
					}
				}
						
				//--------------------------------------------
				if($rollBackFlag!=1){
					if($orderNoArray[0]=='')
					$orderNoArray[0]=0;
					
					if($orderNoArray[1]=='')
					$orderNoArray[1]=0;
					
					$sql3 = "INSERT INTO `ware_mrndetails` (`intMrnNo`,`intMrnYear`,`intOrderNo`,intOrderYear,`strStyleNo`,`intItemId`,`dblQty`,dblIssudQty) 
					VALUES ('$serialNo','$year','$orderNoArray[0]','$orderNoArray[1]','$salesOrderId','$itemId','$Qty','0')";
					$result3 = $db->RunQuery2($sql3);
					if($result3==1){
					$saved++;
					}
				}
				$toSave++;
			}
		}
		//echo $rollBackFlag;
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $rollBackSql;
		}
		else if(($savableFlag==1) &&(!$result)){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Header saving error";
			$response['q'] 			= $sql;
		}
		else if(($savableFlag==1) &&($toSave==0) &&($saved==0)){
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Details saving error";
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$confirmationMode=getConfirmationMode($programCode,$userId,$ApproveLevels,$mrnApproveLevel);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.'.'</br>'.$appMsg;
			else
			$response['msg'] 		= 'Saved successfully.'.'</br>'.$appMsg;
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
//------------CLEAR MRN---------------------------	
	if($requestType=='clear')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$sql = "UPDATE `ware_mrnheader` SET 		  intModifiedBy ='$userId', 
													  dtmModifiedDate =now() 
				WHERE (`intMrnNo`='$serialNo') AND (`intMrnYear`='$year')";
		$result = $db->RunQuery2($sql);
		//-----------delete and insert to detail table-----------------------
		if($result){
			$toSave=0;
			$saved=0;
			$rollBackFlag=0;
			$rollBackMsg ="Maximum clear Qty for items are..."; 
			foreach($arr as $arrVal)
			{
				$orderNo 	 = $arrVal['orderNo'];
				$orderNoArray 	 = explode('/',$orderNo);
				$salesOrderId 	 = $arrVal['salesOrderId'];
				$itemId 	 = $arrVal['itemId'];
				$mrnQty 		 = $arrVal['mrnQty'];
				$clearQty 		 = $arrVal['clearQty'];

				if($orderNoArray[0]==''){
				$orderNoArray[0]=0;
				}
				if($orderNoArray[1]=='')
				$orderNoArray[1]=0;
				
				//------check maximum PO Qty--------------------
			     $sqlc = "SELECT
				ware_mrndetails.dblQty, 
				ware_mrndetails.dblIssudQty, 
				mst_item.strName as itemName,
				mst_item.intBomItem,
				 mst_item.intId as intItemNo, 
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,
				ware_mrndetails.intOrderNo,
				ware_mrndetails.intOrderYear,
				trn_orderdetails.intSalesOrderId,  
				trn_orderdetails.strSalesOrderNo, 
				mst_item.strCode
				FROM
				ware_mrndetails 
				left Join trn_orderdetails ON ware_mrndetails.intOrderNo = trn_orderdetails.intOrderNo AND ware_mrndetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_mrndetails.strStyleNo = trn_orderdetails.intSalesOrderId
				Inner Join mst_item ON ware_mrndetails.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				WHERE
				ware_mrndetails.intOrderNo =  '$orderNoArray[0]' AND
				ware_mrndetails.intOrderYear =  '$orderNoArray[1]' AND  
				ware_mrndetails.strStyleNo =  '$salesOrderId' AND
				ware_mrndetails.intItemId =  '$itemId' AND
				ware_mrndetails.intMrnNo =  '$serialNo' AND
				ware_mrndetails.intMrnNo =  '$year' 
				ORDER BY mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc ";
				$resultsc = $db->RunQuery2($sqlc);
				$rowc = mysqli_fetch_array($resultsc);
				$maxQty 	 = $rowc['dblQty']-$rowc['dblIssudQty'];

				if(($mrnQty<$minQty) && ($mrnQty>$maxQty)){
				//	call roll back--------****************
					$rollBackFlag=1;
					if($rowc['intBomItem']==1){
					 $rollBackMsg .="\n ".$orderNoArray[0]."/".$orderNoArray[1]."-".$salesOrderId."-".$rowc['itemName']." =".$maxQty;
					}
					else
					{
					 $rollBackMsg .="\n ".$rowc['itemName']." =".$maxQty;
					}
					//exit();
				}

				//----------------------------
				if($rollBackFlag!=1){
					
					$sql3 = "UPDATE `ware_mrndetails` SET  
					dblQty ='$mrnQty', 
					dblMRNClearQty =dblMRNClearQty+'$clearQty'  
					WHERE (`intMrnNo`='$serialNo') 
					AND (`intMrnYear`='$year') 
					AND (`intOrderNo`='$orderNoArray[0]') 
					AND (`intOrderYear`='$orderNoArray[1]') 
					AND (`strStyleNo`='$salesOrderId') 
					AND (`intItemId`='$itemId')";
					$result3 = $db->RunQuery2($sql3);
					
					if($result3==1){
					$saved++;
					}
				}
				$toSave++;
			}
		}
		//echo $rollBackFlag;
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Cleared successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
	echo json_encode($response);
//-----------------------------------------	-------------------

//---------------FUNCTIONS-----------------	-------------------
	function getNextMrnNo()
	{
		global $db;
		global $companyId;
		$sql = "SELECT
				sys_no.intMrnNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$companyId'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextMrnNo = $row['intMrnNo'];
		
		$sql = "UPDATE `sys_no` SET intMrnNo=intMrnNo+1 WHERE (`intCompanyId`='$companyId')  ";
		$db->RunQuery2($sql);
		
		return $nextMrnNo;
	}
//---------------------------------------------------	
	function getSaveStatus($mrnNo,$year)
	{
		global $db;
		$sql = "SELECT ware_mrnheader.intStatus, ware_mrnheader.intApproveLevels FROM ware_mrnheader WHERE (intMrnNo='$mrnNo') AND (`intMrnYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			
		return $editableFlag;
	}
//-----------------------------------------------------------
	function getEditLocationValidation($type,$location,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
		  $sql = "SELECT
					ware_mrnheader.intCompanyId
					FROM
					ware_mrnheader
					Inner Join mst_locations ON ware_mrnheader.intCompanyId = mst_locations.intId
					WHERE
					ware_mrnheader.intMrnNo =  '$serialNo' AND
					ware_mrnheader.intMrnYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			if($location == $row['intCompanyId'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function loadConfirmatonMode-------------------
function getConfirmationMode($programCode,$intUser,$approveLevels,$status){
		global $db;
		$k=$approveLevels+2-$status;
		$sqlp = "SELECT 
		menupermision.intEdit, 
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		if($rowp['int'.$k.'Approval']==1) 
		 $confirmatonMode=1; 
		else
		 $confirmatonMode=0; 
	 
	return $confirmatonMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		$resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
		 
		// $editMode;
		 
	return $editMode;
}
//------------------------------function get MaxAppByStatus---------------------
function getMaxAppByStatus($serialNo,$year){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	  $sql = "SELECT
				max(ware_mrnheader_approvedby.intStatus) as status 
				FROM
				ware_mrnheader_approvedby
				WHERE
				ware_mrnheader_approvedby.intMrnNo =  '$serialNo' AND
				ware_mrnheader_approvedby.intYear =  '$year'";
				
		$resultm = $db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm['status'];

}
//--------------------------------------------------------getMaxAppByStatus
function replace1($str)
{
	  $str 	= 	str_replace('\\', "\\\\",	$str);
	  $str 	= 	str_replace("'","\'",	$str);
	  $str	=	str_replace("\n", "<br>",$str);  
	  
	  return $str;
}	

//-------------------------------------------------------
//--------------------------------------------------------
function send_25_ExceedingMRNForApproval($serialNo,$year,$obj_permision,$objMail,$mainPath,$root_path){
	global $db;
 				
 		 	$row	= $obj_permision->get_special_permision('50','RunQuery2');
 			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="MRN ('$serialNo'/'$year') - (EXCEEDING MRN QUANTITIES)"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='MRN';
			$_REQUEST['field1']='MRN No';
			$_REQUEST['field2']='MRN Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="MRN - (EXCEEDING MRN QUANTITIES) ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Approve Quantities - ";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/mrn/listing/rptMrn.php?mrnNo=$serialNo&year=$year&approveMode_exceeding=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
function send_100_ExceedingMRNForApproval($serialNo,$year,$obj_permision,$objMail,$mainPath,$root_path){
	global $db;
 				
 		 	$row	= $obj_permision->get_special_permision('51','RunQuery2');
 			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="MRN ('$serialNo'/'$year') - (EXCEEDING MRN QUANTITIES)"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='MRN';
			$_REQUEST['field1']='MRN No';
			$_REQUEST['field2']='MRN Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="MRN - (EXCEEDING MRN QUANTITIES) ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Approve Quantities - ";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/mrn/listing/rptMrn.php?mrnNo=$serialNo&year=$year&approveMode_exceeding=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
function get_25_MaxAppByStatus($serialNo,$year){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	  $sql = "SELECT
				max(ware_mrn_25_exceeding_approval.intStatus) as status 
				FROM
				ware_mrn_25_exceeding_approval
				WHERE
				ware_mrn_25_exceeding_approval.intMrnNo =  '$serialNo' AND
				ware_mrn_25_exceeding_approval.intYear =  '$year'";
				
		$resultm = $db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm['status'];

}
//--------------------------------------------------------
function get_100_MaxAppByStatus($serialNo,$year){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	  $sql = "SELECT
				max(ware_mrn_100_exceeding_approval.intStatus) as status 
				FROM
				ware_mrn_100_exceeding_approval
				WHERE
				ware_mrn_100_exceeding_approval.intMrnNo =  '$serialNo' AND
				ware_mrn_100_exceeding_approval.intYear =  '$year'";
				
		$resultm = $db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm['status'];

}


?>


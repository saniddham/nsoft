			
$(document).ready(function() {
	
	$("#frmMrn").validationEngine();
	$('#frmMrn #cboSupplier').focus();
	//------	
	$("#butAddItems").click(function(){
		if($('#cboDepartment').val()==''){
			alert("Please select the deparment");
			return false;
		}
		closePopUp();
		loadPopup();
	});
	//------	
  //permision for add 
	$('#frmMrn #butAddItems').show();
/*   if(intAddx)
  {
 	$('#frmMrn #butNew').show();
	$('#frmMrn #butSave').show();
	$('#frmMrn #butAddItems').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmMrn #butSave').show();
	$('#frmMrn #butAddItems').show();
	//$('#frmMrn #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmMrn #butDelete').show();
	//$('#frmMrn #cboSearch').removeAttr('disabled');
  }
*/  
  //permision for view
/*  if(intViewx)
  {
	//$('#frmMrn #cboSearch').removeAttr('disabled');
  }
*/  

  //-------------------------------------------------------
 
  $('#frmMrn #butSave').click(function(){
 	var requestType = '';
	if ($('#frmMrn').validationEngine('validate'))   
    { 
		showWaiting();
		var data = "requestType=save";


		var arrHeader = "{";
							arrHeader += '"serialNo":"'+$('#txtMrnNo').val()+'",' ;
							arrHeader += '"Year":"'+$('#txtYear').val()+'",' ;
							arrHeader += '"department":"'+$('#cboDepartment').val()+'",' ;
							arrHeader += '"remarks":'+URLEncode_json($('#txtRemarks').val())+',' ;
							arrHeader += '"date":"'+$('#dtDate').val()+'"' ;
			arrHeader += ' }';
		
		
			var rowCount = document.getElementById('tblMrn').rows.length;
			if(rowCount==1){
				alert("items not selected to MRN");
				hideWaiting();
				return false;				
			}
			var row = 0;
			var errorFlagQty=0;
			
			var arr="[";
			
			$('#tblMrn .item').each(function(){
	
				var orderNo = 	$(this).parent().find(".orderNo").html();
				var salesOrderId = 	$(this).parent().find(".salesOrderNo").attr('id');
				var itemId	= $(this).attr('id');
				var Quantity	= $(this).parent().find(".Qty").val();
					
					if(Quantity>0){
					    arr += "{";
						arr += '"orderNo":"'+		orderNo +'",' ;
						arr += '"salesOrderId":"'+		salesOrderId +'",' ;
						arr += '"itemId":"'+		itemId +'",' ;
						arr += '"Qty":"'+		Quantity  +'"' ;
						arr +=  '},';
						
					}
					if(Quantity<=0){
						errorFlagQty=1;	
					}
			});
			
			if(errorFlagQty==1){
				alert("Please enter MRN Quantities.");
				return false;
			}
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arrHeader="	+	arrHeader;
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = "mrn-db-set.php";
     	var obj = $.ajax({
			url:url,
			type:'post',
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmMrn #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					hideWaiting();
					if(json.type=='pass')
					{
						var t=setTimeout("alertx()",99000);
						$('#txtMrnNo').val(json.serialNo);
						$('#txtYear').val(json.year);
						if(json.confirmationMode==1){
							$('#butConfirm').show();
						}
						else{
							$('#butConfirm').hide();
						}
						//return;
					}
				},
			error:function(xhr,status){
					hideWaiting();
					$('#frmMrn #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",5000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
  //-------------------------------------------------------
 
  $('#frmMrnClearance #butClear').click(function(){
	if ($('#frmMrnClearance').validationEngine('validate'))   
    { 
		var requestType = '';
		var data = "requestType=clear";
			data+="&serialNo="		+	$('#cboMrnNo').val();
			data+="&Year="	+	$('#cboMrnYear').val();


			var rowCount = document.getElementById('tblMrn').rows.length;
			if(rowCount==1){
				alert("No items to clear");
				return false;				
			}
			var row = 0;
			
			var arr="[";
			
			$('#tblMrn .item').each(function(){
	
				var orderNo = 	$(this).parent().find(".orderNo").html();
				var salesOrderId = 	$(this).parent().find(".salesOrderNo").attr('id');
				var itemId	= $(this).attr('id');
				var mrnQty	= parseFloat($(this).parent().find(".mrnQty").html());
				var clearQty	= parseFloat($(this).parent().find(".Qty").val());
				mrnQty=mrnQty-clearQty;
				
					
					if(clearQty>0){
						arr += "{";
						arr += '"orderNo":"'+		orderNo +'",' ;
						arr += '"salesOrderId":"'+		salesOrderId +'",' ;
						arr += '"itemId":"'+		itemId +'",' ;
						arr += '"mrnQty":"'+		mrnQty +'",' ;
						arr += '"clearQty":"'+		clearQty  +'"' ;
						arr +=  '},';
					}
			});
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = "mrn-db-set.php";
     	var obj = $.ajax({
			url:url,
			type:'post',
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmMrnClearance #butClear').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertxC()",1000);
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmMrnClearance #butClear').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertxC()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
	
//--------------refresh the form----------
$('#frmMrn #butNew').click(function(){
		window.location.href = "mrn.php";
});
//----------------------------------------
$('.delImg').click(function(){
	$(this).parent().parent().remove();
});
//-----------------------------------
$('#butReport').click(function(){
	if($('#txtMrnNo').val()!=''){
		window.open('../listing/rptMrn.php?mrnNo='+$('#txtMrnNo').val()+'&year='+$('#txtYear').val());	
	}
	else{
		alert("There is no MRN to view");
	}
});
//----------------------------------	
$('#butConfirm').click(function(){
	if($('#txtMrnNo').val()!=''){
		window.open('../listing/rptMrn.php?mrnNo='+$('#txtMrnNo').val()+'&year='+$('#txtYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no MRN to confirm");
	}
});
//-----------------------------------------------------
$('#butClose').click(function(){
		//load('main.php');	
});
//--------------refresh the form-----------------------
	$('#frmMrn #butNew').click(function(){
		$('#frmMrn').get(0).reset();
		clearRows();
		$('#frmMrn #txtMrnNo').val('');
		$('#frmMrn #txtYear').val('');
		$('#frmMrn #cboDepartment').removeAttr('disabled');
		$('#frmMrn #dtDate').removeAttr('disabled');
		$('#frmMrn #cboDepartment').val('');
		$('#frmMrn #cboDepartment').focus();
		$('#frmMrn #txtRemarks').val('');
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmMrn #dtDate').val(d);
	});
//-----------------------------------------------------
});

////////////----end of ready ----//////////////////////////////////

//-------------------------------------------------------
function loadPopup()
{
		popupWindow3('1');
		$('#popupContact1').load('mrnPopup.php',function(){
				$('#butAdd').click(addClickedRows);
				$('#butClose1').click(disablePopup);
				 //-------------------------------------------- 
				  $('#frmMrnPopup #cboItemType').change(function(){
					    clearPopupRows();
						var itemType = $('#cboItemType').val();
						if(itemType==0){
						$('#frmMrnPopup #cboOrderNo').val('');
						$('#frmMrnPopup #cbStyleNo').val('');
						document.getElementById('rw1').style.display='none';
						document.getElementById('rw2').style.display='none';
						}
						else{
						document.getElementById('rw1').style.display='';
						document.getElementById('rw2').style.display='';
						}
						
				  });
				 //-------------------------------------------- 
				  $('#frmMrnPopup #cboMainCategory').change(function(){
						var mainCategory = $('#cboMainCategory').val();
						var url 		= "mrn-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
						var httpobj 	= $.ajax({url:url,async:false})
						document.getElementById('cboSubCategory').innerHTML=httpobj.responseText;
						
				  });
				 //-------------------------------------------- 
				  $('#frmMrnPopup #cboOrderNo').change(function(){
						var orderNo = $('#cboOrderNo').val();
						var url 		= "mrn-db-get.php?requestType=loadStyleNo&orderNo="+orderNo;
						var httpobj 	= $.ajax({url:url,async:false})
						document.getElementById('cbStyleNo').innerHTML=httpobj.responseText;
						
				  });
				 //-------------------------------------------- 
				  $('#frmMrnPopup #imgSearchItems').click(function(){
						var rowCount = document.getElementById('tblItemsPopup').rows.length;
						for(var i=1;i<rowCount;i++)
						{
							document.getElementById('tblItemsPopup').deleteRow(1);
						}
						var itemType = $('#cboItemType').val();
						var orderNo = $('#cboOrderNo').val();
						var salesOrderId = $('#cbStyleNo').val();
						var salesOrderNo = $('#cbStyleNo option:selected').text();
						var mainCategory = $('#cboMainCategory').val();
						var subCategory = $('#cboSubCategory').val();
						var description = $('#txtItmDesc').val();
						
						if(itemType==0){//genaral items
							if(mainCategory==''){
								//alert("Please select the main category");
								//return false;
								//comment by roshan 2012-09-27 // users request they want to search items without selecting category //
							}
						}
						else if(itemType==1){//bom items
							if(orderNo==''){
								alert("Please select Order No");
								return false;
							}
							else if(salesOrderId==''){
								alert("Please select the Sales Order No");
								return false;
							}
							else if(mainCategory==''){
								alert("Please select the main category");
								return false;
							}
						}
						
						
						
						if(itemType==0){//genaral items
						var url 		= "mrn-db-get.php?requestType=loadGeneralItems";
						}
						else if(itemType==1){//bom items
						var url 		= "mrn-db-get.php?requestType=loadBomItems";
						}
						
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							data:"orderNo="+orderNo+"&mainCategory="+mainCategory+"&subCategory="+subCategory+"&salesOrderNo="+salesOrderId+"&description="+description, 
							async:false,
							success:function(json){

								var length = json.arrCombo.length;
								var arrCombo = json.arrCombo;


								for(var i=0;i<length;i++)
								{
									var itemId=arrCombo[i]['itemId'];	
									var maincatId=arrCombo[i]['maincatId'];	
									var subCatId=arrCombo[i]['subCatId'];	
									var code=arrCombo[i]['code'];	
									var itemName=arrCombo[i]['itemName'];
									var uom=arrCombo[i]['uom'];
									var stockBal=arrCombo[i]['stockBal'];	
									var mainCatName=arrCombo[i]['mainCatName'];
									var subCatName=arrCombo[i]['subCatName'];	
										
									var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" id=""><input type="checkbox" id="chkDisp" /></td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatNameP">'+mainCatName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatNameP">'+subCatName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+code+'" class="codeP">'+code+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="itemP">'+itemName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uomP">'+uom+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+orderNo+'" style="display:none"  class="orderNoP">'+orderNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderId+'" style="display:none"  class="salesOrderNoP">'+salesOrderNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+stockBal+'" class="stockBalP">'+stockBal+'</td></tr>';

									add_new_row('#frmMrnPopup #tblItemsPopup',content);
									
								}
									checkAlreadySelected();

							}
						});
						
				  });
				  //-------------------------------------------------------
					$('#butAdd').click(addClickedRows);
					$('#butClose1').click(disablePopup);
				  //-------------------------------------------------------
			});	
}//-------------------------------------------------------
function addClickedRows()
{
	var rowCount = document.getElementById('tblItemsPopup').rows.length;
	var totRetAmmount=0;
	var itemType = $('#cboItemType').val();

	var i=0;
	$('#tblItemsPopup .itemP').each(function(){
		i++;
		
		if((document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked==true) && (document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled==false)){
			

			var itemId=$(this).attr('id');
			var maincatId=$(this).parent().find(".mainCatNameP").attr('id');
			var subCatId=$(this).parent().find(".subCatNameP").attr('id');
			var code=$(this).parent().find(".code").html();
			var uom=$(this).parent().find(".uomP").html();
			var itemName=$(this).html();
			var mainCatName=$(this).parent().find(".mainCatNameP").html();
			var subCatName=$(this).parent().find(".subCatNameP").html();
			var stockBal=$(this).parent().find(".stockBalP").html();
			var orderNo=$(this).parent().find(".orderNoP").html();
			var salesOrderNo=$(this).parent().find(".salesOrderNoP").html();
			var salesOrderId=$(this).parent().find(".salesOrderNoP").attr('id');
		
			var qty='';

			var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="../../../../images/del.png" width="15" height="15" /></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+orderNo+'" class="orderNo">'+orderNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderId+'" class="salesOrderNo">'+salesOrderNo+'</td>';
			//content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+itemName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'">'+mainCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'">'+subCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="item">'+itemName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uom">'+uom+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="'+stockBal+'" class="validate[required,custom[number]] calculateValue Qty" style="width:80px;text-align:center" type="text" value="'+qty+'"/></td>';
			content +='</tr>';
		
			add_new_row('#frmMrn #tblMrn',content);
		//----------------------------	
		$('.delImg').click(function(){
			$(this).parent().parent().remove();
		});
		
		}
	});
	disablePopup();
	closePopUp();
}
//-------------------------------------
function alertx()
{
	$('#frmMrn #butSave').validationEngine('hide')	;
	document.location.href=document.location.href;
	//window.opener.location.reload();//reload listing page
	//window.location.href = "mrn.php?mrnNo="+serialNo+"&year="+year;
	window.opener.opener.location.reload();//reload listing page
}
function alertxC()
{
	$('#frmMrn #butClear').validationEngine('hide')	;
	document.location.href=document.location.href;
}
function alertDelete()
{
	$('#frmMrn #butDelete').validationEngine('hide')	;
}

function closePopUp(){
	
}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblMrn').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMrn').deleteRow(1);
			
	}
}
//----------------------------------------------- 
function clearPopupRows()
{
	var rowCount = document.getElementById('tblItemsPopup').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblItemsPopup').deleteRow(1);
			
	}
}
//------------------------------------------------------------
function checkAlreadySelected(){
	$('#tblMrn .item').each(function(){
			var itemId	= $(this).attr('id');
			var orderNo = 	$(this).parent().find(".orderNo").html();
			var salesOrderNo = 	$(this).parent().find(".salesOrderNo").html();
			
			var j=0;
			$('#tblItemsPopup .itemP').each(function(){
				j++;
				var itemIdP = 	$(this).attr('id');
				var orderNoP=$(this).parent().find(".orderNoP").html();
				var salesOrderNoP=$(this).parent().find(".salesOrderNoP").html();
				
				if((orderNo==orderNoP) && (salesOrderNo==salesOrderNoP) && (itemId==itemIdP)){
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].checked=true;
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].disabled=true;
				}
			});
	});
	
	
	var rowCount = document.getElementById('tblMrn').rows.length;
	var rowCount1 = document.getElementById('tblItemsPopup').rows.length;
	
}
//---------------------------------------------------

<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];

include $backwardseperator."dataAccess/Connector.php";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Items</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>


</head>

<body>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>

    
<form id="frmMrnPopup" name="frmMrnPopup" method="post" action="">
<table width="950" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutD" style="width:810px">
		  <div class="trans_text"> Item List</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableBorder">
<tr>
            <td width="24%" height="22" class="normalfnt">&nbsp;</td>
            <td width="27%" class="normalfnt">Item Type</td>
            <td width="9%" class="normalfnt"><select name="cboItemType" style="width:120px" id="cboItemType">
                  <option value="1">Style</option>
                  <option value="0">General</option>
                  </select></td>
            <td width="17%" align="right">&nbsp;</td>
            </tr> <div id="divBom" >  <tr id="rw1">
            <td width="24%" height="22" class="normalfnt">&nbsp;</td>
            <td width="27%" class="normalfnt">Order No</td>
            <td width="23%"><select name="cboOrderNo" style="width:120px" id="cboOrderNo">
                  <option value=""></option>
                  <?php
					$sql = "SELECT DISTINCT
							trn_orderheader.intOrderNo,
							trn_orderheader.intOrderYear
							FROM trn_orderheader 
							WHERE
							trn_orderheader.intStatus =  '1' and intLocationId=$location order by  intOrderYear,intOrderNo desc";

					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intOrderNo']."/".$row['intOrderYear']."\">".$row['intOrderNo']."/".$row['intOrderYear']."</option>";	
					}
				?>
            </select></td>
            <td width="9%" class="normalfnt"></td>
            <td width="17%" align="right">&nbsp;</td>
            </tr>
<tr id="rw2">
            <td width="24%" height="22" class="normalfnt">&nbsp;</td>
            <td width="27%" class="normalfnt">Sales Order No</td>
            <td width="23%"><select name="cbStyleNo" style="width:120px" id="cbStyleNo">
                  <option value=""></option>
             </select></td>
            <td width="9%" class="normalfnt"><input type="text" id="txtPopGrnNo" value="<?php echo $grnNo; ?>" style="display:none" /></td>
            <td width="17%" align="right">&nbsp;</td>
            </tr> </div>           
<tr>
            <td width="24%" height="22" class="normalfnt">&nbsp;</td>
            <td width="27%" class="normalfnt">Main Category</td>
            <td width="23%"><select name="cboMainCategory" style="width:120px" id="cboMainCategory">
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td width="9%" class="normalfnt"><input type="text" id="txtPopGrnNo" value="<?php echo $grnNo; ?>" style="display:none" /></td>
            <td width="17%" align="right">&nbsp;</td>
            </tr>            
          <tr>
            <td width="24%" height="22" class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Sub Category</td>
            <td width="23%"><select name="cboSubCategory" style="width:120px" id="cboSubCategory">
                  <option value=""></option>
            </select></td>
            <td width="9%" class="normalfnt">&nbsp;</td>
            <td width="17%" align="right">&nbsp;</td>
            </tr>
<tr>
            <td width="24%" height="22" class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Description Like</td>
            <td width="23%">
              <input type="text" name="txtItmDesc" id="txtItmDesc"  style="width:120px" /></td>
            <td width="9%" class="normalfnt"><img src="../../../../images/smallSearch.png" width="24" height="24" class="mouseover" id="imgSearchItems" /></td>
            <td width="17%" align="right">&nbsp;</td>
            </tr>          </table></td>
      </tr>
      <?php //echo $pp
	  ?>
      
      <tr>
        <td><div style="width:820px;height:300px;overflow:scroll" >
          <table width="100%" class="bordered" id="tblItemsPopup" >
            <tr>
              <th width="10%" height="22" >Select</th>
              <th width="12%" >Main Catogery</th>
              <th width="12%" >Sub Catogery</th>
              <th width="10%" >Item Code</th>
              <th width="18%" >Description</th>
              <th width="6%" >UOM</th>
              <th width="11%" id="rwh6" style="display:none" >Order No</th>
              <th width="10%" id="rwh7" style="display:none" >Sales Order No</th>
              <th width="11%">Stock Bal</th>
              </tr>
            </table>
          </div></td>
      </tr>
        <td align="center" class="tableBorder_allRound">
        <a class="button white medium" id="butAdd" name="butAdd">Add</a>
        <a class="button white medium" id="butClose1" name="butClose1">Close</a>
        </td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>

</body>
</html>

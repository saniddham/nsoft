<?php
///////////////////////////////
// H B G KORALA   ////////////
//////////////////////////////
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];
$intUser  = $_SESSION["userId"];

//include_once  	"{$backwardseperator}dataAccess/permisionCheck.inc";
include_once  	"../../../../dataAccess/Connector.php";

require_once $_SESSION['ROOT_PATH']."class/warehouse/cls_warehouse_get.php";

$obj_wh_get		= new cls_warehouse_get($db);

$mrnNo = $_REQUEST['mrnNo'];
$year = $_REQUEST['year'];
$editMode=0;
$confirmatonMode=0;

$programName='Material Request Note';
$programCode='P0229';

$userDepartment=getUserDepartment($intUser);


if(($mrnNo=='')&&($year=='')){
	$savedStat = (int)getApproveLevel($programName);
	$department=loadDefaultDepartment($intUser);
	$remarks='';
	$intStatus=$savedStat+1;
	$date='';
}
else{
	$result=loadHeader($mrnNo,$year);
	while($row=mysqli_fetch_array($result))
	{
		$department=$row['department'];
		$remarks=$row['strRemarks'];
		$date = $row['datdate'];
		$savedStat=$row['intApproveLevels'];
		$intStatus=$row['intStatus'];
		$int25ExceedingApproved = $row['int25ExceedingApproved'];
		$int100ExceedingApproved = $row['int100ExceedingApproved'];
	}
}
	
	$editMode=loadEditMode($programCode,$intStatus,$savedStat,$intUser,$userDepartment);
	$confirmatonMode=loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser);
	if($mrnNo==''){
		$confirmatonMode=0;	
	}
 	if(($intStatus > 0) && ($intStatus<=$savedStat)){
		$editMode=0;	
	}
	$mainPage=$backwardseperator."main.php";
	
?> 
      
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Material Requisition Note</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="mrn-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body>
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>


<form id="frmMrn" name="frmMrn" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Material Requisition Note</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF"><tr><td>
   <table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="22" class="normalfnt">MRN No</td>
            <td width="22%"><input name="txtMrnNo" type="text" disabled="disabled" class="txtText" id="txtMrnNo" style="width:60px" value="<?php echo $mrnNo ?>" /><input name="txtYear" type="text" disabled="disabled" class="txtText" id="txtYear" style="width:40px"value="<?php echo $year ?>"  /></td>
            <td width="17%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="8%" class="normalfnt">Date</td>
            <td width="18%"><input name="dtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="dtDate" style="width:120px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="27" class="normalfnt">Department</td>
            <td width="39%"><select name="cboDepartment" id="cboDepartment" style="width:250px"  class="validate[required] txtText" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>>
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_department.intId,
							mst_department.strName
							FROM
							mst_department
							WHERE
							mst_department.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$department)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td width="25%">&nbsp;</td>
            <td width="11%" class="normalfnt">&nbsp;</td>
            <td width="15%">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
<td width="10%" height="27" rowspan="2" class="normalfnt">Note</td>
            <td width="36%" rowspan="2"><textarea name="txtRemarks" id="txtRemarks" cols="35" rows="3" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>><?php echo $remarks; ?></textarea></td>
            <td width="24%" rowspan="2"><div id="divPrice" style="display:none"><?php echo $priceEdit; ?></div></td>
            <td width="10%" rowspan="2" valign="top" class="normalfnt">&nbsp;</td>
            <td width="15%" align="right" valign="bottom">
        	<a class="button green medium" id="butAddItems" name="butAddItems" style="display:none">Add</a>
            </td>
            <td width="5%" align="right" valign="bottom">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr> <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="bordered" id="tblMrn" >
            <tr>
              <th width="6%" height="22" >Del</th>
              <th width="10%" >Order No</th>
              <th width="10%" >Sales Order  No</th>
              <th width="14%" >Main Category</th>
              <th width="13%" >Sub Category</th>
              <th width="29%" >Item Description</th>
              <th width="6%">UOM</th>
              <th width="12%"> Qty</th>
              </tr>
            <?php
			     $sql = "SELECT
				ware_mrndetails.dblQty, 
				mst_item.strName as itemName,
				mst_item.intBomItem,
				 mst_item.intId as intItemNo, 
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,
				ware_mrndetails.intOrderNo,
				ware_mrndetails.intOrderYear,
				ware_mrndetails.strStyleNo as strSalesOrderId,  
				trn_orderdetails.strSalesOrderNo, 
				mst_item.strCode, 
				mst_units.strCode as uom , 
				mst_part.strName as part
				FROM
				ware_mrndetails 
				left Join trn_orderdetails ON ware_mrndetails.intOrderNo = trn_orderdetails.intOrderNo AND ware_mrndetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_mrndetails.strStyleNo = trn_orderdetails.intSalesOrderId
				Inner Join mst_item ON ware_mrndetails.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				left Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
				WHERE 
				/*mst_item.intStatus = '1' AND*/ 
				ware_mrndetails.intMrnNo =  '$mrnNo' AND
				ware_mrndetails.intMrnYear =  '$year'  
				ORDER BY mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc 
";
				$result = $db->RunQuery($sql);
				$totAmm=0;
				while($row=mysqli_fetch_array($result))
				{
					
					$mainCatName=$row['mainCatName'];
					$subCatName=$row['subCatName'];
					$mainCatId=$row['intMainCategory'];
					$subCatId=$row['intSubCategory'];
					$itemId=$row['intItemNo'];
					$itemName=$row['itemName'];
					$uom=$row['uom'];
					$part=$row['part'];
					$allocatedQty=$row['dblAllocatedQty'];
					$mrnQty=$row['dblMrnQty'];
					$balToMrnQty=$allocatedQty-$mrnQty;
					if($row['intBomItem']==1){
						$balToMrnQty=10000;
					}
					$Qty=$row['dblQty'];
					
					$orderNo=$row['intOrderNo']."/".$row['intOrderYear'];
					$salesOrderId=$row['strSalesOrderId'];
					$salesOrderNo=$row['strSalesOrderNo']."/".$part;
					if($row['intOrderNo']==0){
						$orderNo='';
						$salesOrderId='';
						$salesOrderNo='';
					}
					
					$rowColor='#FFFFFF';
					if($orderNo !=''){
						$orderQty 		= $obj_wh_get->get_order_qty($row['intOrderNo'],$row['intOrderYear'],$row['strSalesOrderId'],'RunQuery');
						$sampleConpC	= $obj_wh_get->get_order_item_sample_consumption($row['intOrderNo'],$row['intOrderYear'],$row['strSalesOrderId'],$row['intItemNo'],'RunQuery');
						$actualConPC 	= $obj_wh_get->get_order_item_first_second_consumption($row['intOrderNo'],$row['intOrderYear'],$row['strSalesOrderId'],$row['intItemNo'],'RunQuery');
						$issuedQty	 	= $obj_wh_get->get_order_item_issued_qty($row['intOrderNo'],$row['intOrderYear'],$row['strSalesOrderId'],$row['intItemNo'],'RunQuery');
						//requestd 25%<qty<=100%(actual not raised)
						if((($Qty+$issuedQty) > round($orderQty*$sampleConpC*25/100) && (($Qty+$issuedQty) <= round($orderQty*$sampleConpC))) && ($actualConPC<=0)){
							$rowColor='#FFFFB0';
							if($int25ExceedingApproved==1){
								$rowColor='#FFFFFF';
							}
						}
						//requestd qty>100%(actual not raised)
						else if(((($Qty+$issuedQty) > round($orderQty*$sampleConpC))) && ($actualConPC<=0)){
							$rowColor='#FFCCCC';
							if($int100ExceedingApproved==1){
								$rowColor='#FFFFFF';
							}
						}
						//requestd qty>100%(actual raised)
						else if(((($Qty+$issuedQty) > round($orderQty*$actualConPC))) && ($actualConPC>0)){
							$rowColor='#80FF80';
							if($int100ExceedingApproved==1){
								$rowColor='#FFFFFF';
							}
						}
						
					}
					
				?>
        <tr class="normalfnt"  bgcolor="<?php echo $rowColor; ?>">
            <td align="center"><?php if($editMode==1){  ?><img class="delImg" src="../../../../images/del.png" width="15" height="15" /><?php } ?></td>
			<td align="center" id="<?php echo $orderNo ?>" class="orderNo"><?php echo $orderNo ?></td>
			<td align="center" id="<?php echo $salesOrderId ?>" class="salesOrderNo"><?php echo $salesOrderNo ?></td>
			<td align="center" id="<?php echo $maincatId ?>"><?php echo $mainCatName ?></td>
			<td align="center" id="<?php echo $subCatId ?>"><?php echo $subCatName ?></td>
			<td align="center" id="<?php echo $itemId ?>" class="item"><?php echo $itemName ?></td>
			<td align="center" id=""><?php echo $uom ?></td>
			<td align="center" id=""><input  id="<?php echo $Qty ?>" class="validate[required,custom[number]] calculateValue Qty" style="width:80px;text-align:center" type="text" value="<?php echo $Qty ?>" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/></td>
			</tr>            <?php
	}

			   ?>
          </table>
        </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound">
        <a class="button white medium" id="butNew" name="butNew">New</a>
        <a class="button white medium" id="butSave" name="butSave" style=" <?php if($editMode==0){  echo 'display:none'; } else { ?>  <?php echo 'display:yes';  } ?>">Save</a>
        <a class="button white medium" id="butConfirm" name="butConfirm" style=" <?php if(($intStatus>1) and ($confirmatonMode==1)){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>  ">Confirm</a>
        <a class="button white medium" id="butReport" name="butReport">Report</a>
        <a id="butClose" class="button white medium" name="butClose" href="<?php echo $mainPage; ?>">Close</a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>


</body>
</html>
<?php
//------------------------------function load Header---------------------
function loadHeader($mrnNo,$year)
{
	global $db;
	  $sql = "SELECT
			ware_mrnheader.datdate,
			ware_mrnheader.intStatus,
			ware_mrnheader.intApproveLevels,
			ware_mrnheader.int25ExceedingApproved,
			ware_mrnheader.int100ExceedingApproved,
			ware_mrnheader.intUser,
			ware_mrnheader.strRemarks, 
			sys_users.strUserName, 
			ware_mrnheader.intDepartment as department 
			FROM
			ware_mrnheader 
			Inner Join mst_locations ON ware_mrnheader.intCompanyId = mst_locations.intId
			Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
			Inner Join sys_users ON ware_mrnheader.intUser = sys_users.intUserId
			WHERE
			ware_mrnheader.intMrnNo =  '$mrnNo' AND
			ware_mrnheader.intMrnYear =  '$year' 
			GROUP BY
			ware_mrnheader.intMrnNo,
			ware_mrnheader.intMrnYear"; 
			 $result = $db->RunQuery($sql);
			 return $result;
}
//------------------------------function load Default Department---------------------
function loadDefaultDepartment($intUser){
	global $db;
	 $sql1 = "SELECT
				sys_users.intDepartmentId
				FROM
				sys_users
				WHERE
				sys_users.intUserId =  '$intUser'";	
	$result1 = $db->RunQuery($sql1);
	$row1=mysqli_fetch_array($result1);
	$department=$row1['intDepartmentId'];
	return $department;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser,$depatment){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	  $sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId 
		Inner Join sys_users ON sys_users.intUserId = menupermision.intUserId
		WHERE 
		sys_users.intDepartmentId =  '$depatment' 
		AND menus.strCode =  '$programCode' 
		AND menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
		 
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//------------------------------function load User Department---------------------
function getUserDepartment($intUser){
	global $db;

	$sqlp = "SELECT
			sys_users.intDepartmentId
			FROM
			sys_users
			WHERE
			sys_users.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['intDepartmentId'];

}
?>

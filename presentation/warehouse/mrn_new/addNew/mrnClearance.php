<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];
$intUser  = $_SESSION["userId"];

include  	"{$backwardseperator}dataAccess/permisionCheck.inc";
//include  	"{$backwardseperator}dataAccess/Connector.php";

$mrnNo = $_GET['cboMrnNo'];
$year = $_GET['cboMrnYear'];

$programName='Material Request Note';
$programCode='P0229';

 $userDepartment=getUserDepartment($intUser);

	  $sql = "SELECT
ware_mrnheader.datdate,
ware_mrnheader.intStatus,
ware_mrnheader.intApproveLevels,
ware_mrnheader.intUser,
sys_users.strUserName, 
ware_mrnheader.intDepartment as department 
FROM
ware_mrnheader 
Inner Join mst_locations ON ware_mrnheader.intCompanyId = mst_locations.intId
Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
Inner Join sys_users ON ware_mrnheader.intUser = sys_users.intUserId
WHERE
ware_mrnheader.intMrnNo =  '$mrnNo' AND
ware_mrnheader.intMrnYear =  '$year' 
GROUP BY
ware_mrnheader.intMrnNo,
ware_mrnheader.intMrnYear"; 
			 $result = $db->RunQuery($sql);
			 while($row=mysqli_fetch_array($result))
			 {
				$department=$row['department'];
				$date = $row['datdate'];
				$intStatus = $row['intStatus'];
				$savedStat=$row['intApproveLevels'];
			 }
					//default user department
					if($mrnNo==''){
					
					 $sql1 = "SELECT
								sys_users.intDepartmentId
								FROM
								sys_users
								WHERE
								sys_users.intUserId =  '$intUser'";	
					$result1 = $db->RunQuery($sql1);
					$row1=mysqli_fetch_array($result1);
					$department=$row1['intDepartmentId'];
					}
					
					
	$clearMode=loadClearMode($programCode,$intStatus,$savedStat,$intUser,$userDepartment);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Material Requisition Clearance</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="mrn-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body>
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>


<form id="frmMrnClearance" name="frmMrnClearance" method="get" action="mrnClearance.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Material Requisition Clearance</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="22" class="normalfnt">MRN No</td>
            <td width="22%"><select name="cboMrnNo" id="cboMrnNo" style="width:80px"  class="validate[required] txtText"  onchange="submit()";>
                  <option value=""></option>
                  <?php
					$sql = "SELECT DISTINCT 
							ware_mrnheader.intMrnNo
							FROM ware_mrnheader
							Inner Join ware_mrndetails ON 
							ware_mrnheader.intMrnNo=ware_mrndetails.intMrnNo AND ware_mrnheader.intMrnYear=ware_mrndetails.intMrnYear  
							Left Join sys_users ON ware_mrnheader.intUser = sys_users.intUserId
							WHERE
							ware_mrnheader.intStatus >=  '1' AND
							ware_mrnheader.intApproveLevels >= ware_mrnheader.intStatus AND 
							ware_mrndetails.dblQty-ware_mrndetails.dblIssudQty>0 AND  
							sys_users.intDepartmentId =  '$userDepartment' ";
					if($year!=''){
					$sql .= " AND ware_mrnheader.intMrnYear =  '$year' ";	
					}
					$sql .= " ORDER BY ware_mrnheader.intMrnNo DESC,ware_mrnheader.intMrnYear DESC"; 
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intMrnNo']==$mrnNo)
						echo "<option value=\"".$row['intMrnNo']."\" selected=\"selected\">".$row['intMrnNo']."</option>";	
						else
						echo "<option value=\"".$row['intMrnNo']."\">".$row['intMrnNo']."</option>";	
					}
				?>
            </select>
            <select name="cboMrnYear" id="cboMrnYear" style="width:60px"  class="validate[required] txtText"  onchange="submit()";>
                  <option value=""></option>
                  <?php
					$sql = "SELECT DISTINCT
							ware_mrnheader.intMrnYear
							FROM ware_mrnheader 
							WHERE
							ware_mrnheader.intStatus >=  '1' AND
							ware_mrnheader.intApproveLevels >= ware_mrnheader.intStatus AND
							ware_mrnheader.intUser =  '$intUser' 
							order by ware_mrnheader.intMrnYear desc";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intMrnYear']==$year)
						echo "<option value=\"".$row['intMrnYear']."\" selected=\"selected\">".$row['intMrnYear']."</option>";	
						else
						echo "<option value=\"".$row['intMrnYear']."\">".$row['intMrnYear']."</option>";	
					}
				?>
            </select></td>
            <td width="17%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="8%" class="normalfnt">Date</td>
            <td width="18%"><input name="dtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="dtDate" style="width:120px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="27" class="normalfnt">Department</td>
            <td width="39%"><select name="cboDepartment" id="cboDepartment" style="width:250px"  class="validate[required] txtText" >
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_department.intId,
							mst_department.strName
							FROM
							mst_department
							WHERE
							mst_department.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$department)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td width="25%">&nbsp;</td>
            <td width="11%" class="normalfnt">&nbsp;</td>
            <td width="15%">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="92%">&nbsp;</td>
            <td width="8%"></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="grid" id="tblMrn" >
            <tr class="gridHeader">
              <td width="3%" height="22" >Del</td>
              <td width="6%" >Order No</td>
              <td width="6%" >Sales Order  No</td>
              <td width="12%" >Main Category</td>
              <td width="10%" >Sub Category</td>
              <td width="29%" >Item Description</td>
              <td width="7%">UOM</td>
              <td width="8%"> MRN Qty</td>
              <td width="8%"> Issue Qty</td>
              <td width="11%"> Qty</td>
              </tr>
            <?php
			     $sql = "SELECT
				ware_mrndetails.dblQty, 
				ware_mrndetails.dblIssudQty, 
				mst_item.strName as itemName,
				mst_item.intBomItem,
				 mst_item.intId as intItemNo, 
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,
				ware_mrndetails.intOrderNo,
				ware_mrndetails.intOrderYear,
				ware_mrndetails.strStyleNo as strSalesOrderId,  
				trn_orderdetails.strSalesOrderNo, 
				mst_item.strCode, 
				mst_units.strCode as uom  
				FROM
				ware_mrndetails 
				left Join trn_orderdetails ON ware_mrndetails.intOrderNo = trn_orderdetails.intOrderNo AND ware_mrndetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_mrndetails.strStyleNo = trn_orderdetails.intSalesOrderId
				Inner Join mst_item ON ware_mrndetails.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				WHERE
				ware_mrndetails.intMrnNo =  '$mrnNo' AND
				ware_mrndetails.intMrnYear =  '$year'  AND 
				ware_mrndetails.dblQty-ware_mrndetails.dblIssudQty>0  
				ORDER BY mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc 
";
				$result = $db->RunQuery($sql);
				$totAmm=0;
				while($row=mysqli_fetch_array($result))
				{
					
					$orderNo=$row['intOrderNo']."/".$row['intOrderYear'];
					if($row['intOrderNo']==0){
						$orderNo='';
					}
					$salesOrderId=$row['strSalesOrderId'];
					$salesOrderNo=$row['strSalesOrderNo'];
					$mainCatName=$row['mainCatName'];
					$subCatName=$row['subCatName'];
					$mainCatId=$row['intMainCategory'];
					$subCatId=$row['intSubCategory'];
					$itemId=$row['intItemNo'];
					$itemName=$row['itemName'];
					$uom=$row['uom'];
					$Qty=$row['dblQty'];
					$issueQty=$row['dblIssudQty'];
				?>
<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="../../../../images/del.png" width="15" height="15" /></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $orderNo ?>" class="orderNo"><?php echo $orderNo ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $salesOrderId ?>" class="salesOrderNo"><?php echo $salesOrderNo ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $maincatId ?>"><?php echo $mainCatName ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $subCatId ?>"><?php echo $subCatName ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $itemId ?>" class="item"><?php echo $itemName ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $uom ?>"><?php echo $uom ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $Qty ?>" class="mrnQty"><?php echo $Qty ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $issueQty ?>"><?php echo $issueQty ?></td>
			<td align="center" bgcolor="#FFFFFF" id=""><input  id="<?php echo $Qty ?>" class="validate[required,custom[number],max[<?php echo ($Qty-$issueQty) ?>]] calculateValue Qty" style="width:80px;text-align:center" type="text" value="0"/></td>
			</tr>            <?php
				}
			   ?>
          </table>
        </div></td>
      </tr>
      <?php
	 // echo $clearMode;
	  ?>

  <tr>
        <td align="center" class="tableBorder_allRound"><?php if($clearMode==1){ ?><img src="../../../../images/clear.png" width="92" height="24" id="butClear" name="butClear" class="mouseover" /><?php } ?><img src="../../../../planning/img/Tclose.jpg" width="92" height="24" class="mouseover" /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>


</body>
</html>
<?php
//------------------------------function load loadClearMode---------------------
function loadClearMode($programCode,$intStatus,$savedStat,$intUser,$depatment){
	global $db;
	
	//  echo $intStatus;
	$clearMode=0;
	  $sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId 
		Inner Join sys_users ON sys_users.intUserId = menupermision.intUserId
		WHERE 
		sys_users.intDepartmentId =  '$depatment' 
		AND menus.strCode =  '$programCode' 
		AND menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus<=($savedStat) || ($intStatus==0)){ 
				 $clearMode=1;
			 }
		 }
			 
	return $clearMode;
}
//------------------------------function load User Department---------------------
function getUserDepartment($intUser){
	global $db;

	$sqlp = "SELECT
			sys_users.intDepartmentId
			FROM
			sys_users
			WHERE
			sys_users.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['intDepartmentId'];

}
?>

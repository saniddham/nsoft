<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include_once "{$backwardseperator}dataAccess/Connector.php";
require_once $_SESSION['ROOT_PATH']."class/warehouse/cls_warehouse_get.php";

$obj_wh_get		= new cls_warehouse_get($db);

$mrnNo = $_REQUEST['mrnNo'];
$year = $_REQUEST['year'];
$approveMode = $_REQUEST['approveMode'];

$programName='Material Request Note';
$programCode='P0229';
$mrnApproveLevel = (int)getApproveLevel($programName);


/*$mrnNo = '100000';
$year = '2012';
$approveMode=1;
*/
 $sql = "SELECT
ware_mrnheader.datdate,
ware_mrnheader.strRemarks, 
ware_mrnheader.intStatus,
ware_mrnheader.intApproveLevels,
ware_mrnheader.intUser,
ware_mrnheader.intCompanyId as mrnRaisedLocationId, 
sys_users.strUserName, 
mst_department.strName as department 
FROM
ware_mrnheader 
Inner Join mst_department ON ware_mrnheader.intDepartment = mst_department.intId
Inner Join mst_locations ON ware_mrnheader.intCompanyId = mst_locations.intId
Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
Inner Join sys_users ON ware_mrnheader.intUser = sys_users.intUserId
WHERE
ware_mrnheader.intMrnNo =  '$mrnNo' AND
ware_mrnheader.intMrnYear =  '$year' 
GROUP BY
ware_mrnheader.intMrnNo,
ware_mrnheader.intMrnYear
";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$locationId = $row['mrnRaisedLocationId'];//this locationId use in report header(reportHeader.php)--------------------
					$department=$row['department'];
					$mrnDate = $row['datdate'];
					$mrnBy = $row['strUserName'];
					$intStatus = $row['intStatus'];
					$savedLevels = $row['intApproveLevels'];
					$user = $row['strUserName'];
					$createdUser = $row['intUser'];
					$remarks = $row['strRemarks'];
				 }
				 
$confirmationMode=loadConfirmatonMode($programCode,$intStatus,$savedLevels,$intUser);
$rejectionMode=loadRejectionMode($programCode,$intStatus,$savedLevels,$intUser);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MRN Report - Exceeding Q</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="rptMrn-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:271px;
	top:170px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="../../../../images/pending.png"  /></div>
<?php
}
?>
<form id="frmMrnReport" name="frmMrnReport" method="post" action="rptMrn.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>MATERIAL REQUISITION NOTE</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
    <?php
	if($intStatus>1)
	{
	?>
    <?php if($approveMode==1){
			if($confirmationMode==1) { 
			?>
			<img src="../../../../images/approve.png" align="middle" class="mouseover noPrint" id="imgApprove" />
			<?php
			}
			if($rejectionMode==1) { 
			?>
            <img src="../../../../images/reject.png" align="middle" class="mouseover noPrint" id="imgReject" />
                <?php
                }
            }
	}
	?>
    </td>
  </tr>
  <tr>
  <?php
 	if($intStatus==1)
	{
	?>
   <td colspan="9" class="APPROVE" >CONFIRMED</td>
   <?PHP
	}
	else if($intStatus==0)
	{
   ?>
   <td colspan="9" class="APPROVE" style="color:#F00">REJECTED</td>
   <?php
	}
	else
	{
   ?>
   <td width="10%" colspan="9" class="APPROVE">PENDING</td>
   <?php
	}
   ?>
   <?php
   //-----------------
	  $sqlc = "SELECT
			ware_mrnheader_approvedby.intApproveUser,
			ware_mrnheader_approvedby.dtApprovedDate,
			sys_users.strUserName as UserName,
			ware_mrnheader_approvedby.intApproveLevelNo
			FROM
			ware_mrnheader_approvedby
			Inner Join sys_users ON ware_mrnheader_approvedby.intApproveUser = sys_users.intUserId
			WHERE
			ware_mrnheader_approvedby.intMrnNo =  '$mrnNo' AND
			ware_mrnheader_approvedby.intYear =  '$year' AND
			ware_mrnheader_approvedby.intApproveLevelNo =  '$intStatus'
";
	 $resultc = $db->RunQuery($sqlc);
	 $rowc=mysqli_fetch_array($resultc);
	 $rejectBy=$rowc['UserName'];
	 $rejectDate=$rowc['dtApprovedDate'];

   //-----------------
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="12%"><span class="normalfnt"><strong>MRN No</strong></span></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="33%"><span class="normalfnt"><?php echo $mrnNo ?>/<?php echo $year ?></span></td>
    <td width="12%" class="normalfnt"><strong>Department</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="14%"><span class="normalfnt"><?php echo $department ?></span></td>
    <td width="1%"><div id="divMrnNo" style="display:none"><?php echo $mrnNo ?>/<?php echo $year ?></div></td>
  <td width="5%"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>MRN By</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $user  ?></span></td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    
    <?php if($intStatus==0){
					 $sqlc = "SELECT
							ware_mrnheader_approvedby.intApproveUser,
							ware_mrnheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_mrnheader_approvedby.intApproveLevelNo
							FROM
							ware_mrnheader_approvedby
							Inner Join sys_users ON ware_mrnheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_mrnheader_approvedby.intMrnNo =  '$mrnNo' AND
							ware_mrnheader_approvedby.intYear =  '$year' AND
							ware_mrnheader_approvedby.intApproveLevelNo =  '0'
";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
					 $rejectBy=$rowc['UserName'];
					 $rejectDate=$rowc['dtApprovedDate'];
	}
 	?>
    
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Date</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $mrnDate  ?></span></td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  
  <tr>
<tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Remarks</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td colspan="7" align="left"><span class="normalfnt"><?php echo $remarks ?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr>
              <th width="8%" >Order No</th>
              <th width="12%" >Sales Order  No</th>
              <th width="14%" >Main Category</th>
              <th width="11%" >Sub Category</th>
              <th width="9%" >Item Code</th>
              <th width="25%" >Item</th>
              <th width="6%" >UOM</th>
              <th width="6%" >Qty</th>
              <th width="9%" >Cleared Qty</th>
              </tr>
              <?php 
	  	   $sql1 = "SELECT
ware_mrndetails.intMrnNo,
ware_mrndetails.intMrnYear,
ware_mrndetails.intOrderNo,
ware_mrndetails.intOrderYear,
ware_mrndetails.strStyleNo,
trn_orderdetails.strSalesOrderNo,
trn_orderdetails.intSalesOrderId,
ware_mrndetails.intItemId,
ware_mrndetails.dblQty,
ware_mrndetails.dblMRNClearQty, 
mst_item.strCode,
mst_item.strName,
mst_item.intUOM, 
mst_maincategory.strName AS mainCategory,
mst_subcategory.strName AS subCategory, 
mst_units.strCode as uom , 
mst_part.strName as part 
FROM ware_mrndetails 
left Join trn_orderdetails ON ware_mrndetails.intOrderNo = trn_orderdetails.intOrderNo AND ware_mrndetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_mrndetails.strStyleNo = trn_orderdetails.intSalesOrderId
Inner Join mst_item ON ware_mrndetails.intItemId = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
left Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
WHERE
ware_mrndetails.intMrnNo =  '$mrnNo' AND
ware_mrndetails.intMrnYear =  '$year'  
 Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc";
			$result1 = $db->RunQuery($sql1);
			$totQty=0;
			$totClearQty=0;
			$totAmmount=0;
			while($row=mysqli_fetch_array($result1))
			{
				$orderNo=$row['intOrderNo']."/".$row['intOrderYear'];
				$salesOrderNo=$row['strSalesOrderNo']."/".$row['part'];
				if($row['intOrderNo']==0){
					$orderNo='';
					$salesOrderNo='';	
				}
				$rowColor='#FFFFFF';
				if($orderNo != ''){
					$orderQty 		= $obj_wh_get->get_order_qty($row['intOrderNo'],$row['intOrderYear'],$row['intSalesOrderId'],'RunQuery');
					$sampleConpC	= $obj_wh_get->get_order_item_sample_consumption($row['intOrderNo'],$row['intOrderYear'],$row['intSalesOrderId'],$row['intItemId'],'RunQuery');
					$actualConPC 	= $obj_wh_get->get_order_item_first_second_consumption($row['intOrderNo'],$row['intOrderYear'],$row['intSalesOrderId'],$row['intItemId'],'RunQuery');
					$issuedQty	 	= $obj_wh_get->get_order_item_issued_qty($row['intOrderNo'],$row['intOrderYear'],$row['intSalesOrderId'],$row['intItemId'],'RunQuery2');
					if((($row['dblQty']+$issuedQty) > $orderQty*$sampleConpC*25/100) && ($actualConPC<=0)){
						$rowColor='#FFE1E1';
					}
				}
	  ?>
            <tr class="normalfnt"  bgcolor="<?php echo $rowColor; ?>">
              <td class="normalfnt" >&nbsp;<?php echo $orderNo;?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $salesOrderNo?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['mainCategory']?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['subCategory'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strCode'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strName'] ?>&nbsp;</td>
              <td class="normalfntMid" >&nbsp;<?php echo $row['uom'] ?></td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblQty'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblMRNClearQty'] ?>&nbsp;</td>
              </tr>
      <?php 
			$totQty+=$row['dblQty'];
			$totClearQty+=$row['dblMRNClearQty'];
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $totQty ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $totClearQty ?>&nbsp;</td>
              </tr>
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<?php  
					 $flag=0;
					  $sqlc = "SELECT
							ware_mrnheader_approvedby.intApproveUser,
							ware_mrnheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_mrnheader_approvedby.intApproveLevelNo
							FROM
							ware_mrnheader_approvedby
							Inner Join sys_users ON ware_mrnheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_mrnheader_approvedby.intMrnNo =  '$mrnNo' AND
							ware_mrnheader_approvedby.intYear =  '$year'  /*AND 
							ware_mrnheader_approvedby.intStatus='0'  */
							order by dtApprovedDate asc";
					 $resultc = $db->RunQuery($sqlc);
					while($rowc=mysqli_fetch_array($resultc)){
						if($rowc['intApproveLevelNo']==1)
						$desc="1st ";
						else if($rowc['intApproveLevelNo']==2)
						$desc="2nd ";
						else if($rowc['intApproveLevelNo']==3)
						$desc="3rd ";
						else
						$desc=$rowc['intApproveLevelNo']."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
					 
					 if($rowc['intApproveLevelNo']>0){
						 $flag=1;
				?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
            </tr>
<?php
	//}
	//else{			
					}
					 if($rowc['intApproveLevelNo']==0){
						 $flag=2;
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Rejected By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
					 }
					 if($rowc['intApproveLevelNo']==-1){
						 $flag=3;
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Revised By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
					 }
					 
		 if($rowc['intApproveLevelNo']>0){
			 $finalSavedLevel=$rowc['intApproveLevelNo'];
		 }
	 }//end of while
	 
	if($flag>1){
		$finalSavedLevel=0;
	}
	
	if(($flag<=1) || ($intStatus>0)){
	for($j=$finalSavedLevel+1; $j<=$savedLevels; $j++)
	{ 
		if($j==1)
		$desc="1st ";
		else if($j==2)
		$desc="2nd ";
		else if($j==3)
		$desc="3rd ";
		else
		$desc=$j."th ";
		$desc2='---------------------------------';
	?>
        <tr>
            <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
        </tr>
<?php
	}
	}
?>
<tr height="90" >
  <td align="center" class="normalfntMid"></td>
</tr>
<tr height="40">
<?php
	//$locationId = '';
	$sql = "SELECT mst_locations.intCompanyId FROM mst_locations WHERE mst_locations.intId =  '$locationId'";
	$result 	= $db->RunQuery($sql);
	$row		= mysqli_fetch_array($result);
	$createCompanyId = $row['intCompanyId'];
?>
<?Php 
	$url  = "{$backwardseperator}presentation/sendToApproval.php";		// * file name
	$url .= "?status=$intStatus";										// * set recent status
	$url .= "&approveLevels=$savedLevels";								// * set approve levels in order header
	$url .= "&programCode=$programCode";								// * program code (ex:P10001)
	$url .= "&program=MRN";									// * program name (ex:Purchase Order)
	$url .= "&companyId=$createCompanyId";									// * created company id
	$url .= "&createUserId=$createdUser";	
	
	$url .= "&field1=MRN No";												 
	$url .= "&field2=MRN Year";	
	$url .= "&value1=$mrnNo";												 
	$url .= "&value2=$year";	
	
	$url .= "&subject=MRN FOR APPROVAL ('$mrnNo'/'$year')";	
	
	$url .= "&statement1=Please Approve this";	
	$url .= "&statement2=to Approve this";	
							// * doc year
	$url .= "&link=".urlencode(base64_encode($mainPath."presentation/warehouse/mrn/listing/rptMrn.php?mrnNo=$mrnNo&year=$year&approveMode=1"));
?>
  <td align="center" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:150px;border:none"  ></iframe></td>
</tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
	
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//-------------------------------------------------------------------------
?>	

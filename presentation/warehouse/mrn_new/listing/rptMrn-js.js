//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
// JavaScript Document

$(document).ready(function() {
	//$('#frmMrnReport').validationEngine();
	
 
	
	$('#frmMrnReport #imgApprove').click(function(){
	var val = $.prompt('Are you sure you want to approve this MRN ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					if(validateQuantities()==0){
					///////////
					var url = "rptMrn-db-set.php"+window.location.search+'&status=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmMrnReport #imgApprove').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmMrnReport #imgApprove').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
 					   hideWaiting();
					}
				}});
	});
	
	$('#frmMrnReport #imgReject').click(function(){
	var val = $.prompt('Are you sure you want to reject this MRN ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
					showWaiting();
					if(validateRejecton()==0){
					///////////
					var url = "rptMrn-db-set.php"+window.location.search+'&status=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmMrnReport #imgReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmMrnReport #imgReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
						hideWaiting();
			}
		}});
	});
	
	
	$('#frmMrnReport #imgAppQty25').click(function(){
		approveQty25();
	});
	$('#frmMrnReport #imgRejQty25').click(function(){
		rejectQty25();
	});
	$('#frmMrnReport #imgAppQty100').click(function(){
		approveQty100();
	});
	$('#frmMrnReport #imgRejQty100').click(function(){
		rejectQty100();
	});

	
});



//---------------------------------------------------
function validateQuantities(){
		var ret=0;	
	    var mrnNo = document.getElementById('divMrnNo').innerHTML;
		var url 		= "rptMrn-db-get.php?requestType=getValidation";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"mrnNo="+mrnNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",3000);
					  ret= 1;
					}
				}
			});
			return ret;

}
//---------------------------------------------------
function validateRejecton(){
		var ret=0;	
	    var mrnNo = document.getElementById('divMrnNo').innerHTML;
		var url 		= "rptMrn-db-get.php?requestType=validateRejecton";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"mrnNo="+mrnNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",3000);
					  ret= 1;
					}
				}
			});
			return ret;

}

function approveQty25(){
	
	var val = $.prompt('Are you sure you want to approve this MRN Qtys ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					//if(validateQuantities()==0){
					///////////
					var url = "rptMrn-db-set.php"+window.location.search+'&status=approveQty_25';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmMrnReport #imgAppQty25').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmMrnReport #imgAppQty25').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						//}
 					   hideWaiting();
					}
				}});
	
}
function rejectQty25(){

	var val = $.prompt('Are you sure you want to reject this MRN Qtys ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
					showWaiting();
					//if(validateRejecton()==0){
					///////////
					var url = "rptMrn-db-set.php"+window.location.search+'&status=rejectQty_25';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmMrnReport #imgRejQty25').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmMrnReport #imgRejQty25').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						//}
						hideWaiting();
			}
		}});
	
}
function approveQty100(){
	
	var val = $.prompt('Are you sure you want to approve this MRN Qtys ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					//if(validateQuantities()==0){
					///////////
					var url = "rptMrn-db-set.php"+window.location.search+'&status=approveQty_100';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmMrnReport #imgAppQty100').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmMrnReport #imgAppQty100').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						//}
 					   hideWaiting();
					}
				}});
	
}
function rejectQty100(){

	var val = $.prompt('Are you sure you want to reject this MRN Qtys ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
					showWaiting();
					//if(validateRejecton()==0){
					///////////
					var url = "rptMrn-db-set.php"+window.location.search+'&status=rejectQty_100';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmMrnReport #imgRejQty100').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmMrnReport #imgRejQty100').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						//}
						hideWaiting();
			}
		}});
	
}

//------------------------------------------------
function alertx()
{
	$('#frmMrnReport #imgApprove').validationEngine('hide')	;
}
//--------------------------------------------
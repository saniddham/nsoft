<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";

$mrnNo = $_REQUEST['mrnNo'];
$year = $_REQUEST['year'];
$approveMode = $_REQUEST['approveMode'];

$programName='Material Request Note';
$programCode='P0229';
$mrnApproveLevel = (int)getApproveLevel($programName);

/*$mrnNo = '100000';
$year = '2012';
$approveMode=1;
*/
 $sql = "SELECT
ware_mrnheader.datdate,
ware_mrnheader.intStatus,
ware_mrnheader.intApproveLevels,
ware_mrnheader.intUser,
ware_mrnheader.intCompanyId as mrnRaisedLocationId, 
sys_users.strUserName, 
mst_department.strName as department 
FROM
ware_mrnheader 
Inner Join mst_department ON ware_mrnheader.intDepartment = mst_department.intId
Inner Join mst_locations ON ware_mrnheader.intCompanyId = mst_locations.intId
Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
Inner Join sys_users ON ware_mrnheader.intUser = sys_users.intUserId
WHERE
ware_mrnheader.intMrnNo =  '$mrnNo' AND
ware_mrnheader.intMrnYear =  '$year' 
GROUP BY
ware_mrnheader.intMrnNo,
ware_mrnheader.intMrnYear
";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$locationId = $row['mrnRaisedLocationId'];//this locationId use in report header(reportHeader.php)--------------------
					$department=$row['department'];
					$mrnDate = $row['datdate'];
					$mrnBy = $row['strUserName'];
					$intStatus = $row['intStatus'];
					$savedLevels = $row['intApproveLevels'];
					$user = $row['strUserName'];
				 }
				 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MRN Report</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="rptMrn-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:267px;
	top:348px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<?php
}
?>
<form id="frmMrnReport" name="frmMrnReport" method="post" action="rptMrn.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>MATERIAL REQUETION NOTE STATUS</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
    <?php
	if($intStatus>1)
	{
	
	//------------
	$k=$savedLevels+2-$intStatus;
	   $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
	
	 $resultp = $db->RunQuery($sqlp);
	 $rowp=mysqli_fetch_array($resultp);
	 $userPermission=0;
	 if($rowp['int'.$k.'Approval']==1){
		 $userPermission=1;
	 }
	//--------------	
	?>
    <?php if(($approveMode==1) and ($userPermission==1)) { ?>
    <?php
	}
	}
	?></td>
  </tr>
  <tr>
   <?php
   //-----------------
	  $sqlc = "SELECT
			ware_mrnheader_approvedby.intApproveUser,
			ware_mrnheader_approvedby.dtApprovedDate,
			sys_users.strUserName as UserName,
			ware_mrnheader_approvedby.intApproveLevelNo
			FROM
			ware_mrnheader_approvedby
			Inner Join sys_users ON ware_mrnheader_approvedby.intApproveUser = sys_users.intUserId
			WHERE
			ware_mrnheader_approvedby.intMrnNo =  '$mrnNo' AND
			ware_mrnheader_approvedby.intYear =  '$year' AND
			ware_mrnheader_approvedby.intApproveLevelNo =  '$intStatus' AND 
			ware_mrnheader_approvedby.intStatus='0' 
";
	 $resultc = $db->RunQuery($sqlc);
	 $rowc=mysqli_fetch_array($resultc);
	 $rejectBy=$rowc['UserName'];
	 $rejectDate=$rowc['dtApprovedDate'];

   //-----------------
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="12%"><span class="normalfnt"><strong>MRN No</strong></span></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="33%"><span class="normalfnt"><?php echo $mrnNo ?>/<?php echo $year ?></span></td>
    <td width="12%" class="normalfnt"><strong>Department</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="14%"><span class="normalfnt"><?php echo $department ?></span></td>
    <td width="1%"><div id="divMrnNo" style="display:none"><?php echo $mrnNo ?>/<?php echo $year ?></div></td>
  <td width="5%"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>MRN By</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $user  ?></span></td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    
    <?php if($intStatus==0){
					 $sqlc = "SELECT
							ware_mrnheader_approvedby.intApproveUser,
							ware_mrnheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_mrnheader_approvedby.intApproveLevelNo
							FROM
							ware_mrnheader_approvedby
							Inner Join sys_users ON ware_mrnheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_mrnheader_approvedby.intMrnNo =  '$mrnNo' AND
							ware_mrnheader_approvedby.intYear =  '$year' AND
							ware_mrnheader_approvedby.intApproveLevelNo =  '0' AND 
							ware_mrnheader_approvedby.intStatus='0' 
";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
					 $rejectBy=$rowc['UserName'];
					 $rejectDate=$rowc['dtApprovedDate'];
	}
 	?>
    
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Date</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $mrnDate  ?></span></td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="gridHeader">
              <td width="8%" >Order No</td>
              <td width="8%" >Sales Order No</td>
              <td width="9%" >Main Category</td>
              <td width="10%" >Sub Category</td>
              <td width="8%" >Item Code</td>
              <td width="18%" >Item</td>
              <td width="7%" >MRN Qty</td>
              <td width="7%" >Issue No</td>
              <td width="7%" >Issue Qty</td>
              <td width="9%" >Bal To Issue</td>
              <td width="9%" >Stock Qty</td>
              </tr>
              <?php 
	  	    $sql1 = "SELECT
ware_issuedetails.strOrderNo,
ware_issuedetails.intOrderYear,
trn_orderdetails.intSalesOrderId,  
trn_orderdetails.strSalesOrderNo, 
mst_item.strName,
mst_item.intBomItem , 
mst_maincategory.strName as mainCategory,
mst_subcategory.strName as subCategory,
ware_mrndetails.dblQty,
ware_issuedetails.intIssueNo,
ware_issuedetails.intIssueYear,
ware_issueheader.intStatus,
ware_issuedetails.dblQty as issuedQty,
ware_mrndetails.dblIssudQty,
(ware_mrndetails.dblQty-ware_mrndetails.dblIssudQty) as balToIssueQty,
ware_mrndetails.intItemId,
mst_item.strCode
FROM
ware_mrndetails
Inner Join ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
Left Join ware_issuedetails ON ware_mrndetails.intMrnNo = ware_issuedetails.intMrnNo AND ware_mrndetails.intMrnYear = ware_issuedetails.intMrnYear AND ware_mrndetails.intOrderNo = ware_issuedetails.strOrderNo AND ware_mrndetails.intOrderYear = ware_issuedetails.intOrderYear AND ware_mrndetails.strStyleNo = ware_issuedetails.strStyleNo AND ware_mrndetails.intItemId = ware_issuedetails.intItemId
left Join trn_orderdetails ON ware_mrndetails.intOrderNo = trn_orderdetails.intOrderNo AND ware_mrndetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_mrndetails.strStyleNo = trn_orderdetails.intSalesOrderId
Left Join ware_issueheader ON ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo AND ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear
Inner Join mst_item ON ware_mrndetails.intItemId = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
WHERE 
ware_mrnheader.intStatus =  '1' AND 
ware_mrndetails.intMrnNo =  '$mrnNo' AND
ware_mrndetails.intMrnYear =  '$year'  
 Order by ware_issuedetails.strOrderNo asc,
ware_issuedetails.intOrderYear asc,
ware_issuedetails.strStyleNo asc,mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc";
			$result1 = $db->RunQuery($sql1);
			$totQty=0;
			$totAmmount=0;
			while($row=mysqli_fetch_array($result1))
			{
				$orderNo=$row['strOrderNo']."/".$row['intOrderYear'];
				if($row['strOrderNo']==0){
					$orderNo='';	
				}
				$issueNo='';	
				if($row['intIssueNo']!=''){
					$issueNo=$row['intIssueNo']."/".$row['intIssueYear'];
				}
				if($row['intStatus']=='')
				   $ap="";
				else if($row['intStatus']==1)
				   $ap="(Confimed)";
				else if($row['intStatus']==0)
				   $ap="(Rejected)";
				else if($row['intStatus']>1)
				   $ap="(Pending)";
				   
				   if($row['issuedQty']=='')
				   $issueQty=0;
				   else
				  $issueQty=$row['issuedQty']; 
				
				$location 	= $_SESSION['CompanyID'];
				$company 	= $_SESSION['headCompanyId'];
				if($row['intBomItem']==0){
				$stockBalQty=getStockBalance_bulk($location,$row['intItemId']);
				}
				else{
				$stockBalQty=getStockBalance($company,$location,$row['strOrderNo'],$row['intOrderYear'],$row['intSalesOrderId'],$row['intItemId']);
				}
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" >&nbsp;<?php echo $orderNo;?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strSalesOrderNo']?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['mainCategory']?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['subCategory'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strCode'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strName'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblQty'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $ap.$issueNo ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $issueQty ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['balToIssueQty'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $stockBalQty ?>&nbsp;</td>
              </tr>
      <?php 
			$totQty+=$row['dblQty'];
			}
	  ?>
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<?php  
 	if($intStatus!=0)
	{

				for($i=1; $i<=$savedLevels; $i++)
				{
					  $sqlc = "SELECT
							ware_mrnheader_approvedby.intApproveUser,
							ware_mrnheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_mrnheader_approvedby.intApproveLevelNo
							FROM
							ware_mrnheader_approvedby
							Inner Join sys_users ON ware_mrnheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_mrnheader_approvedby.intMrnNo =  '$mrnNo' AND
							ware_mrnheader_approvedby.intYear =  '$year'  AND
							ware_mrnheader_approvedby.intApproveLevelNo =  '$i' AND 
							ware_mrnheader_approvedby.intStatus='0'   order by intApproveLevelNo asc";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
						if($i==1)
						$desc="1st ";
						else if($i==2)
						$desc="2nd ";
						else if($i==3)
						$desc="3rd ";
						else
						$desc=$i."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
				?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Appoved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
            </tr>
<?php
			}
	}
	else{
					 $sqlc = "SELECT
							ware_mrnheader_approvedby.intApproveUser,
							ware_mrnheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_mrnheader_approvedby.intApproveLevelNo
							FROM
							ware_mrnheader_approvedby
							Inner Join sys_users ON ware_mrnheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_mrnheader_approvedby.intMrnNo =  '$mrnNo' AND
							ware_mrnheader_approvedby.intYear =  '$year'  AND
							ware_mrnheader_approvedby.intApproveLevelNo =  '0' AND 
							ware_mrnheader_approvedby.intStatus='0' ";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Rejected By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
	}
?>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
<?php
	//-----------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$salesOrderId,$item)
	{
		global $db;
		  $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		    $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
?>
</body>
</html>
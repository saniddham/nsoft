<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
session_start();
$backwardseperator = "../../../../";
$backwardseperator = "../../../../";
$location = $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];

$mainPath = $_SESSION['mainPath'];
$intUser  = $_SESSION["userId"];
$thisFilePath =  $_SERVER['PHP_SELF'];

$programName='Material Request Note';
$programCode='P0229';

//$formName		  	= 'frmItem';
include  	"{$backwardseperator}dataAccess/permisionCheck.inc";


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Confirmed MRN Listing</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />


</head>

<body>
<form id="frmReturnlisting" name="frmReturnlisting" method="get" action="mrnConfirmedListing.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<?php
$stat = $_GET['cboMrnStatus'];
if($stat==''){
$stat='1';
}

$supReturnApproveLevel = (int)getApproveLevel($programName);


//$prnApproveLevel = (int)getApproveLevel('Purchase Request Note')+1;
//$grnApproveLevel = (int)getApproveLevel($programName)+1;

?>

<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">MATERIAL REQUESION NOTE STATUS
		    <select style="width:130px" name="cboMrnStatus" id="cboMrnStatus" onchange="submit();" >
             <option <?php if($stat==2){ ?> selected="selected" <?php } ?>value="2">Completed Issues</option>
              <option <?php if($stat==1){ ?> selected="selected" <?php } ?>value="1">Pending Issues</option>
            <option  <?php if($stat==3){ ?> selected="selected" <?php } ?> value="3">All</option>
	        </select>
LISTING</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableBorder_allRound">
          <tr>
            <td width="10%" height="22" class="normalfnt">&nbsp;</td>
            <td width="22%">&nbsp;</td>
            <td width="17%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="11%" class="normalfnt">&nbsp;</td>
            <td width="15%">&nbsp;</td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="grid" >
            <tr class="gridHeader">
              <td width="15%" height="21" >MRN  No</td>
              <td width="19%" >Department</td>
              <td width="19%">Date</td>
              <td width="19%">User</td>
              <td width="10%"> Progress</td>
              <td width="18%">Report</td>
              </tr>
                 <?php
				 if($stat=='3')//all
				 $sqlCon="";
				 else if($stat==1)//pending
				 $sqlCon=" tb1.totMrnQty>tb1.totIssueQty";
				 else if($stat==2)//completed
				 $sqlCon=" tb1.totMrnQty=tb1.totIssueQty";
				 
				 
	 	 		 $sql = "select * from (SELECT
ware_mrnheader.intMrnNo,
ware_mrnheader.intMrnYear,
ware_mrnheader.datdate, 
sys_users.strUserName,
ware_mrnheader.intStatus,
ware_mrnheader.intApproveLevels, 
mst_department.strName as department,  
Sum(ware_mrndetails.dblQty) as totMrnQty,
Sum(ware_mrndetails.dblIssudQty) as totIssueQty 
FROM
ware_mrnheader
Inner Join ware_mrndetails ON ware_mrnheader.intMrnNo = ware_mrndetails.intMrnNo AND ware_mrnheader.intMrnYear = ware_mrndetails.intMrnYear 
Inner Join mst_department ON ware_mrnheader.intDepartment = mst_department.intId
Inner Join sys_users ON ware_mrnheader.intUser = sys_users.intUserId 
Inner Join mst_locations ON ware_mrnheader.intCompanyId = mst_locations.intId WHERE mst_locations.intCompanyId = '$company' AND ware_mrnheader.intCompanyId ='$location' and ware_mrnheader.intStatus=1   
GROUP BY 
ware_mrnheader.intMrnNo,
ware_mrnheader.intMrnYear 
ORDER BY 
ware_mrnheader.intMrnNo DESC,
ware_mrnheader.intMrnYear DESC) as tb1 where  ".$sqlCon;
//echo $sql;
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					 $mrnNo=$row['intMrnNo'];
					 $year=$row['intMrnYear'];
					 $department=$row['department'];
					 $status=$row['intStatus'];
					 $savedLevels=$row['intApproveLevels'];
	  			 ?>
            <tr class="normalfnt">
              <?php
				$editMode=0;
				if($row['intStatus']==0)
				$editMode=1;
				else if($row['intApproveLevels']+1==$row['intStatus'])
				$editMode=1;
				//user can edit only saved and rejected return notes ...... 
			  ?>
              <td align="center" bgcolor="#FFFFFF"><?php if($editMode==1){ ?><a href="../addNew/mrn.php?mrnNo=<?php echo $row['intMrnNo']?>&year=<?php echo $row['intMrnYear']?>" target="_blank"> <?php } ?><?php echo $row['intMrnNo']."/".$row['intMrnYear'];?><?php if($editMode==1){ ?></a><?php } ?></td>
              <td bgcolor="#FFFFFF" align="center"><?php echo $row['department']?></td>
              <td bgcolor="#FFFFFF" align="center"><?php echo $row['datdate']?></td>
              <td bgcolor="#FFFFFF" align="center"><?php echo $row['strUserName']?></td>
              <td bgcolor="#FFFFFF" align="right"><?php echo ($row['totIssueQty']/$row['totMrnQty'])*100?>%&nbsp;&nbsp;</td>
              
              <td bgcolor="#FFFFFF" align="center"><a href="rptConfirmedMrnStatus.php?mrnNo=<?php echo $row['intMrnNo']?>&year=<?php echo $row['intMrnYear']?>" target="_blank"><img src="../../../../images/view.png" width="91" height="19" class="mouseover" /></a></td>
              </tr>
              	<?php 
        		 } 
       			 ?>
            </table>
          </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><img src="../../../../planning/img/Tclose.jpg" width="92" height="24" class="mouseover" /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>

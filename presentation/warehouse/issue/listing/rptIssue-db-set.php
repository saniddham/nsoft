<?php 
ini_set('display_errors',0);
//////////////////////////////////////////////
//Create By:H.B.G Korala
//note://if first confirmation raised,dblIssudQty qty field in ware_mrndetails table shld update.
//note://if final confirmation raised,insert into stocktransaction table.
//////////////////////////////////////////////

	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$companyId 	= $_SESSION['headCompanyId'];
	$ssss		='';

	include "{$backwardseperator}dataAccess/Connector.php";
	require_once "../../../../class/cls_commonFunctions_get.php";
	include "{$backwardseperator}libraries/mail/mail.php";
	include "../../../../class/finance/supplier/cls_issuing_chartofaccount_set.php";
	require_once "../../../../class/warehouse/issue/cls_issue_get.php";
	require_once "../../../../class/cls_mail.php";
	require_once "../../../../class/finance/budget/cls_common_function_budget_get.php";
    require_once 	"../../../../class/warehouse/gatepass/cls_gatepass_get.php";
    require_once 	"../../../../class/finance/budget/entering/cls_budget_set.php";
	require_once "../../../../class/warehouse/cls_warehouse_get.php";
    require_once $_SESSION['ROOT_PATH']."class/common/azureDBconnection.php";

	/////////// parameters /////////////////////////////
	

	$obj_chartOfAccount_set	= new Cls_Issuing_ChartofAccount_Set($db);
	$obj_common				= new cls_commonFunctions_get($db);
	$objMail 				= new cls_create_mail($db);
	$obj_issue_get			= new cls_issue_get($db);
	$obj_comm_budget_get	= new cls_common_function_budget_get($db);
	$obj_gatepass_get		= new cls_gatepass_get($db);
	$obj_budget_set			= new cls_budget_set($db);
	$objwhouseget = new cls_warehouse_get($db);
    $objAzure = new cls_azureDBconnection();
	
	$programName='Material Issued Note';
	$programCode='P0231';
	$issueApproveLevel = (int)getApproveLevel($programName);
	
	//GatePass Approve Levels for not stock maintain locations
	
	$programNameGate='Gate Pass';
    $programCodeGate='P0250';

   $ApproveLevels = (int)getApproveLevel($programNameGate);
   $gatePassApproveLevel = $ApproveLevels+1;

   //GatePass TransferIN Levels for not stock maintain locations
	
       $programNameTransfer='GatePass Transfer In';
	   $programCode='P0252';

		$ApproveLevelsTransfer = (int)getApproveLevel($programNameTransfer);
		$transfApproveLevel = $ApproveLevelsTransfer+1;
	
	/////////// parameters /////////////////////////////

	$issueNo = $_REQUEST['issueNo'];
	$year = $_REQUEST['year'];
	if($_REQUEST['status']=='approve')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';
			
			$sql_1 = "SELECT 
						trn_orderheader.intOrderNo,
						trn_orderheader.intOrderYear,
						trn_orderheader.intStatus,
						trn_orderheader.intApproveLevelStart, 
						ware_mrnheader.mrnType
 					FROM trn_orderheader 
					INNER JOIN ware_issuedetails ON trn_orderheader.intOrderNo = ware_issuedetails.strOrderNo AND
						trn_orderheader.intOrderYear = ware_issuedetails.intOrderYear
				   INNER JOIN ware_mrnheader ON ware_mrnheader.intMrnNo = ware_issuedetails.intMrnNo AND ware_mrnheader.intMrnYear = ware_issuedetails.intMrnYear
					WHERE
						ware_issuedetails.intIssueNo = '$issueNo'
					AND ware_issuedetails.intIssueYear = '$year'";
			$result_1 = $db->RunQuery2($sql_1);
			$mrnType = 2;
			while($row=mysqli_fetch_array($result_1))
			{
				$intOrderNo   = $row['intOrderNo'];
				$intOrderYear = $row['intOrderYear'];
				$intStatus = $row['intStatus'];
				$intApproveLevelStart = $row['intApproveLevelStart'];
				$mrnType			  = $row['mrnType'];
				$order_status	= getOrderStatus($intOrderNo,$intOrderYear);
				
				if($intStatus <= 0 || ($intStatus>=$intApproveLevelStart+1))
				{
				$rollBackFlag=1;
				$sqlM=$sql;
				$rollBackMsg = "Approval error! Unapproved Order.";
				}
				else if($order_status[2]==1 && $order_status[3] !=1 ){
				 $rollBackFlag=1;
				 $rollBackMsg="Can't raise Issue note for Non-Instant pre costing order ".$intOrderNo.'/'.$intOrderNo;
			 	}
				
			}
		
		
		$sql = "UPDATE `ware_issueheader` SET `intStatus`=intStatus-1 WHERE (intIssueNo='$issueNo') AND (`intIssueYear`='$year') and intStatus not in(0,1)";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Approval error!";
		}
		
		if($result){
			$sql = "SELECT ware_issueheader.intUser, 
			ware_issueheader.intStatus,
			ware_issueheader.intApproveLevels,
			ware_issueheader.intCompanyId AS location,
			mst_locations.intCompanyId AS companyId, 
			mst_locations.int_stock_locationin AS int_stock_locationin 
			FROM ware_issueheader 
			Inner Join mst_locations ON ware_issueheader.intCompanyId = mst_locations.intId 
			  WHERE (intIssueNo='$issueNo') AND (`intIssueYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$createdUser = $row['intUser'];
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			//to location
			$Tolocation 	= $row['location'];
			//from location
			$fromLocation = $location;
			
			//Checking Issue Note, company is stock maintaining company
		}
			if($row['int_stock_locationin'] == '0'){
				
				//Create Gatepass
				$gatePassCreated  = createGatepass($fromLocation,$Tolocation,$issueNo,$year,$mrnType);
				if($gatePassCreated['type'] == 'pass')
				{
					//Approving the created gatepass
					$gatePassApprove = gatePassApprove($gatePassCreated['serialNo'],$gatePassCreated['year']);
					
					if($gatePassApprove['type'] == 'pass')
				{
				    // for screenline
				    if($companyId == '1'){
				        updateAzureDBTablesOnGP($gatePassCreated['serialNo'],$gatePassCreated['year'],$db, $fromLocation);
                    }
					//gatepass Transfer IN
					$gatePassTransferin  = gatepassTrnsferin($fromLocation,$Tolocation,$gatePassCreated['serialNo'],$gatePassCreated['year'],$issueNo,$year);

					if($gatePassTransferin['type'] == 'pass'){
						$gatepassTransferInApprove = gatepassTransferInApprove($gatePassTransferin['serialNo'],$gatePassTransferin['year']);
							if($gatepassTransferInApprove['type'] == 'fail') {
								$rollBackFlag=1;
								$sqlM='';
								$rollBackMsg = "Unable To Approve Transfer In  Gatepass";
							}
							else{
							    if($company == '1'){
							        updateAzureDBTablesOnGPIN($gatePassTransferin['serialNo'],$gatePassTransferin['year'],$db, $Tolocation);
                                }
                            }
					}	
					else{
					$rollBackFlag=1;
					$sqlM='';
					$rollBackMsg = "Unable To Create gatepass Transfer In ".$gatePassTransferin['msg'];
					}
				}
				
				else{
					$rollBackFlag=1;
					$sqlM='';
					$rollBackMsg = "Unable To Approve Gatepass";
				}
				}
				
				else{
					
					$rollBackFlag=1;
					$sqlM=$sqlI;
					$rollBackMsg = "Unable To create Gatepass - ".$gatePassCreated['msg'];
				}
			}
				
			
			
			$approveLevel=$savedLevels+1-$status;
			$sqlI = "INSERT INTO `ware_issueheader_approvedby` (`intIssueNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
				VALUES ('$issueNo','$year','$approveLevel','$userId',now(),0)";
			$resultI = $db->RunQuery2($sqlI);
			if((!$resultI) && ($rollBackFlag!=1)){
				$rollBackFlag=1;
				$sqlM=$sqlI;
				$rollBackMsg = "Approval error!";
			}

			if($status==1)
			{
				if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
				////////////// send mail to creater //////////////////
				sendFinlConfirmedEmailToUser($issueNo,$year,$objMail,$mainPath,$root_path);
				}
			}

		//--------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
			 	$sql1 = "SELECT 
						ware_issuedetails.strOrderNo,
						ware_issuedetails.intOrderYear, 
						ware_issuedetails.strStyleNo,
 						sum(ware_issuedetails.dblQty) as Qty,
						ware_issuedetails.intItemId,   
						mst_item.intBomItem 
						FROM 
						ware_issuedetails 
						Inner Join mst_item ON ware_issuedetails.intItemId = mst_item.intId
						WHERE
						ware_issuedetails.intIssueNo =  '$issueNo' AND
						ware_issuedetails.intIssueYear =  '$year' 
						GROUP BY 
						ware_issuedetails.strOrderNo,
						ware_issuedetails.intOrderYear, 
						ware_issuedetails.strStyleNo,
 						ware_issuedetails.intItemId ORDER BY ware_issuedetails.intItemId";
				$result1 = $db->RunQuery2($sql1);
				$bomItemFlag=1;
				$toSavedQty=0;
				$transSavedQty=0;
				$hem_str ='';
				while($row=mysqli_fetch_array($result1))
				{
			
					$toSavedQty+=round($row['Qty'],4);
					$item=$row['intItemId'];
					$Qty=$row['Qty'];
					$orderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$salesOrderId=$row['strStyleNo'];
					
					$order_status	= getOrderStatus($orderNo,$orderYear);
					if(($order_status[0] <= 0 || ($order_status[0]>=$order_status[1]+1)) && $orderNoArray[0] >0){
						 $rollBackFlag=1;
						 $rollBackMsg="Approval error! Unapproved Order.".$orderNoArray[1].'/'.$orderNoArray[2];
					 }
					else if($order_status[2]==1 && $order_status[3] !=1 ){
						 $rollBackFlag=1;
						 $rollBackMsg="Can't raise Issue notes for Pre-Costing order.".$orderNo.'/'.$orderYear;
					 }
					 
					// if($item==10079)
						// echo $rollBackMsg;
 					
					if($orderNo==0){
						$bomItemFlag=0;
					}
					else{
						$bomItemFlag=1;
					}
					$hem_str.='<br>';
					if($bomItemFlag==1){//bom item
						//$resultB = getGrnWiseStockBalance($location,$orderNo,$orderYear,$salesOrderId,$item);
						//echo "A:";
						$resultB = getGrnWiseStockBalance_bulk($Tolocation,$item);
						while($rowB=mysqli_fetch_array($resultB)){
								//if($item==10079)
								//	echo '--'.$rowB['stockBal'];			
								//if($item==10079)
								//	echo '--'.$rowB['stockBal'];			
								if(($Qty>0) && ($rowB['stockBal']>0)){
								if($rowB['stockBal']>=$Qty){
									$saveQty=$Qty;
									$Qty=0;
									}
									else if($Qty>$rowB['stockBal']){
									$saveQty=$rowB['stockBal'];
									$Qty=$Qty-$saveQty;
									}
									$grnNo=$rowB['intGRNNo'];
									$grnYear=$rowB['intGRNYear'];
									$grnDate=$rowB['dtGRNDate'];
									$grnRate=$rowB['dblGRNRate'];	
									$currency=loadCurrency($grnNo,$grnYear,$item);
									$saveQty = round($saveQty,4);
									if($saveQty>0){	
									 	$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intOrderNo,intOrderYear,intSalesOrderId,intUser,dtDate) 
											VALUES ('$companyId','$Tolocation','$issueNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','-$saveQty','ISSUE','$orderNo','$orderYear','$salesOrderId','$userId',now())";
											$sqlII = "INSERT INTO `ware_stocktransactions` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intOrderNo,intOrderYear,intSalesOrderId,intItemId,dblQty,strType,intUser,dtDate) 
											VALUES ('$companyId','$Tolocation','$issueNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$orderNo','$orderYear','$salesOrderId','$item','$saveQty','ISSUE','$userId',now())";
											$sqlIII = "INSERT INTO `ware_stocktransactions` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intOrderNo,intOrderYear,intSalesOrderId,intItemId,dblQty,strType,intUser,dtDate) 
											VALUES ('$companyId','$Tolocation','$issueNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$orderNo','$orderYear','$salesOrderId','$item','-$saveQty','ISSUE','$userId',now())";
										
										//$ssss .= $sqlI;
										$resultI = $db->RunQuery2($sqlI);
										$resultII = $db->RunQuery2($sqlII);
										$resultIII = $db->RunQuery2($sqlIII);
										if($resultI&&$resultII&&$resultIII){
											$transSavedQty+=$saveQty;
											$hem_str		.= $item.':'.$saveQty.'/';
											
										}
										if((!$resultI) && (!$resultII) && (!$resultIII) && ($rollBackFlag!=1)){
											$rollBackFlag=1;
											$sqlM=$sqlI;
											$rollBackMsg = "Approval error1!";
										}
									}
								}
						}
						
					}
					else{//general item 

						$resultG = getGrnWiseStockBalance_bulk($Tolocation,$item);
						while($rowG=mysqli_fetch_array($resultG)){
								if(($Qty>0) && ($rowG['stockBal']>0)){
								if($rowG['stockBal']>=$Qty){
									$saveQty=$Qty;
									$Qty=0;
									}
									else if($Qty>$rowG['stockBal']){
									$saveQty=$rowG['stockBal'];
									$Qty=$Qty-$saveQty;
									}
									$grnNo=$rowG['intGRNNo'];
									$grnYear=$rowG['intGRNYear'];
									$grnDate=$rowG['dtGRNDate'];
									$grnRate=$rowG['dblGRNRate'];	
									$currency=loadCurrency($grnNo,$grnYear,$item);
									$saveQty=round($saveQty,4);
									if($saveQty>0){	
										 	$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
											VALUES ('$companyId','$Tolocation','$issueNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','-$saveQty','ISSUE','$userId',now())";
											$resultI = $db->RunQuery2($sqlI);
											$ssss .=$sqlI;
										if($resultI){
											$transSavedQty+=$saveQty;
										}
										if((!$resultI) && ($rollBackFlag!=1)){
											$rollBackFlag=1;
											$sqlM=$sqlI;
											$rollBackMsg = "Approval error2!";
										}
									}
								}
						}//end of while
					}
				}
			}
		//--------------------------------------------------------------------
			if($status==$savedLevels){//if first confirmation raised,dblIssudQty qty field in ware_mrndetails table shld update
				$sql1 = "SELECT
						ware_issuedetails.intItemId,
						ware_issuedetails.dblQty,
						ware_issuedetails.strOrderNo,
						ware_issuedetails.intOrderYear, 
						ware_issuedetails.strStyleNo,
						ware_issuedetails.intMrnNo,
						ware_issuedetails.intMrnYear
						FROM
						ware_issuedetails
						WHERE
						ware_issuedetails.intIssueNo =  '$issueNo' AND
						ware_issuedetails.intIssueYear =  '$year'" ;
				$result1 = $db->RunQuery2($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$orderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$salesOrderId=$row['strStyleNo'];
					$mrnNo=$row['intMrnNo'];
					$mrnYear=$row['intMrnYear'];
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					
					$sql = "UPDATE `ware_mrndetails` SET 	dblIssudQty = dblIssudQty+'$Qty' 
							WHERE 
							ware_mrndetails.intOrderNo =  '$orderNo' AND
							ware_mrndetails.intOrderYear =  '$orderYear' AND
							ware_mrndetails.strStyleNo =  '$salesOrderId' AND
							ware_mrndetails.intMrnNo =  '$mrnNo' AND
							ware_mrndetails.intMrnYear =  '$mrnYear' AND
							ware_mrndetails.intItemId =  '$item' ";
					$result = $db->RunQuery2($sql);
					if((!$result) && ($rollBackFlag!=1)){
						$rollBackFlag=1;
						$sqlM=$sql;
						$rollBackMsg = "Approval error3!";
					}
				}
			}
		//---------------------------------------------------------------------------
		//BEGIN - BUDGET VALIDATION {
		if($rollBackFlag != 1)
		{			
			$validateResult	= budgetValidation($issueNo,$year);
			if($validateResult['type'] == '1')
			{
				$rollBackFlag	= 1;
				$rollBackMsg	= $validateResult['msg'];
			}
				
		}
		//END 	- BUDGET VALIDATION }
		
		
		
		if($status==1){
			//echo "sdfdsfdwf";
			//update_po_prn_details_for_issue_qry($issueNo,$year);
			update_po_prn_details_transactions($issueNo,$year);
		}
		$toSavedQty=round($toSavedQty,4);
		$transSavedQty=round($transSavedQty,4);
		
		if(($toSavedQty != $transSavedQty) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$rollBackMsg = "Approval Error4-".$toSavedQty."-".$transSavedQty;
			if($userId==2)
				$rollBackMsg = $ssss;
			//$sqlM=$toSavedQty."-".$transSavedQty;
			$sqlM=$hem_str;
		}
		
		if(($rollBackFlag==0) &&($status==1)){
		    if($_SESSION['headCompanyId'] == 1){
                updateAzureDBTables($issueNo,$year,$db, $location);
            }
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Final Approval raised sucessfully";
			$response['q'] 			= $sql;
		
			$obj_chartOfAccount_set->saveFinanceTransaction($issueNo,$year,'ISSUE');	
		}
		else if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Approval raised sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		//----------------------------------------
			echo json_encode($response);
		//-----------------------------------------	
		
	}
	else if($_REQUEST['status']=='reject')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
		//$grnApproveLevel = (int)getApproveLevel($programName);
		$sql = "SELECT  ware_issueheader.intUser, 
		ware_issueheader.intStatus,
		ware_issueheader.intApproveLevels,
		ware_issueheader.intCompanyId AS location,
		mst_locations.intCompanyId AS companyId 
		FROM ware_issueheader 
		Inner Join mst_locations ON ware_issueheader.intCompanyId = mst_locations.intId 
		  WHERE (intIssueNo='$issueNo') AND (`intIssueYear`='$year')";
		$results = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($results);
		$createdUser = $row['intUser'];
		$status = $row['intStatus'];
		$savedLevels = $row['intApproveLevels'];
		$location 	= $row['location'];
		$company = $row['companyId'];
		
		$sql = "UPDATE `ware_issueheader` SET `intStatus`=0 WHERE (intIssueNo='$issueNo') AND (`intIssueYear`='$year')";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Rejection error!";
		}
		
		//$sql = "DELETE FROM `ware_issueheader_approvedby` WHERE (`intIssueNo`='$issueNo') AND (`intYear`='$year')";
		//$result2 = $db->RunQuery($sql);
		
		$sqlI = "INSERT INTO `ware_issueheader_approvedby` (`intIssueNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$issueNo','$year','0','$userId',now(),0)";
		$resultI = $db->RunQuery2($sqlI);
		if((!$resultI) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sqlI;
			$rollBackMsg = "Rejection error!";
		}
		if($resultI)
		{
			if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
			sendRejecedEmailToUser($issueNo,$year,$objMail,$mainPath,$root_path);
			}
		}
		
		//---------------------------------------------------------------------------------
/*		if($status==1){//if final confirmation raised,insert to stock
				$sql1 = "SELECT
						ware_issuedetails.intItemId,
						sum(ware_issuedetails.dblQty) as Qty,
						ware_issuedetails.strOrderNo,
						ware_issuedetails.intOrderYear, 
						ware_issuedetails.strStyleNo, 
						mst_item.intBomItem 
						FROM 
						ware_issuedetails 
						Inner Join mst_item ON ware_issuedetails.intItemId = mst_item.intId
						WHERE
						ware_issuedetails.intIssueNo =  '$issueNo' AND
						ware_issuedetails.intIssueYear =  '$year' 
						GROUP BY 
						ware_issuedetails.strOrderNo,
						ware_issuedetails.intOrderYear, 
						ware_issuedetails.strStyleNo,
 						ware_issuedetails.intItemId";
				$result1 = $db->RunQuery($sql1);
				$bomItemFlag=1;
				while($row=mysqli_fetch_array($result1))
				{
					$orderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$salesOrderId=$row['strStyleNo'];
					$item=$row['intItemId'];
					$Qty=$row['Qty'];
					
					if($orderNo==0){
						$bomItemFlag=0;
					}
					else{
						$bomItemFlag=1;
					}
		
					if($bomItemFlag==1){//bom item
					$sqlI = "INSERT INTO `ware_stocktransactions` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intOrderNo,intOrderYear,intSalesOrderId,intItemId,dblQty,strType,intUser,dtDate) 
						VALUES ('$company','$location','$issueNo','$year','$orderNo','$orderYear','$salesOrderId','$item','$Qty','CISSUE','$userId',now())";
					}
					else{
					$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intItemId,dblQty,strType,intUser,dtDate) 
						VALUES ('$companyId','$location','$issueNo','$year','$item','$Qty','CISSUE','$userId',now())";
					}
					$resultI = $db->RunQuery($sqlI);
				}
			}
*/		//---------------------------------------------------------------------------------
			if($savedLevels>=$status){//if atleast one confirmation processed, roll back return to saupplier qty from grn table-
				$sql1 = "SELECT
						ware_issuedetails.intItemId,
						ware_issuedetails.dblQty,
						ware_issuedetails.strOrderNo,
						ware_issuedetails.intOrderYear, 
						ware_issuedetails.strStyleNo,
						ware_issuedetails.intMrnNo,
						ware_issuedetails.intMrnYear
						FROM
						ware_issuedetails
						WHERE
						ware_issuedetails.intIssueNo =  '$issueNo' AND
						ware_issuedetails.intIssueYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$orderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$salesOrderId=$row['strStyleNo'];
					$mrnNo=$row['intMrnNo'];
					$mrnYear=$row['intMrnYear'];
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					
					$sql = "UPDATE `ware_mrndetails` SET 	dblIssudQty = dblIssudQty-'$Qty' 
							WHERE
							ware_mrndetails.intOrderNo =  '$orderNo' AND
							ware_mrndetails.intOrderYear =  '$orderYear' AND
							ware_mrndetails.strStyleNo =  '$salesOrderId' AND
							ware_mrndetails.intMrnNo =  '$mrnNo' AND
							ware_mrndetails.intMrnYear =  '$mrnYear' AND
							ware_mrndetails.intItemId =  '$item' ";
					$result = $db->RunQuery2($sql);
					if((!$result) && ($rollBackFlag!=1)){
						$rollBackFlag=1;
						$sqlM=$sql;
						$rollBackMsg = "Rejection error!";
					}
				}
			}
			
		//------------------------------------
		if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Rejection raised sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		//----------------------------------------
			echo json_encode($response);
		//-----------------------------------------	
		
	}
	
//--------------------------------------------------------------
function getGrnWiseStockBalance_bulk($location,$item)
{
	global $db;
	   $sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) AS stockBal,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear,
			ware_stocktransactions_bulk.dtGRNDate,
			ware_stocktransactions_bulk.dblGRNRate 
			FROM ware_stocktransactions_bulk 
			WHERE
			ware_stocktransactions_bulk.intItemId =  '$item' AND
			ware_stocktransactions_bulk.intLocationId =  '$location'
			GROUP BY
			ware_stocktransactions_bulk.intItemId,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear,
			ware_stocktransactions_bulk.dtGRNDate, 
			ware_stocktransactions_bulk.dblGRNRate  
			having round(stockBal,6) > 0
			ORDER BY
			ware_stocktransactions_bulk.dtGRNDate ASC";
			
			//if($item==10079)	
			//echo $sql;
	

	$result = $db->RunQuery2($sql);
	return $result;	
}
//--------------------------------------------------------------
function getGrnWiseStockBalance($location,$orderNo,$orderYear,$salesOrderId,$item)
{
	global $db;
	 $sql = "SELECT
			Sum(ware_stocktransactions.dblQty) AS stockBal,
			ware_stocktransactions.intGRNNo,
			ware_stocktransactions.intGRNYear,
			ware_stocktransactions.dtGRNDate,
			ware_stocktransactions.dblGRNRate  
			FROM ware_stocktransactions  
			WHERE
			ware_stocktransactions.intItemId =  '$item' AND
			ware_stocktransactions.intLocationId =  '$location' AND 
			ware_stocktransactions.intOrderNo='$orderNo' AND 
			ware_stocktransactions.intOrderYear='$orderYear' AND 
			ware_stocktransactions.intSalesOrderId='$salesOrderId' 
			GROUP BY
			ware_stocktransactions.intItemId,
			ware_stocktransactions.intGRNNo,
			ware_stocktransactions.intGRNYear, 
			ware_stocktransactions.dtGRNDate,
			ware_stocktransactions.dblGRNRate  
			ORDER BY
			ware_stocktransactions.dtGRNDate ASC";

	$result = $db->RunQuery2($sql);
	return $result;	
}
//--------------------------------------------------------------
function loadCurrency($grnNo,$grnYear,$item)
{
	global $db;
	
	if(($grnNo!=0) && ($grnYear!=0)){
	  $sql = "SELECT
			trn_poheader.intCurrency as currency 
			FROM
			ware_grnheader
			Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
			WHERE
			ware_grnheader.intGrnNo =  '$grnNo' AND
			ware_grnheader.intGrnYear =  '$grnYear'";
	}
	else{
	$sql = "SELECT
			mst_item.intCurrency as currency 
			FROM mst_item
			WHERE
			mst_item.intId =  '$item'";
	}

	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);	
	return $row['currency'];;	
}
//--------------------------------------------------------
function sendFinlConfirmedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_issueheader
			Inner Join sys_users ON ware_issueheader.intUser = sys_users.intUserId
			WHERE
			ware_issueheader.intIssueNo =  '$serialNo' AND
			ware_issueheader.intIssueYear =  '$year'";

				
		 $result = $db->RunQuery2($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="CONFIRMED ISSUE NOTE('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='ISSUE NOTE';
			$_REQUEST['field1']='ISSUE No';
			$_REQUEST['field2']='ISSUE Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="CONFIRMED ISSUE NOTE('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Approved this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/issue/listing/rptIssue.php?issueNo=$serialNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
function sendRejecedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_issueheader
			Inner Join sys_users ON ware_issueheader.intUser = sys_users.intUserId
			WHERE
			ware_issueheader.intIssueNo =  '$serialNo' AND
			ware_issueheader.intIssueYear =  '$year'";

				
		 $result = $db->RunQuery2($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="REJECTED ISSUE NOTE('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='ISSUE NOTE';
			$_REQUEST['field1']='ISSUE No';
			$_REQUEST['field2']='ISSUE Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="REJECTED ISSUE NOTE ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Rejected this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/issue/listing/rptIssue.php?issue 	No=$serialNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}

function budgetValidation($serialNo,$year)
{
	global $db;
	global $location;
	global $company;
	global $obj_issue_get;
	global $obj_comm_budget_get;
	global $obj_common;
	
	$arr_issue_item_tot_U		= NULL;
	$arr_issue_sub_cat_tot_U	= NULL;
	$arr_issue_item_tot_A		= NULL;
	$arr_issue_sub_cat_tot_A	= NULL;
	$rollBackFlag			= 0;
	
	$header_array		= $obj_issue_get->get_header($serialNo,$year,'RunQuery2');
	$results_details	= $obj_issue_get->get_details_results($serialNo,$year,'RunQuery2');
	$results_fin_year	= $obj_comm_budget_get->loadFinanceYear_with_selected_date($company,$header_array["DATE"],'RunQuery2');
	$row_fin_year		= mysqli_fetch_array($results_fin_year);
	$budg_year			= $row_fin_year['intId'];
	
	while($row_details	= mysqli_fetch_array($results_details))
	{
		$item_price_issue			= getItemPrice($location,$row_details['ITEM_ID'],$row_details['QTY']);
		$sub_cat_budget_flag	= $obj_comm_budget_get->check_sub_category_budgeted_or_not($location,$row_details['SUB_CATEGORY_ID'],'RunQuery2');
		
		if($sub_cat_budget_flag	== 0)//item wise budget
		{
			$budget_type			= $obj_comm_budget_get->load_budget_type($location,$row_details['SUB_CATEGORY_ID'],$row_details['ITEM_ID'],'RunQuery2');
			if($budget_type == 'U'){ //unit budget
				$arr_issue_item_tot_U[$row_details['ITEM_ID']]			+= $row_details['QTY'];
			}elseif($budget_type == 'A'){					//amount budget
				$arr_issue_item_tot_A[$row_details['ITEM_ID']]			+= $row_details['QTY'] * $item_price_issue;
			}
		}
		else if($sub_cat_budget_flag == 1)//sub category wise budget
		{
			$budget_type			= $obj_comm_budget_get->load_budget_type($location,$row_details['SUB_CATEGORY_ID'],0,'RunQuery2');
			if($budget_type == 'U'){//unit budget
				$arr_issue_sub_cat_tot_U[$row_details['SUB_CATEGORY_ID']]	+= $row_details['QTY'];
			}elseif($budget_type == 'A'){					//amount budget
				$arr_issue_sub_cat_tot_A[$row_details['SUB_CATEGORY_ID']]	+= $row_details['QTY'] * $item_price_issue;
			}
		}
	}
	
	while (list($key_i, $value_i) = each($arr_issue_item_tot_U)) {//item ,unit budget			
				$budget_bal	= $obj_comm_budget_get->load_budget_balance($location,$header_array['DEPARTMENT_ID'],$budg_year,'',$key_i,date('m'),'RunQuery2');
				$budget_bal = $budget_bal['balUnits'];
				if($value_i > $budget_bal){
					$rollBackFlag	=1;
					$rollBackMsg	="There is no budget balance for this month for item ".$obj_common->get_item_name($key_i,'RunQuery2')."-".$budget_bal;
				}
	}
	while (list($key_i, $value_i) = each($arr_issue_item_tot_A)) {//item, amount budget
				$budget_bal	= $obj_comm_budget_get->load_budget_balance($location,$header_array['DEPARTMENT_ID'],$budg_year,'',$key_i,date('m'),'RunQuery2');
				$budget_bal = $budget_bal['balAmount'];
				if($value_i > $budget_bal){
					$rollBackFlag	=1;
					$rollBackMsg	="There is no budget balance for this month for item ".$obj_common->get_item_name($key_i,'RunQuery2')."-".$budget_bal;
				}
	}
	while (list($key, $value) = each($arr_issue_sub_cat_tot_U)) {//sub category ,unit budget
			$budget_bal	= $obj_comm_budget_get->load_budget_balance($location,$header_array['DEPARTMENT_ID'],$budg_year,$key,'',date('m'),'RunQuery2');
			$budget_bal = $budget_bal['balUnits'];
			if($value > $budget_bal){
				$rollBackFlag	=1;
				$rollBackMsg	="There is no budget balance for this month for sub category ".$obj_common->get_sub_category_name($key,'RunQuery2')."-".$budget_bal;
			}
	}
	while (list($key, $value) = each($arr_issue_sub_cat_tot_A)) {//sub category ,amount budget
			$budget_bal	= $obj_comm_budget_get->load_budget_balance($location,$header_array['DEPARTMENT_ID'],$budg_year,$key,'',date('m'),'RunQuery2');
			$budget_bal = $budget_bal['balAmount'];
			if($value > $budget_bal){
				$rollBackFlag	=1;
				$rollBackMsg	="There is no budget balance for this month for sub category ".$obj_common->get_sub_category_name($key,'RunQuery2')."-".$budget_bal;
			}
	}
	
	//
	$results_details	= $obj_issue_get->get_details_results($serialNo,$year,'RunQuery2');
	while($row_details	= mysqli_fetch_array($results_details))
	{
		$item_price_issue			= getItemPrice($location,$row_details['ITEM_ID'],$row_details['QTY']);
		$sub_cat_budget_flag	= $obj_comm_budget_get->check_sub_category_budgeted_or_not($location,$row_details['SUB_CATEGORY_ID'],'RunQuery2');

		if($sub_cat_budget_flag	== 0)//item wise budget
		{
			$budget_type			= $obj_comm_budget_get->load_budget_type($location,$row_details['SUB_CATEGORY_ID'],$row_details['ITEM_ID'],'RunQuery2');
			if($budget_type == 'U'){ //unit budget
				$result_1 = updateBudget($location,$header_array['DEPARTMENT_ID'],$budg_year,$row_details['SUB_CATEGORY_ID'],$row_details['ITEM_ID'],date('m'),$row_details['QTY'],'ISSUE_BUDGET_UNITS');
				if(!$result_1['type'])
				{
					$rollBackFlag	= 1;
					$rollBackMsg	= $result_1['msg'];
				}
			}elseif($budget_type == 'A'){					//amount budget
				$result_1 = updateBudget($location,$header_array['DEPARTMENT_ID'],$budg_year,$row_details['SUB_CATEGORY_ID'],$row_details['ITEM_ID'],date('m'),round($row_details['QTY']*$item_price_issue,2),'ISSUE_BUDGET');
				if(!$result_1['type'])
				{
					$rollBackFlag	= 1;
					$rollBackMsg	= $result_1['msg'];
				}
			}
		}
		else if($sub_cat_budget_flag == 1)//sub category wise budget
		{
			$budget_type			= $obj_comm_budget_get->load_budget_type($location,$row_details['SUB_CATEGORY_ID'],0,'RunQuery2');
			if($budget_type == 'U'){//unit budget
				$result_1 = updateBudget_sub($location,$header_array['DEPARTMENT_ID'],$budg_year,$row_details['SUB_CATEGORY_ID'],date('m'),$row_details['QTY'],'ISSUE_BUDGET_UNITS');
				if(!$result_1['type'])
				{
					$rollBackFlag	= 1;
					$rollBackMsg	= $result_1['msg'];
				}
			}elseif($budget_type == 'A'){					//amount budget
				$result_1 = updateBudget_sub($location,$header_array['DEPARTMENT_ID'],$budg_year,$row_details['SUB_CATEGORY_ID'],date('m'),round($row_details['QTY']*$item_price_issue,2),'ISSUE_BUDGET');
				if(!$result_1['type'])
				{
					$rollBackFlag	= 1;
					$rollBackMsg	= $result_1['msg'];
				}
			}
		}
	}
	//
	$response['type']	= $rollBackFlag;
	$response['msg']	= $rollBackMsg;
	return $response;
}

function getItemPrice($location,$itemId,$issueQty)
{
	global $obj_comm_budget_get;

	$result1	= $obj_comm_budget_get->get_price_for_item($location,$itemId,'RunQuery2');
	while($row = mysqli_fetch_array($result1))
	{
		
		if($issueQty<=0)
			continue;
			
		$stockQty	= $row["stockBal"];
		$price		= $row["price"];

		if($issueQty<$stockQty)
		{
			$itemPrice .= $issueQty * $price;
			$issueQty	= 0;
		}
		else
		{
			$itemPrice .= $issueQty * $price;
			$issueQty = $issueQty - $stockQty;
		}
	}
	return $itemPrice;
}

function updateBudget($loca,$dept,$financeYear,$category,$item,$month,$amount,$field)
{
	global $db;
	
	$sql = "UPDATE 
			budget_plan_details PD
			SET $field = IFNULL($field,0) + $amount
			WHERE 
				LOCATION_ID = '$loca'
				AND DEPARTMENT_ID = '$dept'
				AND FINANCE_YEAR_ID = '$financeYear'
				AND SUB_CATEGORY_ID = '$category'
				AND ITEM_ID = '$item'
				AND MONTH = '$month'";
	$result = $db->RunQuery2($sql);
	if(!$result)
	{
		$responce['type'] = false;
		$responce['msg']	= $db->errormsg;
	}
		$responce['type'] = true;
	return $responce;
}

function updateBudget_sub($loca,$dept,$financeYear,$category,$month,$amount,$field)
{
	global $db;
	
	$sql = "UPDATE 
			budget_plan_details PD
			SET $field = IFNULL($field,0) + $amount
			WHERE 
				LOCATION_ID = '$loca'
				AND DEPARTMENT_ID = '$dept'
				AND FINANCE_YEAR_ID = '$financeYear'
				AND SUB_CATEGORY_ID = '$category'
				AND MONTH = '$month'";				
	$result = $db->RunQuery2($sql);
	if(!$result)
	{
		$responce['type'] = false;
		$responce['msg']	= $db->errormsg;
	}
		$responce['type'] = true;
	return $responce;
}

function update_po_prn_details_for_issue_qry($issueNo,$year){
	global $db;
	$sql="SELECT
            ware_issuedetails.strOrderNo,
            ware_issuedetails.intOrderYear,
            ware_issuedetails.strStyleNo,
            ware_issuedetails.intItemId,
            ware_issuedetails.dblQty,
            trn_po_prn_details_sales_order.ORDER_NO AS saved_pri
            FROM
            ware_issuedetails
            LEFT JOIN trn_po_prn_details_sales_order ON ware_issuedetails.strOrderNo = trn_po_prn_details_sales_order.ORDER_NO AND ware_issuedetails.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR AND ware_issuedetails.strStyleNo = trn_po_prn_details_sales_order.SALES_ORDER AND ware_issuedetails.intItemId = trn_po_prn_details_sales_order.ITEM
            WHERE
                        ware_issuedetails.intIssueNo = '$issueNo' AND
                        ware_issuedetails.intIssueYear = '$year' and 
                        strOrderNo >0";
	$result = $db->RunQuery2($sql);
	while($row	=mysqli_fetch_array($result)){
		$orderNo			= $row['strOrderNo'];
		$orderYear			= $row['intOrderYear'];
		$so					= $row['strStyleNo'];
		$item				= $row['intItemId'];
		$qty				= $row['dblQty'];
        $saved_pri          = $row['saved_pri'];
		if($saved_pri > 0){
        //PRI ITEM
            $sql_u	="UPDATE `trn_po_prn_details_sales_order` SET `ISSUED_QTY`=IFNULL(`ISSUED_QTY`,0)+'$qty' 
		  WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item')";
            $result_u = $db->RunQuery2($sql_u);
        }else{
             //NON PRI ITEM
                $sql_u	="UPDATE `trn_po_prn_details_sales_order_additional_mrn_items` SET `ISSUED_QTY`=IFNULL(`ISSUED_QTY`,0)+'$qty' 
		  WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item')";
                $result_u = $db->RunQuery2($sql_u);
            
        }
 	}
}
 function update_po_prn_details_transactions($issueNo,$year){
	global $db;
	$sql="SELECT 
			ware_issueheader.intCompanyId,
            ware_issuedetails.strOrderNo,
            ware_issuedetails.intOrderYear,
            ware_issuedetails.strStyleNo,
            ware_issuedetails.intItemId,
            sum(ware_issuedetails.dblQty) as dblQty,
            trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AS saved_pri 
            FROM
            ware_issuedetails
INNER JOIN ware_issueheader ON ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo AND ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear
			LEFT JOIN trn_po_prn_details_sales_order_stock_transactions ON ware_issuedetails.strOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND ware_issuedetails.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR AND ware_issuedetails.strStyleNo = trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER AND ware_issuedetails.intItemId = trn_po_prn_details_sales_order_stock_transactions.ITEM  
			AND (trn_po_prn_details_sales_order_stock_transactions.TRANSACTION='ISSUE' ) 
			AND trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID =ware_issueheader.intCompanyId
            WHERE
			ware_issuedetails.intIssueNo = '$issueNo' AND
			ware_issuedetails.intIssueYear = '$year' and 
			strOrderNo >0
			group by 
			ware_issueheader.intCompanyId,
            ware_issuedetails.strOrderNo,
            ware_issuedetails.intOrderYear,
            ware_issuedetails.strStyleNo,
            ware_issuedetails.intItemId 
			";
	$result = $db->RunQuery2($sql);
	while($row	=mysqli_fetch_array($result)){
		$orderNo			= $row['strOrderNo'];
		$orderYear			= $row['intOrderYear'];
		$so					= $row['strStyleNo'];
		$item				= $row['intItemId'];
		$qty				= $row['dblQty'];
        $saved_pri          = $row['saved_pri'];
		$location			= $row['intCompanyId'];
		if($saved_pri > 0){
        //PRI ITEM
            $sql_u	="UPDATE `trn_po_prn_details_sales_order_stock_transactions` SET `QTY`=IFNULL(`QTY`,0)+'$qty' 
		  WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item') AND (`TRANSACTION`='ISSUE') AND (`LOCATION_ID`='$location')";
            $result_u = $db->RunQuery2($sql_u);
        }
		else{
			$sql_u2	="INSERT INTO `trn_po_prn_details_sales_order_stock_transactions` (`ORDER_NO`, `ORDER_YEAR`, `SALES_ORDER`, `ITEM`, `LOCATION_ID`, `TRANSACTION`, `QTY`) VALUES ('$orderNo', '$orderYear', '$so', '$item', '$location', 'ISSUE', '$qty')";
			$result_u2 	= $db->RunQuery2($sql_u2);
		}
 	}
}
function getOrderStatus($orderNo,$year){
    global $db;
    $sql = "SELECT
			trn_orderheader.intStatus,
			trn_orderheader.intApproveLevelStart ,
			PO_TYPE ,
			INSTANT_ORDER  
			FROM
			trn_orderheader
			WHERE
			trn_orderheader.intOrderNo = '$orderNo' AND
			trn_orderheader.intOrderYear = '$year'

			";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
	$arr[0]	= $row['intStatus'];
	$arr[1]	= $row['intApproveLevelStart'];
	$arr[2]	= $row['PO_TYPE'];
	$arr[3]	= $row['INSTANT_ORDER'];
	return $arr;
	
}

function createGatepass($fromLocation,$tolocation,$issueNo,$year,$mrnType){
global $gatePassApproveLevel;
global $db;
global $ApproveLevels;
global $userId;
//------------save---------------------------	

	$savableFlag= 1;
	$tot_issued =0;
	$tot_issued_gp =0;
	
		$serialNo 	= getNextGatepassNo($fromLocation);
		$year = date('Y');
		$editMode=0;
		$savedStatus=$gatePassApproveLevel;
		$savedLevels=$ApproveLevels;
		$note = 'This Gate Pass is Generated by System';
	 //Here Gate pass type 1 is orderwise and 0 is General.
	if($mrnType != null && ($mrnType == 3 || $mrnType == 6)){
		$type = 1;
	}else{
		$type = 0;
	}
		$sql = "INSERT INTO `ware_gatepassheader` (`intGatePassNo`,`intGatePassYear`,intGPToLocation,strNote,intStatus,intApproveLevels,datdate,dtmCreateDate,intUser,intCompanyId,gpType,isSystemGenerated) 
					VALUES ('$serialNo','$year','$tolocation','$note','$gatePassApproveLevel','$ApproveLevels',now(),now(),'$userId','$fromLocation','$type',1)";

		$result = $db->RunQuery2($sql);
		$maxAppByStatus=(int)getMaxAppByStatus($serialNo,$year)+1;
		$sql = "UPDATE `ware_gatepassheader_approvedby` SET intStatus ='$maxAppByStatus' 
					WHERE (`intGatePassNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
		$result1 = $db->RunQuery2($sql);
		
		//Updating Issue header with gatepass number
		$issueHeaderUpdateSql = "UPDATE `ware_issueheader` SET intGatePassNo ='$serialNo' 
					WHERE (`intIssueNo`='$issueNo') AND (`intIssueYear`='$year')";
		$IHUresult = $db->RunQuery2($issueHeaderUpdateSql);
	
	//-----------delete and insert to detail table-----------------------
	if($result){
		$sql = "DELETE FROM `ware_gatepassdetails` WHERE (`intGatePassNo`='$serialNo') AND (`intGatePassYear`='$year')";
		$result2 = $db->RunQuery2($sql);
		$toSave=0;
		$saved=0;
		$rollBackFlag=0;
		$rollBackMsg ="Maximum Gate Pass Qtys for items are...";
		$budgetFlag	 = 0;

		// Getting Issue Note Details
		
		$sql = "
				SELECT
				ware_issuedetails.intIssueNo,
				ware_issuedetails.intIssueYear,
				ware_issuedetails.strOrderNo,
				ware_issuedetails.intOrderYear,
				ware_issuedetails.strStyleNo,
				ware_issuedetails.intMrnNo,
				ware_issuedetails.intMrnYear,
				ware_issuedetails.intItemId,
				sum(ware_issuedetails.dblQty) as dblQty,
				sum(ware_issuedetails.dblReturnQty) as dblReturnQty 
				FROM `ware_issuedetails`
				WHERE intIssueNo = '$issueNo' AND intIssueYear = '$year'
				group by 
				ware_issuedetails.strOrderNo,
				ware_issuedetails.intOrderYear,
				ware_issuedetails.strStyleNo,
				ware_issuedetails.intItemId 
				";
		$IssueNoteDetails = $db->RunQuery2($sql);

		while($arrVal = mysqli_fetch_array($IssueNoteDetails))
		{
			
			$location 	= $_SESSION['CompanyID'];
			$company 	= $_SESSION['headCompanyId'];
			$orderNo 	 = $arrVal['strOrderNo'];
			$intOrderYear 	 = $arrVal['intOrderYear'];
			//$orderNoArray   = explode('/',$orderNo);
			$allocatedFlag=1;
			$tot_issued	+=$arrVal['dblQty'];

			// if($orderNoArray[0]==''){
				// $orderNoArray[0]=0;
				// $allocatedFlag=0;
			// }
			// else{
				// $allocatedFlag=1;
			// }

			// if($orderNoArray[1]==''){
				// $orderNoArray[1]=0;
			// }
			
			// in this table strStyleNo is sales ID (this is mistake for earlier Developer :) )
			$salesOrderId 		 = $arrVal['strStyleNo'];
			$itemId 	 = $arrVal['intItemId'];
			$Qty 		 = $arrVal['dblQty'];

			$itemName=getItemName($itemId);

			if($allocatedFlag==0){
				$stockBalQty=getStockBalance_bulk($location,$itemId);
			}
			else{
				$stockBalQty=getStockBalance_bulk($location,$itemId);
				//$stockBalQty=getStockBalance($company,$location,$orderNoArray[0],$orderNoArray[1],$salesOrderId,$itemId);
			}

			$confirmedGPqty=get_gpQty($location,$orderNo,$intOrderYear,$salesOrderId,$itemId);//but not raised final confirmation

			//------check maximum ISSUE Qty--------------------
			$Qty = round($Qty,4);
			$balQtyToGP = ($stockBalQty-$confirmedGPqty);
			$balQtyToGP = round($balQtyToGP,4);
			if($Qty>$balQtyToGP){
				//	call roll back--------****************
				$rollBackFlag=1;
				if($allocatedFlag==1){
					$rollBackMsg .="<br> ".$orderNo."/".$intOrderYear."-".$salesOrderId."-".$itemName." =".$balQtyToGP;
					$msg .=$Qty." =".$balQtyToGP;
				}
				else{
					$rollBackMsg .="<br> ".$itemName." =".$balQtyToGP;
				}
				//exit();
			}

			//----------------------------
			if($rollBackFlag!=1){
				
				$Qty=round($Qty,4);
				
				$sql = "INSERT INTO `ware_gatepassdetails` (`intGatePassNo`,`intGatePassYear`,`strOrderNo`,`intOrderYear`,`strStyleNo`,`intItemId`,`dblQty`,dblTransferQty) 
					VALUES ('$serialNo','$year','$orderNo','$intOrderYear','$salesOrderId','$itemId','$Qty','0')";
				$result3 = $db->RunQuery2($sql);
				
				if($result3==1){
					$tot_issued_gp +=$Qty;
					$saved++;
				}
			}
			$toSave++;

			//---------------------- budget Validation ----------------------------------------------------
			if($rollBackFlag==0)
			{
				$validateArr	= validateBudgetBalance($serialNo,$year,$tolocation,$itemId,$Qty);
				
				if($validateArr['Flag']==1 && $rollBackFlag==0)
				{
					$rollBackFlag	= 1;
					$rollBackMsg	= $validateArr['msg'];
				}
				else if($validateArr['Flag']==1 &&round($tot_issued,4)==round($tot_issued_gp,4)){
					$rollBackFlag	= 1;
					$rollBackMsg	= "GP Qty saving error";
				}
			}
			
			//---------------------------------------------------------------------------------------------

		}
	}
	//die($rollBackMsg);
	if($rollBackFlag==1){
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
		$response['q'] 			= $msg;
	}
	else if(($savableFlag==1) &&(!$result)){
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= "Header saving error";
		$response['q'] 			= $sql;
	}
	else if(($savableFlag==1) &&($toSave==0) &&($saved==0)){
		$response['type'] 		= 'fail';
		$response['msg'] 		= "Details saving error";
		$response['q'] 			= '';
	}
	else if(($result) && ($toSave==$saved)){
		

		$response['type'] 		= 'pass';
		if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
		else
			$response['msg'] 		= 'Saved successfully.';

		$response['serialNo'] 		= $serialNo;
		$response['year'] 		= $year;

	}
	else{
		$response['type'] 		= 'fail';
		$response['msg'] 		= $db->errormsg;
		$response['q'] 			= $sql;
	}

//----------------------------------------

return $response;

	
	
	
	
}

function getNextGatepassNo($companyId)
{
	global $db;
	$sql = "SELECT
				sys_no.intGatePassNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$companyId'
				";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	$nextGatePassNo = $row['intGatePassNo'];

	$sql = "UPDATE `sys_no` SET intGatePassNo=intGatePassNo+1 WHERE (`intCompanyId`='$companyId')  ";
	$db->RunQuery2($sql);

	return $nextGatePassNo;
}

//------------------------------function get MaxAppByStatus---------------------
function getMaxAppByStatus($serialNo,$year){
	global $db;

	//echo $savedStat;
	$editMode=0;
	$sql = "SELECT
				max(ware_gatepassheader_approvedby.intStatus) as status 
				FROM
				ware_gatepassheader_approvedby
				WHERE
				ware_gatepassheader_approvedby.intGatePassNo =  '$serialNo' AND
				ware_gatepassheader_approvedby.intYear =  '$year'";

	$resultm = $db->RunQuery2($sql);
	$rowm=mysqli_fetch_array($resultm);

	return $rowm['status'];

}


//--------------------------------------------------------

function get_gpQty($location,$orderNo,$orderYear,$styleNo,$itemId){
	global $db;
	$sql1 = "SELECT
				Sum(ware_gatepassdetails.dblQty) as gpQty
				FROM
				ware_gatepassheader
				Inner Join ware_gatepassdetails ON ware_gatepassheader.intGatePassNo = ware_gatepassdetails.intGatePassNo AND ware_gatepassheader.intGatePassYear = ware_gatepassdetails.intGatePassYear
				WHERE
				ware_gatepassdetails.strOrderNo =  '$orderNo' AND
				ware_gatepassdetails.intOrderYear =  '$orderYear' AND
				ware_gatepassdetails.strStyleNo =  '$styleNo' AND
				ware_gatepassdetails.intItemId =  '$itemId' AND
				ware_gatepassheader.intStatus >  '1' AND
				ware_gatepassheader.intStatus <= ware_gatepassheader.intApproveLevels AND
				ware_gatepassheader.intCompanyId =  '$location'";
	$result1 = $db->RunQuery2($sql1);
	$row1 = mysqli_fetch_array($result1);
	$gpQty = $row1['gpQty'];
	return $gpQty;
}
//-----------------------------------------------------------
function getItemName($itemId){
	global $db;
	$sql = "SELECT
					mst_item.strName
					FROM mst_item
					WHERE
					mst_item.intId =  '$itemId'";

	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	return $row['strName'];
}

//--------------------------------------------------------------
function getStockBalance_bulk($location,$item)
{
	global $db;
	$sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	return val($row['stockBal']);
}
//--------------------------------------------------------getMaxAppByStatus
function validateBudgetBalance($serialNo,$year,$gatePassTo,$itemId,$Qty)
{
	global $obj_gatepass_get;
	global $obj_comm_budget_get;
	global $company;
	global $location;
	global $obj_common;

	$gp_item_tot_U			= 0;
	$gp_sub_cat_tot_U		= 0;
	$gp_item_tot_A			= 0;
	$gp_sub_cat_tot_A		= 0;

	$saveStatus 			= true;
	$saveQty				= 0;
	$header_array			= $obj_gatepass_get->get_header($serialNo,$year,'RunQuery2');
	$subCatId				= $obj_gatepass_get->getSubCategory($itemId,'RunQuery2');
	$results_fin_year		= $obj_comm_budget_get->loadFinanceYear_with_selected_date($company,date('Y-m-d'),'RunQuery2');
	$row_fin_year			= mysqli_fetch_array($results_fin_year);
	$budg_year				= $row_fin_year['intId'];
	$sub_cat_budget_flag	= $obj_comm_budget_get->check_sub_category_budgeted_or_not($gatePassTo,$subCatId,'RunQuery2');

	$check					= $obj_comm_budget_get->checkBudgetApprovalDepWise($gatePassTo,'',$budg_year,'RunQuery2');

	if(!$check && $saveStatus && $sub_cat_budget_flag!='2')
	{
		$saveStatus			= false;
		$data['Flag']		= 1;
		$data['msg']		= "No approve budget for all department which belong to gate pass to location and current finance year ";
	}

	if($sub_cat_budget_flag==0)
	{
		$budget_type		= $obj_comm_budget_get->load_budget_type($gatePassTo,$subCatId,$itemId,'RunQuery2');
		$budget_bal			= $obj_comm_budget_get->load_budget_balance($gatePassTo,'',$budg_year,'',$itemId,date('m'),'RunQuery2');
		$result_itemPrice	= $obj_comm_budget_get->get_price_for_item($location,$itemId,'RunQuery2');

		while($row = mysqli_fetch_array($result_itemPrice))
		{
			if($row['stockBal']>=$Qty)
			{
				$saveQty 	= $Qty;
				$Qty		= 0;
			}
			else if($Qty>$row['stockBal'])
			{
				$saveQty	= $row['stockBal'];
				$Qty		= $Qty-$saveQty;
			}
			if($saveQty>0)
			{
				if($budget_type == 'U')
					$gp_item_tot_U += $saveQty;
				else if($budget_type == 'A')
					$gp_item_tot_A += $saveQty*$row['price'];

				$budget_bal_A 	= $budget_bal['balAmount'];
				if(($gp_item_tot_A > $budget_bal_A) && $saveStatus)
				{
					$saveStatus		= false;
					$data['Flag']	= 1;
					$data['msg']	= "There is no budget balance for this month for item '".$obj_common->get_item_name($itemId,'RunQuery2')."' - ".$budget_bal_A;
				}
				$budget_bal_U 	= $budget_bal['balUnits'];

				if(($gp_item_tot_U > $budget_bal_U) && $saveStatus)
				{
					$saveStatus		= false;
					$data['Flag']	= 1;
					$data['msg']	= "There is no budget balance for this month for item '".$obj_common->get_item_name($itemId,'RunQuery2')."' - ".$budget_bal_U;
				}
			}
		}

	}
	else if($sub_cat_budget_flag==1)
	{
		$budget_type		= $obj_comm_budget_get->load_budget_type($gatePassTo,$subCatId,0,'RunQuery2');
		$budget_bal			= $obj_comm_budget_get->load_budget_balance($gatePassTo,'',$budg_year,$subCatId,0,date('m'),'RunQuery2');
		$result_itemPrice	= $obj_comm_budget_get->get_price_for_item($location,$itemId,'RunQuery2');

		while($row = mysqli_fetch_array($result_itemPrice))
		{
			if($row['stockBal']>=$Qty)
			{
				$saveQty 	= $Qty;
				$Qty		= 0;
			}
			else if($Qty>$row['stockBal'])
			{
				$saveQty	= $row['stockBal'];
				$Qty		= $Qty-$saveQty;
			}
			if($saveQty>0)
			{
				if($budget_type == 'U')
					$gp_sub_cat_tot_U += $saveQty;
				else if($budget_type == 'A')
					$gp_sub_cat_tot_A += $saveQty*$row['price'];

				$budget_bal_A 	= $budget_bal['balAmount'];
				if(($gp_sub_cat_tot_A > $budget_bal_A) && $saveStatus)
				{
					$saveStatus		= false;
					$data['Flag']	= 1;
					$data['msg']	= "There is no budget balance for this month for item '".$obj_common->get_sub_category_name($itemId,'RunQuery2')."' - ".$budget_bal_A;
				}
				$budget_bal_U 	= $budget_bal['balUnits'];
				if(($gp_sub_cat_tot_U > $budget_bal_U) && $saveStatus)
				{
					$saveStatus		= false;
					$data['Flag']	= 1;
					$data['msg']	= "There is no budget balance for this month for item '".$obj_common->get_item_name($itemId,'RunQuery2')."' - ".$budget_bal_U;
				}
			}

		}
	}

	return $data;
}


//------------Gate Pass Transferin---------------------------	
	function gatepassTrnsferin($fromLocation,$tolocation,$gpNo,$gpYear,$issueNo,$issueYear)
	{
		global $userId;
		global $db;
		global $ApproveLevelsTransfer;
		global $transfApproveLevel;
		global $objwhouseget;
		
		$tot_issuedd 		=0;
		$tot_issued_transf 	=0;
		$savableFlag		=1;
	

			$serialNo 	= getNextTransfNo($tolocation);
			$year = date('Y');
			$editMode=0;
			$savedStatus=$transfApproveLevel;
			$savedLevels=$transfApproveLevel;
			$ApproveLevels=$transfApproveLevel;
		
		//--------------------------
		$response['arrData'] = $objwhouseget->getLocationValidation('TRANSFERIN',$tolocation,$gpNo,$gpYear);//check for grn location
		//--------------------------
	
		if($response['arrData']['type']=='fail'){
			 $rollBackFlag=1;
			 $rollBackMsg=$response['arrData']['msg'];
		 }
		else if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This Gate Pass Transfer In No is already confirmed.cant edit";
		 }
		 
		else if($editMode==1){//edit existing grn
		$note = 'This Gate Pass Transferin is Generated by System';
		$sql = "UPDATE `ware_gatepasstransferinheader` SET intStatus ='$transfApproveLevel', 
												       intApproveLevels ='$ApproveLevels', 
												       strNote ='$note', 
													  intModifiedBy ='$userId', 
													  dtmModifiedDate =now() 
				WHERE (`intGpTransfNo`='$serialNo') AND (`intGpTransfYear`='$year')";
		$result = $db->RunQuery2($sql);
		}
		else{
			$note = 'This Gate Pass Transferin is Generated by System';
			//$sql = "DELETE FROM `ware_gatepasstransferinheader` WHERE (`intGpTransfNo`='$serialNo') AND (`intGpTransfYear`='$year')";
			//$result1 = $db->RunQuery2($sql);

			$sql = "INSERT INTO `ware_gatepasstransferinheader` (`intGpTransfNo`,`intGpTransfYear`,intGatePassNo,intGatePassYear,strNote,intStatus,intApproveLevels,datdate,dtmCreateDate,intUser,intCompanyId,isSystemGenerated) 
					VALUES ('$serialNo','$year','$gpNo','$gpYear','$note','$transfApproveLevel','$ApproveLevels',now(),now(),'$userId','$tolocation',1)";
			$result = $db->RunQuery2($sql);
			$maxAppByStatus=(int)getMaxAppByStatus($serialNo,$year)+1;
			$sql = "UPDATE `ware_gatepasstransferinheader_approvedby` SET intStatus ='$maxAppByStatus' 
					WHERE (`intGpTransfNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
			$result1 = $db->RunQuery2($sql);
		}
		//-----------delete and insert to detail table-----------------------
		if(($result) && ($rollBackFlag!=1)){
		$sql = "DELETE FROM `ware_gatepasstransferindetails` WHERE (`intGpTransfNo`='$serialNo') AND (`intGpTransfYear`='$year')";
		$result2 = $db->RunQuery2($sql);
			$toSave=0;
			$saved=0;
			$rollBackFlag=0;
			$rollBackMsg ="Maximum Qtys for items are..."; 
			
				// Getting Issue Note Details
		
		$sql = "SELECT
				ware_issuedetails.intIssueNo,
				ware_issuedetails.intIssueYear,
				ware_issuedetails.strOrderNo,
				ware_issuedetails.intOrderYear,
				ware_issuedetails.strStyleNo,
				ware_issuedetails.intMrnNo,
				ware_issuedetails.intMrnYear,
				ware_issuedetails.intItemId,
				sum(ware_issuedetails.dblQty) as dblQty,
				sum(ware_issuedetails.dblReturnQty) as dblReturnQty 
				FROM `ware_issuedetails`
				WHERE intIssueNo = '$issueNo' AND intIssueYear = '$issueYear'
				group by 
				ware_issuedetails.strOrderNo,
				ware_issuedetails.intOrderYear,
				ware_issuedetails.strStyleNo,
				ware_issuedetails.intItemId 

				";
		$IssueNoteDetails = $db->RunQuery2($sql);
			
			
				while($arrVal = mysqli_fetch_array($IssueNoteDetails))
			{
				$location 	= $_SESSION['CompanyID'];
				$company 	= $_SESSION['headCompanyId'];
				$orderNo 	 = $arrVal['strOrderNo'];
				$orderYear 	 = $arrVal['intOrderYear'];
	
				$salesOrderId 		 = $arrVal['strStyleNo'];
				$itemId 	 = $arrVal['intItemId'];
				$Qty 		 = $arrVal['dblQty'];
				$tot_issuedd += $Qty;
		

				//------check maximum ISSUE Qty--------------------
				 $sqlc = "SELECT ware_gatepassheader.intStatus,
				mst_item.strName as itemName,
				tb1.intGatePassNo,
				tb1.intGatePassYear,
				tb1.strOrderNo,
				tb1.intOrderYear,
				tb1.strStyleNo,
				tb1.dblQty,
				IFNULL((SELECT
				Sum(ware_gatepasstransferindetails.dblQty)
				FROM
				ware_gatepasstransferinheader
				Inner Join ware_gatepasstransferindetails ON ware_gatepasstransferinheader.intGpTransfNo = ware_gatepasstransferindetails.intGpTransfNo AND ware_gatepasstransferinheader.intGpTransfYear = ware_gatepasstransferindetails.intGpTransfYear
				WHERE
				ware_gatepasstransferinheader.intGatePassNo =  tb1.intGatePassNo AND
				ware_gatepasstransferinheader.intGatePassYear =  tb1.intGatePassYear AND
				ware_gatepasstransferindetails.strOrderNo =  tb1.strOrderNo AND
				ware_gatepasstransferindetails.intOrderYear =  tb1.intOrderYear AND
				ware_gatepasstransferindetails.strStyleNo =  tb1.strStyleNo AND
				ware_gatepasstransferindetails.intItemId =  tb1.intItemId AND
				ware_gatepasstransferinheader.intStatus > '0' AND 
				ware_gatepasstransferinheader.intStatus <= ware_gatepasstransferinheader.intApproveLevels),0) as dblTransferQty
				FROM
				ware_gatepassdetails as tb1 
				Inner Join ware_gatepassheader ON tb1.intGatePassNo = ware_gatepassheader.intGatePassNo AND tb1.intGatePassYear = ware_gatepassheader.intGatePassYear
				Inner Join mst_item ON tb1.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				WHERE 
				ware_gatepassheader.intStatus = '1' AND 
				ware_gatepassheader.intGatePassNo =  '$gpNo' AND 
				ware_gatepassheader.intGatePassYear =  '$gpYear' AND 
				tb1.intItemId =  '$itemId' ";
				
				if($orderNo!='')
				$sqlc.=" AND tb1.strOrderNo =  '$orderNo'";
				if($orderYear!='')
				$sqlc.=" AND tb1.intOrderYear =  '$orderYear'";
				if($salesOrderId!='')
				$sqlc.=" AND tb1.strStyleNo =  '$salesOrderId'";		
				$sql_gp	=$sqlc;			
				//echo $sqlc;
				$resultsc = $db->RunQuery2($sqlc);
				$rowc = mysqli_fetch_array($resultsc);
				$item=$rowc['itemName'];

				//if($rowc['dblQty']-$rowc['dblIssudQty']<$stockBalQty)
				$balQtyToTransf = $rowc['dblQty']-$rowc['dblTransferQty'];
				if($editMode==1){
					 $balQtyToTransf=$balQtyToTransf+$Qty;
				//	$balQtyToTransf +=	$Qty;
				}
			//	else
				//$balQtyToIssue = $stockBalQty;
				if($Qty>$balQtyToTransf){
				//	call roll back--------****************
					$rollBackFlag=1;
					if($rowc['strOrderNo']!=0){
					$rollBackMsg .="\n ".$orderNo."/".$orderYear."-".$salesOrderId."-".$item." =".$balQtyToTransf;
					}
					else{
					$rollBackMsg .="\n "."-".$item." =".$balQtyToTransf;
					}
					//exit();
				}

				//----------------------------
				if($rollBackFlag!=1){
				if($orderNoArray[0]==''){
					$orderNoArray[0]=0;
				}
				if($orderNoArray[1]==''){
					$orderNoArray[1]=0;
				}
					$Qty=round($Qty,4);
					$sql = "INSERT INTO `ware_gatepasstransferindetails` (`intGpTransfNo`,`intGpTransfYear`,`strOrderNo`,`intOrderYear`,`strStyleNo`,`intItemId`,`dblQty`) 
					VALUES ('$serialNo','$year','$orderNo','$orderYear','$salesOrderId','$itemId','$Qty')";
					$result3 = $db->RunQuery2($sql);
					if($result3==1){
					$saved++;
					$tot_issued_transf +=$Qty;
					}
				}
				$toSave++;
			}
		}
		//echo $rollBackFlag;
		
		//$rollBackFlag=1;
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sql_gp;
		}
		else if(($savableFlag==1) &&(!$result)){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Header saving error";
			$response['q'] 			= $sql;
		}
		else if(($savableFlag==1) &&($toSave==0) &&($saved==0)){
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Details saving error";
			$response['q'] 			= '';
		}
		else if($tot_issuedd != $tot_issued_transf){
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Transfer Qty saving error ".$tot_issuedd.' != '. $tot_issued_transf;
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
		}
		else{
		
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	
			return $response;
	}
	
	//-----------------------------------------	
	function getNextTransfNo($toCompany)
	{
		global $db;
		 $sql = "SELECT
				sys_no.intGpTransfNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$toCompany'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextTransfNo = $row['intGpTransfNo'];
		
		$sql = "UPDATE `sys_no` SET intGpTransfNo=intGpTransfNo+1 WHERE (`intCompanyId`='$toCompany')  ";
		$db->RunQuery2($sql);
		
		return $nextTransfNo;
	}
	
	//--------------------------------------------------------------
	function getSaveStatus($serialNo,$year)
	{
		global $db;
		$sql = "SELECT ware_gatepasstransferinheader.intStatus, ware_gatepasstransferinheader.intApproveLevels FROM ware_gatepasstransferinheader WHERE (intGpTransfNo='$serialNo') AND (`intGpTransfYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			
		return $editableFlag;
	}

	
	function gatePassApprove($gatePassNo,$year){
		global $userId;
		global $db;
		global $company;
		
		global $issueNo;
		global $year;

		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
			$sql_1 = "SELECT 
						trn_orderheader.intOrderNo,
						trn_orderheader.intOrderYear,
						trn_orderheader.intStatus,
						trn_orderheader.intApproveLevelStart
					FROM trn_orderheader 
					INNER JOIN ware_gatepassdetails ON trn_orderheader.intOrderNo = ware_gatepassdetails.strOrderNo AND
						trn_orderheader.intOrderYear = ware_gatepassdetails.intOrderYear
					WHERE
						ware_gatepassdetails.intGatePassNo = '$gatePassNo'
					AND ware_gatepassdetails.intGatePassYear = '$year'";
			$result_1 = $db->RunQuery2($sql_1);
			while($row=mysqli_fetch_array($result_1))
			{
				$intOrderNo   = $row['intOrderNo'];
				$intOrderYear = $row['intOrderYear'];
				$intStatus = $row['intStatus'];
				$intApproveLevelStart = $row['intApproveLevelStart'];
				
				if($intStatus <= 0 || ($intStatus>=$intApproveLevelStart+1))
				{
				$rollBackFlag=1;
				$sqlM=$sql;
				$rollBackMsg = "Approval error! Unapproved Order.";
				}
				
			}
	
		
		$sql = "UPDATE `ware_gatepassheader` SET `intStatus`=intStatus-1 WHERE (intGatePassNo='$gatePassNo') AND (`intGatePassYear`='$year') and intStatus not in(0,1)";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "GatePass Approval error5!";
		}
		
		if($result){
			$sql = "SELECT ware_gatepassheader.intUser,
					ware_gatepassheader.intStatus,
					ware_gatepassheader.intApproveLevels, 
					ware_gatepassheader.intCompanyId AS location,
					mst_locations.intCompanyId AS companyId,
					ware_gatepassheader.intGPToLocation AS gatepassToLoc
					FROM
					ware_gatepassheader
					Inner Join mst_locations ON ware_gatepassheader.intCompanyId = mst_locations.intId
					WHERE (intGatePassNo='$gatePassNo') AND (`intGatePassYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$createdUser = $row['intUser'];
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			$company = $row['companyId'];
			$location = $row['location'];
			$gatepassToLoc = $row['gatepassToLoc'];
			
			$approveLevel=$savedLevels+1-$status;
			$sqlI = "INSERT INTO `ware_gatepassheader_approvedby` (`intGatePassNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
				VALUES ('$gatePassNo','$year','$approveLevel','$userId',now(),0)";
			$resultI = $db->RunQuery2($sqlI);
			if((!$resultI) && ($rollBackFlag!=1)){
				$rollBackFlag=1;
				$sqlM=$sqlI;
				$rollBackMsg = "Approval error6!";
			}
			
			
		//--------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
				$sql1 = "SELECT 
						ware_gatepassdetails.strOrderNo,
						ware_gatepassdetails.intOrderYear,
						trn_orderdetails.strSalesOrderNo,
						trn_orderdetails.intSalesOrderId,
						ware_gatepassdetails.strStyleNo,
 						ware_gatepassdetails.dblQty,
						ware_gatepassdetails.intItemId,   
						mst_item.intBomItem 
						FROM 
						ware_gatepassdetails  
						left Join trn_orderdetails ON ware_gatepassdetails.strOrderNo = trn_orderdetails.intOrderNo AND ware_gatepassdetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_gatepassdetails.strStyleNo = trn_orderdetails.intSalesOrderId
						Inner Join mst_item ON ware_gatepassdetails.intItemId = mst_item.intId
						WHERE
						ware_gatepassdetails.intGatePassNo =  '$gatePassNo' AND
						ware_gatepassdetails.intGatePassYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				$toSavedQty=0;
				$transSavedQty=0;
				while($row=mysqli_fetch_array($result1))
				{
					$toSavedQty+=round($row['dblQty'],4);
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					$orderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$salesOrderNo=$row['strSalesOrderNo'];
					$salesOrderId=$row['intSalesOrderId'];
					if($orderNo==0){
						$bomItemFlag=0;
					}
					else{
						$bomItemFlag=1;
					}
					
				
					if($bomItemFlag==1){//bom item--get grn wise stock balance
						//$resultB = getGrnWiseStockBalance($location,$orderNo,$orderYear,$salesOrderId,$item);
						$resultB = getGrnWiseStockBalance_bulk($location,$item);
						while($rowB=mysqli_fetch_array($resultB)){
								if(($Qty>0) && ($rowB['stockBal']>0)){
									if(($rowB['stockBal']>=$Qty)){
									$saveQty=$Qty;
									$Qty=0;
									}
									else if($Qty>$rowB['stockBal']){
									$saveQty=$rowB['stockBal'];
									$Qty=$Qty-$saveQty;
									}
									$grnNo=$rowB['intGRNNo'];
									$grnYear=$rowB['intGRNYear'];
									$grnDate=$rowB['dtGRNDate'];
									$grnRate=$rowB['dblGRNRate'];
									$currency=loadCurrency($grnNo,$grnYear,$item);
									$saveQty=round($saveQty,4);		
									if($saveQty>0){	
									/*$sqlI = "INSERT INTO `ware_stocktransactions` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intOrderNo,intOrderYear,intSalesOrderId,intItemId,dblQty,strType,intUser,dtDate) 
										VALUES ('$company','$location','$gatePassNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$orderNo','$orderYear','$salesOrderId','$item','-$saveQty','GATEPASS','$userId',now())";*/
									$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intOrderNo,intOrderYear,intSalesOrderId,intUser,dtDate) 
										VALUES ('$company','$location','$gatePassNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','-$saveQty','GATEPASS','$orderNo','$orderYear','$salesOrderId','$userId',now())";
										//$ssss .= $sqlI;
									$resultI = $db->RunQuery2($sqlI);
									if($resultI){
										$transSavedQty+=$saveQty;
									}
									if((!$resultI) && ($rollBackFlag!=1)){
										$rollBackFlag=1;
										$sqlM=$sqlI;
										$rollBackMsg = "Approval error7!";
									}
									
									}
								}
						}
					}
					else{//general item--get grn wise stock balance
						$resultG = getGrnWiseStockBalance_bulk($location,$item);
						while($rowG=mysqli_fetch_array($resultG)){
								if(($Qty>0) && ($rowG['stockBal']>0)){
									if($Qty<=$rowG['stockBal']){
									$saveQty=$Qty;
									$Qty=0;
									}
									else if($Qty>$rowG['stockBal']){
									$saveQty=$rowG['stockBal'];
									$Qty=$Qty-$saveQty;
									}
									$grnNo=$rowG['intGRNNo'];
									$grnYear=$rowG['intGRNYear'];
									$grnDate=$rowG['dtGRNDate'];
									$grnRate=$rowG['dblGRNRate'];	
									$currency=loadCurrency($grnNo,$grnYear,$item);
									$saveQty=round($saveQty,4);		
									if($saveQty>0){	
									$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
										VALUES ('$company','$location','$gatePassNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','-$saveQty','GATEPASS','$userId',now())";
									$resultI = $db->RunQuery2($sqlI);
									if($resultI){
										$transSavedQty+=$saveQty;
									}
									if((!$resultI) && ($rollBackFlag!=1)){
										$rollBackFlag=1;
										$sqlM=$sqlI;
										$rollBackMsg = "Approval error8!";
									}
									
									}
								}
						}
					}
				}
			}
	
		
		}
		
		if($status==1 ){
			//echo "sdfdsfdwf";
			//update_po_prn_details_for_gp_qty($gatePassNo,$year);
			update_po_prn_details_transactionsGatePass($gatePassNo,$year);
		}
		
		$toSavedQty=round($toSavedQty,4);
		$transSavedQty=round($transSavedQty,4);
		
		if(($toSavedQty!=$transSavedQty) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$rollBackMsg = "Approval Error9";
			$sqlM=$toSavedQty."-".$transSavedQty;
		}
		
		if(($rollBackFlag==0) &&($status==1)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Final Approval raised sucessfully";
			$response['q'] 			= $sql;
		}
		else if($rollBackFlag==0){
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Approval raised sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		
		//----------------------------------------
			return $response;
		//-----------------------------------------	
		
		
		
	}
	
	function update_po_prn_details_transactionsGatePass($gatePassNo,$year){
	global $db;
	$sql="SELECT
			ware_gatepassdetails.strOrderNo,
			ware_gatepassdetails.intOrderYear,
			ware_gatepassdetails.strStyleNo,
			ware_gatepassdetails.intItemId,
			ware_gatepassdetails.dblQty,
			ware_gatepassheader.intCompanyId AS `from`,
			ware_gatepassheader.intGPToLocation AS `to`,
			trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AS saved_pri
			FROM
			ware_gatepassdetails
			INNER JOIN ware_gatepassheader ON ware_gatepassdetails.intGatePassNo = ware_gatepassheader.intGatePassNo AND ware_gatepassdetails.intGatePassYear = ware_gatepassheader.intGatePassYear
			LEFT JOIN trn_po_prn_details_sales_order_stock_transactions ON ware_gatepassdetails.strOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND ware_gatepassdetails.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR AND ware_gatepassdetails.strStyleNo = trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER AND ware_gatepassdetails.intItemId = trn_po_prn_details_sales_order_stock_transactions.ITEM  
			AND (trn_po_prn_details_sales_order_stock_transactions.TRANSACTION='GATE_PASS' ) 
			AND trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID =ware_gatepassheader.intCompanyId
			WHERE
			ware_gatepassdetails.intGatePassNo = '$gatePassNo' AND
			ware_gatepassdetails.intGatePassYear = '$year'
			";
	$result = $db->RunQuery2($sql);
	while($row	=mysqli_fetch_array($result)){
		$orderNo			= $row['strOrderNo'];
		$orderYear			= $row['intOrderYear'];
		$so					= $row['strStyleNo'];
		$item				= $row['intItemId'];
		$totQty				= $row['dblQty'];
		$saved_pri				= $row['saved_pri'];
		$location			= $row['from'];
		if($saved_pri > 0){
        //PRI ITEM
            $sql_u	="UPDATE `trn_po_prn_details_sales_order_stock_transactions` SET `QTY`=IFNULL(`QTY`,0)+'$totQty' 
		  WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item') AND (`TRANSACTION`='GATE_PASS') AND (`LOCATION_ID`='$location')";
            $result_u = $db->RunQuery2($sql_u);
        }
		else{
			$sql_u2	="INSERT INTO `trn_po_prn_details_sales_order_stock_transactions` (`ORDER_NO`, `ORDER_YEAR`, `SALES_ORDER`, `ITEM`, `LOCATION_ID`, `TRANSACTION`, `QTY`) VALUES ('$orderNo', '$orderYear', '$so', '$item', '$location', 'GATE_PASS', '$totQty')";
			$result_u2 	= $db->RunQuery2($sql_u2);
		}
		
 	}
}


function gatepassTransferInApprove($transfNo,$year){
   global $db;
   global $userId;
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
		$sql = "UPDATE `ware_gatepasstransferinheader` SET `intStatus`=intStatus-1 WHERE (intGpTransfNo='$transfNo') AND (`intGpTransfYear`='$year') and intStatus not in(0,1)";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Approval error10!";
		}
		
		if($result){
			  $sql = "SELECT  ware_gatepasstransferinheader.intUser, 
			  		ware_gatepasstransferinheader.intStatus, 
					ware_gatepasstransferinheader.intApproveLevels, 
					ware_gatepasstransferinheader.intCompanyId as location,
					mst_locations.intCompanyId as companyId 
					FROM
					ware_gatepasstransferinheader 
					Inner Join mst_locations ON ware_gatepasstransferinheader.intCompanyId = mst_locations.intId 
					WHERE (intGpTransfNo='$transfNo') AND (`intGpTransfYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$createdUser = $row['intUser'];
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			$company = $row['companyId'];
			$location = $row['location'];
			
			$approveLevel=$savedLevels+1-$status;
			$approveLevelFirst = $approveLevel-1;
			$sqlI = "INSERT INTO `ware_gatepasstransferinheader_approvedby` (`intGpTransfNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
				VALUES ('$transfNo','$year','$approveLevelFirst','$userId',now(),0)";
				$sqlII = "INSERT INTO `ware_gatepasstransferinheader_approvedby` (`intGpTransfNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
				VALUES ('$transfNo','$year','$approveLevel','$userId',now(),0)";
			$resultI = $db->RunQuery2($sqlI);
			$resultII = $db->RunQuery2($sqlII);
			if((!$resultI) && (!$resultII) && ($rollBackFlag!=1)){
				$rollBackFlag=1;
				$sqlM=$sqlI;
				$rollBackMsg = "Approval error11!";
			}
			
		
		//--------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
				$sql1 = "SELECT 
						 ware_gatepasstransferindetails.strOrderNo,
						 ware_gatepasstransferindetails.intOrderYear, 
						 ware_gatepasstransferindetails.strStyleNo,
 						 ware_gatepasstransferindetails.dblQty,
						 ware_gatepasstransferindetails.intItemId,  
						 ware_gatepasstransferinheader.intGatePassNo,
						 ware_gatepasstransferinheader.intGatePassYear,
						 ware_gatepassheader.intCompanyId,  
						 ware_gatepassheader.intGPToLocation,
						 mst_locations.intCompanyId AS MasterCompany,						 
						 mst_item.intBomItem 
						FROM 
						 ware_gatepasstransferindetails  
						Inner Join ware_gatepasstransferinheader ON ware_gatepasstransferindetails.intGpTransfNo = ware_gatepasstransferinheader.intGpTransfNo AND ware_gatepasstransferindetails.intGpTransfYear = ware_gatepasstransferinheader.intGpTransfYear
						Inner Join ware_gatepassheader ON ware_gatepasstransferinheader.intGatePassNo = ware_gatepassheader.intGatePassNo AND ware_gatepasstransferinheader.intGatePassYear = ware_gatepassheader.intGatePassYear
						Inner Join mst_item ON  ware_gatepasstransferindetails.intItemId = mst_item.intId
						Inner Join mst_locations ON  ware_gatepassheader.intGPToLocation = mst_locations.intId
						WHERE
						 ware_gatepasstransferindetails.intGpTransfNo =  '$transfNo' AND
						 ware_gatepasstransferindetails.intGpTransfYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				$toSavedQty=0;
				$transSavedQty=0;
				while($row=mysqli_fetch_array($result1))
				{
					$toSavedQty+=round($row['dblQty'],4);
					$gpNo=$row['intGatePassNo'];
					$gpYear=$row['intGatePassYear'];
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					$orderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$styleNo=$row['strStyleNo'];
					$locationId=$row['intCompanyId'];
					$company=$row['MasterCompany'];
					
					if($row['strOrderNo']!=0){//bom item
						//$resultG = getGrnWiseStockBalance($locationId,$gpNo,$gpYear,$orderNo,$orderYear,$styleNo,$item);
						$resultG = getGrnWiseStockBalance_bulk_GatePass($locationId,$gpNo,$gpYear,$item);
						while($rowG=mysqli_fetch_array($resultG)){
								if(($Qty>0) && (($rowG['gpQty']*(-1))>0)){
									if($Qty<=$rowG['gpQty']*(-1)){
									$saveQty=$Qty;
									$Qty=0;
									}
									else if($Qty>$rowG['gpQty']*(-1)){
									$saveQty=$rowG['gpQty']*(-1);
									$Qty=$Qty-$saveQty;
									}
									$grnNo=$rowG['intGRNNo'];
									$grnYear=$rowG['intGRNYear'];
									$grnDate=$rowG['dtGRNDate'];
									$grnRate=$rowG['dblGRNRate'];	
									$currency=loadCurrency($grnNo,$grnYear,$item);
									$saveQty=round($saveQty,4);
									if($saveQty>0){ 
									/*$sqlI = "INSERT INTO `ware_stocktransactions` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intOrderNo,intOrderYear,intSalesOrderId,intItemId,dblQty,strType,intUser,dtDate) 
										VALUES ('$company','$location','$transfNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$orderNo','$orderYear','$styleNo','$item','$saveQty','TRANSFERIN','$userId',now())";*/
									$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intOrderNo,intOrderYear,intSalesOrderId,intUser,dtDate) 
										VALUES ('$company','$location','$transfNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','$saveQty','TRANSFERIN',$orderNo,'$orderYear','$styleNo','$userId',now())";
									$resultI = $db->RunQuery2($sqlI);
									if($resultI){
										$transSavedQty+=$saveQty;
									}
									if((!$resultI) && ($rollBackFlag!=1)){
										$rollBackFlag=1;
										$sqlM=$sqlI;
										$rollBackMsg = "Approval error12!";
									}
									
									}
								}
						}
					}
					else{//general item
						$resultG = getGrnWiseStockBalance_bulk_GatePass($locationId,$gpNo,$gpYear,$item);
						while($rowG=mysqli_fetch_array($resultG)){
								if(($Qty>0) && (($rowG['gpQty']*(-1))>0)){
									if($Qty<=$rowG['gpQty']*(-1)){
									$saveQty=$Qty;
									$Qty=0;
									}
									else if($Qty>$rowG['gpQty']*(-1)){
									$saveQty=$rowG['gpQty']*(-1);
									$Qty=$Qty-$saveQty;
									}
									$grnNo=$rowG['intGRNNo'];
									$grnYear=$rowG['intGRNYear'];
									$grnDate=$rowG['dtGRNDate'];
									$grnRate=$rowG['dblGRNRate'];	
									$currency=loadCurrency($grnNo,$grnYear,$item);
									$saveQty=round($saveQty,4);
									if($saveQty>0){
									$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
										VALUES ('$company','$location','$transfNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','$saveQty','TRANSFERIN','$userId',now())";
									$resultI = $db->RunQuery2($sqlI);
									if($resultI){
										$transSavedQty+=$saveQty;
									}
									if((!$resultI) && ($rollBackFlag!=1)){
										$rollBackFlag=1;
										$sqlM=$sqlI;
										$rollBackMsg = "Approval error13!";
									}
									
									}
								}
						}
					}
				}
			}
		//--------------------------------------------------------------------
			if($status==$savedLevels){//if first confirmation raised,gp qty field in po table shld update
			 	$sql1 = "SELECT
						 ware_gatepasstransferindetails.intItemId,
						 ware_gatepasstransferindetails.dblQty,
						 ware_gatepasstransferindetails.strOrderNo,
						 ware_gatepasstransferindetails.intOrderYear, 
						 ware_gatepasstransferindetails.strStyleNo,
						 ware_gatepasstransferinheader.intGatePassNo,
						 ware_gatepasstransferinheader.intGatePassYear
						FROM
						ware_gatepasstransferindetails
						Inner Join ware_gatepasstransferinheader ON ware_gatepasstransferindetails.intGpTransfNo = ware_gatepasstransferinheader.intGpTransfNo AND ware_gatepasstransferindetails.intGpTransfYear = ware_gatepasstransferinheader.intGpTransfYear
						WHERE
						 ware_gatepasstransferinheader.intGpTransfNo =  '$transfNo' AND
						 ware_gatepasstransferinheader.intGpTransfYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$orderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$styleNo=$row['strStyleNo'];
					$gpNo=$row['intGatePassNo'];
					$gpYear=$row['intGatePassYear'];
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					
					$sql = "UPDATE `ware_gatepassdetails` SET 	dblTransferQty = dblTransferQty+'$Qty' 
							WHERE 
							 ware_gatepassdetails.strOrderNo =  '$orderNo' AND
							 ware_gatepassdetails.intOrderYear =  '$orderYear' AND
							 ware_gatepassdetails.strStyleNo =  '$styleNo' AND
							 ware_gatepassdetails.intGatePassNo =  '$gpNo' AND
							 ware_gatepassdetails.intGatePassYear =  '$gpYear' AND
							 ware_gatepassdetails.intItemId =  '$item' ";
					$result = $db->RunQuery2($sql);
					if((!$result) && ($rollBackFlag!=1)){
						$rollBackFlag=1;
						$sqlM=$sql;
						$rollBackMsg = "Approval error14!";
					}
				}
			}
		//---------------------------------------------------------------------------
		}
		
		if($status==1 ){
			//echo "sdfdsfdwf";
			//update_po_prn_details_for_gp_qty($gatePassNo,$year);
			update_po_prn_details_transactions_gatepass_transferIN($transfNo,$year);
		}
		
		$toSavedQty=round($toSavedQty,4);
		$transSavedQty=round($transSavedQty,4);
		
		if(($toSavedQty!=$transSavedQty) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$rollBackMsg = "Approval Error15";
			$sqlM=$toSavedQty."-".$transSavedQty;
		}
		
		if(($rollBackFlag==0) &&($status==1)){

			$response['type'] 		= 'pass';
			$response['msg'] 		= "Final Approval raised sucessfully";
			$response['q'] 			= $sql;
		}
		else if($rollBackFlag==0){

			$response['type'] 		= 'pass';
			$response['msg'] 		= "Approval raised sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){

			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		
		//----------------------------------------
			return $response;
		//-----------------------------------------	
		
	
	
	
}

function update_po_prn_details_transactions_gatepass_transferIN($transfNo,$year){
	global $db;
	$sql="SELECT
			ware_gatepasstransferindetails.strOrderNo,
			ware_gatepasstransferindetails.intOrderYear,
			ware_gatepasstransferindetails.strStyleNo,
			ware_gatepasstransferindetails.intItemId,
			ware_gatepasstransferindetails.dblQty,
			ware_gatepasstransferinheader.intCompanyId AS `from`,
			trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AS saved_pri
			FROM
			ware_gatepasstransferindetails
			INNER JOIN ware_gatepasstransferinheader ON ware_gatepasstransferindetails.intGpTransfNo = ware_gatepasstransferinheader.intGpTransfNo AND ware_gatepasstransferindetails.intGpTransfYear = ware_gatepasstransferinheader.intGpTransfYear
			LEFT JOIN trn_po_prn_details_sales_order_stock_transactions ON ware_gatepasstransferindetails.strOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND ware_gatepasstransferindetails.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR AND ware_gatepasstransferindetails.strStyleNo = trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER AND ware_gatepasstransferindetails.intItemId = trn_po_prn_details_sales_order_stock_transactions.ITEM  
			AND (trn_po_prn_details_sales_order_stock_transactions.TRANSACTION='GATE_PASS_IN' ) 
			AND trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID =ware_gatepasstransferinheader.intCompanyId
			WHERE
			ware_gatepasstransferindetails.intGpTransfNo = '$transfNo' AND
			ware_gatepasstransferindetails.intGpTransfYear = '$year'
			";
	$result = $db->RunQuery2($sql);
	while($row	=mysqli_fetch_array($result)){
		$orderNo			= $row['strOrderNo'];
		$orderYear			= $row['intOrderYear'];
		$so					= $row['strStyleNo'];
		$item				= $row['intItemId'];
		$totQty				= $row['dblQty'];
		$saved_pri				= $row['saved_pri'];
		$location			= $row['from'];
		if($saved_pri > 0){
        //PRI ITEM
            $sql_u	="UPDATE `trn_po_prn_details_sales_order_stock_transactions` SET `QTY`=IFNULL(`QTY`,0)+'$totQty' 
		  WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item') AND (`TRANSACTION`='GATE_PASS_IN') AND (`LOCATION_ID`='$location')";
            $result_u = $db->RunQuery2($sql_u);
        }
		else{
			$sql_u2	="INSERT INTO `trn_po_prn_details_sales_order_stock_transactions` (`ORDER_NO`, `ORDER_YEAR`, `SALES_ORDER`, `ITEM`, `LOCATION_ID`, `TRANSACTION`, `QTY`) VALUES ('$orderNo', '$orderYear', '$so', '$item', '$location', 'GATE_PASS_IN', '$totQty')";
			$result_u2 	= $db->RunQuery2($sql_u2);
		}
		
 	}
}

function getGrnWiseStockBalance_bulk_GatePass($location,$gpNo,$gpYear,$item)
{
	global $db;
	 $sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) AS gpQty,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear,
			ware_stocktransactions_bulk.dtGRNDate,
			ware_stocktransactions_bulk.dblGRNRate
			FROM ware_stocktransactions_bulk
			WHERE
			ware_stocktransactions_bulk.intItemId =  '$item' AND
			ware_stocktransactions_bulk.strType =  'GATEPASS' AND
			ware_stocktransactions_bulk.intDocumentNo =  '$gpNo' AND 
			ware_stocktransactions_bulk.intDocumntYear =  '$gpYear' AND 
			ware_stocktransactions_bulk.intLocationId =  '$location'
			GROUP BY
			ware_stocktransactions_bulk.intItemId,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear, 
			ware_stocktransactions_bulk.dblGRNRate 
			ORDER BY
			ware_stocktransactions_bulk.dtGRNDate ASC";
			
		

	$result = $db->RunQuery2($sql);
	return $result;	
}

function updateAzureDBTables($issueNo, $issueYear, $db, $intCompanyId)
{
    global $objAzure;
    $environment = $objAzure->getEnvironment();
    $table = 'Inventory'.$environment;

    $azure_connection = $objAzure->connectAzureDB();
    $sql_select = "SELECT
                    CONCAT(
                        issue.intIssueNo,
                        '/',
                        issue.intIssueYear
                    ) AS ISSUE_NO,
                    issue.strOrderNo,
                    issue.intOrderYear,
                    issue.strStyleNo,
                    trn_orderdetails.strSalesOrderNo,
                    trn_orderdetails.intSalesOrderId,
                    trn_orderdetails.strGraphicNo AS graphicNo,
                    CONCAT(
                        issue.intMrnNo,
                        '/',
                        issue.intMrnYear
                    ) AS MRN_NO,
                    issue.intItemId,
                    Sum(issue.dblQty) AS dblQty,
                    mst_item.strCode AS item_code,
                    mst_item.dblLastPrice AS price,
                    mst_maincategory.strName AS mainCategory,
                    mst_maincategory.intId AS mainCatId,
                    mst_subcategory.strName AS subCategory,
                    mst_maincategory.intSendDetails AS sendMainCat,
                    mst_subcategory.intSendDetails AS sendSubCat,
                    mst_part.strName AS part,
                    ware_issueheader.issueFrom AS location,
                    ware_issueheader.intDepartment AS department_code,
                    mst_supplier.strCode AS vendor_no,
                    mst_item.strName AS description,
                    IFNULL(ware_mrnheader.mrnType, 0) AS MRN_TYPE,
                    (
                        SELECT
                            
                                SUM(
                                    
                                        ware_grnheader.dblExchangeRate * ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate
                                    
                                )/ SUM(ware_stocktransactions_bulk.dblQty)
                        FROM
                            ware_stocktransactions_bulk
                        LEFT JOIN ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo
                        AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear
                        WHERE
                            ware_stocktransactions_bulk.intDocumentNo = issue.intIssueNo
                        AND ware_stocktransactions_bulk.intDocumntYear = issue.intIssueYear
                        AND ware_stocktransactions_bulk.intItemId = issue.intItemId
                        AND ware_stocktransactions_bulk.strType = 'ISSUE'
                        GROUP BY
                            ware_stocktransactions_bulk.intDocumentNo,
                            ware_stocktransactions_bulk.intDocumntYear,
                            ware_stocktransactions_bulk.intItemId
                    ) AS grn_rate
                FROM
                    ware_issuedetails issue
                INNER JOIN ware_issueheader ON issue.intIssueNo = ware_issueheader.intIssueNo
                AND issue.intIssueYear = ware_issueheader.intIssueYear
                LEFT JOIN trn_orderdetails ON issue.strOrderNo = trn_orderdetails.intOrderNo
                AND issue.intOrderYear = trn_orderdetails.intOrderYear
                AND issue.strStyleNo = trn_orderdetails.intSalesOrderId
                INNER JOIN mst_item ON issue.intItemId = mst_item.intId
                INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
                INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
                LEFT JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
                INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
                LEFT JOIN mst_supplier ON ware_issueheader.intSupplier = mst_supplier.intId
                INNER JOIN mst_department ON mst_department.intId = ware_issueheader.intDepartment
                INNER JOIN ware_mrnheader ON issue.intMrnNo = ware_mrnheader.intMrnNo
                AND issue.intMrnYear = ware_mrnheader.intMrnYear
                WHERE
                    issue.intIssueNo = '$issueNo'
                AND issue.intIssueYear = '$issueYear'
                GROUP BY
                    issue.strOrderNo,
                    issue.intOrderYear,
                    issue.strStyleNo,
                    issue.intMrnNo,
                    issue.intMrnYear,
                    issue.intItemId";


    $result = $db->RunQuery2($sql_select);
    $line_no = rand(100,1000);
    while ($row = mysqli_fetch_array($result)) {
        $issue_no = $row['ISSUE_NO'];
        $mrn_no = $row['MRN_NO'];
        $item_category = $row['mainCategory'];
        $item_subcategory = $row['subCategory'];
        $item_code = $row['item_code'];
        $orderNo = $row['strOrderNo'];
        $orderYear = $row['intOrderYear'];
        $line_no++;
        if($orderNo == '0'){
            $orderString = '';
        }
        else{
            $orderString = $orderNo.'/'.$orderYear;
        }
        $graphic_no = $row['graphicNo'];
        $sales_order_no = $row['strSalesOrderNo'];
        $description = str_replace("'","''",$row['description']);
        $unit_price = $row['price'];
        $qty = $row['dblQty'];
        $amount = (-1) * round($qty * $row['grn_rate'],2);
        $location = $row['location'];
        $dept_code = $row['department_code'];
        $vendor_code = $row['vendor_no'];
        $mrn_type = $row['MRN_TYPE'];
        $successHeader = '0';
        $postingDate = date('Y-m-d H:i:s');
        $sendDetails = ($row['sendMainCat'] == 1 && $row['sendSubCat'] == 1)? true:false;

        if($sendDetails) {
            $sql_azure_header = "INSERT INTO $table (Document_Category, Document_No, Line_No, External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Order_No, Graphic_No, Sales_Order_No, Description, Amount, Location, Department_Code, Vendor_No, MRN_Type) VALUES ('IssueNote', '$issue_no', '$line_no', '$mrn_no', '$postingDate', '$item_category', '$item_subcategory', '$item_code', '$orderString','$graphic_no', '$sales_order_no', '$description', '$amount','$location','$dept_code','$vendor_code', '$mrn_type')";
            if ($azure_connection) {
                $getResultsHeader = $objAzure->runQuery($azure_connection, $sql_azure_header);
                if ($getResultsHeader != FALSE) {
                    $successHeader = '1';
                }
            }
            $sql_db_header = "INSERT INTO trn_financemodule_inventory (Document_Category, Document_No, Line_No, External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Order_No, Graphic_No, Sales_Order_No, Description, Amount, Location, Department_Code, Vendor_No, MRN_Type, deliveryStatus, deliveryDate) VALUES ('IssueNote', '$issue_no', '$line_no','$mrn_no', '$postingDate', '$item_category', '$item_subcategory', '$item_code', '$orderString','$graphic_no', '$sales_order_no', '$description', '$amount','$location','$dept_code','$vendor_code', '$mrn_type','$successHeader', NOW())";
            $result_db_header = $db->RunQuery2($sql_db_header);
        }

    }

}

function updateAzureDBTablesOnGP($gatePassNo, $gatePassYear, $db, $intCompanyId)
{
    global $objAzure;
    $environment = $objAzure->getEnvironment();
    $table = 'Inventory'.$environment;

    $azure_connection = $objAzure->connectAzureDB();
    $sql_select = "SELECT
                    CONCAT(
                        GPD.intGatePassNo,
                        '/',
                        GPD.intGatePassYear
                    ) AS GP_NO,
                    GPD.intItemId,
                    Sum(GPD.dblQty) AS dblQty,
                    mst_item.strCode AS item_code,
                    mst_item.strName AS description,
                    mst_item.dblLastPrice AS price,
                    mst_maincategory.strName AS mainCategory,
                    mst_subcategory.strName AS subCategory,
                    mst_maincategory.intSendDetails AS sendMainCat,
                    mst_subcategory.intSendDetails AS sendSubCat,
                    GPH.intCompanyId AS location,
                    (
                        SELECT
                            round(
                                sum(
                                    (
                                        ware_grnheader.dblExchangeRate * ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate
                                    ) / 1.0000
                                ),
                                2
                            )
                        FROM
                            ware_stocktransactions_bulk
                        LEFT JOIN ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo
                        AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear
                        WHERE
                            ware_stocktransactions_bulk.intDocumentNo = GPD.intGatePassNo
                        AND ware_stocktransactions_bulk.intDocumntYear = GPD.intGatePassYear
                        AND ware_stocktransactions_bulk.intItemId = GPD.intItemId
                        AND ware_stocktransactions_bulk.strType = 'GATEPASS'
                        GROUP BY
                            ware_stocktransactions_bulk.intDocumentNo,
                            ware_stocktransactions_bulk.intDocumntYear,
                            ware_stocktransactions_bulk.intItemId
                    ) AS amount
                FROM
                    ware_gatepassdetails GPD
                INNER JOIN ware_gatepassheader AS GPH ON GPD.intGatePassNo = GPH.intGatePassNo
                AND GPD.intGatePassYear = GPH.intGatePassYear
                INNER JOIN mst_item ON GPD.intItemId = mst_item.intId
                INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
                INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
                WHERE
                    GPD.intGatePassNo = '$gatePassNo'
                AND GPD.intGatePassYear = '$gatePassYear'
                GROUP BY
                    GPD.intItemId";

    $result = $db->RunQuery2($sql_select);
    $line_no = rand(100,1000);
    while ($row = mysqli_fetch_array($result)) {
        $line_no++;
        $gp_no = $row['GP_NO'];
        $item_category = $row['mainCategory'];
        $item_subcategory = $row['subCategory'];
        $item_code = $row['item_code'];
        $description = str_replace("'","''",$row['description']);
        $unit_price = $row['price'];
        $qty = $row['dblQty'];
        // GP records should be sent as negative
        $amount = $row['amount'];
        $location = $row['location'];
        $successHeader = '0';
        $postingDate = date('Y-m-d H:i:s');
        $sendDetails = ($row['sendMainCat'] == 1 && $row['sendSubCat'] ==1)? true:false;

        if($sendDetails) {
            $sql_azure_header = "INSERT INTO $table (Document_Category, Document_No, Line_No, External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Description, Amount, Location) VALUES ('GatePass', '$gp_no', '$line_no', '$gp_no', '$postingDate', '$item_category', '$item_subcategory', '$item_code', '$description', '$amount','$location')";
            if ($azure_connection) {
                $getResultsHeader = $objAzure->runQuery($azure_connection, $sql_azure_header);
                if ($getResultsHeader != FALSE) {
                    $successHeader = '1';
                }
            }
            $sql_db_header = "INSERT INTO trn_financemodule_inventory (Document_Category, Document_No, Line_No, External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Description, Amount, Location, deliveryStatus, deliveryDate) VALUES ('GatePass', '$gp_no', '$line_no','$gp_no', '$postingDate', '$item_category', '$item_subcategory', '$item_code', '$description', '$amount','$location','$successHeader', NOW())";
            $result_db_header = $db->RunQuery2($sql_db_header);
        }

    }

}

function updateAzureDBTablesOnGPIN($gpTrInNo, $gpTrInYear, $db, $intCompanyId)
{
    global $objAzure;
    $environment = $objAzure->getEnvironment();
    $table = 'Inventory'.$environment;

    $azure_connection = $objAzure->connectAzureDB();
    $sql_select = "SELECT
                    CONCAT(
                        GPIND.intGpTransfNo,
                        '/',
                        GPIND.intGpTransfYear
                    ) AS GP_IN_NO,
                    CONCAT(
                        GPH.intGatePassNo,
                        '/',
                        GPH.intGatePassYear
                    ) AS GP_NO,
                    ware_gatepasstransferinheader.intCompanyId AS location,
                    GPIND.intItemId,
                    Sum(GPIND.dblQty) AS dblQty,
                    mst_item.strCode AS item_code,
                    mst_item.dblLastPrice AS price,
                    mst_item.strName AS description,
                    mst_maincategory.strName AS mainCategory,
                    mst_maincategory.intSendDetails AS sendMainCat,
                    mst_subcategory.intSendDetails AS sendSubCat,
                    mst_subcategory.strName AS subCategory,
                    (
                        SELECT
                            round(
                                sum(
                                    (
                                        (- 1) * ware_grnheader.dblExchangeRate * ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate
                                    ) / 1.0000
                                ),
                                2
                            )
                        FROM
                            ware_stocktransactions_bulk
                        LEFT JOIN ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo
                        AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear
                        WHERE
                            ware_stocktransactions_bulk.intDocumentNo = GPH.intGatePassNo
                        AND ware_stocktransactions_bulk.intDocumntYear = GPH.intGatePassYear
                        AND ware_stocktransactions_bulk.intItemId = GPIND.intItemId
                        AND ware_stocktransactions_bulk.strType = 'GATEPASS'
                        GROUP BY
                            ware_stocktransactions_bulk.intDocumentNo,
                            ware_stocktransactions_bulk.intDocumntYear,
                            ware_stocktransactions_bulk.intItemId
                    ) AS amount
                FROM
                    ware_gatepasstransferindetails GPIND
                INNER JOIN ware_gatepasstransferinheader ON GPIND.intGpTransfNo = ware_gatepasstransferinheader.intGpTransfNo
                AND GPIND.intGpTransfYear = ware_gatepasstransferinheader.intGpTransfYear
                INNER JOIN ware_gatepassheader GPH ON ware_gatepasstransferinheader.intGatePassNo = GPH.intGatePassNo
                AND ware_gatepasstransferinheader.intGatePassYear = GPH.intGatePassYear
                INNER JOIN mst_item ON GPIND.intItemId = mst_item.intId
                INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
                INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
                WHERE
                    GPIND.intGpTransfNo = '$gpTrInNo'
                AND GPIND.intGpTransfYear = '$gpTrInYear'
                GROUP BY
                    GPIND.intItemId";

    $result = $db->RunQuery2($sql_select);
    $line_no = rand(100,1000);
    while ($row = mysqli_fetch_array($result)) {
        $line_no++;
        $gp_in_no = $row['GP_IN_NO'];
        $gp_no = $row['GP_NO'];
        $item_category = $row['mainCategory'];
        $item_subcategory = $row['subCategory'];
        $item_code = $row['item_code'];
        $description = str_replace("'","''",$row['description']);
        $unit_price = $row['price'];
        $qty = $row['dblQty'];
        $amount = $row['amount'];
        $location = $row['location'];
        $successHeader = '0';
        $postingDate = date('Y-m-d H:i:s');
        $sendDetails = ($row['sendMainCat'] == 1 && $row['sendSubCat'] == 1)? true:false;

        if($sendDetails) {
            $sql_azure_header = "INSERT INTO $table (Document_Category, Document_No, Line_No, External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Description, Amount, Location) VALUES ('TransferIn', '$gp_in_no', '$line_no','$gp_no', '$postingDate', '$item_category', '$item_subcategory', '$item_code', '$description', '$amount','$location')";
            if ($azure_connection) {
                $getResultsHeader = $objAzure->runQuery($azure_connection, $sql_azure_header);
                if ($getResultsHeader != FALSE) {
                    $successHeader = '1';
                }
            }
            $sql_db_header = "INSERT INTO trn_financemodule_inventory (Document_Category, Document_No, Line_No, External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Description, Amount, Location, deliveryStatus, deliveryDate) VALUES ('TransferIn', '$gp_in_no', '$line_no', '$gp_no', '$postingDate', '$item_category', '$item_subcategory', '$item_code', '$description', '$amount','$location','$successHeader', NOW())";
            $result_db_header = $db->RunQuery2($sql_db_header);
        }

    }

}

function getNextSysNo($db, $companyId, $transaction_type){
	    $current_serial_no = $companyId.'00000';
	    $sql = "SELECT `$transaction_type` FROM `trn_financemodule_sysNo` WHERE `COMPANY_ID` = '$companyId'";
	    $result = $db->RunQuery2($sql);
        $row = mysqli_fetch_array($result);
        if($row[$transaction_type] != ''){
           $current_serial_no = $row[$transaction_type];
        }
        //update next serial no
        $sql_update = "UPDATE `trn_financemodule_sysNo` SET `$transaction_type`='$current_serial_no'+1 WHERE `COMPANY_ID`='$companyId'";
        $result = $db->RunQuery2($sql_update);
        return $current_serial_no;
}
	


?>
<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
	ini_set('display_errors',0);
	(session_id() == ''?session_start():'');
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$location 	= $_SESSION['CompanyID'];
//	$company 	= $_SESSION['headCompanyId'];
	$companyId 	= $_SESSION['headCompanyId'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once "../../../../class/warehouse/cls_warehouse_get.php";
	
	$objwhouseget= new cls_warehouse_get($db);
	
	$programName='Material Issued Note';
	$programCode='P0231';
	$issueApproveLevel = (int)getApproveLevel($programName);
	
		$db->OpenConnection();
		$db->RunQuery2('Begin');

 if($requestType=='getValidation')
	{
		$issueNo  = $_REQUEST['issueNo'];
		$issueNoArray=explode("/",$issueNo);
		
		///////////////////////
		$sql = "SELECT
		ware_issueheader.intStatus, 
		ware_issueheader.intApproveLevels 
		FROM ware_issueheader
		WHERE
		ware_issueheader.intIssueNo =  '$issueNoArray[0]' AND 
		ware_issueheader.intIssueYear =  '$issueNoArray[1]'";

		$result = $db->RunQuery2($sql);
		$errorFlg=0;
		$row=mysqli_fetch_array($result);
		$status=$row['intStatus'];
		$approveLevels=$row['intApproveLevels'];	
		
		////////////////////////
		$confirmatonMode=loadConfirmatonMode($programCode,$status,$approveLevels,$userId);
		
		if($status==1){// 
			$errorFlg = 1;
			$msg ="Final confirmation of this Issue Note is already raised"; 
		}
		else if($confirmatonMode==0){// 
			$errorFlg = 1;
			$msg ="No Permission to Approve"; 
		}
		
		
	
		if($errorFlg == 0){
		
/*		           $sql = "SELECT 
		 		ware_issuedetails.strOrderNo,
				ware_issuedetails.intOrderYear,
				trn_orderdetails.strSalesOrderNo,
				trn_orderdetails.intSalesOrderId,
				ware_issuedetails.intMrnNo,
				ware_issuedetails.intMrnYear,
				ware_issuedetails.intItemId, 
				mst_item.intBomItem,
				mst_item.strName as itemName,
				ware_mrndetails.dblQty as dblMrnQty,
				ware_mrndetails.dblIssudQty , 
				ware_issuedetails.dblQty,
				ware_issueheader.intApproveLevels,  
				ware_issueheader.intStatus ,
				mst_locations.intId,
				mst_locations.intCompanyId
				FROM 
				ware_issueheader
				Inner Join ware_issuedetails ON ware_issueheader.intIssueNo = ware_issuedetails.intIssueNo AND ware_issueheader.intIssueYear = ware_issuedetails.intIssueYear
				Inner Join ware_mrndetails ON ware_issuedetails.strOrderNo = ware_mrndetails.intOrderNo AND ware_issuedetails.intOrderYear = ware_mrndetails.intOrderYear AND ware_issuedetails.strStyleNo = ware_mrndetails.strStyleNo AND ware_issuedetails.intMrnNo = ware_mrndetails.intMrnNo AND ware_issuedetails.intMrnYear = ware_mrndetails.intMrnYear AND ware_issuedetails.intItemId = ware_mrndetails.intItemId
				left Join trn_orderdetails ON ware_mrndetails.intOrderNo = trn_orderdetails.intOrderNo AND ware_mrndetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_mrndetails.strStyleNo = trn_orderdetails.intSalesOrderId
				Inner Join mst_item ON ware_issuedetails.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId  
				Inner Join ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
				Inner Join mst_locations ON ware_issueheader.intCompanyId = mst_locations.intId
				WHERE 
				ware_mrnheader.intStatus =  '1' AND
				ware_issueheader.intIssueNo =  '$issueNoArray[0]' AND
				ware_issueheader.intIssueYear =  '$issueNoArray[1]'";
*/	
		//$result = $db->RunQuery($sql);
		$sql = loadIssueDetails($issueNoArray[0],$issueNoArray[1]);
		$result = $db->RunQuery2($sql);
		$errorFlg=0;
		$msg ="Maximum Qtys for items"; 
		$bomItemFlag=1;
		$totTosaveQty=0;
		$totSavableQty=0;
		while($row=mysqli_fetch_array($result))
		{	
		 	 $issue_compnay=$row['companyId'];
			 if($row['intstockMaintain'] == 1){
			 $issue_location=$row['locationId'];
			 }
			 else{
				 $issue_location = $location;
			 }
			 
			 $status=$row['intStatus'];
			 $savingLevels=$row['intApproveLevels'];
			 
			 $intStockMaintain=$row['intstockMaintain'];
			 
			 $issueQty=$row['issueQty'];
			 $returnQty=$row['returnQty'];
			 $mrnQty=$row['mrnQty'];
			 $mrnNo=$row['intMrnNo'];
			 $mrnYear=$row['intMrnYear'];
			 $orderNo=$row['strOrderNo'];
			 $orderYear=$row['intOrderYear'];
			 $salesOrderId=$row['intSalesOrderId'];
			 $item=$row['intItemId'];
			 $totIssueQty=geTotConfirmedIssueQtys($mrnNo,$mrnYear,$orderNo,$orderYear,$salesOrderId,$item);
			// $totRetStoresQty=geTotConfirmedStockRetStoresQtys($mrnNo,$mrnYear,$orderNo,$orderYear,$salesOrderId,$item);
			 
			if($row['strOrderNo']==0){
				$bomItemFlag=0;
			}
			else{
				$bomItemFlag=0; //$bomItemFlag=1;
			}
			
			if($bomItemFlag==0){//general item
			$stockBalQty=getStockBalance_bulk($issue_location,$item);
			}
			else{//bom item
			$stockBalQty=getStockBalance($issue_compnay,$issue_location,$orderNo,$orderYear,$salesOrderId,$item);
			}
			$stockBalQty=round($stockBalQty,4);
				
			if($row['intStatus']==($row['intApproveLevels']+1)){//no confirmation has been raised
				//$balQtyToIssue=$mrnQty-$totIssueQty+$totRetStoresQty;
				$balQtyToIssue=($mrnQty+$returnQty)-$totIssueQty;
			}
			else{//confirmation has been raised
	
				//$balQtyToIssue=$mrnQty-$totIssueQty+$issueQty+$totRetStoresQty;
				$balQtyToIssue=($mrnQty+$returnQty)-$totIssueQty+$issueQty;
		}
		
		$balQtyToIssue1=$balQtyToIssue;
		//echo  "m-".$mrnQty."i-".$issueQty."s-".$stockBalQty."bi-".$balQtyToIssue."ti-".$totIssueQty;
		if($stockBalQty<$balQtyToIssue){
			$balQtyToIssue=$stockBalQty;
		}
			
			
			//--------------------------
			if($intStockMaintain == 1){
			$response['arrData'] = $objwhouseget->getLocationValidation('ISSUE',$location,$mrnNo,$mrnYear);//check for issue location
			
			}
			
			//--------------------------
			
/*			if($response['arrData']['type']=='fail'){
				 $rollBackFlagLoc=1;
				 $msg=$response['arrData']['msg'];
			 }
*/			
		//	if($rollBackFlagLoc!=1){
			if(round($issueQty,4) > round($balQtyToIssue1,4)){//save Qty greater than the balance to issue Qty for this mrn
				$errorFlg=1;
				$msg ="Issue Qty is greater than the balance to issue Qty for <br>".$row['strOrderNo']."/".$row['intOrderYear']."-".$row['intSalesOrderId']."-".$row['intMrnNo']."-".$row['intMrnYear']."-".$row['itemName']." =".$issueQty.">".$balQtyToIssue1;
			}
			else if($stockBalQty<$issueQty){//issue Qty greater than the stock balance
				$errorFlg=1;
				$msg ="No stock balance to issue for <br>".$row['strOrderNo']."/".$row['intOrderYear']."-".$row['intSalesOrderId']."-".$row['intMrnNo']."-".$row['intMrnYear']."-".$row['itemName']." =".$stockBalQty;
			}
			else if($balQtyToIssue<=0){// no balance Qty to issue
				$errorFlg=1;
				$msg ="All MRN Qty Issued for <br>".$row['strOrderNo']."/".$row['intOrderYear']."-".$row['intSalesOrderId']."-".$row['intMrnNo']."-".$row['intMrnYear']."-".$row['itemName']." =".$balQtyToIssue;
			}
			else if(round($issueQty,4)>round($balQtyToIssue,4)){//
				$errorFlg=1;
				if($bomItemFlag==1){
					$msg .="<br>".$row['strOrderNo']."/".$row['intOrderYear']."-".$row['intSalesOrderId']."-".$row['intMrnNo']."-".$row['intMrnYear']."-".$row['itemName']." =".$balQtyToIssue;
				}
				else{
					$msg .="<br> ".$row['intMrnNo']."-".$row['intMrnYear']."-".$row['itemName']." =".$balQtyToIssue;
				}
			}
			
			//}
		
				$qty[$item]+=$issueQty;
				//$balToIssuety[$item]+=$balQtyToIssue;
			 	//echo $qty[$item];
				if($errorFlg!=1){
					if($qty[$item]>$stockBalQty){
						$errorFlg=1;
						$msg.="</br> ".$row['itemName']." (stock Balance)=".$stockBalQty;
					}
				}
			//	$errorFlg=1;
	
			$totTosaveQty +=$issueQty;
			$totSavableQty +=$balQtyToIssue;
		}//end of while
	
	}
	
	
	if($rollBackFlagLoc==1){
	   $errorFlg=1;
	}
	else if((round($totTosaveQty,4) > round($totSavableQty,4)) && ($errorFlg!=1)){
		$errorFlg=1;
		$msg ="Balance to Issue Qty is not tally with This Qtys.";
	}
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}
//---------------------------
else if($requestType=='validateRejecton')
	{
		$issueNo  = $_REQUEST['issueNo'];
		$issueNoArray=explode("/",$issueNo);
		
		///////////////////////
		$sql = "SELECT
		ware_issueheader.intStatus, 
		ware_issueheader.intApproveLevels  
		FROM ware_issueheader 
		WHERE
		ware_issueheader.intIssueNo =  '$issueNoArray[0]' AND 
		ware_issueheader.intIssueYear =  '$issueNoArray[1]'";

		$result = $db->RunQuery2($sql);
		$errorFlg=0;
		$msg =""; 
		$row=mysqli_fetch_array($result);
		
		$rejectionMode=loadRejectionMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);
		
		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this Issue Note is already raised.So cant reject"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This Issue Note is already Rejected"; 
		}
		else if($rejectionMode==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="No Permission to reject this Issue Note"; 
		}
		else{//confirmation has been raised
			$errorFlg = 0;
			
		}
		
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}


		$db->RunQuery2('Commit');
		$db->CloseConnection();		
		

	//-----------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$salesOrderId,$item)
	{
		global $db;
		  $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//-----------------------------------------------------------------
	function loadIssueDetails($serialNo,$year)
	{
		global $db;
		   $sql = "SELECT
					ware_issuedetails.intIssueYear,
					ware_mrndetails.intMrnNo,
					ware_mrndetails.intMrnYear,
					ware_mrndetails.intItemId,
					ware_mrndetails.dblQty AS mrnQty,
					ware_issuedetails.dblQty AS issueQty,
					ware_issueheader.intCompanyId AS locationId,
					mst_locations.intCompanyId AS companyId,
					mst_locations.int_stock_locationin AS intstockMaintain,
					ware_issueheader.intStatus,
					ware_issueheader.intApproveLevels,
					ware_issuedetails.strOrderNo,
					ware_issuedetails.intOrderYear,
					ware_issuedetails.strStyleNo as intSalesOrderId , 
					mst_item.strName as itemName,
					(select sum(WID.dblReturnQty)   from ware_issuedetails as WID
					where  WID.strOrderNo = ware_mrndetails.intOrderNo and 
					WID.intOrderYear = ware_mrndetails.intOrderYear AND 
					WID.strStyleNo = ware_mrndetails.strStyleNo AND 
					WID.intMrnNo = ware_mrndetails.intMrnNo 
					AND WID.intMrnYear = ware_mrndetails.intMrnYear 
					AND WID.intItemId = ware_mrndetails.intItemId) AS returnQty
					FROM
					ware_issuedetails
					Inner Join ware_mrndetails ON ware_issuedetails.strOrderNo = ware_mrndetails.intOrderNo AND ware_issuedetails.intOrderYear = ware_mrndetails.intOrderYear AND ware_issuedetails.strStyleNo = ware_mrndetails.strStyleNo AND ware_issuedetails.intMrnNo = ware_mrndetails.intMrnNo AND ware_issuedetails.intMrnYear = ware_mrndetails.intMrnYear AND ware_issuedetails.intItemId = ware_mrndetails.intItemId
					Inner Join ware_issueheader ON ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo AND ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear
					Inner Join mst_locations ON ware_issueheader.intCompanyId = mst_locations.intId 
					Inner Join mst_item ON ware_issuedetails.intItemId = mst_item.intId
					WHERE
					ware_issuedetails.intIssueNo =  '$serialNo' AND
					ware_issuedetails.intIssueYear =  '$year'
					ORDER BY
					ware_issuedetails.intIssueNo ASC,
					ware_mrndetails.intMrnNo ASC,
					ware_mrndetails.intItemId ASC";

		return $sql;
	}
	//-----------------------------------------------------------------
	function geTotConfirmedIssueQtys($mrnNo,$mrnYear,$orderNo,$orderYear,$salesOrderId,$item){
		global $db;
		 $sql="SELECT
			Sum(ware_issuedetails.dblQty) as issuedQty 
			FROM
			ware_issuedetails
			Inner Join ware_issueheader ON ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo AND ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear
			WHERE
			ware_issueheader.intStatus >  0 AND
			ware_issueheader.intApproveLevels >=  ware_issueheader.intStatus AND
			ware_issuedetails.intMrnNo =  '$mrnNo' AND
			ware_issuedetails.intMrnYear =  '$mrnYear' AND
			ware_issuedetails.strOrderNo =  '$orderNo' AND
			ware_issuedetails.intOrderYear =  '$orderYear' AND
			ware_issuedetails.strStyleNo =  '$salesOrderId' AND 
			ware_issuedetails.intItemId =  '$item' 
			GROUP BY
			ware_issuedetails.intItemId,
			ware_issuedetails.strStyleNo,
			ware_issuedetails.intOrderYear,
			ware_issuedetails.strOrderNo,
			ware_issuedetails.intMrnYear,
			ware_issuedetails.intMrnNo 
			";
		
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$issuedQty = val($row['issuedQty']);
			return $issuedQty;
	}
	//-----------------------------------------------------------------
	function geTotConfirmedStockRetStoresQtys($mrnNo,$mrnYear,$orderNo,$orderYear,$salesOrderId,$item){
		global $db;
		 $sql="SELECT
			Sum(ware_issuedetails.dblReturnQty) as returnQty 
			FROM
			ware_issuedetails
			Inner Join ware_issueheader ON ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo AND ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear
			WHERE
			ware_issueheader.intStatus =  1 AND
			ware_issuedetails.intMrnNo =  '$mrnNo' AND
			ware_issuedetails.intMrnYear =  '$mrnYear' AND
			ware_issuedetails.strOrderNo =  '$orderNo' AND
			ware_issuedetails.intOrderYear =  '$orderYear' AND
			ware_issuedetails.strStyleNo =  '$salesOrderId' AND 
			ware_issuedetails.intItemId =  '$item' 
			GROUP BY
			ware_issuedetails.intItemId,
			ware_issuedetails.strStyleNo,
			ware_issuedetails.intOrderYear,
			ware_issuedetails.strOrderNo,
			ware_issuedetails.intMrnYear,
			ware_issuedetails.intMrnNo,
			ware_issueheader.intStatus,
			ware_issueheader.intApproveLevels
			";
		
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$issuedQty = val($row['returnQty']);
			return $issuedQty;
	}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery2($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery2($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//--------------------------------------------------------

?>

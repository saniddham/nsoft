//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
// JavaScript Document
var basePath = "presentation/warehouse/issue/listing/";

$(document).ready(function() {
	$('#frmIssueReport').validationEngine();
	
	$('#frmIssueReport #imgApprove').click(function(){
	$("#frmIssueReport #imgApprove").hide();
	$("#frmIssueReport #imgReject").hide();
	var val = $.prompt('Are you sure you want to approve this Issue Note ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
					showWaiting();
					if(validateQuantities()==0){
					///////////
					var url = basePath+"rptIssue-db-set.php"+window.location.search+'&status=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$("#frmIssueReport #imgApprove").show();
								$("#frmIssueReport #imgReject").show();
								$('#frmIssueReport #imgApprove').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									// $("#frmIssueReport #imgApprove").attr("disabled", false);
									// $("#frmIssueReport #imgReject").attr("disabled", false);
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								$("#frmIssueReport #imgApprove").show();
								$("#frmIssueReport #imgReject").show();
								$('#frmIssueReport #imgApprove').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
 					   hideWaiting();
					}
				}});
	});
	
	$('#frmIssueReport #imgReject').click(function(){
		$("#frmIssueReport #imgApprove").hide();
		$("#frmIssueReport #imgReject").hide();
	var val = $.prompt('Are you sure you want to reject this Issue Note ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
					showWaiting();
					if(validateRejecton()==0){
					///////////
					var url = basePath+"rptIssue-db-set.php"+window.location.search+'&status=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$("#frmIssueReport #imgApprove").show();
								$("#frmIssueReport #imgReject").show();
								$('#frmIssueReport #imgReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								$("#frmIssueReport #imgApprove").show();
								$("#frmIssueReport #imgReject").show();
								$('#frmIssueReport #imgReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
						hideWaiting();
					}
				}});
	});
	
});
//---------------------------------------------------
function validateQuantities(){
		var ret=0;	
	    var issueNo = document.getElementById('divIssueNo').innerHTML;
		var url 		= basePath+"rptIssue-db-get.php?requestType=getValidation";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"issueNo="+issueNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$("#frmIssueReport #imgApprove").show();
					$("#frmIssueReport #imgReject").show();
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",3000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//---------------------------------------------------
function validateRejecton(){
		var ret=0;	
	    var issueNo = document.getElementById('divIssueNo').innerHTML;
		var url 		= basePath+"rptIssue-db-get.php?requestType=validateRejecton";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"issueNo="+issueNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$("#frmIssueReport #imgApprove").hide();
					$("#frmIssueReport #imgReject").hide();
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",3000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//------------------------------------------------
function alertx()
{
	$('#frmIssueReport #imgApprove').validationEngine('hide')	;
}
//--------------------------------------------
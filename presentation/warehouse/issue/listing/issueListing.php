<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
 require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";

$company 		= $_SESSION['headCompanyId'];
$location 		= $_SESSION['CompanyID'];
$intUser  		= $_SESSION["userId"];

$programName	='Material Issued Note';
$programCode	='P0231';
$menuID			='232';
$reportMenuId	='891';

$userDepartment=getUserDepartment($intUser);
$approveLevel 	= (int)getMaxApproveLevel();


################################################################
################## new part ####################################
################################################################
///// create where part ///////////////////
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'Status'=>'tb1.intStatus',
				'Issue_No'=>'tb1.intIssueNo',
				'Issue_Year'=>'tb1.intIssueYear',
				'MRN_NO'=>'ware_issuedetails.intMrnNo',
				'DEPARTMENT_NAME'=>'DE.strName',
				'Date'=>'substr(tb1.datdate,1,10)',
				'User'=>'sys_users.strUserName'
				);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2');

//----------year-----------------------
$str_year = '';
for ($y = date("Y"); $y >= 2018; $y--) {
    $str_year .= $y . ":" . $y . ";";
}
$str_year .= ":All";
if($arr){
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
}
if(!count($arr)>0)					 
	$where_string .= "AND substr(tb1.datdate,1,10) = '".date('Y-m-d')."'";
	
################## end code ####################################

$locations = loadAvailableLocations();

$sql = "select * from(SELECT DISTINCT 
							if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected','Pending')) as Status,
							tb1.intIssueNo as `Issue_No`,
							tb1.intIssueYear as `Issue_Year`,
							
							issued_from.strName as iss_from,
							issued_to.strName as iss_to, 
							
							ware_gatepasstransferinheader.intGpTransfNo,
							ware_gatepasstransferinheader.intGpTransfYear,
							
							(SELECT GROUP_CONCAT(DISTINCT SUB_ID.intMrnNo,'/',SUB_ID.intMrnYear) 
							FROM ware_issuedetails SUB_ID
							WHERE SUB_ID.intIssueNo =  tb1.intIssueNo
								AND SUB_ID.intIssueYear =  tb1.intIssueYear) AS MRN_NO,
							
							(SELECT GROUP_CONCAT(DISTINCT IF(SUB_ID.strOrderNo=0,'',SUB_ID.strOrderNo),IF(SUB_ID.strOrderNo=0,'','/'),IF(SUB_ID.intOrderYear=0,'',SUB_ID.intOrderYear)) 
							FROM ware_issuedetails SUB_ID
							WHERE SUB_ID.intIssueNo =  tb1.intIssueNo
								AND SUB_ID.intIssueYear =  tb1.intIssueYear) AS ORDER_NO,
							
							DE.strName		AS DEPARTMENT_NAME,
		
							tb1.datdate as `Date`,
							sys_users.strUserName as `User`,

							IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(ware_issueheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_issueheader_approvedby
								Inner Join sys_users ON ware_issueheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_issueheader_approvedby.intIssueNo  = tb1.intIssueNo AND
								ware_issueheader_approvedby.intYear =  tb1.intIssueYear AND
								ware_issueheader_approvedby.intApproveLevelNo =  '1' AND 
								ware_issueheader_approvedby.intStatus =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.intStatus>1),'Approve', '')) as `1st_Approval`,  ";

						for($i=2; $i<=$approveLevel; $i++){

							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}


						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_issueheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_issueheader_approvedby
								Inner Join sys_users ON ware_issueheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_issueheader_approvedby.intIssueNo  = tb1.intIssueNo AND
								ware_issueheader_approvedby.intYear =  tb1.intIssueYear AND
								ware_issueheader_approvedby.intApproveLevelNo =  '$i' AND 
								ware_issueheader_approvedby.intStatus =  '0' 
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.intStatus>1) AND (tb1.intStatus<=tb1.intApproveLevels) AND ((SELECT
								max(ware_issueheader_approvedby.dtApprovedDate) 
								FROM
								ware_issueheader_approvedby
								Inner Join sys_users ON ware_issueheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_issueheader_approvedby.intIssueNo  = tb1.intIssueNo AND
								ware_issueheader_approvedby.intYear =  tb1.intIssueYear AND
								ware_issueheader_approvedby.intApproveLevelNo =  ($i-1) AND 
								ware_issueheader_approvedby.intStatus =  '0')<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*fase part*/
								 if($i>tb1.intApproveLevels,'-----',''))
								
								/*end IFNULL false part*/
								) as `".$approval."`, ";

								}

							$sql .= "'View' as `View`   
						FROM
							ware_issueheader as tb1  
							INNER JOIN ware_issuedetails ON ware_issuedetails.intIssueNo = tb1.intIssueNo AND
							ware_issuedetails.intIssueYear = tb1.intIssueYear
							LEFT JOIN ware_gatepassheader ON tb1.intGatePassNo = ware_gatepassheader.intGatePassNo
							LEFT JOIN ware_gatepasstransferinheader ON ware_gatepassheader.intGatePassNo = ware_gatepasstransferinheader.intGatePassNo
							left JOIN mst_locations AS issued_to ON tb1.intCompanyId = issued_to.intId
							left JOIN mst_locations AS issued_from ON tb1.issueFrom = issued_from.intId
							Inner Join sys_users ON tb1.intUser = sys_users.intUserId 
							Inner Join mst_locations ON tb1.intCompanyId = mst_locations.intId
							INNER JOIN mst_department DE ON DE.intId = tb1.intDepartment
							WHERE
							mst_locations.intCompanyId =  '$company' AND 
							tb1.intCompanyId IN  ($locations)
							$where_string
							)  as t where 1=1
							GROUP BY Issue_No,Issue_Year
						";
					  	   // echo $sql;
						 
$col = array();


//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;	$col=NULL;

//PO No
$col["title"] 		= "Issue No"; // caption of column
$col["name"] 		= "Issue_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 		= "2";
//searchOper
$col["align"] 		= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist

$reportLink  		= "?q=$reportMenuId&issueNo={Issue_No}&year={Issue_Year}";
$reportLinkApprove  = "?q=$reportMenuId&issueNo={Issue_No}&year={Issue_Year}&approveMode=1";
$formLink			= "?q=231&issueNo={Issue_No}&year={Issue_Year}";

$col['link']		= $formLink;	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

$cols[] = $col;	
$col=NULL;

//PO Year
$col["title"] = "Issue Year"; // caption of column
$col["name"] = "Issue_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$col["stype"] = "select";
$str_year = $str_year;
$col["editoptions"] = array("value" => $str_year);
$cols[] = $col;	$col=NULL;

//PO Year
$col["title"] = "Issue From"; // caption of column
$col["name"] = "iss_from"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//PO Year
$col["title"] = "Issue To"; // caption of column
$col["name"] = "iss_to"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//PO Year
$col["title"] 		= "Gp In No"; // caption of column
$col["name"]  		= "intGpTransfNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 		= "2";
$col["align"] 		= "center";
$reportLink_gp_in	= "?q=902&transfNo={intGpTransfNo}&year={intGpTransfYear}";
$col['link']		= $reportLink_gp_in;	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;


//PO Year
$col["title"] = "Gp In Year"; // caption of column
$col["name"] = "intGpTransfYear"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


$col["title"] 	= "MRN No"; // caption of column
$col["name"] 	= "MRN_NO"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
$col["align"] 	= "left";
$cols[] 		= $col;	
$col			= NULL;

$col["title"] 	= "Graphic No"; // caption of column
$col["name"] 	= "graphic_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] 	= "2";
$col["align"] 	= "left";
$cols[] 		= $col;	
$col			= NULL;

//Supplier
$col["title"] 	= "Department"; // caption of column
$col["name"] 	= "DEPARTMENT_NAME"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
$col["align"] 	= "left";
$cols[] 		= $col;	
$col			= NULL;

$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//User
$col["title"] = "User"; // caption of column
$col["name"] = "User"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//FIRST APPROVAL
$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}


//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Issue Note Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'Issue_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);



$out = $jq->render("list1");
?>
<title>Issue Listing</title>
<form id="frmlisting" name="frmlisting" method="post" action="">

        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
</form>

<?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(ware_issueheader.intApproveLevels) AS appLevel
			FROM ware_issueheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
//------------------------------function load User Department---------------------
function getUserDepartment($intUser){
	global $db;

	$sqlp = "SELECT
			sys_users.intDepartmentId
			FROM
			sys_users
			WHERE
			sys_users.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['intDepartmentId'];

}
function loadAvailableLocations(){
	global $db;
	global $location;
	global $company;

	// Getting location type
				  $locationSql = "Select strName,intCompanyId,int_stock_locationin,COMPANY_MAIN_STORES_LOCATION FROM mst_locations WHERE intId = $location";
				  $locationQuery = $db->RunQuery($locationSql);
				  $locationResult = mysqli_fetch_array($locationQuery);
				  
				  // if location is Company Main stores Locations
				  if($locationResult["COMPANY_MAIN_STORES_LOCATION"] == 1) {
				  
				  //getting current location data
				  $locationSql = "Select intId,strName FROM mst_locations WHERE intId = $location ";
				  $locationQuery = $db->RunQuery($locationSql);
				  $locationList = '';
				  //getting All Location IDs
				  $locations = $location;
				  while($row = mysqli_fetch_array($locationQuery)){
					  if($locationList != ''){
					  $locationList .= ",".$row['intId'];
				  }
				  else{
					  $locationList .= $row['intId'];
				  }
				  }
				  		  
				  //getting all not maintaining stock locations according to the Main Company
				  $notMaintainSql = "Select intId,strName FROM mst_locations WHERE int_stock_locationin = 0 AND intCompanyId = $company ";
				  $notMaintainQuery = $db->RunQuery($notMaintainSql);
				 
				  //getting All Location IDs
				  $locations = $location;
				  while($row = mysqli_fetch_array($notMaintainQuery)){
					   	  if($locationList != ''){
					  $locationList .= ",".$row['intId'];
				  }
				  else{
					  $locationList .= $row['intId'];
				  }
				  }
				  
				  return  $locationList;
		}
	else{
				//getting current location data
				  $locationSql = "Select intId,strName FROM mst_locations WHERE intId = $location ";
				  $locationQuery = $db->RunQuery($locationSql);
				  $locationList = '';
				  //getting All Location IDs
				  $locations = $location;
				  while($row = mysqli_fetch_array($locationQuery)){
					  $locationList .= $row['intId'];
				  }
				  return  $locationList;
	}
}
?>


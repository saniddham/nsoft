<?php (define('UNLOCKPAGE', true) ? die('<<< Access denied >>>') : ''); ?>
<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////

$companyId = $_SESSION['CompanyID'];
$intUser = $_SESSION["userId"];

$issueNo = $_REQUEST['issueNo'];
$year = $_REQUEST['year'];
$approveMode = (!isset($_REQUEST['approveMode']) ? '' : $_REQUEST['approveMode']);

$programName = 'Material Issued Note';
$programCode = 'P0231';
//$issueApproveLevel = (int)getApproveLevel($programName);

/*$issueNo = '100000';
$year = '2012';
$approveMode=1;
*/
$sql = "select 
tb1.delToCompany,
tb1.delToLocation,
tb1.datdate,
tb1.strRemarks,
tb1.intStatus,
tb1.intApproveLevels,
tb1.intUser,
tb1.poRaisedLocationId, 
tb1.strUserName, 
tb1.strName, 
tb1.fromLocation,
tb1.GatepassNo,
tb1.supplier,
mst_department.strName as requestedDepartment,
ware_mrnheader.mrnType 
from (SELECT 
ware_issueheader.intIssueNo, 
ware_issueheader.intIssueYear, 
mst_companies.strName as delToCompany,
mst_locations.strName as delToLocation,
ware_issueheader.datdate,
ware_issueheader.strRemarks,
ware_issueheader.intStatus,
ware_issueheader.intApproveLevels,
ware_issueheader.intUser,
ware_issueheader.intCompanyId as poRaisedLocationId, 
locations.strName as fromLocation,
sys_users.strUserName, 
ware_issueheader.intGatePassNo as GatepassNo,
mst_department.strName,
mst_supplier.strName as supplier 
FROM
ware_issueheader 
Inner Join mst_department ON ware_issueheader.intDepartment = mst_department.intId
Inner Join mst_locations ON ware_issueheader.intCompanyId = mst_locations.intId
Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
Inner Join sys_users ON ware_issueheader.intUser = sys_users.intUserId
left Join mst_locations AS locations ON ware_issueheader.issueFrom = locations.intId
LEFT JOIN mst_supplier ON ware_issueheader.intSupplier = mst_supplier.intId
WHERE
ware_issueheader.intIssueNo =  '$issueNo' AND
ware_issueheader.intIssueYear =  '$year' 
GROUP BY
ware_issueheader.intIssueNo,
ware_issueheader.intIssueYear) as tb1 
Inner Join ware_issuedetails ON tb1.intIssueNo = ware_issuedetails.intIssueNo AND tb1.intIssueYear = ware_issuedetails.intIssueYear
Inner Join ware_mrnheader ON ware_issuedetails.intMrnNo = ware_mrnheader.intMrnNo AND ware_issuedetails.intMrnYear = ware_mrnheader.intMrnYear
Inner Join mst_department ON ware_mrnheader.intDepartment = mst_department.intId
";
$result = $db->RunQuery($sql);
while ($row = mysqli_fetch_array($result)) {
    $company = $row['delToCompany'];
    $location = $row['delToLocation'];
    $locationId = $row['poRaisedLocationId'];//this locationId use in report header(reportHeader.php)--------------------
    $department = $row['strName'];
    $issueDate = $row['datdate'];
    $issueBy = $row['strUserName'];
    $remarks = $row['strRemarks'];
    $intStatus = $row['intStatus'];
    $savedLevels = $row['intApproveLevels'];
    $user = $row['strUserName'];
    $createUserId = $row['intUser'];
    $mrnDepartment = $row['requestedDepartment'];
    $ToLocation = $row['delToLocation'];
    $fromLocation = $row['fromLocation'];
    $GatepassNo = $row['GatepassNo'];
    $supplier = $row['supplier'];
    if ($row['mrnType'] == 0)
        $mrnType = "Non RM";
    else if ($row['mrnType'] == 1)
        $mrnType = "Sample";
    else if ($row['mrnType'] == 2)
        $mrnType = "Technical R&D";
    else if ($row['mrnType'] == 3)
        $mrnType = "Production Orderwise";
    else if ($row['mrnType'] == 5)
        $mrnType = "Production Non Orderwise";
    else if ($row['mrnType'] == 6)
        $mrnType = "Production Orderwise Additional";
    else if ($row['mrnType'] == 7)
        $mrnType = "Loan settlement to Supplier";
    else if ($row['mrnType'] == 8)
        $mrnType = "Loan out to Supplier";
}

$confirmationMode = loadConfirmatonMode($programCode, $intStatus, $savedLevels, $intUser);
$rejectionMode = loadRejectionMode($programCode, $intStatus, $savedLevels, $intUser);
?>
<head>
    <title>Issue Note Report</title>
    <script type="application/javascript" src="presentation/warehouse/issue/listing/rptIssue-js.js"></script>
    <style>
        .break {
            page-break-before: always;
        }

        @media print {
            .noPrint {
                display: none;
            }
        }

        #apDiv1 {
            position: absolute;
            left: 255px;
            top: 170px;
            width: 650px;
            height: 322px;
            z-index: 1;
        }

        .APPROVE {
            font-size: 18px;
            font-weight: bold;
        }
    </style>
</head>

<body>
<?php
if ($intStatus > 1)//pending
{
    ?>
    <div id="apDiv1"><img src="images/pending.png"/></div>
    <?php
}
?>
<form id="frmIssueReport" name="frmIssueReport" method="post">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td width="20%"></td>
            <td width="60%" height="80" valign="top"><?php include 'reportHeader.php' ?></td>
            <td width="20%"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
    </table>
    <div align="center">
        <div style="background-color:#FFF"><strong>ISSUE NOTE</strong><strong></strong></div>
        <table width="900" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td colspan="9" align="center" bgcolor="#FFDFCB">
                                <?php
                                if ($intStatus > 1) {
                                    ?>
                                    <?php if ($approveMode == 1) {
                                        if ($confirmationMode == 1) {
                                            ?>
                                            <img src="images/approve.png" align="middle" class="mouseover noPrint"
                                                 id="imgApprove"/>
                                            <?php
                                        }
                                        if ($rejectionMode == 1) {
                                            ?>
                                            <img src="images/reject.png" align="middle" class="mouseover noPrint"
                                                 id="imgReject"/>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <?php
                            if ($intStatus == 1) {
                                ?>
                                <td colspan="9" class="APPROVE">CONFIRMED</td>
                                <?PHP
                            } else if ($intStatus == 0) {
                                ?>
                                <td colspan="9" class="APPROVE" style="color:#F00">REJECTED</td>
                                <?php
                            } else {
                                ?>
                                <td width="10%" colspan="9" class="APPROVE">PENDING</td>
                                <?php
                            }
                            ?>
                        </tr>
                        <tr>
                            <td width="1%">&nbsp;</td>
                            <td width="7%"><span class="normalfnt"><strong>Issue No</strong></span></td>
                            <td width="1%" align="center" valign="middle"><strong>:</strong></td>
                            <td width="26%"><span class="normalfnt"><?php echo $issueNo ?>/<?php echo $year ?></span></td>
                            <td width="18%" class="normalfnt"><strong>Issue Department</strong></td>
                            <td width="2%" align="center" valign="middle"><strong>:</strong></td>
                            <td width="22%"><span class="normalfnt"><?php echo $department ?></span></td>
							
                            <td width="1%"><div id="divIssueNo" style="display:none"><?php echo $issueNo ?>/<?php echo $year ?></div></td>
                            <td width="3%"></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="normalfnt"><strong>Issue By</strong></td>
                            <td align="center" valign="middle"><strong>:</strong></td>
                            <td><span class="normalfnt"><?php echo $issueBy ?></span></td>
                            <td><span class="normalfnt"><strong>Requested Department</strong></span></td>
                            <td align="center" valign="middle"><strong>:</strong></td>
                            <td><span class="normalfnt"><?php echo $mrnDepartment ?></span></td>
                            <td class="normalfnt">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="normalfnt"><strong>Date</strong></td>
                            <td align="center" valign="middle"><strong>:</strong></td>
                            <td><span class="normalfnt"><?php echo $issueDate ?></span></td>
                            <td class="normalfnt"><strong>MRN Type</strong></td>
                            <td align="center" valign="middle"><strong>:</strong></td>
                            <td class="normalfnt"><span><?php echo $mrnType ?></span></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="normalfnt"><strong>Remarks</strong></td>
                            <td align="center" valign="middle"><strong>:</strong></td>
                            <td colspan="1" align="left"><span class="normalfnt"><?php echo $remarks ?></span></td>
							
							 <td class="normalfnt"><strong>Issue Location(From)</strong></td>
							 <td align="center" valign="middle"><strong>:</strong></td>
                            <td colspan="2" align="left"><span class="normalfnt"><?php echo $fromLocation ?></span></td>
                        </tr>
						 <tr>
                            <td>&nbsp;</td>
                            <td class="normalfnt"><strong><?php if($GatepassNo != null) { echo 'Gatepass No';} ?></strong></td>
                            <td align="center" valign="middle"><strong><?php if($GatepassNo != null) { echo ':';} ?></strong></td>
                            <td colspan="1" align="left"><span class="normalfnt"><?php if($GatepassNo != null) { echo $GatepassNo;} ?></span></td>
							
							 <td class="normalfnt"><strong>Issue Location(To)</strong></td>
							 <td align="center" valign="middle"><strong>:</strong></td>
                            <td colspan="2" align="left"><span class="normalfnt"><?php echo $ToLocation ?></span></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="normalfnt"><strong><?php if($supplier != null || $supplier != '') { echo 'Supplier';} ?></strong></td>
                            <td align="center" valign="middle"><strong><?php if($supplier != null || $supplier != '') { echo ':';} ?></strong></td>
                            <td colspan="1" align="left"><span class="normalfnt"><?php if($supplier != null || $supplier != '') { echo $supplier;} ?></span></td>

                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="normalfnt">&nbsp;</td>
                            <td align="center" valign="middle">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="5%">&nbsp;</td>
                            <td colspan="7" class="normalfnt">
                                <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <th width="9%">Order No</th>
                                        <th width="11%">Sales Order No</th>
                                        <th width="11%">MRN No</th>
                                        <th width="11%">Main Category</th>
                                        <th width="14%">Sub Category</th>
                                        <th width="10%">Item Code</th>
                                        <th width="17%">Item</th>
                                        <th width="8%">UOM</th>
                                        <th width="9%">Qty</th>
                                    </tr>
                                    <?php
                                    $sql1 = "SELECT
									ware_issuedetails.intIssueNo,
									ware_issuedetails.intIssueYear,
									ware_issuedetails.strOrderNo,
									ware_issuedetails.intOrderYear,
									ware_issuedetails.strStyleNo,
									trn_orderdetails.strSalesOrderNo,
									trn_orderdetails.intSalesOrderId,
									ware_issuedetails.intMrnNo,
									ware_issuedetails.intMrnYear,
									ware_issuedetails.intItemId,
									Sum(ware_issuedetails.dblQty) as dblQty,
									mst_item.strCode,
									mst_item.strName,
                                                                        mst_item.strCode as SUP_ITEM_CODE,
									mst_item.intUOM, 
                                                                        mst_item.ITEM_HIDE,
									mst_maincategory.strName AS mainCategory,
                                                                        mst_maincategory.intId AS mainCatId,
									mst_subcategory.strName AS subCategory, 
									mst_part.strName as part , 
									mst_units.strCode as uom  
									FROM ware_issuedetails 
									left Join trn_orderdetails ON ware_issuedetails.strOrderNo = trn_orderdetails.intOrderNo AND ware_issuedetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_issuedetails.strStyleNo = trn_orderdetails.intSalesOrderId
									Inner Join mst_item ON ware_issuedetails.intItemId = mst_item.intId
									Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
									Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
									left Join mst_part ON trn_orderdetails.intPart = mst_part.intId  
									Inner Join mst_units ON mst_item.intUOM = mst_units.intId
                                                                        LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
									WHERE
									ware_issuedetails.intIssueNo =  '$issueNo' AND
									ware_issuedetails.intIssueYear =  '$year' 
									GROUP BY
									ware_issuedetails.strOrderNo,
									ware_issuedetails.intOrderYear,
									ware_issuedetails.strStyleNo,
									ware_issuedetails.intMrnNo,
									ware_issuedetails.intMrnYear,
									ware_issuedetails.intItemId 
									";
																		$result1 = $db->RunQuery($sql1);
                                    $totQty = 0;
                                    $totAmmount = 0;
                                    while ($row = mysqli_fetch_array($result1)) {
                                        $orderNo = $row['strOrderNo'] . "/" . $row['intOrderYear'];
                                        $salesOrderNo = $row['strSalesOrderNo'] . "/" . $row['part'];
                                        $mainCatId =  $row['mainCatId']; 
                                        if ($row['strOrderNo'] == 0) {
                                            $orderNo = '';
                                            $salesOrderNo = '';
                                        }
                                        ?>
                                        <tr class="normalfnt" bgcolor="#FFFFFF">
                                            <td class="normalfnt">&nbsp;<?php echo $orderNo; ?>&nbsp;</td>
                                            <td class="normalfnt">&nbsp;<?php echo $salesOrderNo ?>&nbsp;</td>
                                            <td class="normalfnt">
                                                &nbsp;<?php echo $row['intMrnNo'] . "/" . $row['intMrnYear']; ?>
                                                &nbsp;</td>
                                            <td class="normalfnt">&nbsp;<?php echo $row['mainCategory'] ?>&nbsp;</td>
                                            <td class="normalfnt">&nbsp;<?php echo $row['subCategory'] ?>&nbsp;</td>
                                            <?php
                                            if($row['SUP_ITEM_CODE']!=null)
                                            {
                                            ?>
                                            <td class="normalfnt">&nbsp;<?php echo $row['SUP_ITEM_CODE'] ?>&nbsp;</td>
                                            <?php
                                            }else{
                                            ?>
                                            <td class="normalfnt">&nbsp;<?php echo $row['strCode'] ?>&nbsp;</td>
                                            <?php
                                            }
                                           
                                            if($row['ITEM_HIDE'] ==1)
                                            {
                                            ?>    
                                            
                                            <td class="normalfnt">&nbsp;*****&nbsp;</td>
                                            <?php
                                            }else{
                                            ?>
                                             <td class="normalfnt">&nbsp;<?php echo $row['strName'] ?>&nbsp;</td>
                                            <?php
                                            }
                                            ?>
                                            
                                            <td class="normalfntMid"><?php echo $row['uom'] ?></td>
                                            <td class="normalfntRight">&nbsp;<?php echo $row['dblQty'] ?>&nbsp;</td>
                                        </tr>
                                        <?php
                                        $totQty += $row['dblQty'];
                                    }
                                    ?>
                                    <tr class="normalfnt" bgcolor="#CCCCCC">
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfntRight">&nbsp;</td>
                                        <td class="normalfntRight">&nbsp;<?php echo $totQty ?>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                            <td width="6%">&nbsp;</td>
                        </tr>
                        <tr></tr>
                        <tr></tr>
                        <tr></tr>
                        <tr><td><strong>Summary</strong></td></tr>
                        <!-- Summary Table-->
                        <tr>
                            <td width="5%">&nbsp;</td>
                            <td colspan="7" class="normalfnt">
                                <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <th width="11%">Main Category</th>
                                        <th width="14%">Sub Category</th>
                                        <th width="10%">Item Code</th>
                                        <th width="17%">Item</th>
                                        <th width="8%">UOM</th>
                                        <th width="9%">Qty</th>
                                    </tr>
                                    <?php
                                    $sql1 = "SELECT
									ware_issuedetails.intIssueNo,
									ware_issuedetails.intIssueYear,
									ware_issuedetails.strOrderNo,
									ware_issuedetails.intOrderYear,
									ware_issuedetails.strStyleNo,
									trn_orderdetails.strSalesOrderNo,
									trn_orderdetails.intSalesOrderId,
									ware_issuedetails.intMrnNo,
									ware_issuedetails.intMrnYear,
									ware_issuedetails.intItemId,
									Sum(ware_issuedetails.dblQty) as dblQty,
									mst_item.strCode,
									mst_item.strName,
                                                                        mst_item.ITEM_HIDE,
                                                                        mst_item.strCode as SUP_ITEM_CODE,
									mst_item.intUOM, 
									mst_maincategory.strName AS mainCategory,
                                                                        mst_maincategory.intId AS mainCatId,
									mst_subcategory.strName AS subCategory, 
									mst_part.strName as part , 
									mst_units.strCode as uom  
									FROM ware_issuedetails 
									left Join trn_orderdetails ON ware_issuedetails.strOrderNo = trn_orderdetails.intOrderNo AND ware_issuedetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_issuedetails.strStyleNo = trn_orderdetails.intSalesOrderId
									Inner Join mst_item ON ware_issuedetails.intItemId = mst_item.intId
									Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
									Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
									left Join mst_part ON trn_orderdetails.intPart = mst_part.intId  
									Inner Join mst_units ON mst_item.intUOM = mst_units.intId
                                                                        LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
									WHERE
									ware_issuedetails.intIssueNo =  '$issueNo' AND
									ware_issuedetails.intIssueYear =  '$year' 
									GROUP BY
									ware_issuedetails.intItemId 
									";
																		$result1 = $db->RunQuery($sql1);
                                    $totQty = 0;
                                    $totAmmount = 0;
                                    while ($row = mysqli_fetch_array($result1)) {
                                        $orderNo = $row['strOrderNo'] . "/" . $row['intOrderYear'];
                                        $salesOrderNo = $row['strSalesOrderNo'] . "/" . $row['part'];
                                        $mainCatId =  $row['mainCatId']; 
                                        
                                        if ($row['strOrderNo'] == 0) {
                                            $orderNo = '';
                                            $salesOrderNo = '';
                                        }
										
                                        ?>
                                        <tr class="normalfnt" bgcolor="#FFFFFF">
                                            <td class="normalfnt">&nbsp;<?php echo $row['mainCategory'] ?>&nbsp;</td>
                                            <td class="normalfnt">&nbsp;<?php echo $row['subCategory'] ?>&nbsp;</td>
                                            
                                            <?php
                                            if($row['SUP_ITEM_CODE']!=null)
                                            {
                                            ?>
                                            <td class="normalfnt">&nbsp;<?php echo $row['SUP_ITEM_CODE'] ?>&nbsp;</td>
                                            <?php
                                            }else{
                                            ?>
                                            <td class="normalfnt">&nbsp;<?php echo $row['strCode'] ?>&nbsp;</td>
                                            <?php
                                            }
                                           
                                            if($row['ITEM_HIDE']==1)
                                            {
                                            
                                            ?>
                                            <td class="normalfnt">&nbsp;*****&nbsp;</td>
                                            <?php
                                            }
                                            else{
                                                ?>
                                            <td class="normalfnt">&nbsp;<?php echo $row['strName'] ?>&nbsp;</td>
                                            <?php
                                            }
                                            ?>
                                            <td class="normalfntMid"><?php echo $row['uom'] ?></td>
                                            <td class="normalfntRight">&nbsp;<?php echo $row['dblQty'] ?>&nbsp;</td>
                                        </tr>
                                        <?php
                                        $totQty += $row['dblQty'];
                                    }
                                    ?>
                                    <tr class="normalfnt" bgcolor="#CCCCCC">
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfntRight">&nbsp;</td>
                                        <td class="normalfntRight">&nbsp;<?php echo $totQty ?>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                            <td width="6%">&nbsp;</td>
                        </tr>
                        <!-- Summary -->
                    </table>
                </td>
            </tr>
            <?php
            $flag = 0;
            $sqlc = "SELECT
							ware_issueheader_approvedby.intApproveUser,
							ware_issueheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_issueheader_approvedby.intApproveLevelNo
							FROM
							ware_issueheader_approvedby
							Inner Join sys_users ON ware_issueheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_issueheader_approvedby.intIssueNo =  '$issueNo' AND
							ware_issueheader_approvedby.intYear =  '$year'   order by dtApprovedDate asc";
            $resultc = $db->RunQuery($sqlc);
            while ($rowc = mysqli_fetch_array($resultc)) {
                if ($rowc['intApproveLevelNo'] == 1)
                    $desc = "1st ";
                else if ($rowc['intApproveLevelNo'] == 2)
                    $desc = "2nd ";
                else if ($rowc['intApproveLevelNo'] == 3)
                    $desc = "3rd ";
                else
                    $desc = $rowc['intApproveLevelNo'] . "th ";
                //  $desc=$ap.$desc;
                $desc2 = $rowc['UserName'] . "(" . $rowc['dtApprovedDate'] . ")";
                if ($rowc['UserName'] == '')
                    $desc2 = '---------------------------------';

                if ($rowc['intApproveLevelNo'] > 0) {
                    $flag = 1;
                    ?>
                    <tr>
                        <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?>
                                    Approved By - </strong></span><span class="normalfnt"><?php echo $desc2; ?></span>
                        </td>
                    </tr>
                    <?php
                    //}
                    //else{
                }
                if ($rowc['intApproveLevelNo'] == 0) {
                    $flag = 2;
                    ?>
                    <tr>
                        <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Rejected By - </strong></span><span
                                class="normalfnt"><?php echo $rowc['UserName'] . "(" . $rowc['dtApprovedDate'] . ")"; ?></span>
                        </td>
                    </tr>
                    <?php
                }
                if ($rowc['intApproveLevelNo'] == -1) {
                    $flag = 3;
                    ?>
                    <tr>
                        <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Revised By - </strong></span><span
                                class="normalfnt"><?php echo $rowc['UserName'] . "(" . $rowc['dtApprovedDate'] . ")"; ?></span>
                        </td>
                    </tr>
                    <?php
                }

                if ($rowc['intApproveLevelNo'] > 0) {
                    $finalSavedLevel = $rowc['intApproveLevelNo'];
                }
            }//end of while

            if ($flag > 1) {
                $finalSavedLevel = 0;
            }

            if (($flag <= 1) || ($intStatus > 0)) {
                for ($j = $finalSavedLevel + 1; $j <= $savedLevels; $j++) {
                    if ($j == 1)
                        $desc = "1st ";
                    else if ($j == 2)
                        $desc = "2nd ";
                    else if ($j == 3)
                        $desc = "3rd ";
                    else
                        $desc = $j . "th ";
                    $desc2 = '---------------------------------';
                    ?>
                    <tr>
                        <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?>
                                    Approved By - </strong></span><span class="normalfnt"><?php echo $desc2; ?></span>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
            <tr height="90">
                <td align="center" class="normalfntMid"></td>
            </tr>
            <tr height="40">
                <?php
                //$locationId = '';
                $sql = "SELECT mst_locations.intCompanyId FROM mst_locations WHERE mst_locations.intId =  '$locationId'";
                $result = $db->RunQuery($sql);
                $row = mysqli_fetch_array($result);
                $createCompanyId = $row['intCompanyId'];
                ?>
                <?Php
                $url = "{$backwardseperator}presentation/sendToApproval.php";        // * file name
                $url .= "?status=$intStatus";                                        // * set recent status
                $url .= "&approveLevels=$savedLevels";                                // * set approve levels in order header
                $url .= "&programCode=$programCode";                                // * program code (ex:P10001)
                $url .= "&program=ISSUE NOTE";                                    // * program name (ex:Purchase Order)
                $url .= "&companyId=$createCompanyId";                                    // * created company id
                $url .= "&createUserId=$createUserId";

                $url .= "&field1=ISSUE No";
                $url .= "&field2=ISSUE Year";
                $url .= "&value1=$issueNo";
                $url .= "&value2=$year";

                $url .= "&subject=ISSUE NOTE FOR APPROVAL ('$issueNo'/'$year')";

                $url .= "&statement1=Please Approve this";
                $url .= "&statement2=to Approve this";
                // * doc year
                $url .= "&link=" . urlencode(base64_encode($mainPath . "presentation/warehouse/issue/listing/rptIssue.php?issueNo=$issueNo&year=$year&approveMode=1"));
                ?>
                <td align="center" class="normalfntMid">
                    <iframe id="iframeFiles2" src="<?php echo $url; ?>" name="iframeFiles"
                            style="width:500px;height:150px;border:none"></iframe>
                </td>
            </tr>
            <tr height="40">
                <td align="center" class="normalfntMid"><span
                        class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
            </tr>
        </table>
    </div>
</form>
</body>
<?php
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode, $intStatus, $savedStat, $intUser)
{
    global $db;

    $confirmatonMode = 0;
    $k = $savedStat + 2 - $intStatus;
    $sqlp = "SELECT
		menupermision.int" . $k . "Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";

    $resultp = $db->RunQuery($sqlp);
    $rowp = mysqli_fetch_array($resultp);

    if ($rowp['int' . $k . 'Approval'] == 1) {
        if ($intStatus != 1) {
            $confirmatonMode = 1;
        }
    }

    return $confirmatonMode;
}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode, $intStatus, $savedStat, $intUser)
{
    global $db;

    $rejectMode = 0;
    $sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";

    $resultp = $db->RunQuery($sqlp);
    $rowp = mysqli_fetch_array($resultp);

    if ($rowp['intReject'] == 1) {
        if ($intStatus != 0) {
            $rejectMode = 1;
        }
    }

    return $rejectMode;
}
//-------------------------------------------------------------------------


?>	

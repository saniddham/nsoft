<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
ini_set('display_errors',0);
session_start();
$backwardseperator = "../../../../";
$mainPath 	= $_SESSION['mainPath'];
$userId 	= $_SESSION['userId'];
$location 	= $_SESSION['CompanyID'];
$companyId 	= $_SESSION['headCompanyId'];
$requestType 	= $_REQUEST['requestType'];
include "{$backwardseperator}dataAccess/Connector.php";
							
							
///
/////////// type of print load part /////////////////////
					if($requestType=='loadSubCategory')
					{
							$mainCategory  = $_REQUEST['mainCategory'];
							$sql = "SELECT
							mst_subcategory.intId,
							mst_subcategory.strCode,
							mst_subcategory.strName
							FROM mst_subcategory
							WHERE
							mst_subcategory.intMainCategory =  '$mainCategory'  
							ORDER BY mst_subcategory.strName ASC";
							$result = $db->RunQuery($sql);
							$html = "<option value=\"\"></option>";
							while($row=mysqli_fetch_array($result))
							{
							$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
							}
							echo $html;
							echo json_encode($response);
					}
							
					else if($requestType=='loadOrderANDmrncombo')
					{
							 $mrnType  		= $_REQUEST['mrnType'];//orderwise
							 $departmentId  = $_REQUEST['departmentId'];
							 $issueLocationId  = $_REQUEST['issueLocationId'];
								if($mrnType==1)
								{
							
							    $sql = "select 
										distinct intOrderNo,
										intOrderYear
										from(
										SELECT   
										ware_mrnheader.intMrnNo,
										ware_mrnheader.intMrnYear,
										ware_mrndetails.intOrderNo,
										ware_mrndetails.intOrderYear,
										ware_mrndetails.strStyleNo,
										ware_mrndetails.intItemId,
										(ware_mrndetails.dblQty+(sum(ifnull(ware_issuedetails.dblReturnQty,0)))-ware_mrndetails.dblIssudQty) as balance 
										FROM
										ware_mrnheader
										INNER JOIN 
										ware_mrndetails ON 
										ware_mrnheader.intMrnNo = ware_mrndetails.intMrnNo
										AND ware_mrnheader.intMrnYear = ware_mrndetails.intMrnYear
										left JOIN ware_issuedetails ON 
										ware_mrndetails.intMrnNo = ware_issuedetails.intMrnNo
										AND ware_mrndetails.intMrnYear = ware_issuedetails.intMrnYear
										AND ware_mrndetails.intOrderNo   =  ware_issuedetails.strOrderNo
										AND ware_mrndetails.intOrderYear = ware_issuedetails.intOrderYear
										AND ware_mrndetails.strStyleNo   = ware_issuedetails.strStyleNo
										AND ware_mrndetails.intItemId    = ware_issuedetails.intItemId
										WHERE
										ware_mrnheader.intStatus         = '1'
										AND ware_mrnheader.intDepartment = '$departmentId'
										AND ware_mrnheader.intCompanyId = '$issueLocationId'				
										AND ware_mrndetails.MRN_TYPE in (3,6)  
										GROUP BY
										ware_mrnheader.intMrnNo,
										ware_mrnheader.intMrnYear , 
										ware_mrndetails.intOrderNo,
										ware_mrndetails.intOrderYear,
										ware_mrndetails.strStyleNo,
										ware_mrndetails.intItemId 
										having balance > 0
										
										) as tb1 
										group by intMrnYear,intMrnNo ORDER BY intMrnNo,intMrnYear DESC";
								//echo $sql;			
								$result = $db->RunQuery($sql);
								$html = "<option value=\"\"></option>";
								while($row=mysqli_fetch_array($result))
								{
								$html .= "<option value=\"".$row['intOrderNo']."/".$row['intOrderYear']."\">".$row['intOrderNo']."/".$row['intOrderYear']."</option>";
								}
								}
								else if($mrnType==2)//non orderwise
								{
											$sql ="select 
									intMrnNo,
									intMrnYear,
									intOrderNo,
									intOrderYear,
									strStyleNo
									from(
									SELECT   
									ware_mrnheader.intMrnNo,
									ware_mrnheader.intMrnYear,
									ware_mrndetails.intOrderNo,
									ware_mrndetails.intOrderYear,
									ware_mrndetails.strStyleNo,
									ware_mrndetails.intItemId,
								 (ware_mrndetails.dblQty+(sum(ifnull(ware_issuedetails.dblReturnQty,0)))-ware_mrndetails.dblIssudQty) as balance 
								FROM
									ware_mrnheader
								INNER JOIN ware_mrndetails ON ware_mrnheader.intMrnNo = ware_mrndetails.intMrnNo
								AND ware_mrnheader.intMrnYear = ware_mrndetails.intMrnYear
								LEFT JOIN ware_issuedetails ON ware_mrndetails.intMrnNo = ware_issuedetails.intMrnNo
								AND ware_mrndetails.intMrnYear = ware_issuedetails.intMrnYear
								AND ware_mrndetails.intOrderNo = ware_issuedetails.strOrderNo
								AND ware_mrndetails.intOrderYear = ware_issuedetails.intOrderYear
								AND ware_mrndetails.strStyleNo = ware_issuedetails.strStyleNo
								AND ware_mrndetails.intItemId = ware_issuedetails.intItemId
								WHERE
								ware_mrnheader.intStatus = '1'							
								AND ware_mrnheader.intDepartment = '$departmentId'
								AND ware_mrnheader.intCompanyId = '$issueLocationId'
								AND ware_mrndetails.MRN_TYPE in (0,1,2,5,7,8)";
                        
						$sql .= "GROUP BY
									ware_mrnheader.intMrnNo,
									ware_mrnheader.intMrnYear , 
								    ware_mrndetails.intOrderNo,
									ware_mrndetails.intOrderYear,
									ware_mrndetails.strStyleNo,
								    ware_mrndetails.intItemId 
								    having balance > 0
								
								) as tb1 
							group by intMrnYear,intMrnNo ORDER BY intMrnNo DESC,intMrnYear DESC
											";
													
							$result = $db->RunQuery($sql);
							$html = "<option value=\"\"></option>";
							while($row=mysqli_fetch_array($result))
											{
											$html .= "<option value=\"".$row['intMrnNo']."/".$row['intMrnYear']."\">".$row['intMrnNo']."/".$row['intMrnYear']."</option>";
											}
								}
							$response['response'] = $html;
							echo json_encode($response);
							
							
					}
					else if($requestType=='loadStyleNo')
					{
							$orderNo  = $_REQUEST['orderNo'];
							$orderNoArray 	 = explode('/',$orderNo);
							
							$sql = "SELECT DISTINCT 
							trn_orderdetails.intSalesOrderId, 
							trn_orderdetails.strSalesOrderNo,
							mst_part.strName
							FROM
							trn_orderdetails
							Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
							WHERE 
							trn_orderdetails.intOrderNo =  '$orderNoArray[0]' AND
							trn_orderdetails.intOrderYear =  '$orderNoArray[1]'";
							$result = $db->RunQuery($sql);
							$html = "<option value=\"\"></option>";
							while($row=mysqli_fetch_array($result))
							{
							$html .= "<option value=\"".$row['intSalesOrderId']."\">".$row['strSalesOrderNo']."/".$row['strName']."</option>";
							}
							echo $html;
							//echo json_encode($response);
					}
					else if($requestType=='loadSOGrid')
						{
						$response = array('html_1'=>'','html'=>'');
						$mrnNo    = $_REQUEST['mrnNo'];
						$orderNo  = $_REQUEST['orderNo'];
						
						$mrnNoArray 	 = explode('/',$mrnNo);
						$orderNoArray 	 = explode('/',$orderNo);
						
						$sql = "SELECT
						(SELECT COUNT(DISTINCT ware_mrndetails.intOrderNo) FROM ware_mrndetails";
						if($mrnNoArray[0] != '' && $mrnNoArray[1] != ''){
						$sql .= " WHERE
						ware_mrndetails.intMrnNo = '$mrnNoArray[0]' AND
						ware_mrndetails.intMrnYear = '$mrnNoArray[1]'";
						}
						$sql .= " ) as orderNoCount,
						ware_mrndetails.intOrderNo,
						ware_mrndetails.intOrderYear,
						trn_orderdetails.strCombo,
						mst_part.strName as part,
						ware_mrndetails.strStyleNo as intSalesOrderId ,
						concat(ware_mrndetails.intOrderNo,'/',ware_mrndetails.intOrderYear,'/',trn_orderdetails.intSalesOrderId) as so,
						concat(trn_orderdetails.strSalesOrderNo) as so_desc
						FROM `ware_mrndetails`
						INNER JOIN trn_orderdetails ON ware_mrndetails.intOrderNo = trn_orderdetails.intOrderNo AND ware_mrndetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_mrndetails.strStyleNo = trn_orderdetails.intSalesOrderId
						Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId ";
						
						if($mrnNoArray[0] != '' && $mrnNoArray[1] != ''){
						$sql .= "WHERE
						ware_mrndetails.intMrnNo = '$mrnNoArray[0]' AND
						ware_mrndetails.intMrnYear = '$mrnNoArray[1]'";
						}
						if($orderNoArray[0] != '' && $orderNoArray[1] != ''){
						$sql .= "AND
						ware_mrndetails.intOrderNo = '$orderNoArray[0]' AND
						ware_mrndetails.intOrderYear = '$orderNoArray[1]'";
						}
						$sql .= "GROUP BY
						ware_mrndetails.intOrderNo,
						ware_mrndetails.intOrderYear,
						ware_mrndetails.strStyleNo";
						
						$orderCheckDuplicate = '';
						$result = $db->RunQuery($sql);
						$graphicList = array();

						//echo $sql;
						$html_1 = "<option value=\"\"></option>";
						$html = "<tr id=\"\"><th><input name=\"chkSoAll\" id=\"chkSoAll\" class=\"chkSoAll\" type=\"checkbox\" checked></th><th><strong>Order No</strong></th><th><strong>Sales Order</strong></th></tr>"; //undefined variable
						while($row = mysqli_fetch_array($result))
						{
						$orderNoCount = $row['orderNoCount'];
						$strGraphicNo = $row['strGraphicNo'];
                        if ( !in_array($row['strGraphicNo'], $graphicList)){
                            array_push($graphicList, $row['strGraphicNo']);
                            $graphicHtml .= "<option value=\"".$row['strGraphicNo']."\">".$row['strGraphicNo'].""."</option>";
                        }

						$html .= "<tr id=\"".$row['so']."\"><td align=\"center\"><input name=\"chkSo\" id=\"chkSo\" class=\"chkSo\" type=\"checkbox\" checked></td><td id=\"".$row['so']."\" class=\"normalfnt order\" nowrap>".$row['intOrderNo']."/".$row['intOrderYear']."</td><td id=\"".$row['so']."\" class=\"normalfnt so\" nowrap>".$row['so_desc']."/".$row['part']."/".$row['strCombo']."</td></tr>";
						if($mrnNoArray[0] != '' && $mrnNoArray[1] != '' && $row['intOrderNo'] != $orderCheckDuplicate && $orderNoCount == 1){
						$html_1 .= "<option  selected=\"selected\" value=\"".$row['intOrderNo']."/".$row['intOrderYear']."\">".$row['intOrderNo']."/".$row['intOrderYear']."</option>";
						}else if ($orderNoArray[0] != '' && $orderNoArray[1] != '' && ($orderNoArray[0] != $orderCheckDuplicate)){
						$html_1 .= "<option  selected=\"selected\" value=\"".$orderNoArray[0]."/".$orderNoArray[1]."\">".$orderNoArray[0]."/".$orderNoArray[1]."</option>";
						$orderCheckDuplicate = $orderNoArray[0];
						}
						else if($row['intOrderNo'] != $orderCheckDuplicate){
						$html_1 .= "<option  value=\"".$row['intOrderNo']."/".$row['intOrderYear']."\">".$row['intOrderNo']."/".$row['intOrderYear']."</option>";
						}
                        $orderCheckDuplicate      = $row['intOrderNo'];
						//$response['GraphicNo']    = $strGraphicNo;
						}
						if($mrnNoArray[0] != '' && $mrnNoArray[1] != '') {
						
						$response['html']         = $html;
						}
						$response['html_1']       = $html_1;

						$response['html_G']       = $graphicHtml;


						echo json_encode($response);
						}
						
						
				/*else if($requestType=='loadMrnNo')
				{
							$orderNo       = $_REQUEST['orderNo'];
							$orderNoArray  = explode('/',$orderNo);
							$departmentId  = $_REQUEST['departmentId'];
							$mrnNo         = $_REQUEST['mrnNo'];
							$mrnNoArray    = explode('/',$mrnNo);
							$issueLocationId  = $_REQUEST['issueLocationId'];
							
							
							$sql =" select 
							intMrnNo,
							intMrnYear,
							intOrderNo,
							intOrderYear,
							strStyleNo
							from(
							SELECT   
							ware_mrnheader.intMrnNo,
							ware_mrnheader.intMrnYear,
							ware_mrndetails.intOrderNo,
							ware_mrndetails.intOrderYear,
							ware_mrndetails.strStyleNo,
							ware_mrndetails.intItemId,
							(ware_mrndetails.dblQty+(sum(ifnull(ware_issuedetails.dblReturnQty,0)))-ware_mrndetails.dblIssudQty) as balance 
							FROM
							ware_mrnheader
							INNER JOIN ware_mrndetails ON ware_mrnheader.intMrnNo = ware_mrndetails.intMrnNo
							AND ware_mrnheader.intMrnYear = ware_mrndetails.intMrnYear
							LEFT JOIN ware_issuedetails ON ware_mrndetails.intMrnNo = ware_issuedetails.intMrnNo
							AND ware_mrndetails.intMrnYear = ware_issuedetails.intMrnYear
							AND ware_mrndetails.intOrderNo = ware_issuedetails.strOrderNo
							AND ware_mrndetails.intOrderYear = ware_issuedetails.intOrderYear
							AND ware_mrndetails.strStyleNo = ware_issuedetails.strStyleNo
							AND ware_mrndetails.intItemId = ware_issuedetails.intItemId
							WHERE
							ware_mrnheader.intStatus         = '1'							
							AND ware_mrnheader.intDepartment ='$departmentId'
							AND ware_mrnheader.intCompanyId  = '$issueLocationId' 
							AND ware_mrndetails.intOrderNo   = '$orderNoArray[0]' 
							AND ware_mrndetails.intOrderYear = '$orderNoArray[1]' 
							AND ware_mrndetails.MRN_TYPE IN (3,6)";
							
							$sql .= "GROUP BY
							ware_mrnheader.intMrnNo,
							ware_mrnheader.intMrnYear , 
							ware_mrndetails.intOrderNo,
							ware_mrndetails.intOrderYear,
							ware_mrndetails.strStyleNo,
							ware_mrndetails.intItemId 
							having balance > 0
							
							) as tb1 
							group by intMrnYear,intMrnNo ORDER BY intMrnNo DESC,intMrnYear DESC";
							
							
							
							
							/* $sql ="SELECT DISTINCT
							ware_mrnheader.intMrnNo,
							ware_mrnheader.intMrnYear,
							ware_mrndetails.intOrderNo,
							ware_mrndetails.intOrderYear,
							ware_mrndetails.strStyleNo
							FROM ware_mrndetails 
							Inner Join ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
							left Join ware_issuedetails ON ware_mrndetails.intMrnNo = ware_issuedetails.intMrnNo AND ware_mrndetails.intMrnYear = ware_issuedetails.intMrnYear
							AND ware_mrndetails.intOrderNo = ware_issuedetails.strOrderNo AND ware_mrndetails.intOrderYear = ware_issuedetails.intOrderYear AND
							ware_mrndetails.strStyleNo = ware_issuedetails.strStyleNo AND ware_mrndetails.intItemId = ware_issuedetails.intItemId
							WHERE 
							ware_mrnheader.intStatus=1 AND 
							ware_mrnheader.intDepartment = '$departmentId' AND
							ware_mrnheader.intCompanyId =  '$issueLocationId' AND 
							ware_mrndetails.intOrderNo =  '$orderNoArray[0]' AND
							ware_mrndetails.intOrderYear =  '$orderNoArray[1]' AND 
							ware_mrndetails.strStyleNo =  '$styleNo' AND 
							((ware_mrndetails.dblQty + IFNULL(ware_issuedetails.dblReturnQty,'0')) - ware_mrndetails.dblIssudQty) > 0";
							
							
							$result = $db->RunQuery($sql);
							$html = "<option value=\"\"></option>";
							while($row=mysqli_fetch_array($result))
							{
								if($row['intMrnNo']==$mrnNoArray[0])
								{
								$html .= "<option  value=\"".$mrnNo."\" selected>".$mrnNoArray[0]."/".$mrnNoArray[1]."</option>";	
								}
								else
								{
							$html .= "<option value=\"".$row['intMrnNo']."/".$row['intMrnYear']."/".$row['intOrderNo']."/".$row['intOrderYear']."/".$row['strStyleNo']."\">".$row['intMrnNo']."/".$row['intMrnYear']."</option>";
								}
							}
							echo $html;
							$response['html'] = $html;
							echo  json_encode($response);
				}*/
				
				else if($requestType=='loadMrn')//load mrn for mrn type
				{
					 $mrnType  		 = $_REQUEST['mrnType'];//orderwise
					 $departmentId   = $_REQUEST['departmentId'];
					 $issueLocationId  = $_REQUEST['issueLocationId'];
						$sql = "select 
						intMrnNo,
						intMrnYear,
						intOrderNo,
						intOrderYear,
						strStyleNo
						from(
						SELECT   
						ware_mrnheader.intMrnNo,
						ware_mrnheader.intMrnYear,
						ware_mrnheader.mrnType,
						ware_mrndetails.intOrderNo,
						ware_mrndetails.intOrderYear,
						ware_mrndetails.strStyleNo,
						ware_mrndetails.intItemId,
						(ware_mrndetails.dblQty+(sum(ifnull(ware_issuedetails.dblReturnQty,0)))-ware_mrndetails.dblIssudQty) as balance 
						FROM
						ware_mrnheader
						INNER JOIN ware_mrndetails ON ware_mrnheader.intMrnNo = ware_mrndetails.intMrnNo
						AND ware_mrnheader.intMrnYear = ware_mrndetails.intMrnYear
						LEFT JOIN ware_issuedetails ON ware_mrndetails.intMrnNo = ware_issuedetails.intMrnNo
						AND ware_mrndetails.intMrnYear = ware_issuedetails.intMrnYear
						AND ware_mrndetails.intOrderNo = ware_issuedetails.strOrderNo
						AND ware_mrndetails.intOrderYear = ware_issuedetails.intOrderYear
						AND ware_mrndetails.strStyleNo = ware_issuedetails.strStyleNo
						AND ware_mrndetails.intItemId = ware_issuedetails.intItemId
						WHERE
						ware_mrnheader.intStatus = '1'
						AND ware_mrnheader.intDepartment = '$departmentId'
						AND ware_mrnheader.intCompanyId = '$issueLocationId'
						AND ware_mrndetails.MRN_TYPE IN (3,6)
						";
						
						$sql.=" 
						GROUP BY
						ware_mrnheader.intMrnNo,
						ware_mrnheader.intMrnYear , 
						ware_mrndetails.intOrderNo,
						ware_mrndetails.intOrderYear,
						ware_mrndetails.strStyleNo,
						ware_mrndetails.intItemId 
						having balance > 0
						
						) as tb1 
						group by intMrnYear,intMrnNo ORDER BY intMrnNo DESC,intMrnYear DESC";
						
						$result = $db->RunQuery($sql);
						$html = "<option value=\"\"></option>";
						while($row=mysqli_fetch_array($result))
						{
				        $html .= "<option value=\"".$row['intMrnNo']."/".$row['intMrnYear']."/".$row['intOrderNo']."/".$row['intOrderYear']."/".$row['strStyleNo']."\">".$row['intMrnNo']."/".$row['intMrnYear']."</option>";	
						}
						
							$response['html'] = $html;
							echo  json_encode($response);
						//echo json_encode($response);
				}
							
				else if($requestType=='loadItems')
				{
							$orderNo               = $_POST['orderNo'];
							$orderNoArray 	       = explode('/',$orderNo);
							$salesOrderId          = $_POST['salesOrderId'];
							$mrnNo                 = $_POST['mrnNo'];
							$mrnNoArray 	       = explode('/',$mrnNo);
                                                        $mainCategory          = $_POST['mainCategory'];
							$graphicNo             = $_POST['graphicNo'];
							$subCategory           = $_POST['subCategory'];
							$description           = $_POST['description'];
							$so_selected_flag      = $_POST['so_selected_flag'];
							$orderwiseSelected     = $_POST['orderwiseSelected'];
							
							
							$allocatedFlag         = 0; //$allocatedFlag=1;
							
							
							
							// echo "jfkh";
							/*if($styleNo=='null'){
							$styleNo='';
							}
							if($orderNoArray[0]==''){
							$orderNoArray[0]=0;
							$salesOrderId='';
							//$allocatedFlag=0;
							}
							if($orderNoArray[1]==''){
							$orderNoArray[1]=0;
							}*/
							
							$salesOrderNo  = $_POST['salesOrderNo'];
							$salesOrderNo_list='';
							$salesOrderNo_list1='';
							$salesOrderNo 		 = json_decode($_POST['salesOrderNo'], true);
							
							foreach($salesOrderNo as $arrVal)
							{
							if($salesOrderNo_list!=''){
							$salesOrderNo_list      .= ",'".$arrVal['so']."'";
							$salesOrderNo_list1      .= ",".$arrVal['so'];
							}
							else{
							$salesOrderNo_list      .= "'".$arrVal['so']."'";
							$salesOrderNo_list1      .= $arrVal['so'];
							}
							}
							
							
							if($mrnNo[0]==''||$mrnNoArray[1]==''){
							$orderNoArray[0]=0;
							$orderNoArray[1]=0;
							$mrnNoArray[0]=0;
							$mrnNoArray[1]=0;
							$salesOrderId='';
							$allocatedFlag=1;
							}

                            $arrCombo = array();
							
							$sql="select 
							intId,
							intMainCategory,
							intSubCategory,
							strCode,
                            SUP_ITEM_CODE,
							itemName,
                                                        item_hide,
							intBomItem,
							intUOM,
							dblLastPrice,
							mainCatName,
							subCatName,   
							intMrnNo,
							intMrnYear,
							mrnType,
							intOrderNo,
							intOrderYear,
							strSalesOrderNo,
							intSalesOrderId,
							strGraphicNo,
							sum(dblQty) as dblQty,
							sum(ifnull(dblIssudQty,0) ) as dblIssudQty, 
							uom,
							sum(dblReturnQty) as dblReturnQty 
							from (SELECT
							mst_item.intId,
							mst_item.intMainCategory,
							mst_item.intSubCategory,
							mst_item.strCode,
                            mst_item.strCode as SUP_ITEM_CODE,
							mst_item.strName as itemName,
							mst_item.intBomItem,
							mst_item.intUOM,
							mst_item.dblLastPrice,
                                                        mst_item.ITEM_HIDE as item_hide,
							mst_maincategory.strName as mainCatName,
							mst_subcategory.strName as subCatName,   
							ware_mrndetails.intMrnNo,
							ware_mrndetails.intMrnYear,
							ware_mrnheader.mrnType,
							ware_mrndetails.intOrderNo,
							ware_mrndetails.intOrderYear,
							trn_orderdetails.strSalesOrderNo,
							trn_orderdetails.intSalesOrderId,
							trn_orderdetails.strGraphicNo,
							sum(ware_mrndetails.dblQty) as dblQty,
							sum(ifnull(ware_mrndetails.dblIssudQty,0) ) as dblIssudQty, 
							mst_units.strCode as uom,
							(
							SELECT
							sum(ifnull(ware_issuedetails.dblReturnQty,0))
							FROM
							ware_issuedetails
							where ware_mrndetails.intMrnNo = ware_issuedetails.intMrnNo AND ware_mrndetails.intMrnYear = ware_issuedetails.intMrnYear
							AND ware_mrndetails.intOrderNo = ware_issuedetails.strOrderNo AND ware_mrndetails.intOrderYear = ware_issuedetails.intOrderYear AND
							ware_mrndetails.strStyleNo = ware_issuedetails.strStyleNo AND ware_mrndetails.intItemId = ware_issuedetails.intItemId";
							if($salesOrderNo_list!='')
							$sql .= " AND concat(ware_mrndetails.intOrderNo,'/',ware_mrndetails.intOrderYear,'/',ware_mrndetails.strStyleNo) IN  ($salesOrderNo_list)";
							
							$sql .= " ) AS dblReturnQty
							FROM
							ware_mrndetails 
							Inner Join ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
							left Join trn_orderdetails ON ware_mrndetails.intOrderNo = trn_orderdetails.intOrderNo AND ware_mrndetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_mrndetails.strStyleNo = trn_orderdetails.intSalesOrderId 
							Inner Join mst_item ON ware_mrndetails.intItemId = mst_item.intId
							Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
							Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
							Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
                                                        LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier =  
                                                        mst_supplier.intId
							WHERE 
							ware_mrnheader.intStatus = '1' AND 
							ware_mrndetails.intMrnNo =  '$mrnNoArray[0]' AND 
							ware_mrndetails.intMrnYear =  '$mrnNoArray[1]' AND
							mst_item.intStatus = '1'";
							
							if($salesOrderNo_list!='')
							$sql.=" AND concat(ware_mrndetails.intOrderNo,'/',ware_mrndetails.intOrderYear,'/',ware_mrndetails.strStyleNo) IN  ($salesOrderNo_list)";
							
							/*if($orderNoArray[1]!='')
							$sql.=" AND ware_mrndetails.intOrderYear =  '$orderNoArray[1]'";
							if($salesOrderId!='')
							$sql.=" AND ware_mrndetails.strStyleNo =  '$salesOrderId'";	*/
                            if($graphicNo!='' && $graphicNo != 'null')
                            $sql.=" AND trn_orderdetails.strGraphicNo =  '$graphicNo' ";
							if($mainCategory!='')
							$sql.=" AND mst_item.intMainCategory =  '$mainCategory' ";
							if($subCategory!='')
							$sql.=" AND mst_item.intSubCategory =  '$subCategory'";
							if($description!=''){
							$sql.=" AND
							(mst_item.strName LIKE  '%$description%' OR mst_item.strCode LIKE  '%$description%')";
							}
							$sql.=" group by ware_mrndetails.intOrderNo,ware_mrndetails.intOrderYear,ware_mrndetails.strStyleNo,						                mst_item.intId) as tb1 
							where ((tb1.dblQty + IFNULL(tb1.dblReturnQty,'0')) - tb1.dblIssudQty) > 0 
							GROUP BY tb1.intId";	 //tb1.dblQty-tb1.dblIssudQty>0  
							

							$result = $db->RunQuery($sql);
							
							while($row=mysqli_fetch_array($result))
							{
							$mrnQty = $row['dblQty'];
							
							if($allocatedFlag==0){
							$stockBalQty=getStockBalance_bulk($location,$row['intId']);
							//print_r("A1");
							}
							else{
							//$stockBalQty=getStockBalance($companyId,$location,$mrnNoArray[2],$mrnNoArray[3],$mrnNoArray[4],$row['intId']);
							$stockBalQty=getStockBalance_bulk($location,$row['intId']);
							//print_r("A2");
							}
							
							//to get pri qtys
							$salesOrderQty_list='';
							$salesOrderNo1 		 = json_decode($_POST['salesOrderNo'], true);
							$k=0;
							foreach($salesOrderNo1 as $arrVal1)
							{
							if($salesOrderQty_list!=''){
							$salesOrderQty_list      .= ",".getSoQty($arrVal1['so'],$row['intId']);
							}
							else{
							$salesOrderQty_list      .= getSoQty($arrVal1['so'],$row['intId']);
							}
							$k++;
							}
							//
							
							//to get bal to mrn qtys
							$salesOrderBalQty_list='';
							$salesOrderNo1 		 = json_decode($_POST['salesOrderNo'], true);
							$k=0;
							$totBalQty=0;
							$salesOedreArraySize = sizeof($salesOrderNo1);
							foreach($salesOrderNo1 as $arrVal1)
							{
							
							$balToIssueQty			= round(getBalToIssueQty($mrnNoArray[0],$mrnNoArray[1],$arrVal1['so'],$row['intId']),4);
							
							if($balToIssueQty<0)
							$balToIssueQty=0;
							if($salesOrderBalQty_list!=''){
							$salesOrderBalQty_list      .= ",".$balToIssueQty;
							}
							else{
							$salesOrderBalQty_list      .= $balToIssueQty;
							}
							$totBalQty +=$balToIssueQty;
							
							$k++;
							}
							
							//
							
							
							$data['salesOrderNo_list'] 	= $salesOrderNo_list1;
							$data['salesOrderPRIQty_list'] 	= $salesOrderQty_list;
							$data['salesOrderBalQty_list'] 	= $salesOrderBalQty_list;
							$data['intMrnNo'] 		= $row['intMrnNo'];
							$data['intMrnYear'] 	= $row['intMrnYear'];
							$data['mrnType'] 		= $row['mrnType'];
							$data['intOrderNo'] 	= $row['intOrderNo'];
							$data['intOrderYear'] 	= $row['intOrderYear'];
							$data['salesOrderNo'] 	= $row['strSalesOrderNo'];
							$data['salesOrderId'] 	= $row['intSalesOrderId'];
                                                        $data['strGraphicNo'] 	= $row['strGraphicNo'];
							$data['itemId'] 	= $row['intId'];
							$data['maincatId'] = $row['intMainCategory'];
							$data['subCatId'] 	= $row['intSubCategory'];
							$data['code'] = $row['strCode'];
                                                        $data['supItemCode'] = $row['SUP_ITEM_CODE'];
							$data['itemName'] = $row['itemName'];
                                                        $data['item_hide'] = $row['item_hide'];
							$data['uom'] 	= $row['uom'];
							$data['unitPrice'] = $row['dblLastPrice'];
							$data['mainCatName'] = $row['mainCatName'];
							$data['subCatName'] = $row['subCatName'];
							$data['mrnQty'] = round($mrnQty,4);
							$data['issueQty'] = round($row['dblIssudQty'],4);
							$data['stockBalQty'] = round($stockBalQty,4);
							$dblReturnQty = $row['dblReturnQty'];
							if(!$dblReturnQty)
							$dblReturnQty =0;
							$issueQty	=round($row['dblIssudQty'],4);
							$data['returnQty'] = round($dblReturnQty,4);
							$toIssueQty = $issueQty - $dblReturnQty;
							$data['toIssueQty'] = round($toIssueQty,4);
							$balToIssueQty = $mrnQty + $dblReturnQty - $issueQty;
							if($salesOedreArraySize == 0){
							$data['balToIssueQty'] = getRoundedWeight($balToIssueQty,$row['intId']);
							}else{
							$data['balToIssueQty'] = getRoundedWeight($totBalQty,$row['intId']);
							}
							//$data['balToIssueQty'] = getRoundedWeight($balToIssueQty,$row['intId']);
							
							/*if($data['intOrderNo']=''){
							$data['ordeNoFlag'] 	= "None";
							}
							else{
							$data['ordeNoFlag'] 	= "Order wise";
							}*/


							array_push($arrCombo, $data);

							}


							$response['arrCombo'] 	= $arrCombo;
							echo  json_encode($response);
							
				}
							
							//-----------------------------------------------------------
function getStockBalance($company,$location,$orderNo,$orderYear,$salesOrderId,$item)
{
global $db;
$sql = "SELECT
Sum(ware_stocktransactions.dblQty) as stockBal 
FROM ware_stocktransactions
WHERE
ware_stocktransactions.intCompanyId =  '$company' AND
ware_stocktransactions.intLocationId =  '$location' AND
ware_stocktransactions.intOrderNo =  '$orderNo' AND
ware_stocktransactions.intOrderYear =  '$orderYear' AND
ware_stocktransactions.intSalesOrderId =  '$salesOrderId' AND
ware_stocktransactions.intItemId =  '$item'";



$result = $db->RunQuery($sql);
$row = mysqli_fetch_array($result);
return val($row['stockBal']);	
}
//--------------------------------------------------------------
function getStockBalance_bulk($location,$item)
{
global $db;
$sql = "SELECT
Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
FROM ware_stocktransactions_bulk
WHERE
ware_stocktransactions_bulk.intItemId =  '$item' AND
ware_stocktransactions_bulk.intLocationId =  '$location'
GROUP BY
ware_stocktransactions_bulk.intItemId";

$result = $db->RunQuery($sql);
$row = mysqli_fetch_array($result);
return val($row['stockBal']);	
}
//--------------------------------------------------------------
function geTotConfirmedIssueQtys($mrnNo,$mrnYear,$orderNo,$orderYear,$salesOrderId,$item){
global $db;
$sql="SELECT
Sum(ware_issuedetails.dblQty) as issuedQty 
FROM
ware_issuedetails
Inner Join ware_issueheader ON ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo AND ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear
WHERE
ware_issueheader.intStatus >  0 AND
ware_issueheader.intApproveLevels >=  ware_issueheader.intStatus AND
ware_issuedetails.intMrnNo =  '$mrnNo' AND
ware_issuedetails.intMrnYear =  '$mrnYear' AND
ware_issuedetails.intItemId =  '$item' ";

if($orderNo!='')
$sql.=" AND ware_issuedetails.strOrderNo =  '$orderNo'";
if($orderYear!='')
$sql.=" AND ware_issuedetails.intOrderYear =  '$orderYear'";
if($salesOrderId!='')
$sql.=" AND ware_issuedetails.strStyleNo =  '$salesOrderId'";		


$sql.="GROUP BY
ware_issuedetails.intItemId,
ware_issuedetails.strStyleNo,
ware_issuedetails.intOrderYear,
ware_issuedetails.strOrderNo,
ware_issuedetails.intMrnYear,
ware_issuedetails.intMrnNo,
ware_issueheader.intStatus,
ware_issueheader.intApproveLevels
";


$results = $db->RunQuery($sql);
$row = mysqli_fetch_array($results);
$issuedQty = val($row['issuedQty']);
return $issuedQty;
}

//---------------------------------------------------
function getSoQty($so,$item){
global $db;
$sql = "SELECT
trn_po_prn_details_sales_order.REQUIRED
FROM `trn_po_prn_details_sales_order`
WHERE
concat(ORDER_NO,'/',ORDER_YEAR,'/',SALES_ORDER) = '$so' AND
trn_po_prn_details_sales_order.ITEM = '$item'
GROUP BY
trn_po_prn_details_sales_order.ORDER_NO,
trn_po_prn_details_sales_order.ORDER_YEAR,
trn_po_prn_details_sales_order.SALES_ORDER,
trn_po_prn_details_sales_order.ITEM

";
$result = $db->RunQuery($sql);
$row = mysqli_fetch_array($result);
//$actualPriQty_s	= getRoundedWeight($row['REQUIRED'],$item);
$actualPriQty_s	= $row['REQUIRED'];
if($row['REQUIRED']=='')
return 0;
else
return $actualPriQty_s;
}
//------------------------------
function getBalToIssueQty($mrnNo,$mrnYear,$so,$item){
global $db;
$sql="select * from (SELECT
sum(ifnull(ware_mrndetails.dblQty,0)) as dblQty,
sum(ifnull(ware_mrndetails.dblIssudQty,0) ) as dblIssudQty, 
mst_units.strCode as uom,
ifnull((
SELECT
sum(ifnull(ware_issuedetails.dblReturnQty,0))
FROM
ware_issuedetails
where ware_mrndetails.intMrnNo = ware_issuedetails.intMrnNo AND ware_mrndetails.intMrnYear = ware_issuedetails.intMrnYear
AND ware_mrndetails.intOrderNo = ware_issuedetails.strOrderNo AND ware_mrndetails.intOrderYear = ware_issuedetails.intOrderYear AND
ware_mrndetails.strStyleNo = ware_issuedetails.strStyleNo AND ware_mrndetails.intItemId = ware_issuedetails.intItemId
),0) AS dblReturnQty
FROM
ware_mrndetails 
Inner Join ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
left Join trn_orderdetails ON ware_mrndetails.intOrderNo = trn_orderdetails.intOrderNo AND ware_mrndetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_mrndetails.strStyleNo = trn_orderdetails.intSalesOrderId 
Inner Join mst_item ON ware_mrndetails.intItemId = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
WHERE 
ware_mrnheader.intStatus = '1' AND 
ware_mrndetails.intMrnNo =  '$mrnNo' AND 
ware_mrndetails.intMrnYear =  '$mrnYear' AND  ";
if($so){
$sql .= " concat(ware_mrndetails.intOrderNo,'/',ware_mrndetails.intOrderYear,'/',ware_mrndetails.strStyleNo) =  '$so' AND ";
}
$sql .= " ware_mrndetails.intItemId =  '$item' AND ";
$sql .=" mst_item.intStatus = '1' ) as tb ";
$results = $db->RunQuery($sql);
$row = mysqli_fetch_array($results);
$issuedQty = val($row['dblQty']+$row['dblReturnQty']-$row['dblIssudQty']);
return $issuedQty;
}

function getRoundedWeight($qty, $item){
global $db;

$sql = "SELECT
mst_item.strName,
mst_item.intUOM 
FROM `mst_item`
WHERE
mst_item.intId = '$item'
";
$result = $db->RunQuery($sql);
$row = mysqli_fetch_array($result);
$uom	= ($row['intUOM']);

if($uom ==6 && floor($qty)==0){//only for weight < 1kg

if($qty<0.005 && $qty>=0.0001)
$qty=0.005;
//$y	=((round($qty,3)*1000)%100);
$y	= ((floor($qty * 1000) / 1000)*1000)%100;
$z	=ceil($y/10)*10;
$a	=$z-$y;

if($a > 5)
$val1=0;
else if($a>0)
$val1=5;
else
$val1	=0;

$val2 	=  floor((floor($qty * 1000) / 1000)*100)/100;
$val	= $val2+$val1/1000;

}
else
$val	=$qty;
//echo $qty.'/'.$y.'/'.$z.'/'.$a.'/'.$val1.'/'.$val2.'/'.$val.'</br>';
return $val;
}

//-------------------------------------------------------------------------
?>
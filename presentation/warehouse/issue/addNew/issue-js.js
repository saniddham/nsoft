						//////////////////////////////////////////////
						//Create By:H.B.G Korala
						/////////////////////////////////////////////
						var basePath	= "presentation/warehouse/issue/addNew/";
						var reportId	= "891";
						var so_selected_flag = 0;
						var orderwiseSelected = 0;
						
						$(document).ready(function() {
						
						$("#frmIssue").validationEngine();
						$('#frmIssue #cboDepartment').focus();

						$('#frmIssue #radio1,#frmIssue #radio2').on('change', function () {
							if(this.id == 'radio2'){
                                document.getElementById('supplier_tr').style.display = 'block';
							}
						});

						
						$("#frmIssue #butAddItems").click(function(){
						if(!$('#frmIssue #cboDepartment').val()){
						alert("Please select the department");
						return false;				
						}
			if(!$('#frmIssue #IssueLocation').val()){
				alert("Please select the Issue Location");
				return false;				
			}
						//clearRows();
						closePopUp();
						loadPopup();
						});
						
						//--------------------------------------------
						
						//-------------------------------------------- 
						$('#frmIssue #cboCurrency').change(function(){
						var currency = $('#cboCurrency').val();
						var url 		= basePath+"issue-db-get.php?requestType=loadExchangeRate";
						var httpobj = $.ajax({
						url:url,
						dataType:'json',
						data:"currency="+currency,
						async:false,
						success:function(json){
						
						document.getElementById("divExchRate").innerHTML=json.excRate;
						}
						});
						
						});
						//-------------------------------------------------------
						$('#frmIssue #cboSupplier').change(function(){
						var supplier = $('#cboSupplier').val();
						var url 		= basePath+"issue-db-get.php?requestType=loadSupplierDetails";
						var httpobj = $.ajax({
						url:url,
						dataType:'json',
						data:"supplier="+supplier,
						async:false,
						success:function(json){
						
						$('#frmIssue #cboCurrency').val(json.currency);
						$('#frmIssue #cboPayTerm').val(json.payTerm);
						$('#frmIssue #cboPayMode').val(json.payMode);
						$('#frmIssue #cboShipmentMode').val(json.shipmentMode);
						document.getElementById("divExchRate").innerHTML=json.excRate;
						}
						});
						
						});
						//-------------------------------------------------------
						
						$('#frmIssue #butSave').click(function(){
						var requestType = '';
						if ($('#frmIssue').validationEngine('validate'))   
						{ 
						showWaiting();
						var data = "requestType=save";
						
						data+="&serialNo="		+	$('#txtIssueNo').val();
						data+="&Year="	+	$('#txtIssueYear').val();
						data+="&department="	+	$('#cboDepartment').val();
						data+="&date="			+	$('#dtDate').val();
						data+="&remarks="	+	$('#txtRemarks').val();
			            data+="&IssueLocation="	+	$('#IssueLocation').val();
			            data+="&supplier=" + $('#cboSupplier').val();
						
						var rowCount = document.getElementById('tblIssueItems').rows.length;
						if(rowCount==1){
						alert("Items not selected to ISSUE");hideWaiting();
						return false;				
						}
						var row = 0;
						var errorQtyFlag=0;
						
						var arr="[";
						
						$('#tblIssueItems .item').each(function(){
						
						var orderNo	        = $(this).parent().find(".orderNo").html();
						var salesOrderId	= $(this).parent().find(".salesOrderNo").attr('id');
						var mrnNo	        = $(this).parent().find(".mrnNo").html();
						var itemId	        = $(this).attr('id');
						var Qty	            = $(this).parent().find(".Qty").val();
						
						
						if(Qty>0){
						arr += "{";
						row++;
						arr += '"orderNo":"'+	orderNo +'",' ;
						arr += '"salesOrderId":"'+		salesOrderId +'",' ;
						arr += '"mrnNo":"'+		mrnNo +'",' ;
						arr += '"itemId":"'+	itemId +'",' ;
						arr += '"Qty":"'+		Qty +'"' ;
						arr +=  '},';
						
						}
						if(Qty<=0){
						errorQtyFlag=1;
						}
						});
						if(rowCount<=1){
						alert("No selected items to issue");hideWaiting();
						return false;
						}
						else if(errorQtyFlag==1){
						alert("Please Enter Quantities");hideWaiting();
						return false;
						}
						
						
						arr = arr.substr(0,arr.length-1);
						arr += " ]";
						
						data+="&arr="	+	arr;
						
						///////////////////////////// save main infomations /////////////////////////////////////////
						var url = basePath+"issue-db-set.php";
						var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
						//data:'{"requestType":"addsampleInfomations"}',
						async:false,
						
						success:function(json){
						$('#frmIssue #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
						{
						var t=setTimeout("alertx()",1000);
						$('#txtIssueNo').val(json.serialNo);
						$('#txtIssueYear').val(json.year);
						if(json.confirmationMode==1){
						$('#butConfirm').show();
						}
						else{
						$('#butConfirm').hide();
						}
						return;
						}
						},
						error:function(xhr,status){
						
						$('#frmIssue #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertx()",3000);
						}		
						});
						hideWaiting();
						}
						});
						
						
						//--------------refresh the form----------
						$('#frmIssue #butNew').click(function(){
						window.location.href = "?q=231";
						$('#frmIssue').get(0).reset();
						clearRows();
						$('#frmIssue #txtGrnNo').val('');
						$('#frmIssue #txtGrnYear').val('');
						$('#frmIssue #txtInvioceNo').val('');
						$('#frmIssue #cboPO').val('');
						$('#frmIssue #txtDeliveryNo').val('');
						$('#frmIssue #txtTotGrnAmmount').val('');
						var currentTime = new Date();
						var month = currentTime.getMonth()+1 ;
						var day = currentTime.getDate();
						var year = currentTime.getFullYear();
						if(day<10)
						day='0'+day;
						if(month<10)
						month='0'+month;
						d=year+'-'+month+'-'+day;
						
						$('#frmIssue #dtDate').val(d);
						});
						//----------------------------	
						$('#frmIssue .delImg').click(function(){
						$(this).parent().parent().remove();
						});
						
						//----------------------------	
						//-----------------------------------
						$('#frmIssue #butReport').click(function(){
						if($('#txtIssueNo').val()!=''){
						window.open('?q='+reportId+'&issueNo='+$('#txtIssueNo').val()+'&year='+$('#txtIssueYear').val());	
						}
						else{
						alert("There is no Issue No to view");
						}
						});
						//----------------------------------	
						$('#frmIssue #butConfirm').click(function(){
						if($('#txtIssueNo').val()!=''){
						window.open('?q='+reportId+'&issueNo='+$('#txtIssueNo').val()+'&year='+$('#txtIssueYear').val()+'&approveMode=1');	
						}
						else{
						alert("There is no Issue No to confirm");
						}
						});
						//-----------------------------------------------------
						$('#frmIssue #butClose').click(function(){
						//load('main.php');	
						});
						//-----------------------------------------------------
						
						$('#frmIssue #cboDepartment').change(function(){
						$("#tblIssueItems tr:gt(0)").remove();
						//$('#tblIssueItems tbody>tr:not(.gridHeader)').remove();
						});
						});
						
						//------------------------------------------
						//-----------------------------------
						//----------end of ready -------------------------------
						
						//-------------------------------------------------------
						function loadPopup() //==============================
						{
						closePopUp();
						popupWindow3('1');
						var departmentId = $('#cboDepartment').val();  
						var rowCount     = $('#tblIssueItems tr').length;
						var issueLocationId = $('#IssueLocation').val();
						if (rowCount==1)
						var flag_order = 0;
						
						if (rowCount >1)
						{
						var orderNo    = $("#tblIssueItems tr").eq(1).find('.orderNo').attr('id');
						if (orderNo != '')
						var flag_order = 1;
						else
						var flag_order = 2; 
						}
						
						
												$('#popupContact1').load(basePath+'issuePopup.php?departmentId='+departmentId+'&issueLocation='+issueLocationId+'&flag_order='+flag_order+'&type='+$('input[name=type]:checked', '#frmIssue').val(),function(){
						//checkAlreadySelected();
						$('#frmIssuePopup #butAdd').click(addClickedRows);
						$('#frmIssuePopup #butClose1').click(disablePopup);
						
						//-------------------------------------------- 
						/* $('#frmIssuePopup #cboOrderNo').change(function(){
						var orderNo = $('#cboOrderNo').val();
						var url 		= basePath+"issue-db-get.php?requestType=loadStyleNo&orderNo="+orderNo;
						var httpobj 	= $.ajax({url:url,async:false})
						document.getElementById('cbStyleNo').innerHTML=httpobj.responseText;
						
						});*/
						//-------------------------------------------- 
						$('#frmIssuePopup #cboOrderNo').change(function()//orderwise mrn loading
						{
						
 						var orderNo 	= $('#cboOrderNo').val();
						var mrnNo 		= $('#cboMRN').val();
						$("#tblItemsPopup tbody tr").remove();
						if (mrnNo!="")
						{
						  var url 		=  basePath+"issue-db-get.php?requestType=loadSOGrid&orderNo="+orderNo+"&mrnNo="+mrnNo;
						  var httpobj 	= $.ajax({url:url,dataType: "json", async:false,success:function(json)
						  {
						    //document.getElementById('cboOrderNo').innerHTML 	= json.html_1;
						    document.getElementById('salesOrderTbl').innerHTML 	= json.html;
						    //suvini
						  }
						  })
						}
						
						
						});
						//-------------------------------------------- 
						$('#frmIssuePopup #cboMainCategory').change(function(){
						var mainCategory = $('#cboMainCategory').val();
						var url 		= basePath+"issue-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
						var httpobj 	= $.ajax({url:url,async:false})
						document.getElementById('cboSubCategory').innerHTML=httpobj.responseText;
						
						
						});
						
						$('#frmIssuePopup #cboMRN').change(function()//load sales order table
						{
						var orderNo 	= $('#cboOrderNo').val();
						var mrnNo 		= $('#cboMRN').val();
						$("#tblItemsPopup tbody tr").remove();
						if (mrnNo!="")
						{
						var url 		= basePath+"issue-db-get.php?requestType=loadSOGrid&mrnNo="+mrnNo;
						  var httpobj 	= $.ajax({url:url,dataType: "json", async:false,success:function(json)
						  {
						    document.getElementById('cboOrderNo').innerHTML 	= json.html_1;
						    document.getElementById('salesOrderTbl').innerHTML 	= json.html;
						    document.getElementById('cboGraphicNo').innerHTML 	= json.html_G;
						    //suvini
						  }
						  })
						}
						else if(mrnNo=="")
						{
						  $("#salesOrderTbl").html("");//clear saleorder table	
						  var mrnType       	= $('#cboMRNType').val();
						  var url 			    = basePath+"issue-db-get.php?requestType=loadOrderANDmrncombo&mrnType="+mrnType+"&departmentId="+departmentId;
						  var httpobj 		    = $.ajax({url:url,dataType: "json", async:false,success:function(json)
						  {
						   if(mrnType==1)
						     {
						      $('#order_row').show();
						      $('#cboOrderNo').html(json.response);
						     }
						  else if(mrnType==2)
						    { 
						     $('#order_row').hide();
						     $('#cboMRN').html(json.response)
						    }
						  }
						  })
						}
						});
						
						
						
						$('#frmIssuePopup #cboMRNType').change(function() //suvini
						{
						$("#tblItemsPopup tbody tr").remove(); //remove grid rows
						$('#cboMRN').html("");//clear mrn row
						$("#salesOrderTbl").html("");//clear saleorder table
							
						var mrnType       	= $('#cboMRNType').val();
						var issueLocationId  =	$('#IssueLocation').val();
						var url 			= basePath+"issue-db-get.php?requestType=loadOrderANDmrncombo&mrnType="+mrnType+"&departmentId="+departmentId+"&issueLocationId="+issueLocationId;
						var httpobj 		= $.ajax({url:url,dataType: "json", async:false,success:function(json)
						{
							if(mrnType==1)//orderwise
						 	{
						  		$('#order_row').show();
						   		//loadmrn();
						   		$('#cboOrderNo').html(json.response);
								//loadmrn();
						   
						  	}
							else if(mrnType==2)//nonorderwise
						  	{ 
						  		 $('#order_row').hide();
						   		$('#cboMRN').html(json.response)
						  	}
						 }
						});
						if(mrnType==1)
						{
						loadmrn();
						}
						
						});
						//-------------------------------------------- 
						$('#frmIssuePopup .chkSoAll').die('click').live('click',function(){
						if(document.getElementById('chkSoAll').checked==true)
						var chk=true;
						else
						var chk=false;
						var rowCount = document.getElementById('salesOrderTbl').rows.length;
						for(var i=1;i<rowCount;i++)
						{
						//alert(document.getElementById('salesOrderTbl').rows[i].cells[0].childNodes[0].html());
						document.getElementById('salesOrderTbl').rows[i].cells[0].childNodes[0].checked=chk;
						}
						});
						//-----------------------------------------------------
						$('#frmIssuePopup  #chkAll').click(function(){
						if(document.getElementById('chkAll').checked==true)
						var chk=true;
						else
						var chk=false;
						
						var rowCount = document.getElementById('tblItemsPopup').rows.length;
						for(var i=1;i<rowCount;i++)
						{
						document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked=chk;
						}
						});
						//-------------------------------------------- 
						$('#frmIssuePopup #chkSo').die('click').live('click',function(){
						$('#frmIssuePopup #imgSearchItems').click();
						});
						$('#frmIssuePopup #chkSoAll').die('click').live('click',function(){
						$('#frmIssuePopup #imgSearchItems').click();
						});
						
						
						$('#frmIssuePopup #imgSearchItems').click(function(){
						
						
						var rowCount = document.getElementById('tblItemsPopup').rows.length;
						for(var i=1;i<rowCount;i++)
						{
						document.getElementById('tblItemsPopup').deleteRow(1);
						}
						var orderNo      = $('#cboOrderNo').val();
						var salesOrderId = $('#cbStyleNo').val();
						
						//var salesOrderNo = $('#cbStyleNo option:selected').text();
						var mrnNo         = $('#cboMRN').val();
						var mainCategory  = $('#cboMainCategory').val();
						var subCategory   = $('#cboSubCategory').val();
						var description   = $('#txtItmDesc').val();
						var mrnType       = $('#cboMRNType').val();
						var graphicNo     = $('#cboGraphicNo').val();
						//alert(graphicNo)

						
						//so list
						var arr="[";
						var i=0;
						var salesOrderId_list	='';
						var salesOrderNo_list	='';
						so_selected_flag =0;
						orderwiseSelected = 0;
						$('#salesOrderTbl .chkSo').each(function(){
						orderwiseSelected = 1;
						
						i++;
						var salesOrderId = 	$(this).parent().parent().find('.so').attr('id');
						var salesOrderNo = 	$(this).parent().parent().find('.so').html();
						
						if(document.getElementById('salesOrderTbl').rows[i].cells[0].childNodes[0].checked==true){
						so_selected_flag = 1;
						
						arr += "{";
						arr += '"so":"'+		salesOrderId  +'"' ;
						arr +=  '},';
						if(salesOrderId_list==''){
						salesOrderId_list += salesOrderId;
						salesOrderNo_list += salesOrderNo;
						}
						else {
						salesOrderId_list += ','+salesOrderId;
						salesOrderNo_list += ','+salesOrderNo;
						}
						}
						});
						//alert(salesOrderNo_list);
						arr = arr.substr(0,arr.length-1);
						arr += " ]";
						//end of so list
						
						if(mrnNo==''){
						alert("Please select MRN No");
						return false;
						}	
						if(so_selected_flag == 0 && orderwiseSelected == 1 ){
						alert("Please select at least one sales order");
						return false;
						}
						showWaiting();
						var url 		= basePath+"issue-db-get.php?requestType=loadItems";
						var httpobj = $.ajax({
						url:url,
						dataType:'json',
						type:'POST',
						data:"orderNo="+orderNo+"&salesOrderId="+salesOrderId+"&mrnNo="+mrnNo+"&graphicNo="+graphicNo+"&mainCategory="+mainCategory+"&subCategory="+subCategory+"&description="+description+"&salesOrderNo="+arr+"&so_selected_flag="+so_selected_flag+"&orderwiseSelected="+orderwiseSelected,

						async:false,
						
						success:function(json)
						{
						 if(mrnType==1  && orderwiseSelected==0 && so_selected_flag==0 && !json.arrCombo)
						 {   hideWaiting();
							 alert("order wise MRN should have at least one sales order number")
						 }
						else if(mrnType==1  && orderwiseSelected==1 && so_selected_flag==1 &&!json.arrCombo)
						 {
							hideWaiting();
							alert('No Balance To Issue');
						 }
						 if(!json.arrCombo)
						 {
							hideWaiting();
							alert('No Balance To Issue');
						 }
						
						
						var length = json.arrCombo.length;
						var arrCombo = json.arrCombo;
						
						for(var i=0;i<length;i++)
						{
						var intMrnNo=arrCombo[i]['intMrnNo'];	
						var intMrnYear=arrCombo[i]['intMrnYear'];	
						var intOrderNo=arrCombo[i]['intOrderNo'];
						var intOrderYear=arrCombo[i]['intOrderYear'];
						if((intOrderNo==0) && (intOrderYear==0)){
						var orderNo='';
						}
						else{
						var orderNo=intOrderNo+"/"+intOrderYear;
						}
						var salesOrderNo=arrCombo[i]['salesOrderNo'];	
						var salesOrderId=arrCombo[i]['salesOrderId'];
						var graphicNo  = arrCombo[i]['strGraphicNo'];

						if(!salesOrderNo){
						salesOrderNo = '';
						}
						if(!salesOrderId){
						salesOrderId = '';
						}
						
						//var salesOrderNo_list=arrCombo[i]['salesOrderNo_list'];	
						var salesOrderPRIQty_list=arrCombo[i]['salesOrderPRIQty_list'];	
						var salesOrderBalQty_list=arrCombo[i]['salesOrderBalQty_list'];	
						
						var mrnType=arrCombo[i]['mrnType'];	
						var itemId=arrCombo[i]['itemId'];	
						var maincatId=arrCombo[i]['maincatId'];	
						var subCatId=arrCombo[i]['subCatId'];	
						var code=arrCombo[i]['code'];
                                                var supItemCode=arrCombo[i]['supItemCode'];
						var itemName=arrCombo[i]['itemName'];
                                                var ITEM_HIDE=arrCombo[i]['item_hide'];
						var uom=arrCombo[i]['uom'];	
						var unitPrice=arrCombo[i]['unitPrice'];	
						var mainCatName=arrCombo[i]['mainCatName'];
						var subCatName=arrCombo[i]['subCatName'];	
						var mrnQty=arrCombo[i]['mrnQty'];
						var issueQty=arrCombo[i]['issueQty'];
						var returnQty=arrCombo[i]['returnQty'];
						//var issuedQty=parseFloat(issueQty)-parseFloat(returnQty);
						var issuedQty=arrCombo[i]['toIssueQty'];
						//var balToIssueQty=(parseFloat(mrnQty)+parseFloat(returnQty))-parseFloat(issueQty);
						var balToIssueQty=arrCombo[i]['balToIssueQty'];
						var stockBalQty=arrCombo[i]['stockBalQty'];
						var maxIssuable =0;
						if(balToIssueQty>stockBalQty) 
						maxIssuable=stockBalQty;
						else
						maxIssuable=balToIssueQty;
						/*if(arrCombo[i]['ordeNoFlag']=="None");
						{
						orderNo="None";	
						salesOrderNo="None";	
						}*/
						if(maxIssuable>0)
						var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" id=""><input type="checkbox" id="chkDisp" checked /></td>';
						else
						var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" id=""><input type="checkbox" id="chkDisp"/></td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+mrnType+'" style="display:none"  class="mrnType">'+mrnType+'</td>';
                            content +='<td align="center" bgcolor="#FFFFFF" id="'+graphicNo+'" style="display:none"  class="graphicNoP">'+graphicNo+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderId_list+'" style="display:none"  class="salesOrderNoP">'+salesOrderNo_list+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderBalQty_list+'" style="display:none"  class="productionQtyP">'+salesOrderPRIQty_list+'</td>';


						content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatNameP">'+mainCatName+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatNameP">'+subCatName+'</td>';
						if(supItemCode!=null)
                                                {
                                                content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="codeeP">'+supItemCode+'</td>';   
                                                }else{ 
                                                content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="codeeP">'+code+'</td>';
                                                    }
                                                if(ITEM_HIDE==1)
                                                {
                                                content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="itemP">*****</div></td>';    
                                                }else{
						content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="itemP">'+itemName+'</div></td>';
                                                }
						content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uomP">'+uom+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+intMrnNo+"/"+intMrnYear+'" style="display:none"  class="mrnNoP">'+intMrnNo+"/"+intMrnYear+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+mrnQty+'"  class="mrnQtyP">'+mrnQty+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+issuedQty+'" class="issueQtyP" >'+issuedQty+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+balToIssueQty+'"  class="balToIssueQtyP">'+balToIssueQty+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+stockBalQty+'"  class="stockBalQtyP" >'+stockBalQty+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="" class="">'+'<input  id="form-validation-field-0" class="validate[required,custom[number],max['+maxIssuable+']] calculateValue totQtyP" style="width:80px;text-align:center" value="'+maxIssuable+'" type="text"></td>';
						add_new_row('#frmIssuePopup #tblItemsPopup',content);
						}
						checkAlreadySelected();
						
						}
						});hideWaiting();
						
						});
						//-------------------------------------------------------
						});	
						}//-------------------------------------------------------
						//----------------------------------------------------
						function addClickedRows()
						{
						//var rowCount = $('#tblDispatchPopup >tr').length;
						var rowCount = document.getElementById('tblItemsPopup').rows.length;
						
						if(so_selected_flag==0 && $('#cboOrderNo').val() >0 ){
						alert("Please select at least one sales order");
						return false;
						}
						
						var i=0;
						$('#tblItemsPopup .itemP').each(function(){
						i++;
						if((document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked==true) && (document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled==false)){
						
						var intMrnNo=$(this).parent().find(".mrnNoP").attr('id');
						/*var intOrderNo=$(this).parent().find(".orderNoP").attr('id');
						var salesOrderNo=$(this).parent().find(".salesOrderNoP").html();
						var salesOrderId=$(this).parent().find(".salesOrderNoP").attr('id');*/
						var salesOrderNo=$(this).parent().find(".salesOrderNoP").html();
						var salesOrderId=$(this).parent().find(".salesOrderNoP").attr('id');
						var PRI_initial_list=$(this).parent().find(".productionQtyP").html();//initial pri qtys
						var actualBalQty=$(this).parent().find(".productionQtyP").attr('id');//bal to mrn
						
						var itemId=$(this).attr('id');
						var maincatId=$(this).parent().find(".mainCatNameP").attr('id');
						var subCatId=$(this).parent().find(".subCatNameP").attr('id');
						var code=$(this).parent().find(".codeP").html();
						var uom=$(this).parent().find(".uomP").html();
						var mrnType=$(this).parent().find(".mrnType").html();
						var graphicNO=$(this).parent().find(".graphicNoP").html();
						if(graphicNO=="null")
                            graphicNO = "";
						var itemName=$(this).parent().find(".itemP").html();
						var mainCatName=$(this).parent().find(".mainCatNameP").html();
						var subCatName=$(this).parent().find(".subCatNameP").html();
						var mrnQty=parseFloat($(this).parent().find(".mrnQtyP").html());
						var issueQty=parseFloat($(this).parent().find(".issueQtyP").html());
						var stockBalance=parseFloat($(this).parent().find(".stockBalQtyP").html());
						var balToIssueQty=parseFloat($(this).parent().find(".balToIssueQtyP").html());
						var qty=$(this).parent().find(".totQtyP").val();// 
						
						var arr_so = salesOrderId.split(",");
						var arr_so_no = salesOrderNo.split(",");
						
						var arr_pri = PRI_initial_list.split(",");
						var arr_bal = actualBalQty.split(",");
						
						var index =0;
						var sum   =0;
						var sum_act_bal	=0;
						for (index = 0; index < arr_pri.length; ++index) {
						//alert(arr_bal[index]);
						sum+=parseFloat(arr_bal[index]);
						sum_act_bal+=parseFloat(arr_bal[index]);
						}
						var index =0;
						var actualQty_prapo		= 0;
						var actualExQty_prapo	= 0;
						var actualQtyBal		= 0;
						var pri					= 0;
						if(sum>0){
						for (index = 0; index < arr_pri.length; ++index) {
						//alert(arr_pri[index]+'/'+sum+'/'+extra)
						//alert(arr_so[0]!='' && arr_so[0]!='//');
						//if(arr_so[0]>0){
						//alert(1111);
						
						if(sum==0){
						actualQty_prapo		= (parseFloat(qty)/arr_pri.length).toFixed(4);
						}
						else{
						actualQty_prapo		= (parseFloat(arr_bal[index])/sum*parseFloat(qty)).toFixed(4);
						}
						
						//actualQtyBal	= (parseFloat(arr_bal[index])).toFixed(4);
						pri				= arr_pri[index];
						//alert(arr_pri[index]+'/'+sum+'*'+qty+'*'+extra+'_'+actualQtyBal+'___'+pri);
						//}
						//var balToIssueQty= parseFloat(mrnQty-issueQty);
						//balToIssueQty = balToIssueQty.toFixed(4);
						
						/*if(stockBalance<balToIssueQty){
						var maxMrnQty=stockBalance.toFixed(4);
						}
						else{
						var maxMrnQty=balToIssueQty;
						}*/
						//	alert(arr_so_no[index]);
						var salesOrder_1 = arr_so[index];
						var arr_so_1 = salesOrder_1.split("/");
						
						var salesOrderNo_1 = arr_so_no[index];
						//	var arr_so_no_1 = salesOrderNo_1.split("/");
						
						
						
						var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="images/del.png" width="15" height="15" /></td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+arr_so_1[0]+'/'+arr_so_1[1]+'" class="orderNo">'+arr_so_1[0]+'/'+arr_so_1[1]+'</td>';

						content +='<td align="center" bgcolor="#FFFFFF" id="'+arr_so_1[2]+'" class="salesOrderNo">'+salesOrderNo_1+'</td>';
                            content +='<td align="center" bgcolor="#FFFFFF" id="'+graphicNO+'" class="graphicNo">'+graphicNO+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+intMrnNo+'" class="mrnNo">'+intMrnNo+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatName">'+mainCatName+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatName">'+subCatName+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="item">'+itemName+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uom">'+uom+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+arr_bal[index]+'" class="balToIssueQty">'+arr_bal[index]+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="" class="orderNo"><input  id="'+arr_bal[index]+'" class="validate[required,custom[number],max['+arr_bal[index]+']] calculateValue Qty" style="width:80px;text-align:center" type="text" value="'+parseFloat(actualQty_prapo)+'"/></td>';
						content +='</tr>';
						
						
						//	content +='<td align="center" bgcolor="#FFFFFF" id="" class=""><input style="width:80px;text-align:center" type="text" value="0"/></td>';
						if((arr_bal[index] >0))//exists in pri
						add_new_row('#frmIssue #tblIssueItems',content);
						//	totGRNAmmount+=ammount;
						//$('.')
						//$("#frmIssue").validationEngine();
						}
						}
						else{
						var actualQty_prapo		= 0;
						var actualExQty_prapo	= 0;
						var actualQtyBal		= 0;
						var pri					= 0;
						var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="images/del.png" width="15" height="15" /></td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="" class="orderNo"></td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="" class="salesOrderNo"></td>';
                            content +='<td align="center" bgcolor="#FFFFFF" id="'+graphicNO+'" class="graphicNo">'+graphicNO+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+intMrnNo+'" class="mrnNo">'+intMrnNo+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatName">'+mainCatName+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatName">'+subCatName+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="item">'+itemName+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uom">'+uom+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="'+balToIssueQty+'" class="balToIssueQty">'+balToIssueQty+'</td>';
						content +='<td align="center" bgcolor="#FFFFFF" id="" class="orderNo"><input  id="'+balToIssueQty+'" class="validate[required,custom[number],max['+balToIssueQty+']] calculateValue Qty" style="width:80px;text-align:center" type="text" value="'+parseFloat(qty)+'"/></td>';
						content +='</tr>';
						
						add_new_row('#frmIssue #tblIssueItems',content);
						
						}
						//----------------------------	
						$('.delImg').click(function(){
						$(this).parent().parent().remove();
						});
						
						}
						});
						disablePopup();
						closePopUp();
						}
						//-------------------------------------
						function alertx()
						{
						$('#frmIssue #butSave').validationEngine('hide')	;
						}
						function alertDelete()
						{
						$('#frmIssue #butDelete').validationEngine('hide')	;
						}
						
						function closePopUp(){
						
						}
						//----------------------------------------------
						function add_new_row(table,rowcontent){
						if ($(table).length>0){
						if ($(table+' > tbody').length==0) $(table).append('<tbody />');
						($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
						}
						}
						//----------------------------------------------- 
						function clearRows()
						{
						//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
						var rowCount = document.getElementById('tblIssueItems').rows.length;
						//alert(rowCount);
						//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
						for(var i=1;i<rowCount;i++)
						{
						document.getElementById('tblIssueItems').deleteRow(1);
						
						}
						}
						//------------------------------------------------------------
						function checkAlreadySelected(){
						$('#tblIssueItems .item').each(function(){
						
						var orderNo	= $(this).parent().find(".orderNo").html();
						var styleNo	= $(this).parent().find(".salesOrderNo").attr('id');
						var mrnNo	= $(this).parent().find(".mrnNo").html();
						var itemId	= $(this).attr('id');
						
						var j=0;
						$('#tblItemsPopup .itemP').each(function(){
						j++;
						var mrnNoP=$(this).parent().find(".mrnNoP").attr('id');
						var orderNoP=$(this).parent().find(".orderNoP").attr('id');
						var styleNoP=$(this).parent().find(".salesOrderNoP").attr('id');
						var itemIdP=$(this).attr('id');
						
						//alert(orderNo+"=="+orderNoP+"***"+styleNo+"=="+styleNoP+"***"+mrnNo+"=="+mrnNoP+"***"+itemId+"=="+itemIdP+"***")
						if((orderNo==orderNoP) && (styleNo==styleNoP) && (mrnNo==mrnNoP) && (itemId==itemIdP)){
						document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].checked=true;
						document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].disabled=true;
						}
						});
						});
						}
					//------------------------------------------------------------------------------------------	
					 function loadmrn()//function to load mrn for mrn type
						  	 {
					var basePath	= "presentation/warehouse/issue/addNew/";
						var mrnType       	= $('#cboMRNType').val();
						var departmentId    = $('#cboDepartment').val(); 
						var issueLocationId  =	$('#IssueLocation').val();
						var url = basePath+"issue-db-get.php?requestType=loadMrn&mrnType="+mrnType+"&departmentId="+departmentId+"&issueLocationId="+issueLocationId;
						
					 var httpobj_1        = $.ajax({url:url,dataType: "json", async:false,success:function(json)

                        {   

                        

                        $('#cboMRN').html(json.html)

                     

                        }})

                             }
							 
							 
						
					  	 


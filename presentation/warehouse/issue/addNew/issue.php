<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
$mainPath 		= $_SESSION['mainPath'];
$location 		= $_SESSION['CompanyID'];
$company 		= $_SESSION['headCompanyId'];
$intUser 		= $_SESSION["userId"];
$thisFilePath 	=  $_SERVER['PHP_SELF'];
 
$issueNo		= $_REQUEST['issueNo'];
$year 			= $_REQUEST['year'];
$programName	='Material Issued Note';
$programCode	='P0231';
$menuID			='231';

if(($issueNo=='')&&($year=='')){
	$savedStat = '';
	$department=loadDefaultDepartment($intUser);
	$intStatus=$savedStat+1;
	$date='';
	$remarks='';
	$mrnLocation='';
	$supplier='';
}
else{
	$result=loadHeader($issueNo,$year);
	while($row=mysqli_fetch_array($result))
	{
		$department=$row['intDepartment'];
		$date = $row['datdate'];
		$savedStat=$row['intApproveLevels'];
		$intStatus=$row['intStatus'];
		$remarks=$row['strRemarks'];
		$mrnType=$row['mrnType'];
		$supplier = $row['intSupplier'];
		$mrnLocation=$row['intCompanyId'];
		if($mrnType==7 || $mrnType==8)
		$type	= 2;
		else 
		$type =1;
	}
}

$editMode=loadEditMode($programCode,$intStatus,$savedStat,$intUser);
$confirmatonMode=loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser);
	if($issueNo==''){
		$confirmatonMode=0;	
	}
	
	$locationData = loadAvailableLocations();

$mainPage=$backwardseperator."main.php";
?>
<title>Material Issue Note</title>
 <head>
<?php
//include "include/javascript.html";
?>
<!-- <script type="text/javascript" src="presentation/warehouse/issue/addNew/issue-js.js"></script>
--> </head>

<body>

<form id="frmIssue" name="frmIssue" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Material Issue Note</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="22" class="normalfnt">Issue No</td>
            <td width="22%"><input name="txtIssueNo" type="text" disabled="disabled" class="txtText" id="txtIssueNo" style="width:60px" value="<?php echo $issueNo ?>"/><input name="txtIssueYear" type="text" disabled="disabled" class="txtText" id="txtIssueYear" style="width:40px" value="<?php echo $year ?>"/></td>
            <td width="17%">&nbsp;</td>
            <td width="22%">&nbsp;</td>
            <td width="8%" class="normalfnt">Date</td>
            <td width="21%"><input name="dtDate" type="text" value="<?php if($issueNo){ echo substr($date,0,10); }else { echo date("Y-m-d"); }?>" class="txtbox" id="dtDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="27" class="normalfnt">Issue Location</td>
            <td width="39%">
			<select name="IssueLocation" id="IssueLocation" style="width:250px"  class="validate[required] txtText" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>>
                  <option value=""></option>
                  <?php
					$locationList = loadAvailableLocations();
					echo $locationList;
				?>
            </select>
			</td>
            <td width="22%">&nbsp;</td>
            <td width="8%" class="normalfnt">Department</td>
            <td width="21%">
			<select name="cboDepartment" id="cboDepartment" style="width:150px"  class="validate[required] txtText" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>>
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_department.intId,
							mst_department.strName
							FROM
							mst_department
							WHERE
							mst_department.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$department)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td width="22%">&nbsp;</td>
            <td width="8%" class="normalfnt">&nbsp;</td>
            <td width="21%">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
<tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="27" class="normalfnt">Note</td>
            <td width="61%"><textarea name="txtRemarks" id="txtRemarks" cols="35" rows="3" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>><?php echo $remarks; ?></textarea></td>
            <td width="8%" class="normalfnt"><div id="divPrice" style="display:none"><?php echo $priceEdit; ?></div>
              Other
                <input type="radio" id="radio1" name="type" value="1" <?php if($type=='' || $type ==1){ ?>checked <?php } ?>></td>
            <td width="21%" class="normalfnt">Loan In/Out 
              <input type="radio" id="radio2" name="type" value="2"<?php if($type ==2){ ?>checked <?php } ?>></td>
          </tr>
            </table></td>
</tr>
            <tr >
            <td><div id="supplier_tr" style="display: none"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="10%" class="normalfnt">Supplier</td>
                        <td width="60%"><select name="cboSupplier" id="cboSupplier" style="width:150px"
                                    class="validate[required] txtText" <?php if ($editMode == 0) { ?> disabled="disabled"<?php } ?>>
                                <option value=""></option>
                                <?php
                                $sql = "SELECT
                                        mst_supplier.intId,
                                        mst_supplier.strName
                                    FROM
                                        mst_supplier
                                    WHERE
                                        mst_supplier.intStatus = '1'
                                    ORDER BY
                                        mst_supplier.strName ASC";
                                $result = $db->RunQuery($sql);
                                while ($row = mysqli_fetch_array($result)) {
                                    if ($row['intId'] == $supplier)
                                        echo "<option value=\"" . $row['intId'] . "\" selected=\"selected\">" . $row['strName'] . "</option>";
                                    else
                                        echo "<option value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                }
                                ?>
                            </select></td>
                        <td width ="22%"></td>
                    </tr></table></div></td>


      </tr>      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="92%">&nbsp;</td>
            <td width="8%"> <?php if($editMode!=0){  ?><img src="images/Tadd.jpg" width="92" height="24" class="mouseover" id="butAddItems"  /><?php } ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="bordered" id="tblIssueItems" >
            <tr>
              <th width="3%" height="22" >Del</th>
              <th width="6%" >Order No</th>

              <th width="10%" >Sales Order No</th>
                <th width="5%" >Graphic No</th>
              <th width="8%" >MRN No</th>
              <th width="11%" >Main Category</th>
              <th width="11%" >Sub Category</th>
              <th width="26%" >Item Description</th>
              <th width="7%">UOM</th>
              <th width="9%">MRN Bal Qty</th>
              <th width="9%"> Qty</th>
              </tr>
            <?php

				$result = loadDetails($issueNo,$year);
				$totAmm=0;
				while($row=mysqli_fetch_array($result))
				{
					if($row['strOrderNo']==0){
						$bomItemFlag=0;
					}
					else{
						$bomItemFlag=1;
					}
					
					if($bomItemFlag==0){
					$stockBalQty=getStockBalance_bulk($location,$row['intItemId']);
					}
					else{
					//$stockBalQty=getStockBalance($company,$location,$row['strOrderNo'],$row['intOrderYear'],$row['strStyleNo'],$row['intItemId']);
					$stockBalQty=getStockBalance_bulk($location,$row['intItemId']);
					}
					$intOrderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$orderNo=$row['strOrderNo']."/".$row['intOrderYear'];
					$salesOrderNo=$row['strSalesOrderNo']."/".$row['part'];
					$salesOrderId=$row['intSalesOrderId'];
					$strStyleNo=$row['strStyleNo'];
					$strGraphicNo=$row['strGraphicNo'];
					$intMrnNo=$row['intMrnNo'];
					$mrnYear=$row['intMrnYear'];
					$mainCatName=$row['maincat'];
					$subCatName=$row['subcat'];
					$mainCatId=$row['intMainCategory'];
					$subCatId=$row['intSubCategory'];
					$itemId=$row['intItemId'];
					$itemName=$row['item'];
                                        $itemHide=$row['ITEM_HIDE'];
					$uom=$row['uom'];
					$Qty=$row['dblQty'];
					$mrnQty=$row['dbMrnQty'];
					$issedQty=$row['dblIssudQty'];
					$returnQty=$row['dblReturnQty'];
					$balToIssue=$mrnQty+$returnQty-$issedQty;
					if($stockBalQty>$balToIssue){
						$maxIssueQty=$balToIssue;
					}
					else{
						$maxIssueQty=$stockBalQty;
					}
					if($bomItemFlag==0){
					$orderNo='';
					$salesOrderNo='';
					$salesOrderId='';
					$strStyleNo='';
					}
					
					
					$totAmm+=$Qty;
			?>
<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><?php if($editMode==1){  ?><img class="delImg" src="images/del.png" width="15" height="15" /><?php } ?></td> 
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $intOrderNo ?>" class="orderNo"><?php echo $orderNo ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $salesOrderId ?>" class="salesOrderNo"><?php echo $salesOrderNo ?></td>
            <td align="center" bgcolor="#FFFFFF" id="<?php echo $strGraphicNo ?>" class="graphicNo"><?php echo $strGraphicNo ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $intMrnNo."/".$mrnYear ?>" class="mrnNo"><?php echo $intMrnNo."/".$mrnYear ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $maincatId ?>" class="mainCatName"><?php echo $mainCatName ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $subCatId ?>" class="subCatName"><?php echo $subCatName ?></td>
                        <?php
                        if($itemHide==1)
                        {
                         ?>
                        <td align="center" bgcolor="#FFFFFF" id="<?php echo $itemId ?>" class="item">*****</td>
                        <?php
                        }else
                        {
                        ?>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $itemId ?>" class="item"><?php echo $itemName ?></td>
                        <?php
                        }
                        ?>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $uom ?>" class="uom"><?php echo $uom ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $balToIssue ?>" class="balToIssueQty"><?php echo $balToIssue ?></td>
			<td align="center" bgcolor="#FFFFFF" id=""><input  id="<?php echo $maxIssueQty ?>" class="validate[required,custom[number],max[<?php echo $maxIssueQty ?>]] calculateValue Qty" style="width:80px;text-align:center" type="text" value="<?php echo $Qty ?>"  <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/></td>
			</tr>            
            <?php
				}
			?>
          </table>
        </div></td>
      </tr>

      <tr>
        <td align="center" class="tableBorder_allRound">
        <a name="butNew"  class="button white medium" id="butNew">New</a>
         <?php if(($form_permision['add']||$form_permision['edit']) && $editMode==1){ ?>
         <a id="butSave" name="butSave"  class="button white medium">Save</a>
		 <?php } ?>
         <a id="butConfirm" name="butConfirm" style=" <?php if(($intStatus>1) and ($confirmatonMode==1) && $confirmatonMode==1){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>" class="button white medium">Approve</a>
         <a id="butReport" name="butReport"  class="button white medium">Report</a>
         <a href="main.php"  class="button white medium">Close</a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>


</body>
</html>
<?php
	//--------------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$style,$item)
	{
		global $db;
		  $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.intSalesOrderId =  '$style' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return (float)($rows['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return (float)($row['stockBal']);	
	}
//------------------------------function load details---------------------
function loadDetails($serialNo,$year)
{
	global $db;
	  $sql = "SELECT
mst_item.strName as item,
mst_item.intMainCategory,
mst_maincategory.strName as maincat,
mst_item.intSubCategory,
mst_subcategory.strName as subcat,
mst_item.strCode,
mst_item.intBomItem,
mst_item.ITEM_HIDE,
tb1.strOrderNo,
tb1.intOrderYear,
tb1.strStyleNo,
trn_orderdetails.strSalesOrderNo,
trn_orderdetails.intSalesOrderId,
tb1.intMrnNo,
tb1.intMrnYear,
tb1.intItemId, 
tb1.dblQty,
ware_issueheader.intStatus,
ware_issueheader.intApproveLevels,
ware_mrndetails.dblQty as dbMrnQty,
IFNULL((SELECT
Sum(ware_issuedetails.dblQty)
FROM
ware_issueheader
Inner Join ware_issuedetails ON ware_issueheader.intIssueNo = ware_issuedetails.intIssueNo AND ware_issueheader.intIssueYear = ware_issuedetails.intIssueYear
WHERE
ware_issuedetails.intMrnNo =  tb1.intMrnNo AND
ware_issuedetails.intMrnYear =  tb1.intMrnYear AND 
ware_issuedetails.strOrderNo = tb1.strOrderNo AND 
ware_issuedetails.intOrderYear = tb1.intOrderYear AND
ware_issuedetails.strStyleNo = tb1.strStyleNo AND  
ware_issuedetails.intItemId =  tb1.intItemId AND
ware_issueheader.intStatus =  '1'
),0) as dblIssudQty, 
IFNULL(
		(
			SELECT
				Sum(ware_issuedetails.dblReturnQty)
			FROM
				ware_issueheader
			INNER JOIN ware_issuedetails ON ware_issueheader.intIssueNo = ware_issuedetails.intIssueNo
			AND ware_issueheader.intIssueYear = ware_issuedetails.intIssueYear
			WHERE
				ware_issuedetails.intMrnNo = tb1.intMrnNo
			AND ware_issuedetails.intMrnYear = tb1.intMrnYear
			AND ware_issuedetails.strOrderNo = tb1.strOrderNo
			AND ware_issuedetails.intOrderYear = tb1.intOrderYear
			AND ware_issuedetails.strStyleNo = tb1.strStyleNo
			AND ware_issuedetails.intItemId = tb1.intItemId
			AND ware_issueheader.intStatus = '1'
		),
		0
	) AS dblReturnQty,
mst_units.strCode as uom  , 
mst_part.strName as part,
trn_orderdetails.strGraphicNo as strGraphicNo
FROM
ware_issuedetails as tb1  
left Join trn_orderdetails ON tb1.strOrderNo = trn_orderdetails.intOrderNo AND tb1.intOrderYear = trn_orderdetails.intOrderYear AND tb1.strStyleNo = trn_orderdetails.intSalesOrderId
Inner Join ware_issueheader ON tb1.intIssueNo = ware_issueheader.intIssueNo AND tb1.intIssueYear = ware_issueheader.intIssueYear
Inner Join ware_mrndetails ON tb1.intMrnNo = ware_mrndetails.intMrnNo AND tb1.intMrnYear = ware_mrndetails.intMrnYear AND tb1.strOrderNo = ware_mrndetails.intOrderNo AND tb1.intOrderYear = ware_mrndetails.intOrderYear AND tb1.strStyleNo = ware_mrndetails.strStyleNo AND tb1.intItemId = ware_mrndetails.intItemId
Inner Join mst_item ON ware_mrndetails.intItemId = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
Inner Join mst_units ON mst_item.intUOM = mst_units.intId  
left Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
WHERE 
/*mst_item.intStatus = '1' AND*/ 
tb1.intIssueNo =  '$serialNo' AND
tb1.intIssueYear =  '$year' 
ORDER BY mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc 
";			 

//  echo $sql;
$result = $db->RunQuery($sql);
return $result;
}
//------------------------------function load Header---------------------
function loadHeader($serialNo,$year)
{
	global $db;
		 $sql = "SELECT
					ware_issueheader.datdate,
					ware_issueheader.intStatus,
					ware_issueheader.intApproveLevels,
					ware_issueheader.intUser,
					ware_issueheader.strRemarks,
					ware_issueheader.intDepartment,
					ware_issueheader.intCompanyId,
					ware_issueheader.intSupplier,
					(SELECT
						ware_mrnheader.mrnType
						FROM
						ware_mrnheader
						INNER JOIN ware_issuedetails ON ware_mrnheader.intMrnNo = ware_issuedetails.intMrnNo AND ware_mrnheader.intMrnYear = ware_issuedetails.intMrnYear
						WHERE
						ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo AND
						ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear 
						limit 1
					) as mrnType 
					FROM
					ware_issueheader 
					WHERE
					ware_issueheader.intIssueNo =  '$serialNo' AND
					ware_issueheader.intIssueYear =  '$year' ";
			 $result = $db->RunQuery($sql);
			 return $result;
}
//------------------------------function load Default Department---------------------
function loadDefaultDepartment($intUser){
	global $db;
	 $sql1 = "SELECT
				sys_users.intDepartmentId
				FROM
				sys_users
				WHERE
				sys_users.intUserId =  '$intUser'";	
	$result1 = $db->RunQuery($sql1);
	$row1=mysqli_fetch_array($result1);
	$department=$row1['intDepartmentId'];
	return $department;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}

function loadAvailableLocations(){
	global $db;
	global $location;
	global $company;
	global $mrnLocation;

	// Getting location type
				  $locationSql = "Select strName,intCompanyId,int_stock_locationin,COMPANY_MAIN_STORES_LOCATION FROM mst_locations WHERE intId = $location";
				  $locationQuery = $db->RunQuery($locationSql);
				  $locationResult = mysqli_fetch_array($locationQuery);
				  
				  // if location is Company Main stores Locations
				  if($locationResult["COMPANY_MAIN_STORES_LOCATION"] == 1) {
				  
				  //getting current location data
				  $locationSql = "Select intId,strName FROM mst_locations WHERE intId = $location ";
				  $locationQuery = $db->RunQuery($locationSql);
				  $locationList = '';
				  //getting All Location IDs
				  $locations = $location;
				  while($row = mysqli_fetch_array($locationQuery)){
					  if($mrnLocation == $row['intId']){
					  $locationList .= "<option value=\"".$row['intId']."\" selected='selected'>".$row['strName']."</option>";
					  }
					  else{
					  $locationList .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
					  }
				  }	  		  
				  //getting all not maintaining stock locations according to the Main Company
				  $notMaintainSql = "Select intId,strName FROM mst_locations WHERE int_stock_locationin = 0 AND intCompanyId = $company ";
				  $notMaintainQuery = $db->RunQuery($notMaintainSql);
				 
				  //getting All Location IDs
				  $locations = $location;
				  while($row = mysqli_fetch_array($notMaintainQuery)){
					    if($mrnLocation == $row['intId']){
					  $locationList .= "<option value=\"".$row['intId']."\" selected='selected'>".$row['strName']."</option>";
					  }
					  else{
					  $locationList .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
					  }
				  }
				  
				  return  $locationList;
		}
	else{
				//getting current location data
				  $locationSql = "Select intId,strName FROM mst_locations WHERE intId = $location ";
				  $locationQuery = $db->RunQuery($locationSql);
				  $locationList = '';
				  //getting All Location IDs
				  $locations = $location;
				  while($row = mysqli_fetch_array($locationQuery)){
					  $locationList .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
				  }
				  return  $locationList;
	}
}
	
?>

<?php 
ini_set('display_errors',0);
//////////////////////////////////////////////
//Create By:H.B.G Korala 
/////////////////////////////////////////////
	session_start();
	$backwardseperator 	= "../../../../";
	$mainPath 			= $_SESSION['mainPath'];
	$userId 			= $_SESSION['userId'];
	$companyId 			= $_SESSION['CompanyID'];
	$location 			= $_SESSION['CompanyID'];
	$company 			= $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once "../../../../class/cls_commonFunctions_get.php";
	require_once $backwardseperator."class/warehouse/cls_warehouse_get.php";
	require_once "../../../../class/warehouse/issue/cls_issue_get.php";
	require_once "../../../../class/finance/budget/cls_common_function_budget_get.php";
	require_once "../../../../class/finance/budget/cls_common_function_budget_get.php";
	
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$objwhouseget			= new cls_warehouse_get($db);
	$obj_common				= new cls_commonFunctions_get($db);
	$obj_comm_budget_get	= new cls_common_function_budget_get($db);
	$obj_issue_get			= new cls_issue_get($db);
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo 	 = $_REQUEST['serialNo'];
	$year 		 = $_REQUEST['Year'];
	$department  = $_REQUEST['department'];
	$IssueLocation  = $_REQUEST['IssueLocation'];
	$date 		 = $_REQUEST['date'];
	$remarks 	= $_REQUEST['remarks'];
	$supplier   = $_REQUEST['supplier'];
	
	$arr 		= json_decode($_REQUEST['arr'], true);
	
	$programName='Material Issued Note';
	$programCode='P0231';

	$ApproveLevels = (int)getApproveLevel('Material Issued Note');
	$issueApproveLevel = $ApproveLevels+1;
	
	
//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$savableFlag= 1;
		
		if($serialNo==''){
			$serialNo 	= getNextIssueNo();
			$year = date('Y');
			$editMode=0;
			$savedStatus=$issueApproveLevel;
			$savedLevels=$ApproveLevels;
		}
		else{
			$savableFlag=getSaveStatus($serialNo,$year);
			$editMode=1;
			$sql = "SELECT
			ware_issueheader.intStatus, 
			ware_issueheader.intApproveLevels 
			FROM ware_issueheader 
			WHERE
			ware_issueheader.intIssueNo =  '$serialNo' AND
			ware_issueheader.intIssueYear =  '$year'";

			$result = $db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			$savedStatus=$row['intStatus'];
			$savedLevels=$row['intApproveLevels'];
		}
		
		$editPermission=loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
		
		$locationEditFlag=getEditLocationValidation('MRN',$IssueLocation,$serialNo,$year);
		
		
		//--------------------------
		if(( $locationEditFlag==1) && ($editMode==1)){
			 $rollBackFlag=1;
			 $rollBackMsg="Invalid Edit Location";
		 }
		else if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This Issue No is already confirmed.cant edit";
		 }
		 else if($editPermission==0){
			 $rollBackFlag=1;
			 $rollBackMsg="No Permission to save.";
		 }

		else if($editMode==1){//edit existing grn
		$sql = "UPDATE `ware_issueheader` SET intStatus ='$issueApproveLevel', 
												       intApproveLevels ='$ApproveLevels', 
													  datdate ='$date',
													  intModifiedBy ='$userId',
													  strRemarks ='$remarks',
													  dtmModifiedDate =now() , 
													  intCompanyId ='$IssueLocation',
													  intSupplier = '$supplier'  
				WHERE (`intIssueNo`='$serialNo') AND (`intIssueYear`='$year')";
		$result = $db->RunQuery2($sql);
		
		//inactive previous recordrs in approvedby table
		$maxAppByStatus=(int)getMaxAppByStatus($serialNo,$year)+1;
		$sql = "UPDATE `ware_issueheader_approvedby` SET intStatus ='$maxAppByStatus' 
				WHERE (`intIssueNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
		$result1 = $db->RunQuery2($sql);
		
		}
		else{
			//$sql = "DELETE FROM `ware_issueheader` WHERE (`intIssueNo`='$serialNo') AND (`intIssueYear`='$year')";
			//$result1 = $db->RunQuery2($sql);

			$sql = "INSERT INTO `ware_issueheader` (`intIssueNo`,`intIssueYear`,intDepartment,intStatus,intApproveLevels,strRemarks,datdate,dtmCreateDate,intUser,intCompanyId,issueFrom,intSupplier) 
					VALUES ('$serialNo','$year','$department','$issueApproveLevel','$ApproveLevels','$remarks','$date',now(),'$userId','$IssueLocation','$location','$supplier')";
			$result = $db->RunQuery2($sql);
		}
		//-----------delete and insert to detail table-----------------------
		if(($result) && ($rollBackFlag!=1)){
                    
		$sql = "DELETE FROM `ware_issuedetails` WHERE (`intIssueNo`='$serialNo') AND (`intIssueYear`='$year')";
		$result2 = $db->RunQuery2($sql);
			$toSave=0;
			$saved=0;
			$rollBackFlag=0;
			$rollBackMsg ='Maximum Issue Qty </br> for items are...'; 
			$rollBackFlagLoc =0; 
			$i=0;
			foreach($arr as $arrVal)
			{
				$location 	      = $_SESSION['CompanyID'];
				$company 	      = $_SESSION['headCompanyId'];
				$orderNo 	      = $arrVal['orderNo'];
				$orderNoArray     = explode('/',$orderNo);
				$allocatedFlag    = 0; //$allocatedFlag=1;
				
				
				if($orderNoArray[0]==''){
					$orderNoArray[0]=0;
					$allocatedFlag=0;
				}
				else{
					$allocatedFlag=1;//$allocatedFlag=1;
				}
				if($orderNoArray[1]==''){
					$orderNoArray[1]=0;
				}
				$salesOrderId 		 = $arrVal['salesOrderId'];
				$mrnNo 		 = $arrVal['mrnNo'];
				$mrnNoArray  = explode('/',$mrnNo);
				$itemId 	 = $arrVal['itemId'];
				$Qty 		 = $arrVal['Qty'];
			 	
			$order_status	= getOrderStatus($orderNoArray[0],$orderNoArray[1]);
			if(($order_status[0] <= 0 || ($order_status[0]>=$order_status[1]+1)) && $orderNoArray[0] >0){
				 $rollBackFlagLoc=1;
				 $rollBackMsg="Approval error! Unapproved Order.".$orderNoArray[1].'/'.$orderNoArray[2];
			 }
			else if($order_status[2]==1 && $order_status[3] !=1 ){
				 $rollBackFlagLoc=1;
				 $rollBackMsg="Can't raise Issue note for Non-Instant pre costing order ".$orderNoArray[0].'/'.$orderNoArray[1];
			 }
			
			if($allocatedFlag==0){
			$stockBalQty=getStockBalance_bulk($location,$itemId);
			//print_r("A1");
			}
			else{
			$stockBalQty=getStockBalance_bulk($location,$itemId);
			//$stockBalQty=getStockBalance($company,$location,$orderNoArray[0],$orderNoArray[1],$salesOrderId,$itemId);
			//print_r("A2");
			}

				//------check maximum ISSUE Qty--------------------
			 	$sqlc = "SELECT 
				mst_item.strName as itemName,
				ware_mrndetails.intMrnNo,
				ware_mrndetails.intMrnYear,
				ware_mrndetails.intOrderNo,
				ware_mrndetails.intOrderYear,
				ware_mrndetails.strStyleNo,
				sum(ware_mrndetails.dblQty) as dblQty,
				sum(ware_mrndetails.dblIssudQty ) as dblIssudQty
				FROM
				ware_mrndetails 
				Inner Join ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
				Inner Join mst_item ON ware_mrndetails.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				WHERE 
				ware_mrnheader.intStatus = '1' AND 
				ware_mrndetails.intMrnNo =  '$mrnNoArray[0]' AND 
				ware_mrndetails.intMrnYear =  '$mrnNoArray[1]' AND 
				ware_mrndetails.intItemId =  '$itemId' ";
				
				if($orderNoArray[0]!='')
				$sqlc.=" AND ware_mrndetails.intOrderNo =  '$orderNoArray[0]'";
				if($orderNoArray[1]!='')
				$sqlc.=" AND ware_mrndetails.intOrderYear =  '$orderNoArray[1]'";
				if($salesOrderId!='')
				$sqlc.=" AND ware_mrndetails.strStyleNo =  '$salesOrderId'";				
					
				
				//echo $sqlc ;
				
				$resultsc = $db->RunQuery2($sqlc);
				$rowc = mysqli_fetch_array($resultsc);
				$item=$rowc['itemName'];
			 $totIssueQty=geTotConfirmedIssueQtys($mrnNoArray[0],$mrnNoArray[1],$orderNoArray[0],$orderNoArray[1],$salesOrderId,$itemId);
			 $totRetStoresQty=geTotConfirmedStockRetStoresQtys($mrnNoArray[0],$mrnNoArray[1],$orderNoArray[0],$orderNoArray[1],$salesOrderId,$itemId);

/*				if($rowc['dblQty']-$rowc['dblIssudQty']<$stockBalQty)
				$balQtyToIssue = $rowc['dblQty']-$rowc['dblIssudQty'];
				else
				$balQtyToIssue = $stockBalQty;
*/	
				//echo "totIssueQty: ".$totIssueQty.", totRetStoresQty: ".$totRetStoresQty;
				//$dblQty=$rowc['dblQty'];
				//echo "dblQty ".$dblQty;
				$dblQty = round($rowc['dblQty'],4);
				$balQtyToIssue=$dblQty-$totIssueQty+$totRetStoresQty;
				
				//echo "stockBalQty ".$stockBalQty;
				//echo "balQtyToIssue ".$balQtyToIssue;
							
				if($stockBalQty<$balQtyToIssue){
					$balQtyToIssue=$stockBalQty;
				}
				
				//echo "2 balQtyToIssue: ".$balQtyToIssue;
		//--------------------------
				$response['arrData'] = $objwhouseget->getLocationValidation('ISSUE',$IssueLocation,$mrnNoArray[0],$mrnNoArray[1]);//check for issue location
				//--------------------------
				
				if($response['arrData']['type']=='fail'){
					 $rollBackFlagLoc=1;
					 $rollBackMsg=$response['arrData']['msg'];
				 }
				//echo "2Qty : ".round($Qty,2)."  , bal Qty : ".round($balQtyToIssue,2);
				//echo "Qty ".$Qty;
				//echo "balQtyToIssue ".$balQtyToIssue;
				$balQtyToIssue=round($balQtyToIssue,4);
				
				if($rollBackFlagLoc!=1){
					if(round($Qty,2) > round($balQtyToIssue,2)){
						//echo "1Qty : $Qty  , bal Qty : $balQtyToIssue";
					//	call roll back--------****************
						$rollBackFlag=1;
						if($allocatedFlag==1){
							$rollBackMsg .="<br> ".$orderNoArray[0]."/".$orderNoArray[1]."-".$salesOrderId."-".$mrnNoArray[0]."/".$mrnNoArray[1]."-".$item." =".round($Qty,2).":".$balQtyToIssue;
						}
						else{
							$rollBackMsg .="<br> ".$mrnNoArray[0]."/".$mrnNoArray[1]."-".$item." =".$balQtyToIssue;
							
						}
						//exit();
					}
				}

				
				$qty[$item]+=$Qty;
			//	$balToIssuety[$item]+=$balQtyToIssue;
			//	echo $qty[$item];
				if($rollBackFlag!=1){
					if(round($qty[$item],2)>round($stockBalQty,2)){						
						$rollBackFlag=1;
						$rollBackMsg.="</br> ".$item." (stock Balance)=".$stockBalQty;
					}
				}
				

				//----------------------------
				if($rollBackFlag!=1){
				if($orderNoArray[0]==''){
					$orderNoArray[0]=0;
				}
				if($orderNoArray[1]==''){
					$orderNoArray[1]=0;
				}				
				
				$Qty=round($Qty,4);
				
				$mrn_orderwise = "select ware_mrndetails.MRN_TYPE 
					from ware_mrndetails 
					where ware_mrndetails.intMrnNo = '$mrnNoArray[0]' 
					AND ware_mrndetails.intMrnYear = '$mrnNoArray[1]'  ";  
					          
					$result_orderwise = $db->RunQuery2($mrn_orderwise);
					$row_order 		  = mysqli_fetch_array($result_orderwise);
					$order_wise 	  = $row_order['MRN_TYPE'];
					
					if(($order_wise==3 || $order_wise==6) && ($orderNoArray[0]==0 || $orderNoArray[0]=='')){
						$flag_order_err = 1;
					}
				   	
				
					$sql = "INSERT INTO `ware_issuedetails` (`intIssueNo`,`intIssueYear`,`strOrderNo`,`intOrderYear`,`strStyleNo`,`intMrnNo`,`intMrnYear`,`intItemId`,`dblQty`,`dblReturnQty`) 
					VALUES ('$serialNo','$year','$orderNoArray[0]','$orderNoArray[1]','$salesOrderId','$mrnNoArray[0]','$mrnNoArray[1]','$itemId','$Qty','0')";
					//echo $sql; 
                                        $result3 = $db->RunQuery2($sql);
					if($result3==1){
					$saved++;
					}
					else{
					$sqlMsg .=$sql;	
					}
				}
				$toSave++;
				$i++;
			}
		}
		//BEGIN - BUDGET VALIDATION {
		if($rollBackFlag != 1)
		{			
			$validateResult	= budgetValidation($serialNo,$year);
			if($validateResult['type'] == '1')
			{
				$rollBackFlag	= 1;
				$rollBackMsg	= $validateResult['msg'];
			}
				
		}
		//END 	- BUDGET VALIDATION }
		
		if($rollBackFlagLoc==1){
		   $rollBackFlag=1;
		}
		
		if($result){
			if(($saved==0) && ($toSave==0)){
				$detailsSavedFlag=0;
			}
			else if($toSave==$saved){
				$detailsSavedFlag=1;
			}
		}
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($savableFlag==1) &&(!$result)){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Header saving error";
			$response['q'] 			= $sql;
		}
		else if($flag_order_err==1)
		{
			$response['type'] 		= 'fail';
			$response['msg'] 		= 'Orderwise MRN should have a Order number ';  
			$response['q'] 		    =  $mrn_orderwise;
		}
		else if(($savableFlag==1) &&($toSave==0) &&($saved==0)){
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Details saving error";
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$confirmationMode=getConfirmationMode($programCode,$userId,$ApproveLevels,$issueApproveLevel);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
//----------------------------------------
	echo json_encode($response);
//-----------------------------------------	
	function getNextIssueNo()
	{
		global $db;
		global $companyId;
		$sql = "SELECT
				sys_no.intIssueNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$companyId'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextIssueNo = $row['intIssueNo'];
		
		$sql = "UPDATE `sys_no` SET intIssueNo=intIssueNo+1 WHERE (`intCompanyId`='$companyId')  ";
		$db->RunQuery2($sql);
		
		return $nextIssueNo;
	}
	//-----------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$salesOrderId,$item)
	{
		global $db;
	  	 $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				round(Sum(ware_stocktransactions_bulk.dblQty),2) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";
		
		//echo $sql;

		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getSaveStatus($serialNo,$year)
	{
		global $db;
		$sql = "SELECT ware_issueheader.intStatus, ware_issueheader.intApproveLevels FROM ware_issueheader WHERE (intIssueNo='$serialNo') AND (`intIssueYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			
		return $editableFlag;
	}
	//-----------------------------------------------------------------
	function geTotConfirmedIssueQtys($mrnNo,$mrnYear,$orderNo,$orderYear,$salesOrderId,$item){
		global $db;
		 $sql="SELECT
			round(Sum(ware_issuedetails.dblQty),2) as issuedQty 
			FROM
			ware_issuedetails
			Inner Join ware_issueheader ON ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo AND ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear
			WHERE
			ware_issueheader.intStatus >  0 AND
			ware_issueheader.intApproveLevels >=  ware_issueheader.intStatus AND
			ware_issuedetails.intMrnNo =  '$mrnNo' AND
			ware_issuedetails.intMrnYear =  '$mrnYear' AND
			ware_issuedetails.strOrderNo =  '$orderNo' AND
			ware_issuedetails.intOrderYear =  '$orderYear' AND
			ware_issuedetails.strStyleNo =  '$salesOrderId' AND 
			ware_issuedetails.intItemId =  '$item' 
			GROUP BY
			ware_issuedetails.intItemId,
			ware_issuedetails.strStyleNo,
			ware_issuedetails.intOrderYear,
			ware_issuedetails.strOrderNo,
			ware_issuedetails.intMrnYear,
			ware_issuedetails.intMrnNo,
			ware_issueheader.intStatus,
			ware_issueheader.intApproveLevels
			";
			
			//echo $sql;
		
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$issuedQty = val($row['issuedQty']);
			return $issuedQty;
	}
	//-----------------------------------------------------------------
	function geTotConfirmedStockRetStoresQtys($mrnNo,$mrnYear,$orderNo,$orderYear,$salesOrderId,$item){
		global $db;
		 $sql="SELECT
			round(Sum(ware_issuedetails.dblReturnQty),2) as returnQty 
			FROM
			ware_issuedetails
			Inner Join ware_issueheader ON ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo AND ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear
			WHERE
			ware_issueheader.intStatus =  1 AND
			ware_issuedetails.intMrnNo =  '$mrnNo' AND
			ware_issuedetails.intMrnYear =  '$mrnYear' AND
			ware_issuedetails.strOrderNo =  '$orderNo' AND
			ware_issuedetails.intOrderYear =  '$orderYear' AND
			ware_issuedetails.strStyleNo =  '$salesOrderId' AND 
			ware_issuedetails.intItemId =  '$item' 
			GROUP BY
			ware_issuedetails.intItemId,
			ware_issuedetails.strStyleNo,
			ware_issuedetails.intOrderYear,
			ware_issuedetails.strOrderNo,
			ware_issuedetails.intMrnYear,
			ware_issuedetails.intMrnNo,
			ware_issueheader.intStatus,
			ware_issueheader.intApproveLevels
			";
			
			//echo $sql;
		
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$issuedQty = val($row['returnQty']);
			return $issuedQty;
	}
//-----------------------------------------------------------
	function getEditLocationValidation($type,$location,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
		  $sql = "SELECT
					ware_issueheader.intCompanyId
					FROM
					ware_issueheader
					Inner Join mst_locations ON ware_issueheader.intCompanyId = mst_locations.intId
					WHERE
					ware_issueheader.intIssueNo =  '$serialNo' AND
					ware_issueheader.intIssueYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			if($location == $row['intCompanyId'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function loadConfirmatonMode-------------------
function getConfirmationMode($programCode,$intUser,$approveLevels,$status){
		global $db;
		$k=$approveLevels+2-$status;
		$sqlp = "SELECT 
		menupermision.intEdit, 
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		if($rowp['int'.$k.'Approval']==1) 
		 $confirmatonMode=1; 
		else
		 $confirmatonMode=0; 
	 
	return $confirmatonMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		$resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
		 
		// $editMode;
		 
	return $editMode;
}
//------------------------------function get MaxAppByStatus---------------------
function getMaxAppByStatus($serialNo,$year){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	  $sql = "SELECT
				max(ware_issueheader_approvedby.intStatus) as status 
				FROM
				ware_issueheader_approvedby
				WHERE
				ware_issueheader_approvedby.intIssueNo =  '$serialNo' AND
				ware_issueheader_approvedby.intYear =  '$year'";
				
		$resultm = $db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm['status'];

}
//--------------------------------------------------------getMaxAppByStatus

function budgetValidation($serialNo,$year)
{
	global $db;
	global $location;
	global $company;
	global $obj_issue_get;
	global $obj_comm_budget_get;
	global $obj_common;
	
	$arr_issue_item_tot_U		= NULL;
	$arr_issue_sub_cat_tot_U	= NULL;
	$arr_issue_item_tot_A		= NULL;
	$arr_issue_sub_cat_tot_A	= NULL;
	$rollBackFlag			= 0;
	
	$header_array		= $obj_issue_get->get_header($serialNo,$year,'RunQuery2');
	$results_details	= $obj_issue_get->get_details_results($serialNo,$year,'RunQuery2');
	$results_fin_year	= $obj_comm_budget_get->loadFinanceYear_with_selected_date($company,$header_array["DATE"],'RunQuery2');
	$row_fin_year		= mysqli_fetch_array($results_fin_year);
	$budg_year			= $row_fin_year['intId'];
	
	while($row_details	= mysqli_fetch_array($results_details))
	{
		$item_price_issue			= getItemPrice($location,$row_details['ITEM_ID'],$row_details['QTY']);
		$sub_cat_budget_flag	= $obj_comm_budget_get->check_sub_category_budgeted_or_not($location,$row_details['SUB_CATEGORY_ID'],'RunQuery2');

		if($sub_cat_budget_flag	== 0)//item wise budget
		{
			$budget_type			= $obj_comm_budget_get->load_budget_type($location,$row_details['SUB_CATEGORY_ID'],$row_details['ITEM_ID'],'RunQuery2');
			if($budget_type == 'U') //unit budget
				$arr_issue_item_tot_U[$row_details['ITEM_ID']]			+= $row_details['QTY'];
			elseif($budget_type == 'A')				//amount budget
				$arr_issue_item_tot_A[$row_details['ITEM_ID']]			+= $row_details['QTY'] * $item_price_issue;			
		}
		else if($sub_cat_budget_flag == 1)//sub category wise budget
		{
			$budget_type			= $obj_comm_budget_get->load_budget_type($location,$row_details['SUB_CATEGORY_ID'],0,'RunQuery2');
			if($budget_type == 'U')//unit budget
				$arr_issue_sub_cat_tot_U[$row_details['SUB_CATEGORY_ID']]	+= $row_details['QTY'];
			elseif($budget_type == 'A')					//amount budget
				$arr_issue_sub_cat_tot_A[$row_details['SUB_CATEGORY_ID']]	+= $row_details['QTY'] * $item_price_issue;
		}
	}
	
	while (list($key_i, $value_i) = each($arr_issue_item_tot_U)) {//item ,unit budget			
				$budget_bal	= $obj_comm_budget_get->load_budget_balance($location,$header_array['DEPARTMENT_ID'],$budg_year,'',$key_i,date('m'),'RunQuery2');
				$budget_bal = $budget_bal['balUnits'];
				if($value_i > $budget_bal){
					$rollBackFlag	=1;
					$rollBackMsg	="There is no budget balance for this month for item ".$obj_common->get_item_name($key_i,'RunQuery2')."-".$budget_bal;
				}
	}
	while (list($key_i, $value_i) = each($arr_issue_item_tot_A)) {//item, amount budget
				$budget_bal	= $obj_comm_budget_get->load_budget_balance($location,$header_array['DEPARTMENT_ID'],$budg_year,'',$key_i,date('m'),'RunQuery2');
				$budget_bal = $budget_bal['balAmount'];
				if($value_i > $budget_bal){
					$rollBackFlag	=1;
					$rollBackMsg	="There is no budget balance for this month for item ".$obj_common->get_item_name($key_i,'RunQuery2')."-".$budget_bal;
				}
	}
	while (list($key, $value) = each($arr_issue_sub_cat_tot_U)) {//sub category ,unit budget
			$budget_bal	= $obj_comm_budget_get->load_budget_balance($location,$header_array['DEPARTMENT_ID'],$budg_year,$key,'',date('m'),'RunQuery2');
			$budget_bal = $budget_bal['balUnits'];
			if($value > $budget_bal){
				$rollBackFlag	=1;
				$rollBackMsg	="There is no budget balance for this month for sub category ".$obj_common->get_sub_category_name($key,'RunQuery2')."-".$budget_bal;
			}
	}
	while (list($key, $value) = each($arr_issue_sub_cat_tot_A)) {//sub category ,amount budget
			$budget_bal	= $obj_comm_budget_get->load_budget_balance($location,$header_array['DEPARTMENT_ID'],$budg_year,$key,'',date('m'),'RunQuery2');
			$budget_bal = $budget_bal['balAmount'];
			if($value > $budget_bal){
				$rollBackFlag	=1;
				$rollBackMsg	="There is no budget balance for this month for sub category ".$obj_common->get_sub_category_name($key,'RunQuery2')."-".$budget_bal;
			}
	}
	$response['type']	= $rollBackFlag;
	$response['msg']	= $rollBackMsg;
	return $response;
}

function getItemPrice($location,$itemId,$issueQty)
{
	global $obj_comm_budget_get;
	$itemPrice	= 0;
	
	$result1	= $obj_comm_budget_get->get_price_for_item($location,$itemId,'RunQuery2');
	while($row = mysqli_fetch_array($result1))
	{
		
		if($issueQty<=0)
			continue;
			
		$stockQty	= $row["stockBal"];
		$price		= $row["price"];

		if($issueQty<$stockQty)
		{
			$itemPrice .= $issueQty * $price;
			$issueQty	= 0;
		}
		else
		{
			$itemPrice .= $issueQty * $price;
			$issueQty   = $issueQty - $stockQty;
		}
	}
	return $itemPrice;
}
function getOrderStatus($orderNo,$year){
    global $db;
    $sql = "SELECT
			trn_orderheader.intStatus,
			trn_orderheader.intApproveLevelStart ,
			PO_TYPE ,
			INSTANT_ORDER  
			FROM
			trn_orderheader
			WHERE
			trn_orderheader.intOrderNo = '$orderNo' AND
			trn_orderheader.intOrderYear = '$year'

			";
			
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
	$arr[0]	= $row['intStatus'];
	$arr[1]	= $row['intApproveLevelStart'];
	$arr[2]	= $row['PO_TYPE'];
	$arr[3]	= $row['INSTANT_ORDER'];
	return $arr;
	
}

//----------------------------------------------------------------------------------------------------------

function getMRNLocation($mrnNO,$mrnYear){
	 global $db;
    $sql = "SELECT
			intCompanyId
			FROM
			ware_mrnheader
			WHERE
			ware_mrnheader.intMrnNo = '$mrnNO' AND
			ware_mrnheader.intMrnYear = '$mrnYear'

			";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
	return $row['intCompanyId'];
}

?>
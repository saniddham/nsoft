<?php
//ini_set('max_execution_time', 11111111) ;
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	  = $_SESSION['mainPath'];
	$userId 	  = $_SESSION['userId'];
	$requestType  = $_REQUEST['status'];
  	
	$transfNo	= $_REQUEST['transfNo'];
	$year	  	= $_REQUEST['year'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];

	include "{$backwardseperator}dataAccess/Connector.php";
	include "{$backwardseperator}libraries/mail/mail.php";
 	require_once $_SESSION['ROOT_PATH']."class/cls_mail.php";

 	$objMail 				= new cls_create_mail($db);
 	
if($requestType=="approve")
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	$rollBackFlag 	= 0;
	$rollBackMsg1 	= "Maximum Qtys for items are...";
	
	$sql 	= "SELECT intApproveLevels,intStatus FROM ware_storestransferheader WHERE intTransfNo='$transfNo' AND intTransfYear='$year' ";
	$result = $db->RunQuery2($sql);
	$row 	= mysqli_fetch_array($result);
	$status	=$row['intStatus'];
	$levels	=$row['intApproveLevels'];
	
	//if($row['intStatus']>$row['intApproveLevels'])
	//{
		$sql = "SELECT
				STD.intItemId,
				STD.dblQty,
				STD.dblExcessQty,
				STD.intTransfNo,
				STD.intTransfYear, 
				STH.intRequisitionNo,
				STH.intRequisitionYear
				FROM
				ware_storestransferdetails STD
				INNER JOIN ware_storestransferheader STH ON STD.intTransfNo = STH.intTransfNo AND 
				STD.intTransfYear = STH.intTransfYear
				WHERE
				STD.intTransfNo =  '$transfNo' AND
				STD.intTransfYear =  '$year' ";
		
		$result = $db->RunQuery2($sql);
		
		$arr_srn		= NULL; //from style stock
		$arr_srn_ex		= NULL; //from style stock
		
		$arr_srn_s		= NULL; //from style stock
		$arr_srn_ex_b	= NULL;//from bulk stock
		$arr_srn_ex_c	= NULL;//from color room stock
		
		while($row=mysqli_fetch_array($result))
		{	
			$SRNNo 			= $row['intRequisitionNo'];
			$SRNYear 		= $row['intRequisitionYear'];
			$itemId 		= $row['intItemId'];
			$transferExQty	= ceil($row['dblExcessQty']*100)/100;
			$transferQty	= ceil($row['dblQty']*100)/100;
			
			$sqlChk = " SELECT  
						tb1.intItemId as itemID, 
						mst_item.strName as itemName,
						tb1.intRequisitionNo,
						tb1.intRequisitionYear,
						ware_storesrequesitionheader.intReqToStores as stores,
						ware_storesrequesitionheader.intReqFromStores as req_by_stores,
						ware_storesrequesitionheader.intOrderYear,
						ware_storesrequesitionheader.strOrderNos,
						tb1.dblQty,
						tb1.dblExQty,
						IFNULL((SELECT
						Sum(ware_storestransferdetails.dblQty)
						FROM
						ware_storestransferheader
						Inner Join ware_storestransferdetails ON ware_storestransferheader.intTransfNo = ware_storestransferdetails.intTransfNo AND ware_storestransferheader.intTransfYear = ware_storestransferdetails.intTransfYear
						WHERE
						ware_storestransferheader.intRequisitionNo =  tb1.intRequisitionNo AND
						ware_storestransferheader.intRequisitionYear =  tb1.intRequisitionYear AND
						ware_storestransferdetails.intItemId =  tb1.intItemId AND
						ware_storestransferheader.intStatus > '0' AND 
						ware_storestransferheader.intStatus <= ware_storestransferheader.intApproveLevels),0) as dblTransferQty, 
						IFNULL((SELECT
						Sum(ware_storestransferdetails.dblExcessQty)
						FROM
						ware_storestransferheader
						Inner Join ware_storestransferdetails ON ware_storestransferheader.intTransfNo = ware_storestransferdetails.intTransfNo AND ware_storestransferheader.intTransfYear = ware_storestransferdetails.intTransfYear
						WHERE
						ware_storestransferheader.intRequisitionNo =  tb1.intRequisitionNo AND
						ware_storestransferheader.intRequisitionYear =  tb1.intRequisitionYear AND
						ware_storestransferdetails.intItemId =  tb1.intItemId AND
						ware_storestransferheader.intStatus > '0' AND 
						ware_storestransferheader.intStatus <= ware_storestransferheader.intApproveLevels),0) as dblExTransferQty  
						FROM
						ware_storesrequesitiondetails as tb1 
						Inner Join ware_storesrequesitionheader ON tb1.intRequisitionNo = ware_storesrequesitionheader.intRequisitionNo 
						AND tb1.intRequisitionYear = ware_storesrequesitionheader.intRequisitionYear
						Inner Join mst_item ON tb1.intItemId = mst_item.intId
						Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
						Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
						WHERE 
						ware_storesrequesitionheader.intStatus = '1' AND 
						ware_storesrequesitionheader.intRequisitionNo =  '$SRNNo' AND 
						ware_storesrequesitionheader.intRequisitionYear =  '$SRNYear' AND 
						tb1.intItemId =  '$itemId' ";
						
			$resultsc 		= $db->RunQuery2($sqlChk);
			$rowc 			= mysqli_fetch_array($resultsc);
			
			$orderNos			= $rowc['strOrderNos'];
			$orderYear			= $rowc['intOrderYear'];
			$itemID				= $rowc['itemID'];
			$item				= $rowc['itemName'];
			$srnQty 			= ceil($rowc['dblQty']*100)/100;
			$srnExQty 			= ceil($rowc['dblExQty']*100)/100;
			$transfered_ex		= ceil($rowc['dblExTransferQty']*100)/100;
			$transfered_qty		= ceil($rowc['dblTransferQty']*100)/100;
			$stores				= $rowc['stores'];
			$req_by_stores		= $rowc['req_by_stores'];
			
			
			if($status <= $levels){
				if(($transfered_ex+$transfered_qty)-($transferExQty+$transferQty) >= $srnQty ){
					$rollBackFlag	 =1;
					$rollBackMsg 	.="Already transferd the SRN Qty";
				}
				$balQtyToTransf		= $srnQty - $transfered_qty + $transferQty;
				$save_trasfer_qty 	= $balQtyToTransf;
				if($transferQty<$balQtyToTransf)
				$save_trasfer_qty 	= $transferQty;
				
				$balQtyToTransEx	= $srnExQty - $transfered_ex + $transferExQty;
				$save_ex_qty		= $balQtyToTransEx;
				if($transferExQty < $balQtyToTransEx)
				$save_trasfer_qty 	= $transferQty;
			}
			else{
				if(($transfered_ex+$transfered_qty) >= $srnQty ){
					$rollBackFlag	 =1;
					$rollBackMsg 	.="Already transferd the SRN Qty";
				}
				$balQtyToTransf		= $srnQty - $transfered_qty;
				$save_trasfer_qty 	= $balQtyToTransf;
				if($transferQty<$balQtyToTransf)
				$save_trasfer_qty 	= $transferQty;
				
				$balQtyToTransEx	= $srnExQty - $transfered_ex;
				$save_ex_qty		= $balQtyToTransEx;
				if($transferExQty < $balQtyToTransEx)
				$save_trasfer_qty 	= $transferQty;
			}

			//normal srn qty
			if($balQtyToTransf > 0){
 				$stockBalQty_style			= ceil(getStockBalance($stores,$orderYear,$orderNos,$itemId)*100)/100;
				$stock_excess_color_room	= ceil(getExcess_color_stock($req_by_stores,$itemId)*100)/100;
				$stockBalQty				= $stockBalQty_style + $stock_excess_color_room;

				if($save_trasfer_qty > $stockBalQty){
					$rollBackFlag	 =1;
					$rollBackMsg 	.="No style stock balance to transfer SRN Qty ".$item.":".$save_trasfer_qty ."> ".$stockBalQty;
				}
				else{ 
					if($save_trasfer_qty > $stock_excess_color_room){
						$save_trasfer_ex_color_room_qty = $stock_excess_color_room;
						$save_trasfer_qty				= $save_trasfer_qty-$save_trasfer_ex_color_room_qty;
					}
					else{
						$save_trasfer_ex_color_room_qty = $save_trasfer_qty;
						$save_trasfer_qty        		= 0;
					}
						//$save_trasfer_qty					= $save_trasfer_qty - $save_trasfer_ex_color_room_qty;
				}
				
			}
			//excess srn qty
			$save_ex_qty	= $balQtyToTransEx;
			$stockBalQty 	= ceil(getStockBalance_bulk($stores,$itemId)*100)/100;
			if($save_ex_qty>$stockBalQty){
				$rollBackFlag=1;
				$rollBackMsg .="No bulk stock balance to transfer extra Qty";
			}
			else{ 
				if($save_ex_qty > $stockBalQty){
					$save_trasfer_ex_qty	= $stockBalQty;
				}
				else{
					$save_trasfer_ex_qty 	= $save_ex_qty;
				}
			}
			
			//echo $save_trasfer_qty;
			//echo $save_trasfer_ex_color_room_qty;
			//echo $save_trasfer_ex_qty;
			
			$tot_save_trasfer_qty		= $save_trasfer_qty+$save_trasfer_ex_color_room_qty;
			$tot_save_trasfer_ex_qty	= $save_trasfer_ex_qty;
			$tot_save_qty				= $tot_save_trasfer_qty+$tot_save_trasfer_ex_qty;
		
			$arr_srn[$itemId]			= $transferQty; //from style stock
			$arr_srn_ex[$itemId]		= $transferExQty; //from style stock
			
			$arr_srn_s[$itemId]			= $save_trasfer_qty; //from style stock
			$arr_srn_ex_b[$itemId]		= $save_trasfer_ex_qty;//from bulk stock
			$arr_srn_ex_c[$itemId]		= $save_trasfer_ex_color_room_qty;//from color room stock
			$tot_save_trasfer_qty		= $save_trasfer_qty+$save_trasfer_ex_color_room_qty;
			$tot_save_trasfer_ex_qty	= $save_trasfer_ex_qty;
			$tot_save_qty				= $tot_save_trasfer_qty+$tot_save_trasfer_ex_qty;
			
			//---------------------------
			$toSave_s=$arr_srn_s[$itemId];
 			//while($toSave_s > 0){
				$resultG1 	= get_order_wise_qty_style($location,$orderNos,$orderYear,$toSave_s,$itemId);
				while($rowG1=mysqli_fetch_array($resultG1))
				{
					$orderNo	= $rowG1['orderNo'];
					$salesOrder	= $rowG1['salesOrder'];
					$year		= $rowG1['year'];
					//echo $itemId.":".$Qty		= $rowG1['qty'];
					$stockBalQty_style=0;
					$resultG 		   = ceil(getGrnWiseStockBalance($location,$orderNo,$year,$salesOrder,$itemId)*100)/100;
					while($rowG=mysqli_fetch_array($resultG))
					{
						$stockBalQty_style += ceil($rowG['stockBal']*100)/100;
						
					}
					if($Qty > $stockBalQty_style){
						$toSave_s=$toSave_s -$stockBalQty_style; 
					}
					else{
						$toSave_s=$toSave_s -$Qty; 
					}
					//echo $Qty.' > '.$stockBalQty_style;
					if($Qty > $stockBalQty_style){
						//$rollBackFlag=1;
						//$saveMsg .="No style stock balance to transfer Qty"."Order No:".$orderNo." Year: ".$orderYear." SalesOrder: ".$salesOrder." Item : ".$item.'</br></br>';
					}
					
				}
		//	}
			
			$stockBalQty = getStockBalance_bulk($stores,$itemId);
			if($arr_srn_ex_b[$itemId] > $stockBalQty){
				//$rollBackFlag=1;
				$saveMsg .="No Bulk stock balance to transfer Qty"." Item : ".$item.'</br></br>';
			}
			
			
			$toSave_c=$arr_srn_ex_c[$itemId];
 			//while($toSave_c > 0){
				$stockBalQty_color	= ceil(getExcess_color_stock($stores,$itemId)*100)/100;
				$resultG1 			= ceil(get_order_wise_qty($orderNos,$orderYear,$toSave_c)*100)/100;
				while($rowG1=mysqli_fetch_array($resultG1))
				{
					$orderNo	= $rowG1['orderNo'];
					$salesOrder	= $rowG1['salesOrder'];
					$year		= $rowG1['year'];
					$Qty		= $rowG1['qty'];
					$stockBalQty_c=0;
					$sQty	=0;
					$resultG 	= getGrnWiseStockBalance_color_room($location,$itemId);
					while($rowG=mysqli_fetch_array($resultG))
					{
						$stockBalQty_c += ceil($rowG['stockBal']*100)/100-$sQty;
					}
					if($Qty > $stockBalQty_c){
						$toSave_c=$toSave_c -$stockBalQty_c; 
					}
					else{
						$toSave_c=$toSave_c -$Qty; 
					}
					if($Qty > $stockBalQty_c){
						//$rollBackFlag=1;
						$saveMsg .="No color stock balance to transfer Qty"."Order No:".$orderNo." Year: ".$orderYear." SalesOrder: ".$salesOrder." Item : ".$item.'</br></br>';
					}
					else{
						$sQty = $Qty;
					}
				}
			//}
 			//----------------------
		
 		
			if(($rollBackFlag != 1) && ($status == ($levels+1)))
			{
				$sqlUpd = " UPDATE ware_storesrequesitiondetails 
							SET
							dblTransferQty = dblTransferQty+$tot_save_trasfer_qty,
							dblExTransferQty = dblExTransferQty+$tot_save_trasfer_ex_qty 
							WHERE
							intRequisitionNo = '$SRNNo' AND 
							intRequisitionYear = '$SRNYear' AND 
							intItemId = '$itemId' ";
				$resultUpd = $db->RunQuery2($sqlUpd);
				if(!$resultUpd)
				{
					$rollBackFlag 	= 1;
					$rollBackMsg	= $db->errormsg;
					$rollBackSql	= $sqlUpd;
				}
			}
		}
	//}
	
	if($rollBackFlag != 1){
		
	$sqlUpd = "UPDATE ware_storestransferheader 
				SET
				intStatus = intStatus-1
				WHERE
				intTransfNo = '$transfNo' AND 
				intTransfYear = '$year' ";
	$resultUpd = $db->RunQuery2($sqlUpd);
	if($resultUpd)
	{
		$sql = "SELECT intStatus, intApproveLevels 
				FROM ware_storestransferheader 
				WHERE intTransfNo = '$transfNo' AND 
				intTransfYear = '$year' ";
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		
		$approveLevel = $row['intApproveLevels'];
		$updStatus    = $row['intStatus'];
		$newApprLevel = ($approveLevel+1)-$updStatus;
		
		$sqlIns = "INSERT INTO ware_storestransferheader_approvedby 
					(
					intTransfNo, 
					intYear, 
					intApproveLevelNo, 
					intApproveUser, 
					dtApprovedDate, 
					intStatus
					)
					VALUES
					(
					'$transfNo', 
					'$year', 
					'$newApprLevel', 
					'$userId', 
					now(), 
					0
					) ";
		$resultIns = $db->RunQuery2($sqlIns);
		if($resultIns)
		{
			//$rollBackMsg			= "Approval raised sucessfully";
		}
		else
		{
			$rollBackFlag			= 1;
			$rollBackMsg			= $db->errormsg;
			$rollBackSql			= $sqlIns;

		}
		
		if($updStatus==1)
		{
			$sql = "SELECT 
	  				mst_item.strName as itemName, 
					STD.intItemId,
					STD.dblQty,
					STD.dblExcessQty,
					STD.intTransfNo,
					STD.intTransfYear, 
					STH.intRequisitionNo,
					STH.intRequisitionYear,
					/* SRH.intReqFromStores, */
					SRH.intReqToStores as stores,
					SRH.intReqFromStores as req_by_stores,
					SRD.dblQty as SRNQty,
					SRD.dblExQty as SRNExcess 
					FROM
					ware_storestransferdetails STD
					INNER JOIN ware_storestransferheader STH ON STD.intTransfNo = STH.intTransfNo AND 
					STD.intTransfYear = STH.intTransfYear
					INNER JOIN ware_storesrequesitionheader SRH ON STH.intRequisitionNo = SRH.intRequisitionNo AND 
					STH.intRequisitionYear = SRH.intRequisitionYear
					INNER JOIN ware_storesrequesitiondetails SRD ON SRH.intRequisitionNo = SRD.intRequisitionNo AND SRH.intRequisitionYear = SRD.intRequisitionYear AND STD.intItemId = SRD.intItemId
					INNER JOIN mst_item ON mst_item.intId = STD.intItemId
					WHERE
					STD.intTransfNo = '$transfNo' AND
					STD.intTransfYear = '$year' ";
			
			$result 		= $db->RunQuery2($sql);
			$toSavedQty		= 0;
			$transSavedQty	= 0;
			$transSaved_n	= 0;
			$transSaved_x	= 0;
			$transSaved_c	= 0;
			while($row=mysqli_fetch_array($result))
			{
				//--style--------------------------
				$toSavedQty+=round(($row['dblQty']+$row['dblExcessQty']),4);
				$item 		= $row['intItemId'];
				$itemName	= $row['itemName'];
				$Qty	 	= $arr_srn_s[$item];
				$fromStores = $row['req_by_stores'];
				
				$toSave_s=$arr_srn_s[$item];
				$totSaves_s=0;
			//	while($toSave_s > 0){
					$resultG1 	= get_order_wise_qty_style($location,$orderNos,$orderYear,$Qty,$item);
					while($rowG1=mysqli_fetch_array($resultG1))
					{
						$orderNo	= $rowG1['orderNo'];
						$salesOrder	= $rowG1['salesOrder'];
						$year		= $rowG1['year'];
						$Qty		= ceil($rowG1['qty']*100)/100;
						$resultG 	= getGrnWiseStockBalance($location,$orderNo,$year,$salesOrder,$item);
						while($rowG=mysqli_fetch_array($resultG))
						{
							$stockB = ceil($rowG['stockBal']*100)/100;
							if(($Qty>0) && ($stockB>0))
							{
								if($Qty<=$stockB)
								{
									$saveQty = $Qty;
									$Qty	 = 0;
								}
								else if($Qty>$stockB)
								{
									$saveQty = $stockB;
									$Qty	 = $Qty-$saveQty;
								}
								$grnNo		 = $rowG['intGRNNo'];
								$grnYear	 = $rowG['intGRNYear'];
								$grnDate	 = $rowG['dtGRNDate'];
								$grnRate	 = $rowG['dblGRNRate'];	
								$currency	 = loadCurrency($grnNo,$grnYear,$item);
								$saveQty	 = round($saveQty,4);		
								if($saveQty>0)
								{	
									$sqlI 	 = "INSERT INTO `ware_stocktransactions` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intOrderNo,intOrderYear,intSalesOrderId,intItemId,dblQty,strType,intUser,dtDate) 
								VALUES ('$company','$location','$transfNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$orderNo','$orderYear','$salesOrder','$item','-$saveQty','STORESTRANSFER','$userId',now())";
									$resultI = $db->RunQuery2($sqlI);
									if($resultI)
									{
										$sqlJ 	 = "INSERT INTO `ware_sub_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,intSubStores,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intOrderNo,intOrderYear,intSalesOrderId,intItemId,dblQty,strType,intUser,dtDate) 
									VALUES ('$company','$location','$fromStores','$transfNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$orderNo','$orderYear','$salesOrder','$item','$saveQty','STORESTRANSFER','$userId',now())";
										$resultJ = $db->RunQuery2($sqlJ);
										if($resultJ)
										{
											$transSavedQty +=$saveQty;
											$transSaved_n +=$saveQty;
										}
										else{
											$rollBackSql	=	$sqlJ;
										}
									}
									else{
										$rollBackSql	=	$sqlI;
									}
									if((!$resultI) || (!$resultJ))
									{
										$rollBackFlag 	= 1;
										$sqlM			= $sqlI;
										$rollBackMsg 	= $db->errormsg;
									}
								}
							}
						}//end of while stock bal 
						$toSave_s=$toSave_s -$transSaved_n; 
						if($Qty>$transSaved_n){
							//$rollBackFlag=1;
							$saveMsg .="No style stock balance to transfer Qty "."Order No:".$orderNo." Year: ".$orderYear." SalesOrder: ".$salesOrder." Item : ".$itemName.'=>'.$Qty.' > '.$transSaved_n.'</br></br>';
						}
	
					}
				//}

				//---bulk-------------------------------------
 				$item 		= $row['intItemId'];
				$Qty	 	= $arr_srn_ex_b[$item];
				$fromStores = $row['req_by_stores'];
 				
				$resultG 	= getGrnWiseStockBalance_bulk($location,$item);
				while($rowG=mysqli_fetch_array($resultG))
				{
					$stockB = ceil($rowG['stockBal']*100)/100;
					if(($Qty>0) && ($stockB>0))
					{
						if($Qty<=$stockB)
						{
							$saveQty = $Qty;
							$Qty	 = 0;
						}
						else if($Qty>$stockB)
						{
							$saveQty = $stockB;
							$Qty	 = $Qty-$saveQty;
						}
						$grnNo		 = $rowG['intGRNNo'];
						$grnYear	 = $rowG['intGRNYear'];
						$grnDate	 = $rowG['dtGRNDate'];
						$grnRate	 = $rowG['dblGRNRate'];	
						$currency	 = loadCurrency($grnNo,$grnYear,$item);
						$saveQty	 = round($saveQty,4);		
						if($saveQty>0)
						{	
							$sqlI 	 = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intOrderNo,intOrderYear,intSalesOrderId,intItemId,dblQty,strType,intUser,dtDate) 
						VALUES ('$company','$location','$transfNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$orderNo','$orderYear','$salesOrder','$item','-$saveQty','STORESTRANSFER','$userId',now())";
							$resultI = $db->RunQuery2($sqlI);
							if($resultI)
							{
								$sqlJ 	 = "INSERT INTO `ware_sub_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,intSubStores,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,/*intOrderNo,intOrderYear,intSalesOrderId,*/intItemId,dblQty,strType,intUser,dtDate) 
							VALUES ('$company','$location','$fromStores','$transfNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency',/*'$orderNo','$orderYear','$salesOrder',*/'$item','$saveQty','STORESTRANSFER','$userId',now())";
								$resultJ = $db->RunQuery2($sqlJ);
								if($resultJ)
								{
									$transSavedQty+=$saveQty;
									$transSaved_x+=$saveQty;

								}
								else{
									$rollBackSql	=	$sqlJ;
								}
							}
							else{
								$rollBackSql	=	$sqlI;
							}
							if((!$resultI) || (!$resultJ))
							{
								$rollBackFlag 	= 1;
								$sqlM			= $sqlI;
								$rollBackMsg 	= $db->errormsg;
							}
						}
					}//end of while stock bal 
					if($Qty>$transSaved_x){
						//$rollBackFlag=1;
						$saveMsg .="No bulk stock balance to transfer extra Qty"." Item : ".$itemName.'</br></br>';
					}
				}
				
 				//----color room--------------------------------------
 				$item 		= $row['intItemId'];
				$Qty	 	= $arr_srn_ex_c[$item];
				$fromStores = $row['req_by_stores'];
				
				$toSave_c=$arr_srn_s[$item];
				$totSaves_cs=0;
				//while($toSave_c > 0){
				$resultG1 	= get_order_wise_qty($orderNos,$orderYear,$Qty);
				while($rowG1=mysqli_fetch_array($resultG1))
				{
					$orderNo	= $rowG1['orderNo'];
					$salesOrder	= $rowG1['salesOrder'];
					$year		= $rowG1['year'];
					$Qty		= $rowG1['qty'];
					$sval		= 0;
					$resultG 	= getGrnWiseStockBalance_color_room($location,$item);
					while($rowG=mysqli_fetch_array($resultG))
					{
					$stockB = $rowG['stockBal']-$sval;
						if(($Qty>0) && ($stockB>0))
						{
							if($Qty<=$stockB)
							{
								$saveQty = $Qty;
								$Qty	 = 0;
							}
							else if($Qty>$stockB)
							{
								$saveQty = $stockB;
								$Qty	 = $Qty-$saveQty;
							}
							$grnNo		 = $rowG['intGRNNo'];
							$grnYear	 = $rowG['intGRNYear'];
							$grnDate	 = $rowG['dtGRNDate'];
							$grnRate	 = $rowG['dblGRNRate'];	
							$currency	 = loadCurrency($grnNo,$grnYear,$item);
							$saveQty	 = round($saveQty,4);		
							if($saveQty>0)
							{	
								$sqlI 	 = "INSERT INTO `ware_sub_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,intSubStores,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
							VALUES ('$company','$location','$fromStores','$transfNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','-$saveQty','STORESTRANSFER','$userId',now())";
								$resultI = $db->RunQuery2($sqlI);
								if($resultI)
								{
									$sqlJ 	 = "INSERT INTO `ware_sub_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,intSubStores,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intOrderNo,intOrderYear,intSalesOrderId,intItemId,dblQty,strType,intUser,dtDate) 
								VALUES ('$company','$location','$fromStores','$transfNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$orderNo','$orderYear','$salesOrder','$item','$saveQty','STORESTRANSFER','$userId',now())";
									$resultJ = $db->RunQuery2($sqlJ);
									if($resultJ)
									{
										$transSavedQty	+=	$saveQty;
										$transSaved_c	+=	$saveQty;
									}
									else{
										$rollBackSql	=	$sqlJ;
									}
								}
								else{
									$rollBackSql	=	$sqlI;
								}
								if((!$resultI) || (!$resultJ))
								{
									$rollBackFlag 	= 1;
									$sqlM			= $sqlI;
									$rollBackMsg 	= $db->errormsg;
								}
							}
						}
						if($Qty>$transSaved_c){
							//$rollBackFlag=1;
							$saveMsg .="No color room stock balance to transfer extra Qty"." Item : ".$itemName.'</br></br>';
						}
						else{
							$sval	=	$Qty;
						}
					}//end of while stock bal 
						$toSave_c=$toSave_c -$transSaved_c; 
 				}
				//}
				
				//------------------------
				
				
			}
		}
	}
	else
	{
		$rollBackFlag 	= 1;
		$rollBackMsg	= $db->errormsg;
	}
	}
	//echo $rollBackFlag;
	//$rollBackFlag=1;
	
 	if(($toSavedQty!=$transSavedQty) && ($rollBackFlag!=1))
	{
 		//$rollBackFlag	= 1;
		//$saveMsg 	= "Approval Error".$toSavedQty.'!='.$transSavedQty;
		$rollBackSql   = $toSavedQty."-".$transSavedQty."-".$transSaved_n."-".$transSaved_x."-".$transSaved_c;
	}
	
	if($rollBackFlag==1)
	{
 		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
		$response['sql'] 		= $rollBackSql;
	}
	else if(($rollBackFlag==0) &&($status==1))
	{
		sendFinlConfirmedEmailToUser($transfNo,$year,$objMail,$mainPath,$root_path);
  		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		$response['msg'] 		= "Final Approval raised sucessfully".'</br></br>'.$saveMsg;
	}
	else if($rollBackFlag==0)
	{	
 		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		$response['msg'] 		= "Approved sucessfully".'</br></br>'.$saveMsg;
	}
	else
	{
 		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= "Approval fail";
	}
	
	$db->CloseConnection();
	echo json_encode($response);
}
	else if($requestType=='reject')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
		//$grnApproveLevel = (int)getApproveLevel($programName);
		$sql 	= "SELECT intApproveLevels,intStatus FROM ware_storestransferheader WHERE intTransfNo='$transfNo' AND intTransfYear='$year' ";
		$results = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($results);
		$createdUser = $row['intUser'];
		$status = $row['intStatus'];
		$savedLevels = $row['intApproveLevels'];
		$location 	= $row['location'];
		$company = $row['companyId'];
		
		$sql = "UPDATE `ware_storestransferheader` SET `intStatus`=0 WHERE (intTransfNo='$transfNo') AND (`intTransfYear`='$year')";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Rejection error!";
		}
		
		$sqlI = "INSERT INTO `ware_storestransferheader_approvedby` (`intTransfNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$transfNo','$year','0','$userId',now(),0)";
		$resultI = $db->RunQuery2($sqlI);
		if((!$resultI) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sqlI;
			$rollBackMsg = "Rejection error!";
		}
		if($resultI)
		{
			if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
			sendRejecedEmailToUser($transfNo,$year,$objMail,$mainPath,$root_path);
			}
		}
		
		//---------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------
			if($savedLevels>=$status){//if atleast one confirmation processed, roll back return to saupplier qty from grn table-
				$sql1 = "SELECT
						ware_storestransferdetails.intItemId,
						ware_storestransferdetails.dblQty,
						ware_storestransferdetails.dblExcessQty,
						ware_storestransferheader.intRequisitionNo,
						ware_storestransferheader.intRequisitionYear
						FROM
						ware_storestransferdetails 
						INNER JOIN ware_storestransferheader ON ware_storestransferdetails.intTransfNo = ware_storestransferheader.intTransfNo AND ware_storestransferdetails.intTransfYear = ware_storestransferheader.intTransfYear
 						WHERE
						ware_storestransferdetails.intTransfNo =  '$transfNo' AND
						ware_storestransferdetails.intTransfYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$reqNo		= $row['intRequisitionNo'];
					$reqYear	= $row['intRequisitionYear'];
					$item		= $row['intItemId'];
					$Qty		= $row['dblQty'];
					$exQty		= $row['dblExcessQty'];
					
					$sql = "UPDATE `ware_storesrequesitiondetails` 
							SET 	
							dblTransferQty = dblTransferQty-'$Qty' ,
							dblExTransferQty = dblExTransferQty-'$exQty' 
							WHERE
							ware_storesrequesitiondetails.intRequisitionNo 		=  '$reqNo' AND
							ware_storesrequesitiondetails.intRequisitionYear 	=  '$reqYear' AND
							ware_storesrequesitiondetails.intItemId =  '$item' ";
					$result = $db->RunQuery2($sql);
					if((!$result) && ($rollBackFlag!=1)){
						$rollBackFlag	= 1;
						$sqlM			= $sql;
						$rollBackMsg 	= "Rejection error!";
					}
				}
			}
			
		//------------------------------------
		if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Rejection raised sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		//----------------------------------------
			echo json_encode($response);
		//-----------------------------------------	
		
	}


function getStockBalance_bulk($stores,$item)
{
	global $db;
	   $sql = "SELECT 
	  		mst_item.strName as itemName, 
			Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
			FROM ware_stocktransactions_bulk 
			INNER JOIN mst_item ON mst_item.intId = ware_stocktransactions_bulk.intItemId

			WHERE
			ware_stocktransactions_bulk.intItemId =  '$item' AND 
			ware_stocktransactions_bulk.intLocationId = (SELECT intLocation FROM mst_substores WHERE intId='$stores')
			GROUP BY
			ware_stocktransactions_bulk.intItemId";

	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	return val($row['stockBal']);	
}

function getStockBalance($stores,$orderYear,$orderNos,$item)
{
	global $db;
	 $sql = "SELECT 
	  		mst_item.strName as itemName, 
			Sum(ware_stocktransactions.dblQty) as stockBal 
			FROM ware_stocktransactions
			INNER JOIN mst_item ON mst_item.intId = ware_stocktransactions.intItemId
			WHERE
			ware_stocktransactions.intOrderYear = '$orderYear' AND 
			FIND_IN_SET(ware_stocktransactions.intOrderNo,'$orderNos') AND 
			ware_stocktransactions.intLocationId = (SELECT intLocation FROM mst_substores WHERE intId='$stores') AND
			ware_stocktransactions.intItemId =  '$item'";

	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	return val($row['stockBal']);	
}

	//--------------------------------------------------------------
	function getExcess_color_stock($stores,$item)
	{
		global $db;
		   $sql = "SELECT 
	  				mst_item.strName as itemName, 
					SUM(ware_sub_stocktransactions_bulk.dblQty) AS stockBal
					FROM ware_sub_stocktransactions_bulk
					INNER JOIN mst_item ON mst_item.intId = ware_sub_stocktransactions_bulk.intItemId
					WHERE
					ware_sub_stocktransactions_bulk.intItemId =  '$item' AND 
					ware_sub_stocktransactions_bulk.intLocationId = (SELECT intLocation FROM mst_substores WHERE intId='$stores') AND 
					ware_sub_stocktransactions_bulk.intOrderNo IS NULL 
					GROUP BY
					ware_sub_stocktransactions_bulk.intItemId";
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------


function getGrnWiseStockBalance_bulk($location,$item)
{
	global $db;
	  $sql = "SELECT 
	  		mst_item.strName as itemName, 
			Sum(ware_stocktransactions_bulk.dblQty) AS stockBal,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear,
			ware_stocktransactions_bulk.dtGRNDate,
			ware_stocktransactions_bulk.dblGRNRate
			FROM ware_stocktransactions_bulk 
			INNER JOIN mst_item ON mst_item.intId = ware_stocktransactions_bulk.intItemId
 			WHERE
			ware_stocktransactions_bulk.intItemId =  '$item' AND
			ware_stocktransactions_bulk.intLocationId =  '$location'
			GROUP BY
			ware_stocktransactions_bulk.intItemId,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear, 
			ware_stocktransactions_bulk.dblGRNRate 
			ORDER BY
			ware_stocktransactions_bulk.dtGRNDate ASC";

	$result = $db->RunQuery2($sql);
	return $result;	
}

//--------------------------------------------------------------
function getGrnWiseStockBalance($location,$orderNo,$orderYear,$salesOrderId,$item)
{
	global $db;
	 	  $sql = "SELECT 
	  		mst_item.strName as itemName, 
			Sum(ware_stocktransactions.dblQty) AS stockBal,
			ware_stocktransactions.intGRNNo,
			ware_stocktransactions.intGRNYear,
			ware_stocktransactions.dtGRNDate,
			ware_stocktransactions.dblGRNRate  
			FROM ware_stocktransactions  
			INNER JOIN mst_item ON mst_item.intId = ware_stocktransactions.intItemId
			WHERE
			ware_stocktransactions.intItemId =  '$item' AND
			ware_stocktransactions.intLocationId =  '$location' AND 
			ware_stocktransactions.intOrderNo='$orderNo' AND 
			ware_stocktransactions.intOrderYear='$orderYear' AND 
			ware_stocktransactions.intSalesOrderId='$salesOrderId' 
			GROUP BY
			ware_stocktransactions.intItemId,
			ware_stocktransactions.intGRNNo,
			ware_stocktransactions.intGRNYear, 
			ware_stocktransactions.dblGRNRate  
			ORDER BY
			ware_stocktransactions.dtGRNDate ASC";

	$result = $db->RunQuery2($sql);
	return $result;	
}
//--------------------------------------------------------------
function getGrnWiseStockBalance_color_room($location,$item){
	
	global $db;
	  $sql = "SELECT 
	  		mst_item.strName as itemName, 
			Sum(ware_sub_stocktransactions_bulk.dblQty) AS stockBal,
			ware_sub_stocktransactions_bulk.intGRNNo,
			ware_sub_stocktransactions_bulk.intGRNYear,
			ware_sub_stocktransactions_bulk.dtGRNDate,
			ware_sub_stocktransactions_bulk.dblGRNRate
			FROM ware_sub_stocktransactions_bulk
			INNER JOIN mst_item ON mst_item.intId = ware_sub_stocktransactions_bulk.intItemId
			WHERE
			ware_sub_stocktransactions_bulk.intItemId =  '$item' AND
			ware_sub_stocktransactions_bulk.intLocationId =  '$location' AND 
			ware_sub_stocktransactions_bulk.intOrderNo IS NULL 
			GROUP BY
			ware_sub_stocktransactions_bulk.intItemId,
			ware_sub_stocktransactions_bulk.intGRNNo,
			ware_sub_stocktransactions_bulk.intGRNYear, 
			ware_sub_stocktransactions_bulk.dblGRNRate 
			ORDER BY
			ware_sub_stocktransactions_bulk.dtGRNDate ASC";

	$result = $db->RunQuery2($sql);
	return $result;	
	
}
//--------------------------------------------------------------
function loadCurrency($grnNo,$grnYear,$item)
{
	global $db;
	
	if(($grnNo!=0) && ($grnYear!=0))
	{
		$sql = "SELECT
				trn_poheader.intCurrency as currency 
				FROM
				ware_grnheader
				Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
				WHERE
				ware_grnheader.intGrnNo =  '$grnNo' AND
				ware_grnheader.intGrnYear =  '$grnYear'";
	}
	else
	{
		$sql = "SELECT
				mst_item.intCurrency as currency 
				FROM mst_item
				WHERE
				mst_item.intId =  '$item'";
	}
	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
	return $row['currency'];	
}
function get_order_wise_qty($orderNos,$orderYear,$Qty){
	
	global $db;

	$sql	="SELECT
			sum(trn_orderdetails.intQty) as qty 
			FROM `trn_orderdetails`
			WHERE
			trn_orderdetails.intOrderYear = '$orderYear' AND 
			FIND_IN_SET(intOrderNo,'$orderNos')
			";
	$result = $db->RunQuery2($sql);
	$row	=mysqli_fetch_array($result);
	$tot	= $row['qty'];	
	
	$sql	="SELECT
			trn_orderdetails.intOrderNo as orderNo,
			trn_orderdetails.intOrderYear as year,
			trn_orderdetails.intSalesOrderId as salesOrder,
			(trn_orderdetails.intQty/$tot*$Qty) as qty 
			FROM `trn_orderdetails`
			WHERE
			trn_orderdetails.intOrderYear = '$orderYear' AND 
			FIND_IN_SET(intOrderNo,'$orderNos')
			GROUP BY
			trn_orderdetails.intOrderNo,
			trn_orderdetails.intOrderYear,
			trn_orderdetails.intSalesOrderId";
	$result = $db->RunQuery2($sql);
	//$row	=mysqli_fetch_array($result);
	return $result;
}
function get_order_wise_qty_style($location,$orderNos,$orderYear,$Qty,$item){
	
	global $db;

	   $sql	="
	  SELECT sum(TB1.qty) as qty FROM (
	  SELECT 
			(SELECT
			Sum(ware_stocktransactions.dblQty)
			FROM `ware_stocktransactions`
			WHERE
			ware_stocktransactions.intLocationId = '$location' AND
			ware_stocktransactions.intOrderNo = trn_orderdetails.intOrderNo AND
			ware_stocktransactions.intOrderYear = trn_orderdetails.intOrderYear AND
			ware_stocktransactions.intSalesOrderId = trn_orderdetails.intSalesOrderId AND 
			ware_stocktransactions.intItemId=$item) as stk,	
			trn_orderdetails.intOrderNo as orderNo,
			trn_orderdetails.intOrderYear as year,
			trn_orderdetails.intSalesOrderId as salesOrder,
			(trn_orderdetails.intQty) as qty 
			FROM `trn_orderdetails`
			WHERE
			trn_orderdetails.intOrderYear = '$orderYear' AND 
			FIND_IN_SET(intOrderNo,'$orderNos')
			GROUP BY
			trn_orderdetails.intOrderNo,
			trn_orderdetails.intOrderYear,
			trn_orderdetails.intSalesOrderId 
			having stk > 0 ) as TB1";
 	$result = $db->RunQuery2($sql);
	$row	=mysqli_fetch_array($result);
	$tot	= $row['qty'];	
	
	      $sql	="SELECT 
			(SELECT
			Sum(ware_stocktransactions.dblQty)
			FROM `ware_stocktransactions`
			WHERE
			ware_stocktransactions.intLocationId = '$location' AND
			ware_stocktransactions.intOrderNo = trn_orderdetails.intOrderNo AND
			ware_stocktransactions.intOrderYear = trn_orderdetails.intOrderYear AND
			ware_stocktransactions.intSalesOrderId = trn_orderdetails.intSalesOrderId AND 
			ware_stocktransactions.intItemId=$item) as stk,	
			trn_orderdetails.intOrderNo as orderNo,
			trn_orderdetails.intOrderYear as year,
			trn_orderdetails.intSalesOrderId as salesOrder,
			(trn_orderdetails.intQty/$tot*$Qty) as qty 
			FROM `trn_orderdetails`
			WHERE
			trn_orderdetails.intOrderYear = '$orderYear' AND 
			FIND_IN_SET(intOrderNo,'$orderNos')
			GROUP BY
			trn_orderdetails.intOrderNo,
			trn_orderdetails.intOrderYear,
			trn_orderdetails.intSalesOrderId 
			having stk > 0";
	$result = $db->RunQuery2($sql);
	//$row	=mysqli_fetch_array($result);
	return $result;
}
//--------------------------------------------------------
function sendFinlConfirmedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_storestransferheader
			Inner Join sys_users ON ware_storestransferheader.intUser = sys_users.intUserId
			WHERE
			ware_storestransferheader.intTransfNo =  '$serialNo' AND
			ware_storestransferheader.intTransfYear =  '$year'";

				
			$result = $db->RunQuery2($sql);
		 
			$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="CONFIRMED TRANSFER NOTE('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='TRANSFER NOTE';
			$_REQUEST['field1'] ='TRANSFER No';
			$_REQUEST['field2'] ='TRANSFER Year';
			$_REQUEST['field3'] ='';
			$_REQUEST['field4'] ='';
			$_REQUEST['field5'] ='';
			$_REQUEST['value1'] =$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="CONFIRMED TRANSFER NOTE('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Approved this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/stores_transfer_note/listing/rptStoresTransferNote.php?transfNo=$serialNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
//--------------------------------------------------------
function sendRejecedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_storestransferheader
			Inner Join sys_users ON ware_storestransferheader.intUser = sys_users.intUserId
			WHERE
			ware_storestransferheader.intTransfNo =  '$serialNo' AND
			ware_storestransferheader.intTransfYear =  '$year'";
 				
		 $result = $db->RunQuery2($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="REJECTED TRANSFER NOTE('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='TRANSFER NOTE';
			$_REQUEST['field1']='TRANSFER No';
			$_REQUEST['field2']='TRANSFER Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="REJECTED TRANSFER NOTE ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Rejected this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/stores_transfer_note/listing/rptStoresTransferNote.php?transfNo=$serialNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
?>


 
<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo 	 = $_REQUEST['serialNo'];
	$year 		 = $_REQUEST['Year'];
	$srnNo 	 = $_REQUEST['srnNo'];
	$srnYear 	 = $_REQUEST['srnYear'];
	$note 	 = $_REQUEST['note'];
	$date 		 = $_REQUEST['date'];
	
	$arr 		= json_decode($_REQUEST['arr'], true);
	
	$programName='Stores Transfer Note';
	$programCode='P0258';

	$ApproveLevels = (int)getApproveLevel($programName);
	$transfApproveLevel = $ApproveLevels+1;
	
//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		$savableFlag= 1;
		
		if($serialNo==''){
			$serialNo 	= getNextTransfNo();
			$year = date('Y');
			$editMode=0;
			$savedStatus=$transfApproveLevel;
			$savedLevels=$ApproveLevels;
		}
		else{
			$savableFlag=getSaveStatus($serialNo,$year);
			$editMode=1;
			$sql = "SELECT
			ware_storestransferheader.intStatus, 
			ware_storestransferheader.intApproveLevels , 
			FROM ware_storestransferheader 
			WHERE
			ware_storestransferheader.intTransfNo =  '$serialNo' AND
			ware_storestransferheader.intTransfYear =  '$year'";

			$result = $db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			$savedStatus=$row['intStatus'];
			$savedLevels=$row['intApproveLevels'];
		}
		
		//--------------------------
		//$response['arrData'] = $objwhouseget->getLocationValidation('TRANSFERIN',$companyId,$srnNo,$srnYear);//check for grn location
		//--------------------------
		$editPermission=loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
		$locationEditFlag=getEditLocationValidation('TRANSFERIN',$companyId,$serialNo,$year);
		//--------------------------
		
		if($response['arrData']['type']=='fail'){
			 $rollBackFlag = 1;
			 $rollBackMsg = $response['arrData']['msg'];
		 }
		else if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This Stores Transfer In No is already confirmed.cant edit";
		 }
		 else if($editPermission==0){
			 $rollBackFlag=1;
			 $rollBackMsg="No Permission to save.";
		 }
		else if($editMode==1){//edit existing grn
		$sql = "UPDATE `ware_storestransferheader` SET intStatus ='$transfApproveLevel', 
												       intApproveLevels ='$ApproveLevels', 
												       strNote ='$note', 
													  intModifiedBy ='$userId', 
													  dtmModifiedDate =now() 
				WHERE (`intTransfNo`='$serialNo') AND (`intTransfYear`='$year')";
		$result = $db->RunQuery2($sql);
		}
		else{
			$sql = "INSERT INTO `ware_storestransferheader` (`intTransfNo`,`intTransfYear`,intRequisitionNo,intRequisitionYear,strNote,intStatus,intApproveLevels,datdate,dtmCreateDate,intUser,intCompanyId) 
					VALUES ('$serialNo','$year','$srnNo','$srnYear','$note','$transfApproveLevel','$ApproveLevels',now(),now(),'$userId','$companyId')";
			$result = $db->RunQuery2($sql);
			$maxAppByStatus=(int)getMaxAppByStatus($serialNo,$year)+1;
			$sql = "UPDATE `ware_storestransferheader_approvedby` SET intStatus ='$maxAppByStatus' 
					WHERE (`intTransfNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
			$result1 = $db->RunQuery2($sql);
		}
		//-----------delete and insert to detail table-----------------------
		if(($result) && ($rollBackFlag!=1)){
		$sql = "DELETE FROM `ware_storestransferdetails` WHERE (`intTransfNo`='$serialNo') AND (`intTransfYear`='$year')";
		$result2 = $db->RunQuery2($sql);
			$toSave=0;
			$saved=0;
			$rollBackFlag=0;
			//$rollBackMsg ="Maximum Qtys for items are..."; 
			$rollBackMsg ="";
			foreach($arr as $arrVal)
			{
				$location 	 = $_SESSION['CompanyID'];
				$company 	 = $_SESSION['headCompanyId'];
				$itemId 	 = $arrVal['itemId'];
				$Qty 		 = ceil($arrVal['Qty']*100)/100;
				$transferQty = ceil($arrVal['Qty']*100)/100;
		

				//------check maximum ISSUE Qty--------------------
				 $sqlc = "SELECT 
				mst_item.strName as itemName,
				tb1.intRequisitionNo,
				tb1.intRequisitionYear,
				ware_storesrequesitionheader.intReqToStores as stores,
				ware_storesrequesitionheader.intReqFromStores as req_by_stores,
				IFNULL(tb1.dblQty,0) as dblQty,
				IFNULL(tb1.dblExQty,0) as dblExQty,
				ware_storesrequesitionheader.intOrderYear,
				ware_storesrequesitionheader.strOrderNos,
				tb1.intItemId,
				IFNULL((SELECT
				Sum(ware_storestransferdetails.dblQty)
				FROM
				ware_storestransferheader
				Inner Join ware_storestransferdetails ON ware_storestransferheader.intTransfNo = ware_storestransferdetails.intTransfNo AND ware_storestransferheader.intTransfYear = ware_storestransferdetails.intTransfYear
				WHERE
				ware_storestransferheader.intRequisitionNo =  tb1.intRequisitionNo AND
				ware_storestransferheader.intRequisitionYear =  tb1.intRequisitionYear AND
				ware_storestransferdetails.intItemId =  tb1.intItemId AND
				ware_storestransferheader.intStatus > '0' AND 
				ware_storestransferheader.intStatus <= ware_storestransferheader.intApproveLevels),0) as dblTransferQty
				FROM
				ware_storesrequesitiondetails as tb1 
				Inner Join ware_storesrequesitionheader ON tb1.intRequisitionNo = ware_storesrequesitionheader.intRequisitionNo 
				AND tb1.intRequisitionYear = ware_storesrequesitionheader.intRequisitionYear
				Inner Join mst_item ON tb1.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				WHERE 
				ware_storesrequesitionheader.intStatus = '1' AND 
				ware_storesrequesitionheader.intRequisitionNo =  '$srnNo' AND 
				ware_storesrequesitionheader.intRequisitionYear =  '$srnYear' AND 
				tb1.intItemId =  '$itemId' ";
				
				$resultsc 		= $db->RunQuery2($sqlc);
				$rowc 			= mysqli_fetch_array($resultsc);
				$orderNos		= $rowc['strOrderNos'];
				$orderYear		= $rowc['intOrderYear'];
				$item			= $rowc['itemName'];
				$stores			= $rowc['stores'];
				$req_by_stores	= $rowc['req_by_stores'];
				$itemId 		= $rowc['intItemId'];
				$srnQty 		= ceil($rowc['dblQty']*100)/100;
				$srnExQty 		= ceil($rowc['dblExQty']*100)/100;
				$transfered		= ceil($rowc['dblTransferQty']*100)/100;
				$transferedEx	= ceil($rowc['dblExTransferQty']*100)/100;

				$balQtyToTransf 	= ceil(($srnQty-$transfered)*100)/10;
				$balQtyToExTransf 	= ceil(($srnExQty-$transferedEx)*100)/10;
			
				//normal srn qty
	  			if($balQtyToTransf > 0){
					if($transferQty > $balQtyToTransf){
						$save_trasfer_qty =  $balQtyToTransf;
						$balQty			  =  $transferQty-$balQtyToTransf;
					}
					else{
						$save_trasfer_qty =  $transferQty;
						$balQty			  =0;
					}
 					$stockBalQty_style			= ceil(getStockBalance($stores,$orderYear,$orderNos,$itemId)*100)/100;
					$stock_excess_color_room	= ceil(getExcess_color_stock($req_by_stores,$itemId)*10/10);
<<<<<<< .mine
					$stockBalQty				= ceil($stockBalQty_style + $stock_excess_color_room*100)/100;
=======
					$stockBalQty				= ceil(($stockBalQty_style + $stock_excess_color_room)*10)/10;
>>>>>>> .r1979

					if($save_trasfer_qty > $stockBalQty){
						$rollBackFlag	 =1;
						//$rollBackMsg 	.="No style stock balance to transfer SRN Qty";
						$rollBackMsg .="No style stock balance to transfer Qty"." for Item : ".$item." available balance is ".$stockBalQty.'</br></br>';
					}
					else{ 
						if($save_trasfer_qty > $stock_excess_color_room){
							$save_trasfer_ex_color_room_qty = $stock_excess_color_room;
							$stock_excess_color_room        = 0;
							$stockBalQty_style				= $save_trasfer_qty-$save_trasfer_ex_color_room_qty;
						}
						else{
							$save_trasfer_ex_color_room_qty = $save_trasfer_qty;
							$stock_excess_color_room        = $stock_excess_color_room-$save_trasfer_qty;
 						}
						$save_trasfer_qty					= $save_trasfer_qty - $save_trasfer_ex_color_room_qty;
					}
					
					
 
				}
				//excess srn qty
				$save_ex_qty	= $balQty;
 				$stockBalQty 	= ceil(getStockBalance_bulk($stores,$itemId)*100)/100;
				if($save_ex_qty>$stockBalQty){
					$rollBackFlag=1;
					$rollBackMsg .="No bulk stock balance to transfer extra Qty";
				}
				else{ 
					if($save_ex_qty > $stockBalQty){
						$save_trasfer_ex_qty	= $stockBalQty;
					}
					else{
						$save_trasfer_ex_qty 	= $save_ex_qty;
 					}
				}
 				
			
				$tot_save_trasfer_qty		= $save_trasfer_qty+$save_trasfer_ex_color_room_qty;
				$tot_save_trasfer_ex_qty	= $save_trasfer_ex_qty;
				$tot_save_qty				= $tot_save_trasfer_qty+$tot_save_trasfer_ex_qty;
				//echo $save_trasfer_qty.'+'.$save_trasfer_ex_color_room_qty;
 		
				if((round($transferQty,6) != round($tot_save_qty,6) ) && ($rollBackFlag !=1)){
					$rollBackFlag=1;
					$rollBackMsg .="\n "."-".$item." =".$transferQty.":".$tot_save_qty;
 				}
  
				
 				//----------------------------
				if($rollBackFlag!=1){
					$Qty=round($Qty,6);
					$sql = "INSERT INTO `ware_storestransferdetails` (`intTransfNo`,`intTransfYear`,`intItemId`,`dblQty`,dblExcessQty) 
					VALUES ('$serialNo','$year','$itemId','$tot_save_trasfer_qty','$tot_save_trasfer_ex_qty')";
					$result3 = $db->RunQuery2($sql);
					if($result3==1){
						$saved++;
					}
				}
				$toSave++;
			}
		}
		//echo $rollBackFlag;
		
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($savableFlag==1) &&(!$result)){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Header saving error";
			$response['q'] 			= $sql;
		}
		else if(($savableFlag==1) &&($toSave==0) &&($saved==0)){
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Details saving error";
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$confirmationMode=getConfirmationMode($programCode,$userId,$ApproveLevels,$transfApproveLevel);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
//----------------------------------------
	echo json_encode($response);
//-----------------------------------------	
	function getNextTransfNo()
	{
		global $db;
		global $companyId;
		 $sql = "SELECT
				sys_no.intSTNNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$companyId'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextTransfNo = $row['intSTNNo'];
		
		$sqlUpd = "UPDATE `sys_no` SET intSTNNo=intSTNNo+1 WHERE (`intCompanyId`='$companyId') ";
		$db->RunQuery2($sqlUpd);
		
		return $nextTransfNo;
	}
	//-----------------------------------------------------------
	function getStockBalance($stores,$orderYear,$orderNos,$item)
	{
		global $db;
		 $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intOrderYear = '$orderYear' AND 
				FIND_IN_SET(ware_stocktransactions.intOrderNo,'$orderNos') AND 
 				ware_stocktransactions.intLocationId = (SELECT intLocation FROM mst_substores WHERE intId='$stores') AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($stores,$item)
	{
		global $db;
		   $sql = "SELECT
					SUM(ware_stocktransactions_bulk.dblQty) AS stockBal
					FROM ware_stocktransactions_bulk
					WHERE
					ware_stocktransactions_bulk.intItemId =  '$item' AND 
					ware_stocktransactions_bulk.intLocationId = (SELECT intLocation FROM mst_substores WHERE intId='$stores')
					GROUP BY
					ware_stocktransactions_bulk.intItemId";
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getExcess_color_stock($stores,$item)
	{
		global $db;
		   $sql = "SELECT
					SUM(ware_sub_stocktransactions_bulk.dblQty) AS stockBal
					FROM ware_sub_stocktransactions_bulk
					WHERE
					ware_sub_stocktransactions_bulk.intItemId =  '$item' AND 
					ware_sub_stocktransactions_bulk.intLocationId = (SELECT intLocation FROM mst_substores WHERE intId='$stores') AND 
					ware_sub_stocktransactions_bulk.intOrderNo IS NULL 
					GROUP BY
					ware_sub_stocktransactions_bulk.intItemId";
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getSaveStatus($serialNo,$year)
	{
		global $db;
		$sql = "SELECT ware_storestransferheader.intStatus, ware_storestransferheader.intApproveLevels FROM ware_storestransferheader WHERE (intTransfNo='$serialNo') AND (`intTransfYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			
		return $editableFlag;
	}
//-----------------------------------------------------------
	function getEditLocationValidation($type,$location,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
		  $sql = "SELECT
					ware_storesrequesitionheader.intLocationId
					FROM
					ware_storesrequesitionheader
					Inner Join mst_locations ON ware_storesrequesitionheader.intLocationId = mst_locations.intId
					WHERE
					ware_storesrequesitionheader.intRequisitionNo =  '$serialNo' AND
					ware_storesrequesitionheader.intRequisitionYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			if($location == $row['intLocationId'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function loadConfirmatonMode-------------------
function getConfirmationMode($programCode,$intUser,$approveLevels,$status){
		global $db;
		$k=$approveLevels+2-$status;
		$sqlp = "SELECT 
		menupermision.intEdit, 
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		if($rowp['int'.$k.'Approval']==1) 
		 $confirmatonMode=1; 
		else
		 $confirmatonMode=0; 
	 
	return $confirmatonMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
 	 	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		$resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1)
		 {
			 if($intStatus==($savedStat+1) || ($intStatus==0))
			 { 
				 $editMode=1;
			 }
		 }
		 
	//	echo $editMode;
		 
	return $editMode;
}
//------------------------------function get MaxAppByStatus---------------------
function getMaxAppByStatus($serialNo,$year){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	  $sql = "SELECT
				max(ware_storestransferheader_approvedby.intStatus) as status 
				FROM
				ware_storestransferheader_approvedby
				WHERE
				ware_storestransferheader_approvedby.intTransfNo =  '$serialNo' AND
				ware_storestransferheader_approvedby.intYear =  '$year'";
				
		$resultm = $db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm['status'];

}
//--------------------------------------------------------getMaxAppByStatus

?>



<?php
	session_start();
	$backwardseperator 	= "../../../../";
	$mainPath 			= $_SESSION['mainPath'];
	$userId 			= $_SESSION['userId'];
	$requestType 		= $_REQUEST['requestType'];
	$locationId			= $_SESSION["CompanyID"];
	$companyId			= $_SESSION["headCompanyId"];
	include "{$backwardseperator}dataAccess/Connector.php";
	
if($requestType=='loadSubCategory')
{
	$mainCat = $_REQUEST['mainCategory'];
	$sql = "SELECT
			mst_subcategory.intId,
			mst_subcategory.strCode,
			mst_subcategory.strName
			FROM mst_subcategory
			WHERE
			mst_subcategory.intMainCategory =  '$mainCat' 
			ORDER BY mst_subcategory.strName ASC";
	$result = $db->RunQuery($sql);
	$html = "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
	}
	echo $html;
	echo json_encode($response);
}
else if($requestType=='loadItems')
{
	$mainCat 		= $_REQUEST['mainCategory'];
	$subCat 		= $_REQUEST['subCategory'];
	$description	= $_REQUEST['description'];
	$SRNFrom		= $_REQUEST['SRNFrom'];
	
	$sql = "SELECT MI.intId,MI.strName AS itemName,MI.intMainCategory,MI.intSubCategory,MI.intUOM,MI.strCode,
			MC.strName AS mainCatName,SC.strName AS subCatName,MU.strName AS unitName,
			IFNULL((SELECT SUM(dblQty) FROM ware_sub_stocktransactions_bulk 
			WHERE intLocationId='$locationId' AND intSubStores='$SRNFrom' AND intItemId=MI.intId),0) AS stockBlance
			FROM mst_item MI
			INNER JOIN mst_maincategory MC ON MI.intMainCategory=MC.intId
			INNER JOIN mst_subcategory SC ON MI.intSubCategory=SC.intId
			INNER JOIN mst_units MU ON MI.intUOM=MU.intId
			WHERE MI.intStatus=1 AND
			MI.intStoresTrnsItem=1 AND
			MI.intBomItem=1 ";
	if($mainCat!='')
		$sql.="AND MI.intMainCategory='$mainCat' ";
	if($subCat!='')
		$sql.="AND MI.intSubCategory='$subCat' ";
	if($description!='')
		$sql.="AND MI.strName LIKE '%$description%' ";
	$sql.="ORDER BY mainCatName ASC, subCatName ASC,itemName ASC";
	
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$data['itemId'] 		= $row['intId'];
		$data['maincatId'] 		= $row['intMainCategory'];
		$data['subCatId'] 		= $row['intSubCategory'];
		$data['code'] 			= $row['strCode'];
		$data['UOM'] 			= $row['unitName'];
		$data['unitId'] 		= $row['intUOM'];
		$data['mainCatName'] 	= $row['mainCatName'];
		$data['subCatName'] 	= $row['subCatName'];
		$data['itemName'] 		= $row['itemName'];
		$data['stockBalance'] 	= $row['stockBlance'];
		$data['stockBalance']	= getStockBalance_bulk($SRNFrom,$row['intId']);
		
		$arrCombo[] = $data;
	}
	$response['arrCombo'] 	= $arrCombo;
	echo json_encode($response);

}
function getStockBalance_bulk($reqFromStores,$item)
{
	global $db;
	   $sql = "SELECT
			ROUND(IFNULL(SUM(ware_stocktransactions_bulk.dblQty),0),2) AS stockBal
			FROM ware_stocktransactions_bulk
			WHERE
			ware_stocktransactions_bulk.intItemId =  '$item' AND 
			ware_stocktransactions_bulk.intLocationId = (SELECT intLocation FROM mst_substores WHERE intId='$reqFromStores')
			GROUP BY
			ware_stocktransactions_bulk.intItemId";
	
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	return ($row['stockBal']==''?0.00:$row['stockBal']);	
}
?>
// JavaScript Document
$(document).ready(function() {
	
	$("#frmStoresReqNote").validationEngine();
	
	if(intAddx)
	{	
		if(intStatus!='' && approvalLevels!='')
		{
			if((intStatus>approvalLevels) || intStatus==0)
			{
				$('#frmJobCard #butNew').show();
				$('#frmJobCard #butSave').show();
			}
		}
		else
		{
			$('#frmStoresReqNote #butNew').show();
			$('#frmStoresReqNote #butSave').show();
		}
	}
	
	//permision for edit 
	if(intEditx)
	{	
		if(intStatus!='' && approvalLevels!='')
		{
			if((intStatus>approvalLevels) || intStatus==0)
			{
				$('#frmStoresReqNote #butSave').show();
			}
		}
		else
		{
			$('#frmStoresReqNote #butSave').show();
		}
	}
	
	//permision for delete
	if(intDeletex)
	{
		$('#frmStoresReqNote #butDelete').show();
	}
	
	$('#butAdd').live('click',loadItemPopup);
	$('.delImg').live('click',removeRow);
	$('#cboSRNFrom').live('change',removeAllData);
	$('#cboSRNTo').live('change',removeAllData);
	$('#butConfim').live('click',urlApprove);
	$('#butReport').live('click',loadReport);
	$('#butSave').live('click',saveData);
	$('#butNew').live('click',newData);
});

function loadItemPopup()
{
	if ($('#frmStoresReqNote').validationEngine('validate')) 
	{
		var srnFrom = $('#cboSRNFrom').val();
		popupWindow3('1');
		$('#popupContact1').load('srnDetailsPopup.php?srnFrom='+srnFrom,function(){
				//checkAlreadySelected();
				$('#butAdd1').click(addClickedRows);
				$('#butClose1').click(disablePopup);
				 //-------------------------------------------- 
				  $('#frmSRNPopup #cboMainCategory').change(function(){
						
						var mainCategory = $('#cboMainCategory').val();
						var url 		 = "srnDetails-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
						var httpobj 	 = $.ajax({url:url,async:false})
						document.getElementById('cboSubCategory').innerHTML=httpobj.responseText;
						
				  });
				 //-------------------------------------------- 
				  $('#frmSRNPopup #imgSearchItems').click(function(){
						
						$("#tblItemsPopup tr:gt(0)").remove();
						
						var mainCategory 	= $('#cboMainCategory').val();
						var subCategory 	= $('#cboSubCategory').val();
						var description		= $('#txtItmDesc').val();
						var SRNFrom			= $('#txtSRNFrom').val();
						var url 			= "srnDetails-db-get.php?requestType=loadItems";
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							data:"&mainCategory="+mainCategory+"&subCategory="+subCategory+"&description="+URLEncode(description)+"&SRNFrom="+SRNFrom,
							async:false,
							success:function(json){
	
								var length 		= json.arrCombo.length;
								var arrCombo 	= json.arrCombo;
	
	
								for(var i=0;i<length;i++)
								{
									var itemId		= arrCombo[i]['itemId'];	
									var maincatId	= arrCombo[i]['maincatId'];	
									var subCatId	= arrCombo[i]['subCatId'];	
									var code		= arrCombo[i]['code'];	
									var itemName	= arrCombo[i]['itemName'];	
									var UOM			= arrCombo[i]['UOM'];	
									var unitId		= arrCombo[i]['unitId'];	
									var mainCatName	= arrCombo[i]['mainCatName'];
									var subCatName	= arrCombo[i]['subCatName'];	
									var itemName	= arrCombo[i]['itemName'];
									var stockBal	= arrCombo[i]['stockBalance'];
										
									var content='<tr class="normalfnt clschkItem"><td align="center" bgcolor="#FFFFFF" id="'+itemId+'"><input type="checkbox" id="chkItem" name="chkItem" class="clschkItem" /></td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="clsPMainCategory">'+mainCatName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="clsPSubCategory">'+subCatName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="clsPCode">'+code+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="clsPItem">'+itemName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+unitId+'" class="clsPUOM">'+UOM+'</td>';
									content +='<td bgcolor="#FFFFFF" style="text-align:right" class="clsPStockBal">'+stockBal+'</td>';
	
									add_new_row('#frmSRNPopup #tblItemsPopup',content);
								}
									checkAlreadySelected();
							}
						});
						
				  });
				  //-------------------------------------------------------
					$('#tblItemsPopup #chkAll').live('click',function(){
					
						var status = false;
						if($('#chkAll').attr('checked'))
						{
							status = true;
						}
						else
						{
							status = false;
						}
						$('#tblItemsPopup .clschkItem').attr("checked",status);
					});
				  //-------------------------------------------------------
					$('#butAdd1').click(addClickedRows);
					$('#butClose1').click(disablePopup);
			});	
	}
	else
	{
		var t=setTimeout("alertx()",3000);
	}
}
function add_new_row(table,rowcontent)
{
	if ($(table).length>0)
	{
		if ($(table+' > tbody').length==0) $(table).append('<tbody />');
		($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
	}
}
function checkAlreadySelected()
{	
	$('#tblSRNItem .clsItem').each(function(){
		var itemNo	= $(this).attr('id');
		
		$('#tblItemsPopup .clsPItem').each(function(){

			var itemNoP = $(this).attr('id');
			if((itemNo==itemNoP))
			{	
				$(this).parent().find('.clschkItem').attr("checked",true);
				$(this).parent().find('.clschkItem').attr("disabled",true);
			}
		});
	});
}
function addClickedRows()
{
	
	$('#tblItemsPopup .clschkItem').each(function(){
	
		if(($(this).is(":checked")) && (!$(this).is(":disabled")))
		{
			var itemId		= $(this).parent().parent().find('.clsPItem').attr('id');
			var maincatId	= $(this).parent().parent().find(".clsPMainCategory").attr('id');
			var subCatId	= $(this).parent().parent().find(".clsPSubCategory").attr('id');
			var itemName	= $(this).parent().parent().find(".clsPItem").html();
			var mainCatName	= $(this).parent().parent().find(".clsPMainCategory").html();
			var subCatName	= $(this).parent().parent().find(".clsPSubCategory").html();
			var stockBal	= $(this).parent().parent().find(".clsPStockBal").html();
			
			var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="../../../../images/del.png" width="15" height="15" /></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="clsMainCat">'+mainCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="clsSubCat">'+subCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="clsItem">'+itemName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="txtQty" name="txtQty" class="clsQty" style="width:60px;text-align:right" type="text" value="0"/></td>';
			content +='<td align="center" bgcolor="#FFFFFF" class="clsStockBal" >'+stockBal+'</td>';
			content +='</tr>';	
			
			add_new_row('#frmStoresReqNote #tblSRNItem',content);
		}
	});
	disablePopup();
}
function removeAllData()
{
	$("#tblSRNItem tr:gt(0)").remove();
}
function saveData()
{
	var SRNNo = $('#txtSRNNo').val();
	var SRNYear = $('#txtSRNYear').val();
	var dtDate = $('#dtDate').val();
	
	if ($('#frmStoresReqNote').validationEngine('validate')) 
	{
		var chkStatus  = true;
		var chkRecords = false;
		value="[ ";
		$('#tblSRNItem .clsQty').each(function(){
			
			chkRecords = true;
			var qty = $(this).val();
			if(qty==0 || qty<0)
			{
				$(this).validationEngine('showPrompt','Please enter valid Qty.','fail');
				var t=setTimeout("alertx1()",2000);	
				chkStatus = false ;
			}
			var itemId 	= $(this).parent().parent().find('.clsItem').attr('id');
			var Qty 	= $(this).val();
			value += '{ "itemId": "'+itemId+'", "Qty": "'+Qty+'"},';
		});
		if(!chkRecords)
		{
			$(this).validationEngine('showPrompt','No records to save.','fail');
			var t=setTimeout("alertx2()",3000);	
			return;
		}
		if(!chkStatus)
		{
			return;
		}
		value = value.substr(0,value.length-1);
		value += " ]";
		
		var url = "srnDetails-db-set.php?requestType=saveData";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			data:$("#frmStoresReqNote").serialize()+'&SRNNo='+SRNNo+'&SRNYear='+SRNYear+'&dtDate='+dtDate+'&itemDetails='+value,
			async:false,
			success:function(json){
					$('#frmStoresReqNote #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t		= setTimeout("alertx2()",1000);
						var SRNNo 	= json.SRNNo;
						var SRNYear = json.SRNYear;
						setTimeout("document.location.href ='storesRequisitionNote.php?SRNNo="+SRNNo+"&SRNYear="+SRNYear+"'",1000)
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmStoresReqNote #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx2()",1000);
					return;
				}		
			});
	}
}
function urlApprove()
{
	window.open("../listing/rptSrn.php"+window.location.search+'&approveMode=1');	
}
function newData()
{
	window.location.href = "storesRequisitionNote.php";
}
function loadReport()
{
	var SRNNo 		= $('#txtSRNNo').val();
	var SRNYear  	= $('#txtSRNYear').val();

	if(SRNNo!=='' && SRNYear!='' )
	{
		window.open("../listing/rptSrn.php?SRNNo="+SRNNo+'&SRNYear='+SRNYear);
	}
	else
	{
		$('#frmStoresReqNote #butReport').validationEngine('showPrompt','SRN No is empty.','fail');
		var t=setTimeout("alertxReport()",1000);
	}
}
function removeRow()
{
	$(this).parent().parent().remove();
}
function alertx()
{
	$('#frmStoresReqNote #cboSRNFrom').validationEngine('hide')	;
	$('#frmStoresReqNote #cboSRNTo').validationEngine('hide')	;
}
function alertx1()
{
	$('#frmStoresReqNote #tblSRNItem').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmStoresReqNote #butSave').validationEngine('hide')	;
}
function alertxReport()
{
	$('#frmStoresReqNote #butReport').validationEngine('hide')	;
}
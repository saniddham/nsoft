<?php
	session_start();
	$backwardseperator 	= "../../../../";
	$mainPath 			= $_SESSION['mainPath'];
	$userId 			= $_SESSION['userId'];
	$locationId			= $_SESSION["CompanyID"];
	$companyId			= $_SESSION["headCompanyId"];
	$requestType 		= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once "../../../../class/cls_commonFunctions_get.php";
	
	$obj_maxNo	   		= new cls_commonFunctions_get($db);
	
	$arr 				= json_decode($_REQUEST['itemDetails'], true);
	$programName		= 'Stores Requisition Note';
	$programCode		= 'P0256';
	$approveLevel 		= (int)getApproveLevel($programName);

if($requestType=='saveData')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
		
	$SRNNo  			= $_REQUEST['SRNNo'];
	$SRNYear  			= $_REQUEST['SRNYear'];
	$SRNFrom  			= $_REQUEST['cboSRNFrom'];
	$SRNTo  			= $_REQUEST['cboSRNTo'];
	$Date  				= $_REQUEST['dtDate'];
	$note  				= $_REQUEST['txtNote'];
	$rollBackFlag 		= 0;
	
	if($SRNNo=='')
	{
		
		$data['arrData'] = $obj_maxNo->GetSystemMaxNo('intSRNNo',$locationId);
		if($data['arrData']['rollBackFlag']==1)
		{
			$rollBackFlag		= 1;
			$rollBackMsg		= $data['arrData']['msg'];
		}
		else
		{
			$rollBackFlag		= 0;
			$SRNNo				= $data['arrData']['max_no'];
			$SRNYear			= date('Y');
			
			$sql = "INSERT INTO ware_storesrequesitionheader 
					(
					intRequisitionNo, intRequisitionYear, intReqToStores, 
					intReqFromStores, strNote, intStatus, 
					intApproveLevels, dtDate, dtmCreateDate, 
					intUser, intLocationId
					)
					VALUES
					(
					'$SRNNo', '$SRNYear', '$SRNTo', 
					'$SRNFrom', '$note', $approveLevel+1, 
					$approveLevel, '$Date', now(), 
					'$userId', '$locationId'
					);";
			
			$result = $db->RunQuery2($sql);
			if(!$result)
			{
				$rollBackFlag	= 1 ;
				$rollBackMsg 	= $db->errormsg;
			}
			else
			{
				$headerSave			= true;
				$saved				= 0;
				$toSave				= 0;
				$status     		= 'saved';
				
				foreach($arr as $arrVal)
				{
					$Item 	 		= $arrVal['itemId'];
					$Qty 		 	= $arrVal['Qty'];
					
					$sql = "INSERT INTO ware_storesrequesitiondetails 
							(
							intRequisitionNo, intRequisitionYear, 
							intItemId, dblQty
							)
							VALUES
							(
							'$SRNNo', '$SRNYear', 
							'$Item', '$Qty'
							) ";
					$result = $db->RunQuery2($sql);
					if($result)
					{
						$saved++;
						$status = 'saved';
					}
					else
					{
						$rollBackFlag	= 1;
						$rollBackMsg	= "detail saving error.";
					}
					$toSave++;
				}
			}
			
		}
	}
	else
	{
		$sql = "UPDATE ware_storesrequesitionheader 
				SET
				intReqToStores = '$SRNTo' , 
				intReqFromStores = '$SRNFrom' , 
				strNote = '$note' , 
				intStatus = $approveLevel+1 , 
				intApproveLevels = $approveLevel , 
				dtDate = '$Date' , 
				intUser = '$userId' , 
				intLocationId = '$locationId' , 
				intModifiedBy = '$userId' , 
				dtmModifiedDate = now()
				WHERE
				intRequisitionNo = '$SRNNo' AND 
				intRequisitionYear = '$SRNYear' ";
		$result = $db->RunQuery2($sql);
		if(!$result)
		{
			$rollBackFlag	= 1 ;
			$rollBackMsg 	= $db->errormsg;
		}
		else
		{
			$headerSave			= true;
			$saved				= 0;
			$toSave				= 0;
			$status				= 'updated';
			
			$sqlDel = " DELETE FROM ware_storesrequesitiondetails 
						WHERE
						intRequisitionNo = '$SRNNo' AND 
						intRequisitionYear = '$SRNYear' ";
			$resultDel = $db->RunQuery2($sqlDel);
			if(!$resultDel)
			{
				$rollBackFlag	= 1 ;
				$rollBackMsg 	= $db->errormsg;
			}
			else
			{
				foreach($arr as $arrVal)
				{
					$Item 	 		= $arrVal['itemId'];
					$Qty 		 	= $arrVal['Qty'];
					
					$sql = "INSERT INTO ware_storesrequesitiondetails 
							(
							intRequisitionNo, intRequisitionYear, 
							intItemId, dblQty
							)
							VALUES
							(
							'$SRNNo', '$SRNYear', 
							'$Item', '$Qty'
							) ";
					$result = $db->RunQuery2($sql);
					if($result)
					{
						$saved++;
						$status = 'updated';
					}
					else
					{
						$rollBackFlag	= 1;
						$rollBackMsg	= $db->errormsg;
					}
					$toSave++;
				}
			}
		}
	}
	if($rollBackFlag==1)
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
	}
	else if(($headerSave) && ($saved==$toSave))
	{
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		
		if($status=='saved')
			$response['msg'] 	= 'Saved successfully.';
		else
			$response['msg'] 	= 'Updated successfully.';
		
		$response['SRNNo']		= $SRNNo;
		$response['SRNYear']	= $SRNYear;
	}
	else
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
	}
	$db->CloseConnection();
	echo json_encode($response);
}
?>
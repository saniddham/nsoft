<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath		= $_SESSION['mainPath'];
	$locationId		= $_SESSION["CompanyID"];
	$thisFilePath 	= $_SERVER['PHP_SELF'];
	$userId 		= $_SESSION['userId'];
	include  "{$backwardseperator}dataAccess/permisionCheck.inc";
	
	$cboSRNFrom 	= $_REQUEST['cboSRNFrom'];
	$cboSRNTo  		= $_REQUEST['cboSRNTo'];
	$dtDate 		= $_REQUEST['dtDate'];
	$txtNote 		= $_REQUEST['txtNote'];
	
	$SRNNo		 	= $_REQUEST['SRNNo'];
	$SRNYear 		= $_REQUEST['SRNYear'];
	$status			= '';
	$approveLevels	= '';
	
	$programName	= 'Stores Requisition Note';
	$programCode	= 'P0256';
	
	if($SRNNo!="" && $SRNYear!="")
	{
		$result = loadHeader($SRNNo,$SRNYear,$locationId);
		while($row=mysqli_fetch_array($result))
		{
			$cboSRNFrom 	= $row['intReqFromStores'];
			$cboSRNTo 		= $row['intReqToStores'];
			$txtNote 		= $row['strNote'];
			$status 		= $row['intStatus'];
			$dtDate 		= $row['dtDate'];
			$approveLevels 	= $row['intApproveLevels'];
		}
		
		$confirmationMode		= loadConfirmatonMode($programCode,$status,$approveLevels,$userId);
		$rejectionMode			= loadRejectionMode($programCode,$status,$approveLevels,$userId);
	}
	
?>
<script type="text/javascript" >
	var intStatus 		= '<?php echo $status; ?>' ;
	var approvalLevels 	= '<?php echo $approveLevels; ?>' ;
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Stores Requisition Note</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
<script type="application/javascript" src="storesRequisitionNote-js.js"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">

</head>
<body>
<form id="frmStoresReqNote" name="frmStoresReqNote" method="post" action="">
<table width="100%" border="0" align="center">
    <tr>
        <td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
    </tr>
</table> 

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text"> Stores Requisition Note</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="22" class="normalfnt">SRN No</td>
            <td width="22%"><input name="txtSRNNo" type="text" disabled="disabled" class="txtText" id="txtSRNNo" style="width:60px" value="<?php echo $SRNNo; ?>" />&nbsp;<input name="txtSRNYear" type="text" disabled="disabled" class="txtText" id="txtSRNYear" style="width:40px" value="<?php echo $SRNYear; ?>" /></td>
            <td width="17%">&nbsp;</td>
            <td width="18%">&nbsp;</td>
            <td width="10%" class="normalfnt">Date</td>
            <td width="23%"><input name="dtDate" type="text" disabled="disabled" class="txtNumber" id="dtDate" style="width:100px" value="<?php echo(($dtDate)?$dtDate:date('Y-m-d')); ?>" /></td>
          </tr>
          <tr>
            <td height="22" class="normalfnt">SRN From</td>
            <td><select style="width:200px" name="cboSRNFrom" id="cboSRNFrom" class="validate[required]">
            
            <?php
				$sql = "SELECT intId,strName
						FROM mst_substores
						WHERE intParentId=0 AND
						intMainStoresType=0 AND
						intLocation='$locationId' AND
						intStatus=1
						ORDER BY strName";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($cboSRNFrom==$row['intId'])
						echo "<option value=\"".$row["intId"]."\" selected=\"selected\" >".$row["strName"]."</option>";
					else
						echo "<option value=\"".$row["intId"]."\" >".$row["strName"]."</option>";
				}
			?>
            </select></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">SRN To</td>
            <td><select style="width:200px" name="cboSRNTo" id="cboSRNTo" class="validate[required]">
              
              <?php
				$sql = "SELECT intId,strName
						FROM mst_substores
						WHERE intParentId=0 AND
						intMainStoresType=1 AND
						intLocation='$locationId' AND
						intStatus=1
						ORDER BY strName";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($cboSRNTo==$row['intId'])
						echo "<option value=\"".$row["intId"]."\" selected=\"selected\" >".$row["strName"]."</option>";
					else
						echo "<option value=\"".$row["intId"]."\" >".$row["strName"]."</option>";
				}
			?>
            </select></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="27" rowspan="2" valign="top" class="normalfnt">Note</td>
            <td width="38%" rowspan="2"><textarea name="txtNote" id="txtNote" cols="31" rows="3"><?php echo $txtNote; ?></textarea></td>
            <td width="26%" rowspan="2">&nbsp;</td>
            <td width="10%" rowspan="2" valign="top" class="normalfnt">&nbsp;</td>
            <td width="16%" align="right" valign="top">&nbsp;</td>
            </tr>
          <tr>
            <td align="right" valign="bottom"><img id="butAdd" name="butAdd" alt="Add" src="../../../../images/Tadd.jpg" width="92" height="24" /></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="bordered" id="tblSRNItem" >
            <tr>
              <th width="8%" height="22" >Del</th>
              <th width="17%" >Main Category</th>
              <th width="17%" >Sub Category</th>
              <th width="31%" >Item Description</th>
              <th width="13%">Qty</th>
              <th width="14%"> Stock Balance</th>
              </tr>
            <?php
			if($SRNNo!="" && $SRNYear!="")
			{
				$sql = "SELECT SRD.intRequisitionNo,SRD.intRequisitionYear,SRD.intItemId,MI.strName AS itemName,
						MC.strName AS mainCatName,MC.intId AS mainCatId,SC.strName AS subCatName,SC.intId AS subCatId,SRD.dblQty,
						IFNULL((SELECT SUM(dblQty) FROM ware_sub_stocktransactions_bulk 
						WHERE intLocationId='$locationId' AND intSubStores='$cboSRNFrom' AND intItemId=MI.intId),0) AS stockBlance
						FROM ware_storesrequesitiondetails SRD
						INNER JOIN mst_item MI ON MI.intId=SRD.intItemId
						INNER JOIN mst_maincategory MC ON MC.intId=MI.intMainCategory
						INNER JOIN mst_subcategory SC ON SC.intId=MI.intSubCategory
						WHERE SRD.intRequisitionNo='$SRNNo' AND
						SRD.intRequisitionYear='$SRNYear' ";
				
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
				?>
                	<tr class="normalfnt">
                    <td align="center" bgcolor="#FFFFFF"><img class="delImg" src="../../../../images/del.png" width="15" height="15" /></td>
                    <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['mainCatId']; ?>" class="clsMainCat"><?php echo $row['mainCatName']; ?></td>
                    <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['subCatId']; ?>" class="clsSubCat"><?php echo $row['subCatName']; ?></td>
                    <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['intItemId']; ?>" class="clsItem"><?php echo $row['itemName']; ?></td>
                    <td align="center" bgcolor="#FFFFFF" id=""><input  id="txtQty" name="txtQty" class="clsQty" style="width:60px;text-align:right" type="text" value="<?php echo $row['dblQty']; ?>"/></td>
                    <td align="center" bgcolor="#FFFFFF" class="clsStockBal" ><?php echo $row['stockBlance']; ?></td>
                    </tr>
                <?php
				}
			}
			?>
            </table>
          </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><img src="../../../../images/Tnew.jpg" width="92" height="24" id="butNew" name="butNew" /><img src="../../../../images/Tsave.jpg" style="display:none" width="92" height="24" id="butSave" name="butSave" /><img <?php echo((($confirmationMode==1 || $rejectionMode==1) && $status!=1)?'style="display:inline"':'style="display:none"'); ?> src="../../../../images/Tconfirm.jpg" width="92" height="24" id="butConfim" name="butConfirm" /><img src="../../../../images/Treport.jpg" width="92" height="24" id="butReport" name="butReport" /><a href="../../../../main.php"><img src="../../../../images/Tclose.jpg" width="92" height="24" id="butClose" name="butClose" /></a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>

</body>
</html>

<?php

function loadHeader($SRNNo,$SRNYear,$locationId)
{
	global $db;
	$sql = "SELECT 	intReqToStores, intReqFromStores, 
			strNote, intStatus, 
			intApproveLevels, date(dtDate) as dtDate
			FROM 
			ware_storesrequesitionheader 
			WHERE intRequisitionNo = '$SRNNo' AND
			intRequisitionYear = '$SRNYear' AND
			intLocationId = '$locationId' ";

	$result = $db->RunQuery($sql);
	return $result;
}
function loadConfirmatonMode($programCode,$status,$approveLevels,$userId)
{
	global $db;
	$confirmatonMode = 0;
	$k = $approveLevels+2-$status;
	
	$sqlp = "SELECT
			 menupermision.int".$k."Approval 
			 FROM menupermision 
			 Inner Join menus ON menupermision.intMenuId = menus.intId
			 WHERE
			 menus.strCode =  '$programCode' AND
			 menupermision.intUserId = '$userId' ";	
	
	$resultp = $db->RunQuery($sqlp);
	$rowp	 = mysqli_fetch_array($resultp);
	if($rowp['int'.$k.'Approval']==1)
	{
		 if($intStatus!=1)
		 {
		 	$confirmatonMode=1;
		 }
	} 
	return $confirmatonMode;
}
function loadRejectionMode($programCode,$status,$approveLevels,$userId)
{
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
			 menupermision.intReject 
			 FROM menupermision 
			 Inner Join menus ON menupermision.intMenuId = menus.intId
			 WHERE
			 menus.strCode = '$programCode' AND
			 menupermision.intUserId = '$userId'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1)
	{
		 if($status!=0)
		 {
			 $rejectMode=1;
		 }
	}
	return $rejectMode;
}
?>
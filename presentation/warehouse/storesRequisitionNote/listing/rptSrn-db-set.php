<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	  = $_SESSION['mainPath'];
	$userId 	  = $_SESSION['userId'];
	$requestType  = $_REQUEST['status'];
	
	include "{$backwardseperator}dataAccess/Connector.php";
	
	$SRNNo		  = $_REQUEST['SRNNo'];
	$SRNYear	  = $_REQUEST['SRNYear'];

if($requestType=='approve')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	$rollBackFlag 	= 0;
	
	$sqlUpd = "UPDATE ware_storesrequesitionheader 
				SET
				intStatus = intStatus-1
				WHERE
				intRequisitionNo = '$SRNNo' AND 
				intRequisitionYear = '$SRNYear' ";
	$resultUpd = $db->RunQuery2($sqlUpd);
	if($resultUpd)
	{
		$sql = "SELECT intStatus, intApproveLevels 
				FROM ware_storesrequesitionheader 
				WHERE intRequisitionNo = '$SRNNo' AND 
				intRequisitionYear = '$SRNYear' ";
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		
		$approveLevel = $row['intApproveLevels'];
		$updStatus    = $row['intStatus'];
		$newApprLevel = ($approveLevel+1)-$updStatus;
		
		$sqlIns = "INSERT INTO ware_storesrequesitionheader_approvedby 
					(
					intRequisitionNo, 
					intYear, 
					intApproveLevelNo, 
					intApproveUser, 
					dtApprovedDate, 
					intStatus
					)
					VALUES
					(
					'$SRNNo', 
					'$SRNYear', 
					'$newApprLevel', 
					'$userId', 
					now(), 
					0
					) ";
		$resultIns = $db->RunQuery2($sqlIns);
		if($resultIns)
		{
			$rollBackFlag			= 0;
			$rollBackMsg			= "Approval raised sucessfully";
		}
		else
		{
			$rollBackFlag			= 1;
			$rollBackMsg			= $db->errormsg;
		}
	}
	else
	{
		$rollBackFlag			= 1;
		$rollBackMsg			= $db->errormsg;
	}
	if($rollBackFlag==1)
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
	}
	else
	{
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		$response['msg'] 		= $rollBackMsg;
	}
	
	$db->CloseConnection();
	echo json_encode($response);
}
if($requestType=='reject')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	$rollBackFlag 	= 0;
	
	$sqlUpd = "UPDATE ware_storesrequesitionheader 
				SET
				intStatus = 0
				WHERE
				intRequisitionNo = '$SRNNo' AND 
				intRequisitionYear = '$SRNYear' ";
	$resultUpd = $db->RunQuery2($sqlUpd);
	if($resultUpd)
	{
		$sqlMaxStatus = "SELECT MAX(intStatus) AS maxStatus 
							FROM ware_storesrequesitionheader_approvedby 
							WHERE intRequisitionNo='$SRNNo' AND 
							intYear='$SRNYear' ";
		$resultMax = $db->RunQuery2($sqlMaxStatus);
		$rowMax = mysqli_fetch_array($resultMax);
		$maxStatus = $rowMax['maxStatus'];
		
		$sqlUpd = "UPDATE ware_storesrequesitionheader_approvedby 
					SET intStatus = $maxStatus+1 
					WHERE intRequisitionNo='$SRNNo' AND
					intYear='$SRNYear' AND 
					intStatus = 0 ";
		$result = $db->RunQuery2($sqlUpd);
		if($result)
		{
			$sqlIns = "INSERT INTO ware_storesrequesitionheader_approvedby 
						(
						intRequisitionNo, 
						intYear, 
						intApproveLevelNo, 
						intApproveUser, 
						dtApprovedDate, 
						intStatus
						)
						VALUES
						(
						'$SRNNo', 
						'$SRNYear', 
						0, 
						'$userId', 
						now(), 
						0
						) ";
			$resultIns = $db->RunQuery2($sqlIns);
			if($resultIns)
			{
				$rollBackFlag			= 0;
				$rollBackMsg			= "Rejected sucessfully";
			}
			else
			{
				$rollBackFlag			= 1;
				$rollBackMsg			= $db->errormsg;
			}
		}
		else
		{
			$rollBackFlag			= 1;
			$rollBackMsg			= $db->errormsg;
		}

	}
	else
	{
		$rollBackFlag			= 1;
		$rollBackMsg			= $db->errormsg;
	}
	if($rollBackFlag==1)
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
	}
	else
	{
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		$response['msg'] 		= $rollBackMsg;
	}
	
	$db->CloseConnection();
	echo json_encode($response);
}
?>
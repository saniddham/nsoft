function functionList(project,subPriject,year)
{ 
 	loadProjectYearCombo();
	loadProjectCombo();
	loadMainCatCombo();
	loadCurrencyCombo();
	
	$('#frmBOM .year').val(year);
	$('.year').change();
	$('#frmBOM .projects').val(project);
	$('.projects').change();
	$('#frmBOM .subProjects').val(subPriject);
	$('.subProjects').change();
	
	
}		
$(document).ready(function() {
  		$("#frmBOM").validationEngine();
		$('#frmBOM #txtCode').focus();
  //permision for add 
  if(intAddx)
  {
 	$('#frmBOM #butNew').show();
	$('#frmBOM #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmBOM #butSave').show();
	$('#frmBOM #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmBOM #butDelete').show();
	$('#frmBOM #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmBOM #cboSearch').removeAttr('disabled');
  }
 //------------------------------------------------------- 
	$('.projects').live('change',function(){
			clearHeader();
			clearRows();
			loadSubProjectCombo();
	});
 //------------------------------------------------------- 
	$('.subProjects').live('change',function(){
			clearHeader();
			clearRows();
			loadHeaderData();
			loadGridData();
			viewButtons();
	});
 //------------------------------------------------------- 
	$('.mainCat').live('click',function(){
		var currency = $('#frmBOM .bomCurrency').val();
			var date = $('#frmBOM #dtDate').val();
		if((currency=='') || (date=='') ){
			alert("Please select Date and Currency");
			return false;	
		}
	});
 //------------------------------------------------------- 
	$('.mainCat').live('change',function(){
			loadSubCatCombo(this);
	});
 //------------------------------------------------------- 
	$('.subCat').live('change',function(){
			loadItemCombo(this);
	});
 //------------------------------------------------------- 
	$('.item').live('change',function(){
			loadItemDetails(this);
	});
 //------------------------------------------------------- 
	$('.bomCurrency').live('change',function(){
			clearRows();
	 		calculateTotalAmnt();
	});
	
 //------------------------------------------------------- 
	$('.delImg').live('click',function(){
		if($(this).parent().parent().parent().find('tr').length>3)
		$(this).parent().parent().remove();
	 	calculateTotalAmnt();
	});
	
 //------------------------------------------------------- 
	$('.calculateValue').live('keyup',function(){
		calAmmount(this);
	 	calculateTotalAmnt();
	});
 //------------------------------------------------------- 
	
  
 //------------------------------------------------------- 
$('#butInsertRow').live('click',function(){
		
	var rowCount = document.getElementById('tblItems').rows.length;
	document.getElementById('tblItems').insertRow(rowCount-1);
	rowCount = document.getElementById('tblItems').rows.length;
	document.getElementById('tblItems').rows[rowCount-2].innerHTML = document.getElementById('tblItems').rows[rowCount-3].innerHTML;
	document.getElementById('tblItems').rows[rowCount-2].className="normalfnt";
	document.getElementById('tblItems').rows[rowCount-2].id = 'dataRow';
	//$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-2)+') .proID').html(($(this).parent().parent().index()-2));
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') .mainCat').val('');
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') .subCat').html('');
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') .item').html('');
	
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') .uom').html('');
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') .uom').attr('id','');
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') .currency').html('');
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') .currency').attr('id','');
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') .unitPrice').attr('id','xx');
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') .unitPrice').val();
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') .qty').val();
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') .amount').html('');
	
	
	
//	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') .subCat').html('')	
//	alert($(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-2)+') .proID').html());
	
/*	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-2)+') .delDate').attr('id','txtDelDate'+($(this).parent().parent().index()-1));*/	
	});
 //-------------------------------------------------------
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

  ///save button click event
  $('#frmBOM #butSave').click(function(){
	//$('#frmBOM').submit();
	var requestType = '';
	if ($('#frmBOM').validationEngine('validate'))   
    { 
	showWaiting();

		var data = "requestType=save";
		
			data+="&project=" +	$('#cboProjects').val();
			data+="&year=" +	$('#cboYear').val();
			data+="&subProject=" +	$('#cboSubProjects').val();
			data+="&date=" +		$('#dtDate').val();
			data+="&currency="	+	$('#cboBomCurrency').val();
			data+="&remarks="	+	$('#txtRemarks').val();
			
			//////////////////// saving main grid part //////////////////////////////////////////
			//var rowCount = $('#tblMain >tbody >tr').length;
			var rowCount = document.getElementById('tblItems').rows.length;
			var row = 0;
			
			var arr="[";
			var salesIds='';
			$('#tblItems >tbody >tr').not(':last').not(':first').each(function(){
					 
					 arr += "{";
					 
					var itemId 	= 	$(this).find('.item').val();
					var unitPrice 		= 	parseFloat($(this).find('.unitPrice').val());
					var qty 		= 	parseFloat($(this).find('.qty').val());
					
					
					if((itemId=='') || (unitPrice=='') || (qty==''))
					{
						hideWaiting();
						alert('Please select data');
						
						return;
						//return 0;
					}/// end if
					//alert(1);
					arr += '"itemId":"'+	 itemId +'",' 		;
					arr += '"unitPrice":"'+   	unitPrice +'",' 		;
					arr += '"qty":"'+ 	qty +'"' 		;
					arr +=  '},';
			});
			arr = arr.substr(0,arr.length-1);
			
			arr += " ]";
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = "bom-db.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			data:data,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmBOM #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						if(json.confirmationMode==1){
							$('#butConfirm').show();
						}
						else{
							$('#butConfirm').hide();
						}
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmBOM #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
		hideWaiting();	
	}
   });
   
//------------------------------------------------	
	$('#frmBOM #butNew').click(function(){
		window.location.href = "bom.php";
		$('#frmBOM').get(0).reset();
		loadSearchCombo();
		$('#frmBOM #txtCode').focus();
	});
//-------------------------------------------------	


$('#butReport').click(function(){
	if($('#cboSubProjects').val()!=''){
		window.open('../listing/rptBom.php?projectId='+$('#cboProjects').val()+'&subProjectId='+$('#cboSubProjects').val()+'&year='+$('#cboYear').val());	
	}
	else{
		alert("There is no BOM view");
	}
});
//----------------------------------	
$('#butConfirm').click(function(){
	if($('#cboSubProjects').val()!=''){
		window.open('../listing/rptBom.php?projectId='+$('#cboProjects').val()+'&subProjectId='+$('#cboSubProjects').val()+'&year='+$('#cboYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no BOM to confirm");
	}
});
//-----------------------------------------------------
});



//----------------------------------------------------
function loadProjectCombo(){
		var year =$('#cboYear').val();

		var url 		= "bom-db.php?requestType=loadProjectCombo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"&year=" +	year,
			async:false,
			success:function(json){
					document.getElementById("cboProjects").innerHTML=json.arrCombo;
			}
		});
	
}
//----------------------------------------------------
function loadProjectYearCombo(){
	
		var url 		= "bom-db.php?requestType=loadProjectYearCombo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"",
			async:false,
			success:function(json){
					document.getElementById("cboYear").innerHTML=json.arrCombo;
			}
		});
	
}
//----------------------------------------------------
function loadSubProjectCombo(){
		var project = $('#frmBOM .projects').val();
		var year =$('#cboYear').val();
		
		var url 		= "bom-db.php?requestType=loadSubProjectCombo&project="+project;
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"&year=" +	year,
			async:false,
			success:function(json){
					document.getElementById("cboSubProjects").innerHTML=json.arrCombo;
			}
		});
	
}
//----------------------------------------------------
function loadMainCatCombo(){
		var url 		= "bom-db.php?requestType=loadMainCatCombo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"",
			async:false,
			success:function(json){
					document.getElementById("cboMainCat").innerHTML=json.arrCombo;
			}
		});
	
}
//----------------------------------------------------
function loadSubCatCombo(obj){
		var mainCat=$(obj).parent().parent().find(".mainCat").val();
		var url 		= "bom-db.php?requestType=loadSubCatCombo&mainCat="+mainCat;
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"",
			async:false,
			success:function(json){
					$(obj).parent().parent().find(".subCat").html(json.arrCombo);
			}
		});
	
}
//----------------------------------------------------
function loadItemCombo(obj){
		var mainCat=$(obj).parent().parent().find(".mainCat").val();
		var subCat=$(obj).parent().parent().find(".subCat").val();
		var url 		= "bom-db.php?requestType=loadItemCombo&mainCat="+mainCat+"&subCat="+subCat;
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"",
			async:false,
			success:function(json){
					$(obj).parent().parent().find(".item").html(json.arrCombo);
			}
		});
	
}
//----------------------------------------------------
function loadCurrencyCombo(){
		var url 		= "bom-db.php?requestType=loadCurrencyCombo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"",
			async:false,
			success:function(json){
					document.getElementById("cboBomCurrency").innerHTML=json.arrCombo;
			}
		});
	
}

//----------------------------------------------------
function loadItemDetails(obj){
		var itemId=$(obj).parent().parent().find(".item").val();
		var currency = $('#frmBOM .bomCurrency').val();
		var date = $('#frmBOM #dtDate').val();
		
		var url 		= "bom-db.php?requestType=loadItemDetails&itemId="+itemId+"&currency="+currency+"&date="+date;
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"",
			async:false,
			success:function(json){
			//	alert(json.arrItmDetails[0]['uom']);
			
					$(obj).parent().parent().find(".uom").html('');
					$(obj).parent().parent().find(".uom").attr('id','');
					$(obj).parent().parent().find(".currency").html('');
					$(obj).parent().parent().find(".currency").attr('id','');
					$(obj).parent().parent().find(".unitPrice").val('');
					$(obj).parent().parent().find(".unitPrice").attr('id','');
			
					$(obj).parent().parent().find(".uom").html(json.arrItmDetails[0]['uom']);
					$(obj).parent().parent().find(".uom").attr('id',json.arrItmDetails[0]['uom']);
					$(obj).parent().parent().find(".currency").html(json.arrItmDetails[0]['currency']);
					$(obj).parent().parent().find(".currency").attr('id',json.arrItmDetails[0]['intCurrency']);
					$(obj).parent().parent().find(".unitPrice").val(json.arrItmDetails[0]['dblLastPrice']);
					$(obj).parent().parent().find(".unitPrice").attr('id',json.arrItmDetails[0]['dblLastPrice']);
			}
		});
	
}

//-----------------------------------------------------------------------
function calAmmount(obj){
		var price = parseFloat($(obj).parent().parent().find(".unitPrice").val());
		var qty = parseFloat($(obj).parent().parent().find(".qty").val());
		if(isNaN(price)){
			price=0;
		}
		if(isNaN(qty)){
			qty=0;
		}
		var amm=qty*price;
		amm=amm.toFixed(4);
		$(obj).parent().parent().find(".amount").html(amm);
}
//-----------------------------------------------------
function calculateTotalAmnt()
{
	var rowCount = document.getElementById('tblItems').rows.length;
	var totAmmount=0;
	var amnt=0;
	$('#tblItems .amount').each(function(){
		amnt = parseFloat($(this).html());
		if(isNaN(amnt)){
			amnt=0;
		}
		totAmmount+=amnt;
	});
	
	totAmmount=totAmmount.toFixed(4);
	$('#divTotal').html(totAmmount)
	
}
//-----------------------------------------------------
function alertx()
{
	$('#frmBOM #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmBOM #butDelete').validationEngine('hide')	;
}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }

//----------------------------------------------
function clearHeader(){
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		$('#frmBOM #dtDate').val(d);
		$('#frmBOM #cboBomCurrency').val('');
		$('#frmBOM #txtRemarks').val('');
}
//----------------------------------------------
function clearRows(){
	
	var rowCount = document.getElementById('tblItems').rows.length;
	
	for(var i=2;i<rowCount-1;i++)
	{
		document.getElementById('tblItems').deleteRow(1);
	}
	
	$('#tblItems >tbody >tr').not(':last').not(':first').each(function(){
		
	$(this).parent().parent().find(".mainCat").val('');
	$(this).parent().parent().find(".subCat").html('');
	$(this).parent().parent().find(".item").html('');
	$(this).parent().parent().find(".uom").html('');
	$(this).parent().parent().find(".uom").attr('id','');
	$(this).parent().parent().find(".currency").html('');
	$(this).parent().parent().find(".currency").attr('id','');
	$(this).parent().parent().find(".unitPrice").val('');
	$(this).parent().parent().find(".unitPrice").attr('id','');
	$(this).parent().parent().find(".qty").val('');
	$(this).parent().parent().find(".amount").html('');
	});
		
}
//----------------------------------------------
function loadGridData(){
	clearRows();
	var project = $('#frmBOM .projects').val();
	var subProject = $('#frmBOM .subProjects').val();
	var year =$('#cboYear').val();
	
	var url = "bom-db.php";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:'requestType=loadGridData&project='+project+'&year='+year+'&subProject='+subProject,
		async:false,
		success:function(json){
				var arrData = json.arrData;
				for(var i=0; i<arrData.length; i++){
				var mainCat=arrData[i]['intMainCategory'];
				var subCat=arrData[i]['intSubCategory'];
				var itemId=arrData[i]['intItemId'];
				var qty=arrData[i]['dblQty'];
				var unitPrice=arrData[i]['dblUnitPrice'];
				var amount=qty*unitPrice;
				
				if(i>0){
					$('#butInsertRow').click();
				}
				$('#tblItems >tbody >tr:last').prev().find(".mainCat").val(mainCat);
				$('#tblItems >tbody >tr:last').prev().find(".mainCat").change();
				$('#tblItems >tbody >tr:last').prev().find(".subCat").val(subCat);
				$('#tblItems >tbody >tr:last').prev().find(".subCat").change();
				$('#tblItems >tbody >tr:last').prev().find(".item").val(itemId);
				$('#tblItems >tbody >tr:last').prev().find(".item").change();
				$('#tblItems >tbody >tr:last').prev().find(".unitPrice").val(unitPrice);
				$('#tblItems >tbody >tr:last').prev().find(".qty").val(qty);
				$('#tblItems >tbody >tr:last').prev().find(".amount").html(amount);
			}
		}
	});
	calculateTotalAmnt();
	
}
//--------------------------------------
function loadHeaderData(){
	var project = $('#frmBOM .projects').val();
	var subProject = $('#frmBOM .subProjects').val();
	var year =$('#cboYear').val();
	
	var url = "bom-db.php";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:'requestType=loadHeader&project='+project+'&year='+year+'&subProject='+subProject,
		async:false,
		success:function(json){
			var arrData = json.arrData;
			var i=0;
		//	alert(arrData[i]['intCurrencyId']);
			$('#frmBOM #dtDate').val(arrData[i]['dtmDate'].substring(0,10));
			$('#frmBOM #cboBomCurrency').val(arrData[i]['intCurrencyId']);
			$('#frmBOM #txtRemarks').val(arrData[i]['strRemarks']);
		}
	});
}
//--------------------------------------
function viewButtons(){
	var project = $('#frmBOM .projects').val();
	var subProject = $('#frmBOM .subProjects').val();
	var year =$('#cboYear').val();
	var url 		= "bom-db.php?requestType=getViewButtonsPermisions&project="+project+"&subProject="+subProject+"&year="+year;
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:"",
		async:false,
		success:function(json){
				if((json.confirmPermision==1)){
					$('#butConfirm').show();
				}
				else{
					$('#butConfirm').hide();
				}
				if((json.editPermision==1)&&(json.editableFlag==1)){
					$('#butSave').show();
				}
				else{
					$('#butSave').hide();
				}
		}
	});
}
//--------------------------------------

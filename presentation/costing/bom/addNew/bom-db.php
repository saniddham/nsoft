<?php
// ini_set('display_errors',1);//echo $_SESSION['ROOT_PATH'];

///////////////////////////////
// H B G KORALA   ////////////
//////////////////////////////
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$requestType 	= $_REQUEST['requestType'];
	$programName='BOM';
	$programCode='P0600';
	
	include "{$backwardseperator}dataAccess/Connector.php";
	
	require_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
 	require_once $_SESSION['ROOT_PATH']."class/costing/bom/cls_bom_get.php";
	require_once $_SESSION['ROOT_PATH']."class/costing/bom/cls_bom_set.php";
	
	$objcomfunget= new cls_commonFunctions_get($db);
	$objbomget= new cls_bom_get($db);
	$objbomset= new cls_bom_set($db);
	
	$db->begin();
	if($requestType=='loadProjectCombo')
	{ 
		$response['arrCombo'] 	= $objbomget->getProjects_options($company,$_REQUEST['year']);
		echo json_encode($response);
	}
	else if($requestType=='loadProjectYearCombo')
	{ 
		$response['arrCombo'] 	= $objbomget->getProjectsYear_options($company);
		echo json_encode($response);
	}
	else if($requestType=='loadSubProjectCombo')
	{
		$response['arrCombo']  	= $objbomget->getSubProjects_options($_REQUEST['project'],$_REQUEST['year']);
		echo json_encode($response);
	}
	else if($requestType=='loadMainCatCombo')
	{
		$response['arrCombo']  	= $objbomget->getMainCat_options();
		echo json_encode($response);
	}
	else if($requestType=='loadSubCatCombo')
	{
		$response['arrCombo']  	= $objbomget->getSubCat_options($_REQUEST['mainCat']);
		echo json_encode($response);
	}
	else if($requestType=='loadItemCombo')
	{
		$response['arrCombo']  	= $objbomget->getItem_options($_REQUEST['mainCat'],$_REQUEST['subCat']);
		echo json_encode($response);
	}
	else if($requestType=='loadItemDetails')
	{
		$response['arrItmDetails']  	= $objbomget->getItem_Details($_REQUEST['itemId'],$_REQUEST['currency'],$company,$_REQUEST['date']);
		echo json_encode($response);
	}
	else if($requestType=='loadCurrencyCombo')
	{
		$response['arrCombo']  	= $objbomget->getCurrency_options();
		echo json_encode($response);
	}
	else if($requestType=='getViewButtonsPermisions')
	{
		  $val=$objbomget->get_Header($_REQUEST['project'],$_REQUEST['subProject'],$_REQUEST['year']);
		$response['confirmPermision'] 	= $objcomfunget->getPermission($programCode,$userId,$val[0]['intApproveLevels'],$val[0]['intStatus']);
		$response['editableFlag'] 	= $objbomget->getEditableFlag($_REQUEST['project'],$_REQUEST['subProject'],$_REQUEST['year']);
		$response['editPermision'] 	= $objcomfunget->getEditPermission($programCode,$userId,$val[0]['intApproveLevels'],$val[0]['intStatus']);
		echo json_encode($response);
	}
	
	else if($requestType=='loadHeader')
	{
		$response['arrData'] 	= $objbomget->get_Header($_REQUEST['project'],$_REQUEST['subProject'],$_REQUEST['year']);
		echo json_encode($response);
	}
	else if($requestType=='loadGridData')
	{
		$response['arrData'] 	= $objbomget->get_GridData($_REQUEST['project'],$_REQUEST['subProject'],$_REQUEST['year']);
		echo json_encode($response);
	}
	
	else if($requestType=='save')
	{
		
		$rollBackFlag=0;
		$rollBackMsg =''; 
		$sqlString='';
		
		$project 	 = $_REQUEST['project'];
		$subProject  = $_REQUEST['subProject'];
		$date 	 = $_REQUEST['date'];
		$currency 	 = $_REQUEST['currency'];
		$remarks	 = $_REQUEST['remarks'];
		$year=$_REQUEST['year'];
				
		///////////////
		$appLevels = $objcomfunget->getApproveLevels($programName);
		$status=$appLevels+1;
		$editableFlag= $objbomget->getEditableFlag($project,$subProject,$year);
		$editPermision= $objcomfunget->getEditPermission($programCode,$userId,$appLevels,$status);
		$allreadySavedFlag = $objbomget->getAllreadySavedFlag($project,$subProject,$year);
		
		///////////////
		if(($allreadySavedFlag==1) && ($editableFlag==0)){
			$rollBackFlag=1;
			$rollBackMsg ='This BOM is already confirmed.Cant edit'; 
		}
		else if($editPermision==0){
			$rollBackFlag=1;
			$rollBackMsg ='No Permision to edit'; 
		}
		
		///////////////	//save header ////////////////////
		if($rollBackFlag==0){
			if($allreadySavedFlag==0){
			$response['arrData'] = $objbomset->insert_bom_header($project,$subProject,$year,$date,$currency,$remarks,$appLevels,$userId,$company,$location);
			}
			else{
			$response['arrData'] = $objbomset->update_bom_header($project,$subProject,$year,$date,$currency,$remarks,$appLevels,$userId,$company,$location);
			}
			
			$savedHeader=$response['arrData']['result'];
			$sqlString.=$response['arrData']['q'];
			$msg.=$response['arrData']['msg'];
			if($savedHeader!=1){
				$rollBackFlag=1;
				$rollBackMsg="Header saving Error"."</br>".$msg;	
			}
		
		}//end if($rollBackFlag==0)
				 					
		///////////////	//save details ////////////////////
		if($rollBackFlag==0){
			$arr 		= json_decode($_REQUEST['arr'], true);
			$toSave=0;
			$saved=0;
			$sqlString='';
			$msg='';
					
			$response['arrData'] 	= $objbomset->delete_bom_details($project,$subProject,$year);
			$deleted=$response['arrData']['result'];
			if($deleted!=1){
				$rollBackFlag=1;
				$rollBackMsg="Details saving Error"."</br>".$msg;	
			}
			
			if($rollBackFlag==0){
				foreach($arr as $arrVal)
				{
					$toSave++;
					$itemId 	   = $arrVal['itemId'];
					$unitPrice 	   = $arrVal['unitPrice'];
					$qty 		   = $arrVal['qty'];
					
					if($rollBackFlag==0){
						$response['arrData'] = $objbomset->insert_bom_details($project,$subProject,$year,$itemId,$unitPrice,$qty);
						
						$saved=$response['arrData']['result'];
						$sqlString.=$response['arrData']['q'];
						$msg.=$response['arrData']['msg'];
						if($saved!=1){
							$rollBackFlag=1;
							$rollBackMsg="Details saving Error"."</br>".$msg;	
						}
					}
				
				}
			}
		}//end if($rollBackFlag==0)
			
		//////////Response////////////////	
		if($rollBackFlag==1){
			$db->rollback();
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlString;
		}			
		else if(($rollBackFlag==0)){
			$confirmationMode = $objcomfunget->getPermission($programCode,$userId,$appLevels,$status);
			$db->commit();
			$response['type'] 		= 'pass';
			if($allreadySavedFlag==0) 
			$response['msg'] 		= 'Saved successfully.';
			else 
			$response['msg'] 		= 'Updated successfully.';
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->rollback();	
			$response['type'] 		= 'fail';
			//$response['msg'] 		= $db->errormsg;
			if(($msg=='') && $sqlString!='' )
			$msg="Error In Query</br>".$sqlString;
			$response['msg'] 		= $msg;
			$response['q'] 			= $sqlString;
		}
		echo  json_encode($response);
	}
	
	
	
	else if($requestType=='delete')
	{
			$project 	 = $_REQUEST['project'];
			$subProject  = $_REQUEST['subProject'];
	
			$response['arrData'] 	= $objbomset->delete_bom_header($project,$subProject,$year);
			$deleted=$response['arrData']['result'];
			$sqlString.=$response['arrData']['q'];
			$msg.=$response['arrData']['msg'];
			
			$response['arrData'] 	= $objbomset->delete_bom_details($project,$subProject,$year);
			$deleted=$response['arrData']['result'];
			$sqlString.=$response['arrData']['q'];
			$msg.=$response['arrData']['msg'];
				
						
			if($rollBackFlag==1){
				$db->rollback();
				$response['type'] 		= 'fail';
				$response['msg'] 		= $rollBackMsg;
				$response['q'] 			= '';
			}			
			else if(($deleted==1)){
				$db->commit();
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Deleted successfully.';
			}
			else{
				$db->rollback();
				$response['type'] 		= 'fail';
				if(($msg=='') && $sqlString!='' )
				$msg="Error In Query</br>".$sqlString;
				$response['msg'] 		= $msg;
				$response['q'] 			= $sqlString;
			}
			echo  json_encode($response);
	}
	
	
	$db->commit();
	
?>
<?php
///////////////////////////////
// H B G KORALA   ////////////
//////////////////////////////
session_start();
$backwardseperator = "../../../../";
$thisFilePath =  $_SERVER['PHP_SELF'];
$company 	= $_SESSION['headCompanyId'];

$projectId = $_REQUEST['projectId'];
$year = $_REQUEST['year'];
$subProjectId = $_REQUEST['subProjectId'];

include  "{$backwardseperator}dataAccess/permisionCheck.inc";

require_once $_SESSION['ROOT_PATH']."class/cls_permisions.php";
require_once "{$backwardseperator}class/cls_permisions.php";
require_once $_SESSION['ROOT_PATH']."class/costing/bom/cls_bom_get.php";
//---------check Permission to save recive qty more than PO qty.------------
$objpermisionget= new cls_permisions($db);
$objbomget= new cls_bom_get($db);

$permisionEnableCR 	= $objpermisionget->getPermisionCompanyEnableCR($company);
//------------------------------------------------------------------------

$menuID=600;
$userId 	= $_SESSION['userId'];
$bomUnitPriceEditFlag 	= $objbomget->loadBomUnitPriceEditFlag($menuID,$userId);

//echo $_SESSION['ROOT_PATH'];
$editMode=1;
/*if($projectId!='' && $projectId!=''){
	$editMode=$objbomget->getEditableFlag1($projectId,$subProjectId);
}
*/
?>
<script type="application/javascript" >
var cId = '<?php echo $cId ?>';
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BOM</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="bom-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">

</head>

<body onLoad="functionList('<?php echo $projectId; ?>','<?php echo $subProjectId; ?>','<?php echo $year; ?>');">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>	

<script src="../../../../libraries/validate//jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<form id="frmBOM" name="frmBOM" method="post" action="bom-db.php"  autocomplete="off">
<div align="center">
		<div class="trans_layoutD" style="width:890">
		  <div class="trans_text" style="width:890">BILL OF MATERIALS</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="885" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="27" ><table width="100%" border="0" class="">   
                
                
                             <?php
			//	if($permisionEnableCR==1){
				?>
                          <tr>
                            <td width="3" class="normalfnt">&nbsp;</td>
                            <td width="59" class="normalfnt">Order No</td>
                            <td width="260">
                              <select name="cboYear" class="txtbox year" id="cboYear" style="width:70px">
                                <option value=""></option>
                            </select><select name="cboProjects" class="txtbox projects" id="cboProjects" style="width:150px">
                                <option value=""></option>
                            </select></td>
                            <td width="69" class="normalfnt">Sub Project</td>
                            <td width="210">
                              <select name="cboSubProjects" class="txtbox subProjects" id="cboSubProjects" style="width:180px" >
                                <option value=""></option>
                            </select></td>
                            <td width="56" class="normalfnt">Date</td>
                            <td width="199"><input name="dtDate" type="text" value="<?php if($poNo){ echo substr($pODate,0,10); }else { echo date("Y-m-d"); }?>" class="txtbox" id="dtDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                          </tr>    
                <?php
			//	}
				?>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Remarks</td>
                <td colspan="3"><textarea name="txtRemarks" id="txtRemarks" cols="81" rows="2"></textarea></td>
<td width="56" class="normalfnt">Currency</td>
              <td width="199">
              	 <select name="cboBomCurrency" class="txtbox bomCurrency" id="cboBomCurrency" style="width:120px" >
                 <option value=""></option>
                  </select></td>              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td colspan="6">&nbsp;</td>
              </tr>
            <tr>
                <td colspan="7"><div style="width:880px;height:300px;overflow:scroll" >
          <table width="862"  class="grid" id="tblItems" >
            <tr class="gridHeader">
              <td width="19" height="22" >Del</td>
              <td width="120" >Main Category</td>
              <td width="120" >Sub Category</td>
              <td width="220" >Item</td>
              <td width="59" >UOM</td>
              <td width="72" style="display:none" >Currency</td>
              <td width="63" >Unit Price</td>
              <td width="65" >Qty</td>
              <td width="84" >Amount</td>
              </tr>
            <tr class="normalfnt" >
              <td align="center" bgcolor="#FFFFFF"><img src="../../../../images/del.png" width="15" height="15" class="delImg" /></td>
              <td bgcolor="#FFFFFF" align="center"><select name="cboMainCat" class="txtbox mainCat" id="cboMainCat" style="width:120px">
                 <option value=""></option>
                  </select></td>
              <td bgcolor="#FFFFFF"  style="width:70px"><select name="cboSubCat" class="txtbox subCat" id="cboSubCat" style="width:120px">
                  <option value=""></option>
                </select>
              </td>
              <td bgcolor="#FFFFFF"><select name="cboItem" class="txtbox item" id="cboItem" style="width:220px">
                  <option value=""></option>
                </select>
              </td>
              <td bgcolor="#FFFFFF" align="center" class="uom">&nbsp;</td>
              <td bgcolor="#FFFFFF" align="center" class="currency" style="display:none">&nbsp;</td>
              <td bgcolor="#FFFFFF" align="center"><input  id="xx" class="validate[required,custom[number]] calculateValue unitPrice" style="width:60px;text-align:right" type="text" value="" <?php if(($editMode==0) || ($bomUnitPriceEditFlag==0)){  ?> disabled="disabled"<?php } ?>/></td>
              <td bgcolor="#FFFFFF" align="center"><input  id="txtQty" class="validate[required,custom[number]] calculateValue qty" style="width:70px;text-align:right" type="text" value="" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/></td>
              <td bgcolor="#FFFFFF" align="center" class="amount">&nbsp;</td>
              </tr>

          <tr class="dataRow">
            <td colspan="9" align="left"  bgcolor="#FFFFFF" ><img src="../../../../images/Tadd.jpg" name="butInsertRow" width="78" height="24" class="mouseover" id="butInsertRow" /></td>
            </tr>
          </table>
        </div></td>
              </tr> 
              <tr>
              <td colspan="7">
<table width="100%" border="0">
          <tr>
            <td width="17%" class="normalfnt">&nbsp;</td>
            <td width="24%">&nbsp;</td>
            <td width="41%">&nbsp;</td>
            <td width="6%" class="normalfnt"><b>Total</b></td>
            <td width="11%" align="right" class="tableBorder normalfntRight"  ><b><span id="divTotal"><?php echo number_format($total,4) ?></span></b>&nbsp;</td>
            <td width="1%"></td>
            </tr>
          
          </table>              </td>
              </tr>             </table></td>
            </tr>
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img style="display:none" class="mouseover" src="../../../../images/Tconfirm.jpg" width="92" height="24" id="butConfirm" name="butConfirm" /><img style="display:none" border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
</form>
</body>
</html>

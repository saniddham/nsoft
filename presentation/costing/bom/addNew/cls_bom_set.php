<?php
date_default_timezone_set('Asia/Kolkata');
// ini_set('display_errors',1);//echo $_SESSION['ROOT_PATH'];
require_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
//require_once $_SESSION['ROOT_PATH']."class/warehouse/cls_warehouse_get.php";
//require_once $_SESSION['ROOT_PATH']."class/warehouse/cls_warehouse_set.php";
//require_once $_SESSION['ROOT_PATH']."class/warehouse/itemInspection/cls_itemInspection_get.php";


class cls_bom_set
{
	
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	

	
	public function insert_bom_header($project,$subProject,$year,$date,$currency,$remarks,$appLevels,$userId,$company,$location){
		
				$status=$appLevels +1;
		
				$sql = "INSERT INTO `trn_bom_header` (`intProjectId`,`intSubProjectId`,intYear,`dtmDate`,`intCurrencyId`,strRemarks,intStatus,intApproveLevels,dtmCreateDate,intUser,intLocationId,intCompanyId) 
							VALUES ('$project','$subProject','$year','$date','$currency','$remarks','$status','$appLevels',now(),'$userId','$location','$company')";
				//$id=$this->db->AffectedExecuteQuery($sql);
				$result=$this->db->RunQuery2($sql);
				
				$data['result'] = $result;
				if($result==1){
				$data['type'] = 'pass';
				$data['q'] = '';
				$data['msg'] = $db->errormsg;
				}
				else{
				$data['type'] = 'fail';
				$data['q'] = $sql;
				$data['msg'] = $db->errormsg;
				}
				return $data;
	}

	public function update_bom_header($project,$subProject,$year,$date,$currency,$remarks,$appLevels,$userId,$company,$location){
				$status=$appLevels +1;
		
		   $sql = "UPDATE `trn_bom_header` SET dtmDate ='$date', 
													  intCurrencyId ='$currency', 
													  strRemarks ='$remarks', 
													  intApproveLevels ='$appLevels', 
													  intStatus ='$status', 
													  intModifiedBy ='$userId', 
													  dtmModifiedDate =now()   
				WHERE (`intProjectId`='$project') and (`intYear`='$year') and (`intSubProjectId`='$subProject')";
			//	$id=$this->db->AffectedExecuteQuery($sql);
				$result=$this->db->RunQuery2($sql);
				
				$data['result'] = $result;
				if($result==1){
				$data['type'] = 'pass';
				$data['q'] = '';
				$data['msg'] = $db->errormsg;
				}
				else{
				$data['type'] = 'fail';
				$data['q'] = $sql;
				$data['msg'] = $db->errormsg;
				}
				return $data;
	}

	

	public function insert_bom_details($project,$subProject,$year,$itemId,$unitPrice,$qty){
		
		
				$sql = "INSERT INTO `trn_bom_details` (`intProjectId`,`intSubProjectId`,intYear,`intItemId`,`dblQty`,dblUnitPrice) 
							VALUES ('$project','$subProject','$year','$itemId','$qty','$unitPrice')";
		$result=$this->db->RunQuery2($sql);
				$data['result'] = $result;
				if($result==1){
				$data['type'] = 'pass';
				$data['q'] = '';
				$data['msg'] = $db->errormsg;
				}
				else{
				$data['type'] = 'fail';
				$data['q'] = $sql;
				$data['msg'] = $db->errormsg;
				}
				return $data;
	}

	public function delete_bom_details($project,$subProject,$year){
				$sql = "DELETE FROM `trn_bom_details` WHERE (`intProjectId`='$project') and (`intYear`='$year') and (`intSubProjectId`='$subProject')";
				$result=$this->db->RunQuery2($sql);
				
				$data['result'] = $result;
				if($result==1){
				$data['type'] = 'pass';
				$data['q'] = '';
				$data['msg'] = $db->errormsg;
				}
				else{
				$data['type'] = 'fail';
				$data['q'] = $sql;
				$data['msg'] = $db->errormsg;
				}
				return $data;
	}



}
?>
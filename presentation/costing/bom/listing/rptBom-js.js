//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////


// JavaScript Document

$(document).ready(function() {
//	$('#frmlisting').validationEngine();
	
	$('#frmBom #imgApprove').click(function(){
	var val = $.prompt('Are you sure you want to approve?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
						if(validateQuantities()==0){
							var url = "rptBom-db-set.php"+window.location.search+'&status=approve';
							var obj = $.ajax({url:url,async:false});
							window.location.href = window.location.href;
							window.opener.location.reload();//reload listing page
						}
					}
				}});
	});
	
	$('#frmBom #imgReject').click(function(){
	var val = $.prompt('Are you sure you want to reject?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										if(validateRejecton()==0){
											var url = "rptBom-db-set.php"+window.location.search+'&status=reject';
											var obj = $.ajax({url:url,async:false});
											window.location.href = window.location.href;
											window.opener.location.reload();//reload listing page
										}
									}
								}});
	});
	
	$('#frmlisting .butRevise').live('click',reviseBOM);
	
});
//---------------------------------------------------
function validateQuantities(){
		var ret=0;	
	    var projectNo = document.getElementById('divProjectNo').innerHTML;
	    var year = document.getElementById('divYear').innerHTML;
	    var subProjectNo = document.getElementById('divSubProjectNo').innerHTML;
		var url 		= "rptBom-db-get.php?requestType=getValidation";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"projectNo="+projectNo+"&year="+year+"&subProjectNo="+subProjectNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",3000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//---------------------------------------------------
function validateRejecton(){
		var ret=0;	
	    var projectNo = document.getElementById('divProjectNo').innerHTML;
	    var year = document.getElementById('divYear').innerHTML;
	    var subProjectNo = document.getElementById('divSubProjectNo').innerHTML;
		var url 		= "rptBom-db-get.php?requestType=validateRejecton";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"projectNo="+projectNo+"&year="+year+"&subProjectNo="+subProjectNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",3000);
					  ret= 1;
					}
				}
			});
			return ret;
}
function reviseBOM()
{
	
	var year = $(this).parent().parent().find('td:eq(2)').html();
	var project = $(this).parent().parent().find('td:eq(1)').html();
	var subProject = $(this).parent().parent().find('td:eq(3)').html();
	var projectDesc = $(this).parent().parent().find('td:eq(4) a').html();
	var subProjectDesc = $(this).parent().parent().find('td:eq(5) a').html();
	
	var x = confirm('Are you sure want to revise this BOM with Project "'+projectDesc+'" and sub project "'+subProjectDesc+'"');
	if(!x)return;
	
/*	
	
	var url = 'rptBom-db-set.php?status=reviseBOM&projectId='+project+'&subProjectId='+subProject;
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
			document.location.href  = document.location.href;
		}
		});
*/		
	var url 		= "rptBom-db-set.php?status=reviseBOM";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:'projectId='+project+'&subProjectId='+subProject+'&year='+year,
		async:false,
		success:function(json){
				if(json.status=='fail')
				{
				$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
				//alert(json.msg);
				var t=setTimeout("alertx1()",3000);
				}
				var t=setTimeout("alertx1()",3000);
				 alert(json.msg);
				 window.location.href = 'bomListing.php';
			}
		});
		
}
//------------------------------------------------
function alertx()
{
	$('#frmBom #imgApprove').validationEngine('hide')	;
}
//--------------------------------------------
//------------------------------------------------
function alertx1()
{
//	$('#frmlisting .butRevise').validationEngine('hide')	;
}
//--------------------------------------------
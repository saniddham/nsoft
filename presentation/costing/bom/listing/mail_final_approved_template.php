<?php
//$backwardseperator = "../../../../../";
//include  "{$backwardseperator}dataAccess/Connector.php";

$no = $_REQUEST['no'];
$year = $_REQUEST['year'];
$desc1 = $_REQUEST['desc1'];
$desc2 = $_REQUEST['desc2'];
$revNo = $_REQUEST['revNo'];
$mainPath = $_SESSION['mainPath'];
//echo $_REQUEST['x'].'- x';
$customerPO = $_REQUEST['customerPO'];
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BOM Confirmed Email</title>
<style type="text/css">
.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntWhite {
	font-family: Verdana;
	font-size: 11px;
	color: #FFF;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.sampleNo{
	color: #039; font-weight: bold; font-size: 12px; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;	
}
.part{
	color: #096CBD;
	font-weight: bold;
	font-size: 13px;
	font-family: "Comic Sans MS", cursive;
}
.tableBorder_allRound{
	
	border: 1px solid #CCCCCC;
	-moz-border-radius-bottomright:10px;
	-moz-border-radius-bottomleft:10px;
	-moz-border-radius-topright:10px;
	-moz-border-radius-topleft:10px;
}
</style>
</head>

<body>
<table  width="595" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" class="normalfnt" style="color:#3E437D">&nbsp;</td>
  </tr>
  <tr>
    <td width="151" class="normalfnt">Dear <strong>MAILRECIVER_NAME</strong>,</td>
    <td width="440">&nbsp;</td>
    <td width="4">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">I have raised the final approval for a BOM<strong> </strong>to NSOFT system.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">click <a  href="<?php 
	$x_sampleNo 	= $no;
	$x_sampleYear 	= $year;
	$x_customerPO	= $customerPO;
			
			
	echo $mainPath."presentation/costing/bom/listing/rptBom.php?projectId=$no&subProjectId=$year"; ?>">here</a> to view this BOM.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="108" colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="15%" bgcolor="#E6FCD6" class="part">&nbsp;</td>
          <td width="15%" bgcolor="#E6FCD6" class="part">Project</td>
          <td width="15%" bgcolor="#E6FCD6"><?php echo $desc1 ?></td>
          <td width="15%" bgcolor="#E6FCD6" class="part">Sub Project</td>
          <td width="15%" bgcolor="#E6FCD6"><?php echo $desc2 ?></td>
          <td width="15%" bgcolor="#E6FCD6" class="part">&nbsp;</td>
          <td width="16%" bgcolor="#E6FCD6">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td class="normalfnt">Thanks,</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="normalfnt"><strong>MAILFROM_NAME</strong><br />
    ...................</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="normalfnt">(This is a<strong> <span style="color:#F28415">NSOFT</span> </strong>system generated email.)</td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
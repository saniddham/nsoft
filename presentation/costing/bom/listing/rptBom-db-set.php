<?php 

	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	
	$programName='BOM';
	$programCode='P0600';
	
	//ini_set('display_errors',1);
	include "{$backwardseperator}dataAccess/Connector.php";
	include "{$backwardseperator}libraries/mail/mail.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_mail.php";
	
	$objMail = new cls_create_mail($db);
	/////////// parameters /////////////////////////////
	//$requestType 			= $_REQUEST['requestType'];
	
		$projectNo  = $_REQUEST['projectId'];
		$year  = $_REQUEST['year'];
		$subProjectNo  = $_REQUEST['subProjectId'];
	
	if($_REQUEST['status']=='approve')
	{
		$sql = "UPDATE `trn_bom_header` SET `intStatus`=intStatus-1 WHERE (`intProjectId`='$projectNo') AND  (`intYear`='$year') AND(`intSubProjectId`='$subProjectNo') and intStatus not in(0,1)";
		$result = $db->RunQuery($sql);
		
		if($result){
			   $sql = "	SELECT
						trn_bom_header.intStatus,
						trn_bom_header.intApproveLevels, 
						trn_orderheader.strOrderName as strName,
						trn_orderdetails.strSubProjectDesc 
						FROM 
						trn_bom_header
						Inner Join sys_users ON trn_bom_header.intUser = sys_users.intUserId 
						Inner Join trn_orderheader ON trn_bom_header.intProjectId = trn_orderheader.intOrderNo AND trn_bom_header.intYear = trn_orderheader.intOrderYear 
						Inner Join trn_orderdetails ON trn_bom_header.intProjectId = trn_orderdetails.intOrderNo AND trn_bom_header.intSubProjectId = trn_orderdetails.intSalesOrderId AND trn_bom_header.intYear = trn_orderdetails.intOrderYear
						WHERE (trn_bom_header.`intProjectId`='$projectNo')  
						AND (trn_bom_header.`intYear`='$year')
						AND (trn_bom_header.`intSubProjectId`='$subProjectNo')
						";
			$result2 = $db->RunQuery($sql);
			$row2=mysqli_fetch_array($result2);
			$status = $row2['intStatus'];
			$savedLevels = $row2['intApproveLevels'];
			
			$approveLevel=$savedLevels+1-$status;
			 /////////////////// insert approval table ////////////////////
			 $sql = "INSERT INTO `trn_bom_header_approvedby` (`intProjectId`,intYear,`intSubProjectId`,`intApproveLevelNo`,`intApproveUser`,`dtApprovedDate`) VALUES ('$projectNo','$year','$subProjectNo','$approveLevel','$userId',now())";
			 $result = $db->RunQuery($sql);
			 //////////////////////////////////////////////////////////////
			if($status ==1)
			{
				sendFinlConfirmedEmailToUser($projectNo,$subProjectNo,$year,$row2['strName'],$row2['strSubProjectDesc'],$objMail);
			}
		}
		
		
	}
	else if($_REQUEST['status']=='reject')
	{
		
		$sql = "UPDATE `trn_bom_header` SET `intStatus`=0 WHERE (`intProjectId`='$projectNo') AND (`intYear`='$year') AND (`intSubProjectId`='$subProjectNo') ";
		$result = $db->RunQuery($sql);
		
		$sql = "DELETE FROM `trn_bom_header_approvedby` WHERE (`intProjectId`='$projectNo') AND (`intYear`='$year') AND (`intSubProjectId`='$subProjectNo')";
		$result2 = $db->RunQuery($sql);
		
		$sqlI = "INSERT INTO `trn_bom_header_approvedby` (`intProjectId`,intYear,`intSubProjectId`,`intApproveLevelNo`,intApproveUser,dtApprovedDate) 
			VALUES ('$projectNo','$year','$subProjectNo','0','$userId',now())";
		$resultI = $db->RunQuery($sqlI);
		
				 $sql = "	SELECT
							trn_bom_header.intStatus,
							trn_bom_header.intApproveLevels, 
							trn_orderheader.strOrderName as strName,
							trn_orderdetails.strSubProjectDesc 
							FROM 
							trn_bom_header
							Inner Join sys_users ON trn_bom_header.intUser = sys_users.intUserId 
							Inner Join trn_orderheader ON trn_bom_header.intProjectId = trn_orderheader.intOrderNo AND trn_bom_header.intYear = trn_orderheader.intOrderYear 
							Inner Join trn_orderdetails ON trn_bom_header.intProjectId = trn_orderdetails.intOrderNo AND trn_bom_header.intSubProjectId = trn_orderdetails.intSalesOrderId AND trn_bom_header.intYear = trn_orderdetails.intOrderYear
							WHERE
								trn_bom_header.intProjectId =  '$projectNo' AND 
								trn_bom_header.intYear =  '$year' AND 
								trn_bom_header.intSubProjectId =  '$subProjectNo'
							";
				$result2 = $db->RunQuery($sql);
				$row2=mysqli_fetch_array($result2);
				$status = $row2['intStatus'];
			if($status ==0)
			{
				sendRejecedEmailToUser($projectNo,$subProjectNo,$year,$row2['strName'],$row2['strSubProjectDesc'],$objMail);
			}
		
		
	}
	
	
	else if($_REQUEST['status']=='reviseBOM')
	{
		
	  	$sql = "UPDATE `trn_bom_header` SET `intStatus`=`intApproveLevels`+1, `intReviseNo`=`intReviseNo`+1 WHERE (`intProjectId`='$projectNo') AND (`intYear`='$year') AND (`intSubProjectId`='$subProjectNo')";
		$result=$db->RunQuery($sql);
		if($result){
		$sql = "DELETE FROM `trn_bom_header_approvedby` WHERE (`intProjectId`='$projectNo') AND (`intYear`='$year') AND (`intSubProjectId`='$subProjectNo')";
		$resultD=$db->RunQuery($sql);
		}
		
		if($resultD){
			$response['msg'] = "BOM revised successfully.";	
			$response['type'] = 'pass';
		}
		else{
			$response['msg'] = "BOM revision Failed.";	
			$response['type'] = 'fail';
		}
			echo json_encode($response);
		////////////////////////////////////////////////////////
			
	}
//--------------------------------------------------------
function sendFinlConfirmedEmailToUser($projectNo,$subProjectNo,$year,$desc1,$desc2,$objMail){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail , 
			trn_orderheader.strOrderName as strName,
			trn_orderdetails.strSubProjectDesc 
			FROM 
			trn_bom_header
			Inner Join sys_users ON trn_bom_header.intUser = sys_users.intUserId 
			Inner Join trn_orderheader ON trn_bom_header.intProjectId = trn_orderheader.intOrderNo AND trn_bom_header.intYear = trn_orderheader.intOrderYear 
			Inner Join trn_orderdetails ON trn_bom_header.intProjectId = trn_orderdetails.intOrderNo AND trn_bom_header.intSubProjectId = trn_orderdetails.intSalesOrderId AND trn_bom_header.intYear = trn_orderdetails.intOrderYear
			WHERE (trn_bom_header.`intProjectId`='$projectNo') AND (trn_bom_header.`intSubProjectId`='$subProjectNo') AND (trn_bom_header.`intYear`='$year')";

				
		 $result = $db->RunQuery($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
				$header="FINAL APPROVAL RAISED FOR BOM (Project- '".$row['strName']."/".$year."'  / Sub Project- '".$row['strSubProjectDesc']."')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST['no'] 		= $serialNo;
			$_REQUEST['year'] 		= $year;
			$_REQUEST['desc1'] 		= $desc1;
			$_REQUEST['desc2'] 		= $desc2;
			$objMail->send_Response_Mail('mail_final_approved_template.php',$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
function sendRejecedEmailToUser($projectNo,$subProjectNo,$year,$desc1,$desc2,$objMail){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail , 
			trn_orderheader.strOrderName as strName,
			trn_orderdetails.strSubProjectDesc 
			FROM 
			trn_bom_header
			Inner Join sys_users ON trn_bom_header.intUser = sys_users.intUserId 
			Inner Join trn_orderheader ON trn_bom_header.intProjectId = trn_orderheader.intOrderNo AND trn_bom_header.intYear = trn_orderheader.intOrderYear 
			Inner Join trn_orderdetails ON trn_bom_header.intProjectId = trn_orderdetails.intOrderNo AND trn_bom_header.intSubProjectId = trn_orderdetails.intSalesOrderId AND trn_bom_header.intYear = trn_orderdetails.intOrderYear
			WHERE (trn_bom_header.`intProjectId`='$serialNo') AND (trn_bom_header.`intSubProjectId`='$subProjectNo') AND (trn_bom_header.`intYear`='$year')";

				
		 $result = $db->RunQuery($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="REJECTED THE BOM (Project- '".$row['strName']."/".$year."'  / Sub Project- '".$row['strSubProjectDesc']."')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST['no'] 		= $serialNo;
			$_REQUEST['year'] 		= $year;
			$_REQUEST['desc1'] 		= $desc1;
			$_REQUEST['desc2'] 		= $desc2;
			$objMail->send_Response_Mail('mail_rejected_template.php',$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}


// JavaScript Document
var basePath = "presentation/costing/electricityMeterReading/";
$(document).ready(function() {

	$('#view').live('click',pageSubmitOnChange);

	$('.reading').live('keyup',function(){
		calulateUnits(this);
	});


	$('#butSave').live('click',function(){
		
	if ($('#frmMeter').validationEngine('validate'))   
    { 
		showWaiting();
		
		var errorLocFlag=0;
		var errorMeterFlag=0;
		var errorRateFlag=0;
		var location=$('#cboLocation').val();
		var date=$('#dtDate').val();
		
		
		var value="[ ";
		$('.reading').each(function(){
			
			var meterId =$(this).parent().parent().find('.meterName').attr('id');
			var sheduleId =$(this).parent().parent().find('.sheduleName').attr('id');
			var rate =parseFloat($(this).parent().parent().find('.rate').val());
			var meterReading =parseFloat($(this).val());
			
			if(location==''){
				errorLocFlag++;
			}
			if((meterReading=='') || (meterReading==0)){
				errorMeterFlag++;
			}
			if((rate=='') || (rate==0)){
				errorRateFlag++;
			}
			
			value +='{"meterId":"'+meterId+'","sheduleId":"'+sheduleId+'","rate":"'+rate+'","meterReading":"'+meterReading+'"},' ;	
			//value +='{"manager":"'+manager+'"},' ;	
		})
		
		if(errorLocFlag>0){
			alert("Please Enter Location");
			return false;
		}
		else if(errorMeterFlag>0){
			alert("Please Enter Meter Reading");
			return false;
		}
		else if(errorRateFlag>0){
			alert("Please Enter Rate");
			return false;
		}
		
		value = value.substr(0,value.length-1);
		value += " ]";
		
		var url = basePath+"electricityMeterReading-db-set.php?requestType=saveData&location="+location+"&date="+date;
					$.ajax({
							url:url,
							async:false,
							dataType:'json',
							type:'post',
							data:'&userDetails='+value,
						success:function(json){
							$('#frmMeter #butSave').validationEngine('showPrompt', json.msg,json.type );
							if(json.type=='pass')
							{
								var t=setTimeout("alertx()",1000);
								setTimeout("window.location.reload();",1000)
								return;
							}
						},
						error:function(xhr,status){
							
							$('#frmMeter #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",1000);
							return;
						}
					});
	}
	});
	
	
	
});

function alertx()
{
	$('#frmMeter #butSave').validationEngine('hide')	;
}
function pageSubmitOnChange()
{
	var date 		= $('#dtDate').val();
	var location 		= $('#cboLocation').val();
	
	if(date==''){
		alert('Please select the date');
		return false;
	}
	else if(location==''){
		alert('Please select the location');
		return false;
	}
	
	if(date!='' && location!='')
	{
		document.getElementById('frmMeter').submit();
	}
}

function calulateUnits(obj){
	
	var meterId =$(obj).parent().parent().find('.meterName').attr('id');
	var sheduleId =$(obj).parent().parent().find('.sheduleName').attr('id');
	var meterReading =parseFloat($(obj).val());
	var location=$('#cboLocation').val();
	var date=$('#dtDate').val();

	var url 		= basePath+"electricityMeterReading-db-get.php?requestType=loadUnits&meterId="+meterId+"&sheduleId="+sheduleId+"&location="+location+"&date="+date+"&meterReading="+meterReading;
	var httpobj 	= $.ajax({url:url,type:'POST',async:false})
	$(obj).parent().parent().find('.units').html(httpobj.responseText);
}

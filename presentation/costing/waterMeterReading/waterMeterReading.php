<?php
ini_set('display_errors',0);
$locationId 	= $_SESSION['CompanyID'];


	$date 		= (isset($_REQUEST['dtDate'])?$_REQUEST['dtDate']:'');
	$location 	= (isset($_REQUEST['cboLocation'])?$_REQUEST['cboLocation']:'');


if($location==''){
	$location=$locationId;
}
if($date==''){
	$date=date('Y-m-d');
}


?>
<head>
<title>Water Meter Reading</title>
	<script type="text/javascript" src="presentation/costing/waterMeterReading/waterMeterReading-js.js"></script>
</head>

<body>
<form id="frmMeter" name="frmMeter" autocomplete="off" method="post">
<div align="center">
<div class="trans_layoutL" style="width:680">
		  <div class="trans_text">Water Meter Reading</div>
		  <table width="580" border="0" align="center" bgcolor="#FFFFFF">
    <td align="center"><table width="100%" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="37" ><table width="112%" border="0" class="">
              <tr>
                <td  class="normalfnt" colspan="3">
                <table width="112%" border="0" class="">
                <tr>
                <td width="6%" class="normalfnt">Date</td>
                <td width="20%"><input name="dtDate" type="text" value="<?php if($date){ echo $date; }else { echo date("Y-m-d"); }?>" class="txtbox" id="dtDate" style="width:80px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                <td width="10%" class="normalfnt">Location</td>
                <td width="41%"><span class="normalfnt clsLocation">
                  <select style="width:220px" name="cboLocation" id="cboLocation"  class="validate[required]"  >
                    <option value=""></option>
                    <?php
					$sqlm = "SELECT
							mst_locations.intId,
							mst_locations.strName
							FROM `mst_locations`
							WHERE
							mst_locations.intStatus = 1
							ORDER BY
							mst_locations.strName ASC
							";
					$resultm = $db->RunQuery($sqlm);
					while($rowm=mysqli_fetch_array($resultm))
					{
					if($location==$rowm['intId'])
						echo "<option value=\"".$rowm["intId"]."\" selected=\"selected\" >".$rowm["strName"]."</option>";
					else
						echo "<option value=\"".$rowm['intId']."\">".$rowm['strName']."</option>";	
					}
				?>
                  </select>
                </span></td>
                <td width="23%"><img src="images/Tview.jpg" width="92" height="24" alt="view" id="view" class="mouseover" /></td>
                </tr>
                </table>
                </td>
              </tr>
              <tr>
                <td colspan="3" align="center"><table id="tblMain" width="100%" border="0" class="grid">
                  
                  <tr class="gridHeader">
                    <td colspan="6" bgcolor="#FFFFFF" class="normalfntRight">&nbsp;</td>
                    </tr>
                  <tr class="gridHeader">
                    <td width="37%" >Meter</td>
                    <td width="21%" >Last Reading Date</td>
                    <td width="18%" >Last Reading</td>
                    <td width="15%" >Reading</td>
                    <td width="9%" >Units</td>
                    </tr>
                  <?php
				  
				   $sql = " SELECT
							(SELECT
							(CWR.DATE)
							FROM `cost_water_reading` as CWR 
							WHERE
							CWR.LOCATION_ID = mst_water_meters.LOCATION_ID AND 
							CWR.METER_ID = mst_water_meters.METER_ID AND
							CWR.DATE < '$date' order by CWR.DATE DESC LIMIT 1) as LAST_READING_DATE,
							
							IFNULL((SELECT
							CWR.METER_READING 
							FROM `cost_water_reading` as CWR 
							WHERE
							CWR.LOCATION_ID = mst_water_meters.LOCATION_ID AND 
							CWR.METER_ID = mst_water_meters.METER_ID AND
							CWR.DATE < '$date' order by CWR.DATE DESC LIMIT 1),0) as LAST_READING,
							 mst_water_meters.METER_ID,
							mst_water_meters.METER_NAME,
							cost_water_reading.METER_READING,
							cost_water_reading.LOCATION_ID,
							mst_water_meters.LOCATION_ID
							FROM
							mst_water_meters
							LEFT JOIN cost_water_reading ON cost_water_reading.METER_ID = mst_water_meters.METER_ID AND cost_water_reading.LOCATION_ID = mst_water_meters.LOCATION_ID AND cost_water_reading.DATE = '$date'
							WHERE
							mst_water_meters.LOCATION_ID = '$location' 
							ORDER BY
							 mst_water_meters.METER_NAME ASC ";
				  $result = $db->RunQuery($sql);
				  $i=0;
				  while($row=mysqli_fetch_array($result))
				  {
					  $i++;
					  $productionPrapotion=getProductionPrapotion($location,$row['LAST_READING_DATE'],$date);
					  $units=($productionPrapotion)*($row['METER_READING']-$row['LAST_READING']);
				 ?>
					<tr id="<?php echo $row['METER_ID'] ?>" class="normalfnt">
                    <td bgcolor="#FFFFFF" class="normalfnt meterName" align="left" id="<?php echo $row['METER_ID']; ?>"><?php echo $row['METER_NAME']; ?></td>
                    <td bgcolor="#FFFFFF" class="normalfnt lmrDate" align="centet"><?php echo $row['LAST_READING_DATE']; ?></td>
                    <td bgcolor="#FFFFFF" class="normalfntRight lmr" align="centet"><?php echo $row['LAST_READING']; ?></td>
                    <td bgcolor="#FFFFFF" class="normalfntMid meter" align="centet"><input type="text" name="txtMeterReding" id="txtMeterReding"  value="<?php echo $row['METER_READING']; ?>" class="validate[required,custom[number]] reading" style="text-align:right; width:80px"/></td>
                    <td bgcolor="#FFFFFF" class="normalfntRight units" align="centet"><?php echo round($units,2); ?></td>
                    </tr>				 
				 <?php
				  }
				  
				  ?>  
                </table></td>
                </tr>
<tr>
            <td height="34" colspan="3"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><img style="display:" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/>
                  <?php
                if($form_permision['add']||$form_permision['edit'])
				{
				?>
                  <img  style="display:" border="0" src="images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/>
                  <?php
				}
                if($form_permision['delete'])
				{
				?>
                  <img style="display:" border="0" src="images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/>
                  <?Php 
				}
				?>
                  <a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
              </tr>
            </table></td>
            </tr>  
                        <tr height="25">
                <td colspan="3" class="normalfnt"></td>
              </tr>  
              <tr>
                <td colspan="3" align="center">
                <div style="height:300px;overflow:scroll" >
                <table id="tblMainSaved" width="100%" border="0" class="grid">
                  
                  <tr class="gridHeader">
                    <td width="17%" >Date</td>
                    <td width="59%" >Meter</td>
                    <td width="15%" >Reading</td>
                    <td width="9%" >Units</td>
                    </tr>
                  <?php
				  
				  $sql = " SELECT
							cost_water_reading.DATE,
							mst_water_meters.METER_NAME,
							cost_water_reading.METER_READING, 
							(SELECT
							(CWR.DATE)
							FROM `cost_water_reading` as CWR 
							WHERE
							CWR.LOCATION_ID = mst_water_meters.LOCATION_ID AND 
							CWR.METER_ID = mst_water_meters.METER_ID AND
							CWR.DATE < cost_water_reading.DATE 
							order by CWR.DATE DESC LIMIT 1) as LAST_READING_DATE ,

							IFNULL((SELECT
							CWR.METER_READING 
							FROM `cost_water_reading` as CWR 
							WHERE
							CWR.LOCATION_ID = mst_water_meters.LOCATION_ID AND 
							CWR.METER_ID = mst_water_meters.METER_ID AND
							CWR.DATE < cost_water_reading.DATE 
							order by CWR.DATE DESC LIMIT 1),0) as LAST_READING 

							FROM
							cost_water_reading
							INNER JOIN mst_water_meters ON cost_water_reading.METER_ID = mst_water_meters.METER_ID AND cost_water_reading.LOCATION_ID = mst_water_meters.LOCATION_ID
							WHERE 
							mst_water_meters.LOCATION_ID = '$location'
							ORDER BY
					 		cost_water_reading.DATE DESC,
							mst_water_meters.METER_NAME ASC
							 ";
				  $result = $db->RunQuery($sql);
				  $i=0;
				  while($row=mysqli_fetch_array($result))
				  {
					  $productionPrapotion=getProductionPrapotion($location,$row['LAST_READING_DATE'],$row['DATE']);
					  $units=($productionPrapotion)*($row['METER_READING']-$row['LAST_READING']);
					  $i++;
				 ?>
                    <tr>
                      <td bgcolor="#FFFFFF" class="normalfnt meter" align="left"><?php echo $row['DATE']; ?></td>
                    <td bgcolor="#FFFFFF" class="normalfnt meter" align="left"><?php echo $row['METER_NAME']; ?></td>
                    <td bgcolor="#FFFFFF" class="normalfntRight meter" align="left"><?php echo $row['METER_READING']; ?></td>
                    <td bgcolor="#FFFFFF" class="normalfntRight meter" align="left"><?php echo round($units,2); ?></td>
                    </tr>				 
				 <?php
				  }
				  
				  ?>  
                </table>
                </div>
                </td>
                </tr>
              </table></td>
            </tr>
          
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
</form>
</body>
 <div    style="width:500px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
 <div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
<?php
function getProductionPrapotion($location,$lastReadingDate,$curruntDate){
	
		global $db;
		     $sql = "SELECT 
					sum(IFNULL(ware_fabricdispatchdetails.dblFdammageQty,0)+ IFNULL(ware_fabricdispatchdetails.dblPDammageQty,0)+ IFNULL(ware_fabricdispatchdetails.dblEmbroideryQty,0)+ IFNULL(ware_fabricdispatchdetails.dblSampleQty,0)+ IFNULL(ware_fabricdispatchdetails.dblGoodQty,0)+ IFNULL(ware_fabricdispatchdetails.dblCutRetQty,0)) AS qty
					FROM
					ware_fabricdispatchheader
					INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear
					WHERE
					ware_fabricdispatchheader.intStatus = 1 AND
					ware_fabricdispatchheader.dtmdate <= '$curruntDate'  
					AND ware_fabricdispatchheader.dtmdate > '$lastReadingDate' 
					AND ware_fabricdispatchheader.intCompanyId='$location' ";
	
			$result = $db->RunQuery($sql);
			$rows = mysqli_fetch_array($result);
			$total=round($rows['qty']);

		   $sql = "SELECT 
					sum(IFNULL(ware_fabricdispatchdetails.dblFdammageQty,0)+ IFNULL(ware_fabricdispatchdetails.dblPDammageQty,0)+ IFNULL(ware_fabricdispatchdetails.dblEmbroideryQty,0)+ IFNULL(ware_fabricdispatchdetails.dblSampleQty,0)+ IFNULL(ware_fabricdispatchdetails.dblGoodQty,0)+ IFNULL(ware_fabricdispatchdetails.dblCutRetQty,0)) AS qty
					FROM
					ware_fabricdispatchheader
					INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear
					WHERE
					ware_fabricdispatchheader.intStatus = 1 AND
					ware_fabricdispatchheader.dtmdate = '$curruntDate' 
					AND ware_fabricdispatchheader.intCompanyId='$location' ";
	
			$result = $db->RunQuery($sql);
			$rows = mysqli_fetch_array($result);
			$curruntQty=round($rows['qty']);
			$prapotion=$curruntQty/$total;
			
			return $prapotion;
}

?>
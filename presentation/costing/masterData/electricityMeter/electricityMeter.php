<?php
include "include/javascript.html";
?>
<head>
<title>Electricity Meter</title>
<script type="text/javascript" src="presentation/costing/masterData/electricityMeter/electricityMeter-js.js"></script>
</head>
<body>
<form id="frmMeter" name="frmMeter"   autocomplete="off">
<div align="center">
<div class="trans_layoutS">
		  <div class="trans_text">Electricity Meter</div>
		  <table width="400" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="37" ><table width="112%" border="0" class="">
              <tr>
                <td  class="normalfnt">&nbsp;</td>
                <td wclass="normalfnt">&nbsp;</td>
                <td >&nbsp;</td>
              </tr>
              <tr>
                <td colspan="3" class="normalfnt"><table id="tblMain" width="100%" border="0" class="grid">
                  
                  <tr class="gridHeader">
                    <td colspan="4" bgcolor="#FFFFFF" class="normalfntRight"><img src="images/insert.png" name="butAdd" width="78" height="24" id="butAdd" class="mouseover" /></td>
                    </tr>
                  <tr class="gridHeader">
                    <td width="21%" >Delete</td>
                    <td width="79%" >Location</td>
                    <td width="79%" >Meter</td>
                    </tr>
                  <?php
				  
				  $sql = " SELECT
							mst_electricity_meters.METER_ID,
							mst_electricity_meters.LOCATION_ID,
							mst_electricity_meters.METER_NAME
							FROM
							mst_electricity_meters
							INNER JOIN mst_locations ON mst_electricity_meters.LOCATION_ID = mst_locations.intId
							ORDER BY
							mst_locations.strName ASC,
							mst_electricity_meters.METER_NAME ASC ";
				  $result = $db->RunQuery($sql);
				  $i=0;
				  while($row=mysqli_fetch_array($result))
				  {
					  $i++;
				 ?>
					<tr id="<?php echo $row['METER_ID'] ?>" class="normalfnt">
                    <td style="text-align:center" bgcolor="#FFFFFF" class="normalfnt"><img class="mouseover removeRow" src="images/del.png" /></td>
                    <td bgcolor="#FFFFFF" class="normalfnt clsLocation" align="left"><select style="width:191px" name="cboLocation" id="cboLocation"   class="validate[required]">
                <option value=""></option>
                <?php
					$sqlm = "SELECT
							mst_locations.intId,
							mst_locations.strName
							FROM `mst_locations`
							WHERE
							mst_locations.intStatus = 1
							ORDER BY
							mst_locations.strName ASC
							";
					$resultm = $db->RunQuery($sqlm);
					while($rowm=mysqli_fetch_array($resultm))
					{
						if($rowm['intId']==$row['LOCATION_ID'])
						echo "<option value=\"".$rowm['intId']."\" selected=\"selected\">".$rowm['strName']."</option>";	
						else
						echo "<option value=\"".$rowm['intId']."\">".$rowm['strName']."</option>";	
					}
				?>
              </select></td>
                    <td bgcolor="#FFFFFF" class="normalfnt meter" align="left"><input type="text" name="txtMeter" id="txtMeter" width="80px" value="<?php echo $row['METER_NAME']; ?>" /></td>
                    </tr>				 
				 <?php
				  }
				  
				  ?>  
                  <?php
				  if($i==0){
				  ?> 
					<tr class="normalfnt">
                    <td style="text-align:center" bgcolor="#FFFFFF" class="normalfnt"><img class="mouseover removeRow" src="images/del.png" /></td>
                    <td bgcolor="#FFFFFF" class="normalfnt clsLocation" align="left"><select style="width:191px" name="cboLocation" id="cboLocation"   class="validate[required]">
                <option value=""></option>
                <?php
					$sqlm = "SELECT
							mst_locations.intId,
							mst_locations.strName
							FROM `mst_locations`
							WHERE
							mst_locations.intStatus = 1
							ORDER BY
							mst_locations.strName ASC
							";
					$resultm = $db->RunQuery($sqlm);
					while($rowm=mysqli_fetch_array($resultm))
					{
						echo "<option value=\"".$rowm['intId']."\">".$rowm['strName']."</option>";	
					}
				?>
              </select></td>
                    <td bgcolor="#FFFFFF" class="normalfnt meter" align="left"><input type="text" name="txtMeter" id="txtMeter" width="80px" value="" /></td>
                    </tr>				 
                  <?php
				  }
				  ?>  
                </table></td>
                </tr>
              </table></td>
            </tr>
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><img style="display:" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/>
                  <?php
                if($form_permision['add']||$form_permision['edit'])
				{
				?>
                  <img  style="display:" border="0" src="images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/>
                  <?php
				}
                if($form_permision['delete'])
				{
				?>
                  <img style="display:" border="0" src="images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/>
                  <?Php 
				}
				?>
                  <a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
              </tr>
            </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
</form>
</body>
 <div    style="width:500px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
 <div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</html>

// JavaScript Document
var basePath = "presentation/costing/masterData/waterMeter/";
$(document).ready(function() {
	
	//$("#frmProductionAllocation").validationEngine();
	var locationCombo=getLocationCombo();
	
	setRemoveRow();
	$('#butAdd').live('click',function(){
						document.getElementById('tblMain').insertRow(document.getElementById('tblMain').rows.length);
						document.getElementById('tblMain').rows[document.getElementById('tblMain').rows.length-1].innerHTML = "<td style=\"text-align:center\" bgcolor=\"#FFFFFF\" class=\"normalfnt\">"+
						"<img class=\"mouseover removeRow\" src=\"../../../../images/del.png\" /></td>"+
						"<td bgcolor=\"#FFFFFF\" class=\"normalfnt clsLocation\">"+locationCombo+"</td>"+
						"<td bgcolor=\"#FFFFFF\" class=\"normalfnt meter\"><input type=\"text\" name=\"txtMeter\" id=\"txtMeter\" width=\"80px\" value=\"\" /></td>";
						//document.getElementById('tblMain').rows[document.getElementById('tblMain').rows.length-1].id  = fromId;	
	});
	//setRemoveRow();

	$('#butSave').live('click',function(){
		var errorLocFlag=0;
		var errorMeterFlag=0;
		
		var value="[ ";
		$('.clsLocation').each(function(){
			
			var meterId = $(this).parent().attr('id');
			var location = $(this).parent().find("#cboLocation").val();
			var meter = $(this).parent().find("#txtMeter").val();
			if(location==''){
				errorLocFlag++;
			}
			if(meter==''){
				errorMeterFlag++;
			}
			
			value +='{"meterId":"'+meterId+'","location":"'+location+'","meter":"'+meter+'"},' ;	
			//value +='{"manager":"'+manager+'"},' ;	
		})
		
		if(errorLocFlag>0){
			alert("Please Enter Location");
			return false;
		}
		else if(errorMeterFlag>0){
			alert("Please Enter Meter Name");
			return false;
		}
		
		value = value.substr(0,value.length-1);
		value += " ]";
		
		var url = basePath+"waterMeter-db-set.php?requestType=saveData";
					$.ajax({
							url:url,
							async:false,
							dataType:'json',
							type:'post',
							data:'&userDetails='+value,
						success:function(json){
							$('#frmMeter #butSave').validationEngine('showPrompt', json.msg,json.type );
							if(json.type=='pass')
							{
								var t=setTimeout("alertx()",1000);
								//setTimeout("window.location.reload();",1000)
								return;
							}
						},
						error:function(xhr,status){
							
							$('#frmMeter #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",1000);
							return;
						}
					});
	});
	
});
function setRemoveRow()
{
	$('.removeRow').live('click',function(){
		 $(this).parent().parent().remove();
		 //-------
			var meterId = $(this).parent().parent().attr('id');
			var location = $(this).parent().parent().find("#cboLocation").val();
			var meter = $(this).parent().parent().find("#txtMeter").val();
 			//alert(meterId);
			if(meterId!=''){
			value ='[{"meterId":"'+meterId+'","location":"'+location+'","meter":"'+meter+'"}]' ;	
			var url = basePath+"waterMeter-db-set.php?requestType=deleteData";
				$.ajax({
						url:url,
						async:false,
						dataType:'json',
						type:'post',
						data:'&userDetails='+value,
					success:function(json){
						//$(this).attr('id').validationEngine('showPrompt', json.msg,json.type );
						$('#frmMeter #butSave').validationEngine('showPrompt', json.msg,json.type );
						if(json.type=='pass')
						{
							var t=setTimeout("alertx()",1000);
							//setTimeout("window.location.reload();",1000)
							return;
						}
					},
					error:function(xhr,status){
						
						$('#frmMeter #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertx()",1000);
						return;
					}
				});
				
			}
		//-----
		});	
}
function alertx()
{
	$('#frmMeter #butSave').validationEngine('hide');
}
function getLocationCombo(){
	var url 	= basePath+"waterMeter-db-get.php?requestType=getLocationCombo";
	var httpobj = $.ajax({url:url,async:false})
	return httpobj.responseText;
}
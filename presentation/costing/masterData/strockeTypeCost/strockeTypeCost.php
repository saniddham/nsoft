<?php
$search 	= $_REQUEST['cboSearch'];
include "include/javascript.html";
?>
<head>
<title>Stroke Cost</title>
	<script type="text/javascript" src="presentation/costing/masterData/strockeTypeCost/strockeTypeCost-js.js"></script>
</head>

<body>
<form id="frmStrockeTypeCosting" name="frmStrockeTypeCosting" method="post" autocomplete="off"  >
<div align="center">
  <div class="trans_layoutD">
	<div class="trans_text">Shot Cost</div>
    <table width="100%">
<tr>
<td width="100%"><table width="100%">
  <tr>
    <td width="92" class="normalfnt">&nbsp;</td>
    <td width="185" class="normalfnt">Location</td>
    <td colspan="2" bgcolor="#FFFFFF" class=""><select name="cboSearch" id="cboSearch" style="width:250px">
              <option value=""></option>
              <?php
				$sql = "SELECT
						mst_locations.strName AS location,
						mst_locations.intId
						FROM
						mst_locations

				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($search==$row['location']){
						echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['location']."</option>";
						$strPrcs=$row['strProcess'];
					}
					else
						echo "<option value=\"".$row['intId']."\">".$row['location']."</option>";
				}
				  ?>
                </select></td>
    <td width="62" bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td width="330" bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td width="36" bgcolor="#FFFFFF" class="">&nbsp;</td>
  </tr>
  <tr>
    <td class="normalfnt">&nbsp;</td>
    <td width="185" class="normalfnt">Shot Cost</td>
    <td width="112" bgcolor="#FFFFFF" class="">
    <input name="txtProcess" type="text" id="txtProcess" style="width:100px" value="<?php echo $strPrcs; ?>"/></td>
    <td width="449" bgcolor="#FFFFFF" class=""> <font size="2">$</font></td>
    <td bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td bgcolor="#FFFFFF" class=""><img  src="images/Tadd.jpg" alt="add" name="butAdd" width="92" height="24" border="0"  class="mouseover" id="butAdd" tabindex="27"/></td>
    <td bgcolor="#FFFFFF" class="">&nbsp;</td>
  </tr>
</table></td>
</tr>
<tr>
  <td><div style="overflow:scroll;width:100%;height:350px;" id="divGrid">
    <table width="100%" id="tblMainGrid" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900" class="bordered">
      <tr class="">
        <th width="67%" height="27"  class="normalfntMid"><strong>Location</strong></th>
        <th width="14%" class="normalfntMid"><strong>Cost</strong></th>
        </tr>
      <?php
	  $sql = "SELECT
				mst_costingstrocktype.COST,
				mst_locations.strName AS location,
				mst_costingstrocktype.LOCATION locationId
				FROM
				mst_costingstrocktype
				INNER JOIN mst_locations ON mst_costingstrocktype.LOCATION = mst_locations.intId
				WHERE 
				mst_locations.intStatus =  '1' and 
				mst_costingstrocktype.intStatus =  '1'

";
	/*	if($search){
		$sql .= "AND
				mst_costingstrocktype.intId =  '$search'
				";	
		}
	*/
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$locationId = $row['locationId'];
			$location=$row['location'];
			$Cost=$row['COST'];
	  ?>
      	<tr class="normalfnt">
        <td align="center" bgcolor="#FFFFFF" class="process" id="<?php echo $locationId;?>"><?php echo $location;?></td>
        <td align="center" bgcolor="#FFFFFF"><input name="txtCost" type="text" id="txtCost" class="validate[custom[number]] cost" style="width:80px; text-align:right"  value="<?php echo $Cost;?>"/></td>
        </tr>
      <?php 
        } 
       ?>
    </table>
  </div></td>
</tr>
<tr>
  <td width="100%" align="center" bgcolor=""><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
    <tr>
      <td width="100%" align="center" bgcolor=""><img style="display:" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/>
        <?php
                if($form_permision['add']||$form_permision['edit'])
				{ 
				?>
        <img  style="display:" border="0" src="images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/>
       
        <?Php 
				}
				?>
        <a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
    </tr>
  </table></td>
</tr>
</table>
</div>
</div>
</form>   
</body>
</html>
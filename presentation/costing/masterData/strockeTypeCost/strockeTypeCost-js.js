var basePath = "presentation/costing/masterData/strockeTypeCost/";
function functionList()
{
}		
	
$(document).ready(function() {
	
	$("#frmStrockeTypeCosting").validationEngine();
	$('#frmStrockeTypeCosting #cboSearch').focus();
	
	$('#butAdd').click(function(){
		if(document.getElementById('txtProcess').value.trim()==''){
			return false;
		}

		var template	="<tr class=\"normalfnt\"><td class=\"process\" bgcolor=\"#FFFFFF\" align=\"center\" id=\""+$('#cboSearch').val()+"\">"+$("#cboSearch option[value='"+$('#cboSearch').val()+"']").text()+"</td><td bgcolor=\"#FFFFFF\" align=\"center\"><input id=\"txtCost\" class=\"validate[custom[number]] cost\" name=\"txtCost\" style=\"width:80px; text-align:right\" value=\""+$('#txtProcess').val()+"\" type=\"text\"></td></tr>";
		$('#tblMainGrid').html($('#tblMainGrid').html()+template);
		$('#cboSearch').val('');
		$('#txtProcess').val('');
		
	});
  //-----------------------------------------------------------
  	$('#butDelete').click(deleteDetails);
//--------------------------------------------------
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

  ///save button click event
  $('#frmStrockeTypeCosting #butSave').click(function(){
	var requestType = '';
	if ($('#frmStrockeTypeCosting').validationEngine('validate'))   
    { 
	
		var data = "requestType=save";
			var arr="[";
			
			var rowCount = document.getElementById('tblMainGrid').rows.length;
			if(rowCount==1){
				alert("No records to save");
				return false;	
			}
			var rw=0;
			for(var i=1;i<rowCount;i++)
			{
					var location 	= document.getElementById('tblMainGrid').rows[i].cells[0].id;
					var Cost 		= document.getElementById('tblMainGrid').rows[i].cells[1].childNodes[0].value;
					
					if(Cost==0){
						alert("Please select cost");
						document.getElementById('tblMainGrid').rows[i].cells[1].childNodes[0].focus();
						return false;
					}
					if(Cost>0 ){
					 arr += "{";
						arr += '"location":"'+	location +'",' ;
						arr += '"Cost":"'+	Cost +'"' ;
						arr +=  '},';
						rw++;
					}
			}
			
			if(rw==0){
				alert("Please select cost to save");
				return false;
			}
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;
			
		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basePath + "strockeTypeCost-db-set.php";
     	var obj = $.ajax({
			url:url,		
			dataType: "json", 
			type: 'post', 
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmStrockeTypeCosting #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						reloadCombo();
						window.location.reload(true);
						var t=setTimeout("alertx()",1000);
						return;
					}
				},
			error:function(xhr,status){
					$('#frmStrockeTypeCosting #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
			
	}
   });
   
   
   /////////////////////////////////////////////////////
   //// load location details //////////////////////////
   /////////////////////////////////////////////////////
	
});



//--------------------------------------------------------------------
function alertx()
{
	$('#frmStrockeTypeCosting #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmStrockeTypeCosting #butDelete').validationEngine('hide')	;
		document.location.href=document.location.href;

}
//----------------------------------------------------------------------
function reloadCombo(){
	var url 		= basePath+"strockeTypeCost-db-get.php?requestType=loadCombo";
	var httpobj 	= $.ajax({url:url,async:false})
	document.getElementById('cboSearch').innerHTML=httpobj.responseText;
}
//------------------------------------------------------------------------
function deleteDetails()
{			
			var val = $.prompt('Are you sure you want to delete" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
									var data = "requestType=delete";
										var arr="[";
										
										var rowCount = document.getElementById('tblMainGrid').rows.length;
										var rw=0;
										for(var i=1;i<rowCount;i++)
										{
											var Id = 	document.getElementById('tblMainGrid').rows[i].cells[1].id;
											if(document.getElementById('tblMainGrid').rows[i].cells[0].childNodes[0].checked==true){
											 arr += "{";
												arr += '"Id":"'+		Id +'"' ;
												arr +=  '},';
												rw++;
											}
										}
										
										if(rw==0){
											alert("No records to delete");
											return false;	
										}
										arr = arr.substr(0,arr.length-1);
										arr += " ]";
										
										data+="&arr="	+	arr;
							
									///////////////////////////// save main infomations /////////////////////////////////////////
									var url = basePath+"strockeTypeCost-db-set.php";
									var obj = $.ajax({
										url:url,
										
										dataType: "json",
										type:'post',  
										data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
										//data:'{"requestType":"addsampleInfomations"}',
										async:false,
							
										success:function(json){
											
											$('#frmStrockeTypeCosting #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
											if(json.type=='pass')
											{
												$('#frmStrockeTypeCosting').get(0).reset();
												reloadCombo();
												var t=setTimeout("alertDelete()",1000);return;
											}
											var t=setTimeout("alertDelete()",3000);
										}	 
									});
								}
								}
});
			
	//});
	//clearForm();
}
//------------------------------------------------------------
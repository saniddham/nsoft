<?php
$sql = "SELECT MAX(intId) AS colorId FROM mst_colors order by colorId DESC";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result)){
	$colorCode = $row['colorId'] + 1000;
}

$category 	= $_REQUEST['cboSearch'];

?>
<script type="application/javascript" >
	var colorCode = <?php echo $colorCode ?>;
</script>
<head>
<title>Ink Type Costing</title>
<!--	<script type="text/javascript" src="presentation/costing/masterData/inkTypeCost/addNew/inkTypeCosting-js.js"></script>     
--></head>

<body >
<form id="frmInkTypeCosting" name="frmInkTypeCosting" method="post" autocomplete="off"  >
<div align="center">
		<div class="trans_layoutD" style="width:800px">
		  <div class="trans_text">Ink Type Costing</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0" align="center">
   <tr>
    <td width="3" class="normalfnt">&nbsp;</td>
    <td width="220" class="normalfnt">Category<span class="compulsoryRed"></span></td>
    <td colspan="2" bgcolor="#FFFFFF" class=""><select name="cboSearch" id="cboSearch" style="width:250px" onChange="submit();">
                  <option value=""></option>
              <?php
				$sql = "SELECT
						mst_ink_type_category.ID,
						mst_ink_type_category.DESCRIPTION 
						FROM mst_ink_type_category
						WHERE
						mst_ink_type_category.STATUS =  '1'
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($category==$row['ID']){
						echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['DESCRIPTION']."</option>";
						$strPrcs=$row['strProcess'];
					}
					else
						echo "<option value=\"".$row['ID']."\">".$row['DESCRIPTION']."</option>";
				}
				  ?>
                </select></td>
    <td width="26" bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td width="154" bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td width="6" bgcolor="#FFFFFF" class="">&nbsp;</td>
  </tr>      <tr>
        <td colspan="7"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
              

              <tr>
                <td class="normalfnt" colspan="4">
           <table width="100%" class="bordered" id="tblMainGrid" >
      <tr class="">
        <th width="33%" height="27"  class="normalfntMid"><span class="normalfnt"><strong>Ink Type</strong></span></th>
        <th width="30%"  class="normalfntMid"><span class="normalfnt"><strong>Consumption Per Square Inch(kg)</strong></span></th>
        <th width="18%"  class="normalfntMid"><span class="normalfnt"><strong>Price per kg ($)</strong></span></th>
        <th width="19%"  class="normalfntMid"><span class="normalfnt"><strong>Cost Per Square Inch ($)</strong></span></th>
        <th width="19%"  class="normalfntMid"><span class="normalfnt"><strong>Save</strong></span></th>
        <th width="19%"  class="normalfntMid" nowrap><span class="normalfnt"><strong>Approve Ink Type</strong></span></th>
       </tr>
      <?php
	  $sql = "SELECT
				mst_inktypes.intId,
				mst_inktypes.strName, 
				mst_costinginktypecosting.intCurrencyId,
				mst_costinginktypecosting.dblConsPerSqInch,
				mst_costinginktypecosting.dblPricePerKg,
				mst_costinginktypecosting.dblCostPerInch,
				mst_inktypes.intStatus as inkTypeMasterStatus 
				FROM
				mst_inktypes
				LEFT JOIN mst_costinginktypecosting ON mst_inktypes.intId = mst_costinginktypecosting.intInkTypeId
				INNER JOIN mst_ink_type_category ON mst_inktypes.INK_TYPE_CATEGORY = mst_ink_type_category.ID
				WHERE
				mst_inktypes.INK_TYPE_CATEGORY = '$category' and mst_inktypes.intStatus <>0
				ORDER BY
				mst_inktypes.strName ASC";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
	  ?>
      <tr class="normalfnt">
     <td id=<?php echo $row['intId'];?> class="loadId" align="center" bgcolor="#FFFFFF"><?php echo $row['strName'];?></td>
     <td align="center" bgcolor="#FFFFFF"><input type="text" name="txtConsPerSqInch" id="txtConsPerSqInch" style="text-align:right;width:100px;" value="<?php echo $row['dblConsPerSqInch'];?>"  class="validate[custom[number]] calCost cons"/></td>
     <td align="center" bgcolor="#FFFFFF"><input type="text" name="txtCostPerKg" id="txtCostPerKg" style="text-align:right;width:100px;" value="<?php echo $row['dblPricePerKg'];?>"  class="validate[custom[number]] calCost price"/></td>
        <td align="center" bgcolor="#FFFFFF"><input type="text" name="txtCostperInch" id="txtCostperInch" style="text-align:right;width:100px;" value="<?php echo $row['dblCostPerInch'];?>"  class="validate[custom[number]] cost" disabled/></td>
        <td align="center" bgcolor="#FFFFFF"><img  style="display:" border="0" src="images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/>&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;<?php if($row['inkTypeMasterStatus']==1){ ?>Approved<?php } else if($row['inkTypeMasterStatus']==0){?>Rejected<?php } else {?><?php ?><a id="butApprove" class="button green meedium approve" title="Click here to save order" name="butApprove" >Approve</a><?php } ?>&nbsp;</td>
      </tr>
      <?php 
        } 
       ?>
    </table>                </td>
              </tr>
              
              </table></td>
            </tr>
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><img style="display:" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/>
                  <?php
                if($form_permision['add']||$form_permision['edit'])
				{
				?>
                  <!--<img  style="display:" border="0" src="images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/>-->
                  <?php
				}
                if($form_permision['delete'])
				{
				?>
                  <!--<img style="display:" border="0" src="images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/>-->
                  <?Php 
				}
				?>
                  <a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
              </tr>
            </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
</form>
</body>


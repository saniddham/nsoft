var basePath = "presentation/costing/masterData/inkTypeCost/addNew/";
	
$(document).ready(function() {
	
		functionList();
		$("#frmInkTypeCosting").validationEngine();
		$('#frmInkTypeCosting #txtCode').focus();
		
		$('#frmInkTypeCosting .calCost').die('keyup').live('keyup',function(){
			calculateCost(this);
		});

		$('#frmInkTypeCosting .approve').die('click').live('click',function(){
			approveInkType(this);
		});
 		
  ///save button click event
  $('#frmInkTypeCosting #butSave').click(function(){
	  var object =this;
		showWaiting();

	var requestType = '';
	/*if ($('#frmInkTypeCosting').validationEngine('validate'))   
    { */
		var data = "requestType=save";
			var arr="[";
			
 					
					var inkType	=$(object).parent().parent().find(".loadId").attr('id');
					var cost	=$(object).parent().parent().find(".cost").val();
					var cons	=$(object).parent().parent().find(".cons").val();
					var price	=$(object).parent().parent().find(".price").val();
					
					//var currency = 	document.getElementById('tblMainGrid').rows[i].cells[2].childNodes[0].value;
					
					//if(cost>0){
						if(cons=='')
							cons	=0;
						if(price=='')
							price	=0;
							
					 arr += "{";

						arr += '"inkType":"'+		inkType +'",' ;
						arr += '"cost":"'+	cost +'",' ;
						arr += '"cons":"'+	cons +'",' ;
						arr += '"price":"'+		price +'"' ;
						arr +=  '},';
 						/*if(currency==''){
							alert("Select the currency");
							hideWaiting();
							return false;
						}*/
					//}
 			

			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basePath+"inkTypeCosting-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			type: 'post',
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$(object).validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						hideWaiting();
						var t=setTimeout("alertx()",1000);
						return;
					}
				},
			error:function(xhr,status){
					
					$(object).validationEngine('showPrompt', errormsg(xhr.status),'fail');
					hideWaiting();
					var t=setTimeout("alertx()",3000);
				}		
			});
	//}
   });
   
	hideWaiting();
   /////////////////////////////////////////////////////
   //// load location details //////////////////////////
   /////////////////////////////////////////////////////
	
});

 

function functionList()
{
	if(colorCode!='')
	{
		$('#frmInkTypeCosting #txtCode').val('CC'+colorCode);
	}
}

function calculateCost(obj){
	
	var cons	=$(obj).parent().parent().find(".cons").val();
	var price	=$(obj).parent().parent().find(".price").val();
	$(obj).parent().parent().find(".cost").val(cons*price);
}


function alertx()
{
	$('#frmInkTypeCosting #butSave').validationEngine('hide')	;
}

function approveInkType(e){

	var val = $.prompt('Are you sure you want to approve "'+$(e).parent().parent().find('.loadId').html()+'" ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
				var url = basePath+"inkTypeCosting-db-set.php";
				var httpobj = $.ajax({
					url:url,
					dataType:'json',
					type: 'post',
					data:'requestType=approve&inkType='+$(e).parent().parent().find('.loadId').attr('id'),
					async:false,
					success:function(json){							
						$(e).validationEngine('showPrompt', json.msg,json.type);							
						if(json.type=='pass')
						{
							var t=setTimeout("alertx('')",2000);
							return;
						}	
						var t=setTimeout("alertx('')",4000);
					},
					error:function(xhr,status){							
						$(e).validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertx('')",4000);
					} 
				});
	
		}
		}
	});
}


<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	
	$requestType 	= $_REQUEST['requestType'];
	$arr 		= json_decode($_REQUEST['arr'], true);
				
	/////////// location insert part /////////////////////
	if($requestType=='save')
	{
		$k=0;
		$m=0;
		$msg='';
			foreach($arr as $arrVal)
			{
				$k++;
				$inkType 	= $arrVal['inkType'];
				$cost 		= $arrVal['cost'];
				$cons	= $arrVal['cons'];
				$price	= $arrVal['price'];
		
			 	$sql = "SELECT
						mst_costinginktypecosting.intInkTypeId
						FROM mst_costinginktypecosting
						WHERE
						mst_costinginktypecosting.intInkTypeId =  '$inkType'";
				$result = $db->RunQuery($sql);
				$row = mysqli_fetch_array($result);
				if($row['intInkTypeId']==''){
				$sql = "INSERT INTO `mst_costinginktypecosting` (`intInkTypeId`/*,`intCurrencyId`*/,`dblConsPerSqInch`,`dblPricePerKg`,`dblCostPerInch`,dtmDate,intUser) 
						VALUES ('$inkType'/*,'$currency'*/,'$cons','$price','$cost',now(),'$userId')";
				$result = $db->RunQuery($sql);
				}
				else{
				$sql = "UPDATE `mst_costinginktypecosting` SET 	/*intCurrencyId ='$currency',*/
													dblConsPerSqInch	='$cons',
													dblPricePerKg		='$price',
													dblCostPerInch		='$cost',
													dtmDate				=now(),
													intUser				='$userId' 
						WHERE (`intInkTypeId`='$inkType')";
				$result = $db->RunQuery($sql);
				}
				$m+=$result;
				if(!$result){
					$msg.=$sql;
				}
					
		
			}
		
		if($m==$k){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $msg;
		}
	}
	else if($requestType=='approve')
	{
		$k=0;
		$m=0;
		$msg='';
		$k++;
		$inkType 	= $_REQUEST['inkType'];
		$cost 		= $_REQUEST['cost'];
		$cons		= $_REQUEST['cons'];
		$price		= $_REQUEST['price'];

		$sql 		= "UPDATE `mst_inktypes` SET 
								intStatus	 ='1' 
						WHERE (`intId`='$inkType')";
		$result 	= $db->RunQuery($sql);
		if(!$result){
			$msg.=$sql;
		}
		else 
			$m+=$result;

		if($m==$k){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Approved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $msg;
		}
	}
	echo json_encode($response);
?>
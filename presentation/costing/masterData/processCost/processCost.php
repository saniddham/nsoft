<?php
$process 	= $_REQUEST['cboSearch'];
?>
<head>
<title>Process Cost</title>
	<script type="text/javascript" src="presentation/costing/masterData/processCost/processCost-js.js"></script>
</head>
<body>
<form id="frmProcessCosting" name="frmProcessCosting" method="post" autocomplete="off"  >
<div align="center">
  <div class="trans_layoutD">
	<div class="trans_text">Process Cost</div>
    <table width="100%">
<tr>
<td width="100%"><table width="100%">
  <tr>
    <td width="71" class="normalfnt">&nbsp;</td>
    <td width="51" class="normalfnt">Search<span class="compulsoryRed"></span></td>
    <td colspan="2" bgcolor="#FFFFFF" class=""><select name="cboSearch" id="cboSearch" style="width:250px" onChange="submit();">
                  <option value=""></option>
              <?php
				$sql = "SELECT
						mst_costingprocesses.intId,
						mst_costingprocesses.strProcess
						FROM mst_costingprocesses
						WHERE
						mst_costingprocesses.intStatus =  '1'
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($process==$row['intId']){
						echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strProcess']."</option>";
						$strPrcs=$row['strProcess'];
					}
					else
						echo "<option value=\"".$row['intId']."\">".$row['strProcess']."</option>";
				}
				  ?>
                </select></td>
    <td width="26" bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td width="148" bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td width="13" bgcolor="#FFFFFF" class="">&nbsp;</td>
  </tr>
  <tr>
    <td class="normalfnt">&nbsp;</td>
    <td width="51" class="normalfnt">Process</td>
    <td width="250" bgcolor="#FFFFFF" class="">
    <input name="txtProcess" type="text" id="txtProcess" style="width:250px" value="<?php echo $strPrcs; ?>"/></td>
    <td width="1" bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td bgcolor="#FFFFFF" class=""><img  src="images/Tadd.jpg" alt="add" name="butAdd" width="92" height="24" border="0"  class="mouseover" id="butAdd" tabindex="27"/></td>
    <td bgcolor="#FFFFFF" class="">&nbsp;</td>
  </tr>
</table></td>
</tr>
<tr>
  <td><div style="overflow:scroll;width:100%;height:350px;" id="divGrid">
          <table width="100%" class="bordered" id="tblMainGrid" >
      <tr class="">
        <th width="7%" height="27" class="normalfntMid">delete&nbsp;</th>
        <th width="74%"><span class="normalfnt"><strong>Process</strong></span></th>
        <th width="19%"><strong>Cost</strong></th>
        <th width="19%">Printing Process</th>
      </tr>
      
      <?php
	  $sql = "SELECT
				mst_costingprocesses.intId,
				mst_costingprocesses.strProcess,
				mst_costingprocesses.dblCost,
				mst_costingprocesses.printingMethod
				FROM mst_costingprocesses
				WHERE
				mst_costingprocesses.intStatus ='1'
				";
		if($process){
			$sql .= "AND
					mst_costingprocesses.intId =  '$process'
					";	
		}
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$prcsId=$row['intId'];
			$prcs=$row['strProcess'];
			$cost=$row['dblCost'];
			$printmethod = $row['printingMethod'];
	  ?>
      	<tr class="normalfnt" id="row_process">
        <td id="<?php echo $prcsId;?>" align="center" bgcolor="#FFFFFF" class="processID"><img class="delImgProcess" src="images/del.png" width="15" height="15"></td>
        <td align="center" bgcolor="#FFFFFF" class="process" id="<?php echo $prcsId;?>"><?php echo $prcs;?></td>
        <td align="center" bgcolor="#FFFFFF"><input name="txtDescount" type="text" id="txtDescount" class="validate[required,custom[number]] cost" style="width:80px; text-align:right"  value="<?php echo $cost;?>"/></td> 
        
        <td align="center" bgcolor="#FFFFFF"><input name="printing_pro" id="printing_pro" type="checkbox" value="" <?php if($printmethod == 1) echo 'checked' ?> class="printing_pro"/></td>
      </tr>
      
      <?php 
        } 
       ?>
       
    </table>
  </div></td>
</tr>
<tr style="display:none"><td><table id="tr_template">
<tr class="normalfnt">
        <td id="" align="center" bgcolor="#FFFFFF"><input name="chkProcess" type="checkbox" value=""  class="chkBox"/></td>
        <td align="center" bgcolor="#FFFFFF" class="process" id=""></td>
        <td align="center" bgcolor="#FFFFFF"><input name="txtDescount" type="text" id="txtDescount" class="validate[required,custom[number]] cost" style="width:80px; text-align:right"  value=""/></td>
        <td align="center" bgcolor="#FFFFFF"><input name="printing_pro" id="printing_pro" type="checkbox" value=""  class="printing_pro"/></td>
      </tr>
</table></td></tr>
<tr>
  <td width="100%" align="center" bgcolor=""><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
    <tr>
      <td width="100%" align="center" bgcolor=""><img style="display:" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/>
        <?php
                if($form_permision['add']||$form_permision['edit'])
				{
				?>
        <img  style="display:" border="0" src="images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/>
        <?php
				}
                if($form_permision['delete'])
				{
				?>
        <img style="display:" border="0" src="images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/>
        <?Php 
				}
				?>
        <a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
    </tr>
  </table></td>
</tr>
</table>
</div>
</div>
</form>   
</body>
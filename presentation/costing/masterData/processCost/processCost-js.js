var basePath = "presentation/costing/masterData/processCost/";

$(document).ready(function() {
	$("#frmProcessCosting").validationEngine();
	$('#frmProcessCosting #cboSearch').focus();
 
	$('#butAdd').click(function(){
		if(document.getElementById('txtProcess').value.trim()==''){
			return false;
		}
		
		var tr1	=	$('#tr_template').html()
	
		$('#tblMainGrid').html($('#tblMainGrid').html()+tr1);
		
		var rowCount = document.getElementById('tblMainGrid').rows.length;
		document.getElementById('tblMainGrid').rows[rowCount-1].className ='normalfnt';
		document.getElementById('tblMainGrid').rows[rowCount-1].cells[1].innerHTML =document.getElementById('txtProcess').value;
		document.getElementById('tblMainGrid').rows[rowCount-1].cells[0].id ='';
		document.getElementById('tblMainGrid').rows[rowCount-1].cells[1].id ='';
		document.getElementById('tblMainGrid').rows[rowCount-1].cells[2].childNodes[0].value ='';
		document.getElementById('txtProcess').value='';
	});

	$('#butDelete').click(deleteDetails);

  	$('#frmProcessCosting #butSave').click(function(){
		var requestType = '';
		if ($('#frmProcessCosting').validationEngine('validate'))   
    	{ 
			var data = "requestType=save";
			var arr="[";
			
			var rowCount = document.getElementById('tblMainGrid').rows.length;
			if(rowCount==1){
				alert("No records to save");
				return false;	
			}
			var rw=0;
			for(var i=1;i<rowCount;i++)
			{
					 
					var processId = 	document.getElementById('tblMainGrid').rows[i].cells[1].id;
					var process = 	document.getElementById('tblMainGrid').rows[i].cells[1].innerHTML;

					var cost = 	document.getElementById('tblMainGrid').rows[i].cells[2].childNodes[0].value;
					if(document.getElementById('tblMainGrid').rows[i].cells[3].childNodes[0].checked) {
						var printing = 1;
					} else {
						var printing = 0;
					}
					if(cost !=''){
					 arr += "{";

						arr += '"processId":"'+		processId +'",' ;
						arr += '"process":"'+	process +'",' ;
						arr += '"printing":"'+	printing +'",' ;
						arr += '"cost":"'+		cost +'"' ;
						arr +=  '},';
						rw++;
					}
			}
			
			if(rw==0){
				alert("Please select cost to save");
				return false;
			}
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basePath+"processCost-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",
			type: 'post',  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmProcessCosting #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						reloadCombo();
						var t=setTimeout("alertx()",1000);
						return;
					}
				},
			error:function(xhr,status){
					$('#frmProcessCosting #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
	}
   });
   
   
   /////////////////////////////////////////////////////
   //// load location details //////////////////////////
   /////////////////////////////////////////////////////
	
});

$('#frmProcessCosting #butNew').click(function(){
	document.getElementById("cboSearch").value = '';
	document.getElementById("txtProcess").value = '';
});

$('.delImgProcess').die('click').live('click',function(){
 			deleteDetails(this);
		});

//--------------------------------------
function deleteProcess(e){
	alert('delete');	
}
//--------------------------------------------------------------------
function alertx()
{
	$('#frmProcessCosting #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmProcessCosting #butDelete').validationEngine('hide')	;
		document.location.href=document.location.href;

}
//----------------------------------------------------------------------
function reloadCombo(){
	var url 		= basePath+"processCost-db-get.php?requestType=loadCombo";
	var httpobj 	= $.ajax({url:url,async:false})
	document.getElementById('cboSearch').innerHTML=httpobj.responseText;
}
//------------------------------------------------------------------------
function deleteDetails(e)
{			//alert($(e).parent().html());
			var val = $.prompt('Are you sure you want to delete" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
									var data = "requestType=delete";
										var arr="[";
											var processId = $(e).parent().attr('id');
											 arr += "{";
												arr += '"processId":"'+		processId +'"' ;
												arr +=  '},';
										
										arr = arr.substr(0,arr.length-1);
										arr += " ]";
										
										data+="&arr="	+	arr;
									///////////////////////////// save main infomations /////////////////////////////////////////
									var url = basePath+"processCost-db-set.php";
									var obj = $.ajax({
										url:url,
										
										dataType: "json",
										type: 'post',  
										data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
										//data:'{"requestType":"addsampleInfomations"}',
										async:false,
							
										success:function(json){
											
											$('#frmProcessCosting #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
											if(json.type=='pass')
											{
												$(e).parent().parent().remove();
												//$('#frmProcessCosting').get(0).reset();
												reloadCombo();
												//var t=setTimeout("alertDelete()",1000);return;
											}else if(json.type=='fail'){
												alert(json.msg);
											}
											//var t=setTimeout("alertDelete()",3000);
										}	 
									});
								}
								}
});
			
}
//------------------------------------------------------------
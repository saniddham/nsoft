<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$samplePriceMenuId			= 455;
$companyId 					= $_SESSION['CompanyID'];
$locationId 				= $_SESSION['CompanyID'];
$userId 					= $_SESSION['userId'];

$no 						= $_REQUEST['cboSampleNo'];
$year 						= $_REQUEST['cboYear'];
$revNo						= $_REQUEST['cboRevisionNo'];

if($no==''){ 
	$no 					= $_REQUEST['no'];
	$year 					= $_REQUEST['year'];
	$revNo 					= $_REQUEST['revNo'];
}


$sampleNo 					= $no;
$sampleYear 				= $year;
if($sampleYear=='') 
	$sampleYear				=date('Y'); 
if($revNo==0)
	$revNo_p				=0;
else
	$revNo_p 				=$revNo-1;

$header_arr					=get_sampleInfoHeaderResults($sampleNo,$sampleYear,$revNo);
$header_arr_p				=get_sampleInfoHeaderResults($sampleNo,$sampleYear,$revNo_p);
$header_diff_arr			=array_diff_assoc($header_arr,$header_arr_p);
$header_diff_color_arr		= get_diff_color($header_arr,$header_diff_arr); 
   
$arrCombo					=get_sampleInfoComboArr($sampleNo,$sampleYear,$revNo);
$arrType					=get_sampleInfoTypeArr($sampleNo,$sampleYear,$revNo);
$arrType_p					=get_sampleInfoTypeArr($sampleNo,$sampleYear,$revNo_p);
$maxRevNo 					= getMaxRevisionNo($sampleNo,$sampleYear);

$firstUser					=$header_arr['firstUser'];
$firstDate					=$header_arr['firstDate'];
$secondUser					=$header_arr['secondUser'];
$secondDate					=$header_arr['secondDate'];
?>
<title>Sample Infomations Report</title>
<!--<script type="text/javascript" src="presentation/costing/sample/samplePrices/addNew/sampleReport-js.js"></script>
-->
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:274px;
	top:1000px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
.printName {font-family: "Arial Black", Gadget, sans-serif;
	font-size: 16px;
	font-style: normal;
	font-weight: bold;
	color: #009;
	text-align:center
}
.noPrint {    display:none;
}
.statusWithBorder {
	font-family: "Courier New", Courier, monospace;
	border: thin solid #666;
	font-size: 18px;
}
#frmSampleReport div table tr td table tr td {
}
</style>

<form id="frmSampleReport" name="frmSampleReport" method="post" >

<table width="88%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#D3F5DF" class="tableBorder_allRound">
  <tr>
    <td width="3%">&nbsp;</td>
    <td width="20%">&nbsp;</td>
    <td width="15%">&nbsp;</td>
    <td width="9%">&nbsp;</td>
    <td width="18%">&nbsp;</td>
    <td width="20%">&nbsp;</td>
    <td width="15%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td class="normalfnt">Sample Year</td>
    <td class="normalfnt">Graphic No</td>
    <td class="normalfnt">Sample No</td>
    <td class="normalfnt">Revision No</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="tableBorder_allRound mouseover" align="center" ><a  target="_blank" style="text-decoration:none;width:100%" href="?q=<?php echo $samplePriceMenuId?>&cboSampleNo=<?Php echo $sampleNo; ?>&cboYear=<?php echo $sampleYear; ?>&cboRevisionNo=<?PHP echo $revNo; ?>"><span class="printName" >ADD PRICE</span></a></td>
    <td>&nbsp;</td>
    <td><select name="cboYear" id="cboYear" style="width:65px">
      <?php
 					$sql =getSampleYearSql();
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						if($row['intSampleYear']==$sampleYear)
						echo "<option value=\"".$row['intSampleYear']."\" selected=\"selected\">".$row['intSampleYear']."</option>";	
						else
						echo "<option value=\"".$row['intSampleYear']."\">".$row['intSampleYear']."</option>";	
						$i++;
					}
				  ?>
    </select></td>
    <td><select name="cboGraphicNo" id="cboGraphicNo" style="width:150px">
        <option value=""></option>
        <?php
					if($sampleYear!=''){
						$d=$sampleYear;
					}
					else{
						$d= date('Y');
					}
 					$sql =getGraphicNoSql($sampleYear);
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$strGraphicRefNo = $row['strGraphicRefNo'];
						if($header_arr['graficRef']==$strGraphicRefNo)
							echo "<option selected=\"selected\" value=\"$strStyle\">$strGraphicRefNo</option>";
						else
							echo "<option value=\"$strGraphicRefNo\">$strGraphicRefNo</option>";
					}
				?>
    </select></td>
    <td><select name="cboSampleNo" id="cboSampleNo" style="width:150px">
              <option value=""></option>
                  <?php
 					$graficRef = $header_arr['graficRef'];
					$sql=getSampleNoSql($sampleYear,$graficRef,$locationId);
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$no = $row['intSampleNo'];
						if($no==$sampleNo)
							echo "<option selected=\"selected\" value=\"$no\">$no</option>";
						else
							echo "<option value=\"$no\">$no</option>";
					}
				?>
    </select></td>
    <td><select name="cboRevisionNo" id="cboRevisionNo" style="width:50px">
    <option value=""></option>
                  <?php
				$sql =getRevisionNoSql($sampleNo,$sampleYear);
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($revNo==$row['intRevisionNo'])
						echo "<option selected=\"selected\" value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
					else
						echo "<option value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
				}
				  ?>
    </select></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

<div align="center">
<div style="background-color:#FFF" ></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">

  <tr>
     <td colspan="9" ><?php include "reportHeader.php"; ?></td>
  </tr>
  <tr>
    <td colspan="9" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="9" align="center"><span class="reportHeader" style="background-color:#FFF"><strong>SAMPLE INFORMATION SHEET</strong></span></td>
  </tr>
      <?php
	  $x 	= ($header_arr['marketingApproveLevelStart']+1)-$header_arr['marketingStatus'];
	  $sql 	= "	SELECT
					menupermision.int{$x}Approval as approval
				FROM menupermision Inner Join menus ON menus.intId = menupermision.intMenuId
				WHERE
					menus.strCode =  'P0022' AND
					menupermision.intUserId =  '$userId'
			";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	$userPermision = $row['approval'];
	
	$viewApproveButton = false;
	if($userPermision && $header_arr['marketingStatus']>1)
	{
		$viewApproveButton = true;	
	}
	?>
      
      <tr>
        <td colspan="9" class="APPROVE"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="normalfnt">
          <tr>
            <td width="12%" bgcolor="<?php echo ($viewApproveButton?'#CC9900':''); ?>" class="normalfnt">1st Stage (Marketing)</td>
            <?php 

			if($viewApproveButton)
			{
				//$stage = 1;
			?>
            <td width="15%" align="center" bgcolor="#CC9900"  class="statusWithBorder" ><b>Pending</b></td><?php
			
			}
			else
			{
				
				if($header_arr['marketingStatus']==1)
					$text = "Approved";
				else 
					$text = "Pending";
			?>
            <td width="12%" align="center" bgcolor="#D9F4FD" class="statusWithBorder" ><?php echo $text; ?></td>
            <?php
					
				
			}
			?>
            <td width="6%">&nbsp;</td>
            <td width="12%">2nd Stage (Technical)</td>
            <?php
				///////////////2nd stage ////////////////
				$text = '';
				if($header_arr['mainStatus']==1)
				{
			?>
          		<td width="17%" align="center" bgcolor="#D9F4FD"  class="statusWithBorder" >Approved</td>
            <?php
				}
				else if($header_arr['mainStatus']>1)
				{
				
			?>
            	<td width="15%" align="center" bgcolor="#CC9900"  class="statusWithBorder" ><b>Pending</b></td>
            <?php
				}
				else
					echo "<td width=\"20%\" align=\"center\"    >&nbsp;</td>";
			?>
          </tr>
        </table></td>
        
      </tr>
      <tr>
        <td colspan="9" class="compulsoryRed"><input style="width:5px;visibility:hidden"  type="text" name="txtApproveStatus" id="txtApproveStatus" /></td>
       
      </tr>
  <tr>
    <td width="3%">&nbsp;</td>
    <td width="14%"><span class="normalfnt"><strong>Sample No</strong></span></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="17%"><span class="printName"><?php echo $sampleYear ?>/<?php echo $sampleNo ?></span></td>
    <td width="15%"><span class="normalfnt"><strong>Revision No</strong></span></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="19%" align="left"><span class="printName"><?php echo $header_arr['sampleRevision'] ?></span></td>
    <td width="11%"><span class="normalfnt"><strong>Date</strong></span></td>
    <td width="9%" ><span class="normalfnt" style="background:<?php echo $header_diff_color_arr['sampleDate'];  ;?>">: <?php echo $header_arr['sampleDate'] ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td colspan="4" style="color:#F00;font-size:8;font-family:'Times New Roman', Times, serif"><?Php 
	$reportLink  		= "?q=988&no=$sampleNo&year=$sampleYear&revNo=$maxRevNo";
	if($maxRevNo>$revNo)
		echo "This report is not valid sample, revision no <a href=\"$reportLink\">\"".$maxRevNo."\"</a> is the original one.";
		//echo "This report is not valid sample, revision no <a href=\"sampleReport.php?no=$sampleNo&year=$sampleYear&revNo=$maxRevNo\">\"".$maxRevNo."\"</a> is the original one.";
	?></td>
    <td width="11%"><span class="normalfnt"><strong>Delivery Date</strong></span></td>
    <td><span class="normalfnt" style="background:<?php echo $header_diff_color_arr['deliveryDate'];?>">: <?php echo $header_arr['deliveryDate'] ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><span class="normalfnt"><strong>Graphic Ref No#</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt" style="background:<?php echo $header_diff_color_arr['graficRef'];?>"><?php echo $header_arr['graficRef'] ?></span></td>
    <td><span class="normalfnt"><strong>Customer</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td colspan="3"><span class="normalfnt" style="background:<?php echo $header_diff_color_arr['customer'];?>" ><?php echo $header_arr['customer'] ?></span></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td><span class="normalfnt"><strong>Style No</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"  style="background:<?php echo $header_diff_color_arr['style'];?>"><?php echo $header_arr['style'] ?></span></td>
    <td><span class="normalfnt"><strong>Brand</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt" style="background:<?php echo $header_diff_color_arr['brand'];?>"><?php echo $header_arr['brand'] ?></span></td>
    <td width="11%">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td width="11%">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="9">
      <?php 
			$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strComboName
					FROM trn_sampleinfomations_details
					WHERE
					trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_details.intRevNo =  '$revNo'
					";
			$result = $db->RunQuery($sql);
			$rowsCount = mysqli_num_rows($result);
			
			$sql = "SELECT DISTINCT
						strPrintName
					FROM trn_sampleinfomations_printsize
					WHERE
						intSampleNo 	=  '$sampleNo' AND
						intSampleYear 	=  '$sampleYear' AND
						intRevisionNo 	=  '$revNo'
			";
			$result = $db->RunQuery($sql);
			$cellCount = mysqli_num_rows($result);
			
			//$cellCount = 3;
	?>
      </td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td width="37" class="normalfnt"><table id="tblMain" width="74%" border="0" align="left"  class="grid"  >
          <tr class="">
            <td height="152" bgcolor="#FFFFFF" class="normalfnt"  >&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF"><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:264px;height:148px;overflow:hidden" >
              <?php
			if($sampleNo!='')
			{
				echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revNo-1.png\" />";	
			}
			 ?>
              </div></td>
            <?php
				///////////  img cell loop 
				for($i=1;$i<$cellCount;$i++)
				{
					$l=$i+1;
			?>
            <td align="center" bgcolor="#FFFFFF"><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:264px;height:148px;overflow:hidden" >
              <?php

		echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revNo-$l.png\" />";	
			
			 ?>
              </div></td>
            <?php	
				}
			?>
             </tr>
          <tr class="">
            <td height="18" bgcolor="#FFFFFF" class="normalfnt" >&nbsp;</td>
            <?php
				$arrPart					=get_sampleInfoPrintArr($sampleNo,$sampleYear,'print 1',$revNo);
				$arrPart_p					=get_sampleInfoPrintArr($sampleNo,$sampleYear,'print 1',$revNo_p);
				$print_diff_p_arr			=array_diff_assoc($arrPart,$arrPart_p);
				$header_diff_p_color_arr	= get_diff_color($arrPart,$print_diff_p_arr); 
				
 			?>
            <td align="center" style="width:300px" class="printName" bgcolor="#FFFFFF" ><span class="printNameSpan" style="background:<?php echo $header_diff_p_color_arr['printName']; ?>">Print 1</span><span  class="normalfntMid">
              <select disabled="disabled"    class="part validate[required]" name="cboPart1" id="cboPart1" style="width:180px; background:<?php echo $header_diff_p_color_arr['part']; ?>">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_part where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrPart['part'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
                </select>
              <input disabled="disabled"   name="textfield" type="text" class="sizeW validate[required,custom[number],max[1000],min[0]]" id="sizeW1" style="width:40px; background:<?php echo $header_diff_p_color_arr['width']; ?>" value="<?php echo $arrPart['width']; ?>" />
              <span class="normalfntGrey">W
                <input disabled="disabled"  name="textfield2" type="text" class="sizeH validate[required,custom[number],max[1000],min[0]]" id="sizeH1" style="width:40px; background:<?php echo $header_diff_p_color_arr['height']; ?>" value="<?php echo $arrPart['height']; ?>" />
                H(inch)</span></span></td>
            <?php
			  	///////// print name cell loop
				for($i=1;$i<$cellCount;$i++)
				{
					$print						='print '.($i+1);
					$arrPart					=get_sampleInfoPrintArr($sampleNo,$sampleYear,$print,$revNo);
					$arrPart_p					=get_sampleInfoPrintArr($sampleNo,$sampleYear,$print,$revNo_p);
					$print_diff_p_arr			=array_diff_assoc($arrPart,$arrPart_p);
					$header_diff_p_color_arr	=get_diff_color($arrPart,$print_diff_p_arr); 
					
					if(!$arrPart_p)
					$tdColor='#A6FFA6';//new
					else if(!$arrPart){
					$tdColor='#FF8484';//deleted
					}
					else
					$tdColor='#FFFFFF';
			  ?>
            <td align="center" bgcolor="<?php echo $tdColor; ?>" class="printName" ><span class="printNameSpan" style="background:<?php echo $header_diff_p_color_arr['printName']; ?>">Print <?php echo ($i+1); ?></span><span  class="normalfntMid">
              <select disabled="disabled"  class="part validate[required]" name="cboPart2" id="cboPart2" style="width:180px; background:<?php echo $header_diff_p_color_arr['part']; ?>">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_part where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					$e=1;
					while($row=mysqli_fetch_array($result))
					{
						
						if($row['intId']==$arrPart['part'])
							echo "<option selected value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
                </select>
              <input name="textfield" type="text" disabled="disabled" class="sizeW validate[required,custom[number],max[1000],min[0]]" id="sizeW2" style="width:40px; background:<?php echo $header_diff_p_color_arr['width']; ?>" value="<?php echo $arrPart['width']; ?>" />
              <span class="normalfntGrey">W
                <input name="textfield2" type="text" disabled="disabled" class="sizeH validate[required,custom[number],max[1000],min[0]]" id="sizeH2" style="width:40px; background:<?php echo $header_diff_p_color_arr['width']; ?>" value="<?php echo $arrPart['height']; ?>" />
                H(inch)</span></span></td>
            <?php
				}
			  ?>
             </tr>
             <?php
			$sql 	= "SELECT
									trn_sampleinfomations_prices.dblPrice,
									costing_sample_header.QTY_PRICES
									FROM
									trn_sampleinfomations_prices left join  costing_sample_header on 
									costing_sample_header.SAMPLE_NO = trn_sampleinfomations_prices.intSampleNo AND 
									costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations_prices.intSampleYear AND 
									costing_sample_header.REVISION = trn_sampleinfomations_prices.intRevisionNo AND 
									costing_sample_header.COMBO = trn_sampleinfomations_prices.strCombo AND 
									costing_sample_header.PRINT = trn_sampleinfomations_prices.strPrintName
									WHERE
									trn_sampleinfomations_prices.intSampleNo = '$sampleNo' AND
									trn_sampleinfomations_prices.intSampleYear = '$sampleYear' AND
									trn_sampleinfomations_prices.intRevisionNo = '$revNo' AND
									trn_sampleinfomations_prices.strCombo ='".$arrCombo[0]['combo']."' AND
									trn_sampleinfomations_prices.strPrintName = '"."print "."1"."' 
									";

						$result = 	$db->RunQuery($sql);
						$row	=	mysqli_fetch_array($result);
			
			?>
          <tr class="" >
          <?php 
  				$arr_combo_p				=get_combo_arr($sampleNo,$sampleYear,$revNo_p);
				$flag						=in_array($arrCombo[0]['combo'],$arr_combo_p);
 				//print_r($arr_combo_p);
				if($flag==1) {
					$tdColor='#F0F0F0'; //existing
				}
				else
					$tdColor='#A6FFA6';//new
		  ?>
            <td height="10" bgcolor="#CCCCCC" class="gridHeader" ><input disabled="disabled"   align="left" class="validate[required,maxSize[20]]" value="<?php echo $arrCombo[0]['combo']; ?>" style="width:80px; background:<?php echo $tdColor; ?>" type="text" name="txtCombo1" id="txtCombo1" /></td>
                       
            <td align="center" bgcolor="#FFFFFF" class="normalfntRight" ><table width="100%" cellspacing="0" cellpadding="0">
            <tr bgcolor="#CCCCCC"  class="gridHeader"><td width="10%" align="left" nowrap  class="normalfnt" style="display:none">No of ups</td>
            <td width="6%" style="display:none"><input align="left" value="<?php echo getUps($arrCombo[0]['combo'],'print 1');?>" style="width:40px; text-align:right" type="text" name="ups" id="ups"  class="cls_ups"/></td>
            <td width="1%">&nbsp;</td>
            <td width="27%"  class="normalfnt"><a id="butProcess" class="button green medium butProcess" style="" name="butProcess"> Routing Process</a></td>
            <td width="1%"><div style="display:none" class="combo"><?php echo $arrCombo[0]['combo']; ?></div><div style="display:none" class="print">print 1</div>&nbsp;</td>
                <?php
                if ($row['QTY_PRICES'] == 1){
                    ?>
                    <td width="27%"  class="normalfnt"><a id="qtyPrices" class="button green medium" style="" name="qtyPrices"> Qty Prices</a></td>
                    <?php

                } else {
                    ?>
                    <td align="center" bgcolor="#FFFFFF" class="printName" >&nbsp; Price : <?php echo $row['dblPrice']; ?> </td>                    <?php
                }
                ?>
           	</tr>
            </table></td>            
            
            
            <?php
				for($i=1;$i<$cellCount;$i++)
				{
					$sql 	= "SELECT
									trn_sampleinfomations_prices.dblPrice,
									costing_sample_header.QTY_PRICES
									FROM
									trn_sampleinfomations_prices left join  costing_sample_header on 
									costing_sample_header.SAMPLE_NO = trn_sampleinfomations_prices.intSampleNo AND 
									costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations_prices.intSampleYear AND 
									costing_sample_header.REVISION = trn_sampleinfomations_prices.intRevisionNo AND 
									costing_sample_header.COMBO = trn_sampleinfomations_prices.strCombo AND 
									costing_sample_header.PRINT = trn_sampleinfomations_prices.strPrintName
									WHERE
									trn_sampleinfomations_prices.intSampleNo = '$sampleNo' AND
									trn_sampleinfomations_prices.intSampleYear = '$sampleYear' AND
									trn_sampleinfomations_prices.intRevisionNo = '$revNo' AND
									trn_sampleinfomations_prices.strCombo ='".$arrCombo[0]['combo']."' AND
									trn_sampleinfomations_prices.strPrintName = '"."print ".($i+1)."' 
									";
                        
						$result = 	$db->RunQuery($sql);
						$row	=	mysqli_fetch_array($result);
			?>
            <td align="right" bgcolor="#FFFFFF" ><table width="100%" cellspacing="0" cellpadding="0">
            <tr bgcolor="#CCCCCC"  class="gridHeader"><td width="13%" align="left" class="normalfnt" style="display:none">No of ups</td>
            <td width="8%" style="display:none"><input align="left" value="<?php echo getUps($arrCombo[0]['combo'],'print '.($i+1));?>" style="width:40px; text-align:right" type="text" name="ups" id="ups"  class="cls_ups"/></td>
            <td width="4%">&nbsp;</td>
            <td width="16%"  class="normalfnt">&nbsp;</td>
            <td width="30%" align="left"><a id="butProcess" class="button green small butProcess" style="" name="butProcess"> Routing Process</a>&nbsp;</td>
            <td width="4%"><div style="display:none" class="combo"><?php echo $arrCombo[0]['combo']; ?></div><div style="display:none" class="print">print <?php echo $i+1 ?></div>&nbsp;</td>
                <?php
                if ($row['QTY_PRICES'] == 1){
                    ?>
                    <td width="27%"  class="normalfnt"><a id="qtyPrices" class="button green medium" style="" name="qtyPrices"> Qty Prices</a></td>
                    <?php

                } else {
                    ?>
                    <td align="center" bgcolor="#FFFFFF" class="printName" >&nbsp; Price : <?php echo $row['dblPrice']; ?> </td>                    <?php
                }
                ?>
            </table></td>
            <?php
				}
			?>
             <td bgcolor="#FFFFFF" >&nbsp;</td>
             </tr>
            <?php
			$arr_combo_details		=	get_combo_details_arr($sampleNo,$sampleYear,$arrCombo[0]['combo'],$revNo);
			$arr_combo_details_p	=	get_combo_details_arr($sampleNo,$sampleYear,$arrCombo[0]['combo'],$revNo_p);
			$combo_details_diff_arr	=array_diff_assoc($arr_combo_details,$arr_combo_details_p);
 			$combo_details_diff_color_arr		= get_diff_color2($arr_combo_details,$combo_details_diff_arr); 
 				
 					$tdColor='#FFFFFF'; //existing
 					$tdColorNew='#A6FFA6';//new
  			
			?>
          <tr class="dataRow">
            <td width="18%"  bgcolor="#FFFFFF" ><select disabled="disabled"  name="cboPrintMode2" class="printMode validate[required]" id="cboPrintMode2" style="width:100px; background:<?php if($arr_combo_details['printMode']==$arr_combo_details_p['printMode']){echo $tdColor;} else {echo $tdColorNew;} ?>">
              <option value=""></option>
              <?php
					$sql = "select intId,strName from mst_printmode where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[0]['printMode'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select>
              <select disabled="disabled"   class="washStanderd validate[required]" name="butWashingStanderd2" id="butWashingStanderd3" style="width:100px; background:<?php if($arr_combo_details['wash']==$arr_combo_details_p['wash']){echo $tdColor;} else {echo $tdColorNew;} ?>">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_washstanderd where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[0]['wash'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
					}
				?>
                </select>
              <select disabled="disabled"   class="colors validate[required]" name="cboGroundColor2" id="cboGroundColor2" style="width:100px; background:<?php if($arr_combo_details['groundColor']==$arr_combo_details_p['groundColor']){echo $tdColor;} else {echo $tdColorNew;} ?>">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_colors_ground where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[0]['groundColor'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
					}
				?>
                </select>
              <select disabled="disabled"   class="colors validate[required]" name="cboFabricType" id="cboFabricType" style="width:100px; background:<?php if($arr_combo_details['fabricType']==$arr_combo_details_p['fabricType']){echo $tdColor;} else {echo $tdColorNew;} ?>">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_fabrictype where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[0]['fabricType'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
                </select></td>
            <td width="67%"  bgcolor="#FFFFFF" style="padding-top:0px" ><table id="tblGrid2" class="tblGrid2" bgcolor="#006666" cellpadding="0" cellspacing="1" width="650" border="0">
              <tr class="normalfntMid">
                <td width="70" bgcolor="#CEE8FB">Color</td>

                <td width="100" bgcolor="#CEE8FB">Technique</td>
                <td width="100" bgcolor="#CEE8FB">Item</td>
                <td width="40" bgcolor="#CEE8FB">Qty</td>
                <td width="86" bgcolor="#CEE8FB">Size(inch)</td>
                <td bgcolor="#FFE9D2"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                  <tr class="normalfnt">
                    <td  width="40%" bgcolor="#DFFFDF">Ink Type</td>
                    <td width="13%" bgcolor="#DFFFDF">Shots</td>
                    <td width="35%" bgcolor="#DFFFDF">Item</td>
                    <td width="12%" bgcolor="#DFFFDF">Weight</td>
                    </tr>
                  </table></td>
                </tr>
              <?php
				foreach($arrType as $arrT)
				{
 					if($arrT['combo']==$arrCombo[0]['combo'] && $arrT['printName']=='print 1')
					{
 				?>
          <?php 
  				$arr_color					=get_color_arr($sampleNo,$sampleYear,$arrT['printName'],$arrT['combo'],$arrT['intColorId'],$revNo);
  				$arr_color_p				=get_color_arr($sampleNo,$sampleYear,$arrT['printName'],$arrT['combo'],$arrT['intColorId'],$revNo_p);
 			
				$flag=0;
				if($arr_color['intColorId']==$arr_color_p['intColorId']) {
					$tdColor='#FFFFFF'; //existing
				$flag=1;
				}
				else
					$tdColor='#A6FFA6';//new
				
				$color_diff_arr			=array_diff_assoc($arr_color,$arr_color_p);
				$color_diff_color_arr	=get_diff_color($arr_color,$color_diff_arr);
				//print_r($arr_color); 
 	   	?> 
              <tr>
                <td bgcolor="<?php if($flag==1){echo $color_diff_color_arr['intColorId'];} else {echo $tdColor;} ?>" class="normalfnt" id="<?php echo $arrT['intColorId'] ?>"><?php echo $arrT['colorName'] ?></td>
                <td bgcolor="<?php if($flag==1){echo $color_diff_color_arr['typeOfPrintId'];} else {echo $tdColor;} ?>" class="normalfnt" ><?php
			  	$sql 	= "SELECT mst_techniques.strName FROM mst_techniques 
						WHERE mst_techniques.intId = '".$arrT['typeOfPrintId']."'";
				$result = 	$db->RunQuery($sql);
				$row	=	mysqli_fetch_array($result);
				echo $row['strName'];
			  ?></td>
                <td bgcolor="#ffffffff" class="normalfnt">
                <table bgcolor="#ffffffff" width="100%" cellspacing="0" cellpadding="0" border="1">
				<?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];
			  
			  	$sql = "SELECT
							mst_item.intId,
							mst_item.strName
						FROM `mst_item`
						WHERE
							mst_item.intId =  '".$item_tech."'";
				$result = 	$db->RunQuery($sql);
				$row	=	mysqli_fetch_array($result);
				?><tr><td bgcolor="<?php if($flag==1){echo $color_diff_color_arr['marketing_itemId'];} else {echo $tdColor;} ?>" class="normalfnt" nowrap="nowrap"><?php echo $row['strName'];?> </td></tr><?php	
				}
			  ?></table></td>
              
                <td bgcolor="#ffffffff">
                <table bgcolor="#ffffffff" width="100%" border="1" cellspacing="0" cellpadding="0">
				<?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];?>
                <tr><td bgcolor="<?php if($flag==1){echo $color_diff_color_arr['dblQty'];} else {echo $tdColor;} ?>"><?PHP echo ($row_ti['dblQty']<=0?'':$row_ti['dblQty']); ?></td></tr>
                <?php
				}
				?>
                </table></td>
                <td bgcolor="#ffffffff" class="normalfnt"><table bgcolor="#ffffffff" width="100%" border="1" cellspacing="0" cellpadding="0">
				<?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];?>
<tr><td nowrap="nowrap"  bgcolor="<?php if($flag==1){echo $color_diff_color_arr['size_w'];} else {echo $tdColor;} ?>" class="normalfnt"><?PHP echo ($row_ti['size_w']<=0?'':$row_ti['size_w']); ?>
                  W
                  <?PHP echo ($row_ti['size_h']<=0?'':$row_ti['size_h']); ?>
                  H</td></tr>
<?php
				}
				?>
                </table></td>
                <td bgcolor="<?php echo $tdColor ?>"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                  <?php 
					$sql = "	SELECT
									trn_sampleinfomations_details_technical.intInkTypeId,
									trn_sampleinfomations_details_technical.intNoOfShots,
									trn_sampleinfomations_details_technical.intItem,
									trn_sampleinfomations_details_technical.dblColorWeight,
									mst_inktypes.strName AS techName,
									mst_item.strName AS itemName
								FROM
								trn_sampleinfomations_details_technical
									left Join mst_inktypes ON mst_inktypes.intId = trn_sampleinfomations_details_technical.intInkTypeId
									left Join mst_item ON mst_item.intId = trn_sampleinfomations_details_technical.intItem
								WHERE
									trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
									trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
									trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND
									trn_sampleinfomations_details_technical.strPrintName 	=  '".$arrT['printName']."' AND
									trn_sampleinfomations_details_technical.strComboName 	=  '".$arrT['combo']."' AND
									trn_sampleinfomations_details_technical.intColorId 		=  '".$arrT['intColorId']."'
								";
						$result_tech = $db->RunQuery($sql);
						while($row_tech=mysqli_fetch_array($result_tech))
						{
 							$arr_inkType			=get_inkType_arr($sampleNo,$sampleYear,$arrT['printName'],$arrT['combo'],$arrT['intColorId'],$row_tech['intInkTypeId'],$revNo);
							$arr_inkType_p			=get_inkType_arr($sampleNo,$sampleYear,$arrT['printName'],$arrT['combo'],$arrT['intColorId'],$row_tech['intInkTypeId'],$revNo_p);
						
							$flag=0;
							if($arr_inkType['inkType']==$arr_inkType_p['inkType']) {
								$tdColor='#FFFFFF'; //existing
							$flag=1;
							}
							else
								$tdColor='#A6FFA6';//new
								
							
							$inkType_diff_arr			=array_diff_assoc($arr_inkType,$arr_inkType_p);
							$inkType_diff_color_arr		=get_diff_color($arr_inkType,$inkType_diff_arr); 
							
					?>
                  <tr class="normalfnt">
                    <td width="40%"  bgcolor="<?php if($flag==1){echo $inkType_diff_color_arr['inkType'];} else {echo $tdColor;} ?>"><?php echo $row_tech['techName']; ?></td>
                    <td width="13%"  bgcolor="<?php if($flag==1){echo $inkType_diff_color_arr['shots'];} else {echo $tdColor;} ?>"><?php echo $row_tech['intNoOfShots']; ?></td>
                    <td width="35%"  bgcolor="<?php if($flag==1){echo $inkType_diff_color_arr['item'];} else {echo $tdColor;} ?>"><?php echo $row_tech['itemName']; ?></td>
                    <td width="12%"  bgcolor="<?php if($flag==1){echo $inkType_diff_color_arr['weight'];} else {echo $tdColor;} ?>"><?php echo $row_tech['dblColorWeight']; ?></td>
                    </tr>
                  <?php
						}
					?>
                  </table></td>
                </tr>
              <?php
					}
				}
				?>
              </table></td>
            <?php
					/////////// first row table cells //////////////
					for($i=1;$i<$cellCount;$i++)
					{
				?>
            <td width="67%"  bgcolor="#FFFFFF" style="padding-top:0px" ><table id="tblGrid2" class="tblGrid2" bgcolor="#006666" cellpadding="0" cellspacing="1" width="650" border="0">
              <tr class="normalfntMid">
                <td width="70" bgcolor="#CEE8FB">Color</td>
                <td width="100" bgcolor="#CEE8FB">Technique</td>
                <td width="100" bgcolor="#CEE8FB">Item</td>
                <td width="40" bgcolor="#CEE8FB">Qty</td>
                <td width="86" bgcolor="#CEE8FB">Size(inch)</td>
                <td bgcolor="#FFE9D2"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                  <tr class="normalfnt">
                    <td  width="40%" bgcolor="#DFFFDF">Ink Type</td>
                    <td width="13%" bgcolor="#DFFFDF">Shots</td>
                    <td width="35%" bgcolor="#DFFFDF">Item</td>
                    <td width="12%" bgcolor="#DFFFDF">Weight</td>
                    </tr>
                  </table></td>
                </tr>
              <?php
				  
				foreach($arrType as $arrT)
				{
					
					if($arrT['combo']==$arrCombo[0]['combo'] && $arrT['printName']=='print '.($i+1))
					{
						
				?>
          <?php
  				$arr_color					=get_color_arr($sampleNo,$sampleYear,$arrT['printName'],$arrT['combo'],$arrT['intColorId'],$revNo);
  				$arr_color_p				=get_color_arr($sampleNo,$sampleYear,$arrT['printName'],$arrT['combo'],$arrT['intColorId'],$revNo_p);
 				//$flag						=in_array($arrT['intColorId'],$arr_color_p);
				
				if($arr_color['intColorId']==$arr_color_p['intColorId']) {
					$flag	=1;
					$tdColor='#FFFFFF'; //existing
				}
				else{
					$tdColor='#A6FFA6';//new
					$flag=0;
				}
 					
				$color_diff_arr			=array_diff_assoc($arr_color,$arr_color_p);
				$color_diff_color_arr	=get_diff_color($arr_color,$color_diff_arr); 
  	
	   	?> 
               <tr id="<?php echo $arr_color['intColorId'].'=='.$arr_color_p['intColorId'];?>">
                <td bgcolor="<?php if($flag==1){echo $color_diff_color_arr['intColorId'];} else {echo $tdColor;} ?>" class="normalfnt" id="<?php echo $arrT['intColorId'] ?>"><?php echo $arrT['colorName'] ?></td>
                <td bgcolor="<?php if($flag==1){echo $color_diff_color_arr['typeOfPrintId'];} else {echo $tdColor;} ?>" class="normalfnt" ><?php
			  	$sql 	= "SELECT mst_techniques.strName FROM mst_techniques 
						WHERE mst_techniques.intId = '".$arrT['typeOfPrintId']."'";
				$result = 	$db->RunQuery($sql);
				$row	=	mysqli_fetch_array($result);
				echo $row['strName'];
			  ?></td>
                <td bgcolor="#ffffffff" class="normalfnt">
                <table bgcolor="#ffffffff" width="100%" cellspacing="0" cellpadding="0" border="1">
				<?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];
			  
			  	$sql = "SELECT
							mst_item.intId,
							mst_item.strName
						FROM `mst_item`
						WHERE
							mst_item.intId =  '".$item_tech."'";
				$result = 	$db->RunQuery($sql);
				$row	=	mysqli_fetch_array($result);
				?><tr><td bgcolor="<?php if($flag==1){echo $color_diff_color_arr['marketing_itemId'];} else {echo $tdColor;} ?>" class="normalfnt" nowrap="nowrap"><?php echo $row['strName'];?> </td></tr><?php	
				}
			  ?></table></td>
                <td bgcolor="#ffffffff">
                <table bgcolor="#ffffffff" width="100%" border="1" cellspacing="0" cellpadding="0">
				<?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];?>
                <tr><td bgcolor="<?php if($flag==1){echo $color_diff_color_arr['dblQty'];} else {echo $tdColor;} ?>"><?PHP echo ($row_ti['dblQty']<=0?'':$row_ti['dblQty']); ?></td></tr>
                <?php
				}
				?>
                </table></td>
                <td bgcolor="#ffffffff" class="normalfnt"><table bgcolor="#ffffffff" width="100%" border="1" cellspacing="0" cellpadding="0">
				<?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];?>
<tr><td nowrap="nowrap"  bgcolor="<?php if($flag==1){echo $color_diff_color_arr['size_w'];} else {echo $tdColor;} ?>" class="normalfnt"><?PHP echo ($row_ti['size_w']<=0?'':$row_ti['size_w']); ?>
                  W
                  <?PHP echo ($row_ti['size_h']<=0?'':$row_ti['size_h']); ?>
                  H</td></tr>
<?php
				}
				?>
                </table></td>
                <td bgcolor="<?php echo $tdColor; ?>"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                  <?php 
					$sql = "	SELECT
									trn_sampleinfomations_details_technical.intInkTypeId,
									trn_sampleinfomations_details_technical.intNoOfShots,
									trn_sampleinfomations_details_technical.intItem,
									trn_sampleinfomations_details_technical.dblColorWeight,
									mst_inktypes.strName AS techName,
									mst_item.strName AS itemName
								FROM
								trn_sampleinfomations_details_technical
									left Join mst_inktypes ON mst_inktypes.intId = trn_sampleinfomations_details_technical.intInkTypeId
									left Join mst_item ON mst_item.intId = trn_sampleinfomations_details_technical.intItem
								WHERE
									trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
									trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
									trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND
									trn_sampleinfomations_details_technical.strPrintName 	=  '".$arrT['printName']."' AND
									trn_sampleinfomations_details_technical.strComboName 	=  '".$arrT['combo']."' AND
									trn_sampleinfomations_details_technical.intColorId 		=  '".$arrT['intColorId']."'
								";
						$result_tech = $db->RunQuery($sql);
						while($row_tech=mysqli_fetch_array($result_tech))
						{
					?>
                  <tr class="normalfnt">
                    <td width="40%" bgcolor="#FFFFFF"><?php echo $row_tech['techName']; ?></td>
                    <td width="13%" bgcolor="#FFFFFF"><?php echo $row_tech['intNoOfShots']; ?></td>
                    <td width="35%" bgcolor="#FFFFFF"><?php echo $row_tech['itemName']; ?></td>
                    <td width="12%" bgcolor="#FFFFFF"><?php echo $row_tech['dblColorWeight']; ?></td>
                    </tr>
                  <?php
						}
					?>
                  </table></td>
                </tr>
              <?php
					}
				}
				?>
              </table></td>
            <?php
					}
				?>
            </tr>
          <?php
		  	for($mainRow=1;$mainRow<$rowsCount;$mainRow++)
			{
  				$arr_combo					=get_combo_arr($sampleNo,$sampleYear,$revNo);
 				$arr_combo_p				=get_combo_arr($sampleNo,$sampleYear,$revNo_p);
				$flag						=in_array($arrCombo[$mainRow]['combo'],$arr_combo_p);
 				//print_r($arr_combo_p);
				if($flag==1) {
					$tdColor='#F0F0F0'; //existing
				}
				else
					$tdColor='#A6FFA6';//new
 				
		  ?>
          <tr class="">
            <td height="18" bgcolor="#CCCCCC" class="gridHeader" ><input disabled="disabled"   class="validate[required,maxSize[20]]" value="<?php echo $arrCombo[$mainRow]['combo']; ?>" style="width:80px; background:<?php echo $tdColor; ?>" type="text" name="txtCombo2" id="txtCombo2" /></td>
            <?php
                 for($i=1;$i<=$cellCount;$i++)
                 {
					  $sql 	= "SELECT
									trn_sampleinfomations_prices.dblPrice,
									costing_sample_header.QTY_PRICES
									FROM
									trn_sampleinfomations_prices left join  costing_sample_header on 
									costing_sample_header.SAMPLE_NO = trn_sampleinfomations_prices.intSampleNo AND 
									costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations_prices.intSampleYear AND 
									costing_sample_header.REVISION = trn_sampleinfomations_prices.intRevisionNo AND 
									costing_sample_header.COMBO = trn_sampleinfomations_prices.strCombo AND 
									costing_sample_header.PRINT = trn_sampleinfomations_prices.strPrintName
									WHERE
									trn_sampleinfomations_prices.intSampleNo = '$sampleNo' AND
									trn_sampleinfomations_prices.intSampleYear = '$sampleYear' AND
									trn_sampleinfomations_prices.intRevisionNo = '$revNo' AND
									trn_sampleinfomations_prices.strCombo ='".$arrCombo[$mainRow]['combo']."' AND
									trn_sampleinfomations_prices.strPrintName = '"."print ".($i)."' 
									";

						$result = 	$db->RunQuery($sql);
						$row	=	mysqli_fetch_array($result);
                ?>
            <td align="right" bgcolor="#FFFFFF" class="normalfntRight" valign="top" ><table width="100%" cellspacing="0" cellpadding="0">
            <tr bgcolor="#CCCCCC"  class="gridHeader"><td width="12%" align="left" class="normalfnt" style="display:none">No of ups</td>
            <td width="7%" style="display:none"><input align="left" value="<?php echo getUps($arrCombo[$mainRow]['combo'],'print '.($i));?>" style="width:40px; text-align:right" type="text" name="ups" id="ups"  class="cls_ups"/></td>
            <td width="31%"><a id="butProcess2" class="button green medium butProcess" style="" name="butProcess"> Routing Process</a>&nbsp;</td>
            <td width="1%" class="normalfnt">&nbsp;</td>
            <td width="18%" align="left">&nbsp;</td>
            <td width="14%"><div style="display:none" class="combo"><?php echo $arrCombo[$mainRow]['combo']; ?></div><div  style="display:none" class="print">print <?php echo $i; ?></div>&nbsp;</td>
                <?php
                if ($row['QTY_PRICES'] == 1){
                    ?>
                    <td width="27%"  class="normalfnt"><a id="qtyPrices" class="button green medium" style="" name="qtyPrices"> Qty Prices</a></td>
                    <?php

                } else {
                    ?>
                    <td align="center" bgcolor="#FFFFFF" class="printName" >&nbsp; Price : <?php echo $row['dblPrice']; ?> </td>                    <?php
                }
                ?>
           	</tr>
            </table></td>
            <?php
                }
                ?>
            <td align="center" bgcolor="#FFFFFF" >&nbsp;</td>
             </tr>
            <?php
			$arr_combo_details		=	get_combo_details_arr($sampleNo,$sampleYear,$arrCombo[$mainRow]['combo'],$revNo);
			$arr_combo_details_p	=	get_combo_details_arr($sampleNo,$sampleYear,$arrCombo[$mainRow]['combo'],$revNo_p);
			$tdColor='#FFFFFF'; //existing
			$tdColorNew='#A6FFA6';//new
			?>
          <tr class="dataRow">
            <td width="18%"  bgcolor="#FFFFFF" ><select disabled="disabled"   class="printMode validate[required]" name="cboPrintMode2" id="cboPrintMode2" style="width:100px; background:<?php if($arr_combo_details['printMode']==$arr_combo_details_p['printMode']){echo $tdColor;} else {echo $tdColorNew;} ?>">
              <option value=""></option>
              <?php
					$sql = "select intId,strName from mst_printmode where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[$mainRow]['printMode'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select>
              <select disabled="disabled"   class="washStanderd validate[required]" name="butWashingStanderd2" id="butWashingStanderd3" style="width:100px; background:<?php if($arr_combo_details['wash']==$arr_combo_details_p['wash']){echo $tdColor;} else {echo $tdColorNew;} ?>">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_washstanderd where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[$mainRow]['wash'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
                </select>
              <select disabled="disabled"  class="colors validate[required]" name="cboGroundColor2" id="cboGroundColor2" style="width:100px; background:<?php if($arr_combo_details['groundColor']==$arr_combo_details_p['groundColor']){echo $tdColor;} else {echo $tdColorNew;} ?>">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_colors_ground where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[$mainRow]['groundColor'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";		
					}
				?>
                </select>
              <select disabled="disabled"  class="colors validate[required]" name="cboFabricType" id="cboFabricType" style="width:100px;  background:<?php if($arr_combo_details['fabricType']==$arr_combo_details_p['fabricType']){echo $tdColor;} else {echo $tdColorNew;} ?>">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_fabrictype where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[$mainRow]['fabricType'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
                </select></td>
            <?php 
			  	/////////// color grid and technique and type of grid cell ////////////
				for($i=1;$i<=$cellCount;$i++)
                 {
			  ?>
            <td  width="67%"  bgcolor="#FFFFFF" style="padding-top:0px" ><table id="tblGrid2" class="tblGrid2" bgcolor="#006666" cellpadding="0" cellspacing="1" width="650" border="0">
              <tr class="normalfntMid">
                <td width="70" bgcolor="#CEE8FB">Color</td>
                <td width="100" bgcolor="#CEE8FB">Technique</td>
                <td width="100" bgcolor="#CEE8FB">Item</td>
                <td width="40" bgcolor="#CEE8FB">Qty</td>
                <td width="86" bgcolor="#CEE8FB">Size(inch)</td>
                <td bgcolor="#FFE9D2"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                  <tr class="normalfnt">
                    <td  width="40%" bgcolor="#DFFFDF">Ink Type</td>
                    <td width="13%" bgcolor="#DFFFDF">Shots</td>
                    <td width="35%" bgcolor="#DFFFDF">Item</td>
                    <td width="12%" bgcolor="#DFFFDF">Weight</td>
                    </tr>
                  </table></td>
                </tr>
              <?php
			// print_r($arrCombo[0]['combo']);
				foreach($arrType as $arrT)
				{
					if($arrT['combo']==$arrCombo[$mainRow]['combo'] && $arrT['printName']=='print '.$i)
					{
				?>
          <?php 
  				$arr_color					=get_color_arr($sampleNo,$sampleYear,$arrT['printName'],$arrT['combo'],$arrT['intColorId'],$revNo);
  				$arr_color_p				=get_color_arr($sampleNo,$sampleYear,$arrT['printName'],$arrT['combo'],$arrT['intColorId'],$revNo_p);
 				//$flag						=in_array($arrT['intColorId'],$arr_color_p);
				
				if($arr_color['intColorId']==$arr_color_p['intColorId']) {
					$flag	=1;
					$tdColor='#FFFFFF'; //existing
				}
				else{
					$tdColor='#A6FFA6';//new
					$flag=0;
 				}
					
				$color_diff_arr			=array_diff_assoc($arr_color,$arr_color_p);
				$color_diff_color_arr	=get_diff_color($arr_color,$color_diff_arr); 
  	
	   	?> 
               <tr>
                <td bgcolor="<?php if($flag==1){echo $color_diff_color_arr['intColorId'];} else {echo $tdColor;} ?>" class="normalfnt" id="<?php echo $arrT['intColorId'] ?>"><?php echo $arrT['colorName'] ?></td>
                <td bgcolor="<?php if($flag==1){echo $color_diff_color_arr['typeOfPrintId'];} else {echo $tdColor;} ?>" class="normalfnt" ><?php
			  	$sql 	= "SELECT mst_techniques.strName FROM mst_techniques 
						WHERE mst_techniques.intId = '".$arrT['typeOfPrintId']."'";
				$result = 	$db->RunQuery($sql);
				$row	=	mysqli_fetch_array($result);
				echo $row['strName'];
			  ?></td>
                <td bgcolor="#ffffffff" class="normalfnt">
                <table bgcolor="#ffffffff" width="100%" cellspacing="0" cellpadding="0" border="1">
				<?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];
			  
			  	$sql = "SELECT
							mst_item.intId,
							mst_item.strName
						FROM `mst_item`
						WHERE
							mst_item.intId =  '".$item_tech."'";
				$result = 	$db->RunQuery($sql);
				$row	=	mysqli_fetch_array($result);
				?><tr><td bgcolor="<?php if($flag==1){echo $color_diff_color_arr['marketing_itemId'];} else {echo $tdColor;} ?>" class="normalfnt" nowrap="nowrap"><?php echo $row['strName'];?> </td></tr><?php	
				}
			  ?></table></td>
                <td bgcolor="#ffffffff">
                <table bgcolor="#ffffffff" width="100%" border="1" cellspacing="0" cellpadding="0">
				<?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];?>
                <tr><td bgcolor="<?php if($flag==1){echo $color_diff_color_arr['dblQty'];} else {echo $tdColor;} ?>"><?PHP echo ($row_ti['dblQty']<=0?'':$row_ti['dblQty']); ?></td></tr>
                <?php
				}
				?>
                </table></td>
                <td bgcolor="#ffffffff" class="normalfnt"><table bgcolor="#ffffffff" width="100%" border="1" cellspacing="0" cellpadding="0">
				<?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];?>
<tr><td nowrap="nowrap"  bgcolor="<?php if($flag==1){echo $color_diff_color_arr['size_w'];} else {echo $tdColor;} ?>" class="normalfnt"><?PHP echo ($row_ti['size_w']<=0?'':$row_ti['size_w']); ?>
                  W
                  <?PHP echo ($row_ti['size_h']<=0?'':$row_ti['size_h']); ?>
                  H</td></tr>
<?php
				}
				?>
                </table></td>
                <td bgcolor="<?php echo $tdColor ?>"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                  <?php 
					$sql = "	SELECT
									trn_sampleinfomations_details_technical.intInkTypeId,
									trn_sampleinfomations_details_technical.intNoOfShots,
									trn_sampleinfomations_details_technical.intItem,
									trn_sampleinfomations_details_technical.dblColorWeight,
									mst_inktypes.strName AS techName,
									mst_item.strName AS itemName
								FROM
								trn_sampleinfomations_details_technical
									left Join mst_inktypes ON mst_inktypes.intId = trn_sampleinfomations_details_technical.intInkTypeId
									left Join mst_item ON mst_item.intId = trn_sampleinfomations_details_technical.intItem
								WHERE
									trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
									trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
									trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND
									trn_sampleinfomations_details_technical.strPrintName 	=  '".$arrT['printName']."' AND
									trn_sampleinfomations_details_technical.strComboName 	=  '".$arrT['combo']."' AND
									trn_sampleinfomations_details_technical.intColorId 		=  '".$arrT['intColorId']."'
								";
						$result_tech = $db->RunQuery($sql);
						while($row_tech=mysqli_fetch_array($result_tech))
						{
							$arr_inkType			=get_inkType_arr($sampleNo,$sampleYear,$arrT['printName'],$arrT['combo'],$arrT['intColorId'],$row_tech['intInkTypeId'],$revNo);
							$arr_inkType_p			=get_inkType_arr($sampleNo,$sampleYear,$arrT['printName'],$arrT['combo'],$arrT['intColorId'],$row_tech['intInkTypeId'],$revNo_p);
						
							$flag=0;
							if($arr_inkType['inkType']==$arr_inkType_p['inkType']) {
								$tdColor='#FFFFFF'; //existing
							$flag=1;
							}
							else
								$tdColor='#A6FFA6';//new
							
							$inkType_diff_arr			=array_diff_assoc($arr_inkType,$arr_inkType_p);
							$inkType_diff_color_arr		=get_diff_color($arr_inkType,$inkType_diff_arr); 
					?>
                  <tr class="normalfnt">
                    <td width="40%"  bgcolor="<?php if($flag==1){echo $inkType_diff_color_arr['inkType'];} else {echo $tdColor;} ?>"><?php echo $row_tech['techName']; ?></td>
                    <td width="13%"  bgcolor="<?php if($flag==1){echo $inkType_diff_color_arr['shots'];} else {echo $tdColor;} ?>"><?php echo $row_tech['intNoOfShots']; ?></td>
                    <td width="35%"  bgcolor="<?php if($flag==1){echo $inkType_diff_color_arr['item'];} else {echo $tdColor;} ?>"><?php echo $row_tech['itemName']; ?></td>
                    <td width="12%"  bgcolor="<?php if($flag==1){echo $inkType_diff_color_arr['weight'];} else {echo $tdColor;} ?>"><?php echo $row_tech['dblColorWeight']; ?></td>
                    </tr>
                  <?php
						}
					?>
                  </table></td>
                </tr>
              <?php
					}
				}
				?>
              </table></td>
            <?php
				 }
			?>
             </tr>
          <?php
			}
		  ?>
          <tr class="dataRow">
            <td  bgcolor="#FFFFFF" >&nbsp;</td>
            <td  bgcolor="#FFFFFF" style="padding-top:0px" >&nbsp;</td>
            <?php
				for($i=1;$i<$cellCount;$i++)
				{
			?>
            <td  bgcolor="#FFFFFF" style="padding-top:0px" >&nbsp;</td>
            <?php
				}
			?>
            </tr>
          </table></td>
        <td width="6%">&nbsp;</td>
        </tr>
      <tr>
      <tr><td colspan="3">
<table width="800" border="0" cellspacing="0" cellpadding="0">
  <tr>
                <?php
			  	$sql = "
						SELECT
						mst_maincategory.strName AS main_cat,
						mst_subcategory.strName AS sub_cat,
						trn_sample_non_direct_rm_consumption.ITEM as item_id,
						mst_item.strName AS item,
						mst_units.strCode,
						trn_sample_non_direct_rm_consumption.CONSUMPTION 
						FROM
						trn_sample_non_direct_rm_consumption
						INNER JOIN mst_item ON trn_sample_non_direct_rm_consumption.ITEM = mst_item.intId
						INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
						INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
						INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
						WHERE
						trn_sample_non_direct_rm_consumption.SAMPLE_NO = '$sampleNo' AND
						trn_sample_non_direct_rm_consumption.SAMPLE_YEAR = '$sampleYear' AND
						trn_sample_non_direct_rm_consumption.REVISION_NO = '$revNo'
						ORDER BY
						main_cat ASC,
						sub_cat ASC,
						item ASC				";
				$result = $db->RunQuery($sql);
				$i=0;
				while($row=mysqli_fetch_array($result))
				{
					$i++;
					?>
					<?php
					if($i==1){
					?>
                    <tr><td colspan="2"  bgcolor="#F4D3C6" class="normalfnt">Non-Direct Raw Materials</td></tr>
                    <tr><td colspan="2">
                    <table id="tblNoneDirRm" class="tblGrid" bgcolor="#006666" cellpadding="0" cellspacing="1" width="650" border="0">
                    <tr class="normalfntMid">
                    <td width="151" bgcolor="#CEE8FB">Main Category</td>
                    <td width="137" bgcolor="#CEE8FB">Sub Category</td>
                    <td width="137" bgcolor="#CEE8FB">Item</td>
                    <td width="55" bgcolor="#CEE8FB">UOM</td>
                    <td width="122" bgcolor="#CEE8FB">Consumption</td>
                    </tr>
					<?php
					}
					?>
                   <tr>
                     <td bgcolor="#ffffffff" class="normalfnt" id=""><?php echo $row['main_cat'] ?></td>
                    <td bgcolor="#ffffffff" class="normalfnt" ><?php echo $row['sub_cat'] ?></td>
                    <td bgcolor="#ffffffff" class="cls_item"><div style="display:none" id="item"><?php echo $row['item_id'] ?></div><span class="normalfnt"><?php echo $row['item'] ?></span></td>
                    <td bgcolor="#ffffffff"><span class="normalfnt"><?php echo $row['strCode'] ?></span></td>
                    <td bgcolor="#ffffffff" class="normalfntRight" align="right"><?php echo $row['CONSUMPTION'] ?> &nbsp;</td>
                    </tr>
                <?php
				}
				?>
             </table>       </td></tr></table>
      </td></tr>
        <td colspan="3"><table width="800" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="21%" class="normalfnt"><strong>Light Box</strong></td>
            <td width="79%" rowspan="2" align="center" valign="middle"><table width="96%" class="grid" id="tblMainGrid2" cellspacing="0" cellpadding="0">
              <tr class="gridHeader">
                <td height="13" colspan="5">Verivide</td>
                <td colspan="6">Macbeth</td>
                </tr>
              <tr bgcolor="#FFFFFF" class="normalfntMid">
                <td width="9%" height="13">D65</td>
                <td width="10%">TL84</td>
                <td width="10%">CW</td>
                <td width="10%">F</td>
                <td width="10%">UV</td>
                <td width="9%">DL</td>
                <td width="9%">CW</td>
                <td width="9%" >lnca</td>
                <td width="9%">TL84</td>
                <td width="8%" >UV</td>
                <td width="7%">Horizon</td>
                </tr>
              <tr bgcolor="#FFFFFF">
                <td height="20" align="center"><strong>
                  <input style="background:<?php echo $header_diff_color_arr['verivide_d65'];  ;?>"  type="checkbox" disabled="disabled" <?php echo($header_arr['verivide_d65']?'checked':''); ?> />
                  </strong></td>
                <td align="center"><strong>
                  <input style="background:<?php echo $header_diff_color_arr['verivide_tl84'];  ;?>" type="checkbox" disabled="disabled" <?php echo($header_arr['verivide_tl84']?'checked':''); ?> />
                  </strong></td>
                <td align="center"><strong>
                  <input style="background:<?php echo $header_diff_color_arr['verivide_cw'];  ;?>" type="checkbox" disabled="disabled" <?php echo($header_arr['verivide_cw']?'checked':''); ?> />
                  </strong></td>
                <td align="center"><strong>
                  <input style="background:<?php echo $header_diff_color_arr['verivide_f'];  ;?>" type="checkbox" disabled="disabled" <?php echo($header_arr['verivide_f']?'checked':''); ?> />
                  </strong></td>
                <td align="center"><strong>
                  <input style="background:<?php echo $header_diff_color_arr['verivide_uv'];  ;?>" type="checkbox" disabled="disabled" <?php echo($header_arr['verivide_uv']?'checked':''); ?> />
                  </strong></td>
                <td align="center"><strong>
                  <input style="background:<?php echo $header_diff_color_arr['macbeth_dl'];  ;?>" type="checkbox"disabled="disabled"  <?php echo($header_arr['macbeth_dl']?'checked':''); ?> />
                  </strong></td>
                <td align="center"><strong>
                  <input style="background:<?php echo $header_diff_color_arr['macbeth_cw'];  ;?>" type="checkbox" disabled="disabled" <?php echo($header_arr['macbeth_cw']?'checked':''); ?> />
                  </strong></td>
                <td align="center" ><strong>
                  <input style="background:<?php echo $header_diff_color_arr['macbeth_inca'];  ;?>" type="checkbox" disabled="disabled" <?php echo($header_arr['macbeth_inca']?'checked':''); ?> />
                  </strong></td>
                <td align="center"><strong>
                  <input style="background:<?php echo $header_diff_color_arr['macbeth_tl84'];  ;?>" type="checkbox" disabled="disabled" <?php echo($header_arr['macbeth_tl84']?'checked':''); ?> />
                  </strong></td>
                <td align="center" ><strong>
                  <input style="background:<?php echo $header_diff_color_arr['macbeth_uv'];  ;?>" type="checkbox" disabled="disabled" <?php echo($header_arr['macbeth_uv']?'checked':''); ?> />
                  </strong></td>
                <td align="center"><strong>
                  <input style="background:<?php echo $header_diff_color_arr['macbeth_horizon'];  ;?>" type="checkbox" disabled="disabled"  <?php echo($header_arr['macbeth_horizon']?'checked':''); ?> />
                  </strong></td>
                </tr>
              </table></td>
            </tr>
          <tr>
            <td class="normalfnt"><strong>Light Source</strong></td>
            </tr>
          <tr>
            <td width="21%" class="normalfnt"><strong>Curing Condition</strong></td>
            <td align="center" valign="middle"><table width="100%">
              <tr>
                <td width="7%" class="normalfnt">Temp</td>
                <td width="3%" align="center" valign="middle"><strong>:</strong></td>
                <td width="11%" class="normalfnt" ><span class="normalfnt" style="background:<?php echo $header_diff_color_arr['curingTemp']; ?>"><?php echo $header_arr['curingTemp'] ?></span></td>
                <td width="15%" class="normalfnt">Belt Speed</td>
                <td width="9%" align="center" valign="middle"><strong>:</strong></td>
                <td width="55%"><span class="normalfnt" style="background:<?php echo $header_diff_color_arr['curingSpeed']; ?>"><?php echo $header_arr['curingSpeed'] ?></span></td>
                </tr>
            </table></td>
            </tr>
          <tr>
            <td class="normalfnt"><strong>Press Condition</strong></td>
            <td align="center" valign="middle"><table width="100%">
              <tr>
                <td width="7%" class="normalfnt">Temp</td>
                <td width="3%" align="center" valign="middle"><strong>:</strong></td>
                <td width="11%" class="normalfnt"><span class="normalfnt" style="background:<?php echo $header_diff_color_arr['pressTemp']; ?>"><?php echo $header_arr['pressTemp'] ?></span></td>
                <td width="15%" class="normalfnt">Pressure</td>
                <td width="3%" align="center" valign="middle"><strong>:</strong></td>
                <td width="9%"><span class="normalfnt" style="background:<?php echo $header_diff_color_arr['pressPressure']; ?>"><?php echo $header_arr['pressPressure'] ?></span></td>
                <td width="9%" align="center" valign="middle" class="normalfnt">Time</td>
                <td width="2%" align="center"><strong>:</strong></td>
                <td width="41%"><span class="normalfnt" style="background:<?php echo $header_diff_color_arr['pressTime'];  ?>"><?php echo $header_arr['pressTime'] ?></span></td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td class="normalfnt"><strong>Mesh Count</strong></td>
            <td align="center" valign="middle" ><span class="normalfnt" style="background:<?php echo $header_diff_color_arr['meshCount'];  ?>"><?php echo $header_arr['meshCount'] ?></span></td>
          </tr>
          <?php
		  if($header_diff_color_arr['instructionsTech']=='#FFFFFF'){
				$header_diff_color_arr['instructionsTech']='#CCCCFF';  
		  }
		  ?>
          <tr>
            <td class="normalfnt"><strong>Technical Instructions</strong></td>
            <td align="center" valign="middle" bgcolor="#CCCCFF" ><span class="normalfnt" style="background:<?php echo $header_diff_color_arr['instructionsTech'];  ;?>"><?php echo $header_arr['instructionsTech'] ?></span></td>
          </tr>
          <?php
		  if($header_diff_color_arr['instructions']=='#FFFFFF'){
				$header_diff_color_arr['instructions']='#E0FAC9';  
		  }
		  ?>
          <tr>
            <td class="normalfnt"><strong>Marketing Instructions</strong></td>
            <td align="center" valign="middle" bgcolor="#E0FAC9"><span class="normalfnt" style="background:<?php echo $header_diff_color_arr['instructions'];  ;?>"><?php echo $header_arr['instructions'] ?></span></td>
          </tr>
          <tr>
            <td class="normalfnt">&nbsp;</td>
            <td align="left" valign="middle" bgcolor="#FFFFFF"><iframe id="iframeFiles" src="presentation/costing/sample/samplePrices/addNew/filesUpload.php?txtFolder=<?php echo "$sampleNo-$sampleYear"; ?>" name="iframeFiles" style="width:400px;height:200px;border:none"  ></iframe></td>
          </tr>
<?php
if($header_arr['mainStatus']<>1)
{
	
?>
<div <?php /*?>id="apDiv1"<?php */?>><img src="images/pending.png"  /></div>
<?php
}
?>
          <tr>
            <td class="normalfnt">&nbsp;</td>
            <td align="left" valign="middle" bgcolor="#FFFFFF"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="normalfnt">
              <tr>
                <td colspan="2" bgcolor="" class="normalfnt"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="12%">&nbsp;</td>
                    <td width="22%" align="center" valign="middle">&nbsp;</td>
                    <td width="46%">&nbsp;</td>
                    <td width="4%">&nbsp;</td>
                    <td width="4%">&nbsp;</td>
                    <td width="4%">&nbsp;</td>
                    <td width="4%">&nbsp;</td>
                    <td width="4%">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td colspan="2" align="center" bgcolor="" class="reportSubHeader"><span class="normalfnt">Approved By</span></td>
              </tr>
              <tr>
                <td bgcolor="#DCF0FC" class="normalfnt"><span class="normalfnt"><strong>1st</strong> Stage (Marketing)</span></td>
                <td bgcolor="#DCF0FC"><span class="normalfnt"><strong>2nd</strong> Stage (Technical)</span></td>
              </tr>
              <tr>
                <td width="65%" bgcolor="" class="normalfnt"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="25%" height="31"><span class="normalfnt"><strong>PREPARED BY:</strong></span></td>
                    <td width="75%"><span class="normalfnt"><?php echo "$firstUser ($firstDate)"; ?><br />
                    </span></td>
                  </tr>
                </table></td>
                <td width="35%" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="37%"><span class="normalfnt"><strong>PREPARED BY:</strong></span></td>
                    <td width="63%"><span class="normalfnt"><?php echo "$secondUser ($secondDate)"; ?><br />
                    </span></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td bgcolor="" class="normalfnt"><table width="313" border="0" cellspacing="0" cellpadding="0">
                  <?php  
			    //$firstApproveLevel = (int)getApproveLevel('Sample Infomations Sheet');
				for($i=1; $i<$header_arr['marketingApproveLevelStart']; $i++)
				{
					 $sqlc = "SELECT
							intApproveUser,
							dtApprovedDate,
							sys_users.strUserName as UserName,
							intApproveLevelNo
							FROM
							trn_sampleinfomations_approvedby
							Inner Join sys_users ON trn_sampleinfomations_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							intSampleNo 	=  '$sampleNo' AND
							intSampleYear 	=  '$sampleYear' AND
							intRevNo		=  '$revNo' AND
							intApproveLevelNo =  '$i' AND intStage=1";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
						if($i==1)
							$desc="1st ";
						else if($i==2)
							$desc="2nd ";
						else if($i==3)
							$desc="3rd ";
						else
							$desc=$i."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']." (".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='--------------------';
				?>
                  <tr>
                    <td width="313" height="21" bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Appoved By - </strong><?php echo $desc2;?></span></td>
                  </tr>
                  <?php
			}
				
			?>
                </table></td>
                <td><table width="313" border="0" cellspacing="0" cellpadding="0">
                  <?php  

				for($i=1; $i<$header_arr['technicalApproveLevelStart']; $i++)
				{
					 $sqlc = "SELECT
							intApproveUser,
							dtApprovedDate,
							sys_users.strUserName as UserName,
							intApproveLevelNo
							FROM
							trn_sampleinfomations_approvedby
							Inner Join sys_users ON trn_sampleinfomations_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							intSampleNo 	=  '$sampleNo' AND
							intSampleYear 	=  '$sampleYear' AND
							intRevNo		=  '$revNo' AND
							intApproveLevelNo =  '$i' AND intStage=2";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
						if($i==1)
							$desc="1st ";
						else if($i==2)
							$desc="2nd ";
						else if($i==3)
							$desc="3rd ";
						else
							$desc=$i."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']." (".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='--------------------';
				?>
                  <tr>
                    <td width="313" height="21" bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Appoved By - </strong><?php echo $desc2;?></span></td>
                  </tr>
                  <?php
			}
				
			?>
                </table></td>
              </tr>
            </table></td>
          </tr>
          </table></td>
      </tr>
      </table>
    </td>
</tr>
</table>
</div> 
</form>
	<div style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
    <div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>  
<?php
function getMaxRevisionNo($sampleNo,$sampleYear){
	global $db;
	$sql = "SELECT
			Max(trn_sampleinfomations.intRevisionNo) as maxRevNo
			FROM trn_sampleinfomations
			WHERE
			trn_sampleinfomations.intSampleNo =  '$sampleNo' AND
			trn_sampleinfomations.intSampleYear =  '$sampleYear'
			";
	$result 		= $db->RunQuery($sql);
	$row			=mysqli_fetch_array($result);
	$maxRevNo 		= $row['maxRevNo'];
	return $maxRevNo;
}

function getSampleNoSql($sampleYear,$graficRef,$locationId){
	global $db;
	$sql = "SELECT DISTINCT
			trn_sampleinfomations.intSampleNo
			FROM trn_sampleinfomations
		WHERE
			trn_sampleinfomations.intSampleYear =  '$sampleYear' 
			and intCompanyId = '$locationId' ";
		if($graficRef!='')	
		$sql .= " and strGraphicRefNo = '$graficRef' ";	
		
		$sql .= "ORDER BY
			trn_sampleinfomations.intSampleNo ASC
	";
	//echo $sql;
	return $sql;
 }
 
function getSampleYearSql(){
	global $db;
	$sql = "SELECT DISTINCT
			trn_sampleinfomations.intSampleYear
			FROM trn_sampleinfomations
			ORDER BY
			trn_sampleinfomations.intSampleYear DESC";
	return $sql;
}

function getGraphicNoSql($sampleYear){
	global $db;
	$sql = "SELECT DISTINCT
				trn_sampleinfomations.strGraphicRefNo
			FROM trn_sampleinfomations 
			WHERE
			trn_sampleinfomations.intSampleYear =  '$sampleYear'
			ORDER BY
				trn_sampleinfomations.strGraphicRefNo ASC
	";
	return $sql;
}

function getRevisionNoSql($sampleNo,$sampleYear){
	global $db;
	$sql = "SELECT
		trn_sampleinfomations.intRevisionNo
		FROM trn_sampleinfomations
		WHERE
		trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
		trn_sampleinfomations.intSampleNo =  '$sampleNo'
		ORDER BY
		trn_sampleinfomations.intRevisionNo ASC
	";
	return $sql;
}

function get_sampleInfoHeaderResults($sampleNo,$sampleYear,$revNo){
	global $db;
	
		$sql = "SELECT
					sampleinfo.intSampleNo,
					sampleinfo.intSampleYear,
					sampleinfo.intRevisionNo,
					sampleinfo.dtDate,
					
					sampleinfo.strGraphicRefNo,
					sampleinfo.intCustomer,
					sampleinfo.strStyleNo,
					sampleinfo.intBrand,
					sampleinfo.verivide_d65,
					sampleinfo.verivide_tl84,
					sampleinfo.verivide_cw,
					sampleinfo.verivide_f,
					sampleinfo.verivide_uv,
					sampleinfo.macbeth_dl,
					sampleinfo.macbeth_cw,
					sampleinfo.macbeth_inca,
					sampleinfo.macbeth_tl84,
					sampleinfo.macbeth_uv,
					sampleinfo.macbeth_horizon,
					sampleinfo.dblCuringCondition_temp,
					sampleinfo.dblCuringCondition_beltSpeed,
					sampleinfo.dblPressCondition_temp,
					sampleinfo.dblPressCondition_pressure,
					sampleinfo.dblPressCondition_time,
					sampleinfo.strMeshCount,
					sampleinfo.strAdditionalInstructions,
					sampleinfo.strAdditionalInstructionsTech,
					sampleinfo.intMarketingApproveLevelStart,
					sampleinfo.intTechnicalApproveLevelStart,
					sampleinfo.intCompanyId as locationId,
					sampleinfo.intStatus,
					sampleinfo.intMarketingStatus,
					sampleinfo.intCreator,
					sampleinfo.dtmCreateDate,
					sampleinfo.intTechUser,
					sampleinfo.dtmTechEnterDate,
					sampleinfo.intMarketingUser,
					sampleinfo.dtmMerketingDate,
					sampleinfo.intModifyer,
					sampleinfo.dtmModifyDate,
					cus.strName AS customerName,
					brand.strName AS brandName,
					firstUser.strUserName AS firstUser,
					seconduser.strUserName AS secondUser
				FROM
					trn_sampleinfomations AS sampleinfo
					left Join mst_customer AS cus ON cus.intId = sampleinfo.intCustomer
					left Join mst_brand AS brand ON brand.intId = sampleinfo.intBrand
					Left Join sys_users AS firstUser ON sampleinfo.intCreator = firstUser.intUserId
					Left Join sys_users AS seconduser ON seconduser.intUserId = sampleinfo.intTechUser
				where 
					sampleinfo.intSampleNO='$sampleNo' 
					and sampleinfo.intSampleYear='$sampleYear' 
					and sampleinfo.intRevisionNo='$revNo'
				";
	
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	
					$header_arr['locationId']= $row['locationId'];
					$header_arr['sampleNo']= $row['intSampleNo'];
					
					$header_arr['mainStatus']		 = $row['intStatus'];
					$header_arr['marketingStatus'] = $row['intMarketingStatus'];
					
					$header_arr['marketingApproveLevelStart'] = $row['intMarketingApproveLevelStart'];
					$header_arr['technicalApproveLevelStart'] = $row['intTechnicalApproveLevelStart'];
					
					$header_arr['sampleYear'] = $row['intSampleYear'];
					$header_arr['sampleRevision'] = $row['intRevisionNo'];
					$header_arr['sampleDate'] = $row['dtDate'];
					$header_arr['deliveryDate'] = $row['dtDeliveryDate'];
					$header_arr['graficRef'] = $row['strGraphicRefNo'];
					$header_arr['customer'] = $row['customerName'];
					$header_arr['style'] = $row['strStyleNo'];
					$header_arr['brand'] = $row['brandName'];
					$header_arr['sampleQty'] = $row['dblSampleQty'];
					$header_arr['grade'] = $row['intGrade'];
					$header_arr['fabricType'] = $row['strFabricType'];
					$header_arr['curingTemp'] = $row['dblCuringCondition_temp'];
					$header_arr['curingSpeed'] = $row['dblCuringCondition_beltSpeed'];
					$header_arr['pressTemp'] = $row['dblPressCondition_temp'];
					$header_arr['pressPressure'] = $row['dblPressCondition_pressure'];
					$header_arr['pressTime'] = $row['dblPressCondition_time'];
					$header_arr['meshCount'] = $row['strMeshCount'];
					$header_arr['instructions'] = $row['strAdditionalInstructions']; 
					$header_arr['instructionsTech'] = $row['strAdditionalInstructionsTech'];
					$header_arr['verivide_d65'] = $row['verivide_d65'];
					$header_arr['verivide_tl84'] = $row['verivide_tl84'];
					$header_arr['verivide_cw'] = $row['verivide_cw'];
					$header_arr['verivide_f'] = $row['verivide_f'];
					$header_arr['verivide_uv'] = $row['verivide_uv'];
					$header_arr['macbeth_dl'] = $row['macbeth_dl'];
					$header_arr['macbeth_cw'] = $row['macbeth_cw'];
					$header_arr['macbeth_inca'] = $row['macbeth_inca'];
					$header_arr['macbeth_tl84'] = $row['macbeth_tl84'];
					$header_arr['macbeth_uv'] = $row['macbeth_uv'];
					$header_arr['macbeth_horizon'] = $row['macbeth_horizon'];
					
					$header_arr['firstUser'] 			= $row['firstUser'];
					$header_arr['firstDate'] 			= $row['dtmCreateDate'];
					
					$header_arr['secondUser'] 		= $row['secondUser'];
					$header_arr['secondDate'] 		= $row['dtmTechEnterDate'];
	
	
 	
 	
	return $header_arr;
 	
}

function get_sampleInfoPrintArr($sampleNo,$sampleYear,$print,$revNo){

	global $db;
	$sql = "SELECT
				trn_sampleinfomations_printsize.strPrintName,
				trn_sampleinfomations_printsize.intWidth,
				trn_sampleinfomations_printsize.intHeight,
				trn_sampleinfomations_printsize.intPart
			FROM `trn_sampleinfomations_printsize`
			WHERE
				trn_sampleinfomations_printsize.intSampleNo 	=  '$sampleNo' AND
				trn_sampleinfomations_printsize.intSampleYear 	=  '$sampleYear' AND
				trn_sampleinfomations_printsize.intRevisionNo 	=  '$revNo'  AND
				trn_sampleinfomations_printsize.strPrintName 	=  '$print'
			ORDER BY
			cast(SUBSTR(trn_sampleinfomations_printsize.strPrintName,7,2) as unsigned) ASC
			";
			
	$result = $db->RunQuery($sql);
	$arrPart = array();
	while($row=mysqli_fetch_array($result))
	{
		$arr['printName'] = $row['strPrintName'];
		$arr['width'] = $row['intWidth'];
		$arr['height'] = $row['intHeight'];
		$arr['part'] = $row['intPart'];
	//	$arrPart[] = $arr;
	}	
	return $arr;
}

function get_sampleInfoComboArr($sampleNo,$sampleYear,$revNo){
	
	global $db;
	$sql = "SELECT DISTINCT
			trn_sampleinfomations_details.strComboName,
			trn_sampleinfomations_details.intPrintMode,
			trn_sampleinfomations_details.intWashStanderd,
			trn_sampleinfomations_details.intGroundColor,
			trn_sampleinfomations_details.intFabricType
			FROM trn_sampleinfomations_details
			WHERE
			trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
			trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
			trn_sampleinfomations_details.intRevNo 		=  '$revNo'  
			order by strComboName ASC";
	
	//echo $sql;		
 	$result = $db->RunQuery($sql);
	$arrCombo = array();
	while($row=mysqli_fetch_array($result))
	{
		$arr['combo'] 		= $row['strComboName'];
		$arr['printMode'] 	= $row['intPrintMode'];
		$arr['groundColor'] = $row['intGroundColor'];
		$arr['wash'] 		= $row['intWashStanderd'];
		$arr['fabricType'] 	= $row['intFabricType'];
		$arrCombo[] 		= $arr;
	}
	return $arrCombo;
		
}

function get_combo_arr($sampleNo,$sampleYear,$revNo){
	
	global $db;
	$sql = "SELECT DISTINCT trn_sampleinfomations_details.strComboName
			FROM trn_sampleinfomations_details
			WHERE
			trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
			trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
			trn_sampleinfomations_details.intRevNo 		=  '$revNo'   ";
	
	//echo $sql;		
 	$result = $db->RunQuery($sql);
	$i=0;
	while($row=mysqli_fetch_array($result))
	{
		$arr[$i] 		= $row['strComboName'];
		$i++;
  	}
	return $arr;
		
		
}
function get_combo_details_arr($sampleNo,$sampleYear,$combo,$revNo){
	
	global $db;
	$sql = "SELECT DISTINCT
			trn_sampleinfomations_details.strComboName,
			trn_sampleinfomations_details.intPrintMode,
			trn_sampleinfomations_details.intWashStanderd,
			trn_sampleinfomations_details.intGroundColor,
			trn_sampleinfomations_details.intFabricType
			FROM trn_sampleinfomations_details
			WHERE
			trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
			trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
			trn_sampleinfomations_details.intRevNo 		=  '$revNo'   AND 
 			trn_sampleinfomations_details.strComboName 	=  '$combo'  
			order by strComboName ASC";
	//if(($revNo==1) && ($combo=='C 2')) 
	//echo $sql;		
 	$result = $db->RunQuery($sql);
	$arrCombo = array();
	while($row=mysqli_fetch_array($result))
	{
		$arr['combo'] 		= $row['strComboName'];
		$arr['printMode'] 	= $row['intPrintMode'];
		$arr['groundColor'] = $row['intGroundColor'];
		$arr['wash'] 		= $row['intWashStanderd'];
		$arr['fabricType'] 	= $row['intFabricType'];
  	}
	return $arr;
 }

function get_sampleInfoTypeArr($sampleNo,$sampleYear,$revNo){
	
	global $db;
 	  $sql = "SELECT
			trn_sampleinfomations_details.strPrintName,
			trn_sampleinfomations_details.strComboName,
			trn_sampleinfomations_details.intColorId,
			trn_sampleinfomations_details.intItem AS marketing_itemId,
			mst_colors.strName AS colorName,
			mst_techniques.strName AS typeOfPrintName,
			trn_sampleinfomations_details.intTechniqueId AS typeOfPrintId,
			marketingItem.strName AS marketing_itemName,
			trn_sampleinfomations_details.dblQty,
			trn_sampleinfomations_details.size_w,
			trn_sampleinfomations_details.size_h
			FROM
			trn_sampleinfomations_details
			Inner Join mst_colors ON mst_colors.intId = trn_sampleinfomations_details.intColorId
			Left Join mst_techniques ON mst_techniques.intId = trn_sampleinfomations_details.intTechniqueId
			Left Join mst_item AS marketingItem ON marketingItem.intId = trn_sampleinfomations_details.intItem
			
			WHERE
			trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
			trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
			trn_sampleinfomations_details.intRevNo =  '$revNo' 
			GROUP BY 
			trn_sampleinfomations_details.strPrintName,
			trn_sampleinfomations_details.strComboName,
			trn_sampleinfomations_details.intColorId,
			trn_sampleinfomations_details.intTechniqueId 
			";
	$result = $db->RunQuery($sql);
	$arrType= array();
	$arr= array();
	while($row=mysqli_fetch_array($result))
	{
		$arr['combo'] 				= $row['strComboName'];
		$arr['printName'] 			= $row['strPrintName'];
		
		$arr['intColorId'] 			= $row['intColorId'];
		$arr['marketing_itemId'] 	= $row['marketing_itemId'];
		$arr['colorName'] 			= $row['colorName'];
		$arr['typeOfPrintName'] 	= $row['typeOfPrintName'];
		$arr['typeOfPrintId'] 		= $row['typeOfPrintId'];
		$arr['marketing_itemName'] 	= $row['marketing_itemName'];
		$arr['dblQty'] 				= $row['dblQty'];
		
		$arr['size_w'] 					= $row['size_w'];
		$arr['size_h'] 					= $row['size_h'];
	
		$arrType[] 			= $arr;
	}
	return $arrType;
}
function get_color_arr($sampleNo,$sampleYear,$print,$combo,$color,$revNo){
	
	global $db;
 	  $sql = "SELECT 
			trn_sampleinfomations_details.intColorId,
			trn_sampleinfomations_details.intTechniqueId AS typeOfPrintId,
			trn_sampleinfomations_details.intItem AS marketing_itemId,
			IFNULL(trn_sampleinfomations_details.dblQty,0) as dblQty,
			IFNULL(trn_sampleinfomations_details.size_w,0) as size_w,
			IFNULL(trn_sampleinfomations_details.size_h,0) as size_h
			/*
			trn_sampleinfomations_details.strPrintName,
			trn_sampleinfomations_details.strComboName,
			mst_colors.strName AS colorName,
			mst_techniques.strName AS typeOfPrintName,
			marketingItem.strName AS marketing_itemName, */
			FROM
			trn_sampleinfomations_details
			Inner Join mst_colors ON mst_colors.intId = trn_sampleinfomations_details.intColorId
			Left Join mst_techniques ON mst_techniques.intId = trn_sampleinfomations_details.intTechniqueId
			Left Join mst_item AS marketingItem ON marketingItem.intId = trn_sampleinfomations_details.intItem
			
			WHERE
			trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
			trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
			trn_sampleinfomations_details.intRevNo =  '$revNo' AND 
			trn_sampleinfomations_details.strPrintName =  '$print' AND 
			trn_sampleinfomations_details.strComboName =  '$combo' AND 
			trn_sampleinfomations_details.intColorId =  '$color'";	
 			
	$result = $db->RunQuery($sql);
	$arrType=array();
	$arr=array();
	while($row=mysqli_fetch_array($result))
	{
		$arr['combo'] 				= $row['strComboName'];
		$arr['printName'] 			= $row['strPrintName'];
		
		$arr['intColorId'] 			= $row['intColorId'];
		$arr['marketing_itemId'] 	= $row['marketing_itemId'];
		$arr['colorName'] 			= $row['colorName'];
		$arr['typeOfPrintName'] 	= $row['typeOfPrintName'];
		$arr['typeOfPrintId'] 		= $row['typeOfPrintId'];
		$arr['marketing_itemName'] 	= $row['marketing_itemName'];
		$arr['dblQty'] 				= $row['dblQty'];
		
		$arr['size_w'] 					= $row['size_w'];
		$arr['size_h'] 					= $row['size_h'];
	
 	}
	return $arr;
}

function get_inkType_arr($sampleNo,$sampleYear,$print,$combo,$color,$inkType,$revNo){
	
	global $db;
	$sql = "	SELECT
					trn_sampleinfomations_details_technical.intInkTypeId,
					trn_sampleinfomations_details_technical.intNoOfShots,
					trn_sampleinfomations_details_technical.intItem,
					trn_sampleinfomations_details_technical.dblColorWeight,
					mst_inktypes.strName AS techName,
					mst_item.strName AS itemName
				FROM
				trn_sampleinfomations_details_technical
					left Join mst_inktypes ON mst_inktypes.intId = trn_sampleinfomations_details_technical.intInkTypeId
					left Join mst_item ON mst_item.intId = trn_sampleinfomations_details_technical.intItem
				WHERE
					trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
					trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
					trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND
					trn_sampleinfomations_details_technical.strPrintName 	=  '".$print."' AND
					trn_sampleinfomations_details_technical.strComboName 	=  '".$combo."' AND
					trn_sampleinfomations_details_technical.intColorId 		=  '".$color."' AND
					trn_sampleinfomations_details_technical.intInkTypeId 		=  '".$inkType."'
				";
 			
	$result = $db->RunQuery($sql);
	$arrType=array();
	$arr=array();
	while($row=mysqli_fetch_array($result))
	{
		$arr['inkType'] 		= $row['intInkTypeId'];
		$arr['shots'] 			= $row['intNoOfShots'];
		
		$arr['item'] 			= $row['intItem'];
		$arr['weight'] 			= $row['dblColorWeight'];
 	
 	}
	return $arr;
}

function get_diff_color($header_arr,$header_diff_arr){
	
	foreach($header_arr as $k => $v) {
 			$color_arr[$k]='#FFFFFF';
  	}
	foreach($header_diff_arr as $k => $v) {
 			$color_arr[$k]='#FFC1C1';
  	}
	
	return $color_arr;
	
}

function get_diff_color2($header_arr,$header_diff_arr){
	
	foreach($header_arr as $k => $v) {
 			$color_arr[$k]='#F0F0F0';
  	}
	foreach($header_diff_arr as $k => $v) {
 			$color_arr[$k]='#FFC1C1';
  	}
	
	return $color_arr;
	
}
  function getTechniqueItems($sampleNo,$sampleYear,$revNo,$combo,$print,$color){
	  
	  $sql = "SELECT DISTINCT
			trn_sampleinfomations_details.intItem,
			trn_sampleinfomations_details.dblQty,
			trn_sampleinfomations_details.size_w,
			trn_sampleinfomations_details.size_h 
			FROM trn_sampleinfomations_details
			WHERE
			trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
			trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
			trn_sampleinfomations_details.intRevNo 		=  '$revNo' AND
			trn_sampleinfomations_details.strComboName 		=  '$combo' AND
			trn_sampleinfomations_details.strPrintName 		=  '$print' AND
			trn_sampleinfomations_details.intColorId 		=  '$color'  
			";
	return $sql;  
  }

	function getUps($combo,$print){
		
		global $db;
		global $sampleNo;
		global $sampleYear;
		global $revNo;

		$sql		="SELECT
					trn_sampleinfomations_combo_print_details.NO_OF_UPS
					FROM `trn_sampleinfomations_combo_print_details`
					WHERE
					trn_sampleinfomations_combo_print_details.SAMPLE_NO = '$sampleNo' AND
					trn_sampleinfomations_combo_print_details.SAMPLE_YEAR = '$sampleYear' AND
					trn_sampleinfomations_combo_print_details.REVISION = '$revNo' AND
					trn_sampleinfomations_combo_print_details.COMBO = '$combo' AND
					trn_sampleinfomations_combo_print_details.PRINT = '$print'
					";	  
		$result 	= $db->RunQuery($sql);
		$row		=mysqli_fetch_array($result);
		$ups 	= $row['NO_OF_UPS'];
		if($ups=='')
			$ups	=0;
		return $ups;
  }
  ?>

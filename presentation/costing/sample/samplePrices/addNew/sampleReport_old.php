<?php
session_start();
$backwardseperator 		= "../../../../../";
$companyId 				= $_SESSION['CompanyID'];
$locationId 			= $_SESSION['CompanyID'];
$userId 				= $_SESSION['userId'];

$mainPath 				= $_SESSION['mainPath'];
$thisFilePath 			=  $_SERVER['PHP_SELF'];

include  	"{$backwardseperator}dataAccess/permisionCheck.inc";

$no = $_REQUEST['no'];
$year = $_REQUEST['year'];
$revNo = $_REQUEST['revNo'];

$sampleNo = $no;
$sampleYear = $year;

		$sql = "SELECT
					sampleinfo.intSampleNo,
					sampleinfo.intSampleYear,
					sampleinfo.intRevisionNo,
					sampleinfo.dtDate,
					
					sampleinfo.strGraphicRefNo,
					sampleinfo.intCustomer,
					sampleinfo.strStyleNo,
					sampleinfo.intBrand,
					sampleinfo.verivide_d65,
					sampleinfo.verivide_tl84,
					sampleinfo.verivide_cw,
					sampleinfo.verivide_f,
					sampleinfo.verivide_uv,
					sampleinfo.macbeth_dl,
					sampleinfo.macbeth_cw,
					sampleinfo.macbeth_inca,
					sampleinfo.macbeth_tl84,
					sampleinfo.macbeth_uv,
					sampleinfo.macbeth_horizon,
					sampleinfo.dblCuringCondition_temp,
					sampleinfo.dblCuringCondition_beltSpeed,
					sampleinfo.dblPressCondition_temp,
					sampleinfo.dblPressCondition_pressure,
					sampleinfo.dblPressCondition_time,
					sampleinfo.strMeshCount,
					sampleinfo.strAdditionalInstructions,
					sampleinfo.strAdditionalInstructionsTech,
					sampleinfo.intMarketingApproveLevelStart,
					sampleinfo.intTechnicalApproveLevelStart,
					sampleinfo.intCompanyId as locationId,
					sampleinfo.intStatus,
					sampleinfo.intMarketingStatus,
					sampleinfo.intCreator,
					sampleinfo.dtmCreateDate,
					sampleinfo.intTechUser,
					sampleinfo.dtmTechEnterDate,
					sampleinfo.intMarketingUser,
					sampleinfo.dtmMerketingDate,
					sampleinfo.intModifyer,
					sampleinfo.dtmModifyDate,
					cus.strName AS customerName,
					brand.strName AS brandName,
					firstUser.strUserName AS firstUser,
					seconduser.strUserName AS secondUser
				FROM
					trn_sampleinfomations AS sampleinfo
					left Join mst_customer AS cus ON cus.intId = sampleinfo.intCustomer
					left Join mst_brand AS brand ON brand.intId = sampleinfo.intBrand
					Left Join sys_users AS firstUser ON sampleinfo.intCreator = firstUser.intUserId
					Left Join sys_users AS seconduser ON seconduser.intUserId = sampleinfo.intTechUser
				where 
					sampleinfo.intSampleNO='$sampleNo' and sampleinfo.intSampleYear='$sampleYear' and sampleinfo.intRevisionNo='$revNo'
				";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$locationId = $row['locationId'];
					$sampleNo = $row['intSampleNo'];
					
					$mainStatus		 = $row['intStatus'];
					$marketingStatus = $row['intMarketingStatus'];
					
					$marketingApproveLevelStart = $row['intMarketingApproveLevelStart'];
					$technicalApproveLevelStart = $row['intTechnicalApproveLevelStart'];
					
					$sampleYear = $row['intSampleYear'];
					$sampleRevision = $row['intRevisionNo'];
					$sampleDate = $row['dtDate'];
					$deliveryDate = $row['dtDeliveryDate'];
					$graficRef = $row['strGraphicRefNo'];
					$customer = $row['customerName'];
					$style = $row['strStyleNo'];
					$brand = $row['brandName'];
					$sampleQty = $row['dblSampleQty'];
					$grade = $row['intGrade'];
					$fabricType = $row['strFabricType'];
					$curingTemp = $row['dblCuringCondition_temp'];
					$curingSpeed = $row['dblCuringCondition_beltSpeed'];
					$pressTemp = $row['dblPressCondition_temp'];
					$pressPressure = $row['dblPressCondition_pressure'];
					$pressTime = $row['dblPressCondition_time'];
					$meshCount = $row['strMeshCount'];
					$instructions = $row['strAdditionalInstructions'];
					$instructionsTech = $row['strAdditionalInstructionsTech'];
					$verivide_d65 = $row['verivide_d65'];
					$verivide_tl84 = $row['verivide_tl84'];
					$verivide_cw = $row['verivide_cw'];
					$verivide_f = $row['verivide_f'];
					$verivide_uv = $row['verivide_uv'];
					$macbeth_dl = $row['macbeth_dl'];
					$macbeth_cw = $row['macbeth_cw'];
					$macbeth_inca = $row['macbeth_inca'];
					$macbeth_tl84 = $row['macbeth_tl84'];
					$macbeth_uv = $row['macbeth_uv'];
					$macbeth_horizon = $row['macbeth_horizon'];
					
					$firstUser 			= $row['firstUser'];
					$firstDate 			= $row['dtmCreateDate'];
					
					$secondUser 		= $row['secondUser'];
					$secondDate 		= $row['dtmTechEnterDate'];
				 }
				 
					 

						
/*$sql = "SELECT dtDate,dtDeliveryDate,strGraphicRefNo,intCustomer,strStyleNo,intBrand,dblSampleQty,dblDeliverQty,intGrade,strFabricType,
			verivide_d65,verivide_tl84,verivide_cw,verivide_f,verivide_uv,macbeth_dl,macbeth_cw,macbeth_inca,macbeth_tl84,macbeth_uv,macbeth_horizon,
			dblCuringCondition_temp,dblCuringCondition_beltSpeed,dblPressCondition_temp,dblPressCondition_pressure,dblPressCondition_time,strMeshCount,
			strAdditionalInstructions,intApproveLevelStart,intCompanyId,intStatus FROM trn_sampleinfomations 
		WHERE 
			intSampleNo =  '$sampleNo' AND
			intSampleYear =  '$sampleYear' AND
			intRevisionNo =  '$revNo'";

$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$enterDate 				= $row['dtDate'];
	$deliveryDate 			= $row['dtDeliveryDate'];
	$graphicNo 				= $row['strGraphicRefNo'];
	$styleNo 				= $row['strStyleNo'];
	$customerId				= $row['intCustomer'];
	$brandId 				= $row['intBrand'];
	
	//$sa = $row['dblSampleQty'];
	//$no = $row['dblDeliverQty'];
	$grade 					= $row['intGrade'];
	$fabricTypeId 			= $row['strFabricType'];
	
	$verivide_d65 			= $row['verivide_d65'];
	$verivide_tl84 			= $row['verivide_tl84'];
	$verivide_cw 			= $row['verivide_cw'];
	$verivide_f 			= $row['verivide_f'];
	$verivide_uv 			= $row['verivide_uv'];

	$macbeth_dl 			= $row['macbeth_dl'];
	$macbeth_cw 			= $row['macbeth_cw'];
	$macbeth_inca 			= $row['macbeth_inca'];
	$macbeth_tl84 			= $row['macbeth_tl84'];
	$macbeth_uv 			= $row['macbeth_uv'];
	$macbeth_horizon 		= $row['macbeth_horizon'];

	$curing_temp			= $row['dblCuringCondition_temp'];
	$curing_beltspeed		= $row['dblCuringCondition_beltSpeed'];
	$press_temp				= $row['dblPressCondition_temp'];
	$press_pressure			= $row['dblPressCondition_pressure'];
	$press_time				= $row['dblPressCondition_time'];
	$strMeshCount			= $row['strMeshCount'];
	//strMeshCount
	$instructions			= $row['strAdditionalInstructions'];
	$approveLevel			= $row['intApproveLevelStart'];
	$intCompId 				= $row['intCompanyId'];
	$intStatus 				= $row['intStatus'];

}*/

$sql = "SELECT
			trn_sampleinfomations_printsize.strPrintName,
			trn_sampleinfomations_printsize.intWidth,
			trn_sampleinfomations_printsize.intHeight,
			trn_sampleinfomations_printsize.intPart,
			mst_part.strName as partName
		FROM `trn_sampleinfomations_printsize`
		Inner Join mst_part ON trn_sampleinfomations_printsize.intPart = mst_part.intId
		WHERE
			trn_sampleinfomations_printsize.intSampleNo 	=  '$sampleNo' AND
			trn_sampleinfomations_printsize.intSampleYear 	=  '$sampleYear' AND
			trn_sampleinfomations_printsize.intRevisionNo 	=  '$revNo'
		ORDER BY
			trn_sampleinfomations_printsize.strPrintName ASC
		";
$result = $db->RunQuery($sql);
$arrPart;
while($row=mysqli_fetch_array($result))
{
	$arr['printName'] = $row['strPrintName'];
	$arr['width'] = $row['intWidth'];
	$arr['height'] = $row['intHeight'];
	$arr['part'] = $row['intPart'];
	$arr['partName'] = $row['partName'];
	$arrPart[] = $arr;
}

$sql = "SELECT DISTINCT
		trn_sampleinfomations_details.strComboName,
		trn_sampleinfomations_details.intPrintMode,
		trn_sampleinfomations_details.intWashStanderd,
		trn_sampleinfomations_details.intGroundColor,
		trn_sampleinfomations_details.intFabricType
		FROM trn_sampleinfomations_details
		WHERE
		trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
		trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
		trn_sampleinfomations_details.intRevNo 		=  '$revNo'
		";
$result = $db->RunQuery($sql);
$arrCombo;
while($row=mysqli_fetch_array($result))
{
	$arr['combo'] 		= $row['strComboName'];
	$arr['printMode'] 	= $row['intPrintMode'];
	$arr['groundColor'] = $row['intGroundColor'];
	$arr['wash'] 		= $row['intWashStanderd'];
	$arr['fabricType'] 	= $row['intFabricType'];
	$arrCombo[] 		= $arr;
}
//print_r($arrCombo);
////////////// type of print //////////////
 $sql = "SELECT
		trn_sampleinfomations_details.strPrintName,
		trn_sampleinfomations_details.strComboName,
		trn_sampleinfomations_details.intColorId,
		trn_sampleinfomations_details.intItem AS marketing_itemId,
		mst_colors.strName AS colorName,
		mst_typeofprint.strName AS typeOfPrintName,
		trn_sampleinfomations_details.intTypeOfPrint AS typeOfPrintId,
		marketingItem.strName AS marketing_itemName,
		trn_sampleinfomations_details.dblQty,
		trn_sampleinfomations_details.size_w,
		trn_sampleinfomations_details.size_h
		FROM
		trn_sampleinfomations_details
		Inner Join mst_colors ON mst_colors.intId = trn_sampleinfomations_details.intColorId
		Left Join mst_typeofprint ON mst_typeofprint.intId = trn_sampleinfomations_details.intTypeOfPrint
		Left Join mst_item AS marketingItem ON marketingItem.intId = trn_sampleinfomations_details.intItem
		
		WHERE
		trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
		trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
		trn_sampleinfomations_details.intRevNo =  '$revNo'
		";
$result = $db->RunQuery($sql);
$arrType=array();
$arr=array();
while($row=mysqli_fetch_array($result))
{
	$arr['combo'] 				= $row['strComboName'];
	$arr['printName'] 			= $row['strPrintName'];
	
	$arr['intColorId'] 			= $row['intColorId'];
	$arr['marketing_itemId'] 	= $row['marketing_itemId'];
	$arr['colorName'] 			= $row['colorName'];
	$arr['typeOfPrintName'] 	= $row['typeOfPrintName'];
	$arr['typeOfPrintId'] 		= $row['typeOfPrintId'];
	$arr['marketing_itemName'] 	= $row['marketing_itemName'];
	$arr['dblQty'] 				= $row['dblQty'];
	
	$arr['size_w'] 					= $row['size_w'];
	$arr['size_h'] 					= $row['size_h'];

	$arrType[] 			= $arr;
}

$sql = "SELECT DISTINCT
		trn_sampleinfomations_combos.strComboName,
		trn_sampleinfomations_combos.strPrintName,
		trn_sampleinfomations_combos.intTypeOfPrintId,
		trn_sampleinfomations_combos.intTechniqueId,
		mst_technique.strName as techName,
		trn_sampleinfomations_combos.intQty,
		trn_sampleinfomations_combos.intItemId,
		mst_item.strName as itemName,
		mst_technique.intType as spRM
		FROM
		trn_sampleinfomations_combos
		left Join mst_technique ON mst_technique.intId = trn_sampleinfomations_combos.intTechniqueId
		left Join mst_item ON mst_item.intId = trn_sampleinfomations_combos.intItemId
		WHERE
		trn_sampleinfomations_combos.intSampleNo =  '$sampleNo' AND
		trn_sampleinfomations_combos.intSampleYear =  '$sampleYear' AND
		trn_sampleinfomations_combos.intRevisionNo =  '$revNo'
		";
$result = $db->RunQuery($sql);
$arrTech;
while($row=mysqli_fetch_array($result))
{
	$arr['combo'] 		= $row['strComboName'];
	$arr['printName'] 	= $row['strPrintName'];
	$arr['typeId'] 		= $row['intTypeOfPrintId'];
	$arr['techId'] 		= $row['intTechniqueId'];
	$arr['techName'] 	= $row['techName'];
	$arr['qty'] 		= $row['intQty'];
	$arr['itemId'] 		= $row['intItemId'];
	$arr['itemName'] 	= $row['itemName'];
	$arr['spRM'] 		= $row['spRM'];
	
	$arrTech[] 			= $arr;
}


$sql = "SELECT
		trn_sampleinfomations_colors.strComboName,
		trn_sampleinfomations_colors.strPrintName,
		trn_sampleinfomations_colors.intColor,
		mst_colors.strName as colorName
		FROM `trn_sampleinfomations_colors`
		Inner Join mst_colors ON mst_colors.intId = trn_sampleinfomations_colors.intColor
		WHERE
		trn_sampleinfomations_colors.intSampleNo =  '$sampleNo' AND
		trn_sampleinfomations_colors.intSampleYear =  '$sampleYear' AND
		trn_sampleinfomations_colors.intRevisionNo =  '$revNo'
		";
$result = $db->RunQuery($sql);
$arrColor;
while($row=mysqli_fetch_array($result))
{
	$arr['combo'] 		= $row['strComboName'];
	$arr['printName'] 	= $row['strPrintName'];
	$arr['colorId'] 		= $row['intColor'];
	$arr['colorName'] 		= $row['colorName'];
	$arrColor[] 			= $arr;
}

$sql = "SELECT
			Max(trn_sampleinfomations.intRevisionNo) as maxRevNo
		FROM trn_sampleinfomations
		WHERE
			trn_sampleinfomations.intSampleNo =  '$sampleNo' AND
			trn_sampleinfomations.intSampleYear =  '$sampleYear'
		";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$maxRevNo = $row['maxRevNo'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sample Infomations Report</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src=".../../../../../libraries/jquery/jquery-ui.js"></script>

<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>
<script type="application/javascript" src="sampleReport-js.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:380px;
	top:183px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
.printName {font-family: "Arial Black", Gadget, sans-serif;
	font-size: 16px;
	font-style: normal;
	font-weight: bold;
	color: #009;
	text-align:center
}
.noPrint {    display:none;
}
.statusWithBorder {
	font-family: "Courier New", Courier, monospace;
	border: thin solid #666;
	font-size: 18px;
}
#frmSampleReport div table tr td table tr td {
}
</style>
</head>

<body>
<table width="88%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#D3F5DF" class="tableBorder_allRound">
  <tr>
    <td width="4%">&nbsp;</td>
    <td width="21%">&nbsp;</td>
    <td width="16%">&nbsp;</td>
    <td width="17%">&nbsp;</td>
    <td width="9%">&nbsp;</td>
    <td width="15%">&nbsp;</td>
    <td width="18%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td class="normalfnt">Graphic No</td>
    <td class="normalfnt">Sample Year</td>
    <td class="normalfnt">Sample No</td>
    <td class="normalfnt">Revision No</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="tableBorder_allRound mouseover" align="center" ><a  target="_blank" style="text-decoration:none;width:100%" href="samplePrices.php?cboSampleNo=<?Php echo $sampleNo; ?>&cboYear=<?php echo $sampleYear; ?>&cboRevisionNo=<?PHP echo $revNo; ?>"><span class="printName" >ADD PRICE</span></a></td>
    <td>&nbsp;</td>
    <td><select name="cboGraphicNo" id="cboGraphicNo" style="width:150px">
     <option value=""></option>
                  <?php
					//$d = date('Y');
				    $sql = "SELECT DISTINCT
								trn_sampleinfomations.strGraphicRefNo
							FROM trn_sampleinfomations
							ORDER BY
								trn_sampleinfomations.strGraphicRefNo ASC
					";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$strGraphicRefNo = $row['strGraphicRefNo'];
						if($graficRef==$strGraphicRefNo)
							echo "<option selected=\"selected\" value=\"$strStyle\">$strGraphicRefNo</option>";
						else
							echo "<option value=\"$strGraphicRefNo\">$strGraphicRefNo</option>";
					}
				?>
    </select></td>
    <td><select name="cboYear" id="cboYear" style="width:65px">
     <?php
				    $d = date('Y');
				  	for($d;$d>=2012;$d--)
					{
						if($d==$sampleYear)
							echo "<option selected=\"selected\" id=\"$d\" >$d</option>";	
						else
							echo "<option id=\"$d\" >$d</option>";	
					}
				  ?>
    </select></td>
    <td><select name="cboSampleNo" id="cboSampleNo" style="width:150px">
              <option value=""></option>
                  <?php
					$d = date('Y');
				    $sql = "SELECT DISTINCT
							trn_sampleinfomations.intSampleNo
							FROM trn_sampleinfomations
						WHERE
							trn_sampleinfomations.intSampleYear =  '$d' and intCompanyId = '$locationId'
						ORDER BY
							trn_sampleinfomations.intSampleNo ASC
					";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$no = $row['intSampleNo'];
						if($no==$sampleNo)
							echo "<option selected=\"selected\" value=\"$no\">$no</option>";
						else
							echo "<option value=\"$no\">$no</option>";
					}
				?>
    </select></td>
    <td><select name="cboRevisionNo" id="cboRevisionNo" style="width:50px">
    <option value=""></option>
                  <?php
				  	$sql = "SELECT
					trn_sampleinfomations.intRevisionNo
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations.intSampleNo =  '$sampleNo'
				ORDER BY
					trn_sampleinfomations.intRevisionNo ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($revNo==$row['intRevisionNo'])
						echo "<option selected=\"selected\" value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
					else
						echo "<option value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
				}
				  ?>
    </select></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

<?php
if($mainStatus<>1)
{
	
?>
<div id="apDiv1"><img src="../../../../../images/pending.png"  /></div>
<?php
}
?>
<form id="frmSampleReport" name="frmSampleReport" method="post" action="sampleReport.php">
<div align="center">
<div style="background-color:#FFF" ></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">

  <tr>
     <td colspan="9" ><?php include "../../../../../reportHeader.php"; ?></td>
  </tr>
  <tr>
    <td colspan="9" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="9" align="center"><span class="reportHeader" style="background-color:#FFF"><strong>SAMPLE INFORMATION SHEET</strong></span></td>
  </tr>
      <?php
	  
	  //if($mainStatus==(-1) && $technicalStatus==(-1))
	  		//$marketingApproveLevels = (int)getApproveLevel('Sample Infomations Sheet - 1st Stage');
	  //else if($mainStatus==-1 && $technicalStatus==1)
	  		//$marketingApproveLevels = (int)getApproveLevel('Sample Infomations Sheet - 3rd Stage');
			
	  $x = ($marketingApproveLevelStart+1)-$marketingStatus;
	  $sql = "	SELECT
					menupermision.int{$x}Approval as approval
				FROM menupermision Inner Join menus ON menus.intId = menupermision.intMenuId
				WHERE
					menus.strCode =  'P0022' AND
					menupermision.intUserId =  '$userId'
			";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	$userPermision = $row['approval'];
	
	$viewApproveButton = false;
	if($userPermision && $marketingStatus>1)
	{
		$viewApproveButton = true;	
	}
	?>
      
      <tr>
        <td colspan="9" class="APPROVE"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="normalfnt">
          <tr>
            <td width="12%" bgcolor="<?php echo ($viewApproveButton?'#CC9900':''); ?>" class="normalfnt">1st Stage (Marketing)</td>
            <?php 

			if($viewApproveButton)
			{
				//$stage = 1;
			?>
            <td width="15%" align="center" bgcolor="#CC9900"  class="statusWithBorder" ><b>Pending</b></td><?php
			
			}
			else
			{
				
				if($marketingStatus==1)
					$text = "Approved";
				else 
					$text = "Pending";
			?>
            <td width="12%" align="center" bgcolor="#D9F4FD" class="statusWithBorder" ><?php echo $text; ?></td>
            <?php
					
				
			}
			?>
            <td width="6%">&nbsp;</td>
            <td width="12%">2nd Stage (Technical)</td>
            <?php
				///////////////2nd stage ////////////////
				$text = '';
				if($mainStatus==1)
				{
			?>
          		<td width="17%" align="center" bgcolor="#D9F4FD"  class="statusWithBorder" >Approved</td>
            <?php
				}
				else if($mainStatus>1)
				{
				
			?>
            	<td width="15%" align="center" bgcolor="#CC9900"  class="statusWithBorder" ><b>Pending</b></td>
            <?php
				}
				else
					echo "<td width=\"20%\" align=\"center\"    >&nbsp;</td>";
			?>
            

          
            

          </tr>
        </table></td>
        
      </tr>
      <tr>
        <td colspan="9" class="compulsoryRed"><input style="width:5px;visibility:hidden"  type="text" name="txtApproveStatus" id="txtApproveStatus" /></td>
       
      </tr>
  <tr>
    <td width="3%">&nbsp;</td>
    <td width="14%"><span class="normalfnt"><strong>Sample No</strong></span></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="17%"><span class="printName"><?php echo $sampleYear ?>/<?php echo $sampleNo ?></span></td>
    <td width="15%"><span class="normalfnt"><strong>Revision No</strong></span></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="19%" align="left"><span class="printName"><?php echo $sampleRevision ?></span></td>
    <td width="11%"><span class="normalfnt"><strong>Date</strong></span></td>
    <td width="9%"><span class="normalfnt">: <?php echo $sampleDate ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td colspan="4" style="color:#F00;font-size:8;font-family:'Times New Roman', Times, serif"><?Php 
	if($maxRevNo>$revNo)
		echo "This report is not valid sample, revision no <a href=\"sampleReport.php?no=$sampleNo&year=$sampleYear&revNo=$maxRevNo\">\"".$maxRevNo."\"</a> is the original one.";
	?></td>
    <td width="11%"><span class="normalfnt"><strong>Delivery Date</strong></span></td>
    <td><span class="normalfnt">: <?php echo $deliveryDate ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><span class="normalfnt"><strong>Graphic Ref No#</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $graficRef ?></span></td>
    <td><span class="normalfnt"><strong>Customer</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td colspan="3"><span class="normalfnt"><?php echo $customer ?></span></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td><span class="normalfnt"><strong>Style No</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $style ?></span></td>
    <td><span class="normalfnt"><strong>Brand</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $brand ?></span></td>
    <td width="11%">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td width="11%">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="9">
      <?php 
			$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strComboName
					FROM trn_sampleinfomations_details
					WHERE
					trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_details.intRevNo =  '$revNo'
					";
			$result = $db->RunQuery($sql);
			$rowsCount = mysqli_num_rows($result);
			
			$sql = "SELECT DISTINCT
						strPrintName
					FROM trn_sampleinfomations_printsize
					WHERE
						intSampleNo 	=  '$sampleNo' AND
						intSampleYear 	=  '$sampleYear' AND
						intRevisionNo 	=  '$revNo'
			";
			$result = $db->RunQuery($sql);
			$cellCount = mysqli_num_rows($result);
	?>
      </td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td width="37" class="normalfnt"><table id="tblMain" width="74%" border="0" align="left"  class="grid"  >
          <tr class="">
            <td height="152" bgcolor="#FFFFFF" class="normalfnt"  >&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF"><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:264px;height:148px;overflow:hidden" >
              <?php
			if($sampleNo!='')
			{
				echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"../../../../../documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revNo-1.png\" />";	
			}
			 ?>
              </div></td>
            <?php
				///////////  img cell loop 
				for($i=1;$i<$cellCount;$i++)
				{
					$l=$i+1;
			?>
            <td align="center" bgcolor="#FFFFFF"><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:264px;height:148px;overflow:hidden" >
              <?php

		echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"../../../../../documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revNo-$l.png\" />";	
			
			 ?>
              </div></td>
            <?php	
				}
			?>
            <td bgcolor="#FFFFFF" >&nbsp;</td>
            </tr>
          <tr class="">
            <td height="18" bgcolor="#FFFFFF" class="normalfnt" >&nbsp;</td>
            <td align="center" style="width:300px" bgcolor="#FFFFFF" class="printName" ><span class="printNameSpan">Print 1</span><span  class="normalfntMid">
              <select disabled="disabled"    class="part validate[required]" name="cboPart1" id="cboPart1" style="width:180px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_part where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrPart[0]['part'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
                </select>
              <input disabled="disabled"   name="textfield" type="text" class="sizeW validate[required,custom[number],max[1000],min[0]]" id="sizeW1" style="width:40px" value="<?php echo $arrPart[0]['width']; ?>" />
              <span class="normalfntGrey">W
                <input disabled="disabled"  name="textfield2" type="text" class="sizeH validate[required,custom[number],max[1000],min[0]]" id="sizeH1" style="width:40px" value="<?php echo $arrPart[0]['height']; ?>" />
                H(inch)</span></span></td>
            <?php
			  	///////// print name cell loop
				for($i=1;$i<$cellCount;$i++)
				{
			  ?>
            <td align="center" bgcolor="#FFFFFF" class="printName" ><span class="printNameSpan">Print <?php echo ($i+1); ?></span><span  class="normalfntMid">
              <select disabled="disabled"  class="part validate[required]" name="cboPart2" id="cboPart2" style="width:180px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_part where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					$e=1;
					while($row=mysqli_fetch_array($result))
					{
						
						if($row['intId']==$arrPart[$e]['part'])
							echo "<option selected value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
                </select>
              <input name="textfield" type="text" disabled="disabled" class="sizeW validate[required,custom[number],max[1000],min[0]]" id="sizeW2" style="width:40px" value="<?php echo $arrPart[$i]['height']; ?>" />
              <span class="normalfntGrey">W
                <input name="textfield2" type="text" disabled="disabled" class="sizeH validate[required,custom[number],max[1000],min[0]]" id="sizeH2" style="width:40px" value="<?php echo $arrPart[$i]['height']; ?>" />
                H(inch)</span></span></td>
            <?php
				}
			  ?>
            <td bgcolor="#FFFFFF" >&nbsp;</td>
            </tr>
          <tr class="">
            <td height="10" bgcolor="#CCCCCC" class="gridHeader" ><input disabled="disabled"   align="left" class="validate[required,maxSize[20]]" value="<?php echo $arrCombo[0]['combo']; ?>" style="width:80px" type="text" name="txtCombo1" id="txtCombo1" /></td>
            <td align="center" bgcolor="#FFFFFF" class="printName" >&nbsp;</td>
            <?php
				for($i=1;$i<=$cellCount;$i++)
				{
					//echo "00 / ";
			?>
            <td bgcolor="#FFFFFF" >&nbsp;</td>
            <?php
				}
			?>
            <td bgcolor="#FFFFFF" >&nbsp;</td>
            </tr>
          <tr class="dataRow">
            <td width="18%"  bgcolor="#FFFFFF" ><select disabled="disabled"  name="cboPrintMode2" class="printMode validate[required]" id="cboPrintMode2" style="width:100px">
              <option value=""></option>
              <?php
					$sql = "select intId,strName from mst_printmode where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[0]['printMode'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select>
              <select disabled="disabled"   class="washStanderd validate[required]" name="butWashingStanderd2" id="butWashingStanderd3" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_washstanderd where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[0]['wash'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
					}
				?>
                </select>
              <select disabled="disabled"   class="colors validate[required]" name="cboGroundColor2" id="cboGroundColor2" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_colors_ground where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[0]['groundColor'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
					}
				?>
                </select>
              <select disabled="disabled"   class="colors validate[required]" name="cboFabricType" id="cboFabricType" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_fabrictype where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[0]['fabricType'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
                </select></td>
            <td width="67%"  bgcolor="#FFFFFF" style="padding-top:0px" ><table id="tblGrid2" class="tblGrid2" bgcolor="#006666" cellpadding="0" cellspacing="1" width="650" border="0">
              <tr class="normalfntMid">
                <td width="70" bgcolor="#CEE8FB">Color</td>
                <td width="100" bgcolor="#CEE8FB">Technique</td>
                <td width="100" bgcolor="#CEE8FB">Item</td>
                <td width="40" bgcolor="#CEE8FB">Qty</td>
                <td width="86" bgcolor="#CEE8FB">Size(inch)</td>
                <td bgcolor="#FFE9D2"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                  <tr class="normalfnt">
                    <td  width="40%" bgcolor="#DFFFDF">Ink Type</td>
                    <td width="13%" bgcolor="#DFFFDF">Shots</td>
                    <td width="35%" bgcolor="#DFFFDF">Item</td>
                    <td width="12%" bgcolor="#DFFFDF">Weight</td>
                    </tr>
                  </table></td>
                </tr>
              <?php
				foreach($arrType as $arrT)
				{
					//echo $arrT['combo'];
					//echo $arrCombo[0]['combo'];
					if($arrT['combo']==$arrCombo[0]['combo'] && $arrT['printName']=='print 1')
					{
/*						
							$arr['combo'] 				= $row['strComboName'];
							$arr['printName'] 			= $row['strPrintName'];
							
							$arr['intColorId'] 			= $row['intColorId'];
							$arr['marketing_itemId'] 	= $row['marketing_itemId'];
							$arr['colorName'] 			= $row['colorName'];
							$arr['typeOfPrintName'] 	= $row['typeOfPrintName'];
							$arr['typeOfPrintId'] 		= $row['typeOfPrintId'];
							$arr['marketing_itemName'] 	= $row['marketing_itemName'];
							$arr['dblQty'] 				= $row['dblQty'];
							
							$arr['size_w'] 					= $row['size_w'];
							$arr['size_h'] 					= $row['size_h'];
							$arr['technique_techId'] 		= $row['technique_techId'];
							$arr['technique_techName'] 		= $row['technique_techName'];
							$arr['technique_intNoOfShots'] 	= $row['technique_intNoOfShots'];
							$arr['technique_itemId'] 		= $row['technique_itemId'];
							$arr['technique_itemName'] 		= $row['technique_itemName'];
							$arr['technique_dblColorWeight']= $row['technique_dblColorWeight'];*/
				?>
              <tr>
                <td bgcolor="#ffffffff" class="normalfnt" id="<?php echo $arrT['intColorId'] ?>"><?php echo $arrT['colorName'] ?></td>
                <td bgcolor="#ffffffff" class="normalfnt" ><?php
			  	$sql 	= "SELECT mst_typeofprint.strName FROM mst_typeofprint 
						WHERE mst_typeofprint.intId = '".$arrT['typeOfPrintId']."'";
				$result = 	$db->RunQuery($sql);
				$row	=	mysqli_fetch_array($result);
				echo $row['strName'];
			  ?></td>
                <td bgcolor="#ffffffff" class="normalfnt"><?php
			  	$sql = "SELECT
							mst_item.intId,
							mst_item.strName
						FROM `mst_item`
						WHERE
							mst_item.intId =  '".$arrT['marketing_itemId']."'";
				$result = 	$db->RunQuery($sql);
				$row	=	mysqli_fetch_array($result);
				echo $row['strName'];	
			  ?></td>
                <td bgcolor="#ffffffff"><input disabled="disabled"    style="width:40px" value="<?PHP echo ($arrT['dblQty']<=0?'':$arrT['dblQty']); ?>"  type="text" name="textfield5" id="txtQty" /></td>
                <td bgcolor="#ffffffff" class="normalfnt"><input disabled="disabled"   value="<?PHP echo ($arrT['size_w']<=0?'':$arrT['size_w']); ?>" style="width:20px"  type="text" name="textfield3" id="txtSizeW" />
                  W
                  <input disabled="disabled"   style="width:20px" value="<?PHP echo ($arrT['size_h']<=0?'':$arrT['size_h']); ?>"  type="text" name="textfield4" id="txtSizeH" />
                  H</td>
                <td bgcolor="#ffffffff"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                  <?php 
					$sql = "	SELECT
									trn_sampleinfomations_details_technical.intTechnique,
									trn_sampleinfomations_details_technical.intNoOfShots,
									trn_sampleinfomations_details_technical.intItem,
									trn_sampleinfomations_details_technical.dblColorWeight,
									mst_technique.strName AS techName,
									mst_item.strName AS itemName
								FROM
								trn_sampleinfomations_details_technical
									left Join mst_technique ON mst_technique.intId = trn_sampleinfomations_details_technical.intTechnique
									left Join mst_item ON mst_item.intId = trn_sampleinfomations_details_technical.intItem
								WHERE
									trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
									trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
									trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND
									trn_sampleinfomations_details_technical.strPrintName 	=  '".$arrT['printName']."' AND
									trn_sampleinfomations_details_technical.strComboName 	=  '".$arrT['combo']."' AND
									trn_sampleinfomations_details_technical.intColorId 		=  '".$arrT['intColorId']."'
								";
						$result_tech = $db->RunQuery($sql);
						while($row_tech=mysqli_fetch_array($result_tech))
						{
					?>
                  <tr class="normalfnt">
                    <td width="40%" bgcolor="#FFFFFF"><?php echo $row_tech['techName']; ?></td>
                    <td width="13%" bgcolor="#FFFFFF"><?php echo $row_tech['intNoOfShots']; ?></td>
                    <td width="35%" bgcolor="#FFFFFF"><?php echo $row_tech['itemName']; ?></td>
                    <td width="12%" bgcolor="#FFFFFF"><?php echo $row_tech['dblColorWeight']; ?></td>
                    </tr>
                  <?php
						}
					?>
                  </table></td>
                </tr>
              <?php
					}
				}
				?>
              </table></td>
            <?php
					/////////// first row table cells //////////////
					for($i=1;$i<$cellCount;$i++)
					{
				?>
            <td width="67%"  bgcolor="#FFFFFF" style="padding-top:0px" ><table id="tblGrid2" class="tblGrid2" bgcolor="#006666" cellpadding="0" cellspacing="1" width="650" border="0">
              <tr class="normalfntMid">
                <td width="70" bgcolor="#CEE8FB">Color</td>
                <td width="100" bgcolor="#CEE8FB">Technique</td>
                <td width="100" bgcolor="#CEE8FB">Item</td>
                <td width="40" bgcolor="#CEE8FB">Qty</td>
                <td width="86" bgcolor="#CEE8FB">Size(inch)</td>
                <td bgcolor="#FFE9D2"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                  <tr class="normalfnt">
                    <td  width="40%" bgcolor="#DFFFDF">Ink Type</td>
                    <td width="13%" bgcolor="#DFFFDF">Shots</td>
                    <td width="35%" bgcolor="#DFFFDF">Item</td>
                    <td width="12%" bgcolor="#DFFFDF">Weight</td>
                    </tr>
                  </table></td>
                </tr>
              <?php
				  
				foreach($arrType as $arrT)
				{
					
					//echo $arrT['combo'];
					//echo $arrCombo[0]['combo'];
					//echo  $arrT['printName'];
					//echo 'print '.($i+1).'<br>';
					if($arrT['combo']==$arrCombo[0]['combo'] && $arrT['printName']=='print '.($i+1))
					{
						
/*						
							$arr['combo'] 				= $row['strComboName'];
							$arr['printName'] 			= $row['strPrintName'];
							
							$arr['intColorId'] 			= $row['intColorId'];
							$arr['marketing_itemId'] 	= $row['marketing_itemId'];
							$arr['colorName'] 			= $row['colorName'];
							$arr['typeOfPrintName'] 	= $row['typeOfPrintName'];
							$arr['typeOfPrintId'] 		= $row['typeOfPrintId'];
							$arr['marketing_itemName'] 	= $row['marketing_itemName'];
							$arr['dblQty'] 				= $row['dblQty'];
							
							$arr['size_w'] 					= $row['size_w'];
							$arr['size_h'] 					= $row['size_h'];
							$arr['technique_techId'] 		= $row['technique_techId'];
							$arr['technique_techName'] 		= $row['technique_techName'];
							$arr['technique_intNoOfShots'] 	= $row['technique_intNoOfShots'];
							$arr['technique_itemId'] 		= $row['technique_itemId'];
							$arr['technique_itemName'] 		= $row['technique_itemName'];
							$arr['technique_dblColorWeight']= $row['technique_dblColorWeight'];*/
				?>
              <tr>
                <td bgcolor="#ffffffff" class="normalfnt" id="<?php echo $arrT['intColorId'] ?>"><?php echo $arrT['colorName'] ?></td>
                <td bgcolor="#ffffffff" class="normalfnt" ><?php
			  	$sql 	= "SELECT mst_typeofprint.strName FROM mst_typeofprint 
						WHERE mst_typeofprint.intId = '".$arrT['typeOfPrintId']."'";
				$result = 	$db->RunQuery($sql);
				$row	=	mysqli_fetch_array($result);
				echo $row['strName'];
			  ?></td>
                <td bgcolor="#ffffffff" class="normalfnt"><?php
			  	$sql = "SELECT
							mst_item.intId,
							mst_item.strName
						FROM `mst_item`
						WHERE
							mst_item.intId =  '".$arrT['marketing_itemId']."'";
				$result = 	$db->RunQuery($sql);
				$row	=	mysqli_fetch_array($result);
				echo $row['strName'];	
			  ?></td>
                <td bgcolor="#ffffffff"><input disabled="disabled"   style="width:40px" value="<?PHP echo ($arrT['dblQty']<=0?'':$arrT['dblQty']); ?>"  type="text" name="txtQty" id="txtQty" /></td>
                <td bgcolor="#ffffffff" class="normalfnt"><input disabled="disabled"   value="<?PHP echo ($arrT['size_w']<=0?'':$arrT['size_w']); ?>" style="width:20px"  type="text" name="txtSizeW" id="txtSizeW" />
                  W
                  <input disabled="disabled" style="width:20px" value="<?PHP echo ($arrT['size_h']<=0?'':$arrT['size_h']); ?>"  type="text" name="txtSizeH" id="txtSizeH" />
                  H</td>
                <td bgcolor="#ffffffff"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                  <?php 
					$sql = "	SELECT
									trn_sampleinfomations_details_technical.intInkTypeId,
									trn_sampleinfomations_details_technical.intNoOfShots,
									trn_sampleinfomations_details_technical.intItem,
									trn_sampleinfomations_details_technical.dblColorWeight,
									mst_technique.strName AS techName,
									mst_item.strName AS itemName
								FROM
								trn_sampleinfomations_details_technical
									left Join mst_technique ON mst_technique.intId = trn_sampleinfomations_details_technical.intInkTypeId
									left Join mst_item ON mst_item.intId = trn_sampleinfomations_details_technical.intItem
								WHERE
									trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
									trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
									trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND
									trn_sampleinfomations_details_technical.strPrintName 	=  '".$arrT['printName']."' AND
									trn_sampleinfomations_details_technical.strComboName 	=  '".$arrT['combo']."' AND
									trn_sampleinfomations_details_technical.intColorId 		=  '".$arrT['intColorId']."'
								";
						$result_tech = $db->RunQuery($sql);
						while($row_tech=mysqli_fetch_array($result_tech))
						{
					?>
                  <tr class="normalfnt">
                    <td width="40%" bgcolor="#FFFFFF"><?php echo $row_tech['techName']; ?></td>
                    <td width="13%" bgcolor="#FFFFFF"><?php echo $row_tech['intNoOfShots']; ?></td>
                    <td width="35%" bgcolor="#FFFFFF"><?php echo $row_tech['itemName']; ?></td>
                    <td width="12%" bgcolor="#FFFFFF"><?php echo $row_tech['dblColorWeight']; ?></td>
                    </tr>
                  <?php
						}
					?>
                  </table></td>
                </tr>
              <?php
					}
				}
				?>
              </table></td>
            <?php
					}
				?>
            <td width="15%"  bgcolor="#FFFFFF" style="padding-top:0px" >&nbsp;</td>
            </tr>
          <?php
		  	for($mainRow=1;$mainRow<$rowsCount;$mainRow++)
			{
		  ?>
          <tr class="">
            <td height="18" bgcolor="#CCCCCC" class="gridHeader" ><input disabled="disabled"   class="validate[required,maxSize[20]]" value="<?php echo $arrCombo[$mainRow]['combo']; ?>" style="width:80px" type="text" name="txtCombo2" id="txtCombo2" /></td>
            <?php
                 for($i=1;$i<=$cellCount;$i++)
                 {
                ?>
            <td align="center" bgcolor="#FFFFFF" class="printName" >&nbsp;</td>
            <?php
                }
                ?>
            <td bgcolor="#FFFFFF" >&nbsp;</td>
            </tr>
          <tr class="dataRow">
            <td width="18%"  bgcolor="#FFFFFF" ><select disabled="disabled"   class="printMode validate[required]" name="cboPrintMode2" id="cboPrintMode2" style="width:100px">
              <option value=""></option>
              <?php
					$sql = "select intId,strName from mst_printmode where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[$mainRow]['printMode'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select>
              <select disabled="disabled"   class="washStanderd validate[required]" name="butWashingStanderd2" id="butWashingStanderd3" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_washstanderd where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[$mainRow]['wash'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
                </select>
              <select disabled="disabled"  class="colors validate[required]" name="cboGroundColor2" id="cboGroundColor2" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_colors_ground where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[$mainRow]['groundColor'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";		
					}
				?>
                </select>
              <select disabled="disabled"  class="colors validate[required]" name="cboFabricType" id="cboFabricType" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_fabrictype where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[$mainRow]['fabricType'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
                </select></td>
            <?php 
			  	/////////// color grid and technique and type of grid cell ////////////
				for($i=1;$i<=$cellCount;$i++)
                 {
			  ?>
            <td  width="67%"  bgcolor="#FFFFFF" style="padding-top:0px" ><table id="tblGrid2" class="tblGrid2" bgcolor="#006666" cellpadding="0" cellspacing="1" width="650" border="0">
              <tr class="normalfntMid">
                <td width="70" bgcolor="#CEE8FB">Color</td>
                <td width="100" bgcolor="#CEE8FB">Technique</td>
                <td width="100" bgcolor="#CEE8FB">Item</td>
                <td width="40" bgcolor="#CEE8FB">Qty</td>
                <td width="86" bgcolor="#CEE8FB">Size(inch)</td>
                <td bgcolor="#FFE9D2"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                  <tr class="normalfnt">
                    <td  width="40%" bgcolor="#DFFFDF">Ink Type</td>
                    <td width="13%" bgcolor="#DFFFDF">Shots</td>
                    <td width="35%" bgcolor="#DFFFDF">Item</td>
                    <td width="12%" bgcolor="#DFFFDF">Weight</td>
                    </tr>
                  </table></td>
                </tr>
              <?php
			// print_r($arrCombo[0]['combo']);
				foreach($arrType as $arrT)
				{
					//echo $arrT['combo'];
					//echo $arrCombo[$mainRow]['combo'].'<br>';
					if($arrT['combo']==$arrCombo[$mainRow]['combo'] && $arrT['printName']=='print '.$i)
					{
/*						
							$arr['combo'] 				= $row['strComboName'];
							$arr['printName'] 			= $row['strPrintName'];
							
							$arr['intColorId'] 			= $row['intColorId'];
							$arr['marketing_itemId'] 	= $row['marketing_itemId'];
							$arr['colorName'] 			= $row['colorName'];
							$arr['typeOfPrintName'] 	= $row['typeOfPrintName'];
							$arr['typeOfPrintId'] 		= $row['typeOfPrintId'];
							$arr['marketing_itemName'] 	= $row['marketing_itemName'];
							$arr['dblQty'] 				= $row['dblQty'];
							
							$arr['size_w'] 					= $row['size_w'];
							$arr['size_h'] 					= $row['size_h'];
							$arr['technique_techId'] 		= $row['technique_techId'];
							$arr['technique_techName'] 		= $row['technique_techName'];
							$arr['technique_intNoOfShots'] 	= $row['technique_intNoOfShots'];
							$arr['technique_itemId'] 		= $row['technique_itemId'];
							$arr['technique_itemName'] 		= $row['technique_itemName'];
							$arr['technique_dblColorWeight']= $row['technique_dblColorWeight'];*/
				?>
              <tr>
                <td bgcolor="#ffffffff" class="normalfnt" id="<?php echo $arrT['intColorId'] ?>"><?php echo $arrT['colorName'] ?></td>
                <td bgcolor="#ffffffff" class="normalfnt" ><?php
			  	$sql 	= "SELECT mst_typeofprint.strName FROM mst_typeofprint 
						WHERE mst_typeofprint.intId = '".$arrT['typeOfPrintId']."'";
				$result = 	$db->RunQuery($sql);
				$row	=	mysqli_fetch_array($result);
				echo $row['strName'];
			  ?></td>
                <td bgcolor="#ffffffff" class="normalfnt"><?php
			  	$sql = "SELECT
							mst_item.intId,
							mst_item.strName
						FROM `mst_item`
						WHERE
							mst_item.intId =  '".$arrT['marketing_itemId']."'";
				$result = 	$db->RunQuery($sql);
				$row	=	mysqli_fetch_array($result);
				echo $row['strName'];	
			  ?></td>
                <td bgcolor="#ffffffff"><input disabled="disabled"  style="width:40px" value="<?PHP echo ($arrT['dblQty']<=0?'':$arrT['dblQty']); ?>"  type="text" name="textfield7" id="txtQty" /></td>
                <td bgcolor="#ffffffff" class="normalfnt"><input disabled="disabled"   value="<?PHP echo ($arrT['size_w']<=0?'':$arrT['size_w']); ?>" style="width:20px"  type="text" name="textfield7" id="txtSizeW" />
                  W
                  <input disabled="disabled"   style="width:20px" value="<?PHP echo ($arrT['size_h']<=0?'':$arrT['size_h']); ?>"  type="text" name="textfield7" id="txtSizeH" />
                  H</td>
                <td bgcolor="#ffffffff"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                  <?php 
					$sql = "	SELECT
									trn_sampleinfomations_details_technical.intTechnique,
									trn_sampleinfomations_details_technical.intNoOfShots,
									trn_sampleinfomations_details_technical.intItem,
									trn_sampleinfomations_details_technical.dblColorWeight,
									mst_technique.strName AS techName,
									mst_item.strName AS itemName
								FROM
								trn_sampleinfomations_details_technical
									left Join mst_technique ON mst_technique.intId = trn_sampleinfomations_details_technical.intTechnique
									left Join mst_item ON mst_item.intId = trn_sampleinfomations_details_technical.intItem
								WHERE
									trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
									trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
									trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND
									trn_sampleinfomations_details_technical.strPrintName 	=  '".$arrT['printName']."' AND
									trn_sampleinfomations_details_technical.strComboName 	=  '".$arrT['combo']."' AND
									trn_sampleinfomations_details_technical.intColorId 		=  '".$arrT['intColorId']."'
								";
						$result_tech = $db->RunQuery($sql);
						while($row_tech=mysqli_fetch_array($result_tech))
						{
					?>
                  <tr class="normalfnt">
                    <td width="40%" bgcolor="#FFFFFF"><?php echo $row_tech['techName']; ?></td>
                    <td width="13%" bgcolor="#FFFFFF"><?php echo $row_tech['intNoOfShots']; ?></td>
                    <td width="35%" bgcolor="#FFFFFF"><?php echo $row_tech['itemName']; ?></td>
                    <td width="12%" bgcolor="#FFFFFF"><?php echo $row_tech['dblColorWeight']; ?></td>
                    </tr>
                  <?php
						}
					?>
                  </table></td>
                </tr>
              <?php
					}
				}
				?>
              </table></td>
            <?php
				 }
			?>
            <td bgcolor="#FFFFFF" >&nbsp;</td>
            </tr>
          <?php
			}
		  ?>
          <tr class="dataRow">
            <td  bgcolor="#FFFFFF" >&nbsp;</td>
            <td  bgcolor="#FFFFFF" style="padding-top:0px" >&nbsp;</td>
            <?php
				for($i=1;$i<=$cellCount;$i++)
				{
			?>
            <td  bgcolor="#FFFFFF" style="padding-top:0px" >&nbsp;</td>
            <?php
				}
			?>
            <td  bgcolor="#FFFFFF" style="padding-top:0px" >&nbsp;</td>
            </tr>
          </table></td>
        <td width="6%">&nbsp;</td>
        </tr>
      <tr>
        <td colspan="3"><table width="800" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="21%" class="normalfnt"><strong>Light Box</strong></td>
            <td width="79%" rowspan="2" align="center" valign="middle"><table width="96%" class="grid" id="tblMainGrid2" cellspacing="0" cellpadding="0">
              <tr class="gridHeader">
                <td height="13" colspan="5">Verivide</td>
                <td colspan="6">Macbeth</td>
                </tr>
              <tr bgcolor="#FFFFFF" class="normalfntMid">
                <td width="9%" height="13">D65</td>
                <td width="10%">TL84</td>
                <td width="10%">CW</td>
                <td width="10%">F</td>
                <td width="10%">UV</td>
                <td width="9%">DL</td>
                <td width="9%">CW</td>
                <td width="9%" >lnca</td>
                <td width="9%">TL84</td>
                <td width="8%" >UV</td>
                <td width="7%">Horizon</td>
                </tr>
              <tr bgcolor="#FFFFFF">
                <td height="20" align="center"><strong>
                  <input  type="checkbox" disabled="disabled" <?php echo($verivide_d65?'checked':''); ?> />
                  </strong></td>
                <td align="center"><strong>
                  <input type="checkbox" disabled="disabled" <?php echo($verivide_tl84?'checked':''); ?> />
                  </strong></td>
                <td align="center"><strong>
                  <input type="checkbox" disabled="disabled" <?php echo($verivide_cw?'checked':''); ?> />
                  </strong></td>
                <td align="center"><strong>
                  <input type="checkbox" disabled="disabled" <?php echo($verivide_f?'checked':''); ?> />
                  </strong></td>
                <td align="center"><strong>
                  <input type="checkbox" disabled="disabled" <?php echo($verivide_uv?'checked':''); ?> />
                  </strong></td>
                <td align="center"><strong>
                  <input type="checkbox"disabled="disabled"  <?php echo($macbeth_dl?'checked':''); ?> />
                  </strong></td>
                <td align="center"><strong>
                  <input type="checkbox" disabled="disabled" <?php echo($macbeth_cw?'checked':''); ?> />
                  </strong></td>
                <td align="center" ><strong>
                  <input type="checkbox" disabled="disabled" <?php echo($macbeth_inca?'checked':''); ?> />
                  </strong></td>
                <td align="center"><strong>
                  <input type="checkbox" disabled="disabled" <?php echo($macbeth_tl84?'checked':''); ?> />
                  </strong></td>
                <td align="center" ><strong>
                  <input type="checkbox" disabled="disabled" <?php echo($macbeth_uv?'checked':''); ?> />
                  </strong></td>
                <td align="center"><strong>
                  <input type="checkbox" disabled="disabled"  <?php echo($macbeth_horizon?'checked':''); ?> />
                  </strong></td>
                </tr>
              </table></td>
            </tr>
          <tr>
            <td class="normalfnt"><strong>Light Source</strong></td>
            </tr>
          <tr>
            <td width="21%" class="normalfnt"><strong>Curing Condition</strong></td>
            <td align="center" valign="middle"><table width="100%">
              <tr>
                <td width="7%" class="normalfnt">Temp</td>
                <td width="3%" align="center" valign="middle"><strong>:</strong></td>
                <td width="11%" class="normalfnt"><?php echo $curingTemp ?></td>
                <td width="15%" class="normalfnt">Belt Speed</td>
                <td width="9%" align="center" valign="middle"><strong>:</strong></td>
                <td width="55%"><span class="normalfnt"><?php echo $curingSpeed ?></span></td>
                </tr>
            </table></td>
            </tr>
          <tr>
            <td class="normalfnt"><strong>Press Condition</strong></td>
            <td align="center" valign="middle"><table width="100%">
              <tr>
                <td width="7%" class="normalfnt">Temp</td>
                <td width="3%" align="center" valign="middle"><strong>:</strong></td>
                <td width="11%" class="normalfnt"><?php echo $pressTemp ?></td>
                <td width="15%" class="normalfnt">Pressure</td>
                <td width="3%" align="center" valign="middle"><strong>:</strong></td>
                <td width="9%"><span class="normalfnt"><?php echo $pressPressure ?></span></td>
                <td width="9%" align="center" valign="middle" class="normalfnt">Time</td>
                <td width="2%" align="center"><strong>:</strong></td>
                <td width="41%"><span class="normalfnt"><?php echo $pressTime ?></span></td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td class="normalfnt"><strong>Mesh Count</strong></td>
            <td align="center" valign="middle"><span class="normalfnt"><?php echo $meshCount ?></span></td>
          </tr>
          <tr>
            <td class="normalfnt"><strong>Technical Instructions</strong></td>
            <td align="center" valign="middle" bgcolor="#CCCCFF"><span class="normalfnt"><?php echo $instructionsTech ?></span></td>
          </tr>
          <tr>
            <td class="normalfnt"><strong>Marketing Instructions</strong></td>
            <td align="center" valign="middle" bgcolor="#E0FAC9"><span class="normalfnt"><?php echo $instructions ?></span></td>
          </tr>
          <tr>
            <td class="normalfnt">&nbsp;</td>
            <td align="left" valign="middle" bgcolor="#FFFFFF"><iframe id="iframeFiles" src="filesUpload.php?txtFolder=<?php echo "$sampleNo-$sampleYear"; ?>" name="iframeFiles" style="width:400px;height:200px;border:none"  ></iframe></td>
          </tr>
          <tr>
            <td class="normalfnt">&nbsp;</td>
            <td align="left" valign="middle" bgcolor="#FFFFFF"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="normalfnt">
              <tr>
                <td colspan="2" bgcolor="" class="normalfnt"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="12%">&nbsp;</td>
                    <td width="22%" align="center" valign="middle">&nbsp;</td>
                    <td width="46%">&nbsp;</td>
                    <td width="4%">&nbsp;</td>
                    <td width="4%">&nbsp;</td>
                    <td width="4%">&nbsp;</td>
                    <td width="4%">&nbsp;</td>
                    <td width="4%">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td colspan="2" align="center" bgcolor="" class="reportSubHeader"><span class="normalfnt">Approved By</span></td>
              </tr>
              <tr>
                <td bgcolor="#DCF0FC" class="normalfnt"><span class="normalfnt"><strong>1st</strong> Stage (Marketing)</span></td>
                <td bgcolor="#DCF0FC"><span class="normalfnt"><strong>2nd</strong> Stage (Technical)</span></td>
              </tr>
              <tr>
                <td width="65%" bgcolor="" class="normalfnt"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="25%" height="31"><span class="normalfnt"><strong>PREPARED BY:</strong></span></td>
                    <td width="75%"><span class="normalfnt"><?php echo "$firstUser ($firstDate)"; ?><br />
                    </span></td>
                  </tr>
                </table></td>
                <td width="35%" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="37%"><span class="normalfnt"><strong>PREPARED BY:</strong></span></td>
                    <td width="63%"><span class="normalfnt"><?php echo "$secondUser ($secondDate)"; ?><br />
                    </span></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td bgcolor="" class="normalfnt"><table width="313" border="0" cellspacing="0" cellpadding="0">
                  <?php  
 	
	

			    //$firstApproveLevel = (int)getApproveLevel('Sample Infomations Sheet');
				for($i=1; $i<$marketingApproveLevelStart; $i++)
				{
					 $sqlc = "SELECT
							intApproveUser,
							dtApprovedDate,
							sys_users.strUserName as UserName,
							intApproveLevelNo
							FROM
							trn_sampleinfomations_approvedby
							Inner Join sys_users ON trn_sampleinfomations_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							intSampleNo 	=  '$sampleNo' AND
							intSampleYear 	=  '$sampleYear' AND
							intRevNo		=  '$revNo' AND
							intApproveLevelNo =  '$i' AND intStage=1";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
						if($i==1)
							$desc="1st ";
						else if($i==2)
							$desc="2nd ";
						else if($i==3)
							$desc="3rd ";
						else
							$desc=$i."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']." (".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='--------------------';
				?>
                  <tr>
                    <td width="313" height="21" bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Appoved By - </strong><?php echo $desc2;?></span></td>
                  </tr>
                  <?php
			}
				
			?>
                </table></td>
                <td><table width="313" border="0" cellspacing="0" cellpadding="0">
                  <?php  
 	
	

			    //$firstApproveLevel = (int)getApproveLevel('Sample Infomations Sheet');
				for($i=1; $i<$technicalApproveLevelStart; $i++)
				{
					 $sqlc = "SELECT
							intApproveUser,
							dtApprovedDate,
							sys_users.strUserName as UserName,
							intApproveLevelNo
							FROM
							trn_sampleinfomations_approvedby
							Inner Join sys_users ON trn_sampleinfomations_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							intSampleNo 	=  '$sampleNo' AND
							intSampleYear 	=  '$sampleYear' AND
							intRevNo		=  '$revNo' AND
							intApproveLevelNo =  '$i' AND intStage=2";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
						if($i==1)
							$desc="1st ";
						else if($i==2)
							$desc="2nd ";
						else if($i==3)
							$desc="3rd ";
						else
							$desc=$i."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']." (".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='--------------------';
				?>
                  <tr>
                    <td width="313" height="21" bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Appoved By - </strong><?php echo $desc2;?></span></td>
                  </tr>
                  <?php
			}
				
			?>
                </table></td>
              </tr>
            </table></td>
          </tr>
          </table></td>
      </tr>
      </table>
    </td>
</tr>
</table>
</div>        
</form>
</body>
</html>
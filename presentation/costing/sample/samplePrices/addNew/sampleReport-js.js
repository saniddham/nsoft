var basePath = 'presentation/costing/sample/samplePrices/addNew/';
var costingBasePath	="presentation/costing/sample/priceCosting/addNew/"
$(document).ready(function(){
	
	$('#frmSampleReport #cboSampleNo').die('change').live('change',loadRevisionNo);
	$('#frmSampleReport #cboRevisionNo').die('change').live('change',submitPage);
	$('#frmSampleReport #cboGraphicNo').die('change').live('change',loadSampleNo);
	$('#frmSampleReport #cboYear').die('change').live('change',function(){
	 document.location.href = '?q=489&year='+$('#cboYear').val();
	// document.frmSampleReport.submit();
	});
	
	$("#frmSampleReport .butProcess").die('click').live('click',function(){
  		addProcess(this);
		
	});

	$('#frmSampleReport #qtyPrices').die('click').live('click',function(){
		var arrBody  	= "";

		var sampleNo = $('#cboSampleNo').val();
		var SYear = $('#cboYear').val();
		var revision = $('#cboRevisionNo').val();
		var combo = $(this).parent().parent().find('.combo').html();
		var print = $(this).parent().parent().find('.print').html();
		print =   URLEncode(print);
		combo =   URLEncode(combo);

		popupWindow3('1');
		// presentation/costing/sample/priceCosting/addNew/qtyWisePricesPopup.php
		$('#popupContact1').load(costingBasePath +'qtyWisePricesPopup.php?sampleNo='+sampleNo+'&sampleYear='+SYear+'&revision='+revision+'&combo='+combo+'&print='+print+'&editable=0',function(){
			// $('#saveQtyPrices').click(saveQtyPrices);
			$('#butClose1').click(disablePopup);

		});
	});
});

function loadRevisionNo()
{
	
	var url = "presentation/customerAndOperation/sample/sampleInfomations/addNew/sampleInfomations-db-get.php?requestType=loadRevisionNo&sampleNo="+$('#cboSampleNo').val()+"&sampleYear="+$('#cboYear').val();
	var obj = $.ajax({url:url,type:'POST',async:false});	
	$('#cboRevisionNo').html(obj.responseText);
	
}

function loadGraphicNo()
{
	var url = "presentation/customerAndOperation/sample/sampleInfomations/addNew/sampleInfomations-db-get.php?requestType=loadGraphicNo&sampleYear="+$('#cboYear').val();
	var obj = $.ajax({url:url,type:'POST',async:false});	
	$('#cboGraphicNo').html(obj.responseText);	
}

function loadSampleNo()
{
	var url = "presentation/customerAndOperation/sample/sampleInfomations/addNew/sampleInfomations-db-get.php?requestType=loadSampleNo&styleNo="+URLEncode($('#cboGraphicNo').val())+"&sampleYear="+$('#cboYear').val();
	var obj = $.ajax({url:url,type:'POST',async:false});	
	$('#cboSampleNo').html(obj.responseText);	
}


function submitPage()
{
	//document.frmSampleReport.submit();
	var sampleNo 	= $('#cboSampleNo').val();
	var sampleYear 	= $('#cboYear').val();
	var revNo 	= $('#cboRevisionNo').val();
	document.location.href = "?q=489&no="+sampleNo+"&year="+sampleYear+"&revNo="+revNo;	
}

function addProcess(obj)
{
	var rowId 	=  obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
	var cellId 	= obj.parentNode.parentNode.parentNode.parentNode.parentNode.cellIndex;
	popupWindow3('1');
	

	var sampNo	=	$('#cboSampleNo').val();	
	var sampYear=   $('#cboYear').val();
	var rev		=   $('#cboRevisionNo').val();
	var	combo	=   $(obj).parent().parent().find('.combo').html();
	var	printId	=   $(obj).parent().parent().find('.print').html();
	//alert(sampNo+","+sampYear+","+rev+","+combo+","+printId);
	
	$('#popupContact1').load(basePath+'processes.php?sampNo='+sampNo+'&sampYear='+sampYear+'&rev='+rev+'&combo='+URLEncode(combo)+'&print='+URLEncode(printId),function(){
		
		$('#frmProcesses1 #butClose1').die('click').live('click',disablePopup);
		
	});	
}

 
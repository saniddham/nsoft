// JavaScript Document
var basePath = 'presentation/costing/sample/samplePrices/addNew/';
$(document).ready(function() {
	
	$('#frmSamplePrices #butNew').click(function (){
		//newForm();
		document.location.href = '?q=455';
	});
	
	$('#frmSamplePrices #butSave').click(function(){
		save_details();	
		//alert(1);
	});
	
	$('#frmSamplePrices #cboSampleNo').change(function(){
		loadRevisionNo();
	});
	
	$('#frmSamplePrices #cboRevisionNo').change(function(){
		//document.frmSamplePrices.submit();
		document.location.href ="?q=455&cboGraphicNo="+URLEncode($('#cboGraphicNo').val())+"&cboSampleNo="+$('#cboSampleNo').val()+"&cboYear="+$('#cboYear').val()+"&cboRevisionNo="+$('#cboRevisionNo').val()+"&cboCurrency="+$('#cboCurrency').val();
 	});
	
	$('#frmSamplePrices #cboGraphicNo').change(function(){
			
		loadSampleNos();
		
		var intSampleNo = $('#cboSampleNo').val();
		//$('#frmSampleInfomations').get(0).reset();
		$('#cboSampleNo').val(intSampleNo);
		$('#divPicture').html('');
		return;
	});

	$('#frmSamplePrices #cboYear').change(function(){
	 document.location.href = '?q=455&cboYear='+$('#cboYear').val();
	 
	// document.frmSamplePrices.submit();
/*		loadGraphicNos();
		loadSampleNos();
		
		var intSampleNo = $('#cboSampleNo').val();
		//$('#frmSampleInfomations').get(0).reset();
		$('#cboSampleNo').val(intSampleNo);
		$('#divPicture').html('');
		return;
*/	});


});

	function loadSampleNos()
	{
		var url = basePath+"samplePrices-db-get.php?requestType=loadSampleNo&styleNo="+URLEncode($('#cboGraphicNo').val())+"&sampleYear="+$('#cboYear').val();
		var obj = $.ajax({url:url,type:'POST',async:false});	
		$('#cboSampleNo').html(obj.responseText);	
		$('#cboRevisionNo').html('');	
	}
	
	function loadRevisionNo()
	{
		var url = basePath+"samplePrices-db-get.php?requestType=loadRevisionNo&sampleNo="+$('#cboSampleNo').val()+"&sampleYear="+$('#cboYear').val();
		var obj = $.ajax({url:url,type:'POST',async:false});	
		$('#cboRevisionNo').html(obj.responseText);
	}
	function loadSampleNos()
	{
		var url = basePath+"samplePrices-db-get.php?requestType=loadSampleNo&styleNo="+URLEncode($('#cboGraphicNo').val())+"&sampleYear="+$('#cboYear').val();
		var obj = $.ajax({url:url,type:'POST',async:false});	
		$('#cboSampleNo').html(obj.responseText);	
		$('#cboRevisionNo').html('');	
		
	}
	function loadGraphicNos()
	{
		var url = basePath+"samplePrices-db-get.php?requestType=loadGraphicNos&sampleYear="+$('#cboYear').val();
		var obj = $.ajax({url:url,type:'POST',async:false});	
		$('#cboGraphicNo').html(obj.responseText);	
		$('#cboRevisionNo').html('');	
		
	}
	
	function save_details()
	{
		//alert(2);
		if (!$('#frmSamplePrices').validationEngine('validate'))   
			return;
			
		//alert(3);
		var arrJson = "";
		var arrRMCost = "";
		var arrSubCost = "";
		//alert(4);
		var i=0;
		var j=0;
		var k=0;
		var sampleNo 	= $('#cboSampleNo').val();
		var sampleYear	= $('#cboYear').val();
		var revisionNo	= $('#cboRevisionNo').val();
		//alert(5);
		var currency	= $('#cboCurrency').val();
		//alert(6);
		$('.clsValue').each(function(){
			var printName  	= $(this).parent().attr('id');	
			var comboName	= $(this).attr('title');	
			var price		= parseFloat(($(this).val()==''?0:$(this).val()));
			if(price>0)	
			arrJson+=((i++)=='0'?'':',')+'{"combo":"'+URLEncode(comboName)+'","print":"'+URLEncode(printName)+'","price":"'+price+'"}';
		});	
		$('.clsRMCost').each(function(){
			var printName  	= $(this).parent().attr('id');	
			var comboName	= $(this).attr('title');	
			var rmCost		= parseFloat(($(this).val()==''?0:$(this).val()));
			if(rmCost>0)	
			arrRMCost+=((j++)=='0'?'':',')+'{"combo":"'+URLEncode(comboName)+'","print":"'+URLEncode(printName)+'","rmCost":"'+rmCost+'"}';
		});	
		$('.clsSubContPrice').each(function(){
			var printName  	= $(this).parent().attr('id');	
			var comboName	= $(this).attr('title');
			var subID		= $(this).attr('id');
			var subCost		= parseFloat(($(this).val()==''?0:$(this).val()));
			if(subCost>0)	
			arrSubCost+=((k++)=='0'?'':',')+'{"combo":"'+URLEncode(comboName)+'","print":"'+URLEncode(printName)+'","subID":"'+(subID)+'","subCost":"'+subCost+'"}';
		});	
		
		var url=basePath+"samplePrices-db-set.php?requestType=saveDetails&sampleNo="+sampleNo+"&sampleYear="+sampleYear+"&revNo="+revisionNo+'&currency='+currency;
		$.ajax({
			url:url,
			data:"prices=["+arrJson+"]+&rmCost=["+arrRMCost+"]+&subCost=["+arrSubCost+"]",
			type:"post",
			async:false,
			success:function(json){
												
				$('#frmSamplePrices #butSave').validationEngine('showPrompt', 'Saved Successfully.','pass' /*'pass'*/);
			}
			});
	}
	
	function newForm(){  

	 $('#frmSamplePrices').find("input[type=text], textarea,select").val('');
			$('#frmSamplePrices').validationEngine('hideAll');
	 };
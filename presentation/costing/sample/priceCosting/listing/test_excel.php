<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10/12/2017
 * Time: 4:11 PM
 */
ini_set('display_errors', 'on');
set_time_limit(1000);
ini_set('max_execution_time',80000);
session_start();

//GET SELECTED MONTH




/*********END ******/
/** Error reporting */
error_reporting(E_ALL);

define("DIR_PATH", "libraries/excel/Classes/PHPExcel.php");


require_once ($_SESSION['ROOT_PATH'].DIR_PATH);

// Create new PHPExcel
//create alphabet A-Z  and Number range 1-9
$charcters = range('A','Z');
$numbers = range(1,9);
$objPHPExcel = new PHPExcel();
// Set properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");
// Create a first sheet, representing sales data

/*******************START MONTHLY SUMMERY REPORT*****************************/
$reportTitle = 'MONTHLY SUMMERY FROM';

$objPHPExcel->getActiveSheet()->mergeCells('A2:O2');
$objPHPExcel->setActiveSheetIndex(0)
//$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Something');
    ->setCellValue('A2', $reportTitle)
    ->setCellValue('A4', 'ORDER NO')
    ->setCellValue('B4', 'YEAR')
    ->setCellValue('C4', 'SO')
    ->setCellValue('D4', 'GRAPHIC NO')
    ->setCellValue('E4', 'SAMPLE NO')
    ->setCellValue('F4', 'SAMPLE YEAR')
    ->setCellValue('G4', 'REVISION')
    ->setCellValue('H4', 'COMBO')
    ->setCellValue('I4', 'PRINT')
    ->setCellValue('J4', 'PART')
    ->setCellValue('K4', 'RM MOVED LOCATION')
    ->setCellValue('L4', 'MAIN CATEGORY')
    ->setCellValue('M4', 'SUB CATEGORY')
    ->setCellValue('N4', 'ITEM CODE')
    ->setCellValue('O4', 'ITEM')
    ->setCellValue('P4', 'UOM')
    ->setCellValue('Q4', 'CUSTOMER NAME')
    ->setCellValue('R4', 'BRAND NAME')
    ->setCellValue('S4', 'STATUS')
    ->setCellValue('T4', 'COMPLETED DATE')
    ->setCellValue('U4', 'TECHNIQUE GROUP NAME ')
    ->setCellValue('V4', 'PRICE COSTING RM VALUES(USD)')
    ->setCellValue('W4', 'PO PRICE / Graphic Selling Price(USD)')
    ->setCellValue('X4', 'CONS_PC')
    ->setCellValue('Y4', 'EXPECTED FABRIC ARRIVAL QTY')
    ->setCellValue('Z4', 'EXPECTED QTY')
    ->setCellValue('AA4', 'ACTUAL RM USAGE QTY')
    ->setCellValue('AB4', 'ACTUAL RM USAGE VALUE (LKR)')
    ->setCellValue('AC4', 'PER UNIT PRICE')
    ->setCellValue('AD4', 'EXPECTED VALUE (LKR)')
    ->setCellValue('AE4', 'VARIANCE (QTY)')
    ->setCellValue('AF4', 'VARIANCE VALUE (LKR)')
    ->setCellValue('AG4', 'TYPE');

PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);

//Set Bold Titles
for ($i = 'A'; $i !== 'AH'; $i++){
    //echo $i.', '; //A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z, AA, AB,
    $header =$i.'4';

    $objPHPExcel->getActiveSheet()->getStyle($header)->getFont()->setBold(true);
}


$objPHPExcel->getActiveSheet()
    ->getStyle('A2:AG2')
    ->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'cccccc')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font' => array(
                'bold'   => false,
                'color' => array('rgb' => '000000'),
                'size' => 12,
            )
        )
    );


$count=5;

    // output data of each row

    for($count=6;$count<10000;$count++) {
        // echo  '<br>'.$charcters[$count].'-'.$count.'<br>';

        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$count,'TEST');
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$count,'TEST12');
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$count,'TEST124');
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$count,'TEST132qw4523');
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$count,'TEST134674');
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$count,'TEST165890yfkgn');
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$count,'TEST1fxhdfcm');
        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$count,'TEST1fxjhnfxcvmf');
        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$count,'TEST1fxnhfcfv');
        $objPHPExcel->getActiveSheet()->SetCellValue('J'.$count,'TEST1xchnfxv');
        $objPHPExcel->getActiveSheet()->SetCellValue('K'.$count,'TEST1xfcndf');
        $objPHPExcel->getActiveSheet()->SetCellValue('L'.$count,'TEST1fxndfv');
        $objPHPExcel->getActiveSheet()->SetCellValue('M'.$count,'TEST1sdchn');
        $objPHPExcel->getActiveSheet()->SetCellValue('N'.$count,'TEST1fjmfhg');
        $objPHPExcel->getActiveSheet()->SetCellValue('O'.$count,'TEST1fxnjfgmc');
        $objPHPExcel->getActiveSheet()->SetCellValue('P'.$count,'TEST1fsxjfkg');
        $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$count,'TEST1fdcjfg');
        $objPHPExcel->getActiveSheet()->SetCellValue('R'.$count,'TEST1dgckjdfy');
        $objPHPExcel->getActiveSheet()->SetCellValue('S'.$count,'TEST1fdjndgf');
        $objPHPExcel->getActiveSheet()->SetCellValue('T'.$count,'TEST1fxhndf');
        $objPHPExcel->getActiveSheet()->SetCellValue('U'.$count,'TEST1fxhdfx');

        $objPHPExcel->getActiveSheet()->SetCellValue('V'.$count,'TEST1');
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('W'.$count,'TEST1fdxjd', PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('X'.$count,'TEST1srfhjdtgf', PHPExcel_Cell_DataType::TYPE_STRING);
        //$objPHPExcel->getActiveSheet()->SetCellValue('Y'.$count,$row['CONS_PC']);
        $objPHPExcel->getActiveSheet()->SetCellValue('Y'.$count,'TEST1sfhxjdgfm');
        $objPHPExcel->getActiveSheet()->SetCellValue('Z'.$count,'TEST1fxjtfg');
        $objPHPExcel->getActiveSheet()->SetCellValue('AA'.$count,'TEST1dfcjmgf');
        $objPHPExcel->getActiveSheet()->SetCellValue('AB'.$count,'TEST1dfjgm');

        $objPHPExcel->getActiveSheet()->SetCellValue('AC'.$count,'TEST1dgjkfy');

        //Expected value

        $objPHPExcel->getActiveSheet()->SetCellValue('AD'.$count,'TEST1dxhngfm');
        //variance qty=REQUIRED -qty

        $objPHPExcel->getActiveSheet()->SetCellValue('AE'.$count,'TEST1dfjdtgdgn');

        $objPHPExcel->getActiveSheet()->SetCellValue('AF'.$count,'TEST1dfhndg');
        $objPHPExcel->getActiveSheet()->SetCellValue('AG'.$count,'IN_SAMPLErdfjn');

        $count++;



}




// Rename 5th sheet
$objPHPExcel->getActiveSheet()->setTitle('MONTHLY SUMMERY');
// Redirect output to a client’s web browser (Excel5)
mysqli_close($conn);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
ob_end_clean();
$date= 'MONTHLY SUMMERY.xlsx';
header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="'.$date.'"');
//header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
//header("Content-Disposition: attachment;filename=\"filename.xlsx\"");
//header("Cache-Control: max-age=0");
$objWriter->save('php://output');
exit;






<?php


while($row = mysqli_fetch_assoc($result))
{
	
	$price_costing_rm_values=$row['INK_COST'] + $row['RM_COST'];
	
	
	if($rpt_type=='MOVED'){	
		if($row['QTY']<=0)	
			$unit_price =$row['UNIT_PRICE'];
		else
			$unit_price = ($row['AMOUNT']/ $row['QTY']);
	}
	else if($rpt_type=='NOT_MOVED')
		$unit_price = $row['UNIT_PRICE'];
	else {
        $unit_price = $row['UNIT_PRICE']; // put it separate as there may be changes
        $row['NO'] = $row['intOrderNo'];
        $row['YEAR'] = $row['intOrderYear'];
//        $row['LOCATION'] = '';
        $row['ord_loc'] = '';
        $row['QTY'] = 0;
    }

	$expected_value = $row['REQUIRED'] * $unit_price;
	$variance_qty 	= $row['REQUIRED'] - $row['QTY'];
	$variance_value = ($row['REQUIRED']* $unit_price) - $row['AMOUNT'];
	
	$bg_col_type	= $row['TYPE'];


    if($rpt_type=="NO_RM_ISSUED")
        $bCol	="#ccffe6";
    else if($row['TYPE']=="NOT_IN_SAMPLE_BUT_MOVED")
		$bCol	="D79DA9";	
	else if($row['TYPE']=="IN_SAMPLE_MOVED")
		$bCol	="#FFFFFF";	
	else if($row['TYPE']=="IN_SAMPLE_BUT_NOT_MOVED")
		$bCol	="#94BD94";
	else {
        $bCol	="#ccffe6";
    }

	if ($row['orderStatus'] == -10){
        $row['orderStatus'] = "COMPLETED";
    } else if ($row['orderStatus'] > 1){
        $row['orderStatus'] = "PENDING";
    } else if ($row['orderStatus'] == 1){
        $row['orderStatus'] = "APPROVED";
    } else if ($row['orderStatus']=0){
        $row['orderStatus'] = "REJECTED";
    } else if ($row['orderStatus']=-1){
        $row['orderStatus'] = "REVISED";
    } else {
        $row['orderStatus'] = "CANCEL";
    }
    ?>
  <tr bgcolor="<?php echo $bCol; ?>">
          <td><?php echo $row['TYPE']; ?></td>
          <td><?php echo $row['NO']; ?></td>
          <td><?php echo $row['YEAR']; ?></td>
          <td><?php echo $row['strSalesOrderNo']; ?></td>
          <td><?php echo $row['strGraphicNo']; ?></td>
          <td><?php echo $row['ORDER_QTY']; ?></td>
          <td><?php echo $row['DISPATCHED_QTY']; ?></td>
          <td><?php echo $row['RM_LOCATION']; ?></td>
          <td><?php echo $row['dblDamagePercentage']; ?></td>
          <td><?php echo $row['dblOverCutPercentage']; ?></td>
          <td><?php echo $row['intSampleNo']; ?></td>
          <td><?php echo $row['intSampleYear']; ?></td>
          <td><?php echo $row['intRevisionNo']; ?></td>
          <td><?php echo $row['strCombo']; ?></td>
          <td><?php echo $row['strPrintName']; ?></td>
          <td><?php echo $row['part']; ?></td>
          <td><?php echo $row['RM_LOCATION']; ?></td>
          <td><?php echo $row['ORDER_LOCATION'];?></td>
          <td><?php echo $row['MAIN_CATEGORY']; ?></td>
          <td><?php echo $row['SUB_CATEGORY']; ?></td>
          <td><?php if($row['supItemCode']!=null) echo $row['supItemCode']; else echo $row['ITEM_CODE']; ?></td>
          <td><?php echo $row['ITEM_NAME']; ?></td>
          <td><?php echo $row['UOM']; ?></td>
          <td><?php echo $row['CUSTOMER_NAME']; ?></td>
          <td><?php echo $row['BRAND_NAME']; ?></td>
          <td><?php echo $row['orderStatus']; ?></td>
          <td><?php echo $row['COMPLETED_DATE']; ?></td>
          <td><?php echo $row['TECHNIQUE_GROUP_NAME']; ?></td>
          <td><?php echo $price_costing_rm_values ?></td>
          <td><?php echo $row['dblPrice']; ?></td>
          <td><?php echo $row['CONS_PC']; ?></td>
          <td><?php echo $row['REQUIRED']; ?></td>
          <td><?php echo round($row['ACTUAL_RM_QTY'],8); ?></td>
          <td><?php echo round($row['ACTUAL_RM_AMOUNT'],8); ?></td>
          <td><?php echo round($unit_price,8); ?></td>
  </tr>
  <?php
}


	?>


<?php
	session_start();
	$user_id= $_SESSION['userId'];
	if($_SESSION['userId']=='')
	die("Temporary unavailable. Please contact the admin");

  /**
 * Created by PhpStorm.
 * User: User
 * Date: 10/12/2017
 * Time: 4:11 PM
 */
$backwardseperator = "../../../../../";

//include "{$backwardseperator}dataAccess/Connector.php";

$servername = $_SESSION['Server'];
$username = $_SESSION['UserName'];
$password = $_SESSION['Password'];
$dbname = $_SESSION['Database'];

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);



/*
ini_set('Upload_max_filesize', '1500M');
ini_set('Max_input_time','1500');
ini_set('Memory_limit','640M');
ini_set('Max_execution_time',1800);
ini_set('Post_max_size','2000M');

 

ini_set('display_errors', 'off');
//set_time_limit(1000);
ini_set('max_execution_time', 11111111) ;
session_start();
$user_id= $_SESSION['userId'];
if($_SESSION['userId']!=2)
	die("Temporary unavailable");*/

//GET SELECTED MONTH
if($_GET['startDate']) {
    $date = $_GET['startDate'];
    $time = strtotime($date);
    $previous_month = date('Y-m-d 00:00:00', $time);
    //$previous_month = '2017-05-01 00:00:00';

//get one month from specific date
    $current_month = date('Y-m-d 00:00:00', strtotime('+1 month', strtotime($previous_month)));
   // $current_month = '2017-11-10 00:00:00';
    //previous_dix_month from selected month
    $six_month = date('Y-m-d', strtotime('-6 month', strtotime($previous_month)));


}

$s = $current_month;

$date = strtotime($s);
$end_date= date('Y-m-d H:i:s', strtotime('-1 day', $date));

 
$tempory_rm_moved_orders_summery= 'tempory_rm_moved_orders_summery'.$user_id;
$sql= 'DROP TEMPORARY TABLE IF EXISTS .`'.$tempory_rm_moved_orders_summery.'`';
if ($conn->query($sql) === TRUE) {
   // echo "Table Drop".'<br>';
} else {
  //  echo "Error: " . $sql . "<br>" . $db->error;
}
//TEMPORARY
$sql ="CREATE TEMPORARY TABLE $tempory_rm_moved_orders_summery ENGINE=MEMORY 
as (SELECT
ware_fabricdispatchheader.intOrderNo AS NO,
ware_fabricdispatchheader.intOrderYear AS YEAR,
ware_fabricdispatchheader.intCompanyId AS PROD_LOCATION

FROM
ware_fabricdispatchheader INNER JOIN  ware_fabricdispatchheader_approvedby
ON ware_fabricdispatchheader_approvedby.intBulkDispatchNo=ware_fabricdispatchheader.intBulkDispatchNo
AND ware_fabricdispatchheader_approvedby.intYear=ware_fabricdispatchheader.intBulkDispatchNoYear
WHERE 
date(ware_fabricdispatchheader_approvedby.dtApprovedDate) >= '$previous_month' AND
ware_fabricdispatchheader_approvedby.dtApprovedDate < '$current_month' AND
ware_fabricdispatchheader.intStatus = 1 
GROUP BY
ware_fabricdispatchheader.intOrderNo ,
ware_fabricdispatchheader.intOrderYear,
ware_fabricdispatchheader.intCompanyId
 )";

if ($conn->query($sql) === TRUE) {
  //  echo "Table Created".'<br>';
} else {
  //  echo "Error: " . $sql . "<br>" . $db->error;
}
/*$sql	="select * from tempory_rm_moved_orders_summery2 limit 1";
$re		= $conn->query($sql);
$row = $re->fetch_assoc();
print_r($row);
die();*/

//var_dump($sql);

$body	=ob_start();

include "orderwise_monthly_summery_excel_header.php";


//rm moved - in sample or not in sample
$rm_moved_query = getReport($tempory_rm_moved_orders_summery);

$result = $conn->query($rm_moved_query);
$rpt_type	='MOVED';
include "orderwise_monthly_summery_excel.php";

 
  

//rm not moved - but in sample
$rm_not_moved_query= getReportRmNotMoved($tempory_rm_moved_orders_summery);
$result = $conn->query($rm_not_moved_query);
$rpt_type	='NOT_MOVED';
include "orderwise_monthly_summery_excel.php";

//pos having dummys
//$splitedPoQuery = getReportForSPlitedPos($tempory_rm_moved_orders_summery,$temp_lkr_value_item);
//
//$result = $conn->query($splitedPoQuery);
//
//$rpt_type	='NO_RM_ISSUED';
//include "orderwise_monthly_summery_excel.php";
mysqli_close($conn);

 ?>
 
</table>
<?php
echo $body 			= ob_get_clean();
?>
 <?php
 
//get query for each report
function getReportNotInSample($rpt_type,$previous_month,$tempory_rm_moved_orders_summery,$six_month)
{

    $str = $previous_month;
    $date = explode(" ", $str);

    if($rpt_type == 6) {
        $sql= 'SELECT TB1.*,
(SELECT SUM(TRNS.dblQty*-1) FROM ware_stocktransactions_bulk AS TRNS  WHERE
TRNS.intOrderNo = TB1.`NO`
AND TRNS.intOrderYear =TB1.`YEAR`
AND TRNS.intSalesOrderId = TB1.intSalesOrderId
AND TRNS.intItemId = TB1.ITEM_ID
AND TRNS.intLocationId = TB1.LOCATION_ID
AND TRNS.strType IN (\'ISSUE\', \'RETSTORES\')
) AS QTY,
(SELECT SUM(TRNS.dblQty*-1) FROM ware_stocktransactions_bulk AS TRNS  WHERE
TRNS.intOrderNo = TB1.`NO`
AND TRNS.intOrderYear =TB1.`YEAR`
AND TRNS.intSalesOrderId = TB1.intSalesOrderId
AND TRNS.intItemId = TB1.ITEM_ID
AND TRNS.intLocationId = TB1.LOCATION_ID
AND TRNS.strType IN (\'ISSUE\', \'RETSTORES\')
) AS QTY,
(
SELECT
 SUM(TRNS.dblQty* TRNS.dblGRNRate * mst_financeexchangerate.dblBuying *- 1) FROM ware_stocktransactions_bulk AS TRNS
LEFT JOIN mst_financeexchangerate ON mst_financeexchangerate.dtmDate = TRNS.dtGRNDate
AND mst_financeexchangerate.intCompanyId = TRNS.intCompanyId
AND mst_financeexchangerate.intCurrencyId = TRNS.intCurrencyId
WHERE
TRNS.intOrderNo = TB1.`NO`
AND TRNS.intOrderYear =TB1.`YEAR`
AND TRNS.intSalesOrderId = TB1.intSalesOrderId
AND TRNS.intItemId = TB1.ITEM_ID
AND TRNS.intLocationId = TB1.LOCATION_ID
AND TRNS.strType IN (\'ISSUE\', \'RETSTORES\')
) AS AMOUNT,
(SELECT SUM(costing_sample_header.INK_COST) FROM
costing_sample_header WHERE TB1.intSampleNo = costing_sample_header.SAMPLE_NO
AND TB1.intSampleYear = costing_sample_header.SAMPLE_YEAR
AND TB1.intRevisionNo = costing_sample_header.REVISION
AND TB1.strCombo = costing_sample_header.COMBO
AND TB1.strPrintName = costing_sample_header.PRINT
) AS INK_COST,
(SELECT SUM(costing_sample_header.SPECIAL_RM_COST) FROM
costing_sample_header WHERE TB1.intSampleNo = costing_sample_header.SAMPLE_NO
AND TB1.intSampleYear = costing_sample_header.SAMPLE_YEAR
AND TB1.intRevisionNo = costing_sample_header.REVISION
AND TB1.strCombo = costing_sample_header.COMBO
AND TB1.strPrintName = costing_sample_header.PRINT
) AS RM_COST
FROM

(
SELECT DISTINCT
    '.$tempory_rm_moved_orders_summery.'.`NO`,
    '.$tempory_rm_moved_orders_summery.'.`YEAR`,
        trn_orderdetails.intSalesOrderId,
    trn_orderdetails.strSalesOrderNo,
    trn_orderdetails.strGraphicNo,
    trn_orderdetails.intSampleNo,
    trn_orderdetails.intSampleYear,
    trn_orderdetails.intRevisionNo,
    trn_orderdetails.strCombo,
    trn_orderdetails.strPrintName,
    mst_part.strName AS part,
        mst_locations.intId AS LOCATION_ID,
    mst_locations.strName AS LOCATION,
    trn_orderdetails.intQty,
    mst_maincategory.strName AS MAIN_CATEGORY,
    mst_subcategory.strName AS SUB_CATEGORY,
        mst_item.intId AS ITEM_ID,
    mst_item.strCode AS ITEM_CODE,
    CONCAT(mst_supplier.strCode,mst_item.strCode) as supItemCode,
    mst_item.strName AS ITEM_NAME,
    mst_units.strCode AS UOM,
    trn_po_prn_details_sales_order.PRODUCTION_QTY,
    0 AS REQUIRED,
     /*
    trn_po_prn_details_sales_order.CONS_PC,
    trn_po_prn_details_sales_order.PRODUCTION_QTY,
    trn_po_prn_details_sales_order.REQUIRED,
    sum(
        ifnull(
            ware_stocktransactions_bulk.dblQty *- 1,
            0
        )
    ) AS qty,*/
    /*sum(
        ifnull(
            ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate * mst_financeexchangerate.dblBuying *- 1,
            0
        )
    ) AS amount, */
    trn_orderdetails.dblPrice,
    mst_customer.strName AS CUSTOMER_NAME,
    mst_brand.strName AS BRAND_NAME,
    trn_orderheader.intStatus,

IF (
    trn_orderheader.intStatus = 1,
    \'Approved\',

IF (
    trn_orderheader.intStatus = 0,
    \'Rejected\',

IF (
    trn_orderheader.intStatus =- 10,
    \'Completed\',

IF (
    trn_orderheader.intStatus =- 2,
    \'Cancel\',

IF (
    trn_orderheader.intStatus =- 1,
    \'Revised\',
    \'Pending\'
)
)
)
)
) AS STATUS,
 trn_orderdetails.dtDeliveryDate,
 mst_technique_groups.TECHNIQUE_GROUP_NAME,
        (
			SELECT
				trn_orderheader_approvedby.dtApprovedDate
			FROM
				trn_orderheader_approvedby
			WHERE
				trn_orderheader_approvedby.intApproveLevelNo = "-10" 
				AND trn_orderdetails.intOrderNo = trn_orderheader_approvedby.intOrderNo
				AND trn_orderdetails.intOrderYear = trn_orderheader_approvedby.intYear
			ORDER BY
				trn_orderheader_approvedby.dtApprovedDate DESC
			LIMIT 1
		) AS COMPLETED_DATE

FROM
ware_stocktransactions_bulk
INNER JOIN '.$tempory_rm_moved_orders_summery.' ON ware_stocktransactions_bulk.intOrderNo = '.$tempory_rm_moved_orders_summery.'.`NO`
AND ware_stocktransactions_bulk.intOrderYear = '.$tempory_rm_moved_orders_summery.'.`YEAR`
and date(ware_stocktransactions_bulk.dtDate) >= '.$six_month.'
INNER JOIN trn_orderdetails ON ware_stocktransactions_bulk.intOrderNo = trn_orderdetails.intOrderNo
AND ware_stocktransactions_bulk.intOrderYear = trn_orderdetails.intOrderYear
AND ware_stocktransactions_bulk.intSalesOrderId = trn_orderdetails.intSalesOrderId
INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN mst_item ON ware_stocktransactions_bulk.intItemId = mst_item.intId
LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier =  mst_supplier.intId
INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId

INNER JOIN mst_locations ON ware_stocktransactions_bulk.intLocationId = mst_locations.intId
INNER JOIN mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
INNER JOIN trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo
AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear
AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
INNER JOIN mst_technique_groups ON trn_orderdetails.TECHNIQUE_GROUP_ID = mst_technique_groups.TECHNIQUE_GROUP_ID
 


LEFT JOIN trn_po_prn_details_sales_order
on  ware_stocktransactions_bulk.intOrderYear= trn_po_prn_details_sales_order.ORDER_NO
and ware_stocktransactions_bulk.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
and  ware_stocktransactions_bulk.intSalesOrderId = trn_po_prn_details_sales_order.SALES_ORDER
and  ware_stocktransactions_bulk.intItemId = trn_po_prn_details_sales_order.ITEM
where
ware_stocktransactions_bulk.strType IN (\'ISSUE\', \'RETSTORES\')
AND ware_stocktransactions_bulk.intOrderNo > 0 AND
trn_po_prn_details_sales_order.ITEM is null
/*
GROUP BY
    ware_stocktransactions_bulk.intLocationId,
    ware_stocktransactions_bulk.intOrderNo,
    ware_stocktransactions_bulk.intOrderYear,
    ware_stocktransactions_bulk.intSalesOrderId,
    ware_stocktransactions_bulk.intItemId */
) AS TB1';

    }

    return $sql;
}

//in sample but moved
function getReportRmInsample($rpt_type,$previous_month,$tempory_rm_moved_orders_summery,$current_month,$six_month){
    //remove timestamp from the previous month date

    $str = $current_month;
    $date = explode(" ", $str);

    if ($rpt_type == 5) {
        $sql = "SELECT
$tempory_rm_moved_orders_summery.`NO`,
$tempory_rm_moved_orders_summery.`YEAR`,
trn_orderdetails.strSalesOrderNo,
trn_orderdetails.strGraphicNo,
trn_orderdetails.intSampleNo,
trn_orderdetails.intSampleYear,
trn_orderdetails.intRevisionNo,
trn_orderdetails.strCombo,
trn_orderdetails.strPrintName,
mst_part.strName AS part,
mst_locations.strName AS LOCATION,
trn_orderdetails.intQty,
mst_maincategory.strName AS MAIN_CATEGORY,
mst_subcategory.strName AS SUB_CATEGORY,
mst_item.strCode AS ITEM_CODE,
CONCAT(mst_supplier.strCode,mst_item.strCode) as supItemCode,
mst_item.strName AS ITEM,
mst_units.strCode AS UOM,
trn_po_prn_details_sales_order.CONS_PC,
trn_po_prn_details_sales_order.PRODUCTION_QTY,
trn_po_prn_details_sales_order.REQUIRED,
sum(ifnull(ware_stocktransactions_bulk.dblQty*-1,0)) as qty,
sum(ifnull(ware_stocktransactions_bulk.dblQty*ware_stocktransactions_bulk.dblGRNRate*mst_financeexchangerate.dblBuying*-1,0)) as amount,
trn_orderdetails.dblPrice,
mst_customer.strName AS CUSTOMER_NAME,
	mst_brand.strName AS BRAND_NAME,
	trn_orderheader.intStatus,

IF (
	trn_orderheader.intStatus = 1,
	'Approved',

IF (
	trn_orderheader.intStatus = 0,
	'Rejected',

IF (
	trn_orderheader.intStatus =- 10,
	'Completed',

IF (
	trn_orderheader.intStatus =- 2,
	'Cancel',

IF (
	trn_orderheader.intStatus =- 1,
	'Revised',
	'Pending'
)
)
)
)
) AS STATUS,
 trn_orderdetails.dtDeliveryDate,
 mst_technique_groups.TECHNIQUE_GROUP_NAME,
(SELECT SUM(costing_sample_header.INK_COST) FROM
costing_sample_header WHERE trn_orderdetails.intSampleNo = costing_sample_header.SAMPLE_NO
AND trn_orderdetails.intSampleYear = costing_sample_header.SAMPLE_YEAR
AND trn_orderdetails.intRevisionNo = costing_sample_header.REVISION
AND trn_orderdetails.strCombo = costing_sample_header.COMBO
AND trn_orderdetails.strPrintName = costing_sample_header.PRINT
) AS INK_COST,
(SELECT SUM(costing_sample_header.SPECIAL_RM_COST) FROM
costing_sample_header WHERE trn_orderdetails.intSampleNo = costing_sample_header.SAMPLE_NO
AND trn_orderdetails.intSampleYear = costing_sample_header.SAMPLE_YEAR
AND trn_orderdetails.intRevisionNo = costing_sample_header.REVISION
AND trn_orderdetails.strCombo = costing_sample_header.COMBO
AND trn_orderdetails.strPrintName = costing_sample_header.PRINT
) AS SPECIAL_RM_COST,
	(
			SELECT
				trn_orderheader_approvedby.dtApprovedDate
			FROM
				trn_orderheader_approvedby
			WHERE
				trn_orderheader_approvedby.intApproveLevelNo = \"-10\" 
				AND trn_orderdetails.intOrderNo = trn_orderheader_approvedby.intOrderNo
				AND trn_orderdetails.intOrderYear = trn_orderheader_approvedby.intYear
			ORDER BY
				trn_orderheader_approvedby.dtApprovedDate DESC
			LIMIT 1
		) AS COMPLETED_DATE
FROM
(select * from $tempory_rm_moved_orders_summery /*limit 0,500*/) as $tempory_rm_moved_orders_summery
INNER JOIN trn_po_prn_details_sales_order ON $tempory_rm_moved_orders_summery.`NO` = trn_po_prn_details_sales_order.ORDER_NO AND $tempory_rm_moved_orders_summery.`YEAR` = trn_po_prn_details_sales_order.ORDER_YEAR
INNER JOIN trn_orderdetails ON trn_po_prn_details_sales_order.ORDER_NO = trn_orderdetails.intOrderNo AND trn_po_prn_details_sales_order.ORDER_YEAR = trn_orderdetails.intOrderYear AND trn_po_prn_details_sales_order.SALES_ORDER = trn_orderdetails.intSalesOrderId
INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN mst_item ON trn_po_prn_details_sales_order.ITEM = mst_item.intId
LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier =  mst_supplier.intId
INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
LEFT JOIN ware_stocktransactions_bulk ON trn_po_prn_details_sales_order.ORDER_NO = ware_stocktransactions_bulk.intOrderNo AND trn_po_prn_details_sales_order.ORDER_YEAR = ware_stocktransactions_bulk.intOrderYear AND trn_po_prn_details_sales_order.SALES_ORDER = ware_stocktransactions_bulk.intSalesOrderId AND trn_po_prn_details_sales_order.ITEM = ware_stocktransactions_bulk.intItemId
AND ware_stocktransactions_bulk.dtDate >= '$six_month' AND
 date(ware_stocktransactions_bulk.dtDate) <='$date[0]'
 AND
ware_stocktransactions_bulk.strType IN ('ISSUE','RETSTORES')
LEFT JOIN mst_financeexchangerate on mst_financeexchangerate.dtmDate = ware_stocktransactions_bulk.dtGRNDate and mst_financeexchangerate.intCompanyId = ware_stocktransactions_bulk.intCompanyId
and mst_financeexchangerate.intCurrencyId = ware_stocktransactions_bulk.intCurrencyId
LEFT JOIN mst_locations ON ware_stocktransactions_bulk.intLocationId=mst_locations.intId
LEFT JOIN mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
INNER JOIN trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo
AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear
AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
LEFT JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
LEFT JOIN mst_technique_groups ON trn_orderdetails.TECHNIQUE_GROUP_ID = mst_technique_groups.TECHNIQUE_GROUP_ID

WHERE  ware_stocktransactions_bulk.intLocationId IS NOT NULL
GROUP BY
ware_stocktransactions_bulk.intLocationId,
trn_po_prn_details_sales_order.ORDER_NO,
trn_po_prn_details_sales_order.ORDER_YEAR,
trn_po_prn_details_sales_order.SALES_ORDER,
trn_po_prn_details_sales_order.ITEM
";

}

    return $sql;
}



function getReportRmNotMoved($tempory_rm_moved_orders_summery){

     $sql1='SELECT DISTINCT
	"IN_SAMPLE_BUT_NOT_MOVED" as TYPE, 
	0 as ACTUAL_RM_QTY, 
	0 as ACTUAL_RM_AMOUNT,
	"NO LOCATION" as RM_LOCATION,
	
    (SELECT  mst_locations.strName FROM mst_locations   
    where mst_locations.intId = trn_orderheader.intLocationId ) as ORDER_LOCATION,
    
    (SELECT (stock_value/balanceQty) FROM avg_lkr_value_item AS ALKR  WHERE
    ALKR.companyId = 1
    AND ALKR.itemId =mst_item.intId
    ) AS UNIT_PRICE,
    
    (SELECT SUM(DIS_D.dblGoodQty) FROM ware_fabricdispatchdetails DIS_D  INNER JOIN ware_fabricdispatchheader 
      ON  ware_fabricdispatchheader.intBulkDispatchNo = DIS_D.intBulkDispatchNo AND 
      ware_fabricdispatchheader.intBulkDispatchNoYear = DIS_D.intBulkDispatchNoYear 
      WHERE
    ware_fabricdispatchheader.intOrderNo = '.$tempory_rm_moved_orders_summery.'.`NO`
    AND ware_fabricdispatchheader.intOrderYear ='.$tempory_rm_moved_orders_summery.'.`YEAR`
    AND ware_fabricdispatchheader.intStatus = 1 
    AND ware_fabricdispatchheader.intCompanyId= '.$tempory_rm_moved_orders_summery.'.`PROD_LOCATION`
    AND DIS_D.intSalesOrderId = trn_orderdetails.`intSalesOrderId`
    ) AS DISPATCHED_QTY, 

    (SELECT dispatcedLoc.strName FROM mst_locations AS dispatcedLoc  WHERE
    dispatcedLoc.intId = '.$tempory_rm_moved_orders_summery.'.`PROD_LOCATION`
    ) AS RM_LOCATION, 
     			   
	trn_po_prn_details_sales_order.SALES_ORDER AS ID,
	trn_orderdetails.strSalesOrderNo,
	trn_orderdetails.strGraphicNo,
	trn_orderdetails.intQty as ORDER_QTY,
	trn_orderdetails.strCombo,
	trn_orderdetails.intSampleNo,
	trn_orderdetails.intSampleYear,
	trn_orderdetails.dblOverCutPercentage,
    trn_orderdetails.dblDamagePercentage,
	trn_orderdetails.strPrintName,
	trn_orderdetails.intRevisionNo,
	mst_part.strName AS part,
	mst_item.strName AS ITEM_NAME,
	mst_item.strCode AS ITEM_CODE,
    CONCAT(mst_supplier.strCode,mst_item.strCode) as supItemCode,
	mst_maincategory.strName AS MAIN_CATEGORY,
	mst_subcategory.strName AS SUB_CATEGORY,
	mst_units.strName AS UOM,
	trn_po_prn_details_sales_order.CONS_PC,
	trn_po_prn_details_sales_order.PRODUCTION_QTY,
	trn_po_prn_details_sales_order.REQUIRED,
	
	'.$tempory_rm_moved_orders_summery.'.`YEAR`,
	'.$tempory_rm_moved_orders_summery.'.`NO`,
	'.$tempory_rm_moved_orders_summery.'.`PROD_LOCATION`,
	mst_customer.strName AS CUSTOMER_NAME,
	mst_brand.strName AS BRAND_NAME,
	trn_orderheader.intStatus as orderStatus,
    trn_orderdetails.dtDeliveryDate,
    mst_technique_groups.TECHNIQUE_GROUP_NAME,
    trn_orderdetails.dblPrice,
     (
        SELECT
            trn_orderheader_approvedby.dtApprovedDate
        FROM
            trn_orderheader_approvedby
        WHERE
            trn_orderheader_approvedby.intApproveLevelNo = "-10"
        AND trn_orderdetails.intOrderNo = trn_orderheader_approvedby.intOrderNo
        AND trn_orderdetails.intOrderYear = trn_orderheader_approvedby.intYear
        ORDER BY
            trn_orderheader_approvedby.dtApprovedDate DESC
        LIMIT 1
     ) AS COMPLETED_DATE,

(SELECT SUM(costing_sample_header.INK_COST) FROM
costing_sample_header WHERE trn_orderdetails.intSampleNo = costing_sample_header.SAMPLE_NO
AND trn_orderdetails.intSampleYear = costing_sample_header.SAMPLE_YEAR
AND trn_orderdetails.intRevisionNo = costing_sample_header.REVISION
AND trn_orderdetails.strCombo = costing_sample_header.COMBO
AND trn_orderdetails.strPrintName = costing_sample_header.PRINT
) AS INK_COST,
(SELECT SUM(costing_sample_header.SPECIAL_RM_COST) FROM
costing_sample_header WHERE trn_orderdetails.intSampleNo = costing_sample_header.SAMPLE_NO
AND trn_orderdetails.intSampleYear = costing_sample_header.SAMPLE_YEAR
AND trn_orderdetails.intRevisionNo = costing_sample_header.REVISION
AND trn_orderdetails.strCombo = costing_sample_header.COMBO
AND trn_orderdetails.strPrintName = costing_sample_header.PRINT
) AS RM_COST,
 (
	SELECT
		st.QTY
	FROM
		trn_po_prn_details_sales_order_stock_transactions AS st
	WHERE
		st.ORDER_NO > 0
	AND st.`TRANSACTION` = \'ISSUE\'
	AND trn_po_prn_details_sales_order.ORDER_NO = st.ORDER_NO
	AND trn_po_prn_details_sales_order.ORDER_YEAR = st.ORDER_YEAR
	AND trn_po_prn_details_sales_order.SALES_ORDER = st.SALES_ORDER
	AND trn_po_prn_details_sales_order.ITEM = st.ITEM
	AND st.LOCATION_ID = '.$tempory_rm_moved_orders_summery.'.`PROD_LOCATION`
	LIMIT 1
) AS qty


FROM
	trn_po_prn_details_sales_order
INNER JOIN '.$tempory_rm_moved_orders_summery.' ON '.$tempory_rm_moved_orders_summery.'.`NO` = trn_po_prn_details_sales_order.ORDER_NO
AND '.$tempory_rm_moved_orders_summery.'.`YEAR` = trn_po_prn_details_sales_order.ORDER_YEAR


INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO
AND trn_orderdetails.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
AND trn_orderdetails.intSalesOrderId = trn_po_prn_details_sales_order.SALES_ORDER
INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
INNER JOIN mst_item ON trn_po_prn_details_sales_order.ITEM = mst_item.intId
LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier =  mst_supplier.intId
INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
INNER JOIN mst_subcategory ON mst_subcategory.intMainCategory = mst_maincategory.intId
AND mst_item.intSubCategory = mst_subcategory.intId
INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
INNER JOIN trn_orderheader ON trn_po_prn_details_sales_order.ORDER_NO = trn_orderheader.intOrderNo
AND trn_po_prn_details_sales_order.ORDER_YEAR = trn_orderheader.intOrderYear
LEFT JOIN mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
INNER JOIN trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo
AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear
AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
LEFT JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
LEFT JOIN mst_technique_groups ON trn_orderdetails.TECHNIQUE_GROUP_ID = mst_technique_groups.TECHNIQUE_GROUP_ID
HAVING
	qty IS NULL
	';

    return $sql1;

}



function getReport($tempory_rm_moved_orders_summery){
    $sql="SELECT TB1.*,

(SELECT SUM(TRNS.dblQty*-1) FROM ware_stocktransactions_bulk AS TRNS  WHERE
TRNS.intOrderNo = TB1.`NO`
AND TRNS.intOrderYear =TB1.`YEAR`
AND TRNS.intSalesOrderId = TB1.intSalesOrderId
AND TRNS.intItemId = TB1.ITEM_ID
AND TRNS.intLocationId = TB1.LOCATION_ID
AND TRNS.strType IN ('ISSUE', 'RETSTORES')
) AS ACTUAL_RM_QTY,  

(SELECT SUM(DIS_D.dblGoodQty) FROM ware_fabricdispatchdetails DIS_D  INNER JOIN ware_fabricdispatchheader 
  ON  ware_fabricdispatchheader.intBulkDispatchNo = DIS_D.intBulkDispatchNo AND 
  ware_fabricdispatchheader.intBulkDispatchNoYear = DIS_D.intBulkDispatchNoYear 
  WHERE
ware_fabricdispatchheader.intOrderNo = TB1.`NO`
AND ware_fabricdispatchheader.intOrderYear =TB1.`YEAR`
AND ware_fabricdispatchheader.intStatus = 1 
AND ware_fabricdispatchheader.intCompanyId= TB1.`LOCATION_ID`
AND DIS_D.intSalesOrderId = TB1.`intSalesOrderId`
) AS DISPATCHED_QTY, 

(SELECT dispatcedLoc.strName FROM mst_locations AS dispatcedLoc  WHERE
dispatcedLoc.intId = TB1.`PROD_LOCATION`
) AS PRO_LOCATION_NAME,   

(SELECT orderLoc.strName FROM mst_locations AS orderLoc  WHERE
orderLoc.intId = TB1.`orderLocation`
) AS ORDER_LOCATION,  

(SELECT (stock_value/balanceQty) FROM avg_lkr_value_item AS ALKR  WHERE
ALKR.companyId = 1
AND ALKR.itemId =TB1.`ITEM_ID`
) AS UNIT_PRICE,  

(
SELECT
 SUM(TRNS.dblQty* TRNS.dblGRNRate * mst_financeexchangerate.dblBuying *- 1) FROM ware_stocktransactions_bulk AS TRNS
LEFT JOIN mst_financeexchangerate ON mst_financeexchangerate.dtmDate = TRNS.dtGRNDate
AND mst_financeexchangerate.intCompanyId = TRNS.intCompanyId
AND mst_financeexchangerate.intCurrencyId = TRNS.intCurrencyId
WHERE
TRNS.intOrderNo = TB1.`NO`
AND TRNS.intOrderYear =TB1.`YEAR`
AND TRNS.intSalesOrderId = TB1.intSalesOrderId
AND TRNS.intItemId = TB1.ITEM_ID
AND TRNS.intLocationId = TB1.LOCATION_ID
AND TRNS.strType IN ('ISSUE', 'RETSTORES')
) AS ACTUAL_RM_AMOUNT,
(SELECT SUM(costing_sample_header.INK_COST) FROM
costing_sample_header WHERE TB1.intSampleNo = costing_sample_header.SAMPLE_NO
AND TB1.intSampleYear = costing_sample_header.SAMPLE_YEAR
AND TB1.intRevisionNo = costing_sample_header.REVISION
AND TB1.strCombo = costing_sample_header.COMBO
AND TB1.strPrintName = costing_sample_header.PRINT
) AS INK_COST,
(SELECT SUM(costing_sample_header.SPECIAL_RM_COST) FROM
costing_sample_header WHERE TB1.intSampleNo = costing_sample_header.SAMPLE_NO
AND TB1.intSampleYear = costing_sample_header.SAMPLE_YEAR
AND TB1.intRevisionNo = costing_sample_header.REVISION
AND TB1.strCombo = costing_sample_header.COMBO
AND TB1.strPrintName = costing_sample_header.PRINT
) AS RM_COST
FROM

(
SELECT DISTINCT 
    $tempory_rm_moved_orders_summery.`NO`,
    $tempory_rm_moved_orders_summery.`YEAR`,
    $tempory_rm_moved_orders_summery.`PROD_LOCATION`,
    trn_orderdetails.intSalesOrderId,
    trn_orderdetails.strSalesOrderNo,
    trn_orderdetails.strGraphicNo,
    trn_orderdetails.intSampleNo,
    trn_orderdetails.intSampleYear,
    trn_orderdetails.intRevisionNo,
    trn_orderdetails.strCombo,
    trn_orderdetails.strPrintName,
    mst_part.strName AS part,
    mst_locations.intId AS LOCATION_ID,
    mst_locations.strName AS RM_LOCATION,
    trn_orderdetails.intQty,
    mst_maincategory.strName AS MAIN_CATEGORY,
    mst_subcategory.strName AS SUB_CATEGORY,
        mst_item.intId AS ITEM_ID,
    mst_item.strCode AS ITEM_CODE,
    CONCAT(mst_supplier.strCode,mst_item.strCode) as supItemCode,
    mst_item.strName AS ITEM_NAME,
    mst_units.strCode AS UOM,
      
     
    trn_po_prn_details_sales_order.CONS_PC,
    trn_po_prn_details_sales_order.PRODUCTION_QTY,
    trn_po_prn_details_sales_order.REQUIRED,
    trn_orderdetails.dblPrice,
    trn_orderdetails.intQty as ORDER_QTY,
    trn_orderdetails.dblOverCutPercentage,
    trn_orderdetails.dblDamagePercentage,
    mst_customer.strName AS CUSTOMER_NAME,
    mst_brand.strName AS BRAND_NAME,
    trn_orderheader.intStatus as orderStatus,
    trn_orderheader.intLocationId as orderLocation,
    IF(trn_po_prn_details_sales_order.REQUIRED IS NOT NULL,'IN_SAMPLE_MOVED','NOT_IN_SAMPLE_BUT_MOVED')
				AS TYPE,
 trn_orderdetails.dtDeliveryDate,
 mst_technique_groups.TECHNIQUE_GROUP_NAME,
        (
            SELECT
                trn_orderheader_approvedby.dtApprovedDate
            FROM
                trn_orderheader_approvedby
            WHERE
                trn_orderheader_approvedby.intApproveLevelNo = \"-10\"
                AND trn_orderdetails.intOrderNo = trn_orderheader_approvedby.intOrderNo
                AND trn_orderdetails.intOrderYear = trn_orderheader_approvedby.intYear
            ORDER BY
                trn_orderheader_approvedby.dtApprovedDate DESC
            LIMIT 1
        ) AS COMPLETED_DATE
FROM
$tempory_rm_moved_orders_summery
LEFT JOIN trn_po_prn_details_sales_order_stock_transactions
ON  trn_po_prn_details_sales_order_stock_transactions.ORDER_NO= $tempory_rm_moved_orders_summery.`NO`
and trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = $tempory_rm_moved_orders_summery.`YEAR`
AND trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'ISSUE'


LEFT JOIN trn_po_prn_details_sales_order
on  trn_po_prn_details_sales_order_stock_transactions.ORDER_NO= trn_po_prn_details_sales_order.ORDER_NO
and trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
and  trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
and  trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
AND trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'ISSUE'
INNER JOIN trn_orderdetails
ON trn_orderdetails.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
AND trn_orderdetails.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
AND trn_orderdetails.intSalesOrderId = trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER

INNER JOIN trn_orderheader
ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear

INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId

INNER JOIN mst_item ON trn_po_prn_details_sales_order_stock_transactions.ITEM = mst_item.intId
LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier =  mst_supplier.intId
INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId

INNER JOIN mst_locations ON trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID= mst_locations.intId
INNER JOIN mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
INNER JOIN trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo
AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear
AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
INNER JOIN mst_technique_groups ON trn_orderdetails.TECHNIQUE_GROUP_ID = mst_technique_groups.TECHNIQUE_GROUP_ID GROUP BY
mst_item.intId, trn_orderdetails.intOrderNo,trn_orderdetails.intOrderYear,trn_orderdetails.intSalesOrderId,mst_locations.intId
 
) AS TB1";


    return $sql;
}

function getReportForSPlitedPos($tempory_rm_moved_orders_summery,$temp_lkr_value_item){
     $sql="SELECT TB1.*,

(SELECT SUM(costing_sample_header.INK_COST) FROM
costing_sample_header WHERE TB1.intSampleNo = costing_sample_header.SAMPLE_NO
AND TB1.intSampleYear = costing_sample_header.SAMPLE_YEAR
AND TB1.intRevisionNo = costing_sample_header.REVISION
AND TB1.strCombo = costing_sample_header.COMBO
AND TB1.strPrintName = costing_sample_header.PRINT
) AS INK_COST,
(SELECT SUM(costing_sample_header.SPECIAL_RM_COST) FROM
costing_sample_header WHERE TB1.intSampleNo = costing_sample_header.SAMPLE_NO
AND TB1.intSampleYear = costing_sample_header.SAMPLE_YEAR
AND TB1.intRevisionNo = costing_sample_header.REVISION
AND TB1.strCombo = costing_sample_header.COMBO
AND TB1.strPrintName = costing_sample_header.PRINT
) AS RM_COST, 

(SELECT  mst_locations.strName  FROM
trn_po_prn_details_sales_order_stock_transactions INNER JOIN mst_locations ON trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID = mst_locations.intId  WHERE TB1.DUMMY_PO_NO = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
AND TB1.DUMMY_PO_YEAR = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR LIMIT 1
) AS LOCATION,

0 AS AMOUNT
FROM

(
SELECT DISTINCT 
	$temp_lkr_value_item.LKR_VALUE AS UNIT_PRICE,
    trn_orderheader.intOrderNo,
    trn_orderheader.intOrderYear,
    trn_orderheader.DUMMY_PO_NO,
    trn_orderheader.DUMMY_PO_YEAR,
    trn_orderdetails.intSalesOrderId,
    trn_orderdetails.strSalesOrderNo,
    trn_orderdetails.strGraphicNo,
    trn_orderdetails.intSampleNo,
    trn_orderdetails.intSampleYear,
    trn_orderdetails.intRevisionNo,
    trn_orderdetails.strCombo,
    trn_orderdetails.strPrintName,
    mst_part.strName AS part, ".
//    mst_locations.intId AS LOCATION_ID,
//    mst_locations.strName AS LOCATION,
    "trn_orderdetails.intQty, ".
    "mst_maincategory.strName AS MAIN_CATEGORY,
    mst_subcategory.strName AS SUB_CATEGORY,
        mst_item.intId AS ITEM_ID,
    mst_item.strCode AS ITEM_CODE,
    CONCAT(mst_supplier.strCode,mst_item.strCode) as supItemCode,
    mst_item.strName AS ITEM_NAME,
    mst_units.strCode AS UOM, ".


    "trn_po_prn_details_sales_order.CONS_PC,
    trn_po_prn_details_sales_order.PRODUCTION_QTY,
    trn_po_prn_details_sales_order.REQUIRED,
  
    trn_orderdetails.dblPrice,
    mst_customer.strName AS CUSTOMER_NAME,
    mst_brand.strName AS BRAND_NAME,
    trn_orderheader.intStatus,
  'IN_SAMPLE_BUT_NOT_MOVED' as TYPE, 
				
IF (
    trn_orderheader.intStatus = 1,
    'Approved',

IF (
    trn_orderheader.intStatus = 0,
    'Rejected',

IF (
    trn_orderheader.intStatus =- 10,
    'Completed',

IF (
    trn_orderheader.intStatus =- 2,
    'Cancel',

IF (
    trn_orderheader.intStatus =- 1,
    'Revised',
    'Pending'
)
)
)
)
) AS STATUS,
 trn_orderdetails.dtDeliveryDate,
 mst_technique_groups.TECHNIQUE_GROUP_NAME,
        (
            SELECT
                trn_orderheader_approvedby.dtApprovedDate
            FROM
                trn_orderheader_approvedby
            WHERE
                trn_orderheader_approvedby.intApproveLevelNo = \"-10\"
                AND trn_orderdetails.intOrderNo = trn_orderheader_approvedby.intOrderNo
                AND trn_orderdetails.intOrderYear = trn_orderheader_approvedby.intYear
            ORDER BY
                trn_orderheader_approvedby.dtApprovedDate DESC
            LIMIT 1
        ) AS COMPLETED_DATE,
        	IF (
			trn_orderheader.INSTANT_ORDER = 1,
			'YES',
			'NO'
		) AS INSTANT_ORDER,
		CONCAT(trn_orderheader.DUMMY_PO_NO, \"/\",trn_orderheader.DUMMY_PO_YEAR) AS  DUMMY_NO,
		 (
      CASE 
        WHEN trn_orderheader.PO_TYPE = '0' THEN 'OTHER'
        WHEN trn_orderheader.PO_TYPE = '1' THEN 'DUMMY'
        ELSE 'SPLIT'
    END) AS PO_TYPE 
FROM
$tempory_rm_moved_orders_summery

INNER JOIN trn_orderheader ON trn_orderheader.DUMMY_PO_NO = $tempory_rm_moved_orders_summery.`NO` 
AND trn_orderheader.DUMMY_PO_YEAR = $tempory_rm_moved_orders_summery.`YEAR`
INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order.ORDER_NO = trn_orderheader.intOrderNo 
AND trn_po_prn_details_sales_order.ORDER_YEAR = trn_orderheader.intOrderYear 
INNER JOIN $temp_lkr_value_item ON trn_po_prn_details_sales_order.ITEM = $temp_lkr_value_item.intItemId 

INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId ".
"
INNER JOIN mst_item ON trn_po_prn_details_sales_order.ITEM = mst_item.intId
LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier =  mst_supplier.intId
INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId ".

//INNER JOIN mst_locations ON trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID= mst_locations.intId
"INNER JOIN mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
INNER JOIN trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo
AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear
AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
INNER JOIN mst_technique_groups ON trn_orderdetails.TECHNIQUE_GROUP_ID = mst_technique_groups.TECHNIQUE_GROUP_ID ".
"LEFT JOIN ware_stocktransactions_bulk ON trn_orderdetails.intOrderNo = ware_stocktransactions_bulk.intOrderNo
AND trn_orderdetails.intOrderYear = ware_stocktransactions_bulk.intOrderYear
AND trn_orderdetails.intSalesOrderId = ware_stocktransactions_bulk.intSalesOrderId 
WHERE ware_stocktransactions_bulk.id is NULL
 
) AS TB1";

     return $sql;
 }

 
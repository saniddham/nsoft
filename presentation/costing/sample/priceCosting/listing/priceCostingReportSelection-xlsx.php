<?php

//include_once "../../../dataAccess/Connector.php";

//$result2 = $db->RunQuery($report_sql);

header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=report.xls" );
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");

$totQty		= 0;
$totAmount 	= 0;
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
      <th bgcolor="#99CCFF" width="5%">Sample No</th>
      <th bgcolor="#99CCFF" width="5%">Revision No</th>
      <th bgcolor="#99CCFF" width="5%">Graphic</th>
      <th bgcolor="#99CCFF" width="6%">Print</th>
      <th bgcolor="#99CCFF" width="7%">Combo</th>
      <th bgcolor="#99CCFF" width="6%">Brand</th>
      <th bgcolor="#99CCFF" width="17%">Technique</th>
      <th bgcolor="#99CCFF" width="17%">Customer</th>
      <th bgcolor="#99CCFF" width="6%">Location</th>
      <th bgcolor="#99CCFF" width="6%">Company</th>
      <th bgcolor="#99CCFF" width="5%">Graphic H</th>
      <th bgcolor="#99CCFF" width="5%">Graphic W</th>
      <th bgcolor="#99CCFF" width="5%">RM Cost</th>
      <th bgcolor="#99CCFF" width="5%">Number of Shots</th>
      <th bgcolor="#99CCFF" width="5%">Print Price</th>
      <th bgcolor="#99CCFF" width="5%">Number of ups</th>
      <th bgcolor="#99CCFF" width="10%">Fabric Type</th>
      <th bgcolor="#99CCFF" width="10%">Copied From</th>
      <th bgcolor="#99CCFF" width="5%">Marketer</th>
      <th bgcolor="#99CCFF" width="5%">Marketing prepared by</th>
      <th bgcolor="#99CCFF" width="5%">Create User</th>
      <th class="tbheader"><div>Sample information sheet 2nd level approved date</div></th>
      <th bgcolor="#99CCFF" width="5%">1st level approved date</th>
      
  </tr>
<?php


while($row = mysqli_fetch_array($result))
{
			$fabricType = getFabricType($row['SAMPLE_NO'],$row['SAMPLE_YEAR'],$row['REVISION'],$row['COMBO'],$row['PRINT']);
			$marketer = getMarketer($row['MARKETER']);
            $price = $row['APPROVED_PRICE'];
            if ($row['QTY_PRICES'] ==1){
                $sql  = "SELECT max(PRICE) as maxPrice, min(PRICE) as minPrice FROM costing_sample_qty_wise
                                                                                WHERE
                                                                                    SAMPLE_NO 	=  '".$row['SAMPLE_NO']."' AND
                                                                                    SAMPLE_YEAR 	=  '".$row['SAMPLE_YEAR']."' AND
                                                                                    REVISION 	=  '".$row['REVISION']."' AND
                                                                                    PRINT  =  '".$row['PRINT']."' AND
                                                                                    COMBO 		=  '".$row['COMBO']."' 
                                                                        ";
                $price = "";
                $qtyPrices = $db->RunQuery($sql);
                while ($qtyRow = mysqli_fetch_array($qtyPrices)) {
                    $upperPrice = $qtyRow['maxPrice'];
                    $lowerPrice = $qtyRow['minPrice'];
                    $price .= $lowerPrice."-".$upperPrice;
                }
            }
	
?>
  <tr>
          <td style="display:none" id="sampleNo"><?php echo $row['SAMPLE_NO']; ?>&nbsp;</td>
          <td id="rev"><?php echo $row['REVISION']; ?></td>
          <td id="graphic"><?php echo $row['GRAPHIC']; ?>&nbsp;</td>
          <td id="print"><?php echo $row['PRINT']; ?>&nbsp;</td>
          <td id="combo"><?php echo $row['COMBO']; ?>&nbsp;</td>
          <td id="brand"><?php echo $row['BRAND']; ?>&nbsp;</td>
          <td id="customer"><?php echo $row['TECHNIQUE_NAME']; ?></td>
          <td id="customer"><?php echo $row['COUSTOMER']; ?>&nbsp;</td>
          <td id="customer"><?php echo $row['LOCATION']; ?>&nbsp;</td>
          <td id="customer"><?php echo $row['company']; ?>&nbsp;</td>
          <td id="graphic_w"><?php echo $row['GRAPHIC_W']; ?>&nbsp;</td>
          <td id="graphic_h"><?php echo $row['GRAPHIC_H']; ?>&nbsp;</td>
          <td id="cost"><?php echo $row['SPECIAL_RM_COST']; ?>&nbsp;</td>
          <td id="shots"><?php echo $row['SHOTS']; ?>&nbsp;</td>
          <td id="price"><?php echo $price; ?>&nbsp;</td>
          <td id="ups"><?php echo $row['UPS']; ?>&nbsp;</td>
          <td width="10%" id="fabrictype"><?php echo $fabricType; ?>&nbsp;</td>
          <td width="2%" id="fabrictype">
          <?php
		  if($row['COPPIED_FROM_SAMPLE_NO']!="")
		  { 
		  echo $row['COPPIED_FROM_SAMPLE_NO']."/". $row['COPPIED_FROM_SAMPLE_YEAR']."/".$row['COPPIED_FROM_REVISION']."/".$row['COPPIED_FROM_COMBO']."/".$row['COPPIED_FROM_PRINT']; 
		  }else
		  {
			  echo "";
		  }
          
          ?>
          &nbsp;
          </td>
          <td id="marketer"><?php echo $marketer; ?>&nbsp;</td>
          <td id="user marketer"><?php echo $row['USER_MARKETING']; ?></td>
          <td id="created user"><?php echo $row['CREATE_USER']; ?>&nbsp;</td>
          <td id="date"><?php echo $row['SECOND_LEVEL_APPROVAL']; ?></td>
          <td id="date"><?php echo $row['APPROVED_DATE']; ?></td>
           
  </tr>
<?php
	
	$totQty 	= $totQty+$row['Qty'];
	$totAmount 	= $totAmount+$row['Ammount'];
}

?>
</table>
<?php 
function getFabricType($sampleNo,$sampleYear,$revision,$combo,$print){

	global $db;
 
 	$sql			= "SELECT
					mst_fabrictype.strName AS fabricType
					FROM
					trn_sampleinfomations_details
					INNER JOIN mst_fabrictype ON trn_sampleinfomations_details.intFabricType = mst_fabrictype.intId
					WHERE
					trn_sampleinfomations_details.intSampleNo = '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear = '$sampleYear' AND
					trn_sampleinfomations_details.intRevNo = '$revision' AND
					trn_sampleinfomations_details.strPrintName = '$print' AND
					trn_sampleinfomations_details.strComboName = '$combo' limit 1";
	$result 		= $db->RunQuery($sql);
	$row 			= mysqli_fetch_array($result);
 	return $fabType		= $row['fabricType'];
	
}

function getMarketer($marketerUserId){

	global $db;
 
 	$sql			= "SELECT
					sys_users.strUserName AS MARKETER
					FROM
					sys_users
					WHERE
					sys_users.intUserId = '$marketerUserId' ";
	$result 		= $db->RunQuery($sql);
	$row 			= mysqli_fetch_array($result);
 	return $marketer		= $row['MARKETER'];
	
}
 ?>
<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 1/16/2018
 * Time: 10:10 AM
 */

$yrdata= strtotime($previous_month);
$f_date	=date('M-Y', $yrdata);
$fileName= 'Daily_RM_Movement_For_Month -'.$f_date.'	\.\xlsx';


header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=$fileName" );
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");

$reportTitle = 'Daily RM Movement For Month - (' . $previous_month . ' To ' . substr($end_date, 0, -9) . ') ';

?>

<table width="100%" border="1" cellspacing="0" cellpadding="0">
    <tr bgcolor="#C0C0C0"><td colspan="15"><strong><?php echo  $reportTitle; ?></strong></td><td colspan="21"></td></tr>
    <tr><td colspan="25"></td></tr>
    <tr bgcolor="#C0C0C0">

        <th width="5%">ORDER NO</th>
        <th width="5%">YEAR</th>
        <th width="5%">Customer PO NO</th>
        <th width="6%">SO</th>
        <th width="7%">GRAPHIC NO</th>
        <th width="6%">SAMPLE NO</th>
        <th width="17%">SAMPLE YEAR</th>
        <th width="17%">REVISION</th>
        <th width="6%">COMBO</th>
        <th width="6%">PRINT</th>
        <th width="5%">PART</th>
        <th width="5%">MAIN CATEGORY</th>
        <th width="5%">SUB CATEGORY</th>
        <th width="5%">ITEM CODE</th>
        <th width="5%">ITEM</th>
        <th width="5%">UOM</th>
        <th width="10%">LOCATION</th>
        <th width="10%">RM MOVED YEAR</th>
        <th width="5%">RM MOVED MONTH</th>
        <th width="5%">RM MOVED DATE</th>
        <th width="5%">DOCUMENT NO</th>
        <th width="5%">YEAR</th>
        <th width="5%">TYPE</th>
        <th width="5%">MRN NO</th>
        <th width="5%">MRN TYPE</th>
        <th width="5%">ACTUAL RM USAGE</th>
        <th width="5%">ACTUAL RM USAGE VALUE (LKR) </th>
    </tr>

 
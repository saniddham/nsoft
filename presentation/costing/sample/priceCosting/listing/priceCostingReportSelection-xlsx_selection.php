<?php
ini_set('display_errors',0);
	session_start();
	include "../../../../../libraries/pdf-php/class.ezpdf.php";
	include "../../../../../libraries/fpdf/fpdf.php";
	include "../../../../../dataAccess/Connector.php";
	$pdf = new Cezpdf('a4');
	
	$type = $_REQUEST['type'];
//	$type = 'POSummery';
$userId1 				= $_SESSION['userId'];
	


if($type=='costing_to_excel')
{
	$dateFrom 		= $_REQUEST['dateForm'];
	$dateTo 		= $_REQUEST['dateTo'];
	$hourFrom 		= $_REQUEST['hourFrom'];
	$minFrom 		= $_REQUEST['minFrom'];
	$hourTo 		= $_REQUEST['hourTo'];
	$minTo 			= $_REQUEST['minTo'];
	$userId 		= $_REQUEST['userId'];
	$viewImg 		= $_REQUEST['viewImg'];

	if($minFrom=='')
		$minFrom	='00';
	if($minTo=='')
		$minTo		='00';
	
	$from =$dateFrom.' '.$hourFrom.':'.$minFrom.':00';
	$to =$dateTo.' '.$hourTo.':'.$minTo.':00';

	$pdfLandscape = new Cezpdf('a4','landscape');
	
	
	
	$sql = "SELECT
				concat(costing_sample_header.SAMPLE_NO,'/',costing_sample_header.SAMPLE_YEAR) as SAMPLE_NO,
				costing_sample_header.PRINT,
				costing_sample_header.COMBO,
				costing_sample_header.SHOTS,
				costing_sample_header.UPS,
				costing_sample_header.QTY_PRICES,
				costing_sample_header.SAVED_TIME,
				costing_sample_header.SAMPLE_YEAR,
				costing_sample_header.REVISION,
				costing_sample_header.COPPIED_FROM_SAMPLE_NO,
                costing_sample_header.COPPIED_FROM_SAMPLE_YEAR,
                costing_sample_header.COPPIED_FROM_COMBO,
                costing_sample_header.COPPIED_FROM_PRINT,
                costing_sample_header.COPPIED_FROM_REVISION,
				trn_sampleinfomations.strGraphicRefNo as GRAPHIC,
				'View' AS `View`,
				mst_companies.strName as company,
				mst_customer.strName AS COUSTOMER,
				mst_brand.strName AS BRAND,
				trn_sampleinfomations_printsize.intWidth AS GRAPHIC_W,
				trn_sampleinfomations_printsize.intHeight AS GRAPHIC_H,
				costing_sample_header.SPECIAL_RM_COST,
				costing_sample_header.APPROVED_PRICE,
				mst_locations.strName AS LOCATION,
				sys_users.strUserName as CREATE_USER,
				user_marketing.strUserName as USER_MARKETING, 
				mst_marketer.intUserId AS MARKETER,
				costing_sample_header_approved_by.APPROVED_DATE,
                                (SELECT GROUP_CONCAT(trn_sampleinfomations_approvedby.dtApprovedDate)
FROM
trn_sampleinfomations_approvedby
WHERE
trn_sampleinfomations_approvedby.intStage = 2
AND 
trn_sampleinfomations_approvedby.intSampleNo = trn_sampleinfomations.intSampleNo
AND
trn_sampleinfomations_approvedby.intSampleYear = trn_sampleinfomations.intSampleYear
AND trn_sampleinfomations_approvedby.intRevNo = trn_sampleinfomations.intRevisionNo) as SECOND_LEVEL_APPROVAL,
					(SELECT
					GROUP_CONCAT(distinct mst_techniques.strName)
					FROM
					trn_sampleinfomations_details
					INNER JOIN mst_techniques ON trn_sampleinfomations_details.intTechniqueId = mst_techniques.intId
					WHERE trn_sampleinfomations_details.intSampleNo = costing_sample_header.SAMPLE_NO AND trn_sampleinfomations_details.intSampleYear = costing_sample_header.SAMPLE_YEAR AND trn_sampleinfomations_details.intRevNo = costing_sample_header.REVISION AND trn_sampleinfomations_details.strComboName = costing_sample_header.COMBO AND trn_sampleinfomations_details.strPrintName = costing_sample_header.PRINT
					 ) as TECHNIQUE_NAME   
				FROM
				costing_sample_header
				INNER JOIN trn_sampleinfomations ON costing_sample_header.SAMPLE_NO = trn_sampleinfomations.intSampleNo AND costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations.intSampleYear AND costing_sample_header.REVISION = trn_sampleinfomations.intRevisionNo
				INNER JOIN mst_marketer ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
				INNER JOIN mst_customer ON trn_sampleinfomations.intCustomer = mst_customer.intId
				INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
				INNER JOIN trn_sampleinfomations_printsize ON costing_sample_header.SAMPLE_NO = trn_sampleinfomations_printsize.intSampleNo AND costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations_printsize.intSampleYear AND costing_sample_header.REVISION = trn_sampleinfomations_printsize.intRevisionNo AND costing_sample_header.PRINT = trn_sampleinfomations_printsize.strPrintName
				INNER JOIN mst_locations ON costing_sample_header.PRODUCTION_LOCATION = mst_locations.intId
				INNER JOIN sys_users ON costing_sample_header.SAVED_BY = sys_users.intUserId 
				INNER JOIN mst_companies ON mst_companies.intId = mst_locations.intCompanyId 
				INNER JOIN costing_sample_header_approved_by on 
                costing_sample_header.SAMPLE_NO = costing_sample_header_approved_by.SAMPLE_NO AND
                costing_sample_header.SAMPLE_YEAR = costing_sample_header_approved_by.SAMPLE_YEAR AND
                costing_sample_header.REVISION = costing_sample_header_approved_by.REVISION_NO AND
                costing_sample_header.COMBO = costing_sample_header_approved_by.COMBO AND
                costing_sample_header.PRINT = costing_sample_header_approved_by.PRINT  AND
                costing_sample_header_approved_by.STATUS = 0  
				INNER JOIN sys_users as user_marketing ON trn_sampleinfomations.intCreator = user_marketing.intUserId

				WHERE
				DATE(costing_sample_header_approved_by.APPROVED_DATE)  >= date('$from')
				and 
				DATE(costing_sample_header_approved_by.APPROVED_DATE)  <= date('$to')
				and costing_sample_header.`STATUS` = 1 ";
				
if($userId!=''){
	$sql	.=" and costing_sample_header.SAVED_BY = '$userId'";
}
				$sql	.=" GROUP BY
				costing_sample_header.SAMPLE_NO,
				costing_sample_header.SAMPLE_YEAR,
				costing_sample_header.REVISION,
				costing_sample_header.COMBO,
				costing_sample_header.PRINT";
	
	//if($userId1==2)
		//echo $sql;
	
	$result = $db->RunQuery($sql);
	include "priceCostingReportSelection-xlsx.php";
	return;
} 
//// end nuwan
	

function GetMainDetails_loanIn($company,$location,$item,$dtFrom,$dtTo)
{
	global $db;
	
		
	$sql = "SELECT
ware_grnheader.datdate AS grnDate,
mst_companies.strName as grnCompany,
concat(ware_grnheader.intGrnNo,'/',ware_grnheader.intGrnYear) as grnNo,
mst_item.strName as item,
mst_units.strName as uom,
ware_grndetails.dblGrnQty,
ware_grndetails.dblGrnRate,
substring(ware_returntosupplierheader.datdate,1,10) as returnDate,
concat(ware_returntosupplierheader.intReturnNo,'/',ware_grnheader.intGrnYear) as retNo,
ware_returntosupplierdetails.dblQty as retQty 
FROM
trn_poheader
Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
Inner Join ware_grnheader ON trn_poheader.intPONo = ware_grnheader.intPoNo AND trn_poheader.intPOYear = ware_grnheader.intPoYear
Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear
Inner Join mst_item ON ware_grndetails.intItemId = mst_item.intId
Inner Join mst_locations ON mst_supplier.intInterLocation = mst_locations.intId
Inner Join mst_companies ON mst_supplier.intCompanyId = mst_companies.intId
Inner Join mst_units ON mst_item.intUOM = mst_units.intId
left Join ware_returntosupplierheader ON ware_grndetails.intGrnNo = ware_returntosupplierheader.intGrnNo AND ware_grndetails.intGrnYear = ware_returntosupplierheader.intGrnYear and ware_returntosupplierheader.intStatus='1' 
left Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear AND ware_grndetails.intItemId = ware_returntosupplierdetails.intItemId
WHERE
mst_supplier.intInterCompany =  '1' AND
ware_grnheader.intStatus =  '1' AND 
ware_grnheader.intCompanyId =  '$location' ";
if($item!='')
$sql .= " AND ware_grndetails.intItemId =  '$item' ";


if($dtFrom!='')
$sql .= " AND date(ware_grnheader.datdate) >='$dtFrom' ";
if($dtTo!='')
$sql .= " AND date(ware_grnheader.datdate) <='$dtTo' "; 
$sql .= " 
ORDER BY
ware_grnheader.intGrnNo ASC,
ware_grnheader.intGrnYear ASC,
mst_item.strName ASC,
ware_returntosupplierheader.intReturnNo ASC,
ware_returntosupplierheader.intReturnYear ASC
";

//echo $sql;
	return $db->RunQuery($sql);
}
//-----------------------------------------------------------
function GetMainDetails_loanOut($company,$location,$item,$dtFrom,$dtTo)
{
	global $db;
	
		
	$sql = "select 
 tb1.dtmPODate, 
tb1.poCompany, 
tb1.intPONo,
tb1.intPOYear, 
tb1.item, 
tb1.uom, 
tb1.dblGRNRate, 
tb1.intItemId, 
tb1.poQty , 
tb1.rcds , 
ware_returntosupplierheader.intReturnNo,
ware_returntosupplierheader.intReturnYear , 
substr(ware_returntosupplierheader.datdate,1,10) as datdate, 
ware_returntosupplierdetails.dblQty 
 from (SELECT 
substr(trn_poheader.dtmPODate,1,10) as dtmPODate,
mst_companies.strName as poCompany,
trn_poheader.intPONo,
trn_poheader.intPOYear,
mst_item.strName as item,
ware_invoicedetails.intItemId,
mst_units.strName as uom,
ware_stocktransactions_bulk.dblGRNRate,
Sum(ware_invoicedetails.dblQty) as poQty, 
count(ware_invoicedetails.intItemId) as rcds 
FROM
trn_poheader  
Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
Inner Join trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
Inner Join ware_invoiceheader ON trn_podetails.intPONo = ware_invoiceheader.intPONo AND trn_podetails.intPOYear = ware_invoiceheader.intPOYear
Inner Join ware_invoicedetails ON ware_invoiceheader.intInvoiceNo = ware_invoicedetails.intInvoiceNo AND ware_invoiceheader.intInvoiceYear = ware_invoicedetails.intInvoiceYear AND trn_podetails.intItem = ware_invoicedetails.intItemId
Inner Join mst_item on ware_invoicedetails.intItemId = mst_item.intId  
Inner Join mst_locations ON trn_poheader.intCompany = mst_locations.intId
Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
Inner Join mst_units ON mst_item.intUOM = mst_units.intId
Inner Join ware_stocktransactions_bulk ON ware_invoiceheader.intCompanyId = ware_stocktransactions_bulk.intLocationId AND ware_invoicedetails.intInvoiceNo = ware_stocktransactions_bulk.intDocumentNo AND ware_invoicedetails.intInvoiceYear = ware_stocktransactions_bulk.intDocumntYear AND ware_invoicedetails.intItemId = ware_stocktransactions_bulk.intItemId 
Inner Join ware_grnheader ON trn_poheader.intPONo = ware_grnheader.intPoNo AND trn_poheader.intPOYear = ware_grnheader.intPoYear
Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND trn_podetails.intItem = ware_grndetails.intItemId 
WHERE 
mst_supplier.intInterCompany =  '1' AND
mst_supplier.intCompanyId =  '$company' AND
mst_supplier.intInterLocation =  '$location' AND
trn_poheader.intStatus =  '1' AND   
ware_invoiceheader.intStatus =  '1' 
GROUP BY
trn_poheader.intPONo,
trn_poheader.intPOYear,
ware_invoicedetails.intItemId  ) as tb1 
Inner Join ware_grnheader ON tb1.intPONo = ware_grnheader.intPoNo AND tb1.intPOYear = ware_grnheader.intPoYear
Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND tb1.intItemId = ware_grndetails.intItemId
Left Join ware_returntosupplierheader ON ware_grndetails.intGrnNo = ware_returntosupplierheader.intGrnNo AND ware_grndetails.intGrnYear = ware_returntosupplierheader.intGrnYear
Left Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear AND ware_grndetails.intItemId = ware_returntosupplierdetails.intItemId
where 1=1  ";
if($item!='')
$sql .= " AND tb1.intItemId =  '$item' ";
if($dtFrom!='')
$sql .= " AND date(tb1.dtmPODate) >='$dtFrom' ";
if($dtTo!='')
$sql .= " AND date(tb1.dtmPODate) <='$dtTo' "; 
$sql .= " 
ORDER BY
tb1.intPONo ASC,
tb1.intPOYear ASC,
tb1.item ASC,
ware_returntosupplierheader.intReturnNo ASC,
ware_returntosupplierheader.intReturnYear ASC
";
//  echo   $sql;

	return $db->RunQuery($sql);
}
//------------------------------function load Header---------------------
function loadCurrency($company,$date,$currency) 
{
	global $db;
		  $sql = "SELECT
					mst_financeexchangerate.dblBuying,
					mst_financecurrency.strCode
					FROM
					mst_financeexchangerate
					Inner Join mst_financecurrency ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId
					WHERE
					mst_financeexchangerate.intCompanyId =  '$company' AND
					mst_financeexchangerate.intCurrencyId =  '$currency' AND
					mst_financeexchangerate.dtmDate =  '$date'";
				$result = $db->RunQuery($sql);
				$row=mysqli_fetch_array($result);
				return (float)($row['dblBuying']);
}
//------------------------------function load Header---------------------
function loadCurrencyName($currency) 
{
	global $db;
		    $sql = "SELECT mst_financecurrency.strCode
					FROM
					mst_financecurrency
					WHERE
					mst_financecurrency.intId =  '$currency'";
				$result = $db->RunQuery($sql);
				$row=mysqli_fetch_array($result);
				return $row['strCode'];
}
//-----------------------------------------------
function loadSupplierName($supplier){
	global $db;
		    $sql = "SELECT
						mst_supplier.intId,
						mst_supplier.strName
						FROM mst_supplier
						WHERE
					mst_supplier.intId =  '$supplier'";
				$result = $db->RunQuery($sql);
				$row=mysqli_fetch_array($result);
				return $row['strName'];
}
//-----------------------------------------------
function loadLocationName($location){
	global $db;
		    $sql = "SELECT
					mst_locations.strName
					FROM mst_locations
					WHERE
					mst_locations.intId =  '$location'";
				$result = $db->RunQuery($sql);
				$row=mysqli_fetch_array($result);
				return $row['strName'];
}
//-----------------------------------------------
function loadDepartmentName($department){
	global $db;
		    $sql = "SELECT
					mst_department.strName
					FROM mst_department
					WHERE
					mst_department.intId =  '$department'";
				$result = $db->RunQuery($sql);
				$row=mysqli_fetch_array($result);
				return $row['strName'];
}

?>
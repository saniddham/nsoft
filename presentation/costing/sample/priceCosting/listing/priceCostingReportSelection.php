<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId 				= $_SESSION['CompanyID'];
//$userId 				= $_SESSION['userId'];
$userId1 				= $_SESSION['userId'];
$locationId 			= $companyId;
$programCode			= 'P0116';

include_once  "class/cls_commonErrorHandeling_get.php";

$obj_commonErrHandle 	= new cls_commonErrorHandeling_get($db);

$dateFrom 		= $_REQUEST['dateForm'];
$dateTo 		= $_REQUEST['dateTo'];
$hourFrom 		= $_REQUEST['hourFrom'];
$minFrom 		= $_REQUEST['minFrom'];
$hourTo 		= $_REQUEST['hourTo'];
$minTo 			= $_REQUEST['minTo'];
$userId 		= $_REQUEST['userId'];
$viewImg 		= $_REQUEST['viewImg'];

if($minFrom=='')
	$minFrom	='00';
if($minTo=='')
	$minTo		='00';

$from =$dateFrom.' '.$hourFrom.':'.$minFrom.':00';
$to =$dateTo.' '.$hourTo.':'.$minTo.':00';

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Price Costing Report</title>
        <script type="text/javascript"
                src="presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder-js.js?n=1"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/excellentexport@2.1.0/dist/excellentexport.min.js"></script>
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css"/>
        <style type="text/css">
            .break {
                page-break-before: always;
            }

            @media print {
                .noPrint {
                    display: none;
                }
            }

            #apDiv1 {
                position: absolute;
                left: 237px;
                top: 175px;
                width: 619px;
                height: 235px;
                z-index: 1;
            }

            .APPROVE {
                font-size: 16px;
                font-weight: bold;
                color: #00C;
            }
            .foo {
                float: left;
                width: 20px;
                height: 20px;
                margin: 1px;
                border: 1px solid rgba(0, 0, 0, .2);
            }

            .blue {
                background: #71ede4;
            }
            .red {
                background: #FF99CC;
            }

            /*div.dataTables_wrapper {*/
            /*width: 800px;*/
            /*margin: 0 auto;*/
            /*white-space: nowrap;*/
            /*}*/


            div.dataTables_length {
                padding-left: 2em;
            }
            div.dataTables_length,
            div.dataTables_filter {
                padding-top: 0.55em;
            }
            div.dt-buttons{
                position:relative !important;
                float:left !important;
            }
            #tblMainGrid_paginate{
                position: absolute;
                top: -54px;
                right: 1009px;
                width: 436px;
                height: 100px;
            }

            .type_2 {
                background-color:  #FF99CC !important;
            }
            .pending_mrn{
                background-color: #2affe4 !important;
            }

        </style>
        <script type="application/javascript" src="presentation/costing/sample/priceCosting/listing/priceCostingReportSelection-js.js"></script>
        <script src="libraries/dataTables/jquery.dataTables.min.js"></script>
        <script src="libraries/dataTables/dataTables.buttons.min.js"></script>
        <script src="libraries/dataTables/buttons.flash.min.js"></script>
        <script src="libraries/dataTables/jszip.min.js"></script>
        <script src="libraries/dataTables/pdfmake.min.js"></script>
        <script src="libraries/dataTables/vfs_fonts.js"></script>
        <script src="libraries/dataTables/buttons.html5.min.js"></script>
        <script src="libraries/dataTables/buttons.print.min.js "></script>
        <link rel="stylesheet" type="text/css" href="libraries/dataTables/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="libraries/dataTables/css/buttons.dataTables.min.css">
        <script type="text/javascript">
            $(document).ready(function() {
                var dateFrom ="<?php echo $dateFrom; ?>";
                var dateTo ="<?php echo $dateTo; ?>";
                
                var table =$('#tblMainGrid').DataTable( {
                    pageLength: 50,
                    "bFilter": false,
                    responsive: true,
                    "scrollY": 600,
                    "scrollX": true,
                    dom: 'Bfrtip',
                    buttons: [{
                        extend: 'print',
                        text: 'Print',
                        exportOptions: {
                            columns: ':not(.no-print)'
                        },
                        footer: true,
                        autoPrint: false
                    }, {

                        extend: 'pdfHtml5',
                        messageTop: 'Price Costing Report',
                        orientation: 'landscape',
                        pageSize: 'LEGAL',
                        text: 'PDF',


                    },{

                        extend: 'excel',
                        messageTop: dateFrom+'To'+dateTo,
                        text: 'Excel',

                    },
                        {

                            extend: 'csv',
                            text: 'CSV',
                            customize: function (csv) {

                                return '\t Price Costing Report\n\n'+  csv;
                            },
                            exportOptions: {
                                columns: ':not(.no-print)'
                            }


                        }],



                });



            } );


        </script>


    </head>

    <body>
    <form id="frmPriceCostingReport" name="frmPriceCostingReport" method="post">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="3"></td>
            </tr>
            <tr>
                <td width="20%"></td>
                <td width="60%" height="80"
                    valign="top"><?php include 'presentation/procurement/reports/reportHeader.php' ?></td>
                <td width="20%"></td>
            </tr>
            <tr>

                <td colspan="3" style="font-weight: bold">
                    <strong> Sample Date From -:</strong>&nbsp;<strong><?php echo $dateFrom; ?></strong> &nbsp;&nbsp;&nbsp;<strong>To -:</strong>&nbsp;<strong><?php echo $dateTo ; ?></strong>


                </td>
            </tr>
        </table>
        <div align="center">
            <div style="background-color:#FFF"><strong>Price Costing Report</strong><strong></strong></div>
            <table width="900" border="0" align="center" bgcolor="#FFFFFF">
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td colspan="10" align="center" bgcolor="#FFFFFF">
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br/>
                        <table width="100%">
                            <tr>
                                <td colspan="20">
                                    <div>

                                        <table width="100%" class="table table-striped table-bordered bordered" id="tblMainGrid" cellspacing="0"
                                               cellpadding="0">
                                            <thead>
                                            <tr class="">
                                                <?php if($viewImg==1)  { ?>
                                                <th class="tbheader"><div>Image</div></th>
                                                <?php } ?>
                                                <th class="tbheader"><div>Sample No</div></th>
                                                <th class="tbheader"><div>Revision No</div></th>
                                                <th class="tbheader"><div>Graphic</div></th>
                                                <th  class="tbheader"><div>Print</div></th>
                                                <th class="tbheader"><div>Combo</div></th>
                                                <th class="tbheader"><div>Brand</div></th>
                                                <th class="tbheader"><div>Technique</div></th>
                                                <th class="tbheader"><div>Customer</div></th>
                                                <th class="tbheader"><div>Location</div>                </th>
                                                <th class="tbheader"><div>Company</div></th>
                                                <th class="tbheader"><div>Graphic H</div></th>
                                                <th  class="tbheader"><div>Graphic W</div></th>
                                                <th class="tbheader"><div>RM Cost</div></th>
                                                <th class="tbheader"><div>Number of Shots</div></th>
                                                <th  class="tbheader"><div>Print Price</div></th>
                                                <th class="tbheader"><div>Number of ups</div></th>
                                                <th class="tbheader"><div>Number of colors</div></th>
                                                <th class="tbheader"><div>Fabric Type</div></th>
                                                <th class="tbheader" ><div>Copied From</div></th>
                                                <th class="tbheader"><div>Marketer</div></th>
                                                <th class="tbheader"><div>Create User</div></th>
                                                <th class="tbheader"><div>Marketing prepared by</div></th>
                                                <th class="tbheader"><div>Sample information sheet 2nd level approved date</div></th>
                                                <th class="tbheader"><div>1st level approved date</div></th>
                                                
                                                <th class="tbheader"><div>View Costing</div></th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $sql= getReportData($userId,$from,$to);
                                            $result_tech = $db->RunQuery($sql);
                                            while($row=mysqli_fetch_array($result_tech)) {
                                                $fabricType = getFabricType($row['SAMPLE_NO'],$row['SAMPLE_YEAR'],$row['REVISION'],$row['COMBO'],$row['PRINT']);
                                                $marketer = getMarketer($row['MARKETER']);
                                                $price = $row['APPROVED_PRICE'];

                                                if ($row['QTY_PRICES'] ==1){
                                                    $sql  = "SELECT max(PRICE) as maxPrice, min(PRICE) as minPrice FROM costing_sample_qty_wise
                                                                        WHERE
                                                                            SAMPLE_NO 	=  '".$row['SAMPLE_NO']."' AND
                                                                            SAMPLE_YEAR 	=  '".$row['SAMPLE_YEAR']."' AND
                                                                            REVISION 	=  '".$row['REVISION']."' AND
                                                                            PRINT  =  '".$row['PRINT']."' AND
                                                                            COMBO 		=  '".$row['COMBO']."' 
                                                                ";
                                                    $price = "";
                                                    $qtyPrices = $db->RunQuery($sql);
                                                    while ($qtyRow = mysqli_fetch_array($qtyPrices)) {
                                                        $upperPrice = $qtyRow['maxPrice'];
                                                        $lowerPrice = $qtyRow['minPrice'];
                                                        $price .= $lowerPrice."-".$upperPrice;
                                                    }
                                                }

                                                ?>
                                                <tr>
                                                    <?php if($viewImg == 1 ){
                                                        if($row['SAMPLE_NO']!=''){
                                                            ?>
                                                            <td><?php echo "<img id=\"saveimg\" style=\"width:264px;height:100px;\" src=\"../../../../../documents/sampleinfo/samplePictures/".$row['SAMPLE_NO']."-".$row['SAMPLE_YEAR']."-".$row['REVISION']."-".substr($row['PRINT'],6,1).".png\" />"; ?>
                                                            </td>
                                                        <?php } } ?>
                                                    <?php $sampleNo= $row['SAMPLE_NO'].'/'.$row['SAMPLE_YEAR']; ?>
                                                    <td id="sampleData"><?php echo $sampleNo;?></td>
                                                    <td id="rev"><?php echo $row['REVISION']; ?></td>
                                                    <td id="graphic"><?php echo $row['GRAPHIC']; ?></td>
                                                    <td id="print"><?php echo $row['PRINT']; ?></td>
                                                    <td id="combo" ><?php echo $row['COMBO']; ?></td>
                                                    <td><?php echo $row['BRAND']; ?></td>
                                                    <td><?php echo $row['TECHNIQUE_NAME']; ?></td>
                                                    <td><?php echo $row['COUSTOMER']; ?></td>
                                                    <td><?php echo $row['LOCATION']; ?></td>
                                                    <td><?php echo $row['company']; ?></td>
                                                    <td><?php echo $row['GRAPHIC_W']; ?></td>
                                                    <td><?php echo $row['GRAPHIC_H']; ?></td>
                                                    <td><?php echo $row['SPECIAL_RM_COST']; ?></td>
                                                    <td><?php echo $row['SHOTS']; ?></td>
                                                    <td><?php echo $price; ?></td>
                                                    <td><?php echo $row['UPS']; ?></td>
                                                    <td><?php echo $row['NO_OF_COLOURS']; ?></td>
                                                    <td><?php echo $fabricType; ?></td>
                                                    <td><?php
                                                        if($row['COPPIED_FROM_SAMPLE_NO']!="")
                                                        {
                                                            echo $row['COPPIED_FROM_SAMPLE_NO']."/". $row['COPPIED_FROM_SAMPLE_YEAR']."/".$row['COPPIED_FROM_REVISION']."/".$row['COPPIED_FROM_COMBO']."/".$row['COPPIED_FROM_PRINT'];
                                                        }else
                                                        {
                                                            echo "&nbsp;";
                                                        }?></td>
                                                    <td><?php echo $marketer; ?></td>
                                                    <td><?php echo $row['CREATE_USER']; ?></td>
                                                    <td><?php echo $row['USER_MARKETING']; ?></td>
                                                    <td><?php echo $row['SECOND_LEVEL_APPROVAL']; ?></td>
                                                    <td><?php echo $row['APPROVED_DATE']; ?></td>
                                                     
                                                    <td><a id="butcurrentView" class="button green small butcurrentView" style="" name="butcurrentView" on=""> View </a></td>


                                                </tr>


                                            <?php } ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
    </body>
    </html>
<?php

function getReportData($userId,$from,$to){
	
	global $userId1;

    $sql = "SELECT
				costing_sample_header.SAMPLE_NO,
				costing_sample_header.PRINT,
				costing_sample_header.COMBO,
				costing_sample_header.SHOTS,
				costing_sample_header.UPS,
				costing_sample_header.NO_OF_COLOURS,
				costing_sample_header.SAVED_TIME,
				costing_sample_header.SAMPLE_YEAR,
				costing_sample_header.REVISION,
				costing_sample_header.COPPIED_FROM_SAMPLE_NO,
                costing_sample_header.COPPIED_FROM_SAMPLE_YEAR,
                costing_sample_header.COPPIED_FROM_COMBO,
                costing_sample_header.COPPIED_FROM_PRINT,
                costing_sample_header.COPPIED_FROM_REVISION,
				trn_sampleinfomations.strGraphicRefNo as GRAPHIC,
				'View' AS `View`,
				mst_companies.strName as company,
				mst_customer.strName AS COUSTOMER,
				mst_brand.strName AS BRAND,
				trn_sampleinfomations_printsize.intWidth AS GRAPHIC_W,
				trn_sampleinfomations_printsize.intHeight AS GRAPHIC_H,
				costing_sample_header.SPECIAL_RM_COST,
				costing_sample_header.APPROVED_PRICE,
				costing_sample_header.QTY_PRICES,
				mst_locations.strName AS LOCATION,
				sys_users.strUserName as CREATE_USER,
				user_marketing.strUserName as USER_MARKETING, 
				mst_marketer.intUserId AS MARKETER,
				costing_sample_header_approved_by.APPROVED_DATE,
                                (SELECT GROUP_CONCAT(trn_sampleinfomations_approvedby.dtApprovedDate)
FROM
trn_sampleinfomations_approvedby
WHERE
trn_sampleinfomations_approvedby.intStage = 2
AND 
trn_sampleinfomations_approvedby.intSampleNo = trn_sampleinfomations.intSampleNo
AND
trn_sampleinfomations_approvedby.intSampleYear = trn_sampleinfomations.intSampleYear
AND trn_sampleinfomations_approvedby.intRevNo = trn_sampleinfomations.intRevisionNo) as SECOND_LEVEL_APPROVAL,
					(SELECT
					GROUP_CONCAT(distinct mst_techniques.strName)
					FROM
					trn_sampleinfomations_details
					INNER JOIN mst_techniques ON trn_sampleinfomations_details.intTechniqueId = mst_techniques.intId
					WHERE trn_sampleinfomations_details.intSampleNo = costing_sample_header.SAMPLE_NO AND trn_sampleinfomations_details.intSampleYear = costing_sample_header.SAMPLE_YEAR AND trn_sampleinfomations_details.intRevNo = costing_sample_header.REVISION AND trn_sampleinfomations_details.strComboName = costing_sample_header.COMBO AND trn_sampleinfomations_details.strPrintName = costing_sample_header.PRINT
					 ) as TECHNIQUE_NAME   
				FROM
				costing_sample_header
				INNER JOIN trn_sampleinfomations ON costing_sample_header.SAMPLE_NO = trn_sampleinfomations.intSampleNo AND costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations.intSampleYear AND costing_sample_header.REVISION = trn_sampleinfomations.intRevisionNo
				INNER JOIN mst_marketer ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
				INNER JOIN mst_customer ON trn_sampleinfomations.intCustomer = mst_customer.intId
				INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
				left JOIN trn_sampleinfomations_printsize ON costing_sample_header.SAMPLE_NO = trn_sampleinfomations_printsize.intSampleNo AND costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations_printsize.intSampleYear AND costing_sample_header.REVISION = trn_sampleinfomations_printsize.intRevisionNo AND costing_sample_header.PRINT = trn_sampleinfomations_printsize.strPrintName
				INNER JOIN mst_locations ON costing_sample_header.PRODUCTION_LOCATION = mst_locations.intId
				INNER JOIN sys_users ON costing_sample_header.SAVED_BY = sys_users.intUserId 
				INNER JOIN mst_companies ON mst_companies.intId = mst_locations.intCompanyId 
				INNER JOIN costing_sample_header_approved_by on 
                costing_sample_header.SAMPLE_NO = costing_sample_header_approved_by.SAMPLE_NO AND
                costing_sample_header.SAMPLE_YEAR = costing_sample_header_approved_by.SAMPLE_YEAR AND
                costing_sample_header.REVISION = costing_sample_header_approved_by.REVISION_NO AND
                costing_sample_header.COMBO = costing_sample_header_approved_by.COMBO AND
                costing_sample_header.PRINT = costing_sample_header_approved_by.PRINT  AND
                costing_sample_header_approved_by.STATUS = 0  
				
				INNER JOIN sys_users as user_marketing ON trn_sampleinfomations.intCreator = user_marketing.intUserId

				WHERE 
				DATE(costing_sample_header_approved_by.APPROVED_DATE)  >= date('$from')
				and 
				DATE(costing_sample_header_approved_by.APPROVED_DATE)  <= date('$to')
				AND costing_sample_header.`STATUS` = 1 ";

    if($userId!=''){
        $sql	.=" and costing_sample_header.SAVED_BY = '$userId'";
    }
    $sql	.=" GROUP BY
				costing_sample_header.SAMPLE_NO,
				costing_sample_header.SAMPLE_YEAR,
				costing_sample_header.REVISION,
				costing_sample_header.COMBO,
				costing_sample_header.PRINT";
	
		//if($userId1==2)
		//echo $sql;

	
    return $sql;
}

function getFabricType($sampleNo,$sampleYear,$revision,$combo,$print){

	global $db;
 
 	$sql			= "SELECT
					mst_fabrictype.strName AS fabricType
					FROM
					trn_sampleinfomations_details
					INNER JOIN mst_fabrictype ON trn_sampleinfomations_details.intFabricType = mst_fabrictype.intId
					WHERE
					trn_sampleinfomations_details.intSampleNo = '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear = '$sampleYear' AND
					trn_sampleinfomations_details.intRevNo = '$revision' AND
					trn_sampleinfomations_details.strPrintName = '$print' AND
					trn_sampleinfomations_details.strComboName = '$combo' limit 1";
	$result 		= $db->RunQuery($sql);
	$row 			= mysqli_fetch_array($result);
 	return $fabType		= $row['fabricType'];
	
}

function getMarketer($marketerUserId){

	global $db;
 
 	$sql			= "SELECT
					sys_users.strUserName AS MARKETER
					FROM
					sys_users
					WHERE
					sys_users.intUserId = '$marketerUserId' ";
	$result 		= $db->RunQuery($sql);
	$row 			= mysqli_fetch_array($result);
 	return $marketer		= $row['MARKETER'];
	
}
 ?>
<?php
ini_set('display_errors', '0');
	session_start();
	$user_id= $_SESSION['userId'];
	if($_SESSION['userId']=='')
        die("Temporary unavailable. Please contact the admin");

/**
 * Created by PhpStorm.
 * User: HASITHA
 * Date: 12/8/2017
 * Time: 12:32 PM
 */
//ini_set('display_errors', 'on');
//set_time_limit(1000);
//ini_set('max_execution_time',80000);
//session_start();



$backwardseperator = "../../../../../";


/*
 * CREATE DB CONNECTION
 */
$servername = $_SESSION['Server'];
$username = $_SESSION['UserName'];
$password = $_SESSION['Password'];
$dbname = $_SESSION['Database'];

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
//mysql_set_charset('utf8',$conn);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


if($_GET['startDate']) {
    $date = $_GET['startDate'];
    $time = strtotime($date);
    $previous_month = date('Y-m-d', $time);

//get one month from specific date
    $current_month = date('Y-m-d', strtotime('+1 month', strtotime($previous_month)));
}
$s = $current_month;

$date = strtotime($s);
$end_date= date('Y-m-d H:i:s', strtotime('-1 day', $date));

$body	=ob_start();
include "daily_rm_movment_for_month_header.php";

$query= getReportDailyRMmovmentForMonth($previous_month,$current_month);
$result = $conn->query($query);

include "daily_rm_movment_for_month_excel.php";

mysqli_close($conn);


echo $body 			= ob_get_clean();




function getReportDailyRMmovmentForMonth($previous_month,$current_month) {

    $sql="SELECT
    ware_stocktransactions_bulk.intOrderNo AS ORDER_NO,
	ware_stocktransactions_bulk.intOrderYear AS ORDER_YEAR,
	trn_orderdetails.strSalesOrderNo AS SALES_ORDER_NO,
	trn_orderdetails.strGraphicNo AS GRAPHIC_NO,
	trn_orderdetails.intSampleNo,
	trn_orderdetails.intSampleYear,
	trn_orderdetails.intRevisionNo,
	trn_orderdetails.strCombo,
	trn_orderdetails.strPrintName AS PRINT_NAME,
	mst_part.strName AS part,
	mst_locations.strName AS LOCATION,
	trn_orderdetails.intQty AS ORDER_QTY,
	mst_maincategory.strName AS MAIN_CATEGORY,
	mst_subcategory.strName AS SUB_CATEGORY,
	mst_item.strCode AS ITEM_CODE,
    mst_item.strCode as SUP_ITEM_CODE,
	mst_item.strName AS ITEM_NAME,
	
	(if(ware_stocktransactions_bulk.strType='ISSUE',(SELECT 
	GROUP_CONCAT(DISTINCT ISS_D.intMrnNo,'/',ISS_D.intMrnYear) 
	FROM `ware_issuedetails` as ISS_D 
	WHERE 
 	
	if((ISS_D.strOrderNo=0 or ISS_D.strOrderNo='' OR ISS_D.strOrderNo IS NULL),0,ISS_D.strOrderNo) = IF((ware_stocktransactions_bulk.intOrderNo=0 OR ware_stocktransactions_bulk.intOrderNo='' OR ware_stocktransactions_bulk.intOrderNo IS NULL),0,ware_stocktransactions_bulk.intOrderNo)	
	
	AND if((ISS_D.intOrderYear=0 or ISS_D.intOrderYear='' OR ISS_D.intOrderYear IS NULL),0,ISS_D.intOrderYear) = IF((ware_stocktransactions_bulk.intOrderYear=0 OR ware_stocktransactions_bulk.intOrderYear='' OR ware_stocktransactions_bulk.intOrderYear IS NULL),0,ware_stocktransactions_bulk.intOrderYear)
	
	AND if((ISS_D.strStyleNo=0 or ISS_D.strStyleNo='' OR ISS_D.strStyleNo IS NULL),0,ISS_D.strStyleNo) = IF((ware_stocktransactions_bulk.intSalesOrderId=0 OR ware_stocktransactions_bulk.intSalesOrderId='' OR ware_stocktransactions_bulk.intSalesOrderId IS NULL),0,ware_stocktransactions_bulk.intSalesOrderId)
	
	AND ISS_D.intItemId = ware_stocktransactions_bulk.intItemId
	AND `intIssueNo` = ware_stocktransactions_bulk.intDocumentNo
	AND intIssueYear = ware_stocktransactions_bulk.intDocumntYear),'')) as MRN_NO,
	
	(if(ware_stocktransactions_bulk.strType='ISSUE',(SELECT
GROUP_CONCAT(DISTINCT if(tb1.mrnType=3,'Production Orderwise',if(tb1.mrnType=2,'Technical',if(tb1.mrnType=1,'Sample',if(tb1.mrnType=5,'Production Non Orderwise',if(tb1.mrnType=6,'Production Orderwise Additional',if(tb1.mrnType=7,'Loan settlement to Supplier',if(tb1.mrnType=8,'Loan out to Supplier','Non RM')))))))) as `MRN_Type`
FROM
ware_issuedetails AS ISS_D
INNER JOIN ware_mrnheader AS tb1 ON ISS_D.intMrnNo = tb1.intMrnNo AND ISS_D.intMrnYear = tb1.intMrnYear
WHERE 
if((ISS_D.strOrderNo=0 or ISS_D.strOrderNo='' OR ISS_D.strOrderNo IS NULL),0,ISS_D.strOrderNo) = IF((ware_stocktransactions_bulk.intOrderNo=0 OR ware_stocktransactions_bulk.intOrderNo='' OR ware_stocktransactions_bulk.intOrderNo IS NULL),0,ware_stocktransactions_bulk.intOrderNo)	

AND if((ISS_D.intOrderYear=0 or ISS_D.intOrderYear='' OR ISS_D.intOrderYear IS NULL),0,ISS_D.intOrderYear) = IF((ware_stocktransactions_bulk.intOrderYear=0 OR ware_stocktransactions_bulk.intOrderYear='' OR ware_stocktransactions_bulk.intOrderYear IS NULL),0,ware_stocktransactions_bulk.intOrderYear)

AND if((ISS_D.strStyleNo=0 or ISS_D.strStyleNo='' OR ISS_D.strStyleNo IS NULL),0,ISS_D.strStyleNo) = IF((ware_stocktransactions_bulk.intSalesOrderId=0 OR ware_stocktransactions_bulk.intSalesOrderId='' OR ware_stocktransactions_bulk.intSalesOrderId IS NULL),0,ware_stocktransactions_bulk.intSalesOrderId)

AND ISS_D.intItemId = ware_stocktransactions_bulk.intItemId
AND `intIssueNo` = ware_stocktransactions_bulk.intDocumentNo
AND intIssueYear = ware_stocktransactions_bulk.intDocumntYear),'')) as MRN_TYPE, 

mst_units.strCode AS UOM,
YEAR (ware_stocktransactions_bulk.dtDate) AS RM_MOVED_YEAR,
MONTHNAME(
	ware_stocktransactions_bulk.dtDate
) AS RM_MOVED_MONTH,
ware_stocktransactions_bulk.intDocumentNo,
ware_stocktransactions_bulk.intDocumntYear,
ware_stocktransactions_bulk.strType,
DATE(
	ware_stocktransactions_bulk.dtDate
) AS RM_MOVED_DATE,
dblGRNRate,
	sum(
		ifnull(
			ware_stocktransactions_bulk.dblQty *- 1,
			0
		)
	) AS qty,
	sum(
		ifnull(
			ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate * mst_financeexchangerate.dblBuying *- 1,
			0
		)
	) AS amount,
	-- sum(ifnull(ware_stocktransactions_bulk.dblQty*ware_stocktransactions_bulk.dblGRNRate*dblExchangeRate*-1,0)) as amount_movement ,
	
			-- sum(ifnull(ware_stocktransactions_bulk.dblQty*ware_stocktransactions_bulk.dblGRNRate*mst_financeexchangerate.dblBuying*-1,0)))-(sum(ifnull(ware_stocktransactions_bulk.dblQty*ware_stocktransactions_bulk.dblGRNRate*dblExchangeRate*-1,0)))),
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.dtGRNDate,
			mst_financeexchangerate.dblBuying,
			dblExchangeRate,
			strCustomerPoNo AS CustomerPO
		FROM
			ware_stocktransactions_bulk
		
		LEFT JOIN trn_orderdetails ON ware_stocktransactions_bulk.intOrderNo = trn_orderdetails.intOrderNo
		AND ware_stocktransactions_bulk.intOrderYear = trn_orderdetails.intOrderYear
		AND ware_stocktransactions_bulk.intSalesOrderId = trn_orderdetails.intSalesOrderId
		LEFT JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
		LEFT JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
		AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
		LEFT JOIN mst_item ON ware_stocktransactions_bulk.intItemId = mst_item.intId
                LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
		INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
		INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
		INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
		LEFT JOIN mst_financeexchangerate ON mst_financeexchangerate.dtmDate = ware_stocktransactions_bulk.dtGRNDate
		AND mst_financeexchangerate.intCompanyId = ware_stocktransactions_bulk.intCompanyId
		AND mst_financeexchangerate.intCurrencyId = ware_stocktransactions_bulk.intCurrencyId
		LEFT JOIN mst_locations ON ware_stocktransactions_bulk.intLocationId = mst_locations.intId
		LEFT JOIN ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo
		AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear
		WHERE
			ware_stocktransactions_bulk.strType IN ('ISSUE', 'RETSTORES')
		AND date(
			ware_stocktransactions_bulk.`dtDate`
		) >= '$previous_month'
		AND date(
			ware_stocktransactions_bulk.`dtDate`
		) < '$current_month'
		GROUP BY
			ware_stocktransactions_bulk.intLocationId,
			ware_stocktransactions_bulk.intOrderNo,
			ware_stocktransactions_bulk.intOrderYear,
			ware_stocktransactions_bulk.intSalesOrderId,
			ware_stocktransactions_bulk.intItemId,
			/*ware_stocktransactions_bulk.intGRNNo,*/
			(
				ware_stocktransactions_bulk.intDocumentNo
			),
			(
				ware_stocktransactions_bulk.intDocumntYear
			)
    ";

        return $sql;
}




<?php
/*
 * AUTHOR: HASITHA CHARAKA
 * DATE: 2017/10/11
 */
ini_set('display_errors', 'on');
set_time_limit(1000);
ini_set('max_execution_time',80000);
session_start();

//$programCode = 'P1258';
//
//require_once "class/cls_permisions.php";
//$objpermisionget = new cls_permisions($db);
//require_once "class/tables/sys_approvelevels.php";
//$obj_sys_approvelevels = new sys_approvelevels($db);
//
//$permi_price_view = $objpermisionget->boolSPermision(8);
//$str_price_view = 'hidden';
//
//if ($permi_price_view)
//    $str_price_view = '';


//GET SELECTED MONTH
if($_GET['startDate']) {
    $date = $_GET['startDate'];
    $time = strtotime($date);
    $previous_month = date('Y-m-d 00:00:00', $time);

//get one month from specific date
    $current_month = date('Y-m-d 00:00:00', strtotime('+1 month', strtotime($previous_month)));
}
/*
 * CREATE DB CONNECTION
 */
$servername = $_SESSION['Server'];
$username = $_SESSION['UserName'];
$password = $_SESSION['Password'];
$dbname = $_SESSION['Database'];

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
//mysql_set_charset('utf8',$conn);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
//Drop temp table if exsit and create new table
//$result = $db->dropTable('temp_rm_moved_orders');

$tempory_table ='temp_rm_moved_orders'.$_SESSION['userId'];
$sql= "DROP TEMPORARY TABLE IF EXISTS $tempory_table";

if ($conn->query($sql) === TRUE) {
    echo "Table Drop".'<br>';
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}



//TEMPORARY
$sql ="CREATE TEMPORARY TABLE $tempory_table ENGINE=MEMORY 
as (SELECT
ware_stocktransactions_bulk.intOrderNo AS NO,
ware_stocktransactions_bulk.intOrderYear AS YEAR

FROM
ware_stocktransactions_bulk
WHERE
date(ware_stocktransactions_bulk.dtDate) >= '$previous_month' AND
ware_stocktransactions_bulk.dtDate < '$current_month' AND
ware_stocktransactions_bulk.intOrderNo > 0 /*AND (
ware_stocktransactions_bulk.intLocationId = 2 OR
ware_stocktransactions_bulk.intLocationId = 3) */
GROUP BY
ware_stocktransactions_bulk.intOrderYear,
ware_stocktransactions_bulk.intOrderNo)";

if ($conn->query($sql) === TRUE) {
    echo "Table Created".'<br>';
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}
/*
 * METHOD USE TO  INSERT DATA INTO TEMP TABLE
 * AUTHOR : HASITHA CHARAKA
 * DATE :2017/10/10
 *
 */

//$result = $db->InsertQuery($order_no,$year);
/** Error reporting */
error_reporting(E_ALL);
date_default_timezone_set('Asia/Kolkata');
define("DIR_PATH", "libraries/excel/Classes/PHPExcel.php");

require_once ($_SESSION['ROOT_PATH'].DIR_PATH);

// Create new PHPExcel
//create alphabet A-Z  and Number range 1-9
$charcters = range( 'A', 'Z');
$numbers = range(1,9);
$objPHPExcel = new PHPExcel();
// Set properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");
// Create a first sheet, representing sales data
$s = $current_month;

$date = strtotime($s);
$end_date= date('Y-m-d H:i:s', strtotime('-1 day', $date));
/*******************START 1ST REPORT*****************************/
$reportTitle = 'ORDER WHICH HAVE RM MOVEMENTS FROM - (' . $previous_month . ' To ' . $end_date . ') ';

$objPHPExcel->getActiveSheet()->mergeCells('A2:O2');
$objPHPExcel->setActiveSheetIndex(0)
//$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Something');
    ->setCellValue('A2', $reportTitle)

    ->setCellValue('A4', 'LOCATION')
    ->setCellValue('B4', 'ORDER NO')
    ->setCellValue('C4', 'YEAR')
    ->setCellValue('D4', 'Customer PO NO')
    ->setCellValue('E4', 'SO')
    ->setCellValue('F4', 'TECHNIQUE GROUP')
    ->setCellValue('G4', 'GRAPHIC NO')
    ->setCellValue('H4', 'SAMPLE NO')
    ->setCellValue('I4', 'SAMPLE YEAR')
    ->setCellValue('J4', 'REVISION')
    ->setCellValue('K4', 'COMBO')
    ->setCellValue('L4', 'PRINT')
    ->setCellValue('M4', 'PART')
    ->setCellValue('N4', 'ORDER QTY')
    ->setCellValue('O4', 'PRICE PER PIECE($)')
    ->setCellValue('P4', 'PRODUCTION_QTY')
    ->setCellValue('Q4', 'DISPATCHED TOT QTY')
    ->setCellValue('R4', 'DISPATCH GOOD QTY')
    ->setCellValue('S4', 'ACTUAL IN QTY')
    ->setCellValue('T4', 'PO_TYPE')
    ->setCellValue('U4', 'ORDER_TYPE');

PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);

//Set Bold Titles
for($i=0;$i<21;$i++) {
    //echo $i.'-'.$charcters[$i].'<br>';
    $header =$charcters[$i].'4';

    $objPHPExcel->getActiveSheet()->getStyle($header)->getFont()->setBold(true);
}
//add color to row title
$objPHPExcel->getActiveSheet()
    ->getStyle('A2:U2')
    ->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'cccccc')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font' => array(
                'bold' => false,
                'color' => array('rgb' => '000000'),
                'size' => 12,
            )
        )
    );
//method to get data from database
$query_1= getReport(1,$previous_month,$tempory_table);
$result = $conn->query($query_1);

if ($result->num_rows > 0) {
    // output data of each row
    $count=5;
    while($row = $result->fetch_assoc()) {
        // echo  '<br>'.$charcters[$count].'-'.$count.'<br>';
//CustomerPO
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$count,$row['orderRaisedLocation']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$count,$row['NO']);
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$count,$row['YEAR']);
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$count,$row['CustomerPO']);
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$count,$row['strSalesOrderNo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$count,$row['TECHNIQUE_GROUP_NAME']);
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$count,$row['strGraphicNo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$count,$row['intSampleNo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$count,$row['intSampleYear']);
        $objPHPExcel->getActiveSheet()->SetCellValue('J'.$count,$row['intRevisionNo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('K'.$count,$row['strCombo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('L'.$count,$row['strPrintName']);
        $objPHPExcel->getActiveSheet()->SetCellValue('M'.$count,$row['part']);
        $objPHPExcel->getActiveSheet()->SetCellValue('N'.$count,$row['intQty']);
        $objPHPExcel->getActiveSheet()->SetCellValue('O'.$count,$row['dblPrice']);
        $objPHPExcel->getActiveSheet()->SetCellValue('P'.$count,$row['PRODUCTION_QTY']);
        $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$count,$row['fabric_disp_tot']);
        $objPHPExcel->getActiveSheet()->SetCellValue('R'.$count,$row['fabric_disp_good']);
        $objPHPExcel->getActiveSheet()->SetCellValue('S'.$count,$row['fabric_in']);
        $objPHPExcel->getActiveSheet()->SetCellValue('T'.$count,$row['PO_TYPE']);
        if($row['PO_TYPE'] == 0){
            $objPHPExcel->getActiveSheet()->SetCellValue('U'.$count,'OTHER');
        }elseif ($row['PO_TYPE'] == 1){
            $objPHPExcel->getActiveSheet()->SetCellValue('U'.$count,'DUMMY');
        }else{
            $objPHPExcel->getActiveSheet()->SetCellValue('U'.$count,'ACTUAL');
        }


        $count++;
    }
} else {
    echo "0 results";

}
// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('ORDERS');
/***********************END 1ST SHEET******************/
/***********************START 2ND SHEET*******************************/
// Create a new worksheet, after the default sheet 1
$objPHPExcel->createSheet();

// Add some data to the second sheet, resembling some different data types
$objPHPExcel->setActiveSheetIndex(1);
$reportTitle = 'DISPATCHED_MONTHLY - (' . $previous_month . ' To ' . $end_date . ') ';

$objPHPExcel->getActiveSheet()->mergeCells('A2:O2');
$objPHPExcel->setActiveSheetIndex(1)
//$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Something');
    ->setCellValue('A2', $reportTitle)

    ->setCellValue('A4', 'PO_TYPE')
    ->setCellValue('B4', 'ORDER_TYPE')
    ->setCellValue('C4', 'YEAR')
    ->setCellValue('D4', 'CUSTOMER PO NO')
    ->setCellValue('E4', 'SO')
    ->setCellValue('F4', 'GRAPHIC NO')
    ->setCellValue('G4', 'SAMPLE NO')
    ->setCellValue('H4', 'SAMPLE YEAR')
    ->setCellValue('I4', 'REVISION NO')
    ->setCellValue('J4', 'COMBO')
    ->setCellValue('K4', 'PRINT NAME')
    ->setCellValue('L4', 'PART')
    ->setCellValue('M4', 'DISPATCH YEAR')
    ->setCellValue('N4', 'MONTH NAME')
    ->setCellValue('O4', 'LOCATION')
    ->setCellValue('P4', 'TOT_DISPATCH_QTY')
    ->setCellValue('Q4', 'GOOD_DISPATCH_QTY')
    ->setCellValue('R4', 'PRICE_PER_PIECE');
PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);

//Set Bold Titles
for($i=0;$i<18;$i++) {
    //echo $i.'-'.$charcters[$i].'<br>';
    $header =$charcters[$i].'4';

    $objPHPExcel->getActiveSheet()->getStyle($header)->getFont()->setBold(true);
}
//add color to row title
$objPHPExcel->getActiveSheet()
    ->getStyle('A2:R2')
    ->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'cccccc')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font' => array(
                'bold' => false,
                'color' => array('rgb' => '000000'),
                'size' => 12,
            )
        )
    );
//method to get data from database
$query_2= getReport(2,$previous_month,$tempory_table);
$result = $conn->query($query_2);

if ($result->num_rows > 0) {
    // output data of each row
    $count=5;
    while($row = $result->fetch_assoc()) {

        // echo  '<br>'.$charcters[$count].'-'.$count.'<br>';
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$count,$row['PO_TYPE']);
        if($row['PO_TYPE'] == 0){
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$count,'OTHER');
        }elseif ($row['PO_TYPE'] == 1){
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$count,'DUMMY');
        }else{
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$count,'ACTUAL');
        }

        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$count,$row['YEAR']);
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$count,$row['CustomerPO']);
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$count,$row['strSalesOrderNo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$count,$row['strGraphicNo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$count,$row['intSampleNo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$count,$row['intSampleYear']);
        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$count,$row['intRevisionNo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('J'.$count,$row['strCombo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('K'.$count,$row['strPrintName']);
        $objPHPExcel->getActiveSheet()->SetCellValue('L'.$count,$row['part']);
        $objPHPExcel->getActiveSheet()->SetCellValue('M'.$count,$row['YEAR_1']);
        $objPHPExcel->getActiveSheet()->SetCellValue('N'.$count,$row['MONTHNAME']);
        $objPHPExcel->getActiveSheet()->SetCellValue('O'.$count,$row['LOCATION']);
        $objPHPExcel->getActiveSheet()->SetCellValue('P'.$count,$row['TOT_DISP']);
        $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$count,$row['GOOD_DISP']);
        $objPHPExcel->getActiveSheet()->SetCellValue('R'.$count,$row['dblPrice']);

        $count++;
    }
} else {
    echo "0 results";

}


// Rename 2nd sheet
$objPHPExcel->getActiveSheet()->setTitle('DISPATCHED_MONTHLY');

/**********************END 2ND SHEET*********************************/
/**********************START 3RD SHEET**********************************************/
// Create a new worksheet, after the default sheet
/*$objPHPExcel->createSheet();

// Add some data to the second sheet, resembling some different data types
$objPHPExcel->setActiveSheetIndex(2);


$objPHPExcel->setActiveSheetIndex(2);
$reportTitle = 'DISPATCHED_DAILY - (' . $previous_month . ' To ' . $current_month . ') ';

$objPHPExcel->getActiveSheet()->mergeCells('A2:O2');
$objPHPExcel->setActiveSheetIndex(2)
//$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Something');
    ->setCellValue('A2', $reportTitle)

    ->setCellValue('A4', 'PO_TYPE')
    ->setCellValue('B4', 'ORDER_TYPE')
    ->setCellValue('C4', 'NO')
    ->setCellValue('D4', 'YEAR')
    ->setCellValue('E4', 'SALES ORDER NO')
    ->setCellValue('F4', 'GRAPHIC NO')
    ->setCellValue('G4', 'SAMPLE NO')
    ->setCellValue('H4', 'SAMPLE YEAR')
    ->setCellValue('I4', 'REVISION NO')
    ->setCellValue('J4', 'COMBO')
    ->setCellValue('K4', 'PRINT NAME')
    ->setCellValue('L4', 'PART NAME')
    ->setCellValue('M4', 'DATE')
    ->setCellValue('N4', 'DISPATCH_NO')
    ->setCellValue('O4', 'DISPATCH_YEAR')
    ->setCellValue('P4', 'LOCATION')
    ->setCellValue('Q4', 'DISPATCH TOT QTY')
    ->setCellValue('R4', 'DISPATCH GOOD QTY')
    ->setCellValue('S4', 'PRICE PER PIECE($)');
PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);

//Set Bold Titles
for($i=0;$i<19;$i++) {
    //echo $i.'-'.$charcters[$i].'<br>';
    $header =$charcters[$i].'4';

    $objPHPExcel->getActiveSheet()->getStyle($header)->getFont()->setBold(true);
}
//add color to row title
$objPHPExcel->getActiveSheet()
    ->getStyle('A2:O2')
    ->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'cccccc')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font' => array(
                'bold' => false,
                'color' => array('rgb' => '000000'),
                'size' => 12,
            )
        )
    );
//method to get data from database
$query_3= getReport(3,$previous_month);
$result = $conn->query($query_3);

if ($result->num_rows > 0) {
    // output data of each row
    $count=5;
    while($row = $result->fetch_assoc()) {
        // echo  '<br>'.$charcters[$count].'-'.$count.'<br>';

        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$count,$row['PO_TYPE']);
        if($row['PO_TYPE'] == 0){
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$count,'OTHER');
        }elseif ($row['PO_TYPE'] == 1){
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$count,'DUMMY');
        }else{
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$count,'ACTUAL');
        }
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$count,$row['NO']);
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$count,$row['YEAR']);
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$count,$row['strSalesOrderNo   ']);
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$count,$row['strGraphicNo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$count,$row['intSampleNo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$count,$row['intSampleYear']);
        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$count,$row['intRevisionNo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('J'.$count,$row['strCombo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('K'.$count,$row['strPrintName']);
        $objPHPExcel->getActiveSheet()->SetCellValue('L'.$count,$row['part']);
        $objPHPExcel->getActiveSheet()->SetCellValue('M'.$count,$row['DATE']);
        $objPHPExcel->getActiveSheet()->SetCellValue('N'.$count,$row['intDocumentNo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('O'.$count,$row['intDocumentYear']);
        $objPHPExcel->getActiveSheet()->SetCellValue('P'.$count,$row['LOCATION']);
        $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$count,$row['TOT_DISP']);
        $objPHPExcel->getActiveSheet()->SetCellValue('R'.$count,$row['GOOD_DISP']);
        $objPHPExcel->getActiveSheet()->SetCellValue('S'.$count,$row['dblPrice']);




        $count++;
    }
} else {
    echo "0 results";

}


// Rename 3rd sheet
$objPHPExcel->getActiveSheet()->setTitle('DISPATCHED_DAILY'); */
/***********************END 3RD SHEET***********************************/
/***********************START 4TH SHEET*********************************/
// Create a new worksheet, after the default sheet
//$objPHPExcel->createSheet();
//
//// Add some data to the second sheet, resembling some different data types
//$objPHPExcel->setActiveSheetIndex(2);
//
//
//$objPHPExcel->setActiveSheetIndex(2);
//$reportTitle = 'RM_EXPECTATION - (' . $previous_month . ' To ' . $current_month . ') ';
//
//$objPHPExcel->getActiveSheet()->mergeCells('A2:O2');
//$objPHPExcel->setActiveSheetIndex(2)
////$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Something');
//    ->setCellValue('A2', $reportTitle)
//
//    ->setCellValue('A4', 'ORDER NO')
//    ->setCellValue('B4', 'YEAR')
//    ->setCellValue('C4', 'SO')
//    ->setCellValue('D4', 'GRAPHIC NO')
//    ->setCellValue('E4', 'SAMPLE NO')
//    ->setCellValue('F4', 'SAMPLE YEAR')
//    ->setCellValue('G4', 'REVISION')
//    ->setCellValue('H4', 'COMBO')
//    ->setCellValue('I4', 'PRINT')
//    ->setCellValue('J4', 'PART')
//    ->setCellValue('K4', 'MAIN CATEGORY')
//    ->setCellValue('L4', 'SUB CATEGORY')
//    ->setCellValue('M4', 'ITEM CODE')
//    ->setCellValue('N4', 'ITEM')
//    ->setCellValue('O4', 'UOM')
//    ->setCellValue('P4', 'CONSUMPTION PER PIECE')
//    ->setCellValue('Q4', 'EXPECTED FABRIC ARRIVAL')
//    ->setCellValue('R4', 'RM USAGE');
//PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
//$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
//
////Set Bold Titles
//for($i=0;$i<18;$i++) {
//    //echo $i.'-'.$charcters[$i].'<br>';
//    $header =$charcters[$i].'4';
//
//    $objPHPExcel->getActiveSheet()->getStyle($header)->getFont()->setBold(true);
//}
////add color to row title
//$objPHPExcel->getActiveSheet()
//    ->getStyle('A2:O2')
//    ->applyFromArray(
//        array(
//            'fill' => array(
//                'type' => PHPExcel_Style_Fill::FILL_SOLID,
//                'color' => array('rgb' => 'cccccc')
//            ),
//            'alignment' => array(
//                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
//            ),
//            'font' => array(
//                'bold' => false,
//                'color' => array('rgb' => '000000'),
//                'size' => 12,
//            )
//        )
//    );
////method to get data from database
//$query_4= getReport(4,$previous_month);
//$result = $conn->query($query_4);
//
//if ($result->num_rows > 0) {
//    // output data of each row
//    $count=5;
//    while($row = $result->fetch_assoc()) {
//        // echo  '<br>'.$charcters[$count].'-'.$count.'<br>';
//
//
//        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$count,$row['NO']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$count,$row['YEAR']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$count,$row['strSalesOrderNo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$count,$row['strGraphicNo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$count,$row['intSampleNo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$count,$row['intSampleYear']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$count,$row['intRevisionNo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$count,$row['strCombo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$count,$row['strPrintName']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('J'.$count,$row['part']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('K'.$count,$row['MAIN_CATEGORY']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('L'.$count,$row['SUB_CATEGORY']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('M'.$count,$row['strCode']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('N'.$count,$row['ITEM_NAME']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('O'.$count,$row['UOM']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('P'.$count,$row['CONS_PC']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$count,$row['PRODUCTION_QTY']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('R'.$count,$row['REQUIRED']);
//
//
//
//
//
//        $count++;
//    }
//} else {
//    echo "0 results";
//
//}
//
//
//// Rename 4th sheet
//$objPHPExcel->getActiveSheet()->setTitle('RM_EXPECTATION');

/********************END 4TH SHEET*************************************/
/***************START 5TH SHEET***************************************/
// Create a new worksheet, after the default sheet
//$objPHPExcel->createSheet();
//
//// Add some data to the second sheet, resembling some different data types
//$objPHPExcel->setActiveSheetIndex(3);
//
//
//$objPHPExcel->setActiveSheetIndex(3);
//$reportTitle = 'MOVED_RM_TOTAL(RM IN SAMPLE) - (' . $previous_month . ' To ' . $current_month . ') ';
//
//$objPHPExcel->getActiveSheet()->mergeCells('A2:O2');
//$objPHPExcel->setActiveSheetIndex(3)
////$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Something');
//    ->setCellValue('A2', $reportTitle)
//    ->setCellValue('A3','RM IN SAMPLE (IN PRI)')
//    ->setCellValue('A4', 'ORDER NO')
//    ->setCellValue('B4', 'YEAR')
//    ->setCellValue('C4', 'SO')
//    ->setCellValue('D4', 'GRAPHIC NO')
//    ->setCellValue('E4', 'SAMPLE NO')
//    ->setCellValue('F4', 'SAMPLE YEAR')
//    ->setCellValue('G4', 'REVISION')
//    ->setCellValue('H4', 'COMBO')
//    ->setCellValue('I4', 'PRINT')
//    ->setCellValue('J4', 'PART')
//    ->setCellValue('K4', 'LOCATION')
//    ->setCellValue('L4', 'MAIN CATEGORY')
//    ->setCellValue('M4', 'SUB CATEGORY')
//    ->setCellValue('N4', 'ITEM CODE')
//    ->setCellValue('O4', 'ITEM')
//    ->setCellValue('P4', 'UOM')
//    ->setCellValue('Q4', 'CONS_PC')
//    ->setCellValue('R4', 'PRODUCTION QTY')
//    ->setCellValue('S4', 'REQUIRED')
//    ->setCellValue('T4', 'QTY')
//    ->setCellValue('U4', 'ACTUAL RM USAGE VALUE (LKR)');
//PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
//$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
//
////Set Bold Titles
//for($i=0;$i<21;$i++) {
//    //echo $i.'-'.$charcters[$i].'<br>';
//    $header =$charcters[$i].'4';
//
//    $objPHPExcel->getActiveSheet()->getStyle($header)->getFont()->setBold(true);
//}
////add color to row title
//$objPHPExcel->getActiveSheet()
//    ->getStyle('A2:O2')
//    ->applyFromArray(
//        array(
//            'fill' => array(
//                'type' => PHPExcel_Style_Fill::FILL_SOLID,
//                'color' => array('rgb' => 'cccccc')
//            ),
//            'alignment' => array(
//                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
//            ),
//            'font' => array(
//                'bold' => false,
//                'color' => array('rgb' => '000000'),
//                'size' => 12,
//            )
//        )
//    );
////method to get data from database
//$query_5= getReport(5,$previous_month);
//$result = $conn->query($query_5);
//
//if ($result->num_rows > 0) {
//    // output data of each row
//    $count=5;
//    while($row = $result->fetch_assoc()) {
//        // echo  '<br>'.$charcters[$count].'-'.$count.'<br>';
//
//
//        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$count,$row['NO']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$count,$row['YEAR']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$count,$row['strSalesOrderNo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$count,$row['strGraphicNo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$count,$row['intSampleNo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$count,$row['intSampleYear']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$count,$row['intRevisionNo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$count,$row['strCombo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$count,$row['strPrintName']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('J'.$count,$row['part']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('K'.$count,$row['LOCATION']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('L'.$count,$row['MAIN_CATEGORY']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('M'.$count,$row['SUB_CATEGORY']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('N'.$count,$row['ITEM_CODE']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('O'.$count,$row['ITEM']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('P'.$count,$row['UOM']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$count,$row['CONS_PC']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('R'.$count,$row['PRODUCTION_QTY']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('S'.$count,$row['REQUIRED']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('T'.$count,$row['qty']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('U'.$count,$row['amount']);
//
//        $count++;
//    }
//} else {
//    echo "0 results";
//
//}
//
//
//// Rename 5th sheet
//$objPHPExcel->getActiveSheet()->setTitle('MOVED_RM_TOTAL(RM IN SAMPLE)');
/****************END 5TH SHEET ***************************************/
/*************START 6TH SHEET ***************************************/
// Create a new worksheet, after the default sheet
//$objPHPExcel->createSheet();
//
//// Add some data to the second sheet, resembling some different data types
//$objPHPExcel->setActiveSheetIndex(4);
//
//
//$objPHPExcel->setActiveSheetIndex(4);
//$reportTitle = 'MOVED_RM_TOTAL(RM NOT IN SAMPLE) - (' . $previous_month . ' To ' . $current_month . ') ';
//
//$objPHPExcel->getActiveSheet()->mergeCells('A2:O2');
//$objPHPExcel->setActiveSheetIndex(4)
////$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Something');
//    ->setCellValue('A2', $reportTitle)
//    ->setCellValue('A3','RM NOT IN SAMPLE (NOT IN PRI)')
//    ->setCellValue('A4', 'ORDER NO')
//    ->setCellValue('B4', 'YEAR')
//    ->setCellValue('C4', 'SO')
//    ->setCellValue('D4', 'GRAPHIC NO')
//    ->setCellValue('E4', 'SAMPLE NO')
//    ->setCellValue('F4', 'SAMPLE YEAR')
//    ->setCellValue('G4', 'REVISION')
//    ->setCellValue('H4', 'COMBO')
//    ->setCellValue('I4', 'PRINT')
//    ->setCellValue('J4', 'PART')
//    ->setCellValue('K4', 'LOCATION')
//    ->setCellValue('L4', 'MAIN CATEGORY')
//    ->setCellValue('M4', 'SUB CATEGORY')
//    ->setCellValue('N4', 'ITEM CODE')
//    ->setCellValue('O4', 'ITEM')
//    ->setCellValue('P4', 'UOM')
//    ->setCellValue('Q4', 'ACTUAL RM USAGE')
//    ->setCellValue('R4', 'ACTUAL RM USAGE VALUE (LKR)');
//PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
//$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
//
////Set Bold Titles
//for($i=0;$i<18;$i++) {
//    //echo $i.'-'.$charcters[$i].'<br>';
//    $header =$charcters[$i].'4';
//
//    $objPHPExcel->getActiveSheet()->getStyle($header)->getFont()->setBold(true);
//}
////add color to row title
//$objPHPExcel->getActiveSheet()
//    ->getStyle('A2:O2')
//    ->applyFromArray(
//        array(
//            'fill' => array(
//                'type' => PHPExcel_Style_Fill::FILL_SOLID,
//                'color' => array('rgb' => 'cccccc')
//            ),
//            'alignment' => array(
//                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
//            ),
//            'font' => array(
//                'bold' => false,
//                'color' => array('rgb' => '000000'),
//                'size' => 12,
//            )
//        )
//    );
////method to get data from database
//$query_6= getReport(6,$previous_month);
//$result = $conn->query($query_6);
//
//if ($result->num_rows > 0) {
//    // output data of each row
//    $count=5;
//    while($row = $result->fetch_assoc()) {
//        // echo  '<br>'.$charcters[$count].'-'.$count.'<br>';
//
//        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$count,$row['NO']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$count,$row['YEAR']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$count,$row['strSalesOrderNo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$count,$row['strGraphicNo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$count,$row['intSampleNo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$count,$row['intSampleYear']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$count,$row['intRevisionNo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$count,$row['strCombo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$count,$row['strPrintName']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('J'.$count,$row['part']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('K'.$count,$row['LOCATION']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('L'.$count,$row['MAIN_CATEGORY']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('M'.$count,$row['SUB_CATEGORY']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('N'.$count,$row['ITEM_CODE']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('O'.$count,$row['ITEM_NAME']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('P'.$count,$row['UOM']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$count,$row['qty']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('R'.$count,$row['amount']);
//
//        $count++;
//    }
//} else {
//    echo "0 results";
//
//}
//// Rename 6th sheet
//$objPHPExcel->getActiveSheet()->setTitle('MOVED_RM_TOT(RM NOT IN SAMPLE)');
/*************END 6TH  SHEET ***************************************/
/************START 7TH SHEET **************************************/
// Create a new worksheet, after the default sheet
//$objPHPExcel->createSheet();
//
//// Add some data to the second sheet, resembling some different data types
//$objPHPExcel->setActiveSheetIndex(5);
//
//
//$objPHPExcel->setActiveSheetIndex(5);
//$reportTitle = 'MOVED_RM_MONTHLY - (' . $previous_month . ' To ' . $current_month . ') ';
//
//$objPHPExcel->getActiveSheet()->mergeCells('A2:O2');
//$objPHPExcel->setActiveSheetIndex(5)
////$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Something');
//    ->setCellValue('A2', $reportTitle)
//    ->setCellValue('A3','MOVED RM ONLY')
//    ->setCellValue('A4', 'ORDER NO')
//    ->setCellValue('B4', 'YEAR')
//    ->setCellValue('C4', 'SO')
//    ->setCellValue('D4', 'GRAPHIC NO')
//    ->setCellValue('E4', 'SAMPLE NO')
//    ->setCellValue('F4', 'SAMPLE YEAR')
//    ->setCellValue('G4', 'REVISION')
//    ->setCellValue('H4', 'COMBO')
//    ->setCellValue('I4', 'PRINT')
//    ->setCellValue('J4', 'PART')
//    ->setCellValue('K4', 'LOCATION')
//    ->setCellValue('L4', 'MAIN CATEGORY')
//    ->setCellValue('M4', 'SUB CATEGORY')
//    ->setCellValue('N4', 'ITEM CODE')
//    ->setCellValue('O4', 'ITEM')
//    ->setCellValue('P4', 'UOM')
//    ->setCellValue('Q4', 'RM MOVED YEAR')
//    ->setCellValue('R4', 'RM MOVED MONTH')
//    ->setCellValue('S4', 'ACTUAL RM USAGE')
//    ->setCellValue('T4', 'ACTUAL RM VALUE(LKR)');;
//
//PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
//$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
//
////Set Bold Titles
//for($i=0;$i<20;$i++) {
//    //echo $i.'-'.$charcters[$i].'<br>';
//    $header =$charcters[$i].'4';
//
//    $objPHPExcel->getActiveSheet()->getStyle($header)->getFont()->setBold(true);
//}
////add color to row title
//$objPHPExcel->getActiveSheet()
//    ->getStyle('A2:O2')
//    ->applyFromArray(
//        array(
//            'fill' => array(
//                'type' => PHPExcel_Style_Fill::FILL_SOLID,
//                'color' => array('rgb' => 'cccccc')
//            ),
//            'alignment' => array(
//                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
//            ),
//            'font' => array(
//                'bold' => false,
//                'color' => array('rgb' => '000000'),
//                'size' => 12,
//            )
//        )
//    );
////method to get data from database
//$query_7= getReport(7,$previous_month);
//$result = $conn->query($query_7);
//
//if ($result->num_rows > 0) {
//    // output data of each row
//    $count=5;
//    while($row = $result->fetch_assoc()) {
//        // echo  '<br>'.$charcters[$count].'-'.$count.'<br>';
//
//        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$count,$row['NO']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$count,$row['YEAR']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$count,$row['strSalesOrderNo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$count,$row['strGraphicNo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$count,$row['intSampleNo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$count,$row['intSampleYear']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$count,$row['intRevisionNo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$count,$row['strCombo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$count,$row['strPrintName']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('J'.$count,$row['part']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('K'.$count,$row['LOCATION']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('L'.$count,$row['MAIN_CATEGORY']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('M'.$count,$row['SUB_CATEGORY']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('N'.$count,$row['ITEM_CODE']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('O'.$count,$row['ITEM_NAME']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('P'.$count,$row['UOM']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$count,$row['RM_MOVED_YEAR']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('R'.$count,$row['RM_MOVED_MONTH']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('S'.$count,$row['qty']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('T'.$count,$row['amount']);
//
//        $count++;
//    }
//} else {
//    echo "0 results";
//
//}
//// Rename 7th sheet
//$objPHPExcel->getActiveSheet()->setTitle('MOVED_RM_MONTHLY');

/************END 7TH SHEET ***************************************/
/***********START 8TH SHEET***************************************/
// Create a new worksheet, after the default sheet
/*$objPHPExcel->createSheet();

// Add some data to the second sheet, resembling some different data types
$objPHPExcel->setActiveSheetIndex(6);


$objPHPExcel->setActiveSheetIndex(6);
$reportTitle = 'MOVED_RM_DAILY - (' . $previous_month . ' To ' . $current_month . ') ';

$objPHPExcel->getActiveSheet()->mergeCells('A2:O2');
$objPHPExcel->setActiveSheetIndex(6)
//$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Something');
    ->setCellValue('A2', $reportTitle)
    ->setCellValue('A3','MOVED RM ONLY')
    ->setCellValue('A4', 'ORDER NO')
    ->setCellValue('B4', 'YEAR')
    ->setCellValue('C4', 'SO')
    ->setCellValue('D4', 'GRAPHIC NO')
    ->setCellValue('E4', 'SAMPLE NO')
    ->setCellValue('F4', 'SAMPLE YEAR')
    ->setCellValue('G4', 'REVISION')
    ->setCellValue('H4', 'COMBO')
    ->setCellValue('I4', 'PRINT')
    ->setCellValue('J4', 'PART')
    ->setCellValue('K4', 'LOCATION')
    ->setCellValue('L4', 'MAIN CATEGORY')
    ->setCellValue('M4', 'SUB CATEGORY')
    ->setCellValue('N4', 'ITEM CODE')
    ->setCellValue('O4', 'ITEM')
    ->setCellValue('P4', 'UOM')
    ->setCellValue('Q4', 'RM MOVED YEAR')
    ->setCellValue('R4', 'RM MOVED MONTH')
    ->setCellValue('S4', 'DOCUMENT NO')
    ->setCellValue('T4', 'DOCUMNET YEAR')
    ->setCellValue('U4', 'RM MOVED DATE')
    ->setCellValue('V4', 'ACTUAL RM USAGE')
    ->setCellValue('W4', 'ACTUAL RM USAGE VALUE (LKR)')
    ->setCellValue('X4', 'QTY');

PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);

//Set Bold Titles
for($i=0;$i<24;$i++) {
    //echo $i.'-'.$charcters[$i].'<br>';
    $header =$charcters[$i].'4';

    $objPHPExcel->getActiveSheet()->getStyle($header)->getFont()->setBold(true);
}
//add color to row title
$objPHPExcel->getActiveSheet()
    ->getStyle('A2:O2')
    ->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'cccccc')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font' => array(
                'bold' => false,
                'color' => array('rgb' => '000000'),
                'size' => 12,
            )
        )
    );
//method to get data from database
$query_8= getReport(8,$previous_month);
$result = $conn->query($query_8);

if ($result->num_rows > 0) {
    // output data of each row
    $count=5;
    while($row = $result->fetch_assoc()) {
        // echo  '<br>'.$charcters[$count].'-'.$count.'<br>';

        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$count,$row['NO']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$count,'YEAR');
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$count,$row['strSalesOrderNo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$count,$row['strGraphicNo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$count,$row['intSampleNo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$count,$row['intSampleYear']);
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$count,$row['intRevisionNo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$count,$row['strCombo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$count,$row['strPrintName']);
        $objPHPExcel->getActiveSheet()->SetCellValue('J'.$count,$row['part']);
        $objPHPExcel->getActiveSheet()->SetCellValue('K'.$count,$row['LOCATION']);
        $objPHPExcel->getActiveSheet()->SetCellValue('L'.$count,$row['MAIN_CATEGORY']);
        $objPHPExcel->getActiveSheet()->SetCellValue('M'.$count,$row['SUB_CATEGORY']);
        $objPHPExcel->getActiveSheet()->SetCellValue('N'.$count,$row['ITEM_CODE']);
        $objPHPExcel->getActiveSheet()->SetCellValue('O'.$count,$row['ITEM_NAME']);
        $objPHPExcel->getActiveSheet()->SetCellValue('P'.$count,$row['UOM']);
        $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$count,$row['RM_MOVED_YEAR']);
        $objPHPExcel->getActiveSheet()->SetCellValue('R'.$count,$row['RM_MOVED_MONTH']);
        $objPHPExcel->getActiveSheet()->SetCellValue('S'.$count,$row['intDocumentNo']);
        $objPHPExcel->getActiveSheet()->SetCellValue('T'.$count,$row['intDocumntYear']);
        $objPHPExcel->getActiveSheet()->SetCellValue('U'.$count,$row['DATE']);
        $objPHPExcel->getActiveSheet()->SetCellValue('V'.$count,$row['qty']);
        $objPHPExcel->getActiveSheet()->SetCellValue('W'.$count,$row['amount']);
        $objPHPExcel->getActiveSheet()->SetCellValue('X'.$count,$row['intQty']);

        $count++;
    }
} else {
    echo "0 results";

}
// Rename 8th sheet
$objPHPExcel->getActiveSheet()->setTitle('MOVED_RM_DAILY'); */

/***********END 8TH SHEET***************************************/
/**********START 9TH SHEET ************************************/
// Create a new worksheet, after the default sheet
//$objPHPExcel->createSheet();
//
//// Add some data to the second sheet, resembling some different data types
//$objPHPExcel->setActiveSheetIndex(6);
//
//
//$objPHPExcel->setActiveSheetIndex(6);
//$reportTitle = 'NON ORDER WISE - (' . $previous_month . ' To ' . $current_month . ') ';
//
//$objPHPExcel->getActiveSheet()->mergeCells('A2:O2');
//$objPHPExcel->setActiveSheetIndex(6)
////$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Something');
//    ->setCellValue('A2', $reportTitle)
//    ->setCellValue('A3','NON ORDER WISE')
//    ->setCellValue('A4', 'LOCATION')
//    ->setCellValue('B4', 'MAIN CATEGORY')
//    ->setCellValue('C4', 'SUB CATEGORY')
//    ->setCellValue('D4', 'ITEM CODE')
//    ->setCellValue('E4', 'ITEM NAME')
//    ->setCellValue('F4', 'UOM')
//    ->setCellValue('G4', 'MOVED YEAR')
//    ->setCellValue('H4', 'MOVED MONTH')
//    ->setCellValue('I4', 'DOCUMNET NO')
//    ->setCellValue('J4', 'DOCUMENT YEAR')
//    ->setCellValue('K4', 'TRANS TYPE')
//    ->setCellValue('L4', 'DATE')
//    ->setCellValue('M4', 'QTY')
//    ->setCellValue('N4', 'AMOUNT');
//
//PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
//$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
//
////Set Bold Titles
//for($i=0;$i<14;$i++) {
//    //echo $i.'-'.$charcters[$i].'<br>';
//    $header =$charcters[$i].'4';
//
//    $objPHPExcel->getActiveSheet()->getStyle($header)->getFont()->setBold(true);
//}
////add color to row title
//$objPHPExcel->getActiveSheet()
//    ->getStyle('A2:O2')
//    ->applyFromArray(
//        array(
//            'fill' => array(
//                'type' => PHPExcel_Style_Fill::FILL_SOLID,
//                'color' => array('rgb' => 'cccccc')
//            ),
//            'alignment' => array(
//                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
//            ),
//            'font' => array(
//                'bold' => false,
//                'color' => array('rgb' => '000000'),
//                'size' => 12,
//            )
//        )
//    );
////method to get data from database
//$query_9= getNonOrderWise($previous_month,$current_month);
//$result = $conn->query($query_9);
//
//if ($result->num_rows > 0) {
//    // output data of each row
//    $count=5;
//    while($row = $result->fetch_assoc()) {
//        // echo  '<br>'.$charcters[$count].'-'.$count.'<br>';
//
//        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$count,$row['LOCATION']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$count,'MAIN_CATEGORY');
//        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$count,$row['SUB_CATEGORY']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$count,$row['ITEM_CODE']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$count,$row['ITEM_NAME']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$count,$row['UOM']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$count,$row['MOVED_YEAR']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$count,$row['MOVED_MONTH']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$count,$row['intDocumentNo']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('J'.$count,$row['intDocumentYear']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('K'.$count,$row['trans_type']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('L'.$count,$row['DATE']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('M'.$count,$row['qty']);
//        $objPHPExcel->getActiveSheet()->SetCellValue('N'.$count,$row['amount']);
//
//        $count++;
//    }
//} else {
//    echo "0 results";
//
//}
//// Rename 9th sheet
//$objPHPExcel->getActiveSheet()->setTitle('NON order WISE');
/**********END 9TH SHEET ************************************/
// Redirect output to a client’s web browser (Excel5)
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
ob_end_clean();
$date= 'ORDER WISE REVENUE'.$previous_month.'TO'.$end_date.'\.\xlsx';
header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="'.$date.'"');
//header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
//header("Content-Disposition: attachment;filename=\"filename.xlsx\"");
//header("Cache-Control: max-age=0");
$objWriter->save('php://output');
exit;
//get query for each report
function getReport($rpt_type,$previous_month,$tempory_table){
//remove timestamp from the previous month date

    $str = $previous_month;
    $date=explode(" ",$str);
    if($rpt_type == 1) {
        $sql= "SELECT
mst_locations.strName AS orderRaisedLocation,
$tempory_table.`NO`,
$tempory_table.`YEAR`,
trn_orderdetails.strSalesOrderNo,
mst_technique_groups.TECHNIQUE_GROUP_NAME,
trn_orderdetails.strGraphicNo,
trn_orderdetails.intSampleNo,
trn_orderdetails.intSampleYear,
trn_orderdetails.intRevisionNo,
trn_orderdetails.strCombo,
trn_orderdetails.strPrintName,
mst_part.strName AS part,
trn_orderdetails.intQty,
trn_orderdetails.dblPrice,
trn_po_prn_details_sales_order.PRODUCTION_QTY,
(select  sum(ware_fabricdispatchdetails.dblCutRetQty+ware_fabricdispatchdetails.dblEmbroideryQty+ware_fabricdispatchdetails.dblFdammageQty+ware_fabricdispatchdetails.dblGoodQty+ware_fabricdispatchdetails.dblPDammageQty+ware_fabricdispatchdetails.dblSampleQty)
from ware_fabricdispatchheader
INNER JOIN ware_fabricdispatchdetails
on ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo
and ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear
 where ware_fabricdispatchheader.intOrderNo= $tempory_table.`NO`
 and ware_fabricdispatchheader.intOrderYear = $tempory_table.`YEAR`
and ware_fabricdispatchdetails.intSalesOrderId = trn_po_prn_details_sales_order.SALES_ORDER
and ware_fabricdispatchheader.intStatus =1
) AS fabric_disp_tot,
(select  sum(ware_fabricdispatchdetails.dblCutRetQty+ware_fabricdispatchdetails.dblEmbroideryQty+ware_fabricdispatchdetails.dblFdammageQty+ware_fabricdispatchdetails.dblGoodQty+ware_fabricdispatchdetails.dblPDammageQty+ware_fabricdispatchdetails.dblSampleQty)
from ware_fabricdispatchheader
INNER JOIN ware_fabricdispatchdetails
on ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo
and ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear
 where ware_fabricdispatchheader.intOrderNo= $tempory_table.`NO`
 and ware_fabricdispatchheader.intOrderYear = $tempory_table.`YEAR`
and ware_fabricdispatchdetails.intSalesOrderId = trn_po_prn_details_sales_order.SALES_ORDER
and ware_fabricdispatchheader.intStatus =1
) AS fabric_disp_good,
(select sum(ware_fabricreceiveddetails.dblQty) from ware_fabricreceivedheader
INNER JOIN ware_fabricreceiveddetails on ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo
and ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
where ware_fabricreceivedheader.intOrderNo= $tempory_table.`NO`
 and ware_fabricreceivedheader.intOrderYear = $tempory_table.`YEAR`
and ware_fabricreceiveddetails.intSalesOrderId = trn_po_prn_details_sales_order.SALES_ORDER
and ware_fabricreceivedheader.intStatus =1
) AS fabric_in,
trn_orderheader.PO_TYPE,
SUM(

		IF (
			ware_stocktransactions_fabric.strType = 'Dispatched_G',
			ware_stocktransactions_fabric.dblQty *- 1,
			0
		)
	) AS GOOD_DISP,
	strCustomerPoNo AS CustomerPO
FROM
$tempory_table
INNER JOIN trn_po_prn_details_sales_order ON $tempory_table.`NO` = trn_po_prn_details_sales_order.ORDER_NO AND $tempory_table.`YEAR` = trn_po_prn_details_sales_order.ORDER_YEAR
INNER JOIN trn_orderdetails ON trn_po_prn_details_sales_order.ORDER_NO = trn_orderdetails.intOrderNo AND trn_po_prn_details_sales_order.ORDER_YEAR = trn_orderdetails.intOrderYear AND trn_po_prn_details_sales_order.SALES_ORDER = trn_orderdetails.intSalesOrderId
INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN mst_locations on mst_locations.intId = trn_orderheader.intLocationId
INNER JOIN mst_technique_groups ON trn_orderdetails.TECHNIQUE_GROUP_ID = mst_technique_groups.TECHNIQUE_GROUP_ID
LEFT JOIN ware_stocktransactions_fabric ON $tempory_table.`NO` = ware_stocktransactions_fabric.intOrderNo
AND $tempory_table.`YEAR` = ware_stocktransactions_fabric.intOrderYear


GROUP BY
trn_po_prn_details_sales_order.ORDER_NO,
trn_po_prn_details_sales_order.ORDER_YEAR,
trn_po_prn_details_sales_order.SALES_ORDER";

    }else if($rpt_type == 2) {
        $sql="SELECT
trn_orderheader.PO_TYPE,
$tempory_table.`NO`,
$tempory_table.`YEAR`,
trn_orderdetails.strSalesOrderNo,
trn_orderdetails.strGraphicNo,
trn_orderdetails.intSampleNo,
trn_orderdetails.intSampleYear,
trn_orderdetails.intRevisionNo,
trn_orderdetails.strCombo,
trn_orderdetails.strPrintName,
mst_part.strName AS part,
YEAR(ware_stocktransactions_fabric.dtDate) AS YEAR_1,
MONTHNAME(ware_stocktransactions_fabric.dtDate) AS MONTHNAME,
mst_locations.strName AS LOCATION,
SUM(ware_stocktransactions_fabric.dblQty*-1) AS TOT_DISP,
SUM(IF(ware_stocktransactions_fabric.strType='Dispatched_G',ware_stocktransactions_fabric.dblQty*-1,0)) AS GOOD_DISP ,
-- trn_orderdetails.intQty,
trn_orderdetails.dblPrice,
strCustomerPoNo AS CustomerPO
FROM
$tempory_table
INNER JOIN ware_stocktransactions_fabric ON $tempory_table.`NO` = ware_stocktransactions_fabric.intOrderNo
AND $tempory_table.`YEAR` = ware_stocktransactions_fabric.intOrderYear
INNER JOIN trn_orderdetails ON ware_stocktransactions_fabric.intOrderNo = trn_orderdetails.intOrderNo
AND ware_stocktransactions_fabric.intOrderYear = trn_orderdetails.intOrderYear
AND ware_stocktransactions_fabric.intSalesOrderId = trn_orderdetails.intSalesOrderId
INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN mst_locations ON mst_locations.intId = ware_stocktransactions_fabric.intLocationId
WHERE
ware_stocktransactions_fabric.strType LIKE '%DISP%'
GROUP BY
trn_orderdetails.intOrderNo,
trn_orderdetails.intOrderYear,
trn_orderdetails.intSalesOrderId,
MONTH (ware_stocktransactions_fabric.dtDate),
YEAR (ware_stocktransactions_fabric.dtDate),
ware_stocktransactions_fabric.intLocationId";

}else if($rpt_type == 3) {
        $sql='SELECT
trn_orderheader.PO_TYPE,
temp_rm_moved_orders.`NO`,
temp_rm_moved_orders.`YEAR`,
trn_orderdetails.strSalesOrderNo,
trn_orderdetails.strGraphicNo,
trn_orderdetails.intSampleNo,
trn_orderdetails.intSampleYear,
trn_orderdetails.intRevisionNo,
trn_orderdetails.strCombo,
trn_orderdetails.strPrintName,
mst_part.strName AS part,

DATE(ware_stocktransactions_fabric.dtDate) AS DATE,
ware_stocktransactions_fabric.intDocumentNo,
ware_stocktransactions_fabric.intDocumentYear,
mst_locations.strName AS LOCATION,
SUM(ware_stocktransactions_fabric.dblQty*-1) AS TOT_DISP,
SUM(IF(ware_stocktransactions_fabric.strType=\'Dispatched_G\',ware_stocktransactions_fabric.dblQty*-1,0)) AS GOOD_DISP ,

trn_orderdetails.dblPrice
FROM
temp_rm_moved_orders
INNER JOIN ware_stocktransactions_fabric ON temp_rm_moved_orders.`NO` = ware_stocktransactions_fabric.intOrderNo
AND temp_rm_moved_orders.`YEAR` = ware_stocktransactions_fabric.intOrderYear
INNER JOIN trn_orderdetails ON ware_stocktransactions_fabric.intOrderNo = trn_orderdetails.intOrderNo
AND ware_stocktransactions_fabric.intOrderYear = trn_orderdetails.intOrderYear
AND ware_stocktransactions_fabric.intSalesOrderId = trn_orderdetails.intSalesOrderId
INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN mst_locations ON mst_locations.intId = ware_stocktransactions_fabric.intLocationId

WHERE
ware_stocktransactions_fabric.strType LIKE \'%DISP%\'
GROUP BY
trn_orderdetails.intOrderNo,
trn_orderdetails.intOrderYear,
trn_orderdetails.intSalesOrderId,
MONTH (ware_stocktransactions_fabric.dtDate),
YEAR (ware_stocktransactions_fabric.dtDate),
ware_stocktransactions_fabric.intDocumentNo,
ware_stocktransactions_fabric.intDocumentYear,
ware_stocktransactions_fabric.intLocationId';
    }else if($rpt_type == 4) {
        $sql='SELECT
	temp_rm_moved_orders.`NO`,
	temp_rm_moved_orders.`YEAR`,
	trn_orderdetails.strSalesOrderNo,
	trn_orderdetails.strGraphicNo,
	trn_orderdetails.intSampleNo,
	trn_orderdetails.intSampleYear,
	trn_orderdetails.intRevisionNo,
	trn_orderdetails.strCombo,
	trn_orderdetails.strPrintName,
	mst_part.strName AS part,
	trn_orderdetails.intQty,
	mst_maincategory.strName AS MAIN_CATEGORY,
	mst_subcategory.strName AS SUB_CATEGORY,
	mst_item.strCode,
	mst_item.strName AS ITEM_NAME,
	mst_units.strCode AS UOM,
	trn_po_prn_details_sales_order.CONS_PC,
	trn_po_prn_details_sales_order.PRODUCTION_QTY,
	trn_po_prn_details_sales_order.REQUIRED
FROM
	temp_rm_moved_orders
INNER JOIN trn_po_prn_details_sales_order ON temp_rm_moved_orders.`NO` = trn_po_prn_details_sales_order.ORDER_NO
AND temp_rm_moved_orders.`YEAR` = trn_po_prn_details_sales_order.ORDER_YEAR
INNER JOIN trn_orderdetails ON trn_po_prn_details_sales_order.ORDER_NO = trn_orderdetails.intOrderNo
AND trn_po_prn_details_sales_order.ORDER_YEAR = trn_orderdetails.intOrderYear
AND trn_po_prn_details_sales_order.SALES_ORDER = trn_orderdetails.intSalesOrderId
INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN mst_item ON trn_po_prn_details_sales_order.ITEM = mst_item.intId
INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
GROUP BY
	trn_po_prn_details_sales_order.ORDER_NO,
	trn_po_prn_details_sales_order.ORDER_YEAR,
	trn_po_prn_details_sales_order.SALES_ORDER,
	trn_po_prn_details_sales_order.ITEM';
    }else if($rpt_type == 5) {
        $sql='SELECT
temp_rm_moved_orders.`NO`,
temp_rm_moved_orders.`YEAR`,
trn_orderdetails.strSalesOrderNo,
trn_orderdetails.strGraphicNo,
trn_orderdetails.intSampleNo,
trn_orderdetails.intSampleYear,
trn_orderdetails.intRevisionNo,
trn_orderdetails.strCombo,
trn_orderdetails.strPrintName,
mst_part.strName AS part,
mst_locations.strName AS LOCATION,
trn_orderdetails.intQty,
mst_maincategory.strName AS MAIN_CATEGORY,
mst_subcategory.strName AS SUB_CATEGORY,
mst_item.strCode AS ITEM_CODE,
mst_item.strName AS ITEM,
mst_units.strCode AS UOM,
trn_po_prn_details_sales_order.CONS_PC,
trn_po_prn_details_sales_order.PRODUCTION_QTY,
trn_po_prn_details_sales_order.REQUIRED,
sum(ifnull(ware_stocktransactions_bulk.dblQty*-1,0)) as qty,
sum(ifnull(ware_stocktransactions_bulk.dblQty*ware_stocktransactions_bulk.dblGRNRate*mst_financeexchangerate.dblBuying*-1,0)) as amount
FROM
(select * from temp_rm_moved_orders /*limit 0,500*/) as temp_rm_moved_orders
INNER JOIN trn_po_prn_details_sales_order ON temp_rm_moved_orders.`NO` = trn_po_prn_details_sales_order.ORDER_NO AND temp_rm_moved_orders.`YEAR` = trn_po_prn_details_sales_order.ORDER_YEAR
INNER JOIN trn_orderdetails ON trn_po_prn_details_sales_order.ORDER_NO = trn_orderdetails.intOrderNo AND trn_po_prn_details_sales_order.ORDER_YEAR = trn_orderdetails.intOrderYear AND trn_po_prn_details_sales_order.SALES_ORDER = trn_orderdetails.intSalesOrderId
INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN mst_item ON trn_po_prn_details_sales_order.ITEM = mst_item.intId
INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
LEFT JOIN ware_stocktransactions_bulk ON trn_po_prn_details_sales_order.ORDER_NO = ware_stocktransactions_bulk.intOrderNo AND trn_po_prn_details_sales_order.ORDER_YEAR = ware_stocktransactions_bulk.intOrderYear AND trn_po_prn_details_sales_order.SALES_ORDER = ware_stocktransactions_bulk.intSalesOrderId AND trn_po_prn_details_sales_order.ITEM = ware_stocktransactions_bulk.intItemId
AND
ware_stocktransactions_bulk.strType IN (\'ISSUE\',\'RETSTORES\')
LEFT JOIN mst_financeexchangerate on mst_financeexchangerate.dtmDate = ware_stocktransactions_bulk.dtGRNDate and mst_financeexchangerate.intCompanyId = ware_stocktransactions_bulk.intCompanyId
and mst_financeexchangerate.intCurrencyId = ware_stocktransactions_bulk.intCurrencyId
LEFT JOIN mst_locations ON ware_stocktransactions_bulk.intLocationId=mst_locations.intId
GROUP BY
ware_stocktransactions_bulk.intLocationId,
trn_po_prn_details_sales_order.ORDER_NO,
trn_po_prn_details_sales_order.ORDER_YEAR,
trn_po_prn_details_sales_order.SALES_ORDER,
trn_po_prn_details_sales_order.ITEM';
    }else if($rpt_type == 6) {

        $sql ='SELECT
	temp_rm_moved_orders.`NO`,
	temp_rm_moved_orders.`YEAR`,
	trn_orderdetails.strSalesOrderNo,
	trn_orderdetails.strGraphicNo,
	trn_orderdetails.intSampleNo,
	trn_orderdetails.intSampleYear,
	trn_orderdetails.intRevisionNo,
	trn_orderdetails.strCombo,
	trn_orderdetails.strPrintName,
	mst_part.strName AS part,
	mst_locations.strName AS LOCATION,
	trn_orderdetails.intQty,
	mst_maincategory.strName AS MAIN_CATEGORY,
	mst_subcategory.strName AS SUB_CATEGORY,
	mst_item.strCode  AS ITEM_CODE,
	mst_item.strName AS ITEM_NAME,
	mst_units.strCode AS UOM,
	trn_po_prn_details_sales_order.CONS_PC,
	trn_po_prn_details_sales_order.PRODUCTION_QTY,
	trn_po_prn_details_sales_order.REQUIRED,
	sum(
		ifnull(
			ware_stocktransactions_bulk.dblQty *- 1,
			0
		)
	) AS qty,
	sum(
		ifnull(
			ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate * mst_financeexchangerate.dblBuying *- 1,
			0
		)
	) AS amount
FROM
	ware_stocktransactions_bulk
INNER JOIN (
	SELECT
		*
	FROM
		temp_rm_moved_orders /*limit 0,500*/
) AS temp_rm_moved_orders ON temp_rm_moved_orders.`NO` = ware_stocktransactions_bulk.intOrderNo
AND temp_rm_moved_orders.`YEAR` = ware_stocktransactions_bulk.intOrderYear
LEFT JOIN trn_po_prn_details_sales_order ON temp_rm_moved_orders.`NO` = trn_po_prn_details_sales_order.ORDER_NO
AND temp_rm_moved_orders.`YEAR` = trn_po_prn_details_sales_order.ORDER_YEAR
AND trn_po_prn_details_sales_order.SALES_ORDER = ware_stocktransactions_bulk.intSalesOrderId
AND trn_po_prn_details_sales_order.ITEM = ware_stocktransactions_bulk.intItemId
INNER JOIN trn_orderdetails ON ware_stocktransactions_bulk.intOrderNo = trn_orderdetails.intOrderNo
AND ware_stocktransactions_bulk.intOrderYear = trn_orderdetails.intOrderYear
AND ware_stocktransactions_bulk.intSalesOrderId = trn_orderdetails.intSalesOrderId
INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN mst_item ON ware_stocktransactions_bulk.intItemId = mst_item.intId
INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
LEFT JOIN mst_financeexchangerate ON mst_financeexchangerate.dtmDate = ware_stocktransactions_bulk.dtGRNDate
AND mst_financeexchangerate.intCompanyId = ware_stocktransactions_bulk.intCompanyId
AND mst_financeexchangerate.intCurrencyId = ware_stocktransactions_bulk.intCurrencyId
LEFT JOIN mst_locations ON ware_stocktransactions_bulk.intLocationId = mst_locations.intId
WHERE
ware_stocktransactions_bulk.strType IN (\'ISSUE\',\'RETSTORES\')
AND DATE(ware_stocktransactions_bulk.dtDate) >='.$date[0].'
AND ware_stocktransactions_bulk.intOrderNo >0
AND trn_po_prn_details_sales_order.ORDER_NO IS NULL
GROUP BY
ware_stocktransactions_bulk.intLocationId,
ware_stocktransactions_bulk.intOrderNo,
ware_stocktransactions_bulk.intOrderYear,
ware_stocktransactions_bulk.intSalesOrderId,
ware_stocktransactions_bulk.intItemId';



    }else if($rpt_type == 7) {

        $sql='SELECT
	temp_rm_moved_orders.`NO`,
	temp_rm_moved_orders.`YEAR`,
	trn_orderdetails.strSalesOrderNo,
	trn_orderdetails.strGraphicNo,
	trn_orderdetails.intSampleNo,
	trn_orderdetails.intSampleYear,
	trn_orderdetails.intRevisionNo,
	trn_orderdetails.strCombo,
	trn_orderdetails.strPrintName,
	mst_part.strName AS part,
	mst_locations.strName AS LOCATION,
	trn_orderdetails.intQty,
	mst_maincategory.strName AS MAIN_CATEGORY,
	mst_subcategory.strName AS SUB_CATEGORY,
	mst_item.strCode AS ITEM_CODE,
	mst_item.strName AS ITEM_NAME,
	mst_units.strCode AS UOM,
	trn_po_prn_details_sales_order.CONS_PC,
	trn_po_prn_details_sales_order.PRODUCTION_QTY,
	trn_po_prn_details_sales_order.REQUIRED,
	YEAR (
		ware_stocktransactions_bulk.dtDate
	)AS RM_MOVED_YEAR ,
	MONTHNAME(
		ware_stocktransactions_bulk.dtDate
	)RM_MOVED_MONTH ,
	sum(
		ifnull(
			ware_stocktransactions_bulk.dblQty *- 1,
			0
		)
	) AS qty,
	sum(
		ifnull(
			ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate * mst_financeexchangerate.dblBuying *- 1,
			0
		)
	) AS amount
FROM
	ware_stocktransactions_bulk
INNER JOIN (
	SELECT
		*
	FROM
		temp_rm_moved_orders /*limit 0,500*/
) AS temp_rm_moved_orders ON temp_rm_moved_orders.`NO` = ware_stocktransactions_bulk.intOrderNo
AND temp_rm_moved_orders.`YEAR` = ware_stocktransactions_bulk.intOrderYear
LEFT JOIN trn_po_prn_details_sales_order ON temp_rm_moved_orders.`NO` = trn_po_prn_details_sales_order.ORDER_NO
AND temp_rm_moved_orders.`YEAR` = trn_po_prn_details_sales_order.ORDER_YEAR
AND trn_po_prn_details_sales_order.SALES_ORDER = ware_stocktransactions_bulk.intSalesOrderId
AND trn_po_prn_details_sales_order.ITEM = ware_stocktransactions_bulk.intItemId
INNER JOIN trn_orderdetails ON ware_stocktransactions_bulk.intOrderNo = trn_orderdetails.intOrderNo
AND ware_stocktransactions_bulk.intOrderYear = trn_orderdetails.intOrderYear
AND ware_stocktransactions_bulk.intSalesOrderId = trn_orderdetails.intSalesOrderId
INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN mst_item ON ware_stocktransactions_bulk.intItemId = mst_item.intId
INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
LEFT JOIN mst_financeexchangerate ON mst_financeexchangerate.dtmDate = ware_stocktransactions_bulk.dtGRNDate
AND mst_financeexchangerate.intCompanyId = ware_stocktransactions_bulk.intCompanyId
AND mst_financeexchangerate.intCurrencyId = ware_stocktransactions_bulk.intCurrencyId
LEFT JOIN mst_locations ON ware_stocktransactions_bulk.intLocationId = mst_locations.intId
WHERE
ware_stocktransactions_bulk.strType IN (\'ISSUE\',\'RETSTORES\')
AND DATE(ware_stocktransactions_bulk.dtDate) >='.$date[0].'
AND ware_stocktransactions_bulk.intOrderNo >0
GROUP BY
ware_stocktransactions_bulk.intLocationId,
ware_stocktransactions_bulk.intOrderNo,
ware_stocktransactions_bulk.intOrderYear,
ware_stocktransactions_bulk.intSalesOrderId,
ware_stocktransactions_bulk.intItemId,
YEAR(ware_stocktransactions_bulk.dtDate),
MONTH(ware_stocktransactions_bulk.dtDate)';

}else if($rpt_type == 8) {
        $sql='SELECT
temp_rm_moved_orders.`NO`,
temp_rm_moved_orders.`YEAR`,
trn_orderdetails.strSalesOrderNo,
trn_orderdetails.strGraphicNo,
trn_orderdetails.intSampleNo,
trn_orderdetails.intSampleYear,
trn_orderdetails.intRevisionNo,
trn_orderdetails.strCombo,
trn_orderdetails.strPrintName,
mst_part.strName AS part,
mst_locations.strName AS LOCATION,
trn_orderdetails.intQty,
mst_maincategory.strName AS MAIN_CATEGORY,
mst_subcategory.strName AS SUB_CATEGORY,
mst_item.strCode AS ITEM_CODE,
mst_item.strName AS ITEM_NAME,
mst_units.strCode AS UOM,
trn_po_prn_details_sales_order.CONS_PC,
trn_po_prn_details_sales_order.PRODUCTION_QTY,
trn_po_prn_details_sales_order.REQUIRED,
YEAR(ware_stocktransactions_bulk.dtDate) AS RM_MOVED_YEAR,
MONTHNAME(ware_stocktransactions_bulk.dtDate) AS RM_MOVED_MONTH,
ware_stocktransactions_bulk.intDocumentNo,
ware_stocktransactions_bulk.intDocumntYear,
DATE(ware_stocktransactions_bulk.dtDate) AS DATE,
sum(ifnull(ware_stocktransactions_bulk.dblQty*-1,0)) as qty,
sum(ifnull(ware_stocktransactions_bulk.dblQty*ware_stocktransactions_bulk.dblGRNRate*mst_financeexchangerate.dblBuying*-1,0)) as amount
FROM
 ware_stocktransactions_bulk
INNER JOIN
(select * from temp_rm_moved_orders /*limit 0,500*/) as temp_rm_moved_orders
ON temp_rm_moved_orders.`NO`= ware_stocktransactions_bulk.intOrderNo
AND temp_rm_moved_orders.`YEAR` = ware_stocktransactions_bulk.intOrderYear


LEFT JOIN trn_po_prn_details_sales_order
ON temp_rm_moved_orders.`NO` = trn_po_prn_details_sales_order.ORDER_NO
AND temp_rm_moved_orders.`YEAR` = trn_po_prn_details_sales_order.ORDER_YEAR
AND trn_po_prn_details_sales_order.SALES_ORDER = ware_stocktransactions_bulk.intSalesOrderId
AND trn_po_prn_details_sales_order.ITEM = ware_stocktransactions_bulk.intItemId

INNER JOIN trn_orderdetails ON ware_stocktransactions_bulk.intOrderNo = trn_orderdetails.intOrderNo
AND ware_stocktransactions_bulk.intOrderYear = trn_orderdetails.intOrderYear
AND ware_stocktransactions_bulk.intSalesOrderId = trn_orderdetails.intSalesOrderId
INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN mst_item ON ware_stocktransactions_bulk.intItemId = mst_item.intId
INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
LEFT JOIN mst_financeexchangerate on mst_financeexchangerate.dtmDate = ware_stocktransactions_bulk.dtGRNDate and mst_financeexchangerate.intCompanyId = ware_stocktransactions_bulk.intCompanyId
and mst_financeexchangerate.intCurrencyId = ware_stocktransactions_bulk.intCurrencyId
LEFT JOIN mst_locations ON ware_stocktransactions_bulk.intLocationId=mst_locations.intId
WHERE
ware_stocktransactions_bulk.strType IN (\'ISSUE\',\'RETSTORES\')
AND DATE(ware_stocktransactions_bulk.dtDate) >='.$date[0].'
AND ware_stocktransactions_bulk.intOrderNo >0
GROUP BY
ware_stocktransactions_bulk.intLocationId,
ware_stocktransactions_bulk.intOrderNo,
ware_stocktransactions_bulk.intOrderYear,
ware_stocktransactions_bulk.intSalesOrderId,
ware_stocktransactions_bulk.intItemId,
(ware_stocktransactions_bulk.intDocumentNo),
(ware_stocktransactions_bulk.intDocumntYear)';

}


    return $sql;
}

function getNonOrderWise ($previous_month,$current_month) {

    $str1 = $previous_month;
    $date_previous=explode(" ",$str1);
    $str2=$current_month;
    $date_current=explode(" ",$str2);

    $sql='SELECT
	mst_locations.strName AS LOCATION,
	mst_maincategory.strName AS MAIN_CATEGORY,
	mst_subcategory.strName AS SUB_CATEGORY,
	mst_item.strCode AS ITEM_CODE,
	mst_item.strName AS ITEM_NAME,
	mst_units.strCode AS UOM,
	YEAR (
		ware_stocktransactions_bulk.dtDate
	) AS MOVED_YEAR,
	MONTHNAME(
		ware_stocktransactions_bulk.dtDate
	) AS MOVED_MONTH,
	ware_stocktransactions_bulk.intDocumentNo,
	ware_stocktransactions_bulk.intDocumntYear,
	ware_stocktransactions_bulk.strType AS trans_type,
	DATE(
		ware_stocktransactions_bulk.dtDate
	) AS DATE,
	sum(
		ifnull(
			ware_stocktransactions_bulk.dblQty *- 1,
			0
		)
	) AS qty,
	sum(
		ifnull(
			ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate * mst_financeexchangerate.dblBuying *- 1,
			0
		)
	) AS amount
FROM
	ware_stocktransactions_bulk
INNER JOIN mst_item ON ware_stocktransactions_bulk.intItemId = mst_item.intId
INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
LEFT JOIN mst_financeexchangerate ON mst_financeexchangerate.dtmDate = ware_stocktransactions_bulk.dtGRNDate
AND mst_financeexchangerate.intCompanyId = ware_stocktransactions_bulk.intCompanyId
AND mst_financeexchangerate.intCurrencyId = ware_stocktransactions_bulk.intCurrencyId
LEFT JOIN mst_locations ON ware_stocktransactions_bulk.intLocationId = mst_locations.intId
WHERE
	ware_stocktransactions_bulk.strType IN (\'ISSUE\', \'RETSTORES\')
AND DATE(
	ware_stocktransactions_bulk.dtDate
) >= '.$date_previous[0].'
AND DATE(
	ware_stocktransactions_bulk.dtDate
) <= '.$date_current[0].'
AND (
	ware_stocktransactions_bulk.intOrderNo = 0
	OR ware_stocktransactions_bulk.intOrderNo = \'\'
	OR ware_stocktransactions_bulk.intOrderNo IS NULL
)
GROUP BY
	ware_stocktransactions_bulk.intLocationId,
	ware_stocktransactions_bulk.intItemId,
	(
		ware_stocktransactions_bulk.intDocumentNo
	),
	(
		ware_stocktransactions_bulk.intDocumntYear
	)';


    return $sql;
}
<?php
//ini_set('display_errors',1);
session_start();
try{


//include 		"../../../../../dataAccess/Connector.php";
include_once 	"libraries/mail/mail_bcc.php";

include_once  	("class/cls_commonFunctions_get.php");
include_once  	("class/cls_commonErrorHandeling_get.php");
include_once 	("class/tables/costing_sample_header.php");
include_once 	("class/tables/costing_sample_ink_type.php");
include_once 	("class/tables/costing_sample_rm_roll_form.php");
include_once 	("class/tables/costing_sample_rm_non_roll_form.php");
include_once 	("class/tables/costing_sample_rm_non_direct.php");
include_once 	("class/tables/costing_sample_routing_process.php");
include_once 	("class/tables/costing_sample_routing_process.php");
include_once 	("class/dateTime.php");
include_once 	"class/tables/sys_approvelevels.php";					
include_once 	"class/tables/menupermision.php";
include_once 	"class/tables/costing_sample_header.php";
include_once 	"class/tables/costing_sample_header_approved_by.php";
include_once 	"class/tables/trn_sampleinfomations_prices.php";						
include_once 	"class/cls_mail.php";						
 
$requestType 	    				= $_REQUEST['requestType'];
$userId 							= $_SESSION['userId'];
$locationId 						= $_SESSION['CompanyID'];

$apprLevels							= 2;
$programCode						= 'P0116';

//$obj_commonErr					= new cls_commonErrorHandeling_get($db);
//$obj_common						= new cls_commonFunctions_get($db);
$dateTime							= new dateTimes($db);
$costing_sample_header				= new costing_sample_header($db);
$costing_sample_ink_type			= new costing_sample_ink_type($db);
$costing_sample_rm_roll_form		= new costing_sample_rm_roll_form($db);
$costing_sample_rm_non_roll_form	= new costing_sample_rm_non_roll_form($db);
$costing_sample_rm_non_direct		= new costing_sample_rm_non_direct($db);
$costing_sample_routing_process		= new costing_sample_routing_process($db);
$sys_approvelevels 					= new sys_approvelevels($db);
$menupermision 						= new menupermision($db);
$costing_sample_header				= new costing_sample_header($db);
$costing_sample_header_approved_by 	= new costing_sample_header_approved_by($db);
$trn_sampleinfomations_prices		= new trn_sampleinfomations_prices($db);
$objMail 							= new cls_create_mail($db);


if($requestType=='saveConsumption') {
	//Get Values
	$sampleNo    = $_REQUEST['sampleNo'];
	$sample_year = $_REQUEST['sample_year'];
	$revisionNo  = $_REQUEST['revisionNo'];
	$comboName   = $_REQUEST['comboName'];
	$PrintName   = $_REQUEST['PrintName'];
	$foilItem    = $_REQUEST['foilItem'];
	$foilItem_w    = $_REQUEST['foilItem_w'];	
	$technique   = $_REQUEST['technique'];
	$color	    = $_REQUEST['color'];
	$arr 		 = json_decode($_REQUEST['arr'], true);

	$db->begin();
	
	
	$sql_D = "DELETE FROM cost_samplecostsheet_print_sizes 
			WHERE
				intSampleNo 	= '$sampleNo'
			AND intSampleYear	= '$sample_year'
			AND intRevisionNo 	= '$revisionNo'
			AND strComboName 	= '$comboName'
			AND strPrintName 	= '$PrintName'
			AND COLOR 			= '$color'
			AND TECHNIQUE 		= '$technique'
			AND intItem 		= '$foilItem'";
	$result = $db->RunQuery($sql_D);

	
	foreach($arr as $arrVal)
	{
		$serialNo      = $arrVal['serialNo'];
		$width 	       = $arrVal['width'];
		$height 	   = $arrVal['height'];
		$consum 	   = $arrVal['consum'];
        
		    //Delete previouse Record
			$savedStatus	= true;
			$sql = "SELECT MAX(intSerialNo) AS maxSerialNo FROM cost_samplecostsheet_print_sizes WHERE intSampleNo = '$sampleNo' AND intSampleYear = '$sample_year' AND intRevisionNo = '$revisionNo' AND strComboName = '$comboName' AND strPrintName = '$PrintName' AND TECHNIQUE = '$technique'  AND COLOR = '$color' AND intItem = '$foilItem' ";
			//echo $sql;
			$result = $db->RunQuery($sql);
			$row_Max = mysqli_fetch_array($result);
			$maxSerialNo = $row_Max['maxSerialNo'];
			$serialNoNew = $maxSerialNo + 1;
			
			//$serialNoNew = $serialNo+1
			$sql = "INSERT INTO `cost_samplecostsheet_print_sizes` (   `intSampleNo`, `intSampleYear`, `intRevisionNo`, `strComboName`, `strPrintName`,`COLOR`,`TECHNIQUE`, `intItem`, `intSerialNo`, `intPrintWidth`, `intPrintHeight`, `intConsumption`) VALUES ('$sampleNo',     '$sample_year', '$revisionNo', '$comboName', '$PrintName', '$color', '$technique','$foilItem', '$serialNoNew', '$width', '$height', '$consum')";
			
			$result = $db->RunQuery($sql);
			if(!$result && $savedStatus)
			{
				$savedStatus		= false;
				$err_msg		 	= $db->errormsg;
				$errorSql			= $sql;
			}
			$serialNoNew++;
	}
	
	$sql_t_c = "SELECT
			-- SUM(intConsumption) AS tot_consumption
IF(mst_item.intUOM=9 OR mst_item.intUOM=10,(SUM(intConsumption)/($foilItem_w)),SUM(intConsumption)) AS tot_consumption
		FROM
			cost_samplecostsheet_print_sizes
			INNER JOIN mst_item ON cost_samplecostsheet_print_sizes.intItem = mst_item.intId
		WHERE
			intSampleNo 	= '$sampleNo'
		AND intSampleYear	= '$sample_year'
		AND intRevisionNo 	= '$revisionNo'
		AND strComboName 	= '$comboName'
		AND strPrintName 	= '$PrintName'
		AND COLOR 			= '$color'
		AND TECHNIQUE 		= '$technique'
		AND intItem 		= '$foilItem'";

    $result_con = $db->RunQuery($sql_t_c);
	$row_Con = mysqli_fetch_array($result_con);
	$consumption = $row_Con['tot_consumption'];
	
	if($savedStatus) {
		$db->commit();
		$response['type'] = 'pass';
		$response['msg'] = 'Consumptions Saved successfully.';
		$response['con'] = $consumption;
	}
	else{
		$db->rollback();
		$response['type'] 		= 'fail';
		$response['msg'] 		= "Error";
		$response['q'] 			=  $sql;
	}
	//echo json_encode($response);
}
else if($requestType=='copyConsumption_same_revision') {
	//Get Values
	$sampleNo    = $_REQUEST['sampleNo'];
	$sample_year = $_REQUEST['sample_year'];
	$revisionNo  = $_REQUEST['revisionNo'];
	$comboName   = $_REQUEST['comboName'];
	$PrintName   = $_REQUEST['PrintName'];
    $qtyPricesEnabled = 0;
    $copyFromCostingHeader = $costing_sample_header->select("QTY_PRICES",null," SAMPLE_NO=".$sampleNo." AND SAMPLE_YEAR=".$sample_year." AND REVISION=".$revisionNo." AND COMBO = '".$comboName."' AND PRINT='".$PrintName."'" , null, null);
    $row	= mysqli_fetch_array($copyFromCostingHeader);
    $qtyPricesEnabled =  $row['QTY_PRICES'];
	$arr 		 = json_decode($_REQUEST['arr'], true);
	$savedStatus	= true;
	$db->begin();
		$costing_sample_header->set($sampleNo,$sample_year,$revisionNo,$comboName,$PrintName);	
		$prodLocation		=$costing_sample_header->getPRODUCTION_LOCATION();
		if($prodLocation=='')
			$prodLocation	=0;
		$estimatedQty		=$costing_sample_header->getESTIMATED_ORDER_QTY();
		if($estimatedQty=='')
			$estimatedQty	=0;
		$noOfColours		=$costing_sample_header->getNO_OF_COLOURS();
		$shots				=$costing_sample_header->getSHOTS();
		$costPerShot		=$costing_sample_header->getCOST_PER_SHOT();
		if($costPerShot=='')
			$costPerShot	=0;
		$inkCost			=$costing_sample_header->getINK_COST();
		$spRMCost			=$costing_sample_header->getSPECIAL_RM_COST();
		$processCost		=$costing_sample_header->getPROCESS_COST();
		$shotCost			=$costing_sample_header->getSHOT_COST();
		$otherCost			=$costing_sample_header->getOTHER_COST();
		$otherCostRemarks	=$costing_sample_header->getOTHER_COST_REMARKS();
		$totalCost			=$costing_sample_header->getTOTAL_COST();
		$marginPercen		=$costing_sample_header->getMARGIN_PERCENTAGE();
		$margin				=$costing_sample_header->getMARGIN();
		$scPrice			=$costing_sample_header->getSCREENLINE_PRICE();
		$approveLevels		=$costing_sample_header->getAPPROVE_LEVELS();
		$status				=$costing_sample_header->getSTATUS();
		$targetPrice		=$costing_sample_header->getTARGET_PRICE();
		$approvedPrice		=$costing_sample_header->getAPPROVED_PRICE();
		$ups				=$costing_sample_header->getUPS();
	
	foreach($arr as $arrVal)
	{
		$sampleNo_to   	= $sampleNo;
		$sample_year_to	= $sample_year;
		$revisionNo_to 	= $revisionNo;
		$comboName_to  	= $arrVal['combo'];
		$PrintName_to  	= $arrVal['print'];
	
		if(!$costing_sample_header->set($sampleNo_to,$sample_year_to,$revisionNo_to,$comboName_to,$PrintName_to))
			$editMode=0;
		else
			$editMode=1;
		if($editMode==0){
			$result_arr	= $costing_sample_header->insertRec($sampleNo_to,$sample_year_to,$revisionNo_to,$comboName_to,$PrintName_to,$prodLocation,$ups,$estimatedQty,$noOfColours,$shots,$costPerShot,$inkCost,$spRMCost,$processCost,$shotCost,$otherCost,$otherCostRemarks,$totalCost,$marginPercen,$margin,$scPrice,$targetPrice,$approvedPrice,1,1,$sampleNo,$sample_year,$revisionNo,$comboName,$PrintName,$approveLevels+1,$approveLevels,$userId,$dateTime->getCurruntDateTime(),$qtyPricesEnabled);
			//echo $db->getsql();

	if($result_arr['savedStatus']=='fail' && $savedStatus)
			{
			$savedStatus	= false;
			$savedMasseged	= $result_arr['savedMassege'];
			//$error_sql		= $result_arr['error_sql'];
			$error_sql		= $result_arr[$db->getsql()];
			}
			
		}
		
		else{
			$costing_sample_header->set($sampleNo_to,$sample_year_to,$revisionNo_to,$comboName_to,$PrintName_to);
			$costing_sample_header->setPRODUCTION_LOCATION($prodLocation);
			$costing_sample_header->setUPS($sessions->getCompanyId());
			$costing_sample_header->setESTIMATED_ORDER_QTY($estimatedQty);
			$costing_sample_header->setNO_OF_COLOURS($noOfColours);
			$costing_sample_header->setSHOTS($shots);
			$costing_sample_header->setCOST_PER_SHOT($costPerShot);
			$costing_sample_header->setINK_COST($inkCost);
			$costing_sample_header->setSPECIAL_RM_COST($spRMCost);
			$costing_sample_header->setPROCESS_COST($processCost);
			$costing_sample_header->setSHOT_COST($shotCost);
			$costing_sample_header->setOTHER_COST($otherCost);
			$costing_sample_header->setOTHER_COST_REMARKS($otherCostRemarks);
			$costing_sample_header->setTOTAL_COST($totalCost);
			$costing_sample_header->setMARGIN_PERCENTAGE($marginPercen);
			$costing_sample_header->setMARGIN($margin);
			$costing_sample_header->setSCREENLINE_PRICE($scPrice);
			$costing_sample_header->setTARGET_PRICE($targetPrice);
			$costing_sample_header->setAPPROVED_PRICE($approvedPrice);
			$costing_sample_header->setSTATUS($approveLevels+1);
			$costing_sample_header->setCOPPIED_FLAG(1);
            $costing_sample_header->setQTY_PRICES($qtyPricesEnabled);
			$result_arr	= $costing_sample_header->commit();
		}
	
		$costing_sample_header->set($sampleNo_to,$sample_year_to,$revisionNo_to,$comboName_to,$PrintName_to);
		$costing_sample_header->setCOPPIED_FROM_SAMPLE_NO($sampleNo);
		$costing_sample_header->setCOPPIED_FROM_SAMPLE_YEAR($sample_year);
		$costing_sample_header->setCOPPIED_FROM_REVISION($revisionNo);
		$costing_sample_header->setCOPPIED_FROM_COMBO($comboName);
		$costing_sample_header->setCOPPIED_FROM_PRINT($PrintName);
		$result_arr	= $costing_sample_header->commit();
        if(!$result_arr['status'])
            throw new Exception($result_arr['msg']);

        if ($qtyPricesEnabled ==1){
            $qtyResult = get_qty_wise_prices($sampleNo, $sample_year, $revisionNo, $comboName, $PrintName);
            if (mysqli_num_rows($qtyResult) > 0) {
                $sql = "INSERT INTO `costing_sample_qty_wise` (`SAMPLE_YEAR`,`SAMPLE_NO`,`REVISION`,`COMBO`,`PRINT`,`LOWER_MARGIN`,`UPPER_MARGIN`,`PRICE`) VALUES ";
                while ($row = mysqli_fetch_array($qtyResult)) {
                    $upperMargin = $row['UPPER_MARGIN'];
                    $lowerMargin = $row['LOWER_MARGIN'];
                    $qtyPrice = $row['PRICE'];
                    $sql .= " ('$sample_year_to','$sampleNo_to','$revisionNo_to','$comboName_to','$PrintName_to','$lowerMargin','$upperMargin','$qtyPrice'),";
                }

                $deleteSql = "DELETE FROM costing_sample_qty_wise WHERE SAMPLE_NO='$sampleNo_to' AND SAMPLE_YEAR ='$sample_year_to' AND REVISION='$revisionNo_to' 
                  AND COMBO = '$comboName_to' AND PRINT = '$PrintName_to'";
                $deleted = $db->RunQuery($deleteSql);
                $sql = rtrim($sql, ',');
                $result = $db->RunQuery($sql);

                if ($result != 1) {
                    throw new Exception('Qty Prices Not Saved');
                }
            }
        }
		//delete ink Type///////////////////////////////////////////
		$result_arr = $costing_sample_ink_type->delete(" SAMPLE_NO = '$sampleNo_to' AND SAMPLE_YEAR = '$sample_year_to' 
		AND REVISION = '$revisionNo_to' AND COMBO = '$comboName_to' AND PRINT = '$PrintName_to'");
		//echo $db->getsql();
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
	
		//delete RM-Roll form////////////////////////////////////////
		$result_arr = $costing_sample_rm_roll_form->delete(" SAMPLE_NO = $sampleNo_to AND SAMPLE_YEAR = $sample_year_to AND REVISION = $revisionNo_to AND COMBO = '$comboName_to' AND PRINT = '$PrintName_to'");
		//echo $db->getsql();
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
	
		//delete RM-non_roll form
		$result_arr = $costing_sample_rm_non_roll_form->delete(" SAMPLE_NO = $sampleNo_to AND SAMPLE_YEAR = $sample_year_to AND REVISION = $revisionNo_to AND COMBO = '$comboName_to' AND PRINT = '$PrintName_to'");
		//echo $db->getsql();
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
	
		//delete RM-non_direct
		$result_arr = $costing_sample_rm_non_direct->delete(" SAMPLE_NO = $sampleNo_to AND SAMPLE_YEAR = $sample_year_to AND REVISION_NO = $revisionNo_to AND COMBO = '$comboName_to' AND PRINT = '$PrintName_to'");
		//echo $db->getsql();
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
	
		//delete process
		$result_arr = $costing_sample_routing_process->delete(" SAMPLE_NO = $sampleNo_to AND SAMPLE_YEAR = $sample_year_to AND REVISION = $revisionNo_to AND COMBO = '$comboName_to' AND PRINT = '$PrintName_to'");
		//echo $db->getsql();
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
        $copied_process_arr = $costing_sample_routing_process->select("*", null," SAMPLE_NO = $sampleNo AND SAMPLE_YEAR = $sample_year AND REVISION = $revisionNo AND COMBO = '$comboName' AND PRINT = '$PrintName'",null,null);
        while($row=mysqli_fetch_array($copied_process_arr)){
            $result_arr		= $costing_sample_routing_process->insertRec($sampleNo,$sample_year,$revisionNo,$comboName_to,$PrintName_to,$row['ROUTING_ID'],$row['PROCESS_ID'],$row['ORDER'],$row['COST']);
        }
	
	
	}
        
	if($savedStatus) {
		$db->commit();
		$response['type'] = 'pass';
		$response['msg'] = 'Consumptions Coppied successfully.';
		$response['con'] = $consumption;
	}
	else{
		$db->rollback();
		$response['type'] 		= 'fail';
		$response['msg'] 		= "Error";
		$response['q'] 			=  $sql;
	}
	//echo json_encode($response);
}
else if($requestType=='DeleteSerialConsumption'){
	$serialNum_deleted    = $_REQUEST['serialNum_deleted'];
	$sampleNo = $_REQUEST['sampleNo'];
	$sample_year = $_REQUEST['sample_year'];
	$revisionNo  = $_REQUEST['revisionNo'];
	$comboName   = $_REQUEST['comboName'];
	$PrintName   = $_REQUEST['PrintName'];
	$foilItem    = $_REQUEST['foilItem'];
	$technique   = $_REQUEST['technique'];
	$color   	 = $_REQUEST['color'];
	$db->begin();
	$sql = "DELETE FROM `cost_samplecostsheet_print_sizes` WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sample_year') AND (`intRevisionNo`='$revisionNo') AND (`strComboName`='$comboName') AND (`strPrintName`='$PrintName') AND (`TECHNIQUE`='$technique') AND (`COLOR`='$color') AND (`intItem`='$foilItem') AND (`intSerialNo`='$serialNum_deleted')";
	$result = $db->RunQuery2($sql);

	$sql_t_c = "SELECT
			SUM(intConsumption) AS tot_consumption
		FROM
			cost_samplecostsheet_print_sizes
		WHERE
			intSampleNo 	= '$sampleNo'
		AND intSampleYear 	= '$sample_year'
		AND intRevisionNo 	= '$revisionNo'
		AND strComboName 	= '$comboName'
		AND strPrintName 	= '$PrintName'
		AND COLOR 			= '$color'
		AND TECHNIQUE 		= '$technique'
		AND intItem 		= '$foilItem'";

    $result_con = $db->RunQuery2($sql_t_c);
	$row_Con = mysqli_fetch_array($result_con);
	$consumption = $row_Con['tot_consumption'];

	if($result) {
		//$db->commit();
		$response['type'] = 'pass';
		$response['msg']  = 'Consumptions Deleted successfully.';
		$response['con']  = $consumption;
	}
	else{
		$db->rollback();
		$response['type'] 		= 'fail';
		$response['msg'] 		= $db->errormsg;
		$response['q'] 			= $sql;
	}
	//echo json_encode($response);
}
else if($requestType=='saveCosting'){
   //print_r($_REQUEST);
	$sampleNo    		= $_REQUEST['sampleNo'];
	$sampleYear 		= $_REQUEST['sampleYear'];
	$revisionNo  		= $_REQUEST['revisionNo'];
	$comboName   		= $_REQUEST['combo'];
	$PrintName   		= $_REQUEST['print'];
	$coppiedFlag   		= $_REQUEST['coppiedFlag'];
	$sampleNoCopyFrom	= $_REQUEST['sampleNoC'];
	$sampleYearCopyFrom	= $_REQUEST['sampleYearC'];
	$revisionNoCopyFrom	= $_REQUEST['revisionNoC'];
	$comboNameCopyFrom	= $_REQUEST['comboC'];
	$PrintNameCopyFrom	= $_REQUEST['printC'];
	$estimatedQty    	= $_REQUEST['estimatedQty'];
    $qtyPriceEnabled    = $_REQUEST['qtyPriceEnabled'];
	if($estimatedQty=='')
		$estimatedQty	=0;
	$prodLocation   	= $_REQUEST['prod_location'];
	if($prodLocation=='')
		$prodLocation	=0;
	$routing		   	= $_REQUEST['routing'];
	if($routing=='')
		$routing		= "'".NULL."'";
	$ups   				= $_REQUEST['ups'];
	$noOfColours   		= $_REQUEST['colors'];
	$shots   			= $_REQUEST['shots'];
	if($shots=='')
		$shots			= 0;
	$costPerShot   		= $_REQUEST['cost_per_shot'];
	if($costPerShot=='')
		$costPerShot	=0;
	$inkCost   			= $_REQUEST['inkCost'];
	$spRMCost   		= $_REQUEST['spRMCost'];
	$processCost   		= $_REQUEST['process_cost'];
	$shotCost   		= $_REQUEST['shot_cost'];
	$otherCost   		= $_REQUEST['other_cost'];
	$otherCostRemarks   = $_REQUEST['other_cost_remarks'];
	$totalCost   		= $_REQUEST['total_cost'];
	$marginPercen   	= $_REQUEST['margin_percentage'];
	$margin   			= $_REQUEST['margin'];
	$scPrice   			= $_REQUEST['screenline_price'];
	$targetPrice   		= $_REQUEST['target_price'];
	$approvedPrice   	= $_REQUEST['approved_price'];
	
	
	$arrInkType	 		= json_decode($_REQUEST['arrInkType'], true);
	$arrRmRoll	 		= json_decode($_REQUEST['arrRMroll'], true);
	$arrRMnonRoll 		= json_decode($_REQUEST['arrRMnonRoll'], true);
	$arrRMnonDirect		= json_decode($_REQUEST['arrRMnonDirect'], true);
	$arrProcess	 		= json_decode($_REQUEST['arrProcess'], true);

	$db->begin();
    if ($qtyPriceEnabled==1){
        $qtyPricesArray = get_qty_wise_prices($sampleNo,$sampleYear,$revisionNo,$comboName,$PrintName);
        if (mysqli_num_rows($qtyPricesArray) == 0){
            throw new Exception("Enter Quantity Wise Prices");
        }
    }

	if(!$costing_sample_header->set($sampleNo,$sampleYear,$revisionNo,$comboName,$PrintName)){
		$editMode=0;
		//$journal_array 	= $obj_common->GetSystemMaxNo('PRE_COSTING',$locationId);
		//$serialNo		= $journal_array["max_no"];
		//$serialYear		= date('Y');
		$sys_approvelevels->set($programCode);
		$approveLevels	= $sys_approvelevels->getintApprovalLevel();
		$status			=$approveLevels+1;
		
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Edit',$status,$approveLevels);
		
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);
	}
	else{
		$editMode=1;
		$costing_sample_header->set($sampleNo,$sampleYear,$revisionNo,$comboName,$PrintName);
	
		$status			=$costing_sample_header->getSTATUS();
		$sys_approvelevels->set($programCode);
		$approveLevels	= $sys_approvelevels->getintApprovalLevel();
		$status			=$approveLevels+1;
		
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Edit',$status,$approveLevels);
		
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);
	}


	
	if($editMode==0){
		$result_arr	=$costing_sample_header->insertRec($sampleNo,$sampleYear,$revisionNo,$comboName,$PrintName,$prodLocation,$ups,$estimatedQty,$noOfColours,$shots,$costPerShot,$inkCost,$spRMCost,$processCost,$shotCost,$otherCost,$otherCostRemarks,$totalCost,$marginPercen,$margin,$scPrice,$targetPrice,$approvedPrice,0,0,'NULL','NULL','NULL','NULL','NULL',$status,$approveLevels,$userId,$dateTime->getCurruntDateTime(),$qtyPriceEnabled);
		//echo $db->getsql();
	}
	
	else{
		$costing_sample_header->set($sampleNo,$sampleYear,$revisionNo,$comboName,$PrintName);
		$costing_sample_header->setPRODUCTION_LOCATION($prodLocation);
		$costing_sample_header->setUPS($ups);
		$costing_sample_header->setESTIMATED_ORDER_QTY($estimatedQty);
		$costing_sample_header->setNO_OF_COLOURS($noOfColours);
		$costing_sample_header->setSHOTS($shots);
		$costing_sample_header->setCOST_PER_SHOT($costPerShot);
		$costing_sample_header->setINK_COST($inkCost);
		$costing_sample_header->setSPECIAL_RM_COST($spRMCost);
		$costing_sample_header->setPROCESS_COST($processCost);
		$costing_sample_header->setSHOT_COST($shotCost);
		$costing_sample_header->setOTHER_COST($otherCost);
		$costing_sample_header->setOTHER_COST_REMARKS($otherCostRemarks);
		$costing_sample_header->setTOTAL_COST($totalCost);
		$costing_sample_header->setMARGIN_PERCENTAGE($marginPercen);
		$costing_sample_header->setMARGIN($margin);
		$costing_sample_header->setSCREENLINE_PRICE($scPrice);
		$costing_sample_header->setTARGET_PRICE($targetPrice);
		$costing_sample_header->setAPPROVED_PRICE($approvedPrice);
		$costing_sample_header->setCOPPIED_FLAG(0);
		$costing_sample_header->setSTATUS($status);
		$costing_sample_header->setAPPROVE_LEVELS($approveLevels);
		$costing_sample_header->setQTY_PRICES($qtyPriceEnabled);
        if($sampleNoCopyFrom > 0){
		$costing_sample_header->setCOPPIED_FROM_SAMPLE_NO($sampleNoCopyFrom);
		$costing_sample_header->setCOPPIED_FROM_SAMPLE_YEAR($sampleYearCopyFrom);
		$costing_sample_header->setCOPPIED_FROM_REVISION($revisionNoCopyFrom);
		$costing_sample_header->setCOPPIED_FROM_COMBO($comboNameCopyFrom);
		$costing_sample_header->setCOPPIED_FROM_PRINT($PrintNameCopyFrom);
		}
		else{
		$costing_sample_header->setCOPPIED_FROM_SAMPLE_NO("NULL");
		$costing_sample_header->setCOPPIED_FROM_SAMPLE_YEAR("NULL");
		$costing_sample_header->setCOPPIED_FROM_REVISION("NULL");
		$costing_sample_header->setCOPPIED_FROM_COMBO("NULL");
		$costing_sample_header->setCOPPIED_FROM_PRINT("NULL");
		}
	
		$result_arr	= $costing_sample_header->commit();
		//echo $db->getsql();
	}
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	
	
	//save ink Type///////////////////////////////////////////
	$result_arr = $costing_sample_ink_type->delete(" SAMPLE_NO = $sampleNo AND SAMPLE_YEAR = $sampleYear AND REVISION = $revisionNo AND COMBO = '$comboName' AND PRINT = '$PrintName'");
	//echo $db->getsql();
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	
	foreach($arrInkType as $arrVal)
	{
		$colour      		= $arrVal['colour'];
		$technique      	= $arrVal['technique'];
		$ink_type      		= $arrVal['ink_type'];
		$shots      		= $arrVal['shots'];
		$cost_per_sq_inch	= $arrVal['cost_per_sq_inch'];
		$square_inches      = $arrVal['square_inches'];
		$usage      		= $arrVal['usage'];
		$total_cost      	= $arrVal['total_cost'];
	
		
		$result_arr		= $costing_sample_ink_type->insertRec($sampleNo,$sampleYear,$revisionNo,$comboName,$PrintName,$colour,$technique,$ink_type,$shots,$cost_per_sq_inch,$square_inches,$usage,$total_cost);
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
	
	}
	
	
	//save RM-Roll form////////////////////////////////////////
	$result_arr = $costing_sample_rm_roll_form->delete(" SAMPLE_NO = $sampleNo AND SAMPLE_YEAR = $sampleYear AND REVISION = $revisionNo AND COMBO = '$comboName' AND PRINT = '$PrintName'");
	//echo $db->getsql();
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	
	foreach($arrRmRoll as $arrValRmRoll)
	{
		$color		  	 = $arrValRmRoll['color'];
		$technique  	 = $arrValRmRoll['technique'];
		$item      		 = $arrValRmRoll['item'];
		$printWidth_roll = $arrValRmRoll['printWidth_roll'];
		$printHeight_roll= $arrValRmRoll['printHeight_roll'];
		$itemWidth		 = $arrValRmRoll['itemWidth'];
		$itemHeight      = $arrValRmRoll['itemHeight'];
		$price_roll      = $arrValRmRoll['price_roll'];
		$consump_roll    = $arrValRmRoll['consump_roll'];
		$cost_roll       = $arrValRmRoll['cost_roll'];
	
		
		$result_arr		= $costing_sample_rm_roll_form->insertRec($sampleNo,$sampleYear,$revisionNo,$comboName,$PrintName,$color,$technique,$item,$printWidth_roll,$printHeight_roll,$itemWidth,$itemHeight,$price_roll,$consump_roll,$cost_roll);
		//echo $db->getsql();
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
	
	}
	
	
	//save RM-non_roll form
	
	$result_arr = $costing_sample_rm_non_roll_form->delete(" SAMPLE_NO = $sampleNo AND SAMPLE_YEAR = $sampleYear AND REVISION = $revisionNo AND COMBO = '$comboName' AND PRINT = '$PrintName'");
	//echo $db->getsql();
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	
	foreach($arrRMnonRoll as $arrValRmRoll)
	{
		$color		  	 = $arrValRmRoll['color'];
		$technique  	 = $arrValRmRoll['technique'];
		$item      		 = $arrValRmRoll['item'];
		$printWidth_roll = $arrValRmRoll['printWidth_roll'];
		$printHeight_roll= $arrValRmRoll['printHeight_roll'];
		$price_roll      = $arrValRmRoll['price_roll'];
		$consump_roll    = $arrValRmRoll['consump_roll'];
		$cost_roll       = $arrValRmRoll['cost_roll'];
	
		
		$result_arr		= $costing_sample_rm_non_roll_form->insertRec($sampleNo,$sampleYear,$revisionNo,$comboName,$PrintName,$color,$technique,$item,$printWidth_roll,$printHeight_roll,$price_roll,$consump_roll,$cost_roll);
		//echo $db->getsql();
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
	
	}
	
	
	//save RM-non_direct
	
	$result_arr = $costing_sample_rm_non_direct->delete(" SAMPLE_NO = $sampleNo AND SAMPLE_YEAR = $sampleYear AND REVISION_NO = $revisionNo AND COMBO = '$comboName' AND PRINT = '$PrintName'");
	//echo $db->getsql();
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	
	foreach($arrRMnonDirect as $arrValRmRoll)
	{
		$item      		 = $arrValRmRoll['item'];
		$printWidth_roll = $arrValRmRoll['printWidth_roll'];
		$printHeight_roll= $arrValRmRoll['printHeight_roll'];
		$price_roll      = $arrValRmRoll['price_roll'];
		$consump_roll    = $arrValRmRoll['consump_roll'];
		$cost_roll       = $arrValRmRoll['cost_roll'];
	
		
		$result_arr		= $costing_sample_rm_non_direct->insertRec($sampleNo,$sampleYear,$revisionNo,$comboName,$PrintName,$item,$printWidth_roll,$printHeight_roll,$price_roll,$consump_roll,$cost_roll);
		//echo $db->getsql();
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
	
	}
	
	//save process
	
	$result_arr = $costing_sample_routing_process->delete(" SAMPLE_NO = $sampleNo AND SAMPLE_YEAR = $sampleYear AND REVISION = $revisionNo AND COMBO = '$comboName' AND PRINT = '$PrintName'");
	//echo $db->getsql();
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	
	foreach($arrProcess as $arrValRmRoll)
	{
		$process     = $arrValRmRoll['process'];
		$order   	 = $arrValRmRoll['order'];
		$price       = $arrValRmRoll['price'];
	
		
		$result_arr		= $costing_sample_routing_process->insertRec($sampleNo,$sampleYear,$revisionNo,$comboName,$PrintName,$routing,$process,$order,$price);
		//echo $db->getsql();
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
	
	}


	//if($result) {
		$db->commit();
		$response['type'] = 'pass';
		$response['msg']  = 'Pre Costing Saved Successfully.';
		$response['con']  = $consumption;
	/*}
	else{
		$db->rollback();
		$response['type'] 		= 'fail';
		//$response['msg'] 		= $db->errormsg;
		$response['q'] 			= $sql;
	}*/
	//echo json_encode($response);


}
else if($requestType=='copyCosting'){

  // print_r($_REQUEST);
	$sampleNo    		= $_REQUEST['sampleNo'];
	$sampleYear 		= $_REQUEST['sampleYear'];
	$revisionNo  		= $_REQUEST['revisionNo'];
	$comboName   		= $_REQUEST['combo'];
	$PrintName   		= $_REQUEST['print'];
	$coppiedFlag   		= $_REQUEST['coppiedFlag'];
	$sampleNoCopyFrom	= $_REQUEST['sampleNoC'];
	$sampleYearCopyFrom	= $_REQUEST['sampleYearC'];
	$revisionNoCopyFrom	= $_REQUEST['revisionNoC'];
	$comboNameCopyFrom	= $_REQUEST['comboC'];
	$PrintNameCopyFrom	= $_REQUEST['printC'];
	$estimatedQty    	= $_REQUEST['estimatedQty'];
    $qtyPricesEnabled = $_REQUEST['qtyPriceEnabled'];
	if($estimatedQty=='')
		$estimatedQty	=0;
	$prodLocation   	= $_REQUEST['prod_location'];
	if($prodLocation=='')
		$prodLocation	=0;
	$routing		   	= $_REQUEST['routing'];
	$ups   				= $_REQUEST['ups'];
	$noOfColours   		= $_REQUEST['colors'];
	$shots   			= $_REQUEST['shots'];
	$costPerShot   		= $_REQUEST['cost_per_shot'];
	if($costPerShot=='')
		$costPerShot	=0;
	$inkCost   			= $_REQUEST['inkCost'];
	$spRMCost   		= $_REQUEST['spRMCost'];
	$processCost   		= $_REQUEST['process_cost'];
	$shotCost   		= $_REQUEST['shot_cost'];
	$otherCost   		= $_REQUEST['other_cost'];
	$otherCostRemarks   = $_REQUEST['other_cost_remarks'];
	$totalCost   		= $_REQUEST['total_cost'];
	$marginPercen   	= $_REQUEST['margin_percentage'];
	$margin   			= $_REQUEST['margin'];
	$scPrice   			= $_REQUEST['screenline_price'];
	$targetPrice   		= $_REQUEST['target_price'];
	$approvedPrice   	= $_REQUEST['approved_price'];
	
	
	$arrInkType	 		= json_decode($_REQUEST['arrInkType'], true);
	$arrRmRoll	 		= json_decode($_REQUEST['arrRMroll'], true);
	$arrRMnonRoll 		= json_decode($_REQUEST['arrRMnonRoll'], true);
	$arrRMnonDirect		= json_decode($_REQUEST['arrRMnonDirect'], true);
	$arrProcess	 		= json_decode($_REQUEST['arrProcess'], true);

	$db->begin();
	
		
	if(!$costing_sample_header->set($sampleNo,$sampleYear,$revisionNo,$comboName,$PrintName)){
		$editMode=0;
		//$journal_array 	= $obj_common->GetSystemMaxNo('PRE_COSTING',$locationId);
		//$serialNo		= $journal_array["max_no"];
		//$serialYear		= date('Y');
		$sys_approvelevels->set($programCode);
		$approveLevels	= $sys_approvelevels->getintApprovalLevel();
		$status			=$approveLevels+1;
		
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Edit',$status,$approveLevels);
		
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);
	}
	else{
		$editMode=1;
		$costing_sample_header->set($sampleNo,$sampleYear,$revisionNo,$comboName,$PrintName);
	
		$status			=$costing_sample_header->getSTATUS();
		$sys_approvelevels->set($programCode);
		$approveLevels	= $sys_approvelevels->getintApprovalLevel();
		$status			=$approveLevels+1;
		
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Edit',$status,$approveLevels);
		
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);
	}


	
	if($editMode==0){
		$result_arr	=$costing_sample_header->insertRec($sampleNo,$sampleYear,$revisionNo,$comboName,$PrintName,$prodLocation,$ups,$estimatedQty,$noOfColours,$shots,$costPerShot,$inkCost,$spRMCost,$processCost,$shotCost,$otherCost,$otherCostRemarks,$totalCost,$marginPercen,$margin,$scPrice,$targetPrice,$approvedPrice,1,1,$sampleNoCopyFrom,$sampleYearCopyFrom,$revisionNoCopyFrom,$comboNameCopyFrom,$PrintNameCopyFrom,$status,$approveLevels,$userId,$dateTime->getCurruntDateTime(),$qtyPricesEnabled);
		//echo $db->getsql();
	}
	
	else{
		$costing_sample_header->set($sampleNo,$sampleYear,$revisionNo,$comboName,$PrintName);
		$costing_sample_header->setPRODUCTION_LOCATION($prodLocation);
		$costing_sample_header->setUPS($sessions->getCompanyId());
		$costing_sample_header->setESTIMATED_ORDER_QTY($estimatedQty);
		$costing_sample_header->setNO_OF_COLOURS($noOfColours);
		$costing_sample_header->setSHOTS($shots);
		$costing_sample_header->setCOST_PER_SHOT($costPerShot);
		$costing_sample_header->setINK_COST($inkCost);
		$costing_sample_header->setSPECIAL_RM_COST($spRMCost);
		$costing_sample_header->setPROCESS_COST($processCost);
		$costing_sample_header->setSHOT_COST($shotCost);
		$costing_sample_header->setOTHER_COST($otherCost);
		$costing_sample_header->setOTHER_COST_REMARKS($otherCostRemarks);
		$costing_sample_header->setTOTAL_COST($totalCost);
		$costing_sample_header->setMARGIN_PERCENTAGE($marginPercen);
		$costing_sample_header->setMARGIN($margin);
		$costing_sample_header->setSCREENLINE_PRICE($scPrice);
		$costing_sample_header->setTARGET_PRICE($targetPrice);
		$costing_sample_header->setAPPROVED_PRICE($approvedPrice);
		$costing_sample_header->setCOPPIED_FLAG(1);
		$costing_sample_header->setCOPPIED_TYPE(1);
		$costing_sample_header->setSTATUS($status);
		$costing_sample_header->setAPPROVE_LEVELS($approveLevels);

		if($sampleNoCopyFrom > 0){
			$costing_sample_header->setCOPPIED_FROM_SAMPLE_NO($sampleNoCopyFrom);
			$costing_sample_header->setCOPPIED_FROM_SAMPLE_YEAR($sampleYearCopyFrom);
			$costing_sample_header->setCOPPIED_FROM_REVISION($revisionNoCopyFrom);
			$costing_sample_header->setCOPPIED_FROM_COMBO($comboNameCopyFrom);
			$costing_sample_header->setCOPPIED_FROM_PRINT($PrintNameCopyFrom);
		}
        $costing_sample_header->setQTY_PRICES($qtyPricesEnabled);
		$result_arr	= $costing_sample_header->commit();
		//echo $db->getsql();
	}
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
    if ($qtyPricesEnabled ==1){
        $qtyResult = get_qty_wise_prices($sampleNoCopyFrom, $sampleYearCopyFrom, $revisionNoCopyFrom, $comboNameCopyFrom, $PrintNameCopyFrom);
        if (mysqli_num_rows($qtyResult) > 0) {
            $sql = "INSERT INTO `costing_sample_qty_wise` (`SAMPLE_YEAR`,`SAMPLE_NO`,`REVISION`,`COMBO`,`PRINT`,`LOWER_MARGIN`,`UPPER_MARGIN`,`PRICE`) VALUES ";
            while ($row = mysqli_fetch_array($qtyResult)) {
                $upperMargin = $row['UPPER_MARGIN'];
                $lowerMargin = $row['LOWER_MARGIN'];
                $qtyPrice = $row['PRICE'];
                $sql .= " ('$sampleYear','$sampleNo','$revisionNo','$comboName','$PrintName','$lowerMargin','$upperMargin','$qtyPrice'),";
            }

            $deleteSql = "DELETE FROM costing_sample_qty_wise WHERE SAMPLE_NO='$sampleNo' AND SAMPLE_YEAR ='$sampleYear' AND REVISION='$revisionNo' 
                  AND COMBO = '$comboName' AND PRINT = '$PrintName'";
            $deleted = $db->RunQuery($deleteSql);
            $sql = rtrim($sql, ',');
            $result = $db->RunQuery($sql);

            if ($result != 1) {
                throw new Exception('Qty Prices Not Saved');
            }
        }
    }
	//delete ink Type///////////////////////////////////////////
	$result_arr = $costing_sample_ink_type->delete(" SAMPLE_NO = $sampleNo AND SAMPLE_YEAR = $sampleYear AND REVISION = $revisionNo AND COMBO = '$comboName' AND PRINT = '$PrintName'");
	//echo $db->getsql();
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);

	//delete RM-Roll form////////////////////////////////////////
	$result_arr = $costing_sample_rm_roll_form->delete(" SAMPLE_NO = $sampleNo AND SAMPLE_YEAR = $sampleYear AND REVISION = $revisionNo AND COMBO = '$comboName' AND PRINT = '$PrintName'");
	//echo $db->getsql();
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);

	//delete RM-non_roll form
	$result_arr = $costing_sample_rm_non_roll_form->delete(" SAMPLE_NO = $sampleNo AND SAMPLE_YEAR = $sampleYear AND REVISION = $revisionNo AND COMBO = '$comboName' AND PRINT = '$PrintName'");
	//echo $db->getsql();
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);

	//delete RM-non_direct
	$result_arr = $costing_sample_rm_non_direct->delete(" SAMPLE_NO = $sampleNo AND SAMPLE_YEAR = $sampleYear AND REVISION_NO = $revisionNo AND COMBO = '$comboName' AND PRINT = '$PrintName'");
	//echo $db->getsql();
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);

	//delete process
	$result_arr = $costing_sample_routing_process->delete(" SAMPLE_NO = $sampleNo AND SAMPLE_YEAR = $sampleYear AND REVISION = $revisionNo AND COMBO = '$comboName' AND PRINT = '$PrintName'");
	//echo $db->getsql();
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
    $copied_process_arr = $costing_sample_routing_process->select("*", null," SAMPLE_NO = $sampleNoCopyFrom AND SAMPLE_YEAR = $sampleYearCopyFrom AND REVISION = $revisionNoCopyFrom AND COMBO = '$comboNameCopyFrom' AND PRINT = '$PrintNameCopyFrom'",null,null);
    while($row=mysqli_fetch_array($copied_process_arr)){
        $result_arr		= $costing_sample_routing_process->insertRec($sampleNo,$sampleYear,$revisionNo,$comboName,$PrintName,$row['ROUTING_ID'],$row['PROCESS_ID'],$row['ORDER'],$row['COST']);
    }

	//if($result) {
		$db->commit();
		$response['type'] = 'pass';
		$response['msg']  = 'Pre Costing Coppied Successfully.';
		$response['con']  = $consumption;
	/*}
	else{
		$db->rollback();
		$response['type'] 		= 'fail';
		//$response['msg'] 		= $db->errormsg;
		$response['q'] 			= $sql;
	}*/
	//echo json_encode($response);

}
else if($requestType=='copyCosting_old'){

  // print_r($_REQUEST);
	$sampleNo    		= $_REQUEST['sampleNo'];
	$sampleYear 		= $_REQUEST['sampleYear'];
	$revisionNo  		= $_REQUEST['revisionNo'];
	$comboName   		= $_REQUEST['combo'];
	$PrintName   		= $_REQUEST['print'];
	$coppiedFlag   		= $_REQUEST['coppiedFlag'];
	$sampleNoCopyFrom	= $_REQUEST['sampleNoC_O'];
	$sampleYearCopyFrom	= $_REQUEST['sampleYearC_O'];
	$revisionNoCopyFrom	= $_REQUEST['revisionNoC_O'];
	$comboNameCopyFrom	= $_REQUEST['comboC_O'];
	$PrintNameCopyFrom	= $_REQUEST['printC_O'];
	$estimatedQty    	= $_REQUEST['estimatedQty'];
	if($estimatedQty=='')
		$estimatedQty	=0;
	$prodLocation   	= $_REQUEST['prod_location'];
	if($prodLocation=='')
		$prodLocation	=0;
	$routing		   	= $_REQUEST['routing'];
	$ups   				= $_REQUEST['ups'];
	$noOfColours   		= $_REQUEST['colors'];
	$shots   			= $_REQUEST['shots'];
	$costPerShot   		= $_REQUEST['cost_per_shot'];
	if($costPerShot=='')
		$costPerShot	=0;
	$inkCost   			= $_REQUEST['inkCost'];
	$spRMCost   		= $_REQUEST['spRMCost'];
	$processCost   		= $_REQUEST['process_cost'];
	$shotCost   		= $_REQUEST['shot_cost'];
	$otherCost   		= $_REQUEST['other_cost'];
	$otherCostRemarks   = $_REQUEST['other_cost_remarks'];
	$totalCost   		= $_REQUEST['total_cost'];
	$marginPercen   	= $_REQUEST['margin_percentage'];
	$margin   			= $_REQUEST['margin'];
	$scPrice   			= $_REQUEST['screenline_price'];
	$targetPrice   		= $_REQUEST['target_price'];
	$approvedPrice   	= $_REQUEST['approved_price'];
	
	
	$arrInkType	 		= json_decode($_REQUEST['arrInkType'], true);
	$arrRmRoll	 		= json_decode($_REQUEST['arrRMroll'], true);
	$arrRMnonRoll 		= json_decode($_REQUEST['arrRMnonRoll'], true);
	$arrRMnonDirect		= json_decode($_REQUEST['arrRMnonDirect'], true);
	$arrProcess	 		= json_decode($_REQUEST['arrProcess'], true);

	$db->begin();
	
		
	if(!$costing_sample_header->set($sampleNo,$sampleYear,$revisionNo,$comboName,$PrintName)){
		$editMode=0;
		//$journal_array 	= $obj_common->GetSystemMaxNo('PRE_COSTING',$locationId);
		//$serialNo		= $journal_array["max_no"];
		//$serialYear		= date('Y');
		$sys_approvelevels->set($programCode);
		$approveLevels	= $sys_approvelevels->getintApprovalLevel();
		$status			=$approveLevels+1;
		
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Edit',$status,$approveLevels);
		
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);
	}
	else{
		$editMode=1;
		$costing_sample_header->set($sampleNo,$sampleYear,$revisionNo,$comboName,$PrintName);
	
		$status			=$costing_sample_header->getSTATUS();
		$sys_approvelevels->set($programCode);
		$approveLevels	= $sys_approvelevels->getintApprovalLevel();
		$status			=$approveLevels+1;
		
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Edit',$status,$approveLevels);
		
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);
	}


	
	if($editMode==0){
		$result_arr	=$costing_sample_header->insertRec($sampleNo,$sampleYear,$revisionNo,$comboName,$PrintName,$prodLocation,$ups,$estimatedQty,$noOfColours,$shots,$costPerShot,$inkCost,$spRMCost,$processCost,$shotCost,$otherCost,$otherCostRemarks,$totalCost,$marginPercen,$margin,$scPrice,$targetPrice,$approvedPrice,1,2,$sampleNoCopyFrom,$sampleYearCopyFrom,$revisionNoCopyFrom,$comboNameCopyFrom,$PrintNameCopyFrom,$status,$approveLevels,$userId,$dateTime->getCurruntDateTime(),0);
		//echo $db->getsql();
	}
	
	else{
		$costing_sample_header->set($sampleNo,$sampleYear,$revisionNo,$comboName,$PrintName);
		$costing_sample_header->setPRODUCTION_LOCATION($prodLocation);
		$costing_sample_header->setUPS($sessions->getCompanyId());
		$costing_sample_header->setESTIMATED_ORDER_QTY($estimatedQty);
		$costing_sample_header->setNO_OF_COLOURS($noOfColours);
		$costing_sample_header->setSHOTS($shots);
		$costing_sample_header->setCOST_PER_SHOT($costPerShot);
		$costing_sample_header->setINK_COST($inkCost);
		$costing_sample_header->setSPECIAL_RM_COST($spRMCost);
		$costing_sample_header->setPROCESS_COST($processCost);
		$costing_sample_header->setSHOT_COST($shotCost);
		$costing_sample_header->setOTHER_COST($otherCost);
		$costing_sample_header->setOTHER_COST_REMARKS($otherCostRemarks);
		$costing_sample_header->setTOTAL_COST($totalCost);
		$costing_sample_header->setMARGIN_PERCENTAGE($marginPercen);
		$costing_sample_header->setMARGIN($margin);
		$costing_sample_header->setSCREENLINE_PRICE($scPrice);
		$costing_sample_header->setTARGET_PRICE($targetPrice);
		$costing_sample_header->setAPPROVED_PRICE($approvedPrice);
		$costing_sample_header->setCOPPIED_FLAG(1);
		$costing_sample_header->setCOPPIED_TYPE(2);
		$costing_sample_header->setSTATUS($status);
		$costing_sample_header->setAPPROVE_LEVELS($approveLevels);
		
		if($sampleNoCopyFrom > 0){
			$costing_sample_header->setCOPPIED_FROM_SAMPLE_NO($sampleNoCopyFrom);
			$costing_sample_header->setCOPPIED_FROM_SAMPLE_YEAR($sampleYearCopyFrom);
			$costing_sample_header->setCOPPIED_FROM_REVISION($revisionNoCopyFrom);
			$costing_sample_header->setCOPPIED_FROM_COMBO($comboNameCopyFrom);
			$costing_sample_header->setCOPPIED_FROM_PRINT($PrintNameCopyFrom);
		}
		
		$result_arr	= $costing_sample_header->commit();
		//echo $db->getsql();
	}
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	
	//delete ink Type///////////////////////////////////////////
	$result_arr = $costing_sample_ink_type->delete(" SAMPLE_NO = $sampleNo AND SAMPLE_YEAR = $sampleYear AND REVISION = $revisionNo AND COMBO = '$comboName' AND PRINT = '$PrintName'");
	//echo $db->getsql();
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);

	//delete RM-Roll form////////////////////////////////////////
	$result_arr = $costing_sample_rm_roll_form->delete(" SAMPLE_NO = $sampleNo AND SAMPLE_YEAR = $sampleYear AND REVISION = $revisionNo AND COMBO = '$comboName' AND PRINT = '$PrintName'");
	//echo $db->getsql();
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);

	//delete RM-non_roll form
	$result_arr = $costing_sample_rm_non_roll_form->delete(" SAMPLE_NO = $sampleNo AND SAMPLE_YEAR = $sampleYear AND REVISION = $revisionNo AND COMBO = '$comboName' AND PRINT = '$PrintName'");
	//echo $db->getsql();
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);

	//delete RM-non_direct
	$result_arr = $costing_sample_rm_non_direct->delete(" SAMPLE_NO = $sampleNo AND SAMPLE_YEAR = $sampleYear AND REVISION_NO = $revisionNo AND COMBO = '$comboName' AND PRINT = '$PrintName'");
	//echo $db->getsql();
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);

	//delete process
	$result_arr = $costing_sample_routing_process->delete(" SAMPLE_NO = $sampleNo AND SAMPLE_YEAR = $sampleYear AND REVISION = $revisionNo AND COMBO = '$comboName' AND PRINT = '$PrintName'");
	//echo $db->getsql();
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);

    while($row=mysqli_fetch_array($copied_process_arr)){
        $result_arr		= $costing_sample_routing_process->insertRec($sampleNo,$sampleYear,$revisionNo,$comboName,$PrintName,$row['ROUTING_ID'],$row['PROCESS_ID'],$row['ORDER'],$row['COST']);
    }

	//if($result) {
		$db->commit();
		$response['type'] = 'pass';
		$response['msg']  = 'Pre Costing Coppied Successfully.';
		$response['con']  = $consumption;
	/*}
	else{
		$db->rollback();
		$response['type'] 		= 'fail';
		//$response['msg'] 		= $db->errormsg;
		$response['q'] 			= $sql;
	}*/
	//echo json_encode($response);

}
else if($requestType == 'approve'){

			$db->connect();$db->begin();
			//requested parameters
			
			$sampleNo		= $_REQUEST['sampleNo'];
			$sampleYear		= $_REQUEST['sampleyear'];
			$revision		= $_REQUEST['revNo'];
			$combo			= $_REQUEST['combo'];
			$print			= $_REQUEST['print'];
			//1) select header status
			$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print);
			$header_status = $costing_sample_header->getSTATUS();
			$approve_level = $costing_sample_header->getAPPROVE_LEVELS();

						
		##################################
		## 1.check approve permission	##
		##################################
			$menupermision->set($programCode,$userId);
			$approvePermission	= $menupermision->checkMenuPermision('Approve',$header_status,$approve_level);
	
			if(!$approvePermission['type'])
				throw new Exception($approvePermission['msg']);
						
		##################################
		## 3.update header status		##
		##################################
			$where = "SAMPLE_NO = '$sampleNo' AND SAMPLE_YEAR= '$sampleYear' AND REVISION= '$revision' AND COMBO= '$combo' AND PRINT = '$print'";
			
			$data  =array('STATUS'=> -1);
			$result_update_h		= $costing_sample_header->upgrade($data,$where);
			//echo $db->getsql();
			if(!$result_update_h['status']) 
				throw new Exception($result_update_h['msg']);
				
		##########################################
		## 4 insert trn_sample_informatino_price##
		##########################################
		$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print);
			$header_status 	= $costing_sample_header->getSTATUS();
			$price 			= $costing_sample_header->getAPPROVED_PRICE();
			//$level			= $aplevel+1- $hstatus;
		if($header_status == '1'){
			$data 			= array('intSampleNo'		=>$sampleNo,
									'intSampleYear'		=>$sampleYear,
									'intRevisionNo'		=>$revision,
									'strCombo'			=>$combo,
									'strPrintName'		=>$print,
									'intCurrency' 		=>'1',
									'dblPrice' 			=>$price,
									'dblRMCost'			=>'0', 
									'dtEnterDate'		=>$dateTime->getCurruntDateTime(),
									'intEnterBy'		=>$userId);
			$deleteSampleInformationPrice	= $trn_sampleinfomations_prices->delete("intSampleNo = $sampleNo AND intSampleYear = $sampleYear AND intRevisionNo = $revision AND strCombo = '$combo' AND strPrintName = '$print'");
			if(!$deleteSampleInformationPrice['status']) 
				throw new Exception($deleteSampleInformationPrice['msg']);
															
			$inserTosampleInformationPrice	= $trn_sampleinfomations_prices->insert($data);
			if(!$inserTosampleInformationPrice['status']) 
				throw new Exception($inserTosampleInformationPrice['msg']);
			}
				
		##################################
		## 5.insert to approve by table	##
		##################################
			$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print);
			$header_status 	= $costing_sample_header->getSTATUS();
			$approve_level 	= $costing_sample_header->getAPPROVE_LEVELS();
			$level			= $approve_level+1- $header_status;
			/*$sql_p = "SELECT
					Max(costing_sample_header_approved_by.`STATUS`) as status
					FROM
					costing_sample_header_approved_by
					WHERE
					costing_sample_header_approved_by.SAMPLE_NO = '$sampleNo' AND
					costing_sample_header_approved_by.SAMPLE_YEAR = '$sampleYear' AND
					costing_sample_header_approved_by.REVISION_NO = '$revision' AND
					costing_sample_header_approved_by.COMBO = '$combo' AND
					costing_sample_header_approved_by.PRINT = '$print'
					";	
			$result_p = $db->RunQuery($sql_p);
			$row_p = mysqli_fetch_array($result_p);
			$status = $row_p['status'];*/
			
			$data 			= array('SAMPLE_NO'			=>$sampleNo,
									'SAMPLE_YEAR'		=>$sampleYear,
									'REVISION_NO'		=>$revision,
									'COMBO'				=>$combo,
									'PRINT'				=>$print,
									'APPROVED_LEVEL_NO'	=>$level,
									'APPROVED_BY'		=>$userId,
									'APPROVED_DATE'		=>$dateTime->getCurruntDateTime(),
									'STATUS'			=>0);	
				
			
					
			$inserToApprovedBy	= $costing_sample_header_approved_by->insert($data);	
			if(!$inserToApprovedBy['status']) 
				throw new Exception($inserToApprovedBy['msg']);
		
		//get sample entered user email
	
		$sql = "SELECT
					sys_users.strFullName,
					trn_sampleinfomations.strGraphicRefNo,
					sys_users.strEmail,
					marketer.strEmail as email_marketer,
					marketer.strFullName as marketerName 

				FROM
					trn_sampleinfomations
					Inner Join sys_users ON sys_users.intUserId = trn_sampleinfomations.intCreator 
					INNER JOIN sys_users AS marketer ON trn_sampleinfomations.intMarketer = marketer.intUserId
				WHERE
					trn_sampleinfomations.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations.intRevisionNo =  '$revision' AND 
					sys_users.intStatus = 1
				";
		 $result = $db->RunQuery($sql);
		 $row = mysqli_fetch_array($result);
		 $enterUserName 	= 	$row['strFullName'];
		 $enterUserEmail 	=	$row['strEmail'];
		 $email_marketer 	=	$row['email_marketer'];
		 $marketerName		=	$row['marketerName'];
		 $graphicNo 		= 	$row['strGraphicRefNo'];
		 
		//send mails ////
		$_REQUEST = NULL;
		$_REQUEST['no'] 		= $sampleNo;
		$_REQUEST['year'] 		= $sampleYear;
		$_REQUEST['revNo'] 		= $revision;
		
		if($enterUserEmail != '')
		//$objMail->send_Response_Mail('mail_sample_price.php',$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],"SAMPLE PRICES UPDATED - $graphicNo ($sampleNo/$sampleYear)",$enterUserEmail,$enterUserName);
		
	
		$body			= '';
 		//---------
 		$nowDate 		= date('Y-m-d');
  		$mailHeader		= "PO required items Notification (".$orderNo."/".$orderYear.")";
		$FROM_NAME		= "NSOFT";
		$FROM_EMAIL		= 'nsoft@screenlineholdings.com';
		
 		
 		//sendMessage($FROM_EMAIL,$FROM_NAME,$toEmails,$mailHeader,$body,$cc,$bcc);
		//sendMessage($FROM_EMAIL,$FROM_NAME,'roshan.nsoft@gmail.com',$mailHeader,$body,'','');
		$falg_raised_all_prices	=get_falg_raised_all_prices($sampleNo,$sampleYear,$revision);
		if($falg_raised_all_prices==1){
			
			$mailFrom		=$_SESSION["systemUserName"];
			
 			ob_start();
			$to_name	    = $enterUserName;
			include 'mail_sample_price.php';
			$body 			= ob_get_clean();
			sendMessage($_SESSION["email"],$_SESSION["systemUserName"],$enterUserEmail,"SAMPLE PRICES UPDATED - $graphicNo ($sampleNo/$sampleYear)",$body,'','');
			ob_end_clean();
			
 			ob_start();
			$to_name	    = $marketerName;
			include 'mail_sample_price.php';
			$body2 			= ob_get_clean();
			sendMessage($_SESSION["email"],$_SESSION["systemUserName"],$email_marketer,"SAMPLE PRICES UPDATED - $graphicNo ($sampleNo/$sampleYear)",$body2,'','');
		}
	
	
	
	
		
			$db->commit();
			$db->disconnect();
			$response['type'] 			= "pass";
			$response['msg'] 			= "Approved Successfully.";
				
			//echo json_encode($response);
		
	}
else if($requestType == 'reject')
		{
			
			$db->connect();$db->begin();
			
			//requested parameters
			$sampleNo		= $_REQUEST['sampleNo'];
			$sampleYear		= $_REQUEST['sampleyear'];
			$revision		= $_REQUEST['revNo'];
			$combo			= $_REQUEST['combo'];
			$print			= $_REQUEST['print'];
			
			//1) select header status
			$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print);
			$header_status = $costing_sample_header->getSTATUS();
			$approve_level = $costing_sample_header->getAPPROVE_LEVELS();
		##########################
		## 1.check permission	##
		##########################
			$menupermision->set($programCode,$userId);
			$rejectPermission	= $menupermision->checkMenuPermision('Reject',$header_status,$approve_level);
			if(!$rejectPermission['type'])
			{
				$response['type'] 	= 'fail';
				$response['msg']	= $rejectPermission['msg'];
			}
				
		##############################
		## 3.Update Header status	##
		##############################
			$where = "SAMPLE_NO = '$sampleNo' AND SAMPLE_YEAR= '$sampleYear' AND REVISION= '$revision' AND COMBO= '$combo' AND PRINT = '$print'";
		
			$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print);
			$costing_sample_header->setSTATUS(0);
			$result_update_h	= $costing_sample_header->commit();
		/*	$data  =array(	'STATUS'=> 0);
			
			$result_update_h		= $costing_sample_header->upgrade($data,$where);*/
			if(!$result_update_h['status']) 
				throw new Exception($result_update_h['msg']);
				
		##################################
		## 5.insert to approve by table	##
		##################################
			$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print);
			$header_status 	= $costing_sample_header->getSTATUS();
			$approve_level 	= $costing_sample_header->getAPPROVE_LEVELS();
			$level			= - $header_status;
			$sql_p = "SELECT
					Max(costing_sample_header_approved_by.`STATUS`) as status
					FROM
					costing_sample_header_approved_by
					WHERE
					costing_sample_header_approved_by.SAMPLE_NO = '$sampleNo' AND
					costing_sample_header_approved_by.SAMPLE_YEAR = '$sampleYear' AND
					costing_sample_header_approved_by.REVISION_NO = '$revision' AND
					costing_sample_header_approved_by.COMBO = '$combo' AND
					costing_sample_header_approved_by.PRINT = '$print'
					";	 
			 $result = $db->RunQuery($sql_p);
			 $row = mysqli_fetch_array($result);
			 $max_st	=$row['status']+1;
			$sql_u = "UPDATE 
					costing_sample_header_approved_by 
					set STATUS='$max_st'
					WHERE
					costing_sample_header_approved_by.SAMPLE_NO = '$sampleNo' AND
					costing_sample_header_approved_by.SAMPLE_YEAR = '$sampleYear' AND
					costing_sample_header_approved_by.REVISION_NO = '$revision' AND
					costing_sample_header_approved_by.COMBO = '$combo' AND
					costing_sample_header_approved_by.PRINT = '$print'
					and STATUS=0 ";	 
			 $result = $db->RunQuery($sql_u);
			
			
			$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print);
			$status = $costing_sample_header->getSTATUS();
			
			$data 			= array('SAMPLE_NO'			=>$sampleNo,
									'SAMPLE_YEAR'		=>$sampleYear,
									'REVISION_NO'		=>$revision,
									'COMBO'				=>$combo,
									'PRINT'				=>$print,
									'APPROVED_LEVEL_NO'	=>0,
									'APPROVED_BY'		=>$userId,
									'APPROVED_DATE'		=>$dateTime->getCurruntDateTime(),
									'STATUS'			=>0);	
				
			
					
			$inserToApprovedBy	= $costing_sample_header_approved_by->insert($data);	
			if(!$inserToApprovedBy['status']) 
				throw new Exception($inserToApprovedBy['msg']);
				
			$db->commit();
			$db->disconnect();
			$response['type'] 			= "pass";
			$response['msg'] 			= "Rejected Successfully.";
						
		}
else if($requestType == 'urlReject_approved')
		{
			
			$db->connect();$db->begin();
			
			//requested parameters
			$sampleNo		= $_REQUEST['sampleNo'];
			$sampleYear		= $_REQUEST['sampleyear'];
			$revision		= $_REQUEST['revNo'];
			$combo			= $_REQUEST['combo'];
			$print			= $_REQUEST['print'];
			
			//1) select header status
			$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print);
			$header_status = $costing_sample_header->getSTATUS();
			$approve_level = $costing_sample_header->getAPPROVE_LEVELS();
		##########################
		## 1.check permission	##
		##########################
			$menupermision->set($programCode,$userId);
			$rejectPermission	= $menupermision->checkMenuPermision('Reject',2,$approve_level);
			if(!$rejectPermission['type'])
			{
				$response['type'] 	= 'fail';
				$response['msg']	= $rejectPermission['msg'];
			}
				
		##############################
		## 3.Update Header status	##
		##############################
			$where = "SAMPLE_NO = '$sampleNo' AND SAMPLE_YEAR= '$sampleYear' AND REVISION= '$revision' AND COMBO= '$combo' AND PRINT = '$print'";
		
			$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print);
			$costing_sample_header->setSTATUS(0);
			$result_update_h	= $costing_sample_header->commit();
		/*	$data  =array(	'STATUS'=> 0);
			
			$result_update_h		= $costing_sample_header->upgrade($data,$where);*/
			if(!$result_update_h['status']) 
				throw new Exception($result_update_h['msg']);
				
		##################################
		## 5.insert to approve by table	##
		##################################
			$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print);
			$header_status 	= $costing_sample_header->getSTATUS();
			$approve_level 	= $costing_sample_header->getAPPROVE_LEVELS();
			$level			= - $header_status;
			$sql_p = "SELECT
					Max(costing_sample_header_approved_by.`STATUS`) as status
					FROM
					costing_sample_header_approved_by
					WHERE
					costing_sample_header_approved_by.SAMPLE_NO = '$sampleNo' AND
					costing_sample_header_approved_by.SAMPLE_YEAR = '$sampleYear' AND
					costing_sample_header_approved_by.REVISION_NO = '$revision' AND
					costing_sample_header_approved_by.COMBO = '$combo' AND
					costing_sample_header_approved_by.PRINT = '$print'
					";	 
			 $result = $db->RunQuery($sql_p);
			 $row = mysqli_fetch_array($result);
			 $max_st	=$row['status']+1;
			 
			$sql_u = "UPDATE 
					costing_sample_header_approved_by 
					set STATUS='$max_st'
					WHERE
					costing_sample_header_approved_by.SAMPLE_NO = '$sampleNo' AND
					costing_sample_header_approved_by.SAMPLE_YEAR = '$sampleYear' AND
					costing_sample_header_approved_by.REVISION_NO = '$revision' AND
					costing_sample_header_approved_by.COMBO = '$combo' AND
					costing_sample_header_approved_by.PRINT = '$print' 
					and STATUS=0
					
					";
						 
			 $result = $db->RunQuery($sql_u);
		
			$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print);
			$status = $costing_sample_header->getSTATUS();
			
			$data 			= array('SAMPLE_NO'			=>$sampleNo,
									'SAMPLE_YEAR'		=>$sampleYear,
									'REVISION_NO'		=>$revision,
									'COMBO'				=>$combo,
									'PRINT'				=>$print,
									'APPROVED_LEVEL_NO'	=>0,
									'APPROVED_BY'		=>$userId,
									'APPROVED_DATE'		=>$dateTime->getCurruntDateTime(),
									'STATUS'			=>0);	
				
			
					
			$inserToApprovedBy	= $costing_sample_header_approved_by->insert($data);	
			if(!$inserToApprovedBy['status']) 
				throw new Exception($inserToApprovedBy['msg']);
				
			$db->commit();
			$db->disconnect();
			$response['type'] 			= "pass";
			$response['msg'] 			= "Rejected Successfully.";
						
		}////////////////////////////////////////////////////////////////////////
else if($requestType == 'urlReject_approved_cp')
		{
			
			$db->connect();$db->begin();
			
			//requested parameters
			$sampleNo_p			= $_REQUEST['sampleNo'];
			$sampleYear_p		= $_REQUEST['sampleyear'];
			$revision_p			= $_REQUEST['revNo'];
			$combo_p			= $_REQUEST['combo'];
			$print_p			= $_REQUEST['print'];
			
	
			$sql_s = "SELECT
					SAMPLE_NO,
					SAMPLE_YEAR,
					REVISION,
					COMBO,
					PRINT 
					FROM
					costing_sample_header
					WHERE
					costing_sample_header.COPPIED_FROM_SAMPLE_NO = '$sampleNo_p' AND
					costing_sample_header.COPPIED_FROM_SAMPLE_YEAR = '$sampleYear_p' AND
					costing_sample_header.COPPIED_FROM_REVISION = '$revision_p' AND
					costing_sample_header.COPPIED_FROM_COMBO = '$combo_p' AND
					costing_sample_header.COPPIED_FROM_PRINT = '$print_p'
					";	 
			 $result_s = $db->RunQuery($sql_s);
			 while($row_s=mysqli_fetch_array($result_s)){
		
				$sampleNo		= $row_s['SAMPLE_NO'];
				$sampleYear		= $row_s['SAMPLE_YEAR'];
				$revision		= $row_s['REVISION'];
				$combo			= $row_s['COMBO'];
				$print			= $row_s['PRINT'];
			
			//1) select header status
			$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print);
			$header_status = $costing_sample_header->getSTATUS();
			$approve_level = $costing_sample_header->getAPPROVE_LEVELS();
		##########################
		## 1.check permission	##
		##########################
			$menupermision->set($programCode,$userId);
			$rejectPermission	= $menupermision->checkMenuPermision('Reject',2,$approve_level);
			if(!$rejectPermission['type'])
			{
				$response['type'] 	= 'fail';
				$response['msg']	= $rejectPermission['msg'];
			}
				
		##############################
		## 3.Update Header status	##
		##############################
			$where = "SAMPLE_NO = '$sampleNo' AND SAMPLE_YEAR= '$sampleYear' AND REVISION= '$revision' AND COMBO= '$combo' AND PRINT = '$print'";
		
			$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print);
			$costing_sample_header->setSTATUS(0);
			$result_update_h	= $costing_sample_header->commit();
		/*	$data  =array(	'STATUS'=> 0);
			
			$result_update_h		= $costing_sample_header->upgrade($data,$where);*/
			if(!$result_update_h['status']) 
				throw new Exception($result_update_h['msg']);
				
		##################################
		## 5.insert to approve by table	##
		##################################
			$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print);
			$header_status 	= $costing_sample_header->getSTATUS();
			$approve_level 	= $costing_sample_header->getAPPROVE_LEVELS();
			$level			= - $header_status;
			$sql_p = "SELECT
					Max(costing_sample_header_approved_by.`STATUS`) as status
					FROM
					costing_sample_header_approved_by
					WHERE
					costing_sample_header_approved_by.SAMPLE_NO = '$sampleNo' AND
					costing_sample_header_approved_by.SAMPLE_YEAR = '$sampleYear' AND
					costing_sample_header_approved_by.REVISION_NO = '$revision' AND
					costing_sample_header_approved_by.COMBO = '$combo' AND
					costing_sample_header_approved_by.PRINT = '$print'
					";	 
			 $result = $db->RunQuery($sql_p);
			 $row = mysqli_fetch_array($result);
			 $max_st	=$row['status']+1;
			 
			$sql_u = "UPDATE 
					costing_sample_header_approved_by 
					set STATUS='$max_st'
					WHERE
					costing_sample_header_approved_by.SAMPLE_NO = '$sampleNo' AND
					costing_sample_header_approved_by.SAMPLE_YEAR = '$sampleYear' AND
					costing_sample_header_approved_by.REVISION_NO = '$revision' AND
					costing_sample_header_approved_by.COMBO = '$combo' AND
					costing_sample_header_approved_by.PRINT = '$print' 
					and STATUS=0
					";	 
			 $result = $db->RunQuery($sql_u);
		
			$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print);
			$status = $costing_sample_header->getSTATUS();
			
			$data 			= array('SAMPLE_NO'			=>$sampleNo,
									'SAMPLE_YEAR'		=>$sampleYear,
									'REVISION_NO'		=>$revision,
									'COMBO'				=>$combo,
									'PRINT'				=>$print,
									'APPROVED_LEVEL_NO'	=>0,
									'APPROVED_BY'		=>$userId,
									'APPROVED_DATE'		=>$dateTime->getCurruntDateTime(),
									'STATUS'			=>0);	
				
			
					
			$inserToApprovedBy	= $costing_sample_header_approved_by->insert($data);	
			if(!$inserToApprovedBy['status']) 
				throw new Exception($inserToApprovedBy['msg']);
				
			}//end of while
				
			$db->commit();
			$db->disconnect();
			$response['type'] 			= "pass";
			$response['msg'] 			= "Rejected Successfully.";
						
		}////////////////////////////////////////////////////////////////////////
else if($requestType == 'approve_coppied_same_revision'){

			$db->connect();$db->begin();
			//requested parameters
			$arr 		 = json_decode($_REQUEST['arr'], true);
			foreach($arr as $arrVal)
			{
					$sampleNo		= $arrVal['sampleNo'];
					$sampleYear		= $arrVal['sample_year'];
					$revision		= $arrVal['revisionNo'];
					$combo			= $arrVal['combo'];
					$print			= $arrVal['print'];
					
 					/* $sql1 = "SELECT
							costing_sample_header.SAMPLE_NO
							FROM
							costing_sample_header
							WHERE
							costing_sample_header.SAMPLE_NO = '$sampleNo' AND
							costing_sample_header.SAMPLE_YEAR = '$sampleYear' AND
							costing_sample_header.REVISION = '$revision' AND
							costing_sample_header.COMBO = '$combo' AND
							costing_sample_header.PRINT = '$print'
							";*/
					//$result = $db->RunQuery($sql1);
					//echo "/".$recs	= mysqli_num_rows($result);
					if(!$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print)){
						$response['type'] 			= "fail";
						$response['msg'] 			= "please save the (".$print." / ".$combo.") before approve";
						throw new Exception("please save the (".$print." / ".$combo.") before approve:".$sql1);
					}else{
					//1) select header status
					$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print);
					$header_status = $costing_sample_header->getSTATUS();
					$approve_level = $costing_sample_header->getAPPROVE_LEVELS();
		
					
				##################################
				## 1.check approve permission	##
				##################################
					$menupermision->set($programCode,$userId);
					$approvePermission	= $menupermision->checkMenuPermision('Approve',$header_status,$approve_level);
			
					if(!$approvePermission['type'])
						throw new Exception($approvePermission['msg']);
								
				##################################
				## 3.update header status		##
				##################################
					$where = "SAMPLE_NO = '$sampleNo' AND SAMPLE_YEAR= '$sampleYear' AND REVISION= '$revision' AND COMBO= '$combo' AND PRINT = '$print'";
					
					$data  =array('STATUS'=> -1);
					$result_update_h		= $costing_sample_header->upgrade($data,$where);
					//echo $db->getsql();
					if(!$result_update_h['status']) 
						throw new Exception($result_update_h['msg']);
						
				##########################################
				## 4 insert trn_sample_informatino_price##
				##########################################
				$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print);
					$header_status 	= $costing_sample_header->getSTATUS();
					$price 			= $costing_sample_header->getAPPROVED_PRICE();
					//$level			= $aplevel+1- $hstatus;
					
				if($header_status == '1'){
					$data 			= array('intSampleNo'		=>$sampleNo,
											'intSampleYear'		=>$sampleYear,
											'intRevisionNo'		=>$revision,
											'strCombo'			=>$combo,
											'strPrintName'		=>$print,
											'intCurrency' 		=>'1',
											'dblPrice' 			=>$price,
											'dblRMCost'			=>'0', 
											'dtEnterDate'		=>$dateTime->getCurruntDateTime(),
											'intEnterBy'		=>$userId);					
				
					$deleteSampleInformationPrice	= $trn_sampleinfomations_prices->delete("intSampleNo = $sampleNo AND intSampleYear = $sampleYear AND intRevisionNo = $revision AND strCombo = '$combo' AND strPrintName = '$print'");
					if(!$deleteSampleInformationPrice['status']) 
						throw new Exception($deleteSampleInformationPrice['msg']);
				
					$inserTosampleInformationPrice	= $trn_sampleinfomations_prices->insert($data);
					
					if(!$inserTosampleInformationPrice['status']) 
						throw new Exception($inserTosampleInformationPrice['msg']);
					}
						
				##################################
				## 5.insert to approve by table	##
				##################################

					$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print);
					$header_status 	= $costing_sample_header->getSTATUS();
					$approve_level 	= $costing_sample_header->getAPPROVE_LEVELS();
					$level			= $approve_level+1- $header_status;
					$data 			= array('SAMPLE_NO'			=>$sampleNo,
											'SAMPLE_YEAR'		=>$sampleYear,
											'REVISION_NO'		=>$revision,
											'COMBO'				=>$combo,
											'PRINT'				=>$print,
											'APPROVED_LEVEL_NO'	=>$level,
											'APPROVED_BY'		=>$userId,
											'APPROVED_DATE'		=>$dateTime->getCurruntDateTime(),
											'STATUS'			=>'0');					
					$inserToApprovedBy	= $costing_sample_header_approved_by->insert($data);	
					if(!$inserToApprovedBy['status']) 
						throw new Exception($inserToApprovedBy['msg']);
		
		
			}
		
		}
		
			$sql = "SELECT
						sys_users.strFullName,trn_sampleinfomations.strGraphicRefNo,
						sys_users.strEmail
					FROM
						trn_sampleinfomations
						Inner Join sys_users ON sys_users.intUserId = trn_sampleinfomations.intCreator
					WHERE
						trn_sampleinfomations.intSampleNo =  '$sampleNo' AND
						trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
						trn_sampleinfomations.intRevisionNo =  '$revision' AND 
						sys_users.intStatus = 1
					";
			 $result = $db->RunQuery($sql);
			 $row = mysqli_fetch_array($result);
			 $enterUserName 	= 	$row['strFullName'];
			 $enterUserEmail 	=	$row['strEmail'];
			 $graphicNo 		= 	$row['strGraphicRefNo'];
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST['no'] 		= $sampleNo;
			$_REQUEST['year'] 		= $sampleYear;
			$_REQUEST['revNo'] 		= $revision;
			
			if($enterUserEmail != '')
			//$objMail->send_Response_Mail('mail_sample_price.php',$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],"SAMPLE PRICES UPDATED - $graphicNo ($sampleNo/$sampleYear)",$enterUserEmail,$enterUserName);
			
		
			$body			= '';
			ob_start();
			include_once 'mail_sample_price.php';
			   $body 			= ob_get_clean();
			//---------
			$nowDate 		= date('Y-m-d');
			//$mailHeader		= "PO required items Notification (".$orderNo."/".$orderYear.")";
			$FROM_NAME		= "NSOFT";
			$FROM_EMAIL		= 'nsoft@screenlineholdings.com';
			
			
			//sendMessage($FROM_EMAIL,$FROM_NAME,$toEmails,$mailHeader,$body,$cc,$bcc);
			//sendMessage($FROM_EMAIL,$FROM_NAME,'roshan.nsoft@gmail.com',$mailHeader,$body,'','');
			$falg_raised_all_prices	=get_falg_raised_all_prices($sampleNo,$sampleYear,$revision);
			if($falg_raised_all_prices==1)
			sendMessage($_SESSION["email"],$_SESSION["systemUserName"],$enterUserEmail,"SAMPLE PRICES UPDATED - $graphicNo ($sampleNo/$sampleYear)",$body,'','');

		
			$db->commit();
			$db->disconnect();
			$response['type'] 			= "pass";
			$response['msg'] 			= "Approvd Successfully.";
		
			//echo json_encode($response);
		
	}
else if($requestType == 'reject_coppied_same_revision')
	{
			
			$db->connect();$db->begin();
			$arr 		 = json_decode($_REQUEST['arr'], true);
			foreach($arr as $arrVal)
			{
			
				$sampleNo		= $arrVal['sampleNo'];
				$sampleYear		= $arrVal['sample_year'];
				$revision		= $arrVal['revisionNo'];
				$combo			= $arrVal['combo'];
				$print			= $arrVal['print'];
				/*$sql1 = "SELECT
							costing_sample_header.SAMPLE_NO
							FROM
							costing_sample_header
							WHERE
							costing_sample_header.SAMPLE_NO = '$sampleNo' AND
							costing_sample_header.SAMPLE_YEAR = '$sampleYear' AND
							costing_sample_header.REVISION = '$revision' AND
							costing_sample_header.COMBO = '$combo' AND
							costing_sample_header.PRINT = '$print'
							";
					$result = $db->RunQuery($sql1);*/
					if(!$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print)){
						$response['type'] 			= "fail";
						$response['msg'] 			= "please save the (".$print." / ".$combo.") before reject";
						throw new Exception("please save the (".$print." / ".$combo.") before reject");
					}
					else if($costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print) && $costing_sample_header->getSTATUS() =='0'){
						//if($costing_sample_header->getSTATUS() =='0'){
						$response['type'] 			= "fail";
						$response['msg'] 			= "Already rejected (".$print." / ".$combo.")".$costing_sample_header->getSTATUS;
						throw new Exception("Already rejected (".$print." / ".$combo.")");
						//}
						
					}
					else{
						
				//1) select header status
				$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print);
				$header_status = $costing_sample_header->getSTATUS();
				$approve_level = $costing_sample_header->getAPPROVE_LEVELS();
			##########################
			## 1.check permission	##
			##########################
				$menupermision->set($programCode,$userId);
				$rejectPermission	= $menupermision->checkMenuPermision('Reject',$header_status,$approve_level);
				if(!$rejectPermission['type'])
				{
					print_r($rejectPermission);
					$response['type'] 	= 'fail';
					$response['msg']	= $rejectPermission['msg'];
					throw new Exception($rejectPermission['msg']);
				}
					
			##############################
			## 3.Update Header status	##
			##############################
				//$where = "SAMPLE_NO = '$sampleNo' AND SAMPLE_YEAR= '$sampleYear' AND REVISION= '$revision' AND COMBO= '$combo' AND PRINT = '$print'";
			
				/*$data  =array(	'STATUS'=> '0'
				);
				$result_update_h		= $costing_sample_header->upgrade($data,$where);
				*/
				$costing_sample_header->set($sampleNo, $sampleYear , $revision ,$combo , $print);
				$costing_sample_header->setSTATUS(0);
				$result_update_h	= $costing_sample_header->commit();
				
				if(!$result_update_h['status']) 
					throw new Exception($result_update_h['msg']);
				}
                $result_arr = $costing_sample_routing_process->delete(" SAMPLE_NO = $sampleNo AND SAMPLE_YEAR = $sampleYear AND REVISION = $revisionNo AND COMBO = '$comboName' AND PRINT = '$PrintName'");

            }
			
			$db->commit();
			$db->disconnect();
			$response['type'] 			= "pass";
			$response['msg'] 			= "Reject Successfully.";
		}
else if ($requestType == 'saveQtyPrices'){

    $sampleNo    		= $_REQUEST['sampleNo'];
    $sampleYear 		= $_REQUEST['sampleYear'];
    $revisionNo  		= $_REQUEST['revisionNo'];
    $comboName   		= $_REQUEST['combo'];
    $PrintName   		= $_REQUEST['print'];

    $db->connect();$db->begin();
    $msg ="";
    $arr 			= json_decode($_REQUEST['arr'], true);
    $errorFlag=0;
    $currentMaximumRangeValue = 0;
//    var_dump("Array value is : ");
//    var_dump($arr);
    $sql = "INSERT INTO `costing_sample_qty_wise` (`SAMPLE_YEAR`,`SAMPLE_NO`,`REVISION`,`COMBO`,`PRINT`,`LOWER_MARGIN`,`UPPER_MARGIN`,`PRICE`) VALUES ";
    foreach($arr as $arrVal)
    {

        $upperMargin 	   = (int)$arrVal['upper_margin'];
        $lowerMargin 	   = (int)$arrVal['lower_margin'];
        $qtyPrice 	   = (float)$arrVal['qty_price'];
        $saved = 0;
        if ($qtyPrice < 0){
            $errorFlag = 1;
            $msg = "Prices Should Be Greater Than Zero";
            break;
        } else if ($lowerMargin < $currentMaximumRangeValue){
            $errorFlag = 1;
            $msg = "Lower Margin should greater than '$currentMaximumRangeValue'";
            break;
        } else if ($upperMargin < $lowerMargin && $upperMargin != -2){
            $errorFlag = 1;
            $msg = "Upper Margin should greater than lower margin";
            break;
        }
        $sql .= " ('$sampleYear','$sampleNo','$revisionNo','$comboName','$PrintName','$lowerMargin','$upperMargin','$qtyPrice'),";
    }

    if ($errorFlag == 0){
        $deleteSql = "DELETE FROM costing_sample_qty_wise WHERE SAMPLE_NO='$sampleNo' AND SAMPLE_YEAR ='$sampleYear' AND REVISION='$revisionNo' 
                      AND COMBO = '$comboName' AND PRINT = '$PrintName'";
        $deleted = $db->RunQuery($deleteSql);
        if ($deleted){
            $sql = rtrim($sql,',');
            $result = $db->RunQuery($sql);
            if ($result == 1){
                $saved = 1;
            }
        } else {
            $errorFlag = 1;
            $msg = "Could n't delete the previous entries";
        }
    }
    //$db->rollback();

    if ($errorFlag==1){
        $response['type'] 		= 'fail';
        $response['msg'] 		= $msg;
    }  else if($saved == 1){
        $response['type'] 		= 'pass';
        $response['msg'] 		= 'Prices Saved successfully.';
    }  else{
        $response['type'] 		= 'fail';
        $response['msg'] 		= $db->errormsg;
        $response['q'] 			= $sql;
    }
    $db->commit();
    $db->disconnect();
}


echo json_encode($response);
	
} catch(Exception $e)
{	
	switch ($e->getCode()){
		case 0:
			$db->rollback();		
			$response['msg'] 	=  $e->getMessage();
			//$response['error'] 	=  $error_handler->jTraceEx($e);
			$response['type'] 	=  'fail';
			//$response['sql']	=  $db->getSql();
			break;		
		case 1:
			$db->commit();
			$response['msg'] 	=  $e->getMessage();
			$response['pass'] 	=  'fail';
			break;
	}
	echo json_encode($response);
	//$db->disconnect();
        
            
        
}

function get_falg_raised_all_prices($sampleNo,$sampleYear,$revision){
	
	global $db;	
			$sql = "SELECT trn_sampleinfomations_details.intSampleNo
					FROM
					trn_sampleinfomations_details
					left JOIN costing_sample_header ON trn_sampleinfomations_details.intSampleNo = costing_sample_header.SAMPLE_NO AND trn_sampleinfomations_details.intSampleYear = costing_sample_header.SAMPLE_YEAR AND trn_sampleinfomations_details.intRevNo = costing_sample_header.REVISION AND trn_sampleinfomations_details.strComboName = costing_sample_header.COMBO AND trn_sampleinfomations_details.strPrintName = costing_sample_header.PRINT
					 AND
					costing_sample_header.`STATUS` = 1
					WHERE
					trn_sampleinfomations_details.intSampleNo = '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear = '$sampleYear' AND
					trn_sampleinfomations_details.intRevNo = '$revision' AND
					costing_sample_header.SAMPLE_NO IS NULL

					";
			 $result = $db->RunQuery($sql);
			 $row = mysqli_fetch_array($result);
			 if($row['intSampleNo']>0 && $row['intSampleNo'] !='')
			 	return 0;
			 else 
			 	return 1;
 	
}
function get_qty_wise_prices($sampleNo,$sampleYear,$revision, $combo, $print){

    global $db;
    $sql = "SELECT * FROM costing_sample_qty_wise WHERE
					costing_sample_qty_wise.SAMPLE_NO = '$sampleNo' AND
					costing_sample_qty_wise.SAMPLE_YEAR = '$sampleYear' AND
					costing_sample_qty_wise.REVISION = '$revision' AND
					costing_sample_qty_wise.COMBO = '$combo' AND 
                    costing_sample_qty_wise.PRINT = '$print'
					";
    $result = $db->RunQuery($sql);
    return $result;
}

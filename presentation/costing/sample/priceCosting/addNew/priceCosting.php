<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
///////////////////////////////
// H B G KORALA   ////////////
//////////////////////////////
session_start();
$backwardseperator 		= "../../../../../";
$mainPath 				= $_SESSION['mainPath'];
$sessionUserId 			= $_SESSION['userId'];
$thisFilePath 			= $_SERVER['PHP_SELF'];
$locationId				= $_SESSION["CompanyID"];
$company 				= $_SESSION['headCompanyId'];

###########################
### get loading values ####
###########################
$type		 				= $_REQUEST['type'];
 
$x_sampleNo_o 				= $_REQUEST['sampleNo'];
$x_sampleYear_o				= $_REQUEST['sampleYear'];
$x_revNo_o 					= $_REQUEST['revNo'];
$x_combo_o 					= $_REQUEST['combo'];
$x_print_o 					= $_REQUEST['printName'];

$x_sampleNo_c 				= $_REQUEST['sampleNoC'];
$x_sampleYear_c 			= $_REQUEST['sampleYearC'];
$x_revNo_c 					= $_REQUEST['revNoC'];
$x_combo_c 					= $_REQUEST['comboC'];
$x_print_c 					= $_REQUEST['printNameC'];

$x_sampleNo_c_o 			= $_REQUEST['sampleNoC_O'];
$x_sampleYear_c_o 			= $_REQUEST['sampleYearC_O'];
$x_revNo_c_o				= $_REQUEST['revNoC_O'];
$x_combo_c_o				= $_REQUEST['comboC_O'];
$x_print_c_o				= $_REQUEST['printNameC_O'];


$usage						= 100;//by costing
$prod_location				= '';//by costng
$routing					= '';//by costng
$date						= date('Y-m-d');
$ups						= 0;//by technical (combo print wise)
$margin						= 0; // by costing
$other_cost					= 0; // by costing
$target_price				= 0 ;//by costing
$approved_price				= 0; //by costing
$qtyPricesEnable            = 0;

$arr_header_not_saved	= get_header($x_graphicNo_o,$x_sampleNo_o,$x_sampleYear_o,$x_revNo_o,$x_combo_o,$x_print_o,$prod_location,$routing);
$arr_header_saved	= get_header_saved($x_graphicNo_o,$x_sampleNo_o,$x_sampleYear_o,$x_revNo_o,$x_combo_o,$x_print_o,$prod_location,$routing);
if($type=='')
	$type	=3;

if($type==1)//copy from another costing
{
	$arr_header		= get_header($x_graphicNo_o,$x_sampleNo_o,$x_sampleYear_o,$x_revNo_o,$x_combo_o,$x_print_o,$prod_location,$routing);	
	$arr_footer		= get_header_saved($x_graphicNo_c,$x_sampleNo_c,$x_sampleYear_c,$x_revNo_c,$x_combo_c,$x_print_c,$prod_location,$routing);
}
else if($type==4)//copy from old costing
{
	$arr_header		= get_header($x_graphicNo_o,$x_sampleNo_o,$x_sampleYear_o,$x_revNo_o,$x_combo_o,$x_print_o,$prod_location,$routing);	
	$arr_footer		= get_header_saved_old_costing($x_graphicNo_c_o,$x_sampleNo_c_o,$x_sampleYear_c_o,$x_revNo_c_o,$x_combo_c_o,$x_print_c_o,$prod_location,$routing);
}
else if($type==2)//new detailed costing
{
	$arr_header		= get_header($x_graphicNo_o,$x_sampleNo_o,$x_sampleYear_o,$x_revNo_o,$x_combo_o,$x_print_o,$prod_location,$routing);	
	$arr_footer		= get_header($x_graphicNo_o,$x_sampleNo_o,$x_sampleYear_o,$x_revNo_o,$x_combo_o,$x_print_o,$prod_location,$routing);	
//print_r($arr_header);
}
else if($type==3)//load saved costing
{
	if($arr_header_saved['STATUS'] !=''){
		$arr_header		= get_header_saved($x_graphicNo_o,$x_sampleNo_o,$x_sampleYear_o,$x_revNo_o,$x_combo_o,$x_print_o,$prod_location,$routing);
		$arr_footer		= get_header_saved($x_graphicNo_o,$x_sampleNo_o,$x_sampleYear_o,$x_revNo_o,$x_combo_o,$x_print_o,$prod_location,$routing);
	}
	else{
		$arr_header		= get_header($x_graphicNo_o,$x_sampleNo_o,$x_sampleYear_o,$x_revNo_o,$x_combo_o,$x_print_o,$prod_location,$routing);	
		$arr_footer		= get_header_saved($x_graphicNo_c,$x_sampleNo_c,$x_sampleYear_c,$x_revNo_c,$x_combo_c,$x_print_c,$prod_location,$routing);
	}
	if($x_sampleNo_c==''){
		$x_sampleNo_c	= $arr_header['COPPIED_FROM_SAMPLE_NO'];
		$x_sampleYear_c	= $arr_header['COPPIED_FROM_SAMPLE_YEAR'];
		$x_revNo_c 		= $arr_header['COPPIED_FROM_REVISION'];
		$x_combo_c		= $arr_header['COPPIED_FROM_COMBO'];
		$x_print_c		= $arr_header['COPPIED_FROM_PRINT'];
	}
	if($x_sampleNo_c_o==''){
		$x_sampleNo_c_o		= $arr_header['COPPIED_FROM_SAMPLE_NO'];
		$x_sampleYear_c_o	= $arr_header['COPPIED_FROM_SAMPLE_YEAR'];
		$x_revNo_c_o 		= $arr_header['COPPIED_FROM_REVISION'];
		$x_combo_c_o		= $arr_header['COPPIED_FROM_COMBO'];
		$x_print_c_o		= $arr_header['COPPIED_FROM_PRINT'];
	}
	if($arr_header['COPPIED_FROM_SAMPLE_NO'] > 0)
		$coppied	=1;
	else
		$coppied	=0;
		
	if($arr_header['COPPIED_TYPE'] > 0)
		$coppied	=$arr_header['COPPIED_TYPE'];
	else
		$coppied	=0;
		

}
$arr_copy_from	= get_header($x_graphicNo_c,$x_sampleNo_c,$x_sampleYear_c,$x_revNo_c,$x_combo_c,$x_print_c,$prod_location,$routing);
$arr_copy_from_o= get_header($x_graphicNo_c_o,$x_sampleNo_c_o,$x_sampleYear_c_o,$x_revNo_c_o,$x_combo_c_o,$x_print_c_o,$prod_location,$routing);
$qtyPricesEnable = $arr_footer['QTY_PRICES'];
if($arr_header_saved['STATUS']==1){//approved
	$status_string	= 'APPROVED';
	if($arr_header_saved['COPPIED_FROM_SAMPLE_NO'] !='')
		$coppiedString	= " - (COPPIED FROM ".$arr_header_saved['COPPIED_FROM_SAMPLE_NO']."/".$arr_header_saved['COPPIED_FROM_SAMPLE_YEAR']."/".$arr_header_saved['COPPIED_FROM_REVISION']."/".$arr_header_saved['COPPIED_FROM_COMBO']."/".$arr_header_saved['COPPIED_FROM_PRINT'].")";	
	else 
		$coppiedString	= " - DETAILED COSTING";
}
else if($arr_header_saved['STATUS']>1){//pending
	$status_string	= 'PENDING';
 	if($arr_header_saved['COPPIED_FROM_SAMPLE_NO'] !='')
		$coppiedString	= " - (COPPIED FROM ".$arr_header_saved['COPPIED_FROM_SAMPLE_NO']."/".$arr_header_saved['COPPIED_FROM_SAMPLE_YEAR']."/".$arr_header_saved['COPPIED_FROM_REVISION']."/".$arr_header_saved['COPPIED_FROM_COMBO']."/".$arr_header_saved['COPPIED_FROM_PRINT'].")";	
	else 
		$coppiedString	= " - DETAILED COSTING";
}
else if($arr_header_saved['STATUS']==''){//new
	$status_string	= 'NEW';
}
else if($arr_header_saved['STATUS']==0){//rejected
	$status_string	= 'REJECTED';
	if($arr_header_saved['COPPIED_FROM_SAMPLE_NO'] !='')
		$coppiedString	= " - (COPPIED FROM ".$arr_header_saved['COPPIED_FROM_SAMPLE_NO']."/".$arr_header_saved['COPPIED_FROM_SAMPLE_YEAR']."/".$arr_header_saved['COPPIED_FROM_REVISION']."/".$arr_header_saved['COPPIED_FROM_COMBO']."/".$arr_header_saved['COPPIED_FROM_PRINT'].")";	
	else 
		$coppiedString	= " - DETAILED COSTING";
}

//header variables----------------
$x_graphicNo		=	$arr_header_not_saved['graphic'];
$x_fabric			=	$arr_header_not_saved['fab_type'];
if($type==1)
$x_prod_loc			=	$arr_footer['prod_location'];
else
$x_prod_loc			=	$arr_header['prod_location'];
$x_routing			=	$arr_header['ROUTING'];
$x_no_of_ups		=	$arr_header['ups'];
$x_colors			=	$arr_header_not_saved['noOfColors'];
$x_print_width		=	$arr_header_not_saved['print_width'];
$x_print_height		=	$arr_header_not_saved['print_height'];
$x_placement		=	$arr_header_not_saved['partName'];
$x_estimated_qty	=	$arr_header['ESTIMATED_ORDER_QTY'];
$x_max_ups			=	get_max_ups($x_sampleNo_o,$x_sampleYear_o,$x_revNo_o,$x_combo_o,$x_print_o);
//--------------------------------

//print_r($arr_header);	
?>
<title>Sample Pre Costing</title>
<form id="frmSampleCostSheet" name="frmSampleCostSheet" method="post" action="">
<div align="center">
		<div class="trans_layoutL" style="width:1200px">
		  <div class="trans_text">Sample Cost Sheet</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <tr><td><table width="100%" class="bordered">
              
              <tr  nowrap="nowrap" class="headerLoad">
                <th width="7%" class="normalfnt">Graphic No</th>
                <td width="16%"><select name="cboStyleNo" id="cboStyleNo" style="width:140px">
                  <option value=""></option>
                  <?php
				    $sql = "SELECT DISTINCT
								trn_sampleinfomations.strGraphicRefNo
							FROM trn_sampleinfomations
							ORDER BY
								trn_sampleinfomations.strGraphicRefNo ASC
					";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$strGraphicRefNo = $row['strGraphicRefNo'];
						if($x_graphicNo==$strGraphicRefNo)
							echo "<option selected=\"selected\" value=\"$strStyle\">$strGraphicRefNo</option>";
						else
							echo "<option value=\"$strGraphicRefNo\">$strGraphicRefNo</option>";
					}
				?>
                </select></td>
                <th width="7%" class="normalfnt">Sample Year</th>
                <td width="8%"><select name="cboYear" id="cboYear" style="width:80px">
                  <?php
				    $d = date('Y');
				  	for($d;$d>=2012;$d--)
					{
						if($d==$x_sampleYear_o)
							echo "<option selected=\"selected\" id=\"$d\" >$d</option>";	
						else
							echo "<option id=\"$d\" >$d</option>";	
					}
				  ?>
                </select></td>
                <th width="5%" class="normalfnt">Sample No</th>
                <td width="11%"><select name="cboSampleNo" id="cboSampleNo" style="width:120px">
                  <option value=""></option>
                  <?php
				  if($x_sampleYear_o!='')
				  	$d	= $x_sampleYear_o;
				  else
					$d = date('Y');
				   
				   	$sql = "SELECT DISTINCT
							trn_sampleinfomations.intSampleNo
							FROM trn_sampleinfomations
						WHERE
							trn_sampleinfomations.intSampleYear =  '$d' /*and intCompanyId = '$locationId'*/ 
							 AND 
					(trn_sampleinfomations.intTechnicalStatus 	= '1' OR trn_sampleinfomations.intStatus=2 OR trn_sampleinfomations.intStatus=1)  
						ORDER BY
							trn_sampleinfomations.intSampleNo ASC
					";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$no = $row['intSampleNo'];
						if($no==$x_sampleNo_o)
							echo "<option selected=\"selected\" value=\"$no\">$no</option>";
						else
							echo "<option value=\"$no\">$no</option>";
					}
				?>
                </select></td>
                <th width="8%" class="normalfnt">Revision No</th>
                <td width="6%"><select name="cboRevisionNo" id="cboRevisionNo" style="width:60px">
                  <option value=""></option>
                  <?php
				 $sql = "SELECT
					trn_sampleinfomations.intRevisionNo
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleYear =  '$x_sampleYear_o' AND
					trn_sampleinfomations.intSampleNo =  '$x_sampleNo_o'
				ORDER BY
					trn_sampleinfomations.intRevisionNo ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($x_revNo_o==$row['intRevisionNo'])
						echo "<option selected=\"selected\" value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
					else
						echo "<option value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
				}
				  ?>
                </select></td>
                <th width="5%" class="normalfnt">Combo</th>
                <td width="12%"><select name="cboCombo" id="cboCombo" style="width:140px">
                  <option value=""></option>
              <?php
				$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strComboName
					FROM trn_sampleinfomations_details
				WHERE
					trn_sampleinfomations_details.intSampleNo =  '$x_sampleNo_o' AND
					trn_sampleinfomations_details.intSampleYear =  '$x_sampleYear_o'  AND
					trn_sampleinfomations_details.intRevNo =  '$x_revNo_o'
				ORDER BY
					trn_sampleinfomations_details.strComboName ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($x_combo_o==$row['strComboName'])
						echo "<option selected=\"selected\" value=\"".$row['strComboName']."\">".$row['strComboName']."</option>";
					else
						echo "<option value=\"".$row['strComboName']."\">".$row['strComboName']."</option>";
				}
				  ?>
                </select></td>
                <th width="3%">Print</th>
                <td width="12%"><select name="cboPrint" id="cboPrint" style="width:140px">
                  <option value=""></option>
                  <?php
				$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strPrintName, 
					trn_sampleinfomations_printsize.intPart,
					mst_part.strName as part 
					FROM trn_sampleinfomations_details 
					Inner Join trn_sampleinfomations_printsize ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_printsize.intSampleNo AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_printsize.intSampleYear AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_printsize.intRevisionNo AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_printsize.strPrintName
					Inner Join mst_part ON trn_sampleinfomations_printsize.intPart = mst_part.intId
				WHERE
					trn_sampleinfomations_details.intSampleNo 	=  '$x_sampleNo_o' AND
					trn_sampleinfomations_details.intSampleYear =  '$x_sampleYear_o' AND
					trn_sampleinfomations_details.strComboName 		=  '$x_combo_o'   AND
					trn_sampleinfomations_details.intRevNo =  '$x_revNo_o'
				ORDER BY
					trn_sampleinfomations_details.strPrintName ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($x_print_o==$row['strPrintName'])
						echo "<option selected=\"selected\" value=\"".$row['strPrintName']."\">".$row['part']."/".$row['strPrintName']."</option>";
					else
						echo "<option value=\"".$row['strPrintName']."\">".$row['part']."/".$row['strPrintName']."</option>";
				}
				  ?>
                </select></td>
              </tr>
          <tr>
            <td height="22" class="normalfnt" bgcolor="#FF9B9B" colspan="12"><b><?php echo $status_string.$coppiedString ?></b></td>
          </tr>
              
          <tr  nowrap="nowrap" bgcolor="#CCCCCC" >
            <td colspan="2"><span class="normalfnt">
              <input class="chkType" value="2" type="radio" name="chkType" id="chkType" <?php if($type==2){ ?>checked="checked" <?php } ?> />
              &nbsp;New Detailed Costing</span></td>
            <td colspan="3"><span class="normalfnt">
              <input class="chkType" value="3" type="radio" name="chkType" id="chkType" <?php if($type==3){ ?>checked="checked" <?php } ?>  />
  &nbsp;Load Saved Costing</span></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr  nowrap="nowrap" bgcolor="#CCCCCC" >
                <td class="normalfnt" nowrap="nowrap" colspan="2"><input class="chkType" value="1" type="radio" name="chkType" id="chkType"  <?php if($type==1){ ?>checked="checked" <?php } ?>  />&nbsp;Copy From Another Costing</td>
                <td colspan="4"><input class="chkType" value="4" type="radio" name="chkType" id="chkType"  <?php if($type==4){ ?>checked="checked" <?php } ?>  />
                  &nbsp;Copy From Old Costing Method</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>      
              <!-- Copy from a costing which was saved using new costing method -->
<tr  nowrap="nowrap" class="headerLoad copyFrom" <?php if($coppied!=1 && $type!=1){ ?>style="display:none" <?php } ?>>
                <th width="7%" class="normalfnt">Graphic No</th>
                <td width="16%"><select name="cboStyleNoC" id="cboStyleNoC" style="width:140px">
                  <option value=""></option>
                  <?php
				    $sql = "SELECT DISTINCT
								trn_sampleinfomations.strGraphicRefNo
							FROM trn_sampleinfomations
							ORDER BY
								trn_sampleinfomations.strGraphicRefNo ASC
					";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$strGraphicRefNo = $row['strGraphicRefNo'];
						if($arr_copy_from['graphic']==$strGraphicRefNo)
							echo "<option selected=\"selected\" value=\"$strStyle\">$strGraphicRefNo</option>";
						else
							echo "<option value=\"$strGraphicRefNo\">$strGraphicRefNo</option>";
					}
				?>
                </select></td>
                <th width="7%" class="normalfnt">Sample Year</th>
                <td width="8%"><select name="cboYearC" id="cboYearC" style="width:80px">
                  <?php
				    $d = date('Y');
				  	for($d;$d>=2012;$d--)
					{
						if($d==$x_sampleYear_c)
							echo "<option selected=\"selected\" id=\"$d\" >$d</option>";	
						else
							echo "<option id=\"$d\" >$d</option>";	
					}
				  ?>
                </select></td>
                <th width="5%" class="normalfnt">Sample No</th>
                <td width="11%"><select name="cboSampleNoC" id="cboSampleNoC" style="width:120px">
                  <option value=""></option>
                  <?php
					$d = date('Y');
					if($x_sampleYear_c=='')
						$x_sampleYear_c = $d;
				   	$sql = "SELECT DISTINCT
							trn_sampleinfomations.intSampleNo
							FROM trn_sampleinfomations
				INNER JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO AND trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR AND trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
						WHERE
							trn_sampleinfomations.intSampleYear =  '$x_sampleYear_c' /*and intCompanyId = '$locationId'*/
							 AND costing_sample_header.`STATUS` = 1  					
							ORDER BY
							trn_sampleinfomations.intSampleNo ASC
					";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$no = $row['intSampleNo'];
						if($no==$x_sampleNo_c)
							echo "<option selected=\"selected\" value=\"$no\">$no</option>";
						else
							echo "<option value=\"$no\">$no</option>";
					}
				?>
                </select></td>
                <th width="8%" class="normalfnt">Revision No</th>
                <td width="6%"><select name="cboRevisionNoC" id="cboRevisionNoC" style="width:60px">
                  <option value=""></option>
                  <?php
				 $sql = "SELECT
					trn_sampleinfomations.intRevisionNo
				FROM trn_sampleinfomations
				INNER JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO AND trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR AND trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
				WHERE
					trn_sampleinfomations.intSampleYear =  '$x_sampleYear_c' AND
					trn_sampleinfomations.intSampleNo =  '$x_sampleNo_c' AND
					(trn_sampleinfomations.intTechnicalStatus 	= '1' OR trn_sampleinfomations.intStatus=2 OR trn_sampleinfomations.intStatus=1) 
					AND costing_sample_header.`STATUS` = 1 
				ORDER BY
					trn_sampleinfomations.intRevisionNo ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($x_revNo_c==$row['intRevisionNo'])
						echo "<option selected=\"selected\" value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
					else
						echo "<option value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
				}
				  ?>
                </select></td>
                <th width="5%" class="normalfnt">Combo</th>
                <td width="12%"><select name="cboComboC" id="cboComboC" style="width:140px">
                  <option value=""></option>
              <?php
				$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strComboName
					FROM trn_sampleinfomations_details
INNER JOIN costing_sample_header ON trn_sampleinfomations_details.intSampleNo = costing_sample_header.SAMPLE_NO AND trn_sampleinfomations_details.intSampleYear = costing_sample_header.SAMPLE_YEAR AND trn_sampleinfomations_details.intRevNo = costing_sample_header.REVISION AND trn_sampleinfomations_details.strComboName = costing_sample_header.COMBO AND trn_sampleinfomations_details.strPrintName = costing_sample_header.PRINT
				WHERE
					trn_sampleinfomations_details.intSampleNo =  '$x_sampleNo_c' AND
					trn_sampleinfomations_details.intSampleYear =  '$x_sampleYear_c' 
					AND costing_sample_header.`STATUS` = 1 
				ORDER BY
					trn_sampleinfomations_details.strComboName ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($x_combo_c==$row['strComboName'])
						echo "<option selected=\"selected\" value=\"".$row['strComboName']."\">".$row['strComboName']."</option>";
					else
						echo "<option value=\"".$row['strComboName']."\">".$row['strComboName']."</option>";
				}
				  ?>
                </select></td>
                <th width="3%">Print</th>
                <td width="12%"><select name="cboPrintC" id="cboPrintC" style="width:140px">
                  <option value=""></option>
                  <?php
				$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strPrintName, 
					trn_sampleinfomations_printsize.intPart,
					mst_part.strName as part 
					FROM trn_sampleinfomations_details 
					Inner Join trn_sampleinfomations_printsize ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_printsize.intSampleNo AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_printsize.intSampleYear AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_printsize.intRevisionNo AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_printsize.strPrintName
					Inner Join mst_part ON trn_sampleinfomations_printsize.intPart = mst_part.intId
INNER JOIN costing_sample_header ON trn_sampleinfomations_details.intSampleNo = costing_sample_header.SAMPLE_NO AND trn_sampleinfomations_details.intSampleYear = costing_sample_header.SAMPLE_YEAR AND trn_sampleinfomations_details.intRevNo = costing_sample_header.REVISION AND trn_sampleinfomations_details.strComboName = costing_sample_header.COMBO AND trn_sampleinfomations_details.strPrintName = costing_sample_header.PRINT
				WHERE
					trn_sampleinfomations_details.intSampleNo 	=  '$x_sampleNo_c' AND
					trn_sampleinfomations_details.intSampleYear =  '$x_sampleYear_c' AND
					trn_sampleinfomations_details.strComboName 		=  '$x_combo_c' 
					AND costing_sample_header.`STATUS` = 1 
				ORDER BY
					trn_sampleinfomations_details.strPrintName ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($x_print_c==$row['strPrintName'])
						echo "<option selected=\"selected\" value=\"".$row['strPrintName']."\">".$row['part']."/".$row['strPrintName']."</option>";
					else
						echo "<option value=\"".$row['strPrintName']."\">".$row['part']."/".$row['strPrintName']."</option>";
				}
				  ?>
                </select></td>
              </tr> 
                            <!-- Copy from a costing which was saved using old costing method -->
 
<tr  nowrap="nowrap" class="headerLoad copyFromOld" <?php if($coppied!=2 && $type!=4){ ?>style="display:none" <?php } ?>>
                <th width="7%" class="normalfnt">Graphic No</th>
                <td width="16%"><select name="cboStyleNoC_O" id="cboStyleNoC_O" style="width:140px">
                  <option value=""></option>
                  <?php
				    $sql = "SELECT DISTINCT
								trn_sampleinfomations.strGraphicRefNo
							FROM trn_sampleinfomations
							ORDER BY
								trn_sampleinfomations.strGraphicRefNo ASC
					";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$strGraphicRefNo = $row['strGraphicRefNo'];
						if($arr_copy_from_o['graphic']==$strGraphicRefNo)
							echo "<option selected=\"selected\" value=\"$strStyle\">$strGraphicRefNo</option>";
						else
							echo "<option value=\"$strGraphicRefNo\">$strGraphicRefNo</option>";
					}
				?>
                </select></td>
                <th width="7%" class="normalfnt">Sample Year</th>
                <td width="8%"><select name="cboYearC_O" id="cboYearC_O" style="width:80px">
                  <?php
				    $d = date('Y');
				  	for($d;$d>=2012;$d--)
					{
						if($d==$x_sampleYear_c_o)
							echo "<option selected=\"selected\" id=\"$d\" >$d</option>";	
						else
							echo "<option id=\"$d\" >$d</option>";	
					}
				  ?>
                </select></td>
                <th width="5%" class="normalfnt">Sample No</th>
                <td width="11%"><select name="cboSampleNoC_O" id="cboSampleNoC_O" style="width:120px">
                  <option value=""></option>
                  <?php
					$d = date('Y');
					if($x_sampleYear_c_o=='')
						$x_sampleYear_c_o = $d;
				   	$sql = "SELECT DISTINCT
							trn_sampleinfomations.intSampleNo
							FROM trn_sampleinfomations
							INNER JOIN trn_sampleinfomations_prices 
							ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_prices.intSampleNo 
							AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_prices.intSampleYear 
							AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_prices.intRevisionNo
							left JOIN costing_sample_header ON costing_sample_header.SAMPLE_NO = trn_sampleinfomations_prices.intSampleNo 
							AND costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations_prices.intSampleYear 
							AND costing_sample_header.REVISION = trn_sampleinfomations_prices.intRevisionNo 
							AND costing_sample_header.COMBO = trn_sampleinfomations_prices.strCombo 
							AND costing_sample_header.PRINT = trn_sampleinfomations_prices.strPrintName
							WHERE
							trn_sampleinfomations.intSampleYear =  '$x_sampleYear_c_o' /*and intCompanyId = '$locationId'*/
							AND costing_sample_header.`SAMPLE_NO` IS NULL  					
							ORDER BY
							trn_sampleinfomations.intSampleNo ASC
					";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$no = $row['intSampleNo'];
						if($no==$x_sampleNo_c_o)
							echo "<option selected=\"selected\" value=\"$no\">$no</option>";
						else
							echo "<option value=\"$no\">$no</option>";
					}
				?>
                </select></td>
                <th width="8%" class="normalfnt">Revision No</th>
                <td width="6%"><select name="cboRevisionNoC_O" id="cboRevisionNoC_O" style="width:60px">
                  <option value=""></option>
                  <?php
				 $sql = "SELECT distinct 
					trn_sampleinfomations.intRevisionNo
				FROM trn_sampleinfomations
							INNER JOIN trn_sampleinfomations_prices 
							ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_prices.intSampleNo 
							AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_prices.intSampleYear 
							AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_prices.intRevisionNo
							left JOIN costing_sample_header ON costing_sample_header.SAMPLE_NO = trn_sampleinfomations_prices.intSampleNo 
							AND costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations_prices.intSampleYear 
							AND costing_sample_header.REVISION = trn_sampleinfomations_prices.intRevisionNo 
							AND costing_sample_header.COMBO = trn_sampleinfomations_prices.strCombo 
							AND costing_sample_header.PRINT = trn_sampleinfomations_prices.strPrintName
				WHERE
					trn_sampleinfomations.intSampleYear =  '$x_sampleYear_c_o' AND
					trn_sampleinfomations.intSampleNo =  '$x_sampleNo_c_o'  AND
					costing_sample_header.`SAMPLE_NO` IS NULL  
				ORDER BY
					trn_sampleinfomations.intRevisionNo ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($x_revNo_c_o==$row['intRevisionNo'])
						echo "<option selected=\"selected\" value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
					else
						echo "<option value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
				}
				  ?>
                </select></td>
                <th width="5%" class="normalfnt">Combo</th>
                <td width="12%"><select name="cboComboC_O" id="cboComboC_O" style="width:140px">
                  <option value=""></option>
              <?php
				$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strComboName
					FROM trn_sampleinfomations_details
INNER JOIN trn_sampleinfomations_prices ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_prices.intSampleNo AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_prices.intSampleYear AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_prices.intRevisionNo AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_prices.strCombo AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_prices.strPrintName
					left JOIN costing_sample_header ON costing_sample_header.SAMPLE_NO = trn_sampleinfomations_prices.intSampleNo 
					AND costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations_prices.intSampleYear 
					AND costing_sample_header.REVISION = trn_sampleinfomations_prices.intRevisionNo 
					AND costing_sample_header.COMBO = trn_sampleinfomations_prices.strCombo 
					AND costing_sample_header.PRINT = trn_sampleinfomations_prices.strPrintName
				WHERE
					trn_sampleinfomations_details.intSampleNo =  '$x_sampleNo_c_o' AND
					trn_sampleinfomations_details.intSampleYear =  '$x_sampleYear_c_o' AND
					costing_sample_header.`SAMPLE_NO` IS NULL  
				ORDER BY
					trn_sampleinfomations_details.strComboName ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($x_combo_c_o==$row['strComboName'])
						echo "<option selected=\"selected\" value=\"".$row['strComboName']."\">".$row['strComboName']."</option>";
					else
						echo "<option value=\"".$row['strComboName']."\">".$row['strComboName']."</option>";
				}
				  ?>
                </select></td>
                <th width="3%">Print</th>
                <td width="12%"><select name="cboPrintC_O" id="cboPrintC_O" style="width:140px">
                  <option value=""></option>
                  <?php
				$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strPrintName, 
					trn_sampleinfomations_printsize.intPart,
					mst_part.strName as part, 
					costing_sample_header.QTY_PRICES 
					FROM trn_sampleinfomations_details 
					Inner Join trn_sampleinfomations_printsize ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_printsize.intSampleNo AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_printsize.intSampleYear AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_printsize.intRevisionNo AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_printsize.strPrintName
					Inner Join mst_part ON trn_sampleinfomations_printsize.intPart = mst_part.intId
INNER JOIN trn_sampleinfomations_prices ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_prices.intSampleNo AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_prices.intSampleYear AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_prices.intRevisionNo AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_prices.strCombo AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_prices.strPrintName
					left JOIN costing_sample_header ON costing_sample_header.SAMPLE_NO = trn_sampleinfomations_prices.intSampleNo 
					AND costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations_prices.intSampleYear 
					AND costing_sample_header.REVISION = trn_sampleinfomations_prices.intRevisionNo 
					AND costing_sample_header.COMBO = trn_sampleinfomations_prices.strCombo 
					AND costing_sample_header.PRINT = trn_sampleinfomations_prices.strPrintName
				WHERE
					trn_sampleinfomations_details.intSampleNo 	=  '$x_sampleNo_c_o' AND
					trn_sampleinfomations_details.intSampleYear =  '$x_sampleYear_c_o' AND
					trn_sampleinfomations_details.strComboName 		=  '$x_combo_c_o'  AND
					costing_sample_header.`SAMPLE_NO` IS NULL  
				ORDER BY
					trn_sampleinfomations_details.strPrintName ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($x_print_c_o==$row['strPrintName']){
                        echo "<option selected=\"selected\" value=\"".$row['strPrintName']."\">".$row['part']."/".$row['strPrintName']."</option>";
                    } else {
                        echo "<option value=\"".$row['strPrintName']."\">".$row['part']."/".$row['strPrintName']."</option>";
                    }
				}
				  ?>
                </select></td>
              </tr>
            </table></td></tr>
    <tr><td>
    <table width="100%" border="0" id="tableCostingDeails" <?php if($arr_header['COPPIED_FROM_SAMPLE_NO'] >0 && $type==3 || $arr_header['COPPIED_FROM_SAMPLE_NO'] >0 && $type==2){ ?> style="display:none" <?php } ?> >
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="22" colspan="8" class="normalfnt"><table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td><?php //echo $x_sampleNo_o.'/'.$x_sampleYear_o.'/'.$x_revNo_o.'/'.$x_combo_o.'/'.$x_print; ?></td></tr>
            </table></td>
            </tr>
          <tr>
            <td width="218" height="22" class="normalfnt">&nbsp;</td>
            <td width="312">&nbsp;</td>
            <td colspan="2" class="normalfntMid"><strong>Graphic</strong></td>
            <td width="38" class="normalfnt">&nbsp;</td>
            <td width="203" class="normalfnt">&nbsp;</td>
            <td width="151" class="normalfnt">&nbsp;</td>
            <td width="95" align="left">&nbsp;</td>
            </tr>
          <?php
		  ?>
          <tr>
            <td class="normalfnt">Graphic Placement</td>
            <td class="normalfnt"><input type="text" name="textfield" id="textfield" style="width:200px" value="<?php echo $x_placement ?>" disabled="disabled"/></td>
            <td colspan="2" rowspan="6" align="center" ><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:264px;height:148px;overflow:hidden" >
              <?php
			if($x_sampleNo_o!='')
			{
				echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"../../../../../documents/sampleinfo/samplePictures/$x_sampleNo_o-$x_sampleYear_o-$x_revNo_o-".substr($x_print_o,6,1).".png\" />";	
			}
			 ?>
              </div></td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Graphic Width</td>
            <td class="normalfnt"><input name="txtGraphicW" type="text" id="txtGraphicW" style="width:60px; text-align:right" value="<?php echo $x_print_width ;?>" disabled="disabled"/>&nbsp;Inches </td>
            <td class="normalfnt">&nbsp;</td>
          </tr>
<tr>
            <td class="normalfnt">Fabric</td>
            <td style="width:30px"><input name="txtFabric" type="text" id="txtFabric" style="width:200px" value="<?php echo $x_fabric ;?>" disabled="disabled"/></td>
            <td align="left" class="normalfnt">&nbsp;</td>
            
            <td align="left" class="normalfnt">Graphic Height</td>
            <td align="left"><span class="normalfnt">
              <input name="txtGraphicH" type="text" id="txtGraphicH" style="width:60px; text-align:right" value="<?php echo $x_print_height ;?>" disabled="disabled"/>&nbsp;Inches</span></td>
            <td class="normalfnt">&nbsp;</td>
            </tr> 
			<tr>
            <td><span class="normalfnt">Production Location</span></td>
            <td><span style="width:30px">
              <select name="cboLocation" id="cboLocation" style="width:200px" class="validate[required]">
                <option value=""></option>
                <?php
				$sql = "SELECT DISTINCT
					mst_locations.intId,
					mst_locations.strName
					FROM mst_locations
				WHERE
					mst_locations.intStatus =  '1'  
				ORDER BY
					mst_locations.strName ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($x_prod_loc==$row['intId'])
						echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
					else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
				}
				  ?>
              </select>
              </span></td>
            <td>&nbsp;</td>
            <td><span class="normalfnt">Estimated order Qty</span></td>
            <td><input name="textEstimated" type="text" id="textEstimated" style="width:100px; text-align:right" value="<?php echo $x_estimated_qty; ?>" class="validate[custom[number]] estimatedQty" /></td>
            </tr>
             <tr>
            <td class="normalfnt">Routing</td>
            <td><span style="width:30px">
              <select name="cboRouting" id="cboRouting" style="width:200px">
               <!-- <option value=""></option>-->
                <?php
			if($status=='' && $type==1){
				$sql = "SELECT DISTINCT
					mst_routing.ID,
					mst_routing.DESCRIPTION
					FROM mst_routing 
					/*inner join trn_sampleinfomations_combo_print_routing on 
					trn_sampleinfomations_combo_print_routing.ROUTING = mst_routing.ID 
				WHERE
					mst_routing.STATUS =  '1' 
					and  trn_sampleinfomations_combo_print_routing.SAMPLE_NO = '$x_sampleNo_c'
					AND trn_sampleinfomations_combo_print_routing.SAMPLE_YEAR	='$x_sampleYear_c' 
					AND trn_sampleinfomations_combo_print_routing.REVISION = '$x_revNo_c' 
					AND trn_sampleinfomations_combo_print_routing.COMBO = '$x_combo_c'  
					AND trn_sampleinfomations_combo_print_routing.PRINT = '$x_print_c'*/
				ORDER BY
					mst_routing.DESCRIPTION ASC
				";
				}
				else{
				$sql = "SELECT DISTINCT
					mst_routing.ID,
					mst_routing.DESCRIPTION
					FROM mst_routing 
					/*inner join trn_sampleinfomations_combo_print_routing on 
					trn_sampleinfomations_combo_print_routing.ROUTING = mst_routing.ID 
				WHERE
					mst_routing.STATUS =  '1' 
					and  trn_sampleinfomations_combo_print_routing.SAMPLE_NO = '$x_sampleNo'
					AND trn_sampleinfomations_combo_print_routing.SAMPLE_YEAR	='$x_sampleYear' 
					AND trn_sampleinfomations_combo_print_routing.REVISION = '$x_revNo' 
					AND trn_sampleinfomations_combo_print_routing.COMBO = '$x_combo'  
					AND trn_sampleinfomations_combo_print_routing.PRINT = '$x_print' */
				ORDER BY
					mst_routing.DESCRIPTION ASC
				";
				}
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($x_routing==$row['ID'])
						echo "<option selected=\"selected\" value=\"".$row['ID']."\">".$row['DESCRIPTION']."</option>";
					else
						echo "<option value=\"".$row['ID']."\">".$row['DESCRIPTION']."</option>";
				}
				  ?>
              </select>
            </span></td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td align="left">&nbsp;</td>
          </tr>
          <tr>
            <td class="normalfnt">No of ups </td>
            <td style="width:30px"><input name="textNoUp" type="text" id="textNoUp" style="width:100px; text-align:right" value="<?php echo $x_no_of_ups;?>"  class="noOfups cal_cost1" max="<?php echo $x_no_of_ups;?>"/><div id="max_ups" style="display:none"><?php echo $x_max_ups;?></div></td><?php
?>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td align="left">&nbsp;</td>
            </tr>
          
            
            <tr>
            <td><span class="normalfnt">No of Colors</span></td>
            <td><input name="textfield10" type="text" id="textfield10" style="width:100px; text-align:right" value="<?php echo $x_colors ; ?>" disabled="disabled" class="noOfColors" /></td>
            <td>&nbsp;</td>
            <td><span class="normalfnt">View Current Sample Sheet</td>
            <td class="normalfnt"><a id="butcurrentView" class="button green small" style="" name="butcurrentView" on> View </a>&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td id="ink_type_td" <?php if($coppied==1){ ?>style="display:none"<?php } ?>><div style="width:1200px;height:350px;overflow:scroll" >
          <table width="100%" class="bordered" id="table_ink_type" >
              <tr>
                   <th height="17" colspan="11"  class="normalfntMid"><strong>Ink Type Cost</strong></th>
              </tr>
            <tr>
              <th width="4%" >View Similer</th>
              <th width="15%" height="22" >Color</th>
              <th width="11%" >Technique</th>
              <th width="15%" >Ink Type</th>
              <th width="8%" style="display:none" >Category</th>
              <th width="5%" >No of Shots</th>
              <th width="8%" >Cost Per sq inch ($)</th>
              <th width="9%">Square Inches(W*H)</th>
              <th width="6%">Currency</th>
              <th width="8%">Usage %</th>
              <th width="11%">Total Cost Price ($)</th>
              </tr>
                    <?php 
						
						if($type==1 and $approved!=1)
							$result_tech =	get_details_array($x_graphicNo_c,$x_sampleNo_c,$x_sampleYear_c,$x_revNo_c,$x_combo_c,$x_print_c);
						else 
							$result_tech =	get_details_array($x_graphicNo_o,$x_sampleNo_o,$x_sampleYear_o,$x_revNo_o,$x_combo_o,$x_print_o);
						
						
						while($row=mysqli_fetch_array($result_tech))
						{
							$colorId		=$row['intColorId'];
							$color			=$row['color'];
							$techniqueId	=$row['intTechniqueId'];
							$technique		=$row['technique'];
							$inkTypeId		=$row['intInkTypeId'];
							$ink_status		=$row['intStatus'];
							$inkType_edit	=$row['EDITABLE_INK_COST'];
							$inkType		=$row['inkType'];
							$shots			=$row['shots'];
							$costPerInch	=$row['dblCostPerInch'];
							$currencyId		=$row['intCurrencyId'];
							$currency		=$row['currency'];
							$itemId			=$row['intItem'];
							$item			=$row['itemName'];
							$usage			=$row['dblUsage'];
							
							$category		=$row['CATEGORY'];
							$shots_saved	=$row['shots_saved'];
							$cost_p_sq_saved=$row['COST_PER_SQ_INCH'];
							$usage_saved	=$row['usage_saved'];
							$cost_saved		=$row['TOTAL_COST_PRICE'];
							if(!$shots_saved || $type==2)
								$shots_saved=$row['shots'];
							if(!$cost_p_sq_saved || $type==2)
								$cost_p_sq_saved=$row['dblCostPerInch'];
							if(!$usage_saved || $type==2)
								$usage_saved=$row['dblUsage'];
							if(!$cost_saved || $type==2)
								$cost_saved=$arr_header['print_width']*$arr_header['print_height']*$cost_p_sq_saved*$shots_saved*$usage_saved/100;
					
					?>
            <tr class="normalfnt dataRow">
              <td bgcolor="#FFFFFF" id="viewsimiler" class="viewSimmi"><a id="butSimilerView" class="button green small" style="" name="butSimilerView" on> View </a></td>
              <td bgcolor="#FFFFFF" id="<?php echo $colorId ; ?>" class="color"><?php echo $color ; ?></td>
              <td bgcolor="#FFFFFF" id="<?php echo $techniqueId ; ?>" class="technique_getid"><?php echo $technique ; ?></td>
              <td bgcolor="#FFFFFF" id="<?php echo $inkTypeId ; ?>" class="inkType"><?php echo $inkType ; ?></td>
              <td bgcolor="#FFFFFF" style="display:none" id="<?php echo $category ; ?>" class="ink_Cat"><select name="cboInkCategory" id="cboInkCategory" style="width:200px" class="validate[required]" disabled="disabled">
                  <option value=""></option>
                  <?php
				$sql = "SELECT
						mst_ink_type_category.ID,
						mst_ink_type_category.DESCRIPTION
						FROM `mst_ink_type_category`
						ORDER BY
						mst_ink_type_category.DESCRIPTION ASC

				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($category==$row['ID'])
						echo "<option selected=\"selected\" value=\"".$row['ID']."\">".$row['DESCRIPTION']."</option>";
					else
						echo "<option value=\"".$row['ID']."\">".$row['DESCRIPTION']."</option>";
				}
				  ?>
                </select></td>
              <td bgcolor="#FFFFFF" id="<?php echo $itemId ; ?>" class="ink_shots"><?php echo $shots_saved ; ?></td>
              <td bgcolor="#FFFFFF" align="center"><input type="text" name="textfield6" id="textfield6" style="width:90px;text-align:right" value="<?php echo $cost_p_sq_saved; ?>" <?php if($ink_status==1 && $inkType_edit==0 && $cost_p_sq_saved >0 ){ ?>disabled="disabled"<?php } ?> class="validate[required,custom[number]] cal_cost inkCostPerSqInch"/></td>
              <td bgcolor="#FFFFFF" id="inches" class="inches" align="center"><?php echo $arr_header['print_width']*$arr_header['print_height'] ; ?></td>
              <td bgcolor="#FFFFFF" id="<?php echo $currencyId ; ?>" class="currency" align="center">USD</td>
              <td bgcolor="#FFFFFF" align="center"><input name="textfield9" type="text" id="textfield9" style="width:90px;text-align:right" value="<?php echo $usage_saved; ?>" class="validate[required,custom[number],max[100]] usage cal_cost"/></td>
              <td bgcolor="#FFFFFF" align="right" class="totCost_ink_type1"><input name="textfield9" type="text" id="textfield9" style="width:90px;text-align:right" value="<?php echo $cost_saved; ?>" class="totCost_ink_type cal_cost" <?php if($shots_saved > 0){ ?>disabled="disabled" <?php } ?>/></td>
              </tr>
              <?php
			  $tot_ink_prie		+= $cost_saved;
				 }
				?>
            </table>
          </div></td>
      </tr>
      <tr>
        <td align="center" class=""><table width="100%" border="0">
          <tr>
            <td class="normalfnt" colspan="6" id="rf_td" <?php if($coppied==1){ ?>style="display:none"<?php } ?>><table width="1263" cellpadding="0" cellspacing="0" class="bordered" id="table_roll_form">
              <tr class="">
                <th height="17" colspan="13" class="normalfntMid"><strong> RM - Roll Form Cost</strong></th>
              </tr>
              <tr>
                <th width="212" height="19">Colour</th>
                <th width="212" height="19">Technique</th>
                <th width="210" height="19">Item</th>
                <th width="69">Grapic W</th>
                <th width="73">Grapic H</th>
                <th width="84">Item Width</th>
                <th width="104">Item Height</th>
                <th width="137">Price ($)</th>
                <th width="103">Add Sizes</th>
                <th width="103">Consumption</th>
                <th width="132">UOM</th>
                <th width="137">Cost ($)</th>
              </tr>
              <?php 
			  			
						if($type==1 and $approved!=1)
							$result_tech	=	load_roll_form_rm($x_graphicNo_c,$x_sampleNo_c,$x_sampleYear_c,$x_revNo_c,$x_combo_c,$x_print_c);
						else
							$result_tech	=	load_roll_form_rm($x_graphicNo_o,$x_sampleNo_o,$x_sampleYear_o,$x_revNo_o,$x_combo_o,$x_print_o);
						
						while($row=mysqli_fetch_array($result_tech))
						{
							$technique		=$row['technique'];
							$technique_id	=$row['technique_id'];
							$color			=$row['color'];
							$color_id		=$row['color_Id'];
							$item			=$row['item'];
							$item_id		=$row['item_id'];
							$item_w			=$row['foil_width'];
							$item_h			=$row['foil_height'];
							$uom			=$row['uom'];
							$auto_cons		=$row['auto_cons'];
							$conpc			=$row['conpc'];
							$price			=$row['price'];
							$graphic_w_saved	=$row['graphic_w_saved'];
							$graphic_h_saved	=$row['graphic_h_saved'];
							$item_w_saved		=$row['ITEM_W'];
							$item_h_saved		=$row['ITEM_H'];
							$price_saved		=$row['ITEM_PRICE'];
							$cons_saved			=$row['CONSUMPTION'];
							$cost_saved			=$row['COST'];
							if(!$graphic_w_saved)
								$graphic_w_saved=$arr_header['print_width'];
							if(!$graphic_h_saved)
								$graphic_h_saved=$arr_header['print_height'];
							if(!$item_w_saved || $type==2)
								$item_w_saved=$row['foil_width'];
							if(!$item_h_saved || $type==2)
								$item_h_saved=$row['foil_height'];
							if(!$price_saved || $type==2)
								$price_saved=$row['price'];
							if(!$cons_saved || $type==2)
								$cons_saved=$row['conpc'];
							if(!$cost_saved || $type==2)
								$cost_saved=$row['conpc']*$row['price'];
							//echo $auto_cons;
						 ?>
              <tr class="dataRow specil_rm">
                <td height="21" bgcolor="#FFFFFF" id="<?php echo $color_id; ?>" class="color_id" ><?php echo $color; ?></td>
                <td height="21" bgcolor="#FFFFFF" id="<?php echo $technique_id; ?>" class="technique_id" ><?php echo $technique; ?></td>
                <td height="21" bgcolor="#FFFFFF" id="<?php echo $item_id; ?>" class="itemId"><?php echo $item; ?></td>
                <td bgcolor="#FFFFFF" class="normalfnt"><input type="text" name="textfield15" id="textfield15" style="width:50px; text-align:right" value="<?php echo $graphic_w_saved; ?>" class="printWidth_roll" /></td>
                <td bgcolor="#FFFFFF" class="normalfnt"><input type="text" name="textfield16" id="textfield16" style="width:50px; text-align:right" value="<?php echo $graphic_h_saved; ?>" class="printHeight_roll" /></td>
                <td bgcolor="#FFFFFF" class="pcs normalfntRight" ><input type="text" name="textitem_w_roll" id="textitem_w" style="width:50px; text-align:right" value="<?php echo $item_w_saved; ?>" class="itemWidth"  /></td>
                <td bgcolor="#FFFFFF" class="pcs normalfntRight"><input type="text" name="textitem_h_roll" id="textitem_h" style="width:50px; text-align:right" value="<?php echo $item_h_saved; ?>" class="itemHeight"   /></td>
                <td bgcolor="#FFFFFF" class="pcs normalfntRight"><input type="text" name="textitem_h2" id="textitem_h2" style="width:100px; text-align:right" value="<?php echo $price_saved ?>" class="cal_cost price_roll" /></td>
                <td bgcolor="#FFFFFF" class="pcs normalfntRight" nowrap="nowrap"><div id="consId" class="auto_cons" style="display:none"><?php echo $auto_cons ;?></div><a id="butAdd" class="button green medium" style="" name="butAdd" > Add </a><a id="butView" class="button green medium" style="" name="butView" on> View </a>&nbsp;</td>
                <td bgcolor="#FFFFFF" class="pcs normalfntRight"><input type="text" name="textitem_h3" id="textitem_h3" style="width:100px; text-align:right" value="<?php echo $cons_saved; ?>" class="printWidth cal_cost consump_roll" /></td>
                <td bgcolor="#FFFFFF" class="meters normalfntRight"><?php echo $uom; ?></td>
                <td bgcolor="#FFFFFF" class="cost_roll normalfntRight"><?php echo $cost_saved; ?></td>
              </tr>
              <?php
						}
			  ?>
              </table></td>
            </tr>
            <tr height="15"><td colspan="6"></td></tr>
          <tr>
            <td class="normalfnt" colspan="6" id="nrf_td" <?php if($coppied==1){ ?>style="display:none"<?php } ?>><table width="1263" cellpadding="0" cellspacing="0" class="bordered" id="table_non_roll">
              <tr class="">
                <th height="17" colspan="13" class="normalfntMid"><strong> RM - Non-Roll Form Cost</strong></th>
              </tr>
              <tr>
                <th width="211" height="19">Color</th>
                <th width="211" height="19">Technique</th>
                <th width="211" height="19">Item</th>
                <th width="69">Grapic W</th>
                <th width="112">Grapic H</th>
                <th width="106">Price ($)</th>
                <th width="104">Consumption</th>
                <th width="49">UOM</th>
                <th width="70">Cost ($)</th>
                <th width="329">&nbsp;</th>
              </tr>
              <?php 
			  			if($type==1 and $approved!=1)
							$result_nrf =	load_non_roll_form_rm($x_graphicNo_c,$x_sampleNo_c,$x_sampleYear_c,$x_revNo_c,$x_combo_c,$x_print_c);
						else
			  				$result_nrf	=	load_non_roll_form_rm($x_graphicNo_o,$x_sampleNo_o,$x_sampleYear_o,$x_revNo_o,$x_combo_o,$x_print_o);

						while($row=mysqli_fetch_array($result_nrf))
						{
							$technique			=$row['technique'];
							$technique_id		=$row['technique_id'];
							$color				=$row['color'];
							$color_id			=$row['color_Id'];
							$item				=$row['item'];
							$item_id			=$row['item_id'];
							$uom				=$row['uom'];
							$conpc				=$row['conpc'];
							$price				=$row['price'];
							$graphic_w_saved	=$row['graphic_w_saved'];
							$graphic_h_saved	=$row['graphic_h_saved'];
							$price_saved		=$row['price_saved'];
							$cons_saved			=$row['cons_saved'];
							$cost_saved			=$row['cost_saved'];
							if(!$graphic_w_saved)
								$graphic_w_saved=$arr_header['print_width'];
							if(!$graphic_h_saved)
								$graphic_h_saved=$arr_header['print_height'];
							if(!$price_saved || $type==2)
								$price_saved=$row['price'];
							if(!$cons_saved || $type==2)
								$cons_saved=$row['conpc'];
							if(!$cost_saved || $type==2)
								$cost_saved=$row['conpc']*$row['price'];
						
						 ?>
                         
              <tr class="dataRow">
                <td height="21" bgcolor="#FFFFFF" id="<?php echo $color_id; ?>" class="color_id" ><?php echo $color; ?></td>
                <td height="22" bgcolor="#FFFFFF" class="technique" id="<?php echo $technique_id; ?>"><?php echo $technique; ?></td>
                <td height="22" bgcolor="#FFFFFF" class="item" id="<?php echo $item_id; ?>"><?php echo $item; ?></td>
                <td bgcolor="#FFFFFF" class="normalfnt"><input type="text" name="textfield15" id="textfield15" style="width:50px; text-align:right" value="<?php echo $graphic_w_saved; ?>" class="printWidth_nonRoll" /></td>
                <td bgcolor="#FFFFFF" class="normalfnt"><input type="text" name="textfield16" id="textfield16" style="width:50px; text-align:right" value="<?php echo $graphic_h_saved; ?>" class="printHeight_nonRoll" /></td>
                <td bgcolor="#FFFFFF" class="pcs normalfntRight"><input type="text" name="textitem_h2" id="textitem_h2" style="width:100px; text-align:right" value="<?php echo $price_saved; ?>" class=" cal_cost price_nonRoll" /></td>
                <td bgcolor="#FFFFFF" class="pcs normalfntRight"><input type="text" name="textitem_h3" id="textitem_h3" style="width:100px; text-align:right" value="<?php echo $cons_saved; ?>" class="printWidth  cal_cost consump_nonRoll" /></td>
                <td bgcolor="#FFFFFF" class="meters normalfntRight"><?php echo $uom; ?></td>
                <td bgcolor="#FFFFFF" class="cost_nonRoll normalfntRight"><?php echo $cost_saved; ?></td>
                <td bgcolor="#FFFFFF" class="cost normalfntRight"></td>
              </tr>
              <?php
			$tot_sp_rm_prie+=$cost_saved;
				}
				?>
              </table></td>
            </tr>
            <tr height="15"><td colspan="6"></td></tr>
          <tr>
            <td class="normalfnt" colspan="6" id="ndir_td" <?php if($coppied==1){ ?>style="display:none"<?php } ?>><table width="1263" cellpadding="0" cellspacing="0" class="bordered" id="table_non_direct">
              <tr class="">
                <th height="17" colspan="12" class="normalfntMid"><strong> Non-Direct RM Cost</strong></th>
              </tr>
              <tr>
                <th width="210" height="19">Item</th>
                <th width="100">Grapic W</th>
                <th width="112">Grapic H</th>
                <th width="101">Price ($)</th>
                <th width="107">Consumption</th>
                <th width="65">UOM</th>
                <th width="75">Cost ($)</th>
                <th width="491">&nbsp;</th>
              </tr>
              <?php 
			  			if($type==1 and $approved!=1)
			  			$result_nrf	=	load_non_direct_rm($x_graphicNo_c,$x_sampleNo_c,$x_sampleYear_c,$x_revNo_c,$x_combo_c,$x_print_c);
			else 			
			  			$result_nrf	=	load_non_direct_rm($x_graphicNo_o,$x_sampleNo_o,$x_sampleYear_o,$x_revNo_o,$x_combo_o,$x_print_o);
 						while($row=mysqli_fetch_array($result_nrf))
						{
							$item			=$row['item'];
							$item_id		=$row['item_id'];
							$uom			=$row['uom'];
							$conpc			=$row['conpc'];
							$price			=$row['price'];
							$graphic_w_saved	=$row['print_width'];
							$graphic_h_saved	=$row['print_height'];
							$price_saved		=$row['price_saved'];
							$cons_saved			=$row['cons_saved'];
							$cost_saved			=$row['cost_saved'];
							if(!$graphic_w_saved || $type==2)
								$graphic_w_saved=$arr_header['print_width'];
							if(!$graphic_h_saved || $type==2)
								$graphic_h_saved=$arr_header['print_height'];
							if(!$price_saved || $type==2)
								$price_saved=$row['price'];
							if(!$cons_saved || $type==2)
								$cons_saved=$row['conpc'];
							if(!$cost_saved || $type==2)
								$cost_saved=$row['conpc']*$row['price'];

						 ?>
              <tr class="dataRow">
                <td height="21" bgcolor="#FFFFFF" class="item" id="<?php echo $item_id; ?>"><?php echo $item; ?></td>
                <td bgcolor="#FFFFFF" class="normalfnt"><input type="text" name="textfield15" id="textfield15" style="width:50px; text-align:right" value="<?php echo $graphic_w_saved; ?>" class="printWidth_nonDirect" /></td>
                <td bgcolor="#FFFFFF" class="normalfnt"><input type="text" name="textfield16" id="textfield16" style="width:50px; text-align:right" value="<?php echo $graphic_h_saved; ?>" class="printHeight_nonDirect" /></td>
                <td bgcolor="#FFFFFF" class="pcs normalfntRight"><input type="text" name="textitem_h2" id="textitem_h2" style="width:100px; text-align:right" value="<?php echo $price_saved ;?>" class="price_nonDirect cal_cost" /></td>
                <td bgcolor="#FFFFFF" class="pcs normalfntRight"><input type="text" name="textitem_h3" id="textitem_h3" style="width:100px; text-align:right" value="<?php echo $cons_saved; ?>" class="cons_nonDirect cal_cost" /></td>
                <td bgcolor="#FFFFFF" class="meters normalfntRight"><?php echo $uom; ?></td>
                <td bgcolor="#FFFFFF" class="cost_nonDirect normalfntRight"><?php echo $cost_saved; ?></td>
                <td bgcolor="#FFFFFF" class="cost normalfntRight"></td>
              </tr>
              <?php
				$tot_sp_rm_prie+=$cost_saved;
				}
				?>
              </table></td>
            </tr>
          <tr>
            <td colspan="4" class="normalfnt">&nbsp;</td>
            <td width="23%" colspan="2" valign="top" class="normalfnt">&nbsp;</td>
          </tr>
            <tr>
              <td align="left" colspan="6"></td></tr>
          <tr>
            <td colspan="2" class="normalfnt" align="left" valign="top"  id="process_td" <?php if($coppied==1){ ?>style="display:none"<?php } ?>><div id="divProcess"><table width="50%" border="0" align="center" class="bordered"  id="table_process_cost">
              <tr>
                <th colspan="5"  class="normalfntMid"><strong>Processes Cost</strong></th>
                </tr>
              <tr>
                <th >Select</th>
                <th >Process</th>
                <th nowrap="nowrap" >Order</th>
                <th nowrap="nowrap" >Price ($)</th>
                <th nowrap="nowrap" >Cost ($)</th>
                </tr>
              <?php
				       $sql = "	select 
									mst_costingprocesses.intId,
									mst_costingprocesses.strProcess as strProcess,
									mst_costingprocesses.dblCost,
									costing_sample_routing_process.PROCESS_ID AS process_saved,
									sp.`ORDER_BY` AS ORDER_by,
									costing_sample_routing_process.COST AS saved_cost
									
									FROM mst_costingprocesses 
									INNER JOIN trn_sampleinfomations_combo_print_details_processes as sp ON sp.PROCESS = mst_costingprocesses.intId
									and sp.SAMPLE_NO = '$x_sampleNo_o' 
									and sp.SAMPLE_YEAR='$x_sampleYear_o' and sp.REVISION='$x_revNo_o' and sp.COMBO='$x_combo_o' and sp.PRINT='$x_print_o'
									left JOIN costing_sample_routing_process ON sp.SAMPLE_NO = costing_sample_routing_process.SAMPLE_NO 
									AND sp.SAMPLE_YEAR = costing_sample_routing_process.SAMPLE_YEAR AND sp.REVISION = costing_sample_routing_process.REVISION 
									AND sp.COMBO = costing_sample_routing_process.COMBO AND sp.PRINT = costing_sample_routing_process.PRINT 
									AND sp.ROUTING = costing_sample_routing_process.ROUTING_ID AND mst_costingprocesses.intId = costing_sample_routing_process.PROCESS_ID
									WHERE mst_costingprocesses.intStatus = '1' 
";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
                    $id 			= $row['intId'];
                    $name 			= $row['strProcess'];
                    $cost 			= $row['dblCost'];
                    $process_saved 	= $row['process_saved'];
                    $cost_saved 	= $row['saved_cost'];
                    $order 			= $row['ORDER_by'];
                    if($process_saved>0){
                        $cost = $cost_saved;
                    }
			?>
              <tr>
                <td bgcolor="#FFFFFF" class="normalfnt" id="<?php echo $id ?>"><input id="chkDisp" class="chkProcess" type="checkbox" value="" <?php if($process_saved){?> checked="checked" <?php } ?>></td>
                <td bgcolor="#FFFFFF" class="normalfnt procss" id="<?php echo $id ?>"><?php echo $name ?></td>
                <td bgcolor="#FFFFFF" class="normalfnt order" id="<?php echo $order ?>"><?php echo $order ?></td>
                <td bgcolor="#FFFFFF" align="center"><input type="text" name="textfield12" id="textfield12" style="width:50px; text-align:right" value="<?php echo $cost ?>" class="cal_cost pocess_price" /></td>
                <td bgcolor="#FFFFFF" align="center" class="pocess_cost"><?php echo $cost;?></td>
                </tr>
              <?php
			}
			?>
              </table></div></td>
            <td width="9%"></td>
            <?php
		if($status=='' && $type==1){
			$arr_header = NULL;
			$arr_header			= get_header_saved($x_graphicNo_c,$x_sampleNo_c,$x_sampleYear_c,$x_revNo_c,$x_combo_c,$x_print_c,$prod_location,$routing);
		}
			?>
            <td colspan="2" class="normalfnt" align="left" valign="top"  id="shots_td" <?php if($coppied==1){ ?>style="display:none"<?php } ?>><table border="0" align="center" class="bordered"  id="MainGrid3" width="78%">
              <tr>
                <th colspan="5" class="normalfntMid"><strong>Shots Cost</strong></th>
                </tr>
              <tr>
                <th width="148" bgcolor="#FFFFFF" >No of Shots</th>
                <th width="185" bgcolor="#FFFFFF" >Cost Per Shot</th>
                <th width="117" bgcolor="#FFFFFF"nnowrap="nowrap" >Total Cost ($) </th>
                </tr>
              <?php
				//load saved details
				
				?>
              <?php
				
 				?>
              <tr>
                <td bgcolor="#FFFFFF"><input type="text" name="textfield2" id="textfield2" style="width:100px; text-align:right" value="<?php echo $arr_header['shots']; ?>"  class="shots" disabled="disabled" /></td>
                <td bgcolor="#FFFFFF"><input type="text" name="textfield13" id="textfield13" style="width:100px; text-align:right"  class="price_shots  cal_cost" value="<?php echo $arr_header['cost_per_s']; ?>"/></td>
                <td bgcolor="#FFFFFF" class="cost_shots1" style="width:100px" align="right"><input type="text" name="textfield13" id="textfield13" style="width:100px; text-align:right"  class="cost_shots  cal_cost" value="<?php echo ($arr_header_saved['ups'] !=0)?($arr_header['shots']*$arr_header['cost_per_s']/$arr_header_saved['ups']):0;  ?>" <?php if($arr_header['shots']>0){ ?>disabled="disabled" <?php } ?>/></tr>
              <?php
			  $tot_shots_cost  = ($arr_header_saved['ups'] !=0)?($arr_header['shots']*$arr_header['cost_per_s']/$arr_header_saved['ups']):0;

				?>
              </table></td>
          </tr>
          </table></td>
      </tr>
      <tr height="20"><td></td></tr>
    </table></td>
    </tr>
    <?php //print_r($arr_header); ?>
    <tr><td><table width="100%">
<tr>
            <td width="0%">&nbsp;</td>
            <td width="0%">&nbsp;</td>
            <td width="48%">&nbsp;</td>
            <td width="12%" class="normalfnt">Ink Cost</td>
            <td colspan="3" class=""><input type="text" name="textfield18" id="textfield18" style="width:140px; text-align:right" value="<?php echo $arr_footer['INK_COST'];?>" class="cls_tot_ink_cost"  disabled/>&nbsp;$</td>
            </tr>          <tr>
            <td>&nbsp;</td>
            <td width="0%">&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">Special RM Cost</td>
            <td colspan="3" class=""><input type="text" name="textfield19" id="textfield19" style="width:140px; text-align:right"  value="<?php echo $arr_footer['SPECIAL_RM_COST'];?>" class="cls_tot_sp_cost"  disabled/>&nbsp;$</td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">Process Cost</td>
            <td colspan="3" class=""><input type="text" name="textfield20" id="textfield20" style="width:140px; text-align:right" value="<?php echo $arr_footer['PROCESS_COST']; ?>" class="cls_tot_process_cost"  disabled/>&nbsp;$</td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">Shots Cost</td>
            <td colspan="3" class=""><input type="text" name="textfield21" id="textfield21" style="width:140px; text-align:right" value="<?php echo $arr_footer['SHOT_COST']; ?>" class="cls_tot_shot_cost"  disabled/>&nbsp;$</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt" rowspan="2">Other Cost</td>
            <td width="12%" class="" rowspan="2" nowrap="nowrap"><input type="text" name="textfield21" id="textfield21" style="width:140px; height:30px; text-align:right" class="cls_other_cost cal_cost" value="<?php echo $arr_footer['OTHER_COST'];?>"  />&nbsp;$</td>
            <td width="27%" class="" rowspan="2"><textarea name="textfield3" class="cls_other_cost_remarks" id="textfield3" rows="1" cols="30" title="Remarks for other cost"><?php echo $arr_footer['OTHER_COST_REMARKS']; ?></textarea></td>
            <td width="1%" class="" rowspan="2">&nbsp;</td>
            </tr>
          <tr>
             <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td bgcolor="#F4EDB9" class="normalfnt">Total Cost</td>
            <td colspan="3" class=""><input type="text" name="textfield22" id="textfield22" style="width:140px; text-align:right" class="cls_total_cost" value="<?php echo ($arr_footer['TOTAL_COST']);?>" disabled="disabled" />&nbsp;$</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">Margin %
              <input type="text" name="textfield17" id="textfield17" value="<?php echo $arr_footer['MARGIN_PERCENTAGE'] ?>" style="width:50px; text-align:right" class="cal_cost cls_margin" /></td>
            <td colspan="3" class=""><input type="text" name="textfield23" id="textfield23" style="width:140px; text-align:right" class="cls_tot_cost_with_margin" value="<?php echo ($arr_footer['MARGIN']);?>" disabled="disabled" />&nbsp;$</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td bgcolor="#FAD163" class="normalfnt">Screenline    Price</td>
            <td colspan="3" class=""><input type="text" name="textfield24" id="textfield24" style="width:140px; text-align:right" class="cls_screenline_price" value="<?php echo ($arr_footer['SCREENLINE_PRICE']);?>" disabled="disabled" /&nbsp;$></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">Target Price</td>
            <td colspan="" class=""><input type="text" name="textfield25" id="textfield25" style="width:140px; text-align:right" class="cls_target_price" value="<?php echo $arr_footer['TARGET_PRICE'];?>" />&nbsp;$</td>
              <td align="left align" bgcolor="#FFFFFF"><input type="checkbox" name="enableQtyPrice" id="enableQtyPrice"
                                                          value="-1" width="15" height="15" <?php echo ($qtyPricesEnable==1) ? "checked": "unchecked" ?>>Enable Qty Prices </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td bgcolor="#F7D2BB" class="normalfnt" id="approvedPriceColumn">Approved Price</td>
            <td  class=""><input type="text" <?php echo ($qtyPricesEnable==1) ? "disabled": ""  ?> name="textfield26" id="textfield26" style="width:140px; text-align:right" class="cls_approved_price" value="<?php echo $arr_footer['APPROVED_PRICE'];?>" />&nbsp;$</td>
            <td align="left align" id="addQtyPriceBtn" name="addQtyPriceBtn" bgcolor="#FFFFFF" <?php echo ($qtyPricesEnable!=1) ? "hidden": ""  ?>><img
                          src="images/add_new.png" width="15" height="15"
                          class="clsQtyPrice mouseover"/> Add Qty Prices </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td colspan="3" class="">&nbsp;</td>
          </tr>
    </table></td></tr>
      <tr>
        <td align="center" class="" nowrap="nowrap">
        <a href="?q=116" id="butNew" class="button green large" style="" name="butNew" on> New </a>&nbsp;&nbsp;&nbsp;
        
        <?php
		$sql1 = "
				SELECT
				costing_sample_header.`STATUS`
				FROM
				costing_sample_header
				WHERE
				costing_sample_header.SAMPLE_NO = '$x_sampleNo_o' AND
				costing_sample_header.SAMPLE_YEAR = '$x_sampleYear_o' AND
				costing_sample_header.REVISION = '$x_revNo_o' AND
				costing_sample_header.COMBO = '$x_combo_o' AND
				costing_sample_header.PRINT = '$x_print_o'   
				";
		$result = $db->RunQuery($sql1);
		$row = mysqli_fetch_array($result);
		//echo $row['STATUS'];
		if($row['STATUS'] == 2 || $row['STATUS'] == 0 || $row['STATUS']==''){?>
        <a id="butSave" class="button green large" style="" name="butSave" on> Save </a>&nbsp;&nbsp;&nbsp;
        <a id="butApprove" class="button green large" style="" name="butApprove" on> Approve </a>&nbsp;&nbsp;&nbsp;
        <?php } ?>
        <a id="butReport" class="button green large" style="" name="butReport" on> Report </a>&nbsp;&nbsp;&nbsp;
        <?php
		if($row['STATUS'] == 1){?>
        <a id="butCoppyTo" class="button green large" style="" name="butCoppyTo" on="on"> Copy To</a>&nbsp;&nbsp;
         <?php }?>
        <a id="butClose" href="main.php" class="button green large" style="" name="butClose" on> Close </a></td>
      </tr>
  </table>

  </div>
  </div>
</form>
<div id="coppyStatus">
<input id="copy" type="hidden" value="<?php echo $coppied; ?>"/><input id="copyOld" type="hidden" value="<?php echo $coppiedOld; ?>"/>
</div>
<div style="width:900px; position:absolute; display:none;z-index:100;"  id="popupContact1"></div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
<?php
function get_header($x_graphicNo,$x_sampleNo,$x_sampleYear,$x_revNo,$x_combo,$x_print,$prod_location,$routing){
global $db;

			$sql = "SELECT DISTINCT
						trn_sampleinfomations_printsize.intWidth,
						trn_sampleinfomations_printsize.intHeight,
						trn_sampleinfomations.strStyleNo,
						trn_sampleinfomations.strGraphicRefNo,
						mst_part.strName
					FROM
						trn_sampleinfomations_printsize
						Inner Join mst_part ON mst_part.intId = trn_sampleinfomations_printsize.intPart
INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_printsize.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_printsize.intSampleYear AND trn_sampleinfomations_printsize.intRevisionNo = trn_sampleinfomations.intRevisionNo
					WHERE
						trn_sampleinfomations_printsize.strPrintName  =  '$x_print' AND
						trn_sampleinfomations_printsize.intSampleNo   =  '$x_sampleNo' AND
						trn_sampleinfomations_printsize.intSampleYear =  '$x_sampleYear' AND
						trn_sampleinfomations_printsize.intRevisionNo =  '$x_revNo'
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					$resp['graphic'] 		= $row['strGraphicRefNo'];
					$resp['styleNo'] 		= $row['strStyleNo'];
					$resp['print_width']  	= $row['intWidth'];
					$resp['print_height']  	= $row['intHeight'];
					$resp['partName'] 		= $row['strName'];
				}
				
		$sql = "SELECT DISTINCT
				trn_sampleinfomations_details.strComboName,
				trn_sampleinfomations_details.intPrintMode,
				mst_printmode.strName as printMode, 
				trn_sampleinfomations_details.intWashStanderd,
				mst_washstanderd.strName as washStandard, 
				COUNT(DISTINCT trn_sampleinfomations_details.intColorId) AS noOfColors,
				trn_sampleinfomations_details.intFabricType,
				mst_fabrictype.strName as fabric_type_desc 
				FROM trn_sampleinfomations_details 
				inner join mst_washstanderd on mst_washstanderd.intId=trn_sampleinfomations_details.intWashStanderd  
				inner join mst_printmode on mst_printmode.intId=trn_sampleinfomations_details.intPrintMode   
				INNER JOIN mst_fabrictype ON trn_sampleinfomations_details.intFabricType = mst_fabrictype.intId
				WHERE
				trn_sampleinfomations_details.intSampleNo 	=  '$x_sampleNo' AND
				trn_sampleinfomations_details.intSampleYear =  '$x_sampleYear' AND 
				trn_sampleinfomations_details.strPrintName =  '$x_print' AND 
				trn_sampleinfomations_details.strComboName 		=  '$x_combo' AND
				trn_sampleinfomations_details.intRevNo 		=  '$x_revNo' 
				";
		//echo $sql;
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		
		$resp['printMode']		= $row['printMode'];
		$resp['washCondition'] 	= $row['washStandard'];
		$resp['fab_type'] 		= $row['fabric_type_desc'];
		
		$sql = "SELECT COUNT(TB.intColorId) AS noOfColors FROM (SELECT  
 				trn_sampleinfomations_details_technical.intColorId 
				FROM trn_sampleinfomations_details_technical 
				WHERE
				trn_sampleinfomations_details_technical.intSampleNo 	=  '$x_sampleNo' AND
				trn_sampleinfomations_details_technical.intSampleYear =  '$x_sampleYear' AND 
				trn_sampleinfomations_details_technical.strPrintName =  '$x_print' AND 
				trn_sampleinfomations_details_technical.strComboName 		=  '$x_combo' AND
				trn_sampleinfomations_details_technical.intRevNo 		=  '$x_revNo' GROUP BY trn_sampleinfomations_details_technical.intColorId ) AS TB
				"; 
		//Altered on 2017-03-24 to get noOfColors from trn_sampleinfomations_details table
		/*$sql = "SELECT COUNT(TB.intColorId) AS noOfColors FROM (SELECT  
 				trn_sampleinfomations_details.intColorId 
				FROM trn_sampleinfomations_details 
				WHERE
				trn_sampleinfomations_details.intSampleNo 	=  '$x_sampleNo' AND
				trn_sampleinfomations_details.intSampleYear =  '$x_sampleYear' AND 
				trn_sampleinfomations_details.strPrintName =  '$x_print' AND 
				trn_sampleinfomations_details.strComboName 		=  '$x_combo' AND
				trn_sampleinfomations_details.intRevNo 		=  '$x_revNo' GROUP BY trn_sampleinfomations_details.intColorId ) AS TB
				";*/
		$result = $db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		$resp['noOfColors'] 	= $row['noOfColors'];
	
		$sql = "select sum(shots) shots from (SELECT trn_sampleinfomations_details_technical.intNoOfShots  as shots 
				FROM `trn_sampleinfomations_details_technical`
				INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details_technical.intSampleNo = trn_sampleinfomations_details.intSampleNo
            AND trn_sampleinfomations_details_technical.intSampleYear = trn_sampleinfomations_details.intSampleYear
            AND trn_sampleinfomations_details_technical.intRevNo = trn_sampleinfomations_details.intRevNo
            AND trn_sampleinfomations_details_technical.strPrintName = trn_sampleinfomations_details.strPrintName
            AND trn_sampleinfomations_details_technical.strComboName = trn_sampleinfomations_details.strComboName
            AND trn_sampleinfomations_details_technical.intColorId = trn_sampleinfomations_details.intColorId
				WHERE
				trn_sampleinfomations_details_technical.intSampleNo 	=  '$x_sampleNo' AND
				trn_sampleinfomations_details_technical.intSampleYear =  '$x_sampleYear' AND 
				trn_sampleinfomations_details_technical.strPrintName =  '$x_print' AND 
				trn_sampleinfomations_details_technical.strComboName 		=  '$x_combo' AND
				trn_sampleinfomations_details_technical.intRevNo 		=  '$x_revNo' 
		  group by trn_sampleinfomations_details_technical.intSampleNo 	,
				trn_sampleinfomations_details_technical.intSampleYear ,
				trn_sampleinfomations_details_technical.strPrintName ,
				trn_sampleinfomations_details_technical.strComboName,
				trn_sampleinfomations_details_technical.intRevNo ,
trn_sampleinfomations_details_technical.intColorId,
trn_sampleinfomations_details_technical.intInkTypeId ) as tb1";
		$result = $db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		$resp['shots'] 	= ($row['shots'] != null)?$row['shots']:0;
	
		$sql = "SELECT
				mst_costingstrocktype.COST
				FROM `mst_costingstrocktype`
				WHERE
				mst_costingstrocktype.LOCATION = '$prod_location'
				";
		$result = $db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		$resp['shots_cost'] 	= $row['COST'];
	
		$sql = "SELECT
				trn_sampleinfomations_combo_print_routing.SAMPLE_NO,
				trn_sampleinfomations_combo_print_routing.SAMPLE_YEAR,
				trn_sampleinfomations_combo_print_routing.REVISION,
				trn_sampleinfomations_combo_print_routing.COMBO,
				trn_sampleinfomations_combo_print_routing.PRINT,
				(trn_sampleinfomations_combo_print_routing.NO_OF_UPS) as NO_OF_UPS,
				trn_sampleinfomations_combo_print_routing.ROUTING 
				FROM `trn_sampleinfomations_combo_print_routing`
				WHERE
				trn_sampleinfomations_combo_print_routing.SAMPLE_NO = '$x_sampleNo' AND
				trn_sampleinfomations_combo_print_routing.SAMPLE_YEAR = '$x_sampleYear' AND
				trn_sampleinfomations_combo_print_routing.REVISION = '$x_revNo' AND
				trn_sampleinfomations_combo_print_routing.COMBO = '$x_combo' AND
				trn_sampleinfomations_combo_print_routing.PRINT = '$x_print' /*AND
				trn_sampleinfomations_combo_print_routing.ROUTING = '$routing' */
				";
		$result = $db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		$resp['ups'] 		= ($row['NO_OF_UPS']!= null)?$row['NO_OF_UPS']:0;
		$resp['ROUTING'] 	= $row['ROUTING'];
	
		return $resp;
		
	
}

function get_header_saved($x_graphicNo,$x_sampleNo,$x_sampleYear,$x_revNo,$x_combo,$x_print,$prod_location,$routing){

global $db;

			$sql = "SELECT
					trn_sampleinfomations.strGraphicRefNo,
					trn_sampleinfomations.strStyleNo,
					costing_sample_header.SAMPLE_NO,
					costing_sample_header.SAMPLE_YEAR,
					costing_sample_header.REVISION,
					costing_sample_header.COMBO,
					costing_sample_header.PRINT,
					costing_sample_header.COPPIED_FROM_SAMPLE_NO,
					costing_sample_header.COPPIED_FROM_SAMPLE_YEAR,
					costing_sample_header.COPPIED_FROM_REVISION,
					costing_sample_header.COPPIED_FROM_COMBO,
					costing_sample_header.COPPIED_FROM_PRINT,
					costing_sample_header.PRODUCTION_LOCATION,
					costing_sample_header.UPS,
					costing_sample_header.ESTIMATED_ORDER_QTY,
					costing_sample_header.NO_OF_COLOURS,
					costing_sample_header.SHOTS,
					costing_sample_header.COST_PER_SHOT,
					costing_sample_header.INK_COST,
					costing_sample_header.SPECIAL_RM_COST,
					costing_sample_header.PROCESS_COST,
					costing_sample_header.SHOT_COST,
					costing_sample_header.OTHER_COST,
					costing_sample_header.OTHER_COST_REMARKS,
					costing_sample_header.TOTAL_COST,
					costing_sample_header.MARGIN_PERCENTAGE,
					costing_sample_header.MARGIN,
					costing_sample_header.SCREENLINE_PRICE,
					costing_sample_header.TARGET_PRICE,
					costing_sample_header.APPROVED_PRICE,
					costing_sample_header.COPPIED_FLAG,
					costing_sample_header.COPPIED_TYPE,
					costing_sample_header.COPPIED_FROM_SAMPLE_NO,
					costing_sample_header.COPPIED_FROM_SAMPLE_YEAR,
					costing_sample_header.COPPIED_FROM_REVISION,
					costing_sample_header.COPPIED_FROM_COMBO,
					costing_sample_header.COPPIED_FROM_PRINT,
					costing_sample_header.`STATUS`,
					costing_sample_header.APPROVE_LEVELS,
					costing_sample_header.SAVED_BY,
					costing_sample_header.SAVED_TIME,
					costing_sample_header.QTY_PRICES,
					trn_sampleinfomations_combo_print_routing.ROUTING,
					trn_sampleinfomations_printsize.intWidth,
					trn_sampleinfomations_printsize.intHeight,
					mst_part.strName
					FROM
					costing_sample_header
					INNER JOIN trn_sampleinfomations ON costing_sample_header.SAMPLE_NO = trn_sampleinfomations.intSampleNo AND costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations.intSampleYear AND costing_sample_header.REVISION = trn_sampleinfomations.intRevisionNo
					left JOIN trn_sampleinfomations_combo_print_routing ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_combo_print_routing.SAMPLE_NO AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_combo_print_routing.SAMPLE_YEAR AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_combo_print_routing.REVISION AND costing_sample_header.COMBO = trn_sampleinfomations_combo_print_routing.COMBO AND costing_sample_header.PRINT = trn_sampleinfomations_combo_print_routing.PRINT
					left Join trn_sampleinfomations_printsize ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_printsize.intSampleNo 
AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_printsize.intSampleYear   
AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_printsize.intRevisionNo 
AND trn_sampleinfomations_combo_print_routing.PRINT = trn_sampleinfomations_printsize.strPrintName 
					left Join mst_part ON mst_part.intId = trn_sampleinfomations_printsize.intPart
					left JOIN trn_sampleinfomations_prices ON costing_sample_header.SAMPLE_NO = trn_sampleinfomations_prices.intSampleNo AND costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations_prices.intSampleYear AND costing_sample_header.REVISION = trn_sampleinfomations_prices.intRevisionNo AND costing_sample_header.COMBO = trn_sampleinfomations_prices.strCombo AND costing_sample_header.PRINT = trn_sampleinfomations_prices.strPrintName
					WHERE
					costing_sample_header.SAMPLE_NO = '$x_sampleNo' AND
					costing_sample_header.SAMPLE_YEAR = '$x_sampleYear' AND
					costing_sample_header.REVISION = '$x_revNo' AND
					costing_sample_header.COMBO = '$x_combo' AND
					costing_sample_header.PRINT = '$x_print'

				";

				$result = $db->RunQuery($sql);
				$row=mysqli_fetch_array($result);
				$resp['STATUS'] 		= $row['STATUS'];
				$resp['graphic'] 		= $row['strGraphicRefNo'];
				$resp['print_width']  	= $row['intWidth'];
				$resp['print_height']  	= $row['intHeight'];
				$resp['partName'] 		= $row['strName'];
				$resp['noOfColors'] 	= $row['NO_OF_COLOURS'];
				$resp['shots'] 			= $row['SHOTS'];
				$resp['shots_cost'] 	= $row['SHOT_COST'];
				$resp['prod_location'] 	= $row['PRODUCTION_LOCATION'];
				$resp['cost_per_s'] 	= ($row['COST_PER_SHOT']!=null)?$row['COST_PER_SHOT']:0;
				$resp['shot_cost'] 		= $row['SHOT_COST'];
				$resp['ups'] 			= ($row['UPS'] != null)? $row['UPS']:0;
				$resp['ROUTING'] 		= $row['ROUTING'];
				$resp['INK_COST'] 		= ($row['INK_COST'] != null)?$row['INK_COST']:0;
				$resp['SPECIAL_RM_COST']= ($row['SPECIAL_RM_COST']!= null)?$row['SPECIAL_RM_COST']:0;
				$resp['PROCESS_COST'] 	= ($row['PROCESS_COST'] != null)?$row['PROCESS_COST']:0;
				$resp['SHOT_COST'] 		= ($row['SHOT_COST'] != null)?$row['SHOT_COST']:0;
				$resp['OTHER_COST'] 	= $row['OTHER_COST'];
				$resp['OTHER_COST_REMARKS'] = $row['OTHER_COST_REMARKS'];
				$resp['TOTAL_COST'] 		= ($row['TOTAL_COST']!=null)?$row['TOTAL_COST']:0;
				$resp['MARGIN_PERCENTAGE'] 	= $row['MARGIN_PERCENTAGE'];
				$resp['MARGIN'] 			= ($row['MARGIN']!=null)?$row['MARGIN']:0;
				$resp['SCREENLINE_PRICE'] 	= ($row['SCREENLINE_PRICE'] != null)?$row['SCREENLINE_PRICE']:0;
				$resp['TARGET_PRICE'] 		= $row['TARGET_PRICE'];
				$resp['APPROVED_PRICE'] 	= $row['APPROVED_PRICE'];
                $resp['QTY_PRICES'] 	= $row['QTY_PRICES'];
				$resp['COPPIED_TYPE'] 		= $row['COPPIED_TYPE'];
			
				$resp['COPPIED_FROM_SAMPLE_NO'] = $row['COPPIED_FROM_SAMPLE_NO'];
				$resp['COPPIED_FROM_SAMPLE_YEAR']= $row['COPPIED_FROM_SAMPLE_YEAR'];
				$resp['COPPIED_FROM_REVISION'] 	= $row['COPPIED_FROM_REVISION'];
				$resp['COPPIED_FROM_COMBO'] 	= $row['COPPIED_FROM_COMBO'];
				$resp['COPPIED_FROM_PRINT'] 	= $row['COPPIED_FROM_PRINT'];
				$resp['ESTIMATED_ORDER_QTY'] 	= $row['ESTIMATED_ORDER_QTY'];
			
				if($row['SAMPLE_NO'] > 0)
				$resp['SAVED'] 	= 1;
				
				
				
				
				
	
		$sql = "SELECT DISTINCT
				trn_sampleinfomations_details.strComboName,
				trn_sampleinfomations_details.intPrintMode,
				mst_printmode.strName as printMode, 
				trn_sampleinfomations_details.intWashStanderd,
				mst_washstanderd.strName as washStandard, 
				COUNT(DISTINCT trn_sampleinfomations_details.intColorId) AS noOfColors,
				trn_sampleinfomations_details.intFabricType,
				mst_fabrictype.strName as fabric_type_desc 
				FROM trn_sampleinfomations_details 
				inner join mst_washstanderd on mst_washstanderd.intId=trn_sampleinfomations_details.intWashStanderd  
				inner join mst_printmode on mst_printmode.intId=trn_sampleinfomations_details.intPrintMode   
				INNER JOIN mst_fabrictype ON trn_sampleinfomations_details.intFabricType = mst_fabrictype.intId
				WHERE
				trn_sampleinfomations_details.intSampleNo 	=  '$x_sampleNo' AND
				trn_sampleinfomations_details.intSampleYear =  '$x_sampleYear' AND 
				trn_sampleinfomations_details.strPrintName =  '$x_print' AND 
				trn_sampleinfomations_details.strComboName 		=  '$x_combo' AND
				trn_sampleinfomations_details.intRevNo 		=  '$x_revNo' 
				";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		
		$resp['printMode']		= $row['printMode'];
		$resp['washCondition'] 	= $row['washStandard'];
		$resp['fab_type'] 		= $row['fabric_type_desc'];
	
	
		return $resp;
		
	
}

function get_header_saved_old_costing($x_graphicNo,$x_sampleNo,$x_sampleYear,$x_revNo,$x_combo,$x_print,$prod_location,$routing){

global $db;

			$sql = "SELECT
					trn_sampleinfomations.strGraphicRefNo,
					trn_sampleinfomations.strStyleNo,
					0 AS SAMPLE_NO,
					0 AS SAMPLE_YEAR,
					0 AS REVISION,
					0 AS COMBO,
					0 AS PRINT,
					0 AS COPPIED_FROM_SAMPLE_NO,
					0 AS COPPIED_FROM_SAMPLE_YEAR,
					0 AS COPPIED_FROM_REVISION,
					0 AS COPPIED_FROM_COMBO,
					0 AS COPPIED_FROM_PRINT,
					0 AS PRODUCTION_LOCATION,
					0 AS UPS,
					0 AS ESTIMATED_ORDER_QTY,
					0 AS NO_OF_COLOURS,
					0 AS SHOTS,
					0 AS COST_PER_SHOT,
					0 AS INK_COST,
					0 AS SPECIAL_RM_COST,
					0 AS PROCESS_COST,
					0 AS SHOT_COST,
					0 AS OTHER_COST,
					0 AS OTHER_COST_REMARKS,
					0 AS TOTAL_COST,
					0 AS MARGIN_PERCENTAGE,
					0 AS MARGIN,
					0 AS SCREENLINE_PRICE,
					0 AS TARGET_PRICE,
					trn_sampleinfomations_prices.dblPrice AS APPROVED_PRICE,
					0 AS COPPIED_FLAG,
					0 AS COPPIED_FROM_SAMPLE_NO,
					0 AS COPPIED_FROM_SAMPLE_YEAR,
					0 AS COPPIED_FROM_REVISION,
					0 AS COPPIED_FROM_COMBO,
					0 AS COPPIED_FROM_PRINT,
					0 AS `STATUS`,
					0 AS APPROVE_LEVELS,
					0 AS SAVED_BY,
					0 AS SAVED_TIME,
					trn_sampleinfomations_combo_print_routing.ROUTING,
					trn_sampleinfomations_printsize.intWidth,
					trn_sampleinfomations_printsize.intHeight,
					mst_part.strName
					FROM trn_sampleinfomations
					INNER JOIN trn_sampleinfomations_prices 
					ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_prices.intSampleNo 
					AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_prices.intSampleYear 
					AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_prices.intRevisionNo
					left JOIN costing_sample_header ON costing_sample_header.SAMPLE_NO = trn_sampleinfomations.intSampleNo AND costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations.intSampleYear AND costing_sample_header.REVISION = trn_sampleinfomations.intRevisionNo
					left JOIN trn_sampleinfomations_combo_print_routing ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_combo_print_routing.SAMPLE_NO AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_combo_print_routing.SAMPLE_YEAR AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_combo_print_routing.REVISION AND costing_sample_header.COMBO = trn_sampleinfomations_combo_print_routing.COMBO AND costing_sample_header.PRINT = trn_sampleinfomations_combo_print_routing.PRINT
					left Join trn_sampleinfomations_printsize ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_printsize.intSampleNo 
AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_printsize.intSampleYear   
AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_printsize.intRevisionNo 
AND trn_sampleinfomations_combo_print_routing.PRINT = trn_sampleinfomations_printsize.strPrintName 
					left Join mst_part ON mst_part.intId = trn_sampleinfomations_printsize.intPart
 					WHERE
					trn_sampleinfomations_prices.intSampleNo = '$x_sampleNo' AND
					trn_sampleinfomations_prices.intSampleYear = '$x_sampleYear' AND
					trn_sampleinfomations_prices.intRevisionNo = '$x_revNo' AND
					trn_sampleinfomations_prices.strCombo = '$x_combo' AND
					trn_sampleinfomations_prices.strPrintName = '$x_print'

				";
				$result = $db->RunQuery($sql);
				$row=mysqli_fetch_array($result);
				$resp['STATUS'] 		= $row['STATUS'];
				$resp['graphic'] 		= $row['strGraphicRefNo'];
				$resp['print_width']  	= $row['intWidth'];
				$resp['print_height']  	= $row['intHeight'];
				$resp['partName'] 		= $row['strName'];
				$resp['noOfColors'] 	= $row['NO_OF_COLOURS'];
				$resp['shots'] 			= $row['SHOTS'];
				$resp['shots_cost'] 	= $row['SHOT_COST'];
				$resp['prod_location'] 	= $row['PRODUCTION_LOCATION'];
				$resp['cost_per_s'] 	= $row['COST_PER_SHOT'];
				$resp['shot_cost'] 		= $row['SHOT_COST'];
				$resp['ups'] 			= $row['UPS'];
				$resp['ROUTING'] 		= $row['ROUTING'];
				$resp['INK_COST'] 		= $row['INK_COST'];
				$resp['SPECIAL_RM_COST']= $row['SPECIAL_RM_COST'];
				$resp['PROCESS_COST'] 	= $row['PROCESS_COST'];
				$resp['SHOT_COST'] 		= $row['SHOT_COST'];
				$resp['OTHER_COST'] 	= $row['OTHER_COST'];
				$resp['OTHER_COST_REMARKS'] = $row['OTHER_COST_REMARKS'];
				$resp['TOTAL_COST'] 		= $row['TOTAL_COST'];
				$resp['MARGIN_PERCENTAGE'] 	= $row['MARGIN_PERCENTAGE'];
				$resp['MARGIN'] 			= $row['MARGIN'];
				$resp['SCREENLINE_PRICE'] 	= $row['SCREENLINE_PRICE'];
				$resp['TARGET_PRICE'] 		= $row['TARGET_PRICE'];
				$resp['APPROVED_PRICE'] 	= $row['APPROVED_PRICE'];
			
				$resp['COPPIED_FROM_SAMPLE_NO'] = $row['COPPIED_FROM_SAMPLE_NO'];
				$resp['COPPIED_FROM_SAMPLE_YEAR']= $row['COPPIED_FROM_SAMPLE_YEAR'];
				$resp['COPPIED_FROM_REVISION'] 	= $row['COPPIED_FROM_REVISION'];
				$resp['COPPIED_FROM_COMBO'] 	= $row['COPPIED_FROM_COMBO'];
				$resp['COPPIED_FROM_PRINT'] 	= $row['COPPIED_FROM_PRINT'];
				$resp['ESTIMATED_ORDER_QTY'] 	= $row['ESTIMATED_ORDER_QTY'];
			
				if($row['SAMPLE_NO'] > 0)
				$resp['SAVED'] 	= 1;
				
				
				
				
				
	
		$sql = "SELECT DISTINCT
				trn_sampleinfomations_details.strComboName,
				trn_sampleinfomations_details.intPrintMode,
				mst_printmode.strName as printMode, 
				trn_sampleinfomations_details.intWashStanderd,
				mst_washstanderd.strName as washStandard, 
				COUNT(DISTINCT trn_sampleinfomations_details.intColorId) AS noOfColors,
				trn_sampleinfomations_details.intFabricType,
				mst_fabrictype.strName as fabric_type_desc 
				FROM trn_sampleinfomations_details 
				inner join mst_washstanderd on mst_washstanderd.intId=trn_sampleinfomations_details.intWashStanderd  
				inner join mst_printmode on mst_printmode.intId=trn_sampleinfomations_details.intPrintMode   
				INNER JOIN mst_fabrictype ON trn_sampleinfomations_details.intFabricType = mst_fabrictype.intId
				WHERE
				trn_sampleinfomations_details.intSampleNo 	=  '$x_sampleNo' AND
				trn_sampleinfomations_details.intSampleYear =  '$x_sampleYear' AND 
				trn_sampleinfomations_details.strPrintName =  '$x_print' AND 
				trn_sampleinfomations_details.strComboName 		=  '$x_combo' AND
				trn_sampleinfomations_details.intRevNo 		=  '$x_revNo' 
				";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		
		$resp['printMode']		= $row['printMode'];
		$resp['washCondition'] 	= $row['washStandard'];
		$resp['fab_type'] 		= $row['fabric_type_desc'];
	
	
		return $resp;
		
	
}


function get_details_array($x_graphicNo,$x_sampleNo,$x_sampleYear,$x_revNo,$x_combo,$x_print){
	global $db;
	
	 $sql = "select 
			tb1.intColorId,
			tb1.color,
			tb1.intTechniqueId,
			tb1.technique,
			tb1.intInkTypeId,
			tb1.inkType,
			tb1.EDITABLE_INK_COST,
			tb1.CATEGORY,
			tb1.shots,
			tb1.intCurrencyId,
			tb1.currency,
			tb1.dblCostPerInch,
			tb1.dblColorWeight,
			tb1.intItem,
			tb1.itemName , 
			tb1.intStatus,
			cost_samplecostsheet_details_inktypes.dblCost,
			cost_samplecostsheet_details_inktypes.dblQty, 
			cost_samplecostsheet_details_inktypes.dblUsage ,
			costing_sample_ink_type.SHOTS as shots_saved,
			costing_sample_ink_type.COST_PER_SQ_INCH,
			costing_sample_ink_type.SQUARE_INCHES,
			costing_sample_ink_type.`USAGE` as usage_saved,
			costing_sample_ink_type.TOTAL_COST_PRICE   
	from(SELECT
			trn_sampleinfomations_details.intColorId,
			mst_colors.strName AS color,
			trn_sampleinfomations_details.intTechniqueId,
			mst_techniques.strName AS technique,
			trn_sampleinfomations_details_technical.intInkTypeId,
			mst_inktypes.strName AS inkType,
			mst_inktypes.INK_TYPE_CATEGORY as CATEGORY,
			mst_inktypes.EDITABLE_INK_COST,
			intNoOfShots as shots,
			mst_costinginktypecosting.intCurrencyId,
			mst_financecurrency.strCode AS currency,
			mst_costinginktypecosting.dblCostPerInch,
			trn_sampleinfomations_details_technical.dblColorWeight,
			trn_sampleinfomations_details_technical.intItem,
			mst_item.strName as itemName, 
			trn_sampleinfomations_details.intSampleNo, 
			trn_sampleinfomations_details.intSampleYear, 
			trn_sampleinfomations_details.strPrintName, 
			trn_sampleinfomations_details.strComboName, 
			trn_sampleinfomations_details.intRevNo,
			mst_inktypes.intStatus  
			FROM
			trn_sampleinfomations_details
			Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_details_technical.intSampleNo AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_details_technical.intRevNo AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_details_technical.strPrintName AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_details_technical.strComboName AND trn_sampleinfomations_details.intColorId = trn_sampleinfomations_details_technical.intColorId
			Inner Join mst_inktypes ON trn_sampleinfomations_details_technical.intInkTypeId = mst_inktypes.intId
			Inner Join mst_techniques ON trn_sampleinfomations_details.intTechniqueId = mst_techniques.intId
			Inner Join mst_colors ON trn_sampleinfomations_details.intColorId = mst_colors.intId
			LEFT Join mst_costinginktypecosting ON trn_sampleinfomations_details_technical.intInkTypeId = mst_costinginktypecosting.intInkTypeId
			left Join mst_financecurrency ON mst_costinginktypecosting.intCurrencyId = mst_financecurrency.intId
			left Join mst_item ON trn_sampleinfomations_details_technical.intItem = mst_item.intId 
			WHERE
			trn_sampleinfomations_details.intSampleNo 	=  '$x_sampleNo' AND
			trn_sampleinfomations_details.intSampleYear =  '$x_sampleYear' AND 
			trn_sampleinfomations_details.strPrintName  =  '$x_print' AND
			trn_sampleinfomations_details.strComboName 	=  '$x_combo' AND
			trn_sampleinfomations_details.intRevNo 		=  '$x_revNo' 
			GROUP BY  
			trn_sampleinfomations_details.intSampleNo,
			trn_sampleinfomations_details.intSampleYear,
			trn_sampleinfomations_details.strPrintName,
			trn_sampleinfomations_details.strComboName,
			trn_sampleinfomations_details.intRevNo ,
			trn_sampleinfomations_details.intColorId ,
			trn_sampleinfomations_details.intTechniqueId,
			trn_sampleinfomations_details_technical.intInkTypeId
			 ) as tb1 
			left join cost_samplecostsheet_details_inktypes 
			ON 
			tb1.intSampleNo 	=  cost_samplecostsheet_details_inktypes.intSampleNo AND
			tb1.intSampleYear =  cost_samplecostsheet_details_inktypes.intSampleYear AND 
			tb1.strPrintName =  cost_samplecostsheet_details_inktypes.strPrintName AND 
			tb1.strComboName 		=  cost_samplecostsheet_details_inktypes.strComboName AND
			tb1.intRevNo 		=  cost_samplecostsheet_details_inktypes.intRevNo 
left JOIN costing_sample_ink_type ON tb1.intSampleNo = costing_sample_ink_type.SAMPLE_NO 
AND tb1.intSampleYear = costing_sample_ink_type.SAMPLE_YEAR 
AND tb1.intRevNo = costing_sample_ink_type.REVISION 
AND tb1.strComboName = costing_sample_ink_type.COMBO 
AND tb1.strPrintName = costing_sample_ink_type.PRINT 
AND tb1.intColorId = costing_sample_ink_type.COLOUR 
AND tb1.intTechniqueId = costing_sample_ink_type.TECHNIQUE 
AND tb1.intInkTypeId  = costing_sample_ink_type.INK_TYPE  
group by 
tb1.intColorId,
tb1.intTechniqueId,
tb1.intInkTypeId 

			";
			//echo $sql;
		$result_tech = $db->RunQuery($sql);
		return $result_tech;
}


function load_roll_form_rm($x_graphicNo,$x_sampleNo,$x_sampleYear,$x_revNo,$x_combo,$x_print){
	
	global $db;
	global $company;
	global $date;
	$sql	="
				SELECT
				mst_techniques.strName AS technique,
				mst_item.strName AS item,
				mst_item.CONSUMPTION_AUTO AS auto_cons,
				mst_techniques.intId AS technique_id,
				mst_colors.strName as color,
				trn_sample_foil_consumption.intColorId as color_Id,
				mst_item.intId AS item_id,
				mst_item.foil_width,
				mst_item.foil_height,
				mst_units.strCode as uom  ,
				dblMeters as conpc  ,
				round((e1.dblBuying*mst_item.dblLastPrice/e2.dblBuying),6) as price ,
				costing_sample_rm_roll_form.GRAPHIC_W,
				costing_sample_rm_roll_form.GRAPHIC_H,
				costing_sample_rm_roll_form.ITEM_W,
				costing_sample_rm_roll_form.ITEM_H,
				costing_sample_rm_roll_form.ITEM_PRICE,
				costing_sample_rm_roll_form.CONSUMPTION,
				costing_sample_rm_roll_form.COST 
				FROM
				trn_sample_foil_consumption
				INNER JOIN mst_techniques ON trn_sample_foil_consumption.intTechniqueId = mst_techniques.intId
				INNER JOIN mst_item ON trn_sample_foil_consumption.intItem = mst_item.intId
				INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
				left JOIN mst_financeexchangerate as e1 ON mst_item.intCurrency = e1.intCurrencyId
				and 
				e1.dtmDate = '$date' AND
				e1.intCompanyId = '$company'  
				left JOIN mst_financeexchangerate as e2 ON e2.dtmDate = '$date' AND e2.intCompanyId = '$company' AND 			e2.intCurrencyId = 1
				left JOIN costing_sample_rm_roll_form ON trn_sample_foil_consumption.intSampleNo = costing_sample_rm_roll_form.SAMPLE_NO AND trn_sample_foil_consumption.intSampleYear = costing_sample_rm_roll_form.SAMPLE_YEAR AND trn_sample_foil_consumption.intRevisionNo = costing_sample_rm_roll_form.REVISION AND trn_sample_foil_consumption.strCombo = costing_sample_rm_roll_form.COMBO AND trn_sample_foil_consumption.strPrintName = costing_sample_rm_roll_form.PRINT AND trn_sample_foil_consumption.intTechniqueId = costing_sample_rm_roll_form.TECHNIQUE 
				AND trn_sample_foil_consumption.intColorId = costing_sample_rm_roll_form.COLOR 
				AND trn_sample_foil_consumption.intItem = costing_sample_rm_roll_form.ITEM
				INNER JOIN mst_colors ON trn_sample_foil_consumption.intColorId = mst_colors.intId
				WHERE
				trn_sample_foil_consumption.intSampleNo 	=  '$x_sampleNo' AND
				trn_sample_foil_consumption.intSampleYear 	=  '$x_sampleYear' AND 
				trn_sample_foil_consumption.strPrintName 	=  '$x_print' AND 
				trn_sample_foil_consumption.strCombo 		=  '$x_combo' AND
				trn_sample_foil_consumption.intRevisionNo 	=  '$x_revNo'
	";
	$result_rf = $db->RunQuery($sql);
	return $result_rf;
}

function load_non_roll_form_rm($x_graphicNo,$x_sampleNo,$x_sampleYear,$x_revNo,$x_combo,$x_print){
	
	global $db;
	global $company;
	global $date;

	$sql	="
				SELECT
				mst_techniques.strName AS technique,
				mst_item.strName AS item,
				trn_sample_spitem_consumption.intTechniqueId AS technique_id,
				mst_colors.strName as color,
				trn_sample_spitem_consumption.intColorId as color_Id,
				mst_item.intId AS item_id,
				mst_item.foil_width,
				mst_item.foil_height,
				mst_units.strCode as uom ,
				(trn_sample_spitem_consumption.dblQty/dblNoOfPcs) as conpc ,
				round((e1.dblBuying*mst_item.dblLastPrice/e2.dblBuying),6) as price ,
(e1.dblBuying*mst_item.dblLastPrice/e2.dblBuying) AS price,
costing_sample_rm_non_roll_form.GRAPHIC_W as graphic_w_saved,
costing_sample_rm_non_roll_form.GRAPHIC_H as graphic_h_saved,
costing_sample_rm_non_roll_form.ITEM_PRICE as price_saved,
costing_sample_rm_non_roll_form.CONSUMPTION as cons_saved,
costing_sample_rm_non_roll_form.COST as cost_saved 
				FROM
				trn_sample_spitem_consumption
				INNER JOIN mst_techniques ON trn_sample_spitem_consumption.intTechniqueId = mst_techniques.intId
				INNER JOIN mst_item ON trn_sample_spitem_consumption.intItem = mst_item.intId
				INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
				left JOIN mst_financeexchangerate as e1 ON mst_item.intCurrency = e1.intCurrencyId
				and 
				e1.dtmDate = '$date' AND
				e1.intCompanyId = '$company'  
				left JOIN mst_financeexchangerate as e2 ON e2.dtmDate = '$date' AND e2.intCompanyId = '$company' AND 			e2.intCurrencyId = 1
			left JOIN costing_sample_rm_non_roll_form ON trn_sample_spitem_consumption.intSampleNo = costing_sample_rm_non_roll_form.SAMPLE_NO AND trn_sample_spitem_consumption.intSampleYear = costing_sample_rm_non_roll_form.SAMPLE_YEAR AND trn_sample_spitem_consumption.intRevisionNo = costing_sample_rm_non_roll_form.REVISION AND trn_sample_spitem_consumption.strCombo = costing_sample_rm_non_roll_form.COMBO AND trn_sample_spitem_consumption.strPrintName = costing_sample_rm_non_roll_form.PRINT AND trn_sample_spitem_consumption.intTechniqueId = costing_sample_rm_non_roll_form.TECHNIQUE AND
			trn_sample_spitem_consumption.intColorId = costing_sample_rm_non_roll_form.COLOR AND
			trn_sample_spitem_consumption.intItem = costing_sample_rm_non_roll_form.ITEM
				INNER JOIN mst_colors ON trn_sample_spitem_consumption.intColorId = mst_colors.intId
				WHERE
				trn_sample_spitem_consumption.intSampleNo 	=  '$x_sampleNo' AND
				trn_sample_spitem_consumption.intSampleYear =  '$x_sampleYear' AND 
				trn_sample_spitem_consumption.strPrintName 	=  '$x_print' AND 
				trn_sample_spitem_consumption.strCombo 		=  '$x_combo' AND
				trn_sample_spitem_consumption.intRevisionNo =  '$x_revNo'
	";
	$result_rf = $db->RunQuery($sql);
	return $result_rf;
}
function load_non_direct_rm($x_graphicNo,$x_sampleNo,$x_sampleYear,$x_revNo,$x_combo,$x_print){
	
	global $db;
	global $company;
	global $date;
	
	  $sql	="
				SELECT 
				mst_item.intId as item_id,
				mst_item.strName AS item,
				mst_item.foil_width,
				mst_item.foil_height,
				mst_units.strCode as uom ,
				trn_sample_non_direct_rm_consumption.CONSUMPTION as conpc ,
				round((e1.dblBuying*mst_item.dblLastPrice/e2.dblBuying),6) as price ,
				costing_sample_rm_non_direct.GRAHIC_W as print_width,
				costing_sample_rm_non_direct.GRAPHIC_H as print_height,
				costing_sample_rm_non_direct.ITEM_PRICE as price_saved,
				costing_sample_rm_non_direct.CONSUMPTION as cons_saved,
				costing_sample_rm_non_direct.COST as cost_saved 
				FROM
				trn_sample_non_direct_rm_consumption
 				INNER JOIN mst_item ON trn_sample_non_direct_rm_consumption.ITEM = mst_item.intId
				INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
				left JOIN mst_financeexchangerate as e1 ON mst_item.intCurrency = e1.intCurrencyId
				and 
				e1.dtmDate = '$date' AND
				e1.intCompanyId = '$company'  
				left JOIN mst_financeexchangerate as e2 ON e2.dtmDate = '$date' AND e2.intCompanyId = '$company' AND 			e2.intCurrencyId = 1
				left JOIN costing_sample_rm_non_direct ON trn_sample_non_direct_rm_consumption.SAMPLE_NO = costing_sample_rm_non_direct.SAMPLE_NO AND trn_sample_non_direct_rm_consumption.SAMPLE_YEAR = costing_sample_rm_non_direct.SAMPLE_YEAR AND trn_sample_non_direct_rm_consumption.REVISION_NO = costing_sample_rm_non_direct.REVISION_NO AND trn_sample_non_direct_rm_consumption.ITEM = costing_sample_rm_non_direct.ITEM 
and trn_sample_non_direct_rm_consumption.SAMPLE_NO = '$x_sampleNo' AND
trn_sample_non_direct_rm_consumption.SAMPLE_YEAR = '$x_sampleYear' AND
trn_sample_non_direct_rm_consumption.REVISION_NO = '$x_revNo' AND
costing_sample_rm_non_direct.COMBO = '$x_combo' AND
costing_sample_rm_non_direct.PRINT = '$x_print'		
				WHERE
				trn_sample_non_direct_rm_consumption.SAMPLE_NO 	=  '$x_sampleNo' AND
				trn_sample_non_direct_rm_consumption.SAMPLE_YEAR =  '$x_sampleYear' AND 
				trn_sample_non_direct_rm_consumption.REVISION_NO =  '$x_revNo'
	";
	$result_rf = $db->RunQuery($sql);
	return $result_rf;
}
function geCostingSavedConpc($x_sampleNo,$x_sampleYear,$x_revNo,$x_combo,$x_print,$technique_id,$item_id){
		global $db;
		$sql	= "SELECT
					Sum(cost_samplecostsheet_print_sizes.intConsumption) as consumption 
					FROM `cost_samplecostsheet_print_sizes`
					WHERE
					cost_samplecostsheet_print_sizes.intSampleNo = '$x_sampleNo' AND
					cost_samplecostsheet_print_sizes.intSampleYear = '$x_sampleYear' AND
					cost_samplecostsheet_print_sizes.intRevisionNo = '$x_revNo' AND
					cost_samplecostsheet_print_sizes.strComboName = '$x_combo' AND
					cost_samplecostsheet_print_sizes.strPrintName = '$x_print' AND
					cost_samplecostsheet_print_sizes.TECHNIQUE = '$technique_id' AND
					cost_samplecostsheet_print_sizes.intItem = '$item_id'";		
		$result = $db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		return $row['consumption'];
}

function get_max_ups($sampleNo,$sampleYear,$revNo,$combo,$print){
	
		global $db;
	
		$sql	="SELECT 
		trn_sampleinfomations_combo_print_routing.NO_OF_UPS 
					FROM `trn_sampleinfomations_combo_print_routing`
							WHERE
							trn_sampleinfomations_combo_print_routing.SAMPLE_NO = '$sampleNo' AND
							trn_sampleinfomations_combo_print_routing.SAMPLE_YEAR = '$sampleYear' AND
							trn_sampleinfomations_combo_print_routing.REVISION = '$revNo' AND
							trn_sampleinfomations_combo_print_routing.COMBO = '$combo' AND
							trn_sampleinfomations_combo_print_routing.PRINT = '$print'";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		
		return $row['NO_OF_UPS'];
	
}

?>

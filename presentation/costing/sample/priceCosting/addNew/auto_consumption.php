 <?php
ini_set('display_errors',1);
//echo phpinfo();
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];
$company 	   = $_SESSION['headCompanyId'];

$intUser  = $_SESSION["userId"];

include $backwardseperator."dataAccess/Connector.php";
$sampleNo     = $_REQUEST['sampleNo'];
$sample_year  = $_REQUEST['sample_year'];
$revisionNo   = $_REQUEST['revisionNo'];
$comboName    = $_REQUEST['comboName'];
$PrintName    = $_REQUEST['PrintName']; //'16955 Back Top/print 1';//
$foilItem     = $_REQUEST['foilItem'];
$technique    = $_REQUEST['technique'];

$w			= $_REQUEST['foilWidth'];// $_REQUEST['w']; //meters -(foil width)
$h			= $_REQUEST['foilHeight'];//27;// $_REQUEST['h']; // inches - (foil height)\

$sql = "SELECT intPrintWidth AS width,intPrintHeight AS height FROM cost_samplecostsheet_print_sizes WHERE intSampleNo = '$sampleNo'
    AND intSampleYear = '$sample_year' AND intRevisionNo = '$revisionNo' AND strComboName = '$comboName' AND strPrintName = '$PrintName' AND intItem = '$foilItem' AND TECHNIQUE = '$technique'";

$result = $db->RunQuery($sql);
$rowCount = mysqli_num_rows($result);
$h			= $h*0.0254;//convert inches to meters (foil height)

$i = 1;
if($rowCount >0 ) {
while ($row = mysqli_fetch_array($result)) {
$pw			= $row['width']*0.0254;//5;// $_REQUEST['pw']; // meters - (Print Width)
$ph			= $row['height']*0.0254;//4; // $_REQUEST['ph']; // inches -(Print Height)

$w_max=1000;
$h_max=65;
//rows and coloms---
//////////// 1ST METHOD//////////////////
//echo "r(".$h."/".$ph.")";
//echo "w(".$w."/".$pw.")";

$t_a_rows	= intval(strval($h/$ph));
$t_a_cols	= intval(strval($w/$pw));
$t_aw		= $t_a_cols*$pw;
$t_ah		= $t_a_rows*$ph;

$t_b_rows	= 0;
$t_b_cols	= 0;
$t_bw		=$w-$t_aw; 				//$w-$t_a_cols*$pw;
$t_bh		=$h-$t_ah; 				//h-$t_a_rows*$ph;

$t_c_rows	= intval(strval($t_bw/$ph));	//intval(($w-$t_a_cols*$pw)/$ph);
$t_c_cols	= intval(strval($h/$pw));
if($t_c_cols>=1){
	$t_c_cols	= intval(strval($t_bh/$pw));
	$t_c_rows = intval(strval($w/$ph));
	}
$t_cw		= $h;
$t_ch		= $ph*$t_c_rows;

$t_d_rows	= 0;
$t_d_cols	= 0;
$t_dw		= $h;
$t_dh		= $w-($t_aw+$t_ch);

//////////// 2ND METHOD//////////////////
//echo "e-r"."(".$h."/".$pw.")=".intval($h/$pw);
//$t_e_rows	= intval($h/$pw);
//echo "e-c"."(".$w."/".$ph.")=".intval($w/$ph);
//$t_e_cols	= intval($w/$ph);

$t_e_rows	= ($h/$pw);
$t_e_cols	= ($w/$ph);

$t_e_rows	= intval(strval($t_e_rows));
$t_e_cols	= intval(strval($t_e_cols));

$t_ew		= $ph*$t_e_cols;
$t_eh		= $t_e_rows*$pw;


$t_f_rows	= 0;
$t_f_cols	= 0;
$t_fw		= $pw*$t_e_rows;
$t_fh		= $w-$t_e_cols*$ph;

$t_g_rows	= intval(strval(($h-$t_e_rows*$pw)/$ph));
$t_g_cols	= intval(strval($w/$pw));
$t_gw		= $w;
$t_gh		= $ph*$t_g_rows;


$t_h_rows	= 0;
$t_h_cols	= 0;
$t_hw		= $w;
$t_hh		= $h-($t_eh+$t_gh);
//------------------

?>

<body>
<h2>First method for Print <?php echo $i?></h2>
<br />
<table width="79%" height="280" cellpadding="0" cellspacing="0" style="border-collapse:collapse; border:1px solid black">
  <tr>
    <td width="22%" height="50" bgcolor="#00CCFF">&nbsp;</td>
    <td width="55%" >&nbsp;</td>
    <td width="5%" bgcolor="#33CC00">&nbsp;</td>
    <td width="4%">&nbsp;</td>
    <td width="2%" bgcolor="#CC00CC">&nbsp;</td>
    <td width="1%">&nbsp;</td>
    <td width="11%">&nbsp;</td>
  </tr>
  <tr>
    <td height="162">&nbsp;</td>
    <td><em style="font-size:120px ; opacity:.1">foil role </em></td>
    <td bgcolor="#33CC00">&nbsp;</td>
    <td>&nbsp;</td>
    <td bgcolor="#CC00CC">&nbsp;</td>
    <td>&nbsp;</td>
    <td><table width="118%" height="50" border="0" cellpadding="0" cellspacing="0">
      <tr></tr>
      <tr>
        <td width="26%"><table width="69%" border="0" cellspacing="0" cellpadding="0">
          <tr bgcolor="#00CCFF">
            <td>&nbsp;</td>
          </tr>
        </table></td>
        <td width="5%">&nbsp;</td>
        <td width="60%"><?php echo $t_a_rows* $t_a_cols ;?>&nbsp;</td>
      </tr>
      <tr>
        <td height="23" ><table width="69%" border="0" cellspacing="0" cellpadding="0">
          <tr bgcolor="#00CCFF">
            <td bgcolor="#CC00CC">&nbsp;</td>
          </tr>
        </table></td>
        <td>&nbsp;</td>
        <td>wastage</td>
      </tr>
      <tr>
        <td height="23" ><table width="69%" border="0" cellspacing="0" cellpadding="0">
          <tr bgcolor="#33CC00">
            <td>&nbsp;</td>
          </tr>
        </table></td>
        <td>&nbsp;</td>
        <td><?php echo $t_c_rows*$t_c_cols; ?>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="49">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td bgcolor="#CC00CC">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr bgcolor="#CC00CC">
    <td height="19" >&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td bgcolor="#CC00CC">&nbsp;</td>

  </tr>
  
  </tr>
</table>
<br /><br />
<h2>Second method for Print <?php echo $i?></h2>
<br />
<table width="79%" height="280" style="border-collapse:collapse; border:1px solid black" cellpadding="0" cellspacing="0" >
  <tr>
    <td width="22%" height="50" bgcolor="#00CCFF">&nbsp;</td>
    <td width="55%" >&nbsp;</td>
    <td width="5%" bgcolor="#33CC00">&nbsp;</td>
    <td width="4%">&nbsp;</td>
    <td width="2%" bgcolor="#CC00CC">&nbsp;</td>
    <td width="1%">&nbsp;</td>
    <td width="11%">&nbsp;</td>
  </tr>
  <tr>
    <td height="162">&nbsp;</td>
    <td><em style="font-size:120px ; opacity:.1">foil role </em>&nbsp;</td>
    <td bgcolor="#33CC00">&nbsp;</td>
    <td>&nbsp;</td>
    <td bgcolor="#CC00CC">&nbsp;</td>
    <td>&nbsp;</td>
    <td><table width="118%" height="50" border="0" cellpadding="0" cellspacing="0">
      <tr></tr>
      <tr>
        <td width="26%"><table width="69%" border="0" cellspacing="0" cellpadding="0">
          <tr bgcolor="#00CCFF">
            <td>&nbsp;</td>
          </tr>
        </table></td>
        <td width="5%">&nbsp;</td>
        <td width="60%"><?php echo $t_e_rows*$t_e_cols; ?>&nbsp;</td>
      </tr>
      <tr>
        <td height="23" ><table width="69%" border="0" cellspacing="0" cellpadding="0">
          <tr bgcolor="#00CCFF">
            <td bgcolor="#CC00CC">&nbsp;</td>
          </tr>
        </table></td>
        <td>&nbsp;</td>
        <td>wastage</td>
      </tr>
      <tr>
        <td height="23" ><table width="69%" border="0" cellspacing="0" cellpadding="0">
          <tr bgcolor="#33CC00">
            <td>&nbsp;</td>
          </tr>
        </table></td>
        <td>&nbsp;</td>
        <td><?php echo $t_g_rows*$t_g_cols ;?>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="49">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td bgcolor="#CC00CC">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr bgcolor="#CC00CC">
    <td height="19" >&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td bgcolor="#CC00CC">&nbsp;</td>
  </tr>
  <tr>
    <td></tr></td>
  </tr>
</table>
</body>
<br/>
<?php
if (($t_a_rows* $t_a_cols+$t_c_rows*$t_c_cols)>=($t_e_rows*$t_e_cols+$t_g_rows*$t_g_cols )){
	# code...
	echo "first method has effective cut with pcs: ".($t_a_rows* $t_a_cols+$t_c_rows*$t_c_cols);
	echo "<br>"."second method has cut with pcs: ".($t_e_rows*$t_e_cols+$t_g_rows*$t_g_cols );
}else{
	echo "second method has effective cut with pcs: ".($t_e_rows*$t_e_cols+$t_g_rows*$t_g_cols );
	echo "<br>"."first method has cut with pcs: ".($t_a_rows* $t_a_cols+$t_c_rows*$t_c_cols);
}
?>
<?php $i++;} } ?>
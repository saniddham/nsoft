<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId 				= $_SESSION['CompanyID'];
$userId 				= $_SESSION['userId'];
$locationId 			= $companyId;
$programCode			= 'P0116';

include_once  "class/cls_commonErrorHandeling_get.php";

$obj_commonErrHandle 	= new cls_commonErrorHandeling_get($db);

$graphicNo 				= $_REQUEST['graphicNo'];
$sampleNo 				= $_REQUEST['sampleNo'];
$sampleYear 			= $_REQUEST['sampleyear'];
$revNo 					= $_REQUEST['revNo'];
$combo 					= $_REQUEST['combo'];
$print 					= $_REQUEST['print'];
$approveMode 			= $_REQUEST['approveMode'];

if($approveMode=='confirm')
	$mode = 'Confirm';
else if($approveMode=='cancle')
	$mode = 'Cancel';
 				
		$sql = "SELECT 
				tbl.SAMPLE_NO,
				tbl.PRODUCTION_LOCATION,
				tbl.UPS,
				tbl.ESTIMATED_ORDER_QTY,
				tbl.NO_OF_COLOURS,
				tbl.SHOTS,
				tbl.COST_PER_SHOT,
				tbl.INK_COST,
				tbl.SPECIAL_RM_COST,
				tbl.PROCESS_COST,
				tbl.SHOT_COST,
				tbl.OTHER_COST,
				tbl.OTHER_COST_REMARKS,
				tbl.TOTAL_COST,
				tbl.MARGIN_PERCENTAGE,
				tbl.MARGIN,
				tbl.SCREENLINE_PRICE,
				tbl.TARGET_PRICE,
				tbl.APPROVED_PRICE,
				tbl.QTY_PRICES,
				tbl.COPPIED_FLAG,
				tbl.COPPIED_FROM_SAMPLE_NO,
				tbl.COPPIED_FROM_SAMPLE_YEAR,
				tbl.COPPIED_FROM_REVISION,
				tbl.COPPIED_FROM_COMBO,
				tbl.COPPIED_FROM_PRINT,
				tbl.`STATUS`,
				tbl.APPROVE_LEVELS,
				tbl.SAVED_BY,
				tbl.SAVED_TIME,
				mst_fabrictype.strName AS fabric,
				mst_part.strName AS grapic_placement,
				trn_sampleinfomations_printsize.intWidth,
				trn_sampleinfomations_printsize.intHeight,
				mst_routing.DESCRIPTION AS Routing,
				mst_locations.strName AS location,
				sys_users.strUserName as creator ,
				trn_sampleinfomations.strGraphicRefNo 
				FROM
				costing_sample_header AS tbl
				INNER JOIN trn_sampleinfomations ON tbl.SAMPLE_NO = trn_sampleinfomations.intSampleNo AND tbl.SAMPLE_YEAR = trn_sampleinfomations.intSampleYear AND tbl.REVISION = trn_sampleinfomations.intRevisionNo 
				left JOIN trn_sampleinfomations_combo_print_routing ON trn_sampleinfomations_combo_print_routing.SAMPLE_NO = tbl.SAMPLE_NO AND trn_sampleinfomations_combo_print_routing.SAMPLE_YEAR = tbl.SAMPLE_YEAR AND trn_sampleinfomations_combo_print_routing.REVISION = tbl.REVISION AND trn_sampleinfomations_combo_print_routing.COMBO = tbl.COMBO AND trn_sampleinfomations_combo_print_routing.PRINT = tbl.PRINT
				INNER JOIN trn_sampleinfomations_details ON tbl.SAMPLE_NO = trn_sampleinfomations_details.intSampleNo AND tbl.SAMPLE_YEAR = trn_sampleinfomations_details.intSampleYear AND trn_sampleinfomations_details.intRevNo = tbl.REVISION AND trn_sampleinfomations_details.strComboName = tbl.COMBO AND trn_sampleinfomations_details.strPrintName = tbl.PRINT
				INNER JOIN mst_fabrictype ON trn_sampleinfomations_details.intFabricType = mst_fabrictype.intId
				INNER JOIN trn_sampleinfomations_printsize ON trn_sampleinfomations_printsize.intSampleNo = tbl.SAMPLE_NO AND trn_sampleinfomations_printsize.intSampleYear = tbl.SAMPLE_YEAR AND tbl.REVISION = trn_sampleinfomations_printsize.intRevisionNo AND tbl.PRINT = trn_sampleinfomations_printsize.strPrintName
				INNER JOIN mst_part ON trn_sampleinfomations_printsize.intPart = mst_part.intId
				left JOIN mst_routing ON trn_sampleinfomations_combo_print_routing.ROUTING = mst_routing.ID
				left JOIN mst_locations ON tbl.PRODUCTION_LOCATION = mst_locations.intId
				INNER JOIN sys_users ON tbl.SAVED_BY = sys_users.intUserId
				WHERE
				tbl.SAMPLE_NO 		= '$sampleNo' 	AND
				tbl.SAMPLE_YEAR 	= '$sampleYear'	AND
				tbl.REVISION 		= '$revNo' 		AND
				tbl.COMBO 			= '$combo' 		AND
				tbl.PRINT 			= '$print'
				GROUP BY
				tbl.SAMPLE_NO,
				tbl.SAMPLE_YEAR,
				tbl.REVISION,
				tbl.COMBO,
				tbl.PRINT
			";
			//echo $sql;
			$result 			= $db->RunQuery($sql);
			$header_array		= mysqli_fetch_array($result);
			$graphic			= $header_array['strGraphicRefNo'];
			$grpic_plecement	= $header_array['grapic_placement'];
			$noOfColor			= $header_array['NO_OF_COLOURS'];
			$estimated_odr_qty 	= $header_array['ESTIMATED_ORDER_QTY'];
			$noOfUps 			= $header_array['UPS'];
			$routing 			= $header_array['Routing'];
			$location 			= $header_array['location'];
			$width				= $header_array['intWidth'];
			$height				= $header_array['intHeight'];
			$fabric				= $header_array['fabric'];
			$shots 				= $header_array['SHOTS'];
			$cost_per_shot 		= $header_array['COST_PER_SHOT'];
			$int_cost 			= $header_array['INK_COST'];
			$specil_rm_cost		= $header_array['SPECIAL_RM_COST'];
			$process_cost		= $header_array['PROCESS_COST'];
			$shot_cost			= $header_array['SHOT_COST'];
			$other_cost 		= $header_array['OTHER_COST'];
			$other_cost_remark	= $header_array['OTHER_COST_REMARKS'];
			$total_cost			= $header_array['TOTAL_COST'];
			$margin_percentage	= $header_array['MARGIN_PERCENTAGE'];
			$margin				= $header_array['MARGIN'];
			$sl_price			= $header_array['SCREENLINE_PRICE'];
			$target_price 		= $header_array['TARGET_PRICE'];
			$approve_price		= $header_array['APPROVED_PRICE'];
			$qtyPriceEnabled    =  $header_array['QTY_PRICES'];
			$status				= $header_array['STATUS'];
			$levels				= $header_array['APPROVE_LEVELS'];
			$userId				= $header_array['SAVED_BY'];
			$username			= $header_array['creator'];
			$createdTime		= $header_array['SAVED_TIME'];
			$flag_saved	=1;
			if($header_array['SAMPLE_NO']=='')
			$flag_saved	=0;
			
$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_reject($status,$levels,$userId,$programCode,'RunQuery');
$permision_reject			= $permition_arr['permision'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_confirm($status,$levels,$userId,$programCode,'RunQuery');
$permision_confirm			= $permition_arr['permision'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_cancel($status,$levels,$userId,$programCode,'RunQuery');
$permision_cancel			= $permition_arr['permision'];

$approved_or_pending_coppied=getCoppied($sampleNo,$sampleYear,$revNo,$combo,$print);	
	
$prod_location				= '';//by costng
//$routing					= '';//by costng
//check permition to approve (action)

//$header_array	= get_header($graphicNo,$sampleNo,$sampleYear,$revNo,$combo,$print,$prod_location,$routing);
?><head>
<title>Price Costing Report</title>
<link rel="stylesheet" type="text/css" href="css/button.css"/>
<link rel="stylesheet" type="text/css" href="css/promt.css"/>
<script type="application/javascript" src="presentation/costing/sample/priceCosting/addNew/priceCosting-js.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position: absolute;
	left: 300px;
	top: 170px;
	width: 650px;
	height: 322px;
	z-index: 1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>



	<?php
	if($flag_saved==1){
    if($status=='-2')
    {
        echo "<div id=\"apDiv1\" style=\"opacity:0.2\"><img src=\"images/cancelled.png\"/></div>";
    }
    else if($status!=1)//pending
    {
        echo "<div id=\"apDiv1\"><img src=\"images/pending.png\"/></div>";
    }
	}
    ?>
 <form id="frmSampleCostSheetRpt" name="frmSampleCostSheetRpt" method="post" action="">
   <table width="100%" cellpadding="0" cellspacing="0">
    <tr>
    </tr>
    <tr>
    <td width="20%"></td>
    <td width="60%" height="80" valign="top"><?php if($flag_saved==1){ include 'reportHeader.php';} ?></td>
    <td width="20%"></td>
    </tr>
    <tr>
    <td colspan="3"></td>
    </tr>
    </table>

      <table width="88%" border="0" align="center" bgcolor="#FFFFFF" >
      <tr><td colspan="6" align="center">
      <table width="100%">
     	<?php
		if($flag_saved==1)
			include "presentation/report_approve_status_and_buttons.php";
			else{
			echo "<b>No saved costing for this combo and print<b>";
			die();
			}
 		?>
        </table>
        </td></tr>
      <tr><td colspan="6" align="center">
      <table width="100%">
      <?php
			$permition_app_rej_arr				= $obj_commonErrHandle->get_permision_withApproval_reject(2,$levels,$userId,$programCode,'RunQuery');
			$permision_app_reject			= $permition_app_rej_arr['permision'];
	  ?>
     	<tr><td><?php if($permision_app_reject==1 && $status==1 ){ ?><a id="butRptReject_approved" class="button white medium">Reject This Costing</a>&nbsp;&nbsp;<?php } ?><?php if($permision_app_reject==1 && $approved_or_pending_coppied>0){ ?><a id="butRptReject_approved_cp" class="button white medium">Reject All Coppied Costings</a><?php } ?></td></tr>
        </table>
        </td></tr>
        <tr><td colspan="6" class="normalfnt"><?php 	if($header_array['COPPIED_FROM_SAMPLE_NO'] !='') {
                    $coppiedString = " - (COPPIED FROM " . $header_array['COPPIED_FROM_SAMPLE_NO'] . "/" . $header_array['COPPIED_FROM_SAMPLE_YEAR'] . "/" . $header_array['COPPIED_FROM_REVISION'] . "/" . $header_array['COPPIED_FROM_COMBO'] . "/" . $header_array['COPPIED_FROM_PRINT'] . ")";
                    $coppiedUrl ="?q=1212";
                    $coppiedUrl = $coppiedUrl."&graphicNo="."&sampleyear=".$header_array['COPPIED_FROM_SAMPLE_YEAR']."&sampleNo=".$header_array['COPPIED_FROM_SAMPLE_NO']."&revNo=".$header_array['COPPIED_FROM_REVISION']."&combo=".$header_array['COPPIED_FROM_COMBO']."&print=".$header_array['COPPIED_FROM_PRINT'];
                    echo '<a href="'.$coppiedUrl.'" target="_blank"><span class="normalfnt">'.$coppiedString.'</span></a>';
//
                }
 ?></td></tr>
        <tr>
          <td align="center" colspan="6"><strong>Price Costing Report</strong></td>
        </tr>
        <tr><td colspan="6"><table width="100%" class="bordered">
                        <tr  nowrap="nowrap" class="headerLoad">
                          <th width="9%" class="normalfnt">Graphic No</th>
                          <td width="12%" id="txtgrapicNo"><?php echo $graphic; ?></td>
                          <th width="8%" class="normalfnt">Sample Year</th>
                          <td width="7%" id="txtSmapleYear"><?php echo $sampleYear; ?></td>
                          <th width="6%" class="normalfnt">Sample No</th>
                          <td width="11%" id="txtsampleNo"><?php echo $sampleNo; ?></td>
                          <th width="7%" class="normalfnt">Revision No</th>
                          <td width="6%" id="txtrevNo"><?php echo $revNo; ?></td>
                          <th width="6%" class="normalfnt">Combo</th>
                          <td width="12%" id="txtcombo"><?php echo $combo; ?></td>
                          <th width="4%">Print</th>
                          <td width="12%" id="txtprint"><?php echo $print; ?></td>
                        </tr>
                      </table></td></tr>
        
       
       
        <tr>
          <td colspan="6"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="218" height="22" class="normalfnt">&nbsp;</td>
                  <td width="312">&nbsp;</td>
                  <td colspan="2" class="normalfntMid"><strong>Graphic</strong></td>
                  <td width="38" class="normalfnt">&nbsp;</td>
                  <td width="203" class="normalfnt">&nbsp;</td>
                  <td width="151" class="normalfnt">&nbsp;</td>
                  <td width="95" align="left">&nbsp;</td>
                </tr>
               
                <tr>
                  <td class="normalfnt">Graphic Placement</td>
                  <td class="normalfnt"><?php echo $grpic_plecement; ?></td>
                  <td colspan="2" rowspan="6" align="center" ><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:264px;height:148px;overflow:hidden" >
                 <?php
			if($sampleNo!='')
			{
				echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"../../../../../documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revNo-".substr($print,6,1).".png\" />";	
			}
			 	?>
                  </div></td>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt">Graphic Width</td>
                  <td class="normalfnt"><?php echo $width;?>
                    &nbsp;Inches </td>
                  <td class="normalfnt">&nbsp;</td>
                </tr>
                <tr>
                  <td class="normalfnt">Fabric</td>
                  <td class="normalfnt" style="width:30px"><?php echo $fabric; ?></td>
                  <td align="left" class="normalfnt">&nbsp;</td>
                  <td align="left" class="normalfnt">Graphic Height</td>
                  <td align="left"><span class="normalfnt">
                    <?php echo $height;?>
                    &nbsp;Inches</span></td>
                  <td class="normalfnt">&nbsp;</td>
                </tr>
                <tr>
                  <td><span class="normalfnt">Production Location</span></td>
                  <td class="normalfnt"><?php echo $location; ?></td>
                  <td>&nbsp;</td>
                  <td><span class="normalfnt">Estimated order Qty</span></td>
                  <td class="normalfnt"><?php echo $estimated_odr_qty; ?></td>
                </tr>
                <tr>
                  <td class="normalfnt">Routing</td>
                  <td class="normalfnt"><?php echo $routing; ?></td>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt">&nbsp;</td>
                  <td align="left">&nbsp;</td>
                </tr>
                <tr>
                  <td class="normalfnt">No of ups </td>
                  <td class="normalfnt" style="width:30px"><?php echo $noOfUps; ?></td>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt">&nbsp;</td>
                  <td align="left">&nbsp;</td>
                </tr>
                <tr>
                  <td><span class="normalfnt">No of Colors</span></td>
                  <td class="normalfnt"><?php echo $noOfColor; ?></td>
                  <td>&nbsp;</td>
                  <td><span class="normalfnt">View Current Sample Sheet</td>
                  <td class="normalfnt"><a id="butcurrentView" class="button green small" style="" name="butcurrentView" on="on"> View </a>&nbsp;</td>
                </tr>
              </table></td>
            </tr>
        <?php
		if($header_array['COPPIED_FROM_SAMPLE_NO'] ==''){
		?>
            
            <tr>
              <td colspan="6"><div style="width:100%;height:300px;overflow:scroll" >
                <table width="100%" class="bordered" id="table_ink_type" >
                  <tr>
                    <th height="17" colspan="10"  class="normalfntMid"><strong>Ink Type Cost</strong></th>
                  </tr>
                  <tr>
                    <th width="5%" >View Similer</th>
                    <th width="17%" height="22" >Color</th>
                    <th width="13%" >Technique</th>
                    <th width="18%" >Ink Type</th>
                    <th width="6%" >No of Shots</th>
                    <th width="8%" >Cost Per sq inch ($)</th>
                    <th width="8%">Square Inches(W*H)</th>
                    <th width="6%">Currency</th>
                    <th width="8%">Usage %</th>
                    <th width="11%">Total Cost Price ($)</th>
                  </tr>
                  <?php 
						
						$result_tech =	get_details_array($sampleNo,$sampleYear,$revNo,$combo,$print);
						while($row=mysqli_fetch_array($result_tech))
						{
							echo "";
							$colorId	=$row['colour_id'];
							$color		=$row['COLOUR'];
							$techniqueId=$row['technique_id'];
							$technique	=$row['TECHNIQUE'];
							$inkTypeId	=$row['inktype_id'];
							$inkType	=$row['INK_TYPE'];
							$shots_ink	=$row['SHOTS'];
							$costPerInch=$row['COST_PER_SQ_INCH'];
							//$currencyId	=$row['intCurrencyId'];
							//$currency	=$row['currency'];
							//$itemId		=$row['intItem'];
							//$item		=$row['itemName'];
							$usage		=$row['USAGE'];
							$total_cost_price	=$row['TOTAL_COST_PRICE'];

							
					?>
                  <tr class="normalfnt dataRow">
                    <td bgcolor="#FFFFFF" id="viewsimiler" class="color"><a id="butSimilerView" class="button green small" style="" name="butSimilerView" on="on"> View </a></td>
                    <td bgcolor="#FFFFFF" id="<?php echo $colorId ; ?>" class="color"><?php echo $color ; ?></td>
                    <td bgcolor="#FFFFFF" id="<?php echo $techniqueId ; ?>" class="technique_getid"><?php echo $technique ; ?></td>
                    <td bgcolor="#FFFFFF" id="<?php echo $inkTypeId ; ?>" class="inkType"><?php echo $inkType ; ?></td>
                    <td bgcolor="#FFFFFF" class="item"><?php echo $shots_ink; ?></td>
                    <td bgcolor="#FFFFFF" align="center"><?php echo $costPerInch;?></td>
                    <td bgcolor="#FFFFFF" id="inches" class="inches" align="center"><?php echo $header_array['intWidth']*$header_array['intHeight'] ; ?></td>
                    <td bgcolor="#FFFFFF" id="<?php echo 'currencyId' ; ?>" class="currency" align="center">USD</td>
                    <td bgcolor="#FFFFFF" id="textfield9" name="textfield9" align="center" class="validate[required,custom[number],max[100]] usage cal_cost"s><?php echo $usage; ?></td>
                    <td bgcolor="#FFFFFF" align="right" class="totCost_ink_type"><?php echo $total_cost_price; ?></td>
                  </tr>
                  <?php
			  $tot_ink_prie		+= $estimatedOrderQty*$header_array['intWidth']*$header_array['intHeight']*$costPerInch*$usage/100;
				 }
				?>
                </table>
              </div></td>
            </tr>
            <tr>
                   <td class="normalfnt" colspan="6"><table width="1263" cellpadding="0" cellspacing="0" class="bordered" id="table_roll_form">
                    <tr class="">
                      <th height="17" colspan="14" class="normalfntMid"><strong> RM - Roll Form Cost</strong></th>
                    </tr>
                    <tr>
                      <th width="194">Color</th>
                      <th width="194" height="19">Technique</th>
                      <th width="187" height="19">Item</th>
                      <th width="66">Grapic W</th>
                      <th width="70">Grapic H</th>
                      <th width="91">Item Width</th>
                      <th width="103">Item Height</th>
                      <th width="103">Price ($)</th>
                      <th width="97">Add Sizes</th>
                      <th width="101">Consumption</th>
                      <th width="120">UOM</th>
                      <th width="129">Cost ($)</th>
                    </tr>
                    <?php 
			  			$result_tech	=	load_roll_form_rm($sampleNo,$sampleYear,$revNo,$combo,$print);
						while($row=mysqli_fetch_array($result_tech))
						{
							$color			=$row['color'];
							$technique		=$row['TECHNIQUE'];
							$technique_id	=$row['technique_id'];
							$item			=$row['ITEM'];
							$item_id		=$row['item_id'];
							$graphic_w		=$row['GRAPHIC_W'];
							$graphic_h		=$row['GRAPHIC_H'];
							$item_w			=$row['ITEM_W'];
							$item_h			=$row['ITEM_H'];
							$item_price		=$row['ITEM_PRICE'];
							$uom			=$row['UOM'];
							$consumption	=$row['CONSUMPTION'];
							$cost			=$row['COST'];
							//echo $auto_cons;
						 ?>
                    <tr class="dataRow specil_rm">
                      <td bgcolor="#FFFFFF" id="<?php echo $technique_id; ?>" class="technique_id" ><?php echo $color; ?></td>
                      <td height="21" bgcolor="#FFFFFF" id="<?php echo $technique_id; ?>" class="technique_id" ><?php echo $technique; ?></td>
                      <td height="21" bgcolor="#FFFFFF" id="<?php echo $item_id; ?>" class="itemId"><?php echo $item; ?></td>
                      <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $header_array['intWidth']; ?></td>
                      <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $header_array['intHeight']; ?></td>
                      <td bgcolor="#FFFFFF" class="pcs normalfntRight" id="textitem_w" ><?php echo $item_w; ?></td>
                      <td bgcolor="#FFFFFF" class="pcs normalfntRight" id="textitem_h" ><?php echo $item_h; ?></td>
                      <td bgcolor="#FFFFFF" class="pcs normalfntRight"><?php echo $item_price; ?></td>
                      <td bgcolor="#FFFFFF" class="pcs normalfntRight" nowrap="nowrap"><div id="consId" class="auto_cons" style="display:none"><?php echo $consumption ;?></div>
                       <a id="butView" class="button green medium" style="" name="butView" on="on"> View </a>&nbsp;</td>
                      <td bgcolor="#FFFFFF" class="pcs normalfntRight"><?php echo $consumption; ?></td>
                      <td bgcolor="#FFFFFF" class="meters normalfntRight"><?php echo $uom; ?></td>
                      <td bgcolor="#FFFFFF" class="cost_roll normalfntRight"><?php echo $cost; ?></td>
                    </tr>
                    <?php
						}
			  ?>
                  </table></td>
                </tr>
                <tr height="15">
                  <td colspan="6"></td>
                </tr>
                <tr>
                  <td class="normalfnt" colspan="6"><table width="1263" cellpadding="0" cellspacing="0" class="bordered" id="table_non_roll">
                    <tr class="">
                      <th height="17" colspan="13" class="normalfntMid"><strong> RM - Non-Roll Form Cost</strong></th>
                    </tr>
                    <tr>
                      <th width="211">Color</th>
                      <th width="211" height="19">Technique</th>
                      <th width="211" height="19">Item</th>
                      <th width="69">Grapic W</th>
                      <th width="112">Grapic H</th>
                      <th width="106">Price ($)</th>
                      <th width="104">Consumption</th>
                      <th width="49">UOM</th>
                      <th width="70">Cost ($)</th>
                      </tr>
                    <?php 
			  			$result_nrf	=	load_non_roll_form_rm($sampleNo,$sampleYear,$revNo,$combo,$print);
 						while($row=mysqli_fetch_array($result_nrf))
						{
							$color			=$row['color'];
							$technique		=$row['TECHNIQUE'];
							$item			=$row['ITEM'];
							$uom			=$row['UOM'];
							$conpc			=$row['CONSUMPTION'];
							$price			=$row['ITEM_PRICE'];
							$cost			=$row['COST'];
						 ?>
                    <tr class="dataRow">
                      <td bgcolor="#FFFFFF"><span class="technique_id"><?php echo $color; ?></span></td>
                      <td height="22" bgcolor="#FFFFFF"><?php echo $technique; ?></td>
                      <td height="22" bgcolor="#FFFFFF"><?php echo $item; ?></td>
                      <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $header_array['intWidth']; ?></td>
                      <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $header_array['intHeight']; ?></td>
                      <td bgcolor="#FFFFFF" class="pcs normalfntRight"><?php echo $price; ?></td>
                      <td bgcolor="#FFFFFF" class="pcs normalfntRight"><?php echo $conpc; ?></td>
                      <td bgcolor="#FFFFFF" class="meters normalfntRight"><?php echo $uom; ?></td>
                      <td bgcolor="#FFFFFF" class="cost_nonRoll normalfntRight"><?php echo $cost; ?></td>
                      </tr>
                    <?php
			$tot_sp_rm_prie+=$conpc*$price*$estimatedOrderQty;
				}
				?>
                  </table></td>
                </tr>
                <tr height="15">
                  <td colspan="6"></td>
                </tr>
                <tr>
                  <td class="normalfnt" colspan="6"><table width="1263" cellpadding="0" cellspacing="0" class="bordered" id="table_non_direct">
                    <tr class="">
                      <th height="17" colspan="11" class="normalfntMid"><strong> Non-Direct RM Cost</strong></th>
                    </tr>
                    <tr>
                      <th width="210" height="19">Item</th>
                      <th width="100">Grapic W</th>
                      <th width="112">Grapic H</th>
                      <th width="101">Price ($)</th>
                      <th width="107">Consumption</th>
                      <th width="65">UOM</th>
                      <th width="75">Cost ($)</th>
                      </tr>
                    <?php 
			  			$result_nrf	=	load_non_direct_rm($sampleNo,$sampleYear,$revNo,$combo,$print);
 						while($row=mysqli_fetch_array($result_nrf))
						{
							$item			=$row['ITEM'];
							//$item_id		=$row['item_id'];
							$uom			=$row['UOM'];
							$conpc			=$row['CONSUMPTION'];
							$price			=$row['ITEM_PRICE'];
							$cost			=$row['COST'];
						 ?>
                    <tr class="dataRow">
                      <td height="21" bgcolor="#FFFFFF"><?php echo $item; ?></td>
                      <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $header_array['intWidth']; ?></td>
                      <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $header_array['intHeight']; ?></td>
                      <td bgcolor="#FFFFFF" class="pcs normalfntRight"><?php echo $price; ?></td>
                      <td bgcolor="#FFFFFF" class="pcs normalfntRight"><?php echo $conpc; ?></td>
                      <td bgcolor="#FFFFFF" class="meters normalfntRight"><?php echo $uom; ?></td>
                      <td bgcolor="#FFFFFF" class="cost_nonDirect normalfntRight"><?php echo $cost; ?></td>
                      </tr>
                    <?php
				$tot_sp_rm_prie+=$conpc*$price*$estimatedOrderQty;
				}
				?>
                  </table></td>
                </tr>
                <tr>
                  <td class="normalfnt" colspan="4">&nbsp;</td>
                  <td colspan="2" valign="top" class="normalfnt">&nbsp;</td>
                </tr>
                <tr>
                  <td width="16%" align="left"></td>
                </tr>
                <tr>
                  <td colspan="2" class="normalfnt"><table width="100%" border="0" align="center" class="bordered"  id="MainGrid3">
                    <tr>
                      <th colspan="5" class="normalfntMid"><strong>Shots Cost</strong></th>
                    </tr>
                    <tr>
                      <th width="18%" bgcolor="#FFFFFF" >No of Shots</th>
                      <th width="24%" bgcolor="#FFFFFF" >Cost Per Shot</th>
                      <th width="23%" bgcolor="#FFFFFF" >Total Cost ($) </th>
                    </tr>
                    <?php
				//load saved details
				
				?>
                    <?php
				
 				?>
                    <tr>
                      <td bgcolor="#FFFFFF"><?php echo $shots; ?></td>
                      <td bgcolor="#FFFFFF"><?php echo $cost_per_shot; ?></td>
                      <td bgcolor="#FFFFFF" class="cost_shots" style="width:100px" align="right"><?php echo $shot_cost; ?>&nbsp;</td>
                    </tr>
                    <?php
			  $tot_shots_cost  =$estimatedOrderQty*$header['shots']*$header['shots_cost']/$ups;;
			  
				
				 $sql = "SELECT
					Sum(trn_sampleinfomations_details_technical.intNoOfShots) as manualShots 
					FROM
					trn_sampleinfomations_details ,
					trn_sampleinfomations_details_technical
					WHERE
					trn_sampleinfomations_details.intPrintMode =  '2' AND 
					trn_sampleinfomations_details.intSampleNo 	=  '$x_sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$x_sampleYear' AND 
					trn_sampleinfomations_details.strPrintName =  '$x_print' AND 
					trn_sampleinfomations_details.strComboName 		=  '$x_combo' AND
					trn_sampleinfomations_details.intRevNo 		=  '$x_revNo' 
					GROUP BY
					trn_sampleinfomations_details.intSampleNo,
					trn_sampleinfomations_details.intSampleYear,
					trn_sampleinfomations_details.intRevNo,
					trn_sampleinfomations_details.strPrintName,
					trn_sampleinfomations_details.strComboName 
";
					$result = $db->RunQuery($sql);
					$row=mysqli_fetch_array($result);
					$manualShots = $row['manualShots'];
				?>
                  </table></td>
                  <td>&nbsp;</td>
                  <td class="normalfnt">&nbsp;</td>
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" class="normalfnt">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="normalfnt">&nbsp;</td>
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" class="normalfnt"><div id="divProcess">
                    <table width="100%" border="0" align="center" class="bordered"  id="table_process_cost">
                      <tr>
                        <th colspan="4"  class="normalfntMid"><strong>Processes Cost</strong></th>
                      </tr>
                      <tr>
                       <th width="43%" >Name</th>
                        <th width="12%" nowrap="nowrap" >Order</th>
                        <th width="14%" nowrap="nowrap" >Price ($)</th>
                        <th width="20%" nowrap="nowrap" >Cost ($)</th>
                      </tr>
                      <?php
				    $sql = "SELECT
							costing_sample_routing_process.ORDER,
							costing_sample_routing_process.COST,
							mst_costingprocesses.strProcess
							FROM
							costing_sample_routing_process
							INNER JOIN mst_costingprocesses ON costing_sample_routing_process.PROCESS_ID = mst_costingprocesses.intId
							WHERE
							costing_sample_routing_process.SAMPLE_NO = '$sampleNo' AND
							costing_sample_routing_process.SAMPLE_YEAR  = '$sampleYear' AND
							costing_sample_routing_process.REVISION = '$revNo' AND 
							costing_sample_routing_process.COMBO = '$combo' AND
							costing_sample_routing_process.PRINT = '$print'
							";
							//echo $sql;
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$order = $row['ORDER'];
						$cost = $row['COST'];
						$name = $row['strProcess'];
						
				?>
                      <tr>
                   		<td bgcolor="#FFFFFF" class="normalfnt"><?php echo $name ?></td>
                        <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $order ?></td>
                        <td bgcolor="#FFFFFF" align="center"><?php echo $cost; ?></td>
                        <td bgcolor="#FFFFFF" align="center" class="pocess_cost"><?php echo $cost; ?></td>
                      </tr>
                      <?php
				$tot_process_cost	+= $cost;
					}
				?>
                    </table>
                  </div></td>
                  <td width="13%">&nbsp;</td>
                  <td width="17%" class="normalfnt">&nbsp;</td>
                  <td colspan="2">&nbsp;</td>
                </tr>
        <?php
		}
		?>
                
                
                <tr>
                  <td>&nbsp;</td>
                  <td width="21%">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="normalfnt">Ink Cost</td>
                  <td class="" width="11%">$ <?php echo $int_cost;?>
                    &nbsp;</td>
                  <td class="" width="22%">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td width="21%">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="normalfnt">Special RM Cost</td>
                  <td colspan="2" class="">$ <?php echo $specil_rm_cost; ?>&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" >&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="normalfnt">Process Cost</td>
                  <td colspan="2" class="">$ <?php echo $process_cost; ?>&nbsp;</td>
                </tr>
                
                  
                 <tr>
                 	<td>&nbsp;</td>
                    <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="normalfnt">Shots Cost</td>
                  <td colspan="2" class="">$ <?php echo $shot_cost; ?>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="normalfnt" rowspan="2">Other Cost</td>
                  <td width="11%" class="" rowspan="2" nowrap="nowrap">$ <?php echo $other_cost; ?></td>
                  <td width="22%" class="" rowspan="2"><?php echo $other_cost_remark; ?></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="normalfnt">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td bgcolor="#F4EDB9" class="normalfnt">Total Cost</td>
                  <td colspan="2" class="">$ <?php echo $total_cost; ?></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="normalfnt">Margin % <?php echo $margin_percentage; ?>
                   </td>
                  <td colspan="2" class="">$ <?php echo $margin; ?></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td bgcolor="#FAD163" class="normalfnt">Screenline    Price</td>
                  <td colspan="2" class="">$ <?php echo $sl_price; ?></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="normalfnt">Target Price</td>
                  <td colspan="2" class="">$ <?php echo $target_price; ?></td>
                </tr>
                <tr>
                  <td colspan="2">&nbsp;</td>
                  <td>&nbsp;</td>
                    <td bgcolor="#F7D2BB" class="normalfnt">Approved Price</td>
                    <td colspan="2" class="">$ <?php echo $approve_price; ?></td>
                </tr>
          <?php
          if ($qtyPriceEnabled == 1){
              ?>
              <tr>
                  <td colspan="2">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td bgcolor="#F7D2BB" class="normalfnt">Quantity Prices</td>
                  <td width="27%"  class="normalfnt"><a id="rptQtyPrices" class="button green medium" style="" name="rptQtyPrices"> Qty Prices</a></td>
              </tr>
              <?php
          }
          ?>
      	<?php
						$creator			   =$username;
						$createdDate		   =$createdTime;
						$header_array['STATUS']=$status;
						$header_array['LEVELS']=$levels;
						$sqlA = "SELECT
								sys_users.strUserName as UserName,
								tbl.APPROVED_DATE AS dtApprovedDate,
								APPROVED_LEVEL_NO AS intApproveLevelNo 
								FROM
								costing_sample_header_approved_by AS tbl
								INNER JOIN sys_users ON tbl.APPROVED_BY = sys_users.intUserId
								WHERE
								tbl.SAMPLE_NO ='$sampleNo' AND
								tbl.SAMPLE_YEAR ='$sampleYear' AND
								tbl.REVISION_NO ='$revNo' AND
								tbl.COMBO ='$combo' AND
								tbl.PRINT = '$print'
								-- AND	tbl.STATUS = 0 
								order by APPROVED_DATE ASC
		  
				  		";
						
						$resultA = $db->RunQuery($sqlA);
		
			include "presentation/report_approvedBy_details.php"
 		?>
                       
       </table>

</form>

<div style="width:900px; position:absolute; display:none;z-index:100;"  id="popupContact1"></div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
<?php
function get_header($graphicNo,$sampleNo,$sampleYear,$revNo,$combo,$print,$prod_location,$routing){

global $db;

			$sql = "SELECT DISTINCT
						trn_sampleinfomations_printsize.intWidth,
						trn_sampleinfomations_printsize.intHeight,
						trn_sampleinfomations.strStyleNo,
						mst_part.strName
					FROM
						trn_sampleinfomations_printsize
						Inner Join mst_part ON mst_part.intId = trn_sampleinfomations_printsize.intPart
						Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_printsize.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_printsize.intSampleYear
					WHERE
						trn_sampleinfomations_printsize.strPrintName  =  '$print' AND
						trn_sampleinfomations_printsize.intSampleNo   =  '$sampleNo' AND
						trn_sampleinfomations_printsize.intSampleYear =  '$sampleYear' AND
						trn_sampleinfomations_printsize.intRevisionNo =  '$revNo'
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					$resp['styleNo'] 		= $row['strStyleNo'];
					$resp['print_width']  	= $row['intWidth'];
					$resp['print_height']  	= $row['intHeight'];
					$resp['partName'] 		= $row['strName'];
				}
				
		$sql = "SELECT DISTINCT
				trn_sampleinfomations_details.strComboName,
				trn_sampleinfomations_details.intPrintMode,
				mst_printmode.strName as printMode, 
				trn_sampleinfomations_details.intWashStanderd,
				mst_washstanderd.strName as washStandard, 
				COUNT(DISTINCT trn_sampleinfomations_details.intColorId) AS noOfColors,
				trn_sampleinfomations_details.intFabricType,
				mst_fabrictype.strName as fabric_type_desc 
				FROM trn_sampleinfomations_details 
				inner join mst_washstanderd on mst_washstanderd.intId=trn_sampleinfomations_details.intWashStanderd  
				inner join mst_printmode on mst_printmode.intId=trn_sampleinfomations_details.intPrintMode   
				INNER JOIN mst_fabrictype ON trn_sampleinfomations_details.intFabricType = mst_fabrictype.intId
				WHERE
				trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
				trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND 
				trn_sampleinfomations_details.strPrintName 	=  '$print' AND 
				trn_sampleinfomations_details.strComboName 	=  '$combo' AND
				trn_sampleinfomations_details.intRevNo 		=  '$revNo' 
				";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		
		$resp['printMode']		= $row['printMode'];
		$resp['washCondition'] 	= $row['washStandard'];
		$resp['fab_type'] 		= $row['fabric_type_desc'];
		
		$sql = "SELECT COUNT(TB.intColorId) AS noOfColors FROM (SELECT  
 				trn_sampleinfomations_details_technical.intColorId 
				FROM trn_sampleinfomations_details_technical 
				WHERE
				trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
				trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND 
				trn_sampleinfomations_details_technical.strPrintName 	=  '$print' AND 
				trn_sampleinfomations_details_technical.strComboName 	=  '$combo' AND
				trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' GROUP BY trn_sampleinfomations_details_technical.intColorId ) AS TB
				";
		$result = $db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		$resp['noOfColors'] 	= $row['noOfColors'];
	
		$sql = "SELECT
				sum(trn_sampleinfomations_details_technical.intNoOfShots) as shots 
				FROM `trn_sampleinfomations_details_technical`
				WHERE
				trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
				trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND 
				trn_sampleinfomations_details_technical.strPrintName 	=  '$print' AND 
				trn_sampleinfomations_details_technical.strComboName 	=  '$combo' AND
				trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' 
				";
		$result = $db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		$resp['shots'] 	= $row['shots'];
	
		$sql = "SELECT
				mst_costingstrocktype.COST
				FROM `mst_costingstrocktype`
				WHERE
				mst_costingstrocktype.LOCATION = '$prod_location'
				";
		$result = $db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		$resp['shots_cost'] 	= $row['COST'];
	
		$sql = "SELECT
				trn_sampleinfomations_combo_print_routing.SAMPLE_NO,
				trn_sampleinfomations_combo_print_routing.SAMPLE_YEAR,
				trn_sampleinfomations_combo_print_routing.REVISION,
				trn_sampleinfomations_combo_print_routing.COMBO,
				trn_sampleinfomations_combo_print_routing.PRINT,
				(trn_sampleinfomations_combo_print_routing.UPS) as NO_OF_UPS
				FROM `trn_sampleinfomations_combo_print_routing`
				WHERE
				trn_sampleinfomations_combo_print_routing.SAMPLE_NO = '$sampleNo' AND
				trn_sampleinfomations_combo_print_routing.SAMPLE_YEAR = '$sampleYear' AND
				trn_sampleinfomations_combo_print_routing.REVISION = '$revNo' AND
				trn_sampleinfomations_combo_print_routing.COMBO = '$combo' AND
				trn_sampleinfomations_combo_print_routing.PRINT = '$print' AND
				trn_sampleinfomations_combo_print_routing.ROUTING = '$routing'
				";
				
		$result = $db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		$resp['ups'] 	= $row['NO_OF_UPS'];
	
		return $resp;
		
	
}


function get_details_array($sampleNo,$sampleYear,$revNo,$combo,$print){
	global $db;
	 $sql = "SELECT
			tbl.COLOUR AS colour_id,
			tbl.TECHNIQUE AS technique_id,
			tbl.INK_TYPE AS inktype_id,
			tbl.SHOTS,
			tbl.COST_PER_SQ_INCH,
			tbl.SQUARE_INCHES,
			tbl.`USAGE`,
			tbl.TOTAL_COST_PRICE,
			mst_inktypes.strName AS INK_TYPE,
			mst_techniques.strName AS TECHNIQUE,
			mst_colors.strName AS COLOUR
			FROM
			costing_sample_ink_type AS tbl
			INNER JOIN mst_inktypes ON tbl.INK_TYPE = mst_inktypes.intId
			INNER JOIN mst_techniques ON tbl.TECHNIQUE = mst_techniques.intId
			INNER JOIN mst_colors ON tbl.COLOUR = mst_colors.intId
			WHERE
				tbl.SAMPLE_NO = '$sampleNo'
			AND tbl.SAMPLE_YEAR = '$sampleYear'
			AND tbl.REVISION = '$revNo'
			AND tbl.COMBO = '$combo'
			AND tbl.PRINT = '$print'
			";
			//echo $sql;
			$result_tech = $db->RunQuery($sql);
			return $result_tech;
}


function load_roll_form_rm($sampleNo,$sampleYear,$revNo,$combo,$print){
	
	global $db;
	global $company;
	global $date;
	
	$sql	="SELECT
			tbl.TECHNIQUE AS technique_id,
			tbl.ITEM AS item_id,
			tbl.GRAPHIC_W,
			tbl.GRAPHIC_H,
			tbl.ITEM_W,
			tbl.ITEM_H,
			tbl.ITEM_PRICE,
			tbl.CONSUMPTION,
			tbl.COST,
			mst_techniques.strName AS TECHNIQUE,
			mst_item.strName AS ITEM,
			mst_units.strName AS UOM ,
			mst_colors.strName AS color
			FROM
			costing_sample_rm_roll_form AS tbl
			INNER JOIN mst_techniques ON tbl.TECHNIQUE = mst_techniques.intId
			INNER JOIN mst_item ON tbl.ITEM = mst_item.intId
			INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
			INNER JOIN mst_colors ON tbl.COLOR = mst_colors.intId
			WHERE
			tbl.SAMPLE_NO = '$sampleNo' AND
			tbl.SAMPLE_YEAR = '$sampleYear' AND
			tbl.REVISION = '$revNo' AND
			tbl.COMBO = '$combo' AND
			tbl.PRINT = '$print'
			";
	//echo $sql;
	$result_rf = $db->RunQuery($sql);
	return $result_rf;
}

function load_non_roll_form_rm($sampleNo,$sampleYear,$revNo,$combo,$print){
	
	global $db;
	global $company;
	global $date;

	$sql	="
			SELECT
			tbl.TECHNIQUE AS technique_id,
			tbl.ITEM AS item_id,
			tbl.GRAPHIC_W,
			tbl.GRAPHIC_H,
			tbl.ITEM_PRICE,
			tbl.CONSUMPTION,
			tbl.COST,
			mst_item.strName AS ITEM,
			mst_techniques.strName AS TECHNIQUE,
			mst_units.strName AS UOM,
			mst_colors.strName AS color
			FROM
			costing_sample_rm_non_roll_form AS tbl
			INNER JOIN mst_item ON tbl.ITEM = mst_item.intId
			INNER JOIN mst_techniques ON tbl.TECHNIQUE = mst_techniques.intId
			INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
			INNER JOIN mst_colors ON tbl.COLOR = mst_colors.intId
			WHERE
			tbl.SAMPLE_NO = '$sampleNo' AND
			tbl.SAMPLE_YEAR = '$sampleYear' AND
			tbl.REVISION = '$revNo' AND
			tbl.COMBO = '$combo' AND
			tbl.PRINT = '$print'
			";
			//echo $sql;
	$result_rf = $db->RunQuery($sql);
	return $result_rf;
}

function load_non_direct_rm($sampleNo,$sampleYear,$revNo,$combo,$print){
	global $db;
	global $company;
	global $date;
	$sql	="SELECT
			tbl.ITEM,
			tbl.GRAHIC_W,
			tbl.GRAPHIC_H,
			tbl.ITEM_PRICE,
			tbl.CONSUMPTION,
			tbl.COST,
			mst_item.strName AS ITEM,
			mst_units.strName AS UOM
			FROM
			costing_sample_rm_non_direct AS tbl
			INNER JOIN mst_item ON mst_item.intId = tbl.ITEM
			INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
			WHERE
			tbl.SAMPLE_NO = '$sampleNo' AND
			tbl.SAMPLE_YEAR = '$sampleYear' AND
			tbl.REVISION_NO = '$revNo' AND
			tbl.COMBO = '$combo' AND
			tbl.PRINT = '$print'
			";
	$result_rf = $db->RunQuery($sql);
	return $result_rf;
}
function geCostingSavedConpc($sampleNo,$sampleYear,$revNo,$combo,$print,$technique_id,$item_id){
		global $db;
		$sql	= "SELECT
					Sum(cost_samplecostsheet_print_sizes.intConsumption) as consumption 
					FROM `cost_samplecostsheet_print_sizes`
					WHERE
					cost_samplecostsheet_print_sizes.intSampleNo = '$sampleNo' AND
					cost_samplecostsheet_print_sizes.intSampleYear = '$sampleYear' AND
					cost_samplecostsheet_print_sizes.intRevisionNo = '$revNo' AND
					cost_samplecostsheet_print_sizes.strComboName = '$combo' AND
					cost_samplecostsheet_print_sizes.strPrintName = '$print' AND
					cost_samplecostsheet_print_sizes.TECHNIQUE = '$technique_id' AND
					cost_samplecostsheet_print_sizes.intItem = '$item_id'";		
		$result = $db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		return $row['consumption'];
}

function getCoppied($sampleNo,$sampleYear,$revNo,$combo,$print){
		global $db;
		$sql = "SELECT
					SAMPLE_NO,
					SAMPLE_YEAR,
					REVISION,
					COMBO,
					PRINT 
					FROM
					costing_sample_header
					WHERE
					costing_sample_header.COPPIED_FROM_SAMPLE_NO = '$sampleNo' AND
					costing_sample_header.COPPIED_FROM_SAMPLE_YEAR = '$sampleYear' AND
					costing_sample_header.COPPIED_FROM_REVISION = '$revNo' AND
					costing_sample_header.COPPIED_FROM_COMBO = '$combo' AND
					costing_sample_header.COPPIED_FROM_PRINT = '$print' 
					and costing_sample_header.STATUS > 0 LIMIT 1
					";	 
		$result = $db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		return $row['SAMPLE_NO'];
}
?>

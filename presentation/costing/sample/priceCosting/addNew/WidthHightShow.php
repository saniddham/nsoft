<title>SAMPLE SPECIAL TECHNIQUES</title>
<style>

</style>
<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];
$company 	= $_SESSION['headCompanyId'];

$intUser  = $_SESSION["userId"];

include $backwardseperator."dataAccess/Connector.php";

$comboName 		   = $_REQUEST['comboName'];
$printName 		   = $_REQUEST['printName'];
$item              = $_REQUEST['item'];
$sampleNo          = $_REQUEST['sampleNo'];
$sample_year       = $_REQUEST['sample_year'];
$revisionNo        = $_REQUEST['revisionNo'];
$foilWidthPopUp    = $_REQUEST['foilWidthPopUp'];
$foilHeightPopUp   = $_REQUEST['foilHeightPopUp'];
$consum_auto       = $_REQUEST['consum_auto'];
$technique         = $_REQUEST['technique'];
$color		       = $_REQUEST['color'];
?>
<body>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>

<form id="WidthHeightPopup" name="WidthHeightPopup" method="post" action="">
<table width="450" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutS">
		  <div class="trans_text"> RM - Roll Form Cost Sizes </div>
		  <table width="483" border="0" align="center" bgcolor="#FFFFFF">
    <td width="477"><table width="477" border="0">
      <tr>
        <td width="471"></td>
      </tr>
      <tr>
        <td align="center"><div style="width:400px;height:300px;overflow:scroll" align="center" class="tableBorder_allRound" >
          <table width="100%"  class="bordered"  id="tblSizesPopup" align="center" >
            <tr class="">
                <?php
                $canEdit = 1;
                $sql_isApproved = "SELECT * FROM costing_sample_header
									where 
									costing_sample_header.SAMPLE_NO = '$sampleNo' AND
									costing_sample_header.SAMPLE_YEAR = '$sample_year' AND
									costing_sample_header.REVISION = '$revisionNo' AND
									costing_sample_header.COMBO = '$comboName' AND
									costing_sample_header.PRINT = '$printName' AND 
									costing_sample_header.`STATUS` =1 ";
                $result_isApproved = $db->RunQuery($sql_isApproved);
                $num_rows = mysqli_num_rows($result_isApproved);
                if($num_rows == 0){?>
                <?php 
                   $canEdit = 1;
				}
                else{
                    $canEdit = 0;
                }
                ?>
              <th width="11%" height="22" <?php if($canEdit==0){ ?> style="display:none" <?php } ?>>Delete</th>
              <th width="20%" height="22" >Width(Inch)</th>
              <th width="20%" >Height(Inch)</th>
              <th width="30%">Consumption(m)</th>
              </tr>
            <?php
          $sql = "SELECT intSerialNo,intPrintWidth,intPrintHeight,intConsumption FROM cost_samplecostsheet_print_sizes WHERE
                   intSampleNo = '$sampleNo'
                   AND intSampleYear = '$sample_year'
                   AND intRevisionNo = '$revisionNo'
                   AND strComboName = '$comboName'
                   AND strPrintName = '$printName' 
                   AND COLOR = '$color'
                   AND TECHNIQUE = '$technique'
                   AND intItem = '$item' ";
            $result = $db->RunQuery($sql);
            $r=0;

           while($row=mysqli_fetch_array($result)) {
            if($num_rows>0){ ?>
              <tr class="normalfnt widh_edit">
               <td align = "center" bgcolor = "#FFFFFF" id="deleteImage" <?php if($canEdit==0){ ?> style="display:none" <?php } ?>><img src = "images/del.png" width = "15" height = "15" class="delImgPopup" /><span id="deleteImageSerialNo" style="visibility: hidden"><?php echo $row['intSerialNo']?></span></td >
              <td align = "center" bgcolor = "#FFFFFF" id = "txtWidth" ><?php echo round($row['intPrintWidth'],6); ?><span id="isApproved" style="visibility: hidden"><?php echo $canEdit; ?></span></td >
              <td align = "center" bgcolor = "#FFFFFF" id = "txtHeight" ><?php echo round($row['intPrintHeight'],6); ?></td >
              <td align = "center" bgcolor = "#FFFFFF" id = "consumption" ><?php echo round($row['intConsumption'],6); ?></td >
              </tr >
            <?php }
               else{ $r=1; ?>
                   <tr class="normalfnt widh_edit" >
                       <td align = "center" bgcolor = "#FFFFFF" id="deleteImage" <?php if($canEdit==0){ ?> style="display:none" <?php } ?>><img src = "images/del.png" width = "15" height = "15" class="delImgPopup" /><span id="deleteImageSerialNo" style="visibility: hidden"><?php echo $row['intSerialNo']?></span></td >
                       <td align = "center" bgcolor = "#FFFFFF" id = "txtWidth" ><input name = "txtWidth" type = "text" id = "txtWidth" style = "width:80px; text-align:center"  value = "<?php echo round($row['intPrintWidth'],6); ?>" class="cls_cal cls_width validate[required,custom[number]] text-input"  /></td >
                       <td align = "center" bgcolor = "#FFFFFF" id = "txtHeight" ><input name = "txtHeight" type = "text" id = "txtHeight" style = "width:80px; text-align:center"   value = "<?php echo round($row['intPrintHeight'],6); ?>" class="cls_cal cls_height validate[required,custom[number]] text-input"  /></td >
                       <td align = "center" bgcolor = "#FFFFFF" id = "consumption" ><input type="text" value="<?php echo round($row['intConsumption'],6); ?>" size="10" style="text-align: center" id="ConsumptionInput" <?php if($consum_auto == 1){echo "disabled";} ?>></td >
                   </tr >
             <?php }
           }
            if($r==0 && $canEdit == 1){?>
            <tr class="normalfnt widh_edit" >
              <td align = "center" bgcolor = "#FFFFFF" id="deleteImage"><img src = "images/del.png" width = "15" height = "15" class="delImgPopup"  /><span id="deleteImageSerialNo" style="visibility: hidden"><?php echo 0; ?></span></td >
              <td align = "center" bgcolor = "#FFFFFF" id = "txtWidth" ><input name = "txtWidth" type = "text" id = "txtWidth" style = "width:80px; text-align:center"  value = "0" class="cls_cal cls_width validate[required,custom[number]] text-input"  /></td >
              <td align = "center" bgcolor = "#FFFFFF" id = "txtHeight" ><input name = "txtHeight" type = "text" id = "txtHeight" style = "width:80px; text-align:center"   value = "0" class="cls_cal cls_height validate[required,custom[number]] text-input"  /></td >
              <td align = "center" bgcolor = "#FFFFFF" id = "consumption" ><input type="text" value="0" size="10" style="text-align: center" id="ConsumptionInput" <?php if($consum_auto == 1){echo "disabled";} ?> ></td >
            </tr >
            <?php } ?>
            <tr class="dataRow">
            <td colspan="4" align="left"  bgcolor="#FFFFFF"><?php if($canEdit == 1 ){?><img src="images/Tadd.jpg" name="butInsertRowPopup" width="78" height="24" class="mouseover" id="butInsertRowPopup" /><?php } ?>
                <span id="foilWidthPopUp" style="visibility: hidden" ><?php echo $foilWidthPopUp; ?></span>
                <span id="foilHeightPopUp" style="visibility: hidden"><?php echo $foilHeightPopUp; ?></span>
                <span id="consum_auto" style="visibility: hidden"><?php echo $consum_auto;  ?></span>
            </td>
            </tr>
            </table>
          </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><?php if($canEdit == 1 ){?><img src="images/Tsave.jpg" width="92" height="24"  alt="add" id="butAdd" name="butAdd" class="mouseover"/><?php }?><img src="images/Tclose.jpg" width="92" height="24" id="butClose1" name="butClose1" class="mouseover"  /></td>
      </tr>
    </table></td>
    </tr>
  </table>
  </div>
  </div>
</form>
<div  style="visibility: hidden" id="popUpdata">
<p id="ComboNamePopUp"><?php echo $comboName; ?></p>
<p id="printNamePopUp"><?php echo $printName; ?></p>
<p id="itemName"><?php echo $item; ?></p>
<p id="itemWidth"><?php echo $foilWidthPopUp; ?></p>
<p id="consumption_auto"><?php echo $consum_auto; ?></p>
<p id="colorPopUp"><?php echo $color; ?></p>
<p id="techniquePopUp"><?php echo $technique; ?></p>
</div>
</body>
</html>


var basePath	="presentation/costing/sample/priceCosting/addNew/";
var measurments = [];
var max_ups		= 0;
var reportID 	= 1212;
var menuID		= 1068;
//var reportId	="895";


// JavaScript Document
$(document).ready(function() {
	
	
$('#cboStyleNo').die('change').live('change',function(){
		
	loadSampleNos();
	
	var intSampleNo = $('#cboSampleNo').val();
	//$('#frmSampleInfomations').get(0).reset();
	$('#cboSampleNo').val(intSampleNo);
	$('#divPicture').html('');
	return;
});

$('#cboYear').die('change').live('change',function(){
		
	loadSampleNos();
	
	var intSampleNo = $('#cboSampleNo').val();
	//$('#frmSampleInfomations').get(0).reset();
	$('#cboSampleNo').val(intSampleNo);
	$('#divPicture').html('');
	return;
});

$('#cboSampleNo').die('change').live('change',function(){
		
		loadRevisionNo();
		loadGraphic();
		
		var intSampleNo = $('#cboSampleNo').val();
		//$('#frmSampleInfomations').get(0).reset();
		$('#cboSampleNo').val(intSampleNo);
		$('#divPicture').html('');
		return;
});

$('#cboRevisionNo').die('change').live('change',function(){
	loadCombo();
});

$('#cboCombo').die('change').live('change',function(){
	loadPrint();	
});

//copy from -----------------------------------------------------

$('#cboStyleNoC').die('change').live('change',function(){
		
	loadSampleNosC();
	return;
});

$('#cboYearC').die('change').live('change',function(){
		
	loadSampleNosC();
	return;
});

$('#cboSampleNoC').die('change').live('change',function(){
		
		loadRevisionNoC();
		return;
});

$('#cboRevisionNoC').die('change').live('change',function(){
	loadComboC();
});

$('#cboComboC').die('change').live('change',function(){
	loadPrintC();	
});

//copy from old -----------------------------------------------------

$('#cboStyleNoC_O').die('change').live('change',function(){
		
	loadSampleNosC_O();
	return;
});

$('#cboYearC_O').die('change').live('change',function(){
		
	loadSampleNosC_O();
	return;
});

$('#cboSampleNoC_O').die('change').live('change',function(){
		
		loadRevisionNoC_O();
		return;
});

$('#cboRevisionNoC_O').die('change').live('change',function(){
	loadComboC_O();
});

$('#cboComboC_O').die('change').live('change',function(){
	loadPrintC_O();	
});

//----------------------------------------------------------------

$('#table_roll_form #butAdd').die('click').live('click',function(){
		addWidthHeight(this);
});

$('#WidthHeightPopup .cls_cal').die('keyup').live('keyup',function(){
		if($(this).parent().parent().parent().parent().find('#consum_auto').text()==1){
			calculateConsumption(this);
		}
});

$('#frmSampleCostSheet #butView').die('click').live('click',function(){
		getViewPage(this);
});

$('#frmSampleCostSheetRpt #butView').die('click').live('click',function(){
		getViewPageRepot(this);
});


$('#frmSampleCostSheet #butSimilerView').die('click').live('click',function(){
		getSimilerViewPage(this);
});

$('#frmSampleCostSheetRpt #butSimilerView').die('click').live('click',function(){
		getSimilerViewPage(this);
});

$('#frmSampleCostSheet #butcurrentView').die('click').live('click',function(){
		viewCurrentSampleSheet(this);
});	

$('#frmSampleCostSheetRpt #butcurrentView').die('click').live('click',function(){
		viewCurrentSampleSheetReport(this);
});	

$('.noOfups').die('keyup').live('keyup',function(){
	
	var m_ups 	= parseInt($('#max_ups').html());
	var copy	= $('input[name="chkType"]:checked').val();
	/*if(parseInt($('.noOfups').val()) > m_ups)
	{
		alert("Can't increase the ups");
		$('.noOfups').val(m_ups)
		return false;	
	}*/
	if(copy!=1)
	calTotCostPrice();

});

$('.chkProcess').die('click').live('click',function(){
	calTotCostPrice();
});

$('#butSave').die('click').live('click',function(){
	saveCosting();
});
$('#butApprove').die('click').live('click',function(){
	approve();
});


$('#chkType').die('click').live('click',function(){
	
	
	if($(this).is(':checked') && $(this).val()==2){
		$('#tableCostingDeails').show();
		$('.copyFrom').hide();
		$('.copyFromOld').hide();
		window.location.href ="?q=116&graphicNo="+URLEncode($('#cboStyleNo').val())
							+'&sampleNo='+$('#cboSampleNo').val()	
							+'&sampleYear='+$('#cboYear').val()	
							+'&revNo='+$('#cboRevisionNo').val()	
							+'&combo='+($('#cboCombo').val())	
							+'&printName='+($('#cboPrint').val())	
							+'&type=2'	
	}
	else if($(this).is(':checked') && $(this).val()==3){
		$('#tableCostingDeails').show();
		$('.copyFrom').hide();
		$('.copyFromOld').hide();
		window.location.href ="?q=116&graphicNo="+URLEncode($('#cboStyleNo').val())
							+'&sampleNo='+$('#cboSampleNo').val()	
							+'&sampleYear='+$('#cboYear').val()	
							+'&revNo='+$('#cboRevisionNo').val()	
							+'&combo='+($('#cboCombo').val())	
							+'&printName='+($('#cboPrint').val())	
							+'&type=3'	
	}
	else if($(this).is(':checked') && $(this).val()==1){
		$('#tableCostingDeails').hide();
		$('.copyFromOld').hide();
		$('.copyFrom').show();
	}
	else if($(this).is(':checked') && $(this).val()==4){
		$('#tableCostingDeails').hide();
		$('.copyFrom').hide();
		$('.copyFromOld').show();
	}
	
});



$('#frmSampleCostSheetRpt #butRptConfirm').die('click').live('click',urlApprove);
$('#frmSampleCostSheetRpt #butRptReject').die('click').live('click',urlReject);
$('#frmSampleCostSheetRpt #butRptReject_approved').die('click').live('click',urlReject_approved);
$('#frmSampleCostSheetRpt #butRptReject_approved_cp').die('click').live('click',urlReject_approved_cp);

$('#butCoppyTo').die('click').live('click',function(){
	coppyTo();
});

$('#coppyToPopup #butAdd').die('click').live('click',cappyTosave);
$('#coppyToPopup #butApprovepopup').die('click').live('click',cappyToApprove);
$('#coppyToPopup #butRejectpopup').die('click').live('click',cappyToReject);

$('#butInsertQtyPricesPopup').die('click').live('click',function(){
	console.log("this is triggered");
        var rowCount = document.getElementById('tblCostingPricePopup').rows.length;
        document.getElementById('tblCostingPricePopup').insertRow(rowCount-1);
        rowCount = document.getElementById('tblCostingPricePopup').rows.length;
        document.getElementById('tblCostingPricePopup').rows[rowCount-2].innerHTML = document.getElementById('tblCostingPricePopup').rows[rowCount-3].innerHTML;
        document.getElementById('tblCostingPricePopup').rows[rowCount-2].cells[0].innerHTML = "<img src='images/del.png' width='15' height='15' class='delImgPopup' />";
        document.getElementById('tblCostingPricePopup').rows[rowCount-2].cells[3].childNodes[0].className = "validate[required,custom[number],min[0]] sizeQty";
        document.getElementById('tblCostingPricePopup').rows[rowCount-2].cells[1].childNodes[0].value='';
        document.getElementById('tblCostingPricePopup').rows[rowCount-2].cells[2].childNodes[0].value='INF';
        document.getElementById('tblCostingPricePopup').rows[rowCount-2].cells[3].childNodes[0].value=0;

    });


$('.clsQtyPrice').die('click').live('click',function(){
		var value_o	=$('input[name="chkType"]:checked').val();
		var SYear;
		var sampleNo;
		var revision;
		var combo;
		var print;
		if (value_o == 1 ){
			SYear			= $("#cboYearC :selected").attr('id');
			sampleNo		= $("#cboSampleNoC :selected").val();
			revision		= $("#cboRevisionNoC :selected").val();
			combo 		    = $("#cboComboC :selected").val();
			print 			= $("#cboPrintC :selected").val();
		} else {
			SYear			= $("#cboYear :selected").attr('id');
			sampleNo		= $("#cboSampleNo :selected").val();
			revision		= $("#cboRevisionNo :selected").val();
			combo 		    = $("#cboCombo :selected").val();
			print 			= $("#cboPrint :selected").val();
		}

		print =   URLEncode(print);
		combo =   URLEncode(combo);


        popupWindow3('1');
        // presentation/costing/sample/priceCosting/addNew/qtyWisePricesPopup.php
        $('#popupContact1').load(basePath+'qtyWisePricesPopup.php?sampleNo='+sampleNo+'&sampleYear='+SYear+'&revision='+revision+'&combo='+combo+'&print='+print+'&editable=1',function(){
            $('#saveQtyPrices').click(saveQtyPrices);
            $('#butClose1').click(disablePopup);

        });
    });

$('#frmSampleCostSheetRpt #rptQtyPrices').die('click').live('click',function(){
	var arrBody  	= "";
	var sampleNo = $('#txtsampleNo').html();
	var SYear = $('#txtSmapleYear').html();
	var revision = $('#txtrevNo').html();
	var combo = $('#txtcombo').html();
	var print = $('#txtprint').html();
	print =   URLEncode(print);
	combo =   URLEncode(combo);

	popupWindow3('1');
	// presentation/costing/sample/priceCosting/addNew/qtyWisePricesPopup.php
	$('#popupContact1').load(basePath +'qtyWisePricesPopup.php?sampleNo='+sampleNo+'&sampleYear='+SYear+'&revision='+revision+'&combo='+combo+'&print='+print+'&editable=0',function(){
		// $('#saveQtyPrices').click(saveQtyPrices);
		$('#butClose1').click(disablePopup);

	});
});

$('.delImgPopup').die('click').live('click',function(){

	var rowCount1 = document.getElementById('tblCostingPricePopup').rows.length;
	if(rowCount1>4)
	{
		$(this).parent().parent().remove();
	}


});

	$('#enableQtyPrice').on('change', function()
	{
		if ($("#enableQtyPrice").is(":checked")){
			$('#addQtyPriceBtn').show();
			$("#textfield26").prop('disabled', true);
		} else {
			$('#addQtyPriceBtn').hide();
			$("#textfield26").prop('disabled', false);
		}
	});
//--------------------------------------------------------------------------------------
 
function alertx()
{
	$('#WidthHeightPopup #butAdd').validationEngine('hide')	;
}

function alertDelete()
{
	$('#frmColorRecipies #butDelete').validationEngine('hide')	;
}
 function loadGraphic()
{
	var url = basePath+"priceCosting-db-get.php?requestType=loadGraphic&sampleNo="+$('#cboSampleNo').val();
	var obj = $.ajax({url:url,type:'POST',async:false});	
	$('#frmSampleCostSheet #cboStyleNo').val(obj.responseText);
	$('#cboCombo').html('');	
	$('#cboPrint').html('');	
}
function loadRevisionNo()
{
	var url = basePath+"priceCosting-db-get.php?requestType=loadRevisionNo&sampleNo="+$('#cboSampleNo').val()+"&sampleYear="+$('#cboYear').val();
	var obj = $.ajax({url:url,type:'POST',async:false});	
	$('#cboRevisionNo').html(obj.responseText);
	$('#cboCombo').html('');	
	$('#cboPrint').html('');	
}
function loadSampleNos()
{
	var url = basePath+"priceCosting-db-get.php?requestType=loadSampleNo&styleNo="+URLEncode($('#cboStyleNo').val())+"&sampleYear="+$('#cboYear').val();
	var obj = $.ajax({url:url,type:'POST',async:false});	
	$('#cboSampleNo').html(obj.responseText);	
	$('#cboRevisionNo').html('');	
	$('#cboCombo').html('');	
	$('#cboPrint').html('');	
	
}
function loadCombo()
{
	var url = basePath+"priceCosting-db-get.php?requestType=loadCombo&sampleNo="+$('#cboSampleNo').val()+"&sampleYear="+$('#cboYear').val()+"&revNo="+$('#cboRevisionNo').val();
	var obj = $.ajax({url:url,type:'POST',async:false});	
	$('#cboCombo').html(obj.responseText);	
	$('#cboPrint').html('');
}
function loadPrint()
{
	var url = basePath+"priceCosting-db-get.php?requestType=loadPrint&sampleNo="+$('#cboSampleNo').val()+"&sampleYear="+$('#cboYear').val()+"&revNo="+$('#cboRevisionNo').val()+"&combo="+URLEncode($('#cboCombo').val());
	var obj = $.ajax({url:url,type:'POST',async:false});	
	$('#cboPrint').html(obj.responseText);	
}
//---------------------------------------------------------------

function loadRevisionNoC()
{
	var url = basePath+"priceCosting-db-get.php?requestType=loadRevisionNo&sampleNo="+$('#cboSampleNoC').val()+"&sampleYear="+$('#cboYearC').val();
	var obj = $.ajax({url:url,type:'POST',async:false});	
	$('#cboRevisionNoC').html(obj.responseText);
	$('#cboComboC').html('');	
	$('#cboPrintC').html('');	
}
function loadSampleNosC()
{
	var url = basePath+"priceCosting-db-get.php?requestType=loadSampleNo&styleNo="+URLEncode($('#cboStyleNoC').val())+"&sampleYear="+$('#cboYearC').val();
	var obj = $.ajax({url:url,type:'POST',async:false});	
	$('#cboSampleNoC').html(obj.responseText);	
	$('#cboRevisionNoC').html('');	
	$('#cboComboC').html('');	
	$('#cboPrintC').html('');	
	
}
function loadComboC()
{
	var url = basePath+"priceCosting-db-get.php?requestType=loadCombo&sampleNo="+$('#cboSampleNoC').val()+"&sampleYear="+$('#cboYearC').val()+"&revNo="+$('#cboRevisionNoC').val();
	var obj = $.ajax({url:url,type:'POST',async:false});	
	$('#cboComboC').html(obj.responseText);	
	$('#cboPrintC').html('');
}
function loadPrintC()
{
	var url = basePath+"priceCosting-db-get.php?requestType=loadPrint&sampleNo="+$('#cboSampleNoC').val()+"&sampleYear="+$('#cboYearC').val()+"&revNo="+$('#cboRevisionNoC').val()+"&combo="+URLEncode($('#cboComboC').val());
	var obj = $.ajax({url:url,type:'POST',async:false});	
	$('#cboPrintC').html(obj.responseText);	
}

//-LOAD OLD------------------------------------------------------------

function loadRevisionNoC_O()
{
	var url = basePath+"priceCosting-db-get.php?requestType=loadRevisionNo&sampleNo="+$('#cboSampleNoC_O').val()+"&sampleYear="+$('#cboYearC_O').val();
	var obj = $.ajax({url:url,type:'POST',async:false});	
	$('#cboRevisionNoC_O').html(obj.responseText);
	$('#cboComboC_O').html('');	
	$('#cboPrintC_0').html('');	
}
function loadSampleNosC_O()
{
	var url = basePath+"priceCosting-db-get.php?requestType=loadSampleNo&styleNo="+URLEncode($('#cboStyleNoC_O').val())+"&sampleYear="+$('#cboYearC_O').val();
	var obj = $.ajax({url:url,type:'POST',async:false});	
	$('#cboSampleNoC_O').html(obj.responseText);	
	$('#cboRevisionNoC_O').html('');	
	$('#cboComboC_O').html('');	
	$('#cboPrintC_O').html('');	
	
}
function loadComboC_O()
{
	var url = basePath+"priceCosting-db-get.php?requestType=loadCombo&sampleNo="+$('#cboSampleNoC_O').val()+"&sampleYear="+$('#cboYearC_O').val()+"&revNo="+$('#cboRevisionNoC_O').val();
	var obj = $.ajax({url:url,type:'POST',async:false});	
	$('#cboComboC_O').html(obj.responseText);	
	$('#cboPrintC_O').html('');
}
function loadPrintC_O()
{
	var url = basePath+"priceCosting-db-get.php?requestType=loadPrint&sampleNo="+$('#cboSampleNoC_O').val()+"&sampleYear="+$('#cboYearC_O').val()+"&revNo="+$('#cboRevisionNoC_O').val()+"&combo="+URLEncode($('#cboComboC_O').val());
	var obj = $.ajax({url:url,type:'POST',async:false});	
	$('#cboPrintC_O').html(obj.responseText);	
}

//----------------------------------------------------------------	
	
	
/*data load part*/
$('#cboPrint').die('change').live('change',function(){
	window.location.href ="?q=116&graphicNo="+URLEncode($('#cboStyleNo').val())
						+'&sampleNo='+$('#cboSampleNo').val()	
						+'&sampleYear='+$('#cboYear').val()	
						+'&revNo='+$('#cboRevisionNo').val()	
						+'&combo='+URLEncode($('#cboCombo').val())	
						+'&printName='+($('#cboPrint').val())	
});
	
/*data load part*/
$('#cboPrintC').die('change').live('change',function(){
	window.location.href ="?q=116&graphicNo="+URLEncode($('#cboStyleNo').val())
						+'&sampleNo='+$('#cboSampleNo').val()	
						+'&sampleYear='+$('#cboYear').val()	
						+'&revNo='+$('#cboRevisionNo').val()	
						+'&combo='+URLEncode($('#cboCombo').val())	
						+'&printName='+($('#cboPrint').val())	
						+'&graphicNoC='+URLEncode($('#cboStyleNoC').val())	
						+'&sampleNoC='+$('#cboSampleNoC').val()	
						+'&sampleYearC='+$('#cboYearC').val()	
						+'&revNoC='+$('#cboRevisionNoC').val()	
						+'&comboC='+URLEncode($('#cboComboC').val())	
						+'&printNameC='+($('#cboPrintC').val())
						+'&type=1'	

});
/*data load part*/
$('#cboPrintC_O').die('change').live('change',function(){
	window.location.href ="?q=116&graphicNo="+URLEncode($('#cboStyleNo').val())
						+'&sampleNo='+$('#cboSampleNo').val()	
						+'&sampleYear='+$('#cboYear').val()	
						+'&revNo='+$('#cboRevisionNo').val()	
						+'&combo='+URLEncode($('#cboCombo').val())	
						+'&printName='+($('#cboPrint').val())	
						+'&graphicNoC_O='+URLEncode($('#cboStyleNoC_O').val())	
						+'&sampleNoC_O='+$('#cboSampleNoC_O').val()	
						+'&sampleYearC_O='+$('#cboYearC_O').val()	
						+'&revNoC_O='+$('#cboRevisionNoC_O').val()	
						+'&comboC_O='+URLEncode($('#cboComboC_O').val())	
						+'&printNameC_O='+($('#cboPrintC_O').val())
						+'&type=4'	

});

 
 
	$('.cal_cost').die('keyup').live('keyup',function(){
		calTotCostPrice();
	});
 
	$('.delImg_process').die('click').live('click',function(){
		$(this).parent().parent().remove();
		calTotCostPrice();
	});
	
	$('#cboLocation').die('change').live('change',function(){
		var copy	= $('input[name="chkType"]:checked').val();
		loadNumberOfUps();
		loadShotsCost($('#cboLocation').val());
		if($('#cboLocation').val()!='' && $('#cboRouting').val()!='' && copy !=1&& copy !=4)
		calTotCostPrice();
	});
	
	$('#cboRouting').die('change').live('change',function(){
		loadNumberOfUps();
		loadprocess();
		loadShotsCost($('#cboLocation').val());
		if($('#cboLocation').val()!='' && $('#cboRouting').val()!='')
		calTotCostPrice();
	});

	// add new row for specil technique popup window.
	$('#butInsertRowPopup').die('click').live('click',function(){
		var rowCount = document.getElementById('tblSizesPopup').rows.length;
		document.getElementById('tblSizesPopup').insertRow(rowCount-1);
		rowCount = document.getElementById('tblSizesPopup').rows.length;
		var consum_auto,inputFeild;
		$(this).parent().parent().parent().parent().each(function () {
			consum_auto = $(this).find('#consum_auto').text();
		});
		if(consum_auto == 1){
			inputFeild = "<td id='deleteImage' align='center' bgcolor='#FFFFFF'><img src='images/del.png' class='delImgPopup' height='15' width='15'><span id='deleteImageSerialNo' style='visibility: hidden'>0</span></td><td id='txtWidth' align='center' bgcolor='#FFFFFF'><input name='txtWidth' id='txtWidth' style='width:80px; text-align:center' value='0' class='cls_cal cls_width validate[required,custom[number]] text-input' type='text'></td><td id='txtHeight' align='center' bgcolor='#FFFFFF'><input name='txtHeight' id='txtHeight' style='width:80px; text-align:center' value='0' class='cls_cal cls_height validate[required,custom[number]] text-input' type='text'></td><td id='consumption' align='center' bgcolor='#FFFFFF'><input value='0' size='10' style='text-align: center' type='text' id='ConsumptionInput' disabled></td>";
			}
		else if (consum_auto == 0){
			inputFeild = "<td id='deleteImage' align='center' bgcolor='#FFFFFF'><img src='images/del.png' class='delImgPopup' height='15' width='15'><span id='deleteImageSerialNo' style='visibility: hidden'>0</span></td><td id='txtWidth' align='center' bgcolor='#FFFFFF'><input name='txtWidth' id='txtWidth' style='width:80px; text-align:center' value='0' class='cls_cal cls_width validate[required,custom[number]] text-input' type='text'></td><td id='txtHeight' align='center' bgcolor='#FFFFFF'><input name='txtHeight' id='txtHeight' style='width:80px; text-align:center' value='0' class='cls_cal cls_height validate[required,custom[number]] text-input' type='text'></td><td id='consumption' align='center' bgcolor='#FFFFFF'><input value='0' size='10' style='text-align: center' type='text' id='ConsumptionInput'></td>";
		}
		if(rowCount == 3){
			document.getElementById('tblSizesPopup').rows[1].innerHTML = inputFeild;
			document.getElementById('tblSizesPopup').rows[1].cells[0].innerHTML = "<img src='images/del.png' width='15' height='15' class='delImgPopup' /><span id='deleteImageSerialNo' style='visibility: hidden'>0</span>";
			document.getElementById('tblSizesPopup').rows[1].cells[1].innerHTML = "<input name='txtWidth' value='0' type='text' id='txtWidth' style='width:80px; text-align:center' class='cls_cal cls_width validate[required,custom[number]] text-input'/>";
			document.getElementById('tblSizesPopup').rows[1].cells[2].innerHTML = "<input name='txtHeight' value='0' type='text' id='txtHeight' style='width:80px; text-align:center' class='cls_cal cls_height validate[required,custom[number]] text-input' />";
			//document.getElementById('tblSizesPopup').rows[1].cells[3].innerHTML = "<input type='text' value='0' size='10' style='text-align: center' id='ConsumptionInput' disabled>";
		}
		else{
			document.getElementById('tblSizesPopup').rows[rowCount-2].innerHTML = inputFeild;
			document.getElementById('tblSizesPopup').rows[rowCount-2].cells[0].innerHTML = "<img src='images/del.png' width='15' height='15' class='delImgPopup' /><span id='deleteImageSerialNo' style='visibility: hidden'>0</span>";
			document.getElementById('tblSizesPopup').rows[rowCount-2].cells[1].innerHTML = "<input name='txtWidth' value='0' type='text' id='txtWidth' style='width:80px; text-align:center' class='cls_cal cls_width validate[required,custom[number]] text-input'/>";
			document.getElementById('tblSizesPopup').rows[rowCount-2].cells[2].innerHTML = "<input name='txtHeight' value='0' type='text' id='txtHeight' style='width:80px; text-align:center' class='cls_cal cls_height validate[required,custom[number]] text-input' />";
			//document.getElementById('tblSizesPopup').rows[1].cells[3].innerHTML = "<input type='text' value='0' size='10' style='text-align: center' id='ConsumptionInput'>";

		}
	});
	
});//end of ready


//----------------------------------------------------------------------
 function loadItem(obj)
{
	 	var technique = $(obj).parent().parent().find('.technique').val();
	 	var sampleNo =$('#cboSampleNo').val();
	 	var sampleYear = $('#cboYear').val();
	 	var revNo = $('#cboRevisionNo').val();
	 	var combo = $('#cboCombo').val();
	 	var printName = $('#cboPrint').val();
		var url 		= basePath+"priceCosting-db-get.php?requestType=loadItem";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:"technique="+technique+"&sampleNo="+sampleNo+"&revNo="+revNo+"&combo="+URLEncode(combo)+"&sampleYear="+sampleYear+"&printName="+URLEncode(printName),
			async:false,
			success:function(json){
				var itemId=json.itemId;
				 $(obj).parent().parent().find('.foil').val(json.itemId)
				 //	if(itemId){	
					  $(obj).parent().parent().find('.foil').change();
				//	}
			}
		});
}
//----------------------------------------------------------------------
//----------------------------------------------------------------------
function clearRow(){
	var rowCount = document.getElementById('MainGrid2').rows.length;
	var i=0;
		//alert(rowCount-4);
	$('#MainGrid2 .dataRow').each(function(){
		if(i>(rowCount-5)){
/*			$(this).find('.printWidth').val('');
			$(this).find('.printHeight').val('');
			$(this).find('.qty').val('');
*/			$(this).find('.pcsFrmHeight').text('');
			$(this).find('.pcs').text('');
			$(this).find('.meters').text('');
			$(this).find('.cost').text('');
		}
		i++;
	});
}
//----------------------------------------------------------------------
function deleteDetails()
{			

  //  $('#frmCreditNote #butDelete').click(function(){
		
		var serial=$('#frmCreditNote #txtCreditNo').val();
		if($('#frmCreditNote #txtCreditNo').val()=='')
		{
			//$('#frmBankDeposit #butDelete').validationEngine('showPrompt', 'Please select Deposit No.', 'fail');
		//	var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+serial+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "creditNote-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'POST',
											data:'requestType=delete&serialNo='+serial,
											async:false,
											success:function(json){
												
												$('#frmCreditNote #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												if(json.type=='pass')
												{
													$('#frmCreditNote').get(0).reset();
													clearForm();
													loadCombo_search();
													var t=setTimeout("alertDelete()",1000);return;
												}
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	//});
	//clearForm();
}

function calTotCostPrice(){
	//var estimatedQty		= $('.estimatedQty').val();
	var copy				= $('input[name="chkType"]:checked').val();
	var copy_svd			= $('#copy').val();
	var estimatedQty		= 1;
	var ups					= $('.noOfups').val();
	var graphic_width		= $('#txtGraphicW').val();
	var graphic_height		= $('#txtGraphicH').val();
	var other_cost			= $('.cls_other_cost').val();
	var margin				= $('.cls_margin').val();
	var tot_ink_type_cost	= 0;
	var tot_other_rm_cost	= 0;
	var tot_shots_cost		= 0;
	var tot_process_cost	= 0;
	var roundToDeci			= 1000000
	
	if(copy==3 && copy_svd==1)
		copy=1;
	
	if(copy!=1 && copy!=4){
		//ink_type
	   var r =0;
		$('#table_ink_type >tbody >tr').not(':first').each(function(){
			r++;
			if(r>1){
				var costPerInch	= $(this).find('.inkCostPerSqInch').val();
				var shots		= $(this).find('.ink_shots').html();
				var usage		= $(this).find('.usage').val();
				//alert(estimatedQty+'*'+graphic_width+'*'+graphic_height+'*'+costPerInch+'*'+shots+'*'+usage);
				if(shots>0)
				$(this).find('.totCost_ink_type').val(Math.round((estimatedQty*graphic_width*graphic_height*costPerInch*shots*usage/100)*roundToDeci)/roundToDeci)
				tot_ink_type_cost	+= parseFloat($(this).find('.totCost_ink_type').val());
			}
		});
		
	   //roll form
	   var r =0;
		$('#table_roll_form >tbody >tr').not(':first').each(function(){
			r++;
			if(r>1){
				var price	= $(this).find('.price_roll').val();
				var cons	= $(this).find('.consump_roll').val();
				$(this).find('.cost_roll').html(Math.round((estimatedQty*price*cons)*roundToDeci)/roundToDeci)
				tot_other_rm_cost	+= parseFloat($(this).find('.cost_roll').html());
			}
			
		});
		
	   //non-roll form
	   var r =0;
		$('#table_non_roll >tbody >tr').not(':first').each(function(){
			r++;
			if(r>1){
				var price	= $(this).find('.price_nonRoll').val();
				var cons	= $(this).find('.consump_nonRoll').val();
				$(this).find('.cost_nonRoll').html(Math.round((estimatedQty*price*cons)*roundToDeci)/roundToDeci)
				tot_other_rm_cost	+= parseFloat($(this).find('.cost_nonRoll').html());
			}
			
		});
		
	   //non-roll form
	   var r =0;
		$('#table_non_direct >tbody >tr').not(':first').each(function(){
			r++;
			if(r>1){
				var price	= $(this).find('.price_nonDirect').val();
				var cons	= $(this).find('.cons_nonDirect').val();
				$(this).find('.cost_nonDirect').html(Math.round((estimatedQty*price*cons)*roundToDeci)/roundToDeci)
				tot_other_rm_cost	+= parseFloat($(this).find('.cost_nonDirect').html());
			}
			
		});
	
	 // shots cost
		var shots	= $('.shots').val();
		if(shots=='')
			shots=0;
		var price	= $('.price_shots').val();
		if(ups){
			if(shots>0)
				$('.cost_shots').val(Math.round((estimatedQty*price*shots/ups)*roundToDeci)/roundToDeci)
			if($('.cost_shots').val()=='')
				$('.cost_shots').val(0);
			tot_shots_cost	+= parseFloat($('.cost_shots').val());
		}
		if(tot_shots_cost=='')
			tot_shots_cost=0;
	 
		
	   //process
	   var r =0;
		$('#table_process_cost >tbody >tr').not(':first').each(function(){
			r++;
			if(r>1 && ($(this).find('.chkProcess').is(':checked'))){
				var price	= $(this).find('.pocess_price').val();
				 $(this).find('.pocess_cost').html(Math.round((estimatedQty*price)*roundToDeci)/roundToDeci)
				tot_process_cost	+= parseFloat($(this).find('.pocess_cost').html());
			}
			
		});
	}
	
	if(other_cost=='')
		other_cost =0;
	if(margin=='')
		margin =0;
		
	if(copy==1 || copy==4){
		other_cost			= $('.cls_other_cost').val();
		tot_ink_type_cost	= $('.cls_tot_ink_cost').val();
		tot_other_rm_cost	= $('.cls_tot_sp_cost').val();
		tot_process_cost	= $('.cls_tot_process_cost').val();
		tot_shots_cost		= $('.cls_tot_shot_cost').val();
		
		if(other_cost=='')
			$('.cls_other_cost').val(0);
		if(tot_ink_type_cost=='')
			$('.cls_tot_ink_cost').val(0);
		if(tot_other_rm_cost=='')
			$('.cls_tot_sp_cost').val(0);
		if(tot_process_cost=='')
			$('.cls_tot_process_cost').val(0);
		if(tot_shots_cost=='')
			$('.cls_tot_shot_cost').val(0);
			
		
		$('.cls_tot_process_cost').val(Math.round((tot_process_cost*estimatedQty)*roundToDeci)/roundToDeci)
		$('.cls_total_cost').val(Math.round((parseFloat(other_cost)+parseFloat(tot_ink_type_cost)+parseFloat(tot_other_rm_cost)+parseFloat(tot_process_cost)+parseFloat(tot_shots_cost))*roundToDeci)/roundToDeci);
		$('.cls_tot_cost_with_margin').val(Math.round((parseFloat($('.cls_total_cost').val())*parseFloat(margin)/100)*roundToDeci)/roundToDeci);
		$('.cls_screenline_price').val(Math.round((parseFloat($('.cls_total_cost').val())*(100+parseFloat(margin))/100)*roundToDeci)/roundToDeci);
		
	}
	else{
		$('.cls_tot_ink_cost').val(tot_ink_type_cost);
		$('.cls_tot_sp_cost').val(tot_other_rm_cost);
		$('.cls_tot_process_cost').val(Math.round((tot_process_cost*estimatedQty)*roundToDeci)/roundToDeci)
		$('.cls_tot_shot_cost').val(tot_shots_cost);
		$('.cls_total_cost').val(Math.round((parseFloat(other_cost)+parseFloat(tot_ink_type_cost)+parseFloat(tot_other_rm_cost)+parseFloat(tot_process_cost)+parseFloat(tot_shots_cost))*roundToDeci)/roundToDeci);
		$('.cls_tot_cost_with_margin').val(Math.round((parseFloat($('.cls_total_cost').val())*parseFloat(margin)/100)*roundToDeci)/roundToDeci);
		$('.cls_screenline_price').val(Math.round((parseFloat($('.cls_total_cost').val())*(100+parseFloat(margin))/100)*roundToDeci)/roundToDeci);
	}
}


function loadShotsCost(location){
	
	var url = basePath+"priceCosting-db-get.php?requestType=loadShotsCost&location="+location;
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		type:'POST',
		data:"",
		async:false,
		success:function(json){
			$('.price_shots').val(json.cost);	
		}
	});
	
}


function loadNumberOfUps(){
	
	var sampleNo 	=$('#cboSampleNo').val();
	var sampleYear 	= $('#cboYear').val();
	var revNo 		= $('#cboRevisionNo').val();
	var combo 		= $('#cboCombo').val();
	var printName 	= $('#cboPrint').val();
	var routing 	= $('#cboRouting').val();
	
	var url = basePath+"priceCosting-db-get.php?requestType=loadNumberOfUps";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		type:'POST',
		data:"sampleNo="+sampleNo+"&revNo="+revNo+"&combo="+URLEncode(combo)+"&sampleYear="+sampleYear+"&printName="+URLEncode(printName)+"&routing="+routing,
		async:false,
		success:function(json){
			$('.noOfups').val(json.ups);	
			max_ups	=json.ups 
			//deleteRows('table_process_cost');
			//$('#table_process_cost').html($('#table_process_cost').html()+json.process_grid)
		}
	});
	
}

function loadprocess(){
	
	var sampleNo 	=$('#cboSampleNo').val();
	var sampleYear 	= $('#cboYear').val();
	var revNo 		= $('#cboRevisionNo').val();
	var combo 		= $('#cboCombo').val();
	var printName 	= $('#cboPrint').val();
	var routing 	= $('#cboRouting').val();
	
	var url = basePath+"priceCosting-db-get.php?requestType=loadprocess";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		type:'POST',
		data:"sampleNo="+sampleNo+"&revNo="+revNo+"&combo="+URLEncode(combo)+"&sampleYear="+sampleYear+"&printName="+URLEncode(printName)+"&routing="+routing,
		async:false,
		success:function(json){
			$('.noOfups').val(json.ups);	
			//max_ups	=json.ups 
			deleteRows('table_process_cost');
			$('#table_process_cost').html($('#table_process_cost').html()+json.process_grid)
		}
	});
	
}

function addWidthHeight(e){
	var sampleNo    = $("#cboSampleNo").val();
	var sample_year = $("#cboYear").val();
	var revisionNo  = $("#cboRevisionNo").val();
	var comboName,printName,item,foilWidthPopUp,foilHeightPopUp,consum_auto,technique;
	//$('.headerLoad').each(function() {
		comboName = $('#cboCombo').val();
		printName = $('#cboPrint').val();
		//alert(comboName +'//'+printName);
	//});
	
	//technique = $(e).closest('tr').find('.roll_technique').attr('id');
	//$('#table_roll_form .technique').each(function() {
		color	 		= $(e).parent().parent().find('.color_id').attr("id");
		technique 		= $(e).parent().parent().find('.technique_id').attr("id");
		item 		    = $(e).parent().parent().find('.itemId').attr("id");
		foilWidthPopUp  = $(e).parent().parent().find('#textitem_w').val();
		foilHeightPopUp = $(e).parent().parent().find('#textitem_h').val();
		consum_auto     = $(e).parent().parent().find('.auto_cons').html();
  	//});
	popupWindow3('1');
	$('#popupContact1').load(basePath+"WidthHightShow.php?comboName="+URLEncode(comboName)+"&printName="+URLEncode(printName)+"&item="+item+"&sampleNo="+sampleNo+"&sample_year="+sample_year+"&revisionNo="+revisionNo+"&foilWidthPopUp="+foilWidthPopUp+"&foilHeightPopUp="+foilHeightPopUp+"&consum_auto="+consum_auto+"&color="+color+"&technique="+technique,function(){
		$('#butAdd').die('click').live('click',function(){
			savePrintSizes(e);
		});
		$('.delImgPopup').die('click').live('click',function(){
 			deletePrintSizes(e,this);
		});
		$('#butClose1').click(disablePopup);
		$('#butClose1').die('click').live('click',function(){
			//location.reload();
		});

	});
}
function savePrintSizes(e){
	
	var comsumption;
	if ($('#WidthHeightPopup').validationEngine('validate'))
	{
		var sampleNo    = $("#cboSampleNo").val();
		var sample_year = $("#cboYear").val();
		var revisionNo  = $("#cboRevisionNo").val();
		var comboName   = $("#ComboNamePopUp").html();
		var PrintName   = $("#printNamePopUp").html();
		var foilItem    = $("#itemName").html();
		var foilItem_w    = $("#itemWidth").html();
		var consumption_auto   = $("#consumption_auto").text();
		var technique   = $("#techniquePopUp").text();
		var color	    = $("#colorPopUp").text();
		var data 		= "requestType=saveConsumption";

		data+="&sampleNo=" +        sampleNo;
		data+="&sample_year=" +		sample_year;
		data+="&revisionNo=" +	    revisionNo;
		data+="&comboName=" +	    URLEncode(comboName);
		data+="&PrintName=" +	    URLEncode(PrintName);
		data+="&foilItem=" +	    foilItem;
		data+="&foilItem_w=" +	    foilItem_w
		data+="&technique=" +	    technique;
		data+="&color=" +	color;
		//////////////////// saving main grid part //////////////////////////////////////////
		var rowCount = $('#tblMain >tbody >tr').length;
		var rowCount = document.getElementById('tblSizesPopup').rows.length;
		var row = 0;
		var arr="[";
		var totalCoumption = 0;
		var validNumber;
		var consum;
		for(var i=1;i<rowCount-1;i++)
		{
			var serialNo     = document.getElementById('tblSizesPopup').rows[i].cells[0].childNodes[1].innerHTML;
			var width        = document.getElementById('tblSizesPopup').rows[i].cells[1].childNodes[0].value;
			var height 		 = document.getElementById('tblSizesPopup').rows[i].cells[2].childNodes[0].value;
			   consum	 	 = document.getElementById('tblSizesPopup').rows[i].cells[3].childNodes[0].value;
			   
			totalCoumption += +consum;
			if(+width > 0 && +height > 0 && consum > 0) {
				validNumber = true;
				arr += "{";
				arr += '"serialNo":"' + serialNo + '",';
				arr += '"width":"' + width + '",';
				arr += '"height":"' + height + '",';
				arr += '"consum":"' + consum + '"';
				arr += "},";
			}
			else {
				validNumber = false;
				alert("Print sizes Are Invalid");
				break;
			}
		}
		arr = arr.substr(0,arr.length-1);
		arr += " ]";
		data+="&arr="	+	arr;
		///////////////////////////// save main infomations /////////////////////////////////////////
		//var url = "presentation/costing/sample/priceCosting/addNew/priceCosting-db-set.php";
		var url = "controller.php?q=116";

		if(validNumber){
			var obj = $.ajax({
			url:url,
			dataType: "json",
			data:data,
			async:false,
			success:function(json){
					$('#WidthHeightPopup #butAdd').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$(e).parent().parent().find('#textitem_h3').val(json.con);
						return;
					}
					else
						hideWaiting();
				},
			error:function(xhr,status){
					hideWaiting();
				}		
			});
		}
	}
	calTotCostPrice();
}

function saveQtyPrices(){
    if ($('#qtyWisePricePopup').validationEngine('validate'))
    {
        var sampleNo    = $("#cboSampleNo").val();
        var sample_year = $("#cboYear").val();
        var revisionNo  = $("#cboRevisionNo").val();
        var comboName   = $("#cboCombo").val();
        var PrintName   = $("#cboPrint").val();
        var data 		= "requestType=saveQtyPrices";

        data+="&sampleNo=" +        sampleNo;
        data+="&sampleYear=" +		sample_year;
        data+="&revisionNo=" +	    revisionNo;
        data+="&combo=" +	    URLEncode(comboName);
        data+="&print=" +	    URLEncode(PrintName);
        //////////////////// saving main grid part //////////////////////////////////////////
        var rowCount = document.getElementById('tblCostingPricePopup').rows.length;
        var arr="[";
        var validNumber;
        var preUpperBound;
        var maximumBound=0;
        for(var i=1;i<rowCount-1;i++)
        {
            var lowerBound       = document.getElementById('tblCostingPricePopup').rows[i].cells[1].childNodes[0].value;
            var upperBound 		 = document.getElementById('tblCostingPricePopup').rows[i].cells[2].childNodes[0].value;
			var price	 	     = document.getElementById('tblCostingPricePopup').rows[i].cells[3].childNodes[0].value;
            maximumBound = upperBound;
			if (i > 1){
				preUpperBound 		 = document.getElementById('tblCostingPricePopup').rows[i-1].cells[2].childNodes[0].value;
				preUpperBound = parseInt(preUpperBound);
				preUpperBound = preUpperBound + 1;
            } else {
				preUpperBound = 0;
			}

			validNumber = true;
			lowerBound = parseInt(lowerBound);;
            if (lowerBound == preUpperBound) {
            	if (i == rowCount-2 &&  upperBound=='INF'){
					upperBound = -2
				}
                arr += "{";
                arr += '"lower_margin":"' + lowerBound + '",';
                arr += '"upper_margin":"' + upperBound + '",';
                arr += '"qty_price":"' + price + '"';
                arr += "},";
            }  else {
                validNumber = false;
                alert("Qty Prices Are Invalid");
                break;
            }
        }
        if (maximumBound != "INF"){
            validNumber = false;
            alert("Infinity Should Be There");
        }
        arr = arr.substr(0,arr.length-1);
        arr += " ]";
        data+="&arr="	+	arr;
        var url = "controller.php?q=116";

        if (validNumber){
            var obj = $.ajax({
                url:url,
                dataType: "json",
                data:data,
                async:false,
                success:function(json){
                    $('#qtyWisePricePopup #saveQtyPrices').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                    if(json.type=='pass')
                    {
						var t=setTimeout("alertx()",2000);
                        return;
                    }
                    else
                        hideWaiting();
                },
                error:function(xhr,status){
					$('#qtyWisePricePopup #saveQtyPrices').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
                }
            });
        }
    }
    calTotCostPrice();
}

function deletePrintSizes(e,thisObj){

	var rowCount1 = document.getElementById('tblSizesPopup').rows.length;
	if(rowCount1>=3)
	{
		//alert($(thisObj).parent().parent().html())
 		$(thisObj).parent().parent().remove();
		//Get deleting Row serial number
		var sampleNo    = $("#cboSampleNo").val();
		var sample_year = $("#cboYear").val();
		var revisionNo  = $("#cboRevisionNo").val();
		var comboName   = $("#ComboNamePopUp").html();
		var PrintName   = $("#printNamePopUp").html();
		var foilItem    = $("#itemName").html();
		var technique   = $("#techniquePopUp").text();
		var color   	= $("#colorPopUp").text();
		var serialNum_deleted = $(thisObj).closest('tr').find("#deleteImageSerialNo").html();
		var data = "requestType=DeleteSerialConsumption&serialNum_deleted="+serialNum_deleted+"&sampleNo="+sampleNo+"&sample_year="+sample_year+"&revisionNo="+revisionNo+"&comboName="+URLEncode(comboName)+"&PrintName="+URLEncode(PrintName)+"&foilItem="+foilItem+"&technique="+technique+"&color="+color;

		//Send deleting row data to db file
		var url = "presentation/customerAndOperation/sample/colorRecipes/addNew/colorRecipes-db-set.php";
		var obj = $.ajax({
			url:url,
			dataType: "json",
			data:data,
			async:false,
			success:function(json){
				if(json.type=='pass')
				{
					$(e).parent().parent().find('#textfield16R').val(json.con);
					alert(json.msg);
					//var t=setTimeout("alertx()",2000);
					return;
				}
			},
			error:function(xhr,status){
				//$('#frmColorRecipesWidthHeightPopup #butAdd').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				//var t=setTimeout("alertx()",3000);
				//function (xhr, status){errormsg(status)}
			}
		});
	}


}

function calculateConsumption(e){
	if ($('#WidthHeightPopup').validationEngine('validate')) {
		var foilWidth, foilHeight,consum_auto;
		$(e).parent().parent().parent().parent().each(function () {
			foilWidth 	= parseFloat($(this).find('#foilWidthPopUp').text());
			foilHeight 	= parseFloat($(this).find('#foilHeightPopUp').text());
			consum_auto = $(this).find('#consum_auto').text();
  		});
		$(e).closest('tr').find("input").each(function () {
			measurments.push(this.value);
		});

		var widthOne  = parseFloat(measurments[0]);
		var heightOne = parseFloat(measurments[1]);
		var conpc ;
	    var data = "requestType=getConsumption&foilWidth="+foilWidth+"&foilHeight="+foilHeight+"&widthOne="+widthOne+"&heightOne="+heightOne;
		//Send deleting row data to db file
		var url = "presentation/costing/sample/priceCosting/addNew/priceCosting-db-get.php";
		var obj = $.ajax({
			url:url,
			dataType: "json",
			data:data,
			async:false,
			success:function(json){
				if(json.type=='pass')
				{
					consum_auto=1;
					var units = json.val;
					 var pcs ;
					if (units>=0) {
					   pcs = units;
					}
					else {
					   pcs = 0;
					   //console.log(pcs);
					}
					if( pcs > 0 && consum_auto == 1){
						conpc = (foilWidth/ pcs);
						$(e).parent().parent().find("#consumption").find("#ConsumptionInput").val(conpc.toFixed(6));
					}
					else if (pcs > 0 && consum_auto == 0){
						conpc = 0;
						$(e).parent().parent().find("#consumption").find("#ConsumptionInput").val(conpc);
					}
					else if(pcs == 0){
						conpc = 0;
						$(e).parent().parent().find("#consumption").find("#ConsumptionInput").val(0);
					}
					var t=setTimeout("alertx()",2000);
					return;
				}
			},
			error:function(xhr,status){
			}
		});	
	}
   else {
		$(e).parent().parent().find("#consumption").find("#ConsumptionInput").val("");
	}
	measurments = [];
}

function getViewPage(e){
	var sampleNo    = $("#cboSampleNo").val();
	var sample_year = $("#cboYear").val();
	var revisionNo  = $("#cboRevisionNo").val();
	
	var foilWidth,foilHeight,itemName,technique;
	$('.specil_rm').each(function() {
		//alert($(e).parent().parent().find('.itemId').attr('id') );
		itemName   	= $(e).parent().parent().find('.itemId').attr('id');
		foilWidth  	= $(e).parent().parent().find('#textitem_w').val();
		foilHeight	= $(e).parent().parent().find('#textitem_h').val();
		technique 	= $(e).parent().parent().find('.technique_id').attr('id');
	});
	
	var comboName,printName,consumption;
	comboName = $('#cboCombo').val();
	printName = $('#cboPrint').val();
	$('.specil_rm').each(function() {	
		consumption = $(this).find('.consump_roll').value;
	});
	if(consumption != '') {
		window.open(basePath + 'auto_consumption.php?sampleNo=' + sampleNo + "&sample_year=" + sample_year + "&revisionNo=" + revisionNo + "&comboName=" + (comboName) + "&PrintName=" + (printName) + "&foilItem=" + itemName + "&foilWidth=" + foilWidth + "&foilHeight=" + foilHeight+"&technique="+technique);
	}
	else{
		alert("No Consumption For this Item");
	}
}

function getViewPageRepot(e){
	var sampleNo    = $("#txtsampleNo").text();
	var sample_year = $("#txtSmapleYear").text();
	var revisionNo  = $("#txtrevNo").text();
	
	var foilWidth,foilHeight,itemName,technique;
	$('.specil_rm').each(function() {
 		itemName   	= $(e).parent().parent().find('.itemId').attr('id');
		foilWidth  	= $(e).parent().parent().find('#textitem_w').text();
		foilHeight	= $(e).parent().parent().find('#textitem_h').text();
		technique 	= $(e).parent().parent().find('.technique_id').attr('id');
	});
	
	var comboName,printName,consumption;
	$('.headerLoad').each(function() {
		comboName = $(this).find('#txtcombo').text().trim();
		printName = $(this).find('#txtprint').text().trim();
		consumption = $(this).find('#textfield16').val();
	});
	if(consumption != '') {
		window.open(basePath + 'auto_consumption.php?sampleNo=' + sampleNo + "&sample_year=" + sample_year + "&revisionNo=" + revisionNo + "&comboName=" + (comboName) + "&PrintName=" + (printName) + "&foilItem=" + itemName + "&foilWidth=" + foilWidth + "&foilHeight=" + foilHeight+"&technique="+technique);
	}
	else{
		alert("No Consumption For this Item");
	}
}


function getSimilerViewPage(e){
	inkType 	= $(e).parent().parent().find('.inkType').attr('id');
	technique 	= $(e).parent().parent().find('.technique_getid').attr('id');
	popupWindow3('1');
		$('#popupContact1').load(basePath+"ViewSimiler.php?inkType=" + inkType+"&technique=" + technique, function(){		
		$('#butClose1').click(disablePopup);
		$('#butClose1').die('click').live('click',function(){
			//location.reload();
		});

	});
}
function viewCurrentSampleSheet(e)
{
	var sampleNo    = $("#cboSampleNo").val();
	var sample_year = $("#cboYear").val();
	var revisionNo  = $("#cboRevisionNo").val();
	
	window.open('?q=489&no=' + sampleNo + "&year=" + sample_year + "&revNo=" + revisionNo );
	
}
	
function deleteRows(table_id){
   var r =0;
	$('#'+table_id+' >tbody >tr').not(':first').each(function(){
		if(r>0){
 			$(this).remove();
		}
		r++;
	});
	
}

 function viewCurrentSampleSheetReport(e)
 {
	var sampleNo    = $("#txtsampleNo").text();
	var sample_year = $("#txtSmapleYear").text();
	var revisionNo  = $("#txtrevNo").text();
	window.open('?q=489&no=' + sampleNo + "&year=" + sample_year + "&revNo=" + revisionNo );
 }
	
function deleteRows(table_id){
   var r =0;
	$('#'+table_id+' >tbody >tr').not(':first').each(function(){
		if(r>0){
 			$(this).remove();
		}
		r++;
	});
	
}


 function saveCosting(){
 	
 		var value_o	=$('input[name="chkType"]:checked').val();
		var qtyPriceEnabled	=0;
		if ($("#enableQtyPrice").is(":checked")){
			qtyPriceEnabled = 1;
		}
	 //alert($('input[name="chkType"]:checked').val())
		var coppied =$('#copy').val();
		if(coppied=='')
			coppied=0;
		
		if((($('#cboLocation').val()=='' || $('#cboLocation').val()==0)) && (value_o!=1 && value_o!=4 ) && coppied ==0){
			alert("Please select the production location");
			return false;
		}
		
		if(value_o==3 && coppied==1)
			var value=1;
		else
			var value=value_o;
		//alert(value+'/'+value_o+'/'+coppied);
		if(value_o==1 )	
			var data = "requestType=copyCosting";
		else if((value_o==4))	
			var data = "requestType=copyCosting_old";
		else if((value_o==3) && coppied==1)	
			var data = "requestType=copyCosting";
		else if((value_o==3) && coppied==2)	
			var data = "requestType=copyCosting_old";
		else
			var data = "requestType=saveCosting";
		
			data+="&sampleNo="			+	$('#cboSampleNo').val();
			data+="&sampleYear="		+	$('#cboYear').val();
			data+="&revisionNo="		+	$('#cboRevisionNo').val();
			data+="&combo="				+	URLEncode($('#cboCombo').val());
			data+="&print="				+	URLEncode($('#cboPrint').val());
			data+="&qtyPriceEnabled="	+	qtyPriceEnabled;
			if(value==1 || coppied==1){
			data+="&sampleNoC="			+	$('#cboSampleNoC').val();
			data+="&sampleYearC="		+	$('#cboYearC').val();
			data+="&revisionNoC="		+	$('#cboRevisionNoC').val();
			data+="&comboC="			+	URLEncode($('#cboComboC').val());
			data+="&printC="			+	URLEncode($('#cboPrintC').val());
			}
			if(value==4 || coppied==2){
			data+="&sampleNoC_O="		+	$('#cboSampleNoC_O').val();
			data+="&sampleYearC_O="		+	$('#cboYearC_O').val();
			data+="&revisionNoC_O="		+	$('#cboRevisionNoC_O').val();
			data+="&comboC_O="			+	URLEncode($('#cboComboC_O').val());
			data+="&printC_O="			+	URLEncode($('#cboPrintC_O').val());
			}
			data+="&routing="			+	$('#cboRouting').val();
			data+="&colors="			+	$('.noOfColors').val();
			data+="&estimatedQty="		+	$('#textEstimated').val();
			data+="&prod_location="		+	$('#cboLocation').val();
			data+="&ups="				+	parseInt($('.noOfups').val());
			data+="&shots="				+	$('.shots').val();
			data+="&cost_per_shot="		+	$('.price_shots').val();
			data+="&inkCost="			+	$('.cls_tot_ink_cost').val();
			data+="&spRMCost="			+	$('.cls_tot_sp_cost').val();
			data+="&process_cost="		+	$('.cls_tot_process_cost').val();
			data+="&shot_cost="			+	$('.cls_tot_shot_cost').val();
			data+="&other_cost="		+	parseFloat($('.cls_other_cost').val());
			data+="&other_cost_remarks="+	$('.cls_other_cost_remarks').val();
			data+="&total_cost="		+	$('.cls_total_cost').val();
			data+="&margin_percentage="	+	$('.cls_margin').val();
			data+="&margin="			+   $('.cls_tot_cost_with_margin').val();
			data+="&screenline_price="	+	$('.cls_screenline_price').val();
			data+="&target_price="		+	$('.cls_target_price').val();
			data+="&approved_price="	+	$('.cls_approved_price').val();
	
	//ink type cost
		var err			  =0;
		var arrInkType    ='';
		$('#table_ink_type .inkCostPerSqInch').each(function(){
			
			var colour 				= $(this).parent().parent().find('.color').attr('id');
			var technique 			= $(this).parent().parent().find('.technique_getid').attr('id');
			var ink_type 			= $(this).parent().parent().find('.inkType').attr('id');
			var shots 				= $(this).parent().parent().find('.ink_shots').html();
			var cost_per_sq_inch 	= $(this).parent().parent().find('.inkCostPerSqInch').val();
			var square_inches 		= $(this).parent().parent().find('.inches').html();
			var usage 				= $(this).parent().parent().find('.usage').val();
			var total_cost 			= $(this).parent().parent().find('.totCost_ink_type').val();
		
			/*if(cost_per_sq_inch=='' || cost_per_sq_inch==0 || shots=='' || shots==0){
				if(value !=1)
				err	=1;
			}*/
			if(shots=='')
				shots=0;
			if(cost_per_sq_inch=='')
				cost_per_sq_inch=0;
				
			
			arrInkType +='{'+

				'"sampNo":"'+$('#cboSampleNo').val()+'",'+
				'"sampYear":"'+$('#cboYear').val()+'",'+
				'"rev":"'+$('#cboRevisionNo').val()+'",'+
				'"combo":"'+URLEncode($('#cboCombo').val())+'",'+
				'"print":"'+URLEncode($('#cboPrint').val())+'",'+
				'"colour":"'+(colour)+'",'+
				'"technique":"'+(technique)+'",'+
				'"ink_type":"'+(ink_type)+'",'+
				'"shots":"'+(shots)+'",'+
				'"cost_per_sq_inch":"'+(cost_per_sq_inch)+'",'+
				'"square_inches":"'+(square_inches)+'",'+
				'"usage":"'+(usage)+'",'+
				'"total_cost":"'+total_cost+'"'+
				'},';
  		
 		});
		if(err	== 1){
			alert("please enter number of shots and cost per square inch");
			return false;
		}
		
		 
		if(arrInkType!='' && value !=1   && (value_o!=1 && value_o!=4 ) && coppied ==0)
		{
		arrInkType = arrInkType.substr(0,arrInkType.length-1);
		data+="&arrInkType=["+    arrInkType+']'; 
		}
	
	//rm -roll cost
		var err			  =0;
		var arrRMroll    ='';
		$('#table_roll_form .price_roll').each(function(){
			var color	 			= $(this).parent().parent().find('.color_id').attr('id');
			var technique 			= $(this).parent().parent().find('.technique_id').attr('id');
			var item	 			= $(this).parent().parent().find('.itemId').attr('id');
			var printWidth_roll		= $(this).parent().parent().find('.printWidth_roll').val();
			var printHeight_roll 	= $(this).parent().parent().find('.printHeight_roll').val();
			var itemWidth 			= $(this).parent().parent().find('.itemWidth').val();
			var itemHeight 			= $(this).parent().parent().find('.itemHeight').val();
			var price_roll 			= $(this).parent().parent().find('.price_roll').val();
			var consump_roll		= $(this).parent().parent().find('.consump_roll').val();
			var cost_roll 			= $(this).parent().parent().find('.cost_roll').html();

			/*if(consump_roll=='' || consump_roll==0 || price_roll=='' || price_roll==0){
				if(value !=1   && (value_o!=1 && value_o!=4 ) && coppied ==0)
				err	=1;
			}*/
			if(consump_roll=='')
				consump_roll=0;
			if(price_roll=='')
				price_roll=0;
			
			arrRMroll +='{'+

				'"sampNo":"'+$('#cboSampleNo').val()+'",'+
				'"sampYear":"'+$('#cboYear').val()+'",'+
				'"rev":"'+$('#cboRevisionNo').val()+'",'+
				'"combo":"'+URLEncode($('#cboCombo').val())+'",'+
				'"print":"'+URLEncode($('#cboPrint').val())+'",'+
				'"color":"'+(color)+'",'+
				'"technique":"'+(technique)+'",'+
				'"item":"'+(item)+'",'+
				'"printWidth_roll":"'+(printWidth_roll)+'",'+
				'"printHeight_roll":"'+(printHeight_roll)+'",'+
				'"itemWidth":"'+(itemWidth)+'",'+
				'"itemHeight":"'+(itemHeight)+'",'+
				'"price_roll":"'+(price_roll)+'",'+
				'"consump_roll":"'+(consump_roll)+'",'+
				'"cost_roll":"'+cost_roll+'"'+
				'},';
  		
 		});
		if(err	== 1){
			alert("please enter consumption and prices for RM roll form");
			return false;
		}
		
		 
		if(arrRMroll!=''  && value !=1  && (value_o!=1 && value_o!=4 ) && coppied ==0)
		{
		arrRMroll = arrRMroll.substr(0,arrRMroll.length-1);
		data+="&arrRMroll=["+    arrRMroll+']'; 
		}

	//rm -non roll cost
		var err			  =0;
		var arrRMnonRoll    ='';
		$('#table_non_roll .price_nonRoll').each(function(){
			
			var color	 			= $(this).parent().parent().find('.color_id').attr('id');
			var technique 			= $(this).parent().parent().find('.technique').attr('id');
			var item	 			= $(this).parent().parent().find('.item').attr('id');
			var printWidth_roll		= $(this).parent().parent().find('.printWidth_nonRoll').val();
			var printHeight_roll 	= $(this).parent().parent().find('.printHeight_nonRoll').val();
			var price_roll 			= $(this).parent().parent().find('.price_nonRoll').val();
			var consump_roll		= $(this).parent().parent().find('.consump_nonRoll').val();
			var cost_roll 			= $(this).parent().parent().find('.cost_nonRoll').html();

			/*if(consump_roll=='' || consump_roll==0 || price_roll=='' || price_roll==0){

				if(value !=1    && (value_o!=1 && value_o!=4 ) && coppied ==0){
				err	=1;
				}
			}*/
			
			if(consump_roll=='')
				consump_roll=0;
			if(price_roll=='')
				price_roll=0;
			
			arrRMnonRoll +='{'+

				'"sampNo":"'+$('#cboSampleNo').val()+'",'+
				'"sampYear":"'+$('#cboYear').val()+'",'+
				'"rev":"'+$('#cboRevisionNo').val()+'",'+
				'"combo":"'+URLEncode($('#cboCombo').val())+'",'+
				'"print":"'+URLEncode($('#cboPrint').val())+'",'+
				'"color":"'+(color)+'",'+
				'"technique":"'+(technique)+'",'+
				'"item":"'+(item)+'",'+
				'"printWidth_roll":"'+(printWidth_roll)+'",'+
				'"printHeight_roll":"'+(printHeight_roll)+'",'+
				'"price_roll":"'+(price_roll)+'",'+
				'"consump_roll":"'+(consump_roll)+'",'+
				'"cost_roll":"'+cost_roll+'"'+
				'},';
  		
 		});
		if(err	== 1){
			alert("please enter consumption and prices for RM non-roll form");
			return false;
		}
		
		 
		if(arrRMnonRoll!=''  && value !=1   && (value_o!=1 && value_o!=4 ) && coppied ==0)
		{
		arrRMnonRoll = arrRMnonRoll.substr(0,arrRMnonRoll.length-1);
		data+="&arrRMnonRoll=["+    arrRMnonRoll+']'; 
		}
	
	//rm -non direct cost
		var err			  	=0;
		var arrRMnonDirect	='';
		$('#table_non_direct .price_nonDirect').each(function(){
			
			var item	 			= $(this).parent().parent().find('.item').attr('id');
			var printWidth_roll		= $(this).parent().parent().find('.printWidth_nonDirect').val();
			var printHeight_roll 	= $(this).parent().parent().find('.printHeight_nonDirect').val();
			var price_roll 			= $(this).parent().parent().find('.price_nonDirect').val();
			var consump_roll		= $(this).parent().parent().find('.cons_nonDirect').val();
			var cost_roll 			= $(this).parent().parent().find('.cost_nonDirect').html();

			/*if(consump_roll=='' || consump_roll==0 || price_roll=='' || price_roll==0){
				if(coppied !=1 && value !=1   && (value_o!=1 && value_o!=4 ) && coppied ==0)
				err	=1;
			}*/
		
			if(consump_roll=='')
				consump_roll=0;
			if(price_roll=='')
				price_roll=0;
		
			arrRMnonDirect +='{'+

				'"sampNo":"'+$('#cboSampleNo').val()+'",'+
				'"sampYear":"'+$('#cboYear').val()+'",'+
				'"rev":"'+$('#cboRevisionNo').val()+'",'+
				'"combo":"'+URLEncode($('#cboCombo').val())+'",'+
				'"print":"'+URLEncode($('#cboPrint').val())+'",'+
				'"item":"'+(item)+'",'+
				'"printWidth_roll":"'+(printWidth_roll)+'",'+
				'"printHeight_roll":"'+(printHeight_roll)+'",'+
				'"price_roll":"'+(price_roll)+'",'+
				'"consump_roll":"'+(consump_roll)+'",'+
				'"cost_roll":"'+cost_roll+'"'+
				'},';
  		
 		});
		if(err	== 1){
			alert("please enter consumption and prices for non-direct items");
			return false;
		}
		
		 
		if(arrRMnonDirect!=''  && value !=1   && (value_o!=1 && value_o!=4 ) && coppied ==0)
		{
		arrRMnonDirect = arrRMnonDirect.substr(0,arrRMnonDirect.length-1);
		data+="&arrRMnonDirect=["+    arrRMnonDirect+']'; 
		}
	
	//rocess cost
		var err			  	=0;
		var arrProcess		='';
		$('#table_process_cost .pocess_price').each(function(){
			if($(this).parent().parent().find('.chkProcess').is(':checked')){
			
			var process 			= $(this).parent().parent().find('.procss').attr('id');
			var order				= $(this).parent().parent().find('.order').attr('id');
			var price	 			= $(this).parent().parent().find('.pocess_price').val();
			//alert($(this).parent().parent().find('.chkProcess').is(':checked'));
			if(price=='' || price==0){
				if(coppied !=1 && value !=1   && (value_o!=1 && value_o!=4 ) && coppied ==0)
				err	=1;
			}
			arrProcess +='{'+

				'"sampNo":"'+$('#cboSampleNo').val()+'",'+
				'"sampYear":"'+$('#cboYear').val()+'",'+
				'"rev":"'+$('#cboRevisionNo').val()+'",'+
				'"combo":"'+URLEncode($('#cboCombo').val())+'",'+
				'"print":"'+URLEncode($('#cboPrint').val())+'",'+
				'"process":"'+(process)+'",'+
				'"order":"'+(order)+'",'+
				'"price":"'+price+'"'+
				'},';
			}
  		
 		});
		
		if(err	== 1){
			alert("please enter prices for selected processes");
			return false;
		}
		
		 
		if(arrProcess!=''  && value !=1    && (value_o!=1 && value_o!=4 ) && coppied ==0)
		{
		arrProcess = arrProcess.substr(0,arrProcess.length-1);
		data+="&arrProcess=["+    arrProcess+']'; 
		}
	
	
		var url = "controller.php?q=116";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json", 
			type:'POST', 
			data:data, 
			async:false,
			
			success:function(json){

					
					$('#frmSampleCostSheet #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					
					if(json.type=='pass')
					{
						//alert("hhhhhhhhhhh");
						window.location.href ="?q=116&graphicNo="+URLEncode($('#cboStyleNo').val())
											+'&sampleNo='+$('#cboSampleNo').val()	
											+'&sampleYear='+$('#cboYear').val()	
											+'&revNo='+$('#cboRevisionNo').val()	
											+'&combo='+URLEncode($('#cboCombo').val())	
											+'&printName='+$('#cboPrint').val();
						var x=0;
						return;
					}
					else
						hideWaiting();
				},
			error:function(xhr,status){
					hideWaiting();
				}		
			});
		
}

$('#frmSampleCostSheet #butReport').die('click').live('click',function(){
	if($('#frmSampleCostSheet #cboSampleNo').val()!=''){
		
		window.open('?q='+reportID+'&graphicNo='+URLEncode($('#frmSampleCostSheet #cboStyleNo').val())+'&sampleyear='+$('#frmSampleCostSheet #cboYear').val()+'&sampleNo='+$('#frmSampleCostSheet #cboSampleNo').val()+'&revNo='+$('#frmSampleCostSheet #cboRevisionNo').val() +'&combo='+URLEncode($('#frmSampleCostSheet #cboCombo').val())+'&print='+($('#frmSampleCostSheet #cboPrint').val()),'priceCosting-report.php');	
	}
	else
	{
		$('#frmSampleCostSheet #butReport').validationEngine('showPrompt','No Sample no to view Report','fail');
		return;
	}
});

function approve(){

	if($('#frmSampleCostSheet #cboSampleNo').val()!=''){
		window.open('?q='+reportID+'&graphicNo='+URLEncode($('#frmSampleCostSheet #cboStyleNo').val())+'&sampleyear='+$('#frmSampleCostSheet #cboYear').val()+'&sampleNo='+$('#frmSampleCostSheet #cboSampleNo').val()+'&revNo='+$('#frmSampleCostSheet #cboRevisionNo').val() +'&combo='+URLEncode($('#frmSampleCostSheet #cboCombo').val())+'&print='+($('#frmSampleCostSheet #cboPrint').val()) + '&approveMode=confirm','priceCosting-report.php');	
		
	}
	else
	{
		$('#frmSampleCostSheet #butReport').validationEngine('showPrompt','No Sample no to view Report','fail');
		return;
	}
	
	
}

function urlApprove(){
	var val = $.prompt('Are you sure you want to approve this Pre costing ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "controller.php?q="+reportID+window.location.search+'&requestType=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmSampleCostSheetRpt #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("c()",10000);
									window.location.href = window.location.href;
									window.opener.location.reload();
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSampleCostSheetRpt #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertxR1()",3000);
								return;
							}		
						});
					hideWaiting();
					}				
			}});
	
}

function urlReject()
{
	var val = $.prompt('Are you sure you want to reject this Pre costing?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "controller.php?q="+reportID+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,						
						success:function(json){
								$('#frmSampleCostSheetRpt #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertxR2()",10000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){								
								$('#frmSampleCostSheetRpt #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertxR2()",10000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}

function urlReject_approved()
{
	var val = $.prompt('Are you sure you want to reject this Pre costing?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "controller.php?q="+reportID+window.location.search+'&requestType=urlReject_approved';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,						
						success:function(json){
								$('#frmSampleCostSheetRpt #butRptReject_approved').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertxR2()",10000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){								
								$('#frmSampleCostSheetRpt #butRptReject_approved').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertxR2()",10000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}

function urlReject_approved_cp()
{
	var val = $.prompt('Are you sure you want to reject this Pre costing?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "controller.php?q="+reportID+window.location.search+'&requestType=urlReject_approved_cp';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,						
						success:function(json){
								$('#frmSampleCostSheetRpt #butRptReject_approved_cp').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertxR2()",10000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){								
								$('#frmSampleCostSheetRpt #butRptReject_approved_cp').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertxR2()",10000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}

function coppyTo(){
	
	popupWindow3('1');
		$('#popupContact1').load(basePath+'CoppyTo.php?sampleyear=' +$('#frmSampleCostSheet #cboYear').val() + '&sampleNo=' +$('#frmSampleCostSheet #cboSampleNo').val() + '&revNo='+$('#frmSampleCostSheet #cboRevisionNo').val() + '&combo='+URLEncode($('#frmSampleCostSheet #cboCombo').val()) + '&print=' +URLEncode($('#frmSampleCostSheet #cboPrint').val()), function(){		
		$('#butClose1').click(disablePopup);
		$('#butClose1').die('click').live('click',function(){
			//location.reload();
		});

	});	
}

function cappyTosave(){
		
			var sample_year 		= $('#frmSampleCostSheet #cboYear').val();
			var sampleNo			= $('#frmSampleCostSheet #cboSampleNo').val();
			var revisionNo 			= $('#frmSampleCostSheet #cboRevisionNo').val();
			var comboName			= $('#frmSampleCostSheet #cboCombo').val();
			var PrintName			= $('#frmSampleCostSheet #cboPrint').val();			
			var data = "requestType=copyConsumption_same_revision";
			
			data+="&sampleNo=" +        sampleNo;
			data+="&sample_year=" +		sample_year;
			data+="&revisionNo=" +	    revisionNo;
			data+="&comboName=" +	    URLEncode(comboName);
			data+="&PrintName=" +	    URLEncode(PrintName);
			
			var arr 	= '';
	$('#tblcoppyTo .combo_print').each(function(){
			if($(this).find('.chkCoppyTo').is(':checked')){
			arr +='{'+
				'"combo":"'+URLEncode($(this).find('.txtCombo').html())+'",'+
				'"print":"'+URLEncode($(this).find('.txtPrint').html())+'"'+
				'},';
			}
  		
 		});
		if(arr!='')
		{
		arr = arr.substr(0,arr.length-1);
		data+="&arr=["+    arr+']'; 
		}
		var url = "controller.php?q=116";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json", 
			type:'POST', 
			data:data, 
			async:false,
			
			success:function(json){
					$('#tblcoppyTo #butAdd').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						alert(json.msg);
						return;
					}
					else
						hideWaiting();
				},
			error:function(xhr,status){
					hideWaiting();
				}		
			});
		
}

function cappyToApprove(){
			var sample_year 		= $('#frmSampleCostSheet #cboYear').val();
			var sampleNo			= $('#frmSampleCostSheet #cboSampleNo').val();
			var revisionNo 			= $('#frmSampleCostSheet #cboRevisionNo').val();			
			var data 	= "requestType=approve_coppied_same_revision";
			var arr 	= '';
	if ($('#coppyToPopup').validationEngine('validate')){   

	$('#tblcoppyTo .combo_print').each(function(){
			if($(this).find('.chkCoppyTo').is(':checked')){
			arr +='{'+
				'"sample_year":"'+sample_year+'",'+
				'"sampleNo":"'+sampleNo+'",'+
				'"revisionNo":"'+revisionNo+'",'+
				'"combo":"'+URLEncode($(this).find('.txtCombo').html())+'",'+
				'"print":"'+URLEncode($(this).find('.txtPrint').html())+'"'+
				'},';
			}
  		
 		});
		if(arr!='')
		{
		arr = arr.substr(0,arr.length-1);
		data+="&arr=["+    arr+']'; 
		}
		var url = "controller.php?q=116";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json", 
			type:'POST', 
			data:data, 
			async:false,
			
			success:function(json){
					$('#tblcoppyTo #butApprove').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					alert(json.msg);
					
						hideWaiting();
				},
			error:function(xhr,status){
					hideWaiting();
				}		
			});
	}
		
}

function cappyToReject(){
			var sample_year 		= $('#frmSampleCostSheet #cboYear').val();
			var sampleNo			= $('#frmSampleCostSheet #cboSampleNo').val();
			var revisionNo 			= $('#frmSampleCostSheet #cboRevisionNo').val();			
			var data 	= "requestType=reject_coppied_same_revision";
			var arr 	= '';
	if ($('#coppyToPopup').validationEngine('validate')){   

	$('#tblcoppyTo .combo_print').each(function(){
			if($(this).find('.chkCoppyTo').is(':checked')){
			arr +='{'+
				'"sample_year":"'+sample_year+'",'+
				'"sampleNo":"'+sampleNo+'",'+
				'"revisionNo":"'+revisionNo+'",'+
				'"combo":"'+URLEncode($(this).find('.txtCombo').html())+'",'+
				'"print":"'+URLEncode($(this).find('.txtPrint').html())+'"'+
				'},';
			}
  		
 		});
		if(arr!='')
		{
		arr = arr.substr(0,arr.length-1);
		data+="&arr=["+    arr+']'; 
		}
		var url = "controller.php?q=116";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json", 
			type:'POST', 
			data:data, 
			async:false,
			
			success:function(json){
					$('#tblcoppyTo #butApprove').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					alert(json.msg);
						hideWaiting();
				},
			error:function(xhr,status){
					hideWaiting();
				}		
			});
	}
		
} 


function alertxR1()
{
	$('#frmSampleCostSheetRpt #butRptConfirm').validationEngine('hide')	;
}
function alertxR2()
{
	$('#frmSampleCostSheetRpt #butRptReject').validationEngine('hide')	;
}

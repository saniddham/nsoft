<?php
$backwardseperator = "../../../../";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PRICE APPROVAL</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />


</head>

<body>
<form id="frmCompanyDetails" name="frmCompanyDetails" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<div align="center">
		<div class="trans_layoutXL">		  <div class="trans_text">PRICE APPROVAL
		    <select name="select" id="select" style="width:140px">
		      <option>Pending</option>
		      <option>1st Approval</option>
		      <option>2nd Approval</option>
	        </select>
		  </div>
		  <table width="101%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="22" class="normalfnt">&nbsp;</td>
            <td width="22%">&nbsp;</td>
            <td width="17%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="11%" class="normalfnt">&nbsp;</td>
            <td width="15%">&nbsp;</td>
          </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><div style="width:auto;height:350px;overflow:scroll" >
          <table width="896" class="grid" >
            <tr class="gridHeader">
              <td width="106" height="22" >Style No</td>
              <td width="138" >Graphic</td>
              <td width="124" >Customer</td>
              <td width="95" >Date</td>
              <td width="93" >Graphic Size</td>
              <td width="56" >Qty</td>
              <td width="64">Total Cost</td>
              <td width="55">1st Approval</td>
              <td width="58">2nd Approval</td>
              <td width="63">View</td>
              </tr>
            <tr class="normalfnt">
              <td align="center" bgcolor="#FFFFFF">GTE-25214</td>
              <td bgcolor="#FFFFFF"><img src="../../../../images/sample_garment.png" width="100" height="92" /></td>
              <td bgcolor="#FFFFFF">Mas Holdings</td>
              <td bgcolor="#FFFFFF">2011-12-12</td>
              <td align="center" bgcolor="#FFFFFF">5 x 9</td>
              <td align="center" bgcolor="#FFFFFF">1</td>
              <td align="center" bgcolor="#FFFFFF">0.36</td>
              <td align="center" bgcolor="#FFFFFF">Manager</td>
              <td align="center" bgcolor="#FFFFFF"><a href="#">Approve</a></td>
              <td align="center" bgcolor="#FFFFFF"><a href="#">View</a></td>
              </tr>
            <tr class="normalfnt">
              <td align="center" bgcolor="#FFFFFF">GTE-25215</td>
              <td bgcolor="#FFFFFF"><img src="../../../../images/sample_garment2.jpg" width="100" height="75" /></td>
              <td bgcolor="#FFFFFF">Mas Holdings</td>
              <td bgcolor="#FFFFFF">2011-12-13</td>
              <td align="center" bgcolor="#FFFFFF">5 x 6</td>
              <td align="center" bgcolor="#FFFFFF">1</td>
              <td align="center" bgcolor="#FFFFFF">0.56</td>
              <td align="center" bgcolor="#FFFFFF"><a href="#">Approve</a></td>
              <td align="center" bgcolor="#FFFFFF"><a href="#">Approve</a></td>
              <td align="center" bgcolor="#FFFFFF"><a href="#">View</a></td>
            </tr>
            </table>
          </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><img src="../../../../images/Tclose.jpg" width="92" height="24" /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>

<?php
session_start();
$backwardseperator = "../../../";
$companyId = $_SESSION['headCompanyId'];
$locationId = $_SESSION['CompanyID'];

$userId 	= $_SESSION['userId'];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];

$programName='Place Order';
$programCode='P0427';

include  	"{$backwardseperator}dataAccess/Connector.php";

$orderNo 			= $_REQUEST['orderNo'];
$orderYear			= $_REQUEST['orderYear'];
$styleNo 			= $_REQUEST['styleNo'];
$salesOrderNo		= $_REQUEST['salesOrderNo'];
$graphic			= $_REQUEST['graphic'];

/*$orderNo = '500062';
$orderYear = '2013';
$styleNo ='P9716A3 '; 
$salesOrderNo = '110819';
$graphic = 'PYB-057';
*/
/*$orderNo = '1200031';
$orderYear = '2013';
$styleNo ='V0289XS '; 
$salesOrderNo = '120781';
$graphic = 'VV - 3968';*/

		$sql = "SELECT
					trn_orderheader.strCustomerPoNo,
					trn_orderheader.dtDate,
					trn_orderheader.dtDeliveryDate,
					mst_customer.strName AS strCustomer,
					mst_customer_locations_header.strName AS strCustomerLocations,
					mst_financecurrency.strCode AS currencyCode,
					mst_financecurrency.strSymbol as symbol,   
					trn_orderheader.intCurrency as currencyId, 
					trn_orderheader.strContactPerson,
					trn_orderheader.strRemark,
					A.strUserName AS marketer,
					trn_orderheader.intStatus,
					trn_orderheader.intReviseNo, 
					B.strUserName AS creator,
					intApproveLevelStart,
					mst_locations.intCompanyId,
					mst_locations.strName as poLocation, 
					trn_orderheader.intCreator
				FROM
					trn_orderheader
					Inner Join mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
					Inner Join mst_customer_locations_header ON mst_customer_locations_header.intId = trn_orderheader.intCustomerLocation
					Inner Join mst_financecurrency ON mst_financecurrency.intId = trn_orderheader.intCurrency
					Inner Join sys_users AS A ON A.intUserId = trn_orderheader.intMarketer
					Inner Join sys_users AS B ON B.intUserId = trn_orderheader.intCreator
					Inner Join mst_locations on mst_locations.intId = trn_orderheader.intLocationId 
				WHERE
					trn_orderheader.intOrderNo 		=  '$orderNo' AND
					trn_orderheader.intOrderYear 	=  '$orderYear'
					";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$intCreateCompanyId		= $row['intCompanyId'];
		$poLocationName		= $row['poLocation'];
		$intCreateUserId		= $row['intCreator'];
		$strCustomerPoNo 		= $row['strCustomerPoNo'];
		$dtDate 				= $row['dtDate'];
		$dtDeliveryDate 		= $row['dtDeliveryDate'];
		$strCustomer 			= $row['strCustomer'];
		$strCustomerLocations 	= $row['strCustomerLocations'];
		$currencyCode 			= $row['currencyCode'];
		$currencyId 			= $row['currencyId'];
		$symbol 				= $row['symbol'];
		$strContactPerson		= $row['strContactPerson'];
		$strRemark 				= $row['strRemark'];
		$marketer				= $row['marketer'];
		$intStatus				= $row['intStatus'];
		$creator				= $row['creator'];
		$intApproveLevelStart	= $row['intApproveLevelStart']-1;
		$savedLevels				= $row['intApproveLevelStart'];
		$reviseNo = $row['intReviseNo'];
//---------------------------------------
$sql = "SELECT
			mst_financeexchangerate.dblBuying, 
			mst_financecurrency.strCode as currency  
		FROM
			mst_financeexchangerate 
			Inner Join mst_financecurrency ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId
		WHERE
			mst_financeexchangerate.intCurrencyId =  '$currencyId' AND
			mst_financeexchangerate.intCompanyId =  '$company'
		ORDER BY
			mst_financeexchangerate.dtmDate DESC
		LIMIT 1
		";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
//$newExchRate=$row['dblBuying'];

?>
    <?php
//---------------------------------
	  	 $sql1 = "	SELECT   
						sum(trn_orderdetails.intQty) as orderQty 
						FROM
						trn_orderdetails
						WHERE
						trn_orderdetails.intOrderNo 	=  '$orderNo' AND
						trn_orderdetails.intOrderYear 	=  '$orderYear' ";
		if($styleNo!='')				
		$sql1 .= " AND trn_orderdetails.strStyleNo 	=  '$styleNo' ";
		if($graphic!='')				
		$sql1 .= " AND trn_orderdetails.strGraphicNo 	=  '$graphic' ";
		if($salesOrderNo!='')				
		$sql1 .= " AND trn_orderdetails.strSalesOrderNo 	=  '$salesOrderNo' ";  
		$sql1 .= " group by strStyleNo,strGraphicNo";
			$result1 = $db->RunQuery($sql1);
		$row=mysqli_fetch_array($result1);
		$orderQty=$row['orderQty'];
//---------------------------------
	  	 $sql1 = "	SELECT 
						sum(ware_fabricreceiveddetails.dblQty) as dblQty
						FROM
						trn_orderdetails
						Inner Join ware_fabricreceivedheader ON ware_fabricreceivedheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricreceivedheader.intOrderYear = trn_orderdetails.intOrderYear
						Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear 
						AND ware_fabricreceiveddetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
						WHERE
						trn_orderdetails.intOrderNo 	=  '$orderNo' AND
						trn_orderdetails.intOrderYear 	=  '$orderYear' ";
		if($styleNo!='')				
		$sql1 .= " AND trn_orderdetails.strStyleNo 	=  '$styleNo' ";
		if($graphic!='')				
		$sql1 .= " AND trn_orderdetails.strGraphicNo 	=  '$graphic' ";
		if($salesOrderNo!='')				
		$sql1 .= " AND trn_orderdetails.strSalesOrderNo 	=  '$salesOrderNo' ";  
		$sql1 .= " group by strStyleNo,strGraphicNo";
			$result1 = $db->RunQuery($sql1);
		$row=mysqli_fetch_array($result1);
		$recvdQty=$row['dblQty'];
//---------------------------------
	  	 $sql1 = "	SELECT 
						sum(ware_fabricdispatchdetails.dblSampleQty) as dblSampleQty,
						sum(ware_fabricdispatchdetails.dblGoodQty) as dblGoodQty,
						sum(ware_fabricdispatchdetails.dblEmbroideryQty) as dblEmbroideryQty,
						sum(ware_fabricdispatchdetails.dblPDammageQty) as dblPDammageQty,
						sum(ware_fabricdispatchdetails.dblSampleQty+ware_fabricdispatchdetails.dblGoodQty+ware_fabricdispatchdetails.dblEmbroideryQty+ware_fabricdispatchdetails.dblPDammageQty) as dispatchQty 
						FROM
						trn_orderdetails
						Inner Join ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear
						Inner Join ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
						WHERE
						trn_orderdetails.intOrderNo 	=  '$orderNo' AND
						trn_orderdetails.intOrderYear 	=  '$orderYear' ";
		if($styleNo!='')				
		$sql1 .= " AND trn_orderdetails.strStyleNo 	=  '$styleNo' ";
		if($graphic!='')				
		$sql1 .= " AND trn_orderdetails.strGraphicNo 	=  '$graphic' ";
		if($salesOrderNo!='')				
		$sql1 .= " AND trn_orderdetails.strSalesOrderNo 	=  '$salesOrderNo' ";  
		$sql1 .= " group by strStyleNo,strGraphicNo";
			$result1 = $db->RunQuery($sql1);
		$row=mysqli_fetch_array($result1);
		$dispatchQty=$row['dispatchQty'];
		$sampleQty=$row['dblSampleQty'];
		$goodQty=$row['dblGoodQty'];
		$embroideryQty=$row['dblEmbroideryQty'];
		$pDammageQty=$row['dblPDammageQty'];
//---------------------------------
	?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Post Costing Report</title>

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="rptBulkOrder-js.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>

<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>

<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:237px;
	top:175px;
	width:619px;
	height:235px;
	z-index:1;
}
.APPROVE {
	font-size: 16px;
	font-weight: bold;
	color: #00C;
}
</style>
</head>

<body>
<form id="frmSamplePlaceOrderReport" name="frmSamplePlaceOrderReport" method="post" action="rptBulkOrder.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>
<tr>
<td colspan="3"><div id="divPoNo" style="display:none"><?php echo $orderNo ?>/<?php echo $orderYear ?></div></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>POST COSTING</strong><strong></strong></div>
<table width="1200" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="8%" class="normalfnt">Order No</td>
    <td width="1%" align="center" valign="middle">:</td>
    <td width="17%"><span class="normalfnt"><?php echo $orderNo ?>/<?php echo $orderYear ?></span></td>
    <td width="9%" class="normalfnt">Date</td>
    <td width="1%" align="center" valign="middle">:</td>
    <td width="12%"><span class="normalfnt"><?php echo $dtDate ?></span></td>
    <td colspan="9" rowspan="5" class="normalfnt">
    <table width="100%" border="0" align="center" class="tableBorder">
  <tr>
    <td width="21%" class="normalfnt"><strong><strong>Order Qty</strong></strong></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
  <td width="14%" class="normalfnt"><strong><?php echo $orderQty; ?></strong></td>
  <td width="18%" class="normalfnt"><strong>Fabric In Qty</strong></td>
  <td width="2%"><span class="normalfnt"><strong>:</strong></span></td>
  <td width="11%"><span class="normalfnt">
    <strong><?php  echo $recvdQty; ?></strong>
  </span></td>
  <td width="23%" class="normalfnt"><strong><u>Tot Dispatch Qty</u></strong></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <?php
	
	$sql = "SELECT 

			IFNULL((SELECT SUM(CSD.dblQty*CSD.dblUnitPrice) 
			FROM fin_customer_salesinvoice_details CSD
			INNER JOIN fin_customer_salesinvoice_header CSH  ON CSH.intInvoiceNo=CSD.intInvoiceNo AND CSH.intAccPeriodId=CSD.intAccPeriodId AND
			CSH.intLocationId=CSD.intLocationId AND CSH.intCompanyId=CSD.intCompanyId AND CSH.strReferenceNo=CSD.strReferenceNo
			WHERE CSH.strPoNo=OH.strCustomerPoNo ),0) AS invoiceValue,
			
			IFNULL((SELECT SUM(dblPayAmount) FROM fin_customer_receivedpayments_main_details CRPMD
			INNER JOIN fin_customer_receivedpayments_header CRPH  ON CRPH.intReceiptNo=CRPMD.intReceiptNo AND 
			CRPH.intAccPeriodId=CRPMD.intAccPeriodId AND
			CRPH.intLocationId=CRPMD.intLocationId AND 
			CRPH.intCompanyId=CRPMD.intCompanyId AND 
			CRPH.strReferenceNo=CRPMD.strReferenceNo
			INNER JOIN fin_customer_salesinvoice_header CSH ON CSH.strReferenceNo=CRPMD.strDocNo
			WHERE CSH.strPoNo=OH.strCustomerPoNo AND CRPH.intDeleteStatus=0),0) AS paymentRecive
			
			FROM trn_orderheader OH
			INNER JOIN sys_users SU ON SU.intUserId=OH.intCreator
			INNER JOIN mst_locations ML ON ML.intId=OH.intLocationId
			INNER JOIN mst_companies MC ON ML.intCompanyId=MC.intId
			INNER JOIN mst_customer MCU ON MCU.intId=OH.intCustomer
			INNER JOIN mst_financecurrency FC ON FC.intId=OH.intCurrency
			WHERE OH.intOrderYear='$orderYear' AND
			OH.intOrderNo='$orderNo' ";
	
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	$invValue		= $row['invoiceValue'];
	$paymentRecive 	= $row['paymentRecive'];
	?>
    
    <td width="7%"><strong>
     <u> <?php  echo $recvdQty; ?></u>
    </strong></td>
  </tr>
  <tr>
    <td width="21%"><span class="normalfnt">Invoice Value</span></td>
    <td width="2%"><span class="normalfnt">:</span></td>
    <td width="14%"><span class="normalfnt">
      <?php  echo $invValue; ?>
    </span></td>
    <td>&nbsp;</td>
       <td></td>
       <td>&nbsp;</td>
       <td>Sample Qty</td>
    <td width="2%"><span class="normalfnt">:</span></td>
       <td   class="normalfnt"><?php  echo $sampleQty; ?></td>
  </tr>

  <tr>
    <td><span class="normalfnt">Payment Recived</span></td>
    <td><span class="normalfnt">:</span></td>
    <td   class="normalfnt"><?php echo $paymentRecive; ?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Good Qty</td>
    <td width="2%"><span class="normalfnt">:</span></td>
    <td><?php  echo $goodQty; ?></td>
  </tr>  
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td   class="normalfnt">&nbsp;</td>
    <td   class="normalfnt">&nbsp;</td>
    <td   class="normalfnt">&nbsp;</td>
    <td   class="normalfnt">&nbsp;</td>
    <td class="normalfnt">Embroidery Qty</td>
    <td width="2%"><span class="normalfnt">:</span></td>
    <td><?php  echo $embroideryQty; ?></td>
    </tr>
<tr>	

    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td class="normalfnt">PDammage Qty</td>
    <td width="2%"><span class="normalfnt">:</span></td>
    <td class="normalfnt"><?php  echo $pDammageQty; ?>
    </td>
    </tr>   
</table></td>
    <?php
	
	$sql = "SELECT 

			IFNULL((SELECT SUM(CSD.dblQty*CSD.dblUnitPrice) 
			FROM fin_customer_salesinvoice_details CSD
			INNER JOIN fin_customer_salesinvoice_header CSH  ON CSH.intInvoiceNo=CSD.intInvoiceNo AND CSH.intAccPeriodId=CSD.intAccPeriodId AND
			CSH.intLocationId=CSD.intLocationId AND CSH.intCompanyId=CSD.intCompanyId AND CSH.strReferenceNo=CSD.strReferenceNo
			WHERE CSH.strPoNo=OH.strCustomerPoNo ),0) AS invoiceValue,
			
			IFNULL((SELECT SUM(dblPayAmount) FROM fin_customer_receivedpayments_main_details CRPMD
			INNER JOIN fin_customer_receivedpayments_header CRPH  ON CRPH.intReceiptNo=CRPMD.intReceiptNo AND 
			CRPH.intAccPeriodId=CRPMD.intAccPeriodId AND
			CRPH.intLocationId=CRPMD.intLocationId AND 
			CRPH.intCompanyId=CRPMD.intCompanyId AND 
			CRPH.strReferenceNo=CRPMD.strReferenceNo
			INNER JOIN fin_customer_salesinvoice_header CSH ON CSH.strReferenceNo=CRPMD.strDocNo
			WHERE CSH.strPoNo=OH.strCustomerPoNo AND CRPH.intDeleteStatus=0),0) AS paymentRecive
			
			FROM trn_orderheader OH
			INNER JOIN sys_users SU ON SU.intUserId=OH.intCreator
			INNER JOIN mst_locations ML ON ML.intId=OH.intLocationId
			INNER JOIN mst_companies MC ON ML.intCompanyId=MC.intId
			INNER JOIN mst_customer MCU ON MCU.intId=OH.intCustomer
			INNER JOIN mst_financecurrency FC ON FC.intId=OH.intCurrency
			WHERE OH.intOrderYear='$orderYear' AND
			OH.intOrderNo='$orderNo' ";
	
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	$invValue		= $row['invoiceValue'];
	$paymentRecive 	= $row['paymentRecive'];
	?>
    
    <td width="6%" class="normalfnt">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Customer</td>
    <td align="center" valign="middle">:</td>
    <td><span class="normalfnt"><?php echo $strCustomer ?></span></td>
    <td width="9%" class="normalfnt">Delivery Date</td>
    <td width="1%" align="center" valign="middle">:</td>
    <td width="12%"><span class="normalfnt"><?php echo $dtDeliveryDate ?></span></td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Cust PO No</td>
    <td align="center" valign="middle">:</td>
    <td class="normalfnt"><?php echo $strCustomerPoNo; ?></td>
    <td><span class="normalfnt">Marketer</span></td>
    <td align="center" valign="middle">:</td>
    <td><span class="normalfnt"><?php echo $marketer;?></span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Currency</td>
    <td align="center" valign="middle">:</td>
    <td class="normalfnt"><?php echo $currencyCode; ?></td>
    <td class="normalfnt">PO Revise No</td>
    <td align="center" valign="middle">:</td>
    <td class="normalfnt"><?php echo $reviseNo; ?></td>
    <td   class="normalfnt">&nbsp;</td>
    
    </tr>
<tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Location</td>
    <td align="center" valign="middle">:</td>
    <td class="normalfnt"><?php echo $poLocationName; ?></td>
    <td class="normalfnt"></td>
    <td align="center" valign="middle">&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    </tr>  
</table>
  </td>
</tr>

<tr>
  <td>
    <table width="100%">
      <tr>
        <td colspan="7" class="normalfnt">
          <table width="506" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="">
              <th width="25%" >Style No</th>
              <th width="41%" >Graphic No</th>
              <th width="34%" >Graphic</th>
              </tr>
            <?php 
			$tmpSalesId='';
			$tmpGraphic='';
			$tmpSampleNo='';
			$tmpSampleYear='';
			$tmpPlacement='';
			$tmpCombo='';
			$tmpRevNo='';
			
	  	 $sql1 = "	SELECT   
						trn_orderdetails.strGraphicNo,
						trn_orderdetails.strStyleNo , 
						trn_orderdetails.intSampleNo,
						trn_orderdetails.intSampleYear,
						trn_orderdetails.intRevisionNo 
						FROM
						trn_orderdetails
						WHERE
						trn_orderdetails.intOrderNo 	=  '$orderNo' AND
						trn_orderdetails.intOrderYear 	=  '$orderYear' ";
		if($styleNo!='')				
		$sql1 .= " AND trn_orderdetails.strStyleNo 	=  '$styleNo' ";
		if($graphic!='')				
		$sql1 .= " AND trn_orderdetails.strGraphicNo 	=  '$graphic' ";
		if($salesOrderNo!='')				
		$sql1 .= " AND trn_orderdetails.strSalesOrderNo 	=  '$salesOrderNo' ";  
		$sql1 .= " group by strStyleNo,strGraphicNo";
		//echo $sql1;		
			$result1 = $db->RunQuery($sql1);
			$tmpStyle='';
			while($row=mysqli_fetch_array($result1))
			{
				$sampleNo=$row['intSampleNo'];
				$sampleYear=$row['intSampleYear'];
				$revNo=$row['intRevisionNo'];
			?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" >&nbsp;<?php if($tmpStyle!=$row['strStyleNo']){ echo $row['strStyleNo'];} ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strGraphicNo'] ?>&nbsp;</td>
              <td class="normalfnt" align="center" ><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:264px;height:148px;overflow:hidden" ><?php
			if($sampleNo!='')
			{
				echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"../../../documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revNo-1.png\" />";	
			}
			 ?></div></td>
              </tr>
            <?php   
			$tmpStyle=$row['strStyleNo'];
			}
	        ?>
            
            </table>
        </td>
        <td width="6%">&nbsp;</td>
        </tr>
    
    <td class="normalfnt" colspan="7" align="left"></td><td></td></tr>
<tr>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="">
              <th colspan="13" align="left"><b>DRM - Direct Raw Materials</b></th>
            </tr>            
            <tr class="">
              <th colspan="3">&nbsp;</th>
              <th colspan="4">Pre Costing</th>
              <th colspan="6" align="right" >Post Costing</th>
            </tr>            
            <tr class="">
              <th width="7%"  height="22">Style No</th>
              <th width="35%">Item</th>
              <th width="3%">UOM</th>
              <th width="6%">Sample ConPC</th>
              <th width="7%">Expected Consump</th>
              <th width="4%"> Rate <?php echo '('.$symbol.')' ?></th>
              <th width="7%">Expected Cost <?php echo '('.$symbol.')' ?></th>
              <th width="4%" >Bulk ConPC</th>
              <th width="6%" >Bulk Consump</th>
              <th width="6%" >Wastage</th>
              <th width="4%" > Rate <?php echo '('.$symbol.')' ?></th>
              <th width="5%" >Actual Cost <?php echo '('.$symbol.')' ?></th>
              <th width="6%" >Wastage Cost <?php echo '('.$symbol.')' ?></th>
              </tr>
            <?php
			$totPreConPC=0;
			$totPreConsump=0;
			$totPreCost=0;
			$totPostConPC=0;
			$totPostConsump=0;
			$totPostCost=0;
			$totWastage=0;
			$totWastageCost=0;
			?>
            <?php 
			//ink consumptions
			$result1 = getSampleInkConsumptions($orderNo,$orderYear,$currencyId,$styleNo,$salesOrderNo,$graphic);
			while($row=mysqli_fetch_array($result1))
			{
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" >&nbsp;<?php echo $styleNo ?>&nbsp;</td>
              <td class="normalfnt" ><?php echo $row['itemName'] ?>&nbsp;</td>
              <td class="normalfnt" ><?php echo $row['unitName'] ?></td>
              <td class="normalfntRight" align="right" ><?php echo $row['consum'] ?>&nbsp;</td>
              <td class="normalfntRight"  align="right"><?php echo round($row['consum']*$dispatchQty,4); ?></td>
              <td class="normalfntRight" align="right" ><?php echo round($row['dblLastPrice'],4); ?></td>
              <td class="normalfntRight"  align="right"><?php echo round($row['consum']*$dispatchQty*$row['dblLastPrice'],4); ?></td>
              <?php
				$result2 = getBulkConsumptions($orderNo,$orderYear,$currencyId,$styleNo,$salesOrderNo,$graphic,$row['intItem']);
				$row2=mysqli_fetch_array($result2);
			  ?>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['Consump']/$dispatchQty,6); ?>&nbsp;</td>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['Consump'],4) ?>&nbsp;</td>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['wastage'],4) ?>&nbsp;</td>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['Cost']/$row2['Consump'],4) ?>&nbsp;</td>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['Cost'],4) ?>&nbsp;</td>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['wastageCost'],4) ?>&nbsp;</td>
              </tr>
			  <?php
				$totPreConPC +=$row['consum'];
				$totPreConsump +=round($row['consum']*$dispatchQty,4);
				$totPreCost +=round($row['consum']*$dispatchQty*$row['dblLastPrice'],4);
				$totPostConPC +=round($row2['Consump']/$dispatchQty,6);
				$totPostConsump +=round($row2['Consump'],4);
				$totPostCost +=round($row2['Cost'],4);
				$totWastage +=round($row2['wastage'],4);
				$totWastageCost +=round($row2['wastageCost'],4);
			}
			?>
            <?php 
			//special RM consumptions
			$result1 = getSpecialRMConsumptions($orderNo,$orderYear,$currencyId,$styleNo,$salesOrderNo,$graphic);
			while($row=mysqli_fetch_array($result1))
			{
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" >&nbsp;<?php echo $styleNo ?>&nbsp;</td>
              <td class="normalfnt" ><?php echo $row['itemName'] ?>&nbsp;</td>
              <td class="normalfnt" ><?php echo $row['unitName'] ?></td>
              <td class="normalfntRight" align="right" ><?php echo $row['consum'] ?>&nbsp;</td>
              <td class="normalfntRight"  align="right"><?php echo round($row['consum']*$dispatchQty,4); ?></td>
              <td class="normalfntRight" align="right" ><?php echo round($row['dblLastPrice'],4); ?></td>
              <td class="normalfntRight"  align="right"><?php echo round($row['consum']*$dispatchQty*$row['dblLastPrice'],4); ?></td>
              <?php
				$result2 = getBulkConsumptions($orderNo,$orderYear,$currencyId,$styleNo,$salesOrderNo,$graphic,$row['intItem']);
				$row2=mysqli_fetch_array($result2);
			  ?>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['Consump']/$dispatchQty,6); ?>&nbsp;</td>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['Consump'],4) ?>&nbsp;</td>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['wastage'],4) ?>&nbsp;</td>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['Cost']/$row2['Consump'],4) ?>&nbsp;</td>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['Cost'],4) ?>&nbsp;</td>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['wastageCost'],4) ?>&nbsp;</td>
              </tr>
			  <?php
				$totPreConPC +=$row['consum'];
				$totPreConsump +=round($row['consum']*$dispatchQty,4);
				$totPreCost +=round($row['consum']*$dispatchQty*$row['dblLastPrice'],4);
				$totPostConPC +=round($row2['Consump']/$dispatchQty,6);
				$totPostConsump +=round($row2['Consump'],4);
				$totPostCost +=round($row2['Cost'],4);
				$totWastage +=round($row2['wastage'],4);
				$totWastageCost +=round($row2['wastageCost'],4);
			}
			?>
            <?php 
			//Foil consumptions
			$result1 = getFoilConsumptions($orderNo,$orderYear,$currencyId,$styleNo,$salesOrderNo,$graphic);
			while($row=mysqli_fetch_array($result1))
			{
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" >&nbsp;<?php echo $styleNo ?>&nbsp;</td>
              <td class="normalfnt" ><?php echo $row['itemName'] ?>&nbsp;</td>
              <td class="normalfnt" ><?php echo $row['unitName'] ?></td>
              <td class="normalfntRight" align="right" ><?php echo $row['consum'] ?>&nbsp;</td>
              <td class="normalfntRight"  align="right"><?php echo round($row['consum']*$dispatchQty,4); ?></td>
              <td class="normalfntRight" align="right" ><?php echo round($row['dblLastPrice'],4); ?></td>
              <td class="normalfntRight"  align="right"><?php echo round($row['consum']*$dispatchQty*$row['dblLastPrice'],4); ?></td>
              <?php
				$result2 = getBulkConsumptions($orderNo,$orderYear,$currencyId,$styleNo,$salesOrderNo,$graphic,$row['intItem']);
				$row2=mysqli_fetch_array($result2);
			  ?>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['Consump']/$dispatchQty,6); ?>&nbsp;</td>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['Consump'],4) ?>&nbsp;</td>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['wastage'],4) ?>&nbsp;</td>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['Cost']/$row2['Consump'],4) ?>&nbsp;</td>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['Cost'],4) ?>&nbsp;</td>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['wastageCost'],4) ?>&nbsp;</td>
              </tr>
			  <?php
				$totPreConPC +=$row['consum'];
				$totPreConsump +=round($row['consum']*$dispatchQty,4);
				$totPreCost +=round($row['consum']*$dispatchQty*$row['dblLastPrice'],4);
				$totPostConPC +=round($row2['Consump']/$dispatchQty,6);
				$totPostConsump +=round($row2['Consump'],4);
				$totPostCost +=round($row2['Cost'],4);
				$totWastage +=round($row2['wastage'],4);
				$totWastageCost +=round($row2['wastageCost'],4);
			}
			?>
            <tr class="normalfnt"  bgcolor="#F9F9F9">
              <td class="normalfnt" >&nbsp;&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" ></td>
              <td class="normalfntRight" ><?php echo $totPreConPC; ?>&nbsp;</td>
              <td class="normalfntRight" ><?php echo round($totPreConsump,4); ?></td>
              <td class="normalfnt" ></td>
              <td class="normalfntRight" ><b><?php echo round($totPreCost,4); ?></b></td>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($totPostConPC,6); ?>&nbsp;</td>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($totPostConsump,4) ?>&nbsp;</td>
              <td class="normalfntRight" align="right" >&nbsp;<?php echo round($totWastage,4) ?>&nbsp;</td>
              <td class="normalfntRight" align="right" >&nbsp;&nbsp;</td>
              <td class="normalfntRight" align="right" >&nbsp;<b><?php echo round($totPostCost,4) ?></b>&nbsp;</td>
              <td class="normalfntRight" align="right" >&nbsp;<b><?php echo round($totWastageCost,4) ?></b>&nbsp;</td>
              </tr>
            </table>
        </td>
        <td width="6%">&nbsp;</td>
        </tr>
            
        <tr>
        <td colspan="7" class="normalfnt"><table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="">
              <th colspan="7" align="left"><b>Direct Labour Cost</b></th>
            </tr> 
            </table>           
            </td>
         <td></td>
        </tr>

        <tr>
        <td colspan="7" class="normalfnt"><table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="">
              <th colspan="7" align="left"><b>Overhead Consumption</b></th>
            </tr>            
            <tr class="">
              <th width="4%"  height="22">&nbsp;</th>
              <th width="33%">Type</th>
              <th width="20%">Expense up to Oct'09</th>
              <th width="10%">No'of months</th>
              <th width="11%">Avg. expense</th>
              <th width="11%"> Avg Production</th>
              <th width="11%">Cost per unit </th>
            </tr>
            <?php
			$result1 = getOverheadConsumptions();
			$i=0;
			while($row=mysqli_fetch_array($result1))
			{
				$i++;
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" >&nbsp;<?php echo $i ?>&nbsp;</td>
              <td class="normalfnt" ><?php echo $row['strName'] ?>&nbsp;</td>
              <td class="normalfnt" ><?php echo $row['xxx'] ?></td>
              <td class="normalfntRight" align="right" ><?php echo $row['xxx'] ?>&nbsp;</td>
              <td class="normalfntRight"  align="right"><?php echo round($row['xxx'],4); ?></td>
              <td class="normalfntRight" align="right" ><?php echo round($row['xxx'],4); ?></td>
              <td class="normalfntRight"  align="right"><?php echo round($row['xxx'],4); ?></td>
              </tr>
			  <?php
			}
			?>
            <tr class="normalfnt"  bgcolor="#F9F9F9">
              <td class="normalfnt" >&nbsp;&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" ></td>
              <td class="normalfntRight" ><?php echo $xxx; ?>&nbsp;</td>
              <td class="normalfntRight" ><?php echo round($xxx,4); ?></td>
              <td class="normalfnt" ></td>
              <td class="normalfntRight" ><b><?php echo round($xxx,4); ?></b></td>
              </tr>
            </table></td>
        <td></td>
        </tr>
            
      </table>
    </td>
</tr>

<tr height="40" >
  <td align="center" class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></td>
  <td align="center" class="normalfntMid">&nbsp;</td>
</tr>
                </table>
        
</form>
</body>
</html>
<?php
	//--------------------------------------------------------------
function getSampleInkConsumptions($orderNo,$orderYear,$currencyId,$styleNo,$salesOrderNo,$graphic)
{
		global $db;
if($styleNo!='')
	$para .= " AND trn_orderdetails.strStyleNo='$styleNo'";
if($graphic!='')
	$para .= " AND trn_orderdetails.strGraphicNo='$graphic'";
if($salesOrderNo!='')
	$para .= " AND trn_orderdetails.strSalesOrderNo='$salesOrderNo'";
	
$sql = "select mainId,mainCat,subId,subCat, intItem,itemName ,unitId,unitName

,sum(round(dblColorWeight/sumWeight*dblWeight /dblNoOfPcs,6))as consum
,sum(round(dblColorWeight/sumWeight*dblWeight * (intQty) /dblNoOfPcs,6)) as totalQty 
,dblLastPrice 
from

(


SELECT
trn_orderdetails.intOrderNo,
round(((mst_item.dblLastPrice*mst_financeexchangerate.dblBuying)/(SELECT
			mst_financeexchangerate.dblBuying 
		FROM
			mst_financeexchangerate 
			Inner Join mst_financecurrency ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId
		WHERE
			mst_financeexchangerate.intCurrencyId =  '$currencyId' AND
			mst_financeexchangerate.intCompanyId =  mst_locations.intCompanyId AND
			mst_financeexchangerate.dtmDate =  trn_sampleinfomations.dtDate
)),4) as dblLastPrice,
trn_orderdetails.intOrderYear,trn_orderdetails.strSalesOrderNo,trn_orderdetails.intSalesOrderId,
trn_orderdetails.intSampleNo,trn_orderdetails.intSampleYear,trn_orderdetails.strCombo,
trn_orderdetails.strPrintName,trn_orderdetails.intRevisionNo,
trn_orderdetails.intPart,
trn_orderdetails.intQty,
trn_sample_color_recipes.intItem,
trn_sample_color_recipes.dblWeight,
trn_sampleinfomations_details_technical.intInkTypeId,
trn_sampleinfomations_details_technical.dblColorWeight,
trn_sample_color_recipes.intTechniqueId,
mst_item.strName itemName,
mst_units.strName as unitName,
mst_units.intId as unitId,
mst_units.dblNoOfPcs ,
mst_maincategory.strName AS mainCat,mst_maincategory.intId AS mainId,
mst_subcategory.strName AS subCat,mst_subcategory.intId AS subId,



(SELECT
Sum(trn_sample_color_recipes.dblWeight) AS sumWeight
FROM trn_sample_color_recipes
WHERE
trn_sample_color_recipes.intSampleNo = trn_orderdetails.intSampleNo AND
trn_sample_color_recipes.intSampleYear = trn_orderdetails.intSampleYear AND
trn_sample_color_recipes.intRevisionNo =  trn_orderdetails.intRevisionNo AND
trn_sample_color_recipes.strCombo =  trn_orderdetails.strCombo AND
trn_sample_color_recipes.strPrintName =  trn_orderdetails.strPrintName AND
trn_sample_color_recipes.intTechniqueId =  trn_sample_color_recipes.intTechniqueId AND
trn_sample_color_recipes.intInkTypeId =  trn_sampleinfomations_details_technical.intInkTypeId AND
trn_sample_color_recipes.intColorId =  trn_sampleinfomations_details_technical.intColorId 
 )  as sumWeight 


FROM
trn_orderdetails 
INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_orderdetails.intRevisionNo AND trn_sampleinfomations.strGraphicRefNo = trn_orderdetails.strGraphicNo
Inner Join trn_sample_color_recipes ON trn_sample_color_recipes.intSampleNo = trn_orderdetails.intSampleNo AND trn_sample_color_recipes.intSampleYear = trn_orderdetails.intSampleYear AND trn_sample_color_recipes.intRevisionNo = trn_orderdetails.intRevisionNo AND trn_sample_color_recipes.strCombo = trn_orderdetails.strCombo AND trn_sample_color_recipes.strPrintName = trn_orderdetails.strPrintName
Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = trn_sample_color_recipes.intSampleNo AND trn_sampleinfomations_details_technical.intSampleYear = trn_sample_color_recipes.intSampleYear AND trn_sampleinfomations_details_technical.intRevNo = trn_sample_color_recipes.intRevisionNo AND trn_sampleinfomations_details_technical.strComboName = trn_sample_color_recipes.strCombo AND trn_sampleinfomations_details_technical.strPrintName = trn_sample_color_recipes.strPrintName AND trn_sampleinfomations_details_technical.intColorId = trn_sample_color_recipes.intColorId AND trn_sampleinfomations_details_technical.intInkTypeId = trn_sample_color_recipes.intInkTypeId
Inner Join mst_item ON mst_item.intId = trn_sample_color_recipes.intItem
Inner Join mst_locations ON trn_sampleinfomations.intCompanyId = mst_locations.intId 
left Join mst_financeexchangerate ON mst_item.intCurrency = mst_financeexchangerate.intCurrencyId AND trn_sampleinfomations.dtDate = mst_financeexchangerate.dtmDate AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId 
Inner Join mst_units ON mst_item.intUOM = mst_units.intId
Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
WHERE
trn_orderdetails.intOrderNo =  '$orderNo' AND
trn_orderdetails.intOrderYear =  '$orderYear' AND 
trn_orderdetails.intOrderNo > 0 
$para
) as t 

group by

 mainId,mainCat,subId,SubCat, intItem,itemName 
";
//echo $sql;

		return $result = $db->RunQuery($sql);
	}
	//--------------------------------------------------------------
	function getSpecialRMConsumptions($orderNo,$orderYear,$currencyId,$styleNo,$salesOrderNo,$graphic)
	{
		
		if($styleNo!='')
			$para .= " AND trn_orderdetails.strStyleNo='$styleNo'";
		if($graphic!='')
			$para .= " AND trn_orderdetails.strGraphicNo='$graphic'";
		if($salesOrderNo!='')
			$para .= " AND trn_orderdetails.strSalesOrderNo='$salesOrderNo'";
		
		global $db;
		   $sql = "SELECT
	mst_maincategory.intId AS mainId,
	mst_maincategory.strName AS mainCat,
	mst_subcategory.intId AS subId,
	mst_subcategory.strName AS subCat,
	mst_item.intId AS itemId,
	mst_item.strName AS itemName,
	mst_units.intId AS unitId,
	mst_units.strName AS unitName,
	sum(trn_sample_spitem_consumption.dblQty/dblNoOfPcs) as consum,
	mst_units.dblNoOfPcs,
	round(((mst_item.dblLastPrice*mst_financeexchangerate.dblBuying)/(SELECT
			mst_financeexchangerate.dblBuying 
		FROM
			mst_financeexchangerate 
			Inner Join mst_financecurrency ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId
		WHERE
			mst_financeexchangerate.intCurrencyId =  '$currencyId' AND
			mst_financeexchangerate.intCompanyId =  mst_locations.intCompanyId AND
			mst_financeexchangerate.dtmDate =  trn_sampleinfomations.dtDate
	)),4) as dblLastPrice,
	mst_financecurrency.strCode AS currencyName,
	sum((trn_orderdetails.intQty) * trn_sample_spitem_consumption.dblQty /dblNoOfPcs) as totalQty
FROM
	trn_orderdetails 
	INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_orderdetails.intRevisionNo AND trn_sampleinfomations.strGraphicRefNo = trn_orderdetails.strGraphicNo
	Inner Join trn_sample_spitem_consumption ON trn_sample_spitem_consumption.intSampleNo = trn_orderdetails.intSampleNo AND trn_sample_spitem_consumption.intSampleYear = trn_orderdetails.intSampleYear AND trn_sample_spitem_consumption.intRevisionNo = trn_orderdetails.intRevisionNo AND trn_sample_spitem_consumption.strCombo = trn_orderdetails.strCombo AND trn_sample_spitem_consumption.strPrintName = trn_orderdetails.strPrintName
	Inner Join mst_item ON mst_item.intId = trn_sample_spitem_consumption.intItem
	Inner Join mst_locations ON trn_sampleinfomations.intCompanyId = mst_locations.intId 
	left Join mst_financeexchangerate ON mst_item.intCurrency = mst_financeexchangerate.intCurrencyId AND trn_sampleinfomations.dtDate = mst_financeexchangerate.dtmDate AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId 
	Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
	Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
	Inner Join mst_units ON mst_units.intId = mst_item.intUOM
	Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_item.intCurrency
WHERE
	trn_orderdetails.intOrderNo =  '$orderNo' AND
	trn_orderdetails.intOrderYear =  '$orderYear'
$para
GROUP BY
	mst_maincategory.intId,
	mst_subcategory.intId,
	mst_item.intId";
	//echo $sql;

		return $result = $db->RunQuery($sql);
	}
	//--------------------------------------------------------------
	function getFoilConsumptions($orderNo,$orderYear,$currencyId,$styleNo,$salesOrderNo,$graphic)
	{
		
		if($styleNo!='')
			$para .= " AND trn_orderdetails.strStyleNo='$styleNo'";
		if($graphic!='')
			$para .= " AND trn_orderdetails.strGraphicNo='$graphic'";
		if($salesOrderNo!='')
			$para .= " AND trn_orderdetails.strSalesOrderNo='$salesOrderNo'";
		
		global $db;
		   $sql = "SELECT
		mst_item.intId AS itemId,
		mst_maincategory.strName AS mainCat,
		mst_subcategory.strName AS subCat,
		mst_maincategory.intId AS mainId,
		mst_subcategory.intId AS subId,
		mst_item.strName AS itemName,
		mst_units.intId,
		mst_units.strName AS unitName,
		mst_financecurrency.strCode AS currencyName,
		sum(trn_orderdetails.intQty*trn_sample_foil_consumption.dblMeters ) as totalQty,
		sum(trn_sample_foil_consumption.dblMeters) as consum,
		mst_units.dblNoOfPcs,
	round(((mst_item.dblLastPrice*mst_financeexchangerate.dblBuying)/(SELECT
			mst_financeexchangerate.dblBuying 
		FROM
			mst_financeexchangerate 
			Inner Join mst_financecurrency ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId
		WHERE
			mst_financeexchangerate.intCurrencyId =  '$currencyId' AND
			mst_financeexchangerate.intCompanyId =  mst_locations.intCompanyId AND
			mst_financeexchangerate.dtmDate =  trn_sampleinfomations.dtDate
	)),4) as dblLastPrice 
	FROM
		trn_orderdetails 
		INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_orderdetails.intRevisionNo AND trn_sampleinfomations.strGraphicRefNo = trn_orderdetails.strGraphicNo
		Inner Join trn_sample_foil_consumption ON trn_sample_foil_consumption.intSampleNo = trn_orderdetails.intSampleNo AND trn_sample_foil_consumption.intSampleYear = trn_orderdetails.intSampleYear AND trn_sample_foil_consumption.intRevisionNo = trn_orderdetails.intRevisionNo AND trn_sample_foil_consumption.strCombo = trn_orderdetails.strCombo AND trn_sample_foil_consumption.strPrintName = trn_orderdetails.strPrintName
		Inner Join mst_item ON mst_item.intId = trn_sample_foil_consumption.intItem
		Inner Join mst_locations ON trn_sampleinfomations.intCompanyId = mst_locations.intId 
		left Join mst_financeexchangerate ON mst_item.intCurrency = mst_financeexchangerate.intCurrencyId AND trn_sampleinfomations.dtDate = mst_financeexchangerate.dtmDate AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId 
		Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
		Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
		Inner Join mst_units ON mst_units.intId = mst_item.intUOM
		Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_item.intCurrency
	WHERE
		trn_orderdetails.intOrderNo 	=  '$orderNo' AND
		trn_orderdetails.intOrderYear 	=  '$orderYear' 
		$para
	GROUP BY
		mst_maincategory.intId,
		mst_subcategory.intId,
		mst_item.intId";
	//	echo $sql;

		return $result = $db->RunQuery($sql);
	}
	//--------------------------------------------------------------
function getBulkConsumptions($orderNo,$orderYear,$currencyId,$styleNo,$salesOrderNo,$graphic,$itemId){
	
	global $db;
	$sql1 = "
	select 
	round(sum(tb3.wastageCost),6) as wastageCost,
	round(sum(tb3.wastage),6) as wastage,
	round(sum(tb3.Consump),6) as Consump,
	round(sum(tb3.Cost),6) as Cost 
	FROM  ((select 
	 0 as wastageCost,
	 0 as wastage,
IFNULL(sum(tb1.Cost/mst_financeexchangerate.dblBuying),0) as Cost,
IFNULL(sum(tb1.Consump),0) as Consump 

 from (SELECT 
(ware_stocktransactions.dblQty*ware_stocktransactions.dblGRNRate*mst_financeexchangerate.dblBuying) AS Cost,
(ware_stocktransactions.dblQty) AS Consump,
date(ware_stocktransactions.dtDate) as date,
mst_locations.intCompanyId 
FROM
ware_stocktransactions
left JOIN mst_locations ON ware_stocktransactions.intLocationId = mst_locations.intId
left JOIN trn_orderheader ON ware_stocktransactions.intOrderNo = trn_orderheader.intOrderNo AND ware_stocktransactions.intOrderYear = trn_orderheader.intOrderYear
left JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_stocktransactions.intSalesOrderId = trn_orderdetails.intSalesOrderId
left JOIN mst_financeexchangerate ON ware_stocktransactions.intCurrencyId = mst_financeexchangerate.intCurrencyId AND date(ware_stocktransactions.dtDate) = mst_financeexchangerate.dtmDate AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId
WHERE
trn_orderdetails.intOrderNo 	=  '$orderNo' AND
trn_orderdetails.intOrderYear 	=  '$orderYear' AND 
ware_stocktransactions.intItemId = '$itemId' AND 
ware_stocktransactions.strType IN ('ALLOCATE','UNALLOCATE')
";
if($styleNo!='')				
$sql1 .= " AND trn_orderdetails.strStyleNo 	=  '$styleNo' ";
if($graphic!='')				
$sql1 .= " AND trn_orderdetails.strGraphicNo 	=  '$graphic' ";
if($salesOrderNo!='')				
$sql1 .= " AND trn_orderdetails.strSalesOrderNo 	=  '$salesOrderNo' ";  
$sql1 .= " /*group by strStyleNo,strGraphicNo*/ ) as tb1 
left JOIN mst_financeexchangerate ON $currencyId = mst_financeexchangerate.intCurrencyId 
AND tb1.date = mst_financeexchangerate.dtmDate 
AND tb1.intCompanyId = mst_financeexchangerate.intCompanyId)

UNION 

(select 
 
IFNULL(sum(table2.wastageCost/mst_financeexchangerate.dblBuying),0) as wastageCost,
IFNULL(sum(table2.wastage),0) as wastage,
IFNULL(sum(table2.Cost/mst_financeexchangerate.dblBuying),0) as Cost,
IFNULL(sum(table2.Consump),0) as Consump 
from 
(
select 
(table1.wastage*table1.dblGRNRate*mst_financeexchangerate.dblBuying) AS wastageCost,
(table1.wastage) AS wastage,
(table1.consump*table1.dblGRNRate*mst_financeexchangerate.dblBuying) AS Cost,
(table1.consump) AS Consump,
table1.dblGRNRate,
date(table1.dtGRNDate) as date,
mst_locations.intCompanyId 

 from (
SELECT 

Sum(ware_sub_stocktransactions_bulk.dblQty)/
(select Sum(sub.dblQty) 
from ware_sub_stocktransactions_bulk as sub 
where ware_sub_stocktransactions_bulk.intOrderNo=sub.intOrderNo AND 
ware_sub_stocktransactions_bulk.intOrderYear=sub.intOrderYear AND 
ware_sub_stocktransactions_bulk.intSalesOrderId=sub.intSalesOrderId AND 
ware_sub_stocktransactions_bulk.intColorId=sub.intColorId AND 
ware_sub_stocktransactions_bulk.intTechnique=sub.intTechnique AND 
ware_sub_stocktransactions_bulk.intInkType=sub.intInkType) /*as itemConsump,*/
*
(SELECT
IFNULL(Sum(subCol.dblQty*-1),0) 
FROM `ware_sub_color_stocktransactions_bulk` as subCol 
WHERE
subCol.strType = 'WASTAGE' AND 
ware_sub_stocktransactions_bulk.intOrderNo=subCol.intOrderNo AND 
ware_sub_stocktransactions_bulk.intOrderYear=subCol.intOrderYear AND 
ware_sub_stocktransactions_bulk.intSalesOrderId=subCol.intSalesOrderId AND 
ware_sub_stocktransactions_bulk.intColorId=subCol.intColorId AND 
ware_sub_stocktransactions_bulk.intTechnique=subCol.intTechnique AND 
ware_sub_stocktransactions_bulk.intInkType=subCol.intInkType) as wastage, 




Sum(ware_sub_stocktransactions_bulk.dblQty)/
(select Sum(sub.dblQty) 
from ware_sub_stocktransactions_bulk as sub 
where ware_sub_stocktransactions_bulk.intOrderNo=sub.intOrderNo AND 
ware_sub_stocktransactions_bulk.intOrderYear=sub.intOrderYear AND 
ware_sub_stocktransactions_bulk.intSalesOrderId=sub.intSalesOrderId AND 
ware_sub_stocktransactions_bulk.intColorId=sub.intColorId AND 
ware_sub_stocktransactions_bulk.intTechnique=sub.intTechnique AND 
ware_sub_stocktransactions_bulk.intInkType=sub.intInkType) /*as itemConsump,*/
*
((SELECT
IFNULL(Sum(subCol.dblQty*-1),0) 
FROM `ware_sub_color_stocktransactions_bulk` as subCol 
WHERE
subCol.strType = 'ISSUE' AND 
ware_sub_stocktransactions_bulk.intOrderNo=subCol.intOrderNo AND 
ware_sub_stocktransactions_bulk.intOrderYear=subCol.intOrderYear AND 
ware_sub_stocktransactions_bulk.intSalesOrderId=subCol.intSalesOrderId AND 
ware_sub_stocktransactions_bulk.intColorId=subCol.intColorId AND 
ware_sub_stocktransactions_bulk.intTechnique=subCol.intTechnique AND 
ware_sub_stocktransactions_bulk.intInkType=subCol.intInkType)
-((SELECT
IFNULL(Sum(subCol.dblQty),0) 
FROM `ware_sub_color_stocktransactions_bulk` as subCol 
WHERE
subCol.strType = 'RETURN' AND 
ware_sub_stocktransactions_bulk.intOrderNo=subCol.intOrderNo AND 
ware_sub_stocktransactions_bulk.intOrderYear=subCol.intOrderYear AND 
ware_sub_stocktransactions_bulk.intSalesOrderId=subCol.intSalesOrderId AND 
ware_sub_stocktransactions_bulk.intColorId=subCol.intColorId AND 
ware_sub_stocktransactions_bulk.intTechnique=subCol.intTechnique AND 
ware_sub_stocktransactions_bulk.intInkType=subCol.intInkType))
-((SELECT
IFNULL(Sum(subCol.dblQty*-1),0) 
FROM `ware_sub_color_stocktransactions_bulk` as subCol 
WHERE
subCol.strType = 'WASTAGE' AND 
ware_sub_stocktransactions_bulk.intOrderNo=subCol.intOrderNo AND 
ware_sub_stocktransactions_bulk.intOrderYear=subCol.intOrderYear AND 
ware_sub_stocktransactions_bulk.intSalesOrderId=subCol.intSalesOrderId AND 
ware_sub_stocktransactions_bulk.intColorId=subCol.intColorId AND 
ware_sub_stocktransactions_bulk.intTechnique=subCol.intTechnique AND 
ware_sub_stocktransactions_bulk.intInkType=subCol.intInkType))) /*as allocatedInk*/ as consump,

ware_sub_stocktransactions_bulk.intLocationId,
ware_sub_stocktransactions_bulk.intOrderNo,
ware_sub_stocktransactions_bulk.intOrderYear,
ware_sub_stocktransactions_bulk.intSalesOrderId,
ware_sub_stocktransactions_bulk.intColorId,
ware_sub_stocktransactions_bulk.intTechnique,
ware_sub_stocktransactions_bulk.intInkType,
ware_sub_stocktransactions_bulk.intItemId,
ware_sub_stocktransactions_bulk.dblGRNRate,
ware_sub_stocktransactions_bulk.dtGRNDate,
ware_sub_stocktransactions_bulk.intCurrencyId
FROM `ware_sub_stocktransactions_bulk` 

WHERE
ware_sub_stocktransactions_bulk.intItemId = '$itemId' AND 
ware_sub_stocktransactions_bulk.strType IN ('INK_ALLOC')
";

$sql1 .= " GROUP BY 
ware_sub_stocktransactions_bulk.intLocationId, 
ware_sub_stocktransactions_bulk.intOrderNo,
ware_sub_stocktransactions_bulk.intOrderYear,
ware_sub_stocktransactions_bulk.intSalesOrderId,
ware_sub_stocktransactions_bulk.intColorId,
ware_sub_stocktransactions_bulk.intTechnique,
ware_sub_stocktransactions_bulk.intInkType,
ware_sub_stocktransactions_bulk.dblGRNRate,
ware_sub_stocktransactions_bulk.dtGRNDate,
ware_sub_stocktransactions_bulk.intCurrencyId ) as table1 

left JOIN mst_locations ON table1.intLocationId = mst_locations.intId
left JOIN trn_orderheader ON table1.intOrderNo = trn_orderheader.intOrderNo AND table1.intOrderYear = trn_orderheader.intOrderYear
left JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear 
AND table1.intSalesOrderId = trn_orderdetails.intSalesOrderId
left JOIN mst_financeexchangerate ON table1.intCurrencyId = mst_financeexchangerate.intCurrencyId 
AND date(table1.dtGRNDate) = mst_financeexchangerate.dtmDate AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId 
where  
trn_orderdetails.intOrderNo 	=  '$orderNo' AND
trn_orderdetails.intOrderYear 	=  '$orderYear'  ";  

if($styleNo!='')				
$sql1 .= " AND trn_orderdetails.strStyleNo 	=  '$styleNo' ";
if($graphic!='')				
$sql1 .= " AND trn_orderdetails.strGraphicNo 	=  '$graphic' ";
if($salesOrderNo!='')				
$sql1 .= " AND trn_orderdetails.strSalesOrderNo 	=  '$salesOrderNo' ";  

$sql1 .= " ) as table2 

left JOIN mst_financeexchangerate ON $currencyId = mst_financeexchangerate.intCurrencyId 
AND table2.date = mst_financeexchangerate.dtmDate 
AND table2.intCompanyId = mst_financeexchangerate.intCompanyId)) as tb3";	
  //  echo $sql1;
		return $result = $db->RunQuery($sql1);

	}
	
	//--------------------------------------------
	function getOverheadConsumptions(){
		global $db;
		$sql1 = "SELECT
mst_overhead_types.intId,
mst_overhead_types.strName
FROM `mst_overhead_types`
WHERE
mst_overhead_types.intStatus = 1
ORDER BY
mst_overhead_types.strName ASC";
		return $result = $db->RunQuery($sql1);
	}
?>	

<?php
session_start();
$backwardseperator = "../../../";
$mainPath = $_SESSION['mainPath'];

$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];
$intUser  = $_SESSION["userId"];
//include  "{$backwardseperator}dataAccess/permisionCheck.inc";
include_once  "{$backwardseperator}dataAccess/Connector.php";
include_once $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";

$obj_common_err	= new cls_commonErrorHandeling_get($db);
//include 	"../../../class/cls_getData.php";

//BEGIN - CLASS OBJECTS	{
//$obj_getData	= new clsGetData;
//END - CLASS OBJECTS	}

$year			= $_SESSION["Year"];
$month			= $_SESSION["Month"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>nsoft | Costing - Reports</title>

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="reports.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" href="../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../libraries/validate/template.css" type="text/css">
</head>
<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<link rel="stylesheet" type="text/css" href="../../../libraries/calendar/theme.css" />
<script src="../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>

<!--<form id="frmPayrollReport" name="frmPayrollReport" autocomplete="off">
--><div align="center">
		<div class="trans_layoutL" style="width:700px">
		  <div class="trans_text">Costing Reports</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
          
 <!--BEGIN - SAMPLE COSTING-->
		    <tr>
		      <td><table align="left" width="600" class="main_header_table" id="tblSampleCostingHead" cellpadding="2" cellspacing="0">
		        <tr>
		          <td width="544"><img src="../../../images/report_go.png" class="mouseover" />&nbsp;<u>Sample Costing.</u></td>
	            </tr>
		        </table></td>
	        </tr>
		    <tr>
		      <td><form name="frmSampleCosting" id="frmSampleCosting" autocomplete="off">
              <table width="700" align="left" border="0" id="tblSampleCostingBody" class="main_table" cellspacing="0" <?php /*?>style="display:none;"<?php */?>>
		        <thead>
		          <tr>
		            <td colspan="4" style="text-align:left">Sample Costing Criteria<img src="../../../images/close_small.png" align="right" class="mouseover"/></td>
	              </tr>
	            </thead>
		        <tbody>
<tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="11%" class="normalfnt">Sample No</td>
            <td width="38%"><input id="cboSampleYear" type="text" name="cboSampleYear" style="width:60px"  /><input id="cboSampleNo" type="text" name="cboSampleNo" style="width:120px"  /><img src="../../../images/smallSearch.png" width="24" height="24" alt="Load Sales Orders" id="cboSearch" /></td>
            <td width="14%" class="normalfnt">Revision</td>
            <td width="33%"><select name="cboRevision" id="cboRevision" style="width:50px"  class="txtText validate[required]"  >
             </select></td>
            <td width="4%"></td>
          </tr> 
          <tr>
            <td width="11%" class="normalfnt">Print</td>
            <td width="38%"><select name="cboPrint" id="cboPrint" style="width:180px"  class="txtText validate[required]"  >
                  <option value=""></option>
            </select></td>
            <td width="14%" class="normalfnt">Combo</td>
            <td width="33%"><select name="cboCombo" id="cboCombo" style="width:180px"  class="txtText validate[required]"  >
              <option value=""></option>
            </select></td>
        <td></td>
          </tr> 
          <tr>
            <td width="11%" class="normalfnt">Color</td>
            <td width="38%"><select name="cboColor" id="cboColor" style="width:180px"  class="txtText validate[required]"  >
              <option value=""></option>
            </select></td>
            <td width="14%" class="normalfnt">&nbsp;</td>
            <td width="33%">&nbsp;</td>
        <td></td>
          </tr> 
          
          <tr>
            <td colspan="4" class="normalfntMid"><img src="../../../images/Treport.jpg" style="display:inline" border="0"  alt="view report" id="butReport" name="butReport" /></td>
          </tr>

         </table></td>
      </tr>       			</tbody>
	          </table></form></td>
	        </tr>
<!--END - SAMPLE COSTING--> 
             
	      </table>
    </div>
  </div>
<!--</form>
-->
</body>
</html>

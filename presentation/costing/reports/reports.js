var basePath	="presentation/costing/reports/";

$(document).ready(function(){
	//------------------------
	$('#tblCostingTimeWiseHead').click(function(){

		headShowHideGraphic('tblCostingTimeWiseHead','tblCostingTimeWiseBody');

	});

    
	$('#frmCotingTimeWise thead').click(function(){

		bodyShowHideGraphic('tblCostingTimeWiseHead','tblCostingTimeWiseBody');

	});

	
	$('#tblCostingTimeWiseBody thead').click(function(){

		bodyShowHideGraphic('tblCostingTimeWiseHead','tblCostingTimeWiseBody');

	});
	

	$('#frmCostingTimeWise #butNew').click(function(){
		$('#frmCostingTimeWise')[0].reset();
	});

	$('#frmCostingTimeWise #butDownload').click(function(){
		//$('#frmCostingTimeWise')[0].reset();
		loadExcelRpt();
	}); 

	$('#tblMaterialHead').click(function(){

		headShowHideGraphic('tblMaterialHead','tblMaterialBody');
	});

	$('#frmCostingTimeWise thead').click(function(){

		bodyShowHideGraphic('tblMaterialHead','tblMaterialBody');

	});

	$('#tblMaterialBody thead').click(function(){

		bodyShowHideGraphic('tblMaterialHead','tblMaterialBody');

	});
	//---------------------------
	$('#tblMonthlySummeryHead').click(function(){

		headShowHideGraphic('tblMonthlySummeryHead','tblMonthlySummeryBody');
	});

	$('#frmMonthlySummery thead').click(function(){

		bodyShowHideGraphic('tblMonthlySummeryHead','tblMonthlySummeryBody');

	});

	$('#tblMonthlySummeryBody thead').click(function(){

		bodyShowHideGraphic('tblMonthlySummeryHead','tblMonthlySummeryBody');

	});
	
	
	
	//-----------------------------------------------------------------
	
	$('#tbldailyRmMovementforMonthheader').click(function(){

		headShowHideGraphic('tbldailyRmMovementforMonthheader','tbldailyRmMovementforMonthBody');
	});

	$('#frmdailyRmMovementforMonth thead').click(function(){

		bodyShowHideGraphic('tbldailyRmMovementforMonthheader','tbldailyRmMovementforMonthBody');

	});

	$('#tbldailyRmMovementforMonthBody thead').click(function(){

		bodyShowHideGraphic('tbldailyRmMovementforMonthheader','tbldailyRmMovementforMonthBody');

	});
	
	//--------------------------------------------------------------------
	
	
	$('#tblOrderbookHead').click(function(){

		headShowHideGraphic('tblOrderbookHead','tblOrderbookBody');
	});

	$('#frmOrderbook thead').click(function(){

		bodyShowHideGraphic('tblOrderbookHead','tblOrderbookBody');

	});

	$('#tblOrderbookBody thead').click(function(){

		bodyShowHideGraphic('tblOrderbookHead','tblOrderbookBody');

	});
	
	$("#frmOrderbook .chosen-select").chosen({allow_single_deselect: false , no_results_text: "Oops, nothing found!",width:"100%"});

});

function ViewReportGraphic(formId)
{
	//var reportId	="927";
	if ($('#'+formId).validationEngine('validate'))  {
		var dateFrom = $('#frmCostingTimeWise #costingtimewiseDateFrom').val();
		var dateTo   = $('#frmCostingTimeWise #costingtimewiseDateTo').val();
		var HourFrom = $('#frmCostingTimeWise #textHourFrom').val();
		var HourTo   = $('#frmCostingTimeWise #textHourTo').val();
		var MinFrom = $('#frmCostingTimeWise #textMinFrom').val();
		var MinTo   = $('#frmCostingTimeWise #textMinTo').val();
		var userId   = $('#frmCostingTimeWise #cboUser').val();
		var start  = new Date(dateFrom);
		var end  = new Date(dateTo);
		var duration = Math.abs(end - start) / 86400000;
		var viewImg	=0;
		if($('#chkViewImg').attr('checked'))
			viewImg=1;	
 		
		if(HourFrom>23){
			alert("max hours is 23")
			return false
		}
		else if(HourTo>23){
			alert("max hours is 23")
			return false
		}
		else if(MinFrom>59){
			alert("max minutes is 59")
			return false
		}
		else if(MinTo>59){
			alert("max minutes is 59")
			return false
		}
		
		
	var url = "?q=1213&dateForm=" + dateFrom + "&dateTo=" + dateTo + "&hourFrom=" + HourFrom + "&minFrom=" + MinFrom +"&hourTo=" + HourTo + "&minTo=" + MinTo + "&userId=" + userId + "&viewImg=" + viewImg;
	if((dateFrom && dateTo != "")  && duration <=31){
		window.open(url)	
		}else{
		alert('Maximum time gap is one month');

		}
	}
		//$('#frmCostingTimeWise')[0].reset();
	
}

function loadExcelRpt()
{
	
	var dateFrom 	= $('#frmCostingTimeWise #costingtimewiseDateFrom').val();
	var dateTo   	= $('#frmCostingTimeWise #costingtimewiseDateTo').val();
	var HourFrom 	= $('#frmCostingTimeWise #textHourFrom').val();
	var HourTo   	= $('#frmCostingTimeWise #textHourTo').val();
	var MinFrom 	= $('#frmCostingTimeWise #textMinFrom').val();
	var MinTo   	= $('#frmCostingTimeWise #textMinTo').val();
	var userId   	= $('#frmCostingTimeWise #cboUser').val();
	var start  		= new Date(dateFrom);
	var end  		= new Date(dateTo);
	var duration 	= Math.abs(end - start) / 86400000;
	var viewImg		=0;
	if(HourFrom>23){
		alert("max hours is 23")
		return false
	}
	else if(HourTo>23){
		alert("max hours is 23")
		return false
	}
	else if(MinFrom>59){
		alert("max hours is 59")
		return false
	}
	else if(MinTo>59){
		alert("max hours is 59")
		return false
	}
	
	if($('#chkViewImg').attr('checked'))
		viewImg=1;	
	
	var url = "presentation/costing/sample/priceCosting/listing/priceCostingReportSelection-xlsx_selection.php?type=costing_to_excel&dateForm=" + dateFrom + "&dateTo=" + dateTo + "&hourFrom=" + HourFrom + "&minFrom=" + MinFrom +"&hourTo=" + HourTo + "&minTo=" + MinTo + "&userId=" + userId + "&viewImg=" + viewImg;
	if((dateFrom && dateTo != "")  && duration <=31)
	{
		window.open(url)	
	}
	else
	{
		alert('Maximum time gap is one month');
	}
	
}


function ViewOrderBookReport(type,formId) //suvini
{
	if ($('#'+formId).validationEngine('validate')) 
	 {
		var company 			= $('#frmOrderbook #cbocompany').val();
		var cluster    			= $('#frmOrderbook #cbocluster').val();
		var plant    			= $('#frmOrderbook #cboplant').val();
		var year        		= $('#frmOrderbook #cboYear').val();
		var month        		= $('#frmOrderbook #cboMonth').val();
		var order_created   	= ($("#frmOrderbook #create").is(':checked') ) ? 1 : 0;
		var order_dispatched    = ($("#frmOrderbook #dispatch").is(':checked') ) ? 2 : 0;
		
			
		var url = "?q=1265&company="+company+"&cluster="+cluster+"&plant="+plant+"&year="+year+"&month="+month+"&order_created="+order_created+"&order_dispatched="+order_dispatched;
		if(company == '')
		{
			$('#validate_company1').html('<span id="error" style=\"color:red\">* This Field is Required</span>');
			return false;
		}
		else if(cluster=='')
		{
			$('#validate_cluster').html('<span id="error" style=\"color:red\">* This Field is Required</span>');
			return false;
		}
		else
		{
			window.open(url)
		}
	}
}

function headShowHideGraphic(headId,bodyId){

		 $('#'+headId).hide('slow');
		 $('#'+bodyId).toggle();

}

function bodyShowHideGraphic(headId,bodyId){

		$('#'+headId).show('slow');
		$('#'+bodyId).hide('slow');
}

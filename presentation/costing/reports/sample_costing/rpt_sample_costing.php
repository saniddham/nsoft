<?php
//ini_set('display_errors',1);
session_start();
$backwardseperator 	= "../../../../";
$companyId 			= $_SESSION['CompanyID'];
$locationId 		= $_SESSION['CompanyID'];//this locationId use in report header(reportHeader.php)-------
$intUser  			= $_SESSION["userId"];
$mainPath 			= $_SESSION['mainPath'];
$thisFilePath 		=  $_SERVER['PHP_SELF'];
include_once		"{$backwardseperator}dataAccess/Connector.php";
   
 

$sampleNo 				= $_REQUEST['sampleNo'];
$sampleYear				= $_REQUEST['sampleYear'];
$revision				= $_REQUEST['revision'];
$print					= $_REQUEST['print'];
$combo					= $_REQUEST['combo'];
$color					= $_REQUEST['color'];
$currencyId				= 1;


$result			= get_color($color);
$row 			= mysqli_fetch_array($result);
$color_desc 	= $row['strName'];
$currency		= get_currency_code($currencyId); 
 //----------------------------------
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ink Stock Balance Report</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../mrn/listing/rptMrn-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:264px;
	top:247px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<form id="frmMrnReport" name="frmMrnReport" method="post" action="../mrn/listing/rptMrn.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>SAMPLE COSTING REPORT</strong></div>
<table width="868" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td width="905">
  <table width="100%">
          <tr>
            <td width="1%">&nbsp;</td>
            <td width="8%" class="normalfnt">Sample No</td>
            <td width="1%" align="center" valign="middle" class="normalfnt">:</td>
            <td width="9%" class="normalfnt"><?php echo $sampleNo.'/'.$sampleYear; ?></td>
            <td width="8%"><span class="normalfnt">Revision</span></td>
            <td width="1%" align="center" valign="middle" class="normalfnt">:</td>
            <td width="13%"><span class="normalfnt"><?php echo $revision;?></span></td>
            <td width="5%" class="normalfnt">Combo</td>
            <td width="1%" class="normalfnt">:</td>
            <td width="19%" class="normalfnt"><?php echo $combo;?></td>
            <td width="3%" class="normalfnt">Print</td>
            <td width="1%" class="normalfnt">:</td>
            <td width="10%" class="normalfnt"><?php echo $print;?></td>
            <td width="4%" class="normalfnt">Color</td>
            <td width="1%" class="normalfnt">:</td>
            <td width="9%" class="normalfnt"><?php echo $color_desc;?></td>
            <td width="6%">&nbsp;</td>
          </tr>
        </table>
  </td>
</tr>
<tr><td>
<table width="506" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
                <tr class="">
                  <th width="25%" >Style No</th>
                  <th width="41%" >Graphic No</th>
                  <th width="34%" >Graphic</th>
                </tr>
                <?php 
			$tmpSalesId		= '';
			$tmpGraphic		= '';
			$tmpSampleNo	= '';
			$tmpSampleYear	= '';
			$tmpPlacement	= '';
			$tmpCombo		= '';
			$tmpRevNo		= '';
			
	  	 $sql1 = "SELECT   
					SH.strGraphicRefNo,
					SH.strStyleNo , 
					SH.intSampleNo,
					SH.intSampleYear,
					SH.intRevisionNo 
				FROM
					trn_sampleinfomations SH 
					INNER JOIN trn_sampleinfomations_details AS SD ON SH.intSampleNo = SD.intSampleNo 
					AND SH.intSampleYear = SD.intSampleYear 
					AND SH.intRevisionNo = SD.intRevNo

				WHERE
					SH.intSampleNo	= '$sampleNo' AND
					SH.intSampleYear	= '$sampleYear' ";
		if($revision!='')				
			$sql1 .= " AND SH.intRevisionNo = '$revision'"; 
		if($color!='')				
			$sql1 .= " AND SD.intGroundColor = '$color'"; 
		if($print!='')				
			$sql1 .= " AND SD.strPrintName = '$print'"; 
		if($combo!='')				
			$sql1 .= " AND SD.strComboName = '$combo'"; 
		
			$sql1 .= "GROUP BY strStyleNo,strGraphicRefNo";
			//echo $sql1;
			$result1 = $db->RunQuery($sql1);
			$tmpStyle='';
			while($row=mysqli_fetch_array($result1))
			{
				$sampleNo		= $row['intSampleNo'];
				$sampleYear		= $row['intSampleYear'];
				$revNo			= $row['intRevisionNo'];
			?>
                <tr class="normalfnt"  bgcolor="#FFFFFF">
                  <td class="normalfnt" >&nbsp;
                    <?php  echo $row['strStyleNo'];  ?>
                    &nbsp;</td>
                  <td class="normalfnt" >&nbsp;<?php echo $row['strGraphicRefNo'] ?>&nbsp;</td>
                  <td class="normalfnt" style="text-align:center"><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:264px;height:148px;overflow:hidden;text-align:center" >
                      <?php
			if($sampleNo!='')
			{
				echo "<img id=\"saveimg\" style=\"width:264px;height:148px;text-align:center\" src=\"../../../documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revision-1.png\" />";	
			}
			 ?>
                    </div></td>
                </tr>
                <?php   
 			}
	        ?>
              </table>
</td></tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
                <tr class="">
                  <th colspan="6" align="left"><b>DRM - Direct Raw Materials</b></th>
                </tr>
                <tr class="">
                  <th width="7%"  height="22">Style No</th>
                  <th width="35%">Item</th>
                  <th width="3%">UOM</th>
                  <th width="6%">Sample ConPC</th>
                  <th width="4%"> Rate (<?php echo $currency; ?>)</th>
                  <th width="4%">Amount</th>
                </tr>
<?php
			$totPreConPC		= 0;
			$totPreConsump		= 0;
			$totPreCost			= 0;
			$totPostConPC		= 0;
			$totPostConsump		= 0;
			$totPostCost		= 0;
			$totWastage			= 0;
			$totWastageCost		= 0;

			$result1 = getSampleInkConsumptions($sampleNo,$sampleYear,$revision,$combo,$print,$color);
			while($row=mysqli_fetch_array($result1))
			{
?>
                <tr class="normalfnt"  bgcolor="#FFFFFF">
                  <td class="normalfnt" >&nbsp;<?php echo $row["STYLE_NO"] ?>&nbsp;</td>
                  <td class="normalfnt" id="<?php echo $row["intItem"]?>"><?php echo $row['itemName'] ?>&nbsp;</td>
                  <td class="normalfnt" ><?php echo $row['unitName'] ?></td>
                  <td class="normalfntRight" align="right" ><?php echo $row['consum'] ?>&nbsp;</td>
                  <td class="normalfntRight" align="right" ><?php echo round($row['dblLastPrice'],4); ?></td>
                  <td class="normalfntRight" align="right" ><span class="normalfntMid"><?php echo number_format($row['consum']*round($row['dblLastPrice'],4),6); ?></span></td>
                </tr>
                <?php
				$totPreConPC 		+= $row['consum'];
				$totPreConPCAmount	+= $row['consum']*round($row['dblLastPrice'],4);
			}
			?>
                <?php 
			//special RM consumptions
			$result1 = getSpecialRMConsumptions($sampleNo,$sampleYear,$revision,$combo,$print,$color);
			while($row=mysqli_fetch_array($result1))
			{
	  ?>
                <tr class="normalfnt"  bgcolor="#FFFFFF">
                  <td class="normalfnt" >&nbsp;<?php echo $row["strStyleNo"] ?>&nbsp;</td>
                  <td class="normalfnt" id="<?php echo $row["itemId"]?>"><?php echo $row['itemName'] ?>&nbsp;</td>
                  <td class="normalfnt" ><?php echo $row['unitName'] ?></td>
                  <td class="normalfntRight" align="right" ><?php echo $row['consum'] ?>&nbsp;</td>
                  <td class="normalfntRight" align="right" ><?php echo round($row['dblLastPrice'],4); ?></td>
                  <td class="normalfntRight" align="right" ><span class="normalfntMid"><?php echo number_format($row['consum']*round($row['dblLastPrice'],4),6); ?></span></td>
                </tr>
                <?php
				$totPreConPC 		+= $row['consum'];
				$totPreConPCAmount	+= $row['consum']*round($row['dblLastPrice'],4);
			}
			?>
                <?php 
			//Foil consumptions
			$result1 = getFoilConsumptions($sampleNo,$sampleYear,$revision,$combo,$print,$color);
			while($row=mysqli_fetch_array($result1))
			{
	  ?>
                <tr class="normalfnt"  bgcolor="#FFFFFF">
                  <td class="normalfnt" >&nbsp;<?php echo $row["STYLE_NO"] ?>&nbsp;</td>
                  <td class="normalfnt" ><?php echo $row['itemName'] ?>&nbsp;</td>
                  <td class="normalfnt" ><?php echo $row['unitName'] ?></td>
                  <td class="normalfntRight" align="right" ><?php echo $row['consum'] ?>&nbsp;</td>
                  <td class="normalfntRight" align="right" ><?php echo round($row['dblLastPrice'],4); ?></td>
                  <td class="normalfntRight" align="right" ><span class="normalfntMid"><?php echo number_format($row['consum']*round($row['dblLastPrice'],4),6); ?></span></td>
                </tr>
                <?php
				$totPreConPC 		+= $row['consum'];
				$totPreConPCAmount	+= $row['consum']*round($row['dblLastPrice'],4);
			}
			?>
                <tr class="normalfnt"  bgcolor="#F9F9F9">
                  <td class="normalfnt" >&nbsp;&nbsp;</td>
                  <td class="normalfnt" >&nbsp;</td>
                  <td class="normalfnt" ></td>
                  <td class="normalfntRight" ><?php echo number_format($totPreConPC,6); ?>&nbsp;</td>
                  <td class="normalfnt" ></td>
                  <td class="normalfntRight" ><?php echo number_format($totPreConPCAmount,6); ?></td>
                </tr>
              </table>
        </td>
        </tr>
      
      </table>
    </td>
</tr>

<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>

<?php

function get_color($color){
	global $db;
	 		$sql = "SELECT DISTINCT
				trn_sampleinfomations_details.intGroundColor,
				mst_colors_ground.strName
				FROM
					trn_sampleinfomations_details 
					Inner Join mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
				WHERE
					trn_sampleinfomations_details.intGroundColor =  '$color' ";

		$result = $db->RunQuery($sql);
		return $result;
					
}

 	//--------------------------------------------------------------
function getSampleInkConsumptions($sampleNo,$sampleYear,$revision,$combo,$print,$color)
{
		global $db;
		global $currencyId;
		if($sampleNo!='')
			$para .= " AND trn_sample_color_recipes.intSampleNo='$sampleNo'";
		if($sampleYear!='')
			$para .= " AND trn_sample_color_recipes.intSampleYear='$sampleYear'";
		if($revision!='')
			$para .= " AND trn_sample_color_recipes.intRevisionNo='$revision'";
		if($combo!='')
			$para .= " AND trn_sample_color_recipes.strCombo='$combo'";
		if($print!='')
			$para .= " AND trn_sample_color_recipes.strPrintName='$print'";
	
$sql = "select mainId,mainCat,subId,subCat, intItem,itemName ,unitId,unitName

,sum(round(dblColorWeight/sumWeight*dblWeight /dblNoOfPcs,6))as consum
,dblLastPrice ,
currency 
from

(


SELECT
	round(((mst_item.dblLastPrice*mst_financeexchangerate.dblBuying)/(SELECT
			mst_financeexchangerate.dblBuying 
		FROM
			mst_financeexchangerate 
			Inner Join mst_financecurrency ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId
		WHERE
			mst_financeexchangerate.intCurrencyId =  '$currencyId' AND
			mst_financeexchangerate.intCompanyId =  mst_locations.intCompanyId AND
			mst_financeexchangerate.dtmDate =  trn_sampleinfomations.dtDate
	)),4) as dblLastPrice ,
trn_sample_color_recipes.intItem,
trn_sample_color_recipes.dblWeight,
trn_sampleinfomations_details_technical.intInkTypeId,
trn_sampleinfomations_details_technical.dblColorWeight,
trn_sample_color_recipes.intTechniqueId,
mst_item.strName itemName,
mst_units.strName as unitName,
mst_units.intId as unitId,
mst_units.dblNoOfPcs ,
mst_maincategory.strName AS mainCat,mst_maincategory.intId AS mainId,
mst_subcategory.strName AS subCat,mst_subcategory.intId AS subId,



(SELECT
Sum(trn_sample_color_recipes.dblWeight) AS sumWeight
FROM trn_sample_color_recipes
WHERE
trn_sample_color_recipes.intSampleNo = trn_sampleinfomations.intSampleNo AND
trn_sample_color_recipes.intSampleYear = trn_sampleinfomations.intSampleYear AND
trn_sample_color_recipes.intRevisionNo =  trn_sampleinfomations.intRevisionNo AND
trn_sample_color_recipes.strCombo =  trn_sample_color_recipes.strCombo AND
trn_sample_color_recipes.strPrintName =  trn_sample_color_recipes.strPrintName AND
trn_sample_color_recipes.intTechniqueId =  trn_sample_color_recipes.intTechniqueId AND
trn_sample_color_recipes.intInkTypeId =  trn_sampleinfomations_details_technical.intInkTypeId AND
trn_sample_color_recipes.intColorId =  trn_sampleinfomations_details_technical.intColorId 
 )  as sumWeight ,
	mst_financecurrency.strCode as currency  



FROM
trn_sampleinfomations  
Inner Join trn_sample_color_recipes ON trn_sample_color_recipes.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_sample_color_recipes.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_sample_color_recipes.intRevisionNo = trn_sampleinfomations.intRevisionNo  
Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = trn_sample_color_recipes.intSampleNo AND trn_sampleinfomations_details_technical.intSampleYear = trn_sample_color_recipes.intSampleYear AND trn_sampleinfomations_details_technical.intRevNo = trn_sample_color_recipes.intRevisionNo AND trn_sampleinfomations_details_technical.strComboName = trn_sample_color_recipes.strCombo AND trn_sampleinfomations_details_technical.strPrintName = trn_sample_color_recipes.strPrintName AND trn_sampleinfomations_details_technical.intColorId = trn_sample_color_recipes.intColorId AND trn_sampleinfomations_details_technical.intInkTypeId = trn_sample_color_recipes.intInkTypeId
Inner Join mst_item ON mst_item.intId = trn_sample_color_recipes.intItem
Inner Join mst_locations ON trn_sampleinfomations.intCompanyId = mst_locations.intId 
left Join mst_financeexchangerate ON mst_item.intCurrency = mst_financeexchangerate.intCurrencyId AND trn_sampleinfomations.dtDate = mst_financeexchangerate.dtmDate AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId 
Inner Join mst_units ON mst_item.intUOM = mst_units.intId
Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
	Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_item.intCurrency
WHERE 
1=1 
 $para
) as t 

group by

 mainId,mainCat,subId,SubCat, intItem,itemName 
";
 //echo $sql;

		return $result = $db->RunQuery($sql);
	}
	//--------------------------------------------------------------
	function getSpecialRMConsumptions($sampleNo,$sampleYear,$revision,$combo,$print,$color)
	{
		global $currencyId;
		global $db;
		
		if($sampleNo!='')
			$para .= " AND trn_sample_spitem_consumption.intSampleNo='$sampleNo'";
		if($sampleYear!='')
			$para .= " AND trn_sample_spitem_consumption.intSampleYear='$sampleYear'";
		if($revision!='')
			$para .= " AND trn_sample_spitem_consumption.intRevisionNo='$revision'";
		if($combo!='')
			$para .= " AND trn_sample_spitem_consumption.strCombo='$combo'";
		if($print!='')
			$para .= " AND trn_sample_spitem_consumption.strPrintName='$print'";
			
		
		   $sql = "SELECT 
	trn_sampleinfomations.strStyleNo,	   
	mst_maincategory.intId AS mainId,
	mst_maincategory.strName AS mainCat,
	mst_subcategory.intId AS subId,
	mst_subcategory.strName AS subCat,
	mst_item.intId AS itemId,
	mst_item.strName AS itemName,
	mst_units.intId AS unitId,
	mst_units.strName AS unitName,
	round(sum(trn_sample_spitem_consumption.dblQty/dblNoOfPcs),6) as consum,
	mst_units.dblNoOfPcs,
	round(((mst_item.dblLastPrice*mst_financeexchangerate.dblBuying)/(SELECT
			mst_financeexchangerate.dblBuying 
		FROM
			mst_financeexchangerate 
			Inner Join mst_financecurrency ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId
		WHERE
			mst_financeexchangerate.intCurrencyId =  '$currencyId' AND
			mst_financeexchangerate.intCompanyId =  mst_locations.intCompanyId AND
			mst_financeexchangerate.dtmDate =  trn_sampleinfomations.dtDate
	)),4) as dblLastPrice, 
	mst_financecurrency.strCode as currency  
FROM
 	trn_sampleinfomations  
	Inner Join trn_sample_spitem_consumption ON trn_sample_spitem_consumption.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_sample_spitem_consumption.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_sample_spitem_consumption.intRevisionNo = trn_sampleinfomations.intRevisionNo  
	Inner Join mst_item ON mst_item.intId = trn_sample_spitem_consumption.intItem
	Inner Join mst_locations ON trn_sampleinfomations.intCompanyId = mst_locations.intId 
	left Join mst_financeexchangerate ON mst_item.intCurrency = mst_financeexchangerate.intCurrencyId AND trn_sampleinfomations.dtDate = mst_financeexchangerate.dtmDate AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId 
	Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
	Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
	Inner Join mst_units ON mst_units.intId = mst_item.intUOM
	Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_item.intCurrency
WHERE
 	1=1 
$para
GROUP BY
	mst_maincategory.intId,
	mst_subcategory.intId,
	mst_item.intId";
	//echo $sql;

		return $result = $db->RunQuery($sql);
	}
	//--------------------------------------------------------------
	function getFoilConsumptions($sampleNo,$sampleYear,$revision,$combo,$print,$color)
	{
		
		global $currencyId;
		global $db;
		
		if($sampleNo!='')
			$para .= " AND trn_sample_foil_consumption.intSampleNo='$sampleNo'";
		if($sampleYear!='')
			$para .= " AND trn_sample_foil_consumption.intSampleYear='$sampleYear'";
		if($revision!='')
			$para .= " AND trn_sample_foil_consumption.intRevisionNo='$revision'";
		if($combo!='')
			$para .= " AND trn_sample_foil_consumption.strCombo='$combo'";
		if($print!='')
			$para .= " AND trn_sample_foil_consumption.strPrintName='$print'";

		   $sql = "SELECT
		mst_item.intId AS itemId,
		mst_maincategory.strName AS mainCat,
		mst_subcategory.strName AS subCat,
		mst_maincategory.intId AS mainId,
		mst_subcategory.intId AS subId,
		mst_item.strName AS itemName,
		mst_units.intId,
		mst_units.strName AS unitName,
		mst_financecurrency.strCode AS currency,
		sum(trn_sample_foil_consumption.dblMeters) as consum,
		mst_units.dblNoOfPcs,
	round(((mst_item.dblLastPrice*mst_financeexchangerate.dblBuying)/(SELECT
			mst_financeexchangerate.dblBuying 
		FROM
			mst_financeexchangerate 
			Inner Join mst_financecurrency ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId
		WHERE
			mst_financeexchangerate.intCurrencyId =  '$currencyId' AND
			mst_financeexchangerate.intCompanyId =  mst_locations.intCompanyId AND
			mst_financeexchangerate.dtmDate =  trn_sampleinfomations.dtDate
	)),4) as dblLastPrice 
	FROM
		trn_sampleinfomations
		Inner Join trn_sample_foil_consumption ON trn_sample_foil_consumption.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_sample_foil_consumption.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_sample_foil_consumption.intRevisionNo = trn_sampleinfomations.intRevisionNo 
		Inner Join mst_item ON mst_item.intId = trn_sample_foil_consumption.intItem
		Inner Join mst_locations ON trn_sampleinfomations.intCompanyId = mst_locations.intId 
		left Join mst_financeexchangerate ON mst_item.intCurrency = mst_financeexchangerate.intCurrencyId AND trn_sampleinfomations.dtDate = mst_financeexchangerate.dtmDate AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId 
		Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
		Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
		Inner Join mst_units ON mst_units.intId = mst_item.intUOM
		Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_item.intCurrency
	WHERE 
	1=1 
		$para
	GROUP BY
		mst_maincategory.intId,
		mst_subcategory.intId,
		mst_item.intId";
	//	echo $sql;

		return $result = $db->RunQuery($sql);
	}


function get_currency_code($currencyId){
	
	global $db;
	
	$sql 			= "SELECT
						mst_financecurrency.strCode
						FROM `mst_financecurrency`
						WHERE
						mst_financecurrency.intId = '$currencyId'";
	$result 		= $db->RunQuery($sql);
	$row			= mysqli_fetch_array($result);
 	$code			= $row['strCode'];
	return $code;
	
}

?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
//include  "{$backwardseperator}dataAccess/permisionCheck.inc";
//include   "reports.js";
//BEGIN - CLASS OBJECTS {
//$obj_getData  = new clsGetData;
//END - CLASS OBJECTS   }
$userId         = $_SESSION['userId'];
$year           = $_SESSION["Year"];
$month          = $_SESSION["Month"];
?>
<title>nsoft | Costing - Reports</title>
<script type="text/javascript" src="presentation/costing/reports/reports.js"></script>

<div align="center">
<div class="trans_layoutL" style="width:600px">
<div class="trans_text">Costing Reports</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">


<!--BEGIN - PO Summery-->
<tr>
<td><table align="left" width="600" class="main_header_table" id="tblCostingTimeWiseHead" cellpadding="2" cellspacing="0">
<tr>
<td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Price Costing Time Wise</u></td>
</tr>
</table></td>
</tr>
<tr>
<td><form name="frmCostingTimeWise" id="frmCostingTimeWise" autocomplete="off">
<table width="600" align="left" border="0" id="tblCostingTimeWiseBody" class="main_table" cellspacing="0" style="display:none;">
<thead>
<tr>
<td colspan="4" style="text-align:left">Price Costing Time Wise<img src="images/close_small.png" align="right" class="mouseover"/>
</td>
</tr>
</thead>
<tbody align="center">
<tr>
                                <td width="200" align="left"><span class="normalfnt">Date From  <i>(Approved Date)</i></span></td>
<td width="203" align="left"><input name="txtFromDate" type="text" class="validate[required] txtbox" id="costingtimewiseDateFrom" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);" onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value="" class="txtbox" style="visibility:hidden;width:2px;" onclick="return showCalendar(this.id, '%Y-%m-%d');"/></td>
<td width="300" >Hour<input type="number" align="left" name ="texthourfrom" id="textHourFrom" style="width:50px; height:20px" min="0" max="23"/></td>
<td width="300" >Min<input type="number" align="left" name ="textminfrom" id="textMinFrom" style="width:50px;height:20px" min="0" max="59" /></td>
</tr>
<tr>
                            <td width="200" align="left"><span class="normalfnt">Date To <i>(Approved Date)</i></span></td>
<td width="443" align="left"><input name="txtToDate" type="text"
	class="validate[required] txtbox" id="costingtimewiseDateTo" style="width:98px;"
	onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();"
	onkeypress="return ControlableKeyAccess(event);"
	onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value="" class="txtbox" style="visibility:hidden;width:2px;"
																onclick="return showCalendar(this.id, '%Y-%m-%');"/></td>
<td width="300" >Hour<input type="number" align="left" name ="texthourTo" id="textHourTo" style="width:50px;height:20px" min="0" max="23"/></td>
<td width="300" >Min<input type="number" align="left" name ="textminTo" id="textMinTo" style="width:50px;height:20px" min="0" max="59" /></td>
</tr>
<tr>
<td width="200" align="left"><span class="normalfnt">Create User</span></td>
<td width="200" align="left"><select name="cboUser" id="cboUser" class="txtText" style="width:105px" >
<option value=""></option>
<?php
$sql = "SELECT
sys_users.intUserId,
sys_users.strUserName
FROM sys_users
WHERE
sys_users.intStatus =  '1'";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
echo "<option value=\"".$row['intUserId']."\">".$row['strUserName']."</option>";	
}
?>
</select></td>
<td >&nbsp;</td>
<td >&nbsp;</td>
</tr>
<tr>
<td width="200" align="left"><span class="normalfnt">View Image</span></td>
<td width="200" align="left"><input type="checkbox" name="chkViewImg" id="chkViewImg"/></td>
<td >&nbsp;</td>
<td >&nbsp;</td>
</tr>
<tr>
<td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
<tr>
<td width="100%" align="center" bgcolor="">
<img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReportGraphic('frmCostingTimeWise')"/>
<img src="images/Treport_exel.png" style="display:inline" border="0" id="butDownload" name="butDownload" /><a href="main.php">
<img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
</tr>
</table>
</td>
</tr>
</tbody>
</table></form></td>
</tr>
<tr>


<!--BEGIN - ACTUAL VS EXPECTATION-->
<tr>
<td><table align="left" width="600" class="main_header_table" id="tblMaterialHead" cellpadding="2" cellspacing="0">
<tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Revenue Summary (for Orders, RM issued in selected month)</u></td>
</tr>
</table></td>
</tr>
<tr>
<td><form name="frmCostingTimeWise" id="frmMaterial" autocomplete="off">
<table width="600" align="left" border="0" id="tblMaterialBody" class="main_table" cellspacing="0" style="display:none;">
<thead>
<tr>
                                <td colspan="4" style="text-align:left">Revenue Summary (for Orders, RM issued in selected month)<img src="images/close_small.png" align="right" class="mouseover"/>
</td>
</tr>
</thead>
<tbody align="center">
<td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
<tr>

                                        <td><label style="color: #00adee;">SELECT RM ISSUED MONTH :</label> <input name="startDate" id="startDate" class="date-picker-to" /></td>
                                        <span id="validate_date" class="mouseover"></span>
                                        <!--                                            <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReportGraphic2('frmMaterial')"/><img src="images/Treport_exel.png" style="display:inline" border="0" id="butDownload" name="butDownload" /><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>-->
<!--                                        <td><a class="button_href" href="presentation/costing/sample/priceCosting/listing/orderwise_rm_movemnet.php" target="_blank">EXCEL REPORT</a></td>-->
<!--                                            <td><button class="button_href" id="excel_export_matirial_isssued" onclick="matrialIssued();">EXPORT</button></td>-->
<td><img src="images/Treport_exel.png" style="display:inline" border="0"  onclick="matrialIssued();" /></td>
</tr>
</table>
</td>
</tr>
</tbody>
</table></form></td>
</tr>

<!--BEGIN MONTHLY SUMMURY -->
<tr>
<td><table align="left" width="600" class="main_header_table" id="tblMonthlySummeryHead" cellpadding="2" cellspacing="0">
<tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>RM Issuing Summary (for Orders, RM issued in selected month)</u></td>
</tr>
</table></td>
</tr>
<tr>
<td><form name="frmCostingTimeWise" id="frmMonthlySummery" autocomplete="off" action="?q=1258" method="GET" target="_blank">
<table width="600" align="left" border="0" id="tblMonthlySummeryBody" class="main_table" cellspacing="0" style="display:none;">
<thead>
<tr>
                                <td colspan="4" style="text-align:left">RM Issuing Summary (for Orders, RM issued in selected month)<img src="images/close_small.png" align="right" class="mouseover"/>
</td>
</tr>
</thead>
<tbody align="center">
<td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
<tr>
<!-- <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReportGraphic2('frmMaterial')"/><img src="images/Treport_exel.png" style="display:inline" border="0" id="butDownload" name="butDownload" /><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>-->
                                        <td><label style="color: #00adee;">SELECT RM ISSUED MONTH :</label><input name="startDateMonthly" id="startDateMonthly" class="date-picker-to" /></td>
<span id="validate_mothlysummery" class="mouseover"></span>
<!--
<td><a class="button_href" href="presentation/costing/sample/priceCosting/listing/orderwise_monthly_summery.php"  target="_blank">EXCEL REPORT</a></td>-->
<!--href="?q=1258"-->
<!--                                        <td><button class="button_href" id="monthly_summery" onclick="monthlySummery();">EXPORT</button></td>-->
<td><img src="images/Treport_exel.png" style="display:inline" border="0"  onclick="monthlySummery();" /></td>

</tr>
                                    <tr><td><font color="#EB1F39">* Please note that, it will take about 5 to 15 minutes to download the report.</font></td></tr>
</table>
</td>
</tr>
</tbody>
</table></form></td>

  <!-- Daily RM Moved For The Month -->
            <tr>
                <td><table align="left" width="600" class="main_header_table" id="tbldailyRmMovementforMonthheader" cellpadding="2" cellspacing="0">
                        <tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Daily RM (order wise & non order wise) Issuing For The Month</u></td>
                        </tr>
                    </table></td>
            </tr>
                <td><form name="frmdailyRmMovementforMonth" id="frmdailyRmMovementforMonth" autocomplete="off" action="?q=1258" method="GET" target="_blank">
                        <table width="600" align="left" border="0" id="tbldailyRmMovementforMonthBody" class="main_table" cellspacing="0" style="display:none;">
                            <thead>
                            <tr>
                                <td colspan="4" style="text-align:left">Daily RM (order wise & non order wise) Issuing For The Month<img src="images/close_small.png" align="right" class="mouseover"/>
                                </td>
                            </tr>
                            </thead>
                            <tbody align="center">
                            <td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                    <tr>
                                        <!-- <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReportGraphic2('frmMaterial')"/><img src="images/Treport_exel.png" style="display:inline" border="0" id="butDownload" name="butDownload" /><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>-->
                                        <td><label style="color: #00adee;">SELECT RM ISSUED MONTH :</label><input name="dailyrmMovemntforMonth" id="dailyrmMovemntforMonth" class="date-picker-to" /></td>
                                        <span id="validate_dailyrmMovemntforMonth" class="mouseover"></span>
                                        <td><img src="images/Treport_exel.png" style="display:inline" border="0"  onclick="dailyRMmovmentinMonth();" /></td>

                                    </tr>
                                </table>
                            </td>
                            </tr>
                            </tbody>
                        </table></form>
                </td>
            </tr>
            <!--END -->
            <tr>
</tr>
<!--END -->


<!--BEGIN - ORDER BOOK-->
<tr>
<td>
<table align="left" width="600" class="main_header_table" id="tblOrderbookHead" cellpadding="2" cellspacing="0">
<tr>
<td width="544">
<img src="images/report_go.png" class="mouseover" />&nbsp;
<u>Order Book</u>
</td>
</tr>
</table>
</td>
</tr>

<tr>
<td>
<form name="frmOrderbook" id="frmOrderbook" autocomplete="off">
<table width="600" align="left" border="0" id="tblOrderbookBody" class="main_table" cellspacing="0" style="display:none;">
<thead>
<tr>
<td colspan="4" style="text-align:left">Order Book<img src="images/close_small.png" align="right" class="mouseover"/>
</td>
</tr>
</thead>
<tbody align="center">
<td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
<tr>
<td class="normalfnt">Company</td>
<td width="443"  align="left">

<select class="chosen-select clscompany"  data-placeholder="Select Your Option"
style="width:400px; height:50px" tabindex="4" id="cbocompany" >

<?php
$db->connect();
$db->begin();
$html   		= "<option value=\"\"></option>";
$result_company = "select mst_companies.strName,
mst_companies.intId
FROM
mst_companies
where
mst_companies.intStatus ='1'
";
$result = $db->RunQuery($result_company);
while($row=mysqli_fetch_array($result))
{
$html	.=  "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
}
echo $html;
?>
</select>
</td>
<td width="443"  align="left"><span id="validate_company1" name="validate_company1"></span></td>
</tr>


<tr>
<td class="normalfnt">Cluster</td>
<td width="443"  align="left">

<select class="chosen-select clscluster"  data-placeholder="Select Your Option"
style="width:400px; height:50px" tabindex="4" id="cbocluster" >

<?php
$html   		= "<option value=\"\"></option>";
$result_cluster = 
"SELECT sys_main_clusters.ID as ID,
sys_main_clusters.`NAME` as NAME 
FROM
sys_main_clusters WHERE STATUS = 1
ORDER BY
sys_main_clusters.ID
";
$result = $db->RunQuery($result_cluster);
while($row=mysqli_fetch_array($result))
{
$html	.=  "<option value=\"".$row['ID']."\">".$row['NAME']."</option>";
}
echo $html;
?>
</select>
</td>
<td width="443"  align="left"><span id="validate_cluster" name="validate_cluster"></span></td>
</tr>


<tr>
<td class="normalfnt">Plant</td>
<td width="443"  align="left">

<select class="chosen-select clsplant"  data-placeholder="Select Your Option"
style="width:400px; height:50px" tabindex="4" id="cboplant" >

<?php
$html   		= "<option value=\"\"></option>";
$result_plant = 
"SELECT
mst_plant.intPlantId,
mst_plant.strPlantName
from mst_plant WHERE  intStatus = 1
ORDER BY
mst_plant.intPlantId
";
$result = $db->RunQuery($result_plant);
while($row=mysqli_fetch_array($result))
{
$html	.=  "<option value=\"".$row['intPlantId']."\">".$row['strPlantName']."</option>";
}
echo $html;
?>
</select>
</td>
</tr>

<tr>
<td align="left"><input align="right" type="radio" name="radio"  id="create" value="1" checked="checked"> Order created </td>
<td align="left"><input align="right" type="radio" name="radio"  id="dispatch" value="2"> Order dispatched </td>
</tr>

<tr>
 <td width="443"  align="left">Year</td>
<td width="443"  align="left">
<select name="Year" id="cboYear" style="width:70px">
<option value=\"\"></option>
<?php
$d = date('Y');
for($d;$d>=2012;$d--)
{
    echo "<option id=\"$d\" >$d</option>";
}
?>
</select>
</td>
</tr>

<tr>
<td width="443"  align="left" class="cls_deduct">Month</td>
<td width="443"  align="left">
<select id="cboMonth" name="cboMonth" style="width:250px">
<?php 
$sql      = "SELECT intMonthId,strMonth FROM mst_month ORDER BY intMonthId";
$result   = $db->RunQuery($sql);
echo "<option></option>";
while($row = mysqli_fetch_array($result))
{  
echo "<option value=".$row["intMonthId"]." >".$row["strMonth"]."</option>";
}
?>
</select>
</td>
</tr>

<tr>
<td colspan="4" align="center" class="normalfntMid">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
<tr>
<td width="100%" align="center" bgcolor=""><img style="display:inline"
border="0"
src="images/Tnew.jpg"
alt="New" name="butNew"
width="92" height="24"
class="mouseover"
id="butNew"
tabindex="28"/>

<img
style="display:inline" border="0" src="images/Treport.jpg"
alt="Save" name="butReports" class="mouseover" id="butReports"
tabindex="24"
onClick="ViewOrderBookReport('Orderbook','frmOrderbook')"/><a
href="main.php">

<img src="images/Tclose.jpg" alt="Close"
name="butClose" width="92" height="24"
border="0" class="mouseover" id="butClose"
tabindex="27"/></a></td>
</tr>
</table>
</td>
</tr>

</table>
</td>
</tr>
</tbody>
</table>
</form>
</td>
</tr>






</tr>

</table>
<style>
.button_href {
display: block;
width: 99px;
height: 1px;
background: #f1f1ff;
padding: 21px;
text-align: center;
border-radius: 59px;
color: #000000;
font-weight: bold;

}
.button_href:hover {
background-color: #1aa0df;
color: white;
}

.ui-datepicker-calendar {
display: none;
}
</style>
<script>
$(function() {

$('.date-picker-to').datepicker(
{

dateFormat: "yy-mm",
yearRange: "2012",
changeMonth: true,
changeYear: true,
showButtonPanel: true,
onClose: function(dateText, inst) {


function isDonePressed(){
return ($('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > -1);
}

if (isDonePressed()){
var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
$(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');

$('.date-picker').focusout()//Added to remove focus from datepicker input box on selecting date
}
},
beforeShow : function(input, inst) {

inst.dpDiv.addClass('month_year_datepicker')

if ((datestr = $(this).val()).length > 0) {
year = datestr.substring(datestr.length-4, datestr.length);
month = datestr.substring(0, 2);
$(this).datepicker('option', 'defaultDate', new Date(year, month-1, 1));
$(this).datepicker('setDate', new Date(year, month-1, 1));
$(".ui-datepicker-calendar").hide();
}
}
})
});

function matrialIssued() {
//presentation/costing/sample/priceCosting/listing/orderwise_rm_movemnet.php
var startDate = $('#startDate').val();
if(startDate == '') {
$('#validate_date').html('<span id="error" style=\"color:red\">* Please Select Month</span>');
return false;
}

var url = "presentation/costing/sample/priceCosting/listing/orderwise_rm_movemnet.php?startDate="+startDate;
window.open(url, '_blank');

}


function monthlySummery() {
//presentation/costing/sample/priceCosting/listing/orderwise_monthly_summery.php
var startDateMonthly = $('#startDateMonthly').val();
if(startDateMonthly == '') {
//validate_mothlysummery
$('#validate_mothlysummery').html('<span id="error" style=\"color:red\">* Please Select Month</span>');

return false;
}else {
var url = "presentation/costing/sample/priceCosting/listing/orderwise_monthly_summery.php?startDate="+startDateMonthly;
window.open(url, '_blank');
}

            }

            function dailyRMmovmentinMonth() {
                var startDateMonthly = $('#dailyrmMovemntforMonth').val();
                if(startDateMonthly == '') {
                    //validate_mothlysummery
                    $('#validate_dailyrmMovemntforMonth').html('<span id="error" style=\"color:red\">* Please Select Month</span>');

                    return false;
                }else {
                    var url = "presentation/costing/sample/priceCosting/listing/daily_rm_movment_for_month.php?startDate="+startDateMonthly;
                    window.open(url, '_blank');
                }

            }
        </script>
    </div>
    </div>
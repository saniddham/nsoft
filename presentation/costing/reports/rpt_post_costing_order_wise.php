<?php
session_start();
$backwardseperator 		= "../../../";
$companyId 				= $_SESSION['headCompanyId'];
$locationId 			= $_SESSION['CompanyID'];
$userId 				= $_SESSION['userId'];
$mainPath 				= $_SESSION['mainPath'];
$thisFilePath 			= $_SERVER['PHP_SELF'];

include  	"{$backwardseperator}dataAccess/Connector.php";

$orderNo 				= $_REQUEST['Order_No'];
$orderYear				= $_REQUEST['Order_Year'];
$sales_order_id			= $_REQUEST['Sales_Order_Id'];

$invoice_array			= GetSalesInvoice($orderNo,$orderYear);
$payment_array			= GetPayment($orderNo,$orderYear);

//$orderNo 				= '500062';
//$orderYear 			= '2013';

$header_array			= GetReportHeadetDetails($orderNo,$orderYear);

$poLocationName			= $header_array['PO_LOCATION'];
$customerPONo 			= $header_array['CUSTOMER_PONO'];
$orderDate 				= $header_array['ORDER_DATE'];
$deliveryDate 			= $header_array['DELIVERY_DATE'];
$customerName 			= $header_array['CUSTOMER_NAME'];
$currencyCode 			= $header_array['CURRENCY_CODE'];
$currencyId 			= $header_array['CURRENCY_ID'];
$symbol 				= $header_array['SYMBOL'];
$marketer				= $header_array['MARKETER'];
$intStatus				= $header_array['ORDER_STATUS'];
$reviseNo 				= $header_array['REVISION_NO'];
$orderQty				= $header_array['ORDER_QTY'];

$receive_array			= GetFabricReceiveDetails($orderNo,$orderYear);
$recvdQty				= $receive_array['FABRIC_RECEIVE_QTY'];


$dispatch_array			= GetFabricDispatchDetails($orderNo,$orderYear);
$dispatchQty			= $dispatch_array['DISPATCH_QTY'];
$sampleQty				= $dispatch_array['SAMPLE_QTY'];
$goodQty				= $dispatch_array['GOOD_QTY'];
$embroideryQty			= $dispatch_array['EMBROIDERY_QTY'];
$pDammageQty			= $dispatch_array['PROD_DAMMAGE_QTY'];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Post Costing Report</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>
<style>
.break {
	page-break-before: always;
}
 @media print {
.noPrint {
	display: none;
}
}
#apDiv1 {
	position: absolute;
	left: 237px;
	top: 175px;
	width: 619px;
	height: 235px;
	z-index: 1;
}
.APPROVE {
	font-size: 16px;
	font-weight: bold;
	color: #00C;
}
</style>
</head>

<body>
<form id="frmSamplePlaceOrderReport" name="frmSamplePlaceOrderReport" method="post" action="rptBulkOrder.php">
  <table width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="3"></td>
    </tr>
    <tr>
      <td width="20%"></td>
      <td width="60%" height="80" valign="top"><?php include '../../../reportHeader.php'?></td>
      <td width="20%"></td>
    </tr>
    <tr>
      <td colspan="3"><div id="divPoNo" style="display:none"><?php echo $orderNo ?>/<?php echo $orderYear ?></div></td>
    </tr>
  </table>
  <div style="background-color:#FFF" align="center"><strong>POST COSTING</strong><strong></strong></div>
  <table width="1200" border="0" align="center" bgcolor="#FFFFFF">
    <tr>
      <td><table width="100%">
          <tr>
            <td width="1%">&nbsp;</td>
            <td width="8%" class="normalfnt">Order No</td>
            <td width="1%" align="center" valign="middle">:</td>
            <td width="17%"><span class="normalfnt"><?php echo $orderNo ?>/<?php echo $orderYear ?></span></td>
            <td width="9%" class="normalfnt">Date</td>
            <td width="1%" align="center" valign="middle">:</td>
            <td width="12%"><span class="normalfnt"><?php echo $orderDate ?></span></td>
            <td colspan="9" rowspan="5" class="normalfnt"><table width="100%" border="0" align="center" class="tableBorder">
                <tr>
                  <td width="21%" class="normalfnt"><strong><strong>Order Qty</strong></strong></td>
                  <td width="2%" align="center" valign="middle"><strong>:</strong></td>
                  <td width="14%" class="normalfnt"><strong><?php echo $orderQty; ?></strong></td>
                  <td width="18%" class="normalfnt"><strong>Fabric In Qty</strong></td>
                  <td width="2%"><span class="normalfnt"><strong>:</strong></span></td>
                  <td width="11%"><span class="normalfnt"> <strong>
                    <?php  echo $recvdQty; ?>
                    </strong> </span></td>
                  <td width="23%" class="normalfnt"><strong><u>Tot Dispatch Qty</u></strong></td>
                  <td width="2%" align="center" valign="middle"><strong>:</strong></td>
                  <td width="7%"><strong> <u>
                    <?php  echo $recvdQty; ?>
                    </u> </strong></td>
                </tr>
                <tr>
                  <td width="21%">Invoice Qty</td>
                  <td width="2%"><span class="normalfnt">:</span></td>
                  <td width="14%"><?php echo number_format($invoice_array["INVOICE_QTY"],0);?></td>
                  <td>&nbsp;</td>
                  <td></td>
                  <td>&nbsp;</td>
                  <td>Sample Qty</td>
                  <td width="2%"><span class="normalfnt">:</span></td>
                  <td   class="normalfnt"><?php  echo $sampleQty; ?></td>
                </tr>
                <tr>
                  <td>Invoice Value</td>
                  <td><span class="normalfnt">:</span></td>
                  <td   class="normalfnt"><?php echo number_format($invoice_array["INVOICE_AMOUNT"],2);?></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>Good Qty</td>
                  <td width="2%"><span class="normalfnt">:</span></td>
                  <td><?php  echo $goodQty; ?></td>
                </tr>
                <tr>
                  <td>Payment Recived</td>
                  <td>:</td>
                  <td   class="normalfnt"><?php echo number_format($payment_array["PAY_RECEIVE_AMOUNT"],2) ?></td>
                  <td   class="normalfnt">&nbsp;</td>
                  <td   class="normalfnt">&nbsp;</td>
                  <td   class="normalfnt">&nbsp;</td>
                  <td class="normalfnt">Embroidery Qty</td>
                  <td width="2%"><span class="normalfnt">:</span></td>
                  <td><?php  echo $embroideryQty; ?></td>
                </tr>
                <tr>
                  <td class="normalfnt">&nbsp;</td>
                  <td align="center" valign="middle">&nbsp;</td>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt">PDammage Qty</td>
                  <td width="2%"><span class="normalfnt">:</span></td>
                  <td class="normalfnt"><?php  echo $pDammageQty; ?></td>
                </tr>
              </table></td>
            <td width="6%" class="normalfnt">&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="normalfnt">Customer</td>
            <td align="center" valign="middle">:</td>
            <td><span class="normalfnt"><?php echo $customerName ?></span></td>
            <td width="9%" class="normalfnt">Delivery Date</td>
            <td width="1%" align="center" valign="middle">:</td>
            <td width="12%"><span class="normalfnt"><?php echo $deliveryDate ?></span></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="normalfnt">Cust PO No</td>
            <td align="center" valign="middle">:</td>
            <td class="normalfnt"><?php echo $customerPONo; ?></td>
            <td><span class="normalfnt">Marketer</span></td>
            <td align="center" valign="middle">:</td>
            <td><span class="normalfnt"><?php echo $marketer;?></span></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="normalfnt">Currency</td>
            <td align="center" valign="middle">:</td>
            <td class="normalfnt"><?php echo $currencyCode; ?></td>
            <td class="normalfnt">PO Revise No</td>
            <td align="center" valign="middle">:</td>
            <td class="normalfnt"><?php echo $reviseNo; ?></td>
            <td   class="normalfnt">&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="normalfnt">Location</td>
            <td align="center" valign="middle">:</td>
            <td class="normalfnt"><?php echo $poLocationName; ?></td>
            <td class="normalfnt"></td>
            <td align="center" valign="middle">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td><table width="100%">
          <tr>
            <td colspan="7" class="normalfnt"><table width="506" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
                <tr class="">
                  <th width="25%" >Style No</th>
                  <th width="41%" >Graphic No</th>
                  <th width="34%" >Graphic</th>
                </tr>
                <?php 
			$tmpSalesId		= '';
			$tmpGraphic		= '';
			$tmpSampleNo	= '';
			$tmpSampleYear	= '';
			$tmpPlacement	= '';
			$tmpCombo		= '';
			$tmpRevNo		= '';
			
	  	 $sql1 = "SELECT   
					OD.strGraphicNo,
					OD.strStyleNo , 
					OD.intSampleNo,
					OD.intSampleYear,
					OD.intRevisionNo 
				FROM
					trn_orderdetails OD
				WHERE
					OD.intOrderNo	= '$orderNo' AND
					OD.intOrderYear	= '$orderYear' ";
		if($sales_order_id!='')				
			$sql1 .= " AND OD.intSalesOrderId = '$sales_order_id'"; 
			 
			$sql1 .= "GROUP BY strStyleNo,strGraphicNo";
			$result1 = $db->RunQuery($sql1);
			$tmpStyle='';
			while($row=mysqli_fetch_array($result1))
			{
				$sampleNo		= $row['intSampleNo'];
				$sampleYear		= $row['intSampleYear'];
				$revNo			= $row['intRevisionNo'];
			?>
                <tr class="normalfnt"  bgcolor="#FFFFFF">
                  <td class="normalfnt" >&nbsp;
                    <?php if($tmpStyle!=$row['strStyleNo']){ echo $row['strStyleNo'];} ?>
                    &nbsp;</td>
                  <td class="normalfnt" >&nbsp;<?php echo $row['strGraphicNo'] ?>&nbsp;</td>
                  <td class="normalfnt" style="text-align:center"><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:264px;height:148px;overflow:hidden;text-align:center" >
                      <?php
			if($sampleNo!='')
			{
				echo "<img id=\"saveimg\" style=\"width:264px;height:148px;text-align:center\" src=\"../../../documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revNo-1.png\" />";	
			}
			 ?>
                    </div></td>
                </tr>
                <?php   
				$tmpStyle	= $row['strStyleNo'];
			}
	        ?>
              </table></td>
            <td width="6%">&nbsp;</td>
          </tr>
          
            <td class="normalfnt" colspan="7" align="left"></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="7" class="normalfnt"><table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
                <tr class="">
                  <th colspan="12" align="left"><b>DRM - Direct Raw Materials</b></th>
                </tr>
                <tr class="">
                  <th colspan="3">&nbsp;</th>
                  <th colspan="4">Pre Costing</th>
                  <th colspan="5" align="right" >Post Costing</th>
                </tr>
                <tr class="">
                  <th width="7%"  height="22">Style No</th>
                  <th width="35%">Item</th>
                  <th width="3%">UOM</th>
                  <th width="6%">Sample ConPC</th>
                  <th width="7%">Expected Consump</th>
                  <th width="4%"> Rate <?php echo '('.$symbol.')' ?></th>
                  <th width="7%">Expected Cost <?php echo '('.$symbol.')' ?></th>
                  <th width="6%" >Bulk Consump</th>
                  <th width="6%" >Wastage</th>
                  <th width="4%" > Rate <?php echo '('.$symbol.')' ?></th>
                  <th width="5%" >Actual Cost <?php echo '('.$symbol.')' ?></th>
                  <th width="6%" >Wastage Cost <?php echo '('.$symbol.')' ?></th>
                </tr>
<?php
			$totPreConPC		= 0;
			$totPreConsump		= 0;
			$totPreCost			= 0;
			$totPostConPC		= 0;
			$totPostConsump		= 0;
			$totPostCost		= 0;
			$totWastage			= 0;
			$totWastageCost		= 0;

			$result1 = getSampleInkConsumptions($orderNo,$orderYear,$currencyId,$sales_order_id);
			while($row=mysqli_fetch_array($result1))
			{
?>
                <tr class="normalfnt"  bgcolor="#FFFFFF">
                  <td class="normalfnt" >&nbsp;<?php echo $row["STYLE_NO"] ?>&nbsp;</td>
                  <td class="normalfnt" id="<?php echo $row["intItem"]?>"><?php echo $row['itemName'] ?>&nbsp;</td>
                  <td class="normalfnt" ><?php echo $row['unitName'] ?></td>
                  <td class="normalfntRight" align="right" ><?php echo $row['consum'] ?>&nbsp;</td>
                  <td class="normalfntRight"  align="right"><?php echo round($row['consum']*$dispatchQty,4); ?></td>
                  <td class="normalfntRight" align="right" ><?php echo round($row['dblLastPrice'],4); ?></td>
                  <td class="normalfntRight"  align="right"><?php echo round($row['consum']*$dispatchQty*$row['dblLastPrice'],4); ?></td>
                  <?php
				$result2 = getBulkConsumptions($orderNo,$orderYear,$currencyId,$row['intItem']);
				$row2 = mysqli_fetch_array($result2);
				
				$bulkConpc			= GetBulkConpc($orderNo,$orderYear,$sales_order_id,$row['intItem'],$orderQty);
				$postOrderWastage	= GetPostOrderWastage($orderNo,$orderYear,$sales_order_id,$row['intItem'],$orderQty);
				$postOrderCost		= GetPostOrderRate($orderNo,$orderYear,$sales_order_id,$row['intItem'],$orderQty); 
			  ?>
                  <td class="normalfntRight" align="right" >&nbsp;<?php echo number_format($bulkConpc,4)?></td>
                  <td class="normalfntRight" align="right" >&nbsp;<?php echo number_format($postOrderWastage,4)?></td>
                  <td class="normalfntRight" align="right" >&nbsp;<?php echo number_format($postOrderCost/$bulkConpc,4) ?>&nbsp;</td>
                  <td class="normalfntRight" align="right" >&nbsp;<?php echo number_format($postOrderCost,4) ?>&nbsp;</td>
                  <td class="normalfntRight" align="right" >&nbsp;<?php echo number_format(($postOrderWastage*($postOrderCost/$bulkConpc)),4) ?>&nbsp;</td>
                </tr>
                <?php
				$totPreConPC 		+= $row['consum'];
				$totPreConsump 		+= round($row['consum']*$dispatchQty,4);
				$totPreCost 		+= round($row['consum']*$dispatchQty*$row['dblLastPrice'],4);
				$totPostConPC 		+= round($row2['Consump']/$dispatchQty,6);
				$totPostConsump 	+= round($row2['Consump'],4);
				$totPostCost 		+= round($row2['Cost'],4);
				$totWastage 		+= round($row2['wastage'],4);
				$totWastageCost 	+= round($row2['wastageCost'],4);
			}
			?>
                <?php 
			//special RM consumptions
			$result1 = getSpecialRMConsumptions($orderNo,$orderYear,$currencyId,$sales_order_id);
			while($row=mysqli_fetch_array($result1))
			{
	  ?>
                <tr class="normalfnt"  bgcolor="#FFFFFF">
                  <td class="normalfnt" >&nbsp;<?php echo $row["STYLE_NO"] ?>&nbsp;</td>
                  <td class="normalfnt" id="<?php echo $row["itemId"]?>"><?php echo $row['itemName'] ?>&nbsp;</td>
                  <td class="normalfnt" ><?php echo $row['unitName'] ?></td>
                  <td class="normalfntRight" align="right" ><?php echo $row['consum'] ?>&nbsp;</td>
                  <td class="normalfntRight"  align="right"><?php echo round($row['consum']*$dispatchQty,4); ?></td>
                  <td class="normalfntRight" align="right" ><?php echo round($row['dblLastPrice'],4); ?></td>
                  <td class="normalfntRight"  align="right"><?php echo round($row['consum']*$dispatchQty*$row['dblLastPrice'],4); ?></td>
                  <?php
				$result2 = getBulkConsumptions($orderNo,$orderYear,$currencyId,$row['intItem']);
				$row2=mysqli_fetch_array($result2);
				$bulkConpc			= GetBulkConpc($orderNo,$orderYear,$sales_order_id,$row['intItem'],$orderQty);
			  ?>
                  <td class="normalfntRight" align="right" >&nbsp;<?php echo round($bulkConpc,4) ?>&nbsp;</td>
                  <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['wastage'],4) ?>&nbsp;</td>
                  <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['Cost']/$row2['Consump'],4) ?>&nbsp;</td>
                  <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['Cost'],4) ?>&nbsp;</td>
                  <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['wastageCost'],4) ?>&nbsp;</td>
                </tr>
                <?php
				$totPreConPC 		+= $row['consum'];
				$totPreConsump 		+= round($row['consum']*$dispatchQty,4);
				$totPreCost 		+= round($row['consum']*$dispatchQty*$row['dblLastPrice'],4);
				$totPostConPC 		+= round($row2['Consump']/$dispatchQty,6);
				$totPostConsump 	+= round($row2['Consump'],4);
				$totPostCost		+= round($row2['Cost'],4);
				$totWastage 		+= round($row2['wastage'],4);
				$totWastageCost 	+= round($row2['wastageCost'],4);
			}
			?>
                <?php 
			//Foil consumptions
			$result1 = getFoilConsumptions($orderNo,$orderYear,$currencyId,$sales_order_id);
			while($row=mysqli_fetch_array($result1))
			{
	  ?>
                <tr class="normalfnt"  bgcolor="#FFFFFF">
                  <td class="normalfnt" >&nbsp;<?php echo $row["STYLE_NO"] ?>&nbsp;</td>
                  <td class="normalfnt" ><?php echo $row['itemName'] ?>&nbsp;</td>
                  <td class="normalfnt" ><?php echo $row['unitName'] ?></td>
                  <td class="normalfntRight" align="right" ><?php echo $row['consum'] ?>&nbsp;</td>
                  <td class="normalfntRight"  align="right"><?php echo round($row['consum']*$dispatchQty,4); ?></td>
                  <td class="normalfntRight" align="right" ><?php echo round($row['dblLastPrice'],4); ?></td>
                  <td class="normalfntRight"  align="right"><?php echo round($row['consum']*$dispatchQty*$row['dblLastPrice'],4); ?></td>
                  <?php
				$result2 = getBulkConsumptions($orderNo,$orderYear,$currencyId,$row['intItem']);
				$row2=mysqli_fetch_array($result2);
				$bulkConpc			= GetBulkConpc($orderNo,$orderYear,$sales_order_id,$row['intItem'],$orderQty);
			  ?>
                  <td class="normalfntRight" align="right" >&nbsp;<?php echo round($bulkConpc,4) ?>&nbsp;</td>
                  <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['wastage'],4) ?>&nbsp;</td>
                  <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['Cost']/$row2['Consump'],4) ?>&nbsp;</td>
                  <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['Cost'],4) ?>&nbsp;</td>
                  <td class="normalfntRight" align="right" >&nbsp;<?php echo round($row2['wastageCost'],4) ?>&nbsp;</td>
                </tr>
                <?php
				$totPreConPC 		+= $row['consum'];
				$totPreConsump 		+= round($row['consum']*$dispatchQty,4);
				$totPreCost 		+= round($row['consum']*$dispatchQty*$row['dblLastPrice'],4);
				$totPostConPC 		+= round($row2['Consump']/$dispatchQty,6);
				$totPostConsump 	+= round($row2['Consump'],4);
				$totPostCost 		+= round($row2['Cost'],4);
				$totWastage 		+= round($row2['wastage'],4);
				$totWastageCost 	+= round($row2['wastageCost'],4);
			}
			?>
                <tr class="normalfnt"  bgcolor="#F9F9F9">
                  <td class="normalfnt" >&nbsp;&nbsp;</td>
                  <td class="normalfnt" >&nbsp;</td>
                  <td class="normalfnt" ></td>
                  <td class="normalfntRight" ><?php echo $totPreConPC; ?>&nbsp;</td>
                  <td class="normalfntRight" ><?php echo round($totPreConsump,4); ?></td>
                  <td class="normalfnt" ></td>
                  <td class="normalfntRight" ><b><?php echo round($totPreCost,4); ?></b></td>
                  <td class="normalfntRight" align="right" >&nbsp;<?php echo round($totPostConsump,4) ?>&nbsp;</td>
                  <td class="normalfntRight" align="right" >&nbsp;<?php echo round($totWastage,4) ?>&nbsp;</td>
                  <td class="normalfntRight" align="right" >&nbsp;&nbsp;</td>
                  <td class="normalfntRight" align="right" >&nbsp;<b><?php echo round($totPostCost,4) ?></b>&nbsp;</td>
                  <td class="normalfntRight" align="right" >&nbsp;<b><?php echo round($totWastageCost,4) ?></b>&nbsp;</td>
                </tr>
              </table></td>
            <td width="6%">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="7" class="normalfnt"><table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
                <tr class="">
                  <th colspan="7" align="left"><b>Direct Labour Cost</b></th>
                </tr>
              </table></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="7" class="normalfnt"><table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
                <tr class="">
                  <th colspan="7" align="left"><b>Overhead Consumption</b></th>
                </tr>
                <tr class="">
                  <th width="4%"  height="22">&nbsp;</th>
                  <th width="33%">Type</th>
                  <th width="20%">Expense up to Oct'09</th>
                  <th width="10%">No'of months</th>
                  <th width="11%">Avg. expense</th>
                  <th width="11%"> Avg Production</th>
                  <th width="11%">Cost per unit </th>
                </tr>
                <?php
			$result1 = getOverheadConsumptions();
			$i=0;
			while($row=mysqli_fetch_array($result1))
			{
				$i++;
	  ?>
                <tr class="normalfnt"  bgcolor="#FFFFFF">
                  <td class="normalfnt" >&nbsp;<?php echo $i ?>&nbsp;</td>
                  <td class="normalfnt" ><?php echo $row['strName'] ?>&nbsp;</td>
                  <td class="normalfnt" ><?php echo $row['xxx'] ?></td>
                  <td class="normalfntRight" align="right" ><?php echo $row['xxx'] ?>&nbsp;</td>
                  <td class="normalfntRight"  align="right"><?php echo round($row['xxx'],4); ?></td>
                  <td class="normalfntRight" align="right" ><?php echo round($row['xxx'],4); ?></td>
                  <td class="normalfntRight"  align="right"><?php echo round($row['xxx'],4); ?></td>
                </tr>
                <?php
			}
			?>
                <tr class="normalfnt"  bgcolor="#F9F9F9">
                  <td class="normalfnt" >&nbsp;&nbsp;</td>
                  <td class="normalfnt" >&nbsp;</td>
                  <td class="normalfnt" ></td>
                  <td class="normalfntRight" ><?php echo $xxx; ?>&nbsp;</td>
                  <td class="normalfntRight" ><?php echo round($xxx,4); ?></td>
                  <td class="normalfnt" ></td>
                  <td class="normalfntRight" ><b><?php echo round($xxx,4); ?></b></td>
                </tr>
              </table></td>
            <td></td>
          </tr>
        </table></td>
    </tr>
    <tr height="40" >
      <td align="center" class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></td>
      <td align="center" class="normalfntMid">&nbsp;</td>
    </tr>
  </table>
</form>
</body>
</html>
<?php
	//--------------------------------------------------------------
function getSampleInkConsumptions($orderNo,$orderYear,$currencyId,$sales_order_id)
{
	global $db;
	if($sales_order_id!='')
		$para .= " AND OD.intSalesOrderId = '$sales_order_id'";

	$sql = "SELECT 
				mainId,
				mainCat,
				subId,
				subCat,
				intItem,
				itemName,
				unitId,
				unitName,
				SUM(ROUND(dblColorWeight/sumWeight*dblWeight /dblNoOfPcs,6))as consum,
				SUM(ROUND(dblColorWeight/sumWeight*dblWeight * (intQty) /dblNoOfPcs,6)) as totalQty,
				dblLastPrice,
				STYLE_NO 
			FROM
			(SELECT
				OD.intOrderNo,
				ROUND(((I.dblLastPrice*mst_financeexchangerate.dblBuying)/(SELECT
					mst_financeexchangerate.dblBuying 
				FROM
					mst_financeexchangerate 
					Inner Join mst_financecurrency 
						ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId
				WHERE
					mst_financeexchangerate.intCurrencyId =  '$currencyId' AND
					mst_financeexchangerate.intCompanyId =  L.intCompanyId AND
					mst_financeexchangerate.dtmDate = trn_sampleinfomations.dtDate
				)),4) as dblLastPrice,
			OD.intOrderYear,
			OD.strSalesOrderNo,
			OD.intSalesOrderId,
			OD.intSampleNo,
			OD.intSampleYear,
			OD.strStyleNo		AS STYLE_NO,
			OD.strCombo,
			OD.strPrintName,
			OD.intRevisionNo,
			OD.intPart,
			OD.intQty,
			TSCR.intItem,
			TSCR.dblWeight,
			trn_sampleinfomations_details_technical.intInkTypeId,
			trn_sampleinfomations_details_technical.dblColorWeight,
			TSCR.intTechniqueId,
			I.strName itemName,
			I.intId AS intId,
			mst_units.strName AS unitName,
			mst_units.intId AS unitId,
			mst_units.dblNoOfPcs ,
			mst_maincategory.strName AS mainCat,
			mst_maincategory.intId AS mainId,
			mst_subcategory.strName AS subCat,
			mst_subcategory.intId AS subId,			
			(SELECT
				Sum(TSCR_SUB.dblWeight) AS sumWeight
			FROM trn_sample_color_recipes TSCR_SUB
			WHERE
				TSCR_SUB.intSampleNo = OD.intSampleNo AND
				TSCR_SUB.intSampleYear = OD.intSampleYear AND
				TSCR_SUB.intRevisionNo =  OD.intRevisionNo AND
				TSCR_SUB.strCombo =  OD.strCombo AND
				TSCR_SUB.strPrintName =  OD.strPrintName AND
				TSCR_SUB.intTechniqueId =  TSCR.intTechniqueId AND
				TSCR_SUB.intInkTypeId =  trn_sampleinfomations_details_technical.intInkTypeId AND
				TSCR_SUB.intColorId =  trn_sampleinfomations_details_technical.intColorId 
			)  as sumWeight 
		FROM trn_orderdetails OD
		INNER JOIN trn_sampleinfomations 
			ON trn_sampleinfomations.intSampleNo = OD.intSampleNo 
			AND trn_sampleinfomations.intSampleYear = OD.intSampleYear 
			AND trn_sampleinfomations.intRevisionNo = OD.intRevisionNo 
			AND trn_sampleinfomations.strGraphicRefNo = OD.strGraphicNo
		INNER JOIN trn_sample_color_recipes TSCR
			ON TSCR.intSampleNo = OD.intSampleNo 
			AND TSCR.intSampleYear = OD.intSampleYear 
			AND TSCR.intRevisionNo = OD.intRevisionNo 
			AND TSCR.strCombo = OD.strCombo 
			AND TSCR.strPrintName = OD.strPrintName
		INNER JOIN trn_sampleinfomations_details_technical 
			ON trn_sampleinfomations_details_technical.intSampleNo = TSCR.intSampleNo 
			AND trn_sampleinfomations_details_technical.intSampleYear = TSCR.intSampleYear 
			AND trn_sampleinfomations_details_technical.intRevNo = TSCR.intRevisionNo 
			AND trn_sampleinfomations_details_technical.strComboName = TSCR.strCombo 
			AND trn_sampleinfomations_details_technical.strPrintName = TSCR.strPrintName 
			AND trn_sampleinfomations_details_technical.intColorId = TSCR.intColorId 
			AND trn_sampleinfomations_details_technical.intInkTypeId = TSCR.intInkTypeId
		INNER JOIN mst_item I
			ON I.intId = TSCR.intItem
		INNER JOIN mst_locations L
			ON trn_sampleinfomations.intCompanyId = L.intId 
		LEFT JOIN mst_financeexchangerate 
			ON I.intCurrency = mst_financeexchangerate.intCurrencyId 
			AND trn_sampleinfomations.dtDate = mst_financeexchangerate.dtmDate 
			AND L.intCompanyId = mst_financeexchangerate.intCompanyId 
		INNER JOIN mst_units 
			ON I.intUOM = mst_units.intId
		INNER JOIN mst_maincategory 
			ON mst_maincategory.intId = I.intMainCategory
		INNER JOIN mst_subcategory 
			ON mst_subcategory.intId = I.intSubCategory
		WHERE
			OD.intOrderNo =  '$orderNo'
			AND OD.intOrderYear =  '$orderYear' 
			$para
		) as t 
		GROUP BY mainId,mainCat,subId,SubCat, intItem,itemName";
	return $result = $db->RunQuery($sql);
}

function getSpecialRMConsumptions($orderNo,$orderYear,$currencyId,$sales_order_id)
{
	global $db;
		
	if($sales_order_id!='')
		$para .= " AND OD.intSalesOrderId = '$sales_order_id'";		
	
	$sql = "SELECT
				OD.strStyleNo		AS STYLE_NO,
				mst_maincategory.intId AS mainId,
				mst_maincategory.strName AS mainCat,
				mst_subcategory.intId AS subId,
				mst_subcategory.strName AS subCat,
				mst_item.intId AS itemId,
				mst_item.strName AS itemName,
				mst_units.intId AS unitId,
				mst_units.strName AS unitName,
				sum(trn_sample_spitem_consumption.dblQty/dblNoOfPcs) as consum,
				mst_units.dblNoOfPcs,
				round(((mst_item.dblLastPrice*mst_financeexchangerate.dblBuying)/(SELECT
						mst_financeexchangerate.dblBuying 
					FROM
						mst_financeexchangerate 
						Inner Join mst_financecurrency ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId
					WHERE
						mst_financeexchangerate.intCurrencyId =  '$currencyId' AND
						mst_financeexchangerate.intCompanyId =  L.intCompanyId AND
						mst_financeexchangerate.dtmDate =  trn_sampleinfomations.dtDate
				)),4) as dblLastPrice,
				mst_financecurrency.strCode AS currencyName,
				sum((OD.intQty) * trn_sample_spitem_consumption.dblQty /dblNoOfPcs) as totalQty
			FROM
				trn_orderdetails OD
			INNER JOIN trn_sampleinfomations 
				ON trn_sampleinfomations.intSampleNo = OD.intSampleNo 
				AND trn_sampleinfomations.intSampleYear = OD.intSampleYear 
				AND trn_sampleinfomations.intRevisionNo = OD.intRevisionNo 
				AND trn_sampleinfomations.strGraphicRefNo = OD.strGraphicNo
			Inner Join trn_sample_spitem_consumption 
				ON trn_sample_spitem_consumption.intSampleNo = OD.intSampleNo 
				AND trn_sample_spitem_consumption.intSampleYear = OD.intSampleYear 
				AND trn_sample_spitem_consumption.intRevisionNo = OD.intRevisionNo 
				AND trn_sample_spitem_consumption.strCombo = OD.strCombo 
				AND trn_sample_spitem_consumption.strPrintName = OD.strPrintName
			Inner Join mst_item 
				ON mst_item.intId = trn_sample_spitem_consumption.intItem
			Inner Join mst_locations L 
				ON trn_sampleinfomations.intCompanyId = L.intId 
			left Join mst_financeexchangerate 
				ON mst_item.intCurrency = mst_financeexchangerate.intCurrencyId 
				AND trn_sampleinfomations.dtDate = mst_financeexchangerate.dtmDate 
				AND L.intCompanyId = mst_financeexchangerate.intCompanyId 
			Inner Join mst_maincategory 
				ON mst_maincategory.intId = mst_item.intMainCategory
			Inner Join mst_subcategory 
				ON mst_subcategory.intId = mst_item.intSubCategory
			Inner Join mst_units 
				ON mst_units.intId = mst_item.intUOM
			Inner Join mst_financecurrency 
				ON mst_financecurrency.intId = mst_item.intCurrency
			WHERE
				OD.intOrderNo =  '$orderNo'
				AND OD.intOrderYear =  '$orderYear'
				$para
			GROUP BY
				mst_maincategory.intId,
				mst_subcategory.intId,
				mst_item.intId";
				
		return $result = $db->RunQuery($sql);
}

function getFoilConsumptions($orderNo,$orderYear,$currencyId,$sales_order_id)
{
	global $db;
	if($sales_order_id!='')
		$para .= " AND OD.intSalesOrderId = '$sales_order_id'";	
		
 	$sql = "SELECT
				OD.strStyleNo					AS STYLE_NO,
				mst_item.intId 					AS itemId,
				mst_maincategory.strName 		AS mainCat,
				mst_subcategory.strName 		AS subCat,
				mst_maincategory.intId 			AS mainId,
				mst_subcategory.intId 			AS subId,
				mst_item.strName 				AS itemName,
				mst_units.intId,
				mst_units.strName 				AS unitName,
				mst_financecurrency.strCode 	AS currencyName,
				sum(OD.intQty*trn_sample_foil_consumption.dblMeters ) AS totalQty,
				sum(trn_sample_foil_consumption.dblMeters) AS consum,
				mst_units.dblNoOfPcs,
			round(((mst_item.dblLastPrice*mst_financeexchangerate.dblBuying)/(SELECT
					mst_financeexchangerate.dblBuying 
				FROM
					mst_financeexchangerate 
					Inner Join mst_financecurrency ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId
				WHERE
					mst_financeexchangerate.intCurrencyId =  '$currencyId' AND
					mst_financeexchangerate.intCompanyId =  L.intCompanyId AND
					mst_financeexchangerate.dtmDate =  trn_sampleinfomations.dtDate
			)),4) 								AS dblLastPrice 
			FROM
				trn_orderdetails OD
				INNER JOIN trn_sampleinfomations 
					ON trn_sampleinfomations.intSampleNo = OD.intSampleNo 
					AND trn_sampleinfomations.intSampleYear = OD.intSampleYear 
					AND trn_sampleinfomations.intRevisionNo = OD.intRevisionNo 
					AND trn_sampleinfomations.strGraphicRefNo = OD.strGraphicNo
				Inner Join trn_sample_foil_consumption 
					ON trn_sample_foil_consumption.intSampleNo = OD.intSampleNo 
					AND trn_sample_foil_consumption.intSampleYear = OD.intSampleYear 
					AND trn_sample_foil_consumption.intRevisionNo = OD.intRevisionNo
					AND trn_sample_foil_consumption.strCombo = OD.strCombo 
					AND trn_sample_foil_consumption.strPrintName = OD.strPrintName
				Inner Join mst_item 
					ON mst_item.intId = trn_sample_foil_consumption.intItem
				Inner Join mst_locations L 
					ON trn_sampleinfomations.intCompanyId = L.intId 
				left Join mst_financeexchangerate 
					ON mst_item.intCurrency = mst_financeexchangerate.intCurrencyId 
					AND trn_sampleinfomations.dtDate = mst_financeexchangerate.dtmDate 
					AND L.intCompanyId = mst_financeexchangerate.intCompanyId 
				Inner Join mst_maincategory 
					ON mst_maincategory.intId = mst_item.intMainCategory
				Inner Join mst_subcategory 
					ON mst_subcategory.intId = mst_item.intSubCategory
				Inner Join mst_units 
					ON mst_units.intId = mst_item.intUOM
				Inner Join mst_financecurrency 
					ON mst_financecurrency.intId = mst_item.intCurrency
			WHERE
				OD.intOrderNo = '$orderNo' AND
				OD.intOrderYear = '$orderYear' 
				$para
			GROUP BY
				mst_maincategory.intId,
				mst_subcategory.intId,
				mst_item.intId";
		return $result = $db->RunQuery($sql);
}

function getBulkConsumptions($orderNo,$orderYear,$currencyId,$itemId)
{	
	global $db;
	$sql1 = "SELECT 
				ROUND(SUM(tb3.wastageCost),6) 							AS wastageCost,
				ROUND(SUM(tb3.wastage),6) 								AS wastage,
				ROUND(SUM(tb3.Consump),6) 								AS Consump,
				ROUND(SUM(tb3.Cost),6) 									AS Cost 
			FROM ((SELECT 
				 0 														AS wastageCost,
				 0 														AS wastage,
			IFNULL(sum(tb1.Cost/FER1.dblBuying),0) 						AS Cost,
			IFNULL(sum(tb1.Consump),0) 									AS Consump 
			
			 from (SELECT 
			(ST.dblQty * ST.dblGRNRate * FER.dblBuying) 				AS Cost,
			(ST.dblQty) 												AS Consump,
			date(ST.dtDate) 											AS date,
			LO.intCompanyId 
			FROM
			ware_stocktransactions ST
			LEFT JOIN mst_locations LO
				ON ST.intLocationId = LO.intId
			LEFT JOIN trn_orderheader OH
				ON ST.intOrderNo = OH.intOrderNo 
				AND ST.intOrderYear = OH.intOrderYear
			LEFT JOIN trn_orderdetails OD
				ON OH.intOrderNo = OD.intOrderNo 
				AND OH.intOrderYear = OD.intOrderYear 
				AND ST.intSalesOrderId = OD.intSalesOrderId
			LEFT JOIN mst_financeexchangerate FER
				ON ST.intCurrencyId = FER.intCurrencyId 
				AND date(ST.dtDate) = FER.dtmDate 
				AND LO.intCompanyId = FER.intCompanyId
			WHERE
			OD.intOrderNo 	=  '$orderNo' AND
			OD.intOrderYear 	=  '$orderYear' AND 
			ST.intItemId = '$itemId' AND 
			ST.strType IN ('ALLOCATE','UNALLOCATE')	";

	$sql1 .= " /*group by strStyleNo,strGraphicNo*/ ) as tb1 
			LEFT JOIN mst_financeexchangerate FER1 ON $currencyId = FER1.intCurrencyId 
				AND tb1.date = FER1.dtmDate 
				AND tb1.intCompanyId = FER1.intCompanyId)

UNION
(select 
 
IFNULL(sum(table2.wastageCost/mst_financeexchangerate.dblBuying),0) as wastageCost,
IFNULL(sum(table2.wastage),0) as wastage,
IFNULL(sum(table2.Cost/mst_financeexchangerate.dblBuying),0) as Cost,
IFNULL(sum(table2.Consump),0) as Consump 
from 
(
select 
(table1.wastage*table1.dblGRNRate*mst_financeexchangerate.dblBuying) AS wastageCost,
(table1.wastage) AS wastage,
(table1.consump*table1.dblGRNRate*mst_financeexchangerate.dblBuying) AS Cost,
(table1.consump) AS Consump,
table1.dblGRNRate,
date(table1.dtGRNDate) as date,
mst_locations.intCompanyId 

 from (
SELECT 

Sum(ware_sub_stocktransactions_bulk.dblQty)/
(select Sum(sub.dblQty) 
from ware_sub_stocktransactions_bulk as sub 
where ware_sub_stocktransactions_bulk.intOrderNo=sub.intOrderNo AND 
ware_sub_stocktransactions_bulk.intOrderYear=sub.intOrderYear AND 
ware_sub_stocktransactions_bulk.intSalesOrderId=sub.intSalesOrderId AND 
ware_sub_stocktransactions_bulk.intColorId=sub.intColorId AND 
ware_sub_stocktransactions_bulk.intTechnique=sub.intTechnique AND 
ware_sub_stocktransactions_bulk.intInkType=sub.intInkType) /*as itemConsump,*/
*
(SELECT
IFNULL(Sum(subCol.dblQty*-1),0) 
FROM `ware_sub_color_stocktransactions_bulk` as subCol 
WHERE
subCol.strType = 'WASTAGE' AND 
ware_sub_stocktransactions_bulk.intOrderNo=subCol.intOrderNo AND 
ware_sub_stocktransactions_bulk.intOrderYear=subCol.intOrderYear AND 
ware_sub_stocktransactions_bulk.intSalesOrderId=subCol.intSalesOrderId AND 
ware_sub_stocktransactions_bulk.intColorId=subCol.intColorId AND 
ware_sub_stocktransactions_bulk.intTechnique=subCol.intTechnique AND 
ware_sub_stocktransactions_bulk.intInkType=subCol.intInkType) as wastage, 




Sum(ware_sub_stocktransactions_bulk.dblQty)/
(select Sum(sub.dblQty) 
from ware_sub_stocktransactions_bulk as sub 
where ware_sub_stocktransactions_bulk.intOrderNo=sub.intOrderNo AND 
ware_sub_stocktransactions_bulk.intOrderYear=sub.intOrderYear AND 
ware_sub_stocktransactions_bulk.intSalesOrderId=sub.intSalesOrderId AND 
ware_sub_stocktransactions_bulk.intColorId=sub.intColorId AND 
ware_sub_stocktransactions_bulk.intTechnique=sub.intTechnique AND 
ware_sub_stocktransactions_bulk.intInkType=sub.intInkType) /*as itemConsump,*/
*
((SELECT
IFNULL(Sum(subCol.dblQty*-1),0) 
FROM `ware_sub_color_stocktransactions_bulk` as subCol 
WHERE
subCol.strType = 'ISSUE' AND 
ware_sub_stocktransactions_bulk.intOrderNo=subCol.intOrderNo AND 
ware_sub_stocktransactions_bulk.intOrderYear=subCol.intOrderYear AND 
ware_sub_stocktransactions_bulk.intSalesOrderId=subCol.intSalesOrderId AND 
ware_sub_stocktransactions_bulk.intColorId=subCol.intColorId AND 
ware_sub_stocktransactions_bulk.intTechnique=subCol.intTechnique AND 
ware_sub_stocktransactions_bulk.intInkType=subCol.intInkType)
-((SELECT
IFNULL(Sum(subCol.dblQty),0) 
FROM `ware_sub_color_stocktransactions_bulk` as subCol 
WHERE
subCol.strType = 'RETURN' AND 
ware_sub_stocktransactions_bulk.intOrderNo=subCol.intOrderNo AND 
ware_sub_stocktransactions_bulk.intOrderYear=subCol.intOrderYear AND 
ware_sub_stocktransactions_bulk.intSalesOrderId=subCol.intSalesOrderId AND 
ware_sub_stocktransactions_bulk.intColorId=subCol.intColorId AND 
ware_sub_stocktransactions_bulk.intTechnique=subCol.intTechnique AND 
ware_sub_stocktransactions_bulk.intInkType=subCol.intInkType))
-((SELECT
IFNULL(Sum(subCol.dblQty*-1),0) 
FROM `ware_sub_color_stocktransactions_bulk` as subCol 
WHERE
subCol.strType = 'WASTAGE' AND 
ware_sub_stocktransactions_bulk.intOrderNo=subCol.intOrderNo AND 
ware_sub_stocktransactions_bulk.intOrderYear=subCol.intOrderYear AND 
ware_sub_stocktransactions_bulk.intSalesOrderId=subCol.intSalesOrderId AND 
ware_sub_stocktransactions_bulk.intColorId=subCol.intColorId AND 
ware_sub_stocktransactions_bulk.intTechnique=subCol.intTechnique AND 
ware_sub_stocktransactions_bulk.intInkType=subCol.intInkType))) /*as allocatedInk*/ as consump,

ware_sub_stocktransactions_bulk.intLocationId,
ware_sub_stocktransactions_bulk.intOrderNo,
ware_sub_stocktransactions_bulk.intOrderYear,
ware_sub_stocktransactions_bulk.intSalesOrderId,
ware_sub_stocktransactions_bulk.intColorId,
ware_sub_stocktransactions_bulk.intTechnique,
ware_sub_stocktransactions_bulk.intInkType,
ware_sub_stocktransactions_bulk.intItemId,
ware_sub_stocktransactions_bulk.dblGRNRate,
ware_sub_stocktransactions_bulk.dtGRNDate,
ware_sub_stocktransactions_bulk.intCurrencyId
FROM `ware_sub_stocktransactions_bulk` 

WHERE
ware_sub_stocktransactions_bulk.intItemId = '$itemId' AND 
ware_sub_stocktransactions_bulk.strType IN ('INK_ALLOC')
";

$sql1 .= " GROUP BY 
ware_sub_stocktransactions_bulk.intLocationId, 
ware_sub_stocktransactions_bulk.intOrderNo,
ware_sub_stocktransactions_bulk.intOrderYear,
ware_sub_stocktransactions_bulk.intSalesOrderId,
ware_sub_stocktransactions_bulk.intColorId,
ware_sub_stocktransactions_bulk.intTechnique,
ware_sub_stocktransactions_bulk.intInkType,
ware_sub_stocktransactions_bulk.dblGRNRate,
ware_sub_stocktransactions_bulk.dtGRNDate,
ware_sub_stocktransactions_bulk.intCurrencyId ) as table1 

left JOIN mst_locations ON table1.intLocationId = mst_locations.intId
left JOIN trn_orderheader ON table1.intOrderNo = trn_orderheader.intOrderNo AND table1.intOrderYear = trn_orderheader.intOrderYear
left JOIN trn_orderdetails OD ON trn_orderheader.intOrderNo = OD.intOrderNo AND trn_orderheader.intOrderYear = OD.intOrderYear 
AND table1.intSalesOrderId = OD.intSalesOrderId
left JOIN mst_financeexchangerate ON table1.intCurrencyId = mst_financeexchangerate.intCurrencyId 
AND date(table1.dtGRNDate) = mst_financeexchangerate.dtmDate AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId 
where  
OD.intOrderNo 	=  '$orderNo' AND
OD.intOrderYear 	=  '$orderYear'  ";  

$sql1 .= " ) as table2 

left JOIN mst_financeexchangerate ON $currencyId = mst_financeexchangerate.intCurrencyId 
AND table2.date = mst_financeexchangerate.dtmDate 
AND table2.intCompanyId = mst_financeexchangerate.intCompanyId)) as tb3";	
		return $result = $db->RunQuery($sql1);
}
	
function getOverheadConsumptions()
{
	global $db;
	$sql1 = "SELECT
				mst_overhead_types.intId,
				mst_overhead_types.strName
			FROM `mst_overhead_types`
			WHERE
				mst_overhead_types.intStatus = 1
			ORDER BY
				mst_overhead_types.strName ASC";
	return $result = $db->RunQuery($sql1);
}
	
//BEGIN -
function GetSalesInvoice($orderNo,$orderYear)
{
	global $db;
	
	$sql = "SELECT
		  ROUND(COALESCE(SUM(FCID.QTY),0),2) 	AS INVOICE_QTY,
		  ROUND(COALESCE(SUM(FCID.VALUE),0),2) 	AS INVOICE_AMOUNT
		FROM finance_customer_invoice_header FCIH
		  INNER JOIN finance_customer_invoice_details FCID
			ON FCID.SERIAL_NO = FCIH.SERIAL_NO
			  AND FCID.SERIAL_YEAR = FCIH.SERIAL_YEAR
		WHERE ORDER_NO = $orderNo
			AND ORDER_YEAR = $orderYear";
	$result	= $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	return $row;
	
}

function GetPayment($orderNo,$orderYear)
{
	global $db;
	
	$sql = "SELECT
			  ROUND(COALESCE(SUM(CPRD.PAY_AMOUNT),0),2) AS PAY_RECEIVE_AMOUNT
			FROM finance_customer_pay_receive_header CPRH
			  INNER JOIN finance_customer_pay_receive_details CPRD
				ON CPRD.RECEIPT_NO = CPRH.RECEIPT_NO
				  AND CPRD.RECEIPT_YEAR = CPRH.RECEIPT_YEAR
			  INNER JOIN finance_customer_invoice_header CIH
				ON CIH.SERIAL_NO = CPRD.INVOICE_NO
				  AND CIH.SERIAL_YEAR = CPRD.INVOICE_YEAR
			WHERE CIH.ORDER_NO = $orderNo
				AND CIH.ORDER_YEAR = $orderYear";
	$result	= $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	return $row;
}

function GetReportHeadetDetails($orderNo,$orderYear)
{
	global $db;
	
	$sql = "SELECT
				OH.strCustomerPoNo						AS CUSTOMER_PONO,
				OH.dtDate								AS ORDER_DATE,
				OH.dtDeliveryDate						AS DELIVERY_DATE,
				CU.strName 								AS CUSTOMER_NAME,
				OH.intCurrency 							AS CURRENCY_ID, 
				FC.strCode								AS CURRENCY_CODE,
				FC.strSymbol 							AS SYMBOL,   				
				A.strUserName 							AS MARKETER,
				OH.intStatus							AS ORDER_STATUS,
				OH.intReviseNo							AS REVISION_NO, 
				LO.intCompanyId,
				LO.strName 								AS PO_LOCATION,
				SUM(OD.intQty)							AS ORDER_QTY
			FROM
				trn_orderheader OH
				INNER JOIN trn_orderdetails OD ON OD.intOrderNo = OH.intOrderNo AND OD.intOrderYear = OH.intOrderYear
				INNER JOIN mst_customer CU ON CU.intId = OH.intCustomer
				INNER JOIN mst_customer_locations_header CL ON CL.intId = OH.intCustomerLocation
				INNER JOIN mst_financecurrency FC ON FC.intId = OH.intCurrency
				INNER JOIN sys_users AS A ON A.intUserId = OH.intMarketer
				INNER JOIN sys_users AS B ON B.intUserId = OH.intCreator
				INNER JOIN mst_locations LO ON LO.intId = OH.intLocationId 
			WHERE
				OH.intOrderNo 		= '$orderNo' AND
				OH.intOrderYear 	= '$orderYear'
			GROUP BY OH.intOrderNo,OH.intOrderYear";
	$result	= $db->RunQuery($sql);
	return mysqli_fetch_array($result);
}

function GetFabricReceiveDetails($orderNo,$orderYear)
{
	global $db;
	
	$sql = "SELECT 
				SUM(WFRD.dblQty) as FABRIC_RECEIVE_QTY
			FROM
			trn_orderdetails OD
			Inner Join ware_fabricreceivedheader  WFRH
				ON WFRH.intOrderNo = OD.intOrderNo 
				AND WFRH.intOrderYear = OD.intOrderYear
			Inner Join ware_fabricreceiveddetails WFRD
				ON WFRH.intFabricReceivedNo = WFRD.intFabricReceivedNo 
				AND WFRH.intFabricReceivedYear = WFRD.intFabricReceivedYear 
			AND WFRD.intSalesOrderId = OD.intSalesOrderId
			WHERE
				OD.intOrderNo 		= '$orderNo' AND
				OD.intOrderYear 	= '$orderYear' ";
	$result	= $db->RunQuery($sql);
	return mysqli_fetch_array($result);
}

function GetFabricDispatchDetails($orderNo,$orderYear)
{
	global $db;
	
	$sql = "SELECT 
			sum(WFDD.dblSampleQty) 																AS SAMPLE_QTY,
			sum(WFDD.dblGoodQty) 																AS GOOD_QTY,
			sum(WFDD.dblEmbroideryQty) 															AS EMBROIDERY_QTY,
			sum(WFDD.dblPDammageQty) 															AS PROD_DAMMAGE_QTY,
			sum(WFDD.dblSampleQty+WFDD.dblGoodQty+WFDD.dblEmbroideryQty+WFDD.dblPDammageQty) 	AS DISPATCH_QTY 
			FROM
			trn_orderdetails OD
			Inner Join ware_fabricdispatchheader  WFDH
				ON WFDH.intOrderNo = OD.intOrderNo 
				AND WFDH.intOrderYear = OD.intOrderYear
			Inner Join ware_fabricdispatchdetails WFDD
				ON WFDH.intBulkDispatchNo = WFDD.intBulkDispatchNo 
				AND WFDH.intBulkDispatchNoYear = WFDD.intBulkDispatchNoYear 
				AND WFDD.intSalesOrderId = OD.intSalesOrderId
			WHERE
			OD.intOrderNo 	=  '$orderNo' AND
			OD.intOrderYear 	=  '$orderYear' ";
	$result	= $db->RunQuery($sql);
	return mysqli_fetch_array($result);
}

function GetBulkConpc($orderNo,$orderYear,$sales_order_id,$itemId,$orderQty)
{
	global $db;
	
	$sql = "SELECT 
				ROUND(COALESCE((SELECT SUM(ST.dblQty)
			FROM ware_stocktransactions ST
			WHERE ST.intOrderNo = '$orderNo'
				AND ST.intOrderYear = '$orderYear'
				AND ST.intSalesOrderId = '$sales_order_id'
				AND ST.intItemId = $itemId
				AND ST.strType = 'ISSUE'),0),2) -
				
				ROUND(COALESCE((SELECT SUM(ST.dblQty)
			FROM ware_stocktransactions ST
			WHERE ST.intOrderNo = '$orderNo'
				AND ST.intOrderYear = '$orderYear'
				AND ST.intSalesOrderId = '$sales_order_id'
				AND ST.intItemId = $itemId
				AND ST.strType = 'RETURN'),0),2) AS ISSUE";
	$result	= $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	return round($row["ISSUE"],2)/$orderQty;	
}

function GetPostOrderWastage($orderNo,$orderYear,$sales_order_id,$itemId,$orderQty)
{
	global $db;
	
/*	$sql = "SELECT
			  IFNULL(SUM(subCol.dblQty*-1),0)	AS WASTAGE
			FROM `ware_sub_color_stocktransactions_bulk` AS subCol
			INNER JOIN ware_sub_stocktransactions_bulk
				ON ware_sub_stocktransactions_bulk.intOrderNo = subCol.intOrderNo
				AND ware_sub_stocktransactions_bulk.intOrderYear = subCol.intOrderYear
				AND ware_sub_stocktransactions_bulk.intSalesOrderId = subCol.intSalesOrderId
				AND ware_sub_stocktransactions_bulk.intColorId = subCol.intColorId
				AND ware_sub_stocktransactions_bulk.intTechnique = subCol.intTechnique
				AND ware_sub_stocktransactions_bulk.intInkType = subCol.intInkType
			WHERE subCol.strType = 'WASTAGE'
			AND ware_sub_stocktransactions_bulk.intOrderNo = '$orderNo'
			AND ware_sub_stocktransactions_bulk.intOrderYear = '$orderYear'
			AND ware_sub_stocktransactions_bulk.intSalesOrderId = '$sales_order_id'
			AND ware_sub_stocktransactions_bulk.intItemId = '$itemId'";*/
			
	$sql = "SELECT 
				ROUND(SUM(T1.WASTAGE/T1.ALLOCATED_QTY),4) AS WASTAGE
			FROM
			(SELECT
			  COALESCE((SELECT ABS(SUM(WSCSB.dblQty)) 
			  FROM ware_sub_color_stocktransactions_bulk WSCSB 
			  WHERE WSCSB.intOrderNo = WSSB.intOrderNo 
				AND WSCSB.intOrderYear = WSSB.intOrderYear 
				AND WSCSB.intSalesOrderId = WSSB.intSalesOrderId 
				AND WSCSB.intColorId = WSSB.intColorId 
				AND WSCSB.intTechnique = WSSB.intTechnique 
				AND WSCSB.intInkType = WSSB.intInkType 
				AND strType = 'WASTAGE'),0) AS WASTAGE,
			  ABS(SUM(WSSB.dblQty)) AS ALLOCATED_QTY
			FROM ware_sub_stocktransactions_bulk WSSB
			WHERE WSSB.intOrderNo = '$orderNo'
				AND WSSB.intOrderYear = '$orderYear'
				AND WSSB.intSalesOrderId = '$sales_order_id'
				AND WSSB.intItemId = '$itemId'
				AND WSSB.strType = 'INK_ALLOC'
			GROUP BY WSSB.intOrderNo,WSSB.intOrderYear,WSSB.intSalesOrderId,WSSB.intColorId,WSSB.intTechnique,WSSB.intInkType) AS T1";
	$result	= $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	return round($row["WASTAGE"]/$orderQty,4);
}

function GetPostOrderRate($orderNo,$orderYear,$sales_order_id,$itemId,$orderQty)
{
	global $db;
	
	$sql = "SELECT 
		ROUND(COALESCE((SELECT SUM(ST.dblQty * ST.dblGRNRate)
		FROM ware_stocktransactions ST
		WHERE ST.intOrderNo = '$orderNo'
			AND ST.intOrderYear = '$orderYear'
			AND ST.intSalesOrderId = '$sales_order_id'
			AND ST.intItemId = $itemId
			AND ST.strType = 'ISSUE'),0),2) -
			
			ROUND(COALESCE((SELECT SUM(ST.dblQty * ST.dblGRNRate)
		FROM ware_stocktransactions ST
		WHERE ST.intOrderNo = '$orderNo'
			AND ST.intOrderYear = '$orderYear'
			AND ST.intSalesOrderId = '$sales_order_id'
			AND ST.intItemId = $itemId
			AND ST.strType = 'RETURN'),0),2) AS ISSUE";
	$result	= $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	return round($row["ISSUE"],2)/$orderQty;
}
//END	-
?>
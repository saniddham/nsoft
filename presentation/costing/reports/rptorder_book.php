<?php
//ini_set('display_errors',1);
set_time_limit(600);
session_start();

$companyId = $sessions->getCompanyId();
$userId = $sessions->getUserId();
$locationId = $sessions->getLocationId();

$programCode = 'P1265';

$company = $_REQUEST['company'];
$cluster = $_REQUEST['cluster'];
$plant = $_REQUEST['plant'];
$year = $_REQUEST['year'];
$month = $_REQUEST['month'];
$order_created = $_REQUEST['order_created'];
$order_dispatched = $_REQUEST['order_dispatched'];
$STEAKER_PRESSING_PROCESS = 14;
?>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Order Book</title>


        <style>
            .break {
                page-break-before: always;
            }

            @media print {
                .noPrint {
                    display: none;
                }
            }

            #apDiv1 {
                position: absolute;
                left: 237px;
                top: 175px;
                width: 619px;
                height: 235px;
                z-index: 1;
            }

            .APPROVE {
                font-size: 16px;
                font-weight: bold;
                color: #00C;
            }

            .foo {
                float: left;
                width: 20px;
                height: 20px;
                margin: 1px;
                border: 1px solid rgba(0, 0, 0, .2);
            }

            .blue {
                background: #71ede4;
            }

            .red {
                background: #FF99CC;
            }

            div.dataTables_length {
                padding-left: 2em;
            }

            div.dataTables_length,
            div.dataTables_filter {
                padding-top: 0.55em;
            }

            div.dt-buttons {
                position: relative !important;
                float: left !important;
            }

            #tblMainGrid_paginate {
                position: absolute;
                top: -54px;
                right: 1009px;
                width: 436px;
                height: 100px;
            }

            .type_2 {
                background-color: #FF99CC !important;
            }

            .stock {
                background-color: #71ede4 !important;
            }

            .dataTables_paginate {
                margin-top: 15px;
                position: absolute;
                text-align: right;
                left: 30%;
            }
        </style>


        <script src="libraries/dataTables/jquery.dataTables.min.js"></script>
        <script src="libraries/dataTables/dataTables.buttons.min.js"></script>
        <script src="libraries/dataTables/buttons.flash.min.js"></script>
        <script src="libraries/dataTables/jszip.min.js"></script>
        <script src="libraries/dataTables/pdfmake.min.js"></script>
        <script src="libraries/dataTables/vfs_fonts.js"></script>
        <script src="libraries/dataTables/buttons.html5.min.js"></script>
        <script src="libraries/dataTables/buttons.print.min.js "></script>

        <link rel="stylesheet" type="text/css" href="libraries/dataTables/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="libraries/dataTables/css/buttons.dataTables.min.css">

        <script type="text/javascript">

            $(document).ready(function () {

                var table = $('#tblMainGrid').DataTable({


                    "bFilter": false,
                    pageLength: 50,
                    responsive: true,
                    "scrollY": 600,
                    "scrollX": true,
                    dom: 'Bfrtip',
                    buttons: [{
                        extend: 'print',
                        text: 'Print',
                        exportOptions: {
                            columns: ':not(.no-print)'
                        },
                        footer: true,
                        autoPrint: false
                    }, {

                        extend: 'pdfHtml5',
                        messageTop: 'ORDER BOOK',
                        orientation: 'landscape',
                        pageSize: 'LEGAL',
                        text: 'PDF',


                    }, {

                        extend: 'excel',
                        messageTop: 'ORDER BOOK',
                        text: 'Excel',

                    },
                        {

                            extend: 'csv',
                            text: 'CSV',
                            customize: function (csv) {

                                return '\t ORDER BOOK\n' + csv;
                            },
                            exportOptions: {
                                columns: ':not(.no-print)'
                            }


                        }],
                    "createdRow": function (row, data, dataIndex) {
// alert(data[0]);

                        if (data[12].replace(/[\$,]/g, '') * 1 < data[15]) {

                            $(row).find('td:eq(12)').addClass('stock');
                        }


                    },
//invisible type column on table
                    "columnDefs": [
                        {
                            "targets": 0,
//"visible": false,
                            "searchable": false
                        }
                    ]
// table.column( 0 ).visible( false );
                });


            });
        </script>
    </head>

<body>
<form id="frmOrderBook" name="frmOrderBook" method="post" action="off">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3">
            </td>
        </tr>

        <tr>
            <td width="20%"></td>
            <td width="60%" height="80"
                valign="top"><?php include 'presentation/procurement/reports/reportHeader.php' ?></td>
            <td width="20%"></td>
        </tr>
    </table>

    <div align="center">
    <div style="background-color:#FFF"><strong>Order Book</strong></div>

    <table width="900" border="0" align="center" bgcolor="#FFFFFF">
    <tr>
        <td>

            <table width="100%">
                <tr>
                    <td colspan="10" align="center" bgcolor="#FFFFFF">
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>

        </td>
    </tr>
    <tr>
        <td>
            <table width="100%">
                <tr>
                    <td colspan="20">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    <td>
    <br/>
    <table width="100%">
    <tr>
    <td colspan="20">
    <div>
    <table width="100%" class="table table-striped table-bordered bordered" id="tblMainGrid" cellspacing="0"
           cellpadding="0">

    <thead>
    <tr class="">
        <th class="tbheader">
            <div>Customer</div>
        </th>
        <th class="tbheader">
            <div>Marketer</div>
        </th>
        <th class="tbheader">
            <div>Style No</div>
        </th>
        <th class="tbheader">
            <div>Graphic No</div>
        </th>
        <th class="tbheader">
            <div>Order NO</div>
        </th>
        <th class="tbheader">
            <div>Customer Location</div>
        </th>
        <th class="tbheader">
            <div>Location (indicated in the Customer PO)</div>
        </th>
        <th class="tbheader">
            <div>Dispatched location</div>
        </th>
        <th class="tbheader">
            <div>Status</div>
        </th>
        <th class="tbheader">
            <div>Order Type</div>
        </th>
        <th class="tbheader">
            <div>Pre costing No</div>
        </th>
        <th class="tbheader">
            <div>Pre costing Year</div>
        </th>
        <th class="tbheader">
            <div>Start Production Without PO</div>
        </th>
        <th class="tbheader">
            <div>Order created date</div>
        </th>
        <th class="tbheader">
            <div>Order created month
                <div>
        </th>
        <th class="tbheader">
            <div>Sales Order NO</div>
        </th>
        <th class="tbheader">
            <div>Technique group</div>
        </th>
        <th class="tbheader">
            <div>Print</div>
        </th>
        <th class="tbheader">
            <div>Part</div>
        </th>
        <th class="tbheader">
            <div>Combo</div>
        </th>
        <!-- <th class="tbheader"><div>Shots</div></th>-->
        <th class="tbheader">
            <div>Brand</div>
        </th>
        <th class="tbheader">
            <div>Order Qty</div>
        </th>
        <th class="tbheader">
            <div>Price</div>
        </th>
        <th class="tbheader">
            <div>Sticker Price</div>
        </th>
        <th class="tbheader">
            <div>Currancy</div>
        </th>
        <th class="tbheader">
            <div>Order Amount</div>
        </th>
        <th class="tbheader">
            <div>Sticker Amount</div>
        </th>
        <th class="tbheader">
            <div>Fabric received Qty</div>
        </th>
        <th class="tbheader">
            <div>Dispatched Good Qty</div>
        </th>
        <th class="tbheader">
            <div>Dispatched SD Qty</div>
        </th>
        <th class="tbheader">
            <div>Dispatched PD Qty</div>
        </th>
        <th class="tbheader">
            <div>Dispatched FD Qty</div>
        </th>
        <th class="tbheader">
            <div>Dispatched Cut-return Qty</div>
        </th>
        <th class="tbheader">
            <div>Dispatched Qty</div>
        </th>
        <th class="tbheader">
            <div>Dispatched month</div>
        </th>
    </tr>
    </thead>

<?php
$query = get_order_book_details($company, $cluster, $plant, $year, $month, $order_created, $order_dispatched, $STEAKER_PRESSING_PROCESS); // suvini
$result = $db->RunQuery($query);
while ($row = mysqli_fetch_array($result)) {
//---------------------------ORDER STATUS--------------------------------
    if ($row['ORDER_STATUS'] == 1) {
        $order_status = 'Approved';
    } else if ($row['ORDER_STATUS'] == 0) {
        $order_status = 'Rejected';
    } else if ($row['ORDER_STATUS'] == -10) {
        $order_status = 'Completed';
    } else if ($row['ORDER_STATUS'] == -2) {
        $order_status = 'Cancel';
    } else if ($row['ORDER_STATUS'] == -1) {
        $order_status = 'Revised';
    } else {
        $order_status = 'Pending';
    }
//---------------------------------------------------------------------------------

//-----------------------------------ORDER_TYPE------------------------------------
    if ($row['ORDER_TYPE'] == 1) {
        $order_type = 'Dummy';
    } else if ($row['ORDER_TYPE'] == 2) {
        $order_type = 'Acctual';
    } else {
        $order_type = 'Other';
    }
//---------------------------------------------------------------------------------
//---------------------------instant_order----------------------------------------
    if ($row['INSTANT_ORDER'] == 1) {
        $start_production_without_PO = 'Instant Order';
    }

    if ($row['SO_TYPE'] != 0 ) {
        unset($row['pressing_price']);
    }

//--------------------------------------------------------------------------------
    ?>

    <tr bgcolor="$col">

        <td><?php echo $row['CUSTOMER']; ?></td>
        <td><?php echo $row['MARKETER']; ?></td>
        <td><?php echo $row['STYLE']; ?></td>
        <td><?php echo $row['GRAPHIC_NO']; ?></td>
        <td><?php echo $row['ORDER_NO']; ?></td>
        <td><?php echo $row['customer_location']; ?></td>
        <td><?php echo $row['location_in_PO']; ?></td>
        <td><?php echo $row['DISP_loc']; ?></td>
        <td><?php echo $order_status; ?></td>
        <td><?php echo $order_type; ?></td>
        <td><?php echo $row['DUMMY_PO_NO']; ?></td>
        <td><?php echo $row['DUMMY_PO_YEAR']; ?></td>
        <td><?php echo $start_production_without_PO; ?></td>
        <td><?php echo $row['ORDER_CREATED_DATE']; ?></td>
        <td><?php echo $row['ORDER_CREATED_MONTH']; ?></td>
        <td><?php echo $row['SALES_ORDER_NO']; ?></td>
        <td><?php echo $row['STECHNIQUE_GRP']; ?></td>
        <td><?php echo $row['PRINT']; ?></td>
        <td><?php echo $row['PART']; ?></td>
        <td><?php echo $row['COMBO']; ?></td>
        <!--<td>  //echo $row['SHOTS']; </td> -->
        <td><?php echo $row['BRAND']; ?></td>
        <td><?php echo $row['ORDER_QTY']; ?></td>
        <td><?php echo $row['PRICE']; ?></td>
        <td><?php
            if (isset($row['pressing_price'])){
                echo $row['PRICE'] - $row['pressing_price'];
            } else {
                echo 0;
            }
        ?></td>
        <td><?php echo $row['intCurrency']; ?></td>
        <td><?php echo $row['AMOUNT']; ?></td>
        <td><?php
            if (isset($row['pressing_price'])){
                echo ($row['PRICE'] - $row['pressing_price'])*$row['ORDER_QTY'];
            } else {
                echo 0;
            }
             ?></td>
        <td><?php echo $row['FABRIC_RECEIVED_QTY']; ?></td>
        <td><?php echo $row['DISPATCH_GOOD']; ?></td>
        <td><?php echo $row['DISPATCH_SAMPLE']; ?></td>
        <td><?php echo $row['DISPATCH_PD']; ?></td>
        <td><?php echo $row['DISPATCH_FABRIC']; ?></td>
        <td><?php echo $row['DISPATCH_CUT_RET']; ?></td>
        <td><?php echo $row['DISPATCH_QTY']; ?></td>
        <td><?php echo $row['DISPATCH_MONTH']; ?></td>


          </tr>

    <?php
}

?>
<?php


function get_order_book_details($company, $cluster, $plant, $year, $month, $order_created, $order_dispatched, $stk_press_pro )
{

    global $db;

    $sql =
        "select 
sys_users.strFullName AS MARKETER ,
mst_customer.strName as CUSTOMER,
mst_customer_locations_header.strName as customer_location,
mst_locations.strName AS LOCATION,
t.strName as location_in_PO,
mst_plant.strPlantName AS PLANT ,
trn_orderheader.intOrderNo AS ORDER_NO,
trn_orderheader.intOrderYear AS ORDER_YEAR,
trn_orderheader.intStatus as ORDER_STATUS,
trn_orderheader.dtDate AS DATE,
trn_orderheader.strCustomerPoNo AS CUSTOMER_PO,
trn_orderheader.intStatus as ORDER_STATUS,
trn_orderheader.PO_TYPE as ORDER_TYPE,
trn_orderheader.DUMMY_PO_NO,
trn_orderheader.DUMMY_PO_YEAR,
trn_orderheader.INSTANT_ORDER,
trn_orderheader.dtmCreateDate as ORDER_CREATED_DATE,
mst_month.strMonth AS ORDER_CREATED_MONTH,

mst_technique_groups.TECHNIQUE_GROUP_NAME AS STECHNIQUE_GRP,
trn_orderdetails.strSalesOrderNo AS SALES_ORDER_NO,
trn_orderdetails.strGraphicNo AS GRAPHIC_NO,
trn_orderdetails.strStyleNo AS STYLE,
trn_orderdetails.strCombo AS COMBO,
trn_orderdetails.strPrintName AS PRINT,
mst_part.strName as PART,
mst_brand.strName as BRAND ,

mst_financecurrency.strCode as CURRENCY,

(select sum(T.intQty) from trn_orderdetails as T 
WHERE trn_orderdetails.intOrderNo=T.intOrderNo 
AND trn_orderdetails.intOrderYear = T.intOrderYear
AND trn_orderdetails.intSalesOrderId = T.intSalesOrderId 
AND trn_orderdetails.strGraphicNo = T.strGraphicNo 
AND trn_orderdetails.strStyleNo = T.strStyleNo 
) AS ORDER_QTY, 
(select sum(T.intQty*T.dblPrice) from trn_orderdetails as T 
WHERE trn_orderdetails.intOrderNo=T.intOrderNo 
AND trn_orderdetails.intOrderYear = T.intOrderYear
AND trn_orderdetails.intSalesOrderId = T.intSalesOrderId 
AND trn_orderdetails.strGraphicNo = T.strGraphicNo 
AND trn_orderdetails.strStyleNo = T.strStyleNo 
) AS AMOUNT, 
sum(if(ware_stocktransactions_fabric.strType='Dispatched_P',ware_stocktransactions_fabric.dblQty*-1,0)) AS DISPATCH_PD,
sum(if(ware_stocktransactions_fabric.strType='Dispatched_G',ware_stocktransactions_fabric.dblQty*-1,0)) AS DISPATCH_GOOD,
sum(if(ware_stocktransactions_fabric.strType='Dispatched_S',ware_stocktransactions_fabric.dblQty*-1,0)) AS DISPATCH_SAMPLE,
sum(if(ware_stocktransactions_fabric.strType='Dispatched_F',ware_stocktransactions_fabric.dblQty*-1,0)) AS DISPATCH_FABRIC,
sum(if(ware_stocktransactions_fabric.strType='Dispatched_CUT_RET',ware_stocktransactions_fabric.dblQty*-1,0)) AS DISPATCH_CUT_RET,
sum(

		IF (
			ware_stocktransactions_fabric.strType LIKE '%Dispatched%',
			ware_stocktransactions_fabric.dblQty *- 1,
			0
		)
	) AS DISPATCH_QTY,
sum(if(ware_stocktransactions_fabric.strType='Dispatched_G',ware_stocktransactions_fabric.dblQty*trn_orderdetails.dblPrice*-1,0)) AS REVENUE,


'' as x,
group_concat(distinct if(ware_stocktransactions_fabric.strType LIKE '%Dispatched%',date(ware_stocktransactions_fabric.dtDate),'')) AS DISP_DATE,

group_concat(
						DISTINCT
						IF (
							ware_stocktransactions_fabric.strType LIKE '%Dispatched%',
(SELECT
mst_month.strMonth
FROM
mst_month
WHERE
mst_month.intMonthId = MONTH (
								ware_stocktransactions_fabric.dtDate
							)),
							''
						)
			
	) AS DISPATCH_MONTH,

	group_concat(
		DISTINCT
		IF (
			ware_stocktransactions_fabric.strType LIKE '%Dispatched%',
			
(select mst_locations.strName
FROM
mst_locations
WHERE
ware_stocktransactions_fabric.intLocationId = mst_locations.intId),
			''
		)
	) AS DISP_loc,


sum(

		IF (
			ware_stocktransactions_fabric.strType = 'Received',
			ware_stocktransactions_fabric.dblQty,
			0
		)
	) AS FABRIC_RECEIVED_QTY,

trn_orderheader.intCurrency,
trn_orderdetails.dblPrice AS PRICE,
costing_sample_routing_process.COST AS pressing_price,
trn_orderheader.dtDate AS ORDER_DATE ,
trn_orderheader.intCustomer,
trn_orderdetails.SO_TYPE AS SO_TYPE

FROM
ware_stocktransactions_fabric
left JOIN mst_locations ON ware_stocktransactions_fabric.intLocationId = mst_locations.intId
left JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId
left JOIN trn_orderheader ON ware_stocktransactions_fabric.intOrderNo = trn_orderheader.intOrderNo AND ware_stocktransactions_fabric.intOrderYear = trn_orderheader.intOrderYear
left JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_stocktransactions_fabric.intSalesOrderId = trn_orderdetails.intSalesOrderId
left JOIN trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
left JOIN mst_marketer ON trn_orderheader.intMarketer = mst_marketer.intUserId
left JOIN sys_users ON trn_orderheader.intMarketer = sys_users.intUserId
left JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
LEFT JOIN mst_part  ON trn_orderdetails.intPart  = mst_part.intId
left JOIN mst_financecurrency ON trn_orderheader.intCurrency = mst_financecurrency.intId
left JOIN mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
LEFT JOIN mst_technique_groups ON trn_orderdetails.TECHNIQUE_GROUP_ID= mst_technique_groups.TECHNIQUE_GROUP_ID
LEFT JOIN mst_customer_locations_header  ON mst_customer_locations_header.intId= trn_orderheader.intCustomerLocation
LEFT JOIN mst_locations as t on t.intId = trn_orderheader.intLocationId
LEFT JOIN costing_sample_routing_process ON trn_orderdetails.intSampleNo = costing_sample_routing_process.SAMPLE_NO
AND costing_sample_routing_process.SAMPLE_YEAR = trn_orderdetails.intSampleYear
AND costing_sample_routing_process.REVISION = trn_orderdetails.intRevisionNo
AND costing_sample_routing_process.PRINT = trn_orderdetails.strPrintName
AND costing_sample_routing_process.COMBO = trn_orderdetails.strCombo
AND costing_sample_routing_process.PROCESS_ID = '$stk_press_pro'
INNER JOIN mst_month on  mst_month.intMonthId = MONTH (trn_orderheader.dtmCreateDate) 

WHERE  
mst_locations.intCompanyId	 = '$company' AND
mst_plant.MAIN_CLUSTER_ID 	 = '$cluster' AND ";

    if ($plant != "")
        $sql .= " mst_plant.intPlantId =  '$plant' AND 
";

    if ($order_dispatched == 2) {
        $sql .=
            " YEAR(ware_stocktransactions_fabric.dtDate) 	= '$year' 
AND MONTH(ware_stocktransactions_fabric.dtDate) = '$month'
AND	ware_stocktransactions_fabric.strType 		
LIKE '%Dispatched%' ";
    }
    if ($order_created == 1) {
        $sql .=
            " YEAR(trn_orderheader.dtmCreateDate)		= '$year'	
AND MONTH(trn_orderheader.dtmCreateDate)	= '$month'	";
    }
    $sql .= "
GROUP BY 
trn_orderheader.intOrderNo,
trn_orderheader.intOrderYear,
trn_orderdetails.intSalesOrderId,
trn_orderheader.intMarketer,
trn_sampleinfomations.intBrand,
trn_orderdetails.strStyleNo,
trn_orderdetails.strGraphicNo,
ware_stocktransactions_fabric.intLocationId  
ORDER BY
mst_locations.strName ASC , 
sys_users.strFullName ASC, 
mst_customer.strName ASC, 
trn_orderheader.intOrderNo ASC,
trn_orderheader.intOrderYear ASC,
trn_orderdetails.strSalesOrderNo ASC";
//echo  $sql;
    return $sql;

}

?>

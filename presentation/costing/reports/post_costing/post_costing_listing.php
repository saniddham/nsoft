<?php
ini_set('display_errors',0);
session_start();
$companyId				= $_SESSION["headCompanyId"];
$backwardseperator 		= "../../../";
require_once "../../../dataAccess/Connector.php";
require_once("../../../libraries/jqgrid2/inc/jqgrid_dist.php");

$g 						= new jqgrid();

$col 					= array();
$col["title"] 			= "Customer Name";
$col["name"] 			= "CUSTOMER_NAME";
$col["width"] 			= "50";
$col["hidden"] 			= false; // hide column by default
$col["dbname"] 			= "CUSTOMER_ID";
$client_lookup 			= $g->get_dropdown_values("SELECT DISTINCT
													  CU.intId   AS k,
													  CU.strName AS v
													FROM trn_orderheader OH
													  INNER JOIN mst_customer CU
														ON CU.intId = OH.intCustomer
													ORDER BY CU.strName");
$col["stype"] 			= "select";
$str 					= ":;".$client_lookup;  // all row, blank row, then all db values. ; is separator
$col["searchoptions"] = array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;

$col 					= array();
$col["title"] 			= "Order No";
$col["name"] 			= "ORDER_NO"; 
$col["width"] 			= "25";
$col["hidden"] 			= true;
$cols[] 				= $col;	

$col 					= array();
$col["title"] 			= "Order Year";
$col["name"] 			= "ORDER_YEAR"; 
$col["width"] 			= "25";
$col["hidden"] 			= true;
$cols[] 				= $col;	

$col 					= array();
$col["title"] 			= "Order No";
$col["name"] 			= "CONCAT_ORDER_NO"; 
$col["width"] 			= "25";
$col["hidden"] 			= false;
$cols[] 				= $col;	

$col 					= array();
$col["title"] 			= "OSales Order ID";
$col["name"] 			= "SALES_ORDER_ID"; 
$col["width"] 			= "25";
$col["hidden"] 			= true;
$cols[] 				= $col;	

$col 					= array();
$col["title"] 			= "Sales Order Name"; // caption of column
$col["name"] 			= "SALES_ORDER_NAME"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] 			= "40";
$col["hidden"] 			= false; // hide column by default
$cols[] 				= $col;

$col = array();
$col["title"] 			= "Photo";
$col["name"] 			= "PHOTO_URL";
$col["width"] 			= "20";
$col["align"] 			= "center";
$col["search"] 			= false;
$col["sortable"] 		= false;
$col["default"] 		= '<img height=50 src=../../../documents/sampleinfo/samplePictures/{PHOTO_URL} >';
$cols[] 				= $col;

$col 					= array();
$col["title"] 			= "Order Wise";
$col["name"] 			= "VOUCHER";
$col["width"] 			= "10";
$col["align"] 			= "center";
$col["search"] 			= false;
$col["sortable"] 		= false;
$col['link']			= 'rpt_post_costing_order_wise.php?Order_No={ORDER_NO}&Order_Year={ORDER_YEAR}&Sales_Order_Id={SALES_ORDER_ID}';
$col["linkoptions"] 	= "target='rpt_post_costing_order_wise1'";
$col["default"] 		= "View"; // default link text
$cols[] 				= $col;

# Custom made column to show link, must have default value as it's not db driven
$col 					= array();
$col["title"] 			= "Sales Order Wise";
$col["name"] 			= "REPORT";
$col["width"] 			= "10";
$col["align"] 			= "center";
$col["search"] 			= false;
$col["sortable"] 		= false;
$col['link']			= 'rpt_post_costing_order_wise.php?Order_No={ORDER_NO}&Order_Year={ORDER_YEAR}&Sales_Order_Id={SALES_ORDER_ID}';
$col["linkoptions"] 	= "target='rpt_post_costing_order_wise2'"; // extra params with <a> tag
$col["default"] 		= "View"; // default link text
$cols[] 				= $col;

# Customization of Action column width and other properties
$col 					= array();
$col["title"] 			= "Action";
$col["name"] 			= "act";
$col["width"] 			= "50";
$col["hidden"] 			= true;
$cols[] 				= $col;

$col 					= array();
$col["title"] 			= "PAYMENT_NO";
$col["name"] 			= "PAYMENT_NO";
$col["width"] 			= "40";
$col["hidden"] 			= true; // hide column by default
$cols[] 				= $col;	

$col 					= array();
$col["title"] 			= "PAYMENT_YEAR";
$col["name"] 			= "PAYMENT_YEAR";
$col["width"] 			= "40";
$col["hidden"] 			= true; // hide column by default
$cols[] 				= $col;	


// $grid["url"] 		= ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 500; // by default 20
$grid["sortname"] 		= 'ORDER_YEAR,ORDER_NO,SALES_ORDER_ID'; // by default sort grid by this field
$grid["sortorder"] 		= "desc"; // ASC or DESC
$grid["caption"] 		= "Post Order Costing Listing"; // caption of grid
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes

$grid["rowactions"] 	= true; // allow you to multi-select through checkboxes

// export XLS file
// export to excel parameters
$grid["export"] 		= array("format"=>"pdf", "filename"=>"my-file", "sheetname"=>"test");

// RTL support
// $grid["direction"] = "rtl";

// $grid["cellEdit"] = true; // inline cell editing, like spreadsheet

$g->set_options($grid);

$g->set_actions(array(	
						"add"=>false, // allow/disallow add
						"edit"=>true, // allow/disallow edit
						"delete"=>false, // allow/disallow delete
						"rowactions"=>false, // show/hide row wise edit/del/save option
						"export"=>true, // show/hide export to excel option
						"autofilter" => true, // show/hide autofilter for search
						"search" => "advance" // show single/multi field search condition (e.g. simple or advance)
					) 
				);

// you can provide custom SQL query to display data
$sql = "SELECT SUB_1.* FROM
			(SELECT
			  OH.intCustomer     						AS CUSTOMER_ID,
			  CU.strName         						AS CUSTOMER_NAME,
			  OH.intOrderYear    						AS ORDER_YEAR,
			  OH.intOrderNo      						AS ORDER_NO,
			  CONCAT(OH.intOrderNo,'/',OH.intOrderYear)	AS CONCAT_ORDER_NO,
			  OD.intSalesOrderId 						AS SALES_ORDER_ID,
			  OD.strSalesOrderNo 						AS SALES_ORDER_NAME,
			 CONCAT(OD.intSampleNo,'-',OD.intSampleYear,'-',OD.intRevisionNo,'-',OD.intSalesOrderId,'.png') AS PHOTO_URL
			FROM trn_orderheader OH
			  INNER JOIN trn_orderdetails OD
				ON OD.intOrderNo = OH.intOrderNo
				  AND OD.intOrderYear = OH.intOrderYear
			  INNER JOIN mst_customer CU
				ON CU.intId = OH.intCustomer
			  WHERE OH.intStatus = 1
			  ORDER BY OH.intOrderYear,OH.intOrderNo,OD.intSalesOrderId)  
		AS SUB_1 WHERE 1=1";
$g->select_command = $sql;

// this db table will be used for add,edit,delete
$g->table = "finance_supplier_payment_header";

// pass the cooked columns to grid
$g->set_columns($cols);

// generate grid output, with unique grid name as 'list1'
$out = $g->render("list1");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<title>Post Order Costing Listing</title>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $backwardseperator?>libraries/jqgrid2/js/themes/smoothness/jquery-ui.custom.css">
</link>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $backwardseperator?>libraries/jqgrid2/js/jqgrid/css/ui.jqgrid.css">
</link>
<table width="100%" border="0" align="center">
  <tr>
    <td height="6" colspan="2" ><?php include $backwardseperator.'Header.php'; ?></td>
  </tr>
</table>
<script src="<?php echo $backwardseperator?>libraries/jqgrid2/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $backwardseperator?>libraries/jqgrid2/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script src="<?php echo $backwardseperator?>libraries/jqgrid2/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script src="<?php echo $backwardseperator?>libraries/jqgrid2/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
<script src="<?php echo $backwardseperator?>libraries/javascript/script.js" type="text/javascript"></script>
<script src="cheque_print.js" type="text/javascript"></script>
</head>
<body>
<table width="100%" border="0" align="center" >
  <tr>
    <td align="center"><?php echo $out?></td>
  </tr>
</table>
</body>
</html>
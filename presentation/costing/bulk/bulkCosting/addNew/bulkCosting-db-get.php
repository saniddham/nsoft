<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$requestType 	= $_REQUEST['requestType'];
	//ini_set('display_errors',1);
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once $_SESSION['ROOT_PATH']."class/customerAndOperation/cls_textile_stores.php";
	///
	/////////// type of print load part /////////////////////
if($requestType=='loadOrderNo')
	{
		$poNo  = $_REQUEST['poNo'];
		$customer  = $_REQUEST['customer'];
		
		$sql = "SELECT
				trn_orderheader.intOrderNo,
				trn_orderheader.intCustomer,
				trn_orderheader.intOrderYear
				FROM trn_orderheader
				WHERE
				trn_orderheader.intOrderYear !=  '*' ";
				if($poNo!=''){
		$sql .= " AND trn_orderheader.strCustomerPoNo='$poNo'";			
				}
				if($customer!=''){
		$sql .= " AND trn_orderheader.intCustomer='$customer'";			
				}
		$sql .= " ORDER BY
				 trn_orderheader.intOrderNo DESC"; 
				
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intOrderNo']."/".$row['intOrderYear']."\">".$row['intOrderNo']."/".$row['intOrderYear']."</option>";
			$customer=$row['intCustomer'];
		}
			$response['orderNo'] = $html;
			if($poNo==''){
				$response['customer'] = '';
			}
			else{
				$response['customer'] = $customer;
			}
		if($poNo==''){
			//	$response['orderNo'] = '';
				$response['customer'] = '';
				
			 $sql = "SELECT
					trn_orderheader.strCustomerPoNo 
					FROM trn_orderheader
					WHERE
					trn_orderheader.intOrderYear !=  '*'
					ORDER BY 
					trn_orderheader.strCustomerPoNo ASC";
			$html = "<option value=\"\"></option>";
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				$html .= "<option value=\"".$row['strCustomerPoNo']."\">".$row['strCustomerPoNo']."</option>";
			}
				$response['PoNo'] = $html;
		}
		
		echo json_encode($response);
	}
	
	//----------------------------------
	else if($requestType=='loadSampleNos')
		{
			$orderNo  = $_REQUEST['orderNo'];
			$orderYear  = $_REQUEST['orderYear'];
			$sampleYear  = $_REQUEST['sampleYear'];
			$sql = "SELECT DISTINCT 
					trn_orderdetails.intSampleNo
					FROM trn_orderdetails
					WHERE 
					1=1 "; 
			if($orderNo!='')
			$sql.=" AND trn_orderdetails.intOrderNo 	=  '$orderNo'  ";
			if($orderYear!='')
			$sql.=" AND trn_orderdetails.intOrderYear =  '$orderYear'  "; 
			if($sampleYear!='')
			$sql.=" AND trn_orderdetails.intSampleYear =  '$sampleYear'  AND trn_orderdetails.SO_TYPE > -1 ";
			$sql.=" order by intSampleNo desc  "; 
			$html = "<option value=\"\"></option>";
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				$html .= "<option value=\"".$row['intSampleNo']."\">".$row['intSampleNo']."</option>";
			}
				$response['sampNos'] = $html;
		
		echo json_encode($response);
		}
	//----------------------------------
	else if($requestType=='loadOrderNosToSalesOrderNos')
		{
		$year				= $_REQUEST['year'];
		$graphicNo			= $_REQUEST['graphicNo'];
		$styleId			= $_REQUEST['styleId'];
		$customerPONo		= $_REQUEST['customerPONo'];
		$salesOrderNo		= $_REQUEST['salesOrderNo'];
		$customerId			= $_REQUEST['customerId'];
		$sampleNo			= $_REQUEST['sampleNo'];
		$sampleYear			= $_REQUEST['sampleYear'];
		
			$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
		if($graphicNo!='')
			$para.=" AND trn_orderdetails.strGraphicNo =  '$graphicNo'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId'  ";
		if($customerPONo!='')
			$para.=" AND trn_orderheader.strCustomerPoNo =  '$customerPONo' ";
		if($salesOrderNo!='')
			$para.=" AND trn_orderdetails.strSalesOrderNo 	=  '$salesOrderNo'  ";	
		if($sampleNo!='')
			$para.=" AND trn_orderdetails.intSampleNo 	=  '$sampleNo'  ";	
		if($sampleYear!='')
			$para.=" AND trn_orderdetails.intSampleYear 	=  '$sampleYear'  ";	
			
		$sql = "SELECT DISTINCT
					trn_orderheader.intOrderNo 
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
					trn_orderheader.intStatus=1
					$para
					
				ORDER BY intOrderNo DESC
				";
				
		$result = $db->RunQuery($sql);
		$html ='';
		if($orderNo=='')
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option ".($customerId==$row['intOrderNo']?'selected':'')." value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
		}
			$response['orderNo'] = $html;
		
		echo json_encode($response);
	}
	
	//----------------------------------
	else if($requestType=='loadPoNo')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderNoArray 	 = explode('/',$orderNo);
		
		$sql = "SELECT
				trn_orderheader.strCustomerPoNo,
				trn_orderheader.intCustomer 
				FROM trn_orderheader 
				WHERE
				trn_orderheader.intOrderNo='$orderNoArray[0]' 
				AND trn_orderheader.intOrderYear='$orderNoArray[1]'
				ORDER BY 
				trn_orderheader.strCustomerPoNo ASC";

		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$response['poNo'] = $row['strCustomerPoNo'];
				$response['customer'] = $row['intCustomer'];
		}
		if($orderNoArray[0]==''){
				$response['poNo'] = '';
				$response['customer'] = '';
	
		//---order no
			 $sql = "SELECT
					trn_orderheader.intOrderNo,
					trn_orderheader.intCustomer,
					trn_orderheader.intOrderYear
					FROM trn_orderheader
					WHERE
					trn_orderheader.intOrderYear !=  '*' 
					ORDER BY
				    trn_orderheader.intOrderNo DESC"; 
					
			$html = "<option value=\"\"></option>";
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				$html .= "<option value=\"".$row['intOrderNo']."/".$row['intOrderYear']."\">".$row['intOrderNo']."/".$row['intOrderYear']."</option>";
			}
				$response['orderNo'] = $html;
					
		}
		//---sales order no
		 $sql = "SELECT DISTINCT 
					trn_orderdetails.strSalesOrderNo,	concat(trn_orderdetails.strSalesOrderNo,'/',trn_orderdetails.strCombo,'/',trn_orderdetails.strPrintName,'/',mst_part.strName) as so
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear  
							Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId 
							INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
				WHERE
				trn_orderdetails.intOrderNo='$orderNoArray[0]' 
				AND trn_orderdetails.intOrderYear='$orderNoArray[1]' 
				AND trn_orderdetails.SO_TYPE > -1 
				ORDER BY 
				trn_orderdetails.strSalesOrderNo ASC";

		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['strSalesOrderNo']."\">".$row['so']."</option>";
		}
				$response['salesOrderNo'] = $html;
		
		
		
		echo json_encode($response);
	}
	//----------------------------------
	else if($requestType=='loadPONoAndOrderNo')
	{
		$customer  = $_REQUEST['customer'];
		
		$sql = "SELECT
				trn_orderheader.strCustomerPoNo,
				trn_orderheader.intCustomer 
				FROM trn_orderheader"; 
		if($customer!=''){
		$sql .= " WHERE
				trn_orderheader.intCustomer='$customer'"; 
		}
		$sql .= " ORDER BY 
				trn_orderheader.strCustomerPoNo ASC";
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['strCustomerPoNo']."\">".$row['strCustomerPoNo']."</option>";
		}
				$response['poNo'] = $html;
				
		$sql = "SELECT
				trn_orderheader.intOrderNo,
				trn_orderheader.intOrderYear
				FROM trn_orderheader "; 
		if($customer!=''){
		$sql .= " WHERE
				trn_orderheader.intCustomer='$customer'"; 
		}
		$sql .= " ORDER BY 
				trn_orderheader.intOrderNo DESC";
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['intOrderNo']."/".$row['intOrderYear']."\">".$row['intOrderNo']."/".$row['intOrderYear']."</option>";
		}
				$response['orderNo'] = $html;
		
		
		echo json_encode($response);
	}
	
//-----------------------------------
	else if($requestType=='loadQty')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderNoArray 	 = explode('/',$orderNo);
		$salesOrderNo  = $_REQUEST['salesOrderNo'];
		
		$sql = "SELECT
				Sum(trn_orderdetails.intQty) AS qty
				FROM trn_orderdetails 
				WHERE
				trn_orderdetails.intOrderNo='$orderNoArray[0]' 
				AND trn_orderdetails.intOrderYear='$orderNoArray[1]'  
				AND trn_orderdetails.SO_TYPE > -1
"; 
		if($salesOrderNo!=''){
		$sql .= " AND 
				trn_orderdetails.strSalesOrderNo='$salesOrderNo'"; 
		}
		$sql .= " GROUP BY
				trn_orderdetails.intOrderNo,
				trn_orderdetails.intOrderYear"; 
		
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$response['qty'] = $row['qty'];
		
		echo json_encode($response);
	}
//--------------------------------------------------------------------











	else if($requestType=='loadMrnNo')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderNoArray 	 = explode('/',$orderNo);
		$styleNo  = $_REQUEST['styleNo'];
		
		  $sql = "SELECT DISTINCT
				ware_mrndetails.intMrnNo, 
				ware_mrndetails.intMrnYear  
				FROM ware_mrndetails 
				Inner Join ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
				WHERE 
				ware_mrnheader.intStatus=1 AND 
				ware_mrnheader.intCompanyId =  '$location' AND 
				ware_mrndetails.intOrderNo =  '$orderNoArray[0]' AND
				ware_mrndetails.intOrderYear =  '$orderNoArray[1]' AND 
				ware_mrndetails.strStyleNo =  '$styleNo' AND 
				(ware_mrndetails.dblQty-ware_mrndetails.dblIssudQty)>0";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intMrnNo']."/".$row['intMrnYear']."\">".$row['intMrnNo']."/".$row['intMrnYear']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadItems')
	{
		$itemType  = $_REQUEST['itemType'];
		$orderNo  = $_REQUEST['orderNo'];
		$orderNoArray 	 = explode('/',$orderNo);
		$styleNo  = $_REQUEST['styleNo'];
		$mainCategory  = $_REQUEST['mainCategory'];
		$subCategory  = $_REQUEST['subCategory'];
		$description  = $_REQUEST['description'];
		
		   $sql="SELECT
				mst_item.strName as itemName,
				mst_item.intId, 
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName, ";
				if($itemType=='1'){
				$sql.="trn_materialratio.intOrderNo,
				trn_materialratio.intOrderYear,
				trn_materialratio.strStyleNo,
				trn_materialratio.dblRequiredQty,
				trn_materialratio.dblAllocatedQty,
				trn_materialratio.dblMrnQty,
				trn_materialratio.dblIssueQty, ";
				}
				$sql.="mst_item.strCode 
				FROM ";
				
				if($itemType=='1'){
				$sql.="trn_materialratio
				Inner Join mst_item ON trn_materialratio.intItemNo = mst_item.intId ";
				}
				else{
				$sql.="  mst_item ";
				}
				$sql.=" Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				WHERE ";
				if($itemType=='1'){
				$sql.=" trn_materialratio.intOrderNo =  '$orderNoArray[0]' AND
				trn_materialratio.intOrderYear =  '$orderNoArray[1]' AND
				trn_materialratio.strStyleNo =  '$styleNo'";
				$sql.=" AND mst_item.intBomItem =  '$itemType' ";
				}
				else{
				$sql.=" mst_item.intBomItem =  '$itemType' ";
				}
				if($mainCategory!='')
				$sql.=" AND mst_item.intMainCategory =  '$mainCategory' ";
				if($subCategory!='')
				$sql.=" AND mst_item.intSubCategory =  '$subCategory'";
				if($description!=''){
				$sql.=" AND
				mst_item.strName LIKE  '%$description%'";
				}
				if($itemType=='1'){
				//$sql.=" AND trn_materialratio.dblAllocatedQty-trn_materialratio.dblMrnQty>0";
				}
				$sql.=" Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc";
		//echo $sql;
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			
			
			if($itemType=='1'){
			$stockBalQty=getStockBalance($company,$location,$orderNoArray[0],$orderNoArray[1],$row['strStyleNo'],$row['intId']);
			}
			else
			{
			 $stockBalQty=getStockBalance_bulk($location,$row['intId']);
			}

			$data['itemId'] 	= $row['intId'];
			$data['maincatId'] = $row['intMainCategory'];
			$data['subCatId'] 	= $row['intSubCategory'];
			$data['code'] = $row['strCode'];
			$data['itemName'] = $row['itemName'];
			
			$data['requiredQty'] 	= $row['dblRequiredQty'];
			$data['allocatedQty'] 	= $row['dblAllocatedQty'];
			$data['mrnQty'] 	= $row['dblMrnQty'];
			$data['issueQty'] 	= $row['dblIssueQty'];
			$data['stockBal'] = $stockBalQty;
			
/*			if($row['dblGrnQty']-$row['dblRetunSupplierQty']<$stockBalQty)
			$data['Qty'] = $row['dblGrnQty']-$row['dblRetunSupplierQty'];
			else
			$data['Qty'] = $stockBalQty;
*/			
			$data['mainCatName'] = $row['mainCatName'];
			$data['subCatName'] = $row['subCatName'];
			
			$arrCombo[] = $data;
		}
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);

	}
	else if($requestType=='loadAllComboDetails')
	{
		$year				= $_REQUEST['year'];
		$graphicNo			= $_REQUEST['graphicNo'];
		$styleId			= $_REQUEST['styleId'];
		$customerPONo		= $_REQUEST['customerPONo'];
		$orderNo			= $_REQUEST['orderNo'];
		$sampNo			    = $_REQUEST['sampNo'];
		$sampYear			= $_REQUEST['sampYear'];
		$salesOrderNo		= $_REQUEST['salesOrderNo'];
		$customerId			= $_REQUEST['customerId'];
		$locationFlag		= 0;
		$companyFlag		= 0;
		echo loadAllComboDetails_with_sampNo($sampNo,$sampYear,$year,$graphicNo,$styleId,$customerPONo,$orderNo,$customerId,$locationFlag,$location,$companyFlag,$company);	
	}
	//-----------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$style,$item)
	{
		global $db;
		  $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.strStyleNo =  '$style' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function loadAllComboDetails_with_sampNo($sampNo,$sampYear,$year,$graphicNo,$styleId,$customerPONo,$orderNo,$customerId,$locationFlag,$location,$companyFlag,$company)	
	{
		$obj = new cls_texttile();
		//echo $obj->loadAllSearchComboDetails($year,$graphicNo,$styleId,$customerPONo,$orderNo,$customerId,$locationFlag,$location,$companyFlag,$company);	
		echo $obj->loadAllSearchComboDetails_with_sampNo($sampNo,$sampYear,$year,$graphicNo,$styleId,$customerPONo,$orderNo,$customerId,$locationFlag,$location,$companyFlag,$company);	
		
	}
	//--------------------------------------------------------------
?>
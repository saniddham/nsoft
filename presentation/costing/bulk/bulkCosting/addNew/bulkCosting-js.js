var basePath = "presentation/costing/bulk/bulkCosting/addNew/";			
$(document).ready(function() {
	
  		$("#frmBulkCosting").validationEngine();
		
		
		$("#frmBulkCosting #cboPONo").change(function(){
			loadOrderNo();
			clearGrid();
		});
		$("#frmBulkCosting #cboCustomer").change(function(){
			loadPONoAndOrderNo();
			clearGrid();
		});
		
		$('#frmBulkCosting #butcurrentView').die('click').live('click',function(){
		viewPriceCosting(this);
		});	
		
		$("#frmBulkCosting #cboOrderNo").change(function(){
			 $('#cboGraphicNo').val('');
			 $('#cboStyle').val('');
			 $('#cboPONo').val('');
		    $('#cboCustomer').val('');
			$('#cboSalesOrderNo').val('');
			$('#cboCombo').val('');
			$('#cboSampNo').val('');
			$('#cboSampYear').val('');
			$('#txtExQty').val('');
			submitForm();
		});
		$("#frmBulkCosting #butSearch").die('click').live('click',function(){
			submitForm();
		});
		
		$("#frmBulkCosting #cboSalesOrderNo").change(function(){
			submitForm();
		});
		
		$("#frmBulkCosting #cboCombo").change(function(){
			submitForm();
		});
		
		$("#frmBulkCosting #cboOrderYear").die('change').live('change',function(){
		 $('#cboGraphicNo').val('');
		 $('#cboStyle').val('');
		 $('#cboPONo').val('');
		 $('#cboOrderNo').val('');
		 $('#cboCustomer').val('');
		 $('#cboSalesOrderNo').val('');
			$('#cboCombo').val('');
		 $('#cboSampNo').val('');
		 $('#cboSampYear').val('');
			clearGrid();
			submitForm();
		});
		
		//
		$("#frmBulkCosting #cboStyle").die('change').live('change',function(){
		// $('#cboOrderYear').val();
		 //$('#cboGraphicNo').val('');
		 //$('#cboStyle').val();
		// $('#cboPONo').val('');
		// $('#cboOrderNo').val('');
		// $('#cboCustomer').val('');
		 //$('#cboSalesOrderNo').val('');
		// $('#cboSampNo').val('');
			clearGrid();
			submitForm();
		});
		
		$("#frmBulkCosting #cboGraphicNo").die('change').live('change',function(){
		// $('#cboOrderYear').val();
		 //$('#cboGraphicNo').val('');
		 //$('#cboStyle').val();
		// $('#cboPONo').val('');
		// $('#cboOrderNo').val('');
		// $('#cboCustomer').val('');
		 //$('#cboSalesOrderNo').val('');
		// $('#cboSampNo').val('');
			clearGrid();
			submitForm();
		});
		//-----------------------------------------------------------------
		$("#frmBulkCosting #cboSalesOrderNo").die('change').live('change',function(){
/*			if($("#cboOrderNo").val()==''){
			loadOrderNos();
			clearGrid();
			}
*/		});
		
		$("#frmBulkCosting #cboSampYear").die('change').live('change',function(){
			if($("#cboSampYear").val()!=''){
			loadSampleNos();
			loadOrderNos();
			clearGrid();
			}
		});
		$("#frmBulkCosting #cboSampNo").die('change').live('change',function(){
			if($("#cboOrderNo").val()==''){
			loadOrderNos();
			clearGrid();
			}
		});
		//
		//-----------------------------------------------------------------
		
		$('#frmBulkCosting #butNew').die('click').live('click',function(){
			document.location.href = "?q=117";
		});
		
		
		$("#frmBulkCosting .search").die('change').live('change',loadAllCombo);
		
		$('#frmBulkCosting #butPRN').die('click').live('click',loadPRN);
	//--------------------------------------------
  
  function loadAllCombo()
{
	//######################
	//####### get values ###
	//######################
	var year 		= $('#cboOrderYear').val();
	var graphicNo 	= $('#cboGraphicNo').val();
	var styleId 	= $('#cboStyle').val();
	var customerPONo= $('#cboPONo').val();
	var orderNo		= $('#cboOrderNo').val();
	var customerId 	= $('#cboCustomer').val();
	var salesOrderNo 	= $('#cboSalesOrderNo').val();
	var sampleNo 	= $('#cboSampNo').val();
	var sampleYear 	= $('#cboSampYear').val();
	
	//######################
	//####### create url ###
	//######################
	var url		=basePath+"bulkCosting-db-get.php?requestType=loadAllComboDetails";
		url	   +="&year="+			year;	
		url	   +="&graphicNo="+		graphicNo;
		url	   +="&styleId="+		styleId;
		url	   +="&customerPONo="+	customerPONo;
		url	   +="&orderNo="+		orderNo;
		url	   +="&salesOrderNo="+		salesOrderNo;
		url	   +="&customerId="+	customerId;
		url	   +="&sampNo="+	sampleNo;
		url	   +="&sampYear="+	sampleYear;
	
	//######################
	//####### send data  ###
	//######################
	$.ajax({url:url,
			async:false,
			dataType:'json',
			type:'POST',
			success:function(json){
					$('#cboStyle').html(json.styleNo);
					$('#cboGraphicNo').html(json.graphicNo);
					$('#cboPONo').html(json.customerPoNo);
					$('#cboOrderNo').html(json.orderNo);
					$('#cboCustomer').html(json.customer);
					$('#cboSalesOrderNo').html(json.salesOrderNo);
					$('#cboSampNo').html(json.sampNo);
					$('#cboSampYear').val(json.sampYear);
					$('#cboSampNo').val(sampleNo);
					$('#cboOrderNo').val(orderNo);
					$('#cboStyle').val(styleId);
			}
			});
}
//------------------------------------------------------------------------

function loadOrderNos()
{
	//######################
	//####### get values ###
	//######################
	var year 		= $('#cboOrderYear').val();
	var graphicNo 	= $('#cboGraphicNo').val();
	var styleId 	= $('#cboStyle').val();
	var customerPONo= $('#cboPONo').val();
	var orderNo		= $('#cboOrderNo').val();
	var customerId 	= $('#cboCustomer').val();
	var salesOrderNo 	= $('#cboSalesOrderNo').val();
	var sampleNo 	= $('#cboSampNo').val();
	var sampleYear 	= $('#cboSampYear').val();
	
	//######################
	//####### create url ###
	//######################
	var url		=basePath+"bulkCosting-db-get.php?requestType=loadOrderNosToSalesOrderNos";
		url	   +="&year="+			year;	
		url	   +="&graphicNo="+		graphicNo;
		url	   +="&styleId="+		styleId;
		url	   +="&customerPONo="+	customerPONo;
		url	   +="&orderNo="+		orderNo;
		url	   +="&salesOrderNo="+		salesOrderNo;
		url	   +="&customerId="+	customerId;
		url	   +="&sampleNo="+	sampleNo;
		url	   +="&sampleYear="+	sampleYear;
	
	//######################
	//####### send data  ###
	//######################
	$.ajax({url:url,
			async:false,
			dataType:'json',
			type:'POST',
			success:function(json){
					$('#cboOrderNo').html(json.orderNo);
			}
			});
}

//------------------------------------------------------------------------
//-----------------------------------
$('#frmBulkCosting #butReport').click(function(){
	if($('#txtPoNo').val()!=''){
		window.open('../listing/rptInvoice.php?invoiceNo='+$('#txtInvoiceNo').val()+'&year='+$('#txtYear').val());	
	}
	else{
		alert("There is no Invoice No to view");
	}
});


});//----------end of ready --------

//-------------------------------------
function alertx()
{
	$('#frmBulkCosting #butSave').validationEngine('hide')	;
}
//----------------------------------------------- 
function alertDelete()
{
	$('#frmBulkCosting #butDelete').validationEngine('hide')	;
}
//----------------------------------------------- 
function closePopUp(){
	
}
//-----------------------------------------------
function loadOrderNo(){
	    var poNo = $('#cboPONo').val();
	    var customer = $('#cboCustomer').val();
		var url 		= basePath+"bulkCosting-db-get.php?requestType=loadOrderNo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:"poNo="+poNo+"&customer="+customer,
			async:false,
			success:function(json){
				
					document.getElementById("cboOrderNo").innerHTML=json.orderNo;
					document.getElementById("cboCustomer").value=json.customer;
					if(poNo==''){
					document.getElementById("cboPONo").innerHTML=json.PoNo;
					}
			}
		});
}
//-----------------------------------------------
function loadSampleNos(){
	    var orderNo = $('#cboOrderNo').val();
	    var orderYear = $('#cboOrderYear').val();
	    var sampleYear = $('#cboSampYear').val();
	    var sampleNo = $('#cboSampNo').val();
		
	    var customer = $('#cboCustomer').val();
		var url 		= basePath+"bulkCosting-db-get.php?requestType=loadSampleNos";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:"orderNo="+orderNo+"&orderYear="+orderYear+"&sampleYear="+sampleYear,
			async:false,
			success:function(json){
				
					document.getElementById("cboSampNo").innerHTML=json.sampNos;
			}
		});
		
		$('#cboSampNo').val(sampleNo);
}
//--------------------------------------------------
function loadCustomerPO(){
	    var orderNo = $('#cboOrderNo').val();
		var url 		= basePath+"bulkCosting-db-get.php?requestType=loadPoNo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:"orderNo="+orderNo,
			async:false,
			success:function(json){
				
					document.getElementById("cboPONo").value=json.poNo;
					document.getElementById("cboCustomer").value=json.customer;
					if(orderNo==''){
					document.getElementById("cboOrderNo").innerHTML=json.orderNo;
					}
					document.getElementById("cboSalesOrderNo").innerHTML=json.salesOrderNo;
			}
		});
}
//-----------------------------------------------
function loadPONoAndOrderNo(){
	    var poNo = $('#cboPONo').val();
	    var orderNo = $('#cboOrderNo').val();
	    var customer = $('#cboCustomer').val();
		var url 		= basePath+"bulkCosting-db-get.php?requestType=loadPONoAndOrderNo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:"customer="+customer,
			async:false,
			success:function(json){
				
					document.getElementById("cboPONo").innerHTML=json.poNo;
					document.getElementById("cboOrderNo").innerHTML=json.orderNo;
			}
		});
}
//-----------------------------------------------
function loadQty(){
	    var orderNo = $('#cboOrderNo').val();
	    var salesOrderNo = $('#cboSalesOrderNo').val();
		var url 		= basePath+"bulkCosting-db-get.php?requestType=loadQty";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:"orderNo="+orderNo+"&salesOrderNo="+salesOrderNo,
			async:false,
			success:function(json){
				
					document.getElementById("txtQty").value=json.qty;
			}
		});
}
//------------------------------------------------
function submitForm(){
	if ($('#cboSalesOrderNo').val() > 0){
		$('#cboCombo').val("");
	}
	window.location.href = "?q=117&orderNo="+$('#cboOrderNo').val()+'&orderYear='+$('#cboOrderYear').val()
						+'&salesOrderNo='+$('#cboSalesOrderNo').val()+'&combo='+$('#cboCombo').val()+'&styleNo='+$('#cboStyle').val()+'&graphicNo='+$('#cboGraphicNo').val()+'&sampleNo='+$('#cboSampNo').val()+'&sampleYear='+$('#cboSampYear').val()+'&custPO='+$('#cboPONo').val()+'&customer='+$('#cboCustomer').val()+'&extraQty='+$('#txtExQty').val();
		//document.frmBulkCosting.submit();

}
//-----------------------------------------------
function clearGrid()
{
	var rowCount = document.getElementById('tblMain').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMain').deleteRow(1);
	}
	
	//$('#cboSalesOrderNo').html('<option value=\"\"></option>');
	//$('#txtQty').val('');
}
//-----------------------------------------------

function loadPRN()
{
	$('#frmBulkCosting').submit();
}

function viewPriceCosting(e){
	var graphicNo 	= $("#cboGraphicNo").val();
	var sampleNo    = $("#cboSampNo").val();
	var sample_year = $("#cboSampYear").val();
	var revisionNo  = $("#cboRevision").val();
	var combo  = $("#cboCombo").val();
	var printId  = $("#cboPrint").val();
	//alert(graphicNo+","+sampleNo+","+sample_year+","+revisionNo+","+combo+","+printId);
	//window.open('?q=27&sampleNo=' + sampleNo + "&sampleYear=" + sample_year + "&revNo=" + revisionNo );
	window.open('?q=1212&graphicNo=' + graphicNo + "&sampleyear=" + sample_year + "&sampleNo=" + sampleNo + "&revNo=" + revisionNo+ "&combo=" + URLEncode(combo)+ "&print=" + URLEncode(printId));
	}
<style type="text/css">
#rpt_div_watermark {
	position: absolute;
	width: 100%;
	height: 100%;
	opacity: 0.2;
	z-index: 2;
	background-repeat: repeat;
}
.rpt_watermark_rejected {
	background-image: url(images/watermark_rejected.png)
}
.rpt_watermark_pending {
	background-image: url(images/watermark_pending.png)
}
.rpt_watermark_cancel {
	background-image: url(images/watermark_cancel.png)
}
.rpt_watermark_revised {
	background-image: url(images/watermark_revised.png)
}
#rpt_div_ribbon {
	position: absolute;
	left: -13px;
	top: -15px;
	background-repeat: no-repeat
}
</style>
<style type="text/css" media="print">
#rpt_div_watermark {
	opacity: 0.2
}
#rpt_div_table {
	display: inline
}
#iframeFiles2 {
	display: none
}
#rpt_tr_approveStatus {
	display: none
}
#rpt_export_pdf {
	display: none
}
#rpt_div_main {
	left: 10px;
	top: 12px;
	border: #CCC solid 1px;
}
</style>
<?php

switch($status){
	case '-1':
		echo "<div id=\"rpt_div_ribbon\" ><img src=\"images/ribbon_revised.png\" width=\"140\" height=\"141\"></div>";
		echo "<div id=\"rpt_div_watermark\" class=\"rpt_watermark_revised\"></div>";
		break;
	case '0':
		echo "<div id=\"rpt_div_ribbon\" ><img src=\"images/ribbon_rejected.png\" width=\"140\" height=\"141\"></div>";
		echo "<div id=\"rpt_div_watermark\" class=\"rpt_watermark_rejected\"></div>";
		break;
	case '1':
		echo "<div id=\"rpt_div_ribbon\" ><img src=\"images/ribbon_approved.png\" width=\"140\" height=\"141\"></div>";
		break;
	case '-2':
		echo "<div id=\"rpt_div_ribbon\" ><img src=\"images/ribbon_cancel.png\" width=\"140\" height=\"141\"></div>";
		echo "<div id=\"rpt_div_watermark\" class=\"rpt_watermark_cancel\"></div>";
		break;
	default:
		echo "<div id=\"rpt_div_ribbon\" ><img src=\"images/ribbon_pending.png\" width=\"140\" height=\"141\"></div>";
		echo "<div id=\"rpt_div_watermark\" class=\"rpt_watermark_pending\"></div>";
		break;
}

?>

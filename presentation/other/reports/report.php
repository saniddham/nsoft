<?php
	//ini_set('display_errors',1);
	include "../../../class/class.pdf.php";
	include "../../../dataAccess/Connector.php";
	$pdf = new RBpdf($db);
	
	$type = $_REQUEST['type'];
	
	if($type=='Companies')
	{
		$sql = "select 
					@rowid:=@rowid+1 as No, 
					tb1.Code,
					tb1.Name,
					tb1.Country,
					tb1.Web_Site,
					tb1.Account_No,
					tb1.Register_No,
					tb1.Vat_No,
					tb1.Currency,
					tb1.Status
		 from (SELECT 
					mst_companies.strCode AS Code,
					mst_companies.strName AS Name,
					mst_country.strCountryName AS Country,
					mst_companies.strWebSite as Web_Site,
					mst_companies.strAccountNo As Account_No,
					mst_companies.strRegistrationNo as Register_No,
					mst_companies.strVatNo as Vat_No,
					mst_financecurrency.strCode AS Currency,
					if(mst_companies.intStatus=1,'Active','In-Active') as Status 
				FROM
				mst_companies
					Inner Join mst_country ON mst_country.intCountryID = mst_companies.intCountryId
					Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_companies.intBaseCurrencyId 
				ORDER BY
					mst_companies.strName ASC ) as tb1  
				JOIN (SELECT @rowid:=0) as init

				";
		$pdf->Report($sql,'COMPANY LIST');
	}
	else if($type=='Locations')
	{
		$sql = "SELECT 
					@rowid:=@rowid+1 as No, 
					tb1.`Comapny Code`,
					tb1.`Company Name`,
					tb1.`Location Code`,
					tb1.`Location Name`,
					tb1.strAddress,
					tb1.strPhoneNo,
					tb1.strFaxNo,
					tb1.strEmail , 
					tb1.Status
		FROM(
			SELECT 
					mst_companies.strCode AS `Comapny Code`,
					mst_companies.strName AS `Company Name`,
					mst_locations.strCode AS `Location Code`,
					mst_locations.strName AS `Location Name`,
					mst_locations.strAddress,
					mst_locations.strPhoneNo,
					mst_locations.strFaxNo,
					mst_locations.strEmail , 
					if(mst_locations.intStatus=1,'Active','In-Active') as Status
				FROM
				mst_locations
					Inner Join mst_companies ON mst_companies.intId = mst_locations.intCompanyId
				ORDER BY
					mst_companies.strName ASC) as tb1  
				JOIN (SELECT @rowid:=0) as init
				";	
		$pdf->Report($sql,'LOCATIONS LIST');
	}
	else if($type=='Departments')
	{
		$sql = "SELECT 
				@rowid:=@rowid+1 as No, 
				tb1.Code,
				tb1.Name, 
				tb1.Status
			FROM(
				SELECT 
				mst_department.strCode as Code,
				mst_department.strName as Name, 
				if(mst_department.intStatus=1,'Active','In-Active') as Status
				FROM
				mst_department 
				,(SELECT @rowid:=0) as init
				ORDER BY
				mst_department.strName ASC) as tb1
				JOIN (SELECT @rowid:=0) as init
				";	
		$pdf->Report($sql,'DEPARTMENTS LIST');
	}
	else if($type=='Users')
	{
		$sql = "SELECT 
				@rowid:=@rowid+1 as No, 
				tb1.`Login Name`,
				tb1.`Full Name`,
				tb1.`Department`,
				tb1.`Contact No`,
				tb1.`Designation`,
				tb1.`Gender`,
				tb1.`E-mail`, 
				tb1.`Status`
		FROM(
		SELECT 
				sys_users.strUserName as `Login Name`,
				sys_users.strFullName as `Full Name`,
				mst_department.strName as `Department`,
				sys_users.strContactNo as `Contact No`,
				sys_users.strDesignation as `Designation`,
				sys_users.strGender as `Gender`,
				sys_users.strEmail as `E-mail`, 
				if(sys_users.intStatus=1,'Active','In-Active') as `Status`
				FROM
				sys_users
				Inner Join mst_department ON sys_users.intDepartmentId = mst_department.intId 
				where sys_users.intHigherPermision <>1
				ORDER BY
				sys_users.strUserName ASC) AS tb1
				JOIN (SELECT @rowid:=0) as init
				";	
		$pdf->Report($sql,'USERS LIST');
	}
	else if($type=='Customers')
	{
		$sql = "SELECT 
				@rowid:=@rowid+1 as No, 
				tb1.`Code`,
				tb1.`Name`,
				tb1.`Address`,
				tb1.`City`,
				tb1.`Country`,
				tb1.`Phone`,
				tb1.`Mobile`,
				tb1.`Fax`,
				tb1.`E-Mail1`,
				tb1.`Status`
			 FROM 
				(SELECT 
				mst_customer.strCode as `Code`,
				mst_customer.strName as `Name`,
				mst_customer.strAddress `Address`,
				mst_customer.strCity as `City`,
				mst_country.strCountryName as `Country`,
				mst_customer.strPhoneNo as `Phone`,
				mst_customer.strMobileNo as `Mobile`,
				mst_customer.strFaxNo as `Fax`,
				mst_customer.strEmail as `E-Mail1`,
				if(mst_customer.intStatus=1,'Active','In-Active') as `Status`
				FROM
				mst_customer
				Inner Join mst_typeofmarketer ON mst_customer.intTypeId = mst_typeofmarketer.intId
				left Join mst_country ON mst_customer.intCountryId = mst_country.intCountryID
				left Join mst_financecurrency ON mst_customer.intCurrencyId = mst_financecurrency.intId
				left Join mst_shipmentmethod ON mst_customer.intShipmentId = mst_shipmentmethod.intId
				left Join mst_financepaymentsterms ON mst_customer.intPaymentsTermsId = mst_financepaymentsterms.intId
				left Join mst_financepaymentsmethods ON mst_customer.intPaymentsMethodsId = mst_financepaymentsmethods.intId
				left Join mst_financechartofaccounts ON mst_customer.intChartOfAccountId = mst_financechartofaccounts.intId
				ORDER BY
				mst_customer.strName ASC) as tb1  
				JOIN (SELECT @rowid:=0) as init
				";	
		$pdf->Report($sql,'CUSTOMERS LIST');
	}
	else if($type=='Suppliers')
	{
		$sql = "SELECT 
				@rowid:=@rowid+1 as No, 
				tb1.`Address`,
				tb1.`Name`,
				tb1.`Country`, 
				tb1.`Currency`,
				tb1.`Phone`,
				tb1.`Mobile`,
				tb1.`E-Mail` , 
				tb1.`Status`
		FROM(
		SELECT 
				mst_supplier.strName AS `Name`,
				mst_supplier.strAddress as `Address`,
				mst_country.strCountryName as `Country`, 
				mst_supplier.strCity as `City`,
				mst_financecurrency.strCode as `Currency`,
				mst_supplier.strPhoneNo as `Phone`,
				mst_supplier.strMobileNo as `Mobile`,
				mst_supplier.strEmail as `E-Mail` , 
				if(mst_supplier.intStatus=1,'Active','In-Active') as `Status`
				FROM
				mst_supplier
				Inner Join mst_country ON mst_supplier.intCountryId = mst_country.intCountryID
				Inner Join mst_financecurrency ON mst_supplier.intCurrencyId = mst_financecurrency.intId
				ORDER BY
				mst_supplier.strName ASC) AS tb1 
				JOIN (SELECT @rowid:=0) as init
				";	
		$pdf->Report($sql,'SUPPLIERS LIST');
	}
	else if($type=='supplierLocations')
	{
		$sql = "SELECT 
			@rowid:=@rowid+1 as No, 
			tb1.Supplier,
			tb1.Location,
			tb1.Remarks,
			tb1.Status
		FROM(
		SELECT
			mst_supplier.strName as Supplier,
			mst_supplier_locations_header.strName as Location,
			mst_supplier_locations_header.strRemark as Remarks,
			if(mst_supplier_locations_header.intStatus=1,'Active','In-Active') as `Status`
			FROM
			mst_supplier_locations
			Inner Join mst_supplier_locations_header ON mst_supplier_locations.intLocationId = mst_supplier_locations_header.intId
			Inner Join mst_supplier ON mst_supplier_locations.intSupplierId = mst_supplier.intId
			ORDER BY
			mst_supplier.strName ASC,
			mst_supplier_locations_header.strRemark ASC) AS tb1 
			JOIN (SELECT @rowid:=0) as init
				";	
		$pdf->Report($sql,'SUPPLIER LOCATIONS LIST');
	}
	else if($type=='Items')
	{
		$sql = "SELECT 
				@rowid:=@rowid+1 as No, 
				tb1.`Main Category`,
				tb1.`Sub Category`,
				tb1.`Code`,
				tb1.`Item`,
				tb1.`UOM`,
				tb1.`Bom Item`,
				tb1.`Special RM`,
				tb1.`Price`,
				tb1.`Currency`,
				tb1.`Status`
		FROM (
			SELECT 
				mst_maincategory.strName as `Main Category`,
				mst_subcategory.strName as `Sub Category`,
				mst_item.strCode as `Code`,
				mst_item.strName as `Item`,
				mst_units.strName as `UOM`,
				if(mst_item.intBomItem=1,'Yes','') as `Bom Item`,
				if(mst_item.intSpecialRm=1,'Yes','') as `Special RM`,
				mst_item.dblLastPrice as `Price`,
				mst_financecurrency.strSymbol as `Currency`,
				if(mst_item.intStatus=1,'Active','In-Active') as `Status`
				FROM
				mst_item
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId
				Inner Join mst_financecurrency ON mst_item.intCurrency = mst_financecurrency.intId
				ORDER BY
				mst_item.strName ASC) AS tb1
				JOIN (SELECT @rowid:=0) as init
				";	
		$pdf->Report($sql,'ITEMS LIST');
	}
	else if($type=='mainCategory')
	{
		$sql = "SELECT 
				@rowid:=@rowid+1 as No, 
				tb1.`Code`,
				tb1.`Name`,
				tb1.`Remarks`,
				tb1.`Status`
		FROM (SELECT 
				mst_maincategory.strCode as `Code`,
				mst_maincategory.strName as `Name`,
				mst_maincategory.strRemark as `Remarks`,
				if(mst_maincategory.intStatus=1,'Active','In-Active') as `Status`
				FROM mst_maincategory
				ORDER BY
				mst_maincategory.strName ASC) AS tb1 
				JOIN (SELECT @rowid:=0) as init
				";	
		$pdf->Report($sql,'MAIN CATEGORY LIST');
	}
	else if($type=='subCategory')
	{
		$sql = "SELECT 
				@rowid:=@rowid+1 as No, 
				tb1.`Code`,
				tb1.`Name`,
				tb1.`Main Category`,
				tb1.`Remarks`,
				tb1.`Status`
		FROM (SELECT 
				mst_subcategory.strCode as `Code`,
				mst_subcategory.strName as `Name`,
				mst_maincategory.strName as `Main Category`,
				mst_subcategory.strRemark as `Remarks`,
				if(mst_subcategory.intStatus=1,'Active','In-Active') as `Status`
				FROM
				mst_subcategory
				Inner Join mst_maincategory ON mst_subcategory.intMainCategory = mst_maincategory.intId
				ORDER BY
				mst_subcategory.strName ASC) AS  tb1 
				JOIN (SELECT @rowid:=0) as init
				";	
		$pdf->Report($sql,'SUB CATEGORY LIST');
	}
	else if($type=='colors')
	{
		$sql = "SELECT 
				@rowid:=@rowid+1 as No, 
				tb1.Code,
				tb1.Color,
				tb1.Remarks,
				tb1.`Status`
		FROM (SELECT 
				mst_colors.strCode AS `Code`,
				mst_colors.strName `Color`,
				mst_colors.strRemark as `Remarks`,
				if(mst_colors.intStatus=1,'Active','In-Active') as `Status`
				FROM mst_colors
				ORDER BY
				mst_colors.strName ASC) AS  tb1 
				JOIN (SELECT @rowid:=0) as init
				";	
		$pdf->Report($sql,'COLORS LIST');
	}
	else if($type=='Techniques')
	{
		$sql = "SELECT 
				@rowid:=@rowid+1 as No, 
				tb1.Technique,
				tb1.Remarks,
				tb1.`Status`
		FROM (SELECT
			mst_techniques.strName as `Technique`,
			mst_techniques.strRemark as `Remarks`,
			if(mst_techniques.intStatus=1,'Active','In-Active') as `Status`
			FROM mst_techniques
			ORDER BY
			mst_techniques.strName ASC) AS  tb1 
				JOIN (SELECT @rowid:=0) as init
				";	
		$pdf->Report($sql,'TECHNIQUES LIST');
	}
	else if($type=='Parts')
	{
		$sql = "SELECT 
				@rowid:=@rowid+1 as No, 
				tb1.Code,
				tb1.Part,
				tb1.Remarks,
				tb1.`Status`
		FROM (SELECT
				mst_part.strCode as `Code`,
				mst_part.strName as `Part`,
				mst_part.strRemark as `Remarks`,
				if(mst_part.intStatus=1,'Active','In-Active') as `Status`
				FROM mst_part
				ORDER BY
				mst_part.strName ASC
				) AS  tb1 
				JOIN (SELECT @rowid:=0) as init
				";	
		$pdf->Report($sql,'PARTS LIST');
	}
	else if($type=='groundColors')
	{
		$sql = "SELECT 
				@rowid:=@rowid+1 as No, 
				tb1.Code,
				tb1.Name,
				tb1.Remarks,
				tb1.`Status`
		FROM (SELECT
				mst_colors_ground.strCode as `Code`,
				mst_colors_ground.strName as `Name`,
				mst_colors_ground.strRemark as `Remarks`,
				if(mst_colors_ground.intStatus=1,'Active','In-Active') as `Status`
				FROM mst_colors_ground
				ORDER BY
				mst_colors_ground.strName ASC
				) AS  tb1 
				JOIN (SELECT @rowid:=0) as init
				";	
		$pdf->Report($sql,'GROUND COLORS LIST');
	}
	else if($type=='inkTypes')
	{
		$sql = "SELECT 
				@rowid:=@rowid+1 as No, 
				tb1.`Code`,
				tb1.`Ink Type`,
				tb1.`Technique`,
				tb1.`Remarks`,
				tb1.`Status`
		FROM (SELECT
				mst_inktypes.strCode as `Code`,
				mst_inktypes.strName as `Ink Type`,
				mst_techniques.strName as `Technique`,
				mst_inktypes.strRemark as `Remarks`,
				if(mst_inktypes.intStatus=1,'Active','In-Active') as `Status`
				FROM
				mst_inktypes
				Inner Join mst_techniques ON mst_inktypes.intTechniqueId = mst_techniques.intId
				ORDER BY
				mst_inktypes.strName ASC) AS  tb1 
				JOIN (SELECT @rowid:=0) as init
				";	
		$pdf->Report($sql,'INK TYPES LIST');
	}

?>
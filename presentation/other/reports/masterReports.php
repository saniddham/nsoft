<?php
	$backwardseperator = '../../../';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Master Reports</title>

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript">
$(document).ready(function(){
	/*$('.xbutton').live('click',function(){
		var id = "";	
	});*/
});
</script>

<link rel="stylesheet" href="../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../libraries/validate/template.css" type="text/css">
</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<div align="center">
<div class="trans_layoutL" style="width:700px">
		  <div class="trans_text">Master Reports</div>
    <table width="100%" border="0" align="center" bgcolor="#FFFFFF"  class="main_header_table"  cellpadding="2" cellspacing="0">
  <tr>
    <td width="52">&nbsp;</td>
    <td width="166">&nbsp;</td>
    <td width="37">&nbsp;</td>
    <td width="166">&nbsp;</td>
    <td width="42">&nbsp;</td>
    <td width="189">&nbsp;</td>
    <td width="20">&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td ><a href="report.php?type=Companies" target="_blank"  class="mouseover"><img src="../../../images/report_go.png" class="mouseover" />&nbsp;<u>Companies.</u></a></td>
    <td>&nbsp;</td>
    <td ><a  href="report.php?type=Locations" target="_blank" class="mouseover"><img src="../../../images/report_go.png" class="mouseover" />&nbsp;<u>Locations.</u></a></td>
    <td>&nbsp;</td>
    <td ><a  href="report.php?type=Departments" target="_blank" class="mouseover"><img src="../../../images/report_go.png" class="mouseover" />&nbsp;<u>Departments.</u></a></td>
    <td >&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td ><a href="report.php?type=Users" target="_blank"  class="mouseover"><img src="../../../images/report_go.png" class="mouseover" />&nbsp;<u>Users.</u></a></td>
    <td>&nbsp;</td>
    <td ><a  href="report.php?type=Customers" target="_blank" class="mouseover"><img src="../../../images/report_go.png" class="mouseover" />&nbsp;<u>Customers.</u></a></td>
    <td>&nbsp;</td>
    <td ><a  href="report.php?type=Suppliers" target="_blank" class="mouseover"><img src="../../../images/report_go.png" class="mouseover" />&nbsp;<u>Suppliers.</u></a></td>
    <td >&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td ><a href="report.php?type=supplierLocations" target="_blank"  class="mouseover"><img src="../../../images/report_go.png" class="mouseover" />&nbsp;<u>Supplier Locations.</u></a></td>
    <td>&nbsp;</td>
    <td ><a href="report.php?type=Items" target="_blank"  class="mouseover"><img src="../../../images/report_go.png" alt="" class="mouseover" />&nbsp;<u>Items.</u></a><a  href="report.php?type=mainCategory" target="_blank" class="mouseover"></a></td>
    <td>&nbsp;</td>
    <td ><a  href="report.php?type=mainCategory" target="_blank" class="mouseover"><img src="../../../images/report_go.png" alt="" class="mouseover" />&nbsp;<u>Main Category.</u></a><a  href="report.php?type=subCategory" target="_blank" class="mouseover"></a></td>
    <td >&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<tr>
    <td>&nbsp;</td>
    <td ><a  href="report.php?type=subCategory" target="_blank" class="mouseover"><img src="../../../images/report_go.png" alt="" class="mouseover" />&nbsp;<u>Sub Category.</u></a><a href="report.php?type=colors" target="_blank"  class="mouseover"></a></td>
    <td>&nbsp;</td>
    <td ><a href="report.php?type=colors" target="_blank"  class="mouseover"><img src="../../../images/report_go.png" alt="" class="mouseover" />&nbsp;<u>Colors.</u></a></td>
    <td>&nbsp;</td>
    <td ><a href="report.php?type=Techniques" target="_blank"  class="mouseover"><img src="../../../images/report_go.png" alt="" class="mouseover" />&nbsp;<u>Techniques.</u></a></td>
    <td >&nbsp;</td>
  </tr>  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td><a href="report.php?type=Parts" target="_blank"  class="mouseover"><img src="../../../images/report_go.png" alt="" class="mouseover" />&nbsp;<u>Parts.</u></a></td>
    <td>&nbsp;</td>
    <td><a href="report.php?type=groundColors" target="_blank"  class="mouseover"><img src="../../../images/report_go.png" alt="" class="mouseover" />&nbsp;<u>Ground Colors.</u></a></td>
    <td>&nbsp;</td>
    <td><a href="report.php?type=inkTypes" target="_blank"  class="mouseover"><img src="../../../images/report_go.png" alt="" class="mouseover" />&nbsp;<u>Ink Types.</u></a></td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
</table>
    </div>
  </div>

</body>
</html>
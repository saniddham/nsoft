<?php
session_start();
$backwardseperator 	= "../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$requestType 		= $_REQUEST['requestType'];

include "{$backwardseperator}dataAccess/Connector.php";

$savedStatus			= true;
$savedMasseged 			= '';
$error_sql				= '';

if($requestType=='saveData')
{
	$plant	= $_REQUEST['plant'];
	$level	= $_REQUEST['level'];
	$arr 		= json_decode($_REQUEST['itemDetails'], true);
	
	$db->begin();
	
	//$deleteTeqArr	= deleteData($technique);
	
	
/*	if($deleteTeqArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $deleteTeqArr['savedMassege'];
		$error_sql		= $deleteTeqArr['error_sql'];	
	}
*/	$to_save		=0;
	$saved_count	=0;
	foreach($arr as $array_loop)
	{
		$userId			= $array_loop['userId'];
		$exist	=checkExistSaved($plant,$level,$userId);
		if($exist==0){
			$to_save++;
			
			$saveResultArr 	= saveData($plant,$level,$userId);
			if($saveResultArr['savedStatus']=='fail' && $savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged 	= $saveResultArr['savedMassege'];
				$error_sql		= $saveResultArr['error_sql'];	
			}
			else{
				$saved_count++;
			}
		}
	}
	if($saved_count != $to_save){
		$db->rollback();
		$response['type'] 			= "fail";
		$response['msg'] 			= "Details saving error".$saved_count .'!='. $to_save.'/'.$error_sql;
		$response['sql']			= '';
	}
	else if($savedStatus)
	{
		$db->commit();
		$response['type'] 			= "pass";
		$response['msg'] 			= "Saved Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 			= "fail";
		$response['msg'] 			= $savedMasseged;
		$response['sql']			= $error_sql;
	}
	echo json_encode($response);
}
else if($requestType=='deleteData')
{
	$savedStatus	= true;
	$plant			= $_REQUEST['plant'];
	$level			= $_REQUEST['level'];
	$userId			= $_REQUEST['userId'];
	$flag			=0;
	
	$db->begin();
	
	$user_name		= getUserName($userId);
	$exists_saved	= checkExistSaved($plant,$level,$userId);
	$deleteTeqArr	= deleteData($plant,$level,$userId);
	if($deleteTeqArr['savedStatus']=='fail' && $exists_saved==1)
	{
		$flag			=1;
		$savedStatus	= false;
		$savedMasseged 	= $deleteTeqArr['savedMassege'];
		$error_sql		= $deleteTeqArr['error_sql'];	
	}
	else if($deleteTeqArr['savedStatus']!='fail' && $exists_saved==1){
		$flag			=2;
		$savedStatus	= true;
		$savedMasseged 	= '"'.$user_name.'" deleted Successfully.';
	}
	else if($exists_saved==0)
	{
		$flag			=3;
		$savedStatus	= true;
		$savedMasseged 	= '"'.$user_name.'" deleted Successfully.';
	}
	

	if($savedStatus == false){
		$db->rollback();
		$response['type'] 			= "fail";
		$response['msg'] 			= "error";
		$response['sql']			= '';
	}
	else if($savedStatus && $flag > 0)
	{
		$db->commit();
		$response['type'] 			= "pass";
		$response['msg'] 			= $savedMasseged;
	}
	else
	{
		$db->rollback();
		$response['type'] 			= "fail";
		$response['msg'] 			= $savedMasseged/*.'-'. $deleteTeqArr['savedStatus'].'-'.$exists_saved*/;
		$response['sql']			= $error_sql;
	}
	echo json_encode($response);
}


function deleteData($plant,$level,$user)
{
	global $db;
	
	$sql = "DELETE FROM mst_plant_dispatch_approve_users 
			WHERE
			PLANT_ID = '$plant'  and 
			APPROVE_LEVEL = '$level'   and 
			USER = '$user' ";
	
	$result = $db->RunQuery2($sql);
	if(!$result)
	{
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= $db->errormsg;
		$data['error_sql']		= $sql;
	}
	return $data;
}
function saveData($plant,$level,$user)
{
	global $db;
	
	$sql = "INSERT INTO mst_plant_dispatch_approve_users 
			(
			PLANT_ID, 
			APPROVE_LEVEL, 
			USER
			)
			VALUES
			(
			'$plant', 
			'$level', 
			'$user'
			)";
	
	$result = $db->RunQuery2($sql);
	if(!$result)
	{
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= $db->errormsg;
		$data['error_sql']		= $sql;
	}
	return $data;
}

function checkExistSaved($plant,$level,$user)
{
	global $db;
	
	$sql 		= "select * from mst_plant_dispatch_approve_users 
					WHERE
					PLANT_ID = '$plant' and 
					APPROVE_LEVEL = '$level'  and 
					USER = '$user' ";
	
	$result 	= $db->RunQuery2($sql);
	$rowCount	=mysqli_num_rows($result);
	if($rowCount > 0)
	{
		return 1;
 	}
	else
		return 0;
}

 function getUserName($userId)
{
	global $db;
	
	$sql 		= "SELECT
					sys_users.strUserName
					FROM `sys_users`
					WHERE
					sys_users.intUserId = '$userId' ";
	
	$result 	= $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
	return $row['strUserName'];
}


?>
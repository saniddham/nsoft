var basepath	= 'presentation/administration/dispatch_approve_levels/';
var menuID		= ''; 

$(document).ready(function() {	
	//alert("dsfsdfs");
	$("#frmDispApproval").validationEngine();
  	//$('#frmDispApproval #butSave').die('click').live('click',save);
	$('#frmDispApproval #butAdd').die('click').live('click',addNewItem);
   $('#frmDispApproval #cboSearch').die('change').live('change',loadTechniqueGrid);	
	//$('#frmDispApproval #butNew').die('click').live('click',clearForm);	
    //$('#frmDispApproval #butDelete').die('click').live('click',deleteData);
	$('#frmDispApproval #cboLevel').die('change').live('change',loadTechniqueGrid);
	$('#frmDispApproval_popup #butAdd').die('click').live('click',addClickedRows);
	$('#frmDispApproval #butSave').die('click').live('click',saveData);
	$('#frmDispApproval .removeRow').die('click').live('click',deleteRow);
	$("#frmDispApproval #butNew").die('click').live('click',clearForm);
});

function addNewItem()
{
	//alert('ok');
	if($("#frmDispApproval").validationEngine('validate'))
	{
		$('#popupContact1').html('');
		popupWindow3('1');
		$('#popupContact1').load(basepath+'popUp.php',function(){
			
				$('#butPopClose').die('click').live('click',disablePopup);
		});
	}
}

function addClickedRows()
{
	$('.clsChkDisp:checked').each(function(){
		
		var fromId 	= $(this).parent().parent().attr('id') ;
		var found 	= false ;
		$('#tblMain .removeRow').each(function(){
			
			if(fromId == $(this).parent().parent().attr('id'))
			{
				found = true;
			}	
		});
		if(found==false)
		{
			document.getElementById('tblMain').insertRow(document.getElementById('tblMain').rows.length);
			document.getElementById('tblMain').rows[document.getElementById('tblMain').rows.length-1].innerHTML = "<td bgcolor=\"#FFFFFF\" id=\""+$(this).parent().parent().find('.name').attr('id')+"\">"+
			"<img class=\"mouseover removeRow\" src=\"images/del.png\" /></td>"+
			"<td bgcolor=\"#FFFFFF\" class=\"normalfnt user\">"+$(this).parent().parent().find('.name').html()+"-"+$(this).parent().parent().find('.fullName').html()+"</td>";
			document.getElementById('tblMain').rows[document.getElementById('tblMain').rows.length-1].id  = fromId;
			//setRemoveRow();
		}
	});
	disablePopup();
}

function loadTechniqueGrid(){
	
	var plant = $('#frmDispApproval #cboSearch').val();
	var level = $('#frmDispApproval #cboLevel').val();
	
	//var basepath1	= 'presentation/administration/dispatch_approve_levels/';
	
	var url = basepath+"dispatch_approve_levels-db-get.php";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		type:'post',
		data:'requestType=loadApprovalGrid&plant='+plant+'&level='+level,
		async:false,
		success:function(json){
			$('#frmDispApproval #tblMain').show();
			$('#tblMain #tbodyDtl').html(json.gridDtl);
			//$('#tblMain #techniqueHeader').html(json.headerDtl);
		}
	});
	//alert('ok');
}
 
function saveData()
{
	showWaiting();
	var plant = $('#cboSearch').val();
	var level = $('#cboLevel').val();
	var value ='';
	
	if($('#frmDispApproval').validationEngine('validate'))
	{
		value = "[ ";
		$('#tblMain .user').each(function(){
			
			var userId	= $(this).parent().attr('id');
			value += '{"userId": "'+userId+'"},';
		});
		
		value = value.substr(0,value.length-1);
		value += " ]";
		
		var url = basepath+"dispatch_approve_levels-db-set.php?requestType=saveData";
			var obj = $.ajax({
				url:url,
				dataType: "json",  
				data:'&plant='+plant+'&level='+level+'&itemDetails='+value,
				async:false,
				type:'POST',
				success:function(json){
						$('#frmDispApproval #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
						{
							var t = setTimeout("alertx()",3000);
							hideWaiting();
							return;
						}
						else
						{
							hideWaiting();
						}
					},
				error:function(xhr,status){
						
						$('#frmDispApproval #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
					}		
				});
	}
	hideWaiting();
}
function deleteRow(){
	var obj_r 		=this;
	var flag		=0;
	var alertMsg	= '';
	
	showWaiting();
	var plant = $('#frmDispApproval #cboSearch').val();
	var level = $('#frmDispApproval #cboLevel').val();
	
	if($('#frmDispApproval').validationEngine('validate'))
	{
		var userId	= $(obj_r).parent().parent().attr('id');
 		
 		
		var url = basepath+"dispatch_approve_levels-db-set.php?requestType=deleteData";
			var obj = $.ajax({
				url:url,
				dataType: "json",  
				data:'&plant='+plant+'&level='+level+'&userId='+userId,
				async:false,
				type:'POST',
				success:function(json){
						$('#frmDispApproval '+$(obj_r).attr('id')).validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
						{
							flag	=1;
							var t = setTimeout("alertx()",3000);
							hideWaiting();
							alertMsg	=json.msg;
							return;
						}
						else
						{
							hideWaiting();
						}
					},
				error:function(xhr,status){
						
						$('#frmDispApproval '+$(obj_r).attr('id')).validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
					}		
				});
	}
	hideWaiting();
	
	if(flag==1){
	alert(alertMsg);
	$(obj_r).parent().parent().remove();
	}
	else{
	alert('Error');
	}
	

}
function clearForm()
{
	$('#frmDispApproval #cboSearch').val('');
	$("#frmDispApproval #tblMain tr:gt(1)").remove();
}


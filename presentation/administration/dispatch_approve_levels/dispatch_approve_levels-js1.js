var basepath	= 'presentation/administration/dispatch_approve_levels/';
var menuID		= ''; 
alert("Sdfsdfsdfsdf");
$(document).ready(function() {	
	alert("dsfsdfs");
	$("#frmDispApproval").validationEngine();
  	$('#frmDispApproval #butSave').die('click').live('click',save);
    $('#frmDispApproval #cboSearch').die('change').live('change',loadTechniqueGrid);	
	$('#frmDispApproval #butNew').die('click').live('click',clearForm);	
    $('#frmDispApproval #butDelete').die('click').live('click',deleteData);
	$('#frmDispApproval #cboLevel').die('change').live('change',loadTechniqueGrid);
	
});

function save()
{	
	if(!$('#frmDispApproval').validationEngine('validate'))
	{
		var t = setTimeout("alertx('#frmDispApproval')",2000);
		return;
	}
	
	showWaiting();
	
	if($("#frmDispApproval #cboSearch").val()=='')
		requestType = 'add';
	else
		requestType = 'edit';
	
	var url = basepath+"technique-db-set.php";
	var obj = $.ajax({
		url:url,
		dataType: "json",
		type:'post', 
		data:$("#frmDispApproval").serialize()+'&requestType='+requestType,
		async:false,			
		success:function(json){
				$('#frmDispApproval #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				if(json.type=='pass')
				{
					$('#frmDispApproval #txtTechId').val(json.techId);
					var t=setTimeout("alertx('#frmDispApproval #butSave')",2000);
					hideWaiting();
					clearForm();
					return;
				}
				hideWaiting();
				var t = setTimeout("alertx('#frmInkType #butSave')",4000);
			},
		error:function(xhr,status){				
				$('#frmDispApproval #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				var t = setTimeout("alertx('#frmInkType #butSave')",4000);
			}		
	});
}

function deleteData()
{	
	if($('#frmDispApproval #cboSearch').val()=='')
	{
		$('#frmDispApproval #butDelete').validationEngine('showPrompt', 'Please select Type.', 'fail');
		var t=setTimeout("alertx('#frmDispApproval #butDelete')",2000);
		return;	
	}
	
	var val = $.prompt('Are you sure you want to delete "'+$('#frmDispApproval #cboSearch option:selected').text()+'" ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
				var url = basepath+"technique-db-set.php";
				var httpobj = $.ajax({
					url:url,
					dataType:'json',
					type:'post',
					data:'requestType=delete&cboSearch='+$('#frmDispApproval #cboSearch').val(),
					async:false,
					success:function(json){						
						$('#frmDispApproval #butDelete').validationEngine('showPrompt', json.msg,json.type);						
						if(json.type=='pass')
						{
							clearForm();							
							var t = setTimeout("alertx('#frmDispApproval #butDelete')",2000);return;
						}	
						var t = setTimeout("alertx('#frmDispApproval #butDelete')",3000);
					},
					error:function(xhr,status){							
						$('#frmDispApproval #butDelete').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t = setTimeout("alertx('#frmDispApproval #butDelete')",4000);
					} 
				});
			}
		}
	});		
}
function loadTechniqueGrid()
{
	
	var plant = $('#frmDispApproval #cboSearch').val();
	var level = $('#frmDispApproval #cboLevel').val();
	
	var url = basepath+"dispatch_approve_levels-db-get.php";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		type:'post',
		data:'requestType=loadApprovalGrid&plant='+plant&level='+level,
		async:false,
		success:function(json){
			$('#frmDispApproval #tblMain').show();
			$('#tblMain #tbodyDtl').html(json.gridDtl);
			$('#tblMain #techniqueHeader').html(json.headerDtl);
		}
	});
}


function clearForm()
{	
	var t = setTimeout("alertx('#frmDispApproval')",2000);	
	showWaiting();
	loadCombo_frmDispApproval();
	$('#frmDispApproval #cboSearch').val('');
	$('#frmDispApproval #txtName').val('');
	$('#frmDispApproval #txtRemark').val('');
	$('#frmDispApproval #cboTechniqueGroup').val('');
	$('#frmDispApproval #chkFoil').prop('checked',false);
	$('#frmDispApproval #chkActive').prop('checked',true);
	$('#frmDispApproval #txtName').focus();
	$('#frmDispApproval #tblMain').hide();
	hideWaiting();	
}


function alertx(obj)
{
	$(obj).validationEngine('hide')	;
}
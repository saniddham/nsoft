<?php
$ticketNo       = $_REQUEST['ticketNo'];
$reciverName	= $_REQUEST['reciver'];
$senderName		= $_REQUEST['sender'];
$link			= base64_decode($_REQUEST['link']);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add Ticket Email</title>
<style type="text/css">
.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntWhite {
	font-family: Verdana;
	font-size: 11px;
	color: #FFF;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.sampleNo{
	color: #039; font-weight: bold; font-size: 12px; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;	
}
.part{
	color: #096CBD;
	font-weight: bold;
	font-size: 13px;
	font-family: "Comic Sans MS", cursive;
}
.tableBorder_allRound{
	
	border: 1px solid #CCCCCC;
/*	-moz-border-radius-bottomright:10px;
	-moz-border-radius-bottomleft:10px;
	-moz-border-radius-topright:10px;
	-moz-border-radius-topleft:10px;*/
	
	border-radius:10px 10px 10px 10px;
}
.normalfnt1 {	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfnt1 {	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
</style>
</head>

<body>
<table  width="595" border="0" cellspacing="2" cellpadding="0" class="tableBorder_allRound" <?php echo ($_SESSION['projectName']=='qpay'?'style="border-color:#025077"':'style="border-color:#f6a813"'); ?> >
  <tr>
    <td colspan="3" class="normalfnt" style="color:#3E437D">&nbsp;</td>
  </tr>
  <tr>
    <td width="151" class="normalfnt">Dear <strong><?php echo $reciverName; ?></strong>,</td>
    <td width="440">&nbsp;</td>
    <td width="4">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><span class="normalfnt1">I have completed the <strong>ticket #<?php echo $ticketNo; ?></strong> in <?php echo $_SESSION['projectName']; ?> system. Please refer the below link to view details.</span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">click <a  href="<?php  echo $link ?>">here</a> to view completed ticket.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"></td>
  </tr>
  <tr>
    <td class="normalfnt">Thanks,</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="normalfnt"><strong><?php echo $senderName; ?></strong><br />
    ...................</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="normalfnt">(This is a<strong> <span <?php echo ($_SESSION['projectName']=='qpay'?'style="color:#025077"':'style="color:#f6a813"'); ?>><?php echo strtoupper($_SESSION['projectName']); ?></span> </strong>system generated email.)</td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
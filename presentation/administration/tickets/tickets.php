<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>

<?php
$userId				= $_SESSION["userId"];
$moduleId			= -1;
$ticketId 	  		= $_REQUEST['id'];
$comStatus 	   		= $_REQUEST['comStatus']; 

if($ticketId!="")
{
	$sql = "SELECT 	
			intTrack, 
			strNote, 
			intModuleId, 
			iniProgramId, 
			intPriority,
			dtCreatedBy,
			sys_users.strContactNo,
			sys_users.strEmail
			FROM 
			t_tickets 
			INNER JOIN sys_users ON sys_users.intUserId=t_tickets.dtCreatedBy
			WHERE intTicketId='$ticketId' ";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$trackId 	= $row['intTrack'];
		$errNote 	= $row['strNote'];
		$moduleId 	= $row['intModuleId'];
		$programId 	= $row['iniProgramId'];
		$priority 	= $row['intPriority'];
		$createBy   = $row['dtCreatedBy'];
		$contactNo  = $row['strContactNo'];
		$email 		= $row['strEmail'];	
	}
}
else
{
	$sql = "SELECT strContactNo,strEmail FROM sys_users WHERE intStatus = 1 AND intUserId = '$userId' ";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$contactNo = $row['strContactNo'];
		$email	   = $row['strEmail'];
	}
}
?>
<title>Nsoft Ticket</title>

<form id="frmNsoftTicket" name="frmNsoftTicket" enctype="multipart/form-data" method="post">
<div align="center">
<div class="trans_layoutD">
<div class="trans_text">Ticketing System</div>

  
  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <tr>
      <td>
      	<table width="585" border="0" align="center">
        <tr> 
         	<td height="47" ><table width="100%" border="0" cellpadding="2" class="">
                <tr class="ticketNoTr">
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt"><?php echo($ticketId==""?'':'Ticket No');?></td>
                  <td id="<?php echo $ticketId; ?>" width="358" <?php echo($ticketId==""?'':'bgcolor="#4F94CD"'); ?> class="normalfnt clsticketNo tableBorder_allRound" style="color:#FFF;font-size:14px;font-weight:bold;text-align:center;border-color:#FFF"><?php echo($ticketId==""?'':'#'.$ticketId);?></td>
                  <td width="54" class="normalfnt">&nbsp;</td>
                  </tr>
                <tr>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt">Track</td>
                  <td colspan="2"><select name="cboTrack" id="cboTrack" style="width:360px"  class="validate[required]">
                  <option value=""></option>
                    <?php
					$sql = "SELECT intTrackerId,strTrackerName FROM t_trackers WHERE intStatus = 1";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($trackId==$row['intTrackerId'])
							echo "<option value=\"".$row['intTrackerId']."\" selected=\"selected\">".$row['strTrackerName']."</option>";
						else
							echo "<option value=\"".$row['intTrackerId']."\">".$row['strTrackerName']."</option>";
					}
					?>
                  </select></td>
                </tr>
                <tr>
                  <td width="1" class="normalfnt">&nbsp;</td>
                  <td width="140" class="normalfnt">Module&nbsp;<span class="compulsoryRed">*</span></td>
                  <td colspan="2"><select name="cboModule" id="cboModule" style="width:360px"  class="validate[required]">
                  <option value=""></option>
                    <?php
						$sql = "SELECT DISTINCT intId,strName FROM menus 
								INNER JOIN menupermision ON menupermision.intMenuId=menus.intId
								WHERE menus.intParentId=0 AND 
								menus.intStatus=1 
								ORDER BY intOrderBy";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							if($moduleId==$row['intId'])
								echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
							else
								echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
					?>
                  </select></td>
                </tr>
                <tr>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt">Program <font color="#FF0000">*</font>&nbsp;</td>
                  <td colspan="2"><select name="cboProgram" id="cboProgram" style="width:360px"  class="validate[required]">
                  <?php
				  		$sql = "SELECT DISTINCT menus.intId,menus.strName FROM menus 
								INNER JOIN menupermision ON menupermision.intMenuId=menus.intId
								WHERE menus.intParentId='$moduleId' AND 
								
								menus.intStatus = 1
								ORDER BY intOrderBy";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							if($programId==$row['intId'])
								echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
							else
								echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
				  ?>
                    </select></td>
                </tr>
                <tr>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt" style="vertical-align:top">Error Note <font color="#FF0000">*</font></td>
                  <td colspan="2"><textarea name="txtErrorNote" id="txtErrorNote" cols="45" rows="5"  class="validate[required]" style="width:358px" ><?php echo $errNote;?></textarea></td>
                </tr>
                <tr>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt">priority<u></u><u></u></td>
                  <td colspan="2"><select name="cboPriorty" id="cboPriorty" style="width:360px"  class="validate[required]">
                    <option value="1" <?php echo ($priority==1?'selected="selected"':'');?> >Low</option>
                    <option value="2" <?php echo ($priority==2?'selected="selected"':'');?>>Normal</option>
                    <option value="3" <?php echo ($priority==3?'selected="selected"':'');?>>High</option>
                    </select></td>
                </tr>
                <tr>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt">Contact No</td>
                  <td colspan="2"><input type="text" name="contactNo" id="contactNo"  style="width:358px" value="<?php echo $contactNo; ?>" /></td>
                </tr>
                <tr>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt">Contact Email <font color="#FF0000">*</font></td>
                  <td colspan="2"><input type="text" name="contactEmail" id="contactEmail"  style="width:358px" disabled="disabled" class="validate[required]" value="<?php echo $email; ?>"/></td>
                </tr>
               
                <tr class="fileUpload" <?php echo ($ticketId==""?'style="display:none"':'');?>>
                  <td class="normalfnt">&nbsp;</td>
                  <td class="normalfnt">&nbsp;</td>
                  <td colspan="2"><iframe id="iframeFiles" src="presentation/administration/tickets/filesUpload.php?txtFolder=<?php echo $ticketId; ?>&comStatus=<?php echo $comStatus;?>&createBy=<?php echo $createBy;?>" name="iframeFiles" style="width:390px;height:175px;border:none" ></iframe></td>
                </tr>
                
                </table>
         	</td>
        </tr>
        
        <tr>
        	<td height="34">
            	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                <tr>
                  <td width="100%" align="center" bgcolor=""><a class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butSave" name="butSave" <?php echo ($ticketId!=""?(($comStatus==1 || $createBy!=$userId ?'style="display:none"':'')):'');?>>Save</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></a></td>
                </tr>
              </table>
             </td>
        </tr>
      </table>
      </td>
    </tr>
  </table>
</div>
</div>
</form>
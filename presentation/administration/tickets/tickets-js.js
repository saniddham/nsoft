// JavaScript Document
var basePath	= "presentation/administration/tickets/"
var menuId		= 623;

$(document).ready(function(){
	$("#frmNsoftTicket").validationEngine();
	
	$('#frmNsoftTicket #cboModule').die('change').live('change',function(){
		
		if($(this).val()!="")
		{
			var url 	= basePath+'tickets-db-get.php?requestType=loadProgram&moduleId='+$(this).val();
			var htppObj = $.ajax({url:url,async:false});
			$('#frmNsoftTicket #cboProgram').html(htppObj.responseText); 	
		}
		else
		{
			$('#frmNsoftTicket #cboProgram').html('');
		}
	});
	$('#frmNsoftTicket #butSave').die('click').live('click',function(){
		
		if($("#frmNsoftTicket").validationEngine('validate'))
		{
			var url = basePath+'tickets-db-set.php?requestType=saveData';
			var ticketId = $('#frmNsoftTicket .clsticketNo').attr('id');
			$.ajax({
					url:url,
					dataType: "json",  
					data:$("#frmNsoftTicket").serialize()+'&ticketId='+ticketId,
					async:false,
					success:function(json)
					{
						$('#frmNsoftTicket #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
						{
							document.location.href = "main.php?MenuId="+menuId+"&id="+json.ticketId;
							return;
						}
					},
					error:function(xhr,status)
					{
						$('#frmNsoftTicket #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertx()",1000);
						return;	
					}		
			});
		}
	});
	$('#frmNsoftTicket #butNew').die('click').live('click',function(){
		
		document.location.href = "?";
	});
});
function alertx()
{
	$('#frmNsoftTicket #butSave').validationEngine('hide')	;
	$('#frmNsoftTicket #button').validationEngine('hide')	;
}
function newPage(){
	$('#frmNsoftTicket').validationEngine('hide');
	$('#frmNsoftTicket').get(0).reset();
	$('#frmNsoftTicket').focus();
}
<?php
$userId				= $_SESSION["userId"];
$userName			= $_SESSION["systemUserName"];

$menuId				= 623;
$comentMenuId		= 889;

require_once("libraries/jqgrid2/inc/jqgrid_dist.php");

// {
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'NewticketNo'			=> "CONCAT('#',t_tickets.intTicketId)",
				'caitStatus'			=> 't_tickets.intStatus',
				'strTrackerName'		=> 't_trackers.strTrackerName',
				'strUserName'			=> 'sys_users.strUserName',
				'dtCreatedDate'			=> 'date(t_tickets.dtCreatedDate)',
				'module'				=> 'menus.strName',
				'program'				=> "(SELECT M.strName FROM menus M WHERE M.intId = t_tickets.iniProgramId)",
				'strNote'				=> 't_tickets.strNote',
				'priorty'				=> 't_tickets.intPriority',
				'dblDevelopmentHours'	=> 't_tickets.dblDevelopmentHours',
				'completeBy'			=> 'csu.strUserName',
				'dtmCompleteDate'		=> 'date(t_tickets.dtmCompleteDate)',
				'lastCommentedBy'		=> "(SELECT sys_users.strUserName FROM t_comments tc
									 		 INNER JOIN sys_users ON sys_users.intUserId=tc.userId
									 		 WHERE tc.ticketId=t_tickets.intTicketId
									 		 ORDER BY id DESC LIMIT 1)",
				'lastCommentedDate'		=> "(SELECT tc.dateTime FROM t_comments tc
											 WHERE tc.ticketId=t_tickets.intTicketId
											 ORDER BY id DESC LIMIT 1)"
				);
				
$arr_status = array('Completed'=>'1','Pending'=>'0');
$arr_priority = array('Low'=>'1','Normal'=>'2','High'=>'3');
foreach($arr as $k=>$v)
{
	if($v['field']=='caitStatus')
	{
		$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	if($v['field']=='priorty')
	{
		$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_priority[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND date(t_tickets.dtCreatedDate) = '".date('Y-m-d')."'";
	
// }
$obgTest 	= new jqgrid('',$db);
$col 		= array();
$cols		= array();

$col["title"] = "Ticket No"; // caption of column
$col["name"]  = "NewticketNo"; 
$col["width"] = "60";
$col["link"]  = "main.php?MenuId=".$menuId."&id={intTicketId}&comStatus={intStatus}"; // e.g. http://domain.com?id={id} given
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
//$col["default"] = "{intTicketId}";
$cols[] = $col;		

$col = array();
$col["title"] = "ticket Id";
$col["name"] = "intTicketId";
$col["width"] = "80";
$col["align"] = "left";
$col["search"] = true;
$col["hidden"] = true;
$cols[] = $col;

$col = array();
$col["title"] = "Status";
$col["name"] = "caitStatus";
$col["width"] = "70";
$col["align"] = "left";
$col["search"] = true;

# these 3 lines will make dropdown in search autofilter
$col["stype"] = "select";
// all row, blank row, then all db values. ; is separator
$str = ":All;Pending:Pending;Completed:Completed";
$col["searchoptions"] = array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] = $col;

$col = array();
$col["title"] = "Status1";
$col["name"] = "intStatus";
$col["width"] = "80";
$col["align"] = "left";
$col["search"] = true;
$col["hidden"] = true;
$cols[] = $col;	
		
$col = array();
$col["title"] = "Track";
$col["name"] = "strTrackerName"; 
$col["width"] = "70";

# these 3 lines will make dropdown in search autofilter
$col["stype"] = "select";
// all row, blank row, then all db values. ; is separator
$str = ":All;Bug:Bug;Feature:Feature;Support:Support";
$col["searchoptions"] = array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] = $col;

$col = array();
$col["title"] = "Add By";
$col["name"] = "strUserName"; 
$col["width"] = "70";
$cols[] = $col;

$col = array();
$col["title"] = "Add Date";
$col["name"] = "dtCreatedDate"; 
$col["width"] = "70";
$cols[] = $col;

$col = array();
$col["title"] = "Module";
$col["name"] = "module"; 
$col["width"] = "120";
$cols[] = $col;

$col = array();
$col["title"] = "Program";
$col["name"] = "program"; 
$col["width"] = "120";
$cols[] = $col;

$col = array();
$col["title"] = "Note";
$col["name"] = "strNote"; 
$cols[] = $col;
//////
$col = array();
$col["title"] = "Priority";
$col["name"] = "priorty"; 
$col["width"] = "70";
# these 3 lines will make dropdown in search autofilter
$col["stype"] = "select";
// all row, blank row, then all db values. ; is separator
$str = ":All;Low:Low;Normal:Normal;High:High";
$col["searchoptions"] = array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] = $col;

$col = array();
$col["title"] = "Dev Hrs";
$col["name"] = "dblDevelopmentHours"; 
$col["width"] = "60";
$cols[] = $col;

$col = array();
$col["title"] = "CompleteBy";
$col["name"] = "completeBy"; 
$col["width"] = "75";

$cols[] = $col;

$col = array();
$col["title"] = "Complete Date";
$col["name"] = "dtmCompleteDate"; 
$col["width"] = "110";
$cols[] = $col;

$col = array();
$col["title"] = "Lst CommentBy";
$col["name"] = "lastCommentedBy"; 
$col["width"] = "100";
$cols[] = $col;

$col = array();
$col["title"] = "Lst CommentDate";
$col["name"] = "lastCommentedDate"; 
$col["width"] = "110";
$cols[] = $col;


$col = array();
$col["title"] = "View";
$col["width"] = "50";
$col["link"] = "main.php?MenuId=".$comentMenuId."&ticketId={intTicketId}"; // e.g. http://domain.com?id={id} given
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$col["default"] = "View";
$col["search"]	= false;
$cols[] = $col;

$grid["align"] = "center"; // Grid Align
$grid["caption"] = "Tickets List View"; // caption of grid
$grid["export"] = array("format"=>"excel", "filename"=>"my-file", "sheetname"=>"test");
$grid["autowidth"] = true; 
$grid["sortorder"] = "desc"; // ASC or DESC

$grid["search"] = true; 
$grid["postData"] = array("filters" => $sarr ); 

$obgTest->set_options($grid);
$obgTest->set_actions(array(	
						"add"=>false, // allow/disallow add
						"edit"=>false, // allow/disallow edit
						"delete"=>false, // allow/disallow delete
						"rowactions"=>false, // show/hide row wise edit/del/save option
						"export"=>true, // show/hide export to excel option
						"autofilter" => true, // show/hide autofilter for search
						"search" => "advance" // show single/multi field search condition (e.g. simple or advance)
					) 
				);


$obgTest->select_command = "SELECT * FROM
							(SELECT
							t_tickets.intTicketId,
							CONCAT('#',t_tickets.intTicketId) As NewticketNo,
							date(t_tickets.dtCreatedDate) as dtCreatedDate,
							t_tickets.dtCreatedBy,
							sys_users.strUserName,
							t_tickets.intCompletedBy,
							csu.strUserName as completeBy,
							date(t_tickets.dtmCompleteDate) as dtmCompleteDate ,
							t_tickets.strNote,
							t_tickets.intStatus,
							t_tickets.intModuleId,
							menus.strName AS module,
							t_tickets.iniProgramId,
							(SELECT M.strName FROM menus M WHERE M.intId = t_tickets.iniProgramId)AS program,
							t_tickets.intPriority,
							IF(t_tickets.intPriority=1,'Low',IF(t_tickets.intPriority=2,'Normal','High')) AS priorty,
							IF(t_tickets.intStatus=0,'Pending','Completed') AS caitStatus,
							t_tickets.dblDevelopmentHours,
							t_tickets.intTrack,
							t_trackers.strTrackerName,
							(SELECT sys_users.strUserName FROM t_comments tc
							 INNER JOIN sys_users ON sys_users.intUserId=tc.userId
							 WHERE tc.ticketId=t_tickets.intTicketId
							 ORDER BY id DESC LIMIT 1) AS lastCommentedBy,
							 (SELECT tc.dateTime FROM t_comments tc
							 WHERE tc.ticketId=t_tickets.intTicketId
							 ORDER BY id DESC LIMIT 1) AS lastCommentedDate
							FROM
							t_tickets
							INNER JOIN menus ON menus.intId = t_tickets.intModuleId
							INNER JOIN t_trackers ON t_trackers.intTrackerId = t_tickets.intTrack
							INNER JOIN sys_users ON sys_users.intUserId=t_tickets.dtCreatedBy
							LEFT JOIN sys_users csu ON csu.intUserId=t_tickets.intCompletedBy
							WHERE 1=1 
							$where_string
							) AS t WHERE 1=1 "; //echo $obgTest->select_command;
											
$obgTest->set_columns($cols);

$out = $obgTest->render("list1");
?>
<title>Tickets List View</title>
<?php
echo $out;
	
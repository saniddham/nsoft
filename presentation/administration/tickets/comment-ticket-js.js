// JavaScript Document
var basePath	= "presentation/administration/tickets/"
var menuId		= 889;
$(document).ready(function(){
	
	$('#frmComplete #butComplete').die('click').live('click',completeTicket);
	$('#frmComplete #butSaveHrs').die('click').live('click',saveCompleteHours);
	$('#frmComplete #butInsert').die('click').live('click',setToAddNew);
	$('#frmComplete .removeRow').die('click').live('click',removeEUser);
	$('#frmComplete .removeApprRow').die('click').live('click',removeApprEUser);
	$('#frmComplete #butApprInsert').die('click').live('click',setToAddNewAppr);
	$('#frmComplete #butApprove').die('click').live('click',approveTicket);
	
});
/*function changeTrack(){
	var track = $('#cboTrack').val();
	var ticketId = $('#ticketId').val();
	var testUrl  = 'tickets-db.php?requestType=changeTrack';
		testUrl	+='&track='+track;
		testUrl	+='&ticketId='+ticketId;
		///////sent ajax/////
		$.ajax({
				url:testUrl,
				async:false,
				dataType:'json',
				success:saveSuccessss,
			});
			
		function saveSuccessss(json){
		$('#cboTrack').validationEngine('showPrompt', json.msg,json.type);
		setTimeout(timeout,2000);
	}
}*/
function completeTicket(){
	
	var ticketId = $('#frmComplete #ticketId').val();
	var testUrl  = basePath+'tickets-db-set.php?requestType=completeTicket';
	testUrl	+='&ticketId='+ticketId;
	///////sent ajax/////
	$.ajax({
			url:testUrl,
			async:false,
			dataType:'json',
			success:saveSuccessss,
		});
	
	function saveSuccessss(json)
	{
		if(json.type=="pass")
		{
			parent.location.href = "main.php?MenuId="+menuId+"&ticketId="+ticketId;
		}
	}
	
}
function saveCompleteHours()
{
	
	var ticketId = $('#frmComplete #ticketId').val();
	var comHrs   = $('#frmComplete #txtCompleteHrs').val();
	var trackId  = $('#frmComplete #cboTrack').val();
	var testUrl  = basePath+'tickets-db-set.php?requestType=saveCompleteHrs';
	testUrl	+='&ticketId='+ticketId+'&completeHrs='+comHrs+'&trackId='+trackId;
		///////sent ajax/////
		$.ajax({
				url:testUrl,
				async:false,
				dataType:'json',
				success:saveSuccessss,
			});
		
	function saveSuccessss(json)
	{
		if(json.type=="pass")
		{
			document.location.href = "main.php?MenuId="+menuId+"&ticketId="+ticketId;
		}
	}

}
function setToAddNew()
{
	var ticketNo = $('#frmComplete #ticketId').val();
	popupWindow3('1');
	$('#popupContact1').html('');
	$('#popupContact1').load(basePath+'emailPopup.php?ticketNo='+ticketNo,function(){
		
		$('#frmPopUp #butAdd').click(function(){
			//addEmailsToGrid(mainRowId,typeRowId,cellId);
			$('#frmPopUp .chkUsers:checked').each(function(){
				var fromId = $(this).val();
				//var obj = $(this);
				var found = false;
				$('#frmComplete #tblMainAU >tr').each(function(){
					if(fromId ==$(this).attr('id'))
					{
						found = true;
					}	
				});
				if(found==false)
				{
					var chkAddUser  = false;
					var chkStatus = false;
					var url = basePath+"tickets-db-set.php?requestType=additionalEmailUsers";
					$.ajax({
							url:url,
							async:false,
							dataType:'json',
							type:'post',
							data:'&euserId='+fromId+'&ticketNo='+ticketNo,
							success:function(json)
							{
								if(json.type=='pass')
								{
									chkStatus = true;
									intUserId 		= json.userId;
									
									if(intUserId==fromId)
										chkAddUser = true;
								}
								else
								{
									$('#frmPopUp #butAdd').validationEngine('showPrompt',json.msg,'fail');
									var t=setTimeout("alertx()",3000);	
									return;
								}
							},
							error:function(xhr,status)
							{
								$('#frmPopUp #butAdd').validationEngine('showPrompt',json.msg,'fail');
								var t=setTimeout("alertx()",3000);	
								return;					
							}
					});
					if(chkStatus)
					{
						document.getElementById('tblMainAU').insertRow(document.getElementById('tblMainAU').rows.length);
					document.getElementById('tblMainAU').rows[document.getElementById('tblMainAU').rows.length-1].innerHTML = "<td align=\"center\" bgcolor=\"#FFFFFF\">"+(chkAddUser?'<img class=\"mouseover removeApprRow\" src=\"images/del.png\" />':'')+"</td>"+
   		 			"<td bgcolor=\"#FFFFFF\" class=\"normalfnt\">"+$(this).parent().parent().find('td').eq(1).html()+"</td>"+
    				"<td colspan=\"2\" bgcolor=\"#FFFFFF\" class=\"normalfnt\">"+$(this).parent().parent().find('td').eq(2).html()+"</td>";
					document.getElementById('tblMainAU').rows[document.getElementById('tblMainAU').rows.length-1].id  = fromId;
					setRemoveRow();
					}
				}
			});
			disablePopup();	
		});
		
		$('#frmPopUp #butClose1').click(disablePopup);
		
	});			
}
function setToAddNewAppr()
{
	var ticketNo = $('#frmComplete #ticketId').val();
	var intUserId ='';
	popupWindow3('1');
	$('#popupContact1').html('');
	$('#popupContact1').load(basePath+'approvePopup.php?ticketNo='+ticketNo,function(){
		
		$('#frmPopUp #butApprAdd').click(function(){
			//addEmailsToGrid(mainRowId,typeRowId,cellId);
			$('#frmPopUp .chkUsers:checked').each(function(){
				var fromId = $(this).val();
				//var obj = $(this);
				var found = false;
				$('#frmComplete #tblMainAPU >tr').each(function(){
					if(fromId ==$(this).attr('id'))
					{
						found = true;
					}	
				});
				if(found==false)
				{
					var chkStatus  = false;
					var chkApprove = false;
					var url = basePath+"tickets-db-set.php?requestType=approveEmailUsers";
					$.ajax({
							url:url,
							async:false,
							dataType:'json',
							type:'post',
							data:'&euserId='+fromId+'&ticketNo='+ticketNo,
							success:function(json)
							{
								if(json.type=='pass')
								{
									chkStatus  		= true;
									intUserId 		= json.userId;
									
									if(intUserId==fromId)
										chkApprove = true;
								}
								else
								{
									$('#frmPopUp #butAdd').validationEngine('showPrompt',json.msg,'fail');
									var t=setTimeout("alertx()",3000);	
									return;
								}
							},
							error:function(xhr,status)
							{
								$('#frmPopUp #butAdd').validationEngine('showPrompt',json.msg,'fail');
								var t=setTimeout("alertx()",3000);	
								return;					
							}
					});
					if(chkStatus)
					{
						document.getElementById('tblMainAPU').insertRow(document.getElementById('tblMainAPU').rows.length);
					document.getElementById('tblMainAPU').rows[document.getElementById('tblMainAPU').rows.length-1].innerHTML = "<td align=\"center\" bgcolor=\"#FFFFFF\">"+(chkApprove?'<img class=\"mouseover removeApprRow\" src=\"images/del.png\" />':'')+"</td>"+
   		 			"<td bgcolor=\"#FFFFFF\" class=\"normalfnt\">"+$(this).parent().parent().find('td').eq(1).html()+"</td>"+
    				"<td bgcolor=\"#FFFFFF\" class=\"normalfntMid\">"+(chkApprove?'<div class=\"button green small\" id=\"butApprove\" name=\"butApprove\">Approve</div>':'Pending')+"</td>"+
					"<td bgcolor=\"#FFFFFF\" class=\"normalfnt\"></td>";
					document.getElementById('tblMainAPU').rows[document.getElementById('tblMainAPU').rows.length-1].id  = fromId;
					setRemoveRow();
					}
				}
			});
			disablePopup();	
		});
		
		$('#frmPopUp #butClose1').click(disablePopup);
		
	});			
}
function removeEUser()
{
	var ticketNo = $('#frmComplete #ticketId').val();
	var url = basePath+"tickets-db-set.php?requestType=removeEUser";
	$.ajax({
			url:url,
			async:false,
			dataType:'json',
			type:'post',
			data:'&euserId='+$(this).parent().parent().attr('id')+'&ticketNo='+ticketNo,
			success:function(json)
			{
				
			},
			error:function(xhr,status)
			{
							
			}
	});	
	$(this).parent().parent().remove();
}
function removeApprEUser()
{
	var ticketNo = $('#frmComplete #ticketId').val();
	var url = basePath+"tickets-db-set.php?requestType=removeApprEUser";
	$.ajax({
			url:url,
			async:false,
			dataType:'json',
			type:'post',
			data:'&euserId='+$(this).parent().parent().attr('id')+'&ticketNo='+ticketNo,
			success:function(json)
			{
				
			},
			error:function(xhr,status)
			{
							
			}
	});	
	$(this).parent().parent().remove();
}
function approveTicket()
{
	var ticketNo 	= $('#frmComplete #ticketId').val();
	var approveUser = $(this).parent().parent().attr('id');
	
	var url = basePath+"tickets-db-set.php?requestType=approveTicket";
	$.ajax({
			url:url,
			async:false,
			dataType:'json',
			type:'post',
			data:'&approveUser='+approveUser+'&ticketNo='+ticketNo,
			success:function(json)
			{
				if(json.type=='pass')
				{
					document.location.reload();
				}
				else
				{
					$(this).validationEngine('showPrompt',json.msg,'fail');
					var t=setTimeout("alertx2()",3000);	
					return;	
				}
			},
			error:function(xhr,status)
			{
							
			}
	});	
}
function setRemoveRow()
{
	$('#frmComplete .removeRow').click(function(){
		 $(this).parent().parent().remove();
	});	
}
function alertx()
{
	$('#frmPopUp #butAdd').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmComplete').validationEngine('hide')	;
}



<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
ini_set('display_errors',0);
$userId				= $_SESSION["userId"];
$ticketNo 			= $_REQUEST['ticketId'];
$txtComment			= $_REQUEST['txtComment'];
$commentId			= $_REQUEST['commentId'];
$comHrsStatus		= 0;

if(!is_dir('presentation/administration/tickets/uploadFileComment/'))
{
	mkdir('presentation/administration/tickets/uploadFileComment/');
}

if($ticketNo!="")
{
	$sql = "SELECT 	 
			dtCreatedDate, 
			dtCreatedBy,
			SA.strUserName addBy, 
			intTrack, 
			strNote, 
			intModuleId,
			(SELECT MM.strName FROM menus MM WHERE MM.intId = t_tickets.intModuleId)AS module, 
			iniProgramId,
			(SELECT MP.strName FROM menus MP WHERE MP.intId = t_tickets.iniProgramId)AS program, 
			intPriority, 
			dblDevelopmentHours, 
			dtmCompleteDate,
			SC.strUserName completeBy, 
			t_tickets.intStatus
			FROM 
			t_tickets 
			LEFT JOIN sys_users SA ON SA.intUserId=t_tickets.dtCreatedBy
			LEFT JOIN sys_users SC ON SC.intUserId=t_tickets.intCompletedBy
			WHERE intTicketId='$ticketNo' ";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$caitStatus  = $row['intStatus'];
		$note		 = $row['strNote'];
		$createUser  = $row['dtCreatedBy'];
		$completeHrs = $row['dblDevelopmentHours'];
		$trackId	 = $row['intTrack'];
		$noteDate    = $row['dtCreatedDate'];
		$addBy		 = $row['addBy'];
		$completeBy	 = $row['completeBy'];
		$completeDt	 = $row['dtmCompleteDate'];
		$module	 	 = $row['module'];
		$program	 = $row['program'];
		if($completeHrs!="")
			$comHrsStatus = 1;
	}
}
if($caitStatus=="0")
{
	$bdColor = "#06C";
	$test  = "Pending";
}
else if($caitStatus=="1")
{
	$bdColor = "#093";
	$test  = "Completed";
}

?>
<title>Ticket # <?php echo $ticketNo; ?> Comments</title>

<form id="frmComplete" name="frmComplete">
<div align="center" >
<div class="trans_layoutD" >
<div class="trans_text">Ticket # <?php echo $ticketNo; ?><input type="hidden" id="ticketId" name="ticketId" value="<?php echo $ticketNo; ?>" /></div>

<table width="100%" border="0" class="tableBorder" style="border-color:<?php echo $bdColor; ?>">
<tr>
	<td align="left" bgcolor="#E4E4E4" class="tableBorder"><div style="color:<?php echo $bdColor; ?>;text-align:center"><b><?php echo $test; ?></b></div>
    </td>
</tr>
<tr>
    <td>
        <table width="100%" border="0" id="tblComplete" style="display:inline">
        <tr>
        	<td width="17%" class="normalfnt">Track :</td>
        	<td width="24%" class="normalfnt"><select name="cboTrack" id="cboTrack" style="width:130px" <?php echo ($caitStatus==1?'disabled="disabled"':'' );?>>
        <option value=""></option>
        <option value="1" <?php echo ($trackId==1?'selected="selected"':'');?>>Bug</option>
        <option value="2" <?php echo ($trackId==2?'selected="selected"':'');?>>Feature</option>
        <option value="3" <?php echo ($trackId==3?'selected="selected"':'');?>>Support</option>
      	</select></td>
        <td width="24%" class="normalfnt">Development Hours :</td>
        <td width="19%" class="normalfnt"><input type="text" name="txtCompleteHrs" id="txtCompleteHrs" style="width:90px" class='validate[required]' <?php echo ($caitStatus==1?'disabled="disabled"':''); ?> value="<?php echo $completeHrs; ?>"/></td>
        <td width="16%" class="normalfnt" style="text-align:right"><div <?php echo ($caitStatus==1?'style="display:none"':'');?> class="button white " id="butSaveHrs" name="butSaveHrs">Save</div>
        </td>
        </tr>
        <tr>
          <td colspan="5" class="normalfnt"></td>
          </tr>
        </table>
    </td>
</tr>
<tr>
<td>
 <?php
 $fileStatus = true;
 	$dir = 'presentation/administration/tickets/uploadFile/'.$ticketNo;
	if(!is_readable($dir))
		$fileStatus = false;
	else if (count(scandir($dir))==2)
		$fileStatus = false;
	
 ?>
 <table class="" width="100%" border="0" cellspacing="0" cellpadding="4">
            <tr>
              <td width="16%" class="normalfnt">&nbsp;Module</td>
              <td width="36%" class="normalfnt">:&nbsp;<?php echo $module; ?></td>
              <td width="17%" class="normalfnt">&nbsp;Program</td>
              <td width="31%" class="normalfnt">:&nbsp;<?php echo $program; ?></td>
            </tr>
            <tr>
              <td class="normalfnt">&nbsp;Add By</td>
              <td class="normalfnt" >:&nbsp;<b><?php echo $addBy; ?></b></td>
              <td class="normalfnt">&nbsp;Add Date</td>
              <td class="normalfnt" >:&nbsp;<b><?php echo $noteDate; ?></b></td>
            </tr>
            <tr>
              <td class="normalfnt">&nbsp;Complete By</td>
              <td class="normalfnt" >:&nbsp;<b><?php echo $completeBy; ?></b></td>
              <td class="normalfnt">&nbsp;Complete Date</td>
              <td class="normalfnt" >:&nbsp;<b><?php echo $completeDt; ?></b></td>
            </tr>
          </table>
          
          </td>
        </tr>
          <tr>
          <td>
<table width='100%' align="center" class="tableBorder_allRound" <?php echo(!$fileStatus?'style="display:none;"':'style="border-color:#b9cf3c;"'); ?> >
	<tr>
        <td width='14%'></td>
        <td width='86%'></td>
        <td width='86%'></td>
	</tr>
	<tr>
		<td colspan="3">
        <div align="center">
<?php
// create directory pointer
 
$dp = opendir('presentation/administration/tickets/uploadFile/'.$ticketNo);
 
// read directory contents
 
// print filenames found
 
while ($file = readdir($dp)) {
 
if ($file != '.' && $file != '..') {
 
$rr = "$file \n";
	$allowedExts = array("jpg", "jpeg", "gif", "png");
	$extension = end(explode(".", $file));

	if (in_array($extension, $allowedExts))
	{
		 echo  "<a href='presentation/administration/tickets/uploadFile/$ticketNo/$rr' target='_blank'><img src='presentation/administration/tickets/uploadFile/$ticketNo/$rr' width='60'></a>&nbsp;";
	}
	else
	{
		 echo "<a href='presentation/administration/tickets/uploadFile/$ticketNo/$rr' target='_blank'><img src='images/com_document.png' ></a>";
	}
}
 
}
 
// destroy directory pointer
 
closedir($dp);
 
?>
        </div>
		</td>
	</tr>
    </table>
<table width='100%' align="center" <?php echo ($caitStatus==1?'style="display:none;"':'');?>>
<tr>
<td width="100%"></td>
</tr>
<tr >
<td id="completeTd" style="text-align:center" ><div style="width:75%" class="button green medium" id="butComplete" name="butComplete">Complete</div></td>
</tr>
</table>
</td>
</tr>
</table>

 <br />
<table id="tblMainAU" width="100%" cellspacing="0" cellpadding="0" class="bordered">           
    <tr>
      <th colspan="4"><div style="float:left;width:95%;text-align:center">Attached Users</div><div style="float:right;width:5%;text-align:right"><img src="images/add_new.png" alt="" name="butInsert" id="butInsert" class="mouseover" /></div></th>
      </tr>
    <tr>
    	<th width="77">Select</th>
    	<th width="148">User</th>
    	<th colspan="2">Email</th>
   	  </tr>
    <?php
	$sql = "SELECT t_addmail_eventusers.intUserId,sys_users.strUserName,sys_users.strEmail
			FROM t_addmail_eventusers
			INNER JOIN sys_users ON sys_users.intUserId=t_addmail_eventusers.intUserId
			where t_addmail_eventusers.intTicketNo='$ticketNo' ";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
	?>
    <tr id="<?php echo $row['intUserId']; ?>">
    	<td align="center" bgcolor="#FFFFFF" ><?php echo($userId==$row['intUserId']?'<img class="mouseover removeRow" src="images/del.png" />':'') ?></td>
        <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $row['strUserName']; ?></td>
        <td colspan="2" bgcolor="#FFFFFF" class="normalfnt"><?php echo $row['strEmail']; ?></td>
    </tr>
    <?php
	}
	?>
</table>

<br />
<table width='100%' class="tableBorder_allRound" style="background-color:#8B5F65">
	<tr class="normalfnt">
        <td width='24%'></td>
        <td width='41%'></td>
        <td width='35%'></td>
	</tr>
	<tr>
        <td class="normalfnt" style="font-size:12px;color:#FFF" >&nbsp;<b>NOTE:</b>&nbsp;</td>
        <td>
        <textarea name='txtNote' id='txtNote' cols='45' rows='5' style='width:450px;height:60px;border-color:#FFF' 
        class='validate[required]' readonly="readonly"><?php echo $note; ?></textarea>
		</td>
        <td width="20%">&nbsp;</td>
	</tr>
    <tr>
        <td width='24%'></td>
        <td width='41%'></td>
        <td width='35%'></td>
	</tr>
	
</table>
<br />
<table id="tblMainAPU" width="100%" cellspacing="0" cellpadding="0" class="bordered">           
    <tr>
      <th colspan="4"><div style="float:left;width:95%;text-align:center">Approval Users</div><div style="float:right;width:5%;text-align:right"><img src="images/add_new.png" alt="" name="butApprInsert" id="butApprInsert" class="mouseover" /></div></th>
      </tr>
    <tr>
    	<th width="81">Select</th>
    	<th width="157">User</th>
    	<th width="102">Approve</th>
    	<th width="240">Date</th>
   	</tr>
    <?php
	$sql = "SELECT TAE.intUserId,SU.strUserName,SU.strEmail,TAE.intStatus,TAE.dtmApproveDate
			FROM t_approvemail_eventusers TAE
			INNER JOIN sys_users SU ON SU.intUserId=TAE.intUserId
			WHERE TAE.intTicketNo='$ticketNo' ";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
	?>
    <tr id="<?php echo $row['intUserId']; ?>">
    	<td align="center" bgcolor="#FFFFFF" ><?php echo($userId==$row['intUserId']?'<img class="mouseover removeRow" src="images/del.png" />':'') ?></td>
        <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $row['strUserName']; ?></td>
        <td bgcolor="#FFFFFF" class="normalfntMid">
        <?php
		if($row['intStatus']==1)
		{
			echo "Approved";
		}
		else if($row['intUserId']==$userId)
		{
        	echo "<div class=\"button green small\" id=\"butApprove\" name=\"butApprove\">Approve</div>";
		}
		else
		{
			echo "Pending";
		}
		?>
        </td>
        <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $row['dtmApproveDate']; ?></td>
    </tr>
    <?php
	}
	?>
</table>
<?php
$sql 	= "SELECT
			sys_users.strUserName,
			t_comments.`comment`,
			t_comments.`id`,
			t_comments.dateTime,
			t_comments.`userId`
			FROM
			t_comments
			Inner Join sys_users ON sys_users.intUserId = t_comments.userId
			WHERE
			t_comments.ticketId =  '$ticketNo'
			ORDER BY
			t_comments.`dateTime` ASC
					";
			$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	if ($createUser==$row['userId'])
	{
		$color1 ="#cfedc9";
		$borderColor = "#6B8E23";
	}
	else
	{
		$color1 ="#d5c9ed";
		$borderColor = '#7171C6';
	}
$newPostDr = $row['id'];
?>
<table width='100%' class="tableBorder_allRound" style="margin-top:10px; background-color:<?php echo $color1; ?>">
	<tr>
        <td width='31%'></td>
        <td width='34%'></td>
        <td width='35%'></td>
	</tr>
	<tr>
        <td class="normalfnt" style="font-size:12px"><B><?php echo $row['strUserName']; ?></B></td>
        <td><textarea name='' id='' cols='45' rows='5' style='width:450px;height:60px;border-color:<?php echo $borderColor; ?>' readonly="readonly"><?php echo $row['comment']; ?></textarea></td>
        <td>
		<div align="center" style="width:40px">
<?php
// create directory pointer
	$postId = $row['id'];
	$dirs = scandir('presentation/administration/tickets/uploadFileComment/'.$ticketNo.'/'.$postId);
	foreach($dirs as $file)
	{
		if (($file == '.')||($file == '..'))
		{
		}
		else
		{
			if (! is_dir('presentation/administration/tickets/uploadFileComment/'.$ticketNo.'/'.$postId.'/'.$file))
			{
				$allowedExts = array("jpg", "jpeg", "gif", "png");
				$extension = end(explode(".", $file));
		
				if (in_array($extension, $allowedExts))
				{
					echo  "<a href=\"presentation/administration/tickets/uploadFileComment/".$ticketNo."/".$postId."/$file\" target=\"_blank\"><img src='presentation/administration/tickets/uploadFileComment/$ticketNo/$postId/$file' width='25'></a>\n";
				}
				else
				{
					echo "<a target=\"_blank\" href=\"presentation/administration/tickets/uploadFileComment/".$ticketNo."/".$postId."/$file\"><img src='images/com_document.png' ></a>";
				}
			}
		}
	}

?>
                      
		</div>
		</td>
	</tr>
	<tr>
        <td>&nbsp;</td>
        <td class="normalfnt"><?php echo $row['dateTime']; ?></td>
        <td>&nbsp;</td>
	</tr>
</table>
<?php 
}
?>
<iframe id="iframeFiles" src="presentation/administration/tickets/comImageUpload.php?ticketId=<?php echo $ticketNo; ?>&comStatus=<?php echo $caitStatus; ?>&comHrsStatus=<?php echo $comHrsStatus; ?>" name="iframeFiles" style="width:100%;height:185px;border:none" ></iframe>

</div>
</div>
</form>

 <div style="width:500px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
 <div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>

<?php
	session_start();
	$backwardseperator 	= "../../../";
	$mainPath 			= $_SESSION['mainPath'];
	$userId 	 		= $_SESSION['userId'];
	$company 			= $_SESSION['CompanyID'];
	include "{$backwardseperator}dataAccess/Connector.php";
	include "{$backwardseperator}libraries/mail/mail.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_mail.php";
	
	$objMail = new cls_create_mail($db);
	
	$ticketId   = $_REQUEST['ticketId'];
	$comStatus  = $_REQUEST['comStatus']; 
	
	$error = "";
	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		$txtComment = $_REQUEST['txtComment'];
		$newComment = str_replace("'","''",$txtComment);
		
		if($_FILES["file"]["name"])
		{
			if ($_FILES["file"]["error"] == 0)
			{
				if($txtComment!="")
				{ 
					$sql = "INSERT INTO t_comments 
							( 
							 ticketId, 
							 DATETIME, 
							 COMMENT, 
							 userId
							)
							VALUES
							( 
							 '$ticketId', 
							 now(), 
							 '$newComment', 
							 '$userId'
							); ";
							$finalResult = $db->RunQuery($sql);
							$comId = $db->insertId;
				
					$commentId = $comId;
					
					$filePath ="uploadFileComment/".$ticketId."/".$commentId."/";
					if(!is_dir("uploadFileComment/".$ticketId."/"))
					{
						mkdir("uploadFileComment/".$ticketId."/");
					}
					if(!is_dir("uploadFileComment/".$ticketId."/".$commentId."/"))
					{
						mkdir("uploadFileComment/".$ticketId."/".$commentId."/");
					}
					move_uploaded_file($_FILES["file"]["tmp_name"],$filePath . $_FILES["file"]["name"]);
				}
				else
				{
					$error = "Please enter a Comment.";
				}
			}
		}
		else
		{
			if($txtComment!="")
			{
				$sql = "INSERT INTO t_comments 
							( 
							 ticketId, 
							 DATETIME, 
							 COMMENT, 
							 userId
							)
							VALUES
							( 
							 '$ticketId', 
							 now(), 
							 '$newComment', 
							 '$userId'
							); ";
							$finalResult = $db->RunQuery($sql);
							$comId = $db->insertId;
						
				$commentId = $comId;
			}
			else
			{
				$error = "Please enter a Comment.";
			}
		}
		if($finalResult)
		{
			$sql = "SELECT DISTINCT userId
					FROM t_comments
					WHERE ticketId=$ticketId
					UNION SELECT DISTINCT intUserId
					FROM sys_mail_eventusers
					WHERE intMailEventId=1011 AND intCompanyId=$company
					UNION SELECT DISTINCT dtCreatedBy
					FROM t_tickets
					WHERE intTicketId=$ticketId
					UNION SELECT DISTINCT intUserId
					FROM
					t_addmail_eventusers
					WHERE
					intMailEventId = 1011 AND
					intTicketNo = $ticketId
					UNION SELECT DISTINCT intUserId
					FROM
					t_approvemail_eventusers
					WHERE
					intMailEventId = 1011 AND
					intTicketNo = $ticketId";
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				if($userId!=$row['userId'])
				{
					$mailSendUserId = $row['userId'];
							
					$sqlAdd = "SELECT sys_users.strFullName
								FROM t_comments
								INNER JOIN sys_users ON sys_users.intUserId=t_comments.userId
								WHERE id=$commentId";
								$resultAdd = $db->RunQuery($sqlAdd);
								$rowAdd = mysqli_fetch_array($resultAdd);
								$addByFullName = $rowAdd['strFullName'];
					
							sendCommentAddMail($ticketId,$objMail,$mainPath,$root_path,$company,$mailSendUserId,$addByFullName);
					
				}
			}
		}
	}	
function sendCommentAddMail($ticketId,$objMail,$mainPath,$root_path,$company,$mailSendUserId,$addByFullName)
{
	global $db;
	$sql = "SELECT strFullName,strUserName,strEmail FROM sys_users WHERE intUserId='$mailSendUserId' ";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$enterUserName  = $row['strFullName'];
		$enterUserEmail = $row['strEmail'];
		
		$header=$addByFullName." commented on ticket #".$ticketId." in ".$_SESSION['projectName']." system.";
		
		$_REQUEST = NULL;
		$_REQUEST['ticketNo'] = $ticketId;
		$_REQUEST['reciver']  = $enterUserName;
		$_REQUEST['sender']   = $addByFullName;
		
		$_REQUEST['link']=base64_encode($mainPath."presentation/administration/tickets/comment-ticket.php?ticketId=$ticketId"); 
		$path=$root_path."presentation/administration/tickets/mailTemplates/mail_addcomment_template.php";
		$objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/new_css.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>
<script type="application/javascript" src="comment-ticket-js.js"></script>

<!--<script src="../../../libraries/jquery/jquery.alerts.js" type="text/javascript"></script>
<link href="../../../libraries/jquery/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />-->
   
<link rel="stylesheet" href="../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../libraries/validate/template.css" type="text/css">
<script type="application/javascript">
function pageSubmit()
{
	document.frmComment.submit();
}
</script>
</head>
<body>
<form method='post' id="frmComment" name="frmComment">
<table width='200' id="loadComment" class="tableBorder_allRound" style="margin-top:10px;background-color:#D8D8CD">
	<tr>
        <td width='21%'></td>
        <td width='44%'></td>
        <td width='35%'></td>
	</tr>
    <tr >
        <td class="normalfnt">Comment: </td>
        <td>
        <textarea name='txtComment' id='txtComment' cols='45' 
        rows='5' style='width:450px;height:60px;border-color:#6E6E6E' class='validate[required]'></textarea></td>
        <td>             
		<div align="center" style="width:40px">     
     </div>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td> 
        <div align='right'><span class="normalfnt" style="color:#F00;text-align:left"><b><?php echo $error; ?></b>&nbsp;&nbsp;</span><input type="file" name="file" id="file"/>
 		<div class="button blue medium mouseover" id="butPost" name="butPost" onclick="pageSubmit();">Post
 		</div></div>
        <div><input style="visibility:hidden" name="ticketId" type="text" id="ticketId" value="<?php echo $ticketId; ?>" /><input style="visibility:hidden" name="comHrsStatus" type="text" id="comHrsStatus" value="<?php echo $comHrsStatus; ?>" /></div>
        </td>
        <td></td>
	</tr>
</table>


</form>
</body>

<?php
if($commentId!="")
{
?>
	<script>
	var ticketId = <?php echo $ticketId; ?>;
	window.top.location.href ='../../../main.php?MenuId=889&ticketId='+ticketId;
	</script>
<?php
}
?>

</html>
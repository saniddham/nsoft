<?php 
	session_start();
	$backwardseperator 	= "../../../";
	$mainPath 			= $_SESSION['mainPath'];
	$userId 	 		= $_SESSION['userId'];
	$requestType 		= $_REQUEST['requestType'];
	$company 			= $_SESSION['CompanyID'];
	include "{$backwardseperator}dataAccess/Connector.php";
	include "{$backwardseperator}libraries/mail/mail.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_mail.php";
	
	$objMail = new cls_create_mail($db);
	
	if($requestType=="saveData")
	{
		$ticketId	= $_REQUEST['ticketId'];
		$trackId	= $_REQUEST['cboTrack'];
		$moduleId	= $_REQUEST['cboModule'];
		$programId	= $_REQUEST['cboProgram'];
		$erroNote	= $_REQUEST['txtErrorNote'];
		$priority	= $_REQUEST['cboPriorty'];
		
		if($ticketId=="")
		{
			$sqlMax = "SELECT 	MAX(intTicketId) maxTicketId FROM t_tickets";
			$resultMax = $db->RunQuery($sqlMax);
			$rowMax = mysqli_fetch_array($resultMax);
			$newTicketId = $rowMax['maxTicketId']+1;
			
			$sql = "INSERT INTO t_tickets 
						(
						 intTicketId,
						 dtCreatedDate, 
						 dtCreatedBy, 
						 intTrack, 
						 strNote, 
						 intModuleId, 
						 iniProgramId, 
						 intPriority
						)
						VALUES
						(
						 '$newTicketId',
						 now(), 
						 '$userId', 
						 '$trackId', 
						 '$erroNote', 
						 '$moduleId', 
						 '$programId', 
						 '$priority'
						); ";
			$result = $db->RunQuery($sql);
			$id = $db->insertId;
			if($result)
			{
				$sql = "SELECT sys_users.strFullName
						FROM t_tickets
						INNER JOIN sys_users ON sys_users.intUserId=t_tickets.dtCreatedBy
						WHERE intTicketId=$id";
				$result = $db->RunQuery($sql);
				$row = mysqli_fetch_array($result);
				$addByFullName = $row['strFullName'];
				
				sendTicketAddMail($id,$objMail,$mainPath,$root_path,$company,$userId,$addByFullName);
				
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Saved successfully';
				$response['ticketId'] 	= $id;
			}
			else
			{
				$response['type'] 		= 'fail';
				$response['msg'] 		= $db->errormsg;
			}
		}
		else
		{
			$sql = "UPDATE t_tickets 
					SET 
					dtCreatedDate = now() , 
					dtCreatedBy = '$userId' , 
					intTrack = '$trackId' , 
					strNote = '$erroNote' , 
					intModuleId = '$moduleId' , 
					iniProgramId = '$programId' , 
					intPriority = '$priority'
					WHERE
					intTicketId = '$ticketId' ;";
			$result = $db->RunQuery($sql);
			if($result)
			{
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Updated successfully';
				$response['ticketId'] 	= $ticketId;
			}
			else
			{
				$response['type'] 		= 'fail';
				$response['msg'] 		= $db->errormsg;
			}		
		}
		echo json_encode($response);
	}
	else if($requestType=="saveComment")
	{
		$txtComment = $_REQUEST['txtComment'];
		$ticketId 	= $_REQUEST['ticketId'];
		
		$sql = "INSERT INTO t_comments 
				( 
				 ticketId, 
				 DATETIME, 
				 COMMENT, 
				 userId
				)
				VALUES
				( 
				 '$ticketId', 
				 now(), 
				 '$txtComment', 
				 '$userId'
				); ";
		$result = $db->RunQuery($sql);
		$comId = $db->insertId;
		if($result)
		{
			$response['type'] 		= 'pass';
			$response['commentId'] 	= $comId;
		}
		echo json_encode($response);
	}
	else if($requestType=="completeTicket")
	{
		$ticketId 	 = $_REQUEST['ticketId'];
		
		$sql = "UPDATE t_tickets 
				SET 
				dtmCompleteDate = now() , 
				intCompletedBy = '$userId' , 
				intStatus = 1
				WHERE
				intTicketId = '$ticketId' ";
		
		$result = $db->RunQuery($sql);
		if($result)
		{
			$sql = "SELECT sys_users.strFullName
					FROM t_tickets
					INNER JOIN sys_users ON sys_users.intUserId=t_tickets.intCompletedBy
					WHERE intTicketId=$ticketId";
			$result = $db->RunQuery($sql);
			$row = mysqli_fetch_array($result);
			$completeByFullName = $row['strFullName'];
			
			completeTicketMail($ticketId,$objMail,$mainPath,$root_path,$company,$userId,$completeByFullName);
			
			$response['type'] 		= 'pass';
		}
		else
		{
			$response['type'] 		= 'fail';
		}
		echo json_encode($response);	
	}
	else if($requestType=="saveCompleteHrs")
	{
		$ticketId 	 = $_REQUEST['ticketId'];
		$completeHrs = ($_REQUEST['completeHrs']==""?'null':$_REQUEST['completeHrs']);
		$trackId 	 = $_REQUEST['trackId'];
		
		$sql = "UPDATE t_tickets 
				SET 
				intTrack = '$trackId' , 
				dblDevelopmentHours = $completeHrs
				WHERE
				intTicketId = '$ticketId' ";
		
		$result = $db->RunQuery($sql);
		if($result)
		{
			$response['type'] 		= 'pass';
		}
		else
		{
			$response['type'] 		= 'fail';
		}
		echo json_encode($response);
	}
	else if($requestType=="additionalEmailUsers")
	{
		$addUserId = $_REQUEST['euserId'];
		$ticketNo  = $_REQUEST['ticketNo'];
		
		$sql = "INSERT INTO t_addmail_eventusers 
				(
				intMailEventId,
				intTicketNo, 
				intUserId, 
				intCompanyId
				)
				VALUES
				(
				'1011',
				'$ticketNo', 
				'$addUserId', 
				'$company'
				) ";
		$result = $db->RunQuery($sql);
		if($result)
		{
			$sqlEmail = "SELECT strFullName
						 FROM sys_users
						 WHERE intUserId='$userId' ";
			
			$resultEmail 	= $db->RunQuery($sqlEmail);
			$rowEmail 		= mysqli_fetch_array($resultEmail);
			$addByFullName  = $rowEmail['strFullName'];
			
			sendAttachedUserMail($ticketNo,$objMail,$mainPath,$root_path,$company,$userId,$addByFullName,$addUserId);
				
			$response['type'] 		= 'pass';
			$response['userId'] 	= $userId;
		}
		else
		{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
		}
		echo json_encode($response);
	}
	else if($requestType=="approveEmailUsers")
	{
		$addUserId = $_REQUEST['euserId'];
		$ticketNo  = $_REQUEST['ticketNo'];
		
		$sql = "INSERT INTO t_approvemail_eventusers 
				(
				intMailEventId,
				intTicketNo, 
				intUserId, 
				intCompanyId,
				intStatus
				)
				VALUES
				(
				'1011',
				'$ticketNo', 
				'$addUserId', 
				'$company',
				0
				) ";
		$result = $db->RunQuery($sql);
		if($result)
		{
			$sqlEmail = "SELECT strFullName
						 FROM sys_users
						 WHERE intUserId='$userId' ";
			
			$resultEmail 	= $db->RunQuery($sqlEmail);
			$rowEmail 		= mysqli_fetch_array($resultEmail);
			$addByFullName  = $rowEmail['strFullName'];
			
			sendApproveUserMail($ticketNo,$objMail,$mainPath,$root_path,$company,$userId,$addByFullName,$addUserId);
				
			$response['type'] 		= 'pass';
			$response['userId'] 	= $userId;
		}
		else
		{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
		}
		echo json_encode($response);
	}
	else if($requestType=="removeEUser")
	{
		$UserId 	= $_REQUEST['euserId'];
		$ticketNo 	= $_REQUEST['ticketNo'];
		
		$sql = "DELETE FROM t_addmail_eventusers 
				WHERE
				intUserId = '$UserId' AND intTicketNo='$ticketNo' ";
		$result = $db->RunQuery($sql);
	}
	else if($requestType=="removeApprEUser")
	{
		$UserId 	= $_REQUEST['euserId'];
		$ticketNo 	= $_REQUEST['ticketNo'];
		
		$sql = "DELETE FROM t_approvemail_eventusers 
				WHERE
				intUserId = '$UserId' AND intTicketNo='$ticketNo' ";
		$result = $db->RunQuery($sql);
	}
	else if($requestType=='approveTicket')
	{
		$approveUser = $_REQUEST['approveUser'];
		$ticketNo 	 = $_REQUEST['ticketNo'];
		
		$sql = "UPDATE t_approvemail_eventusers 
				SET
				intStatus = 1 , 
				dtmApproveDate = now()
				WHERE
				intTicketNo = '$ticketNo' AND intUserId='$approveUser' AND intCompanyId = '$company' ";
		$result = $db->RunQuery($sql);
		if($result)
		{
			$response['type'] 		= 'pass';
		}
		else
		{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
		}
		echo json_encode($response);	
	}
function sendTicketAddMail($id,$objMail,$mainPath,$root_path,$company,$userId,$addByFullName)
{
	global $db;
	$sql = "SELECT sys_mail_eventusers.intUserId AS userId,
			sys_users.strFullName,
			sys_users.strEmail
			FROM sys_mail_eventusers
			INNER JOIN sys_users ON sys_users.intUserId=sys_mail_eventusers.intUserId
			WHERE intMailEventId='1011' AND intCompanyId='$company'";
	
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		if($row['userId']!=$userId)
		{
			$enterUserName  = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			
			$header=$addByFullName." add a ticket to the ".$_SESSION['projectName']." system.";
			
			$_REQUEST = NULL;
			$_REQUEST['ticketNo'] = $id;
			$_REQUEST['reciver']  = $enterUserName;
			$_REQUEST['sender']   = $addByFullName;
			
			$_REQUEST['link']=base64_encode($mainPath."presentation/administration/tickets/comment-ticket.php?ticketId=$id"); 
			$path=$root_path."presentation/administration/tickets/mailTemplates/mail_addticket_template.php";
			$objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
		}
	}
}
function completeTicketMail($ticketId,$objMail,$mainPath,$root_path,$company,$userId,$completeByFullName)
{
	global $db;
	$sqlcheck = "SELECT DISTINCT dtCreatedBy
					FROM t_tickets
					WHERE intTicketId='$ticketId'
					UNION SELECT DISTINCT intUserId
					FROM sys_mail_eventusers
					WHERE intMailEventId=1011 AND intCompanyId='$company'
					UNION SELECT DISTINCT intUserId
					FROM
					t_addmail_eventusers
					WHERE
					intMailEventId = 1011 AND
					intTicketNo = $ticketId
					UNION SELECT DISTINCT intUserId
					FROM
					t_approvemail_eventusers
					WHERE
					intMailEventId = 1011 AND
					intTicketNo = $ticketId ";
	$resultCheck = $db->RunQuery($sqlcheck);
	while($rowCheck = mysqli_fetch_array($resultCheck))
	{
		if($rowCheck['dtCreatedBy']!=$userId)
		{
			$sql = "SELECT strFullName,strUserName,strEmail FROM sys_users WHERE intUserId='".$rowCheck['dtCreatedBy']."' ";
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				$enterUserName  = $row['strFullName'];
				$enterUserEmail = $row['strEmail'];
				
				$header=$completeByFullName." completed the ticket #".$ticketId." in ".$_SESSION['projectName']." system.";
				
				$_REQUEST = NULL;
				$_REQUEST['ticketNo'] = $ticketId;
				$_REQUEST['reciver']  = $enterUserName;
				$_REQUEST['sender']   = $completeByFullName;
				
				$_REQUEST['link']=base64_encode($mainPath."presentation/administration/tickets/comment-ticket.php?ticketId=$ticketId"); 
				$path=$root_path."presentation/administration/tickets/mailTemplates/mail_completeticket_template.php";
				$objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
				
			}
		}
	}
}
function sendAttachedUserMail($ticketNo,$objMail,$mainPath,$root_path,$company,$userId,$addByFullName,$attachedUID)
{
	global $db;
	$sql = "SELECT TAE.intUserId AS userId,
			sys_users.strFullName,
			sys_users.strEmail
			FROM t_addmail_eventusers TAE
			INNER JOIN sys_users ON sys_users.intUserId=TAE.intUserId
			WHERE TAE.intMailEventId='1011' AND 
			TAE.intCompanyId='$company' AND 
			TAE.intTicketNo='$ticketNo' AND 
			TAE.intUserId='$attachedUID' ";
	
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$enterUserName  = $row['strFullName'];
		$enterUserEmail = $row['strEmail'];
		
		$header=$addByFullName." attached ".$enterUserName." to a ticket in ".$_SESSION['projectName']." system.";
		
		$_REQUEST = NULL;
		$_REQUEST['ticketNo'] = $ticketNo;
		$_REQUEST['reciver']  = $enterUserName;
		$_REQUEST['sender']   = $addByFullName;
		
		$_REQUEST['link']=base64_encode($mainPath."presentation/administration/tickets/comment-ticket.php?ticketId=$ticketNo"); 
		$path=$root_path."presentation/administration/tickets/mailTemplates/mail_attacheduser_template.php";
		$objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
		
	}
}
function sendApproveUserMail($ticketNo,$objMail,$mainPath,$root_path,$company,$userId,$addByFullName,$attachedUID)
{
	global $db;
	$sql = "SELECT TAE.intUserId AS userId,
			sys_users.strFullName,
			sys_users.strEmail
			FROM t_approvemail_eventusers TAE
			INNER JOIN sys_users ON sys_users.intUserId=TAE.intUserId
			WHERE TAE.intMailEventId='1011' AND 
			TAE.intCompanyId='$company' AND 
			TAE.intTicketNo='$ticketNo' AND 
			TAE.intUserId='$attachedUID' ";
	
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$enterUserName  = $row['strFullName'];
		$enterUserEmail = $row['strEmail'];
		
		$header=$addByFullName." added ".$enterUserName." to approve a ticket in ".$_SESSION['projectName']." system.";
		
		$_REQUEST = NULL;
		$_REQUEST['ticketNo'] = $ticketNo;
		$_REQUEST['reciver']  = $enterUserName;
		$_REQUEST['sender']   = $addByFullName;
		
		$_REQUEST['link']=base64_encode($mainPath."presentation/administration/tickets/comment-ticket.php?ticketId=$ticketNo"); 
		$path=$root_path."presentation/administration/tickets/mailTemplates/mail_approveuser_template.php";
		$objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
		
	}
}

?>
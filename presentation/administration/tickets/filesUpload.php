<?php
session_start();
$folderName 		= 	$_REQUEST['txtFolder'];
$comStatus			=   $_REQUEST['comStatus'];
$createBy			=	$_REQUEST['createBy'];
$userId				=   $_SESSION["userId"];
$filePath			='uploadFile/'.$folderName.'/';

if(!is_dir('uploadFile/'.$folderName.'/'))
{
	mkdir('uploadFile/'.$folderName.'/',0700);
}
if ($_FILES["file"]["error"] == 0)
{ 
	move_uploaded_file($_FILES["file"]["tmp_name"],$filePath . $_FILES["file"]["name"]);
}
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Upload Files</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form id="frmUpload" name="frmUpload" method="post" action="filesUpload.php" enctype="multipart/form-data">

<table class="bordered" width="355" height="48" >
<thead>
  <tr class="normalfnt">
    <th colspan="2" bgcolor="" class="normalfntMid">Upload Documents</th>
    </tr>
  <tr class="normalfnt">
    <th width="67" bgcolor="" class="normalfntMid">No</th>
    <th width="312" bgcolor="" class="normalfntMid">File Name</th>
    </tr>
</thead>
<tbody>
  <?php
  	//echo "../../../../../documents/sampleinfo/docs/$folderName";
	filesInDir($filePath);
	
				function filesInDir($tdir)
				{
					global $folderName;
					$m=0;
					$dirs = scandir($tdir);
					foreach($dirs as $file)
					{
						
						if (($file == '.')||($file == '..'))
						{
						}
						else
						{
							
							if (! is_dir($tdir.'/'.$file))
							{
								
							echo "	<tr bgcolor=\"#ffffff\" class=\"normalfnt\">
										<td>".++$m."</td>
										<td><a target=\"_blank\" href=\"uploadFile/".$folderName."/$file\">$file</a></td>
								 	 </tr>";
							}
						}
					}
				}
  ?>
  
  <tr>
    <td colspan="2" bgcolor="#FFFFFF"><input type="file" name="file" id="file" />
      <input <?php echo ($comStatus==1 || $createBy!=$userId?'style="display:none"':'');?> type="submit" name="button" id="button" value="Upload" /></td>
    </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF"><input style="visibility:hidden;width:10px" name="txtFolder" type="text" id="txtFolder" value="<?php echo $folderName; ?>" /><input style="visibility:hidden;width:10px" name="comStatus" type="text" id="comStatus" value="<?php echo $comStatus; ?>" /><input style="visibility:hidden;width:10px" name="createBy" type="text" id="createBy" value="<?php echo $createBy; ?>" /></td>
    </tr>
  </tbody>
</table>
</form>
</body>
</html>
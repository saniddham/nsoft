<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "class/tables/sys_cronjob.php";						$sys_cronjob 		= new sys_cronjob($db);
include_once "class/tables/sys_users.php";							$sys_users 			= new sys_users($db);
?>

<title>Daily Mail Configeration</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />

<form id="frmDailyMailConfig" name="frmDailyMailConfig" method="post" autocomplete="off">
<div align="center">
    <div class="trans_layoutL">
        <div class="trans_text">Daily Mail Configeration</div>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr>
        	<td>
            	<table width="100%" border="0" align="center">
                	<tr class="normalfnt">
                    	<td width="2%">&nbsp;</td>
                    	<td width="12%">Report Type</td>
                        <td width="86%"><select  <?Php if(!($form_permision['edit'] || $form_permision['delete'])){echo 'disabled';}  ?> name="cboReportType" class="txtbox" id="cboReportType" style="width:400px" tabindex="1">
						<?php 
                       		echo $sys_cronjob->getCombo();	
                        ?>
                        </select>
                        </td>
                    </tr>
                    <tr class="normalfnt">
                    	<td width="2%"></td>
                    	<td width="12%">&nbsp;</td>
                        <td width="86%">&nbsp;</td>
                    </tr>
                    <tr class="normalfnt">
                    	<td width="2%"></td>
                    	<td width="12%">TO Users</td>
                        <td width="86%"><select name="cboTOUsers" id="cboTOUsers" multiple="multiple" class="chosen-select" style="width:700px" >
                        <?php 
                       		echo $sys_users->getCombo(NULL,'intStatus=1');	
                        ?>
                        </select></td>
                    </tr>
                    <tr class="normalfnt">
                    	<td width="2%"></td>
                    	<td width="12%">CC Users</td>
                        <td width="86%"><select name="cboCCUsers" id="cboCCUsers" multiple="multiple" class="chosen-select" style="width:700px" >
                          <?php 
                       		echo $sys_users->getCombo(NULL,'intStatus=1');	
                        ?>
                        </select></td>
                    </tr>
                    <tr class="normalfnt">
                    	<td width="2%"></td>
                    	<td width="12%">BCC Users</td>
                        <td width="86%"><select name="cboBCCUsers" id="cboBCCUsers" multiple="multiple" class="chosen-select" style="width:700px" >
                          <?php 
                       		echo $sys_users->getCombo(NULL,'intStatus=1');	
                        ?>
                        </select></td>
                    </tr>
                    <tr class="normalfnt">
                    	<td width="2%"></td>
                    	<td width="12%">&nbsp;</td>
                        <td width="86%">&nbsp;</td>
                    </tr>
                </table>  
            </td>
        </tr>
        <tr>
        	<td>
                <table width="100%" border="0" class="bcgl2">
                    <tr>
                    	<td width="100%" align="center" bgcolor=""><a class="button white medium" id="butNew" name="butNew">New</a><?php echo($form_permision['edit']==1?'<a class="button white medium" id="butSave" name="butSave">Save</a>':'');?><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
    </div>
</div>
</form>
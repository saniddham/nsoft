
$(document).ready(function(){
	
	$(".chosen-select").chosen({allow_single_deselect: true , no_results_text: "Oops, nothing found!"});
	
	$("#frmDailyMailConfig #cboReportType").die('change').live('change',loadData);
	
});

function loadData()
{
	var reportType	= $("#frmDailyMailConfig #cboReportType").val();
	
	if(reportType=='')
	{
		clearSearch();
		return;
	}
	
	showWaiting();
	var url 	= "controller.php?q=1122&requestType=loadData";
	var data 	= "reportType="+reportType;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmMainCluster #txtClusterCode').val(json.clusterCode);
				$('#frmMainCluster #txtClusterName').val(json.clusterName);
				$('#frmMainCluster #cboWeeklyHoliday').val(json.weeklyHolydy);
				$('#frmMainCluster #strRemark').val(json.remarks);
				$('#frmMainCluster #chkActive').attr('checked',(json.status==1?true:false));
				hideWaiting();
			}
	});
	hideWaiting();	
}
function clearSearch()
{
	$(".chosen-select").val('').trigger('chosen:updated');
}
<?php
session_start();
 $companyId = $_SESSION['CompanyID'];
$thisFilePath =  $_SERVER['PHP_SELF'];

 $sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}
if($_SERVER['REQUEST_METHOD'] == "POST")
{
	$optionValue = trim($_REQUEST['cboSearchOption']);
	$searchValue = trim($_REQUEST['txtSearchValue']);
}
?>
 <title>Locations Listing</title>
 
<script type="text/javascript" language="javascript">
function pageSubmit()
{
	document.getElementById('frmLocationsListing').submit();	
}
function clearValue()
{
	document.getElementById('txtSearchValue').value = '';
	document.getElementById('txtSearchValue').focus();
}
</script>

</head>

<body>
<form id="frmLocationsListing" name="frmLocationsListing" method="post" autocomplete="off" action="">
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Locations Listing</div>
		  <table width="800" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="800" border="0">
      <tr>
        <td><table width="800" border="0" cellpadding="1" cellspacing="0" class="tableBorder_allRound">
          <tr>
            <td width="16%" height="22" class="normalfnt">&nbsp;</td>
            <td width="18%" class="normalfntMid">Searching Option</td>
            <td width="17%" class="normalfnt">
            <select name="cboSearchOption" id="cboSearchOption" onchange="clearValue();" style="width:150px">
              <option value="">&nbsp;</option>
<option value="mst_locations.strCode" <?php echo($optionValue=='mst_locations.strCode'?'selected':'') ?> >Code</option>
<option value="mst_locations.strName" <?php echo($optionValue=='mst_locations.strName'?'selected':'') ?>>Name</option>
            </select></td>
            <td width="16%"><input value="<?php echo $searchValue; ?>" type="text" name="txtSearchValue" id="txtSearchValue" /></td>
            <td width="18%" class="normalfnt">
            <a href="#">
            <img src="images/search.png" width="20" height="20" onclick="pageSubmit();" /></a></td>
            <td width="15%">&nbsp;</td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td>
          <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
          <div id="_head_wrap" style="width: 800px; overflow: hidden;">
          <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
          <thead>
            <tr class="gridHeader">
              <th height="22" style="width: 50px;" ><strong>Id</strong></th>
              <th style="width: 100px;" ><strong>Code</strong></th>
              <th style="width:400px;" ><strong>Name</strong></th>
              <th style="width: 100px;" ><strong>Active</strong></th>
              <th style="width: 100px;" >View</th>
              <th style="width: 50px;" ></th>
              </tr>
           </table>
           </div>
           <div class="normalfnt" id="_body_wrap" style="width: 800px; height: 310px; overflow: scroll;">
           <table class="tbl1 scroll" > 
           <tbody>
              <?php

				if($searchValue!='')
			  	$wherePart = "WHERE $optionValue like '%$searchValue%'";
				
	 	 		 $sql = "SELECT
						mst_locations.intId,
						mst_locations.strCode,
						mst_locations.strName,
						mst_locations.intStatus
						FROM
						mst_locations
						$wherePart
						";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$id 		=	 $row['intId'];
	  			 ?>
           		<tr class="normalfnt">
           		<td class="id" height="16" align="center" bgcolor="#FFFFFF">
		   		<?php echo $row['intId'];?></td>
              	<td class="code"  align="center" bgcolor="#FFFFFF"><?php echo $row['strCode'];?></td>
              	<td class="name" bgcolor="#FFFFFF"><?php echo $row['strName'];?></td>
              	<td class="active" align="center" bgcolor="#FFFFFF">
              	  <input disabled="disabled" <?php echo($row['intStatus']?'checked':''); ?> type="checkbox"/></td>
              	<td class="more" align="center" bgcolor="#FFFFFF"><a target="_blank" href="<?php echo "main.php?MenuId=200&id=$id";?>">More</a></td>
              </tr>
               <?php 
        	    } 
       		   ?>
              </tbody>
             </table>
            </div></div>
              </td>
              </tr>
            </table>
			</td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>

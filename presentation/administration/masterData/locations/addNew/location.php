<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "class/tables/mst_locations.php";			$mst_locations 		= new mst_locations($db);
include_once "class/tables/mst_plant.php";				$mst_plant 			= new mst_plant($db);
include_once "class/tables/mst_companies.php";			$mst_companies 		= new mst_companies($db);
include_once "class/finance/cls_get_gldetails.php";		$obj_GLDetails_get 	= new Cls_Get_GLDetails($db);
?>
<title>Locations</title>
<form id="frmLocations" name="frmLocations" method="post" autocomplete="off">
<div align="center">
	<div class="trans_layoutD">
	<div class="trans_text">Locations</div>
    <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr>
            <td>
            	<table width="100%" border="0" align="center">
                	<tr class="normalfnt">
                	  <td colspan="3"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableBorder_allRound" >
                          <tr class="normalfnt">
                            <td height="45" width="24%">&nbsp;Search</td>
                            <td width="68%"><select  <?Php if(!($form_permision['edit'] || $form_permision['delete'])){echo 'disabled';}  ?> name="intId" class="txtbox" id="intId" style="width:101%">
                              <?php 
                            echo $mst_locations->getCombo();
                        ?>
                            </select></td>
                            <td width="8%">&nbsp;</td>
                          </tr>
                        </table></td>
              	  </tr>
                	<tr class="normalfnt">
                    	<td width="24%">&nbsp;</td>
                     	<td width="68%">&nbsp;</td>
                     	<td width="8%">&nbsp;</td>
                    </tr>
                    <tr class="normalfnt">
                      <td>&nbsp;Location Code&nbsp;<span class="compulsoryRed">*</span></td>
                      <td><input name="strCode" type="text" class="txtbox" id="strCode" style="width:140px" /></td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr class="normalfnt">
                      <td>&nbsp;Location Name&nbsp;<span class="compulsoryRed">*</span></td>
                      <td><input name="strName" type="text" class="txtbox" id="strName" style="width:100%" /></td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr class="normalfnt">
                      <td>&nbsp;Company <span class="compulsoryRed">*</span></td>
                      <td><select name="intCompanyId" id="intCompanyId" style="width:101%" >
                        <?php
					echo $mst_companies->getCombo();
				 ?>
                      </select></td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr class="normalfnt">
                      <td>&nbsp;Address</td>
                      <td><textarea name="strAddress" style="width:100%"  rows="2" class="txtbox" id="strAddress"></textarea></td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr class="normalfnt">
                      <td>&nbsp;Street</td>
                      <td><input name="strStreet" type="text" class="txtbox" id="strStreet" style="width:100%" /></td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr class="normalfnt">
                      <td>&nbsp;City</td>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr class="normalfnt">
                          <td width="43%"><input name="strCity" type="text" class="txtbox" id="strCity" style="width:140px"/></td>
                          <td width="17%">District</td>
                          <td width="40%" style="text-align:right"><input name="strDistrict" type="text" class="txtbox" id="strDistrict" style="width:100%"/></td>
                        </tr>
                      </table></td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr class="normalfnt">
                      <td>&nbsp;Phone</td>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr class="normalfnt">
                          <td width="43%"><input name="strPhoneNo" type="text" class="txtbox" id="strPhoneNo" style="width:140px" /></td>
                          <td width="17%">Fax</td>
                          <td width="40%" style="text-align:right"><input name="strFaxNo" type="text" class="txtbox" id="strFaxNo" style="width:100%"/></td>
                        </tr>
                      </table></td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr class="normalfnt">
                      <td>&nbsp;E-Mail</td>
                      <td><input name="strEmail" type="text" class="txtbox" id="strEmail" style="width:100%"/></td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr class="normalfnt">
                    	<td>&nbsp;Remarks</td>
                        <td><textarea name="strRemarks" style="width:100%"  rows="2" class="txtbox" id="strRemarks"></textarea></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="normalfnt">
                      <td>&nbsp;Plant</td>
                      <td><select name="intPlant" id="intPlant" style="width:101%" >
                        <?php
					echo $mst_plant->getCombo();
				 ?>
                      </select></td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr class="normalfnt">
                      <td>&nbsp;Bank Account</td>
                      <td><select id="BANK_ACCOUNT_ID" name="BANK_ACCOUNT_ID" style="width:101%;">
                        <?php   
				  echo $obj_GLDetails_get->getBankGLCombo('');
				  ?>
                      </select></td>
                      <td>&nbsp;</td>
                    </tr>

                    <tr class="normalfnt">
                        <td>&nbsp;Production</td>
                        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr class="normalfnt">
                                    <td width="43%"><input type="checkbox" name="intProduction" id="intProduction" style="vertical-align: middle"/></td>
                                    <td width="47%">Sticker making location</td>
                                    <td width="10%" style="text-align:right"><input type="checkbox" name="RESTRICTIONS" id="RESTRICTIONS" style="center-align: middle"/></td>
                                </tr>
                            </table></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="normalfnt">
                      <td>&nbsp;Active</td>
                      <td><input type="checkbox" name="intStatus" id="intStatus" checked="checked" style="vertical-align: middle"/></td>
                      <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="34"><table width="100%" border="0">
              <tr>
                <td width="24%">&nbsp;</td>
                <td width="68%">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                    <tr>
                        <td width="100%" align="center" bgcolor=""><a class="button white medium" id="butNew" name="butNew" style="display:" tabindex="28">New</a><?php if($form_permision['add']||$form_permision['edit']){?><a class="button white medium" id="butSave" name="butSave" style="display:" tabindex="24">Save</a><?php } if($form_permision['delete']){?><a class="button white medium" id="butDelete" name="butDelete" style="display:" tabindex="25">Delete</a><?Php }?><a href="main.php" class="button white medium" id="butClose" name="butClose" style="display:" tabindex="27">Close</a></td>
                    </tr>
                </table>
                </td>
                <td width="8%">&nbsp;</td>
              </tr>
            </table>
                
            </td>
        </tr>
    </table>
	</div>
 	</div>
</form>
$(function(){
	

	if(cId != ''){
	$('#frmLocations #cboSearch').val(cId);
		loadData();
 	}
	$("#frmLocations").validationEngine();
	$('#frmLocations #txtCode').focus();


  ///save button click event
  $('#frmLocations #butSave').click(function(){

	var requestType = '';
	if ($('#frmLocations').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmLocations #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = "presentation/administration/masterData/locations/addNew/location-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			data:$("#frmLocations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmLocations #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmLocations').get(0).reset();
						loadCombo_frmLocations();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmLocations #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load location details //////////////////////////
   /////////////////////////////////////////////////////
/*   $('#frmLocations #cboSearch').on('click',function(){
	   $('#frmLocations').validationEngine('hide');
   });*/
    $('#frmLocations #cboSearch').change(function(){
  		$('#frmLocations').validationEngine('hide');
		var url = "presentation/administration/masterData/locations/addNew/location-db-get.php";
		if($('#frmLocations #cboSearch').val()=='')
		{
			$('#frmLocations').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmLocations #txtCode').val(json.code);
					$('#frmLocations #txtName').val(json.name);
					$('#frmLocations #cboPlant').val(json.plant);
					$('#frmLocations #cboCompany').val(json.company);
					$('#frmLocations #txtAddress').val(json.address);
					$('#frmLocations #txtStreet').val(json.street);
					$('#frmLocations #txtCity').val(json.city);
					$('#frmLocations #txtDistrict').val(json.district);
					$('#frmLocations #txtPhone').val(json.phone);
					$('#frmLocations #txtFax').val(json.fax);
					$('#frmLocations #txtEMail').val(json.email);
					$('#frmLocations #txtRemark').val(json.remark);
					$('#frmLocations #cboBankAccount').val(json.bankAcc);
					if(json.status)
						$('#frmLocations #chkActive').attr('checked',true);
					else
						$('#frmLocations #chkActive').removeAttr('checked');
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmLocations #butNew').click(function(){
		$('#frmLocations').get(0).reset();
		loadCombo_frmLocations();
		$('#frmLocations #txtCode').focus();
	});
    $('#frmLocations #butDelete').click(function(){
		if($('#frmLocations #cboSearch').val()=='')
		{
			$('#frmLocations #butDelete').validationEngine('showPrompt', 'Please select Location.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmLocations #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "presentation/administration/masterData/locations/addNew/location-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmLocations #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmLocations #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmLocations').get(0).reset();
													loadCombo_frmLocations();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});


function loadCombo_frmLocations()
{
	var url 	= "presentation/administration/masterData/locations/addNew/location-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmLocations #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmLocations #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmLocations #butDelete').validationEngine('hide')	;
}

function loadData(){
	
	var loc	= $('#frmLocations #cboSearch').val();
 	var url = "presentation/administration/masterData/locations/addNew/location-db-get.php";

	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:'requestType=loadDetails&id='+loc,
		async:false,
		success:function(json){
				//json  = eval('('+json+')');
				$('#frmLocations #txtCode').val(json.code);
				$('#frmLocations #txtName').val(json.name);
				$('#frmLocations #cboPlant').val(json.plant);
				$('#frmLocations #cboCompany').val(json.company);
				$('#frmLocations #txtAddress').val(json.address);
				$('#frmLocations #txtStreet').val(json.street);
				$('#frmLocations #txtCity').val(json.city);
				$('#frmLocations #txtDistrict').val(json.district);
				$('#frmLocations #txtPhone').val(json.phone);
				$('#frmLocations #txtFax').val(json.fax);
				$('#frmLocations #txtEMail').val(json.email);
				$('#frmLocations #txtRemark').val(json.remark);
				$('#frmLocations #cboBankAccount').val(json.bankAcc);
				if(json.status)
					$('#frmLocations #chkActive').attr('checked',true);
				else
					$('#frmLocations #chkActive').removeAttr('checked');
		}
	});
	
}

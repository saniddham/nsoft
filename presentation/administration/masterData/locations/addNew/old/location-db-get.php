<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	
	/////////// location load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
					intId,
					strName
				FROM mst_locations
				order by strName
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		$sql = "SELECT
					strCode,
					strName,
					intPlant,
					intCompanyId,
					strAddress,
					strStreet,
					strCity,
					strDistrict,
					strPhoneNo,
					strFaxNo,
					strEmail,
					strRemarks,
					BANK_ACCOUNT_ID,
					intStatus
				FROM mst_locations
				WHERE
					intId =  '$id'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['code'] 	= $row['strCode'];
			$response['name'] 	= $row['strName'];
			$response['plant']	= $row['intPlant'];
			$response['company'] = $row['intCompanyId'];
			$response['address'] = $row['strAddress'];
			$response['street'] = $row['strStreet'];
			$response['city'] = $row['strCity'];
			$response['district'] = $row['strDistrict'];
			$response['phone'] = $row['strPhoneNo'];
			$response['fax'] = $row['strFaxNo'];
			$response['email'] = $row['strEmail'];
			$response['remark'] = $row['strRemarks'];
			$response['bankAcc']= $row['BANK_ACCOUNT_ID'];
			$response['status'] = ($row['intStatus']?true:false);
		}
		echo json_encode($response);
	}
	
?>
<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$database	= $_SESSION['Database'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];
	$code			= trim($_REQUEST['txtCode']);
	$name			= trim($_REQUEST['txtName']);
	$plant			= $_REQUEST['cboPlant'];
	$company 		= $_REQUEST['cboCompany'];
	$bankAcc		= $_REQUEST['cboBankAccount'];
	$address		= $_REQUEST['txtAddress'];
	$street			= $_REQUEST['txtStreet'];
	$city			= trim($_REQUEST['txtCity']);
	$district		= $_REQUEST['txtDistrict'];
	$phone			= $_REQUEST['txtPhone'];
	$fax			= $_REQUEST['txtFax'];
	$email			= trim($_REQUEST['txtEMail']);
	$remark			= trim($_REQUEST['txtRemark']);
	$intStatus		= ($_REQUEST['chkActive']?1:0);	
	//die($plant);
	/////////// location insert part /////////////////////
	if($requestType=='add')
	{
		$sql = "INSERT INTO `mst_locations` 					(`strCode`,`strName`,`intPlant`,`intCompanyId`,`strAddress`,`strStreet`,`strCity`,`strDistrict`,`strPhoneNo`,`strFaxNo`,`strEmail`,`strRemarks`,`BANK_ACCOUNT_ID`,`intStatus`,`intCreator`,`dtmCreateDate`) 
				VALUES ('$code','$name','$plant','$company','$address','$street','$city','$district','$phone','$fax','$email','$remark','$bankAcc','$intStatus','$userId',now())";	
		//die($sql);		
		$result 	= $db->RunQuery($sql);
		/*if($result)
			echo 'true';
		else
			echo 'fale';*/
		$locationId	= $db->insertId;
		//die($sql.'ok'.$db->insertId);
		$dataFeilds	= '';
		$dataValues	= $locationId.',';
		
		$sqlfeild	= "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '$database' AND TABLE_NAME = 'sys_no' ";
		//echo $sqlfeild;
		$resultFeilds = $db->RunQuery($sqlfeild);
		while($rowFeilds = mysqli_fetch_array($resultFeilds))
		{
			if($dataFeilds!='')
				$dataValues .= $locationId.'00000,';
			
			$dataFeilds .= $rowFeilds['COLUMN_NAME'].',';
			
		}
		$dataFeilds	= substr($dataFeilds,0,strlen($dataFeilds)-1);
		$dataValues	= substr($dataValues,0,strlen($dataValues)-1);
		
		$sqlIns	= "INSERT INTO sys_no
					(
					$dataFeilds	
					)
					VALUES
					(
					$dataValues
					)";
		//echo $sqlIns;
		$result 	= $db->RunQuery($sqlIns);
		
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	/////////// location update part /////////////////////
	else if($requestType=='edit')
	{
		$sql = "UPDATE `mst_locations` SET 	strCode				= '$code',
											strName				= '$name',
											intPlant			= '$plant',
											intCompanyId		= '$company',
											strAddress			= '$address',
											strStreet			= '$street',
											strCity				= '$city',
											strDistrict			= '$district',
											strPhoneNo			= '$phone',
											strFaxNo			= '$fax',
											strEmail			= '$email',
											strRemarks			= '$remark',
											BANK_ACCOUNT_ID		= '$bankAcc',
											intStatus			= '$intStatus',
											intModifyer			= '$userId'
				WHERE (`intId`='$id')";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// location delete part /////////////////////
	else if($requestType=='delete')
	{
		$sql = "DELETE FROM `mst_locations` WHERE (`intId`='$id')  ";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
?>
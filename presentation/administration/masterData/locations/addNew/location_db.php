<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
try
{
	$jquery->setForm('frmLocations');
	include_once "class/tables/mst_locations.php";			$mst_locations 		= new mst_locations($db);
	include_once "class/tables/sys_no.php";					$sys_no 			= new sys_no($db);
	include_once "class/dateTime.php";						$dateTimes 			= new dateTimes($db);
	
	## config
	$jquery->setComboId('intId');
	
	function saveLocation()
	{
		//set global
		global $db;
		global $jquery;
		global $mst_locations;
		global $dateTimes;
		global $sessions;
		global $sys_no;
		
		##start connection
		$db->connect();$db->begin();
		
		##function register
		$jquery->regEvent('butSave','click',__FUNCTION__,true);
		
		##jquery validate
		$jquery->get('strCode')->validate('required,maxSize[10]');
		$jquery->get('strName')->validate('required');
		$jquery->get('intCompanyId')->validate('required');
		$jquery->get('strEmail')->validate('custom[email]');
		
		$jquery->validate();
		
		##show waiting window
		$jquery->showWait();
		$locationId = $jquery->get('intId')->null();
		
		$mst_locations->setintId($locationId);
		$mst_locations->setstrCode($jquery->get('strCode')->val());
		$mst_locations->setstrName($jquery->get('strName')->val());
		$mst_locations->setintPlant($jquery->get('intPlant')->null());
		$mst_locations->setintCompanyId($jquery->get('intCompanyId')->val());
		$mst_locations->setstrAddress($jquery->get('strAddress')->val());
		$mst_locations->setstrStreet($jquery->get('strStreet')->val());
		$mst_locations->setstrCity($jquery->get('strCity')->val());
		$mst_locations->setstrDistrict($jquery->get('strDistrict')->val());
		$mst_locations->setstrPhoneNo($jquery->get('strPhoneNo')->val());
		$mst_locations->setstrFaxNo($jquery->get('strFaxNo')->val());
		$mst_locations->setstrEmail($jquery->get('strEmail')->val());
		$mst_locations->setstrRemarks($jquery->get('strRemarks')->val());
		$mst_locations->setBANK_ACCOUNT_ID($jquery->get('BANK_ACCOUNT_ID')->null());
		$mst_locations->setintStatus($jquery->get('intStatus')->val());
		$mst_locations->setintProduction($jquery->get('intProduction')->val());
        $mst_locations->setRESTRICTIONS($jquery->get('RESTRICTIONS')->val());
		$mst_locations->setintCreator($sessions->getUserId());
		$mst_locations->setdtmCreateDate($dateTimes->getCurruntDateTime());
		$finance_sysNo = $mst_locations->getFinanceSysNo($locationId);
		if($jquery->get('intStatus')->val() == 1 && $jquery->get('intCompanyId')->val() == 1 && $finance_sysNo == null){
		    $mst_locations->insertFinanceModuleSysNo($locationId);
        }
		
		##update/insert commit
		$result = $mst_locations->commit($locationId=='NULL'?'insert':'update');
		
		##insert sys_no table
		if($result['status']!=0)
			$sys_no->insertLocationwiseSystemNos($locationId=='NULL'?$result['insertId']:$locationId);

		##commit and close connection
		$db->commit();
		
		## call for functions
        $jquery->onSuccessEvent('newForm');
		
		##finish js function
		$jquery->closeEvent();
		
		## load combo details
        $result['combo']		= $mst_locations->getCombo();
		
		##return results
		return $result;
	}
	function loadLocationDetails()
	{
		//set global
		global $db;
		global $jquery;
		global $mst_locations;
		
		##start connection
		$db->connect();$db->begin();
		
		##function register
		$jquery->regEvent('intId','change',__FUNCTION__,true);
		
		##show waiting window
		$jquery->showWait();
		
		$data = $mst_locations->set($jquery->get('intId')->null());	
		$jquery->setControl($mst_locations->get_field_array());
		
		$jquery->closeEvent();
		$data['type'] = 'pass';
		return $data;
	}
	function deleteLocation()
	{
		//set global
		global $db;
		global $jquery;
		global $mst_locations;
		global $sys_no;
		
		##start connection
		$db->connect();$db->begin();
		
		##function register
		$jquery->regEvent('butDelete','click',__FUNCTION__,true);
		
		## set validate classes
		$jquery->get('intId')->validate('required');
		$jquery->validate();
		
		##confirm before delete
		$jquery->confirm('Are you sure you want to delete this record?.');
		
		##show waiting window
		$jquery->showWait();
		$id 		= $jquery->get('intId')->null();
		$result 	= $mst_locations->delete(" intId ='$id'");
		if($result['status']!=0)
		{
			$result2	= $sys_no->delete("intCompanyId='$id'");
		}
		
		//commit and close connection
		$db->commit();
		
		## call for functions
        $jquery->onSuccessEvent('newForm');
		
		##finish js function
		$jquery->closeEvent();
		
		## load combo details
        $result['combo']		= $mst_locations->getCombo();
		
		##return results
		return $result;
	}
	function newForm()
	{
		//set global
		global $db;
		global $jquery;
		global $mst_locations;
		
		##start connection
		$db->connect();
		
		##function register
		$jquery->regEvent('butNew','click',__FUNCTION__,false);
		//$jquery->addString(' document.location.href="./"');
		$jquery->resetForm();
		$jquery->get('strCode')->focus();
		$jquery->closeEvent();
	}
}
catch(Exception $e)
{
	$db->disconnect();
	
	$response['msg'] 			=  $e->getMessage();;
	$response['error'] 			=  $error_handler->jTraceEx($e);
	$response['type'] 			=  'fail';
	$response['sql']			=  $db->getSql();
	$response['mysql_error']	=  $db->getMysqlError();
	
	if(SHOW_TRY_CATCH_ERRORS) echo json_encode($response);
}
?>
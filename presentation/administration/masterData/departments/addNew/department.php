<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "class/tables/mst_department.php";			$mst_department 		= new mst_department($db);
?>
<title>Departments</title>
<form id="frmDepartment" name="frmDepartment" method="post" autocomplete="off">
<div align="center">
		<div class="trans_layoutD">
		  <div class="trans_text">Departments</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
              <tr>
                <td width="17" class="normalfnt">&nbsp;</td>
                <td colspan="3" class="normalfnt"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableBorder_allRound">
                  <tr class="normalfnt">
                    <td width="31%" height="40">&nbsp;Search</td>
                    <td width="69%"><select  <?Php if(!($form_permision['edit'] || $form_permision['delete'])){echo 'disabled';}  ?> name="intId" class="txtbox" id="intId" style="width:252px" tabindex="1">
                      <?php 
					echo $mst_department->getCombo();	
                ?>
                      </select></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td width="167" class="normalfnt">&nbsp;</td>
                <td>&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;Department Code&nbsp;<span class="compulsoryRed">*</span></td>
                <td width="162"><input name="strCode" type="text" class="txtbox" id="strCode" style="width:140px" tabindex="2"/></td>
                <td width="209" class="normalfnt">&nbsp;</td>
                </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;Department Name&nbsp;<span class="compulsoryRed">*</span></td>
                <td colspan="2"><input name="strName" type="text" class="validate[required,maxSize[50]]" id="strName" style="width:250px" tabindex="3"/></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;remarks</td>
                <td colspan="2">
                  <textarea name="strRemark" style="width:250px" rows="2" id="strRemark" tabindex="4"></textarea></td>
              </tr>
              <tr>
                <td rowspan="2" class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;Active</td>
                <td><input type="checkbox" name="intStatus" id="intStatus" checked="checked" tabindex="5"/></td>
                <td class="normalfnt">&nbsp;</td>
                </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><a class="button white medium" id="butNew" name="butNew" style="display:" tabindex="26">New</a><?php if($form_permision['add']||$form_permision['edit']){?><a class="button white medium" id="butSave" name="butSave" style="display:" tabindex="24">Save</a><?php } if($form_permision['delete']){?><a class="button white medium" id="butDelete" name="butDelete" style="display:" tabindex="25">Delete</a><?Php }?><a href="main.php" class="button white medium" id="butClose" name="butClose" style="display:" tabindex="27">Close</a></td>
              </tr>
            </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
</form>
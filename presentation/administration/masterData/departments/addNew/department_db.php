<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
try
{
	$jquery->setForm('frmDepartment');
	
	include_once "class/tables/mst_department.php";			$mst_department 		= new mst_department($db);
	include_once "class/dateTime.php";						$dateTimes 				= new dateTimes($db);
	
	## config
	$jquery->setComboId('intId');
	
	function saveDepartment()
	{
		//set global
		global $db;
		global $jquery;
		global $mst_department;
		global $dateTimes;
		global $sessions;
		
		##start connection
		$db->connect();$db->begin();
		
		##function register
		$jquery->regEvent('butSave','click',__FUNCTION__,true);
		
		##jquery validate
		$jquery->get('strCode')->validate('required,maxSize[10]');
		$jquery->get('strName')->validate('required,maxSize[50]');
		
		$jquery->validate();
		
		##show waiting window
		$jquery->showWait();
		$departmentId = $jquery->get('intId')->null();
		
		$mst_department->setintId($departmentId);
		$mst_department->setstrCode($jquery->get('strCode')->val());
		$mst_department->setstrName($jquery->get('strName')->val());
		$mst_department->setstrRemark($jquery->get('strRemark')->val());
		$mst_department->setintStatus($jquery->get('intStatus')->val());
		$mst_department->setintCreator($sessions->getUserId());
		$mst_department->setdtmCreateDate($dateTimes->getCurruntDateTime());
		
		##update/insert commit
		$result = $mst_department->commit($departmentId=='NULL'?'insert':'update');
		
		##commit and close connection
		$db->commit();
		
		## call for functions
        $jquery->onSuccessEvent('newForm');
		
		##finish js function
		$jquery->closeEvent();
		
		## load combo details
        $result['combo']		= $mst_department->getCombo();
		
		##return results
		return $result;
	}
	function loadDepartmentDetails()
	{
		//set global
		global $db;
		global $jquery;
		global $mst_department;
		
		##start connection
		$db->connect();$db->begin();
		##function register
		$jquery->regEvent('intId','change',__FUNCTION__,true);
		
		##show waiting window
		$jquery->showWait();
		
		$data = $mst_department->set($jquery->get('intId')->null());	
		$jquery->setControl($mst_department->get_field_array());
	
		$jquery->closeEvent();
		$data['type'] = 'pass';
		return $data;
	}
	function deleteDepartment()
	{
		//set global
		global $db;
		global $jquery;
		global $mst_department;
		
		##start connection
		$db->connect();$db->begin();
		
		##function register
		$jquery->regEvent('butDelete','click',__FUNCTION__,true);
		
		## set validate classes
		$jquery->get('intId')->validate('required');
		$jquery->validate();
		
		##confirm before delete
		$jquery->confirm('Are you sure you want to delete this record?.');
		
		##show waiting window
		$jquery->showWait();
		$id 	= $jquery->get('intId')->null();
		$result = $mst_department->delete(" intId ='$id'");
		
		//commit and close connection
		$db->commit();
		
		## call for functions
        $jquery->onSuccessEvent('newForm');
		
		##finish js function
		$jquery->closeEvent();
		
		## load combo details
        $result['combo']		= $mst_department->getCombo();
		
		##return results
		return $result;
	}
	function newForm()
	{
		//set global
		global $db;
		global $jquery;
		global $mst_department;
		
		##start connection
		$db->connect();
		
		##function register
		$jquery->regEvent('butNew','click',__FUNCTION__,false);
		//$jquery->addString(' document.location.href="./"');
		$jquery->resetForm();
		$jquery->get('strCode')->focus();
		$jquery->closeEvent();
	}
}
catch(Exception $e)
{
	$db->disconnect();
	
	$response['msg'] 			=  $e->getMessage();;
	$response['error'] 			=  $error_handler->jTraceEx($e);
	$response['type'] 			=  'fail';
	$response['sql']			=  $db->getSql();
	$response['mysql_error']	=  $db->getMysqlError();
	
	if(SHOW_TRY_CATCH_ERRORS) echo json_encode($response);
}
?>
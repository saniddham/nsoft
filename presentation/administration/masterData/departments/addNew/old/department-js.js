$(document).ready(function() {
	
	if(cId != ''){
		$('#frmDepartment #cboSearch').val(cId);
		loadData();
 	}
	
	$("#frmDepartment").validationEngine();
	$('#frmDepartment #txtCode').focus();

  ///save button click event
  $('#frmDepartment #butSave').click(function(){
	//$('#frmDepartment').submit();
	var requestType = '';
	if ($('#frmDepartment').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmDepartment #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = "presentation/administration/masterData/departments/addNew/department-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			data:$("#frmDepartment").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmDepartment #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmDepartment').get(0).reset();
						loadCombo_frmDepartment();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					$('#frmDepartment #butSave').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load department details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmLocations #cboSearch').click(function(){
	   $('#frmLocations').validationEngine('hide');
   });
    $('#frmDepartment #cboSearch').change(function(){
		$('#frmDepartment').validationEngine('hide');
		var url = "presentation/administration/masterData/departments/addNew/department-db-get.php";
		if($('#frmDepartment #cboSearch').val()=='')
		{
			$('#frmDepartment').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmDepartment #txtCode').val(json.code);
					$('#frmDepartment #txtName').val(json.name);
					$('#frmDepartment #txtRemark').val(json.remark);
					if(json.status)
						$('#frmDepartment #chkActive').attr('checked',true);
					else
						$('#frmDepartment #chkActive').removeAttr('checked');
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmDepartment #butNew').click(function(){
		$('#frmDepartment').get(0).reset();
		loadCombo_frmDepartment();
		$('#frmDepartment #txtCode').focus();
	});
    $('#frmDepartment #butDelete').click(function(){
		if($('#frmDepartment #cboSearch').val()=='')
		{
			$('#frmDepartment #butDelete').validationEngine('showPrompt', 'Please select Department.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmDepartment #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "presentation/administration/masterData/departments/addNew/department-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmDepartment #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmDepartment #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												if(json.type=='pass')
												{
													$('#frmDepartment').get(0).reset();
													loadCombo_frmDepartment();
													var t=setTimeout("alertDelete()",1000);return;
												}
												var t=setTimeout("alertDelete()",3000);	
											}	 
										});
									}
				}
		 	});
			
		}
	});
});


function loadCombo_frmDepartment()
{
	var url 	= "presentation/administration/masterData/departments/addNew/department-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmDepartment #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmDepartment #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmDepartment #butDelete').validationEngine('hide')	;
}

function loadData(){
	
		var dep	= $('#frmDepartment #cboSearch').val();
		var url = "presentation/administration/masterData/departments/addNew/department-db-get.php";
		if($('#frmDepartment #cboSearch').val()=='')
		{
			$('#frmDepartment').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+dep,
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmDepartment #txtCode').val(json.code);
					$('#frmDepartment #txtName').val(json.name);
					$('#frmDepartment #txtRemark').val(json.remark);
					if(json.status)
						$('#frmDepartment #chkActive').attr('checked',true);
					else
						$('#frmDepartment #chkActive').removeAttr('checked');
			}
	});
	
}
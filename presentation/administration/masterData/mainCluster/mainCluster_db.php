<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
try
{
	$requestType 		= $_REQUEST['requestType']; // request parameter
	$programCode		= 'P1120'; // program code
	
	include_once "class/tables/menupermision.php";						$menupermision 				= new menupermision($db);
	include_once "class/tables/sys_main_clusters.php";					$sys_main_clusters 			= new sys_main_clusters($db);
	
	if($requestType=='loadData')
	{
		$db->connect();
		
		$clusterId		= $_REQUEST['mainCluster'];
		
		$sys_main_clusters->set($clusterId);
		$response['clusterCode']	= $sys_main_clusters->getCODE();
		$response['clusterName']	= $sys_main_clusters->getNAME();
		$response['weeklyHolydy']	= $sys_main_clusters->getWEEKLY_HOLIDAY();
		$response['remarks']		= $sys_main_clusters->getREMARKS();
		$response['status']			= $sys_main_clusters->getSTATUS();
			
	}
	else if($requestType=='saveData')
	{
		$db->connect();$db->begin();//open connection.
		
		$clusterId		= $_REQUEST['clusterId'];
		$clusterCode	= $_REQUEST['clusterCode'];
		$clusterName	= $_REQUEST['clusterName'];
		$weeklyHoliday	= $_REQUEST['weeklyHoliday'];
		$remarks		= $_REQUEST['remarks'];
		$status			= $_REQUEST['status'];
		$editMode		= '';
		
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Edit','','');
		
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);	
		
		if($clusterId=='')
		{
			$editMode	= false;
			$result_arr	= $sys_main_clusters->insertRec($clusterCode,$clusterName,$status,$weeklyHoliday,$remarks);
			
			if(!$result_arr['status'])
				throw new Exception($result_arr['msg']);
		}
		else
		{
			$editMode	= true;
			$sys_main_clusters->set($clusterId);
			
			$sys_main_clusters->setCODE($clusterCode);
			$sys_main_clusters->setNAME($clusterName);
			$sys_main_clusters->setWEEKLY_HOLIDAY($weeklyHoliday);
			$sys_main_clusters->setREMARKS($remarks);
			$sys_main_clusters->setSTATUS($status);
			
			$result_arr	= $sys_main_clusters->commit();
			
			if(!$result_arr['status'])
				throw new Exception($result_arr['msg']);
		}
		
		$db->commit();
		$response['type'] 			= "pass";
		if($editMode)
			$response['msg'] 		= "Updated Successfully.";
		else
			$response['msg'] 		= "Saved Successfully.";
	}
	else if($requestType=='loadCluster')
	{
		$response['cboCluster'] 	= $sys_main_clusters->getCombo();
	}
	else if($requestType=='deleteData')
	{
		$db->connect();$db->begin();//open connection.
		
		$clusterId		= $_REQUEST['clusterId'];
		
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Delete','','');
		
		$resultDel		= $sys_main_clusters->delete("ID = '$clusterId'");
		
		if(!$resultDel['status'])	
			throw new Exception($resultDel['msg']);
		else
		{	
			$db->commit();	
			$response['type']	= 'pass';
			$response['msg']	= 'Deleted Successfully.';
		}
	}
}
catch(Exception $e)
{
	$db->rollback();
	$response['msg'] 	=  $e->getMessage();
	$response['error'] 	=  $error_handler->jTraceEx($e);
	$response['type'] 	=  'fail';
	$response['sql']	=  $db->getSql();
	$db->disconnect();
}
$db->disconnect();
echo json_encode($response);
?>
// JavaScript Document
$(document).ready(function(){
	
	$("#frmMainCluster").validationEngine();
	
	$('#frmMainCluster #cboCluster').die('change').live('change',loadData);
	$('#frmMainCluster #butSave').die('click').live('click',saveData);
	$('#frmMainCluster #butNew').die('click').live('click',clearNew);
	$('#frmMainCluster #butDelete').die('click').live('click',deleteData);
	
});

function loadData()
{
	if($(this).val()=='')
	{
		clearSearch();
		return;
	}
	
	showWaiting();
	var url 	= "controller.php?q=1120&requestType=loadData";
	var data 	= "mainCluster="+$('#frmMainCluster #cboCluster').val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmMainCluster #txtClusterCode').val(json.clusterCode);
				$('#frmMainCluster #txtClusterName').val(json.clusterName);
				$('#frmMainCluster #cboWeeklyHoliday').val(json.weeklyHolydy);
				$('#frmMainCluster #strRemark').val(json.remarks);
				$('#frmMainCluster #chkActive').attr('checked',(json.status==1?true:false));
				hideWaiting();
			}
	});
	hideWaiting();	
}
function saveData()
{
	showWaiting();
	var clusterId 		= $('#frmMainCluster #cboCluster').val();
	var clusterCode 	= $('#frmMainCluster #txtClusterCode').val();
	var clusterName 	= $('#frmMainCluster #txtClusterName').val();
	var weeklyHoliday 	= $('#frmMainCluster #cboWeeklyHoliday').val();
	var remarks 		= $('#frmMainCluster #strRemark').val();
	var status	 		= ($('#frmMainCluster #chkActive').prop('checked')?1:0);
	
	if(!$('#frmMainCluster').validationEngine('validate'))
	{
		hideWaiting();
		return;
	}
	var data	= "clusterId="+clusterId;
		data   += "&clusterCode="+clusterCode;
		data   += "&clusterName="+clusterName;
		data   += "&weeklyHoliday="+weeklyHoliday;
		data   += "&remarks="+remarks;
		data   += "&status="+status;
		
	var url 	= "controller.php?q=1120&requestType=saveData";
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json){
				$('#frmMainCluster #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				if(json.type=='pass')
				{
					var t = setTimeout("alertx()",1000);
					clearNew();
					loadCluster();
					hideWaiting();
					return;
				}
				else
				{
					hideWaiting();
				}
				
			},
			error:function(xhr,status){
						
				$('#frmMainCluster #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				hideWaiting();
				return;
			}	
	});
	
}
function loadCluster()
{
	var url 			= "controller.php?q=1120&requestType=loadCluster";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		type:'POST',
		async:false,
		success:function(json){
			$('#frmMainCluster #cboCluster').html(json.cboCluster);
		}
	})
}
function deleteData()
{
	var clusterId	= $('#frmMainCluster #cboCluster').val();
	if(clusterId=='')
	{
		$('#frmMainCluster #cboCluster').validationEngine('showPrompt','Please select a Cluster.','fail');
		return ;	
	}
	
	var val = $.prompt('Are you sure you want to delete "'+$('#frmMainCluster #cboCluster option:selected').text()+'" ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
					var data 		= "requestType=deleteData";
					data		   += "&clusterId="+clusterId;
					var url	 		= "controller.php?q=1120";
					$.ajax({
							url:url,
							data:data,
							dataType:'json',
							type:'POST',
							async:false,
							success: function(json)
							{
								$('#frmMainCluster #butDelete').validationEngine('showPrompt', json.msg,json.type);
								if(json.type=='pass')
								{
									var t=setTimeout("Deletex()",1000);
									clearNew();
									loadCluster();
									return;
								}	
							},
							error:function(xhr,status)
							{
								$('#frmMainCluster #butDelete').validationEngine('showPrompt', json.msg,json.type);
								return;
							}
					});
				}
			}
			});
}
function clearNew()
{
	$('#frmMainCluster #cboCluster').val('');
	clearSearch();
}
function clearSearch()
{	
	$('#frmMainCluster #txtClusterCode').val('');
	$('#frmMainCluster #txtClusterName').val('');
	$('#frmMainCluster #cboWeeklyHoliday').val('');
	$('#frmMainCluster #strRemark').val('');
	$('#frmMainCluster #chkActive').attr('checked',true);
}
function alertx()
{
	$('#frmMainCluster #butSave').validationEngine('hide')	;
}
function Deletex()
{
	$('#frmMainCluster #butDelete').validationEngine('hide')	;
}
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php 
/*
 $companyId 	= $jquery->get('cboSearch')->val();
 $jquery->alert();
 $jquery->addString(" alert($('#intId').prop('val'))");
 $jsonArray		= $jquery->json();
 */
try{
	$jquery->setForm('frmCompanyDetails');
	require_once 'class/tables/mst_companies.php'; $mst_companies = new mst_companies($db);	

    ## config
    $jquery->setComboId('intId');
	
	function saveCompany(){
		//set global
		global $db;
		global $jquery;
		global $mst_companies;
		
		##start connection
		$db->connect();
		
		##function register
		$jquery->regEvent('butSave','click',__FUNCTION__,true);
		
		##jquery validate
		$jquery->get('strCode')->validate('required,maxSize[10]');
		$jquery->get('strName')->validate('required');
		$jquery->get('intCountryId')->validate('required');
		$jquery->get('strWebSite')->validate('custom[url]');
		$jquery->get('intBaseCurrencyId')->validate('required');
		
		$jquery->validate();
		
		##show waiting window
		$jquery->showWait();
		$companyId = $jquery->get('intId')->null();
		
		$mst_companies->setintId($companyId);
		$mst_companies->setstrCode($jquery->get('strCode')->val());
		$mst_companies->setstrName($jquery->get('strName')->val());
		$mst_companies->setintCountryId($jquery->get('intCountryId')->val());
		$mst_companies->setstrWebSite($jquery->get('strWebSite')->val());
		$mst_companies->setstrRemarks($jquery->get('strRemarks')->val());
		$mst_companies->setstrAccountNo($jquery->get('strAccountNo')->val());
		$mst_companies->setstrRegistrationNo($jquery->get('strRegistrationNo')->val());
		$mst_companies->setstrVatNo($jquery->get('strVatNo')->val());
		$mst_companies->setstrSVatNo($jquery->get('strSVatNo')->val());
		$mst_companies->setintBaseCurrencyId($jquery->get('intBaseCurrencyId')->val());
		$mst_companies->setintStatus($jquery->get('intStatus')->val());
		
		##update/insert commit
		$result = $mst_companies->commit($companyId=='NULL'?'insert':'update');
		
		//commit and close connection
		$db->commit();

        ## call for functions

        $jquery->onSuccessEvent('newForm');

		##finish js function
		$jquery->closeEvent();

        ## load combo details
        $result['combo']=$mst_companies->getCombo();

		##return results
		return $result;
		
	}
	
	function loadCustomerDetails(){
		//set global
		global $db;
		global $jquery;
		global $mst_companies;
		
		##start connection
		$db->connect();
		
		##function register
		$jquery->regEvent('intId','change',__FUNCTION__,true);	
		
		##show waiting window
		$jquery->showWait();
		
		$data = $mst_companies->set($jquery->get('intId')->null());
		$jquery->setControl($mst_companies->get_field_array());
		
		$jquery->closeEvent();
		$data['type'] = 'pass';
		return $data;
	}
	
	function deleteCompany(){
		//set global
		global $db;
		global $jquery;
		global $mst_companies;
		
		##start connection
		$db->connect();
		
		##function register
		$jquery->regEvent('butDelete','click',__FUNCTION__,true);
		
		## set validate classes
        //$jquery->get->removeValidation();
		$jquery->get('intId')->validate('required');
		$jquery->validate();
		
		##confirm before delete
		$jquery->confirm('Are you sure you want to delete this record?.');
		
		##show waiting window
		$jquery->showWait();
		$id = $jquery->get('intId')->null();
		$result = $mst_companies->delete(" intId ='$id'");
		//commit and close connection
		$db->commit();
		
		## call for functions

        $jquery->onSuccessEvent('newForm');
		
		##finish js function
		$jquery->closeEvent();
		
		 ## load combo details
        $result['combo']=$mst_companies->getCombo();
		
		##return results
		return $result;
		
	}
	function newForm()
	{
		//set global
		global $db;
		global $jquery;
		global $mst_companies;
		
		##start connection
		$db->connect();
		
		##function register
		$jquery->regEvent('butNew','click',__FUNCTION__,false);
		//$jquery->addString(' document.location.href="./"');
		$jquery->resetForm();
		$jquery->get('strCode')->focus();
		$jquery->closeEvent();
	}
}catch(Exception $e){
	$db->disconnect();
	
		$response['msg'] 			=  $e->getMessage();;
		$response['error'] 			=  $error_handler->jTraceEx($e);
		$response['type'] 			=  'fail';
		$response['sql']			=  $db->getSql();
		$response['mysql_error']	=  $db->getMysqlError();
		
		if(SHOW_TRY_CATCH_ERRORS) echo json_encode($response);
}
?>
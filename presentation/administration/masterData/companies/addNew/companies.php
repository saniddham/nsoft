<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<title>Company Details</title>
<form id="frmCompanyDetails"  name="frmCompanyDetails" method="post" autocomplete="off">
<div align="center">
    <div class="trans_layoutD">
    <div class="trans_text">Company Details</div>
    <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    	<tr>
        	<td>
            	<table width="100%" border="0" align="center">
                <tr class="normalfnt">
                  <td colspan="3"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableBorder_allRound" >
                    <tr class="normalfnt">
                    <td height="45" width="24%">&nbsp;Search</td>
                    <td width="68%"><select <?Php if(!($form_permision['edit'] || $form_permision['delete'])){echo 'disabled';}  ?>   name="intId"  id="intId" style="width:101%">
                 <option value=""></option>
                 <?php  $sql = "SELECT
								mst_companies.intId,
								mst_companies.strName
								FROM mst_companies
								order by strName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?> 
                  </select></td>
                    <td width="8%">&nbsp;</td>
                    </tr>
                  </table></td>
                  </tr>
                <tr class="normalfnt">
                  <td width="24%">&nbsp;</td>
                  <td width="68%">&nbsp;</td>
                  <td width="8%">&nbsp;</td>
                </tr>
                <tr class="normalfnt">
                  <td>&nbsp;Company Code&nbsp;<span class="compulsoryRed">*</span></td>
                  <td><input name="strCode" type="text" class="txtbox" id="strCode" style="width:140px" /></td>
                  <td>&nbsp;</td>
                </tr>
                <tr class="normalfnt">
                  <td>&nbsp;Company Name&nbsp;<span class="compulsoryRed">*</span></td>
                  <td><input name="strName" type="text" class="txtbox" id="strName" style="width:100%" /></td>
                  <td>&nbsp;</td>
                </tr>
                <tr class="normalfnt">
                  <td>&nbsp;Country&nbsp;<span class="compulsoryRed">*</span></td>
                  <td><select name="intCountryId" class="txtbox" id="intCountryId" style="width:142px">
                    <option value=""></option>
                    <?php  $sql = "SELECT
									mst_country.intCountryID,
									mst_country.strCountryName
								FROM mst_country
								WHERE
									intStatus = 1
									order by strCountryName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intCountryID']."\">".$row['strCountryName']."</option>";
								}
                   ?>
                  </select></td>
                  <td>&nbsp;</td>
                </tr>
                <tr class="normalfnt">
                  <td>&nbsp;Base Currency <span class="compulsoryRed">*</span></td>
                  <td><select name="intBaseCurrencyId" id="intBaseCurrencyId" style="width:142px" >
                    <option value=""></option>
                    <?php  $sql = "SELECT
						intId,
						strCode
						FROM mst_financecurrency
						WHERE
							intStatus = 1
						order by strCode
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
						}
        				?>
                  </select></td>
                  <td>&nbsp;</td>
                </tr>
                <tr class="normalfnt">
                  <td>&nbsp;Website</td>
                  <td><input name="strWebSite" type="text" class="txtbox" id="strWebSite" style="width:100%" /></td>
                  <td>&nbsp;</td>
                </tr>
                <tr class="normalfnt">
                  <td valign="top">&nbsp;Remarks</td>
                  <td><textarea name="strRemarks" style="width:100%"  rows="2" class="txtbox" id="strRemarks"></textarea></td>
                  <td>&nbsp;</td>
                </tr>
                <tr class="normalfnt">
                  <td>&nbsp;Account No.</td>
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr class="normalfnt">
                          <td width="43%"><input name="strAccountNo" type="text" class="txtbox" id="strAccountNo" style="width:140px" /></td>
                          <td width="17%">Reg. No.</td>
                          <td width="40%" style="text-align:right"><input name="strRegistrationNo" type="text" class="txtbox" id="strRegistrationNo" style="width:100%"/></td>
                        </tr>
                      </table></td>
                  <td>&nbsp;</td>
                </tr>
                <tr class="normalfnt">
                  <td>&nbsp;VAT No.</td>
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr class="normalfnt">
                          <td width="43%"><input name="strVatNo" type="text" class="txtbox" id="strVatNo" style="width:140px" /></td>
                          <td width="17%">SVAT No.</td>
                          <td width="40%" style="text-align:right"><input name="strSVatNo" type="text" class="txtbox" id="strSVatNo" style="width:100%"/></td>
                        </tr>
                      </table></td>
                  <td>&nbsp;</td>
                </tr>
                <tr class="normalfnt">
                	<td>&nbsp;Active</td>
                    <td><input type="checkbox" name="intStatus" id="intStatus" checked="checked" style="vertical-align: middle"/></td>
                    <td>&nbsp;</td>
                </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="34"><table width="100%" border="0">
              <tr>
                <td width="24%">&nbsp;</td>
                <td width="68%">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                    <tr>
                        <td width="100%" align="center" bgcolor=""><a class="button white medium" id="butNew" name="butNew" style="display:" tabindex="28">New</a><?php if($form_permision['add']||$form_permision['edit']){?><a class="button white medium" id="butSave" name="butSave" style="display:" tabindex="24">Save</a><?php } if($form_permision['delete']){?><a class="button white medium" id="butDelete" name="butDelete" style="display:" tabindex="25">Delete</a><?Php }?><a href="main.php" class="button white medium" id="butClose" name="butClose" style="display:" tabindex="27">Close</a></td>
                    </tr>
                </table>
                </td>
                <td width="8%">&nbsp;</td>
              </tr>
            </table>
                
            </td>
        </tr>
    </table>
    </div>
    </div>
</form>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "class/tables/sys_sub_clusters.php";					$sys_sub_clusters 		= new sys_sub_clusters($db);
include_once "class/tables/sys_main_clusters.php";					$sys_main_clusters 		= new sys_main_clusters($db);

?>

<title>Sub Cluster</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />

<form id="frmSubCluster" name="frmSubCluster" method="post" autocomplete="off">
<div align="center">
    <div class="trans_layoutD">
        <div class="trans_text">Sub Cluster</div>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr>
        	<td>
            	<table width="100%" border="0" align="center">
                	<tr class="normalfnt">
                    	<td width="9%">&nbsp;</td>
                    	<td width="23%">Sub Cluster</td>
                        <td width="68%"><select  <?Php if(!($form_permision['edit'] || $form_permision['delete'])){echo 'disabled';}  ?> name="cboCluster" class="txtbox" id="cboCluster" style="width:279px" tabindex="1">
						<?php 
                       		echo $sys_sub_clusters->getCombo();	
                        ?>
                        </select>
                        </td>
                    </tr>
                    <tr class="normalfnt">
                    	<td width="9%"></td>
                    	<td width="23%">&nbsp;</td>
                        <td width="68%">&nbsp;</td>
                    </tr>
                    <tr class="normalfnt">
                    	<td width="9%"></td>
                    	<td width="23%">Cluster Code&nbsp;<span class="compulsoryRed">*</span></td>
                        <td width="68%"><input  name="txtClusterCode" type="text" class="validate[required,maxSize[100]]" id="txtClusterCode" style="width:140px"  tabindex="2"/></td>
                    </tr>
                    <tr class="normalfnt">
                    	<td width="9%"></td>
                    	<td width="23%">Cluster Name&nbsp;<span class="compulsoryRed">*</span></td>
                        <td width="68%"><input name="txtClusterName" type="text" class="validate[required,maxSize[255]]" id="txtClusterName" style="width:275px" tabindex="3"/></td>
                    </tr>
                    <tr class="normalfnt">
                    	<td width="9%"></td>
                    	<td width="23%">Main Cluster&nbsp;<span class="compulsoryRed">*</span></td>
                        <td width="68%"><select name="cboMainCluster" class="txtbox validate[required]" id="cboMainCluster" style="width:279px" tabindex="4">
						<?php 
                       		echo $sys_main_clusters->getCombo(NULL,'STATUS = 1');	
                        ?>
                        </select></td>
                    </tr>
                    <tr class="normalfnt">
                    	<td width="9%"></td>
                    	<td width="23%">Remarks</td>
                        <td width="68%"><textarea name="strRemark" style="width:275px" rows="3" id="strRemark" tabindex="5"></textarea></td>
                    </tr>
                    <tr class="normalfnt">
                    	<td width="9%"></td>
                    	<td width="23%">Active</td>
                        <td width="68%"><input type="checkbox" name="chkActive" id="chkActive" checked="checked" tabindex="6"/></td>
                    </tr>
                    <tr class="normalfnt">
                    	<td width="9%"></td>
                    	<td width="23%">&nbsp;</td>
                        <td width="68%">&nbsp;</td>
                    </tr>
                </table>  
            </td>
        </tr>
        <tr>
        	<td>
                <table width="100%" border="0" class="bcgl2">
                    <tr>
                    	<td width="100%" align="center" bgcolor=""><a class="button white medium" id="butNew" name="butNew">New</a><?php echo($form_permision['edit']==1?'<a class="button white medium" id="butSave" name="butSave">Save</a>':'');echo($form_permision['delete']==1?'<a class="button white medium" id="butDelete" name="butDelete">Delete</a>':'');?><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
    </div>
</div>
</form>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
try
{
	$requestType 		= $_REQUEST['requestType']; // request parameter
	$programCode		= 'P1121'; // program code
	
	include_once "class/tables/menupermision.php";						$menupermision 				= new menupermision($db);
	include_once "class/tables/sys_sub_clusters.php";					$sys_sub_clusters 			= new sys_sub_clusters($db);
	
	if($requestType=='loadData')
	{
		$db->connect();
		
		$clusterId		= $_REQUEST['subCluster'];
		
		$sys_sub_clusters->set($clusterId);
		$response['clusterCode']	= $sys_sub_clusters->getCODE();
		$response['clusterName']	= $sys_sub_clusters->getNAME();
		$response['mainCluster']	= $sys_sub_clusters->getMAIN_CLUSTER();
		$response['remarks']		= $sys_sub_clusters->getREMARKS();
		$response['status']			= $sys_sub_clusters->getSTATUS();
			
	}
	else if($requestType=='saveData')
	{
		$db->connect();$db->begin();//open connection.
		
		$clusterId		= $_REQUEST['clusterId'];
		$clusterCode	= $_REQUEST['clusterCode'];
		$clusterName	= $_REQUEST['clusterName'];
		$mainCluster	= $_REQUEST['mainCluster'];
		$remarks		= $_REQUEST['remarks'];
		$status			= $_REQUEST['status'];
		$editMode		= '';
		
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Edit','','');
		
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);	
		
		if($clusterId=='')
		{
			$editMode	= false;
			$result_arr	= $sys_sub_clusters->insertRec($mainCluster,$clusterCode,$clusterName,$status,$remarks);
			
			if(!$result_arr['status'])
				throw new Exception($result_arr['msg']);
		}
		else
		{
			$editMode	= true;
			$sys_sub_clusters->set($clusterId);
			
			$sys_sub_clusters->setCODE($clusterCode);
			$sys_sub_clusters->setNAME($clusterName);
			$sys_sub_clusters->setMAIN_CLUSTER($mainCluster);
			$sys_sub_clusters->setREMARKS($remarks);
			$sys_sub_clusters->setSTATUS($status);
			
			$result_arr	= $sys_sub_clusters->commit();
			
			if(!$result_arr['status'])
				throw new Exception($result_arr['msg']);
		}
		
		$db->commit();
		$response['type'] 			= "pass";
		if($editMode)
			$response['msg'] 		= "Updated Successfully.";
		else
			$response['msg'] 		= "Saved Successfully.";
	}
	else if($requestType=='loadCluster')
	{
		$response['cboCluster'] 	= $sys_sub_clusters->getCombo();
	}
	else if($requestType=='deleteData')
	{
		$db->connect();$db->begin();//open connection.
		
		$clusterId		= $_REQUEST['clusterId'];
		
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Delete','','');
		
		$resultDel		= $sys_sub_clusters->delete("ID = '$clusterId'");
		
		if(!$resultDel['status'])	
			throw new Exception($resultDel['msg']);
		else
		{	
			$db->commit();	
			$response['type']	= 'pass';
			$response['msg']	= 'Deleted Successfully.';
		}
	}
}
catch(Exception $e)
{
	$db->rollback();
	$response['msg'] 	=  $e->getMessage();
	$response['error'] 	=  $error_handler->jTraceEx($e);
	$response['type'] 	=  'fail';
	$response['sql']	=  $db->getSql();
	$db->disconnect();
}
$db->disconnect();
echo json_encode($response);
?>
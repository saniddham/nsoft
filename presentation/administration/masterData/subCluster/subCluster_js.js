// JavaScript Document
$(document).ready(function(){
	
	$("#frmSubCluster").validationEngine();
	
	$('#frmSubCluster #cboCluster').die('change').live('change',loadData);
	$('#frmSubCluster #butSave').die('click').live('click',saveData);
	$('#frmSubCluster #butNew').die('click').live('click',clearNew);
	$('#frmSubCluster #butDelete').die('click').live('click',deleteData);
	
});

function loadData()
{
	if($(this).val()=='')
	{
		clearSearch();
		return;
	}
	
	showWaiting();
	var url 	= "controller.php?q=1121&requestType=loadData";
	var data 	= "subCluster="+$('#frmSubCluster #cboCluster').val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmSubCluster #txtClusterCode').val(json.clusterCode);
				$('#frmSubCluster #txtClusterName').val(json.clusterName);
				$('#frmSubCluster #cboMainCluster').val(json.mainCluster);
				$('#frmSubCluster #strRemark').val(json.remarks);
				$('#frmSubCluster #chkActive').attr('checked',(json.status==1?true:false));
				hideWaiting();
			}
	});
	hideWaiting();	
}
function saveData()
{
	showWaiting();
	var clusterId 		= $('#frmSubCluster #cboCluster').val();
	var clusterCode 	= $('#frmSubCluster #txtClusterCode').val();
	var clusterName 	= $('#frmSubCluster #txtClusterName').val();
	var mainCluster 	= $('#frmSubCluster #cboMainCluster').val();
	var remarks 		= $('#frmSubCluster #strRemark').val();
	var status	 		= ($('#frmSubCluster #chkActive').prop('checked')?1:0);
	
	if(!$('#frmSubCluster').validationEngine('validate'))
	{
		hideWaiting();
		return;
	}
	var data	= "clusterId="+clusterId;
		data   += "&clusterCode="+clusterCode;
		data   += "&clusterName="+clusterName;
		data   += "&mainCluster="+mainCluster;
		data   += "&remarks="+remarks;
		data   += "&status="+status;
		
	var url 	= "controller.php?q=1121&requestType=saveData";
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json){
				$('#frmSubCluster #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				if(json.type=='pass')
				{
					var t = setTimeout("alertx()",1000);
					clearNew();
					loadCluster();
					hideWaiting();
					return;
				}
				else
				{
					hideWaiting();
				}
				
			},
			error:function(xhr,status){
						
				$('#frmSubCluster #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				hideWaiting();
				return;
			}	
	});
	
}
function loadCluster()
{
	var url 			= "controller.php?q=1121&requestType=loadCluster";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		type:'POST',
		async:false,
		success:function(json){
			$('#frmSubCluster #cboCluster').html(json.cboCluster);
		}
	})
}
function deleteData()
{
	var clusterId	= $('#frmSubCluster #cboCluster').val();
	if(clusterId=='')
	{
		$('#frmSubCluster #cboCluster').validationEngine('showPrompt','Please select a Cluster.','fail');
		return ;	
	}
	
	var val = $.prompt('Are you sure you want to delete "'+$('#frmSubCluster #cboCluster option:selected').text()+'" ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
					var data 		= "requestType=deleteData";
					data		   += "&clusterId="+clusterId;
					var url	 		= "controller.php?q=1121";
					$.ajax({
							url:url,
							data:data,
							dataType:'json',
							type:'POST',
							async:false,
							success: function(json)
							{
								$('#frmSubCluster #butDelete').validationEngine('showPrompt', json.msg,json.type);
								if(json.type=='pass')
								{
									var t=setTimeout("Deletex()",1000);
									clearNew();
									loadCluster();
									return;
								}	
							},
							error:function(xhr,status)
							{
								$('#frmSubCluster #butDelete').validationEngine('showPrompt', json.msg,json.type);
								return;
							}
					});
				}
			}
			});
}
function clearNew()
{
	$('#frmSubCluster #cboCluster').val('');
	clearSearch();
}
function clearSearch()
{	
	$('#frmSubCluster #txtClusterCode').val('');
	$('#frmSubCluster #txtClusterName').val('');
	$('#frmSubCluster #cboMainCluster').val('');
	$('#frmSubCluster #strRemark').val('');
	$('#frmSubCluster #chkActive').attr('checked',true);
}
function alertx()
{
	$('#frmSubCluster #butSave').validationEngine('hide')	;
}
function Deletex()
{
	$('#frmSubCluster #butDelete').validationEngine('hide')	;
}
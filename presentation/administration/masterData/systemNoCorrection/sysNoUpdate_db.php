<?php
session_start();
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$locationId	  		= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$savedMasseged		= "";
$error_sql			= "";
$savedStatus		= true;

include "../../../../dataAccess/Connector.php";
include  "../../../../class/administration/masterData/systemNoCorrection/sysNoupdate_get.php";
include  "../../../../class/administration/masterData/systemNoCorrection/sysNoupdate_set.php";

$obj_sysNoUpdate_get	= new sysNoUpdate_get($db);
$obj_sysNoUpdate_set	= new sysNoUpdate_set($db);
$requestType 			= $_REQUEST['requestType'];

if($requestType=='loadGrid')
{
//echo "ok";
	$searchID 				= $_REQUEST['searchID'];
	$response['dataArr']	= $obj_sysNoUpdate_get->loadGridData($searchID);	
	
	echo json_encode($response);
}
else if($requestType=='fixSys_no')
{
	$jsnArr		 = json_decode($_REQUEST['jsnArr'],true);
	$location_id = $jsnArr['location_id'];
	$lastNo		 = $jsnArr['lastNo'];
	$prog_id	 = $jsnArr['prog_id'];
	//echo $lastNo;
	
	$db->begin();
	$dataArr	= $obj_sysNoUpdate_set->sys_noUpdate($location_id,$lastNo,$prog_id);
	if($dataArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	 = false;
		$savedMasseged	 = $dataArr['savedMassege'];
		$error_sql		 = $dataArr['error_sql'];	
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 	= "pass";
		$response['msg']	= "Updated Successfully.";
				
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;	
	}
	echo json_encode($response);
}

?>
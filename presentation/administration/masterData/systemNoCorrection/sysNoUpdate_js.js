var basePath	= "presentation/administration/masterData/systemNoCorrection/";
$(document).ready(function(e) {
	$('#frmSysNoUpdate').validationEngine();
	$('#frmSysNoUpdate #cboProg').die('change').live('change',loadGrid);
	$('#frmSysNoUpdate .cls_butFix').die('click').live('click',fixSysNo);

});

function loadGrid()
{
	$("#tbl_update_no tr:gt(0)").remove();
	
	if($("#cboProg").val()=="")
	{
		return;	
	}
	else
	{
	var searchID    = $('#cboProg').val();
	var data 		= "requestType=loadGrid";
	data		    += "&searchID="+searchID;
	var url 		= basePath+"sysNoUpdate_db.php";
	var obj 	= $.ajax({
					dataType:"json",
					url:url,
					type:"post",
					data:data,
					success: function(json){
						
								var length	= json.dataArr.length;
								var dataArr	= json.dataArr;
								
								for(i = 0; i<length; i++)
								{
									var sys_no		= dataArr[i]['sys_no'];	
									var location_id = dataArr[i]['location_id'];								
									var location	= dataArr[i]['location']; 
									var last_no		= dataArr[i]['last_no'];
	
								
								if(location!=null)
								{
									createTable(sys_no,location,location_id,last_no);
								}
								}
						
						},
						error:function(xhr,status){
				
						}
						
					});
	}
}

function createTable(sys_no,location,location_id,last_no)
{
	var table 			= document.getElementById('tbl_update_no');
	var lstrow 			= table.rows.length;
	var row 			= table.insertRow(lstrow);
	row.id				= location_id;
	
	var cell			= row.insertCell(0);
	cell.className		= 'td_location';
	cell.innerHTML		= location;
	
	var cell			= row.insertCell(1);
	cell.className		= 'td_last_no';
	cell.innerHTML		= last_no;
	
	var cell		 	= row.insertCell(2);
	cell.className	 	= 'td_sys_no';
	cell.innerHTML	 	= sys_no;
		
	var cell		 	= row.insertCell(3);
	cell.className	 	= 'td_btnFix';
	cell.setAttribute("style",'text-align:center')
	if((parseFloat(last_no)+1)!= parseFloat(sys_no) && parseFloat(last_no)!=0)
		cell.innerHTML	= '<a class=\"button green medium cls_butFix\" id=\"butFix\" name=\"butFix\">Fix</a>';
	else
		cell.innerHTML	= "&nbsp";		
	
	
}
function fixSysNo()
{
	var lastNo 		= $(this).parent().parent().find('.td_last_no').html();
	var location_id = $(this).parent().parent().attr('id');
	var prog_id		= $('#cboProg').val();
	var btnFix		= $(this);
	//alert($(btnFix).attr('id'));
	
	var data 		= "requestType=fixSys_no";
	var jsnArr		= "{";
							jsnArr += '"location_id":"'+location_id+'",';
							jsnArr += '"lastNo":"'+lastNo+'",';
							jsnArr += '"prog_id":"'+prog_id+'"';
			
							jsnArr += "}";
		
	data+="&jsnArr="+jsnArr;
	var url 		= basePath+"sysNoUpdate_db.php";
	var obj 	= $.ajax({
					dataType:"json",
					url:url,
					type:"post",
					data:data,
					success: function(json){
						$(btnFix).validationEngine('showPrompt', json.msg,json.type);
						if(json.type=='pass')
						{
							$(btnFix).hide();
							$(btnFix).parent().parent().find('.td_sys_no').html(parseFloat(lastNo)+1);
							var t=setTimeout("alertx();",1000);
								
						}
					},
					error:function(xhr,status){
						
						$(btnFix).validationEngine('showPrompt', errormsg(xhr.status),'fail');
						return;
				}	

	});
		
}
function alertx()
{
	$('#frmSysNoUpdate .cls_butFix').validationEngine('hide');
}
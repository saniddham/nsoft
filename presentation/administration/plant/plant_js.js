// JavaScript Document
var menuID	= 1119;
$(document).ready(function(e) {
	$("#frmPlant").validationEngine();
	$('#frmPlant #butSave').die('click').live('click',savePlant);
    $('#frmPlant #cboMainCluster').die('change').live('change',loadSubCluster);
	$('#frmPlant #cboPlant').die('change').live('change',loadPlant);
	$('#frmPlant #butNew').die('click').live('click',clearAll);
});

function clearAll()
{
	$('#frmPlant').get(0).reset();
	$('#frmPlant #cboSubCluster').html('');
	loadPlantCombo();
}
function loadPlantCombo()
{
	var url			= "controller.php?q="+menuID+"&requestType=loadPlantCombo";
	var httpobj = $.ajax({
			url:url,
			dataType:'json',
			async:false,
			success:function(json){
					$('#frmPlant #cboPlant').html(json.plant);
			}
		});	
}
function loadSubCluster()
{
	var url			= "controller.php?q="+menuID+"&requestType=loadSubCluster";
	var maincluster	= $('#frmPlant #cboMainCluster').val();
	var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"maincluster="+maincluster,
			async:false,
			success:function(json){
					$('#frmPlant #cboSubCluster').html(json.subCluster);
			}
		});
}

function savePlant()
{
	var url				= "controller.php?q="+menuID+"&requestType=savePlant";
	var searchPlant		= $('#frmPlant #cboPlant').val();
	var plantName		= $('#frmPlant #txtPlant').val();
	var plantHeadName	= $('#frmPlant #txtPlantHead').val();
	var HeadContact		= $('#frmPlant #txtPlantHeadPhone').val();
	var mainCluster		= $('#frmPlant #cboMainCluster').val();	
	var subCluster		= $('#frmPlant #cboSubCluster').val();	
	var active			= ($('#frmPlant #chkActive').attr('checked')?1:0);
	
	if($('#frmPlant').validationEngine('validate'))
	{
		var dataArr		= "{";	
			dataArr	   += '"searchPlant":"'+searchPlant+'",';
			dataArr	   += '"plantName":"'+plantName+'",';
			dataArr	   += '"plantHeadName":"'+plantHeadName+'",';
			dataArr	   += '"HeadContact":"'+HeadContact+'",';
			dataArr	   += '"mainCluster":"'+mainCluster+'",';
			dataArr	   += '"subCluster":"'+subCluster+'",';
			dataArr	   += '"active":"'+active+'"';
			dataArr	   += "}";
			
			data	   = "dataArr="+dataArr;
			
		$.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:data,
			async:false,
			success: function(json)
			{
				$('#frmPlant #butSave').validationEngine('showPrompt',json.msg,json.type);
				if(json.type == 'pass')
				{
					
					clearAll();
					var t=setTimeout("alertx()",1000);
					return;
				}
				else
				{
					
				}
				
			},
			error:function(xhr,status){
						
						$('#frmPlant #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						return;
				}		
		});

	}
}

function loadPlant()
{
	var searchPlant	= $('#frmPlant #cboPlant').val();
	var	url			= "controller.php?q="+menuID+"&requestType=loadPlant";
	var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"searchPlant="+searchPlant,
			async:false,
			success:function(json){
					$('#frmPlant #txtPlant').val(json.plantName);
					$('#frmPlant #txtPlantHead').val(json.plantHead);
					$('#frmPlant #txtPlantHeadPhone').val(json.headContat);
					$('#frmPlant #cboMainCluster').val(json.mainCluster);	
					$('#frmPlant #cboSubCluster').html(json.subCluster);
					if(json.active == 1)	
						$('#frmPlant #chkActive').attr('checked',true);
					else
						$('#frmPlant #chkActive').attr('checked',false);
				
			

			}
		});

		
}

function alertx()
{
	$('#frmPlant #butSave').validationEngine('hide')	;	
}
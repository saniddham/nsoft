<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php 
try{
	$userId 			= $sessions->getUserId();
	$locationId	  		= $sessions->getLocationId();
	$companyId			= $sessions->getCompanyId();
	$menuID				= '1119';
	$programCode		= 'P1119';
	$requesType			= $_REQUEST['requestType'];
	$editMode			= false;
	
	include_once "class/tables/sys_main_clusters.php";				$sys_main_clusters 		= new sys_main_clusters($db);
	include_once "class/tables/sys_sub_clusters.php";				$sys_sub_clusters 		= new sys_sub_clusters($db);
	include_once "class/cls_commonFunctions_get.php";
	include_once "class/tables/mst_plant.php";						$mst_plant 				= new mst_plant($db);
	include_once "class/tables/menupermision.php";					$menupermision			= new menupermision($db);
	
	if($requesType == 'loadSubCluster')
	{
		$mainCluster			= $_REQUEST['maincluster'];
		$result					= $sys_sub_clusters->getCombo('',"MAIN_CLUSTER='$mainCluster'");
		$response['subCluster']	= $result;
	}
	else if($requesType == 'loadPlantCombo')
	{
		$result					= $mst_plant->getCombo();
		$response['plant']	= $result;
	}
	else if($requesType == 'savePlant')
	{
		$dataArr		= json_decode($_REQUEST['dataArr'],true);	
		$searchPlant	= $dataArr['searchPlant'];
		$plantName		= $dataArr['plantName'];
		$anualTraget	= $dataArr['anualTraget'];
		$monthlyTraget	= $dataArr['monthlyTraget'];
		$dailyTraget	= $dataArr['dailyTraget'];
		$plantHeadName	= $dataArr['plantHeadName'];
		$HeadContact	= $dataArr['HeadContact'];
		$mainCluster	= $dataArr['mainCluster'];
		$subCluster		= ($dataArr['subCluster']==''?'NULL':$dataArr['subCluster']);
		$active			= $dataArr['active'];
		
		$db->begin();
		$menupermision->set($programCode,$userId);
		$permissionArr	= $menupermision->checkMenuPermision('Edit',NULL,NULL);
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);
			
		if($searchPlant == '')
		{
			$result		= $mst_plant->insertRec($plantName,$active,$plantHeadName,$HeadContact,$subCluster,$mainCluster);
			if($result['type']=='fail')
				throw new Exception($result['msg']);
				
		}
		else
		{
			$editMode 	= true;	
			$data = array('strPlantName'=>$plantName 
					,'intStatus'=>$active 
					,'strPlantHeadName'=>$plantHeadName 
					,'strPlantHeadContactNo'=>$HeadContact 
					,'SUB_CLUSTER_ID'=>$subCluster 
					,'MAIN_CLUSTER_ID'=>$mainCluster 
					);
			$result	  	= $mst_plant->update($data,"intPlantId='$searchPlant'");
			if($result['type'] == 'fail')
				throw new Exception($result['msg']);
		}
		
		$db->commit();
		$response['type'] 		= "pass";
		if($editMode)
			$response['msg'] 		= "Updated Successfully.";
		else
			$response['msg'] 		= "Saved Successfully.";
		$db->disconnect();
	}
	else if($requesType == 'loadPlant')
	{
		$searchPlant 	= $_REQUEST['searchPlant'];	
		$mst_plant->set($searchPlant);
		$response['plantName']		= $mst_plant->getstrPlantName();
		$response['active']			= $mst_plant->getintStatus();
		$response['plantHead']		= $mst_plant->getstrPlantHeadName();
		$response['headContat']		= $mst_plant->getstrPlantHeadContactNo();
		$response['mainCluster']	= $mst_plant->getMAIN_CLUSTER_ID();
		$mainClusterId				= $mst_plant->getMAIN_CLUSTER_ID();
		$response['subCluster']		= $sys_sub_clusters->getCombo($mst_plant->getSUB_CLUSTER_ID(),"MAIN_CLUSTER='$mainClusterId'");
		//$result						= $sys_sub_clusters->getCombo($mst_plant->getSUB_CLUSTER_ID(),"MAIN_CLUSTER='$mainCluster'");
		
		
	}
}

catch(Exception $e)
{
	$db->rollback();
	$response['msg'] 	=  $e->getMessage();
	$response['error'] 	=  $error_handler->jTraceEx($e);
	$response['type']	=  'fail';
	$response['sql']	=  $db->getSql();
}
echo json_encode($response);
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "class/tables/mst_plant.php";					$mst_plant 				= new mst_plant($db);
include_once "class/tables/sys_main_clusters.php";			$sys_main_clusters 		= new sys_main_clusters($db);
include_once "class/tables/sys_sub_clusters.php";			$sys_sub_clusters 		= new sys_sub_clusters($db);

?>
<title>Plants</title>
<form id="frmPlant" name="frmPlant" method="post" autocomplete="off">
<div align="center">
		<div class="trans_layoutS">
		  <div class="trans_text">Plants</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0" align="center">
      <tr>
        <td width="62%">
          <table width="100%" border="0" class="">
          <tr>
            
            <td colspan="4" class="normalfnt"><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr class="normalfnt">
                <td width="41%" height="40">&nbsp;Plant</td>
                <td width="59%"><select name="cboPlant" class="txtbox" id="cboPlant" style="width:252px" tabindex="1">
                  <?php 
					echo $mst_plant->getCombo();	
                ?>
                </select></td>
              
              </tr>
            </table></td>
          </tr>
         
          <tr>
            <td class="normalfnt">Plant Name&nbsp;<span class="compulsoryRed">*</span></td>
            <td><input name="txtPlant" type="text" class="txtbox validate[required]" id="txtPlant" style="width:200px" tabindex="2"/></td>
           
          </tr>
          <tr>
          	 <td  class="normalfnt">Plant Head Name&nbsp;<span class="compulsoryRed">*</span></td>
            <td class="normalfnt"><input name="txtPlantHead" type="text" class="validate[required,maxSize[50]]" id="txtPlantHead" style="width:200px" tabindex="3"/></td>
          </tr>
          <tr>
            <td class="normalfnt">Plant Head Contact No &nbsp;</td>
            <td class="normalfnt"><input name="txtPlantHeadPhone" type="text" id="txtPlantHeadPhone" style="width:200px" tabindex="3"/></td>
          </tr>
          <tr>
            <td><span class="normalfnt">Main Cluster<span class="compulsoryRed">*</span></span></td>
            <td class="normalfnt"><select class="validate[required]" id="cboMainCluster" name="cboMainCluster" style="width:150px">
            <?php echo  $sys_main_clusters->getCombo(); ?>
            </select></td>
          </tr>
          <tr>
           
            <td class="normalfnt">Sub Cluster&nbsp;</td>
            <td><select id="cboSubCluster" name="cboSubCluster" style="width:150px">
            </select></td>
          </tr>
          <tr>
            <td class="normalfnt">Active</td>
            <td class="normalfnt"><input type="checkbox" name="chkActive" id="chkActive" checked="checked" tabindex="5"/></span></td>
          </tr>
          
        </table>
          <table width="100%" border="0" > 
          
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><a class="button white medium" id="butNew" name="butNew" style="display:" tabindex="26">New</a><?php if($form_permision['add']||$form_permision['edit']){?><a class="button white medium" id="butSave" name="butSave" style="display:" tabindex="24">Save</a><?Php }?><a href="main.php" class="button white medium" id="butClose" name="butClose" style="display:" tabindex="27">Close</a></td>
              </tr>
            </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
</form>
<?php
$locationId = $_SESSION['CompanyID'];
include "include/javascript.html";
?>
<title>Company Details</title>
<table width="900" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td colspan="3"><?php include "presentation/reportHeader.php"; ?></td>
  </tr>
  <tr>
    <td width="33">&nbsp;</td>
    <td colspan="2" class="reportHeader">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2" align="center" class="reportHeader">Company Details</td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">
        <?php
  	$sql = "SELECT
			mst_companies.intId,
			mst_companies.strCode,
			mst_companies.strName,
			mst_companies.intCountryId,
			mst_companies.strWebSite,
			mst_companies.strRemarks,
			mst_companies.strAccountNo,
			mst_companies.strRegistrationNo,
			mst_companies.strVatNo,
			mst_companies.strSVatNo,
			mst_companies.intBaseCurrencyId
			FROM mst_companies
			WHERE
			mst_companies.intStatus =  '1'
			ORDER BY
			mst_companies.strName ASC
			";
			
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
	?>
     <table class="pageBreak" width="100%" border="0" cellspacing="0" cellpadding="0">
     <tr>
        <td colspan="2" class="reportSubHeader"><?php echo $row['strCode'].' - '.$row['strName']; ?></td>
        <td>&nbsp;</td>
     </tr>
      <?php
      $companyId = $row['intId'];
		$sql2 = "SELECT
				mst_locations.strCode,
				mst_locations.strName,
				mst_locations.intCompanyId,
				mst_locations.strAddress,
				mst_locations.strStreet,
				mst_locations.strCity,
				mst_locations.strDistrict,
				mst_locations.strPhoneNo,
				mst_locations.strFaxNo,
				mst_locations.strEmail,
				mst_locations.strZip,
				mst_locations.strRemarks,
				mst_locations.intStatus
				FROM mst_locations
				WHERE
				mst_locations.intCompanyId =  '$companyId'
				";
			?>
            <?php
			$result2 = $db->RunQuery($sql2);
			while($row2=mysqli_fetch_array($result2))
			{
			?>
            <tr>
    <td>&nbsp;</td>
    <td colspan="2" bgcolor="#E1F8FF" class="normalfnt"><strong><?php echo $row2['strCode'].' - '.$row2['strName']; ?></strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td width="200" class="normalfnt">Address</td>
    <td width="667" class="normalfnt">: <?php echo $row2['strAddress']; ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Street</td>
    <td><span class="normalfnt">: <?php echo $row2['strStreet']; ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">City</td>
    <td><span class="normalfnt">: <?php echo $row2['strCity']; ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">District</td>
    <td><span class="normalfnt">: <?php echo $row2['strDistrict']; ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Phone </td>
    <td><span class="normalfnt">: <?php echo $row2['strPhoneNo']; ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Fax</td>
    <td><span class="normalfnt">: <?php echo $row2['strFaxNo']; ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Email</td>
    <td><span class="normalfnt">: <?php echo $row2['strEmail']; ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Zip Code</td>
    <td><span class="normalfnt">: <?php echo $row2['strZip']; ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Remarks</td>
    <td><span class="normalfnt">: <?php echo $row2['strRemark']; ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Status</td>
    <td><span class="normalfnt">: <?php echo ($row2['intStatus']==1?'Active':'Inactive'); ?></span></td>
  </tr>
   <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
	<?php
			}
	?>
     </table>
    <?php
	}
	?>
    </td>
  </tr>

  <tr>
    <td>&nbsp;</td>
    <td width="200">&nbsp;</td>
    <td width="667">&nbsp;</td>
  </tr>
</table>

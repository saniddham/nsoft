<?php
date_default_timezone_set('Asia/Kolkata');
//ini_set('allow_url_include',1);
// ini_set('display_errors',1);
session_start();

$backwardseperator 	= "../../../";
$thisFilePath 		=  $_SERVER['PHP_SELF'];
include_once $backwardseperator."dataAccess/Connector.php";

include_once "../../../libraries/jqdrid/inc/jqgrid_dist.php";

$company 	= $_SESSION['headCompanyId'];
$location 	= $_SESSION['CompanyID'];

$sql = "select * from(SELECT 
mst_supplier.strCode as code,
mst_supplier.strName as name,
mst_supplier.strAddress as address,
mst_supplier.strContactPerson as contact_person,
mst_supplier.strPrintOnCheque as print_on_cheque,
mst_supplier.strCity as city,
mst_country.strCountryName as country,
mst_financecurrency.strDescription as description,
mst_supplier.strPhoneNo as phone_no,
mst_supplier.strMobileNo as mobile_no,
mst_supplier.strFaxNo as fax_no,
mst_supplier.strEmail as email,
mst_supplier.strWebSite as web_site,
mst_supplier.strIcCode as IC_Code,
mst_supplier.strVatNo as vat_no,
mst_supplier.strSVatNo as Svat_no,
mst_supplier.strRegistrationNo as reg_no,
mst_supplier.strInvoiceType as invoice_type,
mst_supplier.strAccNo as acc_no,
mst_supplier.intCreditLimit as credit_limit,
mst_supplier.strLeadTime as lead_time,
mst_supplier.strBlocked as blocked,
mst_supplier.intRank as rank,
if(mst_supplier.intStatus=1,'active','in-active') as status ,
A.strUserName AS createBy,
mst_supplier.dtmCreateDate as created_date,
b.strUserName AS modifyBy,
mst_supplier.dtmModifyDate as modifiy_date
FROM
mst_supplier
Inner Join sys_users AS A ON A.intUserId = mst_supplier.intCreator
Inner Join mst_country ON mst_country.intCountryID = mst_supplier.intCountryId
Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_supplier.intCurrencyId
Inner Join sys_users AS b ON b.intUserId = mst_supplier.intModifyer) as t
";
					  

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Supplier log";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; 
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes



$jq->set_options($grid);

$jq->select_command =$sql;
//$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SUPPLIER LOG</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />


</head>

<body>	
<form id="frmlisting" name="frmlisting" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
  <link rel="stylesheet" type="text/css" media="screen" href="../../../libraries/jqdrid/js/themes/smoothness/jquery-ui.custom.css"></link>	
  <link rel="stylesheet" type="text/css" media="screen" href="../../../libraries/jqdrid/js/jqgrid/css/ui.jqgrid.css"></link>	
	
  <script src="../../../libraries/jqdrid/js/jquery.min.js" type="text/javascript"></script>
  <script src="../../../libraries/jqdrid/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
  <script src="../../../libraries/jqdrid/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
  <script src="../../../libraries/jqdrid/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
  <script src="../../../libraries/javascript/script.js" type="text/javascript"></script>
    
    <td><table width="100%" border="0">
      <tr>
        <td>
        <div align="center" style="margin:10px">
        
			<?php echo $out?>
        </div>
        </td>
      </tr>

    </table></td>
    </tr>


</form>
</body>
</html>
<?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(ware_invoiceheader.intApproveLevels) AS appLevel
			FROM ware_invoiceheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
//------------------------------function load User Department---------------------
function getUserDepartment($intUser){
	global $db;

	$sqlp = "SELECT
			sys_users.intDepartmentId
			FROM
			sys_users
			WHERE
			sys_users.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['intDepartmentId'];

}
?>


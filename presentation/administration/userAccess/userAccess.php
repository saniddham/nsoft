<?php
require_once("libraries/jqgrid2/inc/jqgrid_dist.php");

$sql = "SELECT
			sys_users.strUserName,
			sys_users.strFullName,
			sys_useraccess.login,
			sys_useraccess.logout,
			sys_useraccess.IP,
			sys_useraccess.browser,
			sys_useraccess.browserVersion,
			sys_useraccess.os
		FROM
		sys_useraccess
		INNER JOIN sys_users ON sys_users.intUserId = sys_useraccess.userID 
		WHERE 1=1";

$jq = new jqgrid('',$db);	
$col = array();

$col["title"] 			= "User Name";
$col["name"] 			= "strUserName";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Full Name";
$col["name"] 			= "strFullName";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "login";
$col["name"] 			= "login";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "logout";
$col["name"] 			= "logout";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "IP";
$col["name"] 			= "IP";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "browser";
$col["name"] 			= "browser";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "browserVersion";
$col["name"] 			= "browserVersion";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "os";
$col["name"] 			= "os";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["hidden"]  		= false;
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "User System Access";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'login'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
include "include/listing.html";
?>
<form id="frmlisting" name="frmlisting" method="post" >
        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
</form>
</body>
</html>
// JavaScript Document
var basePath = 'presentation/administration/programWiseUserParmission/';
var menuId = 813;
$(document).each(function(){
	
	$('#frmMWUserPermision .clsAddPermission').die('click').live('click',loadPermissionAddPopUp);
	$('#frmAddPermission #butClosePermPopup').die('click').live('click',cloasePermissionAddPopUp);
	$('#frmAddPermission #butClosePermPopup2').die('click').live('click',cloasePermissionAddPopUp2);
	$('#frmSpecialPermission #butCloseSPermPopup').die('click').live('click',closeSPermPopup);
	$('#frmAddPermission #butAddUsers').die('click').live('click',addUsers);
	$('.cboUserformError').die('click').live('click',closeError);
	$('#frmAddPermission .clsCheckUserPermission').die('click').live('click',addUserPermission)
	$('#frmMWUserPermision .butPermision').die('click').live('click',loadSpecialPermissionPopUp);
	$('#frmSpecialPermission .clsAddSP').die('click').live('click',loadSpecialPermissionAddPopUp);
	
});
function loadPermissionAddPopUp()
{
	popupWindow3('1');
	
	var menuId			= $(this).parent().parent().attr('id');
	var permissionType	= $(this).attr('id');
	var menuPermType	= 'normal';
	
	$('#popupContact1').load(basePath+'permissionAddPopup.php?permissionType='+permissionType+'&menuId='+menuId+'&menuPermType='+menuPermType+'&popupType=direct');
}
function loadSpecialPermissionPopUp()
{
	popupWindow3('1');
	
	var menuId			= $(this).attr('id');
	$('#popupContact1').load(basePath+'specialPermissionPopup.php?SMenuId='+menuId);
}
function reloadPage(type)
{
 	//var cboMainModules	=$('#frmMWUserPermision #cboMainModules').val();
	window.frmMWUserPermision.submit();
	//if(cboMainModules != '')
	//window.location.href = '?q='+menuId+'&cboMainModules='+cboMainModules;
}
function cloasePermissionAddPopUp()
{
	disablePopup();
}
function cloasePermissionAddPopUp2()
{
	hideWaiting();
}
function closeSPermPopup()
{
	disablePopup();
}
function addUsers()
{
	var userId		= $('#frmAddPermission #cboUser').val();
	var userName	= $("#frmAddPermission #cboUser option:selected").text();
	var chkUser		= true;
	
	if(userId=='')
	{
		$('#frmAddPermission #cboUser').validationEngine('showPrompt','Please select User.','fail');
		return;
	}
	
	$('#frmAddPermission .clsCheckUserPermission').each(function(index, element) {
        
		if($(this).parent().parent().attr('id')==userId)
			chkUser = false;
    });
	
	if(chkUser)
		addNewRow(userId,userName);
	
}
function addNewRow(userId,userName)
{
	var tbl 		= document.getElementById('tblAddPopup');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);
	row.id			= userId;
	
	var cell 		= row.insertCell(0);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML 	= "<input type=\"checkbox\" id=\"chkUserId\" name=\"chkUserId\" class=\"clsCheckUserPermission\" />";
	
	var cell 		= row.insertCell(1);
	cell.setAttribute("style",'text-align:left;color:#000000');
	cell.innerHTML 	= userName;
}
function addUserPermission()
{
	var permission		= ($(this).attr('checked')?1:0);
	var userId			= $(this).parent().parent().attr('id');
	var menuId			= $('#frmAddPermission #hidMenuId').val();
	var permissionType	= $('#frmAddPermission #hidPermissionType').val();
	var menuPermType	= $('#frmAddPermission #hidMenuPermType').val();
	
	var url = basePath+"moduleWiseUserPermission_db.php?requestType=updateUserPermission&userId="+userId+"&menuId="+menuId+"&permissionType="+permissionType+'&permission='+permission+'&menuPermType='+menuPermType;
	$.ajax({url:url,type:'POST',async:false});
}
function loadSpecialPermissionAddPopUp()
{
	showWaiting();
	var menuId			= $(this).parent().parent().attr('id');
	var menuPermType	= 'special';
	$('#divBackgroundImg').load(basePath+'permissionAddPopup.php?menuId='+menuId+'&menuPermType='+menuPermType+'&popupType=indirect');
}
function closeError()
{
	$('#frmAddPermission #cboUser').validationEngine('hide') ;
}
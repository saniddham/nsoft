<?php
session_start();
$backwardseperator 	= "../../../";
$mainPath 			= $_SESSION['mainPath'];
$thisFilePath 		= $_SERVER['PHP_SELF'];
$companyId			= $_SESSION["headCompanyId"];

include $backwardseperator."dataAccess/Connector.php";
include "../../../class/administration/programWiseUserParmission/cls_mwUserPermission_get.php";

$objpop_mwUserPermission_get = new cls_mwUserPermission_get($db);

$permissionType 	= $_REQUEST['permissionType'];
$menuId 			= $_REQUEST['menuId'];
$menuPermType 		= $_REQUEST['menuPermType'];
$popupType			= $_REQUEST['popupType'];

?>
<title>Add Permissions</title>

    
<form id="frmAddPermission" name="frmAddPermission" method="post" action="">
<table width="450" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutS" style="width:450px">
		  <div class="trans_text"> Add Permissions</div>
            <table width="100%" border="0" align="center">
            <tr class="normalfnt">
            	<td width="17%">User</td>
            	<td width="53%"><select name="cboUser" id="cboUser" style="width:200px">
                <?php
					$result	= $objpop_mwUserPermission_get->loadUsers($companyId,$menuId,$menuPermType);
					echo "<option ></option>" ;
					while($row = mysqli_fetch_array($result))
					{
						echo "<option value=\"". $row["intUserId"] ."\">" . trim($row["strUserName"]) ."</option>" ;
					} 	
				?>
          	    </select></td>
            	<td width="30%" style="text-align:left"><a class="button green small" id="butAddUsers">Add User</a></td>
            </tr>
            <tr class="normalfnt">
              <td><input type="hidden" id="hidMenuId" value="<?php echo $menuId; ?>"></td>
              <td><input type="hidden" id="hidPermissionType" value="<?php echo $permissionType; ?>"></td>
              <td style="text-align:left"><input type="hidden" id="hidMenuPermType" value="<?php echo $menuPermType; ?>"></td>
            </tr>
            <tr>
            	<td colspan="3">
                <div style="width:450px;height:300px;overflow:scroll" >
                    <table width="100%" class="bordered" id="tblAddPopup" >
                    <thead>
                    <tr>
                    	<th width="5%" height="22" style="color:#000000" >select</th>
                    	<th style="color:#000000" >User Name</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    $result    = $objpop_mwUserPermission_get->loadPermissionDetails($menuPermType,$permissionType,$menuId,$companyId);
                    while($row = mysqli_fetch_array($result))
                    {
                    ?>
                    	<tr class="normalfnt" id="<?Php echo $row['intUserId']; ?>" >
                    		<td align="center" ><input type="checkbox" id="chkUserId" name="chkUserId" class="clsCheckUserPermission" checked="checked" /></td>
                    		<td style="color:#000000" ><?php echo $row['strUserName'];?></td>
                        </tr>
                    
                    <?php
                    }
                    ?>
                    </tbody>
                    </table>
          		</div>
                </td>
            </tr>
            <tr>
            <td colspan="3">
                <table width="100%" border="0">
                    <tr>
                        <td height="25" align="center" class="tableBorder_allRound"><a class="button white medium" <?php echo ($popupType=='direct'?'id="butClosePermPopup"':'id="butClosePermPopup2"'); ?>>Close</a></td>
                    </tr>
                </table>
            </td>
    		</tr>
 	 </table>
  	</div>
  </div>
</form>
</body>
</html>

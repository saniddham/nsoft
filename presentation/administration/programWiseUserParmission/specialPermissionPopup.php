<?php
session_start();
$backwardseperator 	= "../../../";
$mainPath 			= $_SESSION['mainPath'];
$thisFilePath 		= $_SERVER['PHP_SELF'];
$companyId			= $_SESSION["headCompanyId"];

include $backwardseperator."dataAccess/Connector.php";
include "../../../class/administration/programWiseUserParmission/cls_mwUserPermission_get.php";

$objpop_mwUserPermission_get = new cls_mwUserPermission_get($db);

$SMenuId 			= $_REQUEST['SMenuId'];

?>
<title>Special Permissions</title>

    
<form id="frmSpecialPermission" name="frmSpecialPermission" method="post" action="">
<table width="450" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
	<div class="trans_layoutS" style="width:450px">
	  <div class="trans_text"> Special Permissions</div>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
          <tr class="normalfnt">
            <td width="17%"><input type="hidden" id="hidSpecialMenuId" value="<?php echo $SMenuId; ?>"></td>
            <td width="53%"></td>
            <td width="30%" style="text-align:left">&nbsp;</td>
          </tr>
          <tr>
           	  <td colspan="3">
                <div style="width:450px;height:300px;overflow:scroll" >
                    <table width="100%" class="bordered" id="tblAddSPopup" >
                    <thead>
                    <tr>
                    	<th width="81%" height="22" >Special Permissions</th>
                    	<th width="19%" >&nbsp;</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    $result    = $objpop_mwUserPermission_get->loadSpecialPermissionDetails($SMenuId,$companyId);
                    while($row = mysqli_fetch_array($result))
                    {
                    ?>
                    	<tr class="normalfnt" id="<?Php echo $row['intId']; ?>" >
                    		<td bgcolor="#FFFFFF" >&nbsp;<?php echo $row['strPermisionType'];?></td>
                    		<td bgcolor="#FFFFFF" style="text-align:center" ><a class="button green small clsAddSP" id="butaddSP">Add</a></td>
                        </tr>
                    
                    <?php
                    }
                    ?>
                    </tbody>
                    </table>
          		</div>
              </td>
          </tr>
          <tr>
            <td colspan="3">
                <table width="100%" border="0">
                    <tr>
                        <td height="25" align="center" class="tableBorder_allRound"><a class="button white medium" id="butCloseSPermPopup">Close</a></td>
                    </tr>
                </table>
            </td>
   		  </tr>
 	 </table>
  	</div>
  </div>
</form>
</body>
</html>

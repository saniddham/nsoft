<?php
session_start();
$mainPath 		= $_SESSION['mainPath'];
$requestType 	= $_REQUEST['requestType'];

include "../../../dataAccess/Connector.php";
include "../../../class/administration/programWiseUserParmission/cls_mwUserPermission_get.php";
include "../../../class/administration/programWiseUserParmission/cls_mwUserPermission_set.php";

$obj_mwUserPermission_get = new cls_mwUserPermission_get($db);
$obj_mwUserPermission_set = new cls_mwUserPermission_set($db);

if($requestType=='updateUserPermission')
{
	$userId			= $_REQUEST['userId'];
	$menuId			= $_REQUEST['menuId'];
	$permissionType	= $_REQUEST['permissionType'];
	$permission		= $_REQUEST['permission'];
	$menuPermType	= $_REQUEST['menuPermType'];

	$checkPermissionAdd = $obj_mwUserPermission_get->checkPermissionAdd($userId,$menuId,$menuPermType);
	
	//--------------------
	$id	= $menuId;
	/*	if($permission==0)
		{
			$sql_m =" SELECT
			m1.intId as grand1,
			m2.intId as grand2,
			m3.intId as grand3,
			m3.intId as grand4   
			FROM
			menus as m1
			left JOIN menus as m2  ON m1.intId = m2.intParentId
			left JOIN menus as m3  ON m2.intId = m3.intParentId
			left JOIN menus as m4  ON m3.intId = m4.intParentId
			
			WHERE
			m1.intParentId = '$id' ";
			$result_m 		= $db->RunQuery($sql_m);
			while($row_m 	= mysqli_fetch_array($result_m)){
				
				if($row_m['grand4'] >0){
				$sql = "DELETE FROM `menupermision` WHERE (`intMenuId`='".$row_m['grand4']."') AND (`intUserId`='$userId')  ";
				$result = $db->RunQuery($sql);
				}
				if($row_m['grand3'] >0){
				$sql = "DELETE FROM `menupermision` WHERE (`intMenuId`='".$row_m['grand3']."') AND (`intUserId`='$userId')  ";
				$result = $db->RunQuery($sql);
				}
				if($row_m['grand2'] >0){
				$sql = "DELETE FROM `menupermision` WHERE (`intMenuId`='".$row_m['grand2']."') AND (`intUserId`='$userId')  ";
				$result = $db->RunQuery($sql);
				}
				if($row_m['grand1'] >0){
				$sql = "DELETE FROM `menupermision` WHERE (`intMenuId`='".$row_m['grand1']."') AND (`intUserId`='$userId')  ";
				$result = $db->RunQuery($sql);
				}
				
				
			}
			$sql = "DELETE FROM `menupermision` WHERE (`intMenuId`='$id') AND (`intUserId`='$userId')  ";
			 $result = $db->RunQuery($sql);
		}
		else
		{ 
			$sql_m =" SELECT
						m1.intId as grand1,
						m2.intId as grand2,
						m3.intId as grand3,
						m3.intId as grand4   
						FROM
						menus as m1
						left JOIN menus as m2  ON m1.intParentId = m2.intId
						left JOIN menus as m3  ON m2.intParentId = m3.intId
						left JOIN menus as m4  ON m3.intParentId = m4.intId
						WHERE
						m1.intId = '$id' ";
			$result_m 		= $db->RunQuery($sql_m);
			while($row_m 	= mysqli_fetch_array($result_m)){
				
				if($row_m['grand4'] >0){
				$sql = "INSERT INTO `menupermision` (`intMenuId`,`intUserId`) VALUES ('".$row_m['grand4']."','$userId')";
				$result = $db->RunQuery($sql);
				}
				if($row_m['grand3'] >0){
				$sql = "INSERT INTO `menupermision` (`intMenuId`,`intUserId`) VALUES ('".$row_m['grand3']."','$userId')";
				$result = $db->RunQuery($sql);
				}
				if($row_m['grand2'] >0){
				$sql = "INSERT INTO `menupermision` (`intMenuId`,`intUserId`) VALUES ('".$row_m['grand2']."','$userId')";
				$result = $db->RunQuery($sql);
				}
				if($row_m['grand1'] >0){
				$sql = "INSERT INTO `menupermision` (`intMenuId`,`intUserId`) VALUES ('".$row_m['grand1']."','$userId')";
				$result = $db->RunQuery($sql);
				}
				
				
			}
		 }*/
	//-------------------
	
	
	if($checkPermissionAdd)
	{
		$dataArr	= $obj_mwUserPermission_set->updateUserPermission($userId,$menuId,$permissionType,$permission,$menuPermType);
		if($dataArr['savedStatus']=='fail')
		{
			$response['type'] 		= "fail";
			$response['msg'] 		= $dataArr['savedMassege'];
			$response['sql'] 		= $dataArr['error_sql'];
		}
	}
	else
	{
		$dataArr	= $obj_mwUserPermission_set->insertUserPermission($userId,$menuId,$menuPermType);
		if($dataArr['savedStatus']=='fail')
		{
			$response['type'] 		= "fail";
			$response['msg'] 		= $dataArr['savedMassege'];
			$response['sql'] 		= $dataArr['error_sql'];
		}
		else
		{
			if($menuPermType=='normal')
			{
				$dataArr	= $obj_mwUserPermission_set->updateUserPermission($userId,$menuId,$permissionType,$permission);
				if($dataArr['savedStatus']=='fail')
				{
					$response['type'] 		= "fail";
					$response['msg'] 		= $dataArr['savedMassege'];
					$response['sql'] 		= $dataArr['error_sql'];
				}
			}
		}	
	}
	echo json_encode($response);
}
?>
<?php
//ini_set('max_execution_time',600);

//include "dataAccess/Connector.php";
include_once "class/administration/programWiseUserParmission/cls_mwUserPermission_get.php";
$obj_mwUserPermission_get = new cls_mwUserPermission_get($db);

//include "include/javascript.html";
?>
<title>Module Wise User Permision</title>
<form name="frmMWUserPermision" id="frmMWUserPermision" method="post">
   <div>
	<div align="left">
		<div class="trans_layoutXL" >
			<div class="trans_text">Module Wise User-Menu Permission</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
  <tr>
    <td><table width="100%" border="0">
      	<tr>
        <td width="14%" height="22" class="normalfnt">Main Modules</td>
        <td width="35%" class="normalfnt">
        <select name="cboMainModules" onChange="reloadPage('M');" class="txtbox" id="cboMainModules" style="width:150px" >
		<?php
			$intMainModules = $_REQUEST['cboMainModules'];
			$result 		= $obj_mwUserPermission_get->getGridMainModules('');
			
			echo "<option ></option>" ;
			while($row = mysqli_fetch_array($result))
			{
				if($intMainModules==$row["intId"])
					echo "<option value=\"". $row["intId"] ."\" selected=\"selected\">" . trim($row["strName"]) ."</option>" ;
				else
					echo "<option value=\"". $row["intId"] ."\">" . trim($row["strName"]) ."</option>" ;
			} 
        ?>
        </select></td>
        <td width="11%" class="normalfnt">&nbsp;</td>
        <td width="40%" class="normalfnt">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0">
      <tr>
        <td  >&nbsp;</td>
        </tr>
      <tr>
        <td class="normalfnt">
        <?php
		if($intMainModules !=''){
		?>
          <table width="408" border="0" id="tblMainGrn" bgcolor="#E5E5E5" class="bordered" >
			<?php
		//	if($intMainModules=='') die(); 
			
			$result = $obj_mwUserPermission_get->getGridMainModules($intMainModules);
			while($row=mysqli_fetch_array($result))
			{
				$mainId = $row["intId"];
			?>
              <tr id="<?php echo $mainId; ?>" bgcolor="#E1F5FF" class="normalfnt" >
              	<th height="20" colspan="4" align="center" ><div style="text-align:left;width:300px"><?php echo $row['strName']; ?></div></th>
                  
            	<?php 
				$result_permision 	= $obj_mwUserPermission_get->getPermissionTypes($_SESSION['Database']);
				$permisionCount		= 0;
				while($row_permision = mysqli_fetch_array($result_permision))
				{
					$permisionCount++;
				?>
			 			<th  style="width:50px" ><?php echo $row_permision['name']; ?></th>
				<?php
				}
				?>
              </tr>
                	<?php 		
					$result2 = $obj_mwUserPermission_get->getSubModules($mainId);
					while($row2 = mysqli_fetch_array($result2))
					{
						$subId1 	= $row2["intId"];
					////////////////////////////// get special permision count ////////////////////
						$resultsp 	= $obj_mwUserPermission_get->getSpecialPermissionCount($subId1);
						$spcount 	= mysqli_num_rows($resultsp);
					///////////////////////////////////////////////////////////////////////////////
					
					?>
                  		<tr id="<?php echo $subId1; ?>"  <?php echo ($row2['strURL']!='#'?'bgcolor="#FFFFFF"':'') ?>>
                    		<td width="2%"><div style="width:18px"></div></td>
                    		<td colspan="3"><?php echo $row2['strName'];
							if($spcount>0)
							{
							?>
                            	<img  id="<?php echo $subId1; ?>" src="images/add_new.png" align="right" class="butPermision mouseover"/>
							<?php 
							} 
							?></td>
                    <?php 
					$result_permision 	= $obj_mwUserPermission_get->getPermissionTypes($_SESSION['Database']);
					$permisionCount		= mysqli_num_rows($result_permision);
					$permision_fields	= '';
			
					while($row_permision=mysqli_fetch_array($result_permision))
					{
						$permision_fields  .= $row_permision['column_name'].',';
						$fieldArray[] 		= $row_permision['column_name'];
					}
		 			$permision_fields 	= substr($permision_fields,0,strlen($permision_fields)-1);
					$result_permision 	= $obj_mwUserPermission_get->checkPermission($permision_fields,$subId1);
					$R = 0;
			
					while($row_permision=mysqli_fetch_array($result_permision))
					{
				//$colLength =  mysqli_field_len($result_permision,0);
						if($R==0)
						{
							for($x = 0;$x<$permisionCount;$x++)
							{
								$xrow1[$x] = $row_permision[$x];
							}
						}
						if($R==1)
						{
							for($x = 0;$x<$permisionCount;$x++)
							{
								$xrow2[$x] = $row_permision[$x];
						
					?>
                            	<td class="normalfntMid"><a class="button green small clsAddPermission" id="<?php echo $fieldArray[$x]; ?>" style="visibility:<?php echo ($xrow1[$x]==0?'hidden':''); ?>">View</a></td>
                        
                    <?php
							}
						}
						$R++;
					}
			 		?>        
                    </tr>
                  	
					<?php 
						$result3 = $obj_mwUserPermission_get->getSubModules($subId1);
						while($row3 = mysqli_fetch_array($result3))
						{
							$subId2 	= $row3["intId"];
					
					////////////////////////////// get special permision count ////////////////////
							$resultsp 	= $obj_mwUserPermission_get->getSpecialPermissionCount($subId2);
							$spcount  	= mysqli_num_rows($resultsp);
					///////////////////////////////////////////////////////////////////////////////
					?>
                  			<tr id="<?php echo $subId2; ?>" <?php echo ($row3['strURL']!='#'?'bgcolor="#FFFFFF"':'') ?>>
                    			<td width="2%"><div style="width:18px"></div></td>
                    			<td width="2%"><div style="width:18px"></div></td>
                    			<td colspan="2"><?php echo $row3['strName']; 
								if($spcount>0)
								{
								?>
                                	<img  id="<?php echo $subId2; ?>" src="images/add_new.png" align="right" class="butPermision mouseover"/>		
								<?php 
								} 
								?>
                                </td>
					<?php 
                    
                            $result_permision 	= $obj_mwUserPermission_get->getPermissionTypes($_SESSION['Database']);
                            $permisionCount		= mysqli_num_rows($result_permision);
                            $permision_fields	= '';
                            while($row_permision = mysqli_fetch_array($result_permision))
                            {
                                $permision_fields .= $row_permision['column_name'].',';
                                $fieldArray[] = $row_permision['column_name'];
                            }
                            
                            $permision_fields = substr($permision_fields,0,strlen($permision_fields)-1);
                            $result_permision = $obj_mwUserPermission_get->checkPermission($permision_fields,$subId2);
                            $R = 0;
                            while($row_permision=mysqli_fetch_array($result_permision))
                            {
                                if($R==0)
                                {
                                    for($x = 0;$x<$permisionCount;$x++)
                                    {
                                        $xrow1[$x] = $row_permision[$x];
                                    }
                                }
                                if($R==1)
                                {
                                    for($x = 0;$x<$permisionCount;$x++)
                                    {
                                        $xrow2[$x] = $row_permision[$x];
                        
                    ?>
                            	<td class="normalfntMid"><a class="button green small clsAddPermission" id="<?php echo $fieldArray[$x]; ?>" style="visibility:<?php echo ($xrow1[$x]==0?'hidden':''); ?>">View</a></td>
                        
                    <?php
									}
								}
								$R++;
							}
			 		?>
                    		</tr>
                  	<?php 		
							$result4 = $obj_mwUserPermission_get->getSubModules($subId2);
							while($row4=mysqli_fetch_array($result4))
							{
								$subId3 	= $row4['intId'];
						////////////////////////////// get special permision count ////////////////////
								$resultsp 	= $obj_mwUserPermission_get->getSpecialPermissionCount($subId3);
								$spcount 	= mysqli_num_rows($resultsp);
						///////////////////////////////////////////////////////////////////////////////
						
						?>
							<tr id="<?php echo $subId3; ?>"  <?php echo ($row4['strURL']!='#'?'bgcolor="#FFFFFF"':'') ?>>
								<td width="2%"><div style="width:18px"></div></td>
								<td width="2%"><div style="width:18px"></div></td>
								<td width="2%"><div style="width:18px"></div></td>
						
								<td><?php echo $row4['strName']; 
								if($spcount>0)
								{
								?>
									<img  id="<?php echo $subId3; ?>" src="images/add_new.png" align="right" class="butPermision mouseover"/>
								<?php 
								} 
								?>
								</td>
						<?php 
								$result_permision 	= $obj_mwUserPermission_get->getPermissionTypes($_SESSION['Database']);
								$permisionCount		= mysqli_num_rows($result_permision);
								$permision_fields	= '';
							
								while($row_permision=mysqli_fetch_array($result_permision))
								{
									$permision_fields  .= $row_permision['column_name'].',';
									$fieldArray[] 		= $row_permision['column_name'];
								}
							
								$permision_fields = substr($permision_fields,0,strlen($permision_fields)-1);
								$result_permision = $obj_mwUserPermission_get->checkPermission($permision_fields,$subId3);
								$R = 0;
								while($row_permision = mysqli_fetch_array($result_permision))
								{
									if($R==0)
									{
										for($x = 0;$x<$permisionCount;$x++)
										{
											$xrow1[$x] = $row_permision[$x];
										}
									}
									if($R==1)
									{
										for($x = 0;$x<$permisionCount;$x++)
										{
											$xrow2[$x] = $row_permision[$x];
						
							?>
									<td class="normalfntMid"><a class="button green small clsAddPermission" id="<?php echo $fieldArray[$x]; ?>" style="visibility:<?php echo ($xrow1[$x]==0?'hidden':''); ?>">View</a></td>
								
							<?php
										}
									}
									$R++;
								}
						?>
							</tr>
					  <?php
							}
						}
					}
				}
			 ?>
          </table>
	  <?php
		}
      ?>
          </td>
        </tr>
    </table></td>
  </tr>
</table>
</div>
</div>
</div>
</form>
	<div style="width:900px; position: absolute;display:none;z-index:100" id="popupContact1"></div>
   
    <div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
    
    <div style="width:900px; position: absolute;display:none;z-index:101" id="popupContact2"></div>
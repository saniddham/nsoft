<?php
$backwardseperator = "../";

$userId			= '';
$oldPswd 		= '';
$newPswd 		= '';
$conPswd 		= '';

$userId			= base64_decode($_GET['userId']);
$error_attempts	= base64_decode($_GET['error_attempts']);
$oldPswd 		= $_POST["txtOldPassword"];
$newPswd 		= $_POST["txtNewPassword"];
$conPswd 		= $_POST["txtConfirmPassword"];
$key			= 'I;p&a^d@s!a+m*p%l$e#R{e*q(u&i?s&i#t@i%o^n*S}y:s]t*e&m' ;
$errorMsg		= '';
$disableAcc		= false;
$currPswdError	= false;
$newPswdUsed	= false;

if($userId=='')
{
	session_unset(); 
	session_destroy(); 
	header("Location:?q=login");
}
if(isset($_POST['txtOldPassword']))
{
	if(trim($oldPswd)=='')
	{
		$errorMsg	= 'Current password empty.';
	}
	else if(trim($newPswd)=='')
	{
		$errorMsg	= 'New password empty.';
	}
	else if(trim($conPswd)=='')
	{
		$errorMsg	= 'Confirm New password empty.';
	}
	else if($newPswd!=$conPswd)
	{
		$errorMsg	= 'Confirm New password does not match New password.';
	}
	else
	{
		include $backwardseperator."dataAccess/LoginDBManageriPad.php";
		
		$db 		=  new LoginDBManager();
		$mainDb		= $db->getMainDatabase();	
		
		$sql = "select sys_users.intUserId,
				sys_users.strUserName,
				sys_users.strPassword,
				sys_users.intStatus,
				sys_users.intHigherPermision,
				sys_users.PASSWORD_ERROR_ATTEMPTS_LEFT
				from sys_users
				where sys_users.strPassword =  '".md5(md5(md5($oldPswd.$key)))."' AND
				sys_users.intUserId = '$userId' ";
		
		$result = $db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		if(mysqli_num_rows($result)==0)
		{
			$sql_chk = "SELECT PASSWORD_ERROR_ATTEMPTS_LEFT,
						intStatus,
						intHigherPermision 
						FROM sys_users 
						WHERE intUserId = '$userId' ";
						
			$result_chk 		= $db->RunQuery($sql_chk);
			$row_chk			= mysqli_fetch_array($result_chk);
			$error_atmpt_left	= $row_chk['PASSWORD_ERROR_ATTEMPTS_LEFT'];
		
			if($row_chk['intStatus']!=1 && $row_chk['intHigherPermision']==0 /*&& $systemUser!='root'*/ )
			{ 
				if($errorMsg=='')
				{
					$errorMsg = "Sorry! Your account has been disabled.";
				}
			}
			else
			{ 
				if($error_atmpt_left=='')
				{
					$error_atmpt_left = $error_attempts-1;
				}
				else
				{
					$error_atmpt_left = $error_atmpt_left-1;
				}
	
				$sql = "UPDATE sys_users 
						SET
						PASSWORD_ERROR_ATTEMPTS_LEFT = '$error_atmpt_left' ";
				if($error_atmpt_left<=0)
				{
					$sql.=",intStatus = 10 ";
				}
				$sql .="WHERE
						intUserId = '$userId' ";
				
				$result		= $db->RunQuery($sql);
				if($errorMsg=='')
				{
					if($error_atmpt_left>0)
					{
						$currPswdError = true;
						$errorMsg 	= "Current password do not match. You are left with $error_atmpt_left more attempts";
					}
					else
					{
						$errorMsg 	= "Sorry! Your account has been disabled.";
						$disableAcc = true;
					}
				}
			}
			$db	= NULL;
		}
		else
		{
			if($row['intStatus']!=1 && $row['intHigherPermision']==0 /*&& $systemUser!='root'*/ )
			{	
				
				session_unset(); 
				session_destroy(); 
				if($errorMsg=='')
					$errorMsg = "Sorry! Your account has been disabled.";
				//exit;
			}
			else
			{
				include $backwardseperator."class/administration/ipadPassword/ipad_password_get.php";
				include $backwardseperator."class/administration/ipadPassword/ipad_password_set.php";
				
				$obj_password_get	= new ipad_password_get($db);
				$obj_password_set	= new ipad_password_set($db);
				
				$chkPswdHistry		= $obj_password_get->checkPasswordHistory($newPswd,$userId,$key);
				//$chkPswdHistry = true;
				if($chkPswdHistry)
				{
					session_unset(); 
					session_destroy();
					
					if($errorMsg=='')
						$errorMsg = "New password already used with this account.";
					
					$newPswdUsed = true;
					$db			 = NULL; 
				}
				else
				{
					$sql 	= " UPDATE sys_users 
								SET
								intStatus = '1',
								strPassword = '".md5(md5(md5($newPswd.$key)))."' , 
								PASSWORD_LAST_MODIFIED_DATE = NOW(),
								PASSWORD_ERROR_ATTEMPTS_LEFT = '$error_attempts'
								WHERE
								intUserId = '$userId'";
					$result = $db->RunQuery($sql);
					
					$obj_password_set->savePasswordHistory($userId,$newPswd,$key);
					
					echo 'Password has changed successfully.';
					echo '<br>You are automatically redirec to login page.....';
					sleep(3);
					session_unset(); 
					session_destroy();
					$db	= NULL; 
					header("Location:?q=login");
				}
			}
		}
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Password Change</title>

<link href="../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../css/button.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../libraries/javascript/script.js"></script>

<link rel="stylesheet" href="../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../libraries/validate/template.css" type="text/css">

<script src="../libraries/validate//jquery-1.js" type="text/javascript"></script>
<script src="../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../libraries/javascript/jquery-impromptu.min.js"></script>

<script type="application/javascript">
function pageSubmit()
{
	if($('#frmChangeExpirePswd').validationEngine('validate'))
		document.frmChangeExpirePswd.submit();	
}
function pageCancle()
{
	window.location.href = "<?php echo $backwardseperator;?>ipad_sample/?q=login";
}
</script>

<style>
.bordered_notRound {
	border-left: 1px solid #ccc;
    border-right: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
	border-top: 1px solid #ccc;
	font-size:11px;
	font-family: Verdana;
}
.redColor {	background-color:;
	font-family: Verdana;
	font-size: 12px;
	color: #F00;
	font-weight: bold;
}
body {
	background: #eeeeee url(<?php echo $backwardseperator ?>ipad_images/bg.jpg) top left repeat;
}
</style>
</head>
<body>
<form id="frmChangeExpirePswd" name="frmChangeExpirePswd" method="post">
<table width="500" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr class="normalfnt">
    	<td height="135">&nbsp;</td>
    </tr>
    <tr>
    	<td>
            <table width="100%" border="0" class="bordered_notRound" align="center" bgcolor="#FFFFFF">
            <tr>
            	<td height="25" class="normalfnt" style="color:#FFF;font-size:14px" bgcolor="#6E7B8B"><strong>&nbsp;&nbsp;Change your Password</strong></td>
            </tr>
            <tr class="normalfnt">
            	<td><table width="100%" border="0" cellspacing="4">
            	  <tr>
            	    <td colspan="3"><ul><li>Your password has expired. Please change your password.</li>
            	    <li>A good password should contain a mix of capital and lower-case letters, numbers and symbols.</li></ul></td>
           	      </tr>
            	  <tr>
            	    <td colspan="3"></td>
          	    </tr>
            	  <tr>
            	    <td width="8%">&nbsp;</td>
            	    <td width="31%">Current Password <span class="compulsoryRed">*</span></td>
            	    <td width="61%"><input type="password" name="txtOldPassword" id="txtOldPassword" style="width:250px;<?php echo(($currPswdError)?'border-color:#CD0000':''); ?>" class="validate[required,maxSize[20],minSize[4]]"/></td>
          	    </tr>
            	  <tr>
            	    <td>&nbsp;</td>
            	    <td>New Password <span class="compulsoryRed">*</span></td>
            	    <td><input type="password" name="txtNewPassword" id="txtNewPassword" style="width:250px;<?php echo(($newPswdUsed)?'border-color:#CD0000':''); ?>" class="validate[required,maxSize[20],minSize[4]]" /></td>
          	    </tr>
            	  <tr>
            	    <td>&nbsp;</td>
            	    <td>Confirm Password <span class="compulsoryRed">*</span></td>
            	    <td><input type="password" name="txtConfirmPassword" id="txtConfirmPassword" style="width:250px" class="validate[required,maxSize[20],minSize[4],equals[txtNewPassword]]" /></td>
          	    </tr>
            	  <tr>
            	    <td>&nbsp;</td>
            	    <td colspan="2" class="clsErrorMsg redColor"><?php echo $errorMsg; ?></td>
           	      </tr>
            	  <tr>
                    <td colspan="3" height="32" >
                        <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                        <tr>
                        <td align="center"><a class="button white medium" id="butChange" name="butChange" onclick="pageSubmit()">Change</a><a class="button white medium" id="butCancle" name="butCancle" onclick="pageCancle()">Cancle</a></td>
                        </tr>
                        </table>
                    </td>
           	      </tr>
          	  </table></td>
            </tr>
            </table>
        </td>
    </tr>
</table>
</form>
</body>
</html>
<?php
	if($disableAcc)
	{
?>
		<script>
			setTimeout('pageCancle();',2000);
		</script>
<?php
	}
?>
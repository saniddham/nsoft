var para;
var objName; 
var reportField;
// JavaScript Document
var basePath = 'presentation/administration/clusters/sub_cluster_details/';
$(document).ready(function() {
  	
	
  	$("#frmCPanel").validationEngine();

	$(".cboMultiple").chosen({allow_single_deselect: false , no_results_text: "Oops, nothing found!"});
	$(".cboSingle").chosen({allow_single_deselect: true , no_results_text: "Oops, nothing found!"});
	
	$('#frmCPanel #butInsertRow').live('click',add_new_row);

	$("#frmCPanel #cboMainCluster").chosen().live('change',getSubClusters);
	$("#frmCPanel .cboCustomer").chosen().live('change',getDataForCustomer);
	
	$('.clsDelete').live('click',deleteRow);
	
	$("#frmCPanel #cboSubCluster").chosen().live('change',loadForm);
	
	$('#frmCPanel #butSave').live('click',save); 
	
	$("#frmCPanel .cboMailType").chosen().live('change',enableDisableEmailAddress);
	
})

 
function getDataForCustomer(){
	var obj= this;
	getCustomerLocations(obj);
	getCustomerBrands(obj);
}
 
function getSubClusters(){
	var url 	= basePath+"sub_cluster_details_db.php?requestType=getSubClusters";
	var data 	= "mainCluster="+$('#frmCPanel #cboMainCluster').val(); 
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
 				$('#frmCPanel #cboSubCluster').html(json.subClusterOptions).trigger("liszt:updated");
			}
	});	
}


 
function getCustomerLocations(obj){

	var url 	= basePath+"sub_cluster_details_db.php?requestType=getCustomerLocations";
	var data 	= "customer="+$(obj).parent().parent().find('.cboCustomer').val(); 
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
  				$(obj).parent().parent().parent().find('.cboLocation').html(json.customerLocations).trigger("liszt:updated");
  			}
	});	
}

function getCustomerBrands(obj){

	var url 	= basePath+"sub_cluster_details_db.php?requestType=getCustomerBrands";
	var data 	= "customer="+$(obj).parent().parent().find('.cboCustomer').val(); 
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
  				$(obj).parent().parent().parent().find('.cboBrand').html(json.customerBrands).trigger("liszt:updated");
  			}
	});	
}

 
 
function alertx()
{
	$('#frmCPanel #butSave').validationEngine('hide')	;
	
}


function add_new_row(){
	
	showWaiting();
	
	var rowCount = document.getElementById('tblMain').rows.length;
	var i		=rowCount+1;

	var url 	= basePath+"sub_cluster_details_db.php?requestType=getNewRow";
	var data 	= "existingRows="+i; 
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
  				var html		= json.newRow;
				$('#tblMain > tbody:last').append(html);
				//alert($('#tblMain tr:eq('+rowCount+')').html());
				$('#tblMain tr:eq('+rowCount+')').find('.cboMultiple').chosen({allow_single_deselect: false , no_results_text: "Oops, nothing found!"});
				$('#tblMain tr:eq('+rowCount+')').find('.cboSingle').chosen({allow_single_deselect: true , no_results_text: "Oops, nothing found!"});
  			}
	});	
	
	hideWaiting();
}


function save(){
  	$("#frmCPanel").validationEngine();
		
		showWaiting();
		
		
		var data = "";
		
 		var arrHeader = "{";
			arrHeader += '"cboSubCluster":"'+ $('#frmCPanel #cboSubCluster').val() +'"' ;
			arrHeader += ' }';
		
		var i=0;
		var rcds=0;
		var errorDescFlag=0;
		var errorActionByFlag=0;
		var arrDetails="";
		
		
		$('.cboCustomer').each(function(){
			i++;
			var cboSubCluster	 		=	$('#frmCPanel #cboSubCluster').val();
			var cboCustomer	 			=	$(this).val();
			var cboLocation				=	$(this).parent().parent().parent().find('.cboLocation').val();
 			var cboBrand	 			=	$(this).parent().parent().parent().find('.cboBrand').val(); 
			var cboMailType	 			=	$(this).parent().parent().parent().find('.cboMailType').val();
			var txtLocEmails	 		=	$(this).parent().parent().parent().find('.txtLocEmails').val();
			var txtBrandEmails 			=	$(this).parent().parent().parent().find('.txtBrandEmails ').val();
			var cboCommon				=	$(this).parent().parent().parent().find('.cboCommon ').val();
			var cboMarketer 			=	$(this).parent().parent().parent().find('.cboMarketer ').val();
			var cboMarketerAss 			=	$(this).parent().parent().parent().find('.cboMarketerAss ').val();
			var cboAccounts 			=	$(this).parent().parent().parent().find('.cboAccounts ').val();
			var cboMarkHead 			=	$(this).parent().parent().parent().find('.cboMarkHead ').val();
			var cboOthers 				=	$(this).parent().parent().parent().find('.cboOthers ').val();
			var cboClusterHead 			=	$(this).parent().parent().parent().find('.cboClusterHead ').val();
		
 			
 
			if((cboCustomer!=''))
			{
				rcds++;
				arrDetails += "{";
							arrDetails += '"cboSubCluster":"'+ cboSubCluster +'",' ;
							arrDetails += '"cboCustomer":"'+ cboCustomer +'",' ;
							arrDetails += '"cboLocation":"'+ cboLocation +'",' ;
							arrDetails += '"cboBrand":"'+ cboBrand +'",' ;
							arrDetails += '"cboMailType":"'+ cboMailType +'",' ;
							arrDetails += '"txtLocEmails":'+URLEncode_json(txtLocEmails)+',' ;
							arrDetails += '"txtBrandEmails":'+URLEncode_json(txtBrandEmails)+',' ;
							arrDetails += '"cboCommon":"'+ cboCommon +'",' ;
							arrDetails += '"cboMarketer":"'+ cboMarketer +'",' ;
							arrDetails += '"cboMarketerAss":"'+ cboMarketerAss +'",' ;
							arrDetails += '"cboAccounts":"'+ cboAccounts +'",' ;
							arrDetails += '"cboMarkHead":"'+ cboMarkHead +'",' ;
							arrDetails += '"cboOthers":"'+ cboOthers +'",' ;
							arrDetails += '"cboClusterHead":"'+ cboClusterHead +'"' ;
							arrDetails +=  '},';
			}
		})
		
		if(rcds==0){
			alert("There is no records to save");
			hideWaiting();
			return false;
		}
		
		arrDetails = arrDetails.substr(0,arrDetails.length-1);
		
		var arrHeader=arrHeader;
		var arrDetails='['+arrDetails+']';
 		data+="&arrHeader="	+	arrHeader+"&arrDetails="	+	arrDetails;
 		
		var url = basePath+"sub_cluster_details_db.php?requestType=save";
		$.ajax({
				url:url,
				async:false,
				dataType:'json',
				type:'post',
				data:data,
		success:function(json){
				$('#frmCPanel #butSave').validationEngine('showPrompt', json.msg,json.type );
				if(json.type=='pass')
				{
					var t=setTimeout("alertx()",1000);
					hideWaiting();
					return;
				}
				hideWaiting();
			},
		error:function(xhr,status){
				$('#frmCPanel #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				var t=setTimeout("alertx()",1000);
				hideWaiting();
				return;
			}
			
		});
 }
 
 function loadForm(){
	 
	var subCluster			= $('#frmCPanel #cboSubCluster').val()
	var mainCluster			= $('#frmCPanel #cboMainCluster').val()
	window.location.href 	= basePath+'sub_cluster_details.php?sub_cluster='+subCluster+'&main_cluster='+mainCluster;
 }
 
 function deleteRow(){
	var obj =	this;
	var val = $.prompt('Are you sure you want to Delete this ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
						$(obj).parent().remove();
					}
				}
		});
	 
 }
 
 function enableDisableEmailAddress(){
	var obj 	= this;
	var type	= $(obj).val();
	
	if(type==1){//location wise
		$(obj).parent().parent().parent().find('.txtBrandEmails').attr('disabled','disabled');
		$(obj).parent().parent().parent().find('.txtLocEmails').removeAttr('disabled'); 
		$(obj).parent().parent().parent().find('.txtBrandEmails').val('');
	}
	else{
		$(obj).parent().parent().parent().find('.txtLocEmails').attr('disabled','disabled');
		$(obj).parent().parent().parent().find('.txtBrandEmails').removeAttr('disabled'); 
		$(obj).parent().parent().parent().find('.txtLocEmails').val('');
	}
	
	 
 }
 

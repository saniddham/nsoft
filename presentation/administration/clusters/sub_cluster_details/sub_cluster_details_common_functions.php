<?php
	//include_once "{$backwardseperator}dataAccess/Connector2.php";					
	include_once "dataAccess/DBManager2.php";								$db										= new DBManager2();
	require_once "class/tables/sys_main_clusters.php";						$sys_main_clusters						= new sys_main_clusters($db);
	require_once "class/tables/sys_sub_clusters.php";						$sys_sub_clusters						= new sys_sub_clusters($db);
	require_once "class/tables/sys_auto_email_report_types.php";			$sys_auto_email_report_types			= new sys_auto_email_report_types($db);
	require_once "class/tables/sys_sub_cluster_customer_brand_details.php";	$sys_sub_cluster_customer_brand_details	= new sys_sub_cluster_customer_brand_details($db);
	require_once "class/tables/mst_customer.php";							$mst_customer							= new mst_customer($db);
	require_once "class/tables/mst_customer_locations.php";					$mst_customer_locations					= new mst_customer_locations($db);
	require_once "class/tables/mst_customer_brand.php";						$mst_customer_brand						= new mst_customer_brand($db);
	require_once "class/tables/sys_users.php";								$sys_users								= new sys_users($db);
	require_once "class/tables/mst_marketer.php";							$mst_marketer							= new mst_marketer($db);
	require_once "class/tables/mst_marketer_manager.php";					$mst_marketer_manager					= new mst_marketer_manager($db);
	require_once "class/tables/sys_auto_email_report_type_address.php";		$sys_auto_email_report_type_address		= new sys_auto_email_report_type_address($db);
	
	
	function  get_main_cluster_options($main_cluster){
		global $sys_main_clusters;
		global $db;
		
 		//get maiin cluster array
		$where					="STATUS = 1 ORDER BY NAME ASC";
		$result					= $sys_main_clusters->select('*',NULL,$where,NULL,NULL);
		$i=0;
		$html = "<option value=\"\"></option>";					
		while($row=mysqli_fetch_array($result))
		{
			if($main_cluster==$row['ID'])
			$html .= "<option value=\"".$row['ID']."\" selected = \"selected\">".$row['NAME']."</option>";					
			else
			$html .= "<option value=\"".$row['ID']."\">".$row['NAME']."</option>";					
			$i++;
		}
 		
		return $html;
					
	}
	
	function  get_sub_cluster_options($main_cluster,$sub_cluster){
		global $sys_sub_clusters;
		global $db;
		
 		//get sub cluster array
		$where					="MAIN_CLUSTER = '$main_cluster' AND STATUS = 1 ORDER BY NAME ASC";
		$result					= $sys_sub_clusters->select('*',NULL,$where,NULL,NULL);
		$i=0;
		$html = "<option value=\"\"></option>";					
		while($row=mysqli_fetch_array($result))
		{
			if($sub_cluster==$row['ID'])
			$html .= "<option value=\"".$row['ID']."\" selected = \"selected\">".$row['NAME']."</option>";					
			else
			$html .= "<option value=\"".$row['ID']."\">".$row['NAME']."</option>";					
			$i++;
		}
 		
		return $html;
					
	}
	
	
	function get_auto_email_report_types_details(){
		
		global $sys_auto_email_report_types;
		global $db;
		
 		//get maiin cluster array
		$where					="STATUS = 1 ORDER BY NAME ASC";
		$result					= $sys_auto_email_report_types->select('*',NULL,$where,NULL,NULL);
  		
		return $result;
					
		
	}
	
	function  get_customer_options($customer){
		
		global $mst_customer;
		global $db;
		
 		//get maiin cluster array
		$where					=" intStatus = 1 ";
		$where					.=" ORDER BY strName ASC";
		
		$result					= $mst_customer->select('*',NULL,$where,NULL,NULL);
		$i=0;
		$html = "<option value=\"\"></option>";					
		while($row=mysqli_fetch_array($result))
		{
			if($customer==$row['intId'])
			$html .= "<option value=\"".$row['intId']."\" selected= \"selected\" >".$row['strName']."</option>";					
			else
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";					
			$i++;
		}
 	
 		return $html;
					
	}
	
	function  get_customer_locations_options($customer,$location){
		
		global $mst_customer_locations;
		global $db;
		
 		//get maiin cluster array
		$where			= "intCustomerId = '$customer' ORDER BY mst_customer_locations_header.strName ASC";
		$join			= "INNER JOIN mst_customer_locations_header ON mst_customer_locations_header.intId=mst_customer_locations.intLocationId ";
		
		$result					= $mst_customer_locations->select('*',$join,$where,NULL,NULL);

		$i=0;
		$html = "<option value=\"\"></option>";					
		while($row=mysqli_fetch_array($result))
		{
			if($location==$row['intId'])
			$html .= "<option value=\"".$row['intId']."\" selected= \"selected\" >".$row['strName']."</option>";					
			else
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";					
			$i++;
		}
 	
 		return $html;
					
	}
	
	
	
	function  get_customer_brand_options($customer,$brand){
		
		global $mst_customer_brand;
		global $db;
		
 		//get maiin cluster array
		$where			= "intCustomerId = '$customer' ORDER BY mst_brand.strName ASC";
		$join			= "INNER JOIN mst_brand ON mst_customer_brand.intBrandId=mst_brand.intId ";
		
		$result			= $mst_customer_brand->select('*',$join,$where,NULL,NULL);
		$i=0;
		$html = "<option value=\"\"></option>";					
		while($row=mysqli_fetch_array($result))
		{
			if($brand==$row['intBrandId'])
			$html .= "<option value=\"".$row['intBrandId']."\" selected= \"selected\" >".$row['strName']."</option>";					
			else
			$html .= "<option value=\"".$row['intBrandId']."\">".$row['strName']."</option>";					
			$i++;
		}
 	
 		return $html;
					
	}
	
	
	
	function get_email_type($type){
		 
 		  
		$html = "<option value=\"\"></option>";					
		
		if($type==1)
		$html .= "<option value=\"1\" selected=\"selected\">Location Wise</option>";	
		else
		$html .= "<option value=\"1\">Location Wise</option>";					
						
		if($type==2)
		$html .= "<option value=\"2\" selected=\"selected\">Brand Wise</option>";	
		else				
		$html .= "<option value=\"2\">Brand Wise</option>";					
		
		
		return $html;
	}
	
	function get_marketer($id_string){
		
		global $mst_marketer;
 		global $db;
		
		$id_arr = explode(",", $id_string);
	
		//get maiin cluster array
		$where					=" intStatus = 1 ";
 		$where					.=" ORDER BY sys_users.strFullName ASC";
		
		$join					='INNER JOIN sys_users ON mst_marketer.intUserId = sys_users.intUserId';
		
		$result					= $mst_marketer->select('mst_marketer.intUserId,sys_users.strFullName',$join,$where,NULL,NULL);
		//die();
		$i=0;
		$html = "<option value=\"\"></option>";					
		while($row=mysqli_fetch_array($result))
		{
			if(in_array($row['intUserId'], $id_arr))
			$html .= "<option value=\"".$row['intUserId']."\" selected= \"selected\">".$row['strFullName']."</option>";					
			else
			$html .= "<option value=\"".$row['intUserId']."\">".$row['strFullName']."</option>";					
			$i++;
		}
 	
 		
		return $html;
	}
	
	function get_marketing_head($id_string){
	
		global $mst_marketer;
		global $db;
		
		$id_arr = explode(",", $id_string);
		
		$where					=" intStatus = 1 ";
		$where					.=" ORDER BY sys_users.strFullName ASC";
		
		$join					='INNER JOIN sys_users ON mst_marketer.intManager = sys_users.intUserId';
		
		$result					= $mst_marketer->select('DISTINCT mst_marketer.intManager,sys_users.strFullName',$join,$where,NULL,NULL);
 		$i=0;
		$html = "<option value=\"\"></option>";					
		while($row=mysqli_fetch_array($result))
		{
			if(in_array($row['intManager'], $id_arr))
			$html .= "<option value=\"".$row['intManager']."\" selected= \"selected\">".$row['strFullName']."</option>";					
			else
			$html .= "<option value=\"".$row['intManager']."\">".$row['strFullName']."</option>";					
			$i++;
		}
 	
 		return $html;
		
		return $html;
	}
	
	function get_user($id_string){
		
		global $sys_users;
 		global $db;
		
		$id_arr = explode(",", $id_string);
		
 		//get maiin cluster array
		$where		=" intStatus = 1 ";
		$where		.=" ORDER BY strFullName ASC";
		
		$result		= $sys_users->select('*',NULL,$where,NULL,NULL);
 		$i=0;
		$html = "<option value=\"\"></option>";					
		while($row=mysqli_fetch_array($result))
		{
			if(in_array($row['intUserId'], $id_arr))
			$html .= "<option value=\"".$row['intUserId']."\" selected= \"selected\">".$row['strFullName']."</option>";					
			else
			$html .= "<option value=\"".$row['intUserId']."\">".$row['strFullName']."</option>";					
			$i++;
		}
 	
 		return $html;
	
 	}
	
	function get_grid_row($saved_records,$row,$customer_options,$cust_location_options,$brand_options,$email_type_options,$customer_location_emails,$customer_brand_emails,$common,$marketer,$marketing_assistant,$accounts,$marketing_head,$others,$cluster_head){
		
		
		if($row['EMAIL_TYPE']==1){
		$dis_brn	='disabled="disabled"';
		$dis_loc	='';
		}
		else{
		$dis_brn	='';
		$dis_loc	='disabled="disabled"';
		}
			

		$html	='		
			<tr>
            <td class="clsDelete" id="'.$saved_records.'"><a class="clsButDelete button green medium" id="butDelete" name="butDelete">Delete</a></td>
            <td title="Customer"><span class="poCombo">
			<select name="cboCustomer'.$saved_records.'" id="cboCustomer'.$saved_records.'" class="cboSingle cboSingle'.$saved_records.' cboCustomer normalfnt chosen-select" style="width:200px;height:50px" tabindex="4">
             '.$customer_options.'
			  </select>
			  </span></td>
            <td title="Location"><span class="poCombo">
			<select name="cboLocation'.$saved_records.'" id="cboLocation'.$saved_records.'"  class="cboSingle cboSingle'.$saved_records.' cboLocation normalfnt chosen-select" style="width:150px;height:50px" tabindex="4">
             '.$cust_location_options.'
		      </select>
			  </span></td>
            <td title="Brand"><span class="poCombo">
              <select name="cboBrand'.$saved_records.'" id="cboBrand'.$saved_records.'" class="cboSingle cboSingle'.$saved_records.' cboBrand normalfnt chosen-select" style="width:120px;height:50px" tabindex="4">
             '.$brand_options.'
		      </select>
            </span></td>
            <td align="center" title="Mail Type"><span class="poCombo">
              <select name="cboMailType'.$saved_records.'" id="cboMailType'.$saved_records.'"  class="cboSingle cboSingle'.$saved_records.' cboMailType normalfnt chosen-select" style="width:150px;height:50px" tabindex="4">
            '.$email_type_options.'
		      </select>
            </span></td>
            <td class="poCombo"  title="Location Emails">
            <input name="txtLocEmails'.$saved_records.'" id="txtLocEmails'.$saved_records.'" '.$dis_loc.' class="txtLocEmails" style="width:300px;height:50px" tabindex="4" value="'.$customer_location_emails.'">
              </td>
            <td align="center"  title="Brand Emails"><span class="poCombo">
              <input name=txtBrandEmails'.$saved_records.'" id="txtBrandEmails'.$saved_records.'" '.$dis_brn.' class="txtBrandEmails" style="width:300px;height:50px" tabindex="4" value="'.$customer_brand_emails.'">
            </span></td>
            <td class="glCombo"  title="Common">
            <select name="cboCommon'.$saved_records.'" id="cboCommon'.$saved_records.'"  multiple="multiple" class="cboMultiple cboMultiple'.$saved_records.' cboCommon normalfnt chosen-select" style="width:200px;height:50px" tabindex="4" data-placeholder="">
            '.$common .'
			  </select>
              </td>
            <td  title="Marketer"><span class="glCombo">
              <select name="cboMarketer'.$saved_records.' id="cboMarketer'.$saved_records.'"  multiple="multiple" class="cboMultiple cboMultiple'.$saved_records.' cboMarketer normalfnt chosen-select" style="width:200px;height:50px" tabindex="4" data-placeholder="">
            '.$marketer.'
			  </select>
            </span></td>
            <td  title="Marketer Assistant"><span class="glCombo">
              <select name="cboMarketerAss'.$saved_records.'" id="cboMarketerAss'.$saved_records.'"  multiple="multiple" class="cboMultiple cboMultiple'.$saved_records.' cboMarketerAss normalfnt chosen-select" style="width:300px;height:50px" tabindex="4" data-placeholder="">
            '.$marketing_assistant.'
		      </select>
            </span></td>
            <td align="center" title="Accountants"><span class="glCombo">
              <select name="cboAccounts'.$saved_records.'" id="cboAccounts'.$saved_records.'"  multiple="multiple" class="cboMultiple cboMultiple'.$saved_records.' cboAccounts normalfnt chosen-select" style="width:200px;height:50px" tabindex="4" data-placeholder="">
            '.$accounts.'
			  </select>
            </span></td>
            <td align="center" title="Marketer Head"><span class="glCombo">
              <select name="cboMarkHead'.$saved_records.'" id="cboMarkHead'.$saved_records.'"  multiple="multiple" class="cboMultiple cboMultiple'.$saved_records.' cboMarkHead normalfnt chosen-select" style="width:200px;height:50px" tabindex="4" data-placeholder="">
            '.$marketing_head.'
			  </select>
            </span></td>
            <td align="center"  title="Others"><span class="glCombo">
              <select name="cboOthers'.$saved_records.'" id="cboOthers'.$saved_records.'"  multiple="multiple" class="cboMultiple cboOthers normalfnt chosen-select" style="width:200px;height:50px" tabindex="4" data-placeholder="">
            '.$others.'
			  </select>
            </span></td>
            <td align="center"  title="Cluster Head"><span class="glCombo">
              <select name="cboClusterHead'.$saved_records.'" id="cboClusterHead'.$saved_records.'" class="cboSingle cboSingle'.$saved_records.' cboClusterHead normalfnt chosen-select" style="width:200px;height:50px" tabindex="4" data-placeholder="">
            '.$cluster_head.'
			  </select>
            </span></td>
            </tr>';
			
		return $html;
		
	}
	
	function get_details($subCluster){
		
		global $sys_sub_cluster_customer_brand_details;
		global $db;
		
 		//get maiin cluster array
		$where					="SUB_CLUSTER_ID = '$subCluster' ORDER BY CUSTOMER_ID ASC";
		$result					= $sys_sub_cluster_customer_brand_details->select('*',NULL,$where,NULL,NULL);
		
		return $result;
	}
	
	function get_grid_report_type_row($records,$active,$id,$name,$to,$cc,$bcc){
		
		if($active==1){
			$chk	='checked="checked"';
		}
		
		$html	='		
			<tr>
            <td title="Report Name" id="'.$id.'" class="clsId">
				'.$name.'
			</td>
            <td title="TO">
			<select name="cboTo'.$records.'" id="cboTo'.$records.'" multiple="multiple"  class="cboSingle cboSingle'.$records.' cboTo normalfnt chosen-select" style="width:100%;height:50px" tabindex="4">
             '.get_user($to).'
			 </select>
			 </td>
            <td title="CC">
			<select name="cboCc'.$records.'" id="cboCc'.$records.'" multiple="multiple" class="cboSingle cboSingle'.$records.' cboCc normalfnt chosen-select" style="width:100%;height:50px" tabindex="4">
             '.get_user($cc).'
		      </select>
			  </td>
            <td align="center" title="BCC">
			<select name="cboBcc'.$records.'" id="cboBcc'.$records.'" multiple="multiple"  class="cboSingle cboSingle'.$records.' cboBcc normalfnt chosen-select" style="width:100%;height:50px" tabindex="4">
             '.get_user($bcc).'
		      </select>
			  </td>
            </tr>';
			
		return $html;
		
	}
	
	
	function get_saved_auto_email_report_types_details($main_cluster,$sub_cluster){
		
		global $sys_auto_email_report_types;
		global $db;
		
 		//get maiin cluster array
		$select					="sys_auto_email_report_types.ID,
									sys_auto_email_report_types.`NAME`,
									sys_auto_email_report_types.`STATUS`,
									sys_auto_email_report_types.ACTIVE_COMPANIES,
									sys_auto_email_report_type_address.TO_EMAIL_USERS,
									sys_auto_email_report_type_address.CC_EMAIL_USERS,
									sys_auto_email_report_type_address.BCC_EMAIL_USERS";
									
 		
		$join					="LEFT JOIN sys_auto_email_report_type_address ON sys_auto_email_report_types.ID = sys_auto_email_report_type_address.ID AND 
								  sys_auto_email_report_type_address.MAIN_CLUSTER = '$main_cluster' AND
								  sys_auto_email_report_type_address.SUB_CLUSTER = '$sub_cluster'
";
		$result					= $sys_auto_email_report_types->select($select,$join,NULL,NULL,NULL);
 		return $result;
		
	}
		
	
 ?>
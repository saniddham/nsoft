<?php
	ini_set('max_execution_time', 11111111) ;

	$programCode			= 'P0879';
	$mainPath 				= $_SESSION['mainPath'];
	$userId 				= $_SESSION['userId'];
	$location 				= $_SESSION['CompanyID'];
	$company 				= $_SESSION['headCompanyId'];
	$HR_DB 					= $_SESSION['HRDatabase'];
	$main_DB 				= $_SESSION['Database'];
	

	$main_cluster			= $_REQUEST['main_cluster'];
	$sub_cluster			= $_REQUEST['sub_cluster'];
 	
?>

<head>
<title>Sub Cluster Details</title>
</head>
<body>
<form id="frmCPanel" name="frmCPanel" autocomplete="off" method="post">
  <table width="100%" border="0" align="center">
   <?php 
	//include_once "{$backwardseperator}dataAccess/Connector2.php";					
	//include_once "../../../../dataAccess/DBManager2.php";								$db										= new DBManager2();
	require_once "class/cls_commonFunctions_get.php";						$cls_commonFunctions_get				= new cls_commonFunctions_get($db);
	require_once "class/cls_commonErrorHandeling_get.php";					$cls_commonErrorHandeling_get			= new cls_commonErrorHandeling_get($db);
	require_once "class/tables/sys_main_clusters.php";						$sys_main_clusters						= new sys_main_clusters($db);
	require_once "class/tables/sys_sub_clusters.php";						$sys_sub_clusters						= new sys_sub_clusters($db);
	require_once "class/tables/sys_sub_cluster_customer_brand_details.php";	$sys_sub_cluster_customer_brand_details	= new sys_sub_cluster_customer_brand_details($db);
	require_once "class/tables/mst_customer.php";							$mst_customer							= new mst_customer($db);
	require_once "class/tables/sys_users.php";								$sys_users								= new sys_users($db);
	require_once "class/tables/mst_marketer.php";							$mst_marketer							= new mst_marketer($db);
	require_once "class/tables/mst_marketer_manager.php";					$mst_marketer_manager					= new mst_marketer_manager($db);
	include_once "presentation/administration/clusters/sub_cluster_details/sub_cluster_details_common_functions.php";
	
	//connect to the db
	$db->connect();
	
 	
	//check save permision
	$permision_save			= $cls_commonFunctions_get->boolPermision($userId,$programCode,'intEdit');
    ?>
  </table>

   
  <div align="center">
    <div class="trans_layoutXL">
      <div class="trans_text">Sub Cluster - Details</div>
      <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
      <tr><td>
          <table width="100%" border="0" class="tableBorder_allRound" id="tblSearch">
          <tr>
          <td width="7%" class="normalfnt">Main Cluster</td>
          <td width="25%">
          <select name="cboMainCluster" id="cboMainCluster" class="cboSingle normalfnt chosen-select" style="width:300px;height:50px" tabindex="4">
			<?php
			echo get_main_cluster_options($main_cluster);
            ?>
          </select>
          </td>
          <td width="0%"></td>
          <td width="1%"></td>
          <td width="6%" class="normalfnt">Sub Cluster</td>
          <td width="24%">
          <select name="cboSubCluster" id="cboSubCluster" class="cboSingle normalfnt chosen-select" style="width:300px;height:50px" tabindex="4">
			<?php
            echo get_sub_cluster_options($main_cluster,$sub_cluster);
            ?>
		  </select>
          </td>
          <td width="3%" class="normalfnt">&nbsp;</td>
          <td width="6%" class="normalfnt">&nbsp;</td>
          <td width="28%">&nbsp;</td>
          </tr>
          </table>
      </td></tr>
      <tr>
      <td><div style="float:right"><a class="button white small" id="butInsertRow">Add New Row</a></div></td>
      </tr>
        <tr>
          <td><div style="width:1260px; height:400px; overflow:scroll" ><table width="100%" border="0" class="bordered" id="tblMain">
              <thead>
                <tr>
                <th>Delete</th>
                <th>Customer</th>
                <th>Location</th>
                <th>Brand</th>
                <th nowrap="nowrap">Email Type</th>
                <th>Customer Location Emails</th>
                <th>Customer Brand Emails</th>
                <th>Common</th>
                <th>Marketer</th>
                <th>Marketing Assistant</th>
                <th>Accounts</th>
                <th>Marketing Head</th>
                <th>Others</th>
                <th>Cluster Head</th>
                </tr>
              </thead>
            <tbody>
            <?php
			$saved_records 		=0;
			$details_results	=get_details($sub_cluster);
			while($row_d=mysqli_fetch_array($details_results))
			{
				$saved_records ++;
				$customer_options			=	get_customer_options($row_d['CUSTOMER_ID']);		
				$cust_location_options		=	get_customer_locations_options($row_d['CUSTOMER_ID'],$row_d['CUST_LOCATION_ID']);
				$brand_options				=	get_customer_brand_options($row_d['CUSTOMER_ID'],$row_d['BRAND']);
				$email_type_options			=	get_email_type($row_d['EMAIL_TYPE']);	
				$customer_location_emails	=	$row_d['EMAIL_LOCATION'];
				$customer_brand_emails		=	$row_d['EMAIL_BRAND'];
				$common						=	get_user($row_d['COMMON']);	
				$marketer					=	get_marketer($row_d['MARKETER']);	
				$marketing_assistant		=	get_user($row_d['MARKETING_ASSISTANT']);	
				$accounts					=	get_user($row_d['ACCOUNTS']);
				$marketing_head				=	get_marketing_head($row_d['COMMON']);	
				$others						=	get_user($row_d['OTHERS']);	
				$cluster_head				=	get_user($row_d['CLUSTER_HEAD']);	
				$saved_records++;
				echo get_grid_row($saved_records,$row_d,$customer_options,$cust_location_options,$brand_options,$email_type_options,$customer_location_emails,$customer_brand_emails,$common,$marketer,$marketing_assistant,$accounts,$marketing_head,$others,$cluster_head);
			?>
            
            <?php
			}
			?>
            
            <?php
			if($saved_records==0){
				$customer_options			=	get_customer_options('');		
				$cust_location_options		=	get_customer_locations_options('','');
				$brand_options				=	get_customer_brand_options('','');
				$email_type_options			=	get_email_type('');	
				$customer_location_emails	=	'';	
				$customer_brand_emails		=	'';	
				$common						=	get_user('');	
				$marketer					=	get_marketer('');	
				$marketing_assistant		=	get_user('');	
				$accounts					=	get_user('');
				$marketing_head				=	get_marketing_head('');	
				$others						=	get_user('');	
				$cluster_head				=	get_user('');	
				echo get_grid_row($saved_records,'',$customer_options,$cust_location_options,$brand_options,$email_type_options,$customer_location_emails,$customer_brand_emails,$common,$marketer,$marketing_assistant,$accounts,$marketing_head,$others,$cluster_head);
			}
			?>
            </tbody>
      </table></div></td>
    </tr>
    <tr><td>
        <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center"><a class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butSave" <?php if($permision_save!=1){ ?>style="display:none"<?php } ?>>Save</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
        </tr>
        </table>
    </td>
    </tr>
    </table>
    </div>
  </div>
</form>
</body>
<div style="width:500px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</html>
<?php
//close the connection
	
	$db->disconnect();	
?>
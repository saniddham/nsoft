<?php
session_start();
ini_set('display_errors',1);

$backwardseperator 		= "../../../../";
$programCode			= 'P0879';
$mainPath 				= $_SESSION['mainPath'];
$userId 				= $_SESSION['userId'];
$location 				= $_SESSION['CompanyID'];
$company 				= $_SESSION['headCompanyId'];
$HR_DB 					= $_SESSION['HRDatabase'];
$main_DB 				= $_SESSION['Database'];

//include_once "{$backwardseperator}dataAccess/Connector2.php";					
include_once "../../../../dataAccess/DBManager2.php";								$db										= new DBManager2();
require_once "../../../../class/cls_commonFunctions_get.php";						$cls_commonFunctions_get				= new cls_commonFunctions_get($db);
require_once "../../../../class/cls_commonErrorHandeling_get.php";					$cls_commonErrorHandeling_get			= new cls_commonErrorHandeling_get($db);
require_once "../../../../class/tables/sys_main_clusters.php";						$sys_main_clusters						= new sys_main_clusters($db);
require_once "../../../../class/tables/sys_sub_clusters.php";						$sys_sub_clusters						= new sys_sub_clusters($db);
require_once "../../../../class/tables/sys_sub_cluster_customer_brand_details.php";	$sys_sub_cluster_customer_brand_details	= new sys_sub_cluster_customer_brand_details($db);
require_once "../../../../class/tables/mst_customer.php";							$mst_customer							= new mst_customer($db);
require_once "../../../../class/tables/mst_customer_locations.php";					$mst_customer_locations					= new mst_customer_locations($db);
require_once "../../../../class/tables/mst_customer_brand.php";						$mst_customer_brand						= new mst_customer_brand($db);

include		"../../../../class/error_handler.php";									$error_handler							= new error_handler(); 
include		"../../../../class/tables/menupermision.php";							$menupermision							= new menupermision($db);


include_once "sub_cluster_details_common_functions.php";
die();
$requestType = $_REQUEST["requestType"];

$db->connect();

if($requestType == 'getSubClusters')
{
	$main_cluster					= $_REQUEST['mainCluster'];
	$options						= get_sub_cluster_options($main_cluster,'');
	$response['subClusterOptions']	= $options;
	
	echo json_encode($response);		
}
else if($requestType == 'getCustomerLocations')
{
	$customer						= $_REQUEST['customer'];
	$options						= get_customer_locations_options($customer,'');
	$response['customerLocations']	= $options;
	
	echo json_encode($response);		
}

else if($requestType == 'getCustomerBrands')
{
	$customer		= $_REQUEST['customer'];
	$options		= get_customer_brand_options($customer,'');
	$response['customerBrands']	= $options;
	
	echo json_encode($response);		
}

else if($requestType == 'getNewRow')
{
	$existingRows				= $_REQUEST['existingRows'];
	$newRow						= $existingRows+1;
	
	$customer_options			=	get_customer_options('');		
	$cust_location_options		=	'';	
	$brand_options				=	'';	
	$email_type_options			=	get_email_type('');	
	$customer_location_emails	=	'';	
	$customer_brand_emails		=	'';	
	$common						=	get_user('');	
	$marketer					=	get_marketer('');	
	$marketing_assistant		=	get_user('');	
	$accounts					=	get_user('');
	$marketing_head				=	get_marketing_head('');	
	$others						=	get_user('');	
	$cluster_head				=	get_user('');	
	
 	$response['newRow']	= get_grid_row($newRow,'',$customer_options,$cust_location_options,$brand_options,$email_type_options,$customer_location_emails,$customer_brand_emails,$common,$marketer,$marketing_assistant,$accounts,$marketing_head,$others,$cluster_head);
	
	echo json_encode($response);		
}

else if($requestType == 'save'){
	
 	$arrHeader 	 		= json_decode($_REQUEST['arrHeader'],true);
 	$arrDetails 		= json_decode($_REQUEST['arrDetails'], true);
	
	try{
		$i=0;
		
		delete($arrHeader);
		
		foreach($arrDetails as $details)
		{
			insert($details,$i);
 			$i++;
		}
		

		$response['type']	= 'pass';
		$response['msg'] 	= 'Saved successfully.';
	
		$db->commit();
			
	}catch(Exception $e){
		$db->rollback();
		$response['msg'] 	=  $e->getMessage();;
		$response['error'] 	=  $error_handler->jTraceEx($e);
		$response['type']	=  'fail';
		$response['sql']	=  $db->getSql();
	}
	
 	echo json_encode($response);

}

function delete($arrDetails){
	
	global $sys_sub_cluster_customer_brand_details;

	$cboSubCluster		=$arrDetails['cboSubCluster'];
 	
	$where				= " sys_sub_cluster_customer_brand_details.SUB_CLUSTER_ID = '$cboSubCluster' ";
	
	
	$result_delete		= $sys_sub_cluster_customer_brand_details->delete($where);
 	if(!$result_delete['0']) 
		throw new Exception($result_delete['1']);
	else
		return true;
	
}

function insert($arrDetails,$order_by){
	
	global $sys_sub_cluster_customer_brand_details;
	global $cls_commonFunctions_get;
	
	$data =array(
		'ORDER_BY'					=>'"'.$order_by.'"',
		'SUB_CLUSTER_ID'			=>'"'.$arrDetails['cboSubCluster'].'"',
		'CUSTOMER_ID'				=>'"'.$arrDetails['cboCustomer'].'"',
		'CUST_LOCATION_ID'			=>'"'.$arrDetails['cboLocation'].'"',
		'BRAND'						=>'"'.$arrDetails['cboBrand'].'"',
		'EMAIL_TYPE'				=>'"'.$arrDetails['cboMailType'].'"',
		'EMAIL_LOCATION'			=>'"'.$cls_commonFunctions_get->replace($arrDetails['txtLocEmails']).'"',
		'EMAIL_BRAND'				=>'"'.$cls_commonFunctions_get->replace($arrDetails['txtBrandEmails']).'"',
		'COMMON'					=>'"'.$arrDetails['cboCommon'].'"',
		'MARKETER'					=>'"'.$arrDetails['cboMarketer'].'"',
		'MARKETING_ASSISTANT'		=>'"'.$arrDetails['cboMarketerAss'].'"',
		'MARKETER_HEADS'			=>'"'.$arrDetails['cboAccounts'].'"',
		'ACCOUNTS'					=>'"'.$arrDetails['cboMarkHead'].'"',
		'OTHERS'					=>'"'.$arrDetails['cboOthers'].'"',
		'CLUSTER_HEAD'				=>'"'.$arrDetails['cboClusterHead'].'"' 
	);
	
 			
	$result_insert		=$sys_sub_cluster_customer_brand_details->insert($data);
	
	if(!$result_insert['0']) 
		throw new Exception($result_insert['1']);
	else
		return true;
}



$db->disconnect();		

?>
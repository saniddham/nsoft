<?php
	session_start();

	$backwardseperator 	= "../../../";
	$mainPath 			= $_SESSION['mainPath'];
	$userId 			= $_SESSION['userId'];
	$intCompany			= $_SESSION['CompanyID'];
	
	$savedStatus		= true;
	$errorMsg			= '';
	
	include "{$backwardseperator}dataAccess/Connector.php";
	include "{$backwardseperator}class/administration/password/cls_password_get.php";
	include "{$backwardseperator}class/administration/password/cls_password_set.php";
	
	$objPasswordGet = new password_get($db);
	$objPasswordSet = new password_set($db);
	
	$oldPass = base64_decode($_REQUEST['oldPass']);
	$newPass = base64_decode($_REQUEST['newPass']);
	
	$chkPswdHistry		= $objPasswordGet->checkPasswordHistory($newPass,$userId);
	if($chkPswdHistry && $savedStatus)
	{
		$savedStatus	= false;
		$errorMsg 		= "New password already used with this account.";
	}
	if($savedStatus)
	{
		
		$errorAttempts	= getPasswordErrorAttempts(1);
		
		$sql = "UPDATE `sys_users` 
				SET `strPassword`='".md5(md5($newPass))."',
				PASSWORD_LAST_MODIFIED_DATE = NOW(),
				PASSWORD_ERROR_ATTEMPTS_LEFT = '".$errorAttempts."'
				WHERE (`intUserId`='$userId') and 
				strPassword='".md5(md5($oldPass))."'";
				
		$result = $db->RunQuery($sql);
		if($result && $savedStatus)
		{
			
			$objPasswordSet->savePasswordHistory($userId,$newPass);
			$toUserDetails		= $objPasswordGet->getUserDetails($userId);
			
			$toUserName			= $toUserDetails['userName'];
			$toUserEmail		= $toUserDetails['userEmail'];
			$toUserFullName		= $toUserDetails['fullName'];
			
			$fromUserName		= 'NSOFT - SCREENLINE HOLDINGS.';
			$fromUserFullName	= 'Administrator';
			$fromUserEmail		= '';
			
			include "../password/password_change_mail_template.php";	
			
		}
		else
		{
			$savedStatus		= false;
			$errorMsg			= $db->errormsg;//'Old password is invalid.';	
		}
	}
	
	if($savedStatus)
	{
		session_unset(); 
		session_destroy(); 

		$response['type'] 		= 'pass';
		$response['msg'] 		= 'Password changed successfully.';
	}
	else
	{
		$response['type'] 		= 'fail';
		$response['msg'] 		= $errorMsg;
		
	}
	echo json_encode($response);
	
	function getPasswordErrorAttempts($companyId)
	{
		global $db;
		
		$sqlPE = " SELECT PASSWORD_ERROR_ATTEMPTS
					FROM sys_config
					WHERE
					intCompanyId = '$companyId' ";
		
		$resultPE = $db->RunQuery($sqlPE);
		
		$rowPE 	= mysqli_fetch_array($resultPE);
		
		return $rowPE['PASSWORD_ERROR_ATTEMPTS'];
		
	}
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<title>Change Password</title>
<form id="frmChangePassword" name="frmChangePassword" method="post" autocomplete="off">
<div align="center">
		<div class="trans_layoutD">
		  <div class="trans_text">Change Password</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="585" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
              <tr>
                <td width="75" class="normalfnt">&nbsp;</td>
                <td width="169" class="normalfnt">&nbsp;</td>
                <td>&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Old Password&nbsp;<span class="compulsoryRed">*</span></td>
                <td width="200"><input name="txtOldPassword" type="password" class="validate[required,maxSize[20]]" id="txtOldPassword" style="width:200px"/></td>
                <td width="111" class="normalfnt">&nbsp;</td>
                </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">New Password <span class="compulsoryRed">*</span></td>
                <td colspan="2"><input name="password" type="password" class="validate[required,maxSize[20],minSize[4]]" id="password" style="width:200px" maxlength="50" /></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Confirm Password <span class="compulsoryRed">*</span></td>
                <td><input name="password2" type="password" class="validate[required,maxSize[20],minSize[4],equals[password]]" id="password2" style="width:200px" maxlength="50" /></td>
                <td class="normalfnt">&nbsp;</td>
                </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
<td width="100%" align="center" bgcolor=""><?php
                if($form_permision['add']||$form_permision['edit'])
				{
				?><a class="button white medium" id="butSave" name="butSave">Change</a>
                  <?php
				}
				?>
                  <a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
</form>

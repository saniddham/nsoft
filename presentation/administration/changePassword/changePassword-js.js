var basePath = 'presentation/administration/changePassword/';	
$(document).ready(function() {
  	
	$("#frmChangePassword").validationEngine();
	$("#frmResetLink").validationEngine();
	$("#frmChangeExpirePswd").validationEngine();	
	$('#frmChangePassword #butSave').die('click').live('click',saveDetails);	
	$('#frmResetPassword #cboDepartment').die('change').live('change',loadUsers);
	$('#frmResetPassword #cboUser').die('change').live('change',function(){
			$('#frmResetPassword #butReset').show();	
	});
	
	$('#frmResetPassword #butReset').die('click').live('click',function(){
			var val = $.prompt('Are you sure you want to Reset Password" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basePath+"resetPassword-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'POST',
											data:'requestType=reset&userId='+$('#frmResetPassword #cboUser').val(),
											async:false,
											success:function(json){
												
												$('#frmResetPassword #butReset').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												if(json.type=='pass')
												{
													$('#frmResetPassword').get(0).reset();
													var t=setTimeout("alertPass()",1000);return;
												}
												//var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
	});
});
function loadUsers()
{
	$('#frmResetPassword #cboUser').load(basePath+'resetPassword-db-get.php?requestType=loadUsers&depId='+$(this).val());	
}
function saveDetails()
{
	if(!$("#frmChangePassword").validationEngine('validate'))
		return ;
		
	var oldPass = $('#txtOldPassword').val();
	var newPass = $('#password').val();
	var url = basePath +"changePassword-db-set.php?oldPass="+btoa(oldPass)+'&newPass='+btoa(newPass);
	$.ajax({
		url:url,
		dataType: "json", 
		type:'POST', 
		async:false,
		success:function(json){
			$("#frmChangePassword #butSave").validationEngine('showPrompt',json.msg,json.type)
			var t=setTimeout("alertCHPsw()",1000);
			return;
		},
		error:function(xhr,status){
			$('#frmChangePassword #butSave').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
		}
	});
}

function alertx()
{
	document.location.href = document.location.href;	
}

function alertPass()
{
	$('#frmChangePassword').validationEngine('hide');
}
function alertCHPsw()
{
	$('#frmChangePassword').validationEngine('hide');
	document.location.href = 'login.php';
}

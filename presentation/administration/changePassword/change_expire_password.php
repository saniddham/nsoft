<?php
$backwardseperator = "../../../";
include_once "../../../dataAccess/DBManager2.php"; 						$db 					= new DBManager2(1);
include_once "../../../class/tables/sys_users.php";						$sys_users				= new sys_users($db);
include_once "../../../class/dateTime.php";								$dateTimes 				= new dateTimes($db);
include_once "../../../class/tables/sys_users_disabled.php";			$sys_users_disabled 	= new sys_users_disabled($db);

$userId			= base64_decode($_GET['userId']);
$error_attempts	= base64_decode($_GET['error_attempts']);

$oldPswd 		= $_POST["txtOldPassword"];
$newPswd 		= $_POST["txtNewPassword"];
$conPswd 		= $_POST["txtConfirmPassword"];
$errorMsg		= '';
$disableAcc		= false;
$currPswdError	= false;
$newPswdUsed	= false;

$db->connect();$db->begin();

if($userId=='')
{
	header("Location:${backwardseperator}login.php");
}

if(isset($_POST['txtOldPassword']))
{
	if(trim($oldPswd)=='')
	{
		$errorMsg	= 'Current password empty.';
	}
	else if(trim($newPswd)=='')
	{
		$errorMsg	= 'New password empty.';
	}
	else if(trim($conPswd)=='')
	{
		$errorMsg	= 'Confirm New password empty.';
	}
	else if($newPswd!=$conPswd)
	{
		$errorMsg	= 'Confirm New password does not match New password.';
	}
	else
	{
		$sys_users->set($userId);
		$error_atmpt_left	= $sys_users->getPASSWORD_ERROR_ATTEMPTS_LEFT();
		
		if($sys_users->getstrPassword() != md5(md5($oldPswd)))
		{
			if($sys_users->getintStatus()!=1 && $errorMsg=='')
			{ 
				$errorMsg = "Sorry! Your account has been disabled.";	
			}
			else if($error_atmpt_left=='')
			{
				$error_atmpt_left = $error_attempts-1;
			}
			else
			{
				$error_atmpt_left = $error_atmpt_left-1;
			}
			
			$sys_users->setPASSWORD_ERROR_ATTEMPTS_LEFT($error_atmpt_left);
				
			if($error_atmpt_left<=0)
			{
				$sys_users->setintStatus(10);
				$sys_users_disabled->insertRec($userId,$dateTimes->getCurruntDateTime());			
			}
			
			$sys_users->commit();
			
			if($errorMsg=='')
			{
				if($error_atmpt_left>0)
				{
					$currPswdError = true;
					$errorMsg 	= "Invalid UserName or Password. You are left with $error_atmpt_left more attempts";
				}
				else
				{
					$disableAcc = true;
					$errorMsg 	= "Sorry! Your account has been disabled.";
				}
			}
			$db->commit();
		}
		else
		{
			if($sys_users->getintStatus()!=1 && $errorMsg=='')
			{
				$errorMsg = "Sorry! Your account has been disabled.";
			}
			else
			{

				include "../../../class/administration/password/cls_password_get.php";
				include "../../../class/administration/password/cls_password_set.php";
				
				$obj_password_get	= new password_get($db);
				$obj_password_set	= new password_set($db);
				
				$chkPswdHistry		= $obj_password_get->checkPasswordHistory($newPswd,$userId);
				if($chkPswdHistry && $errorMsg=='')
				{
					$errorMsg = "New password already used with this account.";
					$newPswdUsed = true;
				}
				else
				{
					$sys_users->setintStatus(1);
					$sys_users->setPASSWORD_LAST_MODIFIED_DATE($dateTimes->getCurruntDateTime());
					$sys_users->setPASSWORD_ERROR_ATTEMPTS_LEFT($error_attempts);
					$sys_users->setstrPassword(md5(md5($newPswd)));
					
					$sys_users->commit();
					$obj_password_set->savePasswordHistory($userId,$newPswd);
					
					$db->commit(); 
					
					echo 'Password has changed successfully.';
					echo '<br>You are automatically redirec to login page.....';
					sleep(3);
					
					
					header("Location:${backwardseperator}login.php");
				}
			}
		}
	}
}
$db->disconnect();
$db	= NULL;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Password Change</title>

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>

<link rel="stylesheet" href="../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../libraries/validate/template.css" type="text/css">

<script src="../../../libraries/validate//jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>

<script type="application/javascript">
function pageSubmit()
{
	if($('#frmChangeExpirePswd').validationEngine('validate'))
		document.frmChangeExpirePswd.submit();	
}
function pageCancle()
{
	window.location.href = "<?php echo$backwardseperator;?>login.php";
}
</script>

<style>
.bordered_notRound {
	border-left: 1px solid #ccc;
    border-right: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
	border-top: 1px solid #ccc;
	font-size:11px;
	font-family: Verdana;
}
.redColor {	background-color:;
	font-family: Verdana;
	font-size: 12px;
	color: #F00;
	font-weight: bold;
}
body {
	background: #eeeeee url(<?php echo $backwardseperator ?>images/bg.jpg) top left repeat;
}
</style>
</head>
<body>
<form id="frmChangeExpirePswd" name="frmChangeExpirePswd" method="post">
<table width="500" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr class="normalfnt">
    	<td height="135">&nbsp;</td>
    </tr>
    <tr>
    	<td>
            <table width="100%" border="0" class="bordered_notRound" align="center" bgcolor="#FFFFFF">
            <tr>
            	<td height="25" class="normalfnt" style="color:#FFF;font-size:14px" bgcolor="#6E7B8B"><strong>&nbsp;&nbsp;Change your Password</strong></td>
            </tr>
            <tr class="normalfnt">
            	<td><table width="100%" border="0" cellspacing="4">
            	  <tr>
            	    <td colspan="3"><ul><li>Your password has expired. Please change your password.</li>
            	    <li>A good password should contain a mix of capital and lower-case letters, numbers and symbols.</li></ul></td>
           	      </tr>
            	  <tr>
            	    <td colspan="3"></td>
          	    </tr>
            	  <tr>
            	    <td width="8%">&nbsp;</td>
            	    <td width="31%">Current Password <span class="compulsoryRed">*</span></td>
            	    <td width="61%"><input type="password" name="txtOldPassword" id="txtOldPassword" style="width:250px;<?php echo(($currPswdError)?'border-color:#CD0000':''); ?>" class="validate[required,maxSize[20],minSize[4]]"/></td>
          	    </tr>
            	  <tr>
            	    <td>&nbsp;</td>
            	    <td>New Password <span class="compulsoryRed">*</span></td>
            	    <td><input type="password" name="txtNewPassword" id="txtNewPassword" style="width:250px;<?php echo(($newPswdUsed)?'border-color:#CD0000':''); ?>" class="validate[required,maxSize[20],minSize[4]]" /></td>
          	    </tr>
            	  <tr>
            	    <td>&nbsp;</td>
            	    <td>Confirm Password <span class="compulsoryRed">*</span></td>
            	    <td><input type="password" name="txtConfirmPassword" id="txtConfirmPassword" style="width:250px" class="validate[required,maxSize[20],minSize[4],equals[txtNewPassword]]" /></td>
          	    </tr>
            	  <tr>
            	    <td>&nbsp;</td>
            	    <td colspan="2" class="clsErrorMsg redColor"><?php echo $errorMsg; ?></td>
           	      </tr>
            	  <tr>
                    <td colspan="3" height="32" >
                        <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                        <tr>
                        <td align="center"><a class="button white medium" id="butChange" name="butChange" onclick="pageSubmit()">Change</a><a class="button white medium" id="butCancle" name="butCancle" onclick="pageCancle()">Cancel</a></td>
                        </tr>
                        </table>
                    </td>
           	      </tr>
          	  </table></td>
            </tr>
            </table>
        </td>
    </tr>
</table>
</form>
</body>
</html>
<?php
	if($disableAcc)
	{
?>
		<script>
			setTimeout('pageCancle();',2000);
		</script>
<?php
	}
?>
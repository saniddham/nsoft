<?php
 include "class/administration/password/cls_password_get.php";
 $obgPasswordGet = new password_get($db);

?>
<title>Change Password</title>
<form id="frmPassword" name="frmPassword">
<div align="center">
  <div class="trans_layoutD">
    <div class="trans_text">Change Password</div>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="21%">&nbsp;</td>
    <td width="41%">&nbsp;</td>
    <td width="9%">&nbsp;</td>
    <td width="9%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Department</td>
    <td><select style="width:150px" name="cboDepartmentSearch" id="cboDepartmentSearch">
    <?php
	echo $obgPasswordGet->getDepartment_option();
	?>
    </select></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">User</td>
    <td><select style="width:150px" name="cboUserSearch" id="cboUserSearch" class="validate[required]">
    <?php
		echo $obgPasswordGet->loadUser($departmentId);
	?>
    </select></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">New Passwrod <span class="compulsoryRed">*</span></td>
    <td><input type="password" name="txtNewPwd" id="txtNewPwd" class="validate[required,minSize[4],maxSize[20]]"/></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Confirm Password <span class="compulsoryRed">*</span></td>
    <td><input type="password" name="txtConPwd" id="txtConPwd" class="validate[required,minSize[4],maxSize[20],equals[txtNewPwd]]"/></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5" align="center"><?php
                if($form_permision['add']||$form_permision['edit'])
				{
				?>
                  <img  style="display:" border="0" src="images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/>
                  <?php
				}
				?></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
    </table>

</div>
</div>
</form>
<?php
// ini_set('display_errors',1);
 $backwardseperator = "../../../";
 include "{$backwardseperator}dataAccess/Connector.php";
 include "../../../class/administration/password/cls_password_get.php";
 include "../../../class/administration/password/cls_password_set.php";
 $objPasswordSet = new password_set($db);
 $objPasswordGet = new password_get($db);

 $requestType = $_REQUEST['requestType'];
 
 /////////////////////////////// load user/////////////////////////////
 if($requestType=="getUsersDepartmentWise"){
	 
	 ///////// load//////
	$response['users'] =$objPasswordGet->loadUser($_REQUEST['departmentId']);
	echo json_encode($response);
	
 }
 
 if($requestType=="changePassword"){
	 
	 /////////// get values////////////
	
	$password 	= md5(md5($_REQUEST['password']));	
	$userId		= $_REQUEST['userId']; 
	
	///////// change password///////
	$chkPswdHistry	= $objPasswordGet->checkPasswordHistory($password,$userId);
	if($chkPswdHistry)
	{
		$response['type']	 = "fail";
		$response['msg']	 = "New password already used with this account.";
	}
	else
	{
		$objPasswordSet->savePasswordHistory($userId,$password);
		
		$response 			= $objPasswordSet->updatePassword($password,$userId);
		if($response['type']=='pass')
		{
			$toUserDetails		= $objPasswordGet->getUserDetails($userId);
			
			$toUserName			= $toUserDetails['userName'];
			$toUserEmail		= $toUserDetails['userEmail'];
			$toUserFullName		= $toUserDetails['fullName'];
			
			$fromUserName		= 'NSOFT - SCREENLINE HOLDINGS.';
			$fromUserFullName	= 'Administrator';
			$fromUserEmail		= '';
	
			include "password_change_mail_template.php";
		}
	}	
	echo json_encode($response);
 }

?>
<?php
include "{$backwardseperator}libraries/mail/mail.php";
ob_start();	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Pre-Production Email</title>
<style type="text/css">
.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntWhite {
	font-family: Verdana;
	font-size: 11px;
	color: #FFF;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntGreen {
	font-family: Verdana;
	font-size: 11px;
	color: #00CD66;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.tableBorder_allRound{
	
	border: 1px solid #F08080;
	-moz-border-radius-bottomright:10px;
	-moz-border-radius-bottomleft:10px;
	-moz-border-radius-topright:10px;
	-moz-border-radius-topleft:10px;
}
</style>
</head>

<body>
<table  width="642" border="0" cellspacing="0" cellpadding="0" class="tableBorder_allRound">
  <tr>
    <td colspan="3" class="normalfnt" style="color:#3E437D">&nbsp;</td>
  </tr>
  <tr>
    <td width="16" class="normalfnt">&nbsp;</td>
    <td width="609"><span class="normalfnt">Dear <strong><?php echo $toUserFullName; ?></strong>,</span></td>
    <td width="15">&nbsp;</td>
  </tr>
  <tr>
    <td width="16" class="normalfnt">&nbsp;</td>
    <td width="609">&nbsp;</td>
    <td width="15">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">The password for your Nsoft ERP Account - '<span class="normalfntGreen"><?php echo $toUserName; ?></span>' - was recently changed. If you made this change, you don't need to do anything more.<br /><br />If you didn't change your password, your account might have been hacked. To get back into your account, you'll need to reset your password. To reset your password contact the Administrator.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="normalfnt">&nbsp;</td>
    <td><span class="normalfnt">Thanks,</span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="normalfnt"></td>
    <td class="normalfnt"><strong><?php echo $fromUserFullName; ?></strong><br />
    ...................</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
  <td class="normalfnt"></td>
    <td class="normalfnt">(This is a<strong> <span style="color:#0000FF">Nsoft</span> </strong>system generated email.)</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="normalfnt" style="color:#3E437D">&nbsp;</td>
  </tr>
</table>
</body>
</html>
<?php

	$body = ob_get_clean();
	//echo $body;
	echo sendMessage($fromUserEmail,$fromUserName,$toUserEmail,"Nsoft ERP Account password changed",$body);
		
?>
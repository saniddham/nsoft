// JavaScript Document
var basePath = 'presentation/administration/password/';
$(document).ready(function() {
  		$("#frmPassword").validationEngine();
		$('#butSave').die('click').live('click',updatePassword);
		$('#cboDepartmentSearch').die('change').live('change',loadUser);

});

function updatePassword(){
	
	//////////////////////get control values////////////////////////
	if(!$("#frmPassword").validationEngine('validate'))
		return;
	var strPassword	= $('#txtNewPwd').val();
		intUserId	= $('#cboUserSearch').val();
		
		/////////// sent ajax///////////
	var testUrl = basePath+'password-db.php?requestType=changePassword';
		testUrl+= '&password='+strPassword;
		testUrl+= '&userId='+intUserId;
		
		$.ajax({url:testUrl,
				async:false,
				dataType:'json',
				type:'POST',
				success:function(json){
					$("#frmPassword #butSave").validationEngine('showPrompt',json.msg,json.type)
					resetForm('#frmPassword');
				}
		});
}

function loadUser(){
	///////////////////////////////////////////////////
	///////////////get user names//////////////////
	///////////////////////////////////////////////////
	
	var intDepartmentId = $('#cboDepartmentSearch').val();
	
	/////////////// ajax sent//////////////////////
	var testUrl = basePath+'password-db.php?requestType=getUsersDepartmentWise';
		testUrl+= '&departmentId='+intDepartmentId ;
		
		$.ajax({url:testUrl,
				async:false,
				dataType:'json',
				type:'POST',
				success:function(json){
					$('#cboUserSearch').html(json.users);
				}
		});
	
}

function resetForm(form) {
    $(form).find('input:text, input:password, input:file, select, textarea').val('');
     $(form).find('input:radio, input:checkbox')
         .removeAttr('checked').removeAttr('selected');
}
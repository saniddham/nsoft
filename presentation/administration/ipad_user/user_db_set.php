<?php
$backwardseperator			= '../../../';		
include "../../../dataAccess/Connector.php";
include "../../../class/cls_commonFunctions_get.php";
include "user_db_get.php";

$_SESSION['ROOT_PATH']		= $_SERVER['DOCUMENT_ROOT'].'/nsoft/';
$_SESSION["projectName"] 	= 'nsoft';
$companyID					= $_SESSION["headCompanyId"];
$srDatabase 				= $_SESSION["SRDatabese"];				
$currDate					= date('Y-m-d');
$key						= 'I;p&a^d@s!a+m*p%l$e#R{e*q(u&i?s&i#t@i%o^n*S}y:s]t*e&m' ;
$requestType				= $_REQUEST['requestType'];
$savedMasseged				= "";
$error_sql					= "";
$savedStatus				= true;
$editMode					= false;
$obj_commonFunctions_get	= new cls_commonFunctions_get($db);
$obj_getUser				= new cls_user_get($db);

if($requestType == 'saveData')
{
	$arrHeader 		= json_decode($_REQUEST['arrHeader'],true);
	$searchID		= $arrHeader['searchID'];
	$fullName		= $arrHeader['fullName'];
	$userName		= $arrHeader['userName'];
	$password		= $arrHeader['password'];
	$rePassword		= $arrHeader['rePassword'];
	$contactNo		= $arrHeader['contactNo'];
	$designation	= $arrHeader['designation'];
	$gender			= $arrHeader['gender'];
	$userType		= $arrHeader['userType'];
	$department		= $arrHeader['department'];
	$email			= $arrHeader['email'];
	$remark			= $obj_commonFunctions_get->replace($arrHeader['remark']);	
	$status			= $arrHeader['status'];
	
	$db->begin();
	$sql	= " SELECT 	
					PASSWORD_ERROR_ATTEMPTS
						 
					FROM 
					sys_config 
					
					WHERE	
					intCompanyId='$companyID'";
		//die($sql);
	$result	= $db->RunQuery2($sql);
	$row	= mysqli_fetch_array($result);
	
	$sql1		= " SELECT 	intId
		 			FROM 
					ipad_sample.mst_locations "	;
	
	$result1	= $db->RunQuery2($sql1);
	$row1		= mysqli_fetch_array($result1);	
	$locationID	= $row1['intId'];
			
	if($searchID=='')
	{
		$query	= saveUser_sql($userName,$password,$fullName,$department,$contactNo,$designation,$gender,$email,$remark,$status,$row['PASSWORD_ERROR_ATTEMPTS'],$userType);
		$dataArr = $db->RunQuery2($query);
		$insertId = $db->insertId;
		if($dataArr['savedStatus']=='fail' && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged	= $dataArr['savedMassege'];
			$error_sql		= $dataArr['error_sql'];
		}	
		else
		{
			$query4	= saveToLocationUser_sql($insertId,$locationID);
			$dataArr = $db->RunQuery2($query4);
			if($dataArr['savedStatus']=='fail' && ($savedStatus))
			{
				$savedStatus	= false;
				$savedMasseged	= $dataArr['savedMassege'];
				$error_sql		= $dataArr['error_sql'];
			}
				
		}	
	}
	else
	{
		$editMode	= true;
		$query1		= updateUser_sql($searchID,$userName,$password,$fullName,$department,$contactNo,$designation,$gender,$email,$remark,$status,$row['PASSWORD_ERROR_ATTEMPTS'],$userType);
		$dataArr = $db->RunQuery2($query1);
			if($dataArr['savedStatus']=='fail' && ($savedStatus))
			{
				$savedStatus	= false;
				$savedMasseged	= $dataArr['savedMassege'];
				$error_sql		= $dataArr['error_sql'];
			}	
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		if($editMode)
			$response['msg']	= "Updated Successfully.";
		else
			$response['msg']	= "Saved Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);
		
}
else if($requestType=='searchData')
{
	$searchId	 = $_REQUEST['searchId'];
	
	$query	 = searchUser_sql($searchId);
	$result	 = $db->RunQuery($query);
	$row	 = mysqli_fetch_array($result);
	
	$response['userName'] 		= $row['strUserName'];
	$response['fullName'] 		= $row['strFullName'];
	//$response['Password'] 		= $row['strPassword'];
	$response['department'] 	= $row['intDepartmentId'];
	$response['contactNo'] 		= $row['strContactNo'];
	$response['designation']	= $row['strDesignation'];
	$response['gender'] 		= $row['strGender'];
	$response['email']  		= $row['strEmail'];
	$response['remark'] 		= $row['strRemark'];
	$response['errorAttempt'] 	= $row['PASSWORD_ERROR_ATTEMPTS_LEFT'];
	$response['userType'] 		= $row['USER_TYPE'];
	$response['status'] 	    = ($row['intStatus']==1?true:false);
	
	echo json_encode($response);	
}
else if($requestType=='loadUser')
{
	$searchID	= $_REQUEST['searchId'];
	$result 	= $obj_getUser->getUser();
	
	$html		 = "<option value=\"\"></option>";
	while($row = mysqli_fetch_array($result))
	{
	  $html .= "<option value=\"".$row['intUserId']."\">".$row['strUserName']."</option>";
	}
	echo $html;
}
else if($requestType == 'deleteUser')
{
	$searchID	 = $_REQUEST['searchId'];
	
	$db->begin();
	
	$query3			= deleteUser_sql($searchID);
	$dataArr 		= $db->RunQuery2($query3);
	
	if($dataArr['savedStatus']=='fail' && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged	= $dataArr['savedMassege'];
		$error_sql		= $dataArr['error_sql'];
	}
	
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg']		= "Deleted Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);
	
}
//sql query functions
function saveUser_sql($username,$Password,$fullName,$department,$contactNo,$designation,$gender,$email,$remark,$status,$errorAttempt,$userType)
	{
		global $srDatabase;
		global $key;
		//die($errorAttempt);
		$sql			= "INSERT INTO $srDatabase.sys_users 
							( 
							strUserName, 
							strPassword, 
							strFullName, 
							intDepartmentId, 
							strContactNo, 
							strDesignation, 
							strGender, 
							strEmail, 
							strRemark, 
							intStatus, 
							intHigherPermision,
							PASSWORD_ERROR_ATTEMPTS_LEFT, 
							USER_TYPE
							)
							VALUES
							( 
							'$username', 
							'".md5(md5(md5($Password.$key)))."', 
							'$fullName', 
							'$department', 
							'$contactNo', 
							'$designation', 
							'$gender', 
							'$email', 
							'$remark', 
							'$status', 
							'0', 
							'$errorAttempt',
							'$userType'
							)";	
		//die($sql);
		return $sql;
	}	
	
	function updateUser_sql($searchID,$username,$Password,$fullName,$department,$contactNo,$designation,$gender,$email,$remark,$status,$errorAttempt,$userType)
	{
		global $srDatabase;
		$sql	= " UPDATE $srDatabase.sys_users 
					SET
					strUserName = '$username' , 
					strFullName = '$fullName' , 
					intDepartmentId = '$department' , 
					strContactNo = '$contactNo' , 
					strDesignation = '$designation' , 
					strGender = '$gender' , 
					strEmail = '$email' , 
					strRemark = '$remark' , 
					intStatus = '$status' , 
					PASSWORD_ERROR_ATTEMPTS_LEFT = '$errorAttempt' , 
					USER_TYPE = '$userType'
					
					WHERE
					intUserId = '$searchID'" ;
		return $sql;
	}
	function searchUser_sql($userID)
	{
		global $srDatabase;
		$sql	= "	SELECT 	intUserId, 
					strUserName, 
					strPassword, 
					strFullName, 
					intDepartmentId, 
					strContactNo, 
					strDesignation, 
					strGender, 
					strEmail, 
					strRemark, 
					intStatus, 
					PASSWORD_ERROR_ATTEMPTS_LEFT, 
					USER_TYPE
					 
					FROM 
					$srDatabase.sys_users su
					WHERE
					su.intUserId='$userID'";
					//die($sql);
		return $sql;
	}
	function deleteUser_sql($searchID)
	{
		global $srDatabase;
		$sql	= " DELETE FROM $srDatabase.sys_users 
					WHERE
					intUserId = '$searchID'";	
		//die ($sql);
		return $sql;
	}
	function saveToLocationUser_sql($userID,$locationID)
	{
		global $srDatabase;
		$sql	= "INSERT INTO $srDatabase.mst_locations_user 
					(intUserId, 
					intLocationId
					)
					VALUES
					('$userID', 
					'$locationID')";	
		//die($sql);	
		return $sql;
	}
?>
$(document).ready(function(e) {
    $("#frmManageUser").validationEngine(); 
	$('#frmManageUser #butSave').live('click',SaveUser);
	$('#frmManageUser #cboSearch').live('change',searchUser);
	$('#frmManageUser #butNew').live('click',clearAll);
	$('#frmManageUser #butDelete').live('click',deleteData);
	
});
function SaveUser()
{
	var searchID	= $('#cboSearch').val();
	var fullName	= $('#txtFullName').val();
	var userName	= $('#txtLoginId').val();
	var password	= $('#password').val();
	var rePassword	= $('#password2').val();
	var contactNo	= $('#txtContactNo').val();
	var designation	= $('#txtDesignation').val();
	if($('#rdoMale').attr('checked'))
			var gender = 'M';
		else
			var gender = 'F';
	var userType	= $('#cboUserType').val();
	var department	= $('#cboDepartment').val();
	var	email		= $('#txtEmail').val();
	var remark		= $('#txtRemarks').val();
	var status		= ($('#chkActive1').attr('checked')?1:10);
	
	if($('#frmManageUser').validationEngine('validate'))
	{
		var data = "requestType=saveData";
		var arrHeader = "{";
							arrHeader += '"searchID":"'+searchID+'",' ;
							arrHeader += '"fullName":"'+fullName+'",';
							arrHeader += '"userName":"'+userName+'",';
							arrHeader += '"password":"'+password+'",' ;
							arrHeader += '"rePassword":"'+rePassword+'",' ;
							arrHeader += '"contactNo":"'+contactNo+'",' ;
							arrHeader += '"designation":"'+designation+'",' ;
							arrHeader += '"gender":"'+gender+'",' ;
							arrHeader += '"userType":"'+userType+'",' ;
							arrHeader += '"department":"'+department+'",' ;
							arrHeader += '"email":"'+email+'",' ;
							arrHeader += '"remark":'+URLEncode_json(remark)+',' ;
							arrHeader += '"status":"'+status+'"' ;
		
			arrHeader += "}";
		
		data+="&arrHeader="+arrHeader;
		var url = "user_db_set.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					//alert ('ok');
					$('#frmManageUser #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmManageUser').get(0).reset();
						loadSearchCombo();
						clearAll();
						var t = setTimeout("alertx('butSave')",1000);
						return;
					}
					else
					{
						
					}
				},
				error:function(xhr,status){
						
						$('#frmManageUser #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						return;
				}		
		});
	}
	else
		var t = setTimeout("alertx1()",5000);
		
}

function searchUser()
{
	var searchId		= $('#cboSearch').val();
	if(searchId=='')
	{
		clearAll();
		return;
	}
	
	var data = "requestType=searchData";
	data	+= "&searchId="+searchId;
	
	var url  = "user_db_set.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					
					$('#txtFullName').val(json.fullName);
					$('#txtLoginId').val(json.userName);
					$('#frmManageUser #password').val('password');
					$('#frmManageUser #password').attr('disabled',true);
					$('#frmManageUser #password2').val('password');
					$('#frmManageUser #password2').attr('disabled',true);
					$('#txtContactNo').val(json.contactNo);
					$('#txtDesignation').val(json.designation);
					if(json.gender=='M')
						$('#rdoMale').attr('checked',true);
					else
						$('#rdoFemale').attr('checked',true);
					
					$('#cboUserType').val(json.userType);
					$('#cboDepartment').val(json.department);
					$('#txtEmail').val(json.email);
					$('#txtRemarks').val(json.remark);
					$('#chkActive1').attr('checked',json.status);
				
				}
		});		
}
function deleteData()
{
	var searchId	 = $('#cboSearch').val();
	if(searchId=='')
	{
		$('#frmManageUser #butDelete').validationEngine('showPrompt', 'Please select the user.', 'fail');
		var t = setTimeout("alertx('butDelete')",1000);
		return;
	}
	var val = $.prompt('Are you sure you want to delete "'+$('#frmManageUser #cboSearch option:selected').text()+'" ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
						var url = "user_db_set.php";
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							data:'requestType=deleteUser&searchId='+searchId,
							async:false,
							success:function(json){
								
								$('#frmManageUser #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									$('#frmManageUser').get(0).reset();
									loadSearchCombo();
									clearAll();
									var t=setTimeout("Deletex()",1000);
									return;
								}	
							}	 
						});
					}
					}
	});	

}
function clearAll()
{
	$('#frmManageUser').get(0).reset();
	$('#frmManageUser #password').attr('disabled',false);
	$('#frmManageUser #password2').attr('disabled',false);
	loadSearchCombo();
}
function alertx(button)
{
	$('#frmManageUser #'+button).validationEngine('hide')	;
}

function alertx1(button)
{
	$('#frmManageUser').validationEngine('hide')	;
}

function Deletex()
{
	$('#frmManageUser #butDelete').validationEngine('hide')	;
}
function loadSearchCombo()
{
	var url 	= "user_db_set.php?requestType=loadUser";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmManageUser #cboSearch').html(httpobj.responseText);
}
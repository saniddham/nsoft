<?php
class cls_user_get
{
	private $db;

	function __construct($db)
	{
		$this->db = $db;
	}
	public function getUser()
	{
		return $this->getUser_sql();
	}	
	public function getDepartment()
	{
		return $this->getDepartment_sql();	
	}
	private function getUser_sql()
	{
		$sql 	= " SELECT 	intUserId, 
					strUserName
						 
					FROM 
					ipad_sample.sys_users 
					
					ORDER BY
					intUserId";	
		$result	= $this->db->RunQuery($sql);
		return $result;
	}	
	private function getDepartment_sql()
	{
		$sql = "SELECT
				mst_department.intId,
				mst_department.strName
				FROM nsoft.mst_department
				WHERE
				mst_department.intStatus =  '1' ";
	
       //die($sql);
	   $result = $this->db->RunQuery($sql);
	   return $result;
	}
}
?>
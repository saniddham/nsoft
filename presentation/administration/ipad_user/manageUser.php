<?php
session_start();
$backwardseperator = "../../../";
$srDatabase = $_SESSION["SRDatabese"];

//include  "{$backwardseperator}dataAccess/permisionCheck.inc";
include "../../../dataAccess/Connector.php";
include "user_db_get.php";
$obj_getUser	= new cls_user_get($db);
$result			= $obj_getUser->getUser();
$department 	= $obj_getUser->getDepartment();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Manage User</title>

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />


<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="user-js.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>
<script type="text/ecmascript" src="manageUser_js.js"></script>

<link rel="stylesheet" href="../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../libraries/validate/template.css" type="text/css">
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form id="frmManageUser" name="frmManageUser" method="post" action="">


<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div align="center">
		<div class="trans_layoutD">
		  <div class="trans_text">Manage User</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="585" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
              <tr>
                <td width="1" class="normalfnt">&nbsp;</td>
                <td width="149" class="normalfnt">User</td>
                <td colspan="3">
                  <select name="cboSearch" class="txtbox" id="cboSearch"   style="width:361px" tabindex="1">
                  <option value=""></option>
                  <?php
				  while($row = mysqli_fetch_array($result))
				  {
					  echo "<option value=\"".$row['intUserId']."\">".$row['strUserName']."</option>";
				  }
				  ?>
                  </select>	</td>                
                </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td width="138">&nbsp;</td>
                <td width="38" class="normalfnt">&nbsp;</td>
                <td width="225">&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Full Name&nbsp;<span class="compulsoryRed">*</span></td>
                <td colspan="3"><input name="txtFullName" type="text" class="validate[required,maxSize[100]]" id="txtFullName" style="width:360px" maxlength="50" tabindex="3"/></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt"><strong>System User Name</strong>&nbsp;<span class="compulsoryRed">*</span></td>
                <td colspan="3"><input name="txtLoginId" type="text" class="validate[required,maxSize[20]]" id="txtLoginId" style="width:200px" maxlength="50" tabindex="3"/></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt"><strong>Password</strong> <span class="compulsoryRed">*</span></td>
                <td colspan="3"><input name="password" type="password" class="validate[required,maxSize[20],minSize[4]]" id="password" style="width:200px" maxlength="50" tabindex="3"/></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt"><strong>Re-enter password</strong> <span class="compulsoryRed">*</span></td>
                <td colspan="3"><input name="password2" type="password" class="validate[required,maxSize[20],minSize[4],equals[password]]" id="password2" style="width:200px" maxlength="50" tabindex="3"/></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Contact No</td>
                <td colspan="3"><input name="txtContactNo" type="text" class="txtbox" id="txtContactNo" style="width:360px" maxlength="50" tabindex="5"/></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Designation</td>
                <td colspan="3"><input name="txtDesignation" type="text" class="txtbox" id="txtDesignation" style="width:360px" maxlength="50" tabindex="5"/></td>
                </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Gender</td>
                <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr class="normalfnt">
                    <td width="27%">Male</td>
                    <td width="20%"><input name="rdoGender" type="radio" id="rdoMale" value="radio" checked="checked" /></td>
                    <td width="38%">Female</td>
                    <td width="15%"><input type="radio" name="rdoGender" id="rdoFemale" value="radio" /></td>
                  </tr>
                </table></td>
                <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="158">&nbsp;</td>
                    <td width="62">&nbsp;</td>
                    </tr>
                  </table></td>
              </tr>
              
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td valign="top" class="normalfnt">User Type <span class="compulsoryRed">*</span></td>
                <td colspan="3"><select class="validate[required]" name="cboUserType" id="cboUserType"   style="width:200px" tabindex="1">
                <option value="M">Mesa</option>
                <option value="B">Brandix</option>
                </select></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Department <span class="compulsoryRed">*</span></td>
                <td colspan="3"><select class="validate[required]" name="cboDepartment" id="cboDepartment"   style="width:200px" tabindex="1">
                <option value=""></option>
				<?php  
                    while($row=mysqli_fetch_array($department))
                    {
                        echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
                    }
                   ?> 
                </select></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">E-Mail  <span class="compulsoryRed">*</span></td>
                <td colspan="3"><input name="txtEmail" tabindex="10" type="text" class="validate[custom[email],required]" id="txtEmail" style="width:360px" maxlength="100"/></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Remarks</td>
                <td colspan="3">
                  <textarea name="txtRemarks" style="width:360px"  rows="2" class="txtbox" id="txtRemarks"></textarea>				</td>
              </tr>
              <tr>
              	<td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Active</td>
                <td><input type="checkbox" name="chkActive1" id="chkActive1" checked="checked" tabindex="24"/></td>
                <td class="normalfnt">&nbsp;</td>
                <td></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="2"></td>
                </tr>
              </table></td>
            </tr>
          <tr>
        <td width="100%" align="center" bgcolor=""><a class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butSave" name="butSave">Save</a><a class="button white medium" id="butDelete" name="butDelete">Delete</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
            			
      </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>

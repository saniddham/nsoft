<?php
///////////////////////////////
// H B G KORALA   ////////////
//////////////////////////////
	session_start();
	$backwardseperator = "../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$requestType 	= $_REQUEST['requestType'];
	$programName='Item Inspection';
	$programCode='P0487';
	
	include "{$backwardseperator}dataAccess/Connector.php";
	
	include "../../../class/cls_commonFunctions_get.php";
	include "../../../class/administration/mailCopyUsers/cls_mailCopyUsers_get.php";
	include "../../../class/administration/mailCopyUsers/cls_mailCopyUsers_set.php";
	
	$objcomfunget= new cls_commonFunctions_get($db);
	$mailcopyusersget= new cls_mailCopyUsers_get($db);
	$mailcopyusersset= new cls_mailCopyUsers_set($db);
	
	$db->begin();
	if($requestType=='loadMainUsers')
	{ 
		$response['arrCombo'] 	= $objcomfunget->getUsers_options($location);
		echo json_encode($response);
	}
	else if($requestType=='loadGridData')
	{
		$response['arrData'] 	= $mailcopyusersget->getDetails($_REQUEST['user'],$location);
		echo json_encode($response);
	}
	else if($requestType=='loadDepartments')
	{ 
		$response['arrCombo'] 	= $objcomfunget->getDepartments_options();
		echo json_encode($response);
	}
	else if($requestType=='loadPopupGridData')
	{
		$response['arrData'] 	= $mailcopyusersget->getPopupDetails($_REQUEST['department'],$_REQUEST['mainUser'],$location);
		echo json_encode($response);
	}
	else if($requestType=='save')
	{
		$arrUser 	= json_decode($_REQUEST['arr'],true);	

		$mainUser 	= $_REQUEST['mainUser'];
		//////// delete records /////////////////
		$mailcopyusersset->delete_sys_mail_copy_users($mainUser);
		
		/////////////////////////////////////////
		$toSave=0;
		$saved=0;
		$rollBackFlag=0;
		foreach($arrUser as $user)
		{
			$toSave++;
			$response['arrData'] = $mailcopyusersset->insert_sys_mail_copy_users($mainUser,$user);
			$saved+=$response['arrData']['result'];
			$sqlString.=$response['arrData']['q'];
			$msg.=$response['arrData']['msg'];
		}
		
			if($rollBackFlag==1){
				$db->rollback();
				$response['type'] 		= 'fail';
				$response['msg'] 		= $rollBackMsg;
				$response['q'] 			= '';
			}			
			else if(($toSave==$saved)){
				$db->commit();
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Saved successfully.';
			}
			else{
				$db->rollback();	
				$response['type'] 		= 'fail';
				//$response['msg'] 		= $db->errormsg;
				$response['msg'] 		= $msg;
				$response['q'] 			= $sqlString;
			}
		echo json_encode($response);
			 	
	}
	
	
	$db->commit();
	
?>
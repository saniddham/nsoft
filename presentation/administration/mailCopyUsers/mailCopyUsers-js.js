var basePath = 'presentation/administration/mailCopyUsers/';			
$(document).ready(function() {
	
	loadMainUsers();
  //-------------------------------------------------------
  $('#frmMailCopyUsers #cboMainUser').die('change').live('change',function(){
		loadGridData();
  });
  //-------------------------------------------------------
	$("#frmMailCopyUsers #butAdd").die('click').live('click',function(){
		setToAddNew();
	});
  //-------------------------------------------------------
	$('#frmMailCopyUsers .removeRow').die('click').live('click',function(){
		$(this).parent().parent().remove();
	});	
  //-------------------------------------------------------
  $('#frmPopUp #cboDepartment').die('change').live('change',function(){
		loadPopupGridData();
  });
  //-------------------------------------------------------
  $('#frmPopUp .chkAll').die('click').live('click',function(){
		if(document.getElementById('chkAll').checked==true)
		 var chk=true;
		else
		 var chk=false;
		
		var rowCount = document.getElementById('tblTechnique').rows.length;
		for(var i=1;i<rowCount;i++)
		{
			document.getElementById('tblTechnique').rows[i].cells[0].childNodes[0].checked=chk;
		}
	});
  //-------------------------------------------------------
function setToAddNew()
{
	var mainUser= $('#frmMailCopyUsers #cboMainUser').val();
	if(mainUser==''){
		alert("Please select user");
		return false;
	}
	popupWindow3('1');
	$('#popupContact1').html('');
	$('#popupContact1').load(basePath+'popup.php?mainUser='+mainUser,function(){
	loadDepartments();
		
		$('#frmPopUp #butAdd').die('click').live('click',function(){
			//addEmailsToGrid(mainRowId,typeRowId,cellId);
			$('#frmPopUp .chkUsers:checked').each(function(){
				var fromId = $(this).val();
				var found = false;
				$('#frmMailCopyUsers #tblMain .user').each(function(){
					if(fromId ==$(this).attr('id'))
					{
						found = true;
					}	
				});
				if(found==false)
				{
					
					document.getElementById('tblMain').insertRow(document.getElementById('tblMain').rows.length);
					document.getElementById('tblMain').rows[document.getElementById('tblMain').rows.length-1].id=$(this).val();
					document.getElementById('tblMain').rows[document.getElementById('tblMain').rows.length-1].innerHTML = "<td align=\"center\" bgcolor=\"#FFFFFF\" id="+$(this).val()+">"+
					"<img class=\"mouseover removeRow\" src=\"images/del.png\" /></td>"+
   		 			"<td bgcolor=\"#FFFFFF\" class=\"normalfnt user\" id="+$(this).val()+">"+$(this).parent().parent().find('td').eq(1).html()+"</td>"+
    				"<td bgcolor=\"#FFFFFF\" class=\"normalfnt\" id="+$(this).val()+">"+$(this).parent().parent().find('td').eq(2).html()+"</td>";
					document.getElementById('tblMain').rows[document.getElementById('tblMain').rows.length-1].id  = fromId;
					
				}
			});

			disablePopup();
			
		});
		
		$('#frmPopUp #butClose1').die('click').live('click',disablePopup);
		
	});		
	
}
  //-------------------------------------------------------
function alertx()
{
	$('#frmMailCopyUsers #butSave').validationEngine('hide')	;
}
  //-------------------------------------------------------
 
  $('#frmMailCopyUsers #butSave').die('click').live('click',function(){
	var mainUser= $('#cboMainUser').val();
		
		if ($('#frmMailCopyUsers').validationEngine('validate'))   
    	{ 
		var data = "requestType=save";
		
			data+="&mainUser="	+mainUser;
			var arr="[";
			
			$('#frmMailCopyUsers #tblMain .user').each(function(){
				
				var userId	= $(this).attr('id');
/*					arr += "{";
					arr += '"userId":"'+	userId  ;
						arr +=  '},';
*/	
					arr += userId +" ,"  ;
			});
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;
		
		var url = basePath+"mailCopyUsers-db.php?";
		$.ajax({
			url:url,
			async:false,
			dataType: "json", 
			type:'POST',
			data:data,
			success:function(json){
					$('#frmMailCopyUsers #butSave').validationEngine('showPrompt', json.msg,json.type );
					if(json.type=='pass')
					{
						var t=setTimeout("alertx()",3000);
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmMailCopyUsers #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				}		
		});
		
		}
	});	
   
  //-------------------------------------------------------
	$('#frmMailCopyUsers .confirm').die('click').live('click',function(){
		
			if(validateQuantities(this)==true){
				var grnNo=$('#cboGRNNo').val();
				var grnYear=$('#cboGRNYear').val();
				var itemId=$(this).parent().parent().find(".item").attr('id');
				var itemName=$(this).parent().parent().find(".item").html();
				var goodQty	= $(this).parent().parent().find(".goodQty").val();
				var dammagedQty	= $(this).parent().parent().find(".dammagedQty ").val();
				
				var val = $.prompt('Are you sure you want to confirm this inspection ?',{
					buttons: { Ok: true, Cancel: false },
					callback: function(v,m,f){
						if(v)
						{
							var data = "grnNo="+grnNo+"&grnYear="+grnYear+"&itemId="+itemId+"&itemName="+itemName+"&goodQty="+goodQty+"&dammagedQty="+dammagedQty+"&requestType=approve";
							var url = basePath+"itemInspection-db.php";
							var obj = $.ajax({
								url:url,
								type:'POST',
								dataType: "json",  
								data:data,
								async:false,
								
								success:function(json){
					$('#frmMailCopyUsers #butConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
										if(json.type=='pass')
										{
											var t=setTimeout("alertx()",1000);
											return;
										}
									},
								error:function(xhr,status){
										
										$(this).parent().parent().find("#butConfirm").validationEngine('showPrompt', errormsg(xhr.status),'fail');
					//$('#frmMailCopyUsers #butConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
										var t=setTimeout("alertx()",3000);
									}		
								});
						}
					}});
			}
	});
  //-------------------------------------------------------
	
	//--------------refresh the form----------
	/*$('#frmGatePassTransferin #butNew').die('click').live('click',function(){
		$('#frmGatePassTransferin').get(0).reset();
		clearRows();
		$('#frmGatePassTransferin #txtGrnNo').val('');
		$('#frmGatePassTransferin #txtGrnYear').val('');
		$('#frmGatePassTransferin #txtInvioceNo').val('');
		$('#frmGatePassTransferin #cboPO').val('');
		$('#frmGatePassTransferin #txtDeliveryNo').val('');
		$('#frmGatePassTransferin #txtTotGrnAmmount').val('');
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmGatePassTransferin #dtDate').val(d);
	});*/

//-----------------------------------
/*$('#butReport').click(function(){
	if($('#cboGRNNo').val()!=''){
		window.open(basePath+'listing/rptItemInspection.php?grnNo='+$('#cboGRNNo').val()+'&year='+$('#cboGRNYear').val());	
	}
	else{
		alert("There is no Issue No to view");
	}
});
*///-----------------------------------------------------
/*$('#butClose').click(function(){
		//load('main.php');	
});*/
//--------------refresh the form-----------------------
	$('#frmMailCopyUsers #butNew').click(function(){
		$('#frmMailCopyUsers').get(0).reset();
		clearRows();
		$('#frmMailCopyUsers #txtGrnNo').val('');
		$('#frmMailCopyUsers #txtGrnYear').val('');
		$('#frmMailCopyUsers #txtInvioceNo').val('');
		$('#frmMailCopyUsers #cboPO').val('');
		$('#frmMailCopyUsers #cboPO').focus();
		$('#frmMailCopyUsers #txtDeliveryNo').val('');
		$('#frmMailCopyUsers #txtTotGrnAmmount').val('');
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmMailCopyUsers #dtDate').val(d);
	});
//-----------------------------------------------------
});

//----------end of ready -------------------------------

//-------------------------------------
function alertx()
{
	$('#frmMailCopyUsers #butSave').validationEngine('hide')	;
}
function alertx()
{
	$('#frmMailCopyUsers #butConfirm').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmMailCopyUsers #butDelete').validationEngine('hide')	;
}

//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblMain').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=2;i<rowCount;i++)
	{
		document.getElementById('tblMain').deleteRow(2);
	}
}
//----------------------------------------------- 
function clearRowsPopup()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblTechnique').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
		document.getElementById('tblTechnique').deleteRow(1);
	}
}
//----------------------------------------------------
function loadMainUsers(){
		var url 		= basePath+"mailCopyUsers-db.php?requestType=loadMainUsers";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:"",
			async:false,
			success:function(json){
					document.getElementById("cboMainUser").innerHTML=json.arrCombo;
			}
		});
}
//----------------------------------------------------
function loadGridData(){
	
		 clearRows();
	
	    var user = $('#frmMailCopyUsers #cboMainUser').val();
		var url 		= basePath+"mailCopyUsers-db.php?requestType=loadGridData";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:"user="+user,
			async:false,
			success:function(json){
				
				var length = json.arrData.length;
				var arrData = json.arrData;
				for(var i=0;i<length;i++)
				{
					var userId=arrData[i]['intUserId'];	
					var userName=arrData[i]['strUserName'];
					var email=arrData[i]['strEmail'];

					var content='<tr class="normalfnt" id="'+userId+'">';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+userId+'"><img class="mouseover removeRow" src="images/del.png"></td>';
					content +='<td align="left" bgcolor="#FFFFFF"  id="'+userId+'" class="normalfnt user">'+userName+'</td>';
					content +='<td align="left" bgcolor="#FFFFFF" id="'+userId+'">'+email+'</td>';
					content +='</tr>';
					
					add_new_row('#frmMailCopyUsers #tblMain',content);
					
				}
			}
		});
}
//----------------------------------------------------
function loadDepartments(){
		var url 		= basePath+"mailCopyUsers-db.php?requestType=loadDepartments";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:"",
			async:false,
			success:function(json){
					document.getElementById("cboDepartment").innerHTML=json.arrCombo;
			}
		});
}
//----------------------------------------------------
function loadPopupGridData(){
		 clearRowsPopup();
	
	    var department = $('#frmPopUp #cboDepartment').val();
		var mainUser= $('#frmMailCopyUsers #cboMainUser').val();

		var url 		= basePath+"mailCopyUsers-db.php?requestType=loadPopupGridData";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:"department="+department+"&mainUser="+mainUser,
			async:false,
			success:function(json){
				
				var length = json.arrData.length;
				var arrData = json.arrData;
				for(var i=0;i<length;i++)
				{
					var userId=arrData[i]['intUserId'];	
					var userName=arrData[i]['strUserName'];
					var email=arrData[i]['strEmail'];

					var content='<tr class="normalfnt">';
					content +='<td align="center" bgcolor="#FFFFFF" id=""><input id="chkDisp" class="chkUsers" type="checkbox" value="'+userId+'"></td>';
					content +='<td align="center" bgcolor="#FFFFFF"  id="'+userId+'">'+userName+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+email+'">'+email+'</td>';
					content +='</tr>';
					
					add_new_row('#frmPopUp #tblTechnique',content);
					
				}
			}
		});
}
//----------------------------------------------------


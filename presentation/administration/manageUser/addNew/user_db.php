<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
try
{
	$requestType 		= $_REQUEST['requestType']; // request parameter
	$programCode		= 'P0012'; // program code
	
	include_once "class/tables/menupermision.php";								$menupermision 				= new menupermision($db);
	include_once "class/tables/mst_locations_user.php";							$mst_locations_user 		= new mst_locations_user($db);
	include_once "class/tables/sys_users.php";									$sys_users 					= new sys_users($db);
	include_once "class/tables/mst_locations_user.php";							$mst_locations_user 		= new mst_locations_user($db);
	include_once "class/tables/mst_companies.php";								$mst_companies 				= new mst_companies($db);
	include_once "class/dateTime.php";											$dateTimes 					= new dateTimes($db);
	include_once "class/tables/sys_config.php";									$sys_config					= new sys_config($db);
	include_once "class/dateTime.php";											$dateTimes 					= new dateTimes($db);
	include_once "class/tables/sys_password_history.php";						$sys_password_history		= new sys_password_history($db);
	
	if($requestType=='loadCopyUsers')
	{
		$db->connect();
		
		$departmentId			= $_REQUEST['departmentId'];
		
		$html					= getLocationDepartmentwiseUsers($sessions->getLocationId(),$departmentId);
		$response['copyUsers'] 	= $html;
	}
	else if($requestType=='saveData')
	{
		$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
		$arrDetails 		= json_decode($_REQUEST['arrDetails'],true);
		
		$userId				= $arrHeader['userId'];
		$fullName			= $arrHeader['fullName'];
		$userName			= $arrHeader['userName'];
		$password			= $arrHeader['password'];
		$contactNo			= $arrHeader['contactNo'];
		$designantion		= $arrHeader['designantion'];	
		$department			= $arrHeader['department'];
		$copyUser			= $arrHeader['copyUser'];
		$email				= $arrHeader['email'];
		$remarks			= $arrHeader['remarks'];
		$gender				= $arrHeader['gender'];
		$chkOutSide			= $arrHeader['chkOutSide'];
		$chkOutSideEmail	= $arrHeader['chkOutSideEmail'];
		$active				= $arrHeader['active'];
		$editMode			= false;
		
		$db->connect();$db->begin();//open connection.
		
		if($userId=='')
		{
			//check save permissions.								
			$menupermision->set($programCode,$sessions->getUserId());
			$permissionArr	= $menupermision->checkMenuPermision('Add','','');
			
			if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);	
				
			$sys_users->setstrPassword(md5(md5($password)));	
		}
		else
		{
			//check edit permissions.
			$menupermision->set($programCode,$sessions->getUserId());
			$permissionArr	= $menupermision->checkMenuPermision('Edit','','');
			$editMode		= true;
			
			if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);
			
			$sys_users->setintUserId($userId);
		}
		
		//insert || update data
		$sys_users->setstrUserName($userName);
		$sys_users->setstrFullName($fullName);
		$sys_users->setintDepartmentId(($department==''?'NULL':$department));
		$sys_users->setstrContactNo($contactNo);
		$sys_users->setstrDesignation($designantion);
		$sys_users->setstrGender($gender);
		$sys_users->setCOPY_PRIVILEGE_USER_ID(($copyUser==''?'NULL':$copyUser));
		$sys_users->setstrEmail($email);
		$sys_users->setstrRemark($remarks);
		$sys_users->setintStatus($active);
		
		$sys_config->set(1);
		$sys_users->setPASSWORD_LAST_MODIFIED_DATE($dateTimes->getCurruntDate());
		$sys_users->setPASSWORD_ERROR_ATTEMPTS_LEFT($sys_config->getPASSWORD_ERROR_ATTEMPTS());
		$sys_users->setACCESS_FROM_OUTSIDE_FLAG($chkOutSide);
		$sys_users->setACCESS_FROM_OUTSIDE_EMAIL_FLAG($chkOutSideEmail);
		
		$resultArr 			= $sys_users->commit($userId==''?'insert':'update');
		
		if(!$resultArr['status'])
				throw new Exception($resultArr['msg']);
			
		if($userId=='')
		{
			$resultPH		= $sys_password_history->insertRec($resultArr['insertId'],md5(md5($password)),$dateTimes->getCurruntDateTime());
			
			if(!$resultPH['status'])
				throw new Exception($resultPH['msg']);	
		}

		$insertId			= ($userId==''?$resultArr['insertId']:$userId);
				
		if($copyUser!='')
		{
//			$resultDel			= $menupermision->delete("intUserId = '".$insertId."'");
//			if(!$resultDel['status'])
//				throw new Exception($resultDel['msg']);
				
			$resultCU 		= $menupermision->copyUserPermission($copyUser,$insertId);
			$getResultArr	= $db->getResult();
			
			if(!$resultCU)
				throw new Exception($getResultArr[0]);
		}
		
		$resultUDel			= $mst_locations_user->delete("intUserId = '".$insertId."'");
		if(!$resultUDel['status'])
				throw new Exception($resultUDel['msg']);
				
		foreach($arrDetails as $array_loop)
		{
			$locationId		= $array_loop['locationId'];
			$resultArr		= $mst_locations_user->insertRec($insertId,$locationId);
			
			if(!$resultArr['status'])
				throw new Exception($resultArr['msg']);	
		}
		
		$db->commit();
		$response['type'] 			= "pass";
		
		if(!$editMode)
			$response['msg'] 		= "Saved successfully.";
		else
			$response['msg'] 		= "Updated successfully.";	
	}
	else if($requestType=='loadSerachCombo')
	{	
		$response['searchCombo'] 	= $sys_users->getLocationWiseCombo('',$sessions->getLocationId());
	}
	else if($requestType=='loadData')
	{
		$db->connect();

		$userId			= $_REQUEST['userId'];
		$sys_users->set($userId);

		$response['fullName']			= $sys_users->getstrFullName();
		$response['userName']			= $sys_users->getstrUserName();
		$response['password']			= $sys_users->getstrPassword();
		$response['contactNo']			= $sys_users->getstrContactNo();
		$response['designantion']		= $sys_users->getstrDesignation();
		$response['department']			= $sys_users->getintDepartmentId();
		$response['copyUser']			= getLocationDepartmentwiseUsers($sessions->getLocationId(),$sys_users->getintDepartmentId(),$sys_users->getCOPY_PRIVILEGE_USER_ID());
		$response['email']				= $sys_users->getstrEmail();
		$response['remarks']			= $sys_users->getstrRemark();
		$response['gender']				= $sys_users->getstrGender();
		$response['chkOutSide']			= $sys_users->getACCESS_FROM_OUTSIDE_FLAG();
		$response['chkOutSideEmail']	= $sys_users->getACCESS_FROM_OUTSIDE_EMAIL_FLAG();
		$response['active']				= $sys_users->getintStatus();
		$response['locDetailData']		= getUserLocationDetailData($userId);
	}
	else if($requestType=='delete')
	{
		$userId			= $_REQUEST['userId'];
		
		$db->connect();$db->begin();//open connection.
		
		//check delete permissions.
		$menupermision->set($programCode,$sessions->getUserId());
		$delPerm		= $menupermision->getintDelete();
			
		if($delPerm==0)
			throw new Exception('No permision to delete.');
		
		$resultS 		= $sys_users->delete("intUserId = '".$userId."'");
		
		if(!$resultS['status'])
				throw new Exception($resultS['msg']);
				
		$resultMP 		= $menupermision->delete("intUserId = '".$userId."'");
		
		if(!$resultMP['status'])
				throw new Exception($resultMP['msg']);
		
		$resultUL 		= $mst_locations_user->delete("intUserId = '".$userId."'");
		
		if(!$resultUL['status'])
				throw new Exception($resultUL['msg']);
		
		$db->commit();
		$response['type'] 			= "pass";
		$response['msg'] 			= "Deleted successfully.";
	}
}
catch(Exception $e)
{
	$db->rollback();
	$response['msg'] 	=  $e->getMessage();
	$response['error'] 	=  $error_handler->jTraceEx($e);
	$response['type'] 	=  'fail';
	$response['sql']	=  $db->getSql();
	$db->disconnect();
}
$db->disconnect();
echo json_encode($response);

function getLocationDepartmentwiseUsers($locationId,$departmentId,$copyUser)
{
	global $mst_locations_user;
	
	$result			= $mst_locations_user->getLocationDepartmentwiseUsers($locationId,$departmentId);
	$html 			= "<option value=\"\">&nbsp;</option>";
	while($row = mysqli_fetch_array($result))
	{
		if($copyUser==$row['intUserId'])
			$html 	   .= "<option selected=\"selected\" value=\"".$row['intUserId']."\">".$row['strUserName']."</option>";
		else
			$html 	   .= "<option value=\"".$row['intUserId']."\">".$row['strUserName']."</option>";
	}
	
	return $html;
}
function getUserLocationDetailData($userId)
{
	global $mst_companies;
	
	$result		= $mst_companies->getcompanywiseLocations($userId);
	$tabIndex	= 8;
	$html		= "";
	while($row = mysqli_fetch_array($result))
	{
		$html	.= "<tr id=\"".$row['locationId']."\">";
		$html	.= "<td style=\"text-align:center\"><input name=\"chkLocation\" id=\"chkLocation\" type=\"checkbox\" class=\"validate[minCheckbox[1]] clsChkLocation\" tabindex=\"".++$tabIndex."\"/ ".($row['checkUser']==1?'checked=\"checked\"':'')."></td>";
		$html	.= "<td style=\"text-align:left\" class=\"clsCompanyName\" id=\"".$row['companyId']."\">".$row['companyName']."</td>";
		$html	.= "<td style=\"text-align:left\" class=\"clsLocationName\" id=\"".$row['locationId']."\">".$row['locationName']."</td>";
		$html	.= "</tr>";
	}
	return $html;
}
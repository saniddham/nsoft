
var basePath = 'presentation/administration/manageUser/addNew/';

		
$(document).ready(function() {
	
$("#frmUsers").validationEngine();
$('#frmUsers #txtFullName').focus();

functionList(cId,basePath);


  $("#chkOutSideAc").on('click',checkOutSideEmail)
  //===================================================
  	$('#frmUsers #txtLoginId').blur(function()
	{
		var url = basePath+"user-db-get.php?requestType=getAvailability&userName="+$('#txtLoginId').val();
		var obj = $.ajax({url:url,async:false});
		if(obj.responseText > 0)
		{
			$('#frmUsers #txtLoginId').validationEngine('showPrompt', 'This username is already exist.', 'fail');
			var t=setTimeout("alertx()",3000);
		}
	});
  //===================================================
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

	$('#cboDepartment').change(function(){
		$('#cboCopyUser').load(basePath+'user-db-get.php?requestType=getUserList&departmentId='+$(this).val());
	});
  ///save button click event
  $('#frmUsers #butSave').click(function(){
	//$('#frmUsers').submit();
	var requestType = '';
	if ($('#frmUsers').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmUsers #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		
		///////////////set to locations array /////////////////////////
		var arrLocation = '[';
		$('#tblCompany >tbody .chkLocation:checked').each(function (){
			arrLocation += '{"locationId":"'+$(this).val()+'"},';
		});
		arrLocation = removeLastChar(arrLocation)+']';
			
		if($('#rdoMale').attr('checked'))
			var gender = 'M';
		else
			var gender = 'F';
		
		var url = basePath+"user-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			data:$("#frmUsers").serialize()+'&requestType='+requestType+'&arrLocation='+arrLocation+'&gender='+gender,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmUsers #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmUsers').get(0).reset();
						$('#frmUsers #cboSearch').html(json.comboData);
						$('#frmUsers #password').attr('disabled',false);
						$('#frmUsers #password2').attr('disabled',false);
						$('#frmUsers #chkOutSideEmailAc').attr('disabled','disabled');
		
						var t=setTimeout("alertx()",1000);
						$('#frmUsers #txtFullName').focus();
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					$('#frmUsers #butSave').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
					//var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load company details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmLocations #cboSearch').click(function(){
	   $('#frmLocations').validationEngine('hide');
   });
    $('#frmUsers #cboSearch').change(function(){
		$('#frmUsers').validationEngine('hide');
		var url = basePath+"user-db-get.php";
		$('.chkLocation').attr('checked',false);
		if($('#frmUsers #cboSearch').val()=='')
		{
			$('#frmUsers').get(0).reset() ;return ;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmUsers #txtFullName').val(json.fullName);
					$('#frmUsers #txtLoginId').val(json.userName);
					$('#frmUsers #txtContactNo').val(json.contactNo);
					$('#frmUsers #txtDesignation').val(json.designation);
					
					$('#frmUsers #cboDepartment').val(json.departmentId);
					$('#frmUsers #txtEmail').val(json.email);
					$('#frmUsers #txtRemarks').val(json.remark);
					$('#frmUsers #chkActive').val(json.active);
					$('#frmUsers #password').val('password');
					$('#frmUsers #password').attr('disabled',true);
					$('#frmUsers #password2').val('password');
					$('#frmUsers #password2').attr('disabled',true);
					
					if(json.gender=='M')
						$('#rdoMale').attr('checked',true);
					else
						$('#rdoFemale').attr('checked',true);
					
					$('#frmUsers #chkActive1').attr('checked',chk(json.status));
					
					$('#chkOutSideAc').attr('checked',chk(json.outSideAc));
					$('#chkOutSideEmailAc').attr('checked',chk(json.outSideEmailAc));
					
					if(json.outSideAc==0)
					{
						$('#chkOutSideEmailAc').removeAttr("checked");
						$('#chkOutSideEmailAc').attr("disabled",'disabled');
					}
					else
					{
						$('#chkOutSideEmailAc').removeAttr("disabled");
					}
					
					setCompany(json.location);
					$('#cboDepartment').change();
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmUsers #butNew').click(function(){
		$('#frmUsers').validationEngine('hide');
		$('#frmUsers').get(0).reset();
		//loadCombo_frmUsers();
		$('#frmUsers #txtFullName').focus();
		$('#frmUsers #password').attr('disabled',false);
		$('#frmUsers #password2').attr('disabled',false);
	});
    $('#frmUsers #butDelete').click(function(){
		if($('#frmUsers #cboSearch').val()=='')
		{
			$('#frmUsers #butDelete').validationEngine('showPrompt', 'Please select user.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmUsers #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basePath+"user-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmUsers #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmUsers #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												if(json.type=='pass')
												{
													$('#frmUsers').get(0).reset();
													$('#frmUsers #cboSearch').html(json.comboData);
													var t=setTimeout("alertDelete()",1000);return;
												}
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});

function checkOutSideEmail()
{
	$('#chkOutSideEmailAc').removeAttr("checked");
	
	if($(this).attr('checked'))
	{
		$('#chkOutSideEmailAc').removeAttr("disabled");
	}
	else
	{
		$('#chkOutSideEmailAc').attr("disabled",'disabled');
	}
}

function loadCombo_frmUsers()
{
	var url 	= basePath+"user-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmUsers #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmUsers #butSave').validationEngine('hide')	;
	$('#frmUsers #txtLoginId').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmUsers #butDelete').validationEngine('hide')	;
}

function setCompany(arrLocation)
{
	var x;
	for(x in arrLocation)
	{
		$('#chkLocation'+arrLocation[x]['id']).attr('checked',true);	
	}
}

function loadData(basePath){
 	var user	=$('#frmUsers #cboSearch').val();
	
		var url = basePath+"user-db-get.php";
		$('.chkLocation').attr('checked',false);
		if($('#frmUsers #cboSearch').val()=='')
		{
			$('#frmUsers').get(0).reset() ;return ;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+user,
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmUsers #txtFullName').val(json.fullName);
					$('#frmUsers #txtLoginId').val(json.userName);
					$('#frmUsers #txtContactNo').val(json.contactNo);
					$('#frmUsers #txtDesignation').val(json.designation);
					
					$('#frmUsers #cboDepartment').val(json.departmentId);
					$('#frmUsers #txtEmail').val(json.email);
					$('#frmUsers #txtRemarks').val(json.remark);
					$('#frmUsers #chkActive').val(json.active);
					$('#frmUsers #password').val('password');
					$('#frmUsers #password').attr('disabled',true);
					$('#frmUsers #password2').val('password');
					$('#frmUsers #password2').attr('disabled',true);
					
					if(json.gender=='M')
						$('#rdoMale').attr('checked',true);
					else
						$('#rdoFemale').attr('checked',true);
					
					$('#frmUsers #chkActive1').attr('checked',chk(json.status));
					
					$('#chkOutSideAc').attr('checked',chk(json.outSideAc));
					$('#chkOutSideEmailAc').attr('checked',chk(json.outSideEmailAc));
					
					if(json.outSideAc==0)
					{
						$('#chkOutSideEmailAc').removeAttr("checked");
						$('#chkOutSideEmailAc').attr("disabled",'disabled');
					}
					else
					{
						$('#chkOutSideEmailAc').removeAttr("disabled");
					}
					
					setCompany(json.location);
					$('#cboDepartment').change();
			}
	});
	
}

function functionList(cId,basePath)
{
	if(cId!='')
	{
 		$('#frmUsers #cboSearch').val(cId);
		loadData(basePath);
	}
}
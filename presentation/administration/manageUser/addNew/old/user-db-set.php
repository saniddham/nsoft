<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$intCompany	= $_SESSION['CompanyID'];
	
	include "{$backwardseperator}dataAccess/Connector.php";
	include "../../../../class/administration/password/cls_password_set.php";
	
	$obj_password_set	= new password_set($db);
	
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$intUserId		= $_REQUEST['cboSearch'];
	$intDepartmentId= $_REQUEST['cboDepartment'];
	$intStatus		=chk( $_REQUEST['chkActive1']);
	
	$strPassword	= md5(md5($_REQUEST['password']));
	$gender			= $_REQUEST['gender'];
	$strContactNo	= trim($_REQUEST['txtContactNo']);
	$strDesignation	= trim($_REQUEST['txtDesignation']);
	$strEmail		= trim($_REQUEST['txtEmail']);
	$strFullName	= $_REQUEST['txtFullName'];
	$loginName		= trim($_REQUEST['txtLoginId']);
	$strRemarks		= trim($_REQUEST['txtRemarks']);
	//cboCopyUser
	$intCopyUser	= $_REQUEST['cboCopyUser'];
	$outSideAc		= chk($_REQUEST['chkOutSideAc']);
	$outSideEmailAc	= chk($_REQUEST['chkOutSideEmailAc']);
	
	$arrLocation 		= json_decode($_REQUEST['arrLocation'], true);
	///////////////////////////////////// company insert part /////////////////////
	if($requestType=='add')
	{
		$sql = "INSERT INTO `sys_users` (`strUserName`,`strPassword`,`strFullName`,`intDepartmentId`,`strContactNo`,
		`strDesignation`,`strGender`,`strEmail`,`strRemark`,`intStatus` ,PASSWORD_LAST_MODIFIED_DATE,ACCESS_FROM_OUTSIDE_FLAG,ACCESS_FROM_OUTSIDE_EMAIL_FLAG) 
		VALUES ('$loginName','$strPassword','$strFullName','$intDepartmentId','$strContactNo','$strDesignation','$gender','$strEmail','$strRemarks','$intStatus',NOW(),'$outSideAc','$outSideEmailAc')";
		$result = $db->RunQuery($sql);
		if($result){
			$intUserNewId  =$db->insertId;
			$obj_password_set->savePasswordHistory($intUserNewId,$strPassword);
			saveLocations($arrLocation,$intUserNewId);
			$response['type'] 			= 'pass';
			$response['msg'] 			= 'Saved successfully.';
			$response['comboData'] 		=  getComboData();
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		///////////////// set permision /////////////////////
		if($intCopyUser>0)
		{
		 $sql = "insert into menupermision SELECT
					intMenuId,
					'".$intUserNewId."',
					intView,
					intAdd,
					intEdit,
					intDelete,
					int1Approval,
					int2Approval,
					int3Approval,
					int4Approval,
					int5Approval,
					intSendToApproval,
					intCancel,
					intReject,
					intRevise,
					intExportToExcel,
					intExportToPDF
					FROM menupermision
					WHERE
					intUserId =  '".$intCopyUser."'
					";
		
		$result = $db->RunQuery($sql);
		}
		
	}
	/////////// company update part /////////////////////
	else if($requestType=='edit')
	{
		$sql = "UPDATE `sys_users` SET
					`strUserName`		=	'$loginName',
					`strFullName`		=	'$strFullName',
					`intDepartmentId`	=	'$intDepartmentId',
					`strContactNo`		=	'$strContactNo',
					`strDesignation`	=	'$strDesignation',
					`strGender`			=	'$gender',
					`strEmail`			=	'$strEmail',
					`strRemark`			=	'$strRemarks',
					`intStatus`			=	'$intStatus',
					ACCESS_FROM_OUTSIDE_FLAG	= '$outSideAc',
					ACCESS_FROM_OUTSIDE_EMAIL_FLAG = '$outSideEmailAc'
				 WHERE (`intUserId`='$intUserId')  ";
		$result = $db->RunQuery($sql);
		saveLocations($arrLocation,$intUserId);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
			$response['comboData'] 		=  getComboData();
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		///////////////// set permision /////////////////////
		if($intCopyUser!='')
		{
		$sql = "DELETE FROM `menupermision` WHERE (`intUserId`='$intUserId')";
		$result = $db->RunQuery($sql);
		
		if($intCopyUser>0)
		{
		$sql = "insert into menupermision SELECT
					intMenuId,
					'".$intUserId."',
					intView,
					intAdd,
					intEdit,
					intDelete,
					int1Approval,
					int2Approval,
					int3Approval,
					int4Approval,
					int5Approval,
					intSendToApproval,
					intCancel,
					intReject,
					intRevise,
					intExportToExcel,
					intExportToPDF
					FROM menupermision
					WHERE
					intUserId =  '".$intCopyUser."'
					";
		$result = $db->RunQuery($sql);
		}
		}
	}
	/////////// company delete part /////////////////////
	else if($requestType=='delete')
	{
		//disable by roshan 2013-07-25
		//$sql = "DELETE FROM `sys_users` WHERE (`intUserId`='$intUserId')  ";
		//$result = $db->RunQuery($sql);
		
		//$sql = "DELETE FROM `mst_locations_user` WHERE (`intUserId`='$intUserId')  ";
		//$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Delete option disabled';//'Deleted successfully.';
			$response['comboData'] 	=  getComboData();
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= 'Delete option disabled';//$db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	echo json_encode($response);
	
	function getComboData()
	{
		global $db;
		$sql = "SELECT
					sys_users.intUserId,
					sys_users.strUserName
				FROM sys_users
				where strUserName <> 'root'
				ORDER BY
					sys_users.strUserName ASC
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intUserId']."\">".$row['strUserName']."</option>";
		}
		return $html;	
	}
	function saveLocations($arrLocation,$intUserId)
	{
		global $db;
		$sql = "DELETE FROM `mst_locations_user` WHERE (`intUserId`='$intUserId')";
		$result = $db->RunQuery($sql);
		foreach($arrLocation as $arrL)
		{
			$sql = "INSERT INTO `mst_locations_user` (`intUserId`,`intLocationId`) VALUES ('".$intUserId."','".$arrL['locationId']."')";
			$result = $db->RunQuery($sql);
		}	
	}
?>
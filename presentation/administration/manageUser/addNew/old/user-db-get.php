<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	
	/////////// company load part /////////////////////
	if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		$sql = "SELECT
					strUserName,
					strPassword,
					strFullName,
					intDepartmentId,
					strContactNo,
					strDesignation,
					strGender,
					strEmail,
					strRemark,
					`intStatus`,
					ACCESS_FROM_OUTSIDE_FLAG,
					ACCESS_FROM_OUTSIDE_EMAIL_FLAG
				FROM sys_users
				WHERE
					sys_users.intUserId =  '$id'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['userName'] 			= $row['strUserName'];
			//$response['password'] 			= $row['strPassword'];
			$response['fullName'] 			= $row['strFullName'];
			$response['departmentId'] 		= $row['intDepartmentId'];
			$response['contactNo'] 			= $row['strContactNo'];
			$response['designation'] 		= $row['strDesignation'];
			$response['gender'] 			= $row['strGender'];
			$response['email'] 				= $row['strEmail'];
			$response['remark'] 			= $row['strRemark'];
			$response['status'] 			= $row['intStatus'];
			$response['outSideAc'] 			= $row['ACCESS_FROM_OUTSIDE_FLAG'];
			$response['outSideEmailAc'] 	= $row['ACCESS_FROM_OUTSIDE_EMAIL_FLAG'];
		}
		
		$sql = "SELECT
					intLocationId
				FROM mst_locations_user
				WHERE
					intUserId =  '$id'
				";
		$result = $db->RunQuery($sql);
		$arrLocation = '';
		while($row=mysqli_fetch_array($result))
		{
			$location['id'] 	= $row['intLocationId'];
			$arrLocation[] 		= $location;
		}
			$response['location'] = $arrLocation;
		echo json_encode($response);
	}
	else if($requestType=='getUserList')
	{
		$depId = $_REQUEST['departmentId'];	
		/*$sql = "SELECT
					sys_users.intUserId,
					sys_users.strUserName
				FROM sys_users
				where intHigherPermision <> 1 and intDepartmentId='$depId'
				ORDER BY
					sys_users.strUserName ASC
				";*/
		$sql = "SELECT
						sys_users.intUserId,
				sys_users.strUserName
				FROM
					mst_locations_user
					Inner Join sys_users ON sys_users.intUserId = mst_locations_user.intUserId
				WHERE
					mst_locations_user.intLocationId =  '".$_SESSION['CompanyID']."' AND
					intHigherPermision <> 1 AND intDepartmentId='$depId'
					ORDER BY strUserName";
					
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intUserId']."\">".$row['strUserName']."</option>";
		}
		echo $html;	
	}
	else if($requestType=='getAvailability')
	{
		$userName 	= $_REQUEST['userName'];	
		$sql	= "SELECT
				   COUNT(strUserName) AS no 
				   FROM sys_users
				   WHERE
				   sys_users.strUserName =  '$userName'";
		$result = $db->RunQuery($sql);
		$row= mysqli_fetch_array($result);
		echo $row['no'];
	}
?>
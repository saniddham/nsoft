<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "class/tables/mst_companies.php";						$mst_companies 				= new mst_companies($db);
include_once "class/tables/sys_users.php";							$sys_users 					= new sys_users($db);
include_once "class/tables/mst_department.php";						$mst_department 			= new mst_department($db);
include_once "class/tables/menupermision.php";						$menupermision 				= new menupermision($db);

$programCode	= 'P0012';

$db->connect();
$menupermision->set($programCode,$sessions->getUserId());
$addPerm		= $menupermision->getintAdd();
$editPerm		= $menupermision->getintEdit();
$deletePerm		= $menupermision->getintDelete();

?>
<title>Manage User</title>
<form id="frmUsers" name="frmUsers" method="post" >
<div align="center">
	<div class="trans_layoutD">
		<div class="trans_text">Manage User</div>
		<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        	<tr>
            	<td>
               	  <table width="100%" border="0" align="center">
                    	<tr class="normalfnt">
                    	  <td colspan="3"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableBorder_allRound" >
                          <tr class="normalfnt">
                            <td height="45" width="28%">&nbsp;Search</td>
                            <td width="68%"><select name="cboSearch" class="txtbox" id="cboSearch" style="width:101%" tabindex="1">
                            <?php
								echo $sys_users->getLocationWiseUserList('',$sessions->getLocationId())
							?>
                            </select></td>
                            <td width="4%">&nbsp;</td>
                          </tr>
                        </table></td>
                   	  </tr>
                    	<tr class="normalfnt">
                    	  <td width="28%">&nbsp;</td>
                    	  <td width="68%">&nbsp;</td>
                    	  <td width="4%">&nbsp;</td>
                  	  </tr>
                    	<tr class="normalfnt">
                    	  <td>&nbsp;Full Name&nbsp;<span class="compulsoryRed">*</span></td>
                    	  <td><input name="txtFullName" type="text" class="validate[required,maxSize[100]]" id="txtFullName" style="width:100%" maxlength="100" tabindex="2"/></td>
                    	  <td>&nbsp;</td>
                  	  </tr>
                    	<tr class="normalfnt">
                    	  <td><strong>&nbsp;System User Name</strong>&nbsp;<span class="compulsoryRed">*</span></td>
                    	  <td><input name="txtUserName" type="text" class="validate[required,maxSize[20]]" id="txtUserName" style="width:200px" maxlength="50" tabindex="3"/></td>
                    	  <td>&nbsp;</td>
                  	  </tr>
                    	<tr class="normalfnt">
                    	  <td><strong>&nbsp;Password</strong> <span class="compulsoryRed">*</span></td>
                    	  <td><input name="password" type="password" class="validate[required,maxSize[20],minSize[4]]" id="password" style="width:200px" maxlength="50" tabindex="4"/></td>
                    	  <td>&nbsp;</td>
                  	  </tr>
                    	<tr class="normalfnt">
                    	  <td><strong>&nbsp;Re-enter Password</strong> <span class="compulsoryRed">*</span></td>
                    	  <td><input name="password2" type="password" class="validate[required,maxSize[20],minSize[4],equals[password]]" id="password2" style="width:200px" maxlength="50" tabindex="5"/></td>
                    	  <td>&nbsp;</td>
                  	  </tr>
                    	<tr class="normalfnt">
                    	  <td><strong>&nbsp;</strong>Contact No</td>
                    	  <td><input name="txtContactNo" type="text" class="txtbox" id="txtContactNo" style="width:100%" maxlength="50" tabindex="6"/></td>
                    	  <td>&nbsp;</td>
                  	  </tr>
                    	<tr class="normalfnt">
                    	  <td><strong>&nbsp;</strong>Designation</td>
                    	  <td><input name="txtDesignation" type="text" class="txtbox" id="txtDesignation" style="width:100%" maxlength="100" tabindex="7"/></td>
                    	  <td>&nbsp;</td>
                  	  </tr>
                        <tr class="normalfnt">
                            <td><strong>&nbsp;</strong>Gender</td>
                            <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr class="normalfnt">
                                    <td width="15%">Male</td>
                                    <td width="14%"><input name="rdoGender" type="radio" id="rdoMale" value="radio" checked="checked" /></td>
                                    <td width="18%">Female</td>
                                    <td width="53%"><input type="radio" name="rdoGender" id="rdoFemale" value="radio" /></td>
                                </tr>
                                </table>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    	<tr class="normalfnt">
                    	  <td valign="top"><strong>&nbsp;</strong>Company <span class="compulsoryRed">*</span></td>
                          <td valign="top"><div id="divTable" style="overflow:scroll;overflow-x: hidden;width:100%;height:200px" class="cls_div_bgstyle">
                          <table width="100%" class="bordered" id="tblCompany" bgcolor="#FFFFFF">
                          <thead>
                          	<tr>
                            	<th width="9%"><input name="chkAll" type="checkbox" id="chkAll" tabindex="8"/></th>
                            	<th width="53%">Company</th>
                            	<th width="38%">Location</th>
                            </tr>
                           </thead>
                           <tbody>
                           <?php
						   $tabIndex	= 8;
						   $result		= $mst_companies->getcompanywiseLocations();
						   while($row = mysqli_fetch_array($result))
						   {
							?>
                            	<tr id="<?php echo $row['locationId']; ?>">
                            		<td style="text-align:center"><input name="chkLocation" id="chkLocation" type="checkbox" class="validate[minCheckbox[1]] clsChkLocation" tabindex="<?php echo ++$tabIndex; ?>"/></td>
                                    <td style="text-align:left" class="clsCompanyName" id="<?php echo $row['companyId']; ?>"><?php echo $row['companyName']; ?></td>
                                    <td style="text-align:left" class="clsLocationName" id="<?php echo $row['locationId']; ?>"><?php echo $row['locationName']; ?></td>
                            	</tr>
                            <?php
						   }
						   ?>
                           	
                           </tbody>
                           </table>
                           </div></td>
                  	  </tr>
                    	<tr class="normalfnt">
                    	  <td><strong>&nbsp;</strong>Department <span class="compulsoryRed">*</span></td>
                    	  <td><select class="validate[required]" name="cboDepartment" id="cboDepartment"  style="width:200px" tabindex="<?php echo ++$tabIndex; ?>">	
                          <?php
						 	 echo $mst_department->getCombo();
						  ?>
                          </select></td>
                    	  <td>&nbsp;</td>
                  	  </tr>
                    	<tr class="normalfnt">
                    	  <td><strong>&nbsp;</strong><strong style="color: #5673F8">Copy From Privileges</strong></td>
                    	  <td><select  name="cboCopyUser" id="cboCopyUser" style="width:200px" tabindex="<?php echo ++$tabIndex; ?>">
                          </select></td>
                    	  <td>&nbsp;</td>
                  	  </tr>
                    	<tr class="normalfnt">
                    	  <td><strong>&nbsp;</strong>E-Mail <span class="compulsoryRed">*</span></td>
                    	  <td><input name="txtEmail" tabindex="<?php echo ++$tabIndex; ?>" type="text" class="validate[custom[email],required]" id="txtEmail" style="width:100%" maxlength="100"/></td>
                    	  <td>&nbsp;</td>
                  	  </tr>
                    	<tr class="normalfnt">
                    	  <td valign="top"><strong>&nbsp;</strong>Remarks</td>
                    	  <td valign="top"><textarea name="txtRemarks" style="width:100%"  rows="2" class="txtbox" id="txtRemarks" tabindex="<?php echo ++$tabIndex; ?>"></textarea></td>
                    	  <td>&nbsp;</td>
                  	  </tr>
                    	<tr class="normalfnt">
                    	  <td><strong>&nbsp;</strong>Outside Access</td>
                    	  <td><input type="checkbox" name="chkOutSideAc" id="chkOutSideAc" tabindex="<?php echo ++$tabIndex; ?>" style="vertical-align: middle"/></td>
                    	  <td>&nbsp;</td>
                  	  </tr>
                    	<tr class="normalfnt">
                    	  <td><strong>&nbsp;</strong>Outside E-Mail Access</td>
                    	  <td><input type="checkbox" name="chkOutSideEmailAc" id="chkOutSideEmailAc" tabindex="<?php echo ++$tabIndex; ?>" disabled="disabled" style="vertical-align: middle"/></td>
                    	  <td>&nbsp;</td>
                  	  </tr>
                    	<tr class="normalfnt">
                        	<td><strong>&nbsp;</strong>Active</td>
                            <td><input type="checkbox" name="chkActive" id="chkActive" checked="checked" tabindex="<?php echo ++$tabIndex; ?>" style="vertical-align: middle"/></td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
            <td height="34"><table width="100%" border="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                    <tr>
                      <td width="100%" align="center" bgcolor=""><a class="button white medium" id="butNew" name="butNew" style="display:" tabindex="28">New</a><a class="button white medium" id="butSave" name="butSave" <?php echo(($addPerm || $editPerm)?'':'style="display:none"');?> tabindex="24">Save</a><a class="button white medium" id="butDelete" name="butDelete" <?php echo($deletePerm?'':'style="display:none"');?> tabindex="25">Delete</a><a href="main.php" class="button white medium" id="butClose" name="butClose" tabindex="27">Close</a></td>
                    </tr>
                  </table></td>
              </tr>
            </table>
                
            </td>
        </tr>
        </table>

	</div>
</div>
</form>
<?php
$db->disconnect();
?>

// JavaScript Document
$(document).ready(function(){
	
	$('#frmUsers #cboDepartment').die('change').live('change',loadCopyPrivilageList);
	$('#frmUsers #butSave').die('click').live('click',saveData);
	$('#frmUsers #tblCompany #chkAll').die('click').live('click',checkAll);
	$('#frmUsers #butNew').die('click').live('click',clearAll);
	$('#frmUsers #cboSearch').die('change').live('change',loadData);
	$('#frmUsers #chkOutSideAc').die('click').live('click',checkOutSideEmail);
	$('#frmUsers #butDelete').die('click').live('click',deleteData);
	
	$('#frmUsers #txtFullName').focus();
});
function loadCopyPrivilageList()
{
	if($(this).val()=='')
	{
		$('#frmUsers #cboCopyUser').html('');
		return
	}
	var url 	= "controller.php?q=14&requestType=loadCopyUsers";
	var data 	= "departmentId="+$('#frmUsers #cboDepartment').val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmUsers #cboCopyUser').html(json.copyUsers);
			}
	});	
}
function checkAll()
{
	if($(this).is(':checked'))
	{
		$('#frmUsers #tblCompany .clsChkLocation').prop('checked',true);
	}
	else
	{
		$('#frmUsers #tblCompany .clsChkLocation').prop('checked',false);
	}
}
function saveData()
{
	if(!$('#frmUsers').validationEngine('validate'))
	{
		var t = setTimeout("alertxAll()",1000);
		return;
	}
	
	showWaiting();
	var userId 			= $('#frmUsers #cboSearch').val();
	var fullName 		= $('#frmUsers #txtFullName').val();
	var userName 		= $('#frmUsers #txtUserName').val();
	var password 		= $('#frmUsers #password').val();
	var contactNo 		= $('#frmUsers #txtContactNo').val();
	var designantion 	= $('#frmUsers #txtDesignation').val();
	
	if($('#frmUsers #rdoMale').is(':checked'))
		var gender 		= 'M';
	else
		var gender 		= 'F';
	
	var department	 	= $('#frmUsers #cboDepartment').val();
	var copyUser	 	= $('#frmUsers #cboCopyUser').val();
	var email		 	= $('#frmUsers #txtEmail').val();
	var remarks		 	= $('#frmUsers #txtRemarks').val();
	var chkOutSide		= ($('#frmUsers #chkOutSideAc').is(':checked')?1:0);
	var chkOutSideEmail = ($('#frmUsers #chkOutSideEmailAc').is(':checked')?1:0);
	var active		 	= ($('#frmUsers #chkActive').is(':checked')?1:0);
	
	var data 		= "requestType=saveData";
	var arrHeader 	= "{";
						arrHeader += '"userId":"'+userId+'",' ;
						arrHeader += '"fullName":'+URLEncode_json(fullName)+',';
						arrHeader += '"userName":'+URLEncode_json(userName)+',';
						arrHeader += '"password":"'+password+'",' ;
						arrHeader += '"contactNo":'+URLEncode_json(contactNo)+',';
						arrHeader += '"designantion":'+URLEncode_json(designantion)+',';
						arrHeader += '"department":"'+department+'",' ;
						arrHeader += '"copyUser":"'+copyUser+'",' ;
						arrHeader += '"email":'+URLEncode_json(email)+',';
						arrHeader += '"remarks":'+URLEncode_json(remarks)+',';
						arrHeader += '"gender":"'+gender+'",' ;
						arrHeader += '"chkOutSide":"'+chkOutSide+'",' ;
						arrHeader += '"chkOutSideEmail":"'+chkOutSideEmail+'",' ;
						arrHeader += '"active":"'+active+'"' ;
	arrHeader 	   += "}";
	var arrHeader	= arrHeader;
	
	var arrDetails		= "";
	var status			= false;
	$('#frmUsers #tblCompany .clsChkLocation').each(function(){
			
		if($(this).is(':checked'))
		{
			status 				= true;
			var locationId 		= $(this).parent().parent().attr('id');
		
			arrDetails += "{";
			arrDetails += '"locationId":"'+ locationId +'"' ;
			arrDetails +=  '},';
		}
	});
	if(!status)
	{
		$(this).validationEngine('showPrompt','Please select a location.','fail');
		hideWaiting();	
		return;
	}
	
	arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
	var arrDetails	= '['+arrDetails+']';
	data	   	   += "&arrHeader="+arrHeader+"&arrDetails="+arrDetails;
	
	var url = "controller.php?q=14";
	$.ajax({
			url:url,
			dataType:'json',
			type:"POST",
			data:data,
			async:false,
			success:function(json){
				$('#frmUsers #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				if(json.type=='pass')
				{
					var t = setTimeout("alertxAll()",3000);
					clearAll();
					loadSerachCombo();
					hideWaiting();
					return;
				}
				else
				{
					hideWaiting();
				}
			},
			error:function(xhr,status){
					
				$('#frmUsers #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				hideWaiting();
				return;
			}		
	});	
}
function checkOutSideEmail()
{
	if($('#frmUsers #chkOutSideAc').is(':checked'))
		$('#frmUsers #chkOutSideEmailAc').prop('disabled',false);
	else
	{
		$('#frmUsers #chkOutSideEmailAc').prop('disabled',true);
		$('#frmUsers #chkOutSideEmailAc').prop('checked',false);
	}
}
function loadData()
{
	clearWhenSerchChange();
	if($(this).val()=='')
	{
		return;
	}
	var url 	= "controller.php?q=14&requestType=loadData";
	var data 	= "userId="+$(this).val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmUsers #txtFullName').val(json.fullName);
				$('#frmUsers #txtUserName').val(json.userName);
				$('#frmUsers #password').val(json.password);
				$('#frmUsers #password').prop('disabled',true);
				$('#frmUsers #password2').val(json.password);
				$('#frmUsers #password2').prop('disabled',true);
				$('#frmUsers #txtContactNo').val(json.contactNo);
				$('#frmUsers #txtDesignation').val(json.designantion);
				$('#frmUsers #cboDepartment').val(json.department);
				$('#frmUsers #cboCopyUser').html(json.copyUser);
				$('#frmUsers #txtEmail').val(json.email);
				$('#frmUsers #txtRemarks').val(json.remarks);
				
				if(json.gender=='M')
					$('#frmUsers #rdoMale').prop('checked',true);
				else
					$('#frmUsers #rdoFemale').prop('checked',true);
				
				$('#frmUsers #chkOutSideAc').prop('checked',(json.chkOutSide==1?true:false));
				$('#frmUsers #chkOutSideEmailAc').prop('disabled',(json.chkOutSide==0?true:false));
				$('#frmUsers #chkOutSideEmailAc').prop('checked',(json.chkOutSideEmail==1?true:false));
				$('#frmUsers #chkActive').prop('checked',(json.active==1?true:false));
				
				$('#frmUsers #tblCompany tbody').html(json.locDetailData);
			}
	});	
}
function deleteData()
{
	if($('#frmUsers #cboSearch').val()=='')
	{
		$('#frmUsers #butDelete').validationEngine('showPrompt','Please select a user.','fail');
		var t=setTimeout("alertxAll()",1000);
		return;	
	}
	var val = $.prompt('Are you sure you want to delete this user?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
			showWaiting();
			var url = "controller.php?q=14&requestType=delete";
			var obj = $.ajax({
				url:url,
				type:"POST",
				dataType: "json",  
				data:"&userId="+$('#frmUsers #cboSearch').val(),
				async:false,						
				success:function(json){
					$('#frmUsers #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertxAll()",1000);
						hideWaiting();
						clearAll();
						loadSerachCombo();
						return;
					}
					else
					{
						var t=setTimeout("alertxAll()",3000);
					}
					 hideWaiting();
				},
				error:function(xhr,status){						
						$('#frmUsers #butDelete').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertxAll()",3000);
						 hideWaiting();
				}		
			});	
		}			  
	}});
}
function alertxAll()
{
	$('#frmUsers').validationEngine('hide')	;
}
function clearAll()
{
	$('#frmUsers').find("input[type=text],input[type=password], textarea,select").val('');
	$('#frmUsers').find("input[type=checkbox]").prop('checked',false);
	$('#frmUsers #chkActive').prop('checked',true);
	$('#frmUsers #chkOutSideEmailAc').prop('disabled',true);
	$('#frmUsers #password').prop('disabled',false);
	$('#frmUsers #password2').prop('disabled',false);
	$('#frmUsers #cboCopyUser').html('');	
	$('#frmUsers #txtFullName').focus();
}
function clearWhenSerchChange()
{
	$('#frmUsers').find("input[type=text],input[type=password], textarea").val('');
	$('#frmUsers').find("input[type=checkbox]").prop('checked',false);
	$('#frmUsers #chkActive').prop('checked',true);
	$('#frmUsers #chkOutSideEmailAc').prop('disabled',true);
	$('#frmUsers #password').prop('disabled',false);
	$('#frmUsers #password2').prop('disabled',false);
	$('#frmUsers #cboDepartment').val('');
	$('#frmUsers #cboCopyUser').html('');	
}
function loadSerachCombo()
{
	var url 	= "controller.php?q=14&requestType=loadSerachCombo";
	$.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmUsers #cboSearch').html(json.searchCombo);
			}
	});	
}
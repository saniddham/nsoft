<?php
session_start();
$companyId = $_SESSION['CompanyID'];
$thisFilePath =  $_SERVER['PHP_SELF'];

$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}
if($_SERVER['REQUEST_METHOD'] == "POST")
{
	$optionValue = trim($_REQUEST['cboSearchOption']);
	$searchValue = trim($_REQUEST['txtSearchValue']);
}
?>
<title>User Listing</title>

<script type="text/javascript" language="javascript">
function pageSubmit()
{
		window.location.href = "main.php";
}
function clearValue()
{
	document.getElementById('txtSearchValue').value = '';
	document.getElementById('txtSearchValue').focus();
}
</script>

</head>

<body>
<form id="frmUserListing" name="frmUserListing" method="post" autocomplete="off" action="">
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">User Listing</div>
		  <table width="900" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="900" border="0">
      <tr>
        <td><table width="900" border="0" cellpadding="1" cellspacing="0" class="tableBorder_allRound">
          <tr>
            <td width="16%" height="22" class="normalfnt">&nbsp;</td>
            <td width="18%" class="normalfntMid">Searching Option</td>
            <td width="17%" class="normalfnt">
            <select name="cboSearchOption" id="cboSearchOption" onchange="clearValue();" style="width:150px">
              <option value="">&nbsp;</option>
<option value="sys_users.strUserName" <?php echo($optionValue=='sys_users.strUserName'?'selected':'') ?> >Login Name</option>
<option value="sys_users.strFullName" <?php echo($optionValue=='sys_users.strFullName'?'selected':'') ?>>Name</option>
<option value="mst_department.strName" <?php echo($optionValue=='mst_department.strName'?'selected':'') ?>>Department</option>
            </select></td>
            <td width="16%"><input value="<?php echo $searchValue; ?>" type="text" name="txtSearchValue" id="txtSearchValue" /></td>
            <td width="18%" class="normalfnt">
            <a href="#">
            <img src="images/search.png" width="20" height="20" onclick="pageSubmit();" /></a></td>
            <td width="15%">&nbsp;</td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td>
          <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
          <div id="_head_wrap" style="width: 900px; overflow: hidden;">
          <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
          <thead>
            <tr class="gridHeader">
              <th height="22" style="width: 22px;" >Id</th>
              <th style="width: 70px;" ><strong>Login Name</strong></th>
              <th style="width:120px;" ><strong>Full Name</strong></th>
              <th style="width: 60px;" ><strong>Department</strong></th>
              <th style="width: 30px;" >View</th>
              <th style="width: 5px;" ></th>
              </tr>
           </table>
           </div>
           <div class="normalfnt" id="_body_wrap" style="width: 900px; height: 310px; overflow: scroll;">
           <table class="tbl1 scroll"> 
           <tbody>
              <?php

				if($searchValue!='')
			  	$wherePart = "AND $optionValue like '%$searchValue%'";
				
	 	 		 $sql = "SELECT DISTINCT
				 		sys_users.intUserId,
						sys_users.strUserName,
						sys_users.strFullName,
						sys_users.intDepartmentId,
						mst_department.strName
						FROM
						sys_users
						Inner Join mst_department ON sys_users.intDepartmentId = mst_department.intId
						Inner Join mst_locations_user ON sys_users.intUserId = mst_locations_user.intUserId
						WHERE sys_users.intUserId <> '1'
						$wherePart
						";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$id 		=	 $row['intUserId'];
	  			 ?>
           		<tr class="normalfnt">
           		<td class="id" height="16" align="center" bgcolor="#FFFFFF">
		   		<?php echo $row['intUserId'];?></td>
              	<td class="logName"  align="center" bgcolor="#FFFFFF"><?php echo $row['strUserName'];?></td>
              	<td class="name" bgcolor="#FFFFFF"><?php echo $row['strFullName'];?></td>
              	<td class="dept" align="center" bgcolor="#FFFFFF"><span class="name"><?php echo $row['strName'];?></span></td>
              	<td class="more" align="center" bgcolor="#FFFFFF"><a target="_blank" href="<?php echo "main.php?MenuId=14&id=$id";?>">More</a></td>
              </tr>
               <?php 
        	    } 
       		   ?>
              </tbody>
             </table>
            </div></div>
              </td>
              </tr>
            </table>
			</td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>

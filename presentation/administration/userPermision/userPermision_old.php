<?php
	$backwardseperator = "../../../";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>User Permision</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<script src="../../../libraries/jquery/jquery.js"></script>

</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<div align="left">
<div class="trans_layoutXL">
		  <div class="trans_text"><div style="position:absolute; left: -8px; top: -25px; width: 65px; height: 70px;"><img src="../../../images/userPermision.png" /></div>User Permision </div>
          
          <table width="100%">
          <tr>
          	<td width="100%"><table width="100%" border="0" class="bcgl2">
          	  <tr>
          	    <td width="7%">&nbsp;</td>
          	    <td width="15%" class="normalfnt">User</td>
          	    <td width="27%">
          	      <select style="width:200px" name="jumpMenu" id="jumpMenu">
                  <option value=""></option>
                    <?php 
					$sql = "SELECT
								useraccounts.intUserID,
								useraccounts.UserName
							FROM useraccounts
							WHERE
								useraccounts.`status` =  '1' order by UserName
							";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intUserID']."\">".$row['UserName']."</option>";
					}
					?>
       	          </select>
       	        </td>
          	    <td width="17%">&nbsp;</td>
          	    <td width="17%">&nbsp;</td>
          	    <td width="17%">&nbsp;</td>
       	      </tr>
          	  <tr>
          	    <td>&nbsp;</td>
          	    <td class="normalfnt">Department</td>
          	    <td><select style="width:200px" name="jumpMenu2" id="jumpMenu2">
                   <option>&nbsp;</option>
          	      <option>Costing Department</option>
          	      <option >IT Department</option>
       	        </select></td>
          	    <td></td>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
       	      </tr>
       	    </table></td>
          </tr>
          <tr><td>
          <div  style="overflow:scroll;width:100%;height:400px;margin-top:10px" id="divGrid">
            <table  border="0" cellpadding="0" cellspacing="1" bgcolor="#FFCC33" class="grid">
              <tr class="gridHeader">
                <td  height="24"  colspan="3"  ><input  style="width:350px;visibility:hidden;" type="text" name="textfield" id="textfield" /></td>
                <!--               <td width="37"  bgcolor="#FFFFFF" class="normalfntMid">View</td>
                <td width="33"  bgcolor="#FFFFFF" class="normalfntMid">Add</td>
                <td width="39"  bgcolor="#FFFFFF" class="normalfntMid">Edit</td>
                <td width="41" bgcolor="#FFFFFF" class="normalfntMid"  >Delete</td>
                <td width="48" bgcolor="#FFFFFF" class="normalfntMid"  >1st Aproval </td>
                <td width="44" bgcolor="#FFFFFF" class="normalfntMid"  >2nd Apprval</td>
                <td width="51" bgcolor="#FFFFFF" class="normalfntMid"  >3rd Approval</td>
                <td width="51" bgcolor="#FFFFFF" class="normalfntMid"  >4th Approval</td>
                <td width="44" bgcolor="#FFFFFF" class="normalfntMid"  >Cancel</td>
                <td width="44" bgcolor="#FFFFFF" class="normalfntMid"  >Reject</td>
                <td width="44" bgcolor="#FFFFFF" class="normalfntMid"  >Revise</td>
                <td width="45" bgcolor="#FFFFFF" class="normalfntMid"  >Export to Excel</td>
                <td width="51" bgcolor="#FFFFFF" class="normalfntMid"  >Export to PDF</td>-->
                <?php 
					$sql = "select column_name,column_comment as name from information_schema.columns where table_name = 'menus' and column_comment<>''";
					$result = $db->RunQuery($sql);
					$permisionCount=0;
					while($row=mysqli_fetch_array($result))
					{
						$permisionCount++;
				?>
                 <td  style="width:50px" ><?php echo $row['name']; ?></td>
                 <?php
					}
				 ?>
              </tr>

				 <?php 
					$sql = "SELECT
						menus.intId,
						menus.strName
					FROM menus
					WHERE
						menus.intParentId =  '0'
						ORDER BY
						menus.intOrderBy ASC";
					$result = $db->RunQuery($sql);
					$permisionCount=0;
					while($row=mysqli_fetch_array($result))
					{
						
				?>
              <tr class="normalfnt">
                <td width="16" bgcolor="#FFFFFF" class="normalfntMid">&nbsp;</td>
                <td colspan="2" bgcolor="#CCCCCC" style="color:#000"><?php echo $row['strName']; ?></td>
                <td align="center" colspan="<?php echo $permisionCount; ?>" bgcolor="#FFFFFF">&nbsp;</td>
               
              </tr>
              <?php
					}
			  ?>
              
              <tr class="normalfnt">
                <td bgcolor="#FFFFFF" class="normalfntMid">&nbsp;</td>
                <td width="61" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="234" bgcolor="#FFFFFF">User</td>
                <td align="center" bgcolor="#FFFFFF"><input name="checkbox" type="checkbox" class="txtbox" id="checkbox" /></td>
                
              </tr>
            </table>
          </div>
          </td></tr>
          </table>
</div>
</div>
</body>
</html>

<?php
################################
####  Roshan Perera  ###########
####  copyright 2011 ###########
####  Database Handling Module #
################################
?>
<title>User Permision</title>
<script type="text/javascript">
	function updateUserPermision(obj)
	{
		var id = obj.id;
		var userId = document.getElementById('cboUser').value;
		var value = 0 ;
		if(obj.checked)value = 1;
		
		var url = "presentation/administration/userPermision/menuPermision-db.php?type=updatePermision&id="+id+"&value="+value+"&userId="+userId;
		$.ajax({url:url,type:'POST',async:false});
	}
	function updateUserPermision2(obj)
	{
		var id = obj.parentNode.parentNode.id;
		var fieldName = obj.id;
	
		var userId = document.getElementById('cboUser').value;
		var value = 0 ;
		if(obj.checked)value = 1;
		
		var url = "presentation/administration/userPermision/menuPermision-db.php?type=updatePermision2&id="+id+"&value="+value+"&userId="+userId+"&fieldName="+fieldName;
		$.ajax({url:url,type:'POST',async:false});
	}
	function reloadPage (type)
	{
		window.frmUserPermision.submit();
		/*var cboMainModules	= document.getElementById('cboMainModules').value;
		var cboUser			= document.getElementById('cboUser').value;
		
		if(type=='U')
			window.location.href = '?cboMainModules='+cboMainModules+'&cboUser='+cboUser;
		else
		{
			if($('#cboUser').val()!='')
			window.location.href = '?cboMainModules='+cboMainModules+'&cboUser='+cboUser;
		}*/
	}
	
	$(document).ready(function() {
		$('.butPermision').unbind('click');
		$('.butPermision').click(spermisionclick)
		
	});
	function spermisionclick()
	{
		popupWindow3('1');
		$('#popupContact1').load('presentation/administration/userPermision/spermisionpopup.php?intUser='+$('#cboUser').val()+'&menuId='+$(this).attr('id'),function(){
		
		$('.chkPermision').unbind('click');
		$('.chkPermision').click(setPermision);
		
		$('#frmTechnique #butClose1').click(disablePopup);
		
	});		
	}
	function setPermision()
	{
		
		var id = $(this).attr('id');
		
		//var userId = document.getElementById('cboUser').value;
		var value = ($(this).attr('checked')?1:0);

		var url = "presentation/administration/userPermision/editPermision-db.php?type=updateSpPermision&id="+id+"&value="+value+'&intUser='+$('#cboUser').val();
		$.ajax({url:url,type:'POST',async:false});
	}
	
	
</script>


<?php
/*	//include "../../../dataAccess/Connector.php";	
	$invoiceNo = $_GET['invoiceNo'];
	
	$intShopId = $_POST['cboCustomer'];
	
	//get Header Details
	$sql = "SELECT
			issueheader.intCustomer,
			issueheader.dtInvoiceDate,
			issueheader.dtDeliveryDate
			FROM issueheader
			WHERE
			issueheader.intInvoiceNo =  '$invoiceNo'";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	
	$intCustomer 	= $row['intCustomer'];
	$dtInvoiceDate 	= $row['dtInvoiceDate'];
	$dtDeliveryDate = $row['dtDeliveryDate'];
	$dblDiscount 	= $row['dblDiscount'];*/
	
?>
<form name="frmUserPermision" id="frmUserPermision" method="post" >
   <div>
	<div align="left">
		<div class="trans_layoutXL" >
			<div class="trans_text">User-Menu Permission</div>
<table width="950" border="0" align="center" bgcolor="#FFFFFF">

  <tr>
    <td><table width="100%" border="0">
      <tr>
        <td width="14%" height="22" class="normalfnt">Main Modules</td>
        <td width="35%" class="normalfnt"><select name="cboMainModules" onChange="reloadPage('M');" class="txtbox" id="cboMainModules" style="width:150px" >
          <?php
				$SQL = 	"SELECT
							menus.intId,
							menus.strName
							FROM menus
							WHERE
							menus.intParentId =  '0' AND
							menus.intStatus =  '1'
							GROUP BY
							menus.intOrderBy
						";
				$intMainModules = $_REQUEST['cboMainModules'];
				$result = $db->RunQuery($SQL);
				echo "<option ></option>" ;
				while($row = mysqli_fetch_array($result))
				{
					if($intMainModules==$row["intId"])
						echo "<option selected value=\"". $row["intId"] ."\">" . trim($row["strName"]) ."</option>" ;
					else
						echo "<option value=\"". $row["intId"] ."\">" . trim($row["strName"]) ."</option>" ;
				}
			?>
        </select></td>
        <td width="11%" class="normalfnt">&nbsp;</td>
        <td width="40%" class="normalfnt">&nbsp;</td>
      </tr>
      <tr>
        <td height="22" class="normalfnt">User</td>
        <td class="normalfnt"><select name="cboUser" onChange="reloadPage('U');" class="txtbox" id="cboUser" style="width:150px" >
          <?php
		  
				$SQL = 	"SELECT
							sys_users.intUserId,
							sys_users.strUserName
						FROM sys_users
						WHERE
							sys_users.intStatus =  '1'
						ORDER BY
							sys_users.strUserName ASC

						";
				
				$intUser = $_REQUEST['cboUser'];
				
				$result = $db->RunQuery($SQL);
				echo "<option value=\"". "" ."\">" . "" ."</option>";
				while($row = mysqli_fetch_array($result))
				{
					if($intUser==$row["intUserId"])
						echo "<option selected value=\"". $row["intUserId"] ."\">" . trim($row["strUserName"]) ."</option>" ;
					else
						echo "<option value=\"". $row["intUserId"] ."\">" . trim($row["strUserName"]) ."</option>" ;
				}
			?>
        </select></td>
        <td class="normalfnt">&nbsp;</td>
        <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td  >&nbsp;</td>
        </tr>
      <tr>
        <td class="normalfnt">
          <table width="408" border="1"  cellpadding="0" cellspacing="1" id="tblMainGrn" bgcolor="#E5E5E5" >
			<?php 
			if($intUser=='') die();
			if($intMainModules!='')
				$xx = " and m.intId=$intMainModules ";
/*				echo $sql = "SELECT
							menus.intId,
							menus.strName,
							ifnull(menupermision.intUserId,0) as permision
							FROM
							menus
							left Join menupermision ON menus.intId = menupermision.intMenuId
							WHERE
							menus.intParentId =  '0' AND
							menus.intStatus =  '1' AND
							(menupermision.intUserId =  '$intUser'  or isnull(menupermision.intUserId )) $xx
							order BY
							menus.intOrderBy
						";*/
				$sql = "SELECT
						m.intId,
						m.strName,
						 ifnull((select p.intUserId from menupermision as p where p.intMenuId = m.intId and p.intUserId='$intUser' ),0) as permision
						FROM
						menus as m
						WHERE
						m.intStatus =  '1' and
						m.intParentId =  '0' 
						$xx
						ORDER BY
						m.intOrderBy ASC
						";
						
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					$mainId = $row["intId"];
			?>
              <tr id="<?php echo $mainId; ?>" bgcolor="#E1F5FF" class="normalfnt" >
                  <td width="2%" align="center" height="20" ><input onClick="updateUserPermision(this);" <?php if($row['permision']) echo "checked"; ?> id="<?php echo $row["intId"]; ?>" name="checkbox" type="checkbox" class="txtbox"  /></td>
                  
                  <td colspan="4"  class="normalfnBLD1"><input disabled="disabled"  value="<?php echo $row['strName']; ?>"  style="width:300px;border:none;background-color:#B8D6F5"  type="text" name="textfield" id="textfield" /></td>
                   <?php 
			$sql_permision = "select column_name,column_comment as name from information_schema.columns where  TABLE_SCHEMA='".$_SESSION['Database']."' and  table_name = 'menus' and column_comment<>''";
			$result_permision = $db->RunQuery($sql_permision);
			$permisionCount=0;
			while($row_permision=mysqli_fetch_array($result_permision))
			{
			$permisionCount++;
			?>
			 <td class="gridHeader"  style="width:50px" ><?php echo $row_permision['name']; ?></td>
			 <?php
				}
			 ?>
                  </tr>

                  <?php 
							
/*				$sql2 = "SELECT
							menus.intId,
							menus.strName,
							ifnull(menupermision.intUserId,0) as permision
							FROM
							menus
							left Join menupermision ON menus.intId = menupermision.intMenuId
							WHERE
							menus.intParentId =  '$mainId' AND
							menus.intStatus =  '1' AND
							(menupermision.intUserId =  '$intUser'  or isnull(menupermision.intUserId )) 
							order BY
							menus.intOrderBy
						";*/
				$sql2 = "SELECT
						m.intId,
						m.strName,
						m.strURL,
						 ifnull((select p.intUserId from menupermision as p where p.intMenuId = m.intId and p.intUserId='$intUser' ),0) as permision
						FROM
						menus as m
						WHERE
						m.intStatus =  '1' and
						m.intParentId =  '$mainId' 
						
						ORDER BY
						m.intOrderBy ASC
						";
				$result2 = $db->RunQuery($sql2);
				while($row2=mysqli_fetch_array($result2))
				{
					$subId1 = $row2["intId"];
					
					////////////////////////////// get special permision count ////////////////////
					$sqlsp = "SELECT
								menus_special.strPermisionType
							FROM
								menus_special
							WHERE
								menus_special.intMenuId =  '$subId1' AND
								menus_special.intStatus =  '1'
							";
					$resultsp = $db->RunQuery($sqlsp);
					$spcount = mysqli_num_rows($resultsp);
					///////////////////////////////////////////////////////////////////////////////
					
				?>
                  <tr id="<?php echo $subId1; ?>"  <?php echo ($row2['strURL']!='#'?'bgcolor="#FFFFFF"':'') ?>>
                    <td width="2%">&nbsp;</td>
                    <td width="2%"><input onClick="updateUserPermision(this);" <?php if($row2['permision']) echo "checked"; ?> id="<?php echo $row2["intId"]; ?>" name="<?php echo $row2["intId"]; ?>" type="checkbox" class="txtbox"  /></td>
         
                    
                    <td colspan="3"   class="normalfnt"><?php echo $row2['strName']; if($spcount>0){?><img  id="<?php echo $subId1; ?>" src="images/add_new.png" align="right" class="butPermision mouseover"/><?php } ?></td>
                    
                    <?php 
			$sql_permision = "select column_name  from information_schema.columns where TABLE_SCHEMA='".$_SESSION['Database']."' and  table_name = 'menus' and column_comment<>''";
			$result_permision = $db->RunQuery($sql_permision);
			$permisionCount=mysqli_num_rows($result_permision);
			$permision_fields='';
			
			while($row_permision=mysqli_fetch_array($result_permision))
			{
				$permision_fields .= $row_permision['column_name'].',';
				$fieldArray[] = $row_permision['column_name'];
			}
		 	$permision_fields = substr($permision_fields,0,strlen($permision_fields)-1);
			
			$sql_permision = "select $permision_fields FROM menus WHERE menus.intId =  '$subId1'
							union all
							select $permision_fields FROM menupermision WHERE menupermision.intMenuId =  '$subId1' and intUserId='$intUser'
			";
			$result_permision = $db->RunQuery($sql_permision);
			$R = 0;
			
			while($row_permision=mysqli_fetch_array($result_permision))
			{
				//$colLength =  mysqli_field_len($result_permision,0);
				if($R==0)
				{
					for($x = 0;$x<$permisionCount;$x++)
					{
						$xrow1[$x] = $row_permision[$x];
					}
				}
				if($R==1)
				{
					for($x = 0;$x<$permisionCount;$x++)
					{
						$xrow2[$x] = $row_permision[$x];
						
						?>
                            <td class="normalfntMid"><input onClick="updateUserPermision2(this);" <?php echo ($xrow2[$x]==1?'checked':'') ?> style="visibility:<?php echo ($xrow1[$x]==0?'hidden':''); ?>"  id="<?php echo $fieldArray[$x]; ?>" type="checkbox" class="txtbox"  /></td>
                        
                        <?php
					}
				}
				$R++;
			}
			 ?>
                            
                    </tr>
                  <?php 
							
/*				$sql3 = "SELECT
							menus.intId,
							menus.strName,
							ifnull(menupermision.intUserId,0) as permision
							FROM
							menus
							left Join menupermision ON menus.intId = menupermision.intMenuId
							WHERE
							menus.intParentId =  '$subId1' AND
							menus.intStatus =  '1' AND
							(menupermision.intUserId =  '$intUser'  or isnull(menupermision.intUserId )) 
							order BY
							menus.intOrderBy
						";*/
				$sql3 = "SELECT
						m.intId,
						m.strName,
						m.strURL ,
						 ifnull((select p.intUserId from menupermision as p where p.intMenuId = m.intId and p.intUserId='$intUser' ),0) as permision
						FROM
						menus as m
						WHERE
						m.intStatus =  '1' and
						m.intParentId =  '$subId1' 
						
						ORDER BY
						m.intOrderBy ASC
						";
				$result3 = $db->RunQuery($sql3);
				while($row3=mysqli_fetch_array($result3))
				{
					$subId2 = $row3["intId"];
					
					////////////////////////////// get special permision count ////////////////////
					$sqlsp = "SELECT
								menus_special.strPermisionType
							FROM
								menus_special
							WHERE
								menus_special.intMenuId =  '$subId2' AND
								menus_special.intStatus =  '1'
							";
					$resultsp = $db->RunQuery($sqlsp);
					$spcount = mysqli_num_rows($resultsp);
					///////////////////////////////////////////////////////////////////////////////
				?>
                  <tr id="<?php echo $subId2; ?>" <?php echo ($row3['strURL']!='#'?'bgcolor="#FFFFFF"':'') ?>>
                    <td width="2%">&nbsp;</td>
                    <td width="2%">&nbsp;</td>
                    <td width="2%"><input onClick="updateUserPermision(this);" <?php if($row3['permision']) echo "checked"; ?> id="<?php echo $row3["intId"]; ?>" name="<?php echo $row3["intId"]; ?>" type="checkbox" class="txtbox"  /></td>
                    <td colspan="2"   class="normalfnt"><?php echo $row3['strName']; if($spcount>0){?><img  id="<?php echo $subId2; ?>" src="images/add_new.png" align="right" class="butPermision mouseover"/><?php } ?></td>
                    <?php 
			$sql_permision = "select column_name  from information_schema.columns where TABLE_SCHEMA='".$_SESSION['Database']."' and  table_name = 'menus' and column_comment<>''";
			$result_permision = $db->RunQuery($sql_permision);
			$permisionCount=mysqli_num_rows($result_permision);
			$permision_fields='';
			while($row_permision=mysqli_fetch_array($result_permision))
			{
				$permision_fields .= $row_permision['column_name'].',';
				$fieldArray[] = $row_permision['column_name'];
			}
		 	$permision_fields = substr($permision_fields,0,strlen($permision_fields)-1);
			
			$sql_permision = "select $permision_fields FROM menus WHERE menus.intId =  '$subId2'
							union all
							select $permision_fields FROM menupermision WHERE menupermision.intMenuId =  '$subId2' and intUserId='$intUser'
			
			";
			$result_permision = $db->RunQuery($sql_permision);
			$R = 0;
			while($row_permision=mysqli_fetch_array($result_permision))
			{
				if($R==0)
				{
					for($x = 0;$x<$permisionCount;$x++)
					{
						$xrow1[$x] = $row_permision[$x];
					}
				}
				if($R==1)
				{
					for($x = 0;$x<$permisionCount;$x++)
					{
						$xrow2[$x] = $row_permision[$x];
						
						?>
                            <td class="normalfntMid"><input onClick="updateUserPermision2(this);" <?php echo ($xrow2[$x]==1?'checked':'') ?> style="visibility:<?php echo ($xrow1[$x]==0?'hidden':''); ?>"  id="<?php echo $fieldArray[$x]; ?>" type="checkbox" class="txtbox"   /></td>
                        
                        <?php
					}
				}
				$R++;
			}
			 ?>
                    </tr>
                  <?php 
							
/*				$sql4 = "SELECT
							menus.intId,
							menus.strName,
							ifnull(menupermision.intUserId,0) as permision
							FROM
							menus
							left Join menupermision ON menus.intId = menupermision.intMenuId
							WHERE
							menus.intParentId =  '$subId2' AND
							menus.intStatus =  '1' AND
							(menupermision.intUserId =  '$intUser'  or isnull(menupermision.intUserId )) 
							order BY
							menus.intOrderBy
						";*/
				$sql4 = "SELECT
						m.intId,
						m.strName,
						m.strURL,
						 ifnull((select p.intUserId from menupermision as p where p.intMenuId = m.intId and p.intUserId='$intUser' ),0) as permision
						FROM
						menus as m
						WHERE
						m.intStatus =  '1' and
						m.intParentId =  '$subId2' 
						
						ORDER BY
						m.intOrderBy ASC
						";
				$result4 = $db->RunQuery($sql4);
				while($row4=mysqli_fetch_array($result4))
				{
					$subId3 = $row4['intId'];
					////////////////////////////// get special permision count ////////////////////
					$sqlsp = "SELECT
								menus_special.strPermisionType
							FROM
								menus_special
							WHERE
								menus_special.intMenuId =  '$subId3' AND
								menus_special.intStatus =  '1'
							";
					$resultsp = $db->RunQuery($sqlsp);
					$spcount = mysqli_num_rows($resultsp);
					///////////////////////////////////////////////////////////////////////////////
					
				?>
                  <tr id="<?php echo $subId3; ?>"  <?php echo ($row4['strURL']!='#'?'bgcolor="#FFFFFF"':'') ?>>
                    <td width="2%">&nbsp;</td>
                    <td width="2%">&nbsp;</td>
                    <td width="2%">&nbsp;</td>

                    <td width="2%"><input onClick="updateUserPermision(this);" <?php if($row4['permision']) echo "checked"; ?> id="<?php echo $row4["intId"]; ?>" name="<?php echo $row4["intId"]; ?>" type="checkbox" class="txtbox"  /></td>
                    <td class="normalfntBlue" width="30%"><?php echo $row4['strName']; if($spcount>0){?><img  id="<?php echo $subId3; ?>" src="images/add_new.png" align="right" class="butPermision mouseover"/><?php } ?></td>
                    <?php 
			$sql_permision = "select column_name  from information_schema.columns where TABLE_SCHEMA='".$_SESSION['Database']."' and  table_name = 'menus' and column_comment<>''";
			$result_permision = $db->RunQuery($sql_permision);
			$permisionCount=mysqli_num_rows($result_permision);
			$permision_fields='';
			while($row_permision=mysqli_fetch_array($result_permision))
			{
				$permision_fields .= $row_permision['column_name'].',';
				$fieldArray[] = $row_permision['column_name'];
			}
		 	$permision_fields = substr($permision_fields,0,strlen($permision_fields)-1);
			
			$sql_permision = "select $permision_fields FROM menus WHERE menus.intId =  '$subId3'
							union all
							select $permision_fields FROM menupermision WHERE menupermision.intMenuId =  '$subId3' and intUserId='$intUser'
			";
			//if($row4['intId']==200)
				//echo $sql_permision;
			$result_permision = $db->RunQuery($sql_permision);
			$R=0;
			while($row_permision=mysqli_fetch_array($result_permision))
			{
				if($R==0)
				{
					for($x = 0;$x<$permisionCount;$x++)
					{
						$xrow1[$x] = $row_permision[$x];
					}
				}
				if($R==1)
				{
					for($x = 0;$x<$permisionCount;$x++)
					{
						$xrow2[$x] = $row_permision[$x];
						
						?>
                            <td class="normalfntMid"><input onClick="updateUserPermision2(this);" <?php echo ($xrow2[$x]==1?'checked':'') ?> style="visibility:<?php echo ($xrow1[$x]==0?'hidden':''); ?>"  id="<?php echo $fieldArray[$x]; ?>" type="checkbox" class="txtbox"  /></td>
                        
                        <?php
					}
				}
				$R++;
			}
			 ?>
                    </tr>
                  <?php
				}
			 ?>
                  <?php
				}
			 ?>
                  <?php
				}
			 ?>
                
              <?php
				}
			 ?>
          </table>
          </td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" cellpadding="0" cellspacing="0" >
      <tr>
        <td width="12%" height="29">&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td width="12%">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</div>
</div>
</div>
</form>
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
    <div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>



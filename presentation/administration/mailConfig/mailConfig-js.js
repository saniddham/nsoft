var basePath= 'presentation/administration/mailConfig/';
			
$(document).ready(function() {
	$("#frmMailConfig").validationEngine();
	
  $('#cboModule').change(function(){
	  $('#frmMailConfig #cboEmailEvents').load(basePath+'mailConfig-db-get.php?requestType=loadEvents&moduleNo='+$(this).val());  
  });
  
  $('#frmMailConfig #cboEmailEvents').change(function(){
	  $('#tblMain').load(basePath+'mainGrid.php?eventId='+$(this).val(),function(){ setButtonEvents();setRemoveRow();});  
  });
  $('#frmMailConfig #butNew').click(function(){
	  $('#frmMailConfig').get(0).reset()
	  clearRows();
	  });  
  $('#frmMailConfig #butSave').click(function(){

   setSaveFunction();});
  setButtonEvents();
});

function setSaveFunction()
{
	
	if ($('#frmMailConfig').validationEngine('validate'))   
    	{ 
		
		var arrEmail = "arrEmail=[";
		$('#frmMailConfig #tblMain > .dataRow').each(function(){
			if($(this).attr('id')!='')
			{
				arrEmail+='{"email":"'+($(this).attr('id'))+'"},';
			}
		});  
		arrEmail = arrEmail.substr(0,arrEmail.length-1);
		
		var url = basePath+"mailConfig-db-set.php?requestType=saveDetails&eventId="+$('#cboEmailEvents').val();
		$.ajax({
			url:url,
			async:false,
			dataType: "json", 
			type:'POST',
			data:arrEmail+']',
			success:function(json){
					$('#frmMailConfig #butSave').validationEngine('showPrompt', json.msg,json.type );
					if(json.type=='pass')
					{
						var t=setTimeout("alertx()",3000);
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmMailConfig #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				}		
		});
		
		}
}
function setButtonEvents()
{

	$("#frmMailConfig #butAdd").unbind('click');
	$("#frmMailConfig #butAdd").click(function(){
			setToAddNew(this);
	});
	//setRemoveRow();
	//setSaveFunction();
}

function setToAddNew(obj)
{
	popupWindow3('1');
	$('#popupContact1').html('');
	$('#popupContact1').load(basePath+'popup.php',function(){
		
		$('#frmPopUp #butAdd').click(function(){
			//addEmailsToGrid(mainRowId,typeRowId,cellId);
			$('.chkUsers:checked').each(function(){
				var fromId = $(this).val();
				//var obj = $(this);
				var found = false;
				$('#tblMain >tr').each(function(){
					if(fromId ==$(this).attr('id'))
					{
						found = true;
					}	
				});
				if(found==false)
				{
					
					var tr=document.getElementById('tblMain').insertRow(document.getElementById('tblMain').rows.length);
					tr.setAttribute('class','dataRow');
					document.getElementById('tblMain').rows[document.getElementById('tblMain').rows.length-1].innerHTML = "<td align=\"center\" bgcolor=\"#FFFFFF\">"+
					"<img class=\"mouseover removeRow\" src=\"../../../images/del.png\" /></td>"+
   		 			"<td bgcolor=\"#FFFFFF\" class=\"normalfnt\">"+$(this).parent().parent().find('td').eq(1).html()+"</td>"+
    				"<td bgcolor=\"#FFFFFF\" class=\"normalfnt\">"+$(this).parent().parent().find('td').eq(2).html()+"</td>";
					document.getElementById('tblMain').rows[document.getElementById('tblMain').rows.length-1].id  = fromId;
					setRemoveRow();
					
				}
			});
			disablePopup();
			
		});
		
		$('#frmPopUp #butClose1').click(disablePopup);
		
	});		
	
}

function setRemoveRow()
{
	$('.removeRow').click(function(){
		 $(this).parent().parent().remove();
	});	
}
function clearRows()
{
	var rowCount = document.getElementById('tblMain').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMain').deleteRow(2);
			
	}
}
function alertx()
{
	$('#frmMailConfig #butSave').validationEngine('hide')	;
}
 
// JavaScript Document
var i = 1;
$(document).ready(function(){
	

	$('#frmCapacityChart #cboCompany').change(function(){
		
		if($('#cboCompany').val()=="")
			$('#cboLocation').html('');
		else
		{
			loadLocation(this);
		}
		
	});
	$('#frmCapacityChart #butView').click(function(){
				
		CallFunction();	
		
	});
	
});
function loadLocation(obj)
{
	var url     = "capacityChart-db-get.php?requestType=loadLocation&companyId="+$(obj).val();
	var httpObj = $.ajax({url:url,async:false})
	$('#frmCapacityChart #cboLocation').html(httpObj.responseText);	
}
function alertx()
{
	$('#frmCapacityChart #cboLocation').validationEngine('hide');
}
function drawGraph(chart)
{
	var myChart = new FusionCharts( "Column3D", "chartId"+i, "1300", "650" );
	myChart.setXMLData(chart);
	myChart.render("chartContainer");
	i++;
}
function getDetails()
{
	if($('#cboCompany').val()!="")
		{
			if($('#cboLocation').val()=="")
			{
				$('#frmCapacityChart #cboLocation').validationEngine('showPrompt', 'Please Choose Location!','fail');
				var t=setTimeout("alertx()",3000);
				return;
			}		
		}
		var requestType = 'getDetails';
		var url = "capacityChart-db-get.php";
			var obj = $.ajax({
				url:url,
				dataType: "json",  
				data:$("#frmCapacityChart").serialize()+'&requestType='+requestType,
				async:false,
				success:function(json)
				{
					drawGraph(json.chart)
				},
				error:function(xhr,status)
				{
						
				}		
		});
}
function CallFunction() {
        getDetails();
        setInterval("getDetails()", 20000);
    }
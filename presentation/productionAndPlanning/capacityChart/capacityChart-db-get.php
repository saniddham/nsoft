<?php 
session_start();
$backwardseperator 	= "../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 	 		= $_SESSION['userId'];
$requestType 		= $_REQUEST['requestType'];
$maxCapTeamId		= "";
include "{$backwardseperator}dataAccess/Connector.php";
include("../../../libraries/fusionChart/Includes/FusionCharts.php");

if($requestType=="loadLocation")
{
	$companyId = $_REQUEST['companyId'];
	
	$sql = "SELECT
			mst_locations_user.intLocationId,
			mst_locations.strName AS location
			FROM
			mst_locations_user
			Inner Join mst_locations ON mst_locations.intId = mst_locations_user.intLocationId
			Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
			WHERE
			mst_locations_user.intUserId ='$userId' AND
			mst_locations.intCompanyId = '$companyId' ";
	$result = $db->RunQuery($sql);
	
	$option = "<option value=\"\"></option>";
	while($row = mysqli_fetch_array($result))
	{
		$option .="<option value=\"".$row['intLocationId']."\">".$row['location']."</option>";
	}
	echo $option;
}
else if($requestType=="getDetails")
{
	$companyId  	= $_REQUEST['cboCompany'];
	$locationId 	= $_REQUEST['cboLocation'];
	$machineType  	= $_REQUEST['cboMachineType'];
	$month		  	= $_REQUEST['cboMonth'];
	$year			= $_REQUEST['cboYear'];
	
	
	$sql1= "SELECT SUM(dblAvailableStrokes) AS totAvailableQty
			FROM (SELECT dblAvailableStrokes FROM plan_upload_details PUD 
			INNER JOIN plan_upload_header PUH ON PUD.intPrePlanId=PUH.intId
			WHERE (MONTH(strDate)='$month') AND (YEAR(strDate)='$year') ";
	if($machineType!='')
		$sql1.= "AND PUH.intPlantId='$machineType' ";
	if($companyId!='')
		$sql1.= "AND PUH.intCompanyId='$companyId' ";
	if($locationId!='')
		$sql1.= "AND PUH.intLocationId='$locationId' ";	
			
	$sql1.="GROUP BY PUD.strDate) AS tb1 ";
	
	$result1 = $db->RunQuery($sql1);
	$row1 = mysqli_fetch_array($result1);
	$totalCapacity = $row1['totAvailableQty'];
	
	$availableCapacity = round($totalCapacity,0);
	
	$sql2= "SELECT IFNULL((SUM(PUD.dblNoOfStrokes)),0) AS planQty,IFNULL((SUM(dblActualQty)),0) AS actualQty
			FROM plan_upload_header PUH
			LEFT JOIN plan_upload_details PUD ON PUD.intPrePlanId=PUH.intId
			WHERE (MONTH(strDate)='$month') AND (YEAR(strDate)='$year') "; 
	if($machineType!='')
		$sql2.= "AND PUH.intPlantId='$machineType' ";
	if($companyId!='')
		$sql2.= "AND PUH.intCompanyId='$companyId' ";
	if($locationId!='')
		$sql2.= "AND PUH.intLocationId='$locationId' ";	
	
	$result2 = $db->RunQuery($sql2);
	$row2 = mysqli_fetch_array($result2);
	
	$totPlanQty   = $row2['planQty'];
	$totalActQty  = $row2['actualQty'];	

	$planCapacity = round($totPlanQty,0);
	$actualQty    = round($totalActQty,0);
	
	$strXML = "<chart yAxisMaxValue='5000000' maxColWidth='180' formatNumberScale='0' caption=\"Main Summary - Cummulative Status\" xAxisName=\"\" yAxisName=\"\" numberPrefix=\"\"> ";
	$strXML .= "<set label='Available' value='" . $availableCapacity . "' color='#A1A1A1' />";
	$strXML .= "<set label='Plan' value='" . $planCapacity . "' color='#00FF00' />";
	$strXML .= "<set label='Actual' value='" . $actualQty . "' color='#0000EE' />";
	$strXML .= "<set label='Freeze Plan' value='" . $planCapacity . "' />";
	$strXML .= "<set label='Actual Delivery' value='" . $actualQty . "' />";
	$strXML .= "<styles>";
	$strXML .= "<definition>";
	$strXML .= "<style name='myCaptionFont' type='font' font='Arial' size='26' color='#7E2217' bold='1'/>";
	$strXML .= "<style name='myLabelsFont' type='font' font='Arial' size='24' bold='1'/>";
	$strXML .= "<style name='myValueFont' type='font' font='Arial' size='20' color='000000' bold='1'/>";
	$strXML .= "<style name='myYaxisFont' type='font' font='Arial' size='20' bold='1'/>";		
	$strXML .= "</definition>";
	$strXML .= "<application>";
	$strXML .= "<apply toObject='Caption' styles='myCaptionFont' />";
	$strXML .= "<apply toObject='DataLabels' styles='myLabelsFont' />";
	$strXML .= "<apply toObject='DataValues' styles='myValueFont' />";
	$strXML .= "<apply toObject='YAxisValues' styles='myYaxisFont' />";
	$strXML .= "</application>";
	$strXML .= "</styles>";
	$strXML .= "</chart>";
	
	$response['chart'] 	= $strXML;
	echo json_encode($response);
}
?>
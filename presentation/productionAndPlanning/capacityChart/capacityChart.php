<?php
session_start();
$backwardseperator = "../../../";
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Capacity Chart</title>

<link href="<?php echo $mainPath ?>images/logo_sm.png" rel="shortcut icon"  />
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>
<script type="application/javascript" src="capacityChart-js.js"></script>
<script type="text/javascript" src="../../../libraries/fusionChart/jquery.min.js"></script>
<SCRIPT LANGUAGE="Javascript" SRC="../../../libraries/fusionChart/FusionCharts.js"></SCRIPT>
<script>
 FusionCharts.setCurrentRenderer('javascript');
</script>
<script>
//setTimeout("window.location.replace('../weeklyCapacityChart/weeklyCapacityChart.php')" ,10000)
</script>
<link rel="stylesheet" href="../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../libraries/validate/template.css" type="text/css">

<style>
.border-right {
	border-right-width: medium;
	border-right-style: solid;
	border-right-color: #000000;
	font-size:24px;
	text-align:right;
}
.large-font {
	font-size:24px;
	text-align:right;
}
.border-top {
	border-top-width: medium;
	border-top-style: solid;
	border-top-color: #000000;
	font-size:24px;
	text-align:center;
}
</style>
</head>

<body onload="CallFunction();">
<form id="frmCapacityChart" name="frmCapacityChart" autocomplete="off">

<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>

<table width="100%" height="100%" cellpadding="2" cellspacing="2" border="0" >
<tr>
    <td colspan="8" align="center">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
    	<td width="5%" class="normalfnt">&nbsp;Company</td>
        <td width="17%" class="normalfnt"><?php 
					$sql = "SELECT intId,strName FROM mst_companies WHERE intStatus=1 ORDER BY strName";
					$result = $db->RunQuery($sql);
			?>              
            <select style="width:180px;" name="cboCompany" id="cboCompany" >
            <option value=""></option>
                <?php
				while($row = mysqli_fetch_array($result))
				{
					echo "<option value=\"".$row["intId"]."\" >".$row["strName"]."</option>";	
				}
			?>              </select></td>
        <td width="5%" class="normalfnt">&nbsp;Location</td>
        <td width="17%" class="normalfnt"><select style="width:180px;" name="cboLocation" id="cboLocation" >
        <option value=""></option>
        </select></td>
        <td width="8%" class="normalfnt">&nbsp;Machine Type</td>
        <td width="15%" class="normalfnt"><select style="width:150px;" name="cboMachineType" id="cboMachineType" >
	<?php
			$sql = "SELECT 	intId, strName
					FROM mst_printertypes 
					WHERE intStatus = 1
					ORDER BY strName ";
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				if($cboPrintType==$row['intId'])
					echo "<option selected=\"selected\" value=\"".$row["intId"]."\" >".$row["strName"]."</option>";
				else
					echo "<option value=\"".$row["intId"]."\" >".$row["strName"]."</option>";
				if($cboPrintType=='')
				$cboPrintType = $row['intId'];
			}
			
    ?>
    </select> </td>
        <td width="3%" class="normalfnt">Year</td>
        <td width="9%" class="normalfnt"><select name="cboYear" id="cboYear" style="width:90px">
          <?php
		$sql ="SELECT DISTINCT YEAR(strDate) as intYear FROM plan_upload_details";
		$result = $db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			if($row['intYear']==date('Y'))
				echo "<option value=\"".$row["intYear"]."\" selected=\"selected\" >".$row["intYear"]."</option>";
			else
				echo "<option value=\"".$row["intYear"]."\" >".$row["intYear"]."</option>";
		}
	  ?>
        </select></td>
    <td width="4%" class="normalfnt">Month</td>
    <td width="9%" class="normalfnt">
      <select name="cboMonth" id="cboMonth" style="width:90px">
      <?php
		$sql ="SELECT intMonthId,strMonth FROM mst_month ORDER BY intMonthId";
		$result = $db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			echo "<option value=\"".$row["intMonthId"]."\" >".$row["strMonth"]."</option>";
		}
	  ?>
      </select></td>
    <td width="8%" class="normalfnt"><img border="0" src="../../../images/Tview.jpg" alt="View" name="butView" width="92" height="24"  class="mouseover" id="butView"/></td>
    </tr>
    </table>
    </td>
    <td align="center"><a href="<?php echo $mainPath ?>main.php" title="Home"><img src="<?php echo $mainPath ?>images/home_sm.png" alt="Home" width="22" height="23"  border="0" /></a></td>
    </tr>
<tr>
<td>
<div id="chartContainer">FusionCharts XT will load here!</div> 
</td>
</tr>
</table>

</form>
</body>
</html>

<?php
session_start();
ini_set('max_execution_time',600000);
$backwardseperator 	= "../../../";
require_once "{$backwardseperator}dataAccess/Connector.php";
require_once '../../../libraries/excel/Classes/PHPExcel.php';
require_once '../../../libraries/excel/Classes/PHPExcel/IOFactory.php';

$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$intPubCompanyId = $row['intCompanyId']; 
}

$fromDate	= $_REQUEST["fromDate"]; 
$toDate		= $_REQUEST["toDate"];
$location	= $_REQUEST["location"];
$printType	= $_REQUEST["printType"];
$prevPrntId = "";
$arrDate    = array();
$arrExcelLetter = array(1=>'A',2=>'B',3=>'C',4=>'D',5=>'E',6=>'F',7=>'G',8=>'H',9=>'I',10=>'J',11=>'K',12=>'L',13=>'M',14=>'N',15=>'O',16=>'P',17=>'Q',18=>'R',19=>'S',20=>'T',21=>'U',22=>'V',23=>'W',24=>'X',25=>'Y',26=>'Z');

$objPHPExcel = new PHPExcel();
$i = 2;
$j = 1;

	/*$start = strtotime($fromDate);
	$end = strtotime($toDate);
	$days_between = ceil(abs($end - $start) / 86400);
	$newFrmDate = $fromDate;
	
	for($t=0;$t<=$days_between;$t++)
	{
		$arrDate[$t]   = $newFrmDate ;	
		$date = date_create($newFrmDate);
		date_add($date, date_interval_create_from_date_string('1 days'));
		$nextDt = date_format($date, 'Y-m-d');
		$newFrmDate = $nextDt;
	}*/
	$objPHPExcel->getActiveSheet()->getStyle("A2")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("999999");
	$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->getColor()->setRGB("0000ff");
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,2,"Qpet-Planner")->getStyle("A2")->getFont()->setBold(true);
	
	$sql = "SELECT DISTINCT dtmDate,strDayStatus FROM plan_calender 
			WHERE intCompanyId='$intPubCompanyId' AND 
			intLocationId='$location' AND
			dtmDate BETWEEN DATE('$fromDate') AND DATE('$toDate')";
	 
	$result = $db->RunQuery($sql);
	$x = 2;
	$y = 0;
	$chk = false;
	while($row=mysqli_fetch_array($result))
	{
		$previ = $i-1;
		if(!$chk)
			$letter = $arrExcelLetter[$x];
		else
			$letter = $arrExcelLetter[$y].$arrExcelLetter[$x];
		if($x==27)
		{
			$chk = true;
			$x = 1;
			$y += 1;
			$letter = $arrExcelLetter[$y].$arrExcelLetter[$x];	
		}
		if($row['strDayStatus']=='work')
		{
			$objPHPExcel->getActiveSheet()->getStyle("$letter$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("BFF9C2");
			$objPHPExcel->getActiveSheet()->getStyle("$letter$i")->getFont()->setName('Times New Roman');
			$objPHPExcel->getActiveSheet()->getStyle("$letter$i")->getFont()->setSize(9);
		}
		else if($row['strDayStatus']=='saturday')
		{
			$objPHPExcel->getActiveSheet()->getStyle("$letter$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("F9FABE");
			$objPHPExcel->getActiveSheet()->getStyle("$letter$i")->getFont()->setName('Times New Roman');
			$objPHPExcel->getActiveSheet()->getStyle("$letter$i")->getFont()->setSize(9);
		}
		else if($row['strDayStatus']=='sunday')
		{
			$objPHPExcel->getActiveSheet()->getStyle("$letter$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("FAD9AD");
			$objPHPExcel->getActiveSheet()->getStyle("$letter$i")->getFont()->setName('Times New Roman');
			$objPHPExcel->getActiveSheet()->getStyle("$letter$i")->getFont()->setSize(9);
		}
		else
		{
			$objPHPExcel->getActiveSheet()->getStyle("$letter$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("ee82ee");
			$objPHPExcel->getActiveSheet()->getStyle("$letter$i")->getFont()->setName('Times New Roman');
			$objPHPExcel->getActiveSheet()->getStyle("$letter$i")->getFont()->setSize(9);
		}
		$objPHPExcel->getActiveSheet()->getStyle("$letter$previ")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("4682b4");	
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,$i,$row["dtmDate"]);
		$j++;
		$x++;
		
	}
	$i++;
	$sql2 = "SELECT pt.intId AS teamId,
			pt.strName AS teamName,
			pt.intPlantId,
			mp.strName AS printType
			FROM plan_teams pt
			INNER JOIN mst_printertypes mp ON mp.intId=pt.intPlantId 
			WHERE 
			pt.intCompanyId='$intPubCompanyId' AND
			pt.intLocationId='$location' ";
	
	if($printType!="")
		$sql2 .= "AND pt.intPlantId='$printType' ";
	
	$sql2 .= "ORDER BY pt.intPlantId,pt.intId ";
	
	$result2 = $db->RunQuery($sql2);
	$chkFirstTime = true;
	
	while($row2 = mysqli_fetch_array($result2))
	{
		$currPrintId = $row2['intPlantId'];
		if($chkFirstTime)
		{
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,1,$row2['printType'])->getStyle("A1")->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("A1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("4682b4");
			$prevPrntId = $currPrintId;
		}
		if($prevPrntId!=$currPrintId)
		{
			$objPHPExcel->getActiveSheet()->insertNewRowBefore($i, 1);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$i,$row2['printType'])->getStyle("A$i")->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("A$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("4682b4");
			$i++;
		}

		$a = 1;
		$sql3 = " SELECT dtmDate,dblWorkingHours,intMaxCapacity,strDayStatus
					FROM plan_calender
					WHERE intCompanyId='$intPubCompanyId' AND
					intLocationId= '$location' AND
					intTeamId = '".$row2['teamId']."' AND
					dtmDate BETWEEN DATE('$fromDate') AND DATE('$toDate') 
					order by dtmDate";
		$result3 = $db->RunQuery($sql3);
		$e = 2;
		$f = 0;
		$chkSts = false;
		$prevStripId ="";
		$comQty = 0;
		while($row3 = mysqli_fetch_array($result3))
		{
			$capacity = (($row3['dblWorkingHours']*$row3['intMaxCapacity'])==0?"":$row3['dblWorkingHours']*$row3['intMaxCapacity']);
			
			$sql4 = " SELECT intStripId,dblQty,dblActualQty
						FROM plan_strip_details
						WHERE intCompanyId='$intPubCompanyId' AND
						intLocationId='$location' AND
						intSubTeam='".$row2['teamId']."' AND
						DATE(dtmDate) ='".$row3['dtmDate']."'";
			$result4 = $db->RunQuery($sql4);
			$row4 = mysqli_fetch_array($result4);
			$currStripId = $row4['intStripId'];
			
			/*if($prevStripId!=$currStripId)
			{
				$sql5 = "SELECT dblCompleteQty FROM plan_stripes WHERE intStripId='".$row4['intStripId']."';";
				$result5 = $db->RunQuery($sql5);
				$row5 = mysqli_fetch_array($result5);
				if(mysqli_num_rows($result5)>0)
				{
					$comQty    = $row5['dblCompleteQty'];
				}			
				$comBalQty = $comQty-$row4["dblQty"];
			}*/
			
			$planQty 	= $row4["dblQty"];
			$balQty  	= (($capacity-$planQty)==0?"":($capacity-$planQty));
			$actualQty 	= $row4["dblActualQty"];
			
			if(!$chkSts)
				$letter2 = $arrExcelLetter[$e];
			else
				$letter2 = $arrExcelLetter[$f].$arrExcelLetter[$e];
			if($e==27)
			{
				$chkSts = true;
				$e = 1;
				$f += 1;
				$letter2 = $arrExcelLetter[$f].$arrExcelLetter[$e];	
			}
			$plnClmn = $i+1;
			$balClmn = $i+2;
			$actClmn = $i+3;
			if($row3['strDayStatus']=='work')
			{
				
				//$objPHPExcel->getActiveSheet()->getStyle("$letter2$i")->getFont()->setName('Times New Roman');
				//$objPHPExcel->getActiveSheet()->getStyle("$letter2$i")->getFont()->setSize(9);
				
			}
			else if($row3['strDayStatus']=='saturday')
			{
				$objPHPExcel->getActiveSheet()->getStyle("$letter2$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("F9FABE");
				$objPHPExcel->getActiveSheet()->getStyle("$letter2$plnClmn")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("F9FABE");
				$objPHPExcel->getActiveSheet()->getStyle("$letter2$balClmn")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("F9FABE");
				$objPHPExcel->getActiveSheet()->getStyle("$letter2$actClmn")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("F9FABE");
				//$objPHPExcel->getActiveSheet()->getStyle("$letter2$i")->getFont()->setName('Times New Roman');
				//$objPHPExcel->getActiveSheet()->getStyle("$letter2$i")->getFont()->setSize(9);
			}
			else if($row3['strDayStatus']=='sunday')
			{
				$objPHPExcel->getActiveSheet()->getStyle("$letter2$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("FAD9AD");
				$objPHPExcel->getActiveSheet()->getStyle("$letter2$plnClmn")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("FAD9AD");
				$objPHPExcel->getActiveSheet()->getStyle("$letter2$balClmn")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("FAD9AD");
				$objPHPExcel->getActiveSheet()->getStyle("$letter2$actClmn")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("FAD9AD");
				//$objPHPExcel->getActiveSheet()->getStyle("$letter2$i")->getFont()->setName('Times New Roman');
				//$objPHPExcel->getActiveSheet()->getStyle("$letter2$i")->getFont()->setSize(9);
			}
			else
			{
				$objPHPExcel->getActiveSheet()->getStyle("$letter2$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("ff0000");
				$objPHPExcel->getActiveSheet()->getStyle("$letter2$plnClmn")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("ff0000");
				$objPHPExcel->getActiveSheet()->getStyle("$letter2$balClmn")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("ff0000");
				$objPHPExcel->getActiveSheet()->getStyle("$letter2$actClmn")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("ff0000");
				//$objPHPExcel->getActiveSheet()->getStyle("$letter2$i")->getFont()->setName('Times New Roman');
				//$objPHPExcel->getActiveSheet()->getStyle("$letter2$i")->getFont()->setSize(9);
			}
			if($prevPrntId!=$currPrintId)
			{
				$previ = $i-1;
				$objPHPExcel->getActiveSheet()->getStyle("$letter2$previ")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("4682b4");
			}
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($a,$i,$capacity);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($a,$plnClmn,$planQty)->getStyle("$letter2$plnClmn")->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($a,$balClmn,$balQty)->getStyle("$letter2$balClmn")->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("$letter2$balClmn")->getFont()->getColor()->setRGB("0000FF");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($a,$actClmn,($actualQty==0?"":$actualQty))->getStyle("$letter2$actClmn")->getFont()->setBold(true);
			$a++;	
			$e++;
		}
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$i,($row2["teamName"]==""?"Sub Team":$row2["teamName"]))->getStyle("A$i")->getFont()->setSize(9);
		$objPHPExcel->getActiveSheet()->getStyle("A$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("CCCCCC");
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$i+1,"plan")->getStyle("A$plnClmn")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$i+2,"balance")->getStyle("A$balClmn")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A$balClmn")->getFont()->getColor()->setRGB("0000FF");
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$i+3,"actual")->getStyle("A$actClmn")->getFont()->setBold(true);
		$prevPrntId = $currPrintId;
		$chkFirstTime = false;
		$i+=4;	
	}

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="capacityReport"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output'); 
echo 'done';
exit;
?>
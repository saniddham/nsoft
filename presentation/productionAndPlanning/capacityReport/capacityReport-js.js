// JavaScript Document
$(document).ready(function(){

	$("#frmCapacityPlan #butDownload").click(function(){
		if($("#frmCapacityPlan").validationEngine('validate')){
			downloadFile();
		}
	});
});
function downloadFile()
{
	var fromDate 	 = $('#frmCapacityPlan #dtFromDate').val();
	var toDate 	 	 = $('#frmCapacityPlan #dtToDate').val();
	var location  	 = $('#frmCapacityPlan #cboLocation').val();
	var printType  	 = $('#frmCapacityPlan #cboPrintType').val();

	window.open('capacityPlanReport-xlsx.php?fromDate='+fromDate+'&toDate='+toDate+'&location='+location+'&printType='+printType);		

}
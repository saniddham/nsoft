// JavaScript Document
$(document).ready(function(){

	$("#frmUploadPlan #butDownload").click(function(){
		if($("#frmUploadPlan").validationEngine('validate')){
			downloadFile();
		}
	});
	$("#frmUploadPlan #butReport").click(function(){
		if($("#frmUploadPlan").validationEngine('validate')){
			viewReport();
		}
	});
});
function downloadFile()
{
	var fromDate 	 = $('#frmUploadPlan #dtFromDate').val();
	var toDate 	 	 = $('#frmUploadPlan #dtToDate').val();
	var location  	 = $('#frmUploadPlan #cboLocation').val();

	window.open('uploadPlanReport-xlsx.php?fromDate='+fromDate+'&toDate='+toDate+'&location='+location);		
}
function viewReport()
{
	var fromDate 	 = $('#frmUploadPlan #dtFromDate').val();
	var toDate 	 	 = $('#frmUploadPlan #dtToDate').val();
	var location  	 = $('#frmUploadPlan #cboLocation').val();

	window.open('uploadPlanReport-html.php?fromDate='+fromDate+'&toDate='+toDate+'&location='+location);		
}
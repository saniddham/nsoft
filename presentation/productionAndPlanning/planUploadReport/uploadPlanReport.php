<?php
session_start();
$backwardseperator = "../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];
$dateOneMonthAdded = date("Y-m-d", strtotime("+1 month") );
$lastMonth		   = date("Y-m-d", strtotime("-1 month") ) ;

include  "{$backwardseperator}dataAccess/permisionCheck.inc";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Upload Plan Report</title>

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>
<script type="application/javascript" src="uploadPlanReport-js.js"></script>

<link rel="stylesheet" href="../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../libraries/validate/template.css" type="text/css">
</head>

<body>
<form id="frmUploadPlan" name="frmUploadPlan" autocomplete="off">
<table width="100%" border="0" align="center">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<link rel="stylesheet" type="text/css" href="../../../libraries/calendar/theme.css" />
<script src="../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div align="center">
		<div class="trans_layoutD">
		  <div class="trans_text">Upload Plan Report</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="585" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Date From&nbsp;&nbsp;</td>
                <td width="145"><input name="dtFromDate" type="text" value="<?php   echo $lastMonth; ?>" class="txtbox" id="dtFromDate" style="width:120px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                <td width="28" class="normalfnt">To</td>
                <td width="216"><input name="dtToDate" type="text" value="<?php  echo $dateOneMonthAdded ?>" class="txtbox" id="dtToDate" style="width:120px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"  /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
              </tr>
              <tr>
                <td width="67" class="normalfnt">&nbsp;</td>
                <td width="95" class="normalfnt">Location</td>
                <td colspan="3">
                <?php
                $sql = "SELECT
							mst_locations_user.intLocationId,
							concat(mst_companies.strName ,\" - \",mst_locations.strName) AS companyName
							FROM
							mst_locations_user
							Inner Join mst_locations ON mst_locations.intId = mst_locations_user.intLocationId
							Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
							WHERE
							mst_locations_user.intUserId =   ".$_SESSION["userId"]."";
					$result = $db->RunQuery($sql);
			?>      
            			 <select style="width:300px;" name="cboLocation" id="cboLocation" >
                <?php
				while($row = mysqli_fetch_array($result))
				{
					if($_SESSION['CompanyID']==$row['intLocationId'])
						echo "<option selected=\"selected\" value=\"".$row["intLocationId"]."\" >".$row["companyName"]."</option>";
					else
						echo "<option value=\"".$row["intLocationId"]."\" >".$row["companyName"]."</option>";
				}
			?>              </select></td>
                </tr>
            </table></td>
            </tr>
          <tr>
            <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
            <tr>
            	<td align="center" bgcolor="">&nbsp;</td>
            </tr>
              <tr>
                <td width="100%" align="center" bgcolor=""><a style="vertical-align:top" name="butNew" class="button white medium" id="butNew">&nbsp;&nbsp;New&nbsp;&nbsp;</a><a style="vertical-align:top" name="butDownload" class="button white medium" id="butDownload">&nbsp;Excel&nbsp;</a><a style="vertical-align:top" name="butReport" class="button white medium" id="butReport">Report</a><a href="../../../main.php" style="vertical-align:top" name="butClose" class="button white medium" id="butClose">&nbsp;Close&nbsp;</a></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
</form>
</body>
</html>

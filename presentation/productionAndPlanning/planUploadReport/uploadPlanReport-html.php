<?php
	session_start();
	ini_set('max_execution_time',600000);
	$backwardseperator 	= "../../../";
	require_once "{$backwardseperator}dataAccess/Connector.php";
	
	$userId 	= $_SESSION['userId'];
	$fromDate	= $_REQUEST["fromDate"]; 
	$toDate		= $_REQUEST["toDate"];
	$location	= $_REQUEST["location"];
	$arrDate    = array();
	
	$sql = "SELECT
			CONCAT(mst_companies.strName ,\" - \",mst_locations.strName) AS companyName
			FROM
			mst_locations_user
			INNER JOIN mst_locations ON mst_locations.intId = mst_locations_user.intLocationId
			INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
			WHERE
			mst_locations_user.intUserId ='$userId' AND
			mst_locations.intId='$location' ";
	
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	$companyLoc = $row["companyName"];
	
	$sql2 ="SELECT MAX(DATE(dtmUploadDate)) AS lastUpdDate FROM plan_upload_header WHERE intLocationId='$location' ";
	$result2 = $db->RunQuery($sql2);
	$row2 = mysqli_fetch_array($result2);
	$lstUpdDate = $row2["lastUpdDate"];
	
	$start = strtotime($fromDate);
	$end = strtotime($toDate);
	$days_between = ceil(abs($end - $start) / 86400);
	$newFrmDate = $fromDate;

	for($t=0;$t<=$days_between;$t++)
	{
		$arrDate[$t]   = $newFrmDate ;
		$date = date_create($newFrmDate);
		date_add($date, date_interval_create_from_date_string('1 days'));
		$nextDt = date_format($date, 'Y-m-d');
		$newFrmDate = $nextDt;
		
	}
	$i = 1;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Upload Plan Report</title>
<style type="text/css">

	.compulsoryRed
	{
		font-family: Verdana;
		font-size: 11px;
		color:#F00;
		margin: 0px;
		font-weight: normal;
		text-align:left;
	}
	.compulsoryRedSm
	{
		font-family: Verdana;
		font-size: 10px;
		color:#F00;
		margin: 0px;
		font-weight: normal;
		text-align:left;
	}
	.rotate
	{
		-moz-transform: rotate(-90.0deg);  /* FF3.5+ */
		-o-transform: rotate(-90.0deg);  /* Opera 10.5 */
		-webkit-transform: rotate(-90.0deg);  /* Saf3.1+, Chrome */
		filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
		-ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */
	}
	.tableBorderNew
	{
		background-color:#000;
		border-spacing:1px;
	}

</style>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../images/logo_sm.png" rel="shortcut icon"  />

</head>

<body>
<form id="frmPlanUploadReport" name="frmPlanUploadReport">
<div wi align="center">
<div style="background-color:#FFF" ><strong>PLAN UPLOAD REPORT</strong>
<table width="100%" border="0"  align="center" bgcolor="#FFFFFF">
<tr>
  <td>&nbsp;</td>
</tr>
<tr>
  <td class="normalfnt"><b><?php echo $companyLoc; ?></b></td>
</tr>
<tr>
  <td class="normalfnt"><b>Production Plan for Printing Process</b></td>
</tr>
<tr>
  <td class="normalfnt"><b>Last Update:</b>&nbsp;&nbsp;&nbsp;<?php echo $lstUpdDate; ?></td>
</tr>
<tr>
  <td>&nbsp;</td>
</tr>
<tr>
<td><table width="100%" class="tableBorderNew">
  <tr bgcolor="#FFFFFF">
    <td colspan="20" class="normalfnt">Avilable Strokes</td>
    <?php
	for($arr=0;$arr<count($arrDate);$arr++)
	{
		$sql ="SELECT DISTINCT dblAvailableStrokes 
				FROM plan_upload_details 
				INNER JOIN plan_upload_header ON plan_upload_details.intPrePlanId=plan_upload_header.intId
				WHERE plan_upload_details.strDate='".$arrDate[$arr]."' AND
				plan_upload_header.intLocationId='$location' ";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
	?>
    	<td class="normalfnt" style="text-align:right"><?php echo $row['dblAvailableStrokes']; ?></td>
    <?php
	}
	?> 
    </tr>
  <tr bgcolor="#FFFFFF">
    <td colspan="20" class="compulsoryRed">Total No of Strokes - Plan</td>
    <?php
	for($arr=0;$arr<count($arrDate);$arr++)
	{
		$sql = "SELECT IFNULL((SUM(dblNoOfStrokes)),0) AS totNoOfStrokes
				FROM plan_upload_details 
				INNER JOIN plan_upload_header ON plan_upload_details.intPrePlanId=plan_upload_header.intId
				WHERE strDate='".$arrDate[$arr]."' AND
				plan_upload_header.intLocationId='$location' ";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
	?>
    	<td class="compulsoryRed" style="text-align:right"><?php echo $row['totNoOfStrokes']; ?></td>
    <?php
	}
	?> 
    </tr>
  <tr bgcolor="#FFFFFF">
    <td colspan="20" class="normalfnt">Utilized % (Plan vs Avilable)</td>
    <?php
	for($arr=0;$arr<count($arrDate);$arr++)
	{
		$sql = "SELECT IFNULL((ROUND((SUM(dblNoOfStrokes)/dblAvailableStrokes),2)*100),0) AS utilized 
				FROM plan_upload_details 
				INNER JOIN plan_upload_header ON plan_upload_details.intPrePlanId=plan_upload_header.intId
				WHERE strDate='".$arrDate[$arr]."' AND
				plan_upload_header.intLocationId='$location' ";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
	?>
    	<td class="normalfnt" style="text-align:right"><?php echo number_format($row['utilized'],0).'%'; ?></td>
    <?php
	}
	?> 
    </tr>
  <tr bgcolor="#FFFFFF">
    <td colspan="20" class="normalfnt">Daily plan cum out put</td>
    <?php
	for($arr=0;$arr<count($arrDate);$arr++)
	{
		$sql = "SELECT IFNULL((SUM(dblNoOfStrokes)),0) AS totNoOfStrokes
				FROM plan_upload_details 
				INNER JOIN plan_upload_header ON plan_upload_details.intPrePlanId=plan_upload_header.intId
				WHERE strDate='".$arrDate[$arr]."' AND
				plan_upload_header.intLocationId='$location' ";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
	?>
    	<td class="normalfnt" style="text-align:right"><?php echo $row['totNoOfStrokes']; ?></td>
    <?php
	}
	?> 
    </tr>
  <tr bgcolor="#FFFFFF">
    <td height="76" class="normalfnt" style="text-align:center">&nbsp;<b>Method of Print</b>&nbsp;</td>
    <td class="normalfnt" style="text-align:center">&nbsp;<b>N<br /></b>&nbsp;<b>E<br /></b>&nbsp;<b>W</b>&nbsp;</td>
    <td class="normalfnt" style="text-align:center">&nbsp;<b>Marketer</b>&nbsp;</td>
    <td class="normalfnt" style="text-align:center">&nbsp;<b>Team</b>&nbsp;</td>
    <td class="normalfnt" style="text-align:center">&nbsp;<b>Customer</b>&nbsp;</td>
    <td class="normalfnt" style="text-align:center">&nbsp;<b>SO/ Colour</b>&nbsp;</td>
    <td class="normalfnt" style="text-align:center">&nbsp;<b>Style</b>&nbsp;</td>
    <td class="normalfnt" style="text-align:center">&nbsp;<b>Graphic</b>&nbsp;</td>
    <td class="normalfnt" style="text-align:center">&nbsp;<b>Sample Year(ERP</b>)&nbsp;</td>
    <td class="normalfnt" style="text-align:center">&nbsp;<b>Sample No(ERP</b>)&nbsp;</td>
    <td class="normalfnt" style="text-align:center">&nbsp;<b>Customer PO No</b>&nbsp;</td>
    <td class="normalfnt" style="text-align:center">&nbsp;<b>Board Size</b>&nbsp;</td>
    <td class="normalfnt" style="text-align:center">&nbsp;<b>Strokes</b>&nbsp;</td>
    <td class="normalfnt" style="text-align:center">&nbsp;<b>Stroke convertion Factor</b>&nbsp;</td>
    <td class="normalfnt" style="text-align:center">&nbsp;<b>VA Process</b>&nbsp;</td>
    <td class="normalfnt" style="text-align:center">&nbsp;<b>Daily Requirement</b>&nbsp;</td>
    <td class="normalfnt" style="text-align:center">&nbsp;<b>PSD</b>&nbsp;</td>
    <td class="normalfnt" style="text-align:center">&nbsp;<b>Revised PSD</b>&nbsp;</td>
    <td class="normalfnt" style="text-align:center">&nbsp;<b>Total</b>&nbsp;</td>
    <td class="normalfnt" bgcolor="#FCD5B4" style="text-align:center">&nbsp;<b>Comments</b>&nbsp;</td>
    
    <?php
	for($arr=0;$arr<count($arrDate);$arr++)
	{
	?>
    	<td class="normalfnt"><div class='rotate'><?php echo $arrDate[$arr]; ?></div></td>
    <?php
	}
	?> 
  </tr>
  <?php
  	$sql = " SELECT DATE(PU.dtmUploadDate) AS uploadDate, 
				PU.intId,
				PU.strMethodofPrint, 
				PU.strMarketer, 
				PT.strName AS Team, 
				PU.strCustomer, 
				PU.strColor, 
				PU.strStyleNo, 
				PU.strGraphic, 
				PU.intSampleYear, 
				PU.intSampleNo, 
				PU.strCustomerPONo, 
				PU.strBordSize, 
				PU.dblStrokes, 
				PU.dblStrokeConFactor, 
				PU.strVAProcess, 
				PU.dblDailyRequirement, 
				PU.dtmPsd, 
				PU.dtmRevisedPsd, 
				PU.dblTotal
				FROM 
				plan_upload_header PU
				INNER JOIN plan_teams PT ON PU.intTeamId=PT.intId
				WHERE PU.intLocationId='$location'
				ORDER BY PU.intId ";
	
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$intId				= $row["intId"];
		$MethodofPrint  	= $row["strMethodofPrint"];
		$Marketer   		= $row["strMarketer"];
		$Team   			= $row["Team"];
		$Customer 			= $row["strCustomer"];
		$Color   			= $row["strColor"];
		$StyleNo   			= $row["strStyleNo"];
		$Graphic 			= $row["strGraphic"]; 
		$SampleYear 		= $row["intSampleYear"]; 
		$SampleNo 			= $row["intSampleNo"]; 
		$CustomerPONo 		= $row["strCustomerPONo"]; 
		$BordSize	   		= $row["strBordSize"]; 
		$Strokes 			= $row["dblStrokes"];
		$StrokeConFactor 	= $row["dblStrokeConFactor"];
		$VAProcess 			= $row["strVAProcess"];
		$DailyRequirement 	= $row["dblDailyRequirement"];
		$Psd 				= $row["dtmPsd"];
		$RevisedPsd 		= $row["dtmRevisedPsd"];
		$Total 				= $row ["dblTotal"];
		
		for($x=0;$x<2;$x++)
		{
			if($i%2==0)
			{
				$operation = "NoOfStrokes";
			?>
				<tr bgcolor="#FFFFFF">
                <td class="compulsoryRed" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo $MethodofPrint;  ?>&nbsp;</td>
            	<td class="compulsoryRed" style="text-align:center" nowrap="nowrap">&nbsp;</td>
                <td class="compulsoryRed" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo $Marketer;  ?>&nbsp;</td>
                <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;</td>
                <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;</td>
                <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;</td>
                <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;</td>
                <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;</td>
                <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;</td>
                <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;</td>
                <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;</td>
                <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;</td>
                <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;</td>
                <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;</td>
                <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;</td>
                <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;</td>
                <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;</td>
                <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;</td>
                <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;</td>
                <td class="compulsoryRedSm" bgcolor="#FCD5B4" style="text-align:center;font-size:10px" nowrap="nowrap">&nbsp;<?php echo "No of Strokes";  ?>&nbsp;</td>
            <?php
			}
			else
			{	
				$operation = "PlanPcs";
			?>
            <tr bgcolor="#FFFFFF">
            <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo $MethodofPrint;  ?>&nbsp;</td>
            <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;</td>
            <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo $Marketer;  ?>&nbsp;</td>
            <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo $Team;  ?>&nbsp;</td>
            <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo $Customer;  ?>&nbsp;</td>
            <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo $Color;  ?>&nbsp;</td>
            <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo $StyleNo;  ?>&nbsp;</td>
            <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo $Graphic;  ?>&nbsp;</td>
            <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo $SampleYear;  ?>&nbsp;</td>
            <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo $SampleNo;  ?>&nbsp;</td>
            <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo $CustomerPONo;  ?>&nbsp;</td>
            <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo $BordSize;  ?>&nbsp;</td>
            <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo $Strokes;  ?>&nbsp;</td>
            <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo $StrokeConFactor;  ?>&nbsp;</td>
            <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo $VAProcess;  ?>&nbsp;</td>
            <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo $DailyRequirement;  ?>&nbsp;</td>
            <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo $Psd;  ?>&nbsp;</td>
            <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo $RevisedPsd;  ?>&nbsp;</td>
            <td class="normalfnt" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo $Total;  ?>&nbsp;</td>
            <td class="normalfntsm" bgcolor="#FCD5B4" style="text-align:center" nowrap="nowrap">&nbsp;<?php echo "Plan Pcs";  ?>&nbsp;</td>
            <?php
			}
			
			$sql2 = " SELECT intPrePlanId,strDate,dblAvailableStrokes,dblPlanPcs,dblNoOfStrokes
						FROM plan_upload_details
						WHERE intPrePlanId='$intId' AND
						strDate BETWEEN DATE('$fromDate') AND DATE('$toDate')";
			$result2 = $db->RunQuery($sql2);
			while($row2=mysqli_fetch_array($result2))
			{
			
            	if($operation == "NoOfStrokes")
				{
			?>
					<td class="compulsoryRedSm" style="text-align:right"><?php echo $row2['dblNoOfStrokes']; ?></td>		
            <?php
				}
				else
				{
				?>
                	<td class="normalfntsm" style="text-align:right"><?php echo $row2['dblPlanPcs']; ?></td>
            <?php
				}
			}
			$i++;
		}
	}
  ?>
</table></td>
</tr>
</table>
</div>
</div>
</form>

</body>
</html>
<?php
	session_start();
	ini_set('max_execution_time',600000);
	$backwardseperator 	= "../../../";
	require_once "{$backwardseperator}dataAccess/Connector.php";
	require_once '../../../libraries/excel/Classes/PHPExcel.php';
	require_once '../../../libraries/excel/Classes/PHPExcel/IOFactory.php';
	
	$userId 	= $_SESSION['userId'];
	$fromDate	= $_REQUEST["fromDate"]; 
	$toDate		= $_REQUEST["toDate"];
	$location	= $_REQUEST["location"];
	$arrDate    = array();
	$uploadDate = "";
	
	$sql = "SELECT
			CONCAT(mst_companies.strName ,\" - \",mst_locations.strName) AS companyName
			FROM
			mst_locations_user
			INNER JOIN mst_locations ON mst_locations.intId = mst_locations_user.intLocationId
			INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
			WHERE
			mst_locations_user.intUserId ='$userId' AND
			mst_locations.intId='$location' ";
	
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	$companyLoc = $row["companyName"];
	
	$file = 'planUploadReport-xslx.xls'; //LOAD TEMPARARY FILE
	
	if (!file_exists($file))
		exit("Can't find $fileName ");
	
	$objPHPExcel = PHPExcel_IOFactory::load($file);
	$i 		= 11;
	$j 		= 20;
	
	$start = strtotime($fromDate);
	$end = strtotime($toDate);
	$days_between = ceil(abs($end - $start) / 86400);
	$newFrmDate = $fromDate;

	for($t=0;$t<=$days_between;$t++)
	{
		$arrDate[$t]   = $newFrmDate ;
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,10,$newFrmDate);	
		$date = date_create($newFrmDate);
		date_add($date, date_interval_create_from_date_string('1 days'));
		$nextDt = date_format($date, 'Y-m-d');
		$newFrmDate = $nextDt;
		$j++;
	}
	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,1,$companyLoc);
	
	$sql = " SELECT DATE(PU.dtmUploadDate) AS uploadDate, 
				PU.intId,
				PU.strMethodofPrint, 
				PU.strMarketer, 
				PT.strName AS Team, 
				PU.strCustomer, 
				PU.strColor, 
				PU.strStyleNo, 
				PU.strGraphic, 
				PU.intSampleYear, 
				PU.intSampleNo, 
				PU.strCustomerPONo, 
				PU.strBordSize, 
				PU.dblStrokes, 
				PU.dblStrokeConFactor, 
				PU.strVAProcess, 
				PU.dblDailyRequirement, 
				PU.dtmPsd, 
				PU.dtmRevisedPsd, 
				PU.dblTotal
				FROM 
				plan_upload_header PU
				INNER JOIN plan_teams PT ON PU.intTeamId=PT.intId
				WHERE PU.intLocationId='$location'
				ORDER BY PU.intId ";
	
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$intId				= $row["intId"];
		$uploadDate   		= $row["uploadDate"];
		$MethodofPrint  	= $row["strMethodofPrint"];
		$Marketer   		= $row["strMarketer"];
		$Team   			= $row["Team"];
		$Customer 			= $row["strCustomer"];
		$Color   			= $row["strColor"];
		$StyleNo   			= $row["strStyleNo"];
		$Graphic 			= $row["strGraphic"]; 
		$SampleYear 		= $row["intSampleYear"]; 
		$SampleNo 			= $row["intSampleNo"]; 
		$CustomerPONo 		= $row["strCustomerPONo"]; 
		$BordSize	   		= $row["strBordSize"]; 
		$Strokes 			= $row["dblStrokes"];
		$StrokeConFactor 	= $row["dblStrokeConFactor"];
		$VAProcess 			= $row["strVAProcess"];
		$DailyRequirement 	= $row["dblDailyRequirement"];
		$Psd 				= $row["dtmPsd"];
		$RevisedPsd 		= $row["dtmRevisedPsd"];
		$Total 				= $row ["dblTotal"];
	
		for($x=0;$x<2;$x++)
		{
			if($i%2==0)
			{
				$operation = "dblNoOfStrokes";
				$objPHPExcel->getActiveSheet()->getStyle("T$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("FCD5B4");
				$objPHPExcel->getActiveSheet()->getStyle("A$i:IV$i")->getFont()->getColor()->setRGB("ff0000");
				$objPHPExcel->getActiveSheet()->getStyle("A$i:IV$i")->getFont()->setName('Trebuchet MS');
				$objPHPExcel->getActiveSheet()->getStyle("A$i:IV$i")->getFont()->setSize(9);
				//$objPHPExcel->getActiveSheet()->getStyle("C$i")->getFont()->getColor()->setRGB('#ff0000');
				//$objPHPExcel->getActiveSheet()->getStyle("T$i")->getFont()->getColor()->setRGB('#ff0000');
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$i,$MethodofPrint);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,$i,$Marketer);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19,$i,"No of Strokes");	
			}
			else
			{	
				$operation = "dblPlanPcs";
				$objPHPExcel->getActiveSheet()->getStyle("T$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("FCD5B4");
				$objPHPExcel->getActiveSheet()->getStyle("A$i:IV$i")->getFont()->setName('Trebuchet MS');
				$objPHPExcel->getActiveSheet()->getStyle("A$i:IV$i")->getFont()->setSize(9);
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$i,$MethodofPrint);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,$i,$Marketer);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,$i,$Team);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$i,$Customer);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,$i,$Color);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$i,$StyleNo);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$i,$Graphic);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8,$i,$SampleYear);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9,$i,$SampleNo);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10,$i,$CustomerPONo);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11,$i,$BordSize);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12,$i,$Strokes);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13,$i,$StrokeConFactor);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14,$i,$VAProcess);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15,$i,$DailyRequirement);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16,$i,$Psd);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17,$i,$RevisedPsd);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18,$i,$Total);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19,$i,"Plan Pcs");	
			}
			
			$sql2 = " SELECT intPrePlanId,strDate,dblAvailableStrokes,dblPlanPcs,dblNoOfStrokes
						FROM plan_upload_details
						WHERE intPrePlanId='$intId' AND
						strDate BETWEEN DATE('$fromDate') AND DATE('$toDate')";
			$result2 = $db->RunQuery($sql2);
			while($row2=mysqli_fetch_array($result2))
			{
				$z=20;
				for($arr=0;$arr<count($arrDate);$arr++)	
				{
					if($row2['strDate']==$arrDate[$arr])
					{
						
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($z,$i,$row2[$operation]);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($z,5,$row2["dblAvailableStrokes"]);
						
						$sql3 = " SELECT SUM(dblNoOfStrokes) AS totNoOfStrokes,ROUND((SUM(dblNoOfStrokes)/dblAvailableStrokes),2) AS utilized 
									FROM plan_upload_details WHERE strDate='".$row2['strDate']."' ";
						$result3 = $db->RunQuery($sql3);
						$row3 = mysqli_fetch_array($result3);
						
						//$utilized = $row3["utilized"]."%";
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($z,6,$row3["totNoOfStrokes"]);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($z,7,$row3["utilized"]);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($z,8,$row3["totNoOfStrokes"]);
					}
					$z++;
				}		
			}
			$i++;
		}
	}
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,3,$uploadDate);
	
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'.$file.'"');
	header('Cache-Control: max-age=0');
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output'); 
	echo 'done';
	exit;



?>
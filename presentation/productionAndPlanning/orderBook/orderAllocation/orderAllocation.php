<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];
if($_SERVER['REQUEST_METHOD'] == "POST")
{
	$frmDate 	= trim($_REQUEST['txtDate']);
	$toDate 	= trim($_REQUEST['txtDate2']);
	$customer  	= trim($_REQUEST['cboCustomer']);
	$process   	= trim($_REQUEST['cboProcess']);
}
include  "{$backwardseperator}dataAccess/permisionCheck.inc";
$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	$locationId = $_SESSION["CompanyID"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Orders Allocation</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../masterData/css/planning.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="orderAllocation-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">

<style type="text/css">
.web_dialog_overlay
{
   position: fixed;
   top: 0;
   right: 0;
   bottom: 0;
   left: 0;
   height: 100%;
   width: 100%;
   margin: 0;
   padding: 0;
   background: #000000;
   opacity: .15;
   filter: alpha(opacity=15);
   -moz-opacity: .15;
   z-index: 0;
   display: none;
}
.web_dialog
{
   display: none;
   position: fixed;
   width: 450px;
   height: 350px;
   top: 50%;
   left: 50%;
   margin-left: -190px;
   margin-top: -100px;
   background-color: #F0F0F0;
   border: 2px solid #336699;
   padding: 0px;
   z-index: 0;
   font-family: Verdana;
   font-size: 10pt;
}
.web_dialog_title
{
   border-bottom: solid 2px #336699;
   background-color: #336699;
   padding: 4px;
   color: White;
   font-weight:bold;
}
.web_dialog_title a
{
   color: White;
   text-decoration: none;
}
.align_right
{
   text-align: right;
}
</style>
<script type="text/javascript">
function pageSubmit()
{
	document.getElementById('frmOrderAllocation').submit();	
}
</script>
</head>

<body>
<form id="frmOrderAllocation" name="frmOrderAllocation" method="post" action="orderAllocation.php" autocomplete="off">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div align="center">
<div class="trans_layoutL">
<div class="trans_text">Orders Allocation</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td colspan="2" bgcolor="#0033FF"><strong><span class="normaltxtmidb2">U  n  a  l  l  o  c  a  t  e  d  O  r  d  e  r  s</span></strong></td>
</tr>
<tr class="tableBorder_allRound">
  <td colspan="2">
  <table width="100%">
  <tr class="normalfntMid">
    <td width="4%">From</td>
    <td width="16%"><input name="txtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="txtDate" style="width:98px;" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
    <td width="2%">To</td>
    <td width="16%"><input name="txtDate2" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="txtDate2" style="width:98px;" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
    <td width="7%">Customer</td>
    <td width="23%">
    <select name="cboCustomer" id="cboCustomer"  style="width:100%" >
      <option value=""></option>
      <?php  $sql = "SELECT
						mst_customer.intId,
						mst_customer.strName,
						mst_financecustomeractivate.intCompanyId
						FROM
						mst_customer
						Inner Join mst_financecustomeractivate ON mst_customer.intId = mst_financecustomeractivate.intCustomerId
						WHERE
						mst_customer.intStatus =  1 AND
						mst_financecustomeractivate.intCompanyId =  '$companyId'
						order by strName";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							if($customer==$row['intId'])
								echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
							else
								echo "<option value=\"".$row['intId']."\" >".$row['strName']."</option>";
						}
          ?>
    </select></td>
    <td width="9%">Process</td>
    <td width="13%">
    <select name="cboProcess" id="cboProcess" style="width:110px" class="validate[required] item">
      <option value="Printing">Printing</option>
      <option value="Curing">Curing</option>
      <option value="Drying">Drying</option>
      <option value="Pressing">Pressing</option>
    	</select></td>
  <td width="10%"><img src="../../../../images/search.png" width="20" height="20" onclick="pageSubmit();"/></td>
  </tr>
  </table>
  </td>
  </tr>
<tr>
      <td colspan="2"><table width="99%">
        <tr>
          <td>
          <div style="overflow:scroll;width:900px;height:200px;" id="divGrid">
          <table width="115%" id="tblMainGrid1" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
            <tr class="">
              <td width="2%" bgcolor="#FAD163" class="normalfntMid">&nbsp;</td>
              <td width="16%"   height="27" bgcolor="#FAD163" class="normalfntMid"><strong>Plan/SalesOrder No</strong> <strong><img id="butItem" src="../../../../images/add_new.png" width="15" height="15" style="display:none"/></strong></td>
              <td width="9%"    bgcolor="#FAD163" class="normalfntMid"><strong>Style</strong></td>
              <td width="14%"    bgcolor="#FAD163" class="normalfntMid"><strong>Graphic No</strong></td>
              <td width="10%"    bgcolor="#FAD163" class="normalfntMid"><strong>Combo</strong></td>
              <td width="18%"    bgcolor="#FAD163" class="normalfntMid"><strong>Customer</strong></td>
              <td width="11%"    bgcolor="#FAD163" class="normalfntMid"><strong>PSD</strong></td>
              <td width="12%"  bgcolor="#FAD163" class="normalfntMid"  ><strong>Delivery Date</strong></td>
              <td width="8%"   bgcolor="#FAD163" class="normalfntMid" ><strong>Quantity</strong></td>
              <td width="15%"   bgcolor="#FAD163" class="normalfntMid" nowrap="nowrap" ><strong>Print Size</strong></td>
              <td width="8%"   bgcolor="#FAD163" class="normalfntMid" ><strong>Panels</strong></td>
              <td width="8%"   bgcolor="#FAD163" class="normalfntMid" ><strong>Sample Sheet</strong></td>
              </tr>
            <?php
			$sql = "SELECT
					pp.intId,
					pp.intPrePlanRefNo,
					YEAR(pp.dtmUploadDate) AS planYear,
					pp.strStyleNo,
					pp.strGraphic,
					pp.strCombo,
					pp.strSalesOrder,
					pp.intSampleNo,
					pp.intSampleYear,
					pp.intRevisionNo,
					pp.strPrintName,
					pp.intCustomerId,
					mc.strName,
					pp.dtmPsd,
					pp.dtmLastDeliveryDate,
					pp.strOrdQty,
					mp.strName as printSize,
					pp.intPanels,
					pp.intPrintSize
					FROM
					plan_pre_plan pp
					INNER JOIN mst_customer mc ON mc.intId = pp.intCustomerId
					INNER JOIN mst_printsizes mp ON mp.intId = pp.intPrintSize
					WHERE
					pp.intEnterLocationId = '$companyId' AND
					pp.intSampleYear IS NOT NULL AND
					pp.intSampleNo IS NOT NULL AND
					pp.intRevisionNo IS NOT NULL AND
					pp.intSampleNo IS NOT NULL AND
					pp.strCombo <>'' AND
					pp.strPrintName <>'' AND
					pp.intPrintSize IS NOT NULL AND
					pp.intPanels IS NOT NULL AND
					CONCAT(pp.intPrePlanRefNo,'-',pp.intId) NOT IN(SELECT CONCAT(intPlanNo,'-',intPlanId) FROM plan_allocateorders )" ;
			if($customer!="")
				$sql .=" AND pp.intCustomerId = '$customer' ";	
			
			 $result = $db->RunQuery($sql);
			 while($row=mysqli_fetch_array($result))
			 {
				$planId = $row['intId'];
			 ?>
             
             <script type="text/javascript">
		
				var arrNewProp = [];
				var planId					=	<?php echo $planId; ?>;
				arrNewProp['sampleNo']		=	<?php echo $row['intSampleNo']; ?>;
				arrNewProp['sampleYear']	=	<?php echo $row['intSampleYear']; ?>;
				arrNewProp['revNo']			=	<?php echo $row['intRevisionNo']; ?>;
				arrNewProp['printName']		=  '<?php echo $row['strPrintName']; ?>';
				arrNewProp['comboName']		=  '<?php echo $row['strCombo']; ?>';
			
				arrDetails[planId]=arrNewProp;
				arrNewProp = null;
			 </script>
             
              <tr class="normalfnt mainRow" bgcolor="#FFFFFF" id="<?php echo $row['planYear']?>">
              <td class="normalfntMid planId" id="<?php echo $planId?>">
              <input type="checkbox" name="<?php echo $row['strSalesOrder'];?>" id="<?php echo $row['strSalesOrder'];?>" class="allocate" /></td>
              <td  class="normalfntMid planNo" id="<?php echo $row['intPrePlanRefNo'];?>"><?php echo $row['intPrePlanRefNo'];?>/<?php echo $row['strSalesOrder'];?></td>
              <td class="normalfntMid style"><?php echo $row['strStyleNo'];?></td>
              <td class="normalfntMid graphic"><?php echo $row['strGraphic'];?></td>
              <td class="normalfntMid combo"><?php echo $row['strCombo'];?></td>
              <td class="normalfntMid customer" id="<?php echo $row['intCustomerId'];?>"><?php echo $row['strName'];?></td>
              <td class="normalfntMid psd"><?php echo $row['dtmPsd'];?></td>
              <td align="center" class="normalfntMid delDate"><?php echo $row['dtmLastDeliveryDate'];?></td>
              <td class="normalfntMid qty"><?php echo $row['strOrdQty'];?></td>
              <td class="normalfntMid printSize" id="<?php echo $row['intPrintSize'];?>"><?php echo $row['printSize'];?></td>
               <td class="normalfntMid panel"><?php echo $row['intPanels'];?></td>
              <td class="normalfntMid"><a target="_blank"  href="../../../../presentation/costing/sample/samplePrices/addNew/sampleReport.php?no=<?php echo $row['intSampleNo']?>&year=<?php echo $row['intSampleYear']?>&revNo=<?php echo $row['intRevisionNo'] ?>">view</a></td>
              </tr>
		   <?php 
            } 
           ?>
           <tr>
           <td colspan="12" class="normalfntMid"><hr /><strong>Customer Plan Allocation Area</strong></td>
           </tr>
            <?php
			if($searchValue!='')
			$wherePart = " and $optionValue like '%$searchValue%'";
			
			$sql = "";
			 $result = $db->RunQuery($sql);
			 while($row=mysqli_fetch_array($result))
			 {
				$id = $row['intOrderNo'];
			 ?>
			  <tr class="normalfnt mainRow" bgcolor="#FFFFFF" id="<?php echo $row['intOrderYear']?>">
			  <td class="normalfntMid orderId" id="<?php echo $row['intOrderNo']?>">
			  <input type="checkbox" name="<?php echo $row['strSalesOrderNo'];?>" id="<?php echo $row['strSalesOrderNo'];?>" class="allocate" /></td>
			  <td  class="normalfntMid salesOrderId" id="<?php echo $row['intSalesOrderId'];?>"><?php echo $row['intOrderNo'];?>-<?php echo $row['strSalesOrderNo'];?></td>
			  <td class="normalfntMid style"><?php echo $row['strStyleNo'];?></td>
			  <td class="normalfntMid graphic"><?php echo $row['strGraphicNo'];?></td>
			  <td class="normalfntMid combo"><?php echo $row['strCombo'];?></td>
			  <td class="normalfntMid customer" id="<?php echo $row['intId'];?>"><?php echo $row['strName'];?></td>
			  <td class="normalfntMid psd"><?php echo $row['dtPSD'];?></td>
			  <td align="center" class="normalfntMid delDate"><?php echo $row['dtDeliveryDate'];?></td>
			  <td class="normalfntMid qty"><?php echo $row['intQty'];?></td>
              <td class="normalfntMid qty"><?php echo $row['printSize'];?></td>
               <td class="normalfntMid qty"><?php echo $row['intPanels'];?></td>
			  <td class="normalfntMid"><a target="_blank"  href="../../../../presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder.php?orderNo=<?php echo $row['intOrderNo']?>&orderYear=<?php echo $row['intOrderYear']?>">view</a></td>
			  </tr>
		   <?php 
			} 
           ?>
          </table>
          </div>
          </td>
        </tr>
      </table></td>
      </tr>
<tr>
  <td colspan="2" bgcolor="#0033FF"><strong><span class="normaltxtmidb2">A  l  l  o  c  a  t  e  d  O  r  d  e  r  s</span></strong></td>
</tr>
<tr class="tableBorder_allRound">
  <td colspan="2">
  <table width="100%">
  <tr class="normalfntMid">
    <td width="15%">Choose Location <span class="compulsoryRed">*</span></td>
    <td width="18%">
    <select  name="cboLocation" id="cboLocation" style="width:100%">
      <option value=""></option>
      <?php  $sql = "SELECT
					mst_locations.intId,
					mst_locations.strName
					FROM mst_locations
					order by strName
					";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
					}
	   ?>
    </select></td>
    <td width="8%"><input style="background-color:#C0C0C0" type="button" name="butCapacity" id="butCapacity" value="Capacity" /></td>
    <td width="43%">&nbsp;</td>
    <td width="7%">
    <input style="background-color:#C0C0C0" type="button" name="butAllocate" id="butAllocate" value="Allocate" /></td>
    <td width="9%">
    <input style="background-color:#C0C0C0" type="button" name="butUnallocate" id="butUnallocate" value="Unallocate" /></td>
    </tr>
  </table>
  </td>
  </tr>
<tr>
<td colspan="2"><table width="99%">
<tr>
          <td>
          <div style="overflow:scroll;width:900px;height:200px;" id="divGrid">
          <table width="115%" id="tblMainGrid2" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
            <tr class="">
              <td width="2%" bgcolor="#FAD163" class="normalfntMid">&nbsp;</td>
              <td width="15%"   height="27" bgcolor="#FAD163" class="normalfntMid"><strong>Plan/SalesOrder No</strong> <strong><img id="butItem" src="../../../../images/add_new.png" width="15" height="15" style="display:none"/></strong></td>
              <td width="9%"    bgcolor="#FAD163" class="normalfntMid"><strong>Style</strong></td>
              <td width="10%"    bgcolor="#FAD163" class="normalfntMid"><strong>Graphic No</strong></td>
              <td width="8%"    bgcolor="#FAD163" class="normalfntMid"><strong>Combo</strong></td>
              <td width="19%"    bgcolor="#FAD163" class="normalfntMid"><strong>Customer</strong></td>
              <td width="8%"    bgcolor="#FAD163" class="normalfntMid"><strong>PSD</strong></td>
              <td width="11%"  bgcolor="#FAD163" class="normalfntMid"  ><strong>Delivery Date</strong></td>
              <td width="8%"   bgcolor="#FAD163" class="normalfntMid" ><strong>Quantity</strong></td>
               <td width="8%"   bgcolor="#FAD163" class="normalfntMid" nowrap="nowrap" ><strong>Print Size</strong></td>
                <td width="8%"   bgcolor="#FAD163" class="normalfntMid" ><strong>Panels</strong></td>
              <td width="10%"   bgcolor="#FAD163" class="normalfntMid" ><strong>Location</strong></td>
              <td width="10%"   bgcolor="#FAD163" class="normalfntMid" ><strong>Sample Sheet</strong></td>
              </tr>
            <?php
			
			$sql = "SELECT 	pa.intPlanId, 
					pa.intPlanNo, 
					pa.intOrderYear,  
					pa.strSalesOrder, 
					pa.strStyle, 
					pa.strGraphic, 
					pa.strCombo, 
					pa.intCustomerId, 
					pa.strProcess, 
					pa.dtPSD, 
					pa.dtDeliveryDate, 
					pa.dblQty, 
					pa.intLocationId, 
					pa.strIsAllocated, 
					pa.strIsPlaned ,
					ml.strName as location,
					mc.strName,
					pp.intSampleNo,
					pp.intSampleYear,
					pp.intRevisionNo,
					mp.strName as printSize,
					pp.intPrintSize,
					pp.intPanels
					FROM 
					plan_allocateorders pa
					INNER JOIN mst_customer mc ON mc.intId=pa.intCustomerId
					INNER JOIN mst_locations ml ON ml.intId=pa.intLocationId
					INNER JOIN plan_pre_plan pp ON pa.intPlanId=pp.intId
					INNER JOIN mst_printsizes mp ON mp.intId = pp.intPrintSize
					WHERE
					pa.strIsAllocated =  'Y' AND
					pa.strIsPlaned =  'N'
					";
			 $result = $db->RunQuery($sql);
			 while($row=mysqli_fetch_array($result))
			 {
				$id = $row['intOrderNo'];
			 ?>
              <tr class="normalfnt mainRow" bgcolor="#FFFFFF" id="<?php echo $row['intOrderYear']?>">
              <td class="normalfntMid planId" id="<?php echo $row['intPlanId']?>">
              <input type="checkbox" name="chkPlan" id="<?php echo $row['strSalesOrder'];?>" class="unAllocate" /></td>
              <td  class="normalfntMid planNo" id="<?php echo $row['intPlanNo'];?>"><?php echo $row['intPlanNo'];?>/<?php echo $row['strSalesOrder'];?></td>
              <td class="normalfntMid style"><?php echo $row['strStyle'];?></td>
              <td class="normalfntMid graphic"><?php echo $row['strGraphic'];?></td>
              <td class="normalfntMid combo"><?php echo $row['strCombo'];?></td>
              <td class="normalfntMid customer" id="<?php echo $row['intCustomerId'];?>"><?php echo $row['strName'];?></td>
              <td class="normalfntMid psd"><?php echo $row['dtPSD'];?></td>
              <td align="center" class="normalfntMid delDate"><?php echo $row['dtDeliveryDate'];?></td>
              <td class="normalfntMid qty"><?php echo $row['dblQty'];?></td>
              <td class="normalfntMid printSize" id="<?php echo $row['intPrintSize']; ?>"><?php echo $row['printSize'];?></td>
              <td class="normalfntMid panel"><?php echo $row['intPanels'];?></td>
              <td class="normalfntMid"><?php echo $row['location'];?></td>
              <td class="normalfntMid"><a target="_blank"  href="../../../../presentation/costing/sample/samplePrices/addNew/sampleReport.php?no=<?php echo $row['intSampleNo']?>&year=<?php echo $row['intSampleYear']?>&revNo=<?php echo $row['intRevisionNo'] ?>">view</a></td>
              </tr>
		   <?php 
            } 
           ?>
          </table>
          </div>
          </td>
        </tr>
</table></td>
</tr>
<tr>
<td width="100%" align="center" bgcolor=""><!--<img  style="display:none" border="0" src="../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/>--><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
</tr>
</table>
</div>
</div>
<!--------------------------Location Capacity View Popup--------------------------->
<div id="viewCapacity" class="web_dialog_overlay"></div>  
<div id="viewCapacityDeldialog" class="web_dialog">
   <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
      <tr>
         <td colspan="2" class="web_dialog_title normalfnt"><strong><span class="normaltxtmidb2">Location Capacity</span></strong></td>
         <td class="web_dialog_title align_right normalfntRight">
            <a href="#" id="btnCapClose">Close</a>
         </td>
      </tr>
      <tr>
      	 <td style="width:30px">&nbsp;</td>
      	<tr>
         <td colspan="2" style="padding-left: 15px;">
            <b class="normalfntBlue"><strong>Select Date Range To Find Location Capacity</strong></b>
         </td>
      </tr>
      <tr>
      	 <td style="width:12px">&nbsp;</td>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
      </tr>
      </tr>
      <tr>
       	 <td style="width:12px">&nbsp;</td>
         <td class="normalfntBlue">From Date</td>
         <td class="normalfntBlue">
         <input name="txtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="txtFromDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" />
         </td>
      </tr>
      <tr>
      	 <td style="width:12px">&nbsp;</td>
		 <td class="normalfntBlue">To Date</td>
         <td class="normalfntBlue">
         <input name="txtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="txtToDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" />
         </td>
      </tr>
      <tr>
         <td style="width:12px">&nbsp;</td>
		 <td class="normalfntBlue">&nbsp;</td>
         <td class="normalfntBlue">
          <input style="background-color:#C0C0C0" type="button" name="butGet" id="butGet" value="Process" />
         </td>
      </tr>
      <tr>
         <td style="width:12px">&nbsp;</td>
         <td class="normalfntBlue">1.) Capacity of The Location</td>
         <td class="normalfntBlue">1500</td>
      </tr>
      <tr>
         <td style="width:12px">&nbsp;</td>
         <td class="normalfntBlue">2.) Planned Capacity</td>
         <td class="normalfntBlue">1200</td>
      </tr>
      <tr>
         <td style="width:12px">&nbsp;</td>
         <td class="normalfntBlue">3.) Available Capacity (1-2)</td>
         <td class="normalfntBlue">300</td>
      </tr>
      <tr>
         <td style="width:12px">&nbsp;</td>
         <td class="normalfntBlue">4.) Allocated Capacity Not Planned</td>
         <td class="normalfntBlue">100</td>
      </tr>
         <tr>
         <td style="width:12px">&nbsp;</td>
         <td class="normalfntBlue">5.) Available Capacity (3-4)</td>
         <td class="normalfntBlue">200</td>
      </tr>
      <tr>
      	 <td style="width:12px">&nbsp;</td>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
      </tr>
      <tr>
      	 <td style="width:12px">&nbsp;</td>
         <td colspan="2" style="text-align: center;">
         <input style="background-color:#C0C0C0" type="button" name="butCapOK" id="butCapOK" value="OK" />
         </td>
      </tr>
   </table>
</div>
<!--------------------------Location Capacity View Popup--------------------------->
</form>
</body>
</html>
<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];

	$details = json_decode($_REQUEST['allocateDetails'], true);
	/////////// order allocation insert part /////////////////////
	if($requestType=='add')
	{
		if(count($details) != 0)
		{
			foreach($details as $detail)
			{
				$year 			= $detail['year'];
				$planId 		= $detail['planId'];
				$planNo 	 	= $detail['planNo'];;
				
				$saleOrder 		= trim(($detail['saleOrder']==''?'NULL':"'".$detail['saleOrder']."'"));
				$style			= trim(($detail['style']==''?'NULL':"'".$detail['style']."'"));
				$graphic		= trim(($detail['graphic']==''?'NULL':"'".$detail['graphic']."'"));
				$combo			= trim(($detail['combo']==''?'NULL':"'".$detail['combo']."'"));
				$cusId			= trim(($detail['cusId']==''?'NULL':"'".$detail['cusId']."'"));
				$process		= trim(($detail['process']==''?'NULL':"'".$detail['process']."'"));
				$psd			= trim(($detail['psd']==''?'NULL':"'".$detail['psd']."'"));
				$delDate		= trim(($detail['delDate']==''?'NULL':"'".$detail['delDate']."'"));
				$qty			= trim(($detail['qty']==''?'NULL':"'".$detail['qty']."'"));
				$location		= trim(($detail['location']==''?'NULL':"'".$detail['location']."'"));
				$printSize		= trim(($detail['printSize']==''?'NULL':"'".$detail['printSize']."'"));
				$panel			= trim(($detail['panel']==''?'NULL':"'".$detail['panel']."'"));
				
				$sampleNo		= trim($detail['sampleNo']);
				$sampleYear		= trim($detail['sampleYear']);
				$revNo			= trim($detail['revNo']);
				$printName		= trim($detail['printName']);
				$comboName		= trim($detail['comboName']);
				
				$sqlColor = " SELECT DISTINCT intColorId
								FROM trn_sampleinfomations_details
								WHERE 
								intSampleNo = '$sampleNo' AND
								intSampleYear = '$sampleYear' AND
								intRevNo = '$revNo' AND
								strPrintName = '$printName' AND
								strComboName = '$comboName'";
				
				$resultColor = $db->RunQuery($sqlColor);
				$colorCount  = mysqli_num_rows($resultColor);
				
				$sqlShots = "SELECT SUM(intNoOfShots) as noOfShots
								FROM trn_sampleinfomations_details_technical
								WHERE 
								intSampleNo = '$sampleNo' AND
								intSampleYear = '$sampleYear' AND
								intRevNo = '$revNo' AND
								strPrintName = '$printName' AND
								strComboName = '$comboName'";
				
				$resultShots = $db->RunQuery($sqlShots);
				while($rowShots = mysqli_fetch_array($resultShots))
				{
					$shots = ($rowShots['noOfShots']=='' || $rowShots['noOfShots']==0?0:$rowShots['noOfShots']);
				}
								
				$sql = "INSERT INTO plan_allocateorders 
						(intPlanId, intPlanNo, intOrderYear, 
						intSplitId, strSalesOrder, strStyle, 
						strGraphic, strCombo, intCustomerId, 
						strProcess, dtPSD, dtDeliveryDate, 
						dblQty, intLocationId, strIsAllocated, 
						strIsPlaned, intColours, intStrocks,
						intPanels, intPrintSize, intCreator, 
						dtmCreateDate
						)
						VALUES
						('$planId', '$planNo', '$year', 
						'1', $saleOrder, $style, 
						 $graphic, $combo, $cusId, 
						 $process, $psd, $delDate, 
						 $qty, $location, 'Y', 
						'N', $colorCount, $shots, 
						$panel, $printSize, '$userId', 
						now());";
				//echo $sql;
				$result = $db->RunQuery($sql);
			}
		}
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	/////////// order allocation update part /////////////////////
	else if($requestType=='edit')
	{
		$sql = "";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// order unallocation delete part /////////////////////
	else if($requestType=='delete')
	{
		if(count($details) != 0)
		{
			foreach($details as $detail)
			{
				$year 			= $detail['year'];
				$planId 		= $detail['planId'];
				$saleOrder 		= $detail['saleOrder'];
				$planNo 		= $detail['planNo'];
			
				$sql = "DELETE FROM plan_allocateorders 
						WHERE
						intPlanId = '$planId' AND 
						intPlanNo = '$planNo' AND 
						intOrderYear = '$year' AND 
						strSalesOrder = '$saleOrder' ;";
				$result = $db->RunQuery($sql);
			}
		}
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
?>
// JavaScript Document

var arrDetails 		= [];

$(document).ready(function() {
$("#frmOrderAllocation").validationEngine();

//permision for add 
  if(intAddx)
  {
 	$('#frmOrderAllocation #butNew').show();
	$('#frmOrderAllocation #butSave').show();
  }

$(".allocate").live('click',function(){
if($(this).attr("checked") == true)
{
	$(this).parent().parent().addClass("highlight");
}
else
{
	$(this).parent().parent().removeClass("highlight");
}
});

$(".unAllocate").live('click',function(){
if($(this).attr("checked") == true)
{
	$(this).parent().parent().addClass("highlight");
}
else
{
	$(this).parent().parent().removeClass("highlight");
}
});

//--------------Capacity View Popup-------------------
$("#butCapacity").click(function (e)
{
	if ($('#cboLocation').val())   
	{
		ShowCapacityView(true);
	}
	else
	{
		$('#frmOrderAllocation #butCapacity').validationEngine('showPrompt', 'Please Choose Location!','fail');
		var t=setTimeout("alertx()",3000);
	}
});
$("#btnCapClose").click(function (e)
{
 	HideCapacityView();
});
$("#butCapOK").click(function (e)
{
	HideCapacityView();
});
//--------------Capacity View Popup-------------------

/*$("#butAllocate").live('click',function(){
	if ($('#cboLocation').val())   
    { 
		allocateData();
	}
	else
	{
		$('#frmOrderAllocation #butAllocate').validationEngine('showPrompt', 'Please Choose Location Before Allocating!','fail');
		var t=setTimeout("alertx()",5000);
	}
});*/
//order allocate click event
$('#frmOrderAllocation #butAllocate').live('click',function(){
if ($('#cboLocation').val())   
{ 
	var planId = "";
	var saleOrder = "";
	var planNo = "";
	var style = "";
	var graphic = "";
	var combo = "";
	var cusId = "";
	var process = "";
	var psd	= "";
	var delDate	= "";
	var qty	= "";
	var printSize	= "";
	var panel	= "";
	var location = "";
	var sampleNo	= "";
	var sampleYear	= "";
	var revNo	= "";
	var printName = "";
	var comboName = "";
	
	process		= $("#cboProcess").val(); // Printing Process
	location	= $("#cboLocation").val(); // Planed Location
			
	value="[ ";
	$('#tblMainGrid1 .mainRow').each(function(){
		
		if ($(this).find('.allocate').attr('checked')) 
		{
			year			= $(this).attr('id');
			planId			= $(this).find(".planId").attr('id');
			saleOrder		= $(this).find(".allocate").attr('id');
			planNo			= $(this).find(".planNo").attr('id');
			style			= $(this).find(".style").html();
			graphic			= $(this).find(".graphic").html();
			combo			= $(this).find(".combo").html();
			cusId			= $(this).find(".customer").attr('id');
			psd				= $(this).find(".psd").html();
			delDate			= $(this).find(".delDate").html();
			qty				= $(this).find(".qty").html();
			printSize		= $(this).find(".printSize").attr('id');
			panel			= $(this).find(".panel").html();
			
			sampleNo		= arrDetails[planId]['sampleNo'];
			sampleYear		= arrDetails[planId]['sampleYear'];
			revNo			= arrDetails[planId]['revNo'];
			printName		= arrDetails[planId]['printName'];
			comboName		= arrDetails[planId]['comboName'];
			
			
			
		value += '{ "year":"'+year+'", "planId":"'+planId+'", "saleOrder": "'+saleOrder+'", "planNo": "'+planNo+'", "style": "'+style+'", "graphic": "'+graphic+'", "combo": "'+combo+'", "cusId": "'+cusId+'", "process": "'+process+'", "psd": "'+psd+'", "delDate": "'+delDate+'", "qty": "'+qty+'", "location": "'+location+'", "printSize": "'+printSize+'", "panel": "'+panel+'", "sampleNo": "'+sampleNo+'", "sampleYear": "'+sampleYear+'", "revNo": "'+revNo+'", "printName": "'+printName+'", "comboName": "'+comboName+'"},';
		}
	});		
	value = value.substr(0,value.length-1);
	value += " ]";
	var requestType = '';
	//alert(value);
	if ($('#frmOrderAllocation').validationEngine('validate'))   
    {
		if(value != '[ ]')
		{
			requestType = 'add';
		
			var url = "orderAllocation-db-set.php";
			var obj = $.ajax({
				url:url,
				dataType: "json",  
				data:$("#frmOrderAllocation").serialize()+'&requestType='+requestType+'&allocateDetails='+value,
				async:false,
				
				success:function(json)
				{
					$('#frmOrderAllocation #butAllocate').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmOrderAllocation').get(0).reset();
						var t=setTimeout("alertx()",1000);
						//$('#txtNo').val(json.invoiceNo);
						loadCombo_frmOrderAllocation();
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
				error:function(xhr,status){
						
						$('#frmOrderAllocation #butAllocate').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertx()",3000);
					}		
				});
			}
		}
}
else
{
	$('#frmOrderAllocation #butAllocate').validationEngine('showPrompt', 'Please Choose Location Before Allocating!','fail');
	var t=setTimeout("alertx()",3000);
}
	});

//order un allocate click event
$('#frmOrderAllocation #butUnallocate').live('click',function(){
	var planId = "";
	var planNo = "";
	var year = "";
	var salesOrder = "";

			
	value="[ ";
	$('#tblMainGrid2 .mainRow').each(function(){
		if ($(this).find('.unAllocate').attr('checked')) 
		{
			year			= $(this).attr('id');
			planId			= $(this).find(".planId").attr('id');
			saleOrder		= $(this).find(".unAllocate").attr('id');
			planNo			= $(this).find(".planNo").attr('id');
			
			value += '{ "year":"'+year+'", "planId":"'+planId+'", "saleOrder": "'+saleOrder+'", "planNo": "'+planNo+'"},';
		}
	 });
	value = value.substr(0,value.length-1);
	value += " ]";
	var requestType = '';
	//alert(value);
	if ($('#frmOrderAllocation').validationEngine('validate'))   
    {
		if(value != '[ ]')
		{
			requestType = 'delete';
		
			var url = "orderAllocation-db-set.php";
			var obj = $.ajax({
				url:url,
				dataType: "json",  
				data:$("#frmOrderAllocation").serialize()+'&requestType='+requestType+'&allocateDetails='+value,
				async:false,
				
				success:function(json)
				{
					$('#frmOrderAllocation #butUnallocate').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmPlanOrder').get(0).reset();
						var t=setTimeout("alertx()",1000);
						//$('#txtNo').val(json.invoiceNo);
						loadCombo_frmOrderAllocation();
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
				error:function(xhr,status){
						
						$('#frmPlanOrder #butUnallocate').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertx()",3000);
					}		
				});
			}
		}
	});
});
/*function allocateData()
{	
	clearRows();
	$('#tblMainGrid1 .mainRow').each(function(){
		if ($(this).find('.allocate').attr('checked')) 
		{
			var orderId			= $(this).find(".orderId").attr('id');
			var saleOrderNo		= $(this).find(".allocate").attr('id');
			var orderNo			= $(this).find(".salesOrderId").html();
			var salesOrderId	= $(this).find(".salesOrderId").attr('id');
			var style			= $(this).find(".style").html();
			var graphic			= $(this).find(".graphic").html();
			var combo			= $(this).find(".combo").html();
			var cusId			= $(this).find(".customer").attr('id');
			var cusName			= $(this).find(".customer").html();
			var psd				= $(this).find(".psd").html();
			var delDate			= $(this).find(".delDate").html();
			var qty				= $(this).find(".qty").html();
			
			var content='<tr class="normalfnt mainRow" bgcolor="#FFFFFF"><td align="center" id="'+orderId+'" class="normalfntMid orderId"><input type="checkbox" name="'+saleOrderNo+'" id="'+saleOrderNo+'" class="unAllocate" /></td>';																																										
			content +='<td  class="normalfntMid salesOrderId" id="'+salesOrderId+'">'+orderNo+'</td>';
			content +='<td class="normalfntMid style">'+style+'</td>';
			content +='<td class="normalfntMid graphic">'+graphic+'</td>';
			content +='<td class="normalfntMid combo">'+combo+'</td>';	 					
			content +='<td class="normalfntMid customer" id="'+cusId+'">'+cusName+'</td>';							
			content +='<td class="normalfntMid psd">'+psd+'</td>';
			content +='<td align="center" class="normalfntMid delDate">'+delDate+'</td>';
			content +='<td class="normalfntMid qty">'+qty+'</td>';
			content +='</tr>';
			add_new_row('#tblMainGrid2',content);
			//var rowId = $(this).find('tr').length;
			//if(rowId!=2)
			//$(this).remove();
		}
	});
}*/
/*function clearRows()
{
	var rowCount = document.getElementById('tblMainGrid2').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMainGrid2').deleteRow(1);			
	}
}*/
/*function add_new_row(table,rowcontent)
{
        if ($(table).length>0)
		{
            if ($(table+' > tbody').length==0) 
			$(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
}*/
function alertx()
{
	$('#frmOrderAllocation #butAllocate').validationEngine('hide');
	$('#frmOrderAllocation #butCapacity').validationEngine('hide');
}
function loadCombo_frmOrderAllocation()
{
	location.reload();
}
//==========================================Capacity View Popup Function=======================================
function ShowCapacityView(modal)
{
  $("#viewCapacity").show();
  $("#viewCapacityDeldialog").fadeIn(300);

  if (modal)
  {
	 $("#viewCapacity").unbind("click");
  }
  else
  {
	 $("#viewCapacity").click(function (e)
	 {
		HideCapacityView();
	 });
  }
}
function HideCapacityView()
{
  $("#viewCapacity").hide();
  $("#viewCapacityDeldialog").fadeOut(300);
}
//==========================================Capacity View Popup Function========================================
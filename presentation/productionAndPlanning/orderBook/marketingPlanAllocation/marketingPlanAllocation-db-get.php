<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	
	include "{$backwardseperator}dataAccess/Connector.php";
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	/////////// pre plan load part /////////////////////
	if($requestType=='loadCombo')
	{
		$prePlanNo 	= $_REQUEST['prePlanRefNo'];
		$sql = "SELECT DISTINCT intCustomerId
				FROM plan_pre_plan
				WHERE intPrePlanRefNo = '".$prePlanNo."' ; 
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$html = $row['intCustomerId'];
		}
		echo $html;
	}
	if($requestType=='loadAllComboes')
	{
		$slected 	= $_REQUEST['slected'];
		$sampYear 	= $_REQUEST['sampYear'];
		$sampNo 	= $_REQUEST['sampNo'];
		$revNo 		= $_REQUEST['revNo'];
		$combo 		= $_REQUEST['combo'];
		$saveValue  = $_REQUEST['saveValue'];
		$graphicNo  = $_REQUEST['graphicNo'];
		
		$html=loadAllComboes($slected,$sampYear,$sampNo,$revNo,$combo,$saveValue,$graphicNo);
		if(!$html[1])
		{
			$revHtml = loadRevCombo($html[2]);
		}
		$response['combo']= $html[0];
		$response['check']= $html[1];
		$response['revCombo']= $revHtml;
		$arrCombo[] = $response;
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);
		
	}
	if($requestType=='loadComboVal')
	{
		$sql = "SELECT DISTINCT
				intPrePlanRefNo
				FROM
				plan_pre_plan
				WHERE
				intEnterLocationId = '$companyId'
				ORDER BY intPrePlanRefNo DESC
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intPrePlanRefNo']."\">".$row['intPrePlanRefNo']."</option>";
		}
		echo $html;
	}
	if($requestType=='loadData')
	{
		$prePlanNo 	= $_REQUEST['prePlanRefNo'];
		//$revisionNo = $_REQUEST['revisionNo'];
		
		//-----------------
		$sqlY = "SELECT DISTINCT
				trn_sampleinfomations.intSampleYear
				FROM trn_sampleinfomations
				ORDER BY
				trn_sampleinfomations.intSampleYear DESC
				";
//		$html = "<option value=\"\"></option>";
/*		while($rowY=mysqli_fetch_array($resultY))
		{
			$html .= "<option value=\"".$row['intSampleYear']."\">".$row['intSampleYear']."</option>";
		}
*/
        if(isset($html)) {
            $comboSampYears = $html;
        }
		//-----------------

		$sqlS = "SELECT intId,strName FROM mst_printsizes WHERE intStatus=1 ORDER BY strName ";

		$sql = "SELECT 	
					p.intId, 
					p.intPrePlanRefNo, 
					p.dtmUploadDate, 
					p.intCustomerId, 
					p.strSeason, 
					p.strInquiry, 
					p.strItem, 
					p.strStyleNo, 
					p.strGraphic, 
					p.strSalesOrder, 
					p.strColor, 
					p.strVaProcess, 
					p.strPartOfPrint, 
					p.strOrdQty, 
					p.dtmPsd, 
					p.dtmPfd, 
					p.dtmFirstDeliveryDate, 
					p.dtmLastDeliveryDate, 
					p.strOperationSend, 
					p.strOperationRcv, 
					p.intEnterLocationId, 
					p.intSampleYear, 
					p.intSampleNo, 
					p.intRevisionNo, 
					p.strCombo, 
					p.strPrintName,
					c.strName,
					p.intPrintSize,
					p.intPanels 
					FROM 
					plan_pre_plan p
					INNER JOIN mst_customer c ON c.intId=p.intCustomerId
					WHERE p.intPrePlanRefNo = '".$prePlanNo."' AND p.intEnterLocationId='".$companyId."' 
					ORDER BY p.intId;";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$checkDisable = false;
			$sql1    = "SELECT intPlanId FROM plan_allocateorders WHERE intPlanId='".$row['intId']."' ";
			$result1 = $db->RunQuery($sql1);
			$count   = mysqli_num_rows($result1);
			if($count>0)
				$checkDisable = true;
				
			$prePlanId		= $row['intId'];
			$prePlanNo		= $row['intPrePlanRefNo'];
			$uploadDate 	= $row['dtmUploadDate'];
			$customerId 	= $row['intCustomerId'];
			$cusName	 	= $row['strName'];
			$season		 	= $row['strSeason'];
			$inquiry	 	= $row['strInquiry'];
			$item		 	= $row['strItem'];
			$styleNo	 	= $row['strStyleNo'];
			$graphic	 	= $row['strGraphic'];
			$salesOrder	 	= $row['strSalesOrder'];
			$color		 	= $row['strColor'];
			$vaProcess	 	= $row['strVaProcess'];
			$partOfPrint 	= $row['strPartOfPrint'];
			$ordQty		 	= $row['strOrdQty'];
			$psd		 	= $row['dtmPsd'];
			$pfd		 	= $row['dtmPfd'];
			$fDelDate	 	= $row['dtmFirstDeliveryDate'];
			$lDelDate	 	= $row['dtmLastDeliveryDate'];
			$operationSnd	= $row['strOperationSend'];
			$operationRcv	= $row['strOperationRcv'];
			
			$sampYear	 	= $row['intSampleYear'];
			$sampNo	 		= $row['intSampleNo'];
			$combo	 		= $row['strCombo'];
			$printName	 	= $row['strPrintName'];
			$revNo	 		= $row['intRevisionNo'];
			$printSize	 	= $row['intPrintSize'];
			$panel		 	= $row['intPanels'];
			
			$dayQty	 = "";
			$dayHead = "";
			$sqlDayQty = "SELECT
							pd.strDate,
							pd.dblRcvQty
							FROM
							plan_pre_plan pp
							Inner Join plan_pre_plan_day_qty pd ON pp.intId = pd.intPrePlanId
							WHERE
							pp.intId =  '$prePlanId'";
			$resultDayQty = $db->RunQuery($sqlDayQty);
			while($rowDayQty=mysqli_fetch_array($resultDayQty))
			{
				$qty		= $rowDayQty['dblRcvQty'];
				$strDate	= $rowDayQty['strDate'];
				$dayQty  .= "<td bgcolor=#F0F8FF>$qty</td>";
				$dayHead .= "<td bgcolor=#FFFFCC><strong>$strDate</strong></td>";
			}
							    $d = date('Y');
			echo "
				<tbody class=normalfnt bgcolor=#FFFFFF>
				<tr bgcolor=#FFFFFF class='normalfntBlue' id='$prePlanId'>
				<td bgcolor=#FFFFF3 width=15 id='$prePlanId'><img align=\"center\" class=\"removeColumn mouseover\" src=\"images/del.png\" width='15' height='15' ".($checkDisable==true?'style="display:none;"':'')." /></td>
				<td bgcolor=#FFFFF3 width=100 id='$prePlanId' class=prePlanId>&nbsp;$prePlanNo&nbsp;</td>
				<td bgcolor=#FEE0C6 width=150>&nbsp;$uploadDate&nbsp;</td>
				<td bgcolor=#F0F8FF id='$customerId' class=customer>&nbsp;$cusName&nbsp;</td>
				<td bgcolor=#F0F8FF>&nbsp;$season&nbsp;</td>
				<td bgcolor=#F0F8FF>&nbsp;$inquiry&nbsp;</td>
				<td bgcolor=#F0F8FF>&nbsp;$item&nbsp;</td>
				<td bgcolor=#F0F8FF class='style'>&nbsp;$styleNo&nbsp;</td>
				<td bgcolor=#F0F8FF class='graphic'>&nbsp;$graphic&nbsp;</td>
				
				<td bgcolor=#F0F8FF>
				<select name=cboYear id=cboYear style=width:100% class='year' ".($checkDisable==true?'disabled=\"disabled\"':'').">
					<option value=\"\"></option>";
				$resultY = $db->RunQuery($sqlY);
				while($rowY=mysqli_fetch_array($resultY))
				{
					if($rowY['intSampleYear']==$sampYear)
						echo  "<option value=\"".$rowY['intSampleYear']."\" selected=\"selected\">".$rowY['intSampleYear']."</option>";
					else if($rowY['intSampleYear']==date('Y'))
						echo  "<option value=\"".$rowY['intSampleYear']."\" selected=\"selected\">".$rowY['intSampleYear']."</option>";
					else
						echo  "<option value=\"".$rowY['intSampleYear']."\">".$rowY['intSampleYear']."</option>";
				}
				
                echo "</select></td>
				
				<td bgcolor=#F0F8FF>
				<select name=txtSampleNo id=txtSampleNo style=width:100% class='sampNo' ".($checkDisable==true?'disabled=\"disabled\"':'').">";
				if($sampNo!='')
				{
					$sampCombo = loadAllComboes('year',$sampYear,'','','',$sampNo,'');
					echo $sampCombo[0];	
				}
				else if($sampYear!='')
				{
					$sampCombo = loadAllComboes('year',$sampYear,'','','',$sampNo,'');
					echo $sampCombo[0];
				}
				else
				{
					$sampCombo = loadAllComboes('year',date('Y'),'','','',$sampNo,$graphic);
					$smpNo     = $sampCombo[2];
					echo $sampCombo[0];
				}
                echo "</select>
				</td>
				
				<td bgcolor=#F0F8FF>
				<select name=txtSampleNoRevision id=txtSampleNoRevision style=width:100% class='revNo' ".($checkDisable==true?'disabled=\"disabled\"':'').">";
				if($revNo!='')
				{
					$RevCombo = loadAllComboes('sampNo',$sampYear,$sampNo,'','',$revNo,'');
					echo $RevCombo[0];
				}
				else if($sampNo!='')
				{
					$RevCombo = loadAllComboes('sampNo',$sampYear,$sampNo,'','',$revNo,'');
					echo $RevCombo[0];
				}
				else
				{
					$RevCombo = loadAllComboes('sampNo',date('Y'),$smpNo,'','',$revNo,'');
					echo $RevCombo[0];
				}
                echo "</select>
				</td>
				
				<td bgcolor=#F0F8FF>
				<select name=txtCombo id=txtCombo style=width:100% class='combo' ".($checkDisable==true?'disabled=\"disabled\"':'').">";
				if($combo!='')
				{
					$Ccombo = loadAllComboes('revNo',$sampYear,$sampNo,$revNo,'',$combo,'');
					echo $Ccombo[0];
				}
				else if($revNo!='')
				{
					$Ccombo = loadAllComboes('revNo',$sampYear,$sampNo,$revNo,'',$combo,'');
					echo $Ccombo[0];
				}
				
                echo "</select>
				</td>
				
				<td bgcolor=#F0F8FF>
				<select name=txtPrintName id=txtPrintName style=width:100% class='printName' ".($checkDisable==true?'disabled=\"disabled\"':'').">";
				if($printName!='')
				{
					$printCombo = loadAllComboes('combo',$sampYear,$sampNo,$revNo,$combo,$printName,'');
					echo $printCombo[0];
				}
				else if($combo!='')
				{
					$printCombo = loadAllComboes('combo',$sampYear,$sampNo,$revNo,$combo,$printName,'');
					echo $printCombo[0];
				}
                echo "</select>
				</td>
            
				<td bgcolor=#F0F8FF>
				<select name=cboSize id=cboSize style=width:100% class='size' ".($checkDisable==true?'disabled=\"disabled\"':'').">
					<option value=\"\"></option>";
				$resultS = $db->RunQuery($sqlS);
				while($rowS=mysqli_fetch_array($resultS))
				{
					if($rowS['intId']==$printSize)
						echo  "<option value=\"".$rowS['intId']."\" selected=\"selected\">".$rowS['strName']."</option>";
					else
						echo  "<option value=\"".$rowS['intId']."\">".$rowS['strName']."</option>";
				}
				echo "</select>
				</td>	
					
				<td bgcolor=#F0F8FF>
				<input type=\"text\" name=\"txtPanel\" id=\"txtPanel\" class=\"panel\" ".($checkDisable==true?'disabled=\"disabled\"':'')." value=\"".($panel==''?'':$panel)."\" style=\"width:80px\"></td>
				
				<td bgcolor=#F0F8FF class='salesOrderNo'>&nbsp;$salesOrder&nbsp;</td>
				<td bgcolor=#F0F8FF>&nbsp;$color&nbsp;</td>
				<td bgcolor=#F0F8FF>&nbsp;$vaProcess&nbsp;</td>
				<td bgcolor=#F0F8FF>&nbsp;$partOfPrint&nbsp;</td>
				<td bgcolor=#F0F8FF>&nbsp;$ordQty&nbsp;</td>
				<td bgcolor=#F0F8FF>&nbsp;$psd&nbsp;</td>
				<td bgcolor=#F0F8FF>&nbsp;$pfd&nbsp;</td>
				<td bgcolor=#F0F8FF>&nbsp;$fDelDate&nbsp;</td>
				<td bgcolor=#F0F8FF>&nbsp;$lDelDate&nbsp;</td>
				</tr>
				</tbody>
				";
		}
		echo " <thead>
		<tr class=normalfnt id=dayHead>
		<td bgcolor=#FAD163 width=15><strong></strong></td>
		<td bgcolor=#FAD163 width=100><strong>Plan No</strong></td>
		<td bgcolor=#FAD163 width=150><strong>Uploaded Date</strong></td>
		<td bgcolor=#FAD163><strong>Customer Name</strong></td>
		<td bgcolor=#FAD163><strong>Season</strong></td>
		<td bgcolor=#FAD163><strong>Inquiry Identification</strong></td>
		<td bgcolor=#FAD163><strong>Item</strong></td>
		<td bgcolor=#FAD163><strong>Style No</strong></td>
		<td bgcolor=#FAD163><strong>Graphic</strong></td>
		<td bgcolor=#FAD163><strong>Sample Year</strong></td>
		<td bgcolor=#FAD163><strong>Sample Number</strong></td>
		<td bgcolor=#FAD163 width='130'><strong>Revision Number</strong></td>
		<td bgcolor=#FAD163 width=100><strong>Combo</strong></td>
		<td bgcolor=#FAD163><strong>Print Name</strong></td>
		<td bgcolor=#FAD163><strong>Print Size</strong></td>
		<td bgcolor=#FAD163><strong>Panel</strong></td>
		<td bgcolor=#FAD163><strong>Sales Order</strong></td>
		<td bgcolor=#FAD163><strong>Color</strong></td>
		<td bgcolor=#FAD163><strong>VA Process</strong></td>
		<td bgcolor=#FAD163><strong>Part of the Print</strong></td>
		<td bgcolor=#FAD163><strong>Order Qty</strong></td>
		<td bgcolor=#FAD163><strong>PSD</strong></td>
		<td bgcolor=#FAD163><strong>PFD</strong></td>
		<td bgcolor=#FAD163><strong>First Delivery Date</strong></td>
		<td bgcolor=#FAD163><strong>Last Delivery Date</strong></td>
	  </tr>
	 </thead>";
	}
	
	
	
	function loadAllComboes($slected,$sampYear,$sampNo,$revNo,$combo,$saveValue,$graphicNo){
		
	global $db;
		$sql = "SELECT DISTINCT ";
		if($slected=='year')
		$sql .= "sd.intSampleNo as value,s.strGraphicRefNo ";
		if($slected=='sampNo')
		$sql .= "sd.intRevNo as value ";
		if($slected=='revNo')
		$sql .= "sd.strComboName as value ";
		if($slected=='combo')
		$sql .= "sd.strPrintName as value ";
	
		$sql .= "FROM trn_sampleinfomations_details sd
				 INNER JOIN trn_sampleinfomations s ON s.intSampleNo=sd.intSampleNo  
				 where sd.intCompanyId = '".$_SESSION["CompanyID"]."' ";
		
		if($sampYear!='')
		$sql .= " AND sd.intSampleYear =  '$sampYear' ";
		if($sampNo!='')
		$sql .= " AND sd.intSampleNo =  '$sampNo' ";
		if($revNo!='')
		$sql .= " AND sd.intRevNo =  '$revNo' ";
		if($combo!='')
		$sql .= " AND sd.strComboName =  '$combo' ";
			
		if($slected=='year')
		$sql .= "ORDER BY sd.intSampleNo DESC";
		if($slected=='sampNo')
		$sql .= "ORDER BY sd.intRevNo DESC";
		if($slected=='revNo')
		$sql .= "ORDER BY sd.strComboName DESC";
		if($slected=='combo')
		$sql .= "ORDER BY sd.strPrintName DESC";
			
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		$check = true;
		while($row=mysqli_fetch_array($result))
		{
			if($saveValue==$row['value'])
			$html .= "<option value=\"".$row['value']."\" selected=\"selected\">".$row['value']."</option>";
			else if($graphicNo!="" && $check)
			{
				if($graphicNo==$row['strGraphicRefNo'])
				{
					$html .= "<option value=\"".$row['value']."\" selected=\"selected\">".$row['value']."</option>";
					$sampNo =  $row['value'];
					$check = false;		
				}
				else
					$html .= "<option value=\"".$row['value']."\">".$row['value']."</option>";
			}
			else
				$html .= "<option value=\"".$row['value']."\">".$row['value']."</option>";
		}
		$data = array($html,$check,$sampNo);
		return $data;	
	}
	function loadRevCombo($sampleNo)
	{
		global $db;
		
		$sql = "SELECT DISTINCT
				sd.intRevNo as value
				FROM trn_sampleinfomations_details sd
				INNER JOIN trn_sampleinfomations s ON s.intSampleNo=sd.intSampleNo 
				where sd.intCompanyId = '".$_SESSION["CompanyID"]."' AND
				sd.intSampleNo =  '$sampleNo'
				ORDER BY sd.intRevNo DESC
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['value']."\">".$row['value']."</option>";
		}
		return $html;
	}
?>
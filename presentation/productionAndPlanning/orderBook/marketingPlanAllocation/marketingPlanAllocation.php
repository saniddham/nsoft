<?php
ini_set('display_errors',0);
$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	$locationId = $_SESSION["CompanyID"];
//===========================================================================
	$prePlanNo = (!isset($_REQUEST['cboSearch'])?'':$_REQUEST['cboSearch']);
//===========================================================================
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Plan - Sample Details Allocation</title>
<?php include 'include/javascript.html'?>
<script type="text/javascript" src="presentation/productionAndPlanning/orderBook/marketingPlanAllocation/marketingPlanAllocation-js.js"></script>

<style type="text/css">
.web_dialog_overlay
{
   position: fixed;
   top: 0;
   right: 0;
   bottom: 0;
   left: 0;
   height: 100%;
   width: 100%;
   margin: 0;
   padding: 0;
   background: #000000;
   opacity: .15;
   filter: alpha(opacity=15);
   -moz-opacity: .15;
   z-index: 0;
   display: none;
}
.web_dialog
{
   display: none;
   position: fixed;
   width: 450px;
   height: 350px;
   top: 50%;
   left: 50%;
   margin-left: -190px;
   margin-top: -100px;
   background-color: #F0F0F0;
   border: 2px solid #336699;
   padding: 0px;
   z-index: 0;
   font-family: Verdana;
   font-size: 10pt;
}
.web_dialog_title
{
   border-bottom: solid 2px #336699;
   background-color: #336699;
   padding: 4px;
   color: White;
   font-weight:bold;
}
.web_dialog_title a
{
   color: White;
   text-decoration: none;
}
.align_right
{
   text-align: right;
}
</style>

</head>

<body onload="loadCombo_frmOrderAllocation();">
<form id="frmOrderAllocation" name="frmOrderAllocation" enctype="multipart/form-data" action="customerPlanAllocation.php" method="post" autocomplete="off">
<input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
<div align="center">
<div class="trans_layoutXL">
<div class="trans_text">Customer Plan - Sample Details Allocation</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td colspan="2" bgcolor="#0033FF"><strong><span class="normaltxtmidb2">Sample Details Allocation</span></strong></td>
</tr>
<tr>
  <td colspan="2">
  <table width="100%">
  <tr>
    <td align="center">
    <table>
    <tr>
      <td class="normalfntMid">Allocation Number: </td>
    <td class="normalfntMid">
    <select name="cboSearch" id="cboSearch" style="width:200px" >
    <option value=""></option>
    <?php   $sql = "SELECT DISTINCT
					intPrePlanRefNo
					FROM
					plan_pre_plan
					WHERE
					intEnterLocationId =  '$companyId'
					ORDER BY intPrePlanRefNo DESC
                    ";
                $result = $db->RunQuery($sql);
                while($row=mysqli_fetch_array($result))
                {
					if($row['intPrePlanRefNo']==$prePlanNo)
					echo "<option value=\"".$row['intPrePlanRefNo']."\" selected=\"selected\">".$row['intPrePlanRefNo']."</option>";	
					else
					echo "<option value=\"".$row['intPrePlanRefNo']."\">".$row['intPrePlanRefNo']."</option>";
                }
  ?>
  </select>
    </td>
    <td class="normalfntMid">Customer: </td>
     <td class="normalfntMid">
     <select name="cboCustomer" id="cboCustomer"  style="width:200px" disabled="disabled" >
    <option value=""></option>
    <?php   $sql = "SELECT 	intId, strName 
					FROM mst_customer 
					WHERE intStatus='1' 
					ORDER BY strName;";
                $result = $db->RunQuery($sql);
                while($row=mysqli_fetch_array($result))
                {
					if($row['intId']==$customerId)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
					else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
                }
  ?>
  </select>
     </td>
    </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td align="center">

    </td>
  </tr>
    </table></td>
</tr>
<tr>
  <td colspan="2" align="center">
  <table width="100%">
    <tr>
      <td align="center">
        <div style="overflow:scroll;width:1270px;height:350px;" id="divGrid">
        <table width="200%" id="tblMainGrid2" class="grid" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
<?php
			//--------------------------------------------------------------------------------------------
				function getNextPrePlanNo($companyId,$locationId)
				{
					global $db;
					$sql = "SELECT
							intPrePlanNo
							FROM sys_plan_no
							WHERE
							intCompanyId = '$companyId' AND intLocationId = '$locationId'
							";	
					$result = $db->RunQuery($sql);
					$row = mysqli_fetch_array($result);
					$nextPrePlanNo = $row['intPrePlanNo'];
					
					$sql = "UPDATE `sys_plan_no` SET intPrePlanNo=intPrePlanNo+1 WHERE (intCompanyId = '$companyId' AND intLocationId = '$locationId')";
					$db->RunQuery($sql);
					return $nextPrePlanNo;
				}
			//--------------------------------------------------------------------------------------------
			//--------------------------------------------------------------------------------------------
				function getLatestAccPeriod($companyId)
				{
					global $db;
					$sql = "SELECT
							MAX(mst_financeaccountingperiod.intId) AS accId,
							mst_financeaccountingperiod.dtmStartingDate,
							mst_financeaccountingperiod.dtmClosingDate,
							mst_financeaccountingperiod.intStatus,
							mst_financeaccountingperiod_companies.intCompanyId,
							mst_financeaccountingperiod_companies.intPeriodId
							FROM
							mst_financeaccountingperiod
							Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
							WHERE
							mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
							ORDER BY
							mst_financeaccountingperiod.intId DESC
							";	
					$result = $db->RunQuery($sql);
					$row = mysqli_fetch_array($result);
					$latestAccPeriodId = $row['accId'];	
					return $latestAccPeriodId;
				}
			//--------------------------------------------------------------------------------------------
			//============================================================================================
				function encodePrePlanNo($prePlanNumber,$accountPeriod,$companyId,$locationId)
				{
					global $db;
					$sql = "SELECT
							mst_financeaccountingperiod.intId,
							mst_financeaccountingperiod.dtmStartingDate,
							mst_financeaccountingperiod.dtmClosingDate,
							mst_financeaccountingperiod.intStatus
							FROM
							mst_financeaccountingperiod
							WHERE
							mst_financeaccountingperiod.intId =  '$accountPeriod'
							";	
					$result = $db->RunQuery($sql);
					$row = mysqli_fetch_array($result);
					$startDate = substr($row['dtmStartingDate'],0,4);
					$closeDate = substr($row['dtmClosingDate'],0,4);
					$sql = "SELECT
							mst_companies.strCode AS company,
							mst_companies.intId,
							mst_locations.intCompanyId,
							mst_locations.strCode AS location,
							mst_locations.intId
							FROM
							mst_companies
							Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
							WHERE
							mst_locations.intId =  '$locationId' AND
							mst_companies.intId =  '$companyId'
							";
					$result = $db->RunQuery($sql);
					$row = mysqli_fetch_array($result);
					$companyCode = $row['company'];
					$locationCode = $row['location'];
					$prePlanFormat = $companyCode."/".$locationCode."/".$startDate."-".$closeDate."/".$prePlanNumber;
					return $prePlanFormat;
			//============================================================================================
			}
            ?>     
        </table>
        <hr / width="190%">
        <table id="tblLoadData" width="190%" class="grid" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
			<!--this area is excel sheet uploaded data preview-->
        </table>
        </div>    
      </td>
     </tr>
    </table>
    </td>
</tr>
<tr>
<td width="100%" align="center" bgcolor=""><img <?php echo $form_permision['add']?'':'style="display:none"'?> border="0" src="images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
</tr>
</table>
</div>
</div>
</form>
</body>
</html>
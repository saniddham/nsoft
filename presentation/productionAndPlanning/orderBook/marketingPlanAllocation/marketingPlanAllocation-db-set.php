<?php 
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////

	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$prePlanRefNo 	 = $_REQUEST['prePlanRefNo'];
	
	$sampYear 		 = $_REQUEST['sampYear'];
	$sampNo  = $_REQUEST['sampNo'];
	$combo 	 = $_REQUEST['combo'];
	$printName 		 = $_REQUEST['printName'];
	$revisionNo 		 = $_REQUEST['revisionNo'];
	
	
	$prePlanId 		 = $_REQUEST['prePlanId'];
	
	$arr 		= json_decode($_REQUEST['arr'], true);
	
	
//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
			foreach($arr as $arrVal)
			{			
				$sampYear 		= ($arrVal['sampYear']==""?'null':$arrVal['sampYear']);
				$sampNo  		= ($arrVal['sampNo']==""?'null':$arrVal['sampNo']);
				$combo 	 		= ($arrVal['combo']=="null"?"":$arrVal['combo']);
				$printName 		= ($arrVal['printName']=="null"?"":$arrVal['printName']);
				$revisionNo 	= ($arrVal['revisionNo']==""?'null':$arrVal['revisionNo']);
				$prePlanId 		= $arrVal['prePlanId'];
				$printSize 		= ($arrVal['printSize']==""?'null':$arrVal['printSize']);
				$panel 			= ($arrVal['panel']=="" || $arrVal['panel']==0?1:$arrVal['panel']);
					
		   	$sql = "UPDATE `plan_pre_plan` SET 
					intSampleYear =$sampYear, 
					intSampleNo =$sampNo, 
					strCombo ='$combo', 
					strPrintName ='$printName', 
					intRevisionNo =$revisionNo,
					intPrintSize = $printSize , 
					intPanels = $panel  
					WHERE plan_pre_plan.intId = $prePlanId ";

			$result = $db->RunQuery2($sql);
			}
		
		$toSave=$saved;
		
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($savableFlag==1) &&(!$result)){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Header saving error";
			$response['q'] 			= $sql;
		}
		else if(($savableFlag==1) &&($toSave==0) &&($saved==0)){
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Details saving error";
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
//----------------------------------------
	echo json_encode($response);
//-----------------------------------------	
	if($requestType == 'deletePlan')
	{
		$planId = $_REQUEST['planId'];
		
		$sql    = "DELETE FROM plan_pre_plan WHERE intId = '$planId' ;";
		$result = $db->RunQuery($sql);
		if($result)
		{
			$sqlD = "DELETE FROM plan_pre_plan_day_qty WHERE intPrePlanId = '$planId' ";
			$resultD = $db->RunQuery($sqlD);
		}
	}
	
	function getNextGatepassNo()
	{
		global $db;
		global $companyId;
		$sql = "SELECT
				sys_no.intGatePassNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$companyId'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextGatePassNo = $row['intGatePassNo'];
		
		$sql = "UPDATE `sys_no` SET intGatePassNo=intGatePassNo+1 WHERE (`intCompanyId`='$companyId')  ";
		$db->RunQuery2($sql);
		
		return $nextGatePassNo;
	}
	//-----------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$salesOrderId,$item)
	{
		global $db;
		 $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getItemName($itemId){
		global $db;
		   $sql = "SELECT
					mst_item.strName
					FROM mst_item
					WHERE
					mst_item.intId =  '$itemId'";

		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return $row['strName'];	
		}
	//--------------------------------------------------------------
	function getSaveStatus($serialNo,$year)
	{
		global $db;
		$sql = "SELECT ware_gatepassheader.intStatus, ware_gatepassheader.intApproveLevels FROM ware_gatepassheader WHERE (intIssueNo='$serialNo') AND (`intIssueYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			
		return $editableFlag;
	}
//------------------------------------------------------------
function get_gpQty($location,$orderNo,$orderYear,$styleNo,$itemId){
	global $db;
	 $sql1 = "SELECT
				Sum(ware_gatepassdetails.dblQty) as gpQty
				FROM
				ware_gatepassheader
				Inner Join ware_gatepassdetails ON ware_gatepassheader.intGatePassNo = ware_gatepassdetails.intGatePassNo AND ware_gatepassheader.intGatePassYear = ware_gatepassdetails.intGatePassYear
				WHERE
				ware_gatepassdetails.strOrderNo =  '$orderNo' AND
				ware_gatepassdetails.intOrderYear =  '$orderYear' AND
				ware_gatepassdetails.strStyleNo =  '$styleNo' AND
				ware_gatepassdetails.intItemId =  '$itemId' AND
				ware_gatepassheader.intStatus >  '1' AND
				ware_gatepassheader.intStatus <= ware_gatepassheader.intApproveLevels AND
				ware_gatepassheader.intCompanyId =  '$location'";	
	$result1 = $db->RunQuery2($sql1);
	$row1=mysqli_fetch_array($result1);
	$gpQty=$row1['gpQty'];
	return $gpQty;
}
//------------------------------------------------------------
?>

<?php
ob_start();
//mysqli_select_db($database, $makeconnection);
//$sql_get_players="
//SELECT * 
//FROM table 
//ORDER BY id ASC";
//
//$get_players = mysqli_query($sql_get_players, $makeconnection) or die(mysqli_error());
//$row_get_players = mysqli_fetch_assoc($get_players);
//
$message = null;
$allowed_extensions = array('csv');
$upload_path = '.'; //same directory

if (!empty($_FILES['file'])) 
{

	if ($_FILES['file']['error'] == 0) 
	{
		// check extension
		$file = explode(".", $_FILES['file']['name']);
		$extension = array_pop($file);
		if (in_array($extension, $allowed_extensions)) 
		{	
			if (move_uploaded_file($_FILES['file']['tmp_name'], $upload_path.'/'.$_FILES['file']['name'])) 
			{	
					if (($handle = fopen($upload_path.'/'.$_FILES['file']['name'], "r")) !== false) 
					{
						$keys = array();
						$out = array();
						$insert = array();
						$line = 1;
						
						while (($row = fgetcsv($handle, 0, ',', '"')) !== FALSE) {
						
						foreach($row as $key => $value) 
						{    		
							if ($line === 1) 
							{
								$keys[$key] = $value;       
							} 
							else 
							{       
								$out[$line][$key] = $value;              
							}       
							}                
							$line++;          
						}        
						fclose($handle);            
						if (!empty($keys) && !empty($out)) 
						{        
							$db = new PDO('mysql:host=name;dbname=database', 'user', 'pw');   
							$db->exec("SET CHARACTER SET utf8");        
							foreach($out as $key => $value) {        
							$sql  = "INSERT INTO `table` (`";    $sql .= implode("`id`", $keys);    
							$sql .= "`) VALUES (";    
							$sql .= implode(", ", array_fill(0, count($keys), "?"));    
							$sql .= ")";    
							$statement = $db->prepare($sql);    
							$statement->execute($value);       
						}      
						$message = '<span class="green">File has been uploaded successfully</span>';      
					}
				}
			}
		} 
		else 
		{
			$message = '<span class="red">Only .csv file format is allowed</span>';
		}
	} 
	else 
	{
		$message = '<span class="red">There was a problem with your file</span>';
	}
}
ob_flush();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>CSV File Upload</title>
</head>
<body>
<h2>Search & Upload file</h2>
<section id="wrapper">
  <form action="" method="post" enctype="multipart/form-data">
    <table cellpadding="0" cellspacing="0" border="0" class="table">
      <tr>
        <th><label for="file">Select file</label>
          <?php echo $message; ?></th>
      </tr>
      <tr>
        <td><input type="file" name="file" id="file" size="30" /></td>
      </tr>
      <tr>
        <td><input type="submit" id="btn" class="fl_l" value="Submit" /></td>
      </tr>
    </table>
  </form>
</section>
<br/>
<h2>Results:</h2>
<?php do { ?>
<p><?php echo $row_get_players['id'];?></p>
<?php } while ($row_get_players = mysqli_fetch_assoc($get_players)); ?>
</body>
</html>

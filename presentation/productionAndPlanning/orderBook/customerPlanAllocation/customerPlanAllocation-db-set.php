<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	 = $_SESSION['mainPath'];
	$userId 	 = $_SESSION['userId'];
	$companyId 	 = $_SESSION['CompanyID'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response    = array('type'=>'', 'msg'=>'');
	
	$requestType = $_REQUEST['requestType'];
	$customer    = $_REQUEST['customerId'];
	$prePlanNo   = $_REQUEST['prePlanRefNo'];
	$arrData 	 = json_decode($_REQUEST['arr'], true);
	
	if($requestType=="save")
	{
		foreach ($arrData as $arr)
		{
			
			$sampYear 		= ($arr['sampYear']==""?'null':$arr['sampYear']);
			$sampNo  		= ($arr['sampNo']==""?'null':$arr['sampNo']);
			$combo 	 		= ($arr['combo']=="null"?"":$arr['combo']);
			$printName 		= ($arr['printName']=="null"?"":$arr['printName']);
			$revisionNo 	= ($arr['revisionNo']==""?'null':$arr['revisionNo']);
			$prePlanId 		= $arr['prePlanId'];
			$printSize 		= ($arr['printSize']==""?'null':$arr['printSize']);
			$panel 			= ($arr['panel']=="" || $arr['panel']==0?1:$arr['panel']);
			
			$sql = "UPDATE plan_pre_plan 
					SET 
					intCustomerId = $customer , 
					intSampleYear = $sampYear , 
					intSampleNo = $sampNo , 
					intRevisionNo = $revisionNo , 
					strCombo = '$combo' , 
					strPrintName = '$printName',
					intPrintSize = $printSize , 
					intPanels = $panel
					WHERE
					intId = '$prePlanId' AND intPrePlanRefNo= '$prePlanNo' ;";
			
			$result = $db->RunQuery($sql);
			
			if($result)
			{
				$response['msg']  = "Saved Successfully";
				$response['type'] = "pass";
			}
			else
			{
				$response['type'] = 'fail';
				$response['msg']  = $sql;
			}
		}
		
	 echo json_encode($response);
	}
	
	if($requestType == 'deletePlan')
	{
		$planId = $_REQUEST['planId'];
		
		$sql    = "DELETE FROM plan_pre_plan WHERE intId = '$planId' ;";
		$result = $db->RunQuery($sql);
		if($result)
		{
			$sqlD = "DELETE FROM plan_pre_plan_day_qty WHERE intPrePlanId = '$planId' ";
			$resultD = $db->RunQuery($sqlD);
		}
	}

?>

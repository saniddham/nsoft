<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$userId 	= $_SESSION['userId'];
$thisFilePath =  $_SERVER['PHP_SELF'];
$insUpdtStatus    = "";
$prePlanReference = "";
$updateHStatus = false;
include  "{$backwardseperator}dataAccess/permisionCheck.inc";
$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	$locationId = $_SESSION["CompanyID"];
//===========================================================================
	$prePlanNo  = $_REQUEST['cboSearch'];
	$customerId = $_REQUEST['cboCustomer'];
//===========================================================================
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Plan Allocation</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../masterData/css/planning.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="customerPlanAllocation-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">

<style type="text/css">
.web_dialog_overlay
{
   position: fixed;
   top: 0;
   right: 0;
   bottom: 0;
   left: 0;
   height: 100%;
   width: 100%;
   margin: 0;
   padding: 0;
   background: #000000;
   opacity: .15;
   filter: alpha(opacity=15);
   -moz-opacity: .15;
   z-index: 0;
   display: none;
}
.web_dialog
{
   display: none;
   position: fixed;
   width: 450px;
   height: 350px;
   top: 50%;
   left: 50%;
   margin-left: -190px;
   margin-top: -100px;
   background-color: #F0F0F0;
   border: 2px solid #336699;
   padding: 0px;
   z-index: 0;
   font-family: Verdana;
   font-size: 10pt;
}
.web_dialog_title
{
   border-bottom: solid 2px #336699;
   background-color: #336699;
   padding: 4px;
   color: White;
   font-weight:bold;
}
.web_dialog_title a
{
   color: White;
   text-decoration: none;
}
.align_right
{
   text-align: right;
}
</style>

</head>

<body onload="loadCombo_frmOrderAllocation();">
<form id="frmOrderAllocation" name="frmOrderAllocation" enctype="multipart/form-data" action="customerPlanAllocation.php" method="post" autocomplete="off">
<input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div align="center">
<div class="trans_layoutXL">
<div class="trans_text">Customer Plan Allocation</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td colspan="2" bgcolor="#0033FF"><strong><span class="normaltxtmidb2">C  u  s  t  o  m  e  r    P  l  a  n    U  p  l  o  a  d</span></strong></td>
</tr>
<tr>
  <td colspan="2">
  <table width="100%">
  <tr>
    <td colspan="3" align="center">
    <table>
    <tr>
      <td class="normalfntMid">Allocation Number: </td>
    <td class="normalfntMid">
    <select name="cboSearch" id="cboSearch" style="width:200px" >
    <option value=""></option>
    <?php   $sql = "SELECT DISTINCT
					intPrePlanRefNo
					FROM
					plan_pre_plan
					WHERE
					intEnterLocationId = '$companyId'
                    ORDER BY intPrePlanRefNo DESC ";
                $result = $db->RunQuery($sql);
                while($row=mysqli_fetch_array($result))
                {
					if($row['intPrePlanRefNo']==$prePlanNo)
						echo "<option value=\"".$row['intPrePlanRefNo']."\" selected=\"selected\">".$row['intPrePlanRefNo']."</option>";	
					else
						echo "<option value=\"".$row['intPrePlanRefNo']."\">".$row['intPrePlanRefNo']."</option>";
                }
  ?>
  </select>
    </td>
     <td class="normalfntMid">Customer: </td>
     <td class="normalfntMid"><select name="cboCustomer" id="cboCustomer"  style="width:200px" class="validate[required]" >
    <option value=""></option>
    <?php   $sql = "SELECT 	intId, strName 
					FROM mst_customer 
					WHERE intStatus='1' 
					ORDER BY strName;";
                $result = $db->RunQuery($sql);
                while($row=mysqli_fetch_array($result))
                {
					if($row['intId']==$customerId)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
					else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
                }
  ?>
  </select></td>
    </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td colspan="3" align="center">

    </td>
  </tr>
  <tr>
  <td width="34%" class="normalfnt"><a href="downLoadFile/sampleExcel.xls">Download sample excel format</a></td>
  <td width="45%"><table>
    <tr>
      <td class="normalfntBlue"><strong>Choose File</strong></td>
      <td><input type="file" name="file" id="file" onchange="uploadFile(this.value)"/>
        <input type="hidden" name="hdPath" id="hdPath" value="" /></td>
      <td><input type="button" name="btnUpload" id="btnUpload" value="Upload" /></td>
    </tr>
  </table></td>
  <td width="21%" style="text-align:right" class="normalfnt">&nbsp;<a target="_new" href="../../../customerAndOperation/sample/sampleProgress/sampleProgress.php">View Sample Details</a></td>
  </tr>
    </table></td>
</tr>
<tr>
  <td colspan="2">
  <table width="100%">
    <tr>
      <td>
        <div style="overflow:scroll;width:1270px;height:350px;" id="divGrid">
        <table width="700%" id="tblMainGrid2" class="grid" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
			<?php
			if($_SERVER["REQUEST_METHOD"] == "POST")
			{
			//==================================================================================================
			
			if($prePlanNo == "")
			{
				$prePlanNumber 	  = getNextPrePlanNo($companyId,$locationId);
				$prePlanReference = $prePlanNumber;
			}
			else
			{
				$prePlanReference = $prePlanNo;
				$sql = "UPDATE plan_pre_plan SET intCustomerId = '$customerId' WHERE intPrePlanRefNo = '$prePlanNo' ;";
				$db->RunQuery($sql);
				
			}
			
			//==================================================================================================
				$db->begin();
			try
			{
				ob_start();
				// include class file.
				include 'reader.php';
				
				$upload_path = 'uploadFile/'; //same directory
				
				if (!empty($_FILES['file'])) 
				{	
					if ($_FILES['file']['error'] == 0) 
					{
						// check extension
						$file = explode(".", $_FILES['file']['name']);
						if (move_uploaded_file($_FILES['file']['tmp_name'], $upload_path.'/'.$_FILES['file']['name'])) 
						{	
							fopen($file_path, 'r');
						}
					} 
				}
				//unlink($file_path);
				$filePath = $_POST['hdPath'];
				//echo $filePath;
				
				// initialize reader object
				$excel = new Spreadsheet_Excel_Reader();
				
				// read spreadsheet data
				$excel->read('uploadFile/'.$filePath);    
				
				// iterate over spreadsheet cells and print as HTML table
				$x=1;
				while($x<=$excel->sheets[0]['numRows']) {
				if($x==1)
				{
					 echo "\t<tr class=normalfntBlue>\n";
					 $y=1;
					 while($y<=$excel->sheets[0]['numCols']) 
					 {
						$cell = isset($excel->sheets[0]['cells'][$x][$y]) ? $excel->sheets[0]['cells'][$x][$y] : '';
						echo "\t\t<td bgcolor=#FAD163>$cell</td>\n";
						$y++;
					 }  
				}
				else
				{
				  echo "\t<tr bgcolor=#FFFFFF class=normalfntBlue>\n";
				  $y=1;
		//----------------------------------------------------------------------------------------------------------------------------
				  $cusId 		= isset($excel->sheets[0]['cells'][$x][1]) ? $excel->sheets[0]['cells'][$x][1] : '';
				  $cusName 		= isset($excel->sheets[0]['cells'][$x][2]) ? $excel->sheets[0]['cells'][$x][2] : '';
				  $season		= isset($excel->sheets[0]['cells'][$x][3]) ? $excel->sheets[0]['cells'][$x][3] : '';
				  $inquiry 		= isset($excel->sheets[0]['cells'][$x][4]) ? $excel->sheets[0]['cells'][$x][4] : '';
				  $item 		= isset($excel->sheets[0]['cells'][$x][5]) ? $excel->sheets[0]['cells'][$x][5] : '';
				  $style 		= isset($excel->sheets[0]['cells'][$x][6]) ? $excel->sheets[0]['cells'][$x][6] : '';
				  $graphic 		= isset($excel->sheets[0]['cells'][$x][7]) ? $excel->sheets[0]['cells'][$x][7] : '';
				  $salesOrder 	= isset($excel->sheets[0]['cells'][$x][8]) ? $excel->sheets[0]['cells'][$x][8] : '';
				  $color	 	= isset($excel->sheets[0]['cells'][$x][9]) ? $excel->sheets[0]['cells'][$x][9] : '';
				  $vaProcess	= isset($excel->sheets[0]['cells'][$x][10]) ? $excel->sheets[0]['cells'][$x][10] : '';
				  $partOfPrint	= isset($excel->sheets[0]['cells'][$x][11]) ? $excel->sheets[0]['cells'][$x][11] : '';
				  $orderQty		= isset($excel->sheets[0]['cells'][$x][12]) ? $excel->sheets[0]['cells'][$x][12] : '';
				  $psd			= isset($excel->sheets[0]['cells'][$x][13]) ? $excel->sheets[0]['cells'][$x][13] : '';
				  $pfd			= isset($excel->sheets[0]['cells'][$x][14]) ? $excel->sheets[0]['cells'][$x][14] : '';
				  $firstDelDate = isset($excel->sheets[0]['cells'][$x][15]) ? $excel->sheets[0]['cells'][$x][15] : '';
				  $lastDelDate 	= isset($excel->sheets[0]['cells'][$x][16]) ? $excel->sheets[0]['cells'][$x][16] : '';
				  $operation 	= isset($excel->sheets[0]['cells'][$x][17]) ? $excel->sheets[0]['cells'][$x][17] : '';				
			
			$updateHStatus = false;
			if(($x%2)==0)
			{
				$sqlChk = "SELECT   intId
						   FROM     plan_pre_plan
						   WHERE    intPrePlanRefNo = '$prePlanReference' AND
									strSeason = '$season' AND 
									strInquiry = '$inquiry' AND 
									strItem = '$item' AND
									strStyleNo = '$style' AND
									strGraphic = '$graphic' AND 
									strSalesOrder = '$salesOrder' AND  
									strColor = '$color' AND 
									strVaProcess = '$vaProcess' AND 
									strPartOfPrint = '$partOfPrint' AND 
									strOrdQty = '$orderQty' AND 
									dtmPsd = '$psd' AND 
									dtmPfd = '$pfd' AND 
									dtmFirstDeliveryDate = '$firstDelDate' AND
									dtmLastDeliveryDate = '$lastDelDate' AND
									intEnterLocationId = '$companyId' AND 
									strOperationSend = '$operation' ;";
									
				$resultChk = $db->RunQuery2($sqlChk);
				while($rowChk=mysqli_fetch_array($resultChk))
				{
					$sqlUpdH = "UPDATE plan_pre_plan 
								SET 
								dtmUploadDate = now() , 
								intCustomerId = '$customerId'  
								WHERE
								intId = '".$rowChk['intId']."' ;";
					$resultUpdH = $db->RunQuery2($sqlUpdH);
					
					$prePlanId   	= $rowChk['intId']; 
					$insUpdtStatus 	= "insert";
					
					if($resultUpdH)
					{
						$updateHStatus = true;
						$sqlDelD = "DELETE FROM plan_pre_plan_day_qty WHERE intPrePlanId = '".$rowChk['intId']."';";
						$db->RunQuery2($sqlDelD);
					}
				}
			}
				  
			if(($x%2)==1)
			{
				$sqlupd = "UPDATE plan_pre_plan SET strOperationRcv = '$operation'
						   WHERE intId = '$prePlanId' ;";
				$firstResult 	= $db->RunQuery2($sqlupd);
				$insUpdtStatus 	= "update";
			}
			if(($x%2)==0 && $updateHStatus==false)
			{
				 $sql = "INSERT INTO plan_pre_plan 
						(intPrePlanRefNo, dtmUploadDate, intCustomerId, strSeason, strInquiry, strItem, strStyleNo, strGraphic, strSalesOrder, 
						strColor, strVaProcess, strPartOfPrint, strOrdQty, dtmPsd, dtmPfd, dtmFirstDeliveryDate, dtmLastDeliveryDate, strOperationSend, 
						intEnterLocationId)
						VALUES 					
						('$prePlanReference',now(),'$customerId','$season','$inquiry','$item','$style','$graphic','$salesOrder','$color',
						'$vaProcess','$partOfPrint','$orderQty','$psd','$pfd','$firstDelDate','$lastDelDate','$operation','$companyId')";

				$firstResult 	= $db->RunQuery2($sql);
        		$prePlanId   	= $db->insertId; 
				$insUpdtStatus 	= "insert";	
			}
		//----------------------------------------------------------------------------------------------------------------------------
		  $z = 18;
		  while($z<=$excel->sheets[0]['numCols']) 
		  {
			 
			  	$date = isset($excel->sheets[0]['cells'][1][$z]) ? $excel->sheets[0]['cells'][1][$z] : '';
				$qty  = isset($excel->sheets[0]['cells'][$x][$z]) ? $excel->sheets[0]['cells'][$x][$z] : '0';
				
				if($insUpdtStatus=="insert")
				{
					
					$sql1 = "INSERT INTO plan_pre_plan_day_qty (intPrePlanId, strDate, intPrePlanRefNo, dblSendQty) 
					VALUES ('$prePlanId','$date','$prePlanReference','$qty')";
					$db->RunQuery2($sql1);
					$z++;
					
				}
				if($insUpdtStatus =="update")
				{
					
					$sql2 = "UPDATE plan_pre_plan_day_qty 
							 SET dblRcvQty = '$qty' 
							 WHERE intPrePlanId = '$prePlanId' AND strDate = '$date' ";
					$db->RunQuery2($sql2);
					$z++;		
				}	
				
		  }
		 
		 // echo $z.'"/"'.$excel->sheets[0]['numCols'];
		  
		//----------------------------------------------------------------------------------------------------------------------------
				  while($y<=$excel->sheets[0]['numCols']) 
				  {
					$cell = isset($excel->sheets[0]['cells'][$x][$y]) ? $excel->sheets[0]['cells'][$x][$y] : '';
					echo "\t\t<td>$cell</td>\n";  
					$y++;
				  }  
				}
				  echo "\t</tr>\n";
				  $x++;
				  $insUpdtStatus = "";
				}
				ob_flush();
			if($firstResult)
			{
				$db->commit();
			}
			else
			{	
				$db->rollback();//roalback
			}
			}
			catch(Exception $e)
			{       
				$db->rollback();//roalback
			}
			}
			//--------------------------------------------------------------------------------------------
				function getNextPrePlanNo($companyId,$locationId)
				{
					global $db;
					$sql = "SELECT
							intPrePlanNo
							FROM sys_plan_no
							WHERE
							intCompanyId = '$companyId' AND intLocationId = '$locationId'
							";	
					$result = $db->RunQuery($sql);
					$row = mysqli_fetch_array($result);
					$nextPrePlanNo = $row['intPrePlanNo'];
					
					$sql = "UPDATE `sys_plan_no` SET intPrePlanNo=intPrePlanNo+1 WHERE (intCompanyId = '$companyId' AND intLocationId = '$locationId')";
					$db->RunQuery($sql);
					return $nextPrePlanNo;
				}
			//--------------------------------------------------------------------------------------------
			//--------------------------------------------------------------------------------------------
				function getLatestAccPeriod($companyId)
				{
					global $db;
					$sql = "SELECT
							MAX(mst_financeaccountingperiod.intId) AS accId,
							mst_financeaccountingperiod.dtmStartingDate,
							mst_financeaccountingperiod.dtmClosingDate,
							mst_financeaccountingperiod.intStatus,
							mst_financeaccountingperiod_companies.intCompanyId,
							mst_financeaccountingperiod_companies.intPeriodId
							FROM
							mst_financeaccountingperiod
							Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
							WHERE
							mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
							ORDER BY
							mst_financeaccountingperiod.intId DESC
							";	
					$result = $db->RunQuery($sql);
					$row = mysqli_fetch_array($result);
					$latestAccPeriodId = $row['accId'];	
					return $latestAccPeriodId;
				}
			//--------------------------------------------------------------------------------------------
			//============================================================================================
				function encodePrePlanNo($prePlanNumber,$accountPeriod,$companyId,$locationId)
				{
					global $db;
					$sql = "SELECT
							mst_financeaccountingperiod.intId,
							mst_financeaccountingperiod.dtmStartingDate,
							mst_financeaccountingperiod.dtmClosingDate,
							mst_financeaccountingperiod.intStatus
							FROM
							mst_financeaccountingperiod
							WHERE
							mst_financeaccountingperiod.intId =  '$accountPeriod'
							";	
					$result = $db->RunQuery($sql);
					$row = mysqli_fetch_array($result);
					$startDate = substr($row['dtmStartingDate'],0,4);
					$closeDate = substr($row['dtmClosingDate'],0,4);
					$sql = "SELECT
							mst_companies.strCode AS company,
							mst_companies.intId,
							mst_locations.intCompanyId,
							mst_locations.strCode AS location,
							mst_locations.intId
							FROM
							mst_companies
							Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
							WHERE
							mst_locations.intId =  '$locationId' AND
							mst_companies.intId =  '$companyId'
							";
					$result = $db->RunQuery($sql);
					$row = mysqli_fetch_array($result);
					$companyCode = $row['company'];
					$locationCode = $row['location'];
					$prePlanFormat = $companyCode."/".$locationCode."/".$startDate."-".$closeDate."/".$prePlanNumber;
					return $prePlanFormat;
			//============================================================================================
			}
            ?>     
        </table>
        <hr width="770%" />
        <table id="tblLoadData" width="770%" class="grid" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
			<!--this area is excel sheet uploaded data preview-->
        </table>
        </div>    
      </td>
     </tr>
    </table>
    </td>
</tr>
<tr>
<td width="100%" align="center" bgcolor=""><img  style="display:none" border="0" src="../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
</tr>
</table>
</div>
</div>
</form>
</body>
</html>
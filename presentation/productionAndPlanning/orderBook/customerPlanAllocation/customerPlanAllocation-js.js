// JavaScript Document
var errMsg = "";
$(document).ready(function() {
	$("#frmOrderAllocation").validationEngine();	
  //permision for add 
  if(intAddx)
  {
	$('#frmOrderAllocation #butSave').show();
  }
  //permision for edit 
  if(intEditx)
  {
  	$('#frmOrderAllocation #butSave').show();
	//$('#frmOrderAllocation #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
	$('#frmOrderAllocation #cboSearch').change(function(){
		if($(this).val()=="")
		{
			document.getElementById('tblLoadData').innerHTML="";
			$('#frmOrderAllocation #cboCustomer').val("");
			return;
		}
		else
		{
			var url 	= "customerPlanAllocation-db-get.php?requestType=loadCombo&prePlanRefNo="+$(this).val();
			var httpobj = $.ajax({url:url,async:false})
			$('#frmOrderAllocation #cboCustomer').val(httpobj.responseText);
			
			var url = "customerPlanAllocation-db-get.php?requestType=loadData&prePlanRefNo="+$(this).val();
			var obj = $.ajax({url:url,async:false});
			document.getElementById('tblLoadData').innerHTML=obj.responseText;
		}
		
	});
	
	$(".year").live('change',function(){
		loadComboes('','year',1,1,1,1,this);
	});
	$(".sampNo").live('change',function(){
		loadComboes('','sampNo',0,1,1,1,this);
	});
	$(".revNo").live('change',function(){
		loadComboes('','revNo',0,0,1,1,this);
	});
	$(".combo").live('change',function(){
		loadComboes('','combo',0,0,0,1,this);
	});
	
	$('#frmOrderAllocation #btnUpload').click(function (){
		
		if ($('#frmOrderAllocation').validationEngine('validate'))
		{
			if(document.getElementById('file').value && errMsg == "")
			{
				document.getElementById('frmOrderAllocation').submit();	
			}
			else
			{
				$('#frmOrderAllocation #btnUpload').validationEngine('showPrompt', 'Please Choose File/ Valid File Before Uploading','fail');
				var t=setTimeout("alertx()",3000);
			}
		}
		else
		{
			return;
		}
		
	});
	
//------------------------ save data ---------------------

	$('#frmOrderAllocation #butSave').click(function(){
	var requestType = '';
	if ($('#frmOrderAllocation').validationEngine('validate'))   
    { 
		var data = "requestType=save";
			data+= "&prePlanRefNo=" + $('#cboSearch').val()+"&customerId="+ $('#cboCustomer').val();

			var rowCount = document.getElementById('tblLoadData').rows.length;
			if(rowCount<=1)
			{
				$('#frmOrderAllocation #butSave').validationEngine('showPrompt', 'No Data To Save','fail');
				var t=setTimeout("alertx()",3000);
				return false;				
			}
			var row = 0;		
			var arr="[";		
			$('#tblLoadData .year').each(function(){
	
				var sampYear	= $(this).parent().parent().find(".year").val();
				var sampNo		= $(this).parent().parent().find(".sampNo").val();
				var combo		= $(this).parent().parent().find(".combo").val();
				var printName	= $(this).parent().parent().find(".printName").val();
				var revisionNo	= $(this).parent().parent().find(".revNo").val();
				var prePlanId	= $(this).parent().parent().find(".prePlanId").attr('id');
				var printSize	= $(this).parent().parent().find(".size").val();
				var panel		= $(this).parent().parent().find(".panel").val();
				arr += "{";
				arr += '"sampYear":"'+	sampYear +'",' ;
						arr += '"sampNo":"'+ sampNo +'",' ;
						arr += '"combo":"'+ combo +'",' ;
						arr += '"printName":"'+ printName +'",' ;
						arr += '"revisionNo":"'+ revisionNo +'",' ;
						arr += '"prePlanId":"'+ prePlanId +'",' ;
						arr += '"printSize":"'+ printSize +'",' ;
						arr += '"panel":"'+ panel +'"' ;
						arr +=  '},';
			
			});
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;
		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = "customerPlanAllocation-db-set.php";
     	var obj = $.ajax({
			url:url,
			type:'post',
			dataType: "json",  
			data:data,
			async:false,
			success:function(json){
					$('#frmOrderAllocation #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertx()",1000);
						setTimeout("window.location.href=window.location.href;",1000)
						
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmOrderAllocation #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",1000);
					return;
				}		
			});
	}
   });

//--------------------------------------------------------
	$('.removeColumn').live('click',function(){	
		
		var planId = $(this).parent().attr('id');
		var val = $.prompt('Are you sure you want to delete "'+$(this).parent().parent().find('.clsSalesOrder').html()+'" ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
				if(v)
				{
					removePlan(planId);
					$('#'+planId).remove();
				}
			}
		});
	});

});
//--------------------functions area-----------------------
function uploadFile(string) 
{
	var flPath = string;
	var fileTypes=["xlsx","csv","xls","xml"];
	var param = false;
	var ext=string.substring(string.lastIndexOf(".")+1,string.length).toLowerCase();
	for (var i=0; i<fileTypes.length; i++)
	{
	 if (fileTypes[i]==ext)
	 {
		param = true;
	 }
	}
	 if(param == true)
	 {
		errMsg = "";
		document.getElementById('hdPath').value = flPath;
		//alert('File:\/\/' + flPath);
	 }
	 else
	 {
		alert("THAT IS NOT A VALID FILE\nPlease load a file with an extention of one of the following:\n\n"+fileTypes.join(", "));
		errMsg = "error";
	 }
}
/*function callSubmit()
{

	if(document.getElementById('file').value && errMsg == "")
	{
		document.getElementById('frmOrderAllocation').submit();	
	}
	else
	{
		alert("Please Choose File/ Valid File Before Uploading");
	}
}*/
function loadCombo_frmOrderAllocation()
{
	var url 	= "customerPlanAllocation-db-get.php?requestType=loadComboVal";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmOrderAllocation #cboSearch').html(httpobj.responseText);
}
function alertx()
{
	$('#frmOrderAllocation #btnUpload').validationEngine('hide');
	$('#frmOrderAllocation #butSave').validationEngine('hide');
}
function loadComboes(saveValue,slected,flag1,flag2,flag3,flag4,obj){ 

	if($(obj).val()=="")
	{
		if(flag1==1)
		$(obj).parent().parent().find(".sampNo").html('');
		if(flag2==1)
		$(obj).parent().parent().find(".revNo").html('');
		if(flag3==1)
		$(obj).parent().parent().find(".combo").html('');
		if(flag4==1)
		$(obj).parent().parent().find(".printName").html('');
		return;
	}	
	if(flag1==1)
	$(obj).parent().parent().find(".sampNo").html('');
	if(flag2==1)
	$(obj).parent().parent().find(".revNo").html('');
	if(flag3==1)
	$(obj).parent().parent().find(".combo").html('');
	if(flag4==1)
	$(obj).parent().parent().find(".printName").html('');
	
	
	var sampYear	= $(obj).parent().parent().find(".year").val();
	var sampNo 		= $(obj).parent().parent().find(".sampNo").val();
	var revNo 	 	= $(obj).parent().parent().find(".revNo").val();
	var combo 		= $(obj).parent().parent().find(".combo").val();
	var printName 	= $(obj).parent().parent().find(".printName").val();
	var graphicNo   = $(obj).parent().parent().find(".graphic").html();
	
	if(sampNo==null)
		sampNo='';
	if(revNo==null)
		revNo='';
	if(combo==null)
		combo='';
	
		var url 		= "customerPlanAllocation-db-get.php?requestType=loadAllComboes";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"sampYear="+sampYear+"&sampNo="+sampNo+"&revNo="+revNo+"&combo="+combo+"&slected="+slected+"&saveValue="+saveValue+"&graphicNo="+graphicNo,
			async:false,
			success:function(json){
				
				if(slected=='year')
				$(obj).parent().parent().find(".sampNo").html(json.combo)
				if(slected=='sampNo')
				$(obj).parent().parent().find(".revNo").html(json.combo)
				if(slected=='revNo')
				$(obj).parent().parent().find(".combo").html(json.combo)
				if(slected=='combo')
				$(obj).parent().parent().find(".printName").html(json.combo)
				if(!json.check)
				$(obj).parent().parent().find(".revNo").html(json.revCombo)	
			}
		});
	
	
}
function removePlan(planId)
{
	var url 		= "customerPlanAllocation-db-set.php?requestType=deletePlan";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"planId="+planId,
			async:false,
			success:function(json){
					
			}
		});
}
//---------------------------------------------------------
<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];

include  "{$backwardseperator}dataAccess/permisionCheck.inc";
$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	$locationId = $_SESSION["CompanyID"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Plan Allocated Orders</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../masterData/css/planning.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="planAllocatedOrder-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">

</head>

<body>
<form id="frmPlanOrder" name="frmPlanOrder" method="post" action="planAllocatedOrder-db-set.php" autocomplete="off">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div align="center">
<div class="trans_layoutL">
<div class="trans_text">Plan Allocated Orders</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td colspan="2" bgcolor="#0033FF"><strong><span class="normaltxtmidb2">A l  l  o  c  a  t  e  d  O  r  d  e  r  s</span></strong></td>
</tr>
<tr>
      <td colspan="2"><table width="99%">
        <tr>
          <td>
          <div style="overflow:scroll;width:900px;height:250px;" id="divGrid">
          <table width="115%" id="tblMainGrid1" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
            <tr class="">
              <td width="2%" bgcolor="#FAD163" class="normalfntMid">&nbsp;</td>
              <td width="16%"   height="27" bgcolor="#FAD163" class="normalfntMid"><strong>Plan/SalesOrder No</strong> <strong><img id="butItem" src="../../../../images/add_new.png" width="15" height="15" style="display:none"/></strong></td>
              <td width="9%"    bgcolor="#FAD163" class="normalfntMid"><strong>Style</strong></td>
              <td width="14%"    bgcolor="#FAD163" class="normalfntMid"><strong>Graphic No</strong></td>
              <td width="10%"    bgcolor="#FAD163" class="normalfntMid"><strong>Combo</strong></td>
              <td width="18%"    bgcolor="#FAD163" class="normalfntMid"><strong>Customer</strong></td>
              <td width="11%"    bgcolor="#FAD163" class="normalfntMid"><strong>PSD</strong></td>
              <td width="12%"  bgcolor="#FAD163" class="normalfntMid"  ><strong>Delivery Date</strong></td>
              <td width="8%"   bgcolor="#FAD163" class="normalfntMid" ><strong>Quantity</strong></td>
              <td width="8%"   bgcolor="#FAD163" class="normalfntMid" nowrap="nowrap" ><strong>Print Size</strong></td>
              <td width="8%"   bgcolor="#FAD163" class="normalfntMid" ><strong>Panels</strong></td>
              <td width="8%"   bgcolor="#FAD163" class="normalfntMid" ><strong>Sample Sheet</strong></td>
              </tr>
            <?php			
			$sql = "SELECT 	pa.intPlanId, 
					pa.intPlanNo, 
					pa.intOrderYear,  
					pa.strSalesOrder, 
					pa.strStyle, 
					pa.strGraphic, 
					pa.strCombo, 
					pa.intCustomerId, 
					pa.strProcess, 
					pa.dtPSD, 
					pa.dtDeliveryDate, 
					pa.dblQty, 
					pa.intLocationId, 
					pa.strIsAllocated, 
					pa.strIsPlaned ,
					mc.strName,
					pp.intSampleNo,
					pp.intSampleYear,
					pp.intRevisionNo,
					mp.strName as printSize,
					pp.intPanels,
					pp.intPrintSize
					FROM 
					plan_allocateorders pa
					INNER JOIN mst_customer mc ON mc.intId=pa.intCustomerId
					INNER JOIN plan_pre_plan pp ON pa.intPlanId=pp.intId
					INNER JOIN mst_printsizes mp ON mp.intId = pp.intPrintSize
					WHERE
					pa.strIsAllocated =  'Y' AND
					pa.strIsPlaned =  'N' AND
					pa.intLocationId =  '$locationId'
					";
			 $result = $db->RunQuery($sql);
			 while($row=mysqli_fetch_array($result))
			 {
				$id = $row['intOrderNo'];
			 ?>
              <tr class="normalfnt mainRow" bgcolor="#FFFFFF" id="<?php echo $row['intOrderYear']?>">
              <td class="normalfntMid planId" id="<?php echo $row['intPlanId']?>">
              <input type="checkbox" name="chkPlan" id="<?php echo $row['strSalesOrder'];?>" class="allocate validate[minCheckbox[1]]" /></td>
              <td  class="normalfntMid planNo" id="<?php echo $row['intPlanNo'];?>"><?php echo $row['intPlanNo'];?>/<?php echo $row['strSalesOrder'];?></td>
              <td class="normalfntMid style"><?php echo $row['strStyle'];?></td>
              <td class="normalfntMid graphic"><?php echo $row['strGraphic'];?></td>
              <td class="normalfntMid combo"><?php echo $row['strCombo'];?></td>
              <td class="normalfntMid customer" id="<?php echo $row['intCustomerId'];?>"><?php echo $row['strName'];?></td>
              <td class="normalfntMid psd"><?php echo $row['dtPSD'];?></td>
              <td align="center" class="normalfntMid delDate"><?php echo $row['dtDeliveryDate'];?></td>
              <td class="normalfntMid qty"><?php echo $row['dblQty'];?></td>
              <td class="normalfntMid printSize"><?php echo $row['printSize'];?></td>
              <td class="normalfntMid panel"><?php echo $row['intPanels'];?></td>
              <td class="normalfntMid"><a target="_blank"  href="../../../../presentation/costing/sample/samplePrices/addNew/sampleReport.php?no=<?php echo $row['intSampleNo']?>&year=<?php echo $row['intSampleYear']?>&revNo=<?php echo $row['intRevisionNo'] ?>">view</a></td>
              </tr>
		   <?php 
            } 
           ?>
          </table>
          </div>
          </td>
        </tr>
      </table></td>
      </tr>
<tr>
  <td width="100%" align="center" bgcolor="">
  <input style="background-color:#C0C0C0" type="button" name="butPlan" id="butPlan" value="Plan Order" class="plan" />  <input style="background-color:#C0C0C0" type="button" name="butClear" id="butClear" value="Clear" class="clear"/>
    <input style="background-color:#C0C0C0" type="button" name="butClose" id="butClose" value="Close" /></td>
</tr>
</table>
</div>
</div>
</form>
</body>
</html>
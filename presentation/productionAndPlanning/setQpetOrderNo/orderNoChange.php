<?php
session_start();
$backwardseperator = "../../../";
$mainPath = $_SESSION['mainPath'];
$userId 	= $_SESSION['userId'];

$thisFilePath =  $_SERVER['PHP_SELF'];
$allocationNo = "";
if($_SERVER['REQUEST_METHOD'] == "POST")
{
	$allocationNo = trim($_REQUEST['cboSearch']);
}

include  "{$backwardseperator}dataAccess/permisionCheck.inc";
$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	$locationId = $_SESSION["CompanyID"];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Plan - Update Qpet Order No</title>

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../masterData/css/planning.css" rel="stylesheet" type="text/css" />
<link href="../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="orderNoChange-js.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" href="../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../libraries/validate/template.css" type="text/css">

<style type="text/css">
.web_dialog_overlay
{
   position: fixed;
   top: 0;
   right: 0;
   bottom: 0;
   left: 0;
   height: 100%;
   width: 100%;
   margin: 0;
   padding: 0;
   background: #000000;
   opacity: .15;
   filter: alpha(opacity=15);
   -moz-opacity: .15;
   z-index: 0;
   display: none;
}
.web_dialog
{
   display: none;
   position: fixed;
   width: 450px;
   height: 350px;
   top: 50%;
   left: 50%;
   margin-left: -190px;
   margin-top: -100px;
   background-color: #F0F0F0;
   border: 2px solid #336699;
   padding: 0px;
   z-index: 0;
   font-family: Verdana;
   font-size: 10pt;
}
.web_dialog_title
{
   border-bottom: solid 2px #336699;
   background-color: #336699;
   padding: 4px;
   color: White;
   font-weight:bold;
}
.web_dialog_title a
{
   color: White;
   text-decoration: none;
}
.align_right
{
   text-align: right;
}
</style>
<script type="text/javascript" language="javascript">
function pageSubmit()
{
	document.getElementById('frmQpetOrderNo').submit();	
}
</script>
</head>

<body >
<form id="frmQpetOrderNo" name="frmQpetOrderNo" enctype="multipart/form-data" action="orderNoChange.php" method="post" autocomplete="off">
<input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div align="center">
<div class="trans_layoutXL">
<div class="trans_text">Customer Plan - Update Qpet Order No</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td colspan="2" bgcolor="#0033FF"><strong><span class="normaltxtmidb2">Update Qpet Order No</span></strong></td>
</tr>
<tr>
  <td colspan="2">
  <table width="100%">
  <tr>
    <td>
    <table>
    <tr>
      <td class="normalfntMid">Allocation Number: </td>
    <td class="normalfntMid">
    <select name="cboSearch" id="cboSearch" style="width:200px" onchange="pageSubmit();" >
    <option value=""></option>
    <?php   $sql = "SELECT DISTINCT
					intPrePlanRefNo
					FROM
					plan_pre_plan
					WHERE
					intEnterLocationId =  '$companyId'
					ORDER BY intPrePlanRefNo DESC
                    ";
                $result = $db->RunQuery($sql);
                while($row=mysqli_fetch_array($result))
                {
					if($row['intPrePlanRefNo']==$allocationNo)
					echo "<option value=\"".$row['intPrePlanRefNo']."\" selected=\"selected\">".$row['intPrePlanRefNo']."</option>";	
					else
					echo "<option value=\"".$row['intPrePlanRefNo']."\">".$row['intPrePlanRefNo']."</option>";
                }
  ?>
  </select>
    </td>
    <td class="normalfntMid">&nbsp;</td>
     <td class="normalfntMid">&nbsp;</td>
    </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td align="center">

    </td>
  </tr>
    </table></td>
</tr>
<tr>
  <td colspan="2" align="center">
  <table width="100%">
    <tr>
      <td align="center">
        <div style="overflow:scroll;width:1270px;height:350px;" id="divGrid">
        <table id="tblLoadData" width="210%" class="grid" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
         <thead>
		<tr class="normalfnt" id="dayHead">
		<td bgcolor="#FAD163" width="100"><strong>Plan No</strong></td>
		<td bgcolor="#FAD163" width="150"><strong>Uploaded Date</strong></td>
		<td bgcolor="#FAD163"><strong>Customer Name</strong></td>
		<td bgcolor="#FAD163"><strong>Season</strong></td>
		<td bgcolor="#FAD163"><strong>Inquiry Identification</strong></td>
		<td bgcolor="#FAD163"><strong>Item</strong></td>
		<td bgcolor="#FAD163"><strong>Style No</strong></td>
		<td bgcolor="#FAD163"><strong>Graphic</strong></td>
		<td bgcolor="#FAD163"><strong>Sample Year</strong></td>
		<td bgcolor="#FAD163"><strong>Sample Number</strong></td>
		<td bgcolor="#FAD163" width="130"><strong>Revision Number</strong></td>
		<td bgcolor="#FAD163" width="100"><strong>Combo</strong></td>
		<td bgcolor="#FAD163"><strong>Print Name</strong></td>
		<td bgcolor="#FAD163"><strong>Print Size</strong></td>
		<td bgcolor="#FAD163"><strong>Panel</strong></td>
        <td bgcolor="#FAD163"><strong>Order Year</strong></td>
        <td bgcolor="#FAD163"><strong>Order No</strong></td>
		<td bgcolor="#FAD163"><strong>Sales Order</strong></td>
		<td bgcolor="#FAD163"><strong>Color</strong></td>
		<td bgcolor="#FAD163"><strong>VA Process</strong></td>
		<td bgcolor="#FAD163"><strong>Part of the Print</strong></td>
		<td bgcolor="#FAD163"><strong>Order Qty</strong></td>
		<td bgcolor="#FAD163"><strong>PSD</strong></td>
		<td bgcolor="#FAD163"><strong>PFD</strong></td>
		<td bgcolor="#FAD163"><strong>First Delivery Date</strong></td>
		<td bgcolor="#FAD163"><strong>Last Delivery Date</strong></td>
	  </tr>
	 </thead>
		<?php
		$sql = "SELECT 	
				p.intId, p.intPrePlanRefNo, 
				p.dtmUploadDate, p.intCustomerId, 
				p.strSeason, p.strInquiry, 
				p.strItem, p.strStyleNo, 
				p.strGraphic, p.strSalesOrder, 
				p.strColor, p.strVaProcess, 
				p.strPartOfPrint, p.strOrdQty, 
				p.dtmPsd, p.dtmPfd, 
				p.dtmFirstDeliveryDate, p.dtmLastDeliveryDate, 
				p.strOperationSend, p.strOperationRcv, 
				p.intEnterLocationId, p.intSampleYear, 
				p.intSampleNo, p.intRevisionNo, 
				p.strCombo, p.strPrintName,
				c.strName,p.intPrintSize,
				mp.strName AS printSize,p.intPanels ,
				p.intQpetOrderNo,p.intQpetOrderYear
				FROM 
				plan_pre_plan p
				INNER JOIN mst_customer c ON c.intId=p.intCustomerId
				INNER JOIN mst_printsizes mp ON mp.intId=p.intPrintSize
				WHERE 
				p.intEnterLocationId = '$companyId' ";
		if($allocationNo!="")
			$sql.="AND p.intPrePlanRefNo = '$allocationNo' ";
					
			$sql.="ORDER BY p.intId ";
		$result = $db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
		?>
			<tbody class="normalfnt" bgcolor="#FFFFFF">
			<tr bgcolor="#FFFFFF" class="normalfntBlue" id="<?php echo $row['intId']; ?>">
            <td bgcolor="#FFFFF3" width="100" id="<?php echo $row['intId']; ?>" class="prePlanId">&nbsp;<?php echo $row['intPrePlanRefNo']; ?>&nbsp;</td>
            <td bgcolor="#FEE0C6" width="150">&nbsp;<?php echo $row['dtmUploadDate']; ?>&nbsp;</td>	
            <td bgcolor="#F0F8FF" id="<?php echo $row['intCustomerId']; ?>" class="customer">&nbsp;<?php echo $row['strName']; ?>&nbsp;</td>
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['strSeason']; ?>&nbsp;</td>	
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['strInquiry']; ?>&nbsp;</td>	
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['strItem']; ?>&nbsp;</td>
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['strStyleNo']; ?>&nbsp;</td>	
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['strGraphic']; ?>&nbsp;</td>
            
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['intSampleYear']; ?>&nbsp;</td>
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['intSampleNo']; ?>&nbsp;</td>
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['intRevisionNo']; ?>&nbsp;</td>
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['strCombo']; ?>&nbsp;</td>
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['strPrintName']; ?>&nbsp;</td>
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['printSize']; ?>&nbsp;</td>	
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['intPanels']; ?>&nbsp;</td>
            <td bgcolor="#F0F8FF" >
             <select name="cboOrderYear" class="orderYear" id="cboOrderYear" style="width:100%">
            <option value=""></option>
            <?php
			$sql1 = "SELECT DISTINCT tor.intOrderYear 
					FROM trn_orderheader tor
					INNER JOIN mst_locations ml ON ml.intId=tor.intLocationId
					WHERE ml.intCompanyId = '$companyId' ";
			$result1 = $db->RunQuery($sql1);
			while($row1=mysqli_fetch_array($result1))
			{
				if($row1['intOrderYear']==$row['intQpetOrderYear'])
					echo "<option value=\"".$row1['intOrderYear']."\" selected=\"selected\">".$row1['intOrderYear']."</option>";
				else if($row1['intOrderYear']==date('Y'))
					echo "<option value=\"".$row1['intOrderYear']."\" selected=\"selected\">".$row1['intOrderYear']."</option>";	
				else
					echo "<option value=\"".$row1['intOrderYear']."\">".$row1['intOrderYear']."</option>";
			}
			?>
            </select>
            </td>
            <td bgcolor="#F0F8FF" >
           <select name="cboOrderNo" class="orderNo" id="cboOrderNo" style="width:100%">
            <option value=""></option>
            <?php
			$sql2 = "SELECT DISTINCT tor.intOrderNo 
					FROM trn_orderheader tor
					INNER JOIN mst_locations ml ON ml.intId=tor.intLocationId
					WHERE ml.intCompanyId = '$companyId' ";
					
			if($row['intQpetOrderYear']!='')
				$sql2.="AND intOrderYear='".$row['intQpetOrderYear']."' ";
			else
				$sql2.="AND intOrderYear='".date('Y')."' ";
				
			$result2 = $db->RunQuery($sql2);
			while($row2=mysqli_fetch_array($result2))
			{
				if($row2['intOrderNo']==$row['intQpetOrderNo'])
					echo "<option value=\"".$row2['intOrderNo']."\" selected=\"selected\">".$row2['intOrderNo']."</option>";	
				else
					echo "<option value=\"".$row2['intOrderNo']."\">".$row2['intOrderNo']."</option>";
			}
			?>
            </select>
            </td>
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['strSalesOrder']; ?>&nbsp;</td>
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['strColor']; ?>&nbsp;</td>
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['strVaProcess']; ?>&nbsp;</td>
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['strPartOfPrint']; ?>&nbsp;</td>
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['strOrdQty']; ?>&nbsp;</td>
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['dtmPsd']; ?>&nbsp;</td>	
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['dtmPfd']; ?>&nbsp;</td>
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['dtmFirstDeliveryDate']; ?>&nbsp;</td>	
            <td bgcolor="#F0F8FF" >&nbsp;<?php echo $row['dtmLastDeliveryDate']; ?>&nbsp;</td>		
					       
        <?php
		}
		?>
        </table>
        </div>    
      </td>
     </tr>
    </table>
    </td>
</tr>
<tr>
<td width="100%" align="center" bgcolor=""><img border="0" src="../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img  src="../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
</tr>
</table>
</div>
</div>
</form>
</body>
</html>
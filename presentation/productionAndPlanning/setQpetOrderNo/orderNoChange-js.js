// JavaScript Document
$(document).ready(function(){

	$("#frmQpetOrderNo").validationEngine();
	
	$('#frmQpetOrderNo #butSave').click(function(){
	
		var rowCount = document.getElementById('tblLoadData').rows.length;
			if(rowCount<=1){
				$('#frmQpetOrderNo #butSave').validationEngine('showPrompt', 'No Data To Save','fail');
				var t=setTimeout("alertx()",3000);
				return false;				
			}
		var value="[ ";
		$('.orderNo').each(function(){
					
						var orderNo   = ($(this).parent().parent().find(".orderNo").val()==null?"":$(this).parent().parent().find(".orderNo").val());
						var orderYear = ($(this).parent().parent().find(".orderYear").val()==null?"":$(this).parent().parent().find(".orderYear").val());
						var prePlanId = $(this).parent().parent().find(".prePlanId").attr('id');
						
						if(orderNo!="" || orderYear!="")
							value +='{"orderNo":"'+orderNo+'","orderYear":"'+orderYear+'","prePlanId":"'+prePlanId+'"},' ;			
					});	
					
					value = value.substr(0,value.length-1);
					value += " ]";
					
					var url = "orderNoChange-db-set.php?requestType=updateOrderNo";
					$.ajax({
							url:url,
							async:false,
							dataType:'json',
							type:'post',
							data:'&orderDetails='+value,
						success:function(json){
							$('#frmQpetOrderNo #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							if(json.type=='pass')
							{
								var t=setTimeout("alertx()",1000);
								setTimeout("window.location.href=window.location.href;",1000)
								return;
							}
						},
						error:function(){
							
							$('#frmQpetOrderNo #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",3000);						
						}
					});
	});
	
	$('.orderYear').live('change',function(){
		
		if($(this).val()=="")
			$(this).parent().parent().find(".orderNo").html('');
		else
		{
			var url 	= "orderNoChange-db-set.php?requestType=loadOrderNo&orderYear="+$(this).val();
			var htpObj  = $.ajax({url:url,async:false});
			$(this).parent().parent().find(".orderNo").html(htpObj.responseText);
		}
		
	});

});
function alertx()
{
	$('#frmQpetOrderNo #butSave').validationEngine('hide')	;
}
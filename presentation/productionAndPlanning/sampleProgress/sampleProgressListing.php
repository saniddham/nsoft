<?php
date_default_timezone_set('Asia/Kolkata');
//ini_set('allow_url_include',1);
ini_set('max_execution_time',3000);
session_start();

$backwardseperator 	= "../../../";
$thisFilePath 		=  $_SERVER['PHP_SELF'];
include_once $backwardseperator."dataAccess/permisionCheck2.inc";

include_once "../../../libraries/jqgrid2/inc/jqgrid_dist.php";

$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$intUser  	= $_SESSION["userId"];
			
/*$sql = "select * from(  SELECT TSI.intSampleNo as Sample_No,
						TSI.intSampleYear as Sample_Year,
						TSI.intRevisionNo as Rev_No,
						TSI.strGraphicRefNo as graphic_No,
						TSI.strStyleNo as Style_No,
						IFNULL(SUM(OD.intQty),0) as orderQty,
						IFNULL(OH.strCustomerPoNo,'') as customerPONo,
						IFNULL(OD.intOrderNo,'') as orderNo,
						IFNULL(OD.intOrderYear,'') as orderYear
						FROM trn_sampleinfomations TSI
						inner JOIN trn_orderdetails OD ON OD.intSampleNo=TSI.intSampleNo AND
						OD.intSampleYear=TSI.intSampleYear AND
						OD.intRevisionNo=TSI.intRevisionNo
						inner JOIN trn_orderheader OH ON OH.intOrderNo=OD.intOrderNo AND OH.intOrderYear=OD.intOrderYear
						GROUP BY TSI.intSampleYear,TSI.intSampleNo,TSI.intRevisionNo,OD.intOrderNo,OD.intOrderYear
					 ) as t where 1=1 "; */
					 
$sql = "SELECT *
FROM (SELECT
        * FROM view_sample_progress) AS t
WHERE 1 = 1"; 
					 
//echo $sql;						  						  
$col = array();

$col["title"] 	= "Customer"; // caption of column
$col["name"] 	= "customerName"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "5";
$col["align"] 	= "left";

$cols[] = $col;	$col=NULL;

$col["title"] 	= "Customer Location"; // caption of column
$col["name"] 	= "customerLocation"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "6";
$col["align"] 	= "left";

$cols[] = $col;	$col=NULL;
$col["title"] 	= "Sample No"; // caption of column
$col["name"] 	= "Sample_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
$col["align"] 	= "center";



$reportLink  = "../../../presentation/customerAndOperation/sample/Report/sampleReport.php?no={Sample_No}&year={Sample_Year}&revNo={Rev_No}";

$cols[] = $col;	$col=NULL;

$col["title"] = "Sample Year"; // caption of column
$col["name"] = "Sample_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

$col["title"] = "Rev No"; // caption of column
$col["name"] = "Rev_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

$col["title"] = "Combo"; // caption of column
$col["name"] = "Combo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

$col["title"] = "Print"; // caption of column
$col["name"] = "Print"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;


$col["title"] = "Currency"; // caption of column
$col["name"] = "strCode"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;


$col["title"] = "Sample Price"; // caption of column
$col["name"] = "Sample_Price"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;


$col["title"] = "Graphic No"; // caption of column
$col["name"] = "graphic_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "6";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

$col["title"] = "Style No"; // caption of column
$col["name"] = "Style_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

$col["title"] = "Customer PO"; // caption of column
$col["name"] = "customerPONo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

$col["title"] = "Order No"; // caption of column
$col["name"] = "orderNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

$col["title"] = "Order Year"; // caption of column
$col["name"] = "orderYear"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

$col["title"] = "SalesOrder No"; // caption of column
$col["name"] = "salesOrderNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

$col["title"] = "Part"; // caption of column
$col["name"] = "part"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

$col["title"] = "Order Qty"; // caption of column
$col["name"] = "orderQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

$col["title"] = "Report"; // caption of column
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["default"] = "View";
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Sample Progress";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'orderby'; // by default sort grid by this field
$grid["sortorder"] 		= "ASC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes

$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);
$out = $jq->render("list1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sample Progress</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
</head>
<body>	
<form id="frmlisting" name="frmlisting" method="post" action="">
<table width="100%" border="0" align="center">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
	<link rel="stylesheet" type="text/css" media="screen" href="../../../libraries/jqdrid/js/themes/smoothness/jquery-ui.custom.css"></link>	
	<link rel="stylesheet" type="text/css" media="screen" href="../../../libraries/jqdrid/js/jqgrid/css/ui.jqgrid.css"></link>	
	
	<script src="../../../libraries/jqdrid/js/jquery.min.js" type="text/javascript"></script>
	<script src="../../../libraries/jqdrid/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script src="../../../libraries/jqdrid/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="../../../libraries/jqdrid/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
	<script src="../../../libraries/javascript/script.js" type="text/javascript"></script>
	
    <td><table width="100%" border="0">
      <tr>
        <td>
        <div align="center" style="margin:10px">     
			<?php echo $out?>
        </div>
        </td>
      </tr>

    </table></td>
    </tr>
</form>
</body>
</html>
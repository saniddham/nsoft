<?php
session_start();
ini_set('max_execution_time',600000);
$backwardseperator 	= "../../../";
require_once "{$backwardseperator}dataAccess/Connector.php";
require_once '../../../libraries/excel/Classes/PHPExcel.php';
require_once '../../../libraries/excel/Classes/PHPExcel/IOFactory.php';

$fromDate	= $_REQUEST["fromDate"]; 
$toDate		= $_REQUEST["toDate"];
$location	= $_REQUEST["location"];
$printType	= $_REQUEST["printType"];
$arrDate    = array();
$prevPrntId = "";
error_reporting(E_ALL);

$file = 'productionPlanReport-xlsx.xls'; //LOAD TEMPARARY FILE

if (!file_exists($file))
	exit("Can't find $fileName ");

$objPHPExcel = PHPExcel_IOFactory::load($file);
$i 		= 2;
$j 		= 18;

	$start = strtotime($fromDate);
	$end = strtotime($toDate);
	$days_between = ceil(abs($end - $start) / 86400);
	$newFrmDate = $fromDate;
	
	for($t=0;$t<=$days_between;$t++)
	{
		$arrDate[$t]   = $newFrmDate ;
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j,1,$newFrmDate);	
		$date = date_create($newFrmDate);
		date_add($date, date_interval_create_from_date_string('1 days'));
		$nextDt = date_format($date, 'Y-m-d');
		$newFrmDate = $nextDt;
		$j++;
	}

	 $sql2 = "SELECT DISTINCT
	 				pp.intId, 
					pp.intPrePlanRefNo, 
					pp.intCustomerId, 
					pp.strSeason, 
					pp.strInquiry, 
					pp.strItem, 
					pp.strStyleNo, 
					pp.strGraphic, 
					pp.strSalesOrder, 
					pp.strColor, 
					pp.strVaProcess, 
					pp.strPartOfPrint, 
					pp.strOrdQty, 
					pp.dtmPsd, 
					pp.dtmPfd, 
					pp.dtmFirstDeliveryDate, 
					pp.dtmLastDeliveryDate, 
					pp.strOperationSend,
					pp.strOperationRcv,
					mc.strName,
					ps.dblQty as planQty,
					ps.startDate,
					ps.endDate,
					ps.intStripId,
					ps.intMainTeam,
					ps.intSubTeam,
					ps.dbCellLeft,
					ps.dblCompleteQty,
					pa.intColours,
					pa.intStrocks,
					pa.intPanels,
					pa.intPrintSize,
					mp.intId as printId,
					mp.strName as printName	
					FROM 
					plan_stripes ps
					INNER JOIN plan_pre_plan pp ON ps.intPlanId=pp.intId AND ps.intOrderNo=pp.intPrePlanRefNo AND ps.strSalesOrder=pp.strSalesOrder
					INNER JOIN mst_customer mc ON mc.intId=pp.intCustomerId
					INNER JOIN plan_allocateorders pa ON pa.intPlanId=ps.intPlanId AND pa.intPlanNo=ps.intOrderNo
					INNER JOIN mst_printertypes mp ON mp.intId=ps.intPrintType 
					AND pa.strSalesOrder=ps.strSalesOrder AND pa.intOrderYear=ps.intOrderYear 
					AND pa.intSplitId=ps.intSplitId AND pa.intLocationId=ps.intLocationId
					WHERE pa.strIsAllocated = 'Y' AND pa.strIsPlaned = 'Y' AND ps.intLocationId = '$location' ";
					
					if($printType!="")
						$sql2 .= "AND ps.intPrintType='$printType' ";
						
					$sql2 .= "ORDER BY ps.intMainTeam,ps.intSubTeam,ps.startDate,ps.intPrintType";
				
	$operation = "dblSendQty";
	$result2 = $db->RunQuery($sql2);
	while($row2=mysqli_fetch_array($result2))
	{
		$operation = "dblSendQty";
		$planQty   = $row2["planQty"];
		$mainTeam  = $row2["intMainTeam"];
		$subTeam   = $row2["intSubTeam"];
		$stripId   = $row2["intStripId"];
		$startDate = $row2["startDate"];
		$endDate   = $row2["endDate"];
		$dclLeft   = $row2["dbCellLeft"];
		$compltQty = $row2["dblCompleteQty"]; 
		$printSize = $row2["intPrintSize"]; 
		$noOfcolor = $row2["intColours"]; 
		$noOfShots = $row2["intStrocks"]; 
		$panels	   = $row2["intPanels"]; 
		$curPrntId = $row2["printId"];
		
		if($prevPrntId!=$curPrntId)
		{
			if($i!=2)
			{
				$objPHPExcel->getActiveSheet()->insertNewRowBefore($i, 1);
				$objPHPExcel->getActiveSheet()->getStyle("A$i:IV$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("ffffff");	
				$i++;
			}
				
			$objPHPExcel->getActiveSheet()->getStyle("A$i:IV$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("add8e6");	
			//$objPHPExcel->getActiveSheet()->mergeCells("A$i:L$i");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$i,$row2['printName'])->getStyle("A$i:L$i")->getFont()->setBold(true);
			$i++;
		}
		
		
		for($x=0;$x<2;$x++)
		{
			//$objPHPExcel->getActiveSheet()->getStyle("A$i:R$i")->getFont()->setName('Times New Roman');
			//$objPHPExcel->getActiveSheet()->getStyle("A$i:R$i")->getFont()->setSize(9);	
		//	$objPHPExcel->getActiveSheet()->getStyle("N$i")->getFont()->getColor()->setARGB('#AACCEE');
						
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$i,$row2["intCustomerId"]);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,$i,$row2["strName"]);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,$i,$row2["strSeason"]);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,$i,$row2["strInquiry"]);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$i,$row2["strItem"]);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,$i,$row2["strStyleNo"]);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$i,$row2["strGraphic"]);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$i,$row2["strSalesOrder"]);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8,$i,$row2["strColor"]);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9,$i,$row2["strVaProcess"]);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10,$i,$row2["strPartOfPrint"]);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11,$i,$row2["strOrdQty"]);
			//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12,$i,$row2["planQty"]);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13,$i,$row2["dtmPsd"]);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14,$i,$row2["dtmPfd"]);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15,$i,$row2["dtmFirstDeliveryDate"]);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16,$i,$row2["dtmLastDeliveryDate"]);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17,$i,($operation=="dblSendQty"?$row2["strOperationSend"]:$row2["strOperationRcv"]));
		
			$sql3 = " SELECT $operation ,strDate
					  FROM plan_pre_plan_day_qty 
					  WHERE intPrePlanId='".$row2["intId"]."';";
			
			$result3 = $db->RunQuery($sql3);
			
				
			while($row3=mysqli_fetch_array($result3))
			{
				$z = 18;
				
				for($arr=0;$arr<count($arrDate);$arr++)	
				{
					if($row3['strDate']==$arrDate[$arr])
					{
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($z,$i,($row3[$operation]==0?"":$row3[$operation]));
						//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($z,$i,$arrDate[$arr]);
					}
					$z++;
				}			
			}
			$operation = "dblRcvQty";
			$i++;
			
		}
		
		$objPHPExcel->getActiveSheet()->insertNewRowBefore($i, 1);
		$objPHPExcel->getActiveSheet()->getStyle("A$i:IV$i")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("00EE76");	
		//$objPHPExcel->getActiveSheet()->mergeCells("A$i:L$i");
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9,$i,"Planning")->getStyle("A$i:L$i")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("J$i")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12,$i,$planQty);
		
		$y = 18;
		for($ar=0;$ar<count($arrDate);$ar++)	
		{
			$sql5 		= " SELECT  DATE(dtmDate) planDate,
							dblQty,dblActualQty
							FROM 
							plan_strip_details 
							WHERE intStripId = '$stripId' ";
			$result5 	= $db->RunQuery($sql5);
			while($row5 = mysqli_fetch_array($result5))
			{
				if($arrDate[$ar]==$row5['planDate'])
				{
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($y,$i,$row5['dblQty']);
					$mi = $i+1;
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($y,$i+1,($row5['dblActualQty']==0?"":$row5['dblActualQty']));
					$objPHPExcel->getActiveSheet()->getStyle("A$mi:IV$mi")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB("D8BFD8");
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9,$i+1,"Actual")->getStyle("A$mi:L$mi")->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("J$mi")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
			}		
			$y++;	
		}	
		$i+=2;	
		$prevPrntId = $curPrntId;
	}

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$file.'"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output'); 
echo 'done';
exit;
?>
// JavaScript Document
$(document).ready(function(){

	$("#frmProductionPlan #butDownload").click(function(){
		if($("#frmProductionPlan").validationEngine('validate')){
			downloadFile();
		}
	});
});
function downloadFile()
{
	var fromDate 	 = $('#frmProductionPlan #dtFromDate').val();
	var toDate 	 	 = $('#frmProductionPlan #dtToDate').val();
	var location  	 = $('#frmProductionPlan #cboLocation').val();
	var printType  	 = $('#frmProductionPlan #cboPrintType').val();
	
	window.open('productionPlanReport-xlsx.php?fromDate='+fromDate+'&toDate='+toDate+'&location='+location+'&printType='+printType);	

}
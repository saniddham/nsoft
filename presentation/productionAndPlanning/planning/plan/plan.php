<?php

$backwardseperator = "../../";
session_start();
//$intPubCompanyId		=$_SESSION["FactoryID"];
$intPubCompanyId		= $_SESSION["CompanyID"];

$year=date('Y');
$last_month_num=date('m')-1;

$day=date('d');
$last_month=$year.'-'.$last_month_num.'-'.$day;


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Planning Board</title>
<link href="../css/planning.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/core.css" type="text/css" charset="utf-8">
<!--<link href="dragable-content.css" rel="stylesheet" type="text/css"/>-->

<!--<script type="text/javascript">
var rememberPositionedInCookie = true;
var rememberPosition_cookieName = 'demo';
</script>-->

<!--

////////this is working context menu

<link rel="stylesheet" type="text/css" href="../jqcontextmenu.css">
<script type="text/javascript" src="../jquery1.js"></script>
<script type="text/javascript" src="../jqcontextmenu.js"></script>

<script type="text/javascript">

jQuery(document).ready(function($){
	$('div').addcontextmenu('contextmenu2') //apply context menu to all images on the page
})

</script>-->
<script language="javascript">
<!--
var ie	= document.all
var ns6	= document.getElementById&&!document.all

var isMenu 	= false ;

var menuSelObj = null ;
var overpopupmenu = false;

function mouseSelect(e)
{
	var obj = ns6 ? e.target.parentNode : event.srcElement.parentElement;

	if( isMenu )
	{
		if( overpopupmenu == false )
		{
			isMenu = false ;
			overpopupmenu = false;
			document.getElementById('menudiv').style.display = "none" ;
			document.getElementById('menudiv1').style.display = "none" ;
			return true ;
		}
		return true ;
	}
	//return false;
}


// POP UP MENU
function	ItemSelMenu(e)
{
	var	obj = ns6 ? e.target.parentNode : event.srcElement.parentElement;	

      menuSelObj = obj ;
	if (ns6)
	{
	//alert(document.getElementById('menudiv').style.top );
		document.getElementById('menudiv').style.left = (e.clientX+document.body.scrollLeft)+'px';
		//alert(document.getElementById('menudiv').style.top );
		document.getElementById('menudiv').style.top = (e.clientY+document.body.scrollTop)+'px';
	} else
	{
		document.getElementById('menudiv').style.pixelLeft = event.clientX+document.body.scrollLeft;
		document.getElementById('menudiv').style.pixelTop = event.clientY+document.body.scrollTop;
	}
	document.getElementById('menudiv').style.display = "";
	//disableAndEnableRightClickpopUp();
	document.getElementById('item1_splitQty').style.backgroundColor='#FFFFFF';
	document.getElementById('item2_applyCurve').style.backgroundColor='#FFFFFF';
	document.getElementById('item3_remove').style.backgroundColor='#FFFFFF';
	document.getElementById('item4_join').style.backgroundColor='#FFFFFF';
	document.getElementById('item1_splitQty').style.color="#000099";
	document.getElementById('item2_applyCurve').style.color="#000099";
	document.getElementById('item3_remove').style.color="#000099";
	document.getElementById('item4_join').style.color="#000099";
	disableAndEnableRightClickpopUp();
	//document.getElementById('item4').style.backgroundColor='#FFFFFF';
	isMenu = true;
	return false ;
}

function	ItemSelMenu1(e,obj)
{
	createPop(obj);
	var	obj = ns6 ? e.target.parentNode : event.srcElement.parentElement;	

      menuSelObj = obj ;
	if (ns6)
	{
	//alert(document.getElementById('menudiv').style.top );
		document.getElementById('menudiv1').style.left = (e.clientX+document.body.scrollLeft)+'px';
		//alert(document.getElementById('menudiv').style.top );
		document.getElementById('menudiv1').style.top = (e.clientY+document.body.scrollTop)+'px';
	} else
	{
		document.getElementById('menudiv1').style.pixelLeft = event.clientX+document.body.scrollLeft;
		document.getElementById('menudiv1').style.pixelTop = event.clientY+document.body.scrollTop;
	}
	document.getElementById('menudiv1').style.display = "";
	//disableAndEnableRightClickpopUp();
	document.getElementById('item1').style.backgroundColor='#FFFFFF';
	document.getElementById('item2').style.backgroundColor='#FFFFFF';
	document.getElementById('item3').style.backgroundColor='#FFFFFF';
	document.getElementById('item4').style.backgroundColor='#FFFFFF';
	document.getElementById('item1').style.color="#000099";
	document.getElementById('item2').style.color="#000099";
	document.getElementById('item3').style.color="#000099";
	document.getElementById('item4').style.color="#000099";
	//alert(obj.id);
	document.getElementById('tbl_menuDiv1').rows[0].id=obj.id;
	
	//disableAndEnableRightClickpopUp();
	//document.getElementById('item4').style.backgroundColor='#FFFFFF';
	isMenu = true;
	return false ;
}

document.onmousedown 	= mouseSelect;


//document.oncontextmenu 	= ItemSelMenu;
//-->
</script>
    



	
	<script type="text/javascript" src="dragable-content.js"></script>	
	
	<script src="../js/jquery-1.js"></script>
	<script src="../js/jquery-ui-1.js"></script>
	
<link href="../css/planning.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../javascript/calendar/theme.css" />
<script src="../../javascript/calendar/calendar.js" type="text/javascript"></script>
<script src="../../javascript/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../js/script.js" type="text/javascript"></script>


<script src="../teams/teams-js.js" type="text/javascript"></script>
<script src="../calender/calender-js.js" type="text/javascript"></script>
<script src="../learningCurve/curves-js.js" type="text/javascript"></script>
<script src="plan-js.js" type="text/javascript"></script>
<script src="../orderBook/orderBook-js.js" type="text/javascript"></script>

<script src="../orderBook/orderBook-js.js" type="text/javascript"></script>

<script type="text/javascript" src="../learningCurve/charts/jquery.min.js"></script>
<script type="text/javascript" src="../learningCurve/charts/highcharts.js"></script>
<script type="text/javascript" src="../learningCurve/charts/highslide-full.min.js"></script>
<script type="text/javascript" src="../learningCurve/charts/highslide.config.js" charset="utf-8"></script>
	
  
	<script type="text/javascript">

	$(function() {
		$("#draggable3").draggable({ containment: '#containment-wrapper', scroll: false });
	});
	</script>
		
<style>
	.strip{
		/*height:19px;*/
		/*overflow:hidden;*/
		/*border-bottom:thin;*/
		/*line-height:16px;*/
		/*background-color: #D6E7F5;*/
		/*border-color:#000000;
		border-style:solid;
		border-width:0px;
		line-height:17px;
		border-bottom:2px;*/
		
	/*	font-size:9px;
		color: #660000;
		cursor:move;
		
		opacity:0.6;		*/ 
		
		color: #660000;
		cursor:move;
		height:18px;
		line-height:18px;
		overflow:hidden;
		background:#D6E7F5;
		opacity:0.6;
		
		font-size: 9px;
		font-family: verdana, arial, sans-serif;
		font-weight:light;
		margin:10px 10px 10px 10px;
		border: green solid 1px;
		white-space: normal;
	}
	
.drag {border: 1px solid black; background-color: rgb(240, 240, 240); position: relative; padding: 0.5em; margin: 0 0 0.5em 1.5em; cursor: move;}
.dragme{
	position:relative;
	left: 72px;
	top: 150px;
}
</style>

<style type="text/css">
	#draggable, #draggable2 { width: 100px; height: 100px; padding: 0.5em; float: left; margin: 10px 10px 10px 0; }
	#droppable { width: 150px; height: 150px; padding: 0.5em; float: left; margin: 10px; }
	.classTeam{background:#FFFFFF;height:17px ;float: left;margin: 1px;}
	.ui-widget-header p, .ui-widget-content p { margin: 0; }

</style>



	

		


<script type="text/javascript">

var oldLink = null;
// code to change the active stylesheet
function setActiveStyleSheet(link, title) {
  var i, a, main;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
    if(a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title")) {
      a.disabled = true;
      if(a.getAttribute("title") == title) a.disabled = false;
    }
  }
  if (oldLink) oldLink.style.fontWeight = 'normal';
  oldLink = link;
  link.style.fontWeight = 'bold';
  return false;
}

// This function gets called when the end-user clicks on some date.
function selected(cal, date) {
  cal.sel.value = date; // just update the date in the input field.
  if (cal.dateClicked && (cal.sel.id == "sel1" || cal.sel.id == "sel3"))
    // if we add this call we close the calendar on single-click.
    // just to exemplify both cases, we are using this only for the 1st
    // and the 3rd field, while 2nd and 4th will still require double-click.
    cal.callCloseHandler();
}

// And this gets called when the end-user clicks on the _selected_ date,
// or clicks on the "Close" button.  It just hides the calendar without
// destroying it.
function closeHandler(cal) {
  cal.hide();                        // hide the calendar
//  cal.destroy();
  _dynarch_popupCalendar = null;
}

// This function shows the calendar under the element having the given id.
// It takes care of catching "mousedown" signals on document and hiding the
// calendar if the click was outside.
function showCalendar(id, format, showsTime, showsOtherMonths) {
  var el = document.getElementById(id);
  if (_dynarch_popupCalendar != null) {
    // we already have some calendar created
    _dynarch_popupCalendar.hide();                 // so we hide it first.
  } else {
    // first-time call, create the calendar.
    var cal = new Calendar(1, null, selected, closeHandler);
    // uncomment the following line to hide the week numbers
    // cal.weekNumbers = false;
    if (typeof showsTime == "string") {
      cal.showsTime = true;
      cal.time24 = (showsTime == "24");
    }
    if (showsOtherMonths) {
      cal.showsOtherMonths = true;
    }
    _dynarch_popupCalendar = cal;                  // remember it in the global var
    cal.setRange(1900, 2070);        // min/max year allowed.
    cal.create();
  }
  _dynarch_popupCalendar.setDateFormat(format);    // set the specified date format
  _dynarch_popupCalendar.parseDate(el.value);      // try to parse the text in field
  _dynarch_popupCalendar.sel = el;                 // inform it what input field we use

  // the reference element that we pass to showAtElement is the button that
  // triggers the calendar.  In this example we align the calendar bottom-right
  // to the button.
  _dynarch_popupCalendar.showAtElement(el.nextSibling, "Br");        // show the calendar

  return false;
}

var MINUTE = 60 * 1000;
var HOUR = 60 * MINUTE;
var DAY = 24 * HOUR;
var WEEK = 7 * DAY;

// If this handler returns true then the "date" given as
// parameter will be disabled.  In this example we enable
// only days within a range of 10 days from the current
// date.
// You can use the functions date.getFullYear() -- returns the year
// as 4 digit number, date.getMonth() -- returns the month as 0..11,
// and date.getDate() -- returns the date of the month as 1..31, to
// make heavy calculations here.  However, beware that this function
// should be very fast, as it is called for each day in a month when
// the calendar is (re)constructed.
function isDisabled(date) {
  var today = new Date();
  return (Math.abs(date.getTime() - today.getTime()) / DAY) > 10;
}

function flatSelected(cal, date) {
  var el = document.getElementById("preview");
  el.innerHTML = date;
}

function showFlatCalendar() {
  var parent = document.getElementById("display");

  // construct a calendar giving only the "selected" handler.
  var cal = new Calendar(0, null, flatSelected);

  // hide week numbers
  cal.weekNumbers = false;

  // We want some dates to be disabled; see function isDisabled above
  cal.setDisabledHandler(isDisabled);
  cal.setDateFormat("%A, %B %e");

  // this call must be the last as it might use data initialized above; if
  // we specify a parent, as opposite to the "showCalendar" function above,
  // then we create a flat calendar -- not popup.  Hidden, though, but...
  cal.create(parent);

  // ... we can show it here.
  cal.show();
  

}

</script>

  
  <link href="../plug/jquery-ui.css" rel="stylesheet" type="text/css"/>
  <script src="../plug/jquery.min.js"></script>
  <script src="../plug/jquery-ui.js"></script>
  

<!--  <style type="text/css">
   <!-- #drag ,#drag1 ,#drag2 ,#drag3,#drag4,#drag5,#drag6,#drag7,#drag8,#drag9,#drag10 { font-size:11px;color:#0000CC;   width: 150px; height: 18px; background-color:#F9C7FA;cursor: pointer;opacity:0.6 }
  </style>-->

	
  <script>
  
  var mainId = 0;
  
	
  
  function checkPosition(obj)
  {
  	var initialTop = document.getElementById("divInitial").style.top;
	//alert(initialTop);
  	//alert(obj.style.left);
	//obj.style.top = '197px';
  }
 function move()
 {
 	var T = this.style.top;
	var L = this.style.left;
	
 }
 
 	var i = 0;
	

 
 
 
 function createObject_old1(obj)
 {

 	var style = obj.cells[2].innerHTML;
 	var newdiv = document.createElement('div');
	var nDrag = 'drag'+(++i);
	
	
	 //popupbox.id = "popupLayer" + zindex;
     newdiv.style.position = 'relative';
	 //newdiv.style.position = 'absolute';
    // popupbox.style.zIndex = zindex;
     newdiv.style.left = 0 + 'px';
     newdiv.style.top = 0 + 'px';  
	
   newdiv.setAttribute('id',nDrag);
  
	//newdiv.setAttribute('name',nextDrag);
    //onmousedown="grab(document.getElementById('frmOrderBook'),event);"
	//newdiv.onmousedown = "grab(document.getElementById('drag'),event);";
  var htmltext = 	 "<table style=\"background-color:#D6E7F5;text-align:center;border-style:solid; border-width:1px;border-color:#0376bf; width=\"100px\" class=\"cursercross\" onMouseDown=\"grab(document.getElementById('"+nDrag+"'),event);\">"+
			 " <tr >"+
			"	<td colspan=\"5\" align=\"center\">"+style+"</td>"+
			"  </tr>"+
			"  </table>";
			

					
	newdiv.innerHTML =	htmltext;			
	newdiv.style.zIndex	 = 1;
	
	 //newdiv.style.position = 'absolute';
	//newdiv.onclick = "alert(123)";
	//newdiv.setAttribute('classname','dragableElement');
	//newdiv.setAttribute("class", "dragableElement");
	newdiv.onclick = objectclick;
	//newdiv.style.position='absolute';
  	document.getElementById('divInitial').appendChild(newdiv);
	
	inc("inc.js");
 }
 
/* function inc(filename)
{
	var body = document.getElementsByTagName('body').item(0);
	script = document.createElement('script');
	script.src = filename;
	script.type = 'text/javascript';
	body.appendChild(script)
}*/

function createObject1(obj)
{

	var style = obj.cells[1].innerHTML+" - "+obj.cells[2].innerHTML;
	  $(document).ready(function() {
	  		 $("#drag"+(mainId++)).draggable();
			 $(style).draggable();
	  });
  	
  	var newdiv = document.createElement('div');
  	newdiv.setAttribute('id','drag'+mainId);
	newdiv.onmousemove = move;
  	newdiv.innerHTML = style;	
	//newdiv.style.top = '-190px';
	//alert(newdiv.style.top);
  	document.getElementById('boxdiv2').appendChild(newdiv);

}

  </script>
  <script type="text/javascript" src="popupMenu.js"></script>
  
  
</head>

<body oncontextmenu="return false;">

<div id="menudiv" style="position:absolute;display:none;top:0px;left:0px;z-index:500;" onmouseover="javascript:overpopupmenu=true;" onmouseout="javascript:overpopupmenu=false;">
<table width=150 cellspacing=1 cellpadding=0 bgcolor=lightgray>
  <tr><td>
    <table bgcolor="" width=150 cellspacing=0 cellpadding=0>
      <tr>
        <td id="item1_splitQty" width="180" height="20" onMouseOver="this.style.backgroundColor='#D5EDFF'" onMouseOut="this.style.backgroundColor='#FFFFFF'" class="onstripmenu" onclick="checkSplit();"><b>Split Qty</b></td>
      </tr>
      <tr>
        <td id="item2_applyCurve" width="118" height="16" onMouseOver="this.style.backgroundColor='#D5EDFF'" onMouseOut="this.style.backgroundColor='#FFFFFF'" class="onstripmenu"  onclick="checkLearning();"><b>Apply Learning Curve</b></td>
      </tr>
	  <tr>
		<td id="item3_remove"  width="118" height="16" onMouseOver="this.style.backgroundColor='#D5EDFF'" onMouseOut="this.style.backgroundColor='#FFFFFF'" class="onstripmenu"   onclick="checkRemove();"><b>Remove Strip</b></td>
	  </tr>
	   <tr>
		<td id="item4_join"  width="118" height="16" onMouseOver="this.style.backgroundColor='#D5EDFF'" onMouseOut="this.style.backgroundColor='#FFFFFF'" class="onstripmenu"   onclick="createJoinStripPopUp();"><b>Join Strip</b></td>
	  </tr>
	 <!-- <tr>
		<td id="item4" bgcolor="#ffffff" width="118" height="16" onMouseOver="this.style.backgroundColor='#EFEFEF'" onMouseOut="this.style.backgroundColor='#FFFFFF'" class="seversion">Item4</td>
	  </tr>-->
    </table>
  </td></tr>
</table>
</div>

<div id="menudiv1" style="position:absolute;display:none;top:0px;left:0px;z-index:500;" onmouseover="javascript:overpopupmenu=true;" onmouseout="javascript:overpopupmenu=false;">
<table width=150 cellspacing=1 cellpadding=0 bgcolor=lightgray >
  <tr><td>
    <table bgcolor="" width=150 cellspacing=0 cellpadding=0 id="tbl_menuDiv1" >
      <tr>
        <td id="item1" width="180" height="20" onMouseOver="this.style.backgroundColor='#D5EDFF'" onMouseOut="this.style.backgroundColor='#FFFFFF'" class="onstripmenu" onclick="dayTimeChange(this);"><b>Change Working Hours</b></td>
      </tr>
      <tr>
        <td id="item2" width="118" height="16" ><b> </b></td>
      </tr>
	  <tr>
		<td id="item3"  width="118" height="16"  ><b> </b></td>
	  </tr>
	   <tr>
		<td id="item4"  width="118" height="16" ><b> </b></td>
	  </tr>
	 <!-- <tr>
		<td id="item4" bgcolor="#ffffff" width="118" height="16" onMouseOver="this.style.backgroundColor='#EFEFEF'" onMouseOut="this.style.backgroundColor='#FFFFFF'" class="seversion">Item4</td>
	  </tr>-->
    </table>
  </td></tr>
</table>
</div>
    


<div id="footer">
    <table width="101%"  height="48" cellpadding="0" cellspacing="0" bgcolor="" class="tablezRED">
      <!--<tr>
        <td height="29" colspan="8"><div >
    <table id="tblFooter2"  width="100%" cellpadding="0" cellspacing="0" bgcolor="" class="tablezRED">
      <tr>
        <td height="29">&nbsp;</td>
        <td height="29">&nbsp;</td>
        <td height="29">&nbsp;</td>
        <td height="29">&nbsp;</td>
        <td height="29">&nbsp;</td>
        <td height="29">&nbsp;</td>
        <td height="29">&nbsp;</td>
        <td height="29">&nbsp;</td>
      </tr>
      <tr>
        <td width="6%" height="16">&nbsp;</td>
        <td width="7%">&nbsp;</td>
        <td width="7%">&nbsp;</td>
        <td width="7%">&nbsp;</td>
        <td width="9%"><a href="../../main.php"></a></td>
        <td width="10%">&nbsp;</td>
        <td width="10%">&nbsp;</td>
        <td width="23%">&nbsp;</td>
      </tr>
    </table>

</div>
</td>
      </tr>-->
      <tr>
        <td width="6%" height="22">&nbsp;</td>
        <td width="7%">&nbsp;</td>
        <td width="7%">&nbsp;</td>
        <td width="7%">&nbsp;</td>
        <td width="9%">&nbsp;</td>
        <td width="10%">&nbsp;</td>
        <td width="10%">&nbsp;</td>
        <td width="23%">&nbsp;</td>
      </tr>
      <tr>
        <td height="24" colspan="8" align="center" valign="top"><img  src="../images/orderbook.png" alt="order Book" name="butOrderBook" class="mouseover" id="butOrderBook" onclick="loadOrderBook();" /><img src="../images/calender.png" alt="calender" name="butCalender" class="mouseover" id="butCalender" onclick="loadCalenderWindow();" /><img src="../images/teams.png" alt="calender" name="butCalender" class="mouseover" id="butCalender" onclick="loadTeamsWindow();" /><img src="../images/learningcurve.png" alt="calender" name="butCalender" width="171" height="24" class="mouseover" id="btnLearningCurve" onclick="loadLearningCurve();" /><img src="../images/save.png" alt="save" name="butSave" width="84" height="24" class="mouseover" id="butSave" onclick="savePlanningDetails();" /><img src="../images/report.png" name="butReport" width="108" height="24" class="mouseover" id="butReport" onclick="grnReport();"/><a href="../../main.php"><img src="../images/close.png" width="97" height="24" border="0" class="mouseover" /></a></td>
      </tr>
  </table>

</div>	


<!--<div onmouseup="checkPosition(this);" id="draggable3" >Style 2</div>-->

<?php
	include "../../../../dataAccess/Connector.php";
?>


<form name="frmbom" id="frmbom" action="grndetails.php" method="post" >

<table id="tblPboard" width="1107" height="200" border="0" align="center" bgcolor="#FFFFFF">
<tr style="background:#660000">
    <td class="mbari9" height="30" >Planning Board</td>
</tr>
  <tr>
    <td></td>
  </tr>
  <tr>
    <td><table  width="100%" border="0" id="tblInfomation">
      <tr>
        <td width="100%" height="0"><table style="display:" id="tblStrip" width="100%" border="0" cellpadding="1" cellspacing="0" class="bcgl1" >

          <tr>
            <td height="18" width="8%"  class="grid_header">Style</td>
            <td width="10%" class="seversion2"><span class="seversion" id="stripStyle"></span></td>
            <td width="9%" class="grid_header" onclick="createObject(this);">Order No </td>
            <td width="11%" class="seversion2"><span id="stripDescription">&nbsp;</span></td>
            <td width="11%" class="grid_header">Start</td>
            <td width="13%" class="seversion2"><span id="stripStart">&nbsp;</span></td>
            <td width="10%" class="grid_header">ExFactory</td>
            <td width="12%" class="normalfnt"><span id="stripExFactory">&nbsp;</span></td>
            <td width="9%" class="grid_header">Style Status </td>
            <td width="7%" class="seversion2"><span id="stripStyleStatus">&nbsp;</span></td>
          </tr>
          <tr>
            <td  height="18" class="grid_header">Qty</td>
            <td class="seversion2"><span id="stripQty">&nbsp;</span></td>
            <td class="grid_header">Team </td>
            <td class="seversion2"><span id="stripTeam">&nbsp;</span></td>
            <td class="grid_header">End</td>
            <td class="seversion2"><span id="stripEnd">&nbsp;</span></td>
            <td class="grid_header">Cut Date </td>
            <td class="normalfnt"><span id="stripCutDate">&nbsp;</span></td>
            <td class="grid_header">Machine Op </td>
            <td class="seversion2"><span id="stripMachineOp">&nbsp;</span></td>
          </tr>
          <tr>
            <td height="18"  class="grid_header">Strip No </td>
            <td  class="seversion2"><span class="" id="stripNo">&nbsp;</span></td>
            <td class="grid_header">SMV</td>
            <td class="seversion2"><span id="stripSmv">&nbsp;</span></td>
            <td class="grid_header">Curve</td>
            <td class="seversion2"><span id="stripCurve">&nbsp;</span></td>
            <td class="grid_header">Completed Qty</td>
            <td class="seversion2"><span id="stripCompletedQty">&nbsp;</span></td>
            <td class="grid_header">&nbsp;</td>
            <td class="seversion2"></td>
          </tr>
        </table>
          <table style="display:none"    id="tblDay" width="100%" border="0" cellpadding="1" cellspacing="0" class="bcgl1" >
          <tr>
            <td height="18" width="8%"  class="grid_header">Date</td>
            <td width="10%" class="seversion2"><span class="seversion" id="dayDate"></span></td>
            <td width="9%" class="grid_header" onclick="createObject(this);">Is Active </td>
            <td width="11%" class="seversion2"><span id="dayActive">&nbsp;</span></td>
            <td width="11%" class="grid_header">Working Hours </td>
            <td width="13%" class="seversion2"><span id="dayWorkingHours">&nbsp;</span></td>
            <td width="10%" class="grid_header">Machines</td>
            <td width="12%" class="seversion2"><span id="dayMachines">&nbsp;</span></td>
            <td width="9%" class="grid_header">&nbsp;</td>
            <td width="7%" class="seversion2"><span id="">&nbsp;</span></td>
          </tr>
          <tr>
            <td  height="18" class="grid_header">Team</td>
            <td class="seversion2"><span id="dayTeam">&nbsp;</span></td>
            <td class="grid_header">Start Time </td>
            <td class="seversion2"><span id="dayStartTime">&nbsp;</span></td>
            <td class="grid_header">Day</td>
            <td class="seversion2"><span id="dayDay">&nbsp;</span></td>
            <td class="grid_header">Efficency</td>
            <td class="seversion2"><span id="dayEfficency">&nbsp;</span></td>
            <td class="grid_header">&nbsp;</td>
            <td class="seversion2"><span id="">&nbsp;</span></td>
          </tr>
          <tr>
            <td height="18"  class="grid_header">Day Type </td>
            <td  class="seversion2"><span class="" id="dayType">&nbsp;</span></td>
            <td class="grid_header">End Time </td>
            <td class="seversion2"><span id="dayEndTime">&nbsp;</span></td>
            <td class="grid_header">&nbsp;</td>
            <td class="seversion2"><span id="">&nbsp;</span></td>
            <td class="grid_header">&nbsp;</td>
            <td class="seversion2"><span id="">&nbsp;</span></td>
            <td class="grid_header">&nbsp;</td>
            <td class="seversion2"></td>
          </tr>
        </table></td>
        </tr>
    </table></td>
  </tr>
 
  <tr><td >&nbsp;</td></tr>
  <style type="text/css">
	#boxdiv1 div{
		float: left;
		margin: 0px;
	}

	#divInitial div{
		float: left;
		margin: 0px;
	}

	.classTeam div{
		float: left;
		margin: 0px;
		border-top: 0px;
		border-left: 1px solid #B4B4B4;
		border-right: 1px solid ;
		border-bottom: 1px ;
	}
	.classGridBox div{

		border-top: 0px;
		border-left: 1px solid #B4B4B4;
		border-right: 1px solid ;
		border-bottom: 1px ;


	}
</style> 
  <tr>
    <td><table height="362" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td  class="normalfnt"><div id="myDiv" style="overflow:scroll; height:350px; width:1200px;">
          <table width="100000"  border="1" cellpadding="0" cellspacing="1" bordercolor="#162350"  id="tblMain">
            <tr>
            	<td  height="18" class="normaltxtmidb2">&nbsp;</td>
				<td  class="normaltxtmidb2" >
				<div id="boxdiv1">
            	<?php
				
				//$last_month_time = mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")); 
				//$last_month = date('F', $last_month_time);
				
				/// LOADING CALENDER
				$this_month=date('m');
				$chk_date=$year.'-'.$this_month.'-01';
			   $sql_date="select distinct dtmdate from plan_calender where intCompanyId=$intPubCompanyId
							AND dtmDate<'$chk_date'";
				$result_date=$db->RunQuery($sql_date);
	
				if(mysqli_num_rows($result_date)==0)
					$last_month=$year.'-'.$this_month.'-'.$day;

				
				  
					$sql1="select distinct dtmdate,intMonth,intDay,strDayStatus,strDay from plan_calender where intCompanyId=$intPubCompanyId AND dtmDate>='$last_month'";
					
					
					$result1 = $db->RunQuery($sql1);
					$i=0;
					while($row1=mysqli_fetch_array($result1))
					{		
						if($row1['dtmdate']==$mem_date)
							continue;
						
						$mem_date = $row1['dtmdate'];
						
						if($row1["strDay"]=='Saturday'){
							$i++;
				?>
							<div class="tablezREDMid2"   id="divInitialQQ" style="width:40px; height:15px;background-color:#A7E05A;"><?php echo $row1["intDay"]."/".$row1["intMonth"]; ?></div>
							
                <?php
						}
						else if($row1["strDay"]=='Sunday'){
							$i++;
				?>
							
							<div class="tablezREDMid1" id="divInitialQQ" style="width:40px; height:15px;background-color:#FCCA6D"><?php echo $row1["intDay"]."/".$row1["intMonth"]; ?></div>
                <?php
						}
						else {
							$i++;
				?>
							
							<div  class="tablezREDMid"  id="divInitialQQ" style="width:40px; height:15px; border:"><?php echo $row1["intDay"]."/".$row1["intMonth"]; ?></div>
						
                <?php
						}
					}
					
					
				?>
				<script type="text/javascript">
					var pub_calender_daycount = <?php echo $i; ?>;
				</script>
			</div>
			</td>
            </tr>              
             
    <tr >
    	<td   height="15" class="normalfnth2"><span id="divInitialCaption">INITIAL</span></td>
		<td class="normaltxtmidb2" bgcolor="">
			<div class="classTeam" id="divInitial" style="width:100%"  >&nbsp;</div>
		</td>
    
 <?php
 
 	////////////////// 	 LOADING MAIN TEAM CALENDER    /////////////////////////////////
 	$sql = "SELECT * FROM plan_teams where intSubTeamOf=0 order by intOrderNo  ";
	$result = $db->RunQuery($sql);
	$n=2;
	while($row=mysqli_fetch_array($result))
	{
		++$n;
		$teamId = $row["intTeamNo"];
		$intSubTeamOf = $row['intSubTeamOf'];
?>
      <tr>
	   <td   width="71"  height="17" class="normalfnt2BITAB"><?php echo $row["strTeam"]; ?></td>
	   

		
		<td  class="normaltxtmidb2">
			<div class="classTeam"  id="team<?php echo $teamId; ?>" >
				<?php
					$sql2="select strDayStatus,intDay,dtmDate,dblWorkingHours from plan_calender where intCompanyId=$intPubCompanyId and intTeamId=$teamId AND dtmDate>='$last_month'";
					$result2 = $db->RunQuery($sql2);
					while($row2=mysqli_fetch_array($result2))
					{	
						$dtmDate = $row2["dtmDate"];
						$intDay = $row2["intDay"];
						$strDayStatus = $row2["strDayStatus"];
						$dblWorkingHours = $row2["dblWorkingHours"];
						
						
						///calculate minuts
						$H1 = floor($dblWorkingHours);
						$M1 = ($H1*60)+ (($dblWorkingHours-$H1)*100);
						//$intMin = $M1;
						$intMin = '';
						///end of calculate minuste
						
						$sql_1="
								SELECT
								plan_teamsvaliddates.intTeamId
								FROM `plan_teamsvaliddates`
								WHERE
								(plan_teamsvaliddates.dtmValidFrom <=  '$dtmDate' AND
								plan_teamsvaliddates.dtmValidTo >=  '$dtmDate') AND 
								intCompanyId=$intPubCompanyId and intTeamId=$teamId;";
								
						$result_1 = $db->RunQuery($sql_1);
						$count = mysqli_num_rows($result_1);
						
						if($count<=0)
						{
			echo "<div class=\"classGridBox\" name=\"inActive\"  id=\"$dtmDate\" style=\"width:40px;opacity:1; height:100%;background-color:#EBEBEB\">&nbsp;</div>";
							continue;
						}
						
					
					
					
						
						
			if($strDayStatus=='work')
			{
				echo "<div class=\"classGridBox\" oncontextmenu=\"ItemSelMenu1(event,this);\"  id=\"$dtmDate\" style=\"width:40px; height:100%;background-color:#FFFFFF\">$intMin</div>";
			}
			else if($strDayStatus=='saturday')
			{
				echo "<div class=\"classGridBox\" oncontextmenu=\"ItemSelMenu1(event,this);\"   id=\"$dtmDate\" style=\"width:40px;opacity:0.2; height:100%;background-color:#A7E05A\">$intMin</div>";
			}
			else if($strDayStatus=='sunday')
			{
				echo "<div class=\"classGridBox\" oncontextmenu=\"ItemSelMenu1(event,this);\"    id=\"$dtmDate\" style=\"width:40px;opacity:0.2; height:100%;background-color:#FCCA6D\">$intMin</div>";
			}
			else if($strDayStatus=='off')
			{
				echo "<div class=\"classGridBox\" oncontextmenu=\"ItemSelMenu1(event,this);\"    id=\"$dtmDate\" style=\"width:40px;opacity:0.2; height:100%;background-color:red\">$intMin</div>";
			}
					}
				?>
			</div>
		</td>
      </tr>
	  
	  	    <?php 
			
			/////////////////////////////             LOADING SUB TEAM CALENDER              /////////////////////////////////////
			
		$sql1 = "SELECT * FROM plan_teams where intSubTeamOf=$teamId order by intOrderNo  ";
		$result1 = $db->RunQuery($sql1);
		while($row1=mysqli_fetch_array($result1))
		{
		 	 $teamId2 = $row1["intTeamNo"];
			
		?>
		<tr>
        <td align="right"  width="71"  height="17"  ><div   style=" background-color:#D3F8DC;text-align:center;height:100%;width:60px;font-style:italic;font-size:10px;color:#990000"><b><?php echo $row1["strTeam"]; ?></b></div></td>
		
		<td  class="normaltxtmidb2">
			<div class="classTeam"    id="team<?php echo $teamId2; ?>" >
				<?php
					$sql3="select strDayStatus,intDay,dtmDate,dblWorkingHours from plan_calender where intCompanyId=$intPubCompanyId and intTeamId=$teamId2 AND dtmDate>='$last_month';";
					$result3 = $db->RunQuery($sql3);
					while($row3=mysqli_fetch_array($result3))
					{	
						$dtmDate3 = $row3["dtmDate"];
						$intDay3 = $row3["intDay"];
						$strDayStatus3 = $row3["strDayStatus"];
						$dblWorkingHours = $row3["dblWorkingHours"];
						
						//calculate minuts
						$H1 = floor($dblWorkingHours);
						$M1 = ($H1*60)+ (($dblWorkingHours-$H1)*100);
						//$intMin = $M1;
						$intMin = '';
						///end of calculate minuste
						$sql_1="
								SELECT
								plan_teamsvaliddates.intTeamId
								FROM `plan_teamsvaliddates`
								WHERE
								(plan_teamsvaliddates.dtmValidFrom <=  '$dtmDate3' AND
								plan_teamsvaliddates.dtmValidTo >=  '$dtmDate3') AND 
								intCompanyId=$intPubCompanyId and intTeamId=$teamId2;";
								
						$result_1 = $db->RunQuery($sql_1);
						//$count = mysqli_num_rows($result_1);
						
						/*if($count<=0)
						{
			echo "<div class=\"classGridBox\" name=\"inActive\"   id=\"$dtmDate3\" style=\"width:40px;opacity:1; height:100%;background-color:#EBEBEB\">&nbsp;</div>";
							//continue;
						}*/
					
					
					
					
					
					
					
			if($strDayStatus3=='work')
			{
				echo "<div class=\"classGridBox\"  oncontextmenu=\"ItemSelMenu1(event,this);\"   id=\"$dtmDate3\" style=\"width:40px; height:100%;background-color:#FFFFFF\">$intMin</div>";
			}
			elseif($strDayStatus3=='saturday')
			{
				echo "<div class=\"classGridBox\"  oncontextmenu=\"ItemSelMenu1(event,this);\"   id=\"$dtmDate3\" style=\"width:40px;opacity:0.2; height:100%;background-color:#A7E05A\">$intMin</div>";
			}
			elseif($strDayStatus3=='sunday')
			{
				echo "<div class=\"classGridBox\"  oncontextmenu=\"ItemSelMenu1(event,this);\"   id=\"$dtmDate3\" style=\"width:40px;opacity:0.2; height:100%;background-color:#FCCA6D\">$intMin</div>";
			}
			elseif($strDayStatus3=='off')
			{
				echo "<div class=\"classGridBox\"  oncontextmenu=\"ItemSelMenu1(event,this);\"   id=\"$dtmDate3\" style=\"width:40px;opacity:0.2; height:100%;background-color:red\">$intMin</div>";
			}

					}
				?>
			</div>
		</td>
      </tr>
		<?php 
		}
		?>
	  
	  
 <?php
 
 		
 	}
 ?>
  

  </tr>

          </table>
	    </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
	<div id="split_qty">
		<table width="100%" cellpadding="0" cellspacing="0" bgcolor="" class="tablezRED" id="splitQty">
      <!--<tr>
        <td width="6%" height="29" class="normalfntLeftBlue">&nbsp;</td>
        <td width="7%" height="29" class="normalfntLeftBlue">Split Qty</td>
        <td width="7%"><input type="text" /></td>
        <td></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>-->
    </table>
	</div>
  </tr>

</table>
<script type="text/javascript">


<?php 
	$sql = "SELECT
			plan_learningcurveheader.strCurveName AS curveName,
			orders.intStyleId AS orderStyleId,
			plan_stripes.intID,
			plan_stripes.strStyleID,
			orders.strOrderNo,
			orders.strStyle AS orderStyle,
			plan_stripes.smv,
			plan_stripes.intTeamNo,
			plan_stripes.intLearningCuveId,
			plan_stripes.dblLeftPercent,
			plan_stripes.startDate,
			plan_stripes.startTime,
			plan_stripes.startTimeLeft,
			plan_stripes.endDate,
			plan_stripes.endTime,
			plan_stripes.totalHours,
			plan_stripes.dblQty,
			plan_stripes.dblActQty,
			((hour(plan_stripes.startTime)*60) +minute(plan_stripes.startTime)) AS startTotalMin,
			((hour(plan_stripes.endTime)*60) +minute(plan_stripes.endTime)) AS lastDayMin
			FROM
			plan_stripes
			INNER JOIN orders ON plan_stripes.strStyleID = orders.intStyleId
			LEFT JOIN plan_learningcurveheader ON plan_learningcurveheader.id=plan_stripes.intLearningCuveId
			WHERE plan_stripes.endDate >='$last_month'";
	$result = $db->RunQuery($sql);
	$maxV = 0;
	
	while($row=mysqli_fetch_array($result))
	{
		$stripId =  $row['intID'];
		if($maxV<(int)$stripId)
		{
			$maxV = (int)$stripId;
		}
?>	
	//STRIP DETAILS
	
	var arrProp = [];
	var stripId					=		<?php echo $row['intID']; ?>;
	arrProp['id']				=		stripId;
	arrProp['orderStyleId']		=		<?php echo $row['orderStyleId']; ?>;
	arrProp['orderNo']			=		'<?php echo $row['strOrderNo']; ?>';
	arrProp['orderStyle']		=		'<?php echo $row['orderStyle']; ?>';
	arrProp['smv']				=		<?php echo $row['smv']; ?>;
	arrProp['style']			=		'<?php echo $row['strStyleID']; ?>';
	arrProp['qty']				=		'<?php echo $row['dblQty']; ?>';
	arrProp['actQty']			=		'<?php echo $row['dblActQty']; ?>';
	arrProp['new']				=		'0';
	arrProp['totalHours']		=		<?php echo $row['totalHours']; ?>;

	arrProp['startDate']	    =	 '<?php echo $row['startDate']; ?>';

	
	arrProp['startTime']	=		'<?php echo $row['startTime']; ?>';
	arrProp['team']			=		<?php echo $row['intTeamNo']; ?>;
	arrProp['curve']		=		<?php echo $row['intLearningCuveId']; ?>;
	arrProp['curveName']	= 		'<?php echo $row['curveName']; ?>';
	arrProp['endDate']		=		'<?php echo $row['endDate']; ?>';
	arrProp['endTime']		=		'<?php echo $row['endTime']; ?>';
	//arrProp['startTotalMin']=		'<?php echo $row['startTotalMin']; ?>';
	arrProp['lastDayMin']=		'<?php echo $row['lastDayMin']; ?>';
	arrProp['cellLeft']=		'<?php echo $row['dblLeftPercent']; ?>';
	arrProp['removeStatus']	=	0;
	
	//arrProp['joinStripCnt']	= 0;
	arrStrip[stripId]=arrProp;
	arrProp = null;
	
	i = <?php echo $maxV; ?>;
<?php
	}
?>
	
	/////////////////////////// START FOR TEAM VALUES LOADING.... ///////////////////////////////
	
	
	<?php
		$sql = "select * from plan_teams";
		$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		
	?>
		//TEAM DETAILS
		
		var arrT			=	[];
		arrT['id'] 			=	 <?php echo $row['intTeamNo']; 		?>;
		arrT['strTeam'] 	=	 '<?php echo $row['strTeam']; 		?>';
		arrT['efficency']	=	 <?php echo $row['intEfficency']; 	?>;
		arrT['machines']	=	 <?php echo $row['intMachines']; 	?>;
		arrT['totalStripWidth']  = 0;
		
		arrTeam[arrT['id']]	=	arrT;
	<?php
	}
	?>
	/////////////////////////////////////////////////////////////////////////////////////////////////
			
		var arrM =[];
		var mem_serial ='' ;
		var mem_date = '';
	/////////////////////////// START FOR CALENDER DATA LOADING ....//////////////////////////////
		<?php
		$sql = "select *,concat(year(dtmDate) ,lpad(month(dtmDate),2,'0') ,lpad(day(dtmDate),2,'0')) as dtSerial 
				from plan_calender where intCompanyId=$intPubCompanyId order by dtmDate ";
		$result = $db->RunQuery($sql);
		//echo $sql;
	$newDate = 0;
	
	while($row=mysqli_fetch_array($result))
	{

		$intTeamId = $row['intTeamId'];
		$dtSerial = $row['dtSerial'];
		
		
		$dblWorkingHours = $row["dblWorkingHours"];
		//calculate minuts
		$H1 = floor($dblWorkingHours);
		$M1 = ($H1*60)+ (($dblWorkingHours-$H1)*100);
		$wokingMInutes = $M1;
		///end of calculate minuste
		
		//$arrCA[$row['intMachines']] = $row['dtmDate'];
		//$arrCAA[$intTeamId] = $arrCA;
	?>
		var dtSerial = <?php echo $dtSerial; ?>;
		var date = '<?php echo $row['dtmDate']; ?>';
		
		if(dtSerial !=mem_serial)
		{
			arrCalender[mem_date] = arrM;
			arrM = [];
		}
		
	
		var team = <?php echo $intTeamId; ?>;
		
		//CALENDER DETAILS
		
		var arrC =[];
		
		arrC['date'] 					=	 '<?php echo $row['dtmDate']; ?>';
		arrC['teamId']					=	 <?php echo $row['intTeamId']; 	?>;
		arrC['workingMinutes']			=	 <?php echo $wokingMInutes; 	?>;
		arrC['workingHours']			=	 <?php echo $row['dblWorkingHours']; 	?>;
		
		arrC['machines']				=	 <?php echo $row['intMachines']; 	?>;
		arrC['efficency']				=	 <?php echo $row['intTeamEfficency']; 	?>;
		arrC['DayStatus']				=	 '<?php echo $row['strDayStatus']; 	?>';
		
		arrC['year']					=	 '<?php echo $row['intYear']; 	?>';
		arrC['month']					=	 '<?php echo $row['intMonth']; 	?>';
		arrC['day']						=	 '<?php echo $row['intDay']; 	?>';
		arrC['strDay']					=	 '<?php echo $row['strDay']; 	?>';
		
		arrC['startTime']				=	 <?php echo $row['dblStartTime']; 	?>;
		arrC['endTime']					=	 <?php echo $row['dblEndTime']; 	?>;
		//alert(arrC['date']);
		arrM[team] = arrC;
		
		mem_serial = dtSerial;
		mem_date = date ; 
		
	<?php
	}
		
	//print_r($arrCA);
	?>
	arrCalender[dtSerial] = arrM;
	
	///////////////////////////////Array for Curves/////////////////////////////////////////
	
	
	<?php
		$sql = "SELECT * FROM plan_learningcurveheader";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
		$curveId=$row['id'];
	?>
		
	<?php		
			$sql1="SELECT dblEfficency FROM plan_learningcurvedetails WHERE id=$curveId";
			$effi="";
	
			$result1 = $db->RunQuery($sql1);
	
			while($row1=mysqli_fetch_array($result1))
			{
				$effi.=$row1['dblEfficency'];
				$effi.=",";
			}
	?>
		var arrCu=[];
		arrCu['id']= <?php echo $row['id'];?>;
		
		arrCu['curveName']='<?php echo $row['strCurveName']; ?>';
		arrCu['efficiency']= '<?php echo $effi ;?>'; 
		arrCurve[arrCu['id']]=arrCu;
		<?php
		}
		?>
		
		<?php
			$this_month1=date('m');
				$chk_date1=$year.'-'.$this_month1.'-01';
				$monthExist=1;
			   $sql_date1="select distinct dtmdate from plan_calender where intCompanyId=$intPubCompanyId
							AND dtmDate<'$chk_date1'";
				$result_date1=$db->RunQuery($sql_date1);
	
				if(mysqli_num_rows($result_date1)==0)
					$monthExist=0;
 
		?>
		var calenderStatus=[];
		calenderStatus['status']=<?php echo $monthExist; ?>;
		arrcalenderStatus[1]=calenderStatus;
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	createAllStrips();
	
</script>
</form>

	<script type="text/javascript">
	function abc()
	{
		//document.getElementById('myDiv').style.height = (screen.height - 420)+'px';
	}
</script>
<ul style="display: none; visibility: visible;" id="contextmenu2" class="jqcontextmenu">
<li><a href="#">Item 1a</a></li>
<li><a href="#">Item 2a</a></li>
<li><a href="#">Item 1a</a></li>
<li><a href="#">Item 2a</a></li>
</ul>

</body>

</html>



<?php
include "../../../../dataAccess/Connector.php";


$id  = $_GET["id"];

if($id=='getPreDate')
{
	$this_month=date('m');
	$chk_date=date('Y').'-'.$this_month.'-01';
	$sql_date="select distinct dtmdate from plan_calender where intCompanyId=$intPubCompanyId AND dtmDate<'$chk_date'";
	$result_date=$db->RunQuery($sql_date);
	
	if(mysqli_num_rows($result_date)==0)
		echo 0;
			
}

if($id=='getStripWidth')
{
	$teamId 	= $_GET["teamId"];
	$style		= $_GET["style"];
	
		$sql = "SELECT plan_teams.intEfficency FROM `plan_teams` WHERE plan_teams.intTeamNo =  '$teamId'";
		$result = $db->RunQuery($sql);
		//echo $sql;
		while($row=mysqli_fetch_array($result))
		{
			$efficency =  $row["intEfficency"];
		}
		
		$sql = "select reaSMV from orders where strStyle='$style'";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$smv =  $row["reaSMV"];
		}
		
		
}

if($id=='getOrderStyle')
{
	$styleId=$_GET['styleId'];
	$sql = "SELECT strStyle FROM orders where intStyleId=$styleId";
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	echo $row['strStyle'];
}

if($id=='savePlanDetails')
{
		$style			=	$_GET['style'];
		$smv			=	(float)$_GET['smv'];
		$stripId		=	$_GET['stripId'];
		$new			=	$_GET['new'];
		$qty			=	$_GET['qty'];
		$actQty			=	$_GET['actQty'];
		$startDate		=	$_GET['startDate'];
		$startTime		=	$_GET['startTime'];
		$endDate		=	$_GET['endDate'];
		$endTime		=	$_GET['endTime'];
		
		$team			=	$_GET['team'];
		$curve			=	$_GET['curve'];
		$totalHours		=	$_GET['totalHours'];
		$stripLeft		=	$_GET['stripLeft'];
		$cellLeft		=	$_GET['cellLeft'];
		
		
		if($new)
		{
			$sql = "insert into plan_stripes 
						( 
						strStyleID, 
						smv,
						intTeamNo, 
						intLearningCuveId, 
						dblLeftPercent,
						startDate, 
						startTime, 
						endDate, 
						endTime, 
						totalHours, 
						dblQty
						)
						values
						(
						'$style', 
						'$smv',
						'$team', 
						'$curve', 
						'$cellLeft',
						'$startDate', 
						'$startTime', 
						'$endDate', 
						'$endTime', 
						'$totalHours', 
						'$qty'
						);
					";
			$result = $db->RunQuery($sql);
			if(! $result)
				echo $sql;
			else
				echo 1;
				//echo "strip $stripId saved ";
		}
		else
		{
			$sql = "		update plan_stripes 
						set
							intTeamNo 			= '$team' , 
							intLearningCuveId 	= '$curve' , 
							startDate 			= '$startDate' , 
							startTime 			= '$startTime' , 
							endDate 			= '$endDate' , 
							endTime 			= '$endTime' , 
							totalHours 			= '$totalHours' , 
							dblQty 				= '$qty' , 
							dblActQty 			= '$actQty',
							dblLeftPercent		= '$cellLeft'
						where
							intID 				= '$stripId'";
							
			$result = $db->RunQuery($sql);
			if(! $result)
				echo $sql;
			else
				echo 1;
				//echo "strip $stripId Updated ";

		}
}


if($id=='deleteStrip')
{
	$activeStrip=$_GET['activeStrip'];
	/*$orderId=$_GET['orderId'];
	$styleId=$_GET['styleId'];
	$qty=$_GET['qty'];
	$smv=$_GET['smv'];
	*/
	$sql_delete="DELETE FROM plan_stripes WHERE intID='$activeStrip'";
	
	$result_delete = $db->RunQuery($sql_delete);
	
	if($result_delete)
	{	
		echo 1;
	}
	
}

if($id=='createStrip')
{
	$curveId=$_GET['curveId'];
	
	
	$sql="SELECT dblEfficency FROM plan_learningcurvedetails WHERE id='$curveId'";
	$effi="";
	
	$result = $db->RunQuery($sql);
	
	while($row=mysqli_fetch_array($result))
	{
		$effi.=$row['dblEfficency'];
		$effi.=",";
		
	}
	
	echo $effi;
	//echo "";
}

if($id=='loadLearningCurve')
{
	$curveId = $_GET['curveId'];
	
	$sql="SELECT id,strCurveName from plan_learningcurveheader";
	
	$result = $db->RunQuery($sql);
	$details = "";
	 if($curveId==0)
			$details .= "<option selected value=\"0\"></option>\n";
	while($row=mysqli_fetch_array($result))
	{
		$id1=$row['id'];
		$name=$row['strCurveName'];
		if($curveId==$id1)
			$details .= "<option selected value=\"$id1\">$name</option>\n";
		else
			$details .= "<option value=\"$id1\">$name</option>\n";
		//echo $effi;
	}
	
	echo $details;
	//echo "";
}



if($id=='disableLearningCurve')
{
	$stripId=$_GET['stripId'];
	$sql="SELECT * FROM plan_actualproduction WHERE intStripeId='$stripId'";
	$result=$db->RunQuery($sql);
	if(mysqli_num_rows($result)>0)
		echo 1;
}

if($id=='selectSewingSmv')
{
	$styleId=$_GET['styleId'];	
	$sql="SELECT dblSewwingSmv FROM orders WHERE intStyleId='$styleId'";
	
	$result = $db->RunQuery($sql);
	if(mysqli_num_rows($result)>0)
	{
	$row=mysqli_fetch_array($result);
	$sewingSmv=$row['dblSewwingSmv'];
	echo $sewingSmv;
	}
	else
	{
	$sql1="SELECT dblSewwingSmv FROM plan_newoders WHERE strStyleId='$styleId'";
	$result1 = $db->RunQuery($sql1);
	$row1=mysqli_fetch_array($result1);
	$sewingSmv1=$row1['dblSewwingSmv'];
	echo $sewingSmv1;
	}
	//echo "";
}

if($id=='getOrderNo')
{
	$styleId=$_GET['styleId'];	
	$sql="SELECT strOrderNo FROM orders WHERE intStyleId='$styleId'";
	
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	echo $row['strOrderNo'];
}

if($id=='getDate')
{
	echo date("Y-m-d",strtotime("-1 Months"));	
}

if($id=='completedQty')
{
	$style=$_GET['style'];
	$team=$_GET['team'];
	$stripId=$_GET['stripId'];
	
    $sql="SELECT SUM(dblProducedQty) AS producedQty  FROM plan_actualproduction
			WHERE intStripeId=$stripId && intTeamNo=$team && strStyleId='$style';";
	
	$result = $db->RunQuery($sql);
	 
	$row=mysqli_fetch_array($result);
	
	if($row['producedQty']=='')
		echo 0;
	else
		echo $row['producedQty'];
	

}

if($id=='checkStripNo')
{
	$stripId=$_GET['stripId'];
	$sql="SELECT intId FROM plan_stripes WHERE intID='$stripId';";
	$result = $db->RunQuery($sql);
	if(mysqli_num_rows($result)>0)
		echo 1;
	else
		echo 0;
}

if($id=='changeDayProperty')
{
	//$active_startTime = 1;
	
	$teamId=$_GET['teamId'];
	$sDate=$_GET['sDate'];
	$eDate=$_GET['eDate'];
	$startTime=$_GET['startTime'];
	$endTime=$_GET['endTime'];
	$sworkingHours=$_GET['sworkingHours'];
	$dayType=$_GET['dayType'];
	$teamEfficiency=$_GET['teamEfficiency'];
	$noOfMachines=$_GET['noOfMachines'];
	
	$actTime=$_GET['actTime'];
	$actEffi=$_GET['actEffi'];
	$actMach=$_GET['actMach'];
	
	$chkAll=$_GET['chkAll'];
	$companyId=$_GET['companyId'];
	
	
	//echo "Updated";

	
	$sql = "UPDATE plan_calender
				SET ";
	if($actTime==1)
	{
		if($actEffi==0 && $actMach==0)
		{
			$sql .=" dblStartTime='$startTime'," ;
			$sql .=" dblEndTime='$endTime'," ;
			$sql .=" dblWorkingHours='$sworkingHours'" ;	
		}
		else
		{
			$sql .=" dblStartTime='$startTime'," ;
			$sql .=" dblEndTime='$endTime'," ;
			$sql .=" dblWorkingHours='$sworkingHours'," ;
		}
	}
		
	if($actEffi==1)
	{
		if($actMach==0)
			$sql .=" intTeamEfficency='$teamEfficiency'";
		else
			$sql .=" intTeamEfficency='$teamEfficiency',";
	}
	
	if($actMach==1)
		  $sql .=" intMachines='$noOfMachines'";
		  	

	$sql .=	" where intCompanyId='$companyId' and dtmDate>='$sDate' and dtmDate<='$eDate'  ";
				
	if($chkAll==0)
		$sql.=" and intTeamId='$teamId' ";
		
	if($dayType=='Sa')
		$sql.=" and strDayStatus='saturday' ";
	else if($dayType=='Su')
		$sql.=" and strDayStatus='sunday' ";
	else if($dayType=='Ho')
		$sql.=" and strDayStatus='holiday' " ;
		
	$result = $db->RunQuery($sql);
	if(! $result)
		echo $sql;
	else
		echo "Updated Successfully";
}	
?>







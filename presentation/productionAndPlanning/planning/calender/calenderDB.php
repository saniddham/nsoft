<?php
	session_start();
	include "../../../../dataAccess/Connector.php";
	$intPubCompanyId		=$_SESSION["CompanyID"];
	
	$id=$_GET["id"];
	
	if($id=='save'){
	
		$intYear=$_GET["intYear"]; 
	    $intMonth=$_GET["intMonth"];
	
		$status=$_GET["holidays"];

		$statusArr;
		$k=0;
		$tokn = strtok($status, ",");		
			
		while ($tokn != false)
		{		
			$statusArr[$k]=$tokn;
			$tokn = strtok(",");
		
			$k++;
		}
		
		$SQL="select 	
				intTeamNo, 
				strTeam, 
				intCompanyId, 
				intOrderNo, 
				intOperators, 
				intMachines, 
				intEfficency, 
				intHelper, 
				dblWorkingHours, 
				intSubTeamOf, 
				intSubOrderNo, 
				dblStartTime, 
				dblEndTime
				 
				from 
				plan_teams where intCompanyId=$intPubCompanyId;";
		
		$result = $db->RunQuery($SQL);
		
		while($row = mysqli_fetch_array($result))
			{
				$intCompanyId=$row["intCompanyId"]; 
				$intTeamId=$row["intTeamNo"]; 
				$dblWorkingHours=$row["dblWorkingHours"]; 
				$dblWorkingHours_old=$row["dblWorkingHours"]; 
				$dblStartTime=$row["dblStartTime"]; 
				$dblEndTime=$row["dblEndTime"]; 
				$intMachines=$row["intMachines"]; 
	    		$intTeamEfficency=$row["intEfficency"]; 				
				//$strMonth="Month";				 	
			 	
				for($i=1;$i<32;$i++){
				
					$dtmDate=$intYear."-".$intMonth."-".$i; 
					$dayandstatus=explode('-',$statusArr[$i-1]);
					
					$strDayStatus=$dayandstatus[0]; 
			
					switch($dayandstatus[1]){
					
						case '1' :
							{
								$strDay="Monday";
								break;
							}
						case '2' :
							{
								$strDay="Tuesday";
								break;
							}
						case '3' :
							{
								$strDay="Wednesday";
								break;
							}
						case '4' :
							{
								$strDay="Thursday";
								break;
							}
						case '5' :
							{
								$strDay="Friday";
								break;
							}
						case '6' :
							{
								$strDay="Saturday";
								break;
							}
						case '0' :
							{
								$strDay="Sunday";
								break;
							}
						default :
							{
								break;
							}
					}
					
					$intDay=$i;
					
					switch($intMonth){					
						
						case '1' :
							{
								$strMonth="January";
								break;
							}
						case '2' :
							{
								$strMonth="February";
								break;
							}
						case '3' :
							{
								$strMonth="March";
								break;
							}
						case '4' :
							{
								$strMonth="April";
								break;
							}
						case '5' :
							{
								$strMonth="May";
								break;
							}
						case '6' :
							{
								$strMonth="June";
								break;
							}
						case '7' :
							{
								$strMonth="July";
								break;
							}
						case '8' :
							{
								$strMonth="August";
								break;
							}
						case '9' :
							{
								$strMonth="September";
								break;
							}
						case '10' :
							{
								$strMonth="October";
								break;
							}
						case '11' :
							{
								$strMonth="November";
								break;
							}
						case '12' :
							{
								$strMonth="December";
								break;
							}
						default :
							{
								break;
							}
					}
					
						if($strDayStatus=='off')
						{
							$dblWorkingHours=0;
						}
						elseif($strDayStatus=='saturday')
						{
							$dblWorkingHours=4;
						}
						elseif($strDayStatus=='sunday')
						{
							$dblWorkingHours=0;
						}
						else
							$dblWorkingHours=$dblWorkingHours_old;
							
							
							//check valid dates
							$sql5 = "select * from plan_teamsvaliddates where 
							dtmValidFrom<='$dtmDate' and 
							dtmValidTo>='$dtmDate'  and 
							intCompanyId=$intPubCompanyId and
							intTeamId = '$intTeamId';
							";
							//echo $sql5;
							$result5 = $db->RunQuery($sql5);
							$count = mysqli_num_rows($result5);
							if($count<=0)
							{
								$SQL2="insert into plan_calender 
									(dtmDate, 
									intCompanyId, 
									intTeamId, 
									strMonth,
									intYear, 
									intMonth, 
									intDay, 
									strDay,
									strDayStatus, 
									dblWorkingHours, 
									dblStartTime, 
									dblEndTime, 
									intMachines, 
									intTeamEfficency
									)
									values
									('$dtmDate', 
									'$intCompanyId', 
									'$intTeamId',
									'$strMonth', 
									'$intYear', 
									'$intMonth', 
									'$intDay', 
									'$strDay',
									'inactive', 
									'0', 
									'0', 
									'0', 
									'0', 
									'0'
									);";
							}
							////////////////////
							else
							{
							
							$SQL2="insert into plan_calender 
									(dtmDate, 
									intCompanyId, 
									intTeamId, 
									strMonth,
									intYear, 
									intMonth, 
									intDay, 
									strDay,
									strDayStatus, 
									dblWorkingHours, 
									dblStartTime, 
									dblEndTime, 
									intMachines, 
									intTeamEfficency
									)
									values
									('$dtmDate', 
									'$intCompanyId', 
									'$intTeamId',
									'$strMonth', 
									'$intYear', 
									'$intMonth', 
									'$intDay', 
									'$strDay',
									'$strDayStatus', 
									'$dblWorkingHours', 
									'$dblStartTime', 
									'$dblEndTime', 
									'$intMachines', 
									'$intTeamEfficency'
									);";
								}
							
					$result2 = $db->RunQuery($SQL2);
					//echo $result2;
					if($result2 == '1')
						echo "Saved SuccessFully";
					else{						
					
					if($count<=0)
					{
					$SQL1="update plan_calender 
							set
							dtmDate          = '$dtmDate' , 
							intCompanyId     = '$intCompanyId' , 
							intTeamId        = '$intTeamId' , 
							strMonth         = '$strMonth' , 
							intYear          = '$intYear',
							intMonth         = '$intMonth',
							intDay           = '$intDay' , 
							strDay           = '$strDay' , 
							strDayStatus     = 'inactive' , 
							dblWorkingHours  = '0' , 
							dblStartTime     = '0' , 
							dblEndTime       = '0' , 
							intMachines      = '0' , 
							intTeamEfficency = '0'
							
							where dtmDate = '$dtmDate' and intCompanyId = '$intCompanyId' and intTeamId = '$intTeamId';";
				   }
				   else
				   {
				   
				   $SQL1="update plan_calender 
							set
							dtmDate          = '$dtmDate' , 
							intCompanyId     = '$intCompanyId' , 
							intTeamId        = '$intTeamId' , 
							strMonth         = '$strMonth' , 
							intYear          = '$intYear',
							intMonth         = '$intMonth',
							intDay           = '$intDay' , 
							strDay           = '$strDay' , 
							strDayStatus     = '$strDayStatus' , 
							dblWorkingHours  = '$dblWorkingHours' , 
							dblStartTime     = '$dblStartTime' , 
							dblEndTime       = '$dblEndTime' , 
							intMachines      = '$intMachines' , 
							intTeamEfficency = '$intTeamEfficency'
							
							where dtmDate = '$dtmDate' and intCompanyId = '$intCompanyId' and intTeamId = '$intTeamId';";
				   }	        
									$result1 = $db->RunQuery($SQL1);
									//echo $result1;
									if($result1 == '1')
										echo "Saved Successfully";
									else
										echo $SQL1;
					}
				}
			}
	}
?>

<?php
	session_start();
	
	$backwardseperator 	= "../../../";
	ini_set('max_execution_time',30000);
	$mainPath 			= $_SESSION['mainPath'];
	$userId 			= $_SESSION['userId'];
	$thisFilePath 		=  $_SERVER['PHP_SELF'];
	$insUpdtStatus    	= "";
	$prePlanReference 	= "";
	$updateHStatus 		= false;
	include  "{$backwardseperator}dataAccess/permisionCheck.inc";
	
	$sql 	= "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	$locationId = $_SESSION["CompanyID"];
	//===========================================================================
	$prePlanNo  = $_REQUEST['cboSearch'];
	//$customerId = $_REQUEST['cboCustomer'];
	//===========================================================================
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Plan Allocation</title>

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../masterData/css/planning.css" rel="stylesheet" type="text/css" />
<link href="../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="planUpload-js.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../libraries/calendar/theme.css" />
<script src="../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">

<style type="text/css">
.web_dialog_overlay
{
   position: fixed;
   top: 0;
   right: 0;
   bottom: 0;
   left: 0;
   height: 100%;
   width: 100%;
   margin: 0;
   padding: 0;
   background: #000000;
   opacity: .15;
   filter: alpha(opacity=15);
   -moz-opacity: .15;
   z-index: 0;
   display: none;
}
.web_dialog
{
   display: none;
   position: fixed;
   width: 450px;
   height: 350px;
   top: 50%;
   left: 50%;
   margin-left: -190px;
   margin-top: -100px;
   background-color: #F0F0F0;
   border: 2px solid #336699;
   padding: 0px;
   z-index: 0;
   font-family: Verdana;
   font-size: 10pt;
}
.web_dialog_title
{
   border-bottom: solid 2px #336699;
   background-color: #336699;
   padding: 4px;
   color: White;
   font-weight:bold;
}
.web_dialog_title a
{
   color: White;
   text-decoration: none;
}
.align_right
{
   text-align: right;
}
</style>

</head>

<body>
<form id="frmPlanUpload" name="frmPlanUpload" enctype="multipart/form-data" action="planUpload.php" method="post" autocomplete="off">
<input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
<table width="100%" border="0" align="center">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div align="center">
<div class="trans_layoutXL">
<div class="trans_text">Customer Plan Allocation</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td colspan="2" bgcolor="#0033FF"><strong><span class="normaltxtmidb2">C  u  s  t  o  m  e  r    P  l  a  n    U  p  l  o  a  d</span></strong></td>
</tr>
<tr>
  <td colspan="2">
  <table width="100%">
  <tr>
    <td colspan="3" align="center">
    <table style="display:none">
    <tr>
      <td class="normalfntMid">Allocation Number: </td>
    <td class="normalfntMid">
    <select name="cboSearch" id="cboSearch" style="width:200px" >
    <option value=""></option>
    <?php   $sql = "SELECT DISTINCT
					intPlanAllocationNo
					FROM plan_upload_header
					WHERE 
					intCompanyId = '$companyId' AND
					intLocationId = '$locationId'
					ORDER BY intPlanAllocationNo DESC ";
                $result = $db->RunQuery($sql);
                while($row=mysqli_fetch_array($result))
                {
					if($row['intPlanAllocationNo']==$prePlanNo)
						echo "<option value=\"".$row['intPlanAllocationNo']."\" selected=\"selected\">".$row['intPlanAllocationNo']."</option>";	
					else
						echo "<option value=\"".$row['intPlanAllocationNo']."\">".$row['intPlanAllocationNo']."</option>";
                }
  ?>
  </select>
    </td>
     <td class="normalfntMid">&nbsp;</td>
     <td class="normalfntMid">&nbsp;</td>
    </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td colspan="3" align="center">

    </td>
  </tr>
  <tr>
  <td width="34%" class="normalfnt"><a style="display:none" href="downLoadFile/sampleExcel.xls">Download sample excel format</a></td>
  <td width="45%"><table>
    <tr>
      <td class="normalfntBlue"><strong>Choose File</strong></td>
      <td><input type="file" name="file" id="file" onchange="uploadFile(this.value)"/>
        <input type="hidden" name="hdPath" id="hdPath" value="" /></td>
      <td><input type="button" name="btnUpload" id="btnUpload" value="Upload" /></td>
    </tr>
  </table></td>
  <td width="21%" style="text-align:right" class="normalfnt">&nbsp;<a target="_new" style="display:none" href="../../../customerAndOperation/sample/sampleProgress/sampleProgress.php">View Sample Details</a></td>
  </tr>
    </table></td>
</tr>
<tr>
  <td colspan="2">
  <div style="overflow:scroll;width:1270px;height:350px;" id="divGrid">
    <table width="700%" id="tblMainGrid2" class="grid" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
<?php
if($_SERVER["REQUEST_METHOD"] == "POST")
{
	
	$rollbackflag = 0;
	$msg		  = "";

	$db->begin();
	
	try
	{
		ob_start();
		// include class file.
		include 'reader.php';
		if(!is_dir("../../../documents/Planning"))
		{
			mkdir("../../../documents/Planning", 0700);
		}
		$upload_path = '../../../documents/Planning/'; //same directory
		
		if (!empty($_FILES['file'])) 
		{	
			if ($_FILES['file']['error'] == 0) 
			{
				// check extension
				$file = explode(".", $_FILES['file']['name']);
				if (move_uploaded_file($_FILES['file']['tmp_name'], $upload_path.'/'.$_FILES['file']['name'])) 
				{	
					fopen($file_path, 'r');
				}
			} 
		}
		//unlink($file_path);
		$filePath = $_POST['hdPath'];
		//echo $filePath;
		// initialize reader object
		$excel = new Spreadsheet_Excel_Reader();
		// read spreadsheet data
		$excel->read($upload_path.$filePath);    
		// iterate over spreadsheet cells and print as HTML table
		$x=10;
		while($x<=$excel->sheets[0]['numRows']) 
		{
			if($x==10)
			{
				 echo "\t<tr class=normalfntBlue>\n";
				 $y=1;
				 while($y<=$excel->sheets[0]['numCols']) 
				 {
					$cell = isset($excel->sheets[0]['cells'][$x][$y]) ? $excel->sheets[0]['cells'][$x][$y] : '';
					echo "\t\t<td bgcolor=#FAD163>$cell</td>\n";
					$y++;
				 }  
			}
			else
			{
			  echo "\t<tr bgcolor=#FFFFFF class=normalfntBlue>\n";
			  $y=1;
	//----------------------------------------------------------------------------------------------------------------------------
			  $methodOfPrint	= isset($excel->sheets[0]['cells'][$x][1]) ? $excel->sheets[0]['cells'][$x][1] : '';
			  $marketer 		= isset($excel->sheets[0]['cells'][$x][3]) ? $excel->sheets[0]['cells'][$x][3] : '';
			  $team 			= isset($excel->sheets[0]['cells'][$x][4]) ? $excel->sheets[0]['cells'][$x][4] : 'null';
			  $customer 		= isset($excel->sheets[0]['cells'][$x][5]) ? $excel->sheets[0]['cells'][$x][5] : '';
			  $color 			= isset($excel->sheets[0]['cells'][$x][6]) ? $excel->sheets[0]['cells'][$x][6] : '';
			  $style 			= isset($excel->sheets[0]['cells'][$x][7]) ? $excel->sheets[0]['cells'][$x][7] : '';
			  $graphic 			= isset($excel->sheets[0]['cells'][$x][8]) ? $excel->sheets[0]['cells'][$x][8] : '';
			  $sampleYear 		= ($excel->sheets[0]['cells'][$x][9])!='' ? $excel->sheets[0]['cells'][$x][9] : 0;
			  $sampleNo 		= ($excel->sheets[0]['cells'][$x][10])!='' ? $excel->sheets[0]['cells'][$x][10] : 0;
			  $cusPONo 			= $excel->sheets[0]['cells'][$x][11];
			  $boardSize 		= isset($excel->sheets[0]['cells'][$x][12]) ? $excel->sheets[0]['cells'][$x][12] : '';
			  $strokes	 		= ($excel->sheets[0]['cells'][$x][13])!='' ? $excel->sheets[0]['cells'][$x][13] : 0;
			  $strokeConFac		= ($excel->sheets[0]['cells'][$x][14])!='' ? str_replace(',','',$excel->sheets[0]['cells'][$x][14]) : 0;
			  $vaProcess		= isset($excel->sheets[0]['cells'][$x][15]) ? $excel->sheets[0]['cells'][$x][15] : '';
			  $dailyReq			= ($excel->sheets[0]['cells'][$x][16])!='' ? str_replace(',','',$excel->sheets[0]['cells'][$x][16]) : 0;
			  $psd				= ($excel->sheets[0]['cells'][$x][17])!='' ? '"'.$excel->sheets[0]['cells'][$x][17].'"' : 'null';
			  $revPsd			= ($excel->sheets[0]['cells'][$x][18])!='' ? '"'.$excel->sheets[0]['cells'][$x][18].'"' : 'null';
			  $total 			= ($excel->sheets[0]['cells'][$x][19])!='' ? str_replace(',','',$excel->sheets[0]['cells'][$x][19]) : 0;				
			
				 if($methodOfPrint!='' && $marketer!='')
				 {		  
					if(($x%2)==0)
					{
						$insUpdtStatus 	= "update";
					}
					if(($x%2)==1)
					{
						$sqlchk = "SELECT intId,intPlantId FROM plan_teams WHERE strName = '".trim($team)."' AND intLocationId='$locationId' ";
						$result = $db->RunQuery2($sqlchk);
						$row	= mysqli_fetch_array($result);
						if(mysqli_num_rows($result)>0)
						{
							$teamId  = $row['intId'];
							$plantId = $row['intPlantId'];
						}
						else
						{
							$rollbackflag = 1;
							$msg		  = "Team name '".$team."' dosen't match with ERP system. Line No : ".$x." ";
							break;
						}
						if($style=='')
						{
							$rollbackflag = 1;
							$msg		  = "Style No is empty. Line No : ".$x." ";
							break;
						}
						
						if($psd!='null')
						{
							$newPSD = str_replace('"', "", $psd);
							if(!MyCheckDate($newPSD))
							{
								$rollbackflag = 1;
								$msg		  = "Invalid PSD date/date format. Line No : ".$x." ";
								break;
							}	
						}
						if($revPsd!='null')
						{
							$newRPSD = str_replace('"', "", $revPsd);
							if(!MyCheckDate($newRPSD))
							{
								$rollbackflag = 1;
								$msg		  = "Invalid Revised PSD date/date format. Line No : ".$x." ";
								break;
							}
						}
						$sqlchk2 = "SELECT 	intId
									FROM plan_upload_header
									WHERE 
									intCompanyId = '".trim($companyId)."' AND
									intLocationId = '".trim($locationId)."' AND 
									strMethodofPrint = '".trim($methodOfPrint)."' AND
									strMarketer = '".trim($marketer)."' AND
									intTeamId = '$teamId' AND 
									strCustomer = '".trim($customer)."' AND
									strColor = '".trim($color)."' AND 
									strStyleNo = '".trim($style)."' AND 
									strGraphic = '".mysqli_real_escape_string$graphic)."' AND
									intSampleYear = '".trim($sampleYear)."' AND 
									intSampleNo = '".trim($sampleNo)."' AND
									strCustomerPONo = '".trim($cusPONo)."' AND
									strBordSize = '".trim($boardSize)."' AND 
									dblStrokes = '".trim($strokes)."' AND 
									dblStrokeConFactor = '".trim($strokeConFac)."' AND 
									strVAProcess = '".trim($vaProcess)."' AND 
									dblDailyRequirement = '".trim($dailyReq)."' ";
						if($psd=='null')
							$sqlchk2.="AND dtmPsd IS NULL ";
						else
							$sqlchk2.="AND dtmPsd = $psd ";
						if($revPsd=='null')
							$sqlchk2.="AND dtmRevisedPsd IS NULL ";
						else
							$sqlchk2.="AND dtmRevisedPsd = $revPsd ";
						
						$sqlchk2.="AND dblTotal = $total ";
								
						$resultchk = $db->RunQuery2($sqlchk2);
						$rowchk = mysqli_fetch_array($resultchk);
						if(mysqli_num_rows($resultchk)>0)
						{
							$sqlUpd = "UPDATE plan_upload_header 
										SET dtmUploadDate = NOW()
										WHERE intId = '".$rowchk['intId']."' ";
							
							$firstResult = $db->RunQuery2($sqlUpd);
							if(!$firstResult)
							{
								$rollbackflag = 1;
								$msg		  = $db->errormsg;
							}
							$prePlanId   	= $rowchk['intId']; 
							$insUpdtStatus 	= "insert";	
							$updateHStatus  = true;
						}
						else
						{
							$sql = "INSERT INTO plan_upload_header 
								(
								 dtmUploadDate, intCompanyId, intLocationId, strMethodofPrint, strMarketer, 
								 intTeamId, intPlantId, strCustomer, strColor, strStyleNo, strGraphic, intSampleYear, intSampleNo, strCustomerPONo, 
								 strBordSize, dblStrokes, dblStrokeConFactor, strVAProcess, dblDailyRequirement, dtmPsd, dtmRevisedPsd, dblTotal
								)
								VALUES
								(
								 now(), '$companyId', '$locationId', '$methodOfPrint', '$marketer', 
								'$teamId', '$plantId', '$customer', '$color', '$style', '".mysqli_real_escape_string$graphic)."', $sampleYear, $sampleNo, 
								'".trim($cusPONo)."', '$boardSize', $strokes,$strokeConFac, '$vaProcess', $dailyReq, $psd, $revPsd, $total
								) ";
						
							$firstResult 	= $db->RunQuery2($sql);
							if(!$firstResult)
							{
								$rollbackflag = 1;
								$msg		  = $db->errormsg;
							}
							$prePlanId   	= $db->insertId; 
							$insUpdtStatus 	= "insert";	
							$updateHStatus  = false;
						}	
					}
		
					$z = 21;
					while($z<=$excel->sheets[0]['numCols']) 
					{
						$date 		= isset($excel->sheets[0]['cells'][10][$z]) ? $excel->sheets[0]['cells'][10][$z] : '';
						$qty  		= (($excel->sheets[0]['cells'][$x][$z])==''?0:$excel->sheets[0]['cells'][$x][$z]);
						$Astrokes  	= ($excel->sheets[0]['cells'][5][$z])!='' ? $excel->sheets[0]['cells'][5][$z] : 0;
						
						if($date!='')
						{
							if(!MyCheckDate($date))
							{
								$rollbackflag = 1;
								$msg		  = "Invalid date/date format. Column No : ".$z." ";
								break;
							}
						}
						if($insUpdtStatus=="insert" && $updateHStatus == false)
						{
							
							$sql1 = "INSERT INTO plan_upload_details 
									(intPrePlanId, strDate, dblAvailableStrokes, dblPlanPcs)
									VALUES
									('$prePlanId', '$date', '$Astrokes', '$qty')";
							
							$result1 = $db->RunQuery2($sql1);
							if(!$result1)
							{
								$rollbackflag = 1;
								$msg		  = $db->errormsg;
								break 2;
							}
							$z++;	
						}
						
						else if($insUpdtStatus=="insert" && $updateHStatus == true)
						{
							$sql1 = " UPDATE plan_upload_details 
										SET
										strDate = '$date' , 
										dblAvailableStrokes = '$Astrokes' , 
										dblPlanPcs = '$qty'
										WHERE
										intPrePlanId = '$prePlanId' AND 
										strDate = '$date' ;";
							
							$result1 = $db->RunQuery2($sql1);
							if(!$result1)
							{
								$rollbackflag = 1;
								$msg		  = $db->errormsg;
								break 2;
							}
							$z++;
						}
						else
						{
							
							$sql2 = "UPDATE plan_upload_details 
									 SET dblNoOfStrokes = '$qty' 
									 WHERE intPrePlanId = '$prePlanId' AND strDate = '$date' ";
							
							$result2 = $db->RunQuery2($sql2);
							if(!$result2)
							{
								$rollbackflag = 1;
								$msg		  = $db->errormsg;
								break 2;
							}
							$z++;		
						}		
					}	
		
					while($y<=$excel->sheets[0]['numCols']) 
					{
						$cell = isset($excel->sheets[0]['cells'][$x][$y]) ? $excel->sheets[0]['cells'][$x][$y] : '';
						echo "\t\t<td>$cell</td>\n";  
						$y++;
					} 
				}
			}
			echo "\t</tr>\n";
			$x++;
			$insUpdtStatus = ""; 
		}
		ob_flush();
		
		if($rollbackflag==0)
		{
			$db->commit();
		}
		else
		{		
			$db->rollback();//roalback
			echo '<script language="javascript">alert("'.$msg.'")</script>';
			echo '<script language="javascript">document.getElementById("tblMainGrid2").innerHTML="";</script>';
		}
	}
	catch(Exception $e)
	{    
		$db->rollback();//roalback
	}
}
        //--------------------------------------------------------------------------------------------
	/*function getNextPrePlanNo($companyId,$locationId)
	{
		global $db;
		$sql = "SELECT
				intPlanUploadNo
				FROM sys_plan_no
				WHERE
				intCompanyId = '$companyId' AND intLocationId = '$locationId'
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$nextPrePlanNo = $row['intPlanUploadNo'];
		
		$sql = "UPDATE `sys_plan_no` SET intPlanUploadNo=intPlanUploadNo+1 
				WHERE (intCompanyId = '$companyId' AND intLocationId = '$locationId')";
		$db->RunQuery($sql);
		return $nextPrePlanNo;
	}*/
function MyCheckDate($postedDate) 
{
	if ( preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $postedDate) ) 
	{
		list($year , $month , $day) = explode('-',$postedDate);
		return checkdate($month , $day , $year);
	} 
	else 
	{
		return false;
	}
} 
        ?>
    </table>
    <hr width="770%" />
    <table id="tblLoadData" width="770%" class="grid" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
      <!--this area is excel sheet uploaded data preview-->
    </table>
  </div>
  <table width="100%">
    <tr>
      <td>&nbsp;</td>
     </tr>
    </table>
    </td>
</tr>
<tr>
<td width="100%" align="center" bgcolor=""><img  style="display:none" border="0" src="../../../images/Tnew.jpg" alt="Save" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="24"/><a href="../../../main.php"><img  src="../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
</tr>
</table>
</div>
</div>
</form>
</body>
</html>
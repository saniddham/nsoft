// JavaScript Document
var errMsg = "";
$(document).ready(function() {
	$("#frmPlanUpload").validationEngine();	
	//permision for add 
	if(intAddx)
	{
		$('#frmPlanUpload #butSave').show();
		$('#frmPlanUpload #butNew').show();
	}
		//permision for edit 
	if(intEditx)
	{
		$('#frmPlanUpload #butSave').show();
		$('#frmPlanUpload #butNew').show();
	}
	
	$('#frmPlanUpload #btnUpload').live('click',uploadExcel);
	$('#frmPlanUpload #butNew').live('click',clearAll);
		
});
function clearAll()
{
	document.location.href = 'planUpload.php';
}
function uploadExcel()
{		
	if ($('#frmPlanUpload').validationEngine('validate'))
	{
		if(document.getElementById('file').value && errMsg == "")
		{
			document.getElementById('frmPlanUpload').submit();	
		}
		else
		{
			$('#frmPlanUpload #btnUpload').validationEngine('showPrompt', 'Please Choose File/ Valid File Before Uploading','fail');
			var t=setTimeout("alertx()",3000);
		}
	}
	else
	{
		return;
	}		
}
function uploadFile(string) 
{
	var flPath = string;
	var fileTypes=["xlsx","csv","xls","xml"];
	var param = false;
	var ext=string.substring(string.lastIndexOf(".")+1,string.length).toLowerCase();
	for (var i=0; i<fileTypes.length; i++)
	{
	 if (fileTypes[i]==ext)
	 {
		param = true;
	 }
	}
	 if(param == true)
	 {
		errMsg = "";
		document.getElementById('hdPath').value = flPath;
		//alert('File:\/\/' + flPath);
	 }
	 else
	 {
		alert("THAT IS NOT A VALID FILE\nPlease load a file with an extention of one of the following:\n\n"+fileTypes.join(", "));
		errMsg = "error";
	 }
}

function alertx()
{
	$('#frmPlanUpload #btnUpload').validationEngine('hide');
}

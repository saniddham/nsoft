<?php 
session_start();
$backwardseperator 	= "../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 	 		= $_SESSION['userId'];
$requestType 		= $_REQUEST['requestType'];
$maxCapTeamId		= "";
$arrAvailable       = array();
$arrPlan      		= array();
$arrActual     		= array();
$arrWeek			= array();
include "{$backwardseperator}dataAccess/Connector.php";

if($requestType=="loadLocation")
{
	$companyId = $_REQUEST['companyId'];
	
	$sql = "SELECT
			mst_locations_user.intLocationId,
			mst_locations.strName AS location
			FROM
			mst_locations_user
			Inner Join mst_locations ON mst_locations.intId = mst_locations_user.intLocationId
			Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
			WHERE
			mst_locations_user.intUserId ='$userId' AND
			mst_locations.intCompanyId = '$companyId' ";
	$result = $db->RunQuery($sql);
	
	$option = "<option value=\"\"></option>";
	while($row = mysqli_fetch_array($result))
	{
		$option .="<option value=\"".$row['intLocationId']."\">".$row['location']."</option>";
	}
	echo $option;
}
else if($requestType=="getDetails")
{
	$companyId  	= $_REQUEST['cboCompany'];
	$locationId 	= $_REQUEST['cboLocation'];
	$machineType  	= $_REQUEST['cboMachineType'];
	$month		  	= $_REQUEST['cboMonth'];
	$year			= $_REQUEST['cboYear'];
	
	$weeksql = "SELECT intyear,intMonth,intWeekId,dtmFromDate,dtmToDate
				FROM plan_week_setup
				WHERE intyear= '$year' AND
				intMonth = '$month' ";
	$weekResult = $db->RunQuery($weeksql);
	while($weekRow = mysqli_fetch_array($weekResult))
	{
		$frmDate = $weekRow['dtmFromDate'];
		$toDate  = $weekRow['dtmToDate'];
		$weekId  = $weekRow['intWeekId'];
		
		$sql = "SELECT intId FROM plan_teams WHERE intParentTeamId=0 AND intPlantId='$machineType' ";
		if($companyId!='')
			$sql .="AND intCompanyId='$companyId' ";
		if($locationId!='')
			$sql .="AND intLocationId='$locationId' ";
		$totalCapacity = 0;
		$result = $db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			$sql3 = "SELECT IFNULL(SUM(intMaxCapacity*dblWorkingHours),0) totalCapacity 
					FROM plan_calender 
					WHERE intTeamId='".$row['intId']."' AND 
					intMonth='$month' AND 
					intYear='$year' AND
					dtmDate BETWEEN ('$frmDate') AND ('$toDate') ";
			$result3 = $db->RunQuery($sql3);
			$row3 = mysqli_fetch_array($result3);
			$totalCapacity = $totalCapacity+$row3['totalCapacity'];
		}
		
		$availableCapacity = round($totalCapacity,0);
		$arrAvailable[$weekId] = $availableCapacity;
		
		$sql4 = "SELECT intStripId,dblCompleteQty FROM plan_stripes 
				WHERE (MONTH(startDate)='$month' OR MONTH(endDate)='$month') AND YEAR(startDate)='$year' AND
				intPrintType='$machineType' "; 
		if($companyId!='')
			$sql4 .="AND intCompanyId='$companyId' ";
		if($locationId!='')
			$sql4 .="AND intLocationId='$locationId' ";	
		
		$result4 = $db->RunQuery($sql4);
		$totalQty 	 = 0;
		$totalActQty = 0;
		$comQty   = 0;
		while($row4 = mysqli_fetch_array($result4))
		{
			//$comQty = $comQty+$row4['dblCompleteQty'];
			$sql5="SELECT SUM(dblQty) totDblQty,SUM(dblActualQty) totActualQty FROM plan_strip_details 
					WHERE intStripId='".$row4['intStripId']."' AND 
					MONTH(dtmDate) ='$month' AND 
					YEAR(dtmDate)='$year' AND
					dtmDate BETWEEN ('$frmDate') AND ('$toDate') ";
			
			$result5 = $db->RunQuery($sql5);
			$row5 = mysqli_fetch_array($result5);
			
			$totalQty 	 = $totalQty+$row5['totDblQty'];
			$totalActQty = $totalActQty+$row5['totActualQty'];
		}
		$planCapacity = round($totalQty,0);
		$actualQty    = round($totalActQty,0);
		
		$arrPlan[$weekId]		= $planCapacity;
		$arrActual[$weekId] 	= $actualQty;
		$arrWeek[$weekId]		= $frmDate.' / '.$toDate;
	}
		$response['available'] 	= $arrAvailable;
		$response['plan'] 		= $arrPlan;
		$response['actual'] 	= $arrActual;
		$response['week'] 		= $arrWeek;
		echo json_encode($response);
}
?>
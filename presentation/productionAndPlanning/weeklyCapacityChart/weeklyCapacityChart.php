<?php
session_start();
$backwardseperator = "../../../";
$mainPath 		= $_SESSION['mainPath'];
$userId 	 	= $_SESSION['userId'];
$thisFilePath 	=  $_SERVER['PHP_SELF'];
$companyId  	= "";
$locationId 	= "";
$machineType  	= "";
$month		  	= "";
$year			= "";
$arrAvailable       = array();
$arrPlan      		= array();
$arrActual     		= array();
$arrWeek			= array();
include  "{$backwardseperator}dataAccess/permisionCheck.inc";
include("../../../libraries/fusionChart/Includes/FusionCharts.php");

if($_SERVER['REQUEST_METHOD']=='POST')
{
	$companyId  	= $_REQUEST['cboCompany'];
	$locationId 	= $_REQUEST['cboLocation'];
	$machineType  	= $_REQUEST['cboMachineType'];
	$month		  	= $_REQUEST['cboMonth'];
	$year			= $_REQUEST['cboYear'];
}

if(!isset($_REQUEST['cboYear']))
	$year	= date('Y');
else
	$year	= $_REQUEST['cboYear'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Weekly Capacity Chart</title>

<link href="<?php echo $mainPath ?>images/logo_sm.png" rel="shortcut icon"  />
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>
<script type="application/javascript" src="weeklyCapacityChart-js.js"></script>
<script type="text/javascript" src="../../../libraries/fusionChart/jquery.min.js"></script>
<SCRIPT LANGUAGE="Javascript" SRC="../../../libraries/fusionChart/FusionCharts.js"></SCRIPT>
<script>
 FusionCharts.setCurrentRenderer('javascript');
</script>
<script>
//setTimeout("window.location.replace('../capacityChart/capacityChart.php')" ,10000)
</script>
<link rel="stylesheet" href="../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../libraries/validate/template.css" type="text/css">

<style>
.border-right {
	border-right-width: medium;
	border-right-style: solid;
	border-right-color: #000000;
	font-size:24px;
	text-align:right;
}
.large-font {
	font-size:24px;
	text-align:right;
}
.border-top {
	border-top-width: medium;
	border-top-style: solid;
	border-top-color: #000000;
	font-size:24px;
	text-align:center;
}
</style>
</head>

<body>
<form id="frmweeklyCapacityChart" name="frmweeklyCapacityChart" autocomplete="off" action="weeklyCapacityChart.php" method="post">

<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>

<table align="center" width="100%" border="0" height="108%" cellpadding="3" cellspacing="5" >
<tr>
<td colspan="3">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
    	<td width="6%" class="normalfnt">&nbsp;Company</td>
        <td width="16%" class="normalfnt"><?php 
					$sql = "SELECT intId,strName FROM mst_companies WHERE intStatus=1 ORDER BY strName";
					$result = $db->RunQuery($sql);
			?>              
            <select style="width:180px;" name="cboCompany" id="cboCompany" >
            <option value=""></option>
                <?php
				while($row = mysqli_fetch_array($result))
				{
					if($companyId==$row["intId"])
						echo "<option value=\"".$row["intId"]."\" selected=\"selected\" >".$row["strName"]."</option>";	
					else
						echo "<option value=\"".$row["intId"]."\" >".$row["strName"]."</option>";	
				}
			?>              </select></td>
        <td width="5%" class="normalfnt">&nbsp;Location</td>
        <td width="16%" class="normalfnt"><select style="width:180px;" name="cboLocation" id="cboLocation" >
        <option value=""></option>
        <?php
		$sql = "SELECT
			mst_locations_user.intLocationId,
			mst_locations.strName AS location
			FROM
			mst_locations_user
			Inner Join mst_locations ON mst_locations.intId = mst_locations_user.intLocationId
			Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
			WHERE
			mst_locations_user.intUserId ='$userId' AND
			mst_locations.intCompanyId = '$companyId' ";
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		if($locationId==$row['intLocationId'])
			echo "<option value=\"".$row['intLocationId']."\" selected=\"selected\">".$row['location']."</option>";
		else
			echo "<option value=\"".$row['intLocationId']."\">".$row['location']."</option>";
	}
		?>
        </select></td>
        <td width="8%" class="normalfnt">&nbsp;Machine Type</td>
        <td width="14%" class="normalfnt"><select style="width:150px;" name="cboMachineType" id="cboMachineType" >
	<?php
			$sql = "SELECT 	intId, strName
					FROM mst_printertypes 
					WHERE intStatus = 1
					ORDER BY strName ";
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				if($machineType==$row['intId'])
					echo "<option selected=\"selected\" value=\"".$row["intId"]."\" >".$row["strName"]."</option>";
				else
					echo "<option value=\"".$row["intId"]."\" >".$row["strName"]."</option>";
				if($machineType=='')
				$machineType = $row['intId'];
			}
			
    ?>
    </select> </td>
    <td width="3%" class="normalfnt">Year</td>
    <td width="9%" class="normalfnt"><select name="cboYear" id="cboYear" style="width:90px">
      <?php
		$sql ="SELECT DISTINCT YEAR(strDate) as intYear FROM plan_upload_details";
		$result = $db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			if($row['intYear']==$year)
				echo "<option value=\"".$row["intYear"]."\" selected=\"selected\" >".$row["intYear"]."</option>";
			else
				echo "<option value=\"".$row["intYear"]."\" >".$row["intYear"]."</option>";
		}
	  ?>
    </select></td>
    <td width="4%" class="normalfnt">Month</td>
    <td width="9%" class="normalfnt"><select name="cboMonth" id="cboMonth" style="width:90px">
      <?php
		$sql ="SELECT intMonthId,strMonth FROM mst_month ORDER BY intMonthId";
		$result = $db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			if($row['intMonthId']=='1')
				echo "<option value=\"".$row["intMonthId"]."\" selected=\"selected\">".$row["strMonth"]."</option>";
			else if($month==$row['intMonthId'])
				echo "<option value=\"".$row["intMonthId"]."\" selected=\"selected\">".$row["strMonth"]."</option>";
			else
				echo "<option value=\"".$row["intMonthId"]."\" >".$row["strMonth"]."</option>";
			if($month=="")
				$month = 1;
		}
	  ?>
    </select></td>
    <td width="8%" class="normalfnt"><img border="0" src="../../../images/Tview.jpg" alt="View" name="butView" width="92" height="24"  class="mouseover" id="butView"/></td>
    <td width="2%" align="center"><a href="<?php echo $mainPath ?>main.php" title="Home"><img src="<?php echo $mainPath ?>images/home_sm.png" alt="Home" width="22" height="23"  border="0" /></a></td>
    </tr>
    </table>
</td>
</tr>
<?php

	$weeksql = "SELECT intyear,intMonth,intWeekId,dtmFromDate,dtmToDate
				FROM plan_week_setup
				WHERE intyear= '$year' AND
				intMonth = '$month' ";
	$weekResult = $db->RunQuery($weeksql);
	while($weekRow = mysqli_fetch_array($weekResult))
	{
		$frmDate = $weekRow['dtmFromDate'];
		$toDate  = $weekRow['dtmToDate'];
		$weekId  = $weekRow['intWeekId'];
		
		$sql1=" SELECT SUM(dblAvailableStrokes) AS totAvailableQty
				FROM (SELECT dblAvailableStrokes FROM plan_upload_details PUD 
				INNER JOIN plan_upload_header PUH ON PUD.intPrePlanId=PUH.intId
				WHERE PUD.strDate BETWEEN ('$frmDate') AND ('$toDate') ";
		if($machineType!='')
			$sql1.= "AND PUH.intPlantId='$machineType' ";
		if($companyId!='')
			$sql1.= "AND PUH.intCompanyId='$companyId' ";
		if($locationId!='')
			$sql1.= "AND PUH.intLocationId='$locationId' ";	
				
		$sql1.="GROUP BY PUD.strDate) AS tb1 ";
					
		$result1 = $db->RunQuery($sql1);
		$row1 = mysqli_fetch_array($result1);
		$totalCapacity = $row1['totAvailableQty'];
		
		$availableCapacity 		= round($totalCapacity,0);
		$arrAvailable[$weekId] 	= $availableCapacity;
		
		$sql2=" SELECT IFNULL((SUM(PUD.dblNoOfStrokes)),0) AS planQty,IFNULL((SUM(dblActualQty)),0) AS actualQty
				FROM plan_upload_header PUH
				LEFT JOIN plan_upload_details PUD ON PUD.intPrePlanId=PUH.intId
				WHERE PUD.strDate BETWEEN ('$frmDate') AND ('$toDate') ";
		if($machineType!='')
			$sql2.= "AND PUH.intPlantId='$machineType' ";
		if($companyId!='')
			$sql2.= "AND PUH.intCompanyId='$companyId' ";
		if($locationId!='')
			$sql2.= "AND PUH.intLocationId='$locationId' ";
		
		$result2 = $db->RunQuery($sql2);
		$row2 = mysqli_fetch_array($result2);
		
		$totalPlanQty 	= $row2['planQty'];
		$totalActQty 	= $row2['actualQty'];
			
		$planCapacity 	= round($totalPlanQty,0);
		$actualQty    	= round($totalActQty,0);
		
		$arrPlan[$weekId]		= $planCapacity;
		$arrActual[$weekId] 	= $actualQty;
		$arrWeek[$weekId]		= $frmDate.' / '.$toDate;
	}
?>
<tr>
	<td width="33%" height="50%" valign="top"><div class="Weekly-chart-render1">
        <?php
        $strXML = "<chart yAxisMaxValue='1500000' formatNumberScale='0' caption=\"Shot Calculation - 1st Week\" subCaption=\"($arrWeek[1])\" xAxisName=\"\" yAxisName=\"\" numberPrefix=\"\"> ";
        $strXML .= "<set label='Available' value='" . $arrAvailable[1] . "' color='#A1A1A1' />";
		$strXML .= "<set label='Plan' value='" . $arrPlan[1] . "' color='#00FF00' />";
		$strXML .= "<set label='Actual' value='" . $arrActual[1] . "' color='#0000EE' />";
		$strXML .= "<set label='Freeze Plan' value='" . $arrAvailable[1] . "'/>";
		$strXML .= "<set label='Actual Delivery' value='" . $arrPlan[1] . "'/>";
		$strXML .= "<styles>";
		$strXML .= "<definition>";
		$strXML .= "<style name='myCaptionFont' type='font' font='Arial' size='16' color='#7E2217' bold='1'/>";
		$strXML .= "<style name='mySubCaptionFont' type='font' font='Arial' size='12' bold='1'/>";
		$strXML .= "<style name='myLabelsFont' type='font' font='Arial' size='14' bold='1'/>";
		$strXML .= "<style name='myValueFont' type='font' font='Arial' size='14' color='000000' bold='1'/>";
		$strXML .= "<style name='myYaxisFont' type='font' font='Arial' size='14' bold='1'/>";		
		$strXML .= "</definition>";
		$strXML .= "<application>";
		$strXML .= "<apply toObject='Caption' styles='myCaptionFont' />";
		$strXML .= "<apply toObject='SubCaption' styles='mySubCaptionFont' />";
		$strXML .= "<apply toObject='DataLabels' styles='myLabelsFont' />";
		$strXML .= "<apply toObject='DataValues' styles='myValueFont' />";
		//$strXML .= "<apply toObject='YAxisValues' styles='myYaxisFont' />";
		$strXML .= "</application>";
		$strXML .= "</styles>";
        $strXML .= "</chart>";
        echo renderChart("Column3D", "", $strXML, "weeklyChart1", 440, 340, false, false);
        ?>     
    </div>
    </td>
	 <td width="33%" height="50%" valign="top"><div class="Weekly-chart-render2">
        <?php
        $strXML = "<chart yAxisMaxValue='1500000' formatNumberScale='0' caption=\"Shot Calculation - 2nd Week\" subCaption=\"($arrWeek[2])\" xAxisName=\"\" yAxisName=\"\" numberPrefix=\"\"> ";
        $strXML .= "<set label='Available' value='" . $arrAvailable[2] . "' color='#A1A1A1' />";
		$strXML .= "<set label='Plan' value='" . $arrPlan[2] . "' color='#00FF00' />";
		$strXML .= "<set label='Actual' value='" . $arrActual[2] . "' color='#0000EE' />";
		$strXML .= "<set label='Freeze Plan' value='" . $arrAvailable[1] . "'/>";
		$strXML .= "<set label='Actual Delivery' value='" . $arrPlan[1] . "'/>";
		$strXML .= "<styles>";
		$strXML .= "<definition>";
		$strXML .= "<style name='myCaptionFont' type='font' font='Arial' size='16' color='#7E2217' bold='1'/>";
		$strXML .= "<style name='mySubCaptionFont' type='font' font='Arial' size='12' bold='1'/>";
		$strXML .= "<style name='myLabelsFont' type='font' font='Arial' size='14' bold='1'/>";
		$strXML .= "<style name='myValueFont' type='font' font='Arial' size='14' color='000000' bold='1'/>";
		$strXML .= "<style name='myYaxisFont' type='font' font='Arial' size='14' bold='1'/>";		
		$strXML .= "</definition>";
		$strXML .= "<application>";
		$strXML .= "<apply toObject='Caption' styles='myCaptionFont' />";
		$strXML .= "<apply toObject='SubCaption' styles='mySubCaptionFont' />";
		$strXML .= "<apply toObject='DataLabels' styles='myLabelsFont' />";
		$strXML .= "<apply toObject='DataValues' styles='myValueFont' />";
		//$strXML .= "<apply toObject='YAxisValues' styles='myYaxisFont' />";
		$strXML .= "</application>";
		$strXML .= "</styles>";
        $strXML .= "</chart>";
        echo renderChart("Column3D", "", $strXML, "weeklyChart2", 440, 340, false, false);
        ?>
       
    </div></td>
	<td width="33%" height="50%" valign="top"><div class="Weekly-chart-render3">
        <?php
        $strXML = "<chart yAxisMaxValue='1500000' formatNumberScale='0' caption=\"Shot Calculation - 3rd Week\" subCaption=\"($arrWeek[3])\" xAxisName=\"\" yAxisName=\"\" numberPrefix=\"\"> ";
        $strXML .= "<set label='Available' value='" . $arrAvailable[3] . "' color='#A1A1A1' />";
		$strXML .= "<set label='Plan' value='" . $arrPlan[3] . "' color='#00FF00' />";
		$strXML .= "<set label='Actual' value='" . $arrActual[3] . "' color='#0000EE' />";
		$strXML .= "<set label='Freeze Plan' value='" . $arrAvailable[1] . "'/>";
		$strXML .= "<set label='Actual Delivery' value='" . $arrPlan[1] . "'/>";
		$strXML .= "<styles>";
		$strXML .= "<definition>";
		$strXML .= "<style name='myCaptionFont' type='font' font='Arial' size='16' color='#7E2217' bold='1'/>";
		$strXML .= "<style name='mySubCaptionFont' type='font' font='Arial' size='12' bold='1'/>";
		$strXML .= "<style name='myLabelsFont' type='font' font='Arial' size='14' bold='1'/>";
		$strXML .= "<style name='myValueFont' type='font' font='Arial' size='14' color='000000' bold='1'/>";
		$strXML .= "<style name='myYaxisFont' type='font' font='Arial' size='14' bold='1'/>";		
		$strXML .= "</definition>";
		$strXML .= "<application>";
		$strXML .= "<apply toObject='Caption' styles='myCaptionFont' />";
		$strXML .= "<apply toObject='SubCaption' styles='mySubCaptionFont' />";
		$strXML .= "<apply toObject='DataLabels' styles='myLabelsFont' />";
		$strXML .= "<apply toObject='DataValues' styles='myValueFont' />";
		//$strXML .= "<apply toObject='YAxisValues' styles='myYaxisFont' />";
		$strXML .= "</application>";
		$strXML .= "</styles>";
        $strXML .= "</chart>";
        echo renderChart("Column3D", "", $strXML, "weeklyChart3", 440, 340, false, false);
        ?>
       
    </div></td>
    </tr>

<tr>
	<td width="33%" height="50%" valign="top"><div class="Weekly-chart-render4">
        <?php
        $strXML = "<chart yAxisMaxValue='1500000' formatNumberScale='0' caption=\"Shot Calculation - 4th Week\" subCaption=\"($arrWeek[4])\" xAxisName=\"\" yAxisName=\"\" numberPrefix=\"\"> ";
        $strXML .= "<set label='Available' value='" . $arrAvailable[4] . "' color='#A1A1A1' />";
		$strXML .= "<set label='Plan' value='" . $arrPlan[4] . "' color='#00FF00' />";
		$strXML .= "<set label='Actual' value='" . $arrActual[4] . "' color='#0000EE' />";
		$strXML .= "<set label='Freeze Plan' value='" . $arrAvailable[1] . "'/>";
		$strXML .= "<set label='Actual Delivery' value='" . $arrPlan[1] . "'/>";
		$strXML .= "<styles>";
		$strXML .= "<definition>";
		$strXML .= "<style name='myCaptionFont' type='font' font='Arial' size='16' color='#7E2217' bold='1'/>";
		$strXML .= "<style name='mySubCaptionFont' type='font' font='Arial' size='12' bold='1'/>";
		$strXML .= "<style name='myLabelsFont' type='font' font='Arial' size='14' bold='1'/>";
		$strXML .= "<style name='myValueFont' type='font' font='Arial' size='14' color='000000' bold='1'/>";
		$strXML .= "<style name='myYaxisFont' type='font' font='Arial' size='14' bold='1'/>";		
		$strXML .= "</definition>";
		$strXML .= "<application>";
		$strXML .= "<apply toObject='Caption' styles='myCaptionFont' />";
		$strXML .= "<apply toObject='SubCaption' styles='mySubCaptionFont' />";
		$strXML .= "<apply toObject='DataLabels' styles='myLabelsFont' />";
		$strXML .= "<apply toObject='DataValues' styles='myValueFont' />";
		//$strXML .= "<apply toObject='YAxisValues' styles='myYaxisFont' />";
		$strXML .= "</application>";
		$strXML .= "</styles>";
        $strXML .= "</chart>";
        echo renderChart("Column3D", "", $strXML, "weeklyChart4", 440, 340, false, false);
        ?>
       
    </div></td>
	<td width="33%" height="50%" valign="top"><div class="Weekly-chart-render5">
        <?php
        $strXML = "<chart yAxisMaxValue='1500000' formatNumberScale='0' caption=\"Shot Calculation - 5th Week\" subCaption=\"($arrWeek[5])\" xAxisName=\"\" yAxisName=\"\" numberPrefix=\"\"> ";
        $strXML .= "<set label='Available' value='" . $arrAvailable[5] . "' color='#A1A1A1' />";
		$strXML .= "<set label='Plan' value='" . $arrPlan[5] . "' color='#00FF00' />";
		$strXML .= "<set label='Actual' value='" . $arrActual[5] . "' color='#0000EE' />";
		$strXML .= "<set label='Freeze Plan' value='" . $arrAvailable[1] . "'/>";
		$strXML .= "<set label='Actual Delivery' value='" . $arrPlan[1] . "'/>";
		$strXML .= "<styles>";
		$strXML .= "<definition>";
		$strXML .= "<style name='myCaptionFont' type='font' font='Arial' size='16' color='#7E2217' bold='1'/>";
		$strXML .= "<style name='mySubCaptionFont' type='font' font='Arial' size='12' bold='1'/>";
		$strXML .= "<style name='myLabelsFont' type='font' font='Arial' size='14' bold='1'/>";
		$strXML .= "<style name='myValueFont' type='font' font='Arial' size='14' color='000000' bold='1'/>";
		$strXML .= "<style name='myYaxisFont' type='font' font='Arial' size='14' bold='1'/>";		
		$strXML .= "</definition>";
		$strXML .= "<application>";
		$strXML .= "<apply toObject='Caption' styles='myCaptionFont' />";
		$strXML .= "<apply toObject='SubCaption' styles='mySubCaptionFont' />";
		$strXML .= "<apply toObject='DataLabels' styles='myLabelsFont' />";
		$strXML .= "<apply toObject='DataValues' styles='myValueFont' />";
		//$strXML .= "<apply toObject='YAxisValues' styles='myYaxisFont' />";
		$strXML .= "</application>";
		$strXML .= "</styles>";
        $strXML .= "</chart>";
        echo renderChart("Column3D", "", $strXML, "weeklyChart5", 440, 340, false, false);
        ?>
       
    </div></td>
	<td width="33%" height="50%" valign="top"><div class="Weekly-chart-render6">
        <?php
        $strXML = "<chart formatNumberScale='0' palette='5' caption=\"Shot Calculation - All\" xAxisName=\"\" yAxisName=\"\" numberPrefix=\"\"> ";
        $strXML .= "<set label='Available' value='" . ($arrAvailable[1]+$arrAvailable[2]+$arrAvailable[3]+$arrAvailable[4]+$arrAvailable[5]) . "' color='#A1A1A1' />";
		$strXML .= "<set label='Plan' value='" . ($arrPlan[1]+$arrPlan[2]+$arrPlan[3]+$arrPlan[4]+$arrPlan[5]) . "' color='#00FF00' />";
		$strXML .= "<set label='Actual' value='" . ($arrActual[1]+$arrActual[2]+$arrActual[3]+$arrActual[4]+$arrActual[5]) . "' color='#0000EE' />";
		$strXML .= "<set label='Freeze Plan' value='" . $arrAvailable[1] . "'/>";
		$strXML .= "<set label='Actual Delivery' value='" . $arrPlan[1] . "'/>";
		$strXML .= "<styles>";
		$strXML .= "<definition>";
		$strXML .= "<style name='myCaptionFont' type='font' font='Arial' size='16' color='#7E2217' bold='1'/>";
		$strXML .= "<style name='myLabelsFont' type='font' font='Arial' size='14' bold='1'/>";
		$strXML .= "<style name='myValueFont' type='font' font='Arial' size='14' color='000000' bold='1'/>";
		$strXML .= "<style name='myYaxisFont' type='font' font='Arial' size='14' bold='1'/>";		
		$strXML .= "</definition>";
		$strXML .= "<application>";
		$strXML .= "<apply toObject='Caption' styles='myCaptionFont' />";
		$strXML .= "<apply toObject='DataLabels' styles='myLabelsFont' />";
		$strXML .= "<apply toObject='DataValues' styles='myValueFont' />";
		//$strXML .= "<apply toObject='YAxisValues' styles='myYaxisFont' />";
		$strXML .= "</application>";
		$strXML .= "</styles>";
        $strXML .= "</chart>";
        echo renderChart("Column3D", "", $strXML, "weeklyChart6", 440, 340, false, false);
        ?>
       
    </div></td>
    </tr>
</table>
</form>
</body>
</html>

// JavaScript Document
$(document).ready(function(){
	
	$('#frmweeklyCapacityChart #cboCompany').change(function(){
		
		if($('#cboCompany').val()=="")
			$('#cboLocation').html('');
		else
		{
			loadLocation(this);
		}
		
	});
	$('#frmweeklyCapacityChart #butView').click(function(){
				
		
		document.getElementById('frmweeklyCapacityChart').submit();
		
		
	});
	CallFunction();	
});
function loadLocation(obj)
{
	var url     = "weeklyCapacityChart-db-get.php?requestType=loadLocation&companyId="+$(obj).val();
	var httpObj = $.ajax({url:url,async:false})
	$('#frmweeklyCapacityChart #cboLocation').html(httpObj.responseText);	
}
function alertx()
{
	$('#frmweeklyCapacityChart #cboLocation').validationEngine('hide');
}
function drawGraph(availblQty,planQty,actualQty,week)
{
	for(x=1;x<5;x++)
	{
		if(availblQty=="")
		{
			$('#chAvailable'+x+'').html(0);
			$('#chPlan'+x+'').html(0);
			$('#chActual'+x+'').html(0);
			$('#week'+x+'').html('');
			
			$('#divAvailable'+x+'').height(1);
			$('#divPlan'+x+'').height(1);
			$('#divActual'+x+'').height(1);
		}
		else
		{
			var availableWidth = parseFloat(parseFloat(availblQty[x])/4000);
			var planWidth 	   = parseFloat(parseFloat(planQty[x])/4000);
			var actualWidth    = parseFloat(parseFloat(actualQty[x])/4000);
			
			$('#chAvailable'+x+'').html(availblQty[x]);
			$('#chPlan'+x+'').html(planQty[x]);
			$('#chActual'+x+'').html(actualQty[x]);
			$('#week'+x+'').html('( '+week[x]+' )');
			
			$('#divAvailable'+x+'').height(availableWidth);
			$('#divPlan'+x+'').height(planWidth);
			$('#divActual'+x+'').height(actualWidth);
		}
	}
}
function getDetails()
{
	if($('#cboCompany').val()!="")
		{
			if($('#cboLocation').val()=="")
			{
				$('#frmweeklyCapacityChart #cboLocation').validationEngine('showPrompt', 'Please Choose Location!','fail');
				var t=setTimeout("alertx()",3000);
				return;
			}		
		}
		var requestType = 'getDetails';
		var url = "weeklyCapacityChart-db-get.php";
			var obj = $.ajax({
				url:url,
				dataType: "json",  
				data:$("#frmweeklyCapacityChart").serialize()+'&requestType='+requestType,
				async:false,
				success:function(json)
				{
					drawGraph(json.available,json.plan,json.actual,json.week)
				},
				error:function(xhr,status)
				{
						
				}		
		});
}
function CallFunction() {
      setInterval("document.getElementById('frmweeklyCapacityChart').submit();", 60000);
    }
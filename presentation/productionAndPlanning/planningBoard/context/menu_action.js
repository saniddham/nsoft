$(document).ready(function ()
   {
      $("#btnLocateClose").click(function (e)
      {
         HideAllocateLocation();
      });
      $("#butAllocate").click(function (e)
      {
		 var val = allocateLocation(); // fire on this function in planing board JS
         HideAllocateLocation();
		 if(val = 'success')
		 {
			 location.reload();
		 }
      });
	  
	  $("#btnDelDateClose").click(function (e)
      {
         HideChangeDeliveryDate();
      });
	  $("#butChange").click(function (e)
      {
		 alert("OK");
         HideChangeDeliveryDate();
      });
	  
	  $("#btnSplitQtyClose").click(function (e)
      {
         HideSplitOrders();
      });
	  $("#btnSetCapacityClose").click(function (e)
      {
         HideSetCapacity();
      });
	  $("#butSplit").click(function (e)
      {
		 var val = splitStrip(); // fire on this function in planing board JS
         HideSplitOrders();
		 if(val = 'success')
		 {
		 	location.reload();
		 }
	});
	$("#butCapacity").click(function (e)
      {
		 var val = setCapcity(); // fire on this function in planing board JS
         HideSetCapacity();
		 if(val == true)
		 {
		 	location.reload();
		 }
	});
	$("#butEditSave").click(function (e)
      {
		 var val = editStrip(); // fire on this function in planing board JS
         HideEditStrip();
		 if(val = 'success')
		 {
		 	location.reload();
		 }
	});
	
	$("#btnEditOrderClose").click(function (e)
      {
         HideEditStrip();
      });
	  
	  $("#btnProDateClose").click(function (e)
      {
         HideChangeProductionStartDate();
      });
	  $("#butProChange").click(function (e)
      {
		 alert("OK");
         HideChangeProductionStartDate();
      });
	  
	  $("#btnMemoClose").click(function (e)
      {
         HideOrderMemo();
      });
	  $("#butMemoSave").click(function (e)
      {
		 applyOrderMemo(); // fire on this function in planing board JS
         HideOrderMemo();
      });

});
//-----------------------------------------------------------------------------------------------------------
function show(){
	$('.vs-context-menu').hide();
    var menu_id = $('a').attr('id');
    alert(menu_id + ' :: Show Daily Quantity Function fired from menu_action.js file');
}
function planed(){
	$('.vs-context-menu').hide();
    var menu_id = $('a').attr('id');
    alert(menu_id + ' :: Planed vs Actual Function fired from menu_action.js file');
    
}
function locat(){
	$('.vs-context-menu').hide();
    var menu_id = $('a').attr('id');
	ShowAllocateLocation(true);
    //alert(menu_id + ' :: Locate Another Function fired from menu_action.js file');
}
function capacity(){
	$('.vs-context-menu1').hide();
    var menu_id = $('a').attr('id');
	ShowSetCapacity(true);
    //alert(menu_id + ' :: Locate Another Function fired from menu_action.js file');
}
function delDate(){
	$('.vs-context-menu').hide();
    var menu_id = $('a').attr('id');
	ShowChangeDeliveryDate(true);
    //alert(menu_id + ' :: Del Date Function fired from menu_action.js file');
}
function splitOrder(){
	$('.vs-context-menu').hide();
	document.getElementById("txtSplitQty").value = '0';
    var menu_id = $('a').attr('id');
	ShowSplitOrders();
    //alert(menu_id + ' :: Split Orders Function fired from menu_action.js file');
}
function edit(){
	$('.vs-context-menu').hide();
    var menu_id = $(this).attr('id');
	editStripData();

}
function undoSplit(){
	$('.vs-context-menu').hide();
    var menu_id = $('a').attr('id');
    alert(menu_id + ' :: Undo Spli Function fired from menu_action.js file');
}
function crosLocat(){
	$('.vs-context-menu').hide();
    var menu_id = $('a').attr('id');
    alert(menu_id + ' :: Other Splits Across Location Function fired from menu_action.js file');
}
function joinOrder(){
	$('.vs-context-menu').hide();
    var menu_id = $('a').attr('id');
    alert(menu_id + ' :: Join Order Function fired from menu_action.js file');
}
function undoJoin(){
	$('.vs-context-menu').hide();
    var menu_id = $('a').attr('id');
    alert(menu_id + ' :: Undo Join Function fired from menu_action.js file');
}
function psdDate(){
	$('.vs-context-menu').hide();
    var menu_id = $('a').attr('id');
	ShowChangeProductionStartDate(true);
    //alert(menu_id + ' :: PSD Function fired from menu_action.js file');
}
function memo(){
	$('.vs-context-menu').hide();
    var menu_id = $('a').attr('id');
	ShowOrderMemo(true);
   // alert(menu_id + ' :: Order Memo Function fired from menu_action.js file');
}
function apply(){
	$('.vs-context-menu').hide();
    var menu_id = $('a').attr('id');
    alert(menu_id + ' :: Apply/Remove Startup Allowance Function fired from menu_action.js file');
}
function initial(){
	$('.vs-context-menu').hide();
    var menu_id = $('a').attr('id');
    var val = sendInitial(); // fire on this function in planing board JS
	if(val = 'success')
	{
		location.reload();
	}
}
function cancel(){
	$('.vs-context-menu').hide();
    var menu_id = $('a').attr('id');
    var val = cancelOrder(); // fire on this function in planing board JS
	if(val = 'success')
	{
		location.reload();
	}
}
//-----------------------------------------------------------------------------------------------------------
//====================================Allocate Location Popup Function=======================================
function ShowAllocateLocation(modal)
{
  $("#allocLocate").show();
  $("#allocDialog").fadeIn(300);

  if (modal)
  {
	 $("#allocLocate").unbind("click");
  }
  else
  {
	 $("#allocLocate").click(function (e)
	 {
		HideAllocateLocation();
	 });
  }
}
function HideAllocateLocation()
{
  $("#allocLocate").hide();
  $("#allocDialog").fadeOut(300);
}
function ShowSetCapacity(modal)
{
  $("#setCapacity").show();
  $("#setCapacitydialog").fadeIn(300);

  if (modal)
  {
	 $("#setCapacity").unbind("click");
  }
  else
  {
	 $("#setCapacity").click(function (e)
	 {
		HideSetCapacity();
	 });
  }
}
function HideSetCapacity()
{
  $("#setCapacity").hide();
  $("#setCapacitydialog").fadeOut(300);
  document.getElementById("txtCapacity").value = "";
}
//====================================Allocate Location Popup Function=======================================
//====================================Change Delivery Date Popup Function====================================
function ShowChangeDeliveryDate(modal)
{
  $("#changeDelDate").show();
  $("#changeDeldialog").fadeIn(300);

  if (modal)
  {
	 $("#changeDelDate").unbind("click");
  }
  else
  {
	 $("#changeDelDate").click(function (e)
	 {
		HideChangeDeliveryDate();
	 });
  }
}
function HideChangeDeliveryDate()
{
  $("#changeDelDate").hide();
  $("#changeDeldialog").fadeOut(300);
}
//====================================Change Delivery Date Popup Function====================================
//======================================Split Orders Popup Function==========================================
function ShowSplitOrders(modal)
{
  $("#splitQty").show();
  $("#splitQtydialog").fadeIn(300);

  if (modal)
  {
	 $("#splitQty").unbind("click");
  }
  else
  {
	 $("#splitQty").click(function (e)
	 {
		HideSplitOrders();
	 });
  }
}
function HideSplitOrders()
{
  $("#splitQty").hide();
  $("#splitQtydialog").fadeOut(300);
}
//======================================Split Orders Popup Function==========================================
//======================================Edit Strip Function==========================================
function editStripData(modal)
{
  $("#editOrder").show();
  $("#editOrderdialog").fadeIn(300);

  if (modal)
  {
	 $("#editOrder").unbind("click");
  }
  else
  {
	 $("#editOrder").click(function (e)
	 {
		HideEditStrip();
	 });
  }
}
function HideEditStrip()
{
  $("#editOrder").hide();
  $("#editOrderdialog").fadeOut(300);
}
//======================================Edit Strip Function==========================================
//====================================Change Production Start Date Popup Function====================================
function ShowChangeProductionStartDate(modal)
{
  $("#changeProDate").show();
  $("#changeProDatedialog").fadeIn(300);

  if (modal)
  {
	 $("#changeProDate").unbind("click");
  }
  else
  {
	 $("#changeProDate").click(function (e)
	 {
		HideChangeProductionStartDate();
	 });
  }
}
function HideChangeProductionStartDate()
{
  $("#changeProDate").hide();
  $("#changeProDatedialog").fadeOut(300);
}
//====================================Change Production Start Date Popup Function====================================
//==========================================Order Memo Popup Function========================================
function ShowOrderMemo(modal)
{
  $("#orderMemo").show();
  $("#orderMemoDeldialog").fadeIn(300);

  if (modal)
  {
	 $("#orderMemo").unbind("click");
  }
  else
  {
	 $("#orderMemo").click(function (e)
	 {
		HideChangeDeliveryDate();
	 });
  }
}
function HideOrderMemo()
{
  $("#orderMemo").hide();
  $("#orderMemoDeldialog").fadeOut(300);
}
//==========================================Order Memo Popup Function========================================
<?php 
	session_start();
	$backwardseperator = "../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response 	= array('type'=>'', 'msg'=>'');
	$sql 		= "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result 	= $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId  = $row['intCompanyId']; 
	}
	$locationId 	= $_SESSION["CompanyID"];
	////////////////////////// main parameters ////////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	//$id 			= $_REQUEST['cboSearch'];
	///////////////////// plan team header parameters ////////////////////
	
	///////////////////// plan team detail parameters /////////////////////
	$details 		= json_decode($_REQUEST['planStripArray'], true);
	$Qtydetails 	= json_decode($_REQUEST['qtyArray'], true);
	
	print_r($grnNumbers);
	
	if($requestType=='edit')
	{
		if(count($details) != 0)
		{
			foreach($details as $detail)
			{
				$intOrderNo			= trim(($detail['intOrderNo']==''?'NULL':"'".$detail['intOrderNo']."'")); // Primary Key
				$intOrderYear		= trim(($detail['intOrderYear']==''?'NULL':"'".$detail['intOrderYear']."'")); // Primary Key
				$strSalesOrder	    = trim(($detail['strSalesOrder']==''?'NULL':"'".$detail['strSalesOrder']."'")); // Primary Key
				$intSplitId			= trim(($detail['intSplitId']==''?'NULL':"'".$detail['intSplitId']."'")); // Primary Key
				//$intTeamNo			= trim(($detail['intTeamNo']==''?'NULL':"'".$detail['intTeamNo']."'"));
				$intLearningCuveId	= trim(($detail['intLearningCuveId']==''?'NULL':"'".$detail['intLearningCuveId']."'"));
				$dblCellLeft		= trim(($detail['dblCellLeft']==''?'NULL':"'".$detail['dblCellLeft']."'"));
				$startDate			= trim(($detail['startDate']==''?'NULL':"'".$detail['startDate']."'"));
				$startTime			= trim(($detail['startTime']==''?'NULL':"'".$detail['startTime']."'"));
				$startTimeLeft		= trim(($detail['startTimeLeft']==''?'NULL':"'".$detail['startTimeLeft']."'"));
				$endDate			= trim(($detail['endDate']==''?'NULL':"'".$detail['endDate']."'"));
				$endTime			= trim(($detail['endTime']==''?'NULL':"'".$detail['endTime']."'"));
				$totalHours			= trim(($detail['totalHours']==''?'NULL':"'".$detail['totalHours']."'"));
				$dblQty				= trim(($detail['dblQty']==''?'NULL':"'".$detail['dblQty']."'"));
				$dblActQty			= trim(($detail['dblActQty']==''?'NULL':"'".$detail['dblActQty']."'"));
				$strOrderMemo		= trim(($detail['strOrderMemo']==''?'NULL':"'".$detail['strOrderMemo']."'"));
				$intPlanId		    = trim(($detail['intPlanId']==''?'NULL':"'".$detail['intPlanId']."'"));
				$printType		    = trim(($detail['printType']==''?'NULL':"'".$detail['printType']."'"));
				$mainTeam		    = trim(($detail['mainTeam']==''?'NULL':"'".$detail['mainTeam']."'"));
				$subTeam		    = trim(($detail['subTeam']==''?'NULL':"'".$detail['subTeam']."'"));
				
				$sql = "DELETE FROM `plan_stripes`
						WHERE
						plan_stripes.intPlanId 			= $intPlanId AND
						plan_stripes.intOrderNo 		= $intOrderNo AND
						plan_stripes.intOrderYear 		= $intOrderYear AND
						plan_stripes.strSalesOrder		= $strSalesOrder AND
						plan_stripes.intLocationId		= $locationId AND
						plan_stripes.intCompanyId  		= $companyId AND
						plan_stripes.intSplitId  		= $intSplitId";
				$db->RunQuery($sql);

				$sql = "INSERT INTO `plan_stripes` 
						(`intPlanId`,`intOrderNo`,`intOrderYear`,
						`strSalesOrder`,`intSplitId`,`intLocationId`,
						`intCompanyId`,`intMainTeam`,`intSubTeam`,`intPrintType`,`intLearningCuveId`,
						`dbCellLeft`,`startDate`,`startTime`,
						`startTimeLeft`,`endDate`,`endTime`,
						`totalHours`,`dblQty`,`dblCompleteQty`,
						`intStatus`,`strOrderMemo`,`intCreator`,
						dtmCreateDate) 
						VALUES 
						($intPlanId,$intOrderNo,$intOrderYear,
						$strSalesOrder,$intSplitId,'$locationId',
						'$companyId',$mainTeam,$subTeam,$printType,$intLearningCuveId,
						$dblCellLeft,$startDate,$startTime,
						$startTimeLeft,$endDate, $endTime, 
						$totalHours, $dblQty,$dblActQty,
						'0',$strOrderMemo,'$userId',
						now())";			
				
				$finalResult = $db->RunQuery($sql);
				
				$sqlStripId = " SELECT intStripId 
								FROM plan_stripes 
								WHERE intPlanId=$intPlanId AND 
								intSplitId=$intSplitId AND 
								intLocationId=$locationId 
								AND intCompanyId=$companyId ";
								
				$SIDResult = $db->RunQuery($sqlStripId);
				$rowSId = mysqli_fetch_array($SIDResult);
				
				$sqlDel = " DELETE FROM plan_strip_details 
							WHERE intPlanId = $intPlanId AND intSplitId =$intSplitId ";
				$ResultDel = $db->RunQuery($sqlDel);
							
				$intPlanId = str_replace("'","",$intPlanId);
				$intSplitId = str_replace("'","",$intSplitId);
				$stripId = $intPlanId.$intSplitId;
				
				foreach($Qtydetails as $Qtydetail)
				{
					$intStripId			= trim(($Qtydetail['stripId']==''?'NULL':$Qtydetail['stripId'])); 
					$date				= trim(($Qtydetail['date']==''?'NULL':"'".$Qtydetail['date']."'")); 
					$qty			    = trim(($Qtydetail['qty']==''?'NULL':"'".$Qtydetail['qty']."'"));
					
					if($stripId==$intStripId) 
					{
						$sql = "INSERT INTO plan_strip_details 
								(
									intStripId,
									intPlanId,
									intSplitId, 
									intCompanyId,
									intLocationId,
									intSubTeam,
									dtmDate, 
									dblQty
								)
								VALUES
								(
									'".$rowSId['intStripId']."',
									$intPlanId,
									$intSplitId,
									$companyId,
									$locationId,
									$subTeam,
									$date, 
									$qty
								);";
						$Result = $db->RunQuery($sql);
					}
				}
			}
		}
		if($finalResult)
		{
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	else if($requestType=='changeLocation')
	{
		$orderNo 		= $_REQUEST['orderNo'];
		$orderYear 		= $_REQUEST['orderYear'];
		$salesOrder 	= $_REQUEST['salesOrder'];
		$splitId 		= $_REQUEST['splitId'];
		$newLocation 	= $_REQUEST['locationId'];
		$qty			= $_REQUEST['earlyQty'];
		$planId 		= $_REQUEST['planId'];
		
		if($newLocation != '')
		{	
			$sql = "DELETE FROM plan_stripes 
					WHERE
					intPlanId = '$planId' AND 
					intOrderNo = '$orderNo' AND 
					intOrderYear = '$orderYear' AND 
					strSalesOrder = '$salesOrder' AND 
					intSplitId = '$splitId' AND 
					intLocationId = '$locationId' ";
			$result = $db->RunQuery($sql);
			
				$sql = "SELECT
				COUNT(*) AS 'no',
				plan_allocateorders.intPlanId,
				plan_allocateorders.intPlanNo,
				plan_allocateorders.intOrderYear,
				plan_allocateorders.intSplitId,
				plan_allocateorders.strSalesOrder,
				plan_allocateorders.strStyle,
				plan_allocateorders.strGraphic,
				plan_allocateorders.strCombo,
				plan_allocateorders.intCustomerId,
				plan_allocateorders.strProcess,
				plan_allocateorders.dtPSD,
				plan_allocateorders.dtDeliveryDate,
				plan_allocateorders.dblQty,
				plan_allocateorders.intLocationId,
				plan_allocateorders.strIsAllocated,
				plan_allocateorders.strIsPlaned,
				plan_allocateorders.intCreator,
				plan_allocateorders.intColours,
				plan_allocateorders.intStrocks,
				plan_allocateorders.intPanels,
				plan_allocateorders.intPrintSize
				FROM
				plan_allocateorders
				WHERE
				intPlanId = '$planId' AND 
				intPlanNo = '$orderNo' AND 
				intOrderYear = '$orderYear' AND 
				strSalesOrder = '$salesOrder' AND 
				intSplitId = '$splitId' AND 
				intLocationId = '$locationId' ";
				
				$result = $db->RunQuery($sql);
				$row= mysqli_fetch_array($result);
				if($row['no']==0)
				{
				$sql = "INSERT INTO `plan_allocateorders` 
							(`intPlanId`,`intPlanNo`,`strSalesOrder`,
							 `intOrderYear`,`intSplitId`,`strStyle`,
							 `strGraphic`,`strCombo`,`intCustomerId`,
							 `strProcess`,`dtPSD`,`dtDeliveryDate`,
							 `dblQty`,`intLocationId`,`strIsAllocated`,
							 `strIsPlaned`,`intColours`,`intStrocks`,
							 `intPanels`,`intPrintSize`,`intCreator`,
							 dtmCreateDate) 
							VALUES 
							('$planId','$orderNo','$salesOrder',
							 '$orderYear','$splitId','".$row['strStyle']."',
							 '".$row['strGraphic']."','".$row['strCombo']."','".$row['intCustomerId']."', 
							 '".$row['strProcess']."', '".$row['dtPSD']."', '".$row['dtDeliveryDate']."', 
							 '$qty', '$newLocation', 'Y', 
							 'Y','".$row['intColours']."','".$row['intStrocks']."',
							 '".$row['intPanels']."','".$row['intPrintSize']."','$userId',
							 now())";
				$fnResult = $db->RunQuery($sql);
				}
				else
				{
				$sql = "UPDATE plan_allocateorders
						SET  intLocationId	='$newLocation',
							 dblQty		='$qty',
							 strIsPlaned	='Y',
							 intModifyer	='$userId'
						WHERE 
							intPlanId = '$planId' AND 
							intPlanNo = '$orderNo' AND 
							intOrderYear = '$orderYear' AND 
							strSalesOrder = '$salesOrder' AND 
							intSplitId = '$splitId' AND 
							intLocationId = '$locationId' ";
			
			$fnResult = $db->RunQuery($sql);
				}
			
			if(($fnResult))
			{
				echo "success";
			}
		}
	}
	else if($requestType=='cancel')
	{
		$orderNo 		= $_REQUEST['orderNo'];
		$orderYear 		= $_REQUEST['orderYear'];
		$salesOrder 	= $_REQUEST['salesOrder'];
		$splitId 		= $_REQUEST['splitId'];
		$planId 		= $_REQUEST['planId'];
		$qty			= $_REQUEST['earlyQty'];
		$style 			= $_REQUEST['style'];
		$graphic 		= $_REQUEST['graphic'];
		$combo 			= $_REQUEST['combo'];
		$cusId 			= $_REQUEST['cusId'];
		$process 		= $_REQUEST['process'];
		$psd			= $_REQUEST['psd'];	
		$delDate 		= $_REQUEST['delDate'];

		
		$sql = "DELETE FROM `plan_stripes`
				WHERE 
				(`intPlanId`='$planId' AND 
				 `intOrderNo`='$orderNo' AND
				 `intOrderYear`='$orderYear' AND 
				 `strSalesOrder`='$salesOrder' AND 
				 `intSplitId`='$splitId' AND 
				 `intLocationId`='$locationId')";
		$result = $db->RunQuery($sql);
		
		$sql = "SELECT
				COUNT(*) AS 'no',
				plan_allocateorders.intPlanId,
				plan_allocateorders.intPlanNo,
				plan_allocateorders.intOrderYear,
				plan_allocateorders.intSplitId,
				plan_allocateorders.strSalesOrder,
				plan_allocateorders.strStyle,
				plan_allocateorders.strGraphic,
				plan_allocateorders.strCombo,
				plan_allocateorders.intCustomerId,
				plan_allocateorders.strProcess,
				plan_allocateorders.dtPSD,
				plan_allocateorders.dtDeliveryDate,
				plan_allocateorders.dblQty,
				plan_allocateorders.intLocationId,
				plan_allocateorders.strIsAllocated,
				plan_allocateorders.strIsPlaned,
				plan_allocateorders.intCreator,
				plan_allocateorders.intColours,
				plan_allocateorders.intStrocks,
				plan_allocateorders.intPanels,
				plan_allocateorders.intPrintSize
				FROM
				plan_allocateorders
				WHERE
				`intPlanNo`='$orderNo' AND 
				`intOrderYear`='$orderYear' AND 
				`strSalesOrder`='$salesOrder' AND 
				`intLocationId`='$locationId' AND 
				`intSplitId`='$splitId' AND
				`intPlanId`='$planId'";
				
				$result = $db->RunQuery($sql);
				$row= mysqli_fetch_array($result);
				if($row['no']==0)
				{
					$sql = "INSERT INTO `plan_allocateorders` 
							(`intPlanId`,`intPlanNo`,`strSalesOrder`,
							 `intOrderYear`,`intSplitId`,`strStyle`,
							 `strGraphic`,`strCombo`,`intCustomerId`,
							 `strProcess`,`dtPSD`,`dtDeliveryDate`,
							 `dblQty`,`intLocationId`,`strIsAllocated`,
							 `strIsPlaned`,`intColours`,`intStrocks`,
							 `intPanels`,`intPrintSize`,`intCreator`,
							 dtmCreateDate) 
							VALUES 
							('$planId','$orderNo','$salesOrder',
							 '$orderYear','$splitId','$style',
							 '$graphic','$combo','$cusId', 
							 '$process', '$psd', '$delDate', 
							 '$qty', '$locationId', 'Y', 
							 'N','".$row['intColours']."','".$row['intStrocks']."',
							 '".$row['intPanels']."','".$row['intPrintSize']."','$userId',
							 now())";
				
					$fnResult = $db->RunQuery($sql);
				}
				else
				{
					$sql = "UPDATE plan_allocateorders 
							SET
							dblQty = '$qty' , 
							strIsPlaned = 'N' , 
							intModifyer = '$userId' , 
							dtmModifyDate = now()
							WHERE
							intPlanId = '$planId' AND 
							intPlanNo = '$orderNo' AND 
							intOrderYear = '$orderYear' AND 
							intSplitId = '$splitId' AND 
							strSalesOrder = '$salesOrder' ;";
					$fnResult = $db->RunQuery($sql);
				}		
		if(($fnResult))
		{
			echo "success";
		}
	}
	else if($requestType=='initial')
	{
		$orderNo 		= $_REQUEST['orderNo'];
		$orderYear 		= $_REQUEST['orderYear'];
		$salesOrder 	= $_REQUEST['salesOrder'];
		$splitId 		= $_REQUEST['splitId'];
		$planId 		= $_REQUEST['planId'];
		
		$sql = "DELETE FROM `plan_stripes`
				WHERE 
				(`intPlanId`='$planId' AND 
				 `intOrderNo`='$orderNo' AND
				 `intOrderYear`='$orderYear' AND 
				 `strSalesOrder`='$salesOrder' AND 
				 `intSplitId`='$splitId' AND 
				 `intLocationId`='$locationId')";
		$result = $db->RunQuery($sql);
		
		if(($fnResult))
		{
			echo "success";
		}
	}
	else if($requestType=='split')
	{
		$orderNo 		= $_REQUEST['orderNo'];
		$orderYear 		= $_REQUEST['orderYear'];
		$salesOrder 	= $_REQUEST['salesOrder'];
		$splitId 		= $_REQUEST['splitId'];
		$planId 		= $_REQUEST['planId'];
		$qty			= $_REQUEST['qty'];
		$style 			= $_REQUEST['style'];
		$graphic 		= $_REQUEST['graphic'];
		$combo 			= $_REQUEST['combo'];
		$cusId 			= $_REQUEST['cusId'];
		$process 		= $_REQUEST['process'];
		$psd			= $_REQUEST['psd'];	
		$delDate 		= $_REQUEST['delDate'];
		$restQty 		= $_REQUEST['restQty'];
		$noOfColors 	= $_REQUEST['noOfColors'];
		$noOfStrocks 	= $_REQUEST['noOfStrocks'];
		$noOfPanels 	= $_REQUEST['noOfPanels'];
		$printSize 		= $_REQUEST['printSize'];
		
		$sqlUpdate = "UPDATE plan_stripes 
						SET
						dblQty = '$restQty' 
						WHERE
						intPlanId = '$planId' AND 
						intOrderNo = '$orderNo' AND 
						intOrderYear = '$orderYear' AND 
						strSalesOrder = '$salesOrder' AND 
						intSplitId = '$splitId' AND 
						intLocationId = '$locationId' AND 
						intCompanyId = '$companyId' ;";

		$ResultUpd = $db->RunQuery($sqlUpdate);
	
		$sqlSplitId = "SELECT MAX(intSplitId) as maxSplitId
						FROM plan_allocateorders
						WHERE 
						intPlanId = $planId AND
						intPlanNo = $orderNo AND
						intOrderYear = $orderYear AND
						strSalesOrder = '$salesOrder' AND
						intLocationId = $locationId ";
		
		$splitIdResult = $db->RunQuery($sqlSplitId);
		while($row = mysqli_fetch_array($splitIdResult))
		{
			$newSplitId = $row['maxSplitId']+1;
			
			$sql = "INSERT INTO `plan_allocateorders` 
							(`intPlanId`,`intPlanNo`,`strSalesOrder`,
							 `intOrderYear`,`intSplitId`,`strStyle`,
							 `strGraphic`,`strCombo`,`intCustomerId`,
							 `strProcess`,`dtPSD`,`dtDeliveryDate`,
							 `dblQty`,`intLocationId`,`strIsAllocated`,
							 `strIsPlaned`,`intColours`,`intStrocks`,
							 `intPanels`,`intPrintSize`,`intCreator`,
							 dtmCreateDate) 
							VALUES 
							('$planId','$orderNo','$salesOrder',
							 '$orderYear','$newSplitId','$style',
							 '$graphic','$combo','$cusId', 
							 '$process', '$psd', '$delDate', 
							 '$qty', '$locationId', 'Y', 
							 'Y','$noOfColors','$noOfStrocks',
							 '$noOfPanels','$printSize','$userId',
							 now())";
			echo $sql;
			$fnResult = $db->RunQuery($sql);
			
			if(($fnResult))
			{
				echo "success";
			}
		}
	}
	else if($requestType=='editStrip')
	{
		$planId 		= $_REQUEST['planId'];
		$printSize 		= $_REQUEST['printSize'];
		$color 			= ($_REQUEST['color']=="" || $_REQUEST['color']==0?1:$_REQUEST['color']);
		$strocks 		= ($_REQUEST['strocks']=="" || $_REQUEST['strocks']==0?1:$_REQUEST['strocks']);
		$panel 			= ($_REQUEST['panel']=="" || $_REQUEST['panel']==0?1:$_REQUEST['panel']);
		$splitId 		= $_REQUEST['splitId'];
		
		$sql = "UPDATE plan_allocateorders 
				SET
				intColours = '$color' , 
				intStrocks = '$strocks' , 
				intPanels = '$panel' , 
				intPrintSize = $printSize
				WHERE
				intPlanId = '$planId' AND intSplitId = '$splitId';";
		
		$fnResult = $db->RunQuery($sql);
			
			if(($fnResult))
			{
				echo "success";
			}
	}
	else if($requestType=='setCapacity')
	{
		$teamId 		= $_REQUEST['teamId'];
		$fromDate 		= $_REQUEST['fromDate'];
		$toDate 		= $_REQUEST['toDate'];
		$capacity 		= ($_REQUEST['capacity']==''?0:$_REQUEST['capacity']);
		$nextTeam	  	= $_REQUEST['nextTeam'];
		$mainTeam	  	= $_REQUEST['mainTeam'];
		
		$sqlGetHurs = " SELECT DISTINCT MAX(dblWorkingHours) maxHours,MAX(intMaxCapacity) maxCapacity 
						FROM plan_calender 
						WHERE intTeamId= $teamId AND
						dtmDate BETWEEN DATE('$fromDate') AND DATE ('$toDate') AND
						intCompanyId = '$companyId' AND 
						intLocationId = '$locationId' ";
		$resultGetHrs = $db->RunQuery($sqlGetHurs);
		$rowGetHrs = mysqli_fetch_array($resultGetHrs);
		
		$sql = " UPDATE plan_calender 
					SET
					intMaxCapacity = ROUND(($capacity/'".$rowGetHrs['maxHours']."'),0) ,  
					intModifyer = $userId , 
					dtmModifyDate = now()
					WHERE dtmDate BETWEEN DATE('$fromDate') AND DATE ('$toDate') AND
					intCompanyId = '$companyId' AND 
					intLocationId = '$locationId' AND 
					intTeamId = '$teamId' ;";
		$fnResult = $db->RunQuery($sql);
		
		if($nextTeam!=0)
		{
			$sqlMt = " SELECT DISTINCT MAX(intMaxCapacity) maxCapacity,MAX(dblWorkingHours) maxMainTmHours FROM plan_calender 
						WHERE 
						intCompanyId = '$companyId' AND 
						intLocationId = '$locationId' AND 
						dtmDate BETWEEN DATE('$fromDate') AND DATE ('$toDate') AND
						intTeamId = '$teamId' ";
						
			$fnResultMt = $db->RunQuery($sqlMt);
			$rowMt = mysqli_fetch_array($fnResultMt);
			
			if($capacity==0)
				$balCapacity = $rowGetHrs['maxCapacity'];
			else
				$balCapacity = $rowGetHrs['maxCapacity'] - round(($capacity/$rowMt['maxMainTmHours']),0);
			
			if($balCapacity>0)
			{
				$sqlUpd = " UPDATE plan_calender 
							SET
							intMaxCapacity = IFNULL(intMaxCapacity,0)+$balCapacity ,  
							intModifyer = $userId , 
							dtmModifyDate = now()
							WHERE dtmDate BETWEEN DATE('$fromDate') AND DATE ('$toDate') AND
							intCompanyId = '$companyId' AND 
							intLocationId = '$locationId' AND 
							intTeamId = '$nextTeam' ;";

				$fnResult = $db->RunQuery($sqlUpd);
			}
			
		}
			
			if(($fnResult))
			{
				echo "success";
			}
	}
	echo json_encode($response);
//============================================================================================
?>
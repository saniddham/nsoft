// JavaScript Document
	var DailyStartDate 		= startDate;
//...........................................
	var today 				= new Date();
	var dd 					= today.getDate(); 
	var mm 					= today.getMonth();
	var yyyy 				= today.getFullYear();
//...........................................
//...........................................
	var intStartMonth=0;
		
	var intStartYear 		= yyyy;
	if(mm>0)
	{
		intStartMonth 		= mm-1;
	}
	else
		intStartMonth   	= 11;
		
	var intStartDay			= dd;
	
	//var url_date="plan-db.php?id=getPreDate";
	//var htmlobj   = $.ajax({url:url_date,async:false})
	//if(htmlobj.responseText==0)
	//{
	//	intStartMonth=mm;
	//}
//...........................................
//...........................................
	var sId					= 0;
	var tId					= [];
	var removeStripId		= [];
	var h 					= 0;
	var t					= 0;
 	var i 					= 0;
	var x 					= 0;
	var arrS 				= [];
	var arrStrip  			= [];
	var arrNewStrip 		= [];
	var arrTeam 			= [];
	var arrCalender 		= [];
	var arrCurve			= [];
	var pub_beforeDragLeft 	= 0;
	var arrcalenderStatus	= [];
	var newSripId 			= 1;
	var dynStripId 			= '';
//............................................

$(document).ready(function(){
//--------------------------------Context Menu----------------------------------------
	$('.context').vscontext({menuBlock: 'vs-context-menu'});
//------------------------------------------------------------------------------------
/*  	$(".classTeam").droppable({
	drop: function(event, ui){
	var A = $(this).find("#"+x).attr('id');
	
	var teamId = $(this).attr('id');
	teamId = teamId.substring(4);

	var obMbar = $(this).offset();
	var obStrip = $("#"+x).offset();
	$("#"+x).offset({top:obMbar.top });
	
	//////////////calculation/////////////////////
	var stripLeft =  obStrip.left-obMbar.left;
	
	var cdays 	= 	(stripLeft/40);
	var A1			=	parseInt((stripLeft / 40))/21;
	
	var A2			= 	(cdays-A1).toFixed(2);
	var A3			=	parseInt(A2);
	var cellLeft 	= ((cdays-A1-A3).toFixed(4));
	
	var calDate	= new Date(intStartYear,intStartMonth,intStartDay+A3);
	
	var strStyle = (arrStrip[x.substring(4)]['style']);
	
	var Dd = calDate.getDate();
	var Mm = calDate.getMonth()+1;
	
	if(Dd<10)
		Dd = '0'+Dd;
	
	if(Mm<10)
		Mm = '0'+Mm;
		
	var newDate  = new String(calDate.getFullYear()+ "-"+Mm+"-"+Dd);
	
////////////////////////////////////// TIME CALCULATION ///////////////////////////////////////
	if(teamId=='nitial')
	{	
		callGetPosition();
		$("#"+x).width(100);
		arrStrip[x.substring(4)]['team'] 		= 0;
		callSetPosition();
		return;
	}
	
	var totMin 		= 	parseInt(arrCalender[newDate][teamId]['workingMinutes']);
	var startMin	=	parseInt((totMin*cellLeft).toFixed(2));
	
	arrStrip[x.substring(4)]['startDate'] 	= newDate;
	arrStrip[x.substring(4)]['startTime'] 	= parseInt(startMin);
	
	var temp_team = arrStrip[x.substring(4)]['team'];

	arrStrip[x.substring(4)]['team'] 		= teamId;
	
	arrStrip[x.substring(4)]['endDate'] 	= newDate;
	arrStrip[x.substring(4)]['endTime']		= parseInt(startMin);
	arrStrip[x.substring(4)]['stripLeft']	= stripLeft;
	arrStrip[x.substring(4)]['cellLeft']	= cellLeft;

	var tempWidth = $("#"+x).width();
	

	var stripWidth = calculateStripWidth(x.substring(4),newDate,cellLeft); 
	if(stripWidth!=0)
	{
		if(checkOverLap())
		stripWidth = 0;
	}
		
	if(stripWidth==0)
	{
		stripWidth = tempWidth;
		arrStrip[x.substring(4)]['team'] = 0;

		callGetPosition();
		
		$("#"+x).remove();
		
		setLastPossition();
		
		callSetPosition();
		return;
	}
	
	callGetPosition();
	
	if(teamId!='divInitial')
		$("#"+x).width(stripWidth);
	else
		$("#"+x).width(100);
	
	 callSetPosition();
	}
});
*/
//--------------------------------Drag and Drop---------------------------------------
	$(function() 
	{
		$( ".draggable" ).draggable({ revert: "invalid" });
		
		$( ".droppable" ).droppable({
		drop: function( event, ui ) {
			//alert(ui.html());
		ui.droppable = $(this);
		//alert(1);
		//################################################

		//var dateLeft = $(this).offset().left;
		var pos 				= ui.draggable.position();
		top_left 				= $(this).offset().left;
		
		//alert(top_left + '  ' +$(this).attr('id'));
		var date 				= $(this).attr('id');
		var dateLeft 			= (parseFloat(pos.left)-parseFloat(top_left));
		//alert(dateLeft);
		var id					= ui.draggable.attr('id');
		var planId  			= id.substring(0,((id.length)-1));
		var stripId 			= id;
		var teamId 				= $(this).parent().attr('id');
		//alert(arrTeam[teamId]['id']);
		//alert(arrCalender[teamId][date]['hours']);
			//create array strip wise
			var caption 		= ui.draggable.html();
			var index_start_qty = (caption.indexOf("("));
			var index_end_qty 	= (caption.indexOf(")"));
			var qty  			= caption.substring(index_start_qty+1,index_end_qty);
			//alert(id);
		if(!arrStrip[id])
		{
		var arrProperty 			 	= [];
			arrProperty['planId'] 	 	= planId
			arrProperty['qty'] 		 	= parseInt(qty);
			arrProperty['actQty'] 	 	= 0;
			arrProperty['orderYear'] 	= arrNewStrip[planId]['orderYear'];
			arrProperty['orderNo'] 	 	= arrNewStrip[planId]['orderNo'];
			arrProperty['salesId'] 	 	= arrNewStrip[planId]['salesOrder'];
			arrProperty['salesOrderNo'] = arrNewStrip[planId]['salesOrder'];
			arrProperty['cusId'] 		= arrNewStrip[planId]['cusId'];
			arrProperty['splitId'] 	 	= id.substring(((id.length)-1),(id.length));
			arrProperty['new'] 		 	= 1;
			arrProperty['totalHours']	= 0;
			arrProperty['team']			= arrTeam[teamId]['id'];
			arrProperty['removeStatus']	= 0;
			arrProperty['curve']		= 0;
			arrProperty['curveName']	= '';
			arrProperty['cellLeft']		= dateLeft;			
			arrProperty['startDate']	= date
			arrProperty['startTime']	= 0;
			arrProperty['endDate']		= date;
			arrProperty['endTime']		= 0;			
			arrProperty['memo']			= '';
			arrStrip[id] 				= arrProperty;
			arrProperty 				= null;
		}
		else
		{
			var i 						= parseInt(dateLeft / 40);
			if(i<=0)
			{
				addDay 					= (i);
				//date = arrStrip[id]['startDate'];
				var startDate 			= addDays(date,addDay);
				//alert(startDate);
				//alert(arrStrip[id]['startDate']);
				//var startDate = arrStrip[id]['startDate'];
			}
			else
				var startDate = date;
			
			arrStrip[id]['cellLeft']	= (dateLeft%40);
			arrStrip[id]['startDate']	= startDate;
			arrStrip[id]['endDate']		= startDate;	
			arrStrip[id]['team']		= arrTeam[teamId]['id'];
		}
		
		//alert(date);
		var planDays 					= getStripWidthCalculte(teamId,date,dateLeft,id);
			planDays =5;
			//planDays = planDays * 1
		var stripWidth 					= parseFloat((planDays*39));
		//alert(stripWidth);
		
		//$("#20121000496").css({position: 'absolute'});
		//callGetPosition();
		//$("#"+id).css("width",(stripWidth)+"px");
		
		$("#"+id).width(stripWidth)
		callSetPosition();
		//$("#20121000496").css({position: 'relative'});
	//	$("#"+id).style.position = 'relative'
		
		
		//alert(arrStrip[id]['startDate']);
		//################################################
		//var targetElem = $(this).parent().attr("id");
		//alert($(this).attr('id')); // GET TEAM NAME
//--------------------------------------------------------------------------------------
		//var id=ui.draggable.attr('id'); // This is Order Strip Id
		//var team = (targetElem).substring(4); // Team Id
		//var date = $(this).attr('id'); // Start Date
		//var isActive = $(this).attr('name'); // Day Status
		
		//var left_pos ="";
		//var top_pos ="";
//		
//		left_pos = $("#"+id).css("left");
		
		
		top_pos = $("#"+id).css("top");
		
//		$("#"+id).css("left",(parseInt(left_pos)-($("#"+id).offset().left - $(this).offset().left))+"px");
		$("#"+id).css("top",(parseInt(top_pos)+($(this).offset().top -$("#"+id).offset().top))+"px");
//		alert("Team Id: "+ team +" Date: "+ date +" Day Status: "+ isActive+" Strip Id: "+ id);
		
		//var pos = ui.draggable.position();
    	//alert( "left: " + pos.left + ", top: " + pos.top ); 
		
//--------------------------------------------------------------------------------------
		
		//ui.draggable.css({
//		    width: 100 + "px"
//		});
//		$("#"+id).mouseenter(function(){
//			$("#"+id).css("width",(30)+"px");
//		});
//		$("#"+id).mouseout(function(){
//			$("#"+id).css("width",(100)+"px");
//		});
		//setLastPossition();
		/*callGetPosition();
		
		$("#"+id).css("width",(100)+"px");
		callSetPosition();*/
	}
	});
	}).css({position: 'relative'});
//--------------------------------------------------------------------------------------
	$(".classGridBox").mouseout(function(){
		//$(this)[0].className ="";
		//$(this)[0].className ="classGridBox classTeam droppable ui-widget-header ui-droppable ";
	})
		
	$(".classGridBox").mousemove(function(){
		//$(this)[0].className ="";
		//$(this)[0].className ="classGridBox classTeam droppable ui-widget-header ui-droppable selectedcolor";
	});
//--------------------------------------------------------------------------------------
	$(".classGridBox").click(function(){
		//var team = ($(this).parent().attr('id')).substring(4);
		//var date = $(this).attr('id');
		//var isActive = $(this).attr('name');
		//alert(team +"/"+date +"/"+isActive);						
	});
//--------------------------------------------------------------------------------------
	$('#butRefresh').click(function(){
		location.reload();
	});
	$('#butSave').click(function(){
		value="[ ";
		for(n in arrStrip)
		{
			value += '{ "intOrderNo":"'+arrStrip[n]['orderNo']+'", "intOrderYear": "'+arrStrip[n]['orderYear']+'", "strSalesOrder": "'+arrStrip[n]['salesOrderNo']+'", "intSplitId": "'+parseInt(arrStrip[n]['splitId'])+'", "intTeamNo": "'+arrStrip[n]['team']+'", "intLearningCuveId": "'+0+'", "dblCellLeft": "'+arrStrip[n]['cellLeft']+'", "startDate": "'+arrStrip[n]['startDate']+'", "startTime": "'+arrStrip[n]['startTime']+'", "startTimeLeft": "'+0+'", "endDate": "'+arrStrip[n]['endDate']+'", "endTime": "'+arrStrip[n]['endTime']+'", "totalHours": "'+arrStrip[n]['totalHours']+'", "dblQty": "'+arrStrip[n]['qty']+'", "dblActQty": "'+arrStrip[n]['actQty']+'", "strOrderMemo": "'+arrStrip[n]['memo']+'", "intPlanId": "'+parseInt(arrStrip[n]['planId'])+'"},';
		}
		value = value.substr(0,value.length-1);
		value += " ]";
		//alert(value);
		if(value != '[ ]')
		{
			//alert("Save Process - Pending");
			requestType = 'edit';
			var url 	= "planningBoard-db-set.php";
			var obj 	= $.ajax({
				url:url,
				dataType: "json",  
				data:$("#frmPlanningBoard").serialize()+'&requestType='+requestType+'&planStripArray='+value,
				async:false,
				
				success:function(json)
				{
					//$('#frmPlanningBoard #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmPlanningBoard').get(0).reset();
						location.reload();
						var t=setTimeout("alertx()",1000);
						//setTimeout("window.location.reload();",1000)
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
				error:function(xhr,status)
				{						
					$('#frmPlanningBoard #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			 });
		}
	});
});

function getStripWidthCalculte(teamId,date,dateLeft,stripId)
{
	//alert(stripId);
	//alert(dateLeft);
	/////////////calculate start date/////////////
	if(dateLeft<0)
	{
		//alert(dateLeft);
		var i = parseInt(dateLeft / 40);
		if(i<=0)
			addDay 		= (i-1);
		var startDate 	= addDays(date,addDay);
		//alert(startDate);
		var hours	 	= parseFloat(arrCalender[teamId][startDate]['hours']);
		//alert(hours);
		var capacity 	= parseFloat(arrTeam[teamId]['capacity']);
		//alert(capacity);
		firstDayPlanQty = parseFloat((capacity*hours)*(((dateLeft % 40)*-1)/40));
		//alert(firstDayPlanQty);
		var firstWidth 	= ((dateLeft%40)*-1)/40;
		//alert(firstWidth);
	}
	else
	{
		var startDate 	= date;	
		//alert('startDate 	= '+startDate);
		var hours	 	= parseFloat(arrCalender[teamId][startDate]['hours']);
		var capacity 	= parseFloat(arrTeam[teamId]['capacity']);
		//alert('Hours 	= '+hours);
		//alert('Capacity = '+capacity);
		firstDayPlanQty = parseFloat((capacity*hours)*((40-(dateLeft%40))/40));
		//alert(firstDayPlanQty);
		var firstWidth 	= (40-(dateLeft%40))/40;
		//alert('firstWidth 	= '+firstWidth);
	}
	//alert(firstDayPlanQty);
	
	
	
	var qty 			= parseFloat(arrStrip[stripId]['qty']);
	//alert(firstDayPlanQty);
	var balQty 			= qty-parseFloat(firstDayPlanQty);
	//alert(balQty);
	if(balQty<0)
	{
		//alert('qty = ' + qty);
		//alert('capacity = ' + capacity);
		var x 			=  (qty/(capacity*hours));
		//var x =parseFloat((firstDayPlanQty*hours)*((40-(dateLeft%40))/40));
		alert(x);
		return (1);
		//return x;
		//alert(firstWidth);	
	}
	alert('blaaaaa');
	var daysWidthWise 	= 0;
	var cdate 			= addDays(startDate,1);
	//alert(startDate+' '+firstDayPlanQty)
	//alert(qty);
	while(balQty>0)
	{
		hours	 		= parseFloat(arrCalender[teamId][cdate]['hours']);
		capacity 		= parseFloat(arrTeam[teamId]['capacity']);
		capacity 		= hours*capacity;
		//alert(balQty);
		if(balQty>capacity)
		{
			balQty 		= parseFloat(balQty)-parseFloat(capacity);	
			daysWidthWise+=1;
			//alert(1);
			//alert(cdate+' '+capacity)
		}	
		else
		{
			
			if(balQty==capacity)
			{
				//alert(cdate+' '+capacity)
				balQty = 0;
				daysWidthWise+=1;	
				//alert(1);
			}	
			else
			{
				//alert(cdate+' '+balQty)
				//alert((balQty/capacity)*40);
				daysWidthWise+=(balQty/capacity);	
				//alert((balQty/capacity));
				balQty 	= 0;
				
			}
		}
		
		var cdate = addDays(cdate,1);
	}
	//alert(daysWidthWise+firstWidth);
	return(daysWidthWise+firstWidth);
	
}

function addDays(date,days)
{
			/////////////// get next date //////////////////
//			alert(days);
//			alert(date.substring(0,4));
//			alert(date.substring(5,7)-1);
//			alert(date.substring(8,10));
		var x 			= new Date(date.substring(0,4),date.substring(5,7)-1,date.substring(8,10));
		x.setDate(x.getDate() +days);
		//create date string //////////////////
		var Dd 			= x.getDate();
			var Mm 		= x.getMonth()+1;
			if(Dd<10)
				Dd 		= '0'+Dd;
			if(Mm<10)
				Mm 		= '0'+Mm;
		var cdate  		= new String(x.getFullYear()+ "-"+Mm+"-"+Dd);
		return cdate;
		///////////////////////////////////////////////	
}
//====================================================================================================
function callSetPosition()
{
	for(n in arrStrip)
	{
		//alert(arrS[n]);
		$("#"+(n)).offset({left:arrS[n]});
	}
}

function callGetPosition()
{
	//alert(arrStrip.length);
	for(n in arrStrip)
	{
		//alert(arrStrip[n]['removeStatus']);
		if(arrStrip[n]['removeStatus']==0)
		{
			try
			{
				var d3 	= $("#"+n).offset();
				var e 	= d3.left-0.5;
				//alert(e);
				arrS[n] = e;
			}
			catch(e)
			{
				
			}
		}
	}
}

function setLastPossition()
{
	if(arrStrip[x.substring(4)]['removeStatus']==0)
	{
		var orderStyle			= arrStrip[x.substring(4)]['orderStyle'];
		var style 				= arrStrip[x.substring(4)]['style'] ;
		var qty 				= arrStrip[x.substring(4)]['qty'] ;
		var smv 				= arrStrip[x.substring(4)]['smv'] ;
		
		arrStrip[x.substring(4)]['team'] = 0;
		
		var styleText 			= orderStyle + '( '+ qty + ' )';
		var newdiv 				= document.createElement('div');
		var nDrag 				= x;
		
		newdiv.setAttribute('id',nDrag);
		newdiv.setAttribute('class','strip');
		newdiv.style.position 	= 'relative';
		newdiv.style.zIndex 	= x.substring(4);
		newdiv.style.border		= " green solid 1px";
	
		newdiv.innerHTML 		= "<span style=\"opacity:1;\">"+styleText+"</span>";
		newdiv.oncontextmenu 	= ItemSelMenu;
	
		newdiv.onmousedown 		= func2;
		newdiv.onclick	   		= funcClickOnStrip;
		
		document.getElementById('divInitial').appendChild(newdiv);
				
		$(function() 
		{
			$("#"+x).draggable
			({
				revert: 'invalid' ,
			});
		});
	}
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function createAllStrips()
{
	 if(dd<10)
		dd = '0'+dd;
	if(mm<10)
		mm = '0'+mm;
	
	var lastMonthFirstDay = yyyy+'-'+mm+'-'+dd;
	
	for(n in arrStrip)
	{			
		if(arrStrip[n]['removeStatus']==0)
		{		
			var orderNo		 			= arrStrip[n]['orderNo'];
			var orderYear	 			= arrStrip[n]['orderYear'];
			var salesOrderId 			= arrStrip[n]['salesId'];
			var spliId		 			= arrStrip[n]['splitId'];
			var planId		 			= arrStrip[n]['planId'];
			var id 						= planId+spliId;		
			var salesOrderNo 			= arrStrip[n]['salesOrderNo'];	
			var qty 					= arrStrip[n]['qty'];		
			var styleText 				= salesOrderNo + '( '+ qty + ' )';
			
			var nDrag 					= n;
			dv 							= document.createElement('div');
			dv.setAttribute('id',nDrag);
			dv.style.zIndex 			= 1;
			dv.className				= "external-event draggable context"; 
			dv.style.position			= "relative";
			dv.style.border				= "green solid 1px";
			dv.onmousemove 				= function() { oll_MouseMove(this) };
			dv.onmouseout 				= function() { oll_MouseLeave(this) };

			dv.style.padding 			= "0.5em";
			dv.style.opacity			= "0.8";
			dv.style.width				= "50px";
			dv.style.height				= "12.5px";
		
			var date 					= arrStrip[n]['startDate'];
			var timeM 					= arrStrip[n]['startTime'];
			var cellLeft 				= arrStrip[n]['cellLeft'];
			var intTeam 				= arrStrip[n]['team'];
			
			if(intTeam>0)
			{
				paDate 					= arrStrip[n]['startDate'];
				var tdSize				= 0;		
				var completedQty		= arrStrip[n]['completeQty'];//getCompletedQty(n);
		
				if(completedQty!=0 )
				{
					var totQty			= arrStrip[n]['qty'];
					arrStrip[n]['qty']	= completedQty;
					var planDay 		= getStripWidthCalculte(intTeam,date,cellLeft,id);
						//planDay = 16;
					var completedWidth 	= parseFloat(planDay*39);
					
					tdSize				= completedWidth+0.000000000000009; // 0.000000000000009 is tolerence
					arrStrip[n]['qty']	= totQty;			
				}
			}
	
		if(tdSize==0)
		{
			dv.innerHTML 		= "<span>"+styleText+"</span>";
		}
		else		
			dv.innerHTML 		= ""+styleText+"<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td width=\""+tdSize+"px\" bgcolor=\"#33CC00\" height=5px ></td></tr></table>";
			
			if(intTeam==0)
			{
				$('#external-events').append(dv);
			}
			else
			{
				$('.'+ date+intTeam).append(dv);
			}
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			if(intTeam>0)
			{
				var planDays 	= getStripWidthCalculte(intTeam,date,cellLeft,id);
					//planDay = 16;
				var stripWidth	= parseFloat(planDays*39);
				$("#"+id).width(stripWidth+0.000000000000009) // 0.000000000000009 is tolerence
				$("#"+id).css("left",(parseInt(cellLeft))+"px");
			}
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		}
	}	
 }
function setObject2()
{
	$(function() {
	for(n in arrStrip)
	{
		if(arrStrip[n]['actQty']==0)
		{		
			$("#"+n).draggable({revert: 'invalid'});
		}
	}	
});	
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function splitStrip()
{
	if(dynStripId != '')
	{
		var date	 		= arrStrip[dynStripId]['startDate'];		
		var intTeam 		= arrStrip[dynStripId]['team'];
		var cellLeft		= arrStrip[dynStripId]['cellLeft'];
		var earlyQty		= arrStrip[dynStripId]['qty'];
		
		var fixedQty		= document.getElementById("splAvailbleQty").innerHTML;
		var splitQty		= document.getElementById("txtSplitQty").value;
	
		var restQty			= fixedQty-splitQty;

		arrStrip[dynStripId]['qty']= restQty; //updating the array....
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		if(intTeam>0)
		{
			var planDays 	= getStripWidthCalculte(intTeam,date,cellLeft,dynStripId);
				//planDay = 16;
			var stripWidth 	= parseFloat(planDays*39);
			$("#"+dynStripId).width(stripWidth+0.000000000000009) // 0.000000000000009 is tolerence
			$("#"+dynStripId).css("left",(parseInt(cellLeft))+"px");
		}
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		createObject_for_split(dynStripId,splitQty);
	}
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function createObject_for_split(activeSplitId,qty)
{
	if(dynStripId != '' && qty > 0)
	{
		var orderNo		 			= arrStrip[activeSplitId]['orderNo'];
		var orderYear	 			= arrStrip[activeSplitId]['orderYear'];
		var planId		 			= arrStrip[activeSplitId]['planId'];
		var spliId		 			= arrStrip[activeSplitId]['splitId'];
		var newSplitId   			= parseInt(spliId)+1;
		var id 			 			= planId+newSplitId;

		var salesOrderNo 			= arrStrip[activeSplitId]['salesOrderNo'];	
		var styleText 				= salesOrderNo + '( '+ qty + ' )';
		
		var nDrag 					= id;
		dv 							= document.createElement('div');
		dv.setAttribute('id',nDrag);
		dv.style.zIndex 			= 1;
		dv.className				= "external-event draggable context"; 
		dv.style.position			= "relative";
		dv.style.border				= "green solid 1px";
		dv.onmousemove 				= function() { oll_MouseMove(this) };
		dv.onmouseout 				= function() { oll_MouseLeave(this) };

		dv.style.padding 			= "0.5em";
		dv.style.opacity			= "0.8";
		dv.style.width				= "50px";
		dv.style.height				= "12.5px";
		dv.innerHTML 				= "<span>"+styleText+"</span>";
		
		var arrProp 				= [];
		arrProp['planId']			=	planId;
		arrProp['qty']				=	qty;
		arrProp['actQty']			=	'0';
		arrProp['orderYear']		=	orderYear;
		arrProp['orderNo']			=	orderNo;
		arrProp['salesId']			=	arrStrip[activeSplitId]['salesOrderNo'];
		arrProp['splitId']			=	newSplitId;
		arrProp['new']				=	'0';
		arrProp['totalHours']		= 	arrStrip[activeSplitId]['totalHours'];
		arrProp['team']				= 	'0';
		arrProp['removeStatus']		=	arrStrip[activeSplitId]['removeStatus'];
		arrProp['curve']			=	arrStrip[activeSplitId]['curve'];
		arrProp['curveName']		=	arrStrip[activeSplitId]['curveName'];
		arrProp['cellLeft']			=	arrStrip[activeSplitId]['cellLeft'];
		arrProp['startDate']		=	arrStrip[activeSplitId]['startDate'];
		arrProp['startTime']		=	arrStrip[activeSplitId]['startTime'];
		arrProp['endDate']			=	arrStrip[activeSplitId]['endDate'];
		arrProp['endTime']			=	arrStrip[activeSplitId]['endTime'];
		arrProp['salesOrderNo']		=	salesOrderNo;
		arrProp['style']			=	arrStrip[activeSplitId]['style'];
		arrProp['graphic']			=	arrStrip[activeSplitId]['graphic'];
		arrProp['combo']			=	arrStrip[activeSplitId]['combo'];
		arrProp['cusName']			=	arrStrip[activeSplitId]['cusName'];
		arrProp['cusId']			=	arrStrip[activeSplitId]['cusId'];
		arrProp['psd']				=	arrStrip[activeSplitId]['psd'];
		arrProp['delDate']			=	arrStrip[activeSplitId]['delDate'];
		arrProp['process']			=	arrStrip[activeSplitId]['process'];
		arrProp['memo']				=	arrStrip[activeSplitId]['memo'];
		
		arrStrip[id]=arrProp;

		$('#external-events').append(dv);
	}
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function applyOrderMemo()
{
	if(dynStripId != '')
	{
		var orderMemo 				 = document.getElementById("txtMemo").value;
		arrStrip[dynStripId]['memo'] = orderMemo;
	}
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function allocateLocation()
{
	if(dynStripId != '')
	{
		var orderNo		 = arrStrip[dynStripId]['orderNo'];
		var orderYear	 = arrStrip[dynStripId]['orderYear'];
		var salesOrderId = arrStrip[dynStripId]['salesId'];
		var spliId		 = arrStrip[dynStripId]['splitId'];
		var location 	 = document.getElementById("cboLocation").value;
		var earlyQty	 = arrStrip[dynStripId]['qty'];
		
		var url = "planningBoard-db-set.php?requestType=changeLocation&orderNo="+orderNo+"&orderYear="+orderYear+"&salesOrderId="+salesOrderId+"&spliId="+spliId+"&newLocation="+location+"&earlyQty="+earlyQty;
		var obj	 		 = $.ajax({url:url,async:false});
		return obj.responseText;
	}
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function cancelOrder()
{
	if(dynStripId != '')
	{
		var orderNo		 = arrStrip[dynStripId]['orderNo'];
		var orderYear	 = arrStrip[dynStripId]['orderYear'];
		var salesOrder   = arrStrip[dynStripId]['salesOrderNo'];
		var splitId		 = arrStrip[dynStripId]['splitId'];
		var planId		 = arrStrip[dynStripId]['planId'];	
		var style		 = arrStrip[dynStripId]['style'];
		var graphic		 = arrStrip[dynStripId]['graphic'];
		var combo	     = arrStrip[dynStripId]['combo'];
		var cusId		 = arrStrip[dynStripId]['cusId'];
		var process		 = arrStrip[dynStripId]['process'];
		var psd			 = arrStrip[dynStripId]['psd'];
		var delDate		 = arrStrip[dynStripId]['delDate'];
		var earlyQty	 = arrStrip[dynStripId]['qty'];
		
		var url = "planningBoard-db-set.php?requestType=cancel&orderNo="+orderNo+"&orderYear="+orderYear+"&salesOrder="+URLEncode(salesOrder)+"&splitId="+splitId+"&planId="+planId+"&earlyQty="+earlyQty+"&style="+URLEncode(style)+"&graphic="+URLEncode(graphic)+"&combo="+URLEncode(combo)+"&cusId="+cusId+"&process="+URLEncode(process)+"&psd="+psd+"&delDate="+delDate;
		
		var obj 		 = $.ajax({url:url,async:false});
		return obj.responseText;
	}
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function sendInitial()
{
	if(dynStripId != '')
	{
		var orderNo		 = arrStrip[dynStripId]['orderNo'];
		var orderYear	 = arrStrip[dynStripId]['orderYear'];
		var salesOrder   = arrStrip[dynStripId]['salesOrderNo'];
		var splitId		 = arrStrip[dynStripId]['splitId'];
		var planId		 = arrStrip[dynStripId]['planId'];
	
		var url = "planningBoard-db-set.php?requestType=initial&orderNo="+orderNo+"&orderYear="+orderYear+"&salesOrder="+salesOrder+"&splitId="+splitId+"&planId="+planId;
		var obj 		 = $.ajax({url:url,async:false});
		return obj.responseText;
	}
}
//====================================================================================================
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>All Mouse Functions Area>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
function oll_MouseMove(obj)
{
	var id = obj.id;
	dynStripId = id;
	document.getElementById("orderNo").innerHTML 			= arrStrip[id]['orderNo'];
	document.getElementById("salesOrderNo").innerHTML 		= arrStrip[id]['salesOrderNo'];
	document.getElementById("qty").innerHTML 				= arrStrip[id]['qty'];		
	document.getElementById("style").innerHTML 				= arrStrip[id]['style'];	
	document.getElementById("graphic").innerHTML 			= arrStrip[id]['graphic'];
	document.getElementById("combo").innerHTML 				= arrStrip[id]['combo'];
	document.getElementById("cusName").innerHTML 			= arrStrip[id]['cusName'];
	document.getElementById("psd").innerHTML 				= arrStrip[id]['psd'];
	document.getElementById("delDate").innerHTML 			= arrStrip[id]['delDate'];
	document.getElementById("process").innerHTML 			= arrStrip[id]['process'];
	document.getElementById("actQty").innerHTML 			= arrStrip[id]['actQty'];
	document.getElementById("memo").innerHTML 				= arrStrip[id]['memo'];
	//###############################popup value assign - split#############################
	document.getElementById("splOrderNo").innerHTML 		= arrStrip[id]['orderNo'];
	document.getElementById("splSalesOrderNo").innerHTML 	= arrStrip[id]['salesOrderNo'];
	document.getElementById("splQty").innerHTML 			= arrStrip[id]['qty'];
	document.getElementById("splActualQty").innerHTML 		= arrStrip[id]['actQty'];
	document.getElementById("splAvailbleQty").innerHTML 	= (arrStrip[id]['qty'] - arrStrip[id]['actQty']);
	//######################################################################################
	//###############################popup value assign - memo##############################
	document.getElementById("txtMemo").value 				= arrStrip[id]['memo'];
	//######################################################################################
}
function oll_MouseHover(obj)
{
	//alert(obj.id);
}
function oll_MouseUp(obj)
{
	//alert(obj.id + "up");
	//alert(DailyStartDate);
	var DailyStartDate = startDate;
	//findParentNode(obj);
}
function oll_MouseDown(obj)
{
	//alert(obj.id +"down");
}
function oll_MouseLeave(obj)
{
	//alert(obj.id + "leave");
	document.getElementById("orderNo").innerHTML 		= "";
	document.getElementById("salesOrderNo").innerHTML 	= "";
	document.getElementById("qty").innerHTML 			= "";
	document.getElementById("style").innerHTML 			= "";
	document.getElementById("graphic").innerHTML 		= "";
	document.getElementById("combo").innerHTML 			= "";
	document.getElementById("cusName").innerHTML 		= "";
	document.getElementById("psd").innerHTML 			= "";
	document.getElementById("delDate").innerHTML 		= "";
	document.getElementById("process").innerHTML 		= "";
	document.getElementById("actQty").innerHTML 		= "";
	document.getElementById("memo").innerHTML 			= "";
}
function oll_MouseClick(obj)
{
	//alert(obj.id + "leave");
}
function findParentNode(childObj) 
{
	var testObj = childObj.parentNode;
    //alert(testObj.id);
}
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>All Mouse Functions Area>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
function alertx()
{
	//$('#frmPlanningBoard #butSave').validationEngine('hide')	;
}
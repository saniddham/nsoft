<?php
session_start();
ini_set('max_execution_time',600);
$backwardseperator 		= "../../../../";
$projectName 			= substr($_SERVER['PHP_SELF'],0,strpos($_SERVER['PHP_SELF'],'/',1));
$thisFilePath 			= $_SERVER['PHP_SELF'];
$locationId				= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];
$userId 				= $_SESSION['userId'];

include  "{$backwardseperator}dataAccess/permisionCheck.inc";
include_once  "../../../../class/productionAndPlanning/planningBoard/cls_planningboard_get.php";
include_once  "../../../../class/cls_commonFunctions_get.php";

$programCode			= 'P0755';
$obj_planningBoard_get	= new Cls_PlanningBoard_Get($db,$locationId,$companyId,$userId);
$obj_common				= new cls_commonFunctions_get($db);

$fromDate	= date("Y-m-01", strtotime("last month"));
$toDate 	= date('Y-m-t');
?>
<script>
	var arrType	= "";
	var projectName = '<?php echo $projectName; ?>';
</script>
<?php

$type_result 	= $obj_planningBoard_get->getTypeData();
while($rowT = mysqli_fetch_array($type_result))
{
	switch($rowT['QTY_TYPE_ID'])
	{
		case 1:
			$permissionId = '35';
		break;
		case 2:
			$permissionId = '36';
		break;
		case 3:
			$permissionId = '37';
		break;
		case 4:
			$permissionId = '38';
		break;
		case 5:
			$permissionId = '39';
		break;
	}
	$planTypeViewMode = $obj_common->ValidateSpecialPermission($permissionId,$userId,'RunQuery');
	if($planTypeViewMode==1)
	{
	?>
    	<script>
				arrType += "{";
				arrType += '"typeId":"'+ <?php echo $rowT['QTY_TYPE_ID']; ?> +'",' ;
				arrType += '"permissionId":"'+ <?php echo $permissionId; ?> +'",' ;
				arrType += '"typeName":"'+ '<?php echo $rowT['HEADER_TYPE']; ?>' +'",' ;
				arrType += '"type":"'+ '<?php echo $rowT['TYPE']; ?>' +'"' ;
				arrType +=  '},';
		</script>   
	<?php
	}
}

?>
<script>
		arrType 		= arrType.substr(0,arrType.length-1);	
	var arrType			= '['+arrType+']';
	var fromDate 		= '<?php echo $fromDate; ?>';
	var toDate 			= '<?php echo $toDate; ?>';
	var planLocation	= <?php echo $locationId; ?>;
	
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Planning Board</title>

<link href="../../../../images/favicon.ico" rel="shortcut icon"  />
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<script type="application/javascript" src="planningBoard-js.js"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">
<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />

<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script> 
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script> 
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script> 
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script> 
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<style>
table .bordered_notRound{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered_notRound {
	border-left: 1px solid #ccc;
    border-right: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered_notRound tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered_notRound td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered_notRound th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered_notRound th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */         
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered_notRound td:first-child, .bordered_notRound th:first-child {
    border-left: none;
}
.normalfnt2BI {
	font-family: Tahoma;
	font-size: 10px;
	color: #164574;
	margin: 0px;
	font-weight: bold;
	font-style: normal;
}
.normalfnt2 {
	font-family: Tahoma;
	font-size: 10px;
	color: #164574;
	margin: 0px;
	font-style: normal;
}
.normalfntHeader {
	font-family: Tahoma;
	font-size: 14px;
	color: #164574;
	margin: 0px;
	font-weight: bold;
	font-style: normal;
}
</style>
</head>

<body onLoad="searchPopup();">
<form id="frmPlanningBoard" name="frmPlanningBoard" action="planningBoard.php">
<table width="100%" border="0" align="center">
    <tr>
    	<td class="normalfnt2"><div style="float:left;width:50%" id="printTypeName" class="normalfntHeader"></div><div style="float:right;width:50%">
        <table width="100%" border="0">
            <tr class="normalfnt">
            	<td width="12%" height="17">&nbsp;</td>
              <td width="4%"><div style="width:20px;height:15px;background-color:#BFF9C2" class="tableBorder_allRound"></div></td>
                <td width="18%">&nbsp;Week Days</td>
                <td width="4%"><div style="width:20px;height:15px;background-color:#F9FABE" class="tableBorder_allRound"></div></td>
                <td width="18%">&nbsp;Saturdays</td>
                <td width="4%"><div style="width:20px;height:15px;background-color:#FAD9AD" class="tableBorder_allRound"></div></td>
                <td width="18%">&nbsp;Sundays</td>
                <td width="4%"><div style="width:20px;height:15px;background-color:#F08080" class="tableBorder_allRound"></div></td>
                <td width="18%">&nbsp;Company Holidays</td>
            </tr>
        </table>
        </div> </td>
    </tr>
    <tr>
    	<td width="100%">
        <div id="myDiv" style="overflow:scroll; height:540px; width:1345px">
            <table width="1010" border="0" class="Bordered_notRound" id="tblPlanningBoard">
            <thead>
            	<!--<tr class="normalfnt">
                	<td colspan="10" bgcolor="#FFFFFF" style="text-align:left">Avilable Strokes</td>
                    <?php
                        $start 			= strtotime($fromDate);
                        $end 			= strtotime($toDate);
                        $days_between 	= ceil(abs($end - $start) / 86400);
                        $newFrmDate 	= $fromDate;
                        $color			= '';
                        for($t=0;$t<=$days_between;$t++)
                        {
                        ?>
                            <td style="text-align:right" class="normalfnt2BI clsAilableStrocks<?php echo $newFrmDate; ?>">100000</td>
                        <?php
                            $date = date_create($newFrmDate);
                            date_add($date, date_interval_create_from_date_string('1 days'));
                            $nextDt = date_format($date, 'Y-m-d');
                            $newFrmDate = $nextDt;
                        }
                    ?>
                </tr>
                <tr class="normalfnt">
                	<td colspan="10" bgcolor="#FFFFFF" style="text-align:left">Avilable Groups</td>
                    <?php
                        $start 			= strtotime($fromDate);
                        $end 			= strtotime($toDate);
                        $days_between 	= ceil(abs($end - $start) / 86400);
                        $newFrmDate 	= $fromDate;
                        $color			= '';
                        for($t=0;$t<=$days_between;$t++)
                        {
                        ?>
                            <td style="text-align:right" class="normalfnt2BI clsAilableGroups<?php echo $newFrmDate; ?>">15</td>
                        <?php
                            $date = date_create($newFrmDate);
                            date_add($date, date_interval_create_from_date_string('1 days'));
                            $nextDt = date_format($date, 'Y-m-d');
                            $newFrmDate = $nextDt;
                        }
                    ?>
                </tr>
                <tr class="normalfnt">
                	<td colspan="10" bgcolor="#FFFFFF" style="text-align:left">Total No of Strokes - Plan</td>
                    <?php
                        $start 			= strtotime($fromDate);
                        $end 			= strtotime($toDate);
                        $days_between 	= ceil(abs($end - $start) / 86400);
                        $newFrmDate 	= $fromDate;
                        $color			= '';
                        for($t=0;$t<=$days_between;$t++)
                        {
							//$getTotStrocks	= $obj_planningBoard_get->getTotalStrocks($newFrmDate);
                        ?>
                            <td style="text-align:right" class="normalfnt2BI clsTotStocks<?php echo $newFrmDate; ?>">0</td>
                        <?php
                            $date = date_create($newFrmDate);
                            date_add($date, date_interval_create_from_date_string('1 days'));
                            $nextDt = date_format($date, 'Y-m-d');
                            $newFrmDate = $nextDt;
                        }
                    ?>
                </tr>
                 <tr class="normalfnt">
                	<td colspan="10" bgcolor="#FFFFFF" style="text-align:left">Total No of Groups - Plan</td>
                    <?php
                        $start 			= strtotime($fromDate);
                        $end 			= strtotime($toDate);
                        $days_between 	= ceil(abs($end - $start) / 86400);
                        $newFrmDate 	= $fromDate;
                        $color			= '';
                        for($t=0;$t<=$days_between;$t++)
                        {
                        ?>
                            <td style="text-align:right" class="normalfnt2BI clsTotGroups<?php echo $newFrmDate; ?>">0</td>
                        <?php
                            $date = date_create($newFrmDate);
                            date_add($date, date_interval_create_from_date_string('1 days'));
                            $nextDt = date_format($date, 'Y-m-d');
                            $newFrmDate = $nextDt;
                        }
                    ?>
                </tr>
                <tr class="normalfnt">
                	<td colspan="10" bgcolor="#FFFFFF" style="text-align:left">Utilized Strokes % (Plan vs Avilable)</td>
                    <?php
                        $start 			= strtotime($fromDate);
                        $end 			= strtotime($toDate);
                        $days_between 	= ceil(abs($end - $start) / 86400);
                        $newFrmDate 	= $fromDate;
                        $color			= '';
                        for($t=0;$t<=$days_between;$t++)
                        {
                        ?>
                            <td style="text-align:right" class="normalfnt2BI clsUtilizedStrocks<?php echo $newFrmDate; ?>">&nbsp;</td>
                        <?php
                            $date = date_create($newFrmDate);
                            date_add($date, date_interval_create_from_date_string('1 days'));
                            $nextDt = date_format($date, 'Y-m-d');
                            $newFrmDate = $nextDt;
                        }
                    ?>
                </tr>
                <tr class="normalfnt">
                	<td colspan="10" bgcolor="#FFFFFF" style="text-align:left">Utilized Groups % (Plan vs Avilable)</td>
                    <?php
                        $start 			= strtotime($fromDate);
                        $end 			= strtotime($toDate);
                        $days_between 	= ceil(abs($end - $start) / 86400);
                        $newFrmDate 	= $fromDate;
                        $color			= '';
                        for($t=0;$t<=$days_between;$t++)
                        {
                        ?>
                            <td style="text-align:right" class="normalfnt2BI clsUtilizedGroups<?php echo $newFrmDate; ?>">&nbsp;</td>
                        <?php
                            $date = date_create($newFrmDate);
                            date_add($date, date_interval_create_from_date_string('1 days'));
                            $nextDt = date_format($date, 'Y-m-d');
                            $newFrmDate = $nextDt;
                        }
                    ?>
                </tr>
                <tr class="normalfnt">
                	<td colspan="10" bgcolor="#FFFFFF" style="text-align:left">Daily plan cum out put</td>
                    <?php
                        $start 			= strtotime($fromDate);
                        $end 			= strtotime($toDate);
                        $days_between 	= ceil(abs($end - $start) / 86400);
                        $newFrmDate 	= $fromDate;
                        $color			= '';
                        for($t=0;$t<=$days_between;$t++)
                        {
                        ?>
                            <td style="text-align:right" class="normalfnt2BI clsDailyPlan<?php echo $newFrmDate; ?>">&nbsp;</td>
                        <?php
                            $date = date_create($newFrmDate);
                            date_add($date, date_interval_create_from_date_string('1 days'));
                            $nextDt = date_format($date, 'Y-m-d');
                            $newFrmDate = $nextDt;
                        }
                    ?>
                </tr>-->
               </thead>
               <tbody>
                <tr class="normalfnt">
                	<td width="10" bgcolor="#999999" style="text-align:center">Del</td>
                    <td width="14" bgcolor="#999999" style="text-align:center">Edit</td>
                    <td bgcolor="#999999" style="text-align:center">Customer</td>
                    <td nowrap="nowrap" bgcolor="#999999" style="text-align:center">Sales Order</td>
                    <td bgcolor="#999999" style="text-align:center">Style</td>
                    <td bgcolor="#999999" style="text-align:center">Graphic</td>
                    <td nowrap="nowrap" bgcolor="#999999" style="text-align:center">Plate Size</td>
                    <td bgcolor="#999999" style="text-align:center">colors</td>
                    <td bgcolor="#999999" style="text-align:center">Strokes</td>
                    <td nowrap="nowrap" bgcolor="#999999" style="text-align:center">No of Panels</td>
                    <td nowrap="nowrap" bgcolor="#999999" style="text-align:center">PO Qty</td>
                    <td bgcolor="#999999" style="text-align:center">Type</td>
                    
                    <?php
                        $start 			= strtotime($fromDate);
                        $end 			= strtotime($toDate);
                        $days_between 	= ceil(abs($end - $start) / 86400);
                        $newFrmDate 	= $fromDate;
                        $color			= '';
                        for($t=0;$t<=$days_between;$t++)
                        {
							$weekday 		= date('l', strtotime($newFrmDate));
							switch($weekday)
							{
								case 'Saturday':
									$color = '#F9FABE';
								break;
								
								case 'Sunday':
									$color = '#FAD9AD';
								break;
								
								default:
									$color = '#BFF9C2';
								break;
							}
							$holidayArr	= $obj_planningBoard_get->getCompanyHoliday($newFrmDate,'RunQuery');
							if($holidayArr['intDayType']!='' && $holidayArr['intDayType']==0 )
								$color = '#F08080';
                        ?>
                            <td nowrap="nowrap" style="text-align:center" bgcolor="<?php echo $color; ?>" class="normalfnt2BI">&nbsp;<?php echo $newFrmDate; ?>&nbsp;</td>
                        <?php
                            $date = date_create($newFrmDate);
                            date_add($date, date_interval_create_from_date_string('1 days'));
                            $nextDt = date_format($date, 'Y-m-d');
                            $newFrmDate = $nextDt;
                        }
                    ?>
                </tr>
                 </tbody>
                <tfoot>                
               
                </tfoot>
            </table>
        </div>
        </td>
    </tr>
    <tr>
    	<td height="32" >
    		<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td align="center" class="tableBorder_allRound"><a class="button white medium" id="butOrderBook" name="butOrderBook">Order Book</a><a class="button white medium" id="butNew" name="butNew" style="display:none">New</a><a class="button white medium" id="butSave" name="butSave" style="display:none">Save</a><a href="../../../../main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
              </tr>
            </table>
        </td>
    </tr>
</table>
</form>
<div style="position:absolute;width:400px;display:none;" id="manualOrderAllc" >
<form id="frmMOrderAlloPopup" name="frmMOrderAlloPopup">
<table width="400" border="0" align="center" bgcolor="#FFFFFF">
    <tr>
        <td>
        <table width="100%" class="bordered" id="tblMOAMain" >
        	<thead>
            	<tr>
                	
                    <th width="23%">Order Year</th>
                    <th width="28%">Order No</th>
                    <th width="31%">Sales Order</th>
                    <th width="18%" style="text-align:right"><img border="0" src="../../../../images/close_plan.png" width="15" height="15" name="butCloseMOrder" class="mouseover" id="butCloseMOrder"/></th>
                </tr>
            </thead>
            <tbody>

					<tr class="normalfnt" id="">
                        <td class="tdClsOrderYear" style="text-align:center"><select name="cboOrderYear" id="cboOrderYear" style="width:100%" class="clsMOAOrderYear">
                    	</select></td>
                        <td style="text-align:center"><select name="cboOrderNo" id="cboOrderNo" style="width:100%" class="clsMOAOrderNo">
                    	</select></td>
                        <td style="text-align:center"><select name="cboSalesOrder" id="cboSalesOrder" style="width:100%" class="clsMOASalesOrder">
                    	</select></td>
                        <td style="text-align:center"><a class="button green small" id="butMOAllocate" name="butMOAllocate">Allocate</a></td>
					</tr>
            </tbody>
        </table>
        </td>
    </tr>
</table>
</form>
</div>
<div style="width:100%; position: absolute;display:none;z-index:100" id="popupContact1"></div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</body>
</html>

<?php
session_start();
$backwardseperator 		= "../../../../";
$userId 				= $_SESSION['userId'];
$locationId	  			= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];

include $backwardseperator."dataAccess/Connector.php";
//include_once  "../../../../class/finance/cls_common_get.php";
//include_once  "../../../../class/productionAndPlanning/cls_plan_common_get.php";
include_once  "../../../../class/productionAndPlanning/planningBoard/cls_planningboard_get.php";

//$obj_common_get			= new Cls_Common_Get($db);
//$obj_plan_common_get 	= new Cls_Plan_Common_Get($db);
$obj_planningBoard_get	= new Cls_PlanningBoard_Get($db,$locationId,$companyId,$userId);

$result_details			= $obj_planningBoard_get->getManualOrderDetails();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Manual Order Allocation</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />

</head>
<form id="frmMOrderAlloPopup" name="frmMOrderAlloPopup">
<div align="center">
<div class="trans_layoutL" style="width:1200px">
<div class="trans_text">Manual Order Allocation</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <tr>
        <td><div style="overflow:scroll;overflow-x:hidden;width:1200px;height:400px;" id="divGrid">
        <table width="100%" class="bordered" id="tblMOAMain" >
        	<thead>
            	<tr>
                	<th width="8%">Marketer</th>
                    <th width="16%">Customer</th>
                    <th width="12%">PO No</th>
                    <th width="11%">Sales Order No</th>
                    <th width="12%">Style</th>
                    <th width="13%">Graphic</th>
                    <th width="7%">Order Year</th>
                    <th width="10%">Order No</th>
                    <th width="11%">Sales Order</th>
                </tr>
            </thead>
            <tbody>
            <?php
				while($row=mysqli_fetch_array($result_details))
				{
			?>
					<tr class="normalfnt" id="<?php echo $row['PLAN_ID']; ?>">
                    	<td style="text-align:left"><?php echo $row['marketer']; ?></td>
                        <td style="text-align:left"><?php echo $row['customer']; ?></td>
                        <td style="text-align:left"><?php echo $row['PO_NO']; ?></td>
                        <td style="text-align:left"><?php echo $row['SALES_ORDER_NO']; ?></td>
                        <td style="text-align:left"><?php echo $row['STYLE_NO']; ?></td>
                        <td style="text-align:left"><?php echo $row['GRAPHIC_NO']; ?></td>
                        <td style="text-align:center"><select name="cboOrderYear" id="cboOrderYear" style="width:100%" class="clsMOAOrderYear">
                    <?php
						echo $obj_planningBoard_get->getMOAOrderYear();
					?>
                    	</select></td>
                        <td style="text-align:center"><select name="cboOrderNo" id="cboOrderNo" style="width:100%" class="clsMOAOrderNo">
                    	</select></td>
                        <td style="text-align:center"><select name="cboSalesOrder" id="cboSalesOrder" style="width:100%" class="clsMOASalesOrder">
                    	</select></td>
					</tr>
            <?php
				}
			?>
            </tbody>
        </table>
        </div></td>
    </tr>	
    <tr>
      <td height="32" >
        <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
          <tr>
            <td align="center" class="tableBorder_allRound"><a class="button white medium" id="butMOASave" name="butMOASave">Save</a><a class="button white medium" id="butMOAClose" name="butMOAClose">Close</a></td>
            </tr>
          </table>
        </td>
    </tr>
</table>
</div>
</div>
</form>
</body>
</html>
<script>
	$('#txtSrhFromDate').val(fromDate);
	$('#txtSrhToDate').val(toDate);
</script>
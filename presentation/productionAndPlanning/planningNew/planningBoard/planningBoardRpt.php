<?php
session_start();
ini_set('max_execution_time',600);
$backwardseperator 		= "../../../../";
$thisFilePath 			= $_SERVER['PHP_SELF'];
$locationId				= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];
$userId 				= $_SESSION['userId'];

include 	  "../../../../dataAccess/Connector.php";
include_once  "../../../../class/productionAndPlanning/planningBoard/cls_planningboard_get.php";
include_once  "../../../../class/cls_commonFunctions_get.php";

$programCode			= 'P0755';
$obj_planningBoard_get	= new Cls_PlanningBoard_Get($db,$locationId,$companyId,$userId);
$obj_common				= new cls_commonFunctions_get($db);

$planId 				= $_REQUEST['planId'];

$getDateRange			= $obj_planningBoard_get->getRptDateRange($planId);
$rptHeaderArr			= $obj_planningBoard_get->getRptHeaderData($planId);

$fromDate				= $getDateRange['startDate'];
$toDate					= $getDateRange['endDate'];	

$sampleImage			= "<img id=\"saveimg\" style=\"width:264px;height:148px;\"
 							src=\"../../../../documents/sampleinfo/samplePictures/".$rptHeaderArr['intSampleNo']."-".$rptHeaderArr['intSampleYear']."-".$rptHeaderArr['intRevisionNo']."-1.png\" />";


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Planning Board Report</title>

<link href="../../../../images/favicon.ico" rel="shortcut icon"  />
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />

<style>
table .bordered_notRound{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered_notRound {
	border-left: 1px solid #ccc;
    border-right: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered_notRound tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered_notRound td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered_notRound th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered_notRound th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */         
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered_notRound td:first-child, .bordered_notRound th:first-child {
    border-left: none;
}
.normalfnt2BI {
	font-family: Tahoma;
	font-size: 10px;
	color: #164574;
	margin: 0px;
	font-weight: bold;
	font-style: normal;
}
.normalfnt2 {
	font-family: Tahoma;
	font-size: 10px;
	color: #164574;
	margin: 0px;
	font-style: normal;
}
</style>
</head>

<body>
<form id="frmPlanningBoardReport" name="frmPlanningBoardReport" action="planningBoard.php">
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
    	<td width="100%">
        <div style="overflow:scroll;width:1345px;overflow-y: hidden;">
            <table width="1010" border="0" class="bordered_notRound" id="tblPlanningBoard">
               <thead>
                <tr class="normalfnt">
                    <td bgcolor="#999999" style="text-align:center">Customer</td>
                    <td nowrap="nowrap" bgcolor="#999999" style="text-align:center">Sales Order</td>
                    <td bgcolor="#999999" style="text-align:center">Style</td>
                    <td bgcolor="#999999" style="text-align:center">Graphic</td>
                    <td nowrap="nowrap" bgcolor="#999999" style="text-align:center">Plate Size</td>
                    <td bgcolor="#999999" style="text-align:center">colors</td>
                    <td bgcolor="#999999" style="text-align:center">Strokes</td>
                    <td nowrap="nowrap" bgcolor="#999999" style="text-align:center">No of Panels</td>
                    <td nowrap="nowrap" bgcolor="#999999" style="text-align:center">PSD</td>
                    <td nowrap="nowrap" bgcolor="#999999" style="text-align:center">PO Qty</td>
                    <td bgcolor="#999999" style="text-align:center">Type</td>
                    
                    <?php
                        $start 			= strtotime($fromDate);
                        $end 			= strtotime($toDate);
                        $days_between 	= ceil(abs($end - $start) / 86400);
                        $newFrmDate 	= $fromDate;
                        $color			= '';
                        for($t=0;$t<=$days_between;$t++)
                        {
							$weekday = date('l', strtotime($newFrmDate));
							switch($weekday)
							{
								case 'Saturday':
									$color = '#F9FABE';
								break;
								
								case 'Sunday':
									$color = '#FAD9AD';
								break;
								
								default:
									$color = '#FFFFFF';
								break;
							}
							$holidayArr	= $obj_planningBoard_get->getCompanyHoliday($newFrmDate,'RunQuery');
							if($holidayArr['intDayType']!='' && $holidayArr['intDayType']==0 )
								$color = '#F08080';
                        ?>
                            <td nowrap="nowrap" style="text-align:center" bgcolor="<?php echo $color; ?>" class="normalfnt2BI">&nbsp;<?php echo $newFrmDate; ?>&nbsp;</td>
                        <?php
                            $date = date_create($newFrmDate);
                            date_add($date, date_interval_create_from_date_string('1 days'));
                            $nextDt = date_format($date, 'Y-m-d');
                            $newFrmDate = $nextDt;
                        }
                    ?>
                </tr>
                <tr class="normalfnt">
                    <td nowrap="nowrap" bgcolor="#FFFFFF" style="text-align:left;height:25px"><?php echo ($rptHeaderArr['customer']==''?'&nbsp;':$rptHeaderArr['customer']); ?></td>
                    <td nowrap="nowrap" bgcolor="#FFFFFF" style="text-align:left"><?php echo ($rptHeaderArr['salesOrderNo']==''?'&nbsp;':$rptHeaderArr['salesOrderNo']); ?></td>
                    <td nowrap="nowrap" bgcolor="#FFFFFF" style="text-align:left"><?php echo ($rptHeaderArr['styleNo']==''?'&nbsp;':$rptHeaderArr['styleNo']); ?></td>
                    <td nowrap="nowrap" bgcolor="#FFFFFF" style="text-align:left"><?php echo ($rptHeaderArr['graphicNo']==''?'&nbsp;':$rptHeaderArr['graphicNo']); ?></td>
                    <td nowrap="nowrap" bgcolor="#FFFFFF" style="text-align:left"><?php echo ($rptHeaderArr['plateSize']==''?'&nbsp;':$rptHeaderArr['plateSize']); ?></td>
                    <td nowrap="nowrap" bgcolor="#FFFFFF" style="text-align:center"><?php echo ($rptHeaderArr['noOfColors']==''?'&nbsp;':$rptHeaderArr['noOfColors']); ?></td>
                    <td nowrap="nowrap" bgcolor="#FFFFFF" style="text-align:center"><?php echo ($rptHeaderArr['noOfStrocks']==''?'&nbsp;':$rptHeaderArr['noOfStrocks']); ?></td>
                    <td nowrap="nowrap" bgcolor="#FFFFFF" style="text-align:center"><?php echo ($rptHeaderArr['noOfPanels']==''?'&nbsp;':$rptHeaderArr['noOfPanels']); ?></td>
                    <td nowrap="nowrap" bgcolor="#FFFFFF" style="text-align:center"><?php echo ($rptHeaderArr['dtPSD']==''?'&nbsp;':$rptHeaderArr['dtPSD']); ?></td>
                    <td nowrap="nowrap" bgcolor="#FFFFFF" style="text-align:right"><?php echo ($rptHeaderArr['POQty']==''?'&nbsp;':$rptHeaderArr['POQty']); ?></td>
                    <td nowrap="nowrap" bgcolor="#FFFFFF" style="text-align:left" class="normalfnt2BI">Loading Plan</td>
                    
                    <?php
                        $start 			= strtotime($fromDate);
                        $end 			= strtotime($toDate);
                        $days_between 	= ceil(abs($end - $start) / 86400);
                        $newFrmDate 	= $fromDate;
                        $color			= '';
                        for($t=0;$t<=$days_between;$t++)
                        {
							$weekday = date('l', strtotime($newFrmDate));
							switch($weekday)
							{
								case 'Saturday':
									$color = '#F9FABE';
								break;
								
								case 'Sunday':
									$color = '#FAD9AD';
								break;
								
								default:
									$color = '#FFFFFF';
								break;
							}
							$lodingPlanQty	= $obj_planningBoard_get->getRptPlanQty($newFrmDate,$planId,1);
							$holidayArr		= $obj_planningBoard_get->getCompanyHoliday($newFrmDate,'RunQuery');
							if($holidayArr['intDayType']!='' && $holidayArr['intDayType']==0 )
								$color = '#F08080';
                        ?>
                            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $color; ?>" class="normalfnt"><?php echo ($lodingPlanQty==''?'&nbsp;':$lodingPlanQty); ?></td>
                        <?php
                            $date = date_create($newFrmDate);
                            date_add($date, date_interval_create_from_date_string('1 days'));
                            $nextDt = date_format($date, 'Y-m-d');
                            $newFrmDate = $nextDt;
                        }
                    ?>
                </tr>
                <tr class="normalfnt">
                    <td colspan="10" rowspan="6" bgcolor="#FFFFFF" align="center">
                    <div contenteditable="true" style="width:264px;height:148px;overflow:hidden" class="tableBorder_allRound divPicture" id="divPicture"><?php echo $sampleImage; ?></div>
                    </td>
                    <td nowrap="nowrap" bgcolor="#FFFFFF" style="text-align:left" class="normalfnt2BI">Production Plan</td>
                    
                    <?php
                        $start 			= strtotime($fromDate);
                        $end 			= strtotime($toDate);
                        $days_between 	= ceil(abs($end - $start) / 86400);
                        $newFrmDate 	= $fromDate;
                        $color			= '';
                        for($t=0;$t<=$days_between;$t++)
                        {
							$weekday = date('l', strtotime($newFrmDate));
							switch($weekday)
							{
								case 'Saturday':
									$color = '#F9FABE';
								break;
								
								case 'Sunday':
									$color = '#FAD9AD';
								break;
								
								default:
									$color = '#FFFFFF';
								break;
							}
							$productionPlanQty	= $obj_planningBoard_get->getRptPlanQty($newFrmDate,$planId,3);
							$holidayArr			= $obj_planningBoard_get->getCompanyHoliday($newFrmDate,'RunQuery');
							if($holidayArr['intDayType']!='' && $holidayArr['intDayType']==0 )
								$color = '#F08080';
                        ?>
                            <td nowrap="nowrap" style="text-align:right" bgcolor="<?php echo $color; ?>" class="normalfnt"><?php echo ($productionPlanQty==''?'&nbsp;':$productionPlanQty); ?></td>
                        <?php
                            $date = date_create($newFrmDate);
                            date_add($date, date_interval_create_from_date_string('1 days'));
                            $nextDt = date_format($date, 'Y-m-d');
                            $newFrmDate = $nextDt;
                        }
                    ?>
                </tr>
                <tr class="normalfnt">
                    <td nowrap="nowrap" bgcolor="#CAE1FF" style="text-align:left" class="normalfnt2BI">Delivery Plan</td>
                    
                    <?php
                        $start 			= strtotime($fromDate);
                        $end 			= strtotime($toDate);
                        $days_between 	= ceil(abs($end - $start) / 86400);
                        $newFrmDate 	= $fromDate;
                        $color			= '';
                        for($t=0;$t<=$days_between;$t++)
                        {
							$deliveryPlanQty	= $obj_planningBoard_get->getRptPlanQty($newFrmDate,$planId,2);
                        ?>
                            <td nowrap="nowrap" style="text-align:right" bgcolor="#CAE1FF" class="normalfnt"><?php echo ($deliveryPlanQty==''?'&nbsp;':$deliveryPlanQty); ?></td>
                        <?php
                            $date = date_create($newFrmDate);
                            date_add($date, date_interval_create_from_date_string('1 days'));
                            $nextDt = date_format($date, 'Y-m-d');
                            $newFrmDate = $nextDt;
                        }
                    ?>
                </tr>
                <tr class="normalfnt">
                    <td nowrap="nowrap" bgcolor="#FFB6C1" style="text-align:left" class="normalfnt2BI">Actual Receiving-before counting</td>
                    
                    <?php
                        $start 			= strtotime($fromDate);
                        $end 			= strtotime($toDate);
                        $days_between 	= ceil(abs($end - $start) / 86400);
                        $newFrmDate 	= $fromDate;
                        $color			= '';
                        for($t=0;$t<=$days_between;$t++)
                        {
							$color = '#EEA2AD';
							$actualReceivingBC	= $obj_planningBoard_get->getActualReceivingBC($newFrmDate,$planId);
                        ?>
                            <td nowrap="nowrap" style="text-align:right" bgcolor="#FFB6C1" class="normalfnt"><?php echo ($actualReceivingBC==''?'&nbsp;': $actualReceivingBC);?></td>
                        <?php
                            $date = date_create($newFrmDate);
                            date_add($date, date_interval_create_from_date_string('1 days'));
                            $nextDt = date_format($date, 'Y-m-d');
                            $newFrmDate = $nextDt;
                        }
                    ?>
                </tr>
                <tr class="normalfnt">
                    <td nowrap="nowrap" bgcolor="#FFFFFF" style="text-align:left" class="normalfnt2BI">Actual receiving</td>
                    
                    <?php
                        $start 			= strtotime($fromDate);
                        $end 			= strtotime($toDate);
                        $days_between 	= ceil(abs($end - $start) / 86400);
                        $newFrmDate 	= $fromDate;
                        $color			= '';
                        for($t=0;$t<=$days_between;$t++)
                        {
							$weekday = date('l', strtotime($newFrmDate));
							switch($weekday)
							{
								case 'Saturday':
									$color = '#F9FABE';
								break;
								
								case 'Sunday':
									$color = '#FAD9AD';
								break;
								
								default:
									$color = '#FFFFFF';
								break;
							}
							$holidayArr	= $obj_planningBoard_get->getCompanyHoliday($newFrmDate,'RunQuery');
							if($holidayArr['intDayType']!='' && $holidayArr['intDayType']==0 )
								$color = '#F08080';
                        ?>
                            <td nowrap="nowrap" style="text-align:center" bgcolor="<?php echo $color; ?>" class="normalfnt2BI">&nbsp;</td>
                        <?php
                            $date = date_create($newFrmDate);
                            date_add($date, date_interval_create_from_date_string('1 days'));
                            $nextDt = date_format($date, 'Y-m-d');
                            $newFrmDate = $nextDt;
                        }
                    ?>
                </tr>
                <tr class="normalfnt">
                    <td nowrap="nowrap" bgcolor="#FFFFFF" style="text-align:left" class="normalfnt2BI">issue to production</td>
                    
                    <?php
                        $start 			= strtotime($fromDate);
                        $end 			= strtotime($toDate);
                        $days_between 	= ceil(abs($end - $start) / 86400);
                        $newFrmDate 	= $fromDate;
                        $color			= '';
                        for($t=0;$t<=$days_between;$t++)
                        {
							$weekday = date('l', strtotime($newFrmDate));
							switch($weekday)
							{
								case 'Saturday':
									$color = '#F9FABE';
								break;
								
								case 'Sunday':
									$color = '#FAD9AD';
								break;
								
								default:
									$color = '#FFFFFF';
								break;
							}
							$holidayArr	= $obj_planningBoard_get->getCompanyHoliday($newFrmDate,'RunQuery');
							if($holidayArr['intDayType']!='' && $holidayArr['intDayType']==0 )
								$color = '#F08080';
                        ?>
                            <td nowrap="nowrap" style="text-align:center" bgcolor="<?php echo $color; ?>" class="normalfnt2BI">&nbsp;</td>
                        <?php
                            $date = date_create($newFrmDate);
                            date_add($date, date_interval_create_from_date_string('1 days'));
                            $nextDt = date_format($date, 'Y-m-d');
                            $newFrmDate = $nextDt;
                        }
                    ?>
                </tr>
                <tr class="normalfnt">
                    <td bgcolor="#FFFFFF" style="text-align:left" class="normalfnt2BI">Print Completed</td>
                    
                    <?php
                        $start 			= strtotime($fromDate);
                        $end 			= strtotime($toDate);
                        $days_between 	= ceil(abs($end - $start) / 86400);
                        $newFrmDate 	= $fromDate;
                        $color			= '';
                        for($t=0;$t<=$days_between;$t++)
                        {
							$weekday = date('l', strtotime($newFrmDate));
							switch($weekday)
							{
								case 'Saturday':
									$color = '#F9FABE';
								break;
								
								case 'Sunday':
									$color = '#FAD9AD';
								break;
								
								default:
									$color = '#FFFFFF';
								break;
							}
							$holidayArr	= $obj_planningBoard_get->getCompanyHoliday($newFrmDate,'RunQuery');
							if($holidayArr['intDayType']!='' && $holidayArr['intDayType']==0 )
								$color = '#F08080';
                        ?>
                            <td nowrap="nowrap" style="text-align:center" bgcolor="<?php echo $color; ?>" class="normalfnt2BI">&nbsp;</td>
                        <?php
                            $date = date_create($newFrmDate);
                            date_add($date, date_interval_create_from_date_string('1 days'));
                            $nextDt = date_format($date, 'Y-m-d');
                            $newFrmDate = $nextDt;
                        }
                    ?>
                </tr>
                 </thead>
                <tbody>                
                
              </tbody>
            </table>
      	</div>
        </td>
    </tr>
    <tr>
    	<td height="30" >
    		<table style="width:1345px;" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td align="center"><a class="button white medium" id="butRptClose" name="butRptClose">Close</a></td>
              </tr>
            </table>
        </td>
    </tr>
</table>
</form>
</body>
</html>
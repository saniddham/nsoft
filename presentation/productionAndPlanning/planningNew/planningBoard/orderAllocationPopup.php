<?php
session_start();
ini_set('max_execution_time',600);
$backwardseperator 		= "../../../../";
$userId 				= $_SESSION['userId'];
$projectName 			= substr($_SERVER['PHP_SELF'],0,strpos($_SERVER['PHP_SELF'],'/',1));

include $backwardseperator."dataAccess/Connector.php";
include_once  "../../../../class/finance/cls_common_get.php";
include_once  "../../../../class/productionAndPlanning/cls_plan_common_get.php";
include_once  "../../../../class/cls_commonFunctions_get.php";

$obj_common_get			= new Cls_Common_Get($db);
$obj_plan_common_get 	= new Cls_Plan_Common_Get($db);
$obj_common				= new cls_commonFunctions_get($db);

$MOSaveMode				= $obj_common->ValidateSpecialPermission('29',$userId,'RunQuery');
$panelAddMode			= $obj_common->ValidateSpecialPermission('30',$userId,'RunQuery');

?>
<script>
	var projectName = '<?php echo $projectName; ?>';
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Order Allocation</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />

</head>
<form id="frmOrderAllocation" name="frmOrderAllocation" autocomplete="off" action="orderAllocation.php" method="post">
<div align="center">
<div class="trans_layoutL" style="width:1230px">
<div class="trans_text">Order Allocation</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <tr>
        <td>
            <table width="100%" border="0">
                <tr class="normalfnt">
                    <td width="11%">Customer</td>
                    <td width="23%"><select name="cboOACustomer" id="cboOACustomer" style="width:230px">
                    <?php
						echo $obj_common_get->getCustomerCombo('');
					?>
                    </select></td>
                    <td width="11%">Sales Order No</td>
                    <td width="22%"><input type="text" name="txtOASalesOrderNo" id="txtOASalesOrderNo" style="width:230px" /></td>
                    <td width="11%">Brand</td>
                    <td width="22%"><input type="text" name="txtOABrand" id="txtOABrand" style="width:230px" /></td>
                </tr>
                <tr class="normalfnt">
                    <td>Style</td>
                    <td><input type="text" name="txtOAStyle" id="txtOAStyle" style="width:230px" /></td>
                    <td>Graphic</td>
                    <td><input type="text" name="txtOAGraphic" id="txtOAGraphic" style="width:230px" /></td>
                    <td>&nbsp;</td>
                    <td><a class="button green small" id="butOASearch" name="butOASearch">Search</a></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="bordered">
   		<th colspan="14" >Order Allocation<div style="float:right"><a class="button white small" id="butInsertRow">Add New Order</a></div></th>
    </tr>
	<tr class="normalfnt">
    	<td><div style="overflow:scroll;width:1230px;height:400px;" id="divGrid">
         	<table style="width:1700px;" class="bordered" id="tblOrderPopup" >
            <thead>
                <tr>
                	<th style="width:25px">Del</th>
                    <th style="width:25px"><input class="clsChkAll" type="checkbox" id="chkAll"/></th>
                     <th style="width:120px">Marketer</th>
                    <th style="width:200px">Customer</th>
                    <th style="width:150px" >Order No</th>
                    <th style="width:120px" >PO No</th>
                    <th style="width:120px" >Sales Order No</th>
                    <th style="width:120px">Brand</th>
                    <th style="width:120px" >Style</th>
                    <th style="width:120px" >Graphic</th>
                    <th style="width:120px" >Combo</th>
                    <th style="width:120px" >Print Method</th>
                    <th style="width:125px" >Plate Size</th>
                    <th style="width:130px" >PSD</th>
                    <th style="width:100px" >Qty</th>
                    <th style="width:60px" >Colors</th>
                    <th style="width:60px" >Stocks</th>
                    <th style="width:60px" >Panels</th>
                </tr>
            </thead>
            <tbody id="tblOrderPopupTbody">
            	<tr id="M" class="cls_first_row">
                	<td style="text-align:center" ><img border="0" src="../../../../images/del.png" alt="Save" name="butDel" class="mouseover clsDel" id="butDel" tabindex="24"/></td>
                    <td style="text-align:center"><input class="clsChkOrder" type="checkbox" id="chkOrder" <?php echo($MOSaveMode!=1?'disabled="disabled"':''); ?>/></td>
                    <td class="clsOAMarketer" id="null"><select name="cboManuMarketer" id="cboManuMarketer" class="clsMarketer" style="width:100%">
                    <?php
						echo $obj_common_get->getMarketerCombo('');
					?>
                    </select></td>
                    <td class="clsOACustomer" id="null"><select name="cboManuCustomer" id="cboManuCustomer" class="clsCustomer" style="width:100%">
                    <?php
						echo $obj_common_get->getCustomerCombo('');
					?>
                    </select></td>
                    <td class="clsOAOrderNo">&nbsp;</td>
                    <td><input name="txtManuPONo" id="txtManuPONo" type="text" style="width:100%" class="clsPONo" /></td>
                    <td class="clsOASalesOrderNo" id="null"><input name="txtManuSalesOrderNo" id="txtManuSalesOrderNo" type="text" style="width:100%" class="clsSalesOrderNo"  /></td>
                    <td><input name="txtManuBrand" id="txtManuBrand" type="text" style="width:100%" class="clsBrand" /></td>
                    <td><input name="txtManuStyle" id="txtManuStyle" type="text" style="width:100%" class="clsStyle"/></td>
                    <td><input name="txtManuGraphic" id="txtManuGraphic" type="text" style="width:100%" class="clsGraphic"  /></td>
                    <td><input name="txtManuCombo" id="txtManuCombo" type="text" style="width:100%" class="clsCombo"  /></td>
                    <td class="clsOAPrintMethod" id="null"><select name="cboManuPrintMethod" id="cboManuPrintMethod" style="width:100%" class="clsPrintMethod">
                    <?php
						echo $obj_plan_common_get->getPrinterMethodCombo('orderBook');
					?>
                    </select></td>
                    <td class="clsOAPlateSize" id="null"><select name="cboPlateSize" id="cboPlateSize" style="width:100%" class="clsPlateSize" disabled="disabled">
                    <?php
						echo $obj_plan_common_get->getPlateSizeCombo();
					?>
                    </select></td>
                    <td class="clsOAPSD"><input name="txtPSDDate" type="text" value="<?php echo date('Y-m-d'); ?>" class="clsPSDDate" id="txtPSDDate~1" style="width:100px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                    <td class="clsOAQty"><input name="txtOAQty" id="txtOAQty" type="text" style="width:100%;text-align:right" class="clsQty" /></td>
                    <td class="clsOAColors"><input name="txtManuColors" id="txtManuColors" type="text" style="width:100%;text-align:right" class="clsColor" /></td>
                    <td class="clsOAStocks"><input name="txtManuStrocks" id="txtManuStrocks" type="text" style="width:100%;text-align:right" class="clsStrock" /></td>
                    <td><input name="txtManuPanel" id="txtManuPanel" type="text" style="width:100%;text-align:right" class="clsPanel" <?php echo($panelAddMode!=1?'disabled="disabled"':'') ?> /></td>
                </tr>
            </tbody>
            </table>
        </div>
        </td>	  
    </tr>	
    <tr>
    	<td height="32" >
    		<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td align="center" class="tableBorder_allRound"><a class="button white medium" id="butOrderAdd" name="butOrderAdd">Add</a><a class="button white medium" id="butOrderClose" name="butOrderClose">Close</a></td>
              </tr>
            </table>
        </td>
    </tr>
</table>
</div>
</div>
</form>
</body>
</html>
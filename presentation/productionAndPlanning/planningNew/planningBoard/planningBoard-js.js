// JavaScript Document
var customer		= '';
var salesOrder		= '';
var style			= '';
var graphic			= '';
var printType		= '';
var printTypeName	= '';
var MOrderObj		= '';

$(document).ready(function(){

	$("#frmPlanningBoard").validationEngine();
	
	/*if(intAddx)
	{
		$('#frmPlanningBoard #butNew').show();
		$('#frmPlanningBoard #butSave').show();
	}
	//permision for edit 
	if(intEditx)
	{
		$('#frmPlanningBoard #butSave').show();
	}*/
	
	//permision for delete
	if(intDeletex)
	{
		$('#frmPlanningBoard #butDelete').show();
	}
	
	//permision for view
	if(intViewx)
	{
		$('#frmPlanningBoard #cboSearch').removeAttr('disabled');
	}
	
	$('#butOrderBook').live('click',orderBookPopup);
	$('#butOASearch').live('click',loadOrderData);
	$('.clsDel').live('click',deleteRow);
	$('#frmOrderAllocation #butInsertRow').live('click',addNewRow);
	$('#chkAll').live('click',chkAll);
	$('#butOrderAdd').live('click',addOrders);
	$('.clsChkOrder').live('click',setPanelClass);
	$('.clsCustomer').live('change',setCustomerId);
	$('.clsMarketer').live('change',setMarketerId);
	$('.clsPrintMethod').live('change',setPrintMethodId);
	$('.clsPlateSize').live('change',setPlateSizeId);
	$('.clsDelPlan').live('click',deletePlan);
	//$('#butSave').live('click',saveData);
	$('.clsPlanQty').live('blur',setTotals);
	$('#butSearch').live('click',searchData);
	//$('#butMOA').live('click',manualOrderAllocation)
	$('#butMOAllocate').live('click',manualOrderAllocationSave)
	$('.clsGraphis').live('click',planningRptPopup)
	$('.clsEditPlan').live('click',function(e){loadManualOrderForm(e,this)});
	$('#butCloseMOrder').live('click',hideMOrderForm);
	
	$('body').live('keyup',function(e) {
          if(e.keyCode == 113) {
            searchPopup();
          }
    });
	$('body').live('keyup',function(e) {
          if(e.keyCode == 36) {
            document.location.href = '../../../../main.php';
          }
    });
	
	$('#frmPlanningBoard #tblPlanningBoard input').live('keyup',function(e){
			if($(this).val()=="" || isNaN($(this).val())){
				$(this).val(0);
				$(this).select();
			}
	});
	
	$('.clsPlanQty').live('keydown',(function(e){
    
		var x = $(this).closest('td').index();
		var y = parseFloat($(this).closest('tr').index())+8;
		
		switch(e.keyCode)
		{
			case 37:
				x--;
			break;
			
			case 38:
				y--;
				if($('#tblPlanningBoard tr').eq(y).find('td').eq(x).children().attr('disabled'))
					y--;	
			break;
			
			case 39:
				x++;
			break;
			
			case 40:
				y++;
				if($('#tblPlanningBoard tr').eq(y).find('td').eq(x).children().attr('disabled'))
					y++;		
			break; 	 
		}
		$('#tblPlanningBoard tr').eq(y).find('td').eq(x).children().focus();
	}));
	
	$(".clsMOAOrderYear").live('change',function(){
		
		$(this).parent().parent().find('.clsMOAOrderNo').addClass('validate[required]');
		$(this).parent().parent().find('.clsMOASalesOrder').addClass('validate[required]');
		
		if($(this).val()=='')
		{
			$(this).parent().parent().find('.clsMOAOrderNo').removeClass('validate[required]');
			$(this).parent().parent().find('.clsMOASalesOrder').removeClass('validate[required]');
		}
		loadComboes('orderYear',1,1,this);
	});
	$(".clsMOAOrderNo").live('change',function(){
		loadComboes('orderNo',0,1,this);
	});
	
});
function orderBookPopup()
{
	$('#popupContact1').html('');
	popupWindow3('1');
	$('#popupContact1').load('orderAllocationPopup.php',function(){

	//$('#butInsertRow').live('click',addNewRow);

			/*$('#butAdd1').click(function(){
				
				var value="[ ";
				$('.clsCapacity').each(function(){
				
					var sizeId   = $(this).parent().parent().find(".clsSize").attr("id");
					var capacity = $(this).val();
					value +='{"sizeId":"'+sizeId+'","capacity":"'+capacity+'"},' ;
							
				});
				
				value = value.substr(0,value.length-1);
				value += " ]";
				
				var url = "teams-db-set.php?requestType=addCapacity";
				$.ajax({
						url:url,
						async:false,
						dataType:'json',
						type:'post',
						data:'&teamId='+teamId+'&sizeDetails='+value,
					success:function(json){
						if(json.type=='pass')
						{
							disablePopup();
						}
					},
					error:function(){
						
						$('#frmTeamSizePopup #butAdd1').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertx()",3000);						
					}
				});
				
			});*/
			setPrintMethodId();
			$('#butOrderClose').click(disablePopup);
	});
}
function searchPopup()
{
	$('#popupContact1').html('');
	popupWindow3('1');
	$('#popupContact1').load('searchPopup.php',function(){
		$('#butSrhClose').click(disablePopup);
	});
}
/*function manualOrderAllocation()
{
	$('#popupContact1').html('');
	popupWindow3('1');
	$('#popupContact1').load('manualOrderAllocationPopup.php',function(){
		$('#butMOAClose').click(disablePopup);
	});
}*/
function planningRptPopup()
{
	$('#popupContact1').html('');
	var planId	= $(this).parent().attr('id');
	popupWindow3('1');
	$('#popupContact1').load('planningBoardRpt.php?planId='+planId,function(){
		$('#butRptClose').click(disablePopup);
	});
}
function loadOrderData()
{
	var lastRowHtml		= $('#frmOrderAllocation #tblOrderPopup .cls_first_row').html();
	
	$("#tblOrderPopup tr:gt(0)").remove();
	var customer		= $('#cboOACustomer').val();
	var salesOrderNo	= $('#txtOASalesOrderNo').val();
	var brand			= $('#txtOABrand').val();
	var style			= $('#txtOAStyle').val();
	var graphic			= $('#txtOAGraphic').val();
	
	var url 	= "planningBoard-db.php";
	var data	= "requestType=loadOrderAllocationData";
	data 	   += "&customer="+customer;
	data 	   += "&salesOrderNo="+salesOrderNo;
	data 	   += "&brand="+brand;
	data 	   += "&style="+style;
	data 	   += "&graphic="+graphic;
	data 	   += "&planLocation="+planLocation;
	
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				
				if(json.arrOrderData!=null)
				{
					var lengthDetail   = json.arrOrderData.length;
					var arrOrderData  = json.arrOrderData;
					
					for(var i=0;i<lengthDetail;i++)
					{
						var orderNo				= arrOrderData[i]['orderNo'];
						var orderYear			= arrOrderData[i]['orderYear'];	
						var customerId			= arrOrderData[i]['customerId'];
						var customerName		= arrOrderData[i]['customerName'];
						var PONo				= arrOrderData[i]['PONo'];
						var salesOrderId		= arrOrderData[i]['salesOrderId'];
						var salesOrderNo		= arrOrderData[i]['salesOrderNo'];
						var style				= arrOrderData[i]['style'];
						var graphic				= arrOrderData[i]['graphic'];
						var combo				= arrOrderData[i]['combo'];
						var brand				= arrOrderData[i]['brand'];
						var colors				= arrOrderData[i]['colors'];
						var strocks				= arrOrderData[i]['strocks'];
						var qty					= arrOrderData[i]['qty'];
						var marketerId			= arrOrderData[i]['marketerId'];
						var marketerName		= arrOrderData[i]['marketerName'];
						var PSD					= arrOrderData[i]['PSD'];
						var printMethodHTML		= arrOrderData[i]['printMethodHTML'];
						var plateSizeHTML		= arrOrderData[i]['plateSizeHTML'];
						var panelAddMode		= arrOrderData[i]['panelAddMode'];
						
						createGrid(orderNo,orderYear,customerId,customerName,PONo,salesOrderId,salesOrderNo,style,graphic,combo,brand,colors,strocks,qty,marketerId,marketerName,PSD,printMethodHTML,plateSizeHTML,panelAddMode);
					}
				}
				$('#frmOrderAllocation #tblOrderPopup tbody tr:last').after("<tr class='cls_first_row' id='M'>"+lastRowHtml+"</tr>");
			}
	});	
}
function createGrid(orderNo,orderYear,customerId,customerName,PONo,salesOrderId,salesOrderNo,style,graphic,combo,brand,colors,strocks,qty,marketerId,marketerName,PSD,printMethodHTML,plateSizeHTML,panelAddMode)
{
	var tbl 		= document.getElementById('tblOrderPopupTbody');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);
	row.id			= 'S';
	
	var cell		= row.insertCell(0);
	cell.innerHTML  = "&nbsp;";
	
	var cell		= row.insertCell(1);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML  = "<input type=\"checkbox\" id=\"chkOrder\" name=\"chkOrder\" class=\"clsChkOrder validate[minCheckbox[1]]\">";
	
	var cell 		= row.insertCell(2);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsOAMarketer';
	cell.id			= marketerId;
	cell.innerHTML 	= (marketerName==''?'&nbsp;':marketerName);
	
	var cell 		= row.insertCell(3);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsOACustomer';
	cell.id			= customerId;
	cell.innerHTML 	= (customerName==''?'&nbsp;':customerName);
	
	var cell 		= row.insertCell(4);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'clsOAOrderNo';
	cell.id			= orderNo+"/"+orderYear;
	cell.innerHTML 	= orderNo+" / "+orderYear;
	
	var cell 		= row.insertCell(5);
	cell.setAttribute("style",'text-align:left');
	cell.innerHTML 	= (PONo==''?'&nbsp;':PONo);
	
	var cell 		= row.insertCell(6);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsOASalesOrderNo';
	cell.id			= salesOrderId;
	cell.innerHTML 	= (salesOrderNo==''?'&nbsp;':salesOrderNo);
	
	var cell 		= row.insertCell(7);
	cell.setAttribute("style",'text-align:left');
	cell.innerHTML 	= (brand==''?'&nbsp;':brand);
	
	var cell 		= row.insertCell(8);
	cell.setAttribute("style",'text-align:left');
	cell.innerHTML 	= (style==''?'&nbsp;':style);
	
	var cell 		= row.insertCell(9);
	cell.setAttribute("style",'text-align:left');
	cell.innerHTML 	= (graphic==''?'&nbsp;':graphic);
	
	var cell 		= row.insertCell(10);
	cell.setAttribute("style",'text-align:left');
	cell.innerHTML 	= (combo==''?'&nbsp;':combo);
	
	var cell 		= row.insertCell(11);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'clsOAPrintMethod';
	cell.innerHTML 	= printMethodHTML;
	
	var cell 		= row.insertCell(12);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'clsOAPlateSize';
	cell.innerHTML 	= plateSizeHTML;
	
	var cell 		= row.insertCell(13);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'clsOAPSD';
	cell.innerHTML 	= (PSD==''?'&nbsp;':PSD);
	
	var cell 		= row.insertCell(14);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsOAQty';
	cell.innerHTML 	= (qty==''?'&nbsp;':qty);
	
	var cell 		= row.insertCell(15);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsOAColors';
	cell.innerHTML 	= (colors==''?'&nbsp;':colors);
	
	var cell 		= row.insertCell(16);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsOAStocks';
	cell.innerHTML 	= (strocks==''?'&nbsp;':strocks);
	
	var cell 		= row.insertCell(17);
	cell.setAttribute("style",'text-align:right');
	cell.innerHTML 	= "<input name=\"txtManuPanel\" id=\"txtManuPanel\" type=\"text\" style=\"width:100%;text-align:right\" class=\"clsPanel\" "+(panelAddMode!=1?'disabled="disabled"':'')+" />";
	
}
function deleteRow()
{
	var rowCount = 0;
	$('#tblOrderPopup .clsChkOrder').each(function(){
        
		if($(this).parent().parent().attr('id')=='M')
			rowCount++;
    });
	if(rowCount>1)
		$(this).parent().parent().remove();
	
	$('#frmOrderAllocation #tblOrderPopup tbody tr').removeClass('cls_first_row');
	$('#frmOrderAllocation #tblOrderPopup tbody #M').addClass('cls_first_row');
}
function addNewRow()
{
	var lastDateIdArr 	= $('#frmOrderAllocation #tblOrderPopup tbody tr:last').find('.clsPSDDate').attr('id').split('~');
	var lastDateNo		= parseFloat(lastDateIdArr[1]);
	$('#frmOrderAllocation #tblOrderPopup tr:last').after("<tr id='M'>"+$('#frmOrderAllocation #tblOrderPopup tbody .cls_first_row').html()+"</tr>");
	$('#frmOrderAllocation #tblOrderPopup tbody tr:last').find('.clsPSDDate').attr('id','txtPSDDate~'+(lastDateNo+1));
}
function chkAll()
{
	if($(this).attr('checked'))
	{
		$('#tblOrderPopup .clsChkOrder').each(function() {

			if(!$(this).attr('disabled'))
			{
				$(this).attr("checked",true);
				$(this).parent().parent().find('.clsPanel').addClass('validate[required]');
				$(this).parent().parent().find('.clsPrintMethod').addClass('validate[required]');
				$(this).parent().parent().find('.clsPlateSize').addClass('validate[required]');
				$(this).parent().parent().find('.clsCustomer').addClass('validate[required]');
			}
        });
	}
	else
	{
		$('.clsChkOrder').attr("checked",false);
		$('.clsPanel').removeClass('validate[required]');
		$('.clsPrintMethod').removeClass('validate[required]');
		$('.clsPlateSize').removeClass('validate[required]');
		$('.clsCustomer').removeClass('validate[required]');
	}
}
function setPanelClass()
{
	if($(this).attr('checked'))
	{
		$(this).parent().parent().find('.clsPanel').addClass('validate[required]');
		$(this).parent().parent().find('.clsPrintMethod').addClass('validate[required]');
		$(this).parent().parent().find('.clsPlateSize').addClass('validate[required]');
		$(this).parent().parent().find('.clsCustomer').addClass('validate[required]');
	}
	else
	{
		$(this).parent().parent().find('.clsPanel').removeClass('validate[required]');
		$(this).parent().parent().find('.clsPrintMethod').removeClass('validate[required]');
		$(this).parent().parent().find('.clsPlateSize').removeClass('validate[required]');
		$(this).parent().parent().find('.clsCustomer').removeClass('validate[required]');
	}
}
function setCustomerId()
{
	var customerId = ($(this).val()==''?'null':$(this).val());
	$(this).parent().attr('id',customerId);
}
function setMarketerId()
{
	var marketerId = ($(this).val()==''?'null':$(this).val());
	$(this).parent().attr('id',marketerId);
}
function setPrintMethodId()
{
	if($(this).val()==2)
	{
		$(this).parent().parent().find('.clsPlateSize').attr('disabled',false);
		$(this).parent().parent().find('.clsPlateSize').addClass('validate[required]');
	}
	else
	{
		$(this).parent().parent().find('.clsPlateSize').val('');
		$(this).parent().parent().find('.clsOAPlateSize').attr('id','null');
		$(this).parent().parent().find('.clsPlateSize').attr('disabled',true);
		$(this).parent().parent().find('.clsPlateSize').removeClass('validate[required]');
	}
		
	var methodId = ($(this).val()==''?'null':$(this).val());
	$(this).parent().attr('id',methodId);
}
function setPlateSizeId()
{
	var PlateSizeId = ($(this).val()==''?'null':$(this).val());
	$(this).parent().attr('id',PlateSizeId);
}
function addOrders()
{
	showWaiting();
	if($('#frmOrderAllocation #tblOrderPopup').validationEngine('validate'))
	{
		var chkStatus  	= false;
		var arrDetails	= "";
		var arrHeader = "{";
						arrHeader += '"fromDate":"'+fromDate+'",' ;
						arrHeader += '"toDate":"'+toDate+'",' ;
						arrHeader += '"customer":"'+customer+'",' ;
						arrHeader += '"planLocation":"'+planLocation+'",' ;
						arrHeader += '"salesOrder":"'+salesOrder+'",' ;
						arrHeader += '"style":"'+style+'",' ;
						arrHeader += '"graphic":"'+graphic+'",' ;
						arrHeader += '"printType":"'+printType+'"' ;

			arrHeader  += "}";
				
		$('#tblOrderPopup .clsChkOrder').each(function(){
			
			if($(this).attr('checked'))
			{
				chkStatus = true;
				var orderType		= $(this).parent().parent().attr('id');
				var marketerId		= $(this).parent().parent().find('.clsOAMarketer').attr('id');
				var customerId		= $(this).parent().parent().find('.clsOACustomer').attr('id');
				var orderNoArr		= $(this).parent().parent().find('.clsOAOrderNo').attr('id');
				var salesOrderId	= $(this).parent().parent().find('.clsOASalesOrderNo').attr('id');
				var printMethod		= $(this).parent().parent().find('.clsOAPrintMethod').attr('id');
				var plateSize		= $(this).parent().parent().find('.clsOAPlateSize').attr('id');
				var qty				= $(this).parent().parent().find('.clsOAQty').html();
				var colors			= $(this).parent().parent().find('.clsOAColors').html();
				var strocks			= $(this).parent().parent().find('.clsOAStocks').html();
				var panels			= $(this).parent().parent().find('.clsPanel').val();
				
				arrDetails += "{";
				arrDetails += '"orderType":"'+ orderType +'",' ;
				arrDetails += '"customerId":"'+ customerId +'",' ;
				arrDetails += '"marketerId":"'+ marketerId +'",' ;
				arrDetails += '"orderNoArr":"'+ orderNoArr +'",' ;
				arrDetails += '"salesOrderId":"'+ salesOrderId +'",' ;
				arrDetails += '"printMethod":"'+ printMethod +'",' ;
				arrDetails += '"plateSize":"'+ plateSize +'",' ;
				
				if(orderType=='M')
				{
					var PONo			= $(this).parent().parent().find('.clsPONo').val();
					var salesOrderNo	= $(this).parent().parent().find('.clsSalesOrderNo').val();
					var style			= $(this).parent().parent().find('.clsStyle').val();
					var brand			= $(this).parent().parent().find('.clsBrand').val();
					var graphic			= $(this).parent().parent().find('.clsGraphic').val();
					var combo			= $(this).parent().parent().find('.clsCombo').val();
					var psdDate			= $(this).parent().parent().find('.clsPSDDate').val();
					qty					= $(this).parent().parent().find('.clsQty').val();
					colors				= $(this).parent().parent().find('.clsColor').val();
					strocks				= $(this).parent().parent().find('.clsStrock').val();
					
					
					arrDetails += '"PONo":"'+ PONo +'",' ;
					arrDetails += '"salesOrderNo":"'+ salesOrderNo +'",' ;
					arrDetails += '"style":'+ URLEncode_json(style) +',' ;
					arrDetails += '"brand":'+ URLEncode_json(brand) +',' ;
					arrDetails += '"graphic":'+ URLEncode_json(graphic) +',' ;
					arrDetails += '"combo":'+ URLEncode_json(combo) +',' ;
					arrDetails += '"psdDate":"'+ psdDate +'",' ;		
				}
				
				arrDetails += '"qty":"'+ qty +'",' ;
				arrDetails += '"colors":"'+ colors +'",' ;
				arrDetails += '"strocks":"'+ strocks +'",' ;
				arrDetails += '"panels":"'+ panels +'"' ;
				arrDetails +=  '},';
			}
		});	
		if(!chkStatus)
		{
			$('#frmOrderAllocation #butOrderAdd').validationEngine('showPrompt','No records to save.','fail');
			hideWaiting();
			return;
		}
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		
		var data		= "requestType=addOrders";
		var arrHeader	= arrHeader;
		var arrDetails	= '['+arrDetails+']';
		data+="&arrHeader="+arrHeader+"&arrDetails="+arrDetails+'&arrType='+arrType;
		
		var url = "planningBoard-db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					if(json.type=='pass')
					{
						//addSaveDataToPlanBoard();
						$("#tblPlanningBoard tfoot tr:last").after(json.detailHtml);
						hideWaiting();
						disablePopup();
						return;
					}
					else
					{
						$('#frmOrderAllocation #butOrderAdd').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						hideWaiting();
						return;
					}
				},
				error:function(xhr,status){
						
						$('#frmOrderAllocation #butOrderAdd').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
				}		
		});
	}
	else
	{
		hideWaiting();
	}
}
function searchData()
{
	if($('#frmSearchPopup').validationEngine('validate'))
	{
		fromDate		= $('#txtSrhFromDate').val();
		toDate			= $('#txtSrhToDate').val();
		customer		= $('#cboSrhCustomer').val();
		planLocation	= $('#cboSrhLocation').val();
		salesOrder		= $('#txtSrhSalesOrder').val();
		style			= $('#txtSrhStyle').val();
		graphic			= $('#txtSrhGraphic').val();
		printType		= $('#cboSrhPrintType').val();
		printTypeName	= $('#cboSrhPrintType option:selected').text();
		
		var difference = (Date.parse(toDate) - Date.parse(fromDate)) / (86400000 * 7);
		if(difference<0 || difference==0 )
		{
			$('#frmSearchPopup #butSearch').validationEngine('showPrompt','* Invalid Date Range','fail');
			return;
		}
		
		addSaveDataToPlanBoard();
		disablePopup();
	}
}
function addSaveDataToPlanBoard()
{
	showPlanWaiting();
	$("#tblPlanningBoard thead tr").remove();
	$("#tblPlanningBoard tfoot tr").remove();
	
	var data = "requestType=loadPlanData";
	var arrHeader = "{";
						arrHeader += '"fromDate":"'+fromDate+'",' ;
						arrHeader += '"toDate":"'+toDate+'",' ;
						arrHeader += '"customer":"'+customer+'",' ;
						arrHeader += '"planLocation":"'+planLocation+'",' ;
						arrHeader += '"salesOrder":"'+salesOrder+'",' ;
						arrHeader += '"style":"'+style+'",' ;
						arrHeader += '"graphic":"'+graphic+'",' ;
						arrHeader += '"printType":"'+printType+'"' ;

		arrHeader  += "}";
	
	var arrHeader	= arrHeader;
		data	   += "&arrHeader="+arrHeader+'&arrType='+arrType;
			
	var url 	= "planningBoard-db.php";
	var httpobj = 	$.ajax({
						url:url,
						dataType:'json',
						type:'post',
						data:data,
						async:false,
						success:function(json){
							
							$("#tblPlanningBoard thead").html(json.header);
							$("#tblPlanningBoard tbody").html(json.dateDetail);
							$("#tblPlanningBoard tfoot").html(json.detail);
							$("#printTypeName").html(printTypeName);
						
						},
						error:function(xhr,status){
							
						}		
					});
	hidePlanWaiting();
}
function deletePlan()
{
	var objThis	= this;
	var planId 	= $(this).parent().parent().attr('id');
	
	var val = $.prompt('Are you sure you want to Delete?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
					var url = "planningBoard-db.php?requestType=deletePlan&planId="+planId;
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: 'json',  
						data:'',
						async:false,
						success:function(json){
								if(json.type=='pass')
								{
									$("#tblPlanningBoard .clsPlan"+planId).remove();
									return;
								}
								else
								{
									$(objThis).validationEngine('showPrompt', json.msg,json.type );
									return;
								}
							},
						error:function(xhr,status){
								
								$(objThis).validationEngine('showPrompt', errormsg(xhr.status),'fail');
								return;
							}		
						});
					
					}
				
			}});
}
function saveData()
{
	showWaiting();
	var chkStatus  	= false;
	var arrDetails	= "";
	$('.clsPlanQty').each(function() {
        
		var planId	= $(this).parent().parent().attr('id');
		var date	= $(this).attr('id');
		var typeId	= $(this).parent().parent().find('.clsType').attr('id');
		var qty		= $(this).val();
		
		arrDetails += "{";
		arrDetails += '"planId":"'+ planId +'",' ;
		arrDetails += '"date":"'+ date +'",' ;
		arrDetails += '"typeId":"'+ typeId +'",' ;
		arrDetails += '"qty":"'+ qty +'"' ;
		arrDetails +=  '},';

    });
	arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		
	var data		= "requestType=saveData";
	var arrDetails	= '['+arrDetails+']';
		data	   += "&arrDetails="+arrDetails;
	
	var url = "planningBoard-db.php";
	$.ajax({
			url:url,
			dataType:'json',
			type:'post',
			data:data,
			async:false,
			success:function(json){
				$('#frmPlanningBoard #butSave').validationEngine('showPrompt', json.msg,json.type );
				if(json.type=='pass')
				{
					hideWaiting();
					var t=setTimeout("alertx()",1000);
					return;
				}
				else
				{
					$('#frmPlanningBoard #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					hideWaiting();
					return;
				}
			},
			error:function(xhr,status){
					
					$('#frmPlanningBoard #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					hideWaiting();
					return;
			}		
	});
}
function setTotals()
{
	var totStrock 		= 0;
	var totGroups 		= 0;
	var date			= $(this).attr('id');
	var rowId			= $(this).parent().parent().attr('id');
	//var colors			= parseFloat($('.clsPlan'+rowId).find('.clsNoOfColors').html());
	var stroks			= parseFloat($('.clsPlan'+rowId).find('.clsNoOfStrocks').html());
	var panels			= parseFloat($('.clsPlan'+rowId).find('.clsNoOfPanels').html());
	var avilableStocks	= parseFloat($('.clsAilableStrocks'+date).html());
	var avilableGroups	= parseFloat($('.clsAilableGroups'+date).html());
	var prodPlanAmount	= $('.clsPlan'+rowId).find('.PRODUCTION_PLAN'+date).val();
	var strockAmount	= RoundNumber((parseFloat(prodPlanAmount==''?0:prodPlanAmount)*stroks/panels),0);
	$('.clsPlan'+rowId).find('.STROCK_PLAN'+date).val(strockAmount);
	
	if(printType==2)
	{
		var strockTarget	= parseFloat($(this).parent().parent().find('.clsPlateSize').attr('id'));
		var noOfGroups		= RoundNumber((strockAmount/strockTarget),2);

		var noOfGroupsArr 	= noOfGroups.split('.');
		var deci 			= noOfGroupsArr[1].substring(0,2);
		if(parseFloat(deci)>50)
			noOfGroups		= parseFloat(noOfGroupsArr[0])+1 ;
		else
			noOfGroups		= parseFloat(noOfGroupsArr[0])+0.5 ;
		
		if(prodPlanAmount=='' || prodPlanAmount==0)
			noOfGroups = 0;
	}
	else
	{
		noOfGroups = 0;
	}
			
	$('.clsPlan'+rowId).find('.GROUP_ALLOCATION'+date).val(noOfGroups);
	
	$('.STROCK_PLAN'+date).each(function() {
	 
			totStrock += parseFloat(($(this).val()==''?0:$(this).val()));
	});
	$('.GROUP_ALLOCATION'+date).each(function() {
	   
			totGroups += parseFloat(($(this).val()==''?0:$(this).val()));
	});
	
	var utilizedStrocks		= RoundNumber(((totStrock/avilableStocks)*100),1);
	var utilizedGroups		= RoundNumber(((totGroups/avilableGroups)*100),1);
	
	$('.clsTotStocks'+date).html(totStrock);
	$('.clsTotGroups'+date).html(totGroups);
	$('.clsUtilizedStrocks'+date).html(utilizedStrocks+'%');
	$('.clsUtilizedGroups'+date).html(utilizedGroups+'%');
	$('.clsDailyPlan'+date).html(totStrock);
	
	var arrDetails	= "";
	$('.clsPlan'+rowId).each(function() {
		
			var typeId	= $(this).find('.clsType').attr('id');
			var Qty		= $(this).find('.clsPlanQty'+date).val();
			
			arrDetails += "{";
			arrDetails += '"typeId":"'+ typeId +'",' ;
			arrDetails += '"Qty":"'+ Qty +'",' ;
			arrDetails += '"planId":"'+ rowId +'",' ;
			arrDetails += '"date":"'+ date +'"' ;
			arrDetails +=  '},';
	});
	
	arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		
	var data		= "requestType=saveData";
	var arrDetails	= '['+arrDetails+']';
		data	   += "&arrDetails="+arrDetails;
	
	var url = "planningBoard-db.php";
	$.ajax({
			url:url,
			dataType:'json',
			type:'post',
			data:data,
			async:false,
			success:function(json){
				
			},
			error:function(xhr,status){
			
			}		
	});
	
}
function alertx()
{
	$('#frmPlanningBoard #butSave').validationEngine('hide');
}
function showPlanWaiting()
{
		var popupbox = document.createElement("div");
		var windowWidth = document.documentElement.clientWidth;
		var windowHeight = document.documentElement.clientHeight;
		var scrollH = (document.body.scrollHeight);
		//alert(windowHeight);
   popupbox.id = "divBackGroundBalck";
   popupbox.style.position = 'absolute';
   popupbox.style.zIndex = 100;
   popupbox.style.textAlign = 'center';
   popupbox.style.left = 0 + 'px';
   popupbox.style.top = 0 + 'px'; 
   popupbox.style.width = screen.width + 'px';
   popupbox.style.height =  (scrollH)+ 'px';
   popupbox.style.opacity = 0.5;
	document.body.appendChild(popupbox);
	//document.getElementById('divBackGroundBalck').innerHTML = "this is text code";
	var popupbox1 = document.createElement("div");
	 popupbox1.id = "divBackgroundImg";
   popupbox1.style.position = 'absolute';
   popupbox1.style.zIndex = 101;
   popupbox1.style.verticalAlign = 'center';
   popupbox1.style.left =  windowWidth/2-150 +'px';
   popupbox1.style.top = ($(window).scrollTop()+100) + 'px'; 
   popupbox1.style.width = '100px';
   popupbox1.style.height =  '100px';
   popupbox1.style.opacity = 1;
	document.body.appendChild(popupbox1);
	
	//alert(mainPath);
	document.getElementById('divBackgroundImg').innerHTML = "<img src=\"../../../../images/loading_plan.gif\" />";
	
}
function hidePlanWaiting()
{
	try
	{
		var box = document.getElementById('divBackGroundBalck');
		box.parentNode.removeChild(box);
		
		var box1 = document.getElementById('divBackgroundImg');
		box1.parentNode.removeChild(box1);
		
	}
	catch(err)
	{        
	}	
}
function loadComboes(slected,flag1,flag2,obj)
{
	if($(obj).val()=="")
	{
		if(flag1==1)
			$(obj).parent().parent().find(".clsMOAOrderNo").html('');
		if(flag2==1)
			$(obj).parent().parent().find(".clsMOASalesOrder").html('');
		return;
	}	
	if(flag1==1)
		$(obj).parent().parent().find(".clsMOAOrderNo").html('');
	if(flag2==1)
		$(obj).parent().parent().find(".clsMOASalesOrder").html('');
	
	var orderYear	= $(obj).parent().parent().find(".clsMOAOrderYear").val();	
	var orderNo		= $(obj).parent().parent().find(".clsMOAOrderNo").val();
	
	if(orderNo==null)
		orderNo='';
	
	var customerId	= $(obj).parent().parent().find('.tdClsOrderYear').attr('id');
		
	var url 		= "planningBoard-db.php?requestType=loadAllComboes";
	var httpobj = $.ajax({
		url		:url,
		dataType:'json',
		data	:"&orderYear="+orderYear+"&orderNo="+orderNo+"&slected="+slected+"&customerId="+customerId,
		async:false,
		success:function(json)
		{
			if(slected=='orderYear')
			$(obj).parent().parent().find(".clsMOAOrderNo").html(json.combo)
			if(slected=='orderNo')
			$(obj).parent().parent().find(".clsMOASalesOrder").html(json.combo)		
		}
	});
}
function manualOrderAllocationSave()
{
	if($('#frmMOrderAlloPopup').validationEngine('validate'))
	{	
		var planId			= $(this).parent().parent().attr('id'); 
		var orderYear		= $(this).parent().parent().find('.clsMOAOrderYear').val(); 
		var orderNo			= $(this).parent().parent().find('.clsMOAOrderNo').val(); 
		var salesOrderId	= $(this).parent().parent().find('.clsMOASalesOrder').val(); 
		
		var arrHeader = "{";
							arrHeader += '"planId":"'+planId+'",' ;
							arrHeader += '"orderYear":"'+orderYear+'",' ;
							arrHeader += '"orderNo":"'+orderNo+'",' ;
							arrHeader += '"salesOrderId":"'+salesOrderId+'"' ;
			arrHeader  += "}";
	
	var arrHeader	= arrHeader;
		
		var data	= "requestType=manualOrderAllocationSave";
		data	   += "&arrHeader="+arrHeader;
		
		var url = "planningBoard-db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					if(json.type=='pass')
					{
						$(MOrderObj).parent().parent().find('.clsSalesOrderNo').html(json.salesOrder);
						$(MOrderObj).parent().parent().find('.clsStyle').html(json.style);
						$(MOrderObj).parent().parent().find('.clsGraphis').html(json.graphic);
						$(MOrderObj).parent().html('&nbsp;');
						hideMOrderForm();
						MOrderObj = '';
						return;
					}
					else
					{
						$('#frmMOrderAlloPopup #butMOAllocate').validationEngine('showPrompt', json.msg,json.type);
						return;
					}
				},
				error:function(xhr,status){
						
					$('#frmMOrderAlloPopup #butMOAllocate').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					return;
				}		
		});
	}
}
function loadManualOrderForm(e,obj)
{
	$('#manualOrderAllc').css(
	{
		top: e.pageY+'px',left:e.pageX+'px'	
	})
	$('#manualOrderAllc').show(500);
	
	var customerId	= $(obj).parent().parent().find('.clsCustomer').attr('id');
	
	$('#manualOrderAllc #tblMOAMain tbody tr').attr('id',$(obj).parent().parent().attr('id'));
	$('#manualOrderAllc #tblMOAMain .tdClsOrderYear').attr('id',customerId);
	MOrderObj	= obj;
	
	var url 	= "planningBoard-db.php?requestType=loadOrderYear";
	var data 	= "customerId="+customerId;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#manualOrderAllc #tblMOAMain .clsMOAOrderYear').html(json.orderCombo);
			}
	});	
}
function hideMOrderForm()
{
	$('#manualOrderAllc #tblMOAMain tbody tr').attr('id','');
	$('#manualOrderAllc #tblMOAMain .tdClsOrderYear').attr('id','');
	$('#manualOrderAllc #tblMOAMain .clsMOAOrderYear').html('');
	$('#manualOrderAllc #tblMOAMain .clsMOAOrderNo').html('');
	$('#manualOrderAllc #tblMOAMain .clsMOASalesOrder').html('');
	$('#manualOrderAllc').hide(500);
}
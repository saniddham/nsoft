<?php
session_start();
$backwardseperator 		= "../../../../";
$userId 				= $_SESSION['userId'];
$locationId	  			= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];

include $backwardseperator."dataAccess/Connector.php";
//include_once  "../../../../class/finance/cls_common_get.php";
//include_once  "../../../../class/productionAndPlanning/cls_plan_common_get.php";
include_once  "../../../../class/productionAndPlanning/planningBoard/cls_planningboard_get.php";

//$obj_common_get			= new Cls_Common_Get($db);
//$obj_plan_common_get 	= new Cls_Plan_Common_Get($db);
$obj_planningBoard_get	= new Cls_PlanningBoard_Get($db,$locationId,$companyId,$userId);

$planId					= $_REQUEST['planId'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Manual Order Allocation</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />

</head>
<form id="frmMOrderAlloPopup" name="frmMOrderAlloPopup">
<table width="400" border="0" align="center" bgcolor="#FFFFFF">
    <tr>
        <td>
        <table width="100%" class="bordered" id="tblMOAMain" >
        	<thead>
            	<tr>
                	
                    <th width="23%">Order Year</th>
                    <th width="28%">Order No</th>
                    <th width="31%">Sales Order</th>
                    <th width="18%">&nbsp;</th>
                </tr>
            </thead>
            <tbody>

					<tr class="normalfnt" id="<?php echo $planId; ?>">
                        <td style="text-align:center"><select name="cboOrderYear" id="cboOrderYear" style="width:100%" class="clsMOAOrderYear">
                    <?php
						echo $obj_planningBoard_get->getMOAOrderYear();
					?>
                    	</select></td>
                        <td style="text-align:center"><select name="cboOrderNo" id="cboOrderNo" style="width:100%" class="clsMOAOrderNo">
                    	</select></td>
                        <td style="text-align:center"><select name="cboSalesOrder" id="cboSalesOrder" style="width:100%" class="clsMOASalesOrder">
                    	</select></td>
                        <td style="text-align:center"><a class="button green small" id="butMOASave" name="butMOASave">Allocate</a></td>
					</tr>
            </tbody>
        </table>
        </td>
    </tr>
</table>
</form>
</body>
</html>
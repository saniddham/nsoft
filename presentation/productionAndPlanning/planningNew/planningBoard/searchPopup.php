<?php
session_start();
$backwardseperator 		= "../../../../";
$userId 				= $_SESSION['userId'];
$locationId	  			= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];

include $backwardseperator."dataAccess/Connector.php";
include_once  "../../../../class/finance/cls_common_get.php";
include_once  "../../../../class/productionAndPlanning/cls_plan_common_get.php";

$obj_common_get			= new Cls_Common_Get($db);
$obj_plan_common_get 	= new Cls_Plan_Common_Get($db);

$fromDate				= date("Y-m-01", strtotime("last month"));
$toDate 				= date('Y-m-t');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Planning Search</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />

</head>
<form id="frmSearchPopup" name="frmSearchPopup">
<div align="center">
<div class="trans_layoutS">
<div class="trans_text">Planning Search</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <tr>
        <td>
            <table width="100%" border="0">
                <tr class="normalfnt">
                  <td>Date From</td>
                  <td width="33%"><input name="txtSrhFromDate" type="text" id="txtSrhFromDate" style="width:100px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                  <td width="7%">To</td>
                  <td width="36%"><input name="txtSrhToDate" type="text" id="txtSrhToDate" style="width:100px;" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                </tr>
                <tr class="normalfnt">
                    <td width="24%">Customer</td>
                    <td colspan="3"><select name="cboSrhCustomer" id="cboSrhCustomer" style="width:300px">
                    <?php
						echo $obj_common_get->getCustomerCombo('');
					?>
                    </select></td>
                </tr>
                <tr class="normalfnt">
                  <td>Location</td>
                  <td colspan="3"><select name="cboSrhLocation" id="cboSrhLocation" style="width:300px">
                    <?php
						echo $obj_plan_common_get->getLocation($userId,$locationId);
					?>
                  </select></td>
                </tr>
                <tr class="normalfnt">
                    <td>Sales Order</td>
                    <td colspan="3"><input type="text" name="txtSrhSalesOrder" id="txtSrhSalesOrder" style="width:200px" /></td>
                </tr>
                <tr class="normalfnt">
                  <td>Style</td>
                  <td colspan="3"><input type="text" name="txtSrhStyle" id="txtSrhStyle" style="width:200px" /></td>
                </tr>
                <tr class="normalfnt">
                  <td>Graphic</td>
                  <td colspan="3"><input type="text" name="txtSrhGraphic" id="txtSrhGraphic" style="width:200px" /></td>
                </tr>
                <tr class="normalfnt">
                  <td>Print Type</td>
                  <td colspan="3"><select name="cboSrhPrintType" id="cboSrhPrintType" style="width:200px" class="validate[required]">
                    <?php
						echo $obj_plan_common_get->getPrinterMethodCombo('search');
					?>
                    </select></td>
                </tr>
            </table>
        </td>
    </tr>	
    <tr>
      <td height="32" >
        <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
          <tr>
            <td align="center" class="tableBorder_allRound"><a class="button white medium" id="butSearch" name="butSearch">Search</a><a class="button white medium" id="butSrhClose" name="butSrhClose">Close</a></td>
            </tr>
          </table>
        </td>
    </tr>
</table>
</div>
</div>
</form>
</body>
</html>
<script>
	$('#txtSrhFromDate').val(fromDate);
	$('#txtSrhToDate').val(toDate);
</script>
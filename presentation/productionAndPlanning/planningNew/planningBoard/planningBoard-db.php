<?php 
session_start();
ini_set('max_execution_time',600);

$backwardseperator 		= "../../../../";
$mainPath 				= $_SESSION['mainPath'];
$userId 				= $_SESSION['userId'];
$locationId	  			= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];

$requestType 			= $_REQUEST['requestType'];
$programCode			= 'P0755';

$savedStatus			= true;
$savedMasseged 			= '';
$error_sql				= '';

include 	  "../../../../dataAccess/Connector.php";
include_once  "../../../../class/productionAndPlanning/planningBoard/cls_planningboard_get.php";
include_once  "../../../../class/productionAndPlanning/planningBoard/cls_planningboard_set.php";
include_once  "../../../../class/cls_commonFunctions_get.php";

$obj_planningBoard_get	= new Cls_PlanningBoard_Get($db,$locationId,$companyId,$userId);
$obj_planningBoard_set	= new Cls_PlanningBoard_Set($db,$locationId,$companyId,$userId);
$obj_common				= new cls_commonFunctions_get($db);

$deleteMode				= $obj_common->Load_menupermision($programCode,$userId,'intDelete');
$saveMode				= $obj_common->Load_menupermision($programCode,$userId,'intAdd');

if($requestType=='loadOrderAllocationData')
{
	$customer			= $_REQUEST['customer'];
	$salesOrderNo		= $_REQUEST['salesOrderNo'];
	$brand				= $_REQUEST['brand'];
	$style				= $_REQUEST['style'];
	$graphic			= $_REQUEST['graphic'];
	$planLocation		= $_REQUEST['planLocation'];
	
	$response['arrOrderData']	= $obj_planningBoard_get->loadOrderData($customer,$salesOrderNo,$brand,$style,$graphic,$planLocation);
	
	echo json_encode($response);
}
else if($requestType=='addOrders')
{
	$arrDetails 	= json_decode($_REQUEST['arrDetails'],true);
	$arrHeader 		= json_decode($_REQUEST['arrHeader'],true);	
	$arrType 		= json_decode($_REQUEST['arrType'],true);
	
	$planocation	= $arrHeader['planLocation'];
	
	$html			= '';
	
	$db->begin();
	
	foreach($arrDetails as $array_loop)
	{
		$orderType			= $array_loop["orderType"];
		$customerId			= $array_loop["customerId"];
		$marketerId			= $array_loop["marketerId"];
		$orderNoArr			= explode("/",$array_loop["orderNoArr"]);
		$orderNo			= ($orderNoArr[0]==''?'null':$orderNoArr[0]);
		$orderYear			= ($orderNoArr[1]==''?'null':$orderNoArr[1]);
		$salesOrderId		= $array_loop["salesOrderId"];
		$printMethod		= $array_loop["printMethod"];
		$plateSize			= $array_loop["plateSize"];
		$qty				= ($array_loop["qty"]==''?0:$array_loop["qty"]);
		$colors				= ($array_loop["colors"]==''?0:$array_loop["colors"]);
		$strocks			= ($array_loop["strocks"]==''?0:$array_loop["strocks"]);
		$panels				= ($array_loop["panels"]==''?0:$array_loop["panels"]);
		$psdDate			= $array_loop["psdDate"];
		
		$resultArr 			= $obj_planningBoard_set->saveOrderBookHeaderData($customerId,$orderNo,$orderYear,$salesOrderId,$printMethod,$plateSize,$qty,$colors,$strocks,$panels,$marketerId,$planocation);
		if($resultArr['savedStatus']=='fail' && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged	= $resultArr['savedMassege'];
			$error_sql		= $resultArr['error_sql'];
		}
		
		$planId				= $resultArr['planId'];
		
		if($orderType=='M')
		{
			$PONo				= $array_loop["PONo"];
			$salesOrderNo		= $obj_common->replace($array_loop["salesOrderNo"]);
			$style				= $obj_common->replace($array_loop["style"]);
			$brand				= $obj_common->replace($array_loop["brand"]);
			$graphic			= $obj_common->replace($array_loop["graphic"]);
			$combo				= $obj_common->replace($array_loop["combo"]);
			
			$resultMArr 		= $obj_planningBoard_set->saveOrderBookManualData($planId,$PONo,$salesOrderNo,$style,$brand,$graphic,$combo,$psdDate);
			if($resultMArr['savedStatus']=='fail' && ($savedStatus))
			{
				$savedStatus	= false;
				$savedMasseged	= $resultMArr['savedMassege'];
				$error_sql		= $resultMArr['error_sql'];
			}
			
			$resultUpdMArr 		= $obj_planningBoard_set->updateOrderBookManualStatus($planId);
			if($resultUpdMArr['savedStatus']=='fail' && ($savedStatus))
			{
				$savedStatus	= false;
				$savedMasseged	= $resultUpdMArr['savedMassege'];
				$error_sql		= $resultUpdMArr['error_sql'];
			}
		}
		$getSavedOrderHtml		= getSavedOrderHTML($planId,$arrHeader,$arrType);	
		$html				   .= $getSavedOrderHtml;
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['detailHtml'] = $html;
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);	
}
else if($requestType=='loadPlanData')
{
	$arrHeader 		= json_decode($_REQUEST['arrHeader'],true);
	
	$fromDate		= $arrHeader["fromDate"];
	$toDate			= $arrHeader["toDate"];
	$customer		= $arrHeader["customer"];
	$planLocation	= $arrHeader["planLocation"];
	$salesOrder		= $arrHeader["salesOrder"];
	$style			= $arrHeader["style"];
	$graphic		= $arrHeader["graphic"];
	$printType		= $arrHeader["printType"];
		
	$arrType 		= json_decode($_REQUEST['arrType'],true);
	$headerHtml		= getHeaderHTML($fromDate,$toDate,$customer,$planLocation,$salesOrder,$style,$graphic,$printType);
	$dateHtml		= getDateHTML($fromDate,$toDate);
	$html			= "<tr></tr>";
	
	
	$header_result	= $obj_planningBoard_get->getHeaderData($customer,$planLocation,$salesOrder,$style,$graphic,$printType,'','RunQuery');
	while($rowH = mysqli_fetch_array($header_result))
	{
		$customer		= ($rowH['customerName']==''?'&nbsp;':$rowH['customerName']);
		$brand			= ($rowH['brand']==''?'&nbsp;':$rowH['brand']);
		$plateSize		= ($rowH['plateSizeName']==''?'&nbsp;':$rowH['plateSizeName']);
		$plateSizeId	= $rowH['plateSizeId'];
		$targetStrocks	= $rowH['targetStrocks'];
		$style			= ($rowH['styleNo']==''?'&nbsp;':$rowH['styleNo']);
		$graphic		= ($rowH['graphicNo']==''?'&nbsp;':$rowH['graphicNo']);
		$salesOrderNo 	= ($rowH['salesOrderNo']==''?'&nbsp;':$rowH['salesOrderNo']);
		$noOfColors 	= ($rowH['noOfColors']==''?'&nbsp;':$rowH['noOfColors']);
		$noOfStocks 	= ($rowH['noOfStrocks']==''?'&nbsp;':$rowH['noOfStrocks']);
		$noOfPanels 	= ($rowH['noOfPanels']==''?'&nbsp;':$rowH['noOfPanels']);
		$POQty 			= ($rowH['POQty']==''?'&nbsp;':$rowH['POQty']);
		$MStatus 		= $rowH['manualOrderStatus'];
		$customerId 	= $rowH['customerId'];
		$delImg			= "<img border=\"0\" src=\"../../../../images/del.png\" name=\"butDelPlan\" class=\"mouseover clsDelPlan\" id=\"butDelPlan\"/>";
		$editImg		= "<img border=\"0\" src=\"../../../../images/edit.png\" name=\"butEditPlan\" class=\"mouseover clsEditPlan\" id=\"butEditPlan\"/>";
		$graphicTitle		= $graphic;
		$salesOrderTitle	= $salesOrderNo;
		$styleTitle			= $style;
		$marketer			= ($rowH['marketer']==''?'&nbsp;':$rowH['marketer']);
		$i					= 0;
		foreach($arrType as $array_loop)
		{
			$planTypeViewMode = $obj_common->ValidateSpecialPermission($array_loop['permissionId'],$userId,'RunQuery');			
			$html .= "<tr class=\"normalfnt2BI clsPlan".$rowH['planId']."\" id=\"".$rowH['planId']."\">
						<td style=\"text-align:center\">".($deleteMode==1?$delImg:"&nbsp;")."</td>
						<td style=\"text-align:center\">".($MStatus==1?$editImg:"&nbsp;")."</td>";
			if($i==1)
				$html .="<td id=\"".$customerId."\" style=\"text-align:left\" nowrap=\"nowrap\" class=\"clsCustomer\">Marketer : ".$marketer."</td>";
			else
				$html .="<td id=\"".$customerId."\" style=\"text-align:left\" nowrap=\"nowrap\" class=\"clsCustomer\">".$customer."</td>";	
						
				$html .="<td style=\"text-align:left\" nowrap=\"nowrap\" class=\"clsSalesOrderNo\">".$salesOrderNo."</td>
						<td style=\"text-align:left\" nowrap=\"nowrap\" class=\"clsStyle\">".$style."</td>
						<td style=\"text-align:left\" nowrap=\"nowrap\" class=\"clsGraphis mouseover\">".$graphic."</td>
						<td id=\"".$targetStrocks."\" style=\"text-align:left\" nowrap=\"nowrap\" class=\"clsPlateSize\">".$plateSize."</td>
						<td style=\"text-align:right\" nowrap=\"nowrap\" class=\"clsNoOfColors\">".$noOfColors."</td>
						<td style=\"text-align:right\" nowrap=\"nowrap\" class=\"clsNoOfStrocks\">".$noOfStocks."</td>
						<td style=\"text-align:right\" nowrap=\"nowrap\" class=\"clsNoOfPanels\">".$noOfPanels."</td>
						<td style=\"text-align:right\" nowrap=\"nowrap\" class=\"clsPOQty\">".$POQty."</td>
						<td id=\"".$array_loop['typeId']."\" style=\"text-align:left\" nowrap=\"nowrap\" class=\"clsType\">".$array_loop['typeName']."</td>
					";
			$start 			= strtotime($fromDate);
			$end 			= strtotime($toDate);
			$days_between 	= ceil(abs($end - $start) / 86400);
			$newFrmDate 	= $fromDate;
			$color			= '';
			$disStatus		= '';
			for($t=0;$t<=$days_between;$t++)
			{
				$weekday = date('l', strtotime($newFrmDate));
				switch($weekday)
				{
					case 'Saturday':
						$color = '#F9FABE';
					break;
					
					case 'Sunday':
						$color = '#FAD9AD';
					break;
					
					default:
						$color = '#BFF9C2';
					break;
				}
				$holidayArr	= $obj_planningBoard_get->getCompanyHoliday($newFrmDate,'RunQuery');
				if($holidayArr['intDayType']!='' && $holidayArr['intDayType']==0 )
					$color = '#F08080';
					
				$planQty	= $obj_planningBoard_get->getPlanQty($newFrmDate,$array_loop['typeId'],$rowH['planId'],'RunQuery');
				if($array_loop['type']=='STROCK_PLAN')
					$disStatus	= 1;
				else
					$disStatus	= 0;
				
				if($printType!=2 && $array_loop['type']=='GROUP_ALLOCATION')
				{
					$html .="<td style=\"text-align:center\" bgcolor=\"$color\">&nbsp;</td>";
				}
				else
				{
					$html .= "<td style=\"text-align:center\" bgcolor=\"$color\"><input name=\"txtPlan\" id=\"".$newFrmDate."\" type=\"text\" class=\"clsPlanQty ".$array_loop['type'].$newFrmDate." clsPlanQty".$newFrmDate."\" style=\"width:100%;text-align:right\" ".($disStatus==0?"":"disabled=\"disabled\"")." value=\"".$planQty."\" title=\"date             : ".$newFrmDate."\n"."Graphic       : ".$graphicTitle."\n"."sales Order : ".$salesOrderTitle."\n"."Style            : ".$styleTitle."\" /></td>";
				}
				
				$date = date_create($newFrmDate);
				date_add($date, date_interval_create_from_date_string('1 days'));
				$nextDt = date_format($date, 'Y-m-d');
				$newFrmDate = $nextDt;
			}
			
			$customer		= '&nbsp;';
			$brand			= '&nbsp;';
			$plateSize		= '&nbsp;';
			$style			= '&nbsp;';
			$graphic		= '&nbsp;';
			$delImg			= "&nbsp;";
			$editImg		= "&nbsp;";
			$salesOrderNo 	= '&nbsp;';
			$noOfColors 	= '&nbsp;';
			$noOfStocks 	= '&nbsp;';
			$noOfPanels 	= '&nbsp;';
			$POQty 			= '&nbsp;';
		
			$html .= "</tr>";	
			$i++;
		}	
	}
	$response['header']			= $headerHtml;
	$response['detail']			= $html;
	$response['dateDetail']		= $dateHtml;
	
	echo json_encode($response);
}
else if($requestType=='deletePlan')
{
	$planId		= $_REQUEST['planId'];
	$db->begin();
	
	validateBeforeDelete($planId);
	
	$resultArr	= $obj_planningBoard_set->delete($planId);
	if($resultArr['savedStatus']=='fail' && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged	= $resultArr['savedMassege'];
		$error_sql		= $resultArr['error_sql'];
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);	
}
else if($requestType=='saveData')
{
	$arrDetails 	= json_decode($_REQUEST['arrDetails'],true);
	$db->begin();
	
	/*if($saveMode==0 && ($savedStatus))
	{
		$savedStatus 	= false;
		$savedMasseged 	= 'No permission to save.';
	}*/
	
	foreach($arrDetails as $array_loop)
	{
		$planId		= $array_loop['planId'];
		$date		= $array_loop['date'];
		$typeId		= $array_loop['typeId'];
		$qty		= $array_loop['Qty'];
		
		$resultDel	= $obj_planningBoard_set->deletePlanDetail($planId,$date,$typeId);
		if($resultDel['savedStatus']=='fail' && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged	= $resultDel['savedMassege'];
			$error_sql		= $resultDel['error_sql'];
		}
		if($qty!='' && $qty!=0)
		{
			$resultIns	= $obj_planningBoard_set->savePlan($planId,$date,$typeId,$qty);
			if($resultIns['savedStatus']=='fail' && ($savedStatus))
			{
				$savedStatus	= false;
				$savedMasseged	= $resultIns['savedMassege'];
				$error_sql		= $resultIns['error_sql'];
			}
		}
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Saved successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);
}
else if($requestType=="loadAllComboes")
{
	$slected 			= $_REQUEST['slected'];
	$orderYear 			= $_REQUEST['orderYear'];
	$orderNo 			= $_REQUEST['orderNo'];
	$customerId			= $_REQUEST['customerId'];
	
	$html				= $obj_planningBoard_get->loadAllComboes($slected,$orderYear,$orderNo,$customerId);
	
	$response['combo']	= $html;
	echo json_encode($response);
}
else if($requestType=="manualOrderAllocationSave")
{
	$arrHeader 	= json_decode($_REQUEST['arrHeader'],true);
	$db->begin();
	
	$planId				= $arrHeader["planId"];
	$orderYear			= $arrHeader["orderYear"];
	$orderNo			= $arrHeader["orderNo"];
	$salesOrderId		= $arrHeader["salesOrderId"];
	
	$resultUpd			= $obj_planningBoard_set->saveManualOrderAllocation($planId,$orderYear,$orderNo,$salesOrderId);
	if($resultUpd['savedStatus']=='fail' && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged	= $resultDel['savedMassege'];
		$error_sql		= $resultDel['error_sql'];
	}
	$orderDetails		= $obj_planningBoard_get->allocateOrderDetails($orderYear,$orderNo,$salesOrderId);
	
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['salesOrder']	= $orderDetails['strSalesOrderNo'];
		$response['style']		= $orderDetails['strStyleNo'];
		$response['graphic']	= $orderDetails['strGraphicNo'];
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);
}
else if($requestType=='loadOrderYear')
{
	$customerId 				= $_REQUEST['customerId'];
	$response['orderCombo'] 	= $obj_planningBoard_get->getMOAOrderYear($customerId);
	
	echo json_encode($response);
}
function validateBeforeDelete($planId)
{
	global $db;
	global $obj_common;
	global $savedStatus;
	global $savedMasseged;
	global $deleteMode;
	
	if($deleteMode==0 && ($savedStatus))
	{
		$savedStatus 	= false;
		$savedMasseged 	= 'No permission to delete.';
	}
}
function getHeaderHTML($fromDate,$toDate,$customer,$planLocation,$salesOrder,$style,$graphic,$printType)
{
	global $db;
	global $obj_planningBoard_get;
	
	$html 	 = "";
	$html	.= "<tr class=\"normalfnt\">
					<td colspan=\"12\" bgcolor=\"#FFFFFF\" style=\"text-align:left\">Avilable Strokes</td>";	
	
	$start 			= strtotime($fromDate);
	$end 			= strtotime($toDate);
	$days_between 	= ceil(abs($end - $start) / 86400);
	$newFrmDate 	= $fromDate;
	
	for($t=0;$t<=$days_between;$t++)
	{
		$month 		= date("m",strtotime($newFrmDate));
		$year 		= date("Y",strtotime($newFrmDate));
		
		$avilableStrocks = $obj_planningBoard_get->getAvilableStrocks($year,$month,$planLocation,$printType);
		
		$html	.= "<td style=\"text-align:right\" class=\"normalfnt2BI clsAilableStrocks".$newFrmDate."\">".($avilableStrocks==''?'0':$avilableStrocks)."</td>";
		$date 	 = date_create($newFrmDate);
		date_add($date, date_interval_create_from_date_string('1 days'));
		$nextDt  = date_format($date, 'Y-m-d');
		$newFrmDate = $nextDt;
	}
	$html.="</tr>";
	
	$html	.= "<tr class=\"normalfnt\">
					<td colspan=\"12\" bgcolor=\"#FFFFFF\" style=\"text-align:left\">Avilable Groups</td>";	
	
	$start 			= strtotime($fromDate);
	$end 			= strtotime($toDate);
	$days_between 	= ceil(abs($end - $start) / 86400);
	$newFrmDate 	= $fromDate;
	
	for($t=0;$t<=$days_between;$t++)
	{
		$month 		= date("m",strtotime($newFrmDate));
		$year 		= date("Y",strtotime($newFrmDate));
		
		$avilableGroups = $obj_planningBoard_get->getAvilableGroups($year,$month,$planLocation,$printType);
		
		$html	.= "<td style=\"text-align:right\" class=\"normalfnt2BI clsAilableGroups".$newFrmDate."\">".($avilableGroups==''?'0':$avilableGroups)."</td>";
		$date = date_create($newFrmDate);
		date_add($date, date_interval_create_from_date_string('1 days'));
		$nextDt = date_format($date, 'Y-m-d');
		$newFrmDate = $nextDt;
	}
	$html.="</tr>";
	
	$html	.= "<tr class=\"normalfnt\">
					<td colspan=\"12\" bgcolor=\"#FFFFFF\" style=\"text-align:left\">Total No of Strokes - Plan</td>";	
	
	$start 			= strtotime($fromDate);
	$end 			= strtotime($toDate);
	$days_between 	= ceil(abs($end - $start) / 86400);
	$newFrmDate 	= $fromDate;
	
	for($t=0;$t<=$days_between;$t++)
	{
		$getTotStrocks	= $obj_planningBoard_get->getTotalStrocks($newFrmDate,$customer,$planLocation,$salesOrder,$style,$graphic,$printType);
		
		$html	.= "<td style=\"text-align:right\" class=\"normalfnt2BI clsTotStocks".$newFrmDate."\">".$getTotStrocks."</td>";
		$date = date_create($newFrmDate);
		date_add($date, date_interval_create_from_date_string('1 days'));
		$nextDt = date_format($date, 'Y-m-d');
		$newFrmDate = $nextDt;
	}
	$html.="</tr>";
	
	$html	.= "<tr class=\"normalfnt\">
					<td colspan=\"12\" bgcolor=\"#FFFFFF\" style=\"text-align:left\">Total No of Groups - Plan</td>";	
	
	$start 			= strtotime($fromDate);
	$end 			= strtotime($toDate);
	$days_between 	= ceil(abs($end - $start) / 86400);
	$newFrmDate 	= $fromDate;
	
	for($t=0;$t<=$days_between;$t++)
	{
		$getTotGroups	= $obj_planningBoard_get->getTotalGroups($newFrmDate,$customer,$planLocation,$salesOrder,$style,$graphic,$printType);
		
		$html	.= "<td style=\"text-align:right\" class=\"normalfnt2BI clsTotGroups".$newFrmDate."\">".$getTotGroups."</td>";
		$date = date_create($newFrmDate);
		date_add($date, date_interval_create_from_date_string('1 days'));
		$nextDt = date_format($date, 'Y-m-d');
		$newFrmDate = $nextDt;
	}
	$html.="</tr>";
	
	$html	.= "<tr class=\"normalfnt\">
					<td colspan=\"12\" bgcolor=\"#FFFFFF\" style=\"text-align:left\">Utilized Strokes % (Plan vs Avilable)</td>";	
	
	$start 			= strtotime($fromDate);
	$end 			= strtotime($toDate);
	$days_between 	= ceil(abs($end - $start) / 86400);
	$newFrmDate 	= $fromDate;
	
	for($t=0;$t<=$days_between;$t++)
	{
		$month 		= date("m",strtotime($newFrmDate));
		$year 		= date("Y",strtotime($newFrmDate));
		
		$availableStrocks 	= $obj_planningBoard_get->getAvilableStrocks($year,$month,$planLocation,$printType);
		$getTotStrocks		= $obj_planningBoard_get->getTotalStrocks($newFrmDate,$customer,$planLocation,$salesOrder,$style,$graphic,$printType);
		$utilizedStrocks	= round(($getTotStrocks/$availableStrocks*100),1);
		
		$html	.= "<td style=\"text-align:right\" class=\"normalfnt2BI clsUtilizedStrocks".$newFrmDate."\">".$utilizedStrocks."%</td>";
		$date = date_create($newFrmDate);
		date_add($date, date_interval_create_from_date_string('1 days'));
		$nextDt = date_format($date, 'Y-m-d');
		$newFrmDate = $nextDt;
	}
	$html.="</tr>";
	
	$html	.= "<tr class=\"normalfnt\">
					<td colspan=\"12\" bgcolor=\"#FFFFFF\" style=\"text-align:left\">Utilized Groups % (Plan vs Avilable)</td>";	
	
	$start 			= strtotime($fromDate);
	$end 			= strtotime($toDate);
	$days_between 	= ceil(abs($end - $start) / 86400);
	$newFrmDate 	= $fromDate;
	
	for($t=0;$t<=$days_between;$t++)
	{
		$month 		= date("m",strtotime($newFrmDate));
		$year 		= date("Y",strtotime($newFrmDate));
		
		$avilableGroups = $obj_planningBoard_get->getAvilableGroups($year,$month,$planLocation,$printType);
		
		$getTotGroups		= $obj_planningBoard_get->getTotalGroups($newFrmDate,$customer,$planLocation,$salesOrder,$style,$graphic,$printType);
		$utilizedGroups		= round(($getTotGroups/$availableGroups*100),1);
		
		$html	.= "<td style=\"text-align:right\" class=\"normalfnt2BI clsUtilizedGroups".$newFrmDate."\">".$utilizedGroups."%</td>";
		$date = date_create($newFrmDate);
		date_add($date, date_interval_create_from_date_string('1 days'));
		$nextDt = date_format($date, 'Y-m-d');
		$newFrmDate = $nextDt;
	}
	$html.="</tr>";
	
	$html	.= "<tr class=\"normalfnt\">
					<td colspan=\"12\" bgcolor=\"#FFFFFF\" style=\"text-align:left\">Utilized Groups % (Plan vs Avilable)</td>";	
	
	$start 			= strtotime($fromDate);
	$end 			= strtotime($toDate);
	$days_between 	= ceil(abs($end - $start) / 86400);
	$newFrmDate 	= $fromDate;
	
	for($t=0;$t<=$days_between;$t++)
	{
		$getTotStrocks		= $obj_planningBoard_get->getTotalStrocks($newFrmDate,$customer,$planLocation,$salesOrder,$style,$graphic,$printType);
		
		$html	.= "<td style=\"text-align:right\" class=\"normalfnt2BI clsDailyPlan".$newFrmDate."\">".$getTotStrocks."</td>";
		$date = date_create($newFrmDate);
		date_add($date, date_interval_create_from_date_string('1 days'));
		$nextDt = date_format($date, 'Y-m-d');
		$newFrmDate = $nextDt;
	}
	$html.="</tr>";
	
	return $html;
}
function getDateHTML($fromDate,$toDate)
{
	global $obj_planningBoard_get;
	
	$html 	 = "";
	$html	.="<tr class=\"normalfnt\">
                	<td width=\"10\" bgcolor=\"#999999\" style=\"text-align:center\">Del</td>
					<td width=\"14\" bgcolor=\"#999999\" style=\"text-align:center\">Edit</td>
                    <td bgcolor=\"#999999\" style=\"text-align:center\">Customer</td>
                    <td nowrap=\"nowrap\" bgcolor=\"#999999\" style=\"text-align:center\">Sales Order</td>
                    <td bgcolor=\"#999999\" style=\"text-align:center\">Style</td>
                    <td bgcolor=\"#999999\" style=\"text-align:center\">Graphic</td>
					<td nowrap=\"nowrap\" bgcolor=\"#999999\" style=\"text-align:center\">Plate Size</td>
                    <td bgcolor=\"#999999\" style=\"text-align:center\">colors</td>
                    <td bgcolor=\"#999999\" style=\"text-align:center\">Strokes</td>
                    <td nowrap=\"nowrap\" bgcolor=\"#999999\" style=\"text-align:center\">No of Panels</td>
                    <td nowrap=\"nowrap\" bgcolor=\"#999999\" style=\"text-align:center\">PO Qty</td>
                    <td bgcolor=\"#999999\" style=\"text-align:center\">Type</td> ";
                    
                  
	$start 			= strtotime($fromDate);
	$end 			= strtotime($toDate);
	$days_between 	= ceil(abs($end - $start) / 86400);
	$newFrmDate 	= $fromDate;
	$color			= '';
	for($t=0;$t<=$days_between;$t++)
	{
		$weekday 		= date('l', strtotime($newFrmDate));			
		switch($weekday)
		{
			case 'Saturday':
				$color = '#F9FABE';
			break;
			
			case 'Sunday':
				$color = '#FAD9AD';
			break;
			
			default:
				$color = '#BFF9C2';
			break;
		}
   		$holidayArr	= $obj_planningBoard_get->getCompanyHoliday($newFrmDate,'RunQuery');
		if($holidayArr['intDayType']!='' && $holidayArr['intDayType']==0 )
			$color = '#F08080';
			
		$html	.="<td nowrap=\"nowrap\" style=\"text-align:center\" bgcolor=\"$color\" class=\"normalfnt2BI\">&nbsp;$newFrmDate&nbsp;</td>";
	
		$date = date_create($newFrmDate);
		date_add($date, date_interval_create_from_date_string('1 days'));
		$nextDt = date_format($date, 'Y-m-d');
		$newFrmDate = $nextDt;
	}
	$html	.="</tr>";
	return $html;
}
function getSavedOrderHTML($planId,$arrHeader,$arrType)
{
	global $obj_planningBoard_get;
	global $obj_common;
	global $userId;
	global $deleteMode;
	
	$fromDate		= $arrHeader["fromDate"];
	$toDate			= $arrHeader["toDate"];
	$customer		= $arrHeader["customer"];
	$planLocation	= $arrHeader["planLocation"];
	$salesOrder		= $arrHeader["salesOrder"];
	$style			= $arrHeader["style"];
	$graphic		= $arrHeader["graphic"];
	$printType		= $arrHeader["printType"];
	
	$html			= "";

	$header_result	= $obj_planningBoard_get->getHeaderData($customer,$planLocation,$salesOrder,$style,$graphic,$printType,$planId,'RunQuery2');
	while($rowH = mysqli_fetch_array($header_result))
	{
		$customer		= ($rowH['customerName']==''?'&nbsp;':$rowH['customerName']);
		$brand			= ($rowH['brand']==''?'&nbsp;':$rowH['brand']);
		$plateSize		= ($rowH['plateSizeName']==''?'&nbsp;':$rowH['plateSizeName']);
		$plateSizeId	= $rowH['plateSizeId'];
		$targetStrocks	= $rowH['targetStrocks'];
		$style			= ($rowH['styleNo']==''?'&nbsp;':$rowH['styleNo']);
		$graphic		= ($rowH['graphicNo']==''?'&nbsp;':$rowH['graphicNo']);
		$salesOrderNo 	= ($rowH['salesOrderNo']==''?'&nbsp;':$rowH['salesOrderNo']);
		$noOfColors 	= ($rowH['noOfColors']==''?'&nbsp;':$rowH['noOfColors']);
		$noOfStocks 	= ($rowH['noOfStrocks']==''?'&nbsp;':$rowH['noOfStrocks']);
		$noOfPanels 	= ($rowH['noOfPanels']==''?'&nbsp;':$rowH['noOfPanels']);
		$POQty 			= ($rowH['POQty']==''?'&nbsp;':$rowH['POQty']);
		$MStatus 		= $rowH['manualOrderStatus'];
		$customerId 	= $rowH['customerId'];
		$delImg			= "<img border=\"0\" src=\"../../../../images/del.png\" name=\"butDelPlan\" class=\"mouseover clsDelPlan\" id=\"butDelPlan\"/>";
		$editImg		= "<img border=\"0\" src=\"../../../../images/edit.png\" name=\"butEditPlan\" class=\"mouseover clsEditPlan\" id=\"butEditPlan\"/>";
		$graphicTitle		= $graphic;
		$salesOrderTitle	= $salesOrderNo;
		$styleTitle			= $style;
		$marketer			= ($rowH['marketer']==''?'&nbsp;':$rowH['marketer']);
		$i					= 0;
		
		foreach($arrType as $array_loop)
		{
			$planTypeViewMode = $obj_common->ValidateSpecialPermission($array_loop['permissionId'],$userId,'RunQuery2');			
			$html .= "<tr class=\"normalfnt2BI clsPlan".$rowH['planId']."\" id=\"".$rowH['planId']."\">
						<td style=\"text-align:center\">".($deleteMode==1?$delImg:"&nbsp;")."</td>
						<td style=\"text-align:center\">".($MStatus==1?$editImg:"&nbsp;")."</td>";
			if($i==1)
				$html .="<td id=\"".$customerId."\" style=\"text-align:left\" nowrap=\"nowrap\" class=\"clsCustomer\">Marketer : ".$marketer."</td>";
			else
				$html .="<td id=\"".$customerId."\" style=\"text-align:left\" nowrap=\"nowrap\" class=\"clsCustomer\">".$customer."</td>";	
						
				$html .="<td style=\"text-align:left\" nowrap=\"nowrap\" class=\"clsSalesOrderNo\">".$salesOrderNo."</td>
						<td style=\"text-align:left\" nowrap=\"nowrap\" class=\"clsStyle\">".$style."</td>
						<td style=\"text-align:left\" nowrap=\"nowrap\" class=\"clsGraphis mouseover\">".$graphic."</td>
						<td id=\"".$targetStrocks."\" style=\"text-align:left\" nowrap=\"nowrap\" class=\"clsPlateSize\">".$plateSize."</td>
						<td style=\"text-align:right\" nowrap=\"nowrap\" class=\"clsNoOfColors\">".$noOfColors."</td>
						<td style=\"text-align:right\" nowrap=\"nowrap\" class=\"clsNoOfStrocks\">".$noOfStocks."</td>
						<td style=\"text-align:right\" nowrap=\"nowrap\" class=\"clsNoOfPanels\">".$noOfPanels."</td>
						<td style=\"text-align:right\" nowrap=\"nowrap\" class=\"clsPOQty\">".$POQty."</td>
						<td id=\"".$array_loop['typeId']."\" style=\"text-align:left\" nowrap=\"nowrap\" class=\"clsType\">".$array_loop['typeName']."</td>
					";
			$start 			= strtotime($fromDate);
			$end 			= strtotime($toDate);
			$days_between 	= ceil(abs($end - $start) / 86400);
			$newFrmDate 	= $fromDate;
			$color			= '';
			$disStatus		= '';
			for($t=0;$t<=$days_between;$t++)
			{
				$weekday = date('l', strtotime($newFrmDate));
				switch($weekday)
				{
					case 'Saturday':
						$color = '#F9FABE';
					break;
					
					case 'Sunday':
						$color = '#FAD9AD';
					break;
					
					default:
						$color = '#BFF9C2';
					break;
				}
				$holidayArr	= $obj_planningBoard_get->getCompanyHoliday($newFrmDate,'RunQuery2');
				if($holidayArr['intDayType']!='' && $holidayArr['intDayType']==0 )
					$color = '#F08080';
					
				$planQty	= $obj_planningBoard_get->getPlanQty($newFrmDate,$array_loop['typeId'],$rowH['planId'],'RunQuery2');
				if($array_loop['type']=='STROCK_PLAN')
					$disStatus	= 1;
				else
					$disStatus	= 0;
				
				if($printType==1 && $array_loop['type']=='GROUP_ALLOCATION')
				{
					$html .="<td style=\"text-align:center\" bgcolor=\"$color\">&nbsp;</td>";
				}
				else
				{
					$html .= "<td style=\"text-align:center\" bgcolor=\"$color\"><input name=\"txtPlan\" id=\"".$newFrmDate."\" type=\"text\" class=\"clsPlanQty ".$array_loop['type'].$newFrmDate." clsPlanQty".$newFrmDate."\" style=\"width:100%;text-align:right\" ".($disStatus==0?"":"disabled=\"disabled\"")." value=\"".$planQty."\" title=\"date             : ".$newFrmDate."\n"."Graphic       : ".$graphicTitle."\n"."sales Order : ".$salesOrderTitle."\n"."Style            : ".$styleTitle."\" /></td>";
				}
				
				$date = date_create($newFrmDate);
				date_add($date, date_interval_create_from_date_string('1 days'));
				$nextDt = date_format($date, 'Y-m-d');
				$newFrmDate = $nextDt;
			}
			
			$customer		= '&nbsp;';
			$brand			= '&nbsp;';
			$plateSize		= '&nbsp;';
			$style			= '&nbsp;';
			$graphic		= '&nbsp;';
			$delImg			= "&nbsp;";
			$editImg		= "&nbsp;";
			$salesOrderNo 	= '&nbsp;';
			$noOfColors 	= '&nbsp;';
			$noOfStocks 	= '&nbsp;';
			$noOfPanels 	= '&nbsp;';
			$POQty 			= '&nbsp;';
		
			$html .= "</tr>";
			$i++;	
		}	
	}
	return $html;
	
}
?>
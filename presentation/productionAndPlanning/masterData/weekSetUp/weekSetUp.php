<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];
$dateOneMonthAdded = date("Y-m-d", strtotime("+1 month") );
$lastMonth		   = date("Y-m-d", strtotime("-1 month") ) ;

include  "{$backwardseperator}dataAccess/permisionCheck.inc";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Week Setup</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
<script type="application/javascript" src="weekSetup-js.js"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">
</head>

<body>
<form id="frmWeekSetup" name="frmWeekSetup" autocomplete="off">
<table width="100%" border="0" align="center">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div align="center">
<div class="trans_layoutD">
<div class="trans_text">Week Setup</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
    	<td>
        	<table width="100%" align="center" bgcolor="#FFFFFF">
            <tr>
              <td width="14%" class="normalfnt">&nbsp;Year</td>
              <td width="23%" class="normalfnt"><select name="cboYear" id="cboYear" class="validate[required]" style="width:100px">
              <option value=""></option>
                <?php
		$sql ="SELECT DISTINCT intYear FROM plan_calender";
		$result = $db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			echo "<option value=\"".$row["intYear"]."\" >".$row["intYear"]."</option>";
		}
	  ?>
              </select></td>
              <td width="14%" class="normalfnt">&nbsp;Month</td>
              <td width="49%" class="normalfnt"><select name="cboMonth" id="cboMonth" class="validate[required]" style="width:100px" disabled="disabled">
               <option value=""></option>
                <?php
				$sql ="SELECT intMonthId,strMonth FROM mst_month ORDER BY intMonthId";
				$result = $db->RunQuery($sql);
				while($row = mysqli_fetch_array($result))
				{
					echo "<option value=\"".$row["intMonthId"]."\" >".$row["strMonth"]."</option>";
				}
			  ?>
              </select></td>
            </tr>
            <tr>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
            </tr>
            <tr>
            	<td colspan="4">
                <table width="100%" class="bordered" id="tblSizesPopup" >
                    <thead>
                     <tr>
                       <th width="40%" height="22" >Week</th>
                       <th width="30%" height="22" >From</th>
                       <th width="30%" height="22" >To</th>
                     </tr>
                    </thead>
                    <tbody id="gridBody">
                    	<tr class="normalfnt" id="1">
                        	<td align="center" bgcolor="#FFFFFF" class="clsWeek" >1st Week</td>
                            <td align="center" bgcolor="#FFFFFF" class="clsFrom" ><input name="txtFrmDate" id="txtFrmDate" type="text" style="width:50px" class="validate[required,custom[onlyNumberSp]] fromDate" /></td>
                            <td align="center" bgcolor="#FFFFFF" class="clsTo" ><input name="txtToDate" id="txtToDate" type="text" style="width:50px" class="validate[required,custom[onlyNumberSp]] toDate" /></td>
                        </tr>
                        <tr class="normalfnt" id="2">
                        	<td align="center" bgcolor="#FFFFFF" class="clsWeek" >2nd Week</td>
                            <td align="center" bgcolor="#FFFFFF" class="clsFrom" ><input name="txtFrmDate2" id="txtFrmDate2" type="text" style="width:50px" class="validate[required,custom[onlyNumberSp]] fromDate" /></td>
                            <td align="center" bgcolor="#FFFFFF" class="clsTo" ><input name="txtToDate2" id="txtToDate2" type="text" style="width:50px" class="validate[required,custom[onlyNumberSp]] toDate" /></td>
                        </tr>
                        <tr class="normalfnt" id="3">
                        	<td align="center" bgcolor="#FFFFFF" class="clsWeek" >3rd Week</td>
                            <td align="center" bgcolor="#FFFFFF" class="clsFrom" ><input name="txtFrmDate3" id="txtFrmDate3" type="text" style="width:50px" class="validate[required,custom[onlyNumberSp]] fromDate" /></td>
                            <td align="center" bgcolor="#FFFFFF" class="clsTo" ><input name="txtToDate3" id="txtToDate3" type="text" style="width:50px" class="validate[required,custom[onlyNumberSp]] toDate" /></td>
                        </tr>
                        <tr class="normalfnt" id="4">
                        	<td align="center" bgcolor="#FFFFFF" class="clsWeek" >4th Week</td>
                            <td align="center" bgcolor="#FFFFFF" class="clsFrom" ><input name="txtFrmDate4" id="txtFrmDate4" type="text" style="width:50px" class="validate[required,custom[onlyNumberSp]] fromDate" /></td>
                            <td align="center" bgcolor="#FFFFFF" class="clsTo" ><input name="txtToDate4" id="txtToDate4" type="text" style="width:50px" class="validate[required,custom[onlyNumberSp]] toDate" /></td>
                        </tr>
                        <tr class="normalfnt" id="5">
                          <td align="center" bgcolor="#FFFFFF" class="clsWeek" >5th Week</td>
                          <td align="center" bgcolor="#FFFFFF" class="clsFrom" ><input name="txtFrmDate5" id="txtFrmDate5" type="text" style="width:50px" class="validate[required,custom[onlyNumberSp]] fromDate" /></td>
                          <td align="center" bgcolor="#FFFFFF" class="clsTo" ><input name="txtToDate5" id="txtToDate5" type="text" style="width:50px" class="validate[required,custom[onlyNumberSp]] toDate" /></td>
                        </tr>
                    </tbody>
                    </table>
                </td>
           	  </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td height="40">
    		<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><img border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img border="0" src="../../../../images/Tsave.jpg" alt="Save" name="butSave" width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
              </tr>
            </table>
        </td>
    </tr>
</table>
</div>
</div>
</form>
</body>
</html>

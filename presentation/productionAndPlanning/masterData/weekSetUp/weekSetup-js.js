// JavaScript Document
$(document).ready(function(){

	$("#frmWeekSetup").validationEngine();
	
	$("#frmWeekSetup #cboYear").live('change',function(){
		
		if($("#cboYear").val()=="")
		{
			$("#cboMonth").val("");
			$("#cboMonth").attr("disabled", "disabled"); 
			
			$('.fromDate').each(function(){
						
				$(this).val("");
				$(this).parent().parent().find('.toDate').val("");	
			});
		}
		else
			$("#cboMonth").removeAttr("disabled"); 
			$("#cboMonth").val("");
			$('.fromDate').each(function(){
						
				$(this).val("");
				$(this).parent().parent().find('.toDate').val("");	
			});
	});
	$("#frmWeekSetup #cboMonth").live('change',function(){
		
		var year  = $('#cboYear').val();
		var month = $(this).val();
		
		var url     = "weekSetup-db-get.php?requestType=loadData";
		$.ajax({
				url:url,
				async:false,
				dataType:'json',
				type:'post',
				data:'&year='+year+'&month='+month,
		success:function(json){
				if(json.status==1)
				{
					$('#frmWeekSetup #gridBody').html(json.html);
					return;
				}
				else
				{
					$('.fromDate').each(function(){
						
						$(this).val("");
						$(this).parent().parent().find('.toDate').val("");	
					});
				}
			},
		error:function(){
				return;
			}
		});
		
		
	});
	$("#frmWeekSetup #butSave").live('click',function(){
		
		if ($('#frmWeekSetup').validationEngine('validate'))
		{
			var year  = $('#frmWeekSetup #cboYear').val();
			var month = $('#frmWeekSetup #cboMonth').val();
			
			var value="[ ";
			$('.fromDate').each(function(){
				
				var weekId   	= $(this).parent().parent().attr("id");
				var fromDay 	= $(this).val();
				var toDay 		= $(this).parent().parent().find('.toDate').val();

				value +='{"weekId":"'+weekId+'","fromDay":"'+fromDay+'","toDay":"'+toDay+'"},' ;
				
			});
			
			value = value.substr(0,value.length-1);
			value += " ]";
			
			var url = "weekSetup-db-get.php?requestType=saveData";
					$.ajax({
							url:url,
							async:false,
							dataType:'json',
							type:'post',
							data:'&year='+year+'&month='+month+'&weekDetails='+value,
						success:function(json){
							$('#frmWeekSetup #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							if(json.type=='pass')
							{
								var t=setTimeout("alertx()",1000);
								setTimeout("window.location.href=window.location.href;",1000)
								return;
							}
						},
						error:function(){
							
							$('#frmWeekSetup #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",3000);						
						}
					});
		}
		else
		{
			var t=setTimeout("alertAll()",1000);
		}
		
	});
	$('#butNew').live('click',function(){
		window.location.href = window.location.href;
		return;
	});
});
function alertAll()
{
	$('#frmWeekSetup').validationEngine('hide')	;
}
function alertx()
{
	$('#frmWeekSetup #butSave').validationEngine('hide')	;
}
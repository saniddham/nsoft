<?php 
session_start();
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 	 		= $_SESSION['userId'];

$requestType 		= $_REQUEST['requestType'];
$weekDetails 		= json_decode($_REQUEST['weekDetails'], true);

include "{$backwardseperator}dataAccess/Connector.php";

if($requestType=="saveData")
{
	$year 		= $_REQUEST['year'];
	$month 		= $_REQUEST['month'];
	
	$sqlCheck = "SELECT * FROM plan_week_setup WHERE intyear='$year' AND intMonth='$month' ";
	$result = $db->RunQuery($sqlCheck);
	$count  = mysqli_num_rows($result);
	if($count>0)
	{
		$status = 'update';
	}
	else
	{
		$status = 'save';
	}
	foreach($weekDetails as $arr)
	{
		$weekId 		= $arr['weekId'];
		$frmDay  		= $arr['fromDay'];
		$toDay  		= $arr['toDay'];
		
		$fromDate       = date($year.'-'.$month.'-'.$frmDay) ;
		$toDate       	= date($year.'-'.$month.'-'.$toDay) ;
		
		if($status=='update')
		{
			$sql = "UPDATE plan_week_setup 
					SET
					intFrmDay = $frmDay , 
					intToDay = $toDay , 
					dtmFromDate = '$fromDate' , 
					dtmToDate = '$toDate'  
					WHERE
					intyear = '$year' AND
					intMonth = '$month' AND
					intWeekId = '$weekId' " ;
			
		}
		elseif($status=='save')
		{
			$sql = " INSERT INTO plan_week_setup 
					( 
						intyear, 
						intMonth, 
						intWeekId, 
						intFrmDay, 
						intToDay, 
						dtmFromDate, 
						dtmToDate, 
						intCreateBy, 
						dtmCreateDate
					)
					VALUES
					( 
						'$year', 
						'$month', 
						$weekId, 
						$frmDay, 
						$toDay, 
						'$fromDate', 
						'$toDate', 
						'$userId', 
						now()
					)";
		}
		$finalResult = $db->RunQuery($sql);
	}
	if($finalResult)
	{
		$response['type'] 		= 'pass';
		$response['msg'] 		= 'Saved successfully';
	}
	else
	{
		$response['type'] 		= 'fail';
		$response['msg'] 		= $db->errormsg;
	}
	echo json_encode($response);
		
}
elseif($requestType=="loadData")
{
	$year 		= $_REQUEST['year'];
	$month 		= $_REQUEST['month'];
	
	$sql = "SELECT * FROM plan_week_setup WHERE intyear='$year' AND intMonth='$month'";
	$result = $db->RunQuery($sql);
	$html = '';
	$validate = 0;
	while($row=mysqli_fetch_array($result))
	{
		$validate = 1;
		$weekId = $row['intWeekId'];
		
		switch($weekId)
		{
			case '1' :
			{
				$Wstatus="st";
				break;
			}
			case '2' :
			{
				$Wstatus="nd";
				break;
			}
			case '3' :
			{
				$Wstatus="rd";
				break;
			}
			case '4' :
			{
				$Wstatus="th";
				break;
			}
			case '5' :
			{
				$Wstatus="th";
				break;
			}
		}
		
		$html.="<tr class=\"normalfnt\" id=\"".$row['intWeekId']."\">
                <td align=\"center\" bgcolor=\"#FFFFFF\" class=\"clsWeek\" >".$weekId.$Wstatus." Week</td>
                <td align=\"center\" bgcolor=\"#FFFFFF\" class=\"clsFrom\" ><input name=\"txtFrmDate\" id=\"txtFrmDate\" type=\"text\" style=\"width:50px\" class=\"validate[required,custom[onlyNumberSp]] fromDate\" value=\"".$row['intFrmDay']."\" /></td>
                <td align=\"center\" bgcolor=\"#FFFFFF\" class=\"clsTo\" ><input name=\"txtToDate\" id=\"txtFrmDate\" type=\"text\" style=\"width:50px\" class=\"validate[required,custom[onlyNumberSp]] toDate\" value=\"".$row['intToDay']."\"  /></td>
                </tr>";
	}
	
	$response['status'] = $validate;
	$response['html']	= $html;		
	
	echo json_encode($response);
}
?>
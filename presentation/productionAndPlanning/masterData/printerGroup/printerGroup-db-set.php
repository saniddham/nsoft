<?php 
	session_start();
	$backwardseperator 	= "../../../../";
	$mainPath 			= $_SESSION['mainPath'];
	$userId 	 		= $_SESSION['userId'];
	$requestType 		= $_REQUEST['requestType'];
	$printerDetails 	= json_decode($_REQUEST['printerDetails'], true);
	
	include "{$backwardseperator}dataAccess/Connector.php";
	
	if($requestType=='saveData')
	{
		$groupId 		= $_REQUEST['cboSearchGroup'];
		$groupName 		= $_REQUEST['txtGroupName'];
		$companyId 		= $_REQUEST['cboNewCompany'];
		$locationId 	= $_REQUEST['cboLocation'];
		$date 			= $_REQUEST['dtDate'];
		$plantId 		= $_REQUEST['cboMachinePlant'];
		$teamId 		= $_REQUEST['cboTeam'];
		
		if($groupId=="")
		{
			$sqlCheck = "SELECT * FROM plan_printer_group WHERE strGroupName LIKE '$groupName' ";
			$resultChk = $db->RunQuery($sqlCheck);
			$rowChk = mysqli_fetch_array($resultChk);
			if(mysqli_num_rows($resultChk)>0)
			{
				$response['type'] 		= 'found';
				$response['msg'] 		= 'Group Name already exists.';
				
				
				echo json_encode($response);
				return;
			}
			
			$sql = "INSERT INTO plan_printer_group 
					(
						strGroupName, 
						dtmAllocatedDate, 
						intPlantId, 
						intTeamId, 
						intCompanyId, 
						intLocationId, 
						intStatus, 
						dtmCreateDate, 
						dtmCreatedBy
					)
					VALUES
					(
						'$groupName', 
						'$date', 
						'$plantId', 
						'$teamId', 
						'$companyId', 
						'$locationId', 
						'1', 
						now(), 
						'$userId'
					)";
			$finalResult = $db->RunQuery($sql);
			$newGroupId = $db->insertId;
		}
		else
		{
			$sqlCheck = "SELECT * FROM plan_printer_group WHERE strGroupName LIKE '$groupName' ";
			$resultChk = $db->RunQuery($sqlCheck);
			$rowChk = mysqli_fetch_array($resultChk);
			if(mysqli_num_rows($resultChk)>0 && $rowChk['intGroupId']!=$groupId)
			{
				$response['type'] 		= 'found';
				$response['msg'] 		= 'Group Name already exists.';
				
				
				echo json_encode($response);
				return;
			}
			$sqlUpd = " UPDATE plan_printer_group 
						SET
						strGroupName = '$groupName' , 
						dtmAllocatedDate = '$date' , 
						intPlantId = '$plantId' , 
						intTeamId = '$teamId' , 
						intCompanyId = '$companyId' , 
						intLocationId = '$locationId' , 
						intStatus = '1'	
						WHERE
						intGroupId = '$groupId' ;";
			
			$finalResult = $db->RunQuery($sqlUpd);
			$newGroupId = $groupId;
		}
		
		$sqlDel = " DELETE FROM plan_printer_group_details 
					WHERE intGroupId = '$newGroupId' ";
		$finalResult = $db->RunQuery($sqlDel);
		
		foreach($printerDetails as $arr)
		{
			$printerId 	= $arr['printerId'];
			
			$sqlIns = "INSERT INTO plan_printer_group_details 
					(
						intGroupId, 
						intPrinterId, 
						intStatus
					)
					VALUES
					(
						'$newGroupId', 
						'$printerId', 
						'1'
					) ";
			$finalResult = $db->RunQuery($sqlIns);
		}
		if($finalResult)
		{
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully';
		}
		else
		{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
		}
		echo json_encode($response);
	}
?>
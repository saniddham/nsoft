<?php 
	session_start();
	$backwardseperator 	= "../../../../";
	$mainPath 			= $_SESSION['mainPath'];
	$userId 	 		= $_SESSION['userId'];
	$requestType 		= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	if($requestType=='loadLocation')
	{
		$companyId = $_REQUEST['companyId'];
		
		$sql = "SELECT
				mst_locations_user.intLocationId,
				mst_locations.strName AS location
				FROM
				mst_locations_user
				Inner Join mst_locations ON mst_locations.intId = mst_locations_user.intLocationId
				Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_locations_user.intUserId ='$userId' AND
				mst_locations.intCompanyId = '$companyId' ";
		$result = $db->RunQuery($sql);
		
		$option = "<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			$option .="<option value=\"".$row['intLocationId']."\">".$row['location']."</option>";
		}
		echo $option;
	}
	if($requestType=='loadTeam')
	{
		$plantId 	 = $_REQUEST['plantId'];
		$companyId	 = $_REQUEST['companyId'];
		$locationId  = $_REQUEST['locationId'];
		
		$sql = "SELECT DISTINCT intId,strName FROM plan_teams
				WHERE intPlantId = '$plantId' ";
		if($companyId!='')
			$sql .="AND intCompanyId= '$companyId' ";
		if($locationId!='')
			$sql .="AND intLocationId= '$locationId' ";
		
		$sql .="AND intParentTeamId = 0 ";
		
		$result = $db->RunQuery($sql);
		$option = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$option.="<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $option;
	}
	if($requestType=='loadHeaderData')
	{
		$searchId = $_REQUEST['searchId'];
		
		$sql = "SELECT * FROM plan_printer_group WHERE intGroupId='$searchId' AND intStatus=1";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$companyId					= $row['intCompanyId'];
			$locationId					= $row['intLocationId'];
			$plantId					= $row['intPlantId'];
			$teamId						= $row['intTeamId'];
			
			$response['groupName'] 		= $row['strGroupName'];
			$response['companyId'] 		= $row['intCompanyId'];
			$response['Allocatedate'] 	= $row['dtmAllocatedDate'];
			$response['plantId'] 		= $row['intPlantId'];
			$response['teamId'] 		= $row['intTeamId'];
		}
		$sql = "SELECT
				mst_locations_user.intLocationId,
				mst_locations.strName AS location
				FROM
				mst_locations_user
				Inner Join mst_locations ON mst_locations.intId = mst_locations_user.intLocationId
				Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_locations_user.intUserId ='$userId' AND
				mst_locations.intCompanyId = '$companyId' ";
		
		$result = $db->RunQuery($sql);
		
		$option = "<option value=\"\"></option>";
		while($row1 = mysqli_fetch_array($result))
		{
			if($row1['intLocationId']==$locationId)
				$option .="<option selected=\"selected\" value=\"".$row1['intLocationId']."\">".$row1['location']."</option>";
			else
				$option .="<option value=\"".$row1['intLocationId']."\">".$row1['location']."</option>";
		}
		$response['location'] = $option;
		
		$sql = "SELECT DISTINCT intId,strName FROM plan_teams
				WHERE intPlantId = '$plantId' AND 
				intCompanyId= '$companyId' AND 
				intLocationId= '$locationId' AND
				intParentTeamId = 0 ";
		
		$result = $db->RunQuery($sql);
		$option = "<option value=\"\"></option>";
		while($row2=mysqli_fetch_array($result))
		{
			if($row2['intId']==$teamId)
				$option.="<option selected=\"selected\" value=\"".$row2['intId']."\">".$row2['strName']."</option>";
			else
				$option.="<option value=\"".$row2['intId']."\">".$row2['strName']."</option>";
		}
		$response['Team'] = $option;
		
		echo json_encode($response);
	}
	if($requestType=='loadDetailData')
	{
		$searchId = $_REQUEST['searchId'];
		
		$sqlDetails = " SELECT intPrinterId,intPrinterId NOT IN (SELECT DISTINCT intPrinterId
						FROM plan_actual_details
						WHERE intPrinterGroupId = '$searchId'
						) AS intStatus,plan_printer.strPrinterName
						FROM plan_printer_group_details
						INNER JOIN plan_printer ON plan_printer.intId=plan_printer_group_details.intPrinterId 
						WHERE intGroupId = '$searchId' ";
		$result = $db->RunQuery($sqlDetails);
		while($rowDetail = mysqli_fetch_array($result))
		{
			$arrData[] = $rowDetail;
		}
		$response['arrData'] 	= $arrData;
		
		echo json_encode($response);
	}
?>
<?php
session_start();
$backwardseperator = "../../../../";
$thisFilePath =  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Printer Group</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="printerGroup-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">

</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<script src="../../../../libraries/validate//jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<form id="frmPrinterGroup" name="frmPrinterGroup"   autocomplete="off">
<div align="center">
		<div class="trans_layoutS">
		  <div class="trans_text">Printer Group</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="112%" border="0" class="">
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td width="362">&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Printer Group</td>
                <td><select name="cboSearchGroup" id="cboSearchGroup" style="width:200px">
                <option value=""></option>
                <?php
					$sql = "SELECT intGroupId,strGroupName FROM plan_printer_group WHERE intStatus=1"; 
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row["intGroupId"]."\" >".$row["strGroupName"]."</option>";
					}
				?>
                </select></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Group Name <span class="compulsoryRed">*</span></td>
                <td><input type="text" name="txtGroupName" id="txtGroupName" style="width:200px" class="validate[required,maxSize[50]]" /></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Company <span class="compulsoryRed">*</span></td>
                <td><select name="cboNewCompany" id="cboNewCompany" style="width:320px" class="validate[required]">
                 <option value=""></option>
                <?php
					$sql = "SELECT intId,strName FROM mst_companies WHERE intStatus=1 ORDER BY strName";
					$result = $db->RunQuery($sql);
					while($row = mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row["intId"]."\" >".$row["strName"]."</option>";	
					}
				?>
                  </select></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Location <span class="compulsoryRed">*</span></td>
                <td><select name="cboLocation" id="cboLocation" style="width:320px" class="validate[required]">
                </select></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Date</td>
                <td><input name="dtDate" type="text" value="<?php echo date('Y-m-d'); ?>" class="txtbox" id="dtDate" style="width:90px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              </tr>
              <tr>
                <td width="1" class="normalfnt">&nbsp;</td>
                <td width="105" class="normalfnt">Machine Plant <span class="compulsoryRed">*</span></td>
                <td><select name="cboMachinePlant" id="cboMachinePlant" style="width:200px" class="validate[required]">
                <option value=""></option>
                <?php
				$sql = "SELECT 	intId, strName
						FROM mst_printertypes 
						WHERE intStatus = 1
						ORDER BY strName ";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					echo "<option value=\"".$row["intId"]."\" >".$row["strName"]."</option>";
				}
				
		?>
                </select></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Team <span class="compulsoryRed">*</span></td>
                <td class="normalfnt"><select name="cboTeam" id="cboTeam" style="width:200px" class="validate[required]">
                </select></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td align="left" valign="top" class="normalfnt">Add Printers</td>
                <td align="left"><div id="divTable" style="overflow:scroll;width:338px;height:200px"  >
                <table id="tblMain" width="320" border="0" align="left" class="grid">
                  <tr class="gridHeader">
                    <td colspan="2" bgcolor="#FFFFFF" class="normalfntRight"><img src="../../../../images/insert.png" alt="altInsert" name="butInsert" width="78" height="24" id="butInsert" class="mouseover" /></td>
                  </tr>
                  <tr class="gridHeader">
                    <td width="46">Select</td>
                    <td width="264">Printer Name</td>
                    </tr>
                </table></div></td>
                </tr>
              </table></td>
            </tr>
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img style="display:none" border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><img border="0" src="../../../../images/Tsave.jpg" id="butSave" name="butSave" width="92" class="mouseover" height="24" /><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
</form>
</body>
 <div    style="width:500px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
 <div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</html>

// JavaScript Document
$(document).ready(function(){
	
	$("#frmPrinterGroup").validationEngine();
	$('#cboNewCompany').live('change',loadLocation);
	$('#cboLocation').live('change',removePrinters);
	$('#cboMachinePlant').live('change',loadTeam);
	$('#butInsert').live('click',loadPopup);
	$('#butSave').live('click',saveData);
	$('#cboSearchGroup').live('change',loadDetails);
});
function loadLocation()
{
	removePrinters();
	if($(this).val()=="")
		$('#cboLocation').html('');
	else
	{
		var url     = "printerGroup-db-get.php?requestType=loadLocation&companyId="+$(this).val();
		var httpObj = $.ajax({url:url,async:false})
		$('#cboLocation').html(httpObj.responseText);
	}
}
function loadTeam()
{
	if($(this).val()=="")
		$('#cboTeam').html('');
	else
	{
		var companyId 	= $('#cboNewCompany').val();
		var locationId 	= ($('#cboLocation').val()==null?'':$('#cboLocation').val());
		var url     	= "printerGroup-db-get.php?requestType=loadTeam&plantId="+$(this).val()+'&companyId='+companyId+'&locationId='+locationId;
		var httpObj 	= $.ajax({url:url,async:false})
		$('#cboTeam').html(httpObj.responseText);
	}
}
function loadPopup()
{
	$('#txtGroupName').removeClass('validate[required,maxSize[50]]');
	$('#cboMachinePlant').removeClass('validate[required]');
	$('#cboTeam').removeClass('validate[required]');
	
	if ($('#frmPrinterGroup').validationEngine('validate'))
	{
		var companyId 	= $('#cboNewCompany').val();
		var locationId 	= $('#cboLocation').val();
		var date		= $('#dtDate').val();
	
	popupWindow3('1');
	$('#popupContact1').html('');
	$('#popupContact1').load('popup.php?companyId='+companyId+'&locationId='+locationId+'&date='+date,function(){
		
		$('#frmPopUp #butAdd').click(function(){
			
			$('.clsPrinters:checked').each(function(){
				var printerId = $(this).parent().parent().attr('id');
				var found = false;
				$('.clsMainPrinter').each(function(){
					if(printerId ==$(this).parent().attr('id'))
					{
						found = true;
					}	
				});
				if(found==false)
				{
					
					document.getElementById('tblMain').insertRow(document.getElementById('tblMain').rows.length);
					document.getElementById('tblMain').rows[document.getElementById('tblMain').rows.length-1].innerHTML = "<td align=\"center\" bgcolor=\"#FFFFFF\" class=\"clsMainPrinter\">"+
					"<img class=\"mouseover removeRow\" src=\"../../../../images/del.png\" /></td>"+
    				"<td bgcolor=\"#FFFFFF\" class=\"normalfnt\">"+$(this).parent().parent().find('.clsPrinterName').html()+"</td>";
					document.getElementById('tblMain').rows[document.getElementById('tblMain').rows.length-1].id  = printerId;
					setRemoveRow();
					
				}
			});
			disablePopup();
			
		});
		$('#frmPopUp #butClose1').click(disablePopup);
		
		$('#frmPopUp #chkAll').live('click',function(){

			var status = false;
			if($('#chkAll').attr('checked'))
			{
				status = true;
			}
			else
			{
				status = false;
			}
			$('#frmPopUp .clsPrinters').attr("checked",status);
		});
		
	});
	}
}
function setRemoveRow()
{
	$('.removeRow').click(function(){
		if($(this).parent().attr('id')==0)
		{
			$(this).validationEngine('showPrompt','cannot remove the printer from this group','fail');
			var t=setTimeout("alertx1()",1500);
			return;
		}
		 $(this).parent().parent().remove();
	});	
}
function removePrinters()
{
	$('.removeRow').parent().parent().remove();
}
function saveData()
{
	$('#txtGroupName').addClass('validate[required,maxSize[50]]');
	$('#cboMachinePlant').addClass('validate[required]');
	$('#cboTeam').addClass('validate[required]');
	
	if ($('#frmPrinterGroup').validationEngine('validate'))
	{
		if($('#tblMain tr').length==2)
		{
			$('#frmPrinterGroup #butSave').validationEngine('showPrompt','Please insert Printers','fail');
			var t=setTimeout("alertx()",1000);
			return;
		}
		else
		{
			var value="[ ";
			$('.removeRow').each(function(){
				
				var printerId  	= $(this).parent().parent().attr("id");
				value +='{"printerId":"'+printerId+'"},' ;
				
			});
			
			value = value.substr(0,value.length-1);
			value += " ]";
			
			var url = "printerGroup-db-set.php?requestType=saveData";
					$.ajax({
							url:url,
							async:false,
							dataType:'json',
							type:'post',
							data:$("#frmPrinterGroup").serialize()+'&printerDetails='+value,
						success:function(json){
							$('#frmPrinterGroup #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							if(json.type=='pass')
							{
								var t=setTimeout("alertx()",1000);
								setTimeout("window.location.href=window.location.href;",1000)
								return;
							}
							else if(json.type=='found')
							{
								var t=setTimeout("alertx()",1000);
								return;
							}
						},
						error:function(){
							
							$('#frmPrinterGroup #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",3000);						
						}
					});
		}
	}
}
function alertx()
{
	$('#frmPrinterGroup #butSave').validationEngine('hide');
}
function loadDetails()
{
	var searchId = $(this).val();
	if(searchId=='')
	{
		removePrinters();
		$('#frmPrinterGroup')[0].reset();
		$('#cboLocation').html('');
		$('#cboTeam').html('');
		return;
	}
	var url = "printerGroup-db-get.php?requestType=loadHeaderData";
					$.ajax({
							url:url,
							async:false,
							dataType:'json',
							type:'post',
							data:'&searchId='+searchId,
							success:function(json){
								
								$('#txtGroupName').val(json.groupName);
								$('#cboNewCompany').val(json.companyId);
								$('#cboLocation').html(json.location);
								$('#dtDate').val(json.Allocatedate);
								$('#cboMachinePlant').val(json.plantId);
								$('#cboTeam').html(json.Team);
							},
							error:function(){
												
							}
						});
	var url = "printerGroup-db-get.php?requestType=loadDetailData";
					$.ajax({
							url:url,
							async:false,
							dataType:'json',
							type:'post',
							data:'&searchId='+searchId,
							success:function(json){
								
								var length  = json.arrData.length;
								var arrData = json.arrData;
								
								var content ='<tr class="gridHeader">';
                    				content+='<td colspan="2" bgcolor="#FFFFFF" class="normalfntRight"><img src="../../../../images/insert.png" alt="altInsert" name="butInsert" width="78" height="24" id="butInsert" class="mouseover" /></td>';
                  					content+='</tr>';
                  					content+='<tr class="gridHeader">';
                    				content+='<td width="46">Select</td>';
                    				content+='<td width="264">Printer Name</td>';
                    				content+='</tr>';
								for(var i=0;i<length;i++)
								{
									
									var printerId	= arrData[i]['intPrinterId'];	
									var printerName	= arrData[i]['strPrinterName'];
									var chkRemove   = arrData[i]['intStatus'];
									
									content+='<tr class="normalfnt" id="'+printerId+'" >';
									content+='<td align="center" bgcolor="#FFFFFF" class="clsMainPrinter" id="'+chkRemove+'"><img class="mouseover removeRow" src="../../../../images/del.png" /></td>';
									content+='<td bgcolor="#FFFFFF" class="normalfnt">'+printerName+'</td>';
									content +='</tr>';
									
									$('#frmPrinterGroup #tblMain').html(content);
									setRemoveRow();
								}
							},
							error:function(){
												
							}
						});
}
function alertx1()
{
	$('.removeRow').validationEngine('hide');
}
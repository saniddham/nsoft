<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include $backwardseperator."dataAccess/Connector.php";

$companyId  = $_REQUEST['companyId'];
$locationId = $_REQUEST['locationId']; 
$date 		= $_REQUEST['date']; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Printer</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />

<link href="../../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>


</head>

<body>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>

    
<form id="frmPopUp" name="frmPopUp" method="post" action="">
<table width="450" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutS" style="width:430px">
		  <div class="trans_text"> Select Printers</div>
		  <table width="400" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="401" border="0">
      <tr>
        <td></td>
      </tr>
      <tr>
        <td><div style="width:400px;height:300px;overflow:scroll" >
          <table width="382" class="grid" id="tblTechnique" >
            <tr class="gridHeader">
              <td width="14%" height="22" ><input class="clsChkAll" type="checkbox" id="chkAll" /></td>
              <td width="86%" >Printer Name</td>
              </tr>
              <?php
					$sql = "SELECT DISTINCT intId,strPrinterName 
							FROM plan_printer
							WHERE intCompanyId='$companyId' AND
							intLocationId='$locationId' AND
							intStatus=1 AND 
							intId NOT IN (SELECT DISTINCT plan_printer_group_details.intPrinterId
							FROM plan_printer_group
							INNER JOIN plan_printer_group_details ON plan_printer_group.intGroupId=plan_printer_group_details.intGroupId
							INNER JOIN plan_printer ON plan_printer.intId=plan_printer_group_details.intPrinterId
							WHERE plan_printer_group.dtmAllocatedDate='$date' AND
							plan_printer_group.intCompanyId='$companyId' AND
							plan_printer_group.intLocationId='$locationId' AND
							plan_printer_group.intStatus='1' )
							ORDER BY plan_printer.strPrinterName  ";
					
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
			  
			  ?>
            	<tr class="normalfnt" id="<?Php echo $row['intId']; ?>">
              		<td align="center" bgcolor="#FFFFFF"><input class="clsPrinters" type="checkbox" id="chkPrinter" /></td>
              		<td bgcolor="#FFFFFF" class="clsPrinterName" ><?Php echo $row['strPrinterName']; ?></td>
              		</tr>
              
              <?php
					}
			  ?>
            </table>
          </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><img src="../../../../images/Tadd.jpg" width="92" height="24"  alt="add" id="butAdd" name="butAdd" class="mouseover"/><img src="../../../../planning/img/Tclose.jpg" width="92" height="24" id="butClose1" name="butClose1" class="mouseover"  /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>

</body>
</html>

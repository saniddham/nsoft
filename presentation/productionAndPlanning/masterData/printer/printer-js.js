// JavaScript Document
$(document).ready(function() {
  		$("#frmPrinter").validationEngine();
		$('#frmPrinter #txtName').focus();
  //permision for add 
	if(intAddx)
	{
		$('#frmPrinter #butNew').show();
		$('#frmPrinter #butSave').show();
	}
	
	//permision for edit 
	if(intEditx)
	{
		$('#frmPrinter #butSave').show();
		$('#frmPrinter #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
	}
	
	//permision for delete
	if(intDeletex)
	{
		$('#frmPrinter #butDelete').show();
		$('#frmPrinter #cboSearch').removeAttr('disabled');
	}
	
	//permision for view
	if(intViewx)
	{
		$('#frmPrinter #cboSearch').removeAttr('disabled');
	}
	
	$('#frmPrinter #butSave').live('click',function(){
		
		var requestType = '';
		if($('#frmPrinter').validationEngine('validate'))	
		{
			if($("#frmPrinter #cboSearch").val()=='')
				requestType = 'Add';
			else
				requestType = 'Edit';
			
			var url = "printer-db-set.php";
			var obj = $.ajax({
				url:url,
				dataType: "json",  
				data:$("#frmPrinter").serialize()+'&requestType='+requestType,
				async:false,
				
				success:function(json){
						$('#frmPrinter #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
						{
							$('#frmPrinter').get(0).reset();
							loadCombo_Printer();
							var t=setTimeout("alertx()",1000);
							return;
						}
						var t=setTimeout("alertx()",3000);
					},
				error:function(xhr,status){
						
						$('#frmPrinter #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertx()",3000);
						
					}		
			});
		}
	});
	
	$('#frmPrinter #cboSearch').live('change',function(){
		
		$('#frmPrinter').validationEngine('hide');
		var url = "printer-db-get.php";
		if($('#frmPrinter #cboSearch').val()=='')
		{
			$('#frmPrinter').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					
					$('#frmPrinter #txtName').val(json.name);
					$('#frmPrinter #cboCompany').val(json.companyId);
					loadLocation(json.companyId);
					$('#frmPrinter #cboLocation').val(json.locationId);
					$('#frmPrinter #txtRemark').val(json.remark);
					$('#frmPrinter #cboType').val(json.type);
					$('#frmPrinter #txtIdNo').val(json.IDNo);
					$('#frmPrinter #chkActive').attr('checked',json.status);
			}
		});	
	});
	
	$('#frmPrinter #butNew').live('click',function(){
		$('#frmPrinter').get(0).reset();
		loadCombo_Printer()
		$('#frmPrinter #txtName').focus();
	});
	
	$('#frmPrinter #butDelete').live('click',function(){
		
		if($('#frmPrinter #cboSearch').val()=='')
		{
			$('#frmPrinter #butDelete').validationEngine('showPrompt', 'Please select Size.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmPrinter #cboSearch option:selected').text()+'" ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
					if(v)
					{
						var url = "printer-db-set.php";
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							data:'requestType=delete&cboSearch='+$('#frmPrinter #cboSearch').val(),
							async:false,
							success:function(json){
								
								$('#frmPrinter #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								
								if(json.type=='pass')
								{
									$('#frmPrinter').get(0).reset();
									loadCombo_Printer();
									var t=setTimeout("alertDelete()",1000);return;
								}	
								var t=setTimeout("alertDelete()",3000);
							}	 
						});
					}
				}
		 	});
		}
	});
	
});
function loadCombo_Printer()
{
	var url 	= "printer-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmPrinter #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmPrinter #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmPrinter #butDelete').validationEngine('hide')	;
}
function loadLocation(companyId)
{
	var url     = "printer-db-get.php?requestType=loadLocation&companyId="+companyId;
	var httpObj = $.ajax({url:url,async:false})
	$('#frmPrinter #cboLocation').html(httpObj.responseText);
}
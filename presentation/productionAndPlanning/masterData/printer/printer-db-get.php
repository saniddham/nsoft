<?php 
	session_start();
	$backwardseperator 	= "../../../../";
	$mainPath 			= $_SESSION['mainPath'];
	$userId 	 		= $_SESSION['userId'];
	$requestType 		= $_REQUEST['requestType'];
	$intLocationId 	    = $_SESSION["CompanyID"];
	
	include "{$backwardseperator}dataAccess/Connector.php";
	
	if($requestType=='loadLocation')
	{
		$companyId = $_REQUEST['companyId'];
		
		$sql = "SELECT
				mst_locations_user.intLocationId,
				mst_locations.strName AS location
				FROM
				mst_locations_user
				Inner Join mst_locations ON mst_locations.intId = mst_locations_user.intLocationId
				Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_locations_user.intUserId ='$userId' AND
				mst_locations.intCompanyId = '$companyId' ";
		$result = $db->RunQuery($sql);
	
		$option = "<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			$option .="<option value=\"".$row['intLocationId']."\">".$row['location']."</option>";
		}
		echo $option;
	}
	else if($requestType=='loadCombo')
	{
		$sql = "SELECT 	intId, strPrinterName FROM plan_printer order by strPrinterName where  intLocationId = '$intLocationId' ";
		$result = $db->RunQuery($sql);
		$option = "<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			$option .="<option value=\"".$row['intId']."\">".$row['strPrinterName']."</option>";
		}
		echo $option;
	}
	else if($requestType=='loadDetails')
	{
		$id = $_REQUEST['id'];
		
		$sql = "SELECT strPrinterName, intCompanyId, 
				intLocationId, intEmployeeType, strIdNo, strRemark, 
				intStatus  
				FROM 
				plan_printer 
				WHERE intId = '$id' ";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['name'] 		= $row['strPrinterName'];
			$response['companyId'] 	= $row['intCompanyId'];
			$response['locationId'] = $row['intLocationId'];
			$response['type'] 		= $row['intEmployeeType'];
			$response['IDNo'] 		= $row['strIdNo'];
			$response['remark'] 	= $row['strRemark'];
			$response['status'] 	= ($row['intStatus']?true:false);		
		}
		echo json_encode($response);
	}
?>
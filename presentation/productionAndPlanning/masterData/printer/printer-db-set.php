<?php 
	session_start();
	$backwardseperator 	= "../../../../";
	$mainPath 			= $_SESSION['mainPath'];
	$userId 	 		= $_SESSION['userId'];
	$requestType 		= $_REQUEST['requestType'];
	$locationId			= $_SESSION["CompanyID"];
	$companyId			= $_SESSION["headCompanyId"];
	
	include "{$backwardseperator}dataAccess/Connector.php";
	
	$name 			= $_REQUEST['txtName'];
	$id 			= $_REQUEST['cboSearch'];
	$Type 			= $_REQUEST['cboType'];
	$IDNo 			= $_REQUEST['txtIdNo'];
	$remark 		= $_REQUEST['txtRemark'];
	$intStatus		= ($_REQUEST['chkActive']?1:0);	
	
	if($requestType=='Add')
	{
		$sql = "INSERT INTO plan_printer 
				(
					strPrinterName, intCompanyId, intLocationId, intEmployeeType, strIdNo, strRemark, intStatus, intCreateBy, dtmCreateDate
				)
				VALUES
				(
					'$name', '$companyId', '$locationId', '$Type', '$IDNo', '$remark', '$intStatus', '$userId', now()
				);";
		$result = $db->RunQuery($sql);
		if($result)
		{
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else
		{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	else if($requestType=='Edit')
	{
		$sql = "UPDATE plan_printer 
				SET
				strPrinterName = '$name' , 
				intEmployeeType = '$Type',
				strIdNo = '$IDNo', 
				strRemark = '$remark' , 
				intStatus = '$intStatus' , 
				intModifiedBy = '$userId' , 
				dtmModifyDate = now()
				WHERE
				intId = '$id' AND
				intLocationId = '$locationId' ";
		$result = $db->RunQuery($sql);
		if($result)
		{
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else
		{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	else if($requestType=='delete')
	{
		$sql = "DELETE FROM plan_printer WHERE intId = '$id' AND intLocationId = '$locationId' ";
		$result = $db->RunQuery($sql);
		if(($result))
		{
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else
		{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
?>
<?php 
	session_start();
	$backwardseperator 	= "../../../../";
	$mainPath 			= $_SESSION['mainPath'];
	
	$userId 	 		= $_SESSION['userId'];
	$locationId			= $_SESSION["CompanyID"];
	$companyId			= $_SESSION["headCompanyId"];
	$requestType 		= $_REQUEST['requestType'];
	//$empDetails 		= json_decode($_REQUEST['empDetails'], true);
	$currDB 			= $_SESSION['Database'];
	$HRDB 				= $_SESSION['HRDatabase'];
	
	include "{$backwardseperator}dataAccess/Connector.php";
	
	if($requestType=='employeeAllocate')
	{
		$empId 			= $_REQUEST['empId'];
		$empType 		= $_REQUEST['empType'];
		$mainTeamId 	= $_REQUEST['mainTeamId'];
		$mainPlantId 	= $_REQUEST['mainPlantId'];
		$mainDate 		= $_REQUEST['mainDate'];
		$chkStatus		= $_REQUEST['chkStatus'];
		
		if($chkStatus==0)
		{
			$sqlDel = " DELETE FROM plan_employeeallocation 
						WHERE
						intTeam = '$mainTeamId' AND 
						dtmDate = '$mainDate' AND 
						intLocationId = '$locationId' AND
						intCompanyId = '$companyId' AND
						intEmpId ='$empId' ";
			$finalResult = $db->RunQuery($sqlDel);
		}
		else
		{
			$sqlIns = " INSERT INTO plan_employeeallocation 
						(
						intTeam, 
						intPlantId, 
						dtmDate, 
						intEmpId, 
						intEmpType, 
						intCompanyId, 
						intLocationId, 
						intCreatedBy, 
						dtmCreatedDate
						)
						VALUES
						(
						'$mainTeamId', 
						'$mainPlantId', 
						'$mainDate', 
						'$empId', 
						'$empType', 
						'$companyId', 
						'$locationId', 
						'$userId', 
						now()
						); ";
			$finalResult = $db->RunQuery($sqlIns);
		}
		
		$sql = "SELECT COUNT(intId) AS totEmp 
				FROM plan_employeeallocation 
				WHERE intTeam='$mainTeamId' AND
				dtmDate='$mainDate' AND
				intLocationId='$locationId'";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$empCount = $row['totEmp'];
		
		if($finalResult)
		{
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully';
			$response['count'] 		= $empCount;
		}
		else
		{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
		}
		echo json_encode($response);
	}
	else if($requestType=="searchEmployee")
	{
		$empType 		= $_REQUEST['empType'];
		$empName 		= $_REQUEST['empName'];
		$teamId 		= $_REQUEST['teamId'];
		$date 			= $_REQUEST['date'];
		
		$sql = "SELECT DISTINCT PP.intId,PP.strPrinterName ,PP.intEmployeeType,PET.strEmployeeType,
				IFNULL(PEA.intEmpId,0) AS empId,
				PP.strIdNo,
				ME.Photo
				FROM $currDB.plan_printer PP
				INNER JOIN $currDB.plan_employeetype PET ON PET.intId=PP.intEmployeeType
				LEFT JOIN $currDB.plan_employeeallocation PEA ON PEA.intEmpId=PP.intId AND
				PP.intLocationId=PEA.intLocationId AND
				PP.intCompanyId=PEA.intCompanyId AND
				PEA.dtmDate='$date'
				LEFT JOIN $HRDB.mst_employee ME ON ME.strNIC = PP.strIdNo   
				LEFT JOIN $HRDB.trn_attendance ATTN ON ATTN.dtDate=PEA.dtmDate  AND  ATTN.intEmployeeId=ME.intEmployeeId
				WHERE PP.intCompanyId='$companyId' AND
				PP.intLocationId='$locationId' AND
				PP.intStatus=1 AND 
				PP.intId NOT IN (SELECT DISTINCT PEA.intEmpId
				FROM $currDB.plan_employeeallocation PEA
				WHERE PEA.dtmDate='$date' AND
				PEA.intCompanyId='$companyId' AND
				PEA.intLocationId='$locationId' AND
				PEA.intTeam<>'$teamId') ";
		if($empType!="")
			$sql.="AND PP.intEmployeeType='$empType' ";
		if($empName!="")
			$sql.="AND PP.strPrinterName LIKE '%$empName%' ";
				
		$sql.="ORDER BY PP.intId";
		
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$data['EmpId'] 	 	 	= $row['intId'];
			$data['allocateEmpId'] 	= $row['empId'];
			$data['EmpName'] 	 	= $row['strPrinterName'];
			$data['EmpType'] 	 	= $row['intEmployeeType'];
			$data['EmpTypeName'] 	= $row['strEmployeeType'];
			$data['Photo']   		= $row['Photo'];		
			
			$arrDetailData[] = $data;
		}
		$response['arrDetailData'] 		= $arrDetailData;
		echo json_encode($response);
	}
	else if($requestType=='copyEmployee')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$fromDate 		= $_REQUEST['fromDate'];
		$toDate 		= $_REQUEST['toDate'];
		$allocatedDate 	= $_REQUEST['allocatedDate'];
		$rollBackFlag	= 0 ;
		$msg			= '';
		
		$startDate 		= strtotime($fromDate);
		$endDate 		= strtotime($toDate);
		$days_between 	= ceil(abs($endDate - $startDate) / 86400);
		$newFrmDate 	= $fromDate;
		
		for($x=0;$x<=$days_between;$x++)
		{
			$date 		= date_create($newFrmDate);
			date_add($date, date_interval_create_from_date_string('1 days'));
			$nextDt 	= date_format($date, 'Y-m-d');
			
			$sqlDel = "DELETE FROM plan_employeeallocation WHERE dtmDate = '$newFrmDate' AND intLocationId = '$locationId' ";
			$resultDel = $db->RunQuery2($sqlDel);
			if($resultDel)
			{
				$sqlIns = " INSERT INTO plan_employeeallocation 
							(intTeam, 
							intPlantId, 
							dtmDate, 
							intEmpId, 
							intEmpType, 
							intCompanyId, 
							intLocationId, 
							intCreatedBy, 
							dtmCreatedDate
							)
							SELECT 	intTeam, 
									intPlantId, 
									'$newFrmDate', 
									intEmpId, 
									intEmpType, 
									intCompanyId, 
									intLocationId, 
									$userId, 
									NOW()
									FROM 
									plan_employeeallocation 
									WHERE dtmDate = '$allocatedDate' AND intLocationId ='$locationId' ";
					
				$resultins = $db->RunQuery2($sqlIns);
				if(!$resultins)
				{
					$rollBackFlag = 1 ;
					$msg = 'error in saving' ;
					break;
				}
			}
			else
			{
				$rollBackFlag = 1 ;
				$msg = 'error in deleting' ;
				break;
			}
			$newFrmDate = $nextDt;
		}
		
		if($rollBackFlag==1)
		{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $msg;
		}
		else
		{
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
		}
		
		$db->CloseConnection();
		echo json_encode($response);
	}
		
?>
// JavaScript Document

$(document).ready(function(){
	
	var obj;
	$("#frmActualProduction").validationEngine();
	$("#frmPopUp").validationEngine();
	$("#frmCopyPopUp").validationEngine();
	
	$('#cboYear').live('change',function(){
			
		document.getElementById('frmEmployeeAllocation').submit();	
	});
	$('#cboMonth').live('change',function(){
			
		document.getElementById('frmEmployeeAllocation').submit();	
	});
	
	$('.insertPrinter').live('click',loadPopup);
	$('#butCopy').live('click',loadCopyPopup);
	$('#butSearch').live('click',seachEmployee);
	$('.clsPrinters').live('click',saveEmployee);
});

function loadPopup()
{
	var teamId	= $(this).parent().parent().parent().attr('id');
	var date	= $(this).parent().parent().attr('id');
	var plantId = $(this).parent().parent().parent().find('.clsPlant').attr('id')
	obj			= $(this);
	
	popupWindow3('1');
	$('#popupContact1').html('');
	$('#popupContact1').load('popup.php?teamId='+teamId+'&date='+date+'&plantId='+plantId,function(){
		
		$('#frmPopUp #butClose1').click(disablePopup);
		$('#frmPopUp #butAdd').click(disablePopup);
		
		/*$('#frmPopUp #chkAll').live('click',function(){

			var status = false;
			if($('#chkAll').attr('checked'))
			{
				status = true;	
			}
			else
			{
				status = false;
			}
			$('#frmPopUp .clsPrinters').attr("checked",status);
		});*/
		
	});
}
function saveEmployee()
{
	var empId 		= $(this).parent().parent().attr('id');
	var empType 	= $(this).parent().parent().find('.clsPrinterType').attr('id');
	var chkStatus	= ($(this).attr('checked')?1:0);
	var objChk		= $(this);
	
	var url = "employeeAllocation-db-set.php?requestType=employeeAllocate";
	$.ajax({
			url:url,
			async:false,
			dataType:'json',
			type:'post',
			data:'&mainTeamId='+mainTeamId+'&mainPlantId='+mainPlantId+'&mainDate='+mainDate+'&empId='+empId+'&empType='+empType+'&chkStatus='+chkStatus,
		success:function(json){
			if(json.type=='pass')
			{
				var empCount = json.count;
				obj.parent().parent().find('.clsNoOfEmp').html(empCount);
			}
			else
			{
				$(objChk).validationEngine('showPrompt',json.msg,'fail');
				var t=setTimeout("alertx()",3000);	
				return;	
				
			}
		},
		error:function(xhr,status){
			
			$(objChk).validationEngine('showPrompt',errormsg(xhr.status),'fail');
			var t=setTimeout("alertx()",3000);	
			return;					
		}
	});
}
function loadCopyPopup()
{
	var year  = $('#cboYear').val();
	var month = $('#cboMonth').val();	
	
	popupWindow3('1');
	$('#popupContact1').html('');
	$('#popupContact1').load('copyPopup.php?',function(){
		
		$('#frmCopyPopUp #butPopCopy').click(function(){
			showWaiting();
			var fromDate		= $('#frmCopyPopUp #dtCopyFromDate').val();
			var toDate			= $('#frmCopyPopUp #dtCopyToDate').val();
			var allocatedDate	= $('#frmCopyPopUp #dtAllocatedDate').val();
		
			var difference = (Date.parse(toDate) - Date.parse(fromDate)) / (86400000 * 7);
			if(difference<0 || difference==0 )
			{
				$('#frmCopyPopUp #butPopCopy').validationEngine('showPrompt','* Invalid Date Range','fail');
				var t=setTimeout("alertx2()",3000);	
				hideWaiting();
				return;
			}
			else
			{
				var url = "employeeAllocation-db-set.php?requestType=copyEmployee";
				$.ajax({
					url:url,
					async:false,
					dataType:'json',
					type:'post',
					data:'&fromDate='+fromDate+'&toDate='+toDate+'&allocatedDate='+allocatedDate,
					success:function(json){
						if(json.type=='pass')
						{
							hideWaiting();
							document.location.href = "employeeAllocation.php?cboYear="+year+"&cboMonth="+month;
							disablePopup();		
						}
						else
						{
							hideWaiting();
							$('#frmCopyPopUp #butPopCopy').validationEngine('showPrompt',json.msg,'fail');
							var t=setTimeout("alertx2()",3000);	
							return;	
							
						}
					},
					error:function(xhr,status){
					
						hideWaiting();
						$('#frmCopyPopUp #butPopCopy').validationEngine('showPrompt',errormsg(xhr.status),'fail');
						var t=setTimeout("alertx2()",3000);	
						return;					
					}
				});
				
			}
			
		});
		$('#frmCopyPopUp #butClose1').click(disablePopup);
		
	});
}
function seachEmployee()
{
	var empType = $('#cboEmpType').val();
	var empName = $('#txtEmpName').val();
	
	$("#tblEmployee tr:gt(0)").remove();
		
		var url 	= "employeeAllocation-db-set.php?requestType=searchEmployee";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"empType="+empType+"&empName="+URLEncode(empName)+"&teamId="+mainTeamId+"&date="+mainDate,
			async:false,
			success:function(json){
				
				var lengthDetail   = json.arrDetailData.length;
				var arrDetailData  = json.arrDetailData;
				
				for(var i=0;i<lengthDetail;i++)
				{
					var EmpId			= arrDetailData[i]['EmpId'];
					var allocateEmpId	= arrDetailData[i]['allocateEmpId'];	
					var EmpName			= arrDetailData[i]['EmpName'];
					var EmpType			= arrDetailData[i]['EmpType'];
					var EmpTypeName		= arrDetailData[i]['EmpTypeName'];
					var Photo			= arrDetailData[i]['Photo'];
					
					var content='<tr class="normalfnt" id="'+EmpId+'"><td align="center" bgcolor="#FFFFFF"><input class="clsPrinters" type="checkbox" id="chkPrinter" '+(allocateEmpId==0?'':'checked="checked"')+' /></td>'
					content+='<td bgcolor="#FFFFFF" class="clsPrinterName" >'+EmpName+'</td>'
					content +='<td bgcolor="#FFFFFF" class="clsPrinterType" id="'+EmpType+'" >'+EmpTypeName+'</td>'
					content +='<td bgcolor="#FFFFFF" style="text-align:center">'+(Photo!=null?'<img src="../../../../../qpay/'+Photo+'" width="60" height="60" />':'<img src="../../../../../qpay/images/User.png" width="60" height="60" />')+'</td></tr>';
					add_new_row('#frmPopUp #tblEmployee',content);
				}
			}
		});
			
	
}
function alertx()
{
	$('#frmPopUp .clsPrinters').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmCopyPopUp #butPopCopy').validationEngine('hide')	;
}

function add_new_row(table,rowcontent)
{
	if ($(table).length>0)
	{
		if ($(table+' > tbody').length==0) $(table).append('<tbody />');
		($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
	}
}
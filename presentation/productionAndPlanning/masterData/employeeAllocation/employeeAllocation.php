<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath = $_SESSION['mainPath'];
	
	$thisFilePath =  $_SERVER['PHP_SELF'];
	include  "{$backwardseperator}dataAccess/permisionCheck.inc";
	
	$locationId			= $_SESSION["CompanyID"];
	$companyId			= $_SESSION["headCompanyId"];
	
	$cboYear 			= $_REQUEST['cboYear'];
	//$cboMachinePlant 	= $_REQUEST['cboMachinePlant'];
	$cboMonth 		 	= $_REQUEST['cboMonth'];
	
	if($cboMonth<10)
		$cboMonth = '0'.$cboMonth;
	
	if(!isset($_REQUEST["cboYear"])&& !isset($_REQUEST["cboMonth"]))
	{
		$cboYear			= date('Y');
		$cboMonth			= date('m');
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Employee Allocation</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
<script type="application/javascript" src="employeeAllocation-js.js"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">

</head>

<body>
<form id="frmEmployeeAllocation" name="frmEmployeeAllocation" action="employeeAllocation.php" method="post" >
<table width="100%" border="0" align="center">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div align="center">
<div class="trans_layoutL">
<div class="trans_text">Employee Allocation</div>
	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <tr>
    	<td>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr>
        	<td width="23%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
            	<td class="normalfnt" width="11%">Year</td>
            	<td class="normalfnt" width="13%"><select name="cboYear" id="cboYear" style="width:100px">
				<?php
					$sql ="SELECT DISTINCT YEAR(strDate) AS intYear FROM plan_upload_details";
					$result = $db->RunQuery($sql);
					while($row = mysqli_fetch_array($result))
					{
						if($cboYear==$row['intYear'])
						echo "<option value=\"".$row["intYear"]."\" selected=\"selected\" >".$row["intYear"]."</option>";
						else
						echo "<option value=\"".$row["intYear"]."\" >".$row["intYear"]."</option>";
					}
                ?>
            	</select>
                </td>
                <td class="normalfnt" width="7%">Month</td>
                <td class="normalfnt" width="16%"><select name="cboMonth" id="cboMonth" style="width:108px">
                <option value=""></option>
                <?php
					$sql ="SELECT intMonthId,strMonth FROM mst_month ORDER BY intMonthId";
					$result = $db->RunQuery($sql);
					while($row = mysqli_fetch_array($result))
					{
						if($cboMonth==$row['intMonthId'])
						echo "<option value=\"".$row["intMonthId"]."\" selected=\"selected\" >".$row["strMonth"]."</option>";
						else
						echo "<option value=\"".$row["intMonthId"]."\" >".$row["strMonth"]."</option>";
					}
                ?>
                </select></td>
                <td class="normalfnt" width="11%"></td>
                <td width="31%" class="normalfnt"><!--<select name="cboMachinePlant" id="cboMachinePlant" style="width:230px">
                <option value=""></option>
                <?php
					$sql ="SELECT intId,strName FROM mst_printertypes WHERE intStatus=1 order by strName ";
					$result = $db->RunQuery($sql);
					while($row = mysqli_fetch_array($result))
					{
						if($cboMachinePlant==$row['intId'])
						echo "<option value=\"".$row["intId"]."\" selected=\"selected\" >".$row["strName"]."</option>";
						else
						echo "<option value=\"".$row["intId"]."\" >".$row["strName"]."</option>";
					}
                ?>
                </select>--></td>
            <td class="normalfnt" width="11%"><img border="0" src="../../../../images/Tcopy.png" alt="Copy" name="butCopy" class="clsCopy mouseover" id="butCopy" tabindex="28"/></td>
            </tr>
            </table>
            </td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        </tr>
        <tr>
        	<td colspan="2"><div style="height:350px;overflow:scroll;width:880" >
        	<table width="100%" class="bordered" id="tblSizesPopup" >
       		<thead>
            <tr>
            	<th height="19" >Plant</th>
                <th height="19" >Team</th>
                <?php
					$num = cal_days_in_month(CAL_GREGORIAN,$cboMonth, $cboYear );
					for($x=1;$x<=$num;$x++)
					{
						if($x<10)
						{
							$x = '0'.$x;
						}
				?>
                		<th width="70" height="19" ><?php echo $cboYear.'-'.$cboMonth.'-'.$x ;?></th>
                <?php
					}
				?>
            </tr>
        </thead>
        <tbody>
        <?php
        $sql = "SELECT plan_teams.intId,plan_teams.strName,intPlantId,mst_printertypes.strName AS plantName 
				FROM plan_teams
				INNER JOIN mst_printertypes ON plan_teams.intPlantId=mst_printertypes.intId
				WHERE plan_teams.intStatus=1 AND intLocationId='$locationId'";
        $result = $db->RunQuery($sql);
        while($row=mysqli_fetch_array($result))
        {
        ?>
            <tr bgcolor="#FFFFFF" class="normalfnt" id="<?php echo $row['intId'];?>">
            <td align="center" class="clsPlant" nowrap="nowrap" id="<?php echo $row['intPlantId']; ?>">&nbsp;<?php echo $row['plantName']; ?>&nbsp;</td>
            <td align="center" class="clsTeam" nowrap="nowrap">&nbsp;<?php echo $row['strName']; ?>&nbsp;</td>
            <?php
            for($x=1;$x<=$num;$x++)
			{
				if($x<10)
				{
					$x = '0'.$x;
				}
				$date = $cboYear.'-'.$cboMonth.'-'.$x;
				$sql1 =" SELECT COUNT(intId) AS totEmp 
						FROM plan_employeeallocation 
						WHERE intTeam='".$row['intId']."' AND
						dtmDate='".$date."' AND
						intLocationId='$locationId' ";
				$result1 = $db->RunQuery($sql1);
				$row1 = mysqli_fetch_array($result1);
			?>
                		<td width="70" height="19" id="<?php echo $cboYear.'-'.$cboMonth.'-'.$x ; ?>" ><div class="clsNoOfEmp" style="text-align:right;width:50%;float:left"><?php echo ($row1['totEmp']==0?'':$row1['totEmp']); ?></div><div style="text-align:right;width:50%;float:right"><img  src="../../../../images/add_new.png" name="butAdd" width="15" height="15" class="mouseover insertPrinter" id="butAdd" /></div></td>
			<?php
             }
            ?>
            </tr>
        <?php
        }
        ?>
        </tbody>
        </table>
        </div>
        </td>
        </tr>
        </table>
        </td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    </tr>
    </table>
    
</div>
</div>
</form>
<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</body>
</html>
<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include $backwardseperator."dataAccess/Connector.php";

$locationId	= $_SESSION["CompanyID"];
$companyId	= $_SESSION["headCompanyId"];
$currDB 	= $_SESSION['Database'];
$HRDB 		= $_SESSION['HRDatabase'];
	 
$date 		= $_REQUEST['date']; 
$teamId 	= $_REQUEST['teamId'];
$plantId 	= $_REQUEST['plantId'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add Employee</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>

<script>
	var mainTeamId 	= <?php echo $teamId; ?>;
	var mainPlantId = <?php echo $plantId; ?>;
	var mainDate 	= '<?php echo $date; ?>';
</script>

</head>

<body>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>

    
<form id="frmPopUp" name="frmPopUp" method="post" action="">
<table width="450" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutD">
		  <div class="trans_text"> Add Printers</div>
         <table width="600" border="0" align="center" bgcolor="#FFFFFF">
         <tr>
            <td><table width="100%" border="0">
         <tr>
           <td class="normalfnt">Type</td>
           <td class="normalfnt"><select name="cboEmpType" id="cboEmpType" style="width:150px">
           <option value=""></option>
           <?php
		   $sql = "SELECT intId,strEmployeeType FROM plan_employeetype WHERE intStatus=1"; 
		   $result = $db->RunQuery($sql);
		   while($row=mysqli_fetch_array($result))
		   {
			   echo "<option value=\"".$row["intId"]."\" >".$row["strEmployeeType"]."</option>";
		   }
		   ?>
           </select></td>
           <td class="normalfnt">Name</td>
           <td width="43%" class="normalfnt"><input type="text" name="txtEmpName" id="txtEmpName" style="width:250px" /></td>
           <td width="10%" class="normalfnt"><img src="../../../../images/search.png" alt="search" id="butSearch" name="butSearch" class="mouseover"/></td>
           </tr>
         <tr>
            <td width="9%" class="normalfnt">&nbsp;</td>
            <td width="29%" class="normalfnt">&nbsp;</td>
            <td width="9%" class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            </tr>
     	 <tr>
        <td colspan="5"><div style="width:600px;height:415px;overflow:scroll" >
          <table width="100%" class="bordered" id="tblEmployee" >
            <tr>
              <th width="6%" height="22" ><input class="clsChkAll" type="checkbox" id="chkAll" style="display:none" /></th>
              <th width="56%" >Employee Name</th>
              <th width="27%" >Type</th>
              <th width="11%" >Photo</th>
              </tr>
              <?php
			  		
					$sql = "SELECT DISTINCT PP.intId,PP.strPrinterName ,PP.intEmployeeType,PET.strEmployeeType,
							IFNULL(PEA.intEmpId,0) AS empId,
							PP.strIdNo,
							ME.Photo
							FROM $currDB.plan_printer PP
							INNER JOIN $currDB.plan_employeetype PET ON PET.intId=PP.intEmployeeType
							LEFT JOIN $currDB.plan_employeeallocation PEA ON PEA.intEmpId=PP.intId AND
							PP.intLocationId=PEA.intLocationId AND
							PP.intCompanyId=PEA.intCompanyId AND
							PEA.dtmDate='$date'
							LEFT JOIN $HRDB.mst_employee ME ON ME.strNIC = PP.strIdNo   
							LEFT JOIN $HRDB.trn_attendance ATTN ON ATTN.dtDate=PEA.dtmDate  AND  ATTN.intEmployeeId=ME.intEmployeeId
							WHERE PP.intCompanyId='$companyId' AND
							PP.intLocationId='$locationId' AND
							PP.intStatus=1 AND 
							PP.intId NOT IN (SELECT DISTINCT PEA.intEmpId
							FROM $currDB.plan_employeeallocation PEA
							WHERE PEA.dtmDate='$date' AND
							PEA.intCompanyId='$companyId' AND
							PEA.intLocationId='$locationId' AND
							PEA.intTeam<>'$teamId')
							ORDER BY PP.intId";
					
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
			  
			  ?>
            	<tr class="normalfnt" id="<?Php echo $row['intId']; ?>">
              		<td align="center" bgcolor="#FFFFFF"><input class="clsPrinters" type="checkbox" id="chkPrinter" <?php echo ($row['empId']!=0?'checked="checked"':'') ?> /></td>
              		<td bgcolor="#FFFFFF" class="clsPrinterName" ><?Php echo $row['strPrinterName']; ?></td>
              		<td bgcolor="#FFFFFF" class="clsPrinterType" id="<?php echo $row['intEmployeeType']; ?>" ><?php echo $row['strEmployeeType']; ?></td>
              		<td bgcolor="#FFFFFF" style="text-align:center"><?php echo($row['Photo']!=""?'<img src="../'.$backwardseperator.'qpay/'.$row['Photo'].'" width="60" height="60" />':'<img src="../'.$backwardseperator.'qpay/images/User.png" width="60" height="60" />'); ?></td>
              		</tr>
              
              <?php
					}
			  ?>
            </table>
          </div></td>
      </tr>
      <tr>
        <td colspan="5" align="center" class="tableBorder_allRound"><img src="../../../../images/Tadd.jpg" width="92" height="24"  alt="add" id="butAdd" name="butAdd" class="mouseover"/><img src="../../../../images/Tclose.jpg" width="92" height="24" id="butClose1" name="butClose1" class="mouseover"  /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>

</body>
</html>

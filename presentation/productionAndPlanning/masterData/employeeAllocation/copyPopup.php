<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include $backwardseperator."dataAccess/Connector.php";

$locationId	= $_SESSION["CompanyID"];
$companyId	= $_SESSION["headCompanyId"];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Copy Employees</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

</head>

<body>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>   
<form id="frmCopyPopUp" name="frmCopyPopUp" method="post" action="">
<div align="center">
		<div class="trans_layoutS" style="width:450px">
		  <div class="trans_text"> Copy Employees</div>
         <table width="450" border="0" align="center" bgcolor="#FFFFFF">
         <tr>
            <td><table width="100%" border="0">
         <tr>
           <td width="24%" class="normalfnt">Allocated Date</td>
           <td width="34%" class="normalfnt"><input name="dtAllocatedDate" type="text" value="<?php echo date('Y-m-d'); ?>" class="txtbox" id="dtAllocatedDate" style="width:120px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
           <td width="8%" class="normalfnt">&nbsp;</td>
           <td width="34%" class="normalfnt">&nbsp;</td>
           </tr>
         <tr>
           <td class="normalfnt">&nbsp;</td>
           <td class="normalfnt">&nbsp;</td>
           <td class="normalfnt">&nbsp;</td>
           <td class="normalfnt">&nbsp;</td>
           </tr>
         <tr>
           <td class="normalfnt">Copy Date From</td>
           <td class="normalfnt"><input name="dtCopyFromDate" type="text" value="<?php echo date('Y-m-d'); ?>" class="txtbox" id="dtCopyFromDate" style="width:120px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
           <td class="normalfnt">To</td>
           <td class="normalfnt"><input name="dtCopyToDate" type="text" value="<?php echo date('Y-m-d'); ?>" class="txtbox" id="dtCopyToDate" style="width:120px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
           </tr>
         <tr>
            <td colspan="4" class="normalfnt">&nbsp;</td>
            </tr>
      <tr>
        <td colspan="4" align="center" class="tableBorder_allRound"><img src="../../../../images/Tcopy.png" width="92" height="24"  alt="copy" id="butPopCopy" name="butPopCopy" class="mouseover"/><img src="../../../../images/Tclose.jpg" width="92" height="24" id="butClose1" name="butClose1" class="mouseover"  /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>

</body>
</html>

<?php
	session_start();
	include "../../../../dataAccess/Connector.php";
	$userId 	= $_SESSION['userId'];
	//$intPubCompanyId = $_SESSION["CompanyID"];
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$intPubCompanyId = $row['intCompanyId']; 
	}
	$locationId = $_SESSION["CompanyID"];
	
	$id=$_GET["id"];
	
	if($id=='save')
	{
	
		$intYear=$_GET["intYear"]; 
	    $intMonth=$_GET["intMonth"];
		$chkAllLocation = $_GET["chkAllLocation"];
		$status=$_GET["holidays"];

		$statusArr;
		$k=0;
		$tokn = strtok($status, ",");		
			
		while ($tokn != false)
		{		
			$statusArr[$k]=$tokn;
			$tokn = strtok(",");
		
			$k++;
		}
		if($chkAllLocation)
		{
			$sqlchk = "SELECT intId,intCompanyId FROM mst_locations WHERE intStatus = 1 ";
			$resultchk = $db->RunQuery($sqlchk);
			
			while($rowchk = mysqli_fetch_array($resultchk))
			{
				$intPubCompanyId = $rowchk['intCompanyId'];
				$locationId 	  = $rowchk['intId'];
				
				$sql="SELECT
				plan_teams.intId,
				plan_teams.intPlantId,
				plan_teams.intCompanyId,
				plan_teams.intLocationId,
				plan_teams.strName,
				plan_teams.intPrinter,
				plan_teams.intPasting,
				plan_teams.intRemoving,
				plan_teams.intHelper,
				plan_teams.intSetter,
				plan_teams.intQC,
				plan_teams.intChecker,
				plan_teams.intDrying,
				plan_teams.intIroning,
				plan_teams.intStroke,
				plan_teams.intColor,
				plan_teams.intPanel,
				plan_teams.dblCapacity,
				plan_teams.dblInTime,
				plan_teams.intStatus,
				plan_teams.dblOutTime,
				hour(timediff(plan_teams.dblInTime,plan_teams.dblOutTime)) AS hours
				FROM
				plan_teams
				WHERE
				plan_teams.intCompanyId =  '$intPubCompanyId' AND
				plan_teams.intLocationId =  '$locationId' AND
				plan_teams.intStatus =  '1'";
			
			$result = $db->RunQuery($sql);
			
			while($row = mysqli_fetch_array($result))
			{
					$companyId				=$row['intCompanyId']; 
					$teamId					=$row['intId']; 
					$workingHours			=$row['hours'];
					$dblWorkingHours_old 	=$row['hours'];
					$inTime					=$row['dblInTime'];
					$inTime_old				=$row['dblInTime'];
					$outTime				=$row['dblOutTime'];
					$outTime_old			=$row['dblOutTime'];
	
					for($i=1;$i<32;$i++)
					{
						$dtmDate=$intYear."-".$intMonth."-".$i; 
						$dayandstatus=explode('-',$statusArr[$i-1]);
						
						$strDayStatus=$dayandstatus[0]; 
				
						switch($dayandstatus[1]){
						
							case '1' :
								{
									$strDay="Monday";
									break;
								}
							case '2' :
								{
									$strDay="Tuesday";
									break;
								}
							case '3' :
								{
									$strDay="Wednesday";
									break;
								}
							case '4' :
								{
									$strDay="Thursday";
									break;
								}
							case '5' :
								{
									$strDay="Friday";
									break;
								}
							case '6' :
								{
									$strDay="Saturday";
									break;
								}
							case '0' :
								{
									$strDay="Sunday";
									break;
								}
							default :
								{
									break;
								}
						}
						
						$intDay=$i;
						
						switch($intMonth)
						{						
							case '1' :
								{
									$strMonth="January";
									break;
								}
							case '2' :
								{
									$strMonth="February";
									break;
								}
							case '3' :
								{
									$strMonth="March";
									break;
								}
							case '4' :
								{
									$strMonth="April";
									break;
								}
							case '5' :
								{
									$strMonth="May";
									break;
								}
							case '6' :
								{
									$strMonth="June";
									break;
								}
							case '7' :
								{
									$strMonth="July";
									break;
								}
							case '8' :
								{
									$strMonth="August";
									break;
								}
							case '9' :
								{
									$strMonth="September";
									break;
								}
							case '10' :
								{
									$strMonth="October";
									break;
								}
							case '11' :
								{
									$strMonth="November";
									break;
								}
							case '12' :
								{
									$strMonth="December";
									break;
								}
							default :
								{
									break;
								}
						}
						
							if($strDayStatus=='off')
							{
								$workingHours =0;
								$inTime = '00:00:00';
								$outTime = '00:00:00';
							}
							elseif($strDayStatus=='saturday')
							{
								$workingHours =4;
								$inTime = '09:30:00';
								$outTime = '12:30:00';
							}
							elseif($strDayStatus=='sunday')
							{
								$workingHours =0;
								$inTime = '00:00:00';
								$outTime = '00:00:00';
							}
							else
							{
								$workingHours=$dblWorkingHours_old;
								$inTime = $inTime_old;
								$outTime = $outTime_old;
							}
								
							$sql1 = "INSERT INTO `plan_calender` (`dtmDate`,`intCompanyId`,`intLocationId`,`intTeamId`,`strMonth`,`intYear`,`intMonth`,`intDay`,`strDay`,`strDayStatus`,`dblWorkingHours`,`dblInTime`,`dblOutTime`,`intCreator`,dtmCreateDate) 
						VALUES ('$dtmDate','$intPubCompanyId','$locationId','$teamId','$strMonth','$intYear','$intMonth','$intDay','$strDay','$strDayStatus','$workingHours', '$inTime','$outTime','$userId',now())";
						
						$result1 = $db->RunQuery($sql1);
						
					}
				}
				
			}
		}
		else
		{
			$sql="SELECT
			plan_teams.intId,
			plan_teams.intPlantId,
			plan_teams.intCompanyId,
			plan_teams.intLocationId,
			plan_teams.strName,
			plan_teams.intPrinter,
			plan_teams.intPasting,
			plan_teams.intRemoving,
			plan_teams.intHelper,
			plan_teams.intSetter,
			plan_teams.intQC,
			plan_teams.intChecker,
			plan_teams.intDrying,
			plan_teams.intIroning,
			plan_teams.intStroke,
			plan_teams.intColor,
			plan_teams.intPanel,
			plan_teams.dblCapacity,
			plan_teams.dblInTime,
			plan_teams.intStatus,
			plan_teams.dblOutTime,
			hour(timediff(plan_teams.dblInTime,plan_teams.dblOutTime)) AS hours
			FROM
			plan_teams
			WHERE
			plan_teams.intCompanyId =  '$intPubCompanyId' AND
			plan_teams.intLocationId =  '$locationId' AND
			plan_teams.intStatus =  '1'";
		
		$result = $db->RunQuery($sql);
		
		while($row = mysqli_fetch_array($result))
		{
				$companyId				=$row['intCompanyId']; 
				$teamId					=$row['intId']; 
				$workingHours			=$row['hours'];
				$dblWorkingHours_old 	=$row['hours'];
				$inTime					=$row['dblInTime'];
				$inTime_old				=$row['dblInTime'];
				$outTime				=$row['dblOutTime'];
				$outTime_old			=$row['dblOutTime'];

				for($i=1;$i<32;$i++)
				{
					$dtmDate=$intYear."-".$intMonth."-".$i; 
					$dayandstatus=explode('-',$statusArr[$i-1]);
					
					$strDayStatus=$dayandstatus[0]; 
			
					switch($dayandstatus[1]){
					
						case '1' :
							{
								$strDay="Monday";
								break;
							}
						case '2' :
							{
								$strDay="Tuesday";
								break;
							}
						case '3' :
							{
								$strDay="Wednesday";
								break;
							}
						case '4' :
							{
								$strDay="Thursday";
								break;
							}
						case '5' :
							{
								$strDay="Friday";
								break;
							}
						case '6' :
							{
								$strDay="Saturday";
								break;
							}
						case '0' :
							{
								$strDay="Sunday";
								break;
							}
						default :
							{
								break;
							}
					}
					
					$intDay=$i;
					
					switch($intMonth)
					{						
						case '1' :
							{
								$strMonth="January";
								break;
							}
						case '2' :
							{
								$strMonth="February";
								break;
							}
						case '3' :
							{
								$strMonth="March";
								break;
							}
						case '4' :
							{
								$strMonth="April";
								break;
							}
						case '5' :
							{
								$strMonth="May";
								break;
							}
						case '6' :
							{
								$strMonth="June";
								break;
							}
						case '7' :
							{
								$strMonth="July";
								break;
							}
						case '8' :
							{
								$strMonth="August";
								break;
							}
						case '9' :
							{
								$strMonth="September";
								break;
							}
						case '10' :
							{
								$strMonth="October";
								break;
							}
						case '11' :
							{
								$strMonth="November";
								break;
							}
						case '12' :
							{
								$strMonth="December";
								break;
							}
						default :
							{
								break;
							}
					}
					
						if($strDayStatus=='off')
						{
							$workingHours =0;
							$inTime = '00:00:00';
							$outTime = '00:00:00';
						}
						elseif($strDayStatus=='saturday')
						{
							$workingHours =4;
							$inTime = '09:30:00';
							$outTime = '12:30:00';
						}
						elseif($strDayStatus=='sunday')
						{
							$workingHours =0;
							$inTime = '00:00:00';
							$outTime = '00:00:00';
						}
						else
						{
							$workingHours=$dblWorkingHours_old;
							$inTime = $inTime_old;
							$outTime = $outTime_old;
						}
							
						$sql1 = "INSERT INTO `plan_calender` (`dtmDate`,`intCompanyId`,`intLocationId`,`intTeamId`,`strMonth`,`intYear`,`intMonth`,`intDay`,`strDay`,`strDayStatus`,`dblWorkingHours`,`dblInTime`,`dblOutTime`,`intCreator`,dtmCreateDate) 
					VALUES ('$dtmDate','$companyId','$locationId','$teamId','$strMonth','$intYear','$intMonth','$intDay','$strDay','$strDayStatus','$workingHours', '$inTime','$outTime','$userId',now())";
					
					$result1 = $db->RunQuery($sql1);
					
				}
			}
		}
		if($result1)
		{
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		echo json_encode($response);
	}
?>

<?php
session_start();
$backwardseperator 	= "../../../../../";
$thisFilePath 		= $_SERVER['PHP_SELF'];
$userId 			= $_SESSION['userId'];
$locationId	  		= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];

include  "{$backwardseperator}dataAccess/permisionCheck.inc";
include "../../../../../class/productionAndPlanning/masterData/teams/cls_teams_get.php";
include "../../../../../class/cls_commonFunctions_get.php";

$obj_team_get		= new team_get($db);
$obj_comFunc_get	= new cls_commonFunctions_get($db);

$programCode		= 'P0267';

$search_result		= $obj_team_get->loadPrintTypes();
$search_result1		= $obj_team_get->loadTeam();

$savedMode			= $obj_comFunc_get->Load_menupermision($programCode,$userId,'intAdd');
$deleteMode			= $obj_comFunc_get->Load_menupermision($programCode,$userId,'intDelete');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Production Teams</title>

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />  
<link href="../../../../../css/button.css" rel="stylesheet" type="text/css" /> 

</head>
<body>
<form id="frmTeams" name="frmTeams" method="post" autocomplete="off">
<table width="100%" border="0" align="center">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
        <script type="application/javascript" src="team_js.js"></script>   
	</tr> 
</table>
<div align="center">
    <div class="trans_layoutS">
   		<div class="trans_text">Production Teams</div>
            <table width="100%" border="0" align="center">
            	<tr>
            		<td width="62%">
                    <table width="100%" border="0" class="">
            			<tr>
            				<td class="normalfnt">&nbsp;</td>
            				<td class="normalfnt">Team</td>
            				<td width="309" colspan="2">
                            <select name="cboSearch" class="txtbox" id="cboSearch" style="width:250px" tabindex="1">
                            <option value=""></option>
                            <?php
					while($row = mysqli_fetch_array($search_result1))
					{
						echo "<option value=\"".$row["TEAM_ID"]."\">".$row["TEAM_NAME"]."</option>";
					}
				    ?>
                                   				
                            </select></td>
            			</tr>
           				<tr>
           					<td width="56" class="normalfnt">&nbsp;</td>
            				<td width="115" class="normalfnt">&nbsp;</td>
            				<td colspan="2">&nbsp;</td>
            			</tr>
            			<tr>
                            <td class="normalfnt">&nbsp;</td>
                            <td class="normalfnt">Name&nbsp;<span class="compulsoryRed">*</span></td>
                            <td colspan="2"><input name="txtName" type="text" class="validate[required,maxSize[50]]" id="txtName" style="width:250px" maxlength="50" tabindex="2"/></td>
            			</tr>
            			<tr>
            			  <td class="normalfnt">&nbsp;</td>
            			  <td class="normalfnt">Print Type&nbsp;<span class="compulsoryRed">*</span></td>
                          <td colspan="2"><select name="cboPrintType" class="validate[required]" id="cboPrintType" style="width:150px" tabindex="3" >
                    <option value=""></option>
                    <?php
					while($row = mysqli_fetch_array($search_result))
					{
						echo "<option value=\"".$row["intId"]."\">".$row["strName"]."</option>";
					}
				    ?>
                          </select></td>
          			  </tr>
            			<tr>
                            <td class="normalfnt">&nbsp;</td>
                        	<td class="normalfnt">Description</td>
                        	<td colspan="2"><textarea name="txtRemark" style="width:250px" class="validate[maxSize[250]]"  rows="2" id="txtRemark"  tabindex="4"></textarea></td>
            			</tr>
            			<tr>
                            <td class="normalfnt">&nbsp;</td>
                            <td class="normalfnt">Active</td>
                            <td><input type="checkbox" name="chkActive" id="chkActive" checked="checked" tabindex="5"/></td>
            			</tr>
            			<tr>
                            <td class="normalfnt">&nbsp;</td>
                            <td class="normalfnt">&nbsp;</td>
                            <td colspan="3" class="normalfnt">&nbsp;</td>
            			</tr>
            		</table>
            		</td>
            	</tr>
            	<tr>
            		<td height="34">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
            			<tr>
            				<td width="100%" align="center" bgcolor=""><a class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butSave" <?php echo($savedMode==1?'':'style="display:none"'); ?>  name="butSave">Save</a><a class="button white medium" id="butDelete" <?php echo($deleteMode==1?'':'style="display:none"'); ?> name="butDelete">Delete</a><a href="../../../../main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
            			</tr>
            		</table>
                    </td>
            	</tr>
            </table>
	</div>
</div>
</form>
</body>
</html>

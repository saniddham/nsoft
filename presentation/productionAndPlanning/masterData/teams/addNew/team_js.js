// JavaScript Document
$(document).ready(function(e) {
   $("#frmTeams").validationEngine();
   $("#frmTeams #butSave").live('click',saveTeam);
   $("#frmTeams #butNew").live('click',clearAll);
   $("#frmTeams #cboSearch").live('change',searchData);
   $("#frmTeams #butDelete").live('click',deleteData);
	 
});

function saveTeam()
{
	var searchId	= $('#cboSearch').val();
	var teamName	= $('#txtName').val();
	var printType	= $('#cboPrintType').val();
	var description	= $('#txtRemark').val();
	var status		= ($('#chkActive').attr('checked')?1:0);
	
	if($('#frmTeams').validationEngine('validate'))
	{
		var data = "requestType=saveData";
		var arrHeader = "{";
							arrHeader += '"searchId":"'+searchId+'",' ;
							arrHeader += '"teamName":"'+teamName+'",';
							arrHeader += '"printType":"'+printType+'",';
							arrHeader += '"description":"'+description+'",' ;
							arrHeader += '"status":"'+status+'"' ;
		
			arrHeader += "}";
		
		data+="&arrHeader="+arrHeader;
		var url = "teams_db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmTeams #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmTeams').get(0).reset();
						loadSearchCombo();
						loadPrintType();
						var t = setTimeout("alertx()",1000);
						return;
					}
					else
					{
						
					}
				},
				error:function(xhr,status){
						
						$('#frmTeams #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						return;
				}		
		});
	}
}

function searchData()
{
var searchId		= $('#cboSearch').val();
	if(searchId=='')
	{
		clearAll();
		return;
	}
	
	var data = "requestType=searchData";
	data	+= "&searchId="+searchId;
	
	var url  = "teams_db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					
					$('#txtName').val(json.teamName);
					$('#cboPrintType').val(json.printType);
					$('#txtRemark').val(json.description);
					$('#chkActive').attr('checked',json.status);
				}
		});	
}
function deleteData()
{
	var searchId	 = $('#cboSearch').val();
	if(searchId=='')
	{
		$('#frmTeams #butDelete').validationEngine('showPrompt', 'Please select Team.', 'fail');
		return;
	}
	var val = $.prompt('Are you sure you want to delete "'+$('#frmTeams #cboSearch option:selected').text()+'" ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
						var url = "teams_db.php";
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							data:'requestType=deleteTeam&searchId='+searchId,
							async:false,
							success:function(json){
								
								$('#frmTeams #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									$('#frmTeams').get(0).reset();
									loadSearchCombo();
									loadPrintType();
									var t=setTimeout("Deletex()",1000);
									return;
								}	
							}	 
						});
					}
					}
	});	

}
function loadSearchCombo()
{
	var url 	= "teams_db.php?requestType=loadTeam";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmTeams #cboSearch').html(httpobj.responseText);
}
function loadPrintType()
{
	var url 	= "teams_db.php?requestType=loadType";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmTeams #cboPrintType').html(httpobj.responseText);
}
function clearAll()
{
	$('#frmTeams').get(0).reset();
	loadSearchCombo();
}
function alertx()
{
	$('#frmTeams #butSave').validationEngine('hide')	;
}
function Deletex()
{
	$('#frmTeams #butDelete').validationEngine('hide')	;
}
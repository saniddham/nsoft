<?php
session_start();
$backwardseperator 		= "../../../../../";
$userId 				= $_SESSION['userId'];
$locationId	  			= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];

$requestType 			= $_REQUEST['requestType'];
$programCode			= 'P0267';
$savedMasseged			= "";
$error_sql				= "";
$savedStatus			= true;
$editMode				= false;

include_once "../../../../../dataAccess/Connector.php";
include_once "../../../../../class/cls_commonFunctions_get.php";
include_once "../../../../../class/productionAndPlanning/masterData/teams/cls_teams_get.php";
include_once "../../../../../class/productionAndPlanning/masterData/teams/cls_teams_set.php";

$obj_comfunc_get		= new cls_commonFunctions_get($db);
$obj_teams_get			= new team_get($db);
$obj_teams_set			= new teams_set($db);

if($requestType=='saveData')
{

	$arrHeader 		= json_decode($_REQUEST['arrHeader'],true);
	$searchId		= $arrHeader['searchId'];
	$teamName		= $arrHeader['teamName'];
	$printType		= $arrHeader['printType'];
	$description	= $arrHeader['description'];
	$status			= $arrHeader['status'];
	
	$db->begin();
	$savePermission	= $obj_comfunc_get->Load_menupermision2($programCode,$userId,'intAdd');
	if($savePermission==0 && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged  = 'No permission to save.';
	}
	if($searchId=='')
	{	
		$dataArr		= $obj_teams_set->saveTeam($teamName,$printType,$description,$status,$companyId,$locationId,$userId);
			if($dataArr['savedStatus']=='fail' && ($savedStatus))
			{
				$savedStatus	= false;
				$savedMasseged	= $dataArr['savedMassege'];
				$error_sql		= $dataArr['error_sql'];
			}	
	}
	else
	{
	$editMode	= true;
	$dataArr		= $obj_teams_set->updateTeam($searchId,$teamName,$printType,$description,$status,$companyId,$locationId,$userId);
	
	if($dataArr['savedStatus']=='fail' && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged	= $dataArr['savedMassege'];
			$error_sql		= $dataArr['error_sql'];
		}
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		if($editMode)
			$response['msg']	= "Updated Successfully.";
		else
			$response['msg']	= "Saved Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);
	
}
else if($requestType =='searchData')
{
	$searchId	 = $_REQUEST['searchId'];
	
	$searchData = $obj_teams_get->searchTeam($searchId);
	
	$response['teamName'] 	= $searchData['TEAM_NAME'];
	$response['printType'] 	= $searchData['PRINT_TYPE_ID'];
	$response['description'] 	= $searchData['REMARKS'];
	$response['status'] 	= ($searchData['STATUS']==1?true:false);
	
	echo json_encode($response);	
}
else if($requestType=='deleteTeam')
{
	$searchId	 = $_REQUEST['searchId'];
	
	$db->begin();
	
	$deletePermission	= $obj_comfunc_get->Load_menupermision2($programCode,$userId,'intDelete');
	if($deletePermission==0 && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged  = 'No permission to delete.';
	}
	
	$dataArr		= $obj_teams_set->deleteTeam($searchId);
	if($dataArr['savedStatus']=='fail' && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged	= $dataArr['savedMassege'];
		$error_sql		= $dataArr['error_sql'];
	}
	
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg']		= "Deleted Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);
	

}
else if($requestType=='loadTeam')
{
	$result = $obj_teams_get->loadTeam();
	
	$html = "<option value=\"\"></option>";
	while($row = mysqli_fetch_array($result))
	{
		$html .= "<option value=\"".$row["TEAM_ID"]."\">".$row["TEAM_NAME"]."</option>";
	}
	echo $html;
}
else if($requestType=='loadType')
{
	$result = $obj_teams_get->loadPrintTypes();
	
	$html = "<option value=\"\"></option>";
	while($row = mysqli_fetch_array($result))
	{
		$html .= "<option value=\"".$row["intId"]."\">".$row["strName"]."</option>";
	}
	echo $html;
}
?>
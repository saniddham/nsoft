<?php
session_start();
$backwardseperator 	= "../../../";
$mainPath 			= $_SESSION['mainPath'];
$thisFilePath 		=  $_SERVER['PHP_SELF'];
$currDB 			= $_SESSION['Database'];
$HRDB 				= $_SESSION['HRDatabase'];

$groupId 	  = "";
$cboGroupId   = "";
include $backwardseperator."dataAccess/Connector.php";

$sql 			= "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result 		= $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId  = $row['intCompanyId']; 
}
$locationId 	= $_SESSION["CompanyID"];

$teamId  		= $_REQUEST['teamId'];
$actualQty 		= $_REQUEST['actualQty'];
$plantId 		= $_REQUEST['plantId'];
$date	 		= trim($_REQUEST['date']); 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Employee's Efficiency</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>
<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>

<script>
	$('#cboPopTeam').live('change',function(){
		
		$("#tblSizesPopup2 tr:gt(0)").remove();
		var plantId = <?php echo $plantId; ?>;
		var date = '<?php echo $date; ?>';
		var url 	= "actualProduction-db-get.php?requestType=loadPrinter";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"plantId="+plantId+"&date="+date+"&teamId="+$(this).val(),
			async:false,
			success:function(json){
				
				var lengthDetail   = json.arrDetailData.length;
				var arrDetailData  = json.arrDetailData;
				
				for(var i=0;i<lengthDetail;i++)
				{
					var EmpId		= arrDetailData[i]['EmpId'];	
					var EmpTypeId	= arrDetailData[i]['EmpTypeId'];
					var EmpType		= arrDetailData[i]['EmpType'];
					var PrinterName	= arrDetailData[i]['PrinterName'];
					var qpayEmpId	= arrDetailData[i]['qpayEmpId'];
					
					var content='<tr class="normalfnt"><td align="center" '+(qpayEmpId==0?'style="color:#C00"':'')+' class="clsEmpName" id='+EmpId+'>'+PrinterName+'</td>';
					content+='<td align="center" '+(qpayEmpId==0?'style="color:#C00"':'')+' class="clsEmpType" id='+EmpTypeId+'>'+EmpType+'</td>'
					content +='<td align="center" ><input name="txtPActQty" id="txtPActQty" class="clsPActQty" type="text" style="width:150px;text-align:center" onkeyup="setQty(this);" '+(qpayEmpId==0?'disabled="disabled"':"")+' /></td></tr>';
					
					add_new_row('#frmPrinterPopup #tblSizesPopup2',content);
				}
			}
		});
	});
</script>

</head>

<body>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>

    
<form id="frmPrinterPopup" name="frmPrinterPopup" method="post" action="">
<div align="center">
		<div class="trans_layoutD" style="width:450px">
		  <div class="trans_text" style="width:450px"> Employee's Efficiency</div>
		  <table width="450" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="450" border="0">
      <tr>
      <td width="50" class="normalfnt">Machine</td>
      <td width="107" class="normalfnt"><select name="cboPopTeam" id="cboPopTeam" style="width:100px">
      <option value=""></option>
      <?php
		$sql = "SELECT DISTINCT intId,strName FROM plan_teams
				WHERE intCompanyId= '$companyId' AND
				intLocationId = '$locationId' AND
				intPlantId = '$plantId' AND
				intStatus = '1' ";
		$result = $db->RunQuery($sql);
		while($rowTeam = mysqli_fetch_array($result))
		{
			if($teamId==$rowTeam['intId'])
				echo "<option value=\"".$rowTeam['intId']."\" selected=\"selected\">".$rowTeam['strName']."</option>";
			else
				echo "<option value=\"".$rowTeam['intId']."\">".$rowTeam['strName']."</option>";
		}
	  ?>
      </select></td>
      <td width="60" class="normalfnt">Actual Qty</td>
      <td width="82" class="normalfnt" ><input type="text" name="txtActQty" id="txtActQty" style="width:80px" disabled="disabled" value="<?php echo $actualQty; ?>"/></td>
      <td width="49" class="normalfnt" >Bal Qty</td>
      <td width="82" class="normalfnt" ><input type="text" name="txtBalQty" id="txtBalQty" style="width:80px" disabled="disabled" value="<?php echo $actualQty; ?>"/></td>
      </tr>
      <tr>
        <td colspan="2" class="normalfnt">&nbsp;</td>
        <td colspan="2" class="normalfnt">&nbsp;</td>
        <td class="normalfnt" style="text-align:right">&nbsp;</td>
        <td class="normalfnt" style="text-align:right">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="6"><div style="width:450px;height:200px;" >
          <table width="450" class="bordered" id="tblSizesPopup2" >
          <thead>
            <tr>
              <th width="37%" height="22" >Employee Name</th>
              <th width="25%" >Type</th>
              <th width="38%" height="22" >Actual Qty</th>
            </tr>
          </thead>
          <tbody id="tbPrinter">
          <?php
		  $sql = " SELECT PEA.intEmpId,
					PEA.intEmpType,
					PP.strPrinterName,
					PET.strEmployeeType,
					PP.strIdNo,
					IFNULL(ATTN.intEmployeeId,0) AS qpayEmpId  
					FROM $currDB.plan_employeeallocation PEA
					INNER JOIN $currDB.plan_printer PP ON PP.intId=PEA.intEmpId
					INNER JOIN $currDB.plan_employeetype PET ON PET.intId=PEA.intEmpType 	
					LEFT JOIN $HRDB.mst_employee ME ON ME.strNIC = PP.strIdNo   
					LEFT JOIN $HRDB.trn_attendance ATTN ON ATTN.dtDate=PEA.dtmDate  AND  ATTN.intEmployeeId=ME.intEmployeeId
					WHERE PEA.intCompanyId='$companyId' AND
					PEA.intLocationId='$locationId' AND
					PEA.intTeam='$teamId' AND
					PEA.dtmDate='$date' AND
					PEA.intEmpType=1 ";
			
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
			?>
            	<tr class="normalfnt">
            	<td align="center" <?php echo($row['qpayEmpId']==0?'style="color:#C00"':'');  ?> class="clsEmpName" id="<?php echo $row['intEmpId']; ?>"><?php echo $row['strPrinterName']; ?></td>
            	<td align="center" <?php echo($row['qpayEmpId']==0?'style="color:#C00"':'');  ?> class="clsEmpType" id="<?php echo $row['intEmpType']; ?>"><?php echo $row['strEmployeeType']; ?></td>
                <td align="center" > <input name="txtPActQty" id="txtPActQty" class="clsPActQty" type="text" style="width:150px;text-align:center" onkeyup="setQty(this);" <?php echo($row['qpayEmpId']==0?'disabled="disabled"':'');  ?> /></td>
                </tr>
            <?php
			}
		  ?>
          </tbody>
          </table>
          </div></td>
      </tr>
		<br />
      <tr>
        <td colspan="6" align="center" class="tableBorder_allRound"><img src="../../../images/Tadd.jpg" width="92" height="24"  alt="add" id="butAdd1" name="butAdd" class="mouseover"/><img src="../../../images/Tclose.jpg" width="92" height="24" id="butClose1" name="butClose1" class="mouseover"  /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>

</body>
</html>

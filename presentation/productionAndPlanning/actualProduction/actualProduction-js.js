// JavaScript Document
var arrPrinter = [];
$(document).ready(function(){
	$("#frmActualProduction").validationEngine();
	$("#frmPrinterPopup").validationEngine();
	
	$('#cboYear').live('change',function(){
		
		if($('#cboMonth').val()!="")
		{
			$('#cboWeek').removeAttr("disabled"); 
		}
		if($('#cboYear').val()=="")
		{
			$('#cboWeek').attr("disabled", "disabled");
			$('#cboWeek').val(''); 
		}
		
	});
	$('#cboMonth').live('change',function(){
		
		if($('#cboYear').val()!="")
		{
			$('#cboWeek').removeAttr("disabled"); 
		}
		if($('#cboMonth').val()=="")
		{
			$('#cboWeek').attr("disabled", "disabled");
			$('#cboWeek').val('');  
		}
	});	
	$('#cboMachinePlant').live('change',function(){
		
		var url		= "actualProduction-db-get.php?requestType=loadTeam&plantId="+$(this).val();
		var httpObj = $.ajax({url:url,async:false})
		$('#frmActualProduction #cboTeam').html(httpObj.responseText);
		document.getElementById('frmActualProduction').submit();
	});
	$('.insertPrinter').live('click',function(){
		
		if($("#frmActualProduction").validationEngine('validate'))
		{
			var MainteamId  = $(this).parent().parent().find('.clsTeam').attr('id');
			var MainPlantId = $('#cboMachinePlant').val(); 
			var rowId	    = $(this).parent().parent().index();
			
			if($(this).parent().parent().find('.clsActualQty').val()=='' || $(this).parent().parent().find('.clsActualQty').val()==0 )
			{
				$(this).parent().parent().find('.clsActualQty').validationEngine('showPrompt', 'Please enter a value.','fail');
				var t=setTimeout("alertx2()",1500);	
				return;	
			}
			else
			{
				var actualQty = parseFloat($(this).parent().parent().find('.clsActualQty').val());
				var date = $(this).parent().parent().find('.clsDate').html();
				loadPopup(MainteamId,MainPlantId,rowId,actualQty,date,this);
			}
		}
	});
	$('#butView').live('click',function(){
			
		document.getElementById('frmActualProduction').submit();	
	});
	/*$('#butSave').live('click',function(){
	
		if($("#frmActualProduction").validationEngine('validate'))
		{
			var value="[ ";
			$('.clsActualQty').each(function(){
			
				var planDetailId = $(this).parent().parent().attr('id');
				var actualQty	 = $(this).val();
				var stripId		 = $(this).parent().parent().find('.planStrip').attr('id');
	
				if(actualQty!="")
					value +='{"planDetailId":"'+planDetailId+'","actualQty":"'+actualQty+'","stripId":"'+stripId+'"},' ;		
			});
			value = value.substr(0,value.length-1);
			value += " ]";
			if(value!="[ ]")
			{
				var url = "actualProduction-db-set.php?requestType=saveData";
							$.ajax({
									url:url,
									async:false,
									dataType:'json',
									type:'post',
									data:'&actualQtyDetails='+value,
								success:function(json){
									$('#frmActualProduction #butSave').validationEngine('showPrompt', json.msg,json.type);
									if(json.type=='pass')
									{
										var t=setTimeout("alertx()",1000);
										setTimeout("window.location.href=window.location.href;",1000)
										return;
									}
								},
								error:function(){
									
									$('#frmActualProduction #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
									var t=setTimeout("alertx()",1000);	
									return;					
								}
							});
			}
			else
			{
				$('#frmActualProduction #butSave').validationEngine('showPrompt','No record to Save' , 'fail');
				var t=setTimeout("alertx()",1000);	
				return;
			}
		}
	});*/
	$('.clsActualQty').live('change',function(){
		
		var rowId = $(this).parent().parent().index();
		delete arrPrinter[rowId];
	});
	$('.clsSave').live('click',function(){
		
		var rowId = $(this).parent().parent().index();
		if (typeof arrPrinter[rowId] == "undefined") 
		{
			$(this).parent().parent().find('.insertPrinter').validationEngine('showPrompt', 'Please set Printer Efficiency.','fail');
			var t=setTimeout("alertx4()",1500);	
			return;
		}
		else
		{
			var obj 		 = this;
			if($("#frmActualProduction").validationEngine('validate'))
			{
				
				var actualQty 	 = $(this).parent().parent().find('.clsActualQty').val();
				var planDetailId = $(this).parent().parent().attr('id');
				var plantId		 = $('#cboMachinePlant').val();
				var date		 = $(this).parent().parent().find('.clsDate').html();
				var team		 = $(this).parent().parent().find('.clsTeam').attr('id');
				
				var value="[ ";
				for(n in arrPrinter[rowId][team])
				{
					var empId 	 = arrPrinter[rowId][team][n]['empId'];
					var popTeam	 = arrPrinter[rowId][team][n]['teamId'];
					var empType	 = arrPrinter[rowId][team][n]['type'];
					var qty		 = arrPrinter[rowId][team][n]['qty'];
					value +='{"empId":"'+empId+'","popTeam":"'+popTeam+'","empType":"'+empType+'","qty":"'+qty+'"},' ;
				}
				value = value.substr(0,value.length-1);
				value += " ]";
				
				var url = "actualProduction-db-set.php?requestType=saveData";
				$.ajax({
						url:url,
						async:false,
						dataType:'json',
						type:'post',
						data:'&plantId='+plantId+'&actualQty='+actualQty+'&planDetailId='+planDetailId+'&date='+date+'&empQtyDetails='+value,
					success:function(json){
						$(obj).validationEngine('showPrompt', json.msg,json.type);
						if(json.type=='pass')
						{
							var t=setTimeout("alertx5()",1000);
							$(obj).parent().parent().find('.clsActQty').html(json.actQty);
							$(obj).parent().parent().find('.clsActualQty').val('');
							delete arrPrinter[planDetailId];
							return;
						}
					},
					error:function(){
						
						$('#frmActualProduction #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertx()",1000);	
						return;					
					}
				});
			}
		}
		
	});
});
function alertx()
{
	$('#frmActualProduction #butSave').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmActualProduction #txtActualQty').validationEngine('hide')	;
}
function alertx3()
{
	$('#frmPrinterPopup #butAdd1').validationEngine('hide')	;
}
function alertx4()
{
	$('.insertPrinter').validationEngine('hide')	;
}
function alertx5()
{
	$('.clsSave').validationEngine('hide')	;
}
function chekQty(obj)
{
	var planQty   = parseFloat($(obj).parent().parent().find('.clsQty').html());
	var ActualQty = parseFloat($(obj).parent().parent().find('.clsActQty').html());
	var balQty    = Math.round((planQty-ActualQty)*10)/10;
	
	if(obj.value>balQty)
	{
		obj.value = parseFloat(balQty);
	}
}
function loadPopup(MteamId,plantId,rowId,actualQty,date,obj)
{
	var chkTotQty = 0;
	popupWindow3('1');
	$('#popupContact1').load('printerPopup.php?teamId='+MteamId+'&actualQty='+actualQty+'&date='+date+'&plantId='+plantId,function(){
			$('#butAdd1').click(function(){
					
				var balQty = $('#txtBalQty').val();
				if(balQty!=0)
				{
					$('#frmPrinterPopup #butAdd1').validationEngine('showPrompt','There are some balance quantity.', 'fail');
					var t=setTimeout("alertx3()",1500);
					return;
				}
				else
				{
					//var arrPrinterGroup = [];
					var arrMain   = [];
					var teamId = $('#cboPopTeam').val();
					arrMain['id'] = rowId;
					var arrMainNew = [];
					
					var arrGroupPrinter = [];
					arrGroupPrinter['id'] = MteamId;
					var arrGroupPrinterNew = [];
					
					$('.clsPActQty').each(function(){
						
						var teamId    = $('#cboPopTeam').val();
						var empId 	  = $(this).parent().parent().find('.clsEmpName').attr('id');
						var empType	  = $(this).parent().parent().find('.clsEmpType').attr('id');
						var qty       = ($(this).val()==''?0:$(this).val());
						if(qty!=0)
						{
							var arrPrinter = [];
							arrPrinter['empId']     = parseFloat(empId);
							arrPrinter['qty']		= parseFloat(qty);
							arrPrinter['type']		= parseFloat(empType);
							arrPrinter['teamId']		= parseFloat(teamId);
							arrGroupPrinterNew[arrPrinter['empId']] = arrPrinter;
						}
						
					});
						arrMainNew[arrGroupPrinter['id']] = arrGroupPrinterNew;
						arrPrinter[arrMain['id']] = arrMainNew;
						disablePopup();
				}
				
			});
			$('#butClose1').click(disablePopup);
		});
}
function setQty(obj)
{
	var totActQty = 0;
	var actualQty = parseFloat($('#txtActQty').val());
	$('.clsPActQty').each(function(){
		
		
		if($(this).val()!="")
			totActQty += parseFloat($(this).val());
	});
	$('#txtBalQty').val(parseFloat(actualQty)-parseFloat(totActQty));
	
	if(totActQty>actualQty)
	{
		obj.value = parseFloat(obj.value)-parseFloat(parseFloat(totActQty)-parseFloat(actualQty));
		$('#txtBalQty').val(0);
	}
	
	
}
function add_new_row(table,rowcontent)
{
	if ($(table).length>0)
	{
		if ($(table+' > tbody').length==0) $(table).append('<tbody />');
		($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
	}
}
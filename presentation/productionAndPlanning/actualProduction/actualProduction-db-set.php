<?php 
	session_start();
	$backwardseperator 	= "../../../";
	$mainPath 			= $_SESSION['mainPath'];
	$userId 	 		= $_SESSION['userId'];
	$requestType 		= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	$sql 		= "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]." ";
	$result 	= $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId  = $row['intCompanyId']; 
	}
	$locationId 	= $_SESSION["CompanyID"];
	
	$requestType 	= $_REQUEST['requestType'];
	$PQtyDetails 	= json_decode($_REQUEST['empQtyDetails'], true);
	
	if($requestType=='saveData')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$planDetailId 	= $_REQUEST['planDetailId'];
		$plantId 		= $_REQUEST['plantId'];
		$actualQty 		= $_REQUEST['actualQty'];
		$date 			= trim($_REQUEST['date']);
		$rollBackFlag 	= 0;
		$saved			= 0;
		$toSave			= 0;
		
		$sql = "UPDATE plan_upload_details 
				SET dblActualQty = dblActualQty+$actualQty
				WHERE intPrePlanId = '$planDetailId' and strDate='$date' ;";
		$finalResult = $db->RunQuery2($sql);
		if(!$finalResult)
		{
			$rollBackFlag = 1;
			$rollBackMsg  = $db->errormsg;
		}
		
		foreach($PQtyDetails as $arrDetails)
		{
			$empId 		= $arrDetails['empId'];
			$Team 		= $arrDetails['popTeam'];
			$empType 	= $arrDetails['empType'];
			$qty 		= $arrDetails['qty'];
			
			$sql = "SELECT 	* FROM plan_actual_details
					WHERE dtmDate='$date' AND
					intPlanDetailId='$planDetailId' AND
					intTeamId='$Team' AND
					intEmpId='$empId' AND
					intCompanyId='$companyId' AND
					intLocationId='$locationId' ";
			
			$result = $db->RunQuery2($sql);
			$count  = mysqli_num_rows($result);
			if($count > 0)
			{
				$sql = "UPDATE plan_actual_details 
						SET  
						dblQty = dblQty + $qty
						WHERE dtmDate='$date' AND
						intPlanDetailId='$planDetailId' AND
						intTeamId='$Team' AND
						intEmpId='$empId' AND
						intCompanyId='$companyId' AND
						intLocationId='$locationId' ";
			}
			else
			{
				$sql = "INSERT INTO plan_actual_details 
						(
						dtmDate, 
						intPlanDetailId, 
						intPlantId, 
						intTeamId, 
						intCompanyId, 
						intLocationId, 
						intEmpId, 
						intEmpType, 
						dblQty
						)
						VALUES
						( 
						'$date', 
						'$planDetailId', 
						'$plantId', 
						'$Team', 
						'$companyId', 
						'$locationId', 
						'$empId', 
						'$empType', 
						'$qty'
						) ";
			}
			$finalResult = $db->RunQuery2($sql);
			if(!$finalResult)
			{
				$rollBackFlag = 1;
				$rollBackMsg  = $db->errormsg;
			}
			else
			{
				$saved++;
			}
			$toSave++;
		}	
		$sqlchk = "SELECT dblActualQty FROM plan_upload_details WHERE intPrePlanId='$planDetailId' and strDate='$date' ";
		$resultChk = $db->RunQuery2($sqlchk);
		$rowChk = mysqli_fetch_array($resultChk);
		$totActualQty = $rowChk['dblActualQty'];
		
		if($rollBackFlag==1)
		{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
		}
		else if(($rollBackFlag==0) && ($saved==$toSave))
		{
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
			$response['actQty'] 	= $totActualQty;
		}
		else
		{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
		}
		$db->CloseConnection();	
		echo json_encode($response);
	}

?>
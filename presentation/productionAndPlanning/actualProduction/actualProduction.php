<?php
session_start();
$backwardseperator = "../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";
$sql 		= "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result 	= $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId  = $row['intCompanyId']; 
}
$locationId 	= $_SESSION["CompanyID"];

if($_SERVER['REQUEST_METHOD'] == "POST")
{
	$cboYear 			= $_REQUEST['cboYear'];
	$cboMonth 		 	= $_REQUEST['cboMonth'];
	$cboMachinePlant 	= $_REQUEST['cboMachinePlant'];
	$cboWeek 			= $_REQUEST['cboWeek'];
	$cboTeam 			= $_REQUEST['cboTeam'];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Actual Production</title>

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>
<script type="application/javascript" src="actualProduction-js.js"></script>

<link rel="stylesheet" href="../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../libraries/validate/template.css" type="text/css">

</head>

<body>
<form id="frmActualProduction" name="frmActualProduction" autocomplete="off" action="actualProduction.php" method="post">
<table width="100%" border="0" align="center">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div align="center">
<div class="trans_layoutL" style="width:1100px">
<div class="trans_text">Actual Production</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
    	<td>
        	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
              <td height="25" class="normalfnt"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="normalfnt" width="11%">Year</td>
                  <td class="normalfnt" width="13%"><select name="cboYear" id="cboYear" style="width:100px">
                    <option value=""></option>
                    <?php
		$sql ="SELECT DISTINCT YEAR(strDate) AS intYear FROM plan_upload_details";
		$result = $db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			if($cboYear==$row['intYear'])
				echo "<option value=\"".$row["intYear"]."\" selected=\"selected\" >".$row["intYear"]."</option>";
			else
				echo "<option value=\"".$row["intYear"]."\" >".$row["intYear"]."</option>";
		}
	  ?>
                  </select></td>
                  <td class="normalfnt" width="7%">Month</td>
                  <td class="normalfnt" width="16%"><select name="cboMonth" id="cboMonth" style="width:108px">
                    <option value=""></option>
                    <?php
				$sql ="SELECT intMonthId,strMonth FROM mst_month ORDER BY intMonthId";
				$result = $db->RunQuery($sql);
				while($row = mysqli_fetch_array($result))
				{
					if($cboMonth==$row['intMonthId'])
						echo "<option value=\"".$row["intMonthId"]."\" selected=\"selected\" >".$row["strMonth"]."</option>";
					else
						echo "<option value=\"".$row["intMonthId"]."\" >".$row["strMonth"]."</option>";
				}
			  ?>
                  </select></td>
                  <td class="normalfnt" width="11%">Machine Plant</td>
                  <td width="31%" class="normalfnt"><select name="cboMachinePlant" id="cboMachinePlant" style="width:230px" class="validate[required]">
                    <option value=""></option>
                    <?php
				$sql ="SELECT intId,strName FROM mst_printertypes WHERE intStatus=1 order by strName ";
				$result = $db->RunQuery($sql);
				while($row = mysqli_fetch_array($result))
				{
					if($cboMachinePlant==$row['intId'])
						echo "<option value=\"".$row["intId"]."\" selected=\"selected\" >".$row["strName"]."</option>";
					else
						echo "<option value=\"".$row["intId"]."\" >".$row["strName"]."</option>";
				}
			  ?>
                  </select></td>
                  <td class="normalfnt" width="11%">&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td width="23%" height="25" class="normalfnt"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="normalfnt" width="11%">Week</td>
                  <td class="normalfnt" width="13%"><select name="cboWeek" id="cboWeek" style="width:100px" <?php echo ($cboYear!="" && $cboMonth!=""?'':'disabled="disabled"')?>>
                    <option value=""></option>
                    <option value="1" <?php echo ($cboWeek==1?'selected="selected"':"") ?>>1st Week</option>
                    <option value="2" <?php echo ($cboWeek==2?'selected="selected"':"") ?>>2nd Week</option>
                    <option value="3" <?php echo ($cboWeek==3?'selected="selected"':"") ?>>3rd Week</option>
                    <option value="4" <?php echo ($cboWeek==4?'selected="selected"':"") ?>>4th Week </option>
                  </select></td>
                  <td class="normalfnt" width="7%">Team</td>
                  <td class="normalfnt" width="16%"><select name="cboTeam" id="cboTeam" style="width:108px">
                  <option value=""></option>
                  <?php
				  $sql = "SELECT DISTINCT intId,strName FROM plan_teams
							WHERE intCompanyId= '$companyId' AND
							intLocationId = '$locationId' AND
							intPlantId = '$cboMachinePlant' AND
							intParentTeamId = 0";
					
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($cboTeam==$row['intId'])
							echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
					}
				  ?>
                  </select></td>
                  <td class="normalfnt" width="11%"><span style="display:none">Sales OrderNo</span></td>
                  <td width="31%" class="normalfnt"><select name="cboSalesOrderNo" id="cboSalesOrderNo" style="width:230px;display:none">
                    <option value=""></option>
                    <?php
				$sql ="SELECT DISTINCT strSalesOrder FROM plan_stripes ORDER BY strSalesOrder";
				$result = $db->RunQuery($sql);
				while($row = mysqli_fetch_array($result))
				{
					echo "<option value=\"".$row["strSalesOrder"]."\" >".$row["strSalesOrder"]."</option>";
				}
			  ?>
                  </select></td>
                  <td class="normalfnt" width="11%"><span style="text-align:right"><img border="0" src="../../../images/Tview.jpg" alt="View" name="butView" class="mouseover" id="butView" tabindex="28"/></span></td>
                </tr>
              </table></td>
              </tr>
            <tr>
            	<td colspan="2"><div style="height:350px;overflow:scroll" >
                <table width="100%" class="bordered" id="tblSizesPopup" >
                    <thead>
                     <tr>
                       <th width="7%" height="19" >Date</th>
                       <th width="10%" height="19" >Style No</th>
                       <th width="9%" height="19" >Order No</th>
                       <th width="10%" height="19" >Sales Order No</th>
                       <th width="10%" height="19" >Graphic No</th>
                       <th width="10%" height="19" >Customer PONo</th>
                       <th width="7%" height="19" >Team</th>
                       <th width="6%" height="19" >Planed Qty</th>
                       <th width="6%" height="19" >Actual Qty</th>
                       <th width="7%" height="19" >Employee</th>
                       <th width="10%" height="19" >Actual Qty</th>
                       <th width="8%" height="19" >&nbsp;</th>
                     </tr>
                    </thead>
                    <tbody>
                    <?php
					$sql = "SELECT  PUD.intPrePlanId,
							DATE(PUD.strDate) AS dtmDate,
							PUD.dblNoOfStrokes,
							PUD.dblActualQty,
							PUH.strCustomerPONo,
							PUH.strStyleNo,
							PUH.intTeamId,
							PT.strName,
							TOH.intOrderNo,
							TOD.strSalesOrderNo,
							TOD.strGraphicNo
							FROM plan_upload_details PUD
							INNER JOIN plan_upload_header PUH ON PUH.intId=PUD.intPrePlanId
							INNER JOIN plan_teams PT ON PT.intId = PUH.intTeamId
							INNER JOIN trn_orderheader TOH ON TOH.strCustomerPoNo=PUH.strCustomerPONo
							INNER JOIN trn_orderdetails TOD ON TOH.intOrderNo=TOD.intOrderNo AND TOH.intOrderYear=TOD.intOrderYear
							WHERE PUD.dblNoOfStrokes <>IFNULL(PUD.dblActualQty,0) ";
					if($cboYear!="")
						$sql.="AND YEAR(PUD.strDate)='$cboYear' ";
					if($cboMonth!="")
						$sql.="AND MONTH(PUD.strDate) = '$cboMonth' ";
					if($cboMachinePlant!="")
						$sql.="AND PUH.intPlantId='$cboMachinePlant' ";
					if($cboTeam!="")
						$sql.="AND PUH.intTeamId = '$cboTeam' ";
					if($cboWeek!="")
					{
						$sql.="AND PUD.strDate BETWEEN (SELECT dtmFromDate
														FROM plan_week_setup 
														WHERE intyear='$cboYear' AND
														intMonth = '$cboMonth' AND
														intWeekId = '$cboWeek') 
												AND    (SELECT dtmToDate
														FROM plan_week_setup 
														WHERE intyear='$cboYear' AND
														intMonth = '$cboMonth' AND
														intWeekId = '$cboWeek') ";
					}				
					$sql.="AND PUH.intLocationId='$locationId' AND TOH.intStatus=1 ORDER BY PUD.intPrePlanId,PUD.strDate";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
					?>
                     <tr bgcolor="#FFFFFF" class="normalfnt" id="<?php echo $row['intPrePlanId'];?>">
                       <td align="center" width="7%" class="clsDate"><?php echo $row['dtmDate']; ?></td>
                       <td align="center" width="10%" class="clsStyleNo"><?php echo $row['strStyleNo']; ?></td>
                       <td align="center" width="9%" ><?php echo $row['intOrderNo']; ?></td>
                       <td align="center" width="10%" ><?php echo ($row['strSalesOrderNo']==''?'&nbsp;':$row['strSalesOrderNo']); ?></td>
                        <td align="center" width="10%" ><?php echo ($row['strGraphicNo']==''?'&nbsp;':$row['strGraphicNo']); ?></td>
                         <td align="center" width="10%" ><?php echo ($row['strCustomerPONo']==''?'&nbsp;':$row['strCustomerPONo']); ?></td>
                       <td align="center" width="7%" class="clsTeam" id="<?php echo $row['intTeamId']; ?>" ><?php echo $row['strName']; ?></td>
                       <td align="right" width="6%" class="clsQty"><?php echo round($row['dblNoOfStrokes'],1); ?></td>
                       <td align="right" width="6%" class="clsActQty"><?php echo ($row['dblActualQty']==""?0:$row['dblActualQty']); ?></td>
                        <td align="center" width="7%" class="clsAddPrinters" ><img  src="../../../images/add_new.png" name="butAdd" width="15" height="15" class="mouseover insertPrinter" id="butAdd" /></td>
                       <td align="center" width="10%" ><input name="txtActualQty" id="txtActualQty" class="clsActualQty validate[custom[number]]" style="width:100px" type="text" onkeyup="chekQty(this);" /></td>
                       <td align="center"><?php echo($row['strCustomerPONo']==''?'&nbsp;':'<img border="0" src="../../../images/Tsave.jpg" alt="Save" name="butSave" height="20" class="mouseover clsSave" id="butSave" tabindex="24"/>');?></td>
                     </tr>
                    <?php
					}
					?>
                    </tbody>
                    </table>
                    </div>
                </td>
           	  </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td height="30" >
    		<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td align="center" class="tableBorder_allRound"><a href="../../../main.php"><img  src="../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
              </tr>
            </table>
        </td>
    </tr>
</table>
</div>
</div>
</form>
<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</body>
</html>
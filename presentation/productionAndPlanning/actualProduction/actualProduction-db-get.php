<?php 
session_start();
$backwardseperator 	= "../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 	 		= $_SESSION['userId'];
$requestType 		= $_REQUEST['requestType'];
$currDB 			= $_SESSION['Database'];
$HRDB 				= $_SESSION['HRDatabase'];

include "{$backwardseperator}dataAccess/Connector.php";

$sql 		= "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result 	= $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId  = $row['intCompanyId']; 
}
$locationId 	= $_SESSION["CompanyID"];

if($requestType=="loadTeam")
{
	$plantId 	= $_REQUEST['plantId'];
	
	$sql = "SELECT DISTINCT intId,strName FROM plan_teams
			WHERE intCompanyId= '$companyId' AND
			intLocationId = '$locationId' AND
			intPlantId = '$plantId' AND
			intParentTeamId = 0";
	
	$result = $db->RunQuery($sql);
	$option = "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$option.="<option value=\"".$row['intId']."\">".$row['strName']."</option>";
	}
	echo $option;
}
if($requestType=="loadPrinter")
{
	$teamId 	= $_REQUEST['teamId'];
	$date 		= $_REQUEST['date'];
	$plantId 	= $_REQUEST['plantId'];
	
	$sql = "SELECT PEA.intEmpId,
			PEA.intEmpType,
			PP.strPrinterName,
			PET.strEmployeeType,
			PP.strIdNo,
			IFNULL(ATTN.intEmployeeId,0) AS qpayEmpId  
			FROM $currDB.plan_employeeallocation PEA
			INNER JOIN $currDB.plan_printer PP ON PP.intId=PEA.intEmpId
			INNER JOIN $currDB.plan_employeetype PET ON PET.intId=PEA.intEmpType 	
			LEFT JOIN $HRDB.mst_employee ME ON ME.strNIC = PP.strIdNo   
			LEFT JOIN $HRDB.trn_attendance ATTN ON ATTN.dtDate=PEA.dtmDate  AND  ATTN.intEmployeeId=ME.intEmployeeId
			WHERE PEA.intCompanyId='$companyId' AND
			PEA.intLocationId='$locationId' AND
			PEA.intTeam='$teamId' AND
			PEA.dtmDate='$date'";

	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$data['EmpId'] 	 	 = $row['intEmpId'];
		$data['EmpTypeId'] 	 = $row['intEmpType'];
		$data['EmpType'] 	 = $row['strEmployeeType'];
		$data['PrinterName'] = $row['strPrinterName'];
		$data['qpayEmpId']   = $row['qpayEmpId'];		
		
		$arrDetailData[] = $data;
	}
	$response['arrDetailData'] 		= $arrDetailData;
	echo json_encode($response);
}

?>
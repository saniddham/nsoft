
			
$(document).ready(function() {
  		$("#frmPanelTypes").validationEngine();
		$('#frmPanelTypes #txtName').focus();
  //permision for add 
  if(intAddx)
  {
 	$('#frmPanelTypes #butNew').show();
	$('#frmPanelTypes #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmPanelTypes #butSave').show();
	$('#frmPanelTypes #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmPanelTypes #butDelete').show();
	$('#frmPanelTypes #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmPanelTypes #cboSearch').removeAttr('disabled');
  }
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

  ///save button click event
  $('#frmPanelTypes #butSave').click(function(){
	//$('#frmPanelTypes').submit();
	var requestType = '';
	if ($('#frmPanelTypes').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmPanelTypes #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = "panelTypes-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			data:$("#frmPanelTypes").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmPanelTypes #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmPanelTypes').get(0).reset();
						loadCombo_frmPanelTypes();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmPanelTypes #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load panel types details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmPanelTypes #cboSearch').click(function(){
	   $('#frmPanelTypes').validationEngine('hide');
   });
    $('#frmPanelTypes #cboSearch').change(function(){
		$('#frmPanelTypes').validationEngine('hide');
		var url = "panelTypes-db-get.php";
		if($('#frmPanelTypes #cboSearch').val()=='')
		{
			$('#frmPanelTypes').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmPanelTypes #txtName').val(json.name);
					$('#frmPanelTypes #txtQty').val(json.qty);
					$('#frmPanelTypes #txtRemark').val(json.remark);
					$('#frmPanelTypes #chkActive').attr('checked',json.status);
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmPanelTypes #butNew').click(function(){
		$('#frmPanelTypes').get(0).reset();
		loadCombo_frmPanelTypes();
		$('#frmPanelTypes #txtName').focus();
	});
    $('#frmPanelTypes #butDelete').click(function(){
		if($('#frmPanelTypes #cboSearch').val()=='')
		{
			$('#frmPanelTypes #butDelete').validationEngine('showPrompt', 'Please select Type.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmPanelTypes #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "panelTypes-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmPanelTypes #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmPanelTypes #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmPanelTypes').get(0).reset();
													loadCombo_frmPanelTypes();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});


function loadCombo_frmPanelTypes()
{
	var url 	= "panelTypes-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmPanelTypes #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmPanelTypes #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmPanelTypes #butDelete').validationEngine('hide')	;
}

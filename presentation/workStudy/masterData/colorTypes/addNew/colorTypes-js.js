
			
$(document).ready(function() {
  		$("#frmColorTypes").validationEngine();
		$('#frmColorTypes #txtName').focus();
  //permision for add 
  if(intAddx)
  {
 	$('#frmColorTypes #butNew').show();
	$('#frmColorTypes #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmColorTypes #butSave').show();
	$('#frmColorTypes #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmColorTypes #butDelete').show();
	$('#frmColorTypes #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmColorTypes #cboSearch').removeAttr('disabled');
  }
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

  ///save button click event
  $('#frmColorTypes #butSave').click(function(){
	//$('#frmColorTypes').submit();
	var requestType = '';
	if ($('#frmColorTypes').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmColorTypes #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = "colorTypes-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			data:$("#frmColorTypes").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmColorTypes #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmColorTypes').get(0).reset();
						loadCombo_frmColorTypes();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmColorTypes #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load color types details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmColorTypes #cboSearch').click(function(){
	   $('#frmColorTypes').validationEngine('hide');
   });
    $('#frmColorTypes #cboSearch').change(function(){
		$('#frmColorTypes').validationEngine('hide');
		var url = "colorTypes-db-get.php";
		if($('#frmColorTypes #cboSearch').val()=='')
		{
			$('#frmColorTypes').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmColorTypes #txtName').val(json.name);
					$('#frmColorTypes #txtQty').val(json.qty);
					$('#frmColorTypes #txtRemark').val(json.remark);
					$('#frmColorTypes #chkActive').attr('checked',json.status);
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmColorTypes #butNew').click(function(){
		$('#frmColorTypes').get(0).reset();
		loadCombo_frmColorTypes();
		$('#frmColorTypes #txtName').focus();
	});
    $('#frmColorTypes #butDelete').click(function(){
		if($('#frmColorTypes #cboSearch').val()=='')
		{
			$('#frmColorTypes #butDelete').validationEngine('showPrompt', 'Please select Type.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmColorTypes #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "colorTypes-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmColorTypes #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmColorTypes #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmColorTypes').get(0).reset();
													loadCombo_frmColorTypes();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});


function loadCombo_frmColorTypes()
{
	var url 	= "colorTypes-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmColorTypes #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmColorTypes #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmColorTypes #butDelete').validationEngine('hide')	;
}

<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];

include  "{$backwardseperator}dataAccess/permisionCheck.inc";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Color Types</title>

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="colorTypes-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" href="../../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../../libraries/validate/template.css" type="text/css">

</head>

<body>
<form id="frmColorTypes" name="frmColorTypes" method="post" action="colorTypes-db-set.php" autocomplete="off">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div align="center">
		<div class="trans_layoutD">
		  <div class="trans_text">Color Types</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="585" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Color Type</td>
                <td colspan="3">
                <select name="cboSearch" class="txtbox" id="cboSearch" style="width:250px" tabindex="1">
                   <option value=""></option>
                 <?php  $sql = "SELECT
									intId,
									strName
								FROM mst_colortypes
								order by strName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?> 
                  </select></td>
              </tr>
              <tr>
                <td width="115" class="normalfnt">&nbsp;</td>
                <td width="119" class="normalfnt">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Name&nbsp;<span class="compulsoryRed">*</span></td>
                <td colspan="3"><input name="txtName" type="text" class="validate[required,maxSize[50]]" id="txtName" style="width:250px" maxlength="50" /></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Color Quantity <span class="compulsoryRed">*</span></td>
                <td><input name="txtQty" type="text" class="validate[required,custom[integer],maxSize[10]]" id="txtQty" style="width:40px"  maxlength="10" /></td>
                <td class="normalfnt">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Description</td>
                <td>
   <textarea name="txtRemark" style="width:250px" class="validate[maxSize[250]]"  rows="2" id="txtRemark"></textarea></td>
                <td class="normalfnt">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Active</td>
             <td width="264"><input type="checkbox" name="chkActive" id="chkActive" checked="checked" tabindex="24"/></td>
                <td width="2" class="normalfnt">&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="2"></td>
                </tr>
              </table></td>
            </tr>
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img style="display:none" border="0" src="../../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../../main.php"><img  src="../../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
</form>
</body>
</html>

<?php
	$backwardseperator = "../../../../../";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Color Types Listing</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
  <tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<div align="center">
<div class="trans_layoutL">
		  <div class="trans_text">Color Types Listing</div>
          
          <table width="395">
          <tr>
          	<td width="275"><table width="100%" border="0" class="bcgl2">
          	  <tr>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
       	      </tr>
          	  <tr>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
       	      </tr>
       	    </table></td>
          </tr>
          <tr><td>
          <div style="overflow:scroll;width:800px;height:400px;margin-top:10px" id="divGrid">
            <table width="1184" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
              <tr class="">
                <td width="44"  height="24" bgcolor="#FAD163" class="normalfntMid"><strong>Id</strong></td>
                <td width="145"  bgcolor="#FAD163" class="normalfntMid"><strong>Code</strong></td>
                <td width="375"  bgcolor="#FAD163" class="normalfntMid"><strong>Name</strong></td>
                <td width="136"  bgcolor="#FAD163" class="normalfntMid"><strong>Divided By </strong></td>
                <td width="94" bgcolor="#FAD163" class="normalfntMid"  ><strong>View</strong></td>
              </tr>
              <tr class="normalfnt">
                <td bgcolor="#FFFFFF" class="normalfntMid">1</td>
                <td bgcolor="#FFFFFF"> C001</td>
                <td bgcolor="#FFFFFF">1 Color</td>
                <td align="center" bgcolor="#FFFFFF">1</td>
                <td  bgcolor="#FFFFFF"><img src="../../../../../images/view.png" width="91" height="19" /></td>
              </tr>
              <tr class="normalfnt">
                <td bgcolor="#FFFFFF" class="normalfntMid">2</td>
                <td bgcolor="#FFFFFF">C002</td>
                <td bgcolor="#FFFFFF">2 Color</td>
                <td align="center" bgcolor="#FFFFFF">2</td>
                <td  bgcolor="#FFFFFF"><img src="../../../../../images/view.png" width="91" height="19" /></td>
              </tr>
            </table>
          
          </div>
          </td></tr>
          </table>
</div>
</div>

</body>
</html>

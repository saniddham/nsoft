<?php
session_start();
$backwardseperator = "../../../../../";
$thisFilePath =  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Company Printer Capacity</title>

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="printerCapacity-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" href="../../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../../libraries/validate/template.css" type="text/css">


</head>

<body>
<form id="frmCompanyPrinterCapacity" name="frmCompanyPrinterCapacity" method="post" action="printerCapacity-db-set.php" autocomplete="off">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div align="center">
	<div class="trans_layoutD">
		  <div class="trans_text">Company Printer Capacity</div>
	  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="585" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
              <tr>
                <td width="118" height="27" class="normalfnt">&nbsp;</td>
                <td width="131" class="normalfnt">Company <span class="compulsoryRed">*</span></td>
                <td width="195">
                <select name="cboSearch" id="cboSearch"  class="validate[required]" style="width:180px">
                  <option value=""></option>
                  <?php  $sql = "SELECT
								mst_companies.intId,
								mst_companies.strName
								FROM mst_companies
								WHERE
									intStatus = 1
									order by strName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?>
                </select></td>
                <td width="111"></select></td>
              </tr>
              <tr>
                <td height="21" class="normalfnt">&nbsp;</td>
                <td colspan="2" class="normalfnt">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td height="27" class="normalfnt">&nbsp;</td>
                <td colspan="2" class="normalfnt">
                <table id="tblMain" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
                <thead>
                  <tr>
                    <td width="62%" height="19" bgcolor="#FAD163" class="normalfntMid">Type</td>
                    <td width="38%" bgcolor="#FAD163" class="normalfntMid">Quantity</td>
                  </tr>
                 </thead>
                 <tbody>
                  <?php
	 	 $sql = "SELECT
				 intId,
				 strName
				 FROM mst_printertypes
				 WHERE
				 intStatus = '1'";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
	  			 ?>
                  <tr>
                    <td bgcolor="#FFFFFF" class="normalfnt" id=<?php echo $row['intId'];?>><?php echo $row['strName'];?></td>
                    <td bgcolor="#FFFFFF">
                    <input class="capacityQty validate[custom[integer],maxSize[10]]" type="text" name=txt<?php echo $row['intId'];?> id=txt<?php echo $row['intId'];?> /></td>
                  </tr>
                 <?php 
        		 } 
       			 ?>
                 </tbody>
                </table>
                </td>
              </tr>
              <tr>
                <td height="27" class="normalfnt">&nbsp;</td>
                <td colspan="2" class="normalfnt">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              </table></td>
            </tr>
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><a href="../../../../../main.php"><img  src="../../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
</form>
</body>
</html>

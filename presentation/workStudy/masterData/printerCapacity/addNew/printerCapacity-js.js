
			
$(document).ready(function() {
  		$("#frmCompanyPrinterCapacity").validationEngine();
		$('#frmCompanyPrinterCapacity #cboSearch').focus();
  //permision for add 
  if(intAddx)
  {
 	$('#frmCompanyPrinterCapacity #butNew').show();
	$('#frmCompanyPrinterCapacity #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmCompanyPrinterCapacity #butSave').show();
	$('#frmCompanyPrinterCapacity #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmCompanyPrinterCapacity #cboSearch').removeAttr('disabled');
  }
  
  ///save button click event
  $('#frmCompanyPrinterCapacity #butSave').click(function(){
		var value="[ ";
		$('.capacityQty').each(function(){
			value += '{ "typeId":"'+$(this).attr('id')+'", "qty": "'+ $(this).val()+'" },';	
		});
		value = value.substr(0,value.length-1);
		value += " ]";
	
	var requestType = '';
	if ($('#frmCompanyPrinterCapacity').validationEngine('validate'))   
    { 
		if($("#frmCompanyPrinterCapacity #cboSearch").val()=='')
			requestType = 'edit';
		else
			requestType = 'edit';
		
		var url = "printerCapacity-db-set.php"+"?requestType="+requestType+"&cboSearch="+$("#frmCompanyPrinterCapacity #cboSearch").val();
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			data: {myJson:  value},
			async:false,
			
			success:function(json){
				$('#frmCompanyPrinterCapacity #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmCompanyPrinterCapacity').get(0).reset();
						loadCombo_frmCompanyPrinterCapacity();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmCompanyPrinterCapacity #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load printer capacity details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmCompanyPrinterCapacity #cboSearch').click(function(){
	   $('#frmCompanyPrinterCapacity').validationEngine('hide');
   });
    $('#frmCompanyPrinterCapacity #cboSearch').change(function(){
		$('#frmCompanyPrinterCapacity').validationEngine('hide');
		var url = "printerCapacity-db-get.php";
		if($('#frmCompanyPrinterCapacity #cboSearch').val()=='')
		{
			$('#frmCompanyPrinterCapacity').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+$(this).val()+'&cboSearch='+$('#cboSearch').val(),
			async:false,
			success:function(json){
				var txtId = "";
				if(eval(json)!=null)
				{
					for(var i=0;i<=json.length;i++)
					{
					txtId = json[i].tpId;
					qty = json[i].qty;
					$('#frmCompanyPrinterCapacity #txt'+txtId).val(qty);
					}
				}
				else
				{
					$('.capacityQty:text').val('');
				}
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmCompanyPrinterCapacity #butNew').click(function(){
		$('#frmCompanyPrinterCapacity').get(0).reset();
		loadCombo_frmCompanyPrinterCapacity();
		$('#frmCompanyPrinterCapacity #cboSearch').focus();
	});
    $('#frmCompanyPrinterCapacity #butDelete').click(function(){
		if($('#frmCompanyPrinterCapacity #cboSearch').val()=='')
		{
			$('#frmCompanyPrinterCapacity #butDelete').validationEngine('showPrompt', 'Please select Type.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmCompanyPrinterCapacity #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "printerCapacity-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmCompanyPrinterCapacity #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmCompanyPrinterCapacity #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmCompanyPrinterCapacity').get(0).reset();
													loadCombo_frmCompanyPrinterCapacity();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
	
	

});


function loadCombo_frmCompanyPrinterCapacity()
{
	var url 	= "printerCapacity-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmCompanyPrinterCapacity #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmCompanyPrinterCapacity #butSave').validationEngine('hide')	;
}

<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$companyId 			= $_REQUEST['cboSearch'];
	$values = json_decode($_REQUEST['myJson'], true);
	
	/////////// printer capacity insert part /////////////////////
	if($requestType=='edit')
	{
		$sql = "DELETE FROM `mst_companyprintercapacity` WHERE (`intCompanyId`='$companyId')";
		$db->RunQuery($sql);
		
		foreach($values as $value)
		{
			$tpId = substr($value['typeId'],3);
			$qty = ($value['qty']==''?0:$value['qty']);
			$sql = "INSERT INTO `mst_companyprintercapacity` 				(`intCompanyId`,`intPrinterTypeId`,`intQty`,`intCreator`,dtmCreateDate) 
			VALUES 
			('$companyId','$tpId','$qty','$userId',now())";
			$result = $db->RunQuery($sql);
		}
		
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	echo json_encode($response);
?>
<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 		= $_SESSION['mainPath'];
	$userId 		= $_SESSION['userId'];
	$requestType	= $_REQUEST['requestType'];
	$companyId 		= $_REQUEST['cboSearch'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('tpId'=>'', 'qty'=>'');
	
	/////////// printer capacity load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
					intId,
					strName
				FROM mst_companies
				order by strName
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		$sql = "SELECT 	intPrinterTypeId,
						intQty
				FROM mst_companyprintercapacity
				WHERE
					intCompanyId =  '$companyId'
				";
		$result = $db->RunQuery($sql);
		$arr;
		while($row=mysqli_fetch_array($result))
		{
			$response['tpId'] 	= $row['intPrinterTypeId'];
			$response['qty'] = $row['intQty'];
			$arr[] = $response;
		}
		echo json_encode($arr);
	}
	
?>
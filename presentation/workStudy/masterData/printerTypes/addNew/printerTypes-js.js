
			
$(document).ready(function() {
  		$("#frmPrinterTypes").validationEngine();
		$('#frmPrinterTypes #txtName').focus();
  //permision for add 
  if(intAddx)
  {
 	$('#frmPrinterTypes #butNew').show();
	$('#frmPrinterTypes #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmPrinterTypes #butSave').show();
	$('#frmPrinterTypes #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmPrinterTypes #butDelete').show();
	$('#frmPrinterTypes #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmPrinterTypes #cboSearch').removeAttr('disabled');
  }
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

  ///save button click event
  $('#frmPrinterTypes #butSave').click(function(){
	//$('#frmPrinterTypes').submit();
	var requestType = '';
	if ($('#frmPrinterTypes').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmPrinterTypes #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = "printerTypes-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			data:$("#frmPrinterTypes").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmPrinterTypes #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmPrinterTypes').get(0).reset();
						loadCombo_frmPrinterTypes();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmPrinterTypes #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load printer types details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmPrinterTypes #cboSearch').click(function(){
	   $('#frmPrinterTypes').validationEngine('hide');
   });
    $('#frmPrinterTypes #cboSearch').change(function(){
		$('#frmPrinterTypes').validationEngine('hide');
		var url = "printerTypes-db-get.php";
		if($('#frmPrinterTypes #cboSearch').val()=='')
		{
			$('#frmPrinterTypes').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmPrinterTypes #txtName').val(json.name);
					$('#frmPrinterTypes #txtRemark').val(json.remark);
					$('#frmPrinterTypes #chkActive').attr('checked',json.status);
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmPrinterTypes #butNew').click(function(){
		$('#frmPrinterTypes').get(0).reset();
		loadCombo_frmPrinterTypes();
		$('#frmPrinterTypes #txtName').focus();
	});
    $('#frmPrinterTypes #butDelete').click(function(){
		if($('#frmPrinterTypes #cboSearch').val()=='')
		{
			$('#frmPrinterTypes #butDelete').validationEngine('showPrompt', 'Please select Type.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmPrinterTypes #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "printerTypes-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmPrinterTypes #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmPrinterTypes #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmPrinterTypes').get(0).reset();
													loadCombo_frmPrinterTypes();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});


function loadCombo_frmPrinterTypes()
{
	var url 	= "printerTypes-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmPrinterTypes #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmPrinterTypes #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmPrinterTypes #butDelete').validationEngine('hide')	;
}

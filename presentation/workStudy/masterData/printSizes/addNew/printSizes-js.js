
			
$(document).ready(function() {
  		$("#frmPrintSizes").validationEngine();
		$('#frmPrintSizes #txtName').focus();
  //permision for add 
  if(intAddx)
  {
 	$('#frmPrintSizes #butNew').show();
	$('#frmPrintSizes #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmPrintSizes #butSave').show();
	$('#frmPrintSizes #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmPrintSizes #butDelete').show();
	$('#frmPrintSizes #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmPrintSizes #cboSearch').removeAttr('disabled');
  }
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

  ///save button click event
  $('#frmPrintSizes #butSave').click(function(){
	//$('#frmPrintSizes').submit();
	var requestType = '';
	if ($('#frmPrintSizes').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmPrintSizes #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = "printSizes-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			data:$("#frmPrintSizes").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmPrintSizes #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmPrintSizes').get(0).reset();
						loadCombo_frmPrintSizes();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmPrintSizes #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load print sizes details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmPrintSizes #cboSearch').click(function(){
	   $('#frmPrintSizes').validationEngine('hide');
   });
    $('#frmPrintSizes #cboSearch').change(function(){
		$('#frmPrintSizes').validationEngine('hide');
		var url = "printSizes-db-get.php";
		if($('#frmPrintSizes #cboSearch').val()=='')
		{
			$('#frmPrintSizes').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmPrintSizes #txtName').val(json.name);
					$('#frmPrintSizes #txtRemark').val(json.remark);
					$('#frmPrintSizes #chkActive').attr('checked',json.status);
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmPrintSizes #butNew').click(function(){
		$('#frmPrintSizes').get(0).reset();
		loadCombo_frmPrintSizes();
		$('#frmPrintSizes #txtName').focus();
	});
    $('#frmPrintSizes #butDelete').click(function(){
		if($('#frmPrintSizes #cboSearch').val()=='')
		{
			$('#frmPrintSizes #butDelete').validationEngine('showPrompt', 'Please select Size.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmPrintSizes #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "printSizes-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmPrintSizes #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmPrintSizes #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmPrintSizes').get(0).reset();
													loadCombo_frmPrintSizes();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});


function loadCombo_frmPrintSizes()
{
	var url 	= "printSizes-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmPrintSizes #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmPrintSizes #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmPrintSizes #butDelete').validationEngine('hide')	;
}


			
$(document).ready(function() {
  		$("#frmStrokesTypes").validationEngine();
		$('#frmStrokesTypes #txtName').focus();
  //permision for add 
  if(intAddx)
  {
 	$('#frmStrokesTypes #butNew').show();
	$('#frmStrokesTypes #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmStrokesTypes #butSave').show();
	$('#frmStrokesTypes #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmStrokesTypes #butDelete').show();
	$('#frmStrokesTypes #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmStrokesTypes #cboSearch').removeAttr('disabled');
  }
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

  ///save button click event
  $('#frmStrokesTypes #butSave').click(function(){
	//$('#frmStrokesTypes').submit();
	var requestType = '';
	if ($('#frmStrokesTypes').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmStrokesTypes #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = "strockTypes-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			data:$("#frmStrokesTypes").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmStrokesTypes #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmStrokesTypes').get(0).reset();
						loadCombo_frmStrokesTypes();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmStrokesTypes #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load strokes types details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmStrokesTypes #cboSearch').click(function(){
	   $('#frmStrokesTypes').validationEngine('hide');
   });
    $('#frmStrokesTypes #cboSearch').change(function(){
		$('#frmStrokesTypes').validationEngine('hide');
		var url = "strockTypes-db-get.php";
		if($('#frmStrokesTypes #cboSearch').val()=='')
		{
			$('#frmStrokesTypes').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmStrokesTypes #txtName').val(json.name);
					$('#frmStrokesTypes #txtQty').val(json.qty);
					$('#frmStrokesTypes #txtRemark').val(json.remark);
					$('#frmStrokesTypes #chkActive').attr('checked',json.status);
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmStrokesTypes #butNew').click(function(){
		$('#frmStrokesTypes').get(0).reset();
		loadCombo_frmStrokesTypes();
		$('#frmStrokesTypes #txtName').focus();
	});
    $('#frmStrokesTypes #butDelete').click(function(){
		if($('#frmStrokesTypes #cboSearch').val()=='')
		{
			$('#frmStrokesTypes #butDelete').validationEngine('showPrompt', 'Please select Type.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmStrokesTypes #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "strockTypes-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmStrokesTypes #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmStrokesTypes #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmStrokesTypes').get(0).reset();
													loadCombo_frmStrokesTypes();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});


function loadCombo_frmStrokesTypes()
{
	var url 	= "strockTypes-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmStrokesTypes #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmStrokesTypes #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmStrokesTypes #butDelete').validationEngine('hide')	;
}

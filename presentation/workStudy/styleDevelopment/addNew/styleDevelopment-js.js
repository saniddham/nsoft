			
$(document).ready(function() {
	
  		$("#frmStyleDevelopment").validationEngine();
		
		$("#cboPONo").live('change',function(){
			//loadOrderNo();
			$('#cboOrderNo').val($(this).val());
			$('#cboOrderNo').change();
			//loadSalesOrderNo();
			//clearGrid();
		});
		$("#cboOrderYear").live('change',function(){
			loadPONoAndOrderNo();
			//clearGrid();
		});
		$("#cboOrderNo").live('change',function(){
			$('#cboPONo').val($(this).val());
			loadSalesOrderNo();
			//clearGrid();
		});
		$("#cboSalesOrderNo").live('change',function(){
			submitForm();
		});
		
		$('#butSave').live('click',function(){
			if(!$('#frmStyleDevelopment').validationEngine('validate'))
			{
				return;	
			}
			var arrStrocks='';
			var orderNo 	= $('#cboOrderNo').val();
			var orderYear 	= $('#cboOrderYear').val();
			var salesId 	= $('#cboSalesOrderNo').val();
			var printSizeId	= $('#cboPrintSize').val();
			var panelQty	= $('#txtPanelQty').val();
			
			$('.clsBulkStrocks').each(function(){
				var colorId = $(this).attr('id');
				var strocks = $(this).val();
				arrStrocks+= (arrStrocks==''?'':',')+'{"colorId":"'+colorId+'","strocks":"'+strocks+'"}';
			});
			var url = "styleDevelopment-db-set.php?requestType=saveDetails";
			var data  = "&orderNo="+orderNo;
				data += "&orderYear="+orderYear;
				data += "&salesId="+salesId;
				data += "&printSizeId="+printSizeId;
				data += "&panelQty="+panelQty;
				data += "&arrData=["+arrStrocks+']';
				
			
			$.ajax({url:url,
					async:false,
					dataType:'json',
					data:data,
					success:function(){
							document.location.href = document.location.href;
					}
					});
		});

});
 function submitForm(){
	window.location.href = "styleDevelopment.php?cboOrderNo="+$('#cboOrderNo').val()
						+'&cboSalesOrderNo='+$('#cboSalesOrderNo').val()
						+'&cboPONo='+$('#cboPONo').val()
						+'&cboOrderYear='+$('#cboOrderYear').val()
}
//----------------------------------------------- 
function clearGrid()
{
	var rowCount = document.getElementById('tblMain').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMain').deleteRow(1);
	}
	
	$('#txtQty').val('');
}
  //-------------------------------------------------------
 
function loadSalesOrderNo()
{
	var orderYear 	= $('#cboOrderYear').val();
	var orderNo		= $('#cboOrderNo').val();
	
	var url = "styleDevelopment-db-get.php?requestType=loadSalesOrderNo&orderNo="+orderNo+'&orderYear='+orderYear;
	var obj = $.ajax({url:url,async:false}); 
	$('#cboSalesOrderNo').html(obj.responseText);
}

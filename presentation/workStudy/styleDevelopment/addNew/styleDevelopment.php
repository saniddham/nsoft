<?php
$backwardseperator = "../../../../";
include  	"{$backwardseperator}dataAccess/Connector.php";

$orderYear 	= $_REQUEST['cboOrderYear'];
$orderNo	= $_REQUEST['cboOrderNo'];
$poNo		= $_REQUEST['cboPONo'];
$salesNo	= $_REQUEST['cboSalesOrderNo'];

$sql = "SELECT DISTINCT
		plan_styledevelopment.intPrintSizeId,
		plan_styledevelopment.intPanelCount
		FROM plan_styledevelopment
		WHERE
		plan_styledevelopment.intOrderNo =  '$orderNo' AND
		plan_styledevelopment.intOrderYear =  '$orderYear' AND
		plan_styledevelopment.intSalesOrderId =  '$salesNo'
		";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$intPrintSizeId = $row['intPrintSizeId'];	
	$intPanelCount  = $row['intPanelCount'];	
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Style Development</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="styleDevelopment-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body>
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
 <script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
<form id="frmStyleDevelopment" name="frmStyleDevelopment" method="post" action="styleDevelopment.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text"> Style Development</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td align="center"><table bgcolor="#DEF2FE" width="833" border="0" cellpadding="0" cellspacing="0" class="tableBorder_allRound">
          <tr>
            <td width="17" class="normalfnt">&nbsp;</td>
            <td width="68" height="27" class="normalfnt">Year</td>
            <td width="109" class="normalfnt"><select name="cboOrderYear" id="cboOrderYear" style="width:70px">
              <?php
					$sql = "SELECT DISTINCT
							trn_orderheader.intOrderYear
							FROM trn_orderheader
							ORDER BY
							trn_orderheader.intOrderYear DESC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intOrderYear']==$x_year)
						echo "<option value=\"".$row['intOrderYear']."\" selected=\"selected\">".$row['intOrderYear']."</option>";	
						else
						echo "<option value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";	
					}
				?>
            </select></td>
            <td width="97" class="normalfnt">Customer PO</td>
            <td width="123" class="normalfnt"><select name="cboPONo" id="cboPONo" style="width:103px">
              <option value=""></option>
              <?php
			  	//if($po)
					$sql = "SELECT DISTINCT
							trn_orderheader.strCustomerPoNo,intOrderNo
							FROM trn_orderheader 
							ORDER BY
							trn_orderheader.strCustomerPoNo ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intOrderNo']==$poNo)
							echo "<option value=\"".$row['intOrderNo']."\" selected=\"selected\">".$row['strCustomerPoNo']."</option>";	
						else
							echo "<option value=\"".$row['intOrderNo']."\">".$row['strCustomerPoNo']."</option>";	
					}
				?>
            </select></td>
            <td width="81" class="normalfnt">Order No</td>
            <td width="135" class="normalfnt"><select name="cboOrderNo" id="cboOrderNo" style="width:103px">
              <option value=""></option>
              <?php
					$sql = "SELECT DISTINCT
							trn_orderheader.intOrderNo, 
							trn_orderheader.intOrderYear 
							FROM trn_orderheader ORDER BY
							 trn_orderheader.intOrderNo DESC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intOrderNo']==$orderNo)
						echo "<option value=\"".$row['intOrderNo']."\" selected=\"selected\">".$row['intOrderNo']."</option>";	
						else
						echo "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";	
					}
				?>
            </select></td>
            <td width="116" class="normalfnt">Sales Order No</td>
            <td width="179" class="normalfnt"><select name="cboSalesOrderNo" id="cboSalesOrderNo" style="width:103px">
              <option value=""></option>
              <?php
					$sql = "SELECT
								trn_orderdetails.strSalesOrderNo,
								trn_orderdetails.intSalesOrderId,intQty
							FROM trn_orderdetails
							WHERE
								trn_orderdetails.intOrderNo =  '$orderNo' AND
								trn_orderdetails.intOrderYear =  '$orderYear'
							ORDER BY
								trn_orderdetails.strSalesOrderNo ASC
							";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intSalesOrderId']==$salesNo)
						{
							echo "<option value=\"".$row['intSalesOrderId']."\" selected=\"selected\">".$row['strSalesOrderNo']."</option>";	
							$orderQty = $row['intQty'];
						}
						else
							echo "<option value=\"".$row['intSalesOrderId']."\">".$row['strSalesOrderNo']."</option>";	
					}
				?>
            </select></td>
          </tr>
          <tr>
            <td class="normalfnt">&nbsp;</td>
            <td height="27" class="normalfnt">&nbsp;Qty</td>
            <td class="normalfnt">:&nbsp;<strong><?php echo $orderQty; ?></strong></td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
          </tr>
        </table>
          <table style="margin-top:10px" bgcolor="#FCFDC8" width="833" border="0" cellpadding="0" cellspacing="0" class="tableBorder_allRound">
            <tr>
              <td width="14" class="normalfnt">&nbsp;</td>
              <td width="62" height="27" class="normalfnt">Print Size</td>
              <td width="104" class="normalfnt"><select class="validate[required]" name="cboPrintSize" id="cboPrintSize" style="width:80px">
               <?php
			   	$sql = "SELECT
							mst_printsizes.intId,
							mst_printsizes.strName
						FROM mst_printsizes
						WHERE
							mst_printsizes.intStatus =  '1'
						ORDER BY
							mst_printsizes.strName ASC
						";
				$result = $db->RunQuery($sql);
				echo "<option value=\"\"></option>";
				while($row=mysqli_fetch_array($result))
				{
					if($intPrintSizeId==$row['intId'])
						echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
					else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					
				}
			   ?>
              </select></td>
              <td width="86" class="normalfnt">Panel Count</td>
              <td width="59" class="normalfnt" id=""><input value="<?Php echo $intPanelCount; ?>"  class="validate[required,custom[number],min[1]]" style="width:50px;text-align:center" type="text" name="txtPanelQty" id="txtPanelQty" /></td>
              <td class="normalfnt"><table width="98%" class="grid" id="tblMain" >

                <tr class="gridHeader">
                  <td width="25%" height="22" >Color</td>
                  <td width="48%">Technique</td>
                  <td width="16%">Sample Stroke </td>
                  <td width="11%">Bulk Stroke</td>
                 </tr>

                           <?php
			  $sql = "SELECT
				trn_sampleinfomations_details.intColorId,
				mst_colors.strName as colorName,
				mst_technique.strName as techniqueName,
				sum(trn_sampleinfomations_details_technical.intNoOfShots) as shots,
				trn_sampleinfomations_details.intTechniqueId as intTechniqueId
				FROM
				trn_orderdetails
				Inner Join trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations_details.intSampleYear = trn_orderdetails.intSampleYear AND trn_sampleinfomations_details.intRevNo = trn_orderdetails.intRevisionNo AND trn_sampleinfomations_details.strPrintName = trn_orderdetails.strPrintName AND trn_sampleinfomations_details.strComboName = trn_orderdetails.strCombo
				Inner Join mst_colors ON mst_colors.intId = trn_sampleinfomations_details.intColorId
				Inner Join mst_technique ON mst_technique.intId = trn_sampleinfomations_details.intTechniqueId
				Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations_details_technical.intSampleYear = trn_orderdetails.intSampleYear AND trn_sampleinfomations_details_technical.intRevNo = trn_orderdetails.intRevisionNo AND trn_sampleinfomations_details_technical.strPrintName = trn_orderdetails.strPrintName AND trn_sampleinfomations_details_technical.strComboName = trn_orderdetails.strCombo AND trn_sampleinfomations_details_technical.intColorId = trn_sampleinfomations_details.intColorId
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_orderdetails.intSalesOrderId =  '$salesNo'
				GROUP BY
				trn_sampleinfomations_details.intColorId,
				mst_technique.intId
				";
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
			   $sql1 = "SELECT intBulkStrocks	 from 	plan_styledevelopment	
						WHERE
						plan_styledevelopment.intOrderNo =  '$orderNo' AND
						plan_styledevelopment.intOrderYear =  '$orderYear' AND
						plan_styledevelopment.intSalesOrderId =  '$salesNo' AND
						plan_styledevelopment.intColor =  '".$row['intColorId']."' 
					";
			$result1 = $db->RunQuery($sql1);
			while($row1=mysqli_fetch_array($result1))
			{
				$intStrocks = $row1['intBulkStrocks'];	
			}
			if($intStrocks=='')
				$intStrocks = $row['shots'];
			  ?> 
                <tr class="normalfnt">
                  <td align="center" bgcolor="#FFFFFF"><?php echo $row['colorName']; ?></td>
                  <td align="center" bgcolor="#FFFFFF"><?php echo $row['techniqueName']; ?></td>
                  <td align="center" bgcolor="#FFFFFF"><input value="<?php echo $row['shots']; ?>" disabled="disabled" style="width:50px;text-align:center" type="text" name="textfield3" id="textfield3" /></td>
                  <td align="center" bgcolor="#FFFFFF"><input value="<?php echo $intStrocks; ?>" class="clsBulkStrocks validate[required,custom[number],min[1]]" style="width:50px;text-align:center" type="text" name="textfield2" 
                  id="<?php echo $row['intColorId']; ?>" /></td>
                  </tr>
             <?php
			}
			 ?>  
              </table></td>
              </tr>
            </table></td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><img src="../../../../images/Tsave.jpg" width="92" height="24" class="mouseover" id="butSave" /><img src="../../../../planning/img/Tclose.jpg" width="92" height="24" /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>

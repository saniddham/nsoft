<?php
//
$status 				= 	$dataArray['status'];
$approveLevels 			= 	$dataArray['approveLevels'];
$programCode			= 	$dataArray['programCode'];
$companyId				= 	$dataArray['companyId'];

$program			= 	$dataArray['program'];

$field1				= 	$dataArray['field1'];
$field2				= 	$dataArray['field2'];
$field3				= 	$dataArray['field3'];
$field4				= 	$dataArray['field4'];
$field5				= 	$dataArray['field5'];
$value1				= 	$dataArray['value1'];
$value2				= 	$dataArray['value2'];
$value3				= 	$dataArray['value3'];
$value4				= 	$dataArray['value4'];
$value5				= 	$dataArray['value5'];

$subject			= 	$dataArray['subject'];

$statement1			= 	$dataArray['statement1'];
$statement2			= 	$dataArray['statement2'];

$remarksField		= 	$dataArray['remarksField'];
$remarksValue		= 	$dataArray['remarksValue'];

$link				= base64_decode($dataArray['link']);

$showDetails		= 	$dataArray['showDetails'];
$details			= 	$dataArray['details'];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Marketing Confirm Email</title>
<style type="text/css">
.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntWhite {
	font-family: Verdana;
	font-size: 11px;
	color: #FFF;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.sampleNo{
	color: #039; font-weight: bold; font-size: 12px; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;	
}
.part{
	color: #096CBD;
	font-weight: bold;
	font-size: 12px;
	font-family: "Comic Sans MS", cursive;
}
.tableBorder_allRound{
	
	border: 1px solid #CCCCCC;
	-moz-border-radius-bottomright:10px;
	-moz-border-radius-bottomleft:10px;
	-moz-border-radius-topright:10px;
	-moz-border-radius-topleft:10px;
}
.tblBorder{
	border-top:solid;
	border-top-width:thin;
	border-bottom:solid;
	border-bottom-width:thin;
	border-left:solid;
	border-left-width:thin;
	border-right:solid;
	border-right-width:thin;
	border-color:#dce9f9;
}
</style>
</head>

<body>
<table  width="595" border="0" cellspacing="0" cellpadding="2" class="tblBorder">
  <tr>
    <td colspan="3" class="normalfnt" style="color:#3E437D">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="normalfnt">Dear <strong>MAILRECIVER_NAME</strong>,</td>
    <td width="5">&nbsp;</td>
  </tr>
  <tr>
    <td width="72" height="25">&nbsp;</td>
    <td width="502" class="normalfnt"><?php echo $statement1; ?> <?php echo $program; ?></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="10">&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Click <a  href="<?php  echo $link ?>">here</a> <?php echo $statement2; ?> <?php echo $program; ?>.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="normalfnt">
        <tr height="24">
        <?php 
		$cols =0;
		if($field1!=''){
		$cols =$cols+2;
		?>
          <td width="10%"  height="24" bgcolor="#E6FCD6" class="part" nowrap="nowrap"><?php echo $field1; ?>&nbsp;</td>
          <td width="10%" bgcolor="#E6FCD6"><?php echo $value1 ?></td> 
        <?php 
		}
		?>
        <?php 
		if($field2!=''){
		$cols =$cols+2;
		?>
          <td width="10%" bgcolor="#E6FCD6" class="part" nowrap="nowrap"><?php echo $field2; ?>&nbsp;</td>
          <td width="10%" bgcolor="#E6FCD6"><?php echo $value2 ?></td>
        <?php 
		}
		?>
        <?php 
		if($field3!=''){
		$cols =$cols+2;
		?>
          <td width="10%" bgcolor="#E6FCD6" class="part"><?php echo $field3; ?>&nbsp;</td>
          <td width="10%" bgcolor="#E6FCD6"><?php echo $value3 ?></td>
        <?php 
		}
		?>
        <?php 
		if($field4!=''){
		$cols =$cols+2;
		?>
          <td width="10%" bgcolor="#E6FCD6" class="part"><?php echo $field4; ?>&nbsp;</td>
          <td width="10%" bgcolor="#E6FCD6"><?php echo $value4 ?></td>
        <?php 
		}
		?>
        <?php 
		if($field5!=''){
		$cols =$cols+2;
		?>
          <td width="10%" bgcolor="#E6FCD6" class="part"><?php echo $field5; ?>&nbsp;</td>
          <td width="10%" bgcolor="#E6FCD6"><?php echo $value5 ?></td>
        <?php 
		}
		?>
        </tr>
		 <?php
         if($remarksField!=''){
         ?> 
    <tr height="40">
          <td width="10%" bgcolor="#E6FCD6" class="part"  height="40"><?php echo $remarksField; ?>&nbsp;</td>
          <td colspan="<?php echo $cols-1 ; ?>" width="90%" bgcolor="#E6FCD6"><?php echo $remarksValue ?></td>
    </tr>
			<?php
         }
            ?>
    </table></td>
  </tr>
  <?php
  if($showDetails==1){
  ?>
   <tr>
      <td colspan="3"><table border="1"><?php echo get_detail_rowssss($program,$value1,$value2); ?></table></td>
   </tr>
   <?php
  }
   ?>
   <tr>
    <td colspan="2" class="normalfnt">Thanks,</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="normalfnt"><strong>MAILFROM_NAME</strong><br />
    ...................</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="normalfnt">(This is a<strong> <span style="color:#0000FF">Nsoft</span> </strong>system generated email.)</td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>

<?php

?>
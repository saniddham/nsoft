<?php
session_start();

$status 				= 	$_REQUEST['status'];
$approveLevels 			= 	$_REQUEST['approveLevels'];
$programCode			= 	$_REQUEST['programCode'];
$companyId				= 	$_REQUEST['companyId'];
$program				= 	$_REQUEST['program'];
$field1					= 	$_REQUEST['field1'];
$field2					= 	$_REQUEST['field2'];
$field3					= 	$_REQUEST['field3'];
$field4					= 	$_REQUEST['field4'];
$field5					= 	$_REQUEST['field5'];
$value1					= 	$_REQUEST['value1'];
$value2					= 	$_REQUEST['value2'];
$value3					= 	$_REQUEST['value3'];
$value4					= 	$_REQUEST['value4'];
$value5					= 	$_REQUEST['value5'];
$subject				= 	$_REQUEST['subject'];
$statement1				= 	$_REQUEST['statement1'];
$statement2				= 	$_REQUEST['statement2'];

$remarksField			= 	$_REQUEST['remarksField'];
$remarksValue			= 	$_REQUEST['remarksValue'];

$link					= 	$_REQUEST['link'];
$createUserId			=	$_REQUEST['createUserId'];

$showDetails			=	$_REQUEST['showDetails'];
  
require_once  	"../dataAccess/Connector.php";
require_once 	"../class/cls_mail.php";
$objMail = new cls_create_mail($db);
if (isset($_REQUEST['cboUser']))
{
		//ini_set('display_errors',1);
		$userId					= 	$_REQUEST['cboUser'];
		echo "<span style=\"color:green\">Email has sent.</span>";
			$sql = "SELECT
						sys_users.strFullName,
						sys_users.strEmail
					FROM sys_users
					WHERE
						sys_users.intUserId =  '$userId'";	
		 	$result = $db->RunQuery($sql);
			$row	=mysqli_fetch_array($result);
			$enterUserName 	= $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			 
			//send mails ////
			$_REQUEST = NULL;
			
			$_REQUEST['program']=$program;
			$_REQUEST['field1']=$field1;
			$_REQUEST['field2']=$field2;
			$_REQUEST['field3']=$field3;
			$_REQUEST['field4']=$field4;
			$_REQUEST['field5']=$field5;
			$_REQUEST['value1']=$value1;
			$_REQUEST['value2']=$value2;
			$_REQUEST['value3']=$value3;
			$_REQUEST['value4']=$value4;
			$_REQUEST['value5']=$value5;
			
			$_REQUEST['remarksField']=$remarksField;
			$_REQUEST['remarksValue']=$remarksValue;
			
			$_REQUEST['subject']=$subject;
			
			$_REQUEST['statement1']=$statement1;
			$_REQUEST['statement2']=$statement2;
			$_REQUEST['link']=$link;
			$_REQUEST['showDetails']=$showDetails;
			$_REQUEST['details']=$details;
			
			$objMail->send_Response_Mail('mail_approval_template.php',$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$subject,$enterUserEmail,$enterUserName);
			
			
			
		//	$objMail->send_Response_Mail('mail_approval_template.php',$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
		
}

// check if user have at least 1 approve level.
$permisionToSendToApproval = false;
for($i=1;$i<=5;$i++)
{
	$sql = "SELECT
				menupermision.int{$i}Approval
			FROM
				menupermision
			Inner Join menus ON menus.intId = menupermision.intMenuId
			WHERE
				menus.strCode 				=  '$programCode' AND
				menupermision.intUserId 	=  '".$_SESSION["userId"]."' AND
				menupermision.int{$i}Approval = 1
	";
	$result = $db->RunQuery($sql);
	
	while($row=mysqli_fetch_array($result))
	{
		$permisionToSendToApproval = true;	
	}
	
}
if((!$permisionToSendToApproval &&($createUserId !=$_SESSION["userId"]))||($status==1)||($status==0))
		die();

?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Upload Files</title>
<link href="../css/mainstyle.css" rel="stylesheet" type="text/css" />
<script type="application/javascript">
	function submit()
	{
			document.frmSendToApproval.submit();
	}
</script>
</head>

<body>
<form id="frmSendToApproval" name="frmSendToApproval" method="post" action="sendToApproval.php" enctype="multipart/form-data">

<table class="bordered" width="477" height="48" >
<thead>

  <tr class="normalfnt">
    <th width="97" bgcolor="" class="normalfntMid">      Send a mail to</th>
    <th width="199" bgcolor="" class="normalfntMid"><select style="width:100%" name="cboUser" id="cboUser">
    <?php
	$field = "int".(($approveLevels+2)-$status)."Approval";
		$sql = "SELECT DISTINCT
					menupermision.intUserId,
					sys_users.strFullName
				FROM
					menus
					Inner Join menupermision ON menupermision.intMenuId = menus.intId
					Inner Join sys_users ON sys_users.intUserId = menupermision.intUserId
					Inner Join mst_locations_user ON mst_locations_user.intUserId = sys_users.intUserId
					Inner Join mst_locations ON mst_locations.intId = mst_locations_user.intLocationId
				WHERE
					mst_locations.intCompanyId 	=  '$companyId' AND
					menus.strCode				=  '$programCode' AND
					menupermision.$field 		=  '1' AND 
					menupermision.intUserId<>".$_SESSION["userId"]." AND
					sys_users.intStatus			= 1 
				ORDER BY sys_users.strFullName ASC
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			if($userId==$row['intUserId'])
				echo "<option selected=\"selected\" value=".$row['intUserId'].">".$row['strFullName']."</option>";	
			else
				echo "<option value=".$row['intUserId'].">".$row['strFullName']."</option>";	
		}
	?>
    </select></th>
    <th width="90" bgcolor="" class="normalfntMid">for approval.</th>
    <th width="71" bgcolor="" class="normalfntMid mouseover"><input style="visibility:hidden;width:10px" name="txtFolder" type="text" id="txtFolder" /><img src="../images/mail_send2.png" width="45" id="butSend" height="37" onclick="submit();" title="Send to Approval" class="mouseover" /></th>
  </tr>
</thead>
<tbody>
<tr style="visibility:hidden" class="normalfnt">
    <td colspan="4" bgcolor="" class="normalfntMid"><span class="normalfntMid mouseover">
    <input style="visibility:hidden;width:10px" value="<?Php echo $status; ?>" name="status" type="text" id="status" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $approveLevels; ?>" name="approveLevels" type="text" id="approveLevels" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $programCode; ?>" name="programCode" type="text" id="programCode" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $companyId; ?>" name="companyId" type="text" id="companyId" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $program; ?>" name="program" type="text" id="program" />
    
    <input style="visibility:hidden;width:10px" value="<?Php echo $field1; ?>" name="field1" type="text" id="field1" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $field2; ?>" name="field2" type="text" id="field2" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $field3; ?>" name="field3" type="text" id="field3" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $field4; ?>" name="field4" type="text" id="field4" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $field5; ?>" name="field5" type="text" id="field5" />
 
    <input style="visibility:hidden;width:10px" value="<?Php echo $value1; ?>" name="value1" type="text" id="value1" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $value2; ?>" name="value2" type="text" id="value2" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $value3; ?>" name="value3" type="text" id="value3" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $value4; ?>" name="value4" type="text" id="value4" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $value5; ?>" name="value5" type="text" id="value5" />
    
    <input style="visibility:hidden;width:10px" value="<?Php echo $subject; ?>" name="subject" type="text" id="subject" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $statement1; ?>" name="statement1" type="text" id="statement1" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $statement2; ?>" name="statement2" type="text" id="statement" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $link; ?>" name="link" type="text" id="link" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $createUserId; ?>" name="createUserId" type="text" id="createUserId;" />
    <input style="visibility:hidden;width:10px" value="<?Php echo $showDetails; ?>" name="showDetails" type="text" id="showDetails;" />
     </span></td>
    </tr>
  </tbody>
</table>
</form>
</body>
</html>
<?php
function get_detail_rowssss($program,$field1,$field2){
	global $db;
	if($program=='MRN'){
		ob_start();
		
  $sql1 = "SELECT
                                        ware_mrndetails.intMrnNo,
                                        ware_mrndetails.intMrnYear,
                                        ware_mrndetails.intOrderNo,
                                        ware_mrndetails.intOrderYear,
                                        ware_mrndetails.strStyleNo,
                                        ware_mrndetails.dblIssudQty,
                                        trn_orderdetails.strSalesOrderNo,
                                        trn_orderdetails.intSalesOrderId,
                                        ware_mrndetails.intItemId,
                                        ware_mrndetails.INITIAL_QTY,
                                        ware_mrndetails.INITIAL_EXTRA_QTY,
                                        ware_mrndetails.dblQty,
                                        ware_mrndetails.dblMRNClearQty, 
                                        ware_mrndetails.EXTRA_QTY, 
                                        ware_mrndetails.MRN_TYPE,
                                        mst_item.strCode,
                                        mst_item.intId AS itemId,
                                        mst_item.strName,
                                        mst_item.intUOM, 
                                        mst_item.dblLastPrice,
                                        mst_financecurrency.strCode AS currencyCode,
                                        mst_maincategory.strName AS mainCategory,
                                        mst_subcategory.strName AS subCategory, 
                                        mst_units.strCode as uom , 
                                        mst_part.strName as part,
										mst_brand.strName AS Brand, 
									    mst_customer.strName AS Customer										
                                        FROM ware_mrndetails 
                                        left Join trn_orderdetails ON ware_mrndetails.intOrderNo = trn_orderdetails.intOrderNo AND ware_mrndetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_mrndetails.strStyleNo = trn_orderdetails.intSalesOrderId
										Inner JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
										Inner JOIN trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
										INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
										INNER JOIN mst_customer ON  trn_orderheader.intCustomer = mst_customer.intId
                                        Inner Join mst_item ON ware_mrndetails.intItemId = mst_item.intId
                                        INNER JOIN mst_financecurrency ON mst_item.intCurrency = mst_financecurrency.intId
                                        Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
                                        Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
                                        Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
                                        left Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
                                        WHERE
                                        ware_mrndetails.intMrnNo =  '$field1' AND
                                        ware_mrndetails.intMrnYear =  '$field2'  
                                         Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc 
 ";
                                    $result1 = $db->RunQuery($sql1);
                                    $totQty_S = 0;
                                    $totAmmount_S = 0;
                                    $totWithoutExtra_S = 0;
                                ?>
                                        <th width="8%"><strong>MRN Type</strong></th>
										<th width="9%"><strong>Customer</strong></th>
                                        <th width="8%"><strong>Order No</strong></th>						
                                        <th width="12%"><strong>Sales Order  No</strong></th>
										<th width="9%"><strong>Brand</strong></th>
                                        <th width="14%"><strong>Main Category</strong></th>
                                        <th width="11%"><strong>Sub Category</strong></th>
                                        <th width="9%"><strong>Item Code</strong></th>
                                        <th width="25%"><strong>Item</strong></th>
                                        <th width="6%"><strong>UOM</strong></th>
                                        <th width="6%"><strong>Qty</strong></th>
                                        <th width="9%" bgcolor="#FFE6E6"><strong>Extra Qty</strong></th>
                                        <th width="9%"><strong>Total Qty</strong></th>
                                        
                                        
                                <?php
								    while($row = mysqli_fetch_array($result1))
                                    {
                                        $orderNo = $row['intOrderNo']."/".$row['intOrderYear'];
                                        $salesOrderNo = $row['strSalesOrderNo']."/".$row['part'];
                                        $orderQty = $row['dblQty'];
                                        $ExtraQty = $row['EXTRA_QTY'];
                                        $mrnType_Details = $row['MRN_TYPE'];
                                        $QtyWithoutExtra = $orderQty - $ExtraQty;
                                        if($row['intOrderNo'] == 0){
                                            $orderNo = '';
                                            $salesOrderNo = '';
                                        }
                                        if($mrnType_Details == 6){
                                            $backgroundColor = "bgcolor='#FFE6E6'";
                                        }else{
                                            $backgroundColor = "bgcolor='#FFFFFF'";
                                        }
                                        if($mrnType_Details==3)
                                            $mrnType_dec='Order based';
                                        else if($mrnType_Details==6)
                                            $mrnType_dec='Order based Additional';
                                        else if($mrnType_Details==5)
                                            $mrnType_dec='Non Order based';
                                        else if($mrnType_Details==2)
                                            $mrnType_dec='Technical R&D';
                                        else if($mrnType_Details==1)
                                            $mrnType_dec='Sample';
                                        else if($mrnType_Details==0)
                                            $mrnType_dec='Non RM';
                                        ?>
                                        <tr class="normalfnt"  <?php echo $backgroundColor; ?> id="dataRowRpt">
                                            <td class="normalfnt mrnType" id="<?php echo $mrnType_Details; ?>" nowrap>&nbsp;<?php echo $mrnType_dec; ?></td>
											 <td class="normalfntRight"><?php echo $row['Customer'];?></td>
                                            <td class="normalfnt" id="rptOrderNo"><?php echo $orderNo; ?></td>
                                            <td class="StrStyle" id="<?php echo $row['strStyleNo']; ?>">&nbsp;<?php echo $salesOrderNo; ?>&nbsp;</td>
											<td class="normalfntRight"><?php echo $row['Brand'];?></td>
                                            <td class="normalfnt" nowrap id="rptMainCat">&nbsp;<?php echo $row['mainCategory']; ?>&nbsp;</td>
                                            <td class="normalfnt" nowrap id="rptSubCat">&nbsp;<?php echo $row['subCategory']; ?>&nbsp;</td>
                                            <td class="normalfnt" nowrap id="itemCode">&nbsp;<?php echo $row['strCode']; ?>&nbsp;</td>
                                            <td class="itemIdRpt" nowrap id="<?php echo $row['itemId']; ?>">&nbsp;<?php echo $row['strName']; ?>&nbsp;</td>
                                            <td class="normalfntMid" id="unitOfMe"><?php echo $row['uom']; ?></td>
                                            <td class="normalfntRight" id="rptOrderQtyWithoutExtra"><?php echo round($QtyWithoutExtra,4); ?></td>
                                            <td class="normalfntRight" id="rptExtra"><?php echo round($ExtraQty,4); ?></td>
                                            <td class="normalfntRight"><?php echo $orderQty;?></td>
                                            
                                           
                                        </tr>
                                        <?php
                                        $totQty_S += $row['dblQty'];
                                        $totAmmount_S += $ExtraQty;
                                        $totWithoutExtra_S += $QtyWithoutExtra;
                                    }
                                    ?>
                                    <tr class="normalfnt"  bgcolor="#CCCCCC">
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>                                   
										<td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
										 <td class="normalfntRight"><?php echo round($totWithoutExtra_S,4); ?></td>
                                        <td class="normalfntRight"><?php echo $totAmmount_S; ?></td>
                                        <td class="normalfntRight"><?php echo $totQty_S; ?></td>
                                    </tr>
                                    <?php
		  $body 			= ob_get_clean();
	}
	
	
	return $body;
}
?>
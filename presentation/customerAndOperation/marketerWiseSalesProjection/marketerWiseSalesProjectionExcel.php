<?php
session_start();
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=marketer_wise_sales_report.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");

$startDate = $_REQUEST['startDate'];
$endDate = $_REQUEST['endDate'];
$marketer = $_REQUEST['marketer'];
$location = $_REQUEST['location'];
$type = $_REQUEST['filterBy'] == '0'?"Order Delivery Date":($_REQUEST['filterBy'] == '1'?"Order Created Date":"Order Dispatched Date");
$dataArray = $_SESSION['SALES_PROJECTION_DATA'];

$amountForPeriod = $dataArray['amountForPeriod'];
$records = $dataArray['records'];
$currSymbol			= '$';

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <th width="100%"
            colspan="9" style="font-size:18px"><?php echo "Marketer-wise Sales Report By ".$type.": From ".$startDate." To ".$endDate;  ?></th>
    </tr>
    <tr>
        <th width="100%" colspan="9"></th>
    </tr>
</table>
<table width="100%" border="1" cellspacing="0" cellpadding="0">
    <tr height="50">
        <th width="2%" height="23">Customer</th>
        <th width="2%" height="23">Marketer</th>
        <th width="6%" height="23">Order No</th>
        <th width="5%" height="23">order Year</th>
        <th width="5%" height="23">Sales Order</th>
        <th width="5%" height="23">Style No</th>
        <th width="6%" height="23">Graphic No</th>
        <th width="5%" height="23">Sample Year</th>
        <th width="6%" height="23">Sample No</th>
        <th width="3%" height="23">Rev No</th>
        <th width="5%">Combo</th>
        <th width="5%">Print/Part</th>
        <th width="3%" height="23">Brand</th>
        <th width="5%">Ground Color</th>
        <th width="3%">Order Qty</th>
        <th width="3%">Dispatched Good-Qty For This Period</th>
        <th width="3%">Dispatched Damaged-Qty For This Period</th>
        <th width="3%">Dispatched Good-Qty Till End Date</th>
        <th width="3%">Dispatched Damaged-Qty Till End Date</th>
        <th width="3%">Unit Price</th>
        <th width="4%">Dispatched Amount For This Period(USD)</th>
        <th width="12%">PSD</th>
        <th width="12%">Order Delivery Date</th>
        <th width="12%">Order Created Date</th>
        <th width="12%">Order Dispatched Date</th>
        <th width="7%">PO No</th>
    </tr>
    <?php

    for($i=0;$i<sizeof($records);$i++){
        $sampleNo = strip_tags($records[$i]['sampleNo']);
        $sampleYear = strip_tags($records[$i]['sampleYear']);
        $revNo = strip_tags($records[$i]['revNo']);
        ?>
        <tr>
        <td align="center" width="8%" title="Customer"><?php echo strip_tags($records[$i]["customer"]); ?></td>
        <td align="center" width="8%" title="Marketer"><?php echo strip_tags($records[$i]["marketer"]); ?></td>
        <td align="center" width="6%" class="orderNo" title="Order No"><?php echo strip_tags($records[$i]['orderNo']); ?></td>
        <td align="center" width="5%" class="orderYear" title="Order Year"><?php echo strip_tags($records[$i]['orderYear']); ?></td>
        <td align="center" class="salesOrderId" id="<?php echo strip_tags($records[$i]['salesOrderId']); ?>"
            title="sales Order No"><?php echo strip_tags($records[$i]['salesOrderNo']); ?></td>
        <td align="center" width="5%" class="styleNo" title="Style No"><?php echo strip_tags($records[$i]['styleNo']); ?></td>
        <td align="center" width="6%" class="graphicNo" title="Graphic No"><?php echo strip_tags($records[$i]['graphicNo']); ?></td>
        <td align="center" width="5%" title="Sample Year"><?php echo strip_tags($records[$i]['sampleYear']); ?></td>
        <td align="center" width="6%" title="Sample No"><?php echo strip_tags($records[$i]['sampleNo']); ?></td>
        <td width="3%" align="center" title="Revision No"><?php echo strip_tags($records[$i]['revNo']); ?></td>
        <td width="5%" align="center" title="Combo"><?php echo strip_tags($records[$i]['combo']); ?></td>
        <td width="5%" align="center" title="Print Name"><?php echo strip_tags($records[$i]['print'].'/'.$records[$i]['part']); ?></td>
        <td align="center" title="Brand">&nbsp;<?php echo strip_tags($records[$i]['brand']); ?></td>
        <td align="center" title="Color">&nbsp;<?php echo strip_tags($records[$i]['color']); ?></td>
        <td align="right" title="Order Qty">&nbsp;<?php echo strip_tags($records[$i]['orderQty']); ?></td>
        <td align="right" title="Dispatched Qty for this month">&nbsp;<?php echo strip_tags($records[$i]['dispQtyThisMonth']); ?></td>
        <td align="right" title="Damaged Qty for this month">&nbsp;<?php echo strip_tags($records[$i]['damageQtyThisMonth']); ?></td>
        <td align="right" title="Dispatched Qty till this month-end"><?php echo strip_tags($records[$i]['dispQtyUntilMonth']); ?></td>
         <td align="right" title="Damaged Qty till this month-end"><?php echo strip_tags($records[$i]['damageQtyUntilMonth']); ?></td>
        <td align="right" title="Unit Price">&nbsp;<?php echo strip_tags($records[$i]['price']); ?></td>
        <td align="right" title="Dispatched Amount For This Period(USD)"><?php echo strip_tags($records[$i]['amount']); ?></td>
        <td align="center" title="PSD(day)">&nbsp;<?php echo strip_tags($records[$i]['psdDate']); ?></td>
        <td align="center" title="Delivery Date">&nbsp;<?php echo strip_tags($records[$i]['dtDeliveryDate']); ?></td>
        <td align="center" title="Order created Date">&nbsp;<?php echo strip_tags($records[$i]['dtmCreateDate']); ?></td>
        <td align="center" title="Last Dispatched Date">&nbsp;<?php echo strip_tags($records[$i]['lastDispatchDate']); ?></td>
        <td align="center" title="PO No">&nbsp;<?php echo strip_tags($records[$i]['strCustomerPoNo']); ?></td>
        </tr>
        <?php
    }
    ?>
</table>
<div> <table width="1950" class="bordered" id="tblMain" border="0">
        <tr><td width="100%" colspan="3"></td> </tr>
        <tr>
            <td width="60%" align="left"  bgcolor="#FFFFFF" style="font-size:14px" >&nbsp;</td>
            <td width="27%" align="left"  bgcolor="#FFFFFF">Total Dispatched </td>
            <td align="right"  bgcolor="#FFFFFF" style="font-size:14px" ><b><?php echo $currSymbol.' '.number_format(round($amountForPeriod),2); ?></b></td>
        </tr>
    </table></div>

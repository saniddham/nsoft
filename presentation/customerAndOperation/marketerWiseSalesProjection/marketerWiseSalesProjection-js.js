var basepath	= 'presentation/customerAndOperation/marketerWiseSalesProjection/';
// JavaScript Document
$(document).ready(function(){
	$("#frmMWSProjection").validationEngine();	
	$('#startDate').die('change').live('change',pageSubmitOnChane);
	$('#endDate').die('change').live('change',pageSubmitOnChane);
	//$('#cboMarketer').die('change').live('change',pageSubmitOnChane);
    $('#cboLocation').die('change').live('change',pageSubmitOnChane);
    $('#cboDateType').die('change').live('change',pageSubmitOnChane);
	$('#butInsertRow').die('click').live('click',addNewRow);	
	$('.clsDel').die('click').live('click',deleteRow);
	$('.clsDelSaved').die('click').live('click',deleteSavedRow);
	$('.clsDelSysSaved').die('click').live('click',deleteSysSavedRow);
	$('.clsSave').die('click').live('click',saveData);
	$('#butView').die('click').live('click',pageSubmit);
	$('#butViewActuals').die('click').live('click',viewActualReport);
 	$('.clsEdit').die('click').live('click',activeEditMode);
    $('#butReport').die('click').live('click', monthlySummery);
	
	$('.clsOrderQty').die('keyup').live('keyup',function(){
		setAmount('order',this);
	});
	$('.clsUnitPrice').die('keyup').live('keyup',function(){
		setAmount('unit',this);
	});
	
	$(".clsGraphicNo").die('change').live('change',function(){
		loadComboes('','graphic',1,1,1,1,1,this);
	});
	$(".clsSampleYear").die('change').live('change',function(){
		loadComboes('','year',0,1,1,1,1,this);
	});
	$(".clsSampleNo").die('change').live('change',function(){
		loadComboes('','sampleNo',0,0,1,1,1,this);
	});
	$(".clsRevNo").die('change').live('change',function(){
		loadComboes('','revNo',0,0,0,1,1,this);
	});	
	$(".clsCombo").die('change').live('change',function(){
		loadComboes('','combo',0,0,0,0,1,this);
	});	
	$(".clsPrint").die('change').live('change',loadImg);

    $("#frmMWSProjection .chosen-select").chosen({allow_single_deselect: false , no_results_text: "Oops, nothing found!",width:"100%"});

});

function addNewRow()
{
	var maxId=0;
	$('.clsPSD').each(function(){
		var id 		=	$(this).attr('id');
		var numb = id.match(/\d/g);
		numb = parseFloat(numb.join(""));
		
		if((maxId<numb))
		{
			maxId=numb;
		}
	});
	maxId	= maxId+1;
	maxId	= 'txtPSD'+maxId;

	var rowCount 	= document.getElementById('tblMain').rows.length;
	document.getElementById('tblMain').insertRow(rowCount-1);
	
	rowCount 	 	= document.getElementById('tblMain').rows.length;
	document.getElementById('tblMain').rows[rowCount-2].innerHTML = document.getElementById('tblMain').rows[rowCount-3].innerHTML;
	var a = rowCount-2;
	
	$('#tblMain tr:eq('+a+')').find('.clsSaved').html('<img border="0" src="images/del.png" alt="Save" name="butDel" class="mouseover clsDel" id="butDel" tabindex="24"/>');
	$('#tblMain tr:eq('+a+')').find('.clsEdited').html('&nbsp;');
	$('#tblMain tr:eq('+a+')').find('.clsSaved').attr('id',0);	
	$('#tblMain tr:eq('+a+')').find('.clsCustomer').val('');
	$('#tblMain tr:eq('+a+')').find('.clsOrderNo').val('');
	$('#tblMain tr:eq('+a+')').find('.clsOrderYear').val('');
	$('#tblMain tr:eq('+a+')').find('.clsStyleNo').val('');
	$('#tblMain tr:eq('+a+')').find('.clsSalesOrder').val('');
 	$('#tblMain tr:eq('+a+')').find('.clsGraphicNo').val('');
	$('#tblMain tr:eq('+a+')').find('.clsSampleYear').html('<option value=""></option>');
	$('#tblMain tr:eq('+a+')').find('.clsSampleNo').html('<option value=""></option>');
	$('#tblMain tr:eq('+a+')').find('.clsRevNo').html('<option value=""></option>');
	$('#tblMain tr:eq('+a+')').find('.clsCombo').html('<option value=""></option>');
	$('#tblMain tr:eq('+a+')').find('.clsPrint').html('<option value=""></option>');
	$('#tblMain tr:eq('+a+')').find('.clsPicture').html('');
	$('#tblMain tr:eq('+a+')').find('.clsBrand').html('&nbsp;');
	$('#tblMain tr:eq('+a+')').find('.clsColors').html('&nbsp;');
	$('#tblMain tr:eq('+a+')').find('.clsDescription').val('');
	$('#tblMain tr:eq('+a+')').find('.clsOrderQty').val(0);
	$('#tblMain tr:eq('+a+')').find('.clsUnitPrice').val(0);
	$('#tblMain tr:eq('+a+')').find('.clsPrice').val(0);
	$('#tblMain tr:eq('+a+')').find('.clsPSD').val('');
	$('#tblMain tr:eq('+a+')').find('.clsPONo').val('');
	$('#tblMain tr:eq('+a+')').find('.clsPSD').attr('id',maxId);	
	$('#tblMain tr:eq('+a+')').find('.clsCustomer').attr("disabled", false);
	$('#tblMain tr:eq('+a+')').find('.clsOrderNo').attr("disabled", false);
	$('#tblMain tr:eq('+a+')').find('.clsOrderYear').attr("disabled", false);
	$('#tblMain tr:eq('+a+')').find('.clsStyleNo').attr("disabled", false);
	$('#tblMain tr:eq('+a+')').find('.clsSalesOrder').attr("disabled", false);
 	$('#tblMain tr:eq('+a+')').find('.clsGraphicNo').attr("disabled", false);
	$('#tblMain tr:eq('+a+')').find('.clsSampleYear').attr("disabled", false);
	$('#tblMain tr:eq('+a+')').find('.clsSampleNo').attr("disabled", false);
	$('#tblMain tr:eq('+a+')').find('.clsRevNo').attr("disabled", false);
	$('#tblMain tr:eq('+a+')').find('.clsCombo').attr("disabled", false);
	$('#tblMain tr:eq('+a+')').find('.clsPrint').attr("disabled", false);
	$('#tblMain tr:eq('+a+')').find('.clsDescription').attr("disabled", false);
	$('#tblMain tr:eq('+a+')').find('.clsOrderQty').attr("disabled", false);
	$('#tblMain tr:eq('+a+')').find('.clsUnitPrice').attr("disabled", false);
	$('#tblMain tr:eq('+a+')').find('.clsPSD').attr("disabled", false);
	$('#tblMain tr:eq('+a+')').find('.clsPONo').attr("disabled", false);
	$('#tblMain tr:eq('+a+')').find('.clsbtnSave').html('<img border="0" src="images/Tsave.jpg" alt="Save" name="butSave" height="20" class="mouseover clsSave" id="butSave" tabindex="24" />');
}

function activeEditMode()
{
 	$(this).parent().parent().find('.clsCustomer').attr("disabled", false);
	$(this).parent().parent().find('.clsOrderNo').attr("disabled", false);
	$(this).parent().parent().find('.clsOrderYear').attr("disabled", false);
	$(this).parent().parent().find('.clsStyleNo').attr("disabled", false);
	$(this).parent().parent().find('.clsSalesOrder').attr("disabled", false);
  	$(this).parent().parent().find('.clsGraphicNo').attr("disabled", false);
	$(this).parent().parent().find('.clsSampleYear').attr("disabled", false);
	$(this).parent().parent().find('.clsSampleNo').attr("disabled", false);
	$(this).parent().parent().find('.clsRevNo').attr("disabled", false);
	$(this).parent().parent().find('.clsCombo').attr("disabled", false);
	$(this).parent().parent().find('.clsPrint').attr("disabled", false);
	$(this).parent().parent().find('.clsDescription').attr("disabled", false);
	$(this).parent().parent().find('.clsOrderQty').attr("disabled", false);
	$(this).parent().parent().find('.clsUnitPrice').attr("disabled", false);
	$(this).parent().parent().find('.clsPSD').attr("disabled", false);
	$(this).parent().parent().find('.clsPONo').attr("disabled", false);
	$(this).parent().parent().find('.clsbtnSave').html('<img border="0" src="images/Tsave.jpg" alt="Save" name="butSave" height="20" class="mouseover clsSave" id="butSave" tabindex="24" />');	
}

function deleteRow()
{
	var rowCount = parseInt(savedRowCount);
	var delRowCount = parseInt(document.getElementById('tblMain').rows.length);
	
	if(delRowCount>3)
		$(this).parent().parent().remove();
}

function deleteSavedRow()
{
	var rowId 		= $(this).parent().parent().find('.clsSaved').attr('id');
	var year 		= $('#cboYear').val();
	var month 		= $('#cboMonth').val();
	var marketer 	= $('#cboMarketer').val();
	var obj			= this;
	
	var rowCount = parseInt(savedRowCount);
	var delRowCount = parseInt(document.getElementById('tblMain').rows.length);

	var val = $.prompt('Are you sure you want to delete "'+$(this).parent().parent().find('.clsOrderNo').val()+'/'+$(this).parent().parent().find('.clsOrderYear').val()+'" ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
					if(v)
					{
						var url = basepath+"marketerWiseSalesProj-db-set.php?requestType=deleteData";
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							data:'&rowId='+rowId+'&year='+year+'&month='+month+'&marketer='+marketer,
							async:false,
							success:function(json){
								if(json.type=='pass')
								{
									$(obj).parent().parent().remove();
									return;
								}
								else
								{
									$(obj).validationEngine('showPrompt', json.msg,json.type /*'pass'*/);	
									return;
								}
							},
							error:function(xhr,status){
									
								$(obj).validationEngine('showPrompt', errormsg(xhr.status),'fail');
								return;
							}		
								 
						});
					}
				}
		 	});
}

function deleteSysSavedRow()
{
	var orderNo 		= $(this).parent().parent().find('.orderNo').html();
	var orderYear 		= $(this).parent().parent().find('.orderYear').html();
	var salesOrderId 	= $(this).parent().parent().find('.salesOrderId').attr('id');
	var obj				= this;	
	var rowCount 		= parseInt(savedRowCount);
	var delRowCount 	= parseInt(document.getElementById('tblMain').rows.length);

	var val = $.prompt('Are you sure you want to delete "'+$(this).parent().parent().find('.orderNo').html()+'/'+$(this).parent().parent().find('.orderYear').html()+'/'+$(this).parent().parent().find('.styleNo').html()+'/'+$(this).parent().parent().find('.graphicNo').html()+'" ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
					if(v)
					{
						var url = basepath+"marketerWiseSalesProj-db-set.php?requestType=deleteSystemOrders";
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							data:'&orderNo='+orderNo+'&orderYear='+orderYear+'&salesOrderId='+salesOrderId,
							async:false,
							success:function(json){
								if(json.type=='pass')
								{
									$(obj).parent().parent().remove();
									return;
								}
								else
								{
									$(obj).validationEngine('showPrompt', json.msg,json.type /*'pass'*/);	
									return;
								}
							},
							error:function(xhr,status){
									
								$(obj).validationEngine('showPrompt', errormsg(xhr.status),'fail');
								return;
							}		
								 
						});
					}
				}
		 	});
}

function loadComboes(saveValue,slected,flag1,flag2,flag3,flag4,flag5,obj)
{
	if($(obj).val()=="")
	{
		$(obj).parent().parent().find(".clsPicture").html('');
		if(flag1==1)
			$(obj).parent().parent().find(".clsSampleYear").html('');
		if(flag2==1)
			$(obj).parent().parent().find(".clsSampleNo").html('');
		if(flag3==1)
			$(obj).parent().parent().find(".clsRevNo").html('');
		if(flag4==1)
			$(obj).parent().parent().find(".clsCombo").html('');
		if(flag5==1)
			$(obj).parent().parent().find(".clsPrint").html('');
		if(flag1==1 || flag2==1 || flag3==1 || flag4==1 || flag5==1 )
		{
			$(obj).parent().parent().find(".clsBrand").html('&nbsp;');
			$(obj).parent().parent().find(".clsColors").html('&nbsp;');
			$(obj).parent().parent().find('.clsUnitPrice').val('');
		}
		return;
	}	
	
	if(flag1==1)
		$(obj).parent().parent().find(".clsSampleYear").html('');
	if(flag2==1)
		$(obj).parent().parent().find(".clsSampleNo").html('');
	if(flag3==1)
		$(obj).parent().parent().find(".clsRevNo").html('');
	if(flag4==1)
		$(obj).parent().parent().find(".clsCombo").html('');
	if(flag5==1)
		$(obj).parent().parent().find(".clsPrint").html('');
	if(flag1==1 || flag2==1 || flag3==1 || flag4==1 || flag5==1 )
	{
		$(obj).parent().parent().find(".clsBrand").html('&nbsp;');
		$(obj).parent().parent().find(".clsColors").html('&nbsp;');
		$(obj).parent().parent().find('.clsUnitPrice').val('');
	}

	var graphic		= $(obj).parent().parent().find(".clsGraphicNo").val();	
	var sampYear	= $(obj).parent().parent().find(".clsSampleYear").val();
	var sampNo 		= $(obj).parent().parent().find(".clsSampleNo").val();
	var revNo 	 	= $(obj).parent().parent().find(".clsRevNo").val();
	var combo 		= $(obj).parent().parent().find(".clsCombo").val();
	var cboprint 	= ($(obj).parent().parent().find(".clsPrint").val()==null?'':$(obj).parent().parent().find(".clsPrint").val());
	
	if(sampYear==null)
		sampYear='';
	if(sampNo==null)
		sampNo='';
	if(revNo==null)
		revNo='';
	if(combo==null)
		combo='';
	
	var url 		= basepath+"marketerWiseSalesProj-db-get.php?requestType=loadAllComboes";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:"&graphic="+URLEncode(graphic)+"&sampYear="+sampYear+"&sampNo="+sampNo+"&revNo="+revNo+"&combo="+combo+"&slected="+slected+"&saveValue="+saveValue,
		async:false,
		success:function(json)
		{
			if(slected=='graphic')
			$(obj).parent().parent().find(".clsSampleYear").html(json.combo)
			if(slected=='year')
			$(obj).parent().parent().find(".clsSampleNo").html(json.combo)
			if(slected=='sampleNo')
			$(obj).parent().parent().find(".clsRevNo").html(json.combo)
			if(slected=='revNo')
			$(obj).parent().parent().find(".clsCombo").html(json.combo)
			if(slected=='combo')
			$(obj).parent().parent().find(".clsPrint").html(json.combo)
		
			$(obj).parent().parent().find(".clsPicture").html('');
			$(obj).parent().parent().find(".clsBrand").html(json.brand);
			$(obj).parent().parent().find(".clsColors").html(json.color);
			
		}
	});
}

function loadImg()
{
	var sampYear	= $(this).parent().parent().find(".clsSampleYear").val();
	var sampNo 		= $(this).parent().parent().find(".clsSampleNo").val();
	var revNo 	 	= $(this).parent().parent().find(".clsRevNo").val();
	var cboprint 	= $(this).parent().parent().find(".clsPrint").val();
	var combo 		= $(this).parent().parent().find(".clsCombo").val();
	var printNo		= cboprint.substring(6,7);
	
	if($(this).val()=='')
		$(this).parent().parent().find(".clsPicture").html('');
	else
		$(this).parent().parent().find(".clsPicture").html('<img id=\"saveimg\" style=\"width:70px;height:70px;\" src=\"documents/sampleinfo/samplePictures/'+sampNo+'-'+sampYear+'-'+revNo+'-'+printNo+'.png\" />');
	
	var url = basepath+"marketerWiseSalesProj-db-get.php?requestType=getPrice&sampleNo="+sampNo+"&sampleYear="+sampYear+"&revId="+revNo+"&printName="+cboprint+"&comboId="+combo;
	var obj =  $.ajax({url:url,async:false});
	$(this).parent().parent().find('.clsUnitPrice').val(obj.responseText);	
}

function pageSubmit()
{
	if($("#frmMWSProjection").validationEngine('validate'))
	{
		document.location.href = '?q=668&startDate='+$('#startDate').val()+'&endDate='+$('#endDate').val()+'&cboMarketer='+$('#cboMarketer').val()+'&cboLocation='+$('#cboLocation').val()+'&cboDateType='+$('#cboDateType').val();
	}
}

function pageSubmitOnChane()
{
	var startDate	= $('#startDate').val();
	var endDate 		= $('#endDate').val();
	var marketer 	= $('#cboMarketer').val();
	var location    = $('#cboLocation').val();
	var filterBy	= $('#cboDateType').val();

	
	if(startDate!='' && endDate!='' && filterBy!='')
	{
		document.location.href = '?=668&startDate='+$('#startDate').val()+'&endDate='+$('#endDate').val()+'&cboMarketer='+$('#cboMarketer').val()+'&cboLocation='+$('#cboLocation').val()+'&cboDateType='+$('#cboDateType').val();
	}
}

function saveData()
{
	var obj 		 = this;

	if(!$("#frmMWSProjection").validationEngine('validate'))
		return;
		
	// if($('#cboMarketer').val()=='')
	// {
	// 	$('#cboMarketer').validationEngine('showPrompt', 'Please select Marketer.','fail');
	// 	return;
	// }
	if($(this).parent().parent().find('.clsCustomer').val()=='')
	{
		$(this).parent().parent().find('.clsPSD').validationEngine('showPrompt', 'Please select Customer.','fail');
		return;
	}
	if($(this).parent().parent().find('.clsGraphicNo').val()=='')
	{
		$(this).parent().parent().find('.clsPSD').validationEngine('showPrompt', 'Please select Graphic No.','fail');
		return;
	}
	if($(this).parent().parent().find('.clsSampleYear').val()=='')
	{
		$(this).parent().parent().find('.clsPSD').validationEngine('showPrompt', 'Please select Sample Year.','fail');
		return;
	}
	if($(this).parent().parent().find('.clsSampleNo').val()=='')
	{
		$(this).parent().parent().find('.clsPSD').validationEngine('showPrompt', 'Please select Sample No.','fail');
		return;
	}
	if($(this).parent().parent().find('.clsRevNo').val()=='')
	{
		$(this).parent().parent().find('.clsPSD').validationEngine('showPrompt', 'Please select Revision No.','fail');
		return;
	}
	if($(this).parent().parent().find('.clsCombo').val()=='')
	{
		$(this).parent().parent().find('.clsPSD').validationEngine('showPrompt', 'Please select Combo.','fail');
		return;
	}
	if($(this).parent().parent().find('.cboPrint').val()=='')
	{
		$(this).parent().parent().find('.clsPSD').validationEngine('showPrompt', 'Please select Print.','fail');
		return;
	}
	
	var startDate 		= $('#startDate').val();
	var endDate 		= $('#endDate').val();
	var marketer 		= $('#cboMarketer').val();
	var customer 		= $(this).parent().parent().find('.clsCustomer').val();
	var orderNo 		= $(this).parent().parent().find('.clsOrderNo').val();
	var orderYear 		= $(this).parent().parent().find('.clsOrderYear').val();
	var salesOrder 		= $(this).parent().parent().find('.clsSalesOrder').val();
	var style 			= $(this).parent().parent().find('.clsStyleNo').val();
	var graphicNo 		= $(this).parent().parent().find('.clsGraphicNo').val();
	var sampleYear 		= $(this).parent().parent().find('.clsSampleYear').val();
	var sampleNo 		= $(this).parent().parent().find('.clsSampleNo').val();
	var revisionNo 		= $(this).parent().parent().find('.clsRevNo').val();
	var combo 			= $(this).parent().parent().find('.clsCombo').val();
	var cboprint 		= $(this).parent().parent().find('.clsPrint').val();
	var description 	= $(this).parent().parent().find('.clsDescription').val();
	var orderQty 		= $(this).parent().parent().find('.clsOrderQty').val();
	var unitPrice 		= $(this).parent().parent().find('.clsUnitPrice').val();
	var amount 			= $(this).parent().parent().find('.clsPrice').val();
	var psdDay 			= $(this).parent().parent().find('.clsPSD').val();
	var PONo 			= $(this).parent().parent().find('.clsPONo').val();
	var saved			= $(this).parent().parent().find('.clsSaved').attr('id');
	
	if((orderQty=='') || (orderQty==0)){
		alert('Please enter Order Qty');
		return false;
	}
	else if((unitPrice=='') || (unitPrice==0)){
		alert('There is no price');
		return false;
	} 		
		
	var url = basepath+"marketerWiseSalesProj-db-set.php?requestType=saveData";
		$.ajax({
		url:url,
		async:false,
		dataType:'json',
		type:'post',
		data:'&startDate='+year+'&endDate='+month+'&marketer='+marketer+'&customer='+customer+'&orderNo='+orderNo+'&orderYear='+orderYear+'&style='+URLEncode(style)+'&salesOrder='+URLEncode(salesOrder)+'&graphicNo='+URLEncode(graphicNo)+'&sampleYear='+sampleYear+'&sampleNo='+sampleNo+'&revisionNo='+revisionNo+'&combo='+combo+'&cboprint='+cboprint+'&description='+URLEncode(description)+'&orderQty='+orderQty+'&unitPrice='+unitPrice+'&amount='+amount+'&psdDay='+psdDay+'&PONo='+PONo+'&saved='+saved,
		success:function(json){				
			$(obj).validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
			if(json.type=='pass')
			{
				var t=setTimeout("alertx()",3000);
				$(obj).parent().parent().find('.clsSaved').attr('id',json.rowId);					
				$(obj).parent().parent().find('.clsCustomer').attr("disabled", true);
				$(obj).parent().parent().find('.clsOrderNo').attr("disabled", true);
				$(obj).parent().parent().find('.clsOrderYear').attr("disabled", true);
				$(obj).parent().parent().find('.clsSalesOrder').attr("disabled", true);
				$(obj).parent().parent().find('.clsStyleNo').attr("disabled", true);
				$(obj).parent().parent().find('.clsGraphicNo').attr("disabled", true);
				$(obj).parent().parent().find('.clsSampleYear').attr("disabled", true);
				$(obj).parent().parent().find('.clsSampleNo').attr("disabled", true);
				$(obj).parent().parent().find('.clsRevNo').attr("disabled", true);
				$(obj).parent().parent().find('.clsCombo').attr("disabled", true);
				$(obj).parent().parent().find('.clsPrint').attr("disabled", true);
				$(obj).parent().parent().find('.clsDescription').attr("disabled", true);
				$(obj).parent().parent().find('.clsOrderQty').attr("disabled", true);
				$(obj).parent().parent().find('.clsUnitPrice').attr("disabled", true);
				$(obj).parent().parent().find('.clsPSD').attr("disabled", true);
				$(obj).parent().parent().find('.clsPONo').attr("disabled", true);
				$(obj).parent().parent().find('.clsbtnSave').html('&nbsp;');
				
				if(json.delMode==1)
					$(obj).parent().parent().find('.clsSaved').html('<img border="0" src="images/del.png" alt="Save" name="butDelSaved" class="mouseover clsDelSaved" id="butDelSaved" tabindex="24"/>');	
				else
					$(obj).parent().parent().find('.clsSaved').html('&nbsp;');
				if(json.editMode==1)
					$(obj).parent().parent().find('.clsEdited').html('<img border="0" src="images/edit.png" alt="Save" name="butEdit" class="mouseover clsEdit" id="butEdit" tabindex="24" />');
				else
					$(obj).parent().parent().find('.clsEdited').html('&nbsp;');
				
				return;
			}
		},
		error:function(){			
			$(obj).validationEngine('showPrompt', errormsg(xhr.status),'fail');	
			return;				
		}
	});		
}

function setAmount(type,obj)
{		
	if(type=='order')
	{
		var orderQty  = parseFloat(($(obj).val()==''?0:$(obj).val()));
		var unitPrice = parseFloat(($(obj).parent().parent().find('.clsUnitPrice').val()==''?0:$(obj).parent().parent().find('.clsUnitPrice').val()));
		var amount	  = orderQty*unitPrice;
		
		$(obj).parent().parent().find('.clsPrice').val(amount)	
	}
	else
	{
		var unitPrice  = parseFloat(($(obj).val()==''?0:$(obj).val()));
		var orderQty   = parseFloat(($(obj).parent().parent().find('.clsOrderQty').val()==''?0:$(obj).parent().parent().find('.clsOrderQty').val()));
		var amount	   = orderQty*unitPrice;
		amount=Math.round(amount*Math.pow(10,2))/Math.pow(10,2)
		//amount=round(amount,4);
		$(obj).parent().parent().find('.clsPrice').val(amount)	
	}
}

function alertx()
{
	$('#frmMWSProjection').validationEngine('hide')	;
}

function viewActualReport(){
	
	if(($('#cboMarketer').val()!='')&& ($('#cboYear').val()!='')&& ($('#cboMonth').val()!='')){
		window.open('?q=892&marketer='+$('#cboMarketer').val()+'&year='+$('#cboYear').val()+'&month='+$('#cboMonth').val());	
	}
	else{
		alert("Please select marketer, year and month to view");
	}	
}

function monthlySummery() {
    var startDate	= $('#startDate').val();
    var endDate 	= $('#endDate').val();
    var marketer 	= $('#cboMarketer').val();
    var location    = $('#cboLocation').val();
    var filterBy	= $('#cboDateType').val();
    window.open(basepath + 'marketerWiseSalesProjectionExcel.php?startDate='+startDate+'&endDate='+endDate+'&location='+location+'&marketer='+marketer+'&filterBy='+filterBy);

}
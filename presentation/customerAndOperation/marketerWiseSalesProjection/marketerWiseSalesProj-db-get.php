<?php
session_start();
$backwardseperator 	= "../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$requestType 		= $_REQUEST['requestType'];

include "{$backwardseperator}dataAccess/Connector.php";

if($requestType=='loadAllComboes')
{
	$slected 	= $_REQUEST['slected'];
	$graphic 	= $_REQUEST['graphic'];
	$sampYear 	= $_REQUEST['sampYear'];
	$sampNo 	= $_REQUEST['sampNo'];
	$revNo 		= $_REQUEST['revNo'];
	$combo 		= $_REQUEST['combo'];
	$saveValue  = $_REQUEST['saveValue'];
	$brand		= '&nbsp;';
	$color		= '&nbsp;';
	
	$html		= loadAllComboes($slected,$graphic,$sampYear,$sampNo,$revNo,$combo,$saveValue);
	if($sampNo!='' && $sampYear!='' && $revNo!='' && $combo)
	{
		$brand		= loadBrand($sampYear,$sampNo,$revNo);
		$color		= loadGroundColor($sampYear,$sampNo,$combo,$revNo);
	}
	
	$response['combo']= $html;
	$response['brand']= $brand;
	$response['color']= $color;
	echo json_encode($response);
}
else if($requestType=='getPrice')
{
	$sampleNo  	= $_REQUEST['sampleNo'];
	$sampleYear	= $_REQUEST['sampleYear'];
	$revId		= $_REQUEST['revId'];
	$printName	= $_REQUEST['printName'];
	$comboId	= $_REQUEST['comboId'];	
	
	$sql = "SELECT
				trn_sampleinfomations_prices.dblPrice
				FROM trn_sampleinfomations_prices
			WHERE
				trn_sampleinfomations_prices.intSampleNo 		=  '$sampleNo' AND
				trn_sampleinfomations_prices.intSampleYear 		=  '$sampleYear' AND
				trn_sampleinfomations_prices.intRevisionNo 		=  '$revId' AND
				trn_sampleinfomations_prices.strCombo 			=  '$comboId' AND
				trn_sampleinfomations_prices.strPrintName 		=  '$printName'
			";
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	echo $row['dblPrice'];
}
function loadAllComboes($slected,$graphic,$sampYear,$sampNo,$revNo,$combo,$saveValue)
{
	global $db;
		$sql = "SELECT DISTINCT ";
		if($slected=='graphic')
		$sql .= "sd.intSampleYear as value ";
		if($slected=='year')
		$sql .= "sd.intSampleNo as value ";
		if($slected=='sampleNo')
		$sql .= "sd.intRevNo as value ";
		if($slected=='revNo')
		$sql .= "sd.strComboName as value ";
		if($slected=='combo')
		$sql .= "sd.strPrintName as value ";
	
		$sql .= "FROM trn_sampleinfomations_details sd
				 INNER JOIN trn_sampleinfomations s ON s.intSampleNo=sd.intSampleNo ";
		
		if($graphic!='')
		$sql .= " AND s.strGraphicRefNo =  '$graphic' ";
		if($sampYear!='')
		$sql .= " AND sd.intSampleYear =  '$sampYear' ";
		if($sampNo!='')
		$sql .= " AND sd.intSampleNo =  '$sampNo' ";
		if($revNo!='')
		$sql .= " AND sd.intRevNo =  '$revNo' ";
		if($combo!='')
		$sql .= " AND sd.strComboName =  '$combo' ";
		
		if($slected=='graphic')
		$sql .= "ORDER BY sd.intSampleYear DESC";	
		if($slected=='year')
		$sql .= "ORDER BY sd.intSampleNo DESC";
		if($slected=='sampNo')
		$sql .= "ORDER BY sd.intRevNo DESC";
		if($slected=='revNo')
		$sql .= "ORDER BY sd.strComboName DESC";
		if($slected=='combo')
		$sql .= "ORDER BY sd.strPrintName DESC";
		
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";

		while($row=mysqli_fetch_array($result))
		{
			if($saveValue==$row['value'])
				$html .= "<option value=\"".$row['value']."\" selected=\"selected\">".$row['value']."</option>";
			else
				$html .= "<option value=\"".$row['value']."\">".$row['value']."</option>";
		}
		return $html;	
}
function loadGroundColor($sampYear,$sampNo,$combo,$revNo)
{
	global $db;
	
	$sql = "SELECT DISTINCT mst_colors_ground.strName as groundColor
			FROM
			trn_sampleinfomations_details 
			INNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
			WHERE
			trn_sampleinfomations_details.intSampleNo = '$sampNo' AND
			trn_sampleinfomations_details.intSampleYear = '$sampYear'  AND
			trn_sampleinfomations_details.strComboName = '$combo' AND
			trn_sampleinfomations_details.intRevNo = '$revNo'
			LIMIT 1";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	
	return $row['groundColor'];
}
function loadBrand($sampYear,$sampNo,$revNo)
{
	global $db;
	
	$sql = "SELECT MB.strName AS brand
			FROM trn_sampleinfomations TSI
			INNER JOIN mst_brand MB ON MB.intId=TSI.intBrand
			WHERE TSI.intSampleNo = '$sampNo' AND
			TSI.intSampleYear = '$sampYear' AND
			TSI.intRevisionNo = '$revNo' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	
	return $row['brand'];
}
?>
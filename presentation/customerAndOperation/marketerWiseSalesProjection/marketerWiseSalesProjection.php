<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
ini_set('max_execution_time', 11111111) ;
$userId 	 		= $sessions->getUserId();
$locationId			= $sessions->getLocationId();
$companyId			= $sessions->getCompanyId();

require_once "class/cls_permisions.php";		$objpermisionget	= new cls_permisions($db);

$programCode		= 'P0668';
$toCurrency			= 1;
$currSymbol			= '$';

$startDate 			= (!isset($_REQUEST["startDate"])?date("Y-m-d"):$_REQUEST["startDate"]);
$endDate 			= (!isset($_REQUEST["endDate"])?date("Y-m-d"):$_REQUEST["endDate"]);
$location           = (!isset($_REQUEST["cboLocation"])?'':$_REQUEST["cboLocation"]);
$type               = (!isset($_REQUEST["cboDateType"])?0:$_REQUEST["cboDateType"]);

$cboMarketer		= (!isset($_REQUEST["cboMarketer"])?'':$_REQUEST["cboMarketer"]);
$marketerArray = explode(",",$cboMarketer);

$editMode 			= loadEditMode($programCode,$userId);
$deleteMode 		= loadDeleteMode($programCode,$userId);
$deleteSOMode 		= $objpermisionget->boolSPermision(13);
?>
<title>Marketer Wise Sales Projection</title>
<form id="frmMWSProjection" name="frmMWSProjection" autocomplete="off" method="post">
<div align="center">
<div class="trans_layoutXL">
<div class="trans_text">Marketer Wise Sales Projection</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
    	<td>
        	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
              <td height="26" class="normalfnt"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="normalfnt" width="5%">Start Date</td>
<!--                  <td class="normalfnt" width="6%">-->
<!--                      <select name="cboYear" id="cboYear" style="width:108px" class="validate[required]">-->
<!--                    <option value=""></option>-->
<!--                    --><?php
//				$sql ="SELECT DISTINCT intOrderYear AS intYear FROM trn_orderheader
//						UNION SELECT YEAR(CURDATE()) AS intYear UNION SELECT YEAR(CURDATE()) + 1 AS intYear Order by intYear";
//				echo $sql;
//				$result = $db->RunQuery($sql);
//				while($row = mysqli_fetch_array($result))
//				{
//					if($cboYear==$row['intYear'])
//						echo "<option value=\"".$row["intYear"]."\" selected=\"selected\" >".$row["intYear"]."</option>";
//					else
//						echo "<option value=\"".$row["intYear"]."\" >".$row["intYear"]."</option>";
//				}
//			  ?>
<!--                  </select>-->
                    <td width="12%" class="normalfnt"><input name="startDate" type="text" value="<?php echo $startDate;?>" class="validate[required]" id="startDate" style="width:98px;" onClick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;" onClick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                  <td class="normalfnt" width="4%" > End Date</td>
<!--                  <td class="normalfnt" width="10%">-->
<!--                      <select name="cboMonth" id="cboMonth" style="width:118px" class="validate[required]">-->
<!--                    <option value=""></option>-->
<!--                    --><?php
//				$sql ="SELECT intMonthId,strMonth FROM mst_month ORDER BY intMonthId";
//				$result = $db->RunQuery($sql);
//				while($row = mysqli_fetch_array($result))
//				{
//					if($cboMonth==$row['intMonthId'])
//						echo "<option value=\"".$row["intMonthId"]."\" selected=\"selected\" >".$row["strMonth"]."</option>";
//					else
//						echo "<option value=\"".$row["intMonthId"]."\" >".$row["strMonth"]."</option>";
//				}
//			  ?>
<!--                  </select>-->
<!--                  </td>-->
                    <td width="10%" class="normalfnt"><input name="endDate" type="text" value="<?php echo $endDate ?>" class="validate[required]" id="endDate" style="width:98px;" onClick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;" onClick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                  <td width="4%" class="normalfnt">Marketer</td>
                  <td width="16%" class="normalfnt"><select class="chosen-select clsorder" multiple="multiple"  data-placeholder="Select Your Option" name="cboMarketer" id="cboMarketer"  tabindex="4" style="width:180px height:10px">
                   <?php
                    $html   		        = "<option value=\"\"></option>";
				$sql ="SELECT DISTINCT
						mst_marketer.intUserId,
						sys_users.strFullName
						FROM
						mst_marketer
						INNER JOIN sys_users ON mst_marketer.intUserId = sys_users.intUserId
						INNER JOIN mst_locations_user ON mst_locations_user.intUserId = sys_users.intUserId	where mst_marketer.STATUS = '1'";
				$result = $db->RunQuery($sql);
				while($row = mysqli_fetch_array($result))
				{
				    if(in_array($row["intUserId"],$marketerArray))
                        $html.= "<option value=\"".$row["intUserId"]."\" selected=\"selected\" class=\"normalfnt\" >".$row["strFullName"]."</option>";
				    else
						$html.= "<option value=\"".$row["intUserId"]."\"  class=\"normalfnt\">".$row["strFullName"]."</option>";
				}
				echo $html;
			  ?>
                  </select></td>
                   <td width="6%" class="normalfnt" style="text-align:right">Location</td>
                    <td width="12%" class="normalfnt"><select name="cboLocation" id="cboLocation" style="width:180px">
                            <option value=""></option>
                            <?php
                            $sql ="SELECT mst_locations.intId AS locationId,
							concat(mst_companies.strName ,' - ',mst_locations.strName) AS companyName
							FROM
							mst_locations							
							Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
							WHERE
							mst_locations.intStatus=1";
                            $result = $db->RunQuery($sql);
                            while($row = mysqli_fetch_array($result))
                            {
                                if($location == $row['locationId'])
                                    echo "<option value=\"".$row["locationId"]."\" selected=\"selected\" class=\"normalfnt\">".$row["companyName"]."</option>";
                                else
                                    echo "<option value=\"".$row["locationId"]."\" class=\"normalfnt\">".$row["companyName"]."</option>";
                            }
                            ?>
                        </select></td>
                    <td class="normalfnt" width="6%" style="text-align:right">Filter By</td>
                    <td width="12%" class="normalfnt"><select name="cboDateType" id="cboDateType" style="width:160px" class="validate[required]">
                            <option value="0" class="normalfnt" <?php if($_REQUEST['cboDateType'] == 0) echo "selected='selected'"; ?>>Delivery Date</option>
                            <option value="1" class="normalfnt" <?php if($_REQUEST['cboDateType'] == 1) echo "selected='selected'"; ?>>Order Created Date</option>
                            <option value="2" class="normalfnt" <?php if($_REQUEST['cboDateType'] == 2) echo "selected='selected'"; ?>>Order Dispatched Date</option>
                        </select></td>
                    <td class="normalfnt" width="2%" style="text-align:right">&nbsp;</td>
                  <td class="normalfnt" width="6%" style="text-align:right"><a class="button white medium" id="butView">Search</a></td>
                    <td class="normalfnt" width="2%" style="text-align:right">&nbsp;</td>
                    <td width="6%" class="normalfntMid"><img src="images/Treport_exel.png" style="display:inline" border="0"  alt="view report" id="butReport" name="butReport" ;></td>
                    <td class="normalfnt" width="4%" style="text-align:right">&nbsp;</td>
<!--                  <td class="normalfnt" width="10%" style="text-align:right"><a class="button white medium" id="butViewActuals">View Actuals</a></td>-->
                </tr>
              </table></td>
            </tr>
            <tr>
              <td width="23%" height="25" class="normalfnt"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
<!--                  <td class="normalfnt" width="4%"><div style="width:40px; height:20px; background-color:#FFDDEE;  border-color:#000; border-style:ridge"></div></td>-->
<!--                  <td class="normalfnt" width="12%">Push back to this month</td>-->
<!--                  <td class="normalfnt" width="19%">&nbsp;</td>-->
<!--                  <td class="normalfnt" width="18%">&nbsp;</td>-->
<!--                  <td width="11%" class="normalfnt">&nbsp;</td>-->
<!--                  <td width="25%" class="normalfnt">&nbsp;</td>-->
<!--                  <td class="normalfnt" width="11%" style="text-align:right"></td>-->
                </tr>
              </table></td>
              </tr>
            <tr>
            	<td colspan="2"><div style="height:450px;overflow:scroll;width:1280px"  >
                <table width="1950" class="bordered" id="tblMain" >
                    <thead>
                     <tr>
<!--                       <th width="2%" height="23" >Del</th>-->
<!--                       <th width="2%" height="23" >Edit</th>-->
                       <th width="2%" height="23" >Customer</th>
                       <th width="2%" height="23" >Marketer</th>
                       <th width="6%" height="23" >Order No</th>
                       <th width="5%" height="23" >Order Year</th>
                       <th width="5%" height="23" >Sales Order</th>
                       <th width="5%" height="23" >Style No</th>
                       <th width="6%" height="23" >Graphic No</th>
                       <th width="5%" height="23" >Sample Year</th>
                       <th width="6%" height="23" >Sample No</th>
                       <th width="3%" height="23" >Rev No</th>
                       <th width="5%" >Combo</th>
                       <th width="5%" >Print/Part</th>
                       <th width="5%" >Graphic</th>
                       <th width="3%" height="23" >Brand</th>
                       <th width="5%" >Ground Color</th>
                       <th width="3%" >Order Qty</th>
                       <th width="3%">Dispatched Good-Qty For This Period</th>
                       <th width="3%">Dispatched Damaged-Qty For This Period</th>
                       <th width="3%">Dispatched Good-Qty Till End Date</th>
                       <th width="3%">Dispatched Damaged-Qty Till End Date</th>
                       <th width="3%" >Unit Price</th>
                       <th width="4%" >Dispatched Amount For This Period(USD)</th>
                       <th width="12%" >PSD</th>
                       <th width="12%">Delivery Date</th>
                       <th width="12%">Order Created Date</th>
                         <th width="12%">Last Dispatched Date</th>
                       <th width="7%" >PO No</th>
                     </tr>
                    </thead>
                    <?php
					/*if($cboMonth==12){
						$yearN=$cboYear+1;
						$monthN=1;

					}
					else{
						$monthN=$cboMonth+1;
						$yearN=$cboYear;
					}*/
				
 					/*$maxPsdDate=$yearN.'-'.sprintf("%02d",$monthN).'-01';
					$maxDispDate=$yearN.'-'.sprintf("%02d",($monthN)).'-01';
					$minDispDate=$cboYear.'-'.sprintf("%02d",($cboMonth)).'-01';*/

 					$maxDispDate = $endDate;
 					$maxPsdDate = $endDate;
 					$minDispDate = $startDate;
                    $amountSOP=0;
					$amountSOT=0;
					$amountMOP=0;
					$amountMOT=0;
					$amount	  =0;
                    $amountForPeriod =0;
					$rowCount = 0;
				   	 $sql =" SELECT OH.intOrderNo,
							OH.intOrderYear,
							OD.intSalesOrderId,
							OD.strSalesOrderNo,
							MC.strName AS customer,
							Marketer.strFullName AS marketer,
							OD.strStyleNo,
							OD.strGraphicNo,
							OD.intSampleYear,
							OD.intSampleNo,
							OD.intRevisionNo,
							OD.strCombo,
							OD.strPrintName,
							DATE(OH.dtmCreateDate) as dtmCreateDate,
                            OD.dtDeliveryDate,
							mst_part.strName as part, 
							OD.intViewInSalesProjection, 
							IFNULL(SUM(OD.intQty),0) AS orderQty,
							ROUND(OD.dblPrice,4) as dblPrice,
							ROUND(SUM(IFNULL(OD.dblPrice*OD.intQty,0)),4) AS amount,
							OD.dblPrice as price,
							DAY(dtPSD) AS psdDate1,
							(dtPSD) AS psdDate,
							(SELECT DISTINCT mst_colors_ground.strName
							FROM
							trn_sampleinfomations_details 
							INNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
							WHERE
							trn_sampleinfomations_details.intSampleNo =  OD.intSampleNo AND
							trn_sampleinfomations_details.intSampleYear =  OD.intSampleYear  AND
							trn_sampleinfomations_details.strPrintName =  OD.strPrintName AND
							trn_sampleinfomations_details.strComboName =  OD.strCombo AND
							trn_sampleinfomations_details.intRevNo =  OD.intRevisionNo
							LIMIT 1
							) AS color,
							(SELECT SUM(ware_stocktransactions_fabric.dblQty) 	FROM
				            ware_stocktransactions_fabric
			                WHERE
                            ware_stocktransactions_fabric.intOrderNo = OD.intOrderNo
                            AND ware_stocktransactions_fabric.intOrderYear = OD.intOrderYear
                            AND ware_stocktransactions_fabric.intSalesOrderId = OD.intSalesOrderId
                            AND ware_stocktransactions_fabric.strType = 'Dispatched_G'
                            AND ware_stocktransactions_fabric.dtDate >= '$startDate'
                            AND ware_stocktransactions_fabric.dtDate <= '$endDate'
                            GROUP BY
                                ware_stocktransactions_fabric.intOrderNo,
                                ware_stocktransactions_fabric.intOrderYear,
                                ware_stocktransactions_fabric.intSalesOrderId		
                        	) AS DispatchedQtyThisMonth,
                        	(
                            SELECT
                                SUM(
                                    ware_stocktransactions_fabric.dblQty
                                )
                            FROM
                                ware_stocktransactions_fabric
                            WHERE
                                ware_stocktransactions_fabric.intOrderNo = OD.intOrderNo
                            AND ware_stocktransactions_fabric.intOrderYear = OD.intOrderYear
                            AND ware_stocktransactions_fabric.intSalesOrderId = OD.intSalesOrderId
                            AND ware_stocktransactions_fabric.strType IN ('Dispatched_S','Dispatched_E','Dispatched_P','Dispatched_F','Dispatched_CUT_RET')
                            AND ware_stocktransactions_fabric.dtDate >= '$startDate'
                            AND ware_stocktransactions_fabric.dtDate <= '$endDate'
                            GROUP BY
                                ware_stocktransactions_fabric.intOrderNo,
                                ware_stocktransactions_fabric.intOrderYear,
                                ware_stocktransactions_fabric.intSalesOrderId
                            ) AS DamageQtyForThisPeriod,
                        	(SELECT SUM(ware_stocktransactions_fabric.dblQty) 	FROM
				            ware_stocktransactions_fabric
			                WHERE
                            ware_stocktransactions_fabric.intOrderNo = OD.intOrderNo
                            AND ware_stocktransactions_fabric.intOrderYear = OD.intOrderYear
                            AND ware_stocktransactions_fabric.intSalesOrderId = OD.intSalesOrderId
                            AND ware_stocktransactions_fabric.strType = 'Dispatched_G'
                            AND ware_stocktransactions_fabric.dtDate <= '$maxDispDate'                            
                            GROUP BY
                                ware_stocktransactions_fabric.intOrderNo,
                                ware_stocktransactions_fabric.intOrderYear,
                                ware_stocktransactions_fabric.intSalesOrderId		
                        	) AS DispatchedQtyUntilMonth,                        	
                            ( SELECT
                                SUM(
                                    ware_stocktransactions_fabric.dblQty
                                )
                            FROM
                                ware_stocktransactions_fabric
                            WHERE
                                ware_stocktransactions_fabric.intOrderNo = OD.intOrderNo
                            AND ware_stocktransactions_fabric.intOrderYear = OD.intOrderYear
                            AND ware_stocktransactions_fabric.intSalesOrderId = OD.intSalesOrderId
                            AND ware_stocktransactions_fabric.strType IN ('Dispatched_S','Dispatched_E','Dispatched_P','Dispatched_F','Dispatched_CUT_RET')
                            AND ware_stocktransactions_fabric.dtDate <= '$maxDispDate'
                            GROUP BY
                                ware_stocktransactions_fabric.intOrderNo,
                                ware_stocktransactions_fabric.intOrderYear,
                                ware_stocktransactions_fabric.intSalesOrderId
                            ) AS DamageQtyUntilMonth,
                        	(
                            SELECT
                            CAST(MAX(
                                ware_stocktransactions_fabric.dtDate
                            ) as DATE)
                            FROM
                                ware_stocktransactions_fabric
                            WHERE
                                ware_stocktransactions_fabric.intOrderNo = OD.intOrderNo
                            AND ware_stocktransactions_fabric.intOrderYear = OD.intOrderYear
                            AND ware_stocktransactions_fabric.intSalesOrderId = OD.intSalesOrderId
                            AND ware_stocktransactions_fabric.strType LIKE '%Dispatched%'
                            AND ware_stocktransactions_fabric.dtDate <= '$maxDispDate'
                            GROUP BY
                                ware_stocktransactions_fabric.intOrderNo,
                                ware_stocktransactions_fabric.intOrderYear,
                                ware_stocktransactions_fabric.intSalesOrderId
                        ) AS lastDispatchDate,							
							(SELECT
							Min(FDHA.dtApprovedDate)
							FROM
							ware_fabricdispatchheader as FDH 
							INNER JOIN ware_fabricdispatchdetails as FDD ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
							AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear 
							INNER JOIN ware_fabricdispatchheader_approvedby as FDHA 
							ON FDH.intBulkDispatchNo = FDHA.intBulkDispatchNo 
							AND FDH.intBulkDispatchNoYear = FDHA.intYear 
							AND FDH.intApproveLevels = FDHA.intApproveLevelNo 
							AND FDHA.intStatus = 0 
							
							INNER JOIN trn_orderdetails as OD2 ON FDH.intOrderNo = OD2.intOrderNo AND FDH.intOrderYear = OD2.intOrderYear AND FDD.intSalesOrderId = OD2.intSalesOrderId
					
							WHERE FDH.intStatus=1 AND FDH.intOrderNo = OD.intOrderNo AND FDH.intOrderYear = OD.intOrderYear AND OD2.strStyleNo = OD.strStyleNo
							AND FDHA.dtApprovedDate IS NOT NULL
 							AND (FDHA.dtApprovedDate) < '$maxDispDate'
 							ORDER BY
							FDHA.dtApprovedDate ASC) AS dispDate, 
							
(SELECT MB.strName FROM trn_sampleinfomations TSI INNER JOIN mst_brand MB ON MB.intId=TSI.intBrand 
WHERE TSI.intSampleNo = OD.intSampleNo AND TSI.intSampleYear = OD.intSampleYear AND TSI.intRevisionNo = OD.intRevisionNo ) AS brand, 

OH.strCustomerPoNo ,

IFNULL((SELECT FDH.dtmdate FROM ware_fabricdispatchheader as FDH  
INNER JOIN ware_fabricdispatchdetails as FDD ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear 
WHERE FDH.intOrderNo = OH.intOrderNo AND FDH.intOrderYear = OH.intOrderYear 
AND FDD.intSalesOrderId = OD.intSalesOrderId AND FDH.intStatus = 1 
ORDER BY FDH.dtmdate ASC LIMIT 1),
IF((month(OD.dtPSD) < month(now()) && (year(OD.dtPSD) <= year(now()))),now(),OD.dtPSD)) as date 

FROM trn_orderheader  as OH 
INNER JOIN trn_orderdetails as OD ON OH.intOrderNo=OD.intOrderNo AND OH.intOrderYear=OD.intOrderYear 
INNER JOIN mst_part ON OD.intPart = mst_part.intId
INNER JOIN mst_customer MC ON MC.intId=OH.intCustomer 
INNER JOIN sys_users Marketer ON Marketer.intUserId = OH.intMarketer
LEFT JOIN mst_locations ON OH.intLocationId = mst_locations.intId 
LEFT JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId 
LEFT JOIN mst_financeexchangerate ON OH.intCurrency = mst_financeexchangerate.intCurrencyId 
AND DATE(OH.dtDate) = mst_financeexchangerate.dtmDate 
AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId 
LEFT JOIN mst_financeexchangerate as exc ON $toCurrency = exc.intCurrencyId 
AND DATE(OH.dtDate) = exc.dtmDate 
AND mst_locations.intCompanyId = exc.intCompanyId";
				   	 if($type == 2){
				   	     $sql.= " INNER JOIN ware_stocktransactions_fabric ON ware_stocktransactions_fabric.intOrderNo = OH.intOrderNo AND ware_stocktransactions_fabric.intOrderYear = OH.intOrderYear AND ware_stocktransactions_fabric.intSalesOrderId = OD.intSalesOrderId ";
                     }

$sql.= " WHERE  
mst_plant.MAIN_CLUSTER_ID =(SELECT mst_plant.MAIN_CLUSTER_ID FROM mst_locations INNER JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId WHERE mst_locations.intId = '$locationId') AND ";

                    if ($type == 0) {
                        $sql .= " OD.dtDeliveryDate >= '$startDate'  AND  OD.dtDeliveryDate <= '$endDate' AND  ";
                        if($location != ''){
                            $sql.= " OH.intLocationId = '$location' AND ";
                        }
                    }
                    else if ($type == 1) {
                        $sql .= " OH.dtmCreateDate >= '$startDate' AND OH.dtmCreateDate <= '$endDate' AND  ";
                        if($location != ''){
                            $sql.= " OH.intLocationId = '$location' AND ";
                        }
                    } else {
                        $sql .= " ware_stocktransactions_fabric.dtDate  >='$startDate' AND  ware_stocktransactions_fabric.dtDate <= '$endDate' AND ware_stocktransactions_fabric.strType LIKE '%Dispatched%' AND ";
                        if($location != ''){
                            $sql.= " ware_stocktransactions_fabric.intLocationId = '$location' AND ";
                        }
                    }

				   	 if($cboMarketer != '' && $cboMarketer != 'null'){
				   	     $sql.= " OH.intMarketer IN ($cboMarketer) AND ";
				   	     }

$sql.= " (OH.intStatus != '-2') AND 
OD.intViewInSalesProjection <>'0' AND OD.intQty <> '0'
GROUP BY OD.intOrderNo,OD.intOrderYear,OD.intSalesOrderId ";
//				   	 if($type == 0){
//				   	     $sql.=" HAVING dispDate IS NULL ORDER BY OD.dtDeliveryDate ASC ";
//				   	     }
//				   	    else if($type == 1){
//				   	     $sql.=" ORDER BY OH.dtmCreateDate ASC";
//                        }
//                        else{
//				   	     $sql.=" ORDER BY ware_stocktransactions_fabric.dtDate ASC";
//                        }

                     $result = $db->RunQuery($sql);
                     $detailList = array();
                     $recordCount = 0;
					while($row=mysqli_fetch_array($result))//system back orders
					{
                        $dispQtyUntilMonth = $row['DispatchedQtyUntilMonth'] != ''?$row['DispatchedQtyUntilMonth']*(-1):0;
                        $dispQtyThisMonth = $row['DispatchedQtyThisMonth'] != ''?$row['DispatchedQtyThisMonth']*(-1):0;
                        $damageQtyThisPeriod = $row['DispatchedQtyThisMonth'] != ''?$row['DamageQtyForThisPeriod']*(-1):0;
                        $damageQtyUntilMonth = $row['DispatchedQtyUntilMonth'] != ''?$row['DamageQtyUntilMonth']*(-1):0;
					    $detail = array();
                        $details['customer'] = $row['customer'];
                        $details['marketer'] = $row['marketer'];
                        $details['orderNo'] = $row['intOrderNo'];
                        $details['orderYear'] = $row['intOrderYear'];
                        $details['salesOrderNo'] = $row['strSalesOrderNo'];
                        $details['styleNo'] = $row['strStyleNo'];
                        $details['graphicNo'] = $row['strGraphicNo'];
                        $details['sampleNo'] = $row['intSampleNo'];
                        $details['sampleYear'] = $row['intSampleYear'];
                        $details['revNo'] = $row['intRevisionNo'];
                        $details['combo'] = $row['strCombo'];
                        $details['print'] = $row['strPrintName'];
                        $details['part'] = $row['part'];
                        $details['brand'] = $row['brand'];
                        $details['color'] = $row['color'];
                        $details['orderQty'] = $row['orderQty'];
                        $details['dispQtyThisMonth'] = $dispQtyThisMonth;
                        $details['damageQtyThisMonth'] = $damageQtyThisPeriod;
                        $details['dispQtyUntilMonth'] = $dispQtyUntilMonth;
                        $details['damageQtyUntilMonth'] = $damageQtyUntilMonth;
                        $details['price'] = $row['dblPrice'];
                        $details['amount'] = round($row['price']* $dispQtyThisMonth,4);
                        $details['psdDate'] = $row['psdDate'];
                        $details['dtDeliveryDate'] = $row['dtDeliveryDate'];
                        $details['dtmCreateDate'] = $row['dtmCreateDate'];
                        $details['lastDispatchDate'] = $row['lastDispatchDate'];
                        $details['strCustomerPoNo'] = $row['strCustomerPoNo'];
						$SampleNo = $row['intSampleNo'];
						$SampleYear = $row['intSampleYear'];
						$RevisionNo = $row['intRevisionNo'];
						$PrintName = $row['strPrintName'];

						if($row['psdDate']< ($cboYear.'-'.sprintf("%02d",$cboMonth).'-01')){
							$bgCol='#FFDDEE';
							$amountSOP +=$row['amount'];
						}
						else{
							$bgCol='#FFFFFF';
							$amountSOT +=$row['amount'];
						}
						//echo $row['psdDate'] .'<'. ($cboYear.'-'.sprintf("%02d",$cboMonth).'-01').'<br>';
					?>
<!--						<tr bgcolor="--><?php //echo $bgCol; ?><!--" class="normalfnt">-->
<!--                        <td align="center" width="2%" class="clsSaved" title="Delete order">--><?php //echo($deleteSOMode!=1?'&nbsp;':'<img border="0" src="images/del.png" alt="Save" name="butDelSysSaved" class="mouseover clsDelSysSaved" id="butDelSysSaved" tabindex="24"/>'); ?><!--</td>-->
<!--                        <td align="center" width="2%">&nbsp;</td>-->
                        <td align="center" width="8%" title="Customer"><?php echo $row['customer']; ?></td>
                        <td align="center" width="8%" title="Marketer"><?php echo $row['marketer']; ?></td>
                        <td align="center" width="6%" class="orderNo" title="Order No" ><?php echo $row['intOrderNo']; ?></td>
                        <td align="center" width="5%" class="orderYear" title="Order Year" ><?php echo $row['intOrderYear']; ?></td>
                        <td align="center" class="salesOrderId" id="<?php echo $row['intSalesOrderId']; ?>" title="sales Order No"><?php echo $row['strSalesOrderNo']; ?></td>
                        <td align="center" width="5%" class="styleNo" title="Style No" ><?php echo $row['strStyleNo']; ?></td>
                        <td align="center" width="6%" class="graphicNo" title="Graphic No"><?php echo $row['strGraphicNo']; ?></td>
                        <td align="center" width="5%" title="Sample Year" ><?php echo $row['intSampleYear']; ?></td>
                        <td align="center" width="6%" title="Sample No" ><?php echo $row['intSampleNo']; ?></td>
                        <td width="3%" align="center" title="Revision No" ><?php echo $row['intRevisionNo']; ?></td>
                        <td width="5%" align="center" title="Combo" ><?php echo $row['strCombo']; ?></td>
                        <td width="5%" align="center" title="Print Name" >&nbsp;<?php echo $row['strPrintName'].'/',$row['part']; ?></td>
                        <td width="5%" align="center" title="Graphic"><div id="divPicture" class="tableBorder_allRound clsPicture" align="center" contenteditable="true" style="width:70px;height:70px;overflow:hidden" ><?php echo "<img id=\"saveimg\" style=\"width:70px;height:70px;\" src=\"documents/sampleinfo/samplePictures/$SampleNo-$SampleYear-$RevisionNo-".substr($PrintName,6,1).".png\" />"; ?></div></td>
                        <td align="center" title="Brand">&nbsp;<?php echo $row['brand']; ?></td>
                        <td align="center" title="Color">&nbsp;<?php echo $row['color']; ?></td>
                        <td align="right" title="Order Qty">&nbsp;<?php echo $row['orderQty']; ?></td>
                        <td align="right" title="Dispatched Qty for this month">&nbsp;<?php echo $dispQtyThisMonth; ?></td>
                        <td align="right" title="Damaged Qty for this month">&nbsp;<?php echo $damageQtyThisPeriod; ?></td>
                        <td align="right" title="Dispatched Qty till this month-end"><?php echo $dispQtyUntilMonth;?></td>
                        <td align="right" title="Damaged Qty till this month-end"><?php echo $damageQtyUntilMonth;?></td>
                        <td align="right" title="Unit Price">&nbsp;<?php echo $row['dblPrice']; ?></td>
                        <td align="right" title="Amount For This Period(USD)"><?php echo round($row['price']* $dispQtyThisMonth,4); ?></td>
                        <td align="center" title="PSD(day)">&nbsp;<?php echo (/*($row['psdDate']<10)?'0'.$row['psdDate']:*/$row['psdDate']); ?></td>
                        <td align="center" title="Delivery Date">&nbsp;<?php echo $row['dtDeliveryDate']; ?></td>
                        <td align="center" title="Order created Date">&nbsp;<?php echo $row['dtmCreateDate']; ?></td>
                        <td align="center" title="Last Dispatched Date">&nbsp;<?php echo $row['lastDispatchDate']; ?></td>
                        <td align="center" title="PO No">&nbsp;<?php echo $row['strCustomerPoNo']; ?></td>
<!--                        <td>&nbsp;</td>-->
                        </tr>
					<?php
                        $amountForPeriod+= round($row['price']* $dispQtyThisMonth,4);
						$amount+=$row['amount'];
                        $detailList['records'][$recordCount] = $details;
                        $detailList['amountForPeriod'] = $amountForPeriod;
                        $recordCount++;
					}

					$_SESSION['SALES_PROJECTION_DATA'] = $detailList;
										/************************* Hiding plan_sales_project table results *************************************************
					$sql = "SELECT tb1.* FROM 
					(SELECT 	plan_sales_project.* ,
					DATEDIFF('$maxDispDate',(concat(intYear,'-',LPAD(intMonth,2,0),'-',LPAD(dtPSDDay,2,0)))) as dayOrder ,
					(SELECT MB.strName AS brand
							FROM trn_sampleinfomations TSI
							INNER JOIN mst_brand MB ON MB.intId=TSI.intBrand
							WHERE TSI.intSampleNo = plan_sales_p=roject.intSampleNo AND
							TSI.intSampleYear = plan_sales_project.intSampleYear AND
							TSI.intRevisionNo = plan_sales_project.intRevNo
							) AS brand,
							(SELECT DISTINCT mst_colors_ground.strName
							FROM
							trn_sampleinfomations_details 
							INNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
							WHERE
							trn_sampleinfomations_details.intSampleNo =  plan_sales_project.intSampleNo AND
							trn_sampleinfomations_details.intSampleYear =  plan_sales_project.intSampleYear  AND
							trn_sampleinfomations_details.strPrintName =  plan_sales_project.strPrintName AND
							trn_sampleinfomations_details.strComboName =  plan_sales_project.strCombo AND
							trn_sampleinfomations_details.intRevNo =  plan_sales_project.intRevNo
							LIMIT 1
							) AS color,
							concat(intYear,'-',LPAD(intMonth,2,0),'-',LPAD(dtPSDDay,2,0)) as PSDdate 
							FROM (select * from plan_sales_project where plan_sales_project.strCostomerPO='' and intMarketer='$cboMarketer' AND DATEDIFF((concat(intYear,'-',LPAD(intMonth,2,0),'-',LPAD(dtPSDDay,2,0))),'$maxPsdDate')  < 0 ) as plan_sales_project  
							LEFT JOIN mst_locations ON plan_sales_project.LOCATION_ID = mst_locations.intId 
							LEFT JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId 
 							WHERE 
							mst_plant.MAIN_CLUSTER_ID =(SELECT mst_plant.MAIN_CLUSTER_ID FROM mst_locations INNER JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId WHERE mst_locations.intId = '$locationId') AND 
							/*intYear <= '$cboYear' AND
							intMonth <= '$cboMonth' AND *//*
							intMarketer='$cboMarketer' AND 
							plan_sales_project.strCostomerPO='' AND
							DATEDIFF((concat(intYear,'-',LPAD(intMonth,2,0),'-',LPAD(dtPSDDay,2,0))),'$maxPsdDate')  < 0) as tb1 
							order by tb1.dayOrder desc
							";
							//if($cboMonth==9){
							//	echo $sql;
 							//}
							
							
 					$result = $db->RunQuery($sql);
					$k	= 0;
					while($row=mysqli_fetch_array($result))//none system orders
					{
						$k++;
						$SampleNo = $row['intSampleNo'];
						$SampleYear = $row['intSampleYear'];
						$RevisionNo = $row['intRevNo'];
						$PrintName = $row['strPrintName'];
						
						if($row['PSDdate']< ($maxDispDate)){//none system push back orders
							//$bgCol='#FFDDEE';
							$amountMOP +=$row['dblAmount'];
						}
						else{
							//$bgCol='#FFFFFF';
							$amountMOT +=$row['dblAmount'];
						}
					?>
                    <tr bgcolor="<?php echo $bgCol; ?>" class="normalfnt">
                       <td align="center" width="2%" id="<?php echo $row['intId']; ?>" class="clsSaved" title="Delete Order"><?php echo($deleteMode==0?'&nbsp;':'<img border="0" src="images/del.png" alt="Save" name="butDelSaved" class="mouseover clsDelSaved" id="butDelSaved" tabindex="24"/>'); ?></td>
                       <td align="center" width="2%" class="clsEdited" title="Edit Order" ><?php echo($editMode==0?'&nbsp;':'<img border="0" src="images/edit.png" alt="Save" name="butEdit" class="mouseover clsEdit" id="butEdit" tabindex="24" />'); ?></td>
                       <td align="center" width="8%" title="Customer"><select name="cboCustomer" id="cboCustomer" class="clsCustomer" style="width:130px" disabled="disabled">
                       <option value=""></option>
                       <?php
					   		$sql1 = "SELECT intId,strName FROM mst_customer WHERE intStatus=1";
							$result1 = $db->RunQuery($sql1);
							while($row1=mysqli_fetch_array($result1))
							{
								if($row1['intId']==$row['intCustomer'])
									echo "<option value=\"".$row1["intId"]."\" selected=\"selected\" >".$row1["strName"]."</option>";
								else
									echo "<option value=\"".$row1["intId"]."\" >".$row1["strName"]."</option>";
							}
					   ?>
                       </select></td>
                       <td align="center" width="6%" title="Order No" ><input type="text" name="txtOrderNo" id="txtOrderNo" class="clsOrderNo" style="width:100px" value="<?php echo $row['intOrderNo']; ?>" disabled="disabled" />
                       </td>
                       <td align="center" width="5%" title="Order Year" ><select name="cboOrderYear" id="cboOrderYear" class="clsOrderYear" style="width:80px" disabled="disabled">
                         <option value=""></option>
                          <?php
					   		$sql2 = "SELECT DISTINCT intOrderYear FROM trn_orderheader";
							$result2 = $db->RunQuery($sql2);
							while($row2=mysqli_fetch_array($result2))
							{
								if($row2['intOrderYear']==$row['intOrderYear'])
									echo "<option value=\"".$row2["intOrderYear"]."\" selected=\"selected\" >".$row2["intOrderYear"]."</option>";
								else
									echo "<option value=\"".$row2["intOrderYear"]."\" >".$row2["intOrderYear"]."</option>";
							}
					  		?>
                         </select>
                       </td>
                        <td align="center" class="salesOrderId" id="" title="sales Order No"><input type="text" name="txtSalesOrder" id="txtSalesOrder" class="clsSalesOrder" style="width:80px" value="<?php echo $row['strSalesOrderNo']; ?>" disabled="disabled" /></td>
                       <td align="center" width="5%" title="Style No" ><input type="text" name="txtStyleNo" id="txtStyleNo" class="clsStyleNo" style="width:80px" value="<?php echo $row['strStyleNo']; ?>" disabled="disabled" /></td>
                       <td align="right" width="6%" title="Graphic No" ><select name="cboGraphicNo" id="cboGraphicNo" class="clsGraphicNo" style="width:100px" disabled="disabled">
                         <option value=""></option>
                         <?php
						 	$sql3 = "SELECT DISTINCT
									trn_sampleinfomations.strGraphicRefNo
									FROM trn_sampleinfomations
									ORDER BY strGraphicRefNo";
							$result3 = $db->RunQuery($sql3);
							while($row3=mysqli_fetch_array($result3))
							{
								if($row3['strGraphicRefNo']==$row['strGraphicNo'])
									echo "<option value=\"".$row3["strGraphicRefNo"]."\" selected=\"selected\" >".$row3["strGraphicRefNo"]."</option>";
								else
									echo "<option value=\"".$row3["strGraphicRefNo"]."\" >".$row3["strGraphicRefNo"]."</option>";
							}
						 ?>
                         </select>
                       </td>
                       <td align="right" width="5%" title="Sample Year" ><select name="cboSampleYear" id="cboSampleYear" class="clsSampleYear" style="width:80px" disabled="disabled">
                       <option value=""></option>
                       <?php
						 	$sql4 = " SELECT DISTINCT SD.intSampleYear
										FROM trn_sampleinfomations_details SD
										INNER JOIN trn_sampleinfomations S ON S.intSampleNo=SD.intSampleNo
										WHERE S.strGraphicRefNo='".$row['strGraphicNo']."' ";
							$result4 = $db->RunQuery($sql4);
							while($row4=mysqli_fetch_array($result4))
							{
								if($row4['intSampleYear']==$row['intSampleYear'])
									echo "<option value=\"".$row4["intSampleYear"]."\" selected=\"selected\" >".$row4["intSampleYear"]."</option>";
								else
									echo "<option value=\"".$row4["intSampleYear"]."\" >".$row4["intSampleYear"]."</option>";
							}
						 ?>
                         </select>
                       </td>
                        <td align="center" width="6%" title="Sample No" ><select name="cboSampleNo" id="cboSampleNo" class="clsSampleNo" style="width:100px" disabled="disabled">
                        <option value=""></option>
                       <?php
						 	$sql5 = " SELECT DISTINCT SD.intSampleNo
										FROM trn_sampleinfomations_details SD
										INNER JOIN trn_sampleinfomations S ON S.intSampleNo=SD.intSampleNo
										WHERE S.strGraphicRefNo='".$row['strGraphicNo']."' AND
										SD.intSampleYear='".$row['intSampleYear']."' ";
							$result5 = $db->RunQuery($sql5);
							while($row5=mysqli_fetch_array($result5))
							{
								if($row5['intSampleNo']==$row['intSampleNo'])
									echo "<option value=\"".$row5["intSampleNo"]."\" selected=\"selected\" >".$row5["intSampleNo"]."</option>";
								else
									echo "<option value=\"".$row5["intSampleNo"]."\" >".$row5["intSampleNo"]."</option>";
							}
						 ?>
                          </select>
                        </td>
                       <td width="3%" align="center" title="Revision No" ><select name="cboRevNo" id="cboRevNo" class="clsRevNo" style="width:50px" disabled="disabled">
                        <option value=""></option>
                       <?php
						 	$sql6 = " SELECT DISTINCT SD.intRevNo
										FROM trn_sampleinfomations_details SD
										INNER JOIN trn_sampleinfomations S ON S.intSampleNo=SD.intSampleNo
										WHERE S.strGraphicRefNo='".$row['strGraphicNo']."' AND
										SD.intSampleYear='".$row['intSampleYear']."' AND
										SD.intSampleNo='".$row['intSampleNo']."' ";
							$result6 = $db->RunQuery($sql6);
							while($row6=mysqli_fetch_array($result6))
							{
								if($row6['intRevNo']==$row['intRevNo'])
									echo "<option value=\"".$row6["intRevNo"]."\" selected=\"selected\" >".$row6["intRevNo"]."</option>";
								else
									echo "<option value=\"".$row6["intRevNo"]."\" >".$row6["intRevNo"]."</option>";
							}
						 ?>
                         </select>
                     	</td>
                       <td width="5%" align="center" title="Combo" ><select name="cboCombo" id="cboCombo" class="clsCombo" style="width:80px" disabled="disabled">
                        <option value=""></option>
                       <?php
						 	$sql7 = " SELECT DISTINCT SD.strComboName
										FROM trn_sampleinfomations_details SD
										INNER JOIN trn_sampleinfomations S ON S.intSampleNo=SD.intSampleNo
										WHERE S.strGraphicRefNo='".$row['strGraphicNo']."' AND
										SD.intSampleYear='".$row['intSampleYear']."' AND
										SD.intSampleNo='".$row['intSampleNo']."' AND
										SD.intRevNo='".$row['intRevNo']."' ";
							$result7 = $db->RunQuery($sql7);
							while($row7=mysqli_fetch_array($result7))
							{
								if($row7['strComboName']==$row['strCombo'])
									echo "<option value=\"".$row7["strComboName"]."\" selected=\"selected\" >".$row7["strComboName"]."</option>";
								else
									echo "<option value=\"".$row7["strComboName"]."\" >".$row7["strComboName"]."</option>";
							}
						 ?>
                         </select>
                       </td>
                       <td width="5%" align="center" title="Print" ><select name="cboPrint" id="cboPrint" class="clsPrint" style="width:80px" disabled="disabled">
                       <option value=""></option>
                       <?php
						 	$sql8 = " SELECT DISTINCT SD.strPrintName
										FROM trn_sampleinfomations_details SD
										INNER JOIN trn_sampleinfomations S ON S.intSampleNo=SD.intSampleNo
										WHERE S.strGraphicRefNo='".$row['strGraphicNo']."' AND
										SD.intSampleYear='".$row['intSampleYear']."' AND
										SD.intSampleNo='".$row['intSampleNo']."' AND
										SD.intRevNo='".$row['intRevNo']."' AND
										SD.strComboName='".$row['strCombo']."' ";
							$result8 = $db->RunQuery($sql8);
							while($row8=mysqli_fetch_array($result8))
							{
								if($row8['strPrintName']==$row['strPrintName'])
									echo "<option value=\"".$row8["strPrintName"]."\" selected=\"selected\" >".$row8["strPrintName"]."</option>";
								else
									echo "<option value=\"".$row8["strPrintName"]."\" >".$row8["strPrintName"]."</option>";
							}
						 ?>
                         
                         </select>
                       </td>
                       <td width="5%" align="center" title="Graphic" ><div id="divPicture" class="tableBorder_allRound clsPicture" align="center" contenteditable="true" style="width:70px;height:70px;overflow:hidden" ><?php echo "<img id=\"saveimg\" style=\"width:70px;height:70px;\" src=\"documents/sampleinfo/samplePictures/$SampleNo-$SampleYear-$RevisionNo-".substr($PrintName,6,1).".png\" />"; ?></div></td>
                       <td align="center" class="clsBrand" title="Brand"><?php echo $row['brand']; ?></td>
                       <td align="center" class="clsColors" title="Color"><?php echo $row['color']; ?></td>
                       <td align="center" title="Description"><textarea name="txtDescription" id="txtDescription" class="clsDescription" cols="20" rows="2" disabled="disabled" ><?php echo $row['strDescription']; ?></textarea>
                       </td>
                       <td align="center" title="Order Qty"><input type="text" name="txtOrderQty" id="txtOrderQty" class="clsOrderQty" style="width:80px; text-align:right" value="<?php echo $row['intOrderQty'];  ?>" disabled="disabled"  />
                       </td>
                       <td align="center" title="Unit Price"><input type="text" name="txtUnitPrice" id="txtUnitPrice" class="clsUnitPrice" style="width:80px; text-align:right" value="<?php echo $row['dblUnitPrice']; ?>" disabled="disabled" />
                       </td>
                       <td align="center" title="Amount(USD)"><input type="text" name="txtPrice" id="txtPrice" class="clsPrice" style="width:80px; text-align:right" value="<?php echo round($row['dblAmount'],2); ?>" disabled="disabled"/>
                       </td>
                        <td align="center" title="PSD(day)"><input type="text" disabled="disabled" name="txtPSD" id="txtPSD<?php echo $k; ?>" class="clsPSD"   style="width:80px" value="<?php if($row['PSDdate']){ echo $row['PSDdate']; }else { echo date("Y-m-d"); } ?>"  onmousedown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="<?php /*?>return ControlableKeyAccess(event);<?php ?>"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" />
                        </td>
                         <td align="center" title="PO No"><input type="text" name="txtPONo" id="txtPONo" class="clsPONo" style="width:80px" value="<?php echo $row['strCostomerPO']; ?>"  />
                        </td>
                       <td align="center" class="clsbtnSave"><img border="0" src="images/Tsave.jpg" alt="Save" name="butSave" height="20" class="mouseover clsSave" id="butSave" tabindex="24"/></td>
                     </tr>
                    <?php
					$amount+=$row['dblAmount'];
					}
					$k++;
					?>
              		<script>
					var savedRowCount = '<?php echo $rowCount; ?>';
					</script>
  ************************************************************************************ ending commenting *****************************************/
                    ?>
<!--                     <tr bgcolor="#FFFFFF" class="normalfnt">-->
<!--                       <td align="center" width="2%" id="0"  class="clsSaved" title="Delete Order"><img border="0" src="images/del.png" alt="Save" name="butDel" class="mouseover clsDel" id="butDel" tabindex="24"/></td>-->
<!--                       <td align="center" width="2%" class="clsEdited">&nbsp;</td>-->
<!--                       <td align="center" width="8%" title="Customer"><select name="cboCustomer" id="cboCustomer" class="clsCustomer" style="width:130px">-->
<!--                       <option value=""></option>-->
<!--                       --><?php
//					   		$sql = "SELECT intId,strName FROM mst_customer WHERE intStatus=1";
//							$result = $db->RunQuery($sql);
//							while($row=mysqli_fetch_array($result))
//							{
//								echo "<option value=\"".$row["intId"]."\" >".$row["strName"]."</option>";
//							}
//					   ?>
<!--                       </select></td>-->
<!--                       <td align="center" width="6%" title="Order No" ><input type="text" name="txtOrderNo" id="txtOrderNo" class="clsOrderNo" style="width:100px" />-->
<!--                       </td>-->
<!--                       <td align="center" width="5%" title="Order Year" ><select name="cboOrderYear" id="cboOrderYear" class="clsOrderYear" style="width:80px">-->
<!--                         <option value=""></option>-->
<!--                          --><?php
//					   		$sql = "SELECT DISTINCT intOrderYear FROM trn_orderheader";
//							$result = $db->RunQuery($sql);
//							while($row=mysqli_fetch_array($result))
//							{
//								echo "<option value=\"".$row["intOrderYear"]."\" >".$row["intOrderYear"]."</option>";
//							}
//					  		?>
<!--                         </select>-->
<!--                       </td>-->
<!--                        <td align="center" class="salesOrderId" id=""  title="sales Order No"><input type="text" name="txtSalesOrder" id="txtSalesOrder" class="clsSalesOrder" style="width:80px" value="" /></td>-->
<!--                       <td align="center" width="5%" title="Style No" ><input type="text" name="txtStyleNo" id="txtStyleNo" class="clsStyleNo" style="width:80px" /></td>-->
<!--                       <td align="right" width="6%" title="Graphic No" ><select name="cboGraphicNo" id="cboGraphicNo" class="clsGraphicNo" style="width:100px">-->
<!--                         <option value=""></option>-->
<!--                         --><?php
//						 	$sql = "SELECT DISTINCT
//									trn_sampleinfomations.strGraphicRefNo
//									FROM trn_sampleinfomations
//									ORDER BY strGraphicRefNo";
//							$result = $db->RunQuery($sql);
//							while($row=mysqli_fetch_array($result))
//							{
//								echo "<option value=\"".$row["strGraphicRefNo"]."\" >".$row["strGraphicRefNo"]."</option>";
//							}
//						 ?>
<!--                         </select>-->
<!--                       </td>-->
<!--                       <td align="right" width="5%" title="Sample Year" ><select name="cboSampleYear" id="cboSampleYear" class="clsSampleYear" style="width:80px">-->
<!--                         </select>-->
<!--                       </td>-->
<!--                        <td align="center" width="6%" title="Sample No" ><select name="cboSampleNo" id="cboSampleNo" class="clsSampleNo" style="width:100px">-->
<!--                          </select>-->
<!--                        </td>-->
<!--                       <td width="3%" align="center" title="Revision No" ><select name="cboRevNo" id="cboRevNo" class="clsRevNo" style="width:50px">-->
<!--                         </select>-->
<!--                     	</td>-->
<!--                       <td width="5%" align="center" title="Combo" ><select name="cboCombo" id="cboCombo" class="clsCombo" style="width:80px">-->
<!--                         </select>-->
<!--                       </td>-->
<!--                       <td width="5%" align="center" title="Print" ><select name="cboPrint" id="cboPrint" class="clsPrint" style="width:80px">-->
<!--                         </select>-->
<!--                       </td>-->
<!--                       <td width="5%" align="center" title="Graphic" ><div id="divPicture" class="tableBorder_allRound clsPicture" align="center" contenteditable="true" style="width:70px;height:70px;overflow:hidden" ></div></td>-->
<!--                       <td align="center" class="clsBrand">&nbsp;</td>-->
<!--                       <td align="center" class="clsColors">&nbsp;</td>-->
<!--                       <td align="center" title="Description"><textarea name="txtDescription" id="txtDescription" class="clsDescription" cols="20" rows="2"></textarea>-->
<!--                       </td>-->
<!--                       <td align="center" title="Order Qty"><input type="text" name="txtOrderQty" id="txtOrderQty" class="clsOrderQty" style="width:80px; text-align:right" value="0" />-->
<!--                       </td>-->
<!--                         <td align="center" title="Dispatched Qty for this month"><input type="text" name="txtDispQtyMonth" id="txtDispQtyMonth" class="clsOrderQty" style="width:80px; text-align:right" value="0" />-->
<!--                         </td>-->
<!--                         <td align="center" title="Dispatched Qty until this month"><input type="text" name="txtDispQtyMonth" id="txtDispQtyMonth" class="clsOrderQty" style="width:80px; text-align:right" value="0" />-->
<!--                         </td>-->
<!--                       <td align="center" title="Unit Price"><input type="text" name="txtUnitPrice" id="txtUnitPrice" class="clsUnitPrice" style="width:80px; text-align:right" value="0" />-->
<!--                       </td>-->
<!--                       <td align="center" title="Amount"><input type="text" name="txtPrice" id="txtPrice" class="clsPrice" style="width:80px; text-align:right" value="0" disabled="disabled" />-->
<!--                       </td>-->
<!--                        <td align="center" title="PSD(day)"><input type="text" name="txtPSD" id="txtPSD--><?php //echo $k; ?><!--" class="clsPSD" style="width:80px" value="--><?php ///* if($row['intMonth'] < $cboMonth){echo 1;} else {   echo $row['dtPSDDay'];}*/ /*echo $row['PSDdate'] */ /* if($row['PSDdate']){ echo $row['PSDdate']; }else {*/ echo date("Y-m-d");/* } */?><!--"  onmousedown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="--><?php /*?>return ControlableKeyAccess(event);<?php */?><!--"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" />-->
<!--                        </td>-->
<!--                         <td align="center" title="delDay"><input type="text" name="txtDelivery" id="txtDelivery--><?php //echo $k; ?><!--" class="clsPSD"  style="width:80px" value="--><?php // echo date("Y-m-d");/* } */?><!--"  onmousedown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="--><?php /*?>return ControlableKeyAccess(event);<?php */?><!--"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" />-->
<!--                         </td>-->
<!--                         <td align="center" title="orderCreatedDate"><input type="text" name="txtOHCreated" id="txtOHCreated--><?php //echo $k; ?><!--" class="clsPSD"  style="width:80px" value="--><?php // echo date("Y-m-d");/* } */?><!--"  onmousedown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="--><?php /*?>return ControlableKeyAccess(event);<?php */?><!--"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" />-->
<!--                         </td>-->
<!--                         <td align="center" title="PO No"><input type="text" name="txtPONo" id="txtPONo" class="clsPONo" style="width:80px" />-->
<!--                        </td>-->
<!--                       <td align="center" class="clsbtnSave" ><img border="0" src="images/Tsave.jpg" alt="Save" name="butSave" height="20" class="mouseover clsSave" id="butSave" tabindex="24"/></td>-->
<!--                     </tr>-->
<!--                   	 <tr class="dataRow">-->
<!--                         <td colspan="16" align="left"  bgcolor="#FFFFFF" ><a class="button white medium" id="butInsertRow">Add</a></td>-->
<!--                         <td colspan="2" align="left"  bgcolor="#FFFFFF" ></td>-->
<!--                         <td align="right"  bgcolor="#FFFFFF" ></td>-->
<!--                         <td  align="left"  bgcolor="#FFFFFF" >&nbsp;</td>-->
<!--                         <td  align="left"  bgcolor="#FFFFFF" >&nbsp;</td>-->
<!--                         <td  align="left"  bgcolor="#FFFFFF" >&nbsp;</td>-->
<!--                         <td  align="left"  bgcolor="#FFFFFF" >&nbsp;</td>-->
<!--                         <td  align="left"  bgcolor="#FFFFFF" >&nbsp;</td>-->
<!--                         <td  align="left"  bgcolor="#FFFFFF" >&nbsp;</td>-->
<!--                         <td  align="left"  bgcolor="#FFFFFF" >&nbsp;</td>-->
<!--                         <td  align="left"  bgcolor="#FFFFFF" >&nbsp;</td>-->
<!---->
<!--                     </tr>-->

<!--                     <tr bgcolor="#FFFFFF" class="normalfnt">-->
<!--                       <td align="center" width="2%" id="0"  class="clsSaved" title="Delete Order"><img border="0" src="images/del.png" alt="Save" name="butDel" class="mouseover clsDel" id="butDel" tabindex="24"/></td>-->
<!--                       <td align="center" width="2%" class="clsEdited">&nbsp;</td>-->
<!--                       <td align="center" width="8%" title="Customer"><select name="cboCustomer" id="cboCustomer" class="clsCustomer" style="width:130px">-->
<!--                       <option value=""></option>-->
<!--                       --><?php
//					   		$sql = "SELECT intId,strName FROM mst_customer WHERE intStatus=1";
//							$result = $db->RunQuery($sql);
//							while($row=mysqli_fetch_array($result))
//							{
//								echo "<option value=\"".$row["intId"]."\" >".$row["strName"]."</option>";
//							}
//					   ?>
<!--                       </select></td>-->
<!--                       <td align="center" width="6%" title="Order No" ><input type="text" name="txtOrderNo" id="txtOrderNo" class="clsOrderNo" style="width:100px" />-->
<!--                       </td>-->
<!--                       <td align="center" width="5%" title="Order Year" ><select name="cboOrderYear" id="cboOrderYear" class="clsOrderYear" style="width:80px">-->
<!--                         <option value=""></option>-->
<!--                          --><?php
//					   		$sql = "SELECT DISTINCT intOrderYear FROM trn_orderheader";
//							$result = $db->RunQuery($sql);
//							while($row=mysqli_fetch_array($result))
//							{
//								echo "<option value=\"".$row["intOrderYear"]."\" >".$row["intOrderYear"]."</option>";
//							}
//					  		?>
<!--                         </select>-->
<!--                       </td>-->
<!--                        <td align="center" class="salesOrderId" id=""  title="sales Order No"><input type="text" name="txtSalesOrder" id="txtSalesOrder" class="clsSalesOrder" style="width:80px" value="" /></td>-->
<!--                       <td align="center" width="5%" title="Style No" ><input type="text" name="txtStyleNo" id="txtStyleNo" class="clsStyleNo" style="width:80px" /></td>-->
<!--                       <td align="right" width="6%" title="Graphic No" ><select name="cboGraphicNo" id="cboGraphicNo" class="clsGraphicNo" style="width:100px">-->
<!--                         <option value=""></option>-->
<!--                         --><?php
//						 	$sql = "SELECT DISTINCT
//									trn_sampleinfomations.strGraphicRefNo
//									FROM trn_sampleinfomations
//									ORDER BY strGraphicRefNo";
//							$result = $db->RunQuery($sql);
//							while($row=mysqli_fetch_array($result))
//							{
//								echo "<option value=\"".$row["strGraphicRefNo"]."\" >".$row["strGraphicRefNo"]."</option>";
//							}
//						 ?>
<!--                         </select>-->
<!--                       </td>-->
<!--                       <td align="right" width="5%" title="Sample Year" ><select name="cboSampleYear" id="cboSampleYear" class="clsSampleYear" style="width:80px">-->
<!--                         </select>-->
<!--                       </td>-->
<!--                        <td align="center" width="6%" title="Sample No" ><select name="cboSampleNo" id="cboSampleNo" class="clsSampleNo" style="width:100px">-->
<!--                          </select>-->
<!--                        </td>-->
<!--                       <td width="3%" align="center" title="Revision No" ><select name="cboRevNo" id="cboRevNo" class="clsRevNo" style="width:50px">-->
<!--                         </select>-->
<!--                     	</td>-->
<!--                       <td width="5%" align="center" title="Combo" ><select name="cboCombo" id="cboCombo" class="clsCombo" style="width:80px">-->
<!--                         </select>-->
<!--                       </td>-->
<!--                       <td width="5%" align="center" title="Print" ><select name="cboPrint" id="cboPrint" class="clsPrint" style="width:80px">-->
<!--                         </select>-->
<!--                       </td>-->
<!--                       <td width="5%" align="center" title="Graphic" ><div id="divPicture" class="tableBorder_allRound clsPicture" align="center" contenteditable="true" style="width:70px;height:70px;overflow:hidden" ></div></td>-->
<!--                       <td align="center" class="clsBrand">&nbsp;</td>-->
<!--                       <td align="center" class="clsColors">&nbsp;</td>-->
<!--                       <td align="center" title="Description"><textarea name="txtDescription" id="txtDescription" class="clsDescription" cols="20" rows="2"></textarea>-->
<!--                       </td>-->
<!--                       <td align="center" title="Order Qty"><input type="text" name="txtOrderQty" id="txtOrderQty" class="clsOrderQty" style="width:80px; text-align:right" value="0" />-->
<!--                       </td>-->
<!--                       <td align="center" title="Unit Price"><input type="text" name="txtUnitPrice" id="txtUnitPrice" class="clsUnitPrice" style="width:80px; text-align:right" value="0" />-->
<!--                       </td>-->
<!--                       <td align="center" title="Amount"><input type="text" name="txtPrice" id="txtPrice" class="clsPrice" style="width:80px; text-align:right" value="0" disabled="disabled" />-->
<!--                       </td>-->
<!--                        <td align="center" title="PSD(day)"><input type="text" name="txtPSD" id="txtPSD--><?php //echo $k; ?><!--" class="clsPSD" --><?php /*/*?>maxlength="2"<?php */?><!-- style="width:80px" value="--><?php ///* if($row['intMonth'] < $cboMonth){echo 1;} else {   echo $row['dtPSDDay'];}*/ /*echo $row['PSDdate'] */ /* if($row['PSDdate']){ echo $row['PSDdate']; }else {*/ echo date("Y-m-d");/* } */?><!--"  onmousedown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="--><?php /*/*?>return ControlableKeyAccess(event);<?php */?><!--"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" />-->
<!--                        </td>-->
<!--                         <td align="center" title="PO No"><input type="text" name="txtPONo" id="txtPONo" class="clsPONo" style="width:80px" />-->
<!--                        </td>-->
<!--                       <td align="center" class="clsbtnSave" ><img border="0" src="images/Tsave.jpg" alt="Save" name="butSave" height="20" class="mouseover clsSave" id="butSave" tabindex="24"/></td>-->
<!--                     </tr>-->
<!--                   	 <tr class="dataRow">-->
<!--                         <td colspan="16" align="left"  bgcolor="#FFFFFF" ><a class="button white medium" id="butInsertRow">Add</a></td>-->
<!--                         <td colspan="2" align="left"  bgcolor="#FFFFFF" ></td>-->
<!--                         <td align="right"  bgcolor="#FFFFFF" ></td>-->
<!--                         <td  align="left"  bgcolor="#FFFFFF" >&nbsp;</td>-->
<!--                         <td  align="left"  bgcolor="#FFFFFF" >&nbsp;</td>-->
<!--                         <td  align="left"  bgcolor="#FFFFFF" >&nbsp;</td>-->
<!--                         <td  align="left"  bgcolor="#FFFFFF" >&nbsp;</td>-->
<!--                         <td  align="left"  bgcolor="#FFFFFF" >&nbsp;</td>-->
<!--                     </tr>-->

                     
                     
                    </table>
                   </div>
                </td>
           	  </tr>
              <tr>
              <td colspan="2">
                              <table width="1950" class="bordered" id="tblMain" border="0">
                              
<!--                     <tr>-->
<!--                       <td width="60%" align="left"  bgcolor="#FFFFFF" style="font-size:14px" >&nbsp;</td>-->
<!--                         <td width="27%" align="left"  bgcolor="#FFFFFF"  class="normalfnt" >System Orders(Push Back) Projection </td>-->
<!--                         <td width="11%" align="right"  bgcolor="#FFFFFF" style="font-size:14px" ><b>--><?php //echo $currSymbol.' '.number_format(round($amountSOP),0); ?><!--</b></td>-->
<!--                         </tr>-->
<!--                     <tr>-->
<!--                       <td width="60%" align="left"  bgcolor="#FFFFFF" style="font-size:14px" >&nbsp;</td>-->
<!--                         <td width="27%" align="left"  bgcolor="#FFFFFF" class="normalfnt" >System Orders(This Month) Projection </td>-->
<!--                         <td align="right"  bgcolor="#FFFFFF" style="font-size:14px" ><b>--><?php //echo $currSymbol.' '.number_format(round($amountSOT),0); ?><!--</b></td>-->
<!--                         </tr>-->
<!--                     <tr>-->
<!--                       <td width="60%" align="left"  bgcolor="#FFFFFF" style="font-size:14px" >&nbsp;</td>-->
<!--                         <td width="27%" align="left"  bgcolor="#FFFFFF" class="normalfnt" >Manual Orders(Push Back) Projection </td>-->
<!--                         <td align="right"  bgcolor="#FFFFFF" style="font-size:14px" ><b>--><?php //echo $currSymbol.' '.number_format(round($amountMOP),0); ?><!--</b></td>-->
<!--                         </tr>-->
<!--                     <tr>-->
<!--                       <td width="60%" align="left"  bgcolor="#FFFFFF" style="font-size:14px" >&nbsp;</td>-->
<!--                         <td width="27%" align="left"  bgcolor="#FFFFFF" >Manual Orders(This Month) Projection </td>-->
<!--                         <td align="right"  bgcolor="#FFFFFF" style="font-size:14px" ><b>--><?php //echo $currSymbol.' '.number_format(round($amountMOT),0); ?><!--</b></td>-->
<!--                         </tr>-->
                     <tr>
                       <td width="60%" align="left"  bgcolor="#FFFFFF" style="font-size:14px" >&nbsp;</td>
                         <td width="27%" align="left"  bgcolor="#FFFFFF">Total Dispatched </td>
                         <td align="right"  bgcolor="#FFFFFF" style="font-size:14px" ><b><?php echo $currSymbol.' '.number_format(round($amountForPeriod),2); ?></b></td>
                         </tr>
					</table>
              </td>
              </tr>
             </table>
        </td>
    </tr>
    <tr>
    	<td height="30" >
    		<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td align="center" class="tableBorder_allRound"><a href="main.php" class="button white medium" id="butClose">Close</a></td>
              </tr>
            </table>
        </td>
    </tr>
</table>
</div>
</div>
</form>
<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
<?php
function loadEditMode($programCode,$userId)
{
	global $db;
	$editMode = 0;
	
	$sql = "SELECT
			menupermision.intEdit  
			FROM menupermision 
			INNER JOIN menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode =  '$programCode' AND
			menupermision.intUserId =  '$userId' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	
	if($row['intEdit']==1)
	{
	 	$editMode = 1;
	}
	return $editMode;
}
function loadDeleteMode($programCode,$userId)
{
	global $db;
	$deleteMode = 0;
	
	$sql = "SELECT
			menupermision.intDelete  
			FROM menupermision 
			INNER JOIN menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode =  '$programCode' AND
			menupermision.intUserId =  '$userId' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	
	if($row['intDelete']==1)
	{
	 	$deleteMode = 1;
	}
	return $deleteMode;
}

function loadSystemOrderDeleteMode($programCode,$userId)
{
	global $db;
	$deleteMode = 0;
	
	$sql = "SELECT
			menupermision.intDelete  
			FROM menupermision 
			INNER JOIN menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode =  '$programCode' AND
			menupermision.intUserId =  '$userId' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	
	if($row['intDelete']==1)
	{
	 	$deleteMode = 1;
	}
	return $deleteMode;
}

function boolSPermision1($menuId)
{
	global $db;
	global $userId;
	$sql = "SELECT
				menus_special_permision.intUser
			FROM menus_special_permision
			WHERE
				menus_special_permision.intSpMenuId 	=  '$menuId' AND
				menus_special_permision.intUser 		=  '$userId'
			";
	$result = $db->RunQuery($sql);
	if( mysqli_num_rows($result)>0)
		return 1;
	else
		return 0;		
}


?>

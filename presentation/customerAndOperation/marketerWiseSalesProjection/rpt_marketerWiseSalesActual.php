<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
ini_set('max_execution_time', 11111111) ;
session_start();


include_once "class/cls_commonFunctions_get.php";
include_once "class/customerAndOperation/marketerWiseSalesProjection/cls_marketerWiseSalesProjection_get.php";

$obj_common					= new cls_commonFunctions_get($db);
$obj_salesProjection_get	= new cls_marketerWiseSalesProjection_get($db);

$locationId 				= $_SESSION['CompanyID'];
$intUser  					= $_SESSION["userId"];
$mainPath 					= $_SESSION['mainPath'];
$thisFilePath 				= $_SERVER['PHP_SELF'];

$marketer 					= $_REQUEST['marketer'];
$year 						= $_REQUEST['year'];
$month 						= $_REQUEST['month'];
$toCureency					=	'1';


$currencyName				= $obj_common->getCurrencyName($toCureency); 
$monthName					= $obj_common->getMonthName($month);
$marketerName				= $obj_common->getUserName($marketer);
 		
//---------------------------------------------------------------
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sales Actuals</title>
 <style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position: absolute;
	left: 329px;
	top: 199px;
	width: 554px;
	height: 222px;
	z-index: 1;
}
#apDiv2 {
	position: absolute;
	left: 449px;
	top: 179px;
	width: 357px;
	height: 183px;
	z-index: 1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>

<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>
</table>

<div style="background-color:#FFF" ></div><table width="1500" border="0" align="center" bgcolor="#FFFFFF">
<tr><td><table width="1500">
<tr><td colspan="8" align="center"><div align="center"><b>Marketer  Sales  Actual </b></div></td></tr>
<tr><td colspan="8" align="center" height="15"></td></tr>

<tr><td width="10">&nbsp;</td><td colspan="7" class="normalfnt">
    <table width="100%"   border="0" align="center" bgcolor="#FFFFFF" class="normalfnt">
    <tr>
    <td width="6%">Marketer : </td>
    <td width="23%"><?php echo $marketerName?></td>
    <td width="3%">Year : </td>
    <td width="6%"><?php echo $year?></td>
    <td width="4%">Month : </td>
    <td width="7%"><?php echo $monthName?></td>
    <td width="5%">Currency : </td>
    <td width="46%"><?php echo $currencyName?></td>
     </tr>
    <tr><td colspan="8" align="center" height="15"></td></tr>
    </table>
   <table width="100%"  class="rptBordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
  <tr>
    <th width="15%">Customer</th>
    <th width="5%">Order No</th>
    <th width="6%">order Year</th>
    <th width="6%">Sales Order</th>
    <th width="8%">Style No</th>
    <th width="11%">Graphic No</th>
    <th width="4%">Sample Year</th>
    <th width="4%">Sample No</th>
    <th width="3%">Rev No</th>
    <th width="4%">Combo</th>
    <th width="5%">Print/Part</th>
    <th width="6%">Graphic</th>
     <th width="6%">Ground Color</th>
     <th width="4%">Order Qty</th>
    <th width="3%">Unit Price</th>
    <th width="7%">Amount(USD)</th>
    <th width="6%">PSD(day)</th>
    <th width="3%">PO No</th>
  </tr>
  <?php
	$grand_totQty		=0;
	$grand_totAmount	=0;
	$noOfclusters		=0;
  
	$resultC	= $obj_common->get_marketerClusters($marketer);
	while($rowC=mysqli_fetch_array($resultC)){
		$noOfclusters++;
		$totQty			=0;
		$totAmount		=0;
		$cluster		=$rowC['CLUSTER_TYPE'];
		$clusterType	=$rowC['NO'];
   ?>
  <tr bgcolor="#E4E4E4">
     <td align="left" colspan="18"><b><?php echo $cluster;?></b></td>
    </tr>
  <?php
	$result	= $obj_salesProjection_get->get_actuals_result($year,$month,$marketer,$toCureency,$clusterType);
	//die();
	while($row=mysqli_fetch_array($result)){
	$SampleNo	= $row['intSampleNo'];
	$SampleYear	= $row['intSampleYear'];
	$RevisionNo	= $row['intRevisionNo'];
	$PrintName	= $row['strPrintName'];
		
  ?>
  <tr>
    <td><?php echo $row['customer'];?></td>
    <td><?php echo $row['intOrderNo'];?></td>
    <td><?php echo $row['intOrderYear'];?></td>
    <td><?php echo $row['strSalesOrderNo'];?></td>
    <td><?php echo $row['strStyleNo'];?></td>
    <td><?php echo $row['strGraphicNo'];?></td>
    <td><?php echo $row['intSampleYear'];?></td>
    <td><?php echo $row['intSampleNo'];?></td>
    <td><?php echo $row['intRevisionNo'];?></td>
    <td><?php echo $row['strCombo'];?></td>
    <td><?php echo $row['strPrintName']."/".$row['part'];?></td>
    <td><div id="divPicture" class="tableBorder_allRound clsPicture" align="center" contenteditable="true" style="width:70px;height:70px;overflow:hidden" ><?php echo "<img id=\"saveimg\" style=\"width:70px;height:70px;\" src=\"documents/sampleinfo/samplePictures/$SampleNo-$SampleYear-$RevisionNo-".substr($PrintName,6,1).".png\" />"; ?></div></td>
    <td><?php echo $row['color'];?></td>
    <td align="right"><?php echo number_format(round($row['intQty']),0);?></td>
    <td align="right"><?php echo number_format($row['dblPrice'],2);?></td>
    <td align="right"><?php echo number_format(round($row['dispQty']),2);?></td>
    <td><?php echo $row['dtPSD'];?></td>
    <td><?php echo $row['strCustomerPoNo'];?></td>
  </tr>
  <?php
	  $totQty+=$row['intQty'];
	  $totAmount+=round($row['dispQty']);
	}
	$grand_totQty+=$totQty;
	$grand_totAmount+=$totAmount;
  ?>
  <tr bgcolor="#F3F3F3">
    <td colspan="15" align="right"><b>Total Amount </b></td>
     <td align="right"><b><font size="2"><?php echo number_format($totAmount,2);?></font></b></td>
    <td colspan="2"></td>
   </tr>
  <?php
   }
  ?>
  <?php
  if($noOfclusters>1){
  ?>
  <tr bgcolor="#F3F3F3">
    <td colspan="15" align="right"><b>Grand Amount </b></td>
     <td align="right"><b><font size="2"><?php echo number_format($grand_totAmount,2);?></font></b></td>
    <td colspan="2"></td>
   </tr>
   <?php
  }
   ?>
</table>

  </td>
  <td width="10">&nbsp;</td>
  </tr>
  
  <tr>
    <td colspan="8" height="15"></td>
    </tr>
  
  <tr>
    </tr>
  </table>
  
  <td width="10">&nbsp;</td>
</tr>        
      
      </table>
    </td>
</tr>
<tr height="90" >
  <td align="center" class="normalfntMid"></td>
</tr>
</table>
</body>
 
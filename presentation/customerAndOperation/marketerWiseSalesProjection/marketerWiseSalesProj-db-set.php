<?php
session_start();
$backwardseperator 	= "../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$requestType 		= $_REQUEST['requestType'];
$locationId			= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];

$programName		= 'Marketer Wise Sales Projection';
$programCode		= 'P0668';

include "{$backwardseperator}dataAccess/Connector.php";
require_once $_SESSION['ROOT_PATH']."class/cls_permisions.php";
//---------------------------
$objpermisionget= new cls_permisions($db);
//---------------------------


if($requestType=='saveData')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	
	$editMode       = 0;
	$rollBackFlag 	= 0;
	$startDate		= $_REQUEST['startDate'];
	$endDate		= $_REQUEST['endDate'];
	$marketer		= $_REQUEST['marketer'];
	$customer		= $_REQUEST['customer'];
	$orderNo		= ($_REQUEST['orderNo']==""?'null':$_REQUEST['orderNo']);
	$orderYear		= ($_REQUEST['orderYear']==""?'null':$_REQUEST['orderYear']);
	$style			= $_REQUEST['style'];
	$salesOrder		= $_REQUEST['salesOrder'];
	$graphicNo		= $_REQUEST['graphicNo'];
	$sampleYear		= $_REQUEST['sampleYear'];
	$sampleNo		= $_REQUEST['sampleNo'];
	$revisionNo		= $_REQUEST['revisionNo'];
	$combo			= $_REQUEST['combo'];
	$cboprint		= $_REQUEST['cboprint'];
	$description	= $_REQUEST['description'];
	$orderQty		= $_REQUEST['orderQty'];
	$unitPrice		= $_REQUEST['unitPrice'];
	$amount			= $_REQUEST['amount'];
	$psdDay			= ($_REQUEST['psdDay']==""?'null':$_REQUEST['psdDay']);
	$psdYear  = substr($psdDay,0,4); 
	$psdMonth = substr($psdDay,5,2);  
	$psdDay   = substr($psdDay,8,2);
	$dateArray = explode('-', $startDate);
	$year = $dateArray[0];
	$month = $dateArray[1];
	 	
	$PONo			= $_REQUEST['PONo'];

	
	$saved			= $_REQUEST['saved'];
	$rowId			= 0;
	$editPermission = loadEditMode($programCode,$userId);
	$deleteMode		= loadDeleteMode($programCode,$userId);
	
	if(!validatePONo($PONo))
	{
		$rollBackFlag 	= 1;
		$rollBackMsg  	= "Invalid PO no.";
	}	
	/*if(($psdYear != $year))
	{
		//$rollBackFlag 	= 1;
		//$rollBackMsg  	= "Year of PSD date should be equal to selected 'Year'.";
	}
	else if(($psdMonth != $month))
	{
		//$rollBackFlag 	= 1;
		//$rollBackMsg  	= "Month of PSD date should be equal to selected 'Month'.";
	}*/
	
	if($rollBackFlag != 1){
	if($saved==0)
	{
		  $sql = "INSERT INTO plan_sales_project 
				(
				intYear, intMonth, intMarketer, 
				intCustomer, intOrderNo, intOrderYear,strSalesOrderNo, 
				strStyleNo, strGraphicNo, intSampleNo, 
				intSampleYear, intRevNo, strCombo, 
				strPrintName,strDescription, strCostomerPO, 
				intOrderQty, dblAmount, dblUnitPrice, 
				dtPSDDay, intCreateId, dtmCreateUserId,LOCATION_ID
				)
				VALUES
				(
				'$year', '$month', '$marketer', 
				'$customer', $orderNo, $orderYear,'$salesOrder',
				'$style', '$graphicNo', '$sampleNo', 
				'$sampleYear', '$revisionNo', '$combo', 
				'$cboprint','$description', '$PONo', 
				'$orderQty', '$amount', '$unitPrice', 
				$psdDay, '$userId', now(),'$locationId'
				); ";
		
		$result = $db->RunQuery2($sql);
		$rowId	= $db->insertId;
		
		if(!$result)
		{
			$rollBackFlag 	= 1;
			$rollBackMsg  	= $db->errormsg;
			$sqlE			= $sql;
		}
		else
		{
			$editMode		= 0;
		}
	}
	else
	{
		if($editPermission==0)
		{
			$rollBackFlag 	= 1;
			$rollBackMsg  	= "You have not permission to Edit.";
		}
		else
		{
			  $sql = "UPDATE plan_sales_project 
					SET
					intCustomer = '$customer' , 
					intOrderNo = $orderNo , 
					intOrderYear = $orderYear ,
					strSalesOrderNo = '$salesOrder' ,
					strStyleNo = '$style' , 
					strGraphicNo = '$graphicNo' , 
					intSampleNo = '$sampleNo' , 
					intSampleYear = '$sampleYear' , 
					intRevNo = '$revisionNo' , 
					strCombo = '$combo' , 
					strPrintName = '$cboprint' , 
					strDescription = '$description' , 
					strCostomerPO = '$PONo' , 
					intOrderQty = '$orderQty' , 
					dblAmount = '$amount' , 
					dblUnitPrice = '$unitPrice' , 
					intYear = '$psdYear',
					intMonth = '$psdMonth',
					dtPSDDay = '$psdDay',
					LOCATION_ID = '$locationId'
					WHERE
					/*intYear = '$year' AND 
					intMonth = '$month' AND 
					intMarketer = '$marketer' AND */
					intId = '$saved' ";
			
			$result = $db->RunQuery2($sql);
			if(!$result)
			{
				$rollBackFlag 	= 1;
				$rollBackMsg  	= $db->errormsg;
				$sqlE			= $sql;
			}
			else
			{
				$editMode		= 1;
				$rowId			= $saved;
			}
		}
	}
	}
	if($rollBackFlag==1)
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
		$response['sql'] 		= $sqlE;
	}
	else
	{
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		if($editMode==1)
			$response['msg'] 	= 'Updated successfully.';
		else
			$response['msg'] 	= 'Saved successfully.';
		$response['rowId'] 		= $rowId;
		$response['editMode'] 	= $editPermission;
		$response['delMode'] 	= $deleteMode;
	}
	$db->CloseConnection();	
	echo json_encode($response);
	
}
else if($requestType=='deleteData')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	
	$rollBackFlag 	= 0;
	$rowId  		= $_REQUEST['rowId'];
	$year 			= $_REQUEST['year'];
	$month  		= $_REQUEST['month'];
	$marketer  		= $_REQUEST['marketer'];
	
	$deleteMode		= loadDeleteMode($programCode,$userId);
	if($deleteMode==0)
	{
		$rollBackFlag 	= 1;
		$rollBackMsg  	= "You have not permission to Delete.";
	}
	else
	{
		$sql =" DELETE FROM plan_sales_project 
				WHERE
				intId = '$rowId' /*AND 
				intYear = '$year' AND 
				intMonth = '$month' AND 
				intMarketer = '$marketer'*/ ";
		
		$result = $db->RunQuery2($sql);
		if(!$result)
		{
			$rollBackFlag 	= 1;
			$rollBackMsg  	= $db->errormsg;
		}
	}
	if($rollBackFlag==1)
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
		$response['sql'] 		= $sql;
	}
	else
	{
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		$response['sql'] 		= $sql;
	}
	
	$db->CloseConnection();	
	echo json_encode($response);
	
}
else if($requestType=='deleteSystemOrders')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	
	$rollBackFlag 	= 0;
	$rowId  		= $_REQUEST['rowId'];
	$orderNo 			= $_REQUEST['orderNo'];
	$orderYear  		= $_REQUEST['orderYear'];
	$salesOrderId  		= $_REQUEST['salesOrderId'];
	
	//$deleteSOMode 	= boolSPermision(13);
	$deleteSOMode 	= $objpermisionget->boolSPermision2(13);
	
	if($deleteSOMode=!1)
	{
		$rollBackFlag 	= 1;
		$rollBackMsg  	= "You have not permission to Delete.";
	}
	else
	{
		$sql =" UPDATE `trn_orderdetails` 
				SET intViewInSalesProjection ='0' 
				WHERE
				intOrderNo = '$orderNo' AND 
				intOrderYear = '$orderYear' AND 
				intSalesOrderId = '$salesOrderId' ";
		
		$result = $db->RunQuery2($sql);
		if(!$result)
		{
			$rollBackFlag 	= 1;
			$rollBackMsg  	= $db->errormsg;
		}
	}
	if($rollBackFlag==1)
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
	}
	else
	{
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
	}
	
	$db->CloseConnection();	
	echo json_encode($response);
	
}

function loadEditMode($programCode,$userId)
{
	global $db;
	$editMod = 0;
	
	$sql = "SELECT
			menupermision.intEdit  
			FROM menupermision 
			INNER JOIN menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode =  '$programCode' AND
			menupermision.intUserId =  '$userId' ";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	
	if($row['intEdit']==1)
	{
	 	$editMod = 1;
	}
	return $editMod;
}
function loadDeleteMode($programCode,$userId)
{
	global $db;
	$deleteMode = 0;
	
	$sql = "SELECT
			menupermision.intDelete  
			FROM menupermision 
			INNER JOIN menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode =  '$programCode' AND
			menupermision.intUserId =  '$userId' ";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	
	if($row['intDelete']==1)
	{
	 	$deleteMode = 1;
	}
	return $deleteMode;
}
function boolSPermision1($menuId)
{
	global $db;
	global $userId;
	$sql = "SELECT
				menus_special_permision.intUser
			FROM menus_special_permision
			WHERE
				menus_special_permision.intSpMenuId 	=  '$menuId' AND
				menus_special_permision.intUser 		=  '$userId'
			";
	$result = $db->RunQuery2($sql);
	if( mysqli_num_rows($result)>0)
		return 1;
	else
		return 0;		
}

function validatePONo($poNo)
{
	global $db;
	$sql = "SELECT
				trn_orderheader.strCustomerPoNo
			FROM `trn_orderheader`
			WHERE
				trn_orderheader.strCustomerPoNo = '$poNo'
			";
	$result = $db->RunQuery2($sql);
	if( mysqli_num_rows($result)>0)
		return true;
	else
		return false;		
}


?>
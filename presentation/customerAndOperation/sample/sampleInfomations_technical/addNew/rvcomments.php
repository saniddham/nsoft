<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
$backwardseperator 	= "../../../../../";
$sampleYear 		= $_REQUEST['txtSampleYear'];
$sampleNo			= $_REQUEST['txtSampleNo'];
$revNo				= $_REQUEST['txtRevNo'];
$userId 	 		= $_SESSION['userId'];
$mainPath 			= $_SESSION['mainPath'];

include "{$backwardseperator}dataAccess/Connector.php";
include "{$backwardseperator}libraries/mail/mail.php";
require_once $_SESSION['ROOT_PATH']."class/cls_mail.php";

$objMail = new cls_create_mail($db);

$error 				= "";

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$txtRVcomments 	= $_REQUEST['txtRVcomments'];
	$txtRVcomments 	= str_replace("'","''",$txtRVcomments);
	
	if($txtRVcomments=='')
		$error = 'Please enter a comment.';	
	else if($sampleNo=='' || $revNo=='' )
		$error = 'Please select a sample No and Revision No.';	
	else
	{
		$sql =" SELECT IFNULL(MAX(RV_COMMENTS_NO),0) as maxNo FROM 
				trn_sampleinfomations_rv_comments 
				WHERE SAMPLE_NO='$sampleNo' AND 
				SAMPLE_YEAR='$sampleYear' AND 
				REV_NO='$revNo' ";
		$result = $db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		$newNo 	= $row['maxNo']+1;
		
		$sqlIns = "INSERT INTO trn_sampleinfomations_rv_comments 
					(SAMPLE_YEAR, 
					SAMPLE_NO, 
					REV_NO, 
					RV_COMMENTS_NO, 
					RV_COMMENTS
					)
					VALUES
					('$sampleYear', 
					'$sampleNo', 
					'$revNo', 
					$newNo, 
					'$txtRVcomments'
					);";
		$resultIns = $db->RunQuery($sqlIns);
		if(!$resultIns)
		{
			$$error = 'Comment saving error.';
		}
		else
		{
			$sql = "SELECT DISTINCT intCreator
					FROM trn_sampleinfomations
					WHERE intSampleNo='$sampleNo' AND
					intSampleYear='$sampleYear' AND
					intRevisionNo='$revNo' ";
			
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				if($userId!=$row['intCreator'])
				{
					$mailSendUserId = $row['intCreator'];
					$sqlAdd = " SELECT sys_users.strFullName
								FROM sys_users
								WHERE intUserId='$userId' ";
					
					$resultAdd = $db->RunQuery($sqlAdd);
					$rowAdd = mysqli_fetch_array($resultAdd);
					$addByFullName = $rowAdd['strFullName'];
					sendCommentAddMail($sampleYear,$sampleNo,$revNo,$newNo,$txtRVcomments,$objMail,$mainPath,$root_path,$mailSendUserId,$addByFullName);
				}
			}
		}
	}
}
function sendCommentAddMail($sampleYear,$sampleNo,$revNo,$newNo,$txtRVcomments,$objMail,$mainPath,$root_path,$mailSendUserId,$addByFullName)
{
	global $db;
	$sql = "SELECT strFullName,strUserName,strEmail FROM sys_users WHERE intUserId='$mailSendUserId' ";
	
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$enterUserName  = $row['strFullName'];
		$enterUserEmail = $row['strEmail'];
		
		$header=$addByFullName." added RA Comments on ".$sampleYear."/".$sampleNo;
		
		$_REQUEST = NULL;
		$_REQUEST['sampleNo'] 	= $sampleNo;
		$_REQUEST['sampleYear'] = $sampleYear;
		$_REQUEST['revNo'] 		= $revNo;
		$_REQUEST['comNo'] 		= $newNo;
		$_REQUEST['reciver']  	= $enterUserName;
		$_REQUEST['sender']   	= $addByFullName;
		$_REQUEST['comment']	= $txtRVcomments;
		
		$_REQUEST['link']=base64_encode($mainPath."presentation/customerAndOperation/sample/sampleInfomations_technical/addNew/sampleInfomations.php?sampleNo=$sampleNo&sampleYear=$sampleYear&revNo=$revNo"); 
		$path=$root_path."presentation/customerAndOperation/sample/Report/mailTemplates/torvcommentemail.php";
		$objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
		
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>RV Comments</title>
<script type="application/javascript">
	function addRVcomments()
	{
		document.frmRVcomments.submit();
	}
</script>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form id="frmRVcomments" name="frmRVcomments" action="rvcomments.php" method="post">
<table width="410" height="127" border="0">
  <tr class="normalfnt">
    <td width="69" height="23" align="center" bgcolor="#FFCCCC">No</td>
    <td width="331" align="center" bgcolor="#FFCCCC">RA Comments</td>
  </tr>
  <?php
  if($sampleNo!='' && $sampleYear!='' && $revNo!='')
  {
  	$sql = "SELECT RV_COMMENTS_NO,RV_COMMENTS FROM trn_sampleinfomations_rv_comments
			WHERE SAMPLE_NO='$sampleNo' AND
			SAMPLE_YEAR='$sampleYear' AND
			REV_NO='$revNo' ";
	
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
	?>
        <tr>
        	<td class="normalfntMid" valign="top"><?php echo $row['RV_COMMENTS_NO']; ?></td>
        	<td style="text-align:left" class="normalfnt"><?php echo $row['RV_COMMENTS']; ?></td>
        </tr>
    <?php
	}
  }
  ?>
  <tr>
    <td height="23" class="normalfntMid">&nbsp;</td>
    <td><textarea name="txtRVcomments" id="txtRVcomments" cols="55" rows="5" ></textarea></td>
  </tr>
  <tr>
    <td height="23" align="center">&nbsp;</td>
    <td><img src="../../../../../images/add_alone.png" width="72" height="18" class="mouseover" id="butAddComment" onclick="addRVcomments();" />
    <input value="<?Php echo $sampleYear; ?>" style="width:50px;visibility:hidden" type="text" name="txtSampleYear" id="txtSampleYear" />
    <input value="<?Php echo $sampleNo; ?>" style="width:50px;visibility:hidden" type="text" name="txtSampleNo" id="txtSampleNo" />
    <input value="<?Php echo $revNo; ?>" style="width:50px;visibility:hidden" type="text" name="txtRevNo" id="txtRevNo" /></td>
  </tr>
  <tr>
    <td height="23" align="center">&nbsp;</td>
    <td><div align='left'><span class="normalfnt" style="color:#F00;text-align:left"><?php echo $error; ?></span></div></td>
  </tr>
</table>
</form>
</body>
</html>
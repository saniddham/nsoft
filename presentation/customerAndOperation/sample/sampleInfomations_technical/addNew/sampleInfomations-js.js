  var basepath				= 'presentation/customerAndOperation/sample/sampleInfomations_technical/addNew/';
var arr 					= [];
var techniqueTableScript 	= '';

$(document).ready(function() {
	$("#cboMainCategory").die('change').live('change',function(){
			loadSubCategory($(this).val(),this);
	});

	$("#cboTechnique").die('change').live('change',function(){
		$(this).find("option[value='"+$(this).val()+"']").attr("selected", true);
	});

	$("#cboSubCategory").die('change').live('change',function(){
			loadItem($(this).val(),this);
	});
	
	 $("#cboItem").die('change').live('change',function(){
			$(this).find("option[value='"+$(this).val()+"']").attr("selected", true);
			loaduom($(this).val(),this);
	});
	
	$("#txtShots").die('keyup').live('keyup',function(){
 		$(this).attr("value", $(this).val());
	});
	
	$("#txtWeight").die('keyup').live('keyup',function(){
 		$(this).attr("value", $(this).val());
	});
	
	$(".butAddNewTechniqueItem").die('click').live('click',addNewTechniqueItemRow);
	
	
	$('#frmSampleInfomations #cboYear').die('change').live('change',function(){
		window.location.href  = "?q=369&sampleYear="+$('#cboYear').val();
		//loadSampleNos();
	});
	$(".butTypeOfPrint").die('click').live('click',function(){
			setToTypeButton(this);
	});
	$(".butColors").die('click').live('click',function(){
		setToColorGrid(this);
	});
	$("#frmSampleInfomations .butCopy").die('click').live('click',setToCopy);
	
	$(".rmvParent").die('click').live('click',function(){
 		if($(this).parent().parent().find('.colSaved').html()==1){
			removeRow_db(this);
		}
		$(this).parent().parent().remove();
	});
	
	$(".removeRow").die('click').live('click',function(){
		removeRow(this);
	});
	
	$(".removeTechItmRow").die('click').live('click',function(){
		removeTechSampItem(this,1);
	});
  
        $("#delImg").die('click').live('click',function(){
            
			remove(this,0);//image 
                        
	});
	$("#frmSampleInfomations .removeRow2").die('click').live('click',function(){
			removeRow2(this);
	});
	$("#butTechnique").die('click').live('click',function(){
			setToTechnique(this);
	});
	$("#frmSampleInfomations .butAddNewTechnique").die('click').live('click',addNewTechniqueRow);

	$("#frmSampleInfomations .butProcess").die('click').live('click',function(){
  		addProcess(this);
		
	});

	$("#frmSampleInfomations .butCopyProcess").die('click').live('click',function(){
  		copyProcess(this);
		
	});
	
	$("#frmSampleInfomations .removeRow").die('click').live('click',function(){
			removeRow(this);
	});
	
	$("#frmSampleInfomations .butColors").die('click').live('click',function(){
 		setToColorGrid(this);
		
	});
	
	$('#frmColors #butSearch').die('click').live('click',function(){
		var url = basepath+"sampleInfomations-db-get.php?requestType=loadColors&text="+$('#txtColorName').val();
		var obj = $.ajax({url:url,async:false});
		$('#tblColors').html(obj.responseText);
	});
	
	$('#frmSampleInfomations .cboTech').die('change').live('change',function(){
		$(this).parent().attr('id',$(this).val())
		loadTechItems(this);
	});

	$('#frmSampleInfomations .techItem').die('change').live('change',function(){
		setParentItemsForQtyWHGrid(this);
	});


	
	techniqueTableScript = "<tr class=\"normalfnt\">"+
	                 "<td bgcolor=\"#FFFFFF\"><img src=\"images/del.png\" class=\"mouseover removeRow\" /></td>"+ 
                     " <td width=\"40%\" bgcolor=\"#FFFFFF\"><select  id=\"cboTechnique\" name=\"select4\" class=\"cboTechnique\" style=\"width:92px\">"+
                    "<option value=\"\"></option>"+
                 " </select></td>"+
                    "  <td width=\"13%\" bgcolor=\"#FFFFFF\"><input   style=\"width:30px\"   type=\"text\" name=\"textfield5\" id=\"txtShots\" /></td>"+
                      "<td width=\"35%\" bgcolor=\"#FFFFFF\"><select    id=\"cboItem\" name=\"select5\" class=\"cboItem\" style=\"width:100px\">"+
                        "<option value=\"\"></option>"+
                      "</select></td>"+
                      "<td width=\"12%\" bgcolor=\"#FFFFFF\"><input   style=\"width:30px\"   type=\"text\" name=\"textfield5\" id=\"txtWeight\" /></td>"+
                    "</tr>";
				  
	
	//clickEventForAddColumn();
	
	
	$.fn.sort_select_box = function(){
    // Get options from select box
    var my_options = $("#" + this.attr('id') + ' option');
    // sort alphabetically
    my_options.sort(function(a,b) {
        if (a.text > b.text) return 1;
        else if (a.text < b.text) return -1;
        else return 0
    })
	
	
	/// remove collumn function ///////////////////
	$.fn.removeCol = function(col){
    // Make sure col has value
    if(!col){ col = 1; }
    $('tr td:nth-child('+col+'), tr th:nth-child('+col+')', this).remove();
    return this;
	};

   //replace with sorted my_options;
   $(this).empty().append( my_options );

   // clearing any selections
   	$("#"+this.attr('id')+" option").attr('selected', false);
	}

	
  		$("#frmSampleInfomations").validationEngine();
		$('#frmSampleInfomations #txtCode').focus();
		
		
		//setButtonEvents();
		
		
		//alert(isOldRevNo);
  //permision for add 
/*  if(intAddx)
  {
 	$('#frmSampleInfomations #butNew').show();
	if(isOldRevNo==0 && intTechnicalStatus!=1)
		$('#frmSampleInfomations #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
	if(isOldRevNo==0 && intTechnicalStatus!=1)
  		$('#frmSampleInfomations #butSave').show();
	$('#frmSampleInfomations #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmSampleInfomations #butDelete').show();
	$('#frmSampleInfomations #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmSampleInfomations #cboSearch').removeAttr('disabled');
  }*/
  
  	$('#frmSampleInfomations .removeColumn').die('click').live('click',function(){
		var cel  = $(this).parent();
		var celIndex = cel.parent().children().index(cel);
		var cellCount = ($(this).parent().parent().find('td').length);
		if(cellCount>3)
		$('#tblMain >tbody >tr td:nth-child('+(celIndex+1)+')').remove();
	});
	
	/////////////////////////////// BUT COPY ALL ///////////////////////////////////
	$('#frmSampleInfomations #butCopyAll').die('click').live('click',function(){
	//this is to copy items to the combo prints where techniques are equal (no need to be same color)
 	//	$('#frmSampleInfomations #butCopyAll').css('display', 'none');
		showWaiting();
 		//alert(1);
			var inkId		='';
			var shots		='';
			var itemId		='';
			var color		='';
			var technique	='';
			var objM		='';
			var objC		='';
			
			var inkId_c		='';
			var shots_c		='';
			var itemId_c	='';
			var color_c		='';
			var technique_c	='';
			var c			=0;

			var r_cont		=$('#tblMain tr').length;
			var numCols 	=$("#tblMain").find('tr')[0].cells.length;
		
			$('#tblMain >tbody >tr:eq(3) >td:eq(1) >#tblGrid2 >tbody >tr').each(function(){//first combo row
				var r =  '#tblMain >tbody >tr:eq(3) >td:eq(1) >#tblGrid2 tr';
				objM=this;
				color = $(objM).find('td:eq(0)').attr('id');
				technique = $(objM).find('td:eq(1)').attr('id');
 
					$(objM).find('#tblGrid3').each(function(){
						//first combo ink types
						c++;
						var objT=this;
						var html	=$(objT).find('td').html();
						var inkId	= $(objT).find('#cboTechnique').val();
						var itemId	= $(objT).find('#cboItem').val();
						
					for(var l=3;l<r_cont;l++){		
						for(var d=1;d<numCols;d++){	
							if(l!=3 || d!=1){	
								$('#tblMain >tbody >tr:eq('+l+') >td:eq('+d+') >#tblGrid2 >tbody >tr').each(function(){//other comboes
									var objM_S	=this;
									color_S		 = $(objM_S).find('td:eq(0)').attr('id');
									technique_S	 = $(objM_S).find('td:eq(1)').attr('id');
									$(objM_S).find('#tblGrid3').each(function(){
										var objT_S=this;
										if(color_S==color && technique_S==technique){
											//alert($(objT).html());
											$(objT_S).html($(objT).html());
										}
									});
								});
							}
						}
					}
 				});
 		
			});


			alert('Coppied Successfully.');
			hideWaiting();
	});
	////////////////////////////////////////////////////////////////////////////////
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/
	
  	$('#frmSampleInfomations #cboCustomer').die('change').live('change',function(){
		$('#frmSampleInfomations #cboBrand').html(combo_brand($(this).val()));
	});

  ///save button click event
  $('#frmSampleInfomations #butSave').die('click').live('click',function(){
	//$('#frmSampleInfomations').submit();
	//$('#frmSampleInfomations #butSave');
	var requestType = '';
	if ($('#frmSampleInfomations').validationEngine('validate'))   
    {
        $('#frmSampleInfomations #butSave').hide();
		showWaiting();

		var data = "requestType=save";
		
			data+="&sampleNo="		+	$('#cboSampleNo').val();
			data+="&sampleYear="	+	$('#cboYear').val();
			data+="&revisionNo="	+	$('#cboRevisionNo').val();
			data+="&date="			+	$('#dtDate').val();
			
			data+="&graphicNo="		+	$('#txtGraphicRefNo').val();
			data+="&customerId="	+	$('#cboCustomer').val();
			data+="&styleNo="		+	$('#txtStyleNo').val();
			data+="&brandId="		+	$('#cboBrand').val();
			
			data+="&verivide_d65="	+	$('#txtVerivide_D65').is(':checked');
			data+="&verivide_tl85="	+	$('#txtVerivide_TL84').is(':checked');
			data+="&verivide_cw="	+	$('#txtVerivide_CW').is(':checked');
			data+="&verivide_f="	+	$('#txtVerivide_F').is(':checked');
			data+="&verivide_uv="	+	$('#txtVerivide_UV').is(':checked');
			
			data+="&macbeth_dl="	+	$('#txtMacbeth_DL').is(':checked');
			data+="&macbeth_cw="	+	$('#txtMacbeth_CW').is(':checked');
			data+="&macbeth_inca="	+	$('#txtMacbeth_INCA').is(':checked');
			data+="&macbeth_tl84="	+	$('#txtMacbeth_TL84').is(':checked');
			data+="&macbeth_uv="	+	$('#txtMacbeth_UV').is(':checked');
			data+="&macbeth_horizon="	+	$('#txtMacbeth_Horizon').is(':checked');
			
			data+="&curing_temp="	+	$('#txtCuringTemp').val();
			data+="&curing_speed="	+	$('#txtCuringBeltSpeed').val();
			data+="&press_temp="	+	$('#txtPressTemp').val();
			data+="&press_pressure="+	$('#txtPressPressure').val();
			data+="&press_time="	+	$('#txtPressTime').val();
			data+="&checkStatus="	+	window.checkStaus;
			
			data+="&meshCount="	+	$('#txtMeshCount').val();
			data+="&instruction="+	URLEncode(document.getElementById('txtInstructions').value) ;
			data+="&instructionTech="+	$('#txtInstructionsTech').val();
			
			//data+="&image="	+	$('#divPicture >img').attr('src');
			
			/*var arrTechnique="[ ";
			$('.chkTechnique:checked').each(function(){
				arrTechnique += '{ "techniqueId":"'+$(this).val()+ '"},';
			});
			arrTechnique = arrTechnique.substr(0,arrTechnique.length-1);
			arrTechnique += " ]";
			
			data+="&arrTechnique="	+	arrTechnique;*/
								/////////////////////////// create part and width height //////////////////////////
			var arrPart = '';
			var printCount  =0;
			//arrPart += '[';
			//$('#tblMain >tbody >tr:eq('+i+') >td:eq('+n+')> .tblColors >tbody >tr:eq(1)>td:eq(0) >span').each(function(){
			$('#tblMain >tbody >tr:eq(1) >td').not(':first').not(':last').each(function(){
				
				var partId = $(this).find('.part').val();	
				var size_w = $(this).find('.sizeW').val();	
				var size_h = $(this).find('.sizeH').val();	
				arrPart += '{"partId":"'+partId+'","size_w":"'+size_w+'","size_h":"'+size_h+'"},';
				printCount++;
			});
			if(arrPart!='')
			{
				arrPart = arrPart.substr(0,arrPart.length-1);
				data+="&arrPart=["+	arrPart+']';	
			}
			
			var rowCount = $('#tblMain >tbody >tr').length;
			var cellCount = document.getElementById('tblMain').rows[1].cells.length;
			var row = 0;
			
			var arrCombo="[";
			
			for(var i=3;i<rowCount-1;i+=2)
			{
				var combo 			= document.getElementById('tblMain').rows[i-1].cells[0].childNodes[0].value;
				
				var printMode 		= document.getElementById('tblMain').rows[i].cells[0].childNodes[1].value;
				var washStanderd 	= document.getElementById('tblMain').rows[i].cells[0].childNodes[3].value;
				var groundColor 	= document.getElementById('tblMain').rows[i].cells[0].childNodes[5].value;
				var fabricType 		= document.getElementById('tblMain').rows[i].cells[0].childNodes[7].value;
				//alert(cellCount);
				for(var n=1;n<cellCount-1;n++)
				{
					//alert(n);
					 arrCombo += "{";
					 
					var printName = 	'print '+(n);//document.getElementById('tblMain').rows[0].cells[n].childNodes[0].value;
					var techniqueId = 	document.getElementById('tblMain').rows[1].cells[n].childNodes[0].value;
					var colorId 	= 	document.getElementById('tblMain').rows[i].cells[n].childNodes[0].value;
					
					//var sizeW 	= 	document.getElementById('tblSizes').rows[n-3].cells[1].childNodes[0].rows[0].cells[0].childNodes[0].value;
					//var sizeH 	= 	document.getElementById('tblSizes').rows[n-3].cells[1].childNodes[0].rows[0].cells[2].childNodes[0].value;
					//var partId 	= 	document.getElementById('tblSizes').rows[n-3].cells[2].childNodes[0].value;
					
					
					arrCombo += '"printMode":"'+	printMode +'",' ;
					arrCombo += '"washStanderd":"'+	washStanderd +'",' ;
					arrCombo += '"comboName":"'+	combo +'",' ;
					arrCombo += '"groundColor":"'+	groundColor +'",' ;
					arrCombo += '"fabricType":"'+	fabricType +'",' ;
					arrCombo += '"printName":"'+	printName +'",' ;
					arrCombo += '"gridColMarketingDetails":[';
					var grid2Id=0;
					var gridColTechManDetails='';
					var gridTechnicalDetails='';
					var gridTIDetails = "";
					var gridTIId = 0;
			
					$('#tblMain >tbody >tr:eq('+i+') >td:eq('+n+')').find('#tblGrid2 >tbody >tr:not(:first)').each(function(){
						grid2Id++;
						var colorId 	= $(this).find('td:eq(0)').attr('id');
						var flag_newColor	=$(this).find('.colType').html();
						var new_col_saved	=$(this).find('.colSaved').html();
						var techId_p  	= '';
						if(flag_newColor==1 && new_col_saved !=1)
						techId_p  	= $(this).find('#cboTechnique').val();
						else
						techId_p  	= $(this).find('td:eq(1)').attr('id');

						
 						arrCombo +='{"colorId":"'+colorId+'",'+
								'"techId_p":"'+techId_p+'",'+
								'"flag_newColor":"'+flag_newColor+'",'+
								'"marketingItems":[';

                                           
					//marketingItems details for relevant colour and technique
					var r=0;
					$('#tblMain >tbody >tr:eq('+i+') >td:eq('+n+')').find('#tblGrid2 >tbody >tr:eq('+grid2Id+')').find('#tblGridTI >tbody >tr:not(:last)').each(function(){
						r++;
 						var objTI = this
							var itemId  = $(objTI).find('#cboItem').val();
 							var itemC	='';
							var qty		='';
							var w		='';
							var h		='';
							$(objTI).parent().parent().parent().parent().find('#tblGridTIQ >tbody >tr').each(function(){
							itemC	= $(this).find('.parentItem').html()
							if(itemC==itemId)
								qty  = $(this).find('#txtQty').val();
							});
							
 							$(objTI).parent().parent().parent().parent().find('#tblGridTIWH >tbody >tr').each(function(){
							itemC	= $(this).find('.parentItem').html()
								if(itemC==itemId){
									w  = $(this).find('#txtSizeW').val();
									h  = $(this).find('#txtSizeH').val();
								}
							});
							//alert(qty+"/"+w+"/"+h);
							arrCombo +='{'+
 									'"itemId":"'+itemId+'",'+
									'"qty":"'+qty+'",'+
									'"w":"'+w+'",'+
									'"h":"'+h+'"'+
									'},';
					});
					
					if(r>0)
					{
						arrCombo = arrCombo.substr(0,arrCombo.length-1);	
					}
					
					
					arrCombo +='],"technicalDetails":[';
					
						//technical details
						var r=0;
						$('#tblMain >tbody >tr:eq('+i+') >td:eq('+n+')').find('#tblGrid2 >tbody >tr:eq('+grid2Id+')').find('#tblGrid3 >tbody >tr:not(:last)').each(function(){
							r++;
								var techId  = $(this).find('#cboTechnique').val();
								var shots   = $(this).find('#txtShots').val();
								var itemId  = $(this).find('#cboItem').val();
								var weight  = $(this).find('#txtWeight').val();
								
								arrCombo +='{'+
										'"inkType":"'+techId+'",'+
										'"shots":"'+shots+'",'+
										'"itemId":"'+itemId+'",'+
										'"weight":"'+weight+'"'+
										'},';
										
						}); //end //technical details
						if(r>0)
						{
							arrCombo = arrCombo.substr(0,arrCombo.length-1);	
						}
 						arrCombo +=  ']},';//END OF gridColMarketingDetails
						
				});
				arrCombo = arrCombo.substr(0,arrCombo.length-1);	
				arrCombo +=  ']},';
					
				}
				
				//arrCombo = arrCombo.substr(0,arrCombo.length-1);	
				//arrCombo += "},";
			}
                        
 			arrCombo = arrCombo.substr(0,arrCombo.length-1);
			arrCombo += "]";
			///////////////////// end of combo array /////////////////////////////////
			
			data+="&arrCombo="	+	arrCombo;
                        
                        
                     
		//------------------------------------------------------------------
		var errorFlag;
		var x=0;
		var arrNonDirRm    ='';
		$('#tblGrid5 >tbody >tr').not(':first').each(function(){

			var item         = $(this).find('#cboItem').val();
			var consumption  = $(this).find('#txtNonDirRmConsm').val();
			if(item!=""&&consumption!=""){
			if(consumption==0){errorFlag =1;}
			
			arrNonDirRm +='{'+
			
				'"item":"'+item+'",'+
				'"consumption":"'+consumption+'"'+
				'},';
				
			}
		
 		});
		
		
		
		if(arrNonDirRm!='')
		{
		arrNonDirRm = arrNonDirRm.substr(0,arrNonDirRm.length-1);
		data+="&arrNonDirRm=["+    arrNonDirRm+']'; 
		}
		if(errorFlag==1){
					//  showWaiting();//to stop zero value being saved
			alert("consumption for non direct RM should be greater than zero");
			hideWaiting();
		}
		
		//--------------------------------------------------------------------
	/*	var errorFlag;
		var x=0;
		var arrUps    ='';
	
		$('.cls_ups').each(function(){
			var combo         = $(this).parent().parent().find('.combo').html();
			var printI  		  = $(this).parent().parent().find('.print').html();
			var ups  		  = $(this).parent().find('.cls_ups').val();
			if(combo!=""&&print!=""){
			if(ups==0){errorFlag =1;}
			
			arrUps +='{'+
			
				'"combo":"'+URLEncode(combo)+'",'+
				'"print":"'+URLEncode(printI)+'",'+
				'"ups":"'+ups+'"'+
				'},';
				
			}
		
 		});
		
		
		
		if(arrUps!='')
		{
		arrUps = arrUps.substr(0,arrUps.length-1);
		data+="&arrUps=["+    arrUps+']'; 
		}
		if(errorFlag==1){
					//  showWaiting();//to stop zero value being saved
			alert("Number of ups should be greater than zero");
			hideWaiting();
		}
  */                             
/*			$('#tblMain >tbody >tr').each(function(){
				
				var cellCount = $(this).find('td').length;
				if(row>=2)
				{
					var printId = $(this).find('td .printMode').val();
					alert(printId);
				}
				row++;
			});*/
			/////////////////////////////////////////////////////////////////////////////////////
			
			
			//return;
		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basepath+"sampleInfomations-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json", 
			type:'POST', 
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
				    $('#frmSampleInfomations #butSave').show();
					$('#frmSampleInfomations #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//hideWaiting();
                                //$('#frmSampleInfomations').get(0).reset();
						var x=0;
						//hideWaiting();
						//document.location.href = '?q=369&sampleNo='+$('#cboSampleNo').val()+'&sampleYear='+$('#cboYear').val()+'&revNo='+$('#cboRevisionNo').val();
						//alert(1);
				//saveImage(json.sampleNo,json.sampleYear,json.revisionNo,printCount);
				//document.location.href = 'sampleInfomations.php?sampleNo='+json.sampleNo+'&sampleYear='+json.sampleYear+'&revNo='+json.revisionNo;
						//$('#cboSampleNo').html(json.sampleNoOption);
						//$('#cboSampleNo').val(json.sampleNo);
						//$('#cboSampleNo').change();
						//$('#cboRevisionNo').val(json.revisionNo);
						//$('#cboRevisionNo').change();
						
						//var t=setTimeout("alertx("+json.sampleNo+","+json.sampleYear+","+json.revisionNo+","+printCount+")",2000);
						return;
					}
					else
						hideWaiting();
				},
			error:function(xhr,status){
					hideWaiting();
				    $('#frmSampleInfomations #butSave').show();
					$('#frmSampleInfomations #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					//var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
		
		
			
	}
						hideWaiting();
   });
   
  
   
   
   /////////////////////////////////////////////////////
   //// load sampleInfomations details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmSampleInfomations #cboRevisionNo').die('click').live('click',function(){
	   $('#frmSampleInfomations').validationEngine('hide');
   });
    $('#frmSampleInfomations #cboRevisionNo').die('change').live('change',function(){
		$('#frmSampleInfomations').validationEngine('hide');
		
		if($('#frmSampleInfomations #cboRevisionNo').val()=='')
		{
			
			var intSampleNo = $('#cboSampleNo').val();
			document.location.href = '?q=369';
			//$('#frmSampleInfomations').get(0).reset();
			
			$('#cboSampleNo').val(intSampleNo);
			//$('.divPicture').html('');
			return;
		}
		
		document.location.href = '?q=369&sampleNo='+$('#cboSampleNo').val()+'&sampleYear='+$('#cboYear').val()+'&revNo='+$('#cboRevisionNo').val();
		return;
		
		var url = basepath+'sampleInfomations-db-get.php?requestType=loadMainDetails';
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"sampleNo="+$('#cboSampleNo').val()+"&sampleYear="+$('#cboYear').val()+"&revisionNo="+$('#cboRevisionNo').val(),
			async:false,
			success:function(json){
					
					$('#iframeFiles').attr('src','filesUpload.php?txtFolder='+$('#cboSampleNo').val()+'_'+$('#cboYear').val());
					
					$("#iframeFiles").contents().find("#txtFolder").val($('#cboSampleNo').val()+'_'+$('#cboYear').val());
					
					//json  = eval('('+json+')');
					$('#frmSampleInfomations #dtDate').val(				json.dtDate);
					//$('#frmSampleInfomations #dtDeliveryDate').val(		json.dtDeliveryDate);
					$('#frmSampleInfomations #txtGraphicRefNo').val(	json.strGraphicRefNo);
					$('#frmSampleInfomations #cboCustomer').val(		json.intCustomer);
						$('#frmSampleInfomations #cboCustomer').change();
					$('#frmSampleInfomations #txtStyleNo').val(			json.strStyleNo);
					$('#frmSampleInfomations #cboBrand').val(			json.intBrand);
					$('#frmSampleInfomations #txtSampleQty').val(		json.dblSampleQty);
					$('#frmSampleInfomations #txtGrade').val(			json.intGrade);
					$('#frmSampleInfomations #txtFabricType').val(		json.strFabricType);
					$('#frmSampleInfomations #txtVerivide_D65').prop('checked',chk(json.verivide_d65));
					$('#frmSampleInfomations #txtVerivide_TL84').prop('checked',chk(	json.verivide_tl84));
					$('#frmSampleInfomations #txtVerivide_CW').prop('checked',chk(		json.verivide_cw));
					$('#frmSampleInfomations #txtVerivide_F').prop('checked',chk(		json.verivide_f));
					$('#frmSampleInfomations #txtVerivide_UV').prop('checked',chk(		json.verivide_uv));
					$('#frmSampleInfomations #txtMacbeth_DL').prop('checked',chk(		json.macbeth_dl));
					$('#frmSampleInfomations #txtMacbeth_CW').prop('checked',chk(		json.macbeth_cw));
					$('#frmSampleInfomations #txtMacbeth_INCA').prop('checked',chk(	json.macbeth_inca));
					$('#frmSampleInfomations #txtMacbeth_TL84').prop('checked',chk(	json.macbeth_tl84));
					$('#frmSampleInfomations #txtMacbeth_UV').prop('checked',chk(		json.macbeth_uv));
					$('#frmSampleInfomations #txtMacbeth_Horizon').prop('checked',chk(json.macbeth_horizon));
					$('#frmSampleInfomations #txtCuringTemp').val(		json.dblCuringCondition_temp);
					$('#frmSampleInfomations #txtCuringBeltSpeed').val(	json.dblCuringCondition_beltSpeed);
					$('#frmSampleInfomations #txtPressTemp').val(		json.dblPressCondition_temp);
					$('#frmSampleInfomations #txtPressPressure').val(	json.dblPressCondition_pressure);
					$('#frmSampleInfomations #txtPressTime').val(		json.dblPressCondition_time);
					$('#frmSampleInfomations #txtMeshCount').val(		json.strMeshCount);
					$('#frmSampleInfomations #txtInstructions').val(	json.strAdditionalInstructions);
					
					$('#divPicture').html('');
					$(document.createElement("img"))
						.attr({ src: 'documents/sampleinfo/samplePictures/'+$('#cboSampleNo').val()+'-'+$('#cboYear').val()+'-'+$('#cboRevisionNo').val()+'.jpg', title: 'Sample Picture ' })
						.appendTo($('#divPicture'))
						.attr('id','saveimg')
						.click(function(){
							// Do something
						})

					//alert(json.arrTechnique[0]);
					
					
					///insert columns /////////////////////////////////////
					clearRows();
					clearColumns();
					//clearSizes();
					//$('#tblMain >select').each;
					//clearColumns();
					for(var i=1;i<json.columnCount;i++)
					{
						clickEventForAddColumn();
						$('#tblSizes >tbody >tr:last').after('<tr>'+$('#tblSizes >tbody >tr:last').html()+'</tr>');
						
						var nextCellId = parseInt($('#tblMain .dataRow').find('td').length)-5;
						$('#tblSizes >tbody >tr:last').find('td:eq(0)').html('Print '+nextCellId);	
					}
					
					///insert rows /////////////////////////////////////
					for(var i=1;i<json.rowCount;i++)
					{
						$('#tblMain >tbody >tr:last').before('<tr>'+$('#tblMain >tbody >tr:gt(1)').html()+'</tr>');	
					}
					
					////////////////// set values for main grid ////////////////////////////////////
					var arrCombo = json.arrCombo;
					//alert(json.arrPrintName.length);
					var rowCount1 = document.getElementById('tblMain').rows.length;
					for(var i=2;i<rowCount1-1;i++)
					{
						document.getElementById('tblMain').rows[i].cells[0].childNodes[0].value = arrCombo[i-2]['modeId'];
						document.getElementById('tblMain').rows[i].cells[1].childNodes[0].value = arrCombo[i-2]['washId'];
						document.getElementById('tblMain').rows[i].cells[2].childNodes[0].value = arrCombo[i-2]['comboName'];
						document.getElementById('tblMain').rows[i].cells[3].childNodes[0].value = arrCombo[i-2]['groundId'];
						
						//arrInkColor
						var x = 0;
						for(var a=0;a<json.arrInkColor.length;a++)
						{
							
							if(arrCombo[i-2]['comboName']==json.arrInkColor[a]['comboName'])
							{
								document.getElementById('tblMain').rows[i].cells[4+x++].childNodes[0].value = json.arrInkColor[a]['color'];
								//alert(json.arrInkColor[a]['comboName']);
							}
						}
						
					}
					////////////////////////////////////////////////////////////////////////////////
					
					loadGridCheck(json.arrTechnique,'chkTechnique')
					updateTechniqueCombo();
					loadGridCheck(json.arrType,'chkTypeOfPrint')
					
					///////// set report path ///////////////////
					/*$('#butReport').unbind('click');
					$('#butReport').click(function(){
						//alert(1);
						window.open('../../Report/sampleReport.php?no='+$('#cboSampleNo').val()+'&year='+$('#cboYear').val()+'&revNo='+$('#cboRevisionNo').val());	
					});
					$('#butConfirm').unbind('click');
					$('#butConfirm').click(function(){
						window.open('../../Report/sampleReport.php?no='+$('#cboSampleNo').val()+'&year='+$('#cboYear').val()+'&revNo='+$('#cboRevisionNo').val());	
					});*/
					
					/////////////////////////////////////////////
					
					var arrPrintName = json.arrPrintName;
					for(var i=0;i<json.arrPrintName.length;i++)
					{
						document.getElementById('tblMain').rows[0].cells[4+i].childNodes[0].value = arrPrintName[i]['name'];
						document.getElementById('tblMain').rows[1].cells[4+i].childNodes[0].value = arrPrintName[i]['techId'];
						
						document.getElementById('tblSizes').rows[i+1].cells[0].childNodes[0].nodeValue = arrPrintName[i]['name'];
		document.getElementById('tblSizes').rows[i+1].cells[1].childNodes[0].rows[0].cells[0].childNodes[0].value=arrPrintName[i]['intSize_width'];
		document.getElementById('tblSizes').rows[i+1].cells[1].childNodes[0].rows[0].cells[2].childNodes[0].value=arrPrintName[i]['intSize_height'];
		document.getElementById('tblSizes').rows[i+1].cells[2].childNodes[0].value = arrPrintName[i]['intPart'];
						
					}
					
					///////////// set iframe files src ////////////////////
					
					
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmSampleInfomations #butNew').die('click').live('click',function(){
		document.location.href = '?q=369';
		//loadCombo_frmSampleInfomations();
		$('#frmSampleInfomations #txtCode').focus();
	});
    $('#frmSampleInfomations #butDelete').die('click').live('click',function(){
		if($('#frmSampleInfomations #cboSearch').val()=='')
		{
			$('#frmSampleInfomations #butDelete').validationEngine('showPrompt', 'Please select sampleInfomations.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmSampleInfomations #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basepath+"sampleInfomations-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmSampleInfomations #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmSampleInfomations #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmSampleInfomations').get(0).reset();
													//loadCombo_frmSampleInfomations();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
	
	$('.chkTechnique').die('click').live('click',function(){
		
		var val = $(this).is(':checked');
		var value = $(this).closest('tr').attr('id');
		var text = $(this).closest('tr').find('td').eq(1).text();
		if(val)
			$('#cboTechnique').append(new Option(text,value));
		else
			$("#cboTechnique option[value='"+value+"']").remove();
			
		
		$("#cboTechnique").sort_select_box();
		
		//add technique to all select boxes////
		$('#tblMain .cboTechnique2').each(function(){
			var val = $(this).val();
			$(this).html($("#cboTechnique").html());
			$(this).val(val);
		});
		///////////////////////////////////////////////////////////
	});
	
	$('#butInsertCol').die('click').live('click',function(){
		
		clickEventForAddColumn();
		$('#tblSizes >tbody >tr:last').after('<tr>'+$('#tblSizes >tbody >tr:last').html()+'</tr>');
		
		var nextCellId = parseInt($('#tblMain .dataRow').find('td').length)-5;
		$('#tblSizes >tbody >tr:last').find('td:eq(0)').html('Print '+nextCellId);
		//alert(1);
		
		inc(basepath+'sampleInfomations-js.js');
		inc('libraries/javascript/script.js');
		inc('libraries/validate/jquery-1.js');
		inc('libraries/validate/jquery_002.js');
		inc('libraries/validate/jquery.js');
		$('#butInsertCol').unbind('click');
		$('#butInsertRow').unbind('click');

	});
	



$('#butInsertRow').die('click').live('click',function(){
		
	var rowCount = document.getElementById('tblMain').rows.length;
	//var cellCount = document.getElementById('tblMain').rows[0].cells.length;
	document.getElementById('tblMain').insertRow(rowCount-1);
	document.getElementById('tblMain').insertRow(rowCount-1);
	rowCount = document.getElementById('tblMain').rows.length;
	
	document.getElementById('tblMain').rows[rowCount-3].innerHTML = document.getElementById('tblMain').rows[rowCount-5].innerHTML;
	
	document.getElementById('tblMain').rows[rowCount-2].innerHTML = document.getElementById('tblMain').rows[rowCount-4].innerHTML;

	//document.getElementById('tblMain').rows[rowCount-3].cells[1].childNodes[0].nodeValue = '';
	var cellCount = document.getElementById('tblMain').rows[rowCount-3].cells.length;
	
	document.getElementById('tblMain').rows[rowCount-3].cells[cellCount-1].innerHTML = '';
	/*for(var i=0;i<rowCount;i++)
	{
		document.getElementById('tblMain').rows[i].insertCell(cellCount-1);	
		document.getElementById('tblMain').rows[i].cells[cellCount-1].innerHTML  = document.getElementById('tblMain').rows[i].cells[cellCount-2].innerHTML
		document.getElementById('tblMain').rows[i].cells[cellCount-1].bgColor 		= "#FFFFFF";
		document.getElementById('tblMain').rows[i].cells[cellCount-1].className 		= "printName";
	}*/
		//var html = "<tr>abc</tr>";
		//$('#tblMain >tbody >tr:last').before('<tr>'+$('#tblMain >tbody >tr:gt(1)').html()+'</tr>');
		
		//setButtonEvents();
	});

$('#butAddRow').die('click').live('click',function(){
		
	var rowCount = document.getElementById('tblGrid5').rows.length;
       // alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[0].cells.length;
	document.getElementById('tblGrid5').insertRow(rowCount);
	//document.getElementById('dd').insertRow(rowCount-1);
	//rowCount = document.getElementById('dd').rows.length;
	
	document.getElementById('tblGrid5').rows[rowCount].innerHTML = document.getElementById('tblGrid5').rows[rowCount-1].innerHTML;
   
       var cellCount = document.getElementById('tblGrid5').rows[rowCount].cells.length;
       // alert(cellCount-4);
      // alert(  document.getElementById('tblGrid5').rows[rowCount].cells[cellCount-3].innerHTML);
        var obj =document.getElementById('tblGrid5').rows[rowCount] ;
	
    ($(obj).find('#cboMainCategory').val('')) ;
    ($(obj).find('#cboSubCategory').html(''));
    ($(obj).find('#cboItem').html(''));
    ($(obj).find('#uom').val(''));
    ($(obj).find('#txtNonDirRmConsm').val(''));
   
    
     
    ($(obj).find('#cboMainCategory').attr("disabled", false)) ;
    ($(obj).find('#cboSubCategory').attr("disabled", false)) ;
    ($(obj).find('#cboItem').attr("disabled", false)) ;
    ($(obj).find('#uom').attr("disabled", false)) ;
    ($(obj).find('#txtNonDirRmConsm').attr("disabled", false)) ;
   
    
	//document.getElementById('dd').rows[rowCount-2].innerHTML = document.getElementById('dd').rows[rowCount-4].innerHTML;

	//document.getElementById('tblMain').rows[rowCount-3].cells[1].childNodes[0].nodeValue = '';
	//var cellCount = document.getElementById('dd').rows[rowCount-3].cells.length;
	
	//document.getElementById('dd').rows[rowCount-3].cells[cellCount-1].innerHTML = '';
	/*for(var i=0;i<rowCount;i++)
	{
		document.getElementById('tblMain').rows[i].insertCell(cellCount-1);	
		document.getElementById('tblMain').rows[i].cells[cellCount-1].innerHTML  = document.getElementById('tblMain').rows[i].cells[cellCount-2].innerHTML
		document.getElementById('tblMain').rows[i].cells[cellCount-1].bgColor 		= "#FFFFFF";
		document.getElementById('tblMain').rows[i].cells[cellCount-1].className 		= "printName";
	}*/
		//var html = "<tr>abc</tr>";
		//$('#tblMain >tbody >tr:last').before('<tr>'+$('#tblMain >tbody >tr:gt(1)').html()+'</tr>');
		
		//setButtonEvents();
	});


	$('#frmSampleInfomations #cboSampleNo').die('change').live('change',function(){
		
		loadRevisionNo();
		
		var intSampleNo = $('#cboSampleNo').val();
		//$('#frmSampleInfomations').get(0).reset();
		$('#cboSampleNo').val(intSampleNo);
		$('##divPicture').html('');
		return;
	});
/*	$('#cboRevisionNo').change(function(){
		loadMainDetails();
	});*/
});

				function	updateTechniqueCombo()
				{
					var i=0;
			
	arr = [];
	$('.chkTechnique').each(function(){
		if($(this).is(':checked'))
		{
			arr[i++]=$(this).parent().parent().attr('id');
		}
	});

			
			$('.chkTechnique').each(function(){
				var x =$.inArray($(this).parent().parent().attr('id'),arr);
				//alert(x);
				if(x!=-1)
				{
					$(this).prop('checked',true);
					//////////////////////////// add options to combo /////////
					var value = $(this).closest('tr').attr('id');
					var text = $(this).closest('tr').find('td').eq(1).text();
					$('#cboTechnique').append(new Option(text,value));
				}	
			
			$("#frmSampleInfomations #cboTechnique").sort_select_box();
		//add technique to all select boxes////
			$('#tblMain .cboTechnique2').each(function(){
				var val = $(this).val();
				$(this).html($("#cboTechnique").html());
				$(this).val(val);
			});
			
			
	});
        
        
        
					
				}

function saveImage(sampleNo,sampleYear,revisionNo,printCount)
{
	var x=0;
	$('#tblMain >tbody >tr:eq(0) >td').not(':last').not(':first').each(function(){
		//alert(1);
		var img =$(this).find('img').attr('src');
		//alert(img);
		var newImage = 1;
		if($(this).find('img').attr('id')=='saveimg')
		{
			newImage=0;
		}
		var ajax = new XMLHttpRequest();
		ajax.open("POST",basepath+'saveImage.php?sampleNo='+sampleNo+'&sampleYear='+sampleYear+'&revisionNo='+revisionNo+'&newImage='+newImage+'&printId='+(++x));		//ajax.count = x;
		
		ajax.onreadystatechange=function()
		  {
		  if (ajax.readyState==4 && ajax.status==200)
			{
				printCount--;
				if(printCount<=0)
					{
					hideWaiting();
					document.location.href = '?q=369&sampleNo='+sampleNo+'&sampleYear='+sampleYear+'&revNo='+revisionNo;
					}
			}
		  };
		ajax.setRequestHeader('Content-Type', 'application/upload');
		ajax.send(img);
	});
}
/*function loadCombo_frmSampleInfomations()
{
	var url 	= "sampleInfomations-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmSampleInfomations #cboSearch').html(httpobj.responseText);
}*/
function loadDetails(no)
{
    
    
}
function loadSampleNo(sampleYear)
{
	var url 	= basepath+"sampleInfomations-db-get.php?requestType=loadSampleNoCombo&sampleYear="+sampleYear;
	var httpobj = $.ajax({url:url,async:false})
	$('#frmSampleInfomations #cboSampleNo').html(httpobj.responseText);
}

function alertx(sampleNo,sampleYear,revisionNo,printCount)
{
	//alert(sampleNo);
	hideWaiting();
	$('#frmSampleInfomations #butSave').validationEngine('hide')	;
	document.location.href = '?q=369&sampleNo='+sampleNo+'&sampleYear='+sampleYear+'&revNo='+revisionNo;
}
function alertDelete()
{
	$('#frmSampleInfomations #butDelete').validationEngine('hide')	;
}

function loadMain()
{
	$("#butLoadTechnique").die('click').live('click',function(){
		popupWindow('1');
	});
}

function loadPrintMode()
{
	$("#butPrintMode").die('click').live('click',function(){
		popupWindow2('2','#cboSearch','.printMode','#frmSampleInfomations');
	});
}

function loadWasingCondistion()
{
	$("#butWashingStanderd").die('click').live('click',function(){
		popupWindow2('3','#cboSearch','.washStanderd','#frmSampleInfomations');
	});
}

function loadColors()

{
	$("#butColor").die('click').live('click',function(){
		popupWindow2('4','#cboSearch','.colors','#frmSampleInfomations');
	});	
}

function loadPart()
{
	$("#butLoadPart").die('click').live('click',function(){
		popupWindow2('5','#cboSearch','.part','#frmSampleInfomations');
	});		
}

function loadTypeOfPrint()
{
	
	$("#butTypeOfPrint").die('click').live('click',function(){
		popupWindow('6');
	});
}

function loadNewColors()
{
		var m = $("#iframeMain"+x).contents().find(cboFrom).html();
		//alert(m);
		$(pageTo+" "+cboTo).html(m);	
}

function closePopUp()
{
	
/*	var i=0;
/*	$('#tblTechnique tbody tr').each(function(){
		//alert($(this).attr('id'));
		var t = $(this).find('td').eq(0).eq(0).attr('checked');
		alert(t);
	});
	
	arr = [];
	$('.chkTechnique').each(function(){
		if($(this).attr('checked'))
		{
			arr[i++]=$(this).parent().parent().attr('id');
		}
	});
	$("#tblTechnique").load('table-technique.php',function(){
		$('#cboTechnique').html('');
		$('#cboTechnique').append(new Option('',''));
			$('.chkTechnique').bind('click',chkTechniqueClick);
			
			$('.chkTechnique').each(function(){
				var x =$.inArray($(this).parent().parent().attr('id'),arr);
				//alert(x);
				if(x!=-1)
				{
					$(this).attr('checked',true);
					//////////////////////////// add options to combo /////////
					var value = $(this).closest('tr').attr('id');
					var text = $(this).closest('tr').find('td').eq(1).text();
					$('#cboTechnique').append(new Option(text,value));
				}	
			});
			
			$("#cboTechnique").sort_select_box();
		//add technique to all select boxes////
			$('#tblMain .cboTechnique2').each(function(){
				var val = $(this).val();
				$(this).html($("#cboTechnique").html());
				$(this).val(val);
			});
			
			
	});
	
	$("#tblTypeOfPrint").load('table-typeofprint.php');*/
}

function chkTechniqueClick(){
	var val = $(this).attr('checked');
	var value = $(this).closest('tr').attr('id');
	var text = $(this).closest('tr').find('td').eq(1).text();
	if(val)
		$('#cboTechnique').append(new Option(text,value));
	else
		$("#cboTechnique option[value='"+value+"']").remove();
		
	
	$("#cboTechnique").sort_select_box();
	
	//add technique to all select boxes////
	$('#tblMain .cboTechnique2').each(function(){
		var val = $(this).val();
		$(this).html($("#cboTechnique").html());
		$(this).val(val);
	});
}

function clickEventForAddColumn()
{
	var rowCount 	= document.getElementById('tblMain').rows.length;
	var cellCount 	= document.getElementById('tblMain').rows[0].cells.length;
	
	for(var i=0;i<rowCount;i++)
	{
		document.getElementById('tblMain').rows[i].insertCell(cellCount-1);	
		document.getElementById('tblMain').rows[i].cells[cellCount-1].innerHTML  	= document.getElementById('tblMain').rows[i].cells[cellCount-2].innerHTML
		document.getElementById('tblMain').rows[i].cells[cellCount-1].align 		= "center";
		document.getElementById('tblMain').rows[i].cells[cellCount-1].bgColor 		= "#FFFFFF";
	}
	
	cellCount = document.getElementById('tblMain').rows[0].cells.length;
	
	var text1 = document.getElementById('tblMain').rows[1].cells[cellCount-3].innerHTML;
	document.getElementById('tblMain').rows[1].cells[cellCount-2].innerHTML 				= text1;
	document.getElementById('tblMain').rows[1].cells[cellCount-2].childNodes[0].innerHTML 	= "Print "+(cellCount-2);
	document.getElementById('tblMain').rows[1].cells[cellCount-2].className 				= "printName";
}

function loadRevisionNo()
{
	var url = basepath+"sampleInfomations-db-get.php?requestType=loadRevisionNo&sampleNo="+$('#cboSampleNo').val()+"&sampleYear="+$('#cboYear').val();
	var obj = $.ajax({url:url,async:false});	
	$('#cboRevisionNo').html(obj.responseText);
}

function loadGridCheck(arr,chkClass)
{
	var id = "";
	$('.'+chkClass).removeAttr('checked');	
	
	if(arr!=null)
	{
		for(var i=0;i<=arr.length-1;i++)
		{
			$('#'+chkClass+arr[i]).click();				
		}
	}
}

function clearRows()
{
	var rowCount = $('#tblMain >tbody >tr').length;
	for(var i=3;i<rowCount-1;i++)
	{
		document.getElementById('tblMain').deleteRow(i);
	}
	
	var rowCount1 = $('#tblSizes >tbody >tr').length;
	for(var i=1;i<rowCount1;i++)
	{
		document.getElementById('tblSizes').deleteRow(1);
	}
}

function clearColumns()
{
	var columLength = document.getElementById('tblMain').rows[0].cells.length;
	for(var i=5;i<columLength-1;i++)
	{
		$('#tblMain >tbody >tr').find('td:not(:last):eq('+5+')').remove();
	}
}

function addTypeOfPrintToGrid(rowId,cellId)
{
	$('#tblTypeOfPrint .chkTypeOfPrint:checked').each(function(){
		var tblType	= document.getElementById('tblMain').rows[rowId].cells[cellId].childNodes[0];
		var rows 	= document.getElementById('tblTypeOfPrint').rows.length;	
		var id 		= $(this).val();
		var name 	= $(this).parent().parent().find('td').eq(1).html();
		var y 		= tblType.rows.length;
		var found 	= false;
		
		for(var i=1;i<y;i++)
		{
			if(tblType.rows[i].cells[0].id==id)
			{
				found = true;break;
			}
		}
		
		if(!found)
		{
			tblType.insertRow(y);
			tblType.rows[y].innerHTML = "<td id=\""+id+"\" class=\"normalfnt\" bgcolor=\"#ffffffff\"><img class=\"mouseover removeRow\" src=\"images/del.png\"/>"+name+"</td><td bgcolor=\"#ffffffff\">"+techniqueTableScript+"</td>";
		}
	});
}

function removeRow(obj)
{
	obj.parentNode.parentNode.parentNode.deleteRow(obj.parentNode.parentNode.rowIndex)	
}

function removeRow2(obj)
{
	var count 	= obj.parentNode.parentNode.parentNode.rows.length;
	var id 		= obj.parentNode.parentNode.rowIndex;

	if(count>5)
	{
		$('#tblMain >tbody >tr:eq('+id+')').remove();
		$('#tblMain >tbody >tr:eq('+id+')').remove();
	}
}

function setToTypeButton(obj)
{
	var rowId 	= obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
	var cellId 	= obj.parentNode.parentNode.parentNode.parentNode.parentNode.cellIndex;
	popupWindow3('1');
	
	$('#popupContact1').load(basepath+'typeOfPrint.php',function() {		
		$('#frmTypeOfPrintPopUp #butAdd').die('click').live('click',function() {
			addTypeOfPrintToGrid(rowId,cellId);
			disablePopup();
		});
		
		$('#frmTypeOfPrintPopUp #butClose1').die('click').live('click',disablePopup);		
	});	
}

function setToColorGrid(obj)
{
	var rowId 	=  obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
	var cellId 	= obj.parentNode.parentNode.parentNode.parentNode.parentNode.cellIndex;
	popupWindow3('1');
	
	$('#popupContact1').load(basepath+'colors.php',function(){
		
		$('#frmColors #butAdd').die('click').live('click',function(){
 			addColorstoGrid(rowId,cellId);
			disablePopup();
		});
		
		$('#frmColors #butClose1').die('click').live('click',disablePopup);
		
	});		
}

function addProcess(obj)
{
	var rowId 	=  obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
	var cellId 	= obj.parentNode.parentNode.parentNode.parentNode.parentNode.cellIndex;
	popupWindow3('1');
	

	var sampNo	=	$('#cboSampleNo').val();
	var sampYear=   $('#cboYear').val();
	var rev		=   $('#cboRevisionNo').val();
	var	combo	=   $(obj).parent().parent().find('.combo').html();
	var	printId	=   $(obj).parent().parent().find('.print').html();
	
	
	$('#popupContact1').load(basepath+'processes.php?sampNo='+sampNo+'&sampYear='+sampYear+'&rev='+rev+'&combo='+URLEncode(combo)+'&print='+URLEncode(printId),function(){
		
		$('#frmProcesses #butAdd').die('click').live('click',function(){
 			saveProcess(sampNo,sampYear,rev,combo,printId);
		});
		
		$('#frmProcesses .chkRouting').die('click').live('click',function(){
			if($(this).is(':checked')){
 				var routing_id	= $(this).val();
				$('#frmProcesses #process'+routing_id).show();
			}
			else{
 				var routing_id	= $(this).val();
				$('#frmProcesses #process'+routing_id).hide();
			}
		});
	
		$('#frmProcesses #butClose1').die('click').live('click',disablePopup);
		
	});		
}

//
function setToCopy()
{
	popupWindow3('1');
	var x = $(this).parent().parent().index();
	var y = $(this).parent().index();
	$('#popupContact1').load(basepath+'copyPopup.php?sampleNo='+$('#cboSampleNo').val()+'&sampleYear='+$('#cboYear').val()+'&revNo='+$('#cboRevisionNo').val()+'&rowId='+x+'&cellId='+y,function(){
		
		$('#frmCopy #butAdd').click(function(){
			if ($('#frmCopy').validationEngine('validate'))   
			{ 			
				var printName = $('#frmCopy #cboPrintName').val();
				var comboName = $('#frmCopy #cboCombo').val();
				var columnId  = '';
				var rowId	  = '';	
				$('.printNameSpan').each(function(){
					if(($(this).html())==printName)
					{
						columnId = $(this).parent().index();
					}
				});
				
				$('.comboName').each(function(){
					if(($(this).val())==comboName)
					{
						rowId = $(this).parent().parent().index();
					}
				});
				
				var x = parseInt($('#frmCopy #txtRowId').val())+1;
				var y = parseInt($('#frmCopy #txtCellId').val());

				$('#tblMain >tbody >tr:eq('+x+') >td:eq('+y+')').find('#tblGrid2 >tbody >tr:not(:first)').each(function(){
					var colorId = $(this).find('td:eq(0)').attr('id');///////////////// click loop // to
					var isFound = false;
					var gHtml = '';
					var objFrom = '';
					$('#tblMain >tbody >tr:eq('+(rowId+1)+') >td:eq('+(columnId)+')').find('#tblGrid2 >tbody >tr:not(:first)').each(function(){
						if($(this).find('td:eq(0)').attr('id')==colorId) // from
						{
							isFound = true;
							gHtml 	= $(this).find('td:eq(5)').html();
							objFrom = $(this);
						}
					});
					if(isFound)
					{
						$(this).find('td:eq(5)').html(gHtml);
						objTo  = $(this);
						objFrom.find('#tblGrid3 >tbody >tr:not(:last)').each(function(){
							var rowIndex 	= $(this).index();
							var inkType1 	= $(this).find('#cboTechnique').val();
							var shots1 		= $(this).find('#txtShots').val();
							var item1 		= $(this).find('#cboItem').val();
							var weight1 	= $(this).find('#txtWeight').val();
							var objTr2 		= objTo.find('td:eq(5)').find('#tblGrid3 >tbody >tr:eq('+rowIndex+')');
								objTr2.find('#cboTechnique').val(inkType1);
								objTr2.find('#txtShots').val(shots1);
								objTr2.find('#cboItem').val(item1);
								objTr2.find('#txtWeight').val(inkType1);
						});
					}
				});

				disablePopup();
			}
		});
		
		$('#frmCopy #butClose1').die('click').live('click',disablePopup);
		
	});		
}

function addColorstoGrid(rowId,cellId)
{
		$('#tblColors .chkColors:checked').each(function(){
		var tblType	= document.getElementById('tblMain').rows[rowId].cells[cellId].childNodes[0];
		var rows 	= document.getElementById('tblColors').rows.length;
		var id 		= $(this).val();
		var name 	= $(this).parent().parent().find('td').eq(1).html();
		var y 		= tblType.rows.length;
		var found 	= false;
		
		for(var i=1;i<y;i++)
		{
			if(tblType.rows[i].cells[0].id==id)
			{
				found = true;break;
			}
		}
		
		if(!found)
		{
			var techniqueHtml = $('#cboTechniqueDumy').html();
			var spItem = $('#cboItemDumySPRM').html();
			tblType.insertRow(y);
			var html = "<td bgcolor=\"#ffffffff\" class=\"normalfnt\" id=\""+id+"\"><img src=\"images/del.png\" class=\"mouseover rmvParent\" />"+name+"</td>"+
                  "<td bgcolor=\"#ffffffff\" class=\"normalfnt\" id=\"\"><select id=\"cboTechnique\" name=\"cboTechnique\" class=\"cboTechnique cboTech\" style=\"width:100px\">"+techniqueHtml +
                  "</select><div class=\"colType\" style=\"display:none\">1</div><div class=\"colSaved\" style=\"display:none\">0</div><div class=\"template_tech_item\" style=\"display:none\"><select id=\"cboItem\" name=\"select5\" class=\"cboItem\" style=\"width:100px\"><option value=\"\"></option></select></div></td>"+
                  "<td bgcolor=\"#ffffffff\" class=\"cls_tech_item_td\"  valign=\"top\" >"+'<table id="tblGridTI" class="tblGridTI" width="100%" cellspacing="1" cellpadding="0" border="0" bgcolor="#999999"><tbody><tr><td bgcolor="#FFFFFF"><img class="mouseover removeTechItmRow" src="images/del.png"></td><td><div class="technical_item_flag" style=\"display:none\">1</div><select id="cboItem" class="cboItem techItem" name="select5" style="width:100px"></td></tr><tr><td colspan="5" width="100%" bgcolor="#FFFFFF"><img id="butAddNewTechniqueItem" class="mouseover butAddNewTechniqueItem" src="images/add.png" width="16" height="16"></td></tr></tbody></table>'+"</td>"+
                  "<td bgcolor=\"#ffffffff\" valign=\"top\"><table id=\"tblGridTIQ\" class=\"tblGridTIQ\" width=\"100%\" cellspacing=\"1\" cellpadding=\"0\" border=\"0\" bgcolor=\"#999999\"><tbody><tr><td><input id=\"txtQty\" type=\"text\" name=\"textfield5\" value=\"\" style=\"width:30px\" ><div class=\"parentItem\" style=\"display:none\"></div></td></tr></tbody></table></td>"+
                  "<td bgcolor=\"#ffffffff\" class=\"normalfnt\" valign=\"top\"><table id=\"tblGridTIWH\" class=\"tblGridTIWH\" width=\"100%\" cellspacing=\"1\" cellpadding=\"0\" border=\"0\" bgcolor=\"#999999\"><tbody><tr><td><div class=\"parentItem\" style=\"display:none\"></div><input id=\"txtSizeW\" type=\"text\" name=\"textfield3\" style=\"width:30px\" value=\"\" >W<input id=\"txtSizeH\" type=\"text\" name=\"textfield4\" value=\"\" style=\"width:30px\" >H</td></tr></tbody></table></td>"+
                  "<td colspan=\"3\" bgcolor=\"#ffffffff\">"+
				  "<table id=\"tblGrid3\" class=\"tblGrid3\" width=\"100%\" "+
				  " cellspacing=\"1\" cellpadding=\"0\" border=\"0\" bgcolor=\"#999999\"><tbody><tr><td width=\"100%\" "+
				  " bgcolor=\"#FFFFFF\" colspan=\"5\"> "+
				  "<img id=\"butAddNewTechnique\" class=\"mouseover butAddNewTechnique\" width=\"16\" "+
				  " height=\"16\" src=\"images/add.png\"></td></tr></tbody></table></td>";
			tblType.rows[y].innerHTML = html;
		}
	});
}


function saveProcess(sampNo,sampYear,rev,combo,printId)
{
	//routing array
		var err			  =0;
		var arrRouting    ='';
		$('#tblRouting .chkRouting:checked').each(function(){
			
			var id 		= $(this).val();
			var ups 		= $(this).parent().parent().find('.ups').val();
			if(ups=='' || ups==0){
				err	=1;
			}
			arrRouting +='{'+

				'"sampNo":"'+(sampNo)+'",'+
				'"sampYear":"'+(sampYear)+'",'+
				'"rev":"'+(rev)+'",'+
				'"combo":"'+URLEncode(combo)+'",'+
				'"print":"'+URLEncode(printId)+'",'+
				'"id":"'+(id)+'",'+
				'"ups":"'+ups+'"'+
				'},';
  		
 		});
		if(err	== 1){
			alert("please enter number of ups");
			return false;
		}
		
		
		var data = "requestType=save_process";
			data += "&sampleNo="+sampNo+"&sampleYear="+sampYear+"&revisionNo="+rev+"&combo="+URLEncode(combo)+"&printId="+URLEncode(printId);

		if(arrRouting!='')
		{
		arrRouting = arrRouting.substr(0,arrRouting.length-1);
		data+="&arrRouting=["+    arrRouting+']'; 
		}
	
	//process array-----------------------------------------------------------------
		var arrProcess    ='';
		var err_or		  =0;
		
		$('#tblRouting .chkRouting:checked').each(function(){
			var routing_id		= $(this).val();
			var row_routing_id	= '#process'+routing_id;

			$(row_routing_id+' #tblProcess .chkProcess:checked').each(function(){
				
				var id 		= $(this).val();
				var order	= $(this).parent().parent().find('.cls_order').val();
				if(order==''){
					err_or	=1;
				}
				
				arrProcess +='{'+
	
					'"sampNo":"'+(sampNo)+'",'+
					'"sampYear":"'+(sampYear)+'",'+
					'"rev":"'+(rev)+'",'+
					'"combo":"'+URLEncode(combo)+'",'+
					'"print":"'+URLEncode(printId)+'",'+
					'"routing_id":"'+routing_id+'",'+
					'"id":"'+id+'",'+
					'"order":"'+(order)+'"'+ 
					'},';
			
			});
		});
		
		if(err_or	== 1){
			alert("please enter process order");
			return false;
		}
		
	
		if(arrProcess!='')
		{
		arrProcess = arrProcess.substr(0,arrProcess.length-1);
		data+="&arrProcess=["+    arrProcess+']'; 
		}
		
		var url = basepath+"sampleInfomations-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json", 
			type:'POST', 
			data:data, 
			async:false,
			
			success:function(json){
					$('#frmProcesses #butAdd').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					window.checkStaus =json.status;
					if(json.type=='pass')
					{
						var x=0;
						return;
					}
					else
						hideWaiting();
				},
			error:function(xhr,status){
					hideWaiting();
				}		
			});
		
 }


function setToTechnique(obj)
{
	var  typeRowId	= obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
	var  mainRowId	= obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
	var cellId 		= obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.cellIndex;
	popupWindow3('1');
	
	$('#popupContact1').load(basepath+'technique.php',function(){
		
		$('#frmTechnique #butAdd').die('click').live('click',function(){
			addToTechniqueGrid(mainRowId,typeRowId,cellId);
			disablePopup();
		});
		
		$('#frmTechnique #butClose1').die('click').live('click',disablePopup);		
	});		
}

function addToTechniqueGrid(mainRowId,typeRowId,cellId)
{
	$('#tblTechnique .chkTechnique:checked').each(function(){
		var tblTech= document.getElementById('tblMain').rows[mainRowId].cells[cellId].childNodes[0].rows[typeRowId].cells[1].childNodes[0];
		var id 		= $(this).val();
		var name 	= $(this).parent().parent().find('td').eq(1).html();
		var typeId 	= $(this).parent().parent().find('td').eq(1).attr('id');		
		var y 		= tblTech.rows.length;
		var found 	= false;
		for(var i=1;i<y;i++)
		{
			if(tblTech.rows[i].cells[0].id==id)
			{
				found = true;break;
			}
		}
		if(!found)
		{
			tblTech.insertRow(y);
			tblTech.rows[y].innerHTML = "<td class=\"normalfnt\" id=\""+id+"\"><img class=\"mouseover removeRow\" src=\"images/del.png\"/>"+name+"</td>"+
					"<td class=\"normalfnt\"><input class=\"qty\"  id=\"txtQty\" style=\"width:30px\" /></td>"+
                    "<td><select class=\"cboItem\" style=\"width:150px\"></select></td>";
		}
		
		if(typeId!=3)
		{
			tblTech.rows[y].cells[1].childNodes[0].disabled = true;//cboItemDumySPRM
			tblTech.rows[y].cells[2].childNodes[0].innerHTML = document.getElementById('cboItemDumy').innerHTML;
		}
		else
		{
			tblTech.rows[y].cells[2].childNodes[0].innerHTML = document.getElementById('cboItemDumySPRM').innerHTML;	
		}
	});
}

function addNewTechniqueRow()
{
	$(this).parent().parent().before(techniqueTableScript);
	var html 		= $('#cboTechniqueDumy2').html();
	var html2 		= $('#cboItemDumy2').html();
        
        
        
	var rowIndex 	= $(this).parent().parent().index();
	$(this).parent().parent().parent().find('tr:eq('+(rowIndex-1)+') #cboTechnique').html(html);
	$(this).parent().parent().parent().find('tr:eq('+(rowIndex-1)+') #cboItem').html(html2);
	
	$(".removeRow").unbind('click');
	$(".removeRow").die('click').live('click',function(){
			removeRow(this);
	});
}

function setButtonEvents()
{
	$(".butTypeOfPrint").unbind('click');
	$(".butTypeOfPrint").die('click').live('click',function(){
			setToTypeButton(this);
	});
	$(".butColors").unbind('click');
	$(".butColors").die('click').live('click',function(){
		setToColorGrid(this);
	});
	$(".butCopy").unbind('click');
	$(".butCopy").die('click').live('click',setToCopy);
	
	$(".removeRow").unbind('click');
	$(".removeRow").die('click').live('click',function(){
			removeRow(this);
	});
	
	$(".removeRow2").unbind('click');
	$(".removeRow2").die('click').live('click',function(){
			removeRow2(this);
	});
	
	$(".butColors").unbind('click');
	$(".butColors").die('click').live('click',function(){
		setToColorGrid(this);
	});
	
	$("#butTechnique").unbind('click');
	$("#butTechnique").die('click').live('click',function(){
			setToTechnique(this);
	});
	
	$('.butAddNewTechnique').unbind('click');
	$(".butAddNewTechnique").die('click').live('click',addNewTechniqueRow);
}

function loadSampleNos()
{
	var url = basepath+"sampleInfomations-db-get.php?requestType=loadSampleNo&styleNo="+URLEncode($('#cboStyleNo').val())+"&sampleYear="+$('#cboYear').val();
	var obj = $.ajax({url:url,async:false});	
	$('#cboSampleNo').html(obj.responseText);	
}
function loadSubCategory(mainCategory,obj)
{

        var url 	= basepath+"sampleInfomations-db-get.php?requestType=loadsubcategoty&maincategory="+mainCategory;
	var httpobj = $.ajax({url:url,async:false})
	$(obj).parent().parent().find('#cboSubCategory').html(httpobj.responseText);
    
}

function loadItem(subCategory,obj)
{
   //alert(hh);
        var url 	= basepath+"sampleInfomations-db-get.php?requestType=loaditem&subcategory="+subCategory;
	var httpobj = $.ajax({url:url,async:false})
	$(obj).parent().parent().find('#cboItem').html(httpobj.responseText);
    
}function loaduom(item,obj)
{
   //alert(hh);
       var url 	= basepath+"sampleInfomations-db-get.php?requestType=loaduom&item="+item;
	var httpobj = $.ajax({url:url,async:false})
	$(obj).parent().parent().find('#uom').val(httpobj.responseText);
    
}

function remove(obj,type)

{
    
    var rowCount 	= document.getElementById('tblGrid5').rows.length;
	 sampleNo=$('#cboSampleNo').val();
	 sampleYear=$('#cboYear').val();
	 revisionNo=$('#cboRevisionNo').val();
     item=$(obj).parent().parent().find('#cboItem').val();
  //  alert(rowCount);
   
   //alert("hdhdhd");
  
         // obj.parentNode.parentNode.parentNode.deleteRow(obj.parentNode.parentNode.rowIndex);
       //  var item        =$("#cboItem").val();
         var val = $.prompt('Are you sure you want to delete?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{  // alert($(obj).parent().parent().find()('#cboItem').val(httpobj.responseText));
										var url = basepath+"sampleInfomations-db-set.php";
                                                                               // alert('ok');
                                                                              //  alert($(obj).parent().parent().find('#cboItem').val());
										var httpobj = $.ajax({
											url:url,
											//dataType:'json',
											data:'requestType=deleteData&sampleNo='+sampleNo+'&sampleYear='+sampleYear+'&revisionNo='+revisionNo+'&item='+item,
											async:false,
											success:function(data)
                                                                                        {
                                                                                            //$(obj).validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												//alert("lll");
                                                                                               // return false;
                                                                                                
                                                                                                if(rowCount!=2)
                                                                                             {
                                                                                              //   $(this).validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                                                                                               obj.parentNode.parentNode.parentNode.deleteRow(obj.parentNode.parentNode.rowIndex);
                                                                                               alert("deleted successfully");
                                                                                                //document.getElementById('deltData').html();
                                                                                             }
                                                                                             
                                                                                             if(rowCount==2)
                                                                                             {
                                                                                               //  $(obj).validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                                                                                               obj.parentNode.parentNode.parentNode.deleteRow(obj.parentNode.parentNode.rowIndex);
                                                                                                 alert("deleted successfully");
                                                                                                //  document.getElementById("tblGrid5").innerHTML =  document.getElementById("tblGrid5").innerHTML+document.getElementById("deltData").innerHTML;
                                                                                            //$(obj).parent().parent().find('#tblGrid5').innerHTML =  $(obj).parent().parent().find('#tblGrid5').innerHTML+ $(obj).parent().parent().find('#deltData').innerHTML;
                                                                                         // alert( $('#tblGrid5').html() );
                                                                                        //  alert( $('#tbl_div').html() );
                                                                                         $('#tblGrid5').html( $('#tblGrid5').html() + $('#tbl_div').html());
                                                                                          
                                                                                               // alert( $(obj).parent().parent().parent().find('#deltData').innerHTML );
                                                                                              // alert(document.getElementById("deltData").innerHTML );
                                                                                            }
                                                                                             
												//$(this).validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                                                                                               
												
                                                                                            
											}	 
										});
									}
				}
		 	});
   
   
   
}

function removeTechSampItem(obj,type)

{
    
 	 sampleNo			=$('#cboSampleNo').val();
	 sampleYear			=$('#cboYear').val();
	 revisionNo			=$('#cboRevisionNo').val();

	var colorId 		= $(obj).parent().parent().parent().parent().parent().parent().find('td:eq(0)').attr('id')
	var flag_newColor	=$(obj).parent().parent().find('.colType').html();
	var new_col_saved	=$(obj).parent().parent().find('.colSaved').html();
	var technique  		= '';
	if(flag_newColor==1 && new_col_saved !=1)
	technique  			= $(obj).parent().parent().parent().parent().parent().parent().find('td:eq(0)').attr('id')
	else
	technique  			= $(obj).parent().parent().parent().parent().parent().parent().find('td:eq(1)').attr('id');
   var combo			=($(obj).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().find('.combo').html());
   var print			=($(obj).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().find('.print').html());
   var item				=$(obj).parent().parent().find('#cboItem').val();
	 
	 
	 
  //  alert(rowCount);
   
   //alert("hdhdhd");
  
         // obj.parentNode.parentNode.parentNode.deleteRow(obj.parentNode.parentNode.rowIndex);
       //  var item        =$("#cboItem").val();
         var val = $.prompt('Are you sure you want to delete?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{  // alert($(obj).parent().parent().find()('#cboItem').val(httpobj.responseText));
										var url = basepath+"sampleInfomations-db-set.php";
                                                                               // alert('ok');
                                                                              //  alert($(obj).parent().parent().find('#cboItem').val());
										var httpobj = $.ajax({
											url:url,
											//dataType:'json',
											data:'requestType=deleteDataTechSampple&sampleNo='+sampleNo+'&sampleYear='+sampleYear+'&revisionNo='+revisionNo+'&print='+print+'&combo='+combo+'&color='+colorId+'&technique='+technique+'&item='+item,
											async:false,
											success:function(data)
											{
												alert("deleted successfully");
												if(type==1){
													var rcds=0;
													$(obj).parent().parent().parent().parent().parent().parent().find('#tblGridTIQ >tbody >tr').each(function(){								
															var ob1=this;
															var itemC	= $(ob1).find('.parentItem').html()
															//alert(itemC+'=='+item);
															if(itemC==item && rcds==0){
																rcds++;
																$(ob1).closest('tr').remove();
															}
															
													});
													var rcds=0;
													$(obj).parent().parent().parent().parent().parent().parent().find('#tblGridTIWH >tbody >tr').each(function(){								
															var itemC	= $(this).find('.parentItem').html()
															if(itemC==item && rcds==0){
																rcds++;
																$(this).closest('tr').remove();
															}
															
													});
													$(obj).parent().parent().closest('tr').remove();
												}
											}	 
										});
									}
				}
		 	
			});
   
   
   
}

function removeRow_db(obj1)
{
  
	var sampNo		=$('#cboSampleNo').val();
	var sampYear		=$('#cboYear').val();
	var rev			=$('#cboRevisionNo').val();
	var combo		=$(obj1).parent().parent().find('.combo').html();
	var printName	=$(obj1).parent().parent().find('.print').html();
	var color		=$(obj1).parent().parent().find('td:eq(0)').attr('id');
	var technique	=$(obj1).parent().parent().find('td:eq(1)').attr('id');
 	var data		='';	
 
	data+="&sampNo="		+	sampNo;
	data+="&sampYear="	+	sampYear;
	data+="&rev="	+	rev;
	data+="&combo="		+	combo;
	data+="&printName="	+	printName;
	data+="&color="		+	color;
	data+="&technique="	+	technique;
 
  
   var url 	= basepath+"sampleInfomations-db-set.php";
	//$(obj).parent().parent().find('#uom').val(httpobj.responseText);
    
	var httpobj = $.ajax({
	url:url,
	dataType:'json',
	type:'POST',
	data:'requestType=delete_saved_colors'+data,
	async:false,
	success:function(json){
 			if(json.type=='pass')
			{
				alert(json.msg);
				removeRow(obj1);
			}
 			if(json.type=='fail')
			{
				alert(json.msg);
 			}
 		},
	error:function(xhr,status){
 			alert(json.msg);
			//function (xhr, status){errormsg(status)}
		}		
	});
 }


function copyProcess(obj)
{
  
	var sampNo	=	$('#cboSampleNo').val();
	var sampYear=   $('#cboYear').val();
	var rev		=   $('#cboRevisionNo').val();
	var	combo	=   $(obj).parent().parent().find('.combo').html();
	var	printId	=   $(obj).parent().parent().find('.print').html();
 	var data	='';
	
	data+="&sampNo="		+	sampNo;
	data+="&sampYear="	+	sampYear;
	data+="&rev="	+	rev;
	data+="&combo="		+	combo;
	data+="&printName="	+	printId;
 
  
   var url 	= basepath+"sampleInfomations-db-set.php";
	//$(obj).parent().parent().find('#uom').val(httpobj.responseText);
    
	var httpobj = $.ajax({
	url:url,
	dataType:'json',
	type:'POST',
	data:'requestType=copy_routing'+data,
	async:false,
	success:function(json){
 			if(json.type=='pass')
			{
				alert(json.msg);
			}
 			if(json.type=='fail')
			{
				alert(json.msg);
 			}
 		},
	error:function(xhr,status){
 			alert(json.msg);
			//function (xhr, status){errormsg(status)}
		}		
	});
 }

function addNewTechniqueItemRow(){
	//alert($(this).parent().parent().parent().parent().parent().parent().html());
	var curObj	=this;
var obj	= $(curObj).parent().parent().parent();
($(curObj).parent().parent().html('<td bgcolor="#FFFFFF"><img class="mouseover removeTechItmRow" src="images/del.png"></td><td>'+$(curObj).parent().parent().parent().parent().parent().parent().find('.template_tech_item').html()+'</td>'));

	 (obj.html(obj.html()+'<tr><div class="technical_item_flag">1</div><td colspan="2" width="100%" bgcolor="#FFFFFF"><img id="butAddNewTechniqueItem" class="mouseover butAddNewTechniqueItem" src="images/add.png" width="16" height="16"></td></tr>'));
	
	 (obj.parent().parent().parent().find('#tblGridTIQ >tbody').append(/*obj.parent().parent().parent().find('#tblGridTIQ tbody').html()+*/'<tr><td><input id="txtQty" style="width:30px" value="" name="textfield5" type="text"><div class="parentItem" style="display:none"></div></td></tr>'));
	
	 (obj.parent().parent().parent().find('#tblGridTIWH  >tbody').append(/*obj.parent().parent().parent().find('#tblGridTIWH >tbody').html()+*/'<tr><td nowrap="" bgcolor="#ffffffff"><div class="parentItem" style="display:none"></div><input id="txtSizeW" value="" style="width:30px" name="textfield3" type="text"> W <input  id="txtSizeH"  style="width:30px" value="" name="textfield4" type="text">H</td></tr>'));
//$(this).parent().parent().parent().html($(this).parent().parent().parent().html()+'<tr><td>nnnnn</td></tr>');
// combo alert($(this).parent().parent().parent().find('.cboItem').attr('id'));
//table id	alert($(this).parent().parent().parent().parent().attr('id'));
//alert($(this).parent().parent().parent().parent().parent().find('.cboItem').attr('id'));
}

function loadTechItems(object)
{
	var technique	= $(object).val()
 	var url 		= basepath+"sampleInfomations-db-get.php?requestType=loadTechItems&technique="+technique;
	var obj 		= $.ajax({url:url,async:false});	
	$(object).parent().parent().find('.cboItem').html(obj.responseText);
}

function setParentItemsForQtyWHGrid(object){
	//alert($(object).parent().parent().html())
	var itemId		=$(object).val();
	var rowIndex	=($(object).parent().parent().closest('tr').index());
	$(object).parent().parent().parent().parent().parent().parent().find('#tblGridTIQ >tbody >tr').each(function(){
 		var rowIndexQ	=($(this).closest('tr').index());
		//alert(rowIndexQ+'=='+rowIndex+'===='+itemId);
		if(rowIndexQ==rowIndex)
			$(this).find('.parentItem').html(itemId);
	});
	$(object).parent().parent().parent().parent().parent().parent().find('#tblGridTIWH >tbody >tr').each(function(){
		var rowIndexWH	=($(this).closest('tr').index());
		if(rowIndexWH==rowIndex)
			$(this).find('.parentItem').html(itemId);
	});
	//alert($('table').find('tr').index(object));
}



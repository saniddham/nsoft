<?php 
//ini_set("display_errors",1);//to see the error

	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];

	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 			= $_REQUEST['requestType'];
	$sampleNo 				= $_REQUEST['sampleNo'];
	$sampleYear 			= $_REQUEST['sampleYear'];
	$revisionNo 			= $_REQUEST['revisionNo'];
	$date 					= $_REQUEST['date'];
	//$deliveryDate			= $_REQUEST['deliveryDate'];
			
	$graphicNo 				= $_REQUEST['graphicNo'];
	$customerId 			= $_REQUEST['customerId'];
	$styleNo 				= $_REQUEST['styleNo'];
	$brandId 				= $_REQUEST['brandId'];
	$sampleQty 				= val($_REQUEST['sampleQty']);
	$grade 					= val($_REQUEST['grade']);
	$fabricType 			= $_REQUEST['fabricType'];
	$verivide_d65 			= ($_REQUEST['verivide_d65']=='true'?'1':'0');
	$verivide_tl85 			= ($_REQUEST['verivide_tl85']=='true'?'1':'0');
	$verivide_cw 			= ($_REQUEST['verivide_cw']=='true'?'1':'0');
	$verivide_f 			= ($_REQUEST['verivide_f']=='true'?'1':'0');
	$verivide_uv 			= ($_REQUEST['verivide_uv']=='true'?'1':'0');
	$macbeth_dl 			= ($_REQUEST['macbeth_dl']=='true'?'1':'0');
	$macbeth_cw 			= ($_REQUEST['macbeth_cw']=='true'?'1':'0');
	$macbeth_inca 			= ($_REQUEST['macbeth_inca']=='true'?'1':'0');
	$macbeth_tl84 			= ($_REQUEST['macbeth_tl84']=='true'?'1':'0');
	$macbeth_uv 			= ($_REQUEST['macbeth_uv']=='true'?'1':'0');
	$macbeth_horizon 		= ($_REQUEST['macbeth_horizon']=='true'?'1':'0');
	
	$curing_temp 			= val($_REQUEST['curing_temp']);
	$curing_speed 			= val($_REQUEST['curing_speed']);
	$press_temp 			= val($_REQUEST['press_temp']);
	
	$press_pressure 		= val($_REQUEST['press_pressure']);
	$press_time 			= val($_REQUEST['press_time']);
	$meshCount 				= $_REQUEST['meshCount'];
	$instruction 			= $_REQUEST['instruction'];
	$instructionTech		= quote($_REQUEST['instructionTech']);
	$status = 0;
	//echo $_REQUEST['arrCombo'];
	$arrCombo 		= json_decode($_REQUEST['arrCombo'], true);	
	//print_r($arrCombo);

try{

		$db->begin();
	/////////////////////////// find the stage 1st or 3rd //////////////////////////
		$sql = "SELECT
					trn_sampleinfomations.intStatus,
					trn_sampleinfomations.intTechnicalStatus,
					trn_sampleinfomations.intTechnicalApproveLevelStart,
					trn_sampleinfomations.intMarketingStatus
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations.intRevisionNo =  '$revisionNo'
				";
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$intTechnicalStatus = $row['intTechnicalStatus'];
		$intMarketingStatus = $row['intMarketingStatus'];
		$intTechnicalApproveLevelStart = $row['intTechnicalApproveLevelStart'];
		
		
	if($requestType=='save')
	{ 
		$status = $_REQUEST['checkStatus'];
        $arrNonDirRm = $_REQUEST['arrNonDirRm'];
              //echo  $arrNonDirRm;
		$db->begin();
		
		if($intMarketingStatus!=1)
		{
			$msg =  "This sample has't approved yet by maketing department.";
			$response['type'] 		= 'fail';
			$response['msg'] 		= $msg;
			throw new Exception($msg);
			//return;

		}
		if($intTechnicalStatus==1)
		{
			$msg =  "This sample is already confirmed by the technical department.You can't do changes now.";
			$response['type'] 		= 'fail';
			$response['msg'] 		= $msg;
			throw new Exception($msg);
			//return;

		}
		
		//-------------check whether each combo assigned at least one routing process
		$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strPrintName AS PRINT,
					trn_sampleinfomations_details.strComboName AS COMBO,
					trn_sampleinfomations_combo_print_details_processes.PROCESS AS PROCESS
				FROM
					trn_sampleinfomations
				INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo
				AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_details.intSampleYear
				AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_details.intRevNo
				LEFT JOIN trn_sampleinfomations_combo_print_details_processes ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_combo_print_details_processes.SAMPLE_NO
				AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_combo_print_details_processes.SAMPLE_YEAR
				AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_combo_print_details_processes.REVISION
				AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_combo_print_details_processes.PRINT
				AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_combo_print_details_processes.COMBO
				WHERE
					trn_sampleinfomations.intSampleNo = '$sampleNo'
				AND trn_sampleinfomations.intSampleYear = '$sampleYear'
				AND trn_sampleinfomations.intRevisionNo = '$revisionNo'
				";
		//echo $sql;
		$result = $db->RunQuery2($sql);
		$routingFlag=0;
		$errorMsg="";
		while ($row = mysqli_fetch_array($result)) {
			if(!($row['PROCESS']))
			{
				$errorMsg = $row['COMBO']." in ".$row['PRINT']."	".$errorMsg;
				$routingFlag=1;
			}
		}
		
		if($routingFlag==1)
		{
			$msg =  "Routing process is not assigned for ".$errorMsg;
			$response['type'] 		= 'fail';
			$response['msg'] 		= $msg;
			throw new Exception($msg);
		}
		
			$sql = "UPDATE `trn_sampleinfomations`
			 SET `dblCuringCondition_temp`='$curing_temp',`dblCuringCondition_beltSpeed`='$curing_speed',
				`dblPressCondition_temp`='$press_temp',`dblPressCondition_pressure`='$press_pressure',
				`dblPressCondition_time`='$press_time',
			 	`strMeshCount`='$meshCount',
				`strAdditionalInstructionsTech`='$instructionTech' ,
				`intTechnicalStatus`='$intTechnicalApproveLevelStart',
				 intTechUser = '$userId',dtmTechEnterDate=now()
				WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevisionNo`='$revisionNo')  ";
				$result = $db->RunQuery2($sql);
				if(!$result)
				throw new Exception('Failed to update technical data111');
	

		
					/*$arrPart 		= json_decode($_REQUEST['arrPart'], true);
					$e=1;
					$sql = "DELETE FROM `trn_sampleinfomations_printsize` WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevisionNo`='$revisionNo')";
					$result2 = $db->RunQuery($sql);
					foreach($arrPart as $arrP)
					{
						$partId 			= $arrP['partId'];
						$size_w 			= $arrP['size_w'];
						$size_h 			= $arrP['size_h'];
						$printN 			= 'print '.$e++;
						$sql = "INSERT INTO 
											`trn_sampleinfomations_printsize` 
									(`intSampleNo`,`intSampleYear`,`intRevisionNo`,`strPrintName`,`intWidth`,
										`intHeight`,`intPart`,`intCompanyId`) 
									VALUES ('$sampleNo','$sampleYear','$revisionNo','$printN','$size_w','$size_h','$partId','$companyId')";
						$result2 = $db->RunQuery($sql);
					}*/
					
		
		
		
		
			$x=0;
			$y=0;
			$z=0;
			$sql = "DELETE FROM `trn_sampleinfomations_details_technical` WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevNo`='$revisionNo') ";
			$db->RunQuery2($sql);
			$sql = "UPDATE `trn_sampleinfomations_details` SET TECHNICAL_SAVED_OR_UPDATED=0 WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevNo`='$revisionNo') ";
			$db->RunQuery2($sql);
			//$sql = "UPDATE `trn_sampleinfomations_details` SET TECHNICAL_SAVED_OR_UPDATED=1 WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevNo`='$revisionNo') and intItem is null"; //2017-05-23  (not to delete colours which has no marketing items)
			//$db->RunQuery2($sql);
		
			//echo 'pass - ';
			//print_r($arrCombo);
			$comboa	=array();
			foreach($arrCombo as $comboa)
			{
				//echo 1;
				$printMode 			= $comboa['printMode'];
				$washStanderd 		= $comboa['washStanderd'];
				$comboName			= $comboa['comboName'];
				$groundColor 		= $comboa['groundColor'];
				$printName 			= $comboa['printName'];
				$techniqueId 		= $comboa['techniqueId'];
				$fabricType 		= $comboa['fabricType'];
				
				/*$resp	= getRoutingSavedStatus($sampleNo,$sampleYear,$revisionNo,$comboName,$printName);
				if($resp['err'] ==1)
				{
					$msg =  "Please add routing for ".$comboName."/".$printName;
					$response['type'] 		= 'fail';
					$response['msg'] 		= $msg;
					throw new Exception($msg);
					//return;
		
				}*/
				
				//if($comboName=='c1' && $printName=='print 1') 
				//print_r($comboa['gridColMarketingDetails']);
				$arrColTech	=array();
				$x=0;
				$color_existing_flag=0;
				$marketng_item_existing_flag=0;
				foreach($comboa['gridColMarketingDetails'] as $arrColTech)
				{

					$colorId			= null($comboa['gridColMarketingDetails'][$x]['colorId']);
					$techId_p			= null($comboa['gridColMarketingDetails'][$x]['techId_p']);
					if($colorId)
						$color_existing_flag=1;

						$y=0; 
						//if($comboName=='c1' && $printName=='print 1' && $colorId=='11851' && $techId_p=='4') 
						//print_r($arrCombo['gridColMarketingDetails'][$x]['marketingItems']);
						foreach($comboa['gridColMarketingDetails'][$x]['marketingItems'] as $arrMarketing)
						{
							//if($comboName=='c1' && $printName=='print 1' && $colorId=='11851' && $techId_p=='4') 
							$itemId		= null($arrColTech['marketingItems'][$y]['itemId']);
							$qty		= null($arrColTech['marketingItems'][$y]['qty']);
							$w			= null($arrColTech['marketingItems'][$y]['w']);
							$h			= null($arrColTech['marketingItems'][$y]['h']);
							if($itemId>0)
								$marketng_item_existing_flag=1;
							
						//if($comboName=='c1' && $printName=='print 1' && $colorId=='5097' && $techId_p=='353') 
						//echo "/".$itemId;
							$y++;
		
							
							$exists	= checkForColourExists($sampleNo,$sampleYear,$revisionNo,$comboName,$printName,$colorId,$techId_p,$itemId);	
							
							
							if((!$exists) /*&& $flag_newColor==1*/)//insert in to sample_information_details
							{
								insertSample_dtails($sampleNo,$sampleYear,$revisionNo,$comboName,$printName,$colorId,$techId_p,$itemId,$qty,$w,$h);
							}
							else
								updateSample_dtails($sampleNo,$sampleYear,$revisionNo,$comboName,$printName,$colorId,$techId_p,$itemId,$qty,$w,$h);
							//else
								//updateSample_dtails($sampleNo,$sampleYear,$revisionNo,$comboName,$printName,$colorId,$techId_p,$item,$qty,$w,$h);
						}
						
						
						$z=0;
						$arrTechDet =array();
						//if($comboName=='c1' && $printName== 'print 1')
						foreach($arrColTech['technicalDetails'] as $arrTechDet)
						{
						//print_r($arrColTech['technicalDetails'][$z]);
							$inkType		 	 = null($arrColTech['technicalDetails'][$z]['inkType']);
							$itemId			 	 = null($arrColTech['technicalDetails'][$z]['itemId']);
							$shots				 = val($arrColTech['technicalDetails'][$z]['shots']);
							$weight			 	 = null($arrColTech['technicalDetails'][$z]['weight']);
							$z++;
						//if($flag_newColor && $exists)
						//$list	.=
	
						 $sql = "INSERT INTO `trn_sampleinfomations_details_technical` (`intSampleNo`,`intSampleYear`,`intRevNo`,`strPrintName`,`strComboName`,`intColorId`,`intInkTypeId`,`intNoOfShots`,`intItem`,`dblColorWeight`) VALUES ('$sampleNo','$sampleYear','$revisionNo','$printName','$comboName','$colorId','$inkType',$shots,$itemId,$weight)";
							//if($comboName=='c1' && $printName== 'print 1')
							//	echo $sql;
						$result2 = $db->RunQuery2($sql);
						if(!$result2)
						throw new Exception('Failed to update technical data....'.$comboName.'/'.$printName.'/'.$colorId.'/'.$inkType.'/'.$z);
						}
					
					$x++;
					
					//----------------------------------------------------
					if($color_existing_flag==1 && $marketng_item_existing_flag==0){//not to delete if no item selected as colour needs to be continued for ink type details
							$sql = "UPDATE trn_sampleinfomations_details 
									SET
									TECHNICAL_SAVED_OR_UPDATED=1 ,
									 intItem = NULL 
								  where 
									trn_sampleinfomations_details.intSampleNo = '$sampleNo' AND
									trn_sampleinfomations_details.intSampleYear = '$sampleYear' AND
									trn_sampleinfomations_details.intRevNo = '$revisionNo' AND
									trn_sampleinfomations_details.strPrintName = '$printName' AND
									trn_sampleinfomations_details.strComboName = '$comboName'  AND
									trn_sampleinfomations_details.intColorId = '$colorId' AND
									trn_sampleinfomations_details.intTechniqueId = '$techId_p'  
										";
									$db->RunQuery2($sql);
						}					
					//--------------------------------------------------------
					
					
					
				}
					

					
				}
			//die();
			
			$sql = "DELETE FROM `trn_sampleinfomations_details` WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevNo`='$revisionNo') AND TECHNICAL_SAVED_OR_UPDATED=0 ";
			$db->RunQuery2($sql);
	
				$sql_1 = "UPDATE `trn_sampleinfomations_details` SET `intPrintMode`='$printMode' WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevNo`='$revisionNo') AND (`strPrintName`='$printName') AND (`strComboName`='$comboName')";
				$result_1 = $db->RunQuery2($sql_1);
				if(!$result_1)
				throw new Exception('Failed to update technical data222');
				//for($m=0;$m<count($arrCombo);$m++)
				//{
					//if($comboName=='c2' && $printName=='print 1' )
					//print_r($arrCombo[$y]['gridTechnicalDetails']);
		//////////////////////////////////////////////////////////////////////
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
			$response['sampleNo'] 	= $sampleNo;
			$response['sampleYear'] = $sampleYear;
			$response['revisionNo'] = $revisionNo;
			
			//////////////// set sample no ////////////////////////////////
			
			$sql = "SELECT DISTINCT
						trn_sampleinfomations.intSampleNo
					FROM trn_sampleinfomations
					WHERE
						trn_sampleinfomations.intSampleYear =  '$sampleYear'
					order by intSampleNo
					";
			$result = $db->RunQuery2($sql);
			$sampleNoOption='<option value=""></option>';
			while($row=mysqli_fetch_array($result))
			{
				$sampleNoOption.= "<option value=\"".$row['intSampleNo']."\">".$row['intSampleNo']."</option>";
			}
			$response['sampleNoOption'] = $sampleNoOption;
			///////////////////////////////////////////////////////////////
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
			//if(!$result2)
			throw new Exception('Failed to update technical data444');
		}
          
		  
		 //-----------------non dirct RM------------------------       
		$arr = json_decode($_REQUEST['arrNonDirRm'],true);
		$saved=0;
		$tobeSaved=0;
		foreach($arr as $NonDirRm)
		{
                  
			$tobesaved++;
			
			$sampleNo 		= $_REQUEST['sampleNo'];
			$sampleYear 	= $_REQUEST['sampleYear'];
			$revisionNo 	= $_REQUEST['revisionNo'];
			$ITEM           = $NonDirRm['item'];
			$CONSUMPTION    = $NonDirRm['consumption'];
			$sql_select = "SELECT
						trn_sample_non_direct_rm_consumption.SAMPLE_NO,
						trn_sample_non_direct_rm_consumption.SAMPLE_YEAR,
						trn_sample_non_direct_rm_consumption.REVISION_NO,
						trn_sample_non_direct_rm_consumption.ITEM,
						trn_sample_non_direct_rm_consumption.CONSUMPTION
						FROM
						trn_sample_non_direct_rm_consumption
						where (`SAMPLE_NO`='$sampleNo') AND (`SAMPLE_YEAR`='$sampleYear') AND (`REVISION_NO`='$revisionNo ')AND(`ITEM`='$ITEM')";
			
			$result_select = $db->RunQuery2($sql_select);//run without considering errors
			$row_select = mysqli_fetch_array($result_select);
                       
		   if($row_select['ITEM']=="")
			   {
					
					$sql = "INSERT INTO `trn_sample_non_direct_rm_consumption`(`SAMPLE_NO`,`SAMPLE_YEAR`,`REVISION_NO`,`ITEM`,`CONSUMPTION`) VALUES ('$sampleNo','$sampleYear','$revisionNo','$ITEM ','$CONSUMPTION')";
					//alert($sql);
					$result = $db->RunQuery2($sql);
					
					if($result)
					{
					$saved++;
					}
					
				}
                else
				{
					$sql_update = "UPDATE `trn_sample_non_direct_rm_consumption` SET `CONSUMPTION`='$CONSUMPTION' WHERE (`SAMPLE_NO`='$sampleNo') AND (`SAMPLE_YEAR`='$sampleYear') AND (`REVISION_NO`='$revisionNo ') AND (`ITEM`='$ITEM')  ";
					//  echo $sql_update;
					$result_update = $db->RunQuery2($sql_update);
					
					if($result_update)
					{
						$saved++;
					}
                }
             
		
 				if($saved==$tobesaved)
				{
					$response['type'] 		= 'pass';
					$response['msg'] 		= 'saved successfully.';
				}
				else
				{
					$msg                      = "Failed to save";
					$response['type']         = 'fail';
					$response['msg'] 		= $msg; 
					throw new Exception($msg);
				}
                    
                   
			}
                
		 //-----------------no of ups------------------------       
	/*	$arr = json_decode($_REQUEST['arrUps'],true);
		$saved=0;
		$tobeSaved=0;
		foreach($arr as $ups)
		{
			$tobesaved++;
			
			$sampleNo 	= $_REQUEST['sampleNo'];
			$sampleYear	= $_REQUEST['sampleYear'];
			$revisionNo	= $_REQUEST['revisionNo'];
			$combo 		= $ups['combo'];
			$print 		= $ups['print'];
			$ups      	= $ups['ups'];
			$sql_select = "SELECT
							trn_sampleinfomations_combo_print_details.SAMPLE_NO,
							trn_sampleinfomations_combo_print_details.SAMPLE_YEAR,
							trn_sampleinfomations_combo_print_details.REVISION,
							trn_sampleinfomations_combo_print_details.COMBO,
							trn_sampleinfomations_combo_print_details.PRINT,
							trn_sampleinfomations_combo_print_details.NO_OF_UPS
							FROM `trn_sampleinfomations_combo_print_details`
							WHERE
							trn_sampleinfomations_combo_print_details.SAMPLE_NO = '$sampleNo' AND
							trn_sampleinfomations_combo_print_details.SAMPLE_YEAR = '$sampleYear' AND
							trn_sampleinfomations_combo_print_details.REVISION = '$revisionNo' AND
							trn_sampleinfomations_combo_print_details.COMBO = '$combo' AND
							trn_sampleinfomations_combo_print_details.PRINT = '$print'
							";
										
			$result_select = $db->RunQuery2($sql_select);//run without considering errors
			$row_select = mysqli_fetch_array($result_select);
                       
		   if($row_select['SAMPLE_NO']=="")
			   {
					
					$sql = "INSERT INTO `trn_sampleinfomations_combo_print_details`(`SAMPLE_NO`,`SAMPLE_YEAR`,`REVISION`,`COMBO`,`PRINT`,`NO_OF_UPS`) VALUES ('$sampleNo','$sampleYear','$revisionNo','$combo','$print','$ups')";
					//alert($sql);
					$result = $db->RunQuery2($sql);
					
					if($result)
					{
					$saved++;
					}
					
				}
                else
				{
					$sql_update = "UPDATE `trn_sampleinfomations_combo_print_details` SET `NO_OF_UPS`='$ups' WHERE (`SAMPLE_NO`='$sampleNo') AND (`SAMPLE_YEAR`='$sampleYear') AND (`REVISION`='$revisionNo ') AND (`COMBO`='$combo') AND (`PRINT`='$print')  ";
					//  echo $sql_update;
					$result_update = $db->RunQuery2($sql_update);
					
					if($result_update)
					{
						$saved++;
					}
                }
             
				if($saved==$tobesaved)
				{
					$response['type'] 		= 'pass';
					$response['msg'] 		= 'saved successfully.';
				}
				else
				{
					$msg                      = "Failed to save";
					$response['type']         = 'fail';
					$response['msg'] 		= $msg; 
					throw new Exception($msg);
				}
                    
                   
			} */
	
	         
                
	}
    else if($requestType=='deleteData')
	{
		$sampleNo 	 = $_REQUEST['sampleNo'];
		$sampleYear 	 = $_REQUEST['sampleYear'];
		$revisionNo   = $_REQUEST['revisionNo']; 
		$item         = $_REQUEST["item"];
		$sql_item_del = "DELETE FROM `trn_sample_non_direct_rm_consumption` WHERE (`SAMPLE_NO`='$sampleNo') AND (`SAMPLE_YEAR`='$sampleYear') AND (`REVISION_NO`='$revisionNo ') AND(`ITEM`='$item') ";
          //echo $sql_item_del;
	   $result_del = $db->RunQuery2($sql_item_del); 
         if($result_del)
             {
          $response['type'] 		= 'pass';
	  $response['msg'] 		= 'Deleted successfully.';
          	
             }
           else
           {
           $msg = "Failed to Delete";
          $response['type'] 		= 'fail';
	  $response['msg'] 		= $msg; 
          throw new Exception($msg);
           }
         
        }
    else if($requestType=='deleteDataTechSampple')
	{
		$sampleNo 	 	= $_REQUEST['sampleNo'];
		$sampleYear 	= $_REQUEST['sampleYear'];
		$revisionNo   	= $_REQUEST['revisionNo']; 
		$combo		   	= $_REQUEST['combo']; 
		$print		   	= $_REQUEST['print']; 
		$colour		   	= $_REQUEST['color']; 
		$technique   	= $_REQUEST['technique']; 
		$item         	= $_REQUEST["item"];
		if($item=='')
			$item='NULL';
		
        $sql_sel 	= "SELECT * FROM `trn_sampleinfomations_details` WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevNo`='$revisionNo ') AND (`strComboName`='$combo ') AND (`strPrintName`='$print ') AND (`intColorId`='$colour ') AND (`intTechniqueId`='$technique ')";
	   	$result_sel = $db->RunQuery2($sql_sel); 
		$records		=mysqli_num_rows($result_sel);
	
		
        $sql_selT 	= "SELECT * FROM `trn_sampleinfomations_details_technical` WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevNo`='$revisionNo ') AND (`strComboName`='$combo ') AND (`strPrintName`='$print ') AND (`intColorId`='$colour ')";
	   	$result_selT = $db->RunQuery2($sql_selT); 
		$records_tec		=mysqli_num_rows($result_selT);

		
		if($records>1 ){
      		$sql_item_del = "DELETE FROM `trn_sampleinfomations_details` WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevNo`='$revisionNo ') AND (`strComboName`='$combo ') AND (`strPrintName`='$print ') AND (`intColorId`='$colour ') AND (`intTechniqueId`='$technique ') AND(`intItem`=$item) ";
	   if($item=='NULL')
       		$sql_item_del = "DELETE FROM `trn_sampleinfomations_details` WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevNo`='$revisionNo ') AND (`strComboName`='$combo ') AND (`strPrintName`='$print ') AND (`intColorId`='$colour ') AND (`intTechniqueId`='$technique ') AND(`intItem` IS NULL) LIMIT 1 ";
		}
		else if($records==1){
        	$sql_item_del = "UPDATE `trn_sampleinfomations_details` SET intItem=NULL WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevNo`='$revisionNo ') AND (`strComboName`='$combo ') AND (`strPrintName`='$print ') AND (`intColorId`='$colour ') AND (`intTechniqueId`='$technique ') AND(`intItem`='$item') ";
		}
	   $result_del = $db->RunQuery2($sql_item_del); 
     
	 
		/*if($records_tec > 0){
			$msg = "Can't delete. Ink type details exists";
			$response['type'] 		= 'fail';
			$response['msg'] 		= $msg; 
			throw new Exception($msg);
		}
		else */if($result_del)
		{
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else
		{
			$msg = "Failed to Delete";
			$response['type'] 		= 'fail';
			$response['msg'] 		= $msg; 
			throw new Exception($msg);
		}
         
	}
    else if($requestType=='delete_saved_colors')
	{
 
	$sampNo 	 = $_REQUEST['sampNo'];
	$sampYear 	 = $_REQUEST['sampYear'];
	$rev   = $_REQUEST['rev']; 
	$combo   = $_REQUEST['combo']; 
	$printName   = $_REQUEST['printName']; 
	$color   = $_REQUEST['color']; 
	$technique   = $_REQUEST['technique']; 

	$sql = "SELECT
				trn_sampleinfomations.intStatus,
				trn_sampleinfomations.intTechnicalStatus,
				trn_sampleinfomations.intTechnicalApproveLevelStart,
				trn_sampleinfomations.intMarketingStatus
			FROM trn_sampleinfomations
			WHERE
				trn_sampleinfomations.intSampleNo =  '$sampNo' AND
				trn_sampleinfomations.intSampleYear =  '$sampYear' AND
				trn_sampleinfomations.intRevisionNo =  '$rev'
			";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	$intTechnicalStatus = $row['intTechnicalStatus'];

	if($intTechnicalStatus==1)
	{
		$msg =  "This sample is already confirmed by the technical department.You can't do changes now.";
		$response['type'] 		= 'fail';
		$response['msg'] 		= $msg;
		throw new Exception($msg);

	}

	  
	$sql = "DELETE FROM `trn_sampleinfomations_details_technical` WHERE (`intSampleNo`='$sampNo') AND (`intSampleYear`='$sampYear') AND (`intRevNo`='$rev')  AND (`strComboName`='$combo')  AND (`strPrintName`='$printName')
	 AND (`intColorId`='$color') ";
	$result_del_tech = $db->RunQuery2($sql); 

	$sql_d = "DELETE FROM `trn_sampleinfomations_details` WHERE (`intSampleNo`='$sampNo') AND (`intSampleYear`='$sampYear') AND (`intRevNo`='$rev')  AND (`strComboName`='$combo')  AND (`strPrintName`='$printName')
	 AND (`intColorId`='$color') AND (`intTechniqueId`='$technique') ";
	$result_del_sd = $db->RunQuery2($sql_d); 

	if($result_del_tech && $result_del_sd)
	{
		$response['type'] 		= 'pass';
		$response['msg'] 		= 'Deleted successfully.';
	
	}
	else
	{
		$msg = "Failed to Delete";
		$response['type'] 		= 'fail';
		$response['msg'] 		= $msg; 
		throw new Exception($msg);
	}
	
	
         
}
    else if($requestType=='copy_routing')
	{
 
		$sampleNo 	= $_REQUEST['sampNo'];
		$sampleYear	= $_REQUEST['sampYear'];
		$revisionNo	= $_REQUEST['rev'];
		$combo_f 		= $_REQUEST['combo'];
		$print_f 		= $_REQUEST['printName'];
		
	
		$sql 	= "select  * 
						FROM `trn_sampleinfomations_combo_print_routing`
						WHERE
						trn_sampleinfomations_combo_print_routing.SAMPLE_NO = '$sampleNo' AND
						trn_sampleinfomations_combo_print_routing.SAMPLE_YEAR = '$sampleYear' AND
						trn_sampleinfomations_combo_print_routing.REVISION = '$revisionNo' AND
						(trn_sampleinfomations_combo_print_routing.COMBO = '$combo_f' OR
						trn_sampleinfomations_combo_print_routing.PRINT = '$print_f')  
						
						";
		$result 	= $db->RunQuery2($sql);
		$row 		= mysqli_fetch_array($result);
		$routing 	= $row['ROUTING'];
		$ups		= $row['NO_OF_UPS'];
		
	
		$sql_del 	= "DELETE 
						FROM `trn_sampleinfomations_combo_print_routing`
						WHERE
						trn_sampleinfomations_combo_print_routing.SAMPLE_NO = '$sampleNo' AND
						trn_sampleinfomations_combo_print_routing.SAMPLE_YEAR = '$sampleYear' AND
						trn_sampleinfomations_combo_print_routing.REVISION = '$revisionNo' AND
						(trn_sampleinfomations_combo_print_routing.COMBO <> '$combo_f' OR
						trn_sampleinfomations_combo_print_routing.PRINT <> '$print_f')  
						
						";
		$result_del = $db->RunQuery2($sql_del);//run without considering errors
		
 		
		$sql_del = "DELETE 
					FROM `trn_sampleinfomations_combo_print_details_processes`
					WHERE
					trn_sampleinfomations_combo_print_details_processes.SAMPLE_NO 	= '$sampleNo' AND
					trn_sampleinfomations_combo_print_details_processes.SAMPLE_YEAR = '$sampleYear' AND
					trn_sampleinfomations_combo_print_details_processes.REVISION 	= '$revisionNo' AND
					(trn_sampleinfomations_combo_print_details_processes.COMBO 		<> '$combo_f' OR
					trn_sampleinfomations_combo_print_details_processes.PRINT 		<> '$print_f')
					";
									
		$result_del = $db->RunQuery2($sql_del);//run without considering errors
 	
	
	//$arr = json_decode($_REQUEST['arrRouting'],true);
	$saved =0;
	$tobeSaved=0;
 	//if($_REQUEST['arrProcess'] !=''){



	$sql	="SELECT
				trn_sampleinfomations_details.intSampleNo,
				trn_sampleinfomations_details.intSampleYear,
				trn_sampleinfomations_details.intRevNo,
				trn_sampleinfomations_details.strComboName,
				trn_sampleinfomations_details.strPrintName
				FROM `trn_sampleinfomations_details`
				WHERE
				trn_sampleinfomations_details.intSampleNo = '$sampleNo' AND
				trn_sampleinfomations_details.intSampleYear = '$sampleYear' AND
				trn_sampleinfomations_details.intRevNo = '$revisionNo' AND
				(trn_sampleinfomations_details.strPrintName <> '$print_f' OR
				trn_sampleinfomations_details.strComboName <> '$combo_f')
				GROUP BY
				trn_sampleinfomations_details.intSampleNo,
				trn_sampleinfomations_details.intSampleYear,
				trn_sampleinfomations_details.intRevNo,
				trn_sampleinfomations_details.strPrintName,
				trn_sampleinfomations_details.strComboName
				";
	$result = $db->RunQuery2($sql);
	while($rout=mysqli_fetch_array($result))
	{

	//foreach($arr as $rout)
	//{
		$tobeSaved++;
		//$sampleNo 	= $rout['sampNo'];
		//$sampleYear	= $rout['sampYear'];
		//$revisionNo	= $rout['rev'];
		$combo 		= $rout['strComboName'];
		$print 		= $rout['strPrintName'];
		//$routing	= $rout['id'];
		//$ups   		= $rout['ups'];
 	
				 //--------------------------------------------  
 				 $sql_r = "INSERT INTO trn_sampleinfomations_combo_print_routing (`SAMPLE_NO`,`SAMPLE_YEAR`,`REVISION`,`COMBO`,`PRINT`,`ROUTING`,`NO_OF_UPS`)  
            SELECT `SAMPLE_NO`,`SAMPLE_YEAR`,`REVISION`,'$combo','$print',`ROUTING`,`NO_OF_UPS` 
              FROM trn_sampleinfomations_combo_print_routing 
             WHERE SAMPLE_NO = '$sampleNo' AND 
			 SAMPLE_YEAR = '$sampleYear' AND
			 REVISION = '$revisionNo' AND
			 COMBO = '$combo_f' AND
			 PRINT = '$print_f' 
			 ";
				//alert($sql);
				$result_r = $db->RunQuery2($sql_r);
				
				if($result_r)
				{
				$saved++;
				}	
				//-------------------------------------------
 				$sql_p = "INSERT INTO `trn_sampleinfomations_combo_print_details_processes`(`SAMPLE_NO`,`SAMPLE_YEAR`,`REVISION`,`COMBO`,`PRINT`,`ROUTING`,`PROCESS`,`ORDER_BY`) 
				SELECT 
`SAMPLE_NO`,`SAMPLE_YEAR`,`REVISION`,'$combo','$print',`ROUTING`,`PROCESS`,`ORDER_BY`	FROM trn_sampleinfomations_combo_print_details_processes 
             WHERE SAMPLE_NO = '$sampleNo' AND 
			 SAMPLE_YEAR = '$sampleYear' AND
			 REVISION = '$revisionNo' AND
			 COMBO = '$combo_f' AND
			 PRINT = '$print_f' 	";
				$result_p = $db->RunQuery2($sql_p);
				
				if($result_p)
				{
				$saved++;
				}	
							
 		 

			   
		}

	if($saved==$tobeSaved*2)
	{
		$response['type'] 		= 'pass';
		$response['msg'] 		= 'Coppied successfully.';
	}
	else
	{
		$msg                    = "Failed to save processes";
		$response['type']       = 'fail';
		$response['msg'] 		= $msg; 
		throw new Exception($msg);
	}
	
         
}


else if($requestType=='save_process')
{

		$sampleNo 	= $_REQUEST['sampleNo'];
		$sampleYear	= $_REQUEST['sampleYear'];
		$revisionNo	= $_REQUEST['revisionNo'];
		$combo 		= $_REQUEST['combo'];
		$print 		= $_REQUEST['printId'];
	
		$sql_del 	= "DELETE 
						FROM `trn_sampleinfomations_combo_print_routing`
						WHERE
						trn_sampleinfomations_combo_print_routing.SAMPLE_NO = '$sampleNo' AND
						trn_sampleinfomations_combo_print_routing.SAMPLE_YEAR = '$sampleYear' AND
						trn_sampleinfomations_combo_print_routing.REVISION = '$revisionNo' AND
						trn_sampleinfomations_combo_print_routing.COMBO = '$combo' AND
						trn_sampleinfomations_combo_print_routing.PRINT = '$print'  
						
						";
		$result_del = $db->RunQuery2($sql_del);//run without considering errors
		
 		
		$sql_del = "DELETE 
					FROM `trn_sampleinfomations_combo_print_details_processes`
					WHERE
					trn_sampleinfomations_combo_print_details_processes.SAMPLE_NO 	= '$sampleNo' AND
					trn_sampleinfomations_combo_print_details_processes.SAMPLE_YEAR = '$sampleYear' AND
					trn_sampleinfomations_combo_print_details_processes.REVISION 	= '$revisionNo' AND
					trn_sampleinfomations_combo_print_details_processes.COMBO 		= '$combo' AND
					trn_sampleinfomations_combo_print_details_processes.PRINT 		= '$print'
					";
									
		$result_del = $db->RunQuery2($sql_del);//run without considering errors
 	
	
	$arr = json_decode($_REQUEST['arrRouting'],true);
	$saved =0;
	$tobeSaved=0;
 	//if($_REQUEST['arrProcess'] !=''){
	foreach($arr as $rout)
	{
		$tobeSaved++;
		$sampleNo 	= $rout['sampNo'];
		$sampleYear	= $rout['sampYear'];
		$revisionNo	= $rout['rev'];
		$combo 		= $rout['combo'];
		$print 		= $rout['print'];
		$routing	= $rout['id'];
		$ups   		= $rout['ups'];
 				   
 				 $sql = "INSERT INTO `trn_sampleinfomations_combo_print_routing`(`SAMPLE_NO`,`SAMPLE_YEAR`,`REVISION`,`COMBO`,`PRINT`,`ROUTING`,`NO_OF_UPS`) VALUES ('$sampleNo','$sampleYear','$revisionNo','$combo','$print','$routing','$ups')";
				//alert($sql);
				$result = $db->RunQuery2($sql);
				
				if($result)
				{
				$saved++;
				}				
 		 
			if($saved==$tobeSaved)
			{
				
				global $status;
				$status++ ;
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'saved successfully.';
			}
			else
			{
				$msg                      = "Failed to save routings";
				$response['type']         = 'fail';
				$response['msg'] 		= $msg; 
				$response['sql'] 		= $sql; 
				throw new Exception($msg);
			}
				
			   
		}
	//}
	 $response['status'] = $status;
	 //save process------------------------       
	$arr = json_decode($_REQUEST['arrProcess'],true);
	$saved=0;
	$tobeSaved=0;
	foreach($arr as $pro)
	{
		$tobeSaved++;
		$sampleNo 	= $pro['sampNo'];
		$sampleYear	= $pro['sampYear'];
		$revisionNo	= $pro['rev'];
		$combo 		= $pro['combo'];
		$print 		= $pro['print'];
		$routing	= $pro['routing_id'];
		$process   	= $pro['id'];
		$order   	= $pro['order'];
 				   
 				$sql = "INSERT INTO `trn_sampleinfomations_combo_print_details_processes`(`SAMPLE_NO`,`SAMPLE_YEAR`,`REVISION`,`COMBO`,`PRINT`,`ROUTING`,`PROCESS`,`ORDER_BY`) VALUES ('$sampleNo','$sampleYear','$revisionNo','$combo','$print','$routing','$process','$order')";
				//alert($sql);
				$result = $db->RunQuery2($sql);
				$sql1 = "SELECT
						COUNT(tbl.PROCESS) as count
						FROM
						trn_sampleinfomations_combo_print_details_processes AS tbl
						INNER JOIN mst_costingprocesses ON tbl.PROCESS = mst_costingprocesses.intId
						WHERE
						tbl.SAMPLE_NO  = '$sampleNo' AND
						tbl.SAMPLE_YEAR = '$sampleYear' AND
						tbl.REVISION = '$revisionNo' AND
						tbl.COMBO = '$combo' AND
						tbl.PRINT = '$print' AND 
						mst_costingprocesses.printingMethod = 1
						";
				$result_printingprocess = $db->RunQuery2($sql1);
				$printing = mysqli_fetch_array($result_printingprocess);
				$cont = $printing['count'];
				if($cont > 1)
				{
					$msg =  "can not add two ro more printing process";
					$response['type'] 		= 'fail';
					$response['msg'] 		= $msg;
					throw new Exception($msg);
					//return;
		
				}
				if($result)
				{
				$saved++;
				}
		
			if($saved==$tobeSaved)
			{
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'saved successfully.';
			}
			else
			{
				$msg                    = "Failed to save processes";
				$response['type']       = 'fail';
				$response['msg'] 		= $msg; 
				throw new Exception($msg);
			}
				
			 
		}
		
		
	if($_REQUEST['arrProcess']==''){
		$response['type'] 		= 'pass';
		$response['msg'] 		= 'Routing process is not assigned. Technical sample sheet persistance requires routing process for each combo print.';
		
	}
 
}



	$db->commit();
	echo json_encode($response);
	//$db->disconnect();	
	
}
catch(Exception $e)
{	
	switch ($e->getCode()){
		case 0:
			$db->rollback();		
			$response['msg'] 	=  $e->getMessage();
			//$response['error'] 	=  $error_handler->jTraceEx($e);
			$response['type'] 	=  'fail';
			//$response['sql']	=  $db->getSql();
			break;		
		case 1:
			$db->commit();
			$response['msg'] 	=  $e->getMessage();
			$response['pass'] 	=  'fail';
			break;
	}
	echo json_encode($response);
	//$db->disconnect();
        
            
        
}
	
function checkForColourExists($sampleNo,$sampleYear,$revisionNo,$comboName,$printName,$colorId,$technique,$item){
	global $db;
	 $sql_select = "SELECT
					trn_sampleinfomations_details.intColorId
					FROM `trn_sampleinfomations_details`
					WHERE
					trn_sampleinfomations_details.intSampleNo = '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear = '$sampleYear' AND
					trn_sampleinfomations_details.intRevNo = '$revisionNo' AND
					trn_sampleinfomations_details.strPrintName = '$printName' AND
					trn_sampleinfomations_details.strComboName = '$comboName' AND
					trn_sampleinfomations_details.intColorId = '$colorId' AND 
					trn_sampleinfomations_details.intTechniqueId = '$technique' AND 
					trn_sampleinfomations_details.intItem = '$item'
					";
	  
	   $result_select = $db->RunQuery2($sql_select);//run without considering errors
	   $row_select = mysqli_fetch_array($result_select);
	   
	   if($row_select['intColorId']=="")
	   return false;
	   else
	   return true;
}

function insertSample_dtails($sampleNo,$sampleYear,$revisionNo,$comboName,$printName,$colorId,$technique,$item,$qty,$w,$h){
	global $db;
	if($item=='')
		$item	='NULL';
	  $sql_select = "INSERT INTO trn_sampleinfomations_details (`intSampleNo`,`intSampleYear`,`intRevNo`,`strPrintName`,`strComboName`,`intColorId`,intPrintMode,intWashStanderd,intGroundColor,intFabricType,`intTechniqueId`,`intItem`,`dblQty`,`size_w`,`size_h`,intCompanyId,COLOUR_ADDED_BY_TECHNICAL,TECHNICAL_SAVED_OR_UPDATED) 
            SELECT `intSampleNo`,`intSampleYear`,`intRevNo`,`strPrintName`,`strComboName`,'$colorId',intPrintMode,intWashStanderd,intGroundColor,intFabricType,'$technique',$item,$qty,$w,$h,intCompanyId , 1 ,1 
              FROM trn_sampleinfomations_details
			  where 
				trn_sampleinfomations_details.intSampleNo = '$sampleNo' AND
				trn_sampleinfomations_details.intSampleYear = '$sampleYear' AND
				trn_sampleinfomations_details.intRevNo = '$revisionNo' AND
				trn_sampleinfomations_details.strPrintName = '$printName' AND
				trn_sampleinfomations_details.strComboName = '$comboName' limit 1 
					";
		   $result_select = $db->RunQuery2($sql_select);//run without considering errors
		
		//if($comboName=='c1' && $printName=='print 1' && $colorId=='5097' && $technique=='353'){
		//print_r($arrColTech['marketingItems']);
	 		//echo $sql_select;
		//}
		//if($item=='NULL'){
		 
			 $sql_sel 	= "SELECT * FROM `trn_sampleinfomations_details` WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevNo`='$revisionNo ') AND (`strComboName`='$comboName') AND (`strPrintName`='$printName') AND (`intColorId`='$colorId ') AND (`intTechniqueId`='$technique') AND `intItem` IS NOT NULL";
			$result_sel = $db->RunQuery2($sql_sel); 
			$records		=mysqli_num_rows($result_sel);
			if($records>0){
				 $sql_del 	= "DELETE FROM `trn_sampleinfomations_details` WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevNo`='$revisionNo ') AND (`strComboName`='$comboName') AND (`strPrintName`='$printName') AND (`intColorId`='$colorId ') AND (`intTechniqueId`='$technique') AND `intItem` IS NULL";
			$result_del = $db->RunQuery2($sql_del); 
		//	}
		}
		
	 
	   
	   if(!$result_select)
	   return false;
	   else
	   return true;
}

function updateSample_dtails($sampleNo,$sampleYear,$revisionNo,$comboName,$printName,$colorId,$technique,$item,$qty,$w,$h){
	global $db;
	if($item=='')
		$item	='NULL';
	if($w=='')
		$w	='NULL';
	if($h=='')
		$h	='NULL';
		
	  $sql_select = "UPDATE trn_sampleinfomations_details 
	  			SET 
					`dblQty`=$qty,
					`size_w`=$w,
					`size_h`=$h ,
					TECHNICAL_SAVED_OR_UPDATED=1 
			  where 
				trn_sampleinfomations_details.intSampleNo = '$sampleNo' AND
				trn_sampleinfomations_details.intSampleYear = '$sampleYear' AND
				trn_sampleinfomations_details.intRevNo = '$revisionNo' AND
				trn_sampleinfomations_details.strPrintName = '$printName' AND
				trn_sampleinfomations_details.strComboName = '$comboName'  AND
				trn_sampleinfomations_details.intColorId = '$colorId' AND
				trn_sampleinfomations_details.intTechniqueId = '$technique' AND
				trn_sampleinfomations_details.intItem = '$item'
					";
	  //if($item==8037 && $colorId=='14369')
	  //	echo $sql_select;
	   $result_select = $db->RunQuery2($sql_select);//run without considering errors
 	   
	   if(!$result_select)
	   return false;
	   else
	   return true;
}

function getRoutingSavedStatus($sampleNo,$sampleYear,$revision,$combo,$print){
	
	global $db;
	
	$sql_select = "SELECT
trn_sampleinfomations_details.intColorId,
trn_sampleinfomations_combo_print_routing.ROUTING
FROM
trn_sampleinfomations_details 
left JOIN trn_sampleinfomations_combo_print_routing 
ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_combo_print_routing.SAMPLE_NO 
AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_combo_print_routing.SAMPLE_YEAR 
AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_combo_print_routing.REVISION 
AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_combo_print_routing.COMBO 
AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_combo_print_routing.PRINT
WHERE
trn_sampleinfomations_details.intSampleNo = '$sampleNo' AND
trn_sampleinfomations_details.intSampleYear = '$sampleYear' AND
trn_sampleinfomations_details.intRevNo = '$revision' AND
trn_sampleinfomations_details.strPrintName = '$print' AND
trn_sampleinfomations_details.strComboName = '$combo'

					";
	  
	   $result_select = $db->RunQuery2($sql_select);//run without considering errors
	   $row_select = mysqli_fetch_array($result_select);
	
	if((($row_select['intColorId']) <> '13703' && ($row_select['intColorId']) <> '5347' && ($row_select['intColorId']) <> '25795') && $row_select['ROUTING']==''){
		$resp['err'] =1;
		$resp['sql'] = $sql_select;
	}
	else{
		$resp['err'] =0;
		$resp['sql'] = $sql_select;
		
	}
	return $resp;
}
	

?>
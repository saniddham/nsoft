<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$sessionUserId = $_SESSION['userId'];
$thisFilePath =  $_SERVER['PHP_SELF'];
$companyId		= $_SESSION["CompanyID"];

include  	"{$backwardseperator}dataAccess/Connector.php";

$sampleNo 	= $_REQUEST['sampleNo'];
$sampleYear = $_REQUEST['sampleYear'];
$revNo		= $_REQUEST['revNo'];

$rowId		= $_REQUEST['rowId'];
$cellId		= $_REQUEST['cellId'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Copy Ink Types</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
</head>

<body >
<form id="frmCopy" name="frmCopy" method="post">
<table width="431" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td colspan="4" class="gridHeader">Copy Ink Types</td>
    </tr>
  <tr class="normalfnt">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="normalfnt">
    <td width="98">Print Name</td>
    <td width="131"><select class="validate[required]" style="width:110px" name="cboPrintName" id="cboPrintName">
    <?php
		$sql = "SELECT
					distinct trn_sampleinfomations_details.strPrintName
				FROM trn_sampleinfomations_details
				WHERE
					trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_details.intRevNo =  '$revNo' AND
					trn_sampleinfomations_details.intCompanyId =  '$companyId'
				ORDER BY
					trn_sampleinfomations_details.strPrintName ASC
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			echo "<option value=\"".$row['strPrintName']."\">".$row['strPrintName']."</option>";	
		}
	?>
    </select></td>
    <td width="92">Combo</td>
    <td width="110"><select class="validate[required]" style="width:110px" name="cboCombo" id="cboCombo">
    <?php
		$sql = "SELECT
					distinct trn_sampleinfomations_details.strComboName
				FROM trn_sampleinfomations_details
				WHERE
					trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_details.intRevNo =  '$revNo' AND
					trn_sampleinfomations_details.intCompanyId =  '$companyId'
				ORDER BY
					trn_sampleinfomations_details.strComboName ASC
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			echo "<option value=\"".$row['strComboName']."\">".$row['strComboName']."</option>";	
		}
	?>
    </select></td>
  </tr>
  <tr>
    <td><input type="text" name="textfield" id="txtRowId" value="<?php echo $rowId; ?>" style="width:10px;display:none" />
      <input type="text" name="txtRowId" id="txtCellId" value="<?php echo $cellId; ?>" style="width:10px;display:none" /></td>
    <td colspan="2" align="right">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="right"><img src="images/Tadd.jpg" width="92" height="24" class="mouseover" id="butAdd" /></td>
    <td align="right"><img src="images/Tclose.jpg" width="92" height="24" class="mouseover" id="butClose1" /></td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>
<?php

$imageData = file_get_contents('php://input');
if (strlen(trim($imageData)) == 0 && isset($GLOBALS['HTTP_RAW_POST_DATA'])) {
    $imageData = $GLOBALS['HTTP_RAW_POST_DATA'];
}
$sampleNo = $_REQUEST['sampleNo'];
$printId = $_REQUEST['printId'];
$sampleYear = $_REQUEST['sampleYear'];
$revisionNo = $_REQUEST['revisionNo'];
$newImage = $_REQUEST['newImage'];

if ($newImage == 1) {
    $filteredData = substr($imageData, strpos($imageData, ",") + 1);
    $unencodedData = base64_decode($filteredData);

    $stylenumber = $_GET['stylenumber'];
    $filename = $_GET['filename'];

    $fp = fopen("../../../../../documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revisionNo-$printId.png", 'w');
    fwrite($fp, $unencodedData);
    fclose($fp);
} else {
    copy($imageData, "../../../../../documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revisionNo-$printId.png");

}
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php

include_once "libraries/jqgrid2/inc/jqgrid_dist.php";
//BEGIN - ADD DEFAULT WHERE STRING WHEN FORM LOADING {
$arr =  json_decode($_REQUEST['filters'],true);

//----------marketers--------------------------------
$str_marketer = "" ;
if(!count($arr)>0){
    $sql1 = "SELECT
			sys_users.strUserName
			FROM
			mst_marketer
			INNER JOIN sys_users ON mst_marketer.intUserId = sys_users.intUserId
			where
			sys_users.intUserId  <> 2
			order by sys_users.strUserName asc
			";
    $result1 = $db->RunQuery($sql1);

//BEGIN - ADD DEFAULT WHERE STRING WHEN FORM LOADING {
    $arr =  json_decode($_REQUEST['filters'],true);
    $str1   = "" ;
//$str1   = ":All;" ;
    $j		= 0;
    while($row=mysqli_fetch_array($result1))
    {
        if($j==0)
            $ini_marketer	=$row['strUserName'];
        $marketer	    = $row['strUserName'];
        $str1 		   .= $marketer.":".$marketer.";" ;
        $j++;
    }
//$str2	= $marketer.":".$marketer.";" ;
    $str_marketer	.= $str1;
    $str_marketer .= ":All" ;
}
//----------customers--------------------------------
$str_customer	='';
if(!count($arr)>0){
    $sql1 = 'SELECT DISTINCT
   			( REPLACE ( REPLACE( REPLACE( mst_customer.strName, "  ", " " ), "  ", " " ), "  ", " " ) ) as strName
 			FROM
			mst_customer
 			order by mst_customer.strName asc
			';
    $result1 = $db->RunQuery($sql1);

    $str1   = "" ;
//$str1   = ":All;" ;
    $j		= 0;
    while($row=mysqli_fetch_array($result1))
    {
        if($j==0)
            $ini_customer	=$row['strName'];

        $customer	= $row['strName'];
        $str1 		   .= $customer.":".$customer.";" ;
        $j++;
    }
//$str2	= $marketer.":".$marketer.";" ;
    $str_customer	.= $str1;
    $str_customer .= ":All" ;

}

//----------Date--------------------------------
if(!count($arr)>0){


    $str_date   = "" ;
    for($i=date('Y');$i>=2012; $i--)
    {
        $str1 = "";

        $str1 		   .= $i ;
        $str_date	.= $str1.":".$str1.";" ;
    }
    $lastdate = $str1;
    $str_date .= ":All" ;

}

$arr = $arr['rules'];
$where_string = '';
foreach($arr as $k=>$v)
{

    if($v['field']=='Status')
    {
        if($arr_status[$v['data']]==2)
            $where_string .= " AND  ".$where_array[$v['field']]." >1 ";
        else
            $where_string .= " AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
    }
    else if($where_array[$v['field']])
        $where_string .= " AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}

$currentYear = date('Y');
$intUser = $sessions->getUserId();
if(!count($arr)>0 && ($ini_customer != '' && $ini_marketer != '')){
    $where_string .= ' AND ( REPLACE ( REPLACE( REPLACE( cus.strName, "  ", " " ), "  ", " " ), "  ", " " ) ) = ( REPLACE ( REPLACE( REPLACE('."'".$ini_customer."'".', "  ", " " ), "  ", " " ), "  ", " " ) )';
    $where_string .= ' AND ( REPLACE ( REPLACE( REPLACE( Marketer.strUserName, "  ", " " ), "  ", " " ), "  ", " " ) ) = ( REPLACE ( REPLACE( REPLACE('."'".$ini_marketer."'".', "  ", " " ), "  ", " " ), "  ", " " ) )';
    $where_string .=  " AND sampleinfo.intSampleYear = '%$currentYear%' ";
}


$sql = " SELECT * FROM (SELECT
					sampleinfo.intSampleNo,
					sampleinfo.intSampleYear,
					intSampleNo AS 'SampleYearNO',
					sampleinfo.intRevisionNo AS RevisionNo,
					sampleinfo.dtDate,
					sampleinfo.strGraphicRefNo,
					sampleinfo.intCustomer,
					sampleinfo.strStyleNo,
					sampleinfo.intBrand,
					sampleinfo.verivide_d65,
					sampleinfo.verivide_tl84,
					sampleinfo.verivide_cw,
					sampleinfo.verivide_f,
					sampleinfo.verivide_uv,
					sampleinfo.macbeth_dl,
					sampleinfo.macbeth_cw,
					sampleinfo.macbeth_inca,
					sampleinfo.macbeth_tl84,
					sampleinfo.macbeth_uv,
					sampleinfo.macbeth_horizon,
					sampleinfo.dblCuringCondition_temp,
					sampleinfo.dblCuringCondition_beltSpeed,
					sampleinfo.dblPressCondition_temp,
					sampleinfo.dblPressCondition_pressure,
					sampleinfo.dblPressCondition_time,
					sampleinfo.strMeshCount,
					sampleinfo.strAdditionalInstructions,
					sampleinfo.strAdditionalInstructionsTech,
					sampleinfo.intMarketingApproveLevelStart,
					sampleinfo.intTechnicalApproveLevelStart,
					sampleinfo.intCompanyId as locationId,
					sampleinfo.intStatus,
					mst_brand.strName AS brand,
					case sampleinfo.intMarketingStatus
                    when '1' then 'Approved'
                    when '0' then 'Rejected'
                    ELSE 'Pending'
                    end as MarketingStatus,

				    case sampleinfo.intTechnicalStatus
                    when '1' then 'Approved'
                    WHEN sampleinfo.intTechnicalStatus > 1 then 'Pending'
                    ELSE 'Pending'
                    end as TechnicalStatus,

                     case
                    when sampleinfo.intTechnicalOpenUser > 0  then 'YES'
                    ELSE 'NO'
                    end as TechnicalOpened,

                     case
                    when sampleinfo.intSampleRoomOpenUser > 0 then 'YES'
                    ELSE 'NO'
                    end as SampleOpened,

                     case sampleinfo.intSampleRoomStatus
                    when '1' then 'Approved'
                    WHEN sampleinfo.intSampleRoomStatus > 1 then 'Pending'
                    ELSE 'Pending'
                    end as SampleRoomStatus,

					sampleinfo.intSampleRoomStatus,
					sampleinfo.intCreator,
					sampleinfo.dtmCreateDate,
					sampleinfo.intTechUser,

					 IFNULL ((SELECT
							'YES' as 'YES'
							FROM
							costing_sample_header
							WHERE
							SAMPLE_NO 	=  sampleinfo.intSampleNo AND
							SAMPLE_YEAR 	=  sampleinfo.intSampleYear AND
							REVISION		=  sampleinfo.intRevisionNo LIMIT 1),'NO') AS sampleRaiced,

					IFNULL ((SELECT
							'YES' as 'YES'
							FROM
							trn_sample_special_technical_header
							WHERE
							SAMPLE_NO 	=  sampleinfo.intSampleNo AND
							SAMPLE_YEAR 	=  sampleinfo.intSampleYear AND
							REVISION_NO		=  sampleinfo.intRevisionNo AND
                            STATUS = '1'
							LIMIT 1),'NO') AS specialConsumer,


                    IFNULL ((SELECT
							'YES' as 'YES'
							FROM
							trn_orderdetails
							WHERE
							intSampleNo 	=  sampleinfo.intSampleNo AND
							intSampleYear 	=  sampleinfo.intSampleYear AND
							intRevisionNo		=  sampleinfo.intRevisionNo
							LIMIT 1),'NO') AS BulkOrder,



					IFNULL ((SELECT
							'YES' as 'YES'
							FROM
							trn_sample_color_recipes
							WHERE
							intSampleNo 	=  sampleinfo.intSampleNo AND
							intSampleYear 	=  sampleinfo.intSampleYear AND
							intRevisionNo		=  sampleinfo.intRevisionNo LIMIT 1),'NO') AS receiptRaiced,

					IFNULL ((SELECT
							'YES' as 'YES'
							FROM
							trn_sample_foil_consumption
							JOIN trn_sample_spitem_consumption  ON trn_sample_foil_consumption.intSampleNo =trn_sample_spitem_consumption.intSampleNo
							AND trn_sample_foil_consumption.intSampleYear = trn_sample_spitem_consumption.intSampleYear
							AND trn_sample_foil_consumption.intRevisionNo = trn_sample_spitem_consumption.intRevisionNo
							WHERE
							trn_sample_foil_consumption.intSampleNo 	=  sampleinfo.intSampleNo AND
							trn_sample_foil_consumption.intSampleYear 	=  sampleinfo.intSampleYear AND
							trn_sample_foil_consumption.intRevisionNo		=  sampleinfo.intRevisionNo LIMIT 1),'NO') AS SpecialItemRaiced,
					case
                  when (SELECT
							CONCAT(sys_users.strUserName,'/',trn_sampleinfomations_approvedby.dtApprovedDate) AS 'UserName'
							FROM
							trn_sampleinfomations_approvedby
							Inner Join sys_users ON trn_sampleinfomations_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							intSampleNo 	=  sampleinfo.intSampleNo AND
							intSampleYear 	=  sampleinfo.intSampleYear AND
							intRevNo		=  sampleinfo.intRevisionNo AND
							intApproveLevelNo =  '1' AND intStage=1) != ' '
							then (SELECT
							sys_users.strUserName as UserName
							FROM
							trn_sampleinfomations_approvedby
							Inner Join sys_users ON trn_sampleinfomations_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							intSampleNo 	=  sampleinfo.intSampleNo AND
							intSampleYear 	=  sampleinfo.intSampleYear AND
							intRevNo		=  sampleinfo.intRevisionNo AND
							intApproveLevelNo =  '1' AND intStage=1)
                    ELSE  'Approve'
                    end as MarketingApproved,

                    case
                  when (SELECT
							CONCAT(sys_users.strUserName,'/',trn_sampleinfomations_approvedby.dtApprovedDate) AS 'UserName'
							FROM
							trn_sampleinfomations_approvedby
							Inner Join sys_users ON trn_sampleinfomations_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							intSampleNo 	=  sampleinfo.intSampleNo AND
							intSampleYear 	=  sampleinfo.intSampleYear AND
							intRevNo		=  sampleinfo.intRevisionNo AND
							intApproveLevelNo =  '1' AND intStage=2 LIMIT 1) != ' '
							then (SELECT
							sys_users.strUserName as UserName
							FROM
							trn_sampleinfomations_approvedby
							Inner Join sys_users ON trn_sampleinfomations_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							intSampleNo 	=  sampleinfo.intSampleNo AND
							intSampleYear 	=  sampleinfo.intSampleYear AND
							intRevNo		=  sampleinfo.intRevisionNo AND
							intApproveLevelNo =  '1' AND intStage=2  LIMIT 1)
                    ELSE  'Approve'
                    end as TechnicleApprove,

                     case
                  when (SELECT
							CONCAT(sys_users.strUserName,'/',trn_sampleinfomations_approvedby.dtApprovedDate) AS 'UserName'
							FROM
							trn_sampleinfomations_approvedby
							Inner Join sys_users ON trn_sampleinfomations_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							intSampleNo 	=  sampleinfo.intSampleNo AND
							intSampleYear 	=  sampleinfo.intSampleYear AND
							intRevNo		=  sampleinfo.intRevisionNo AND
							intApproveLevelNo =  '1' AND intStage=3 LIMIT 1) != ' '
							then (SELECT
							sys_users.strUserName as UserName
							FROM
							trn_sampleinfomations_approvedby
							Inner Join sys_users ON trn_sampleinfomations_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							intSampleNo 	=  sampleinfo.intSampleNo AND
							intSampleYear 	=  sampleinfo.intSampleYear AND
							intRevNo		=  sampleinfo.intRevisionNo AND
							intApproveLevelNo =  '1' AND intStage=3  LIMIT 1)
                    ELSE  'Approve'
                    end as SampleApprove,
					sampleinfo.dtmTechEnterDate,
					sampleinfo.intMarketingUser,
					sampleinfo.dtmMerketingDate,
					sampleinfo.intSampleRoomEnterUser,
					sampleinfo.dtSampleRoomEnterDate,
					sampleinfo.intModifyer,
					sampleinfo.dtmModifyDate,
					cus.strName AS customerName,
					brand.strName AS brandName,
					firstUser.strUserName AS firstUser,
					seconduser.strUserName AS secondUser,
					thirduser.strUserName AS thirduser,
					Marketer.strUserName AS Marketer
				FROM
					trn_sampleinfomations AS sampleinfo
					left Join mst_customer AS cus ON cus.intId = sampleinfo.intCustomer
					left Join mst_brand AS brand ON brand.intId = sampleinfo.intBrand
					Left Join sys_users AS firstUser ON sampleinfo.intCreator = firstUser.intUserId
					Left Join sys_users AS seconduser ON seconduser.intUserId = sampleinfo.intTechUser
					Left Join sys_users AS thirduser ON thirduser.intUserId = sampleinfo.intSampleRoomEnterUser
					Left Join sys_users AS Marketer ON Marketer.intUserId = sampleinfo.intMarketer
					Join mst_brand ON mst_brand.intId = sampleinfo.intBrand
                     WHERE 1=1 $where_string
					) AS tb1 WHERE 1=1
				";


//echo $sql;
$col = array();

//Sample Created Date
$col["title"] = "Sample Created Date"; // caption of column
$col["name"] = "dtDate"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "4";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Customer"; // caption of column
$col["name"] 	= "customerName"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] 	= "4";
$col["stype"] 	= "select";
$str = $str;
$col["editoptions"] 	=  array("value"=> $str_customer);
$col["align"] 	= "center";

$cols[] = $col;	$col=NULL;

// Marketer
$col["title"] 	= "Marketer"; // caption of column
$col["name"] 	= "Marketer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] 	= "4";
$col["stype"] 	= "select";
$str = $str ;
$col["editoptions"] 	=  array("value"=> $str_marketer);
$col["align"] 	= "center";

$cols[] = $col;	$col=NULL;


//Sample NO
$col["title"] = "Sample"; // caption of column
$col["name"] = "intSampleNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";
$col["hidden"] = true;
$cols[] = $col;
$col=NULL;

//Sample Year
$col["title"] = "Sample Year"; // caption of column
$col["name"] = "intSampleYear"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";
$col["stype"] 	= "select";
$str = $str ;
$col["editoptions"] 	=  array("value"=> $str_date);
$cols[] = $col;
$col=NULL;


//Graphic NO
$col["title"] = "Graphic No"; // caption of column
$col["name"] = "strGraphicRefNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//Brand
$col["title"] = "Brand"; // caption of column
$col["name"] = "brand"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Sample Year/NO
$col["title"] = "Sample No"; // caption of column
$col["name"] = "SampleYearNO"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Technicle Opened
$col["title"] = "Technicle Opened"; // caption of column
$col["name"] = "TechnicalOpened"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "5";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Sample Opened
$col["title"] = "Sample Opened"; // caption of column
$col["name"] = "SampleOpened"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Revision
$col["title"] = "Revision"; // caption of column
$col["name"] = "RevisionNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;


//Marketing Status
$col["title"] = "Marketing Status"; // caption of column
$col["name"] = "MarketingStatus"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "5";
$col["align"] = "left";
$col["stype"] 	= "select";
$str 			= ":All;Pending:Pending;Approved:Approved;Rejected:Rejected" ;
$col["editoptions"] 	=  array("value"=> $str);
$cols[] = $col;	$col=NULL;

//Marketing Approved
$col["title"] = "Approve"; // caption of column
$col["name"] = "MarketingApproved"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "4";
$col["align"] = "left";
$col["sortable"] 			= true;
$col["search"] 				= true;
$col["editable"] 			= false;

$approvedcheck = $col["name"];
$col['link']				= '?q=990&no={intSampleNo}&year={intSampleYear}&revNo={RevisionNo}';


$col["linkoptions"] 		= "target='_blank'";
$cols[] = $col;	$col=NULL;

//Technicle Status
$col["title"] = "Technicle Status"; // caption of column
$col["name"] = "TechnicalStatus"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "5";
$col["align"] = "left";
$col["stype"] 	= "select";
$str 			= ":All;Pending:Pending;Approved:Approved;Rejected:Rejected" ;
$col["editoptions"] 	=  array("value"=> $str);
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Approve"; // caption of column
$col["name"] = "TechnicleApprove"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "4";
$col["align"] = "left";
$col["search"] 				= true;
$col["editable"] 			= false;
$col['link']				= '?q=976&no={intSampleNo}&year={intSampleYear}&revNo={RevisionNo}';
$col["linkoptions"] 		= "target='_blank'";
$cols[] = $col;	$col=NULL;
//CUSTOMER
$col["title"] = "Sample Room Status"; // caption of column
$col["name"] = "SampleRoomStatus"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "5";
$col["align"] = "left";
$col["stype"] 	= "select";
$str 			= ":All;Pending:Pending;Approved:Approved;Rejected:Rejected" ;
$col["editoptions"] 	=  array("value"=> $str);
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Approve"; // caption of column
$col["name"] = "SampleApprove"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "4";
$col["align"] = "left";
$col['link']				= '?q=1117&no={intSampleNo}&year={intSampleYear}&revNo={RevisionNo}';
$col["linkoptions"] 		= "target='_blank'";
$cols[] = $col;	$col=NULL;

//Status
$col["title"] = "Costing raised"; // caption of column
$col["name"] = "sampleRaiced"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Status
$col["title"] = "Receipt raised"; // caption of column
$col["name"] = "receiptRaiced"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;


//Status
$col["title"] = "Special Items Exists"; // caption of column
$col["name"] = "SpecialItemRaiced"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;


//Status
$col["title"] = "Special Consumer Approved"; // caption of column
$col["name"] = "specialConsumer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;


//Status
$col["title"] = "Bulk Orders"; // caption of column
$col["name"] = "BulkOrder"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;


$jq = new jqgrid('',$db);

$grid["caption"] 		= "Sample Sheet Listings";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= ''; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(
        "add"=>false, // allow/disallow add
        "edit"=>false, // allow/disallow edit
        "delete"=>false, // allow/disallow delete
        "rowactions"=>false, // show/hide row wise edit/del/save option
        "search" => "advance", // show single/multi field search condition (e.g. simple or advance)
        "export"=>true
    )
);
$out = $jq->render("list1");
?>
<title>Sample Sheet Listings</title>

<form id="frmlisting" name="frmlisting" method="post" action="">
    <div align="center" style="margin:10px"><?php echo $out; ?></div>
</form>
<?php die; ?>

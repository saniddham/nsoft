<?php
$companyId = $_SESSION['CompanyID'];
if($_SERVER['REQUEST_METHOD'] == "POST")
{
	$value = trim($_REQUEST['cboStatus']);
	$optionValue = trim($_REQUEST['cboSearchOption']);
	$searchValue = trim($_REQUEST['txtSearchValue']);
}
?>
<head>
<title>SEARCH SAMPLE</title>
<?php
include "include/javascript.html";
?>

<script type="text/javascript" language="javascript">
function pageSubmit()
{
	window.location.href = "main.php?cboStatus="+$('#cboStatus').val()
						+'&cboSearchOption='+$('#cboSearchOption').val()	
						+'&txtSearchValue='+$('#txtSearchValue').val()	 
}
function clearValue()
{
	document.getElementById('txtSearchValue').value = '';
	document.getElementById('txtSearchValue').focus();
}
</script>

</head>

<body>
<form id="frmSampleSearch" name="frmSampleSearch" method="post" autocomplete="off" >
<div align="center">
		<div class="trans_layoutXL"><div class="trans_text">SAMPLE SEARCHING..</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="9%" height="22" class="normalfnt">&nbsp;</td>
            <td width="21%" align="center"><span class="normalfntMid">Searching Option</span></td>
            <td width="18%"><span class="normalfnt">
              <select name="cboSearchOption" id="cboSearchOption" onchange="clearValue();" style="width:150px">
                <option value="">&nbsp;</option>
                <option value="intSampleNo" <?php echo($optionValue=='intSampleNo'?'selected':'') ?> >Sample No</option>
                <option value="strStyleNo" <?php echo($optionValue=='strStyleNo'?'selected':'') ?>>Style No</option>
                <option value="strName" <?php echo($optionValue=='strName'?'selected':'') ?>>Customer</option>
                <option value="dtDate" <?php echo($optionValue=='dtDate'?'selected':'') ?>>Date</option>
              </select>
            </span></td>
            <td width="17%"><input value="<?php echo $searchValue; ?>" type="text" name="txtSearchValue" id="txtSearchValue" /></td>
            <td width="21%" class="normalfnt"><a href="#"><img src="images/search.png" width="20" height="20" onclick="pageSubmit();" /></a></td>
            <td width="14%">&nbsp;</td>
          </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td>
        <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
        <div id="_head_wrap" style="width: 925px; overflow: hidden;">
        <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
        <thead>
            <tr class="gridHeader">
              <th style="width: 90px;" height="22" >Sample No</th>
              <th style="width: 80px;" >Style No</th>
              <th style="width: 140px;" >Graphic</th>
              <th style="width: 161px;" >Customer</th>
              <th style="width: 40px;" >Qty</th>
              <th style="width: 91px;" >Rec. Date</th>
              <th style="width: 70px;" >Dispatch Date</th>
              <th style="width: 90px;" >Cost Approval</th>
              <th style="width: 90px;" >Customer Approval</th>
              <th style="width: 40px;" >View</th>
              <th style="width: 16px;" ></th>
              </tr>
           </thead>
           </table>
           </div>
           <div class="normalfnt" id="_body_wrap" style="width: 925px; height: 400px; overflow: scroll;">
           <table class="tbl1 scroll"> 
           <tbody>
              <?php
			  
			  if($searchValue!='')
			  	$wherePart = " and $optionValue like '%$searchValue%'";
				
	 	 		 $sql = "SELECT 
				 s.intSampleNo,
				 s.intCustomer,
				 s.strStyleNo,
				 s.intSampleYear,
				 c.strName,
				 s.dtDate,
				 s.dtDeliveryDate,
				 s.dblSampleQty,
				 s.intStatus,
				 s.dblDeliverQty,
				 s.intRevisionNo,
				 s.intCompanyId
				 FROM trn_sampleinfomations s,mst_customer c
				 WHERE
				 s.intCustomer = c.intId
				 AND
				 s.intRevisionNo = (SELECT MAX(j.intRevisionNo) FROM trn_sampleinfomations j where 				s.intSampleNo=j.intSampleNo AND s.intSampleYear = j.intSampleYear)
				 AND
				 s.intCompanyId = $companyId
				 $wherePart
				 Group by intSampleNo
				 Order by intSampleYear,intSampleNo DESC";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					 $no 		=	 $row['intSampleNo'];
					 $year 		=	 $row['intSampleYear'];
					 $revNo 	= 	 $row['intRevisionNo'];
	  			 ?>
                <tr class="normalfnt">
				<td class="sampleno" bgcolor="#FFFFFF"  align="center">
				<?php echo $row['intSampleNo'];?>/<?php echo $row['intSampleYear'];?></td>
     			<td class="styleno" bgcolor="#FFFFFF"  align="center"  >
				<?php echo $row['strStyleNo'];?></td>
				<td class="graphic" bgcolor="#FFFFFF" align="center">
                <img src="documents/sampleinfo/samplePictures/<?php echo "$no-$year-$revNo.jpg";  ?>" width="140" height="90" /></td>
              	<td class="customer"  bgcolor="#FFFFFF">
				<?php echo $row['strName'];?></td>
                <td  class="qty" align="center" bgcolor="#FFFFFF"><?php echo $row['dblSampleQty'];?></td>
              	<td class="recdate" bgcolor="#FFFFFF" align="center"><?php echo $row['dtDate'];?></td>
                <td class="dispatchdate" align="center" bgcolor="#FFFFCC">Pending...</td>
                <td class="costapproval" align="center" bgcolor="#FFFFCC">Pending...</td>
              	<td class="customerapproval" align="center" bgcolor="#FFFFFF">
				<?php echo ($row['intStatus']=='1'?'Approved':'Pending');?></td>
              	<td class="view" align="center" bgcolor="#FFFFFF"><a target="_blank" href="<?php echo "../Report/sampleReport.php?no=$no&year=$year&revNo=$revNo";?>">More</a></td>
                </tr>
              <?php 
        	   } 
       		  ?>
             </tbody>
             </table>
            </div></div>
              </td>
              </tr>
            </table>
          	</td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><img src="images/Tclose.jpg" width="92" height="24" /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>

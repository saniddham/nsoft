<?php 

//ini_set('display_errors',0);
	session_start();
	//ini_set('max_execution_time', 11111111) ;
	
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];

	//require_once	"{$backwardseperator}class/error_handler.php";					$error_handler 			= new error_handler();
	include_once "{$backwardseperator}class/tables/mst_customer.php";
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	
	/////////// parameters /////////////////////////////
	$requestType 			= $_REQUEST['requestType'];
	
	$sampleNo 				= $_REQUEST['sampleNo'];
	$sampleYear 			= $_REQUEST['sampleYear'];
	$revisionNo 			= $_REQUEST['revisionNo'];
	$revisionNo_p 			= $_REQUEST['revisionNo'];
	$marketer 				= $_REQUEST['marketer'];
	$date 					= $_REQUEST['date'];
	//$deliveryDate			= $_REQUEST['deliveryDate'];
	$reqNoArr 				= explode('/',$_REQUEST['reqNoArr']);
	$reqNo					= (empty($reqNoArr) && $reqNoArr[0]!='')?$reqNoArr[0]:'null';
	$reqYear				= (sizeof($reqNoArr)>1 && $reqNoArr[1]!='')?$reqNoArr[1]:'null';
	$graphicNo 				= trim($_REQUEST['graphicNo']);
	$customerId 			= $_REQUEST['customerId'];
	$styleNo 				= trim($_REQUEST['styleNo']);
	$brandId 				= $_REQUEST['brandId'];
	$sampleQty 				= isset($_REQUEST['sampleQty'])?val($_REQUEST['sampleQty']):'null';
	$grade 					= isset($_REQUEST['grade'])?val($_REQUEST['grade']):'null';
	$fabricType 			= isset($_REQUEST['fabricType'])?$_REQUEST['fabricType']:'null';
	$verivide_d65 			= ($_REQUEST['verivide_d65']=='true'?'1':'0');
	$verivide_tl85 			= ($_REQUEST['verivide_tl85']=='true'?'1':'0');
	$verivide_cw 			= ($_REQUEST['verivide_cw']=='true'?'1':'0');
	$verivide_f 			= ($_REQUEST['verivide_f']=='true'?'1':'0');
	$verivide_uv 			= ($_REQUEST['verivide_uv']=='true'?'1':'0');
	$macbeth_dl 			= ($_REQUEST['macbeth_dl']=='true'?'1':'0');
	$macbeth_cw 			= ($_REQUEST['macbeth_cw']=='true'?'1':'0');
	$macbeth_inca 			= ($_REQUEST['macbeth_inca']=='true'?'1':'0');
	$macbeth_tl84 			= ($_REQUEST['macbeth_tl84']=='true'?'1':'0');
	$macbeth_uv 			= ($_REQUEST['macbeth_uv']=='true'?'1':'0');
	$macbeth_horizon 		= ($_REQUEST['macbeth_horizon']=='true'?'1':'0');
	
	$curing_temp 			= val($_REQUEST['curing_temp']);
	$curing_speed 			= val($_REQUEST['curing_speed']);
	$press_temp 			= val($_REQUEST['press_temp']);
	
	$press_pressure 		= val($_REQUEST['press_pressure']);
	$press_time 			= val($_REQUEST['press_time']);
	$meshCount 				= $_REQUEST['meshCount'];
	$instruction 			= quote($_REQUEST['instruction']);
	$instructionTech		= quote($_REQUEST['instructionTech']);
	
	//echo $_REQUEST['arrCombo'];
	$arrCombo 		= json_decode($_REQUEST['arrCombo'], true);
//print_r($arrCombo);

//die();

try{

		$db->begin();

	///find the stage 1st or 3rd /////////////////////////////////////////////////////
		$sql = "SELECT
				trn_sampleinfomations.intMarketingStatus,
				trn_sampleinfomations.intMarketingApproveLevelStart,
				intTechnicalOpenUser,
				intSampleRoomOpenUser
			FROM trn_sampleinfomations
			WHERE
				trn_sampleinfomations.intSampleNo =  '$sampleNo' AND
				trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
				trn_sampleinfomations.intRevisionNo =  '$revisionNo'
			";
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$intMarketingStatus = $row['intMarketingStatus'];
		$intTechnicalOpenUser = $row['intTechnicalOpenUser'];
		$intSampleRoomOpenUser	= $row['intSampleRoomOpenUser'];
		$intMarketingApproveLevelStart = $row['intMarketingApproveLevelStart'];
		

	$insert = false;
	if($requestType=='save')
	{
		$db->begin();
	
		if($sampleNo==''){
			$newRecord = true;
			$sampleNo 	= getNextSampleNo();
			$sampleYear = date('Y');
			$revisionNo	= 0;
			$insert = true;
		}
		else
		{
			if(($intMarketingStatus==1 && $intTechnicalOpenUser) || ($intMarketingStatus == 1 && $intSampleRoomOpenUser))
			{
				$revisionNo = getNextRevisionNo ($sampleNo,$sampleYear);
				$insert = true;
			}
		}
		
		//backup response  -2017-09-15 (to find not saving problem)
		$res=$_REQUEST['arrCombo'];
		$sql_r = "INSERT INTO trn_sampleinformation_response_backup (`SAMPLE_NO`,`YEAR`,`REVISION`,`RESPONSE`,TIME)  
            VALUES ('$sampleNo','$sampleYear','$revisionNo','$res',now())
			 ";
		$result_r = $db->RunQuery2($sql_r);
		//---------------------------------------------------------
		
		//$arr_graphic	= get_graphic_status($sampleNo,$sampleYear,$graphicNo);
		//if($arr_graphic['status']==1)
		//throw new Exception('This graphic already exists in sample '.$arr_graphic['details']);
		
		$marketingApproveLevels = (int)getApproveLevel2('Sample Infomations Sheet - Marketing')+1;
		$technicalApproveLevels = (int)getApproveLevel2('Sample Infomations Sheet - Technical')+1;
		$sampleApproveLevels	= (int)getApproveLevel2('Sample Infomations Sheet - Sample Room')+1;
		
		$arr_duplicate_graphic	= get_duplicate_graphic_details($sampleNo,$sampleYear,$graphicNo);
		if($arr_duplicate_graphic['status']==1){
			throw new Exception("Graphic no can't be duplicated. Already exists in the sample ".$arr_duplicate_graphic['details']); 
		}
		
		
		if($insert)
		{
			$sql = "INSERT INTO `trn_sampleinfomations` 
					(`intSampleNo`,
					`intSampleYear`,
					`intRevisionNo`,
					`dtDate`,
					`intRequisitionNo`,
					`intRequisitionYear`,
					`strGraphicRefNo`,
					`intCustomer`,
					`intMarketer`,
					`strStyleNo`,
					`intBrand`,
					`verivide_d65`,
					`verivide_tl84`,
					`verivide_cw`,
					`verivide_f`,
					`verivide_uv`,
					`macbeth_dl`,
					`macbeth_cw`,
					`macbeth_inca`,
					`macbeth_tl84`,
					`macbeth_uv`,
					`macbeth_horizon`,
					`dblCuringCondition_temp`,
					`dblCuringCondition_beltSpeed`,
					`dblPressCondition_temp`,
					`dblPressCondition_pressure`,
					`dblPressCondition_time`,
					`strMeshCount`,
					`strAdditionalInstructions`,
					`strAdditionalInstructionsTech`,
					intStatus,
					intMarketingStatus,
					intCompanyid,
					intMarketingApproveLevelStart,
					intTechnicalApproveLevelStart,
					intSampleRoomApproveLevelStart,
					intCreator,
					dtmCreateDate
					) 
					VALUES ('$sampleNo',
					'$sampleYear',
					'$revisionNo',
					'$date',
					$reqNo,
					$reqYear,
					'$graphicNo',
					'$customerId',
					'$marketer',
					'$styleNo',
					'$brandId',
					 $verivide_d65,
					 $verivide_tl85,
					 $verivide_cw,
					 $verivide_f,
					 $verivide_uv,
					 $macbeth_dl,
					 $macbeth_cw,
					 $macbeth_inca,
					 $macbeth_tl84,
					 $macbeth_uv,
					 $macbeth_horizon,
					 $curing_temp,
					'$curing_speed',
					'$press_temp',
					'$press_pressure',
					'$press_time',
					'$meshCount',
					'$instruction',
					'$instructionTech',
					4,
					$marketingApproveLevels,
					$companyId,
					$marketingApproveLevels,
					$technicalApproveLevels,
					$sampleApproveLevels,
					$userId,now())";
                        
                       
			$sql_block="select ACTIVE_SAMPLE from mst_customer where mst_customer.intId= $customerId";	
                        $result_block=$db->RunQuery2($sql_block);
                        $row_block=mysqli_fetch_array($result_block);
                            
                        $sample=$row_block['ACTIVE_SAMPLE'];
                          //  $bulk=$row_block['ACTIVE_BULK'];
                            
                        if($sample!=1){
			$result = $db->RunQuery2($sql);
			if(!$result)
                        throw new Exception('Failed to save sample information header'.$db->errormsg);}
                        else
                            {
                        throw new Exception('failed to save because customer is blocked'.$db->errormsg); 
                            
                            }
		
		}
		else
		{
			$sql = "UPDATE `trn_sampleinfomations`
			 		SET 
					`intRequisitionNo`=$reqNo,
					`intRequisitionYear`=$reqYear,
					`strGraphicRefNo`='$graphicNo',
					`intCustomer`='$customerId',
					`intMarketer`='$marketer',
					`strStyleNo`='$styleNo',
					`intBrand`='$brandId',
					`verivide_d65`='$verivide_d65',
					`verivide_tl84`='$verivide_tl85',
					`verivide_cw`='$verivide_cw',
					`verivide_f`='$verivide_f',
					`verivide_uv`='$verivide_uv',
					`macbeth_dl`='$macbeth_dl',
					`macbeth_cw`='$macbeth_cw',
					`macbeth_inca`='$macbeth_inca',
					`macbeth_tl84`='$macbeth_tl84',
					`macbeth_uv`='$macbeth_uv',
					`macbeth_horizon`='$macbeth_horizon',
					`dblCuringCondition_temp`='$curing_temp',
					`dblCuringCondition_beltSpeed`='$curing_speed',
					`dblPressCondition_temp`='$press_temp',
					`dblPressCondition_pressure`='$press_pressure',
					`dblPressCondition_time`='$press_time',
			 		`strMeshCount`='$meshCount',
					`strAdditionalInstructions`='$instruction' ,
					`strAdditionalInstructionsTech`='$instructionTech' ,
					 intMarketingUser = '$userId',dtmMerketingDate=now(),
					 intMarketingStatus=$intMarketingApproveLevelStart,
					 intStatus  = (4)
					WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevisionNo`='$revisionNo')  ";
                        	
                        
                       
                            
                       
                         $sql_block="select ACTIVE_SAMPLE from mst_customer where mst_customer.intId= $customerId";	
                        $result_block=$db->RunQuery2($sql_block);
                        $row_block=mysqli_fetch_array($result_block);
                            
                        $sample=$row_block['ACTIVE_SAMPLE'];
                          //  $bulk=$row_block['ACTIVE_BULK'];
                            
                        if($sample!=1){
			$result = $db->RunQuery2($sql);
			if(!$result)
                        throw new Exception('Failed to update sample information header'.$db->errormsg);
                        
                        }
                        else
                            {
                        throw new Exception('failed to update because customer is blocked'.$db->errormsg); 
                            
                            }
                            
                        
				//$result = $db->RunQuery2($sql);
				//if(!$result)
				//throw new Exception('Failed to update sample information header');
                        
                        
                            
                            
		}

		
					$arrPart 		= json_decode($_REQUEST['arrPart'], true);
					$e=1;
					$sql = "DELETE FROM `trn_sampleinfomations_printsize` WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevisionNo`='$revisionNo')";
					$result2 = $db->RunQuery2($sql);
					//if(!$result2)
					//throw new Exception('Failed to save sample print sizes');
					$p=0;
					foreach($arrPart as $arrP)
					{
						$p++;
						$partId 			= $arrP['partId'];
						$size_w 			= $arrP['size_w'];
						$size_h 			= $arrP['size_h'];
						$printN 			= 'print '.$e++;
						$sql = "INSERT INTO 
											`trn_sampleinfomations_printsize` 
									(`intSampleNo`,`intSampleYear`,`intRevisionNo`,`strPrintName`,`intWidth`,
										`intHeight`,`intPart`,`intCompanyId`) 
									VALUES ('$sampleNo','$sampleYear','$revisionNo','$printN','$size_w','$size_h','$partId','$companyId')";
						$result2 = $db->RunQuery2($sql);
						if(!$result2)
						throw new Exception('Failed to save sample print sizes');
					}
					if($p==0)
					throw new Exception('Failed to save sample print sizes');
					
					
			$x=0;
			$sql = "DELETE FROM `trn_sampleinfomations_details` WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevNo`='$revisionNo') ";
			$res	= $db->RunQuery2($sql);
			//if(!$res)
			//throw new Exception('Failed to save sample information details');
			
			foreach($arrCombo as $comboa)
			{
				//echo 1;
				$printMode 			= isset($comboa['printMode'])?$comboa['printMode']:'';
				$washStanderd 		= isset($comboa['washStanderd'])?$comboa['washStanderd']:'';
				$comboName			= isset($comboa['comboName'])?$comboa['comboName']:'';
				$groundColor 		= isset($comboa['groundColor'])?$comboa['groundColor']:'';
				$printName 			= isset($comboa['printName'])?$comboa['printName']:'';
				$techniqueId 		= isset($comboa['techniqueId'])?$comboa['techniqueId']:'';
				$fabricType 		= isset($comboa['fabricType'])?$comboa['fabricType']:'';
				
				//for($m=0;$m<count($arrCombo);$m++)
				//{
				$arrColTech	=array();
				$x=0;
				foreach($comboa['gridColMarketingDetails'] as $arrColTech)
				{

					$colorId			= null($comboa['gridColMarketingDetails'][$x]['colorId']);
					$techId_p			= null($comboa['gridColMarketingDetails'][$x]['techId_p']);

						$y=0; 
						//if($comboName=='c1' && $printName=='print 1' && $colorId=='11851' && $techId_p=='4') 
						//print_r($arrCombo['gridColMarketingDetails'][$x]['marketingItems']);
						foreach($comboa['gridColMarketingDetails'][$x]['marketingItems'] as $arrMarketing)
						{
							//if($comboName=='c1' && $printName=='print 1' && $colorId=='11851' && $techId_p=='4') 
							$itemId		= null($arrColTech['marketingItems'][$y]['itemId']);
							$qty		= null($arrColTech['marketingItems'][$y]['qty']);
							$w			= null($arrColTech['marketingItems'][$y]['w']);
							$h			= null($arrColTech['marketingItems'][$y]['h']);
							
						//if($comboName=='c1' && $printName=='print 1' && $colorId=='5097' && $techId_p=='353') 
						//echo "/".$itemId;
							$y++;
						
					 $sql = "INSERT INTO `trn_sampleinfomations_details` (`intSampleNo`,`intSampleYear`,`intRevNo`,`strPrintName`,`strComboName`,`intColorId`,intPrintMode,intWashStanderd,intGroundColor,intFabricType,`intTechniqueId`,`intItem`,`dblQty`,`size_w`,`size_h`,intCompanyId) VALUES ('$sampleNo','$sampleYear','$revisionNo','$printName','$comboName',$colorId ,$printMode,$washStanderd,$groundColor,$fabricType,$techId_p,$itemId,$qty,$w,$h,$companyId)";
					
					$result2 = $db->RunQuery2($sql);
					if(!$result2)
						throw new Exception('Failed to save sample information details');
					
					}
					$x++;
				if($y==0)
				throw new Exception('Failed to save sample details');
				}
			}
		//////////////////////////////////////////////////////////////////////
		//2016-03-14
		if($insert){ // IF REVISE
		//copy technical details in revision
		   /* $sqlm			= "SELECT DISTINCT
								trn_sampleinfomations_details_technical.strPrintName,
								trn_sampleinfomations_details_technical.strComboName,
								trn_sampleinfomations_details_technical.intColorId,
								trn_sampleinfomations_details_technical.intInkTypeId
								FROM `trn_sampleinfomations_details_technical`
								WHERE
								trn_sampleinfomations_details_technical.intSampleNo = '$sampleNo' AND
								trn_sampleinfomations_details_technical.intSampleYear = '$sampleYear' AND
								trn_sampleinfomations_details_technical.intRevNo = '$revisionNo'
								";	
			$result_s	= $db->RunQuery2($sqlm);
			while($rowm=mysqli_fetch_array($result_s))
			{
				$printName 	= $rowm['strPrintName'];
				$comboName 	= $rowm['strComboName']; 
				$inkType 	= $rowm['intInkTypeId'];
				$colour 	= $rowm['intColorId']; */
				
				 $sql_tech = "INSERT INTO `trn_sampleinfomations_details_technical` (`intSampleNo`,`intSampleYear`,`intRevNo`,`strPrintName`,`strComboName`,`intColorId`,`intInkTypeId`,`intNoOfShots`,`intItem`,`dblColorWeight`) (select '$sampleNo','$sampleYear','$revisionNo',`strPrintName`,`strComboName`,`intColorId`,`intInkTypeId`,`intNoOfShots`,`intItem`,`dblColorWeight` from trn_sampleinfomations_details_technical WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevNo`='$revisionNo_p') /* AND (`strPrintName`='$printName') AND (`strComboName`='$comboName') AND (`intColorId`='$colour')  AND (`intInkTypeId`='$inkType') */ )";
//				//die();
			$result_tech = $db->RunQuery2($sql_tech);
		//	}
		//	
		//copy recipes details in revision
		    $sqlm			= "SELECT
								trn_sampleinfomations_details_technical.strPrintName,
								trn_sampleinfomations_details_technical.strComboName,
								trn_sampleinfomations_details_technical.intColorId,
								trn_sampleinfomations_details_technical.intInkTypeId
								FROM `trn_sampleinfomations_details_technical`
								WHERE
								trn_sampleinfomations_details_technical.intSampleNo = '$sampleNo' AND
								trn_sampleinfomations_details_technical.intSampleYear = '$sampleYear' AND
								trn_sampleinfomations_details_technical.intRevNo = '$revisionNo'
								";	
 			$result_s	= $db->RunQuery2($sqlm);

			while($rowm=mysqli_fetch_array($result_s))
			{

				$printName 	= $rowm['strPrintName']; 
				$comboName 	= $rowm['strComboName']; 
				$inkType 	= $rowm['intInkTypeId'];
				$colour 	= $rowm['intColorId'];
				
				  $sql_tech = "INSERT INTO `trn_sample_color_recipes` 
			(`intSampleNo`,`intSampleYear`,`intRevisionNo`,`strCombo`,`strPrintName`,`intColorId`,`intTechniqueId`,
				`intInkTypeId`,`intItem`,`dblWeight`,`intUser`,`dtDate`) (SELECT '$sampleNo','$sampleYear','$revisionNo',`strCombo`,`strPrintName`,`intColorId`,`intTechniqueId`,
				`intInkTypeId`,`intItem`,`dblWeight`,`intUser`,`dtDate` FROM trn_sample_color_recipes where intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revisionNo_p' and strCombo='$combo' and strPrintName='$printName' and intColorId='$colour'  and intInkTypeId='$inkType' )";
				//die();
				$result_tech = $db->RunQuery2($sql_tech);
			}
		//	
		
			$sql = "INSERT INTO 
								`trn_sampleinfomations_printsize` 
						(`intSampleNo`,`intSampleYear`,`intRevisionNo`,`strPrintName`,`intWidth`,
							`intHeight`,`intPart`,`intCompanyId`) 
						 (select '$sampleNo','$sampleYear','$revisionNo',`strPrintName`,`intWidth`,
							`intHeight`,`intPart`,`intCompanyId` from trn_sampleinfomations_printsize WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevNo`='$revisionNo_p') )";
			$result2 = $db->RunQuery2($sql);
		
		
		//copy routing and process
		
	$sql	="SELECT
				trn_sampleinfomations_details.intSampleNo,
				trn_sampleinfomations_details.intSampleYear,
				trn_sampleinfomations_details.intRevNo,
				trn_sampleinfomations_details.strComboName,
				trn_sampleinfomations_details.strPrintName
				FROM `trn_sampleinfomations_details`
				WHERE
				trn_sampleinfomations_details.intSampleNo = '$sampleNo' AND
				trn_sampleinfomations_details.intSampleYear = '$sampleYear' AND
				trn_sampleinfomations_details.intRevNo = '$revisionNo_p' 
				GROUP BY
				trn_sampleinfomations_details.intSampleNo,
				trn_sampleinfomations_details.intSampleYear,
				trn_sampleinfomations_details.intRevNo,
				trn_sampleinfomations_details.strPrintName,
				trn_sampleinfomations_details.strComboName
				";
	$result = $db->RunQuery2($sql);
	$tobeSaved = 0;
	$saved = 0;
	while($rout=mysqli_fetch_array($result))
	{

	//foreach($arr as $rout)
	//{
		$tobeSaved++;
		//$sampleNo 	= $rout['sampNo'];
		//$sampleYear	= $rout['sampYear'];
		//$revisionNo	= $rout['rev'];
		$combo 		= $rout['strComboName'];
		$print 		= $rout['strPrintName'];
		//$routing	= $rout['id'];
		//$ups   		= $rout['ups'];
 	
				 //--------------------------------------------  
 				 $sql_r = "INSERT INTO trn_sampleinfomations_combo_print_routing (`SAMPLE_NO`,`SAMPLE_YEAR`,`REVISION`,`COMBO`,`PRINT`,`ROUTING`,`NO_OF_UPS`)  
            SELECT `SAMPLE_NO`,`SAMPLE_YEAR`,'$revisionNo','$combo','$print',`ROUTING`,`NO_OF_UPS` 
              FROM trn_sampleinfomations_combo_print_routing 
             WHERE SAMPLE_NO = '$sampleNo' AND 
			 SAMPLE_YEAR = '$sampleYear' AND
			 REVISION = '$revisionNo_p' AND
			 COMBO = '$combo' AND
			 PRINT = '$print' 
			 ";
				//alert($sql);
				$result_r = $db->RunQuery2($sql_r);
				
				if($result_r)
				{
				$saved++;
				}	
				//-------------------------------------------
 				$sql_p = "INSERT INTO `trn_sampleinfomations_combo_print_details_processes`(`SAMPLE_NO`,`SAMPLE_YEAR`,`REVISION`,`COMBO`,`PRINT`,`ROUTING`,`PROCESS`,`ORDER_BY`) 
				SELECT 
`SAMPLE_NO`,`SAMPLE_YEAR`,'$revisionNo','$combo','$print',`ROUTING`,`PROCESS`,`ORDER_BY`	FROM trn_sampleinfomations_combo_print_details_processes 
             WHERE SAMPLE_NO = '$sampleNo' AND 
			 SAMPLE_YEAR = '$sampleYear' AND
			 REVISION = '$revisionNo_p' AND
			 COMBO = '$combo' AND
			 PRINT = '$print' 	";
				$result_p = $db->RunQuery2($sql_p);
				
				if($result_p)
				{
				$saved++;
				}	
		}//end of details

 		
			//coppy non direct RM-----------------------------------------------
			$sql_nd="INSERT INTO trn_sample_non_direct_rm_consumption 
			(
				SAMPLE_NO,
				SAMPLE_YEAR,
				REVISION_NO,
				ITEM,
				CONSUMPTION
			)
			(
				SELECT
				trn_sample_non_direct_rm_consumption.SAMPLE_NO,
				trn_sample_non_direct_rm_consumption.SAMPLE_YEAR,
				$revisionNo,
				trn_sample_non_direct_rm_consumption.ITEM,
				trn_sample_non_direct_rm_consumption.CONSUMPTION
				FROM `trn_sample_non_direct_rm_consumption`
				WHERE
				trn_sample_non_direct_rm_consumption.SAMPLE_NO = '$sampleNo' AND
				trn_sample_non_direct_rm_consumption.SAMPLE_YEAR = '$sampleYear' AND
				trn_sample_non_direct_rm_consumption.REVISION_NO = '$revisionNo_p'
			)";
			$result_nd = $db->RunQuery2($sql_nd);
			//------------------------------------------------------------------
		
		
		}
		
		$bulk_order_for_prevoius_rev = get_bulk_order_for_pre_revision($sampleNo,$sampleYear,$revisionNo);
		
		//////////////////////////////////////////////////////////////////////
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
			$response['sampleNo'] 	= $sampleNo;
			$response['sampleYear'] = $sampleYear;
			$response['revisionNo'] = $revisionNo;
			$response['bulk_order'] = $bulk_order_for_prevoius_rev[0];
			$response['bulk_order_status']	= $bulk_order_for_prevoius_rev[1];
			$response['ordersCount'] 		= $bulk_order_for_prevoius_rev[2];
			//////////////// set sample no ////////////////////////////////
			
			$sql = "SELECT DISTINCT
						trn_sampleinfomations.intSampleNo
					FROM trn_sampleinfomations
					WHERE
						trn_sampleinfomations.intSampleYear =  '$sampleYear'
					order by intSampleNo
					";
			$result = $db->RunQuery2($sql);
			$sampleNoOption='<option value=""></option>';
			while($row=mysqli_fetch_array($result))
			{
				$sampleNoOption.= "<option value=\"".$row['intSampleNo']."\">".$row['intSampleNo']."</option>";
			}
			$response['sampleNoOption'] = $sampleNoOption;
			///////////////////////////////////////////////////////////////
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
			throw new Exception($db->errormsg);
		}
	}

	
/*	/////////// location update part /////////////////////
	else if($requestType=='edit')
	{
		$sql = "UPDATE `mst_colors` SET 	strCode		='$code',
											strName		='$name',
											strRemark	='$remark',
											intStatus	='$intStatus',
											intModifyer	='$userId'
				WHERE (`intId`='$id')";
		$result = $db->RunQuery2($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}*/
	/////////// location delete part /////////////////////
	else if($requestType=='saveColor')
	{
		$db->begin();
	
		$colorName = $_REQUEST['colorName'];
	$sql = "INSERT INTO `mst_colors` (`strCode`,`strName`,`intCreator`,`dtmCreateDate`) VALUES ('$colorName','$colorName','$userId',now())";
		$result = $db->RunQuery2($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
			throw new Exception($db->errormsg);
		}
	}
	
	
	$db->commit();
	echo json_encode($response);
	//$db->disconnect();	
	
	
}
catch(Exception $e)
{	
	switch ($e->getCode()){
		case 0:
			$db->rollback();		
			$response['msg'] 	=  $e->getMessage();
			//$response['error'] 	=  $error_handler->jTraceEx($e);
			$response['type'] 	=  'fail';
			//$response['sql']	=  $db->getSql();
			break;		
		case 1:
			$db->commit();
			$response['msg'] 	=  $e->getMessage();
			$response['pass'] 	=  'fail';
			break;
	}
	echo json_encode($response);
	//$db->disconnect();
	
}
	
	
	
	
	function getNextSampleNo()
	{
		global $db;
		global $companyId;
		$sql = "SELECT
				sys_no.intSampleNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$companyId'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextSampleNo = $row['intSampleNo'];
		
		$sql = "UPDATE `sys_no` SET intSampleNo=intSampleNo+1 WHERE (`intCompanyId`='$companyId')  ";
		$db->RunQuery2($sql);
		
		return $nextSampleNo;
	}
	
	function getNextRevisionNo($sampleNo,$sampleYear)
	{
		global $db;
		global $companyId;	
		
		$sql = "SELECT
				Max(trn_sampleinfomations.intRevisionNo)+1 as nextRevNo
				FROM trn_sampleinfomations
				WHERE
				trn_sampleinfomations.intSampleNo =  '$sampleNo' AND
				trn_sampleinfomations.intSampleYear =  '$sampleYear' 
				
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextRevNo = $row['nextRevNo'];
		
		return $nextRevNo;
	}
	
	function deleteAllDataForThisRevision($sampleNo,$sampleYear,$revisionNo)
	{
		global $db;
		global $companyId;	
		
		$sql = "DELETE FROM `trn_sampleinfomations` WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevisionNo`='$revisionNo') and intCompanyId=$companyId";	
		$result 	= $db->RunQuery2($sql);
		
	}
	
	function sampleStatusIsPending($sampleNo,$sampleYear,$revisionNo)
	{
		global $db;
		global $companyId;	
		
		$sql = "SELECT
					trn_sampleinfomations.intMarketingStatus
					FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleNo =  	'$sampleNo' AND
					trn_sampleinfomations.intSampleYear =  	'$sampleYear' AND
					trn_sampleinfomations.intRevisionNo =  	'$revisionNo'
				";	
		$result 	= $db->RunQuery2($sql);	
		$row = mysqli_fetch_array($result);
		if($row['intMarketingStatus']!=1)
			return true;
		else
			return false;
		
	}
function get_bulk_order_for_pre_revision($sample,$sampleYear,$revisionNo){
	
		global $db;
		$sql 		= "SELECT
						group_concat(trn_orderdetails.intOrderNo,'/',trn_orderdetails.intOrderYear) AS orderNo,
					    count(trn_orderdetails.intOrderNo) as ordersCount,	group_concat(if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected',if(tb1.intStatus=-10,'Completed',if(tb1.intStatus=-2,'Cancel',if(tb1.intStatus=-1,'Revised','Pending')))))) as Status
						FROM
						trn_orderdetails
						INNER JOIN trn_orderheader AS tb1 ON trn_orderdetails.intOrderYear = tb1.intOrderYear AND trn_orderdetails.intOrderNo = tb1.intOrderNo
						WHERE
						trn_orderdetails.intSampleNo = '$sample' AND
						trn_orderdetails.intSampleYear = '$sampleYear' AND
						trn_orderdetails.intRevisionNo < '$revisionNo'
						";	
		$result 	= $db->RunQuery2($sql);	
		$row 		= mysqli_fetch_array($result);
		//if($row['orderNo']!='')
			$arr[0]= $row['orderNo'];
			$arr[1]= $row['Status'];
			$arr[2]= $row['ordersCount'];
	//else
			//return false;
	return $arr;
	
}

function get_graphic_status($sampleNo,$sampleYear,$graphicNo){
		
		global $db;
	
		$samp		=$sampleNo.'/'.$sampleYear;
	
		$sql		="SELECT
		concat(trn_sampleinfomations.intSampleNo,'/',trn_sampleinfomations.intSampleYear) as intSampleNo
		FROM `trn_sampleinfomations`
		WHERE
		trim(trn_sampleinfomations.strGraphicRefNo) = trim('$graphicNo') AND
		concat(trn_sampleinfomations.intSampleNo,'/',trn_sampleinfomations.intSampleYear) <> '$samp'
		limit 1";	
		$result 	= $db->RunQuery2($sql);	
		$row 		= mysqli_fetch_array($result);
	
		if($row['intSampleNo']!='' && $row['intSampleNo'] > 0)
			$flag=1;
		else
			$flag=0;
 			
			$arr['status'] = $flag;
			$arr['details']= $row['intSampleNo'];
	//else
			//return false;
	return $arr;
}

function get_duplicate_graphic_details($sampleNo,$sampleYear,$graphicNo){
		
		global $db;
	
	$samp		=$sampleNo.'/'.$sampleYear;
	$sql		="SELECT
	concat(trn_sampleinfomations.intSampleNo,'/',trn_sampleinfomations.intSampleYear,'/',trn_sampleinfomations.intRevisionNo) as intSampleNo
	FROM `trn_sampleinfomations`
	WHERE
	trim(trn_sampleinfomations.strGraphicRefNo) = trim('$graphicNo') AND
	concat(trn_sampleinfomations.intSampleNo,'/',trn_sampleinfomations.intSampleYear) <> '$samp'
	limit 1";	
	$result 	= $db->RunQuery2($sql);	
	$row 		= mysqli_fetch_array($result);

	if($row['intSampleNo']!='' && $row['intSampleNo'] > 0)
		$flag=1;
	else
		$flag=0;
	
	$arr['status'] = $flag;
	$arr['details']= $row['intSampleNo'];
	
	return $arr;
}

?>








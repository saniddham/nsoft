<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$sessionUserId = $_SESSION['userId'];
$thisFilePath =  $_SERVER['PHP_SELF'];
$locationId		= $_SESSION["CompanyID"];
//$formName		  	= 'frmItem';
include  	"{$backwardseperator}dataAccess/permisionCheck.inc";


$sampleNo 	= $_REQUEST['sampleNo'];
$sampleYear = $_REQUEST['sampleYear'];
$revNo		= $_REQUEST['revNo'];
/////////////////////////// get max rev no //////////////////////
$sql = "SELECT
			Max(trn_sampleinfomations.intRevisionNo) as maxRevNo
		FROM trn_sampleinfomations
		WHERE
			trn_sampleinfomations.intSampleNo =  '$sampleNo' AND
			trn_sampleinfomations.intSampleYear =  '$sampleYear'
		";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$maxRevNo = $row['maxRevNo'];
/////////////////////////////////////////////////////////////////

$sql = "SELECT dtDate,strGraphicRefNo,intCustomer,strStyleNo,intBrand,
			verivide_d65,verivide_tl84,verivide_cw,verivide_f,verivide_uv,macbeth_dl,macbeth_cw,macbeth_inca,macbeth_tl84,macbeth_uv,macbeth_horizon,
			dblCuringCondition_temp,dblCuringCondition_beltSpeed,dblPressCondition_temp,dblPressCondition_pressure,dblPressCondition_time,strMeshCount,
			strAdditionalInstructions,strAdditionalInstructionsTech,intMarketingApproveLevelStart,intTechnicalApproveLevelStart,intCompanyId,intStatus,intMarketingStatus,intMarketingApproveLevelStart FROM trn_sampleinfomations 
		WHERE 
			intSampleNo =  '$sampleNo' AND
			intSampleYear =  '$sampleYear' AND
			intRevisionNo =  '$revNo'";

$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$enterDate 				= $row['dtDate'];
	$deliveryDate 			= $row['dtDeliveryDate'];
	$graphicNo 				= $row['strGraphicRefNo'];
	$styleNo 				= $row['strStyleNo'];
	$customerId				= $row['intCustomer'];
	$brandId 				= $row['intBrand'];//intCreator
	$intCreator				= $row['intCreator'];//intCreator
	//$sa = $row['dblSampleQty'];
	//$no = $row['dblDeliverQty'];
	$grade 					= $row['intGrade'];
	$fabricTypeId 			= $row['strFabricType'];
	
	$verivide_d65 			= $row['verivide_d65'];
	$verivide_tl84 			= $row['verivide_tl84'];
	$verivide_cw 			= $row['verivide_cw'];
	$verivide_f 			= $row['verivide_f'];
	$verivide_uv 			= $row['verivide_uv'];

	$macbeth_dl 			= $row['macbeth_dl'];
	$macbeth_cw 			= $row['macbeth_cw'];
	$macbeth_inca 			= $row['macbeth_inca'];
	$macbeth_tl84 			= $row['macbeth_tl84'];
	$macbeth_uv 			= $row['macbeth_uv'];
	$macbeth_horizon 		= $row['macbeth_horizon'];

	$curing_temp			= $row['dblCuringCondition_temp'];
	$curing_beltspeed		= $row['dblCuringCondition_beltSpeed'];
	$press_temp				= $row['dblPressCondition_temp'];
	$press_pressure			= $row['dblPressCondition_pressure'];
	$press_time				= $row['dblPressCondition_time'];
	$strMeshCount			= $row['strMeshCount'];
	//strMeshCount
	$instructions			= $row['strAdditionalInstructions'];
	$instructionsTech			= $row['strAdditionalInstructionsTech'];
	$approveLevel			= $row['intApproveLevelStart'];
	$intCompId 				= $row['intCompanyId'];
	$intStatus 				= $row['intStatus'];
	$intMarketingStatus 	= $row['intMarketingStatus'];
	//$intTechnicalStatus 	= $row['intTechnicalStatus'];//intMarketingApproveLevelStart
	$intMarketingApproveLevelStart 	= $row['intMarketingApproveLevelStart'];//intMarketingApproveLevelStart

}

$sql = "SELECT
			trn_sampleinfomations_printsize.strPrintName,
			trn_sampleinfomations_printsize.intWidth,
			trn_sampleinfomations_printsize.intHeight,
			trn_sampleinfomations_printsize.intPart
		FROM `trn_sampleinfomations_printsize`
		WHERE
			trn_sampleinfomations_printsize.intSampleNo 	=  '$sampleNo' AND
			trn_sampleinfomations_printsize.intSampleYear 	=  '$sampleYear' AND
			trn_sampleinfomations_printsize.intRevisionNo 	=  '$revNo'
		ORDER BY
			trn_sampleinfomations_printsize.strPrintName ASC
		";
$result = $db->RunQuery($sql);
$arrPart;
while($row=mysqli_fetch_array($result))
{
	$arr['printName'] = $row['strPrintName'];
	$arr['width'] = $row['intWidth'];
	$arr['height'] = $row['intHeight'];
	$arr['part'] = $row['intPart'];
	$arrPart[] = $arr;
}

$sql = "SELECT DISTINCT
		trn_sampleinfomations_details.strComboName,
		trn_sampleinfomations_details.intPrintMode,
		trn_sampleinfomations_details.intWashStanderd,
		trn_sampleinfomations_details.intGroundColor,
		trn_sampleinfomations_details.intFabricType
		FROM trn_sampleinfomations_details
		WHERE
		trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
		trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
		trn_sampleinfomations_details.intRevNo 		=  '$revNo'
		";
$result = $db->RunQuery($sql);
$arrCombo;
while($row=mysqli_fetch_array($result))
{
	$arr['combo'] 		= $row['strComboName'];
	$arr['printMode'] 	= $row['intPrintMode'];
	$arr['groundColor'] = $row['intGroundColor'];
	$arr['wash'] 		= $row['intWashStanderd'];
	$arr['fabricType'] 	= $row['intFabricType'];
	$arrCombo[] 		= $arr;
}

////////////// type of print //////////////
/*$sql = "SELECT DISTINCT
			trn_sampleinfomations_combos.strComboName,
			trn_sampleinfomations_combos.strPrintName,
			trn_sampleinfomations_combos.intTypeOfPrintId,
			mst_typeofprint.strName as typeName
		FROM `trn_sampleinfomations_combos`
		Inner Join mst_typeofprint ON mst_typeofprint.intId = trn_sampleinfomations_combos.intTypeOfPrintId
		WHERE
			trn_sampleinfomations_combos.intSampleNo 	=  '$sampleNo' AND
			trn_sampleinfomations_combos.intSampleYear 	=  '$sampleYear' AND
			trn_sampleinfomations_combos.intRevisionNo 	=  '$revNo'
		";*/
 $sql = "SELECT
		trn_sampleinfomations_details.strPrintName,
		trn_sampleinfomations_details.strComboName,
		trn_sampleinfomations_details.intColorId,
		trn_sampleinfomations_details.intItem AS marketing_itemId,
		mst_colors.strName AS colorName,
		mst_typeofprint.strName AS typeOfPrintName,
		trn_sampleinfomations_details.intTypeOfPrint AS typeOfPrintId,
		marketingItem.strName AS marketing_itemName,
		trn_sampleinfomations_details.dblQty,
		trn_sampleinfomations_details.size_w,
		trn_sampleinfomations_details.size_h
		FROM
		trn_sampleinfomations_details
		Inner Join mst_colors ON mst_colors.intId = trn_sampleinfomations_details.intColorId
		Left Join mst_typeofprint ON mst_typeofprint.intId = trn_sampleinfomations_details.intTypeOfPrint
		Left Join mst_item AS marketingItem ON marketingItem.intId = trn_sampleinfomations_details.intItem
		
		WHERE
		trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
		trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
		trn_sampleinfomations_details.intRevNo =  '$revNo'
		";
$result = $db->RunQuery($sql);
$arrType=array();
$arr=array();
while($row=mysqli_fetch_array($result))
{
	$arr['combo'] 				= $row['strComboName'];
	$arr['printName'] 			= $row['strPrintName'];
	
	$arr['intColorId'] 			= $row['intColorId'];
	$arr['marketing_itemId'] 	= $row['marketing_itemId'];
	$arr['colorName'] 			= $row['colorName'];
	$arr['typeOfPrintName'] 	= $row['typeOfPrintName'];
	$arr['typeOfPrintId'] 		= $row['typeOfPrintId'];
	$arr['marketing_itemName'] 	= $row['marketing_itemName'];
	$arr['dblQty'] 				= $row['dblQty'];
	
	$arr['size_w'] 					= $row['size_w'];
	$arr['size_h'] 					= $row['size_h'];

	$arrType[] 			= $arr;
}
//print_r($arrType);
 $sql = "SELECT DISTINCT
		trn_sampleinfomations_combos.strComboName,
		trn_sampleinfomations_combos.strPrintName,
		trn_sampleinfomations_combos.intTypeOfPrintId,
		trn_sampleinfomations_combos.intTechniqueId,
		mst_technique.strName as techName,
		trn_sampleinfomations_combos.intQty,
		trn_sampleinfomations_combos.intItemId,
		mst_technique.intType as spRM
		FROM
		trn_sampleinfomations_combos
		Inner Join mst_technique ON mst_technique.intId = trn_sampleinfomations_combos.intTechniqueId
		
		WHERE
		trn_sampleinfomations_combos.intSampleNo =  '$sampleNo' AND
		trn_sampleinfomations_combos.intSampleYear =  '$sampleYear' AND
		trn_sampleinfomations_combos.intRevisionNo =  '$revNo'
		";
$result = $db->RunQuery($sql);
$arrTech;
while($row=mysqli_fetch_array($result))
{
	$arr['combo'] 		= $row['strComboName'];
	$arr['printName'] 	= $row['strPrintName'];
	$arr['typeId'] 		= $row['intTypeOfPrintId'];
	$arr['techId'] 		= $row['intTechniqueId'];
	$arr['techName'] 	= $row['techName'];
	$arr['qty'] 		= $row['intQty'];
	$arr['itemId'] 		= $row['intItemId'];
	$arr['spRM'] 		= $row['spRM'];
	
	$arrTech[] 			= $arr;
}


$sql = "SELECT
		trn_sampleinfomations_colors.strComboName,
		trn_sampleinfomations_colors.strPrintName,
		trn_sampleinfomations_colors.intColor,
		mst_colors.strName as colorName
		FROM `trn_sampleinfomations_colors`
		Inner Join mst_colors ON mst_colors.intId = trn_sampleinfomations_colors.intColor
		WHERE
		trn_sampleinfomations_colors.intSampleNo =  '$sampleNo' AND
		trn_sampleinfomations_colors.intSampleYear =  '$sampleYear' AND
		trn_sampleinfomations_colors.intRevisionNo =  '$revNo'
		";
$result = $db->RunQuery($sql);
$arrColor;
while($row=mysqli_fetch_array($result))
{
	$arr['combo'] 		= $row['strComboName'];
	$arr['printName'] 	= $row['strPrintName'];
	$arr['colorId'] 		= $row['intColor'];
	$arr['colorName'] 		= $row['colorName'];
	$arrColor[] 			= $arr;
}



////////////////////////// check confirm permision //////////////////////////
	if($intMarketingStatus>1)
		$x = ($intMarketingApproveLevelStart+1)-$intMarketingStatus;
	else
		$x = ($intMarketingApproveLevelStart+1)-$intStatus;
//	echo $x;
	$sql = "	SELECT
					menupermision.int{$x}Approval as approval
				FROM menupermision Inner Join menus ON menus.intId = menupermision.intMenuId
				WHERE
					menus.strCode =  'P0022' AND
					menupermision.intUserId =  '$sessionUserId'
			";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	$userPermision = $row['approval'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<title>Sample Infomations</title>

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript">
var isOldRevNo = '<?php echo ((int)$revNo<(int)$maxRevNo?'1':'0') ?>';
</script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="sampleInfomations-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>



<style type="text/css">
.printName {
	font-family: "Arial Black", Gadget, sans-serif;
	font-size: 16px;
	font-style: normal;
	font-weight: bold;
	color: #009;
	text-align:center
}
.statusWithBorder {	font-family: "Courier New", Courier, monospace;
	border: thin solid #666;
	font-size: 18px;
}
</style></head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../../libraries/calendar/theme.css" />
<script src="../../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<form id="frmSampleInfomations" name="frmSampleInfomations" method="post" action="">
<div align="center">
		<div class="trans_layoutXL">
		  <div class="trans_text">Sample Infomations</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="86%" border="0" align="center">
      <tr>
        <td width="100%" colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="22" colspan="5" align="center" valign="middle" ><table bgcolor="#FEF1C0" width="76%" border="0" cellpadding="0" cellspacing="0" class="normalfnt">
              
              <tr>
                <td width="8%"  class="normalfnt">1st Stage (Marketing)</td>
                <?php if($intMarketingStatus==1){ ?>
                <td width="12%" align="center" bgcolor="#D9F4FD"  class="statusWithBorder" >Approved</td>
                <?php }else if($intMarketingStatus>1){?>
                <td width="10%" align="center" bgcolor="#CC9900"  class="statusWithBorder" ><b>Pending</b></td>
                <?php }else{?>
                <td width="1%" align="center"   class="" >&nbsp;</td>
                <?php }?>
                
                <td width="1%">&nbsp;</td>
                <td width="8%">2nd Stage (Technical)</td>
                <?php if($intStatus==1){ ?>
                <td width="12%" align="center" bgcolor="#D9F4FD"  class="statusWithBorder" >Approved</td>
                <?php }else if($intStatus>1) {?>
                <td width="10%" align="center" bgcolor="#CC9900"  class="statusWithBorder" ><b>Pending</b></td>
                <?php }else {?>
                <?php }?>
                <td width="3%" align="center"   class="" >&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          <tr>
            <td width="12%" height="22" class="normalfnt">&nbsp;</td>
            <td width="21%">&nbsp;</td>
            <td width="6%">&nbsp;</td>
            <td width="10%" class="normalfnt">&nbsp;</td>
            <td width="51%">&nbsp;</td>
            </tr>
          <tr>
            <td height="28" colspan="5" class="normalfnt"><table bgcolor="#E3F4FD" width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td height="28" bgcolor="#FFFFFF">&nbsp;</td>
                <td class="normalfnt">Sample Year</td>
                <td class="normalfnt">Graphic No</td>
                <td class="normalfnt">Sample No</td>
                <td class="normalfnt">Revision No</td>
                <td class="normalfnt">Date</td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
              </tr>
              <tr>
                <td width="7%" height="28" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="6%" bgcolor="#FFFFFF"><select name="cboYear" id="cboYear" style="width:70px">
                  <?php
				    $d = date('Y');
				  	for($d;$d>=2012;$d--)
					{
						if($d==$sampleYear)
							echo "<option selected=\"selected\" id=\"$d\" >$d</option>";	
						else
							echo "<option id=\"$d\" >$d</option>";	
					}
				  ?>
                </select></td>
                <td width="9%" bgcolor="#FFFFFF"><select name="cboStyleNo" id="cboStyleNo" style="width:160px">
                  <option value=""></option>
                  <?php
					//$d = date('Y');
				    $sql = "SELECT DISTINCT
								trn_sampleinfomations.strGraphicRefNo
							FROM trn_sampleinfomations
							ORDER BY
								trn_sampleinfomations.strGraphicRefNo ASC
					";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$strGraphicRefNo = $row['strGraphicRefNo'];
						if($graphicNo==$strGraphicRefNo)
							echo "<option selected=\"selected\" value=\"$strStyle\">$strGraphicRefNo</option>";
						else
							echo "<option value=\"$strGraphicRefNo\">$strGraphicRefNo</option>";
					}
				?>
                </select></td>
                <td width="7%" bgcolor="#FFFFFF"><select name="cboSampleNo" id="cboSampleNo" style="width:120px">
                  <option value=""></option>
                  <?php
					$d = date('Y');
				    $sql = "SELECT DISTINCT
							trn_sampleinfomations.intSampleNo
							FROM trn_sampleinfomations
						WHERE
							trn_sampleinfomations.intSampleYear =  '$d' and intCompanyId = '$locationId'
						ORDER BY
							trn_sampleinfomations.intSampleNo ASC
					";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$no = $row['intSampleNo'];
						if($no==$sampleNo)
							echo "<option selected=\"selected\" value=\"$no\">$no</option>";
						else
							echo "<option value=\"$no\">$no</option>";
					}
				?>
                  </select></td>
                <td width="22%" bgcolor="#FFFFFF" class="normalfnt"><select name="cboRevisionNo" id="cboRevisionNo" style="width:100px">
                  <option value=""></option>
                  <?php
				  	$sql = "SELECT
					trn_sampleinfomations.intRevisionNo
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations.intSampleNo =  '$sampleNo'
				ORDER BY
					trn_sampleinfomations.intRevisionNo ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($revNo==$row['intRevisionNo'])
						echo "<option selected=\"selected\" value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
					else
						echo "<option value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
				}
				  ?>
                </select></td>
                <td width="22%" bgcolor="#FFFFFF" class="normalfnt"><input name="dtDate" type="text" disabled="disabled" class="txtbox" id="dtDate" style="width:98px;"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);" value="<?php echo ($enterDate==''?date("Y-m-d"):$enterDate); ?>"/>
                  <input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                <td width="6%" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="21%" bgcolor="#FFFFFF">&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          <tr>
            <td height="19" colspan="5" class="normalfnt">&nbsp;</td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Graphic Ref NO</td>
            <td colspan="4"><input value="<?php echo $graphicNo; ?>" name="txtGraphicRefNo" type="text" class="validate[required,maxSize[200]]" id="txtGraphicRefNo" style="width:570px" /></td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Customer</td>
            <td colspan="4"><select name="cboCustomer" id="cboCustomer" class="validate[required]" style="width:570px">
              <option value=""></option>
              <?php
					$sql = "select intId,strName from mst_customer where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($customerId==$row['intId'])
							echo "<option selected value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select></td>
            </tr>
          <tr>
            <td height="27" class="normalfnt">Style No</td>
            <td colspan="4"><input value="<?php echo $styleNo; ?>"  name="txtStyleNo" type="text" class="validate[maxSize[50]]"  id="txtStyleNo" style="width:570px" /></td>
            </tr>
          <tr>
            <td height="27" class="normalfnt">Brand</td>
            <td colspan="4"><select class="validate[required]" name="cboBrand" id="cboBrand" style="width:570px">
              <?php
				$sql = "SELECT
						mst_brand.intId,
						mst_brand.strName
					FROM
						mst_customer_brand
						Inner Join mst_brand ON mst_brand.intId = mst_customer_brand.intBrandId
					WHERE
						mst_customer_brand.intCustomerId =  '$customerId' AND
						mst_brand.intStatus =  '1'
					ORDER BY
						mst_brand.strName ASC
						";
				$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($brandId==$row['intId'])
							echo "<option selected value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
			?>
              </select></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td colspan="2"><select  name="cboTechnique" id="cboTechnique" style="width:100px;display:none">
          <option value=""></option>
          <?php
					$sql = "select intId,strName from mst_washstanderd where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						//echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
          </select><select class="colors"  name="cboColor" id="cboColor" style="width:100px;display:none">
            <option value=""></option>
            <?php
					$sql = "select intId,strName from mst_colors where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
      </tr>
      <tr>
        <td id="tdPlaceMainGrid" colspan="2">
        <?php 
					
			$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strComboName
					FROM trn_sampleinfomations_details
					WHERE
					trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_details.intRevNo =  '$revNo'
					";
			$result = $db->RunQuery($sql);
			$rowsCount = mysqli_num_rows($result);
			
			$sql = "SELECT DISTINCT
						strPrintName
					FROM trn_sampleinfomations_printsize
					WHERE
						intSampleNo 	=  '$sampleNo' AND
						intSampleYear 	=  '$sampleYear' AND
						intRevisionNo 	=  '$revNo'
			";
			$result = $db->RunQuery($sql);
			$cellCount = mysqli_num_rows($result);
			
		?>     
        <table id="tblMain" width="74%" border="0" align="left"  class="grid"  >
          <tr class="">
            <td height="152" bgcolor="#FFFFFF" class="normalfnt"  >&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF"><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:264px;height:148px;overflow:hidden" ><?php
			if($sampleNo!='')
			{
				echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"../../../../../documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revNo-1.png\" />";	
			}
			 ?></div>
              <img align="right" class="removeColumn mouseover" src="../../../../../images/del.png" width="15" height="15" /></td>
            <?php
				///////////  img cell loop 
				for($i=1;$i<$cellCount;$i++)
				{
					$l=$i+1;
			?>
            	<td align="center" bgcolor="#FFFFFF"><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:264px;height:148px;overflow:hidden" ><?php

		echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"../../../../../documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revNo-$l.png\" />";	
			
			 ?></div>
            	  <img align="right" class="removeColumn mouseover" src="../../../../../images/del.png" width="15" height="15" /></td>
            <?php	
				}
			?>
            <td bgcolor="#FFFFFF" ><img src="../../../../../images/insert.png" name="butInsertCol" width="78" height="24" class="mouseover" id="butInsertCol" /></td>
          </tr>
          <tr class="">
            <td height="18" bgcolor="#FFFFFF" class="normalfnt" >&nbsp;</td>
            <td align="center" style="width:300px" bgcolor="#FFFFFF" class="printName" ><span class="printNameSpan">Print 1</span><span  class="normalfntMid">
              <select  class="part validate[required]" name="cboPart1" id="cboPart1" style="width:180px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_part where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrPart[0]['part'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select>
              <input name="textfield" type="text" class="sizeW validate[required,custom[number],max[1000],min[0]]" id="sizeW1" style="width:40px" value="<?php echo $arrPart[0]['width']; ?>" />
              <span class="normalfntGrey">W
              <input name="textfield2" type="text" class="sizeH validate[required,custom[number],max[1000],min[0]]" id="sizeH1" style="width:40px" value="<?php echo $arrPart[0]['height']; ?>" />
              H(inch)</span></span></td>
              <?php
			  	///////// print name cell loop
				for($i=1;$i<$cellCount;$i++)
				{
			  ?>
              		<td align="center" bgcolor="#FFFFFF" class="printName" ><span class="printNameSpan">Print <?php echo ($i+1); ?></span><span  class="normalfntMid">
              <select  class="part validate[required]" name="cboPart2" id="cboPart2" style="width:180px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_part where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					$e=1;
					while($row=mysqli_fetch_array($result))
					{
						
						if($row['intId']==$arrPart[$e]['part'])
							echo "<option selected value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select>
              <input name="textfield" type="text" class="sizeW validate[required,custom[number],max[1000],min[0]]" id="sizeW2" style="width:40px" value="<?php echo $arrPart[$i]['width']; ?>" />
              <span class="normalfntGrey">W
              <input name="textfield2" type="text" class="sizeH validate[required,custom[number],max[1000],min[0]]" id="sizeH2" style="width:40px" value="<?php echo $arrPart[$i]['height']; ?>" />
              H(inch)</span></span></td>
              
              <?php
				}
			  ?>
              
            <td bgcolor="#FFFFFF" >&nbsp;</td>
          </tr>
          <tr class="">
            <td height="10" bgcolor="#CCCCCC" class="gridHeader" ><input align="left" class="validate[required,maxSize[20]]" value="<?php echo $arrCombo[0]['combo']; ?>" style="width:80px" type="text" name="txtCombo1" id="txtCombo1" />
              <img align="right" class="removeRow2 mouseover" src="../../../../../images/del.png" width="15" height="15" /></td>
            <td align="center" bgcolor="#FFFFFF" class="printName" >&nbsp;</td>
            <?php
				for($i=1;$i<$cellCount;$i++)
				{
			?>
            <td bgcolor="#FFFFFF" >&nbsp;</td>
            <?php
				}
			?>
             <td bgcolor="#FFFFFF" >&nbsp;</td>
          </tr>
          <tr class="dataRow">
            <td width="18%"  bgcolor="#FFFFFF" >
              <select  name="cboPrintMode2" class="printMode validate[required]" id="cboPrintMode2" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_printmode where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[0]['printMode'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
                </select>
              <select class="washStanderd validate[required]" name="butWashingStanderd2" id="butWashingStanderd3" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_washstanderd where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[0]['wash'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
					}
				?>
                </select>
              <select class="colors validate[required]" name="cboGroundColor2" id="cboGroundColor2" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_colors_ground where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[0]['groundColor'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
					}
				?>
                </select>
              <select class="colors validate[required]" name="cboFabricType" id="cboFabricType" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_fabrictype where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[0]['fabricType'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select></td>
            <td width="67%"  bgcolor="#FFFFFF" style="padding-top:0px" ><table id="tblGrid2" class="tblGrid2" bgcolor="#006666" cellpadding="0" cellspacing="1" width="650" border="0">
              <tr class="normalfntMid">
                <td width="70" bgcolor="#CEE8FB">Color<img  src="../../../../../images/add_new.png" name="butColors" width="15" height="15" class="mouseover butColors" id="butColors" /></td>
                <td width="100" bgcolor="#CEE8FB">Technique</td>
                <td width="100" bgcolor="#CEE8FB">Item</td>
                <td width="40" bgcolor="#CEE8FB">Qty</td>
                <td width="86" bgcolor="#CEE8FB">Size(inch)</td>
                <td bgcolor="#FFE9D2"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                  <tr class="normalfnt">
                    <td  width="40%" bgcolor="#DFFFDF">Ink Type</td>
                    <td width="13%" bgcolor="#DFFFDF">Shots</td>
                    <td width="35%" bgcolor="#DFFFDF">Item</td>
                    <td width="12%" bgcolor="#DFFFDF">Weight</td>
                  </tr>
                </table></td>
                </tr>
                <?php
				foreach($arrType as $arrT)
				{
					//echo $arrT['combo'];
					//echo $arrCombo[0]['combo'];
					if($arrT['combo']==$arrCombo[0]['combo'] && $arrT['printName']=='print 1')
					{
/*						
							$arr['combo'] 				= $row['strComboName'];
							$arr['printName'] 			= $row['strPrintName'];
							
							$arr['intColorId'] 			= $row['intColorId'];
							$arr['marketing_itemId'] 	= $row['marketing_itemId'];
							$arr['colorName'] 			= $row['colorName'];
							$arr['typeOfPrintName'] 	= $row['typeOfPrintName'];
							$arr['typeOfPrintId'] 		= $row['typeOfPrintId'];
							$arr['marketing_itemName'] 	= $row['marketing_itemName'];
							$arr['dblQty'] 				= $row['dblQty'];
							
							$arr['size_w'] 					= $row['size_w'];
							$arr['size_h'] 					= $row['size_h'];
							$arr['technique_techId'] 		= $row['technique_techId'];
							$arr['technique_techName'] 		= $row['technique_techName'];
							$arr['technique_intNoOfShots'] 	= $row['technique_intNoOfShots'];
							$arr['technique_itemId'] 		= $row['technique_itemId'];
							$arr['technique_itemName'] 		= $row['technique_itemName'];
							$arr['technique_dblColorWeight']= $row['technique_dblColorWeight'];*/
				?>
                <tr>
                  <td bgcolor="#ffffffff" class="normalfnt" id="<?php echo $arrT['intColorId'] ?>"><img src="../../../../../images/del.png" class="mouseover removeRow" /><?php echo $arrT['colorName'] ?></td>
                  <td bgcolor="#ffffffff" class="normalfnt" ><select id="cboTechnique" name="select4" class="validate[required] cboItem" style="width:100px">
                    <option value=""></option>
                    <?php
			  	$sql = "SELECT mst_typeofprint.intId, mst_typeofprint.strName FROM mst_typeofprint WHERE mst_typeofprint.intStatus =  '1' order by strName
						";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
			 
              		if($row['intId']==$arrT['typeOfPrintId'])
              			echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
					else
						echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
              
				}
			  ?>
                  </select></td>
                  <td bgcolor="#ffffffff"><select id="cboItem" name="select" class="cboItem" style="width:100px">
                    <option value=""></option>
                    <?php
			  	$sql = "SELECT
							mst_item.intId,
							mst_item.strName
						FROM `mst_item`
						WHERE
							mst_item.intStatus =  '1' and intMainCategory=1
						ORDER BY
							mst_item.strName ASC
						";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
			 
              		if($row['intId']==$arrT['marketing_itemId'])
              			echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
					else
						echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
              
				}
			  ?>
                  </select></td>
                  <td bgcolor="#ffffffff">
                    <input class="validate[custom[number]]"  style="width:40px" value="<?PHP echo ($arrT['dblQty']<=0?'':$arrT['dblQty']); ?>"  type="text" name="textfield5" id="txtQty" />
                 </td>
                  <td bgcolor="#ffffffff" class="normalfnt"><input value="<?PHP echo ($arrT['size_w']<=0?'':$arrT['size_w']); ?>" style="width:20px"  type="text" name="textfield3" id="txtSizeW" class="validate[custom[number]]" />
                    W
                      <input class="validate[custom[number]]" style="width:20px" value="<?PHP echo ($arrT['size_h']<=0?'':$arrT['size_h']); ?>"  type="text" name="textfield4" id="txtSizeH" />
                      H</td>
                  <td bgcolor="#ffffffff"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                    <?php 
					$sql = "	SELECT
									trn_sampleinfomations_details_technical.intTechnique,
									trn_sampleinfomations_details_technical.intNoOfShots,
									trn_sampleinfomations_details_technical.intItem,
									trn_sampleinfomations_details_technical.dblColorWeight,
									mst_technique.strName AS techName,
									mst_item.strName AS itemName
								FROM
								trn_sampleinfomations_details_technical
									left Join mst_technique ON mst_technique.intId = trn_sampleinfomations_details_technical.intTechnique
									left Join mst_item ON mst_item.intId = trn_sampleinfomations_details_technical.intItem
								WHERE
									trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
									trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
									trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND
									trn_sampleinfomations_details_technical.strPrintName 	=  '".$arrT['printName']."' AND
									trn_sampleinfomations_details_technical.strComboName 	=  '".$arrT['combo']."' AND
									trn_sampleinfomations_details_technical.intColorId 		=  '".$arrT['intColorId']."'
								";
						$result_tech = $db->RunQuery($sql);
						while($row_tech=mysqli_fetch_array($result_tech))
						{
					?>
                    <tr class="normalfnt">
                      <td width="40%" bgcolor="#FFFFFF"><?php echo $row_tech['techName']; ?></td>
                      <td width="13%" bgcolor="#FFFFFF"><?php echo $row_tech['intNoOfShots']; ?></td>
                      <td width="35%" bgcolor="#FFFFFF"><?php echo $row_tech['itemName']; ?></td>
                      <td width="12%" bgcolor="#FFFFFF"><?php echo $row_tech['dblColorWeight']; ?></td>
                    </tr>
                    <?php
						}
					?>
                  </table></td>
                  </tr>
                <?php
					}
				}
				?>
              </table></td>
                
                <?php
					/////////// first row table cells //////////////
					for($i=1;$i<$cellCount;$i++)
					{
				?>
                <td width="67%"  bgcolor="#FFFFFF" style="padding-top:0px" ><table id="tblGrid2" class="tblGrid2" bgcolor="#006666" cellpadding="0" cellspacing="1" width="650" border="0">
                  <tr class="normalfntMid">
                    <td width="70" bgcolor="#CEE8FB">Color<img  src="../../../../../images/add_new.png" name="butColors" width="15" height="15" class="mouseover butColors" id="butColors2" /></td>
                    <td width="100" bgcolor="#CEE8FB">Technique</td>
                    <td width="100" bgcolor="#CEE8FB">Item</td>
                    <td width="40" bgcolor="#CEE8FB">Qty</td>
                    <td width="86" bgcolor="#CEE8FB">Size</td>
                    <td bgcolor="#FFE9D2"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                      <tr class="normalfnt">
                        <td  width="40%" bgcolor="#DFFFDF">Ink Type</td>
                        <td width="13%" bgcolor="#DFFFDF">Shots</td>
                        <td width="35%" bgcolor="#DFFFDF">Item</td>
                        <td width="12%" bgcolor="#DFFFDF">Weight</td>
                      </tr>
                    </table></td>
                  </tr>
                  <?php
				  
				foreach($arrType as $arrT)
				{
					
					//echo $arrT['combo'];
					//echo $arrCombo[0]['combo'];
					//echo  $arrT['printName'];
					//echo 'print '.($i+1).'<br>';
					if($arrT['combo']==$arrCombo[0]['combo'] && $arrT['printName']=='print '.($i+1))
					{
						
/*						
							$arr['combo'] 				= $row['strComboName'];
							$arr['printName'] 			= $row['strPrintName'];
							
							$arr['intColorId'] 			= $row['intColorId'];
							$arr['marketing_itemId'] 	= $row['marketing_itemId'];
							$arr['colorName'] 			= $row['colorName'];
							$arr['typeOfPrintName'] 	= $row['typeOfPrintName'];
							$arr['typeOfPrintId'] 		= $row['typeOfPrintId'];
							$arr['marketing_itemName'] 	= $row['marketing_itemName'];
							$arr['dblQty'] 				= $row['dblQty'];
							
							$arr['size_w'] 					= $row['size_w'];
							$arr['size_h'] 					= $row['size_h'];
							$arr['technique_techId'] 		= $row['technique_techId'];
							$arr['technique_techName'] 		= $row['technique_techName'];
							$arr['technique_intNoOfShots'] 	= $row['technique_intNoOfShots'];
							$arr['technique_itemId'] 		= $row['technique_itemId'];
							$arr['technique_itemName'] 		= $row['technique_itemName'];
							$arr['technique_dblColorWeight']= $row['technique_dblColorWeight'];*/
				?>
                  <tr>
                    <td bgcolor="#ffffffff" class="normalfnt" id="<?php echo $arrT['intColorId'] ?>"><img src="../../../../../images/del.png" class="mouseover removeRow" /><?php echo $arrT['colorName'] ?></td>
                    <td bgcolor="#ffffffff" class="normalfnt" ><select id="cboTechnique" name="cboTechnique" class="validate[required] cboTechnique" style="width:100px">
                      <option value=""></option>
                      <?php
			  	$sql = "SELECT mst_typeofprint.intId, mst_typeofprint.strName FROM mst_typeofprint WHERE mst_typeofprint.intStatus =  '1' order by strName
						";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
			 
              		if($row['intId']==$arrT['typeOfPrintId'])
              			echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
					else
						echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
              
				}
			  ?>
                    </select></td>
                    <td bgcolor="#ffffffff"><select id="cboItem" name="cboItem" class="cboItem" style="width:100px">
                      <option value=""></option>
                      <?php
			  	$sql = "SELECT
							mst_item.intId,
							mst_item.strName
						FROM `mst_item`
						WHERE
							mst_item.intStatus =  '1' and intMainCategory=1
						ORDER BY
							mst_item.strName ASC
						";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
			 
              		if($row['intId']==$arrT['marketing_itemId'])
              			echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
					else
						echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
              
				}
			  ?>
                    </select></td>
                    <td bgcolor="#ffffffff"><input class="validate[custom[number]]"  style="width:40px" value="<?PHP echo ($arrT['dblQty']<=0?'':$arrT['dblQty']); ?>"  type="text" name="txtQty" id="txtQty" /></td>
                    <td bgcolor="#ffffffff" class="normalfnt"><input class="validate[custom[number]]" value="<?PHP echo ($arrT['size_w']<=0?'':$arrT['size_w']); ?>" style="width:20px"  type="text" name="txtSizeW" id="txtSizeW" />
                      W
                      <input class="validate[custom[number]]" style="width:20px" value="<?PHP echo ($arrT['size_h']<=0?'':$arrT['size_h']); ?>"  type="text" name="txtSizeH" id="txtSizeH" />
                      H</td>
                    <td bgcolor="#ffffffff"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                      <?php 
					$sql = "	SELECT
									trn_sampleinfomations_details_technical.intTechnique,
									trn_sampleinfomations_details_technical.intNoOfShots,
									trn_sampleinfomations_details_technical.intItem,
									trn_sampleinfomations_details_technical.dblColorWeight,
									mst_technique.strName AS techName,
									mst_item.strName AS itemName
								FROM
								trn_sampleinfomations_details_technical
									left Join mst_technique ON mst_technique.intId = trn_sampleinfomations_details_technical.intTechnique
									left Join mst_item ON mst_item.intId = trn_sampleinfomations_details_technical.intItem
								WHERE
									trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
									trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
									trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND
									trn_sampleinfomations_details_technical.strPrintName 	=  '".$arrT['printName']."' AND
									trn_sampleinfomations_details_technical.strComboName 	=  '".$arrT['combo']."' AND
									trn_sampleinfomations_details_technical.intColorId 		=  '".$arrT['intColorId']."'
								";
						$result_tech = $db->RunQuery($sql);
						while($row_tech=mysqli_fetch_array($result_tech))
						{
					?>
                      <tr class="normalfnt">
                        <td width="40%" bgcolor="#FFFFFF"><?php echo $row_tech['techName']; ?></td>
                        <td width="13%" bgcolor="#FFFFFF"><?php echo $row_tech['intNoOfShots']; ?></td>
                        <td width="35%" bgcolor="#FFFFFF"><?php echo $row_tech['itemName']; ?></td>
                        <td width="12%" bgcolor="#FFFFFF"><?php echo $row_tech['dblColorWeight']; ?></td>
                      </tr>
                      <?php
						}
					?>
                    </table></td>
                  </tr>
                  <?php
					}
				}
				?>
                </table></td>
                
                <?php
					}
				?>
            <td width="15%"  bgcolor="#FFFFFF" style="padding-top:0px" >&nbsp;</td>
          </tr>
          <?php
		  	for($mainRow=1;$mainRow<$rowsCount;$mainRow++)
			{
		  ?>
           <tr class="">
            <td height="18" bgcolor="#CCCCCC" class="gridHeader" ><input class="validate[required,maxSize[20]]" value="<?php echo $arrCombo[$mainRow]['combo']; ?>" style="width:80px" type="text" name="txtCombo2" id="txtCombo2" />
              <img align="right" class="removeRow2 mouseover" src="../../../../../images/del.png" width="15" height="15" /></td>
				<?php
                 for($i=1;$i<=$cellCount;$i++)
                 {
                ?>
                <td align="center" bgcolor="#FFFFFF" class="printName" >&nbsp;</td>
                <?php
                }
                ?>
            <td bgcolor="#FFFFFF" >&nbsp;</td>
          </tr>
                    <tr class="dataRow">
            <td width="18%"  bgcolor="#FFFFFF" >
              <select class="printMode validate[required]" name="cboPrintMode2" id="cboPrintMode2" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_printmode where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[$mainRow]['printMode'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
                </select>
              <select class="washStanderd validate[required]" name="butWashingStanderd2" id="butWashingStanderd3" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_washstanderd where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[$mainRow]['wash'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
                </select>
              <select class="colors validate[required]" name="cboGroundColor2" id="cboGroundColor2" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_colors_ground where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[$mainRow]['groundColor'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";		
					}
				?>
                </select>
              <select class="colors validate[required]" name="cboFabricType" id="cboFabricType" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_fabrictype where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[$mainRow]['fabricType'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select></td>
              <?php 
			  	/////////// color grid and technique and type of grid cell ////////////
				for($i=1;$i<=$cellCount;$i++)
                 {
			  ?>
            <td  width="67%"  bgcolor="#FFFFFF" style="padding-top:0px" ><table id="tblGrid2" class="tblGrid2" bgcolor="#006666" cellpadding="0" cellspacing="1" width="650" border="0">
              <tr class="normalfntMid">
                <td width="70" bgcolor="#CEE8FB">Color<img  src="../../../../../images/add_new.png" name="butColors" width="15" height="15" class="mouseover butColors" id="butColors3" /></td>
                <td width="100" bgcolor="#CEE8FB">Technique</td>
                <td width="100" bgcolor="#CEE8FB">Item</td>
                <td width="40" bgcolor="#CEE8FB">Qty</td>
                <td width="86" bgcolor="#CEE8FB">Size</td>
                <td bgcolor="#FFE9D2"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                  <tr class="normalfnt">
                    <td  width="40%" bgcolor="#DFFFDF">Ink Type</td>
                    <td width="13%" bgcolor="#DFFFDF">Shots</td>
                    <td width="35%" bgcolor="#DFFFDF">Item</td>
                    <td width="12%" bgcolor="#DFFFDF">Weight</td>
                  </tr>
                </table></td>
              </tr>
              <?php
			// print_r($arrCombo[0]['combo']);
				foreach($arrType as $arrT)
				{
					//echo $arrT['combo'];
					//echo $arrCombo[$mainRow]['combo'].'<br>';
					if($arrT['combo']==$arrCombo[$mainRow]['combo'] && $arrT['printName']=='print '.$i)
					{
/*						
							$arr['combo'] 				= $row['strComboName'];
							$arr['printName'] 			= $row['strPrintName'];
							
							$arr['intColorId'] 			= $row['intColorId'];
							$arr['marketing_itemId'] 	= $row['marketing_itemId'];
							$arr['colorName'] 			= $row['colorName'];
							$arr['typeOfPrintName'] 	= $row['typeOfPrintName'];
							$arr['typeOfPrintId'] 		= $row['typeOfPrintId'];
							$arr['marketing_itemName'] 	= $row['marketing_itemName'];
							$arr['dblQty'] 				= $row['dblQty'];
							
							$arr['size_w'] 					= $row['size_w'];
							$arr['size_h'] 					= $row['size_h'];
							$arr['technique_techId'] 		= $row['technique_techId'];
							$arr['technique_techName'] 		= $row['technique_techName'];
							$arr['technique_intNoOfShots'] 	= $row['technique_intNoOfShots'];
							$arr['technique_itemId'] 		= $row['technique_itemId'];
							$arr['technique_itemName'] 		= $row['technique_itemName'];
							$arr['technique_dblColorWeight']= $row['technique_dblColorWeight'];*/
				?>
              <tr>
                <td bgcolor="#ffffffff" class="normalfnt" id="<?php echo $arrT['intColorId'] ?>"><img src="../../../../../images/del.png" class="mouseover removeRow" /><?php echo $arrT['colorName'] ?></td>
                <td bgcolor="#ffffffff" class="normalfnt" ><select id="cboTechnique" name="select3" class="validate[required] cboItem" style="width:100px">
                  <option value=""></option>
                  <?php
			  	$sql = "SELECT mst_typeofprint.intId, mst_typeofprint.strName FROM mst_typeofprint WHERE mst_typeofprint.intStatus =  '1' order by strName
						";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
			 
              		if($row['intId']==$arrT['typeOfPrintId'])
              			echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
					else
						echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
              
				}
			  ?>
                </select></td>
                <td bgcolor="#ffffffff"><select name="select3" class="cboItem" id="cboItem" style="width:100px">
                  <option value=""></option>
                  <?php
			  	$sql = "SELECT
							mst_item.intId,
							mst_item.strName
						FROM `mst_item`
						WHERE
							mst_item.intStatus =  '1' and intMainCategory=1
						ORDER BY
							mst_item.strName ASC
						";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
			 
              		if($row['intId']==$arrT['marketing_itemId'])
              			echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
					else
						echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
              
				}
			  ?>
                </select></td>
                <td bgcolor="#ffffffff"><input class="validate[custom[number]]" style="width:40px" value="<?PHP echo ($arrT['dblQty']<=0?'':$arrT['dblQty']); ?>"  type="text" name="textfield7" id="txtQty" /></td>
                <td bgcolor="#ffffffff" class="normalfnt"><input class="validate[custom[number]]" value="<?PHP echo ($arrT['size_w']<=0?'':$arrT['size_w']); ?>" style="width:20px"  type="text" name="textfield7" id="txtSizeW" />
                  W
                  <input class="validate[custom[number]]" style="width:20px" value="<?PHP echo ($arrT['size_h']<=0?'':$arrT['size_h']); ?>"  type="text" name="textfield7" id="txtSizeH" />
                  H</td>
                <td bgcolor="#ffffffff"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                  <?php 
					$sql = "	SELECT
									trn_sampleinfomations_details_technical.intTechnique,
									trn_sampleinfomations_details_technical.intNoOfShots,
									trn_sampleinfomations_details_technical.intItem,
									trn_sampleinfomations_details_technical.dblColorWeight,
									mst_technique.strName AS techName,
									mst_item.strName AS itemName
								FROM
								trn_sampleinfomations_details_technical
									left Join mst_technique ON mst_technique.intId = trn_sampleinfomations_details_technical.intTechnique
									left Join mst_item ON mst_item.intId = trn_sampleinfomations_details_technical.intItem
								WHERE
									trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
									trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
									trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND
									trn_sampleinfomations_details_technical.strPrintName 	=  '".$arrT['printName']."' AND
									trn_sampleinfomations_details_technical.strComboName 	=  '".$arrT['combo']."' AND
									trn_sampleinfomations_details_technical.intColorId 		=  '".$arrT['intColorId']."'
								";
						$result_tech = $db->RunQuery($sql);
						while($row_tech=mysqli_fetch_array($result_tech))
						{
					?>
                  <tr class="normalfnt">
                    <td width="40%" bgcolor="#FFFFFF"><?php echo $row_tech['techName']; ?></td>
                    <td width="13%" bgcolor="#FFFFFF"><?php echo $row_tech['intNoOfShots']; ?></td>
                    <td width="35%" bgcolor="#FFFFFF"><?php echo $row_tech['itemName']; ?></td>
                    <td width="12%" bgcolor="#FFFFFF"><?php echo $row_tech['dblColorWeight']; ?></td>
                  </tr>
                  <?php
						}
					?>
                </table></td>
              </tr>
              <?php
					}
				}
				?>
            </table></td>
            <?php
				 }
			?>
              <td bgcolor="#FFFFFF" >&nbsp;</td>
          </tr>
          <?php
			}
		  ?>
          
          <tr class="dataRow">
            <td  bgcolor="#FFFFFF" ><img src="../../../../../images/insert.png" name="butInsertRow" width="78" height="24" class="mouseover" id="butInsertRow" /></td>
            <td  bgcolor="#FFFFFF" style="padding-top:0px" >&nbsp;</td>
            <?php
				for($i=1;$i<$cellCount;$i++)
				{
			?>
            <td  bgcolor="#FFFFFF" style="padding-top:0px" >&nbsp;</td>
            <?php
				}
			?>
            <td  bgcolor="#FFFFFF" style="padding-top:0px" >&nbsp;</td>
          </tr>
          </table>
          <p>&nbsp;</p></td>
        </tr>
      <tr>
        <td colspan="2" align="center" class=""><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="19" class="normalfnt">&nbsp;</td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td width="12%" height="19" class="normalfnt">Light Box</td>
            <td width="352%" colspan="4"><table width="72%" height="38" border="0" cellpadding="0" cellspacing="1" bgcolor="#CCCCCC">
              <tr class="normalfntMid">
                <td colspan="5" align="center" bgcolor="#FFFFFF">Verivide</td>
                <td colspan="6" align="center" bgcolor="#FFFFFF">Macbeth</td>
                </tr>
                
              <tr class="normalfntMid">
                <td width="6%" bgcolor="#FFFFFF">D65<strong> <br /><input <?php echo ($verivide_d65?'checked':'') ?> type="checkbox" name="txtVerivide_D65" id="txtVerivide_D65" />
                  </strong></td>
                <td width="9%" bgcolor="#FFFFFF">TL84<strong> <br/><input <?php echo ($verivide_tl84?'checked':'') ?> type="checkbox" name="txtVerivide_TL84" id="txtVerivide_TL84" />
                  </strong></td>
                <td width="11%" bgcolor="#FFFFFF">CW<strong> <br/><input <?php echo ($verivide_cw?'checked':'') ?> type="checkbox" name="txtVerivide_CW" id="txtVerivide_CW" />
                  </strong></td>
                <td width="11%" bgcolor="#FFFFFF">F<strong> <br/> <input <?php echo ($verivide_f?'checked':'') ?> type="checkbox" name="txtVerivide_F" id="txtVerivide_F" />
                  </strong></td>
                <td width="11%" bgcolor="#FFFFFF">UV<strong> <br/> <input <?php echo ($verivide_uv?'checked':'') ?> type="checkbox" name="txtVerivide_UV" id="txtVerivide_UV" />
                  </strong></td>
                <td width="8%" bgcolor="#FFFFFF">DL<strong> <br/>  <input <?php echo ($macbeth_dl?'checked':'') ?> type="checkbox" name="txtMacbeth_DL" id="txtMacbeth_DL" />
                  </strong></td>
                <td width="9%" bgcolor="#FFFFFF">CW<strong> <br/>  <input <?php echo ($macbeth_cw?'checked':'') ?> type="checkbox" name="txtMacbeth_CW" id="txtMacbeth_CW" />
                  </strong></td>
                <td width="11%" bgcolor="#FFFFFF">Inca<strong> <br/>  <input <?php echo ($macbeth_inca?'checked':'') ?> type="checkbox" name="txtMacbeth_INCA" id="txtMacbeth_INCA" />
                  </strong></td>
                <td width="8%" bgcolor="#FFFFFF">TL84<strong> <br/>  <input <?php echo ($macbeth_tl84?'checked':'') ?> type="checkbox" name="txtMacbeth_TL84" id="txtMacbeth_TL84" />
                  </strong></td>
                <td width="9%" bgcolor="#FFFFFF">UV<strong> <br/>  <input <?php echo ($macbeth_uv?'checked':'') ?> type="checkbox" name="txtMacbeth_UV" id="txtMacbeth_UV" />
                  </strong></td>
                <td width="7%" bgcolor="#FFFFFF">Horizon<strong> <br/> <input <?php echo ($macbeth_horizon?'checked':'') ?> type="checkbox" name="txtMacbeth_Horizon" id="txtMacbeth_Horizon" />
                  </strong></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td height="45" class="normalfnt">&nbsp;</td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td height="45" colspan="5" class="normalfnt"><table width="762" border="0" class="tableBorder_topRound">
              <tr>
                <td colspan="2" bgcolor="#CCCCCC" class="normalfnt"><strong>Technical Data</strong></td>
                </tr>
              <tr>
                <td width="129" height="47"><span class="normalfnt">Curing Condition</span></td>
                <td width="621"><table width="63%" height="38" border="0" cellpadding="0" cellspacing="0" >
                  <tr>
                    <td width="44%" bgcolor="#FFFFFF" class="normalfntMid"><span class="normalfnt"><strong>TEMP</strong>                      
                      <input name="txtCuringTemp" type="text" disabled="disabled" class=" " id="txtCuringTemp" style="width:170px" value="<?php echo $curing_temp; ?>" />
                    </span></td>
                    <td width="56%" bgcolor="#FFFFFF" class="normalfntMid"><span class="normalfnt"><strong>BELT SPEED
                      <input name="txtCuringBeltSpeed" type="text" disabled="disabled" class=" " id="txtCuringBeltSpeed" style="width:170px" value="<?php echo $curing_beltspeed; ?>" />
                    </strong></span></td>
                    </tr>
                  </table></td>
                </tr>
              <tr>
                <td height="49"><span class="normalfnt">Press Condition</span></td>
                <td><table width="72%" height="38" border="0" cellpadding="0" cellspacing="0" >
                  <tr class="normalfntMid">
                    <td width="6%" bgcolor="#FFFFFF"><span class="normalfnt"><strong>TEMP</strong>                      
                      <input name="txtPressTemp" type="text" disabled="disabled" class="" id="txtPressTemp" style="width:170px" value="<?php echo $press_temp; ?>" />
                    </span></td>
                    <td width="5%" bgcolor="#FFFFFF"><span class="normalfnt"><strong>Pressure
                      <input name="txtPressPressure" type="text" disabled="disabled" class="" id="txtPressPressure" style="width:170px" value="<?php echo $press_pressure; ?>" />
                      </strong></span></td>
                    <td width="6%" bgcolor="#FFFFFF"><span class="normalfnt"><strong>Time
                      <input name="txtPressTime" type="text" disabled="disabled" class="" id="txtPressTime" style="width:170px" value="<?php echo $press_time; ?>" />
                      </strong></span></td>
                    </tr>
                  </table></td>
                </tr>
              <tr>
                <td><span class="normalfnt">Mesh Count</span></td>
                <td><span class="normalfnt">
                  <input name="txtMeshCount" type="text" disabled="disabled" class="" id="txtMeshCount" style="width:570px" value="<?php echo $strMeshCount; ?>" />
                  </span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">Technical Instructions</td>
                <td><textarea disabled="disabled"  name="txtInstructionsTech" id="txtInstructionsTech" cols="89" rows="5"><?php echo $instructionsTech; ?></textarea></td>
              </tr>
              </table></td>
          </tr>
          <tr>
            <td height="23" class="normalfnt">&nbsp;</td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td height="23" class="normalfnt">Additional Instructions</td>
            <td colspan="4"><textarea  name="txtInstructions" id="txtInstructions" cols="89" rows="5"><?php echo $instructions; ?></textarea></td>
          </tr>
          <tr>
            <td height="23" valign="top" class="normalfnt">Upload Inquiry</td>
            <td colspan="4"><iframe id="iframeFiles" src="filesUpload.php?txtFolder=<?php echo "$sampleNo-$sampleYear"; ?>" name="iframeFiles" style="width:400px;height:200px;border:none"  ></iframe></td>
          </tr>
          <tr>
            <td height="23" class="normalfnt">&nbsp;</td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td height="23" class="normalfnt">&nbsp;</td>
            <td colspan="4"><select  class="cboItemDumy" name="cboItemDumy" id="cboItemDumy" style="width:100px;display:none">
              <option value=""></option>
              <?php
					$sql = "select intId,strName from mst_item where intStatus = 1 and intSpecialRm=0 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select>
              <select  class="cboItemDumy" name="cboItemDumySPRM" id="cboItemDumySPRM" style="width:100px;display:none">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_item where intStatus = 1 and intSpecialRm = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select><select  class="cboTechniqueDumy" name="cboTechniqueDumy" id="cboTechniqueDumy" style="width:100px;display:none">
              <option value=""></option>
              <?php
					$sql = "SELECT mst_typeofprint.intId, mst_typeofprint.strName FROM mst_typeofprint WHERE mst_typeofprint.intStatus =  '1' order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2" align="center" class="tableBorder_allRound"><img style="display:none" src="../../../../../images/Tnew.jpg" width="92" height="24" class="mouseover" id="butNew" /><img style="display:none" src="../../../../../images/Tsave.jpg" width="92" height="24" class="mouseover" id="butSave" /><img src="../../../../../images/Tconfirm.jpg" style="display:none" width="92" height="24" class="mouseover" id="butConfirm" /><?php 
		if($intMarketingStatus==1)
		{
		?><a target="_blank" href="../../sampleOrder/addNew/sampleOrder.php?sampleNo=<?php echo $sampleNo; ?>&year=<?php echo $sampleYear; ?>"><img border="0" src="../../../../../images/TsampleOrder.png"  width="92" height="24" class="mouseover" id="butReport2" /></a><?php 
		}
		?><a target="_blank" href="../../Report/sampleReport.php?no=<?php echo $sampleNo; ?>&year=<?php echo $sampleYear; ?>&revNo=<?php echo $revNo; ?>"><?php
        
		if($userPermision && $intMarketingStatus>1 )
		{
		?><img src="../../../../../images/Tconfirm.jpg" width="92" height="24" class="mouseover"  id="butReport" /><?php
		
		}
		?><img src="../../../../../images/Treport.jpg" width="92" height="24" class="mouseover"  id="butReport" /></a><img src="../../../../../planning/img/Tclose.jpg" width="92" height="24" class="mouseover" /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<!-- technique -->
<!--	<div  style="position: absolute;display:none;z-index:100"  id="popupContact1">
    <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="<?php echo $mainPath; ?>presentation/customerAndOperation/masterData/technique/addNew/technique.php" style="width:800;height:400;border:0;overflow:hidden">
    </iframe>
    </div>-->
    
    <!-- print Mode -->
<!--	<div  style="position: absolute;display:none;z-index:100"  id="popupContact2">
    <iframe onload="loadPrintMode();"   id="iframeMain2" name="iframeMain2" src="<?php echo $mainPath; ?>presentation/customerAndOperation/masterData/printMode/addNew/printMode.php" style="width:800;height:400;border:0;overflow:hidden">
    </iframe>
    </div>-->
    
        <!-- washing Condition -->
<!--    <div  style="position: absolute;display:none;z-index:100"  id="popupContact3">
    <iframe onload="loadWasingCondistion();"   id="iframeMain3" name="iframeMain3" src="<?php echo $mainPath; ?>presentation/customerAndOperation/masterData/washStanderd/addNew/washStanderd.php" style="width:800;height:400;border:0;overflow:hidden">
    </iframe>
    </div>-->
    
    <!-- colors -->
<!--    <div  style="position: absolute;display:none;z-index:100"  id="popupContact4">
    <iframe onload="loadColors();"   id="iframeMain4" name="iframeMain4" src="<?php echo $mainPath; ?>presentation/customerAndOperation/masterData/colors/addNew/colors.php" style="width:800;height:400;border:0;overflow:hidden">
    </iframe>
    </div>-->
    
      <!-- Part -->
<!--    <div  style="position: absolute;display:none;z-index:100"  id="popupContact5">
    <iframe onload="loadPart();"   id="iframeMain5" name="iframeMain5" src="<?php echo $mainPath; ?>presentation/customerAndOperation/masterData/part/addNew/part.php" style="width:800;height:400;border:0;overflow:hidden">
    </iframe>
    </div>-->
    
	 <!-- Part -->
<!--    <div  style="position: absolute;display:none;z-index:100"  id="popupContact6">
    <iframe onload="loadTypeOfPrint();"   id="iframeMain6" name="iframeMain6" src="<?php echo $mainPath; ?>presentation/workStudy/masterData/typeOfPrint/addNew/typeOfPrint.php" style="width:800;height:400;border:0;overflow:hidden">
    </iframe>
    </div>
    -->
    <div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
    <div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</body>
</html>

<?php
//BEGIN - SESSION {
session_start();
$session_companyId	= $_SESSION["COMPANYID"];
//END	- SESSION }

//BEGIN - INCLUDE FILES {
require_once ("../../../../../dataAccess/Connector.php");
require_once ("../../../../../libraries/fpdf/fpdf.php");
//END	- INCLUDE FILES }

//BEGIN - CREATE OBJECTS {
//END	- CREATE OBJECTS }

//BEGIN - PARAMETERS {
//$sampleNo = 205019;$sampleYear = 2013;$revisionNo = 1;
$sampleNo 		= $_REQUEST['sampleNo'];
$sampleYear 	= $_REQUEST['sampleYear'];
$revisionNo 	= $_REQUEST['revisionNo'];
//END	- PARAMETERS }

class PDF extends FPDF
{
	function DashedRect($end)
	{
		$s = "";
		for($i=1;$i<$end;$i++)
		{
			$s .= ".";
		}
		return $s;
	}
	
	function Header()
	{
		$this->SetFont('Times','B',20);
		$this->Cell(0,10,'SAMPLE INFORMATION',0,1,'C',0);
	}
	
	function Part1($row)
	{	
		$y	= 8;
        $four = 6;
        $sixteen = 16;
        $eight = 8;



        foreach($row["prints"] as $printKey => $printItem) {
//            echo $item.' is begin with ('.$key.')';


            foreach($row["combos"] as $comboKey => $comboItem) {
//                echo $item.' is begin with ('.$key.')';

#header details
                $this->AddPage();
                $this->SetXY(17,30);
                $this->SetFont('Times','',12);
                $this->Cell(30,7,'Sample No : ','0','0','L');
                $this->SetFont('Times','',8);
                $this->Cell(25,7,$row["sampleNo"],'B','0','L');
                $this->SetFont('Times','',12);
                $this->Cell(30,7,'Rev No : ','0','0','L');
                $this->SetFont('Times','',8);
                $this->Cell(20,7,$row["revisionNo"],'B','0','L');
                $this->SetFont('Times','',12);
                $this->Cell(30,7,'Date : ','0','0','R');
                $this->SetFont('Times','',8);
                $this->Cell(25,7,$row["date"],'B','0','L');

                $this->SetXY(17,$this->GetY()+$y);
                $this->SetFont('Times','',12);
                $this->Cell(35,7,'Requisition No : ','0','0','L');
                $this->SetFont('Times','',8);
                $this->Cell(45,7,$row["conReqNo"],'B','0','L');
                $this->SetFont('Times','',12);
                $this->Cell(35,7,'Marketer : ','0','0','R');
                $this->SetFont('Times','',8);
                $this->Cell(45,7,$row["marketerName"],'B','0','L');

                $this->SetXY(17,$this->GetY()+$y);
                $this->SetFont('Times','',12);
                $this->Cell(35,7,'Graphic No : ','0','0','L');
                $this->SetFont('Times','',8);
                $this->Cell(45,7,$row["graphicNo"],'B','0','L');
                $this->SetFont('Times','',12);
                $this->Cell(35,7,'Client : ','0','0','R');
                $this->SetFont('Times','',8);
                $this->Cell(45,7,$row["customerName"],'B','0','L');

                $this->SetXY(17,$this->GetY()+$y);
                $this->SetFont('Times','',12);
                $this->Cell(35,7,'Style No : ','0','0','L');
                $this->SetFont('Times','',8);
                $this->Cell(45,7,$row["styleNo"],'B','0','L');
                $this->SetFont('Times','',12);
                $this->Cell(35,7,'Brand : ','0','0','R');
                $this->SetFont('Times','',8);
                $this->Cell(45,7,$row["brand"],'B','0','L');


#print details
                $this->Image('../../../../../'.$printItem["imageUrl"],75,$this->GetY()+$y + 5,65,36);



                $this->SetXY(34,$this->GetY()+$sixteen + 36);
                $this->SetFont('Times','B',18);
                $this->Cell(30,7, $printKey,'0','0','L');
                $this->SetFont('Times','',12);
                $this->Cell(20,7, 'Part : ','0','0','L');

                $this->Cell(30,7, $printItem["partName"],'0','0','L');
                $this->Cell(15,7, 'Width : ','0','0','L');
                $this->Cell(15,7, $printItem["intWidth"],'0','0','L');
                $this->Cell(15,7, 'Height : ','0','0','L');
                $this->Cell(15,7, $printItem["intHeight"],'0','0','L');


                        $this->SetXY(17,$this->GetY() + $eight);
                        $this->SetFont('Times','B',18);
                        $this->Cell(47,7, $comboKey,'0','0','L');
                        $this->SetFont('Times','B',12);

                        #combo info
                        $this->SetFont('Times','',12);
                        $this->SetXY(34,$this->GetY()+$y);
                        $this->Cell(30,7, 'Print Mode : ','0','0','L');
                        $this->Cell(40,7,$comboItem["printMode"],'0','0','L');
                        $this->Cell(30,7, 'Wash Type : ','0','0','L');
                        $this->Cell(40,7,$comboItem["wash"],'0','0','L');

                        $this->SetXY(34,$this->GetY()+$four);
                        $this->Cell(30,7, 'Ground Color : ','0','0','L');
                        $this->Cell(40,7,$comboItem["groundColor"],'0','0','L');
                        $this->Cell(30,7, 'Fabric Type : ','0','0','L');
                        $this->Cell(40,7,$comboItem["fabricType"],'0','0','L');

                        #entry info

                        $this->SetXY(17,$this->GetY()+ 8 + 5);
                        $this->Cell(20,7, 'Color','0','0','L');
                        $this->Cell(30,7,"Technique",'0','0','L');
                        $this->Cell(30,7, 'Item','0','0','L');
                        $this->Cell(10,7,"Qty",'0','0','L');
                        $this->Cell(20,7,"Size(inch)",'0','0','L');
                        $this->Cell(30,7, 'Ink Type','0','0','L');
                        $this->Cell(10,7,"Shots",'0','0','L');
                        $this->Cell(15,7, 'Item','0','0','L');
                        $this->Cell(15,7,"Weight",'0','0','L');
                        $this->SetXY(34,$this->GetY()+ 5 );
                        $entryList = $row["entries"][$comboKey.$printKey]["rows"];
                        $this->SetFont('Times','',8);

                        $this->SetFont('Times','',6);
                        for ($index = 0; sizeof($entryList) > $index ; $index++){
                            $entry = $entryList[$index];
                            $this->SetXY(17,$this->GetY()+ 5);
                            $current_x=17;
                            $current_y = $this->GetY();

                            $this->SetXY($current_x, $current_y);
                            $this->MultiCell(20,5,$entry["colorName"],1);
                            $current_x += 20;
                            $this->SetXY($current_x, $current_y);
                            $this->MultiCell(30,5,$entry["techniqueName"],1);
                            $current_x += 30;
                            $this->SetXY($current_x, $current_y);
                            $this->MultiCell(30,5,$entry["itemName"],1);
                            $current_x += 30;
                            $this->SetXY($current_x, $current_y);
                            $this->MultiCell(10,5,$entry["dblQty"],1);
                            $current_x += 10;
                            $this->SetXY($current_x, $current_y);
                            $this->MultiCell(20,5,$entry["size_w"].' W    '.$entry["size_h"].' H',1);
                            $current_x += 20;

                            $inkEntryList = $entry["inkEntries"];

                            if (is_null($inkEntryList) || sizeof($inkEntryList) == 0 ){
                                $this->SetXY($current_x, $current_y);
                                $this->MultiCell(30,5,$entry["2ndtechName"],1);
                                $current_x += 30;
                                $this->SetXY($current_x, $current_y);
                                $this->MultiCell(10,5,$entry["intNoOfShots"],1);
                                $current_x += 10;
                                $this->SetXY($current_x, $current_y);
                                $this->MultiCell(15,5,$entry["2nditemName"],1);
                                $current_x += 15;
                                $this->SetXY($current_x, $current_y);
                                $this->MultiCell(15,5,$entry["dblColorWeight"],1);
                            } else {
                                $inkCurrent_x = $current_x;
                                $inkCurrent_y = $current_y;
                                for ($inkIdex = 0; sizeof($inkEntryList) > $inkIdex ; $inkIdex++){
                                    $inkEntry = $inkEntryList[$inkIdex];
                                    $current_x = $inkCurrent_x;
                                    if ($inkIdex > 0 && $inkIdex){
                                        $current_y = $this->GetY()+ 5;
                                    }
                                    $this->SetXY($current_x, $current_y);
                                    $this->MultiCell(30,5,$inkEntry["2ndtechName"],1);
                                    $current_x += 30;
                                    $this->SetXY($current_x, $current_y);
                                    $this->MultiCell(10,5,$inkEntry["intNoOfShots"],1);
                                    $current_x += 10;
                                    $this->SetXY($current_x, $current_y);
                                    $this->MultiCell(15,5,$inkEntry["2nditemName"],1);
                                    $current_x += 15;
                                    $this->SetXY($current_x, $current_y);
                                    $this->MultiCell(15,5,$inkEntry["dblColorWeight"],1);
                                    $current_x += 15;
                                }
                            }
                        }

                #check boxes
                $lightBoxes = $row["lightBoxes"];
                $this->SetFont('Times','',10);
                $this->SetXY(30,$this->GetY()+$y + 5);
                $checkbox_size = 4;
                $checkBoxGap = 10;

                $this->Cell(($checkbox_size + $checkBoxGap)*5 - 3, 5, "Verivide", 0, 0);
                $this->Cell(($checkbox_size + $checkBoxGap)*6, 5, "Macbeth", 0, 0);


                $this->SetXY(30,$this->GetY()+ 8);
                $this->Cell($checkbox_size, $checkbox_size, "D65", 0, 0);
                $this->SetXY($this->GetX() + $checkBoxGap-3,$this->GetY());
                $this->Cell($checkbox_size, $checkbox_size, "TL84", 0, 0);
                $this->SetXY($this->GetX() + $checkBoxGap,$this->GetY());
                $this->Cell($checkbox_size, $checkbox_size, "CW", 0, 0);
                $this->SetXY($this->GetX() + $checkBoxGap,$this->GetY());
                $this->Cell($checkbox_size, $checkbox_size, "F", 0, 0);
                $this->SetXY($this->GetX() + $checkBoxGap,$this->GetY());
                $this->Cell($checkbox_size, $checkbox_size, "UV", 0, 0);
                $this->SetXY($this->GetX() + $checkBoxGap,$this->GetY());
                $this->Cell($checkbox_size, $checkbox_size, "DL", 0, 0);
                $this->SetXY($this->GetX() + $checkBoxGap,$this->GetY());
                $this->Cell($checkbox_size, $checkbox_size, "CW", 0, 0);
                $this->SetXY($this->GetX() + $checkBoxGap,$this->GetY());
                $this->Cell($checkbox_size, $checkbox_size, "Inca", 0, 0);
                $this->SetXY($this->GetX() + $checkBoxGap,$this->GetY());
                $this->Cell($checkbox_size, $checkbox_size, "TL84", 0, 0);
                $this->SetXY($this->GetX() + $checkBoxGap,$this->GetY());
                $this->Cell($checkbox_size, $checkbox_size, "UV", 0, 0);
                $this->SetXY($this->GetX() + $checkBoxGap,$this->GetY());
                $this->Cell($checkbox_size, $checkbox_size, "Horizon", 0, 0);

                $this->SetXY(30,$this->GetY()+ 5);
                $this->SetFont('ZapfDingbats','', 10);
                if ($lightBoxes["verivide_d65"]){
                    $this->Cell($checkbox_size, $checkbox_size, "4", 1, 0);
                } else {
                    $this->Cell($checkbox_size, $checkbox_size, "", 1, 0);
                }
                $this->SetXY($this->GetX() + $checkBoxGap,$this->GetY());
                if ($lightBoxes["verivide_tl84"]){
                    $this->Cell($checkbox_size, $checkbox_size, "4", 1, 0);
                } else {
                    $this->Cell($checkbox_size, $checkbox_size, "", 1, 0);
                }
                $this->SetXY($this->GetX() + $checkBoxGap,$this->GetY());
                if ($lightBoxes["verivide_cw"]){
                    $this->Cell($checkbox_size, $checkbox_size, "4", 1, 0);
                } else {
                    $this->Cell($checkbox_size, $checkbox_size, "", 1, 0);
                }
                $this->SetXY($this->GetX() + $checkBoxGap,$this->GetY());
                if ($lightBoxes["verivide_f"]){
                    $this->Cell($checkbox_size, $checkbox_size, "4", 1, 0);
                } else {
                    $this->Cell($checkbox_size, $checkbox_size, "", 1, 0);
                }
                $this->SetXY($this->GetX() + $checkBoxGap,$this->GetY());
                if ($lightBoxes["verivide_uv"]){
                    $this->Cell($checkbox_size, $checkbox_size, "4", 1, 0);
                } else {
                    $this->Cell($checkbox_size, $checkbox_size, "", 1, 0);
                }
                $this->SetXY($this->GetX() + $checkBoxGap,$this->GetY());
                if ($lightBoxes["macbeth_dl"]){
                    $this->Cell($checkbox_size, $checkbox_size, "4", 1, 0);
                } else {
                    $this->Cell($checkbox_size, $checkbox_size, "", 1, 0);
                }
                $this->SetXY($this->GetX() + $checkBoxGap,$this->GetY());
                if ($lightBoxes["macbeth_cw"]){
                    $this->Cell($checkbox_size, $checkbox_size, "4", 1, 0);
                } else {
                    $this->Cell($checkbox_size, $checkbox_size, "", 1, 0);
                }
                $this->SetXY($this->GetX() + $checkBoxGap,$this->GetY());
                if ($lightBoxes["macbeth_inca"]){
                    $this->Cell($checkbox_size, $checkbox_size, "4", 1, 0);
                } else {
                    $this->Cell($checkbox_size, $checkbox_size, "", 1, 0);
                }
                $this->SetXY($this->GetX() + $checkBoxGap,$this->GetY());
                if ($lightBoxes["macbeth_tl84"]){
                    $this->Cell($checkbox_size, $checkbox_size, "4", 1, 0);
                } else {
                    $this->Cell($checkbox_size, $checkbox_size, "", 1, 0);
                }
                $this->SetXY($this->GetX() + $checkBoxGap,$this->GetY());
                if ($lightBoxes["macbeth_uv"]){
                    $this->Cell($checkbox_size, $checkbox_size, "4", 1, 0);
                } else {
                    $this->Cell($checkbox_size, $checkbox_size, "", 1, 0);
                }
                $this->SetXY($this->GetX() + $checkBoxGap,$this->GetY());
                if ($lightBoxes["macbeth_horizon"]){
                    $this->Cell($checkbox_size, $checkbox_size, "4", 1, 0);
                } else {
                    $this->Cell($checkbox_size, $checkbox_size, "", 1, 0);
                }


#footer details
                $this->SetFont('Times','',12);
                $this->SetXY(17,$this->GetY()+$y + 5);
                $this->SetFont('Times','',12);
                $this->Cell(46,7,'Curing Temperature : ','0','0','L');
                $this->SetFont('Times','',8);
                $this->Cell(46,7,$row["curingTemp"],'B','0','L');
                $this->SetFont('Times','',12);
                $this->Cell(46,7,'Curing Belt Speed : ','0','0','L');
                $this->SetFont('Times','',8);
                $this->Cell(46,7,$row["curingBeltSpeed"],'B','0','L');

                $this->SetXY(17,$this->GetY()+$y);
                $this->SetFont('Times','',12);
                $this->Cell(46,7,'Pressing Temperature : ','0','0','L');
                $this->SetFont('Times','',8);
                $this->Cell(20,7,$row["pressTemp"],'B','0','L');
                $this->SetFont('Times','',12);
                $this->Cell(33,7,'Pressure : ','0','0','L');
                $this->SetFont('Times','',8);
                $this->Cell(20,7,$row["pressPressure"],'B','0','L');
                $this->SetFont('Times','',12);
                $this->Cell(33,7,'Pressing Time : ','0','0','L');
                $this->SetFont('Times','',8);
                $this->Cell(20,7,$row["pressTime"],'B','0','L');

                $this->SetXY(17,$this->GetY()+$y);
                $this->SetFont('Times','',12);
                $this->Cell(46,7,'Mesh Count : ','0','0','L');
                $this->SetFont('Times','',8);
                $this->Cell(0,7,$row["meshCount"],'B','0','L');

                $this->SetXY(17,$this->GetY()+$y);
                $this->SetFont('Times','',12);
                $this->Cell(46,7,'Technical Instructions : ','0','0','L');
                $this->SetFont('Times','',8);
                $this->Cell(0,7,$row["techInstructions"],'B','0','L');

                $this->SetXY(17,$this->GetY()+$y);
                $this->SetFont('Times','',12);
                $this->Cell(46,7,'Additional Instructions : ','0','0','L');
                $this->SetFont('Times','',8);
                $this->Cell(0,7,$row["additionalInstructions"],'B','0','L');
            }
        }
	}
	function Footer()
	{		
		$this->SetY(-10);
		$this->SetFont('Times','I',10);
		$this->Cell(0,2,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}
}

$pdf 				= new PDF('P','mm','A4');
$sampleInfo  =  $_SESSION["SAMPLE_PDF_DATA"];

$pdf->Part1($sampleInfo);

$pdf->Output('rptSampleInformationSheet_pdf.pdf','I');

function getHeaders($sampleNo,$sampleYear,$revisionNo)
{
	global $db;
	
	$sql = "SELECT
				SI.strGraphicRefNo				AS GRAPHIC_REF_NO,
				SI.strStyleNo					AS STYLE_NO,
				SI.dtDate						AS DATE,
				B.strName 						AS BRAND_NAME,
				C.strName 						AS CUSTOMER_NAME,
				
				SI.dblCuringCondition_temp		AS CURING_CONDITION_TEMP,
				SI.dblCuringCondition_beltSpeed	AS CURING_CONDITION_BELT_SPEED,
				SI.dblPressCondition_temp		AS PRESS_CONDITION_TEMP,
				SI.dblPressCondition_pressure	AS PRESS_CONDITION_PRESSURE,
				SI.dblPressCondition_time		AS PRESS_CONDITION_TIME,
				SI.strMeshCount					AS MESH_COUNT
			FROM
				trn_sampleinfomations AS SI
			LEFT JOIN mst_customer AS C 
				ON C.intId = SI.intCustomer
			LEFT JOIN mst_brand AS B 
				ON B.intId = SI.intBrand
			LEFT JOIN sys_users AS firstUser 
				ON SI.intCreator = firstUser.intUserId
			LEFT JOIN sys_users AS seconduser 
				ON seconduser.intUserId = SI.intTechUser
			WHERE 
				SI.intSampleNO = '$sampleNo' 
				AND SI.intSampleYear = '$sampleYear' 
				AND SI.intRevisionNo = '$revisionNo'";
	$result = $db->RunQuery($sql);
	return mysqli_fetch_array($result);
}

?>
<?php
ini_set("post_max_size", "30M");
ini_set("upload_max_filesize", "30M");
ini_set("memory_limit", -1 );

if($_GET['txtFolder']){
    $folderName 		= 	$_REQUEST['txtFolder'];
    $editMode = isset($_GET['approved'])?$_GET['approved']:null;
    $actual_link = $_SERVER['HTTP_X_FORWARDED_PROTO']."://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

}

if(isset($_POST['txtFolder']) && ($_FILES !='' && $_POST['txtFolder'] !='')){
    $pathName = "../../../../../documents/sampleinfo/docs/$folderName/";
    if (!file_exists($pathName)) {
        mkdir($pathName, 0700);
    }
    $size =  $_FILES["file"]["size"] / 1028 / 1028;
    if ($size <=5)
    {
        if ($_FILES["file"]["error"] > 0)
        {
            //echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        }
        else
        {
            if (preg_match('/[\'^£$%&*}{@#~?><>,|=]/', $_FILES['file']['name']))
            {
                // one or more of the 'special characters' found in $string
                echo $_FILES["file"]["name"]  ." - File Name Contain Special Character Please Rename ";
                return;
            }

            if (file_exists("../../../../../documents/sampleinfo/docs/$folderName/" . $_FILES["file"]["name"]))
            {
                echo $_FILES["file"]["name"] ."- File Already Exist";
                return;
                //if($_FILES["file"]["name"]!='')
                //echo $_FILES["file"]["name"] . " already exists. ";
            }
            else
            {
                move_uploaded_file($_FILES["file"]["tmp_name"],"../../../../../documents/sampleinfo/docs/$folderName/" . $_FILES["file"]["name"]);
                //echo "uploaded successfully.";
                echo $_FILES["file"]["name"] ." - Uploaded successfully";
                return;
            }
        }
    }
    else
    {
        echo "Invalid Size";
        return;
    }

}

/*** REMOVE THIS UPLOAD METHOD AND CONVERT INTO AJAX
 *AUTHOR : Hasitha Charaka
 * Created Date :2017/09/15
 **/
//else
//{
//mkdir("../../../../../documents/sampleinfo/docs/$folderName/",0700);
//$size =  $_FILES["file"]["size"] / 1028 / 1028;
//if ($size <=5)
//  {
//  if ($_FILES["file"]["error"] > 0)
//    {
//   //echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
//    }
//  else
//    {
//
//    if (file_exists("../../../../../documents/sampleinfo/docs/$folderName/" . $_FILES["file"]["name"]))
//      {
//		  //if($_FILES["file"]["name"]!='')
//     	 //echo $_FILES["file"]["name"] . " already exists. ";
//      }
//    else
//      {
//      move_uploaded_file($_FILES["file"]["tmp_name"],"../../../../../documents/sampleinfo/docs/$folderName/" . $_FILES["file"]["name"]);
//      //echo "uploaded successfully.";
//      }
//    }
//  }
//else
//  {
//  echo "Invalid Size";
//  }
//}
//echo $_FILES["file"]["error"];

if((isset($_POST['file_name']) && $_POST['folder_name']) &&($_POST['file_name'] !='' &&($_POST['folder_name']!=''))){

    define('DIR_FILE_PATH', '../../../../../documents/sampleinfo/docs/');
    $fileName = $_POST['file_name'];
    $folderName = $_POST['folder_name'];
    
    $file_path= DIR_FILE_PATH.''.$folderName.'/'.$fileName;
    if (!unlink($file_path))
    {
        // deleteFile();
        echo ("Error deleting $fileName");
        return;
    }
    else
    {
        //  deleteFile();

        echo ("$fileName Deleted successfully ! ");
        return;
    }


}

?>
<span  id="error"></span>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Upload Files</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
</head>

<body>
<form id="frmUpload" name="frmUpload" method="post" action="filesUpload.php" enctype="multipart/form-data">

<table width="355" height="48" border="0" bgcolor="#999999">
  <tr class="normalfnt">
    <td width="67" bgcolor="#99CCCC" class="normalfntMid">No</td>
    <td width="312" bgcolor="#99CCCC" class="normalfntMid">File Name</td>
    <td width="48" bgcolor="#99CCCC" class="normalfntMid">Delete</td>
  </tr>
  <?php
  	//echo "../../../../../documents/sampleinfo/docs/$folderName";
	filesInDir("../../../../../documents/sampleinfo/docs/$folderName",$editMode);
	
				function filesInDir($tdir,$editMode)
				{
					global $folderName;
					$m=0;
					$dirs = scandir($tdir);
					foreach($dirs as $file)
					{
						
						if (($file == '.')||($file == '..'))
						{
						}
						else
						{
							
							if (! is_dir($tdir.'/'.$file))
							{
								
							echo "	<tr bgcolor=\"#ffffff\" class=\"normalfnt\">
										<td>".++$m."</td>
										<td><a target=\"_blank\" href=\"../../../../../documents/sampleinfo/docs/".$folderName."/$file\">$file</a></td>";
                                if($editMode ==1) {
                                    echo "<td><button class='btn' name=\"delete_pdf \" data-toggle=\"tooltip\" title=\"DELETE!\"  onclick=\"deleteFile('$folderName', '$file');\" type=\"button\">Delete</i></button></td>
								 	 </tr>";
                                }else {
                                    echo '</tr>';
                                }

							}
						}
					}
				}
  ?>
  
  <tr>
    <td colspan="2" bgcolor="#FFFFFF"><input type="file" name="file_upload" id="file_upload" />
        <?php if($editMode ==1) { ?>
        <button type="button" name="button" id="button_upload" value="button_upload" >Upload</button></td>
      <?php } ?>
    <td bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF"><input style="visibility:hidden" name="txtFolder" type="text" id="txtFolder" value="<?php echo $folderName; ?>" /></td>
    <td bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
</form>
</body>
<style>
    .btn {

        display: block;
        width: 70px;
        height: 42px;
        background: #fa491e;
        padding: 8px;
        text-align: center;
        border-radius: 27px;
        color: white;
        font-weight: bold;
    }
    .btn:hover{
        cursor: pointer !important;
        background: #bb1715;
    }
</style>
<!--Attachment Upload Script -->
<!-- Authr :Hasitha-->
<script type="text/javascript">
    $('#button_upload').click(function (e) {
        var file_data = $('#file_upload').prop('files')[0];
        var formData = new FormData();
        // var txtFolder = $('#txtFolder').val();
        var txtFolder='<?php echo $folderName; ?>';

        if(txtFolder != '-') {

            if (jQuery('#file_upload').val() == '') {
                $('#error')
                    .html('<span id="error" style=\"color:red\">*Please Select File</span>');
                return false;
            }

            formData.append("file", file_data);
            formData.append("txtFolder", txtFolder);

            $.ajax({
                url: '<?php echo $actual_link ?>',
                type: 'post',
                data: formData,
                mimeType: 'multipart/form-data',
                contentType: false,
                cache: false,
                processData: false,
                dataType: 'html',
                success: function (data) {
                    alert(data) ? "" : location.reload();
                },
                error: function (jqXHR, status, errorThrown) {
                    return false;
                    // alert(errorThrown + "\r\n" + jqXHR.statusText + "\r\n" + jqXHR.responseText);
                }
            })

        }else{
            $('#error')
                .html('<span id="error" style=\"color:red\">*Please Save The Sample Sheet First</span>');
            return false;
        }
    });
</script>
<!-- Attachment Delelte  Script-->
<!--Author : Hasitha -->
<script type="text/javascript">
    function deleteFile(folder_name,file_name){
        if( confirm("Are You Sure !")){
            $.ajax({
                url:'<?php echo $actual_link ?>',
                cache: false,
                type: "POST",
                dataType:'html',
                data: {file_name:file_name,folder_name:folder_name,status:'1'},
                success:function(data) {

                    alert(data) ? "" : window.location.replace('<?php echo $actual_link ?>');
                }
            });


        }
        return false;
    }

</script>
</html>
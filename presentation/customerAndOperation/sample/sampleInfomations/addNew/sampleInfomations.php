<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>

<?php
include_once("class/tables/mst_customer.php");         
$obj_mstCustomer = new mst_customer($db);
$mainPath 		= $_SESSION['mainPath'];
$sessionUserId 	= $sessions->getUserId();
$locationId		= $sessions->getLocationId();
$SRdb			= $_SESSION["SRDatabese"];

$sampleNo 		= (!isset($_REQUEST["sampleNo"])?'':$_REQUEST["sampleNo"]);
$sampleYear 	= (!isset($_REQUEST["sampleYear"])?'':$_REQUEST["sampleYear"]);
$revNo			= (!isset($_REQUEST["revNo"])?'':$_REQUEST["revNo"]);


$reqNo 			= (!isset($_REQUEST["reqNo"])?'':$_REQUEST["reqNo"]);
$reqYear 		= (!isset($_REQUEST["reqYear"])?'':$_REQUEST["reqYear"]);

$graphicNo			= '';
$styleNo			= '';
$instructionsTech 	= '';
$instructions		= '';

if($sampleYear==''){
	$sampleYear=date('Y');	
}

/////////////////////////// get max rev no //////////////////////
///
$sampleInfo = array();
$sampleInfo["sampleNo"] = $sampleNo;
$sampleInfo["sampleYear"] = $sampleYear;
$sql = "SELECT
			Max(trn_sampleinfomations.intRevisionNo) as maxRevNo
		FROM trn_sampleinfomations
		WHERE
			trn_sampleinfomations.intSampleNo =  '$sampleNo' AND
			trn_sampleinfomations.intSampleYear =  '$sampleYear'
		";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$maxRevNo = $row['maxRevNo'];
$sampleInfo["revNo"] = $maxRevNo;
/////////////////////////////////////////////////////////////////

$sql = "SELECT dtDate,CONCAT(intRequisitionNo,'/',intRequisitionYear) as conReqNo,strGraphicRefNo,intCustomer,intMarketer,strStyleNo,intBrand,
			verivide_d65,verivide_tl84,verivide_cw,verivide_f,verivide_uv,macbeth_dl,macbeth_cw,macbeth_inca,macbeth_tl84,macbeth_uv,macbeth_horizon,
			dblCuringCondition_temp,dblCuringCondition_beltSpeed,dblPressCondition_temp,dblPressCondition_pressure,dblPressCondition_time,strMeshCount,
			strAdditionalInstructions,strAdditionalInstructionsTech,intMarketingApproveLevelStart,intTechnicalApproveLevelStart,intCompanyId,intStatus,intMarketingStatus,intTechnicalStatus,intSampleRoomStatus,intMarketingApproveLevelStart,intSampleRoomEnterUser,
			intSampleRoomOpenUser,intTechnicalOpenUser FROM trn_sampleinfomations 
		WHERE 
			intSampleNo =  '$sampleNo' AND
			intSampleYear =  '$sampleYear' AND
			intRevisionNo =  '$revNo'";

$intStatus 						= 0;
$intMarketingStatus 			= 0;
$intMarketingApproveLevelStart	= 0;
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$enterDate 				= $row['dtDate'];
    $sampleInfo["date"] = $enterDate;
	//$deliveryDate 			= $row['dtDeliveryDate'];
	$conReqNo 				= $row['conReqNo'];
	$graphicNo 				= $row['strGraphicRefNo'];
    $sampleInfo["graphicNo"] = $graphicNo;
	$styleNo 				= $row['strStyleNo'];
    $sampleInfo["styleNo"] = $styleNo;
	$customerId				= $row['intCustomer'];
	$marketerId  			= $row['intMarketer'];
	$brandId 				= $row['intBrand'];
	//$intCreator				= $row['intCreator'];
	//$sa = $row['dblSampleQty'];
	//$no = $row['dblDeliverQty'];
	//$grade 					= $row['intGrade'];
	//$fabricTypeId 			= $row['strFabricType'];
	
	$verivide_d65 			= $row['verivide_d65'];
	$verivide_tl84 			= $row['verivide_tl84'];
	$verivide_cw 			= $row['verivide_cw'];
	$verivide_f 			= $row['verivide_f'];
	$verivide_uv 			= $row['verivide_uv'];

	$macbeth_dl 			= $row['macbeth_dl'];
	$macbeth_cw 			= $row['macbeth_cw'];
	$macbeth_inca 			= $row['macbeth_inca'];
	$macbeth_tl84 			= $row['macbeth_tl84'];
	$macbeth_uv 			= $row['macbeth_uv'];
	$macbeth_horizon 		= $row['macbeth_horizon'];
    $lightBoxes["verivide_d65"] = $verivide_d65;
    $lightBoxes["verivide_tl84"] = $verivide_tl84;
    $lightBoxes["verivide_cw"] = $verivide_cw;
    $lightBoxes["verivide_f"] = $verivide_f;
    $lightBoxes["verivide_uv"] = $verivide_uv;

    $lightBoxes["macbeth_dl"] = $macbeth_dl;
    $lightBoxes["macbeth_cw"] = $macbeth_cw;
    $lightBoxes["macbeth_inca"] = $macbeth_inca;
    $lightBoxes["macbeth_tl84"] = $macbeth_tl84;
    $lightBoxes["macbeth_uv"] = $macbeth_uv;
    $lightBoxes["macbeth_horizon"] = $macbeth_horizon;

	$curing_temp			= $row['dblCuringCondition_temp'];
	$curing_beltspeed		= $row['dblCuringCondition_beltSpeed'];
	$press_temp				= $row['dblPressCondition_temp'];
	$press_pressure			= $row['dblPressCondition_pressure'];
	$press_time				= $row['dblPressCondition_time'];
	$strMeshCount			= $row['strMeshCount'];

	$instructions			= $row['strAdditionalInstructions'];
	$instructionsTech		= $row['strAdditionalInstructionsTech'];
	//$approveLevel			= $row['intApproveLevelStart'];
	$intCompId 				= $row['intCompanyId'];
	$intStatus 				= $row['intStatus'];
	$intMarketingStatus 	= $row['intMarketingStatus'];
	$intTechnicalStatus 	= $row['intTechnicalStatus'];
	$sampleRoomStatus		= $row['intSampleRoomStatus'];
	//$intTechnicalStatus 	= $row['intTechnicalStatus'];//intMarketingApproveLevelStart
	$intMarketingApproveLevelStart 	= $row['intMarketingApproveLevelStart'];//intMarketingApproveLevelStart
	$technicalOpenUser		= $row['intTechnicalOpenUser'];
	$sampleOpenUser			= $row['intSampleRoomOpenUser'];

}

$sql = "SELECT
			trn_sampleinfomations_printsize.strPrintName,
			trn_sampleinfomations_printsize.intWidth,
			trn_sampleinfomations_printsize.intHeight,
			trn_sampleinfomations_printsize.intPart
		FROM `trn_sampleinfomations_printsize`
		WHERE
			trn_sampleinfomations_printsize.intSampleNo 	=  '$sampleNo' AND
			trn_sampleinfomations_printsize.intSampleYear 	=  '$sampleYear' AND
			trn_sampleinfomations_printsize.intRevisionNo 	=  '$revNo'
		ORDER BY
			cast(SUBSTR(trn_sampleinfomations_printsize.strPrintName,7,2) as unsigned) ASC 
		";
$result = $db->RunQuery($sql);
$arrPart;
$pdfArrPart;
$pdfEntries;
while($row=mysqli_fetch_array($result))
{
	$arr['printName'] = $row['strPrintName'];
	$arr['width'] = $row['intWidth'];
	$arr['height'] = $row['intHeight'];
	$arr['part'] = $row['intPart'];
	$arrPart[] = $arr;
    $pdfArrPartEntry = array();
    $pdfArrPartEntry["intWidth"] = $row['intWidth'];
    $pdfArrPartEntry["intHeight"] = $row['intHeight'];
    $pdfArrPartEntry["intPart"] = $row['intPart'];
    $pdfArrPart[$row['strPrintName']] = $pdfArrPartEntry;
}
$sampleInfo["prints"] = $pdfArrPart;

$sql = "SELECT DISTINCT
		trn_sampleinfomations_details.strComboName,
		trn_sampleinfomations_details.intPrintMode,
		trn_sampleinfomations_details.intWashStanderd,
		trn_sampleinfomations_details.intGroundColor,
		trn_sampleinfomations_details.intFabricType
		FROM trn_sampleinfomations_details
		WHERE
		trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
		trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
		trn_sampleinfomations_details.intRevNo 		=  '$revNo'
		";
$result = $db->RunQuery($sql);
$arrCombo;
$pdfArrCombo;
while($row=mysqli_fetch_array($result))
{
	$arr['combo'] 		= $row['strComboName'];
	$arr['printMode'] 	= $row['intPrintMode'];
	$arr['groundColor'] = $row['intGroundColor'];
	$arr['wash'] 		= $row['intWashStanderd'];
	$arr['fabricType'] 	= $row['intFabricType'];
	$arrCombo[] 		= $arr;

    $pdfArrComboEntry = array();
    $pdfArrComboEntry["printMode"] = $row['intPrintMode'];
    $pdfArrComboEntry["groundColor"] = $row['intGroundColor'];
    $pdfArrComboEntry["wash"] = $row['intWashStanderd'];
    $pdfArrComboEntry["fabricType"] = $row['intFabricType'];
    $pdfArrCombo[$row['strComboName']] = $pdfArrComboEntry;
}

////////////// type of print //////////////
/*$sql = "SELECT DISTINCT
			trn_sampleinfomations_combos.strComboName,
			trn_sampleinfomations_combos.strPrintName,
			trn_sampleinfomations_combos.intTypeOfPrintId,
			mst_techniques.strName as typeName
		FROM `trn_sampleinfomations_combos`
		Inner Join mst_techniques ON mst_techniques.intId = trn_sampleinfomations_combos.intTypeOfPrintId
		WHERE
			trn_sampleinfomations_combos.intSampleNo 	=  '$sampleNo' AND
			trn_sampleinfomations_combos.intSampleYear 	=  '$sampleYear' AND
			trn_sampleinfomations_combos.intRevisionNo 	=  '$revNo'
		";*/
 $sql = "SELECT
		trn_sampleinfomations_details.strPrintName,
		trn_sampleinfomations_details.strComboName,
		trn_sampleinfomations_details.intColorId,
		trn_sampleinfomations_details.intItem AS marketing_itemId,
		mst_colors.strName AS colorName,
		mst_techniques.strName AS typeOfPrintName,
		trn_sampleinfomations_details.intTechniqueId AS typeOfPrintId,
		marketingItem.strName AS marketing_itemName,
		trn_sampleinfomations_details.dblQty,
		trn_sampleinfomations_details.size_w,
		trn_sampleinfomations_details.size_h
		FROM
		trn_sampleinfomations_details
		Inner Join mst_colors ON mst_colors.intId = trn_sampleinfomations_details.intColorId
		Left Join mst_techniques ON mst_techniques.intId = trn_sampleinfomations_details.intTechniqueId
		Left Join mst_item AS marketingItem ON marketingItem.intId = trn_sampleinfomations_details.intItem
		
		WHERE
		trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
		trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
		trn_sampleinfomations_details.intRevNo =  '$revNo' 
		group by 
		trn_sampleinfomations_details.strPrintName,
		trn_sampleinfomations_details.strComboName,
		trn_sampleinfomations_details.intColorId,
		trn_sampleinfomations_details.intTechniqueId 
		";
$result = $db->RunQuery($sql);
$arrType	= array();
$arr= array();
while($row=mysqli_fetch_array($result))
{
	$arr['combo'] 				= $row['strComboName'];
	$arr['printName'] 			= $row['strPrintName'];
	$arr['intColorId'] 			= $row['intColorId'];
	$arr['intTechniqueId'] 		= $row['typeOfPrintId'];
	$arr['marketing_itemId'] 	= $row['marketing_itemId'];
	$arr['colorName'] 			= $row['colorName'];
	$arr['typeOfPrintName'] 	= $row['typeOfPrintName'];
	$arr['typeOfPrintId'] 		= $row['typeOfPrintId'];
	$arr['marketing_itemName'] 	= $row['marketing_itemName'];
	$arr['dblQty'] 				= $row['dblQty'];
	
	$arr['size_w'] 					= $row['size_w'];
	$arr['size_h'] 					= $row['size_h'];

	$arrType[] 			= $arr;
}
//print_r($arrType);
 $sql = "SELECT DISTINCT
		trn_sampleinfomations_combos.strComboName,
		trn_sampleinfomations_combos.strPrintName,
		trn_sampleinfomations_combos.intTypeOfPrintId,
		trn_sampleinfomations_combos.intTechniqueId,
		mst_inktypes.strName as techName,
		trn_sampleinfomations_combos.intQty,
		trn_sampleinfomations_combos.intItemId,
		mst_inktypes.intType as spRM
		FROM
		trn_sampleinfomations_combos
		Inner Join mst_inktypes ON mst_inktypes.intId = trn_sampleinfomations_combos.intTechniqueId
		
		WHERE
		trn_sampleinfomations_combos.intSampleNo =  '$sampleNo' AND
		trn_sampleinfomations_combos.intSampleYear =  '$sampleYear' AND
		trn_sampleinfomations_combos.intRevisionNo =  '$revNo'
		";
$result = $db->RunQuery($sql);
$arrTech;
while($row=mysqli_fetch_array($result))
{
	$arr['combo'] 		= $row['strComboName'];
	$arr['printName'] 	= $row['strPrintName'];
	$arr['typeId'] 		= $row['intTypeOfPrintId'];
	$arr['techId'] 		= $row['intTechniqueId'];
	$arr['techName'] 	= $row['techName'];
	$arr['qty'] 		= $row['intQty'];
	$arr['itemId'] 		= $row['intItemId'];
	$arr['spRM'] 		= $row['spRM'];
	
	$arrTech[] 			= $arr;
}


$sql = "SELECT
		trn_sampleinfomations_colors.strComboName,
		trn_sampleinfomations_colors.strPrintName,
		trn_sampleinfomations_colors.intColor,
		mst_colors.strName as colorName
		FROM `trn_sampleinfomations_colors`
		Inner Join mst_colors ON mst_colors.intId = trn_sampleinfomations_colors.intColor
		WHERE
		trn_sampleinfomations_colors.intSampleNo =  '$sampleNo' AND
		trn_sampleinfomations_colors.intSampleYear =  '$sampleYear' AND
		trn_sampleinfomations_colors.intRevisionNo =  '$revNo'
		";
$result = $db->RunQuery($sql);
$arrColor;
while($row=mysqli_fetch_array($result))
{
	$arr['combo'] 		= $row['strComboName'];
	$arr['printName'] 	= $row['strPrintName'];
	$arr['colorId'] 		= $row['intColor'];
	$arr['colorName'] 		= $row['colorName'];
	$arrColor[] 			= $arr;
}

////////////////////////// check confirm permision //////////////////////////
	if($intMarketingStatus>1)
		$x = ($intMarketingApproveLevelStart+1)-$intMarketingStatus;
	else
		$x = ($intMarketingApproveLevelStart+1)-$intStatus;

	$sql = "	SELECT
					menupermision.int{$x}Approval as approval
				FROM menupermision Inner Join menus ON menus.intId = menupermision.intMenuId
				WHERE
					menus.strCode =  'P0022' AND
					menupermision.intUserId =  '$sessionUserId'
			";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	$userPermision = $row['approval'];
	
	if($reqNo!='' && $reqYear!='')
	{
		$sql = "SELECT 
				CONCAT(REQUISITION_NO,'/',REQUISITION_YEAR) AS conReqNo,
				GRAPHIC,
				CUSTOMER,
				STYLE,
				BRAND,
				MARKETER
				FROM $SRdb.trn_sample_requisition_header
				WHERE REQUISITION_NO = '$reqNo' AND
				REQUISITION_YEAR = '$reqYear' ";
		
		$result 	= $db->RunQuery($sql);
		$row		= mysqli_fetch_array($result);
		
		$conReqNo 		= $row['conReqNo'];
		$marketerId 	= $row['MARKETER'];
		$graphicNo 		= $row['GRAPHIC'];
		$customerId 	= $row['CUSTOMER'];
		$styleNo 		= $row['STYLE'];
		$brandId 		= $row['BRAND'];
		
	}
	
//BEGIN - CHECK PERMISSION {
$oldRevNo	= ((int)$revNo<(int)$maxRevNo?'1':'0');
$permissionNew		= false;
$permissionSave		= false;
$permissionDelete	= false;
$permissionSearch	= false;
$status				= false;

if(( $intStatus == 1 || ($intMarketingStatus==NULL) || ($intMarketingStatus==0) || ($intMarketingStatus <=2 && $intMarketingStatus >=1 && !$technicalOpenUser && !$technicalOpenUser) ||($intMarketingStatus == 1 && $technicalOpenUser) || ($intTechnicalStatus == 1 && $sampleOpenUser))){
	$status				= true;
}
if($form_permision['add']=='1')
{
	$permissionNew		= true;
	if($oldRevNo==0 && $status)
		$permissionSave	= true;
}

if($form_permision['edit']=='1')
{
	if($oldRevNo==0 && $status)
		$permissionSave	= true;
	$permissionSearch	= true;
}

if($form_permision['delete']=='1')
{
	$permissionDelete	= true;
	$permissionSearch	= true;
}

if($form_permision['view']=='1')
{
	$permissionSearch	= true;
}
//END 	}

?><head>
<title>Sample Infomations</title>
<script type="application/javascript">
var isOldRevNo = '<?php echo ((int)$revNo<(int)$maxRevNo?'1':'0') ?>';
</script>
<style type="text/css">
.printName {
	font-family: "Arial Black", Gadget, sans-serif;
	font-size: 16px;
	font-style: normal;
	font-weight: bold;
	color: #009;
	text-align:center
}
.statusWithBorder {	font-family: "Courier New", Courier, monospace;
	border: thin solid #666;
	font-size: 18px;
}
</style></head>

<body>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>

<form id="frmSampleInfomations" name="frmSampleInfomations" method="post" action="">
<div align="center">
		<div class="trans_layoutXL">
		  <div class="trans_text">Sample Informations</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="86%" border="0" align="center">
      <tr>
        <td width="100%" colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="22" colspan="5" align="center" valign="middle" ><table bgcolor="#FEF1C0" width="76%" border="0" cellpadding="0" cellspacing="0" class="normalfnt">
              
              <tr>
                <td width="8%"  class="normalfnt">1st Stage (Marketing)</td>
                <?php if($intMarketingStatus==1){ ?>
                <td width="12%" align="center" bgcolor="#D9F4FD"  class="statusWithBorder" >Approved</td>
                <?php }else if($intMarketingStatus>1){?>
                <td width="10%" align="center" bgcolor="#CC9900"  class="statusWithBorder" ><b>Pending</b></td>
                <?php }else{?>
                <td width="1%" align="center"   class="" >&nbsp;</td>
                <?php }?>
                
                <td width="1%">&nbsp;</td>
                <td width="8%">2nd Stage (Technical)</td>
                <?php if(isset($intTechnicalStatus) && $intTechnicalStatus==1){ ?>
                <td width="12%" align="center" bgcolor="#D9F4FD"  class="statusWithBorder" >Approved</td>
                <?php }else if(isset($intTechnicalStatus) && $intTechnicalStatus>1) {?>
                <td width="10%" align="center" bgcolor="#CC9900"  class="statusWithBorder" ><b>Pending</b></td>
                <?php }else{?>
                <?php }?>
                <td width="3%" align="center"   class="" >&nbsp;</td>
                
                <td width="1%">&nbsp;</td>
                <td width="8%">3rd Stage (Sample Room)</td>
                <?php if(isset($sampleRoomStatus) && $sampleRoomStatus==1){ ?>
                <td width="12%" align="center" bgcolor="#D9F4FD"  class="statusWithBorder" >Approved</td>
                <?php }else if(isset($sampleRoomStatus) && $sampleRoomStatus>1) {?>
                <td width="10%" align="center" bgcolor="#CC9900"  class="statusWithBorder" ><b>Pending</b></td>
                <?php }else {?>
                <?php }?>
                <td width="3%" align="center"   class="" >&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          <tr>
            <td width="12%" height="22" class="normalfnt">&nbsp;</td>
            <td width="21%">&nbsp;</td>
            <td width="6%">&nbsp;</td>
            <td width="10%" class="normalfnt">&nbsp;</td>
            <td width="51%">&nbsp;</td>
            </tr>
          <tr>
            <td height="28" colspan="5" class="normalfnt"><table bgcolor="#E3F4FD" width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td height="28" bgcolor="#FFFFFF" >&nbsp;</td>
                <td class="normalfnt" width="10%">Sample Year</td>
                <td class="normalfnt" width="10%">Graphic No</td>
                <td class="normalfnt" width="10%">Sample No</td>
                <td class="normalfnt" width="10%">Revision No</td>
                <td class="normalfnt" width="10%">Date</td>
                <td bgcolor="#FFFFFF"></td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
              </tr>
              <tr>
                <td width="7%" height="28" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="10%" bgcolor="#FFFFFF"><select name="cboYear" id="cboYear" style="width:70px">
                  <?php
				    $d = date('Y');
				  	for($d;$d>=2012;$d--)
					{
						if($d==$sampleYear)
							echo "<option selected=\"selected\" id=\"$d\" >$d</option>";	
						else
							echo "<option id=\"$d\" >$d</option>";	
					}
				  ?>
                </select></td>
                <td width="20%" bgcolor="#FFFFFF">
				<select class="js-example-data-ajax normalfnt" id="cboStyleNo" name="cboStyleNo">
				 <option value=""  selected="selected">Please Clear Cache</option>
				</select>
				
				
				<!--<select name="cboStyleNo" id="cboStyleNo" style="width:160px">
                  <option value=""></option>-->
                  <?php
					//$d = date('Y');
				    // $sql = "SELECT DISTINCT
								// trn_sampleinfomations.strGraphicRefNo
							// FROM trn_sampleinfomations 
							// where 
							// trn_sampleinfomations.intSampleYear =  '$sampleYear' 
							// ORDER BY
								// trn_sampleinfomations.strGraphicRefNo ASC
					// ";
					// $result = $db->RunQuery($sql);
					// while($row=mysqli_fetch_array($result))
					// {
						// $strGraphicRefNo = $row['strGraphicRefNo'];
						// if($graphicNo==$strGraphicRefNo)
							// echo "<option selected=\"selected\" value=\"$strStyle\">$strGraphicRefNo</option>";
						// else
							// echo "<option value=\"$strGraphicRefNo\">$strGraphicRefNo</option>";
					// }
				?>
               <!-- </select>-->
				
				
				
				</td>
                <td width="16%" bgcolor="#FFFFFF">
				<select name="cboSampleNo" id="cboSampleNo" style="width:120px">
                  <option value=""></option>
                  <?php
					$d = date('Y');
					//if($sampleYear!='')
						//	$d = $sampleYear;
				    $sql = "SELECT DISTINCT
							trn_sampleinfomations.intSampleNo
							FROM trn_sampleinfomations
						WHERE
							trn_sampleinfomations.intSampleYear =  '$sampleYear' 
						ORDER BY
							trn_sampleinfomations.intSampleNo ASC
					";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$no = $row['intSampleNo'];
						if($no==$sampleNo)
							echo "<option selected=\"selected\" value=\"$no\">$no</option>";
						else
							echo "<option value=\"$no\">$no</option>";
					}
				?>
                  </select>
				  </td>
                <td width="16%" bgcolor="#FFFFFF" class="normalfnt"><select name="cboRevisionNo" id="cboRevisionNo" style="width:100px">
                  <option value=""></option>
                  <?php
				  	$sql = "SELECT
					trn_sampleinfomations.intRevisionNo
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations.intSampleNo =  '$sampleNo'
				ORDER BY
					trn_sampleinfomations.intRevisionNo ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($revNo==$row['intRevisionNo']) {
                        echo "<option selected=\"selected\" value=\"" . $row['intRevisionNo'] . "\">" . $row['intRevisionNo'] . "</option>";
                        $sampleInfo['revisionNo'] = $row['intRevisionNo'];
					} else {
                        echo "<option value=\"" . $row['intRevisionNo'] . "\">" . $row['intRevisionNo'] . "</option>";
                    }
				}
				  ?>
                </select></td>
                <td width="22%" bgcolor="#FFFFFF" class="normalfnt"><input name="dtDate" type="text"  class="txtbox" id="dtDate" style="width:98px;"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);" value="<?php echo (!isset($enterDate)?date("Y-m-d"):$enterDate); ?>"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                <td width="6%" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="21%" bgcolor="#FFFFFF">&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          <tr>
            <td height="19" colspan="5" class="normalfnt">&nbsp;</td>
          </tr>
           <tr>
            <td height="19" colspan="1" class="normalfnt">Sample Order No</td>
            <td  colspan="4" ><?php
                $sql_Get_Graphic = "SELECT
               DISTINCT CONCAT(trn_sampleorderheader.intSampleOrderYear,'/',trn_sampleorderheader.intSampleOrderNo) AS sampleOrder,
               trn_sampleorderheader.intSampleOrderYear,trn_sampleorderheader.intSampleOrderNo
                FROM
                trn_sampleorderheader
                INNER JOIN trn_sampleinfomations ON trn_sampleorderheader.intSampleNo = trn_sampleinfomations.intSampleNo
                AND trn_sampleorderheader.intSampleYear = trn_sampleinfomations.intSampleYear
                WHERE trn_sampleinfomations.intSampleNo = '$sampleNo' AND trn_sampleinfomations.intSampleYear = '$sampleYear' AND trn_sampleorderheader.intStatus = 1";
				
                $result_Graphic = $db->RunQuery($sql_Get_Graphic);
                $arrSampleOrder;
                while($row = mysqli_fetch_array($result_Graphic))
                {
                    echo  '<a href=?q=983'.'&orderNo='.$row['intSampleOrderNo'].'&year='.$row['intSampleOrderYear'].' target="_blank">'.$row['sampleOrder'].'</a>'.'&nbsp&nbsp';
                }
                ?></td>
           </tr>
          <tr>
            <td height="27" class="normalfnt">Requisition No</td>
            <td colspan="4"><select disabled="disabled" name="cboRequisitionNo" id="cboRequisitionNo" style="width:120px">
            <option value=""></option>
              <?php
					$sql = "SELECT REQUISITION_NO,REQUISITION_YEAR,CONCAT(REQUISITION_NO,'/',REQUISITION_YEAR) AS conReqNo
							FROM $SRdb.trn_sample_requisition_header
							WHERE STATUS = 1";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($conReqNo==$row['conReqNo']) {
                            $sampleInfo["conReqNo"] = $row['conReqNo'];
                            echo "<option selected value=\"" . $row['conReqNo'] . "\">" . $row['conReqNo'] . "</option>";
                        } else {
                            echo "<option value=\"".$row['conReqNo']."\">".$row['conReqNo']."</option>";
                        }
					}
				?>
            </select></td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Marketer</td>
            <td colspan="4"><select name="cboMarketer" id="cboMarketer"  class="validate[required]" style="width:570px">
              <option value=""></option>
              <?php
					$sql = "(SELECT
							sys_users.intUserId,
							sys_users.strUserName
							FROM
							mst_marketer
							INNER JOIN sys_users ON mst_marketer.intUserId = sys_users.intUserId
							WHERE
							sys_users.intStatus = 1 and mst_marketer.`STATUS`=1
							ORDER BY
							sys_users.strUserName ASC 
							)
							UNION  
							(
							SELECT
							sys_users.intUserId,
							sys_users.strUserName
							FROM
							trn_sampleinfomations
							INNER JOIN sys_users ON trn_sampleinfomations.intMarketer = sys_users.intUserId
							WHERE 
							intSampleNo =  '$sampleNo' AND
							intSampleYear =  '$sampleYear' AND
							intRevisionNo =  '$revNo'
							)";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($marketerId==$row['intUserId']){
                            echo "<option selected value=\"".$row['intUserId']."\">".$row['strUserName']."</option>";
                            $sampleInfo["marketerName"] = $row['strUserName'];
                        } else {
                            echo "<option value=\"".$row['intUserId']."\">".$row['strUserName']."</option>";
                        }
					}
				?>
              </select></td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Graphic Ref NO</td>
            <td colspan="4"><input value="<?php echo $graphicNo; ?>" name="txtGraphicRefNo" type="text" class="validate[required,maxSize[200]]" id="txtGraphicRefNo" style="width:570px" /></td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Customer</td>
            <td colspan="4"><select name="cboCustomer" id="cboCustomer" class="validate[required]" style="width:570px">
              <option value=""></option>
              <?php
					$sql = "select intId,strName from mst_customer where intStatus = 1 order by strName";
                                       // echo $sql;
					$result = $db->RunQuery($sql);
                                        //echo $result;
					while($row=mysqli_fetch_array($result))
					{
                                             //$id=$row['intId'];
                                             //echo $id;
                                            // $raw_code=$obj_mstCustomer->set($id);  
              //  if($id > 0){
               // $data=$obj_mstCustomer->getactiveSample($raw_code);
                //echo $data;
               // }
			
                        if($customerId==$row['intId']&&($data==0)) {
                            echo "<option selected value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                            $sampleInfo["customerName"] = $row['strName'];
                        } else {
                            echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
                        }
					}
				?>
              </select></td>
          
            </tr>
          <tr>
            <td height="27" class="normalfnt">Style No</td>
            <td colspan="4"><input value="<?php echo $styleNo; ?>"  name="txtStyleNo" type="text" class="validate[maxSize[50]]"  id="txtStyleNo" style="width:570px" /></td>
            </tr>
          <tr>
            <td height="27" class="normalfnt">Brand</td>
            <td colspan="4"><select class="validate[required]" name="cboBrand" id="cboBrand" style="width:570px">
            <option></option>
              <?php
				$sql = "(SELECT
						mst_brand.intId,
						mst_brand.strName
					FROM
						mst_customer_brand
						Inner Join mst_brand ON mst_brand.intId = mst_customer_brand.intBrandId
					WHERE
						mst_customer_brand.intCustomerId =  '$customerId' AND
						mst_brand.intStatus =  '1'
					ORDER BY
						mst_brand.strName ASC)
						UNION
						(SELECT
						mst_brand.intId,
						mst_brand.strName
						FROM
						mst_customer_brand
						Inner Join mst_brand ON mst_brand.intId = mst_customer_brand.intBrandId
						INNER JOIN trn_sampleinfomations ON mst_brand.intId = trn_sampleinfomations.intBrand
						
						WHERE
							mst_customer_brand.intCustomerId =  '$customerId'
						ORDER BY
							mst_brand.strName ASC)
						";
				$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($brandId==$row['intId']) {
                            echo "<option selected value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                            $sampleInfo["brand"] = $row['strName'];
                        } else {
                            echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
                        }
					}
			?>
              </select></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td colspan="2"><select  name="cboTechnique" id="cboTechnique" style="width:100px;display:none">
          <option value=""></option>
          <?php
					$sql = "select intId,strName from mst_washstanderd where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						//echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
          </select><select class="colors"  name="cboColor" id="cboColor" style="width:100px;display:none">
            <option value=""></option>
            <?php
					$sql = "select intId,strName from mst_colors where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
      </tr>
      <tr>
        <td id="tdPlaceMainGrid" colspan="2">
        <?php 
					
			$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strComboName
					FROM trn_sampleinfomations_details
					WHERE
					trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_details.intRevNo =  '$revNo'
					";
			$result = $db->RunQuery($sql);
			$rowsCount = mysqli_num_rows($result);
			
			$sql = "SELECT DISTINCT
						strPrintName
					FROM trn_sampleinfomations_printsize
					WHERE
						intSampleNo 	=  '$sampleNo' AND
						intSampleYear 	=  '$sampleYear' AND
						intRevisionNo 	=  '$revNo'
			";
			$result = $db->RunQuery($sql);
			$cellCount = mysqli_num_rows($result);
			
		?>     
        <table id="tblMain" width="74%" border="0" align="left"  class="grid"  >
          <tr class="">
            <td height="152" bgcolor="#FFFFFF" class="normalfnt"  >&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF"><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:264px;height:148px;overflow:hidden" ><?php
			if($sampleNo!='')
			{
                $sampleInfo["prints"]["print 1"]["imageUrl"] = "documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revNo-1.png" ;
				echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revNo-1.png\" />";	
			}
			 ?></div>
              <img align="right" class="removeColumn mouseover" src="images/del.png" width="15" height="15" /></td>
            <?php
				///////////  img cell loop 
				for($i=1;$i<$cellCount;$i++)
				{
                    $sampleInfo["prints"]["print ".($i + 1)]["imageUrl"] = "documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revNo-".($i+1).".png" ;
			?>
            	<td class="tdRemove" align="center" bgcolor="#FFFFFF"><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:264px;height:148px;overflow:hidden" ><?php

		echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revNo-".($i+1).".png\" />";	
			
			 ?></div>
            	  <img align="right" class="removeColumn mouseover" src="images/del.png" width="15" height="15" /></td>
            <?php	
				}
			?>
            <td bgcolor="#FFFFFF" ><img src="images/insert.png" name="butInsertCol" width="78" height="24" class="mouseover" id="butInsertCol" /></td>
          </tr>
          <tr class="">
            <td height="18" bgcolor="#FFFFFF" class="normalfnt" >&nbsp;</td>
            <td align="center" style="width:300px" bgcolor="#FFFFFF" class="printName" ><span class="printNameSpan">Print 1</span><span  class="normalfntMid">
              <select  class="part validate[required]" name="cboPart1" id="cboPart1" style="width:180px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_part where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrPart[0]['part']){
                            echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
                            $sampleInfo["prints"]["print 1"]["partName"] = $row['strName'];
                        } else {
                            echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
                        }
					}
				?>
              </select>
              <input name="textfield" type="text" class="sizeW validate[required,custom[number],max[1000],min[0]]" id="sizeW1" style="width:40px" value="<?php echo $arrPart[0]['width']; ?>" />
              <span class="normalfntGrey">W
              <input name="textfield2" type="text" class="sizeH validate[required,custom[number],max[1000],min[0]]" id="sizeH1" style="width:40px" value="<?php echo $arrPart[0]['height']; ?>" />
              H(inch)</span></span></td>
              <?php
			 // print_r($arrPart[2]);
			  	///////// print name cell loop
				$e=0;
				for($i=1;$i<$cellCount;$i++)
				{
					
			  ?>
              		<td   align="center" bgcolor="#FFFFFF" class="printName tdRemove" ><span class="printNameSpan">Print <?php echo ($i+1); ?></span><span  class="normalfntMid">
              <select  class="part validate[required]" name="cboPart2" id="cboPart2" style="width:180px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_part where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					$e++;
					while($row=mysqli_fetch_array($result))
					{

						if($row['intId']==$arrPart[$e]['part']) {
                            echo "<option selected value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                            $sampleInfo["prints"]["print ".($i + 1)]["partName"] = $row['strName'];
                        } else {
                            echo "<option value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";

                        }	//$e++;
					}
				?>
              </select>
              <input name="textfield" type="text" class="sizeW validate[required,custom[number],max[1000],min[0]]" id="sizeW2" style="width:40px" value="<?php echo $arrPart[$i]['width']; ?>" />
              <span class="normalfntGrey">W
              <input name="textfield2" type="text" class="sizeH validate[required,custom[number],max[1000],min[0]]" id="sizeH2" style="width:40px" value="<?php echo $arrPart[$i]['height']; ?>" />
              H(inch)</span></span></td>
              
              <?php
				}
			  ?>
         <td bgcolor="#FFFFFF" >&nbsp;</td>
          </tr>
           <?php
			$sql 	= "SELECT
									trn_sampleinfomations_prices.dblPrice,
									costing_sample_header.QTY_PRICES
									FROM
									trn_sampleinfomations_prices left join  costing_sample_header on 
									costing_sample_header.SAMPLE_NO = trn_sampleinfomations_prices.intSampleNo AND 
									costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations_prices.intSampleYear AND 
									costing_sample_header.REVISION = trn_sampleinfomations_prices.intRevisionNo AND
									costing_sample_header.COMBO = trn_sampleinfomations_prices.strCombo AND 
									costing_sample_header.PRINT = trn_sampleinfomations_prices.strPrintName 
									WHERE
									trn_sampleinfomations_prices.intSampleNo = '$sampleNo' AND
									trn_sampleinfomations_prices.intSampleYear = '$sampleYear' AND
									trn_sampleinfomations_prices.intRevisionNo = '$revNo' AND
									trn_sampleinfomations_prices.strCombo ='".$arrCombo[0]['combo']."' AND
									trn_sampleinfomations_prices.strPrintName = '"."print "."1"."' 
									";

						$result = 	$db->RunQuery($sql);
						$row	=	mysqli_fetch_array($result);
			
			?>
          <tr class="">
            <td height="10" bgcolor="#CCCCCC" class="gridHeader" ><input align="left" class="validate[required,maxSize[20]]" value="<?php echo $arrCombo[0]['combo']; ?>" style="width:80px" type="text" name="txtCombo1" id="txtCombo1" />
              <img align="right" class="removeRow2 mouseover" src="images/del.png" width="15" height="15" /></td>
            <td align="center" bgcolor="#FFFFFF" class="normalfntRight" ><table width="100%" cellspacing="0" cellpadding="0">
            <tr bgcolor="#CCCCCC"  class="gridHeader"><td width="10%" align="left" nowrap  class="normalfnt" style="display:none">No of ups</td>
            <td width="6%" style="display:none"><input align="left" value="<?php echo getUps($arrCombo[0]['combo'],'print 1');?>" style="width:40px; text-align:right" type="text" name="ups" id="ups"  class="cls_ups"/></td>
            <td width="1%">&nbsp;</td>
            <td width="27%"  class="normalfnt"><a id="butProcess" class="button green medium butProcess" style="" name="butProcess"> Routing Process</a></td>
            <td width="1%"><div style="display:none" class="combo"><?php echo $arrCombo[0]['combo']; ?></div><div style="display:none" class="print">print 1</div>&nbsp;</td>
                <?php
                if ($row['QTY_PRICES'] == 1){
                    ?>
                    <td width="27%"  class="normalfnt"><a id="qtyPrices" class="button green medium" style="" name="qtyPrices"> Qty Prices</a></td>
                    <?php

                } else {
                    ?>
                    <td align="center" bgcolor="#FFFFFF" class="printName" >&nbsp; Price : <?php echo $row['dblPrice']; $pdfEntries[$arrCombo[0]['combo'].'print 1']["price"] = $row['dblPrice']; ?> </td>
                    <?php
                }
                ?>
           	</tr>
            </table></td>
              
            <?php
				for($i=1;$i<$cellCount;$i++)
				{
					//echo "00 / ";
					$sql 	= "SELECT
									trn_sampleinfomations_prices.dblPrice,
									costing_sample_header.QTY_PRICES
									FROM
									trn_sampleinfomations_prices left join  costing_sample_header on 
									costing_sample_header.SAMPLE_NO = trn_sampleinfomations_prices.intSampleNo AND
									costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations_prices.intSampleYear AND
									costing_sample_header.REVISION = trn_sampleinfomations_prices.intRevisionNo AND
									costing_sample_header.COMBO = trn_sampleinfomations_prices.strCombo AND
									costing_sample_header.PRINT = trn_sampleinfomations_prices.strPrintName
									WHERE
									trn_sampleinfomations_prices.intSampleNo = '$sampleNo' AND
									trn_sampleinfomations_prices.intSampleYear = '$sampleYear' AND
									trn_sampleinfomations_prices.intRevisionNo = '$revNo' AND
									trn_sampleinfomations_prices.strCombo ='".$arrCombo[0]['combo']."' AND
									trn_sampleinfomations_prices.strPrintName = '"."print ".($i+1)."' 
									";

						$result = 	$db->RunQuery($sql);
						$row	=	mysqli_fetch_array($result);
			?>
            <td align="right" bgcolor="#FFFFFF" ><table width="100%" cellspacing="0" cellpadding="0">
            <tr bgcolor="#CCCCCC"  class="gridHeader"><td width="13%" align="left" class="normalfnt" style="display:none">No of ups</td>
            <td width="8%" style="display:none"><input align="left" value="<?php echo getUps($arrCombo[0]['combo'],'print '.($i+1));?>" style="width:40px; text-align:right" type="text" name="ups" id="ups"  class="cls_ups"/></td>
            <td width="4%">&nbsp;</td>
            <td width="16%"  class="normalfnt">&nbsp;</td>
            <td width="30%" align="left"><a id="butProcess" class="button green small butProcess" style="" name="butProcess"> Routing Process</a>&nbsp;</td>
            <td width="4%"><div style="display:none" class="combo"><?php echo $arrCombo[0]['combo']; ?></div><div style="display:none" class="print">print <?php echo $i+1 ?></div>&nbsp;</td>
                <?php
                if ($row['QTY_PRICES'] == 1){
                    ?>
                    <td width="27%"  class="normalfnt"><a id="qtyPrices" class="button green medium" style="" name="qtyPrices"> Qty Prices</a></td>
                    <?php

                } else {
                    ?>
                    <td align="center" bgcolor="#FFFFFF" class="printName" >&nbsp; Price : <?php echo $row['dblPrice']; $pdfEntries[$arrCombo[0]['combo'].'print '.($i +1)]["price"] = $row['dblPrice'];?> </td>
                    <?php
                }
                ?>
                </table></td>
            
            <?php
				}
			?>
            <td bgcolor="#FFFFFF" >&nbsp;</td>
          </tr>
          <tr class="dataRow">
            <td width="18%"  bgcolor="#FFFFFF" >
              <select title="Print Mote"  name="cboPrintMode2" class="printMode validate[required]" id="cboPrintMode2" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_printmode where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[0]['printMode']) {
                            $pdfArrCombo[$arrCombo[0]["combo"]]["printMode"] = $row['strName'];
                            echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
                        } else {
                            echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
                        }
					}
				?>
                </select>
              <select title="Wash Standerd" class="washStanderd validate[required]" name="butWashingStanderd2" id="butWashingStanderd3" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_washstanderd where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
                    {
                        if($row['intId']==$arrCombo[0]['wash']){
                            $pdfArrCombo[$arrCombo[0]["combo"]]["wash"] = $row['strName'];
                            echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
                        }
                        else {
                             echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
                        }
					}
				?>
                </select>
              <select title="Ground Colour" class="colors validate[required]" name="cboGroundColor2" id="cboGroundColor2" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_colors_ground where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[0]['groundColor']) {
                            $pdfArrCombo[$arrCombo[0]["combo"]]["groundColor"] = $row['strName'];
                            echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
                        } else {
                            echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
                        }
					}
				?>
                </select>
              <select title="Fabric Type" class="colors validate[required]" name="cboFabricType" id="cboFabricType" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_fabrictype where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[0]['fabricType']) {
                            $pdfArrCombo[$arrCombo[0]["combo"]]["fabricType"] = $row['strName'];
                            echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
                        } else {
                            echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
                        }
					}
				?>
              </select></td>
            <td width="67%"  bgcolor="#FFFFFF" style="padding-top:0px" ><table id="tblGrid2" class="tblGrid2" bgcolor="#006666" cellpadding="0" cellspacing="1" width="650" border="0">
              <tr class="normalfntMid">
                <td width="70" bgcolor="#CEE8FB">Color<img  src="images/add_new.png" name="butColors" width="15" height="15" class="mouseover butColors" id="butColors" /></td>
                <td width="100" bgcolor="#CEE8FB">Technique</td>
                <td width="100" bgcolor="#CEE8FB">Item</td>
                <td width="40" bgcolor="#CEE8FB">Qty</td>
                <td width="86" bgcolor="#CEE8FB">Size(inch)</td>
                <td bgcolor="#FFE9D2"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                  <tr class="normalfnt">
                    <td  width="40%" bgcolor="#DFFFDF">Ink Type</td>
                    <td width="13%" bgcolor="#DFFFDF">Shots</td>
                    <td width="35%" bgcolor="#DFFFDF">Item</td>
                    <td width="12%" bgcolor="#DFFFDF">Weight</td>
                  </tr>
                </table></td>
                </tr>
                <?php
				foreach($arrType as $arrT)
				{
					//echo $arrT['combo'];
					//echo $arrCombo[0]['combo'];
					if($arrT['combo']==$arrCombo[0]['combo'] && $arrT['printName']=='print 1')
					{

                        $entry = array() ;
                        $entryKey = $arrCombo[0]['combo'].'print 1';
/*
							$arr['combo'] 				= $row['strComboName'];
							$arr['printName'] 			= $row['strPrintName'];
							
							$arr['intColorId'] 			= $row['intColorId'];
							$arr['marketing_itemId'] 	= $row['marketing_itemId'];
							$arr['colorName'] 			= $row['colorName'];
							$arr['typeOfPrintName'] 	= $row['typeOfPrintName'];
							$arr['typeOfPrintId'] 		= $row['typeOfPrintId'];
							$arr['marketing_itemName'] 	= $row['marketing_itemName'];
							$arr['dblQty'] 				= $row['dblQty'];
							
							$arr['size_w'] 					= $row['size_w'];
							$arr['size_h'] 					= $row['size_h'];
							$arr['technique_techId'] 		= $row['technique_techId'];
							$arr['technique_techName'] 		= $row['technique_techName'];
							$arr['technique_intNoOfShots'] 	= $row['technique_intNoOfShots'];
							$arr['technique_itemId'] 		= $row['technique_itemId'];
							$arr['technique_itemName'] 		= $row['technique_itemName'];
							$arr['technique_dblColorWeight']= $row['technique_dblColorWeight'];*/
				?>
                <tr>
                  <td bgcolor="#ffffffff" class="normalfnt" id="<?php echo $arrT['intColorId'] ?>"><img src="images/del.png" class="mouseover removeRow" /><?php echo $arrT['colorName']; $entry["colorName"] = $arrT['colorName']; $pdfEntries[$entryKey]["combo"] = $arrCombo[0]['combo']; $pdfEntries[$entryKey]["print"] = 'print 1';?></td>
                  <td bgcolor="#ffffffff" class="normalfnt" ><select id="cboTechnique" name="select4" class="validate[required] cboItem cboTech" style="width:100px">
                    <option value=""></option>
                    <?php
			  	$sql = "SELECT mst_techniques.intId, mst_techniques.strName FROM mst_techniques WHERE mst_techniques.intStatus =  '1' order by strName
						";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
			 
              		if($row['intId']==$arrT['typeOfPrintId']) {
                        echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>" ;
                        $entry["techniqueName"] = $row['strName'];
                    } else {
                        echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
                    }

				}
			  ?>
                  </select></td>
                  <td bgcolor="#ffffffff"><table  bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0" class="tblGridTI" id="tblGridTI">
                    <?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId'],$arrT['intTechniqueId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				$itm_count	=0;
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$itm_count++;
				$item_tech	=	$row_ti['intItem'];
					?>
                    <tr>
                    <td>
              <select name="select3" class="cboItem cboItems techItem" id="cboItem" style="width:100px">
                        <option value=""></option>
                        <?php
			$para = " WHERE mst_techniques_wise_item.TECHNIQUE_ID =  '".$arrT['intTechniqueId']."'";
			
			$sql = "SELECT
					mst_item.intId,
					mst_item.strName
					FROM `mst_item`  
					INNER JOIN mst_techniques_wise_item ON 
					mst_item.intId = mst_techniques_wise_item.ITEM_ID 
					$para 
					ORDER BY
					mst_item.strName ASC ";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
              		if($row['intId']==$item_tech) {
                        echo "<option selected=\"selected\" value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                        $entry["itemName"] = $row['strName'];
                    } else {
                        echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
                    }
				}
			  ?>
                      </select>
                 </td></tr>
                 <?php
					}
					
				if($itm_count==0){ ?>
                    <tr>
                    <td>
              <select name="select3" class="cboItem cboItems techItem" id="cboItem" style="width:100px">
                        <option value=""></option>
                </select>
				<?php } ?>
                 </table></td>
                  <td bgcolor="#ffffffff">
                    <table  bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0" class="tblGridTIQ" id="tblGridTIQ">
                    <?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId'],$arrT['intTechniqueId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];
				
?><tr><td>
                    <input     style="width:30px" value="<?PHP echo ($row_ti['dblQty']<=0?'':$row_ti['dblQty']);  $entry["dblQty"] = $row_ti['dblQty'];?>"  type="text" name="textfield5" id="txtQty" /><div style="display:none" class="parentItem"><?php echo $item_tech; ?></div>
                    </td></tr>
                    <?php
					}
					?>
                    </table>
                 </td>
                  <td bgcolor="#ffffffff" class="normalfnt"><table  bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0" class="tblGridTIWH" id="tblGridTIWH">
                    <?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId'],$arrT['intTechniqueId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];
				?><tr><td nowrap  bgcolor="#ffffffff"><div style="display:none" class="parentItem"><?php echo $item_tech; ?></div>
                  <input   value="<?PHP echo ($row_ti['size_w']<=0?'':$row_ti['size_w']); $entry["size_w"] = $row_ti['size_w'];?>" style="width:30px"  type="text" name="textfield3" id="txtSizeW" />
                    W
                    <input style="width:30px" value="<?PHP echo ($row_ti['size_h']<=0?'':$row_ti['size_h']);  $entry["size_h"] = $row_ti['size_h']; ?>"  type="text" name="textfield4" id="txtSizeH" />
                    H</td></tr>
                    <?php
				}
					?>
                    </table></td>
                  <td bgcolor="#ffffffff"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                    <?php 
					 $sql = "	SELECT
									trn_sampleinfomations_details_technical.intInkTypeId,
									trn_sampleinfomations_details_technical.intNoOfShots,
									trn_sampleinfomations_details_technical.intItem,
									trn_sampleinfomations_details_technical.dblColorWeight,
									mst_inktypes.strName AS techName,
									mst_item.strName AS itemName
								FROM
								trn_sampleinfomations_details_technical
									left Join mst_inktypes ON mst_inktypes.intId = trn_sampleinfomations_details_technical.intInkTypeId
									left Join mst_item ON mst_item.intId = trn_sampleinfomations_details_technical.intItem
								WHERE
									trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
									trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
									trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND
									trn_sampleinfomations_details_technical.strPrintName 	=  '".$arrT['printName']."' AND
									trn_sampleinfomations_details_technical.strComboName 	=  '".$arrT['combo']."' AND
									trn_sampleinfomations_details_technical.intColorId 		=  '".$arrT['intColorId']."'
								";
						$result_tech = $db->RunQuery($sql);
						while($row_tech=mysqli_fetch_array($result_tech))
						{
						    $inkEntry = array();
					?>
                    <tr class="normalfnt">
                      <td width="40%" bgcolor="#FFFFFF"><?php echo $row_tech['techName']; $inkEntry["2ndtechName"] = $row_tech['techName'];?></td>
                      <td width="13%" bgcolor="#FFFFFF"><?php echo $row_tech['intNoOfShots']; $inkEntry["intNoOfShots"] = $row_tech['intNoOfShots'];?></td>
                      <td width="35%" bgcolor="#FFFFFF"><?php echo $row_tech['itemName']; $inkEntry["2nditemName"] = $row_tech['itemName'];?></td>
                      <td width="12%" bgcolor="#FFFFFF"><?php echo $row_tech['dblColorWeight']; $inkEntry["dblColorWeight"] = $row_tech['dblColorWeight'];?></td>
                    </tr>
                    <?php
                            if (isset($entry["inkEntries"])){
                                array_push($entry["inkEntries"], $inkEntry);
                            } else {
                                $entry["inkEntries"] = array();
                                array_push($entry["inkEntries"], $inkEntry);
                            }
//                            var_dump($entry);
//                            die();
						}
					?>
                  </table></td>
                  </tr>
                <?php

                        if (isset($pdfEntries[$entryKey]["rows"])){
                            array_push($pdfEntries[$entryKey]["rows"], $entry);
                        } else {
                            $pdfEntries[$entryKey]["rows"] = array();
                            array_push($pdfEntries[$entryKey]["rows"], $entry);
                        }
					}
				}
				?>
              </table></td>
                
                <?php
					/////////// first row table cells //////////////
					for($i=1;$i<$cellCount;$i++)
					{
				?>
                <td class="tdRemove" width="67%"  bgcolor="#FFFFFF" style="padding-top:0px" ><table id="tblGrid2" class="tblGrid2" bgcolor="#006666" cellpadding="0" cellspacing="1" width="650" border="0">
                  <tr class="normalfntMid">
                    <td width="70" bgcolor="#CEE8FB">Color<img  src="images/add_new.png" name="butColors" width="15" height="15" class="mouseover butColors" id="butColors2" /></td>
                    <td width="100" bgcolor="#CEE8FB">Technique</td>
                    <td width="100" bgcolor="#CEE8FB">Item</td>
                    <td width="40" bgcolor="#CEE8FB">Qty</td>
                    <td width="86" bgcolor="#CEE8FB">Size</td>
                    <td bgcolor="#FFE9D2"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                      <tr class="normalfnt">
                        <td  width="40%" bgcolor="#DFFFDF">Ink Type</td>
                        <td width="13%" bgcolor="#DFFFDF">Shots</td>
                        <td width="35%" bgcolor="#DFFFDF">Item</td>
                        <td width="12%" bgcolor="#DFFFDF">Weight</td>
                      </tr>
                    </table></td>
                  </tr>
                  <?php
				  
				foreach($arrType as $arrT)
				{
					
					//echo $arrT['combo'];
					//echo $arrCombo[0]['combo'];
					//echo  $arrT['printName'];
					//echo 'print '.($i+1).'<br>';
					if($arrT['combo']==$arrCombo[0]['combo'] && $arrT['printName']=='print '.($i+1))
					{
						
/*						
							$arr['combo'] 				= $row['strComboName'];
							$arr['printName'] 			= $row['strPrintName'];
							
							$arr['intColorId'] 			= $row['intColorId'];
							$arr['marketing_itemId'] 	= $row['marketing_itemId'];
							$arr['colorName'] 			= $row['colorName'];
							$arr['typeOfPrintName'] 	= $row['typeOfPrintName'];
							$arr['typeOfPrintId'] 		= $row['typeOfPrintId'];
							$arr['marketing_itemName'] 	= $row['marketing_itemName'];
							$arr['dblQty'] 				= $row['dblQty'];
							
							$arr['size_w'] 					= $row['size_w'];
							$arr['size_h'] 					= $row['size_h'];
							$arr['technique_techId'] 		= $row['technique_techId'];
							$arr['technique_techName'] 		= $row['technique_techName'];
							$arr['technique_intNoOfShots'] 	= $row['technique_intNoOfShots'];
							$arr['technique_itemId'] 		= $row['technique_itemId'];
							$arr['technique_itemName'] 		= $row['technique_itemName'];
							$arr['technique_dblColorWeight']= $row['technique_dblColorWeight'];*/
                        $entry = array() ;
                        $entryKey = $arrCombo[0]['combo'].'print '.($i +1);
				?>
                  <tr>
                    <td bgcolor="#ffffffff" class="normalfnt" id="<?php echo $arrT['intColorId'] ?>"><img src="images/del.png" class="mouseover removeRow" /><?php echo $arrT['colorName']; $entry["colorName"] = $arrT['colorName']; $pdfEntries[$entryKey]["combo"] = $arrCombo[0]['combo'];$pdfEntries[$entryKey]["print"] = 'print '.($i +1);  ?></td>
                    <td bgcolor="#ffffffff" class="normalfnt" ><select id="cboTechnique" name="cboTechnique" class="validate[required] cboTechnique cboTech" style="width:100px">
                      <option value=""></option>
                      <?php
			  	$sql = "SELECT mst_techniques.intId, mst_techniques.strName FROM mst_techniques WHERE mst_techniques.intStatus =  '1' order by strName
						";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
			 
              		if($row['intId']==$arrT['typeOfPrintId']) {
                        echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
                        $entry["techniqueName"] =  $row['strName'];

                    } else {
                        echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
                    }
				}
			  ?>
                    </select></td>
                    <td bgcolor="#ffffffff"><table  bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0" class="tblGridTI" id="tblGridTI">
                    <?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId'],$arrT['intTechniqueId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				$itm_count	=0;
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$itm_count++;
				$item_tech	=	$row_ti['intItem'];
					?>
                    <tr>
                    <td>
              <select name="select3" class="cboItem cboItems techItem" id="cboItem" style="width:100px">
                        <option value=""></option>
                        <?php
			$para = " WHERE mst_techniques_wise_item.TECHNIQUE_ID =  '".$arrT['intTechniqueId']."'";
			
			$sql = "SELECT
					mst_item.intId,
					mst_item.strName
					FROM `mst_item`  
					INNER JOIN mst_techniques_wise_item ON 
					mst_item.intId = mst_techniques_wise_item.ITEM_ID 
					$para 
					ORDER BY
					mst_item.strName ASC ";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
			 
              		if($row['intId']==$item_tech) {
                        echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
                        $entry["itemName"] =  $row['strName'];
                    } else {
                        echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
                    }
				}
			  ?>
                      </select>
                 </td></tr>
                 <?php
					}
					
				if($itm_count==0){ ?>
                    <tr>
                    <td>
              <select name="select3" class="cboItem cboItems techItem" id="cboItem" style="width:100px">
                        <option value=""></option>
                </select>
				<?php } ?>
                 </table></td>
                    <td bgcolor="#ffffffff"><table  bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0" class="tblGridTIQ" id="tblGridTIQ">
                    <?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId'],$arrT['intTechniqueId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];
				
?><tr><td>
                    <input     style="width:30px" value="<?PHP echo ($row_ti['dblQty']<=0?'':$row_ti['dblQty']); $entry["dblQty"] =  $row_ti['dblQty']; ?>"  type="text" name="textfield5" id="txtQty" /><div style="display:none" class="parentItem"><?php echo $item_tech; ?></div>
                    </td></tr>
                    <?php
					}
					?>
                    </table></td>
                    <td bgcolor="#ffffffff" class="normalfnt"><table  bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0" class="tblGridTIWH" id="tblGridTIWH">
                    <?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId'],$arrT['intTechniqueId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];
				?><tr><td nowrap  bgcolor="#ffffffff"><div style="display:none" class="parentItem"><?php echo $item_tech; ?></div>
                  <input   value="<?PHP echo ($row_ti['size_w']<=0?'':$row_ti['size_w']); $entry["size_w"] =  $row_ti['size_w'];?>" style="width:30px"  type="text" name="textfield3" id="txtSizeW" />
                    W
                    <input style="width:30px" value="<?PHP echo ($row_ti['size_h']<=0?'':$row_ti['size_h']); $entry["size_h"] =  $row_ti['size_h'];?>"  type="text" name="textfield4" id="txtSizeH" />
                    H</td></tr>
                    <?php
				}
					?>
                    </table></td>
                    <td bgcolor="#ffffffff"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                      <?php 
					$sql = "	SELECT
									trn_sampleinfomations_details_technical.intInkTypeId,
									trn_sampleinfomations_details_technical.intNoOfShots,
									trn_sampleinfomations_details_technical.intItem,
									trn_sampleinfomations_details_technical.dblColorWeight,
									mst_inktypes.strName AS techName,
									mst_item.strName AS itemName
								FROM
								trn_sampleinfomations_details_technical
									left Join mst_inktypes ON mst_inktypes.intId = trn_sampleinfomations_details_technical.intInkTypeId
									left Join mst_item ON mst_item.intId = trn_sampleinfomations_details_technical.intItem
								WHERE
									trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
									trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
									trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND
									trn_sampleinfomations_details_technical.strPrintName 	=  '".$arrT['printName']."' AND
									trn_sampleinfomations_details_technical.strComboName 	=  '".$arrT['combo']."' AND
									trn_sampleinfomations_details_technical.intColorId 		=  '".$arrT['intColorId']."'
								";
						$result_tech = $db->RunQuery($sql);
						while($row_tech=mysqli_fetch_array($result_tech))
						{
                            $inkEntry = array();
					?>
                      <tr class="normalfnt">
                        <td width="40%" bgcolor="#FFFFFF"><?php echo $row_tech['techName'];  $inkEntry["2ndtechName"] =  $row_tech['techName']; ?></td>
                        <td width="13%" bgcolor="#FFFFFF"><?php echo $row_tech['intNoOfShots'];  $inkEntry["intNoOfShots"] =  $row_tech['intNoOfShots'];?></td>
                        <td width="35%" bgcolor="#FFFFFF"><?php echo $row_tech['itemName']; $inkEntry["2nditemName"] =  $row_tech['2nditemName'];?></td>
                        <td width="12%" bgcolor="#FFFFFF"><?php echo $row_tech['dblColorWeight']; $inkEntry["dblColorWeight"] =  $row_tech['dblColorWeight'];?></td>
                      </tr>
                      <?php
                            if (isset($entry["inkEntries"])){
                                array_push($entry["inkEntries"], $inkEntry);
                            } else {
                                $entry["inkEntries"] = array();
                                array_push($entry["inkEntries"], $inkEntry);
                            }
						}

						if (isset($pdfEntries[$entryKey]["rows"])){
						    array_push($pdfEntries[$entryKey]["rows"], $entry);
                        } else {
                            $pdfEntries[$entryKey]["rows"] = array();
                            array_push($pdfEntries[$entryKey]["rows"], $entry);
                        }
					?>
                    </table></td>
                  </tr>
                  <?php
					}
				}
				?>
                </table></td>
                
                <?php
					}
				?>
            <td width="15%"  bgcolor="#FFFFFF" style="padding-top:0px" >&nbsp;</td>
          </tr>
          <?php

//          $sampleInfo["prints"] = $arrPart;
//          $sampleInfo["combos"] = $arrCombo;
//          $sampleInfo["arraPrint"] = $arrType;
		  	for($mainRow=1;$mainRow<$rowsCount;$mainRow++)
			{
		  ?>
           <tr class="">
            <td height="18" bgcolor="#CCCCCC" class="gridHeader" ><input class="validate[required,maxSize[20]]" value="<?php echo $arrCombo[$mainRow]['combo']; ?>" style="width:80px" type="text" name="txtCombo2" id="txtCombo2" />
              <img align="right" class="removeRow2 mouseover" src="images/del.png" width="15" height="15" /></td>
				<?php
                 for($i=1;$i<=$cellCount;$i++)
                 {
					 $sql 	= "SELECT
									trn_sampleinfomations_prices.dblPrice,
									costing_sample_header.QTY_PRICES
									FROM
									trn_sampleinfomations_prices left join  costing_sample_header on 
									costing_sample_header.SAMPLE_NO = trn_sampleinfomations_prices.intSampleNo AND
									costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations_prices.intSampleYear AND 
									costing_sample_header.REVISION = trn_sampleinfomations_prices.intRevisionNo AND 
									costing_sample_header.COMBO = trn_sampleinfomations_prices.strCombo AND 
									costing_sample_header.PRINT = trn_sampleinfomations_prices.strPrintName 
									WHERE
									trn_sampleinfomations_prices.intSampleNo = '$sampleNo' AND
									trn_sampleinfomations_prices.intSampleYear = '$sampleYear' AND
									trn_sampleinfomations_prices.intRevisionNo = '$revNo' AND
									trn_sampleinfomations_prices.strCombo ='".$arrCombo[$mainRow]['combo']."' AND
									trn_sampleinfomations_prices.strPrintName = '"."print ".($i)."' 
									";

						$result = 	$db->RunQuery($sql);
						$row	=	mysqli_fetch_array($result);

					 
                ?>
                <td align="right" bgcolor="#FFFFFF" class="normalfntRight" valign="top" ><table width="100%" cellspacing="0" cellpadding="0">
            <tr bgcolor="#CCCCCC"  class="gridHeader"><td width="12%" align="left" class="normalfnt" style="display:none">No of ups</td>
            <td width="7%" style="display:none"><input align="left" value="<?php echo getUps($arrCombo[$mainRow]['combo'],'print '.($i));?>" style="width:40px; text-align:right" type="text" name="ups" id="ups"  class="cls_ups"/></td>
            <td width="31%"><a id="butProcess2" class="button green medium butProcess" style="" name="butProcess"> Routing Process</a>&nbsp;</td>
            <td width="1%" class="normalfnt">&nbsp;</td>
            <td width="18%" align="left">&nbsp;</td>
            <td width="14%"><div style="display:none" class="combo"><?php echo $arrCombo[$mainRow]['combo']; ?></div><div  style="display:none" class="print">print <?php echo $i; ?></div>&nbsp;</td>
            <?php
            if ($row['QTY_PRICES'] == 1){
                ?>
                <td width="27%"  class="normalfnt"><a id="qtyPrices" class="button green medium" style="" name="qtyPrices"> Qty Prices</a></td>
                <?php

            } else {
                ?>
                <td  align="center" bgcolor="#FFFFFF" class="printName" >&nbsp; Price: <?php echo $row['dblPrice']; $pdfEntries[$arrCombo[$mainRow]['combo'].'print '.($i)]["price"] = $row['dblPrice'];?> </td>
                <?php
            }
            ?>
           	</tr>
            </table></td>
                
                <?php
                }
                ?>
            <td class="tdRemove" bgcolor="#FFFFFF" >&nbsp;</td>
          </tr>
                    <tr class="dataRow">
            <td width="18%"  bgcolor="#FFFFFF" >
              <select title="Print Mode" class="printMode validate[required]" name="cboPrintMode2" id="cboPrintMode2" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_printmode where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[$mainRow]['printMode']) {
                            $pdfArrCombo[$arrCombo[$mainRow]["combo"]]["printMode"] = $row['strName'];
                            echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
                        } else {
                            echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
                        }
					}
				?>
                </select>
              <select title="Wash Standerd" class="washStanderd validate[required]" name="butWashingStanderd2" id="butWashingStanderd3" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_washstanderd where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[$mainRow]['wash']){
                            $pdfArrCombo[$arrCombo[$mainRow]["combo"]]["wash"] = $row['strName'];
                            echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
                        } else {
                            echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
                        }
					}
				?>
                </select>
              <select title="Ground Colour" class="colors validate[required]" name="cboGroundColor2" id="cboGroundColor2" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_colors_ground where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[$mainRow]['groundColor']) {
                            $pdfArrCombo[$arrCombo[$mainRow]["combo"]]["groundColor"] = $row['strName'];
                            echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
                        } else {
                            echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
                        }
					}
				?>
                </select>
              <select title="Fabric Type" class="colors validate[required]" name="cboFabricType" id="cboFabricType" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_fabrictype where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[$mainRow]['fabricType']) {
                            $pdfArrCombo[$arrCombo[$mainRow]["combo"]]["fabricType"] = $row['strName'];
                            echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
                        } else {
                            echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
                        }
					}
				?>
              </select></td>
              <?php
			  	/////////// color grid and technique and type of grid cell ////////////
				for($i=1;$i<=$cellCount;$i++)
                 {
			  ?>
            <td  width="67%"  bgcolor="#FFFFFF" style="padding-top:0px" ><table id="tblGrid2" class="tblGrid2" bgcolor="#006666" cellpadding="0" cellspacing="1" width="650" border="0">
              <tr class="normalfntMid">
                <td width="70" bgcolor="#CEE8FB">Color<img  src="images/add_new.png" name="butColors" width="15" height="15" class="mouseover butColors" id="butColors3" /></td>
                <td width="100" bgcolor="#CEE8FB">Technique</td>
                <td width="100" bgcolor="#CEE8FB">Item</td>
                <td width="40" bgcolor="#CEE8FB">Qty</td>
                <td width="86" bgcolor="#CEE8FB">Size</td>
                <td bgcolor="#FFE9D2"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                  <tr class="normalfnt">
                    <td  width="40%" bgcolor="#DFFFDF">Ink Type</td>
                    <td width="13%" bgcolor="#DFFFDF">Shots</td>
                    <td width="35%" bgcolor="#DFFFDF">Item</td>
                    <td width="12%" bgcolor="#DFFFDF">Weight</td>
                  </tr>
                </table></td>
              </tr>
              <?php
			// print_r($arrCombo[0]['combo']);

				foreach($arrType as $arrT)
				{
					//echo $arrT['combo'];
					//echo $arrCombo[$mainRow]['combo'].'<br>';
					if($arrT['combo']==$arrCombo[$mainRow]['combo'] && $arrT['printName']=='print '.$i)
					{
                        $entry = array();
					    $entryKey = $arrCombo[$mainRow]['combo'].'print '.$i;
                        $pdfEntries[$entryKey]["combo"] = $arrCombo[$mainRow]['combo'];
                        $pdfEntries[$entryKey]["print"] = 'print '.$i;
/*						
							$arr['combo'] 				= $row['strComboName'];
							$arr['printName'] 			= $row['strPrintName'];
							
							$arr['intColorId'] 			= $row['intColorId'];
							$arr['marketing_itemId'] 	= $row['marketing_itemId'];
							$arr['colorName'] 			= $row['colorName'];
							$arr['typeOfPrintName'] 	= $row['typeOfPrintName'];
							$arr['typeOfPrintId'] 		= $row['typeOfPrintId'];
							$arr['marketing_itemName'] 	= $row['marketing_itemName'];
							$arr['dblQty'] 				= $row['dblQty'];
							
							$arr['size_w'] 					= $row['size_w'];
							$arr['size_h'] 					= $row['size_h'];
							$arr['technique_techId'] 		= $row['technique_techId'];
							$arr['technique_techName'] 		= $row['technique_techName'];
							$arr['technique_intNoOfShots'] 	= $row['technique_intNoOfShots'];
							$arr['technique_itemId'] 		= $row['technique_itemId'];
							$arr['technique_itemName'] 		= $row['technique_itemName'];
							$arr['technique_dblColorWeight']= $row['technique_dblColorWeight'];*/
				?>
              <tr>
                <td bgcolor="#ffffffff" class="normalfnt" id="<?php echo $arrT['intColorId'] ?>"><img src="images/del.png" class="mouseover removeRow" /><?php echo $arrT['colorName'] ; $entry['colorName'] = $arrT['colorName'];?></td>
                <td bgcolor="#ffffffff" class="normalfnt" ><select id="cboTechnique" name="select3" class="validate[required] cboItem cboTech" style="width:100px">
                  <option value=""></option>
                  <?php
			  	$sql = "SELECT mst_techniques.intId, mst_techniques.strName FROM mst_techniques WHERE mst_techniques.intStatus =  '1' order by strName
						";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
			 
              		if($row['intId']==$arrT['typeOfPrintId']){
                        $pdfEntries[$entryKey]['techniqueName'] = $row['strName'];
                        $entry["techniqueName"] =  $row['strName'];
                        echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
                    } else {
                        echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
                    }
				}
			  ?>
                </select></td>
                <td bgcolor="#ffffffff"><table  bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0" class="tblGridTI" id="tblGridTI">
                    <?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId'],$arrT['intTechniqueId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				$itm_count	=0;
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$itm_count++;
				$item_tech	=	$row_ti['intItem'];
					?>
                    <tr>
                    <td>
              <select name="select3" class="cboItem cboItems techItem" id="cboItem" style="width:100px">
                        <option value=""></option>
                        <?php
			$para = " WHERE mst_techniques_wise_item.TECHNIQUE_ID =  '".$arrT['intTechniqueId']."'";
			
			$sql = "SELECT
					mst_item.intId,
					mst_item.strName
					FROM `mst_item`  
					INNER JOIN mst_techniques_wise_item ON 
					mst_item.intId = mst_techniques_wise_item.ITEM_ID 
					$para 
					ORDER BY
					mst_item.strName ASC ";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
			 
              		if($row['intId']==$item_tech) {
                        $pdfEntries[$entryKey]['itemName'] = $row['strName'];
                        $entry['itemName'] = $row['strName'];
                        echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
                    } else {
                        echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
                    }
				}
			  ?>
                      </select>
                 </td></tr>
                 <?php
					}
					
				if($itm_count==0){ ?>
                    <tr>
                    <td>
              <select name="select3" class="cboItem cboItems techItem" id="cboItem" style="width:100px">
                        <option value=""></option>
                </select>
				<?php } ?>
                 </table></td>
                <td bgcolor="#ffffffff"><table  bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0" class="tblGridTIQ" id="tblGridTIQ">
                    <?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId'],$arrT['intTechniqueId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];
				
?><tr><td>
                    <input     style="width:30px" value="<?PHP echo ($row_ti['dblQty']<=0?'':$row_ti['dblQty']); $entry['dblQty'] = $row['dblQty']; ?>"  type="text" name="textfield5" id="txtQty" /><div style="display:none" class="parentItem"><?php echo $item_tech; ?></div>
                    </td></tr>
                    <?php
					}
					?>
                    </table></td>
                <td bgcolor="#ffffffff" class="normalfnt"><table  bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0" class="tblGridTIWH" id="tblGridTIWH">
                    <?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId'],$arrT['intTechniqueId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];
				?><tr><td nowrap  bgcolor="#ffffffff"><div style="display:none" class="parentItem"><?php echo $item_tech; ?></div>
                  <input   value="<?PHP echo ($row_ti['size_w']<=0?'':$row_ti['size_w']); $entry['size_w'] = $row_ti['size_w'];?>" style="width:30px"  type="text" name="textfield3" id="txtSizeW" />
                    W
                    <input style="width:30px" value="<?PHP echo ($row_ti['size_h']<=0?'':$row_ti['size_h']); $entry['size_h'] = $row_ti['size_h'];?>"  type="text" name="textfield4" id="txtSizeH" />
                    H</td></tr>
                    <?php
				}
					?>
                    </table></td>
                <td bgcolor="#ffffffff"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
						<?php
						$sql = "SELECT
									trn_sampleinfomations_details_technical.intInkTypeId,
									trn_sampleinfomations_details_technical.intNoOfShots,
									trn_sampleinfomations_details_technical.intItem,
									trn_sampleinfomations_details_technical.dblColorWeight,
									mst_inktypes.strName AS techName,
									mst_item.strName AS itemName
								FROM
								trn_sampleinfomations_details_technical
									left Join mst_inktypes ON mst_inktypes.intId = trn_sampleinfomations_details_technical.intInkTypeId
									left Join mst_item ON mst_item.intId = trn_sampleinfomations_details_technical.intItem
								WHERE
									trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
									trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
									trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND
									trn_sampleinfomations_details_technical.strPrintName 	=  '".$arrT['printName']."' AND
									trn_sampleinfomations_details_technical.strComboName 	=  '".$arrT['combo']."' AND
									trn_sampleinfomations_details_technical.intColorId 		=  '".$arrT['intColorId']."'
								";
						$result_tech = $db->RunQuery($sql);
						while($row_tech=mysqli_fetch_array($result_tech))
						{
                            $inkEntry = array();
					?>
                  <tr class="normalfnt">
                    <td width="40%" bgcolor="#FFFFFF"><?php echo $row_tech['techName']; $inkEntry['2ndtechName'] = $row_tech['techName'];?></td>
                    <td width="13%" bgcolor="#FFFFFF"><?php echo $row_tech['intNoOfShots']; $inkEntry['intNoOfShots'] = $row_tech['intNoOfShots']; ?></td>
                    <td width="35%" bgcolor="#FFFFFF"><?php echo $row_tech['itemName']; $inkEntry['2nditemName'] = $row_tech['itemName'];?></td>
                    <td width="12%" bgcolor="#FFFFFF"><?php echo $row_tech['dblColorWeight']; $inkEntry['dblColorWeight'] = $row_tech['dblColorWeight'];?></td>
                  </tr>
                  <?php
                            if (isset($entry["inkEntries"])){
                                array_push($entry["inkEntries"], $inkEntry);
                            } else {
                                $entry["inkEntries"] = array();
                                array_push($entry["inkEntries"], $inkEntry);
                            }
						}
					?>
                </table></td>
              </tr>
              <?php

                        if (isset($pdfEntries[$entryKey]["rows"])){
                            array_push($pdfEntries[$entryKey]["rows"], $entry);
                        } else {
                            $pdfEntries[$entryKey]["rows"] = array();
                            array_push($pdfEntries[$entryKey]["rows"], $entry);
                        }
					}
				}
				?>
            </table></td>
            <?php
				 }
			?>
              <td class="tdRemove" bgcolor="#FFFFFF" >&nbsp;</td>
          </tr>
          <?php
			}
		  ?>
          
          <tr class="dataRow">
            <td  bgcolor="#FFFFFF" ><img src="images/insert.png" name="butInsertRow" width="78" height="24" class="mouseover" id="butInsertRow" /></td>
            <td  bgcolor="#FFFFFF" style="padding-top:0px" >&nbsp;</td>
            <?php
				for($i=1;$i<$cellCount;$i++)
				{
			?>
            <td class="tdRemove"  bgcolor="#FFFFFF" style="padding-top:0px" >&nbsp;</td>
            <?php
				}
			?>
            <td  bgcolor="#FFFFFF" style="padding-top:0px" >&nbsp;</td>
          </tr>
          </table>
          <p>&nbsp;</p></td>
        </tr>
      <tr>
                <?php
//                var_dump("row entries are : ");
//                var_dump($pdfEntries);
//                die();
                $sampleInfo["entries"] = $pdfEntries;
                $sampleInfo["combos"] = $pdfArrCombo;
                $sampleInfo["lightBoxes"] = $lightBoxes;

			  	$sql = "
						SELECT
						mst_maincategory.strName AS main_cat,
						mst_subcategory.strName AS sub_cat,
						trn_sample_non_direct_rm_consumption.ITEM as item_id,
						mst_item.strName AS item,
						mst_units.strCode,
						trn_sample_non_direct_rm_consumption.CONSUMPTION 
						FROM
						trn_sample_non_direct_rm_consumption
						INNER JOIN mst_item ON trn_sample_non_direct_rm_consumption.ITEM = mst_item.intId
						INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
						INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
						INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
						WHERE
						trn_sample_non_direct_rm_consumption.SAMPLE_NO = '$sampleNo' AND
						trn_sample_non_direct_rm_consumption.SAMPLE_YEAR = '$sampleYear' AND
						trn_sample_non_direct_rm_consumption.REVISION_NO = '$revNo'
						ORDER BY
						main_cat ASC,
						sub_cat ASC,
						item ASC				";
				$result = $db->RunQuery($sql);
				$i=0;
				while($row=mysqli_fetch_array($result))
				{
					$i++;
					?>
					<?php
					if($i==1){
					?>
              <tr><td colspan="2"  bgcolor="#F4D3C6" class="normalfnt">Non-Direct Raw Materials</td></tr>
                    <tr><td colspan="2">
                    <table id="tblNoneDirRm" class="tblGrid" bgcolor="#006666" cellpadding="0" cellspacing="1" width="650" border="0">
                    <tr class="normalfntMid">
                    <td width="151" bgcolor="#CEE8FB">Main Category</td>
                    <td width="137" bgcolor="#CEE8FB">Sub Category</td>
                    <td width="137" bgcolor="#CEE8FB">Item</td>
                    <td width="55" bgcolor="#CEE8FB">UOM</td>
                    <td width="122" bgcolor="#CEE8FB">Consumption</td>
                    </tr>
					<?php
					}
					?>
                   <tr>
                     <td bgcolor="#ffffffff" class="normalfnt" id=""><?php echo $row['main_cat'] ?></td>
                    <td bgcolor="#ffffffff" class="normalfnt" ><?php echo $row['sub_cat'] ?></td>
                    <td bgcolor="#ffffffff" class="cls_item"><div style="display:none" id="item"><?php echo $row['item_id'] ?></div><span class="normalfnt"><?php echo $row['item'] ?></span></td>
                    <td bgcolor="#ffffffff"><span class="normalfnt"><?php echo $row['strCode'] ?></span></td>
                    <td bgcolor="#ffffffff" class="normalfntRight" align="right"><?php echo $row['CONSUMPTION'] ?> &nbsp;</td>
                    </tr>
                <?php
				}
				?>
             </table>       </td></tr>
      <tr>
        <td colspan="2" align="center" class=""><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="19" class="normalfnt">&nbsp;</td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td width="12%" height="19" class="normalfnt">Light Box</td>
            <td width="352%" colspan="4"><table width="72%" height="38" border="0" cellpadding="0" cellspacing="1" bgcolor="#CCCCCC">
              <tr class="normalfntMid">
                <td colspan="5" align="center" bgcolor="#FFFFFF">Verivide</td>
                <td colspan="6" align="center" bgcolor="#FFFFFF">Macbeth</td>
                </tr>
                
              <tr class="normalfntMid">
                <td width="6%" bgcolor="#FFFFFF">D65<strong> <br /><input <?php echo ($verivide_d65?'checked':'') ?> type="checkbox" name="txtVerivide_D65" id="txtVerivide_D65" />
                  </strong></td>
                <td width="9%" bgcolor="#FFFFFF">TL84<strong> <br/><input <?php echo ($verivide_tl84?'checked':'') ?> type="checkbox" name="txtVerivide_TL84" id="txtVerivide_TL84" />
                  </strong></td>
                <td width="11%" bgcolor="#FFFFFF">CW<strong> <br/><input <?php echo ($verivide_cw?'checked':'') ?> type="checkbox" name="txtVerivide_CW" id="txtVerivide_CW" />
                  </strong></td>
                <td width="11%" bgcolor="#FFFFFF">F<strong> <br/> <input <?php echo ($verivide_f?'checked':'') ?> type="checkbox" name="txtVerivide_F" id="txtVerivide_F" />
                  </strong></td>
                <td width="11%" bgcolor="#FFFFFF">UV<strong> <br/> <input <?php echo ($verivide_uv?'checked':'') ?> type="checkbox" name="txtVerivide_UV" id="txtVerivide_UV" />
                  </strong></td>
                <td width="8%" bgcolor="#FFFFFF">DL<strong> <br/>  <input <?php echo ($macbeth_dl?'checked':'') ?> type="checkbox" name="txtMacbeth_DL" id="txtMacbeth_DL" />
                  </strong></td>
                <td width="9%" bgcolor="#FFFFFF">CW<strong> <br/>  <input <?php echo ($macbeth_cw?'checked':'') ?> type="checkbox" name="txtMacbeth_CW" id="txtMacbeth_CW" />
                  </strong></td>
                <td width="11%" bgcolor="#FFFFFF">Inca<strong> <br/>  <input <?php echo ($macbeth_inca?'checked':'') ?> type="checkbox" name="txtMacbeth_INCA" id="txtMacbeth_INCA" />
                  </strong></td>
                <td width="8%" bgcolor="#FFFFFF">TL84<strong> <br/>  <input <?php echo ($macbeth_tl84?'checked':'') ?> type="checkbox" name="txtMacbeth_TL84" id="txtMacbeth_TL84" />
                  </strong></td>
                <td width="9%" bgcolor="#FFFFFF">UV<strong> <br/>  <input <?php echo ($macbeth_uv?'checked':'') ?> type="checkbox" name="txtMacbeth_UV" id="txtMacbeth_UV" />
                  </strong></td>
                <td width="7%" bgcolor="#FFFFFF">Horizon<strong> <br/> <input <?php echo ($macbeth_horizon?'checked':'') ?> type="checkbox" name="txtMacbeth_Horizon" id="txtMacbeth_Horizon" />
                  </strong></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td height="45" class="normalfnt">&nbsp;</td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td height="45" colspan="5" class="normalfnt"><table width="762" border="0" class="tableBorder_topRound">
              <tr>
                <td colspan="2" bgcolor="#CCCCCC" class="normalfnt"><strong>Technical Data</strong></td>
                </tr>
              <tr>
                <td width="129" height="47"><span class="normalfnt">Curing Condition</span></td>
                <td width="621"><table width="63%" height="38" border="0" cellpadding="0" cellspacing="0" >
                  <tr>
                    <td width="44%" bgcolor="#FFFFFF" class="normalfntMid"><span class="normalfnt"><strong>TEMP</strong>                      
                      <input name="txtCuringTemp" type="text" disabled="disabled" class=" " id="txtCuringTemp" style="width:170px" value="<?php echo $curing_temp; $sampleInfo["curingTemp"] = $curing_temp;?>" />
                    </span></td>
                    <td width="56%" bgcolor="#FFFFFF" class="normalfntMid"><span class="normalfnt"><strong>BELT SPEED
                      <input name="txtCuringBeltSpeed" type="text" disabled="disabled" class=" " id="txtCuringBeltSpeed" style="width:170px" value="<?php echo $curing_beltspeed; $sampleInfo["curingBeltSpeed"] = $curing_beltspeed?>" />
                    </strong></span></td>
                    </tr>
                  </table></td>
                </tr>
              <tr>
                <td height="49"><span class="normalfnt">Press Condition</span></td>
                <td><table width="72%" height="38" border="0" cellpadding="0" cellspacing="0" >
                  <tr class="normalfntMid">
                    <td width="6%" bgcolor="#FFFFFF"><span class="normalfnt"><strong>TEMP</strong>                      
                      <input name="txtPressTemp" type="text" disabled="disabled" class="" id="txtPressTemp" style="width:170px" value="<?php echo $press_temp; $sampleInfo["pressTemp"] = $press_temp;?>" />
                    </span></td>
                    <td width="5%" bgcolor="#FFFFFF"><span class="normalfnt"><strong>Pressure
                      <input name="txtPressPressure" type="text" disabled="disabled" class="" id="txtPressPressure" style="width:170px" value="<?php echo $press_pressure; $sampleInfo["pressPressure"] = $press_pressure;?>" />
                      </strong></span></td>
                    <td width="6%" bgcolor="#FFFFFF"><span class="normalfnt"><strong>Time
                      <input name="txtPressTime" type="text" disabled="disabled" class="" id="txtPressTime" style="width:170px" value="<?php echo $press_time; $sampleInfo["pressTime"] = $press_time;?>" />
                      </strong></span></td>
                    </tr>
                  </table></td>
                </tr>
              <tr>
                <td><span class="normalfnt">Mesh Count</span></td>
                <td><span class="normalfnt">
                  <input name="txtMeshCount" type="text" disabled="disabled" class="" id="txtMeshCount" style="width:570px" value="<?php echo $strMeshCount; $sampleInfo["meshCount"] = $strMeshCount?>" />
                  </span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">Technical Instructions</td>
                <td><textarea disabled="disabled"  name="txtInstructionsTech" id="txtInstructionsTech" cols="89" rows="5"><?php echo $instructionsTech; $sampleInfo["techInstructions"] = $instructionsTech?></textarea></td>
              </tr>
              </table></td>
          </tr>
          <tr>
            <td height="23" class="normalfnt">&nbsp;</td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td height="23" class="normalfnt">Additional Instructions</td>
            <td colspan="4"><textarea  name="txtInstructions" id="txtInstructions" cols="89" rows="5"><?php echo $instructions;  $sampleInfo["additionalInstructions"] = $instructions;?></textarea></td>
          </tr>
          <tr>
			  <?php
              $_SESSION["SAMPLE_PDF_DATA"] 		= $sampleInfo;
//              var_dump("this is my list");
//              var_dump($sampleInfo);
			  if($userPermision && $intMarketingStatus>1 )
			  {
				  $edit_mode= 1;
			  }else {
				  $edit_mode = 0;
			  }

			  ?>
            <td height="23" valign="top" class="normalfnt">Upload Inquiry</td>
            <td colspan="4"><iframe id="iframeFiles" src="presentation/customerAndOperation/sample/sampleInfomations/addNew/filesUpload.php?txtFolder=<?php if($sampleNo !='' && $sampleYear !=''){echo "$sampleNo-$sampleYear".'&approved='.$edit_mode;}else{ echo "-";}  ?>" name="iframeFiles" style="width:400px;height:200px;border:none"  ></iframe></td>
          </tr>
          <tr>
            <td height="23" class="normalfnt">&nbsp;</td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td height="23" class="normalfnt">&nbsp;</td>
            <td colspan="4"><select  class="cboItemDumy" name="cboItemDumy" id="cboItemDumy" style="width:100px;display:none">
              <option value=""></option>
              <?php
					$sql = "select intId,strName from mst_item where intStatus = 1 and intSpecialRm=0 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select>
              <select  class="cboItemDumy" name="cboItemDumySPRM" id="cboItemDumySPRM" style="width:100px;display:none">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_item where intStatus = 1 and intSpecialRm = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select>
			  <select  class="cboTechniqueDumy" name="cboTechniqueDumy" id="cboTechniqueDumy" style="width:100px;display:none">
              <option value=""></option>
              <?php
					$sql = "SELECT mst_techniques.intId, mst_techniques.strName FROM mst_techniques WHERE mst_techniques.intStatus =  '1' order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2" align="center" class="tableBorder_allRound">
        <img <?php if($permissionNew==true){'';}else{echo 'style="display:none"';} ?> src="images/Tnew.jpg" width="92" height="24" class="mouseover" id="butNew" />
        <img <?php 
		if($permissionSave==true){'';}else{echo 'style="display:none"';} ?> src="images/Tsave.jpg" width="92" height="24" class="mouseover" id="butSave" />
        <img src="images/Tconfirm.jpg" style="display:none" width="92" height="24" class="mouseover" id="butConfirm" />
		<?php 
		if($intMarketingStatus==1)
		{
		?><a target="_blank" href="?q=354&sampleNo=<?php echo $sampleNo; ?>&year=<?php echo $sampleYear; ?>"><img border="0" src="images/TsampleOrder.png"  width="92" height="24" class="mouseover" id="butReport2" /></a><?php 
		}
		?><a target="_blank" href="?q=990&no=<?php echo $sampleNo; ?>&year=<?php echo $sampleYear; ?>&revNo=<?php echo $revNo; ?>"><?php
        
		if($userPermision && $intMarketingStatus>1 )
		{
		?><img src="images/Tconfirm.jpg" width="92" height="24" class="mouseover"  id="butReport" /><?php
		
		}
		?><img src="images/Treport.jpg" width="92" height="24" class="mouseover"  id="butReport" /></a>
        <?php if($sampleNo!='' && $sampleYear !='' && $revNo != ''){ ?><a id="butPDF" class="button white medium" style="vertical-align:top" name="butPDF"> PDF </a><?php } ?>
        <img src="images/Tclose.jpg" width="92" height="24" class="mouseover" /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<!-- technique -->
<!--	<div  style="position: absolute;display:none;z-index:100"  id="popupContact1">
    <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="<?php echo $mainPath; ?>presentation/customerAndOperation/masterData/technique/addNew/technique.php" style="width:800;height:400;border:0;overflow:hidden">
    </iframe>
    </div>-->
    
    <!-- print Mode -->
<!--	<div  style="position: absolute;display:none;z-index:100"  id="popupContact2">
    <iframe onload="loadPrintMode();"   id="iframeMain2" name="iframeMain2" src="<?php echo $mainPath; ?>presentation/customerAndOperation/masterData/printMode/addNew/printMode.php" style="width:800;height:400;border:0;overflow:hidden">
    </iframe>
    </div>-->
    
        <!-- washing Condition -->
<!--    <div  style="position: absolute;display:none;z-index:100"  id="popupContact3">
    <iframe onload="loadWasingCondistion();"   id="iframeMain3" name="iframeMain3" src="<?php echo $mainPath; ?>presentation/customerAndOperation/masterData/washStanderd/addNew/washStanderd.php" style="width:800;height:400;border:0;overflow:hidden">
    </iframe>
    </div>-->
    
    <!-- colors -->
<!--    <div  style="position: absolute;display:none;z-index:100"  id="popupContact4">
    <iframe onload="loadColors();"   id="iframeMain4" name="iframeMain4" src="<?php echo $mainPath; ?>presentation/customerAndOperation/masterData/colors/addNew/colors.php" style="width:800;height:400;border:0;overflow:hidden">
    </iframe>
    </div>-->
    
      <!-- Part -->
<!--    <div  style="position: absolute;display:none;z-index:100"  id="popupContact5">
    <iframe onload="loadPart();"   id="iframeMain5" name="iframeMain5" src="<?php echo $mainPath; ?>presentation/customerAndOperation/masterData/part/addNew/part.php" style="width:800;height:400;border:0;overflow:hidden">
    </iframe>
    </div>-->
    
	 <!-- Part -->
<!--    <div  style="position: absolute;display:none;z-index:100"  id="popupContact6">
    <iframe onload="loadTypeOfPrint();"   id="iframeMain6" name="iframeMain6" src="<?php echo $mainPath; ?>presentation/workStudy/masterData/typeOfPrint/addNew/typeOfPrint.php" style="width:800;height:400;border:0;overflow:hidden">
    </iframe>
    </div>
    -->
    <div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
    <div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</body>
<?php
  function getTechniqueItems($sampleNo,$sampleYear,$revNo,$combo,$print,$color,$technique){
	  
	 $sql = "SELECT DISTINCT
			trn_sampleinfomations_details.intItem,
			trn_sampleinfomations_details.dblQty,
			trn_sampleinfomations_details.size_w,
			trn_sampleinfomations_details.size_h 
			FROM trn_sampleinfomations_details
			WHERE
			trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
			trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
			trn_sampleinfomations_details.intRevNo 		=  '$revNo' AND
			trn_sampleinfomations_details.strComboName 		=  '$combo' AND
			trn_sampleinfomations_details.strPrintName 		=  '$print' AND
			trn_sampleinfomations_details.intColorId 		=  '$color' AND
			trn_sampleinfomations_details.intTechniqueId 		=  '$technique' 
			order by trn_sampleinfomations_details.intItem asc
			";
	return $sql;  
  }
  
  function getUps($combo,$print){
		
		global $db;
		global $sampleNo;
		global $sampleYear;
		global $revNo;

		$sql		="SELECT
					trn_sampleinfomations_combo_print_details.NO_OF_UPS
					FROM `trn_sampleinfomations_combo_print_details`
					WHERE
					trn_sampleinfomations_combo_print_details.SAMPLE_NO = '$sampleNo' AND
					trn_sampleinfomations_combo_print_details.SAMPLE_YEAR = '$sampleYear' AND
					trn_sampleinfomations_combo_print_details.REVISION = '$revNo' AND
					trn_sampleinfomations_combo_print_details.COMBO = '$combo' AND
					trn_sampleinfomations_combo_print_details.PRINT = '$print'
					";	  
		$result 	= $db->RunQuery($sql);
		$row		=mysqli_fetch_array($result);
		$ups 	= $row['NO_OF_UPS'];
		if($ups=='')
			$ups	=0;
		return $ups;
  }
?>


<!--<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>-->
<link href="libraries/select2/select2.min.css" rel="stylesheet" />
<script src="libraries/select2/select2.min.js"></script>
<script>	
$(".js-example-data-ajax").select2({
	
 placeholder: "Search for a repository",
	
ajax: {
    url: "presentation/customerAndOperation/sample/sampleInfomations/addNew/sampleInfomations-db-get.php",
    dataType: 'json',
    delay: 250,
    data:  function (params) {
      return {
        val: (params.term == null) ? 'ALLSELECT' : params.term, // search term
        page: params.page,
		sampleYear : <?php echo $sampleYear; ?>
      };
    },
    processResults: function (data,params) {
      // parse the results into the format expected by Select2
      // since we are using custom formatting functions we do not need to
      // alter the remote JSON data, except to indicate that infinite
      // scrolling can be used
      params.page = params.page || 1;

      return {
        results: data.items,
        pagination: {
          more: (params.page * 10) < data.total_count
        }
      };
    },
    cache: true
  },
  placeholder: 'Search for a repository',
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 0,
  templateResult: formatRepo,
  templateSelection: formatRepoSelection
});
// window.onload = function() {
    // if(!window.location.hash) {
        // window.location = window.location + '#loaded';
        // window.location.reload();
    // }
// }
$(document).ready(function() {
$("#select2-cboStyleNo-container").text("<?php echo $graphicNo; ?>");
});

function formatRepo (repo) {
  if (repo.loading) {
    return repo.text;
  }

  return repo.sampleNO;
}

function formatRepoSelection (repo) {
  return repo.sampleNO;
}
				
				
	</script>
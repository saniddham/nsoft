var basepath = 'presentation/customerAndOperation/sample/sampleInfomations/addNew/';
var costingBasePath	="presentation/costing/sample/priceCosting/addNew/"
var arr = [];
var techniqueTableScript = '';

$(document).ready(function () {

    $('#frmSampleInfomations #cboRequisitionNo').die('change').live('change', loadRequisitionDetails);

    $("#cboCustomer").die('change').live('change', function () {
        //loadSubCategory($(this).val(),this);
        checkCustomer($(this).val());
        //  alert("hdh");
    });

    /*  $('#frmColors #txtColorName').die('keyup').live('keyup',function(){
     //var url = basepath+"sampleInfomations-db-get.php?requestType=loadColors&text="+$('#txtColorName').val();
     //var obj = $.ajax({url:url,async:false});
     //$('#tblColors').html(obj.responseText);
     // alert("jdjdj");
     loadColorTxt();
     });  */

    $('#frmColors #butSearch').die('click').live('click', function () {
        var url = basepath + "sampleInfomations-db-get.php?requestType=loadColors&text=" + $('#txtColorName').val();
        var obj = $.ajax({url: url, async: false});
        $('#tblColors').html(obj.responseText);
    });
    $('#frmSampleInfomations #cboYear').die('change').live('change', function () {
        document.location.href = '?q=27&sampleYear=' + $('#cboYear').val();
        //loadGraphicNo();
        //loadSampleNo();
    });

    $('#frmSampleInfomations .cboTech').die('change').live('change', function () {
        loadTechItems(this);
    });

    $('#frmSampleInfomations .techItem').die('change').live('change', function () {
        setParentItemsForQtyWHGrid(this);
    });


    $.fn.removeCol = function (col) {
        // Make sure col has value
        if (!col) {
            col = 1;
        }
        $('tr td:nth-child(' + col + '), tr th:nth-child(' + col + ')', this).remove();
        return this;
    };
    //$('select').change(function(){
    //alert(1);
    //$("#cboStyleNo option[value=\"a\"]").attr("selected","selected") ;
    //});
    ////////////// button events //////////////////////////
    $("#frmSampleInfomations .butTechnique").die('click').live('click', function () {
        setToTechnique(this);
    });
    $("#frmSampleInfomations .butTypeOfPrint").die('click').live('click', function () {
        setToTypeButton(this);
    });
    $("#frmSampleInfomations .butColors").die('click').live('click', function () {
        setToColorGrid(this);

    });
    $("#frmSampleInfomations .removeRow").die('click').live('click', function () {
        removeRow(this);
    });
    $("#frmSampleInfomations .removeRow2").die('click').live('click', function () {
        removeRow2(this);
    });


    techniqueTableScript = "<table class=\"tblTechnique\" width=\"322\" border=\"0\" cellspacing=\"1\" cellpadding=\"0\">" +
        " <tr class=\"normalfntMid\">" +
        "<td width=\"179\" bgcolor=\"#FBF2D7\">Technique<img  src=\"images/add_new.png\" name=\"butTechnique\" width=\"15\" height=\"15\" class=\"mouseover butTechnique\" id=\"butTechnique\" /></td>" +
        " <td width=\"40\" bgcolor=\"#FBF2D7\">Qty</td>" +
        " <td width=\"121\" bgcolor=\"#FBF2D7\">Item</td>" +
        "</tr>" +

        "</table>";
    //clickEventForAddColumn();


    $.fn.sort_select_box = function () {
        // Get options from select box
        var my_options = $("#" + this.attr('id') + ' option');
        // sort alphabetically
        my_options.sort(function (a, b) {
            if (a.text > b.text) return 1;
            else if (a.text < b.text) return -1;
            else return 0
        })


        /// remove collumn function ///////////////////
        $.fn.removeCol = function (col) {
            // Make sure col has value
            if (!col) {
                col = 1;
            }
            $('tr td:nth-child(' + col + '), tr th:nth-child(' + col + ')', this).remove();
            return this;
        };

        //replace with sorted my_options;
        $(this).empty().append(my_options);

        // clearing any selections
        $("#" + this.attr('id') + " option").attr('selected', false);
    }


    $("#frmSampleInfomations").validationEngine();
    $('#frmSampleInfomations #txtCode').focus();


    setButtonEvents();


    //alert(isOldRevNo);
    //permision for add
    /*  if(intAddx)
     {
     $('#frmSampleInfomations #butNew').show();
     if(isOldRevNo==0)
     $('#frmSampleInfomations #butSave').show();
     }

     //permision for edit
     if(intEditx)
     {
     if(isOldRevNo==0)
     $('#frmSampleInfomations #butSave').show();
     $('#frmSampleInfomations #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
     }

     //permision for delete
     if(intDeletex)
     {
     $('#frmSampleInfomations #butDelete').show();
     $('#frmSampleInfomations #cboSearch').removeAttr('disabled');
     }

     //permision for view
     if(intViewx)
     {
     $('#frmSampleInfomations #cboSearch').removeAttr('disabled');
     }*/

    $('#frmSampleInfomations .removeColumn').die('click').live('click', function () {

        //$(this).parent().next().remove();
        //$('#tblMain').removeCol(2);
        var cel = $(this).parent();
        var celIndex = cel.parent().children().index(cel);
        //alert(celIndex);

        var rows = $('#tblMain >tbody >tr').length;
        for (var i = 0; i <= rows; i++)
            $('#tblMain >tbody >tr:eq(' + i + ') >td:eq(' + celIndex + ')').remove();


        //$('#tblMain >tbody >tr:eq(1) >td:eq('+celIndex+')').remove();
        //$('#tblMain >tbody >tr:eq(2) >td:eq('+celIndex+')').remove();
        //$('#tblMain >tbody >tr:eq(3) >td:eq('+celIndex+')').remove();
        //	$('#tblMain >tbody >tr:eq(4) >td:eq('+celIndex+')').remove();


        //var cellCount = ($(this).parent().parent().find('td').length);
        //alert(cellCount);
        //if(cellCount>3)
        //$('#tblMain >tbody >tr').each(function(){
        //$(this).find('td:eq('+celIndex+')').remove('.tdRemove');
        //$(this).closest('td:eq('+celIndex+')').remove();
        //$(this).find('td:last-child').remove();
        //});
        //$('#tblMain >tbody >tr td:nth-child('+(celIndex+1)+')').remove();
    });

    /*  $(':input').bind('keyup mouseup', function(){
     $(this).val($.trim($(this).val())) ;
     });*/


    $('#frmSampleInfomations #cboCustomer').die('change').live('change', function () {
        $('#frmSampleInfomations #cboBrand').html(combo_brand($(this).val()));
    });

    $("#frmSampleInfomations .butProcess").die('click').live('click', function () {
        addProcess(this);

    });

    ///save button click event
    $('#frmSampleInfomations #butSave').die('click').live('click', function () {
        //$('#frmSampleInfomations').submit();
        //$('#frmSampleInfomations #butSave').hide();
        var requestType = '';
        if ($('#frmSampleInfomations').validationEngine('validate')) {
            showWaiting();
            /////////////////////////////// validate this is open by technical or not //////////////////////
            var url = basepath + "sampleInfomations-db-get.php?requestType=checkNewRevision";
            url += "&sampleNo=" + $('#cboSampleNo').val();
            url += "&sampleYear=" + $('#cboYear').val();
            url += "&revisionNo=" + $('#cboRevisionNo').val();
            var obj = $.ajax({url: url, async: false});
            if (obj.responseText == 'true') {
                hideWaiting();
                var val = $.prompt('Are you sure you want to save this sample as a new Revision', {
                    buttons: {Ok: true, Cancel: false},
                    callback: function (v, m, f) {
                        if (!v) {
                            hideWaiting();
                            return;
                        }
                        else {
                            showWaiting();
                            saveDetails();
                            hideWaiting();

                        }
                    }
                });

            }
            else {

                saveDetails();
                hideWaiting();
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////


        }
    });

    /////////////////////////////////////////////////////
    //// load sampleInfomations details //////////////////////////
    /////////////////////////////////////////////////////
    $('#frmSampleInfomations #cboRevisionNo').die('click').live('click', function () {
        $('#frmSampleInfomations').validationEngine('hide');
    });
    $('#frmSampleInfomations #cboRevisionNo').die('change').live('change', function () {
        $('#frmSampleInfomations').validationEngine('hide');

        if ($('#frmSampleInfomations #cboRevisionNo').val() == '') {

            var intSampleNo = $('#cboSampleNo').val();
            document.location.href = '?q=27';
            //$('#frmSampleInfomations').get(0).reset();

            $('#cboSampleNo').val(intSampleNo);
            //$('.divPicture').html('');
            return;
        }

        document.location.href = '?q=27&sampleNo=' + $('#cboSampleNo').val() + '&sampleYear=' + $('#cboYear').val() + '&revNo=' + $('#cboRevisionNo').val();
        return;

        var url = basepath + 'sampleInfomations-db-get.php?requestType=loadMainDetails';
        var httpobj = $.ajax({
            url: url,
            dataType: 'json',
            data: "sampleNo=" + $('#cboSampleNo').val() + "&sampleYear=" + $('#cboYear').val() + "&revisionNo=" + $('#cboRevisionNo').val(),
            async: false,
            success: function (json) {
                $('#iframeFiles').attr('src', 'filesUpload.php?txtFolder=' + $('#cboSampleNo').val() + '_' + $('#cboYear').val());

                $("#iframeFiles").contents().find("#txtFolder").val($('#cboSampleNo').val() + '_' + $('#cboYear').val());

                //json  = eval('('+json+')');
                $('#frmSampleInfomations #dtDate').val(json.dtDate);
                //$('#frmSampleInfomations #dtDeliveryDate').val(		json.dtDeliveryDate);
                $('#frmSampleInfomations #txtGraphicRefNo').val(json.strGraphicRefNo);
                $('#frmSampleInfomations #cboCustomer').val(json.intCustomer);
                $('#frmSampleInfomations #cboMarketer').val(json.intMarketer);
                $('#frmSampleInfomations #cboCustomer').change();
                $('#frmSampleInfomations #txtStyleNo').val(json.strStyleNo);
                $('#frmSampleInfomations #cboBrand').val(json.intBrand);
                $('#frmSampleInfomations #txtSampleQty').val(json.dblSampleQty);
                $('#frmSampleInfomations #txtGrade').val(json.intGrade);
                $('#frmSampleInfomations #txtFabricType').val(json.strFabricType);
                $('#frmSampleInfomations #txtVerivide_D65').prop('checked', chk(json.verivide_d65));
                $('#frmSampleInfomations #txtVerivide_TL84').prop('checked', chk(json.verivide_tl84));
                $('#frmSampleInfomations #txtVerivide_CW').prop('checked', chk(json.verivide_cw));
                $('#frmSampleInfomations #txtVerivide_F').prop('checked', chk(json.verivide_f));
                $('#frmSampleInfomations #txtVerivide_UV').prop('checked', chk(json.verivide_uv));
                $('#frmSampleInfomations #txtMacbeth_DL').prop('checked', chk(json.macbeth_dl));
                $('#frmSampleInfomations #txtMacbeth_CW').prop('checked', chk(json.macbeth_cw));
                $('#frmSampleInfomations #txtMacbeth_INCA').prop('checked', chk(json.macbeth_inca));
                $('#frmSampleInfomations #txtMacbeth_TL84').prop('checked', chk(json.macbeth_tl84));
                $('#frmSampleInfomations #txtMacbeth_UV').prop('checked', chk(json.macbeth_uv));
                $('#frmSampleInfomations #txtMacbeth_Horizon').prop('checked', chk(json.macbeth_horizon));
                $('#frmSampleInfomations #txtCuringTemp').val(json.dblCuringCondition_temp);
                $('#frmSampleInfomations #txtCuringBeltSpeed').val(json.dblCuringCondition_beltSpeed);
                $('#frmSampleInfomations #txtPressTemp').val(json.dblPressCondition_temp);
                $('#frmSampleInfomations #txtPressPressure').val(json.dblPressCondition_pressure);
                $('#frmSampleInfomations #txtPressTime').val(json.dblPressCondition_time);
                $('#frmSampleInfomations #txtMeshCount').val(json.strMeshCount);
                $('#frmSampleInfomations #txtInstructions').val(json.strAdditionalInstructions);

                $('#divPicture').html('');
                $(document.createElement("img"))
                    .attr({
                        src: 'documents/sampleinfo/samplePictures/' + $('#cboSampleNo').val() + '-' + $('#cboYear').val() + '-' + $('#cboRevisionNo').val() + '.jpg',
                        title: 'Sample Picture '
                    })
                    .appendTo($('#divPicture'))
                    .attr('id', 'saveimg')
                    .click(function () {
                        // Do something
                    })

                //alert(json.arrTechnique[0]);


                ///insert columns /////////////////////////////////////
                clearRows();
                clearColumns();
                //clearSizes();
                //$('#tblMain >select').each;
                //clearColumns();
                for (var i = 1; i < json.columnCount; i++) {
                    clickEventForAddColumn();
                    $('#tblSizes >tbody >tr:last').after('<tr>' + $('#tblSizes >tbody >tr:last').html() + '</tr>');

                    var nextCellId = parseInt($('#tblMain .dataRow').find('td').length) - 5;
                    $('#tblSizes >tbody >tr:last').find('td:eq(0)').html('Print ' + nextCellId);
                }

                ///insert rows /////////////////////////////////////
                for (var i = 1; i < json.rowCount; i++) {
                    $('#tblMain >tbody >tr:last').before('<tr>' + $('#tblMain >tbody >tr:gt(1)').html() + '</tr>');
                }

                ////////////////// set values for main grid ////////////////////////////////////
                var arrCombo = json.arrCombo;
                //alert(json.arrPrintName.length);
                var rowCount1 = document.getElementById('tblMain').rows.length;
                for (var i = 2; i < rowCount1 - 1; i++) {
                    document.getElementById('tblMain').rows[i].cells[0].childNodes[0].value = arrCombo[i - 2]['modeId'];
                    document.getElementById('tblMain').rows[i].cells[1].childNodes[0].value = arrCombo[i - 2]['washId'];
                    document.getElementById('tblMain').rows[i].cells[2].childNodes[0].value = arrCombo[i - 2]['comboName'];
                    document.getElementById('tblMain').rows[i].cells[3].childNodes[0].value = arrCombo[i - 2]['groundId'];

                    //arrInkColor
                    var x = 0;
                    for (var a = 0; a < json.arrInkColor.length; a++) {

                        if (arrCombo[i - 2]['comboName'] == json.arrInkColor[a]['comboName']) {
                            document.getElementById('tblMain').rows[i].cells[4 + x++].childNodes[0].value = json.arrInkColor[a]['color'];
                            //alert(json.arrInkColor[a]['comboName']);
                        }
                    }

                }
                ////////////////////////////////////////////////////////////////////////////////

                loadGridCheck(json.arrTechnique, 'chkTechnique')
                updateTechniqueCombo();
                loadGridCheck(json.arrType, 'chkTypeOfPrint')

                ///////// set report path ///////////////////
                /*$('#butReport').unbind('click');
                 $('#butReport').click(function(){
                 //alert(1);
                 window.open('../../Report/sampleReport.php?no='+$('#cboSampleNo').val()+'&year='+$('#cboYear').val()+'&revNo='+$('#cboRevisionNo').val());
                 });
                 $('#butConfirm').unbind('click');
                 $('#butConfirm').click(function(){
                 window.open('../../Report/sampleReport.php?no='+$('#cboSampleNo').val()+'&year='+$('#cboYear').val()+'&revNo='+$('#cboRevisionNo').val());
                 });*/

                /////////////////////////////////////////////

                var arrPrintName = json.arrPrintName;
                for (var i = 0; i < json.arrPrintName.length; i++) {
                    document.getElementById('tblMain').rows[0].cells[4 + i].childNodes[0].value = arrPrintName[i]['name'];
                    document.getElementById('tblMain').rows[1].cells[4 + i].childNodes[0].value = arrPrintName[i]['techId'];

                    document.getElementById('tblSizes').rows[i + 1].cells[0].childNodes[0].nodeValue = arrPrintName[i]['name'];
                    document.getElementById('tblSizes').rows[i + 1].cells[1].childNodes[0].rows[0].cells[0].childNodes[0].value = arrPrintName[i]['intSize_width'];
                    document.getElementById('tblSizes').rows[i + 1].cells[1].childNodes[0].rows[0].cells[2].childNodes[0].value = arrPrintName[i]['intSize_height'];
                    document.getElementById('tblSizes').rows[i + 1].cells[2].childNodes[0].value = arrPrintName[i]['intPart'];

                }

                ///////////// set iframe files src ////////////////////


            }
        });
        //////////// end of load details /////////////////

    });

    $('#frmSampleInfomations #butNew').die('click').live('click', function () {
        document.location.href = '?q=27';
        //loadCombo_frmSampleInfomations();
        $('#frmSampleInfomations #txtCode').focus();
    });

    $('#qtyPrices').die('click').live('click',function(){
        var arrBody  	= "";

        var sampleNo = $('#cboSampleNo').val();
        var SYear = $('#cboYear').val();
        var revision = $('#cboRevisionNo').val();
        var combo = $(this).parent().parent().find('.combo').html();
        var print = $(this).parent().parent().find('.print').html();
        print =   URLEncode(print);
        combo =   URLEncode(combo);

        popupWindow3('1');
        // presentation/costing/sample/priceCosting/addNew/qtyWisePricesPopup.php
        $('#popupContact1').load(costingBasePath +'qtyWisePricesPopup.php?sampleNo='+sampleNo+'&sampleYear='+SYear+'&revision='+revision+'&combo='+combo+'&print='+print+'&editable=0',function(){
            // $('#saveQtyPrices').click(saveQtyPrices);
            $('#butClose1').click(disablePopup);

        });
    });

    $('#frmSampleInfomations #butDelete').die('click').live('click', function () {
        if ($('#frmSampleInfomations #cboSearch').val() == '') {
            $('#frmSampleInfomations #butDelete').validationEngine('showPrompt', 'Please select sampleInfomations.', 'fail');
            var t = setTimeout("alertDelete()", 1000);
        }
        else {
            var val = $.prompt('Are you sure you want to delete "' + $('#frmSampleInfomations #cboSearch option:selected').text() + '" ?', {
                buttons: {Ok: true, Cancel: false},
                callback: function (v, m, f) {
                    if (v) {
                        var url = basepath + "sampleInfomations-db-set.php";
                        var httpobj = $.ajax({
                            url: url,
                            dataType: 'json',
                            data: 'requestType=delete&cboSearch=' + $('#frmSampleInfomations #cboSearch').val(),
                            async: false,
                            success: function (json) {

                                $('#frmSampleInfomations #butDelete').validationEngine('showPrompt', json.msg, json.type /*'pass'*/);

                                if (json.type == 'pass') {
                                    $('#frmSampleInfomations').get(0).reset();
                                    //loadCombo_frmSampleInfomations();
                                    var t = setTimeout("alertDelete()", 1000);
                                    return;
                                }
                                var t = setTimeout("alertDelete()", 3000);
                            }
                        });
                    }
                }
            });

        }
    });

    $('.chkTechnique').die('click').live('click', function () {

        var val = $(this).is(':checked');
        var value = $(this).closest('tr').attr('id');
        var text = $(this).closest('tr').find('td').eq(1).text();
        if (val)
            $('#cboTechnique').append(new Option(text, value));
        else
            $("#cboTechnique option[value='" + value + "']").remove();


        $("#cboTechnique").sort_select_box();

        //add technique to all select boxes////
        $('#tblMain .cboTechnique2').each(function () {
            var val = $(this).val();
            $(this).html($("#cboTechnique").html());
            $(this).val(val);
        });
        ///////////////////////////////////////////////////////////
    });

    $('#frmSampleInfomations #butInsertCol').die('click').live('click', function () {

        clickEventForAddColumn();
        $('#tblSizes >tbody >tr:last').after('<tr>' + $('#tblSizes >tbody >tr:last').html() + '</tr>');
        var nextCellId = parseInt($('#tblMain .dataRow').find('td').length) - 5;
        $('#tblSizes >tbody >tr:last').find('td:eq(0)').html('Print ' + nextCellId);
    });


    $('#frmSampleInfomations #butInsertRow').die('click').live('click', function () {

        var rowCount = document.getElementById('tblMain').rows.length;
        //var cellCount = document.getElementById('tblMain').rows[0].cells.length;
        document.getElementById('tblMain').insertRow(rowCount - 1);
        document.getElementById('tblMain').insertRow(rowCount - 1);
        rowCount = document.getElementById('tblMain').rows.length;

        document.getElementById('tblMain').rows[rowCount - 3].innerHTML = document.getElementById('tblMain').rows[rowCount - 5].innerHTML;

        document.getElementById('tblMain').rows[rowCount - 2].innerHTML = document.getElementById('tblMain').rows[rowCount - 4].innerHTML;

        //document.getElementById('tblMain').rows[rowCount-3].cells[1].childNodes[0].nodeValue = '';
        var cellCount = document.getElementById('tblMain').rows[rowCount - 3].cells.length;

        document.getElementById('tblMain').rows[rowCount - 3].cells[cellCount - 1].innerHTML = '';
        /*for(var i=0;i<rowCount;i++)
         {
         document.getElementById('tblMain').rows[i].insertCell(cellCount-1);
         document.getElementById('tblMain').rows[i].cells[cellCount-1].innerHTML  = document.getElementById('tblMain').rows[i].cells[cellCount-2].innerHTML
         document.getElementById('tblMain').rows[i].cells[cellCount-1].bgColor 		= "#FFFFFF";
         document.getElementById('tblMain').rows[i].cells[cellCount-1].className 		= "printName";
         }*/
        //var html = "<tr>abc</tr>";
        //$('#tblMain >tbody >tr:last').before('<tr>'+$('#tblMain >tbody >tr:gt(1)').html()+'</tr>');

        setButtonEvents();
    });


    $('#frmSampleInfomations #cboSampleNo').die('change').live('change', function () {

        loadRevisionNo();

        var intSampleNo = $('#cboSampleNo').val();
        //$('#frmSampleInfomations').get(0).reset();
        $('#cboSampleNo').val(intSampleNo);
        $('#divPicture').html('');
        return;
    });
    $('#frmSampleInfomations #cboStyleNo').die('change').live('change', function () {

        loadSampleNos();

        var intSampleNo = $('#cboSampleNo').val();
        //$('#frmSampleInfomations').get(0).reset();
        $('#cboSampleNo').val(intSampleNo);
        $('#divPicture').html('');
        return;
    });
    /*	$('#cboRevisionNo').change(function(){
     loadMainDetails();
     });*/


    $('#butPDF').die('click').live('click', function () {
        window.open(basepath + 'rptSampleInformationSheet_pdf.php?sampleNo=' + $('#cboSampleNo').val() + '&sampleYear=' + $('#cboYear').val() + '&revisionNo=' + $('#cboRevisionNo').val());
    });


});


function updateTechniqueCombo() {
    var i = 0;

    arr = [];
    $('.chkTechnique').each(function () {
        if ($(this).is(':checked')) {
            arr[i++] = $(this).parent().parent().attr('id');
        }
    });


    $('.chkTechnique').each(function () {
        var x = $.inArray($(this).parent().parent().attr('id'), arr);
        //alert(x);
        if (x != -1) {
            $(this).prop('checked', true);
            //////////////////////////// add options to combo /////////
            var value = $(this).closest('tr').attr('id');
            var text = $(this).closest('tr').find('td').eq(1).text();
            $('#cboTechnique').append(new Option(text, value));
        }

        $("#cboTechnique").sort_select_box();
        //add technique to all select boxes////
        $('#tblMain .cboTechnique2').each(function () {
            var val = $(this).val();
            $(this).html($("#cboTechnique").html());
            $(this).val(val);
        });


    });

}

function saveImage(sampleNo, sampleYear, revisionNo, printCount) {
    var x = 0;
    $('#tblMain >tbody >tr:eq(0) >td').not(':last').not(':first').each(function () {
        //alert(1);
        var img = $(this).find('img').attr('src');
        //alert(img);
        var newImage = 1;
        if ($(this).find('img').attr('id') == 'saveimg') {
            newImage = 0;
        }
        var ajax = new XMLHttpRequest();
        ajax.open("POST", basepath + 'saveImage.php?sampleNo=' + sampleNo + '&sampleYear=' + sampleYear + '&revisionNo=' + revisionNo + '&newImage=' + newImage + '&printId=' + (++x));		//ajax.count = x;

        ajax.onreadystatechange = function () {
            if (ajax.readyState == 4 && ajax.status == 200) {
                printCount--;
                if (printCount <= 0) {
                    hideWaiting();
                    document.location.href = '?q=27&sampleNo=' + sampleNo + '&sampleYear=' + sampleYear + '&revNo=' + revisionNo;
                }
            }
        };
        ajax.setRequestHeader('Content-Type', 'application/upload');
        ajax.send(img);
    });
}
/*function loadCombo_frmSampleInfomations()
 {
 var url 	= "sampleInfomations-db-get.php?requestType=loadCombo";
 var httpobj = $.ajax({url:url,async:false})
 $('#frmSampleInfomations #cboSearch').html(httpobj.responseText);
 }*/


function loadGraphicNo() {
    var url = basepath + "sampleInfomations-db-get.php?requestType=loadGraphicNo&sampleYear=" + $('#cboYear').val();
    var obj = $.ajax({url: url, async: false});
    $('#cboGraphicNo').html(obj.responseText);
}
function loadSampleNo() {
    sampleYear = $('#cboYear').val();
    var url = basepath + "sampleInfomations-db-get.php?requestType=loadSampleNo&sampleYear=" + sampleYear;
    var httpobj = $.ajax({url: url, async: false})
    $('#frmSampleInfomations #cboSampleNo').html(httpobj.responseText);
}

function alertx(sampleNo, sampleYear, revisionNo, printCount) {
    //alert(sampleNo);
    hideWaiting();
    $('#frmSampleInfomations #butSave').validationEngine('hide');
    document.location.href = '?q=27&sampleNo=' + sampleNo + '&sampleYear=' + sampleYear + '&revNo=' + revisionNo;
}
function alertDelete() {
    $('#frmSampleInfomations #butDelete').validationEngine('hide');
}

function loadMain() {
    $("#butLoadTechnique").die('click').live('click', function () {
        popupWindow('1');
    });

    //$('#butInsertCol').trigger('click');clickEventForAddColumn
    //clickEventForAddColumn();
}

function loadPrintMode() {
    $("#butPrintMode").die('click').live('click', function () {
        popupWindow2('2', '#cboSearch', '.printMode', '#frmSampleInfomations');
    });
}
function loadWasingCondistion() {
    $("#butWashingStanderd").die('click').live('click', function () {
        popupWindow2('3', '#cboSearch', '.washStanderd', '#frmSampleInfomations');
    });
}

function loadColors() {
    $("#butColor").die('click').live('click', function () {
        popupWindow2('4', '#cboSearch', '.colors', '#frmSampleInfomations');
    });
}

function loadPart() {
    $("#butLoadPart").die('click').live('click', function () {
        popupWindow2('5', '#cboSearch', '.part', '#frmSampleInfomations');
    });
}
//loadTypeOfPrint

function loadTypeOfPrint() {

    $("#butTypeOfPrint").die('click').live('click', function () {
        popupWindow('6');
    });
}

function loadNewColors() {
    var m = $("#iframeMain" + x).contents().find(cboFrom).html();
    //alert(m);
    $(pageTo + " " + cboTo).html(m);
}

function closePopUp() {

    /*	var i=0;
     /*	$('#tblTechnique tbody tr').each(function(){
     //alert($(this).attr('id'));
     var t = $(this).find('td').eq(0).eq(0).attr('checked');
     alert(t);
     });

     arr = [];
     $('.chkTechnique').each(function(){
     if($(this).attr('checked'))
     {
     arr[i++]=$(this).parent().parent().attr('id');
     }
     });
     $("#tblTechnique").load('table-technique.php',function(){
     $('#cboTechnique').html('');
     $('#cboTechnique').append(new Option('',''));
     $('.chkTechnique').bind('click',chkTechniqueClick);

     $('.chkTechnique').each(function(){
     var x =$.inArray($(this).parent().parent().attr('id'),arr);
     //alert(x);
     if(x!=-1)
     {
     $(this).attr('checked',true);
     //////////////////////////// add options to combo /////////
     var value = $(this).closest('tr').attr('id');
     var text = $(this).closest('tr').find('td').eq(1).text();
     $('#cboTechnique').append(new Option(text,value));
     }
     });

     $("#cboTechnique").sort_select_box();
     //add technique to all select boxes////
     $('#tblMain .cboTechnique2').each(function(){
     var val = $(this).val();
     $(this).html($("#cboTechnique").html());
     $(this).val(val);
     });


     });

     $("#tblTypeOfPrint").load('table-typeofprint.php');*/
}

function chkTechniqueClick() {
    var val = $(this).is(':checked');
    var value = $(this).closest('tr').attr('id');
    var text = $(this).closest('tr').find('td').eq(1).text();
    if (val)
        $('#cboTechnique').append(new Option(text, value));
    else
        $("#cboTechnique option[value='" + value + "']").remove();


    $("#cboTechnique").sort_select_box();

    //add technique to all select boxes////
    $('#tblMain .cboTechnique2').each(function () {
        var val = $(this).val();
        $(this).html($("#cboTechnique").html());
        $(this).val(val);
    });
    ///////////////////////////////////////////////////////////
}
function checkCustomer(customerId) {

    //alert(customerId);
    var url = basepath + "sampleInfomations-db-get.php?requestType=checkCustomer&customerId=" + customerId;
    var httpobj = $.ajax({
        url: url,
        async: false,
        type: "post",
        dataType: "json",
        success: function (json) {

            var type = (json.type);
            if (type == "fail") {

                alert("Customer is Blocked for Sample");
            }

        }

    });
}

function clickEventForAddColumn() {

    var rowCount = document.getElementById('tblMain').rows.length;
    var cellCount = document.getElementById('tblMain').rows[0].cells.length;

    for (var i = 0; i < rowCount; i++) {
        document.getElementById('tblMain').rows[i].insertCell(cellCount - 1);
        document.getElementById('tblMain').rows[i].cells[cellCount - 1].innerHTML = document.getElementById('tblMain').rows[i].cells[cellCount - 2].innerHTML
        document.getElementById('tblMain').rows[i].cells[cellCount - 1].align = "center";
        document.getElementById('tblMain').rows[i].cells[cellCount - 1].bgColor = "#FFFFFF";


    }

    cellCount = document.getElementById('tblMain').rows[0].cells.length;

    var text1 = document.getElementById('tblMain').rows[1].cells[cellCount - 3].innerHTML;
    document.getElementById('tblMain').rows[1].cells[cellCount - 2].innerHTML = text1;//"Print "+(cellCount-2)+text1;
    document.getElementById('tblMain').rows[1].cells[cellCount - 2].childNodes[0].innerHTML = "Print " + (cellCount - 2);
    document.getElementById('tblMain').rows[1].cells[cellCount - 2].className = "printName";

    setButtonEvents();
}

function loadRevisionNo() {
    var url = basepath + "sampleInfomations-db-get.php?requestType=loadRevisionNo&sampleNo=" + $('#cboSampleNo').val() + "&sampleYear=" + $('#cboYear').val();
    var obj = $.ajax({url: url, async: false});
    $('#cboRevisionNo').html(obj.responseText);
}
function loadSampleNos() {
    var url = basepath + "sampleInfomations-db-get.php?requestType=loadSampleNo&styleNo=" + URLEncode($('#cboStyleNo').val()) + "&sampleYear=" + $('#cboYear').val();
    var obj = $.ajax({url: url, async: false});
    $('#cboSampleNo').html(obj.responseText);
}

function loadGridCheck(arr, chkClass) {
    var id = "";
    $('.' + chkClass).removeAttr('checked');

    if (arr != null) {
        for (var i = 0; i <= arr.length - 1; i++) {
            //$('#'+chkClass+arr[i]).attr('checked',true);
            $('#' + chkClass + arr[i]).click();

        }
    }


}

function clearRows() {
    var rowCount = $('#tblMain >tbody >tr').length;
    //var cellCount = document.getElementById('tblMain').rows[1].cells.length;
    for (var i = 3; i < rowCount - 1; i++) {
        document.getElementById('tblMain').deleteRow(i);
    }

    var rowCount1 = $('#tblSizes >tbody >tr').length;
    //var cellCount = document.getElementById('tblMain').rows[1].cells.length;
    for (var i = 1; i < rowCount1; i++) {
        document.getElementById('tblSizes').deleteRow(1);
    }
}

function clearColumns() {


    var columLength = document.getElementById('tblMain').rows[0].cells.length;
    for (var i = 5; i < columLength - 1; i++) {
        //alert(i);
        //$('#tblMain').removeCol(i);
        //document.getElementById('tblMain').rows[0].deleteColumn(4);
        //$('td[col='+i+']').remove();
        $('#tblMain >tbody >tr').find('td:not(:last):eq(' + 5 + ')').remove();
    }

}

function addTypeOfPrintToGrid(rowId, cellId) {
    $('#tblTypeOfPrint .chkTypeOfPrint:checked').each(function () {
        var tblType = document.getElementById('tblMain').rows[rowId].cells[cellId].childNodes[0];
        var rows = document.getElementById('tblTypeOfPrint').rows.length;
        var id = $(this).val();
        var name = $(this).parent().parent().find('td').eq(1).html();
        var y = tblType.rows.length;
        var found = false;

        for (var i = 1; i < y; i++) {
            if (tblType.rows[i].cells[0].id == id) {
                found = true;
                break;
            }
        }
        if (!found) {
            tblType.insertRow(y);
            tblType.rows[y].innerHTML = "<td id=\"" + id + "\" class=\"normalfnt\" bgcolor=\"#ffffffff\"><img class=\"mouseover removeRow\" src=\"images/del.png\"/>" + name + "</td><td bgcolor=\"#ffffffff\">" + techniqueTableScript + "</td>";
        }
    });
    setButtonEvents();
}
function removeRow(obj) {
    obj.parentNode.parentNode.parentNode.deleteRow(obj.parentNode.parentNode.rowIndex)
}

function removeRow2(obj) {
    var count = obj.parentNode.parentNode.parentNode.rows.length;
    var id = obj.parentNode.parentNode.rowIndex;

    if (count > 5) {
        $('#tblMain >tbody >tr:eq(' + id + ')').remove();
        $('#tblMain >tbody >tr:eq(' + id + ')').remove();
    }
}

function setToTypeButton(obj) {
    var rowId = obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
    var cellId = obj.parentNode.parentNode.parentNode.parentNode.parentNode.cellIndex;
    popupWindow3('1');

    $('#popupContact1').load(basepath + 'typeOfPrint.php', function () {

        $('#frmTypeOfPrintPopUp #butAdd').die('click').live('click', function () {
            addTypeOfPrintToGrid(rowId, cellId);
            disablePopup();
        });
        $('#frmTypeOfPrintPopUp #butClose1').die('click').live('click', disablePopup);
    });
}

function setToColorGrid(obj) {
    var rowId = obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
    var cellId = obj.parentNode.parentNode.parentNode.parentNode.parentNode.cellIndex;
    popupWindow3('1');

    $('#popupContact1').load(basepath + 'colors.php', function () {
        $('#frmColors #butAdd').die('click').live('click', function () {
            addColorstoGrid(rowId, cellId);
            //enablePopup();
        });
        $('#frmColors #butClose1').die('click').live('click', disablePopup);
        $('#frmColors #butSaveColor').die('click').live('click', setColorSaveButton);
    });
}

function addColorstoGrid(rowId, cellId) {
    $('#tblColors .chkColors:checked').each(function () {
        var tblType = document.getElementById('tblMain').rows[rowId].cells[cellId].childNodes[0];
        var rows = document.getElementById('tblColors').rows.length;

        var id = $(this).val();
        var name = $(this).parent().parent().find('td').eq(1).html();
        var y = tblType.rows.length;
        var found = false;

        for (var i = 1; i < y; i++) {
            if (tblType.rows[i].cells[0].id == id) {
                found = true;
                break;
            }
        }

        if (!found) {
            var techniqueHtml = $('#cboTechniqueDumy').html();
            var spItem = $('#cboItemDumySPRM').html();
            tblType.insertRow(y);
            var html = "<td bgcolor=\"#ffffffff\" class=\"normalfnt\" id=\"" + id + "\"><img src=\"images/del.png\" class=\"mouseover removeRow\" />" + name + "</td>" +
                "<td bgcolor=\"#ffffffff\" class=\"normalfnt\" id=\"\"><select id=\"cboTechnique\" name=\"cboTechnique\" class=\"cboTechnique cboTech\" style=\"width:100px\">" + techniqueHtml +
                "</select></td>" +
                "<td bgcolor=\"#ffffffff\">" + '<table id="tblGridTI" class="tblGridTI" width="100%" cellspacing="1" cellpadding="0" border="0" bgcolor="#999999"><tbody><tr><td><select id="cboItem" class="cboItem cboItems techItem" name="select5" style="width:100px"></td></tr></tbody></table>' + "</td>" +
                "<td bgcolor=\"#ffffffff\" valign=\"top\"><table id=\"tblGridTIQ\" class=\"tblGridTIQ\" width=\"100%\" cellspacing=\"1\" cellpadding=\"0\" border=\"0\" bgcolor=\"#999999\"><tbody><tr><td><input id=\"txtQty\" type=\"text\" name=\"textfield5\" value=\"\" style=\"width:30px\" ><div class=\"parentItem\" style=\"display:none\"></div></td></tr></tbody></table></td>" +
                "<td bgcolor=\"#ffffffff\" class=\"normalfnt\" valign=\"top\"><table id=\"tblGridTIWH\" class=\"tblGridTIWH\" width=\"100%\" cellspacing=\"1\" cellpadding=\"0\" border=\"0\" bgcolor=\"#999999\"><tbody><tr><td><div class=\"parentItem\" style=\"display:none\"></div><input id=\"txtSizeW\" type=\"text\" name=\"textfield3\" style=\"width:30px\" value=\"\" >W<input id=\"txtSizeH\" type=\"text\" name=\"textfield4\" value=\"\" style=\"width:30px\" >H</td></tr></tbody></table></td>" +
                "<td colspan=\"3\" bgcolor=\"#ffffffff\"></td>";
            //alert(html);
            tblType.rows[y].innerHTML = html;
        }
    });
    setButtonEvents();
}

function setToTechnique(obj) {

    var typeRowId = obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
    var mainRowId = obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;

    var cellId = obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.cellIndex;
    popupWindow3('1');
    $('#popupContact1').load(basepath + 'technique.php', function () {

        $('#frmTechnique #butAdd').die('click').live('click', function () {
            addToTechniqueGrid(mainRowId, typeRowId, cellId);
            disablePopup();
        });

        $('#frmTechnique #butClose1').die('click').live('click', disablePopup);

    });
}

function addToTechniqueGrid(mainRowId, typeRowId, cellId) {
    $('#tblTechnique .chkTechnique:checked').each(function () {
        var tblTech = document.getElementById('tblMain').rows[mainRowId].cells[cellId].childNodes[0].rows[typeRowId].cells[1].childNodes[0];
        var id = $(this).val();
        var name = $(this).parent().parent().find('td').eq(1).html();
        var typeId = $(this).parent().parent().find('td').eq(1).attr('id');

        var y = tblTech.rows.length;
        var found = false;
        for (var i = 1; i < y; i++) {
            if (tblTech.rows[i].cells[0].id == id) {
                found = true;
                break;
            }
        }
        if (!found) {
            tblTech.insertRow(y);
            tblTech.rows[y].innerHTML = "<td class=\"normalfnt\" id=\"" + id + "\"><img class=\"mouseover removeRow\" src=\"images/del.png\"/>" + name + "</td>" +
                "<td class=\"normalfnt\"><input class=\"qty\"  id=\"txtQty\" style=\"width:30px\" /></td>" +
                "<td><select class=\"cboItem\" style=\"width:150px\"></select></td>";
        }

        if (typeId != 3) {
            tblTech.rows[y].cells[1].childNodes[0].disabled = true;//cboItemDumySPRM
            tblTech.rows[y].cells[2].childNodes[0].innerHTML = document.getElementById('cboItemDumy').innerHTML;
        }
        else {
            tblTech.rows[y].cells[2].childNodes[0].innerHTML = document.getElementById('cboItemDumySPRM').innerHTML;
        }
        //$(".butTechnique").unbind('click');

    });
    setButtonEvents()
    //alert($('#tblTypeOfPrint >tbody >tr').length);
}
function loadColorTxt() {
    //alert("gjgjgj");
    // alert($('#txtColorName').val());
    var url = basepath + "sampleInfomations-db-get.php?requestType=loadColorTxt&text=" + $('#txtColorName').val();
    var obj = $.ajax({url: url, async: false});
    $('#tblColors').html(obj.responseText);
//	 $.ajax({
//            url:url,
//            async:false,
//            type: "post",
//            dataType:"json",
//     success: function(json)
//		 {
//                        
//	//$('#txtColorName').val(json.txtColorName);
//                    
//     }

    // });
    //$('#txtColorName').html(obj.responseText);
    //alert($('#txtColorName').val());

}

function setColorSaveButton() {

    var color = $('#txtColorName').val();
    var url = basepath + "sampleInfomations-db-set.php?requestType=saveColor&colorName=" + color;
    if ($('#frmColors').validationEngine('validate')) {
        showWaiting();
        $.ajax({
            url: url,
            async: false,
            dataType: 'JSON',
            success: function (json) {

                $('#frmColors #butSaveColor').validationEngine('showPrompt', json.msg, json.type);
                if (json.type == 'pass') {
                    $('#txtColorName').val('');
                    $('#frmColors #divColors').load(basepath + 'colorsTable.php', function () {
                        hideWaiting();
                    });
                }
                else
                    hideWaiting();
            },
            error: function (xhr, status) {
                hideWaiting();
                $('#frmColors #butSaveColor').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
            }
        });
    }
}

function setButtonEvents() {
}

function saveDetails() {
    var data = "requestType=save";

    data += "&sampleNo=" + $('#cboSampleNo').val();
    data += "&sampleYear=" + $('#cboYear').val();
    data += "&revisionNo=" + $('#cboRevisionNo').val();
    data += "&marketer=" + $('#cboMarketer').val();
    data += "&date=" + $('#dtDate').val();

    data += "&reqNoArr=" + $('#cboRequisitionNo').val();
    data += "&graphicNo=" + $('#txtGraphicRefNo').val();
    data += "&customerId=" + $('#cboCustomer').val();
    data += "&styleNo=" + $('#txtStyleNo').val();
    data += "&brandId=" + $('#cboBrand').val();

    data += "&verivide_d65=" + $('#txtVerivide_D65').is(':checked');
    data += "&verivide_tl85=" + $('#txtVerivide_TL84').is(':checked');
    data += "&verivide_cw=" + $('#txtVerivide_CW').is(':checked');
    data += "&verivide_f=" + $('#txtVerivide_F').is(':checked');
    data += "&verivide_uv=" + $('#txtVerivide_UV').is(':checked');

    data += "&macbeth_dl=" + $('#txtMacbeth_DL').is(':checked');
    data += "&macbeth_cw=" + $('#txtMacbeth_CW').is(':checked');
    data += "&macbeth_inca=" + $('#txtMacbeth_INCA').is(':checked');
    data += "&macbeth_tl84=" + $('#txtMacbeth_TL84').is(':checked');
    data += "&macbeth_uv=" + $('#txtMacbeth_UV').is(':checked');
    data += "&macbeth_horizon=" + $('#txtMacbeth_Horizon').is(':checked');

    data += "&curing_temp=" + $('#txtCuringTemp').val();
    data += "&curing_speed=" + $('#txtCuringBeltSpeed').val();
    data += "&press_temp=" + $('#txtPressTemp').val();
    data += "&press_pressure=" + $('#txtPressPressure').val();
    data += "&press_time=" + $('#txtPressTime').val();

    data += "&meshCount=" + $('#txtMeshCount').val();
    data += "&instruction=" + URLEncode(document.getElementById('txtInstructions').value);
    data += "&instructionTech=" + $('#txtInstructionsTech').val();

    //data+="&image="	+	$('#divPicture >img').attr('src');

    /*var arrTechnique="[ ";
     $('.chkTechnique:checked').each(function(){
     arrTechnique += '{ "techniqueId":"'+$(this).val()+ '"},';
     });
     arrTechnique = arrTechnique.substr(0,arrTechnique.length-1);
     arrTechnique += " ]";

     data+="&arrTechnique="	+	arrTechnique;*/
    /////////////////////////// create part and width height //////////////////////////
    var arrPart = '';
    var printCount = 0;
    //arrPart += '[';
    //$('#tblMain >tbody >tr:eq('+i+') >td:eq('+n+')> .tblColors >tbody >tr:eq(1)>td:eq(0) >span').each(function(){
    $('#tblMain >tbody >tr:eq(1) >td').not(':first').not(':last').each(function () {

        var partId = $(this).find('.part').val();
        var size_w = $(this).find('.sizeW').val();
        var size_h = $(this).find('.sizeH').val();
        arrPart += '{"partId":"' + partId + '","size_w":"' + size_w + '","size_h":"' + size_h + '"},';
        printCount++;
    });
    if (arrPart != '') {
        arrPart = arrPart.substr(0, arrPart.length - 1);
        data += "&arrPart=[" + arrPart + ']';
    }


    var rowCount = $('#tblMain >tbody >tr').length;
    var cellCount = document.getElementById('tblMain').rows[1].cells.length;
    var row = 0;

    var arrCombo = "[";
    var checkComboArray = [];

    for (var i = 3; i < rowCount - 1; i += 2) {
        var combo = document.getElementById('tblMain').rows[i - 1].cells[0].childNodes[0].value;
        if (checkComboArray[combo]) {
            if (checkComboArray[combo] == 1) {
                alert("Duplicate combo name '" + combo + "'");
                return false;
            }
        }
        checkComboArray[combo] = 1;

        var printMode = document.getElementById('tblMain').rows[i].cells[0].childNodes[1].value;
        var washStanderd = document.getElementById('tblMain').rows[i].cells[0].childNodes[3].value;
        var groundColor = document.getElementById('tblMain').rows[i].cells[0].childNodes[5].value;
        var fabricType = document.getElementById('tblMain').rows[i].cells[0].childNodes[7].value;
        //alert(cellCount);
        for (var n = 1; n < cellCount - 1; n++) {
            //alert(n);
            arrCombo += "{";

            var printName = 'print ' + (n);//document.getElementById('tblMain').rows[0].cells[n].childNodes[0].value;
            var techniqueId = document.getElementById('tblMain').rows[1].cells[n].childNodes[0].value;
            var colorId = document.getElementById('tblMain').rows[i].cells[n].childNodes[0].value;

            //var sizeW 	= 	document.getElementById('tblSizes').rows[n-3].cells[1].childNodes[0].rows[0].cells[0].childNodes[0].value;
            //var sizeH 	= 	document.getElementById('tblSizes').rows[n-3].cells[1].childNodes[0].rows[0].cells[2].childNodes[0].value;
            //var partId 	= 	document.getElementById('tblSizes').rows[n-3].cells[2].childNodes[0].value;


            arrCombo += '"printMode":"' + printMode + '",';
            arrCombo += '"washStanderd":"' + washStanderd + '",';
            arrCombo += '"comboName":"' + combo + '",';
            arrCombo += '"groundColor":"' + groundColor + '",';
            arrCombo += '"fabricType":"' + fabricType + '",';
            arrCombo += '"printName":"' + printName + '",';
            arrCombo += '"gridColMarketingDetails":[';
            var grid2Id = 0;
            var gridColTechManDetails = '';
            var gridTechnicalDetails = '';
            var gridTIDetails = "";
            var gridTIId = 0;

            $('#tblMain >tbody >tr:eq(' + i + ') >td:eq(' + n + ')').find('#tblGrid2 >tbody >tr:not(:first)').each(function () {
                var colorId = $(this).find('td:eq(0)').attr('id');
                var techId_p = '';
                techId_p = $(this).find('#cboTechnique').val();
				if(colorId>0 && techId_p> 0 && colorId !='' && techId_p !=''){
                	grid2Id++;
				}

                arrCombo += '{"colorId":"' + colorId + '",' +
                    '"techId_p":"' + techId_p + '",' +
                    '"marketingItems":[';


                //marketingItems details for relevant colour and technique
                var r = 0;
                $('#tblMain >tbody >tr:eq(' + i + ') >td:eq(' + n + ')').find('#tblGrid2 >tbody >tr:eq(' + grid2Id + ')').find('#tblGridTI >tbody >tr').each(function () {
                    r++;
                    var objTI = this
                    var itemId = $(objTI).find('#cboItem').val();
                    var itemC = '';
                    var qty = '';
                    var w = '';
                    var h = '';
                    $(objTI).parent().parent().parent().parent().find('#tblGridTIQ >tbody >tr').each(function () {
                        itemC = $(this).find('.parentItem').html()
                        if (itemC == itemId)
                            qty = $(this).find('#txtQty').val();
                    });

                    $(objTI).parent().parent().parent().parent().find('#tblGridTIWH >tbody >tr').each(function () {
                        itemC = $(this).find('.parentItem').html()
                        if (itemC == itemId) {
                            w = $(this).find('#txtSizeW').val();
                            h = $(this).find('#txtSizeH').val();
                        }
                    });
                    //alert(qty+"/"+w+"/"+h);
                    arrCombo += '{' +
                        '"itemId":"' + itemId + '",' +
                        '"qty":"' + qty + '",' +
                        '"w":"' + w + '",' +
                        '"h":"' + h + '"' +
                        '},';
                });

                if (r > 0) {
                    arrCombo = arrCombo.substr(0, arrCombo.length - 1);
                }


                arrCombo += ']},';


            });
			
			if(grid2Id==0){//if colours haven't been added....
				alert("Please select at least one 'COLOR' and 'TECHNIQUE' for "+combo+'/'+printName+" . If it is not applicable, then select 'NULL' for 'COLOR' and 'TECHNIQUE'");
				return false;
				//arrCombo+='[';
 			}

            arrCombo = arrCombo.substr(0, arrCombo.length - 1);
            arrCombo += ']},';

        }
        //arrCombo = arrCombo.substr(0,arrCombo.length-1);
        //arrCombo += " },";
    }
    arrCombo = arrCombo.substr(0, arrCombo.length - 1);
    arrCombo += " ]";
    ///////////////////// end of combo array /////////////////////////////////

    data += "&arrCombo=" + arrCombo;

    /*			$('#tblMain >tbody >tr').each(function(){

     var cellCount = $(this).find('td').length;
     if(row>=2)
     {
     var printId = $(this).find('td .printMode').val();
     alert(printId);
     }
     row++;
     });*/
    /////////////////////////////////////////////////////////////////////////////////////


    ///////////////////////////// save main infomations /////////////////////////////////////////
    var url = basepath + "sampleInfomations-db-set.php";
    var obj = $.ajax({
        url: url,

        dataType: "json",
        type: 'POST',
        data: data,
        async: false,

        success: function (json) {

            $('#frmSampleInfomations #butSave').validationEngine('showPrompt', json.msg, json.type /*'pass'*/);
            if (json.type == 'pass') {
                var x = 0;
				//if(json.bulk_order != '' && json.bulk_order != 'NULL' && json.bulk_order != null){
					if(json.ordersCount>0){
						alert("Bulk order "+json.bulk_order+" has been already raised for previous sample revisions. Order status is, "+json.bulk_order_status);
					}
				//}
                saveImage(json.sampleNo, json.sampleYear, json.revisionNo, printCount);
				
                return;
            }
            else
                hideWaiting();
        },
        error: function (xhr, status) {
            hideWaiting();
            $('#frmSampleInfomations #butSave').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
        }
    });

}

function loadTechItems(object) {
    var technique = $(object).val()
    var url = basepath + "sampleInfomations-db-get.php?requestType=loadTechItems&technique=" + technique;
    var obj = $.ajax({url: url, async: false});
    $(object).parent().parent().find('.cboItems').html(obj.responseText);
}

function loadRequisitionDetails() {
    if ($('#cboRequisitionNo').val() == '') {
        clearReqDetails();
        return;
    }

    var url = basepath + "sampleInfomations-db-get.php?requestType=loadRequisitionDetails";
    var obj = $.ajax({
        url: url,
        dataType: "json",
        type: 'POST',
        data: "&reqNoArr=" + $('#cboRequisitionNo').val(),
        async: false,
        success: function (json) {
            $('#cboMarketer').val(json.marketer);
            $('#txtGraphicRefNo').val(json.graphic);
            $('#cboCustomer').val(json.customer);
            $('#cboCustomer').change();
            $('#txtStyleNo').val(json.style);
            $('#cboBrand').val(json.brand);
        },
        error: function (xhr, status) {
        }
    });

}

function clearReqDetails() {
    $('#cboMarketer').val('');
    $('#txtGraphicRefNo').val('');
    $('#cboCustomer').val('');
    $('#txtStyleNo').val('');
    $('#cboBrand').html('');
}

function setParentItemsForQtyWHGrid(object) {
    //alert($(object).parent().parent().html())
    var itemId = $(object).val();
    var rowIndex = ($(object).parent().parent().closest('tr').index());
    $(object).parent().parent().parent().parent().parent().parent().find('#tblGridTIQ >tbody >tr').each(function () {
        var rowIndexQ = ($(this).closest('tr').index());
        if (rowIndexQ == rowIndex)
            $(this).find('.parentItem').html(itemId);
    });
    $(object).parent().parent().parent().parent().parent().parent().find('#tblGridTIWH >tbody >tr').each(function () {
        var rowIndexWH = ($(this).closest('tr').index());
        if (rowIndexWH == rowIndex)
            $(this).find('.parentItem').html(itemId);
    });
    //alert($('table').find('tr').index(object));
}

function addProcess(obj) {
    var rowId = obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
    var cellId = obj.parentNode.parentNode.parentNode.parentNode.parentNode.cellIndex;
    popupWindow3('1');


    var sampNo = $('#cboSampleNo').val();
    var sampYear = $('#cboYear').val();
    var rev = $('#cboRevisionNo').val();
    var combo = $(obj).parent().parent().find('.combo').html();
    var printId = $(obj).parent().parent().find('.print').html();
    //alert(sampNo+","+sampYear+","+rev+","+combo+","+printId);

    $('#popupContact1').load(basepath + 'processes.php?sampNo=' + sampNo + '&sampYear=' + sampYear + '&rev=' + rev + '&combo=' + URLEncode(combo) + '&print=' + URLEncode(printId), function () {

        $('#frmProcesses #butClose1').die('click').live('click', disablePopup);

    });
}

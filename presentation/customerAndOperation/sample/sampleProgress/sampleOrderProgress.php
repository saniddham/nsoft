<?php
date_default_timezone_set('Asia/Kolkata');
//ini_set('allow_url_include',1);
//ini_set('display_errors',1);
 
$thisFilePath 		=  $_SERVER['PHP_SELF'];
require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";

$location = $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];


$approveLevel = (int)getMaxApproveLevel();
$programName='Sample Place Order';
$programCode='P0354';
$intUser  = $_SESSION["userId"];

			if($intStatus==0)
				$para = " balQty>0";
			else if ($intStatus==1)
				$para = " qty>0 and balQty=0 ";

$sql = "select * from(select * from view_sample_order_progress)  as t where t.intCompanyId='$location' and  1=1";
					        	//echo $sql;
 						  
$col = array();

$col["title"] 	= "Sample No"; // caption of column
$col["name"] 	= "Sample_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
//$col['link']	= "../addNew/sampleOrder.php?orderNo={Sample_No}&orderYear={Sample_Year}";	 
//$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

//$reportLinkApprove  = "../Report/sampleReport.php?no={Sample_No}&year={Sample_Year}&approveMode=1";

$cols[] = $col;	$col=NULL;


//Sample Year
$col["title"] = "Sample Year"; // caption of column
$col["name"] = "Sample_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Rev No
$col["title"] = "Rev No"; // caption of column
$col["name"] = "Rev_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Graphic No
$col["title"] = "Graphic No"; // caption of column
$col["name"] = "Graphic_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Combo
$col["title"] = "Combo"; // caption of column
$col["name"] = "Combo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Part
$col["title"] = "Part"; // caption of column
$col["name"] = "Part"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Sample Type
$col["title"] = "Sample Type"; // caption of column
$col["name"] = "Sample_Type"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Grade
$col["title"] = "Grade"; // caption of column
$col["name"] = "Grade"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Size
$col["title"] = "Size"; // caption of column
$col["name"] = "Size"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Delivery Date
$col["title"] = "Delivery Date"; // caption of column
$col["name"] = "Delivery_Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Progress
$col["title"] = "Progress"; // caption of column
$col["name"] = "Progress"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Customer"; // caption of column
$col["name"] = "Customer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;






$jq = new jqgrid('',$db);

$grid["caption"] 		= "Sample Order Progress";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'Sample_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);




$out = $jq->render("list1");
?>
<head>
	<?php 
		include "include/listing.html";
	?>
</head>
<body>	
<form id="frmlisting" name="frmlisting" method="post" action="">

        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
</form>
</body>

</html>
<?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(trn_sampleorderheader.intApproveLevels) AS appLevel
			FROM trn_sampleorderheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
?>


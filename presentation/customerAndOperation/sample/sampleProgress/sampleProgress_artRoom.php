<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
 
include_once "libraries/jqgrid2/inc/jqgrid_dist.php";

$sql = "select * from(
		 SELECT
						if(isNull(intArtRoomEnterUser),'Pending','Completed') as Status,
						M.intSampleNo as Sample_No,
						M.intSampleYear as Sample_Year,
						M.dtDate as Date,
						mst_customer.strName as Customer,
						M.strStyleNo as Style_No,
						M.intMarketingStatus AS firstStatus,
						M.intStatus AS secondStatus,
						M.intRevisionNo as Rev_No,
							
						'View' as `View` 
					FROM
						trn_sampleinfomations M
						Inner Join mst_customer ON mst_customer.intId = M.intCustomer
						where M.intRevisionNo = (select max(trn_sampleinfomations.intRevisionNo ) 
					from trn_sampleinfomations where trn_sampleinfomations.intSampleNo =M.intSampleNo 
						and trn_sampleinfomations.intSampleYear = M.intSampleYear   ) and M.intMarketingStatus=1
					order by intSampleYear,intSampleNo desc
)  as t where 1=1
						";
					    //  	echo $sql;
						  
						  
						  
						  
$col = array();

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Completed:Completed" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist

$cols[] = $col;	$col=NULL;
//Fabric Receive No
$col["title"] 	= "Sample No"; // caption of column
$col["name"] 	= "Sample_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
//$col['link']	= "../addNew/sampleOrder.php?orderNo={Sample_No}&orderYear={Sample_Year}";	 
//$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

$reportLink  = "?q=416&sampleNo={Sample_No}&sampleYear={Sample_Year}&revNo={Rev_No}";
//$reportLinkApprove  = "../Report/sampleReport.php?no={Sample_No}&year={Sample_Year}&approveMode=1";

$cols[] = $col;	$col=NULL;


//Fabric Receive Year
$col["title"] = "Sample Year"; // caption of column
$col["name"] = "Sample_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//CUSTOMER PO NO
$col["title"] = "Rev No"; // caption of column
$col["name"] = "Rev_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Style No"; // caption of column
$col["name"] = "Style_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Customer"; // caption of column
$col["name"] = "Customer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;



//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Sample Progress - Art Room";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'Sample_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);




$out = $jq->render("list1");
?>
<title>Sample Progress - Art Room</title>

<?php //include 'include/listing.html'?>
<form id="frmlisting" name="frmlisting" method="post" action="">
<div align="center" style="margin:10px"><?php echo $out?></div>
</form>


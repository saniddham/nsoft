var basepath			= 'presentation/customerAndOperation/sample/Report/';
var userCheckedDetails 	= [];

$(document).ready(function() {	
	//$('.chkClass').click(getChecked);
	$('.chkClassAll').click(getCheckedAll);
	
	$('#frmSampleReport #imgApprove').click(function(){
	var jsonData = getChecked();
	// jsonData.splice(-1,1);
	var st = false;
	$('#frmSampleReport .chkClass').each(function(){
		if($(this).is(':checked'))
		{
			st = true;
		}	
	});
	if(st){
		var val = $.prompt('Are you sure you want to approve this sample ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										showWaiting();
										var url = basepath+"sampleReport_sampleRoom_set.php"+window.location.search+'&status=approve';
		                                var userSelected = JSON.stringify(jsonData);	
										hideWaiting();
										$.ajax({type: 'post',url:url,async:false,data:"userSelectedArr="+ userSelected, dataType:'json',success:function(json){
											
											alert(json.msg);
											window.location.href = window.location.href;
											window.opener.location=window.opener.location;
											userCheckedDetails 	= [];
										}})
									}
								}});
		}
	if(!st)
		alert('Not Selected');
	return;
		
	});
	
	$("#frmSampleReport .butProcess").die('click').live('click',function(){
		addProcess(this);
	});
	
	$('#frmSampleReport #imgReject').click(function(){
	var jsonData = getChecked();
	//jsonData.splice(-1,1);
	var st = false;
	
	$('#frmSampleReport .chkClass').each(function(){
		
		if($(this).is(':checked'))
		{
			st = true; 
		}	
	});
	if(st){
		var val = $.prompt('Are you sure you want to reject this sample ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
			if(v)
			 {   
			   var userSelected = JSON.stringify(jsonData);
			   var url = basepath+"sampleReport_sampleRoom_set.php"+window.location.search+'&status=reject';
			   var obj = $.ajax({type: 'post',url:url,async:false,data:"userSelectedArr="+ userSelected, dataType:'json',success:function(json){
			   alert(json.msg);
			   window.location.href = window.location.href;
			   userCheckedDetails 	= [];
				}});
			} 
		}});	
	}
	if(!st)
		alert('Not Selected');
	return;

	});
	$("div#extraControls").removeClass("hidden");
	$("td#selectAllBackground").css("background-color", "#0FF");
});

function getChecked(){
	$('#frmSampleReport .chkClass').each(function(){
		var combo  		   = $(this).parent().children('#divCombo').html();
	    var printCheckBox  = $(this).parent().children('#divPrint').html();
	    var isChecked      = $(this).prop('checked');
		
	if(!userCheckedDetails[combo]){
		userCheckedDetails[combo] = [];
	  }

	if(isChecked==true){
		 userCheckedDetails[combo][printCheckBox]=1;
  	}    
	else{
		userCheckedDetails[combo][printCheckBox]=0;
	} 
	
	});
	var JSONObj = [];
	for (var k in userCheckedDetails){
		if (userCheckedDetails.hasOwnProperty(k)) {
			 //-------------------------------------------------
			
			for (var l in userCheckedDetails[k]){
				if (userCheckedDetails[k].hasOwnProperty(l)) {
					
					    var obj = { 
							Main_Key: k,
							Sub_Key: l,
							value: userCheckedDetails[k][l]
						};
					//alert(JSON.stringify(obj));	
					JSONObj.push(obj);
				}
			}
			
			 //-------------------------------------------------
		}
		
	}
	return JSONObj;}

function getCheckedAll(){
  if(this.checked) {
      $(':checkbox').each(function() {
          this.checked = true;
      });
  }
  else {
    $(':checkbox').each(function() {
          this.checked = false;
      });
  }

}

function addProcess(obj)
{
	var rowId 	=  obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
	var cellId 	= obj.parentNode.parentNode.parentNode.parentNode.parentNode.cellIndex;
	popupWindow3('1');
	

	var sampNo	=	$(obj).parent().parent().find('.sampleNo').html();
	var sampYear	=	$(obj).parent().parent().find('.sampleYear').html();
	var rev	=	$(obj).parent().parent().find('.sampleRevision').html();
	var	combo	=   $(obj).parent().parent().find('.combo').html();
	var	printId	=   $(obj).parent().parent().find('.print').html();
	//alert(sampNo+","+sampYear+","+rev+","+combo+","+printId);
	
	$('#popupContact1').load(basepath+'processes.php?sampNo='+sampNo+'&sampYear='+sampYear+'&rev='+rev+'&combo='+URLEncode(combo)+'&print='+URLEncode(printId),function(){
		
		$('#frmProcesses #butClose1').die('click').live('click',disablePopup);
		
	});	
}



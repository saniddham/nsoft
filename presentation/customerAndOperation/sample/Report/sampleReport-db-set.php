<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];

	include "{$backwardseperator}dataAccess/Connector.php";
	include "../../../../libraries/mail/mail.php";
	
	$no 	= $_REQUEST['no'];
	$year 	= $_REQUEST['year'];
	$revNo 	= $_REQUEST['revNo'];
	
	if($_REQUEST['status']=='approve')
	{
		$sql = "SELECT
					trn_sampleinfomations.intStatus,
					trn_sampleinfomations.intMarketingStatus,
					trn_sampleinfomations.intTechnicalApproveLevelStart
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleNo 	=  '$no' AND
					trn_sampleinfomations.intSampleYear =  '$year' AND
					trn_sampleinfomations.intRevisionNo =  '$revNo'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$intMainStatus 					= $row['intStatus'];
		$intMarketingStatus 			= $row['intMarketingStatus'];
		$intTechnicalApproveLevelStart 	= $row['intTechnicalApproveLevelStart'];
		if($intMarketingStatus==1)
		{
			echo "This Sample is already confirmed.";
			return;
		}
		
		$bulk_order_for_prevoius_rev = get_bulk_order_for_pre_revision($no,$year,$revNo);
		if($bulk_order_for_prevoius_rev[2]>0){
			echo "Bulk order ".$bulk_order_for_prevoius_rev[0]."has been already raised. Order status is ".$bulk_order_for_prevoius_rev[1];
		}

		
		if($intMarketingStatus>1)
		{
		$sql = "UPDATE `trn_sampleinfomations` SET `intMarketingStatus`=intMarketingStatus-1 WHERE (`intSampleNo`='$no') AND (`intSampleYear`='$year') AND (`intRevisionNo`='$revNo') and intStatus not in(0,1)";
		$result = $db->RunQuery($sql);
			
			if($intMarketingStatus==2)
			{
				$ApproveLevels	= (int)getApproveLevel('Sample Information Sheet');
				
				$sql = "UPDATE `trn_sampleinfomations` SET `intStatus`=$ApproveLevels WHERE (`intSampleNo`='$no') AND (`intSampleYear`='$year') AND (`intRevisionNo`='$revNo') ";	
				$result = $db->RunQuery($sql);
				///////////////////////////// send a email /////////////////////////////////////////////////////
				include "mailTemplates/marketing1stConfirmEmail.php";
				
				//header("Location:mailTemplates/marketing1stConfirmEmail.php?no=$no&year=$year&revNo=$revNo");///
				////////////////////////////////////////////////////////////////////////////////////////////////
			}
		}
		
		/////////////// set approvel names ///////////////////////////
		$sql = "SELECT
					((trn_sampleinfomations.intMarketingApproveLevelStart)-trn_sampleinfomations.intMarketingStatus) as levels
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleNo =  '$no' AND
					trn_sampleinfomations.intSampleYear =  '$year' AND
					trn_sampleinfomations.intRevisionNo =  '$revNo'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$levels = $row['levels'];
		$sql = "DELETE FROM `trn_sampleinfomations_approvedby` WHERE (`intSampleNo`='$no') AND (`intSampleYear`='$year') AND (`intRevNo`='$revNo') AND (`intStage`='1') AND (`intApproveLevelNo`='$levels') ";
		$result = $db->RunQuery($sql);
		$sql = "INSERT INTO `trn_sampleinfomations_approvedby`(`intSampleNo`,`intSampleYear`,intRevNo,intStage,`intApproveLevelNo`,`intApproveUser`,`dtApprovedDate`) VALUES ('$no','$year','$revNo',1,'$levels','$userId',now())";
		$result = $db->RunQuery($sql);
	}
	else if($_REQUEST['status']=='reject')
	{
		$sql = "UPDATE `trn_sampleinfomations` SET `intMarketingStatus`=0 WHERE (`intSampleNo`='$no') AND (`intSampleYear`='$year') AND (`intRevisionNo`='$revNo') ";
		$result = $db->RunQuery($sql);
		
		$sqlAppBy	=  "INSERT INTO trn_sampleinfomations_approvedby 
						(intSampleNo, 
						intSampleYear, 
						intRevNo, 
						intStage, 
						intApproveLevelNo, 
						intApproveUser, 
						dtApprovedDate,
						intStatus
						)
						VALUES
						('$no', 
						'$year', 
						'$revNo', 
						1, 
						0, 
						'$userId',
						now(),
						0)";
						
		$result = $db->RunQuery($sqlAppBy);
	}

function get_bulk_order_for_pre_revision($sample,$sampleYear,$revisionNo){
	
		global $db;
		$sql 		= "SELECT
						group_concat(trn_orderdetails.intOrderNo,'/',trn_orderdetails.intOrderYear) AS orderNo,
					    count(trn_orderdetails.intOrderNo) as ordersCount,	group_concat(if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected',if(tb1.intStatus=-10,'Completed',if(tb1.intStatus=-2,'Cancel',if(tb1.intStatus=-1,'Revised','Pending')))))) as Status
						FROM
						trn_orderdetails
						INNER JOIN trn_orderheader AS tb1 ON trn_orderdetails.intOrderYear = tb1.intOrderYear AND trn_orderdetails.intOrderNo = tb1.intOrderNo
						WHERE
						trn_orderdetails.intSampleNo = '$sample' AND
						trn_orderdetails.intSampleYear = '$sampleYear' AND
						trn_orderdetails.intRevisionNo < '$revisionNo'
						";	
		$result 	= $db->RunQuery($sql);
		$row 		= mysqli_fetch_array($result);
		//if($row['orderNo']!='')
			$arr[0]= $row['orderNo'];
			$arr[1]= $row['Status'];
			$arr[2]= $row['ordersCount'];
	//else
			//return false;
	return $arr;
	
}
?>
var basepath	= 'presentation/customerAndOperation/sample/Report/';

$(document).ready(function() {
	$('#frmSampleReport #imgApprove').click(function(){
	var val = $.prompt('Are you sure you want to approve this sample ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										showWaiting();
										var url = basepath+"sampleReport-technical-db-set.php"+window.location.search+'&status=approve';
										$.ajax({type: 'post',url:url,async:false,dataType:'json',success:function(json){		
										hideWaiting();
										alert(json.msg);
										window.opener.location=window.opener.location;
										window.location.href = window.location.href;
										}});
										//$('txtApproveStatus').val('1');
										//document.getElementById('txtApproveStatus').value = 1;
										//document.getElementById('frmSampleReport').submit();
									}
								}});
	});
	
	$("#frmSampleReport .butProcess").die('click').live('click',function(){
		addProcess(this);
	});
	
	$('#frmSampleReport #imgReject').click(function(){
	var val = $.prompt('Are you sure you want to reject this sample ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basepath+"sampleReport-technical-db-set.php"+window.location.search+'&status=reject';
										//var obj = $.ajax({url:url,async:false});
										//window.location.href = window.location.href;
										$.ajax({type: 'post',url:url,async:false,dataType:'json',success:function(json){		
										hideWaiting();
										alert(json.msg);
										//window.opener.location=window.opener.location;
										window.location.href = window.location.href;
										}});
										//document.getElementById('txtApproveStatus').value = 0;
										//document.getElementById('frmSampleReport').submit();
									}
								}});
	});
});

function addProcess(obj)
{
	var rowId 	=  obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
	var cellId 	= obj.parentNode.parentNode.parentNode.parentNode.parentNode.cellIndex;
	popupWindow3('1');
	

	var sampNo	=	$(obj).parent().parent().find('.sampleNo').html();
	var sampYear	=	$(obj).parent().parent().find('.sampleYear').html();
	var rev	=	$(obj).parent().parent().find('.sampleRevision').html();
	var	combo	=   $(obj).parent().parent().find('.combo').html();
	var	printId	=   $(obj).parent().parent().find('.print').html();
	//alert(sampNo+","+sampYear+","+rev+","+combo+","+printId);
	
	$('#popupContact1').load(basepath+'processes.php?sampNo='+sampNo+'&sampYear='+sampYear+'&rev='+rev+'&combo='+URLEncode(combo)+'&print='+URLEncode(printId),function(){
		
		$('#frmProcesses #butClose1').die('click').live('click',disablePopup);
		
	});	
}

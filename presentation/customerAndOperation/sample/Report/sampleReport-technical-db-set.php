<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	$session_user=$_SESSION["userId"];
	//error_reporting(E_ALL);
    //ini_set('display_errors', 1);
	
try{
	include "{$backwardseperator}dataAccess/Connector.php";
	include "{$backwardseperator}libraries/mail/mail.php";
	//$response = array('type'=>'', 'msg'=>'');
	/////////// parameters /////////////////////////////
	//$requestType 			= $_REQUEST['requestType'];
	
	$no 	= $_REQUEST['no'];
	$year 	= $_REQUEST['year'];
	$revNo 	= $_REQUEST['revNo'];
	
	
	$err_msg = "";
	if($_REQUEST['status']=='approve')
	{
		$db->begin();
		$sql = "SELECT
					trn_sampleinfomations.intStatus,
					trn_sampleinfomations.intMarketingStatus,
					trn_sampleinfomations.intTechnicalStatus,
					trn_sampleinfomations.intTechnicalApproveLevelStart,
					trn_sampleinfomations.intMarketingApproveLevelStart
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleNo =  '$no' AND
					trn_sampleinfomations.intSampleYear =  '$year' AND
					trn_sampleinfomations.intRevisionNo =  '$revNo'
				";
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$intTechnicalStatus = $row['intTechnicalStatus'];
		//$intMainStatus = $row['intStatus'];
		$intMarketingApproveLevelStart = $row['intMarketingApproveLevelStart'];
		if($intTechnicalStatus==1)
		{
			$err_msg		 	= "This Sample is already confirmed.";
		    throw new Exception($err_msg);
		}
		
		$saved_routing	= getRoutingSavedStatus_all($no,$year,$revNo);
	/*	if($saved_routing['type'] ==0)
		{
			$msg =  "Please add routing for ".$saved_routing['combo']."/".$saved_routing['print'];
			$response['type'] 		= 'fail';
			$response['msg'] 		= $msg;
			throw new Exception($msg);
			//return;

		}*/
		//$stage = 2;
		//////////////////end of find stage//////////////////////////////////////////////////////
		
		$sql = "UPDATE `trn_sampleinfomations` SET `intStatus`=intStatus-1,`intTechnicalStatus`=intTechnicalStatus-1 WHERE (`intSampleNo`='$no') AND (`intSampleYear`='$year') AND (`intRevisionNo`='$revNo') and intStatus>1";
		$result = $db->RunQuery2($sql);
		if(!$result)
			{
				$err_msg		 	= $db->errormsg;
				throw new Exception($err_msg);
			}
		insert_special_items($no,$year,$revNo);		
		insert_foil_items($no,$year,$revNo)	;	
		
 		$sqltech = "SELECT
					trn_sampleinfomations.intTechnicalStatus
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleNo =  '$no' AND
					trn_sampleinfomations.intSampleYear =  '$year' AND
					trn_sampleinfomations.intRevisionNo =  '$revNo'
				";
		$resulttech = $db->RunQuery2($sqltech);	
		$rowtech	= mysqli_fetch_array($resulttech);
		
		if($rowtech['intTechnicalStatus']==1)
		{
			///////////////////////////// send a email /////////////////////////////////////
			include "mailTemplates/technical2ndConfirmEmail.php"; /*?no=$no&year=$year&revNo=$revNo")*/;
			include "mailTemplates/tocostingemail2.php"; /*?no=$no&year=$year&revNo=$revNo")*/;
			//header("Location:mailTemplates/technical2ndConfirmEmail.php?no=$no&year=$year&revNo=$revNo");
		}
		/////////////// set approvel names ///////////////////////////
		$sql = "SELECT
					((trn_sampleinfomations.intTechnicalApproveLevelStart)-trn_sampleinfomations.intTechnicalStatus) as levels
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleNo =  '$no' AND
					trn_sampleinfomations.intSampleYear =  '$year' AND
					trn_sampleinfomations.intRevisionNo =  '$revNo'
				";
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		//$levels = $row['levels'];
		$levels = 1;
		
		$sql = "INSERT INTO `trn_sampleinfomations_approvedby`(`intSampleNo`,`intSampleYear`,intRevNo,intStage,`intApproveLevelNo`,`intApproveUser`,`dtApprovedDate`) VALUES ('$no','$year','$revNo',2,'$levels','$userId',now())";
		$result = $db->RunQuery2($sql);
		if(!$result)
			{
				$err_msg		 	= $db->errormsg;
				throw new Exception($err_msg);	
			}
	 $db->commit();
	 $response['type'] 		= "pass";
	 $response['msg'] 		= "Approved Successfully."; 
	
	/* if($st){
			//header("Location:mailTemplates/technical2ndConfirmEmail.php?no=$no&year=$year&revNo=$revNo");
		 }*/
	 
	}
	else if($_REQUEST['status']=='reject')
	{   
		$db->begin();
		$sql = "UPDATE `trn_sampleinfomations` SET `intTechnicalStatus`=0 WHERE (`intSampleNo`='$no') AND (`intSampleYear`='$year') AND (`intRevisionNo`='$revNo') ";
		$result = $db->RunQuery2($sql);
		////////////////////Get Max ///////////////////
	   $sqlf	="SELECT max(intStatus) AS max FROM trn_sampleinfomations_approvedby WHERE (`intSampleNo` = '$no') AND (`intSampleYear` = '$year') AND (`intRevNo` = '$revNo') AND intStage = 2 ";
	    $resultsf = $db->RunQuery2($sqlf);
	    $row = mysqli_fetch_array($resultsf);
	    $max = $row['max']+1;
			
		
	    $sql = "UPDATE `trn_sampleinfomations_approvedby` SET intStatus = '$max' WHERE (`intSampleNo` = '$no') AND (`intSampleYear` = '$year') AND (`intRevNo` = '$revNo') AND intStage = 2 AND intStatus = 0";
	    $result = $db->RunQuery2($sql);
		if(!$result)
			{
				$err_msg		 	= $db->errormsg;
				throw new Exception($err_msg);
			}
		
		$sqlAppBy	=  "INSERT INTO trn_sampleinfomations_approvedby 
						(intSampleNo, 
						intSampleYear, 
						intRevNo, 
						intStage, 
						intApproveLevelNo, 
						intApproveUser, 
						dtApprovedDate,
						intStatus
						)
						VALUES
						('$no', 
						'$year', 
						'$revNo', 
						2, 
						0, 
						'$userId',
						now(),
						0)";
						
		$result = $db->RunQuery2($sqlAppBy);
		
		if(!$result)
			{
				$err_msg		 	= $db->errormsg;
				throw new Exception($err_msg);
			}	
		
			$db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Rejected Successfully."; 		
	 }  
	}
	catch(Exception $e)
    {	
		   $db->rollback();		 
		   $response['type'] 	=  'fail';
		   $response['msg']      =  $e->getMessage();
		  // echo json_encode($response);
    }
 echo json_encode($response);
	 
	
 	
//-------------------------------------------------------------------	
	
	
		function insert_foil_items($sampleNo,$sampleYear,$revisionNo){
	
		global $db;
		global $companyId;	
		global $session_user;
	
		$sqlm = "SELECT DISTINCT 
				trn_sampleinfomations_details.strPrintName, 
				trn_sampleinfomations_details.strComboName, 
				trn_sampleinfomations_details.intTechniqueId, 
				trn_sampleinfomations_details.intColorId, 
				trn_sampleinfomations_details.strPrintName, 
				trn_sampleinfomations_details.intItem,(size_w+0.5) as size_w,(size_h+0.5) as size_h,
				mst_item.strName  ,'new' as status ,  
				1 as qty ,
				dblQty as meters
				FROM
				trn_sampleinfomations_details
				Right Join mst_item ON trn_sampleinfomations_details.intItem = mst_item.intId
				INNER JOIN mst_techniques ON trn_sampleinfomations_details.intTechniqueId=mst_techniques.intId
				WHERE
				trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
				trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
				trn_sampleinfomations_details.intRevNo =  '$revisionNo' AND
				/*trn_sampleinfomations_details.intTechniqueId =  '4' */
				mst_item.ROLL_FORM =  '1' 

			ORDER BY
				mst_item.strName ASC
			";
			$result_tech = $db->RunQuery2($sqlm);
			while($rowm=mysqli_fetch_array($result_tech))
			{
				$printName 		= $rowm['strPrintName'];
				$comboName 		= $rowm['strComboName']; 
				$technique 		= $rowm['intTechniqueId'];
				$intColorId		= $rowm['intColorId'];
				$foilItem 		= $rowm['intItem'];
				$qty 			= $rowm['qty'];
				$meters			= $rowm['meters'];
				$print_width	= $rowm['size_w'];
				$print_height	= $rowm['size_h'];

				$sql = "INSERT INTO `trn_sample_foil_consumption` (`intSampleNo`,`intSampleYear`,`intRevisionNo`,`strCombo`,`strPrintName`,`intTechniqueId`,`intColorId`,`intItem`,				`dblWidth`,`dbHeight`,`dblFoilQty`,`dblMeters`,`strCuttingSide`,`intUser`,`dtDate`) 
															VALUES ('$sampleNo','$sampleYear','$revisionNo','$comboName','$printName','$technique','$intColorId','$foilItem','$print_width','$print_height','$qty','$meters','','$session_user',now())";
				$result = $db->RunQuery2($sql);
				if(!$result)
			      {
					$err_msg		 	= $db->errormsg;
					throw new Exception($err_msg);	
			      }
			}

	}
	
	function insert_special_items($sampleNo,$sampleYear,$revisionNo){
		global $db;
		global $companyId;	
		global $session_user;
		
		 $sqlm = "SELECT DISTINCT 
		 		trn_sampleinfomations_details.strComboName,
				trn_sampleinfomations_details.strPrintName,  
				trn_sampleinfomations_details.intTechniqueId, 
				trn_sampleinfomations_details.intColorId, 
				trn_sampleinfomations_details.intItem,
				sum(trn_sampleinfomations_details.dblQty) as dblQty , 
				mst_item.strName  
				FROM
				trn_sampleinfomations_details
				Right Join mst_item ON trn_sampleinfomations_details.intItem = mst_item.intId
				INNER JOIN mst_techniques ON trn_sampleinfomations_details.intTechniqueId=mst_techniques.intId
				WHERE
				trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
				trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
				trn_sampleinfomations_details.intRevNo =  '$revisionNo' AND
				/*trn_sampleinfomations_details.intTechniqueId !=  '4'*/
				mst_item.ROLL_FORM <>  '1'
			group by 
			trn_sampleinfomations_details.strComboName,
				trn_sampleinfomations_details.strPrintName,  
				trn_sampleinfomations_details.intTechniqueId, 
				trn_sampleinfomations_details.intColorId, 
				trn_sampleinfomations_details.intItem
			
			ORDER BY
				mst_item.strName ASC
			";
			$result_tech = $db->RunQuery2($sqlm);
			while($rowm=mysqli_fetch_array($result_tech))
			{
				
				$printName 	= $rowm['strPrintName'];
				$comboName 	= $rowm['strComboName']; 
				$technique 	= $rowm['intTechniqueId'];
				$intColorId	= $rowm['intColorId'];
				$item 		= $rowm['intItem'];
				$qty 		= $rowm['dblQty'];
				$sql = "INSERT INTO `trn_sample_spitem_consumption`(`intSampleNo`,`intSampleYear`,`intRevisionNo`,`strCombo`,`strPrintName`,`intTechniqueId`,`intColorId`,`intItem`,`dblQty`,`intUser`,`dtDate`) 
				VALUES ('$sampleNo','$sampleYear','$revisionNo','$comboName','$printName','$technique','$intColorId','$item','$qty','$session_user',now())";
				$result = $db->RunQuery2($sql);
				if(!$result)
			      {
					$err_msg		 	= $db->errormsg;
					throw new Exception($err_msg);	
			      }
			}

	}
	
	function getRoutingSavedStatus_all($sampleNo,$sampleYear,$revisionNo){
	
		global $db;
		
		$sql	= "SELECT
					trn_sampleinfomations_details.strPrintName,
					trn_sampleinfomations_details.strComboName
					FROM `trn_sampleinfomations_details`
					WHERE
					trn_sampleinfomations_details.intSampleNo = '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear = '$sampleYear' AND
					trn_sampleinfomations_details.intRevNo = '$revisionNo' 
					";
		$result = $db->RunQuery2($sql);

		while($row=mysqli_fetch_array($result)){
            $combo	= $row['strComboName'];
			$print	= $row['strPrintName'];
			
			$saved_routing	= getRoutingSavedStatus($sampleNo,$sampleYear,$revisionNo,$combo,$print);
			if($saved_routing ==0)
			{
				$msg =  "Please add routing for ".$combo."/".$print;
				$response['type'] 		= 'fail';
				$response['msg'] 		= $msg;
				throw new Exception($msg);
				//return;
	
			}
		}
		
	}
	
	function getRoutingSavedStatus($sampleNo,$sampleYear,$revision,$combo,$print){
	
	global $db;
	
	$sql_select = "SELECT
					trn_sampleinfomations_combo_print_routing.SAMPLE_NO,
					trn_sampleinfomations_combo_print_routing.SAMPLE_YEAR
					FROM `trn_sampleinfomations_combo_print_routing`
					WHERE
					trn_sampleinfomations_combo_print_routing.SAMPLE_NO = '$sampleNo' AND
					trn_sampleinfomations_combo_print_routing.SAMPLE_YEAR = '$sampleYear' AND
					trn_sampleinfomations_combo_print_routing.REVISION = '$revision' AND
					trn_sampleinfomations_combo_print_routing.COMBO = '$combo' AND
					trn_sampleinfomations_combo_print_routing.PRINT = '$print'
					";
	  
	   $result_select = $db->RunQuery2($sql_select);//run without considering errors
 	   
	   if(!$result_select)
	   return 0;
	   else
	   return 1;

}
	


?>
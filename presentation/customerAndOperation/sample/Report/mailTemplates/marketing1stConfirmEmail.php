<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

$backwardseperator = "../../../../../";
$companyId = $_SESSION['CompanyID'];
$userId = $_SESSION['userId'];

$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include_once $_SESSION['ROOT_PATH']."dataAccess/Connector.php";


$no = $_REQUEST['no'];
$year = $_REQUEST['year'];
$revNo = $_REQUEST['revNo'];
$techniqueGroups = "";


include_once $_SESSION['ROOT_PATH']."libraries/mail/mail.php";
//sendMessage('anuruddha@screenlineholdings.com','qpet','roshanocs@gmail.com',"NEW SAMPLE",'body');
////////////////////////// GET SAMPLE ENTERED USER //////////////////////////////////////////////////
$sql1 = "SELECT
			sys_users.strUserName,
			sys_users.strEmail,trn_sampleinfomations.strAdditionalInstructions
		FROM
			trn_sampleinfomations
			Inner Join sys_users ON sys_users.intUserId = trn_sampleinfomations.intCreator
		WHERE
			trn_sampleinfomations.intSampleNo =  '$no' AND
			trn_sampleinfomations.intSampleYear =  '$year' AND
			trn_sampleinfomations.intRevisionNo =  '$revNo'
		";
$result1 = $db->RunQuery($sql1);
while($row1=mysqli_fetch_array($result1))
{
	$enterUserName = $row1['strUserName'];
	$enterUserEmail = $row1['strEmail'];
	$strAdditionalInstructions = $row1['strAdditionalInstructions'];
}

$techniqueGroupQuery= "select  group_concat(TECHNIQUE_GROUP_NAME separator ', ') as technique_groups 
                       from (select distinct TECHNIQUE_GROUP_NAME from mst_technique_groups left join 
                       mst_techniques on mst_techniques.intGroupId=mst_technique_groups.TECHNIQUE_GROUP_ID left join 
                       trn_sampleinfomations_details on trn_sampleinfomations_details.intTechniqueId=mst_techniques.intId where 
                       intSampleNo='$no' and intSampleYear='$year' and intRevNo='$revNo') as tb
		";
$tGroups = $db->RunQuery($techniqueGroupQuery);
while($group=mysqli_fetch_array($tGroups))
{
    $techniqueGroups = $group["technique_groups"];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////


$sql1 ="SELECT
			sys_users.strUserName,
			sys_users.strEmail
		FROM
			sys_mail_eventusers
			Inner Join sys_users ON sys_users.intUserId = sys_mail_eventusers.intUserId  and sys_users.intStatus = 1
		WHERE
			sys_mail_eventusers.intMailEventId =  '1000' AND
			sys_mail_eventusers.intCompanyId =  '$companyId'
		";
$result1 = $db->RunQuery($sql1);
while($row1=mysqli_fetch_array($result1))
{
	
	ob_start();
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Marketing Confirm Email</title>
<style type="text/css">
.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntWhite {
	font-family: Verdana;
	font-size: 11px;
	color: #FFF;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.sampleNo{
	color: #039; font-weight: bold; font-size: 12px; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;	
}
.part{
	color: #096CBD;
	font-weight: bold;
	font-size: 13px;
	font-family: "Comic Sans MS", cursive;
}
.tableBorder_allRound{
	
	border: 1px solid #CCCCCC;
	-moz-border-radius-bottomright:10px;
	-moz-border-radius-bottomleft:10px;
	-moz-border-radius-topright:10px;
	-moz-border-radius-topleft:10px;
}
</style>
</head>

<body>
<table  width="642" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2" class="normalfnt" style="color:#3E437D">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="117" class="normalfnt">Dear <strong><?php echo $row1['strUserName']; ?></strong>,</td>
    <td width="474">&nbsp;</td>
    <td width="51">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <?PHP 
	if($revNo==0)
	{
	?>
    <td class="normalfnt">I just entered a new sample to NSOFT. Please refer the below link to update Techniques.</td>
    <?PHP
	}else{
	?>
     <td class="normalfnt">I did some changes for this sample.</td>
    <?php } ?>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">click <a href="<?php 
	echo $mainPath."?q=369&sampleNo=$no&sampleYear=$year&revNo=$revNo"; ?>">here</a> to view this sample.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="108" colspan="3"><table  style="border:double;border-color:#09F" width="100%" height="70" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="19%" height="19" bgcolor="#D9ED9C" class="normalfnt"><strong>Sample No</strong></td>
        <td width="21%" bgcolor="#D9ED9C" class="sampleNo" ><?php echo "$no/$year"; ?></td>
        <td width="17%" bgcolor="#D9ED9C" class="normalfnt"><strong>Revision No</strong></td>
        <td width="34%" bgcolor="#D9ED9C" class="sampleNo"><?php echo $revNo; ?></td>
        <td width="9%" bgcolor="#D9ED9C">&nbsp;</td>
        </tr>
      <tr>
        <td height="19" class="normalfnt"><strong>Marketer Remarks</strong></td>
        <td colspan="4"><span class="tableBorder_allRound"><?php echo $strAdditionalInstructions; ?></span></td>
        </tr>
      <?php
	  	$sql = "SELECT
					trn_sampleinfomations_printsize.strPrintName,
					mst_part.strName,
					trn_sampleinfomations_printsize.intWidth,
					trn_sampleinfomations_printsize.intHeight
				FROM
					trn_sampleinfomations_printsize
				Inner Join mst_part ON mst_part.intId = trn_sampleinfomations_printsize.intPart
				WHERE
					trn_sampleinfomations_printsize.intSampleNo =  '$no' AND
					trn_sampleinfomations_printsize.intSampleYear =  '$year' AND
					trn_sampleinfomations_printsize.intRevisionNo =  '$revNo'
				order by strPrintName
					
				";
		$result = $db->RunQuery($sql);
		$i=0;
		while($row=mysqli_fetch_array($result))
		{
			$printName = $row['strPrintName'];
			$strName = $row['strName'];
			$intWidth = $row['intWidth'];
			$intHeight = $row['intHeight'];
			$i++;
			
			//$img =  base64_encode(file_get_contents("../../../../../documents/sampleinfo/samplePictures/$no-$year-$revNo-$i.jpg"));
	  ?>
      <tr>
        <td colspan="2" class="part"><?php echo "$printName - $strName - ($intWidth * $intHeight)" ?></td>
        <td colspan="2">
          <div id="divPicture3" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:264px;height:148px;overflow:hidden" >
          <img src="<?php 
	echo $mainPath."/documents/sampleinfo/samplePictures/$no-$year-$revNo-$i.png"; ?>" width="260" height="148" id="saveimg"  />
            
          </div></td>
        <td>&nbsp;</td>
        </tr>
        <?php
		}
		?>
        
      <tr>
        <td colspan="2">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class="normalfnt">Thanks,</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="normalfnt"><strong><?php echo $enterUserName; ?></strong><br />
    ...................</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="normalfnt">(This is a<strong> <span style="color:#F28415">NSOFT</span> </strong>system generated email.)</td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
<?php
$body = ob_get_clean();

//echo $body;
//$file = 	base64_encode(file_get_contents("../../../../../documents/sampleinfo/samplePictures/$no-$year-$revNo-$i.jpg")); 
//$body = "<img  style=\"width:200px;height:110px\" src=\"http://122.255.62.148/qpet/abc.png\" />";

		echo sendMessage($enterUserEmail,$enterUserName,$row1['strEmail'],($revNo>0?"NEW REVISION FOR":"NEW SAMPLE")." ($no/$year) - ".$techniqueGroups,$body);
}


?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";
$location = $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];


$approveLevel 	= 	(int)getMaxApproveLevel();
$programName	=	'Sample Place Order';
$programCode	=	'P0354';
$intUser  		= 	$_SESSION["userId"];
$reportID		=   983;
$menuID			=   354;

$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];

$where_string = '';
$where_array = array(
				'Status'=>'tb1.intStatus',
				'Order_No'=>'tb1.intSampleOrderNo',
				'Order_Year'=>'tb1.intSampleOrderYear',
				'Sample_ID'=>"concat(tb1.intSampleNo,'/',tb1.intSampleYear)",
				'Required_date'=>'trn_sampleorderdetails.dtRequiredDate',
				'Date'=>'tb1.dtDate',
				'customer'=>'mst_customer.strName',
				'Style'=>'trn_sampleinfomations.strStyleNo'
				);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2');
if($arr)
{
	foreach($arr as $k=>$v)
	{
		if($v['field']=='Status')
		{
			if($arr_status[$v['data']]==2)
				$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
			else
				$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
		}
		else if($where_array[$v['field']])
			$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
	}
}
if(!count($arr)>0)					 
	$where_string .= "AND tb1.dtDate = '".date('Y-m-d')."'";
	
$sql = "select *,if(dispQty=0,0,round(((dispQty)/qty * 100)))  as progress,(dispQty) as disQty  from(SELECT DISTINCT 
							if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected',if(tb1.intStatus='-2','Canceled','Pending'))) as Status,
							tb1.intSampleOrderNo as Order_No, 
							tb1.intSampleOrderYear as Order_Year,
							mst_brand.strName AS Brand,
							trn_sampleinfomations.strGraphicRefNo AS GraphicNo,
							sys_users.strUserName AS Marketer,
							concat(tb1.intSampleNo,'/',tb1.intSampleYear) as Sample_ID, 
							(SELECT
							trn_sampleorderdetails.dtRequiredDate
							FROM
							trn_sampleorderdetails 
							WHERE trn_sampleorderdetails.intSampleOrderNo = tb1.intSampleOrderNo AND trn_sampleorderdetails.intSampleOrderYear = tb1.intSampleOrderYear  
							limit 1) as Required_date, 
							tb1.dtDate as Date, 
							mst_customer.strName AS customer,
							trn_sampleinfomations.strStyleNo as Style, 
							IFNULL((SELECT
								Sum(trn_sampleorderdetails.intQty) 
								FROM trn_sampleorderdetails
								WHERE
								trn_sampleorderdetails.intSampleOrderNo = tb1.intSampleOrderNo AND
								trn_sampleorderdetails.intSampleOrderYear =  tb1.intSampleOrderYear
							),0) as Qty, 
							
	
							IFNULL((SELECT
								Sum(trn_sampleorderdetails.intBalToDeliverQty) 
								FROM trn_sampleorderdetails
								WHERE
								trn_sampleorderdetails.intSampleOrderNo = tb1.intSampleOrderNo AND
								trn_sampleorderdetails.intSampleOrderYear =  tb1.intSampleOrderYear
							),0) as balQty, 
							IFNULL((SELECT
									Sum(trn_sampledispatchdetails.dblDispatchQty)
									FROM
									trn_sampledispatchdetails
									INNER JOIN trn_sampledispatchheader ON trn_sampledispatchdetails.intDispatchNo = trn_sampledispatchheader.intDispatchNo AND trn_sampledispatchdetails.intDispatchYear = trn_sampledispatchheader.intDispatchYear
									WHERE
									trn_sampledispatchdetails.SAMPLE_ORDER_NO = tb1.intSampleOrderNo AND
									trn_sampledispatchdetails.SAMPLE_ORDER_YEAR = tb1.intSampleOrderYear AND
									trn_sampledispatchheader.intStatus = 1),0) as dispQty, 
							IFNULL((SELECT
									Sum(trn_sampleorder_fabricin_details.INQTY)
									FROM
									trn_sampleorder_fabricin_details
									INNER JOIN trn_sampleorder_fabricin_header ON trn_sampleorder_fabricin_details.SAMPLE_FABRICIN_NO = trn_sampleorder_fabricin_header.SAMPLE_FABRICIN_NO AND trn_sampleorder_fabricin_details.SAMPLE_FABRICIN_YEAR = trn_sampleorder_fabricin_header.SAMPLE_FABRICIN_YEAR
									WHERE
									trn_sampleorder_fabricin_header.SAMPLE_ORDER_NO = tb1.intSampleOrderNo AND
									trn_sampleorder_fabricin_header.SAMPLE_ORDER_YEAR = tb1.intSampleOrderYear AND
									trn_sampleorder_fabricin_header.STATUS = 1),0) as inQty, 
							0 as progress2,
							(select  GROUP_CONCAT(DISTINCT intDispatchNo) from trn_sampledispatchdetails sampleDis where sampleDis.intSampleNo=tb1.intSampleNo and sampleDis.SAMPLE_ORDER_NO = tb1.intSampleOrderNo and sampleDis.intSampleYear=tb1.intSampleYear and sampleDis.intRevisionNo=tb1.intRevNo) as DispatchNO,
(select  GROUP_CONCAT(DISTINCT intDispatchYear) from trn_sampledispatchdetails sampleDis where sampleDis.intSampleNo=tb1.intSampleNo and sampleDis.SAMPLE_ORDER_NO = tb1.intSampleOrderNo and sampleDis.intSampleYear=tb1.intSampleYear and sampleDis.intRevisionNo=tb1.intRevNo) as DispatchYear,
							IFNULL((
                                      SELECT
								concat(sys_users.strUserName,'(',max(trn_sampleorderheader_approvedby.dtApprovedDate),')' )
								FROM
								trn_sampleorderheader_approvedby
								Inner Join sys_users ON trn_sampleorderheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								trn_sampleorderheader_approvedby.intSampleOrderNo  = tb1.intSampleOrderNo AND
								trn_sampleorderheader_approvedby.intSampleOrderYear =  tb1.intSampleOrderYear AND
								trn_sampleorderheader_approvedby.intApproveLevelNo =  '1' AND
								trn_sampleorderheader_approvedby.STATUS =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.intStatus>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(trn_sampleorderheader_approvedby.dtApprovedDate),')' )
								FROM
								trn_sampleorderheader_approvedby
								Inner Join sys_users ON trn_sampleorderheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								trn_sampleorderheader_approvedby.intSampleOrderNo  = tb1.intSampleOrderNo AND
								trn_sampleorderheader_approvedby.intSampleOrderYear =  tb1.intSampleOrderYear AND
								trn_sampleorderheader_approvedby.intApproveLevelNo =  '$i' AND
								trn_sampleorderheader_approvedby.STATUS =  '0'
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.intStatus>1) AND (tb1.intStatus<=tb1.APPROVE_LEVELS) AND ((SELECT
								max(trn_sampleorderheader_approvedby.dtApprovedDate)
								FROM
								trn_sampleorderheader_approvedby
								Inner Join sys_users ON trn_sampleorderheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								trn_sampleorderheader_approvedby.intSampleOrderNo  = tb1.intSampleOrderNo AND
								trn_sampleorderheader_approvedby.intSampleOrderYear =  tb1.intSampleOrderYear AND
								trn_sampleorderheader_approvedby.intApproveLevelNo =  ($i-1) AND
								trn_sampleorderheader_approvedby.STATUS='0' )<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*fase part*/
								 if($i>tb1.APPROVE_LEVELS,'-----',''))
								
								/*end IFNULL false part*/
								) as `".$approval."`, "; 
									
								}
						$sql .= "IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(trn_sampleorderheader_approvedby.dtApprovedDate),')' )
								FROM
								trn_sampleorderheader_approvedby
								Inner Join sys_users ON trn_sampleorderheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								trn_sampleorderheader_approvedby.intSampleOrderNo  = tb1.intSampleOrderNo AND
								trn_sampleorderheader_approvedby.intSampleOrderYear =  tb1.intSampleOrderYear AND
								trn_sampleorderheader_approvedby.intApproveLevelNo =  '-2' AND
								trn_sampleorderheader_approvedby.STATUS =  '0'
							),IF(((SELECT
								menupermision.intCancel 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND 
								tb1.intStatus=1),'Cancel', '')) as `Cancel`,";
								
						$sql .= " 'View' as `View`,
   								   mst_sample_category.`NAME` AS SAMPLE_CATEGORY
						FROM
							trn_sampleorderheader as tb1
							Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = tb1.intSampleNo 
							AND trn_sampleinfomations.intSampleYear = tb1.intSampleYear 
							AND tb1.intRevNo = trn_sampleinfomations.intRevisionNo
							INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
							Inner Join mst_customer ON mst_customer.intId = trn_sampleinfomations.intCustomer
							INNER JOIN sys_users ON trn_sampleinfomations.intMarketer = sys_users.intUserId
							INNER JOIN mst_sample_category ON tb1.`SAMPLE CATEGORY` = mst_sample_category.ID 
							WHERE tb1.intLocationId='$location' 
							$where_string
							)  as t where 1=1
						";
					    //  	echo $sql;
						  
						  
						  
						  
$col = array();

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected;Canceled:Canceled" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist

$cols[] = $col;	$col=NULL;
//Fabric Receive No
$col["title"] 	= "Order No"; // caption of column
$col["name"] 	= "Order_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= "?q=".$menuID."&orderNo={Order_No}&orderYear={Order_Year}";	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

$reportLink  = "?q=".$reportID."&orderNo={Order_No}&year={Order_Year}";
$reportLinkApprove  = "?q=".$reportID."&orderNo={Order_No}&year={Order_Year}&approveMode=1";
$reportLinkCancel   = "?q=".$reportID."&orderNo={Order_No}&year={Order_Year}&approveMode=2";

$cols[] = $col;	$col=NULL;


//Fabric Receive Year
$col["title"] = "Order Year"; // caption of column
$col["name"] = "Order_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//CUSTOMER PO NO
$col["title"] = "Sample ID"; // caption of column
$col["name"] = "Sample_ID"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER PO NO
$col["title"] = "Sample Category"; // caption of column
$col["name"] = "SAMPLE_CATEGORY"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Date
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Required Date
$col["title"] = "Required Date"; // caption of column
$col["name"] = "Required_date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Customer"; // caption of column
$col["name"] = "customer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Style
$col["title"] = "Style"; // caption of column
$col["name"] = "Style"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Brand
$col["title"] = "Brand"; // caption of column
$col["name"] = "Brand"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;


//GraphicNo
$col["title"] = "Graphic No"; // caption of column
$col["name"] = "GraphicNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Marketer
$col["title"] = "Marketer"; // caption of column
$col["name"] = "Marketer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;


//Dispatch No
$col["title"] = "Dispatched NO"; // caption of column
$col["name"] = "DispatchNO"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Dispatch Year
$col["title"] = "Dispatched Year"; // caption of column
$col["name"] = "DispatchYear"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//DATE
$col["title"] = "Qty"; // caption of column
$col["name"] = "Qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//DATE
$col["title"] = "In Qty"; // caption of column
$col["name"] = "inQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//DATE
$col["title"] = "Dispatch Qty"; // caption of column
$col["name"] = "disQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

# Custom made column to show bar graph
$col = array();
$col["title"] = "Progress";
$col["name"] = "progress";
$col["width"] = "3";
$col["align"] = "left";
$col["search"] = false;
$col["sortable"] = false;
$col["default"] = "<div style='width:{progress}px; background-color:green; height:18px; align:left'><div style='width:{4}px;color:blue; height:14px;text-align:left'>{progress}%</div></div>";
$cols[] = $col;
$col=NULL;

//FIRST APPROVAL
$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}

$col["title"] 				= 'Cancel'; 
$col["name"] 				= 'Cancel'; 
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= $reportLinkCancel;
$col['linkName']			= 'Cancel';
$col["linkoptions"] 		= "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col		= NULL;

//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$d	=date('Y-m-d');
$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"},{"field":"Date","op":"eq","data":"$d"}
     ]

}
SEARCH_JSON;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Sample Order Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'Required_date'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);




$out = $jq->render("list1");
?>
<title>Sample Order Listing</title>
<?php 
		//include "include/listing.html";
	?>

<form id="frmlisting" name="frmlisting" method="post" action="">
        <div align="center" style="margin:10px">
        
			<?php echo $out?>
        </div>
</form>
<?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(trn_sampleorderheader.APPROVE_LEVELS) AS appLevel
			FROM trn_sampleorderheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
?>


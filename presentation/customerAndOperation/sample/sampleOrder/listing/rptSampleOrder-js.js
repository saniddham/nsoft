// JavaScript Document
var basePath	= "presentation/customerAndOperation/sample/sampleOrder/listing/";
$(document).ready(function() {
	
	$('#frmSamplePlaceOrderReport').validationEngine();
	
	$('#frmSamplePlaceOrderReport #butRptConfirm').click(function(){
		
		var val = $.prompt('Are you sure you want to approve this Sample Order ?',{
					buttons: { Ok: true, Cancel: false },
					callback: function(v,m,f){
						if(v)
						{
						showWaiting();
						var url = basePath+"rptSampleOrder-db-set.php"+window.location.search+'&requestType=approve';
						var obj = $.ajax({
							url:url,
							type:'post',
							dataType: "json",  
							data:'',
							async:false,
							
							success:function(json){
									$('#frmSamplePlaceOrderReport #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
									if(json.type=='pass')
									{
										var t=setTimeout("alertx1()",1000);
										window.location.href = window.location.href;
										window.opener.location.reload();//reload listing page
										return;
									}
								},
							error:function(xhr,status){
									
									$('#frmSamplePlaceOrderReport #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
									var t=setTimeout("alertx1()",3000);
								}		
							});
	
							}
						   hideWaiting();
					}});
	});
	
	$('#frmSamplePlaceOrderReport #butRptReject').click(function(){
		
		var val = $.prompt('Are you sure you want to Reject this Sample Order ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"rptSampleOrder-db-set.php"+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmSamplePlaceOrderReport #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx2()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSamplePlaceOrderReport #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx2()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
	});
	$('#frmSamplePlaceOrderReport #butRptCancel').click(function(){
		
		var val = $.prompt('Are you sure you want to Cancel this Sample Order ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"rptSampleOrder-db-set.php"+window.location.search+'&requestType=cancel';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmSamplePlaceOrderReport #butRptCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx3()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSamplePlaceOrderReport #butRptCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx3()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
	});
});
function alertx1()
{
	$('#frmSamplePlaceOrderReport #butRptConfirm').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmSamplePlaceOrderReport #butRptReject').validationEngine('hide')	;
}
function alertx3()
{
	$('#frmSamplePlaceOrderReport #butRptCancel').validationEngine('hide')	;
}

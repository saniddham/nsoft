<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId 				= $_SESSION['CompanyID'];
$userId 				= $_SESSION['userId'];
$locationId 			= $companyId;
$programCode			= 'P0354';

include_once  "class/cls_commonErrorHandeling_get.php";

$obj_commonErrHandle 	= new cls_commonErrorHandeling_get($db);

$poNo 					= $_REQUEST['orderNo'];
$year 					= $_REQUEST['year'];
$poYear 				= $_REQUEST['year'];
$approveMode 			= $_REQUEST['approveMode'];

if($approveMode==1)
	$mode = 'Confirm';
else if($approveMode==2)
	$mode = 'Cancel';

$sql = "SELECT
		trn_sampleorderheader.dtDate as orderDate,
		trn_sampleorderheader.dtmCreateDate AS CREATED_DATE,
		trn_sampleorderheader.intStatus,
		mst_customer.strName,
		trn_sampleinfomations.strStyleNo,strGraphicRefNo ,		
		sys_users.strUserName as enterUser ,
		sys_users.strUserName as CREATOR ,
		trn_sampleorderheader.intSampleNo,
		trn_sampleorderheader.intSampleYear,
		trn_sampleorderheader.strRemarks,
		mst_customer_locations_header.strName as custLocation, 
		trn_sampleorderheader.strAttentionTo,
		trn_sampleorderheader.intStatus as STATUS,
		trn_sampleorderheader.APPROVE_LEVELS as LEVELS,
		mst_sample_category.`NAME` AS category_name
		FROM
		trn_sampleorderheader
		Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleorderheader.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleorderheader.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_sampleorderheader.intRevNo
		Inner Join mst_customer ON mst_customer.intId = trn_sampleinfomations.intCustomer
		Inner Join sys_users ON sys_users.intUserId = trn_sampleorderheader.intCreator 
		left Join mst_customer_locations_header ON trn_sampleorderheader.intCustomerLocation = mst_customer_locations_header.intId
		INNER JOIN mst_sample_category ON  mst_sample_category.ID = trn_sampleorderheader.`SAMPLE CATEGORY`
		WHERE
		trn_sampleorderheader.intSampleOrderNo =  '$poNo' AND
		trn_sampleorderheader.intSampleOrderYear =  '$poYear'
		";
		
			$result 		= $db->RunQuery($sql);
			$header_array	= mysqli_fetch_array($result);
			$customer 		= $header_array['strName'];
			$style 			= $header_array['strStyleNo'];
			$graphic 		= $header_array['strGraphicRefNo'];
			$orderDate 		= $header_array['orderDate'];
			$enterUser 		= $header_array['enterUser'];
			$attentionTo	= $header_array['strAttentionTo'];
			$custLocation	= $header_array['custLocation'];
			$sampleNo 		= $header_array['intSampleNo'];
			$sampleYear		= $header_array['intSampleYear'];
			//$poBy 		= $row['UserName'];
			$intStatus 		= $header_array['intStatus'];
			$remarks 		= $header_array['strRemarks'];
			$levels			= $header_array['LEVELS'];
			$category_name  = $header_array['category_name'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_reject($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_reject			= $permition_arr['permision'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_confirm			= $permition_arr['permision'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_cancel($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_cancel			= $permition_arr['permision'];	

?>
<head>
<title>Sample Place Order Report</title>
<link rel="stylesheet" type="text/css" href="css/button.css"/>
<link rel="stylesheet" type="text/css" href="css/promt.css"/>
<script type="application/javascript" src="presentation/customerAndOperation/sample/sampleOrder/listing/rptSampleOrder-js.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position: absolute;
	left: 350px;
	top: 150px;
	width: 650px;
	height: 322px;
	z-index: 1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
if($intStatus=='-2')
{
	echo "<div id=\"apDiv1\" style=\"opacity:0.2\"><img src=\"images/cancelled.png\"/></div>";
}
elseif($intStatus!=1)//pending
{
	echo "<div id=\"apDiv1\"><img src=\"images/pending.png\"/></div>";
}
?>
<form id="frmSamplePlaceOrderReport" name="frmSamplePlaceOrderReport" method="post">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>
<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>SAMPLE PLACE ORDER</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<?php
	include "presentation/report_approve_status_and_buttons.php"
 ?>
<tr>
<td> 
  <table width="100%">
  <tr>
    <td width="7%">&nbsp;</td>
    <td width="13%" class="normalfnt"><strong>Order No</strong></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="30%"><span class="normalfnt"><?php echo $poNo ?>/<?php echo $year ?></span></td>
    <td width="21%" class="normalfnt"><strong>Sample No</strong></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td colspan="2"><span class="normalfnt"><?php echo "$sampleNo/$sampleYear"; ?></span></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Customer</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $customer ?></span></td>
    <td width="21%" class="normalfnt"><strong>Graphic No</strong></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="25%"><span class="normalfnt"><?php echo $graphic ?></span></td>
    </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Style No</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $style ?></span></td>
    <td><span class="normalfnt"><strong> Order Date</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $orderDate ?></span></td>
    </tr>
<tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Remarks</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $remarks ?></span></td>
    <td class="normalfnt"><strong>Customer Location</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td colspan="2"><span class="normalfnt"><?php echo $custLocation?></span></td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Sample Category</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td colspan="2"><span class="normalfnt"><?php echo $category_name ?></span></td>
    </tr>
    </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td class="normalfnt">
          <table width="850" class="rptBordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr>
              <th width="13%"  height="22">Combo</th>
              <th width="17%" >Part</th>
              <th width="12%" >Grade</th>
              <th width="24%" >Sample Type</th>
              <th width="9%" >Size</th>
              <th width="19%" >Required Date</th>
              <th width="6%" >Qty</th>
              </tr>
            <?php 
	  	 $sql1 = "SELECT
trn_sampleorderdetails.strCombo,
mst_part.strName,
trn_sampleorderdetails.intGrade,
trn_sampleorderdetails.intQty,
mst_sampletypes.strName as sampleType,
trn_sampleorderdetails.strSize,
trn_sampleorderdetails.dtRequiredDate
FROM
trn_sampleorderdetails
Inner Join mst_part ON trn_sampleorderdetails.intPart = mst_part.intId
inner join mst_sampletypes on trn_sampleorderdetails.intSampleTypeId = mst_sampletypes.intId
WHERE
trn_sampleorderdetails.intSampleOrderNo =  '$poNo' AND
trn_sampleorderdetails.intSampleOrderYear =  '$year'";
			$result1 = $db->RunQuery($sql1);
			while($row=mysqli_fetch_array($result1))
			{
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfntMid" >&nbsp;<?php echo $row['strCombo'] ?>&nbsp;</td>
              <td class="normalfntMid" >&nbsp;<?php echo $row['strName']?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['intGrade'] ?>&nbsp;</td>
              <td class="normalfntMid" ><?php echo $row['sampleType'] ?></td>
              <td class="normalfntMid"  >&nbsp;<?php echo $row['strSize'] ?>&nbsp;</td>
              <td >&nbsp;<?php echo $row['dtRequiredDate'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['intQty'] ?>&nbsp;</td>
              </tr>
            <?php        
			}
	  ?>
            
            </table>
        </td>
        </tr>
      
      </table>
    </td>
</tr>

<tr>
                <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="44%" class="normalfnt" valign="top">
                   <?php
					$creator		= $header_array['CREATOR'];
					$createdDate	= $header_array['CREATED_DATE'];
					$resultA 		= getRptApproveDetails($poNo,$year);
					include "presentation/report_approvedBy_details.php"
					?>
                    </td>
                    <td width="56%" style="text-align:right" valign="top" class="normalfntMid">                    Deliver To :
                    <textarea name="textarea" cols="45" rows="5" disabled="disabled" class="tableBorder_allRound" id="textarea"><?php echo $attentionTo; ?></textarea></td>
                  </tr>
                  
        </table></td>
            </tr>

<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>

<?php
function getRptApproveDetails($poNo,$year)
{
	global $db;
	
	$sql = "SELECT
			SRAB.intApproveUser AS APPROVED_BY,
			SRAB.dtApprovedDate AS dtApprovedDate,
			SU.strUserName AS UserName,
			SRAB.intApproveLevelNo AS intApproveLevelNo
			FROM
			trn_sampleorderheader_approvedby SRAB
			INNER JOIN sys_users SU ON SRAB.intApproveUser = SU.intUserId
			WHERE SRAB.intSampleOrderNo ='$poNo' AND
			SRAB.intSampleOrderYear ='$year'      
			ORDER BY SRAB.dtApprovedDate ASC ";
	
	$result = $db->RunQuery($sql);
	return $result;
}
?>
<?php 
session_start();
$backwardseperator 		= "../../../../../";
$mainPath 				= $_SESSION['mainPath'];
$userId 				= $_SESSION['userId'];
$companyId 				= $_SESSION['CompanyID'];
$SRdb					= $_SESSION["SRDatabese"];
$Maindb					= $_SESSION["Database"];

$requestType 			= $_REQUEST['requestType'];
$programName			= 'Sample Place Order';
$programCode			= 'P0354';

include_once "{$backwardseperator}dataAccess/Connector.php";
include_once "../../../../../class/cls_mail.php";
include_once "../../../../../class/cls_commonFunctions_get.php";
include_once "../../../../../class/cls_commonErrorHandeling_get.php";

$obj_common				= new cls_commonFunctions_get($db);
$obj_errorHandeling 	= new cls_commonErrorHandeling_get($db);
$objMail 				= new cls_create_mail($db);

$savedStatus			= true;
$finalApprove			= false;
$savedMasseged 			= '';
$error_sql				= '';


if($requestType=='approve')
{
    $sampleOrderNo		= $_REQUEST["orderNo"];
	$sampleOrderYear	= $_REQUEST["year"];
	
	$db->begin();
	
	$validateArr		= validateBeforeApprove($sampleOrderNo,$sampleOrderYear,$programCode,$userId);
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateArr["msg"];
	}
	
	$resultUHSArr		= updateHeaderStatus($sampleOrderNo,$sampleOrderYear,'');
	if(!empty($resultUHSArr) && $resultUHSArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultUHSArr["savedMassege"];
		$error_sql		= $resultUHSArr['error_sql'];	
	}
	$chekQtyArr			= checkQtyBeforeFinalApproval($sampleOrderNo,$sampleOrderYear);
	if(!empty($chekQtyArr) && $chekQtyArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $chekQtyArr["msg"];
	}
	$qtyUpdArr			= sampleRequisitionBalQtyUpdate($sampleOrderNo,$sampleOrderYear,'approve');
	if($qtyUpdArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $qtyUpdArr["savedMassege"];
		$error_sql		= $qtyUpdArr['error_sql'];
	}
	$qtyUpdArr			= sampleRequisitionQtyUpdate($sampleOrderNo,$sampleOrderYear,'approve');
	if($qtyUpdArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $qtyUpdArr["savedMassege"];
		$error_sql		= $qtyUpdArr['error_sql'];
	}	
	$resultAPArr		= approvedData($sampleOrderNo,$sampleOrderYear,$userId);
	if(!empty($resultAPArr) && $resultAPArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultAPArr["savedMassege"];
		$error_sql		= $resultAPArr['error_sql'];	
	}
	
	
	//save in log table
	$header_arr		= loadHeaderData($sampleOrderNo,$sampleOrderYear,'RunQuery2');
	if(($header_arr['intStatus']==1) && ($header_arr['intSampleReqNo']!='') && ($header_arr['intSampleReqYear']!=''))
	{
		$resultLogArr	= saveLogQueries($sampleOrderNo,$sampleOrderYear);
		if($resultLogArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultLogArr["savedMassege"];
			$error_sql		= $resultLogArr['error_sql'];	
		}
	}


	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		if(isset($finalApproval))
			$response['msg'] 	= "Final Approval Raised Successfully.";
		else
			$response['msg'] 	= "Approved Successfully.";
		
		sendSampleOrderMail($sampleOrderNo,$sampleOrderYear,$mainPath,$root_path);
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql']		= $error_sql;
	}
	echo json_encode($response);
}
else if($requestType=='reject')
{
	$sampleOrderNo		= $_REQUEST["orderNo"];
	$sampleOrderYear	= $_REQUEST["year"];
	
	$db->begin();
	
	$validateArr		= validateBeforeReject($sampleOrderNo,$sampleOrderYear,$programCode,$userId);
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateArr["msg"];
	}
	
	$resultUHSArr		= updateHeaderStatus($sampleOrderNo,$sampleOrderYear,'0');
	if(!empty($resultUHSArr) && $resultUHSArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultUHSArr["savedMassege"];
		$error_sql		= $resultUHSArr['error_sql'];	
	}
	
	$resultAPArr		= approved_by_insert($sampleOrderNo,$sampleOrderYear,$userId,0);
	if($resultAPArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultAPArr["savedMassege"];
		$error_sql		= $resultAPArr['error_sql'];	
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Rejected Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql']		= $error_sql;
	}
	echo json_encode($response);
}
else if($requestType=='cancel')
{
	$sampleOrderNo		= $_REQUEST["orderNo"];
	$sampleOrderYear	= $_REQUEST["year"];
	
	$db->begin();
	
	$validateArr		= validateBeforeCancel($sampleOrderNo,$sampleOrderYear,$programCode,$userId);
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateArr["msg"];
	}
	$qtyUpdArr			= sampleRequisitionBalQtyUpdate($sampleOrderNo,$sampleOrderYear,'cancel');
	if($qtyUpdArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $qtyUpdArr["savedMassege"];
		$error_sql		= $qtyUpdArr['error_sql'];
	}
	$qtyUpdArr			= sampleRequisitionQtyUpdate($sampleOrderNo,$sampleOrderYear,'cancel');
	if($qtyUpdArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $qtyUpdArr["savedMassege"];
		$error_sql		= $qtyUpdArr['error_sql'];
	}
	$resultUHSArr		= updateHeaderStatus($sampleOrderNo,$sampleOrderYear,'-2');
	if(!empty($resultUHSArr) && $resultUHSArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultUHSArr["savedMassege"];
		$error_sql		= $resultUHSArr['error_sql'];	
	}
	$resultAPArr		= approved_by_insert($sampleOrderNo,$sampleOrderYear,$userId,'-2');
	if($resultAPArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultAPArr["savedMassege"];
		$error_sql		= $resultAPArr['error_sql'];	
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Cancelled Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql']		= $error_sql;
	}
	echo json_encode($response);
}
function validateBeforeApprove($sampleOrderNo,$sampleOrderYear,$programCode,$userId)
{
	global $db;
	global $obj_errorHandeling;
	
	$header_arr			= loadHeaderData($sampleOrderNo,$sampleOrderYear,'RunQuery2');
	$validateArr 		= $obj_errorHandeling->get_permision_withApproval_confirm($header_arr['intStatus'],$header_arr['APPROVE_LEVELS'],$userId,$programCode,'RunQuery2');
	
	return  $validateArr;
}
function loadHeaderData($sampleOrderNo,$sampleOrderYear,$executionType)
{
	global $db;
	
	$sql = "SELECT 	intSampleNo, 
			intSampleYear, 
			intSampleReqNo, 
			intSampleReqYear, 
			intRevNo, 
			dtDate, 
			strAttentionTo, 
			intCustomerLocation, 
			strRemarks, 
			intStatus, 
			APPROVE_LEVELS, 
			intLocationId
			FROM trn_sampleorderheader
			WHERE intSampleOrderNo = '$sampleOrderNo' AND
			intSampleOrderYear = '$sampleOrderYear' ";
	
	$result = $db->$executionType($sql);
	$row	= mysqli_fetch_array($result);
	
	return $row;
}
function updateHeaderStatus($sampleOrderNo,$sampleOrderYear,$status)
{
	global $db;
	
	if($status=='') 
		$para 	= 'intStatus-1';
	else
		$para 	= $status;
		
	$sql = "UPDATE trn_sampleorderheader 
			SET
			intStatus = $para
			WHERE
			intSampleOrderNo = '$sampleOrderNo' AND 
			intSampleOrderYear = '$sampleOrderYear' ";
	
	$result 		= $db->RunQuery2($sql);
	$data = array();
	if(!$result)
	{
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= $db->errormsg;
		$data['error_sql']		= $sql;
	}
	return $data;
}
function approvedData($sampleOrderNo,$sampleOrderYear,$userId)
{
	global $db;
	global $finalApprove;
	
	$header_arr 	= loadHeaderData($sampleOrderNo,$sampleOrderYear,'RunQuery2');
	
	if($header_arr['intStatus']==1)
		$finalApprove = true;
	
	$approval		= $header_arr['APPROVE_LEVELS']+1-$header_arr['intStatus'];
	$resultArr		= approved_by_insert($sampleOrderNo,$sampleOrderYear,$userId,$approval);
	
	return $resultArr;
}
function approved_by_insert($sampleOrderNo,$sampleOrderYear,$userId,$approval)
{
	global $db;
	
	$sql = "INSERT INTO trn_sampleorderheader_approvedby 
				(
				intSampleOrderNo, 
				intSampleOrderYear, 
				intApproveLevelNo, 
				intApproveUser, 
				dtApprovedDate, 
				STATUS
				)
				VALUES
				(
				'$sampleOrderNo', 
				'$sampleOrderYear', 
				'$approval', 
				'$userId', 
				NOW(), 
				'0'
				) ";
		
		$result 		= $db->RunQuery2($sql);
		$data = array();
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
}
function validateBeforeReject($sampleOrderNo,$sampleOrderYear,$programCode,$userId)
{
	global $obj_errorHandeling;
	
	$header_arr			= loadHeaderData($sampleOrderNo,$sampleOrderYear,'RunQuery2');
	$validateArr 		= $obj_errorHandeling->get_permision_withApproval_reject($header_arr['intStatus'],$header_arr['APPROVE_LEVELS'],$userId,$programCode,'RunQuery2');
	
	return  $validateArr;
}
function sendSampleOrderMail($sampleOrderNo,$sampleOrderYear,$mainPath,$root_path)
{
	global $db;
	global $companyId;
	global $userId;
	global $objMail;
	
	$header_arr			= loadHeaderData($sampleOrderNo,$sampleOrderYear,'RunQuery');
	$intSampleNo 		= $header_arr['intSampleNo'];
	$intSampleYear 		= $header_arr['intSampleYear'];
	$revNo 				= $header_arr['intRevNo'];
	$status				= $header_arr['intStatus'];
	
	$sql = "SELECT
			trn_sampleinfomations.intStatus
			FROM trn_sampleinfomations
			WHERE
			trn_sampleinfomations.intSampleNo = '$intSampleNo' AND
			trn_sampleinfomations.intSampleYear = '$intSampleYear' AND
			trn_sampleinfomations.intRevisionNo = '$revNo' ";
				
	$result 		= $db->RunQuery($sql);
	$row 			= mysqli_fetch_array($result);
	$sampleStatus 	= $row['intStatus'];
	
	if($status==1 && $sampleStatus==1)
	{
		$sql1 = "SELECT
					sys_users.strUserName,
					sys_users.strEmail,
					(select sum(trn_sampleorderdetails.intQty)as intQty from trn_sampleorderdetails 
					where trn_sampleorderdetails.intSampleOrderNo =trn_sampleorderheader.intSampleOrderNo 
					and trn_sampleorderdetails.intSampleOrderYear=trn_sampleorderheader.intSampleOrderYear  ) as qty
					FROM
					trn_sampleorderheader
					Inner Join sys_users ON sys_users.intUserId = trn_sampleorderheader.intCreator
					WHERE
					trn_sampleorderheader.intSampleOrderNo =  '$sampleOrderNo' AND
					trn_sampleorderheader.intSampleOrderYear =  '$sampleOrderYear' ";

		$result1 	= $db->RunQuery($sql1);
		$row1 		= mysqli_fetch_array($result1);
		
		$enterUserName 	= $row1['strUserName'];
		$enterUserEmail = $row1['strEmail'];
		$sampleQty 		= $row1['qty'];
		
		$sql2 = "SELECT DISTINCT
					sys_users.strUserName,
					sys_mail_eventusers.intUserId,
					sys_users.strEmail
					FROM
					sys_mail_eventusers
					Inner Join sys_users ON sys_users.intUserId = sys_mail_eventusers.intUserId
					WHERE
					sys_mail_eventusers.intMailEventId =  '1004' AND
					sys_mail_eventusers.intCompanyId =  '$companyId' ";	
		
		$result2 	= $db->RunQuery($sql2);
		while($row2 = mysqli_fetch_array($result2))
		{
			if($row2['intUserId']!=$userId)
			{
				$header = "NEW SAMPLE FOR SAMPLE ROOM ($intSampleNo/$intSampleYear)";
				
				$_REQUEST 				= NULL;
				$_REQUEST['no'] 		= $intSampleNo;
				$_REQUEST['year']  		= $intSampleYear;
				$_REQUEST['revNo']   	= $revNo;
				$_REQUEST['orderNo']   	= $sampleOrderNo;
				$_REQUEST['orderYear']  = $sampleOrderYear;
				$_REQUEST['sampleQty']  = $sampleQty;
				$_REQUEST['fromName']  	= $enterUserName;
				$_REQUEST['toName']  	= $row2['strUserName'];
				$_REQUEST['mainPath']  	= $mainPath;
				 
				$path = $root_path."presentation/customerAndOperation/sample/sampleOrder/listing/tosampleorderemail.php";
				$objMail->send_Response_Mail($path,$_REQUEST,$enterUserName,$enterUserEmail,$header,$row2['strEmail'],$row2['strUserName']);
			}
		}	
	}
}	
function checkQtyBeforeFinalApproval($sampleOrderNo,$sampleOrderYear)
{
	global $db;
	global $SRdb;
	$checkQty		= false;
	$data = array();
	
	$header_arr		= loadHeaderData($sampleOrderNo,$sampleOrderYear,'RunQuery2');
	if($header_arr['intStatus']==1 && ($header_arr['intSampleReqNo']!='' && $header_arr['intSampleReqYear']!=''))
	{
	
		$sql = "SELECT STATUS 
				FROM $SRdb.trn_sample_requisition_header 
				WHERE REQUISITION_NO = '".$header_arr['intSampleReqNo']."' AND
				REQUISITION_YEAR = '".$header_arr['intSampleReqYear']."' ";
		
		$result = $db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		if($row['STATUS']!=1)
		{
			$data['type'] 	= 'fail';
			$data['msg'] 	= 'Can not Approved. Sample Requisition cancelled ';
		}
		else
		{
			$sql = "SELECT intSampleTypeId,intQty
					FROM trn_sampleorderdetails
					WHERE intSampleOrderNo='$sampleOrderNo' AND
					intSampleOrderYear='$sampleOrderYear' ";
			
			$result = $db->RunQuery2($sql);
			$data = array();
			while($row=mysqli_fetch_array($result))
			{
				$sql2 = "SELECT BAL_QTY
							FROM $SRdb.trn_sample_requisition_detail
							WHERE REQUISITION_NO = '".$header_arr['intSampleReqNo']."' AND
							REQUISITION_YEAR = '".$header_arr['intSampleReqYear']."' AND
							SAMPLE_TYPE = '".$row['intSampleTypeId']."' ";

				$result2 = $db->RunQuery2($sql2);
				$row2	 = mysqli_fetch_array($result2);
				
				if($row['intQty']>$row2['BAL_QTY'])
				{
					$checkQty	= true;
				}
			}
			if($checkQty)
			{
				$data['type'] 	= 'fail';
				$data['msg'] 	= 'Some Qty exceed sample requisition bal qty. ';
			}
		}
	}
	return $data;	
}
function sampleRequisitionBalQtyUpdate($sampleOrderNo,$sampleOrderYear,$mode)
{
	global $db;
	global $SRdb;
	
	$header_arr		= loadHeaderData($sampleOrderNo,$sampleOrderYear,'RunQuery2');
	if($header_arr['intStatus']==1)
	{
		if($header_arr['intSampleReqNo']!='' && $header_arr['intSampleReqYear']!='')
		{
			$sql = "SELECT intSampleTypeId,intQty,dtRequiredDate
					FROM trn_sampleorderdetails
					WHERE intSampleOrderNo='$sampleOrderNo' AND
					intSampleOrderYear='$sampleOrderYear' ";
			
			$result = $db->RunQuery2($sql);
			while($row=mysqli_fetch_array($result))
			{
				$qty 			= $row['intQty'];
				//$requiredDate	= $row['dtRequiredDate'];
				if($mode=='approve')
					$updatePart = "BAL_QTY = BAL_QTY - $qty ";
				else
					$updatePart = "BAL_QTY = BAL_QTY + $qty ";
				
				$sql2 = "UPDATE $SRdb.trn_sample_requisition_detail 
						SET 
						$updatePart 
						WHERE
						REQUISITION_NO = '".$header_arr['intSampleReqNo']."' AND  
						REQUISITION_YEAR = '".$header_arr['intSampleReqYear']."' AND 
						SAMPLE_TYPE = '".$row['intSampleTypeId']."' ";
				
				$result2 		= $db->RunQuery2($sql2);
				if(!$result2)
				{
					$data['savedStatus']	= 'fail';
					$data['savedMassege']	= $db->errormsg;
					$data['error_sql']		= $sql2;
				}
				// save in queries_sample
				$result_ipad 	= $db->RunQuery_S($sql2);
				if(!$result_ipad){
					$data['savedStatus']	= 'fail';
					$data['savedMassege']	= $db->errormsg;
					$data['error_sql']		= $sql2;
				}
			}
 			return $data;
		}
	}
}
function sampleRequisitionQtyUpdate($sampleOrderNo,$sampleOrderYear,$mode)
{
	global $db;
	global $SRdb;
	
	$header_arr		= loadHeaderData($sampleOrderNo,$sampleOrderYear,'RunQuery2');
	if($header_arr['intStatus']==1)
	{
		if($header_arr['intSampleReqNo']!='' && $header_arr['intSampleReqYear']!='')
		{
			$sql = "SELECT intSampleTypeId,intQty,dtRequiredDate
					FROM trn_sampleorderdetails
					WHERE intSampleOrderNo='$sampleOrderNo' AND
					intSampleOrderYear='$sampleOrderYear' ";
			
			$result = $db->RunQuery2($sql);
			while($row=mysqli_fetch_array($result))
			{
				$qty = $row['intQty'];
				
				if($mode=='approve')
					$updatePart = "SAMPLE_ORDER_QTY = SAMPLE_ORDER_QTY + $qty ";
				else
					$updatePart = "SAMPLE_ORDER_QTY = SAMPLE_ORDER_QTY - $qty ";
				
				$sql2 = "UPDATE $SRdb.trn_sample_requisition_detail 
						SET 
						$updatePart 
						WHERE
						REQUISITION_NO = '".$header_arr['intSampleReqNo']."' AND  
						REQUISITION_YEAR = '".$header_arr['intSampleReqYear']."' AND 
						SAMPLE_TYPE = '".$row['intSampleTypeId']."' ";

				$result2 		= $db->RunQuery2($sql2);
				if(!$result2)
				{
					$data['savedStatus']	= 'fail';
					$data['savedMassege']	= $db->errormsg;
					$data['error_sql']		= $sql2;
				}
				
				$result_ipad 	= $db->RunQuery_S($sql2);
				if(!$result_ipad)
				{
					$data['savedStatus']	= 'fail';
					$data['savedMassege']	= $db->errormsg;
					$data['error_sql']		= $sql2;
				}
			}
			return $data;
		}
	}
}
function validateBeforeCancel($sampleOrderNo,$sampleOrderYear,$programCode,$userId)
{
	global $db;
	global $obj_errorHandeling;
	$checkStatus = false;
	
	$header_arr			= loadHeaderData($sampleOrderNo,$sampleOrderYear,'RunQuery2');
	$validateArr 		= $obj_errorHandeling->get_permision_withApproval_cancel($header_arr['intStatus'],$header_arr['APPROVE_LEVELS'],$userId,$programCode,'RunQuery2');
	if($validateArr['type']=='pass')
	{
		if($header_arr['intSampleReqNo']!='' && $header_arr['intSampleReqYear']!='')
		{
			$sql = "SELECT strCombo, 
					intPart, 
					intGrade, 
					strSize, 
					intQty, 
					intBalToDeliverQty, 
					intBalToFabricInQty, 
					intSampleTypeId, 
					dtRequiredDate,
					SFIH.SAMPLE_FABRICIN_NO,
					SFIH.SAMPLE_FABRICIN_YEAR
					FROM trn_sampleorderdetails SOD
					LEFT JOIN trn_sampleorder_fabricin_header SFIH ON SFIH.SAMPLE_ORDER_NO=SOD.intSampleOrderNo AND
					SFIH.SAMPLE_ORDER_YEAR=SOD.intSampleOrderYear
					WHERE SOD.intSampleOrderNo='$sampleOrderNo' AND
					SOD.intSampleOrderYear='$sampleOrderYear' AND
					SFIH.STATUS = 1 ";
			
			$result = $db->RunQuery2($sql);
			while($row=mysqli_fetch_array($result))
			{
				$sql2 = "SELECT SAMPLE_FABRICIN_NO ,
							SAMPLE_FABRICIN_YEAR 
							FROM trn_sampleorder_fabricin_details	
							WHERE
							SAMPLE_FABRICIN_NO = '".$row['SAMPLE_FABRICIN_NO']."' AND 
							SAMPLE_FABRICIN_YEAR = '".$row['SAMPLE_FABRICIN_YEAR']."' AND 
							COMBO = '".$row['strCombo']."' AND 
							PART = '".$row['intPart']."' AND 
							GRADE = '".$row['intGrade']."' AND 
							SIZE = '".$row['strSize']."' AND 
							SAMPLETYPE_ID = '".$row['intSampleTypeId']."' AND 
							REQUIRED_DATE = '".$row['dtRequiredDate']."' ";
				
				$result2 = $db->RunQuery2($sql2);
				$count	= mysqli_num_rows($result2);
				
				if($count>0)
				{
					$checkStatus	= true;
				}	
			}
			if($checkStatus)
			{
				$validateArr['type'] 	= 'fail';
				$validateArr['msg'] 	= 'Can not Cancelled. Sample Fabric In Raised ';
			}
		}
	}
	return $validateArr;
}

function saveLogQueries($orderNo,$orderYear){
	global $db;
	global $SRdb;
	global $Maindb;
	
		$savedStatus	= "pass";
	
		$sql_types		= loadOrderDetails_sql($orderNo,$orderYear);
		$result_types 	= $db->RunQuery2($sql_types);
		while($row_types=mysqli_fetch_array($result_types))
		{
			$graphc			= $row_types['GRAPHIC'];
			$samp_type		= $row_types['SAMPLE_TYPE'];
			$trns_type		= $row_types['transacType'];
			$created_by	 	= $row_types['createdBy'];
			$modified_by 	= "'".$row_types['modifyBy']."'";
			if($row_types['modifyBy']==''){
			$modified_by 	= 'NULL';
			}
			$log_date	 	= $row_types['transacDate'];
			
			  $sql_log = "INSERT INTO $SRdb.dashboard_event_log 
						(
						GRAPHIC_NO, 
						SAMPLE_TYPE_ID, 
						TYPE, 
						CREATED_BY, 
						MODIFY_BY, 
						LOG_DATE_TIME
						)
						VALUES
						(
						'$graphc', 
						'$samp_type', 
						'$trns_type', 
						'$created_by', 
						$modified_by, 
						'$log_date') ";
			
			$result		 	= $db->RunQuery2($sql_log);
			if(!$result){
					$savedStatus			= 'fail';
					$msg					= $db->errormsg;
					$error_sql				= $sql_log;
			}
			$result_ipad 	= $db->RunQuery_S($sql_log);// save in queries_sample
			if(!$result_ipad){
					$savedStatus			= 'fail';
					$msg					= $db->errormsg;
					$error_sql				= $sql_log;
			}
		}
		
		$data['savedStatus']	= $savedStatus;
		$data['savedMassege']	= $msg;
		$data['error_sql']		= $error_sql;
 			
 	return $data;
		
}

function loadOrderDetails_sql($orderNo,$orderYear){
	
	global $SRdb;
	global $Maindb;
	
	$sql	="
			SELECT SRH.GRAPHIC,
			SRD.SAMPLE_TYPE,
			SOH.dtmCreateDate AS transacDate,
			SRH.CREATED_BY AS createdBy,
			SRH.MODIFY_BY AS modifyBy,
			SOD.intQty AS Qty,
			'SAMPLEORDER' AS transacType
			FROM 
			$Maindb.trn_sampleorderheader SOH
			INNER JOIN $Maindb.trn_sampleorderdetails SOD ON SOH.intSampleOrderNo=SOD.intSampleOrderNo AND
			SOH.intSampleOrderYear=SOD.intSampleOrderYear
			INNER JOIN $SRdb.trn_sample_requisition_header SRH ON SRH.REQUISITION_NO=SOH.intSampleReqNo AND
			SRH.REQUISITION_YEAR=SOH.intSampleReqYear
			INNER JOIN $SRdb.trn_sample_requisition_detail SRD ON SRH.REQUISITION_NO=SRD.REQUISITION_NO AND
			SRH.REQUISITION_YEAR=SRD.REQUISITION_YEAR AND
			SOD.intSampleTypeId=SRD.SAMPLE_TYPE
			WHERE
			SOH.intStatus 			= 1 AND
			SOH.intSampleOrderNo 	= '$orderNo' AND
			SOH.intSampleOrderYear 	= '$orderYear' AND
			SRD.SAMPLE_ORDER_QTY > 0 AND
			SRD.QTY > 0
			GROUP BY SRD.SAMPLE_TYPE,SOH.dtmCreateDate
			ORDER BY SRD.SAMPLE_TYPE
			LIMIT 0,2	";
	return $sql;	
}
?>
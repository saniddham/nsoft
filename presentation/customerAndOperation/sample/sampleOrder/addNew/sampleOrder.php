<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$SRdb					= $_SESSION["SRDatabese"];
$locationId				= $_SESSION["CompanyID"];
$userId					= $_SESSION["userId"];

include_once "class/cls_commonFunctions_get.php";
include_once "class/cls_commonErrorHandeling_get.php";

$obj_commonErrHandle	= new cls_commonErrorHandeling_get($db);
$obj_common				= new cls_commonFunctions_get($db);

$programCode			= 'P0354';	
$orderNo 				= $_REQUEST['orderNo'];//load from sample place order listing page
$orderYear 				= $_REQUEST['orderYear'];

$reqNo 					= $_REQUEST['reqNo'];//load from sample place order listing page
$reqYear 				= $_REQUEST['reqYear'];



$year = $_REQUEST['sampleYear'];
if($year==''){
	$year=date('Y');
}

 $sql="		SELECT
				trn_sampleorderheader.intSampleNo,
				trn_sampleorderheader.intSampleYear,
				trn_sampleorderheader.intRevNo,
				trn_sampleorderheader.intSampleReqNo,
				trn_sampleorderheader.intSampleReqYear,
				trn_sampleorderheader.strAttentionTo,
				trn_sampleorderheader.intCustomerLocation,
				trn_sampleorderheader.strRemarks,
				trn_sampleorderheader.`SAMPLE CATEGORY`,
				dtDate,
				intStatus,
				APPROVE_LEVELS
			FROM
				trn_sampleorderheader
			WHERE
				trn_sampleorderheader.intSampleOrderNo =  '$orderNo' AND
				trn_sampleorderheader.intSampleOrderYear =  '$orderYear'
";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);

$sampleID 			= $row['intSampleNo'];
$year 				= $row['intSampleYear'];
$intStatus			= $row['intStatus'];
$levels				= $row['APPROVE_LEVELS'];
$revNo				= $row['intRevNo'];
$attentionTo 		= $row['strAttentionTo'];
$custLocation 		= $row['intCustomerLocation'];
$remarks 			= $row['strRemarks'];
$dtDate 			= $row['dtDate'];
$SAMPLE_CATEGORY	= $row['SAMPLE CATEGORY'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_save($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_save			= $permition_arr['permision'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_confirm		= $permition_arr['permision'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_cancel($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_cancel		= $permition_arr['permision'];

$newRecord = false;	
if($orderNo=='')
{//load from sample information page
	
	if($reqNo!='')
	{
		$sql = "SELECT intSampleNo,intSampleYear
				FROM trn_sampleinfomations
				WHERE intRequisitionNo = '$reqNo' AND
				intRequisitionYear = '$reqYear' ";
		
		$result = $db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		
		$sampleID 	= $row['intSampleNo'];
		$year 		= $row['intSampleYear'];
	}
	else
	{
		$sampleID 	= $_REQUEST['sampleNo'];
		$year 		= $_REQUEST['year'];
	}
	
	$intStatus = 10;
	$newRecord = true;	
	
	$sql = "SELECT
				Max(trn_sampleinfomations.intRevisionNo) AS revNo
			FROM
				trn_sampleinfomations
			WHERE
				trn_sampleinfomations.intSampleNo =  '$sampleID' AND
				trn_sampleinfomations.intSampleYear =  '$year'
			";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	$revNo = $row['revNo'];
}


/*$sampleID = '100229';
$year = '2012';
*/
		$sql = "SELECT
				mst_customer.strName,
				mst_customer.intId , 
				trn_sampleinfomations.strGraphicRefNo,
				trn_sampleinfomations.strStyleNo,
				mst_brand.strName as brand,
				trn_sampleinfomations.intRequisitionNo,
				trn_sampleinfomations.intRequisitionYear,
				CONCAT(trn_sampleinfomations.intRequisitionNo,'/',trn_sampleinfomations.intRequisitionYear) conReqNo
				FROM
				trn_sampleinfomations
				Inner Join mst_customer ON mst_customer.intId = trn_sampleinfomations.intCustomer
				Inner Join mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
				WHERE
				trn_sampleinfomations.intSampleNo =  '$sampleID' AND
				trn_sampleinfomations.intSampleYear =  '$year' AND
				trn_sampleinfomations.intRevisionNo =  '$revNo'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$customerId 		= $row['intId'];
			$customer 			= $row['strName'];
			$graphic 			= $row['strGraphicRefNo'];
			$style 				= $row['strStyleNo'];
			$brand 				= $row['brand'];
			$requisitionNo 		= $row['intRequisitionNo'];
			$requisitionYear 	= $row['intRequisitionYear'];
			$conReqNo 			= $row['conReqNo'];
		}

?>
<head>
<title>Sample Order</title>
</head>
<body >
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<form id="frmSampleOrder" name="frmSampleOrder" >
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">SAMPLE ORDER</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" class="">
          <tr>
            <td width="14%" class="normalfnt">Sample  Order No</td>
            <td width="36%"><input name="txtSampOrderNo" type="text" disabled="disabled" id="txtSampOrderNo" style="width:80px" value="<?php echo $orderNo ?>" />&nbsp;<input name="txtSampYear" type="text" disabled="disabled" id="txtSampYear" style="width:50px" value="<?php echo $orderYear ?>" /></td>
            <td width="3%">&nbsp;</td>
            <td width="13%" class="normalfnt">Date</td>
            <td width="34%"><input name="dtDate" type="text" value="<?php echo ($dtDate==''?date("Y-m-d"):$dtDate); ?>" class="txtbox" id="dtDate" style="width:100px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
          <tr>
            <td class="normalfnt">Sample ID <span class="compulsoryRed">*</span></td>
            <td class="normalfnt"><select class="validate[required]" name="cboSampleNo" id="cboSampleNo" style="width:80px" <?php echo($newRecord?'':' disabled="disabled" '); ?>>
                  <option value=""></option>
                  <?php
					$d = date('Y');
					if($year=='')
						$year = $d;
				    $sql = "SELECT DISTINCT
							trn_sampleinfomations.intSampleNo
							FROM trn_sampleinfomations
						WHERE
							trn_sampleinfomations.intSampleYear =  '$year' and intMarketingStatus=1 
						ORDER BY
							trn_sampleinfomations.intSampleNo DESC
						
					";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$no = $row['intSampleNo'];
						if($no==$sampleID)
						echo "<option value=\"$no\" selected=\"selected\">$no</option>";
						else
						echo "<option value=\"$no\">$no</option>";
					}
				?>
                    </select>
              <select name="cboYear" class="validate[required]" id="cboYear" style="width:65px" <?php echo($newRecord?'':' disabled="disabled" '); ?>>
                <?php
				    $d = date('Y');
					
				  	for($d;$d>=2012;$d--)
					{
						if($year==$d){
							echo "<option selected=\"selected\" id=\"$d\" >$d</option>";	
						}
						else{
							echo "<option id=\"$d\" >$d</option>";	
						}
					}
				  ?>
              </select>
                    Rev No (<?php echo $revNo; ?>)</td>
            <td>&nbsp;</td>
            <td class="normalfnt">Customer</td>
            <td class="normalfnt"><input id="dtxtCustomer" name="dtxtCustomer" value="<?php echo $customer; ?>" style="width:300px" disabled="disabled"  />
                <div style="visibility: hidden;" id="customer"><?php echo $customerId; ?></div>
            </td>
          </tr>
          <tr>
            <td height="22" class="normalfnt">Requisition No</td>
            <td class="normalfnt"><input id="txtRequisitionNo" name="txtRequisitionNo" value="<?php echo $conReqNo; ?>" style="width:150px" disabled="disabled"  /></td>
            <td>&nbsp;</td>
            <td class="normalfnt">Location <span class="compulsoryRed">*</span></td>
            <td class="normalfnt"><select name="cboLocation" id="cboLocation" class="validate[required]"  style="width:150px">
                <?php
					$sql = "SELECT
								mst_customer_locations.intLocationId,
								mst_customer_locations_header.strName
							FROM
								mst_customer_locations
								Inner Join mst_customer_locations_header ON mst_customer_locations.intLocationId = mst_customer_locations_header.intId
							WHERE
								mst_customer_locations.intCustomerId =  '$customerId' order by strName";
					$result = $db->RunQuery($sql);
					echo  "<option value=\"\"></option>";
					while($row=mysqli_fetch_array($result))
					{
						if($row['intLocationId']==$custLocation)
							echo "<option  selected=\"selected\" value=\"".$row['intLocationId']."\">".$row['strName']."</option>";
						else
							echo "<option value=\"".$row['intLocationId']."\">".$row['strName']."</option>";
					}	
			?>
              </select></td>
          </tr>
          <tr>
            <td class="normalfnt">Brand</td>
            <td class="normalfnt"><input id="txtBrand" name="txtBrand" value="<?php echo $brand ?>" style="width:150px" disabled="disabled"  /></td>
            <td>&nbsp;</td>
            <td class="normalfnt">Style No</td>
            <td class="normalfnt"><input id="txtStyle" name="txtStyle" value="<?php echo $style ?>" style="width:150px"  disabled="disabled" /></td>
          </tr>
          <tr>
            <td class="normalfnt">Graphic No</td>
            <td class="normalfnt"><input id="txtGraphic" name="txtGraphic" value="<?php echo $graphic ?>" style="width:150px" disabled="disabled"  /></td>
            <td>&nbsp;</td>
            <td class="normalfnt">Sample Category</td>
            <td><select name="cboCategory" id="cboCategory" class="validate[required]"  style="width:150px">
                <?php
					$sql = "SELECT
						mst_sample_category.ID AS categoryID,
						mst_sample_category.`NAME` AS category_name
					FROM
						mst_sample_category
					WHERE
						1 = 1 ";
					if($SAMPLE_CATEGORY != ''){
						$sql .= "AND mst_sample_category.ID = '$SAMPLE_CATEGORY'";
						}
					else{
						$sql .= "AND mst_sample_category.`STATUS` = 1";
					}
					$result = $db->RunQuery($sql);
					echo  "<option value=\"\"></option>";
					while($row=mysqli_fetch_array($result))
					{
						if($row['categoryID']==$SAMPLE_CATEGORY)
							echo "<option  selected=\"selected\" value=\"".$row['categoryID']."\">".$row['category_name']."</option>";
						else
							echo "<option value=\"".$row['categoryID']."\">".$row['category_name']."</option>";
					}	
			?>
              </select></td>
          </tr>
          <tr>
            <td class="normalfnt" valign="top">Remarks</td>
            <td valign="top" class="normalfnt"><textarea name="txtNote" id="txtNote" rows="3" style="width:100%"><?php echo $remarks; ?></textarea></td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td rowspan="2" valign="top"><div id="divPicture" class="tableBorder_allRound divPicture" align="center"  style="width:200px;height:110px;overflow:hidden" >
              <?php
			if($sampleID!='')
			{
				echo "<a target=\"_blank\" href=\"documents/sampleinfo/samplePictures/$sampleID-$year-$revNo-1.png\"><img id=\"saveimg\" style=\"width:200px;height:110px;\" src=\"documents/sampleinfo/samplePictures/$sampleID-$year-$revNo-1.png\" /></a>";	
			}
			 ?>
            </div></td>
          </tr>
          <tr>
            <td class="normalfnt" valign="top"><span class="normalfntMid">Attention To </span></td>
            <td class="normalfnt" valign="top"><textarea style="width:100%" rows="3" class="validate[maxSize[200]]" id="txtAttentionTo" name="txtAttentionTo"><?php echo $attentionTo; ?></textarea></td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:200px;overflow:scroll" >
          <table class="bordered" id="tblSampOrders" style="width:1000px" >
            <tr>
              <th width="28" height="22" >Del</th>
              <th width="123" >Combo <span class="compulsoryRed">*</span></th>
              <th width="173" >Part <span class="compulsoryRed">*</span></th>
              <th width="123" >Sample Type <span class="compulsoryRed">*</span></th>
              <th width="65" >Grade <span class="compulsoryRed">*</span></th>
              <th width="63">Size</th>
              <th width="78">Qty <span class="compulsoryRed">*</span></th>
              <th width="122">Required Date</th>
               <th width="122">FabricIn Date</th>
              <th width="85">Time (HH:MM)</th>
              </tr>
            <?php 
			  $haveRecord = false;
			  $x	= 0;
			  $y	= 0;
			  if($orderNo!=''){
				  $sqlExist="SELECT
							trn_sampleorderdetails.intPart,
							mst_part.strName,
							trn_sampleorderdetails.strCombo,
							trn_sampleorderdetails.intGrade,
							trn_sampleorderdetails.intQty,
							intSampleTypeId,
							strSize,
							dtRequiredDate,
							$SRdb.trn_sample_requisition_detail.BAL_QTY,
							trn_sampleorderdetails.dtFabricInDate
							FROM
							trn_sampleorderdetails
							Inner Join mst_part ON trn_sampleorderdetails.intPart = mst_part.intId
							LEFT JOIN $SRdb.trn_sample_requisition_detail ON $SRdb.trn_sample_requisition_detail.REQUISITION_NO='$requisitionNo' AND
							$SRdb.trn_sample_requisition_detail.REQUISITION_YEAR='$requisitionYear' AND
							$SRdb.trn_sample_requisition_detail.SAMPLE_TYPE=trn_sampleorderdetails.intSampleTypeId
							WHERE
							trn_sampleorderdetails.intSampleOrderNo =  '$orderNo' AND
							trn_sampleorderdetails.intSampleOrderYear =  '$orderYear'
							";
		//echo $sqlExist;
		$resultExist = $db->RunQuery($sqlExist);
		while($rowE=mysqli_fetch_array($resultExist))
		{
			
			$haveRecord = true;
			$existCombo=$rowE['strCombo'];
			$existPartID=$rowE['intPart'];
			$existPart=$rowE['strName'];
			$existGrade=$rowE['intGrade'];
			$existQty=$rowE['intQty'];
			$existSize=$rowE['strSize'];
			$reqBalQty=$rowE['BAL_QTY'];
			$existSampleTypeId=$rowE['intSampleTypeId'];
			$existReqDate1=$rowE['dtRequiredDate'];
			$existReqDate=substr($existReqDate1,0,10);
			$existTimeHH=substr($existReqDate1,11,2);
			$existTimeMM=substr($existReqDate1,14,2);
			$existFabInDate=$rowE['dtFabricInDate'];
			
				  ?>
            
            <tr class="normalfnt">
              <td align="center" bgcolor="#FFFFFF" class="delImg"><img src="images/del.png" width="15" height="15" /></td>
              <td bgcolor="#FFFFFF" class="normalfnt"><select class="validate[required] combo" name="cboCombo" id="cboCombo" style="width:100%" >
                <option value=""></option>
                <?php
			   /*$sql="SELECT DISTINCT
					trn_sampleinfomations_combos.strComboName
					FROM trn_sampleinfomations_combos
					WHERE
					trn_sampleinfomations_combos.intSampleNo =  '$sampleID' AND
					trn_sampleinfomations_combos.intSampleYear =  '$year' AND
					trn_sampleinfomations_combos.intRevisionNo =  '$revNo'
					ORDER BY
					trn_sampleinfomations_combos.strComboName ASC
					";*/
				$sql = "SELECT DISTINCT
							trn_sampleinfomations_details.strComboName
						FROM trn_sampleinfomations_details
						WHERE
							trn_sampleinfomations_details.intSampleNo =  '$sampleID' AND
							trn_sampleinfomations_details.intSampleYear =  '$year' AND
							trn_sampleinfomations_details.intRevNo =  '$revNo'
						ORDER BY
							trn_sampleinfomations_details.strComboName ASC";
				
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
			  ?>
                <option value="<?php echo $row['strComboName'] ?>" <?php if($row['strComboName']==$existCombo){ ?> selected="selected" <?php } ?>><?php echo $row['strComboName'] ?></option>
                <?php
				
					}
			  ?>
                </select>
                </td>
              <td bgcolor="#FFFFFF" class="normalfnt" id=""><select class="validate[required] part" name="cboCombo" id="cboCombo"  style="width:100%" >
                <option value=""></option>
                <?php
			   $sql="	SELECT distinct
							mst_part.intId,
							mst_part.strName
						FROM
							trn_sampleinfomations_printsize
						Inner Join mst_part ON mst_part.intId = trn_sampleinfomations_printsize.intPart
						WHERE
							trn_sampleinfomations_printsize.intSampleNo 	=  '$sampleID' AND
							trn_sampleinfomations_printsize.intSampleYear 	=  '$year' AND
							trn_sampleinfomations_printsize.intRevisionNo 	=  '$revNo'

					";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
			  ?>
                <option value="<?php echo $row['intId'] ?>"<?php if($row['intId']==$existPartID){ ?> selected="selected" <?php } ?>><?php echo $row['strName'] ?></option>
                <?php
					}
			  ?>
                </select>
                </td>
              <td bgcolor="#FFFFFF"><select class="validate[required] sampType" name="cboSampleTypes" id="cboSampleTypes" style="width:100%" >
                <option value=""></option>
                <?php
				$sql = "SELECT MST.intId AS TYPE_ID,
				MST.strName AS TYPE_NAME
				FROM mst_sampletypes MST ";
				
				if($requisitionNo!='')
				{
					$sql.="INNER JOIN $SRdb.trn_sample_requisition_detail SRD ON MST.intId=SRD.SAMPLE_TYPE ";
				}
				
				$sql.="WHERE MST.intStatus = 1 ";
				
				if($requisitionNo!='')
				{
					$sql.=" AND SRD.REQUISITION_NO = '$requisitionNo' 
							AND SRD.REQUISITION_YEAR = '$requisitionYear'
							AND SRD.QTY > 0 ";
				}
				$sql.="ORDER BY MST.strName ";
				
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($row['TYPE_ID']==$existSampleTypeId)
						echo " <option selected=\"selected\" value=\"".$row['TYPE_ID']."\">".$row['TYPE_NAME']."</option>";
					else
						echo " <option  value=\"".$row['TYPE_ID']."\">".$row['TYPE_NAME']."</option>";
				}
			  ?>
                </select></td>
              <td bgcolor="#FFFFFF"><input name="textfield4" type="text"  class="validate[required] grade" id="textfield4" style="width:100%" value="<?php echo  $existGrade; ?>" /></td>
              <td bgcolor="#FFFFFF"><input name="txtSize" type="text"  id="txtSize" style="width:100%" value="<?php echo  $existSize; ?>" class="size" /></td>
              <td class="cls_tdQty" bgcolor="#FFFFFF"><input name="textfield5" class="validate[required,custom[number],min[1]<?php echo($reqBalQty!=''?'max['.$reqBalQty.']':''); ?>] qty" type="text"  id="textfield5" style="width:100%;text-align:right" value="<?php echo  $existQty; ?>" /></td>
              <td bgcolor="#FFFFFF"><input name="dtDate2" type="text" value="<?php echo ($existReqDate==''?date("Y-m-d"):$existReqDate); ?>" class="txtbox date" id="dtDate<?php echo ++$x; ?>" style="width:90px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              <td bgcolor="#FFFFFF"><input name="dtFabricInDate" type="text" value="<?php echo ($existFabInDate==''?date("Y-m-d"):$existFabInDate); ?>" class="txtbox fabIndate validate[required]" id="dtFabricInDate<?php echo ++$y; ?>" style="width:90px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              <td bgcolor="#FFFFFF"><input name="txtTimeHH2" type="text"  id="txtTimeHH2" style="width:30px" value="<?php echo  $existTimeHH; ?>"   class="txtText validate[required, max[24]] clsTimeHH" /> 
                : <input name="txtTimeMM" type="text"  id="txtTimeMM" style="width:30px" value="<?php echo  $existTimeMM; ?>"   class="txtText validate[required, max[60]] clsTimeMM" /></td>
              </tr>
            <?php
			  
			  }
			  }
			  if( $haveRecord==false)
			  {
				 
			  ?>
            <tr class="normalfnt">
              <td align="center" bgcolor="#FFFFFF" class="delImg"><img src="images/del.png" width="15" height="15" /></td>
              <td bgcolor="#FFFFFF" class="normalfnt"><select  class="validate[required] combo" name="cboCombo" id="cboCombo" style="width:100%" >
                <option value=""></option>
                <?php
				$sql = "SELECT DISTINCT
							trn_sampleinfomations_details.strComboName
						FROM trn_sampleinfomations_details
						WHERE
							trn_sampleinfomations_details.intSampleNo =  '$sampleID' AND
							trn_sampleinfomations_details.intSampleYear =  '$year' AND
							trn_sampleinfomations_details.intRevNo =  '$revNo'
						ORDER BY
							trn_sampleinfomations_details.strComboName ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
			  ?>
                <option value="<?php echo $row['strComboName'] ?>"><?php echo $row['strComboName'] ?></option>
                <?php
					}
			  ?>
                </select>
                </td>
              <td bgcolor="#FFFFFF" class="normalfnt" id=""><select  class="validate[required] part" name="cboCombo" id="cboPart" style="width:100%" >
                <option value=""></option>
                <?php
			   $sql="	SELECT distinct
							mst_part.intId,
							mst_part.strName
						FROM
							trn_sampleinfomations_printsize
						Inner Join mst_part ON mst_part.intId = trn_sampleinfomations_printsize.intPart
						WHERE
							trn_sampleinfomations_printsize.intSampleNo 	=  '$sampleID' AND
							trn_sampleinfomations_printsize.intSampleYear 	=  '$year' AND
							trn_sampleinfomations_printsize.intRevisionNo 	=  '$revNo'

					";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
			  ?>
                <option value="<?php echo $row['intId'] ?>"><?php echo $row['strName'] ?></option>
                <?php
					}
			  ?>
                </select>
                </td>
              <td bgcolor="#FFFFFF"><select class="validate[required] sampType" name="cboSampleTypes" id="cboSampleTypes" style="width:100%" >
                <option value=""></option>
                <?php

				$sql = "SELECT MST.intId AS TYPE_ID,
				MST.strName AS TYPE_NAME
				FROM mst_sampletypes MST ";
				
				if($requisitionNo!='')
				{
					$sql.="INNER JOIN $SRdb.trn_sample_requisition_detail SRD ON MST.intId=SRD.SAMPLE_TYPE ";
				}
				
				$sql.="WHERE MST.intStatus = 1 ";
				
				if($requisitionNo!='')
				{
					$sql.="AND SRD.REQUISITION_NO = '$requisitionNo' 
							AND SRD.REQUISITION_YEAR = '$requisitionYear' 
							AND SRD.QTY > 0 ";
				}
				$sql.="ORDER BY MST.strName ";
				
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
			  ?>
                <option value="<?php echo $row['TYPE_ID'] ?>"><?php echo $row['TYPE_NAME'] ?></option>
                <?php
					}
			  ?>
                </select></td>
              <td bgcolor="#FFFFFF"><input name="textfield4" type="text"   class="validate[required] grade" id="txtGrade" style="width:100%" value="" /></td>
              <td bgcolor="#FFFFFF"><input name="txtSize" type="text"   id="txtSize" style="width:100%" value="<?php echo  $existSize; ?>" class="size" /></td>
              <td class="cls_tdQty" bgcolor="#FFFFFF"><input name="textfield5" type="text"  class="validate[required,custom[number],min[1]] qty" id="txtQty" style="width:100%;text-align:right" value="0" /></td>
              <td bgcolor="#FFFFFF"><input name="dtDate3" type="text" value="" class="txtbox date" id="dtDate<?php echo ++$x; ?>" style="width:90px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              <td bgcolor="#FFFFFF"><input name="dtFabricInDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox fabIndate validate[required]" id="dtFabricInDate<?php echo ++$y; ?>" style="width:90px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              <td bgcolor="#FFFFFF"><input name="txtTimeHH" type="text"  id="txtTimeHH" style="width:30px" value="00"   class="txtText validate[required, max[24]] clsTimeHH" /> : <input name="txtTimeMM" type="text"  id="txtTimeMM" style="width:30px" value="00"   class="txtText validate[required, max[60]] clsTimeMM" /></td>
              </tr>
            <?php
			  }
			  ?>
            
            <tr class="dataRow">
              <td colspan="10" align="left"  bgcolor="#FFFFFF" ><img src="images/Tadd.jpg" name="butInsertRow" width="78" height="24" class="mouseover" id="butInsertRow" /></td>
              </tr>
            </table>
          </div></td>
      </tr>
      <tr>
       <td width="100%" align="center" bgcolor="">
                    <a class="button white medium" alt="New" name="butNew" width="92" height="24" id="butNew" tabindex="28">New</a>
                    <?php
                if(($form_permision['add']||$form_permision['edit'] )and $permision_save == 1)
				{
				?>
                    <a class="button white medium"  alt="Save" name="butSave"width="92" height="24"   id="butSave" tabindex="24">Save</a><?php } 
				
				?>
                    <a class="button white medium"  style=" <?php if($permision_confirm!=1){ ?> style="display:none"<?php }?>"   alt="Approve" name="butConfirm"width="92" height="24"   id="butConfirm" tabindex="30">Approve</a>
                    <a class="button white medium"   alt="Report" name="butReport"width="92" height="24"   id="butReport" tabindex="31">Report</a>
                    <a class="button white medium" href="main.php"  alt="Close" name="butClose" width="92" height="24" border="0"   id="butClose" tabindex="27">Close</a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
    
</form>

</body>

<script>
    
 //function checkCustomer(){
 //alert('jjdj');
    var basePath	= "presentation/customerAndOperation/sample/sampleOrder/addNew/";
    var customerId= $("#customer").html();
    //alert(customerId);
    var url = basePath+"sampleOrder-db-get.php?requestType=checkCustomer&customerId="+customerId;
    var  httpobj = $.ajax({
            url:url,
            async:false,
            type: "post",
           dataType:"json",
    success: function(json)
		 {
                  
	var type = (json.type);
        //alert(json.type);
        if(type=="fail"){
            
        alert("Customer is Blocked for Sample");
        }
                    
  }
    
});
//}
</script>
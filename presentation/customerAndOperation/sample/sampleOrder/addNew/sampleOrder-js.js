var basePath	= "presentation/customerAndOperation/sample/sampleOrder/addNew/";	
var menuID		= 354;	
var reportID	= 983;	
$(document).ready(function() {
	
	$('#frmSampleOrder').validationEngine();
	$('#frmSampleOrder #txtCustomerPO').focus();
         

  	$('#frmSampleOrder #cboSampleNo').die('change').live('change',function(){
		//$('#frmSampleOrder').submit();
          //  showWaiting();	
		window.location.href  = "?q="+menuID+"&sampleNo="+$('#cboSampleNo').val()+"&year="+$('#cboYear').val();
              
               // showWaiting();
                //alert($("#customer").html());
	});
	
	$("#frmSampleOrder #cboYear").die('change').live('change',function(){
		document.location.href = "?q="+menuID+"&year="+$('#cboYear').val();
		//loadSampleNos();
	});
	
	$('#frmSampleOrder #butNew').die('click').live('click',function(){
		window.location.href = "?q="+menuID;
	});
	
	$('#frmSampleOrder .sampType').die('change').live('change',loadBalQty);

	$('#frmSampleOrder .delImg').die('click').live('click',function(){
		var rowCount1 = document.getElementById('tblSampOrders').rows.length;
		if(rowCount1>3)
		$(this).parent().remove();
	});

$('#frmSampleOrder #butInsertRow').die('click').live('click',function() {		
	var rowCount = document.getElementById('tblSampOrders').rows.length;
	document.getElementById('tblSampOrders').insertRow(rowCount-1);
	rowCount = document.getElementById('tblSampOrders').rows.length;
	document.getElementById('tblSampOrders').rows[rowCount-2].innerHTML = document.getElementById('tblSampOrders').rows[rowCount-3].innerHTML;
	document.getElementById('tblSampOrders').rows[rowCount-2].cells[7].childNodes[0].id = 'dtDate'+(rowCount-2);
	document.getElementById('tblSampOrders').rows[rowCount-2].cells[8].childNodes[0].id = 'dtFabricInDate'+(rowCount-2);
		//-------------
		$('.delImg').die('click').live('click',function(){
			var rowCount1 = document.getElementById('tblSampOrders').rows.length;
			if(rowCount1>3)
			$(this).parent().remove();
			//this.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);
		});
		//----------
		$('.selcStyle').die('change').live('change',function(){
			var style = $(this).val();
			var row=this.parentNode.parentNode.rowIndex;
			var year = document.getElementById('tblPOs').rows[row].cells[3].childNodes[0].value;
			var url 		= basePath+"placeOrder-db-get.php?requestType=loadSampleNos&style="+style+"&year="+year;
			var httpobj 	= $.ajax({url:url,async:false})
			document.getElementById('tblPOs').rows[row].cells[3].childNodes[1].innerHTML=httpobj.responseText;
			document.getElementById('tblPOs').rows[row].cells[3].childNodes[1].focus();
		});
		//-------------
		$('.cboSampNo1').die('change').live('change',function(){
			var sampNo = $(this).val();
			var row=this.parentNode.parentNode.rowIndex;
			var style = document.getElementById('tblPOs').rows[row].cells[2	].childNodes[0].value;
			var year = document.getElementById('tblPOs').rows[row].cells[3].childNodes[0].value;
			var url 		= basePath+"placeOrder-db-get.php?requestType=loadGraphicNo&style="+style+"&year="+year+"&sampNo="+sampNo;
			var httpobj 	= $.ajax({url:url,async:false})
			document.getElementById('tblPOs').rows[row].cells[4].innerHTML=httpobj.responseText;
			
			var url 		= basePath+"placeOrder-db-get.php?requestType=loadCombos&style="+style+"&year="+year+"&sampNo="+sampNo;
			var httpobj 	= $.ajax({url:url,async:false})
			document.getElementById('tblPOs').rows[row].cells[5].childNodes[0].innerHTML=httpobj.responseText;
		});
		//-------------
		$('.clsCombo').die('change').live('change',function(){
			var combo = $(this).val();
			var row=this.parentNode.parentNode.rowIndex;
			var style = document.getElementById('tblPOs').rows[row].cells[2	].childNodes[0].value;
			var year = document.getElementById('tblPOs').rows[row].cells[3].childNodes[0].value;
			var sampNo = document.getElementById('tblPOs').rows[row].cells[3].childNodes[1].value;
			var url 		= basePath+"placeOrder-db-get.php?requestType=loadPlacement&style="+style+"&year="+year+"&sampNo="+sampNo+"&combo="+combo;
			var httpobj 	= $.ajax({url:url,async:false})
			document.getElementById('tblPOs').rows[row].cells[7].childNodes[0].innerHTML=httpobj.responseText;
		});
		//-------------
		$('.calculateValue').die('keyup').live('keyup',function(){
			var row=this.parentNode.parentNode.rowIndex;
			var qty = document.getElementById('tblPOs').rows[row].cells[9].childNodes[0].value;
			var price = document.getElementById('tblPOs').rows[row].cells[10].childNodes[0].value;
			document.getElementById('tblPOs').rows[row].cells[11].innerHTML=qty*price;
			calculateTotalVal();
		});
		//-------------
		$('.clsAddSizes').die('click').live('click',function(){
			var row=this.parentNode.parentNode.rowIndex;
			var salesOrderNo= document.getElementById('tblPOs').rows[row].cells[1].childNodes[0].value;
			var orderNo=$('#txtOrderNo').val();
			var year=$('#txtYear').val();
			var Qty =  document.getElementById('tblPOs').rows[row].cells[9].childNodes[0].value;
			if(orderNo==''){
				alert("Please Save Shedules before add Sizes");
				return false;	
			}
			
			popupWindow3('1');
			$('#popupContact1').load('placeOrderSizesPopup.php?salesOrderNo='+salesOrderNo+'&orderNo='+orderNo+'&year='+year+'&Qty='+Qty,function(){
					//checkAlreadySelected();
					$('#butAdd').die('click').live('click',addClickedRows);
					$('#butClose1').die('click').live('click',disablePopup);
					//------------------
					$('#butInsertRowPopup').die('click').live('click',function(){
						var rowCount = document.getElementById('tblSizesPopup').rows.length;
						document.getElementById('tblSizesPopup').insertRow(rowCount-1);
						rowCount = document.getElementById('tblSizesPopup').rows.length;
						document.getElementById('tblSizesPopup').rows[rowCount-2].innerHTML = document.getElementById('tblSizesPopup').rows[rowCount-3].innerHTML;
					//	document.getElementById('tblSizesPopup').rows[rowCount-2].className="normalfnt";
					//	document.getElementById('tblSizesPopup').rows[rowCount-2].cells[4].innerHTML='';
							//-------------
							$('.delImgPopup').die('click').live('click',function(){
								var rowCount1 = document.getElementById('tblSizesPopup').rows.length;
								if(rowCount1>3)
								$(this).parent().parent().remove();
								//this.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);
							});
							//----------
					});
					//------------------
				});	
		});
		//------------
		
	});
 //-------------------------------------------------------
	$('.delImg').die('click').live('click',function(){
		// $(this).parent().parent().remove();
	});
 //------------------------------------------------------
	$('#frmSampleOrder  .selcStyle').die('change').live('change',function(){
		var style = $(this).val();
		var row=this.parentNode.parentNode.rowIndex;
		var year = document.getElementById('tblPOs').rows[row].cells[3].childNodes[0].value;
		var url 		= basePath+"placeOrder-db-get.php?requestType=loadSampleNos&style="+style+"&year="+year;
		var httpobj 	= $.ajax({url:url,async:false})
			document.getElementById('tblPOs').rows[row].cells[3].childNodes[1].innerHTML=httpobj.responseText;
			document.getElementById('tblPOs').rows[row].cells[3].childNodes[1].focus();
	});
 //------------------------------------------------------
	$('#frmSampleOrder  .cboSampNo1').die('change').live('change',function(){
		var sampNo = $(this).val();
		var row=this.parentNode.parentNode.rowIndex;
		var style = document.getElementById('tblPOs').rows[row].cells[2	].childNodes[0].value;
		var year = document.getElementById('tblPOs').rows[row].cells[3].childNodes[0].value;
		var url 		= basePath+"placeOrder-db-get.php?requestType=loadGraphicNo&style="+style+"&year="+year+"&sampNo="+sampNo;
		var httpobj 	= $.ajax({url:url,async:false})
		document.getElementById('tblPOs').rows[row].cells[4].innerHTML=httpobj.responseText;
		
		var url 		= basePath+"placeOrder-db-get.php?requestType=loadCombos&style="+style+"&year="+year+"&sampNo="+sampNo;
		var httpobj 	= $.ajax({url:url,async:false})
		document.getElementById('tblPOs').rows[row].cells[5].childNodes[0].innerHTML=httpobj.responseText;
	});
 //------------------------------------------------------
	$('#frmSampleOrder  .clsCombo').die('change').live('change',function(){
		var combo = $(this).val();
		var row=this.parentNode.parentNode.rowIndex;
		var style = document.getElementById('tblPOs').rows[row].cells[2	].childNodes[0].value;
		var year = document.getElementById('tblPOs').rows[row].cells[3].childNodes[0].value;
		var sampNo = document.getElementById('tblPOs').rows[row].cells[3].childNodes[1].value;
		var url 		= basePath+"placeOrder-db-get.php?requestType=loadPlacement&style="+style+"&year="+year+"&sampNo="+sampNo+"&combo="+combo;
		var httpobj 	= $.ajax({url:url,async:false})
		document.getElementById('tblPOs').rows[row].cells[7].childNodes[0].innerHTML=httpobj.responseText;
	});
 //------------------------------------------------------------------
	$('#frmSampleOrder .calculateValue').die('keyup').live('keyup',function(){
		var row=this.parentNode.parentNode.rowIndex;
		var qty = document.getElementById('tblPOs').rows[row].cells[9].childNodes[0].value;
		var price = document.getElementById('tblPOs').rows[row].cells[10].childNodes[0].value;
		document.getElementById('tblPOs').rows[row].cells[11].innerHTML=qty*price;
		calculateTotalVal();
	});
//-----------------------------------------------------------------
	$('#frmSampleOrder .clsAddSizes').die('click').live('click',function(){
		var row=this.parentNode.parentNode.rowIndex;
		var salesOrderNo= document.getElementById('tblPOs').rows[row].cells[1].childNodes[0].value;
		var Qty =  document.getElementById('tblPOs').rows[row].cells[9].childNodes[0].value;
		var orderNo=$('#txtOrderNo').val();
		var year=$('#txtYear').val();
		if(orderNo==''){
			alert("Please Save Shedules before add Sizes");
			return false;	
		}
		popupWindow3('1');
		$('#popupContact1').load('placeOrderSizesPopup.php?salesOrderNo='+salesOrderNo+'&orderNo='+orderNo+'&year='+year+'&Qty='+Qty,function(){
				//loadAlreadySaved();
				$('#butAdd').die('click').live('click',addClickedRows);
					$('#butClose1').die('click').live('click',disablePopup);
					//------------------
					$('#butInsertRowPopup').die('click').live('click',function(){
						var rowCount = document.getElementById('tblSizesPopup').rows.length;
						document.getElementById('tblSizesPopup').insertRow(rowCount-1);
						rowCount = document.getElementById('tblSizesPopup').rows.length;
						document.getElementById('tblSizesPopup').rows[rowCount-2].innerHTML = document.getElementById('tblSizesPopup').rows[rowCount-3].innerHTML;
					//	document.getElementById('tblSizesPopup').rows[rowCount-2].className="normalfnt";
					//	document.getElementById('tblSizesPopup').rows[rowCount-2].cells[4].innerHTML='';
							//-------------
							$('.delImgPopup').die('click').live('click',function(){
								var rowCount1 = document.getElementById('tblSizesPopup').rows.length;
								if(rowCount1>3)
								$(this).parent().parent().remove();
								//this.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);
							});
							//----------
					});
					//------------------
				
			});	
	});
//------------------------------------------------------------------
  $('#frmSampleOrder #butSave').die('click').live('click',function(){
	var requestType = '';
	if ($('#frmSampleOrder').validationEngine('validate'))   
    { 
		//if($('#tblSampOrders >tbody >tr').length<=2)
		//{
			
			//return;
		//}
		showWaiting();
		var data = "requestType=save";
		
			/*data+="&serialNo=" +	$('#txtSampOrderNo').val();
			data+="&year=" +	$('#txtSampYear').val();
			data+="&sampNo=" +	$('#cboSampleNo').val();
			data+="&sampYear="	+	$('#cboYear').val();
			data+="&reqNoArr="	+	$('#cboRequisition').val();
			data+="&dat="	+	$('#dtDate').val();
			data+="&attentionTo="	+	URLEncode($('#txtAttentionTo').val());
			data+="&remarks="	+	URLEncode($('#txtNote').val());
			data+="&custLocation=" +	$('#cboLocation').val();*/
			
			var arrHeader = "{";
							arrHeader += '"serialNo":"'+$('#txtSampOrderNo').val()+'",' ;
							arrHeader += '"year":"'+$('#txtSampYear').val()+'",' ;
							arrHeader += '"sampNo":"'+$('#cboSampleNo').val()+'",' ;
							arrHeader += '"sampYear":"'+$('#cboYear').val()+'",' ;
							arrHeader += '"reqNoArr":"'+$('#txtRequisitionNo').val()+'",' ;
							arrHeader += '"dat":"'+$('#dtDate').val()+'",' ;
							arrHeader += '"attentionTo":'+URLEncode_json($('#txtAttentionTo').val())+',';
							arrHeader += '"remarks":'+URLEncode_json($('#txtNote').val())+',';
                                                        arrHeader += '"customer":"'+$('#customer').html()+'",' ;
							arrHeader += '"custLocation":"'+$('#cboLocation').val()+'",' ;
							arrHeader += '"cboCategory":"'+$('#cboCategory').val()+'"' ;
			arrHeader += "}";
                       // var cus=document.getElementById('dtxtCustomer').value;
                       // alert(arrHeader);
			//////////////////// saving main grid part //////////////////////////////////////////
			//var rowCount = $('#tblMain >tbody >tr').length;
			var rowCount = document.getElementById('tblSampOrders').rows.length;
			var row = 0;
			
			var arr="[";
			
			$('#tblSampOrders .combo').each(function(){
				
		//		var cutNo	= $(this).attr('id');
		//		var salesOrderId	= $(this).parent().find(".salesOrderNo").attr('id');
		//		var salesOrderNo	= $(this).parent().find(".salesOrderNo").html();
				//alert(i);
					 arr += "{";
					// alert(document.getElementById('tblPOs').rows[i].cells[1].childNodes[0].value);
					// alert($('#tblPOs >tbody >tr').eq(i).find('td').eq(1).html());
					var combo 			= 	$(this).val();
					var part 			= 	$(this).parent().parent().find(".part").val();
					var sampleTypeId 	= 	$(this).parent().parent().find(".sampType").val();
					var grade 			= 	$(this).parent().parent().find(".grade").val();
					var size 			= 	$(this).parent().parent().find(".size").val();
					var qty 			= 	$(this).parent().parent().find(".qty").val();
					var reqDate			= 	$(this).parent().parent().find(".date").val();
					var timeHH			= 	$(this).parent().parent().find(".clsTimeHH").val();
					var timeMM			= 	$(this).parent().parent().find(".clsTimeMM").val();
					reqDate			= 	reqDate+' '+timeHH+':'+timeMM+':00';
					var fabInDate		= 	$(this).parent().parent().find(".fabIndate").val();
					
/*					var combo 			= 	document.getElementById('tblSampOrders').rows[i].cells[1].childNodes[0].value;
					var part 			= 	document.getElementById('tblSampOrders').rows[i].cells[2].childNodes[0].value;
					var sampleTypeId 	= 	document.getElementById('tblSampOrders').rows[i].cells[3].childNodes[0].value;
					var grade 			= 	document.getElementById('tblSampOrders').rows[i].cells[4].childNodes[0].value;
					var size 			= 	document.getElementById('tblSampOrders').rows[i].cells[5].childNodes[0].value;
					var qty 			= 	document.getElementById('tblSampOrders').rows[i].cells[6].childNodes[0].value;
					var reqDate			= 	document.getElementById('tblSampOrders').rows[i].cells[7].childNodes[0].value;
*/					
					//txtAttentionTo
					
					if(qty>0){
						arr += '"combo":'+URLEncode_json(combo)+',';
						//arr += '"combo":"'+				combo +'",' ;
						arr += '"part":"'+				part +'",' ;
						arr += '"grade":"'+				grade +'",' ;
						arr += '"qty":"'+				qty +'",' ;
						
						arr += '"size":"'+				size +'",' ;
						arr += '"reqDate":"'+			reqDate +'",' ;
						arr += '"fabInDate":"'+			fabInDate +'",' ;
						
						arr += '"sampleTypeId":"'+		sampleTypeId +'"' ;
						arr +=  '},';
					}
			});
			if( arr == "{")
			{
				alert("You can't save order without order qty");	
				showWaiting();
				return;
			}
			arr 			= arr.substr(0,arr.length-1);
			arr 		   += " ]";
			var arrHeader	= arrHeader;
			
			data+="&arr="+arr+"&arrHeader="+arrHeader;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basePath+"sampleOrder-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			type:"POST",
			async:false,
			
			success:function(json){
					$('#frmSampleOrder #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						hideWaiting();
						var t=setTimeout("alertx()",1000);
						$('#txtSampOrderNo').val(json.serialNo);
						$('#txtSampYear').val(json.year);
						
						return;
					}
					else
						hideWaiting();
				},
			error:function(xhr,status){
					//hideWaiting();
					$('#frmSampleOrder #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					//var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	hideWaiting();
      }
       // hideWaiting();
   });
   
	
	//--------------refresh the form----------
	$('#frmPurchaseRequisitionNote #butNew').die('click').live('click',function(){
		$('#frmPurchaseRequisitionNote').get(0).reset();
		clearRows();
		$('#frmPurchaseRequisitionNote #cboDispatchTo').removeAttr('disabled');
		$('#frmPurchaseRequisitionNote #cboDispatchTo').val('');
		$('#frmPurchaseRequisitionNote #cboDispatchTo').focus();
		$('#frmPurchaseRequisitionNote #txtDispNo').val('');
		$('#frmPurchaseRequisitionNote #txtDispYear').val('');
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmPurchaseRequisitionNote #dtDate').val(d);
	});
	//----------------------------------------
	
/*    $('#frmPurchaseRequisitionNote #butDelete').click(function(){
		if($('#frmPurchaseRequisitionNote #cboSearch').val()=='')
		{
			$('#frmPurchaseRequisitionNote #butDelete').validationEngine('showPrompt', 'Please select Type.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmPurchaseRequisitionNote #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "typeOfPrint-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmPurchaseRequisitionNote #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmPurchaseRequisitionNote #butDelete').validationEngine('showPrompt', json.msg,json.type );
												
												if(json.type=='pass')
												{
													$('#frmPurchaseRequisitionNote').get(0).reset();
													loadCombo_frmPurchaseRequisitionNote();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
	
*/	

//-----------------------------------
$('#frmSampleOrder #butReport').die('click').live('click',function(){
	if($('#frmSampleOrder #txtSampOrderNo').val()!=''){
		window.open('?q='+reportID+'&orderNo='+$('#frmSampleOrder #txtSampOrderNo').val()+'&year='+$('#frmSampleOrder #txtSampYear').val(),'rptSampleOrder.php');	
	}
	else
	{
		$('#frmSampleOrder #butReport').validationEngine('showPrompt','No Sample Order no to view Report','fail');
		return;
	}
});

//----------------------------------	
$('#frmSampleOrder #butConfirm').die('click').live('click',function(){
	
	var url  = "?q="+reportID+"&orderNo="+$('#frmSampleOrder #txtSampOrderNo').val();
	    url += "&year="+$('#frmSampleOrder #txtSampYear').val();
	    url += "&approveMode=1";
	window.open(url,'rptSampleOrder.php');
});
//-----------------------------------------------------
//----------------------------------	
$('#frmSampleOrder #butCancel').die('click').live('click',function(){
	
	var url  = "?q="+reportID+"&orderNo="+$('#frmSampleOrder #txtSampOrderNo').val();
	    url += "&year="+$('#frmSampleOrder #txtSampYear').val();
	    url += "&approveMode=2";
	window.open(url,'rptSampleOrder.php');
});
//-----------------------------------------------------
//--------------refresh the form-----------------------
	$('#frmPurchaseRequisitionNote #butNew').die('click').live('click',function(){
		$('#frmPurchaseRequisitionNote').get(0).reset();
		clearRows();
		$('#frmPurchaseRequisitionNote #cboDepartment').val('');
		$('#frmPurchaseRequisitionNote #cboDepartment').focus();
		$('#frmPurchaseRequisitionNote #txtPrnNo').val('');
		$('#frmPurchaseRequisitionNote #txtYear').val('');
		$('#frmPurchaseRequisitionNote #txtRemarks').val('');
		document.getElementById('chkInternal').checked=false;
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmPurchaseRequisitionNote #dtPrnDate').val(d);
		$('#frmPurchaseRequisitionNote #dtRequiredDate').val(d);
	});
        
//-----------------------------------------------------
});

//----------end of ready -------------------------------
function loadMain()
{
	//alert(1);
/*	$("#butAddPendingDispatch").click(function(){
		popupWindow3('1');
		//$('#iframeMain1').attr('src','http://localhost/qpet/presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php?id='+$('#cboDispatchTo').val());
		$('#popupContact1').load('sampleDispatchPopup.php');	
		
	});*/
	//$("#iframeMain1").contents().find("#butAdd").click(abc);
	//$("#frmPurchaseRequisitionNotePopup").contents().find("#butAdd").click(abc);
//	$('#frmPurchaseRequisitionNotePopup #butClose').click(abc);
}




function addClickedRows()
{
	//var rowCount = $('#tblDispatchPopup >tr').length;
	var rowCount = document.getElementById('tblItemsPopup').rows.length;

	for(var i=1;i<rowCount;i++)
	{
		if((document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked==true) && (document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled==false)){
			var categoryID=document.getElementById('tblItemsPopup').rows[i].cells[1].id;
			var category=document.getElementById('tblItemsPopup').rows[i].cells[1].innerHTML;
			var subCatID=document.getElementById('tblItemsPopup').rows[i].cells[2].id;
			var subCat=document.getElementById('tblItemsPopup').rows[i].cells[2].innerHTML;
			var itemID=document.getElementById('tblItemsPopup').rows[i].cells[3].id;
			var itemCode=document.getElementById('tblItemsPopup').rows[i].cells[3].innerHTML;
			var idemDesc=document.getElementById('tblItemsPopup').rows[i].cells[4].innerHTML;
			
			//alert($('#frmPurchaseRequisitionNote #cboDispatchTo').val());
			var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="images/del.png" width="15" height="15" /></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+categoryID+'">'+category+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatID+'">'+subCat+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemID+'">'+itemCode+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemID+'">'+idemDesc+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemID+'" ><input  id="'+itemID+'" class="validate[required,custom[integer],max[1000000]]" style="width:80px;text-align:center" type="text" value="0"/></td>';
		//	content +='<td align="center" bgcolor="#FFFFFF" id="" class=""><input style="width:80px;text-align:center" type="text" value="0"/></td>';
		
			add_new_row('#frmPurchaseRequisitionNote #tblItems',content);
			//$('.')
			//$("#frmPurchaseRequisitionNote").validationEngine();
		//----------------------------	
		$('.delImg').die('click').live('click',function(){
			$(this).parent().parent().remove();
		});
		
		//----------------------------	
		$('.clsValidateBalQty').die('keyup').live('keyup',function(){
			var input=$(this).val();
			var balQty=$(this).closest('td').attr('id');
			if((input>balQty) || (input<0)){
			alert("Invalid Qty");
				$(this).val(balQty);
			}

		});
		
/*	  //--------------change customer------------
		$('#frmPurchaseRequisitionNote #cboDispatchTo').change(function(){
				var val = $.prompt('Are you sure you want to delete existing samples  ?',{
							buttons: { Ok: true, Cancel: false },
							callback: function(v,m,f){
								if(v)
								{
									clearRows();
								}
					}
				});
		});
	//--------------------------------------------	
*/		}
	}
	
	disablePopup();
}
//-------------------------------------
function alertx()
{
	$('#frmPurchaseRequisitionNote #butSave').validationEngine('hide')	;
 	window.location.href = '?q='+menuID+'&orderNo='+$('#txtSampOrderNo').val()+'&orderYear='+$('#txtSampYear').val();
}
function alertDelete()
{
	$('#frmPurchaseRequisitionNote #butDelete').validationEngine('hide')	;
}

function closePopUp(){
	
}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblItems').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblItems').deleteRow(1);
			
	}
}
//------------------------------------------------------------
function checkAlreadySelected(){
	var rowCount = document.getElementById('tblItems').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			var itemNo = 	document.getElementById('tblItems').rows[i].cells[3].id;
		
			var rowCount1 = document.getElementById('tblItemsPopup').rows.length;
			for(var j=1;j<rowCount1;j++)
			{
				var itemNoP = 	document.getElementById('tblItemsPopup').rows[i].cells[3].id;
				
				if(itemNo==itemNoP){
					document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked=true;
					document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled=true;
				}
			}
	}
}
//---------------------------------------------------------------------
function addClickedRows(){
	if ($('#frmPlaceOrderPopup').validationEngine('validate'))   
    { 
		var data = "requestType=saveSizes";
		
			data+="&serialNo=" +	$('#txtOrderNo').val();
			data+="&year=" +	$('#txtYear').val();
			data+="&salesOrderNo=" +	$('#txtsalesOrderNo').val();
			
			//////////////////// saving main grid part //////////////////////////////////////////
			//var rowCount = $('#tblMain >tbody >tr').length;
			var rowCount = document.getElementById('tblSizesPopup').rows.length;
			var row = 0;
			
			var arr="[";
			
			for(var i=1;i<rowCount-1;i++)
			{
					 arr += "{";
					var size = 	document.getElementById('tblSizesPopup').rows[i].cells[1].childNodes[0].value;
					var grade = 	document.getElementById('tblSizesPopup').rows[i].cells[2].childNodes[0].value;
					var qty= 	document.getElementById('tblSizesPopup').rows[i].cells[3].childNodes[0].value;
					
					if(qty>0){

						arr += '"size":"'+		size  +'",' ;
						arr += '"grade":"'+		grade +'",' ;
						arr += '"qty":"'+		qty +'"' ;
						arr +=  '},';
						
					}
			}
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basePath+"placeOrder-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmPlaceOrderPopup #butAdd').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertx()",1000);
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmPlaceOrderPopup #butAdd').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
}
//------------------------------------------------------------------------------
function calculateTotalVal(){
	var rowCount = document.getElementById('tblPOs').rows.length;
	var row = 0;
	var totQty=0;
	var totValue=0;
	
	for(var i=1;i<rowCount-1;i++)
	{
		var qty= 	document.getElementById('tblPOs').rows[i].cells[9].childNodes[0].value;
		var price = 	document.getElementById('tblPOs').rows[i].cells[10].childNodes[0].value;
		totQty+=parseFloat(qty);
		totValue+=parseFloat(qty)*parseFloat(price);
	}
	$('#txtTotQty').val(totQty);
	$('#txtTotVal').val(totValue);
}
//------------------------------------------------------------
function loadSampleNos(){
	var year = $('#cboYear').val();
	var url 		= basePath+"sampleOrder-db-get.php?requestType=loadSampleNos&year="+year;
	var httpobj 	= $.ajax({url:url,async:false})
	document.getElementById('cboSampleNo').innerHTML=httpobj.responseText;
}

function loadSampleType()
{
	var requsitionNoArr = $(this).val();
	var url 		= basePath+"sampleOrder-db-get.php?requestType=loadSampleType&requsitionNoArr="+requsitionNoArr;
	var httpobj 	= $.ajax({url:url,async:false})
	$('#tblSampOrders').find('.sampType').html(httpobj.responseText);
	$('#tblSampOrders').find('.cls_tdQty').children().removeClass();
	$('#tblSampOrders').find('.cls_tdQty').children().addClass('validate[required,custom[number],min[1]] qty');
	
}
function loadBalQty()
{
	var requsitionNoArr = $('#txtRequisitionNo').val();
	var obj				= this;
	
	if(requsitionNoArr!='')
	{
		if($(this).val()=='')
		{
			$(this).parent().parent().find('.cls_tdQty').children().removeClass();
			$(this).parent().parent().find('.cls_tdQty').children().addClass('validate[required,custom[number],min[1]] qty');
			$(this).parent().parent().find('.date').val('');
			return;
		}
		var typeId	= $(this).val();		
		var url 	= basePath+"sampleOrder-db-get.php?requestType=loadBalQty";
		var data 	= "typeId="+typeId+"&requsitionNoArr="+requsitionNoArr;
		$.ajax({
				url:url,
				data:data,
				dataType:'json',
				async:false,
				success:function(json)
				{
					var maxVal			= json.balQty;
					var reqDate			= json.reqDate;
					
					$(obj).parent().parent().find('.cls_tdQty').children().removeClass();
					$(obj).parent().parent().find('.cls_tdQty').children().addClass('validate[required,custom[number],min[1],max['+maxVal+']] qty');
					$(obj).parent().parent().find('.date').val(reqDate);
				}
		});	
	}
}
//------------------------------------------------------------

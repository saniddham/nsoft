<?php
session_start();
$backwardseperator = "../../../../../";
$userId = $_SESSION['userId'];
$locationId = $_SESSION["CompanyID"];
$companyId = $_SESSION["headCompanyId"];
$SRdb = $_SESSION["SRDatabese"];

$requestType = $_REQUEST['requestType'];
$programName = 'Sample Place Order';
$programCode = 'P0354';

include_once "../../../../../dataAccess/Connector.php";
include_once "../../../../../class/cls_commonFunctions_get.php";
include_once "../../../../../class/cls_commonErrorHandeling_get.php";
include_once("../../../../../class/tables/mst_customer.php");

$obj_common = new cls_commonFunctions_get($db);
$obj_errorHandeling = new cls_commonErrorHandeling_get($db);

$savedStatus = true;
$finalApprove = false;
$savedMasseged = '';
$error_sql = '';

if ($requestType == 'save') {
    $arrHeader = json_decode($_REQUEST['arrHeader'], true);
    $arr = json_decode($_REQUEST['arr'], true);

    $db->begin();

    $serialNo = $arrHeader['serialNo'];
    $year = $arrHeader['year'];
    $sampNo = $arrHeader['sampNo'];
    $sampYear = $arrHeader['sampYear'];
    $requistionNoCon = $arrHeader['reqNoArr'];
    $requistionNoArr = explode('/', $requistionNoCon);
    $requisitionNo = ($requistionNoArr[0] == '' ? 'null' : $requistionNoArr[0]);
    $requisitionYear = (sizeof($requistionNoArr) > 1 && $requistionNoArr[1] != '' ? $requistionNoArr[1] : 'null');
    $dat = $arrHeader['dat'];
    $attentionTo = $obj_common->replace($arrHeader["attentionTo"]);
    $remarks = $obj_common->replace($arrHeader["remarks"]);
    $customer = $arrHeader['customer'];
    $custLocation = $arrHeader['custLocation'];
    $cboCategory = $arrHeader['cboCategory'];
    $editMode = false;
    $maxRevNo = getMaxRevisionNo($sampNo, $sampYear);

    $validateArr = validateBeforeSave($serialNo, $year, $programCode, $userId);
    if ($validateArr['type'] == 'fail' && $savedStatus) {
        $savedStatus = false;
        $savedMasseged = $validateArr["msg"];
    }
    if ($serialNo == '' && $year == '') {
        $sysNo_arry = $obj_common->GetSystemMaxNo('intSampleOrderNo', $locationId);
        if ($sysNo_arry["rollBackFlag"] == 1 && $savedStatus) {
            $savedStatus = false;
            $savedMasseged = $sysNo_arry["msg"];
            $error_sql = $sysNo_arry["q"];
        }

        $serialNo = $sysNo_arry["max_no"];
        $year = date('Y');
        $approveLevels = $obj_common->getApproveLevels($programName);
        $status = $approveLevels + 1;

        $resultHArr = saveHeader($serialNo, $year, $sampNo, $sampYear, $requisitionNo, $requisitionYear, $maxRevNo, $custLocation, $remarks, $dat, $status, $approveLevels, $userId, $attentionTo, $locationId, $customer, $cboCategory);
        if (!empty($resultHArr) && $resultHArr['savedStatus'] == 'fail' && $savedStatus) {
            $savedStatus = false;
            $savedMasseged = $resultHArr['savedMassege'];
            $error_sql = $resultHArr['error_sql'];

        }
    } else {
        $approveLevels = $obj_common->getApproveLevels($programName);
        $status = $approveLevels + 1;

        $resultUHArr = updateHeader($serialNo, $year, $requisitionNo, $requisitionYear, $custLocation, $remarks, $status, $approveLevels, $userId, $attentionTo);
        if ($resultUHArr['savedStatus'] == 'fail' && $savedStatus) {
            $savedStatus = false;
            $savedMasseged = $resultUHArr['savedMassege'];
            $error_sql = $resultUHArr['error_sql'];
        }

        $resultUHArr = updateMaxStatus($serialNo, $year);
        if ($resultUHArr['savedStatus'] == 'fail' && $savedStatus) {
            $savedStatus = false;
            $savedMasseged = $resultUHArr['savedMassege'];
            $error_sql = $resultUHArr['error_sql'];
        }

        $resultDDArr = deleteDetails($serialNo, $year);
        if ($resultUHArr['savedStatus'] == 'fail' && $savedStatus) {
            $savedStatus = false;
            $savedMasseged = $resultUHArr['savedMassege'];
            $error_sql = $resultUHArr['error_sql'];
        }
        $editMode = true;
    }
    foreach ($arr as $arrVal) {
        $combo = $obj_common->replace($arrVal["combo"]);
        $part = $arrVal['part'];
        $grade = $arrVal['grade'];
        $qty = $arrVal['qty'];
        $sampleTypeId = $arrVal['sampleTypeId'];
        $size = $arrVal['size'];
        $reqDate = $arrVal['reqDate'];
        $fabInDate = $arrVal['fabInDate'];

        $resultDArr = saveDetails($serialNo, $year, $combo, $part, $grade, $qty, $sampleTypeId, $size, $reqDate, $locationId, $fabInDate);
        if (!empty($resultDArr) && $resultDArr['savedStatus'] == 'fail' && $savedStatus) {
            echo $savedStatus = false;
            $savedMasseged = $resultDArr['savedMassege'];
            $error_sql = $resultDArr['error_sql'];
        }

    }
    if ($savedStatus) {
        $db->commit();
        $response['type'] = "pass";
        if ($editMode)
            $response['msg'] = "Updated Successfully.";
        else
            $response['msg'] = "Saved Successfully.";
        $response['serialNo'] = $serialNo;
        $response['year'] = $year;
    } else {
        $db->rollback();
        $response['type'] = "fail";
        $response['msg'] = $savedMasseged;
        $response['sql'] = $error_sql;
    }

    // print_r($response);
    echo json_encode($response);
}
function getMaxRevisionNo($sampNo, $sampYear)
{
    global $db;

    $sql = "SELECT MAX(trn_sampleinfomations.intRevisionNo) AS maxRevNo 
			FROM trn_sampleinfomations 
			WHERE intSampleNo = '$sampNo' AND 
			intSampleYear = '$sampYear' ";

    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);

    return $row['maxRevNo'];
}

function validateBeforeSave($serialNo, $year, $programCode, $userId)
{
    global $db;
    global $obj_errorHandeling;

    $header_arr = loadHeaderData($serialNo, $year, 'RunQuery2');
    $validateArr = $obj_errorHandeling->get_permision_withApproval_save($header_arr['intStatus'], $header_arr['APPROVE_LEVELS'], $userId, $programCode, 'RunQuery2');

    return $validateArr;
}

function loadHeaderData($serialNo, $year, $executionType)
{
    global $db;

    $sql = "SELECT 	intSampleNo, 
			intSampleYear, 
			intSampleReqNo, 
			intSampleReqYear, 
			intRevNo, 
			dtDate, 
			strAttentionTo, 
			intCustomerLocation, 
			strRemarks, 
			intStatus, 
			APPROVE_LEVELS, 
			intLocationId
			FROM trn_sampleorderheader
			WHERE intSampleOrderNo = '$serialNo' AND
			intSampleOrderYear = '$year' ";

    $result = $db->$executionType($sql);
    $row = mysqli_fetch_array($result);

    return $row;
}

function saveHeader($serialNo, $year, $sampNo, $sampYear, $requisitionNo, $requisitionYear, $maxRevNo, $custLocation, $remarks, $dat, $status, $approveLevels, $userId, $attentionTo, $locationId, $customer, $cboCategory)
{
    global $db;

    $sql = "INSERT INTO trn_sampleorderheader 
			(
			intSampleOrderNo, 
			intSampleOrderYear, 
			intSampleNo, 
			intSampleYear, 
			intSampleReqNo, 
			intSampleReqYear, 
			intRevNo, 
			dtDate, 
			strAttentionTo, 
			intCustomerLocation, 
			strRemarks, 
			intStatus, 
			APPROVE_LEVELS, 
			intCreator, 
			dtmCreateDate, 
			intLocationId,
			`SAMPLE CATEGORY`
			)
			VALUES
			(
			'$serialNo', 
			'$year', 
			'$sampNo', 
			'$sampYear', 
			$requisitionNo, 
			$requisitionYear, 
			'$maxRevNo', 
			'$dat', 
			'$attentionTo', 
			'$custLocation', 
			'$remarks', 
			'$status', 
			'$approveLevels', 
			'$userId', 
			NOW(), 
			'$locationId',
			 '$cboCategory'
			) ";
    //echo $sql ;

    $result = $db->RunQuery2($sql);
    $data = array();
    //$result_ipad 	= $db->RunQuery_S($sql);
    if (!$result) {
        $data['savedStatus'] = 'fail';
        $data['savedMassege'] = $db->errormsg;
        $data['error_sql'] = $sql;
    }


    $sql_block = "select ACTIVE_SAMPLE from mst_customer where mst_customer.intId= $customer";
    $result_block = $db->RunQuery2($sql_block);
    $row_block = mysqli_fetch_array($result_block);

    $sample = $row_block['ACTIVE_SAMPLE'];
    //$sample=0;
    if ($sample == 1) {

        $data['savedStatus'] = 'fail';
        $data['savedMassege'] = 'not allowed to save because customer is blocked';
        $data['error_sql'] = $sql;
    }
    return $data;

}

function updateHeader($serialNo, $year, $requisitionNo, $requisitionYear, $custLocation, $remarks, $status, $approveLevels, $userId, $attentionTo)
{
    global $db;
    $data = array();

    $sql = "UPDATE trn_sampleorderheader 
			SET
			intSampleReqNo = $requisitionNo , 
			intSampleReqYear = $requisitionYear , 
			strAttentionTo = '$attentionTo' , 
			intCustomerLocation = '$custLocation' , 
			strRemarks = '$remarks' , 
			intStatus = '$status' , 
			APPROVE_LEVELS = '$approveLevels' , 
			intModifyer = '$userId' , 
			dtmModifyDate = NOW()	
			WHERE
			intSampleOrderNo = '$serialNo' AND 
			intSampleOrderYear = '$year' ";

    $result = $db->RunQuery2($sql);
    //$result_ipad = $db->RunQuery_S($sql);
    if (!$result) {
        $data['savedStatus'] = 'fail';
        $data['savedMassege'] = $db->errormsg;
        $data['error_sql'] = $sql;
    }
    return $data;
}

function updateMaxStatus($serialNo, $year)
{
    global $db;
    $data = array();

    $sql = "SELECT MAX(tb.STATUS) AS maxStatus  
			FROM trn_sampleorderheader_approvedby AS tb
			WHERE 
			tb.intSampleOrderNo='$serialNo' AND
			tb.intSampleOrderYear='$year' ";

    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $maxStatus = $row['maxStatus'];

    $sqlIns = "UPDATE trn_sampleorderheader_approvedby 
				SET
				STATUS = $maxStatus+1
				WHERE
				intSampleOrderNo = '$serialNo' AND 
				intSampleOrderYear = '$year' ";

    $resultIns = $db->RunQuery2($sqlIns);
    //$result_ipad = $db->RunQuery_S($sqlIns);
    if (!$resultIns) {
        $data['savedStatus'] = 'fail';
        $data['savedMassege'] = $db->errormsg;
        $data['error_sql'] = $sqlIns;
    }
    return $data;
}

function deleteDetails($serialNo, $year)
{
    global $db;

    $sql = "DELETE FROM trn_sampleorderdetails 
			WHERE
			intSampleOrderNo = '$serialNo' AND 
			intSampleOrderYear = '$year' ";

    $result = $db->RunQuery2($sql);
    //$result_ipad	= $db->RunQuery_S($sql);
    if (!$result) {
        $data['savedStatus'] = 'fail';
        $data['savedMassege'] = $db->errormsg;
        $data['error_sql'] = $sql;
    }
    return $data;
}

function saveDetails($serialNo, $year, $combo, $part, $grade, $qty, $sampleTypeId, $size, $reqDate, $locationId, $fabInDate)
{
    global $db;

    $sql = "INSERT INTO trn_sampleorderdetails 
			(
			intSampleOrderNo, 
			intSampleOrderYear, 
			strCombo, 
			intPart, 
			intGrade, 
			strSize, 
			intQty, 
			intBalToFabricInQty,
			intSampleTypeId, 
			dtRequiredDate, 
			dtFabricInDate,
			intCompanyId
			)
			VALUES
			(
			'$serialNo', 
			'$year', 
			'$combo', 
			'$part', 
			'$grade', 
			'$size', 
			'$qty', 
			'$qty',
			'$sampleTypeId', 
			'$reqDate',
			'$fabInDate', 
			'$locationId'
			) ";

    $result = $db->RunQuery2($sql);
    $data = array();
    //$result_ipad	= $db->RunQuery_S($sql);
    if (!$result) {
        $data['savedStatus'] = 'fail';
        $data['savedMassege'] = $db->errormsg;
        $data['error_sql'] = $sql;
    }
    return $data;
}

/*function updateFabInDate($requisitionNo,$requisitionYear,$sampleTypeId,$fabInDate)
{
	global $db;
	global $SRdb;

	$sql = "UPDATE $SRdb.trn_sample_requisition_detail
			SET
			FABRIC_IN_DATE = '$fabInDate'
			WHERE
			REQUISITION_NO = '$requisitionNo' AND
			REQUISITION_YEAR = '$requisitionYear' AND
			SAMPLE_TYPE = '$sampleTypeId' ";

	$result 	= $db->RunQuery2($sql);
	if(!$result)
	{
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= $db->errormsg;
		$data['error_sql']		= $sql;
	}
	return $data;
}*/
?>
/**
 * Created by PhpStorm.
 * User: Hasitha
 * Date: 1/26/2018
 * Time: 11:35 AM
 */
//close_modal

var basepath = 'presentation/customerAndOperation/sample/grading_sample/';
const valuezeroAlert = 'Please make sure the value is not zero';
const changeValueAlert = 'change the value';
const validateAreaValue = 'Please check again the height and width values for area percentage';

$(document).ready(function(){

    // Get the modal
    var modal = document.getElementById('myModal');

    $("#loadGrading").click(function(){
        $("#myTable tbody").empty();
        var year = $('#cboYear').val();
        var sampleNo = $('#cboSampleNo').val();
        var RevisionNo = $('#cboRevision').val();
        var combo = URLEncode($('#cboCombo').val());

        var print = URLEncode($('#cboPrint').val());

        if(sampleNo =='SELECT GRAPHIC' || sampleNo == 'LOAD YEAR FIRST'){
            alert('Please Select Sample Number');
            return false;
        }else if(RevisionNo == 'SELECT REVISION'){
            alert('Please Select Revision');
            return false;
        }else if(combo =='SELECT COMBO'){
            alert('Please Select Combo');
            return false;
        }else if(print == 'SELECT PRINT'){
            alert('Please Select PRINT');
            return false;
        }
        var url = basepath+"grading_db.php?type=LoadModalTable&sampleNo="+sampleNo+"&year="+year+"&RevisionNo="+RevisionNo+"&print="+print+"&combo="+combo;
		
        var obj =  $.ajax({url:url,async:false});
        var result =JSON.parse(obj['responseText']);
        //console.log(result);

        $("#myTable tbody").append(result);


var i=0;
        $('#myTable >tbody >tr').not("#first").not("#second").each(function() {

            // $('#deleteRow'+i).die('click').live('click',function() {

            $('#deleteRow'+i).click(function() {
                $('#loader').removeClass('hidden');
               // var size= $('#myTable >tbody >tr').find('#size').val();
               var size= $(this).parent().parent().find('#size').val();
				//alert(size)
                var sampleNo = $('#sampleNo').val();
				//alert(sampleNo)
                var year = $('#year').val();
                var revision = $('#revision').val();
                var combo = $('#combo').val();
                var print = $('#print').val();
                var type = 'DeleteRow';
                var url = basepath+"grading_db.php";
                var obj = $.ajax({
                    url:url,
                    dataType: "json",
                    type:'POST',
                    data:{'Request_Type':type,sampleNo:sampleNo,year:year,revision:revision,combo:combo,print:print,size:size},
                    async:false,
                    success:function(json){
                        $('#loader').addClass('hidden');
                        if(json['status'] == "fail"){

                            return false;
                        }
                    }
                });

            });


            if ($('#default'+i).val()== 1)
            {
                $('input[type=radio]').removeAttr('checked');
                $('#default'+i).attr('checked', 'checked');

            }

            i++;




        });


        // if(obj['responseText'] != 'null') {
        //     //var length = Object.keys(obj['SIZE']).length;
        //     var result = JSON.parse(obj['responseText']);
        //     var count = result.length;
        //     if (count >= 1) {
        //         for (i = 0; i < count; i++) {
        //
        //             if (result[i]['DEFAULT'] == 1) {
        //                 var checked = 'checked';
        //             } else {
        //                 var checked = false;
        //             }
        //             html = ' <tr id="row_'+i+'">\n' +
        //                 '                        <td>\n' +
        //                 '                            <input type="button"  value="Delete" />\n' +
        //                 '                        </td>\n' +
        //                 '                        <td>\n' +
        //                 '                            <input id="size" name="size[]" type="text" value="' + result[i]['SIZE'] + '" class="grading" />\n' +
        //                 '                        </td>\n' +
        //                 '                        <td>\n' +
        //                 '                            <input id="default" name="default" type="radio" value="' + i + '"  class="grading grading_default" />\n' +
        //                 '                        </td>\n' +
        //                 '                        <td>\n' +
        //                 '                            <input id="uom" name="uom[]" type="text" value="" class="grading" />\n' +
        //                 '                        </td>\n' +
        //                 '                        <td>\n' +
        //                 '                            <input id="precentage" name="precentage[]" type="text" value="' + result[i]['PRESENTAGE'] + '" class="grading" />\n' +
        //                 '                        </td>\n' +
        //                 '                        <td>\n' +
        //                 '                            <input id="height" name="height[]" type="text" value="' + result[i]['WIDTH'] + '" class="grading" />\n' +
        //                 '                        </td>\n' +
        //                 '                        <td>\n' +
        //                 '                            <input id="width" name="width[]" type="text" value="' + result[i]['HEIGHT'] + '" class="grading" />\n' +
        //                 '                        </td>\n' +
        //                 '\n' +
        //                 '                    </tr>';
        //
        //             $("#myTable tbody").append(html);
        //         }
        //     }
        // }
        //console.log(length);

        // Get the button that opens the modal
        var btn =  document.getElementById("modal");


        modal.style.display = "block";
        $('#year').val(year);
        $('#sampleNo').val(sampleNo);
        $('#revision').val(RevisionNo);
        $('#combo').val(combo);
        $('#print').val(print);

        //ob.parent().parent().find('#txtPrice').val(obj.responseText);

        // When the user clicks on <span> (x), close the modal


    });
// Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    span.onclick = function() {
        html='';
        $('#myTable tbody tr').replaceWith(html);
        modal.style.display = "none";
        LoadSize ();

        // var x= $('#frmShift #cboShift').val();


    };


    $( "#close_modal" ).click(function() {

        $( "#myTable tbody tr" ).remove();

    });

    $('#myTable').on('click', 'input[type="button"]', function () {

        $(this).closest('tr').remove();
    });

    var x=0;

    $('#add').click(function () {
        rowCount = document.getElementById('myTable').rows.length-2;

        html=' <tr id="row_'+rowCount+'">\n' +
            '                        <td>\n' +
            '                            <input id="deleteRow" type="button" onclick="DeleteData(0)"  value="Delete" class="deleteRow"/>\n' +
            '                        </td>\n' +
            '                        <td>\n' +
            '                            <input id="size" name="size[]" type="text" value="" class="grading size_gr" />\n' +
            '                        </td>\n' +
            '                        <td>\n' +
            '                            <input id="default'+rowCount+'" name="default" type="radio" value="'+rowCount+'" class="grading grading_default" />\n' +
            '                        </td>\n' +
            '                        <td style="display:none;">\n' +
            '                            <input id="uom" name="uom[]" type="text" value="" class="grading" />\n' +
            '                        </td>\n' +
            '                        <td style="display: none;">\n' +
            '                            <input id="precentage" name="precentage[]" type="text" value="" class="grading" />\n' +
            '                        </td>\n' +
            '                        <td>\n' +
            '                            <input id="height" name="height[]" type="text" value="" class="grading" />\n' +
            '                        </td>\n' +
            '                        <td>\n' +
            '                            <input id="width" name="width[]" type="text" value="" class="grading" />\n' +
            '                        </td>\n' +
            '\n' +
            '                    </tr>';


        $('#myTable tbody').append(html);

    });



    /*
      get onchange
     */



    $("#cboYear").die('change').live('change',function(){
   // $("#cboYear").change(function() {

        var year = $('#cboYear').val();

        var url = basepath + "grading_db.php?type=LoadGraphicNo&year="+year;
        var obj = $.ajax({url: url, async: false});
        // console.log(obj['responseText']);
        $(".empty").empty();
        $("#cboSampleNo").empty().append('<option>SELECT GRAPHIC</option>'+obj['responseText']);

    });

    $("#cboSampleNo").change(function () {
        var sampleNo = $('#cboSampleNo').val();
        var url = basepath + "grading_db.php?type=LoadRevisionNo&sampleNo="+sampleNo;
        var obj = $.ajax({url: url, async: false});
        // console.log(obj['responseText']);
        $("#cboCombo").empty();
        $("#cboRevision").empty().append('<option>SELECT REVISION</option>'+obj['responseText']);
    });

    $("#cboRevision").change(function () {
        var RevisionNo = $('#cboRevision').val();
        var year = $('#cboYear').val();
        var sampleNo = $('#cboSampleNo').val();
        var url = basepath + "grading_db.php?type=LoadCombo&RevisionNo="+RevisionNo+"&year="+year+"&sampleNo="+sampleNo;
        var obj = $.ajax({url: url, async: false});
        // console.log(obj['responseText']);
        //$("#cboCombo").empty();
        // $("#cboCombo").empty().append('<option>SELECT COMBO</option>'+obj['responseText']);

        $("#cboCombo").html('<option>SELECT COMBO</option>'+obj.responseText);
    });

    $("#cboCombo").change(function () {
        var combo = $("#cboCombo").val();
        var RevisionNo = $('#cboRevision').val();
        var year = $('#cboYear').val();
        var sampleNo = $('#cboSampleNo').val();
        var data= {type:'LoadPrint',year:year,sampleNo:sampleNo,Revision:RevisionNo,combo:combo};
        var getData = $.ajax({
            type: 'POST',
            url: basepath+"grading_db.php?type=LoadPrint",
            data: data,
            dataType: "html",
            success: function(json) {

                // $("#cboPrint").empty().append(json);
                $("#cboPrint").empty().html('<option>SELECT COMBO</option>'+json);

            }
        });




        // var url = basepath + "grading_db.php?type=LoadPrint&RevisionNo="+RevisionNo+"&year="+year+"&sampleNo="+sampleNo+"&combo="+combo;
        // var obj = $.ajax({url: url, async: false});
        // console.log(obj['responseText']);
        //
        // $("#cboPrint").empty().append('<option>SELECT PRINT</option>'+obj['responseText']);
    });


    $("#cboPrint").change(function () {

        var combo = $("#cboCombo").val();
        var RevisionNo = $('#cboRevision').val();
        var year = $('#cboYear').val();
        var sampleNo = $('#cboSampleNo').val();
        var  print = $("#cboPrint").val();
        var data= {type:'LoadPrintWiseTable',year:year,sampleNo:sampleNo,Revision:RevisionNo,combo:combo,print:print};
        var getData = $.ajax({
            type: 'POST',
            url: basepath+"grading_db.php?type=LoadTables",
            data: data,
            dataType: "json",
            success: function(json) {
                //console.log(json);
                $("#cbosize").empty().append(json['size_combo']);

                $("#mainTableNonrollform tbody").empty();
                $("#mainTableRollform tbody").empty();
                $("#mainTableInkType tbody").empty();
                $("#mainTableNonDirectitem tbody").empty();

                LoadTables(1,json);


            }
        });
    });


    /*-----SIZE WISE COMBO ON CHANGE LOAD ---*/
    $('#cbosize').change(function() {

        var combo = $("#cboCombo").val();
        var RevisionNo = $('#cboRevision').val();
        var year = $('#cboYear').val();
        var sampleNo = $('#cboSampleNo').val();
        var  print = $("#cboPrint").val();
        var size = $('#cbosize').val();
        var data= {type:'LoadSizeWiseTable',year:year,sampleNo:sampleNo,Revision:RevisionNo,combo:combo,print:print,size:size};
        var getData = $.ajax({
            type: 'POST',
            url: basepath + "grading_db.php?type=LoadSizeWiseTable",
            data: data,
            dataType: "json",
            success: function(json) {
                $("#mainTableNonrollform tbody").empty();
                $("#mainTableRollform tbody").empty();
                $("#mainTableInkType tbody").empty();
                $("#mainTableNonDirectitem tbody").empty();
                LoadTables(2,json);


            }
        });
    });

    function setWarningAlertWhenChangeValue(attrName){
       attrName.change(function() {
            var valuIs = $(this).val();
            if (valuIs == 0){
                $(this).attr({
                    "title" : valuezeroAlert
                });
                alert(valuezeroAlert);
            } else {
                $(this).attr({
                    "title" : changeValueAlert
                });
            }
        });
    }


    /*-----SIZE WISE COMBO ON CHANGE LOAD ---*/

/*--Load Main Tables From Ajax--*/
    function LoadTables(type,json) {

        var countInkType =Object.keys(json['inkType']).length;
        if(countInkType != 0) {
            $('#mainTableInkType').show();
            for (i = 0; i < countInkType; i++) {

                if(type ==1){
                     var consuption =json['inkType'][i]['CONSUMPTION'];


                    if(json['inkType'][i]['EDITEDWIDTH'] != null) {
                        var width = json['inkType'][i]['EDITEDWIDTH'];

                    }else{
                        var width = json['inkType'][i]['WIDTH'];
                    }
                    if(json['inkType'][i]['EDITEDHEIGHT'] != null) {
                         var height = json['inkType'][i]['EDITEDHEIGHT'];

                    }else {
                         var height = json['inkType'][i]['HEIGHT'];
                    }

                    if(json['inkType'][i]['EDITEDWIDTH'] !=null && json['inkType'][i]['EDITEDHEIGHT'] !=null){
                        var autoConsumption = json['inkType'][i]['AUTO_CONSUMPTION'];
                    }else {

                        var autoConsumption1 = (consuption/ (width*height))*(width*height);
                        autoConsumption=autoConsumption1.toFixed(8);
                        if($.isNumeric(autoConsumption1) !=true ){
                            autoConsumption=0;
                        }
                    }


                }
                else{
                    var consuption =json['inkType'][i]['CONSUMPTION'];

                    if(json['inkType'][i]['EDITEDWIDTH'] != null) {
                        var width = json['inkType'][i]['EDITEDWIDTH'];

                    }else{
                         var width = json['inkType'][i]['SELECTED_SIZE_WIDTH'];
                    }
                    if(json['inkType'][i]['EDITEDHEIGHT'] != null) {
                         var height = json['inkType'][i]['EDITEDHEIGHT'];

                    }else {
                         var height = json['inkType'][i]['SELECTED_SIZE_HEIGHT'];
                    }

                    //var  width =json['inkType'][i]['SELECTED_SIZE_WIDTH'];
                   // var height =json['inkType'][i]['SELECTED_SIZE_HEIGHT'];
                    var autoConsumption = json['inkType'][i]['AUTO_CONSUMPTION'];
                }
                if(json['inkType'][i]['EDITED_CONSUMPTION'] != null) {
                    var editedConsumption = json['inkType'][i]['EDITED_CONSUMPTION'];
                }else{
                    var editedConsumption =0;
                }
                var defwidth =json['inkType'][i]['WIDTH'];
                var defheight =json['inkType'][i]['HEIGHT'];
                var method ="calAutoConsumptionn("+'1,'+consuption+','+defwidth+','+defheight+','+i+")";

				var bgColor = "#FFFFFF";
				if(editedConsumption > 0)
					bgColor = "#8FD799";

                var newRowContent = "<tr  bgcolor='"+bgColor+"'><td>" + json['inkType'][i]['ColorName'] + "</td><td>"+ json['inkType'][i]['inkType']+"</td><td>" + json['inkType'][i]['techniques'] + "</td><td>" + json['inkType'][i]['itemCode'] + "</td><td>" + json['inkType'][i]['itemName'] + "</td><td>" + json['inkType'][i]['UOM'] + "</td><td>"+ json['inkType'][i]['CONSUMPTION'] +"<input type='hidden' name='defconsumption_inktype[]' id='defconsumption_inktype' value='"+json['inkType'][i]['CONSUMPTION']+"'></td>" +
                    "<td>W :<input type='text' title='Change the Value' name='width_inktype[]' id='inktypeW' class='txtbox inktype_W whclass' maxlength='5' value="+width+" onkeyup='"+method+"'> &nbsp;<br>&nbsp;H :<input type='text' maxlength='5' name='height_inktype[]' id='inktypeH' class='txtbox inktype_H whclass' value="+height+" onkeyup='"+method+"'></td>\n" +
                    "                            <td><input name='autoConsumption_inktype[]' id='autoConsumption_inktype'  class='autoConsumption_inktype' type='text' value='"+autoConsumption+"' readonly ></td>\n" +
                    "                            <td><input type='text' id='edited_consumpition_inktype' name='edited_consumpition_inktype[]' value='"+editedConsumption+"'/></td>" +
                    "<td style='display: none'><input type='hidden' name='colourName_inktype[]' id='colourName_inktype' value='"+json['inkType'][i]['intColorId']+"'>" +
                    "<input type='hidden' name='inktype[]' id='inktype' value='"+json['inkType'][i]['intInkTypeId']+"'>" +
                    "<input type='hidden' name='techniques_inktype[]' id='techniques_inktype' value='"+json['inkType'][i]['intTechniqueId']+"'>" +
                    "<input type='hidden' name='itemCode_inktype[]' id='itemCode_inktype' value='"+json['inkType'][i]['intItem']+"'></td></tr>";
                $("#mainTableInkType tbody").append(newRowContent);


            }
        }else{
            $("#mainTableInkType").css('display','none');
        }
        var countRollForm = Object.keys(json['rollForm']).length;
        if(countRollForm != 0) {
            $('#mainTableRollform').show();
            for (i = 0; i < countRollForm; i++) {

                if(type ==1){
                   // console.log(json['rollForm'][i]); return false;
                    var consuption =json['rollForm'][i]['CONSUMPTION'];


                    if(json['rollForm'][i]['EDITEDWIDTH'] != null) {
                        var width = json['rollForm'][i]['EDITEDWIDTH'];

                    }else{
                        var width = json['rollForm'][i]['WIDTH'];
                    }
                    if(json['rollForm'][i]['EDITEDHEIGHT'] != null) {
                        var height = json['rollForm'][i]['EDITEDHEIGHT'];

                    }else {
                        var height = json['rollForm'][i]['HEIGHT'];
                    }

                    if(json['rollForm'][i]['EDITEDWIDTH'] !=null && json['rollForm'][i]['EDITEDHEIGHT'] !=null){
                        var autoConsumption = json['rollForm'][i]['AUTO_CONSUMPTION'];
                    }else {

                        var autoConsumption1 = (consuption/ (width*height))*(width*height);
                        autoConsumption=autoConsumption1.toFixed(8);
                        if($.isNumeric(autoConsumption1) !=true ){
                            autoConsumption=0;
                        }
                    }




                }else{
                  //  console.log(json['rollForm'][i]); return false;
                    var consuption =json['rollForm'][i]['CONSUMPTION'];

                    if(json['rollForm'][i]['EDITEDWIDTH'] != null) {
                        var width = json['rollForm'][i]['EDITEDWIDTH'];

                    }else{
                        var width = json['rollForm'][i]['SELECTED_SIZE_WIDTH'];
                    }
                    if(json['rollForm'][i]['EDITEDHEIGHT'] != null) {
                        var height = json['rollForm'][i]['EDITEDHEIGHT'];

                    }else {
                        var height = json['rollForm'][i]['SELECTED_SIZE_HEIGHT'];
                    }


                   // var  width =json['rollForm'][i]['SELECTED_SIZE_WIDTH'];
                    //var height =json['rollForm'][i]['SELECTED_SIZE_HEIGHT'];

                    var autoConsumption = json['rollForm'][i]['AUTO_CONSUMPTION'];
                }
                if(json['rollForm'][i]['EDITED_CONSUMPTION'] != null) {
                    var editedConsumption = json['rollForm'][i]['EDITED_CONSUMPTION'];
                }else{
                    var editedConsumption =0;
                }

                var defwidth =json['rollForm'][i]['WIDTH'];
                var defheight =json['rollForm'][i]['HEIGHT'];
                var method ="calAutoConsumptionn("+'2,'+consuption+','+defwidth+','+defheight+','+i+")";

                
				var bgColor = "#FFFFFF";
				if(editedConsumption > 0)
					bgColor = "#8FD799";

                var newRowContent1 = "<tr  bgcolor='"+bgColor+"'><td colspan=\"8\">" + json['rollForm'][i]['ColorName'] + "</td><td>" + json['rollForm'][i]['techniques'] + "</td><td>" + json['rollForm'][i]['itemCode'] + "</td><td>" + json['rollForm'][i]['itemName'] + "</td><td>" + json['rollForm'][i]['UOM'] + "</td><td>" + json['rollForm'][i]['CONSUMPTION'] + "<input type='hidden' name='defconsumption_rollForm[]' id='defconsumption_rollForm' value='"+json['rollForm'][i]['CONSUMPTION']+"'></td>" +
                    "<td>W :<input maxlength='5' type='text' name='width_rollForm[]' id='RollFromW' class='txtbox rollForm_W whclass' value="+width+" onkeyup='"+method+"'> &nbsp;<br>&nbsp;H :<input type='text' name='height_rollForm[]' id='RollFromH'  maxlength='5' class='txtbox rollForm_H whclass' value="+height+" onkeyup='"+method+"'></td>\n" +
                    "                            <td><input name='autoConsumption_rollForm[]' id='autoConsumption_RollFrom'  class='autoConsumption_rollForm' type='text' value='"+autoConsumption+"' readonly ></td>\n" +
                    "                            <td><input type='text' id='edited_consumpition_rollForm' name='edited_consumpition_rollForm[]' value='"+editedConsumption+"'/></td>" +
                    "<td style='display: none'><input type='hidden' id='colourName_rollForm' name='colourName_rollForm[]' value='"+json['rollForm'][i]['intColorId']+"'>" +
                    "<input type='hidden' name='techniques_rollForm[]' id='techniques_rollForm' value='"+json['rollForm'][i]['intTechniqueId']+"'>" +
                    "<input type='hidden' name='itemCode_rollForm[]' id='itemCode_rollForm' value='"+json['rollForm'][i]['intItem']+"'></td></tr>";
                $("#mainTableRollform tbody").append(newRowContent1);

            }

        }else{
            $("#mainTableRollform").css('display','none');
        }
        var countNonRollForm = Object.keys(json['nonRollFrom']).length;
        if(countNonRollForm != 0) {
            $('#mainTableNonrollform').show();
            for (i = 0; i < countNonRollForm; i++) {

                if(type ==1){

                    var consuption =json['nonRollFrom'][i]['CONSUMPTION'];

                    if(json['nonRollFrom'][i]['EDITEDWIDTH'] != null) {
                        var width = json['nonRollFrom'][i]['EDITEDWIDTH'];

                    }else{
                        var width = json['nonRollFrom'][i]['WIDTH'];
                    }
                    if(json['nonRollFrom'][i]['EDITEDHEIGHT'] != null) {
                        var height = json['nonRollFrom'][i]['EDITEDHEIGHT'];

                    }else {
                        var height = json['nonRollFrom'][i]['HEIGHT'];
                    }

                    if(json['nonRollFrom'][i]['EDITEDWIDTH'] !=null && json['nonRollFrom'][i]['EDITEDHEIGHT'] !=null){
                        var autoConsumption = json['nonRollFrom'][i]['AUTO_CONSUMPTION'];
                    }else {

                        var autoConsumption1 = (consuption/ (width*height))*(width*height);
                        autoConsumption=autoConsumption1.toFixed(8);
                        if($.isNumeric(autoConsumption1) !=true ){
                            autoConsumption=0;
                        }
                    }






                }else{
                    var consuption =json['nonRollFrom'][i]['CONSUMPTION'];

                    if(json['nonRollFrom'][i]['EDITEDWIDTH'] != null) {
                        var width = json['nonRollFrom'][i]['EDITEDWIDTH'];


                    }else{
                        var width = json['nonRollFrom'][i]['SELECTED_SIZE_WIDTH'];

                    }
                    if(json['nonRollFrom'][i]['EDITEDHEIGHT'] != null) {
                        var height = json['nonRollFrom'][i]['EDITEDHEIGHT'];


                    }else {
                        var height = json['nonRollFrom'][i]['SELECTED_SIZE_HEIGHT'];

                    }

                    //var  width =json['nonRollFrom'][i]['SELECTED_SIZE_WIDTH'];
                    //var height =json['nonRollFrom'][i]['SELECTED_SIZE_HEIGHT'];

                    var autoConsumption = json['nonRollFrom'][i]['AUTO_CONSUMPTION'];
                }

                if( json['nonRollFrom'][i]['EDITED_CONSUMPTION'] != null) {
                    var editedConsumption = json['nonRollFrom'][i]['EDITED_CONSUMPTION'];
                }else{
                    var editedConsumption =0;
                }
                var defwidth =json['nonRollFrom'][i]['WIDTH'];
                var defheight =json['nonRollFrom'][i]['HEIGHT'];
                var method ="calAutoConsumptionn("+'3,'+consuption+','+defwidth+','+defheight+','+i+")";

                //var UOM= json['nonRollFrom'][i]['UOM'];
              
				var bgColor = "#FFFFFF";
				if(editedConsumption > 0)
					bgColor = "#8FD799";
				var newRowContent = "<tr bgcolor='"+bgColor+"'><td>" + json['nonRollFrom'][i]['ColorName'] + "</td><td>" + json['nonRollFrom'][i]['techniques'] + "</td><td>" + json['nonRollFrom'][i]['itemCode'] + "</td><td>" + json['nonRollFrom'][i]['itemName'] + "</td><td>" + json['nonRollFrom'][i]['UOM'] + "</td><td>" + json['nonRollFrom'][i]['CONSUMPTION'] + "<input type='hidden' name='defconsumption_nonRollFrom[]' id='defconsumption_nonRollFrom' value='"+json['nonRollFrom'][i]['CONSUMPTION']+"'></td>" +
                    "<td>W :<input type='text' name='width_nonRollFrom[]' id='nonRollFromW' class='txtbox nonRollFrom_W whclass' maxlength='5' value="+width+" onkeyup='"+method+"'  >  &nbsp;<br>&nbsp;H :<input type='text' name='height_nonRollFrom[]' id='nonRollFromH' maxlength='5' class='txtbox nonRollFrom_H whclass' value="+height+"  onkeyup='"+method+"'></td>\n" +
                    "                            <td><input name='autoConsumption_nonRollFrom[]'  id='autoConsumption_nonRollFrom' class='autoConsumption_nonRollFrom' type='text' value='"+autoConsumption+"'   readonly ></td>\n" +
                    "                            <td><input type='text' id='edited_consumpition_nonRollFrom' name='edited_consumpition_nonRollFrom[]' value='"+editedConsumption+"'/></td>" +
                    "<td style='display: none'><input type='hidden' id='colourName_nonRollFrom' name='colourName_nonRollFrom[]' value='"+json['nonRollFrom'][i]['intColorId']+"'>" +
                    "<input type='hidden' name='techniques_nonRollFrom[]' id='techniques_nonRollFrom' value='"+json['nonRollFrom'][i]['intTechniqueId']+"'>" +
                    "<input type='hidden' name='itemCode_nonRollFromm[]' id='itemCode_nonRollFromm' value='"+json['nonRollFrom'][i]['intItem']+"'></td>"+
                    "</tr>";

                $("#mainTableNonrollform tbody").append(newRowContent);
                //console.log(json['nonRollFrom'][i]);
            }

        }else{
            $("#mainTableNonrollform").css('display','none');
        }

        var countNonDirectitem = Object.keys(json['nonDirect']).length;
        // console.log(json['nonDirect']);
        if(countNonDirectitem != 0) {
            $('#mainTableNonDirectitem').show();
            for (i = 0; i < countNonDirectitem; i++) {
                //load from  print type 1 else  load from size
                if(type ==1){

        //console.log(json['nonDirect'][i]); return false;
                    var consuption =json['nonDirect'][i]['CONSUMPTION'];

                    if(json['nonDirect'][i]['EDITEDWIDTH'] != null) {
                        var width = json['nonDirect'][i]['EDITEDWIDTH'];

                    }else{
                        var width = json['nonDirect'][i]['WIDTH'];

                    }
                    if(json['nonDirect'][i]['EDITEDHEIGHT'] != null) {
                        var height = json['nonDirect'][i]['EDITEDHEIGHT'];



                    }else {
                        var height = json['nonDirect'][i]['HEIGHT'];

                    }


                    if(json['nonDirect'][i]['EDITEDWIDTH'] !=null && json['nonDirect'][i]['EDITEDHEIGHT'] !=null){
                        var autoConsumption = json['nonDirect'][i]['AUTO_CONSUMPTION'];
                    }else {

                        var autoConsumption1 = (consuption / (width * height)) * (width * height);
                        autoConsumption = autoConsumption1.toFixed(8);
                        if ($.isNumeric(autoConsumption1) != true) {
                            autoConsumption = 0;
                        }
                    }

                }else{
                    var consuption =json['nonDirect'][i]['CONSUMPTION'];

                    if(json['nonDirect'][i]['EDITEDWIDTH'] != null) {
                        var width = json['nonDirect'][i]['EDITEDWIDTH'];
                        var defwidth =json['nonDirect'][i]['WIDTH'];

                    }else{
                        var width = json['nonDirect'][i]['SELECTED_SIZE_WIDTH'];

                    }
                    if(json['nonDirect'][i]['EDITEDHEIGHT'] != null) {
                        var height = json['nonDirect'][i]['EDITEDHEIGHT'];


                    }else {
                        var height = json['nonDirect'][i]['SELECTED_SIZE_HEIGHT'];

                    }



                    var autoConsumption = json['nonDirect'][i]['AUTO_CONSUMPTION'];
                }

                var defwidth =json['nonDirect'][i]['WIDTH'];
                var defheight =json['nonDirect'][i]['HEIGHT'];
                var method ="calAutoConsumptionn("+'4,'+consuption+','+defwidth+','+defheight+','+i+")";
                if( json['nonDirect'][i]['EDITED_CONSUMPTION'] != null) {
                    var editedConsumption = json['nonDirect'][i]['EDITED_CONSUMPTION'];
                }else{
                    var editedConsumption =0;
                }
              
				var bgColor = "#FFFFFF";
				if(editedConsumption > 0)
					bgColor = "#8FD799";
				var newRowContent = "<tr bgcolor='"+bgColor+"'><td>" + json['nonDirect'][i]['itemCode'] + "</td><td>"+ json['nonDirect'][i]['itemName']+"</td><td>" + json['nonDirect'][i]['UOM'] + "</td><td>" + json['nonDirect'][i]['CONSUMPTION'] + "<input type='hidden' name='defconsumption_nonDirect[]' id='defconsumption_nonDirect' value='"+json['nonDirect'][i]['CONSUMPTION']+"'></td>" +
                    "<td>W :<input type='text' name='width_nonDirect[]'  id='nonDirectW' class='txtbox nonDirect_W whclass' maxlength='5' value='"+width+"' onkeyup='"+method+"'>  &nbsp;<br>&nbsp;H :<input type='text' name='height_nonDirect[]' maxlength='5' id='nonDirectH' class='txtbox nonDirect_H whclass' value='"+height+"' onkeyup='"+method+"'></td>\n" +
                    "                            <td><input name='autoConsumption_nonDirect[]' id='autoConsumption_nonDirect'  class='autoConsumption_nonDirect' type='text' value="+autoConsumption+" readonly ></td>\n" +
                    "                            <td><input type='text' id='edited_consumpition_nonDirect' name='edited_consumpition_nonDirect[]' value='"+editedConsumption+"'/></td>" +
                    "<td style='display: none'><input type='hidden' id='itemCode_nonDirect' name='itemCode_nonDirect[]' value='"+json['nonDirect'][i]['intItem']+"'>" +
                    "</td>"+
                    "</tr>";


                $("#mainTableNonDirectitem tbody").append(newRowContent);
                //  console.log(json['nonDirect'][i]);
            }
        }else{
            $("#mainTableNonDirectitem").css('display','none');
        }


        /*-------- Raising alerts when width/height values change into 0----------*/

        var inkEleW = $("#inktypeW");
        setWarningAlertWhenChangeValue(inkEleW);
        var inkEleH = $("#inktypeH");
        setWarningAlertWhenChangeValue(inkEleH);
        var rollFormW = $("#RollFromW");
        setWarningAlertWhenChangeValue(rollFormW);
        var rollForH = $("#RollFromH");
        setWarningAlertWhenChangeValue(rollForH);
        var nonRollFormH = $("#nonRollFromH");
        setWarningAlertWhenChangeValue(nonRollFormH);
        var nonRollFormW = $("#nonRollFromW");
        setWarningAlertWhenChangeValue(nonRollFormW);
        var nonDirectW = $("#nonDirectW");
        setWarningAlertWhenChangeValue(nonDirectW);
        var nonDirectH = $("#nonDirectH");
        setWarningAlertWhenChangeValue(nonDirectH);



    }
    /*---END TABLE LOAD---*/





/*--SAVE MODAL DATA--*/
    var data;
    $("#save").die('click').live('click',function(){

		//saveGradingData(0);
		saveGradingData(1);

	});
  /*  $("#saveAutomatedCons").die('click').live('click',function(){

		saveGradingData(1);

	}); */
	
	
    function LoadSize() {
        var sampleNo = $('#sampleNo').val();
        var year = $('#year').val();
        var revision = $('#revision').val();
        var combo = $('#combo').val();
        var print = $('#print').val();
        var url = basepath+"grading_db.php?type=LoadSizeCombo&sampleNo="+sampleNo+"&year="+year+"&revision="+revision+"&print="+print+"&combo="+combo;
        var obj =  $.ajax({url:url,async:false});
        //var result =JSON.parse(obj['responseText']);

        //console.log(obj['responseText']);
        $("#cbosize").empty().append(obj.responseText);

        //var x= $("#cbosize").val();

        var combo = $("#cboCombo").val();
        var RevisionNo = $('#cboRevision').val();
        var year = $('#cboYear').val();
        var sampleNo = $('#cboSampleNo').val();
        var  print = $("#cboPrint").val();
        var size = $('#cbosize').val();
        var data= {type:'LoadSizeWiseTable',year:year,sampleNo:sampleNo,Revision:RevisionNo,combo:combo,print:print,size:size};
        var getData = $.ajax({
            type: 'POST',
            url: basepath + "grading_db.php?type=LoadSizeWiseTable",
            data: data,
            dataType: "json",
            success: function(json) {
                $("#mainTableNonrollform tbody").empty();
                $("#mainTableRollform tbody").empty();
                $("#mainTableInkType tbody").empty();
                $("#mainTableNonDirectitem tbody").empty();
                LoadTables(2,json);


            }
        });

    }


    //saveMaindata
    $("#saveMaindata").die('click').live('click',function() {

        var combo = $("#cboCombo").val();
        var RevisionNo = $('#cboRevision').val();
        var year = $('#cboYear').val();
        var sampleNo = $('#cboSampleNo').val();
        var print = $("#cboPrint").val();
        var size = $('#cbosize').val();
        if(size == null){
            alert("Please Add Grading Before Save !");
            return false;
        }

        var MainData = "[";
        MainData += "{";
        MainData += '"sampleNo":"' + sampleNo + '",';
        MainData += '"year":"' + year + '",';
        MainData += '"RevisionNo":"' + RevisionNo + '",';
        MainData += '"combo":"' + combo + '",';
        MainData += '"print":"' + print + '",';
        MainData += '"size":"' + size + '"';
        MainData += "}";
        MainData += "]";
        var data = '';
        data +='&MainData='+MainData;
        /*--INK TYPE TABLE--*/
        var colourName_inktype = 0;
        var inktype = 0;
        var itemCode_inktype = 0;
        var edited_consumpition_inktype = 0;
        var defconsumption_inktype = 0;
        var height_inktype = 0;
        var width_inktype = 0;
        var techniques_inktype = 0;
        var autoConsumption_inktype = 0;
        mainTableInkType = document.getElementById('mainTableInkType').rows.length-2;

        if(mainTableInkType !=0){
            var arr = "[";
            $('#mainTableInkType >tbody >tr').each(function () {
                arr += "{";
                colourName_inktype = $(this).find('#colourName_inktype').val();
                inktype = $(this).find('#inktype').val();
                itemCode_inktype = $(this).find('#itemCode_inktype').val();
                edited_consumpition_inktype = $(this).find('#edited_consumpition_inktype').val();
                defconsumption_inktype = $(this).find('#defconsumption_inktype').val();
                height_inktype = $(this).find('#inktypeH').val();
                width_inktype = $(this).find('#inktypeW').val();
                techniques_inktype = $(this).find('#techniques_inktype').val();
                autoConsumption_inktype = $(this).find('#autoConsumption_inktype').val();
                //autoConsumption_inktype =  $(this).find('#autoConsumption_inktype').val();

                if(height_inktype === '0' || width_inktype === '0'){
                    alert(validateAreaValue);
                }


                arr += '"colourName_inktype":"' + colourName_inktype + '",';
                arr += '"inktype":"' + inktype + '",';
                arr += '"itemCode_inktype":"' + itemCode_inktype + '",';
                arr += '"edited_consumpition_inktype":"' + edited_consumpition_inktype + '",';
                arr += '"defconsumption_inktype":"' + defconsumption_inktype + '",';
                arr += '"height_inktype":"' + height_inktype + '",';
                arr += '"width_inktype":"' + width_inktype + '",';
                arr += '"techniques_inktype":"' + techniques_inktype + '",';
                arr += '"autoConsumption_inktype":"' + autoConsumption_inktype + '"';


                arr += "},";

            });

            arr = arr.substr(0, arr.length - 1);

            arr += " ]";

            data+='&inkTypeMainTable='	+arr;

        }
        /*--ROLL ITEM--*/
        //mainTableRollform
        var colourName_rollForm= 0;
        var techniques_rollForm= 0;
        var itemCode_rollForm= 0;
        var width_rollForm= 0;
        var height_rollForm= 0;
        var autoConsumption_rollForm= 0;
        var edited_consumpition_rollForm= 0;

        rowCountrollForm = document.getElementById('mainTableRollform').rows.length-2;
        if(rowCountrollForm != 0) {
            var rollFormarr = "[";
            $('#mainTableRollform >tbody >tr').each(function () {

                rollFormarr += "{";
                colourName_rollForm = $(this).find('#colourName_rollForm').val();
                techniques_rollForm = $(this).find('#techniques_rollForm').val();
                itemCode_rollForm = $(this).find('#itemCode_rollForm').val();
                width_rollForm = $(this).find('#RollFromW').val();
                height_rollForm = $(this).find('#RollFromH').val();
                autoConsumption_rollForm = $(this).find('#autoConsumption_RollFrom').val();
                edited_consumpition_rollForm = $(this).find('#edited_consumpition_rollForm').val();

                if(height_rollForm === '0' || width_rollForm === '0'){
                    alert(validateAreaValue);
                }

                rollFormarr += '"colourName_rollForm":"' + colourName_rollForm + '",';
                rollFormarr += '"techniques_rollForm":"' + techniques_rollForm + '",';
                rollFormarr += '"itemCode_rollForm":"' + itemCode_rollForm + '",';
                rollFormarr += '"width_rollForm":"' + width_rollForm + '",';
                rollFormarr += '"height_rollForm":"' + height_rollForm + '",';
                rollFormarr += '"autoConsumption_rollForm":"' + autoConsumption_rollForm + '",';
                rollFormarr += '"edited_consumpition_rollForm":"' + edited_consumpition_rollForm + '"';

                rollFormarr += "},";

            });
            rollFormarr = rollFormarr.substr(0, rollFormarr.length - 1);
            rollFormarr += "]";
            //console.log(rollFormarr);  return false;
            data+='&rollFormMainTable='+rollFormarr;
        }




        /*-- NON ROLL FORM--*/
        //mainTableNonrollform
        var colourName_nonRollFrom =0;
        var techniques_nonRollFrom =0;
        var itemCode_nonRollFromm =0;
        var width_nonRollFrom =0;
        var height_nonRollFrom =0;
        var autoConsumption_nonRollFrom =0;
        var edited_consumpition_nonRollFrom =0;
        var   mainTableNonrollform = document.getElementById('mainTableNonrollform').rows.length-2;

        if(mainTableNonrollform !=0) {
            var nonRollarr = "[";
            $('#mainTableNonrollform >tbody >tr').each(function () {
                nonRollarr += "{";
                colourName_nonRollFrom = $(this).find('#colourName_nonRollFrom').val();
                techniques_nonRollFrom = $(this).find('#techniques_nonRollFrom').val();
                itemCode_nonRollFromm = $(this).find('#itemCode_nonRollFromm').val();
                width_nonRollFrom = $(this).find('#nonRollFromW').val();
                height_nonRollFrom = $(this).find('#nonRollFromH').val();
                autoConsumption_nonRollFrom = $(this).find('#autoConsumption_nonRollFrom').val();
                edited_consumpition_nonRollFrom = $(this).find('#edited_consumpition_nonRollFrom').val();

                if(height_nonRollFrom === '0' || width_nonRollFrom === '0'){
                    alert(validateAreaValue);
                }

                nonRollarr += '"colourName_nonRollFrom":"' + colourName_nonRollFrom + '",';
                nonRollarr += '"techniques_nonRollFrom":"' + techniques_nonRollFrom + '",';
                nonRollarr += '"itemCode_nonRollFromm":"' + itemCode_nonRollFromm + '",';
                nonRollarr += '"width_nonRollFrom":"' + width_nonRollFrom + '",';
                nonRollarr += '"height_nonRollFrom":"' + height_nonRollFrom + '",';
                nonRollarr += '"autoConsumption_nonRollFrom":"' + autoConsumption_nonRollFrom + '",';
                nonRollarr += '"edited_consumpition_nonRollFrom":"' + edited_consumpition_nonRollFrom + '"';
                nonRollarr += "},";

            });

            nonRollarr = nonRollarr.substr(0, nonRollarr.length - 1);

            nonRollarr += " ]";
            data+='&nonRollFormMainTable='+nonRollarr;
        }
        /*--NON DIRECT ITEMS--*/

        //mainTableNonDirectitem


        var  itemCode_nonDirect=0;
        var  width_nonDirect=0;
        var  height_nonDirect=0;
        var  autoConsumption_nonDirect=0;
        var  edited_consumpition_nonDirect=0;
        var   mainTableNonDirectitem = document.getElementById('mainTableNonDirectitem').rows.length-2;
        if(mainTableNonDirectitem !=0) {


            var nonDirectarr = "[";
            $('#mainTableNonDirectitem >tbody >tr').each(function () {

                nonDirectarr += "{";
                itemCode_nonDirect = $(this).find('#itemCode_nonDirect').val();
                width_nonDirect = $(this).find('#nonDirectW').val();
                height_nonDirect = $(this).find('#nonDirectH').val();
                autoConsumption_nonDirect = $(this).find('#autoConsumption_nonDirect').val();
                edited_consumpition_nonDirect = $(this).find('#edited_consumpition_nonDirect').val();

                if(height_nonDirect === '0' || width_nonDirect === '0'){
                    alert(validateAreaValue);
                }

                nonDirectarr += '"itemCode_nonDirect":"' + itemCode_nonDirect + '",';
                nonDirectarr += '"width_nonDirect":"' + width_nonDirect + '",';
                nonDirectarr += '"height_nonDirect":"' + height_nonDirect + '",';
                nonDirectarr += '"autoConsumption_nonDirect":"' + autoConsumption_nonDirect + '",';
                nonDirectarr += '"edited_consumpition_nonDirect":"' + edited_consumpition_nonDirect + '"';


                nonDirectarr += "},";


            });

            nonDirectarr = nonDirectarr.substr(0, nonDirectarr.length - 1);
            nonDirectarr += "]";

            data +='&nonDirectMainTable='+nonDirectarr;
        }
        // console.log(nonDirectarr); return false;


        data+= "&Request_Type=saveMaindata";

        var url = basepath+"grading_db.php";
        var obj = $.ajax({
            url:url,
            dataType: "json",
            type:'POST',
            data:data,
            async:false,
            success:function(json){
                arr=0;
                MainData=0;
                // $('#gradindg_form #save').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                //console.log(json);

                if(json.type=='1')
                {
                    alert(json.msg);


                    //console.log(json.msg);
                    //alert(json.msg);
                    //setTimeout("alertx('#save')",2000);
                }
                if(json.type=='2')
                {
                    alert(json.msg);
                }

                window.setTimeout(function() {
                    $(".alert").fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove();
                        //window.location.href='?q=1264';
                    });
                }, 4000);
            },
            error:function(xhr,status){
                $('#frmShift #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                //setTimeout("alertx()",2000);
            }
        });

    });

});

function saveGradingData(auto){
	
	
        $('#loader').removeClass('hidden');
        /*----Save Modal Data---*/

        var arr="[";
        var sampleNo = $('#sampleNo').val();
        var year = $('#year').val();
        var revision = $('#revision').val();
        var combo = $('#combo').val();
        var print = $('#print').val();
        var areaValue = 0;

        var MainData="[";
        MainData+="{";
        MainData += '"sampleNo":"'+			sampleNo +'",' 	;
        MainData += '"year":"'+			year +'",' 	;
        MainData += '"revision":"'+			revision +'",' 	;
        MainData += '"combo":"'+			combo +'",' 	;
        MainData += '"print":"'+			print +'",' 	;
        MainData += '"auto":"'+			auto +'"' 	;
		MainData+="}";
        MainData+="]";
        var  data='';
        // var $checked =$('.grading_default').find('input[type=radio]:checked');
        // var isChecked = $checked.length > 0;
        // if (!isChecked) {
        //     $('#loader').addClass('hidden');
        //     alert('Please Choose Default Size !.');
        //     return false;
        // }



        var i=0;
        var precentage =0;
        var height=0;
        var width=0;
        var  checked =0;
        rowCount = document.getElementById('myTable').rows.length-2;
        // var table = document.getElementById('myTable');
        //
        // var rowLength = table.rows.length;

        // for(var i=0; i<rowLength; i+=1){
        //     var row = table.rows[i];
        //
        //     //your code goes here, looping over every row.
        //     //cells are accessed as easy
        //
        //     var cellLength = row.cells.length;
        //     for(var y=0; y<cellLength; y+=1){
        //         var cell = row.cells[y];
        //
        //         //do something with every cell here
        //     }
        // }

        var count =1;
        $('#myTable >tbody >tr').each(function() {
        //$('#myTable >tbody >tr').each(function() {
            //$('.size_gr').each(function(){

            // alert($(this).val());

            console.log(rowCount + ' rowc\n');
            console.log(count + ' loop\n');


                arr += "{";

            var size = $(this).find('#size').val();
            //alert(size);

                if ($(this).find(".grading_default").is(":checked")) {
                    var default_size = 1;
                    checked++;
                } else {
                    var default_size = 0;
                }


                precentage = $(this).find('#precentage').val();
                height = $(this).find('#height').val();
                width = $(this).find('#width').val();

                if(size != '' && (height == '' || height =='0' || width == '' || width == '0')){
                    areaValue = 1;
                    return false;
                }

                arr += '"count":"' + i + '",';
                arr += '"size":"' + size + '",';
                arr += '"default_size":"' + default_size + '",';
                arr += '"precentage":"' + precentage + '",';
                arr += '"height":"' + height + '",';
                arr += '"width":"' + width + '"';
                arr += '},';

            i++;
            count++;
        });


        arr = arr.substr(0,arr.length-1);
        arr += " ]";

if(areaValue ==1) {
    alert('Please give vaild height and width details for sizes');
    $('#loader').addClass('hidden');
    return false;
}

if(checked !=1) {
    alert('SELECT EITHER ONE OPTION !');
    $('#loader').addClass('hidden');
    return false;
}



        data+="&arr="	+	arr+'&MainData='+MainData;
        data+= "&Request_Type=SaveModalData";
        //  console.log(data);
        /*---end---*/
        var url = basepath+"grading_db.php";
        var obj = $.ajax({
            url:url,
            dataType: "json",
            type:'POST',
            data:data,
            async:false,
            success:function(json){
                arr=0;
                MainData=0;
                $('#gradindg_form #save').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                //console.log(json);
                $('#loader').addClass('hidden');
                if(json.type=='pass')
                {   //console.log(json.msg);
                    alert(json.msg);
                    //setTimeout("alertx('#save')",2000);
                }
                if(json.type=='fail')
                {
                    alert(json.msg);
                    //setTimeout("alertx('#save')",10000);
                }
            },
            error:function(xhr,status){
                $('#frmShift #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                //setTimeout("alertx()",2000);
            }
        });
	
	
}




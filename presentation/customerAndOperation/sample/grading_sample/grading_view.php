<?php (define('UNLOCKPAGE', true) ? die('<<< Access denied >>>') : '');
/**
 * Created by PhpStorm.
 * User: Hasitha
 * Date: 1/26/2018
 * Time: 11:35 AM
 */
ini_set('display_errors', '0');
?>
<?php
$mainPath = $_SESSION['mainPath'];

$thisFilePath = $_SERVER['PHP_SELF'];

$userId = $_SESSION['userId'];
$year = $_SESSION["Year"];
$month = $_SESSION["Month"];

$programCode = 'P1262';

require_once "class/cls_permisions.php";
$objpermisionget = new cls_permisions($db);
require_once "class/tables/sys_approvelevels.php";
$obj_sys_approvelevels = new sys_approvelevels($db);
require_once "class/customerAndOperation/sample/cls_grading.php";
$obj_grading = new cls_grading($db);
?>

<html>
<head>
    <title>nsoft | Grading</title>
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />

    <link rel="stylesheet" type="text/css" href="presentation/customerAndOperation/sample/grading_sample/grading.css">
    <link rel="stylesheet" href="presentation/customerAndOperation/sample/grading_sample/bootstrap/css/bootstrap.min.css">
    <script src="presentation/customerAndOperation/sample/grading_sample/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="presentation/customerAndOperation/sample/grading_sample/grading.js"></script>
</head>
<body>

<form id="frmGrading" name="frmGrading">
    <div align="center">
        <div class="trans_layoutXL">
            <div class="trans_text">Grading wise Consumption</div>
            <table width="100%" border="0" align="center">
                <div class="well well-sm">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label">Sample Year</label>
                                <select name="cboYear" id="cboYear" class="validate[required]" style="width:100px!important;">
                                   <option> SELECT YEAR</option>
                                    <?php
                                    $d = date('Y');
                                    for($d;$d>=2012;$d--)
                                    {
                                        if($d==$sampleYear)
                                            echo "<option selected=\"selected\" id=\"$d\" >$d</option>";
                                        else
                                            echo "<option id=\"$d\" >$d</option>";
                                    }
                                    ?>
                                </select>
                            </div>

                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label">Sample No  &nbsp;&nbsp;</label>
                                <select class="empty validate[required]" name="cboSampleNo" id="cboSampleNo" style="width:100px!important;">
                                    <option>LOAD YEAR FIRST</option>
                                </select></td>
                            </div>

                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label">Revision No</label>
                                <select class="empty validate[required]" name="cboRevision" id="cboRevision" style="width:100px!important;">
                                    <option>LOAD YEAR FIRST</option>
                                </select>
                            </div>

                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label">COMBO &nbsp;&nbsp;</label><br>
                                <select class="empty validate[required]" name="cboCombo" id="cboCombo" style="width:100px!important;">
                                    <option>LOAD YEAR FIRST</option>
                                </select>
                            </div>

                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label class="control-label">PRINT &nbsp;&nbsp;&nbsp;</label><br>
                                <select class="empty validate[required]" name="cboPrint" id="cboPrint" style="width:100px!important;">
                                    <option>LOAD YEAR FIRST</option>
                                </select>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="well well-sm">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Grading Type :</label>
                                <span><input  title="W-H" type="radio" value="1" name="optradio" checked> W-H Wise </span>
                                <span style="display: none"><input  title="% wise" type="radio" value="2" name="optradio"> % Wise  </span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Add Grading</label>
                                <button type="button" id="loadGrading" class="btn btn-info btn-sm" data-toggle="modal" data-backdrop="static" data-keyboard="false">+</button>
                            </div>

                        </div>
                        <div class="col-sm-4 pull-right">
                            <div class="col-md-4 col-md-offset-4">
                                <div class="form-group">
                                    <label class="control-label">Size</label>
                                    <select class="empty" name="cbosize" id="cbosize" style="width:100px;">
                                        <option>SELECT SIZE</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container table-responsive">
                    <table id="mainTableInkType" class=" table bordered " width="60">
                        <thead>
                        <tr id="mainTableInkType_H1"><td colspan="10" bgcolor="#dce9f9">INK TYPE</td></tr>
                        <tr id="mainTableInkType_H2">
                            <th>Colour</th>
                            <th>Ink Type</th>
                            <th>Technique</th>
                            <th>Item Code</th>
                            <th>Name</th>
                            <th>UOM</th>
                            <th>Default Consumtion</th>
                            <th>% or Actual Area</th>
                            <th>Auto Consumption</th>
                            <th>Edited Consumption</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>

                        </tr>
                        </tbody>
                    </table>
                    <table id="mainTableRollform" class="bordered" width="60">
                        <thead>
                        <tr id="mainTableRollform_H1"><td colspan="16" bgcolor="#dce9f9">Roll-Form-Item</td></tr>
                        <tr id="mainTableRollform_H2">

                            <th colspan="8">Colour</th>
                            <th>Technique</th>
                            <th>Item Code</th>
                            <th>Name</th>
                            <th>UOM</th>
                            <th>Default Consumtion</th>
                            <th>% or Actual Area</th>
                            <th>Auto Consumption</th>
                            <th>Edited Consumption</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>

                        </tr>
                        </tbody>
                    </table>
                    &nbsp;
                    &nbsp;
                    &nbsp;

                    <img src="images/loading.gif" class="hidden"  name="loaderMain" id="loaderMain" alt="loaderMain" height="100" width="100">
                    <table id="mainTableNonrollform" class="bordered" width="60">
                        <thead>
                        <tr id="mainTableNonrollform_H1"><td colspan="10" bgcolor="#dce9f9">Non-Roll-Form</td></tr>
                        <tr id="mainTableNonrollform_H2">

                            <th>Colour</th>
                            <th>Technique</th>
                            <th>Item Code</th>
                            <th>Name</th>
                            <th>UOM</th>
                            <th>Default Consumtion</th>
                            <th>% or Actual Area</th>
                            <th>Auto Consumption</th>
                            <th>Edited Consumption</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>

                        </tr>
                        </tbody>

                    </table>
                    &nbsp;
                    &nbsp;
                    &nbsp;
                    <table id="mainTableNonDirectitem" class="bordered" width="60">
                        <thead>
                        <tr id="mainTableNonDirecH_1"><td colspan="10" bgcolor="#dce9f9">Non-Direct-Item</td></tr>
                        <tr id="mainTableNonDirecH_2">
                            <th>Item Code</th>
                            <th>Name</th>
                            <th>UOM</th>
                            <th>Default Consumtion</th>
                            <th>% or Actual Area</th>
                            <th>Auto Consumption</th>
                            <th>Edited Consumption</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>

                        </tr>
                        </tbody>

                    </table>
</form>
                    <br/>
                    <span>
                        <button  type="button" name="saveMaindata"  id="saveMaindata" class="btn btn-info">
                            <span class="glyphicon glyphicon-floppy-disk"></span> SAVE
                        </button>
                    </span>
                    <br/>
                    <tr style="display: none">
                        <td bgcolor="#FFFFFF">&nbsp;</td>
                        <td bgcolor="#FFFFFF">&nbsp;</td>
                        <td bgcolor="#FFFFFF">&nbsp;</td>
                        <td bgcolor="#FFFFFF">&nbsp;</td>
                    </tr>

            </table>

        </div>
    </div>


<!-- The Modal -->
<div id="myModal" class="modal" style="display: none">

    <!-- Modal content -->
    <div class="modal-content">
        <img src="images/loading.gif" class="hidden"  name="loader" id="loader" alt="loader" height="100" width="100">
        <div class="alert alert-success hidden" id=success">
            <strong>Success!</strong>
        </div>
        <span id="close_modal" class="close">&times;</span>
        <form name="gradindg_form" action="?q=1264" method="post">
            <input type="hidden" name="year" id="year" value="">
            <input type="hidden" name="sampleNo" id="sampleNo" value="">
            <input type="hidden" name="revision" id="revision" value="">
            <input type="hidden" name="combo" id="combo" value="">
            <input type="hidden" name="print" id="print" value="">
            <div class="table-responsive">
                <table id="myTable" class="bordered table" cellspacing="1" width="100%">
                    <thead>
                    <tr id="first">
                        <th>Delete</th>
                        <th>SIZE</th>
                        <th>DEFAULT</th>
                         <th style="display:none;">UOM</th>
                        <th style="display: none">Precentage(%)</th>
                        <th colspan="2">Actual</th>
                    </tr>
                    <tr style="border: none" id="second">
                        <th style="border: none">&nbsp;</th>
                        <th style="border: none">&nbsp;</th>
                        <th style="display:none;" style="border: none">&nbsp;</th>
                        <th style="border: none">&nbsp;</th>
                        <th>Graphic Height</th>
                        <th>Graphic Width</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <p>
                <input id="add" type="button" value="Insert row">
            </p>
            <div>
                <button type="button"  id="save" class="btn btn-info">
                    <span class="glyphicon glyphicon-floppy-disk"></span> SAVE GRAPHIC GRADINGS AND AUTOMATED RM CONSUMPTIONS
                </button>
            </div>
        </form>
    </div>

</div>
<!-- end Modal-->
<script>
    //onkey up load auto consumption
    function calAutoConsumptionn(type,defconsumtion,defwidth,defheight,rowId) {
    //alert('ok');
		//alert($(this).val());
        if(type == '4') {
           var rowIdC =0;
            $('#mainTableNonDirectitem >tbody >tr').not("#mainTableNonDirecH_1").not("#mainTableNonDirecH_2").each(function () {
                rowIdC=rowIdC+1;
                var width = $(this).find('#nonDirectW').val();
                var height = $(this).find('#nonDirectH').val();
                var consumption =$(this).find('#defconsumption_nonDirect').val();

                var autoConsumption1 = (consumption / (defwidth * defheight)) * (width * height);
                var autoConsumption =autoConsumption1.toFixed(8);
                //console.log(consumption + '/' + defwidth + '*' + defheight + '*' + width + '*' + height);
                if(rowId==(rowIdC-1)){
                $(this).find('#autoConsumption_nonDirect').val(autoConsumption);
                $(this).find('#edited_consumpition_nonDirect').val(autoConsumption);
                }
            })
        }else if (type == '3'){
           var rowIdC =0;
            $('#mainTableNonrollform >tbody >tr').not("#mainTableNonrollform_H1").not("#mainTableNonrollform_H2").each(function () {
			   rowIdC=rowIdC+1;
                var width = $(this).find('#nonRollFromW').val();
                var height = $(this).find('#nonRollFromH').val();
                var consumption =$(this).find('#defconsumption_nonRollFrom').val();
                //console.log('W :'+width+'\n'+'H :'+height);
                var autoConsumption1 = (consumption / (defwidth * defheight)) * (width * height);
                var autoConsumption =autoConsumption1.toFixed(8);
               // console.log(consumption + '/' + defwidth + '*' + defheight + '*' + width + '*' + height);
                              
                if(rowId==(rowIdC-1)){
                $(this).find('#autoConsumption_nonRollFrom').val(autoConsumption);
                $(this).find('#edited_consumpition_nonRollFrom').val(autoConsumption);
                }
            })

        }else if (type == '2'){
           var rowIdC =0;
            $('#mainTableRollform >tbody >tr').not("#mainTableRollform_H1").not("#mainTableRollform_H2").each(function () {
			   rowIdC=rowIdC+1;
                var width = $(this).find('#RollFromW').val();
                var height = $(this).find('#RollFromH').val();
                var consumption =$(this).find('#defconsumption_rollForm').val();

                var autoConsumption1 = (consumption / (defwidth * defheight)) * (width * height);
                var autoConsumption =autoConsumption1.toFixed(8);
               // console.log(consumption + '/' + defwidth + '*' + defheight + '*' + width + '*' + height);
               
                if(rowId==(rowIdC-1)){
                $(this).find('#autoConsumption_RollFrom').val(autoConsumption);
                $(this).find('#edited_consumpition_rollForm').val(autoConsumption);
                }
            })
        }else if (type == '1') {
           var rowIdC =0;
                  $('#mainTableInkType >tbody >tr').not("#mainTableInkType_H1").not("#mainTableInkType_H2").each(function () {
			    rowIdC=rowIdC+1;
                var width = $(this).find('#inktypeW').val();
                var height = $(this).find('#inktypeH').val();
                var consumption = $(this).find('#defconsumption_inktype').val();
               // console.log(consumption+'\n');
                var autoConsumption1 = (consumption / (defwidth * defheight)) * (width * height);
                var autoConsumption =autoConsumption1.toFixed(8);
               // console.log(consumption + '/' + defwidth + '*' + defheight + '*' + width + '*' + height);
               
                if(rowId==(rowIdC-1)){
                $(this).find('#autoConsumption_inktype').val(autoConsumption);
                $(this).find('#edited_consumpition_inktype').val(autoConsumption);
                }
            })
            }
        }

</script>
</body>
</html>

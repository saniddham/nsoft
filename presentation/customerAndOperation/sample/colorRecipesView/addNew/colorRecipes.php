<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId				= $sessions->getLocationId();


###########################
### get loading values ####
###########################
$x_graphicNo 	= $_REQUEST['graphicNo'];
$x_sampleNo 	= $_REQUEST['sampleNo'];
$x_sampleYear 	= $_REQUEST['sampleYear'];
$x_revNo 		= $_REQUEST['revNo'];
$x_combo 		= $_REQUEST['combo'];
$x_print 		= $_REQUEST['printName'];
$showExcelReportStatus = 0;
if($x_sampleYear==''){
	$x_sampleYear=date('Y'); 
}
$detailList = array();
$detailList['sampleYear'] = $x_sampleYear;
$detailList['sampleNo'] = $x_sampleNo;
$detailList['revNo'] = $x_revNo;
$detailList['combo'] = $x_combo;
$detailList['print'] = $x_print;
$detailList['graphicNo'] = $x_graphicNo;


//------------------------------------
if($x_sampleNo){
	$sql		="SELECT
					trn_sampleinfomations.intStatus,
					trn_sampleinfomations.intTechnicalStatus
					FROM `trn_sampleinfomations`
					WHERE
					trn_sampleinfomations.intSampleNo = '$x_sampleNo' AND
					trn_sampleinfomations.intSampleYear = '$x_sampleYear' AND
					trn_sampleinfomations.intRevisionNo = '$x_revNo'
					";	
	$results 	= $db->RunQuery($sql);
	$row		=mysqli_fetch_array($results);
	$sampStatus 	= $row['intStatus'];
	$techStatus 	= $row['intTechnicalStatus'];
}

//----------------2015-03-18-------
	$sql 		= "SELECT `trn_sample_color_recipes_approve`.`intApproved` from trn_sample_color_recipes_approve WHERE intSampleNo='$x_sampleNo' and intSampleYear='$x_sampleYear' and intRevisionNo='$x_revNo' and strCombo='$x_combo' and strPrintName='$x_print'";
	$result_app	=$db->RunQuery($sql);
	$row_app	=mysqli_fetch_array($result_app);
	$flag_recipe_approved	= $row_app['intApproved'];
//---------------------------------

$viewSaveButton=0;
if($x_sampleNo=='' || $x_sampleYear=='' || $x_revNo=='')
	$viewSaveButton=1;
if($flag_recipe_approved!=1 && $techStatus==1)
	$viewSaveButton=1;
	
 
//------------------------------------

 //--------------------------------------load foil item(intTypeOfPrint =  '4')
/*	  	$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strPrintName, 
					trn_sampleinfomations_details.intItem,
					mst_item.strName  
					FROM
					trn_sampleinfomations_details
					Right Join mst_item ON trn_sampleinfomations_details.intItem = mst_item.intId
					WHERE
					trn_sampleinfomations_details.intSampleNo 	=  '$x_sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$x_sampleYear' AND
					trn_sampleinfomations_details.strComboName 		=  '$x_combo' AND 
					trn_sampleinfomations_details.intRevNo =  '$x_revNo' AND
					trn_sampleinfomations_details.intTechniqueId =  '4'

				ORDER BY
					mst_item.strName ASC
				";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$foilItem 	= $row['intItem'];
*///--------------------------------------

?>
<head>
<title>Color Recipes</title>
</head>

<body>
 <form id="frmColorRecipies" name="frmColorRecipies" method="post" >
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
 </table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">COLOR RECIPES</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="22" colspan="7" class="normalfnt"><table width="100%" border="0" cellspacing="0" cellpadding="0"   class="bordered">
              <tr>
                <th width="10%" height="19">&nbsp;</th>
                <th width="8%"  class="normalfnt">Year</th>
                <th width="18%"  class="normalfnt">Graphic No</th>
                <th width="13%"  class="normalfnt">Sample No</th>
                <th width="11%"  class="normalfnt">Revision No</th>
                <th width="11%"  class="normalfnt">Combo</th>
                <th width="9%"  class="normalfnt">Print</th>
                <th width="20%">&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><select name="cboYear" id="cboYear" style="width:70px">
                  <?php
				    $d = date('Y');
				  	for($d;$d>=2012;$d--)
					{
						if($d==$x_sampleYear)
							echo "<option selected=\"selected\" id=\"$d\" >$d</option>";	
						else
							echo "<option id=\"$d\" >$d</option>";	
					}
					if($x_sampleYear!=''){
						$d=$x_sampleYear;
					}
					else{
						$d= date('Y');
					}
					
				  ?>
                </select></td>
                <td><select name="cboStyleNo" id="cboStyleNo" style="width:160px">
                  <option value=""></option>
                  <?php
					//$d = date('Y');
				     $sql = "SELECT DISTINCT
								trn_sampleinfomations.strGraphicRefNo
							FROM trn_sampleinfomations 
							WHERE
							trn_sampleinfomations.intSampleYear =  '$x_sampleYear' /*and intCompanyId = '$locationId'*/ 
							AND trn_sampleinfomations.intTechnicalStatus =1 
							ORDER BY
								trn_sampleinfomations.strGraphicRefNo ASC
					";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$strGraphicRefNo = $row['strGraphicRefNo'];
						if($x_graphicNo==$strGraphicRefNo)
							echo "<option selected=\"selected\" value=\"$strStyle\">$strGraphicRefNo</option>";
						else
							echo "<option value=\"$strGraphicRefNo\">$strGraphicRefNo</option>";
					}
				?>
                </select></td>
                <td><select name="cboSampleNo" id="cboSampleNo" style="width:120px">
                  <option value=""></option>
                  <?php
					$d = date('Y');
				   	echo $sql = "SELECT DISTINCT
							trn_sampleinfomations.intSampleNo
							FROM trn_sampleinfomations
						WHERE
							trn_sampleinfomations.intSampleYear =  '$x_sampleYear' 
							AND trn_sampleinfomations.intTechnicalStatus =1 
						ORDER BY
							trn_sampleinfomations.intSampleNo ASC
					";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$no = $row['intSampleNo'];
						if($no==$x_sampleNo)
							echo "<option selected=\"selected\" value=\"$no\">$no</option>";
						else
							echo "<option value=\"$no\">$no</option>";
					}
				?>
                </select></td>
                <td><select name="cboRevisionNo" id="cboRevisionNo" style="width:100px">
                  <option value=""></option>
                  <?php
				 $sql = "SELECT
					trn_sampleinfomations.intRevisionNo
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleYear =  '$x_sampleYear' AND
					trn_sampleinfomations.intSampleNo =  '$x_sampleNo' 
					AND trn_sampleinfomations.intTechnicalStatus =1 
				ORDER BY
					trn_sampleinfomations.intRevisionNo ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($x_revNo==$row['intRevisionNo'])
						echo "<option selected=\"selected\" value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
					else
						echo "<option value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
				}
				  ?>
                </select></td>
                <td><select name="cboCombo" id="cboCombo" style="width:100px">
                  <option value=""></option>
              <?php
				$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strComboName
					FROM trn_sampleinfomations_details
				WHERE
					trn_sampleinfomations_details.intSampleNo =  '$x_sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$x_sampleYear' 
				ORDER BY
					trn_sampleinfomations_details.strComboName ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($x_combo==$row['strComboName'])
						echo "<option selected=\"selected\" value=\"".$row['strComboName']."\">".$row['strComboName']."</option>";
					else
						echo "<option value=\"".$row['strComboName']."\">".$row['strComboName']."</option>";
				}
				  ?>
                </select></td>
                <td><select name="cboPrint" id="cboPrint" style="width:100px">
                  <option value=""></option>
                <?php
				$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strPrintName
					FROM trn_sampleinfomations_details
				WHERE
					trn_sampleinfomations_details.intSampleNo 	=  '$x_sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$x_sampleYear' AND
					trn_sampleinfomations_details.strComboName 		=  '$x_combo'
				ORDER BY
					trn_sampleinfomations_details.strPrintName ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($x_print==$row['strPrintName'])
						echo "<option selected=\"selected\" value=\"".$row['strPrintName']."\">".$row['strPrintName']."</option>";
					else
						echo "<option value=\"".$row['strPrintName']."\">".$row['strPrintName']."</option>";
				}
				  ?>
                </select></td>
                <td>&nbsp;</td>
              </tr>
            </table></td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2" class="normalfntMid"><strong>Graphic</strong></td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td align="left">&nbsp;</td>
          </tr>
          <?php
		  	#########################
			## get header details ###
			#########################
			$sql = "SELECT DISTINCT
						trn_sampleinfomations_printsize.intWidth,
						trn_sampleinfomations_printsize.intHeight,
						trn_sampleinfomations.strStyleNo,
						mst_part.strName
					FROM
						trn_sampleinfomations_printsize
						Inner Join mst_part ON mst_part.intId = trn_sampleinfomations_printsize.intPart
						Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_printsize.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_printsize.intSampleYear
					WHERE
						trn_sampleinfomations_printsize.strPrintName  =  '$x_print' AND
						trn_sampleinfomations_printsize.intSampleNo   =  '$x_sampleNo' AND
						trn_sampleinfomations_printsize.intSampleYear =  '$x_sampleYear' AND
						trn_sampleinfomations_printsize.intRevisionNo =  '$x_revNo'
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					$styleNo 		= $row['strStyleNo'];
					$print_width 	= $row['intWidth'];
					$print_height 	= $row['intHeight'];
					$partName 		= $row['strName'];
				}
          $detailList['styleNo'] = $styleNo;
          $detailList['print_width'] = $print_width;
          $detailList['print_height'] = $print_height;
          $detailList['partName'] = $partName;
		  ?>
          <tr>
            <td width="13%" height="22" class="normalfnt">Style No</td>
            <td width="21%" class="normalfnt">: <?php echo $styleNo; ?></td>
            <td colspan="2" rowspan="5" class="normalfntMid"><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:264px;height:148px;overflow:hidden" >
              <?php
			if($x_sampleNo!='')
			{
				echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"documents/sampleinfo/samplePictures/$x_sampleNo-$x_sampleYear-$x_revNo-".substr($x_print,6,1).".png\" />";	
			}
			 ?>
            </div></td>
            <td width="6%" class="normalfnt">&nbsp;</td>
            <td width="11%" class="normalfnt">&nbsp;</td>
            <td width="18%" align="left">&nbsp;</td>
            </tr>
          <tr>
            <td height="19" class="normalfnt">Graphic Placement</td>
            <td class="normalfnt">: <?php echo $partName; ?></td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td align="left">&nbsp;</td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">Graphic Size <span class="normalfntGrey">(Inch)</span></td>
            <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr class="normalfnt">
                <td width="15%" align="center"><input disabled="disabled" value="<?php echo $print_width; ?>" type="text" name="txtPrintWidth" id="txtPrintWidth" style="width:30px" /></td>
                <td width="18%">W</td>
                <td width="21%" align="center"><input disabled="disabled" value="<?php echo $print_height; ?>" type="text" name="txtPrintHeight" id="txtPrintHeight" style="width:30px" /></td>
                <td width="46%">H</td>
                </tr>
              </table></td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td align="left">&nbsp;</td>
          </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td align="left">&nbsp;</td>
          </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td align="right"><img src="images/Tadd.jpg" width="92" height="24" /></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:500px;overflow:scroll" >
          <table width="100%"   class="bordered"  >
            <tr>
              <th width="2%" height="22" >&nbsp;</th>
              <th width="11%" >Color</th>
              <th width="19%" >Technique</th>
              <th width="16%" >Ink Type</th>
              <th width="13%">Shots </th>
              <th width="10%">Weight(g)</th>
              <th width="29%">Item/Weight(g)</th>
              </tr>
             <?php
			 $sql = "SELECT
						trn_sampleinfomations_details_technical.intNoOfShots,
						trn_sampleinfomations_details_technical.dblColorWeight,
						trn_sampleinfomations_details_technical.intColorId,
						trn_sampleinfomations_details_technical.intInkTypeId as intInkType,
						mst_inktypes.strName AS inkTypeName,
						mst_colors.strName AS colorName,
						trn_sampleinfomations_details.intTechniqueId as intTechnique,
						mst_techniques.strName AS technique
					FROM
						trn_sampleinfomations_details_technical
						Inner Join mst_inktypes ON mst_inktypes.intId = trn_sampleinfomations_details_technical.intInkTypeId
						Inner Join mst_colors ON mst_colors.intId = trn_sampleinfomations_details_technical.intColorId
						Inner Join trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_details_technical.intSampleNo AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_details_technical.intRevNo AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_details_technical.strPrintName AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_details_technical.strComboName AND trn_sampleinfomations_details.intColorId = trn_sampleinfomations_details_technical.intColorId
						Inner Join mst_techniques ON mst_techniques.intId = trn_sampleinfomations_details.intTechniqueId
					WHERE
						trn_sampleinfomations_details_technical.intSampleNo =  '$x_sampleNo' AND
						trn_sampleinfomations_details_technical.intSampleYear =  '$x_sampleYear' AND
						trn_sampleinfomations_details_technical.intRevNo =  '$x_revNo' AND
						trn_sampleinfomations_details_technical.strPrintName =  '$x_print' AND
						trn_sampleinfomations_details_technical.strComboName =  '$x_combo' 
						group by 
						trn_sampleinfomations_details_technical.intColorId,
						trn_sampleinfomations_details.intTechniqueId,
						trn_sampleinfomations_details_technical.intInkTypeId
				";
				$result = $db->RunQuery($sql);
				$detailArrayIndex = 0;
				while($row=mysqli_fetch_array($result))
				{
				    $showExcelReportStatus = 1;
				    $details = array();
				    $details['color'] = $row['colorName'];
				    $details['technique'] = $row['technique'];
				    $details['inkType'] = $row['inkTypeName'];
				    $details['shots'] = $row['intNoOfShots'];
				    $details['weight'] = $row['dblColorWeight'];

			 ?>
            <tr class="normalfnt dataRow" >
              <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
              <td bgcolor="#FFFFFF" id="<?Php echo $row['intColorId']; ?>"><?php echo $row['colorName'] ?></td>
              <td bgcolor="#FFFFFF" id="<?php echo $row['intTechnique']; ?>"><?php echo $row['technique'] ?></td>
              <td bgcolor="#FFFFFF" id="<?php echo $row['intInkType']; ?>"><?php echo $row['inkTypeName'] ?></td>
              <td bgcolor="#FFFFFF" class="normalfntMid"><?php echo $row['intNoOfShots'] ?></td>
              <td bgcolor="#FFFFFF" class="normalfntMid"><?php echo $row['dblColorWeight'] ?></td>
              <td bgcolor="#FFFFFF"><table  width="287" bgcolor="#FFFFFF" class="normalfnt" cellpadding="0" cellspacing="1">
              <?php
			  	$sql2 = "SELECT
							trn_sample_color_recipes.intItem,
							trn_sample_color_recipes.dblWeight
							
					FROM trn_sample_color_recipes
					WHERE
					trn_sample_color_recipes.intSampleNo =  '$x_sampleNo' AND
					trn_sample_color_recipes.intSampleYear =  '$x_sampleYear' AND
					trn_sample_color_recipes.intRevisionNo =  '$x_revNo' AND
					trn_sample_color_recipes.strCombo =  '$x_combo' AND
					trn_sample_color_recipes.strPrintName =  '$x_print' AND
					trn_sample_color_recipes.intColorId =  '".$row['intColorId']."' AND
					intInkTypeId='".$row['intInkType']."' AND
					intTechniqueId='".$row['intTechnique']."'
					";
				$result2 = $db->RunQuery($sql2);
				$recCount = mysqli_num_rows($result2);
				$weightIndex = 0;
				if($recCount>0)
				{
					while($row2=mysqli_fetch_array($result2))
					{
					    $weightDetails = array();
						$itemId = $row2['intItem'];
						$weight = $row2['dblWeight'];
                        $weightDetails['itemId'] = $itemId;
						$weightDetails['weight']=$weight;
			  ?>
              
                <tr >
                  <td width="236" bgcolor="#FFFFFF"><select class="cboItem" style="width:200px" id="cboItem" name="cboItem" >
                  <option value=""></option>
                <?php
				$sql1 = "SELECT mst_item.strName AS itemName,mst_item.intId	FROM mst_item ORDER BY itemName ASC ";
				$result1 = $db->RunQuery($sql1);
				while($row1=mysqli_fetch_array($result1))
				{
					if($itemId==$row1['intId']) {
                        $weightDetails['itemName'] = $row1['itemName'];
                        echo "<option selected=\"selected\" value=\"" . $row1['intId'] . "\">" . $row1['itemName'] . "</option>";
                    }
					else
						echo "<option value=\"".$row1['intId']."\">".$row1['itemName']."</option>";
				}
				  ?>
                  </select></td>
                  <td width="100" bgcolor="#FFFFFF"><input value="<?php echo $weight; ?>" class="normalfntRight"  type="text" name="txtWeight" id="txtWeight" style="width:60px" /></td>
                </tr>
                
        
              <?php
                        $details['weightInfo'][$weightIndex] = $weightDetails;
                        $weightIndex ++;
					}
				   }
				   else
				   {
					 ?>
                     	<tr >
                  <td width="236" bgcolor="#FFFFFF"><select class="cboItem" style="width:200px" id="cboItem" name="cboItem" >
                  <option value=""></option>
                <?php
				$sql1 = "SELECT mst_item.strName AS itemName,mst_item.intId	FROM mst_item ORDER BY itemName ASC ";
				$result1 = $db->RunQuery($sql1);
				while($row1=mysqli_fetch_array($result1))
						echo "<option value=\"".$row1['intId']."\">".$row1['itemName']."</option>";
				  ?>
                  </select></td>
                  <td width="100" bgcolor="#FFFFFF"><input class="normalfntRight"  type="text" name="txtWeight" id="txtWeight" style="width:60px" /></td>
                </tr>
                     <?php  
					}
				   ?>
                   <tr >
                  <td bgcolor="#FFFFFF"><img class="mouseover" id="butAddRow" src="images/add.png" width="16" height="16" /></td>
                  <td bgcolor="#FFFFFF">&nbsp;</td>
                  </tr>
                </table>
                   <?php
                   $detailList['records'][$detailArrayIndex] = $details;
                   $detailArrayIndex++;
				}
                   $_SESSION['COLOR_RECIPE_DATA'] = $detailList;
			  ?>
            </table>
          </div></td>
      </tr>
<tr>
    <td align="center"><img src="images/Treport_exel.png" style="display:<?php if($showExcelReportStatus == 1) echo "inline"; else echo "none";?>" border="0"  alt="view report" id="butReport" name="butReport"  height="30" width="100"></td>
</tr>

      </table></td>
    </tr>
  </table>

  </div>
  </div>

</form>
</body>
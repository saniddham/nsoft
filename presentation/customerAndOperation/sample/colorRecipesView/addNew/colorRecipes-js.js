// JavaScript Document
var basePath	="presentation/customerAndOperation/sample/colorRecipesView/addNew/";
 
$(document).ready(function() {
	
	$('#frmColorRecipies #butNew_foil').die('click').live('click',addNewRow);
 	
$('#frmColorRecipies .technique').change();

$('#frmColorRecipies #cboYear').die('change').live('change',function(){
	
	loadSampleNos();
	
});
	
$('#frmColorRecipies #cboYear').die('change').live('change',function(){
	window.location.href  =  "?q=1183&sampleYear="+$('#cboYear').val();
	//loadSampleNos();
});
	
$('#frmColorRecipies #cboStyleNo').die('change').live('change',function(){
		
	loadSampleNos();
	
	var intSampleNo = $('#cboSampleNo').val();
	//$('#frmSampleInfomations').get(0).reset();
	$('#cboSampleNo').val(intSampleNo);
	$('#divPicture').html('');
	return;
});
$('#frmColorRecipies #cboSampleNo').die('change').live('change',function(){
		
		loadRevisionNo();
		
		var intSampleNo = $('#cboSampleNo').val();
		//$('#frmSampleInfomations').get(0).reset();
		$('#cboSampleNo').val(intSampleNo);
		$('#divPicture').html('');
		return;
});

$('#frmColorRecipies #cboRevisionNo').die('click').live('change',function(){
	loadCombo();
		
});

$('#frmColorRecipies #cboCombo').die('change').live('change',function(){
	loadPrint();
		
});

    $('#butReport').die('click').live('click', viewExcelReport);

    //----------------------------
		$('#frmColorRecipies .costPerInch').die('keyup').live('keyup',function(){
			calTotCostPrice(this);
		});
		//----------------------------
		$('#frmColorRecipies .usage').die('keyup').live('keyup',function(){
			calTotCostPrice(this);
		});
		//----------------------------
		$('.technique').die('change').live('change',function(){
			loadItem(this);
		});
		//----------------------------
		$('#frmColorRecipies .foil').die('change').live('change',function(){
			techniqueCalculations(this);
		});
		//----------------------------
		$('#frmColorRecipies .printWidth').die('keyup').live('keyup',function(){
			techniqueCalculations(this);
		});
		//----------------------------
		$('#frmColorRecipies .printHeight').die('keyup').live('keyup',function(){
			techniqueCalculations(this);
		});
		//----------------------------
		$('#frmColorRecipies .qty').die('keyup').live('keyup',function(){
			techniqueCalculations(this);
		});
		//----------------------------
		$('#frmColorRecipies .delImg').die('click').live('click',function(){
			$(this).parent().parent().remove();
		});
		//----------------------------	

function loadRevisionNo()
{
	var url = basePath+"colorRecipes-db-get.php?requestType=loadRevisionNo&sampleNo="+$('#cboSampleNo').val()+"&sampleYear="+$('#cboYear').val();
	var obj = $.ajax({url:url,async:false});	
	$('#cboRevisionNo').html(obj.responseText);
	$('#cboCombo').html('');	
	$('#cboPrint').html('');	
}
function loadSampleNos()
{
	var url = basePath+"colorRecipes-db-get.php?requestType=loadSampleNo&styleNo="+URLEncode($('#cboStyleNo').val())+"&sampleYear="+$('#cboYear').val();
	var obj = $.ajax({url:url,async:false});	
	$('#cboSampleNo').html(obj.responseText);	
	$('#cboRevisionNo').html('');	
	$('#cboCombo').html('');	
	$('#cboPrint').html('');	
	
}
function loadCombo()
{
	var url = basePath+"colorRecipes-db-get.php?requestType=loadCombo&sampleNo="+URLEncode($('#cboSampleNo').val())+"&sampleYear="+$('#cboYear').val()+"&revNo="+$('#cboRevisionNo').val();
	var obj = $.ajax({url:url,async:false});	
	$('#cboCombo').html(obj.responseText);	
	$('#cboPrint').html('');
}
function loadPrint()
{
	var url = basePath+"colorRecipes-db-get.php?requestType=loadPrint&sampleNo="+URLEncode($('#cboSampleNo').val())+"&sampleYear="+$('#cboYear').val()+"&revNo="+$('#cboRevisionNo').val()+"&combo="+$('#cboCombo').val();
	var obj = $.ajax({url:url,async:false});	
	$('#cboPrint').html(obj.responseText);	
}
	
	
/*data load part*/
$('#cboPrint').change(function(){
	window.location.href = "?q=1183&graphicNo="+URLEncode($('#cboStyleNo').val())
						+'&sampleNo='+$('#cboSampleNo').val()	
						+'&sampleYear='+$('#cboYear').val()	
						+'&revNo='+$('#cboRevisionNo').val()	
						+'&combo='+$('#cboCombo').val()	
						+'&printName='+$('#cboPrint').val()	
});
	
	
$('#butAddRow').die('click').live('click',function(){
	$(this).parent().parent().before('<tr>'+$(this).parent().parent().parent().find('tr:eq(0)').html()+'</tr>');
});
	
//----------------------------
	
//if ($('#frmSampleInfomations').validationEngine('validate'))   

$('#butSave').die('click').live('click',function(){
	var arrRecipe = '';
	var arrFoil = '';
	var arrSpecial = '';
	var data='';
	var i =0;
	
	var sampleNo 		= 	$('#cboSampleNo').val();
	var sampleYear 		= 	$('#cboYear').val();
	var revisionNo 		= 	$('#cboRevisionNo').val();
	var combo 			=	$('#cboCombo').val();
	var printName 		=	$('#cboPrint').val();
	
	$('.cboItem').each(function(){
		var itemId 		= $(this).val();
		
		if(itemId!='')
		{
			var colorId 	= $(this).parent().parent().parent().parent().parent().parent().find('td:eq(1)').attr('id');
			var techniqueId = $(this).parent().parent().parent().parent().parent().parent().find('td:eq(2)').attr('id');
			var inkTypeId 	= $(this).parent().parent().parent().parent().parent().parent().find('td:eq(3)').attr('id');
			var weight 		= $(this).parent().parent().find('#txtWeight').val();
			arrRecipe +=(i++ == 0?'':',' )+'{"color":"'+colorId+'","techId":"'+techniqueId+'","inkTypeId":"'+inkTypeId+'","weight":"'+weight+'","sampleNo":"'+sampleNo+'","sampleYear":"'+sampleYear+'","revisionNo":"'+revisionNo+'","combo":"'+combo+'","printName":"'+printName+'","itemId":"'+itemId+'"}';
		}
	});
	//-----------------
	var itemId = '';
	var j=0;
	i=0;
	
		var techniqueId = 	'';
		var itemId 		= 	'';
		var width 		= 	'';
		var height 		= 	'';
		var qty 		= 	'';
		var cuttingSide ='';
		var meters 		= 0;
		
	$('.meters').each(function(){
		//alert($(this).parent().find('.technique').val());
		j++;
		if(j%2!=0){
			//alert($(this).parent().find('.technique').val());
		 techniqueId 	= 	$(this).parent().find('.technique').val();
		 itemId 		= 	$(this).parent().find('.foil').val();
		 width 			= 	$(this).parent().find('.printWidth').val();
		 height 		= 	$(this).parent().find('.printHeight').val();
		 qty 			= 	$(this).parent().find('.qty').val();
		 cuttingSide 	=	'w';
		 meters 		= 	$(this).parent().find('.meters').html();
		}
		else{
			if(parseFloat(meters)>parseFloat($(this).parent().find('.meters').html())){
			 	meters 		= $(this).parent().find('.meters').html();
				cuttingSide 	=	'h';
			}
		}
		
		if(itemId!='' && j%2==0)
		{
			
			arrFoil +=(i++ == 0?'':',' )+'{"techniqueId":"'+techniqueId+'","itemId":"'+itemId+'","width":"'+width+'","height":"'+height+'","qty":"'+qty+'","cuttingSide":"'+cuttingSide+'","meters":"'+meters+'","sampleNo":"'+sampleNo+'","sampleYear":"'+sampleYear+'","revisionNo":"'+revisionNo+'","combo":"'+combo+'","printName":"'+printName+'"}';
			
			 techniqueId 	= 	'';
			 itemId 		= 	'';
			 width 			= 	'';
			 height 		= 	'';
			 qty 			= 	'';
			 cuttingSide 	=	'';
			 meters 		= 	0;
		}
	});
	//-------------------
	var itemId = '';
	i=0;
	$('.itemRM').each(function(){
		var techniqueId = 	$(this).parent().parent().find('.techniqueRM').val();
		var itemId 		= 	$(this).parent().parent().find('.itemRM').val();
		var qty 		= 	$(this).parent().parent().find('.qtyRM').val();
		
		if(itemId!='')
		{
			arrSpecial +=(i++ == 0?'':',' )+'{"techniqueId":"'+techniqueId+'","itemId":"'+itemId+'","qty":"'+qty+'","sampleNo":"'+sampleNo+'","sampleYear":"'+sampleYear+'","revisionNo":"'+revisionNo+'","combo":"'+combo+'","printName":"'+printName+'"}';
		}
	});
	
	//alert(arrRecipe);
		// SAVE AJAX PART //
		var arrHeader ='{"sampleNo":"'+sampleNo+'","sampleYear":"'+sampleYear+'","revisionNo":"'+revisionNo+'","combo":"'+combo+'","printName":"'+printName+'"}';
		
		data  = 'arrRecipe=['+ arrRecipe +']';
		data += '&arrFoil=['+ arrFoil +']';
		data += '&arrSpecial=['+ arrSpecial +']';
		data += '&arrHeader=['+ arrHeader +']';
		var url 	= basePath+"colorRecipes-db-set.php?requestType=saveDetails";
		$.ajax({
			url:url,
			data:data,
			type:'post',
			async:false,
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmColorRecipies #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertx()",1000);
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmColorRecipies #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
   });
	
	$('#butNew').die('click').live('click',function(){
		window.location.href = '?q=1183';
	});

});

//----------------------------------------------------------------------
function calTotCostPrice(obj)
{
		var printWidth = document.getElementById('txtPrintWidth').value;
		var printHeight = document.getElementById('txtPrintHeight').value;
	 	var costPerInch = $(obj).parent().parent().find('.costPerInch').val();
	 	var usage = $(obj).parent().parent().find('.usage').val();
	//	var totCost=qty*costPerInch*usage/100;
		var totCost=printWidth*printHeight*costPerInch*usage/100;
		//alert(totCost);
		$(obj).parent().parent().find('.totCost').text(totCost);
	
}
//----------------------------------------------------------------------
function loadItem(obj)
{
	 	var technique = $(obj).parent().parent().find('.technique').val();
	 	var sampleNo =$('#cboSampleNo').val();
	 	var sampleYear = $('#cboYear').val();
	 	var revNo = $('#cboRevisionNo').val();
	 	var combo = $('#cboCombo').val();
	 	var printName = $('#cboPrint').val();
		var url 		= basePath+"colorRecipes-db-get.php?requestType=loadItem";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"technique="+technique+"&sampleNo="+sampleNo+"&revNo="+revNo+"&combo="+combo+"&sampleYear="+sampleYear+"&printName="+printName,
			async:false,
			success:function(json){
				var itemId=json.itemId;
				 $(obj).parent().parent().find('.foil').val(json.itemId)
				 //	if(itemId){	
					  $(obj).parent().parent().find('.foil').change();
				//	}
			}
		});
}
//----------------------------------------------------------------------
function techniqueCalculations(obj)
{
	 	var foil = $(obj).parent().parent().find('.foil').val();
		var url 		= basePath+"colorRecipes-db-get.php?requestType=loadMeasurements";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"foil="+foil,
			async:false,
			success:function(json){
				foilWidth=json.width;
				foilHeight=json.height;
				foilcostPerMtr=json.price;
			}
		});
	
		var printWidth = $(obj).parent().parent().find('.printWidth').val();
		var printHeight = $(obj).parent().parent().find('.printHeight').val();
	 	var qty = $(obj).parent().parent().find('.qty').val();
		
		//------------------------------
		var pcs=Math.floor((foilHeight/printWidth))
		if(printWidth==0){
			pcs=0;
		}
		//alert(printWidth);
		$(obj).parent().parent().find('.pcsFrmHeight').text((foilHeight/printWidth).toFixed(4));
		$(obj).parent().parent().find('.pcs').text(pcs);
		var mtrs=((printHeight/pcs)*qty)/39.5;
		if(pcs==0){
			mtrs=0;
		}
		$(obj).parent().parent().find('.meters').text(mtrs.toFixed(4));
		$(obj).parent().parent().find('.cost').text((mtrs*foilcostPerMtr).toFixed(4));
		
		//-------------------
		var pcs=Math.floor((foilHeight/printHeight));
		if(printWidth==0){
			pcs=0;
		}
		$(obj).parent().parent().next().find('.pcsFrmHeight').text((foilHeight/printHeight).toFixed(4));
		$(obj).parent().parent().next().find('.pcs').text(pcs);
		var mtrs=((printWidth/pcs)*qty)/39.5;
		if(pcs==0){
			mtrs=0;
		}
		$(obj).parent().parent().next().find('.meters').text(mtrs.toFixed(4));
		$(obj).parent().parent().next().find('.cost').text((mtrs*foilcostPerMtr).toFixed(4));
}
//----------------------------------------------------------------------
function alertx()
{
	$('#frmColorRecipies #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmColorRecipies #butDelete').validationEngine('hide')	;
}

function closePopUp(){
	
}
//----------------------------------------------

function addNewRow()
{
	var tr1 = $(this).parent().parent().prev().html();
	var tr2 = $(this).parent().parent().prev().prev().html();
	
	$(this).parent().parent().before('<tr>'+tr2+'</tr><tr>'+tr1+'</tr>');
	//$(this).parent().parent().before(tr1);
	//alert(tr1);
}

function viewExcelReport(){

    var url =basePath + 'colorRecipesExcel.php';
    window.open(url);
}
<?php 

	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];

	include "{$backwardseperator}dataAccess/Connector.php";
	
	$requestType 			= $_REQUEST['requestType'];
	
	if($requestType=='loadRevisionNo')
	{
		$sampleNo 			= $_REQUEST['sampleNo'];
		$sampleYear 		= $_REQUEST['sampleYear'];
		
		$sql = "SELECT
					trn_sampleinfomations.intRevisionNo
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations.intSampleNo =  '$sampleNo' 
					AND trn_sampleinfomations.intTechnicalStatus =1 
				ORDER BY
					trn_sampleinfomations.intRevisionNo ASC
				";
		$result = $db->RunQuery($sql);
		echo "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			echo "<option value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
		}
	}
	//loadSampleNo
	elseif($requestType=='loadSampleNo')
	{
		$styleNo 			= $_REQUEST['styleNo'];
		$sampleYear 		= $_REQUEST['sampleYear'];
		
		if($styleNo!='')
			$para = " and trn_sampleinfomations.strGraphicRefNo =  '$styleNo'";
		$sql = "SELECT distinct
					trn_sampleinfomations.intSampleNo
				FROM trn_sampleinfomations
				WHERE
					
					trn_sampleinfomations.intSampleYear =  '$sampleYear' 
					 $para 
				AND trn_sampleinfomations.intTechnicalStatus =1 	 
				order by intSampleNo
				";
		$result = $db->RunQuery($sql);
		echo "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			echo "<option value=\"".$row['intSampleNo']."\">".$row['intSampleNo']."</option>";
		}
	}
	elseif($requestType=='loadCombo')
	{
		$sampleNo 			= $_REQUEST['sampleNo'];
		$sampleYear 		= $_REQUEST['sampleYear'];
		$revNo				= $_REQUEST['revNo'];
		
		$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strComboName
					FROM trn_sampleinfomations_details
				WHERE
					trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear'
				ORDER BY
					trn_sampleinfomations_details.strComboName ASC
				";
		$result = $db->RunQuery($sql);
		echo "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			echo "<option value=\"".$row['strComboName']."\">".$row['strComboName']."</option>";
		}
	}
	elseif($requestType=='loadPrint')
	{
		$sampleNo 			= $_REQUEST['sampleNo'];
		$sampleYear 		= $_REQUEST['sampleYear'];
		$revNo				= $_REQUEST['revNo'];
		$combo				= $_REQUEST['combo'];
		
		$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strPrintName
					FROM trn_sampleinfomations_details
				WHERE
					trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_details.strComboName 		=  '$combo'
				ORDER BY
					trn_sampleinfomations_details.strPrintName ASC
				";
		$result = $db->RunQuery($sql);
		echo "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			echo "<option value=\"".$row['strPrintName']."\">".$row['strPrintName']."</option>";
		}
	}
	else if($requestType=='loadMeasurements')
	{
		$foil  = $_REQUEST['foil'];
		 $sql = "SELECT
				mst_item.foil_width,
				mst_item.foil_height, 
				mst_item.dblLastPrice  
				FROM
				mst_item
				WHERE
				mst_item.intId =  '$foil'";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);

			$response['width'] 	= $row['foil_width'];
			$response['height'] = $row['foil_height'];
			$response['price'] = $row['dblLastPrice'];
			
		echo json_encode($response);
	}
	else if($requestType=='loadItem')
	{
		$technique  = $_REQUEST['technique'];
		$sampleNo 			= $_REQUEST['sampleNo'];
		$sampleYear 		= $_REQUEST['sampleYear'];
		$revNo				= $_REQUEST['revNo'];
		$combo				= $_REQUEST['combo'];
		
	  	$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strPrintName, 
					trn_sampleinfomations_details.intItem,
					mst_item.strName  
					FROM
					trn_sampleinfomations_details
					Right Join mst_item ON trn_sampleinfomations_details.intItem = mst_item.intId
					WHERE
					trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_details.strComboName 		=  '$combo' AND 
					trn_sampleinfomations_details.intRevNo =  '$revNo' AND
					trn_sampleinfomations_details.intTechniqueId =  '$technique'

				ORDER BY
					mst_item.strName ASC
				";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);

		$response['itemId'] 	= $row['intItem'];
			
		echo json_encode($response);
	}
?>
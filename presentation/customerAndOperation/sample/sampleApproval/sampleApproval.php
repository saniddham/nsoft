<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
 	$companyId = $_SESSION['CompanyID'];
	$thisFilePath =  $_SERVER['PHP_SELF'];
 	
	$intUserId = $_REQUEST['cboUser'];
	$intStatus = $_REQUEST['cboStatus'];
	
?>
 <head>
 <title>SAMPLE APPROVAL FROM CUSTOMER</title>
<?php
include "include/javascript.html";
?>
 <script type="application/javascript" src="presentation/customerAndOperation/sample/sampleApproval/sampleApproval-js.js"></script>
<script type="text/javascript" language="javascript">
 function pageSubmit()
{
	document.getElementById('frmSampleApproval').submit();	
}
function clearValue()
{
	document.getElementById('txtSearchValue').value = '';
	document.getElementById('txtSearchValue').focus();
}
</script>

</head>

<body>
<form id="frmSampleApproval" name="frmSampleApproval" method="post" autocomplete="off" >
 <div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">SAMPLE APPROVAL FROM CUSTOMER
		    <select onChange="pageSubmit();" style="width:120px" name="cboStatus" id="cboStatus">
		      <option <?php echo ($intStatus==0?'selected':'') ?> value="0">Pending</option>
		      <option <?php echo ($intStatus==1?'selected':'') ?> value="1">Approved</option>
	        </select>
		  </div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="99%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="1" cellspacing="0" class="tableBorder_allRound">
          <tr>
            <td width="14%" height="22" class="normalfnt"><span class="normalfntMid">Searching Option</span></td>
            <td width="19%" ><select name="cboSearchOption" id="cboSearchOption" onChange="clearValue();" style="width:150px">
              <option value="">&nbsp;</option>
              <option value="intSampleNo" <?php echo($optionValue=='intSampleNo'?'selected':'') ?> >Sample No</option>
              <option value="strStyleNo" <?php echo($optionValue=='strStyleNo'?'selected':'') ?>>Style No</option>
              <option value="strName" <?php echo($optionValue=='strName'?'selected':'') ?>>Customer</option>
              <option value="dtDate" <?php echo($optionValue=='dtDate'?'selected':'') ?>>Date</option>
            </select></td>
            <td width="16%" class="normalfnt"><input value="<?php echo $searchValue; ?>" type="text" name="txtSearchValue" id="txtSearchValue" /></td>
            <td width="16%"><span class="normalfnt"><a href="#"><img src="images/search.png" width="20" height="20" onClick="pageSubmit();" /></a></span></td>
            <td width="17%" class="normalfnt">&nbsp;</td>
            <td width="18%">&nbsp;</td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">Create User</td>
            <td><span class="normalfnt">
              <select onChange="pageSubmit();" name="cboUser" id="cboUser"  style="width:150px">
                <option value=""></option>
                <?php
					$sql = "SELECT
								sys_users.intUserId,
								sys_users.strUserName
							FROM
								sys_users
							
							";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intUserId']==$intUserId)
							echo "<option selected value=\"".$row['intUserId']."\">".$row['strUserName']."</option>";
						else
							echo "<option value=\"".$row['intUserId']."\">".$row['strUserName']."</option>";
					}
				?>
              </select>
            </span></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td><?php if($intStatus==0){ ?>
              <img src="images/Tapprove.png" width="92" height="24" class="mouseover" id="butApprove" />
              <?php }?></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:400px;overflow:auto"><table id="tblMain" bgcolor="#666666"  width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr class="gridHeader">
            <td width="10%" height="25">Sample No</td>
            <td width="8%">Date</td>
            <td width="12%">Graphic No</td>
            <td width="30%">Customer</td>
            <td width="4%">View</td>
            </tr>
          <?php

		 if($intStatus==1)
		 { $sql = "	SELECT DISTINCT
						trn_sampleinfomations.intSampleNo,
						trn_sampleinfomations.intSampleYear,
						trn_sampleinfomations.dtDate,
						trn_sampleinfomations.strGraphicRefNo
					FROM
						trn_sampleinfomations
					where concat(intSampleYear,trn_sampleinfomations.intSampleNo) in (select concat(intSampleYear,intSampleNo) from trn_sampleinfomations_customer_approved ) 
					";
		 }
		 elseif($intStatus==0)
		 {
			$sql = "SELECT DISTINCT
trn_sampleinfomations_details.intSampleNo,
trn_sampleinfomations_details.intSampleYear,
trn_sampleinfomations.dtDate,
trn_sampleinfomations.strGraphicRefNo
FROM
trn_sampleinfomations_details
Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_details.intSampleYear
where concat(trn_sampleinfomations_details.intSampleYear, trn_sampleinfomations_details.intSampleNo,trn_sampleinfomations_details.strPrintName) not in( select concat(intSampleYear,intSampleNo,strPrintName) from trn_sampleinfomations_customer_approved  )
";	 
		 }
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				$sampleNo 	= $row['intSampleNo'];
				$sampleYear = $row['intSampleYear'];
				
				
				////////////// find revisions  //////////////////////////
				$sql1 = "SELECT
							trn_sampleinfomations.strGraphicRefNo,
							mst_customer.strName
						FROM
						trn_sampleinfomations
						Inner Join mst_customer ON mst_customer.intId = trn_sampleinfomations.intCustomer
						WHERE
							trn_sampleinfomations.intSampleNo =  '$sampleNo' AND
							trn_sampleinfomations.intSampleYear =  '$sampleYear'
						";
				$result1 = $db->RunQuery($sql1);
				while($row1=mysqli_fetch_array($result1))
				{
					$customer 			= $row1['strName'];
					$strGraphicRefNo 	= $row1['strGraphicRefNo'];
				}
				////////////////////////////////////////////////////////
		  ?>
          <tr class="normalfnt">
            <td bgcolor="#FFFFFF" id="<?php  echo $row['intSampleNo']; ?>"><?php echo $row['intSampleNo'].'/'.$row['intSampleYear']; ?></td>
            <td bgcolor="#FFFFFF" id="<?php  echo $row['intSampleYear']; ?>"><?php echo $row['dtDate']; ?></td>
            <td bgcolor="#FFFFFF"><?php echo $strGraphicRefNo; ?></td>
            <td bgcolor="#FFFFFF"><?php echo $customer; ?></td>
            <td bgcolor="#FFFFFF" class="normalfntMid"><a style="text-decoration:none" href="presentation/customerAndOperation/sample/Report/sampleReport.php?no=<?php echo $row['intSampleNo']; ?>&amp;year=<?php echo $row['intSampleYear'] ?>&amp;revNo=<?php echo $row['intRevisionNo']; ?>" target="_blank">more</a></td>
            </tr>
          <tr class="normalfnt">
            <td bgcolor="#FFFFFF" id="<?php  echo $row['intSampleNo']; ?>2">&nbsp;</td>
            <td colspan="4" bgcolor="#FFFFFF" id="<?php  echo $row['intSampleYear']; ?>2"><table class="tblSubGrid" bgcolor="#0099FF" width="54%" border="0" cellspacing="1" cellpadding="0">
              <tr>
                <td width="36%" bgcolor="#BBC9AD" class="normalfntMid" id="<?php echo $row['intSampleNo']; ?>">Print Name</td>
                <td width="31%" bgcolor="#BBC9AD" class="normalfnt" id="<?php echo $row['intSampleYear']; ?>">Combo</td>
                <td width="18%" bgcolor="#BBC9AD" class="normalfnt">Revision No</td>
                <td width="15%" bgcolor="#BBC9AD" class="normalfntMid">Select</td>
                </tr>
              <?php
/*			  	SELECT DISTINCT
							trn_sampleinfomations_printsize.strPrintName
						FROM
							trn_sampleinfomations_printsize
							Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_printsize.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_printsize.intSampleYear AND trn_sampleinfomations_printsize.intRevisionNo = trn_sampleinfomations.intRevisionNo
						WHERE
							trn_sampleinfomations_printsize.intSampleNo =  '$sampleNo' AND
							trn_sampleinfomations_printsize.intSampleYear =  '$sampleYear' AND
							trn_sampleinfomations.intStatus =  '1' */
							if($intStatus==0)
								$para = '';
							else
								$para = ' NOT ';
								
								
			  	if($intStatus==0)
				{
				$sql ="
							SELECT DISTINCT
								trn_sampleinfomations_details.strPrintName,
								trn_sampleinfomations_customer_approved.strPrintName AS print2
							FROM
								trn_sampleinfomations_details
								Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_details.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_details.intRevNo
								Left Join trn_sampleinfomations_customer_approved ON trn_sampleinfomations_customer_approved.intSampleNo = trn_sampleinfomations_details.intSampleNo AND trn_sampleinfomations_customer_approved.intSampleYear = trn_sampleinfomations_details.intSampleYear AND trn_sampleinfomations_customer_approved.strPrintName = trn_sampleinfomations_details.strPrintName
								
							WHERE
								trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
								trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
								trn_sampleinfomations.intStatus =  '1' 
								and $para(isNull(trn_sampleinfomations_customer_approved.strPrintName))

							";
				}
				elseif($intStatus==1)
				{
					$sql = "SELECT
								trn_sampleinfomations_customer_approved.strPrintName,
								trn_sampleinfomations_customer_approved.strCombo,
								trn_sampleinfomations_customer_approved.intRevisionNo
							FROM trn_sampleinfomations_customer_approved
							WHERE
								trn_sampleinfomations_customer_approved.intSampleNo =  '$sampleNo' AND
								trn_sampleinfomations_customer_approved.intSampleYear =  '$sampleYear'
							";	
				}
				$result1 = $db->RunQuery($sql);
				while($row1=mysqli_fetch_array($result1))
				{ 
					$printName 		= $row1['strPrintName'];
					$strCombo 		= $row1['strCombo'];
					$intRevisionNo 	= $row1['intRevisionNo'];
			  ?>
              <tr>
                <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $row1['strPrintName'] ?></td>
                <td bgcolor="#FFFFFF"><select name="cboCombo" id="cboCombo" style="width:100px">
                  <?php 
				  	$sql2 ="SELECT DISTINCT
								trn_sampleinfomations_details.strComboName
							FROM trn_sampleinfomations_details
							WHERE
								trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
								trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
								trn_sampleinfomations_details.strPrintName =  '$printName'
							";
					$result2 = $db->RunQuery($sql2);
					while($row2=mysqli_fetch_array($result2))
					{
						if($row2['strComboName']==$strCombo)
							echo "<option selected=\"selected\"  value=\"".$row2['strComboName']."\">".$row2['strComboName']."</option>"; 
						else
							echo "<option  value=\"".$row2['strComboName']."\">".$row2['strComboName']."</option>"; 
					}
				  ?>
                </select></td>
                <td bgcolor="#FFFFFF"><select name="cboRev" id="cboRev" style="width:40px">
                 <?php 
				  	$sql2 ="SELECT DISTINCT
								trn_sampleinfomations_details.intRevNo
							FROM trn_sampleinfomations_details
							WHERE
								trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
								trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
								trn_sampleinfomations_details.strPrintName =  '$printName'
							";
					$result2 = $db->RunQuery($sql2);
					while($row2=mysqli_fetch_array($result2))
					{
						if($intRevisionNo==$row2['intRevNo'])
							echo "<option selected=\"selected\" value=\"".$row2['intRevNo']."\">".$row2['intRevNo']."</option>"; 
						else
							echo "<option value=\"".$row2['intRevNo']."\">".$row2['intRevNo']."</option>"; 
					}
				  ?>
                </select></td>
                <td align="center" bgcolor="#FFFFFF"><input <?php
				
				if($intStatus==1)
				{
					echo " disabled=\"disabled\"  checked=\"checked\" ";	
				}
				 ?> class="chkSelect" type="checkbox" name="checkbox" id="checkbox" /></td>
                </tr>
              <?php
				}
			  ?>
            </table></td>
            </tr>
          <?Php
			}
		  ?>
        </table></div></td>
              </tr>
            </table>
			</td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><img src="images/Tclose.jpg" width="92" height="24" class="mouseover" /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>


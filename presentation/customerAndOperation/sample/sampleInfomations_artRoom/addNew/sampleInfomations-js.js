var basepath	= 'presentation/customerAndOperation/sample/sampleInfomations_artRoom/addNew/';
				  
var arr = [];
var techniqueTableScript = '';
$(document).ready(function() {



	techniqueTableScript = "<tr class=\"normalfnt\">"+
	                 "<td bgcolor=\"#FFFFFF\"><img src=\"images/del.png\" class=\"mouseover removeRow\" /></td>"+ 
                     " <td width=\"40%\" bgcolor=\"#FFFFFF\"><select  id=\"cboTechnique\" name=\"select4\" class=\"cboTechnique\" style=\"width:92px\">"+
                    "<option value=\"\"></option>"+
                 " </select></td>"+
                    "  <td width=\"13%\" bgcolor=\"#FFFFFF\"><input   style=\"width:30px\"   type=\"text\" name=\"textfield5\" id=\"txtShots\" /></td>"+
                      "<td width=\"35%\" bgcolor=\"#FFFFFF\"><select    id=\"cboItem\" name=\"select5\" class=\"cboItem\" style=\"width:100px\">"+
                        "<option value=\"\"></option>"+
                      "</select></td>"+
                      "<td width=\"12%\" bgcolor=\"#FFFFFF\"><input   style=\"width:30px\"   type=\"text\" name=\"textfield5\" id=\"txtWeight\" /></td>"+
                    "</tr>";
				  
	
	//clickEventForAddColumn();
	
	
	$.fn.sort_select_box = function(){
    // Get options from select box
    var my_options = $("#" + this.attr('id') + ' option');
    // sort alphabetically
    my_options.sort(function(a,b) {
        if (a.text > b.text) return 1;
        else if (a.text < b.text) return -1;
        else return 0
    })
	
	
	/// remove collumn function ///////////////////
	$.fn.removeCol = function(col){
    // Make sure col has value
    if(!col){ col = 1; }
    $('tr td:nth-child('+col+'), tr th:nth-child('+col+')', this).remove();
    return this;
	};

   //replace with sorted my_options;
   $(this).empty().append( my_options );

   // clearing any selections
   	$("#"+this.attr('id')+" option").attr('selected', false);
	}

	
  		$("#frmSampleInfomations").validationEngine();
		$('#frmSampleInfomations #txtCode').focus();
		
		
		setButtonEvents();
		
		
		//alert(isOldRevNo);
/*  //permision for add 
  if(intAddx)
  {
 	$('#frmSampleInfomations #butNew').show();
	//alert(intTechnicalStatus);
	if(isOldRevNo==0 && intTechnicalStatus!=1)
		$('#frmSampleInfomations #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
	if(isOldRevNo==0 && intTechnicalStatus!=1)
  		$('#frmSampleInfomations #butSave').show();
	$('#frmSampleInfomations #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmSampleInfomations #butDelete').show();
	$('#frmSampleInfomations #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmSampleInfomations #cboSearch').removeAttr('disabled');
  }*/
  
  	$('#frmSampleInfomations .removeColumn').die('click').live('click',function(){
		var cel  = $(this).parent();
		var celIndex = cel.parent().children().index(cel);
		var cellCount = ($(this).parent().parent().find('td').length);
		if(cellCount>3)
		$('#tblMain >tbody >tr td:nth-child('+(celIndex+1)+')').remove();
	});
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/
	
  	$('#frmSampleInfomations #cboCustomer').die('change').live('change',function(){
		$('#frmSampleInfomations #cboBrand').html(combo_brand($(this).val()));
	});

  ///save button click event
  $('#frmSampleInfomations #butSave').die('click').live('click',function(){
	//$('#frmSampleInfomations').submit();
	var requestType = '';
	if ($('#frmSampleInfomations').validationEngine('validate'))   
    { 
		showWaiting();

		var data = "requestType=save";
		
			data+="&sampleNo="		+	$('#cboSampleNo').val();
			data+="&sampleYear="	+	$('#cboYear').val();
			data+="&revisionNo="	+	$('#cboRevisionNo').val();
			data+="&date="			+	$('#dtDate').val();
			
			data+="&graphicNo="		+	$('#txtGraphicRefNo').val();
			data+="&customerId="	+	$('#cboCustomer').val();
			data+="&styleNo="		+	$('#txtStyleNo').val();
			data+="&brandId="		+	$('#cboBrand').val();
			
			data+="&verivide_d65="	+	$('#txtVerivide_D65').is(':checked');
			data+="&verivide_tl85="	+	$('#txtVerivide_TL84').is(':checked');
			data+="&verivide_cw="	+	$('#txtVerivide_CW').is(':checked');
			data+="&verivide_f="	+	$('#txtVerivide_F').is(':checked');
			data+="&verivide_uv="	+	$('#txtVerivide_UV').is(':checked');
			
			data+="&macbeth_dl="	+	$('#txtMacbeth_DL').is(':checked');
			data+="&macbeth_cw="	+	$('#txtMacbeth_CW').is(':checked');
			data+="&macbeth_inca="	+	$('#txtMacbeth_INCA').is(':checked');
			data+="&macbeth_tl84="	+	$('#txtMacbeth_TL84').is(':checked');
			data+="&macbeth_uv="	+	$('#txtMacbeth_UV').is(':checked');
			data+="&macbeth_horizon="	+	$('#txtMacbeth_Horizon').is(':checked');
			
			data+="&curing_temp="	+	$('#txtCuringTemp').val();
			data+="&curing_speed="	+	$('#txtCuringBeltSpeed').val();
			data+="&press_temp="	+	$('#txtPressTemp').val();
			data+="&press_pressure="+	$('#txtPressPressure').val();
			data+="&press_time="	+	$('#txtPressTime').val();
			
			data+="&meshCount="	+	$('#txtMeshCount').val();
			data+="&instruction="+	URLEncode(document.getElementById('txtInstructions').value) ;
			data+="&instructionTech="+	$('#txtInstructionsTech').val();
			
			//data+="&image="	+	$('#divPicture >img').attr('src');
			
			/*var arrTechnique="[ ";
			$('.chkTechnique:checked').each(function(){
				arrTechnique += '{ "techniqueId":"'+$(this).val()+ '"},';
			});
			arrTechnique = arrTechnique.substr(0,arrTechnique.length-1);
			arrTechnique += " ]";
			
			data+="&arrTechnique="	+	arrTechnique;*/
								/////////////////////////// create part and width height //////////////////////////
			var arrPart = '';
			var printCount  =0;
			//arrPart += '[';
			//$('#tblMain >tbody >tr:eq('+i+') >td:eq('+n+')> .tblColors >tbody >tr:eq(1)>td:eq(0) >span').each(function(){
			$('#tblMain >tbody >tr:eq(1) >td').not(':first').not(':last').each(function(){
				
				var partId = $(this).find('.part').val();	
				var size_w = $(this).find('.sizeW').val();	
				var size_h = $(this).find('.sizeH').val();	
				arrPart += '{"partId":"'+partId+'","size_w":"'+size_w+'","size_h":"'+size_h+'"},';
				printCount++;
			});
			if(arrPart!='')
			{
				arrPart = arrPart.substr(0,arrPart.length-1);
				data+="&arrPart=["+	arrPart+']';	
			}
			
			var rowCount = $('#tblMain >tbody >tr').length;
			var cellCount = document.getElementById('tblMain').rows[1].cells.length;
			var row = 0;
			
			var arrCombo="[";
			
			for(var i=3;i<rowCount-1;i+=2)
			{
				var combo 			= document.getElementById('tblMain').rows[i-1].cells[0].childNodes[0].value;
				
				var printMode 		= document.getElementById('tblMain').rows[i].cells[0].childNodes[1].value;
				var washStanderd 	= document.getElementById('tblMain').rows[i].cells[0].childNodes[3].value;
				var groundColor 	= document.getElementById('tblMain').rows[i].cells[0].childNodes[5].value;
				var fabricType 		= document.getElementById('tblMain').rows[i].cells[0].childNodes[7].value;
				//alert(cellCount);
				for(var n=1;n<cellCount-1;n++)
				{
					//alert(n);
					 arrCombo += "{";
					 
					var printName = 	'print '+(n);//document.getElementById('tblMain').rows[0].cells[n].childNodes[0].value;
					var techniqueId = 	document.getElementById('tblMain').rows[1].cells[n].childNodes[0].value;
					var colorId 	= 	document.getElementById('tblMain').rows[i].cells[n].childNodes[0].value;
					
					//var sizeW 	= 	document.getElementById('tblSizes').rows[n-3].cells[1].childNodes[0].rows[0].cells[0].childNodes[0].value;
					//var sizeH 	= 	document.getElementById('tblSizes').rows[n-3].cells[1].childNodes[0].rows[0].cells[2].childNodes[0].value;
					//var partId 	= 	document.getElementById('tblSizes').rows[n-3].cells[2].childNodes[0].value;
					
					
					arrCombo += '"printMode":"'+	printMode +'",' ;
					arrCombo += '"washStanderd":"'+	washStanderd +'",' ;
					arrCombo += '"comboName":"'+	combo +'",' ;
					arrCombo += '"groundColor":"'+	groundColor +'",' ;
					arrCombo += '"fabricType":"'+	fabricType +'",' ;
					arrCombo += '"printName":"'+	printName +'",' ;
					
					var grid2Details = "";
					var grid2Id = 0;
					$('#tblMain >tbody >tr:eq('+i+') >td:eq('+n+')').find('#tblGrid2 >tbody >tr:not(:first)').each(function(){
						
						grid2Id++;
						var colorId 	= $(this).find('td:eq(0)').attr('id');
						//var techId  	= $(this).find('#cboTechnique').val();
						//var itemId  	= $(this).find('#cboItem').val();
						var qty     	= $(this).find('#txtQty').val();
						var sizeW  		= $(this).find('#txtSizeW').val();
						var sizeH  		= $(this).find('#txtSizeH').val();
						
					$('#tblMain >tbody >tr:eq('+i+') >td:eq('+n+')').find('#tblGrid2 >tbody >tr:eq('+grid2Id+')').find('#tblGrid3 >tbody >tr').each(function(){
							var techId  = $(this).find('#cboTechnique').val();
							var shots   = $(this).find('#txtShots').val();
							var itemId  = $(this).find('#cboItem').val();
							var weight  = $(this).find('#txtWeight').val();
							
							grid2Details +='{'+
									'"colorId":"'+colorId+'",'+
									'"techId":"'+techId+'",'+
									'"shots":"'+shots+'",'+
									'"itemId":"'+itemId+'",'+
									'"weight":"'+weight+'",'+
									'"qty":"'+qty+'",'+
									'"sizeW":"'+sizeW+'",'+
									'"sizeH":"'+sizeH+'"'+
									'},';
									
							//cboTechnique
					});
					//return;
						//alert(n);
					
					});
					if(grid2Details!='')
					{
						grid2Details = grid2Details.substr(0,grid2Details.length-1);	
					}
					arrCombo +=   '"grid2Details":['+grid2Details+']';	
					
					


						
					////////////////////////// create type of technique array /////////////////
					//var arrTypeOf='';
//						arrTypeOf = ',"arrTechnique":[';
//						var pass2 = false;
//						var typeOfPrintCount = document.getElementById('tblMain').rows[i].cells[n].childNodes[0].rows.length;
//						//alert(typeOfPrintCount);
//						if(parseInt(typeOfPrintCount)<=1)
//						{
//							alert('Type of prints cano\'t be blank');	
//							hideWaiting();
//							return;
//						}
//						for(t=1;t<typeOfPrintCount;t++)
//						{
//							pass2 = true;
//							var typeOfPrintId = document.getElementById('tblMain').rows[i].cells[n].childNodes[0].rows[t].cells[0].id;
//							//alert(typeOfPrintId);
//							
//						//var countTechnique = document.getElementById('tblMain').rows[i].cells[n].childNodes[0].rows[t].cells[1].childNodes[0].rows.length;
//						var countTechnique = $('#tblMain >tbody >tr:eq('+i+') >td:eq('+n+') .tblTypeOfPrint >tbody >tr:eq('+t+') >td:eq(1) .tblTechnique >tbody >tr').length;
//							var pass  = false;
//							for(var p=1;p<countTechnique;p++)
//							{
//								pass = true;
//				var techId 	= $('#tblMain >tbody >tr:eq('+i+') >td:eq('+n+') .tblTypeOfPrint >tbody >tr:eq('+t+') >td:eq(1) .tblTechnique >tbody >tr:eq('+p+') >td:eq(0)').attr('id');
//				
//				var qty 	=  $('#tblMain >tbody >tr:eq('+i+') >td:eq('+n+') .tblTypeOfPrint >tbody >tr:eq('+t+') >td:eq(1) .tblTechnique >tbody >tr:eq('+p+') >td:eq(1) .qty').val();
//	
//				var itemId 	= $('#tblMain >tbody >tr:eq('+i+') >td:eq('+n+') .tblTypeOfPrint >tbody >tr:eq('+t+') >td:eq(1) .tblTechnique >tbody >tr:eq('+p+') >td:eq(2) .cboItem').val();
//								//alert(techId);
//								//alert(typeOfPrintId);
//								arrTypeOf += '{"typeId":"'+typeOfPrintId +'",' ;
//								arrTypeOf += '"techId":"'+techId +'",' ;
//								arrTypeOf += '"qty":"'+qty +'",' ;
//								arrTypeOf += '"itemId":"'+itemId +'"},' ;
//							}
//							if(pass==false)
//							{
//								//alert(typeOfPrintId);
//								arrTypeOf += '{"typeId":"'+typeOfPrintId +'",' ;
//								arrTypeOf += '"techId":"",' ;
//								arrTypeOf += '"qty":"",' ;
//								arrTypeOf += '"itemId":""},' ;	
//							}
//						}
//						arrTypeOf = arrTypeOf.substr(0,arrTypeOf.length-1);
//						arrTypeOf +=	']' ;
//						//if(pass)
//						if(pass2==false)
//							arrCombo +=  ',"arrTechnique":""';
//						else
//							arrCombo +=  arrTypeOf;
						//else
							//arrCombo +=  ',"arrTechnique":""';
					
					//var url
					//var obj = $.ajax({url:url,async:false});
					arrCombo +=  '},';
					
				}
				
			}
			arrCombo = arrCombo.substr(0,arrCombo.length-1);
			arrCombo += " ]";
			///////////////////// end of combo array /////////////////////////////////
			
			data+="&arrCombo="	+	arrCombo;
			
/*			$('#tblMain >tbody >tr').each(function(){
				
				var cellCount = $(this).find('td').length;
				if(row>=2)
				{
					var printId = $(this).find('td .printMode').val();
					alert(printId);
				}
				row++;
			});*/
			/////////////////////////////////////////////////////////////////////////////////////
			
			
			//return;
		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basepath+"sampleInfomations-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json", 
			type:'POST', 
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmSampleInfomations #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmSampleInfomations').get(0).reset();
						var x=0;
						hideWaiting();
						//document.location.href = 'sampleInfomations.php?sampleNo='+sampleNo+'&sampleYear='+sampleYear+'&revNo='+revisionNo;
				//saveImage(json.sampleNo,json.sampleYear,json.revisionNo,printCount);
				//document.location.href = 'sampleInfomations.php?sampleNo='+json.sampleNo+'&sampleYear='+json.sampleYear+'&revNo='+json.revisionNo;
						//$('#cboSampleNo').html(json.sampleNoOption);
						//$('#cboSampleNo').val(json.sampleNo);
						//$('#cboSampleNo').change();
						//$('#cboRevisionNo').val(json.revisionNo);
						//$('#cboRevisionNo').change();
						
						//var t=setTimeout("alertx("+json.sampleNo+","+json.sampleYear+","+json.revisionNo+","+printCount+")",2000);
						return;
					}
					else
						hideWaiting();
				},
			error:function(xhr,status){
					hideWaiting();
					$('#frmSampleInfomations #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					//var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
		
		
			
	}
   });
   
   /////////////////////////////////////////////////////
	$('#frmSampleInfomations #cboYear').die('change').live('change',function(){
		window.location.href  = "?q=416&sampleYear="+$('#cboYear').val();
		//loadSampleNos();
	});
   /////////////////////////////////////////////////////
   //// load sampleInfomations details //////////////////////////
   /////////////////////////////////////////////////////
   
   $('#frmSampleInfomations #cboRevisionNo').die('click').live('click',function(){
	   $('#frmSampleInfomations').validationEngine('hide');
   });
    $('#frmSampleInfomations #cboRevisionNo').die('change').live('change',function(){
		$('#frmSampleInfomations').validationEngine('hide');
		
		if($('#frmSampleInfomations #cboRevisionNo').val()=='')
		{
			
			var intSampleNo = $('#cboSampleNo').val();
			document.location.href = '?q=416';
			//$('#frmSampleInfomations').get(0).reset();
			
			$('#cboSampleNo').val(intSampleNo);
			//$('.divPicture').html('');
			return;
		}
		
		document.location.href = '?q=416&sampleNo='+$('#cboSampleNo').val()+'&sampleYear='+$('#cboYear').val()+'&revNo='+$('#cboRevisionNo').val();
		return;
		
		var url = basepath+'sampleInfomations-db-get.php?requestType=loadMainDetails';
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"sampleNo="+$('#cboSampleNo').val()+"&sampleYear="+$('#cboYear').val()+"&revisionNo="+$('#cboRevisionNo').val(),
			async:false,
			success:function(json){
					
					$('#iframeFiles').attr('src','filesUpload.php?txtFolder='+$('#cboSampleNo').val()+'_'+$('#cboYear').val());
					
					$("#iframeFiles").contents().find("#txtFolder").val($('#cboSampleNo').val()+'_'+$('#cboYear').val());
					
					//json  = eval('('+json+')');
					$('#frmSampleInfomations #dtDate').val(				json.dtDate);
					//$('#frmSampleInfomations #dtDeliveryDate').val(		json.dtDeliveryDate);
					$('#frmSampleInfomations #txtGraphicRefNo').val(	json.strGraphicRefNo);
					$('#frmSampleInfomations #cboCustomer').val(		json.intCustomer);
						$('#frmSampleInfomations #cboCustomer').change();
					$('#frmSampleInfomations #txtStyleNo').val(			json.strStyleNo);
					$('#frmSampleInfomations #cboBrand').val(			json.intBrand);
					$('#frmSampleInfomations #txtSampleQty').val(		json.dblSampleQty);
					$('#frmSampleInfomations #txtGrade').val(			json.intGrade);
					$('#frmSampleInfomations #txtFabricType').val(		json.strFabricType);
					$('#frmSampleInfomations #txtVerivide_D65').prop('checked',chk(json.verivide_d65));
					$('#frmSampleInfomations #txtVerivide_TL84').prop('checked',chk(	json.verivide_tl84));
					$('#frmSampleInfomations #txtVerivide_CW').prop('checked',chk(		json.verivide_cw));
					$('#frmSampleInfomations #txtVerivide_F').prop('checked',chk(		json.verivide_f));
					$('#frmSampleInfomations #txtVerivide_UV').prop('checked',chk(		json.verivide_uv));
					$('#frmSampleInfomations #txtMacbeth_DL').prop('checked',chk(		json.macbeth_dl));
					$('#frmSampleInfomations #txtMacbeth_CW').prop('checked',chk(		json.macbeth_cw));
					$('#frmSampleInfomations #txtMacbeth_INCA').prop('checked',chk(	json.macbeth_inca));
					$('#frmSampleInfomations #txtMacbeth_TL84').prop('checked',chk(	json.macbeth_tl84));
					$('#frmSampleInfomations #txtMacbeth_UV').prop('checked',chk(		json.macbeth_uv));
					$('#frmSampleInfomations #txtMacbeth_Horizon').prop('checked',chk(json.macbeth_horizon));
					$('#frmSampleInfomations #txtCuringTemp').val(		json.dblCuringCondition_temp);
					$('#frmSampleInfomations #txtCuringBeltSpeed').val(	json.dblCuringCondition_beltSpeed);
					$('#frmSampleInfomations #txtPressTemp').val(		json.dblPressCondition_temp);
					$('#frmSampleInfomations #txtPressPressure').val(	json.dblPressCondition_pressure);
					$('#frmSampleInfomations #txtPressTime').val(		json.dblPressCondition_time);
					$('#frmSampleInfomations #txtMeshCount').val(		json.strMeshCount);
					$('#frmSampleInfomations #txtInstructions').val(	json.strAdditionalInstructions);
					
					$('#divPicture').html('');
					$(document.createElement("img"))
						.attr({ src: 'documents/sampleinfo/samplePictures/'+$('#cboSampleNo').val()+'-'+$('#cboYear').val()+'-'+$('#cboRevisionNo').val()+'.jpg', title: 'Sample Picture ' })
						.appendTo($('#divPicture'))
						.attr('id','saveimg')
						.click(function(){
							// Do something
						})

					//alert(json.arrTechnique[0]);
					
					
					///insert columns /////////////////////////////////////
					clearRows();
					clearColumns();
					//clearSizes();
					//$('#tblMain >select').each;
					//clearColumns();
					for(var i=1;i<json.columnCount;i++)
					{
						clickEventForAddColumn();
						$('#tblSizes >tbody >tr:last').after('<tr>'+$('#tblSizes >tbody >tr:last').html()+'</tr>');
						
						var nextCellId = parseInt($('#tblMain .dataRow').find('td').length)-5;
						$('#tblSizes >tbody >tr:last').find('td:eq(0)').html('Print '+nextCellId);	
					}
					
					///insert rows /////////////////////////////////////
					for(var i=1;i<json.rowCount;i++)
					{
						$('#tblMain >tbody >tr:last').before('<tr>'+$('#tblMain >tbody >tr:gt(1)').html()+'</tr>');	
					}
					
					////////////////// set values for main grid ////////////////////////////////////
					var arrCombo = json.arrCombo;
					//alert(json.arrPrintName.length);
					var rowCount1 = document.getElementById('tblMain').rows.length;
					for(var i=2;i<rowCount1-1;i++)
					{
						document.getElementById('tblMain').rows[i].cells[0].childNodes[0].value = arrCombo[i-2]['modeId'];
						document.getElementById('tblMain').rows[i].cells[1].childNodes[0].value = arrCombo[i-2]['washId'];
						document.getElementById('tblMain').rows[i].cells[2].childNodes[0].value = arrCombo[i-2]['comboName'];
						document.getElementById('tblMain').rows[i].cells[3].childNodes[0].value = arrCombo[i-2]['groundId'];
						
						//arrInkColor
						var x = 0;
						for(var a=0;a<json.arrInkColor.length;a++)
						{
							
							if(arrCombo[i-2]['comboName']==json.arrInkColor[a]['comboName'])
							{
								document.getElementById('tblMain').rows[i].cells[4+x++].childNodes[0].value = json.arrInkColor[a]['color'];
								//alert(json.arrInkColor[a]['comboName']);
							}
						}
						
					}
					////////////////////////////////////////////////////////////////////////////////
					
					loadGridCheck(json.arrTechnique,'chkTechnique')
					updateTechniqueCombo();
					loadGridCheck(json.arrType,'chkTypeOfPrint')
					
					///////// set report path ///////////////////
					/*$('#butReport').unbind('click');
					$('#butReport').click(function(){
						//alert(1);
						window.open('../../Report/sampleReport.php?no='+$('#cboSampleNo').val()+'&year='+$('#cboYear').val()+'&revNo='+$('#cboRevisionNo').val());	
					});
					$('#butConfirm').unbind('click');
					$('#butConfirm').click(function(){
						window.open('../../Report/sampleReport.php?no='+$('#cboSampleNo').val()+'&year='+$('#cboYear').val()+'&revNo='+$('#cboRevisionNo').val());	
					});*/
					
					/////////////////////////////////////////////
					
					var arrPrintName = json.arrPrintName;
					for(var i=0;i<json.arrPrintName.length;i++)
					{
						document.getElementById('tblMain').rows[0].cells[4+i].childNodes[0].value = arrPrintName[i]['name'];
						document.getElementById('tblMain').rows[1].cells[4+i].childNodes[0].value = arrPrintName[i]['techId'];
						
						document.getElementById('tblSizes').rows[i+1].cells[0].childNodes[0].nodeValue = arrPrintName[i]['name'];
		document.getElementById('tblSizes').rows[i+1].cells[1].childNodes[0].rows[0].cells[0].childNodes[0].value=arrPrintName[i]['intSize_width'];
		document.getElementById('tblSizes').rows[i+1].cells[1].childNodes[0].rows[0].cells[2].childNodes[0].value=arrPrintName[i]['intSize_height'];
		document.getElementById('tblSizes').rows[i+1].cells[2].childNodes[0].value = arrPrintName[i]['intPart'];
						
					}
					
					///////////// set iframe files src ////////////////////
					
					
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmSampleInfomations #butNew').die('click').live('click',function(){
		document.location.href = '?q=416';
		//loadCombo_frmSampleInfomations();
		$('#frmSampleInfomations #txtCode').focus();
	});
    $('#frmSampleInfomations #butDelete').die('click').live('click',function(){
		if($('#frmSampleInfomations #cboSearch').val()=='')
		{
			$('#frmSampleInfomations #butDelete').validationEngine('showPrompt', 'Please select sampleInfomations.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmSampleInfomations #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basepath+"sampleInfomations-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmSampleInfomations #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmSampleInfomations #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmSampleInfomations').get(0).reset();
													//loadCombo_frmSampleInfomations();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
	
	$('#frmSampleInfomations .chkTechnique').die('click').live('click',function(){
		
		var val = $(this).is(':checked');
		var value = $(this).closest('tr').attr('id');
		var text = $(this).closest('tr').find('td').eq(1).text();
		if(val)
			$('#cboTechnique').append(new Option(text,value));
		else
			$("#cboTechnique option[value='"+value+"']").remove();
			
		
		$("#cboTechnique").sort_select_box();
		
		//add technique to all select boxes////
		$('#tblMain .cboTechnique2').each(function(){
			var val = $(this).val();
			$(this).html($("#cboTechnique").html());
			$(this).val(val);
		});
		///////////////////////////////////////////////////////////
	});
	
	$('#frmSampleInfomations #butInsertCol').die('click').live('click',function(){
		
		clickEventForAddColumn();
		$('#tblSizes >tbody >tr:last').after('<tr>'+$('#tblSizes >tbody >tr:last').html()+'</tr>');
		
		var nextCellId = parseInt($('#tblMain .dataRow').find('td').length)-5;
		$('#tblSizes >tbody >tr:last').find('td:eq(0)').html('Print '+nextCellId);
		//alert(1);
		
		inc(basepath+'sampleInfomations-js.js');
		inc('libraries/javascript/script.js');
		inc('libraries/validate/jquery-1.js');
		inc('libraries/validate/jquery_002.js');
		inc('libraries/validate/jquery.js');
		$('#butInsertCol').unbind('click');
		$('#butInsertRow').unbind('click');

	});
	

$('#frmSampleInfomations #butInsertRow').die('click').live('click',function(){
		
	var rowCount = document.getElementById('tblMain').rows.length;
	//var cellCount = document.getElementById('tblMain').rows[0].cells.length;
	document.getElementById('tblMain').insertRow(rowCount-1);
	document.getElementById('tblMain').insertRow(rowCount-1);
	rowCount = document.getElementById('tblMain').rows.length;
	
	document.getElementById('tblMain').rows[rowCount-3].innerHTML = document.getElementById('tblMain').rows[rowCount-5].innerHTML;
	
	document.getElementById('tblMain').rows[rowCount-2].innerHTML = document.getElementById('tblMain').rows[rowCount-4].innerHTML;

	//document.getElementById('tblMain').rows[rowCount-3].cells[1].childNodes[0].nodeValue = '';
	var cellCount = document.getElementById('tblMain').rows[rowCount-3].cells.length;
	
	document.getElementById('tblMain').rows[rowCount-3].cells[cellCount-1].innerHTML = '';
	/*for(var i=0;i<rowCount;i++)
	{
		document.getElementById('tblMain').rows[i].insertCell(cellCount-1);	
		document.getElementById('tblMain').rows[i].cells[cellCount-1].innerHTML  = document.getElementById('tblMain').rows[i].cells[cellCount-2].innerHTML
		document.getElementById('tblMain').rows[i].cells[cellCount-1].bgColor 		= "#FFFFFF";
		document.getElementById('tblMain').rows[i].cells[cellCount-1].className 		= "printName";
	}*/
		//var html = "<tr>abc</tr>";
		//$('#tblMain >tbody >tr:last').before('<tr>'+$('#tblMain >tbody >tr:gt(1)').html()+'</tr>');
		
		setButtonEvents();
	});


	$('#frmSampleInfomations #cboSampleNo').die('change').live('change',function(){
		
		loadRevisionNo();
		
		var intSampleNo = $('#cboSampleNo').val();
		$('#frmSampleInfomations').get(0).reset();
		$('#cboSampleNo').val(intSampleNo);
		$('#divPicture').html('');
		return;
	});
/*	$('#cboRevisionNo').change(function(){
		loadMainDetails();
	});*/
});

				function	updateTechniqueCombo()
				{
					var i=0;
			
	arr = [];
	$('#frmSampleInfomations .chkTechnique').each(function(){
		if($(this).is(':checked'))
		{
			arr[i++]=$(this).parent().parent().attr('id');
		}
	});

			
			$('#frmSampleInfomations .chkTechnique').each(function(){
				var x =$.inArray($(this).parent().parent().attr('id'),arr);
				//alert(x);
				if(x!=-1)
				{
					$(this).prop('checked',true);
					//////////////////////////// add options to combo /////////
					var value = $(this).closest('tr').attr('id');
					var text = $(this).closest('tr').find('td').eq(1).text();
					$('#cboTechnique').append(new Option(text,value));
				}	
			
			$("#cboTechnique").sort_select_box();
		//add technique to all select boxes////
			$('#tblMain .cboTechnique2').each(function(){
				var val = $(this).val();
				$(this).html($("#cboTechnique").html());
				$(this).val(val);
			});
			
			
	});
					
				}

function saveImage(sampleNo,sampleYear,revisionNo,printCount)
{
	var x=0;
	$('#tblMain >tbody >tr:eq(0) >td').not(':last').not(':first').each(function(){
		//alert(1);
		var img =$(this).find('img').attr('src');
		//alert(img);
		var newImage = 1;
		if($(this).find('img').attr('id')=='saveimg')
		{
			newImage=0;
		}
		var ajax = new XMLHttpRequest();
		ajax.open("POST",basepath+'saveImage.php?sampleNo='+sampleNo+'&sampleYear='+sampleYear+'&revisionNo='+revisionNo+'&newImage='+newImage+'&printId='+(++x));		//ajax.count = x;
		
		ajax.onreadystatechange=function()
		  {
		  if (ajax.readyState==4 && ajax.status==200)
			{
				printCount--;
				if(printCount<=0)
					{
					hideWaiting();
					document.location.href = '?q=416&sampleNo='+sampleNo+'&sampleYear='+sampleYear+'&revNo='+revisionNo;
					}
			}
		  };
		ajax.setRequestHeader('Content-Type', 'application/upload');
		ajax.send(img);
	});
}
/*function loadCombo_frmSampleInfomations()
{
	var url 	= "sampleInfomations-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmSampleInfomations #cboSearch').html(httpobj.responseText);
}*/

function loadSampleNo(sampleYear)
{
	var url 	= basepath+"sampleInfomations-db-get.php?requestType=loadSampleNoCombo&sampleYear="+sampleYear;
	var httpobj = $.ajax({url:url,async:false})
	$('#frmSampleInfomations #cboSampleNo').html(httpobj.responseText);
}

function alertx(sampleNo,sampleYear,revisionNo,printCount)
{
	//alert(sampleNo);
	hideWaiting();
	$('#frmSampleInfomations #butSave').validationEngine('hide')	;
	document.location.href = '?q=416&sampleNo='+sampleNo+'&sampleYear='+sampleYear+'&revNo='+revisionNo;
}
function alertDelete()
{
	$('#frmSampleInfomations #butDelete').validationEngine('hide')	;
}

function loadMain()
{
	$("#butLoadTechnique").die('click').live('click',function(){
		popupWindow('1');
	});
	
	//$('#butInsertCol').trigger('click');clickEventForAddColumn
	//clickEventForAddColumn();
}

function loadPrintMode()
{
	$("#butPrintMode").die('click').live('click',function(){
		popupWindow2('2','#cboSearch','.printMode','#frmSampleInfomations');
	});
}
function loadWasingCondistion()
{
	$("#butWashingStanderd").die('click').live('click',function(){
		popupWindow2('3','#cboSearch','.washStanderd','#frmSampleInfomations');
	});
}

function loadColors()
{
	$("#butColor").die('click').live('click',function(){
		popupWindow2('4','#cboSearch','.colors','#frmSampleInfomations');
	});	
}

function loadPart()
{
	$("#butLoadPart").die('click').live('click',function(){
		popupWindow2('5','#cboSearch','.part','#frmSampleInfomations');
	});		
}
//loadTypeOfPrint

function loadTypeOfPrint()
{
	
	$("#butTypeOfPrint").die('click').live('click',function(){
		popupWindow('6');
	});
}

function loadNewColors()
{
		var m = $("#iframeMain"+x).contents().find(cboFrom).html();
		//alert(m);
		$(pageTo+" "+cboTo).html(m);	
}

function closePopUp(){
	
/*	var i=0;
/*	$('#tblTechnique tbody tr').each(function(){
		//alert($(this).attr('id'));
		var t = $(this).find('td').eq(0).eq(0).attr('checked');
		alert(t);
	});
	
	arr = [];
	$('.chkTechnique').each(function(){
		if($(this).attr('checked'))
		{
			arr[i++]=$(this).parent().parent().attr('id');
		}
	});
	$("#tblTechnique").load('table-technique.php',function(){
		$('#cboTechnique').html('');
		$('#cboTechnique').append(new Option('',''));
			$('.chkTechnique').bind('click',chkTechniqueClick);
			
			$('.chkTechnique').each(function(){
				var x =$.inArray($(this).parent().parent().attr('id'),arr);
				//alert(x);
				if(x!=-1)
				{
					$(this).attr('checked',true);
					//////////////////////////// add options to combo /////////
					var value = $(this).closest('tr').attr('id');
					var text = $(this).closest('tr').find('td').eq(1).text();
					$('#cboTechnique').append(new Option(text,value));
				}	
			});
			
			$("#cboTechnique").sort_select_box();
		//add technique to all select boxes////
			$('#tblMain .cboTechnique2').each(function(){
				var val = $(this).val();
				$(this).html($("#cboTechnique").html());
				$(this).val(val);
			});
			
			
	});
	
	$("#tblTypeOfPrint").load('table-typeofprint.php');*/
}

function chkTechniqueClick(){
	var val = $(this).is(':checked');
	var value = $(this).closest('tr').attr('id');
	var text = $(this).closest('tr').find('td').eq(1).text();
	if(val)
		$('#cboTechnique').append(new Option(text,value));
	else
		$("#cboTechnique option[value='"+value+"']").remove();
		
	
	$("#cboTechnique").sort_select_box();
	
	//add technique to all select boxes////
	$('#tblMain .cboTechnique2').each(function(){
		var val = $(this).val();
		$(this).html($("#cboTechnique").html());
		$(this).val(val);
	});
	///////////////////////////////////////////////////////////
}


function clickEventForAddColumn()
{
	
	var rowCount = document.getElementById('tblMain').rows.length;
	var cellCount = document.getElementById('tblMain').rows[0].cells.length;
	
	for(var i=0;i<rowCount;i++)
	{
		document.getElementById('tblMain').rows[i].insertCell(cellCount-1);	
		document.getElementById('tblMain').rows[i].cells[cellCount-1].innerHTML  = document.getElementById('tblMain').rows[i].cells[cellCount-2].innerHTML
		document.getElementById('tblMain').rows[i].cells[cellCount-1].align = "center";
		document.getElementById('tblMain').rows[i].cells[cellCount-1].bgColor 		= "#FFFFFF";
		
		
	}
	
	cellCount = document.getElementById('tblMain').rows[0].cells.length;
	
	var text1 = document.getElementById('tblMain').rows[1].cells[cellCount-3].innerHTML;
	document.getElementById('tblMain').rows[1].cells[cellCount-2].innerHTML = text1;//"Print "+(cellCount-2)+text1;
	document.getElementById('tblMain').rows[1].cells[cellCount-2].childNodes[0].innerHTML = "Print "+(cellCount-2);
	document.getElementById('tblMain').rows[1].cells[cellCount-2].className 		= "printName";
	
/*	var cellLength = $("#tblMain tbody tr").eq(2).find('td').length;
		
		
		
		var nextCellId = parseInt($('#tblMain .dataRow').find('td').length)-1;
		var i = 0;
		var html = '';
		$('#tblMain').find('tr').each(function(){
				//html =$(this).find('td').eq(cellLength-1).html();
				//html = "";
			
		
        	$(this).find('td').eq(cellLength).before('<td  bgcolor="#FFFFFF">&nbsp;</td>');
			
						
			/*switch(i){
				case 0:
					html = '<td height="18" bgcolor="#CCCCCC" class="gridHeader"><input style="width:80px;text-align:center" type="text" name="textfield'+nextCellId+'" id="textfield'+nextCellId+'" value="Print '+nextCellId+'"/></td>';
					break;
				case 1:
					html = '<td bgcolor=\"#CAFBF5\" class=\"normalfnt\"><select class="cboTechnique2" style="width:80px">'+$('#cboTechnique').html()+'</select></td>';
					break;
				case 2:
					html = '<td bgcolor=\"#FFFFFF\" class=\"normalfnt\"><select class="colors" style="width:80px">'+$('#cboColor').html()+'</select></td>';
					break;
				default:
				if($(this).attr('rowIndex')==($('#tblMain').find('tr').length-1))
					html = '<td bgcolor=\"#FFFFFF\" class=\"normalfnt\">&nbsp;</select></td>';
				else
					html = '<td bgcolor=\"#FFFFFF\" class=\"normalfnt\"><select class="colors" style="width:80px">'+$('#cboColor').html()+'</select></td>';
			}*/
			
			//html = "";
			
		
        	//$(this).find('td').eq(cellLength-1).before(html);
			////////////////////////////////////////////////////////////////
			//var m = $("#iframeMain4").contents().find('#cboColors').html();
			//$('#frmSampleInfomations .cboColors').html(m);
			////////////////////////////////////////////////////////////////
			//i++;
    	//});	*/
		setButtonEvents();
}

function loadRevisionNo()
{
	var url = basepath+"sampleInfomations-db-get.php?requestType=loadRevisionNo&sampleNo="+$('#cboSampleNo').val()+"&sampleYear="+$('#cboYear').val();
	var obj = $.ajax({url:url,async:false});	
	$('#cboRevisionNo').html(obj.responseText);
}

function loadGridCheck(arr,chkClass)
{
	var id = "";
	$('.'+chkClass).removeAttr('checked');	
	
	if(arr!=null)
	{
		for(var i=0;i<=arr.length-1;i++)
		{
			//$('#'+chkClass+arr[i]).attr('checked',true);	
			$('#'+chkClass+arr[i]).click();	
				
		}
	}	
	
	
}

function clearRows()
{
	var rowCount = $('#tblMain >tbody >tr').length;
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=3;i<rowCount-1;i++)
	{
			document.getElementById('tblMain').deleteRow(i);
	}
	
	var rowCount1 = $('#tblSizes >tbody >tr').length;
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount1;i++)
	{
		document.getElementById('tblSizes').deleteRow(1);
	}
}

function clearColumns()
{
	
	
	var columLength = document.getElementById('tblMain').rows[0].cells.length;
	for(var i=5;i<columLength-1;i++)
	{
		//alert(i);
		//$('#tblMain').removeCol(i);
		//document.getElementById('tblMain').rows[0].deleteColumn(4);
		//$('td[col='+i+']').remove();
		$('#tblMain >tbody >tr').find('td:not(:last):eq('+5+')').remove();
	}

}

function addTypeOfPrintToGrid(rowId,cellId)
{
		//alert("row = "+rowId+"<br>cell = "+cellId);
	
	//alert(rows);
	$('#tblTypeOfPrint .chkTypeOfPrint:checked').each(function(){
		var tblType= document.getElementById('tblMain').rows[rowId].cells[cellId].childNodes[0];
		var rows = document.getElementById('tblTypeOfPrint').rows.length;
	
		var id = $(this).val();
		var name = $(this).parent().parent().find('td').eq(1).html();
		var y = tblType.rows.length;
		var found = false;
		for(var i=1;i<y;i++)
		{
			
			//alert(tblType.rows[i].cells[0].id);
			//alert(id);
			if(tblType.rows[i].cells[0].id==id)
			{
				found = true;break;
			}
		}
		if(!found)
		{
			//tblType= document.getElementById('tblMain').rows[rowId].cells[cellId].childNodes[0];
			
			tblType.insertRow(y);
			tblType.rows[y].innerHTML = "<td id=\""+id+"\" class=\"normalfnt\" bgcolor=\"#ffffffff\"><img class=\"mouseover removeRow\" src=\"images/del.png\"/>"+name+"</td><td bgcolor=\"#ffffffff\">"+techniqueTableScript+"</td>";
		}
	});
	//alert($('#tblTypeOfPrint >tbody >tr').length);
	setButtonEvents();
}
function removeRow(obj)
{
	//alert(1);
	obj.parentNode.parentNode.parentNode.deleteRow(obj.parentNode.parentNode.rowIndex)	
}

function removeRow2(obj)
{
	var count = obj.parentNode.parentNode.parentNode.rows.length;
	var id = obj.parentNode.parentNode.rowIndex;
	//alert(id);
	//alert(id);
	if(count>5)
	{
		$('#tblMain >tbody >tr:eq('+id+')').remove();
		$('#tblMain >tbody >tr:eq('+id+')').remove();
	}
	//obj.parentNode.parentNode.parentNode.deleteRow(id)	;
	//obj.parentNode.parentNode.parentNode.deleteRow(id)	;
}

function setToTypeButton(obj)
{
	//var id = obj.parentNode.parentNode.parentNode.innerHTML();
	var rowId = obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
	var cellId = obj.parentNode.parentNode.parentNode.parentNode.parentNode.cellIndex;
	popupWindow3('1');
	$('#popupContact1').load(basepath+'typeOfPrint.php',function(){
		
		$('#frmTypeOfPrintPopUp #butAdd').die('click').live('click',function(){
			addTypeOfPrintToGrid(rowId,cellId);
			disablePopup();
		});
		
		$('#frmTypeOfPrintPopUp #butClose1').die('click').live('click',disablePopup);
		
	});	
}

function setToColorGrid(obj)
{
	//var id = obj.parentNode.parentNode.parentNode.innerHTML();
	var rowId =  obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
	var cellId = obj.parentNode.parentNode.parentNode.parentNode.parentNode.cellIndex;
	popupWindow3('1');
	$('#popupContact1').load(basepath+'colors.php',function(){
		
		$('#frmColors #butAdd').die('click').live('click',function(){
			addColorstoGrid(rowId,cellId);
			disablePopup();
		});
		
		$('#frmColors #butClose1').die('click').live('click',disablePopup);
		
	});		
}

function setToCopy()
{
	//var id = obj.parentNode.parentNode.parentNode.innerHTML();
	popupWindow3('1');
	var x = $(this).parent().parent().index();
	var y = $(this).parent().index();
	$('#popupContact1').load(basepath+'copyPopup.php?sampleNo='+$('#cboSampleNo').val()+'&sampleYear='+$('#cboYear').val()+'&revNo='+$('#cboRevisionNo').val()+'&rowId='+x+'&cellId='+y,function(){
		
		$('#frmCopy #butAdd').die('click').live('click',function(){
			if ($('#frmCopy').validationEngine('validate'))   
			{ 			
				var printName = $('#frmCopy #cboPrintName').val();
				var comboName = $('#frmCopy #cboCombo').val();
				var columnId  = '';
				var rowId	  = '';	
				$('.printNameSpan').each(function(){
					//alert(printName.toUpperCase());
					if(($(this).html())==printName)
					{
						columnId = $(this).parent().index();
						//alert(columnId);
					}
				});
				
				$('.comboName').each(function(){
					if(($(this).val())==comboName)
					{
						rowId = $(this).parent().parent().index();
						//alert(rowId);
					}
				});
				var x = parseInt($('#frmCopy #txtRowId').val())+1;
				var y = parseInt($('#frmCopy #txtCellId').val());
				//alert(x +' - '+ y);
				$('#tblMain >tbody >tr:eq('+x+') >td:eq('+y+')').find('#tblGrid2 >tbody >tr:not(:first)').each(function(){
					var colorId = $(this).find('td:eq(0)').attr('id');///////////////// click loop // to
					var isFound = false;
					var gHtml = '';
					var objFrom = '';
					$('#tblMain >tbody >tr:eq('+(rowId+1)+') >td:eq('+(columnId)+')').find('#tblGrid2 >tbody >tr:not(:first)').each(function(){
						if($(this).find('td:eq(0)').attr('id')==colorId) // from
						{
							isFound = true;
							gHtml = $(this).find('td:eq(5)').html();
							objFrom = $(this);
						}
					});
					if(isFound)
					{
						$(this).find('td:eq(5)').html(gHtml);
						objTo  = $(this);
						objFrom.find('#tblGrid3 >tbody >tr:not(:last)').each(function(){
							var rowIndex 	= $(this).index();
							var inkType1 	= $(this).find('#cboTechnique').val();
							var shots1 		= $(this).find('#txtShots').val();
							var item1 		= $(this).find('#cboItem').val();
							var weight1 	= $(this).find('#txtWeight').val();
							//alert(inkType1);
							var objTr2 = objTo.find('td:eq(5)').find('#tblGrid3 >tbody >tr:eq('+rowIndex+')');
								objTr2.find('#cboTechnique').val(inkType1);
								objTr2.find('#txtShots').val(shots1);
								objTr2.find('#cboItem').val(item1);
								objTr2.find('#txtWeight').val(inkType1);
						});
						
						$(".removeRow").unbind('click');
						$(".removeRow").die('click').live('click',function(){
								removeRow(this);
						});
						$('.butAddNewTechnique').unbind('click');
						$(".butAddNewTechnique").die('click').live('click',addNewTechniqueRow);
					}
				});

				disablePopup();
			}
		});
		
		$('#frmCopy #butClose1').die('click').live('click',disablePopup);
		
	});		
}

function addColorstoGrid(rowId,cellId)
{
		//alert("row = "+rowId+"<br>cell = "+cellId);
	/*
		var tblType= document.getElementById('tblMain').rows[rowId].cells[cellId].childNodes[1];
		
		var rows = document.getElementById('tblColors').rows.length;
		tblType.rows[1].cells[0].innerHTML  = '';
	
	$('#tblColors .chkColors:checked').each(function(){
		
		var id = $(this).val();
		var name = $(this).parent().parent().find('td').eq(1).html();
		var y = tblType.rows.length;
		tblType.rows[1].cells[0].innerHTML  = tblType.rows[1].cells[0].innerHTML +'<span id="'+id+'">'+name+'</span>/'
	});*/
	
		$('#tblColors .chkColors:checked').each(function(){
		var tblType= document.getElementById('tblMain').rows[rowId].cells[cellId].childNodes[0];
		var rows = document.getElementById('tblColors').rows.length;

		var id = $(this).val();
		var name = $(this).parent().parent().find('td').eq(1).html();
		var y = tblType.rows.length;
		var found = false;
		for(var i=1;i<y;i++)
		{
			
			//alert(tblType.rows[i].cells[0].id);
			//alert(id);
			if(tblType.rows[i].cells[0].id==id)
			{
				found = true;break;
			}
		}
		if(!found)
		{
			//tblType= document.getElementById('tblMain').rows[rowId].cells[cellId].childNodes[0];
			
			var techniqueHtml = $('#cboTechniqueDumy').html();
			var spItem = $('#cboItemDumySPRM').html();
			tblType.insertRow(y);
			var html = "<td bgcolor=\"#ffffffff\" class=\"normalfnt\" id=\""+id+"\"><img src=\"images/del.png\" class=\"mouseover removeRow\" />"+name+"</td>"+
                  "<td bgcolor=\"#ffffffff\" class=\"normalfnt\" id=\"\"><select id=\"cboTechnique\" name=\"cboTechnique\" class=\"cboTechnique\" style=\"width:100px\">"+techniqueHtml +
                  "</select></td>"+
                  "<td bgcolor=\"#ffffffff\"><select name=\"select\" id=\"cboItem\" class=\"cboItem\" style=\"width:100px\">"+ spItem+
                  "</select></td>"+
                  "<td bgcolor=\"#ffffffff\">"+
                   " <input style=\"width:40px\"  type=\"text\" name=\"textfield5\" id=\"txtQty\" />"+
                 "</td>"+
                  "<td bgcolor=\"#ffffffff\" class=\"normalfnt\"><input style=\"width:20px\"  type=\"text\" name=\"textfield3\" id=\"txtSizeW\" />"+
                  "  W"+
                   "<input style=\"width:20px\"  type=\"text\" name=\"textfield4\" id=\"txtSizeH\" />"+
                   " H</td>"+
                  "<td colspan=\"3\" bgcolor=\"#ffffffff\">&nbsp;</td>";
			tblType.rows[y].innerHTML = html;
		}
	});
	//alert($('#tblTypeOfPrint >tbody >tr').length);
	setButtonEvents();
}

function setToTechnique(obj)
{
	//alert(1);
	var  typeRowId	= obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;
	var  mainRowId	= obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.rowIndex;

	var cellId 		= obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.cellIndex;
	popupWindow3('1');
	$('#popupContact1').load(basepath+'technique.php',function(){
		
		$('#frmTechnique #butAdd').die('click').live('click',function(){
			addToTechniqueGrid(mainRowId,typeRowId,cellId);
			disablePopup();
		});
		
		$('#frmTechnique #butClose1').die('click').live('click',disablePopup);
		
	});		
}

function addToTechniqueGrid(mainRowId,typeRowId,cellId)
{
		//alert("row = "+rowId+"<br>cell = "+cellId);
	$('#tblTechnique .chkTechnique:checked').each(function(){
		var tblTech= document.getElementById('tblMain').rows[mainRowId].cells[cellId].childNodes[0].rows[typeRowId].cells[1].childNodes[0];
		//var rows = document.getElementById('tblTypeOfPrint').rows.length;
		//alert(tblTech);
		var id 		= $(this).val();
		var name 	= $(this).parent().parent().find('td').eq(1).html();
		var typeId 	= $(this).parent().parent().find('td').eq(1).attr('id');
		
		var y = tblTech.rows.length;
		var found = false;
		for(var i=1;i<y;i++)
		{
			//alert(tblType.rows[i].cells[0].id);
			//alert(id);
			if(tblTech.rows[i].cells[0].id==id)
			{
				found = true;break;
			}
		}
		if(!found)
		{
			//tblType= document.getElementById('tblMain').rows[rowId].cells[cellId].childNodes[0];
			
			tblTech.insertRow(y);
			tblTech.rows[y].innerHTML = "<td class=\"normalfnt\" id=\""+id+"\"><img class=\"mouseover removeRow\" src=\"images/del.png\"/>"+name+"</td>"+
					"<td class=\"normalfnt\"><input class=\"qty\"  id=\"txtQty\" style=\"width:30px\" /></td>"+
                    "<td><select class=\"cboItem\" style=\"width:150px\"></select></td>";
		}
		
		if(typeId!=3)
		{
			tblTech.rows[y].cells[1].childNodes[0].disabled = true;//cboItemDumySPRM
			tblTech.rows[y].cells[2].childNodes[0].innerHTML = document.getElementById('cboItemDumy').innerHTML;
		}
		else
		{
			tblTech.rows[y].cells[2].childNodes[0].innerHTML = document.getElementById('cboItemDumySPRM').innerHTML;	
		}
		//$(".butTechnique").unbind('click');
		
	});
	setButtonEvents()
	//alert($('#tblTypeOfPrint >tbody >tr').length);
}

function addNewTechniqueRow()
{
	$(this).parent().parent().before(techniqueTableScript);
	var html 		= $('#cboTechniqueDumy2').html();
	var html2 		= $('#cboItemDumy2').html();
	var rowIndex 	= $(this).parent().parent().index();
	$(this).parent().parent().parent().find('tr:eq('+(rowIndex-1)+') #cboTechnique').html(html);
	$(this).parent().parent().parent().find('tr:eq('+(rowIndex-1)+') #cboItem').html(html2);
	
	$(".removeRow").unbind('click');
	$(".removeRow").die('click').live('click',function(){
			removeRow(this);
	});
}
function setButtonEvents()
{
	$(".butTypeOfPrint").unbind('click');
	$(".butTypeOfPrint").die('click').live('click',function(){
			setToTypeButton(this);
	});
	$(".butColors").unbind('click');
	$(".butColors").die('click').live('click',function(){
		setToColorGrid(this);
	});
	$(".butCopy").unbind('click');
	$(".butCopy").die('click').live('click',setToCopy);
	
	$(".removeRow").unbind('click');
	$(".removeRow").die('click').live('click',function(){
			removeRow(this);
	});
	
	$(".removeRow2").unbind('click');
	$(".removeRow2").die('click').live('click',function(){
			removeRow2(this);
	});
	
	$(".butColors").unbind('click');
	$(".butColors").die('click').live('click',function(){
		setToColorGrid(this);
	});
	
	$("#butTechnique").unbind('click');
	$("#butTechnique").die('click').live('click',function(){
			setToTechnique(this);
	});
	
	$('.butAddNewTechnique').unbind('click');
	$(".butAddNewTechnique").die('click').live('click',addNewTechniqueRow);
}

/*function inc(filename)
{				
	var body = document.getElementsByTagName('body').item(0);				
	script = document.createElement('script');				
	script.src = filename;
	script.type = 'text/javascript';				
	body.appendChild(script);
}	*/
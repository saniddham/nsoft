<?php 

	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];

	include "{$backwardseperator}dataAccess/Connector.php";
	//$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 			= $_REQUEST['requestType'];
	
	if($requestType=='loadRevisionNo')
	{
		$sampleNo 			= $_REQUEST['sampleNo'];
		$sampleYear 		= $_REQUEST['sampleYear'];
		
		$sql = "SELECT
					trn_sampleinfomations.intRevisionNo
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations.intSampleNo =  '$sampleNo' and
					intMarketingStatus = 1
				ORDER BY
					trn_sampleinfomations.intRevisionNo ASC
				";
		$result = $db->RunQuery($sql);
		echo "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			echo "<option value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
		}
	}
	else if($requestType=='loadMainDetails')
	{
		$sampleNo 			= $_REQUEST['sampleNo'];
		$sampleYear 		= $_REQUEST['sampleYear'];
		$revisionNo			= $_REQUEST['revisionNo'];
		
		
		$sql = "SELECT
				trn_sampleinfomations.dtDate,
				trn_sampleinfomations.strGraphicRefNo,
				trn_sampleinfomations.intCustomer,
				trn_sampleinfomations.strStyleNo,
				trn_sampleinfomations.intBrand,
				trn_sampleinfomations.verivide_d65,
				trn_sampleinfomations.verivide_cw,
				trn_sampleinfomations.verivide_tl84,
				trn_sampleinfomations.verivide_f,
				trn_sampleinfomations.macbeth_dl,
				trn_sampleinfomations.verivide_uv,
				trn_sampleinfomations.macbeth_cw,
				trn_sampleinfomations.macbeth_inca,
				trn_sampleinfomations.macbeth_tl84,
				trn_sampleinfomations.macbeth_uv,
				trn_sampleinfomations.macbeth_horizon,
				trn_sampleinfomations.dblCuringCondition_temp,
				trn_sampleinfomations.dblCuringCondition_beltSpeed,
				trn_sampleinfomations.dblPressCondition_temp,
				trn_sampleinfomations.dblPressCondition_pressure,
				trn_sampleinfomations.dblPressCondition_time,
				trn_sampleinfomations.strMeshCount,
				trn_sampleinfomations.strAdditionalInstructions,
				trn_sampleinfomations.intCompanyId,
				trn_sampleinfomations.intStatus
				FROM trn_sampleinfomations
				WHERE
				trn_sampleinfomations.intSampleNo =  '$sampleNo' AND
				trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
				trn_sampleinfomations.intRevisionNo =  '$revisionNo'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['dtDate']	 			= $row['dtDate'];
			$response['dtDeliveryDate'] 	= $row['dtDeliveryDate'];
			$response['strGraphicRefNo'] 	= $row['strGraphicRefNo'];
			$response['intCustomer'] 		= $row['intCustomer'];
			$response['strStyleNo'] 		= $row['strStyleNo'];
			$response['intBrand'] 			= $row['intBrand'];
			$response['dblSampleQty'] 		= $row['dblSampleQty'];
			$response['intGrade'] 			= $row['intGrade'];
			$response['strFabricType'] 		= $row['strFabricType'];
			$response['verivide_d65'] 		= $row['verivide_d65'];
			$response['verivide_cw'] 		= $row['verivide_cw'];
			$response['verivide_tl84'] 		= $row['verivide_tl84'];
			$response['verivide_f'] 		= $row['verivide_f'];
			$response['macbeth_dl'] 		= $row['macbeth_dl'];
			$response['verivide_uv'] 		= $row['verivide_uv'];
			$response['macbeth_cw'] 		= $row['macbeth_cw'];
			$response['macbeth_inca'] 		= $row['macbeth_inca'];
			$response['macbeth_tl84'] 		= $row['macbeth_tl84'];
			$response['macbeth_uv'] 		= $row['macbeth_uv'];
			$response['macbeth_horizon'] 	= $row['macbeth_horizon'];
			
			$response['dblCuringCondition_temp'] 		= $row['dblCuringCondition_temp'];
			$response['dblCuringCondition_beltSpeed'] 	= $row['dblCuringCondition_beltSpeed'];
			$response['dblPressCondition_temp'] 		= $row['dblPressCondition_temp'];
			$response['dblPressCondition_pressure'] 	= $row['dblPressCondition_pressure'];
			$response['dblPressCondition_time'] 		= $row['dblPressCondition_time'];
			$response['strMeshCount'] 					= $row['strMeshCount'];
			$response['strAdditionalInstructions'] 		= $row['strAdditionalInstructions'];
			$response['intCompanyId'] 					= $row['intCompanyId'];
			$response['intStatus'] 						= $row['intStatus'];
		}
		
		
		/////////// load technique /////////////////////////////
		$sql = "SELECT
					trn_sampleinfomations_technique.intInkTypeId
				FROM trn_sampleinfomations_technique
				WHERE
					trn_sampleinfomations_technique.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_technique.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_technique.intRevisionNo =  '$revisionNo' AND
					trn_sampleinfomations_technique.intCompanyId =  '$companyId'
				";
		$result = $db->RunQuery($sql);
		$arrTechnique;
		while($row=mysqli_fetch_array($result))
		{
			$arrTechnique[] = $row['intInkTypeId'];
		}
		$response['arrTechnique'] 	= $arrTechnique;
		
		/////////// load type of print /////////////////////////////
		$sql = "SELECT
					trn_sampleinfomations_typeofprint.intTypeOfPrint
				FROM trn_sampleinfomations_typeofprint
				WHERE
					trn_sampleinfomations_typeofprint.intSampleNo 	=  	'$sampleNo' AND
					trn_sampleinfomations_typeofprint.intSampleYear =  	'$sampleYear' AND
					trn_sampleinfomations_typeofprint.intRevisionNo =  	'$revisionNo' AND
					trn_sampleinfomations_typeofprint.intCompanyId 	=  	'$companyId'
				";
		$result = $db->RunQuery($sql);
		$arrType;
		while($row=mysqli_fetch_array($result))
		{
			$arrType[] = $row['intTypeOfPrint'];
		}
		$response['arrType'] 	= $arrType;
		
		/////////// get row count /////////////////////////////
		$sql = "SELECT DISTINCT
					trn_sampleinfomations_combos.strComboName
				FROM trn_sampleinfomations_combos
				WHERE
					trn_sampleinfomations_combos.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_combos.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_combos.intRevisionNo =  '$revisionNo'
				GROUP BY
					trn_sampleinfomations_combos.intPrintMode,
					trn_sampleinfomations_combos.intWashStanderd,
					trn_sampleinfomations_combos.strComboName
				";
		$result = $db->RunQuery($sql);
		//$row=mysqli_fetch_array($result);
		$response['rowCount'] 	= mysqli_num_rows($result);
		
		/////////// get columns count /////////////////////////////
		$sql = "SELECT DISTINCT
					trn_sampleinfomations_combos.strPrintName
				FROM trn_sampleinfomations_combos
				WHERE
					trn_sampleinfomations_combos.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_combos.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_combos.intRevisionNo =  '$revisionNo'
				";
		$result = $db->RunQuery($sql);
		$response['columnCount'] 	= mysqli_num_rows($result);
		
		////////// get combo rows /////////////////////////////
		$sql = "SELECT DISTINCT
					trn_sampleinfomations_combos.strComboName,
					trn_sampleinfomations_combos.intPrintMode,
					trn_sampleinfomations_combos.intWashStanderd,
					trn_sampleinfomations_combos.intGroundColor
				FROM
					trn_sampleinfomations_combos
				WHERE
					trn_sampleinfomations_combos.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_combos.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_combos.intRevisionNo =  '$revisionNo'
				order by strComboName
				";
		$result = $db->RunQuery($sql);
		$arrCombo;
		while($row=mysqli_fetch_array($result))
		{
			$data['modeId'] 	= $row['intPrintMode'];
			$data['washId']		= $row['intWashStanderd'];
			$data['comboName'] 	= $row['strComboName'];
			$data['groundId'] 	= $row['intGroundColor'];
			
			$arrCombo[] = $data;
			
		}
		$response['arrCombo'] 	= $arrCombo;
		
		////////// get print name data /////////////////////////////
		$sql = "SELECT DISTINCT
					trn_sampleinfomations_combos.strPrintName,
					trn_sampleinfomations_combos.intTechnique,
				
					trn_sampleinfomations_combos.intSize_width,
					trn_sampleinfomations_combos.intSize_height,
					trn_sampleinfomations_combos.intPart
				FROM trn_sampleinfomations_combos
				WHERE
					trn_sampleinfomations_combos.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_combos.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_combos.intRevisionNo =  '$revisionNo'
				ORDER BY
					trn_sampleinfomations_combos.strPrintName ASC
				";
		$result = $db->RunQuery($sql);
		$arrPrintName;
		while($row=mysqli_fetch_array($result))
		{
			$print1['name'] 			= $row['strPrintName'];
			$print1['techId']			= $row['intTechnique'];
			$print1['intSize_width']	= $row['intSize_width'];
			$print1['intSize_height']	= $row['intSize_height'];
			$print1['intPart']			= $row['intPart'];
			
			$arrPrintName[] = $print1;
			
		}
		$response['arrPrintName'] 	= $arrPrintName;
		
		////////// get colors array's /////////////////////////////
		$sql = "SELECT 
					trn_sampleinfomations_combos.strComboName,
					trn_sampleinfomations_combos.strPrintName,
					trn_sampleinfomations_combos.intColor
				FROM trn_sampleinfomations_combos
				WHERE
					trn_sampleinfomations_combos.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_combos.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_combos.intRevisionNo =  '$revisionNo'
				ORDER BY
					trn_sampleinfomations_combos.strComboName ASC,
					trn_sampleinfomations_combos.strPrintName ASC

				";
		$result = $db->RunQuery($sql);
		$arrInkColor;
		while($row=mysqli_fetch_array($result))
		{
			$color1['comboName'] 		= $row['strComboName'];
			$color1['printName'] 		= $row['strPrintName'];
			$color1['color'] 			= $row['intColor'];
			
			$arrInkColor[] = $color1;
			
		}
		$response['arrInkColor'] 	= $arrInkColor;
		
		
		echo json_encode($response);
	}
	else if($requestType=='loadSampleNoCombo')
	{
		$sampleYear = $_REQUEST['sampleYear'];
		$sql = "SELECT DISTINCT
					trn_sampleinfomations.intSampleNo
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleYear =  '$sampleYear'
				order by intSampleNo
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			echo "<option value=\"".$row['intSampleNo']."\">".$row['intSampleNo']."</option>";
		}
	}
	?>
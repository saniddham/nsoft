<?php
session_start();
$backwardseperator 		= "../../../../../";
$thisFilePath 			= $_SERVER['PHP_SELF'];
$locationId				= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];
$userId					= $_SESSION["userId"];

include_once "{$backwardseperator}dataAccess/permisionCheck.inc";
include_once "../../../../../class/customerAndOperation/sample/sampleRequisition/cls_sample_requisition_get.php";
include_once "../../../../../class/cls_commonFunctions_get.php";
include_once "../../../../../class/cls_commonErrorHandeling_get.php";

$obj_requisition_get	= new Cls_sample_requisition_get($db);
$obj_commonErrHandle	= new cls_commonErrorHandeling_get($db);
$obj_common				= new cls_commonFunctions_get($db);

$programCode			= 'P0827';
$requisitionNo			= $_REQUEST["requisitionNo"];
$requisitionYear		= $_REQUEST["requisitionYear"];

$header_arr				= $obj_requisition_get->loadHeaderData($requisitionNo,$requisitionYear,'RunQuery');
$detail_result			= $obj_requisition_get->loadDetailData($requisitionNo,$requisitionYear,'RunQuery');

$intStatus				= $header_arr['STATUS'];
$levels					= $header_arr['APPROVE_LEVELS'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_save($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_save			= $permition_arr['permision'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_cancel($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_cancel		= $permition_arr['permision'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_confirm		= $permition_arr['permision'];


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<title>Sample Requisition</title>

<link rel="stylesheet" type="text/css" href="../../../../../css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="../../../../../css/button.css"/>
<link rel="stylesheet" type="text/css" href="../../../../../css/promt.css"/>

<script src="../../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

</head>
<body>
<form id="frmSampleRequest" name="frmSampleRequest" autocomplete="off" method="post">
<table width="100%" border="0" align="center">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
        <script type="application/javascript" src="sampleRequest_js.js"></script>
	</tr> 
</table>
<div align="center">
	<div class="trans_layoutD">
	<div class="trans_text">Sample Requisition</div>
	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <tr>
    	<td>
        <table width="100%" border="0" > 
            <tr>
              <td class="normalfnt">Requisition No</td>
              <td><input name="txtRequisitionNo" type="text" disabled="disabled" id="txtRequisitionNo" style="width:80px" value="<?php echo $requisitionNo; ?>" />&nbsp;<input name="txtRequisitionYear" type="text" disabled="disabled" id="txtRequisitionYear" style="width:50px" value="<?php echo $requisitionYear; ?>" /></td>
            </tr>
            <tr>
                <td width="24%" class="normalfnt">Graphic Ref No <span class="compulsoryRed">*</span></td>
                <td width="76%"><input value="<?php echo $header_arr['GRAPHIC']; ?>" name="txtGraphicRefNo" type="text" class="validate[required,maxSize[200]]" id="txtGraphicRefNo" style="width:400px" /></td>
            </tr>
            <tr>
            	<td class="normalfnt">Customer <span class="compulsoryRed">*</span></td>
            	<td><select name="cboCustomer" id="cboCustomer" class="validate[required]" style="width:400px">
                <option value=""></option>
                <?php
					$result = $obj_requisition_get->getCustomer($userId);
					while($row = mysqli_fetch_array($result))
					{
						if($header_arr['CUSTOMER']==$row['intId'])
							echo "<option selected value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
                ?>
                </select></td>
            </tr>
            <tr>
            	<td class="normalfnt">Style No</td>
            	<td><input value="<?php echo $header_arr['STYLE']; ?>"  name="txtStyleNo" type="text" class="validate[maxSize[50]]"  id="txtStyleNo" style="width:400px" /></td>
            </tr>
            <tr>
            	<td class="normalfnt">Brand <span class="compulsoryRed">*</span></td>
           		<td><select class="validate[required]" name="cboBrand" id="cboBrand" style="width:400px">
                 <?php
					$result = $obj_requisition_get->getBrand($header_arr['CUSTOMER']);
					while($row = mysqli_fetch_array($result))
					{
						if($header_arr['BRAND']==$row['intId'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
                ?>
           		</select></td>
            </tr>
            <tr>
            	<td class="normalfnt">Marketer <span class="compulsoryRed">*</span></td>
            	<td ><select name="cboMarketer" id="cboMarketer" class="validate[required]" style="width:400px">
            	<option value=""></option>
				<?php
                  
                    $result = $obj_requisition_get->getMarketer();
                    while($row = mysqli_fetch_array($result))
                    {
                        if($header_arr['MARKETER']==$row['intUserId'])
                            echo "<option selected=\"selected\" value=\"".$row['intUserId']."\">".$row['strUserName']."</option>";	
                        else
                            echo "<option value=\"".$row['intUserId']."\">".$row['strUserName']."</option>";	
                    }
                ?>
            </select></td>
            </tr>
            <tr>
            	<td class="normalfnt">Remarks</td>
            	<td ><textarea name="txtRemarks" id="txtRemarks" cols="45" rows="5" style="width:400px"><?php echo $header_arr['REMARKS']; ?></textarea></td>
            </tr>
            <tr>
                <td class="normalfnt">&nbsp; </td>
                <td  class="normalfnt">
                <table  border="0" class="bordered" id="tblSampleType" style="width:400px">
                <thead>
                	<tr>
                 		<th colspan="4" style="text-align:left">Sample Types<div style="float:right"><a class="button white small" id="butInsertRow">Add New Row</a></div></th>
                	</tr>
                    <tr>
                    	<th width="10">Del</th>
                    	<th width="210">Sample Types</th>
                    	<th width="100">Sample Qty</th>
                    	<th width="80">Req. Days</th>
                    </tr>
                </thead>
  				<tbody>
                <?php
					$i = 1;
					if($requisitionNo!='' && $requisitionYear!='')
					{
						while($rowD = mysqli_fetch_array($detail_result))
						{
				?>
                            <tr <?php echo($i==1?'class="cls_tr_firstRow"':''); ?>>
                                <td style="text-align:center"><img border="0" src="../../../../../images/del.png" alt="Save" name="butDel" class="mouseover clsDel" id="butDel" tabindex="24"/></td>
                                <td><select name="cboSampleType" id="cboSampleType" class="validate[required] clsSampleType" style="width:100%">
                                <option value=""></option>
                                <?php
                                $result = $obj_requisition_get->getSampleType();
                                while($row = mysqli_fetch_array($result))
                                {
									if($rowD['SAMPLE_TYPE']==$row['intId'])
										echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
									else
										echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
                                }
                                ?>
                                </select></td>
                                <td><input type="text" name="txtSampleQty" id="txtSampleQty" class="validate[required,custom[number]] clsSampleQty" style="width:100%;text-align:right" value="<?php echo $rowD['QTY'];?>" /></td>
                                <td><input type="text" name="txtRequiredDays" id="txtRequiredDays" class="validate[required,custom[number]] clsRequiredDays" style="width:100%;text-align:right" value="<?php echo $rowD['MINIMUM_DAYS'];?>" /></td>
                            </tr>		
                <?php
						$i++;
						}
					}
					else
					{
				?>
                    <tr class="cls_tr_firstRow">
                    	 <td style="text-align:center"><img border="0" src="../../../../../images/del.png" alt="Save" name="butDel" class="mouseover clsDel" id="butDel" tabindex="24"/></td>
                        <td><select name="cboSampleType" id="cboSampleType" class="validate[required] clsSampleType" style="width:100%">
                        <option value=""></option>
                        <?php
                            $result = $obj_requisition_get->getSampleType();
                            while($row = mysqli_fetch_array($result))
                            {
                            	echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
                            }
                        ?>
                        </select></td>
                        <td><input type="text" name="txtSampleQty" id="txtSampleQty" class="validate[required,custom[number]] clsSampleQty" style="width:100%;text-align:right" /></td>
                    	<td><input type="text" name="txtRequiredDays" id="txtRequiredDays" class="validate[required,custom[number]] clsRequiredDays" style="width:100%;text-align:right"  /></td>
                    </tr>
                    <?php
					}
					?>
				</tbody>
                </table>
                </td>
			</tr>
		</table></td>
    </tr>
    <tr>
        <td height="34">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                <tr>
                    <td align="center"><a class="button white medium" id="butNew">New</a><a class="button white medium" id="butSave" <?php if($permision_save!=1){ ?>style="display:none"<?php } ?>>Save</a><a class="button white medium" id="butConfirm"  <?php if($permision_confirm!=1){ ?> style="display:none"<?php } ?>>Approve</a><a class="button white medium" id="butCancel" <?php if($permision_cancel!=1){ ?>  style="display:none"<?php } ?>>Cancel</a><a class="button white medium" id="butReport">Report</a><a href="../../../../../main.php" class="button white medium" id="butSave">Close</a></td>
                </tr>
            </table>
        </td>
     </tr>
	</table>
	</div>
</div>
</form>
</body>
</html>

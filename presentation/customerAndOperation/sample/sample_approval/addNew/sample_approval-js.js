var basepath		= 'presentation/customerAndOperation/sample/sample_approval/addNew/';

$(document).ready(function() {	
	
	$('#butNew').die('click').live('click',function(){	
		window.location.href = 'main.php';
	});
	
	$('#butSave').die('click').live('click',function(){
		save_details();	
 	});
	
	$('#cboSampleNo').die('change').live('change',function(){
		clearGrid();
		loadRevisionNo();
	});
	
	$('#cboRevisionNo').die('change').live('change',function(){
		clearGrid();
		document.frmSample_approval.submit();
	});
	
	$('#cboGraphicNo').die('change').live('change',function(){
		clearGrid();
		loadSampleNos();		
		var intSampleNo = $('#cboSampleNo').val();
		$('#cboSampleNo').val(intSampleNo);
		$('#divPicture').html('');
		return;
	});

	$('#cboYear').die('change').live('change',function(){
		document.location.href = 'main.php?cboYear='+$('#cboYear').val();
	});


});

	function loadSampleNos()
	{
		var url = basepath+"sample_approval-db.php?requestType=loadSampleNo&styleNo="+URLEncode($('#cboGraphicNo').val())+"&sampleYear="+$('#cboYear').val();
		var obj = $.ajax({url:url,async:false});	
		$('#cboSampleNo').html(obj.responseText);	
		$('#cboRevisionNo').html('');	
	}
	
	function loadRevisionNo()
	{
		var url = basepath+"sample_approval-db.php?requestType=loadRevisionNo&sampleNo="+$('#cboSampleNo').val()+"&sampleYear="+$('#cboYear').val();
		var obj = $.ajax({url:url,async:false});	
		$('#cboRevisionNo').html(obj.responseText);
	}
	
	function loadSampleNos()
	{
		var url = basepath+"sample_approval-db.php?requestType=loadSampleNo&styleNo="+URLEncode($('#cboGraphicNo').val())+"&sampleYear="+$('#cboYear').val();
		var obj = $.ajax({url:url,async:false});	
		$('#cboSampleNo').html(obj.responseText);	
		$('#cboRevisionNo').html('');	
		
	}
	function loadGraphicNos()
	{
		var url = basepath+"sample_approval-db.php?requestType=loadGraphicNos&sampleYear="+$('#cboYear').val();
		var obj = $.ajax({url:url,async:false});	
		$('#cboGraphicNo').html(obj.responseText);	
		$('#cboRevisionNo').html('');	
		
	}
	
	function save_details()
	{
		//alert(2);
		if (!$('#frmSample_approval').validationEngine('validate'))   
			return;
			
		//alert(3);
		var arrJson = "";
 		//alert(4);
		var i=0;
		var j=0;
		var sampleNo 	= $('#cboSampleNo').val();
		var sampleYear	= $('#cboYear').val();
		var revisionNo	= $('#cboRevisionNo').val();
		//alert(5);
 		//alert(6);
		$('.clsValue').each(function(){
			var printName  	= $(this).parent().attr('id');	
			var comboName	= $(this).attr('title');	
			var approved		= $(this).is(':checked');
			if(approved==true){
				approved=1;
			}
			else{
				approved=0;
			}
			
			
			arrJson+=((i++)=='0'?'':',')+'{"combo":"'+URLEncode(comboName)+'","print":"'+URLEncode(printName)+'","approved":"'+approved+'"}';
		});	
 		
		var url = basepath+"sample_approval-db.php?requestType=saveDetails&sampleNo="+sampleNo+"&sampleYear="+sampleYear+"&revNo="+revisionNo;
		$.ajax({
			url:url,
			async:false,
			dataType:'json',
			type:'post',
			data:"data=["+arrJson+"]",
 			success:function(json){
												
				$('#frmSample_approval #butSave').validationEngine('showPrompt', json.msg,json.type );
				if(json.type=='pass')
				{
 					var t=setTimeout("alertx()",1000);
 					return;
				}
				hideWaiting_c();
			},
		error:function(xhr,status){
				jQuery('#frmSample_approval #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				var t=setTimeout("alertx()",1000);
 				return;
			}
			
		});
	}
	
	function clearGrid()
	{
		var rowCount = document.getElementById('tblMain').rows.length;
		for(var i=1;i<rowCount;i++)
		{
				document.getElementById('tblMain').deleteRow(1);
		}
	}
	
	function alertx()
	{
		jQuery('#frmSample_approval #butSave').validationEngine('hide');
	}

<?php 
	ini_set('display_errors',0);
	session_start();
	$backwardseperator	= "../../../../../";
	$mainPath			= $_SESSION['mainPath'];
	$userId				= $_SESSION['userId'];
	$companyId			= $_SESSION['CompanyID'];
	
	include "../../../../../class/cls_mail.php";
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once $_SESSION['ROOT_PATH']."class/customerAndOperation/sample/cls_sample_approval_db.php";
	
	$objsampApproval		= new cls_sample_approval_db($db);
  	$requestType 			= $_REQUEST['requestType'];
	
	
	if($requestType	==	'loadRevisionNo')
	{
		$sampleNo	=	 $_REQUEST['sampleNo'];
		$sampleYear	=	 $_REQUEST['sampleYear'];
		
		$sql	=	$objsampApproval->trn_sampleinfomations_revision_select($sampleYear,$sampleNo,$companyId);
 		$result	=	$db->RunQuery($sql);
		echo "<option value=\"\"></option>";
		while($row	=	mysqli_fetch_array($result))
		{
			echo "<option value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
		}
	}

	elseif($requestType	==	'loadGraphicNos')
	{
		$styleNo	=	$_REQUEST['styleNo'];
		$sampleYear	=	$_REQUEST['sampleYear'];
		
		$sql	=	$objsampApproval->trn_sampleinfomations_graphic_select($sampleYear,$companyId);
 		$result	=	$db->RunQuery($sql);
		echo "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			echo "<option value=\"".$row['strGraphicRefNo']."\">".$row['strGraphicRefNo']."</option>";
		}
	}
	elseif($requestType	==	'loadSampleNo')
	{
		$styleNo		=	$_REQUEST['styleNo'];
		$sampleYear		=	$_REQUEST['sampleYear'];
		
	
		echo $sql	=	$objsampApproval->trn_sampleinfomations_sampleNo_select($sampleYear,$companyId,$graphic,$styleNo);
 		$result =	$db->RunQuery($sql);
		echo "<option value=\"\"></option>";
		while($row	=	mysqli_fetch_array($result))
		{
			echo "<option value=\"".$row['intSampleNo']."\">".$row['intSampleNo']."</option>";
		}
	}
	
 	else if($requestType	==	'saveDetails')
	{
  		$db->begin();
		
		$arrData 	= json_decode($_REQUEST['data'],true);
 		$sampleNo 	= $_REQUEST['sampleNo'];
		$sampleYear	= $_REQUEST['sampleYear'];
 		$revNo		= $_REQUEST['revNo'];
  
  		foreach($arrData as $x)
		{
			$edit	=	0;
			$sql	=	$objsampApproval->getSampleApprovalRecordSavedStatus($sampleNo,$sampleYear,$revNo,$x['combo'],$x['print']);
			$result =	$db->RunQuery2($sql);
 			$row	=	mysqli_fetch_array($result);
			if($row['intSampleNo']	!=	'')
			{
				$edit=1;
 			}
			
			if($edit	==	1){
				$sql		=	$objsampApproval->trn_sampleinfomations_approval_update($sampleNo,$sampleYear,$revNo,$x['combo'],$x['print'],$x['approved'],$userId);
				$result1	=	$db->RunQuery2($sql);
			}
			else{
				$sql		=	$objsampApproval->trn_sampleinfomations_approval_insert($sampleNo,$sampleYear,$revNo,$x['combo'],$x['print'],$x['approved'],$userId);
				$result1	=	$db->RunQuery2($sql);
			}
			if($$result1){
				$rollBackFlag	=	1;
				$rollBackMsg	=	"Save failed";
				
			}
		}
  	
		if($rollBackFlag	==	1){
			$db->rollback();
			$response['type']	=	'fail';
			$response['msg']	=	$rollBackMsg;
			$response['q']		=	$sql;
		}
		else if(($result1)){
 			$db->commit();
			$response['type'] 		=	'pass';
			$response['msg'] 		=	'Saved successfully.';
 		}
		else{
			$db->rollback();
			$response['type'] 		=	'fail';
			$response['msg'] 		=	$db->errormsg;
			$response['q'] 			=	$sql;
		}
 	echo json_encode($response);
		
 	}
	
	function getRecordStatus($sampleNo,$sampleYear,$revNo,$combo){
		global $db;
		$val	=	0;
		$SQL	=	"SELECT * FROM 
							WHERE
								trn_sampleinfomations_approval.intSampleNo =  '$sampleNo' AND
								trn_sampleinfomations_approval.intSampleYear =  '$sampleYear' AND
								trn_sampleinfomations_approval.intRevisionNo =  '$revNo' AND 
								.trn_sampleinfomations_approvalstr.Combo = '$combo';
								";	
			$result = $db->RunQuery($sql);
			if($result){
				$val	=	1;
 			}
		return $val;
	}
?>
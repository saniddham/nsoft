<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId			= $sessions->getLocationId();

require_once "class/customerAndOperation/sample/cls_sample_approval_db.php";		$objsampApproval	=	new cls_sample_approval_db($db);

$x_graphicNo 		= (!isset($_REQUEST["cboGraphicNo"])?'':$_REQUEST["cboGraphicNo"]);
$x_sampleNo 		= (!isset($_REQUEST["cboSampleNo"])?'':$_REQUEST["cboSampleNo"]);
$x_sampleYear 		= (!isset($_REQUEST["cboYear"])?'':$_REQUEST["cboYear"]);
$x_revisionNo		= (!isset($_REQUEST["cboRevisionNo"])?'':$_REQUEST["cboRevisionNo"]);

if($x_sampleYear	==	''){
	$x_sampleYear	=	date('Y');
}
 
if($x_graphicNo	==	'')
{
	$sql1			= $objsampApproval->trn_sampleinfomations_select($x_sampleNo,$x_sampleYear,$x_revisionNo);
	$result1		= $db->RunQuery($sql1);
	$row1			= mysqli_fetch_array($result1);
	$x_graphicNo 	= $row1['strGraphicRefNo'];
}

 ?>
<head>
<title>Sample Approval</title>
</head>

<body>
<form id="frmSample_approval" name="frmSample_approval" method="get" action="">
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Sample Approval</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
         <td colspan="3"><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="69%" height="22" class="normalfnt"><table class="tableBorder_allRound" bgcolor="#B7D6F9" align="center" width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="2%" class="normalfnt">&nbsp;</td>
                <td width="10%" height="33" class="normalfnt">Sample Year</td>
                <td width="10%"><select name="cboYear" id="cboYear" style="width:60px"  class="validate[required]">
                  <?php
				    $d = date('Y');
				  	for($d;$d>=2012;$d--)
					{
						if($d==$x_sampleYear)
							echo "<option selected=\"selected\" id=\"$d\" >$d</option>";	
						else
							echo "<option id=\"$d\" >$d</option>";	
					}
				  ?>
                </select></td>
                <td width="8%" class="normalfnt">Graphic No</td>
                <td width="21%"><select name="cboGraphicNo" id="cboGraphicNo" style="width:140px">
                  <option value=""></option>
                  <?php
					echo $sql=$objsampApproval->trn_sampleinfomations_graphic_select($x_sampleYear,$locationId);
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$strGraphicRefNo = $row['strGraphicRefNo'];
						if($x_graphicNo==$strGraphicRefNo)
							echo "<option selected=\"selected\" value=\"$strStyle\">$strGraphicRefNo</option>";
						else
							echo "<option value=\"$strGraphicRefNo\">$strGraphicRefNo</option>";
					}
				?>
                </select></td>
                <td width="9%" class="normalfnt">Sample No</td>
                <td width="16%"><select name="cboSampleNo" id="cboSampleNo" style="width:120px"  class="validate[required]">
                  <option value=""></option>
                  <?php
				  if($x_sampleYear==''){
					$x_sampleYear = date('Y');
				  }
					$sql=$objsampApproval->trn_sampleinfomations_sampleNo_select($x_sampleYear,$locationId,$x_graphicNo,$style);
 					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
					echo 	$no = $row['intSampleNo'];
					echo $x_sampleNo;
						if($no==$x_sampleNo)
							echo "<option selected=\"selected\" value=\"$no\">$no</option>";
						else
							echo "<option value=\"$no\">$no</option>";
					}
				?>
                </select></td>
                <td width="9%" class="normalfnt">Revision No</td>
                <td width="15%"><select name="cboRevisionNo" id="cboRevisionNo" style="width:60px" class="validate[required]">
                  <option value=""></option>
                  <?php
					$sql=$objsampApproval->trn_sampleinfomations_revision_select($x_sampleYear,$x_sampleNo,$locationId);
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$no = $row['intRevisionNo'];
						if($no==$x_revisionNo)
							echo "<option selected=\"selected\" value=\"$no\">$no</option>";
						else
							echo "<option value=\"$no\">$no</option>";
					}
				?>
                </select></td>
                </tr>
          </table></td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
          </tr>
          </table></td>
       </tr>
      <tr>
      <td width="10%"></td>
        <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="1" id="tblMain"  class="bordered">
         <?php
				$sql=$objsampApproval->trn_sampleinfomations_sampleDetails_select($x_sampleYear,$x_sampleNo,$locationId);
				$result = $db->RunQuery($sql);
				$numOfPrints=0;
 				while($row=mysqli_fetch_array($result))
				{
					$numOfPrints++;
				}
		 ?>
          <tr>
            <th colspan="<?php echo ($numOfPrints+1);?>" align="left" class="normalfnt"><b>Samples</b></th></tr>
          <tr>
            <td width="15%" bgcolor="#FFFFFF">&nbsp;</td>
            <?Php
				$sql=$objsampApproval->trn_sampleinfomations_sampleDetails_select($x_sampleYear,$x_sampleNo,$locationId);
				$result = $db->RunQuery($sql);
				$arrPrint ='';
				while($row=mysqli_fetch_array($result))
				{
					$arrPx[]=$row['strPrintName'];
			?>
            <td width="156" bgcolor="#FFFFFF" align="center">
              <?php
			if($x_sampleNo!='')
			{
				echo "<img id=\"saveimg\" style=\"width:154px;height:98px;\" src=\"documents/sampleinfo/samplePictures/$x_sampleNo-$x_sampleYear-".$row['intRevisionNo']."-".substr($row['strPrintName'],6,1).".png\" /></br><span class=\"normalfntMid\"><b>".$row['strPrintName']."</b></span>";	
			}
			 ?>
              </td>
            <?php
				$arrPrint[]=$row['strPrintName'];
				}
			?>
            
            </tr>
          
          <?php
 			$sql=$objsampApproval->trn_sampleinfomations_combo_select($x_sampleNo,$x_sampleYear,$x_revisionNo);
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
		  ?>
          <tr>
            <td align="center" bgcolor="#E0F8AB" class="reportSubHeader" style="font-size:12px" ><?php echo $row['strComboName']; ?></td>
            <?php
			 	$printIndex = 0;
				$i=0;
				foreach($arrPrint as $x)
				{
					$checked='';
					$sql1=$objsampApproval->getSampleApprovalRecordSavedStatus($x_sampleNo,$x_sampleYear,$x_revisionNo,$row['strComboName'],$x);
					$result1 = $db->RunQuery($sql1);
					$row1=mysqli_fetch_array($result1);
					$val=$row1['intApproved'];
					if($val==1){
					$checked="checked=checked";
					}
 			?>
            <td align="center" bgcolor="#FFFFFF" id="<?PHP echo $x; ?>" class="normalfntMid"><input type="checkbox" <?php echo $checked; ?> class="clsValue"   title="<?php echo $row['strComboName'];  ?>" id="<?php echo ++$i; ?>"  name="" /></td>
            <?php
				}
			 ?>
            </tr>
          <?php
			}
		  ?>
          </table></td>
      <td width="10%"></td>
      </tr>
<tr>
  <td colspan="3" align="center" class="">&nbsp;</td>
</tr>
      <tr>
        <td colspan="3" align="center" class="tableBorder_allRound"><a id="butNew" class="button white medium" style="" name="butSave"> New </a><a id="butSave" class="button white medium" style="" name="butSave"> Save </a><a id="butClose" class="button white medium" style="" name="butClose" href="main.php"> Close </a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body> 
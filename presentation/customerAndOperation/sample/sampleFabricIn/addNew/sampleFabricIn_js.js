var basePath	="presentation/customerAndOperation/sample/sampleFabricIn/addNew/";
var reportId	="972";


// JavaScript Document
$(document).ready(function(){
	
	try
	{
		$('#frmSampleFabricIn').validationEngine();
		$('#frmRptFabricIn').validationEngine();
		$("#frmSampleFabricIn .chosen-select").chosen({allow_single_deselect: true , no_results_text: "Oops, nothing found!"});
		$("#frmSampleFabricIn #cboGraphicNo").chosen().change(loadSampleOrderDetails); 
	}
	catch(err)
	{
		
	}
	//$('#frmSampleFabricIn #cboSampleOrder').live('change',loadSampleOrderDetails);
	$('#frmSampleFabricIn #butSave').die('click').live('click',saveData);
	$('#frmSampleFabricIn #butNew').die('click').live('click',clearAll);
	
	$('#frmSampleFabricIn #butConfirm').die('click').live('click',Confirm);
	$('#frmSampleFabricIn #butCancel').die('click').live('click',Cancel);
	$('#frmSampleFabricIn #butReport').die('click').live('click',loadReport);
	
	$('#frmRptFabricIn #butRptConfirm').die('click').live('click',ConfirmRpt);
	$('#frmRptFabricIn #butRptReject').die('click').live('click',RejectRpt);
	$('#frmRptFabricIn #butRptCancel').die('click').live('click',CancelRpt);
});

function loadSampleOrderDetails()
{
	clearData()
	
	if($(this).val()=='')
		return;
	
	var url 	= basePath+"sampleFabricIn_db.php?requestType=loadSampleOrderDetails";
	var data 	= "sampleOrderNoArr="+$(this).val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmSampleFabricIn #txtCustomer').val(json.customer);
				$('#frmSampleFabricIn #txtRequisitionNo').val(json.conReqNo);
				$('#frmSampleFabricIn #txtLocation').val(json.customerLocation);
				$('#frmSampleFabricIn #txtOrderNo').val(json.conOrderNo);
				$('#frmSampleFabricIn #txtStyle').val(json.StyleNo);
				$('#frmSampleFabricIn #txtSampleOrderRemarks').val(json.sampleOrderRemarks);
				
				var lengthDetail   = json.arrDetailData.length;
				var arrDetailData  = json.arrDetailData;	
				for(var i=0;i<lengthDetail;i++)
				{
					if(arrDetailData[i]['Combo']!=null)
					{
						var combo			= arrDetailData[i]['Combo'];
						var partId			= arrDetailData[i]['PartId'];	
						var partName		= arrDetailData[i]['partName'];
						var sampleTypeName	= arrDetailData[i]['sampleTypeName'];
						var sampleTypeId	= arrDetailData[i]['SampleTypeId'];
						var grade			= arrDetailData[i]['Grade'];
						var size			= arrDetailData[i]['Size'];
						var orderQty		= arrDetailData[i]['orderQty'];
						var requiredDate	= arrDetailData[i]['RequiredDate'];
						var fabInBalQty		= arrDetailData[i]['fabInBalQty'];
						var totInQty_app	= arrDetailData[i]['totInQty_app'];
						var totInQty_pen	= arrDetailData[i]['totInQty_pen'];
						createGrid(combo,partId,partName,sampleTypeName,sampleTypeId,grade,size,orderQty,requiredDate,fabInBalQty,totInQty_app,totInQty_pen);
					}
				}
			}
	});	
}
function clearData()
{
	$('#frmSampleFabricIn #txtCustomer').val('');
	$('#frmSampleFabricIn #txtRequisitionNo').val('');
	$('#frmSampleFabricIn #txtLocation').val('');
	$('#frmSampleFabricIn #txtOrderNo').val('');
	$('#frmSampleFabricIn #txtStyle').val('');
	$('#frmSampleFabricIn #txtSampleOrderRemarks').val('');
	$('#frmSampleFabricIn #txtRemarks').val('');
	$('#frmSampleFabricIn #tblSampOrders tr:gt(0)').remove();
}
function createGrid(combo,partId,partName,sampleTypeName,sampleTypeId,grade,size,orderQty,requiredDate,fabInBalQty,totInQty_app,totInQty_pen)
{
	var tbl 		= document.getElementById('tblSampOrders');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);
	
	var cell 		= row.insertCell(0);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsCombo';
	cell.innerHTML 	= combo;
	
	var cell 		= row.insertCell(1);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsPart';
	cell.id			= partId;
	cell.innerHTML 	= partName;
	
	var cell 		= row.insertCell(2);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsSampleType';
	cell.id			= sampleTypeId;
	cell.innerHTML 	= sampleTypeName;
	
	var cell 		= row.insertCell(3);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsGrade';
	cell.innerHTML 	= grade;
	
	var cell 		= row.insertCell(4);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsSize';
	cell.id			= size;
	cell.innerHTML 	= (size==''?'&nbsp;':size);
	
	var cell 		= row.insertCell(5);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsOrderQty';
	cell.innerHTML 	= orderQty;
	
	var cell 		= row.insertCell(6);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsTotInQty_app';
	cell.innerHTML 	= totInQty_app;
	
	var cell 		= row.insertCell(7);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsTotInQty_pen';
	cell.innerHTML 	= totInQty_pen;
	
	var cell 		= row.insertCell(8);
	cell.setAttribute("style",'text-align:center');
	//tempororily comment this on 2016-10-26 as sashika needs unlmited fabric in facility.
	cell.innerHTML 	= "<input name=\"txtFabricInQty\" type=\"text\"  class=\"validate[custom[number],max["+fabInBalQty+"]] clsFabInQty\" id=\"txtFabricInQty\" style=\"width:100%;text-align:right\" />";
	/*cell.innerHTML 	= "<input name=\"txtFabricInQty\" type=\"text\"  class=\"validate[custom[number]] clsFabInQty\" id=\"txtFabricInQty\" style=\"width:100%;text-align:right\" />"; */
	
	var cell 		= row.insertCell(9);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsRequireDate';
	cell.innerHTML 	= requiredDate;
}
function saveData()
{
	showWaiting();
	var fabricInNo			= $('#txtSmpFabricInNo').val();
	var fabricInYear		= $('#txtSmpFabricInYear').val();
	var fabricInDate		= $('#dtDate').val();
	var sampleOrderArr		= $('#cboGraphicNo').val();
	var remarks				= $('#txtRemarks').val();
	
	if($('#frmSampleFabricIn').validationEngine('validate'))
	{
		var data = "requestType=saveData";
		var arrHeader = "[{";
							arrHeader += '"fabricInNo":"'+fabricInNo+'",' ;
							arrHeader += '"fabricInYear":"'+fabricInYear+'",' ;
							arrHeader += '"fabricInDate":"'+fabricInDate+'",' ;
							arrHeader += '"sampleOrderArr":"'+sampleOrderArr+'",' ;
							arrHeader += '"remarks":'+URLEncode_json(remarks)+'';

			arrHeader += "}]";
		
		var chkStatus	= false;
		var arrDetails	= "";
		
		$('#frmSampleFabricIn .clsFabInQty').each(function(){
			
			var intRegex = /^\d+$/;
			var floatRegex = /^((\d+(\.\d *)?)|((\d*\.)?\d+))$/;
			
			var fabricInQty		= $(this).val();
			var combo			= $(this).parent().parent().find('.clsCombo').html();
			var partId			= $(this).parent().parent().find('.clsPart').attr('id');
			var sampleTypeId	= $(this).parent().parent().find('.clsSampleType').attr('id');
			var grade			= $(this).parent().parent().find('.clsGrade').html();
			var size			= $(this).parent().parent().find('.clsSize').html();
			var reqDate			= $(this).parent().parent().find('.clsRequireDate').html();
			
			if(fabricInQty!='')
			{
				chkStatus	= true;
				arrDetails += "{";
					arrDetails += '"fabricInQty":"'+ fabricInQty +'",' ;
					arrDetails += '"combo":'+ URLEncode_json(combo) +',' ;
					arrDetails += '"partId":"'+ partId +'",' ;
					arrDetails += '"sampleTypeId":"'+ sampleTypeId +'",' ;
					arrDetails += '"grade":"'+ grade +'",' ;
					if($.isNumeric(size)){
					arrDetails += '"size":'+ (size) +',' ;
					}
					else{
						arrDetails += '"size":'+ URLEncode_json(size) +',' ;
					}
					arrDetails += '"reqDate":'+ '"'+(reqDate) +'"' ;
				arrDetails += "},";
			}	
		});
		if(!chkStatus)
		{
			$(this).validationEngine('showPrompt','No Data to save.','fail');
			hideWaiting();	
			return;
		}
		
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		arrDetails		= '['+arrDetails+']';
		data+="&arrHeader="+arrHeader+"&arrDetails="+arrDetails;
		
		var url = basePath+"sampleFabricIn_db.php";
		//var url = "sampleFabricIn_db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmSampleFabricIn #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t = setTimeout("alertx()",3000);
						$('#txtSmpFabricInNo').val(json.fabricInNo);
						$('#txtSmpFabricInYear').val(json.fabricInYear);
						$('#frmSampleFabricIn #butConfirm').show();
						hideWaiting();
						return;
					}
					else
					{
						hideWaiting();
					}
				},
				error:function(xhr,status){
						
						$('#frmSampleFabricIn #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
				}		
		});
	}
	else
	{
		hideWaiting();
	}
}
function Confirm()
{
	var path = '?q='+reportId;
	var url  = path+"&fabricInNo="+$('#frmSampleFabricIn #txtSmpFabricInNo').val();
	    url += "&fabricInYear="+$('#frmSampleFabricIn #txtSmpFabricInYear').val();
	    url += "&mode=Confirm";
	window.open(url,'rpt_sampleFabricIn.php');
}
function Cancel()
{
	var path = '?q='+reportId;
	var url  = path+"&fabricInNo="+$('#frmSampleFabricIn #txtSmpFabricInNo').val();
	    url += "&fabricInYear="+$('#frmSampleFabricIn #txtSmpFabricInYear').val();
	    url += "&mode=Cancel";
	window.open(url,'rpt_sampleFabricIn.php');
}
function loadReport()
{
	var path = '?q='+reportId;
	if($('#frmSampleFabricIn #txtSmpFabricInNo').val()=='')
	{
		$('#frmSampleFabricIn #butReport').validationEngine('showPrompt','No Fabric In no to view Report','fail');
		return;	
	}
	var url  = path+"&fabricInNo="+$('#frmSampleFabricIn #txtSmpFabricInNo').val();
	    url += "&fabricInYear="+$('#frmSampleFabricIn #txtSmpFabricInYear').val();
	window.open(url,'rpt_sampleFabricIn.php');
}
function ConfirmRpt()
{
	var val = $.prompt('Are you sure you want to Approve this Sample Fabric In ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"sampleFabricIn_db.php"+window.location.search+'&requestType=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptFabricIn #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx1()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptFabricIn #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx1()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}
function RejectRpt()
{
	var val = $.prompt('Are you sure you want to Reject this Fabric In ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"sampleFabricIn_db.php"+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptFabricIn #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx2()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptFabricIn #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx2()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}
function CancelRpt()
{
	var val = $.prompt('Are you sure you want to Cancel this Fabric In ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"sampleFabricIn_db.php"+window.location.search+'&requestType=cancel';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptFabricIn #butRptCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx3()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptFabricIn #butRptCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx3()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}
function clearAll()
{
	window.location.href = "?q=824";
}
function alertx()
{
	$('#frmSampleFabricIn #butSave').validationEngine('hide')	;
}
function alertx1()
{
	$('#frmRptFabricIn #butRptConfirm').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmRptFabricIn #butRptReject').validationEngine('hide')	;
}
function alertx3()
{
	$('#frmRptFabricIn #butRptCancel').validationEngine('hide')	;
}
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "class/customerAndOperation/sample/sampleFabricIn/cls_sample_fabricin_get.php";
include_once "class/cls_commonErrorHandeling_get.php";

$obj_fabricin_get			= new Cls_sample_fabricin_get($db);
$obj_commonErrHandle		= new cls_commonErrorHandeling_get($db);

$locationId					= $_SESSION["CompanyID"];
$companyId					= $_SESSION["headCompanyId"];
$userId 					= $_SESSION["userId"];
$thisFilePath 				= $_SERVER['PHP_SELF'];
$programCode				= 'P0824';

$fabricInNo 				= $_REQUEST['fabricInNo'];
$fabricInYear 				= $_REQUEST['fabricInYear'];
$mode						= $_REQUEST["mode"];

$header_array				= $obj_fabricin_get->loadHeaderData($fabricInNo,$fabricInYear,'RunQuery');
$sampleOrder_arr			= $obj_fabricin_get->getSampleHeaderData($header_array['SAMPLE_ORDER_NO'],$header_array['SAMPLE_ORDER_YEAR']);
$detail_result				= $obj_fabricin_get->loadDetailData($fabricInNo,$fabricInYear,'RunQuery');

$intStatus					= $header_array['STATUS'];
$levels						= $header_array['APPROVE_LEVELS'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_cancel($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_cancel			= $permition_arr['permision'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_reject($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_reject			= $permition_arr['permision'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_confirm			= $permition_arr['permision'];

?>
<head>
<title>Sample Fabric In Report</title>
<script type="application/javascript" src="presentation/customerAndOperation/sample/sampleFabricIn/addNew/sampleFabricIn_js.js"></script>

<style>
#apDiv1 {
	position: absolute;
	left: 468px;
	top: 181px;
	width: 650px;
	height: 322px;
	z-index: 1;
	visibility: hidden;
}
.APPROVE {
	font-size: 16px;
	font-weight:bold;
	font-family: "Arial Black", Gadget, sans-serif;
	color: #36F;
}
</style>
</head>
<body>
<?php
if($intStatus==-2)
{
?>
	<div id="apDiv1"><img src="images/cancelled.png" style="opacity:0.3" /></div>
<?php
}
?>
<form id="frmRptFabricIn" name="frmRptFabricIn" method="post" autocomplete="off">
<table width="900" align="center">
	<tr>
    	<td colspan="3"><?php include 'reportHeader.php'?></td>
    </tr>
    <tr>
    	<td colspan="3" style="text-align:center">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="3" style="text-align:center"><strong>SAMPLE FABRIC IN REPORT</strong></td>
    </tr>
    <?php
		include "presentation/report_approve_status_and_buttons.php"
     ?>
    <tr>
    	<td colspan="3">
            <table width="100%" border="0">
            	<tr class="normalfnt">
                	<td width="16%">FabricIn No</td>
                    <td width="1%">:</td>
                    <td width="33%"><?php echo $fabricInNo.' / '.$fabricInYear?></td>
                    <td width="17%">In Date</td>
                    <td width="1%">:</td>
                    <td width="32%"><?php echo $header_array["INDATE"]?></td>
                </tr>
            	<tr class="normalfnt">
            	  <td>Sample Order No</td>
            	  <td>:</td>
            	  <td><?php echo $header_array["SAMPLE_ORDER_NO"].'/'.$header_array["SAMPLE_ORDER_YEAR"];?></td>
            	  <td>Customer</td>
            	  <td>:</td>
            	  <td><?php echo $sampleOrder_arr["customer"]?></td>
          	  </tr>
            	<tr class="normalfnt">
            	  <td>Requisition No</td>
            	  <td>:</td>
            	  <td><?php echo $sampleOrder_arr["conCatReqNo"]?></td>
            	  <td>Location</td>
            	  <td>:</td>
            	  <td><?php echo $sampleOrder_arr["customerLocation"]?></td>
          	  </tr>
            	<tr class="normalfnt">
            	  <td>Graphic No</td>
            	  <td>:</td>
            	  <td valign="top"><?php echo $sampleOrder_arr["strGraphicRefNo"]?></td>
            	  <td valign="top">Style</td>
            	  <td>:</td>
            	  <td valign="top"><?php echo $sampleOrder_arr["strStyleNo"]?></td>
          	  </tr>
            	<tr class="normalfnt">
            	  <td>Remarks</td>
            	  <td>:</td>
            	  <td valign="top"><?php echo $header_array["REMARKS"]?></td>
            	  <td valign="top">SampleOrder Remarks</td>
            	  <td>:</td>
            	  <td valign="top"><?php echo $sampleOrder_arr["sampleOrderRemarks"]?></td>
          	  </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td colspan="3">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="3">
        	<table border="0" class="rptBordered" id="tblSampOrders" >
            <tr>
                <th width="130">Combo</th>
                <th width="180" >Part</th>
                <th width="130" >Sample Type</th>
                <th width="72" >Grade</th>
                <th width="70">Size</th>
                <th width="72">Qty</th>
                <th width="83">InQty</th>
                <th width="127">Required Date</th>
                </tr>
            <?php 
          		
				while($row=mysqli_fetch_array($detail_result))
				{
			?>
					<tr class="normalfnt">
						<td class="clsCombo" style="text-align:left"><?php echo $row['COMBO']; ?></td>
						<td class="clsPart" id="<?php echo $row['PART']; ?>" style="text-align:left"><?php echo $row['partName']; ?></td>
						<td class="clsSampleType" id="<?php echo $row['SAMPLETYPE_ID']; ?>" style="text-align:left"><?php echo $row['sampleType']; ?></td>
						<td class="clsGrade" style="text-align:left"><?php echo $row['GRADE']; ?></td>
						<td class="clsSize" style="text-align:left"><?php echo $row['SIZE']; ?></td>
						<td class="clsOrderQty" style="text-align:right"><?php echo $row['intQty']; ?></td>
						<td style="text-align:right"><?php echo $row['INQTY']; ?></td>
						<td class="clsRequireDate" style="text-align:left"><?php echo $row['REQUIRED_DATE']; ?></td>
					</tr>
			<?php
				}
				
            ?>
            </table>
        </td>
    </tr>
    <tr>
    	<td colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">
			<?php
            $creator		= $header_array['CREATOR'];
            $createdDate	= $header_array['CREATED_DATE'];
            $resultA 		= $obj_fabricin_get->getRptApproveDetails($fabricInNo,$fabricInYear);
            include "presentation/report_approvedBy_details.php"
            ?>
        </td>
    </tr>
    <tr height="40">
      <td align="center" colspan="3" class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></td>
    </tr>
</table>
</form>
</body>
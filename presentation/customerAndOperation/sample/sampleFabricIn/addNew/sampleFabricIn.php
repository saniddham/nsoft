<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId				= $sessions->getLocationId();
$companyId				= $sessions->getCompanyId();
$userId					= $sessions->getUserId();

include_once "class/customerAndOperation/sample/sampleFabricIn/cls_sample_fabricin_get.php";
include_once "class/cls_commonFunctions_get.php";
include_once "class/cls_commonErrorHandeling_get.php";

$obj_fabricin_get		= new Cls_sample_fabricin_get($db);
$obj_commonErrHandle	= new cls_commonErrorHandeling_get($db);
$obj_common				= new cls_commonFunctions_get($db);

$programCode			= 'P0824';	
$fabricInNo 			= $_REQUEST['fabricInNo'];
$fabricInYear 			= $_REQUEST['fabricInYear'];

$header_arr				= $obj_fabricin_get->loadHeaderData($fabricInNo,$fabricInYear,'RunQuery');
$sampleOrder_arr		= $obj_fabricin_get->getSampleHeaderData($header_arr['SAMPLE_ORDER_NO'],$header_arr['SAMPLE_ORDER_YEAR']);
$detail_result			= $obj_fabricin_get->loadDetailData($fabricInNo,$fabricInYear,'RunQuery');

$intStatus				= $header_arr['STATUS'];
$levels					= $header_arr['APPROVE_LEVELS'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_save($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_save			= $permition_arr['permision'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_confirm		= $permition_arr['permision'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_cancel($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_cancel		= $permition_arr['permision'];
?>
<head>
<title>Sample Fabric In</title>
</head>
<body >
<form id="frmSampleFabricIn" name="frmSampleFabricIn" <?php /*?>autocomplete="off"<?php */?> method="post">
<div align="center">
	<div class="trans_layoutL">
	<div class="trans_text">SAMPLE FABRIC IN</div>
	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    	<tr>
        	<td>
                <table width="100%" border="0" class="">
                <tr>
                    <td width="15%" class="normalfnt">Sample  Fabric In No</td>
                    <td width="33%"><input name="txtSmpFabricInNo" type="text" disabled="disabled" id="txtSmpFabricInNo" style="width:88px" value="<?php echo $fabricInNo; ?>" />&nbsp;<input name="txtSmpFabricInYear" type="text" disabled="disabled" id="txtSmpFabricInYear" style="width:56px" value="<?php echo $fabricInYear; ?>" /></td>
                    <td width="1%">&nbsp;</td>
                    <td width="17%" class="normalfnt">Date</td>
                    <td width="34%"><input name="dtDate" type="text" value="<?php echo ($header_arr['INDATE']==''?date("Y-m-d"):$header_arr['INDATE']); ?>" class="txtbox" id="dtDate" style="width:100px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                </tr>
                <tr>
                  <td class="normalfnt">Graphic No</td>
                  <td class="normalfnt"><select name="cboGraphicNo" id="cboGraphicNo" class="chosen-select validate[required]" style="width:100%" >
                    <option value=""></option>
                    <?php
                    $result	= $obj_fabricin_get->getGraphicCombo();
                    while($row = mysqli_fetch_array($result))
                    {
                    if($header_arr['SAMPLE_ORDER_NO']==$row['intSampleOrderNo'] && $header_arr['SAMPLE_ORDER_YEAR']==$row['intSampleOrderYear'])
                    echo "<option selected=\"selected\" value=\"".$row['intSampleOrderNo'].'/'.$row['intSampleOrderYear']."\">".$row['conGraphicNo']."</option>"; 
                    else
                    echo "<option value=\"".$row['intSampleOrderNo'].'/'.$row['intSampleOrderYear']."\">".$row['conGraphicNo']."</option>"; 
                    }
                    ?>
                  </select></td>
                  <td>&nbsp;</td>
                  <td class="normalfnt">Customer</td>
                  <td class="normalfnt"><input id="txtCustomer" name="txtCustomer" value="<?php echo $sampleOrder_arr['customer']; ?>" style="width:100%" disabled="disabled"  /></td>
                </tr>
                
                
                <tr>
                    <td class="normalfnt">Requisition No</td>
                    <td class="normalfnt"><input id="txtRequisitionNo" name="txtRequisitionNo" value="<?php echo $sampleOrder_arr['conCatReqNo']; ?>" style="width:150px" disabled="disabled"  /></td>
                    <td>&nbsp;</td>
                    <td class="normalfnt">Location</td>
                    <td class="normalfnt"><input id="txtLocation" name="txtLocation" value="<?php echo $sampleOrder_arr['customerLocation']; ?>" style="width:100%" disabled="disabled"  /></td>
                </tr>
                <tr>
                    <td valign="top" class="normalfnt">Sample Order No</td>
                    <td valign="top" class="normalfnt"><input id="txtOrderNo" name="txtOrderNo" value="<?php echo $sampleOrder_arr['conSampleOrderNo']; ?>" style="width:150px" disabled="disabled"  /></td>
                    <td>&nbsp;</td>
                    <td class="normalfnt" valign="top">Style</td>
                    <td class="normalfnt" valign="top"><input id="txtStyle" name="txtStyle" value="<?php echo $sampleOrder_arr['strStyleNo'];  ?>" style="width:150px" disabled="disabled"  /></td>
                </tr>
                <tr>
                  <td valign="top" class="normalfnt">Remarks</td>
                  <td valign="top" class="normalfnt"><textarea name="txtRemarks" id="txtRemarks" rows="2" style="width:100%"><?php echo $header_arr['REMARKS']; ?></textarea></td>
                    <td>&nbsp;</td>
                    <td class="normalfnt" valign="top">Sample Order Remarks</td>
                    <td class="normalfnt" valign="top"><textarea name="txtSampleOrderRemarks" id="txtSampleOrderRemarks" rows="2" readonly style="width:100%"><?php echo $sampleOrder_arr['sampleOrderRemarks']; ?></textarea></td>
                </tr>
                </table>
            </td>
        </tr>
        
                
        
        <tr>
       	  <td>
            <div style="width:900px;height:250px;overflow:scroll" >
            <table class="bordered" id="tblSampOrders" >
            <tr>
                <th width="130" height="22" >Combo</th>
                <th width="180" >Part</th>
                <th width="130" >Sample Type</th>
                <th width="72" >Grade</th>
                <th width="70">Size</th>
                <th width="72">Order Qty</th>
                <th width="72">Approved In Qty</th>
                <th width="72">Pending In Qty</th>
                <th width="83"> Qty</th>
                <th width="127">Required Date</th>
                </tr>
            <?php 
          		if($fabricInNo!='' && $fabricInYear!='')
				{
					while($row=mysqli_fetch_array($detail_result))
					{
            ?>
                        <tr class="normalfnt">
                            <td class="clsCombo" style="text-align:left"><?php echo $row['COMBO']; ?></td>
                            <td class="clsPart" id="<?php echo $row['PART']; ?>" style="text-align:left"><?php echo $row['partName']; ?></td>
                            <td class="clsSampleType" id="<?php echo $row['SAMPLETYPE_ID']; ?>" style="text-align:left"><?php echo $row['sampleType']; ?></td>
                            <td class="clsGrade" style="text-align:left"><?php echo $row['GRADE']; ?></td>
                            <td class="clsSize" style="text-align:left"><?php echo $row['SIZE']; ?></td>
                            <td class="clsOrderQty" style="text-align:right"><?php echo $row['intQty']; ?></td>
                            <td class="clsOrderQty" style="text-align:right"><span class="clsTotInQty" style="text-align:right"><?php echo $row['totInQty_app']; ?></span></td>
                            <td class="clsOrderQty" style="text-align:right"><span class="clsTotInQty_pen" style="text-align:right"><?php echo $row['totInQty_pen']; ?></span></td>
                            <td style="text-align:center"><input name="txtFabricInQty" type="text"  class="validate[custom[number],max[<?php echo $row['intBalToFabricInQty']; ?>]] clsFabInQty" id="txtFabricInQty" style="width:100%;text-align:right" value="<?php echo $row['INQTY']; ?>" /></td>
                            <td class="clsRequireDate" style="text-align:left"><?php echo $row['REQUIRED_DATE']; ?></td>
                        </tr>
            <?php
					}
				}
            ?>
            </table>
            </div>
          </td>   
        </tr>
        
        
        <tr>
        	<td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                    <tr>
                        <td align="center"><a class="button white medium" id="butNew">New</a><a class="button white medium" id="butSave" <?php if($permision_save!=1){ ?>style="display:none"<?php } ?>>Save</a><a class="button white medium" id="butConfirm"  <?php if($permision_confirm!=1){ ?> style="display:none"<?php } ?>>Approve</a><a class="button white medium" id="butCancel" <?php if($permision_cancel!=1){ ?>  style="display:none"<?php } ?>>Cancel</a><a class="button white medium" id="butReport">Report</a><a href="main.php" class="button white medium" id="butSave">Close</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    
  	</div>
</div>
</form>
</body>
</html>
 
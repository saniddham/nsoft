<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$session_companyId 	= $sessions->getCompanyId();
$intUser  			= $sessions->getUserId();

$programCode		= 'P0824';

$reportMenuId		= '972';
$menuId				= '824';

include_once 		"libraries/jqgrid2/inc/jqgrid_dist.php";

$approveLevel 		= (int)getMaxApproveLevel();

################################################################
################## new part ####################################
################################################################
///// create where part ///////////////////
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'Status'=>'tb1.STATUS',
				'CONCAT_FABRICIN_NO'=>"CONCAT(tb1.SAMPLE_FABRICIN_NO,'/',tb1.SAMPLE_FABRICIN_YEAR)",
				'CONCAT_REQUISITION_NO'=>"CONCAT(SOH.intSampleReqNo,'/',SOH.intSampleReqYear)",
				'CONCAT_SMPORDER_NO'=>"CONCAT(tb1.SAMPLE_ORDER_NO,'/',tb1.SAMPLE_ORDER_YEAR)",
				'CUSTOMER_NAME'=>'MC.strName',
				'CUSTOMER_LOCATION'=>'CLH.strName',
				'GRAPHIC'=>'SI.strGraphicRefNo',
				'STYLE'=>'SI.strStyleNo',
				'SAMPLEORDER_REMARKS'=>'SOH.strRemarks',
				'FABRICIN_REMARKS'=>'tb1.REMARKS',
				'TOT_INQTY'=>'(SELECT SUM(INQTY) AS TOT_QTY
				FROM trn_sampleorder_fabricin_details SFID
				WHERE SFID.SAMPLE_FABRICIN_NO = tb1.SAMPLE_FABRICIN_NO AND
				SFID.SAMPLE_FABRICIN_YEAR = tb1.SAMPLE_FABRICIN_YEAR)'
				);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}

if(!count($arr)>0)					 
	$where_string .= "AND substr(tb1.CREATED_DATE,1,10) = '".date('Y-m-d')."'";
	
################## end code ####################################


$sql = "SELECT SUB_1.* FROM
			(SELECT
				CONCAT(tb1.SAMPLE_FABRICIN_NO,'/',tb1.SAMPLE_FABRICIN_YEAR)	AS CONCAT_FABRICIN_NO,
				tb1.SAMPLE_FABRICIN_NO,
				tb1.SAMPLE_FABRICIN_YEAR,
				CONCAT(tb1.SAMPLE_ORDER_NO,'/',tb1.SAMPLE_ORDER_YEAR) AS CONCAT_SMPORDER_NO,
				CONCAT(SOH.intSampleReqNo,'/',SOH.intSampleReqYear) AS CONCAT_REQUISITION_NO,
				SI.intCustomer,
				MC.strName AS CUSTOMER_NAME,
				SOH.intCustomerLocation,
				CLH.strName AS CUSTOMER_LOCATION,
				SI.strGraphicRefNo AS GRAPHIC,
				SI.strStyleNo AS STYLE,
				SOH.strRemarks AS SAMPLEORDER_REMARKS,
				tb1.REMARKS AS FABRICIN_REMARKS,
				tb1.APPROVE_LEVELS,
				tb1.COMPANY_ID,
				tb1.CREATED_BY,
				tb1.CREATED_DATE,
				if(tb1.STATUS=1,'Approved',if(tb1.STATUS=0,'Rejected',if(tb1.STATUS=-1,'Revised',if(tb1.STATUS=-2,'Cancelled','Pending')))) as Status,
				sys_users.strUserName AS CREATOR,
				(SELECT SUM(INQTY) AS TOT_QTY
				FROM trn_sampleorder_fabricin_details SFID
				WHERE SFID.SAMPLE_FABRICIN_NO = tb1.SAMPLE_FABRICIN_NO AND
				SFID.SAMPLE_FABRICIN_YEAR = tb1.SAMPLE_FABRICIN_YEAR) AS TOT_INQTY,
				
							IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(trn_sampleorder_fabricin_approveby.APPROVED_DATE),')' )
								FROM
								trn_sampleorder_fabricin_approveby
								Inner Join sys_users ON trn_sampleorder_fabricin_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								trn_sampleorder_fabricin_approveby.SAMPLE_FABRICIN_NO  = tb1.SAMPLE_FABRICIN_NO AND
								trn_sampleorder_fabricin_approveby.SAMPLE_FABRICIN_YEAR =  tb1.SAMPLE_FABRICIN_YEAR AND
								trn_sampleorder_fabricin_approveby.APPROVE_LEVEL_NO = '1' AND
								trn_sampleorder_fabricin_approveby.STATUS =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
								$approval	= "2nd_Approval";
							}
							else if($i==3){
								$approval	= "3rd_Approval";
							}
							else {
								$approval	= $i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(trn_sampleorder_fabricin_approveby.APPROVED_DATE),')' )
								FROM
								trn_sampleorder_fabricin_approveby
								Inner Join sys_users ON trn_sampleorder_fabricin_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								trn_sampleorder_fabricin_approveby.SAMPLE_FABRICIN_NO  = tb1.SAMPLE_FABRICIN_NO AND
								trn_sampleorder_fabricin_approveby.SAMPLE_FABRICIN_YEAR =  tb1.SAMPLE_FABRICIN_YEAR AND
								trn_sampleorder_fabricin_approveby.APPROVE_LEVEL_NO =  '$i' AND
								trn_sampleorder_fabricin_approveby.STATUS =  '0' 
							),
							IF(
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.STATUS>1) AND (tb1.STATUS<=tb1.APPROVE_LEVELS) AND ((SELECT
								concat(sys_users.strUserName )
								FROM
								trn_sampleorder_fabricin_approveby
								Inner Join sys_users ON trn_sampleorder_fabricin_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								trn_sampleorder_fabricin_approveby.SAMPLE_FABRICIN_NO  = tb1.SAMPLE_FABRICIN_NO AND
								trn_sampleorder_fabricin_approveby.SAMPLE_FABRICIN_YEAR =  tb1.SAMPLE_FABRICIN_YEAR AND
								trn_sampleorder_fabricin_approveby.APPROVE_LEVEL_NO =  ($i-1) AND 
								trn_sampleorder_fabricin_approveby.STATUS='0' )<>'')),
								
								'Approve',
								 if($i>tb1.APPROVE_LEVELS,'-----',''))
								
								) as `".$approval."`, "; 
									
								}
								
							$sql .= "IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(trn_sampleorder_fabricin_approveby.APPROVED_DATE),')' )
								FROM
								trn_sampleorder_fabricin_approveby
								Inner Join sys_users ON trn_sampleorder_fabricin_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								trn_sampleorder_fabricin_approveby.SAMPLE_FABRICIN_NO  = tb1.SAMPLE_FABRICIN_NO AND
								trn_sampleorder_fabricin_approveby.SAMPLE_FABRICIN_YEAR =  tb1.SAMPLE_FABRICIN_YEAR AND
								trn_sampleorder_fabricin_approveby.APPROVE_LEVEL_NO =  '0' AND
								trn_sampleorder_fabricin_approveby.STATUS =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS<=tb1.APPROVE_LEVELS AND 
								tb1.STATUS>1),'Reject', '')) as `Reject`,";
								
							$sql .= "IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(trn_sampleorder_fabricin_approveby.APPROVED_DATE),')' )
								FROM
								trn_sampleorder_fabricin_approveby
								Inner Join sys_users ON trn_sampleorder_fabricin_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								trn_sampleorder_fabricin_approveby.SAMPLE_FABRICIN_NO  = tb1.SAMPLE_FABRICIN_NO AND
								trn_sampleorder_fabricin_approveby.SAMPLE_FABRICIN_YEAR =  tb1.SAMPLE_FABRICIN_YEAR AND
								trn_sampleorder_fabricin_approveby.APPROVE_LEVEL_NO =  '-2' AND
								trn_sampleorder_fabricin_approveby.STATUS =  '0'
							),IF(((SELECT
								menupermision.intCancel 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND 
								tb1.STATUS=1),'Cancel', '')) as `Cancel`,";
							
							$sql .= "IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(trn_sampleorder_fabricin_approveby.APPROVED_DATE),')' )
								FROM
								trn_sampleorder_fabricin_approveby
								Inner Join sys_users ON trn_sampleorder_fabricin_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								trn_sampleorder_fabricin_approveby.SAMPLE_FABRICIN_NO  = tb1.SAMPLE_FABRICIN_NO AND
								trn_sampleorder_fabricin_approveby.SAMPLE_FABRICIN_YEAR =  tb1.SAMPLE_FABRICIN_YEAR AND
								trn_sampleorder_fabricin_approveby.APPROVE_LEVEL_NO =  '-1' AND
								trn_sampleorder_fabricin_approveby.STATUS =  '0'
							),IF(((SELECT
								menupermision.intRevise 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND 
								tb1.STATUS=1),'Revise', '')) as `Revise`,";
								
							$sql .= "'View' as `View`   
				
				FROM trn_sampleorder_fabricin_header as tb1 
				INNER JOIN trn_sampleorderheader SOH ON SOH.intSampleOrderNo=tb1.SAMPLE_ORDER_NO AND
				SOH.intSampleOrderYear=tb1.SAMPLE_ORDER_YEAR
				INNER JOIN trn_sampleinfomations SI ON SI.intSampleNo=SOH.intSampleNo AND
				SI.intSampleYear=SOH.intSampleYear AND
				SI.intRevisionNo=SOH.intRevNo 
				INNER JOIN mst_customer MC ON MC.intId=SI.intCustomer
				INNER JOIN mst_customer_locations_header CLH ON CLH.intId=SOH.intCustomerLocation
				INNER JOIN sys_users ON tb1.CREATED_BY = sys_users.intUserId
				WHERE tb1.COMPANY_ID = '$session_companyId' 
				$where_string
 
		)  
		AS SUB_1 WHERE 1=1";

		
$formLink			= "?q=$menuId&fabricInNo={SAMPLE_FABRICIN_NO}&fabricInYear={SAMPLE_FABRICIN_YEAR}";	 
$reportLink  		= "?q=$reportMenuId&fabricInNo={SAMPLE_FABRICIN_NO}&fabricInYear={SAMPLE_FABRICIN_YEAR}";
$reportLinkApprove  = "?q=$reportMenuId&fabricInNo={SAMPLE_FABRICIN_NO}&fabricInYear={SAMPLE_FABRICIN_YEAR}&mode=Confirm";
$reportLinkCancel  = "?q=$reportMenuId&fabricInNo={SAMPLE_FABRICIN_NO}&fabricInYear={SAMPLE_FABRICIN_YEAR}&mode=Cancel";
$reportLinkPrint  	= "?q=$reportMenuId&fabricInNo={SAMPLE_FABRICIN_NO}&fabricInYear={SAMPLE_FABRICIN_YEAR}&mode=print";
		
				
$jq = new jqgrid('',$db);	
$g 	= new jqgrid();

$col["title"] 				= "Status";
$col["name"] 				= "Status";
$col["width"] 				= "3"; 						
$col["align"] 				= "center";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["stype"] 				= "select";
$str 						= ":All;Pending:Pending;Approved:Approved;Rejected:Rejected;Cancelled:Cancelled" ;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;	
$col						= NULL;



$col["title"] 				= "Date";
$col["name"] 				= "CREATED_DATE";
$col["width"] 				= "3";
$col["align"] 				= "left";
$col["sortable"] 			= true; 					
$col["search"] 				= true; 					
$col["editable"] 			= false;
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "fabricInNo";
$col["name"] 				= "SAMPLE_FABRICIN_NO";
$col["sortable"] 			= true; 					
$col["search"] 				= true;
$col["hidden"]  			= true;				
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "fabricInYear";
$col["name"] 				= "SAMPLE_FABRICIN_YEAR";
$col["sortable"] 			= true; 					
$col["search"] 				= true;
$col["hidden"]  			= true;					
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Serial No";
$col["name"] 				= "CONCAT_FABRICIN_NO";
$col["width"] 				= "3";
$col["align"] 				= "center";
$col["sortable"] 			= true; 					
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col['link']				= $formLink;
$col["linkoptions"] 		= "target='sampleFabricIn.php'";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Requisition No";
$col["name"] 				= "CONCAT_REQUISITION_NO";
$col["width"] 				= "3";
$col["align"] 				= "left";
$col["sortable"] 			= true; 					
$col["search"] 				= true; 					
$col["editable"] 			= false;
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "SampleOrder No";
$col["name"] 				= "CONCAT_SMPORDER_NO";
$col["width"] 				= "3";
$col["align"] 				= "left";
$col["sortable"] 			= true; 					
$col["search"] 				= true; 					
$col["editable"] 			= false;
$cols[] 					= $col;	
$col						= NULL;


$col["title"] 				= "Customer";
$col["name"] 				= "CUSTOMER_NAME";
$col["width"] 				= "6"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["dbname"] 				= "intCustomer";
$client_lookup 				= $jq->get_dropdown_values("select intId AS k,strName AS v
														from mst_customer 
														where intStatus = 1 
														order by strName");
$col["stype"] 				= "select";
$str 						= ":;".$client_lookup;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;
$col						= NULL;

$col["title"] 				= "CU. Location";
$col["name"] 				= "CUSTOMER_LOCATION";
$col["width"] 				= "4"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["dbname"] 				= "intCustomerLocation";
$client_lookup 				= $jq->get_dropdown_values("SELECT DISTINCT intId AS k,strName AS v
														FROM mst_customer_locations_header
														WHERE intStatus = 1
														ORDER BY strName");
$col["stype"] 				= "select";
$str 						= ":;".$client_lookup;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;
$col						= NULL;

$col["title"] 				= "Graphic No";
$col["name"] 				= "GRAPHIC";
$col["width"] 				= "3";
$col["align"] 				= "left";
$col["sortable"] 			= true; 					
$col["search"] 				= true; 					
$col["editable"] 			= false;
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Style No";
$col["name"] 				= "STYLE";
$col["width"] 				= "3";
$col["align"] 				= "left";
$col["sortable"] 			= true; 					
$col["search"] 				= true; 					
$col["editable"] 			= false;
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Sample Order Remarks";
$col["name"] 				= "SAMPLEORDER_REMARKS";
$col["width"] 				= "4"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 											
$col["editable"] 			= false;
$cols[] 					= $col;
$col						= NULL;

$col["title"] 				= "Fabric In Remarks";
$col["name"] 				= "FABRICIN_REMARKS";
$col["width"] 				= "4"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 											
$col["editable"] 			= false;
$cols[] 					= $col;
$col						= NULL;

$col["title"] 				= "Qty";
$col["name"] 				= "TOT_INQTY";
$col["width"] 				= "2";
$col["align"] 				= "right";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "1st Approval";
$col["name"] 				= "1st_Approval";
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= $reportLinkApprove;
$col["linkoptions"] 		= "target='rpt_sampleFabricIn.php'";
$col['linkName']			= 'Approve';
$cols[] 					= $col;	
$col						= NULL;

for($i=2; $i<=$approveLevel; $i++)
{
	if($i==2){
		$ap		= "2nd Approval";
		$ap1	= "2nd_Approval";
	}
	else if($i==3){
		$ap		= "3rd Approval";
		$ap1	= "3rd_Approval";
	}
	else {
		$ap		= $i."th Approval";
		$ap1	= $i."th_Approval";
	}

$col["title"] 				= $ap; 
$col["name"] 				= $ap1; 
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= $reportLinkApprove;
$col["linkoptions"] 		= "target='rpt_sampleRequest.php'";
$col['linkName']			= 'Approve';
$cols[] 					= $col;	
$col						= NULL;
}

$col["title"] 				= 'Cancel'; 
$col["name"] 				= 'Cancel'; 
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= $reportLinkCancel;
$col["linkoptions"] 		= "target='rpt_sampleFabricIn.php'";
$col['linkName']			= 'Cancel';
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Report";
$col["name"] 				= "View";
$col["width"] 				= "2";
$col["align"] 				= "center"; 
$col["sortable"]			= false;
$col["editable"] 			= false; 	
$col["search"] 				= false; 
$col['link']				= $reportLink;
$col["linkoptions"] 		= "target='rpt_sampleFabricIn.php'";
$col['linkName']			= 'View';
$cols[] 					= $col;	
$col						= NULL;

$grid["caption"] 			= "Sample Fabric In Listing";
$grid["multiselect"] 		= false;
$grid["rowNum"] 			= 20; 
$grid["sortname"] 			= 'SAMPLE_FABRICIN_YEAR,SAMPLE_FABRICIN_NO'; 
$grid["sortorder"] 			= "DESC"; 
$grid["autowidth"] 			= true; 
$grid["multiselect"] 		= false; 
$grid["search"] 			= true; 
$grid["postData"] 			= array("filters" => $sarr ); 
$grid["export"] 			= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

/*$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"}
     ]
}
SEARCH_JSON;

$grid["postData"] = array("filters" => $sarr ); */

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Sample Fabric In Listing</title>
<?php echo $out?>
<?php
function getMaxApproveLevel()
{
	global $db;
	$appLevel = 0;
	
	$sqlp = "SELECT
			Max(trn_sampleorder_fabricin_approveby.APPROVE_LEVEL_NO) AS appLevel
			FROM trn_sampleorder_fabricin_approveby";					
	$resultp 	= $db->RunQuery($sqlp);
	$rowp		= mysqli_fetch_array($resultp);
	return $rowp['appLevel'];
}
?>

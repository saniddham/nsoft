<?php
session_start();
$backwardseperator 		= "../../../../../";
$userId 				= $_SESSION['userId'];
$locationId	  			= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];
$SRdb					= $_SESSION["SRDatabese"];
$Maindb					= $_SESSION["Database"];

$requestType 			= $_REQUEST['requestType'];
$programName			= 'Sample Fabric In';
$programCode			= 'P0824';

include_once "../../../../../dataAccess/Connector.php";
include_once "../../../../../class/customerAndOperation/sample/sampleFabricIn/cls_sample_fabricin_get.php";
include_once "../../../../../class/customerAndOperation/sample/sampleFabricIn/cls_sample_fabricin_set.php";
include_once "../../../../../class/cls_commonFunctions_get.php";
include_once "../../../../../class/cls_commonErrorHandeling_get.php";

$obj_faabricin_get		= new Cls_sample_fabricin_get($db);
$obj_faabricin_set		= new Cls_sample_fabricin_set($db);
$obj_common				= new cls_commonFunctions_get($db);
$obj_errorHandeling 	= new cls_commonErrorHandeling_get($db);

$savedStatus			= true;
$finalApprove			= false;
$savedMasseged 			= '';
$error_sql				= '';

if($requestType=='loadSampleOrderDetails')
{
	$sampleOrderNoArr	= explode('/',$_REQUEST['sampleOrderNoArr']);
	$sampleOrderNo		= $sampleOrderNoArr[0];
	$sampleOrderYear	= $sampleOrderNoArr[1];
	
	$headerDataArr		= $obj_faabricin_get->getSampleHeaderData($sampleOrderNo,$sampleOrderYear);
	
	$response['SampleReqNo'] 		= $headerDataArr['intSampleReqNo'];
	$response['SampleReqYear'] 		= $headerDataArr['intSampleReqYear'];
	$response['conReqNo'] 			= $headerDataArr['conCatReqNo'];
	$response['conOrderNo'] 		= $headerDataArr['conSampleOrderNo'];
	$response['customer'] 			= $headerDataArr['customer'];
	$response['customerLocation'] 	= $headerDataArr['customerLocation'];
	$response['GraphicRefNo'] 		= $headerDataArr['strGraphicRefNo'];
	$response['StyleNo'] 			= $headerDataArr['strStyleNo'];
	$response['sampleOrderRemarks'] = $headerDataArr['sampleOrderRemarks'];
	$response['arrDetailData']		= getSampleOrderDetailData($sampleOrderNo,$sampleOrderYear);
	
	echo json_encode($response);
}
else if($requestType=='saveData')
{
	//echo $_REQUEST['arrDetails'];
	
	$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
	$arrDetails 		= json_decode($_REQUEST['arrDetails'],true);
	//print_r($arrDetails);
	
	$arrHeader			= $arrHeader[0];
	$db->begin();
	
	$fabricInNo			= $arrHeader['fabricInNo'];
	$fabricInYear		= $arrHeader['fabricInYear'];
	$fabricInDate		= $arrHeader['fabricInDate'];
	//$style				= $obj_common->replace($arrHeader["style"]);
	$sampleOrderArr		= explode('/',$arrHeader['sampleOrderArr']);
	$sampleOrderNo		= $sampleOrderArr[0];
	$sampleOrderYear	= $sampleOrderArr[1];
	$remarks			= $obj_common->replace($arrHeader["remarks"]);
	$editMode			= false;
	
	$validateArr		= validateBeforeSave($fabricInNo,$fabricInYear,$programCode,$userId);
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateArr["msg"];
	}
	if($fabricInNo=='' && $fabricInYear=='')
	{
		$sysNo_arry 	= $obj_common->GetSystemMaxNo('SAMPLE_FABRICIN_NO',$locationId);
		if($sysNo_arry["rollBackFlag"]==1 && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $sysNo_arry["msg"];
			$error_sql		= $sysNo_arry["q"];	
		}
		$fabricInNo			= $sysNo_arry["max_no"];
		$fabricInYear		= date('Y');
		$approveLevels 		= $obj_common->getApproveLevels($programName);
		$status				= $approveLevels+1;
		
		$resultHArr			= $obj_faabricin_set->saveHeader($fabricInNo,$fabricInYear,$fabricInDate,$sampleOrderNo,$sampleOrderYear,$remarks,$locationId,$companyId,$userId,$status,$approveLevels);
		if(isset($resultHArr['savedStatus']) && $resultHArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultHArr['savedMassege'];
			$error_sql		= $resultHArr['error_sql'];	
		}
	}
	else
	{
		$header_array 		= $obj_faabricin_get->loadHeaderData($fabricInNo,$fabricInYear,'RunQuery2');
		$status				= $header_array['STATUS'];
		$approveLevels		= $header_array['APPROVE_LEVELS'];
		$approveLevels 		= $obj_common->getApproveLevels($programName);
		$status				= $approveLevels+1;
		
		$resultUHArr		= $obj_faabricin_set->updateHeader($fabricInNo,$fabricInYear,$fabricInDate,$sampleOrderNo,$sampleOrderYear,$remarks,$locationId,$companyId,$userId,$status,$approveLevels);
		if($resultUHArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultUHArr['savedMassege'];
			$error_sql		= $resultUHArr['error_sql'];	
		}
		$resultUHArr		= updateMaxStatus($fabricInNo,$fabricInYear);
		if($resultUHArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultUHArr['savedMassege'];
			$error_sql		= $resultUHArr['error_sql'];	
		}
		$resultDDArr		= $obj_faabricin_set->deleteDetails($fabricInNo,$fabricInYear);
		if($resultDDArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultDDArr['savedMassege'];
			$error_sql		= $resultDDArr['error_sql'];	
		}
		$editMode			= true;
	}
	foreach($arrDetails as $array_loop)
	{
		$fabricInQty		= $array_loop['fabricInQty'];
		$combo				= $array_loop['combo'];
		$partId				= $array_loop['partId'];
		$sampleTypeId		= $array_loop['sampleTypeId'];
		$grade				= $array_loop['grade'];
		$size				= $array_loop['size'];
		$reqDate			= $array_loop['reqDate'];
		
		$chekQtyArr			= checkQtyBeforeSave($sampleOrderNo,$sampleOrderYear,$combo,$partId,$sampleTypeId,$size,$grade,$reqDate,$fabricInQty);
		if($chekQtyArr['type']=='fail' && $savedStatus)
		{
			$savedStatus	= false; //temporarily commented on 2016-10-26 sice they need unlimited fabric in facility.//activated on 2017-10-25
			$savedMasseged 	= $chekQtyArr["msg"];
		}
		$resultDArr			= $obj_faabricin_set->saveDetails($fabricInNo,$fabricInYear,$fabricInQty,$combo,$partId,$sampleTypeId,$grade,$size,$reqDate);
		if($resultDArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultDArr['savedMassege'];
			$error_sql		= $resultDArr['error_sql'];	
		}
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 			= "pass";
		if($editMode)
			$response['msg'] 		= "Updated Successfully.";
		else
			$response['msg'] 		= "Saved Successfully.";
		
		$response['fabricInNo']		= $fabricInNo;
		$response['fabricInYear']	= $fabricInYear;
	}
	else
	{
		$db->rollback();
		$response['type'] 			= "fail";
		$response['msg'] 			= $savedMasseged;
		$response['sql']			= $error_sql;
	}
	echo json_encode($response);	
}
else if($requestType=='approve')
{
	$fabricInNo		= $_REQUEST["fabricInNo"];
	$fabricInYear	= $_REQUEST["fabricInYear"];
	
	$db->begin();
	
	$validateArr		= validateBeforeApprove($fabricInNo,$fabricInYear,$programCode,$userId);
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateArr["msg"];
	}
	$resultUHSArr		= updateHeaderStatus($fabricInNo,$fabricInYear,'');
	if(isset($resultUHSArr['savedStatus']) && $resultUHSArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultUHSArr["savedMassege"];
		$error_sql		= $resultUHSArr['error_sql'];	
	}
	$chekQtyArr			= checkQtyBeforeFinalApproval($fabricInNo,$fabricInYear);
	if(isset($chekQtyArr['type']) && $chekQtyArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $chekQtyArr["msg"];
	}
	$qtyUpdArr			= sampleFabricInBalQtyUpdate($fabricInNo,$fabricInYear,'approve');
	if(isset($qtyUpdArr['savedStatus']) && $qtyUpdArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $qtyUpdArr["savedMassege"];
		$error_sql		= $qtyUpdArr['error_sql'];
	}
	$resultAPArr		= approvedData($fabricInNo,$fabricInYear,$userId);
	if(isset($resultAPArr['savedStatus']) && $resultAPArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultAPArr["savedMassege"];
		$error_sql		= $resultAPArr['error_sql'];	
	}
	
	//-------save in sample_log_table
	$header_arr		= $obj_faabricin_get->loadHeaderData($fabricInNo,$fabricInYear,'RunQuery2');
	if($header_arr['STATUS'] ==1 ){
		$requsitionNoArr 	= $obj_faabricin_get->getRequsitionNo($header_arr['SAMPLE_ORDER_NO'],$header_arr['SAMPLE_ORDER_YEAR']);
		if($requsitionNoArr['intSampleReqNo']!=''){
		//-----
 			$saved			= true;
			$result = $db->RunQuery2($sql);
			$result_types	= $obj_faabricin_get->load_fabricIn_details_for_requesition($fabricInNo,$fabricInYear,$SRdb,$Maindb,'RunQuery2');
			while($row_types = mysqli_fetch_array($result_types)){
			$graphc			= $row_types['GRAPHIC'];
			$samp_type		= $row_types['SAMPLE_TYPE'];
			$trns_type		= $row_types['transacType'];
			$created_by	 	= $row_types['createdBy'];
			$modified_by 	= $row_types['modifyBy'];
			$log_date	 	= $row_types['transacDate'];
			
			$resultLogArr	= $obj_faabricin_set->saveLogQueries($SRdb,$graphc,$samp_type,$trns_type,$created_by,$modified_by,$log_date,'RunQuery_S');
			if($resultLogArr['savedStatus']=='fail' && $savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged 	= $resultLogArr["savedMassege"];
				$error_sql		= $resultLogArr['error_sql'];
			}
		}
 		//-----
		}
	}
	
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		if(isset($finalApproval))
			$response['msg'] 	= "Final Approval Raised Successfully.";
		else
			$response['msg'] 	= "Approved Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql']		= $error_sql;
	}
	echo json_encode($response);
}
else if($requestType=='reject')
{
	$fabricInNo			= $_REQUEST["fabricInNo"];
	$fabricInYear		= $_REQUEST["fabricInYear"];
	
	$db->begin();
	
	$validateArr		= validateBeforeReject($fabricInNo,$fabricInYear,$programCode,$userId);
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateArr["msg"];
	}
	$resultUHSArr		= updateHeaderStatus($fabricInNo,$fabricInYear,'0');
	if(isset($resultUHSArr['savedStatus']) && $resultUHSArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultUHSArr["savedMassege"];
		$error_sql		= $resultUHSArr['error_sql'];	
	}
	$resultAPArr		= $obj_faabricin_set->approved_by_insert($fabricInNo,$fabricInYear,$userId,0);
	if(isset($resultAPArr['savedStatus']) && $resultAPArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultAPArr["savedMassege"];
		$error_sql		= $resultAPArr['error_sql'];	
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Rejected Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql']		= $error_sql;
	}
	echo json_encode($response);
}
else if($requestType=='cancel')
{
	$fabricInNo			= $_REQUEST["fabricInNo"];
	$fabricInYear		= $_REQUEST["fabricInYear"];
	
	$db->begin();
	
	$validateArr		= validateBeforeCancel($fabricInNo,$fabricInYear,$programCode,$userId);
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateArr["msg"];
	}
	$qtyUpdArr			= sampleFabricInBalQtyUpdate($fabricInNo,$fabricInYear,'cancel');
	if($qtyUpdArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $qtyUpdArr["savedMassege"];
		$error_sql		= $qtyUpdArr['error_sql'];
	}
	$resultUHSArr		= updateHeaderStatus($fabricInNo,$fabricInYear,'-2');
	if($resultUHSArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultUHSArr["savedMassege"];
		$error_sql		= $resultUHSArr['error_sql'];	
	}
	$resultAPArr		= $obj_faabricin_set->approved_by_insert($fabricInNo,$fabricInYear,$userId,'-2');
	if($resultAPArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultAPArr["savedMassege"];
		$error_sql		= $resultAPArr['error_sql'];	
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Cancelled Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql']		= $error_sql;
	}
	echo json_encode($response);
	
}
function getSampleOrderDetailData($sampleOrderNo,$sampleOrderYear)
{
	global $obj_faabricin_get;
    $arrDetailData = array();
	
	$result	= $obj_faabricin_get->getSampleOrderDetailData($sampleOrderNo,$sampleOrderYear);
	while($row = mysqli_fetch_array($result))
	{
		$data['Combo']			= $row['strCombo'];
		$data['PartId']			= $row['intPart'];
		$data['partName']		= $row['partName'];
		$data['sampleTypeName']	= $row['sampleType'];
		$data['SampleTypeId']	= $row['intSampleTypeId'];
		$data['Grade']			= $row['intGrade'];
		$data['orderQty']		= $row['intQty'];
		$data['Size']			= $row['strSize'];
		$data['RequiredDate']	= $row['dtRequiredDate'];
		$data['fabInBalQty']	= $row['intBalToFabricInQty'];
		$data['totInQty_app']	= $row['totInQty_app'];
		$data['totInQty_pen']	= $row['totInQty_pen'];
	
		array_push($arrDetailData, $data);
	}
	return $arrDetailData;
}
function validateBeforeSave($fabricInNo,$fabricInYear,$programCode,$userId)
{
	global $obj_errorHandeling;
	global $obj_faabricin_get;

	$header_arr		= $obj_faabricin_get->loadHeaderData($fabricInNo,$fabricInYear,'RunQuery2');
	$validateArr 	= $obj_errorHandeling->get_permision_withApproval_save($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$userId,$programCode,'RunQuery2');
	
	return  $validateArr;
}
function updateMaxStatus($fabricInNo,$fabricInYear)
{
	global $obj_faabricin_get;
	global $obj_faabricin_set;
	
	$maxStatus	= $obj_faabricin_get->getMaxStatus($fabricInNo,$fabricInYear);
	$resultArr	= $obj_faabricin_set->updateApproveByStatus($fabricInNo,$fabricInYear,$maxStatus);
	
	return $resultArr;
}
function validateBeforeApprove($fabricInNo,$fabricInYear,$programCode,$userId)
{
	global $obj_errorHandeling;
	global $obj_faabricin_get;
	
	$header_arr		= $obj_faabricin_get->loadHeaderData($fabricInNo,$fabricInYear,'RunQuery2');
	$validateArr 	= $obj_errorHandeling->get_permision_withApproval_confirm($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$userId,$programCode,'RunQuery2');
	
	return  $validateArr;
}
function updateHeaderStatus($fabricInNo,$fabricInYear,$status)
{
	global $obj_faabricin_set;
	
	if($status=='') 
		$para 	= 'STATUS-1';
	else
		$para 	= $status;
		
	$resultArr	= $obj_faabricin_set->updateHeaderStatus($fabricInNo,$fabricInYear,$para);
	
	return  $resultArr;
}
function approvedData($fabricInNo, $fabricInYear, $userId)
{
	global $obj_faabricin_get;
	global $obj_faabricin_set;
	global $finalApprove;
	
	$header_arr 	= $obj_faabricin_get->loadHeaderData($fabricInNo,$fabricInYear,'RunQuery2');
	
	if($header_arr['STATUS']==1)
		$finalApprove = true;
	
	$approval		= $header_arr['APPROVE_LEVELS']+1-$header_arr['STATUS'];
	$resultArr		= $obj_faabricin_set->approved_by_insert($fabricInNo,$fabricInYear,$userId,$approval);
	
	return  $resultArr;
}
function checkQtyBeforeFinalApproval($fabricInNo,$fabricInYear)
{
	global $obj_faabricin_get;
	$checkQty		= false;
	$data = array();
	
	$header_arr		= $obj_faabricin_get->loadHeaderData($fabricInNo,$fabricInYear,'RunQuery2');
	if($header_arr['STATUS']==1)
	{
		$smpHeaderStatus = $obj_faabricin_get->getSampleOrderStatus($header_arr['SAMPLE_ORDER_NO'],$header_arr['SAMPLE_ORDER_YEAR']);
		if($smpHeaderStatus!=1)
		{
			$data['type'] 	= 'fail';
			$data['msg'] 	= 'Can not Approved. Sample Order cancelled ';
		}
		else
		{
			$fabIn_result		= $obj_faabricin_get->getSampleFabricInQty($fabricInNo,$fabricInYear);
			while($row = mysqli_fetch_array($fabIn_result))
			{
				$fabInBalQtyArr = $obj_faabricin_get->getSampleOrderFabInBalQty($header_arr['SAMPLE_ORDER_NO'],$header_arr['SAMPLE_ORDER_YEAR'],$row['COMBO'],$row['PART'],$row['GRADE'],$row['SIZE'],$row['SAMPLETYPE_ID'],$row['REQUIRED_DATE']);
				
				if($row['INQTY']>$fabInBalQtyArr['intBalToFabricInQty'])
				{
					$checkQty	= true; //temporarily commented on 2016-10-26 sice they need unlimited fabric in facility.//activated again on 2017-10-25
				}
			}
			if($checkQty)
			{
				$data['type'] 	= 'fail';
				$data['msg'] 	= 'Some Qty exceed Sample Fabric In bal qty. '.$checkQty;
			}
		}
	}
	return $data;	
}
function sampleFabricInBalQtyUpdate($fabricInNo,$fabricInYear,$mode)
{
	global $obj_faabricin_get;
	global $obj_faabricin_set;
	global $SRdb;
	$status	= true;
	$data = array();
	
	$header_arr 	= $obj_faabricin_get->loadHeaderData($fabricInNo,$fabricInYear,'RunQuery2');
	if($header_arr['STATUS']==1)
	{
		$fabIn_result		= $obj_faabricin_get->getSampleFabricInQty($fabricInNo,$fabricInYear);
		while($row = mysqli_fetch_array($fabIn_result))
		{
			$updateBal_result	= $obj_faabricin_set->updateFafbricInBalQty($header_arr['SAMPLE_ORDER_NO'],$header_arr['SAMPLE_ORDER_YEAR'],$row['COMBO'],$row['PART'],$row['GRADE'],$row['SIZE'],$row['SAMPLETYPE_ID'],$row['REQUIRED_DATE'],$row['INQTY'],$mode);
			if(isset($updateBal_result['savedStatus']) && $updateBal_result['savedStatus']=='fail' && $status)
			{
				$status					= false;
				$data['savedStatus']	= 'fail';
				$data['savedMassege']	= $updateBal_result['savedMassege'];
				$data['error_sql']		= $updateBal_result['error_sql'];
			}

			$requsitionNoArr 			= $obj_faabricin_get->getRequsitionNo($header_arr['SAMPLE_ORDER_NO'],$header_arr['SAMPLE_ORDER_YEAR']);
			$updateFabInDate_result		= $obj_faabricin_set->updateFabInDateinRequsition($requsitionNoArr['intSampleReqNo'],$requsitionNoArr['intSampleReqYear'],$row['SAMPLETYPE_ID'],$header_arr['INDATE'],$row['INQTY'],$mode,$SRdb);
			if($updateFabInDate_result['savedStatus']=='fail' && $status)
			{
				$status					= false;
				$data['savedStatus']	= 'fail';
				$data['savedMassege']	= $updateFabInDate_result['savedMassege'];
				$data['error_sql']		= $updateFabInDate_result['error_sql'];
			}
		}
	}
	
	return $data;
}
function validateBeforeReject($fabricInNo,$fabricInYear,$programCode,$userId)
{
	global $obj_errorHandeling;
	global $obj_faabricin_get;
	
	$header_arr		= $obj_faabricin_get->loadHeaderData($fabricInNo,$fabricInYear,'RunQuery2');
	$validateArr 	= $obj_errorHandeling->get_permision_withApproval_reject($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$userId,$programCode,'RunQuery2');
	
	return  $validateArr;
}
function validateBeforeCancel($fabricInNo,$fabricInYear,$programCode,$userId)
{
	global $obj_errorHandeling;
	global $obj_faabricin_get;
	
	$checkStatus	= false;
	
	$header_arr		= $obj_faabricin_get->loadHeaderData($fabricInNo,$fabricInYear,'RunQuery2');
	$validateArr 	= $obj_errorHandeling->get_permision_withApproval_cancel($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$userId,$programCode,'RunQuery2');
	if($validateArr['type']=='pass')
	{
		$detail_result	= $obj_faabricin_get->getSampleFabricInDetail($fabricInNo,$fabricInYear);
		while($row=mysqli_fetch_array($detail_result))
		{
			$checkDispatch_result = $obj_faabricin_get->checkDispatchRaised($row['SAMPLE_ORDER_NO'],$row['SAMPLE_ORDER_YEAR'],$row['COMBO'],$row['PART'],$row['GRADE'],$row['SIZE'],$row['SAMPLETYPE_ID'],$row['REQUIRED_DATE']);
			
			$count	= mysqli_num_rows($checkDispatch_result);
			if($count>0)
			{
				$checkStatus	= true;
			}
		}
		if($checkStatus)
		{
			$validateArr['type'] 	= 'fail';
			$validateArr['msg'] 	= 'Can not Cancelled. Sample Dispatch Raised ';
		}
	}
	return $validateArr;
}
function checkQtyBeforeSave($sampleOrderNo,$sampleOrderYear,$combo,$partId,$sampleTypeId,$size,$grade,$reqDate,$fabricInQty)
{
	global $obj_faabricin_get;
	$data = array();
	
	$fabInBalQtyArr	= $obj_faabricin_get->getSampleOrderFabInBalQty($sampleOrderNo,$sampleOrderYear,$combo,$partId,$grade,$size,$sampleTypeId,$reqDate);
	
	if($fabricInQty > $fabInBalQtyArr['intBalToFabricInQty'])
	{
		$data['type'] 	= 'fail';
		$data['msg'] 	= 'Some Qty exceed Sample Fabric In bal qty. ';
	}
	return $data;
}
?>
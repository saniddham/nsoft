<?php
/**
 * Created by PhpStorm.
 * User: Hasitha
 * Date: 1/26/2018
 * Time: 11:36 AM
 */


ini_set('display_errors',0);
session_start();
$backwardseperator = "../../../../";
$mainPath 	= $_SESSION['mainPath'];
$userId 	= $_SESSION['userId'];
$companyId 	= $_SESSION['CompanyID'];

$programName='Grading';
$programCode='PO1264';
require_once "../../../../dataAccess/Connector.php";
require_once $_SESSION['ROOT_PATH']."class/cls_permisions.php";


require_once $_SESSION['ROOT_PATH']."class/customerAndOperation/sample/cls_grading.php";
$obj_grading = new cls_grading($db);





if(isset($_REQUEST['Request_Type'])){
    $Request_type=  $_REQUEST['Request_Type'];
    if($Request_type == 'SaveModalData') {

        //var_dump($_REQUEST['MainData']); die;
        $MainData= json_decode($_REQUEST['MainData'], true);

        $ModalData= json_decode($_REQUEST['arr'], true);

        $result=$obj_grading->checkData($MainData);

        if($result ==true){
            $sql=$obj_grading->saveModalData($ModalData,$MainData);
            $flag=InserData($sql);
            $sql_check_default_double = "select
		trn_sampleinfomations_gradings.`DEFAULT`
		from 
		trn_sampleinfomations_gradings
		where
		SAMPLE_NO = $sampleNo AND 
		SAMPLE_YEAR =  $year  AND
		REVISION_NO = $revision AND
		COMBO =  $combo AND
		PRINT = $print AND
		SIZE = $size
		HAVING trn_sampleinfomations_gradings.`DEFAULT` = 1 
		";
            $result  	= 	$db->RunQuery2($sql);
            $rowcount	=	mysqli_num_rows($result);
            if ($rowcount>=2)
            {
                $flag = 0;
            }
            if($flag==1)
            {
                $responce["type"] = 'pass';
                $responce["msg"] = 'Insert Data Successfully';
            }else{
                $responce["type"] = 'pass';
                $responce["msg"] = 'Insert Data Successfully';
            }

        }else{

            $sql=$obj_grading->updateModalData($ModalData,$MainData);

            $flag=InserData($sql);

            $sql_check_default_double = "select
		trn_sampleinfomations_gradings.`DEFAULT`
		from 
		trn_sampleinfomations_gradings
		where
		SAMPLE_NO = $sampleNo AND 
		SAMPLE_YEAR =  $year  AND
		REVISION_NO = $revision AND
		COMBO =  $combo AND
		PRINT = $print AND
		SIZE = $size
		HAVING trn_sampleinfomations_gradings.`DEFAULT` = 1 
		";
            $result  	= 	$db->RunQuery2($sql);
            $rowcount	=	mysqli_num_rows($result);
            if ($rowcount>=2)
            {
                $flag = 0;
            }

            if($flag==1){
                $responce["type"] = 'pass';
                $responce["msg"] = 'Update Data Successfully';
            }else{
                $responce["type"] = 'pass';
                $responce["msg"] = 'Update Data Successfully';
            }
        }
        echo json_encode($responce);
        return ;
    }else if ($Request_type == 'DeleteRow'){
        $MainData= array(
            'sampleNo' => $_REQUEST['sampleNo'],
            'year' => $_REQUEST['year'],
            'revision' => $_REQUEST['revision'],
            'combo' => $_REQUEST['combo'],
            'print' => $_REQUEST['print'],
            'size' => $_REQUEST['size']
        );
        echo $result=DeleteRow($MainData);

    }else if ($Request_type == 'saveMaindata') {


        $mainData=json_decode($_REQUEST['MainData'],true);

        /*-SAVE METHOD-*/
        $result= saveGradingData($mainData,$_REQUEST);

        $res=count(array_keys($result, 'true')) == count($result);

        if($res == true){
            $result['msg'] 	= 'Successfully Save Data';
            $result['type'] 	=  '1';
            json_encode($result);
        }else {
            $result['msg'] 	= 'Failed to Save';
            $result['type'] 	=  '2';
            json_encode($result);
        }
        echo json_encode($result);

    }




}
if(($_GET['type']) && !empty($_GET['type'])){
    $type = $_GET['type'];
    if($type == 'LoadGraphicNo'){
        $year =$_GET['year'];
        //$row['intSampleNo']
        $result = LoadGraphicNo($year);
        while($row =mysqli_fetch_array($result)){
            $intSampleNo= $row['intSampleNo'];
            echo "<option value=$intSampleNo>$intSampleNo</option>";
        }


    }elseif ($type == 'LoadRevisionNo'){
        $SampleNo = $_GET['sampleNo'];
        $result= LoadRevision($SampleNo);
        while($row =mysqli_fetch_array($result)){
            $intRevNo= $row['intRevNo'];
            echo "<option value=$intRevNo>$intRevNo</option>";
        }
    }elseif ($type =='LoadCombo'){
        $RevisionNo =$_GET[
        'RevisionNo'];
        $year =$_GET['year'];
        $sampleNo =$_GET['sampleNo'];
        $html ='';
        $result = LoadCombo($RevisionNo,$year,$sampleNo);
        while($row =mysqli_fetch_array($result)){
            $strComboName= $row['strComboName'];
            $html .= "<option value='$strComboName'>$strComboName</option>";

        }
        echo $html;
    }elseif ($type =='LoadPrint') {


        $combo = $_REQUEST['combo'];
        $RevisionNo = $_REQUEST['Revision'];
        $year = $_REQUEST['year'];
        $sampleNo = $_REQUEST['sampleNo'];
        $result = LoadPrint($combo, $RevisionNo, $sampleNo, $year);
        while ($row = mysqli_fetch_array($result)) {

            $strPrintName = $row['strPrintName'];
            $html .= "<option value='$strPrintName'>$strPrintName</option>";
            // echo "<option value=".$strPrintName.">$strPrintName</option>";
        }
        echo $html;

    }else if($type =='LoadModalTable'){
        $combo = $_GET['combo'];
        $RevisionNo = $_GET['RevisionNo'];
        $year = $_GET['year'];
        $sampleNo = $_GET['sampleNo'];
        $print = $_GET['print'];

        $result = LoadModalTableData($year,$sampleNo,$combo,$print,$RevisionNo);

        while ($row = mysqli_fetch_array($result)) {

            $tables[]=array(
                'SIZE'=>$row['SIZE'],
                'DEFAULT'=>$row['DEFAULT'],
                'PRESENTAGE'=>$row['PRESENTAGE'],
                'WIDTH'=>$row['WIDTH'],
                'HEIGHT'=>$row['HEIGHT'],
            );
//            $html[] ="<tr><td><input type=\"button\"  value=\"Delete\" /></td>
//                    <td></td>
//                    <td></td>
//                    <td></td>
//                    <td></td>
//                    <td></td>
//                    <td></td>
//                </tr>";

        }
        $rowCount=0;
        foreach ($tables as $t){
            if($t['DEFAULT'] ==1){
                $sizeM=  $t['SIZE'];
                $html .="<tr><td><input id='deleteRow$rowCount' type=\"button\"  value=\"Delete\"/></td>
                    <td><input id=\"size\" name=\"size[]\" type=\"text\" value=".$t['SIZE']." class=\"grading size_gr\" /></td>
                    <td><input id=\"default$rowCount\" name=\"default\" type=\"radio\" value=".$t['DEFAULT']." class=\"grading grading_default selected\" /></td>
                    <td style=\"display:none;\"> <input id=\"uom\" name=\"uom[]\" type=\"text\" value=\"\" class=\"grading\"  /></td>
                    <td style=\"display:none;\"><input id=\"precentage\" name=\"precentage[]\" type=\"text\" value=".$t['PRESENTAGE']." class=\"grading\" /></td>
                    <td> <input id=\"height\" name=\"height[]\" type=\"text\" value=".$t['HEIGHT']." class=\"grading\" /></td>
                    <td><input id=\"width\" name=\"width[]\" type=\"text\" value=".$t['WIDTH']." class=\"grading\" /></td>
                </tr>";
            }else {
                $sizeM=  $t['SIZE'];
                $html .= "<tr><td><input id='deleteRow$rowCount' type=\"button\"  value=\"Delete\"/></td>
                    <td><input id=\"size\" name=\"size[]\" type=\"text\" value=" . $t['SIZE'] . " class=\"grading size_gr\" /></td>
                    <td><input id=\"default$rowCount\" name=\"default\" type=\"radio\" value=" . $t['DEFAULT'] . "  class=\"grading grading_default\" /></td>
                    <td style=\"display:none;\"> <input id=\"uom\" name=\"uom[]\" type=\"text\" value=\"\" class=\"grading\" /></td>
                    <td style=\"display:none;\"><input id=\"precentage\" name=\"precentage[]\" type=\"text\" value=" . $t['PRESENTAGE'] . " class=\"grading\" /></td>
                    <td> <input id=\"height\" name=\"height[]\" type=\"text\" value=" . $t['HEIGHT'] . " class=\"grading\" /></td>
                    <td><input id=\"width\" name=\"width[]\" type=\"text\" value=" . $t['WIDTH'] . " class=\"grading\" /></td>
                </tr>";
            }

            $rowCount++;
        }
        //var_dump($html); die;
        echo json_encode($html);
    }else if($type =='LoadSizeCombo'){
        $combo = $_GET['combo'];
        $RevisionNo = $_GET['revision'];
        $year = $_GET['year'];
        $sampleNo = $_GET['sampleNo'];
        $print = $_GET['print'];

        $result = LoadSizeCombo($year,$sampleNo,$combo,$print,$RevisionNo);
        while($row =mysqli_fetch_array($result)){

            $default =$row['DEFAULT'];
            $SIZE= $row['SIZE'];

            if($default ==1){

                echo "<option  selected value=$SIZE>$SIZE</option>";
            }else{

                echo "<option value=$SIZE>$SIZE</option>";
            }

        }
    }


}
/*---LOAD DATA TO TABLES ----*/

if(isset($_POST['type'])&&!empty($_POST['type'])){
    $type= $_POST['type'];
    if($type =='LoadSizeWiseTable'){
        $sampleYear = $_POST['year'];
        $sampleNo = $_POST['sampleNo'];
        $revision = $_POST['Revision'];
        $combo = $_POST['combo'];
        $print = $_POST['print'];
        $size= $_POST['size'];

        unset($inkType);
        unset($rollFrom);
        unset($nonRollFrom);
        unset($nonDirect);

        $inkType = array();
        $rollFrom = array();
        $nonRollFrom = array();
        $nonDirect = array();

        $inkType = ink_item_sampleCosumptionForSampleNo_sql($sampleNo, $sampleYear, $revision, $combo, $print, $size);
        $rollFrom = foil_item_sampleCosumptionForSampleNo_sql($sampleNo, $sampleYear, $revision, $combo, $print, $size);
        $nonRollFrom = special_item_sampleCosumptionForSampleNo_sql($sampleNo, $sampleYear, $revision, $combo, $print, $size);
        if ($print != 'SELECT PRINT') {
            $nonDirect = non_direct_item_sampleCosumptionForSampleNo_sql($sampleNo, $sampleYear, $revision, $combo, $print, $size);
        }
        unset($all_table);
        $all_table = array();
        $all_table = array(
            'inkType' => $inkType,
            'rollForm' => $rollFrom,
            'nonRollFrom' => $nonRollFrom,
            'nonDirect' => $nonDirect,
        );

        echo json_encode($all_table);

    }else if($type == 'LoadPrintWiseTable'){
        $sampleYear = $_POST['year'];
        $sampleNo = $_POST['sampleNo'];
        $revision = $_POST['Revision'];
        $combo = $_POST['combo'];
        $print = $_POST['print'];
        $inkType = array();
        $rollFrom = array();


        $nonRollFrom   = array();
        $nonDirect = array();
        $default = '';
        $nonDirect = 0;
        // $t1=array(1,2,3,4,5,6,7,8);
        $inkType = ink_item_sampleCosumptionForSampleNo_sql($sampleNo, $sampleYear, $revision, $combo, $print, $default);
        $rollFrom = foil_item_sampleCosumptionForSampleNo_sql($sampleNo, $sampleYear, $revision, $combo, $print, $default);
        $nonRollFrom = special_item_sampleCosumptionForSampleNo_sql($sampleNo, $sampleYear, $revision, $combo, $print, $default);

        if ($print != 'SELECT PRINT') {
            $nonDirect = non_direct_item_sampleCosumptionForSampleNo_sql($sampleNo, $sampleYear, $revision, $combo, $print, $default);
        }
        $size_dropdown = LoadSizeCombo($sampleYear, $sampleNo, $combo, $print, $revision);


        while ($row = mysqli_fetch_array($size_dropdown)) {

            $size_combo[] = array(
                'DEFAULT' => $row['DEFAULT'],
                'SIZE' => $row['SIZE']
            );

        }

        foreach ($size_combo as $size) {

            if ($size['DEFAULT'] == 1) {
                $sizeD= $size['SIZE'];

                $load_combo .= "<option selected value='$sizeD'>" . $size['SIZE'] . "</option>";
            } else {
                $sizeD= $size['SIZE'];

                $load_combo .= "<option value='$sizeD'>" . $size['SIZE'] . "</option>";
            }

        }
        // $load_combo .= "<option  selected value=''>SELECT SIZE </option>";
        $all_table = array(
            'inkType' => $inkType,
            'rollForm' => $rollFrom,
            'nonRollFrom' => $nonRollFrom,
            'size_combo' => $load_combo,
            'nonDirect' => $nonDirect,
        );

        echo json_encode($all_table);
        // LoadTables($year,$sampleNo,$RevisionNo,$combo,$print);
    }
}


function LoadGraphicNo($year)
{
    global $db;
    $db->OpenConnection();
    $sql = "SELECT DISTINCT
trn_sampleinfomations_details.intSampleNo
FROM
trn_sampleinfomations_details
where trn_sampleinfomations_details.intSampleYear = $year
";

    $result = $db->RunQuery2($sql);

    return $result;
}
function LoadRevision($SampleNo){
    global $db;
    $db->OpenConnection();
    $sql="SELECT DISTINCT
trn_sampleinfomations_details.intRevNo
-- trn_sampleinfomations_details.intSampleNo,
-- trn_sampleinfomations_details.intSampleYear
FROM
trn_sampleinfomations_details
INNER JOIN trn_sampleinfomations
ON trn_sampleinfomations.intSampleNo 		= trn_sampleinfomations_details.intSampleNo AND
trn_sampleinfomations.intSampleYear  		= trn_sampleinfomations_details.intSampleYear AND
trn_sampleinfomations.intRevisionNo    	= trn_sampleinfomations_details.intRevNo 

WHERE trn_sampleinfomations_details.intSampleNo=$SampleNo
AND
trn_sampleinfomations.intStatus = 1";
    $result = $db->RunQuery2($sql);
    return $result;
}
function LoadCombo($RevisionNo,$year,$sampleNo){
    global $db;
    $db->OpenConnection();
    $sql="SELECT DISTINCT
 -- trn_sampleinfomations_details.intRevNo,
-- trn_sampleinfomations_details.intSampleNo,
-- trn_sampleinfomations_details.intSampleYear,
trn_sampleinfomations_details.strComboName
FROM
	trn_sampleinfomations_details
WHERE
	trn_sampleinfomations_details.intSampleYear = $year
AND trn_sampleinfomations_details.intSampleNo = $sampleNo
AND trn_sampleinfomations_details.intRevNo =$RevisionNo
";
    $result =$db->RunQuery2($sql);
    return $result;
}
function LoadPrint($combo,$RevisionNo,$sampleNo,$year){
    global $db;
    $db->OpenConnection();
    $sql= "SELECT DISTINCT
-- trn_sampleinfomations_details.intRevNo,
-- trn_sampleinfomations_details.intSampleNo,
-- trn_sampleinfomations_details.intSampleYear,
-- trn_sampleinfomations_details.strComboName,
trn_sampleinfomations_details.strPrintName
FROM
	trn_sampleinfomations_details
WHERE
	trn_sampleinfomations_details.intSampleYear = $year
AND trn_sampleinfomations_details.intSampleNo = $sampleNo
AND trn_sampleinfomations_details.intRevNo =$RevisionNo 
AND trn_sampleinfomations_details.strComboName ='$combo'";

    $result =$db->RunQuery2($sql);
    return $result;
}
function LoadTables($year,$sampleNo,$RevisionNo,$combo,$print){
    global $db;
    $db->OpenConnection();
    $sql="";

    return ;
}

/*-----INK--------*/
function ink_item_sampleCosumptionForSampleNo_sql($sampleNo,$sampleYear,$revision,$combo,$print,$size){
    global $db;
    $db->OpenConnection();
    $sql= " SELECT
TB1.UOM,
TB1.itemName,
TB1.itemCode,
TB1.techniques,
TB1.ColorName,
TB1.intTechniqueId, 
TB1.intInkTypeId,
TB1.intColorId,
TB1.inkType,
TB1.intItem,
TB1.CONSUMPTION,
TB1.DEFAULT_SIZE,
TB1.WIDTH,
TB1.HEIGHT,
TB1.SELECTED_SIZE_WIDTH,
TB1.SELECTED_SIZE_HEIGHT,
TB1.EDITED,
TB1.EDITEDWIDTH,
TB1.EDITEDHEIGHT
FROM
  (
 SELECT
	t.UOM,
	t.itemName,
	t.itemCode,
	t.techniques,
	t.ColorName,
	t.inkType,
	t.intTechniqueId, 
	t.intInkTypeId,
	t.intColorId,
	intItem,
	ROUND(
		SUM(
			dblColorWeight / sumWeight * dblWeight / dblNoOfPcs
		),
		9
	) AS CONSUMPTION,
	t.DEFAULT_SIZE,
(if(t.DEFAULT_SIZE !='',
 (SELECT tsg.WIDTH FROM trn_sampleinfomations_gradings tsg WHERE  tsg.SIZE =t.DEFAULT_SIZE AND tsg.SAMPLE_NO='$sampleNo' AND tsg.SAMPLE_YEAR ='$sampleYear' AND tsg.COMBO='$combo' AND tsg.PRINT='$print' AND tsg.REVISION_NO='$revision'),
0))AS WIDTH,
(if(t.DEFAULT_SIZE !='',
 (SELECT tsg.HEIGHT FROM trn_sampleinfomations_gradings tsg WHERE  tsg.SIZE =t.DEFAULT_SIZE AND tsg.SAMPLE_NO='$sampleNo' AND tsg.SAMPLE_YEAR ='$sampleYear' AND tsg.COMBO='$combo' AND tsg.PRINT='$print' AND tsg.REVISION_NO='$revision'),
0))AS HEIGHT,
t.SELECTED_SIZE_WIDTH,
t.SELECTED_SIZE_HEIGHT,
t.EDITED,
t.EDITEDWIDTH,
t.EDITEDHEIGHT

FROM
	(
		SELECT
			SCR.intItem,
			SCR.dblWeight,
			trn_sampleinfomations_details_technical.dblColorWeight,
			mst_units.dblNoOfPcs,
			(
				SELECT
					Sum(
						trn_sample_color_recipes.dblWeight
					) AS sumWeight
				FROM
					trn_sample_color_recipes
				WHERE
					trn_sample_color_recipes.intSampleNo = SCR.intSampleNo
				AND trn_sample_color_recipes.intSampleYear = SCR.intSampleYear
				AND trn_sample_color_recipes.intRevisionNo = SCR.intRevisionNo
				AND trn_sample_color_recipes.strCombo = SCR.strCombo
				AND trn_sample_color_recipes.strPrintName = SCR.strPrintName
				AND trn_sample_color_recipes.intTechniqueId = SCR.intTechniqueId
				AND trn_sample_color_recipes.intInkTypeId = SCR.intInkTypeId
				AND trn_sample_color_recipes.intColorId = SCR.intColorId
			) AS sumWeight,
			mst_colors.strName AS ColorName,
			mst_inktypes.strName AS inkType,
			mst_techniques.strName AS techniques,
			mst_item.strCode AS itemCode,
			mst_item.strName AS itemName,
			mst_units.strCode AS UOM,
			SCR.intTechniqueId, 
			SCR.intInkTypeId,
			SCR.intColorId, ";

    if($size !=''){
        $sql .="(
				SELECT
					tsg.WIDTH
				FROM
					trn_sampleinfomations_gradings tsg
				WHERE
					tsg.SAMPLE_YEAR = SCR.intSampleYear
				AND tsg.SAMPLE_NO = SCR.intSampleNo
				AND tsg.REVISION_NO = SCR.intRevisionNo
				AND tsg.COMBO = SCR.strCombo
				AND tsg.PRINT = SCR.strPrintName
				AND tsg.SIZE='$size'
			) AS SELECTED_SIZE_WIDTH,
			(
				SELECT
					tsg.HEIGHT
				FROM
					trn_sampleinfomations_gradings tsg
				WHERE
					tsg.SAMPLE_YEAR = SCR.intSampleYear
				AND tsg.SAMPLE_NO = SCR.intSampleNo
				AND tsg.REVISION_NO = SCR.intRevisionNo
				AND tsg.COMBO = SCR.strCombo
				AND tsg.PRINT = SCR.strPrintName
				AND tsg.SIZE='$size'
			) AS SELECTED_SIZE_HEIGHT,
			
			(SELECT
trn_sampleinfomations_grading_ink_items.EDITED_CONSUMPTION

FROM
	trn_sampleinfomations_grading_ink_items
INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_ink_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
AND trn_sampleinfomations_grading_ink_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
AND trn_sampleinfomations_grading_ink_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
AND trn_sampleinfomations_grading_ink_items.COMBO = trn_sampleinfomations_gradings.COMBO
AND trn_sampleinfomations_grading_ink_items.PRINT = trn_sampleinfomations_gradings.PRINT
where 
trn_sampleinfomations_gradings.`DEFAULT`=1 AND
trn_sampleinfomations_grading_ink_items.SAMPLE_NO= SCR.intSampleNo
AND
trn_sampleinfomations_grading_ink_items.SAMPLE_YEAR= SCR.intSampleYear
AND
trn_sampleinfomations_grading_ink_items.REVISION = SCR.intRevisionNo
AND
trn_sampleinfomations_grading_ink_items.COMBO='$combo'
AND
trn_sampleinfomations_grading_ink_items.PRINT='$print'
AND
trn_sampleinfomations_grading_ink_items.SIZE='$size'
AND trn_sampleinfomations_grading_ink_items.COLOUR = SCR.intColorId
AND trn_sampleinfomations_grading_ink_items.INK_TYPE = SCR.intInkTypeId
AND trn_sampleinfomations_grading_ink_items.TECHNIQUE = SCR.intTechniqueId	
AND trn_sampleinfomations_grading_ink_items.ITEM = SCR.intItem

) AS EDITED,

	(
						SELECT
							trn_sampleinfomations_grading_ink_items.HEIGHT
						FROM
							trn_sampleinfomations_grading_ink_items
						INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_ink_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
						AND trn_sampleinfomations_grading_ink_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
						AND trn_sampleinfomations_grading_ink_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
						AND trn_sampleinfomations_grading_ink_items.COMBO = trn_sampleinfomations_gradings.COMBO
						AND trn_sampleinfomations_grading_ink_items.PRINT = trn_sampleinfomations_gradings.PRINT
						WHERE
							trn_sampleinfomations_gradings.`DEFAULT` = 1
						AND trn_sampleinfomations_grading_ink_items.SAMPLE_NO = SCR.intSampleNo
						AND trn_sampleinfomations_grading_ink_items.SAMPLE_YEAR = SCR.intSampleYear
						AND trn_sampleinfomations_grading_ink_items.REVISION = SCR.intRevisionNo
						AND trn_sampleinfomations_grading_ink_items.COMBO = '$combo'
						AND trn_sampleinfomations_grading_ink_items.PRINT = '$print'
						AND trn_sampleinfomations_grading_ink_items.ITEM = mst_item.intId
						AND trn_sampleinfomations_grading_ink_items.COLOUR = SD.intColorId
						AND trn_sampleinfomations_grading_ink_items.SIZE = '$size'
						AND trn_sampleinfomations_grading_ink_items.COLOUR = SCR.intColorId
						AND trn_sampleinfomations_grading_ink_items.INK_TYPE = SCR.intInkTypeId
						AND trn_sampleinfomations_grading_ink_items.TECHNIQUE = SCR.intTechniqueId
						AND trn_sampleinfomations_grading_ink_items.ITEM = SCR.intItem
					) AS EDITEDHEIGHT,
	(
						SELECT
							trn_sampleinfomations_grading_ink_items.WIDTH
						FROM
							trn_sampleinfomations_grading_ink_items
						INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_ink_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
						AND trn_sampleinfomations_grading_ink_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
						AND trn_sampleinfomations_grading_ink_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
						AND trn_sampleinfomations_grading_ink_items.COMBO = trn_sampleinfomations_gradings.COMBO
						AND trn_sampleinfomations_grading_ink_items.PRINT = trn_sampleinfomations_gradings.PRINT
						WHERE
							trn_sampleinfomations_gradings.`DEFAULT` = 1
						AND trn_sampleinfomations_grading_ink_items.SAMPLE_NO = SCR.intSampleNo
						AND trn_sampleinfomations_grading_ink_items.SAMPLE_YEAR = SCR.intSampleYear
						AND trn_sampleinfomations_grading_ink_items.REVISION = SCR.intRevisionNo
						AND trn_sampleinfomations_grading_ink_items.COMBO = '$combo'
						AND trn_sampleinfomations_grading_ink_items.PRINT = '$print'
						AND trn_sampleinfomations_grading_ink_items.ITEM = mst_item.intId
						AND trn_sampleinfomations_grading_ink_items.COLOUR = SD.intColorId
						AND trn_sampleinfomations_grading_ink_items.SIZE = '$size'
						AND trn_sampleinfomations_grading_ink_items.COLOUR = SCR.intColorId
						AND trn_sampleinfomations_grading_ink_items.INK_TYPE = SCR.intInkTypeId
						AND trn_sampleinfomations_grading_ink_items.TECHNIQUE = SCR.intTechniqueId
						AND trn_sampleinfomations_grading_ink_items.ITEM = SCR.intItem
					) AS EDITEDWIDTH,
";
    }else {
        $sql .="0 AS SELECTED_SIZE_WIDTH,
                0 AS SELECTED_SIZE_HEIGHT,
                 			(SELECT
trn_sampleinfomations_grading_ink_items.EDITED_CONSUMPTION

FROM
	trn_sampleinfomations_grading_ink_items
INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_ink_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
AND trn_sampleinfomations_grading_ink_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
AND trn_sampleinfomations_grading_ink_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
AND trn_sampleinfomations_grading_ink_items.COMBO = trn_sampleinfomations_gradings.COMBO
AND trn_sampleinfomations_grading_ink_items.PRINT = trn_sampleinfomations_gradings.PRINT
where 
trn_sampleinfomations_gradings.`DEFAULT`=1 AND
trn_sampleinfomations_grading_ink_items.SAMPLE_NO= SCR.intSampleNo
AND
trn_sampleinfomations_grading_ink_items.SAMPLE_YEAR= SCR.intSampleYear
AND
trn_sampleinfomations_grading_ink_items.REVISION = SCR.intRevisionNo
AND
trn_sampleinfomations_grading_ink_items.COMBO='$combo'
AND
trn_sampleinfomations_grading_ink_items.PRINT='$print'
AND trn_sampleinfomations_grading_ink_items.ITEM=mst_item.intId
AND  trn_sampleinfomations_grading_ink_items.COLOUR=SD.intColorId
AND trn_sampleinfomations_grading_ink_items.SIZE = trn_sampleinfomations_gradings.SIZE
AND trn_sampleinfomations_grading_ink_items.COLOUR = SCR.intColorId
AND trn_sampleinfomations_grading_ink_items.INK_TYPE = SCR.intInkTypeId
AND trn_sampleinfomations_grading_ink_items.TECHNIQUE = SCR.intTechniqueId
AND trn_sampleinfomations_grading_ink_items.ITEM = SCR.intItem				
) AS EDITED,
	(
						SELECT
							trn_sampleinfomations_grading_ink_items.HEIGHT
						FROM
							trn_sampleinfomations_grading_ink_items
						INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_ink_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
						AND trn_sampleinfomations_grading_ink_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
						AND trn_sampleinfomations_grading_ink_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
						AND trn_sampleinfomations_grading_ink_items.COMBO = trn_sampleinfomations_gradings.COMBO
						AND trn_sampleinfomations_grading_ink_items.PRINT = trn_sampleinfomations_gradings.PRINT
						WHERE
							trn_sampleinfomations_gradings.`DEFAULT` = 1
						AND trn_sampleinfomations_grading_ink_items.SAMPLE_NO = SCR.intSampleNo
						AND trn_sampleinfomations_grading_ink_items.SAMPLE_YEAR = SCR.intSampleYear
						AND trn_sampleinfomations_grading_ink_items.REVISION = SCR.intRevisionNo
						AND trn_sampleinfomations_grading_ink_items.COMBO = '$combo'
						AND trn_sampleinfomations_grading_ink_items.PRINT = '$print'
						AND trn_sampleinfomations_grading_ink_items.ITEM = mst_item.intId
						AND trn_sampleinfomations_grading_ink_items.COLOUR = SD.intColorId
						AND trn_sampleinfomations_grading_ink_items.SIZE = trn_sampleinfomations_gradings.SIZE
						AND trn_sampleinfomations_grading_ink_items.COLOUR = SCR.intColorId
						AND trn_sampleinfomations_grading_ink_items.INK_TYPE = SCR.intInkTypeId
						AND trn_sampleinfomations_grading_ink_items.TECHNIQUE = SCR.intTechniqueId
						AND trn_sampleinfomations_grading_ink_items.ITEM = SCR.intItem
					) AS EDITEDHEIGHT,
	(
						SELECT
							trn_sampleinfomations_grading_ink_items.WIDTH
						FROM
							trn_sampleinfomations_grading_ink_items
						INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_ink_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
						AND trn_sampleinfomations_grading_ink_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
						AND trn_sampleinfomations_grading_ink_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
						AND trn_sampleinfomations_grading_ink_items.COMBO = trn_sampleinfomations_gradings.COMBO
						AND trn_sampleinfomations_grading_ink_items.PRINT = trn_sampleinfomations_gradings.PRINT
						WHERE
							trn_sampleinfomations_gradings.`DEFAULT` = 1
						AND trn_sampleinfomations_grading_ink_items.SAMPLE_NO = SCR.intSampleNo
						AND trn_sampleinfomations_grading_ink_items.SAMPLE_YEAR = SCR.intSampleYear
						AND trn_sampleinfomations_grading_ink_items.REVISION = SCR.intRevisionNo
						AND trn_sampleinfomations_grading_ink_items.COMBO = '$combo'
						AND trn_sampleinfomations_grading_ink_items.PRINT = '$print'
						AND trn_sampleinfomations_grading_ink_items.ITEM = mst_item.intId
						AND trn_sampleinfomations_grading_ink_items.COLOUR = SD.intColorId
						AND trn_sampleinfomations_grading_ink_items.SIZE = trn_sampleinfomations_gradings.SIZE
						AND trn_sampleinfomations_grading_ink_items.COLOUR = SCR.intColorId
						AND trn_sampleinfomations_grading_ink_items.INK_TYPE = SCR.intInkTypeId
						AND trn_sampleinfomations_grading_ink_items.TECHNIQUE = SCR.intTechniqueId
						AND trn_sampleinfomations_grading_ink_items.ITEM = SCR.intItem
					) AS EDITEDWIDTH,
";
    }
    $sql .=" ( SELECT
      tsg.SIZE
      FROM
      trn_sampleinfomations_gradings tsg
      WHERE
      tsg.SAMPLE_YEAR=SCR.intSampleYear AND
       tsg.SAMPLE_NO =SCR.intSampleNo AND
      tsg.REVISION_NO = SCR.intRevisionNo AND
      tsg.COMBO = SCR.strCombo AND
      tsg.PRINT = SCR.strPrintName AND tsg.DEFAULT=1 ) AS DEFAULT_SIZE  ";

    $sql .=" FROM
			trn_sample_color_recipes AS SCR
		INNER JOIN trn_sampleinfomations_details AS SD ON SCR.intSampleNo = SD.intSampleNo
		AND SCR.intSampleYear = SD.intSampleYear
		AND SCR.intRevisionNo = SD.intRevNo
		AND SCR.strCombo = SD.strComboName
		AND SCR.strPrintName = SD.strPrintName
		AND SCR.intColorId = SD.intColorId
		AND SCR.intTechniqueId = SD.intTechniqueId
		INNER JOIN trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = SCR.intSampleNo
		AND trn_sampleinfomations_details_technical.intSampleYear = SCR.intSampleYear
		AND trn_sampleinfomations_details_technical.intRevNo = SCR.intRevisionNo
		AND trn_sampleinfomations_details_technical.strComboName = SCR.strCombo
		AND trn_sampleinfomations_details_technical.strPrintName = SCR.strPrintName
		AND trn_sampleinfomations_details_technical.intColorId = SCR.intColorId
		AND trn_sampleinfomations_details_technical.intInkTypeId = SCR.intInkTypeId
		INNER JOIN mst_item ON mst_item.intId = SCR.intItem
		INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
		INNER JOIN mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
		INNER JOIN mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
		-- added
		INNER JOIN mst_colors ON SCR.intColorId = mst_colors.intId
		INNER JOIN mst_inktypes ON SCR.intInkTypeId = mst_inktypes.intId
		INNER JOIN mst_techniques ON mst_inktypes.intTechniqueId = mst_techniques.intId
					WHERE SCR.intSampleNo='$sampleNo' 
					AND SCR.intSampleYear='$sampleYear' 
					AND SCR.intRevisionNo='$revision' 
					AND SCR.strCombo='$combo' 
					AND SCR.strPrintName='$print' 
					group by 
                     SCR.intTechniqueId 
					,SCR.intInkTypeId 
					, SCR.intColorId 
                    ,SCR.intItem
					 ) as t 
					   GROUP BY
					t.intTechniqueId,
					t.intInkTypeId,
					t.intColorId,
					t.intItem)AS TB1 ";
    //echo $sql; die;
    // echo $sql;


    $result = $db->RunQuery2($sql);
    while($row   =mysqli_fetch_array($result)){
        //$auto_consumption=round(($row['CONSUMPTION'] /($row['WIDTH'] *$row['HEIGHT']))*($row['SELECTED_SIZE_HEIGHT']*$row['SELECTED_SIZE_WIDTGH']),2);
        if($row['EDITEDHEIGHT'] !='' &&$row['EDITEDWIDTH'] != ''){
            $auto_consumption = ($row['CONSUMPTION'] / ($row['WIDTH'] * $row['HEIGHT'])) * ($row['EDITEDHEIGHT'] * $row['EDITEDWIDTH']);

        }else {
            $auto_consumption = ($row['CONSUMPTION'] / ($row['WIDTH'] * $row['HEIGHT'])) * ($row['SELECTED_SIZE_HEIGHT'] * $row['SELECTED_SIZE_WIDTH']);
        }

        //echo "({$row['CONSUMPTION'] }/({$row['WIDTH']} *{$row['HEIGHT']}))*({$row['SELECTED_SIZE_HEIGHT']}*{$row['SELECTED_SIZE_WIDTH']})";

        $InkType[] = array(
            "UOM" =>$row['UOM'],
            "itemName" => $row['itemName'],
            "itemCode" => $row['itemCode'],
            "techniques"=>$row['techniques'],
            "ColorName"=>$row['ColorName'],
            "inkType"=>$row['inkType'],
            "intItem"=>$row['intItem'],
            "CONSUMPTION"=>$row['CONSUMPTION'],
            "DEFAULT_SIZE"=>$row['DEFAULT_SIZE'],
            "WIDTH"=>$row['WIDTH'],
            "HEIGHT"=>$row['HEIGHT'],
            "SELECTED_SIZE_WIDTH"=>$row['SELECTED_SIZE_WIDTH'],
            "SELECTED_SIZE_HEIGHT"=>$row['SELECTED_SIZE_HEIGHT'],
            "AUTO_CONSUMPTION"=>number_format((float)$auto_consumption, 8, '.', ''),
            "intTechniqueId"=>$row['intTechniqueId'],
            "intInkTypeId"=>$row['intInkTypeId'],
            "intColorId"=>$row['intColorId'],
            "EDITED_CONSUMPTION"=>$row['EDITED'],
            "EDITEDHEIGHT"=>$row['EDITEDHEIGHT'],
            "EDITEDWIDTH"=>$row['EDITEDWIDTH']

        );


    }

    if(!empty($InkType)){

        return $InkType;
    }
    return 0;

}

/*-----ROLL FORM  ITEM ----*/
function foil_item_sampleCosumptionForSampleNo_sql($sampleNo,$sampleYear,$revision,$combo,$print,$size){

    global $db;
    $db->OpenConnection();
    $sql	="  
    SELECT
    TB1.intItem,
TB1.CONSUMPTION,
TB1.ColorName,
TB1.techniques,
TB1.itemCode,
TB1.itemName,
TB1.UOM,
TB1.DEFAULT_SIZE,
(IF(TB1.DEFAULT_SIZE !=''  ,
	(SELECT tsg.WIDTH FROM trn_sampleinfomations_gradings tsg WHERE  tsg.SIZE =TB1.DEFAULT_SIZE AND tsg.SAMPLE_NO='$sampleNo' AND tsg.SAMPLE_YEAR ='$sampleYear' AND tsg.COMBO='$combo' AND tsg.PRINT='$print' AND tsg.REVISION_NO='$revision'),

0)) AS WIDTH,

(IF(TB1.DEFAULT_SIZE !=''  ,
	(SELECT tsg.HEIGHT FROM trn_sampleinfomations_gradings tsg WHERE  tsg.SIZE =TB1.DEFAULT_SIZE  AND tsg.SAMPLE_NO='$sampleNo' AND tsg.SAMPLE_YEAR ='$sampleYear' AND tsg.COMBO='$combo' AND tsg.PRINT='$print' AND tsg.REVISION_NO='$revision'),

0)) AS HEIGHT,
TB1.SELECTED_SIZE_WIDTH,
TB1.SELECTED_SIZE_HEIGHT,
TB1.intColorId,
TB1.intTechniqueId,
TB1.EDITED,
TB1.EDITEDWIDTH,
TB1.EDITEDHEIGHT
    FROM 
    ( SELECT
	mst_item.intId AS intItem,
	sum(SFC.dblMeters) AS CONSUMPTION,
	mst_colors.strName AS ColorName,
	mst_techniques.strName AS techniques,
	mst_item.strCode AS itemCode,
	mst_item.strName AS itemName,
	mst_units.strCode AS UOM,
	SFC.intColorId,
    SFC.intTechniqueId,

	  (
      SELECT
      tsg.SIZE
      FROM
      trn_sampleinfomations_gradings tsg
      WHERE
      tsg.SAMPLE_YEAR=SFC.intSampleYear AND
      tsg.SAMPLE_NO =SFC.intSampleNo AND
      tsg.REVISION_NO = SFC.intRevisionNo AND
      tsg.COMBO = SFC.strCombo AND
      tsg.PRINT = SFC.strPrintName AND tsg.DEFAULT=1 ) AS DEFAULT_SIZE,";
    if($size !=''){

        $sql .=" (
				SELECT
					tsg.WIDTH
				FROM
					trn_sampleinfomations_gradings tsg
				WHERE
					tsg.SAMPLE_YEAR = SFC.intSampleYear
				AND tsg.SAMPLE_NO = SFC.intSampleNo
				AND tsg.REVISION_NO = SFC.intRevisionNo
				AND tsg.COMBO = SFC.strCombo
				AND tsg.PRINT = SFC.strPrintName
				AND tsg.SIZE ='$size'
			) AS SELECTED_SIZE_WIDTH,
			(
				SELECT
					tsg.HEIGHT
				FROM
					trn_sampleinfomations_gradings tsg
				WHERE
					tsg.SAMPLE_YEAR = SFC.intSampleYear
				AND tsg.SAMPLE_NO = SFC.intSampleNo
				AND tsg.REVISION_NO = SFC.intRevisionNo
				AND tsg.COMBO = SFC.strCombo
				AND tsg.PRINT = SFC.strPrintName
				AND tsg.SIZE ='$size'
			) AS SELECTED_SIZE_HEIGHT,
			 (SELECT
trn_sampleinfomations_grading_roll_items.EDITED_CONSUMPTION

FROM
	trn_sampleinfomations_grading_roll_items
INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_roll_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
AND trn_sampleinfomations_grading_roll_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
AND trn_sampleinfomations_grading_roll_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
AND trn_sampleinfomations_grading_roll_items.COMBO = trn_sampleinfomations_gradings.COMBO
AND trn_sampleinfomations_grading_roll_items.PRINT = trn_sampleinfomations_gradings.PRINT
where 
 trn_sampleinfomations_gradings.`DEFAULT`=1 AND
trn_sampleinfomations_grading_roll_items.SAMPLE_NO= '$sampleNo'
AND
trn_sampleinfomations_grading_roll_items.SAMPLE_YEAR= '$sampleYear'
AND
trn_sampleinfomations_grading_roll_items.REVISION = '$revision'
AND
trn_sampleinfomations_grading_roll_items.COMBO='$combo'
AND
trn_sampleinfomations_grading_roll_items.PRINT='$print'
AND
trn_sampleinfomations_grading_roll_items.SIZE='$size'

AND trn_sampleinfomations_grading_roll_items.ITEM=mst_item.intId
AND trn_sampleinfomations_grading_roll_items.COLOUR = mst_colors.intId
AND trn_sampleinfomations_grading_roll_items.TECHNIQUE = mst_techniques.intId
) AS EDITED,
(
				SELECT
					trn_sampleinfomations_grading_roll_items.WIDTH
				FROM
					trn_sampleinfomations_grading_roll_items
				INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_roll_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
				AND trn_sampleinfomations_grading_roll_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
				AND trn_sampleinfomations_grading_roll_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
				AND trn_sampleinfomations_grading_roll_items.COMBO = trn_sampleinfomations_gradings.COMBO
				AND trn_sampleinfomations_grading_roll_items.PRINT = trn_sampleinfomations_gradings.PRINT
				WHERE
					trn_sampleinfomations_gradings.`DEFAULT` = 1
				AND trn_sampleinfomations_grading_roll_items.SAMPLE_NO = SFC.intSampleNo
				AND trn_sampleinfomations_grading_roll_items.SAMPLE_YEAR = SFC.intSampleYear
				AND trn_sampleinfomations_grading_roll_items.REVISION = SFC.intRevisionNo
				AND trn_sampleinfomations_grading_roll_items.COMBO = '$combo'
				AND trn_sampleinfomations_grading_roll_items.PRINT = '$print'
				AND trn_sampleinfomations_grading_roll_items.SIZE = '$size'
				AND trn_sampleinfomations_grading_roll_items.ITEM = mst_item.intId
				AND trn_sampleinfomations_grading_roll_items.COLOUR = mst_colors.intId
				AND trn_sampleinfomations_grading_roll_items.TECHNIQUE = mst_techniques.intId
			) AS EDITEDWIDTH,

(
				SELECT
					trn_sampleinfomations_grading_roll_items.HEIGHT
				FROM
					trn_sampleinfomations_grading_roll_items
				INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_roll_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
				AND trn_sampleinfomations_grading_roll_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
				AND trn_sampleinfomations_grading_roll_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
				AND trn_sampleinfomations_grading_roll_items.COMBO = trn_sampleinfomations_gradings.COMBO
				AND trn_sampleinfomations_grading_roll_items.PRINT = trn_sampleinfomations_gradings.PRINT
				WHERE
					trn_sampleinfomations_gradings.`DEFAULT` = 1
				AND trn_sampleinfomations_grading_roll_items.SAMPLE_NO = SFC.intSampleNo
				AND trn_sampleinfomations_grading_roll_items.SAMPLE_YEAR = SFC.intSampleYear
				AND trn_sampleinfomations_grading_roll_items.REVISION = SFC.intRevisionNo
				AND trn_sampleinfomations_grading_roll_items.COMBO = '$combo'
				AND trn_sampleinfomations_grading_roll_items.PRINT = '$print'
				AND trn_sampleinfomations_grading_roll_items.SIZE = '$size'
				AND trn_sampleinfomations_grading_roll_items.ITEM = mst_item.intId
				AND trn_sampleinfomations_grading_roll_items.COLOUR = mst_colors.intId
				AND trn_sampleinfomations_grading_roll_items.TECHNIQUE = mst_techniques.intId
			) AS EDITEDHEIGHT
			 ";
    }else {
        $sql .="0 AS SELECTED_SIZE_HEIGHT,
                0 AS SELECTED_SIZE_WIDTH,
                (SELECT
trn_sampleinfomations_grading_roll_items.EDITED_CONSUMPTION

FROM
	trn_sampleinfomations_grading_roll_items
INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_roll_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
AND trn_sampleinfomations_grading_roll_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
AND trn_sampleinfomations_grading_roll_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
AND trn_sampleinfomations_grading_roll_items.COMBO = trn_sampleinfomations_gradings.COMBO
AND trn_sampleinfomations_grading_roll_items.PRINT = trn_sampleinfomations_gradings.PRINT
where 
trn_sampleinfomations_gradings.`DEFAULT`=1 AND
trn_sampleinfomations_grading_roll_items.SAMPLE_NO= SFC.intSampleNo
AND
trn_sampleinfomations_grading_roll_items.SAMPLE_YEAR= SFC.intSampleYear
AND
trn_sampleinfomations_grading_roll_items.REVISION = SFC.intRevisionNo
AND
trn_sampleinfomations_grading_roll_items.COMBO='$combo'
AND
trn_sampleinfomations_grading_roll_items.PRINT='$print'
AND
trn_sampleinfomations_grading_roll_items.SIZE =trn_sampleinfomations_gradings.SIZE
AND 
trn_sampleinfomations_grading_roll_items.ITEM=mst_item.intId
AND trn_sampleinfomations_grading_roll_items.COLOUR = mst_colors.intId
AND trn_sampleinfomations_grading_roll_items.TECHNIQUE = mst_techniques.intId
) AS EDITED,
(
				SELECT
					trn_sampleinfomations_grading_roll_items.WIDTH
				FROM
					trn_sampleinfomations_grading_roll_items
				INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_roll_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
				AND trn_sampleinfomations_grading_roll_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
				AND trn_sampleinfomations_grading_roll_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
				AND trn_sampleinfomations_grading_roll_items.COMBO = trn_sampleinfomations_gradings.COMBO
				AND trn_sampleinfomations_grading_roll_items.PRINT = trn_sampleinfomations_gradings.PRINT
				WHERE
					trn_sampleinfomations_gradings.`DEFAULT` = 1
				AND trn_sampleinfomations_grading_roll_items.SAMPLE_NO = SFC.intSampleNo
				AND trn_sampleinfomations_grading_roll_items.SAMPLE_YEAR = SFC.intSampleYear
				AND trn_sampleinfomations_grading_roll_items.REVISION = SFC.intRevisionNo
				AND trn_sampleinfomations_grading_roll_items.COMBO = '$combo'
				AND trn_sampleinfomations_grading_roll_items.PRINT = '$print'
				AND trn_sampleinfomations_grading_roll_items.SIZE = trn_sampleinfomations_gradings.SIZE
				AND trn_sampleinfomations_grading_roll_items.ITEM = mst_item.intId
				AND trn_sampleinfomations_grading_roll_items.COLOUR = mst_colors.intId
				AND trn_sampleinfomations_grading_roll_items.TECHNIQUE = mst_techniques.intId
			) AS EDITEDWIDTH,

(
				SELECT
					trn_sampleinfomations_grading_roll_items.HEIGHT
				FROM
					trn_sampleinfomations_grading_roll_items
				INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_roll_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
				AND trn_sampleinfomations_grading_roll_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
				AND trn_sampleinfomations_grading_roll_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
				AND trn_sampleinfomations_grading_roll_items.COMBO = trn_sampleinfomations_gradings.COMBO
				AND trn_sampleinfomations_grading_roll_items.PRINT = trn_sampleinfomations_gradings.PRINT
				WHERE
					trn_sampleinfomations_gradings.`DEFAULT` = 1
				AND trn_sampleinfomations_grading_roll_items.SAMPLE_NO = SFC.intSampleNo
				AND trn_sampleinfomations_grading_roll_items.SAMPLE_YEAR = SFC.intSampleYear
				AND trn_sampleinfomations_grading_roll_items.REVISION = SFC.intRevisionNo
				AND trn_sampleinfomations_grading_roll_items.COMBO = '$combo'
				AND trn_sampleinfomations_grading_roll_items.PRINT = '$print'
				AND trn_sampleinfomations_grading_roll_items.SIZE = trn_sampleinfomations_gradings.SIZE
				AND trn_sampleinfomations_grading_roll_items.ITEM = mst_item.intId
				AND trn_sampleinfomations_grading_roll_items.COLOUR = mst_colors.intId
				AND trn_sampleinfomations_grading_roll_items.TECHNIQUE = mst_techniques.intId
			) AS EDITEDHEIGHT";
    }

    $sql .=" FROM
trn_sample_foil_consumption SFC
INNER JOIN mst_item ON mst_item.intId = SFC.intItem
INNER JOIN mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
INNER JOIN mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
INNER JOIN mst_units ON mst_units.intId = mst_item.intUOM
INNER JOIN mst_financecurrency ON mst_financecurrency.intId = mst_item.intCurrency
INNER JOIN mst_colors ON SFC.intColorId = mst_colors.intId
INNER JOIN mst_techniques ON SFC.intTechniqueId = mst_techniques.intId
					WHERE
					SFC.intSampleNo = '$sampleNo' AND
					SFC.intSampleYear = '$sampleYear' AND
					SFC.intRevisionNo = '$revision' AND
					SFC.strCombo = '$combo' AND
					SFC.strPrintName = '$print' AND 
					SFC.dblMeters > 0
					GROUP BY
					mst_maincategory.intId,
					mst_subcategory.intId,
					mst_techniques.intId,
					mst_colors.intId,
					mst_item.intId		) AS TB1 ";


    $result = $db->RunQuery2($sql);

    while($row =mysqli_fetch_array($result)){
        // $auto_consumption=round(($row['CONSUMPTION'] /($row['WIDTH'] *$row['HEIGHT']))*($row['SELECTED_SIZE_HEIGHT']*$row['SELECTED_SIZE_WIDTGH']),2);

        if($row['EDITEDHEIGHT'] !='' &&$row['EDITEDWIDTH'] != ''){
            $auto_consumption = ($row['CONSUMPTION'] / ($row['WIDTH'] * $row['HEIGHT'])) * ($row['EDITEDHEIGHT'] * $row['EDITEDWIDTH']);

        }else {

            $auto_consumption = ($row['CONSUMPTION'] / ($row['WIDTH'] * $row['HEIGHT'])) * ($row['SELECTED_SIZE_HEIGHT'] * $row['SELECTED_SIZE_WIDTH']);
        }

        //$auto_consumption=($row['CONSUMPTION'] /($row['WIDTH'] *$row['HEIGHT']))*($row['SELECTED_SIZE_HEIGHT']*$row['SELECTED_SIZE_WIDTH']);

        $RollFrom[] = array(
            "UOM" => $row['UOM'],
            "itemName" => $row['itemName'],
            "itemCode" => $row['itemCode'],
            "techniques"=>$row['techniques'],
            "ColorName"=>$row['ColorName'],
            "intItem"=>$row['intItem'],
            "CONSUMPTION"=>$row['CONSUMPTION'],
            "DEFAULT_SIZE"=>$row['DEFAULT_SIZE'],
            "WIDTH"=>$row['WIDTH'],
            "HEIGHT"=>$row['HEIGHT'],
            "SELECTED_SIZE_HEIGHT"=>$row['SELECTED_SIZE_HEIGHT'],
            "SELECTED_SIZE_WIDTH"=>$row['SELECTED_SIZE_WIDTH'],
            "AUTO_CONSUMPTION"=>number_format((float)$auto_consumption, 8, '.', ''),
            "intTechniqueId"=>$row['intTechniqueId'],
            "intColorId"=>$row['intColorId'],
            "EDITED_CONSUMPTION"=>$row['EDITED'],
            "EDITEDHEIGHT"=>$row['EDITEDHEIGHT'],
            "EDITEDWIDTH"=>$row['EDITEDWIDTH'],

        );

    }
    if(!empty($RollFrom)){

        //var_dump($RollFrom); die;
        return $RollFrom;
    }
    return 0;

}

/*------ NON ROLL FORM -----*/
function special_item_sampleCosumptionForSampleNo_sql($sampleNo,$sampleYear,$revision,$combo,$print,$size){
    global $db;

    $sql	= "SELECT
TB1.intItem,
TB1.CONSUMPTION,
TB1.ColorName,
TB1.techniques,
TB1.itemCode,
TB1.itemName,
TB1.itemCode,
TB1.UOM,
TB1.DEFAULT_SIZE,
(IF(TB1.DEFAULT_SIZE !=''  ,
	(SELECT tsg.WIDTH FROM trn_sampleinfomations_gradings tsg WHERE  tsg.SIZE =TB1.DEFAULT_SIZE  AND tsg.SAMPLE_NO='$sampleNo' AND tsg.SAMPLE_YEAR ='$sampleYear' AND tsg.COMBO='$combo' AND tsg.PRINT='$print' AND tsg.REVISION_NO='$revision'),

0)) AS WIDTH,

(IF(TB1.DEFAULT_SIZE !=''  ,
	(SELECT tsg.HEIGHT FROM trn_sampleinfomations_gradings tsg WHERE  tsg.SIZE =TB1.DEFAULT_SIZE  AND tsg.SAMPLE_NO='$sampleNo' AND tsg.SAMPLE_YEAR ='$sampleYear' AND tsg.COMBO='$combo' AND tsg.PRINT='$print' AND tsg.REVISION_NO='$revision'),

0)) AS HEIGHT,
TB1.SELECTED_SIZE_WIDTH,
TB1.SELECTED_SIZE_HEIGHT,
TB1.SELECTED_SIZE_HEIGHT,
TB1.intColorId,
TB1.intTechniqueId,
TB1.EDITED,
TB1.EDITEDHEIGHT,
TB1.EDITEDWIDTH
  FROM
(SELECT
	    mst_item.intId AS intItem,
	sum(
		SSC.dblQty / dblNoOfPcs
	) AS CONSUMPTION,
	mst_colors.strName AS ColorName,
mst_techniques.strName AS techniques,
mst_item.strCode AS itemCode,
mst_item.strName AS itemName,
mst_units.strCode AS UOM,
SSC.intColorId,
SSC.intTechniqueId, ";

    if($size != '') {
        $sql .=" (
				SELECT
					tsg.WIDTH
				FROM
					trn_sampleinfomations_gradings tsg
				WHERE
					tsg.SAMPLE_YEAR = SSC.intSampleYear
				AND tsg.SAMPLE_NO = SSC.intSampleNo
				AND tsg.REVISION_NO = SSC.intRevisionNo
				AND tsg.COMBO = SSC.strCombo
				AND tsg.PRINT = SSC.strPrintName
				AND tsg.SIZE='$size'
			) AS SELECTED_SIZE_WIDTH ,
			(
				SELECT
					tsg.HEIGHT
				FROM
					trn_sampleinfomations_gradings tsg
				WHERE
					tsg.SAMPLE_YEAR = SSC.intSampleYear
				AND tsg.SAMPLE_NO = SSC.intSampleNo
				AND tsg.REVISION_NO = SSC.intRevisionNo
				AND tsg.COMBO = SSC.strCombo
				AND tsg.PRINT = SSC.strPrintName
				AND tsg.SIZE='$size'
			) AS SELECTED_SIZE_HEIGHT,
			                (SELECT
trn_sampleinfomations_grading_non_roll_items.EDITED_CONSUMPTION

FROM
	trn_sampleinfomations_grading_non_roll_items
INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_non_roll_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
AND trn_sampleinfomations_grading_non_roll_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
AND trn_sampleinfomations_grading_non_roll_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
AND trn_sampleinfomations_grading_non_roll_items.COMBO = trn_sampleinfomations_gradings.COMBO
AND trn_sampleinfomations_grading_non_roll_items.PRINT = trn_sampleinfomations_gradings.PRINT
where 
trn_sampleinfomations_gradings.`DEFAULT`=1 AND
trn_sampleinfomations_grading_non_roll_items.SAMPLE_NO= SSC.intSampleNo
AND
trn_sampleinfomations_grading_non_roll_items.SAMPLE_YEAR= SSC.intSampleYear
AND
trn_sampleinfomations_grading_non_roll_items.REVISION = SSC.intRevisionNo
AND
trn_sampleinfomations_grading_non_roll_items.COMBO='$combo'
AND
trn_sampleinfomations_grading_non_roll_items.PRINT='$print'
AND
trn_sampleinfomations_grading_non_roll_items.SIZE='$size'
AND trn_sampleinfomations_grading_non_roll_items.ITEM=mst_item.intId

AND trn_sampleinfomations_grading_non_roll_items.COLOUR = mst_colors.intId
AND trn_sampleinfomations_grading_non_roll_items.TECHNIQUE = mst_techniques.intId
AND trn_sampleinfomations_grading_non_roll_items.COLOUR = SSC.intColorId
AND trn_sampleinfomations_grading_non_roll_items.TECHNIQUE = mst_techniques.intId
) AS EDITED,
(
				SELECT
					trn_sampleinfomations_grading_non_roll_items.WIDTH
				FROM
					trn_sampleinfomations_grading_non_roll_items
				INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_non_roll_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
				AND trn_sampleinfomations_grading_non_roll_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
				AND trn_sampleinfomations_grading_non_roll_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
				AND trn_sampleinfomations_grading_non_roll_items.COMBO = trn_sampleinfomations_gradings.COMBO
				AND trn_sampleinfomations_grading_non_roll_items.PRINT = trn_sampleinfomations_gradings.PRINT
				WHERE
					trn_sampleinfomations_gradings.`DEFAULT` = 1
				AND trn_sampleinfomations_grading_non_roll_items.SAMPLE_NO = SSC.intSampleNo
				AND trn_sampleinfomations_grading_non_roll_items.SAMPLE_YEAR = SSC.intSampleYear
				AND trn_sampleinfomations_grading_non_roll_items.REVISION = SSC.intRevisionNo
				AND trn_sampleinfomations_grading_non_roll_items.COMBO = '$combo'
				AND trn_sampleinfomations_grading_non_roll_items.PRINT = '$print'
				AND trn_sampleinfomations_grading_non_roll_items.SIZE = '$size'
				AND trn_sampleinfomations_grading_non_roll_items.ITEM = mst_item.intId
				AND trn_sampleinfomations_grading_non_roll_items.COLOUR = SSC.intColorId
				AND trn_sampleinfomations_grading_non_roll_items.TECHNIQUE = mst_techniques.intId
			) AS EDITEDWIDTH,
(
				SELECT
					trn_sampleinfomations_grading_non_roll_items.HEIGHT
				FROM
					trn_sampleinfomations_grading_non_roll_items
				INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_non_roll_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
				AND trn_sampleinfomations_grading_non_roll_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
				AND trn_sampleinfomations_grading_non_roll_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
				AND trn_sampleinfomations_grading_non_roll_items.COMBO = trn_sampleinfomations_gradings.COMBO
				AND trn_sampleinfomations_grading_non_roll_items.PRINT = trn_sampleinfomations_gradings.PRINT
				WHERE
					trn_sampleinfomations_gradings.`DEFAULT` = 1
				AND trn_sampleinfomations_grading_non_roll_items.SAMPLE_NO = SSC.intSampleNo
				AND trn_sampleinfomations_grading_non_roll_items.SAMPLE_YEAR = SSC.intSampleYear
				AND trn_sampleinfomations_grading_non_roll_items.REVISION = SSC.intRevisionNo
				AND trn_sampleinfomations_grading_non_roll_items.COMBO = '$combo'
				AND trn_sampleinfomations_grading_non_roll_items.PRINT = '$print'
				AND trn_sampleinfomations_grading_non_roll_items.SIZE = '$size'
				AND trn_sampleinfomations_grading_non_roll_items.ITEM = mst_item.intId
				AND trn_sampleinfomations_grading_non_roll_items.COLOUR = SSC.intColorId
				AND trn_sampleinfomations_grading_non_roll_items.TECHNIQUE = mst_techniques.intId
			) AS EDITEDHEIGHT,
				";
    }else {
        $sql .=" NULL AS SELECTED_SIZE_WIDTH,
                 NULL AS SELECTED_SIZE_HEIGHT, 
                 (SELECT
trn_sampleinfomations_grading_non_roll_items.EDITED_CONSUMPTION

FROM
	trn_sampleinfomations_grading_non_roll_items
INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_non_roll_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
AND trn_sampleinfomations_grading_non_roll_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
AND trn_sampleinfomations_grading_non_roll_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
AND trn_sampleinfomations_grading_non_roll_items.COMBO = trn_sampleinfomations_gradings.COMBO
AND trn_sampleinfomations_grading_non_roll_items.PRINT = trn_sampleinfomations_gradings.PRINT
where 
 trn_sampleinfomations_gradings.`DEFAULT`=1 AND
trn_sampleinfomations_grading_non_roll_items.SAMPLE_NO= SSC.intSampleNo
AND
trn_sampleinfomations_grading_non_roll_items.SAMPLE_YEAR= SSC.intSampleYear
AND
trn_sampleinfomations_grading_non_roll_items.REVISION = SSC.intRevisionNo
AND
trn_sampleinfomations_grading_non_roll_items.COMBO='$combo'
AND
trn_sampleinfomations_grading_non_roll_items.PRINT='$print'
AND
trn_sampleinfomations_grading_non_roll_items.SIZE =trn_sampleinfomations_gradings.SIZE
AND trn_sampleinfomations_grading_non_roll_items.ITEM=mst_item.intId
AND trn_sampleinfomations_grading_non_roll_items.COLOUR = SSC.intColorId
AND trn_sampleinfomations_grading_non_roll_items.TECHNIQUE = mst_techniques.intId
) AS EDITED,
(
				SELECT
					trn_sampleinfomations_grading_non_roll_items.WIDTH
				FROM
					trn_sampleinfomations_grading_non_roll_items
				INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_non_roll_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
				AND trn_sampleinfomations_grading_non_roll_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
				AND trn_sampleinfomations_grading_non_roll_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
				AND trn_sampleinfomations_grading_non_roll_items.COMBO = trn_sampleinfomations_gradings.COMBO
				AND trn_sampleinfomations_grading_non_roll_items.PRINT = trn_sampleinfomations_gradings.PRINT
				WHERE
					trn_sampleinfomations_gradings.`DEFAULT` = 1
				AND trn_sampleinfomations_grading_non_roll_items.SAMPLE_NO = SSC.intSampleNo
				AND trn_sampleinfomations_grading_non_roll_items.SAMPLE_YEAR = SSC.intSampleYear
				AND trn_sampleinfomations_grading_non_roll_items.REVISION = SSC.intRevisionNo
				AND trn_sampleinfomations_grading_non_roll_items.COMBO = '$combo'
				AND trn_sampleinfomations_grading_non_roll_items.PRINT = '$print'
				AND trn_sampleinfomations_grading_non_roll_items.SIZE = trn_sampleinfomations_gradings.SIZE
				AND trn_sampleinfomations_grading_non_roll_items.ITEM = mst_item.intId
				AND trn_sampleinfomations_grading_non_roll_items.COLOUR = SSC.intColorId
				AND trn_sampleinfomations_grading_non_roll_items.TECHNIQUE = mst_techniques.intId
			) AS EDITEDWIDTH,
(
				SELECT
					trn_sampleinfomations_grading_non_roll_items.HEIGHT
				FROM
					trn_sampleinfomations_grading_non_roll_items
				INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_non_roll_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
				AND trn_sampleinfomations_grading_non_roll_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
				AND trn_sampleinfomations_grading_non_roll_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
				AND trn_sampleinfomations_grading_non_roll_items.COMBO = trn_sampleinfomations_gradings.COMBO
				AND trn_sampleinfomations_grading_non_roll_items.PRINT = trn_sampleinfomations_gradings.PRINT
				WHERE
					trn_sampleinfomations_gradings.`DEFAULT` = 1
				AND trn_sampleinfomations_grading_non_roll_items.SAMPLE_NO = SSC.intSampleNo
				AND trn_sampleinfomations_grading_non_roll_items.SAMPLE_YEAR = SSC.intSampleYear
				AND trn_sampleinfomations_grading_non_roll_items.REVISION = SSC.intRevisionNo
				AND trn_sampleinfomations_grading_non_roll_items.COMBO = '$combo'
				AND trn_sampleinfomations_grading_non_roll_items.PRINT = '$print'
				AND trn_sampleinfomations_grading_non_roll_items.SIZE = trn_sampleinfomations_gradings.SIZE
				AND trn_sampleinfomations_grading_non_roll_items.ITEM = mst_item.intId
				AND trn_sampleinfomations_grading_non_roll_items.COLOUR = SSC.intColorId
				AND trn_sampleinfomations_grading_non_roll_items.TECHNIQUE = mst_techniques.intId
			) AS EDITEDHEIGHT,
                 ";
    }

    $sql .="(
      SELECT
      tsg.SIZE
      FROM
      trn_sampleinfomations_gradings tsg
      WHERE
      tsg.SAMPLE_YEAR=SSC.intSampleYear AND
       tsg.SAMPLE_NO =SSC.intSampleNo AND
      tsg.REVISION_NO = SSC.intRevisionNo AND
      tsg.COMBO = SSC.strCombo AND
      tsg.PRINT = SSC.strPrintName AND tsg.DEFAULT=1 ) AS DEFAULT_SIZE 
     FROM
	trn_sample_spitem_consumption SSC
INNER JOIN mst_item ON mst_item.intId = SSC.intItem
INNER JOIN mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
INNER JOIN mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
INNER JOIN mst_units ON mst_units.intId = mst_item.intUOM
INNER JOIN mst_financecurrency ON mst_financecurrency.intId = mst_item.intCurrency
INNER JOIN mst_colors ON SSC.intColorId = mst_colors.intId
INNER JOIN mst_techniques ON SSC.intTechniqueId = mst_techniques.intId
				WHERE
				SSC.intSampleNo = '$sampleNo' AND
				SSC.intSampleYear = '$sampleYear' AND
				SSC.intRevisionNo = '$revision' AND
				SSC.strCombo = '$combo' AND
				SSC.strPrintName = '$print' AND 
				SSC.dblQty > 0 
				GROUP BY
				mst_maincategory.intId,
				mst_subcategory.intId,
				mst_techniques.intId,
				SSC.intColorId,
				mst_item.intId ) AS TB1";

    $result = $db->RunQuery2($sql);
    while($row =mysqli_fetch_array($result)){
        //$auto_consumption=round(($row['CONSUMPTION'] /($row['WIDTH'] *$row['HEIGHT']))*($row['SELECTED_SIZE_HEIGHT']*$row['SELECTED_SIZE_WIDTGH']),2);
        // echo "({$row['CONSUMPTION'] }/({$row['WIDTH']} *{$row['HEIGHT']}))*({$row['SELECTED_SIZE_HEIGHT']}*{$row['SELECTED_SIZE_WIDTGH']})";

        if($row['EDITEDHEIGHT'] !='' &&$row['EDITEDWIDTH'] != ''){
            $auto_consumption = ($row['CONSUMPTION'] / ($row['WIDTH'] * $row['HEIGHT'])) * ($row['EDITEDHEIGHT'] * $row['EDITEDWIDTH']);

        }else {

            $auto_consumption = ($row['CONSUMPTION'] / ($row['WIDTH'] * $row['HEIGHT'])) * ($row['SELECTED_SIZE_HEIGHT'] * $row['SELECTED_SIZE_WIDTH']);
        }


      //  $auto_consumption=($row['CONSUMPTION'] /($row['WIDTH'] *$row['HEIGHT']))*($row['SELECTED_SIZE_HEIGHT']*$row['SELECTED_SIZE_WIDTH']);

        //var_dump(round($row['CONSUMPTION'], 8)); die;
        $nonRollFrom[] = array(
            "UOM" => $row['UOM'],
            "itemName" => $row['itemName'],
            "itemCode" => $row['itemCode'],
            "techniques"=>$row['techniques'],
            "ColorName"=>$row['ColorName'],
            "intItem"=>$row['intItem'],
            "CONSUMPTION"=>round($row['CONSUMPTION'], 8),
            "DEFAULT_SIZE"=>$row['DEFAULT_SIZE'],
            "WIDTH"=>$row['WIDTH'],
            "HEIGHT"=>$row['HEIGHT'],
            "SELECTED_SIZE_WIDTH"=>$row['SELECTED_SIZE_WIDTH'],
            "SELECTED_SIZE_HEIGHT"=>$row['SELECTED_SIZE_HEIGHT'],
            "AUTO_CONSUMPTION"=>number_format((float)$auto_consumption, 8, '.', ''),
            "intColorId"=>$row['intColorId'],
            "intTechniqueId"=>$row['intTechniqueId'],
            "EDITED_CONSUMPTION"=>$row['EDITED'],
            "EDITEDHEIGHT"=>$row['EDITEDHEIGHT'],
            "EDITEDWIDTH"=>$row['EDITEDWIDTH']
        );
    }

    if(!empty($nonRollFrom)){
        return $nonRollFrom;
    }
    return 0;


}


/*------ NON DIRECT ITEM -----*/
function non_direct_item_sampleCosumptionForSampleNo_sql($sampleNo,$sampleYear,$revision,$combo,$print,$size){

    global $db;
    $db->OpenConnection();
    $sql	="SELECT
TB1.itemCode,
TB1.itemName,
TB1.CONSUMPTION,
TB1.UOM,
TB1.intItem,
TB1.DEFAULT_SIZE,
(IF(TB1.DEFAULT_SIZE !=''  ,
	(SELECT tsg.WIDTH FROM trn_sampleinfomations_gradings tsg WHERE  tsg.SIZE =TB1.DEFAULT_SIZE  AND tsg.SAMPLE_NO='$sampleNo' AND tsg.SAMPLE_YEAR ='$sampleYear' AND tsg.COMBO='$combo' AND tsg.PRINT='$print' AND tsg.REVISION_NO='$revision'),

0)) AS WIDTH,
(IF(TB1.DEFAULT_SIZE !=''  ,
	(SELECT tsg.HEIGHT FROM trn_sampleinfomations_gradings tsg WHERE  tsg.SIZE =TB1.DEFAULT_SIZE  AND tsg.SAMPLE_NO='$sampleNo' AND tsg.SAMPLE_YEAR ='$sampleYear' AND tsg.COMBO='$combo' AND tsg.PRINT='$print' AND tsg.REVISION_NO='$revision'),

0)) AS HEIGHT,
TB1.SELECTED_SIZE_WIDTH,
TB1.SELECTED_SIZE_HEIGHT,
TB1.EDITED,
TB1.EDITEDWIDTH,
TB1.EDITEDHEIGHT


FROM
(SELECT
	mst_item.strCode AS itemCode,
	mst_item.strName AS itemName,
	sum(SNDRC.CONSUMPTION) AS CONSUMPTION,
	mst_units.strCode AS UOM,
	SNDRC.ITEM AS intItem,
	(
		SELECT
			tsg.SIZE
		FROM
			trn_sampleinfomations_gradings tsg
		WHERE
			tsg.SAMPLE_YEAR = SNDRC.SAMPLE_YEAR
		AND tsg.SAMPLE_NO = SNDRC.SAMPLE_NO
		AND tsg.REVISION_NO = SNDRC.REVISION_NO
		AND 
		  tsg.COMBO = '$combo' AND
		  tsg.PRINT = '$print' AND tsg.DEFAULT = 1
	) AS DEFAULT_SIZE,";

    if($size != ''){
        $sql .="(
				SELECT
					tsg.WIDTH
				FROM
					trn_sampleinfomations_gradings tsg
				WHERE
					tsg.SAMPLE_YEAR = SNDRC.SAMPLE_YEAR
				AND tsg.SAMPLE_NO = SNDRC.SAMPLE_NO
				AND tsg.REVISION_NO = SNDRC.REVISION_NO
				AND tsg.COMBO = '$combo'
				AND tsg.PRINT = '$print'
				AND tsg.SIZE ='$size'
			) AS SELECTED_SIZE_WIDTH,
					(
				SELECT
					tsg.HEIGHT
				FROM
					trn_sampleinfomations_gradings tsg
				WHERE
					tsg.SAMPLE_YEAR = SNDRC.SAMPLE_YEAR
				AND tsg.SAMPLE_NO = SNDRC.SAMPLE_NO
				AND tsg.REVISION_NO = SNDRC.REVISION_NO
				AND tsg.COMBO = '$combo'
				AND tsg.PRINT = '$print'
				AND tsg.SIZE ='$size'
			) AS SELECTED_SIZE_HEIGHT,
				(SELECT
trn_sampleinfomations_grading_non_direct_items.EDITED_CONSUMPTION

FROM
	trn_sampleinfomations_grading_non_direct_items
INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_non_direct_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
AND trn_sampleinfomations_grading_non_direct_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
AND trn_sampleinfomations_grading_non_direct_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
AND trn_sampleinfomations_grading_non_direct_items.COMBO = trn_sampleinfomations_gradings.COMBO
AND trn_sampleinfomations_grading_non_direct_items.PRINT = trn_sampleinfomations_gradings.PRINT
where 
trn_sampleinfomations_gradings.`DEFAULT`=1 AND
trn_sampleinfomations_grading_non_direct_items.SAMPLE_NO= SNDRC.SAMPLE_NO
AND
trn_sampleinfomations_grading_non_direct_items.SAMPLE_YEAR= SNDRC.SAMPLE_YEAR
AND
trn_sampleinfomations_grading_non_direct_items.REVISION = SNDRC.REVISION_NO
AND
trn_sampleinfomations_grading_non_direct_items.COMBO='$combo'
AND
trn_sampleinfomations_grading_non_direct_items.PRINT='$print'
AND
trn_sampleinfomations_grading_non_direct_items.SIZE='$size'
AND trn_sampleinfomations_grading_non_direct_items.ITEM=mst_item.intId)AS EDITED,
	(
				SELECT
					trn_sampleinfomations_grading_non_direct_items.WIDTH
				FROM
					trn_sampleinfomations_grading_non_direct_items
				INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_non_direct_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
				AND trn_sampleinfomations_grading_non_direct_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
				AND trn_sampleinfomations_grading_non_direct_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
				AND trn_sampleinfomations_grading_non_direct_items.COMBO = trn_sampleinfomations_gradings.COMBO
				AND trn_sampleinfomations_grading_non_direct_items.PRINT = trn_sampleinfomations_gradings.PRINT
				WHERE
					trn_sampleinfomations_gradings.`DEFAULT` = 1
				AND trn_sampleinfomations_grading_non_direct_items.SAMPLE_NO = SNDRC.SAMPLE_NO
				AND trn_sampleinfomations_grading_non_direct_items.SAMPLE_YEAR = SNDRC.SAMPLE_YEAR
				AND trn_sampleinfomations_grading_non_direct_items.REVISION = SNDRC.REVISION_NO
				AND trn_sampleinfomations_grading_non_direct_items.COMBO = '$combo'
				AND trn_sampleinfomations_grading_non_direct_items.PRINT = '$print'
				AND trn_sampleinfomations_grading_non_direct_items.SIZE = '$size'
				AND trn_sampleinfomations_grading_non_direct_items.ITEM = mst_item.intId
			) AS EDITEDWIDTH,
(
				SELECT
					trn_sampleinfomations_grading_non_direct_items.HEIGHT
				FROM
					trn_sampleinfomations_grading_non_direct_items
				INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_non_direct_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
				AND trn_sampleinfomations_grading_non_direct_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
				AND trn_sampleinfomations_grading_non_direct_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
				AND trn_sampleinfomations_grading_non_direct_items.COMBO = trn_sampleinfomations_gradings.COMBO
				AND trn_sampleinfomations_grading_non_direct_items.PRINT = trn_sampleinfomations_gradings.PRINT
				WHERE
					trn_sampleinfomations_gradings.`DEFAULT` = 1
				AND trn_sampleinfomations_grading_non_direct_items.SAMPLE_NO = SNDRC.SAMPLE_NO
				AND trn_sampleinfomations_grading_non_direct_items.SAMPLE_YEAR = SNDRC.SAMPLE_YEAR
				AND trn_sampleinfomations_grading_non_direct_items.REVISION = SNDRC.REVISION_NO
				AND trn_sampleinfomations_grading_non_direct_items.COMBO = '$combo'
				AND trn_sampleinfomations_grading_non_direct_items.PRINT = '$print'
				AND trn_sampleinfomations_grading_non_direct_items.SIZE = '$size'
				AND trn_sampleinfomations_grading_non_direct_items.ITEM = mst_item.intId
			) AS EDITEDHEIGHT
			";
    }else {
        $sql .= "NULL AS SELECTED_SIZE_WIDTH,
                 NULL AS SELECTED_SIZE_HEIGHT,
                  (SELECT
trn_sampleinfomations_grading_non_direct_items.EDITED_CONSUMPTION

FROM
	trn_sampleinfomations_grading_non_direct_items
INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_non_direct_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
AND trn_sampleinfomations_grading_non_direct_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
AND trn_sampleinfomations_grading_non_direct_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
AND trn_sampleinfomations_grading_non_direct_items.COMBO = trn_sampleinfomations_gradings.COMBO
AND trn_sampleinfomations_grading_non_direct_items.PRINT = trn_sampleinfomations_gradings.PRINT
where 
trn_sampleinfomations_gradings.`DEFAULT`=1 AND
trn_sampleinfomations_grading_non_direct_items.SAMPLE_NO= SNDRC.SAMPLE_NO
AND
trn_sampleinfomations_grading_non_direct_items.SAMPLE_YEAR= SNDRC.SAMPLE_YEAR
AND
trn_sampleinfomations_grading_non_direct_items.REVISION = SNDRC.REVISION_NO
AND
trn_sampleinfomations_grading_non_direct_items.COMBO='$combo'
AND
trn_sampleinfomations_grading_non_direct_items.PRINT='$print'
AND
trn_sampleinfomations_grading_non_direct_items.SIZE =trn_sampleinfomations_gradings.SIZE
AND trn_sampleinfomations_grading_non_direct_items.ITEM=mst_item.intId) AS EDITED,

	(
				SELECT
					trn_sampleinfomations_grading_non_direct_items.WIDTH
				FROM
					trn_sampleinfomations_grading_non_direct_items
				INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_non_direct_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
				AND trn_sampleinfomations_grading_non_direct_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
				AND trn_sampleinfomations_grading_non_direct_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
				AND trn_sampleinfomations_grading_non_direct_items.COMBO = trn_sampleinfomations_gradings.COMBO
				AND trn_sampleinfomations_grading_non_direct_items.PRINT = trn_sampleinfomations_gradings.PRINT
				WHERE
					trn_sampleinfomations_gradings.`DEFAULT` = 1
				AND trn_sampleinfomations_grading_non_direct_items.SAMPLE_NO = SNDRC.SAMPLE_NO
				AND trn_sampleinfomations_grading_non_direct_items.SAMPLE_YEAR = SNDRC.SAMPLE_YEAR
				AND trn_sampleinfomations_grading_non_direct_items.REVISION = SNDRC.REVISION_NO
				AND trn_sampleinfomations_grading_non_direct_items.COMBO = '$combo'
				AND trn_sampleinfomations_grading_non_direct_items.PRINT = '$print'
				AND trn_sampleinfomations_grading_non_direct_items.SIZE = trn_sampleinfomations_gradings.SIZE
				AND trn_sampleinfomations_grading_non_direct_items.ITEM = mst_item.intId
			) AS EDITEDWIDTH,
(
				SELECT
					trn_sampleinfomations_grading_non_direct_items.HEIGHT
				FROM
					trn_sampleinfomations_grading_non_direct_items
				INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_non_direct_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
				AND trn_sampleinfomations_grading_non_direct_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
				AND trn_sampleinfomations_grading_non_direct_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
				AND trn_sampleinfomations_grading_non_direct_items.COMBO = trn_sampleinfomations_gradings.COMBO
				AND trn_sampleinfomations_grading_non_direct_items.PRINT = trn_sampleinfomations_gradings.PRINT
				WHERE
					trn_sampleinfomations_gradings.`DEFAULT` = 1
				AND trn_sampleinfomations_grading_non_direct_items.SAMPLE_NO = SNDRC.SAMPLE_NO
				AND trn_sampleinfomations_grading_non_direct_items.SAMPLE_YEAR = SNDRC.SAMPLE_YEAR
				AND trn_sampleinfomations_grading_non_direct_items.REVISION = SNDRC.REVISION_NO
				AND trn_sampleinfomations_grading_non_direct_items.COMBO = '$combo'
				AND trn_sampleinfomations_grading_non_direct_items.PRINT = '$print'
				AND trn_sampleinfomations_grading_non_direct_items.SIZE = trn_sampleinfomations_gradings.SIZE
				AND trn_sampleinfomations_grading_non_direct_items.ITEM = mst_item.intId
			) AS EDITEDHEIGHT
";
    }

    $sql .=" FROM
	trn_sample_non_direct_rm_consumption SNDRC
INNER JOIN mst_item ON mst_item.intId = SNDRC.ITEM
INNER JOIN mst_units ON mst_units.intId = mst_item.intUOM
    WHERE
	SNDRC.SAMPLE_NO = '$sampleNo' AND
	SNDRC.SAMPLE_YEAR = '$sampleYear' AND
	SNDRC.REVISION_NO = '$revision' 
	GROUP BY 
	SNDRC.ITEM ) AS TB1";


    $result = $db->RunQuery2($sql);
    while($row =mysqli_fetch_array($result)){

        if($row['EDITEDHEIGHT'] !='' &&$row['EDITEDWIDTH'] != ''){
            $auto_consumption = ($row['CONSUMPTION'] / ($row['WIDTH'] * $row['HEIGHT'])) * ($row['EDITEDHEIGHT'] * $row['EDITEDWIDTH']);

        }else{

            $auto_consumption = ($row['CONSUMPTION'] / ($row['WIDTH'] * $row['HEIGHT'])) * ($row['SELECTED_SIZE_HEIGHT'] * $row['SELECTED_SIZE_WIDTH']);
        }
//var_dump($auto_consumption); die;
       // $auto_consumption=($row['CONSUMPTION'] /($row['WIDTH'] *$row['HEIGHT']))*($row['SELECTED_SIZE_HEIGHT']*$row['SELECTED_SIZE_WIDTH']);
      //  $editedConsumption=loadEditConsumption ('trn_sampleinfomations_grading_non_direct_items',$sampleNo,$sampleYear,$revision,$combo,$print,$size);

        $nonDirectitem[] = array(
            "UOM" => $row['UOM'],
            "itemName" => $row['itemName'],
            "itemCode" => $row['itemCode'],
            "intItem"=>$row['intItem'],
            "CONSUMPTION"=>$row['CONSUMPTION'],
            "DEFAULT_SIZE"=>$row['DEFAULT_SIZE'],
            "WIDTH"=>$row['WIDTH'],
            "HEIGHT"=>$row['HEIGHT'],
            "SELECTED_SIZE_HEIGHT"=>$row['SELECTED_SIZE_HEIGHT'],
            "SELECTED_SIZE_WIDTH"=>$row['SELECTED_SIZE_WIDTH'],
            "AUTO_CONSUMPTION"=>number_format((float)$auto_consumption, 8, '.', ''),
            "EDITED_CONSUMPTION"=>$row['EDITED'],
            "EDITEDWIDTH"=> $row['EDITEDWIDTH'],
            "EDITEDHEIGHT"=> $row['EDITEDHEIGHT']
        );


    }




    if(!empty($nonDirectitem)){
        return $nonDirectitem;
    }
    return 0;


}


function InserData($sqls) {
    global $db;
    $db->OpenConnection();
    $flag=1;
    $sql='';
    foreach ($sqls as $sql){



        $result = $db->RunQuery2($sql);
        if($result == false){
            $flag=0;
        }

    }


    return $flag;
}

function LoadModalTableData($year,$sampleNo,$combo,$print,$RevisionNo){
    global $db;
    $db->OpenConnection();
    $sql="SELECT
trn_sampleinfomations_gradings.SIZE,
trn_sampleinfomations_gradings.`DEFAULT`,
trn_sampleinfomations_gradings.PRESENTAGE,
trn_sampleinfomations_gradings.WIDTH,
trn_sampleinfomations_gradings.HEIGHT
FROM
trn_sampleinfomations_gradings
WHERE
trn_sampleinfomations_gradings.SAMPLE_NO='$sampleNo' AND
trn_sampleinfomations_gradings.SAMPLE_YEAR='$year' AND
trn_sampleinfomations_gradings.REVISION_NO='$RevisionNo' AND
trn_sampleinfomations_gradings.COMBO='$combo'AND
trn_sampleinfomations_gradings.PRINT='$print'";
    $result = $db->RunQuery2($sql);
    return $result;
}

function LoadSizeCombo($year,$sampleNo,$combo,$print,$RevisionNo){

    global $db;
    $db->OpenConnection();
    $sql="SELECT
    trn_sampleinfomations_gradings.SIZE,
    trn_sampleinfomations_gradings.DEFAULT
    FROM
    trn_sampleinfomations_gradings
    WHERE
    trn_sampleinfomations_gradings.SAMPLE_NO='$sampleNo' AND
    trn_sampleinfomations_gradings.SAMPLE_YEAR='$year' AND
    trn_sampleinfomations_gradings.REVISION_NO='$RevisionNo' AND
    trn_sampleinfomations_gradings.COMBO='$combo'AND
    trn_sampleinfomations_gradings.PRINT='$print'
  ";
//echo $sql;
    $result = $db->RunQuery2($sql);

    return $result;

}

/*---SAVE TABLE DATA--*/
function saveGradingData($mainData,$tableData){
    global $db;
    $inkTypeMainTable =json_decode($_REQUEST['inkTypeMainTable'], true);
    $rollFormMainTable    =json_decode($_REQUEST['rollFormMainTable'],true);
    $nonRollFormMainTable = json_decode($_REQUEST['nonRollFormMainTable'],true);
    $nonDirectMainTable = json_decode($_REQUEST['nonDirectMainTable'],true);

    try {
        $db->OpenConnection();
        $year = $mainData[0]['year'];
        $SampleNo = $mainData[0]['sampleNo'];
        $RevisionNo = $mainData[0]['RevisionNo'];
        $Combo = $mainData[0]['combo'];
        $Print = $mainData[0]['print'];
        $Size = $mainData[0]['size'];

        if ($inkTypeMainTable) {


            $sql = "DELETE
FROM
	`trn_sampleinfomations_grading_ink_items`
WHERE
	(`SAMPLE_NO` = '$SampleNo')
AND (`SAMPLE_YEAR` = '$year')
AND (`REVISION` = '$RevisionNo')
AND (`COMBO` = '$Combo')
AND (`PRINT` = '$Print')
AND (`SIZE` ='$Size')";


            $delete = $db->RunQuery2($sql);
            if($delete != true) {

                $db->rollback();
                $arr[]=array(
                    "flag"=>0,
                    "type"=>'failed to delete',
                );
                $result=json_encode($arr);
                throw new Exception($db->errormsg);
                // return $result ;
            }
            // $inktypeCount = count($inkTypeMainTable[0]['itemCode_inktype']) ;
            $i=0;
            foreach ($inkTypeMainTable as $tableData){
                // for ($i = 0; $i <= $inktypeCount; $i++) {

                $colourName_inktype = $tableData['colourName_inktype'];
                $inktype = $tableData['inktype'];
                $itemCode_inktype = $tableData['itemCode_inktype'];
                $edited_consumpition_inktype = $tableData['edited_consumpition_inktype'];
                $edit_consumption = !empty($edited_consumpition_inktype) ? "'$edited_consumpition_inktype'" : "NULL";
                $defconsumption_inktype = $tableData['defconsumption_inktype'];
                $height_inktype = $tableData['height_inktype'];
                $width_inktype = $tableData['width_inktype'];
                $techniques_inktype = $tableData['techniques_inktype'];
                $autoConsumption_inktype = $tableData['autoConsumption_inktype'];
                $user = $_SESSION['userId'];
                if($edit_consumption=="NULL")
                    $edit_consumption = $autoConsumption_inktype;
                $inktype_sql = "INSERT INTO `trn_sampleinfomations_grading_ink_items` (
	`SAMPLE_NO`,
	`SAMPLE_YEAR`,
	`REVISION`,
	`COMBO`,
	`PRINT`,
	`SIZE`,
	`COLOUR`,
	`TECHNIQUE`,
	`INK_TYPE`,
	`ITEM`,
	`WIDTH`,
	`HEIGHT`,
	`AUTOMATED_CONSUMPTION`,
	`EDITED_CONSUMPTION`,
	`LAST_MODIFIED_BY`,
	`MODIFIED_TIME`
)
VALUES
	(
		'$SampleNo',
		'$year',
		'$RevisionNo',
		'$Combo',
		'$Print',
		'$Size',
		'$colourName_inktype',
		'$techniques_inktype',
		'$inktype',
		'$itemCode_inktype',
		'$width_inktype',
		'$height_inktype',
		'$autoConsumption_inktype',
		$edit_consumption,
		'$user',
		NOW()
	) ";

                $result[] = $db->RunQuery2($inktype_sql);

                if($result[$i] != true){
                    $db->rollback();
                    $arr[]=array(
                        "flag"=>1,
                        "type"=>'failed to insert',
                    );
                    $result=json_encode($arr);
                    throw new Exception($db->errormsg);
                    // return $result ;
                }
                $i++;
            }

        }


        if ($rollFormMainTable) {
            $sql = "DELETE
FROM
	`trn_sampleinfomations_grading_roll_items`
WHERE
	(`SAMPLE_NO` = '$SampleNo')
AND (`SAMPLE_YEAR` = '$year')
AND (`REVISION` = '$RevisionNo')
AND (`COMBO` = '$Combo')
AND (`PRINT` = '$Print')
AND (`SIZE` ='$Size')";

            $delete = $db->RunQuery2($sql);

            if($delete != true) {

                $db->rollback();
                $arr[]=array(
                    "flag"=>0,
                    "type"=>'failed to delete',
                );
                $result=json_encode($arr);
                throw new Exception($db->errormsg);
                //return $result ;
            }

            //$rollFormCoint = count($tableData['itemCode_rollForm'])-1;
            //for($i=0;$i<=$rollFormCoint;$i++){
            $i=0;
            foreach ($rollFormMainTable AS $tableData) {

                $colourName_rollForm = $tableData['colourName_rollForm'];
                $techniques_rollForm=$tableData['techniques_rollForm'];
                $itemCode_rollForm=$tableData['itemCode_rollForm'];
                // $defconsumption_rollForm=$tableData['defconsumption_rollForm'][$i];
                $width_rollForme=$tableData['width_rollForm'];
                $height_rollForm=$tableData['height_rollForm'];
                $autoConsumption_rollForm = $tableData['autoConsumption_rollForm'];
                $edited_consumpition_rollForm=$tableData['edited_consumpition_rollForm'];
                $edit_consumption=!empty($edited_consumpition_rollForm) ? "'$edited_consumpition_rollForm'" : "NULL";
                $user=$_SESSION['userId'];

                if($edit_consumption=="NULL")
                    $edit_consumption = $autoConsumption_rollForm;

                $roolFrom_sql= "INSERT INTO `trn_sampleinfomations_grading_roll_items` (
	`SAMPLE_NO`,
	`SAMPLE_YEAR`,
	`REVISION`,
	`COMBO`,
	`PRINT`,
	`SIZE`,
	`COLOUR`,
	`TECHNIQUE`,
	`ITEM`,
	`WIDTH`,
	`HEIGHT`,
	`AUTOMATED_CONSUMPTION`,
	`EDITED_CONSUMPTION`,
	`LAST_MODIFIED_BY`,
	`MODIFIED_TIME`
)
VALUES
	(
		'$SampleNo',
		'$year',
		'$RevisionNo',
		'$Combo',
		'$Print',
		'$Size',
		'$colourName_rollForm',
		'$techniques_rollForm',
		'$itemCode_rollForm',
		'$width_rollForme',
		'$height_rollForm',
		$autoConsumption_rollForm,
		$edit_consumption,
		'$user',
		NOW()
	)";

                $result[] = $db->RunQuery2($roolFrom_sql);

                if($result[$i] != true){
                    $db->rollback();
                    $arr[]=array(
                        "flag"=>2,
                        "type"=>'failed to insert',
                    );
                    $result=json_encode($arr);
                    throw new Exception($db->errormsg);
                    // return $result ;
                }
                $i++;
            }

        }

        if ($nonRollFormMainTable){

            $sql = "DELETE
FROM
	`trn_sampleinfomations_grading_non_roll_items`
WHERE
	(`SAMPLE_NO` = '$SampleNo')
AND (`SAMPLE_YEAR` = '$year')
AND (`REVISION` = '$RevisionNo')
AND (`COMBO` = '$Combo')
AND (`PRINT` = '$Print')
AND (`SIZE` ='$Size')";

            $delete = $db->RunQuery2($sql);

            if($delete != true) {

                $db->rollback();
                $arr[]=array(
                    "flag"=>0,
                    "type"=>'failed to delete',
                );
                $result=json_encode($arr);
                throw new Exception($db->errormsg);
                //return $result ;
            }

            //$nonRollFrommCount = count($tableData['itemCode_nonRollFromm'])-1;
            $i=0;
            foreach ($nonRollFormMainTable as $tableData){


                $colourName_nonRollFrom = $tableData['colourName_nonRollFrom'];
                $techniques_nonRollFrom = $tableData['techniques_nonRollFrom'];
                $itemCode_nonRollFromm = $tableData['itemCode_nonRollFromm'];
                $defconsumption_nonRollFrom = $tableData['defconsumption_nonRollFrom'];
                $width_nonRollFrom = $tableData['width_nonRollFrom'];
                $height_nonRollFrom = $tableData['height_nonRollFrom'];
                $autoConsumption_nonRollFrom = $tableData['autoConsumption_nonRollFrom'];
                $edited_consumpition_nonRollFrom = $tableData['edited_consumpition_nonRollFrom'];
                $edit_consumption = !empty($edited_consumpition_nonRollFrom) ? "'$edited_consumpition_nonRollFrom'" : "NULL";

                if($edit_consumption=="NULL")
                    $edit_consumption = $autoConsumption_nonRollFrom;
                $user = $_SESSION['userId'];

                $nonRoolFrom_sql = "INSERT INTO `trn_sampleinfomations_grading_non_roll_items` (
	`SAMPLE_NO`,
	`SAMPLE_YEAR`,
	`REVISION`,
	`COMBO`,
	`PRINT`,
	`SIZE`,
	`COLOUR`,
	`TECHNIQUE`,
	`ITEM`,
	`WIDTH`,
	`HEIGHT`,
	`AUTOMATED_CONSUMPTION`,
	`EDITED_CONSUMPTION`,
	`LAST_MODIFIED_BY`,
	`MODIFIED_TIME`
)
VALUES
	(
		'$SampleNo',
		'$year',
		'$RevisionNo',
		'$Combo',
		'$Print',
		'$Size',
		'$colourName_nonRollFrom',
		'$techniques_nonRollFrom',
		'$itemCode_nonRollFromm',
		'$width_nonRollFrom',
		'$height_nonRollFrom',
		'$autoConsumption_nonRollFrom',
		  $edit_consumption,
		'$user',
		NOW()
	)";



                $result[] = $db->RunQuery2($nonRoolFrom_sql);

                if ($result[$i] != true) {
                    $db->rollback();
                    //var_dump('hit');
                    $arr[] = array(
                        "flag" => 3,
                        "type" => 'failed to insert',
                    );
                    $result = json_encode($arr);

                    throw new Exception($db->errormsg);
                    // return $result;
                }
                $i++;

            }

        }

        if ($nonDirectMainTable) {

            $sql = "DELETE
FROM
	`trn_sampleinfomations_grading_non_direct_items`
WHERE
	(`SAMPLE_NO` = '$SampleNo')
AND (`SAMPLE_YEAR` = '$year')
AND (`REVISION` = '$RevisionNo')
AND (`COMBO` = '$Combo')
AND (`PRINT` = '$Print')
AND (`SIZE` ='$Size')";

            $delete = $db->RunQuery2($sql);

            if ($delete != true) {

                $db->rollback();
                $arr[] = array(
                    "flag" => 0,
                    "type" => 'failed to delete',
                );
                $result = json_encode($arr);
                throw new Exception($db->errormsg);
                // return $result;
            }
            //$nonDirectCount = count($tableData['itemCode_nonDirect']) - 1;
            $i=0;
            foreach ($nonDirectMainTable as $tableData) {


                //$colourName_nonRollFrom = $tableData['colourName_nonRollFrom'][$i];
                // $techniques_nonRollFrom = $tableData['techniques_nonRollFrom'][$i];
                $itemCode_nonDirect = $tableData['itemCode_nonDirect'];
                $defconsumption_nonDirect = $tableData['defconsumption_nonDirect'];
                $width_nonDirect = $tableData['width_nonDirect'];
                $height_nonDirect = $tableData['height_nonDirect'];
                $autoConsumption_nonDirect = $tableData['autoConsumption_nonDirect'];
                $edited_consumpition_nonDirect = $tableData['edited_consumpition_nonDirect'];
                $edit_consumption = !empty($edited_consumpition_nonDirect) ? "'$edited_consumpition_nonDirect'" : "NULL";
                $user = $_SESSION['userId'];

                if($edit_consumption=="NULL")
                    $edit_consumption = $autoConsumption_nonDirect;

                $nonDirect_sql = "INSERT INTO `trn_sampleinfomations_grading_non_direct_items` (
	`SAMPLE_NO`,
	`SAMPLE_YEAR`,
	`REVISION`,
	`COMBO`,
	`PRINT`,
	`SIZE`,
	`ITEM`,
	`WIDTH`,
	`HEIGHT`,
	`AUTOMATED_CONSUMPTION`,
	`EDITED_CONSUMPTION`,
	`LAST_MODIFIED_BY`,
	`MODIFIED_TIME`
)
VALUES
	(
	    '$SampleNo',
		'$year',
		'$RevisionNo',
		'$Combo',
		'$Print',
		'$Size',
		'$itemCode_nonDirect',
		'$width_nonDirect',
		'$height_nonDirect',
		'$autoConsumption_nonDirect',
		  $edit_consumption,
		'$user',
		NOW()
	)";

                $result[] = $db->RunQuery2($nonDirect_sql);

                if ($result[$i] != true) {
                    $db->rollback();
                    $arr[] = array(
                        "flag" => 4,
                        "type" => 'failed to insert',
                    );
                    $result = json_encode($arr);
                    throw new Exception($db->errormsg);
                    //return $result;
                }

                $i++;
            }

        }

    }
    catch (Exception $e){
        $db->rollback();
        $result['msg'] 	=  $e->getMessage();
        $result['type'] 	=  'fail';
        json_encode($result);
    }
    return $result ;
}


function loadEditConsumption($table,$sampleNo,$sampleYear,$revision,$combo,$print,$size){
    global $db;
    $db->OpenConnection();
    if($size == ''){
        $sql="SELECT
trn_sampleinfomations_grading_non_direct_items.EDITED_CONSUMPTION

FROM
	trn_sampleinfomations_grading_non_direct_items
INNER JOIN trn_sampleinfomations_gradings ON trn_sampleinfomations_grading_non_direct_items.SAMPLE_NO = trn_sampleinfomations_gradings.SAMPLE_NO
AND trn_sampleinfomations_grading_non_direct_items.SAMPLE_YEAR = trn_sampleinfomations_gradings.SAMPLE_YEAR
AND trn_sampleinfomations_grading_non_direct_items.REVISION = trn_sampleinfomations_gradings.REVISION_NO
AND trn_sampleinfomations_grading_non_direct_items.COMBO = trn_sampleinfomations_gradings.COMBO
AND trn_sampleinfomations_grading_non_direct_items.PRINT = trn_sampleinfomations_gradings.PRINT
where 
trn_sampleinfomations_gradings.`DEFAULT`=1 AND
trn_sampleinfomations_grading_non_direct_items.SAMPLE_NO= '$sampleNo'
AND
trn_sampleinfomations_grading_non_direct_items.SAMPLE_YEAR= '$sampleYear'
AND
trn_sampleinfomations_grading_non_direct_items.REVISION ='$revision'
AND
trn_sampleinfomations_grading_non_direct_items.COMBO='$combo'
AND
trn_sampleinfomations_grading_non_direct_items.PRINT='$print'";

        $result = $db->RunQuery2($sql);
        $i=0;
        while($row =mysqli_fetch_array($result)){


            $EditedConsumption[] = array(
                "EDITED_CONSUMPTION"=>$row['EDITED_CONSUMPTION']
            );
            $i++;
        }


    }

    return $EditedConsumption;
}


 function DeleteRow($MainData)
{
        global $db;
        $sql = "	DELETE
	FROM
		`trn_sampleinfomations_gradings`
	WHERE
		(`SAMPLE_NO` = '" . $MainData['sampleNo'] . "')
	AND (`SAMPLE_YEAR` = '" . $MainData['year'] . "')
	AND (`REVISION_NO` = '" . $MainData['revision'] . "')
	AND (`COMBO` = '" . $MainData['combo'] . "')
	AND (`PRINT` = '" . $MainData['print'] . "')
	AND (`SIZE` = '" . $MainData['size'] . "')";

    $result = $db->RunQuery($sql);

    if($result == true){

        $arr = array(
            "status" => 'pass',
            "type" => 'Delte successfully'
        );
    }else{
        $arr = array(
            "status" => 'fail',
            "type" => 'fail insert data'
        );
    }

    return  json_encode($arr);

}
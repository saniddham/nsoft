<?php
$backwardseperator = "../../../../../";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sample Cost Sheet</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
<form id="frmCompanyDetails" name="frmCompanyDetails" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Sample Cost Sheet</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="14%" height="22" class="normalfnt">Style No</td>
            <td width="20%"><select name="select" id="select" style="width:140px">
              </select></td>
            <td colspan="2" class="normalfntMid"><strong>Graphic</strong></td>
            <td width="6%" class="normalfnt">&nbsp;</td>
            <td width="11%" class="normalfnt">Combo</td>
            <td width="18%" align="left"><select name="select2" id="select2" style="width:140px">
              </select></td>
            </tr>
          <tr>
            <td height="19" class="normalfnt">Graphic Placement</td>
            <td><input type="text" name="textfield" id="textfield" style="width:140px" /></td>
            <td colspan="2" rowspan="5" align="center" class="tableBorder_allRound"><img src="../../../../../images/sample_garment.png" width="100" height="92" /></td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Technique</td>
            <td align="left"><select name="select3" id="select3" style="width:140px">
              </select></td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">Graphic Size <span class="normalfntGrey">(Inch)</span></td>
            <td><table width="100%" border="0">
              <tr class="normalfnt">
                <td width="19%" align="center">W</td>
                <td width="25%"><input type="text" name="textfield2" id="textfield2" style="width:30px" /></td>
                <td width="15%" align="center">H</td>
                <td width="41%"><input type="text" name="textfield3" id="textfield3" style="width:30px" /></td>
                </tr>
              </table></td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Print process</td>
            <td align="left"><select name="select4" id="select4" style="width:140px">
              </select></td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">Wash conditions</td>
            <td><select name="select5" id="select5" style="width:140px">
              </select></td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Fabric</td>
            <td align="left"><input type="text" name="textfield4" id="textfield4" style="width:140px" /></td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">Qty</td>
            <td><input type="text" name="textfield5" id="textfield5" style="width:30px" /></td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">No of Colors</td>
            <td align="left"><input type="text" name="textfield10" id="textfield10" style="width:30px" /></td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">Usage %</td>
            <td><input type="text" name="textfield8" id="textfield8" style="width:30px" /></td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td align="left">&nbsp;</td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2" align="center">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td align="right"><img src="../../../../../images/Tadd.jpg" width="92" height="24" /></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="grid" >
            <tr class="gridHeader">
              <td width="4%" height="22" >Del</td>
              <td width="12%" >Main Category</td>
              <td width="11%" >Sub Category</td>
              <td width="25%" >Item Description</td>
              <td width="8%">UOM</td>
              <td width="10%">Cost Price(per Inch)</td>
              <td width="10%">Usage %</td>
              <td width="10%">Total Cost Price</td>
              </tr>
            <tr class="normalfnt">
              <td align="center" bgcolor="#FFFFFF"><img src="../../../../../images/del.png" width="15" height="15" /></td>
              <td bgcolor="#FFFFFF">&nbsp;</td>
              <td bgcolor="#FFFFFF">&nbsp;</td>
              <td bgcolor="#FFFFFF">&nbsp;</td>
              <td bgcolor="#FFFFFF">&nbsp;</td>
              <td bgcolor="#FFFFFF"><input type="text" name="textfield6" id="textfield6" style="width:90px" /></td>
              <td bgcolor="#FFFFFF"><input type="text" name="textfield9" id="textfield9" style="width:90px" /></td>
              <td bgcolor="#FFFFFF">&nbsp;</td>
              </tr>
            </table>
          </div></td>
      </tr>
      <tr>
        <td align="center" class=""><table width="100%" border="0">
          <tr>
            <td class="normalfnt"><table width="31%" border="0" align="center" class="grid">
              <tr>
                <td colspan="4" bgcolor="#F4EDB9" class="normalfntMid"><strong>Strockes</strong></td>
                </tr>
              <tr class="gridHeader">
                <td width="35%" bgcolor="#FFFFFF" class="gridHeader">Type</td>
                <td width="18%" bgcolor="#FFFFFF" class="gridHeader">No of Strockes</td>
                <td width="24%" bgcolor="#FFFFFF" class="gridHeader">Strocks Cost</td>
                <td width="23%" bgcolor="#FFFFFF" class="gridHeader">Total </td>
                </tr>
              <tr>
                <td bgcolor="#FFFFFF">Machine</td>
                <td bgcolor="#FFFFFF"><input type="text" name="textfield12" id="textfield12" style="width:50px" /></td>
                <td bgcolor="#FFFFFF"><input type="text" name="textfield13" id="textfield13" style="width:90px" /></td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                </tr>
              <tr>
                <td bgcolor="#FFFFFF">Manual</td>
                <td bgcolor="#FFFFFF"><input type="text" name="textfield11" id="textfield11" style="width:50px" /></td>
                <td bgcolor="#FFFFFF"><input type="text" name="textfield14" id="textfield14" style="width:90px" /></td>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                </tr>
            </table></td>
            <td colspan="4" valign="top" class="normalfnt"><table cellpadding="0" cellspacing="0" class="grid">
              <col width="94" />
                <col width="64" span="3" />
                <col width="100" />
                <col width="101" />
                <col width="84" />
                <col width="79" />
                <tr class="">
                  <td height="17" colspan="8" bgcolor="#F4EDB9" class="normalfntMid"><strong>Technique</strong></td>
                  </tr>
                <tr class="gridHeader">
                  <td width="92" height="19">&nbsp;</td>
                  <td width="63">Width</td>
                  <td width="63">Hight</td>
                  <td width="62">Qty</td>
                  <td width="98">Pcs frm Hight</td>
                  <td width="100">pcs(round)</td>
                  <td width="89">Mtr</td>
                  <td width="73">Cost</td>
                </tr>
                <tr>
                  <td height="21" bgcolor="#FFFFFF"><select name="select6" id="select6" style="width:100px">
                    <option>Foil 29</option>
                    <option>Foil 24</option>
                    <option>Hot Fix</option>
                    <option>Heat Seal</option>
                    <option>Flock</option>
                  </select></td>
                  <td bgcolor="#FFFFFF"><input type="text" name="textfield15" id="textfield15" style="width:50px" /></td>
                  <td bgcolor="#FFFFFF"><input type="text" name="textfield16" id="textfield16" style="width:50px" /></td>
                  <td bgcolor="#FFFFFF">1</td>
                  <td align="right" bgcolor="#FFFFFF">5.80</td>
                  <td bgcolor="#FFFFFF">5</td>
                  <td bgcolor="#FFFFFF">0.015</td>
                  <td bgcolor="#FFFFFF">0.013</td>
                </tr>
                <tr>
                  <td height="23" bgcolor="#FFFFFF">&nbsp;</td>
                  <td bgcolor="#FFFFFF">&nbsp;</td>
                  <td bgcolor="#FFFFFF">&nbsp;</td>
                  <td bgcolor="#FFFFFF">&nbsp;</td>
                  <td align="right" bgcolor="#FFFFFF">9.67</td>
                  <td bgcolor="#FFFFFF">9</td>
                  <td bgcolor="#FFFFFF">0.014</td>
                  <td bgcolor="#FFFFFF">0.012</td>
                </tr>
                </table></td>
            </tr>
          <tr>
            <td width="28%" class="normalfnt">&nbsp;</td>
            <td width="19%">&nbsp;</td>
            <td width="22%">&nbsp;</td>
            <td width="18%" class="normalfnt">RM Cost</td>
            <td width="13%" class="tableBorder_allRound"><input type="text" name="textfield18" id="textfield18" style="width:140px" /></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">Foil Cost</td>
            <td class="tableBorder_allRound"><input type="text" name="textfield19" id="textfield19" style="width:140px" /></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">Services Cost</td>
            <td class="tableBorder_allRound"><input type="text" name="textfield20" id="textfield20" style="width:140px" /></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">Strock Cost</td>
            <td class="tableBorder_allRound"><input type="text" name="textfield21" id="textfield21" style="width:140px" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td bgcolor="#F4EDB9" class="normalfnt">Total Cost</td>
            <td class="tableBorder_allRound"><input type="text" name="textfield22" id="textfield22" style="width:140px" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">Margin %
              <input type="text" name="textfield17" id="textfield17" value="50" style="width:50px" /></td>
            <td class="tableBorder_allRound"><input type="text" name="textfield23" id="textfield23" style="width:140px" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td bgcolor="#FAD163" class="normalfnt">Screenline    Price</td>
            <td class="tableBorder_allRound"><input type="text" name="textfield24" id="textfield24" style="width:140px" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">Target Price</td>
            <td class="tableBorder_allRound"><input type="text" name="textfield25" id="textfield25" style="width:140px" /></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td bgcolor="#F7D2BB" class="tableBorder_allRound">Approved Price</td>
            <td class="tableBorder_allRound"><input type="text" name="textfield26" id="textfield26" style="width:140px" /></td>
          </tr>
          </table></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><img src="../../../../../images/Tnew.jpg" width="92" height="24" /><img src="../../../../../images/Tsave.jpg" width="92" height="24" /><img src="../../../../../images/Tconfirm.jpg" width="92" height="24" /><img src="../../../../../images/Treport.jpg" width="92" height="24" /><img src="../../../../../planning/img/Tclose.jpg" width="92" height="24" /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>

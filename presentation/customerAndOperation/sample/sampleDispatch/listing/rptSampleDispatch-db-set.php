<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$userId = $_SESSION['userId'];
$companyId = $_SESSION['CompanyID'];
$SRdb = $_SESSION["SRDatabese"];

$requestType = $_REQUEST['requestType'];
$programName = 'Sample Dispatch';
$programCode = 'P0350';

include_once "{$backwardseperator}dataAccess/Connector.php";
include_once "../../../../../class/cls_commonFunctions_get.php";
include_once "../../../../../class/cls_commonErrorHandeling_get.php";
include "../../../../../libraries/mail/mail.php";

$obj_common = new cls_commonFunctions_get($db);
$obj_errorHandeling = new cls_commonErrorHandeling_get($db);

$savedStatus = true;
$finalApprove = false;
$savedMasseged = '';
$error_sql = '';

if ($requestType == 'approve') {
    $dispatchNo = $_REQUEST["dispatchNo"];
    $dispatchYear = $_REQUEST["dispatchYear"];

    $db->begin();

    $validateArr = validateBeforeApprove($dispatchNo, $dispatchYear, $programCode, $userId);
    if ($validateArr['type'] == 'fail' && $savedStatus) {
        $savedStatus = false;
        $savedMasseged = $validateArr["msg"];
    }
    $resultUHSArr = updateHeaderStatus($dispatchNo, $dispatchYear, '');
    if (isset($resultUHSArr['savedStatus']) && $resultUHSArr['savedStatus'] == 'fail' && $savedStatus) {
        $savedStatus = false;
        $savedMasseged = $resultUHSArr["savedMassege"];
        $error_sql = $resultUHSArr['error_sql'];
    }
    $chekQtyArr = checkQtyBeforeFinalApproval($dispatchNo, $dispatchYear);
    if (!empty($chekQtyArr) && $chekQtyArr['type'] == 'fail' && $savedStatus) {
        $savedStatus = false;
        $savedMasseged = $chekQtyArr["msg"];
    }
    $qtyUpdArr = sampleDispatchBalQtyUpdate($dispatchNo, $dispatchYear);
    if (isset($qtyUpdArr['savedStatus']) && $qtyUpdArr['savedStatus'] == 'fail' && $savedStatus) {
        $savedStatus = false;
        $savedMasseged = $qtyUpdArr["savedMassege"];
        $error_sql = $qtyUpdArr['error_sql'];
    }
    $qtyInsArr = sampleDispatchEventlog($dispatchNo, $dispatchYear);
    if (isset($qtyUpdArr['savedStatus']) && $qtyUpdArr['savedStatus'] == 'fail' && $savedStatus) {
        $savedStatus = false;
        $savedMasseged = $qtyUpdArr["savedMassege"];
        $error_sql = $qtyUpdArr['error_sql'];
    }
    $resultAPArr = approvedData($dispatchNo, $dispatchYear, $userId);
    if (isset($resultAPArr['savedStatus']) && $resultAPArr['savedStatus'] == 'fail' && $savedStatus) {
        $savedStatus = false;
        $savedMasseged = $resultAPArr["savedMassege"];
        $error_sql = $resultAPArr['error_sql'];
    }
    if ($savedStatus) {
        if($sampleDetails != null){
        $no = $sampleDetails['intSampleNo'];
        $year = $sampleDetails['intSampleYear'];
        $revisionNo = $sampleDetails['intRevNo'];
        $status = $sampleDetails['intStatus'];
        include "dispatchToMarketingEmail.php";
    }
        $db->commit();
        $response['type'] = "pass";
        if (isset($finalApproval))
            $response['msg'] = "Final Approval Raised Successfully.";
        else
            $response['msg'] = "Approved Successfully.";
    } else {
        $db->rollback();
        $response['type'] = "fail";
        $response['msg'] = $savedMasseged;
        $response['sql'] = $error_sql;
    }

    echo json_encode($response);
} else if ($requestType == 'reject') {
    $dispatchNo = $_REQUEST["dispatchNo"];
    $dispatchYear = $_REQUEST["dispatchYear"];

    $db->begin();

    $validateArr = validateBeforeReject($dispatchNo, $dispatchYear, $programCode, $userId);
    if ($validateArr['type'] == 'fail' && $savedStatus) {
        $savedStatus = false;
        $savedMasseged = $validateArr["msg"];
    }
    $resultUHSArr = updateHeaderStatus($dispatchNo, $dispatchYear, '0');
    if ($resultUHSArr['savedStatus'] == 'fail' && $savedStatus) {
        $savedStatus = false;
        $savedMasseged = $resultUHSArr["savedMassege"];
        $error_sql = $resultUHSArr['error_sql'];
    }
    $resultAPArr = approved_by_insert($dispatchNo, $dispatchYear, $userId, 0);
    if ($resultAPArr['savedStatus'] == 'fail' && $savedStatus) {
        $savedStatus = false;
        $savedMasseged = $resultAPArr["savedMassege"];
        $error_sql = $resultAPArr['error_sql'];
    }
    if ($savedStatus) {
        $db->commit();
        $response['type'] = "pass";
        $response['msg'] = "Rejected Successfully.";
    } else {
        $db->rollback();
        $response['type'] = "fail";
        $response['msg'] = $savedMasseged;
        $response['sql'] = $error_sql;
    }
    echo json_encode($response);
}
function validateBeforeApprove($dispatchNo, $dispatchYear, $programCode, $userId)
{
    global $obj_errorHandeling;

    $header_arr = loadHeaderData($dispatchNo, $dispatchYear, 'RunQuery2');
    $validateArr = $obj_errorHandeling->get_permision_withApproval_confirm($header_arr['intStatus'], $header_arr['intApproveLevelStart'], $userId, $programCode, 'RunQuery2');

    return $validateArr;
}

function loadHeaderData($dispatchNo, $dispatchYear, $executionType)
{
    global $db;

    $sql = "SELECT intDispatchTo, 
			intCustLocation, 
			strAttentionTo, 
			intSavedBy, 
			dtSaved, 
			intConfirmedBy, 
			dtConfirmed, 
			intCompanyId, 
			intStatus, 
			intApproveLevelStart
			FROM 
			trn_sampledispatchheader 
			WHERE intDispatchNo = '$dispatchNo' AND
			intDispatchYear = '$dispatchYear' ";

    $result = $db->$executionType($sql);

    $row = mysqli_fetch_array($result);

    return $row;
}

function loadSampleData($dispatchNo, $dispatchYear, $executionType)
{
    global $db;

    $sql = "SELECT intSampleNo, 
			intSampleYear, 
			intRevisionNo, intStatus
			FROM 
			trn_sampledispatchdetails INNER JOIN trn_sampledispatchheader ON trn_sampledispatchdetails.intDispatchNo = trn_sampledispatchheader.intDispatchNo AND trn_sampledispatchdetails.intDispatchYear = trn_sampledispatchheader.intDispatchYear 
			WHERE trn_sampledispatchdetails.intDispatchNo = '$dispatchNo' AND
			trn_sampledispatchdetails.intDispatchYear = '$dispatchYear' ";

    $result = $db->$executionType($sql);
    $row = mysqli_fetch_array($result);
    echo $row;

    return $row;
}

function updateHeaderStatus($dispatchNo, $dispatchYear, $status)
{
    global $db;
    $data = array();

    if ($status == '')
        $para = 'intStatus-1';
    else
        $para = $status;

    $sql = "UPDATE trn_sampledispatchheader 
			SET
			intStatus = $para
			WHERE
			intDispatchNo = '$dispatchNo' AND 
			intDispatchYear = '$dispatchYear' ";

    $result = $db->RunQuery2($sql);
    if (!$result) {
        $data['savedStatus'] = 'fail';
        $data['savedMassege'] = $db->errormsg;
        $data['error_sql'] = $sql;
    }
    return $data;
}

function checkQtyBeforeFinalApproval($dispatchNo, $dispatchYear)
{
    global $db;
    $data = array();
    $checkCancelled = false;
    $checkQty = false;
    $checkFabInQty = false;

    $header_arr = loadHeaderData($dispatchNo, $dispatchYear, 'RunQuery2');
    if ($header_arr['intStatus'] == 1) {
        $sql = "SELECT DISTINCT SDD.SAMPLE_ORDER_NO,SDD.SAMPLE_ORDER_YEAR,SOH.intStatus
				FROM trn_sampledispatchdetails SDD
				INNER JOIN trn_sampleorderheader SOH ON SOH.intSampleOrderNo=SDD.SAMPLE_ORDER_NO AND
				SOH.intSampleOrderYear=SDD.SAMPLE_ORDER_YEAR
				WHERE intDispatchNo = '$dispatchNo' AND
				intDispatchYear = '$dispatchYear' ";

        $result = $db->RunQuery2($sql);
        while ($row = mysqli_fetch_array($result)) {
            if ($row['intStatus'] != 1) {
                $checkCancelled = true;
            }
        }
        if ($checkCancelled) {
            $data['type'] = 'fail';
            $data['msg'] = 'Can not Approved. Sample Order cancelled ';
        } else {
            $sql = "SELECT SAMPLE_ORDER_NO, 
					SAMPLE_ORDER_YEAR, 
					strCombo, 
					intPartId, 
					intGrade, 
					intSampleTypeId, 
					strSize, 
					DATE(dtDeliveryDate) as deliveryDate, 
					dblDispatchQty 
					FROM 
					trn_sampledispatchdetails 
					WHERE intDispatchNo = '$dispatchNo' AND
					intDispatchYear = '$dispatchYear' ";

            $result = $db->RunQuery2($sql);
            while ($row = mysqli_fetch_array($result)) {
                $sqlBal = " SELECT intBalToDeliverQty
							FROM trn_sampleorderdetails 
							WHERE
							intSampleOrderNo = '" . $row['SAMPLE_ORDER_NO'] . "' AND 
							intSampleOrderYear = '" . $row['SAMPLE_ORDER_YEAR'] . "' AND 
							strCombo = '" . $row['strCombo'] . "' AND 
							intPart = '" . $row['intPartId'] . "' AND 
							intGrade = '" . $row['intGrade'] . "' AND 
							strSize = '" . $row['strSize'] . "' AND 
							intSampleTypeId = '" . $row['intSampleTypeId'] . "' AND 
							DATE(dtRequiredDate) = '" . $row['deliveryDate'] . "' ";

                $resultBal = $db->RunQuery2($sqlBal);
                $rowBal = mysqli_fetch_array($resultBal);

                if ($row['dblDispatchQty'] > $rowBal['intBalToDeliverQty']) {
                    $checkQty = true;
                }
            }
            if ($checkQty) {
                $data['type'] = 'fail';
                $data['msg'] = 'Some Qty exceed Sample Dispatch bal qty. ';
            }
        }
    }
    return $data;
}

function sampleDispatchBalQtyUpdate($dispatchNo, $dispatchYear)
{
    global $db;
    global $SRdb;

    $header_arr = loadHeaderData($dispatchNo, $dispatchYear, 'RunQuery2');
    if ($header_arr['intStatus'] == 1) {
        $data = array();
        $sql = "SELECT SDD.SAMPLE_ORDER_NO, 
				SDD.SAMPLE_ORDER_YEAR, 
				SDD.strCombo, 
				SDD.intPartId, 
				SDD.intGrade, 
				SDD.intSampleTypeId, 
				SDD.strSize, 
				DATE(SDD.dtDeliveryDate) as deliveryDate, 
				SDD.dblDispatchQty,
				SOH.intSampleReqNo,
				SOH.intSampleReqYear
				FROM 
				trn_sampledispatchdetails SDD
				INNER JOIN trn_sampleorderheader SOH ON SOH.intSampleOrderNo=SDD.SAMPLE_ORDER_NO AND
				SOH.intSampleOrderYear=SDD.SAMPLE_ORDER_YEAR
				WHERE SDD.intDispatchNo = '$dispatchNo' AND
				SDD.intDispatchYear = '$dispatchYear' AND
				SOH.intStatus = 1 ";

        $result = $db->RunQuery2($sql);

        while ($row = mysqli_fetch_array($result)) {
            $dispatchedQty = $row['dblDispatchQty'];

            $sqlUpd = " UPDATE trn_sampleorderdetails 
						SET
						intBalToDeliverQty = intBalToDeliverQty - $dispatchedQty
						WHERE
						intSampleOrderNo = '" . $row['SAMPLE_ORDER_NO'] . "' AND 
						intSampleOrderYear = '" . $row['SAMPLE_ORDER_YEAR'] . "' AND 
						strCombo = '" . $row['strCombo'] . "' AND 
						intPart = '" . $row['intPartId'] . "' AND 
						intGrade = '" . $row['intGrade'] . "' AND 
						strSize = '" . $row['strSize'] . "' AND 
						intSampleTypeId = '" . $row['intSampleTypeId'] . "' AND 
						DATE(dtRequiredDate) = '" . $row['deliveryDate'] . "' ";

            $resultUpd = $db->RunQuery2($sqlUpd);
            if (!$resultUpd) {
                $data['savedStatus'] = 'fail';
                $data['savedMassege'] = $db->errormsg;
                $data['error_sql'] = $sqlUpd;
            }

            /*COMMENTED BY HEMANTHI 2014-09-26 (ROSHAN'S REQUISITION)
                $sqlUpdReq = "UPDATE $SRdb.trn_sample_requisition_detail
                                SET
                                DISPATCH_QTY = DISPATCH_QTY + $dispatchedQty
                                WHERE
                                REQUISITION_NO = '".$row['intSampleReqNo']."' AND
                                REQUISITION_YEAR = '".$row['intSampleReqYear']."' AND
                                SAMPLE_TYPE = '".$row['intSampleTypeId']."' ";

                $resultUpdReq  = $db->RunQuery2($sqlUpdReq);
                $resultUpdReq2 = $db->RunQuery_S($sqlUpdReq);
                if(!$resultUpdReq)
                {
                    $data['savedStatus']	= 'fail';
                    $data['savedMassege']	= $db->errormsg;
                    $data['error_sql']		= $sqlUpdReq;
                }
                if(!$resultUpdReq2)
                {
                    $data['savedStatus']	= 'fail';
                    $data['savedMassege']	= $db->errormsg;
                    $data['error_sql']		= $sqlUpdReq;
                }*/
        }

        return $data;
    }
}

function approvedData($dispatchNo, $dispatchYear, $userId)
{
    global $finalApprove;

    $header_arr = loadHeaderData($dispatchNo, $dispatchYear, 'RunQuery2');
    if ($header_arr['intStatus'] == 1)
        $finalApprove = true;

    $approval = $header_arr['intApproveLevelStart'] + 1 - $header_arr['intStatus'];
    $resultArr = approved_by_insert($dispatchNo, $dispatchYear, $userId, $approval);

    return $resultArr;
}

function approved_by_insert($dispatchNo, $dispatchYear, $userId, $approval)
{
    global $db;
    $data = array();

    $sql = "INSERT INTO trn_sampledispatchheader_approvedby 
				(
				intDispatchNo, 
				intDispatchYear, 
				intApproveLevelNo, 
				intApproveUser, 
				dtApprovedDate, 
				intStatus
				)
				VALUES
				(
				'$dispatchNo', 
				'$dispatchYear', 
				'$approval', 
				'$userId', 
				NOW(), 
				'0'
				) ";

    $result = $db->RunQuery2($sql);
    if (!$result) {
        $data['savedStatus'] = 'fail';
        $data['savedMassege'] = $db->errormsg;
        $data['error_sql'] = $sql;
    }
    return $data;
}

function validateBeforeReject($dispatchNo, $dispatchYear, $programCode, $userId)
{
    global $obj_errorHandeling;

    $header_arr = loadHeaderData($dispatchNo, $dispatchYear, 'RunQuery2');
    $validateArr = $obj_errorHandeling->get_permision_withApproval_reject($header_arr['intStatus'], $header_arr['intApproveLevelStart'], $userId, $programCode, 'RunQuery2');

    return $validateArr;
}

function sampleDispatchEventlog($dispatchNo, $dispatchYear)
{
    global $db;
    global $SRdb;
    $data = array();

    $header_arr = loadHeaderData($dispatchNo, $dispatchYear, 'RunQuery2');
    if ($header_arr['intStatus'] == 1) {
        $sql = "SELECT SRH.GRAPHIC,
				SRD.SAMPLE_TYPE,
				SDH.dtSaved AS transacDate,
				SRH.CREATED_BY AS createdBy,
				SRH.MODIFY_BY AS modifyBy,
				SDD.dblDispatchQty AS Qty,
				'DISPATCHED' AS transacType
				FROM trn_sampledispatchheader SDH
				INNER JOIN trn_sampledispatchdetails SDD ON SDH.intDispatchNo=SDD.intDispatchNo AND
				SDH.intDispatchYear=SDD.intDispatchYear
				INNER JOIN trn_sampleorderdetails SOD ON SOD.intSampleOrderNo=SDD.SAMPLE_ORDER_NO AND
				SOD.intSampleOrderYear=SDD.SAMPLE_ORDER_YEAR AND
				SOD.strCombo=SDD.strCombo AND
				SOD.intPart=SDD.intPartId AND
				SOD.intGrade=SDD.intGrade AND
				SOD.strSize=SDD.strSize AND
				SOD.intSampleTypeId=SDD.intSampleTypeId AND
				SOD.dtRequiredDate=SDD.dtDeliveryDate
				INNER JOIN trn_sampleorderheader SOH ON SOH.intSampleOrderNo=SOD.intSampleOrderNo AND
				SOH.intSampleOrderYear=SOD.intSampleOrderYear
				INNER JOIN $SRdb.trn_sample_requisition_header SRH ON SRH.REQUISITION_NO=SOH.intSampleReqNo AND
				SRH.REQUISITION_YEAR=SOH.intSampleReqYear
				INNER JOIN $SRdb.trn_sample_requisition_detail SRD ON SRH.REQUISITION_NO=SRD.REQUISITION_NO AND
				SRH.REQUISITION_YEAR=SRD.REQUISITION_YEAR AND
				SOD.intSampleTypeId=SRD.SAMPLE_TYPE
				WHERE
				SDH.intStatus = 1 AND
				SDH.intDispatchNo = '$dispatchNo' AND
				SDH.intDispatchYear = '$dispatchYear' AND
				SRD.DISPATCH_QTY > 0 AND
				SRD.QTY > 0
				GROUP BY SRD.SAMPLE_TYPE,SDH.dtSaved
				ORDER BY SRD.SAMPLE_TYPE
				LIMIT 0,2 ";


        $result = $db->RunQuery2($sql);

        while ($result && $row = mysqli_fetch_array($result)) {
            $sqlInsLog = " INSERT INTO $SRdb.dashboard_event_log 
							( 
							GRAPHIC_NO, 
							SAMPLE_TYPE_ID, 
							CREATED_BY, 
							MODIFY_BY, 
							TYPE, 
							LOG_DATE_TIME
							)
							VALUES
							(
							'" . $row['GRAPHIC'] . "', 
							'" . $row['SAMPLE_TYPE'] . "', 
							" . ($row['createdBy'] == '' ? 'null' : $row['createdBy']) . ", 
							" . ($row['modifyBy'] == '' ? 'null' : $row['modifyBy']) . ", 
							'" . $row['transacType'] . "',  
							'" . $row['transacDate'] . "'
							);";

            $resultInsLog = $db->RunQuery2($sqlInsLog);
            $resultInsLog2 = $db->RunQuery_S($sqlInsLog);
            if (!$resultInsLog) {
                $data['savedStatus'] = 'fail';
                $data['savedMassege'] = $db->errormsg;
                $data['error_sql'] = $sqlInsLog;
            }
            if (!$resultInsLog2) {
                $data['savedStatus'] = 'fail';
                $data['savedMassege'] = $db->errormsg;
                $data['error_sql'] = $sqlInsLog;
            }
        }
    }
    return $data;
}

function sendSampleDispatchMail($dispatchNo, $dispatchYear)
{
    global $db;

    $header_arr = loadSampleData($dispatchNo, $dispatchYear, 'RunQuery2');
    $no = $header_arr['intSampleNo'];
    $year = $header_arr['intSampleYear'];
    $revisionNo = $header_arr['intRevisionNo'];
    $status = $header_arr['intStatus'];

    $sql = "SELECT * 			
			FROM trn_sampleinfomations_details
			INNER JOIN mst_techniques ON trn_sampleinfomations_details.intTechniqueId = mst_techniques.intId			
			WHERE
			trn_sampleinfomations_details.intSampleNo = '$no' AND
			trn_sampleinfomations_details.intSampleYear = '$year' AND
			trn_sampleinfomations_details.intRevNo = '$revisionNo' AND
			mst_techniques.heatTransfer = '1' ";


    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    if ($status == 1 ) {
        return $row;
    }


}

?>
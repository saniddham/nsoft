<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";

$location 	= $sessions->getLocationId();
$company 	= $sessions->getCompanyId();
$intUser  	= $sessions->getUserId();

$programCode	= 'P0350';

$reportMenuId	= '982';
$menuId			= '350';


$approveLevel = (int)getMaxApproveLevel();

################################################################
################## new part ####################################
################################################################
///// create where part ///////////////////
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'Status'=>'tb1.intStatus',
				'Dispatch_No'=>'tb1.intDispatchNo',
				'Dispatch_Year'=>'tb1.intDispatchYear',
				'Date'=>'tb1.dtSaved',
				'User'=>'sys_users.strUserName',
				'Customer'=>'mst_customer.strName'
				);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}

if(!count($arr)>0)					 
	$where_string .= "AND substr(tb1.dtSaved,1,10) = '".date('Y-m-d')."'";
	
################## end code ####################################

$sql = "select * from(SELECT DISTINCT 
							if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected','Pending')) as Status,
							tb1.intDispatchNo as `Dispatch_No`,
							tb1.intDispatchYear as `Dispatch_Year`,
							(SELECT
group_concat(distinct trn_sampledispatchdetails.intSampleNo,'/',trn_sampledispatchdetails.intSampleYear) as sample 
FROM
trn_sampledispatchdetails
where trn_sampledispatchdetails.intDispatchNo = tb1.intDispatchNo AND trn_sampledispatchdetails.intDispatchYear = tb1.intDispatchYear
) as sampleNo,
							(SELECT
group_concat(distinct trn_sampledispatchdetails.SAMPLE_ORDER_NO,'/',trn_sampledispatchdetails.SAMPLE_ORDER_YEAR) as sample 
FROM
trn_sampledispatchdetails
where trn_sampledispatchdetails.intDispatchNo = tb1.intDispatchNo AND trn_sampledispatchdetails.intDispatchYear = tb1.intDispatchYear
) as sampleOrderNo,
							(SELECT
group_concat(distinct sys_users.strFullName) as mar 
FROM
trn_sampledispatchdetails
INNER JOIN trn_sampleinfomations ON trn_sampledispatchdetails.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_sampledispatchdetails.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_sampledispatchdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
INNER JOIN sys_users ON trn_sampleinfomations.intMarketer = sys_users.intUserId 
where trn_sampledispatchdetails.intDispatchNo = tb1.intDispatchNo AND trn_sampledispatchdetails.intDispatchYear = tb1.intDispatchYear
) as marketer,
(SELECT
group_concat(distinct mst_brand.strName) AS brand  
FROM
trn_sampledispatchdetails
INNER JOIN trn_sampleinfomations ON trn_sampledispatchdetails.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_sampledispatchdetails.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_sampledispatchdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId 
where trn_sampledispatchdetails.intDispatchNo = tb1.intDispatchNo AND trn_sampledispatchdetails.intDispatchYear = tb1.intDispatchYear
) as brand,
(SELECT
group_concat(distinct mst_sample_category.`NAME`) AS type 
FROM
trn_sampledispatchdetails
INNER JOIN trn_sampleinfomations ON trn_sampledispatchdetails.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_sampledispatchdetails.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_sampledispatchdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
INNER JOIN trn_sampleorderheader ON trn_sampleinfomations.intSampleNo = trn_sampleorderheader.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleorderheader.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_sampleorderheader.intRevNo AND trn_sampledispatchdetails.SAMPLE_ORDER_NO = trn_sampleorderheader.intSampleOrderNo AND trn_sampledispatchdetails.SAMPLE_ORDER_YEAR = trn_sampleorderheader.intSampleOrderYear
INNER JOIN mst_sample_category ON trn_sampleorderheader.`SAMPLE CATEGORY` = mst_sample_category.ID
where trn_sampledispatchdetails.intDispatchNo = tb1.intDispatchNo AND trn_sampledispatchdetails.intDispatchYear = tb1.intDispatchYear
) as sampleType,
(SELECT
Sum(trn_sampledispatchdetails.dblDispatchQty) AS dispQty
FROM `trn_sampledispatchdetails`
WHERE
trn_sampledispatchdetails.intDispatchNo = tb1.intDispatchNo AND
trn_sampledispatchdetails.intDispatchYear = tb1.intDispatchYear) as dispatched_qty,
(SELECT
group_concat(distinct trn_sampleinfomations.strGraphicRefNo) AS graphic
FROM `trn_sampledispatchdetails`
INNER JOIN trn_sampleinfomations ON trn_sampledispatchdetails.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_sampledispatchdetails.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_sampledispatchdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
WHERE
trn_sampledispatchdetails.intDispatchNo = tb1.intDispatchNo AND
trn_sampledispatchdetails.intDispatchYear = tb1.intDispatchYear) as graphic,
	
							tb1.dtSaved as `Date`,
							sys_users.strUserName as User,
							mst_customer.strName as Customer,
							tb1.intApproveLevelStart,
							tb1.intStatus,
							IFNULL((
                                      SELECT
								concat(sys_users.strUserName,'(',max(trn_sampledispatchheader_approvedby.dtApprovedDate),')' )
								FROM
								trn_sampledispatchheader_approvedby
								Inner Join sys_users ON trn_sampledispatchheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								trn_sampledispatchheader_approvedby.intDispatchNo  = tb1.intDispatchNo AND
								trn_sampledispatchheader_approvedby.intDispatchYear =  tb1.intDispatchYear AND
								trn_sampledispatchheader_approvedby.intApproveLevelNo =  '1'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.intStatus>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(trn_sampledispatchheader_approvedby.dtApprovedDate),')' )
								FROM
								trn_sampledispatchheader_approvedby
								Inner Join sys_users ON trn_sampledispatchheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								trn_sampledispatchheader_approvedby.intDispatchNo  = tb1.intDispatchNo AND
								trn_sampledispatchheader_approvedby.intDispatchYear =  tb1.intDispatchYear AND
								trn_sampledispatchheader_approvedby.intApproveLevelNo =  '$i'
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.intStatus>1) AND (tb1.intStatus<=(tb1.intApproveLevelStart)) AND ((SELECT
								max(trn_sampledispatchheader_approvedby.dtApprovedDate)
								FROM
								trn_sampledispatchheader_approvedby
								Inner Join sys_users ON trn_sampledispatchheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								trn_sampledispatchheader_approvedby.intDispatchNo  = tb1.intDispatchNo AND
								trn_sampledispatchheader_approvedby.intDispatchYear =  tb1.intDispatchYear AND
								trn_sampledispatchheader_approvedby.intApproveLevelNo =  ($i-1))<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*false part*/
								 if($i>(tb1.intApproveLevelStart-1),'-----',''))
								
								/*end IFNULL false part*/

								) as `".$approval."`, "; 
									
								}
								
						$sql .= " 'View' as `View`   
						FROM
						trn_sampledispatchheader as tb1 
						Inner Join mst_customer ON tb1.intDispatchTo = mst_customer.intId
						Inner Join sys_users ON tb1.intSavedBy = sys_users.intUserId 
						WHERE 1=1 
						$where_string
						ORDER BY
						tb1.intDispatchNo DESC,
						tb1.intDispatchYear DESC
						)  as t where 1=1
						";
					       //	echo $sql;
						 
$formLink			= "?q=$menuId&dispatchNo={Dispatch_No}&year={Dispatch_Year}";	 
$reportLink  		= "?q=$reportMenuId&dispatchNo={Dispatch_No}&dispatchYear={Dispatch_Year}";
$reportLinkApprove  = "?q=$reportMenuId&dispatchNo={Dispatch_No}&dispatchYear={Dispatch_Year}&mode=Confirm";
$reportLinkPrint  	= "?q=$reportMenuId&dispatchNo={Dispatch_No}&dispatchYear={Dispatch_Year}&mode=print";
						 
$col = array();

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;	$col=NULL;

//Fabric Receive No
$col["title"] 	= "Dispatch No"; // caption of column
$col["name"] 	= "Dispatch_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= $formLink;	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

//Fabric Receive Year
$col["title"] = "Dispatch Year"; // caption of column
$col["name"] = "Dispatch_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//Fabric Receive Year
$col["title"] = "Graphic"; // caption of column
$col["name"] = "graphic"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Fabric Receive Year
$col["title"] = "Dispatched Qty"; // caption of column
$col["name"] = "dispatched_qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Fabric Receive No
$col["title"] 	= "Sample No"; // caption of column
$col["name"] 	= "sampleNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= $formLink;	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

 
$cols[] = $col;	$col=NULL;

//Fabric Receive No
$col["title"] 	= "Sample Order No"; // caption of column
$col["name"] 	= "sampleOrderNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
//$col['link']	= $formLink;	 
//$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

 
$cols[] = $col;	$col=NULL;


//Fabric Receive Year
$col["title"] = "Marketer"; // caption of column
$col["name"] = "marketer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Fabric Receive Year
$col["title"] = "Brand"; // caption of column
$col["name"] = "brand"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Fabric Receive Year
$col["title"] = "Sample Category"; // caption of column
$col["name"] = "sampleType"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//DATE
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//DATE
$col["title"] = "User"; // caption of column
$col["name"] = "User"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//DATE
$col["title"] = "Customer"; // caption of column
$col["name"] = "Customer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//FIRST APPROVAL
$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;

$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}


//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Sample Dispatch Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'Dispatch_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);




$out = $jq->render("list1");
?>
<title>Sample Dispatch Listing</title>
<?php echo $out?>

<?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(trn_sampledispatchheader.intApproveLevelStart) AS appLevel
			FROM trn_sampledispatchheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
?>


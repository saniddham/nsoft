<?php
//ini_set('display_errors',1);
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
$backwardseperator = "../../../../../";
$companyId = $_SESSION['CompanyID'];
$userId = $_SESSION['userId'];

$mainPath = $_SESSION['mainPath'];
$thisFilePath = $_SERVER['PHP_SELF'];

include "../../../../../libraries/mail/mail.php";


####################
## get graphic no###
####################
$sql = "SELECT trn_sampleinfomations.strGraphicRefNo FROM trn_sampleinfomations WHERE trn_sampleinfomations.intSampleNo =  '$no' 
		AND trn_sampleinfomations.intSampleYear =  '$year' AND trn_sampleinfomations.intRevisionNo = '$revisionNo'";
$result = $db->RunQuery2($sql);
$row = mysqli_fetch_array($result);
$graphicNo = $row['strGraphicRefNo'];

########## email is sent by system ################
$enterUserName = "NSOFT - SCREENLINE HOLDINGS";
$enterUserEmail = "";
###################################################

/////////////////////////////////////////////////////////////////////////////////////////////////////

$sql1 = "SELECT
			sys_users.strUserName,
			sys_users.strEmail
		FROM
			sys_mail_eventusers
			Inner Join sys_users ON sys_users.intUserId = sys_mail_eventusers.intUserId
		WHERE
			sys_mail_eventusers.intMailEventId =  '1033' AND
			sys_mail_eventusers.intCompanyId =  '$companyId'
		";

$result1 = $db->RunQuery2($sql1);

while ($row1 = mysqli_fetch_array($result1)) {

    ob_start();

    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Marketing Confirm Email</title>
        <style type="text/css">
            .normalfnt {
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                font-weight: normal;
                text-align: left;
            }

            .normalfntWhite {
                font-family: Verdana;
                font-size: 11px;
                color: #FFF;
                margin: 0px;
                font-weight: normal;
                text-align: left;
            }

            .normalfntBlue {
                font-family: Verdana;
                font-size: 11px;
                color: #0B3960;
                margin: 0px;
                font-weight: normal;
                text-align: left;
            }

            .sampleNo {
                color: #039;
                font-weight: bold;
                font-size: 12px;
                font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;
            }

            .part {
                color: #096CBD;
                font-weight: bold;
                font-size: 13px;
                font-family: "Comic Sans MS", cursive;
            }

            .tableBorder_allRound {

                border: 1px solid #CCCCCC;
                -moz-border-radius-bottomright: 10px;
                -moz-border-radius-bottomleft: 10px;
                -moz-border-radius-topright: 10px;
                -moz-border-radius-topleft: 10px;
            }
        </style>
    </head>

    <body>
    <table width="642" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="3" class="normalfnt" style="color:#3E437D">&nbsp;</td>
        </tr>
        <tr>
            <td width="117" class="normalfnt">Dear <strong><?php echo $row1['strUserName']; ?></strong>,</td>
            <td width="474">&nbsp;</td>
            <td width="51">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="normalfnt">I have added dispatch to NSOFT system. Please refer the below link to view details.</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="normalfnt">click <a href="<?php
                echo $mainPath . "?q=982&dispatchNo=$dispatchNo&dispatchYear=$dispatchYear&mode=Confirm"; ?>">here</a>
                to view details about this dispatch.
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td height="108" colspan="3">
                <table style="border:double;border-color:#09F" width="100%" height="70" border="0" cellpadding="0"
                       cellspacing="0">
                    <tr>
                        <td width="19%" height="19" bgcolor="#9BDFEE" class="normalfnt"><strong>Sample No</strong></td>
                        <td width="18%" bgcolor="#9BDFEE" class="sampleNo"><?php echo "$no/$year"; ?></td>
                        <td width="20%" bgcolor="#9BDFEE" class="normalfnt"><strong>Revision No</strong></td>
                        <td width="24%" bgcolor="#9BDFEE" class="sampleNo"><?php echo $revisionNo; ?></td>
                        <td width="24%" bgcolor="#9BDFEE" class="sampleNo">&nbsp;</td>

                    </tr>
                    <tr>
                        <td width="24%" bgcolor="#9BDFEE">&nbsp;</td>
                        <td width="24%" bgcolor="#9BDFEE">&nbsp;</td>
                        <td width="24%" bgcolor="#9BDFEE">&nbsp;</td>
                        <td width="24%" bgcolor="#9BDFEE">&nbsp;</td>
                        <td width="24%" bgcolor="#9BDFEE">&nbsp;</td>

                    </tr>
                    <?php
                    $sql = "SELECT
					trn_sampleinfomations_printsize.strPrintName,
					mst_part.strName,
					trn_sampleinfomations_printsize.intWidth,
					trn_sampleinfomations_printsize.intHeight
				FROM
					trn_sampleinfomations_printsize
				Inner Join mst_part ON mst_part.intId = trn_sampleinfomations_printsize.intPart
				WHERE
					trn_sampleinfomations_printsize.intSampleNo =  '$no' AND
					trn_sampleinfomations_printsize.intSampleYear =  '$year' AND
					trn_sampleinfomations_printsize.intRevisionNo =  '$revisionNo'
				order by strPrintName
					
				";
                    $result = $db->RunQuery2($sql);
                    $i = 0;
                    while ($row = mysqli_fetch_array($result)) {
                        $printName = $row['strPrintName'];
                        $strName = $row['strName'];
                        $intWidth = $row['intWidth'];
                        $intHeight = $row['intHeight'];
                        $i++;
                        ?>
                        <tr>
                            <td colspan="2"
                                class="part"><?php echo "$printName - $strName - ($intWidth * $intHeight)" ?></td>
                            <td colspan="2">
                                <div id="divPicture" class="tableBorder_allRound" align="center"
                                     style="width:264px;height:148px;overflow:hidden">
                                    <img src="<?php
                                    echo $mainPath . "/documents/sampleinfo/samplePictures/$no-$year-$revNo-$i.png"; ?>"
                                         width="260" height="148" id="saveimg"/>
                                </div>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <?php
                    }
                    ?>

                    <tr>
                        <td colspan="2">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="normalfnt">(This is a<strong> <span style="color:#F28415">NSOFT</span> </strong>system
                generated email.)
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
    </body>
    </html>
    <?php
    $body = ob_get_clean();
   if(isset($enterUserEmail) && isset($enterUserName) ) {
       echo sendMessage2($enterUserEmail, $enterUserName, $row1['strEmail'], "$graphicNo ($no/$year)", $body);
   }
}




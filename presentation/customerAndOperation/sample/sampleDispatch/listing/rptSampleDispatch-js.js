// JavaScript Document
var basePath = "presentation/customerAndOperation/sample/sampleDispatch/listing/";

$(document).ready(function() {
	
	$('#frmSampleDispatchReport').validationEngine();
	
	$('#frmSampleDispatchReport #butRptConfirm').click(function(){
		
		var val = $.prompt('Are you sure you want to approve this Dispatch ?',{
					buttons: { Ok: true, Cancel: false },
					callback: function(v,m,f){
						if(v)
						{
						showWaiting();
						var url = basePath+"rptSampleDispatch-db-set.php"+window.location.search+'&requestType=approve';
						var obj = $.ajax({
							url:url,
							type:'post',
							dataType: "json",  
							data:'',
							async:false,
							
							success:function(json){
									$('#frmSampleDispatchReport #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
									if(json.type=='pass')
									{
										hideWaiting();
										var t=setTimeout("alertx1()",1000);
										window.location.href = window.location.href;
										window.opener.location.reload();//reload listing page
										return;
									}
								},
							error:function(xhr,status){
									
									$('#frmSampleDispatchReport #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
									var t=setTimeout("alertx1()",3000);
								}		
							});
	
							}
						   hideWaiting();
					}});
	});
	
	$('#frmSampleDispatchReport #butRptReject').click(function(){
		
		var val = $.prompt('Are you sure you want to Reject this Dispatch ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"rptSampleDispatch-db-set.php"+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmSampleDispatchReport #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									hideWaiting();
									var t=setTimeout("alertx2()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSampleDispatchReport #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx2()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
	});
	
});
function alertx1()
{
	$('#frmSampleDispatchReport #butRptConfirm').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmSampleDispatchReport #butRptReject').validationEngine('hide')	;
}
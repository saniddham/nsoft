<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId 				= $_SESSION['CompanyID'];
$userId 				= $_SESSION['userId'];
$locationId 			= $companyId;
$mainPath 				= $_SESSION['mainPath'];
$thisFilePath 			= $_SERVER['PHP_SELF'];
$programCode			= 'P0350';

include_once  	"class/cls_commonErrorHandeling_get.php";

$obj_commonErrHandle 	= new cls_commonErrorHandeling_get($db);

$dispatchNo 			= $_REQUEST['dispatchNo'];
$dispatchYear 			= $_REQUEST['dispatchYear'];
$mode 					= $_REQUEST['mode'];

$sql = "SELECT
		mst_customer.strName AS customerName,
		trn_sampledispatchheader.dtSaved AS dispatchDate,
		trn_sampledispatchheader.dtSaved AS CREATED_DATE,
		sys_users.strUserName AS enterUser,
		sys_users.strUserName as CREATOR ,
		trn_sampledispatchheader.intStatus,
		trn_sampledispatchheader.intStatus as STATUS,
		trn_sampledispatchheader.strAttentionTo,
		trn_sampledispatchheader.intApproveLevelStart,
		trn_sampledispatchheader.intApproveLevelStart as LEVELS,
		trn_sampledispatchheader.intCompanyId, 
		mst_customer_locations_header.strName as custLocation 
		FROM
		trn_sampledispatchheader
		Inner Join mst_customer ON mst_customer.intId = trn_sampledispatchheader.intDispatchTo
		Inner Join sys_users ON sys_users.intUserId = trn_sampledispatchheader.intSavedBy 
		left Join mst_customer_locations_header ON trn_sampledispatchheader.intCustLocation = mst_customer_locations_header.intId 
		WHERE 
		trn_sampledispatchheader.intDispatchNo =  '$dispatchNo' AND
		trn_sampledispatchheader.intDispatchYear =  '$dispatchYear' ";

$result 					= $db->RunQuery($sql);
$header_array 				= mysqli_fetch_array($result);
$customerName 				= $header_array['customerName'];
$attentionTo				= $header_array['strAttentionTo'];
$custLocation				= $header_array['custLocation'];
$saveBy 					= $header_array['enterUser'];
$savedate 					= $header_array['dispatchDate'];
$intStatus 					= $header_array['intStatus'];
$locationId 				= $header_array['intCompanyId'];
$levels 					= $header_array['intApproveLevelStart'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_reject($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_reject			= $permition_arr['permision'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_confirm			= $permition_arr['permision'];

?>
<head>
<title>Sample Dispatch Report</title>
<script type="application/javascript" src="presentation/customerAndOperation/sample/sampleDispatch/listing/rptSampleDispatch-js.js"></script>

<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:222px;
	top:173px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 16px;
	font-weight: bold;
	font-family: "Arial Black", Gadget, sans-serif;
	color: #36F;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="images/pending.png"  /></div>
<?php
}
?>
<form id="frmSampleDispatchReport" name="frmSampleDispatchReport" method="post">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>
<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>SAMPLE </strong><strong>DISPATCH</strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
	<?php
        include "presentation/report_approve_status_and_buttons.php"
     ?>
    <tr>
        <td>
        <table width="100%" border="0">
        <tr class="normalfnt">
            <td width="16%"><span class="normalfnt">Dispatch No</span></td>
            <td width="1%" align="center" valign="middle">:</td>
            <td width="37%"><span class="normalfnt"><?php echo $dispatchNo ?>/<?php echo $dispatchYear ?></span></td>
            <td width="14%"><span class="normalfnt">Dispatch Date</span></td>
            <td width="1%" align="center" valign="middle">:</td>
            <td width="31%"><span class="normalfnt"><?php echo $savedate ?></span></td>
        </tr>
        <tr class="normalfnt">
            <td>Customer</td>
            <td align="center" valign="middle">:</td>
            <td><span class="normalfnt"><?php echo $customerName ?></span></td>
            <td><span class="normalfnt">Dispatch By</span></td>
            <td align="center" valign="middle">:</td>
            <td><span class="normalfnt"><?php echo $saveBy ?></span></td>
        </tr>
        <tr class="normalfnt">
            <td>Attention To</td>
            <td align="center" valign="middle">:</td>
            <td><span class="normalfnt"><?php echo $attentionTo ?></span></td>
            <td>Location</td>
            <td align="center" valign="middle">:</td>
            <td><span class="normalfnt"><?php echo $custLocation ?></span></td>
        </tr>
        </table>
        </td>
    </tr>
    <tr>
    	<td>
        <table width="100%" class="rptBordered" id="tblDispatch" >
        <tr class="normalfnt">
            <th width="11%" >Sample No</th>
            <th width="4%" >Rev No</th>
            <th width="11%" >SampleOrder No</th>
            <th width="17%" >Graphic No</th>
            <th width="16%">Sample Type</th>
            <th width="9%">Combo</th>
            <th width="10%">Part</th>
            <th width="5%">Grade</th>
            <th width="4%">Size</th>
            <th width="6%">Dispatch Qty</th>
            <th width="7%"> Bag No</th>
        </tr>
          <?php

		  $sql = "SELECT
						DIS.intSampleNo,
						DIS.intSampleYear,
						DIS.intRevisionNo,
						DIS.SAMPLE_ORDER_NO,
						DIS.SAMPLE_ORDER_YEAR,
						DIS.strCombo,
						mst_part.strName,
						mst_part.intId,
						DIS.intGrade,
						sum(DIS.dblDispatchQty) as dblDispatchQty,
						DIS.strBagNo,
						trn_sampleinfomations.strGraphicRefNo,
						mst_sampletypes.strName as sampleType,
						DIS.intSampleTypeId,
						DIS.strSize,
						DIS.dtDeliveryDate,
						((
						SELECT
		Sum(ifnull(trn_sampleorderdetails.intBalToDeliverQty,0)) as balToDeliverQty
		FROM
			trn_sampleorderdetails
			Inner Join trn_sampleorderheader ON trn_sampleorderheader.intSampleOrderNo = trn_sampleorderdetails.intSampleOrderNo AND trn_sampleorderheader.intSampleOrderYear = trn_sampleorderdetails.intSampleOrderYear
			
		WHERE
			trn_sampleorderheader.intSampleNo 	=  DIS.intSampleNo AND
			trn_sampleorderheader.intSampleYear =  DIS.intSampleYear AND
			trn_sampleorderheader.intRevNo 		=  DIS.intRevisionNo AND
			trn_sampleorderdetails.strCombo 	=  DIS.strCombo AND
			trn_sampleorderdetails.intPart 		=  DIS.intPartId AND
			trn_sampleorderdetails.intGrade 	=  DIS.intGrade AND
			trn_sampleorderdetails.strSize 	=  DIS.strSize AND
			trn_sampleorderdetails.intSampleTypeId 	=  DIS.intSampleTypeId and
			trn_sampleorderdetails.dtRequiredDate 	=  DIS.dtDeliveryDate
		GROUP BY
			trn_sampleorderheader.intSampleNo,
			trn_sampleorderheader.intSampleYear,
			trn_sampleorderheader.intRevNo,
			trn_sampleorderdetails.strCombo,
			trn_sampleorderdetails.intPart,
			trn_sampleorderdetails.intGrade

						)) as balToDeliverQty
					FROM
						trn_sampledispatchdetails DIS
						Inner Join mst_part ON mst_part.intId = DIS.intPartId
						Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = DIS.intSampleNo AND trn_sampleinfomations.intSampleYear = DIS.intSampleYear AND trn_sampleinfomations.intRevisionNo = DIS.intRevisionNo
						inner Join mst_sampletypes on mst_sampletypes.intId = DIS.intSampleTypeId
					WHERE
						DIS.intDispatchNo 	=  '$dispatchNo' AND
						DIS.intDispatchYear 	=  '$dispatchYear'
					group by 
						DIS.intSampleNo,
						DIS.intSampleYear,
						DIS.intRevisionNo,
						DIS.SAMPLE_ORDER_NO,
						DIS.SAMPLE_ORDER_YEAR,
						DIS.strCombo,
						DIS.intGrade,
						DIS.intPartId,
						DIS.strSize,
						DIS.intSampleTypeId,
						DIS.dtDeliveryDate
					";

					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
				  	?>
                        <tr class="normalfnt" >
                            <td align="left" bgcolor="#FFFFFF"><?php echo $row['intSampleNo'].'/'.$row['intSampleYear'] ?></td>
                            <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['intRevisionNo'] ?>"><?php echo $row['intRevisionNo'] ?></td>
                            <td align="left" bgcolor="#FFFFFF" ><?php echo $row['SAMPLE_ORDER_NO'].'/'.$row['SAMPLE_ORDER_YEAR']; ?></td>
                            <td align="left" bgcolor="#FFFFFF" ><?php echo $row['strGraphicRefNo'] ?></td>
                            <td align="left" bgcolor="#FFFFFF" ><?php echo $row['sampleType'] ?></td>
                            <td align="left" bgcolor="#FFFFFF" ><?php echo $row['strCombo'] ?></td>
                            <td align="left" bgcolor="#FFFFFF" id="<?php echo $row['intId'] ?>" ><?php echo $row['strName'] ?></td>
                            <td align="left" bgcolor="#FFFFFF" ><?php echo $row['intGrade'] ?></td>
                            <td align="left" bgcolor="#FFFFFF" ><?php echo $row['strSize'] ?></td>
                            <td align="right" bgcolor="<?php
                            
                            echo (($row['dblDispatchQty']>$row['balToDeliverQty']) && ($intStatus==$intApproveLevelStart)?'#FF9797':'#FFFFFF') 
                            
                            ?>"  ><?php echo $row['dblDispatchQty'] ?></td>
                            <td align="right" bgcolor="#FFFFFF"><?php echo $row['strBagNo'] ?></td>
                        </tr>
         		 	<?php
					}
				  	?>
        </table>
        </td>
    </tr>
    <tr class="normalfnt">
    	<td>
        	<?php
				$creator		= $header_array['CREATOR'];
				$createdDate	= $header_array['CREATED_DATE'];
				$resultA 		= getRptApproveDetails($dispatchNo,$dispatchYear);
				include "presentation/report_approvedBy_details.php"
			?>
        </td>
    </tr>
    <tr height="40">
    	<td align="center" class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></td>
    </tr>
</table>
</div>        
</form>
</body>
</html>
<?php
function getRptApproveDetails($dispatchNo,$dispatchYear)
{
	global $db;
	
	$sql = "SELECT
			SDAB.intApproveUser AS APPROVED_BY,
			SDAB.dtApprovedDate AS dtApprovedDate,
			SU.strUserName AS UserName,
			SDAB.intApproveLevelNo AS intApproveLevelNo
			FROM
			trn_sampledispatchheader_approvedby SDAB
			INNER JOIN sys_users SU ON SDAB.intApproveUser = SU.intUserId
			WHERE SDAB.intDispatchNo ='$dispatchNo' AND
			SDAB.intDispatchYear ='$dispatchYear'      
			ORDER BY SDAB.dtApprovedDate ASC ";
	
	$result = $db->RunQuery($sql);
	return $result;
}
?>
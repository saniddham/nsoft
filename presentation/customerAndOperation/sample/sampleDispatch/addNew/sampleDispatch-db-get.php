<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	$companyId 	= $_SESSION['CompanyID'];
	
	include "{$backwardseperator}dataAccess/Connector.php";
	
	///
	/////////// type of print load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
					intId,
					strName
				FROM mst_techniques
				order by strName
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	elseif($requestType=='loadSampleNos')
	{
		$styleNo 			= $_REQUEST['styleNo'];
		$sampleYear 		= $_REQUEST['sampleYear'];
		
		if($styleNo!='')
			$para = " and trn_sampleinfomations.strGraphicRefNo =  '$styleNo'";
		$sql = "SELECT distinct
					trn_sampleinfomations.intSampleNo
				FROM trn_sampleinfomations
				WHERE
					
					trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations.intCompanyId =  '$companyId' /*$para*/
				order by intSampleNo
				";
				//echo $sql;
		$result = $db->RunQuery($sql);
		echo "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			echo "<option value=\"".$row['intSampleNo']."\">".$row['intSampleNo']."</option>";
		}
	}
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		$sql = "SELECT 	strName,
						strRemark,
						intStatus
				FROM mst_techniques
				WHERE
					intId =  '$id'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['name'] 	= $row['strName'];
			$response['remark'] = $row['strRemark'];
			$response['status'] = ($row['intStatus']?true:false);
		}
		echo json_encode($response);
	}
	
	else if($requestType=='loadCustLocationCombo'){
		
		$customer  = $_REQUEST['customer'];
		
		$sql = "SELECT
					mst_customer_locations.intLocationId,
					mst_customer_locations_header.strName
				FROM
					mst_customer_locations
					Inner Join mst_customer_locations_header ON mst_customer_locations.intLocationId = mst_customer_locations_header.intId
				WHERE
					mst_customer_locations.intCustomerId =  '$customer' order by strName";
		$result = $db->RunQuery($sql);
		$response['cboCustLocations'] 	= "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
				$response['cboCustLocations'] 	.=  "<option value=\"".$row['intLocationId']."\">".$row['strName']."</option>";
		}	
		echo json_encode($response);
	}
	else if($requestType=='loadAttentionToAndLocation'){
		$sampleNo  = $_REQUEST['sampleNo'];
		$arrNo=explode('/',$sampleNo);
		$sampleNo=$arrNo[0];
		$sampleYear=$arrNo[1];
		$revNo  = $_REQUEST['revNo'];
		$graphicNo  = $_REQUEST['graphicNo'];
		$combo  = $_REQUEST['combo'];
		$partId  = $_REQUEST['partId'];
		
		  $sql = "SELECT 
				trn_sampleorderheader.intCustomerLocation,
				trn_sampleorderheader.strAttentionTo,  
				trn_sampleorderheader.strRemarks   
				FROM
				trn_sampleorderheader
				Inner Join trn_sampleorderdetails ON trn_sampleorderheader.intSampleOrderNo = trn_sampleorderdetails.intSampleOrderNo AND trn_sampleorderheader.intSampleOrderYear = trn_sampleorderdetails.intSampleOrderYear
				WHERE 
				trn_sampleorderheader.intSampleNo =  '$sampleNo' AND
				trn_sampleorderheader.intSampleYear =  '$sampleYear' AND
				trn_sampleorderheader.intRevNo =  '$revNo' AND
				trn_sampleorderdetails.strCombo =  '$combo' AND
				trn_sampleorderdetails.intPart =  '$partId'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['location'] 	= $row['intCustomerLocation'];
			$response['attentionTo'] = $row['strAttentionTo'];
			$response['remarks'] = $row['strRemarks'];
		}
		echo json_encode($response);
	}
	
	else if($requestType=='loadPopupGrid'){
		$graphicNo  = $_REQUEST['graphicNo'];
	
					$sql = "SELECT
								trn_sampleorderheader.intSampleNo,
								trn_sampleorderheader.intSampleYear,
								trn_sampleorderheader.intRevNo,
								trn_sampleorderheader.intSampleOrderNo,
								trn_sampleorderheader.intSampleOrderYear,
								trn_sampleinfomations.strGraphicRefNo,
								trn_sampleorderdetails.strCombo,
								mst_part.intId AS partId,
								mst_part.strName AS partName,
								trn_sampleorderdetails.intGrade,
								trn_sampleorderdetails.strSize,
								trn_sampleorderdetails.dtRequiredDate,
								IFNULL(SUM(intBalToDeliverQty),0) AS balQty,
								mst_sampletypes.strName as sampleType,
								trn_sampleorderdetails.intSampleTypeId
								FROM
								trn_sampleorderheader
								Inner Join trn_sampleorderdetails ON trn_sampleorderdetails.intSampleOrderNo = trn_sampleorderheader.intSampleOrderNo AND trn_sampleorderdetails.intSampleOrderYear = trn_sampleorderheader.intSampleOrderYear
								Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleorderheader.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleorderheader.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_sampleorderheader.intRevNo
								Inner Join mst_part ON mst_part.intId = trn_sampleorderdetails.intPart
								inner Join mst_sampletypes on mst_sampletypes.intId = trn_sampleorderdetails.intSampleTypeId
							WHERE
								trn_sampleorderheader.intStatus =  '1' ";
						if($graphicNo!='')
						{		
							$sql .= " AND trn_sampleinfomations.strGraphicRefNo = '$graphicNo'";
						}	
						$sql .= " GROUP BY
								trn_sampleorderheader.intSampleOrderNo,
								trn_sampleorderheader.intSampleOrderYear,
								trn_sampleorderheader.intSampleNo,
								trn_sampleorderheader.intSampleYear,
								trn_sampleorderheader.intRevNo,
								trn_sampleorderdetails.strCombo,
								trn_sampleorderdetails.intGrade,
								trn_sampleorderdetails.intPart,
								trn_sampleorderdetails.intSampleTypeId,
								trn_sampleinfomations.strStyleNo,
								trn_sampleorderdetails.strSize,
								trn_sampleorderdetails.dtRequiredDate,
								mst_part.intId,
								mst_part.strName
							having balQty>0 
							ORDER BY 
								trn_sampleorderheader.intSampleNo ASC,
								trn_sampleorderheader.intSampleYear ASC,
								trn_sampleorderheader.intSampleOrderNo ASC,
								trn_sampleorderheader.intSampleOrderYear ASC,
								trn_sampleorderheader.intRevNo ASC,
								trn_sampleinfomations.strGraphicRefNo ASC,
								trn_sampleorderdetails.strCombo ASC,
								mst_part.strName ASC,
								trn_sampleorderdetails.intGrade ASC,
								trn_sampleorderdetails.strSize ASC 
							";
							
							//echo $sql;
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$data['sampleNo']=$row['intSampleNo'];	
						$data['sampleYear']=$row['intSampleYear'];		
						$data['revNo']=$row['intRevNo'];
						$data['sampleOrderNo']=$row['intSampleOrderNo'];	
						$data['sampleOrderYear']=$row['intSampleOrderYear'];		
						$data['graphicRefNo']=$row['strGraphicRefNo'];		
						$data['combo']=$row['strCombo'];		
						$data['partId']=$row['partId'];		
						$data['partName']=$row['partName'];	
						$data['sampleTypeId']=$row['intSampleTypeId'];	
						$data['sampleType']=$row['sampleType'];		
						$data['intGrade']=$row['intGrade'];	
						$data['strSize']=$row['strSize'];	
						$data['requiredDate']=$row['dtRequiredDate'];	
						$data['balQty']=$row['balQty'];	
						
						$arrCombo[] = $data;
		}
		$response['arrCombo'] 	= $arrCombo;
		 echo json_encode($response);
		
	}
	
?>
<?php 
session_start();
$backwardseperator 		= "../../../../../";
$mainPath 				= $_SESSION['mainPath'];
$userId 				= $_SESSION['userId'];
$locationId	  			= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];

$requestType 			= $_REQUEST['requestType'];
$programName			= 'Sample Dispatch';
$programCode			= 'P0350';

include_once "../../../../../dataAccess/Connector.php";
include_once "../../../../../class/cls_commonFunctions_get.php";
include_once "../../../../../class/cls_commonErrorHandeling_get.php";

$obj_common				= new cls_commonFunctions_get($db);
$obj_errorHandeling 	= new cls_commonErrorHandeling_get($db);

$savedStatus			= true;
$finalApprove			= false;
$savedMasseged 			= '';
$error_sql				= '';

if($requestType=='save')
{
	$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
	$arr 				= json_decode($_REQUEST['arr'], true);
	
	$db->begin();
	
	$serialNo			= $arrHeader['serialNo'];
	$Year				= $arrHeader['Year'];
	$date				= $arrHeader['date'];
	$dispatchTo			= $arrHeader['dispatchTo'];
	$custLocation		= $arrHeader['custLocation'];
	$attentionTo		= $obj_common->replace($arrHeader["attentionTo"]);
	$editMode			= false;
	
	$validateArr		= validateBeforeSave($serialNo,$Year,$programCode,$userId);
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateArr["msg"];
	}
	if($serialNo=='' && $Year=='')
	{
		$sysNo_arry 	= $obj_common->GetSystemMaxNo('intSampleDispatchNo',$locationId);
		if($sysNo_arry["rollBackFlag"]==1 && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $sysNo_arry["msg"];
			$error_sql		= $sysNo_arry["q"];	
		}
		
		$serialNo			= $sysNo_arry["max_no"];
		$Year				= date('Y');
		$approveLevels 		= $obj_common->getApproveLevels($programName);
		$status				= $approveLevels+1;
	
		$resultHArr			= saveHeader($serialNo,$Year,$date,$dispatchTo,$custLocation,$attentionTo,$status,$approveLevels,$userId,$locationId);
		if($resultHArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultHArr['savedMassege'];
			$error_sql		= $resultHArr['error_sql'];	
		}
	}
	else
	{
		$approveLevels 		= $obj_common->getApproveLevels($programName);
		$status				= $approveLevels+1;
		
		$resultUHArr		= updateHeader($serialNo,$Year,$date,$dispatchTo,$custLocation,$attentionTo,$status,$approveLevels,$userId,$locationId);
		if($resultUHArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultUHArr['savedMassege'];
			$error_sql		= $resultUHArr['error_sql'];	
		}
		
		$resultUHArr		= updateMaxStatus($serialNo,$Year);
		if($resultUHArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultUHArr['savedMassege'];
			$error_sql		= $resultUHArr['error_sql'];	
		}
		
		$resultDDArr		= deleteDetails($serialNo,$Year);
		if($resultUHArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultUHArr['savedMassege'];
			$error_sql		= $resultUHArr['error_sql'];	
		}
		$editMode			= true;
	}
	foreach($arr as $arrVal)
	{
		$sampleNo 			= $arrVal['sampleNo'];
		$sampleYear 		= $arrVal['sampleYear'];
		$revNo 				= $arrVal['revNo'];
		$sampleOrderNo 		= $arrVal['sampleOrderNo'];
		$sampleOrderYear 	= $arrVal['sampleOrderYear'];
		$combo 				= $arrVal['combo'];
		$partId 			= $arrVal['partId'];
		$typeId 			= $arrVal['typeId'];
		$size 				= $arrVal['size'];
		$delDate 			= $arrVal['delDate'];
		$grade 				= $arrVal['grade'];
		$disQty 			= $arrVal['disQty'];
		$bagNo 				= $arrVal['bagNo'];
		
		$chekQtyArr			= checkQtyBeforeFinalApproval($sampleOrderNo,$sampleOrderYear,$combo,$partId,$typeId,$size,$grade,$delDate,$disQty);
		if($chekQtyArr['type']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $chekQtyArr["msg"];
			$error_sql		= $chekQtyArr["sql"];
		}
		
		$resultDArr		= saveDetails($serialNo,$Year,$sampleNo,$sampleYear,$revNo,$sampleOrderNo,$sampleOrderYear,$combo,$partId,$typeId,$size,$delDate,$grade,$disQty,$bagNo);
		if($resultDArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultDArr['savedMassege'];
			$error_sql		= $resultDArr['error_sql'];	
		}
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 			= "pass";
		if($editMode)
			$response['msg'] 		= "Updated Successfully.";
		else
			$response['msg'] 		= "Saved Successfully.";
		
		$response['serialNo'] 		= $serialNo;
		$response['year'] 			= $Year;
	}
	else
	{
		$db->rollback();
		$response['type'] 			= "fail";
		$response['msg'] 			= $savedMasseged;
		$response['sql']			= $error_sql;
	}
	echo json_encode($response);
}
function validateBeforeSave($serialNo,$Year,$programCode,$userId)
{
	global $obj_errorHandeling;
	
	$header_arr		= loadHeaderData($serialNo,$Year,'RunQuery2');
	$validateArr 	= $obj_errorHandeling->get_permision_withApproval_save($header_arr['intStatus'],$header_arr['intApproveLevelStart'],$userId,$programCode,'RunQuery2');
	
	return  $validateArr;
}
function loadHeaderData($serialNo,$Year,$executionType)
{
	global $db;
	
	$sql = "SELECT intDispatchTo, 
			intCustLocation, 
			strAttentionTo, 
			intSavedBy, 
			dtSaved, 
			intConfirmedBy, 
			dtConfirmed, 
			intCompanyId, 
			intStatus, 
			intApproveLevelStart
			FROM 
			trn_sampledispatchheader 
			WHERE intDispatchNo = '$serialNo' AND
			intDispatchYear = '$Year' ";
	
	$result = $db->$executionType($sql);
	$row	= mysqli_fetch_array($result);
	
	return $row;
}
function saveHeader($serialNo,$Year,$date,$dispatchTo,$custLocation,$attentionTo,$status,$approveLevels,$userId,$locationId)
{
	global $db;
	
	$sql = "INSERT INTO trn_sampledispatchheader 
			(
			intDispatchNo, 
			intDispatchYear, 
			intDispatchTo, 
			intCustLocation, 
			strAttentionTo, 
			intSavedBy, 
			dtSaved, 
			intCompanyId, 
			intStatus, 
			intApproveLevelStart
			)
			VALUES
			(
			'$serialNo', 
			'$Year', 
			'$dispatchTo', 
			'$custLocation', 
			'$attentionTo', 
			'$userId', 
			NOW(), 
			'$locationId', 
			'$status', 
			'$approveLevels'
			)";
	
	
	$result = $db->RunQuery2($sql);
	
	if(!$result)
	{
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= $db->errormsg;
		$data['error_sql']		= $sql;
	}
	return $data;
}
function updateHeader($serialNo,$Year,$date,$dispatchTo,$custLocation,$attentionTo,$status,$approveLevels,$userId,$locationId)
{
	global $db;
	
	$sql = "UPDATE trn_sampledispatchheader 
			SET
			intDispatchTo = '$dispatchTo' , 
			intCustLocation = '$custLocation' , 
			strAttentionTo = '$attentionTo' , 
			intSavedBy = '$userId' , 
			dtSaved = NOW() , 
			intCompanyId = '$locationId' , 
			intStatus = '$status' , 
			intApproveLevelStart = '$approveLevels'
			
			WHERE
			intDispatchNo = '$serialNo' AND 
			intDispatchYear = '$Year' ";
	
	$result = $db->RunQuery2($sql);
	
	if(!$result)
	{
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= $db->errormsg;
		$data['error_sql']		= $sql;
	}
	return $data;
}
function updateMaxStatus($serialNo,$Year)
{
	global $db;
	
	$sql = "SELECT MAX(tb.intStatus) AS maxStatus  
			FROM trn_sampledispatchheader_approvedby AS tb
			WHERE 
			tb.intDispatchNo='$serialNo' AND
			tb.intDispatchYear='$Year' ";
	
	$result 	= $db->RunQuery2($sql);
	$row		= mysqli_fetch_array($result);
	$maxStatus	= $row['maxStatus'];
	
	$sqlIns = "UPDATE trn_sampledispatchheader_approvedby 
				SET
				intStatus = $maxStatus+1
				WHERE
				intDispatchNo = '$serialNo' AND 
				intDispatchYear = '$Year' ";
	
	$resultIns 	= $db->RunQuery2($sqlIns);
	if(!$resultIns)
	{
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= $db->errormsg;
		$data['error_sql']		= $sqlIns;
	}
	return $data;
}
function deleteDetails($serialNo,$Year)
{
	global $db;
	
	$sql = "DELETE FROM trn_sampledispatchdetails 
			WHERE
			intDispatchNo = '$serialNo' AND 
			intDispatchYear = '$Year' ";
	
	$result 	= $db->RunQuery2($sql);
	if(!$result)
	{
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= $db->errormsg;
		$data['error_sql']		= $sql;
	}
	return $data;
}
function saveDetails($serialNo,$Year,$sampleNo,$sampleYear,$revNo,$sampleOrderNo,$sampleOrderYear,$combo,$partId,$typeId,$size,$delDate,$grade,$disQty,$bagNo)
{
	global $db;
	
	$sql = "INSERT INTO trn_sampledispatchdetails 
			(
			intDispatchNo, 
			intDispatchYear, 
			intSampleNo, 
			intSampleYear, 
			intRevisionNo, 
			SAMPLE_ORDER_NO, 
			SAMPLE_ORDER_YEAR, 
			strCombo, 
			intPartId, 
			intGrade, 
			intSampleTypeId, 
			strSize, 
			dtDeliveryDate, 
			dblDispatchQty, 
			strBagNo
			)
			VALUES
			(
			'$serialNo', 
			'$Year', 
			'$sampleNo', 
			'$sampleYear', 
			'$revNo', 
			'$sampleOrderNo', 
			'$sampleOrderYear', 
			'$combo', 
			'$partId', 
			'$grade', 
			'$typeId', 
			'$size', 
			'$delDate', 
			'$disQty', 
			'$bagNo'
			)";
	
	$result 	= $db->RunQuery2($sql);
	if(!$result)
	{
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= $db->errormsg;
		$data['error_sql']		= $sql;
	}
	return $data;
}
function checkQtyBeforeFinalApproval($sampleOrderNo,$sampleOrderYear,$combo,$partId,$typeId,$size,$grade,$delDate,$disQty)
{
	global $db;
	$checkQty = false;
	
	$sqlBal = " SELECT intBalToDeliverQty
				FROM trn_sampleorderdetails 
				WHERE
				intSampleOrderNo = '$sampleOrderNo' AND 
				intSampleOrderYear = '$sampleOrderYear' AND 
				strCombo = '$combo' AND 
				intPart = '$partId' AND 
				intGrade = '$grade' AND 
				strSize = '$size' AND 
				intSampleTypeId = '$typeId' AND 
				DATE(dtRequiredDate) = DATE('$delDate') ";
	
	$resultBal 	= $db->RunQuery2($sqlBal);
	$rowBal 	= mysqli_fetch_array($resultBal);
	
	if($disQty > $rowBal['intBalToDeliverQty'])
	{
		$data['type'] 	= 'fail';
		$data['msg'] 	= 'Some Qty exceed Sample Dispatch bal qty. ';
		$data['sql'] 	= $sqlBal;
	}
	return $data;	
}
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$mainPath 				= $_SESSION['mainPath'];
$thisFilePath			=  $_SERVER['PHP_SELF'];
$locationId				= $_SESSION["CompanyID"];
$userId					= $_SESSION["userId"];

include_once "class/cls_commonFunctions_get.php";
include_once "class/cls_commonErrorHandeling_get.php";

$obj_commonErrHandle	= new cls_commonErrorHandeling_get($db);
$obj_common				= new cls_commonFunctions_get($db);

$programName			= 'Sample Dispatch';
$programCode			= 'P0350';
$dispatchNo 			= $_REQUEST['dispatchNo'];
$dispatchYear 			= $_REQUEST['year'];

$sql = "SELECT
		trn_sampledispatchheader.intDispatchTo,
		trn_sampledispatchheader.strAttentionTo,
		trn_sampledispatchheader.intCustLocation,
		DATE(trn_sampledispatchheader.dtSaved) as saveDate,
		trn_sampledispatchheader.dtConfirmed,
		trn_sampledispatchheader.intStatus,
		trn_sampledispatchheader.intApproveLevelStart
		FROM trn_sampledispatchheader
		WHERE
		trn_sampledispatchheader.intDispatchNo =  '$dispatchNo' AND
		trn_sampledispatchheader.intDispatchYear =  '$dispatchYear'";

$result = $db->RunQuery($sql);
$k 		= 0;
while($row = mysqli_fetch_array($result))
{
	if($k==0)
	{
		$remarks	= getRemarks($dispatchNo,$dispatchYear);
	}
	$customer			= $row['intDispatchTo'];
	$x_customerLocation	= $row['intCustLocation'];
	$attentionTo		= $row['strAttentionTo'];
	$date				= $row['saveDate'];
	$intStatus 			= $row['intStatus'];	
	$intApproveLevelStart = $row['intApproveLevelStart'];
	$k++;
}

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_save($intStatus,$intApproveLevelStart,$userId,$programCode,'RunQuery');
$permision_save			= $permition_arr['permision'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$intApproveLevelStart,$userId,$programCode,'RunQuery');
$permision_confirm		= $permition_arr['permision'];

?>
<title>SAMPLE DISPATCH</title>

<form id="frmSampleDispatch" name="frmSampleDispatch" method="post" action="">

<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text"> SAMPLE DISPATCH</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="13%" height="22" class="normalfnt">Sample Dispatch No</td>
            <td width="30%"><input name="txtDispNo" type="text" disabled="disabled" class="txtText" id="txtDispNo" style="width:60px" value="<?php echo $dispatchNo ?>"/>
              <input name="txtDispYear" type="text" disabled="disabled" class="txtText" id="txtDispYear" style="width:40px" value="<?php echo $dispatchYear ?>"/></td>
            <td width="6%">&nbsp;</td>
            <td width="13%">&nbsp;</td>
            <td width="21%" class="normalfntRight">Date</td>
            <td width="17%" align="right"><input name="dtDate" type="text" value="<?php if($dispatchNo){ echo $date; }else { echo date("Y-m-d");} ?>" class="txtbox" id="dtDate" style="width:98px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="13%" height="22" class="normalfnt">Dispatch To</td>
            <td width="36%"><select name="cboDispatchTo" id="cboDispatchTo" style="width:250px" <?php if($dispatchNo){ ?>disabled="disabled" <?php } ?>>
                  <option value=""></option>
                  <?php
				  if(!$dispatchNo)
				  {
					$sql = "SELECT DISTINCT
							mst_customer.strName,
							mst_customer.intId
							FROM
							trn_sampleorderheader
							Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleorderheader.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleorderheader.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_sampleorderheader.intRevNo
							Inner Join mst_customer ON trn_sampleinfomations.intCustomer = mst_customer.intId
							Inner Join trn_sampleorderdetails ON trn_sampleorderdetails.intSampleOrderNo = trn_sampleorderheader.intSampleOrderNo AND trn_sampleorderdetails.intSampleOrderYear = trn_sampleorderheader.intSampleOrderYear
							
							where (ifnull(trn_sampleorderdetails.intBalToDeliverQty,0))>0
						";
				  }else{
					$sql = "SELECT
								mst_customer.intId,
								mst_customer.strName
							FROM mst_customer
							";  
				  }
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$customer)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td width="7%" class="normalfntRight">Location&nbsp;</td>
            <td width="27%" class="normalfnt"><select name="cboLocation" id="cboLocation" class="validate[required]"  style="width:133px">
                <?php
					$sql = "SELECT
								mst_customer_locations.intLocationId,
								mst_customer_locations_header.strName
							FROM
								mst_customer_locations
								Inner Join mst_customer_locations_header ON mst_customer_locations.intLocationId = mst_customer_locations_header.intId
							WHERE
								mst_customer_locations.intCustomerId =  '$customer' order by strName";
					$result = $db->RunQuery($sql);
					echo  "<option value=\"\"></option>";
					while($row=mysqli_fetch_array($result))
					{
						if($row['intLocationId']==$x_customerLocation)
							echo "<option  selected=\"selected\" value=\"".$row['intLocationId']."\">".$row['strName']."</option>";
						else
							echo "<option value=\"".$row['intLocationId']."\">".$row['strName']."</option>";
					}	
			?>
              </select></td>
            <td width="17%" align="right">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
<tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="13%" height="22" class="normalfnt">Attention To</td>
            <td width="36%"><input name="txtAttentionTo" type="text" style="width:250px" id="txtAttentionTo" class="validate[required]" value="<?php echo $attentionTo ; ?>"  /></td>
            <td width="7%" class="normalfntRight">&nbsp;</td>
            <td width="27%" class="normalfnt">&nbsp;</td>
            <td width="17%" align="right">&nbsp;</td>
          </tr>
        </table></td>
      </tr>  
<tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="13%" height="22" class="normalfnt">Remarks</td>
            <td><textarea name="txtNote" id="txtNote" rows="3" style="width:425px" disabled="disabled"><?php echo $remarks; ?></textarea></td>
            <td width="17%" align="right"><img src="images/Tadd.jpg" width="92" height="24" id="butAddPendingDispatch" class="mouseover" /></td>
          </tr>
        </table></td>
      </tr>          <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="1300" class="bordered" id="tblDispatch" >
          <thead>
            <tr class="normalfnt">
              <th width="4%" height="22" >Del</th>
              <th width="10%" >Sample No</th>
              <th width="4%" >Rev No</th>
              <th width="10%" >SampleOrder No</th>
              <th width="10%" >Graphic No</th>
              <th width="9%">Combo</th>
              <th width="14%">Part</th>
              <th width="11%">Sample Type </th>
              <th width="6%">Grade</th>
              <th width="4%">Size</th>
              <th width="8%">Delivery Date</th>
              <th width="6%">Bal Qty</th>
              <th width="8%">Dispatch Qty</th>
              <th width="6%"> Bag No</th>
              </tr>
              </thead>
              <tbody>
              <?php
			  
			$sql = "SELECT
						DIS.intSampleNo,
						DIS.intSampleYear,
						DIS.intRevisionNo,
						DIS.strCombo,
						mst_part.strName,
						mst_part.intId,
						DIS.intGrade,
						DIS.dblDispatchQty,
						DIS.SAMPLE_ORDER_NO,
						DIS.SAMPLE_ORDER_YEAR,
						((
						SELECT
		Sum(ifnull(trn_sampleorderdetails.intBalToDeliverQty,0)) as balToDeliverQty
		FROM
			trn_sampleorderdetails
			Inner Join trn_sampleorderheader ON trn_sampleorderheader.intSampleOrderNo = trn_sampleorderdetails.intSampleOrderNo AND trn_sampleorderheader.intSampleOrderYear = trn_sampleorderdetails.intSampleOrderYear
			
		WHERE
			trn_sampleorderheader.intSampleNo 	=  DIS.intSampleNo AND
			trn_sampleorderheader.intSampleYear =  DIS.intSampleYear AND
			trn_sampleorderheader.intRevNo 		=  DIS.intRevisionNo AND
			trn_sampleorderdetails.intSampleOrderNo = DIS.SAMPLE_ORDER_NO AND
			trn_sampleorderdetails.intSampleOrderYear = DIS.SAMPLE_ORDER_YEAR AND
			trn_sampleorderdetails.strCombo 	=  DIS.strCombo AND
			trn_sampleorderdetails.intPart 		=  DIS.intPartId AND
			trn_sampleorderdetails.intGrade 	=  DIS.intGrade AND
			trn_sampleorderdetails.strSize 	=  DIS.strSize AND
			trn_sampleorderdetails.intSampleTypeId 	=  DIS.intSampleTypeId AND
			trn_sampleorderdetails.dtRequiredDate 	=  DIS.dtDeliveryDate
		GROUP BY
			trn_sampleorderheader.intSampleNo,
			trn_sampleorderheader.intSampleYear,
			trn_sampleorderheader.intRevNo,
			trn_sampleorderdetails.intSampleOrderNo,
			trn_sampleorderdetails.intSampleOrderYear,
			trn_sampleorderdetails.strCombo,
			trn_sampleorderdetails.intPart,
			trn_sampleorderdetails.intGrade,
			trn_sampleorderdetails.strSize,
			trn_sampleorderdetails.intSampleTypeId,
			trn_sampleorderdetails.dtRequiredDate

						)) as balQty,
						DIS.strBagNo,
						trn_sampleinfomations.strGraphicRefNo,
						mst_sampletypes.strName as sampleType,
						DIS.intSampleTypeId,
						DIS.strSize,
						DIS.dtDeliveryDate
					FROM
						trn_sampledispatchdetails DIS
						Inner Join mst_part ON mst_part.intId = DIS.intPartId
						Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = DIS.intSampleNo AND trn_sampleinfomations.intSampleYear = DIS.intSampleYear AND trn_sampleinfomations.intRevisionNo = DIS.intRevisionNo
						inner Join mst_sampletypes on mst_sampletypes.intId = DIS.intSampleTypeId
					WHERE
						DIS.intDispatchNo 	=  '$dispatchNo' AND
						DIS.intDispatchYear 	=  '$dispatchYear'
					";

					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
				  ?>
			<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" id="<?php echo $customer; ?>"><img class="mouseover delImg" src="images/del.png" width="15" height="15" /></td>
			<td align="center" bgcolor="#FFFFFF"><?php echo $row['intSampleNo'].'/'.$row['intSampleYear'] ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['intRevisionNo'] ?>"><?php echo $row['intRevisionNo'] ?></td>
            <td align="center" bgcolor="#FFFFFF" id=""><?php echo $row['SAMPLE_ORDER_NO'].'/'.$row['SAMPLE_ORDER_YEAR'] ?></td>
			<td align="center" bgcolor="#FFFFFF" ><?php echo $row['strGraphicRefNo'] ?></td>
			<td align="center" bgcolor="#FFFFFF" ><?php echo $row['strCombo'] ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['intId'] ?>" ><?php echo $row['strName'] ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['intSampleTypeId'] ?>"><?php echo $row['sampleType'] ?></td>
			<td align="center" bgcolor="#FFFFFF" ><?php echo $row['intGrade'] ?></td>
			<td align="center" bgcolor="#FFFFFF" ><?php echo $row['strSize'] ?></td>
			<td align="center" bgcolor="#FFFFFF" ><?php echo $row['dtDeliveryDate'] ?></td>
			<td align="center" bgcolor="#FFFFFF" ><?php echo $row['balQty'] ?></td>
			<td align="center" bgcolor="#FFFFFF" ><input id="txtDispatchQty" style="width:50px;text-align:right" type="text" value="<?php echo $row['dblDispatchQty'] ?>" class="validate[required,custom[integer],max[<?php echo $row['balQty'] ?>]]" /></td>
			<td align="center" bgcolor="#FFFFFF"><input style="width:50px;text-align:right" type="text" value="<?php echo $row['strBagNo'] ?>"/></td></tr>
              <?php
					}
				  ?>
              </tbody>   
            </table>
          </div></td>
      </tr> 
        <tr>
        	<td align="center" class=""><a class="button white medium" id="butNew">New</a><a class="button white medium" id="butSave" <?php if($permision_save!=1){ ?>style="display:none"<?php } ?>>Save</a><a class="button white medium" id="butConfirm"  <?php if($permision_confirm!=1){ ?> style="display:none"<?php } ?>>Approve</a><a class="button white medium" id="butReport">Report</a><a href="main.php" class="button white medium" id="butClose">Close</a></td>
        </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>

	<!-- penging dispatches -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</body>
</html>
<?php
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+1-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//--------------------------------------------------
function getRemarks($dispatchNo,$dispatchYear){
	global $db;
	
		  $sql = "SELECT
trn_sampleorderheader.strRemarks
FROM
trn_sampledispatchdetails
Inner Join trn_sampleorderheader ON trn_sampledispatchdetails.intSampleNo = trn_sampleorderheader.intSampleNo AND trn_sampledispatchdetails.intSampleYear = trn_sampleorderheader.intSampleYear AND trn_sampledispatchdetails.intRevisionNo = trn_sampleorderheader.intRevNo
WHERE
trn_sampledispatchdetails.intDispatchNo =  '$dispatchNo' AND
trn_sampledispatchdetails.intDispatchYear =  '$dispatchYear'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$remarks 	= $row['strRemarks'];
		}
	return 	$remarks;
}
?>


<?php
session_start();
$backwardseperator = "../../../../../";

//$formName		  	= 'frmItem';
//include  	"{$backwardseperator}dataAccess/permisionCheck.inc";
include $backwardseperator."dataAccess/Connector.php";

$customerId = $_REQUEST["id"];

?>
<link rel="stylesheet" href="libraries/chosen/chosen_1.1.0/chosen.min.css" type="text/css"/>
<script src="libraries/chosen/chosen_1.1.0/chosen.jquery.min.js" type="text/javascript"></script>
<form id="frmSampleDispatchPopup" name="frmSampleDispatchPopup" method="post" action="">
<table width="450" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text"> SAMPLE DISPATCH</div>
		  <table width="840" border="0" align="center" bgcolor="#FFFFFF">
    <td width="834"><table width="831" border="0">
      <tr>
        <td><table width="650" border="0" cellpadding="0" cellspacing="0">
          <tr class="normalfnt">
            <td width="88" height="22" class="normalfnt">Graphic No</td>
            <td width="562" class="normalfnt"><select name="cboGraphicNo" id="cboGraphicNo" class="chosen-select" style="width:250px" >
              <option value=""></option>
              <?php
			$sql = "SELECT DISTINCT SI.strGraphicRefNo
					FROM trn_sampleorderheader SOH
					INNER JOIN trn_sampleinfomations SI ON SI.intSampleNo=SOH.intSampleNo AND
					SI.intSampleYear=SOH.intSampleYear AND
					SI.intRevisionNo=SOH.intRevNo
					ORDER BY SI.strGraphicRefNo ";
			
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				echo "<option value=\"".$row['strGraphicRefNo']."\">".$row['strGraphicRefNo']."</option>";	
			}
			?>
            </select></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="bordered" id="tblDispatchPopup" >
            <tr class="normalfnt">
              <th width="3%">&nbsp;</th>
              <th width="10%" >Sample No</th>
              <th width="4%" >Rev No</th>
              <th width="10%" >SampleOrder No</th>
              <th width="12%" >Graphic No</th>
              <th width="8%" >Combo</th>
              <th width="10%" >Part</th>
              <th width="12%" >Sample Type</th>
              <th width="5%" >Grade</th>
              <th width="7%" >Size</th>
              <th width="10%" >Delivery Date</th>
              <th width="9%">Bal  Qty</th>
              </tr>
              <?php
					$sql = "SELECT
								trn_sampleorderheader.intSampleNo,
								trn_sampleorderheader.intSampleYear,
								trn_sampleorderheader.intSampleOrderNo,
								trn_sampleorderheader.intSampleOrderYear,
								trn_sampleorderheader.intRevNo,
								trn_sampleinfomations.strGraphicRefNo,
								trn_sampleorderdetails.strCombo,
								mst_part.intId AS partId,
								mst_part.strName AS partName,
								trn_sampleorderdetails.intGrade,
								trn_sampleorderdetails.strSize,
								trn_sampleorderdetails.dtRequiredDate,
								IFNULL(SUM(intBalToDeliverQty),0) AS balQty,
								mst_sampletypes.strName as sampleType,
								trn_sampleorderdetails.intSampleTypeId
								FROM
								trn_sampleorderheader
								Inner Join trn_sampleorderdetails ON trn_sampleorderdetails.intSampleOrderNo = trn_sampleorderheader.intSampleOrderNo AND trn_sampleorderdetails.intSampleOrderYear = trn_sampleorderheader.intSampleOrderYear
								Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleorderheader.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleorderheader.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_sampleorderheader.intRevNo
								Inner Join mst_part ON mst_part.intId = trn_sampleorderdetails.intPart
								inner Join mst_sampletypes on mst_sampletypes.intId = trn_sampleorderdetails.intSampleTypeId
							WHERE
								trn_sampleorderheader.intStatus =  '1' AND
								trn_sampleinfomations.intCustomer =  '$customerId'
							
							GROUP BY
								trn_sampleorderheader.intSampleNo,
								trn_sampleorderheader.intSampleYear,
								trn_sampleorderheader.intSampleOrderNo,
								trn_sampleorderheader.intSampleOrderYear,
								trn_sampleorderheader.intRevNo,
								trn_sampleorderdetails.strCombo,
								trn_sampleorderdetails.intGrade,
								trn_sampleorderdetails.intPart,
								trn_sampleorderdetails.intSampleTypeId,
								trn_sampleorderdetails.strSize,
								trn_sampleorderdetails.dtRequiredDate
							having balQty>0 
							ORDER BY 
								trn_sampleorderheader.intSampleNo ASC,
								trn_sampleorderheader.intSampleYear ASC,
								trn_sampleorderheader.intSampleOrderNo ASC,
								trn_sampleorderheader.intSampleOrderYear ASC,
								trn_sampleorderheader.intRevNo ASC,
								trn_sampleinfomations.strGraphicRefNo ASC,
								trn_sampleorderdetails.strCombo ASC,
								mst_part.strName ASC,
								trn_sampleorderdetails.intGrade ASC,
								trn_sampleorderdetails.strSize ASC 
							";
					
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
			  
			  ?>
            <tr class="normalfnt data">
              <td align="center" bgcolor="#FFFFFF"><input type="checkbox" id="chkDisp" /></td>
              <td bgcolor="#FFFFFF" ><?php echo $row['intSampleNo']."/".$row['intSampleYear'];?></td>
              <td style="text-align:center" bgcolor="#FFFFFF" ><?php echo $row['intRevNo'];?></td>
              <td bgcolor="#FFFFFF" ><?php echo $row['intSampleOrderNo']."/".$row['intSampleOrderYear'];?></td>
              <td bgcolor="#FFFFFF" ><?php echo $row['strGraphicRefNo'];?></td>
              <td bgcolor="#FFFFFF" ><?php echo $row['strCombo'];?></td>
              <td bgcolor="#FFFFFF" id="<?php echo $row['partId']; ?>" ><?php echo $row['partName'];?></td>
              <td bgcolor="#FFFFFF" class="normalfnt" id="<?php echo $row['intSampleTypeId'];?>" ><?php echo $row['sampleType'];?></td>
              <td bgcolor="#FFFFFF" class="normalfnt" ><?php echo $row['intGrade'];?></td>
              <td bgcolor="#FFFFFF" class="normalfnt" ><?php echo $row['strSize'];?></td>
              <td bgcolor="#FFFFFF" class="normalfntMid" ><?php echo $row['dtRequiredDate'];?></td>
              <td bgcolor="#FFFFFF" class="normalfntRight" style="color:#00C" ><?php echo $row['balQty'];?></td>
              </tr>
              
              <?php
					}
			  ?>
            </table>
          </div></td>
      </tr>
      <tr>
        <td align="center" class=""><a class="button white medium" id="butAdd">Add</a><a class="button white medium" id="butClose1">Close</a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
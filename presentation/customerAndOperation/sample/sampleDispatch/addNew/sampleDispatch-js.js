var basePath	="presentation/customerAndOperation/sample/sampleDispatch/addNew/";
var reportId	="982";
			
$(document).ready(function() {
  		$("#frmSampleDispatch").validationEngine();
		$('#frmSampleDispatch #cboDispatchTo').focus();
		
		
		$('.delImg').die('click').live('click',function(){
			$(this).parent().parent().remove();
		});

		$("#butAddPendingDispatch").die('click').live('click',function(){
		popupWindow3('1');
		//$('#iframeMain1').attr('src','http://localhost/qpet/presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php?id='+$('#cboDispatchTo').val());
		$('#popupContact1').load(basePath+'sampleDispatchPopup.php?id='+$('#frmSampleDispatch #cboDispatchTo').val(),function(){
				
				$(".chosen-select").chosen({allow_single_deselect: true , no_results_text: "Oops, nothing found!"});
				$("#frmSampleDispatchPopup #cboGraphicNo").chosen().change(loadPopupGrid);
				//loadPopupGrid(); 
				checkAlreadySelected();
				$('#butAdd').die('click').live('click',addClickedRows);
				$('#butClose1').die('click').live('click',disablePopup);
				
			});	
		});
		
	  //--------------change customer------------
		$('#frmSampleDispatch #cboDispatchTo').die('change').live('change',function(){
				var rowCount = document.getElementById('tblDispatch').rows.length;
				if(rowCount>1){
				var val = $.prompt('Are you sure you want to delete existing samples  ?',{
							buttons: { Ok: true, Cancel: false },
							callback: function(v,m,f){
								if(v)
								{
									clearRows();
								}
								else{
									$('#cboDispatchTo').val(document.getElementById('tblDispatch').rows[1].cells[0].id);
								}
					}
				});
				}
				loadCustLocationCombo();
		});
	//--------------------------------------------

/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/
  //-------save button click event----------------------
  $('#frmSampleDispatch #butSave').die('click').live('click',function(){
	//$('#frmSampleInfomations').submit();
	var requestType = '';
	if ($('#frmSampleDispatch').validationEngine('validate'))   
    { 
		showWaiting();
		var data = "requestType=save";
		
			/*data+="&serialNo="		+	$('#txtDispNo').val();
			data+="&Year="	+	$('#txtDispYear').val();
			data+="&date="			+	$('#dtDate').val();
			data+="&dispatchTo="	+	$('#cboDispatchTo').val();
			data+="&custLocation="	+	$('#cboLocation').val();
			data+="&attentionTo="	+	$('#txtAttentionTo').val();*/
		
		var arrHeader = "{";
							arrHeader += '"serialNo":"'+$('#txtDispNo').val()+'",' ;
							arrHeader += '"Year":"'+$('#txtDispYear').val()+'",' ;
							arrHeader += '"date":"'+$('#dtDate').val()+'",' ;
							arrHeader += '"dispatchTo":"'+$('#cboDispatchTo').val()+'",' ;
							arrHeader += '"custLocation":"'+$('#cboLocation').val()+'",' ;
							arrHeader += '"attentionTo":'+URLEncode_json($('#txtAttentionTo').val())+'';
			arrHeader += "}";	
			//////////////////// saving main grid part //////////////////////////////////////////
			//var rowCount = $('#tblMain >tbody >tr').length;
			var rowCount = document.getElementById('tblDispatch').rows.length;
			var row = 0;
			
			var arr="[";
			
			for(var i=1;i<rowCount;i++)
			{
					 arr += "{";
					 
					//var styleNo = 	document.getElementById('tblDispatch').rows[i].cells[1].innerHTML;
					var sampleID 		= 	document.getElementById('tblDispatch').rows[i].cells[1].innerHTML;
					var revNo 			= 	document.getElementById('tblDispatch').rows[i].cells[2].id;
					var smpOrderID 		= 	document.getElementById('tblDispatch').rows[i].cells[3].innerHTML;
					var combo 			= 	document.getElementById('tblDispatch').rows[i].cells[5].innerHTML;
					var partId 			= 	document.getElementById('tblDispatch').rows[i].cells[6].id;
					var typeId 			= 	document.getElementById('tblDispatch').rows[i].cells[7].id;
					var grade 			= 	document.getElementById('tblDispatch').rows[i].cells[8].innerHTML;
					var size 			= 	document.getElementById('tblDispatch').rows[i].cells[9].innerHTML;
					var delDate			= 	document.getElementById('tblDispatch').rows[i].cells[10].innerHTML;
					var disQty 			= 	document.getElementById('tblDispatch').rows[i].cells[12].childNodes[0].value;
					var bagNo 			= 	document.getElementById('tblDispatch').rows[i].cells[13].childNodes[0].value;
					
					var sampleID1		=	sampleID.split("/");
					var sampleNo		=	sampleID1[0];
					var sampleYear		=	sampleID1[1];
					
					var smpOrderID1		=	smpOrderID.split("/");
					var smpOrderNo		=	smpOrderID1[0];
					var smpOrderYear	=	smpOrderID1[1];
					
					arr += '"sampleNo":"'+			sampleNo +'",' ;
					arr += '"sampleYear":"'+		sampleYear +'",' ;
					arr += '"revNo":"'+				revNo +'",' ;
					arr += '"sampleOrderNo":"'+		smpOrderNo +'",' ;
					arr += '"sampleOrderYear":"'+	smpOrderYear +'",' ;
					arr += '"combo":"'+				combo +'",' ;
					arr += '"partId":"'+			partId +'",' ;
					arr += '"typeId":"'+			typeId +'",' ;
					arr += '"size":"'+				size +'",' ;
					arr += '"delDate":"'+			delDate +'",' ;
					arr += '"grade":"'+				grade +'",' ;
					arr += '"disQty":"'+			disQty +'",' ;
					arr += '"bagNo":"'+				bagNo +'"' ;
					
					arr +=  '},';
				
			}
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			var arrHeader	= arrHeader;
			data+="&arr="+arr+"&arrHeader="+arrHeader;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basePath+"sampleDispatch-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			type:'POST', 
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmSampleDispatch #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{	//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						//loadCombo_frmSampleInfomations();
						hideWaiting();
						var t=setTimeout("alertx()",1000);
						$('#txtDispNo').val(json.serialNo);
						$('#txtDispYear').val(json.year);
						document.location.href = '?q=350&dispatchNo='+json.serialNo+'&year='+json.year;
						return;
					}
					else
						hideWaiting();
				},
			error:function(xhr,status){
					$('#frmSampleDispatch #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					hideWaiting();
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
	
	//--------------refresh the form----------
	$('#frmSampleDispatch #butNew').die('click').live('click',function(){
		window.location.href = "?q=350";
		/*$('#frmSampleDispatch').get(0).reset();
		clearRows();
		$('#frmSampleDispatch #cboDispatchTo').removeAttr('disabled');
		$('#frmSampleDispatch #cboDispatchTo').val('');
		$('#frmSampleDispatch #cboDispatchTo').focus();
		$('#frmSampleDispatch #txtDispNo').val('');
		$('#frmSampleDispatch #txtDispYear').val('');
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmSampleDispatch #dtDate').val(d);*/
	});
	//----------------------------------------
	
/*    $('#frmSampleDispatch #butDelete').click(function(){
		if($('#frmSampleDispatch #cboSearch').val()=='')
		{
			$('#frmSampleDispatch #butDelete').validationEngine('showPrompt', 'Please select Type.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmSampleDispatch #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "typeOfPrint-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmSampleDispatch #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmSampleDispatch #butDelete').validationEngine('showPrompt', json.msg,json.type );
												
												if(json.type=='pass')
												{
													$('#frmSampleDispatch').get(0).reset();
													loadCombo_frmSampleDispatch();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
	
*/	

//-----------------------------------
$('#frmSampleDispatch #butReport').die('click').live('click',function(){
	if($('#frmSampleDispatch #txtDispNo').val()!=''){
		window.open('?q='+reportId+'&dispatchNo='+$('#frmSampleDispatch #txtDispNo').val()+'&dispatchYear='+$('#frmSampleDispatch #txtDispYear').val(),'rptSampleDispatch.php');	
	}
	else
	{
		$('#frmSampleDispatch #butReport').validationEngine('showPrompt','There is no Dispatch No to view','fail');
		return;
	}
});
//----------------------------------	
$('#frmSampleDispatch #butConfirm').die('click').live('click',function(){
	
	var url  = '?q='+reportId+'&dispatchNo='+$('#frmSampleDispatch #txtDispNo').val();
	    url += "&dispatchYear="+$('#frmSampleDispatch #txtDispYear').val();
	    url += "&mode=Confirm";
	window.open(url);
});
//----------------------------------
});

//----------end of ready ----------------------------------------------
function loadMain()
{
	//alert(1);
/*	$("#butAddPendingDispatch").click(function(){
		popupWindow3('1');
		//$('#iframeMain1').attr('src','http://localhost/qpet/presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php?id='+$('#cboDispatchTo').val());
		$('#popupContact1').load('sampleDispatchPopup.php');	
		
	});*/
	//$("#iframeMain1").contents().find("#butAdd").click(abc);
	//$("#frmSampleDispatchPopup").contents().find("#butAdd").click(abc);
//	$('#frmSampleDispatchPopup #butClose').click(abc);
}


function addClickedRows()
{
	//var rowCount = $('#tblDispatchPopup >tr').length;
	var rowCount = document.getElementById('tblDispatchPopup').rows.length;

	for(var i=1;i<rowCount;i++)
	{
		var chkstatus = false;
		if((document.getElementById('tblDispatchPopup').rows[i].cells[0].childNodes[0].checked==true) && (document.getElementById('tblDispatchPopup').rows[i].cells[0].childNodes[0].disabled==false)){
			var sampleNo		=document.getElementById('tblDispatchPopup').rows[i].cells[1].innerHTML;
			var revNo			=document.getElementById('tblDispatchPopup').rows[i].cells[2].innerHTML;
			var sampleOrderNo	=document.getElementById('tblDispatchPopup').rows[i].cells[3].innerHTML;
			var graphicNo		=document.getElementById('tblDispatchPopup').rows[i].cells[4].innerHTML;
			var combo			=document.getElementById('tblDispatchPopup').rows[i].cells[5].innerHTML;
			var partId			=document.getElementById('tblDispatchPopup').rows[i].cells[6].id;
			var part			=document.getElementById('tblDispatchPopup').rows[i].cells[6].innerHTML;
			var type			=document.getElementById('tblDispatchPopup').rows[i].cells[7].innerHTML;
			var typeId			=document.getElementById('tblDispatchPopup').rows[i].cells[7].id;
			var grade			=document.getElementById('tblDispatchPopup').rows[i].cells[8].innerHTML;
			var size			=document.getElementById('tblDispatchPopup').rows[i].cells[9].innerHTML;
			var delDate			=document.getElementById('tblDispatchPopup').rows[i].cells[10].innerHTML;
			var balQty			=document.getElementById('tblDispatchPopup').rows[i].cells[11].innerHTML;
			
			var rowCount1 		= document.getElementById('tblDispatch').rows.length;
			for(var j=1;j<rowCount1;j++)
			{		
					var sampleNoM	 	= document.getElementById('tblDispatch').rows[j].cells[1].innerHTML;
					var revNoM	 	 	= document.getElementById('tblDispatch').rows[j].cells[2].innerHTML;
					var smpOrderNoM	 	= document.getElementById('tblDispatch').rows[j].cells[3].innerHTML;
					var comboM			= document.getElementById('tblDispatch').rows[j].cells[5].innerHTML;
					var partIdM		 	= document.getElementById('tblDispatch').rows[j].cells[6].id;
					var typeIdM		 	= document.getElementById('tblDispatch').rows[j].cells[7].id;
					var gradeM		 	= document.getElementById('tblDispatch').rows[j].cells[8].innerHTML;
					var sizeM		 	= document.getElementById('tblDispatch').rows[j].cells[9].innerHTML;
					var deliveryDateM 	= document.getElementById('tblDispatch').rows[j].cells[10].innerHTML;
					
					if((sampleNo==sampleNoM) && (revNo==revNoM) && (sampleOrderNo==smpOrderNoM) && (combo==comboM) && (partId==partIdM)&& (typeId==typeIdM)&& (grade==gradeM)&& (size==sizeM)&& (delDate==deliveryDateM))
					{
						chkstatus = true;
					}
			}
			
			if(!chkstatus)
			{
			//alert($('#frmSampleDispatch #cboDispatchTo').val());
			var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" id="'+$('#cboDispatchTo').val()+'"><img class="mouseover delImg" src="images/del.png" width="15" height="15" /></td>';
			
			content +='<td align="center" bgcolor="#FFFFFF" >'+sampleNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+revNo+'" >'+revNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="" >'+sampleOrderNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" >'+graphicNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" >'+combo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+partId+'">'+part+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+typeId+'">'+type+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" >'+grade+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" >'+size+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" >'+delDate+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+balQty+'">'+balQty+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+balQty+'"><input style="width:50px;text-align:right" type="text" value="'+balQty+'" class="validate[required,custom[integer],max['+balQty+']] " id="'+balQty+'"/></td>';
			content +='<td align="center" bgcolor="#FFFFFF"><input style="width:50px;text-align:right" type="text" value=""/></td></tr>';
			add_new_row('#frmSampleDispatch #tblDispatch',content);
			}
			
			if(i==1){
				loadAttentionToAndLocation(sampleNo,revNo,graphicNo,combo,partId)
			}
		//----------------------------	

		
		//----------------------------	
/*		$('.clsValidateBalQty').keyup(function(){
			var input=$(this).val();
			var balQty=$(this).closest('td').attr('id');
			if((input>balQty) || (input<0)){
			alert("Invalid Qty");
				$(this).val(balQty);
			}

		});
*/		
/*	  //--------------change customer------------
		$('#frmSampleDispatch #cboDispatchTo').change(function(){
				var val = $.prompt('Are you sure you want to delete existing samples  ?',{
							buttons: { Ok: true, Cancel: false },
							callback: function(v,m,f){
								if(v)
								{
									clearRows();
								}
					}
				});
		});
	//--------------------------------------------	
*/		}
	}
	
	disablePopup();
}
//-------------------------------------
function loadCombo_frmSampleDispatch()
{
	var url 	= "typeOfPrint-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmSampleDispatch #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmSampleDispatch #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmSampleDispatch #butDelete').validationEngine('hide')	;
}

function closePopUp(){
	
}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblDispatch').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblDispatch').deleteRow(1);
			
	}
}
//------------------------------------------------------------
function checkAlreadySelected(){
	var rowCount =document.getElementById('tblDispatchPopup').rows.length;
	//alert(rowCount);
	for(var i=1;i<rowCount;i++)
	{
				var sampleNo2	 = 	document.getElementById('tblDispatchPopup').rows[i].cells[1].innerHTML;
				var revNo2	 	 = 	document.getElementById('tblDispatchPopup').rows[i].cells[2].innerHTML;
				var combo2		 = 	document.getElementById('tblDispatchPopup').rows[i].cells[4].innerHTML;
				var partId2		 = 	document.getElementById('tblDispatchPopup').rows[i].cells[5].id;
				var typeId2		 = 	document.getElementById('tblDispatchPopup').rows[i].cells[6].id;
				var grade2		 = 	document.getElementById('tblDispatchPopup').rows[i].cells[7].innerHTML;
				var size2		 = 	document.getElementById('tblDispatchPopup').rows[i].cells[8].innerHTML;
				var deliveryDate2= 	document.getElementById('tblDispatchPopup').rows[i].cells[9].innerHTML;
		
			var rowCount1 =  document.getElementById('tblDispatch').rows.length;
			for(var j=1;j<rowCount1;j++)
			{		
					var sampleNo	 = 	document.getElementById('tblDispatch').rows[j].cells[1].innerHTML;
					var revNo	 	 = 	document.getElementById('tblDispatch').rows[j].cells[2].innerHTML;
					var combo		 = 	document.getElementById('tblDispatch').rows[j].cells[4].innerHTML;
					var partId		 = 	document.getElementById('tblDispatch').rows[j].cells[5].id;
					var typeId		 = 	document.getElementById('tblDispatch').rows[j].cells[6].id;
					var grade		 = 	document.getElementById('tblDispatch').rows[j].cells[7].innerHTML;
					var size		 = 	document.getElementById('tblDispatch').rows[j].cells[8].innerHTML;
					var deliveryDate = 	document.getElementById('tblDispatch').rows[j].cells[9].innerHTML;
				
				if((sampleNo==sampleNo2) && (revNo==revNo2) && (combo==combo2) && (partId==partId2)&& (typeId==typeId2)&& (grade==grade2)&& (size==size2)&& (deliveryDate==deliveryDate2)){
					//alert(size2 + '' + size);
					document.getElementById('tblDispatchPopup').rows[i].cells[0].childNodes[0].checked=true;
					document.getElementById('tblDispatchPopup').rows[i].cells[0].childNodes[0].disabled=true;
					break;
				}
			}
	}
}

function loadCustLocationCombo(){
	var url 		= basePath+"sampleDispatch-db-get.php?requestType=loadCustLocationCombo";
	var customer=$('#cboDispatchTo').val();
	
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"&customer="+customer,
			async:false,
			success:function(json){
					$('#cboLocation').html(json.cboCustLocations);
			}
		});
}

function loadAttentionToAndLocation(sampleNo,revNo,graphicNo,combo,partId){
	var url 		= basePath+"sampleDispatch-db-get.php?requestType=loadAttentionToAndLocation";
	
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"&sampleNo="+sampleNo+"&revNo="+revNo+"&graphicNo="+graphicNo+"&combo="+combo+"&partId="+partId,
			async:false,
			success:function(json){
					$('#txtAttentionTo').val(json.attentionTo);
					$('#cboLocation').val(json.location);
					$('#txtNote').val(json.remarks);
			}
		});
}

function loadPopupGrid()
{
	$("#tblDispatchPopup tr:gt(0)").remove();
	var graphicNo	= $('#cboGraphicNo').val();
	var url 		= basePath+"sampleDispatch-db-get.php?requestType=loadPopupGrid";
	
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:"graphicNo="+graphicNo, 
		async:false,
		success:function(json){

			var length = json.arrCombo.length;
			var arrCombo = json.arrCombo;
			//alert(arrCombo[0]['sampleNo']);
			for(var i=0;i<length;i++)
			{
				var sampleNo=arrCombo[i]['sampleNo'];	
				var sampleYear=arrCombo[i]['sampleYear'];	
				var revNo=arrCombo[i]['revNo'];	
				var sampleOrderNo=arrCombo[i]['sampleOrderNo'];	
				var sampleOrderYear=arrCombo[i]['sampleOrderYear'];
				var graphicRefNo=arrCombo[i]['graphicRefNo'];	
				var combo=arrCombo[i]['combo'];	
				var partId=arrCombo[i]['partId'];	
				var partName=arrCombo[i]['partName'];	
				var sampleTypeId=arrCombo[i]['sampleTypeId'];
				var sampleType=arrCombo[i]['sampleType'];	
				var intGrade=arrCombo[i]['intGrade'];	
				var strSize=arrCombo[i]['strSize'];	
				var requiredDate=arrCombo[i]['requiredDate'];	
				
			//	var qty=arrCombo[i]['qty'];	
				var balQty=parseFloat(arrCombo[i]['balQty']);	
			//	var nonstckConfGpQty=parseFloat(arrCombo[i]['nonstckConfGpQty']);	
			//	balQty=balQty-nonstckConfGpQty;
					
			  var content='<tr class="normalfnt data">';
              content +='<td align="center" bgcolor="#FFFFFF"><input type="checkbox" id="chkDisp" /></td>';
              content +='<td bgcolor="#FFFFFF" >'+sampleNo+'/'+sampleYear+'</td>';
              content +='<td bgcolor="#FFFFFF" class="normalfntMid" >'+revNo+'</td>';
			  content +='<td bgcolor="#FFFFFF" >'+sampleOrderNo+'/'+sampleOrderYear+'</td>';
              content +='<td bgcolor="#FFFFFF" >'+graphicRefNo+'</td>';
              content +='<td bgcolor="#FFFFFF" >'+combo+'</td>';
              content +='<td bgcolor="#FFFFFF" id="'+partId+'">'+partName+'</td>';
              content +='<td bgcolor="#FFFFFF" id="'+sampleTypeId+'" >'+sampleType+'</td>';
              content +='<td bgcolor="#FFFFFF" >'+intGrade+'</td>';
              content +='<td bgcolor="#FFFFFF" >'+strSize+'</td>';
              content +='<td bgcolor="#FFFFFF" class="normalfntMid" >'+requiredDate+'</td>';
              content +='<td bgcolor="#FFFFFF" class="normalfntRight" style="color:#00C" >'+balQty+'</td></tr>';
			  add_new_row('#frmSampleDispatchPopup #tblDispatchPopup',content);
				
				
			}
			
		}
		
	});

}


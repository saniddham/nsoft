<?php 
$backwardseperator 	= "../";
$userId 	 		= (isset($_SESSION["userId"])?$_SESSION["userId"]:'');
$mainDb				= (isset($_SESSION["mainDatabase"])?$_SESSION["mainDatabase"]:'');
$userType			= (isset($_SESSION["iPadUserType"])?$_SESSION["iPadUserType"]:'');
$requestType 		= base64_decode($_REQUEST['req_t']);

include_once "../dataAccess/ConnectoriPad.php";
include_once "../libraries/fusionChart/Includes/FusionCharts.php";
include_once "../class/ipad/sample/cls_sampleindex_get.php";

$obj_index_get		= new Cls_sampleindex_get($db);

if($requestType=='getDetails')
{
	$monthStart					= date('Y-m-d', strtotime('first day of this month',time()));
	$monthEnd					= date('Y-m-d', strtotime('last day of this month',time()));
	
	$nextMontStart				= date('Y-m-01', strtotime("+ 1 month",time()));
	$nextMontEnd				= date('Y-m-t', strtotime("+ 1 month",time()));
	
	$sumarryArr					= getRequisitionSummary($monthStart,$monthEnd,$userId,$mainDb,$userType);
	
	$totRequisition 			= $sumarryArr['totReqQty'];
	$totFabIn					= $sumarryArr['totFabInQty'];
	$totRecieved				= $sumarryArr['totDispatchQty'];
	
	$response['totRequisition']	= $totRequisition;
	$response['totFabIn']		= $totFabIn;
	$response['totRecieved']	= $totRecieved;
	
	$response['arrDetailData']	= getNewsSticker($userId,$userType,$mainDb);
	
	$strXMLP  = "<chart caption=\"Sample Requisition Summary - ".date('Y').' / '.date('F')."\" palette=\"1\" animation=\"1\" formatnumberscale=\"0\" numberprefix=\"\" pieslicedepth=\"20\" startingangle=\"125\" showLegend=\"1\" bgcolor=\"#3C659D\">";
	$strXMLP .= "<set label=\"Requisition\" value=\"$totRequisition\" color=\"#CDCD00\" />";
	$strXMLP .= "<set label=\"Fabric In\" value=\"$totFabIn\" issliced=\"0\" color=\"#008B8B\" />";
	$strXMLP .= "<set label=\"Received\" value=\"$totRecieved\" issliced=\"0\" color=\"#8B0A50\" />";
	$strXMLP .= "<styles>";
	$strXMLP .= "<definition>";
	$strXMLP .= "<style name='myCaptionFont' type='font' font='Arial' size='16' color='#FFFFFF' bold='1'/>";
	$strXMLP .= "<style name='myLabelsFont' type='font' font='Arial' size='12' color='#FFFFFF' bold='0'/>";
	$strXMLP .= "<style name='myValueFont' type='font' font='Arial' size='12' color='#000000' bold='0'/>";
	$strXMLP .= "</definition>";
	$strXMLP .= "<application>";
	$strXMLP .= "<apply toObject='Caption' styles='myCaptionFont' />";
	$strXMLP .= "<apply toObject='DataLabels' styles='myLabelsFont' />";
	$strXMLP .= "<apply toObject='DataValues' styles='myValueFont' />";
	$strXMLP .= "</application>";
	$strXMLP .= "</styles>";
	$strXMLP .= "</chart>";
	
	$response['peiChart'] 		= $strXMLP;
	
	$strXMLB = "<chart yAxisMaxValue='1000' palette='4' formatNumberScale='0' caption=\"Sample Requisition Summary\" xAxisName=\"\" yAxisName=\"\" numberPrefix=\"\" bgColor='#4D23B5'> ";
	
	$currentMonth = date('F');
	
	$strXMLB .= "<categories>";
	
	for($i=4;$i>=0;$i--)
	{
		$month	  = date('M',strtotime("-".$i." month"));
		$strXMLB .= "<category label='$month'/>";
	}
	
	$nextMonth	  = date('M',strtotime("".date('F')."+ 1 month"));
		$strXMLB .= "<category label='$nextMonth'/>";
	
	$strXMLB .= "</categories>";
	
	$strXMLB .= "<dataset seriesName='Requisition' showValues='0' color=\"#CDCD00\">";
	for($i=4;$i>=0;$i--)
	{
		$start			= date('Y-m-01', strtotime('-'.$i.' month',time()));
		$end			= date('Y-m-t', strtotime('-'.$i.' month',time()));
		
		$sumarryArr		= getRequisitionSummary($start,$end,$userId,$mainDb,$userType);
		$totReqQty		= $sumarryArr['totReqQty'];
		
		$strXMLB   .= "<set value='$totReqQty' color=\"#CDCD00\" />";
	}
	
	$sumarryArr	= getRequisitionSummary($nextMontStart,$nextMontEnd,$userId,$mainDb,$userType);
	$totReqQty	= $sumarryArr['totReqQty'];
	$strXMLB .= "<set value='$totReqQty' color=\"#CDCD00\" />";
	$strXMLB .= "</dataset>";
	
	$strXMLB .= "<dataset seriesName='Fabric In' showValues='0' color=\"#008B8B\">";
	for($i=4;$i>=0;$i--)
	{
		$start			= date('Y-m-01', strtotime('-'.$i.' month',time()));
		$end			= date('Y-m-t', strtotime('-'.$i.' month',time()));
		
		$sumarryArr		= getRequisitionSummary($start,$end,$userId,$mainDb,$userType);
		$totFabInQty	= $sumarryArr['totFabInQty'];
		
		$strXMLB .= "<set value='$totFabInQty' color=\"#008B8B\" />";
	}
	
	$sumarryArr			= getRequisitionSummary($nextMontStart,$nextMontEnd,$userId,$mainDb,$userType);
	$totFabInQty		= $sumarryArr['totFabInQty'];
	$strXMLB .= "<set value='$totFabInQty' color=\"#008B8B\" />";
	$strXMLB .= "</dataset>";
	
	$strXMLB .= "<dataset seriesName='Received' showValues='0' color=\"#8B0A50\">";
	for($i=4;$i>=0;$i--)
	{
		$start			= date('Y-m-01', strtotime('-'.$i.' month',time()));
		$end			= date('Y-m-t', strtotime('-'.$i.' month',time()));
		
		$sumarryArr		= getRequisitionSummary($start,$end,$userId,$mainDb,$userType);
		$totReceiveQty	= $sumarryArr['totDispatchQty'];
		
		$strXMLB .= "<set value='$totReceiveQty' color=\"#8B0A50\" />";
	}
	$sumarryArr			= getRequisitionSummary($nextMontStart,$nextMontEnd,$userId,$mainDb,$userType);
	$totReceiveQty		= $sumarryArr['totDispatchQty'];
	$strXMLB .= "<set value='$totReceiveQty' color=\"#8B0A50\" />";
	$strXMLB .= "</dataset>";
	
	$strXMLB .= "<styles>";
	$strXMLB .= "<definition>";
	$strXMLB .= "<style name='myCaptionFont' type='font' font='Arial' size='16' color='#FFFFFF' bold='1'/>";
	$strXMLB .= "<style name='myLabelsFont' type='font' font='Arial' size='14' color='#FFFFFF' bold='0'/>";
	$strXMLB .= "<style name='myValueFont' type='font' font='Arial' size='14' color='#000000' bold='1'/>";
	$strXMLB .= "<style name='myYaxisFont' type='font' font='Arial' size='14' color='#FFFFFF' bold='0'/>";		
	$strXMLB .= "</definition>";
	$strXMLB .= "<application>";
	$strXMLB .= "<apply toObject='Caption' styles='myCaptionFont' />";
	$strXMLB .= "<apply toObject='DataLabels' styles='myLabelsFont' />";
	$strXMLB .= "<apply toObject='DataValues' styles='myValueFont' />";
	$strXMLB .= "<apply toObject='YAxisValues' styles='myYaxisFont' />";
	$strXMLB .= "</application>";
	$strXMLB .= "</styles>";
	$strXMLB .= "</chart>";
	
	$response['barChart'] 	= $strXMLB;
	echo json_encode($response);
}
function getRequisitionSummary($monthStart,$monthEnd,$userId,$mainDb,$userType)
{
	global $db;
	global $obj_index_get;
	
	$smmarryArr		= $obj_index_get->getRequisitionSummary($monthStart,$monthEnd,$userId,$mainDb,$userType);
	return $smmarryArr;
	
}
function getNewsSticker($userId,$userType,$mainDb)
{
	global $obj_index_get;
	
	$result		= $obj_index_get->getNewsSticker($userId,$userType,$mainDb);
	while($row=mysqli_fetch_array($result))
	{
		$data['graphicNo']		= $row['GRAPHIC_NO'];
		$data['transacDate']	= $row['transacDate'];
		$data['transacType']	= $row['transacType'];
		
		$arrDetailData[] 		= $data;
	}
	return $arrDetailData;
}
?>
<?php
$backwardseperator		= '../';
$thisFilePath 			= (isset($_SESSION['PHP_SELF'])?$_SESSION['PHP_SELF']:'');
$locationId				= (isset($_SESSION['CompanyID'])?$_SESSION['CompanyID']:'');
$companyId				= (isset($_SESSION['headCompanyId'])?$_SESSION['headCompanyId']:'');
$userId					= (isset($_SESSION['userId'])?$_SESSION['userId']:'');
$mainDb					= (isset($_SESSION['mainDatabase'])?$_SESSION['mainDatabase']:'');
$userType				= (isset($_SESSION['iPadUserType'])?$_SESSION['iPadUserType']:'');

include_once "../dataAccess/ConnectoriPad.php";
include_once "../class/customerAndOperation/sample/sampleRequisition/cls_sample_requisition_get.php";
$tot_recCount			= totalRecCount($userType);

$obj_requisition_get	= new Cls_sample_requisition_get($db);

ob_start();
?>
<head>
<title>Sample Requisition Listing</title>
<link rel="stylesheet" type="text/css" href="../../../../../css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="../../../../../css/button.css"/>
<link rel="stylesheet" type="text/css" href="../../../../../css/promt.css"/>
<style type="text/css">
.content {
	background: #eeeeee url(../ipad_images/bg.jpg) top left repeat;
}
html, body {
	height: 100%;
}
body {
	margin: 0;
	padding: 0;
	background: #000000;
}
#progress {
	width: 100px;
	border: 1px solid black;
	position: relative;
	padding: 1px;
}
#percent {
	position: absolute;
	left: 50%;
}
.recordCount {
	font-family: Verdana;
	font-size: 10px;
	color: #CCC;
}
</style>
<script src="../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>
<body>
<form id="frmSampleRequestListing" name="frmSampleRequestListing" autocomplete="off" method="post">
  <table width="100%" class="tableBorder_allRound" height="100%" bgcolor="#FFFFFF" border="0" cellspacing="1" cellpadding="0">
    <tr>
      <td height="10%" colspan="2"><?php include '../presentation/customerAndOperation/sample/ipad/dashBoard/header.php'; ?></td>
      <script type="application/javascript" src="../presentation/customerAndOperation/sample/ipad/requitition_listing/rpt_requisition.js"></script> 
    </tr>
    <tr>
      <td ><table width="100%" height="100%" class="tableBorder_allRound content" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table width="100%" border="0" align="center">
                <tr>
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordered" id="tblMain" bgcolor="#FFFFFF">
                      <thead>
                        <tr>
                          <th colspan="11">Requisition Listing<div style="float:right"><a class="button white medium" id="cls_search">Search</a></div></th>
                        </tr>
                        <tr>
                          <th width="8%">Requisition No</th>
                          <th width="12%">Graphic</th>
                          <th width="16%">Customer</th>
                          <th width="12%">Marketer</th>
                          <th width="8%">Progress</th>
                          <th width="6%">Req Qty</th>
                          <th width="9%"> Sample Order Qty</th>
                          <th width="7%">Fabric In Qty</th>
                          <th width="6%">Dispatch Qty</th>
                          <th width="7%">Request By</th>
                          <th width="9%">Request Date</th>
                        </tr>
                        <tr>
                          <th><input type="text" name="txtReqId" id="txtReqId" style="width:95px"/></th>
                          <th><input type="text" name="txtGraphic" id="txtGraphic" style="width:100%"/></th>
                          <th><input type="text" name="txtCustomer" id="txtCustomer" style="width:100%"/></th>
                          <th><input type="text" name="txtMarketer" id="txtMarketer" style="width:100%"/></th>
                          <th>&nbsp;</th>
                          <th>&nbsp;</th>
                          <th>&nbsp;</th>
                          <th>&nbsp;</th>
                          <th>&nbsp;</th>
                          <th>&nbsp;</th>
                          <th>&nbsp;</th>
                        </tr>
                      </thead>
                      <tbody>
        <?php
		$progressColor = '';
		$result = $obj_requisition_get->getRequisitionListing('',$mainDb,'LIMIT 20',$userType);
		while($row = mysqli_fetch_array($result))
		{
			$graphic = $row["GRAPHIC"];
			
			if($row["DISPATCH_QTY"]<=0)
				$progress 	= 0;
			else
				$progress 	= round(($row["DISPATCH_QTY"]/$row["SAMPLE_ORDER_QTY"])*100);
			
			if(strlen($row["GRAPHIC"])>25)
				$graphic	= substr($row["GRAPHIC"],0,25).'...';
		?>
                      <tr>
                        <td><a target="requisition" href="?q=requisition&rs_i=<?php echo base64_encode($row["REQUISITION_NO"])?>&rs_y=<?php echo base64_encode($row["REQUISITION_YEAR"])?>"><?php echo $row["CONCAT_REQU_NO"]?></a></td>
                        <td><?php echo $graphic?></td>
                        <td><?php echo $row["CUSTOMER_NAME"]?></td>
                        <td><?php echo $row["MARKETER"]?></td>
                        <td nowrap="nowrap"><div id="progress"><span id="percent"><?php echo $progress?>%</span>
                            <div style="width:<?php echo $progress?>px;height:15px;background-color:<?php echo ($progressColor=='red'?'#E81414':'#00CC00')?>;"></div>
                          </div></td>
                        <td align="right"><?php echo number_format($row["REQUISITION_QTY"])?></td>
                        <td align="right"><?php echo number_format($row["SAMPLE_ORDER_QTY"])?></td>
                        <td align="right"><?php echo number_format($row["FABRIC_IN_QTY"])?></td>
                        <td align="right"><?php echo number_format($row["DISPATCH_QTY"])?></td>
                        <td><?php echo $row["REQUEST_BY"]?></td>
                        <td align="center" nowrap="nowrap"><?php echo $row["REQUEST_DATE"]?></td>
                      </tr>
                      <?php
		}
		?>
        </tbody>
                    </table></td>
                </tr>
                <tr>
                  <td class="recordCount">Records <?php echo ($tot_recCount<20)? $tot_recCount:20?> of <?php echo $tot_recCount?></td>
                </tr>
              </table></td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
</body>
<?php
$body = ob_get_clean();
echo $body;

function totalRecCount($userType)
{
	global $db;
	if($userType=='M')
		$para .= "AND RD.SAMPLE_TYPE IN (6)";
	else
		$para .= "AND RD.SAMPLE_TYPE NOT IN (6)";
			
	/*$sql = "SELECT
			  COUNT(*) AS COUNTA
			FROM trn_sample_requisition_header limit 20";*/
	$sql = "SELECT
			  RH.REQUISITION_NO
			FROM trn_sample_requisition_header RH
			  INNER JOIN trn_sample_requisition_detail RD
				ON RD.REQUISITION_NO = RH.REQUISITION_NO
				  AND RD.REQUISITION_YEAR = RH.REQUISITION_YEAR
			WHERE 1 = 1
				$para
			GROUP BY RH.REQUISITION_NO,RH.REQUISITION_YEAR";
	$result = $db->RunQuery($sql);
	$rowCount	= mysqli_num_rows($result);
	return $rowCount;
}
?>

<?php
$backwardseperator 		= "../";
$userId 				= (isset($_SESSION['userId'])?$_SESSION['userId']:'');
$locationId	  			= (isset($_SESSION['CompanyID'])?$_SESSION['CompanyID']:'');
$companyId				= (isset($_SESSION['headCompanyId'])?$_SESSION['headCompanyId']:'');
$mainDb					= (isset($_SESSION['mainDatabase'])?$_SESSION['mainDatabase']:'');
$userType				= (isset($_SESSION['iPadUserType'])?$_SESSION['iPadUserType']:'');

$requestType 			= base64_decode($_REQUEST['req_t']);

include_once "../dataAccess/ConnectoriPad.php";
include_once "../class/customerAndOperation/sample/sampleRequisition/cls_sample_requisition_get.php";

$obj_requisition_get	= new Cls_sample_requisition_get($db);

if($requestType=='URLSearch')
{
	$header_array 			= json_decode(base64_decode($_REQUEST['he_ar']),true);
	$result = $obj_requisition_get->getRequisitionListing($header_array,$mainDb,'',$userType);
	while($row = mysqli_fetch_array($result))
	{
		$progress = round(($row["DISPATCH_QTY"]/$row["SAMPLE_ORDER_QTY"])*100);
		$graphic  = $row["GRAPHIC"];
		
		if(strlen($row["GRAPHIC"])>25)
			$graphic	= substr($row["GRAPHIC"],0,25).'...';
		$data .= "<tr>".
					"<td><a target=\"requisition\" href='?q=requisition&rs_i=".base64_encode($row["REQUISITION_NO"])."&rs_y=".base64_encode($row["REQUISITION_YEAR"])."'>".$row["CONCAT_REQU_NO"]."</a></td>".
					"<td>".$graphic."</td>".
					"<td>".$row["CUSTOMER_NAME"]."</td>".
					"<td>".$row["MARKETER"]."</td>".
					"<td nowrap=\"nowrap\"><div id=\"progress\"><span id=\"percent\">".$progress."%</span>".
						"<div style='width:".$progress."px;height:15px;background-color:".($progressColor=='red'?'#E81414':'#00CC00')."'></div>".
			  			"</div></td>".
					"<td align=\"right\">".number_format($row["REQUISITION_QTY"])."</td>".
					"<td align=\"right\">".number_format($row["SAMPLE_ORDER_QTY"])."</td>".
					"<td align=\"right\">".number_format($row["FABRIC_IN_QTY"])."</td>".
					"<td align=\"right\">".number_format($row["DISPATCH_QTY"])."</td>".
					"<td>".$row["REQUEST_BY"]."</td>".
					"<td align=\"center\" nowrap=\"nowrap\">".$row["REQUEST_DATE"]."</td>".
		  		"</tr>";
	}
	$response['Data']	= $data;
	echo json_encode($response);
}
?>
<?php
$requisitionNo      = $_REQUEST['reqNo'];
$requisitionYear    = $_REQUEST['reqYear'];
$graphic    		= $_REQUEST['graphic'];
$customer    		= $_REQUEST['customer'];
$style    			= $_REQUEST['style'];
$brand    			= $_REQUEST['brand'];
$qty    			= $_REQUEST['qty'];
$reqDate    		= $_REQUEST['reqDate'];
$reciverName		= $_REQUEST['toName'];
$senderName			= $_REQUEST['fromName'];
$link				= base64_decode($_REQUEST['link']);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add Sample Requisition Email</title>
<style type="text/css">
.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntWhite {
	font-family: Verdana;
	font-size: 11px;
	color: #FFF;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.sampleNo{
	color: #039; font-weight: bold; font-size: 12px; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;	
}
.part{
	color: #096CBD;
	font-weight: bold;
	font-size: 13px;
	font-family: "Comic Sans MS", cursive;
}
.tableBorder_allRound{
	
	border: 1px solid #CCCCCC;
/*	-moz-border-radius-bottomright:10px;
	-moz-border-radius-bottomleft:10px;
	-moz-border-radius-topright:10px;
	-moz-border-radius-topleft:10px;*/
	
	border-radius:10px 10px 10px 10px;
}
.normalfnt1 {	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfnt1 {	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
</style>
</head>

<body>
<table  width="595" border="0" cellspacing="2" cellpadding="0" class="tableBorder_allRound" style="border-color:#025077">
  <tr>
    <td colspan="3" class="normalfnt">&nbsp;</td>
  </tr>
  <tr>
    <td width="150" class="normalfnt">Dear <strong><?php echo $reciverName; ?></strong>,</td>
    <td width="492">&nbsp;</td>
    <td width="4">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><span class="normalfnt1">I have added <strong>a Sample Requisition</strong> to NSOFT system. Please refer the below link to view sample information sheet.</span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">click <a  href="<?php  echo $link ?>">here</a> to view sample information sheet.</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="normalfnt">
    <td>&nbsp;</td>
    <td colspan="2"><table width="100%" border="0" cellpadding="3" class="tableBorder_allRound" style="border-color:#008B45">
      <tr class="normalfnt">
        <td width="30%">Graphic Ref No</td>
        <td width="70%"><?php echo $graphic; ?></td>
      </tr>
      <tr class="normalfnt">
        <td>Customer</td>
        <td><?php echo $customer; ?></td>
      </tr>
      <tr class="normalfnt">
        <td>Style No</td>
        <td><?php echo $style; ?></td>
      </tr>
      <tr class="normalfnt">
        <td>Brand</td>
        <td><?php echo $brand; ?></td>
      </tr>
      <tr class="normalfnt">
        <td colspan="2"><table width="100%" border="0" cellpadding="0">
          <tr bgcolor="#9AC0CD" class="normalfnt">
            <th style="text-align:center;height:20px">Sample Type</th>
            <th style="text-align:center">Sample Qty</th>
            <th style="text-align:center">Required Date</th>
          </tr>
          <tr class="normalfnt">
            <td>1st Submission</td>
            <td style="text-align:right"><?php echo $qty; ?></td>
            <td style="text-align:center"><?php echo $reqDate; ?></td>
          </tr>
        </table></td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3"></td>
  </tr>
  <tr>
    <td class="normalfnt">Thanks,</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="normalfnt"><strong><?php echo $senderName; ?></strong><br />
    ...................</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="normalfnt">(This is a <strong><span style="color:#025077">NSOFT</span> </strong>system generated email.)</td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
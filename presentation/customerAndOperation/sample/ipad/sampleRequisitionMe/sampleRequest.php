<?php
$backwardseperator		= '../';
$thisFilePath 			= $_SERVER['PHP_SELF'];
$locationId				= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];
$userId					= $_SESSION["userId"];
$mainDb					= $_SESSION['mainDatabase'];
$userType				= $_SESSION['iPadUserType'];

include_once "../dataAccess/ConnectoriPad.php";
include_once "../class/customerAndOperation/sample/sampleRequisition/cls_sample_requisition_get.php";
include_once "../class/cls_commonFunctions_get.php";
//include_once "../class/cls_commonErrorHandeling_get.php";

$obj_requisition_get	= new Cls_sample_requisition_get($db);
//$obj_commonErrHandle	= new cls_commonErrorHandeling_get($db);
$obj_common				= new cls_commonFunctions_get($db);

$programCode			= 'P0827';
$requisitionNo			= (isset($_REQUEST["rs_i"])?$_REQUEST["rs_i"]:'');
$requisitionYear		= (isset($_REQUEST["rs_y"])?$_REQUEST["rs_y"]:'');

$header_arr				= $obj_requisition_get->loadHeaderData(base64_decode($requisitionNo),base64_decode($requisitionYear),'RunQuery');
$detail_result			= $obj_requisition_get->loadDetailData(base64_decode($requisitionNo),base64_decode($requisitionYear),'RunQuery');
$rowD					= mysqli_fetch_array($detail_result);
$intStatus				= $header_arr['STATUS'];
$levels					= $header_arr['APPROVE_LEVELS'];

//$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_save($intStatus,$levels,$userId,$programCode,'RunQuery');
//$permision_save			= $permition_arr['permision'];

$permision_save			= $obj_requisition_get->getPermission($userId,$programCode,'intEdit',$mainDb,'RunQuery');
$permision_cancel		= $obj_requisition_get->getPermission($userId,$programCode,'intCancel',$mainDb,'RunQuery');

//$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_cancel($intStatus,$levels,$userId,$programCode,'RunQuery');
//$permision_cancel		= $permition_arr['permision'];

//$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$levels,$userId,$programCode,'RunQuery');
//$permision_confirm		= $permition_arr['permision'];

ob_start();
?>
<head>
<title>Sample Requisition</title>

<link rel="stylesheet" type="text/css" href="../../../../../css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="../../../../../css/button.css"/>
<link rel="stylesheet" type="text/css" href="../../../../../css/promt.css"/>

<style type="text/css">
.content
{
	background: #eeeeee url(../ipad_images/bg.jpg) top left repeat;
}
html,body{
	height:100%;
}
body{
  margin:0;
  padding:0;
  background:#000000;
}
</style>

<script src="../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../libraries/calendar/runCalender.js" type="text/javascript"></script>

</head>
<body>
<form id="frmSampleRequest" name="frmSampleRequest" autocomplete="off" method="post">
<table width="100%" class="tableBorder_allRound" height="100%" bgcolor="#FFFFFF" border="0" cellspacing="1" cellpadding="0">
  <tr>
    <td height="10%" colspan="2"><?php include '../presentation/customerAndOperation/sample/ipad/dashBoard/header.php'; ?></td>
    <script type="application/javascript" src="../presentation/customerAndOperation/sample/ipad/sampleRequisitionMe/sampleRequest_js.js"></script>
  </tr>
  <tr>
    <td >
        <table width="100%" height="100%" class="tableBorder_allRound content" border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td valign="top">
            <div align="center">
	<div class="trans_layoutD">
	<div class="trans_text">Sample Requisition</div>
	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <tr>
    	<td>
        <table width="100%" border="0" id="tblDetail" > 
            <tr style="display:none" class="clsRequisiNo">
              <td class="normalfnt">Requisition No</td>
              <td><input name="txtRequisitionNo" type="text" disabled="disabled" id="txtRequisitionNo" style="width:80px" value="<?php echo base64_decode($requisitionNo); ?>" />&nbsp;<input name="txtRequisitionYear" type="text" disabled="disabled" id="txtRequisitionYear" style="width:50px" value="<?php echo base64_decode($requisitionYear); ?>" /></td>
            </tr>
            <tr>
                <td width="24%" class="normalfnt">Graphic Ref No <span class="compulsoryRed">*</span></td>
                <td width="76%"><input value="<?php echo htmlentities($header_arr['GRAPHIC']); ?>" name="txtGraphicRefNo" type="text" class="validate[required,maxSize[50]]" id="txtGraphicRefNo" style="width:400px;text-transform:uppercase" /></td>
            </tr>
            <tr>
            	<td class="normalfnt">Customer <span class="compulsoryRed">*</span></td>
            	<td><select name="cboCustomer" id="cboCustomer" class="validate[required]" style="width:400px">
                <option value=""></option>
                <?php
					$result = $obj_requisition_get->getCustomer($userId,$mainDb);
					$count	= mysqli_num_rows($result);
					while($row = mysqli_fetch_array($result))
					{
						if($header_arr['CUSTOMER']==$row['intId'])
							echo "<option selected value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else if($count==1)
							echo "<option selected value=\"".$row['intId']."\">".$row['strName']."</option>";
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
                ?>
                </select></td>
            </tr>
            <tr>
            	<td class="normalfnt">Style No</td>
            	<td><input value="<?php echo htmlentities($header_arr['STYLE']); ?>"  name="txtStyleNo" type="text" class="validate[maxSize[50]]"  id="txtStyleNo" style="width:400px" /></td>
            </tr>
            <tr>
            	<td class="normalfnt">Brand <span class="compulsoryRed">*</span></td>
           		<td><select class="validate[required]" name="cboBrand" id="cboBrand" style="width:400px">
                <option value=""></option>
                 <?php
					$result = $obj_requisition_get->getBrand($header_arr['CUSTOMER'],$mainDb);
					while($row = mysqli_fetch_array($result))
					{
						if($header_arr['BRAND']==$row['intId'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
                ?>
           		</select></td>
            </tr>
            <tr>
            	<td class="normalfnt">Marketer <span class="compulsoryRed">*</span></td>
            	<td ><select name="cboMarketer" id="cboMarketer" class="validate[required]" style="width:400px">
            	<option value=""></option>
				<?php
                  
                    $result = $obj_requisition_get->getMarketer($mainDb);
					$count	= mysqli_num_rows($result);
                    while($row = mysqli_fetch_array($result))
                    {
                        if($header_arr['MARKETER']==$row['USER_ID'])
                            echo "<option selected=\"selected\" value=\"".$row['USER_ID']."\">".$row['strUserName']."</option>";	
						else if($count==1)
							echo "<option selected=\"selected\" value=\"".$row['USER_ID']."\">".$row['strUserName']."</option>";
                        else
                            echo "<option value=\"".$row['USER_ID']."\">".$row['strUserName']."</option>";	
                    }
                ?>
            </select></td>
            </tr>
            <tr>
              <td class="normalfnt">Washing Stand</td>
              <td ><select name="cboWashStand" id="cboWashStand" class="" style="width:400px">
                <option value=""></option>
                <?php
                  
                    $result = $obj_requisition_get->getWashingStand($mainDb);
					$count	= mysqli_num_rows($result);
                    while($row = mysqli_fetch_array($result))
                    {
                        if($header_arr['WASH_STANDERD']==$row['intId'])
                            echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else if($count==1)
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
                        else
                            echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
                    }
                ?>
              </select></td>
            </tr>
            <tr>
            	<td class="normalfnt">Type of Print</td>
            	<td ><select name="cboPrintType" id="cboPrintType" class="" style="width:400px">
            	  <option value=""></option>
            	  <?php
                  
                    $result = $obj_requisition_get->getTypeOfPrint($mainDb);
					$count	= mysqli_num_rows($result);
                    while($row = mysqli_fetch_array($result))
                    {
                        if($header_arr['PRINT_MODE']==$row['intId'])
                            echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else if($count==1)
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
                        else
                            echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
                    }
                ?>
          	  </select></td>
            </tr>
            <tr class="normalfnt">
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="normalfnt">&nbsp; </td>
                <td  class="normalfnt">
                <table  border="0" class="bordered" id="tblSampleType" style="width:400px">
                <thead>
                    <tr>
                      <th width="230">Sample Types</th>
                      <th width="100">Sample Qty</th>
                      <th width="120">Req. Date</th>
                    </tr>
                </thead>
  				<tbody>
                <tr id="6">
                    <td>1st Submission</td>
                    <td><input type="text" name="txtSampleQty" id="txtSampleQty" class="validate[required,custom[number],min[0]] clsSampleQty" style="width:100%;text-align:right" value="<?php echo $rowD['QTY'];?>" /></td>
                    <td><input name="txtReqDate" type="text" value="<?php echo ($rowD["REQUIRED_DATE"]==''? date('Y-m-d'):$rowD["REQUIRED_DATE"]) ?>" class="clsReqDate" id="txtReqDate" style="width:80px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                </tr>		
               
				</tbody>
                </table>
                </td>
			</tr>
		</table></td>
    </tr>
    <tr>
        <td height="32">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                <tr>
                    <td align="center"><a class="button white medium" id="butNew">New</a><a class="button white medium" id="butSave">Save</a><a class="button white medium" id="butCancel" <?php if((base64_decode($requisitionNo)=='')){ ?>  style="display:none"<?php } ?>>Cancel</a><a href="?q=dash" class="button white medium" id="butClose">Close</a></td>
                </tr>
            </table>
        </td>
     </tr>
	</table>
	</div>
</div>
            </td>
            </tr>
        </table>
    </td>
  </tr>
</table>
</form>
</body>
<?php
$body = ob_get_clean();
echo $body;
?>
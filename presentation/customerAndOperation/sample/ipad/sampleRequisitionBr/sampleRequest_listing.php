<?php
//ini_set('display_errors',1);
date_default_timezone_set('Asia/Kolkata');
session_start();
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$intUser  			= $_SESSION["userId"];

$programCode		= 'P0827';

$backwardseperator 	= "../../../../";
require_once "../../../../dataAccess/Connector.php";
require_once("../../../../libraries/jqgrid2/inc/jqgrid_dist.php");

$approveLevel = (int)getMaxApproveLevel();

$sql = "SELECT SUB_1.* FROM
			(SELECT
				CONCAT(tb1.REQUISITION_NO,'/',tb1.REQUISITION_YEAR)	AS CONCAT_REQUISITION_NO,
				tb1.REQUISITION_NO,
				tb1.REQUISITION_YEAR,
				tb1.GRAPHIC,
				tb1.STYLE,
				tb1.CUSTOMER,
				tb1.MARKETER,
				tb1.BRAND,
				MC.strName AS CUSTOMER_NAME,
				MB.strName AS BRAND_NAME,
				SU.strUserName AS MARKETER_NAME,
				tb1.REMARKS,
				tb1.APPROVE_LEVELS,
				tb1.COMPANY_ID,
				tb1.CREATED_BY,
				tb1.CREATED_DATE,
				if(tb1.STATUS=1,'Approved',if(tb1.STATUS=0,'Rejected',if(tb1.STATUS=-1,'Revised',if(tb1.STATUS=-2,'Cancelled','Pending')))) as Status,
				sys_users.strUserName AS CREATOR,
				(SELECT SUM(QTY) AS TOT_QTY
				FROM trn_sample_requisition_detail SRD
				WHERE SRD.REQUISITION_NO = tb1.REQUISITION_NO AND
				SRD.REQUISITION_YEAR = tb1.REQUISITION_YEAR) AS TOT_QTY,
				(SELECT SUM(BAL_QTY) AS TOT_BALQTY
				FROM trn_sample_requisition_detail SRD
				WHERE SRD.REQUISITION_NO = tb1.REQUISITION_NO AND
				SRD.REQUISITION_YEAR = tb1.REQUISITION_YEAR) AS TOT_BALQTY,
				(SELECT IFNULL(SUM(DISPATCH_QTY),0) AS TOT_DISPATCHQTY
				FROM trn_sample_requisition_detail SRD
				WHERE SRD.REQUISITION_NO = tb1.REQUISITION_NO AND
				SRD.REQUISITION_YEAR = tb1.REQUISITION_YEAR) AS TOT_DISPATCHQTY,
				
							IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(trn_sample_requisition_approveby.APPROVED_DATE),')' )
								FROM
								trn_sample_requisition_approveby
								Inner Join sys_users ON trn_sample_requisition_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								trn_sample_requisition_approveby.REQUISITION_NO  = tb1.REQUISITION_NO AND
								trn_sample_requisition_approveby.REQUISITION_YEAR =  tb1.REQUISITION_YEAR AND
								trn_sample_requisition_approveby.APPROVE_LEVEL_NO = '1' AND
								trn_sample_requisition_approveby.STATUS =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
								$approval	= "2nd_Approval";
							}
							else if($i==3){
								$approval	= "3rd_Approval";
							}
							else {
								$approval	= $i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(trn_sample_requisition_approveby.APPROVED_DATE),')' )
								FROM
								trn_sample_requisition_approveby
								Inner Join sys_users ON trn_sample_requisition_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								trn_sample_requisition_approveby.REQUISITION_NO  = tb1.REQUISITION_NO AND
								trn_sample_requisition_approveby.REQUISITION_YEAR =  tb1.REQUISITION_YEAR AND
								trn_sample_requisition_approveby.APPROVE_LEVEL_NO =  '$i' AND
								trn_sample_requisition_approveby.STATUS =  '0' 
							),
							IF(
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.STATUS>1) AND (tb1.STATUS<=tb1.APPROVE_LEVELS) AND ((SELECT
								concat(sys_users.strUserName )
								FROM
								trn_sample_requisition_approveby
								Inner Join sys_users ON trn_sample_requisition_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								trn_sample_requisition_approveby.REQUISITION_NO  = tb1.REQUISITION_NO AND
								trn_sample_requisition_approveby.REQUISITION_YEAR =  tb1.REQUISITION_YEAR AND
								trn_sample_requisition_approveby.APPROVE_LEVEL_NO =  ($i-1) AND 
								trn_sample_requisition_approveby.STATUS='0' )<>'')),
								
								'Approve',
								 if($i>tb1.APPROVE_LEVELS,'-----',''))
								
								) as `".$approval."`, "; 
									
								}
								
							$sql .= "IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(trn_sample_requisition_approveby.APPROVED_DATE),')' )
								FROM
								trn_sample_requisition_approveby
								Inner Join sys_users ON trn_sample_requisition_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								trn_sample_requisition_approveby.REQUISITION_NO  = tb1.REQUISITION_NO AND
								trn_sample_requisition_approveby.REQUISITION_YEAR =  tb1.REQUISITION_YEAR AND
								trn_sample_requisition_approveby.APPROVE_LEVEL_NO =  '0' AND
								trn_sample_requisition_approveby.STATUS =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS<=tb1.APPROVE_LEVELS AND 
								tb1.STATUS>1),'Reject', '')) as `Reject`,";
								
							$sql .= "IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(trn_sample_requisition_approveby.APPROVED_DATE),')' )
								FROM
								trn_sample_requisition_approveby
								Inner Join sys_users ON trn_sample_requisition_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								trn_sample_requisition_approveby.REQUISITION_NO  = tb1.REQUISITION_NO AND
								trn_sample_requisition_approveby.REQUISITION_YEAR =  tb1.REQUISITION_YEAR AND
								trn_sample_requisition_approveby.APPROVE_LEVEL_NO =  '-2' AND
								trn_sample_requisition_approveby.STATUS =  '0'
							),IF(((SELECT
								menupermision.intCancel 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND 
								tb1.STATUS=1),'Cancel', '')) as `Cancel`,";
							
							$sql .= "IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(trn_sample_requisition_approveby.APPROVED_DATE),')' )
								FROM
								trn_sample_requisition_approveby
								Inner Join sys_users ON trn_sample_requisition_approveby.APPROVED_BY = sys_users.intUserId
								WHERE
								trn_sample_requisition_approveby.REQUISITION_NO  = tb1.REQUISITION_NO AND
								trn_sample_requisition_approveby.REQUISITION_YEAR =  tb1.REQUISITION_YEAR AND
								trn_sample_requisition_approveby.APPROVE_LEVEL_NO =  '-1' AND
								trn_sample_requisition_approveby.STATUS =  '0'
							),IF(((SELECT
								menupermision.intRevise 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND 
								tb1.STATUS=1),'Revise', '')) as `Revise`,";
								
							$sql .= "'View' as `View`   
				
FROM trn_sample_requisition_header as tb1 
				INNER JOIN mst_customer MC ON MC.intId=tb1.CUSTOMER
				INNER JOIN sys_users SU ON SU.intUserId=tb1.MARKETER
				INNER JOIN mst_brand MB ON MB.intId=tb1.BRAND
				INNER JOIN sys_users ON tb1.CREATED_BY = sys_users.intUserId
				WHERE tb1.COMPANY_ID = '$session_companyId'
 
		)  
		AS SUB_1 WHERE 1=1";
				
$jq = new jqgrid('',$db);	
$g 	= new jqgrid();

$col["title"] 				= "Status";
$col["name"] 				= "Status";
$col["width"] 				= "3"; 						
$col["align"] 				= "center";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["stype"] 				= "select";
$str 						= "Pending:Pending;Approved:Approved;Rejected:Rejected;Cancelled:Cancelled;:All" ;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "requisitionNo";
$col["name"] 				= "REQUISITION_NO";
$col["sortable"] 			= true; 					
$col["search"] 				= true;
$col["hidden"]  			= true;				
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "requisitionYear";
$col["name"] 				= "REQUISITION_YEAR";
$col["sortable"] 			= true; 					
$col["search"] 				= true;
$col["hidden"]  			= true;					
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Serial No";
$col["name"] 				= "CONCAT_REQUISITION_NO";
$col["width"] 				= "3";
$col["align"] 				= "center";
$col["sortable"] 			= true; 					
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col['link']				= 'sampleRequest.php?requisitionNo={REQUISITION_NO}&requisitionYear={REQUISITION_YEAR}';
$col["linkoptions"] 		= "target='sampleRequest.php'";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Customer";
$col["name"] 				= "CUSTOMER_NAME";
$col["width"] 				= "6"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["dbname"] 				= "CUSTOMER";
$client_lookup 				= $jq->get_dropdown_values("select intId AS k,strName AS v
														from mst_customer 
														where intStatus = 1 
														order by strName");
$col["stype"] 				= "select";
$str 						= ":;".$client_lookup;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;
$col						= NULL;

$col["title"] 				= "Graphic No";
$col["name"] 				= "GRAPHIC";
$col["width"] 				= "3";
$col["align"] 				= "left";
$col["sortable"] 			= true; 					
$col["search"] 				= true; 					
$col["editable"] 			= false;
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Style No";
$col["name"] 				= "STYLE";
$col["width"] 				= "3";
$col["align"] 				= "left";
$col["sortable"] 			= true; 					
$col["search"] 				= true; 					
$col["editable"] 			= false;
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Brand";
$col["name"] 				= "BRAND_NAME";
$col["width"] 				= "4"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["dbname"] 				= "BRAND";
$client_lookup 				= $jq->get_dropdown_values("SELECT
														mst_brand.intId AS k,
														mst_brand.strName AS v
														FROM
														mst_customer_brand
														INNER JOIN mst_brand ON mst_brand.intId = mst_customer_brand.intBrandId
														WHERE
														mst_brand.intStatus =  '1'
														ORDER BY
														mst_brand.strName ASC");
$col["stype"] 				= "select";
$str 						= ":;".$client_lookup;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;
$col						= NULL;

$col["title"] 				= "Marketer";
$col["name"] 				= "MARKETER_NAME";
$col["width"] 				= "4"; 						
$col["align"] 				= "left";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["dbname"] 				= "MARKETER";
$client_lookup 				= $jq->get_dropdown_values("SELECT mst_marketer.intUserId AS k,sys_users.strUserName AS v
														FROM mst_marketer
														INNER JOIN sys_users ON sys_users.intUserId=mst_marketer.intUserId
														WHERE sys_users.intStatus = 1
														ORDER BY sys_users.strUserName");
$col["stype"] 				= "select";
$str 						= ":;".$client_lookup;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;
$col						= NULL;

$col["title"] 				= "Remarks";
$col["name"] 				= "REMARKS";
$col["width"] 				= "3";
$col["align"] 				= "left";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Qty";
$col["name"] 				= "TOT_QTY";
$col["width"] 				= "2";
$col["align"] 				= "right";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Bal Qty";
$col["name"] 				= "TOT_BALQTY";
$col["width"] 				= "2";
$col["align"] 				= "right";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Dispatch Qty";
$col["name"] 				= "TOT_DISPATCHQTY";
$col["width"] 				= "2";
$col["align"] 				= "right";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "1st Approval";
$col["name"] 				= "1st_Approval";
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= 'rpt_sampleRequest.php?requisitionNo={REQUISITION_NO}&requisitionYear={REQUISITION_YEAR}&mode=Confirm';
$col["linkoptions"] 		= "target='rpt_sampleRequest.php'";
$col['linkName']			= 'Approve';
$cols[] 					= $col;	
$col						= NULL;

for($i=2; $i<=$approveLevel; $i++)
{
	if($i==2){
		$ap		= "2nd Approval";
		$ap1	= "2nd_Approval";
	}
	else if($i==3){
		$ap		= "3rd Approval";
		$ap1	= "3rd_Approval";
	}
	else {
		$ap		= $i."th Approval";
		$ap1	= $i."th_Approval";
	}

$col["title"] 				= $ap; 
$col["name"] 				= $ap1; 
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= 'rpt_sampleRequest.php?requisitionNo={REQUISITION_NO}&requisitionYear={REQUISITION_YEAR}&mode=Confirm';
$col["linkoptions"] 		= "target='rpt_sampleRequest.php'";
$col['linkName']			= 'Approve';
$cols[] 					= $col;	
$col						= NULL;
}

$col["title"] 				= 'Cancel'; 
$col["name"] 				= 'Cancel'; 
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= 'rpt_sampleRequest.php?requisitionNo={REQUISITION_NO}&requisitionYear={REQUISITION_YEAR}&mode=Cancel';
$col["linkoptions"] 		= "target='rpt_sampleRequest.php'";
$col['linkName']			= 'Cancel';
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Report";
$col["name"] 				= "View";
$col["width"] 				= "2";
$col["align"] 				= "center"; 
$col["sortable"]			= false;
$col["editable"] 			= false; 	
$col["search"] 				= false; 
$col['link']				= 'rpt_sampleRequest.php?requisitionNo={REQUISITION_NO}&requisitionYear={REQUISITION_YEAR}';
$col["linkoptions"] 		= "target='rpt_sampleRequest.php'";
$col['linkName']			= 'View';
$cols[] 					= $col;	
$col						= NULL;

$grid["caption"] 			= "Sample Requisition Listing";
$grid["multiselect"] 		= false;
$grid["rowNum"] 			= 20; 
$grid["sortname"] 			= 'REQUISITION_YEAR,REQUISITION_NO'; 
$grid["sortorder"] 			= "DESC"; 
$grid["autowidth"] 			= true; 
$grid["multiselect"] 		= false; 
$grid["search"] 			= true; 
$grid["postData"] 			= array("filters" => $sarr ); 
$grid["export"] 			= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"}
     ]
}
SEARCH_JSON;

$grid["postData"] = array("filters" => $sarr ); 

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sample Requisition Listing</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
</head>
<body>	
<form id="frmlisting" name="frmlisting" method="post" action="">
<table width="100%" border="0" align="center">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $backwardseperator?>libraries/jqdrid/js/themes/smoothness/jquery-ui.custom.css"></link>	
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $backwardseperator?>libraries/jqdrid/js/jqgrid/css/ui.jqgrid.css"></link>	

<script src="<?php echo $backwardseperator?>libraries/jqdrid/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $backwardseperator?>libraries/jqdrid/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script src="<?php echo $backwardseperator?>libraries/jqdrid/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
<script src="<?php echo $backwardseperator?>libraries/jqdrid/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
<script src="<?php echo $backwardseperator?>libraries/javascript/script.js" type="text/javascript"></script>
   <td><table width="100%" border="0">
      <tr>
        <td>
        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
        </td>
      </tr>
    </table></td>
    </tr>
</form>
</body>
</html>
<?php
function getMaxApproveLevel()
{
	global $db;
	$appLevel = 0;
	
	$sqlp = "SELECT
			MAX(trn_sample_requisition_header.APPROVE_LEVELS) AS appLevel
			FROM trn_sample_requisition_header";					
	$resultp 	= $db->RunQuery($sqlp);
	$rowp		= mysqli_fetch_array($resultp);
	return $rowp['appLevel'];
}
?>

<?php
$backwardseperator		= '../';
$thisFilePath 			= (isset($_SESSION["PHP_SELF"])?$_SESSION["PHP_SELF"]:'');
$locationId				= (isset($_SESSION["CompanyID"])?$_SESSION["CompanyID"]:'');
$companyId				= (isset($_SESSION["headCompanyId"])?$_SESSION["headCompanyId"]:'');
$userId					= (isset($_SESSION["userId"])?$_SESSION["userId"]:'');
$mainDb					= (isset($_SESSION["mainDatabase"])?$_SESSION["mainDatabase"]:'');
$userType				= (isset($_SESSION["iPadUserType"])?$_SESSION["iPadUserType"]:'');

include_once "../dataAccess/ConnectoriPad.php";
include_once "../class/customerAndOperation/sample/sampleRequisition/cls_sample_requisition_get.php";
include_once "../class/cls_commonFunctions_get.php";
//include_once "../class/cls_commonErrorHandeling_get.php";

$obj_requisition_get	= new Cls_sample_requisition_get($db);
//$obj_commonErrHandle	= new cls_commonErrorHandeling_get($db);
$obj_common				= new cls_commonFunctions_get($db);

$programCode			= 'P0827';
$requisitionNo			= (isset($_REQUEST["rs_i"])?$_REQUEST["rs_i"]:'');
$requisitionYear		= (isset($_REQUEST["rs_y"])?$_REQUEST["rs_y"]:'');

$concatRequiNo			= base64_decode($requisitionNo).'/'.base64_decode($requisitionYear);

$header_arr				= $obj_requisition_get->loadHeaderData(base64_decode($requisitionNo),base64_decode($requisitionYear),'RunQuery');
$detail_result			= $obj_requisition_get->loadDetailData(base64_decode($requisitionNo),base64_decode($requisitionYear),'RunQuery');

$intStatus				= $header_arr['STATUS'];
$levels					= $header_arr['APPROVE_LEVELS'];

$permision_save			= $obj_requisition_get->getPermission($userId,$programCode,'intEdit',$mainDb,'RunQuery');
$permision_cancel		= $obj_requisition_get->getPermission($userId,$programCode,'intCancel',$mainDb,'RunQuery');

ob_start();
?>

<head>
<title>Sample Requisition</title>

<link rel="stylesheet" type="text/css" href="../../../../../css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="../../../../../css/button.css"/>
<link rel="stylesheet" type="text/css" href="../../../../../css/promt.css"/>
<link rel="stylesheet" href="../libraries/chosen/chosen/chosen.css" />

<style type="text/css">
.content
{
	background: #eeeeee url(../ipad_images/bg.jpg) top left repeat;
}
html,body{
	height:100%;
}
body{
  margin:0;
  padding:0;
  background:#000000;
}
</style>

<script src="../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../libraries/calendar/runCalender.js" type="text/javascript"></script>

</head>
<body >
<form id="frmSampleRequest" name="frmSampleRequest" autocomplete="off" method="post">
<table width="100%" class="tableBorder_allRound" height="100%" bgcolor="#FFFFFF" border="0" cellspacing="1" cellpadding="0">
  <tr>
    <td height="10%" colspan="2"><?php include '../presentation/customerAndOperation/sample/ipad/dashBoard/header.php'; ?></td>
    
   	<script type="application/javascript" src="../libraries/javascript/jquery-impromptu.js"></script> 
	<script type="application/javascript" src="../libraries/javascript/jquery-impromptu.min.js"></script>
    <link rel="stylesheet" href="../libraries/chosen/chosen/chosen.css" />
    
    <script src="../libraries/chosen/chosen/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="../libraries/validate/jquery-1.js" type="text/javascript"></script> 
    <script src="../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script> 
    <script src="../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script> 
    <script type="application/javascript" src="../presentation/customerAndOperation/sample/ipad/sampleRequisitionBr/sampleRequest_js.js"></script>

<script>
	jQuery.noConflict();
</script>    
  </tr>
  <tr>
    <td >
        <table width="100%" height="100%" class="tableBorder_allRound content" border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td valign="top">
            <div align="center">
	<div class="trans_layoutL">
	<div class="trans_text">Sample Requisition</div>
	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <tr>
    	<td>
        	<table width="100%" border="0" > 
            <tr class="normalfnt">
              <td>Search</td>
              <td><div id="grap"><select name="cboGraphic" id="cboGraphic" class="chosen-select validate[required]" style="width:280px">
                <option value=""></option>
                <?php
					$result = $obj_requisition_get->getGraphic();
					$count	= mysqli_num_rows($result);
					while($row = mysqli_fetch_array($result))
					{
						if($concatRequiNo==$row['concatReqNo'])
							echo "<option selected value=\"".$row['concatReqNo']."\" style=\"background-color:".$row["BGCOLOR"]."\">".$row['GRAPHIC']."</option>";	
						else if($count==1)
							echo "<option selected value=\"".$row['concatReqNo']."\" style=\"background-color:".$row["BGCOLOR"]."\">".$row['GRAPHIC']."</option>";	
						else
							echo "<option value=\"".$row['concatReqNo']."\" style=\"background-color:".$row["BGCOLOR"]."\">".$row['GRAPHIC']."</option>";	
					}
                ?>
              </select></div></td>
              <td>&nbsp;</td>
              <td class="clsCustomer">&nbsp;</td>
            </tr>
            <tr class="normalfnt">
              <td width="15%">Graphic Ref No <span class="compulsoryRed">*</span></td>
              <td width="35%"><input value="<?php echo htmlentities($header_arr['GRAPHIC']); ?>" name="txtGraphicRefNo" type="text" class="validate[required,maxSize[50]]" id="txtGraphicRefNo" style="width:280px;text-transform:uppercase" /></td>
              <td width="15%">Customer <span class="compulsoryRed">*</span></td>
              <td width="35%" class="clsCustomer"><select name="cboCustomer" id="cboCustomer" class="validate[required]" style="width:280px">
                <option value=""></option>
                <?php
					$result = $obj_requisition_get->getCustomer($userId,$mainDb);
					$count	= mysqli_num_rows($result);
					while($row = mysqli_fetch_array($result))
					{
						if($header_arr['CUSTOMER']==$row['intId'])
							echo "<option selected value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else if($count==1)
							echo "<option selected value=\"".$row['intId']."\">".$row['strName']."</option>";
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
                ?>
                </select></td>
            </tr>
            <tr class="normalfnt">
              <td>Style No</td>
              <td><input value="<?php echo htmlentities($header_arr['STYLE']); ?>"  name="txtStyleNo" type="text" class="validate[maxSize[50]]"  id="txtStyleNo" style="width:280px" /></td>
              <td>Brand <span class="compulsoryRed">*</span></td>
              <td class="clsBrand"><select class="validate[required]" name="cboBrand" id="cboBrand" style="width:280px">
                <option value=""></option>
                 <?php
					$result = $obj_requisition_get->getBrand($header_arr['CUSTOMER'],$mainDb);
					while($row = mysqli_fetch_array($result))
					{
						if($header_arr['BRAND']==$row['intId'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
                ?>
           		</select></td>
            </tr>
            <tr class="normalfnt">
              <td>Marketer <span class="compulsoryRed">*</span></td>
              <td class="clsMarketer"><select name="cboMarketer" id="cboMarketer" class="validate[required]" style="width:280px">
            	<option value=""></option>
				<?php
                  
                    $result = $obj_requisition_get->getMarketer($mainDb);
					$count	= mysqli_num_rows($result);
                    while($row = mysqli_fetch_array($result))
                    {
                        if($header_arr['MARKETER']==$row['USER_ID'])
                            echo "<option selected=\"selected\" value=\"".$row['USER_ID']."\">".$row['strUserName']."</option>";	
						else if($count==1)
							echo "<option selected=\"selected\" value=\"".$row['USER_ID']."\">".$row['strUserName']."</option>";
                        else
                            echo "<option value=\"".$row['USER_ID']."\">".$row['strUserName']."</option>";	
                    }
                ?>
            </select></td>
              <td>Wash Standerd</td>
              <td class="clsWashStanderd"><select name="cboWashStand" id="cboWashStand" class="" style="width:280px">
                <option value=""></option>
                <?php
                  
                    $result = $obj_requisition_get->getWashingStand($mainDb);
					$count	= mysqli_num_rows($result);
                    while($row = mysqli_fetch_array($result))
                    {
                        if($header_arr['WASH_STANDERD']==$row['intId'])
                            echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else if($count==1)
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
                        else
                            echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
                    }
                ?>
              </select></td>
            </tr>
            <tr class="normalfnt">
              <td>Type Of Print</td>
              <td class="clsTypeOfPrint"><select name="cboPrintType" id="cboPrintType" class="" style="width:280px">
            	  <option value=""></option>
            	  <?php
                  
                    $result = $obj_requisition_get->getTypeOfPrint($mainDb);
					$count	= mysqli_num_rows($result);
                    while($row = mysqli_fetch_array($result))
                    {
                        if($header_arr['PRINT_MODE']==$row['intId'])
                            echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else if($count==1)
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
                        else
                            echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
                    }
                ?>
          	  </select></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr class="normalfnt">
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <?php
				$rowCount		= $obj_requisition_get->getSampleTypesRowCount($mainDb);
				$halfRwCount	= round(($rowCount/2),0);
				$i				= 0;
			?>
            <tr class="normalfnt">
            	<td colspan="2" valign="top"><table  border="0" class="bordered clsTblSampleType" id="tblSampleType" width="100%">
            	  <thead>
            	    <tr>
            	      <th width="46%">Sample Types</th>
            	      <th width="25%">Sample Qty</th>
            	      <th width="29%">Req. Date</th>
          	      </tr>
          	    </thead>
            	  <tbody>
                  <?php
				  	$result	= $obj_requisition_get->getSampleTypes(0,$halfRwCount,$mainDb,base64_decode($requisitionNo),base64_decode($requisitionYear));
					while($row=mysqli_fetch_array($result))
					{
					?>
                    	<tr id="<?php echo $row['SAMPLE_TYPE']; ?>">
                          <td class="clsTypeId"><?php echo $row['strName'];?></td>
                          <td><input type="text" name="txtSampleQty" class="clsSampleQty validate[custom[number],min[0]]" id="txtSampleQty" style="width:100%;text-align:right" value="<?php echo $row['QTY'];?>"></td>
                          <td><input name="txtReqDate" type="text" class="clsPayDate" id="txtReqDate<?php echo $i; ?>" style="width:80px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" value="<?php echo $row['REQUIRED_DATE'];?>"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                    	</tr>
                    <?php
					$i++;	
					}
				  ?>
          	      </tbody>
          	  </table></td>
            	<td colspan="2" valign="top"><table  border="0" class="bordered clsTblSampleType" id="tblSampleType2" width="100%">
            	  <thead>
            	    <tr>
            	      <th width="46%">Sample Types</th>
            	      <th width="25%">Sample Qty</th>
            	      <th width="29%">Req. Date</th>
          	      </tr>
          	    </thead>
            	  <tbody>
                  <?php
				  	$result	= $obj_requisition_get->getSampleTypes($halfRwCount,$rowCount,$mainDb,base64_decode($requisitionNo),base64_decode($requisitionYear));
					while($row=mysqli_fetch_array($result))
					{
					?>
            	    	<tr id="<?php echo $row['SAMPLE_TYPE']; ?>">
                          <td class="clsTypeId" id="<?php echo $row['SAMPLE_TYPE']; ?>"><?php echo $row['strName'];?></td>
                          <td><input type="text" name="txtSampleQty" class="clsSampleQty validate[custom[number],min[0]]" id="txtSampleQty" style="width:100%;text-align:right" value="<?php echo $row['QTY'];?>"></td>
                          <td><input name="txtReqDate" type="text" class="clsPayDate" id="txtReqDate<?php echo $i; ?>" style="width:80px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" value="<?php echo $row['REQUIRED_DATE'];?>"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                    	</tr>
                    <?php
					$i++;	
					}
				  ?>
          	      </tbody>
          	  </table></td>
            	</tr>
            </table>
        </td>
    </tr>
    <tr>
      <td height="32">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
          <tr>
            <td align="center"><a class="button white medium" id="butNew">New</a><a class="button white medium" id="butSave">Save</a><a class="button white medium" id="butCancel" <?php if(base64_decode($requisitionNo)==''){ ?>  style="display:none"<?php } ?>>Cancel</a><a href="?q=dash" class="button white medium" id="butClose">Close</a></td>
            </tr>
          </table>
        </td>
    </tr>
	</table>
	</div>
</div>
            </td>
            </tr>
        </table>
    </td>
  </tr>
</table>
</form>
</body>
<?php
$body = ob_get_clean();
echo $body;
?>
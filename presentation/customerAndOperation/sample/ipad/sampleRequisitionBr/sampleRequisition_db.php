<?php
$backwardseperator 		= "../";
$userId 				= (isset($_SESSION["userId"])?$_SESSION["userId"]:'');
$locationId	  			= (isset($_SESSION["CompanyID"])?$_SESSION["CompanyID"]:'');
$companyId				= (isset($_SESSION["headCompanyId"])?$_SESSION["headCompanyId"]:'');
$mainDb					= (isset($_SESSION["mainDatabase"])?$_SESSION["mainDatabase"]:'');
$userType				= (isset($_SESSION["iPadUserType"])?$_SESSION["iPadUserType"]:'');

$requestType 			= base64_decode($_REQUEST['req_t']);
$programName			= 'Sample Reqisition';
$programCode			= 'P0827';

include_once "../dataAccess/ConnectoriPad.php";
include_once "../class/customerAndOperation/sample/sampleRequisition/cls_sample_requisition_get.php";
include_once "../class/customerAndOperation/sample/sampleRequisition/cls_sample_requisition_set.php";
include_once "../class/cls_commonFunctions_get.php";
include_once "../class/cls_mail.php";
//include_once "../class/cls_commonErrorHandeling_get.php";

$obj_requisition_get	= new Cls_sample_requisition_get($db);
$obj_requisition_set	= new Cls_sample_requisition_set($db);
$obj_common				= new cls_commonFunctions_get($db);
$objMail 				= new cls_create_mail($db);
//$obj_errorHandeling 	= new cls_commonErrorHandeling_get($db);

//$saveMode				= $obj_requisition_get->getPermission($userId,$programCode,'intEdit',$mainDb,'RunQuery');

$savedStatus			= true;
$finalApprove			= false;
$savedMasseged 			= '';
$error_sql				= '';
$error_code				= "";

if($requestType=='loadBrand')
{
	$customer	= base64_decode($_REQUEST['customer']);
	
	$result		= $obj_requisition_get->getBrand($customer,$mainDb);
	$html		= "<option value=\"\"></option>";
	
	while($row = mysqli_fetch_array($result))
	{
		$html.="<option value=\"".$row['intId']."\">".$row['strName']."</option>";
	}
	$response['brandCombo']	= $html;
	echo json_encode($response);	
}
else if($requestType=='getSavedData')
{
	$reqNoArr			= explode('/',base64_decode($_REQUEST['rs_ar']));
	$reqNo				= $reqNoArr[0];
	$reqYear			= $reqNoArr[1];
	
	$result				= $obj_requisition_get->getSavedHeaderData($reqNo,$reqYear,$mainDb);
	$row				= mysqli_fetch_array($result);
	
	$response['style'] 			= $row['STYLE'];
	$response['marketer'] 		= $row['MARKETER'];
	$response['typeOfPeint'] 	= $row['PRINT_MODE'];
	$response['customer'] 		= $row['CUSTOMER'];
	$response['brand'] 			= $row['BRAND'];
	$response['washStanderd'] 	= $row['WASH_STANDERD'];
	$response['graphic'] 		= $row['GRAPHIC'];
	$response['arrDetailData'] 	= getSavedDetailData($reqNo,$reqYear);
	
	echo json_encode($response);
	
}
else if($requestType=='saveData')
{
	$arrHeader 			= json_decode(base64_decode($_REQUEST['arrHeader']),true);
	$arrDetails 		= json_decode(base64_decode($_REQUEST['arrDetails']),true);
	
	$db->begin();
	
	$requisitionNoArr	= explode('/',$arrHeader['requisitionNoArr']);
	$requisitionNo		= $requisitionNoArr[0];
	$requisitionYear	= $requisitionNoArr[1];
	$graphic			= $obj_common->replace($arrHeader["graphic"]);
	$customer			= $arrHeader['customer'];
	$style				= $obj_common->replace($arrHeader["style"]);
	$brand				= $arrHeader['brand'];
	$marketer			= $arrHeader['marketer'];
	$washStand			= ($arrHeader['washStand']==''?'null':$arrHeader['washStand']);
	$printMode			= ($arrHeader['printMode']==''?'null':$arrHeader['printMode']);
	$editMode			= false;
	
	if($requisitionNo=='' && $requisitionYear=='')
	{
		$sysNo_arry 	= $obj_common->GetSystemMaxNo('SAMPLE_REQUISITION_NO',$locationId);
		if($sysNo_arry["rollBackFlag"]==1 && $savedStatus)
		{
			$savedStatus	= false;
			$error_code		= '0007';
			$savedMasseged 	= $obj_common->get_sys_user_msg($error_code,$mainDb,'RunQuery2');
			$array_errors[] = $obj_requisition_get->get_error_log_sql($sysNo_arry["msg"],$sysNo_arry["q"],$error_code,$userId);
		}
		$requisitionNo		= $sysNo_arry["max_no"];
		$requisitionYear	= date('Y');
		
		$approveLevels 		= 2;
		$status				= 1;
		
		$resultHArr			= $obj_requisition_set->saveHeader($requisitionNo,$requisitionYear,$graphic,$customer,$style,$brand,$marketer,$washStand,$printMode,$locationId,$companyId,$userId,$status,$approveLevels);
		if($resultHArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$error_code		= '0007';
			$savedMasseged 	= $obj_common->get_sys_user_msg($error_code,$mainDb,'RunQuery2');
			$error_sql		= $resultHArr['error_sql'];	
			$array_errors[] = $obj_requisition_get->get_error_log_sql($resultHArr["savedMassege"],$resultHArr["error_sql"],$error_code,$userId);
		}
	}
	else
	{
		$validateArr		= $obj_requisition_get->check_approved_orders_for_requisition($requisitionNo,$requisitionYear,$mainDb,'RunQuery2');
		if($validateArr['type']=='false' && $savedStatus)
		{
			$savedStatus	= false;
			$error_code		= '0002';
			$savedMasseged 	= $obj_common->get_sys_user_msg($error_code,$mainDb,'RunQuery2');
		}
		$rsultUpdHistry		= $obj_requisition_set->updateHistoryHeader($requisitionNo,$requisitionYear,$userId);
		if($rsultUpdHistry['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$error_code		= '0007';
			$savedMasseged	= $obj_common->get_sys_user_msg($error_code,$mainDb,'RunQuery2');
			$error_sql		= $rsultUpdHistry['error_sql'];	
			$array_errors[] = $obj_requisition_get->get_error_log_sql($rsultUpdHistry["savedMassege"],$rsultUpdHistry["error_sql"],$error_code,$userId);
		}
		$histryInsId		= $rsultUpdHistry['insertId'];
		
		$resultUHArr		= $obj_requisition_set->updateHeader($requisitionNo,$requisitionYear,$graphic,$customer,$style,$brand,$marketer,$washStand,$printMode,$locationId,$companyId,$userId,$userType);
		if($resultUHArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$error_code		= '0007';
			$savedMasseged	= $obj_common->get_sys_user_msg($error_code,$mainDb,'RunQuery2');
			$error_sql		= $resultUHArr['error_sql'];	
			$array_errors[] = $obj_requisition_get->get_error_log_sql($resultUHArr["savedMassege"],$resultUHArr["error_sql"],$error_code,$userId);
		}
		
		$rsultUpdDetHistry	= $obj_requisition_set->updateHistoryDetail($requisitionNo,$requisitionYear,$histryInsId);
		if($rsultUpdDetHistry['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$error_code		= '0007';
			$savedMasseged	= $obj_common->get_sys_user_msg($error_code,$mainDb,'RunQuery2');
			$error_sql		= $rsultUpdDetHistry['error_sql'];	
			$array_errors[] = $obj_requisition_get->get_error_log_sql($rsultUpdDetHistry["savedMassege"],$rsultUpdDetHistry["error_sql"],$error_code,$userId);
		}
		$editMode		= true;
	}
	
	foreach($arrDetails as $array_loop)
	{
		
		$typeId			= $array_loop['typeId'];
		$checkQty		= $array_loop['qty'];
		$qty			= ($array_loop['qty']==''?'0':$array_loop['qty']);
		$reqDate		= ($array_loop['reqDate']==''?'null':'"'.$array_loop['reqDate'].'"');
		
		$checkStatus	= $obj_requisition_get->checkDataExist($requisitionNo,$requisitionYear,$typeId);
		if($checkStatus)
		{	
			$dataArr	= $obj_requisition_get->validateTypeWiseBeforeEdit($requisitionNo,$requisitionYear,$typeId);
			if($dataArr['count']>0)
			{
				if(($dataArr['qty']!=$dataArr['balQty']) && $savedStatus)
				{
					continue;
				}
			}
			$resultDArr	= $obj_requisition_set->updateDetails($requisitionNo,$requisitionYear,$typeId,$qty,$reqDate);
 			if($resultDArr['savedStatus']=='fail' && $savedStatus)
			{
				$savedStatus		= false;
				$error_code			= '0007';
				//$savedMasseged 	= $resultDArr['savedMassege'];
				$savedMasseged 		= $obj_common->get_sys_user_msg($error_code,$mainDb,'RunQuery2');
				$error_sql			= $resultDArr['error_sql'];	
				$array_errors[] 	= $obj_requisition_get->get_error_log_sql($resultDArr["savedMassege"],$resultDArr["error_sql"],$error_code,$userId);
			}
 		}
		else
		{
 			$resultDArr1	= $obj_requisition_set->saveDetails($requisitionNo,$requisitionYear,$typeId,$qty,$reqDate);
			if($resultDArr1['savedStatus']=='fail' && $savedStatus)
			{
				$savedStatus		= false;
				$error_code			= '0007';
				//$savedMasseged 	= $resultDArr['savedMassege'];
				$savedMasseged 		= $obj_common->get_sys_user_msg($error_code,$mainDb,'RunQuery2');
				$error_sql			= $resultDArr1['error_sql'];	
				$array_errors[] 	= $obj_requisition_get->get_error_log_sql($resultDArr1["savedMassege"],$resultDArr1["error_sql"],$error_code,$userId);
			}
		}
	}	
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 			= "pass";
		if($editMode)
			$error_code				= '0003';
		else
			$error_code				= '0008';
		//$response['msg'] 			= "Updated Successfully.";
		$response['msg'] 			=  $obj_common->get_sys_user_msg($error_code,$mainDb,'RunQuery');
		
		sendSampleRequisitionMail($requisitionNo,$requisitionYear,$mainPath,$root_path,$editMode);
		
	}
	else
	{
		$db->rollback();
		$response['type'] 			= "fail";
		//$response['msg'] 			= $savedMasseged;
		//$response['sql']			= $error_sql;
		if($error_code==''){
			$error_code		= '0007';
			$array_errors[] = $obj_requisition_get->get_error_log_sql($savedMasseged,$error_sql,$error_code,$userId);
		}

		$response['msg'] 			= $obj_common->get_sys_user_msg($error_code,$mainDb,'RunQuery');
	}
	
	$obj_requisition_set->save_error_log($array_errors);
	echo json_encode($response);
}
else if($requestType=='cancelData')
{
	$requisitionNoArr	= explode('/',base64_decode($_REQUEST['rs_ar']));
	$requisitionNo		= $requisitionNoArr[0];
	$requisitionYear	= $requisitionNoArr[1];
	
	$db->begin();

	$validateArr		= validateBeforeCancel($requisitionNo,$requisitionYear,$programCode,$userId,$mainDb);
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$error_code		= $validateArr["error_code"]; //0005 or 0006
		$savedMasseged= $obj_common->get_sys_user_msg($error_code,$mainDb,'RunQuery2');
	}
	
	$resultUHSArr		= updateHeaderStatus($requisitionNo,$requisitionYear,'-2');
	if($resultUHSArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$error_code		= '0007';
		//$savedMasseged 	= $resultUHSArr["savedMassege"];
		$savedMasseged 	= $obj_common->get_sys_user_msg($error_code,$mainDb,'RunQuery2');
		$error_sql		= $resultUHSArr['error_sql'];	
		$array_errors[]	= $obj_requisition_get->get_error_log_sql($savedMasseged,$error_sql,$error_code,$userId);
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$error_code				= '0004';
		//$response['msg'] 		= "Cancelled Successfully.";
		$response['msg'] 		=  $obj_common->get_sys_user_msg($error_code,$mainDb,'RunQuery');
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		if($error_code==''){
			$error_code		= '0007';
			$array_errors[]	= $obj_requisition_get->get_error_log_sql($savedMasseged,$error_sql,$error_code,$userId);
		}
		//$response['msg'] 		= $savedMasseged;
		//$response['sql']		= $error_sql;
		$response['msg'] 		= $obj_common->get_sys_user_msg($error_code,$mainDb,'RunQuery');
	}
	
	$obj_requisition_set->save_error_log($array_errors);
	echo json_encode($response);
}
else if($requestType=='loadCombo')
{
	$result	= $obj_requisition_get->getGraphic();
	$html	= "<option value=\"\"></option>";
	
	while($row=mysqli_fetch_array($result))
	{
		$html.="<option value=\"".$row['concatReqNo']."\">".$row['GRAPHIC']."</option>";	
	}
	$response['combo'] = $html;
	
	echo json_encode($response);
}
function validateBeforeSave($requisitionNo,$requisitionYear,$programCode,$userId)
{
	global $obj_errorHandeling;
	global $obj_requisition_get;

	$header_arr		= $obj_requisition_get->loadHeaderData($requisitionNo,$requisitionYear,'RunQuery2');
	$validateArr 	= $obj_errorHandeling->get_permision_withApproval_save($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$userId,$programCode,'RunQuery2');
	
	return  $validateArr;
}


function updateHeaderStatus($requisitionNo,$requisitionYear,$status)
{
	global $obj_requisition_set;
	
	if($status=='') 
		$para 	= 'STATUS-1';
	else
		$para 	= $status;
		
	$resultArr	= $obj_requisition_set->updateHeaderStatus($requisitionNo,$requisitionYear,$para);
	
	return  $resultArr;
}
function validateBeforeCancel($requisitionNo,$requisitionYear,$programCode,$userId,$mainDb)
{
	global $obj_requisition_get;
	global $obj_common;
	
	$cancelMode			= 1;//$obj_requisition_get->getPermission($userId,$programCode,'intCancel',$mainDb,'RunQuery2');
	
	if($cancelMode==1)
	{
		$checkQtyArr	= validateBeforeEdit($requisitionNo,$requisitionYear);
		{
			if($checkQtyArr['type']=='fail')
			{
				$validateArr['type'] 	= 'fail';
				$validateArr['error_code'] 	= '0005';
				//$validateArr['msg'] 	= 'Sample order raised.Can not Cancel ';
				$validateArr['msg'] 	=  $obj_common->get_sys_user_msg('0005',$mainDb,'RunQuery');
				
			}
		}
		
	}
	else
	{
		$validateArr['type'] 	= 'fail';
		$validateArr['error_code'] 	= '0006';
		//$validateArr['msg'] 	= 'No permission to Cancel ';
		$validateArr['msg'] 	=  $obj_common->get_sys_user_msg('0006',$mainDb,'RunQuery');
	}
	
	return $validateArr;
}
function getSavedDetailData($reqNo,$reqYear)
{
	global $obj_requisition_get;
	
	$result	= $obj_requisition_get->getSavedDetailData($reqNo,$reqYear);
	while($row = mysqli_fetch_array($result))
	{
		$data['typeId'] 	= $row['SAMPLE_TYPE'];
		$data['qty'] 		= $row['QTY'];
		$data['reqDate'] 	= $row['REQUIRED_DATE'];
		
		$arrDetailData[] 	= $data;	
	}
	return $arrDetailData;
}
function validateBeforeEdit($requisitionNo,$requisitionYear)
{
	global $obj_requisition_get;
	$checkStatus	= false;
	
	$result		= $obj_requisition_get->validateQty($requisitionNo,$requisitionYear);
	while($row = mysqli_fetch_array($result))
	{
		if($row['QTY']!=$row['BAL_QTY'] && !$checkStatus)
			$checkStatus = true;
	}
	if($checkStatus)
	{
		$validateArr['type']	= 'fail';
	}
	
	return $validateArr;
}
function sendSampleRequisitionMail($requisitionNo,$requisitionYear,$mainPath,$root_path,$editMode)
{
	global $companyId;
	global $userId;
	global $objMail;
	global $mainDb;
	global $obj_requisition_get;
	
	if(!$editMode)
	{
		$header_arr		= $obj_requisition_get->loadEmailHeaderData($requisitionNo,$requisitionYear,$mainDb,'RunQuery');
		$reqNo 			= $requisitionNo;
		$reqYear 		= $requisitionYear;
		$graphic 		= $header_arr['GRAPHIC'];
		$customer		= $header_arr['CUSTOMER_NAME'];
		$style			= $header_arr['STYLE'];
		$brand			= $header_arr['BRAND_NAME'];
		$status			= $header_arr['STATUS'];
		
		if($status==1)
		{
			$detail_result	= $obj_requisition_get->loadEmailSaveDetailData($requisitionNo,$requisitionYear,'RunQuery');
			while($row = mysqli_fetch_array($detail_result))
			{
				$data['qty']			= $row['QTY'];
				$data['reqDate']		= $row['REQUIRED_DATE'];
				$data['sampleType']		= $row['SAMPLE_TYPE'];
				
				$detailData[]			= $data;
			}
			
			$enter_arr		= $obj_requisition_get->getCreatorByDetail($requisitionNo,$requisitionYear,'RunQuery');
			$enterUserName 	= $enter_arr['strUserName'];
			$enterUserEmail = $enter_arr['strEmail'];
			
			$receiver_res	= $obj_requisition_get->getReceiverDetail($requisitionNo,$requisitionYear,$companyId,$mainDb,'RunQuery');
			while($row = mysqli_fetch_array($receiver_res))
			{
				if($row['userId']!=$userId)
				{
					$header = "ADD SAMPLE REQUISITION ($reqNo/$reqYear)";
					$_REQUEST 					= NULL;
					$_REQUEST['graphic']   		= $graphic;
					$_REQUEST['customer']   	= $customer;
					$_REQUEST['style']  		= $style;
					$_REQUEST['brand']  		= $brand;
					$_REQUEST['detailData']  	= $detailData;
					$_REQUEST['fromName']  		= $enterUserName;
					$_REQUEST['toName']  		= $row['userName'];
					$_REQUEST['link']			= base64_encode($mainPath."presentation/customerAndOperation/sample/sampleInfomations/addNew/sampleInfomations.php?reqNo=$reqNo&reqYear=$reqYear");
					
					$path 	= $root_path."presentation/customerAndOperation/sample/ipad/sampleRequisitionBr/sampleRequisition_email.php";
					
					$objMail->send_Response_Mail($path,$_REQUEST,$enterUserName,$enterUserEmail,$header,$row['emailAddress'],$row['userName']);
				}
			}
		}
	}
	else
	{
		$update_arr		= $obj_requisition_get->loadEmailHeaderUpdateData($requisitionNo,$requisitionYear,$mainDb,'RunQuery');
		
		$graphicArr 	= explode('~',$update_arr['GRAPHIC']);
		$graphic		= $graphicArr[0];
		$graphicTd		= $graphicArr[1];
		
		$styleArr 		= explode('~',$update_arr['STYLE']);
		$style			= $styleArr[0];
		$styleTd		= $styleArr[1];
		
		$customerArr 	= explode('~',$update_arr['CUSTOMER']);
		$customer		= $customerArr[0];
		$customerTd		= $customerArr[1];
		
		$brandArr 		= explode('~',$update_arr['BRAND']);
		$brand			= $brandArr[0];
		$brandTd		= $brandArr[1];
		
		$marketerArr 	= explode('~',$update_arr['MARKETER']);
		$marketer		= $marketerArr[0];
		$marketerTd		= $marketerArr[1];
		
		$washArr 		= explode('~',$update_arr['WASH_STANDERD']);
		$washStand		= $washArr[0];
		$washStandTd	= $washArr[1];
		
		$printArr 		= explode('~',$update_arr['PRINT_MODE']);
		$printMode		= $printArr[0];
		$printModeTd	= $printArr[1];
		
		$historyId		= $obj_requisition_get->getLastHistoryId($requisitionNo,$requisitionYear);
		$updateDet_res	= $obj_requisition_get->loadBrndxEmailDetailUpdateData($requisitionNo,$requisitionYear,$mainDb,$historyId,'RunQuery');
		
		while($rowE=mysqli_fetch_array($updateDet_res))
		{
			$data['qty']			= $rowE['QTY'];
			$data['reqDate']		= $rowE['REQDATE'];
			$data['sampleType']		= $rowE['sampleType'];
			
			$detailData[]			= $data;
		}
		
		$enter_arr		= $obj_requisition_get->getCreatorByDetail($requisitionNo,$requisitionYear,'RunQuery');
		$enterUserName 	= $enter_arr['strUserName'];
		$enterUserEmail = $enter_arr['strEmail'];
		
		$receiver_res	= $obj_requisition_get->getReceiverDetail($requisitionNo,$requisitionYear,$companyId,$mainDb,'RunQuery');
		while($row = mysqli_fetch_array($receiver_res))
		{
			if($row['userId']!=$userId)
			{
				$header = "UPDATE SAMPLE REQUISITION ($requisitionNo/$requisitionYear)";
				$_REQUEST 					= NULL;
				$_REQUEST['graphic'] 		= $graphic;
				$_REQUEST['graphicTd']  	= $graphicTd;
				$_REQUEST['style']   		= $style;
				$_REQUEST['styleTd']   		= $styleTd;
				$_REQUEST['customer']  		= $customer;
				$_REQUEST['customerTd']  	= $customerTd;
				$_REQUEST['brand']  		= $brand;
				$_REQUEST['brandTd']  		= $brandTd;
				$_REQUEST['marketer']  		= $marketer;
				$_REQUEST['marketerTd']  	= $marketerTd;
				$_REQUEST['washStand']  	= $washStand;
				$_REQUEST['washStandTd']  	= $washStandTd;
				$_REQUEST['printMode']  	= $printMode;
				$_REQUEST['printModeTd']  	= $printModeTd;
				$_REQUEST['detailData']  	= $detailData;
				$_REQUEST['fromName']  		= $enterUserName;
				$_REQUEST['toName']  		= $row['userName'];
				
				$path 	= $root_path."presentation/customerAndOperation/sample/ipad/sampleRequisitionBr/sampleRequisition_upd_email.php";
				
				$objMail->send_Response_Mail($path,$_REQUEST,$enterUserName,$enterUserEmail,$header,$row['emailAddress'],$row['userName']);
			}
		}
	}
}
?>
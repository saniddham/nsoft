// JavaScript Document
$(document).ready(function(){
	
	$('#frmSampleRequest').validationEngine();
	$(".chosen-select").chosen({allow_single_deselect: true , no_results_text: "Oops, nothing found!"}); 
	$("#cboGraphic").chosen().change(getSavedData);
	//$('#frmSampleRequest .graphics').live('change',getSavedData);
	$('#frmSampleRequest #butSave').live('click',saveData);
	$('#frmSampleRequest #butNew').live('click',function(){clearAll('New')});
	$('#frmSampleRequest #butCancel').live('click',Cancel);
	$('#frmSampleRequest #cboCustomer').live('change',loadBrand);
	//getSavedData();
	$('#frmSampleRequest #cboCustomer').change();
});
function loadBrand()
{
	if($('#frmSampleRequest #cboCustomer').val()=='')
	{
		$('#frmSampleRequest #cboBrand').html('');
		return;
	}
	var url 	= "ajax.php?q=requisition&req_t="+window.btoa('loadBrand');
	var data 	= "customer="+window.btoa($('#frmSampleRequest #cboCustomer').val());
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmSampleRequest #cboBrand').html(json.brandCombo);
			}
	});	
}
function getSavedData()
{
	clearAll('');
	if($('#cboGraphic').val()=='')
	{
		return;
	}
	var data = "req_t="+window.btoa('getSavedData');
		data+= "&rs_ar="+window.btoa($('#cboGraphic').val());
	
	var url = "ajax.php?q=requisition";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json)
				{
					$('#frmSampleRequest #butCancel').show();
					$('#txtStyleNo').val(json.style);
					$('#txtGraphicRefNo').val(json.graphic);
					$('#cboCustomer').val(json.customer);
					loadBrand();
					$('#cboBrand').val(json.brand);
					$('#cboMarketer').val(json.marketer);
					$('#cboWashStand').val(json.washStanderd);
					$('#cboPrintType').val(json.typeOfPeint);
					
					var lengthDetail   = json.arrDetailData.length;
					var arrDetailData  = json.arrDetailData;	
					for(var i=0;i<lengthDetail;i++)
					{
						var typeId		= arrDetailData[i]['typeId'];
						var qty			= arrDetailData[i]['qty'];	
						var reqDate		= arrDetailData[i]['reqDate'];
						
						
							$('.clsSampleQty').each(function(){
								
								 var gridTypeId = $(this).parent().parent().attr('id');
								 if(gridTypeId==typeId)
								 {
									 $(this).val(qty);
									 $(this).parent().parent().find('.clsPayDate').val(reqDate);
								 }
							});
						
					}
					
				},
				error:function(xhr,status)
				{
					
				}		
		});
	
	
	
}
function saveData()
{
	showWaiting();
	
	var requisitionNoArr	= $('#frmSampleRequest #cboGraphic').val();
	var graphic				= $('#frmSampleRequest #txtGraphicRefNo').val();
	var customer			= $('#frmSampleRequest #cboCustomer').val();
	var style				= $('#frmSampleRequest #txtStyleNo').val();
	var brand				= $('#frmSampleRequest #cboBrand').val();
	var marketer			= $('#frmSampleRequest #cboMarketer').val();
	var washStand			= $('#frmSampleRequest #cboWashStand').val();
	var printMode			= $('#frmSampleRequest #cboPrintType').val();

	if($('#frmSampleRequest').validationEngine('validate'))
	{

		var data = "req_t="+window.btoa('saveData');
		var arrHeader 					= {};
		arrHeader.requisitionNoArr 		= requisitionNoArr;
		arrHeader.graphic 				= graphic;
		arrHeader.customer 				= customer;
		arrHeader.style 				= style;
		arrHeader.brand 				= brand;
		arrHeader.marketer 				= marketer;
		arrHeader.washStand 			= washStand;
		arrHeader.printMode 			= printMode;
		arrHeader 						= JSON.stringify(arrHeader);
		
		var arrDetails	= "";	
		var chkStatus	= false;
		$('.clsSampleQty').each(function(){
				
				var typeId	= $(this).parent().parent().attr('id');
				var qty		= $(this).val();
				var reqDate	= $(this).parent().parent().find('.clsPayDate').val();
				
				if(qty!='')
					chkStatus = true;
					
				arrDetails += "{";
				arrDetails += '"typeId":"'+ typeId +'",' ;
				arrDetails += '"qty":"'+ qty +'",' ;
				arrDetails += '"reqDate":"'+ reqDate +'"' ;
				arrDetails += "},";

			});
			if(!chkStatus)
			{
				$(this).validationEngine('showPrompt','No Sample Qty to save.','fail');
				hideWaiting();	
				return;
			}
				
			arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
			var arrHeader	= arrHeader;
			var arrDetails	= '['+arrDetails+']';
			data+="&arrHeader="+window.btoa(arrHeader)+"&arrDetails="+window.btoa(arrDetails);
			
			var url = "ajax.php?q=requisition";
			$.ajax({
					url:url,
					dataType:'json',
					type:'post',
					data:data,
					async:false,
					success:function(json){
						$('#frmSampleRequest #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
						{
							$('#frmSampleRequest #butCancel').hide();
							var t = setTimeout("alertx()",1000);
							loadCombo();
							clearAll('New');
							hideWaiting();
							return;
						}
						else
						{
							hideWaiting();
						}
					},
					error:function(xhr,status){
							
							$('#frmSampleRequest #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							hideWaiting();
							return;
					}		
			});
	}
	else
	{
		hideWaiting();
	}
}
function Cancel()
{
	if($('#cboGraphic').val()=='')
	{
		$('#frmSampleRequest #grap').validationEngine('showPrompt','Select Graphic Reference No. ','fail');
		hideWaiting();
		return;
	}
	var val = $.prompt('Are you sure you want to Cancel this Requisition ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = "ajax.php?q=requisition";
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:"req_t="+window.btoa('cancelData')+"&rs_ar="+window.btoa($('#cboGraphic').val()),
						async:false,
						
						success:function(json){
								$('#frmSampleRequest #butCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx3()",1000);
									loadCombo();
									clearAll('New');
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSampleRequest #butCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx3()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}
function clearAll(type)
{
	$('#txtGraphicRefNo').val('');
	$('#cboCustomer').val('');
	$('#txtStyleNo').val('');
	$('#cboBrand').html('');
	$('#cboMarketer').val('');
	$('#cboWashStand').val('');
	$('#cboPrintType').val('');
	
	$('.clsSampleQty').val('');
	$('.clsPayDate').val('');
	$('#frmSampleRequest #butCancel').hide();
	
	if(type=='New')
	{
		$('.chosen-select').val('').trigger('liszt:updated');	
	}
}
function loadCombo()
{
	var url = "ajax.php?q=requisition";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:"req_t="+window.btoa('loadCombo'),
				async:false,
				success:function(json)
				{
					$('#cboGraphic').html(json.combo);
				},
				error:function(xhr,status)
				{
				}
		});
}
function alertx()
{
	$('#frmSampleRequest #butSave').validationEngine('hide')	;
}
function alertx3()
{
	$('#frmSampleRequest #butCancel').validationEngine('hide')	;
}
function encodeChars(str)
{
    return encodeURIComponent(str).replace(/['()]/g, escape).replace(/\*/g, '%2A').replace(/%(?:7C|60|5E)/g, unescape);
}
<?php 

	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];

	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 			= $_REQUEST['requestType'];
	
	$sampleNo 				= $_REQUEST['sampleNo'];
	$sampleYear 			= $_REQUEST['sampleYear'];
	$revisionNo 			= $_REQUEST['revisionNo'];
	$date 					= $_REQUEST['date'];
	//$deliveryDate			= $_REQUEST['deliveryDate'];
			
	$graphicNo 				= $_REQUEST['graphicNo'];
	$customerId 			= $_REQUEST['customerId'];
	$styleNo 				= $_REQUEST['styleNo'];
	$brandId 				= $_REQUEST['brandId'];
	$sampleQty 				= val($_REQUEST['sampleQty']);
	$grade 					= val($_REQUEST['grade']);
	$fabricType 			= $_REQUEST['fabricType'];
	$verivide_d65 			= ($_REQUEST['verivide_d65']=='true'?'1':'0');
	$verivide_tl85 			= ($_REQUEST['verivide_tl85']=='true'?'1':'0');
	$verivide_cw 			= ($_REQUEST['verivide_cw']=='true'?'1':'0');
	$verivide_f 			= ($_REQUEST['verivide_f']=='true'?'1':'0');
	$verivide_uv 			= ($_REQUEST['verivide_uv']=='true'?'1':'0');
	$macbeth_dl 			= ($_REQUEST['macbeth_dl']=='true'?'1':'0');
	$macbeth_cw 			= ($_REQUEST['macbeth_cw']=='true'?'1':'0');
	$macbeth_inca 			= ($_REQUEST['macbeth_inca']=='true'?'1':'0');
	$macbeth_tl84 			= ($_REQUEST['macbeth_tl84']=='true'?'1':'0');
	$macbeth_uv 			= ($_REQUEST['macbeth_uv']=='true'?'1':'0');
	$macbeth_horizon 		= ($_REQUEST['macbeth_horizon']=='true'?'1':'0');
	
	$curing_temp 			= val($_REQUEST['curing_temp']);
	$curing_speed 			= val($_REQUEST['curing_speed']);
	$press_temp 			= val($_REQUEST['press_temp']);
	
	$press_pressure 		= val($_REQUEST['press_pressure']);
	$press_time 			= val($_REQUEST['press_time']);
	$meshCount 				= $_REQUEST['meshCount'];
	$instruction 			= $_REQUEST['instruction'];
	$instructionTech			= $_REQUEST['instructionTech'];
	

try{

		$db->begin();

		$sql = "SELECT
					trn_sampleinfomations.intStatus,
					trn_sampleinfomations.intTechnicalApproveLevelStart,
					trn_sampleinfomations.intSampleRoomApproveLevelStart,
					trn_sampleinfomations.intSampleRoomStatus,
					trn_sampleinfomations.intMarketingStatus
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations.intRevisionNo =  '$revisionNo'
				";
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$intStatus = $row['intStatus'];
		$intMarketingStatus = $row['intMarketingStatus'];
		$intTechnicalApproveLevelStart = $row['intMarketingApproveLevelStart'];
		$intSampleRoomApproveLevelStart= $row['intSampleRoomApproveLevelStart'];
		$intSampleRoomStatus	= $row['intSampleRoomStatus'];

	if($requestType=='save')
	{
			$arrCombo 		= json_decode($_REQUEST['arrCombo'], true);	
			//print_r($arrCombo);
			
			if($intSampleRoomStatus==1)
			{
				$msg =  "This sample is already approved.";
				$response['type'] 		= 'fail';
				$response['msg'] 		= $msg;
				//echo json_encode($response);
				//return;
				throw new Exception($msg);
			}
			else
			{
			$sql = "UPDATE `trn_sampleinfomations`
			 SET intSampleRoomEnterUser = '$userId',
			 intSampleRoomStatus = '$intSampleRoomApproveLevelStart',
			 dtSampleRoomEnterDate=now()
				WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevisionNo`='$revisionNo')  ";
				
				$result = $db->RunQuery2($sql);
				if(!$result)
				throw new Exception('Failed to save '.$db->errormsg);

					/*$arrPart 		= json_decode($_REQUEST['arrPart'], true);
					$e=1;
					$sql = "DELETE FROM `trn_sampleinfomations_printsize` WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevisionNo`='$revisionNo')";
					$result2 = $db->RunQuery2($sql);
					foreach($arrPart as $arrP)
					{
						$partId 			= $arrP['partId'];
						$size_w 			= $arrP['size_w'];
						$size_h 			= $arrP['size_h'];
						$printN 			= 'print '.$e++;
						$sql = "INSERT INTO 
											`trn_sampleinfomations_printsize` 
									(`intSampleNo`,`intSampleYear`,`intRevisionNo`,`strPrintName`,`intWidth`,
										`intHeight`,`intPart`,`intCompanyId`) 
									VALUES ('$sampleNo','$sampleYear','$revisionNo','$printN','$size_w','$size_h','$partId','$companyId')";
						$result2 = $db->RunQuery2($sql);
					}*/
					
			$x=0;	
//print_r($arrCombo);
			foreach($arrCombo as $comboa)
			{
				$printMode 			= $comboa['printMode'];
				$washStanderd 		= $comboa['washStanderd'];
				$comboName			= $comboa['comboName'];
				$groundColor 		= $comboa['groundColor'];
				$printName 			= $comboa['printName'];
				$techniqueId 		= $comboa['techniqueId'];
				$fabricType 		= $comboa['fabricType'];
		
				/*if(confirmedOrders($sampleNo,$sampleYear,$revisionNo,$comboName,$printName))
				{
					$msg =  "Can't edit.Approved bulk orders exist";
					$response['type'] 		= 'fail';
					$response['msg'] 		= $msg;
					throw new Exception($msg);
				}*/

				/////////////new
				$arrColTech	=array();
				$x=0;
				foreach($comboa['gridColMarketingDetails'] as $arrColTech)
				{

					$colorId			= null($comboa['gridColMarketingDetails'][$x]['colorId']);
					$techId_p			= null($comboa['gridColMarketingDetails'][$x]['techId_p']);

						$y=0; 
						//if($comboName=='c1' && $printName=='print 1' && $colorId=='11851' && $techId_p=='4') 
						//print_r($arrCombo['gridColMarketingDetails'][$x]['marketingItems']);
						foreach($comboa['gridColMarketingDetails'][$x]['marketingItems'] as $arrMarketing)
						{
							//if($comboName=='c1' && $printName=='print 1' && $colorId=='11851' && $techId_p=='4') 
							$itemId		= null($arrColTech['marketingItems'][$y]['itemId']);
							$qty		= null($arrColTech['marketingItems'][$y]['qty']);
							$w			= null($arrColTech['marketingItems'][$y]['w']);
							$h			= null($arrColTech['marketingItems'][$y]['h']);
							$sql_1 = "UPDATE `trn_sampleinfomations_details` SET `size_w`=$w,`size_h`=$h,`dblQty`=$qty WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevNo`='$revisionNo') AND (`strPrintName`='$printName') AND (`strComboName`='$comboName') AND (`intColorId`='$colorId') AND (`intTechniqueId`='$techId_p') AND (`intItem`='$itemId')  ";
							$result_1 = $db->RunQuery2($sql_1);
							if(!$result_1)
							throw new Exception('Failed to save '.$db->errormsg);
							//else
								//updateSample_dtails($sampleNo,$sampleYear,$revisionNo,$comboName,$printName,$colorId,$techId_p,$item,$qty,$w,$h);
							$y++;
						}
						
						
						$z=0;
						$arrTechDet =array();
						//if($comboName=='c1' && $printName== 'print 1')
						foreach($arrColTech['technicalDetails'] as $arrTechDet)
						{
						//print_r($arrColTech['technicalDetails'][$z]);
							$inkType		 	 = null($arrColTech['technicalDetails'][$z]['inkType']);
							$itemId			 	 = null($arrColTech['technicalDetails'][$z]['itemId']);
							$shots				 = val($arrColTech['technicalDetails'][$z]['shots']);
							$weight			 	 = null($arrColTech['technicalDetails'][$z]['weight']);
							$orderBy		 	 = null($arrColTech['technicalDetails'][$z]['orderBy']);
							if($orderBy=='')
								$orderBy	='NULL';
							
							$z++;

							$sql = "UPDATE `trn_sampleinfomations_details_technical` SET `dblColorWeight`='$weight' , intNoOfShots='$shots',intOrderBy=$orderBy WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevNo`='$revisionNo') AND (`strPrintName`='$printName') AND (`strComboName`='$comboName') AND (`intColorId`='$colorId') AND (`intInkTypeId`='$inkType')  ";
							
							$result2 = $db->RunQuery2($sql);
							if(!$result2)
							throw new Exception('Failed to save '.$db->errormsg);
						}
					
					$x++;
				}
				}
				///////////////
			}
			
			$sql_c_approved	="
							SELECT
							trn_sampleinfomations_combo_print_approvedby.SAMPLE_NO,
							trn_sampleinfomations_combo_print_approvedby.SAMPLE_YEAR,
							trn_sampleinfomations_combo_print_approvedby.REVISION
							FROM `trn_sampleinfomations_combo_print_approvedby`
							WHERE
							trn_sampleinfomations_combo_print_approvedby.SAMPLE_NO = '$sampleNo' AND
							trn_sampleinfomations_combo_print_approvedby.SAMPLE_YEAR = '$sampleYear' AND
							trn_sampleinfomations_combo_print_approvedby.REVISION = '$revisionNo' AND
							trn_sampleinfomations_combo_print_approvedby.`STATUS` = 1
			";
			$result_c_approved	= $db->RunQuery2($sql_c_approved);
			if(mysqli_num_rows($result_c_approved)>0){
				$flag_c_ap	=1;
			}
			else{
				//save non-direct rm
				$arrNonDirRm 		= json_decode($_REQUEST['arrNonDirRm'], true);
				foreach($arrNonDirRm as $x)
				{
					$itemId			= null($x['itemId']);
					$weight			= null($x['weight']);
	
					$sql_1 = "UPDATE `trn_sample_non_direct_rm_consumption` SET `CONSUMPTION`='$weight' WHERE (`SAMPLE_NO`='$sampleNo') AND (`SAMPLE_YEAR`='$sampleYear') AND (`REVISION_NO`='$revisionNo') AND (`ITEM`='$itemId')";
					$result_1 = $db->RunQuery2($sql_1);
					if(!$result_1)
					throw new Exception('Failed to save non-direct rm consumption '.$db->errormsg);
				}
			}
	
		
		
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
			$response['sampleNo'] 	= $sampleNo;
			$response['sampleYear'] = $sampleYear;
			$response['revisionNo'] = $revisionNo;
			
			//////////////// set sample no ////////////////////////////////
			
			$sql = "SELECT DISTINCT
						trn_sampleinfomations.intSampleNo
					FROM trn_sampleinfomations
					WHERE
						trn_sampleinfomations.intSampleYear =  '$sampleYear'
					order by intSampleNo
					";
			$result = $db->RunQuery2($sql);
			$sampleNoOption='<option value=""></option>';
			while($row=mysqli_fetch_array($result))
			{
				$sampleNoOption.= "<option value=\"".$row['intSampleNo']."\">".$row['intSampleNo']."</option>";
			}
			$response['sampleNoOption'] = $sampleNoOption;
			///////////////////////////////////////////////////////////////
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
			throw new Exception($db->errormsg);
		}
	}
 	$db->commit();
	echo json_encode($response);
	//$db->disconnect();	
	
}
catch(Exception $e)
{	
	switch ($e->getCode()){
		case 0:
			$db->rollback();		
			$response['msg'] 	=  $e->getMessage();
			//$response['error'] 	=  $error_handler->jTraceEx($e);
			$response['type'] 	=  'fail';
			//$response['sql']	=  $db->getSql();
			break;		
		case 1:
			$db->commit();
			$response['msg'] 	=  $e->getMessage();
			$response['pass'] 	=  'fail';
			break;
	}
	echo json_encode($response);
	//$db->disconnect();
	
}
	

	//---------------------------------------
	function confirmedOrders($sampleNo,$sampleYear,$revNo,$combo,$printName){
		global $db;
	 	$sql  	= " SELECT
					trn_orderheader.intOrderNo,
					trn_orderheader.intOrderYear
					FROM
					trn_orderheader
					Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					WHERE
					trn_orderdetails.intSampleNo =  '$sampleNo' AND
					trn_orderdetails.intSampleYear =  '$sampleYear' AND
					trn_orderheader.intApproveLevelStart-
					trn_orderheader.intStatus >=  2 AND
					trn_orderdetails.strCombo =  '$combo' AND
					trn_orderdetails.strPrintName =  '$printName' AND
					trn_orderdetails.intRevisionNo =  '$revNo'
					";
		$result	= $db->RunQuery2($sql);
		if(mysqli_num_rows($result)>0)
			return true;
		else
			return false;
	}
	
	function inkTypeAvailability($sampleNo,$sampleYear,$revNo,$combo,$printName){
		global $db;
	 	$sql  	= " SELECT
					trn_sampleinfomations_details_technical.intSampleNo,
					trn_sampleinfomations_details_technical.intSampleYear
					FROM `trn_sampleinfomations_details_technical`
					WHERE
					trn_sampleinfomations_details_technical.intSampleNo = '$sampleNo' AND
					trn_sampleinfomations_details_technical.intSampleYear = '$sampleYear' AND
					trn_sampleinfomations_details_technical.intRevNo = '$revNo' AND
					trn_sampleinfomations_details_technical.strPrintName = '$printName' AND
					trn_sampleinfomations_details_technical.strComboName = '$combo' AND
					trn_sampleinfomations_details_technical.intInkTypeId > 0
					";
		$result	= $db->RunQuery2($sql);
		if(mysqli_num_rows($result)>0)
			return true;
		else
			return false;
	}
	
	
	function confirmedRecipes($sampleNo,$sampleYear,$revNo,$combo,$printName){
		global $db;
	 	$sql  	= " SELECT *
					FROM `trn_sample_color_recipes_approve`
					WHERE
					trn_sample_color_recipes_approve.intSampleNo = '$sampleNo' AND
					trn_sample_color_recipes_approve.intSampleYear = '$sampleYear' AND
					trn_sample_color_recipes_approve.intRevisionNo = '$revNo' AND
					trn_sample_color_recipes_approve.strCombo = '$combo' AND
					trn_sample_color_recipes_approve.strPrintName = '$printName' AND
					trn_sample_color_recipes_approve.intApproved = 1";
		$result	= $db->RunQuery2($sql);
		if(mysqli_num_rows($result)>0)
			return true;
		else
			return false;
	}
	
?>
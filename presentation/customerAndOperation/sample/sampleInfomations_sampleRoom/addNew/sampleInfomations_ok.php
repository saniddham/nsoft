<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
 ini_set('display_errors',0);
 
$sessionUserId 		= $sessions->getUserId();
$locationId			= $sessions->getLocationId();

$sampleNo 			= $_REQUEST['sampleNo'];
$sampleYear 		= $_REQUEST['sampleYear'];
$revNo				= $_REQUEST['revNo'];

$db->connect();
$sql  = "UPDATE `trn_sampleinfomations` SET `intSampleRoomOpenUser`='$sessionUserId',`dtSampleRoomOpenDate`=now() WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevisionNo`='$revNo') and (intSampleRoomOpenUser='' or intSampleRoomOpenUser IS NULL)";
$result = $db->RunQuery($sql);

/////////////////////////// get max rev no //////////////////////
$sql = "SELECT
			Max(trn_sampleinfomations.intRevisionNo) as maxRevNo
		FROM trn_sampleinfomations
		WHERE
			trn_sampleinfomations.intSampleNo =  '$sampleNo' AND
			trn_sampleinfomations.intSampleYear =  '$sampleYear'
		";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$maxRevNo = $row['maxRevNo'];
/////////////////////////////////////////////////////////////////

$sql = "SELECT dtDate,strGraphicRefNo,intCustomer,strStyleNo,intBrand,
			verivide_d65,verivide_tl84,verivide_cw,verivide_f,verivide_uv,macbeth_dl,macbeth_cw,macbeth_inca,macbeth_tl84,macbeth_uv,macbeth_horizon,
			dblCuringCondition_temp,dblCuringCondition_beltSpeed,dblPressCondition_temp,dblPressCondition_pressure,dblPressCondition_time,strMeshCount,
			strAdditionalInstructions,strAdditionalInstructionsTech,intMarketingApproveLevelStart,intTechnicalApproveLevelStart,intCompanyId,intStatus,intMarketingStatus,intTechnicalStatus,intSampleRoomStatus,intMarketingApproveLevelStart FROM trn_sampleinfomations 
		WHERE 
			intSampleNo =  '$sampleNo' AND
			intSampleYear =  '$sampleYear' AND
			intRevisionNo =  '$revNo'";

$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$enterDate 				= $row['dtDate'];
	$deliveryDate 			= $row['dtDeliveryDate'];
	$graphicNo 				= $row['strGraphicRefNo'];
	$styleNo 				= $row['strStyleNo'];
	$customerId				= $row['intCustomer'];
	$brandId 				= $row['intBrand'];//intCreator
	$intCreator				= $row['intCreator'];//intCreator
	//$sa = $row['dblSampleQty'];
	//$no = $row['dblDeliverQty'];
	$grade 					= $row['intGrade'];
	$fabricTypeId 			= $row['strFabricType'];
	
	$verivide_d65 			= $row['verivide_d65'];
	$verivide_tl84 			= $row['verivide_tl84'];
	$verivide_cw 			= $row['verivide_cw'];
	$verivide_f 			= $row['verivide_f'];
	$verivide_uv 			= $row['verivide_uv'];

	$macbeth_dl 			= $row['macbeth_dl'];
	$macbeth_cw 			= $row['macbeth_cw'];
	$macbeth_inca 			= $row['macbeth_inca'];
	$macbeth_tl84 			= $row['macbeth_tl84'];
	$macbeth_uv 			= $row['macbeth_uv'];
	$macbeth_horizon 		= $row['macbeth_horizon'];

	$curing_temp			= $row['dblCuringCondition_temp'];
	$curing_beltspeed		= $row['dblCuringCondition_beltSpeed'];
	$press_temp				= $row['dblPressCondition_temp'];
	$press_pressure			= $row['dblPressCondition_pressure'];
	$press_time				= $row['dblPressCondition_time'];
	$strMeshCount			= $row['strMeshCount'];
	//strMeshCount
	$instructions			= $row['strAdditionalInstructions'];
	$instructionsTech		= $row['strAdditionalInstructionsTech'];
	$approveLevel			= $row['intApproveLevelStart'];
	$intCompId 				= $row['intCompanyId'];
	$intStatus 				= $row['intSampleRoomStatus'];
	$intMarketingStatus 	= $row['intMarketingStatus'];
	$intTechnicalStatus 	= $row['intTechnicalStatus'];//intMarketingApproveLevelStart
	$intSampleRoomStatus	= $row['intSampleRoomStatus'];
	$intMarketingApproveLevelStart 	= $row['intMarketingApproveLevelStart'];//intMarketingApproveLevelStart

}

$sql = "SELECT
			trn_sampleinfomations_printsize.strPrintName,
			trn_sampleinfomations_printsize.intWidth,
			trn_sampleinfomations_printsize.intHeight,
			trn_sampleinfomations_printsize.intPart
		FROM `trn_sampleinfomations_printsize`
		WHERE
			trn_sampleinfomations_printsize.intSampleNo 	=  '$sampleNo' AND
			trn_sampleinfomations_printsize.intSampleYear 	=  '$sampleYear' AND
			trn_sampleinfomations_printsize.intRevisionNo 	=  '$revNo'
		ORDER BY
			cast(SUBSTR(trn_sampleinfomations_printsize.strPrintName,7,2) as unsigned) ASC 
		";
$result = $db->RunQuery($sql);
$arrPart;
while($row=mysqli_fetch_array($result))
{
	$arr['printName'] = $row['strPrintName'];
	$arr['width'] = $row['intWidth'];
	$arr['height'] = $row['intHeight'];
	$arr['part'] = $row['intPart'];
	$arrPart[] = $arr;
}

$sql = "SELECT DISTINCT
		trn_sampleinfomations_details.strComboName,
		trn_sampleinfomations_details.intPrintMode,
		trn_sampleinfomations_details.intWashStanderd,
		trn_sampleinfomations_details.intGroundColor,
		trn_sampleinfomations_details.intFabricType
		FROM trn_sampleinfomations_details
		WHERE
		trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
		trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
		trn_sampleinfomations_details.intRevNo 		=  '$revNo'
		";
$result = $db->RunQuery($sql);
$arrCombo;
while($row=mysqli_fetch_array($result))
{
	$arr['combo'] 		= $row['strComboName'];
	$arr['printMode'] 	= $row['intPrintMode'];
	$arr['groundColor'] = $row['intGroundColor'];
	$arr['wash'] 		= $row['intWashStanderd'];
	$arr['fabricType'] 	= $row['intFabricType'];
	$arrCombo[] 		= $arr;
}

////////////// type of print //////////////
/*$sql = "SELECT DISTINCT
			trn_sampleinfomations_combos.strComboName,
			trn_sampleinfomations_combos.strPrintName,
			trn_sampleinfomations_combos.intTypeOfPrintId,
			mst_techniques.strName as typeName
		FROM `trn_sampleinfomations_combos`
		Inner Join mst_techniques ON mst_techniques.intId = trn_sampleinfomations_combos.intTypeOfPrintId
		WHERE
			trn_sampleinfomations_combos.intSampleNo 	=  '$sampleNo' AND
			trn_sampleinfomations_combos.intSampleYear 	=  '$sampleYear' AND
			trn_sampleinfomations_combos.intRevisionNo 	=  '$revNo'
		";*/
 $sql = "SELECT
		trn_sampleinfomations_details.strPrintName,
		trn_sampleinfomations_details.strComboName,
		trn_sampleinfomations_details.intColorId,
		trn_sampleinfomations_details.intItem AS marketing_itemId,
		mst_colors.strName AS colorName,
		mst_techniques.strName AS typeOfPrintName,
		trn_sampleinfomations_details.intTechniqueId AS typeOfPrintId,
		marketingItem.strName AS marketing_itemName,
		trn_sampleinfomations_details.dblQty,
		trn_sampleinfomations_details.size_w,
		trn_sampleinfomations_details.size_h
		FROM
		trn_sampleinfomations_details
		Inner Join mst_colors ON mst_colors.intId = trn_sampleinfomations_details.intColorId
		Left Join mst_techniques ON mst_techniques.intId = trn_sampleinfomations_details.intTechniqueId
		Left Join mst_item AS marketingItem ON marketingItem.intId = trn_sampleinfomations_details.intItem
		
		WHERE
		trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
		trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
		trn_sampleinfomations_details.intRevNo =  '$revNo' 
		GROUP BY 
		trn_sampleinfomations_details.strPrintName,
		trn_sampleinfomations_details.strComboName,
		trn_sampleinfomations_details.intColorId,
		trn_sampleinfomations_details.intTechniqueId 
		";
$result = $db->RunQuery($sql);
$arrType=array();
$arr=array();
while($row=mysqli_fetch_array($result))
{
	$arr['combo'] 				= $row['strComboName'];
	$arr['printName'] 			= $row['strPrintName'];
	
	$arr['intColorId'] 			= $row['intColorId'];
	$arr['marketing_itemId'] 	= $row['marketing_itemId'];
	$arr['colorName'] 			= $row['colorName'];
	$arr['typeOfPrintName'] 	= $row['typeOfPrintName'];
	$arr['typeOfPrintId'] 		= $row['typeOfPrintId'];
	$arr['marketing_itemName'] 	= $row['marketing_itemName'];
	$arr['dblQty'] 				= $row['dblQty'];
	
	$arr['size_w'] 					= $row['size_w'];
	$arr['size_h'] 					= $row['size_h'];

	$arrType[] 			= $arr;
}
//print_r($arrType);
 $sql = "SELECT DISTINCT
		trn_sampleinfomations_combos.strComboName,
		trn_sampleinfomations_combos.strPrintName,
		trn_sampleinfomations_combos.intTypeOfPrintId,
		trn_sampleinfomations_combos.intTechniqueId,
		mst_inktypes.strName as techName,
		trn_sampleinfomations_combos.intQty,
		trn_sampleinfomations_combos.intItemId,
		mst_inktypes.intType as spRM
		FROM
		trn_sampleinfomations_combos
		Inner Join mst_inktypes ON mst_inktypes.intId = trn_sampleinfomations_combos.intTechniqueId
		
		WHERE
		trn_sampleinfomations_combos.intSampleNo =  '$sampleNo' AND
		trn_sampleinfomations_combos.intSampleYear =  '$sampleYear' AND
		trn_sampleinfomations_combos.intRevisionNo =  '$revNo'
		";
$result = $db->RunQuery($sql);
$arrTech;
while($row=mysqli_fetch_array($result))
{
	$arr['combo'] 		= $row['strComboName'];
	$arr['printName'] 	= $row['strPrintName'];
	$arr['typeId'] 		= $row['intTypeOfPrintId'];
	$arr['techId'] 		= $row['intTechniqueId'];
	$arr['techName'] 	= $row['techName'];
	$arr['qty'] 		= $row['intQty'];
	$arr['itemId'] 		= $row['intItemId'];
	$arr['spRM'] 		= $row['spRM'];
	
	$arrTech[] 			= $arr;
}


$sql = "SELECT
		trn_sampleinfomations_colors.strComboName,
		trn_sampleinfomations_colors.strPrintName,
		trn_sampleinfomations_colors.intColor,
		mst_colors.strName as colorName
		FROM `trn_sampleinfomations_colors`
		Inner Join mst_colors ON mst_colors.intId = trn_sampleinfomations_colors.intColor
		WHERE
		trn_sampleinfomations_colors.intSampleNo =  '$sampleNo' AND
		trn_sampleinfomations_colors.intSampleYear =  '$sampleYear' AND
		trn_sampleinfomations_colors.intRevisionNo =  '$revNo'
		";
$result = $db->RunQuery($sql);
$arrColor;
while($row=mysqli_fetch_array($result))
{
	$arr['combo'] 		= $row['strComboName'];
	$arr['printName'] 	= $row['strPrintName'];
	$arr['colorId'] 		= $row['intColor'];
	$arr['colorName'] 		= $row['colorName'];
	$arrColor[] 			= $arr;
}



////////////////////////// check confirm permision //////////////////////////
	if($intMarketingStatus>1)
		$x = ($intMarketingApproveLevelStart+1)-$intMarketingStatus;
	else
		$x = ($intMarketingApproveLevelStart+1)-$intStatus;
//	echo $x;
	$sql = "	SELECT
					menupermision.int{$x}Approval as approval
				FROM menupermision Inner Join menus ON menus.intId = menupermision.intMenuId
				WHERE
					menus.strCode =  'P0022' AND
					menupermision.intUserId =  '$sessionUserId'
			";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	$userPermision = $row['approval'];
//BEGIN - CHECK PERMISSION {
$oldRevNo	= ((int)$revNo<(int)$maxRevNo?'1':'0');
$permissionNew		= false;
$permissionSave		= false;
$permissionDelete	= false;
$permissionSearch	= false;

if($form_permision['add']=='1')
{
	$permissionNew		= true;
	if($intStatus!='1')
		$permissionSave	= true;
}

if($form_permision['edit']=='1')
{
	if($intStatus!='1')
		$permissionSave	= true;
	$permissionSearch	= true;
}

if($form_permision['delete']=='1')
{
	$permissionDelete	= true;
	$permissionSearch	= true;
}

if($form_permision['view']=='1')
{
	$permissionSearch	= true;
}
//END 	}


//-------------------------------------------
	$sql_c_approved	="
					SELECT
					trn_sampleinfomations_combo_print_approvedby.SAMPLE_NO,
					trn_sampleinfomations_combo_print_approvedby.SAMPLE_YEAR,
					trn_sampleinfomations_combo_print_approvedby.REVISION
					FROM `trn_sampleinfomations_combo_print_approvedby`
					WHERE
					trn_sampleinfomations_combo_print_approvedby.SAMPLE_NO = '$sampleNo' AND
					trn_sampleinfomations_combo_print_approvedby.SAMPLE_YEAR = '$sampleYear' AND
					trn_sampleinfomations_combo_print_approvedby.REVISION = '$revNo' AND
					trn_sampleinfomations_combo_print_approvedby.`STATUS` = 1
	";
	$result_c_approved	= $db->RunQuery($sql_c_approved);
	if(mysqli_num_rows($result_c_approved)>0)
		$flag_c_ap	=1;
 	else
		$flag_c_ap	=0;
//-------------------------------------------
?>
<title>Sample Infomations - Sample Room</title>

<script type="application/javascript">
var isOldRevNo = '<?php echo ((int)$revNo<(int)$maxRevNo?'1':'0') ?>';
var intTechnicalStatus =  '<?php echo $intStatus; ?>';
</script>
<style type="text/css">
.printName {
	font-family: "Arial Black", Gadget, sans-serif;
	font-size: 16px;
	font-style: normal;
	font-weight: bold;
	color: #009;
	text-align:center
}
.statusWithBorder {	font-family: "Courier New", Courier, monospace;
	border: thin solid #666;
	font-size: 18px;
}
</style> 

<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<form id="frmSampleInfomations" name="frmSampleInfomations" method="post" action="">
<div align="center">
		<div class="trans_layoutXL">
		  <div class="trans_text">Sample Information - Sample Room</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="86%" border="0" align="center">
      <tr>
        <td width="100%" colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="12%" height="22" class="normalfnt">&nbsp;</td>
            <td width="21%">&nbsp;</td>
            <td width="6%">&nbsp;</td>
            <td width="10%" class="normalfnt">&nbsp;</td>
            <td width="51%">&nbsp;</td>
          </tr>
          <tr>
            <td height="28" colspan="5" class="normalfnt"><table bgcolor="#E3F4FD" width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td width="6%" height="28" bgcolor="#FDBDEA"><span class="normalfnt">Sample Year</span></td>
                <td width="6%" bgcolor="#FDBDEA"><select name="cboYear" id="cboYear" style="width:70px">
                  <?php
				    $d = date('Y');
				  	for($d;$d>=2012;$d--)
					{
						if($d==$sampleYear)
							echo "<option selected=\"selected\" id=\"$d\" >$d</option>";	
						else
							echo "<option id=\"$d\" >$d</option>";	
					}
				  ?>
                </select></td>
                <td width="6%" bgcolor="#FDBDEA" class="normalfnt">Graphic No</td>
                <td width="11%" bgcolor="#FDBDEA" class="normalfnt">
                  <select name="cboStyleNo" id="cboStyleNo" style="width:160px">
                    <option value=""></option>
                    <?php
					//$d = date('Y');
				    $sql = "SELECT DISTINCT
								trn_sampleinfomations.strGraphicRefNo
							FROM trn_sampleinfomations
							ORDER BY
								trn_sampleinfomations.strGraphicRefNo ASC
					";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$strGraphicRefNo = $row['strGraphicRefNo'];
						if($graphicNo==$strGraphicRefNo)
							echo "<option selected=\"selected\" value=\"$strStyle\">$strGraphicRefNo</option>";
						else
							echo "<option value=\"$strGraphicRefNo\">$strGraphicRefNo</option>";
					}
				?>
                  </select>
                </td>
                <td width="11%" bgcolor="#FDBDEA" class="normalfnt">Sample No</td>
                <td width="11%" bgcolor="#FDBDEA"><select name="cboSampleNo" id="cboSampleNo" style="width:120px">
                  <option value=""></option>
                  <?php
					$d = date('Y');
					if($sampleYear!='')
							$d = $sampleYear;
				    $sql = "SELECT DISTINCT
							trn_sampleinfomations.intSampleNo
							FROM trn_sampleinfomations
						WHERE
							trn_sampleinfomations.intSampleYear =  '$d' 
							and ( trn_sampleinfomations.intStatus=2 OR trn_sampleinfomations.intStatus=1)
						ORDER BY
							trn_sampleinfomations.intSampleNo ASC
					";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$no = $row['intSampleNo'];
						if($no==$sampleNo)
							echo "<option selected=\"selected\" value=\"$no\">$no</option>";
						else
							echo "<option value=\"$no\">$no</option>";
					}
				?>
                  </select></td>
                <td width="6%" bgcolor="#FDBDEA" class="normalfnt">Revision No</td>
                <td width="16%" bgcolor="#FDBDEA" class="normalfnt"><select name="cboRevisionNo" id="cboRevisionNo" style="width:100px">
                  <option value=""></option>
                  <?php
				  	$sql = "SELECT
					trn_sampleinfomations.intRevisionNo
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations.intSampleNo =  '$sampleNo' and intMarketingStatus = 1
				ORDER BY
					trn_sampleinfomations.intRevisionNo ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($revNo==$row['intRevisionNo'])
						echo "<option selected=\"selected\" value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
					else
						echo "<option value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
				}
				  ?>
                  </select></td>
                <td width="4%" bgcolor="#FDBDEA"><span class="normalfnt">Date</span></td>
                <td width="23%" bgcolor="#FDBDEA"><input name="dtDate" type="text" disabled="disabled" class="txtbox" id="dtDate" style="width:98px;"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);" value="<?php echo ($enterDate==''?date("Y-m-d"):$enterDate); ?>"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                </tr>
              </table></td>
            </tr>
          <tr>
            <td height="19" colspan="5" class="normalfnt">&nbsp;</td>
            </tr>
          <tr>
            <td height="27" class="normalfnt">&nbsp;</td>
            <td colspan="4" align="left">
              
              </td>
            </tr>
          <tr>
            <td height="27" class="normalfnt">Graphic Ref NO</td>
            <td colspan="4"><input name="txtGraphicRefNo" type="text" disabled="disabled" class="validate[required,maxSize[200]]" id="txtGraphicRefNo" style="width:570px" value="<?php echo $graphicNo; ?>" /></td>
            </tr>
          <tr>
            <td height="27" class="normalfnt">Customer</td>
            <td colspan="4"><select disabled="disabled" name="cboCustomer" id="cboCustomer" class="validate[required]" style="width:570px">
              <option value=""></option>
              <?php
					$sql = "select intId,strName from mst_customer where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($customerId==$row['intId'])
							echo "<option selected value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select></td>
            </tr>
          <tr>
            <td height="27" class="normalfnt">Style No</td>
            <td colspan="4"><input disabled="disabled" value="<?php echo $styleNo; ?>"  name="txtStyleNo" type="text" class="validate[maxSize[50]]"  id="txtStyleNo" style="width:570px" /></td>
            </tr>
          <tr>
            <td height="27" class="normalfnt">Brand</td>
            <td colspan="4"><select disabled="disabled" class="validate[required]" name="cboBrand" id="cboBrand" style="width:570px">
            <option></option>
              <?php
				$sql = "(SELECT
						mst_brand.intId,
						mst_brand.strName
					FROM
						mst_customer_brand
						Inner Join mst_brand ON mst_brand.intId = mst_customer_brand.intBrandId
					WHERE
						mst_customer_brand.intCustomerId =  '$customerId' AND
						mst_brand.intStatus =  '1'
					ORDER BY
						mst_brand.strName ASC)
						UNION
						(SELECT
						mst_brand.intId,
						mst_brand.strName
						FROM
						mst_customer_brand
						Inner Join mst_brand ON mst_brand.intId = mst_customer_brand.intBrandId
						INNER JOIN trn_sampleinfomations ON mst_brand.intId = trn_sampleinfomations.intBrand
						
						WHERE
							mst_customer_brand.intCustomerId =  '$customerId'
						ORDER BY
							mst_brand.strName ASC)
						";
				$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($brandId==$row['intId'])
							echo "<option selected value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
			?>
              </select></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td colspan="2"><select  name="cboTechnique" id="cboTechnique" style="width:100px;display:none">
          <option value=""></option>
          <?php
					$sql = "select intId,strName from mst_washstanderd where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						//echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
          </select><select class="colors"  name="cboColor" id="cboColor" style="width:100px;display:none">
            <option value=""></option>
            <?php
					$sql = "select intId,strName from mst_colors where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
      </tr>
      <tr>
        <td id="tdPlaceMainGrid" colspan="2">
        <?php 
					
			$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strComboName
					FROM trn_sampleinfomations_details
					WHERE
					trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_details.intRevNo =  '$revNo'
					";
			$result = $db->RunQuery($sql);
			$rowsCount = mysqli_num_rows($result);
			
			$sql = "SELECT DISTINCT
						strPrintName
					FROM trn_sampleinfomations_printsize
					WHERE
						intSampleNo 	=  '$sampleNo' AND
						intSampleYear 	=  '$sampleYear' AND
						intRevisionNo 	=  '$revNo'
			";
			$result = $db->RunQuery($sql);
			$cellCount = mysqli_num_rows($result);
			
		?>     
        <table id="tblMain" width="74%" border="0" align="left"  class="grid"  >
          <tr class="">
            <td height="152" bgcolor="#FFFFFF" class="normalfnt"  >&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF"><div id="divPicture" class="tableBorder_allRound divPicture" align="center" style="width:264px;height:148px;overflow:hidden" ><?php
			if($sampleNo!='')
			{
				echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revNo-1.png\" />";	
			}
			 ?></div></td>
            <?php
				///////////  img cell loop 
				for($i=1;$i<$cellCount;$i++)
				
				{
			?>
            	<td align="center" bgcolor="#FFFFFF"><div id="divPicture" class="tableBorder_allRound divPicture" align="center" style="width:264px;height:148px;overflow:hidden" ><?php

		echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revNo-".($i+1).".png\" />";	
			
			 ?></div></td>
            <?php	
				}
			?>
            <td bgcolor="#FFFFFF" >&nbsp;</td>
          </tr>
          <tr class="">
            <td height="18" bgcolor="#FFFFFF" class="normalfnt" >&nbsp;</td>
            <td align="center" style="width:300px" bgcolor="#FFFFFF" class="printName" ><span class="printNameSpan">print 1</span><span  class="normalfntMid">
              <select disabled="disabled"  class="part validate[required]" name="cboPart1" id="cboPart1" style="width:180px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_part where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrPart[0]['part'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select>
              <input  disabled="disabled"  name="textfield" type="text" class="sizeW validate[required,custom[number],max[1000],min[0]]" id="sizeW1" style="width:40px" value="<?php echo $arrPart[0]['width']; ?>" />
              <span class="normalfntGrey">W
              <input  disabled="disabled" name="textfield2" type="text" class="sizeH validate[required,custom[number],max[1000],min[0]]" id="sizeH1" style="width:40px" value="<?php echo $arrPart[0]['height']; ?>" />
              H(inch)</span></span></td>
              <?php
			  	///////// print name cell loop
				for($i=1;$i<$cellCount;$i++)
				{
			  ?>
              		<td align="center" bgcolor="#FFFFFF" class="printName" ><span class="printNameSpan">print <?php echo ($i+1); ?></span><span  class="normalfntMid">
              <select disabled="disabled"   class="part validate[required]" name="cboPart2" id="cboPart2" style="width:180px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_part where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					$e=1;
					while($row=mysqli_fetch_array($result))
					{
						
						if($row['intId']==$arrPart[$i]['part'])
							echo "<option selected value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select>
              <input  disabled="disabled"  name="textfield" type="text" class="sizeW validate[required,custom[number],max[1000],min[0]]" id="sizeW2" style="width:40px" value="<?php echo $arrPart[$i]['width']; ?>" />
              <span class="normalfntGrey">W
              <input disabled="disabled"  name="textfield2" type="text" class="sizeH validate[required,custom[number],max[1000],min[0]]" id="sizeH2" style="width:40px" value="<?php echo $arrPart[$i]['height']; ?>" />
              H(inch)</span></span></td>
              
              <?php
				}
			  ?>              
            <td bgcolor="#FFFFFF" >&nbsp;</td>
            </tr>
          <tr class="">
            <td height="10" bgcolor="#CCCCCC" class="gridHeader" ><input  disabled="disabled"  align="left" class="comboName validate[required,maxSize[20]]" value="<?php echo $arrCombo[0]['combo']; ?>" style="width:80px" type="text" name="txtCombo1" id="txtCombo1" /></td>
            <td align="center" bgcolor="#FFFFFF" class="normalfntRight" ><table width="100%" cellspacing="0" cellpadding="0">
            <tr bgcolor="#CCCCCC"  class="gridHeader"><td width="10%" align="left" nowrap  class="normalfnt" style="display:none">No of ups</td>
            <td width="6%" style="display:none"><input align="left" value="<?php echo getUps($arrCombo[0]['combo'],'print 1');?>" style="width:40px; text-align:right" type="text" name="ups" id="ups"  class="cls_ups"/></td>
            <td width="1%">&nbsp;</td>
            <td width="27%"  class="normalfnt"><a id="butProcess" class="button green medium butProcess" style="" name="butProcess"> Routing Process</a></td>
            <td width="1%"><div style="display:none" class="combo"><?php echo $arrCombo[0]['combo']; ?></div><div style="display:none" class="print">print 1</div>&nbsp;</td>
           	</tr>
            </table></td>
            <?php
				for($i=1;$i<$cellCount;$i++)
				{
			?>
            <td align="right" bgcolor="#FFFFFF" ><table width="100%" cellspacing="0" cellpadding="0">
            <tr bgcolor="#CCCCCC"  class="gridHeader"><td width="13%" align="left" class="normalfnt" style="display:none">No of ups</td>
            <td width="8%" style="display:none"><input align="left" value="<?php echo getUps($arrCombo[0]['combo'],'print '.($i+1));?>" style="width:40px; text-align:right" type="text" name="ups" id="ups"  class="cls_ups"/></td>
            <td width="4%">&nbsp;</td>
            <td width="16%"  class="normalfnt">&nbsp;</td>
            <td width="30%" align="left"><a id="butProcess" class="button green small butProcess" style="" name="butProcess"> Routing Process</a>&nbsp;</td>
            <td width="4%"><div style="display:none" class="combo"><?php echo $arrCombo[0]['combo']; ?></div><div style="display:none" class="print">print <?php echo $i+1 ?></div>&nbsp;</td>
            </table></td>
            <?php
				}
			?>
            <td bgcolor="#FFFFFF" >&nbsp;</td>
          </tr>
          <tr class="dataRow">
            <td width="18%"  bgcolor="#FFFFFF" >
              <select disabled="disabled"    name="cboPrintMode2" class="printMode validate[required]" id="cboPrintMode2" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_printmode where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[0]['printMode'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
                </select>
              <select  disabled="disabled"  class="washStanderd validate[required]" name="butWashingStanderd2" id="butWashingStanderd3" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_washstanderd where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[0]['wash'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
					}
				?>
                </select>
              <select  disabled="disabled"  class="colors validate[required]" name="cboGroundColor2" id="cboGroundColor2" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_colors_ground where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[0]['groundColor'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
					}
				?>
                </select>
              <select  disabled="disabled"  class="colors validate[required]" name="cboFabricType" id="cboFabricType" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_fabrictype where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[0]['fabricType'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select></td>
            <td width="67%"  bgcolor="#FFFFFF" style="padding-top:0px" ><table id="tblGrid2" class="tblGrid2" bgcolor="#006666" cellpadding="0" cellspacing="1" width="650" border="0">
              <tr class="normalfntMid">
                <td width="66" bgcolor="#CEE8FB">Color</td>
                <td width="100" bgcolor="#CEE8FB">Technique</td>
                <td width="100" bgcolor="#CEE8FB">Item</td>
                <td width="30" bgcolor="#CEE8FB">Qty</td>
                <td width="80" bgcolor="#CEE8FB">Size(inch)</td>
                <td width="267" bgcolor="#FFE9D2"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                  <tr class="normalfnt">
                    <td  width="6%" bgcolor="#DFFFDF">&nbsp;</td>
                    <td  width="34%" bgcolor="#DFFFDF">Ink Type</td>
                    <td width="12%" bgcolor="#DFFFDF">Shots</td>
                    <td width="33%" bgcolor="#DFFFDF">Item</td>
                    <td width="15%" bgcolor="#DFFFDF">Weight</td>
                    <td width="15%" bgcolor="#DFFFDF">Order</td>
                    </tr>
                </table></td>
                </tr>
                <?php
				foreach($arrType as $arrT)
				{
					//echo $arrT['combo'];
					//echo $arrCombo[0]['combo'];
					if($arrT['combo']==$arrCombo[0]['combo'] && $arrT['printName']=='print 1')
					{
					$comboIs = $arrCombo[0]['combo'];
				    $print   = 'print 1';
					$sqlGetCkecked = "SELECT COMBO,PRINT,STATUS FROM trn_sampleinfomations_combo_print_approvedby WHERE SAMPLE_NO = '$sampleNo' AND SAMPLE_YEAR = '$sampleYear' AND REVISION = '$revNo' AND COMBO = '$comboIs' AND PRINT = '$print'  AND STATUS = 1 ";
			    	$resultGetChecked = $db->RunQuery($sqlGetCkecked);
					$num_rows = mysqli_num_rows($resultGetChecked);
					if($num_rows >0){
						$disable_sample_editable_t	='disabled="disabled"';
						$disable_sample_editable_all	='disabled="disabled"';
						}	
/*						
							$arr['combo'] 				= $row['strComboName'];
							$arr['printName'] 			= $row['strPrintName'];
							
							$arr['intColorId'] 			= $row['intColorId'];
							$arr['marketing_itemId'] 	= $row['marketing_itemId'];
							$arr['colorName'] 			= $row['colorName'];
							$arr['typeOfPrintName'] 	= $row['typeOfPrintName'];
							$arr['typeOfPrintId'] 		= $row['typeOfPrintId'];
							$arr['marketing_itemName'] 	= $row['marketing_itemName'];
							$arr['dblQty'] 				= $row['dblQty'];
							
							$arr['size_w'] 					= $row['size_w'];
							$arr['size_h'] 					= $row['size_h'];
							$arr['technique_techId'] 		= $row['technique_techId'];
							$arr['technique_techName'] 		= $row['technique_techName'];
							$arr['technique_intNoOfShots'] 	= $row['technique_intNoOfShots'];
							$arr['technique_itemId'] 		= $row['technique_itemId'];
							$arr['technique_itemName'] 		= $row['technique_itemName'];
							$arr['technique_dblColorWeight']= $row['technique_dblColorWeight'];*/
				?>
                <tr>
                  <td bgcolor="#ffffffff" class="normalfnt" id="<?php echo $arrT['intColorId'] ?>"><?php echo $arrT['colorName'] ?></td>
                  <td bgcolor="#ffffffff" class="normalfnt" id="<?php echo $arrT['typeOfPrintId']; ?>" >
                    <?php
			  	$sql 	= "SELECT mst_techniques.strName FROM mst_techniques 
						WHERE mst_techniques.intId = '".$arrT['typeOfPrintId']."'";
				$result = 	$db->RunQuery($sql);
				$row	=	mysqli_fetch_array($result);
				echo $row['strName'];
			  ?>
                  </td>
                  <td bgcolor="#ffffffff" class="normalfnt" valign="top">
                    <table bgcolor="#ffffffff" width="100%" cellspacing="0" cellpadding="0" border="1" id="tblGridTI">
				<?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];
				$by_tech	=	$row_ti['COLOUR_ADDED_BY_TECHNICAL'];
			  
			  	$sql = "SELECT
							mst_item.intId,
							mst_item.strName
						FROM `mst_item`
						WHERE
							mst_item.intId =  '".$item_tech."'";
				$result = 	$db->RunQuery($sql);
				$row	=	mysqli_fetch_array($result);
				?><tr><td nowrap="nowrap" id="<?php echo $item_tech; ?>" class="cboItem"><?php echo $row['strName'];?> </td></tr><?php	
				}
			  ?></table>
                  </td>
                  <td bgcolor="#ffffffff"  valign="top">
                    <table  bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0" class="tblGridTIQ" id="tblGridTIQ">
                    <?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId'],$arrT['intTechniqueId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];
				$by_tech	=	$row_ti['COLOUR_ADDED_BY_TECHNICAL'];
				if($by_tech==1 && $disable_sample_editable_all=='')
				$disable_sample_editable_t ='';
				else
				$disable_sample_editable_t	='disabled="disabled"';
?><tr><td>
                    <input <?php echo $disable_sample_editable_t; ?> style="width:30px" value="<?PHP echo ($row_ti['dblQty']<=0?'':$row_ti['dblQty']); ?>"  type="text" name="textfield5" id="txtQty" /><div style="display:none" class="parentItem"><?php echo $item_tech; ?></div>
                    </td></tr>
                    <?php
					}
					?>
                    </table>
                 </td>
                  <td bgcolor="#FFFFFF" class="normalfnt"><table  bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0" class="tblGridTIWH" id="tblGridTIWH">
                    <?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId'],$arrT['intTechniqueId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];
				$by_tech	=	$row_ti['COLOUR_ADDED_BY_TECHNICAL'];
				if($by_tech==1 && $disable_sample_editable_all=='')
				$disable_sample_editable_t ='';
				else
				$disable_sample_editable_t	='disabled="disabled"';
				?><tr><td nowrap  bgcolor="#ffffffff"><div style="display:none" class="parentItem"><?php echo $item_tech; ?></div>
                  <input   value="<?PHP echo ($row_ti['size_w']<=0?'':$row_ti['size_w']); ?>" style="width:30px"  type="text" name="textfield3" id="txtSizeW" <?php echo $disable_sample_editable_all; ?> />
                    W
                    <input style="width:30px" value="<?PHP echo ($row_ti['size_h']<=0?'':$row_ti['size_h']); ?>"  type="text" name="textfield4" id="txtSizeH" <?php echo $disable_sample_editable_all; ?>/>
                    H</td></tr>
                    <?php
				}
					?>
                    </table>&nbsp;</td>
                  <td bgcolor="#ffffffff">
  <table  bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0" id="tblGrid3">
  <?php							
  	$sql_3 = "SELECT
				trn_sampleinfomations_details_technical.intInkTypeId,
				trn_sampleinfomations_details_technical.intNoOfShots,
				trn_sampleinfomations_details_technical.intItem,
				trn_sampleinfomations_details_technical.dblColorWeight, 
				trn_sampleinfomations_details_technical.intOrderBy  
			FROM trn_sampleinfomations_details_technical
			WHERE
				trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
				trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
				trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND
				trn_sampleinfomations_details_technical.strPrintName 	=  '".$arrT['printName']."' AND
				trn_sampleinfomations_details_technical.strComboName 	=  '".$arrT['combo']."' AND
				trn_sampleinfomations_details_technical.intColorId 		=  '".$arrT['intColorId']."'
			";
	$result_3 = $db->RunQuery($sql_3);
	while($row_3=mysqli_fetch_array($result_3))
	{
  ?>
                    <tr class="normalfnt">
                    <td bgcolor="#FFFFFF">&nbsp;</td>
                      <td width="40%" bgcolor="#FFFFFF"><select disabled="disabled"  id="cboTechnique" name="select4" class="cboItem" style="width:92px">
                    <option value=""></option>
                    <?php
			  	$sql = "(	SELECT
							mst_inktypes.intId,
							mst_inktypes.strName
							FROM mst_inktypes
							WHERE
							mst_inktypes.intStatus =  '1'
							order by strName
						) 
						union 
						(
							SELECT
							mst_inktypes.intId,
							mst_inktypes.strName
							FROM mst_inktypes 
							INNER JOIN trn_sampleinfomations_details_technical ON mst_inktypes.intId = trn_sampleinfomations_details_technical.intInkTypeId
							WHERE
							/*mst_inktypes.intStatus <>  '1' AND */
							trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' 
							AND trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' 
							/*AND trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND 
							 trn_sampleinfomations_details_technical.strPrintName 	=  '".$arrT['printName']."' AND
							trn_sampleinfomations_details_technical.strComboName 	=  '".$arrT['combo']."' AND 
							trn_sampleinfomations_details_technical.intColorId 		=  '".$arrT['intColorId']."' */						order by strName )
						
						";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
			 
              		if($row_3['intInkTypeId']==$row['intId'])
              			echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
					else
						echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
              
				}
			  ?>
                  </select></td>
                      <td width="13%" bgcolor="#FFFFFF"><input name="textfield5"  type="text" id="txtShots"  <?php echo $disable_sample_editable_all; ?> style="width:30px;background-color:#FF9" value="<?PHP echo $row_3['intNoOfShots']; ?>" /></td>
                      <td width="35%" bgcolor="#FFFFFF"><select disabled="disabled"    id="cboItem" name="select5" class="cboItem" style="width:100px">
                        <option value=""></option>
                        <?php
			  	$sql = "SELECT
							mst_item.intId,
							mst_item.strName
						FROM `mst_item`
						WHERE
							mst_item.intStatus =  '1' and intMainCategory=1
						ORDER BY
							mst_item.strName ASC
						";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
			 
              		if($row['intId']==$row_3['intItem'])
              			echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
					else
						echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
              
				}
			  ?>
                      </select></td>
                      <td width="12%" bgcolor="#FFFFFF"><input  style="width:30px;background-color:#FF9" value="<?PHP echo $row_3['dblColorWeight']; ?>"  type="text" <?php echo $disable_sample_editable_all; ?> name="txtWeight" id="txtWeight" /></td>
                      <td width="12%" bgcolor="#FFFFFF"><input  style="width:30px;background-color:#FF9" value="<?PHP if($row_3['intOrderBy']>0){ echo $row_3['intOrderBy'];} ?>"  type="text" <?php echo $disable_sample_editable_all; ?> name="txtOrderBy" id="txtOrderBy" /></td>
                    </tr>
  <?php
	}
  ?>

  </table></td>
                  </tr>
                <?php
				$disable_sample_editable_t ='';
				$disable_sample_editable_all ='';
					}
				}
				?>
              </table></td>
                
                <?php
					/////////// first row table cells //////////////
					for($i=1;$i<$cellCount;$i++)
					{
				?>
                <td width="67%"  bgcolor="#FFFFFF" style="padding-top:0px" ><table id="tblGrid2" class="tblGrid2" bgcolor="#006666" cellpadding="0" cellspacing="1" width="650" border="0">
                  <tr class="normalfntMid">
                    <td width="60" bgcolor="#CEE8FB">Color</td>
                    <td width="89" bgcolor="#CEE8FB">Technique</td>
                    <td width="96" bgcolor="#CEE8FB">Item</td>
                    <td width="30" bgcolor="#CEE8FB">Qty</td>
                    <td width="76" bgcolor="#CEE8FB">Size(inch)</td>
                    <td width="292" bgcolor="#FFE9D2"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                      <tr class="normalfnt">
                        <td  width="6%" bgcolor="#DFFFDF">&nbsp;</td>
                        <td  width="34%" bgcolor="#DFFFDF">Ink Type</td>
                        <td width="12%" bgcolor="#DFFFDF">Shots</td>
                        <td width="33%" bgcolor="#DFFFDF">Item</td>
                        <td width="15%" bgcolor="#DFFFDF">Weight</td>
                        <td width="15%" bgcolor="#DFFFDF">Order</td>
                      </tr>
                    </table></td>
                  </tr>
                  <?php	  
				foreach($arrType as $arrT)
				{	
					//echo $arrT['combo'];
					//echo $arrCombo[0]['combo'];
					//echo  $arrT['printName'];
					//echo 'print '.($i+1).'<br>';
					if($arrT['combo']==$arrCombo[0]['combo'] && $arrT['printName']=='print '.($i+1))
					{
					$comboIs = $arrCombo[0]['combo'];
				    $print   = 'print '.($i+1);
					$sqlGetCkecked = "SELECT COMBO,PRINT,STATUS FROM trn_sampleinfomations_combo_print_approvedby WHERE SAMPLE_NO = '$sampleNo' AND SAMPLE_YEAR = '$sampleYear' AND REVISION = '$revNo' AND COMBO = '$comboIs' AND PRINT = '$print' AND STATUS = 1";
			    	$resultGetChecked = $db->RunQuery($sqlGetCkecked);
					$num_rows = mysqli_num_rows($resultGetChecked);
					if($num_rows >0){
						$disable_sample_editable_all	='disabled="disabled"';
						$disable_sample_editable_t	='disabled="disabled"';
						}
/*						
							$arr['combo'] 				= $row['strComboName'];
							$arr['printName'] 			= $row['strPrintName'];
							
							$arr['intColorId'] 			= $row['intColorId'];
							$arr['marketing_itemId'] 	= $row['marketing_itemId'];
							$arr['colorName'] 			= $row['colorName'];
							$arr['typeOfPrintName'] 	= $row['typeOfPrintName'];
							$arr['typeOfPrintId'] 		= $row['typeOfPrintId'];
							$arr['marketing_itemName'] 	= $row['marketing_itemName'];
							$arr['dblQty'] 				= $row['dblQty'];
							
							$arr['size_w'] 					= $row['size_w'];
							$arr['size_h'] 					= $row['size_h'];
							$arr['technique_techId'] 		= $row['technique_techId'];
							$arr['technique_techName'] 		= $row['technique_techName'];
							$arr['technique_intNoOfShots'] 	= $row['technique_intNoOfShots'];
							$arr['technique_itemId'] 		= $row['technique_itemId'];
							$arr['technique_itemName'] 		= $row['technique_itemName'];
							$arr['technique_dblColorWeight']= $row['technique_dblColorWeight'];*/
				?>
                  <tr>
                    <td bgcolor="#ffffffff" class="normalfnt" id="<?php echo $arrT['intColorId'] ?>"><?php echo $arrT['colorName'] ?></td>
                    <td bgcolor="#ffffffff" class="normalfnt"  id="<?php echo $arrT['typeOfPrintId']; ?>" ><?php
			  	$sql 	= "SELECT mst_techniques.strName FROM mst_techniques 
						WHERE mst_techniques.intId = '".$arrT['typeOfPrintId']."'";
				$result = 	$db->RunQuery($sql);
				$row	=	mysqli_fetch_array($result);
				echo $row['strName'];
			  ?></td>
                    <td bgcolor="#ffffffff"><span class="normalfnt">
                      <table bgcolor="#ffffffff" width="100%" cellspacing="0" cellpadding="0" border="1"  id="tblGridTI">
				<?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];
				$by_tech	=	$row_ti['COLOUR_ADDED_BY_TECHNICAL'];
			  	$sql = "SELECT
							mst_item.intId,
							mst_item.strName
						FROM `mst_item`
						WHERE
							mst_item.intId =  '".$item_tech."'";
				$result = 	$db->RunQuery($sql);
				$row	=	mysqli_fetch_array($result);
				?><tr><td nowrap="nowrap" id="<?php echo $item_tech; ?>"  class="cboItem"><?php echo $row['strName'];?> </td></tr><?php	
				}
			  ?></table>
                    </span></td>
                    <td bgcolor="#ffffffff"  valign="top"><table  bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0" class="tblGridTIQ" id="tblGridTIQ">
                    <?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId'],$arrT['intTechniqueId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];
				$by_tech	=	$row_ti['COLOUR_ADDED_BY_TECHNICAL'];
				if($by_tech==1 && $disable_sample_editable_all=='')
				$disable_sample_editable_t ='';
				else
				$disable_sample_editable_t	='disabled="disabled"';
?><tr><td>
                    <input  <?php echo $disable_sample_editable_t; ?>    style="width:30px" value="<?PHP echo ($row_ti['dblQty']<=0?'':$row_ti['dblQty']); ?>"  type="text" name="textfield5" id="txtQty" /><div style="display:none" class="parentItem"><?php echo $item_tech; ?></div>
                    </td></tr>
                    <?php
					}
					?>
                    </table></td>
                    <td bgcolor="#ffffffff" class="normalfnt"  valign="top"><table  bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0" class="tblGridTIWH" id="tblGridTIWH">
                    <?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId'],$arrT['intTechniqueId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];
				$by_tech	=	$row_ti['COLOUR_ADDED_BY_TECHNICAL'];
				if($by_tech==1 && $disable_sample_editable_all=='')
				$disable_sample_editable_t ='';
				else
				$disable_sample_editable_t	='disabled="disabled"';
				?><tr><td nowrap  bgcolor="#ffffffff"><div style="display:none" class="parentItem"><?php echo $item_tech; ?></div>
                  <input   value="<?PHP echo ($row_ti['size_w']<=0?'':$row_ti['size_w']); ?>" style="width:30px"  type="text" name="textfield3" id="txtSizeW" <?php echo $disable_sample_editable_all ?>/>
                    W
                    <input style="width:30px" value="<?PHP echo ($row_ti['size_h']<=0?'':$row_ti['size_h']); ?>"  type="text" name="textfield4" id="txtSizeH" <?php echo $disable_sample_editable_all ?>/>
                    H</td></tr>
                    <?php
				}
					?>
                    </table></td>
                    <td bgcolor="#ffffffff"><table  bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0" id="tblGrid3">
                      <?php							
  	$sql_3 = "SELECT
				trn_sampleinfomations_details_technical.intInkTypeId,
				trn_sampleinfomations_details_technical.intNoOfShots,
				trn_sampleinfomations_details_technical.intItem,
				trn_sampleinfomations_details_technical.dblColorWeight, 
				trn_sampleinfomations_details_technical.intOrderBy  
			FROM trn_sampleinfomations_details_technical
			WHERE
				trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
				trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
				trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND
				trn_sampleinfomations_details_technical.strPrintName 	=  '".$arrT['printName']."' AND
				trn_sampleinfomations_details_technical.strComboName 	=  '".$arrT['combo']."' AND
				trn_sampleinfomations_details_technical.intColorId 		=  '".$arrT['intColorId']."'
			";
	$result_3 = $db->RunQuery($sql_3);
	while($row_3=mysqli_fetch_array($result_3))
	{
  ?>
                      <tr class="normalfnt">
                        <td bgcolor="#FFFFFF">&nbsp;</td>
                        <td width="40%" bgcolor="#FFFFFF"><select disabled="disabled"   id="cboTechnique" name="cboTechnique" class="cboItem" style="width:92px">
                          <option value=""></option>
                          <?php
			  	$sql = "(	SELECT
							mst_inktypes.intId,
							mst_inktypes.strName
							FROM mst_inktypes
							WHERE
							mst_inktypes.intStatus =  '1'
							order by strName
						) 
						union 
						(
							SELECT
							mst_inktypes.intId,
							mst_inktypes.strName
							FROM mst_inktypes 
							INNER JOIN trn_sampleinfomations_details_technical ON mst_inktypes.intId = trn_sampleinfomations_details_technical.intInkTypeId
							WHERE
							/*mst_inktypes.intStatus <>  '1' AND */
							trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' 
							AND trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' 
							/*AND trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND 
							 trn_sampleinfomations_details_technical.strPrintName 	=  '".$arrT['printName']."' AND
							trn_sampleinfomations_details_technical.strComboName 	=  '".$arrT['combo']."' AND 
							trn_sampleinfomations_details_technical.intColorId 		=  '".$arrT['intColorId']."' */						order by strName )
						
						";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
			 
              		if($row_3['intInkTypeId']==$row['intId'])
              			echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
					else
						echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
              
				}
			  ?>
                        </select></td>
                        <td width="13%" bgcolor="#FFFFFF"><input name="txtShots2"  type="text" <?php echo $disable_sample_editable_all; ?> id="txtShots"   style="width:30px;background-color:#FF9" value="<?PHP echo $row_3['intNoOfShots']; ?>" /></td>
                        <td width="35%" bgcolor="#FFFFFF"><select disabled="disabled"     id="cboItem" name="select7" class="cboItem" style="width:100px">
                          <option value=""></option>
                          <?php
			  	$sql = "SELECT
							mst_item.intId,
							mst_item.strName
						FROM `mst_item`
						WHERE
							mst_item.intStatus =  '1' and intMainCategory=1
						ORDER BY
							mst_item.strName ASC
						";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
			 
              		if($row['intId']==$row_3['intItem'])
              			echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
					else
						echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
              
				}
			  ?>
                        </select></td>
                        <td width="12%" bgcolor="#FFFFFF"><input  style="width:30px;background-color:#FF9" value="<?PHP echo $row_3['dblColorWeight']; ?>"  type="text" <?php echo $disable_sample_editable_all; ?> name="txtWeight" id="txtWeight" /></td>
                        <td width="12%" bgcolor="#FFFFFF"><input  style="width:30px;background-color:#FF9" value="<?PHP if($row_3['intOrderBy']>0){ echo $row_3['intOrderBy'];} ?>"  type="text" <?php echo $disable_sample_editable_all; ?> name="txtOrderBy" id="txtOrderBy" /></td>
                      </tr>
                      <?php
	}
  ?>
                     
                    </table></td>
                  </tr>
                  <?php
				  $disable_sample_editable_t	='';
				  $disable_sample_editable_all	='';
					}
				}
				?>
                </table></td>
                
                <?php
					}
				?>
            <td width="15%"  bgcolor="#FFFFFF" style="padding-top:0px" >&nbsp;</td>
          </tr>
          <?php
		  	for($mainRow=1;$mainRow<$rowsCount;$mainRow++)
			{
		  ?>
           <tr class="">
            <td height="18" bgcolor="#CCCCCC" class="gridHeader" ><input  disabled="disabled"  class="comboName validate[required,maxSize[20]]" value="<?php echo $arrCombo[$mainRow]['combo']; ?>" style="width:80px" type="text" name="txtCombo2" id="txtCombo2" /></td>
				<?php
                 for($i=1;$i<=$cellCount;$i++)
                 {
                ?>
                <td align="right" bgcolor="#FFFFFF" class="normalfntRight" valign="top" ><table width="100%" cellspacing="0" cellpadding="0">
                <tr bgcolor="#CCCCCC"  class="gridHeader"><td width="12%" align="left" class="normalfnt" style="display:none">No of ups</td>
                <td width="7%" style="display:none"><input align="left" value="<?php echo getUps($arrCombo[$mainRow]['combo'],'print '.($i));?>" style="width:40px; text-align:right" type="text" name="ups" id="ups"  class="cls_ups"/></td>
                <td width="31%"><a id="butProcess2" class="button green medium butProcess" style="" name="butProcess"> Routing Process</a>&nbsp;</td>
                <td width="1%" class="normalfnt">&nbsp;</td>
                <td width="18%" align="left">&nbsp;</td>
                <td width="14%"><div style="display:none" class="combo"><?php echo $arrCombo[$mainRow]['combo']; ?></div><div  style="display:none" class="print">print <?php echo $i; ?></div>&nbsp;</td>
                </tr>
                </table></td>
                <?php
                }
                ?>
            <td bgcolor="#FFFFFF" >&nbsp;</td>
          </tr>
            <tr class="dataRow">
            <td width="18%"  bgcolor="#FFFFFF" >
              <select disabled="disabled"    class="printMode validate[required]" name="cboPrintMode2" id="cboPrintMode2" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_printmode where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[$mainRow]['printMode'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
                </select>
              <select  disabled="disabled"  class="washStanderd validate[required]" name="butWashingStanderd2" id="butWashingStanderd3" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_washstanderd where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[$mainRow]['wash'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
                </select>
              <select  disabled="disabled"  class="colors validate[required]" name="cboGroundColor2" id="cboGroundColor2" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_colors_ground where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[$mainRow]['groundColor'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";		
					}
				?>
                </select>
              <select  disabled="disabled"  class="colors validate[required]" name="cboFabricType" id="cboFabricType" style="width:100px">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_fabrictype where intStatus = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$arrCombo[$mainRow]['fabricType'])
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select></td>
              <?php 
			  	/////////// color grid and technique and type of grid cell ////////////
				for($i=1;$i<=$cellCount;$i++)
                 {
					
			  ?>
            <td  width="67%"  bgcolor="#FFFFFF" style="padding-top:0px" ><table id="tblGrid2" class="tblGrid2" bgcolor="#006666" cellpadding="0" cellspacing="1" width="650" border="0">
              <tr class="normalfntMid">
                <td width="60" bgcolor="#CEE8FB">Color</td>
                
                <td width="90" bgcolor="#CEE8FB">Technique</td>
                <td width="94" bgcolor="#CEE8FB">Item</td>
                <td width="30" bgcolor="#CEE8FB">Qty</td>
                <td width="77" bgcolor="#CEE8FB">Size(inch)</td>
                <td width="292" bgcolor="#FFE9D2"><table bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0">
                  <tr class="normalfnt">
                    <td  width="6%" bgcolor="#DFFFDF">&nbsp;</td>
                    <td  width="34%" bgcolor="#DFFFDF">Ink Type</td>
                    <td width="12%" bgcolor="#DFFFDF">Shots</td>
                    <td width="33%" bgcolor="#DFFFDF">Item</td>
                    <td width="15%" bgcolor="#DFFFDF">Weight</td>
                    <td width="15%" bgcolor="#DFFFDF">Order</td>
                  </tr>
                </table></td>
              </tr>
              <?php
			// print_r($arrCombo[0]['combo']);
				foreach($arrType as $arrT)
				{
					//echo $arrT['combo'];
					//echo $arrCombo[$mainRow]['combo'].'<br>';
					if($arrT['combo']==$arrCombo[$mainRow]['combo'] && $arrT['printName']=='print '.$i)
					{
					$comboIs = $arrCombo[$mainRow]['combo'];
				    $print   = 'print '.$i;
					$sqlGetCkecked = "SELECT COMBO,PRINT,STATUS FROM trn_sampleinfomations_combo_print_approvedby WHERE SAMPLE_NO = '$sampleNo' AND SAMPLE_YEAR = '$sampleYear' AND REVISION = '$revNo' AND COMBO = '$comboIs' AND PRINT = '$print' AND STATUS = 1";
			    	$resultGetChecked = $db->RunQuery($sqlGetCkecked);
					$num_rows = mysqli_num_rows($resultGetChecked);
					if($num_rows >0){
						$disable_sample_editable_t	='disabled="disabled"';
						$disable_sample_editable_all	='disabled="disabled"';
						}	
/*						
							$arr['combo'] 				= $row['strComboName'];
							$arr['printName'] 			= $row['strPrintName'];
							
							$arr['intColorId'] 			= $row['intColorId'];
							$arr['marketing_itemId'] 	= $row['marketing_itemId'];
							$arr['colorName'] 			= $row['colorName'];
							$arr['typeOfPrintName'] 	= $row['typeOfPrintName'];
							$arr['typeOfPrintId'] 		= $row['typeOfPrintId'];
							$arr['marketing_itemName'] 	= $row['marketing_itemName'];
							$arr['dblQty'] 				= $row['dblQty'];
							
							$arr['size_w'] 					= $row['size_w'];
							$arr['size_h'] 					= $row['size_h'];
							$arr['technique_techId'] 		= $row['technique_techId'];
							$arr['technique_techName'] 		= $row['technique_techName'];
							$arr['technique_intNoOfShots'] 	= $row['technique_intNoOfShots'];
							$arr['technique_itemId'] 		= $row['technique_itemId'];
							$arr['technique_itemName'] 		= $row['technique_itemName'];
							$arr['technique_dblColorWeight']= $row['technique_dblColorWeight'];*/
				?>
              <tr>
                <td bgcolor="#ffffffff" class="normalfnt" id="<?php echo $arrT['intColorId'] ?>"><?php echo $arrT['colorName'] ?></td>
                <td bgcolor="#ffffffff" class="normalfnt" id="<?php echo $arrT['typeOfPrintId']; ?>"  ><?php
			  	$sql 	= "SELECT mst_techniques.strName FROM mst_techniques 
						WHERE mst_techniques.intId = '".$arrT['typeOfPrintId']."'";
				$result = 	$db->RunQuery($sql);
				$row	=	mysqli_fetch_array($result);
				echo $row['strName'];
			  ?></td>
                <td bgcolor="#ffffffff"><span class="normalfnt">
                  <table bgcolor="#ffffffff" width="100%" cellspacing="0" cellpadding="0" border="1"  id="tblGridTI">
				<?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];
				$by_tech	=	$row_ti['COLOUR_ADDED_BY_TECHNICAL'];
			  	$sql = "SELECT
							mst_item.intId,
							mst_item.strName
						FROM `mst_item`
						WHERE
							mst_item.intId =  '".$item_tech."'";
				$result = 	$db->RunQuery($sql);
				$row	=	mysqli_fetch_array($result);
				?><tr><td nowrap="nowrap" id="<?php echo $item_tech; ?>"  class="cboItem"><?php echo $row['strName'];?> </td></tr><?php	
				}
			  ?></table>
                </span></td>
                <td bgcolor="#ffffffff"  class="normalfnt"><table  bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0" class="tblGridTIQ" id="tblGridTIQ">
                    <?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId'],$arrT['intTechniqueId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];
				$by_tech	=	$row_ti['COLOUR_ADDED_BY_TECHNICAL'];
				if($by_tech==1 && $disable_sample_editable_all=='')
				$disable_sample_editable_t ='';
				else
				$disable_sample_editable_t	='disabled="disabled"';
?><tr><td>
                    <input <?php echo $disable_sample_editable_t; ?>     style="width:30px" value="<?PHP echo ($row_ti['dblQty']<=0?'':$row_ti['dblQty']); ?>"  type="text" name="textfield5" id="txtQty" /><div style="display:none" class="parentItem"><?php echo $item_tech; ?></div>
                    </td></tr>
                    <?php
					}
					?>
                    </table></td>
                <td bgcolor="#ffffffff" class="normalfnt"><table  bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0" class="tblGridTIWH" id="tblGridTIWH">
                    <?php
				$sql_ti		=	getTechniqueItems($sampleNo,$sampleYear,$revNo,$arrT['combo'],$arrT['printName'],$arrT['intColorId'],$arrT['intTechniqueId']);
				$result_ti 	= 	$db->RunQuery($sql_ti);
				while($row_ti		=	mysqli_fetch_array($result_ti)){
				$item_tech	=	$row_ti['intItem'];
				$by_tech	=	$row_ti['COLOUR_ADDED_BY_TECHNICAL'];
				if($by_tech==1 && $disable_sample_editable_all=='')
				$disable_sample_editable_t ='';
				else
				$disable_sample_editable_t	='disabled="disabled"';
				?><tr><td nowrap  bgcolor="#ffffffff"><div style="display:none" class="parentItem"><?php echo $item_tech; ?></div>
                  <input   value="<?PHP echo ($row_ti['size_w']<=0?'':$row_ti['size_w']); ?>" style="width:30px"  type="text" name="textfield3" id="txtSizeW" <?php echo $disable_sample_editable_all; ?>/>
                    W
                    <input style="width:30px" value="<?PHP echo ($row_ti['size_h']<=0?'':$row_ti['size_h']); ?>"  type="text" name="textfield4" id="txtSizeH" <?php echo $disable_sample_editable_all; ?>/>
                    H</td></tr>
                    <?php
				}
					?>
                    </table></td>
                <td bgcolor="#ffffffff"><table  bgcolor="#999999" width="100%" border="0" cellspacing="1" cellpadding="0" id="tblGrid3">
                  <?php							
  	$sql_3 = "SELECT
				trn_sampleinfomations_details_technical.intInkTypeId,
				trn_sampleinfomations_details_technical.intNoOfShots,
				trn_sampleinfomations_details_technical.intItem,
				trn_sampleinfomations_details_technical.dblColorWeight, 
				trn_sampleinfomations_details_technical.intOrderBy  
			FROM trn_sampleinfomations_details_technical
			WHERE
				trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
				trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
				trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND
				trn_sampleinfomations_details_technical.strPrintName 	=  '".$arrT['printName']."' AND
				trn_sampleinfomations_details_technical.strComboName 	=  '".$arrT['combo']."' AND
				trn_sampleinfomations_details_technical.intColorId 		=  '".$arrT['intColorId']."'
			";
	$result_3 = $db->RunQuery($sql_3);
	while($row_3=mysqli_fetch_array($result_3))
	{
  ?>
                  <tr class="normalfnt">
                    <td bgcolor="#FFFFFF">&nbsp;</td>
                    <td width="40%" bgcolor="#FFFFFF"><select disabled="disabled"   id="cboTechnique" name="select6" class="cboItem" style="width:92px">
                      <option value=""></option>
                      <?php
			  	$sql = "(	SELECT
							mst_inktypes.intId,
							mst_inktypes.strName
							FROM mst_inktypes
							WHERE
							mst_inktypes.intStatus =  '1'
							order by strName
						) 
						union 
						(
							SELECT
							mst_inktypes.intId,
							mst_inktypes.strName
							FROM mst_inktypes 
							INNER JOIN trn_sampleinfomations_details_technical ON mst_inktypes.intId = trn_sampleinfomations_details_technical.intInkTypeId
							WHERE
							/*mst_inktypes.intStatus <>  '1' AND */
							trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' 
							AND trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' 
							/*AND trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND 
							 trn_sampleinfomations_details_technical.strPrintName 	=  '".$arrT['printName']."' AND
							trn_sampleinfomations_details_technical.strComboName 	=  '".$arrT['combo']."' AND 
							trn_sampleinfomations_details_technical.intColorId 		=  '".$arrT['intColorId']."' */						order by strName )
						
						";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
			 
              		if($row_3['intInkTypeId']==$row['intId'])
              			echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
					else
						echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
              
				}
			  ?>
                    </select></td>
                    <td width="13%" bgcolor="#FFFFFF"><input name="txtShots"  type="text" <?php echo $disable_sample_editable_all; ?> id="txtShots"   style="width:30px;background-color:#FF9" value="<?PHP echo $row_3['intNoOfShots']; ?>" /></td>
                    <td width="35%" bgcolor="#FFFFFF"><select  disabled="disabled"     id="cboItem" name="select6" class="cboItem" style="width:100px">
                      <option value=""></option>
                      <?php
			  	$sql = "SELECT
							mst_item.intId,
							mst_item.strName
						FROM `mst_item`
						WHERE
							mst_item.intStatus =  '1' and intMainCategory=1
						ORDER BY
							mst_item.strName ASC
						";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
			 
              		if($row['intId']==$row_3['intItem'])
              			echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
					else
						echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
              
				}
			  ?>
                    </select></td>
                    <td width="12%" bgcolor="#FFFFFF"><input   style="width:30px;background-color:#FF9" value="<?PHP echo $row_3['dblColorWeight']; ?>"  type="text" <?php echo $disable_sample_editable_all; ?> name="txtWeight" id="txtWeight" /></td>
                    <td width="12%" bgcolor="#FFFFFF"><input   style="width:30px;background-color:#FF9" value="<?PHP if($row_3['intOrderBy']>0){ echo $row_3['intOrderBy'];} ?>"  type="text" <?php echo $disable_sample_editable_all; ?> name="txtOrderBy" id="txtOrderBy" /></td>
                  </tr>
                  <?php 
				  
	}
  ?>
            
                </table></td>
              </tr>
              <?php
			   $disable_sample_editable_t	='';
			   $disable_sample_editable_all	='';
					}
				}
				?>
            </table></td>
            <?php
				 }
			?>
              <td bgcolor="#FFFFFF" >&nbsp;</td>
          </tr>
          <?php
			}
		  ?>
          
          <tr class="dataRow">
            <td  bgcolor="#FFFFFF" >&nbsp;</td>
            <td  bgcolor="#FFFFFF" style="padding-top:0px" ><select   id="cboTechniqueDumy2" name="select2" class="cboItem" style="width:92px;display:none">
              <option value=""></option>
              <?php
			  	$sql = "SELECT
							mst_inktypes.intId,
							mst_inktypes.strName
						FROM mst_inktypes
						WHERE
							mst_inktypes.intStatus =  '1'
						";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
						echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
				}
			  ?>
            </select>
              <select    id="cboItemDumy2" name="cboItemDumy2" class="cboItemDumy2" style="width:100px;display:none">
                <option value=""></option>
                <?php
			  	$sql = "SELECT
							mst_item.intId,
							mst_item.strName
						FROM `mst_item`
						WHERE
							mst_item.intStatus =  '1' and intMainCategory=1
						ORDER BY
							mst_item.strName ASC
						";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					echo "<option  value=\"".$row['intId']."\">".$row['strName']."</option>";
				}
			  ?>
              </select></td>
            <?php
				for($i=1;$i<$cellCount;$i++)
				{
			?>
            <td  bgcolor="#FFFFFF" style="padding-top:0px" >&nbsp;</td>
            <?php
				}
			?>
            <td  bgcolor="#FFFFFF" style="padding-top:0px" >&nbsp;</td>
          </tr>
          </table>
          <p>&nbsp;</p></td>
        </tr>
      <tr>
                <?php
			  	$sql = "
						SELECT
						mst_maincategory.strName AS main_cat,
						mst_subcategory.strName AS sub_cat,
						trn_sample_non_direct_rm_consumption.ITEM as item_id,
						mst_item.strName AS item,
						mst_units.strCode,
						trn_sample_non_direct_rm_consumption.CONSUMPTION 
						FROM
						trn_sample_non_direct_rm_consumption
						INNER JOIN mst_item ON trn_sample_non_direct_rm_consumption.ITEM = mst_item.intId
						INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
						INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
						INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
						WHERE
						trn_sample_non_direct_rm_consumption.SAMPLE_NO = '$sampleNo' AND
						trn_sample_non_direct_rm_consumption.SAMPLE_YEAR = '$sampleYear' AND
						trn_sample_non_direct_rm_consumption.REVISION_NO = '$revNo'
						ORDER BY
						main_cat ASC,
						sub_cat ASC,
						item ASC				";
				$result = $db->RunQuery($sql);
				$i=0;
				while($row=mysqli_fetch_array($result))
				{
					$i++;
					?>
					<?php
					if($i==1){
					?>
                    <tr><td colspan="2"  bgcolor="#F4D3C6" class="normalfnt">Non-Direct Raw Materials</td></tr>
                    <tr><td colspan="2">
                    <table id="tblNoneDirRm" class="tblGrid" bgcolor="#006666" cellpadding="0" cellspacing="1" width="650" border="0">
                    <tr class="normalfntMid">
                    <td width="151" bgcolor="#CEE8FB">Main Category</td>
                    <td width="137" bgcolor="#CEE8FB">Sub Category</td>
                    <td width="137" bgcolor="#CEE8FB">Item</td>
                    <td width="55" bgcolor="#CEE8FB">UOM</td>
                    <td width="122" bgcolor="#CEE8FB">Consumption</td>
                    </tr>
					<?php
					}
					?>
                   <tr>
                     <td bgcolor="#ffffffff" class="normalfnt" id=""><?php echo $row['main_cat'] ?></td>
                    <td bgcolor="#ffffffff" class="normalfnt" ><?php echo $row['sub_cat'] ?></td>
                    <td bgcolor="#ffffffff" class="cls_item"><div style="display:none" id="item"><?php echo $row['item_id'] ?></div><span class="normalfnt"><?php echo $row['item'] ?></span></td>
                    <td bgcolor="#ffffffff"><span class="normalfnt"><?php echo $row['strCode'] ?></span></td>
                    <td bgcolor="#ffffffff" class="normalfnt" align="center"><input style="width:120px; text-align:right" class="validate[min[0]]" value="<?php echo $row['CONSUMPTION'] ?>"  type="text" name="txtNonDirRmConsm" id="txtNonDirRmConsm" <?php if($flag_c_ap==1){ ?>disabled="disabled" <?php } ?> /></td>
                    </tr>
                <?php
				}
				?>
             </table>       </td></tr>
        <td colspan="2" align="center" class=""><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="19" class="normalfnt">&nbsp;</td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td width="12%" height="19" class="normalfnt">Light Box</td>
            <td width="352%" colspan="4"><table width="72%" height="38" border="0" cellpadding="0" cellspacing="1" bgcolor="#CCCCCC">
              <tr class="normalfntMid">
                <td colspan="5" align="center" bgcolor="#FFFFFF">Verivide</td>
                <td colspan="6" align="center" bgcolor="#FFFFFF">Macbeth</td>
                </tr>
                
              <tr class="normalfntMid">
                <td width="6%" bgcolor="#FFFFFF">D65<strong> <br /><input  disabled="disabled"  <?php echo ($verivide_d65?'checked':'') ?> type="checkbox" name="txtVerivide_D65" id="txtVerivide_D65" />
                  </strong></td>
                <td width="9%" bgcolor="#FFFFFF">TL84<strong> <br/><input  disabled="disabled"  <?php echo ($verivide_tl84?'checked':'') ?> type="checkbox" name="txtVerivide_TL84" id="txtVerivide_TL84" />
                  </strong></td>
                <td width="11%" bgcolor="#FFFFFF">CW<strong> <br/><input  disabled="disabled"  <?php echo ($verivide_cw?'checked':'') ?> type="checkbox" name="txtVerivide_CW" id="txtVerivide_CW" />
                  </strong></td>
                <td width="11%" bgcolor="#FFFFFF">F<strong> <br/> <input  disabled="disabled" <?php echo ($verivide_f?'checked':'') ?> type="checkbox" name="txtVerivide_F" id="txtVerivide_F" />
                  </strong></td>
                <td width="11%" bgcolor="#FFFFFF">UV<strong> <br/> <input disabled="disabled"  <?php echo ($verivide_uv?'checked':'') ?> type="checkbox" name="txtVerivide_UV" id="txtVerivide_UV" />
                  </strong></td>
                <td width="8%" bgcolor="#FFFFFF">DL<strong> <br/>  <input  disabled="disabled" <?php echo ($macbeth_dl?'checked':'') ?> type="checkbox" name="txtMacbeth_DL" id="txtMacbeth_DL" />
                  </strong></td>
                <td width="9%" bgcolor="#FFFFFF">CW<strong> <br/>  <input disabled="disabled"  <?php echo ($macbeth_cw?'checked':'') ?> type="checkbox" name="txtMacbeth_CW" id="txtMacbeth_CW" />
                  </strong></td>
                <td width="11%" bgcolor="#FFFFFF">Inca<strong> <br/>  <input disabled="disabled"  <?php echo ($macbeth_inca?'checked':'') ?> type="checkbox" name="txtMacbeth_INCA" id="txtMacbeth_INCA" />
                  </strong></td>
                <td width="8%" bgcolor="#FFFFFF">TL84<strong> <br/>  <input disabled="disabled"  <?php echo ($macbeth_tl84?'checked':'') ?> type="checkbox" name="txtMacbeth_TL84" id="txtMacbeth_TL84" />
                  </strong></td>
                <td width="9%" bgcolor="#FFFFFF">UV<strong> <br/>  <input  disabled="disabled" <?php echo ($macbeth_uv?'checked':'') ?> type="checkbox" name="txtMacbeth_UV" id="txtMacbeth_UV" />
                  </strong></td>
                <td width="7%" bgcolor="#FFFFFF">Horizon<strong> <br/> <input  disabled="disabled" <?php echo ($macbeth_horizon?'checked':'') ?> type="checkbox" name="txtMacbeth_Horizon" id="txtMacbeth_Horizon" />
                  </strong></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td height="45" class="normalfnt">&nbsp;</td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td height="45" colspan="5" class="normalfnt"><table width="762" border="0" class="tableBorder_topRound">
              <tr>
                <td colspan="2" bgcolor="#CCCCCC" class="normalfnt"><strong>Technical Data</strong></td>
                </tr>
              <tr>
                <td width="129" height="47"><span class="normalfnt">Curing Condition</span></td>
                <td width="621"><table width="63%" height="38" border="0" cellpadding="0" cellspacing="0" >
                  <tr>
                    <td width="44%" bgcolor="#FFFFFF" class="normalfntMid"><span class="normalfnt"><strong>TEMP</strong>                      
                      <input name="txtCuringTemp" type="text" disabled="disabled"  class=" " id="txtCuringTemp" style="width:170px" value="<?php echo $curing_temp; ?>" />
                    </span></td>
                    <td width="56%" bgcolor="#FFFFFF" class="normalfntMid"><span class="normalfnt"><strong>BELT SPEED
                      <input name="txtCuringBeltSpeed" type="text" disabled="disabled"  class=" " id="txtCuringBeltSpeed" style="width:170px" value="<?php echo $curing_beltspeed; ?>" />
                    </strong></span></td>
                    </tr>
                  </table></td>
                </tr>
              <tr>
                <td height="49"><span class="normalfnt">Press Condition</span></td>
                <td><table width="72%" height="38" border="0" cellpadding="0" cellspacing="0" >
                  <tr class="normalfntMid">
                    <td width="6%" bgcolor="#FFFFFF"><span class="normalfnt"><strong>TEMP</strong>                      
                      <input name="txtPressTemp" type="text" disabled="disabled"  class="" id="txtPressTemp" style="width:170px" value="<?php echo $press_temp; ?>" />
                    </span></td>
                    <td width="5%" bgcolor="#FFFFFF"><span class="normalfnt"><strong>Pressure
                      <input name="txtPressPressure" type="text" disabled="disabled"  class="" id="txtPressPressure" style="width:170px" value="<?php echo $press_pressure; ?>" />
                      </strong></span></td>
                    <td width="6%" bgcolor="#FFFFFF"><span class="normalfnt"><strong>Time
                      <input name="txtPressTime" type="text" disabled="disabled"  class="" id="txtPressTime" style="width:170px" value="<?php echo $press_time; ?>" />
                      </strong></span></td>
                    </tr>
                  </table></td>
                </tr>
              <tr>
                <td><span class="normalfnt">Mesh Count</span></td>
                <td><span class="normalfnt">
                  <input name="txtMeshCount" type="text" disabled="disabled" class="" id="txtMeshCount" style="width:570px" value="<?php echo $strMeshCount; ?>" />
                  </span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">Technical Instructions</td>
                <td><textarea   name="txtInstructionsTech" cols="89" rows="5" disabled="disabled" id="txtInstructionsTech"><?php echo $instructionsTech; ?></textarea></td>
              </tr>
              </table></td>
          </tr>
          <tr>
            <td height="23" class="normalfnt">&nbsp;</td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td height="23" class="normalfnt">Additional Instructions</td>
            <td colspan="4"><textarea  disabled="disabled"   name="txtInstructions" id="txtInstructions" cols="89" rows="5"><?php echo $instructions; ?></textarea></td>
          </tr>
          <tr>
            <td height="23" valign="top" class="normalfnt">&nbsp;</td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td height="23" class="normalfnt">&nbsp;</td>
            <td colspan="4">&nbsp;</td>
          </tr>
          <tr>
            <td height="23" class="normalfnt">&nbsp;</td>
            <td colspan="4"><select  class="cboItemDumy" name="cboItemDumy" id="cboItemDumy" style="width:100px;display:none">
              <option value=""></option>
              <?php
					$sql = "select intId,strName from mst_item where intStatus = 1 and intSpecialRm=0 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select>
              <select  class="cboItemDumy" name="cboItemDumySPRM" id="cboItemDumySPRM" style="width:100px;display:none">
                <option value=""></option>
                <?php
					$sql = "select intId,strName from mst_item where intStatus = 1 and intSpecialRm = 1 order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select><select  class="cboTechniqueDumy" name="cboTechniqueDumy" id="cboTechniqueDumy" style="width:100px;display:none">
              <option value=""></option>
              <?php
					$sql = "SELECT mst_techniques.intId, mst_techniques.strName FROM mst_techniques WHERE mst_techniques.intStatus =  '1' order by strName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2" align="center" class="tableBorder_allRound">
     <img <?php if($permissionNew==true){'';}else{echo 'style="display:none"';} ?> src="images/Tnew.jpg" width="92" height="24" class="mouseover" id="butNew" />
        <img <?php if($permissionSave==true){'';}else{echo 'style="display:none"';} ?> src="images/Tsave.jpg" width="92" height="24" class="mouseover" id="butSave" />
        <img src="images/Tconfirm.jpg" style="display:none" width="92" height="24" class="mouseover" id="butConfirm" /><?php 

		?><a target="_blank" href="?q=1117&no=<?php echo $sampleNo; ?>&year=<?php echo $sampleYear; ?>&revNo=<?php echo $revNo; ?>"><?php
        
		if($userPermision && $intSampleRoomStatus!='NULL' && $intSampleRoomStatus>1 )
		{
		?><img src="images/Tconfirm.jpg" width="92" height="24" class="mouseover"  id="butReport" /><?php
		
		}
		?><img src="images/Treport.jpg" width="92" height="24" class="mouseover"  id="butReport" /></a>
        <img src="images/Tclose.jpg" width="92" height="24" class="mouseover" /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<!-- technique -->
<!--	<div  style="position: absolute;display:none;z-index:100"  id="popupContact1">
    <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="<?php echo $mainPath; ?>presentation/customerAndOperation/masterData/technique/addNew/technique.php" style="width:800;height:400;border:0;overflow:hidden">
    </iframe>
    </div>-->
    
    <!-- print Mode -->
<!--	<div  style="position: absolute;display:none;z-index:100"  id="popupContact2">
    <iframe onload="loadPrintMode();"   id="iframeMain2" name="iframeMain2" src="<?php echo $mainPath; ?>presentation/customerAndOperation/masterData/printMode/addNew/printMode.php" style="width:800;height:400;border:0;overflow:hidden">
    </iframe>
    </div>-->
    
        <!-- washing Condition -->
<!--    <div  style="position: absolute;display:none;z-index:100"  id="popupContact3">
    <iframe onload="loadWasingCondistion();"   id="iframeMain3" name="iframeMain3" src="<?php echo $mainPath; ?>presentation/customerAndOperation/masterData/washStanderd/addNew/washStanderd.php" style="width:800;height:400;border:0;overflow:hidden">
    </iframe>
    </div>-->
    
    <!-- colors -->
<!--    <div  style="position: absolute;display:none;z-index:100"  id="popupContact4">
    <iframe onload="loadColors();"   id="iframeMain4" name="iframeMain4" src="<?php echo $mainPath; ?>presentation/customerAndOperation/masterData/colors/addNew/colors.php" style="width:800;height:400;border:0;overflow:hidden">
    </iframe>
    </div>-->
    
      <!-- Part -->
<!--    <div  style="position: absolute;display:none;z-index:100"  id="popupContact5">
    <iframe onload="loadPart();"   id="iframeMain5" name="iframeMain5" src="<?php echo $mainPath; ?>presentation/customerAndOperation/masterData/part/addNew/part.php" style="width:800;height:400;border:0;overflow:hidden">
    </iframe>
    </div>-->
    
	 <!-- Part -->
<!--    <div  style="position: absolute;display:none;z-index:100"  id="popupContact6">
    <iframe onload="loadTypeOfPrint();"   id="iframeMain6" name="iframeMain6" src="<?php echo $mainPath; ?>presentation/workStudy/masterData/typeOfPrint/addNew/typeOfPrint.php" style="width:800;height:400;border:0;overflow:hidden">
    </iframe>
    </div>
    -->
    <div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
    <div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</body>
  <?php
  function getTechniqueItems($sampleNo,$sampleYear,$revNo,$combo,$print,$color){
	  
	  $sql = "SELECT DISTINCT
			trn_sampleinfomations_details.intItem,
			trn_sampleinfomations_details.dblQty,
			trn_sampleinfomations_details.size_w,
			trn_sampleinfomations_details.size_h ,
			COLOUR_ADDED_BY_TECHNICAL 
			FROM trn_sampleinfomations_details
			WHERE
			trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
			trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
			trn_sampleinfomations_details.intRevNo 		=  '$revNo' AND
			trn_sampleinfomations_details.strComboName 		=  '$combo' AND
			trn_sampleinfomations_details.strPrintName 		=  '$print' AND
			trn_sampleinfomations_details.intColorId 		=  '$color'  
			";
	return $sql;  
  }
  
  function getUps($combo,$print){
		
		global $db;
		global $sampleNo;
		global $sampleYear;
		global $revNo;

		$sql		="SELECT
					trn_sampleinfomations_combo_print_details.NO_OF_UPS
					FROM `trn_sampleinfomations_combo_print_details`
					WHERE
					trn_sampleinfomations_combo_print_details.SAMPLE_NO = '$sampleNo' AND
					trn_sampleinfomations_combo_print_details.SAMPLE_YEAR = '$sampleYear' AND
					trn_sampleinfomations_combo_print_details.REVISION = '$revNo' AND
					trn_sampleinfomations_combo_print_details.COMBO = '$combo' AND
					trn_sampleinfomations_combo_print_details.PRINT = '$print'
					";	  
		$result 	= $db->RunQuery($sql);
		$row		=mysqli_fetch_array($result);
		$ups 	= $row['NO_OF_UPS'];
		if($ups=='')
			$ups	=0;
		return $ups;
  }
  
 $db->commit();		
$db->disconnect();
?>
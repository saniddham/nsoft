<?php
	session_start();
 	include "../../../../../dataAccess/Connector.php";
	$requestType = $_REQUEST['requestType'];
	$userId		 = $_SESSION["userId"];
	if($requestType=='saveDetails')
	{
		$toSave=0;
		$saved=0;
		$arrRecipe 		= json_decode($_REQUEST['arrRecipe'], true);
		
			$sampleNo 		= $arrRecipe[0]['sampleNo'];
			$sampleYear 	= $arrRecipe[0]['sampleYear'];
			$revNo 			= $arrRecipe[0]['revisionNo'];
			$combo 			= $arrRecipe[0]['combo'];
			$printName 		= $arrRecipe[0]['printName'];
	
			$color 			= $arrRecipe[0]['color'];
			$techId 		= $arrRecipe[0]['techId'];
			$inkTypeId 		= $arrRecipe[0]['inkTypeId'];
			
		
			//----------------------------------
			if($sampleNo==''){
			$sampleNo 		= $_REQUEST['sampleNo'];
			$sampleYear 	= $_REQUEST['sampleYear'];
			$revNo 			= $_REQUEST['revisionNo'];
			$combo 			= $_REQUEST['combo'];
			$printName 		= $_REQUEST['printName'];
			
			$color 			= $_REQUEST['colorId'];
			$techId 		= $_REQUEST['techniqueId'];
			$inkTypeId 		= $_REQUEST['inkTypeId'];
			}
			//----------------------------------
	
	/* comment by roshan - 2013-02-19 -> please remove below comment within 2 months(2013-04-19) */
	
		if(confirmedOrders($sampleNo,$sampleYear,$revNo,$combo,$printName))
		{
			$response['msg'] = 'PO confirmations has been already raised.can not Edit.';	
			$response['status'] = 1;	
			echo json_encode($response);
			return;
		}
		
		
//--------2015-03-06----------------------------

	if($sampleNo){
		$sql		="SELECT
						trn_sampleinfomations.intStatus,
						trn_sampleinfomations.intTechnicalStatus
						FROM `trn_sampleinfomations`
						WHERE
						trn_sampleinfomations.intSampleNo = '$sampleNo' AND
						trn_sampleinfomations.intSampleYear = '$sampleYear' AND
						trn_sampleinfomations.intRevisionNo = '$revNo'
						";	
		$results 	= $db->RunQuery($sql);
		$rowS		=mysqli_fetch_array($results);
		$sampStatus 	= $rowS['intStatus'];
		$techStatus 	= $rowS['intTechnicalStatus'];
	}
	
	//----------------2015-03-18-------
	$sql 		= "SELECT `trn_sample_color_recipes_approve`.`intApproved` from trn_sample_color_recipes_approve WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName'";
	$result_app	=$db->RunQuery($sql);
	$row_app	=mysqli_fetch_array($result_app);
	$flag_recipe_approved	= $row_app['intApproved'];
 	//---------------------------------
	
	$editable=0;
	if($flag_recipe_approved!=1 && $techStatus==1)
		$editable=1;
	 
	//------------------------------------
if($editable==1){		
		$sql = "delete from trn_sample_color_recipes where intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName' and intColorId='$color' and intTechniqueId='$techId' and intInkTypeId='$inkTypeId'";
		$result = $db->RunQuery($sql);
		
		foreach($arrRecipe as $x)
		{
			$toSave++;
			$sql = "INSERT INTO `trn_sample_color_recipes` 
			(`intSampleNo`,`intSampleYear`,`intRevisionNo`,`strCombo`,`strPrintName`,`intColorId`,`intTechniqueId`,
				`intInkTypeId`,`intItem`,`dblWeight`,`intUser`,`dtDate`,'recipeRevision') 
			VALUES ('".$x['sampleNo']."','".$x['sampleYear']."','".$x['revisionNo']."','".$x['combo']."','".$x['printName']."','".$x['color']."','".$x['techId']."','".$x['inkTypeId']."','".$x['itemId']."','".val($x['weight'])."','".$_SESSION["userId"]."',now(),0)";
			$result 	= $db->RunQuery($sql);
			$saved+=$result;
		}
}
	//	}
		//-------------------------------------
		if($editable==0){
			$response['type'] 		= 'fail';
			if($sampStatus==1)
			$response['msg'] 		="Final approval has been raised !";
			else if($techStatus!=1)
			$response['msg'] 		="Technical approval hasn't been raised !";
			else if($flag_recipe_approved==1)
			$response['msg'] 		="Color recipes are approved. Can't edit !";
		}
		else if($toSave==0){
			$response['type'] 		= 'fail';
			$response['msg'] 		="No items to save !";
		}
		else if(($toSave==$saved)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	
	if($requestType=='approveRecipForBulk')
	{
		$toSave=0;
		$saved=0;
		$arrInk 		= json_decode($_REQUEST['arrInk'], true);
			
		
			//----------------------------------
			$sampleNo 		= $_REQUEST['sampleNo'];
			$sampleYear 	= $_REQUEST['sampleYear'];
			$revNo 			= $_REQUEST['revisionNo'];
			$combo 			= $_REQUEST['combo'];
			$printName 		= $_REQUEST['printName'];
			
			//----------------------------------
		
		$errFlag=0;
		
		//------
		$sql 	= "SELECT
		Count(trn_orderdetails.intOrderNo) AS rcds
		FROM
		trn_sample_color_recipes_approve
		INNER JOIN trn_orderdetails ON trn_sample_color_recipes_approve.intRevisionNo = trn_orderdetails.intRevisionNo AND trn_sample_color_recipes_approve.strCombo = trn_orderdetails.strCombo AND trn_sample_color_recipes_approve.strPrintName = trn_orderdetails.strPrintName AND trn_sample_color_recipes_approve.intSampleNo = trn_orderdetails.intSampleNo AND trn_sample_color_recipes_approve.intSampleYear = trn_orderdetails.intSampleYear
		where intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName'";
		$result = $db->RunQuery($sql);
		$row	=mysqli_fetch_array($result);
		if($row['rcds']>0){
			$errFlag=1;
			$msg	="Bulk Consumption entered.Can't Approve again !";
		}
		//------
		$sql 		= "SELECT `trn_sample_color_recipes_approve`.`intApproved` from trn_sample_color_recipes_approve WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName'";
		$result_app	=$db->RunQuery($sql);
		$row_app	=mysqli_fetch_array($result_app);
		if($row_app['intApproved']==1){
			$errFlag=1;
			$msg	="Already approved !";
		}
		//------
		
		$records			=	0;
		$rcds_recp_entered	=	0;	
		foreach($arrInk as $x)
		{ 
			$color		=$x['color'];
			$techId		=$x['techId'];
			$inkTypeId	=$x['inkTypeId'];
			$sql 	= "select count(intSampleNo) as rcds from trn_sample_color_recipes where intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName' and intColorId='$color' and intTechniqueId='$techId' and intInkTypeId='$inkTypeId'";
			$result = $db->RunQuery($sql);
			$row=mysqli_fetch_array($result);
			if($row['rcds']>0){
				$rcds_recp_entered++;
			}
			$records++;
		}
		
		
		if($records==0){
				$errFlag=1;
				$msg	="No color recipies to Approve !";
		}
		else if($rcds_recp_entered	!=	$records){
				$errFlag=1;
				$msg	="Please enter recipies for all colors !";
		}
	
		
		if($errFlag!=1){
			$sql 	= "select count(intSampleNo) as rcds from trn_sample_color_recipes_approve where intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName'";
			$result = $db->RunQuery($sql);
			$row=mysqli_fetch_array($result);
			if($row['rcds']>0){
				
			}
			else{
				 $sql = "INSERT INTO `trn_sample_color_recipes_approve` 
				(`intSampleNo`,`intSampleYear`,`intRevisionNo`,`strCombo`,`strPrintName`,`intApproved`,`dtmdateApproved`,`intUser`) 
				VALUES ('$sampleNo','$sampleYear','$revNo','$combo','$printName','1',now(),'".$_SESSION["userId"]."')";
				$result 	= $db->RunQuery($sql);
				if(!$result){
					$errFlag	=1;
					$msg		="Approval Error";
				}
			}
				
			$sql = "UPDATE `trn_sample_color_recipes_approve` SET `intApproved`='1' WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName'";
			$result_app=$db->RunQuery($sql);
		}
	
		//-------------------------------------
		
		if(($result_app==1)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Approved successfully.';
		}
		else if($errFlag==1){
			$response['type'] 		= 'fail';
			$response['msg'] 		= $msg;
		}
		else
		{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}

	if($requestType=='cancelApprovedRecipForBulk')
	{
		$toSave=0;
		$saved=0;

		
			//----------------------------------
			$sampleNo 		= $_REQUEST['sampleNo'];
			$sampleYear 	= $_REQUEST['sampleYear'];
			$revNo 			= $_REQUEST['revisionNo'];
			$combo 			= $_REQUEST['combo'];
			$printName 		= $_REQUEST['printName'];
			
			//----------------------------------
			$errFlag=0;
		
		//------
		$sql 	= "SELECT
		Count(trn_orderdetails.intOrderNo) AS rcds
		FROM
		trn_sample_color_recipes_approve
		INNER JOIN trn_orderdetails ON trn_sample_color_recipes_approve.intRevisionNo = trn_orderdetails.intRevisionNo AND trn_sample_color_recipes_approve.strCombo = trn_orderdetails.strCombo AND trn_sample_color_recipes_approve.strPrintName = trn_orderdetails.strPrintName AND trn_sample_color_recipes_approve.intSampleNo = trn_orderdetails.intSampleNo AND trn_sample_color_recipes_approve.intSampleYear = trn_orderdetails.intSampleYear
		where intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName'";
		if($row['rcds']>0){
			$errFlag=1;
			$msg	="Bulk Consumption entered.Can't Cancel !";
		}
	
		//------
		$sql 		= "SELECT `trn_sample_color_recipes_approve`.`intApproved` from trn_sample_color_recipes_approve WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName'";
		$result_app	=$db->RunQuery($sql);
		$row_app	=mysqli_fetch_array($result_app);
		if($row_app['intApproved']==0){
			$errFlag=1;
			$msg	="Already cancelled !";
		}
		//------
		
		if($errFlag!=1){
				
			$sql = "UPDATE `trn_sample_color_recipes_approve` SET `intApproved`='0' WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName'";
			$result_app=$db->RunQuery($sql);
		}
	
		//-------------------------------------
		
		if(($result_app==1)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Canceled successfully.';
		}
		else if($errFlag==1){
			$response['type'] 		= 'fail';
			$response['msg'] 		= $msg;
		}
		else
		{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	
	else if($requestType=='saveTemplate')
	{
		$toSave=0;
		$saved=0;
		$arrRecipe 		= json_decode($_REQUEST['arrRecipe'], true);
			
			$color 			= $arrRecipe[0]['color'];
			$techId 		= $arrRecipe[0]['techId'];
			$inkTypeId 		= $arrRecipe[0]['inkTypeId'];
			$type	 		= $arrRecipe[0]['type'];
		
		
		$sql = "delete from trn_sample_color_recipes_template where  intColorId='$color' and intTechniqueId='$techId' and intInkTypeId='$inkTypeId' AND TYPE='$type'";
		$result = $db->RunQuery($sql);
		
		foreach($arrRecipe as $x)
		{
			$toSave++;
			$sql = "INSERT INTO `trn_sample_color_recipes_template` 
			(TYPE,`intColorId`,`intTechniqueId`,	`intInkTypeId`,`intItem`,`dblWeight`,`intUser`,`dtDate`) 
			VALUES ('$type','".$x['color']."','".$x['techId']."','".$x['inkTypeId']."','".$x['itemId']."','".val($x['weight'])."','".$_SESSION["userId"]."',now())";
			$result 	= $db->RunQuery($sql);
			$saved+=$result;
		}
		
		if(($toSave==$saved)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Template Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	else if($requestType=='addNewUndercoat')
	{
		
		$response['status'] = 0;	
		$sampleNo  		= $_REQUEST['sampleNo'];
		$sampleYear		= $_REQUEST['sampleYear'];
		$revNo			= $_REQUEST['revNo'];
		$combo			= $_REQUEST['combo'];
		$printName		= $_REQUEST['printName'];
		
		$colorId		= $_REQUEST['colorId'];
		$techniqueId	= $_REQUEST['techniqueId'];
		$inkTypeId		= $_REQUEST['inkTypeId'];
		
		$sql_ca = "select intApproved from `trn_sample_color_recipes_approve`  WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName'";
		$result_ca=$db->RunQuery($sql_ca);
		$row_ca=mysqli_fetch_array($result_ca);
		$app_flag=$row_ca['intApproved'];
	
		if(confirmedOrders($sampleNo,$sampleYear,$revNo,$combo,$printName))
		{
			$response['msg'] = 'PO confirmations already raised.cant Edit.';	
			$response['status'] = 1;	
			echo json_encode($response);
			return;
		}
		else if($app_flag==1)
		{
			$response['msg'] = 'Recipes confirmation is already raised.cant Edit.';	
			$response['status'] = 1;	
			echo json_encode($response);
			return;
		}
		
		$sql = "SELECT
					trn_sampleinfomations_details.intPrintMode,
					trn_sampleinfomations_details.intWashStanderd,
					trn_sampleinfomations_details.intGroundColor,
					trn_sampleinfomations_details.intFabricType,
					trn_sampleinfomations_details.intTechniqueId,
					trn_sampleinfomations_details.intItem,
					trn_sampleinfomations_details.intCompanyId
				FROM trn_sampleinfomations_details
				WHERE
					trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_details.intRevNo 		=  '$revNo' AND
					trn_sampleinfomations_details.strPrintName 	=  '$printName' AND
					trn_sampleinfomations_details.strComboName 	=  '$combo' AND
					trn_sampleinfomations_details.intColorId 	=  '$colorId' AND
					intParentColorId = 0
				";
		$result 			=	$db->RunQuery($sql);
		$row				=	mysqli_fetch_array($result);
		$intPrintMode 		= $row['intPrintMode'];
		$intWashStanderd 	= $row['intWashStanderd'];
		$intGroundColor 	= $row['intGroundColor'];
		$intFabricType 		= $row['intFabricType'];
		$intTechniqueId 	= $row['intTechniqueId'];
		$intItem 			= $row['intItem'];
		$intCompanyId 		= $row['intCompanyId'];
		
		//delete record
		//$sql = "DELETE FROM `trn_sampleinfomations_details` WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevNo`='$revNo') AND (`strPrintName`='$printName') AND (`strComboName`='$combo') AND (`intParentColorId`='$colorId')";
		//$db->RunQuery($sql);
		//
		if($inkTypeId!=10000)
		{
		$sql = "INSERT INTO `trn_sampleinfomations_details_technical` (`intSampleNo`,`intSampleYear`,`intRevNo`,`strPrintName`,`strComboName`,`intColorId`,`intInkTypeId`) VALUES ('$sampleNo','$sampleYear','$revNo','$printName','$combo','$colorId','10000')"; 
			/* ####
			10000 = UNDER COART , screen line requested this feature , after completed the process. 
			   #####*/
		}
		else
		{
			$sql = "DELETE FROM `trn_sampleinfomations_details_technical` WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevNo`='$revNo') AND (`strPrintName`='$printName') AND (`strComboName`='$combo') AND (`intColorId`='$colorId') AND (`intInkTypeId`='10000')  ";	
		}
		/*$sql = "INSERT INTO `trn_sampleinfomations_details` (`intSampleNo`,`intSampleYear`,`intRevNo`,`strPrintName`,`strComboName`,`intColorId`,`intParentColorId`,`intPrintMode`,`intWashStanderd`,`intGroundColor`,`intFabricType`,`intTechniqueId`,`intCompanyId`)
		 VALUES ('$sampleNo','$sampleYear','$revNo','$printName','$combo','25000','$colorId','$intPrintMode','$intWashStanderd','$intGroundColor','$intFabricType','$intTechniqueId','$intCompanyId')";*/
		$db->RunQuery($sql);
		
	}
	else if($requestType=='saveShots')
	{
		$sampleNo  		= $_REQUEST['sampleNo'];
		$sampleYear		= $_REQUEST['sampleYear'];
		$revNo			= $_REQUEST['revNo'];
		$combo			= $_REQUEST['combo'];
		$printName		= $_REQUEST['printName'];
		
		$colorId		= $_REQUEST['colorId'];
		$techniqueId	= $_REQUEST['techniqueId'];
		$inkTypeId		= $_REQUEST['inkTypeId'];	
		$shots			= $_REQUEST['shots'];	
	
		if(confirmedOrders($sampleNo,$sampleYear,$revNo,$combo,$printName))
		{
			$response['msg'] = 'PO confirmations already raised.cant Edit.';	
			$response['status'] = 1;	
			echo json_encode($response);
			return;
		}
	
		
		$sql = "UPDATE `trn_sampleinfomations_details_technical` SET `intNoOfShots`='$shots' WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevNo`='$revNo') AND (`strPrintName`='$printName') AND (`strComboName`='$combo') AND (`intColorId`='$colorId') AND (`intInkTypeId`='$inkTypeId')";
		$db->RunQuery($sql);
		
	}
	else if($requestType=='butAddAntiMigration')
	{
		$sampleNo  		= $_REQUEST['sampleNo'];
		$sampleYear		= $_REQUEST['sampleYear'];
		$revNo			= $_REQUEST['revNo'];
		$combo			= $_REQUEST['combo'];
		$printName		= $_REQUEST['printName'];
	
	
	$sql = "select intApproved from `trn_sample_color_recipes_approve`  WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName'";
	$result=$db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	$app_flag=$row['intApproved'];
	
		if(confirmedOrders($sampleNo,$sampleYear,$revNo,$combo,$printName))
		{
			$response['msg'] = 'PO confirmations is already raised.cant Edit.';	
			$response['status'] = 1;	
			echo json_encode($response);
			return;
		}
		else if($app_flag==1)
		{
			$response['msg'] = 'colour recipes confirmations is already raised.cant Edit.';	
			$response['status'] = 1;	
			echo json_encode($response);
			return;
		}
		
		if($response['status'] !=1 ){
		$sql = "SELECT * FROM 
				`trn_sampleinfomations_details_technical` 
				WHERE `intSampleNo` = '$sampleNo' 
					AND `intSampleYear` = '$sampleYear' 
					AND `intRevNo` = '$revNo' 
					AND `strPrintName` = '$printName' 
					AND `strComboName` = '$combo' 
				order by intColorId,intInkTypeId desc limit 1";	
		$result 	= $db->RunQuery($sql);
		$row 		= mysqli_fetch_array($result);
		
		$colorId		= $row['intColorId'];
		
		//10001 = ANTI MIGRATION, screen line requested this feature , after completed the process. 
	
	
	
		$sql = "INSERT INTO `trn_sampleinfomations_details_technical` (`intSampleNo`,`intSampleYear`,`intRevNo`,`strPrintName`,`strComboName`,`intColorId`,`intInkTypeId`) VALUES ('$sampleNo','$sampleYear','$revNo','$printName','$combo','$colorId','10001')";
		$result 	= $db->RunQuery($sql);
			$response['msg'] = 'Anti-migration is added successfully.';	
			$response['status'] = 0;	
			//echo json_encode($response);
			
		}
	}
	//addNewItem
	elseif($requestType=='addNewItem2')
	{
		$mainId 	= ($_REQUEST['mainId']	=='null'?'':$_REQUEST['mainId']);
		$subId 		= ($_REQUEST['subId']	=='null'?'':$_REQUEST['subId']);
		$itemId 	= ($_REQUEST['itemId']	=='null'?'':$_REQUEST['itemId']);

		$sql = "INSERT INTO `mst_inkcategory_item` (`intSubCategoryId`,`intMainCategoryId`,`intItemId`) VALUES ('$subId','$mainId','$itemId')";
		$result = $db->RunQuery($sql);
		if($result)	{
			$response['msg'] 	= 'Saved Successfully.';
			$response['status'] = 1;
		}
		else	
		{
			$response['msg'] 	= 'Saving error...';
			$response['status'] = 0;
		}
			
	}
	elseif($requestType=='copyRecipes')
	{
		$sampleNo 		= $_REQUEST['sampleNo'];
		$sampleYear 	= $_REQUEST['sampleYear'];
		
		$rev1 			= $_REQUEST['rev1'];
		$combo1 		= $_REQUEST['combo1'];
		$print1 		= $_REQUEST['print1'];
		
		$rev2 			= $_REQUEST['rev2'];
		$combo2 		= $_REQUEST['combo2'];
		$print2 		= $_REQUEST['print2'];
		
		if(haveTOvalues($sampleNo,$sampleYear,$rev2,$combo2,$print2))
		{
			$response['msg'] = 'Records already exist.';	
			$response['status'] = 1;	
			echo json_encode($response);
			return;
		}
		$sql			= " SELECT
								trn_sampleinfomations_details_technical.intColorId,
								trn_sampleinfomations_details_technical.intInkTypeId
							FROM
								trn_sampleinfomations_details_technical
							WHERE
								trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
								trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
								trn_sampleinfomations_details_technical.intRevNo 		=  '$rev2' AND
								trn_sampleinfomations_details_technical.strPrintName 	=  '$print2' AND
								trn_sampleinfomations_details_technical.strComboName 	=  '$combo2'
							";
		$result			= $db->RunQuery($sql);
		$count = 0;
		while($row=mysqli_fetch_array($result))
		{
			$intColorId 	= $row['intColorId'];
			$intInkTypeId 	= $row['intInkTypeId'];
			
			$sql2	= " SELECT
							trn_sample_color_recipes.intTechniqueId,
							trn_sample_color_recipes.intItem,
							trn_sample_color_recipes.dblWeight
						FROM
							trn_sample_color_recipes
						WHERE
							trn_sample_color_recipes.intSampleNo 		=  '$sampleNo' AND
							trn_sample_color_recipes.intSampleYear 		=  '$sampleYear' AND
							trn_sample_color_recipes.intRevisionNo 		=  '$rev1' AND
							trn_sample_color_recipes.strCombo 			=  '$combo1' AND
							trn_sample_color_recipes.strPrintName 		=  '$print1' AND
							trn_sample_color_recipes.intColorId 		=  '$intColorId' AND
							trn_sample_color_recipes.intInkTypeId 		=  '$intInkTypeId'
						";
			$result2	= $db->RunQuery($sql2);
			
			while($row2=mysqli_fetch_array($result2))
			{
				$intTechniqueId 	= $row2['intTechniqueId'];
				$intItem 			= $row2['intItem'];
				$dblWeight 			= $row2['dblWeight'];
				
				$sql3 = "INSERT INTO `trn_sample_color_recipes` (`intSampleNo`,`intSampleYear`,`intRevisionNo`,`strCombo`,`strPrintName`,`intColorId`,`intTechniqueId`,`intInkTypeId`,`intItem`,`dblWeight`,`intUser`,`dtDate`)
				 VALUES ('$sampleNo','$sampleYear','$rev2','$combo2','$print2','$intColorId','$intTechniqueId','$intInkTypeId','$intItem','$dblWeight','$userId',now())";
				$result3	= $db->RunQuery($sql3);
				if($result3)
					$count++;
				//die();
			}
		}
		
			$response['msg'] = $count.' Records copied.';
			$response['status'] = 1;	
	}
	echo json_encode($response);
	
	function haveTOvalues($sampleNo,$sampleYear,$revNo,$combo,$print)
	{
		global $db;
		$sql  	= " SELECT
						trn_sample_color_recipes.intInkTypeId
						FROM trn_sample_color_recipes
					WHERE
						trn_sample_color_recipes.intSampleNo =  '$sampleNo' AND
						trn_sample_color_recipes.intSampleYear =  '$sampleYear' AND
						trn_sample_color_recipes.intRevisionNo =  '$revNo' AND
						trn_sample_color_recipes.strCombo =  '$combo' AND
						trn_sample_color_recipes.strPrintName =  '$print'
					";
		$result	= $db->RunQuery($sql);
		if(mysqli_num_rows($result)>0)
			return true;
		else
			return false;
	}
	
	//---------------------------------------
	function confirmedOrders($sampleNo,$sampleYear,$revNo,$combo,$printName){
		global $db;
	 	$sql  	= " SELECT
					trn_orderheader.intOrderNo,
					trn_orderheader.intOrderYear
					FROM
					trn_orderheader
					Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					WHERE
					trn_orderdetails.intSampleNo =  '$sampleNo' AND
					trn_orderdetails.intSampleYear =  '$sampleYear' AND
					trn_orderheader.intApproveLevelStart-
					trn_orderheader.intStatus >=  2 AND
					trn_orderdetails.strCombo =  '$combo' AND
					trn_orderdetails.strPrintName =  '$printName' AND
					trn_orderdetails.intRevisionNo =  '$revNo'
					";
		$result	= $db->RunQuery($sql);
		if(mysqli_num_rows($result)>0)
			return true;
		else
			return false;
	}
		
?>
<?php
	session_start();
	$backwardseperator 		= "../../../../../";
	$mainPath 				= $_SESSION['mainPath'];
	$sessionUserId 			= $_SESSION['userId'];
	$thisFilePath 			=  $_SERVER['PHP_SELF'];
	$locationId				= $_SESSION["CompanyID"];

	include  	"{$backwardseperator}dataAccess/Connector.php";
	
	$sampleNo  		= $_REQUEST['sampleNo'];
	$sampleYear		= $_REQUEST['sampleYear'];
	$revNo			= $_REQUEST['revNo'];
	$combo			= $_REQUEST['combo'];
	$printName		= $_REQUEST['printName'];
	
	$colorId		= $_REQUEST['colorId'];
	$techniqueId	= $_REQUEST['techniqueId'];
	$inkTypeId		= $_REQUEST['inkTypeId'];

//--get reference-----------------------------------------------
 $sql = "SELECT
		GROUP_CONCAT(mst_item.strName) AS ITEM 
		FROM
		trn_sampleinfomations_details
		INNER JOIN mst_item ON trn_sampleinfomations_details.intItem = mst_item.intId
		WHERE
				trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
				trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
				trn_sampleinfomations_details.intRevNo =  '$revNo' AND
				trn_sampleinfomations_details.strComboName =  '$combo' AND
				trn_sampleinfomations_details.strPrintName =  '$printName' AND
				trn_sampleinfomations_details.intColorId =  '$colorId' AND
				trn_sampleinfomations_details.intTechniqueId =  '$techniqueId'
		GROUP BY
		trn_sampleinfomations_details.intSampleNo,
		trn_sampleinfomations_details.intSampleYear,
		trn_sampleinfomations_details.intRevNo,
		trn_sampleinfomations_details.strPrintName,
		trn_sampleinfomations_details.strComboName,
		trn_sampleinfomations_details.intColorId,
		trn_sampleinfomations_details.intTechniqueId
		";
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	$reference	= $row['ITEM'];
//--------------------------------------------------------------
 $sql = "SELECT
			trn_sampleinfomations_details_technical.intNoOfShots,
			trn_sampleinfomations_details_technical.dblColorWeight,
			trn_sampleinfomations_details_technical.intColorId,
			trn_sampleinfomations_details_technical.intInkTypeId as intInkType,
			mst_inktypes.strName AS inkTypeName,
			mst_colors.strName AS colorName,
			trn_sampleinfomations_details.intTechniqueId as intTechnique,
			mst_techniques.strName AS technique
		FROM
			trn_sampleinfomations_details_technical
			Inner Join mst_inktypes ON mst_inktypes.intId = trn_sampleinfomations_details_technical.intInkTypeId
			Inner Join mst_colors ON mst_colors.intId = trn_sampleinfomations_details_technical.intColorId
			Inner Join trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_details_technical.intSampleNo AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_details_technical.intRevNo AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_details_technical.strPrintName AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_details_technical.strComboName AND trn_sampleinfomations_details.intColorId = trn_sampleinfomations_details_technical.intColorId
			Inner Join mst_techniques ON mst_techniques.intId = trn_sampleinfomations_details.intTechniqueId
		WHERE
			trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
			trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
			trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND
			trn_sampleinfomations_details_technical.strPrintName 	=  '$printName' AND
			trn_sampleinfomations_details_technical.intColorId 	=  '$colorId' AND
			trn_sampleinfomations_details.intTechniqueId 	=  '$techniqueId' AND
			trn_sampleinfomations_details_technical.intInkTypeId 	=  '$inkTypeId'
	";
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	$str='Color: '.$row['colorName'].'/   Technique: '.$row['technique'].'/   Ink Type: '.$row['inkTypeName'];
//-----------------------------
		$sql 	= "SELECT
		Count(trn_orderdetails.intOrderNo) AS rcds
		FROM
		trn_orderdetails 
		INNER JOIN ware_bulkallocation_details ON trn_orderdetails.intOrderNo = ware_bulkallocation_details.intOrderNo 
		AND trn_orderdetails.intOrderYear = ware_bulkallocation_details.intOrderYear 
		AND trn_orderdetails.intSalesOrderId = ware_bulkallocation_details.intSalesOrderId		
		where trn_orderdetails.intSampleNo='$sampleNo' 
		and trn_orderdetails.intSampleYear='$sampleYear' 
		and trn_orderdetails.intRevisionNo='$revNo' 
		and trn_orderdetails.strCombo='$combo' 
		and trn_orderdetails.strPrintName='$printName'";
		$result=$db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		//$app_permision=1;
		$butViewFlag = 1;	
		if($row['rcds']>0){
			$butViewFlag = 0;	
		}
		
		$sql = "select intApproved from `trn_sample_color_recipes_approve`  WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName'";
		$result=$db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$app_flag=$row['intApproved'];


		$sql = "select intItem from `trn_sample_color_recipes_template`  WHERE intColorId='$colorId' and intTechniqueId='$techniqueId' and intInkTypeId='$inkTypeId'  and TYPE='0' ";
		$result=$db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		if($row['intItem']>0)
		$savedTemplateLight=1;

		$sql = "select intItem from `trn_sample_color_recipes_template`  WHERE intColorId='$colorId' and intTechniqueId='$techniqueId' and intInkTypeId='$inkTypeId'  and TYPE='1' ";
		$result=$db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		if($row['intItem']>0)
		$savedTemplateDark=1;
//-----------------------------
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="css.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table bgcolor="#000000" width="1010" height="709" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#333333" align="right"></td>
    <td bgcolor="#333333"></td>
</tr>
  <tr>
    <td width="413" align="center" valign="top"><select name="cboItemTypeMain" size="1" multiple="multiple" id="cboItemTypeMain" style="height:240px;width:200px"><?php 
		$sql = "SELECT
				mst_inkmaincategory.intInkCategoryId,
				mst_inkmaincategory.strInkCategory
				FROM mst_inkmaincategory
				ORDER BY
				mst_inkmaincategory.strInkCategory ASC

				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
		?>
		<option value="<?Php echo $row['intInkCategoryId']; ?>"><?php echo $row['strInkCategory']; ?></option>
		<?php
		}
	 ?></select>
      <select name="cboItemTypeSub" size="1" multiple="multiple" id="cboItemTypeSub" style="height:240px;width:200px">
      </select></td>
     <td width="597" style="width:100px">
       <table width="500" border="0" cellspacing="0" cellpadding="0">
         <tr>
           <td width="391" height="20" align="left" valign="middle"><input style="width:400px;height:50px" type="text" name="txtSearchItem" id="txtSearchItem" /></td>
           <td width="109" height="20" align="left" valign="middle"><div id="butSearchItem" style="width:50px;height:35px" class="button orange" >Search</div></td>
         </tr>
         <tr>
           <td height="67" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                 <td width="78%"><select name="cboItemList" size="1" multiple="multiple" id="cboItemList" style="height:190px;width:400px">
                 </select></td>
                 <td width="22%" valign="bottom"><div id="butAddItemToGrid" style="width:50px;height:35px" class="button green" >ADD</div></td>
              </tr>
           </table></td>
         </tr>
       </table>
     </td>
  </tr>
  <tr>
    <td height="486" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="3"><div style="width:100%;height:400px;color:#FFF"><div id="msg" align="center" style="width:100%;height:30px;color:#3F6"></div>
          <table id="tblPopUpGrid1" width="100%" border="0" cellspacing="0" cellpadding="0" class="tableBorder_allRound">
          <thead>
            <tr>
			<th width="16%"><?php if($app_flag !=1){ ?>
			  <div id="butSaveTemplate" style="width:100px;height:35px" class="button white" >Save Template-Light</div><?php } ?></th>
			<th width="16%" bgcolor=""><?php if($app_flag !=1){ ?>
			  <div id="butSaveTemplateDark" style="width:100px;height:35px" class="button white" ><font color="#000000"><b>Save Template-Dark</b></font></div>
			  <?php } ?></th>
			<th width="16%"><?php if($savedTemplateLight==1){ ?><div id="butLoadTemplate" style="width:100px;height:35px" class="button green" >Load Template-Light</div><?php } ?></th>
			<th width="16%"><?php if($savedTemplateDark==1){ ?><div id="butLoadTemplateDark" style="width:100px;height:35px" class="button green"><font color="#000000"><b>Load Template-Dark</b></font></div><?php } ?></th>
			<td width="52%"><div id="msgTemplate" align="center" style="width:100%;height:30px;color:#3F6"></div></td>
            </tr>
            </thead>
            </table>
          <table id="tblPopUpGrid2" width="100%" border="0" cellspacing="0" cellpadding="0" class="tableBorder_allRound">
          <thead>
            <tr>
              <td colspan="3" width="65%" align="left"><?php echo $str; ?><br><span style="color:#9F3" >Reference : <?php echo $reference ; ?></span></td>
              	<?php		$sqlt = "SELECT
							IFNULL(sum(trn_sample_color_recipes.dblWeight),0) as tot 
							FROM
							trn_sample_color_recipes
							Inner Join mst_item ON mst_item.intId = trn_sample_color_recipes.intItem
							WHERE
							trn_sample_color_recipes.intSampleNo =  '$sampleNo' AND
							trn_sample_color_recipes.intSampleYear =  '$sampleYear' AND
							trn_sample_color_recipes.intRevisionNo =  '$revNo' AND
							trn_sample_color_recipes.strCombo =  '$combo' AND
							trn_sample_color_recipes.strPrintName =  '$printName' AND
							trn_sample_color_recipes.intColorId =  '$colorId' AND
							trn_sample_color_recipes.intTechniqueId =  '$techniqueId' AND
							trn_sample_color_recipes.intInkTypeId =  '$inkTypeId'
							AND mst_item.intStatus	=	1
							";
					$resultt = $db->RunQuery($sqlt);
					$rowt=mysqli_fetch_array($resultt);
						$tot=$rowt['tot'];
                        ?>

              <td width="10%" bgcolor="#CCCCCC"><input disabled="disabled" value="<?php echo $tot; ?>" style="width:100px;height:50px" class="txtTotQty" type="text" name="txtTotQty" id="txtTotQty" /></td>
              <td width="10%" bgcolor="#CCCCCC"><input value="" style="width:100px;height:50px" class="txtNewQty" type="text" name="txtNewQty" id="txtNewQty" /></td>
              <td colspan="2" align="left"><div id="butCalNew" style="width:50px;height:35px" class="button green" >Calculate</div></td>
            </tr>
            </thead>
            </table>
          <table id="tblPopUpGrid" width="100%" border="0" cellspacing="0" cellpadding="0" class="tableBorder_allRound">
          <thead>
            <tr>
              <td width="5%" height="32">&nbsp;</td>
              <td width="10%" align="center" bgcolor="#006666"><strong>REMOVE</strong></td>
              <td width="50%" align="center" bgcolor="#006666"><strong>ITEM</strong></td>
              <td colspan="3" align="center" bgcolor="#006666"><strong>WEIGHT(g)</strong></td>
              <td width="10%">&nbsp;</td>
            </tr>
            </thead>
            <tbody>
            	<?PHp
					$sql = "SELECT
							trn_sample_color_recipes.intItem,
							trn_sample_color_recipes.intAddedByColorRoom,
							mst_item.strName,
							trn_sample_color_recipes.dblWeight
							FROM
							trn_sample_color_recipes
							Inner Join mst_item ON mst_item.intId = trn_sample_color_recipes.intItem
							WHERE
							trn_sample_color_recipes.intSampleNo =  '$sampleNo' AND
							trn_sample_color_recipes.intSampleYear =  '$sampleYear' AND
							trn_sample_color_recipes.intRevisionNo =  '$revNo' AND
							trn_sample_color_recipes.strCombo =  '$combo' AND
							trn_sample_color_recipes.strPrintName =  '$printName' AND
							trn_sample_color_recipes.intColorId =  '$colorId' AND
							trn_sample_color_recipes.intTechniqueId =  '$techniqueId' AND
							trn_sample_color_recipes.intInkTypeId =  '$inkTypeId'
							AND mst_item.intStatus	=	1
							";
					$result = $db->RunQuery($sql);
					$saved_rec_item=0;
					while($row=mysqli_fetch_array($result))
					{
						$saved_rec_item++;
						$bgCol="#CCCCCC";
						if($row['intAddedByColorRoom']==1){
							$bgCol="#FFCCE6";
						}
					?>
                    	<tr id="<?php echo $row['intItem']; ?>"><td>&nbsp;</td><td id="removeRow" align="center" bgcolor="<?php echo $bgCol;?>" style="color:#F00">DEL</td><td style="color:black" bgcolor="<?php echo $bgCol;?> "><?php echo $row['strName']; ?></td>
                    	  <td width="10%" bgcolor="#CCCCCC"><input value="<?php echo $row['dblWeight']; ?>" style="width:100px;height:50px" class="txtQty2" type="text" name="txtQty2" id="txtQty2" /></td>
                    	  <td width="10%" bgcolor="#CCCCCC"><input id="txtQty" class="hex txtQty ui-keyboard-input ui-widget-content ui-corner-all" type="text" aria-haspopup="true" style="width:100px;height:50px; text-align:right" value="" role="textbox"></td>
                   	    <td width="15%" bgcolor="#CCCCCC"><div id="butCal" style="width:50px;height:35px" class="button green" >+</div></td><td>&nbsp;</td></tr>
                    <?php	
					}
				?>
           
            	<?PHp
				if($saved_rec_item==0){
					  $sql = "SELECT
							trn_sampleinfomations_details.intItem,
							0 as dblColorWeight,
							mst_item.strCode,
							mst_item.strName
							FROM 
							trn_sampleinfomations_details
							INNER JOIN mst_item ON trn_sampleinfomations_details.intItem = mst_item.intId
							WHERE
							trn_sampleinfomations_details.intSampleNo = '$sampleNo' AND
							trn_sampleinfomations_details.intSampleYear = '$sampleYear' AND
							trn_sampleinfomations_details.intRevNo = '$revNo' AND
							trn_sampleinfomations_details.strComboName = '$combo' AND 
							trn_sampleinfomations_details.strPrintName = '$printName' AND 
							trn_sampleinfomations_details.intTechniqueId = '$techniqueId' AND
							trn_sampleinfomations_details.intColorId = '$colorId' AND
							mst_item.intUOM = 6 group by mst_item.intId order by mst_item.strName asc
							";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$bgCol="#5FBCAC";
					?>
                    	<tr id="<?php echo $row['intItem']; ?>"><td>&nbsp;</td><td id="removeRow" align="center" bgcolor="<?php echo $bgCol;?>" style="color:#F00">DEL</td><td style="color:black" bgcolor="<?php echo $bgCol;?> "><?php echo $row['strName']; ?><br><font color="#26782A"><b>(Not Saved-Suggested by Marketing Dep)</b></font></td>
                    	  <td width="10%" bgcolor="#CCCCCC"><input value="<?php echo $row['dblColorWeight']; ?>" style="width:100px;height:50px" class="txtQty2" type="text" name="txtQty2" id="txtQty2" /></td>
                    	  <td width="10%" bgcolor="<?php echo $bgCol ?>"><input id="txtQty" class="hex txtQty ui-keyboard-input ui-widget-content ui-corner-all" type="text" aria-haspopup="true" style="width:100px;height:50px; text-align:right" value="" role="textbox"></td>
                   	    <td width="15%" bgcolor="<?php echo $bgCol ?>"><div id="butCal" style="width:50px;height:35px" class="button green" >+</div></td><td>&nbsp;</td></tr>
                    <?php	
					}
				}
				?>
           
            </tbody>
            </table>
        </div></td>
      </tr>
    </table></td>
  </tr>
  <?php
//--------2015-03-06----------------------------

if($sampleNo){
	$sql		="SELECT
					trn_sampleinfomations.intStatus,
					trn_sampleinfomations.intTechnicalStatus
					FROM `trn_sampleinfomations`
					WHERE
					trn_sampleinfomations.intSampleNo = '$sampleNo' AND
					trn_sampleinfomations.intSampleYear = '$sampleYear' AND
					trn_sampleinfomations.intRevisionNo = '$revNo'
					";	
	$results 	= $db->RunQuery($sql);
	$row		=mysqli_fetch_array($results);
	$sampStatus 	= $row['intStatus'];
	$techStatus 	= $row['intTechnicalStatus'];
}
//---2015-03-18-----------------------
 	$sql 		= "SELECT `trn_sample_color_recipes_approve`.`intApproved` from trn_sample_color_recipes_approve WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName'";
	$result_app	=$db->RunQuery($sql);
	$row_app	=mysqli_fetch_array($result_app);
	$flag_recipe_approved	= $row_app['intApproved'];
 //--------------------------

$viewSaveButton=0;
if($sampleNo=='' || $sampleYear=='' || $revNo=='')
	$viewSaveButton=1;
if($flag_recipe_approved!=1 && $techStatus==1)
	$viewSaveButton=1;
 
//------------------------------------
?>
  
      <tr>
        
        <td align="center" colspan="2"><?php  if($butViewFlag==1 && $viewSaveButton==1){ ?><div style="height:28px" align="right" id="butSave2" class="button orange" >SAVE</div><?php  } ?><div style="height:28px" align="right" id="butClose_popup" class="button pink" >CLOSE</div></td>
        
      </tr>
</table>
</body>
</html>
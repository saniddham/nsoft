
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Color Room Item Allocations</title>

	<link href="css.css" rel="stylesheet" />
	<!-- jQuery & jQuery UI + theme (required) -->
	<link href="other/jquery-ui.css" rel="stylesheet">
	<script src="other/jquery.js"></script>
	<script src="other/jquery-ui.min.js"></script>

	<!-- keyboard widget css & script (required) -->
	<link href="css/keyboard.css" rel="stylesheet">
	<script src="js/jquery.keyboard.js"></script>

	<!-- keyboard extensions (optional) -->
	<script src="js/jquery.mousewheel.js"></script>
	<script src="js/jquery.keyboard.extension-typing.js"></script>
	<script src="js/jquery.keyboard.extension-autocomplete.js"></script>

	<!-- demo only -->
	<link href="demo/demo.css" rel="stylesheet">
	<script src="demo/demo.js"></script>
	<script src="other/jquery.jatt.min.js"></script> <!-- tooltips -->

	<!-- theme switcher -->
	<script src="other/themeswitchertool.htm"></script>

	<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
    <script type="application/javascript" src="index.js"></script>
	<script>
		$(function(){
			$('#keyboard').keyboard();
		});
		var projectName = '/<?php echo $_SESSION["projectName"]; ?>' ;
	</script>
  
    <script type="application/javascript" src="bulkStockAllocation_inkType-js.js"></script>
</head>

<body>
<table width="1024" height="111" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td height="24"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="25%">Color Room Requisition Note</td>
          <td width="49%">&nbsp;</td>
          <td width="13%"><a id="butSearch" href="../logout.php" class="white button logout" style="height:25px;margin-top:5px; width:65px">Home</a></td>
          <td width="13%"><a id="butSearch" href="../logout.php" class="red button logout" style="height:25px;margin-top:5px; width:65px">Log Out</a></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="13%"><span class="label">ORDER YEAR</span></td>
          <td width="13%"><select name="cboOrderYear" class="down"   id="cboOrderYear" >
            <?php
				    $d = date('Y');
					if($orderYear=='')
						$orderYear = $d;
				  	for($d;$d>=2012;$d--)
					{
						if($d==$orderYear)
							echo "<option selected=\"selected\" id=\"$d\" >$d</option>";	
						else
							echo "<option id=\"$d\" >$d</option>";	
					}
		?>
          </select></td>
          <td width="12%"><span class="label">GRAPHIC NO</span></td>
          <td width="17%"><input type="text" style="height:50px;margin-top:5px; width:155px" class="txtGraphicNo" id="num"   role="textbox" value="<?php echo $graphicNo; ?>" /></td>
          <td width="11%"><span class="label">ORDER NO</span></td>
          <td width="20%"><input type="text" style="height:50px;margin-top:5px; width:155px" class="txtGraphicNo" id="num2"   role="textbox" value="<?php echo $graphicNo; ?>" /></td>
          <td width="14%"><a id="butSearch" class="orange button search" style="height:35px;margin-top:5px">Search</a></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="33%"><table width="100%" border="0" cellspacing="1" cellpadding="0" class="tableBorder_allRound" style="background:#FFF;margin-top:5px">
            <tr style="color:#FF0">
              <td bgcolor="#333333" width="92%" style="text-align:center" height="25">GRAPHIC NO</td>
            </tr>
            <tr>
              <td bgcolor="#333333" height="25">Graphic No 1</td>
            </tr>
            <tr>
              <td bgcolor="#333333" height="25">Graphic No 2</td>
            </tr>
            <tr>
              <td bgcolor="#333333" height="25">Graphic No 3</td>
            </tr>
            <tr>
              <td bgcolor="#333333" height="25">Graphic No 4</td>
            </tr>
            <tr>
              <td bgcolor="#333333" height="25">Graphic No 5</td>
            </tr>
          </table></td>
          <td width="3%">&nbsp;</td>
          <td width="31%"><table width="100%" border="0" cellspacing="1" cellpadding="0" class="tableBorder_allRound" style="background:#FFF;margin-top:5px">
            <tr style="color:#FF0">
              <td bgcolor="#333333" width="7%" style="text-align:center" height="25"><input type="checkbox" style="height:10px;width:15px" class="txtGraphicNo" id="num3"   role="checkbox" /></td>
              <td bgcolor="#333333" width="93%" style="text-align:center" height="25">ORDER NO / SALES ORDER NO</td>
            </tr>
            <tr>
              <td bgcolor="#333333" height="25"><input type="checkbox" style="height:10px;width:15px" class="txtGraphicNo" id="num4" role="checkbox" checked="checked" /></span></td>
              <td bgcolor="#333333" height="25">Order No 1 / Sales Order 1</td>
            </tr>
            <tr>
              <td bgcolor="#333333" height="25"><input type="checkbox" style="height:10px;width:15px" class="txtGraphicNo" id="num5" role="checkbox" /></td>
              <td bgcolor="#333333" height="25">Order No 2 / Sales Order 2</td>
            </tr>
            <tr>
              <td bgcolor="#333333" height="25"><input type="checkbox" style="height:10px;width:15px" class="txtGraphicNo" id="num6" role="checkbox" /></td>
              <td bgcolor="#333333" height="25">Order No 3 / Sales Order 3</td>
            </tr>
            <tr>
              <td bgcolor="#333333" height="25"><input type="checkbox" style="height:10px;width:15px" class="txtGraphicNo" id="num7" role="checkbox" /></td>
              <td bgcolor="#333333" height="25">Order No 4 / Sales Order 4</td>
            </tr>
            <tr>
              <td bgcolor="#333333" height="25"><input type="checkbox" style="height:10px;width:15px" class="txtGraphicNo" id="num8"   role="checkbox" /></td>
              <td bgcolor="#333333" height="25"><span style="text-align:center">Order No</span> 5 / Sales Order 5</td>
            </tr>
          </table></td>
          <td width="2%">&nbsp;</td>
          <td width="31%"><table width="100%" border="0" cellspacing="1" cellpadding="0" class="tableBorder_allRound" style="background:#FFF;margin-top:5px">
            <tr style="color:#FF0">
              <td bgcolor="#333333" width="7%" style="text-align:center" height="25"><input type="checkbox" style="height:10px;width:15px" class="txtGraphicNo" id="num9"   role="checkbox" /></td>
              <td bgcolor="#333333" width="60%" style="text-align:center" height="25">ITEM DESCRIPTION</td>
              <td bgcolor="#333333" width="33%" style="text-align:center" height="25">REQ. WEIGHT</td>
            </tr>
            <tr>
              <td bgcolor="#333333" height="25"><input type="checkbox" style="height:10px;width:15px" class="txtGraphicNo" id="num10"   role="checkbox" checked="checked"/></td>
              <td bgcolor="#333333" height="25">Item 1</td>
              <td bgcolor="#333333" align="right" height="25">20 Kg</td>
            </tr>
            <tr>
              <td bgcolor="#333333" height="25"><span style="text-align:center">
                <input type="checkbox" style="height:10px;width:15px" class="txtGraphicNo" id="num11"   role="checkbox" checked="checked"/>
              </span></td>
              <td bgcolor="#333333" height="25">Item 2</td>
              <td bgcolor="#333333" align="right" height="25">20 Kg</td>
            </tr>
            <tr>
              <td bgcolor="#333333" height="25"><span style="text-align:center">
                <input type="checkbox" style="height:10px;width:15px" class="txtGraphicNo" id="num12"   role="checkbox" />
              </span></td>
              <td bgcolor="#333333" height="25">Item 3</td>
              <td bgcolor="#333333" align="right" height="25">20 Kg</td>
            </tr>
            <tr>
              <td bgcolor="#333333" height="25"><span style="text-align:center">
                <input type="checkbox" style="height:10px;width:15px" class="txtGraphicNo" id="num13"   role="checkbox" />
              </span></td>
              <td bgcolor="#333333" height="25">Item 4</td>
              <td bgcolor="#333333" align="right" height="25">20 Kg</td>
            </tr>
            <tr>
              <td bgcolor="#333333" height="25"><span style="text-align:center">
                <input type="checkbox" style="height:10px;width:15px" class="txtGraphicNo" id="num14"   role="checkbox" />
              </span></td>
              <td bgcolor="#333333" height="25">Item 5</td>
              <td bgcolor="#333333" align="right" height="25">20 Kg</td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table class="tableBorder_allRound"  style="background:#FFF;margin-top:5px" width="100%" border="0" cellspacing="1" cellpadding="0" id="tblParent">
        <tr style="color:#FF0">
          <td width="7%" align="center" bgcolor="#333333">REMOVE</td>
          <td width="37%" align="center" bgcolor="#333333">ITEM</td>
          <td width="9%" align="center" bgcolor="#333333">UOM</td>
          <td width="11%" align="center" bgcolor="#333333">RECEIVED QTY</td>
          <td width="13%" align="center" bgcolor="#333333">WEIGHT/QTY</td>
          <td width="13%" align="center" bgcolor="#333333">EXTRA</td>
          <td width="10%" align="center" bgcolor="#333333">ORDER WISE</td>
        </tr>        
        <tr>
          <td bgcolor="#333333" style="text-align:center"><a  id="butAdd" class="red button small" style="height:35px;vertical-align:central">Remove</a></td>
          <td bgcolor="#333333">Item 1</td>
          <td bgcolor="#333333" style="text-align:center">KG</td>
          <td bgcolor="#333333" style="text-align:center">50</td>
          <td bgcolor="#333333" style="text-align:center"><span style="text-align:center">
            <input id="txtProdQty3" class="hex txtQty ui-keyboard-input ui-widget-content ui-corner-all" type="text" value="<?php echo $Qty; ?>" role="textbox" aria-haspopup="true" style="height:30px;margin-top:3px;margin-bottom:3px; width:110px; alignment-adjust:before-edge; text-align:right" />
          </span></td>
          <td bgcolor="#333333" style="text-align:center"><input id="txtProdQty2" class="hex txtQty ui-keyboard-input ui-widget-content ui-corner-all" type="text" value="<?php echo $Qty; ?>" role="textbox" aria-haspopup="true" style="height:30px;margin-top:3px;margin-bottom:3px; width:110px; alignment-adjust:before-edge; text-align:right" /></td>
          <td bgcolor="#333333" style="text-align:center"><a  id="butAdd" class="green button " style="height:25px;margin:2px 2px 2px 2px;">Add</a></td>
        </tr>
        <tr>
          <td bgcolor="#333333" style="text-align:center"><a  id="butAdd" class="red button small" style="height:35px;vertical-align:central">Remove</a></td>
          <td bgcolor="#333333">Item 2</td>
          <td bgcolor="#333333" style="text-align:center">KG</td>
          <td bgcolor="#333333" style="text-align:center">50</td>
          <td bgcolor="#333333" style="text-align:center"><span style="text-align:center">
            <input id="txtProdQty" class="hex txtQty ui-keyboard-input ui-widget-content ui-corner-all" type="text" value="<?php echo $Qty; ?>" role="textbox" aria-haspopup="true" style="height:30px;margin-top:3px;margin-bottom:3px; width:110px; alignment-adjust:before-edge; text-align:right" />
          </span></td>
          <td bgcolor="#333333" style="text-align:center"><input id="txtProdQty4" class="hex txtQty ui-keyboard-input ui-widget-content ui-corner-all" type="text" value="<?php echo $Qty; ?>" role="textbox" aria-haspopup="true" style="height:30px;margin-top:3px;margin-bottom:3px; width:110px; alignment-adjust:before-edge; text-align:right" /></td>
          <td bgcolor="#333333" style="text-align:center"><a  id="butAdd" class="green button " style="height:25px;margin:2px 2px 2px 2px;">Add</a></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td style="text-align:center"><a class="button green" style="height:25px;margin:2px 2px 2px 2px;">SAVE</a></td>
    </tr>
  </table>
			
		</div>
		<p>
		  
</p>
	
</body>
</html>
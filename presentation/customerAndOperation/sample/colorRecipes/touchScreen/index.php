<?php
ini_set('display_errors',0);
	session_start();
	//print_r($_SESSION);
	
	$backwardseperator 		= "../../../../../";
	$mainPath 				= $_SESSION['mainPath'];
	$sessionUserId 			= $_SESSION['userId'];
	$thisFilePath 			=  $_SERVER['PHP_SELF'];
	$locationId				= $_SESSION["CompanyID"];

	require_once "{$backwardseperator}dataAccess/permisionCheck_touch1.inc";
	require_once $backwardseperator."class/cls_commonErrorHandeling_get.php";

	$obj_commonErr	= new cls_commonErrorHandeling_get($db);
	
	$sampleNo  		= $_REQUEST['sampleNo'];
	$sampleYear		= $_REQUEST['sampleYear'];
	$revNo			= $_REQUEST['revNo'];
	$combo			= $_REQUEST['combo'];
	$printName		= $_REQUEST['printName'];
	$groundColour	= get_ground_colour($sampleNo,$sampleYear,$revNo,$combo,$printName);
	
	$mode = 'first';
	if(isset($sampleNo) && isset($sampleYear) && isset($revNo) && isset($combo) && isset($printName))
	{
		$mode = 'last';
	}
	else if(isset($sampleNo) && isset($sampleYear))
	{
		$mode = "second";	
	}
	//combo
	$programCode='P545';

	$app_permision 		= get_app_permision($sessionUserId,$programCode,'int1Approval');
	$special_view_bulk	= $obj_commonErr->Load_special_menupermision('59',$sessionUserId,'RunQuery')
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sample Recipes</title>

	<link href="css.css" rel="stylesheet" />
	<!-- jQuery & jQuery UI + theme (required) -->
	<link href="other/jquery-ui.css" rel="stylesheet">
	<script src="other/jquery.js"></script>
	<script src="other/jquery-ui.min.js"></script>

	<!-- keyboard widget css & script (required) -->
	<link href="css/keyboard.css" rel="stylesheet">
	<script src="js/jquery.keyboard.js"></script>

	<!-- keyboard extensions (optional) -->
	<script src="js/jquery.mousewheel.js"></script>
	<script src="js/jquery.keyboard.extension-typing.js"></script>
	<script src="js/jquery.keyboard.extension-autocomplete.js"></script>

	<!-- demo only -->
	<link href="demo/demo.css" rel="stylesheet">
	<script src="demo/demo.js"></script>
	<script src="other/jquery.jatt.min.js"></script> <!-- tooltips -->

	<!-- theme switcher -->
	<script src="other/themeswitchertool.htm"></script>

	<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>
    <script type="application/javascript" src="index.js"></script>
	<script>
		$(function(){
			$('#keyboard').keyboard();
		});
		var projectName = '/<?php echo $_SESSION["projectName"]; ?>' ;
	</script>
  
    <script type="application/javascript" src="colorRecipes-js.js"></script>
</head>

<body>
<table width="1024" height="199" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="145" height="48" class="label" >&nbsp;SAMPLE YEAR</td>
      <td width="131"><select name="cboSampleYear" class="down"   id="cboSampleYear" >
      	<?php
				    $d = date('Y');
					if($sampleYear=='')
						$sampleYear = $d;
				  	for($d;$d>=2012;$d--)
					{
						if($d==$sampleYear)
							echo "<option selected=\"selected\" id=\"$d\" >$d</option>";	
						else
							echo "<option id=\"$d\" >$d</option>";	
					}
		?>
      </select></td>
      <td width="117" class="label">SAMPLE NO</td>
      <td width="287"><input type="text" style="height:50px;margin-top:5px" class="hex txtSampleNo" id="num" aria-haspopup="true" role="textbox" value="<?php echo $sampleNo; ?>" /></td>
      <td width="117">&nbsp;<a id="butSearch" class="orange button" style="height:35px;margin-top:5px">Search</a></td>
      <td width="101" style="text-align:right"><?php if($special_view_bulk ==1){  ?><a id="butBulk" href="../../../bulk/bulkStockAllocation_inkType/index.php" class="white button" style="height:35px;margin-top:5px">&nbsp;BULK&nbsp;</a><?php } ?></td>
      <td width="126" style="text-align:right"><a id="butSearch" href="../logout.php" class="pink button" style="height:35px;margin-top:5px">LOG OUT</a></td>
    </tr>
    
    <?php 
			
$sql = "SELECT
		 DISTINCT 
		trn_orderheader.intOrderNo,
		trn_orderheader.intOrderYear
		FROM
		trn_orderheader
		Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
		WHERE
		trn_orderdetails.intSampleNo =  '$sampleNo' AND
		trn_orderdetails.intSampleYear =  '$sampleYear' AND 
		trn_orderheader.intApproveLevelStart-trn_orderheader.intStatus>=2
				";
$string='</br>';	
$string_tmp=$string;	
				$result = $db->RunQuery($sql);
				$i=0;
				while($row=mysqli_fetch_array($result))
				{
					$i++;
					$string 	.=$row['intOrderNo']."/".$row['intOrderYear']."&nbsp;";
					$string_tmp .=$row['intOrderNo']."/".$row['intOrderYear']."&nbsp;";
					$len		= strlen($string_tmp);
					if($len> 80 ){
						$string .= "</br>";
						$string_tmp = '';
					}
					
				}?>
                
    <?php 
		if($i>0)
		{
			?>
    <tr>
      <td>&nbsp;</td>
      <td colspan="7">PO Confirmations are already raised for PO Nos :&nbsp;<?php echo $string; ?></td>
    </tr>
    
    <?Php
		}
	?>
    
    <?php
		if($mode=='second')
		{
	?>
    <tr>
      <td height="17" colspan="7"><table style="margin-top:150px" align="center" width="500" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center">Revision No</td>
          <td>Combo</td>
          <td>Print</td>
        </tr>
        <tr style="margin-top:250px">
          <td><select name="cboRevisionNo" size="1" multiple="multiple" id="cboRevisionNo" style="height:200px;width:200px">
            <?php
				 echo $sql = "SELECT
					trn_sampleinfomations.intRevisionNo
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations.intSampleNo =  '$sampleNo' 
					AND trn_sampleinfomations.intTechnicalStatus =1 
				ORDER BY
					trn_sampleinfomations.intRevisionNo ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($revNo==$row['intRevisionNo'])
						echo "<option selected=\"selected\" value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
					else
						echo "<option value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
				}
				  ?>
          </select></td>
          <td><select name="cboCombo" size="1" multiple="multiple" id="cboCombo"  style="height:200px;width:200px">
            <?php
			if($revNo!='')
			{
				echo $sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strComboName
					FROM trn_sampleinfomations_details
				WHERE
					trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear' and trn_sampleinfomations_details.intRevNo='$revNo'
				ORDER BY
					trn_sampleinfomations_details.strComboName ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($combo==$row['strComboName'])
						echo "<option selected=\"selected\" value=\"".$row['strComboName']."\">".$row['strComboName']."</option>";
					else
						echo "<option value=\"".$row['strComboName']."\">".$row['strComboName']."</option>";
				}
			}
				  ?>
          </select></td>
          <td><select name="cboPrint" size="1" multiple="multiple" id="cboPrint" style="height:200px;width:200px">
            <?php
			
				$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strPrintName
					FROM trn_sampleinfomations_details
				WHERE
					trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_details.strComboName 		=  '$combo' and trn_sampleinfomations_details.intRevNo='$revNo'
				ORDER BY
					trn_sampleinfomations_details.strPrintName ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($printName==$row['strPrintName'])
						echo "<option selected=\"selected\" value=\"".$row['strPrintName']."\">".$row['strPrintName']."</option>";
					else
						echo "<option value=\"".$row['strPrintName']."\">".$row['strPrintName']."</option>";
				}
				  ?>
          </select></td>
        </tr>
      </table></td>
    </tr>
    <?PHp
		die();
		}
	?>
    <tr>
      <td height="17" colspan="7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="27%" rowspan="3"><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:264px;height:148px;overflow:hidden" >
            <?php
			if($sampleNo!='')
			{
				echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"../../../../../documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revNo-".substr($printName,6,1).".png\" />";	
			}
			 ?>
          </div></td>
          <td height="51"  bgcolor="#333333" class="normalfntMid" style="height:20px"><a   id="butViewDetails" class="pink button " style="height:40px;margin:2px 2px 2px 2px;width:100px">View Details</a></td>
          <td>&nbsp;</td>
          <td><a   id="butViewAddItem" class="green button " style="height:40px;margin:2px 2px 2px 2px;width:100px">Allocate Items</a></td>
          <td width="8%" rowspan="2">&nbsp;</td>
          <td width="34%" rowspan="2" style="text-align:left" valign="top"><?php 
	  
	  if($mode=='last')
	  {
	   ?><table style="font-size:12px" width="100%" border="0" cellspacing="0" cellpadding="0" >
        <tr>
          <td width="48%" >Revision No</td>
          <td width="2%" >:</td>
          <td width="47%" id="txtRevNo"><?php echo $revNo; ?></td>
          <td width="1%" rowspan="3" id="txtRevNo">&nbsp;</td>
        </tr>
        <tr>
          <td>Combo</td>
          <td>:</td>
          <td id="txtCombo"><?php echo $combo; ?></td>
          <td width="2%">&nbsp;</td>
        </tr>
        <tr>
          <td>Print</td>
          <td>:</td>
          <td id="txtPrintName"><?php echo $printName; ?></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>Ground Colour</td>
          <td>:</td>
          <td id="txtPrintName"><?php echo $groundColour; ?></td>
          <td>&nbsp;</td>
        </tr>
        </table>
      
      <?php
	  }
	  ?></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td height="47" style="display:none"><a  id="butAddAntiMigration" class="pink button " style="height:40px;margin:2px 2px 2px 2px;">Add Anti-migration</a></td>
          <td>&nbsp;</td>
          <td><a   id="butViewCopyItem" class="green button " style="height:40px;margin:2px 2px 2px 2px;width:100px">Copy Recipes</a></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td width="13%">&nbsp;</td>
          <td width="2%">&nbsp;</td>
          <td width="13%">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
          <td width="3%">&nbsp;</td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td colspan="5" align="center"><div id="msg_parent" align="center" style="width:100%;height:30px;color:#3F6"></div></td>
      <td>&nbsp;</td>
    </tr>
    <tr style="margin-top:250px">
      <td colspan="7"><table id="tblInk" class="tableBorder_allRound"  style="background:#FFF;margin-top:5px" width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr style="color:#FF0">
          <td class="tableBorder_allRound" width="33%" align="center" bgcolor="#333333">COLOR</td>
          <td class="tableBorder_allRound" width="27%" align="center" bgcolor="#333333">TECHNIQUE</td>
          <td class="tableBorder_allRound" width="20%" align="center" bgcolor="#333333">INK TYPE</td>
          <td class="tableBorder_allRound" width="5%" align="center" bgcolor="#333333">SHOTS</td>
          <td class="tableBorder_allRound" width="6%" align="center" bgcolor="#333333">ITEM</td>
          <td class="tableBorder_allRound" width="9%" align="center" bgcolor="#333333">UNDERCOAT</td>
        </tr>
        <?Php
			 $sql = "SELECT
						trn_sampleinfomations_details_technical.intNoOfShots,
						trn_sampleinfomations_details_technical.dblColorWeight,
						trn_sampleinfomations_details_technical.intColorId,
						trn_sampleinfomations_details_technical.intInkTypeId as intInkType,
						mst_inktypes.strName AS inkTypeName,
						mst_colors.strName AS colorName,
						trn_sampleinfomations_details.intTechniqueId as intTechnique,
						mst_techniques.strName AS technique
					FROM
						trn_sampleinfomations_details_technical
						Inner Join mst_inktypes ON mst_inktypes.intId = trn_sampleinfomations_details_technical.intInkTypeId
						Inner Join mst_colors ON mst_colors.intId = trn_sampleinfomations_details_technical.intColorId
						Inner Join trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_details_technical.intSampleNo AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_details_technical.intRevNo AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_details_technical.strPrintName AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_details_technical.strComboName AND trn_sampleinfomations_details.intColorId = trn_sampleinfomations_details_technical.intColorId
						Inner Join mst_techniques ON mst_techniques.intId = trn_sampleinfomations_details.intTechniqueId
					WHERE
						trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
						trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
						trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND
						trn_sampleinfomations_details_technical.strPrintName 	=  '$printName' AND
						trn_sampleinfomations_details_technical.strComboName 	=  '$combo' 
						GROUP BY 
						trn_sampleinfomations_details_technical.intColorId,
						trn_sampleinfomations_details_technical.intInkTypeId,
						trn_sampleinfomations_details.intTechniqueId 
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					$colorId	= $row['intColorId'];
					$techniqueId	= $row['intTechnique'];
					//$inkType	= $row['intInkType'];
					//---------------
				//--get reference-----------------------------------------------
				 $sql_ref = "SELECT
						GROUP_CONCAT(mst_item.strName) AS ITEM 
						FROM
						trn_sampleinfomations_details
						INNER JOIN mst_item ON trn_sampleinfomations_details.intItem = mst_item.intId
						WHERE
								trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
								trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
								trn_sampleinfomations_details.intRevNo =  '$revNo' AND
								trn_sampleinfomations_details.strComboName =  '$combo' AND
								trn_sampleinfomations_details.strPrintName =  '$printName' AND
								trn_sampleinfomations_details.intColorId =  '$colorId' AND
								trn_sampleinfomations_details.intTechniqueId =  '$techniqueId'
						GROUP BY
						trn_sampleinfomations_details.intSampleNo,
						trn_sampleinfomations_details.intSampleYear,
						trn_sampleinfomations_details.intRevNo,
						trn_sampleinfomations_details.strPrintName,
						trn_sampleinfomations_details.strComboName,
						trn_sampleinfomations_details.intColorId,
						trn_sampleinfomations_details.intTechniqueId
						";
					$result_ref = $db->RunQuery($sql_ref);
					$row_ref	= mysqli_fetch_array($result_ref);
					$reference	= $row_ref['ITEM'];
					//---------------
					
					
					$sql2 = "SELECT * FROM `trn_sampleinfomations_details_technical` WHERE `intSampleNo` = '$sampleNo' AND `intSampleYear` = '$sampleYear' AND `intRevNo` = '$revNo' AND `strPrintName` = '$printName' AND `strComboName` = '$combo' AND `intColorId` = '".$row['intColorId']."' AND `intInkTypeId` = '10000'";
					$result2 = $db->RunQuery($sql2);
					if(mysqli_num_rows($result2))
						$haveUncerCoat = true;
					else
						$haveUncerCoat = false;
		?>
        <tr>
              <td bgcolor="#333333" class="color" id="<?Php echo $row['intColorId']; ?>"><?php echo $row['colorName'] ?><br><span style="color:#9F3" >(Reference : <?php echo $reference ; ?> )</span></td>
              <td bgcolor="#333333" class="technique" id="<?php echo $row['intTechnique']; ?>"><?php echo $row['technique'] ?></td>
              <td bgcolor="#333333" class="inkType" id="<?php echo $row['intInkType']; ?>"><?php echo $row['inkTypeName'] ?></td>
              <td align="center" bgcolor="#333333" class="normalfntMid"><?php
			  if($row['intInkType']!=10000)
			  {
			   echo $row['intNoOfShots'] ;
			  }
			  else
			  {?>
				  <input type="text" style="height:50px;margin-top:5px" class="hex txtShots" id="txtShots" aria-haspopup="true" role="textbox" value="<?php echo$row['intNoOfShots']; ?>" /><?php
			  }
			   
			   
			   ?></td>
              <td style="height:20px"  bgcolor="#333333" class="normalfntMid"><a  id="butAdd" class="green button " style="height:16px;margin:2px 2px 2px 2px;">Add</a></td>
              <td align="center" bgcolor="#333333" class="normalfntMid"><?Php 
	
 	  
		$sql_ca = "select intApproved from `trn_sample_color_recipes_approve`  WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName'";
		$result_ca=$db->RunQuery($sql_ca);
		$row_ca=mysqli_fetch_array($result_ca);
		$app_flag=$row_ca['intApproved'];
		$sql_od 	= "SELECT
		Count(trn_orderdetails.intOrderNo) AS rcds
		FROM
		trn_orderdetails 
		INNER JOIN ware_bulkallocation_details ON trn_orderdetails.intOrderNo = ware_bulkallocation_details.intOrderNo 
		AND trn_orderdetails.intOrderYear = ware_bulkallocation_details.intOrderYear 
		AND trn_orderdetails.intSalesOrderId = ware_bulkallocation_details.intSalesOrderId		
		where trn_orderdetails.intSampleNo='$sampleNo' 
		and trn_orderdetails.intSampleYear='$sampleYear' 
		and trn_orderdetails.intRevisionNo='$revNo' 
		and trn_orderdetails.strCombo='$combo' 
		and trn_orderdetails.strPrintName='$printName'";
		$result_od=$db->RunQuery($sql_od);
		$row_od=mysqli_fetch_array($result_od);
	
		
		//$app_permision=1;
		$butViewFlag = 0;	
		if($row_od['rcds']>0){
			$butViewFlag = 0;	
		}
		else if($app_permision==1 && $row_od['rcds']<=0){
			$butViewFlag = 1;	
		}
		 // $butViewFlag=1;
		
 			  
			  if((!$haveUncerCoat || $row['intInkType']==10000 ) && ($app_flag==''))
			  {
			  ?><a   id="butAddUnderCoat" class="<?php echo ($row['intInkType']==10000?'pink':'green'); ?> button " style="height:16px;margin:2px 2px 2px 2px;width:30px"><?php echo ($row['intInkType']==10000?'Remove':'Add'); ?></a><?Php
				}
			  ?></td>
        </tr>
        <?php
				}
		?>
      </table></td>
    </tr>
    <tr height="20">
    <td colspan="7"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td colspan="3" align="center" id="but_parent"><?php if($app_flag!=1 && $butViewFlag==1){ ?><!--<a  id="butApprove" class="green button " style="height:16px;margin:2px 2px 2px 2px;">Approval For Bulk</a>--><?php } ?><?php if($app_flag==1 && $butViewFlag==1){ ?><?php /*?><a  id="butCancel" class="green button " style="height:16px;margin:2px 2px 2px 2px;">Cancel Approval of Bulk recipies</a><?php */?><?php } ?><?php if($app_flag==1){ echo "Approved"; } ?></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
			
		</div>
		<p>
		  
</p>
	
</body>
</html>
<?php
function get_app_permision($userId,$programCode,$field)
	{
global $db;
		$sql = "SELECT
					menupermision.$field as value
				FROM
					menupermision
					Inner Join menus ON menus.intId = menupermision.intMenuId
				WHERE
					menus.strCode 			=  '$programCode' AND
					menupermision.intUserId =  '$userId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row['value'];	
	}

function get_ground_colour($sampleNo,$sampleYear,$revNo,$combo,$printName){
global $db;
		$sql = "SELECT
trn_sampleinfomations_details.intGroundColor,
mst_colors_ground.strName
FROM
trn_sampleinfomations_details
INNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
WHERE
trn_sampleinfomations_details.intSampleNo = '$sampleNo' AND
trn_sampleinfomations_details.intSampleYear = '$sampleYear' AND
trn_sampleinfomations_details.intRevNo = '$revNo' AND
trn_sampleinfomations_details.strPrintName = '$printName' AND
trn_sampleinfomations_details.strComboName = '$combo'
limit 1
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row['strName'];	
}

?>

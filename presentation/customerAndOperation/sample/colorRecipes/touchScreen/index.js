// JavaScript Document
var colorId;
var techniqueId;
var inkTypeId;

$(document).ready(function(){
	$('#butSearch').live('click',searchDetails);
	$('#butAdd').live('click',viewPopup);//viewPopup_comments
	$('#butViewDetails').live('click',viewPopup_comments);//viewPopup_comments
	//$('#cboItemTypeMain').live('change',loadItems);
	//$('#butClose_popup').live('click','hidePopup');
	$('#cboItemTypeMain').live('change',loadSubCategory);
	$('#cboItemTypeMain2').live('change',loadSubCategory2);
	
	$('#cboItemTypeSub').live('change',loadItems);
	$('#butSearchItem').live('click',searchItems);
	$('#butSearchItem2').live('click',searchItems2);
	
	$('#butAddItemToGrid').live('click',addItemToGrid);
	$('#butAddItemToGrid2').live('click',addItemToGrid2);
	$('#butAddItemToGrid3').live('click',addItemToGrid3);
	$('#frmCopyRecipes #cboRevisionNo1').live('click',loadCombo1);
 	$('#frmCopyRecipes #cboCombo1').live('click',loadPrint1);
	$('#frmCopyRecipes #cboRevisionNo2').live('click',loadCombo2);
 	$('#frmCopyRecipes #cboCombo2').live('click',loadPrint2);
	
	$('#butSave2').live('click',saveDetails);

	$("#butSaveTemplate").die('click').live('click',function(){
		saveTemplate(0);	
	}); 
	$("#butSaveTemplateDark").die('click').live('click',function(){
		saveTemplate(1);	
	}); 
	$("#butLoadTemplate").die('click').live('click',function(){
		loadTemplate(0);	
	}); 
	$("#butLoadTemplateDark").die('click').live('click',function(){
		loadTemplate(1);	
	}); 
	
	$('#butCal').live('click',cal);
	$('#removeRow').live('click',removeRow);
	$('#butAddUnderCoat').live('click',addUndercoat);
	$('.txtShots').live('blur',updateShots);
	$('#butAddAntiMigration').live('click',butAddAntiMigration);
	//butViewAddItem
	$('#butViewAddItem').live('click',butViewAddItem);
	$('#butViewCopyItem').live('click',butViewCopyItem);
	
	$('#butCalNew').live('click',newRecepiesCalculation);

});

function butViewCopyItem()
{
	showWaiting();
	colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
	techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
	inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
	
	$('#divBackgroundImg').css('left','10px');
	$('#divBackgroundImg').css('top','10px');
	
	var url = "popup_copyRecipes.php";
		url +="?sampleYear="+$('#cboSampleYear').val();
		url +="&sampleNo="+$('.txtSampleNo').val();
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
		url +="&colorId="+colorId;
		url +="&techniqueId="+techniqueId;
		url +="&inkTypeId="+inkTypeId;
	//alert(url);
	$('#divBackgroundImg').load(url,function(){
		$('#butClose_popup').click(function(){
			hideWaiting();
		});	
		$('#txtSearchItem').keyboard();
		//$('#txtQty').addClass('hex txtSampleNo ui-keyboard-input ui-widget-content ui-corner-all');
		$('#txtQty').keyboard();
		$('.txtQty').keyboard();
		$('#txtNewQty').keyboard();
		
	});
}
function butViewAddItem()
{
	showWaiting();
	colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
	techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
	inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
	
	$('#divBackgroundImg').css('left','10px');
	$('#divBackgroundImg').css('top','10px');
	
	var url = "popup_addItems.php";
		url +="?sampleYear="+$('#cboSampleYear').val();
		url +="&sampleNo="+$('.txtSampleNo').val();
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
		url +="&colorId="+colorId;
		url +="&techniqueId="+techniqueId;
		url +="&inkTypeId="+inkTypeId;
	//alert(url);
	$('#divBackgroundImg').load(url,function(){
		$('#butClose_popup').click(function(){
			hideWaiting();
		});	
		$('#txtSearchItem').keyboard();
		//$('#txtQty').addClass('hex txtSampleNo ui-keyboard-input ui-widget-content ui-corner-all');
		$('#txtQty').keyboard();
		$('.txtQty').keyboard();
		$('#txtNewQty').keyboard();
		
	});
}
function butAddAntiMigration()
{
	var url = "colorRecipes-db-set.php?requestType=butAddAntiMigration";
		url +="&sampleYear="+$('#cboSampleYear').val();
		url +="&sampleNo="+$('.txtSampleNo').val();
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
		$.ajax({
			url:url,
			async:false,
			dataType:'json',
			success:function(json){
					
					$('#msg_parent').html(json.msg);
		}
		});		
	//document.location.href = document.location.href;
}
function updateShots()
{
	colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
	techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
	inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
	
	var url = "colorRecipes-db-set.php?requestType=saveShots";
		url +="&sampleYear="+$('#cboSampleYear').val();
		url +="&sampleNo="+$('.txtSampleNo').val();
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
		url +="&colorId="+colorId;
		url +="&techniqueId="+techniqueId;
		url +="&inkTypeId="+inkTypeId;
		url +="&shots="+$(this).val();
	$.ajax({url:url,async:false});
}
function addUndercoat()
{
	showWaiting();
	colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
	techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
	inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
	
	$('#divBackgroundImg').css('left','10px');
	$('#divBackgroundImg').css('top','10px');
	
	var url	= "colorRecipes-db-set.php?requestType=addNewUndercoat";
		url +="&sampleYear="+$('#cboSampleYear').val();
		url +="&sampleNo="+$('.txtSampleNo').val();
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
		url +="&colorId="+colorId;
		url +="&techniqueId="+techniqueId;
		url +="&inkTypeId="+inkTypeId;
		
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		
		success:function(json){
			if(json.status==1){
			alert(json.msg);
			document.location.href = document.location.href;
			}
			else 
			document.location.href = document.location.href;
	}
	});		
}


function removeRow()
{
	$(this).parent().remove();	
}
function cal()
{
	var y =$(this).parent().parent().find('.txtQty').val();
	var x = $(this).parent().parent().find('.txtQty2').val();
	if(y=='')
		y = 0;
	if(x=='')
		x=0;
	$(this).parent().parent().find('.txtQty2').val(parseFloat(x)+parseFloat(y));
	$(this).parent().parent().find('.txtQty').val('');
	$('#txtTotQty').val(parseFloat($('#txtTotQty').val())+parseFloat(y));

}
function addItemToGrid()
{
	//alert(1);
	var itemName 	= $('#cboItemList option:selected').text();
	var id 			= $('#cboItemList').val();
	var err			=0;
	//-------
	$('#tblPopUpGrid >tbody>tr').each(function(){
 			var itemId		= $(this).attr('id');
			if(id==itemId){
				err	=1;
				alert("Can't Add Duplicate Items");
				return false;
			}
	});
	//-------
	
	if(id == null)
	id = '';
	if(id!='' && err==0)
	{
		
	$('#tblPopUpGrid').append('<tr id="'+id+'"><td>&nbsp;</td><td id="removeRow" align="center" bgcolor="#CCCCCC" style="color:#F00">DEL</td><td style="color:black" bgcolor="#CCCCCC">'+itemName+'</td><td bgcolor="#CCCCCC"><input disabled="disabled"  style="width:100px;height:50px" class="txtQty2" type="text" name="txtQty2" id="txtQty2" /></td><td bgcolor="#CCCCCC"><input style="width:100px;height:50px" class="txtQty" type="text" name="txtQty" id="txtQty" /></td><td width="15%" bgcolor="#CCCCCC"><div id="butCal" style="width:50px;height:35px" class="button green" >+</div></td><td>&nbsp;</td></tr>');
	$('.txtQty').keyboard();
	}
}
function addItemToGrid2()
{
	var id 			= $('#cboItemList').val();
	var url = "colorRecipes-db-set.php?requestType=addNewItem2&mainId="+$('#cboItemTypeMain2').val()+'&subId='+$('#cboItemTypeSub2').val()+'&itemId='+id;
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				if(json.status==1)
					$('#msg').html('SAVED SUCCESSFULLY.');
				else
					$('#msg').html('');
	}
	});
}

function addItemToGrid3()
{
	var sampleNo		= $('#txtSampleNo').val();
	var sampleYear		= $('#txtSampleYear').val();
	
	var rev1 			= $('#cboRevisionNo1').val();
	var combo1 			= $('#cboCombo1').val();
	var print1 			= $('#cboPrint1').val();
	
	var rev2 			= $('#cboRevisionNo2').val();
	var combo2 			= $('#cboCombo2').val();
	var print2 			= $('#cboPrint2').val();
	
	var url 		 = "colorRecipes-db-set.php?requestType=copyRecipes";
		url			+= "&sampleNo="+sampleNo;
		url			+= "&sampleYear="+sampleYear;
		
		url			+= "&rev1="+rev1;
		url			+= "&rev2="+rev2;
		url			+= "&combo1="+combo1;
		url			+= "&combo2="+combo2;
		url			+= "&print1="+print1;
		url			+= "&print2="+print2;
		
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				if(json.status==1)
					$('#msg').html(json.msg);
				else
					$('#msg').html('');
	}
	});
}

function searchItems()
{
	var url = "colorRecipes-db-get.php?requestType=searchItems&mainId="+$('#cboItemTypeMain').val()+'&subId='+$('#cboItemTypeSub').val()+'&like='+$('#txtSearchItem').val();
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				
				$('#cboItemList').html(json.itemList);
	}
	});		
}

function searchItems2()
{
	var url = "colorRecipes-db-get.php?requestType=searchItems2&mainId="+$('#cboItemTypeMain2').val()+'&subId='+$('#cboItemTypeSub2').val()+'&like='+$('#txtSearchItem').val();
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				$('#cboItemList').html(json.itemList);
	}
	});		
}

function loadItems()
{
	$('#txtSearchItem').val('');
	var url = "colorRecipes-db-get.php?requestType=loadItems&mainId="+$('#cboItemTypeMain').val()+'&subId='+$('#cboItemTypeSub').val();
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				$('#cboItemList').html(json.itemList);
	}
	});	
}
function loadSubCategory()
{
	var url = "colorRecipes-db-get.php?requestType=loadSubCategoryList&mainId="+$('#cboItemTypeMain').val();
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				$('#cboItemTypeSub').html(json.subList);
				$('#cboItemList').html(json.itemList);
	}
	});	
}

function loadSubCategory2()
{
	var url = "colorRecipes-db-get.php?requestType=loadSubCategoryList&mainId="+$('#cboItemTypeMain2').val();
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				$('#cboItemTypeSub2').html(json.subList);
				
	}
	});	
}

function searchDetails()
{
	var sampleYear 	= $('#cboSampleYear').val();
	var sampleNo 	= $('#num').val();
	document.location.href = 'index.php?sampleYear='+sampleYear+'&sampleNo='+sampleNo;
}

/*function hidePopup()
{
	alert(1);
}*/

function viewPopup_comments()
{
	showWaiting();
	colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
	techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
	inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
	
	$('#divBackgroundImg').css('left','10px');
	$('#divBackgroundImg').css('top','10px');
	
	var url = "popup_comments.php";
		url +="?sampleYear="+$('#cboSampleYear').val();
		url +="&sampleNo="+$('.txtSampleNo').val();
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
		url +="&colorId="+colorId;
		url +="&techniqueId="+techniqueId;
		url +="&inkTypeId="+inkTypeId;
	//alert(url);
	$('#divBackgroundImg').load(url,function(){
		$('#butClose_popup').click(function(){
			hideWaiting();
		});	
		$('#txtSearchItem').keyboard();
		//$('#txtQty').addClass('hex txtSampleNo ui-keyboard-input ui-widget-content ui-corner-all');
		$('#txtQty').keyboard();
		$('.txtQty').keyboard();
		$('#txtNewQty').keyboard();
		
	});
}

function viewPopup()
{
	showWaiting();
	colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
	techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
	inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
	
	$('#divBackgroundImg').css('left','10px');
	$('#divBackgroundImg').css('top','10px');
	
	var url = "popup.php";
		url +="?sampleYear="+$('#cboSampleYear').val();
		url +="&sampleNo="+$('.txtSampleNo').val();
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
		url +="&colorId="+colorId;
		url +="&techniqueId="+techniqueId;
		url +="&inkTypeId="+inkTypeId;
	//alert(url);
	$('#divBackgroundImg').load(url,function(){
		$('#butClose_popup').click(function(){
			hideWaiting();
		});	
		$('#txtSearchItem').keyboard();
		//$('#txtQty').addClass('hex txtSampleNo ui-keyboard-input ui-widget-content ui-corner-all');
		$('#txtQty').keyboard();
		$('.txtQty').keyboard();
		$('#txtNewQty').keyboard();
		
	});
}


function saveDetails(){
	//alert(colorId);
	var arrRecipe = '';
	var data='';
	var i =0;
	
	var sampleNo 		= 	$('.txtSampleNo').val();
	var sampleYear 		= 	$('#cboSampleYear').val();
	var revisionNo 		= 	$('#txtRevNo').text();
	var combo 			=	$('#txtCombo').text();
	var printName 		=	$('#txtPrintName').text();
	
	data += 'sampleNo='+ sampleNo;
	data += '&sampleYear='+ sampleYear;
	data += '&revisionNo='+ revisionNo;
	data += '&combo='+ combo;
	data += '&printName='+ printName;
	data += '&colorId='+ colorId;
	data += '&techniqueId='+ techniqueId;
	data += '&inkTypeId='+ inkTypeId;
	
	$('#tblPopUpGrid >tbody>tr').each(function(){
		//var itemId 		= $(this).val();
		
		//if(itemId!='')
		//{
			//var colorId 	= colorId
			//var techniqueId = technique
			//var inkTypeId 	= $(this).parent().parent().parent().parent().parent().parent().find('td:eq(3)').attr('id');
			var weight 		= $(this).find('.txtQty2').val();
			var itemId		= $(this).attr('id');
			if(weight!='')
			{
			arrRecipe +=(i++ == 0?'':',' )+'{"color":"'+colorId+'","techId":"'+techniqueId+'","inkTypeId":"'+inkTypeId+'","weight":"'+weight+'","sampleNo":"'+sampleNo+'","sampleYear":"'+sampleYear+'","revisionNo":"'+revisionNo+'","combo":"'+combo+'","printName":"'+printName+'","itemId":"'+itemId+'"}';
			}
		//}
	});

		data  += '&arrRecipe=['+ arrRecipe +']';
		
		var url 	= "colorRecipes-db-set.php?requestType=saveDetails";
		$.ajax({
			type: "POST",
			url:url,
			data:data,
			async:false,
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#msg').html(json.msg);
					$('#msgTemplate').html('');
				},
			error:function(xhr,status){
					
					
				}		
			});
			
			
			
			
   }
//------------------
function saveTemplate(type){
	//alert(colorId);
	var arrRecipe = '';
	var data='';
	var i =0;
	
	$('#tblPopUpGrid >tbody>tr').each(function(){
			var weight 		= $(this).find('.txtQty2').val();
			var itemId		= $(this).attr('id');
			if(weight!='')
			{
			arrRecipe +=(i++ == 0?'':',' )+'{"type":"'+type+'","color":"'+colorId+'","techId":"'+techniqueId+'","inkTypeId":"'+inkTypeId+'","weight":"'+weight+'","itemId":"'+itemId+'"}';
			}
		//}
	});

		data  = 'arrRecipe=['+ arrRecipe +']';
		
		var url 	= "colorRecipes-db-set.php?requestType=saveTemplate";
		$.ajax({
			type: "POST",
			url:url,
			data:data,
			async:false,
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#msg').html('');
					$('#msgTemplate').html(json.msg);
				},
			error:function(xhr,status){
					
					
				}		
			});
   }
//------------------
function loadTemplate(type){
	
	var url 		= "colorRecipes-db-get.php?requestType=loadTemplate";
						
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:"type="+type+"&color="+colorId+"&techId="+techniqueId+"&inkTypeId="+inkTypeId,  
		async:false,
		success:function(json){

		var length = json.arrCombo.length;
		var arrCombo = json.arrCombo;
		if(arrCombo[0]['saved']==1){
			
			var rowCount = document.getElementById('tblPopUpGrid').rows.length;
			for(var j=1;j<rowCount;j++)
			{
					document.getElementById('tblPopUpGrid').deleteRow(1);
			}
			
			for(var i=0;i<length;i++)
			{
				var itemId=arrCombo[i]['itemId'];	
				var itemName=arrCombo[i]['item'];	
				var weight=arrCombo[i]['weight'];	
				
				var content='<tr  id="'+itemId+'">';
				content +='<td></td>';
				content +='<td id="removeRow" bgcolor="#CCCCCC" align="center" style="color:#F00">DEL</td>';
				content +='<td bgcolor="#CCCCCC" style="color:black">'+itemName+'</td>';
				content +='<td bgcolor="#CCCCCC"><input id="txtQty2" class="txtQty2" type="text" name="txtQty2" style="width:100px;height:50px" value="'+weight+'"></td>';
				content +='<td bgcolor="#CCCCCC"><input id="txtQty" class="hex txtQty ui-keyboard-input ui-widget-content ui-corner-all" type="text" aria-haspopup="true" style="width:100px;height:50px; text-align:right" value="" role="textbox"></td>';
				content +='<td bgcolor="#CCCCCC"><div id="butCal" class="button green" style="width:50px;height:35px">+</div></td>';
				content +='<td></td>';
				content +='</tr>';
				
				add_new_row('#tblPopUpGrid',content);
				$('#txtQty').keyboard();
				$('.txtQty').keyboard();
				$('#msgTemplate').html('');
				$('#msg').html('');
			}
		}
		else{
				$('#msgTemplate').html('No template to load');
				$('#msg').html('');
		}
	}
	});
}
//------------------
function newRecepiesCalculation(obj){
	var newTot 	= $('#txtNewQty').val();
	if(newTot<=0){
		return false;
		$('#txtNewQty').val(0);
	}
	
	var sampleNo 		= 	$('.txtSampleNo').val();
	var sampleYear 		= 	$('#cboSampleYear').val();
	var revisionNo 		= 	$('#txtRevNo').text();
	var combo 			=	$('#txtCombo').text();
	var printName 		=	$('#txtPrintName').text();
	var tot=0;
	
	$('#tblPopUpGrid >tbody>tr').each(function(){
					var ob=this;

		//var itemId 		= $(this).val();
		
		//if(itemId!='')
		//{
			//var colorId 	= colorId
			//var techniqueId = technique
			//var inkTypeId 	= $(this).parent().parent().parent().parent().parent().parent().find('td:eq(3)').attr('id');
			var itemId		= $(ob).attr('id');
			var url 		= "colorRecipes-db-get.php?requestType=loadNewWeights";
			var httpobj = $.ajax({
				url:url,
				dataType:'json',
				data:"colorId="+colorId+"&techniqueId="+techniqueId+"&inkTypeId="+inkTypeId+"&sampleNo="+sampleNo+"&sampleYear="+sampleYear+"&revisionNo="+revisionNo+"&combo="+combo+"&printName="+printName+"&itemId="+itemId+"&newTot="+newTot,
				async:false,
				success:function(json){
					$(ob).find('.txtQty2').val(json.weight);
					tot+=parseFloat(json.weight);
				}
			});
	//	}
		
	});
	$('#txtTotQty').val(tot);
	$('#txtNewQty').val(0);

}
//---------------------------------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//------------------------------------------------------------------------------
function loadCombo1()
{
	var url = "colorRecipes-db-get.php?requestType=loadCombo&sampleNo="+($('.txtSampleNo').val())+"&sampleYear="+$('#cboSampleYear').val()+"&revNo="+$('#cboRevisionNo1').val();
	var obj = $.ajax({url:url,async:false});	
	$('#cboCombo1').html(obj.responseText);	
	$('#cboPrint1').html('');
}
function loadPrint1()
{
	var url = "colorRecipes-db-get.php?requestType=loadPrint&sampleNo="+($('.txtSampleNo').val())+"&sampleYear="+$('#cboSampleYear').val()+"&revNo="+$('#cboRevisionNo1').val()+"&combo="+encodeURIComponent($('#cboCombo1').val());
	var obj = $.ajax({url:url,async:false});	
	$('#cboPrint1').html(obj.responseText);	
}
//------------------------------------------------------------------------------
function loadCombo2()
{
	var url = "colorRecipes-db-get.php?requestType=loadCombo&sampleNo="+($('.txtSampleNo').val())+"&sampleYear="+$('#cboSampleYear').val()+"&revNo="+$('#cboRevisionNo2').val();
	var obj = $.ajax({url:url,async:false});	
	$('#cboCombo2').html(obj.responseText);	
	$('#cboPrint2').html('');
}
function loadPrint2()
{
	var url = "colorRecipes-db-get.php?requestType=loadPrint&sampleNo="+($('.txtSampleNo').val())+"&sampleYear="+$('#cboSampleYear').val()+"&revNo="+$('#cboRevisionNo2').val()+"&combo="+encodeURIComponent($('#cboCombo2').val());
	var obj = $.ajax({url:url,async:false});	
	$('#cboPrint2').html(obj.responseText);	
}

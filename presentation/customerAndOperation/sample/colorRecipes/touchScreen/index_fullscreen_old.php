<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sample Recipes</title>
	<style>
	body {
		background: #808080;
	}
	#container {
		width: 600px;
		padding: 30px;
		background: #F8F8F8;
		border: solid 1px #ccc;
		color: #111;
		margin: 20px auto;
		border-radius: 3px;
	}
	
	#specialstuff {
		background: #333;
		padding: 0px;
		margin: 0px;
		color: #fff;
	}
	#specialstuff a {
		color: #eee;
	}
	
	#fsstatus {
		background: #e33;
		color: #111;
	}
	
	#fsstatus.fullScreenSupported {
		background: #3e3;
	}
	.label{
		font-size:18px;
	}

	input[type="text"] { 
    	background: #999;
		   width: 120px;
		   color:#CF3;
		   padding: 5px;
		   font-size: 16px;
		   border: 1px solid #ccc;
		   height: 26px;
		   font-size:22px;
		   
	}
	select { 
    		background: #999;color:#CF3;
		   width: 120px;
		   padding: 5px;
		   font-size: 16px;
		   border: 1px solid #ccc;
		   height: 34px;
		   		   font-size:22px;
	}
	.button {
	display: inline-block;
	outline: none;
	cursor: pointer;
	text-align: center;
	text-decoration: none;
	font: 14px/100% Arial, Helvetica, sans-serif;
	padding: .5em 2em .55em;
	text-shadow: 0 1px 1px rgba(0,0,0,.3);
	-webkit-border-radius: .5em; 
	-moz-border-radius: .5em;
	border-radius: .5em;
	-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
	-moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
	box-shadow: 0 1px 2px rgba(0,0,0,.2);
}
.button:hover {
	text-decoration: none;
}
.button:active {
	position: relative;
	top: 1px;
}
.orange {
	color: #fef4e9;
	border: solid 1px #da7c0c;
	background: #f78d1d;
	background: -webkit-gradient(linear, left top, left bottom, from(#faa51a), to(#f47a20));
	background: -moz-linear-gradient(top,  #faa51a,  #f47a20);
	filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#faa51a', endColorstr='#f47a20');
}
.orange:hover {
	background: #f47c20;
	background: -webkit-gradient(linear, left top, left bottom, from(#f88e11), to(#f06015));
	background: -moz-linear-gradient(top,  #f88e11,  #f06015);
	filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#f88e11', endColorstr='#f06015');
}
.orange:active {
	color: #fcd3a5;
	background: -webkit-gradient(linear, left top, left bottom, from(#f47a20), to(#faa51a));
	background: -moz-linear-gradient(top,  #f47a20,  #faa51a);
	filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#f47a20', endColorstr='#faa51a');
}
	</style>
    
<!-- jQuery & jQuery UI + theme (required) -->
	<link href="other/jquery-ui.css" rel="stylesheet">
	<script src="other/jquery.js"></script>
	<script src="other/jquery-ui.min.js"></script>

	<!-- keyboard widget css & script (required) -->
	<link href="css/keyboard.css" rel="stylesheet">
	<script src="js/jquery.keyboard.js"></script>

	<!-- keyboard extensions (optional) -->
	<script src="js/jquery.mousewheel.js"></script>
	<script src="js/jquery.keyboard.extension-typing.js"></script>
	<script src="js/jquery.keyboard.extension-autocomplete.js"></script>

	<!-- demo only -->
	<link href="demo/demo.css" rel="stylesheet">
	
	<script src="other/jquery.jatt.min.js"></script> <!-- tooltips -->

	<!-- initialize keyboard (required) -->
	<script>
		$(function(){
			$('#keyboard').keyboard();
		});
		$('#num').keyboard({
	  layout : 'num',
	  restrictInput : true, 
	  preventPaste : true,  
	  autoAccept : true
	 })
	</script>

</head>

<body>
	
		
<div  id="specialstuff" style="width:100%;height:600px;visibility:hidden">
  <table width="1024" height="304" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="145" height="48" class="label" >&nbsp;SAMPLE YEAR</td>
      <td width="151"><select name="cboSampleYear"   id="cboSampleYear" >
      	<option>2012</option>
      </select></td>
      <td width="123" class="label">SAMPLE NO</td>
      <td width="132"><input name="textfield" type="text"  style="width:130px" id="textfield"  /></td>
      <td width="123">&nbsp;<a class="orange button">Search</a></td>
      <td width="275">&nbsp;<input id="keyboard" type="text"><input type="text" class="alignRight ui-keyboard-input ui-widget-content ui-corner-all ui-keyboard-autoaccepted" id="num" aria-haspopup="true" role="textbox"></td>
      <td width="75">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
			
		</div>
		
        <div style="overflow:hidden; position:absolute; left: 563px; top: 428px; width: 202px; height: 66px;">
		<input type="button" value="LOING TO SYSTEM" id="fsbutton" />
		</div>
		<p>
			
		</p>
	
</body>
</html>
<script>

/* 
Native FullScreen JavaScript API
-------------
Assumes Mozilla naming conventions instead of W3C for now
*/

(function() {
	var 
		fullScreenApi = { 
			supportsFullScreen: false,
			isFullScreen: function() { return false; }, 
			requestFullScreen: function() {}, 
			cancelFullScreen: function() {},
			fullScreenEventName: '',
			prefix: ''
		},
		browserPrefixes = 'webkit moz o ms khtml'.split(' ');
	
	// check for native support
	if (typeof document.cancelFullScreen != 'undefined') {
		fullScreenApi.supportsFullScreen = true;
	} else {	 
		// check for fullscreen support by vendor prefix
		for (var i = 0, il = browserPrefixes.length; i < il; i++ ) {
			fullScreenApi.prefix = browserPrefixes[i];
			
			if (typeof document[fullScreenApi.prefix + 'CancelFullScreen' ] != 'undefined' ) {
				fullScreenApi.supportsFullScreen = true;
				
				break;
			}
		}
	}
	
	// update methods to do something useful
	if (fullScreenApi.supportsFullScreen) {
		fullScreenApi.fullScreenEventName = fullScreenApi.prefix + 'fullscreenchange';
		
		fullScreenApi.isFullScreen = function() {
			switch (this.prefix) {	
				case '':
					return document.fullScreen;
				case 'webkit':
					return document.webkitIsFullScreen;
				default:
					return document[this.prefix + 'FullScreen'];
			}
		}
		fullScreenApi.requestFullScreen = function(el) {
			return (this.prefix === '') ? el.requestFullScreen() : el[this.prefix + 'RequestFullScreen']();
		}
		fullScreenApi.cancelFullScreen = function(el) {
			return (this.prefix === '') ? document.cancelFullScreen() : document[this.prefix + 'CancelFullScreen']();
		}		
	}

	// jQuery plugin
	if (typeof jQuery != 'undefined') {
		jQuery.fn.requestFullScreen = function() {
	
			return this.each(function() {
				var el = jQuery(this);
				if (fullScreenApi.supportsFullScreen) {
					fullScreenApi.requestFullScreen(el);
				}
			});
		};
	}

	// export api
	window.fullScreenApi = fullScreenApi;	
})();

</script>

<script>

//window.fullScreenApi = fullScreenApi;	
// do something interesting with fullscreen support
var fsButton = document.getElementById('fsbutton'),
	fsElement = document.getElementById('specialstuff')
	//fsStatus = document.getElementById('fsstatus');


if (window.fullScreenApi.supportsFullScreen) {
	//fsStatus.innerHTML = 'YES: Your browser supports FullScreen';
	//fsStatus.className = 'fullScreenSupported';
	
	// handle button click
	fsButton.addEventListener('click', function() {
		window.fullScreenApi.requestFullScreen(fsElement);
		fsElement.style.visibility = "visible";
	}, true);
	
	fsElement.addEventListener(fullScreenApi.fullScreenEventName, function() {
		if (fullScreenApi.isFullScreen()) {
			//fsStatus.innerHTML = 'Whoa, you went fullscreen';
		} else {
			//fsStatus.innerHTML = 'Back to normal';
		}
	}, true);
	
} else {
	//fsStatus.innerHTML = 'SORRY: Your browser does not support FullScreen';
}




</script>

<link href="css.css" rel="stylesheet" />
<body onload="keyBoard();">
<table bgcolor="#000000" width="1010" height="202" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td height="250"><table id="tblPopUpGrid" width="100%" border="1" cellspacing="0" cellpadding="0" class="tableBorder_allRound">
      <thead>
        <tr>
          <td width="18%" align="center" bgcolor="#006666"><strong>ORDER NO</strong></td>
          <td width="25%" align="center" bgcolor="#006666"><strong>SALES ORDER</strong></td>
          <td width="22%" align="center" bgcolor="#006666"><strong>RECEIVED QTY</strong></td>
          <td width="22%" align="center" bgcolor="#006666"><strong>STOCK BAL</strong></td>
          <td width="13%" align="center" bgcolor="#006666"><strong>WEIGHT</strong></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="color:black" bgcolor="#CCCCCC" align="center" id="<?php echo $orderNo; ?>">200001/2014</td>
          <td style="color:black" bgcolor="#CCCCCC" align="center" id="<?php echo $salesOrderId; ?>">Sales Order 1</td>
          <td style="color:black" bgcolor="#CCCCCC" align="right" id="<?php echo $completedQty; ?>">100</td>
          <td style="color:black" bgcolor="#CCCCCC" align="right" id="<?php echo $prapotion; ?>">5000</td>
          <td style="color:black" bgcolor="#CCCCCC" align="right" id="<?php echo round($qty*($orderQty/$totOrderQty),4); ?>"><span style="text-align:center">
            <input id="txtProdQty2" class="hex txtQty ui-keyboard-input ui-widget-content ui-corner-all" type="text" value="<?php echo $Qty; ?>" role="textbox" aria-haspopup="true" style="height:30px;margin-top:3px;margin-bottom:3px; width:110px; alignment-adjust:before-edge; text-align:right" />
          </span></td>
        </tr>
        <tr>
          <td style="color:black" bgcolor="#CCCCCC" align="center" id="<?php echo $orderNo; ?>">200001/2014</td>
          <td style="color:black" bgcolor="#CCCCCC" align="center" id="<?php echo $salesOrderId; ?>">Sales Order 2</td>
          <td style="color:black" bgcolor="#CCCCCC" align="right" id="<?php echo $completedQty; ?>">100</td>
          <td style="color:black" bgcolor="#CCCCCC" align="right" id="<?php echo $prapotion; ?>">5000</td>
          <td style="color:black" bgcolor="#CCCCCC" align="right" id="<?php echo round($qty*($orderQty/$totOrderQty),4); ?>"><span style="text-align:center">
            <input id="txtProdQty" class="hex txtQty ui-keyboard-input ui-widget-content ui-corner-all" type="text" value="<?php echo $Qty; ?>" role="textbox" aria-haspopup="true" style="height:30px;margin-top:3px;margin-bottom:3px; width:110px; alignment-adjust:before-edge; text-align:right" />
          </span></td>
        </tr>
      </tbody>
    </table></td>
  </tr>
  <tr>
    <td height="39"><table width="100%" border="0" cellspacing="1" cellpadding="1">
      <tr>
        <td align="right"><div style="height:28px" align="right" id="butClose_popup" class="button green" >&nbsp;&nbsp;ADD&nbsp;&nbsp;</div></td>
        <td align="left"><div style="height:28px" align="right" id="butClose_popup" class="button pink" >CLOSE</div></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>

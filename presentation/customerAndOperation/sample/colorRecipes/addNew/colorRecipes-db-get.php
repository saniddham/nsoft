<?php 

	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];

	include "{$backwardseperator}dataAccess/Connector.php";
	
	$requestType 			= $_REQUEST['requestType'];
	
	if($requestType=='loadRevisionNo')
	{
		$sampleNo 			= $_REQUEST['sampleNo'];
		$sampleYear 		= $_REQUEST['sampleYear'];
		
		$sql = "SELECT
					trn_sampleinfomations.intRevisionNo
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations.intSampleNo =  '$sampleNo' 
					AND trn_sampleinfomations.intTechnicalStatus =1 
				ORDER BY
					trn_sampleinfomations.intRevisionNo ASC
				";
		$result = $db->RunQuery($sql);
		echo "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			echo "<option value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
		}
	}
	//loadSampleNo
	elseif($requestType=='loadSampleNo')
	{
		$styleNo 			= $_REQUEST['styleNo'];
		$sampleYear 		= $_REQUEST['sampleYear'];
		
		if($styleNo!='')
			$para = " and trn_sampleinfomations.strGraphicRefNo =  '$styleNo'";
		$sql = "SELECT distinct
					trn_sampleinfomations.intSampleNo
				FROM trn_sampleinfomations
				WHERE
					
					trn_sampleinfomations.intSampleYear =  '$sampleYear' 
					 $para 
				AND trn_sampleinfomations.intTechnicalStatus =1 	 
				order by intSampleNo
				";
		$result = $db->RunQuery($sql);
		echo "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			echo "<option value=\"".$row['intSampleNo']."\">".$row['intSampleNo']."</option>";
		}
	}
	elseif($requestType=='loadCombo')
	{
		$sampleNo 			= $_REQUEST['sampleNo'];
		$sampleYear 		= $_REQUEST['sampleYear'];
		$revNo				= $_REQUEST['revNo'];
		
		$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strComboName
					FROM trn_sampleinfomations_details
				WHERE
					trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear'
				ORDER BY
					trn_sampleinfomations_details.strComboName ASC
				";
		$result = $db->RunQuery($sql);
		echo "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			echo "<option value=\"".$row['strComboName']."\">".$row['strComboName']."</option>";
		}
	}
	elseif($requestType=='loadPrint')
	{
		$sampleNo 			= $_REQUEST['sampleNo'];
		$sampleYear 		= $_REQUEST['sampleYear'];
		$revNo				= $_REQUEST['revNo'];
		$combo				= $_REQUEST['combo'];
		
		$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strPrintName
					FROM trn_sampleinfomations_details
				WHERE
					trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_details.strComboName 		=  '$combo'
				ORDER BY
					trn_sampleinfomations_details.strPrintName ASC
				";
		$result = $db->RunQuery($sql);
		echo "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			echo "<option value=\"".$row['strPrintName']."\">".$row['strPrintName']."</option>";
		}
	}
	else if($requestType=='loadMeasurements')
	{
		$foil  = $_REQUEST['foil'];
		 $sql = "SELECT
				mst_item.foil_width,
				mst_item.foil_height, 
				mst_item.dblLastPrice  
				FROM
				mst_item
				WHERE
				mst_item.intId =  '$foil'";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);

			$response['width'] 	= $row['foil_width'];
			$response['height'] = $row['foil_height'];
			$response['price'] = $row['dblLastPrice'];
			
		echo json_encode($response);
	}
	else if($requestType=='loadItem')
	{
		$technique  = $_REQUEST['technique'];
		$sampleNo 			= $_REQUEST['sampleNo'];
		$sampleYear 		= $_REQUEST['sampleYear'];
		$revNo				= $_REQUEST['revNo'];
		$combo				= $_REQUEST['combo'];
		
	  	$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strPrintName, 
					trn_sampleinfomations_details.intItem,
					mst_item.strName  
					FROM
					trn_sampleinfomations_details
					Right Join mst_item ON trn_sampleinfomations_details.intItem = mst_item.intId
					INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
					WHERE
					trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_details.strComboName 		=  '$combo' AND 
					trn_sampleinfomations_details.intRevNo =  '$revNo' AND
					trn_sampleinfomations_details.intTechniqueId =  '$technique' AND 
					mst_units.intId <> 6 

				ORDER BY
					mst_item.strName ASC
				";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);

		$response['itemId'] 	= $row['intItem'];
			
		echo json_encode($response);
	}
		else if($requestType=='getConsumption')
	{
		$w    		    = $_REQUEST['foilWidth'];
		$h			 	= $_REQUEST['foilHeight'];
		$widthOne 		= $_REQUEST['widthOne'];
		$heightOne		= $_REQUEST['heightOne'];
		$numOfPices = 0 ; 
		
		$pw			= $widthOne*0.0254;//meters - (Print Width)
		$ph			= $heightOne*0.0254;//inches -(Print Height)
		
		$h			= $h*0.0254;//convert inches to meters (foil height)
				
		$t_a_rows	= intval(strval($h/$ph));
		$t_a_cols	= intval(strval($w/$pw));
		$t_aw		= $t_a_cols*$pw;
		$t_ah		= $t_a_rows*$ph;
		
		$t_b_rows	= 0;
		$t_b_cols	= 0;
		$t_bw		=$w-$t_aw; 				//$w-$t_a_cols*$pw;
		$t_bh		=$h-$t_ah; 				//h-$t_a_rows*$ph;
		
		$t_c_rows	= intval(strval($t_bw/$ph));	//intval(($w-$t_a_cols*$pw)/$ph);
		$t_c_cols	= intval(strval($h/$pw));
		
		if($t_c_cols>=1){
			$t_c_cols	= intval(strval($t_bh/$pw));
			$t_c_rows = intval(strval($w/$ph));
			}
		$t_c_rows;
		$t_c_cols;
		$t_cw		= $h;
		$t_ch		= $ph*$t_c_rows;
		
		$t_d_rows	= 0;
		$t_d_cols	= 0;
		$t_dw		= $h;
		$t_dh		= $w-($t_aw+$t_ch);
		
		//////////// 2ND METHOD//////////////////
		//echo "e-r"."(".$h."/".$pw.")=".intval($h/$pw);
		//$t_e_rows	= intval($h/$pw);
		//echo "e-c"."(".$w."/".$ph.")=".intval($w/$ph);
		//$t_e_cols	= intval($w/$ph);
		
		$t_e_rows	= ($h/$pw);
		$t_e_cols	= ($w/$ph);
		
		$t_e_rows	= intval(strval($t_e_rows));
		$t_e_cols	= intval(strval($t_e_cols));
		
		$t_ew		= $ph*$t_e_cols;
		$t_eh		= $t_e_rows*$pw;
		
		
		$t_f_rows	= 0;
		$t_f_cols	= 0;
		$t_fw		= $pw*$t_e_rows;
		$t_fh		= $w-$t_e_cols*$ph;
		
		$t_g_rows	= intval(strval(($h-$t_e_rows*$pw)/$ph));
		$t_g_cols	= intval(strval($w/$pw));
		$t_gw		= $w;
		$t_gh		= $ph*$t_g_rows;
		
		
		$t_h_rows	= 0;
		$t_h_cols	= 0;
		$t_hw		= $w;
		$t_hh		= $h-($t_eh+$t_gh);
		
		if (($t_a_rows* $t_a_cols+$t_c_rows*$t_c_cols)>=($t_e_rows*$t_e_cols+$t_g_rows*$t_g_cols)){
			$numOfPices = 	$t_a_rows* $t_a_cols+$t_c_rows*$t_c_cols;
		}else{
			$numOfPices = 	$t_e_rows*$t_e_cols+$t_g_rows*$t_g_cols;
		}
		$response['type'] 		= 'pass';
		$response['val'] 		= $numOfPices;		
		
		echo json_encode($response);
	}
?>
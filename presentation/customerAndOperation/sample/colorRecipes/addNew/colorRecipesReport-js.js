var basepath	= 'presentation/customerAndOperation/sample/colorRecipes/addNew/'
var userCheckedDetails 	= [];
$(document).ready(function() {
	//$('.chkClass').click(getChecked);
	$('.chkClassAll').click(getCheckedAll);
	
	$('#frmColRecipes').validationEngine();	
	
	$('#frmColRecipes #imgApprove').click(function(){
	var jsonData = getChecked();
	//jsonData.splice(-1,1);
	var st = false;
	$('#frmColRecipes .chkClass').each(function(){
		if($(this).is(':checked'))
		{
			st = true;	
		}});
	if(st){
		var val = $.prompt('Are you sure you want to approve this ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
 					 var url = basepath+"colorRecipes-db-set.php"+window.location.search+'&requestType=approve';
					 var userSelected = JSON.stringify(jsonData);
					 var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:"userSelectedArr="+ userSelected,
						async:false,
						
						success:function(json){
								$('#frmColRecipes #imgApprove').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
								if(json.type=='fail'){
								$('#frmColRecipes .chkClass').each(function(){
								if($(this).is(':checked'))
								{
									$(this).prop('checked', false);
									window.setTimeout('location.reload()', 3000);
								}});
									}
							},
						error:function(xhr,status){
								
								$('#frmColRecipes #imgApprove').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
						
						}
						
  				}});
		}	
	if(!st)
		alert('Not Selected');
	return;
	
	});
	
	$('#frmColRecipes #imgReject').click(function(){
	var jsonData = getChecked();
	//jsonData.splice(-1,1);
    
	var st = false;
	$('#frmColRecipes .chkClass').each(function(){
		if($(this).is(':checked'))
		{
			st = true;
		}});
		
	if(st){
			var val = $.prompt('Are you sure you want to reject this?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
			if(v)
				{
 				  var url = basepath+"colorRecipes-db-set.php"+window.location.search+'&requestType=reject';
				  var userSelected = JSON.stringify(jsonData);
					$.ajax({
					  url:url,
					  data:"userSelectedArr="+ userSelected,
					  type:'post',
					  async:false,
					  dataType: "json",  
											//data:'{"requestType":"addsampleInfomations"}',
											
					 success:function(json){
					 $('#frmColRecipes #imgReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
							{
							 var t=setTimeout("alertx()",1000);
							 window.location.href = window.location.href;
							 window.opener.location.reload();//reload listing page
							 return;
							 }},
						error:function(xhr,status){
							$('#frmColRecipes #imgReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",3000);
							}});}
 			}}); 
		}	
	if(!st)
	alert('Not Selected');
	return;
	});
});

function alertx()
{
	$('#frmColRecipes #imgApprove').validationEngine('hide')	;
}	


function getChecked(){
	$('#frmColRecipes .chkClass').each(function(){ 
	  var combo          = $(this).parent().parent().children("#combo").children("#getCombo").html();
	  var printCheckBox  = $(this).closest("tr").children("#print").html();
	  var isChecked      = $(this).prop('checked');

	if(!userCheckedDetails[combo]){
		userCheckedDetails[combo] = [];
	}

	if(isChecked==true){
		 userCheckedDetails[combo][printCheckBox]=1;
  	}    
	else{
		userCheckedDetails[combo][printCheckBox]=0;
	}
	});
 
	var JSONObj = [];
	for (var k in userCheckedDetails){
		if (userCheckedDetails.hasOwnProperty(k)) {
			 //-------------------------------------------------
			
			for (var l in userCheckedDetails[k]){
				if (userCheckedDetails[k].hasOwnProperty(l)) {
					
					    var obj = { 
							Main_Key: k,
							Sub_Key: l,
							value: userCheckedDetails[k][l]
						};
					JSONObj.push(obj);
				}
			}
			
			 //-------------------------------------------------
		}
		
	}
	return JSONObj;
}

function getCheckedAll(){
  if(this.checked) {
      $(':checkbox').each(function() {
          this.checked = true;
      });
  }
  else {
    $(':checkbox').each(function() {
          this.checked = false;
      });
  }

}
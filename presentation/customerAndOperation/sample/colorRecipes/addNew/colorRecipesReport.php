<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId 		= $sessions->getLocationId();
$locationId		= $companyId;
$intUser  		= $sessions->getUserId();
$programCode	= 'P0433';

//if($intUser==2)
	//ini_set('display_erros',1);

$x_graphicNo 	= $_REQUEST['graphicNo'];
$x_sampleNo 	= $_REQUEST['sampleNo'];
$x_sampleYear 	= $_REQUEST['sampleYear'];
$x_revNo 		= $_REQUEST['revNo'];
//$x_combo 		= $_REQUEST['combo'];
//$x_print 		= $_REQUEST['printName'];
$approveMode 	= (!isset($_REQUEST["mode"])?'':$_REQUEST["mode"]);
$approveLevel			= 2;

$sql_Graphic_Style  = "SELECT
	trn_sampleinfomations.intSpecialTechniques,
	trn_sampleinfomations.strGraphicRefNo,
	trn_sampleinfomations.strStyleNo
	FROM
		`trn_sample_special_technical_header`
	INNER JOIN trn_sampleinfomations ON trn_sample_special_technical_header.SAMPLE_NO = trn_sampleinfomations.intSampleNo
	AND trn_sample_special_technical_header.SAMPLE_YEAR = trn_sampleinfomations.intSampleYear
	AND trn_sample_special_technical_header.REVISION_NO = trn_sampleinfomations.intRevisionNo
	WHERE
		trn_sample_special_technical_header.SAMPLE_NO = '$x_sampleNo'
	AND trn_sample_special_technical_header.SAMPLE_YEAR = '$x_sampleYear'
	AND trn_sample_special_technical_header.REVISION_NO = '$x_revNo'
	GROUP BY trn_sample_special_technical_header.COMBO, trn_sample_special_technical_header.PRINT ";
				
$result_graphic_style = $db->RunQuery($sql_Graphic_Style);
$row = mysqli_fetch_array($result_graphic_style);
$intStatus		= $row['intSpecialTechniques'];
$x_graphic		= $row['strGraphicRefNo'];
$x_style		= $row['strStyleNo'];	   
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Report - Consumptions for Special Techniques</title>
<script type="text/javascript" src="presentation/customerAndOperation/sample/colorRecipes/addNew/colorRecipesReport-js.js?n=1"></script>
<style>
.break {
	page-break-before: always;
}
 @media print {
.noPrint {
	display: none;
}
}
#apDiv1 {
	position: absolute;
	left: 354px;
	top: 173px;
	width: 650px;
	height: 322px;
	z-index: 1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="images/pending.png"  /></div>
<?php
}
?>
<form id="frmColRecipes" name="frmColRecipes" method="post" action="rptFabricGatePass.php">
  <table width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="3"></td>
    </tr>
    <tr>
      <td width="20%"></td>
      <td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
      <td width="20%"></td>
    </tr>
    <tr>
      <td colspan="3"></td>
    </tr>
  </table>
  <div align="center">
    <div style="background-color:#FFF" ><strong>CONSUMPTIONS FOR SPECIAL TECHNIQUES</strong><strong></strong></div>
    <table width="907" border="0" align="center" bgcolor="#FFFFFF">
      <tr>
        <td width="901"><table width="100%">
            <tr>
              <td colspan="10" align="center" bgcolor="#FFDFCB"><?php

			
	if($intStatus>1)
	{
	
	//------------
	$k=$approveLevel+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
	
	 $resultp = $db->RunQuery($sqlp);
	 $rowp=mysqli_fetch_array($resultp);
	 $userPermission=0;
	 if($rowp['int'.$k.'Approval']==1){
		 $userPermission=1;
	 }
	//--------------
		
	?>
                <?php if(($approveMode=='approve') and ($userPermission==1)) { ?>
                <img src="images/approve.png" align="middle" class="noPrint mouseover" id="imgApprove" /> <img src="images/reject.png" align="middle" class="noPrint mouseover" id="imgReject" />
                <?php
	}
	}
	?></td>
            </tr>
            <tr>
              <?php		  
 	if($intStatus==1)
	{
	?>
              <td colspan="10" class="APPROVE" >CONFIRMED</td>
              <?PHP
	}
	else if($intStatus==0)
	{
   ?>
              <td colspan="9" class="APPROVE" style="color:#F00">REJECTED</td>
              <?php
	}
	else
	{
   ?>
              <td width="10%" colspan="9" class="APPROVE">PENDING</td>
              <?php
	}
   ?>
            </tr>
            <tr>
              <td width="1%">&nbsp;</td>
              <td width="14%"><span class="normalfnt"><strong>Sample No</strong></span></td>
              <td width="2%" align="center" valign="middle"><strong>:</strong></td>
              <td width="18%"><span class="normalfnt"><?php echo $x_sampleNo  ?>/<?php echo $x_sampleYear ?></span></td>
              <td width="10%" class="normalfnt"><strong>Revision No</strong></td>
              <td width="1%" align="center" valign="middle"><strong>:</strong></td>
              <td width="9%"><span class="normalfnt"><?php echo $x_revNo  ?></span></td>
              <td width="12%" class="normalfnt"></td>
              <td width="1%">&nbsp;</td>
              <td width="13%">&nbsp;</td>
            </tr>

     		<tr>
              <td>&nbsp;</td>
              <td class="normalfnt"><strong>Graphic</strong></td>
              <td align="center" valign="middle"><strong>:</strong></td>
              <td><span class="normalfnt"><?php echo $x_graphic   ?></span></td>
              <td><span class="normalfnt"><strong>Style</strong></span></td>
              <td align="center" valign="middle"><strong>:</strong></td>
              <td><span class="normalfnt"><?php echo $x_style ?></span></td>
              <td class="normalfnt">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <?php if($intStatus > 1){?>
              <td class="normalfnt" style="background-color:#9FF; font-weight:bold"><input type="checkbox" name="chkComboPrintAll" id="chkComboPrintAll" class="chkClassAll"/>&nbsp;&nbsp;Select All</td>
               <?php }
			   else{?>
               <td class="normalfnt">&nbsp;</td>
               <?php }?>
              <td align="center" valign="middle">&nbsp;</td>
              <td>&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
              <td align="center" valign="middle">&nbsp;</td>
              <td>&nbsp;</td>
              <td class="normalfnt">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            
          </table></td>
      </tr>
      <?php 
		$sql	="SELECT
				trn_sample_special_technical_header.SAMPLE_NO,
				trn_sample_special_technical_header.SAMPLE_YEAR,
				trn_sample_special_technical_header.REVISION_NO,
				trn_sample_special_technical_header.COMBO,
				trn_sample_special_technical_header.PRINT,	
                trn_sample_special_technical_header.STATUS,
				trn_sample_special_technical_header.LEVELS,
				trn_sample_special_technical_header.SAVED_BY,
				trn_sample_special_technical_header.SAVED_DATE,
				trn_sample_special_technical_header.MODIFIRED_BY,
				trn_sample_special_technical_header.MODIFIED_DATE,
				trn_sample_special_technical_header.APPROVED_BY,
				trn_sample_special_technical_header.APPROVED_DATE,
				B.strUserName AS creator 
				FROM `trn_sample_special_technical_header` 
				Inner Join sys_users AS B ON B.intUserId = trn_sample_special_technical_header.SAVED_BY
				INNER JOIN trn_sampleinfomations ON trn_sample_special_technical_header.SAMPLE_NO = trn_sampleinfomations.intSampleNo AND trn_sample_special_technical_header.SAMPLE_YEAR = trn_sampleinfomations.intSampleYear AND trn_sample_special_technical_header.REVISION_NO = trn_sampleinfomations.intRevisionNo
				WHERE
				trn_sample_special_technical_header.SAMPLE_NO = '$x_sampleNo' AND
				trn_sample_special_technical_header.SAMPLE_YEAR = '$x_sampleYear' AND
				trn_sample_special_technical_header.REVISION_NO = '$x_revNo' 
				GROUP BY
				trn_sample_special_technical_header.COMBO,
				trn_sample_special_technical_header.PRINT";
		
	        $result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result)){
			$x_combo 		= $row['COMBO'];
            $x_print 		= $row['PRINT'];
			$status         = $row['STATUS'];
			$levels         = $row['LEVELS'];
			 ?>
      
  <?php
		ob_start();
		$flag_rm	= false;
  ?>
      <tr>
    <td>
    <table cellpadding="0" cellspacing="0"  class="bordered" id="MainGrid2" width="100%">
    <tr style="background-color:#9CF;" id="Combo_Print">
          <td class="normalfnt" style="font-weight:bold">
          <?php if($status == 1) { ?>
          Approved
          <?php } 
		  else if($status == 0){?>
          Rejected
          <?php }
		  else if($status > 1 && $status <= $levels+1){ ?>
          <input type="checkbox" name="chkComboSpecialRM" id="chkComboSpecialRM" class="chkClass" style="transform:scale(1.5); padding:10px;"/>
          <?php }?>
          </td>
          <td id="combo" style="font-weight:bold; padding:5px; word-spacing: 5px">Combo :<span id="getCombo"><?php echo $x_combo; ?></span></td>
          <td  style="font-weight:bold; padding:5px; text-align:right">Print :</td>
          <td id="print" style="font-weight:bold"><?php echo $x_print ?></td>

      </tr>
      <tr>
        <th height="17" colspan="9"   class="bordered"><strong>ITEMS IN ROLL FORM</strong></th>
      </tr>
      <tr>
        <th width="100" height="19">Technique</th>
        <th width="140" height="19">Item</th>
        <th width="50">Print Sizes</th>
        <th width="85">Consumption</th>
      </tr>
           <?php 
			  
			  		   $bulk_order_flag	=check_special_consumption_approved($x_sampleNo,$x_sampleYear,$x_revNo,$x_combo,$x_print);
			  
 		             $sqlm = "SELECT 
						mst_techniques.strName AS TECHNIQUE,
						trn_sample_foil_consumption.intTechniqueId,
						trn_sample_foil_consumption.intItem,
						mst_item.strName as item,
						mst_item.foil_width,
						mst_item.foil_height,
						trn_sample_foil_consumption.intColorId,
						trn_sample_foil_consumption.intTechniqueId,
						trn_sample_foil_consumption.dblWidth as size_w,
						trn_sample_foil_consumption.dbHeight as size_h,
						trn_sample_foil_consumption.dblFoilQty as qty,
						trn_sample_foil_consumption.dblMeters, 
						trn_sample_foil_consumption.strCuttingSide ,
						'saved' as status
						FROM trn_sample_foil_consumption  
						INNER JOIN mst_techniques ON trn_sample_foil_consumption.intTechniqueId = mst_techniques.intId
						INNER Join mst_item ON trn_sample_foil_consumption.intItem = mst_item.intId 
												INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId

						WHERE
						trn_sample_foil_consumption.intSampleNo =  '$x_sampleNo' AND
						trn_sample_foil_consumption.intSampleYear =  '$x_sampleYear' AND
						trn_sample_foil_consumption.intRevisionNo =  '$x_revNo' AND
						trn_sample_foil_consumption.strCombo =  '$x_combo' AND
						trn_sample_foil_consumption.strPrintName =  '$x_print' AND  ";
						if($bulk_order_flag!=1){
						$sqlm .= " mst_item.ROLL_FORM =  '1' AND 
					    mst_units.intId <> 6 AND "; 
						}
						$sqlm .= "
						 CHANGE_CONPC_AFTER_SAMPLE =1  /*AND
						mst_item.ROLL_FORM =  '1'  AND 
						mst_item.intUOM <> 6 */";
				
						//if($intUser==2)
						//	echo $sqlm;

 						$result_tech = $db->RunQuery($sqlm);
						while($rowm=mysqli_fetch_array($result_tech))
						{
                            if(getRecieptAvailable($x_sampleNo,$x_sampleYear,$x_revNo,$x_combo,$x_print,$rowm['intColorId'],$rowm['intTechniqueId'],$rowm['intItem']) == 1){
							$technique 		= $rowm['intTechniqueId'];
							$techniqueName 	= $rowm['TECHNIQUE'];
							$foilItem 		= $rowm['intItem'];
							$foilItemName 	= $rowm['item']." (".$rowm['foil_width']."/".$rowm['foil_height']." )";
							$print_width 	= $rowm['size_w'];
							$print_height	= $rowm['size_h'];
							$consump		= $rowm['dblMeters'];
                            $foilItemName_t = $rowm['item'];
							$flag_rm		= true;
							?>
      <tr class="dataRow roll">
        <td height="21" bgcolor="#FFFFFF" id="<?php echo $technique; ?>" class="roll_technique"><?php echo $techniqueName; ?></td>
        <td height="21" bgcolor="#FFFFFF" id="<?php echo $foilItem; ?>" class="roll_item"><?php echo $foilItemName; ?></td>
          <td>
              <table>
                  <tr>
                      <th width="100" height="15">Width</th>
                      <th width="140" height="15">Height</th>
                      <th width="50">Consumption</th>
                  </tr>
         <?php
         $sql_width_Height = "SELECT intPrintWidth,intPrintHeight, intConsumption FROM trn_sample_specialtechniques_print_width_height
               WHERE intSampleNo = '$x_sampleNo' AND intSampleYear = '$x_sampleYear' AND intRevisionNo = '$x_revNo' AND strComboName = '$x_combo'
               AND strPrintName = '$x_print' AND intItem = '$foilItem' AND TECHNIQUE = '$technique'";
         $result_WH = $db->RunQuery($sql_width_Height);
         $num_rows = mysqli_num_rows($result_WH);
         if($num_rows>0){
         while($rowWH = mysqli_fetch_array($result_WH))
         {
             $userE_width 	    = round($rowWH['intPrintWidth'],6);
             $userE_height	    = round($rowWH['intPrintHeight'],6);
             $userE_consumption = round($rowWH['intConsumption'],6);
             ?>
             <tr>
                 <td bgcolor="#FFFFFF" class="normalfnt" style="text-align: center"><?php {echo $userE_width;} ?></td>
                 <td bgcolor="#FFFFFF" class="normalfnt" style="text-align: center"><?php {echo $userE_height;} ?></td>
                 <td bgcolor="#FFFFFF" class="normalfnt" style="text-align: center"><?php {echo $userE_consumption;} ?></td>
             </tr>
         <?php }
          } else{?>
             <tr>
                 <td bgcolor="#FFFFFF" class="normalfnt" style="text-align: center"><?php {echo $print_width;} ?></td>
                 <td bgcolor="#FFFFFF" class="normalfnt" style="text-align: center"><?php {echo $print_height;} ?></td>
                 <td bgcolor="#FFFFFF" class="normalfnt" style="text-align: center">&nbsp;</td>
             </tr>
      <?php   }?>
        </table>
       </td>
        <td bgcolor="#FFFFFF" class="normalfnt" style="text-align: right"><?php { echo $consump; }?></td>
      </tr>
      <tr class="dataRow">
        <td height="23" bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
      </tr>
      <?php
						}
                        }
						?>
    </table>
  </td>
  </tr>
  <tr>
    <td>
      <table cellpadding="0" cellspacing="0"   class="bordered" id="MainGrid3" width="65%">
        <tr class="">
          <th height="17" colspan="3"><strong>ITEMS IN NON-ROLL FORM</strong></th>
        </tr>
        <tr>
          <th width="33%" height="19">Technique</th>
          <th width="45%" height="19">Item</th>
          <th width="22%" >Consumption</th>
        </tr>
        <?php 
 							  	    $sqlw = "SELECT
										mst_techniques.strName AS TECHNIQUE,
										trn_sample_spitem_consumption.intTechniqueId,
										mst_item.strName as item,
										mst_item.foil_width,
										mst_item.foil_height,
										trn_sample_spitem_consumption.intColorId,
										trn_sample_spitem_consumption.intTechniqueId,
										trn_sample_spitem_consumption.intItem,
										trn_sample_spitem_consumption.dblQty
										FROM trn_sample_spitem_consumption
										INNER JOIN mst_techniques ON trn_sample_spitem_consumption.intTechniqueId = mst_techniques.intId
										INNER Join mst_item ON trn_sample_spitem_consumption.intItem = mst_item.intId 
										WHERE
										trn_sample_spitem_consumption.intSampleNo =  '$x_sampleNo' AND
										trn_sample_spitem_consumption.intSampleYear =  '$x_sampleYear' AND
										trn_sample_spitem_consumption.intRevisionNo =  '$x_revNo' AND
										trn_sample_spitem_consumption.strCombo =  '$x_combo' AND
										trn_sample_spitem_consumption.strPrintName =  '$x_print'  AND 
										CHANGE_CONPC_AFTER_SAMPLE =1  /*AND 
										mst_item.ROLL_FORM <>  '1' AND 
										mst_item.intUOM <> 6 */ ";
    
 							$resultw = $db->RunQuery($sqlw);
							while($roww=mysqli_fetch_array($resultw))
							{
                                if(getRecieptAvailable($x_sampleNo,$x_sampleYear,$x_revNo,$x_combo,$x_print,$roww['intColorId'],$roww['intTechniqueId'],$roww['intItem']) == 1){

								$technique 		= $roww['intTechniqueId'];
								$techniqueName 	= $roww['TECHNIQUE'];
								$foilItem 		= $roww['intItem'];
								$foilItemName 	= $roww['item'];
								$conpc			= $roww['dblQty'];
								$flag_rm		= true;
							?>
        <tr class="dataRow non_roll">
          <td height="21" bgcolor="#FFFFFF" id="<?php echo $technique; ?>" class="non_roll_technique"><?php echo $techniqueName; ?></td>
          <td height="21" bgcolor="#FFFFFF" id="<?php echo $foilItem; ?>" class="non_roll_item"><?php echo $foilItemName; ?></td>
          <td bgcolor="#FFFFFF" class="normalfnt" ><?php echo $conpc; ?></td>
        </tr><?php } } ?>
      </table>
    </td>
  </tr><?php } ?>
        </table>
     <?php
 	$body 			= ob_get_clean();
	if($flag_rm	== true)
		echo $body;
	  
	  
	  //if($intUser==2)
		 // echo $sqlw;
	  
	?>
    
    <table style="margin-left:-30%">
                  <tr>
                    <td width="56%" height="32" bgcolor="" class="normalfnt"><b>Enter By</b> - <?php echo $creator;?></td>
                    <td width="38%" bgcolor="" class="normalfnt">&nbsp;</td>
                    <td width="6%" class="normalfnt">&nbsp;</td>
                  </tr>
      <?php  
		$sql	="SELECT
				trn_sample_special_technical_header.SAMPLE_NO,
				trn_sample_special_technical_header.SAMPLE_YEAR,
				trn_sample_special_technical_header.REVISION_NO,
				trn_sample_special_technical_header.COMBO,
				trn_sample_special_technical_header.PRINT,
				trn_sample_special_technical_header.`STATUS`,
				trn_sample_special_technical_header.SAVED_BY,
				trn_sample_special_technical_header.SAVED_DATE,
				trn_sample_special_technical_header.MODIFIRED_BY,
				trn_sample_special_technical_header.MODIFIED_DATE,
				trn_sample_special_technical_header.APPROVED_BY,
				trn_sample_special_technical_header.APPROVED_DATE,
				B.strUserName AS creator 
				FROM `trn_sample_special_technical_header` 
				Inner Join sys_users AS B ON B.intUserId = trn_sample_special_technical_header.SAVED_BY
				INNER JOIN trn_sampleinfomations ON trn_sample_special_technical_header.SAMPLE_NO = trn_sampleinfomations.intSampleNo AND trn_sample_special_technical_header.SAMPLE_YEAR = trn_sampleinfomations.intSampleYear AND trn_sample_special_technical_header.REVISION_NO = trn_sampleinfomations.intRevisionNo
				WHERE
				trn_sample_special_technical_header.SAMPLE_NO = '$x_sampleNo' AND
				trn_sample_special_technical_header.SAMPLE_YEAR = '$x_sampleYear' AND
				trn_sample_special_technical_header.REVISION_NO = '$x_revNo' 
				GROUP BY
				trn_sample_special_technical_header.COMBO,
				trn_sample_special_technical_header.PRINT";
		
	        $result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result)){
			$intStatus		= $row['STATUS'];
			$x_combo 		= $row['COMBO'];
            $x_print 		= $row['PRINT']; 
	         ?> 
             <tr style="background-color:#9CF">
              <td><strong>Combo :</strong><span class="normalfnt">&nbsp;&nbsp;<?php echo $x_combo;?></span>
                  <strong>Print :</strong><span class="normalfnt">&nbsp;&nbsp;<?php echo $x_print ?></td>
            </tr><?php
 	if($intStatus!=0)
	{

				for($i=1; $i<=$approveLevel; $i++)
				{
					   $sqlc = "SELECT
							trn_sample_special_technical_header_approved_by.APPROVED_BY as intApproveUser,
							trn_sample_special_technical_header_approved_by.APPROVED_DATE as dtApprovedDate,
							sys_users.strUserName as UserName, 
							trn_sample_special_technical_header_approved_by.APPROVE_LEVEL as intApproveLevelNo
							FROM
							trn_sample_special_technical_header_approved_by
							Inner Join sys_users ON trn_sample_special_technical_header_approved_by.APPROVED_BY = sys_users.intUserId
WHERE
trn_sample_special_technical_header_approved_by.SAMPLE_NO = '$x_sampleNo' AND
trn_sample_special_technical_header_approved_by.SAMPLE_YEAR = '$x_sampleYear' AND
trn_sample_special_technical_header_approved_by.REVISION_NO = '$x_revNo' AND
trn_sample_special_technical_header_approved_by.COMBO = '$x_combo' AND
trn_sample_special_technical_header_approved_by.PRINT = '$x_print' AND
trn_sample_special_technical_header_approved_by.APPROVE_LEVEL =  '$i' AND 
trn_sample_special_technical_header_approved_by.STATUS = 0  order by APPROVE_LEVEL asc";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
						if($i==1)
						$desc="1st ";
						else if($i==2)
						$desc="2nd ";
						else if($i==3)
						$desc="3rd ";
						else
						$desc=$i."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
				?>
      <tr>
        <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
      </tr>
      <?php
			}
	}
	else{
					 $sqlc = "SELECT
							trn_sample_special_technical_header_approved_by.APPROVED_BY as intApproveUser,
							trn_sample_special_technical_header_approved_by.APPROVED_DATE as dtApprovedDate,
							sys_users.strUserName as UserName, 
							trn_sample_special_technical_header_approved_by.APPROVE_LEVEL as intApproveLevelNo
							FROM
							trn_sample_special_technical_header_approved_by
							Inner Join sys_users ON trn_sample_special_technical_header_approved_by.APPROVED_BY = sys_users.intUserId 
WHERE
trn_sample_special_technical_header_approved_by.SAMPLE_NO = '$x_sampleNo' AND
trn_sample_special_technical_header_approved_by.SAMPLE_YEAR = '$x_sampleYear' AND
trn_sample_special_technical_header_approved_by.REVISION_NO = '$x_revNo' AND
trn_sample_special_technical_header_approved_by.COMBO = '$x_combo' AND
trn_sample_special_technical_header_approved_by.PRINT = '$x_print' AND
trn_sample_special_technical_header_approved_by.APPROVE_LEVEL =  '0' AND 
trn_sample_special_technical_header_approved_by.STATUS = 0    order by APPROVE_LEVEL asc";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
					  ?>
      <tr>
        <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Rejected By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
      </tr>
      <?php
	}
			}
	
?>
      <tr height="40">
        <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
      </tr>
    </table>
  </div>
</form>
</body>
</html>
<?php
function getRecieptAvailable($sampleNo,$sampleYear,$revisionNo,$combo,$printName,$colorID,$intTechnicleID,$itemID){

global $db;

$sql	="SELECT
trn_sample_color_recipes.intSampleNo
FROM
trn_sample_color_recipes
WHERE
trn_sample_color_recipes.intSampleNo = '$sampleNo' AND
trn_sample_color_recipes.intSampleYear = '$sampleYear' AND
trn_sample_color_recipes.intRevisionNo = '$revisionNo' AND
trn_sample_color_recipes.strCombo = '$combo' AND
trn_sample_color_recipes.strPrintName = '$printName' AND
trn_sample_color_recipes.intColorId = '$colorID' AND
trn_sample_color_recipes.intTechniqueId = '$intTechnicleID' AND
trn_sample_color_recipes.intItem = '$itemID'
";

$result 	= $db->RunQuery($sql);
$row		= mysqli_fetch_array($result);
$status_cp		= $row['intSampleNo'];

if($status_cp){
return 0;
}
else
return 1;

}
			
function check_special_consumption_approved($sampleNo,$year,$revision,$combo,$print){
	global $db;
	
	  $sql		= "SELECT
	`trn_sample_special_technical_header` .`STATUS`
	FROM trn_sample_special_technical_header
	WHERE
	`trn_sample_special_technical_header` .SAMPLE_NO = '$sampleNo' AND
	`trn_sample_special_technical_header` .SAMPLE_YEAR = '$year' AND
	`trn_sample_special_technical_header` .REVISION_NO = '$revision' AND
	`trn_sample_special_technical_header` .COMBO = '$combo' AND
	`trn_sample_special_technical_header` .PRINT = '$print'
	";
    $result 	= $db->RunQuery($sql);
    $row		= mysqli_fetch_array($result);
    $status		= $row['STATUS'];

    if($status==1){
        return 1;
    }
    else
        return 0;
}


?>
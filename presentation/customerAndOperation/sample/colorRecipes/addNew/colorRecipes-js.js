// JavaScript Document
var basePath	="presentation/customerAndOperation/sample/colorRecipes/addNew/";
var measurments = [];

$(document).ready(function() {

	$("#frmColorRecipies").validationEngine();

	$('#frmColorRecipesWidthHeightPopup .cls_cal').die('keyup').live('keyup',function(){
		calculateConsumption(this);
	});

	$('#frmColorRecipies #buttonAdd').die('click').live('click',function(){
		addWidthHeightConsumption(this);
	});

	$('#frmColorRecipies #buttonView').die('click').live('click',function(){
		getViewPage(this);
	});

	$('#frmColorRecipies #buttonView').click(viewFrabricOptimalUsage);

	$('#frmColorRecipies #butNew_foil').die('click').live('click',addNewRow);
	$('#frmColorRecipies .technique').change();

	$('#frmColorRecipies #cboYear').die('change').live('change',function(){

		$('#frmColorRecipies #tr_detail').css('display','none');
		loadSampleNos();

	});

	$('#frmColorRecipies #cboYear').die('change').live('change',function(){
		window.location.href  =  "?q=433&sampleYear="+$('#cboYear').val();
		//loadSampleNos();
	});

	$('#frmColorRecipies #cboStyleNo').die('change').live('change',function(){

		$('#frmColorRecipies #tr_detail').css('display','none');
		loadSampleNos();

		var intSampleNo = $('#cboSampleNo').val();
		//$('#frmSampleInfomations').get(0).reset();
		$('#cboSampleNo').val(intSampleNo);
		$('#divPicture').html('');
		return;
	});

	$('#frmColorRecipies #cboSampleNo').die('change').live('change',function(){

		$('#frmColorRecipies #tr_detail').css('display','none');
		loadRevisionNo();

		var intSampleNo = $('#cboSampleNo').val();
		//$('#frmSampleInfomations').get(0).reset();
		$('#cboSampleNo').val(intSampleNo);
		$('#divPicture').html('');
		return;
	});

	$('#frmColorRecipies #cboRevisionNo').change(function(){
		window.location.href = "?q=433&graphicNo="+URLEncode($('#cboStyleNo').val())
			+'&sampleNo='+$('#cboSampleNo').val()
			+'&sampleYear='+$('#cboYear').val()
			+'&revNo='+$('#cboRevisionNo').val()
	});

	/*$('#frmColorRecipies #cboRevisionNo').die('click').live('change',function(){
	 loadCombo();

	 });
	 $('#frmColorRecipies #cboCombo').die('change').live('change',function(){
	 loadPrint();

	 });
	 */
	//----------------------------
	$('#frmColorRecipies .costPerInch').die('keyup').live('keyup',function(){
		calTotCostPrice(this);
	});
	//----------------------------
	$('#frmColorRecipies .usage').die('keyup').live('keyup',function(){
		calTotCostPrice(this);
	});
	//----------------------------
	$('.technique').die('change').live('change',function(){
		loadItem(this);
	});
	//----------------------------
	$('#frmColorRecipies .foil').die('change').live('change',function(){
		techniqueCalculations(this);
	});
	//----------------------------
	$('#frmColorRecipies .printWidth').die('keyup').live('keyup',function(){
		techniqueCalculations(this);
	});
	//----------------------------
	$('#frmColorRecipies .printHeight').die('keyup').live('keyup',function(){
		techniqueCalculations(this);
	});
	//----------------------------
	$('#frmColorRecipies .qty').die('keyup').live('keyup',function(){
		techniqueCalculations(this);
	});
	//----------------------------
	$('#frmColorRecipies .delImg').die('click').live('click',function(){
		$(this).parent().parent().remove();
	});
	//----------------------------

	function loadRevisionNo()
	{
		var url = basePath+"colorRecipes-db-get.php?requestType=loadRevisionNo&sampleNo="+$('#cboSampleNo').val()+"&sampleYear="+$('#cboYear').val();
		var obj = $.ajax({url:url,async:false});
		$('#cboRevisionNo').html(obj.responseText);
		$('#cboCombo').html('');
		$('#cboPrint').html('');
	}
	function loadSampleNos()
	{
		var url = basePath+"colorRecipes-db-get.php?requestType=loadSampleNo&styleNo="+URLEncode($('#cboStyleNo').val())+"&sampleYear="+$('#cboYear').val();
		var obj = $.ajax({url:url,async:false});
		$('#cboSampleNo').html(obj.responseText);
		$('#cboRevisionNo').html('');
		$('#cboCombo').html('');
		$('#cboPrint').html('');

	}
	function loadCombo()
	{
		var url = basePath+"colorRecipes-db-get.php?requestType=loadCombo&sampleNo="+URLEncode($('#cboSampleNo').val())+"&sampleYear="+$('#cboYear').val()+"&revNo="+$('#cboRevisionNo').val();
		var obj = $.ajax({url:url,async:false});
		$('#cboCombo').html(obj.responseText);
		$('#cboPrint').html('');
	}
	function loadPrint()
	{
		var url = basePath+"colorRecipes-db-get.php?requestType=loadPrint&sampleNo="+URLEncode($('#cboSampleNo').val())+"&sampleYear="+$('#cboYear').val()+"&revNo="+$('#cboRevisionNo').val()+"&combo="+$('#cboCombo').val();
		var obj = $.ajax({url:url,async:false});
		$('#cboPrint').html(obj.responseText);
	}


//------view the optimal usage of the frabric
	function viewFrabricOptimalUsage(){
		//alert("frabric usage");
	}

	/*data load part*/
	/*$('#cboPrint').change(function(){
	 window.location.href = "?q=433&graphicNo="+URLEncode($('#cboStyleNo').val())
	 +'&sampleNo='+$('#cboSampleNo').val()
	 +'&sampleYear='+$('#cboYear').val()
	 +'&revNo='+$('#cboRevisionNo').val()
	 +'&combo='+$('#cboCombo').val()
	 +'&printName='+$('#cboPrint').val()
	 });*/


	$('#butAddRow').die('click').live('click',function(){
		$(this).parent().parent().before('<tr>'+$(this).parent().parent().parent().find('tr:eq(0)').html()+'</tr>');
	});

//----------------------------

	$('#butReport').click(function() {

		window.open('?q=1177&graphicNo='+URLEncode($('#cboStyleNo').val())
			+'&mode=report'
			+'&sampleNo='+$('#cboSampleNo').val()
			+'&sampleYear='+$('#cboYear').val()
			+'&revNo='+$('#cboRevisionNo').val()
			+'&combo='+$('#cboCombo').val()
			+'&printName='+$('#cboPrint').val());
	});


	$('#butApprove').click(function() {

		window.open('?q=1177&graphicNo='+URLEncode($('#cboStyleNo').val())
			+'&mode=approve'
			+'&sampleNo='+$('#cboSampleNo').val()
			+'&sampleYear='+$('#cboYear').val()
			+'&revNo='+$('#cboRevisionNo').val()
			+'&combo='+$('#cboCombo').val()
			+'&printName='+$('#cboPrint').val());
	});


//if ($('#frmSampleInfomations').validationEngine('validate'))   

	$('#frmColorRecipies #butSave').die('click').live('click',function(){

		saveData('save');
	});

	$('#frmColorRecipies #butCopy').die('click').live('click',function(){

		saveData('copy');
	});

	$('#butNew').die('click').live('click',function(){
		window.location.href = '?q=433';
	});
	$('#butInsertRowPopup').die('click').live('click',function(){
		var rowCount = document.getElementById('tblSizesPopup').rows.length;
		document.getElementById('tblSizesPopup').insertRow(rowCount-1);
		rowCount = document.getElementById('tblSizesPopup').rows.length;
		var consum_auto,inputFeild;
		$(this).parent().parent().parent().parent().each(function () {
			consum_auto = $(this).find('#consum_auto').text();
		});
		if(consum_auto == 1){
			inputFeild = "<td id='deleteImage' align='center' bgcolor='#FFFFFF'><img src='images/del.png' class='delImgPopup' height='15' width='15'><span id='deleteImageSerialNo' style='visibility: hidden'>0</span></td><td id='txtWidth' align='center' bgcolor='#FFFFFF'><input name='txtWidth' id='txtWidth' style='width:80px; text-align:center' value='0' class='cls_cal cls_width validate[required,custom[number]] text-input' type='text'></td><td id='txtHeight' align='center' bgcolor='#FFFFFF'><input name='txtHeight' id='txtHeight' style='width:80px; text-align:center' value='0' class='cls_cal cls_height validate[required,custom[number]] text-input' type='text'></td><td id='consumption' align='center' bgcolor='#FFFFFF'><input value='0' size='10' style='text-align: center' type='text' id='ConsumptionInput' disabled></td>";
			}
		else if (consum_auto == 0){
			inputFeild = "<td id='deleteImage' align='center' bgcolor='#FFFFFF'><img src='images/del.png' class='delImgPopup' height='15' width='15'><span id='deleteImageSerialNo' style='visibility: hidden'>0</span></td><td id='txtWidth' align='center' bgcolor='#FFFFFF'><input name='txtWidth' id='txtWidth' style='width:80px; text-align:center' value='0' class='cls_cal cls_width validate[required,custom[number]] text-input' type='text'></td><td id='txtHeight' align='center' bgcolor='#FFFFFF'><input name='txtHeight' id='txtHeight' style='width:80px; text-align:center' value='0' class='cls_cal cls_height validate[required,custom[number]] text-input' type='text'></td><td id='consumption' align='center' bgcolor='#FFFFFF'><input value='0' size='10' style='text-align: center' type='text' id='ConsumptionInput'></td>";
		}
		if(rowCount == 3){
			document.getElementById('tblSizesPopup').rows[1].innerHTML = inputFeild;
			document.getElementById('tblSizesPopup').rows[1].cells[0].innerHTML = "<img src='images/del.png' width='15' height='15' class='delImgPopup' /><span id='deleteImageSerialNo' style='visibility: hidden'>0</span>";
			document.getElementById('tblSizesPopup').rows[1].cells[1].innerHTML = "<input name='txtWidth' value='0' type='text' id='txtWidth' style='width:80px; text-align:center' class='cls_cal cls_width validate[required,custom[number]] text-input'/>";
			document.getElementById('tblSizesPopup').rows[1].cells[2].innerHTML = "<input name='txtHeight' value='0' type='text' id='txtHeight' style='width:80px; text-align:center' class='cls_cal cls_height validate[required,custom[number]] text-input' />";
			//document.getElementById('tblSizesPopup').rows[1].cells[3].innerHTML = "<input type='text' value='0' size='10' style='text-align: center' id='ConsumptionInput' disabled>";
		}
		else{
			document.getElementById('tblSizesPopup').rows[rowCount-2].innerHTML = inputFeild;
			document.getElementById('tblSizesPopup').rows[rowCount-2].cells[0].innerHTML = "<img src='images/del.png' width='15' height='15' class='delImgPopup' /><span id='deleteImageSerialNo' style='visibility: hidden'>0</span>";
			document.getElementById('tblSizesPopup').rows[rowCount-2].cells[1].innerHTML = "<input name='txtWidth' value='0' type='text' id='txtWidth' style='width:80px; text-align:center' class='cls_cal cls_width validate[required,custom[number]] text-input'/>";
			document.getElementById('tblSizesPopup').rows[rowCount-2].cells[2].innerHTML = "<input name='txtHeight' value='0' type='text' id='txtHeight' style='width:80px; text-align:center' class='cls_cal cls_height validate[required,custom[number]] text-input' />";
			//document.getElementById('tblSizesPopup').rows[1].cells[3].innerHTML = "<input type='text' value='0' size='10' style='text-align: center' id='ConsumptionInput'>";

		}
	});
});

function saveData(type)
{
	showWaiting();

	var sampleNo 			= 	$('#cboSampleNo').val();
	var sampleYear 			= 	$('#cboYear').val();
	var revisionNo 			= 	$('#cboRevisionNo').val();

	var arrHeader			= "";
	var combo				= "";
	var printName			= "";
	var data 				= "requestType=saveDetails&type="+type;

	$('#frmColorRecipies #tblMainDetails > thead').each(function(index, element) {

		combo			= $(this).find('.tdCombo').html();
		printName		= $(this).find('.tdPrint').html();

		arrHeader += "{";
		arrHeader += '"sampleNo":"'+ sampleNo +'",' ;
		arrHeader += '"sampleYear":"'+ sampleYear +'",' ;
		arrHeader += '"revisionNo":"'+ revisionNo +'",' ;
		arrHeader += '"combo":"'+ combo +'",' ;
		arrHeader += '"printName":"'+ printName +'"' ;
		arrHeader +=  '},';

	});

	var arrFoil			= "";
	var techniqueId 	= "";
	var colourId	 	= "";
	var trIdRArr		= "";
	var itemId 			= "";
	var width 			= "";
	var height 			= "";
	var qty 			= "";
	var cuttingSide 	= "";
	var comboR			= "";
	var printR			= "";
	var meters 			= 0;

	$('#frmColorRecipies #tblMainDetails .roll').each(function(index, element) {

		trIdRArr		= 	$(this).attr('id').split('~');
		comboR			= 	trIdRArr[0];
		printR			= 	trIdRArr[1];
		colourId	 	= 	$(this).find('.roll_color').attr('id');
		techniqueId 	= 	$(this).find('.roll_technique').attr('id');
		itemId 			= 	$(this).find('.roll_item').attr('id');
		width 			= 	$(this).find('.printWidth').val();
		height 			= 	$(this).find('.printHeight').val();
		item_width 		= 	$(this).find('#foilWidth').val();
		item_height 	= 	$(this).find('#foilHeight').val();
		qty 			= 	1;
		meters 			= 	$(this).find('.roll_item_cons').val();
		cycles 			= 	$(this).find('.roll_item_cycles').val();

		arrFoil += "{";
		arrFoil += '"techniqueId":"'+ techniqueId +'",' ;
		arrFoil += '"colourId":"'+ colourId +'",' ;
		arrFoil += '"itemId":"'+ itemId +'",' ;
		arrFoil += '"width":"'+ width +'",' ;
		arrFoil += '"height":"'+ height +'",' ;
		arrFoil += '"item_width":"'+ item_width +'",' ;
		arrFoil += '"item_height":"'+ item_height +'",' ;
		arrFoil += '"qty":"'+ qty +'",' ;
		arrFoil += '"meters":"'+ meters +'",' ;
		arrFoil += '"cuttingSide":"'+ cuttingSide +'",' ;
		arrFoil += '"sampleNo":"'+ sampleNo +'",' ;
		arrFoil += '"sampleYear":"'+ sampleYear +'",' ;
		arrFoil += '"revisionNo":"'+ revisionNo +'",' ;
		arrFoil += '"combo":"'+ comboR +'",' ;
		arrFoil += '"cycles":"'+ cycles +'",' ;
		arrFoil += '"printName":"'+ printR +'"' ;
		arrFoil +=  '},';

	});

	var itemId 			= "";
	var arrSpecial		= "";
	var trIdNRArr		= "";
	var comboNR			= "";
	var printNR			= "";
	var	techniqueId		= "";
	var colourId	 	= "";
	var itemId			= "";
	var qty				= "";

	$('#frmColorRecipies #tblMainDetails .non_roll_technique').each(function(index, element) {

		trIdNRArr		= 	$(this).parent().attr('id').split('~');
		comboNR			= 	trIdNRArr[0];
		printNR			= 	trIdNRArr[1];
		techniqueId		= 	$(this).attr('id');
		colourId		= 	$(this).parent().find('.non_roll_color').attr('id');
		itemId			= 	$(this).parent().find('.non_roll_item').attr('id');
		qty 			= 	$(this).parent().find('.non_roll_item_cons').val();

		if(itemId!='')
		{
			arrSpecial += "{";
			arrSpecial += '"techniqueId":"'+ techniqueId +'",' ;
			arrSpecial += '"colourId":"'+ colourId +'",' ;
			arrSpecial += '"itemId":"'+ itemId +'",' ;
			arrSpecial += '"qty":"'+ qty +'",' ;
			arrSpecial += '"sampleNo":"'+ sampleNo +'",' ;
			arrSpecial += '"sampleYear":"'+ sampleYear +'",' ;
			arrSpecial += '"revisionNo":"'+ revisionNo +'",' ;
			arrSpecial += '"combo":"'+ comboNR +'",' ;
			arrSpecial += '"printName":"'+ printNR +'"' ;
			arrSpecial +=  '},';
		}

	});

	arrHeader 		= arrHeader.substr(0,arrHeader.length-1);
	var arrHeader	= '['+arrHeader+']';

	arrFoil 		= arrFoil.substr(0,arrFoil.length-1);
	var arrFoil		= '['+arrFoil+']';

	arrSpecial 		= arrSpecial.substr(0,arrSpecial.length-1);
	var arrSpecial	= '['+arrSpecial+']';

	data	   	   += "&arrHeader="+arrHeader+"&arrFoil="+arrFoil+"&arrSpecial="+arrSpecial;
	var url 		= basePath+"colorRecipes-db-set.php";
	$.ajax({
		url:url,
		data:data,
		type:'post',
		async:false,
		dataType: "json",
		data:data,
		async:false,
		success:function(json){
			if(type=='copy')
				$('#frmColorRecipies #butCopy').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
			else
				$('#frmColorRecipies #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
			if(json.type=='pass')
			{
				var t=setTimeout("alertx()",1000);
				var t=setTimeout("alertCopy()",1000);
				hideWaiting();
				window.location.href = window.location.href;
				return;
			}
		},
		error:function(xhr,status){

			if(type=='copy')
			{
				$('#frmColorRecipies #butCopy').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				var t=setTimeout("alertCopy()",3000);
			}
			else
			{
				$('#frmColorRecipies #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				var t=setTimeout("alertx()",3000);
			}

			hideWaiting();
		}
	});
	hideWaiting();
}
//----------------------------------------------------------------------
function calTotCostPrice(obj)
{
	var printWidth = document.getElementById('txtPrintWidth').value;
	var printHeight = document.getElementById('txtPrintHeight').value;
	var costPerInch = $(obj).parent().parent().find('.costPerInch').val();
	var usage = $(obj).parent().parent().find('.usage').val();
	//	var totCost=qty*costPerInch*usage/100;
	var totCost=printWidth*printHeight*costPerInch*usage/100;
	//alert(totCost);
	$(obj).parent().parent().find('.totCost').text(totCost);

}
//----------------------------------------------------------------------
function loadItem(obj)
{
	var technique = $(obj).parent().parent().find('.technique').val();
	var sampleNo =$('#cboSampleNo').val();
	var sampleYear = $('#cboYear').val();
	var revNo = $('#cboRevisionNo').val();
	var combo = $('#cboCombo').val();
	var printName = $('#cboPrint').val();
	var url 		= basePath+"colorRecipes-db-get.php?requestType=loadItem";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:"technique="+technique+"&sampleNo="+sampleNo+"&revNo="+revNo+"&combo="+combo+"&sampleYear="+sampleYear+"&printName="+printName,
		async:false,
		success:function(json){
			var itemId=json.itemId;
			$(obj).parent().parent().find('.foil').val(json.itemId)
			//	if(itemId){
			$(obj).parent().parent().find('.foil').change();
			//	}
		}
	});
}
//----------------------------------------------------------------------
function techniqueCalculations(obj)
{
	var foil = $(obj).parent().parent().find('.foil').val();
	var url 		= basePath+"colorRecipes-db-get.php?requestType=loadMeasurements";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:"foil="+foil,
		async:false,
		success:function(json){
			foilWidth=json.width;
			foilHeight=json.height;
			foilcostPerMtr=json.price;
		}
	});

	var printWidth = $(obj).parent().parent().find('.printWidth').val();
	var printHeight = $(obj).parent().parent().find('.printHeight').val();
	var qty = $(obj).parent().parent().find('.qty').val();

	//------------------------------
	var pcs=Math.floor((foilHeight/printWidth))
	if(printWidth==0){
		pcs=0;
	}
	//alert(printWidth);
	$(obj).parent().parent().find('.pcsFrmHeight').text((foilHeight/printWidth).toFixed(4));
	$(obj).parent().parent().find('.pcs').text(pcs);
	var mtrs=((printHeight/pcs)*qty)/39.5;
	if(pcs==0){
		mtrs=0;
	}
	$(obj).parent().parent().find('.meters').text(mtrs.toFixed(4));
	$(obj).parent().parent().find('.cost').text((mtrs*foilcostPerMtr).toFixed(4));

	//-------------------
	var pcs=Math.floor((foilHeight/printHeight));
	if(printWidth==0){
		pcs=0;
	}
	$(obj).parent().parent().next().find('.pcsFrmHeight').text((foilHeight/printHeight).toFixed(4));
	$(obj).parent().parent().next().find('.pcs').text(pcs);
	var mtrs=((printWidth/pcs)*qty)/39.5;
	if(pcs==0){
		mtrs=0;
	}
	$(obj).parent().parent().next().find('.meters').text(mtrs.toFixed(4));
	$(obj).parent().parent().next().find('.cost').text((mtrs*foilcostPerMtr).toFixed(4));
}
//----------------------------------------------------------------------
function alertx()
{
	$('#frmColorRecipies #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmColorRecipies #butDelete').validationEngine('hide')	;
}

function closePopUp(){

}
//----------------------------------------------

function addNewRow()
{
	var tr1 = $(this).parent().parent().prev().html();
	var tr2 = $(this).parent().parent().prev().prev().html();

	$(this).parent().parent().before('<tr>'+tr2+'</tr><tr>'+tr1+'</tr>');
	//$(this).parent().parent().before(tr1);
	//alert(tr1);
}

function alertCopy()
{
	$('#frmColorRecipies #butCopy').validationEngine('hide')	;
}

//--------Add width height and consumption for each
function addWidthHeightConsumption(e){
	var sampleNo    = $("#cboSampleNo").val();
	var sample_year = $("#cboYear").val();
	var revisionNo  = $("#cboRevisionNo").val();
	var comboName,printName,item,foilWidthPopUp,foilHeightPopUp,consum_auto,technique,colorId;
	$(e).parent().parent().parent().parent().parent().parent().parent().parent().each(function() {
		comboName = $(this).find('#comboName').text();
		printName = $(this).find('#printName').text();
	});
	technique 	= $(e).closest('tr').find('.roll_technique').attr('id');
	colorId 	= $(e).closest('tr').find('.roll_color').attr('id');
	$(e).parent().parent().each(function() {
		item 		    = $(this).find('#foilItem').text();
		foilWidthPopUp  = $(this).find('#foilWidth').val();
		foilHeightPopUp = $(this).find('#foilHeight').val();
		consum_auto     = $(this).find('.isComsum').text();
		cycles     		= $(this).find('.roll_item_cycles').val();
	});
	popupWindow3('1');
	$('#popupContact1').load(basePath+"colorRecipesWidthHeightPopup.php?comboName="+URLEncode(comboName)+"&printName="+URLEncode(printName)+"&item="+URLEncode(item)+"&sampleNo="+sampleNo+"&sample_year="+sample_year+"&revisionNo="+revisionNo+"&foilWidthPopUp="+foilWidthPopUp+"&foilHeightPopUp="+foilHeightPopUp+"&consum_auto="+consum_auto+"&technique="+technique+"&colorId="+colorId+"&cycles="+cycles,function(){
		$('#butAdd').die('click').live('click',function(){
			savePrintSizes(e,cycles);
		});
		$('.delImgPopup').die('click').live('click',function(){
 			deletePrintSizes(e,this);
		});
		$('#butClose1').click(disablePopup);
		$('#butClose1').die('click').live('click',function(){
			//location.reload();
		});

	});
}
function deletePrintSizes(e,thisObj){

	var rowCount1 = document.getElementById('tblSizesPopup').rows.length;
	if(rowCount1>=3)
	{
		//alert($(thisObj).parent().parent().html())
 		$(thisObj).parent().parent().remove();
		//Get deleting Row serial number
		var sampleNo    = $("#cboSampleNo").val();
		var sample_year = $("#cboYear").val();
		var revisionNo  = $("#cboRevisionNo").val();
		var comboName   = $("#ComboNamePopUp").html();
		var PrintName   = $("#printNamePopUp").html();
		var foilItem    = $("#itemName").html();
		var technique   = $("#techniquePopUp").text();
		var color   	= $("#colorPopUp").text();
		var serialNum_deleted = $(thisObj).closest('tr').find("#deleteImageSerialNo").html();
		var data = "requestType=DeleteSerialConsumption&serialNum_deleted="+serialNum_deleted+"&sampleNo="+sampleNo+"&sample_year="+sample_year+"&revisionNo="+revisionNo+"&comboName="+comboName+"&PrintName="+PrintName+"&foilItem="+foilItem+"&technique="+technique+"&color="+color;

		//Send deleting row data to db file
		var url = "presentation/customerAndOperation/sample/colorRecipes/addNew/colorRecipes-db-set.php";
		var obj = $.ajax({
			url:url,
			dataType: "json",
			data:data,
			async:false,
			success:function(json){
				if(json.type=='pass')
				{
					$(e).parent().parent().find('#textfield16R').val(json.con);
					alert(json.msg);
					var t=setTimeout("alertx()",2000);
					return;
				}
			},
			error:function(xhr,status){
				//$('#frmColorRecipesWidthHeightPopup #butAdd').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				//var t=setTimeout("alertx()",3000);
				//function (xhr, status){errormsg(status)}
			}
		});
	}


}
//--------Add clicked rows
function savePrintSizes(e,cycles){
	var comsumption;

	if ($('#frmColorRecipesWidthHeightPopup').validationEngine('validate'))
	{
		var sampleNo    = $("#cboSampleNo").val();
		var sample_year = $("#cboYear").val();
		var revisionNo  = $("#cboRevisionNo").val();
		var comboName   = $("#ComboNamePopUp").html();
		var PrintName   = $("#printNamePopUp").html();
		var foilItem    = $("#itemName").html();
		var consumption_auto   = $("#consumption_auto").text();
		var technique   = $("#techniquePopUp").text();
		var color   = $("#colorPopUp").text();
		var data = "requestType=saveConsumption";

		data+="&sampleNo=" +        sampleNo;
		data+="&sample_year=" +		sample_year;
		data+="&revisionNo=" +	    revisionNo;
		data+="&comboName=" +	    comboName;
		data+="&PrintName=" +	    PrintName;
		data+="&foilItem=" +	    foilItem;
		data+="&technique=" +	    technique;
		data+="&color=" +	    color;
		//////////////////// saving main grid part //////////////////////////////////////////
		var rowCount = $('#tblMain >tbody >tr').length;
		var rowCount = document.getElementById('tblSizesPopup').rows.length;
		var row = 0;
		var arr="[";
		var totalCoumption = 0;
		var validNumber;
		var consum;
		for(var i=1;i<rowCount-1;i++)
		{
			var serialNo     = document.getElementById('tblSizesPopup').rows[i].cells[0].childNodes[1].innerHTML;
			var width        = document.getElementById('tblSizesPopup').rows[i].cells[1].childNodes[0].value;
			var height 		 = document.getElementById('tblSizesPopup').rows[i].cells[2].childNodes[0].value;
			   consum	 	 = document.getElementById('tblSizesPopup').rows[i].cells[3].childNodes[0].value;

			totalCoumption += +consum;
			if(+width > 0 && +height > 0 && consum > 0) {
				validNumber = true;
				arr += "{";
				arr += '"serialNo":"' + serialNo + '",';
				arr += '"width":"' + width + '",';
				arr += '"height":"' + height + '",';
				arr += '"cycles":"' + cycles + '",';
				arr += '"consum":"' + consum + '"';
				arr += '},';
			}
			else {
				validNumber = false;
				alert("Print sizes Are Invalid");
				break;
			}
		}
		arr = arr.substr(0,arr.length-1);
		arr += " ]";

		data+="&arr="	+	arr;
		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = "presentation/customerAndOperation/sample/colorRecipes/addNew/colorRecipes-db-set.php";
		if(validNumber){
			var obj = $.ajax({
			url:url,
			dataType: "json",
			data:data,
			async:false,
			success:function(json){
				$('#frmColorRecipesWidthHeightPopup #butAdd').validationEngine('showPrompt', json.msg,json.type );
				if(json.type=='pass')
				{
					$(e).parent().parent().find('#textfield16R').val(json.con);
					var t=setTimeout("alertx()",2000);
					return;
				}
				if(json.type=='fail')
				{

					var t=setTimeout("alertx()",2000);
					return;
				}
			},
			error:function(xhr,status){
				$('#frmColorRecipesWidthHeightPopup #butAdd').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				var t=setTimeout("alertx()",3000);
			}
		});}
	}
}

function calculateConsumption(e){
	if ($('#frmColorRecipesWidthHeightPopup').validationEngine('validate')) {
		var foilWidth, foilHeight,consum_auto;
		$(e).parent().parent().parent().parent().each(function () {
			foilWidth = parseFloat($(this).find('#foilWidthPopUp').text());
			foilHeight = parseFloat($(this).find('#foilHeightPopUp').text());
			consum_auto = $(this).find('#consum_auto').text();
			
		});
		$(e).closest('tr').find("input").each(function () {
			measurments.push(this.value);
		});
		
		var cycles	= parseFloat($('#cycles').html());
		if(cycles=='' || cycles==0)
			cycles	=1;

		var widthOne  = parseFloat(measurments[0]);
		var heightOne = parseFloat(measurments[1]);
		var conpc ;
	    var data = "requestType=getConsumption&foilWidth="+foilWidth+"&foilHeight="+foilHeight+"&widthOne="+widthOne+"&heightOne="+heightOne;

		//Send deleting row data to db file
		var url = "presentation/customerAndOperation/sample/colorRecipes/addNew/colorRecipes-db-get.php";
		var obj = $.ajax({
			url:url,
			dataType: "json",
			data:data,
			async:false,
			success:function(json){
				if(json.type=='pass')
				{
					var units = json.val;
					 var pcs ;
					if (units>=0) {
					   pcs = units;
					}
					else {
					   pcs = 0;
					}
					if( pcs > 0 && consum_auto == 1){
						//alert(foilWidth+'/'+pcs+'/'+cycles)
						conpc = (foilWidth/ pcs/ cycles);
						$(e).parent().parent().find("#consumption").find("#ConsumptionInput").val(conpc.toFixed(6));
					}
					else if (pcs > 0 && consum_auto == 0){
						conpc = 0;
						$(e).parent().parent().find("#consumption").find("#ConsumptionInput").val(conpc);
					}
					else if(pcs == 0){
						conpc = 0;
						$(e).parent().parent().find("#consumption").find("#ConsumptionInput").val(0);
					}
					var t=setTimeout("alertx()",2000);
					return;
				}
			},
			error:function(xhr,status){
			}
		});
		
	}
   else {
		$(e).parent().parent().find("#consumption").find("#ConsumptionInput").val("");
	}
	measurments = [];
}

function getViewPage(e){
	var sampleNo    = $("#cboSampleNo").val();
	var sample_year = $("#cboYear").val();
	var revisionNo  = $("#cboRevisionNo").val();

	var foilWidth,foilHeight,itemName,technique;
	$(e).parent().parent().each(function() {
		itemName   	= $(this).find('.roll_item').attr('id');
		foilWidth  	= $(this).find('#foilWidth').val();
		foilHeight 	= $(this).find('#foilHeight').val();
		technique 	= $(this).find('.roll_technique').attr('id');
		colorId		= $(this).find('.roll_color').attr('id');
	});
	var comboName,printName,consumption;
	$(e).parent().parent().parent().parent().parent().parent().parent().parent().each(function() {
		comboName = $(this).find('#comboName').text();
		printName = $(this).find('#printName').text();
		consumption = $(this).find('#textfield16').val();
	});
	if(consumption != '') {
		window.open(basePath + 'auto_consumption.php?sampleNo=' + sampleNo + "&sample_year=" + sample_year + "&revisionNo=" + revisionNo + "&comboName=" + comboName + "&PrintName=" + printName + "&foilItem=" + itemName + "&foilWidth=" + foilWidth + "&foilHeight=" + foilHeight+"&technique="+technique+"&colorId="+colorId);
	}
	else{
		alert("No Consumption For this Item");
	}
}


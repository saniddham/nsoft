<?php

session_start();
include 		"../../../../../dataAccess/Connector.php";
include_once 	"../../../../../class/cls_commonErrorHandeling_get.php";
//ini_set('display_errors',1);
$requestType 	    = $_REQUEST['requestType'];
//$sampleNo 		= $_REQUEST['sampleNo'];
$userId 			= $_SESSION['userId'];
$apprLevels			= 2;
$programCode		= 'P0433';

//if($userId==2)
	//ini_set('display_errors',1);

$obj_errorHandeling			 = new cls_commonErrorHandeling_get($db);

if($requestType=='saveDetails')
{
	$db->begin();

	$savedStatus	= true;
	$errorMsg		= '';
	$errorSql		= '';

	$type_copy		= $_REQUEST['type'];
	$arrHeader		= json_decode($_REQUEST['arrHeader'], true);//arrHeader
	$arrFoil 		= json_decode($_REQUEST['arrFoil'], true);
	$arrSpecial 	= json_decode($_REQUEST['arrSpecial'], true);
	$chkCopyStatus	= 0;

	$sampleNo   = "";
	$sampleYear = "";
	$revisionNo = "";

	foreach($arrHeader as $rowHeader)
	{
		$sampleNo		= $rowHeader['sampleNo'];
		$sampleYear		= $rowHeader['sampleYear'];
		$revisionNo		= $rowHeader['revisionNo'];
		$combo			= $rowHeader['combo'];
		$printName		= $rowHeader['printName'];
		$prev_revNo		= $revisionNo - 1;

		$sample_room_approved	= getSampleRoomApproval($sampleNo,$sampleYear,$revisionNo,$combo,$printName);
		$sp_tech_approved		= getSpeciaTechniquesApproval($sampleNo,$sampleYear,$revisionNo,$combo,$printName);

		if($sample_room_approved==1 && $sp_tech_approved !=1){
			if($sampleNo)
			{
				$sql		=" SELECT
									trn_sampleinfomations.intStatus,
									trn_sampleinfomations.intTechnicalStatus
									FROM `trn_sampleinfomations`
									WHERE
									trn_sampleinfomations.intSampleNo = '$sampleNo' AND
									trn_sampleinfomations.intSampleYear = '$sampleYear' AND
									trn_sampleinfomations.intRevisionNo = '$revisionNo' ";

				$results 		= $db->RunQuery2($sql);
				$rowS			= mysqli_fetch_array($results);
				$sampStatus 	= $rowS['intStatus'];
				$techStatus 	= $rowS['intTechnicalStatus'];

				$sample_room_approved	= getSampleRoomApproval($sampleNo,$sampleYear,$revisionNo,$combo,$printName);
				$sp_tech_approved		= getSpeciaTechniquesApproval($sampleNo,$sampleYear,$revisionNo,$combo,$printName);

				if($sample_room_approved==1 && $sp_tech_approved !=1){

					/*$responseChk 	 		= validateBeforeSave($sampleNo,$sampleYear,$revisionNo,$combo,$printName,$userId,$programCode);
                    if($responseChk["type"]=='fail' && $savedStatus)
                    {
                        $savedStatus		= false;
                        $err_msg 			= $responseChk["msg"];
                    }*/
					if(confirmedOrders($sampleNo,$sampleYear,$revisionNo,$combo,$printName) && $savedStatus)
					{
						$savedStatus		= false;
						$err_msg		 	= 'PO confirmations already raised.cant Edit.'.$sampleNo.','.$sampleYear.','.$revisionNo.','.$combo.','.$printName;
					}
				}
			}
			if($type_copy=='copy')
			{
				/*$sqlF = "SELECT
                            trn_sample_foil_consumption.intSampleNo
                            FROM trn_sample_foil_consumption
                            INNER JOIN mst_techniques ON trn_sample_foil_consumption.intTechniqueId = mst_techniques.intId
                            WHERE
                            mst_techniques.CHANGE_CONPC_AFTER_SAMPLE = 1 AND
                            mst_techniques.intRoll = 1 AND
                            trn_sample_foil_consumption.intSampleNo =  '$sampleNo' AND
                            trn_sample_foil_consumption.intSampleYear =  '$sampleYear' AND
                            trn_sample_foil_consumption.intRevisionNo =  '$revisionNo' AND
                            trn_sample_foil_consumption.strCombo =  '$combo' AND
                            trn_sample_foil_consumption.strPrintName =  '$printName'
                            ";*/
				$sqlF = "SELECT`
						trn_sample_special_technical_header.SAMPLE_NO
						FROM trn_sample_special_technical_header
						WHERE SAMPLE_NO = '$sampleNo' AND
						SAMPLE_YEAR = '$sampleYear' AND
						REVISION_NO = '$revisionNo' AND
						COMBO = '$combo' AND
						PRINT = '$printName' ";

				$result = $db->RunQuery2($sql);

				$resultF = $db->RunQuery2($sqlF);
				if(mysqli_num_rows($resultF)>0 && $savedStatus)
				{
					//$savedStatus	= false;
					//$err_msg		= 'Details already Saved. Cant copy.';
					//$errorSql		= $sqlF;
				}
				else
					$coppied_string	.='('.$combo.'/'.$printName.'), ';

			}




			// Update approved by status----------
			$sql_M = "SELECT MAX(STATUS) as M_status FROM
									trn_sample_special_technical_header_approved_by
									WHERE SAMPLE_NO = '$sampleNo' AND
									SAMPLE_YEAR = '$sampleYear' AND
									REVISION_NO = '$revisionNo' AND
									COMBO = '$combo' AND
									PRINT = '$printName'";
			$resultM 		= $db->RunQuery2($sql_M);
			$rowM			= mysqli_fetch_array($resultM);
			$max_status 	= $rowM['M_status'];

			$sql = "UPDATE  trn_sample_special_technical_header_approved_by
					SET STATUS	= ($max_status+1)
					WHERE SAMPLE_NO = '$sampleNo' AND
					SAMPLE_YEAR = '$sampleYear' AND
					REVISION_NO = '$revisionNo' AND
					COMBO = '$combo' AND
					PRINT = '$printName' ";

			$result = $db->RunQuery2($sql);

			//------------------------------------

			$sql = "DELETE FROM trn_sample_special_technical_header
					WHERE SAMPLE_NO = '$sampleNo' AND
					SAMPLE_YEAR = '$sampleYear' AND
					REVISION_NO = '$revisionNo' AND
					COMBO = '$combo' AND
					PRINT = '$printName' ";

			$result = $db->RunQuery2($sql);
			if(!$result && $savedStatus)
			{
				$savedStatus		= false;
				$err_msg		 	= $db->errormsg ;
				$errorSql			= $sql;
			}

			$Status		= $apprLevels+1;
			$sql = "INSERT INTO trn_sample_special_technical_header(SAMPLE_NO,SAMPLE_YEAR,REVISION_NO,COMBO,PRINT,STATUS,LEVELS,SAVED_BY,SAVED_DATE) VALUES ( '$sampleNo',      '$sampleYear','$revisionNo','$combo','$printName','$Status','$apprLevels','$userId',now())";
			$result = $db->RunQuery2($sql);
			if(!$result && $savedStatus)
			{
				$savedStatus		= false;
				$err_msg		 	= $db->errormsg ;
				$errorSql			= $sql;
			}

		}
	}

//--------2015-03-06---------------------------

	/////////SAVE HEADER/////////////////////////////////////////


	////////////SAVE FOIL CONSUMPTIONS///////////////////////////
 	foreach($arrFoil as $rowFoil)
	{
		$technique		= $rowFoil['techniqueId'];
		$colourId		= $rowFoil['colourId'];
		$item			= $rowFoil['itemId'];
		$sampleNoF		= $rowFoil['sampleNo'];
		$sampleYearF	= $rowFoil['sampleYear'];
		$revisionNoF	= $rowFoil['revisionNo'];
		$comboF			= $rowFoil['combo'];
		$printNameF		= $rowFoil['printName'];
		$cycles			= $rowFoil['cycles'];

		$sample_room_approved	= getSampleRoomApproval($sampleNoF,$sampleYearF,$revisionNoF,$comboF,$printNameF);
		$sp_tech_approved		= getSpeciaTechniquesApproval($sampleNoF,$sampleYearF,$revisionNoF,$comboF,$printNameF);
		$sp_width_height_added	= checkWidthHeight($sampleNoF,$sampleYearF,$revisionNoF,$comboF,$printNameF,$technique,$colourId,$item);
		if($sp_width_height_added==0 && $type_copy=='save' && $sample_room_approved == 1){
			$savedStatus		= false;
			$err_msg		 	= 'Please save print width and height' ;
			$errorSql			= $sql;
		}
		else if($sample_room_approved!=1 && $type_copy=='save' && $rowFoil['qty'] >0){
			$savedStatus		= false;
			$err_msg		 	= 'Please raise sample room approval for '.$comboF.'/'.$printNameF ;
			$errorSql			= '';
		}
		else if($sample_room_approved!=1){
			$savedStatus		= false;
			$err_msg		 	= 'Please raise sample room approval for '.$comboF.'/'.$printNameF ;
			$errorSql			= '';
		}
		if($sample_room_approved==1 && $sp_tech_approved !=1)
		{

			$sqlDel2 = " DELETE
						FROM trn_sample_spitem_consumption
						WHERE
						intSampleNo='$sampleNoF' and
						intSampleYear='$sampleYearF' and
						intRevisionNo='$revisionNoF' and
						strCombo='$comboF' and
						strPrintName='$printNameF'
						and intTechniqueId='$technique'
						and intColorId='$colourId' and
						intItem='$item' ";

			$resultDel2 = $db->RunQuery2($sqlDel2);
			
			$sqlDel = " DELETE
						FROM trn_sample_foil_consumption
						WHERE
						intSampleNo='$sampleNoF' and
						intSampleYear='$sampleYearF' and
						intRevisionNo='$revisionNoF' and
						strCombo='$comboF' and
						strPrintName='$printNameF'
						and intTechniqueId='$technique'
						and intColorId='$colourId' and
						intItem='$item' ";

			$resultDel = $db->RunQuery2($sqlDel);
			if(!$resultDel && $savedStatus)
			{
				$savedStatus		= false;
				$err_msg		 	= $db->errormsg ;
				$errorSql			= $sqlDel;
			}

			$sqlIns = "INSERT INTO trn_sample_foil_consumption
					(
					intSampleNo,
					intSampleYear,
					intRevisionNo,
					strCombo,
					strPrintName,
					intTechniqueId,
					intColorId,
					intItem,
					dblWidth,
					dbHeight,
					dblFoilQty,
					dblMeters,
					strCuttingSide,
					intUser,
					dtDate,
					CYCLES,
					edited_foil_width,
					edited_foil_height
					)
					VALUES
					(
					'".$rowFoil['sampleNo']."',
					'".$rowFoil['sampleYear']."',
					'".$rowFoil['revisionNo']."',
					'".$rowFoil['combo']."',
					'".$rowFoil['printName']."',
					'".$rowFoil['techniqueId']."',
					'".$rowFoil['colourId']."',
					'".$rowFoil['itemId']."',
					'".val($rowFoil['width'])."',
					'".val($rowFoil['height'])."',
					'".val($rowFoil['qty'])."',
					'".val($rowFoil['meters'])."',
					'".$rowFoil['cuttingSide']."',
					'$userId',
					now(),
					'".$rowFoil['cycles']."',
					'".$rowFoil['item_width']."',
					'".$rowFoil['item_height']."'
					)";
			$resultIns 	= $db->RunQuery2($sqlIns);
			if(!$resultIns && $savedStatus)
			{
				$savedStatus		= false;
				$err_msg		 	= $db->errormsg ;
				$errorSql			= $sqlIns;
			}


			if($type_copy=='copy' && $sample_room_approved==1 && $sp_tech_approved !=1)
			{
				$sqlChkF = "SELECT *
							FROM
							trn_sample_foil_consumption
							INNER JOIN mst_item ON trn_sample_foil_consumption.intItem = mst_item.intId
							INNER JOIN mst_techniques ON trn_sample_foil_consumption.intTechniqueId = mst_techniques.intId
							left JOIN trn_sampleinfomations_combo_print_approvedby ON trn_sample_foil_consumption.intSampleNo = trn_sampleinfomations_combo_print_approvedby.SAMPLE_NO AND trn_sample_foil_consumption.intSampleYear = trn_sampleinfomations_combo_print_approvedby.SAMPLE_YEAR AND trn_sample_foil_consumption.intRevisionNo = trn_sampleinfomations_combo_print_approvedby.REVISION AND trn_sample_foil_consumption.strCombo = trn_sampleinfomations_combo_print_approvedby.COMBO AND trn_sample_foil_consumption.strPrintName = trn_sampleinfomations_combo_print_approvedby.PRINT
							INNER JOIN trn_sampleinfomations ON trn_sample_foil_consumption.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_sample_foil_consumption.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_sample_foil_consumption.intRevisionNo = trn_sampleinfomations.intRevisionNo
							LEFT JOIN trn_sample_special_technical_header ON trn_sample_foil_consumption.intSampleNo = trn_sample_special_technical_header.SAMPLE_NO AND trn_sample_foil_consumption.intSampleYear = trn_sample_special_technical_header.SAMPLE_YEAR AND trn_sample_foil_consumption.intRevisionNo = trn_sample_special_technical_header.REVISION_NO AND trn_sample_foil_consumption.strCombo = trn_sample_special_technical_header.COMBO AND trn_sample_foil_consumption.strPrintName = trn_sample_special_technical_header.PRINT
							WHERE
							mst_techniques.CHANGE_CONPC_AFTER_SAMPLE = 1 AND
							/*mst_techniques.intRoll = 1 AND*/
							 mst_item.ROLL_FORM =  '1' AND 
					    	 mst_item.intUOM <> 6 AND 
							(trn_sampleinfomations.intSampleRoomStatus = 1 OR trn_sampleinfomations_combo_print_approvedby.`STATUS` = 1) /*AND
							(trn_sample_special_technical_header.`STATUS` <> 1 or trn_sample_special_technical_header.`STATUS` IS NULL)*/ AND
							trn_sample_foil_consumption.intSampleNo = '".$rowFoil['sampleNo']."' AND
							trn_sample_foil_consumption.intSampleYear = '".$rowFoil['sampleYear']."' AND
							trn_sample_foil_consumption.intRevisionNo = '$prev_revNo' AND
							trn_sample_foil_consumption.strCombo = '".$rowFoil['combo']."' AND
							trn_sample_foil_consumption.strPrintName = '".$rowFoil['printName']."' AND
							trn_sample_foil_consumption.intTechniqueId = '".$rowFoil['techniqueId']."' AND
							trn_sample_foil_consumption.intColorId = '".$rowFoil['colourId']."' AND
							trn_sample_foil_consumption.intItem = '".$rowFoil['itemId']."' ";

				$sqlChkF_s	.=$sqlChkF.'/';
				$resultChkF 	= $db->RunQuery2($sqlChkF);
				$chkCopyStatus	=0;
				$coppied		=0;
				while($rowChkF = mysqli_fetch_array($resultChkF))
				{
					copy_print_width($sampleNoF,$sampleYearF,$prev_revNo,$revisionNo,$comboF,$printNameF,$technique,$colourId,$item);
					$arrCoppied[$chkCopyStatus]= $combo.'/'.$printName;
					$chkCopyStatus++;

					$sqlUpd	= " UPDATE trn_sample_foil_consumption
								SET
								dblWidth = '".val($rowChkF['dblWidth'])."' ,
								dbHeight = '".val($rowChkF['dbHeight'])."' ,
								dblFoilQty = '".val($rowChkF['dblFoilQty'])."' ,
								dblMeters = '".val($rowChkF['dblMeters'])."' ,
								strCuttingSide = '".$rowChkF['strCuttingSide']."' ,
								CYCLES = '".$rowChkF['CYCLES']."'

								WHERE
								intSampleNo = '".$rowFoil['sampleNo']."' AND
								intSampleYear = '".$rowFoil['sampleYear']."' AND
								intRevisionNo = '".$revisionNo."' AND
								strCombo = '".$rowFoil['combo']."' AND
								strPrintName = '".$rowFoil['printName']."' AND
								intTechniqueId = '".$rowFoil['techniqueId']."' AND
								intColorId = '".$rowFoil['colourId']."' AND
								intItem = '".$rowFoil['itemId']."' ";

					$resultUpd 	= $db->RunQuery2($sqlUpd);
					if(!$resultUpd && $savedStatus)
					{
						$savedStatus		= false;
						$err_msg		 	= $db->errormsg ;
						$errorSql			= $sqlUpd;
					}
					else
						$coppied++;
				}

			}
		}

	}

	////////////SAVE SPECIAL ITEM CONSUMPTIONS///////////////////////////
	foreach($arrSpecial as $rowSpecial)
	{
		$technique		= $rowSpecial['techniqueId'];
		$colourId		= $rowSpecial['colourId'];
		$item			= $rowSpecial['itemId'];
		$sampleNoS		= $rowSpecial['sampleNo'];
		$sampleYearS	= $rowSpecial['sampleYear'];
		$revisionNoS	= $rowSpecial['revisionNo'];
		$comboS			= $rowSpecial['combo'];
		$printNameS		= $rowSpecial['printName'];

		$sample_room_approved	= getSampleRoomApproval($sampleNoS,$sampleYearS,$revisionNoS,$comboS,$printNameS);
		$sp_tech_approved		= getSpeciaTechniquesApproval($sampleNoS,$sampleYearS,$revisionNoS,$comboS,$printNameS);

		if($sample_room_approved==1 && $sp_tech_approved !=1){

			$sqlDel2 = " DELETE
						FROM trn_sample_foil_consumption
						WHERE intSampleNo='$sampleNoS' and
						intSampleYear='$sampleYearS' and
						intRevisionNo='$revisionNoS' and
						strCombo='$comboS' and
						strPrintName='$printNameS' and
						intTechniqueId = '$technique' and
						intColorId = '$colourId' and
						intItem='$item' ";

			$resultDel2 = $db->RunQuery2($sqlDel2);

			$sqlDel = " DELETE
						FROM trn_sample_spitem_consumption
						WHERE intSampleNo='$sampleNoS' and
						intSampleYear='$sampleYearS' and
						intRevisionNo='$revisionNoS' and
						strCombo='$comboS' and
						strPrintName='$printNameS' and
						intTechniqueId = '$technique' and
						intColorId = '$colourId' and
						intItem='$item' ";

			$resultDel = $db->RunQuery2($sqlDel);
			
			if(!$resultDel && $savedStatus)
			{
				$savedStatus		= false;
				$err_msg		 	= $db->errormsg ;
				$errorSql			= $sqlDel;
			}

			$sqlIns = "INSERT INTO trn_sample_spitem_consumption
					(
					intSampleNo,
					intSampleYear,
					intRevisionNo,
					strCombo,
					strPrintName,
					intTechniqueId,
					intColorId,
					intItem,
					dblQty,
					intUser,
					dtDate
					)
					VALUES
					(
					'".$rowSpecial['sampleNo']."',
					'".$rowSpecial['sampleYear']."',
					'".$rowSpecial['revisionNo']."',
					'".$rowSpecial['combo']."',
					'".$rowSpecial['printName']."',
					'".$rowSpecial['techniqueId']."',
					'".$rowSpecial['colourId']."',
					'".$rowSpecial['itemId']."',
					'".val($rowSpecial['qty'])."',
					'$userId',
					now()
					)";

			$resultIns 	= $db->RunQuery2($sqlIns);
			if(!$resultIns && $savedStatus)
			{
				$savedStatus		= false;
				$err_msg		 	= $db->errormsg ;
				$errorSql			= $sqlIns;
			}

			if($type_copy=='copy' && $sample_room_approved==1 && $sp_tech_approved !=1)
			{
				$sqlChkS = "SELECT *
							FROM
							trn_sample_spitem_consumption 
							INNER JOIN mst_item ON trn_sample_spitem_consumption.intItem = mst_item.intId
							INNER JOIN mst_techniques ON trn_sample_spitem_consumption.intTechniqueId = mst_techniques.intId
							left JOIN trn_sampleinfomations_combo_print_approvedby
							ON trn_sample_spitem_consumption.intSampleNo = trn_sampleinfomations_combo_print_approvedby.SAMPLE_NO
							AND trn_sample_spitem_consumption.intSampleYear = trn_sampleinfomations_combo_print_approvedby.SAMPLE_YEAR
							AND trn_sample_spitem_consumption.intRevisionNo = trn_sampleinfomations_combo_print_approvedby.REVISION
							AND trn_sample_spitem_consumption.strCombo = trn_sampleinfomations_combo_print_approvedby.COMBO
							AND trn_sample_spitem_consumption.strPrintName = trn_sampleinfomations_combo_print_approvedby.PRINT
							INNER JOIN trn_sampleinfomations ON trn_sample_spitem_consumption.intSampleNo = trn_sampleinfomations.intSampleNo
							AND trn_sample_spitem_consumption.intSampleYear = trn_sampleinfomations.intSampleYear
							AND trn_sample_spitem_consumption.intRevisionNo = trn_sampleinfomations.intRevisionNo
							LEFT JOIN trn_sample_special_technical_header ON trn_sample_spitem_consumption.intSampleNo = trn_sample_special_technical_header.SAMPLE_NO AND trn_sample_spitem_consumption.intSampleYear = trn_sample_special_technical_header.SAMPLE_YEAR AND trn_sample_spitem_consumption.intRevisionNo = trn_sample_special_technical_header.REVISION_NO AND trn_sample_spitem_consumption.strCombo = trn_sample_special_technical_header.COMBO AND trn_sample_spitem_consumption.strPrintName = trn_sample_special_technical_header.PRINT
							WHERE
							mst_techniques.CHANGE_CONPC_AFTER_SAMPLE = 1 AND
							/*mst_techniques.intRoll <> 1 AND*/
							mst_item.ROLL_FORM <>  '1' AND 
					    	mst_item.intUOM <> 6 AND 
							(trn_sampleinfomations.intSampleRoomStatus = 1 OR trn_sampleinfomations_combo_print_approvedby.`STATUS` = 1) /*AND
	(trn_sample_special_technical_header.`STATUS` <> 1 or trn_sample_special_technical_header.`STATUS` IS NULL)*/ AND
							trn_sample_spitem_consumption.intSampleNo = '".$rowSpecial['sampleNo']."' AND
							trn_sample_spitem_consumption.intSampleYear = '".$rowSpecial['sampleYear']."' AND
							trn_sample_spitem_consumption.intRevisionNo = '$prev_revNo' AND
							trn_sample_spitem_consumption.strCombo = '".$rowSpecial['combo']."' AND
							trn_sample_spitem_consumption.strPrintName = '".$rowSpecial['printName']."' AND
							trn_sample_spitem_consumption.intTechniqueId = '".$rowSpecial['techniqueId']."' AND
							trn_sample_spitem_consumption.intItem = '".$rowSpecial['itemId']."' ";
				$sqlChkF_s	.=$sqlChkS.'/';
				$resultChkS 	= $db->RunQuery2($sqlChkS);
				while($rowChkS = mysqli_fetch_array($resultChkS))
				{

					$arrCoppied[$chkCopyStatus]= $combo.'/'.$printName;
					$chkCopyStatus++;

					$sqlUpd	= "UPDATE trn_sample_spitem_consumption
								SET
								dblQty = '".val($rowChkS['dblQty'])."'
								WHERE
								intSampleNo = '".$rowSpecial['sampleNo']."' AND
								intSampleYear = '".$rowSpecial['sampleYear']."' AND
								intRevisionNo = '".$revisionNo."' AND
								strCombo = '".$rowSpecial['combo']."' AND
								strPrintName = '".$rowSpecial['printName']."' AND
								intTechniqueId = '".$rowSpecial['techniqueId']."' AND
								intColorId = '".$rowSpecial['colourId']."' AND
								intItem = '".$rowSpecial['itemId']."' ";

					$resultUpd 	= $db->RunQuery2($sqlUpd);
					if(!$resultUpd && $savedStatus)
					{
						$savedStatus		= false;
						$err_msg		 	= $db->errormsg ;
						$errorSql			= $sqlUpd;
					}
					else
						$coppied++;
				}
			}
		}
	}
	/////////////////////////////////////////

	//	$bomSave=saveToBOM($sampleNo,$sampleYear,$revNo,$combo,$printName,$_SESSION["userId"]);//commented on 26/04/2013
	//$savedStatus=false;
	if($type_copy=='copy')
	{
		if($chkCopyStatus != $coppied || $coppied==0)
		{
			$savedStatus	= false;
			$arrCoppied_unique	=array_unique($arrCoppied);
			$err_msg		= "Cant Copy.Items are not tally with previous revision.".'!'.implode("','",$arrCoppied_unique).' && '.$coppied;
			$response['sqlChkF_s'] = $sqlChkF_s;
		}
	}
	if($savedStatus)
	{
		$sql_upd_SampleInformation = "UPDATE `trn_sampleinfomations` SET `intSpecialTechniques`='2' WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevisionNo`='$revisionNo')";
		$result_SampleInformation_update = $db->RunQuery2($sql_upd_SampleInformation);

		if(!$result_SampleInformation_update && $savedStatus)
		{
			$savedStatus		= false;
			$err_msg		 	= "Updated Fail in trn_sampleinfomations".$errorSql;
			$errorSql			= $sql_upd_SampleInformation;
		}
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= 'pass';

		if($type_copy=='copy')
			$response['msg'] 	= 'Copied successfully.' . $coppied_string;
		else
			$response['msg'] 	= 'Saved successfully.';
	}
	else
	{
		$db->rollback();
		$response['type'] 		= 'fail';
		$response['msg'] 		= $err_msg;
		$response['sql'] 		= $errorSql;
	}

	echo json_encode($response);

}
else if($requestType=='approve')
{
	$savedStatus	= true;
	$errorMsg		= '';
	$errorSql		= '';

	$sampleNo 		= $_REQUEST['sampleNo'];
	$sampleYear 	= $_REQUEST['sampleYear'];
	$revNo 			= $_REQUEST['revNo'];
	$userSelected   = json_decode($_REQUEST['userSelectedArr']);

	$db->begin();

	for($i = 0;$i<sizeof($userSelected);$i++){
		$combo     =  $userSelected[$i]->Main_Key;  //The combo of the Graphic
		$printName =  $userSelected[$i]->Sub_Key;  //The print of the combo
		$value     =  $userSelected[$i]->value;    //The statues of combo and print approved or not

		if($value == 1){
			
			//check for consumsmption > 0
			//checkConsumpition($sampleNo,$sampleYear,$revNo,$combo,$printName);
			$err_flag	= checkConsumpition($sampleNo,$sampleYear,$revNo,$combo,$printName);
			if($err_flag && $savedStatus){
				$savedStatus		= false;
				$err_msg            = "Consumption Value 0 of ".$combo."/".$printName;
			}

			$sql_SpecialTenical_update = "UPDATE `trn_sample_special_technical_header` SET STATUS=STATUS-1 WHERE (`SAMPLE_NO`='$sampleNo') AND (`SAMPLE_YEAR`='$sampleYear') AND (`REVISION_NO`='$revNo') AND (`COMBO`='$combo') AND (`PRINT`='$printName')";
			$result_spetecnical = $db->RunQuery2($sql_SpecialTenical_update);

			if(!$result_spetecnical && $savedStatus)
			{
				$savedStatus		= false;
				$err_msg		 	= $db->errormsg;
				$errorSql			= $sql_SpecialTenical_update;
			}

			$sqlf	="select * from trn_sample_special_technical_header WHERE (`SAMPLE_NO`='$sampleNo') AND (`SAMPLE_YEAR`='$sampleYear') AND (`REVISION_NO`='$revNo') AND (`COMBO`='$combo') AND (`PRINT`='$printName')";
			$resultsf = $db->RunQuery2($sqlf);


			$row = mysqli_fetch_array($resultsf);
			$status = $row['STATUS'];
			$savedLevels = 2;
			$approveLevel=$savedLevels+1-$status;

			$sql = "INSERT INTO `trn_sample_special_technical_header_approved_by` (`SAMPLE_NO`, `SAMPLE_YEAR`, `REVISION_NO`, `COMBO`, `PRINT`, `APPROVE_LEVEL`, `STATUS`, `APPROVED_BY`, `APPROVED_DATE`) VALUES ('$sampleNo', '$sampleYear', '$revNo', '$combo', '$printName', '$approveLevel', '0', '$userId', now())";
			$result = $db->RunQuery2($sql);

			if(!$result && $savedStatus)
			{
				$savedStatus		= false;
				$err_msg		 	= $db->errormsg;
				$errorSql			= $sql;
			}

			//check for consumsmption > 0
			//checkConsumpition($sampleNo,$sampleYear,$revNo,$combo,$printName);
			$err_flag	= checkConsumpition($sampleNo,$sampleYear,$revNo,$combo,$printName);
			if($err_flag && $savedStatus){
				$savedStatus		= false;
				$err_msg            = "Consumption Value 0 of ".$combo."/".$printName;
			}
		}
	}

	$sql_Special_RM_Count = "SELECT COUNT(ComCount) AS ComPrintCount FROM ( SELECT COUNT(strComboName) AS ComCount
	FROM trn_sampleinfomations_details
	 INNER JOIN mst_techniques ON trn_sampleinfomations_details.intTechniqueId = mst_techniques.intId
	WHERE
	trn_sampleinfomations_details.intSampleNo = '$sampleNo' AND
	trn_sampleinfomations_details.intSampleYear = '$sampleYear' AND
	trn_sampleinfomations_details.intRevNo = '$revNo' AND
	mst_techniques.CHANGE_CONPC_AFTER_SAMPLE = '1'
	GROUP BY intRevNo,strComboName,strPrintName
	 ) AS TB1";
	$resultSQLComCount = $db->RunQuery2($sql_Special_RM_Count);
	if(!$resultSQLComCount && $savedStatus)
	{
		$savedStatus		= false;
		$err_msg		 	= $db->errormsg;
		$errorSql			= $sql_Special_RM_Count;
	}

	$rowSampleInfo = mysqli_fetch_array($resultSQLComCount);
	$Sample_ComPrintCount = $rowSampleInfo['ComPrintCount'];
	$Sample_ComPrintCount;

	$sql_app = "SELECT COUNT(ComCount) AS ComPrintCountApprove FROM (SELECT count(COMBO) AS ComCount FROM trn_sample_special_technical_header
		WHERE SAMPLE_NO = '$sampleNo' AND SAMPLE_YEAR = '$sampleYear' AND REVISION_NO= '$revNo' AND STATUS = 1 GROUP BY REVISION_NO,COMBO, PRINT) AS TB1";
	$result_ApprovedBy = $db->RunQuery2($sql_app);
	$row_Approve = mysqli_fetch_array($result_ApprovedBy);
	if(!$result_ApprovedBy && $savedStatus)
	{
		$savedStatus		= false;
		$err_msg		 	= $db->errormsg;
		$errorSql			= $sql_app;
	}

	$Approve_ComPrintCount = $row_Approve['ComPrintCountApprove'];
	$Approve_ComPrintCount;

	if($Sample_ComPrintCount == $Approve_ComPrintCount){
		$sql_app = "UPDATE `trn_sampleinfomations` SET `intSpecialTechniques`='1' WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevisionNo`='$revNo')";
		$result_ApprovedBy = $db->RunQuery2($sql_app);
		if(!$result_ApprovedBy && $savedStatus)
		{
			$savedStatus		= false;
			$err_msg		 	= $db->errormsg;
			$errorSql			= $sql_app;
		}

	}
	if(($savedStatus)){
		$db->commit();
		$response['type'] 		= 'pass';
		$response['msg'] 		= 'Approved successfully.';
	}
	else{
		$db->rollback();
		$response['type'] 		= 'fail';
		$response['msg'] 		= $err_msg;
		$response['q'] 			= $errorSql;
	}

	echo json_encode($response);

}
else if($requestType=='reject')
{
	$sampleNo 		= $_REQUEST['sampleNo'];
	$sampleYear 	= $_REQUEST['sampleYear'];
	$revNo 			= $_REQUEST['revNo'];

	$errorMsg		= '';
	$errorSql		= '';

	$db->begin();

	$userSelected   = json_decode($_REQUEST['userSelectedArr']);
	//print_r($userSelected);
	for($i = 0;$i<sizeof($userSelected);$i++){
		$combo     =  $userSelected[$i]->Main_Key;  //The combo of the Graphic
		$printName =  $userSelected[$i]->Sub_Key;  //The print of the combo
		$value     =  $userSelected[$i]->value;    //The statues of combo and print approved or not

		if($value == 1){
			$sql = "UPDATE `trn_sample_special_technical_header` SET STATUS=0 WHERE (`SAMPLE_NO`='$sampleNo') AND (`SAMPLE_YEAR`='$sampleYear') AND (`REVISION_NO`='$revNo') AND (`COMBO`='$combo') AND (`PRINT`='$printName')";
			$result = $db->RunQuery2($sql);

			$sqlf	="select max(STATUS) as max from trn_sample_special_technical_header_approved_by WHERE (`SAMPLE_NO`='$sampleNo') AND (`SAMPLE_YEAR`='$sampleYear') AND (`REVISION_NO`='$revNo') AND (`COMBO`='$combo') AND (`PRINT`='$printName')";
			$resultsf = $db->RunQuery2($sqlf);
			$row = mysqli_fetch_array($resultsf);
			$max = $row['max']+1;

			$sql = "UPDATE `trn_sample_special_technical_header_approved_by` SET STATUS='$max' WHERE (`SAMPLE_NO`='$sampleNo') AND (`SAMPLE_YEAR`='$sampleYear') AND (`REVISION_NO`='$revNo') AND (`COMBO`='$combo') AND (`PRINT`='$printName')  AND (`STATUS`= 0 )";
			$result = $db->RunQuery2($sql);

			$sql = "INSERT INTO `trn_sample_special_technical_header_approved_by` (`SAMPLE_NO`, `SAMPLE_YEAR`, `REVISION_NO`, `COMBO`, `PRINT`, `APPROVE_LEVEL`, `STATUS`, `APPROVED_BY`, `APPROVED_DATE`) VALUES ('$sampleNo', '$sampleYear', '$revNo', '$combo', '$printName', '0', '0', '$userId', now())";
			$result = $db->RunQuery2($sql);

		}
	}

	$sql_Special_RM_Count = "SELECT COUNT(ComCount) AS ComPrintCount FROM ( SELECT COUNT(strComboName) AS ComCount
	FROM trn_sampleinfomations_details
	 INNER JOIN mst_techniques ON trn_sampleinfomations_details.intTechniqueId = mst_techniques.intId
	WHERE
	trn_sampleinfomations_details.intSampleNo = '$sampleNo' AND
	trn_sampleinfomations_details.intSampleYear = '$sampleYear' AND
	trn_sampleinfomations_details.intRevNo = '$revNo' AND
	mst_techniques.CHANGE_CONPC_AFTER_SAMPLE = '1'
	GROUP BY intRevNo,strComboName,strPrintName
	 ) AS TB1";
	$resultSQLComCount = $db->RunQuery2($sql_Special_RM_Count);
	$rowSampleInfo = mysqli_fetch_array($resultSQLComCount);
	$Sample_ComPrintCount = $rowSampleInfo['ComPrintCount'];
	$Sample_ComPrintCount;

	$sql_app = "SELECT COUNT(ComCount) AS ComPrintCountApprove FROM (SELECT count(COMBO) AS ComCount FROM trn_sample_special_technical_header
		WHERE SAMPLE_NO = '$sampleNo' AND SAMPLE_YEAR = '$sampleYear' AND REVISION_NO= '$revNo' AND STATUS = 0 GROUP BY REVISION_NO,COMBO, PRINT) AS TB1";
	$result_ApprovedBy = $db->RunQuery2($sql_app);
	$row_Approve = mysqli_fetch_array($result_ApprovedBy);
	$Approve_ComPrintCount = $row_Approve['ComPrintCountApprove'];
	$Approve_ComPrintCount;

	if($Sample_ComPrintCount == $Approve_ComPrintCount){
		$sql_app = "UPDATE `trn_sampleinfomations` SET `intSpecialTechniques`='0' WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sampleYear') AND (`intRevisionNo`='$revNo')";
		$result_ApprovedBy = $db->RunQuery2($sql_app);
	}

	if(($result)){
		$db->commit();
		$response['type'] 		= 'pass';
		$response['msg'] 		= 'Rejected successfully.';
	}
	else{
		$db->rollback();
		$response['type'] 		= 'fail';
		$response['msg'] 		= $db->errormsg;
		$response['q'] 			= $sql;
	}

	echo json_encode($response);


}
else if($requestType=='saveConsumption') {
	//Get Values
	$sampleNo    = $_REQUEST['sampleNo'];
	$sample_year = $_REQUEST['sample_year'];
	$revisionNo  = $_REQUEST['revisionNo'];
	$comboName   = $_REQUEST['comboName'];
	$PrintName   = $_REQUEST['PrintName'];
	$foilItem    = $_REQUEST['foilItem'];
	$technique   = $_REQUEST['technique'];
	$color	   	 = $_REQUEST['color'];
	$cycles		 = $_REQUEST['cycles'];
	$arr 		 = json_decode($_REQUEST['arr'], true);
	$db->begin();

	$sql_D = "DELETE FROM trn_sample_specialtechniques_print_width_height
			WHERE
				intSampleNo = '$sampleNo'
			AND intSampleYear = '$sample_year'
			AND intRevisionNo = '$revisionNo'
			AND strComboName = '$comboName'
			AND strPrintName = '$PrintName'
			AND TECHNIQUE = '$technique'
			AND COLOUR 		= '$color'
			AND intItem = '$foilItem'";
	$result = $db->RunQuery2($sql_D);


	foreach($arr as $arrVal)
	{
		$serialNo      = $arrVal['serialNo'];
		$width 	       = $arrVal['width'];
		$height 	   = $arrVal['height'];
		$consum 	   = $arrVal['consum']/$arrVal['cycles'];

		    //Delete previouse Record
			$savedStatus	= true;
			$sql = "SELECT MAX(intSerialNo) AS maxSerialNo FROM trn_sample_specialtechniques_print_width_height WHERE intSampleNo = '$sampleNo' AND intSampleYear = '$sample_year' AND intRevisionNo = '$revisionNo' AND strComboName = '$comboName' AND strPrintName = '$PrintName' AND TECHNIQUE = '$technique' AND COLOUR = '$color' AND intItem = '$foilItem' ";
			$result = $db->RunQuery2($sql);
			$row_Max = mysqli_fetch_array($result);
			$maxSerialNo = $row_Max['maxSerialNo'];
			$serialNoNew = $maxSerialNo + 1;

			//$serialNoNew = $serialNo+1
			$sql = "INSERT INTO `trn_sample_specialtechniques_print_width_height` (   `intSampleNo`, `intSampleYear`, `intRevisionNo`, `strComboName`, `strPrintName`,`TECHNIQUE`,`COLOUR`,  `intItem`, `intSerialNo`, `intPrintWidth`, `intPrintHeight`, `intConsumption`) VALUES ('$sampleNo',     '$sample_year', '$revisionNo', '$comboName', '$PrintName', '$technique','$color','$foilItem', '$serialNoNew', '$width', '$height', '$consum')";
			$result = $db->RunQuery2($sql);
			if(!$result && $savedStatus)
			{
				$savedStatus		= false;
				$err_msg		 	= $db->errormsg;
				$errorSql			= $sql;
			}
			$serialNoNew++;
	}

	$sql_t_c = "SELECT
			-- SUM(intConsumption) AS tot_consumption
IF(mst_item.intUOM=9 OR mst_item.intUOM=10,(SUM(intConsumption)/(mst_item.foil_width)),SUM(intConsumption)) AS tot_consumption
		FROM
			trn_sample_specialtechniques_print_width_height
			INNER JOIN mst_item ON trn_sample_specialtechniques_print_width_height.intItem = mst_item.intId
		WHERE
			intSampleNo = '$sampleNo'
		AND intSampleYear = '$sample_year'
		AND intRevisionNo = '$revisionNo'
		AND strComboName = '$comboName'
		AND strPrintName = '$PrintName'
		AND TECHNIQUE = '$technique'
		AND COLOUR = '$color'
		AND intItem = '$foilItem'";

    $result_con = $db->RunQuery2($sql_t_c);
	$row_Con = mysqli_fetch_array($result_con);
	$consumption = $row_Con['tot_consumption'];

	if($savedStatus) {
		$db->commit();
		$response['type'] = 'pass';
		$response['msg'] = 'Consumptions Saved successfully.';
		$response['con'] = $consumption;
	}
	else{
		$db->rollback();
		$response['type'] 		= 'fail';
		$response['msg'] 		= "Error";
		$response['q'] 			=  $sql;
	}
	echo json_encode($response);
}
else if($requestType=='DeleteSerialConsumption'){
	$serialNum_deleted    = $_REQUEST['serialNum_deleted'];
	$sampleNo = $_REQUEST['sampleNo'];
	$sample_year = $_REQUEST['sample_year'];
	$revisionNo  = $_REQUEST['revisionNo'];
	$comboName   = $_REQUEST['comboName'];
	$PrintName   = $_REQUEST['PrintName'];
	$foilItem    = $_REQUEST['foilItem'];
	$technique   = $_REQUEST['technique'];
	$color   	 = $_REQUEST['color'];
	$db->begin();
	$sql = "DELETE FROM `trn_sample_specialtechniques_print_width_height` WHERE (`intSampleNo`='$sampleNo') AND (`intSampleYear`='$sample_year') AND (`intRevisionNo`='$revisionNo') AND (`strComboName`='$comboName') AND (`strPrintName`='$PrintName') AND (`TECHNIQUE`='$technique') AND (`COLOUR`='$color') AND (`intItem`='$foilItem') AND (`intSerialNo`='$serialNum_deleted')";
	$result = $db->RunQuery2($sql);

	$sql_t_c = "SELECT
			SUM(intConsumption) AS tot_consumption
		FROM
			trn_sample_specialtechniques_print_width_height
		WHERE
			intSampleNo = '$sampleNo'
		AND intSampleYear = '$sample_year'
		AND intRevisionNo = '$revisionNo'
		AND strComboName = '$comboName'
		AND strPrintName = '$PrintName'
		AND TECHNIQUE = '$technique'
		AND intItem = '$foilItem'";

    $result_con = $db->RunQuery2($sql_t_c);
	$row_Con = mysqli_fetch_array($result_con);
	$consumption = $row_Con['tot_consumption'];

	if($result) {
		$db->commit();
		$response['type'] = 'pass';
		$response['msg']  = 'Consumptions Deleted successfully.';
		$response['con']  = $consumption;
	}
	else{
		$db->rollback();
		$response['type'] 		= 'fail';
		$response['msg'] 		= $db->errormsg;
		$response['q'] 			= $sql;
	}
	echo json_encode($response);
}
//---------------------------------------
function confirmedOrders($sampleNo,$sampleYear,$revNo,$combo,$printName){
	global $db;
	$sql  	= " SELECT
					trn_orderheader.intOrderNo,
					trn_orderheader.intOrderYear
					FROM
					trn_orderheader
					Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					WHERE
					trn_orderdetails.intSampleNo =  '$sampleNo' AND
					trn_orderdetails.intSampleYear =  '$sampleYear' AND
					trn_orderheader.intApproveLevelStart >=	trn_orderheader.intStatus AND
					trn_orderheader.intStatus > 0 AND
					trn_orderdetails.strCombo =  '$combo' AND
					trn_orderdetails.strPrintName =  '$printName' AND
					trn_orderdetails.intRevisionNo =  '$revNo'
					";
	$result	= $db->RunQuery2($sql);
	if(mysqli_num_rows($result)>0)
		return true;
	else
		return false;
}

function saveToBOM($sampleNo,$sampleYear,$revNo,$combo,$printName,$user){

	global $db;

	$sql = "delete from trn_sample_item_consumption where intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName'";
	$result = $db->RunQuery2($sql);


	$sql="select
t.type,
t.intSampleNo,
t.intSampleYear,
t.intRevisionNo ,
t.strCombo ,
t.strPrintName ,
 t.intId ,
 sum(consum) as consum

 from ((
SELECT
1 as type,
sc.intSampleNo,
sc.intSampleYear,
sc.intRevisionNo,
sc.strCombo,
sc.strPrintName,
 mst_item.intId,
 sum(round(dblColorWeight/((SELECT
Sum(trn_sample_color_recipes.dblWeight)
FROM trn_sample_color_recipes
WHERE
trn_sample_color_recipes.intSampleNo = sc.intSampleNo AND
trn_sample_color_recipes.intSampleYear = sc.intSampleYear AND
trn_sample_color_recipes.intRevisionNo =  sc.intRevisionNo AND
trn_sample_color_recipes.strCombo =  sc.strCombo AND
trn_sample_color_recipes.strPrintName =  sc.strPrintName AND
trn_sample_color_recipes.intTechniqueId =  sc.intTechniqueId AND
trn_sample_color_recipes.intInkTypeId =  trn_sampleinfomations_details_technical.intInkTypeId AND
trn_sample_color_recipes.intColorId =  trn_sampleinfomations_details_technical.intColorId
 ))*dblWeight /dblNoOfPcs,6))as consum
FROM
trn_sample_color_recipes as sc
Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = sc.intSampleNo
AND trn_sampleinfomations_details_technical.intSampleYear = sc.intSampleYear
AND trn_sampleinfomations_details_technical.intRevNo = sc.intRevisionNo
AND trn_sampleinfomations_details_technical.strComboName = sc.strCombo
AND trn_sampleinfomations_details_technical.strPrintName = sc.strPrintName
AND trn_sampleinfomations_details_technical.intColorId = sc.intColorId
AND trn_sampleinfomations_details_technical.intInkTypeId = sc.intInkTypeId
Inner Join mst_item ON mst_item.intId = sc.intItem
Inner Join mst_units ON mst_item.intUOM = mst_units.intId
group by
sc.intSampleNo,
sc.intSampleYear,
sc.intRevisionNo,
sc.strCombo,
sc.strPrintName ,
 mst_item.intId
)
UNION
(
SELECT
		2 as type,
trn_sample_spitem_consumption.intSampleNo,
trn_sample_spitem_consumption.intSampleYear,
trn_sample_spitem_consumption.intRevisionNo,
trn_sample_spitem_consumption.strCombo,
trn_sample_spitem_consumption.strPrintName,
 mst_item.intId,
       sum(trn_sample_spitem_consumption.dblQty/dblNoOfPcs) as consum
       FROM
	trn_sample_spitem_consumption
        Inner Join mst_item ON mst_item.intId = trn_sample_spitem_consumption.intItem
	Inner Join mst_units ON mst_units.intId = mst_item.intUOM
group by
trn_sample_spitem_consumption.intSampleNo,
trn_sample_spitem_consumption.intSampleYear,
trn_sample_spitem_consumption.intRevisionNo,
trn_sample_spitem_consumption.strCombo,
trn_sample_spitem_consumption.strPrintName ,
 mst_item.intId
)

UNION
(
SELECT
		3 as type,
trn_sample_foil_consumption.intSampleNo,
trn_sample_foil_consumption.intSampleYear,
trn_sample_foil_consumption.intRevisionNo,
trn_sample_foil_consumption.strCombo,
trn_sample_foil_consumption.strPrintName,
 mst_item.intId,
		sum(trn_sample_foil_consumption.dblMeters) as consum
	FROM
		trn_sample_foil_consumption
		Inner Join mst_item ON mst_item.intId = trn_sample_foil_consumption.intItem
		Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
		Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
		Inner Join mst_units ON mst_units.intId = mst_item.intUOM
group by
trn_sample_foil_consumption.intSampleNo,
trn_sample_foil_consumption.intSampleYear,
trn_sample_foil_consumption.intRevisionNo,
trn_sample_foil_consumption.strCombo,
trn_sample_foil_consumption.strPrintName ,
 mst_item.intId
)) as t
WHERE
t.intSampleNo='$sampleNo' AND
t.intSampleYear='$sampleYear' AND
t.intRevisionNo='$revNo' AND
t.strCombo='$combo' AND
t.strPrintName='$printName'
GROUP BY
t.type,
t.intSampleNo,
t.intSampleYear,
t.intRevisionNo ,
t.strCombo ,
t.strPrintName,
 t.intId
 ";

	$result = $db->RunQuery2($sql);

	$toSave=0;
	$saved=0;
	while($row=mysqli_fetch_array($result))
	{
		$toSave++;
		$itemId=$row['intId'];
		$qty=$row['consum'];
		$type=$row['type'];


		$sqlI = "INSERT INTO `trn_sample_item_consumption` 			(`intSampleNo`,`intSampleYear`,`intRevisionNo`,`strCombo`,`strPrintName`,`intItem`,`dblQty`,`intType`,`intUser`,`dtDate`)
			VALUES ('".$sampleNo."','".$sampleYear."','".$revNo."','".$combo."','".$printName."','".$itemId."','".val($qty)."','".$type."','".$user."',now())";
		$resultI 	= $db->RunQuery2($sqlI);
		$saved+=$resultI;
	}

	if($toSave==$saved)
		return 1;
	else
		return 0;
}

function validateBeforeSave($sampleNo,$sampleYear,$revisionNo,$combo,$printName,$userId,$programCode)
{
	global $db;
	global $obj_errorHandeling;

	$sql = "SELECT 	STATUS,
				LEVELS
				FROM
				trn_sample_special_technical_header
				WHERE
				SAMPLE_NO = '$sampleNo' AND
				SAMPLE_YEAR = '$sampleYear' AND
				REVISION_NO = '$revisionNo' AND
				COMBO = '$combo' AND
				PRINT = '$printName' ";

	$result 	= $db->RunQuery2($sql);
	$row		= mysqli_fetch_array($result);

	$status		= $row['STATUS'];
	$levels		= $row['LEVELS'];

	$response 	= $obj_errorHandeling->get_permision_withApproval_save($status,$levels,$userId,$programCode,'RunQuery2');

	return $response;
}

function checkConsumpition($sampleNo,$sampleYear,$revNo,$combo,$printName){

	global $db;
	global $userId;
	
		  $sql		= "SELECT
	`trn_sample_special_technical_header` .`STATUS`
	FROM trn_sample_special_technical_header
	WHERE
	`trn_sample_special_technical_header` .SAMPLE_NO = '$sampleNo' AND
	`trn_sample_special_technical_header` .SAMPLE_YEAR = '$sampleYear' AND
	`trn_sample_special_technical_header` .REVISION_NO = '$revNo' AND
	`trn_sample_special_technical_header` .COMBO = '$combo' AND
	`trn_sample_special_technical_header` .PRINT = '$printName'
	";
	
	
    $result 	= $db->RunQuery2($sql);
    $row		= mysqli_fetch_array($result);
    $status		= $row['STATUS'];

    if($status==1){
        $bulk_order_flag =1;
    }
    else
        $bulk_order_flag= 0;

	
	
	
	
    $foil_consumption_count = 0;
    $item_consumption_count = 0;
	$sql_foil = "SELECT *
	FROM trn_sample_foil_consumption
	INNER JOIN mst_techniques ON trn_sample_foil_consumption.intTechniqueId = mst_techniques.intId
						INNER Join mst_item ON trn_sample_foil_consumption.intItem = mst_item.intId 
												INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
  WHERE trn_sample_foil_consumption.intSampleNo = '$sampleNo' AND
	trn_sample_foil_consumption.intSampleYear = '$sampleYear' AND
	trn_sample_foil_consumption.intRevisionNo = '$revNo' AND
    trn_sample_foil_consumption.strCombo = '$combo' AND
    trn_sample_foil_consumption.strPrintName = '$printName' AND  ";
	if($bulk_order_flag!=1){
	$sql_foil .= " mst_item.ROLL_FORM =  '1' AND 
	mst_units.intId <> 6 AND "; 
	}
	$sql_foil .= "
	mst_techniques.CHANGE_CONPC_AFTER_SAMPLE = 1 AND
	(trn_sample_foil_consumption.dblMeters <= 0 or trn_sample_foil_consumption.dblMeters IS NULL)";
	
		if($userId==2)
		//echo $bulk_order_flag;


    $result_foil 	= $db->RunQuery2($sql_foil);

    if(mysqli_num_rows($result_foil)>0){
        while($rowFoilData =  mysqli_fetch_array($result_foil)){
            $colorReceipt = getRecieptAvailable($sampleNo,$sampleYear,$revNo,$combo,$printName,$rowFoilData['intColorId'],$rowFoilData['intTechniqueId'],$rowFoilData['intItem']);
            if($colorReceipt === 1){
                $foil_consumption_count = 1;
            }
        }

    }

	$sql_item = "SELECT *
    FROM trn_sample_spitem_consumption
    INNER JOIN mst_techniques ON trn_sample_spitem_consumption.intTechniqueId = mst_techniques.intId
    WHERE
	  trn_sample_spitem_consumption.intSampleNo = '$sampleNo'
    AND trn_sample_spitem_consumption.intSampleYear = '$sampleYear'
    AND trn_sample_spitem_consumption.intRevisionNo = '$revNo'
	AND trn_sample_spitem_consumption.strCombo = '$combo'
	AND trn_sample_spitem_consumption.strPrintName = '$printName'
	AND mst_techniques.CHANGE_CONPC_AFTER_SAMPLE = 1 AND
	(trn_sample_spitem_consumption.dblQty <= 0 OR trn_sample_spitem_consumption.dblQty IS NULL)";

	$result_item	= $db->RunQuery2($sql_item);

    if(mysqli_num_rows($result_item)>0){
        while($rowFoilData =  mysqli_fetch_array($result_item)){
            $colorReceipt = getRecieptAvailable($sampleNo,$sampleYear,$revNo,$combo,$printName,$rowFoilData['intColorId'],$rowFoilData['intTechniqueId'],$rowFoilData['intItem']);
            if($colorReceipt === 1){
                $item_consumption_count = 1;
            }
        }

    }

	if($foil_consumption_count > 0 || $item_consumption_count > 0){
		return 1;//error
	}
	else{
		return 0;//ok
	}
}

function getSampleRoomApproval($sampleNo,$sampleYear,$revisionNo,$combo,$printName){

	global $db;

	$sql	=" SELECT
					trn_sampleinfomations.intSampleRoomStatus
					FROM
					trn_sampleinfomations
					WHERE
					trn_sampleinfomations.intSampleNo = '$sampleNo' AND
					trn_sampleinfomations.intSampleYear = '$sampleYear' AND
					trn_sampleinfomations.intRevisionNo = '$revisionNo'
					";

	$result 	= $db->RunQuery2($sql);
	$row		= mysqli_fetch_array($result);
	$status		= $row['STATUS'];

	$sql	=" SELECT
					trn_sampleinfomations_combo_print_approvedby.`STATUS`
					FROM
					trn_sampleinfomations_combo_print_approvedby
					WHERE
					trn_sampleinfomations_combo_print_approvedby.SAMPLE_NO = '$sampleNo' AND
					trn_sampleinfomations_combo_print_approvedby.SAMPLE_YEAR = '$sampleYear' AND
					trn_sampleinfomations_combo_print_approvedby.REVISION = '$revisionNo' AND
					trn_sampleinfomations_combo_print_approvedby.COMBO = '$combo' AND
					trn_sampleinfomations_combo_print_approvedby.PRINT = '$printName'
					";

	$result 	= $db->RunQuery2($sql);
	$row		= mysqli_fetch_array($result);
	$status_cp		= $row['STATUS'];

	if($status==1 || $status_cp==1){
		return true;
	}
	else
		return false;

}
function getSpeciaTechniquesApproval($sampleNo,$sampleYear,$revisionNo,$combo,$printName){

	global $db;
	$sql = "SELECT 	STATUS,
				LEVELS
				FROM
				trn_sample_special_technical_header
				WHERE
				SAMPLE_NO = '$sampleNo' AND
				SAMPLE_YEAR = '$sampleYear' AND
				REVISION_NO = '$revisionNo' AND
				COMBO = '$combo' AND
				PRINT = '$printName' ";

	$result 	= $db->RunQuery2($sql);
	$row		= mysqli_fetch_array($result);

	return $status		= $row['STATUS'];
}
function checkWidthHeight($sampleNoF,$sampleYearF,$revisionNoF,$comboF,$printNameF,$technique,$colorId,$item){

	global $db;
	$sql		= "SELECT
					trn_sample_specialtechniques_print_width_height.intPrintWidth,
					trn_sample_specialtechniques_print_width_height.intPrintHeight
					FROM `trn_sample_specialtechniques_print_width_height`
					WHERE
					trn_sample_specialtechniques_print_width_height.intSampleNo = '$sampleNoF' AND
					trn_sample_specialtechniques_print_width_height.intSampleYear = '$sampleYearF' AND
					trn_sample_specialtechniques_print_width_height.intRevisionNo = '$revisionNoF' AND
					trn_sample_specialtechniques_print_width_height.strComboName = '$comboF' AND
					trn_sample_specialtechniques_print_width_height.strPrintName = '$printNameF' AND
					trn_sample_specialtechniques_print_width_height.TECHNIQUE = '$technique' AND
					trn_sample_specialtechniques_print_width_height.COLOUR = '$colorId' AND
					trn_sample_specialtechniques_print_width_height.intItem = '$item'
					";
	$result 	= $db->RunQuery2($sql);
	$row		= mysqli_fetch_array($result);
	if($row['intPrintWidth'])
		return 1;
	else
		return 0;
}

function copy_print_width($sampleNoF,$sampleYearF,$prev_revNo,$revisionNo,$comboF,$printNameF,$technique,$colourId,$item){

	global $db;
	$sql_S = "SELECT TB1.* FROM
trn_sample_specialtechniques_print_width_height AS TB1
INNER JOIN
trn_sample_specialtechniques_print_width_height AS TB2
ON
    TB1.intSampleNo = TB2.intSampleNo
		AND TB1.intSampleYear = TB2.intSampleYear
		AND TB1.intRevisionNo = (TB2.intRevisionNo+1)
		AND TB1.strComboName = TB2.strComboName
		AND TB1.strPrintName = TB2.strPrintName
		AND TB1.TECHNIQUE = TB2.TECHNIQUE
		AND TB1.intItem = TB2.intItem
WHERE
  TB1.intSampleNo = '$sampleNoF'
		AND TB1.intSampleYear = '$sampleYearF'
		AND TB1.intRevisionNo = '$revisionNo'
		AND TB1.strComboName = '$comboF'
		AND TB1.strPrintName = '$printNameF'
		AND TB1.COLOUR = '$colourId'
		AND TB1.TECHNIQUE = '$technique'
		AND TB1.intItem = '$item' LIMIT 1";
	$result_S	= $db->RunQuery2($sql_S);
	$num_rows = mysqli_num_rows($result_S);
	if($num_rows > 0){
		while($row_S = mysqli_fetch_array($result_S)){
		$sql_D = "DELETE FROM trn_sample_specialtechniques_print_width_height  WHERE
  		intSampleNo = '$sampleNoF'
		AND intSampleYear = '$sampleYearF'
		AND intRevisionNo = '$revisionNo'
		AND strComboName = '$comboF'
		AND strPrintName = '$printNameF'
		AND COLOUR = '$colourId'
		AND TECHNIQUE = '$technique'
		AND intItem = '$item' ";

		$result_D	= $db->RunQuery2($sql_D);
		}
	}


	$sql		= "INSERT INTO trn_sample_specialtechniques_print_width_height (intSampleNo,
		 intSampleYear,
		 intRevisionNo,
		 strComboName,
		 strPrintName,
		 TECHNIQUE,
		 COLOUR,
		 intItem,
		 intSerialNo,
		  intPrintWidth,
		  intPrintHeight,
		   intConsumption)
SELECT
'$sampleNoF',
'$sampleYearF',
'$revisionNo',
'$comboF',
'$printNameF',
'$technique',
'$colourId',
'$item',
intSerialNo,
 intPrintWidth,
  intPrintHeight,
  intConsumption
FROM   trn_sample_specialtechniques_print_width_height
WHERE
					trn_sample_specialtechniques_print_width_height.intSampleNo = '$sampleNoF' AND
					trn_sample_specialtechniques_print_width_height.intSampleYear = '$sampleYearF' AND
					trn_sample_specialtechniques_print_width_height.intRevisionNo = '$prev_revNo' AND
					trn_sample_specialtechniques_print_width_height.strComboName = '$comboF' AND
					trn_sample_specialtechniques_print_width_height.strPrintName = '$printNameF' AND
					trn_sample_specialtechniques_print_width_height.TECHNIQUE = '$technique' AND
					trn_sample_specialtechniques_print_width_height.COLOUR = '$colourId' AND
					trn_sample_specialtechniques_print_width_height.intItem = '$item'";

	$result 	= $db->RunQuery2($sql);
}

function getRecieptAvailable($sampleNo,$sampleYear,$revisionNo,$combo,$printName,$colorID,$intTechnicleID,$ItemID){

    global $db;

    $sql	="SELECT
					trn_sample_color_recipes.intSampleNo
					FROM
					trn_sample_color_recipes
					WHERE
					trn_sample_color_recipes.intSampleNo = '$sampleNo' AND
					trn_sample_color_recipes.intSampleYear = '$sampleYear' AND
					trn_sample_color_recipes.intRevisionNo = '$revisionNo' AND
					trn_sample_color_recipes.strCombo = '$combo' AND
					trn_sample_color_recipes.strPrintName = '$printName' AND
					trn_sample_color_recipes.intColorId = '$colorID' AND
					trn_sample_color_recipes.intTechniqueId = '$intTechnicleID' AND
					trn_sample_color_recipes.intItem = '$ItemID'
					";

    $result 	= $db->RunQuery2($sql);
    $row		= mysqli_fetch_array($result);
    $status_cp		= $row['intSampleNo'];

    if($status_cp){
        return 0;
    }
    else
        return 1;

}
?>
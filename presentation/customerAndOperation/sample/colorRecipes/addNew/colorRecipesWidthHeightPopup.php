<title>SAMPLE SPECIAL TECHNIQUES</title>
<style>

</style>
<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];
$company 	= $_SESSION['headCompanyId'];

$intUser  = $_SESSION["userId"];

include $backwardseperator."dataAccess/Connector.php";

$comboName 		   = $_REQUEST['comboName'];
$printName 		   = $_REQUEST['printName'];
$item              = $_REQUEST['item'];
$sampleNo          = $_REQUEST['sampleNo'];
$sample_year       = $_REQUEST['sample_year'];
$revisionNo        = $_REQUEST['revisionNo'];
$foilWidthPopUp    = $_REQUEST['foilWidthPopUp'];
$foilHeightPopUp   = $_REQUEST['foilHeightPopUp'];
$consum_auto       = $_REQUEST['consum_auto'];
$technique         = $_REQUEST['technique'];
$colorId	       = $_REQUEST['colorId'];
$cycles		       = $_REQUEST['cycles'];
?>
<body>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>

<form id="frmColorRecipesWidthHeightPopup" name="frmColorRecipesWidthHeightPopup" method="post" action="">
<table width="450" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutS">
		  <div class="trans_text"> Print Sizes </div>
		  <table width="483" border="0" align="center" bgcolor="#FFFFFF">
    <td width="477"><table width="477" border="0">
      <tr>
        <td width="471"></td>
      </tr>
      <tr>
        <td align="center"><div style="width:400px;height:300px;overflow:scroll" align="center" class="tableBorder_allRound" >
          <table width="100%"  class="bordered"  id="tblSizesPopup" align="center" >
            <tr class="">
                <?php
                $canEdit = 1;
                $sql_isApproved = "
SELECT * FROM trn_sample_special_technical_header_approved_by
									INNER JOIN trn_sample_special_technical_header ON trn_sample_special_technical_header_approved_by.SAMPLE_NO = trn_sample_special_technical_header.SAMPLE_NO AND trn_sample_special_technical_header_approved_by.SAMPLE_YEAR = trn_sample_special_technical_header.SAMPLE_YEAR AND trn_sample_special_technical_header_approved_by.REVISION_NO = trn_sample_special_technical_header.REVISION_NO AND trn_sample_special_technical_header_approved_by.COMBO = trn_sample_special_technical_header.COMBO AND trn_sample_special_technical_header_approved_by.PRINT = trn_sample_special_technical_header.PRINT
									WHERE trn_sample_special_technical_header_approved_by.SAMPLE_NO = '$sampleNo'
									AND trn_sample_special_technical_header_approved_by.SAMPLE_YEAR = '$sample_year'
									AND trn_sample_special_technical_header_approved_by.REVISION_NO = '$revisionNo'
									AND trn_sample_special_technical_header_approved_by.COMBO = '$comboName'
									AND trn_sample_special_technical_header_approved_by.PRINT = '$printName'
									AND trn_sample_special_technical_header_approved_by.`STATUS` = '0'
									AND trn_sample_special_technical_header_approved_by.APPROVE_LEVEL = '1'
									AND trn_sample_special_technical_header.`STATUS` =1";
				$result_isApproved = $db->RunQuery($sql_isApproved);
                $num_rows = mysqli_num_rows($result_isApproved);
                if($num_rows == 0){
					$canEdit = 1;
					?>
				<?php }
                else{
                    $canEdit = 0;
                }
                ?>
              <th width="11%" height="22" <?php if($canEdit==0){ ?> style="display:none" <?php } ?>>Delete</th>
              <th width="20%" height="22" >Width(Inch)</th>
              <th width="20%" >Height(Inch)</th>
              <th width="30%">Consumption(m)</th>
              </tr>
            <?php
          $sql = "SELECT intSerialNo,intPrintWidth,intPrintHeight,intConsumption FROM trn_sample_specialtechniques_print_width_height WHERE
                   intSampleNo = '$sampleNo'
                   AND intSampleYear = '$sample_year'
                   AND intRevisionNo = '$revisionNo'
                   AND strComboName = '$comboName'
                   AND strPrintName = '$printName'
                   AND TECHNIQUE 	= '$technique'
                   AND COLOUR 		= '$colorId'
                   AND intItem 		= '$item' ";
            $result = $db->RunQuery($sql);
            $r=0;

           while($row=mysqli_fetch_array($result)) {
            if($num_rows>0){ $r=1;?>
              <tr class="normalfnt">
                       <td align = "center" bgcolor = "#FFFFFF" id="deleteImage" <?php if($canEdit==0){ ?> style="display:none" <?php } ?>><img src = "images/del.png" width = "15" height = "15" class="delImgPopup" /><span id="deleteImageSerialNo" style="visibility: hidden"><?php echo $row['intSerialNo']?></span></td >
              <td align = "center" bgcolor = "#FFFFFF" id = "txtWidth" ><?php echo round($row['intPrintWidth'],6); ?><span id="isApproved" style="visibility: hidden"><?php echo $canEdit; ?></span></td >
              <td align = "center" bgcolor = "#FFFFFF" id = "txtHeight" ><?php echo round($row['intPrintHeight'],6); ?></td >
              <td align = "center" bgcolor = "#FFFFFF" id = "consumption" ><?php echo round($row['intConsumption'],6); ?></td >
              </tr >
            <?php }
               else{ $r=1; ?>
                   <tr class="normalfnt" >
                       <td align = "center" bgcolor = "#FFFFFF" id="deleteImage" <?php if($canEdit==0){ ?> style="display:none" <?php } ?>><img src = "images/del.png" width = "15" height = "15" class="delImgPopup" /><span id="deleteImageSerialNo" style="visibility: hidden"><?php echo $row['intSerialNo']?></span></td >
                       <td align = "center" bgcolor = "#FFFFFF" id = "txtWidth" ><input name = "txtWidth" type = "text" id = "txtWidth" style = "width:80px; text-align:center"  value = "<?php echo round($row['intPrintWidth'],6); ?>" class="cls_cal cls_width validate[required,custom[number]] text-input"  /></td >
                       <td align = "center" bgcolor = "#FFFFFF" id = "txtHeight" ><input name = "txtHeight" type = "text" id = "txtHeight" style = "width:80px; text-align:center"   value = "<?php echo round($row['intPrintHeight'],6); ?>" class="cls_cal cls_height validate[required,custom[number]] text-input"  /></td >
                       <td align = "center" bgcolor = "#FFFFFF" id = "consumption" ><input type="text" value="<?php echo round($row['intConsumption'],6); ?>" size="10" style="text-align: center" id="ConsumptionInput" <?php if($consum_auto == 1){echo "disabled";} ?>></td >
                   </tr >
             <?php }
           }
            if($r==0 && $canEdit == 1){?>
            <tr class="normalfnt" >
              <td align = "center" bgcolor = "#FFFFFF" id="deleteImage"><img src = "images/del.png" width = "15" height = "15" class="delImgPopup"  /><span id="deleteImageSerialNo" style="visibility: hidden"><?php echo 0; ?></span></td >
              <td align = "center" bgcolor = "#FFFFFF" id = "txtWidth" ><input name = "txtWidth" type = "text" id = "txtWidth" style = "width:80px; text-align:center"  value = "0" class="cls_cal cls_width validate[required,custom[number]] text-input"  /></td >
              <td align = "center" bgcolor = "#FFFFFF" id = "txtHeight" ><input name = "txtHeight" type = "text" id = "txtHeight" style = "width:80px; text-align:center"   value = "0" class="cls_cal cls_height validate[required,custom[number]] text-input"  /></td >
              <td align = "center" bgcolor = "#FFFFFF" id = "consumption" ><input type="text" value="0" size="10" style="text-align: center" id="ConsumptionInput" <?php if($consum_auto == 1){echo "disabled";} ?> ></td >
            </tr >
            <?php } ?>
            <tr class="dataRow">
            <td colspan="4" align="left"  bgcolor="#FFFFFF"><?php if($canEdit == 1 ){?><img src="images/Tadd.jpg" name="butInsertRowPopup" width="78" height="24" class="mouseover" id="butInsertRowPopup" /><?php } ?>
                <span id="foilWidthPopUp" style="visibility: hidden" ><?php echo $foilWidthPopUp; ?></span>
                <span id="foilHeightPopUp" style="visibility: hidden"><?php echo $foilHeightPopUp; ?></span>
                <span id="consum_auto" style="visibility: hidden"><?php echo $consum_auto;  ?></span>
            </td>
            </tr>
            </table>
          </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><?php if($canEdit == 1 ){?><img src="images/Tsave.jpg" width="92" height="24"  alt="add" id="butAdd" name="butAdd" class="mouseover"/><?php }?><img src="images/Tclose.jpg" width="92" height="24" id="butClose1" name="butClose1" class="mouseover"  /></td>
      </tr>
    </table></td>
    </tr>
  </table>
  </div>
  </div>
</form>
<div  style="visibility: hidden" id="popUpdata">
<p id="ComboNamePopUp"><?php echo $comboName; ?></p>
<p id="printNamePopUp"><?php echo $printName; ?></p>
<p id="itemName"><?php echo $item; ?></p>
<p id="consumption_auto"><?php echo $consum_auto; ?></p>
<p id="techniquePopUp"><?php echo $technique; ?></p>
<p id="colorPopUp"><?php echo $colorId; ?></p>
<p id="cycles"><?php echo $cycles; ?></p>
</div>
</body>
</html>


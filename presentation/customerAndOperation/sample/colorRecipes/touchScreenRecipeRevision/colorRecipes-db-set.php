<?php
session_start();
include "../../../../../dataAccess/Connector.php";
$requestType = $_REQUEST['requestType'];
$userId		 = $_SESSION["userId"];
if($requestType=='saveDetails')
{
    $db->begin();
    $toSave=0;
    $saved=0;
    $arrRecipe 		= json_decode($_REQUEST['arrRecipe'], true);

    $sampleNo 		= $arrRecipe[0]['sampleNo'];
    $sampleYear 	= $arrRecipe[0]['sampleYear'];
    $revNo 			= $arrRecipe[0]['revisionNo'];
    $combo 			= $arrRecipe[0]['combo'];
    $printName 		= $arrRecipe[0]['printName'];
    $recipeRevision	= $arrRecipe[0]['recipeRevision'];

    $color 			= $arrRecipe[0]['color'];
    $techId 		= $arrRecipe[0]['techId'];
    $inkTypeId 		= $arrRecipe[0]['inkTypeId'];


   if($sampleNo){
        $sql		="SELECT
						trn_sampleinfomations.intStatus,
						trn_sampleinfomations.intTechnicalStatus
						FROM `trn_sampleinfomations`
						WHERE
						trn_sampleinfomations.intSampleNo = '$sampleNo' AND
						trn_sampleinfomations.intSampleYear = '$sampleYear' AND
						trn_sampleinfomations.intRevisionNo = '$revNo'
						";
        $results 	= $db->RunQuery2($sql);
        $rowS		=mysqli_fetch_array($results);
        $sampStatus 	= $rowS['intStatus'];
        $techStatus 	= $rowS['intTechnicalStatus'];
    }

    $sql 		= "SELECT `trn_sample_color_recipes_bulk_approve`.`intApproved` from trn_sample_color_recipes_approve WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName' and recipeRevision = '$recipeRevision'";
    $result_app	=$db->RunQuery2($sql);
    $row_app	=mysqli_fetch_array($result_app);
    $flag_recipe_approved	= $row_app['intApproved'];
    //---------------------------------

    $editable=0;
    if($flag_recipe_approved!=1 && $techStatus==1)
        $editable=1;

    //------------------------------------
    if($editable==1){
        $toSave++;
        $sql = "delete from trn_sample_color_recipes_bulk where intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName' and bulkRevNumber ='$recipeRevision'";
        $result = $db->RunQuery2($sql);
        if ($result){
            $saved++;
        }
        $previousRecipe = getPreviousRevData($sampleNo,$sampleYear,$revNo,$combo,$printName, $recipeRevision-1);
        $preCount = $previousRecipe['count'];
        $currentCount = 0;
        $approvalLvl = 1;
        foreach($arrRecipe as $x)
        {
            $toSave++;
            $sql = "INSERT INTO `trn_sample_color_recipes_bulk` 
			(`intSampleNo`,`intSampleYear`,`intRevisionNo`,`strCombo`,`strPrintName`,`bulkRevNumber`,`intColorId`,`intTechniqueId`,
				`intInkTypeId`,`intItem`,`dblWeight`,`intNumOfShots`,`intUser`,`dtDate`) 
			VALUES ('".$x['sampleNo']."','".$x['sampleYear']."','".$x['revisionNo']."','".$x['combo']."','".$x['printName']."', '".$x['recipeRevision']."','".$x['color']."','".$x['techId']."','".$x['inkTypeId']."','".$x['itemId']."','".val($x['weight'])."','".val($x['noOfShots'])."','".$_SESSION["userId"]."',now())";
            $result 	= $db->RunQuery2($sql);
            if ($result){
                $saved++;
                $currentCount++;
            }
            if ($approvalLvl < 3 && isset( $previousRecipe[$x['techId']][$x['inkTypeId']][$x['color']][$x['itemId']])){
                $approvalLvl = 1;
            } else {
                $approvalLvl = 3;
            }
        }
        if ($currentCount != $preCount){
            $approvalLvl = 3;
        }
        $toSave++;
        $headerStatus = $approvalLvl + 1;
        $sql = "UPDATE `trn_sample_color_recipes_bulk_header` SET `STATUS`= '$headerStatus',`approvalLvl`='$approvalLvl' WHERE 
                intSampleNo='$sampleNo' AND intSampleYear='$sampleYear' AND intRevisionNo='$revNo' AND strCombo='$combo' AND strPrintName='$printName' AND recipeRevision='$recipeRevision'";
        $result 	= $db->RunQuery2($sql);
        if ($result){
            $saved++;
        }
    }
    //	}
    //-------------------------------------
    if($editable==0){
        $db->RunQuery2('Rollback');
        $response['type'] 		= 'fail';
        if($sampStatus==1)
            $response['msg'] 		="Final approval has been raised !";
        else if($techStatus!=1)
            $response['msg'] 		="Technical approval hasn't been raised !";
        else if($flag_recipe_approved==1)
            $response['msg'] 		="Color recipes are approved. Can't edit !";

    }
    else if($toSave==0){
        $response['type'] 		= 'fail';
        $response['msg'] 		="No items to save !";
    }
    else if(($toSave==$saved)){
        $db->RunQuery2('Commit');
        $response['type'] 		= 'pass';
        $response['revision']	= 'pass';
        $response['msg'] 		= 'Saved successfully.';
    }

    else if($toSave > $saved){
        $db->RunQuery2('Rollback');
        $response['type'] 		= 'fail';
        $response['msg'] 		= "Duplicate Records to save.";
        $response['q'] 			= $sql;
    }
    else{
        $db->RunQuery2('Rollback');
        $response['type'] 		= 'fail';
        $response['msg'] 		= $db->errormsg;
        $response['q'] 			= $sql;
    }
    $db->commit();
}
if($requestType=='saveNewRevision')
{
    $toSave=0;
    $saved=0;
    $arrRecipe 		= json_decode($_REQUEST['arrRecipe'], true);

    $sampleNo 		= $arrRecipe[0]['sampleNo'];
    $sampleYear 	= $arrRecipe[0]['sampleYear'];
    $revNo 			= $arrRecipe[0]['revisionNo'];
    $combo 			= $arrRecipe[0]['combo'];
    $printName 		= $arrRecipe[0]['printName'];

    $color 			= $arrRecipe[0]['color'];
    $techId 		= $arrRecipe[0]['techId'];
    $inkTypeId 		= $arrRecipe[0]['inkTypeId'];
    $db->begin();


    if($sampleNo){
        $sql		="SELECT
						trn_sampleinfomations.intStatus,
						trn_sampleinfomations.intTechnicalStatus
						FROM `trn_sampleinfomations`
						WHERE
						trn_sampleinfomations.intSampleNo = '$sampleNo' AND
						trn_sampleinfomations.intSampleYear = '$sampleYear' AND
						trn_sampleinfomations.intRevisionNo = '$revNo'
						";
        $results 	= $db->RunQuery2($sql);
        $rowS		=mysqli_fetch_array($results);
        $sampStatus 	= $rowS['intStatus'];
        $techStatus 	= $rowS['intTechnicalStatus'];
    }

    $sql_revision = "SELECT MAX(recipeRevision) AS next_rev from `trn_sample_color_recipes_bulk_header` WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName'";
    $result_rev = $db->RunQuery2($sql_revision);
    if(mysqli_num_rows($result_rev) > 0){
        $row_rev = mysqli_fetch_array($result_rev);
        $next_revision = $row_rev['next_rev'];
    } else {
        $next_revision =0;
    }
    $previousRecipe = getPreviousRevData($sampleNo,$sampleYear,$revNo,$combo,$printName, $next_revision);
    $preCount = $previousRecipe['count'];
    $currentCount = 0;
    $next_revision++ ;
    foreach($arrRecipe as $x)
    {
        $toSave++;
        $sql = "INSERT INTO `trn_sample_color_recipes_bulk` 
        (`intSampleNo`,`intSampleYear`,`intRevisionNo`,`strCombo`,`strPrintName`,`bulkRevNumber`,`intColorId`,`intTechniqueId`,
            `intInkTypeId`,`intItem`,`dblWeight`,`intUser`,`intNumOfShots`,`dtDate`) 
        VALUES ('".$x['sampleNo']."','".$x['sampleYear']."','".$x['revisionNo']."','".$x['combo']."','".$x['printName']."', '".$next_revision."','".$x['color']."','".$x['techId']."','".$x['inkTypeId']."','".$x['itemId']."','".val($x['weight'])."','".$_SESSION["userId"]."','".val($x['shots'])."',now())";
        $result 	= $db->RunQuery2($sql);
        if ($result){
            $saved++;
            $currentCount++;
        }
        $previousRecipe[$x['techId']][$x['inkTypeId']][$x['color']][$x['itemId']];
        if ($approvalLvl < 3 && isset( $previousRecipe[$x['techId']][$x['inkTypeId']][$x['color']][$x['itemId']])){
            $approvalLvl = 1;
        } else {
            $approvalLvl = 3;
        }
    }
    if ($currentCount != $preCount){
        $approvalLvl = 3;
    }
    if ($toSave == $saved) {
        $headerStatus = $approvalLvl + 1;
        $toSave++;
        $sql = "INSERT INTO `trn_sample_color_recipes_bulk_header` 
        (`intSampleNo`,`intSampleYear`,`intRevisionNo`,`strCombo`,`strPrintName`,`recipeRevision`,`STATUS`,`approvalLvl`,`createdDate`,`createdBy`) 
        VALUES ('".$sampleNo."','".$sampleYear."','".$revNo."','".$combo."','".$printName."', '".$next_revision."','$headerStatus','".$approvalLvl."', now(), $userId)";
        $result 	= $db->RunQuery2($sql);
        if ($result){
            $saved++;
        }
    }

    if($toSave==0){
        $response['type'] 		= 'fail';
        $response['msg'] 		="No items to save !";
    }
    else if(($toSave == $saved)){
        $db->RunQuery2('Commit');
        $response['type'] 		= 'pass';
        $response['revision']	= $next_revision;
        $response['msg'] 		= 'Saved successfully.';
    }
    else{
        $db->RunQuery2('Rollback');
        $response['type'] 		= 'fail';
        $response['msg'] 		= $db->errormsg;
        $response['q'] 			= $sql;
    }
    $db->commit();
}

if($requestType=='approveRecipForBulk')
{
    $toSave=0;
    $saved=0;
    $db->begin();


    //----------------------------------
    $sampleNo 		= $_REQUEST['sampleNo'];
    $sampleYear 	= $_REQUEST['sampleYear'];
    $revNo 			= $_REQUEST['revisionNo'];
    $combo 			= $_REQUEST['combo'];
    $printName 		= $_REQUEST['printName'];
    $recipeRevision	= $_REQUEST['recipeRevision'];

    $errFlag=0;

    $sql 	= "SELECT STATUS, approvalLvl
		FROM trn_sample_color_recipes_bulk_header 
		WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName' and recipeRevision='$recipeRevision'";
    $result = $db->RunQuery2($sql);
    $row	=mysqli_fetch_array($result);
    $headerStatus = $row['STATUS'];
    $initialApprovalLvls = $row['approvalLvl'];
    $currentApprovalLvl = $initialApprovalLvls + 2 - $headerStatus;
    $userPermission = 1; // get form methos

    if ($userPermission ==1){
        $toSave++;
        $sql 		= "UPDATE `trn_sample_color_recipes_bulk_header` SET `STATUS` = STATUS-1 WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName' and recipeRevision='$recipeRevision'";
        $result_header	=$db->RunQuery2($sql);
        if ($result_header){
            $saved++;
        }
        $approvCount = 0;
        if ($currentApprovalLvl ==1){
            $sql = "SELECT MAX(intStatus) as revCount FROM trn_sample_color_recipes_bulk_approve  WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName' and recipeRevision='$recipeRevision' and intApproveLevelNo = $currentApprovalLvl";
            $result = $db->RunQuery2($sql);
            $row	=mysqli_fetch_array($result);
            $approvCount = $row['revCount'] + 1;
        } else {
            $queryAppLvl = $currentApprovalLvl-1;
            $sql = "SELECT MAX(intStatus) as revCount FROM trn_sample_color_recipes_bulk_approve  WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName' and recipeRevision='$recipeRevision' and intApproveLevelNo = $queryAppLvl";
            $result = $db->RunQuery2($sql);
            $row	=mysqli_fetch_array($result);
            $approvCount = $row['revCount'];
        }
        $toSave++;
        $sql = "INSERT INTO `trn_sample_color_recipes_bulk_approve` 
            (`intSampleNo`,`intSampleYear`,`intRevisionNo`,`strCombo`,`strPrintName`,`recipeRevision`,`intApproveLevelNo`,`dtmdateApproved`,`intUser`,`intStatus`) 
            VALUES ('$sampleNo','$sampleYear','$revNo','$combo','$printName', '$recipeRevision','$currentApprovalLvl',now(),'".$_SESSION["userId"]."','$approvCount')";
        $result 	= $db->RunQuery2($sql);
        if ($result){
            $saved++;
        }
    }

    if(($toSave==$saved)){
        $db->RunQuery2('Commit');
        $response['type'] 		= 'pass';
        $response['revision']	= 'pass';
        $response['msg'] 		= 'Approved successfully.';
    } else{
        $db->RunQuery2('Rollback');
        $response['type'] 		= 'fail';
        $response['msg'] 		= $db->errormsg;
        $response['q'] 			= $sql;
    }
    $db->commit();
}

if($requestType=='cancelApprovedRecipForBulk')
{
    $toSave=0;
    $saved=0;


    //----------------------------------
    $sampleNo 		= $_REQUEST['sampleNo'];
    $sampleYear 	= $_REQUEST['sampleYear'];
    $revNo 			= $_REQUEST['revisionNo'];
    $combo 			= $_REQUEST['combo'];
    $printName 		= $_REQUEST['printName'];

    //----------------------------------
    $errFlag=0;

    //------
    $sql 	= "SELECT
		Count(trn_orderdetails.intOrderNo) AS rcds
		FROM
		trn_sample_color_recipes_approve
		INNER JOIN trn_orderdetails ON trn_sample_color_recipes_approve.intRevisionNo = trn_orderdetails.intRevisionNo AND trn_sample_color_recipes_approve.strCombo = trn_orderdetails.strCombo AND trn_sample_color_recipes_approve.strPrintName = trn_orderdetails.strPrintName AND trn_sample_color_recipes_approve.intSampleNo = trn_orderdetails.intSampleNo AND trn_sample_color_recipes_approve.intSampleYear = trn_orderdetails.intSampleYear
		where intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName'";
    if($row['rcds']>0){
        $errFlag=1;
        $msg	="Bulk Consumption entered.Can't Cancel !";
    }

    //------
    $sql 		= "SELECT `trn_sample_color_recipes_approve`.`intApproved` from trn_sample_color_recipes_approve WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName'";
    $result_app	=$db->RunQuery($sql);
    $row_app	=mysqli_fetch_array($result_app);
    if($row_app['intApproved']==0){
        $errFlag=1;
        $msg	="Already cancelled !";
    }
    //------

    if($errFlag!=1){

        $sql = "UPDATE `trn_sample_color_recipes_approve` SET `intApproved`='0' WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName'";
        $result_app=$db->RunQuery($sql);
    }

    //-------------------------------------

    if(($result_app==1)){
        $response['type'] 		= 'pass';
        $response['msg'] 		= 'Canceled successfully.';
    }
    else if($errFlag==1){
        $response['type'] 		= 'fail';
        $response['msg'] 		= $msg;
    }
    else
    {
        $response['type'] 		= 'fail';
        $response['msg'] 		= $db->errormsg;
        $response['q'] 			= $sql;
    }
}
elseif($requestType=='addNewItem2')
{
    $mainId 	= ($_REQUEST['mainId']	=='null'?'':$_REQUEST['mainId']);
    $subId 		= ($_REQUEST['subId']	=='null'?'':$_REQUEST['subId']);
    $itemId 	= ($_REQUEST['itemId']	=='null'?'':$_REQUEST['itemId']);

    $sql = "INSERT INTO `mst_inkcategory_item` (`intSubCategoryId`,`intMainCategoryId`,`intItemId`) VALUES ('$subId','$mainId','$itemId')";
    $result = $db->RunQuery($sql);
    if($result)	{
        $response['msg'] 	= 'Saved Successfully.';
        $response['status'] = 1;
    }
    else
    {
        $response['msg'] 	= 'Saving error...';
        $response['status'] = 0;
    }

}
echo json_encode($response);

//---------------------------------------
function confirmedOrders($sampleNo,$sampleYear,$revNo,$combo,$printName){
    global $db;
    $sql  	= " SELECT
					trn_orderheader.intOrderNo,
					trn_orderheader.intOrderYear
					FROM
					trn_orderheader
					Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					WHERE
					trn_orderdetails.intSampleNo =  '$sampleNo' AND
					trn_orderdetails.intSampleYear =  '$sampleYear' AND
					trn_orderheader.intApproveLevelStart-
					trn_orderheader.intStatus >=  2 AND
					trn_orderdetails.strCombo =  '$combo' AND
					trn_orderdetails.strPrintName =  '$printName' AND
					trn_orderdetails.intRevisionNo =  '$revNo'
					";
    $result	= $db->RunQuery($sql);
    if(mysqli_num_rows($result)>0)
        return true;
    else
        return false;
}

function getPreviousRevData($sampleNo,$sampleYear,$revNo,$combo,$printName, $previousRev){
    global $db;
    $preColorRecipe = array();
    $table = "trn_sample_color_recipes_bulk";
    if ($previousRev == 0){
        $table = "trn_sample_color_recipes";
    }
    $sql  	= "SELECT intTechniqueId, intInkTypeId, intColorId, intItem
					FROM ".$table." as recipe
					WHERE
					recipe.intSampleNo =  '$sampleNo' AND
					recipe.intSampleYear =  '$sampleYear' AND
					recipe.strCombo =  '$combo' AND
					recipe.strPrintName =  '$printName' AND
					recipe.intRevisionNo =  '$revNo' ";
    if ($previousRev != 0){
        $sql   .=  "AND recipe.bulkRevNumber =  '$previousRev'" ;
    }

    $result	= $db->RunQuery2($sql);
    $count = 0;
    while($row=mysqli_fetch_array($result))
    {
        $technique 			= $row['intTechniqueId'];
        $inkType 			= $row['intInkTypeId'];
        $color 			    = $row['intColorId'];
        $item 			    = $row['intItem'];
        $preColorRecipe[$technique][$inkType][$color][$item] = true;
        $count++;
    }
    $preColorRecipe['count'] = $count;
    return $preColorRecipe;
}

function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
    global $db;

    $confirmatonMode=0;
    $k=$savedStat+2-$intStatus;
    $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";

    $resultp = $db->RunQuery($sqlp);
    $rowp=mysqli_fetch_array($resultp);

    if($rowp['int'.$k.'Approval']==1){
        if($intStatus!=1){
            $confirmatonMode=1;
        }
    }

    return $confirmatonMode;
}

?>
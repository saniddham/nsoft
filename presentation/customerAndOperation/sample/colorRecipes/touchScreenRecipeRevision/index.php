<?php
ini_set('display_errors', 0);
session_start();
//print_r($_SESSION);

$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$sessionUserId = $_SESSION['userId'];
$thisFilePath = $_SERVER['PHP_SELF'];
$locationId = $_SESSION["CompanyID"];

require_once "{$backwardseperator}dataAccess/permisionCheck_touch1.inc";
require_once $backwardseperator . "class/cls_commonErrorHandeling_get.php";

$obj_commonErr = new cls_commonErrorHandeling_get($db);

$sampleNo = $_REQUEST['sampleNo'];
$sampleYear = $_REQUEST['sampleYear'];
$revNo = $_REQUEST['revNo'];
$combo = $_REQUEST['combo'];
$printName = $_REQUEST['printName'];
$recipeRevision = $_REQUEST['recipeRevision'];
$recipeRevisionName = "";
if ($recipeRevision == ''){
    unset($recipeRevision);
}
$groundColour = get_ground_colour($sampleNo, $sampleYear, $revNo, $combo, $printName);

$mode = 'first';
if (isset($sampleNo) && isset($sampleYear) && isset($revNo) && isset($combo) && isset($printName) && isset($recipeRevision)) {
    $mode = 'last';
} else if (isset($sampleNo) && isset($sampleYear)) {
    $mode = "second";
}

$flag_recipe_status = 0;
$flag_bulk_revision_there = 0;

$disabled = "";
$hide = "";
if($mode == 'last'){
    $sql 		= "SELECT `trn_sample_color_recipes_bulk_header`.`STATUS` as status, `trn_sample_color_recipes_bulk_header`.`approvalLvl` as approvalLevels from trn_sample_color_recipes_bulk_header WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName' and recipeRevision = '$recipeRevision'";
    $result_app	=$db->RunQuery($sql);
    $row_app	=mysqli_fetch_array($result_app);
    $flag_recipe_status	= $row_app['status'];
    $approvalLevelsNeeded = $row_app['approvalLevels'];
    $maxRecipe = 0;
    if ($flag_recipe_status - $approvalLevelsNeeded != 1 || $recipeRevision == 0){
        $disabled = "disabled";
    }
    $sql 		= "SELECT IFNULL(MAX(recipeRevision),0) as maxBulkRev from trn_sample_color_recipes_bulk_header WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName' ";
    $result_max	=$db->RunQuery($sql);
    $row_app	=mysqli_fetch_array($result_max);
    $maxRecipe = $row_app['maxBulkRev'];
    $flag_bulk_revision_there = $maxRecipe;

    $sql 		= "SELECT alphabatic from mst_number_mapping WHERE intNumber='$recipeRevision' ";
    $result_map	=$db->RunQuery($sql);
    $row_map	=mysqli_fetch_array($result_map);
    $recipeRevisionName = $row_map['alphabatic'];


}
//combo
$programCode = 'P545';

$app_permision = get_app_permision($sessionUserId, $programCode, 'int1Approval');
$special_view_bulk = $obj_commonErr->Load_special_menupermision('59', $sessionUserId, 'RunQuery')
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Bulk Recipes</title>

    <link href="css.css" rel="stylesheet"/>
    <!-- jQuery & jQuery UI + theme (required) -->
    <link href="other/jquery-ui.css" rel="stylesheet">
    <script src="other/jquery.js"></script>
    <script src="other/jquery-ui.min.js"></script>

    <!-- keyboard widget css & script (required) -->
    <link href="css/keyboard.css" rel="stylesheet">
    <script src="js/jquery.keyboard.js"></script>

    <!-- keyboard extensions (optional) -->
    <script src="js/jquery.mousewheel.js"></script>
    <script src="js/jquery.keyboard.extension-typing.js"></script>
    <script src="js/jquery.keyboard.extension-autocomplete.js"></script>

    <!-- demo only -->
    <link href="demo/demo.css" rel="stylesheet">
    <script src="demo/demo.js"></script>
    <script src="other/jquery.jatt.min.js"></script> <!-- tooltips -->

    <!-- theme switcher -->
    <script src="other/themeswitchertool.htm"></script>

    <script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>
    <script type="application/javascript" src="../../../../../libraries/validate/jquery.js"></script>
    <script type="application/javascript" src="../../../../../libraries/validationEngine/js/jquery.validationEngine-en.js"></script>
    <script type="application/javascript" src="index.js"></script>
    <script>
        $(function () {
            $('#keyboard').keyboard();
        });
        var projectName = '/<?php echo $_SESSION["projectName"]; ?>';
    </script>

    <script type="application/javascript" src="colorRecipes-js.js"></script>
</head>

<body>
<form id="frmColorRecipieRevision" name="frmColorRecipieRevision" method="post" >
<table width="100%" border="0" align="center">
    <tr>
<td><table width="1024" height="199" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="145" height="48" class="label">&nbsp;SAMPLE YEAR</td>
        <td width="131"><select name="cboSampleYear" class="down" id="cboSampleYear">
                <?php
                $d = date('Y');
                if ($sampleYear == '')
                    $sampleYear = $d;
                for ($d; $d >= 2012; $d--) {
                    if ($d == $sampleYear)
                        echo "<option selected=\"selected\" id=\"$d\" >$d</option>";
                    else
                        echo "<option id=\"$d\" >$d</option>";
                }
                ?>
            </select></td>
        <td width="117" class="label">SAMPLE NO</td>
        <td width="287"><input type="text" style="height:50px;margin-top:5px" class="hex txtSampleNo" id="cboSampleNo"
                               aria-haspopup="true" role="textbox" value="<?php echo $sampleNo; ?>"/></td>
        <td width="117">&nbsp;<a id="butSearch" class="orange button" style="height:35px;margin-top:5px">Search</a></td>
        <td width="101" style="text-align:right"><?php if ($special_view_bulk == 1) { ?><a id="butBulk"
                                                                                           href="../../../bulk/bulkStockAllocation_inkType/index.php"
                                                                                           class="white button"
                                                                                           style="height:35px;margin-top:5px">&nbsp;BULK&nbsp;</a><?php } ?>
        </td>
        <td width="126" style="text-align:right"><a id="butSearch" href="../logout.php" class="pink button"
                                                    style="height:35px;margin-top:5px">LOG OUT</a></td>
    </tr>

    <?php

    $sql = "SELECT
		 DISTINCT 
		trn_orderheader.intOrderNo,
		trn_orderheader.intOrderYear
		FROM
		trn_orderheader
		Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
		WHERE
		trn_orderdetails.intSampleNo =  '$sampleNo' AND
		trn_orderdetails.intSampleYear =  '$sampleYear' AND 
		trn_orderheader.intApproveLevelStart-trn_orderheader.intStatus>=2
				";
    $string = '</br>';
    $string_tmp = $string;
    $result = $db->RunQuery($sql);
    $i = 0;
    while ($row = mysqli_fetch_array($result)) {
        $i++;
        $string .= $row['intOrderNo'] . "/" . $row['intOrderYear'] . "&nbsp;";
        $string_tmp .= $row['intOrderNo'] . "/" . $row['intOrderYear'] . "&nbsp;";
        $len = strlen($string_tmp);
        if ($len > 80) {
            $string .= "</br>";
            $string_tmp = '';
        }

    } ?>

    <?php
    if ($i > 0 && $mode =='last' && $recipeRevision != 0) {
        ?>
        <tr>
            <td class="label"><?php if ($flag_recipe_status ==1){
                ?> <span style="color:green">APPROVED</span> <?php
                } else if ($flag_recipe_status == -1 ){
                    ?> <span style="color:red">REJECTED</span> <?php
                } else {
                ?> <span style="color:yellow">PENDING</span> <?php
                } ?></td>
            <td colspan="7">PO Confirmations are already raised for PO Nos :&nbsp;<?php echo $string; ?></td>
        </tr>

        <?Php
    }
    ?>

    <?php
    if ($mode == 'second') {
        ?>
        <tr>
            <td height="17" colspan="7">
                <table style="margin-top:150px" align="center" width="500" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center">Revision No</td>
                        <td>Combo</td>
                        <td>Print</td>
                        <td>Recipe Revision</td>
                    </tr>
                    <tr style="margin-top:250px">
                        <td><select name="cboRevisionNo" size="1" multiple="multiple" id="cboRevisionNo"
                                    style="height:200px;width:200px">
                                <?php
                                echo $sql = "SELECT
					trn_sampleinfomations.intRevisionNo
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations.intSampleNo =  '$sampleNo' 
					AND trn_sampleinfomations.intTechnicalStatus =1 
				ORDER BY
					trn_sampleinfomations.intRevisionNo ASC
				";
                                $result = $db->RunQuery($sql);
                                while ($row = mysqli_fetch_array($result)) {
                                    if ($revNo == $row['intRevisionNo'])
                                        echo "<option selected=\"selected\" value=\"" . $row['intRevisionNo'] . "\">" . $row['intRevisionNo'] . "</option>";
                                    else
                                        echo "<option value=\"" . $row['intRevisionNo'] . "\">" . $row['intRevisionNo'] . "</option>";
                                }
                                ?>
                            </select></td>
                        <td><select name="cboCombo" size="1" multiple="multiple" id="cboCombo"
                                    style="height:200px;width:200px">
                                <?php
                                if ($revNo != '') {
                                    echo $sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strComboName
					FROM trn_sampleinfomations_details
				WHERE
					trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear' and trn_sampleinfomations_details.intRevNo='$revNo'
				ORDER BY
					trn_sampleinfomations_details.strComboName ASC
				";
                                    $result = $db->RunQuery($sql);
                                    while ($row = mysqli_fetch_array($result)) {
                                        if ($combo == $row['strComboName'])
                                            echo "<option selected=\"selected\" value=\"" . $row['strComboName'] . "\">" . $row['strComboName'] . "</option>";
                                        else
                                            echo "<option value=\"" . $row['strComboName'] . "\">" . $row['strComboName'] . "</option>";
                                    }
                                }
                                ?>
                            </select></td>
                        <td><select name="cboPrint" size="1" multiple="multiple" id="cboPrint"
                                    style="height:200px;width:200px">
                                <?php

                                $sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strPrintName
					FROM trn_sampleinfomations_details
				WHERE
					trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_details.strComboName 		=  '$combo' and trn_sampleinfomations_details.intRevNo='$revNo'
				ORDER BY
					trn_sampleinfomations_details.strPrintName ASC
				";
                                $result = $db->RunQuery($sql);
                                while ($row = mysqli_fetch_array($result)) {
                                    if ($printName == $row['strPrintName'])
                                        echo "<option selected=\"selected\" value=\"" . $row['strPrintName'] . "\">" . $row['strPrintName'] . "</option>";
                                    else
                                        echo "<option value=\"" . $row['strPrintName'] . "\">" . $row['strPrintName'] . "</option>";
                                }
                                ?>
                            </select></td>
                        <td><select name="cboRecipeRevision" size="1" multiple="multiple" id="cboRecipeRevision"
                                    style="height:200px;width:200px">
                                <?php

                                $sql = "SELECT DISTINCT  recipeRevision
                                                        FROM
                                                            trn_sample_color_recipes
                                                        WHERE
                                                            trn_sample_color_recipes.intSampleNo =  '$sampleNo' AND
                                                            trn_sample_color_recipes.intSampleYear =  '$sampleYear' AND
                                                            trn_sample_color_recipes.intRevisionNo =  '$revNo' AND
                                                            trn_sample_color_recipes.strCombo =  '$combo' AND
                                                            trn_sample_color_recipes.strPrintName =  '$printName' 
                                                            ORDER BY
                                                            trn_sample_color_recipes.recipeRevision ASC";
                                $result = $db->RunQuery($sql);
                                while ($row = mysqli_fetch_array($result)) {
                                    if ($printName == $row['strPrintName'])
                                        echo "<option selected=\"selected\" value=\"" . $row['recipeRevision'] . "\">" . $row['recipeRevision'] . "</option>";
                                    else
                                        echo "<option value=\"" . $row['recipeRevision'] . "\">" . $row['recipeRevision'] . "</option>";
                                }
                                ?>
                            </select></td>

                    </tr>
                </table>
            </td>
        </tr>
        <?PHp
        die();
    }
    ?>
    <tr>
        <td height="17" colspan="7">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="27%" rowspan="3">
                        <div id="divPicture" class="tableBorder_allRound divPicture" align="center"
                             contenteditable="true" style="width:264px;height:148px;overflow:hidden">
                            <?php
                            if ($sampleNo != '') {
                                echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"../../../../../documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revNo-" . substr($printName, 6, 1) . ".png\" />";
                            }
                            ?>
                        </div>
                    </td>
                    <?php

                    if ($mode == 'last')
                    {
                    ?>
                    <table style="font-size:12px" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="48%">Revision No</td>
                            <td width="2%">:</td>
                            <td width="47%" ><div id="txtRevNo"><?php echo $revNo; ?></div></td>
                            <td width="1%" rowspan="3">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Combo</td>
                            <td>:</td>
                            <td ><div id="txtCombo"><?php echo $combo; ?></div></td>
                            <td width="2%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Print</td>
                            <td>:</td>
                            <td><div id="txtPrintName"><?php echo $printName; ?></div></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Recipe Revision No</td>
                            <td>:</td>
                            <td><div id="txtRecipeRevisionName"><?php echo $recipeRevisionName; ?></div></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr hidden>
                            <td>Recipe Revision No</td>
                            <td>:</td>
                            <td><div id="txtRecipeRevision"><?php echo $recipeRevision; ?></div></td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>

                    <?php
                    }
                    ?></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="13%">&nbsp;</td>
                    <td width="2%">&nbsp;</td>
                    <td width="13%">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                    <td width="3%">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td colspan="5" align="center">
            <div id="msg_parent" align="center" style="width:100%;height:30px;color:#3F6"></div>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr style="margin-top:250px">
        <td colspan="7">
            <table id="tblInk" class="tableBorder_allRound" style="background:#FFF;margin-top:5px" width="100%"
                   border="0" cellspacing="1" cellpadding="0">
                <tr style="color:#FF0">
                    <th class="tableBorder_allRound" width="8%" align="center" bgcolor="#333333"></th>
                    <th class="tableBorder_allRound" width="20%" align="center" bgcolor="#333333">COLOR</th>
                    <th class="tableBorder_allRound" width="20%" align="center" bgcolor="#333333">TECHNIQUE</th>
                    <th class="tableBorder_allRound" width="20%" align="center" bgcolor="#333333">INK TYPE</th>
                    <th class="tableBorder_allRound" width="5%" align="center" bgcolor="#333333">SHOTS</th>
                    <th class="tableBorder_allRound" width="10%" align="center" bgcolor="#333333">ITEM</th>
                </tr>
                <?Php
                if ($recipeRevision == 0){


                $sql = "SELECT
						trn_sampleinfomations_details_technical.intNoOfShots,
						trn_sampleinfomations_details_technical.dblColorWeight,
						trn_sampleinfomations_details_technical.intColorId,
						trn_sampleinfomations_details_technical.intInkTypeId as intInkType,
						mst_inktypes.strName AS inkTypeName,
						mst_colors.strName AS colorName,
						trn_sampleinfomations_details.intTechniqueId as intTechnique,
						mst_techniques.strName AS technique
					FROM
						trn_sampleinfomations_details_technical
						Inner Join mst_inktypes ON mst_inktypes.intId = trn_sampleinfomations_details_technical.intInkTypeId
						Inner Join mst_colors ON mst_colors.intId = trn_sampleinfomations_details_technical.intColorId
						Inner Join trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_details_technical.intSampleNo AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_details_technical.intRevNo AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_details_technical.strPrintName AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_details_technical.strComboName AND trn_sampleinfomations_details.intColorId = trn_sampleinfomations_details_technical.intColorId
						Inner Join mst_techniques ON mst_techniques.intId = trn_sampleinfomations_details.intTechniqueId
					WHERE
						trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
						trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
						trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND
						trn_sampleinfomations_details_technical.strPrintName 	=  '$printName' AND
						trn_sampleinfomations_details_technical.strComboName 	=  '$combo' 
						GROUP BY 
						trn_sampleinfomations_details_technical.intColorId,
						trn_sampleinfomations_details_technical.intInkTypeId,
						trn_sampleinfomations_details.intTechniqueId 
				";
                $result = $db->RunQuery($sql);
                while ($row = mysqli_fetch_array($result))
                {
                $colorId = $row['intColorId'];
                $techniqueId = $row['intTechnique'];
                $inkType = $row['intInkType'];

                $sql_ref = "SELECT
						GROUP_CONCAT(mst_item.strName) AS ITEM 
						FROM
						trn_sampleinfomations_details
						INNER JOIN mst_item ON trn_sampleinfomations_details.intItem = mst_item.intId
						WHERE
								trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
								trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
								trn_sampleinfomations_details.intRevNo =  '$revNo' AND
								trn_sampleinfomations_details.strComboName =  '$combo' AND
								trn_sampleinfomations_details.strPrintName =  '$printName' AND
								trn_sampleinfomations_details.intColorId =  '$colorId' AND
								trn_sampleinfomations_details.intTechniqueId =  '$techniqueId'
						GROUP BY
						trn_sampleinfomations_details.intSampleNo,
						trn_sampleinfomations_details.intSampleYear,
						trn_sampleinfomations_details.intRevNo,
						trn_sampleinfomations_details.strPrintName,
						trn_sampleinfomations_details.strComboName,
						trn_sampleinfomations_details.intColorId,
						trn_sampleinfomations_details.intTechniqueId
						";
                $result_ref = $db->RunQuery($sql_ref);
                $row_ref = mysqli_fetch_array($result_ref);
                $reference = $row_ref['ITEM'];
                ?>
                <tr class="label" id="dataRow">
                    <td align="center" bgcolor="#333333"><img src="../../../../../images/del.png" width="20" height="20" <?php echo $disabled ?>
                                                              class="delRow"/>
                    <td bgcolor="#333333" align="center"><select class="color" style="width:200px;font-size:20px" id="cboColor" <?php echo $disabled ?>>
                            <?php
                            $sqlColor = "SELECT
                                            trn_sampleinfomations_details_technical.intColorId,
                                            mst_colors.strName AS colorName                                        
                                        FROM
                                            trn_sampleinfomations_details_technical
                                        INNER JOIN mst_colors ON mst_colors.intId = trn_sampleinfomations_details_technical.intColorId
                                        WHERE
                                            trn_sampleinfomations_details_technical.intSampleNo = '$sampleNo'
                                        AND trn_sampleinfomations_details_technical.intSampleYear = '$sampleYear'
                                        AND trn_sampleinfomations_details_technical.intRevNo = '$revNo'
                                        AND trn_sampleinfomations_details_technical.strPrintName = '$printName'
                                        AND trn_sampleinfomations_details_technical.strComboName = '$combo'";
                            $result1 = $db->RunQuery($sqlColor);
                            while ($row1 = mysqli_fetch_array($result1)) {
                                if ($colorId == $row1['intColorId']) {
                                    echo "<option selected=\"selected\" value=\"" . $row1['intColorId'] . "\">" . $row1['colorName'] . "</option>";
                                } else
                                    echo "<option value=\"" . $row1['intColorId'] . "\">" . $row1['colorName'] . "</option>";
                            }
                            ?>
                        </select></td>
                    <td bgcolor="#333333" align="center"><select class="technique" style="width:200px;font-size:20px" id="cboTechnique"  <?php echo $disabled ?>>
                            <?php
                            $sqlTeq = "SELECT
                                trn_sampleinfomations_details.intTechniqueId AS intTechnique,
                                mst_techniques.strName AS technique
                            FROM
                                trn_sampleinfomations_details_technical
                            INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_details_technical.intSampleNo
                            AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear
                            AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_details_technical.intRevNo
                            AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_details_technical.strPrintName
                            AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_details_technical.strComboName
                            AND trn_sampleinfomations_details.intColorId = trn_sampleinfomations_details_technical.intColorId
                            INNER JOIN mst_techniques ON mst_techniques.intId = trn_sampleinfomations_details.intTechniqueId
                            WHERE
                                trn_sampleinfomations_details_technical.intSampleNo = '$sampleNo'
                            AND trn_sampleinfomations_details_technical.intSampleYear = '$sampleYear'
                            AND trn_sampleinfomations_details_technical.intRevNo = '$revNo'
                            AND trn_sampleinfomations_details_technical.strPrintName = '$printName'
                            AND trn_sampleinfomations_details_technical.strComboName = '$combo'";
                            $result2 = $db->RunQuery($sqlTeq);
                            while ($row2 = mysqli_fetch_array($result2)) {
                                if ($techniqueId == $row2['intTechnique']) {
                                    echo "<option selected=\"selected\" value=\"" . $row2['intTechnique'] . "\">" . $row2['technique'] . "</option>";
                                } else
                                    echo "<option value=\"" . $row2['intTechnique'] . "\">" . $row2['technique'] . "</option>";
                            }
                            ?>
                        </select></td>
                    <td bgcolor="#333333" align="center"><select class="inkType" style="width:200px;font-size:20px" id="cboInkType"  <?php echo $disabled ?>>
                            <?php
                            $sqlInkType = "SELECT
                                                mst_inktypes.intId AS intInkType,
                                                mst_inktypes.strName AS inkTypeName
                                            FROM
                                                mst_inktypes
                                            ORDER BY
                                                mst_inktypes.strName ASC";
                            $result3 = $db->RunQuery($sqlInkType);
                            while ($row3 = mysqli_fetch_array($result3)) {
                                if ($inkType == $row3['intInkType']) {
                                    echo "<option selected=\"selected\" value=\"" . $row3['intInkType'] . "\">" . $row3['inkTypeName'] . "</option>";
                                } else
                                    echo "<option value=\"" . $row3['intInkType'] . "\">" . $row3['inkTypeName'] . "</option>";
                            }
                            ?>
                        </select></td>
                    <td align="center" bgcolor="#333333">
                        <input type="text" style="height:50px;margin-top:5px;margin-left:10px;margin-right:10px" class="hex txtShots" id="txtShots"
                               aria-haspopup="true" role="textbox" <?php echo $disabled ?> value="<?php echo $row['intNoOfShots']; ?>"/></td>
                    <td bgcolor="#FFFFFF">
                        <table width="287" bgcolor="#333333" cellpadding="0" cellspacing="1" id="tblItem">
                            <?php
                            $sql2 = "SELECT
							trn_sample_color_recipes.intItem,
							trn_sample_color_recipes.dblWeight
					FROM trn_sample_color_recipes
					WHERE
					trn_sample_color_recipes.intSampleNo =  '$sampleNo' AND
					trn_sample_color_recipes.intSampleYear =  '$sampleYear' AND
					trn_sample_color_recipes.intRevisionNo =  '$revNo' AND
					trn_sample_color_recipes.strCombo =  '$combo' AND
					trn_sample_color_recipes.strPrintName =  '$printName' AND
					trn_sample_color_recipes.intColorId =  '$colorId' AND
					intInkTypeId='$inkType' AND
					intTechniqueId='$techniqueId'
					";
                            $result2 = $db->RunQuery($sql2);
                            $recCount = mysqli_num_rows($result2);
                            if ($recCount > 0) {
                                while ($row2 = mysqli_fetch_array($result2)) {
                                    $weightDetails = array();
                                    $itemId = $row2['intItem'];
                                    $weight = $row2['dblWeight'];
                                    $weightDetails['itemId'] = $itemId;
                                    $weightDetails['weight'] = $weight;
                                    ?>

                                    <tr>
                                        <td align="center" bgcolor="#333333"><img src="../../../../../images/del.png"
                                                                                  width="20" height="20"
                                                                                  class="<?php echo $disabled ?>"/>
                                        <td width="236" bgcolor="#333333"><select class="cboItem"
                                                                                  style="width:150px;font-size:20px"
                                                                                  id="cboItem" name="cboItem"  <?php echo $disabled ?>>
                                                <option value=""></option>
                                                <?php
                                                $sql1 = "SELECT mst_item.strName AS itemName,mst_item.intId	FROM mst_item ORDER BY itemName ASC ";
                                                $result1 = $db->RunQuery($sql1);
                                                while ($row1 = mysqli_fetch_array($result1)) {
                                                    if ($itemId == $row1['intId']) {
                                                        $weightDetails['itemName'] = $row1['itemName'];
                                                        echo "<option selected=\"selected\" value=\"" . $row1['intId'] . "\">" . $row1['itemName'] . "</option>";
                                                    } else
                                                        echo "<option value=\"" . $row1['intId'] . "\">" . $row1['itemName'] . "</option>";
                                                }
                                                ?>
                                            </select></td>
                                        <td width="50" bgcolor="#333333"><input type="text" <?php echo $disabled ?>
                                                                                style="height:50px;width:60px"
                                                                                class="hex txtWeight"
                                                                                aria-haspopup="true" role="textbox"
                                                                                id="txtWeight"
                                                                                value="<?php echo $weight; ?>"/></td>
                                    </tr>


                                    <?php

                                }
                            } else {
                                ?>
                                <tr>
                                    <td align="center" bgcolor="#333333"><img src="../../../../../images/del.png"
                                                                              width="20" height="20" class="delImg" <?php echo $disabled ?>/>
                                    </td>
                                    <td width="236" bgcolor="#333333"><select class="cboItem"
                                                                              style="width:150px;font-size:20px"
                                                                              id="cboItem" name="cboItem" <?php echo $disabled ?>>
                                            <option value=""></option>
                                            <?php
                                            $sql1 = "SELECT mst_item.strName AS itemName,mst_item.intId	FROM mst_item ORDER BY itemName ASC ";
                                            $result1 = $db->RunQuery($sql1);
                                            while ($row1 = mysqli_fetch_array($result1))
                                                echo "<option value=\"" . $row1['intId'] . "\">" . $row1['itemName'] . "</option>";
                                            ?>
                                        </select></td>
                                    <td width="50" bgcolor="#333333"><input type="text" style="height:50px;width:60px"
                                                                            class="hex txtWeight" aria-haspopup="true"
                                                                            id="txtWeight"
                                                                            role="textbox" <?php echo $disabled ?>/></td>
                                </tr>
                                <?php
                            }
                            ?>
                            <tr>
                                <td bgcolor="#333333"><img class="mouseover" id="butAddRow" <?php echo $disabled ?>
                                                           src="../../../../../images/add.png" width="16" height="16"/>
                                </td>
                                <td bgcolor="#333333">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <?php
                    }
                } else {

                    $sql = "SELECT
						trn_sample_color_recipes_bulk.intNumOfShots as intNoOfShots,
						trn_sample_color_recipes_bulk.intColorId,
						trn_sample_color_recipes_bulk.intInkTypeId as intInkType,
						mst_inktypes.strName AS inkTypeName,
						mst_colors.strName AS colorName,
						trn_sample_color_recipes_bulk.intTechniqueId as intTechnique,
						mst_techniques.strName AS technique
					FROM
						trn_sample_color_recipes_bulk
						Inner Join mst_inktypes ON mst_inktypes.intId = trn_sample_color_recipes_bulk.intInkTypeId
						Inner Join mst_colors ON mst_colors.intId = trn_sample_color_recipes_bulk.intColorId
						Inner Join mst_techniques ON mst_techniques.intId = trn_sample_color_recipes_bulk.intTechniqueId
					WHERE
						trn_sample_color_recipes_bulk.intSampleNo 	=  '$sampleNo' AND
						trn_sample_color_recipes_bulk.intSampleYear	=  '$sampleYear' AND
						trn_sample_color_recipes_bulk.intRevisionNo	=  '$revNo' AND
						trn_sample_color_recipes_bulk.strPrintName 	=  '$printName' AND
						trn_sample_color_recipes_bulk.strCombo  	=  '$combo' AND 
						trn_sample_color_recipes_bulk.bulkRevNumber =  '$recipeRevision'
						GROUP BY 
						trn_sample_color_recipes_bulk.intColorId,
						trn_sample_color_recipes_bulk.intInkTypeId,
						trn_sample_color_recipes_bulk.intTechniqueId 
				";

                    $result = $db->RunQuery($sql);
                    while ($row = mysqli_fetch_array($result))
                    {
                    $colorId = $row['intColorId'];
                    $techniqueId = $row['intTechnique'];
                    $inkType = $row['intInkType'];
                    ?>
                <tr class="label" id="dataRow">
                    <td align="center" bgcolor="#333333"><img src="../../../../../images/del.png" width="20" height="20" <?php echo $disabled ?>
                                                              class="delRow"/>
                    <td bgcolor="#333333" align="center"><select class="color" style="width:200px;font-size:20px" id="cboColor" <?php echo $disabled ?>>
                            <?php
                            $sqlColor = "SELECT
                                            trn_sampleinfomations_details_technical.intColorId,
                                            mst_colors.strName AS colorName                                        
                                        FROM
                                            trn_sampleinfomations_details_technical
                                        INNER JOIN mst_colors ON mst_colors.intId = trn_sampleinfomations_details_technical.intColorId
                                        WHERE
                                            trn_sampleinfomations_details_technical.intSampleNo = '$sampleNo'
                                        AND trn_sampleinfomations_details_technical.intSampleYear = '$sampleYear'
                                        AND trn_sampleinfomations_details_technical.intRevNo = '$revNo'
                                        AND trn_sampleinfomations_details_technical.strPrintName = '$printName'
                                        AND trn_sampleinfomations_details_technical.strComboName = '$combo'";
                            $result1 = $db->RunQuery($sqlColor);
                            while ($row1 = mysqli_fetch_array($result1)) {
                                if ($colorId == $row1['intColorId']) {
                                    echo "<option selected=\"selected\" value=\"" . $row1['intColorId'] . "\">" . $row1['colorName'] . "</option>";
                                } else
                                    echo "<option value=\"" . $row1['intColorId'] . "\">" . $row1['colorName'] . "</option>";
                            }
                            ?>
                        </select></td>
                    <td bgcolor="#333333" align="center"><select class="technique" style="width:200px;font-size:20px" id="cboTechnique" <?php echo $disabled ?>>
                            <?php
                            $sqlTeq = "SELECT
                                trn_sampleinfomations_details.intTechniqueId AS intTechnique,
                                mst_techniques.strName AS technique
                            FROM
                                trn_sampleinfomations_details_technical
                            INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_details_technical.intSampleNo
                            AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear
                            AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_details_technical.intRevNo
                            AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_details_technical.strPrintName
                            AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_details_technical.strComboName
                            AND trn_sampleinfomations_details.intColorId = trn_sampleinfomations_details_technical.intColorId
                            INNER JOIN mst_techniques ON mst_techniques.intId = trn_sampleinfomations_details.intTechniqueId
                            WHERE
                                trn_sampleinfomations_details_technical.intSampleNo = '$sampleNo'
                            AND trn_sampleinfomations_details_technical.intSampleYear = '$sampleYear'
                            AND trn_sampleinfomations_details_technical.intRevNo = '$revNo'
                            AND trn_sampleinfomations_details_technical.strPrintName = '$printName'
                            AND trn_sampleinfomations_details_technical.strComboName = '$combo'";
                            $result2 = $db->RunQuery($sqlTeq);
                            while ($row2 = mysqli_fetch_array($result2)) {
                                if ($techniqueId == $row2['intTechnique']) {
                                    echo "<option selected=\"selected\" value=\"" . $row2['intTechnique'] . "\">" . $row2['technique'] . "</option>";
                                } else
                                    echo "<option value=\"" . $row2['intTechnique'] . "\">" . $row2['technique'] . "</option>";
                            }
                            ?>
                        </select></td>
                    <td bgcolor="#333333" align="center"><select class="inkType" style="width:200px;font-size:20px" id="cboInkType" <?php echo $disabled ?>>
                            <?php
                            $sqlInkType = "SELECT
                                                mst_inktypes.intId AS intInkType,
                                                mst_inktypes.strName AS inkTypeName
                                            FROM
                                                mst_inktypes 
                                            WHERE
                                                mst_inktypes.intStatus = 1 AND mst_inktypes.intTechniqueId = '$techniqueId'
                                            ORDER BY
                                                mst_inktypes.strName ASC";
                            $result3 = $db->RunQuery($sqlInkType);
                            while ($row3 = mysqli_fetch_array($result3)) {
                                if ($inkType == $row3['intInkType']) {
                                    echo "<option selected=\"selected\" value=\"" . $row3['intInkType'] . "\">" . $row3['inkTypeName'] . "</option>";
                                } else
                                    echo "<option value=\"" . $row3['intInkType'] . "\">" . $row3['inkTypeName'] . "</option>";
                            }
                            ?>
                        </select></td>
                    <td align="center" bgcolor="#333333">
                        <input type="text" style="height:50px;margin-top:5px;margin-left:10px;margin-right:10px" class="hex txtShots" id="txtShots"
                               aria-haspopup="true" role="textbox" <?php echo $disabled ?> value="<?php echo $row['intNoOfShots']; ?>"/></td>
                    <td bgcolor="#FFFFFF">
                        <table width="287" bgcolor="#333333" cellpadding="0" cellspacing="1" id="tblItem">
                            <?php

                            $sql2 = "SELECT
							trn_sample_color_recipes_bulk.intItem,
							trn_sample_color_recipes_bulk.dblWeight
					FROM trn_sample_color_recipes_bulk
					WHERE
					trn_sample_color_recipes_bulk.intSampleNo =  '$sampleNo' AND
					trn_sample_color_recipes_bulk.intSampleYear =  '$sampleYear' AND
					trn_sample_color_recipes_bulk.intRevisionNo =  '$revNo' AND
					trn_sample_color_recipes_bulk.strCombo =  '$combo' AND
					trn_sample_color_recipes_bulk.strPrintName =  '$printName' AND
					trn_sample_color_recipes_bulk.bulkRevNumber = '$recipeRevision' AND
					trn_sample_color_recipes_bulk.intColorId =  '$colorId' AND
					trn_sample_color_recipes_bulk.intInkTypeId='$inkType' AND
					trn_sample_color_recipes_bulk.intTechniqueId='$techniqueId'
					";
                            $result2 = $db->RunQuery($sql2);
                            $recCount = mysqli_num_rows($result2);
                            if ($recCount > 0) {
                                while ($row2 = mysqli_fetch_array($result2)) {
                                    $weightDetails = array();
                                    $itemId = $row2['intItem'];
                                    $weight = $row2['dblWeight'];
                                    $weightDetails['itemId'] = $itemId;
                                    $weightDetails['weight'] = $weight;
                                    ?>

                                    <tr>
                                        <td align="center" bgcolor="#333333"><img src="../../../../../images/del.png"
                                                                                  width="20" height="20" <?php echo $disabled ?>
                                                                                  class="delImg"/>
                                        <td width="236" bgcolor="#333333"><select class="cboItem"
                                                                                  style="width:150px;font-size:20px"
                                                                                  id="cboItem" name="cboItem" <?php echo $disabled ?>>
                                                <option value=""></option>
                                                <?php
                                                $sql1 = "SELECT mst_item.strName AS itemName,mst_item.intId	FROM mst_item 
                                                INNER JOIN mst_techniques_wise_item ON mst_techniques_wise_item.ITEM_ID= mst_item.intId WHERE mst_item.intStatus = 1 AND mst_techniques_wise_item.TECHNIQUE_ID='$techniqueId' ORDER BY mst_item.strName ASC ";
                                                $result1 = $db->RunQuery($sql1);
                                                while ($row1 = mysqli_fetch_array($result1)) {
                                                    if ($itemId == $row1['intId']) {
                                                        $weightDetails['itemName'] = $row1['itemName'];
                                                        echo "<option selected=\"selected\" value=\"" . $row1['intId'] . "\">" . $row1['itemName'] . "</option>";
                                                    } else
                                                        echo "<option value=\"" . $row1['intId'] . "\">" . $row1['itemName'] . "</option>";
                                                }
                                                ?>
                                            </select></td>
                                        <td width="50" bgcolor="#333333"><input type="text"
                                                                                style="height:50px;width:60px"
                                                                                class="hex txtWeight"
                                                                                aria-haspopup="true" role="textbox"
                                                                                id="txtWeight" <?php echo $disabled ?>
                                                                                value="<?php echo $weight; ?>"/></td>
                                    </tr>


                                    <?php

                                }
                            } else {
                                ?>
                                <tr>
                                    <td align="center" bgcolor="#333333"><img src="../../../../../images/del.png"
                                                                              width="20" height="20" class="delImg" <?php echo $disabled ?>/>
                                    </td>
                                    <td width="236" bgcolor="#333333"><select class="cboItem"
                                                                              style="width:150px;font-size:20px"
                                                                              id="cboItem" name="cboItem" <?php echo $disabled ?>>
                                            <option value=""></option>
                                            <?php
                                            $sql1 = "SELECT mst_item.strName AS itemName,mst_item.intId	FROM mst_item ORDER BY itemName ASC ";
                                            $result1 = $db->RunQuery($sql1);
                                            while ($row1 = mysqli_fetch_array($result1))
                                                echo "<option value=\"" . $row1['intId'] . "\">" . $row1['itemName'] . "</option>";
                                            ?>
                                        </select></td>
                                    <td width="50" bgcolor="#333333"><input type="text" style="height:50px;width:60px"
                                                                            class="hex txtWeight" aria-haspopup="true"
                                                                            id="txtWeight"
                                                                            role="textbox"/></td>
                                </tr>
                                <?php
                            }
                            ?>
                            <tr>
                                <td bgcolor="#333333"><img class="mouseover" id="butAddRow"
                                                           src="../../../../../images/add.png" width="16" height="16" <?php echo $disabled ?>/>
                                </td>
                                <td bgcolor="#333333">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <?php
                    }
                }

              ?>
            </table>
        </td>
    </tr>
        <?php if($recipeRevision != 0 &&  $flag_recipe_status - $approvalLevelsNeeded == 1 ){?>
        <tr>
            <td colspan="7" align="left"><a id="butAdd" class="green button"
                                            style="display:<?php if ($mode != 'last') echo 'none'; ?>"
                                            name="butAdd">Add Row</a>
            </td>
        </tr>
        <?php } ?>
        <tr height="20">
            <td colspan="7"></td>
        </tr>
    </table>
</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <?php if (($flag_recipe_status - $approvalLevelsNeeded == 1) && $recipeRevision != 0){?>
    <tr>
       <td align="center"><a id="butSave" class="orange button"
                                          name="butSave">Save</a></td>
        <td align="center"><a id="butApprove" class="orange button"
                              name="butApprove">Approve</a></td>
    </tr>
    <?php } else if (($recipeRevision == 0 && $flag_bulk_revision_there ==0) || ($flag_recipe_status ==1 && $maxRecipe==$recipeRevision)){?>
    <tr>
        <td colspan="7" align="center"><a id="butSaveNewRevision" class="pink button"
                                          style="display:<?php if ($mode != 'last') echo 'none'; ?>"
                                          name="butSaveNewRevision">Save As a revision</a></td>
    </tr>
    <?php } else if ($flag_recipe_status == -1){ ?>
    <tr>
        <td align="center"><a id="butSave" class="orange button"
                              name="butSave">Save</a></td>
    </tr>
    <?php } ?>

    </table>
</form>
</body>
</html>
<?php
function get_app_permision($userId, $programCode, $field)
{
    global $db;
    $sql = "SELECT
					menupermision.$field as value
				FROM
					menupermision
					Inner Join menus ON menus.intId = menupermision.intMenuId
				WHERE
					menus.strCode 			=  '$programCode' AND
					menupermision.intUserId =  '$userId'
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['value'];
}

function get_ground_colour($sampleNo, $sampleYear, $revNo, $combo, $printName)
{
    global $db;
    $sql = "SELECT
trn_sampleinfomations_details.intGroundColor,
mst_colors_ground.strName
FROM
trn_sampleinfomations_details
INNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
WHERE
trn_sampleinfomations_details.intSampleNo = '$sampleNo' AND
trn_sampleinfomations_details.intSampleYear = '$sampleYear' AND
trn_sampleinfomations_details.intRevNo = '$revNo' AND
trn_sampleinfomations_details.strPrintName = '$printName' AND
trn_sampleinfomations_details.strComboName = '$combo'
limit 1
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['strName'];
}

function getApprovedStatus($sampleNo, $sampleYear, $revisionNo, $combo, $printName, $recipeRevision){
    global $db;
    $sql_approve = "SELECT `trn_sample_color_recipes_approve`.`intApproved` from trn_sample_color_recipes_approve WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revisionNo' and strCombo='$combo' and strPrintName='$printName' and recipeRevision = '$recipeRevision' and intApproved= 1";
    $result = $db->RunQuery($sql_approve);
    return mysqli_num_rows($result);
}

?>

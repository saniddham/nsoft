// JavaScript Document
$(document).ready(function() {

$('.technique').change();
$('#butSearch').live('click',searchDetails);
$('.txtWeight').keydown(function () {
	alert("jooo");
	$(this).focus();
})
$('#cboStyleNo').change(function(){
		
	loadSampleNos();
	
	var intSampleNo = $('#cboSampleNo').val();
	//$('#frmSampleInfomations').get(0).reset();
	$('#cboSampleNo').val(intSampleNo);
	$('#divPicture').html('');
	return;
});
$('#cboSampleNo').change(function(){
		
		loadRevisionNo();
		
		var intSampleNo = $('#cboSampleNo').val();
		//$('#frmSampleInfomations').get(0).reset();
		$('#cboSampleNo').val(intSampleNo);
		$('#divPicture').html('');
		return;
});

$('#cboRevisionNo').change(function(){
	loadCombo();
		
});
$('#cboCombo').change(function(){
	loadPrint();
		
});

    $('#cboPrint').change(function(){
    	loadRecipeRevision();
    });

		//----------------------------
		$('.costPerInch').keyup(function(){
			calTotCostPrice(this);
		});
		//----------------------------
		$('.usage').keyup(function(){
			calTotCostPrice(this);
		});
		//----------------------------
		$('.technique').change(function(){
			loadInksAndItems(this);
		});
		//----------------------------
		$('.foil').change(function(){
			techniqueCalculations(this);
		});
		//----------------------------
		$('.printWidth').keyup(function(){
			techniqueCalculations(this);
		});
		//----------------------------
		$('.printHeight').keyup(function(){
			techniqueCalculations(this);
		});
		//----------------------------
		$('.qty').keyup(function(){
			techniqueCalculations(this);
		});

		$('.cboItem').hover(function(){
			showItemName(this);
		});
		//----------------------------
		$('.delImg').live('click',function(){
			var rowCount1 	= $(this).closest('table').find('tr').length
            if(rowCount1<3)
            {
                alert("Can't delete.Atleast one item should be selected");
                return false;
            }
            else {
                $(this).closest('tr').remove();
            }
		});

    $('.delRow').live('click',function(){

        var rowCount1 	= document.getElementById('tblInk').rows.length;
        if(rowCount1<3)
        {
            alert("Can't delete.Atleast one item should be selected");
            return false;
        }
        else {
            $(this).closest('tr').remove();
        }
    });
		//----------------------------

function loadRevisionNo()
{
	var url = "colorRecipes-db-get.php?requestType=loadRevisionNo&sampleNo="+$('#cboSampleNo').val()+"&sampleYear="+$('#cboYear').val();
	var obj = $.ajax({url:url,async:false});	
	$('#cboRevisionNo').html(obj.responseText);
	$('#cboCombo').html('');	
	$('#cboPrint').html('');	
}
function loadSampleNos()
{
	var url = "colorRecipes-db-get.php?requestType=loadSampleNo&styleNo="+URLEncode($('#cboStyleNo').val())+"&sampleYear="+$('#cboYear').val();
	var obj = $.ajax({url:url,async:false});	
	$('#cboSampleNo').html(obj.responseText);	
	$('#cboRevisionNo').html('');	
	$('#cboCombo').html('');	
	$('#cboPrint').html('');	
	
}
function loadCombo()
{
	var url = "colorRecipes-db-get.php?requestType=loadCombo&sampleNo="+($('.txtSampleNo').val())+"&sampleYear="+$('#cboSampleYear').val()+"&revNo="+$('#cboRevisionNo').val();
	var obj = $.ajax({url:url,async:false});	
	$('#cboCombo').html(obj.responseText);	
	$('#cboPrint').html('');
}
function loadPrint()
{
	var url = "colorRecipes-db-get.php?requestType=loadPrint&sampleNo="+($('.txtSampleNo').val())+"&sampleYear="+$('#cboSampleYear').val()+"&revNo="+$('#cboRevisionNo').val()+"&combo="+encodeURIComponent($('#cboCombo').val());
	var obj = $.ajax({url:url,async:false});	
	$('#cboPrint').html(obj.responseText);	
}

function loadRecipeRevision()
{
	var url = "colorRecipes-db-get.php?requestType=loadRecipeRevision&sampleNo="+($('.txtSampleNo').val())+"&sampleYear="+$('#cboSampleYear').val()+"&revNo="+$('#cboRevisionNo').val()+"&combo="+encodeURIComponent($('#cboCombo').val())+'&printName='+$('#cboPrint').val();
	var obj = $.ajax({url:url,async:false});
	$('#cboRecipeRevision').html(obj.responseText);
}
	
	
/*data load part*/
$('#cboRecipeRevision').change(function(){
	window.location.href = "index.php?sampleNo="+$('.txtSampleNo').val()
						+'&sampleYear='+$('#cboSampleYear').val()
						+'&revNo='+$('#cboRevisionNo').val()
						+'&combo='+encodeURIComponent($('#cboCombo').val())
						+'&printName='+$('#cboPrint').val()
						+ '&recipeRevision='+$('#cboRecipeRevision').val()
});

$('#butAddRow').live('click',function(){
	$(this).parent().parent().before('<tr>'+$(this).parent().parent().parent().find('tr:eq(0)').html()+'</tr>');
	$('.txtWeight').keyboard();
});

$('#butAdd').die('click').live('click',function () {
    var rowCount = document.getElementById('tblInk').rows.length;
    document.getElementById('tblInk').insertRow(rowCount);
    rowCount = document.getElementById('tblInk').rows.length;
    document.getElementById('tblInk').rows[rowCount-1].innerHTML = document.getElementById('tblInk').rows[rowCount-2].innerHTML;
    document.getElementById('tblInk').rows[rowCount-1].className="label";
    document.getElementById('tblInk').rows[rowCount-1].id = 'dataRow';
    $('.txtWeight').keyboard();
});
	
//----------------------------
	
//if ($('#frmSampleInfomations').validationEngine('validate'))   
//---------------------------------------------------------------------------
$('#butSave').live('click',function(){
	var arrRecipe = '';
	var arrFoil = '';
	var arrSpecial = '';
	var data='';
	var i =0;
	
	var sampleNo 		= 	$('#cboSampleNo').val();
	var sampleYear 		= 	$('#cboSampleYear').val();
	var revisionNo 		= 	$('#txtRevNo').text();
	var combo 			=	$('#txtCombo').text();
	var printName 		=	$('#txtPrintName').text();
	var recipeRevision  =   $('#txtRecipeRevision').text();

	$('.cboItem').each(function(){
		var itemId 		= $(this).val();
		
		if(itemId!='')
		{
			var colorId 	= $(this).closest('#dataRow').find('.color').val();
			var techniqueId = $(this).closest('#dataRow').find('.technique').val();
			var inkTypeId 	= $(this).closest('#dataRow').find('.inkType').val();
			var weight 		= $(this).parent().parent().find('#txtWeight').val();
			var noOfShots 	= $(this).closest('#dataRow').find('#txtShots').val();
			arrRecipe +=(i++ == 0?'':',' )+'{"color":"'+colorId+'","techId":"'+techniqueId+'","inkTypeId":"'+inkTypeId+'","weight":"'+weight+'","sampleNo":"'+sampleNo+'","sampleYear":"'+sampleYear+'","revisionNo":"'+revisionNo+'","combo":"'+combo+'","printName":"'+printName+'","itemId":"'+itemId+'","noOfShots":"'+noOfShots+'","recipeRevision":"'+recipeRevision+'"}';
		}
	});
	
	//alert(arrRecipe);
		// SAVE AJAX PART //
		data  = 'arrRecipe=['+ arrRecipe +']';
		console.log(data);
		
		var url 	= "colorRecipes-db-set.php?requestType=saveDetails";
		$.ajax({
			type: "POST",
			url:url,
			data:data,
			async:false,
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmColorRecipieRevision #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
                       window.location.href="index.php?sampleNo="+sampleNo+ "&sampleYear="+sampleYear+"&revNo="+revisionNo+"&combo="+URLEncode(combo)+"&printName="+ URLEncode(printName)+"&recipeRevision="+recipeRevision;
					}
				},
			error:function(xhr,status){
					
					$('#frmColorRecipieRevision #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
   });

    $('#butSaveNewRevision').live('click',function(){
        var arrRecipe = '';
        var arrFoil = '';
        var arrSpecial = '';
        var data='';
        var i =0;

        var sampleNo 		= 	$('#cboSampleNo').val();
        var sampleYear 		= 	$('#cboSampleYear').val();
        var revisionNo 		= 	$('#txtRevNo').text();
        var combo 			=	$('#txtCombo').text();
        var printName 		=	$('#txtPrintName').text();

        $('.cboItem').each(function(){
            var itemId 		= $(this).val();

            if(itemId!='')
            {
                var colorId 	= $(this).closest('#dataRow').find('.color').val();
                var techniqueId = $(this).closest('#dataRow').find('.technique').val();
                var inkTypeId 	= $(this).closest('#dataRow').find('.inkType').val();
				var shots 	= $(this).closest('#dataRow').find('.txtShots').val();
                var weight 		= $(this).parent().parent().find('#txtWeight').val();
                arrRecipe +=(i++ == 0?'':',' )+'{"color":"'+colorId+'","techId":"'+techniqueId+'","inkTypeId":"'+inkTypeId+'","weight":"'+weight+'","sampleNo":"'+sampleNo+'","sampleYear":"'+sampleYear+'","revisionNo":"'+revisionNo+'","combo":"'+combo+'","printName":"'+printName+'","shots":"'+shots+'","itemId":"'+itemId+'"}';
            }
        });

        //alert(arrRecipe);
        // SAVE AJAX PART //
        data  = 'arrRecipe=['+ arrRecipe +']';

        var url 	= "colorRecipes-db-set.php?requestType=saveNewRevision";
        $.ajax({
            type: "POST",
            url:url,
            data:data,
            async:false,
            dataType: "json",
            data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
            //data:'{"requestType":"addsampleInfomations"}',
            async:false,

            success:function(json){
                $('#msg_parent').html(json.msg);
                $('#frmColorRecipieRevision #butSaveNewRevision').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                if(json.type=='pass')
                {
                	var revision = json.revision;
					window.location.href="index.php?sampleNo="+sampleNo+ "&sampleYear="+sampleYear+"&revNo="+revisionNo+"&combo="+URLEncode(combo)+"&printName="+ URLEncode(printName)+"&recipeRevision="+ revision;
                }
            },
            error:function(xhr,status){

                $('#frmColorRecipieRevision #butSaveNewRevision').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                var t=setTimeout("alertx()",3000);
            }
        });
    });

$('#butApprove').live('click',function(){
	var arrInk = '';
	var data='';
	var i =0;
	
	var sampleNo 		= 	$('.txtSampleNo').val();
	var sampleYear 		= 	$('#cboSampleYear').val();
	var revisionNo 		= 	$('#txtRevNo').text();
	var combo 			=	$('#txtCombo').text();
	var printName 		=	$('#txtPrintName').text();
	var recipeRevision  =   $('#txtRecipeRevision').text();
	
	data += 'sampleNo='+ sampleNo;
	data += '&sampleYear='+ sampleYear;
	data += '&revisionNo='+ revisionNo;
	data += '&combo='+ combo;
	data += '&printName='+ printName;
	data += '&recipeRevision='+ recipeRevision;

	$('.color').each(function(){
		
			var colorId 	= $(this).parent().find('.color').attr('id');
			var techniqueId = $(this).parent().find('.technique').attr('id');
			var inkTypeId 	= $(this).parent().find('.inkType').attr('id');
			arrInk +=(i++ == 0?'':',' )+'{"color":"'+colorId+'","techId":"'+techniqueId+'","inkTypeId":"'+inkTypeId+'","sampleNo":"'+sampleNo+'","sampleYear":"'+sampleYear+'","revisionNo":"'+revisionNo+'","combo":"'+combo+'","printName":"'+printName+'"}';
	});
	
	//alert(arrRecipe);
		// SAVE AJAX PART //
		data  += '&arrInk=['+ arrInk +']';
		
		var url 	= "colorRecipes-db-set.php?requestType=approveRecipForBulk";
		$.ajax({
			type: "POST",
			url:url,
			data:data,
			async:false,
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmColorRecipieRevision #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass'){
						window.location.href="index.php?sampleNo="+sampleNo+ "&sampleYear="+sampleYear+"&revNo="+revisionNo+"&combo="+URLEncode(combo)+"&printName="+ URLEncode(printName)+"&recipeRevision="+ recipeRevision;
					}
				},
			error:function(xhr,status){
					
					
				}		
			});
   });
   //-----------------------------------------------------
$('#butCancel').live('click',function(){
	var arrInk = '';
	var data='';
	var i =0;
	
	var sampleNo 		= 	$('.txtSampleNo').val();
	var sampleYear 		= 	$('#cboSampleYear').val();
	var revisionNo 		= 	$('#txtRevNo').text();
	var combo 			=	$('#txtCombo').text();
	var printName 		=	$('#txtPrintName').text();
	
	data += 'sampleNo='+ sampleNo;
	data += '&sampleYear='+ sampleYear;
	data += '&revisionNo='+ revisionNo;
	data += '&combo='+ combo;
	data += '&printName='+ printName;

		
		var url 	= "colorRecipes-db-set.php?requestType=cancelApprovedRecipForBulk";
		$.ajax({
			url:url,
			data:data,
			async:false,
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#msg_parent').html(json.msg);
					if(msg.type=='pass'){
						/*$('#but_parent').html('<a id="butApprove" class="green button " style="height:16px;margin:2px 2px 2px 2px;">Approval For Bulk</a>');*/
					}
				},
			error:function(xhr,status){
					
					
				}		
			});
   });
   
   //---------------------------------------------------
	$('#butNew').click(function(){
		window.location.href = 'colorRecipes.php';
	});

});

//----------------------------------------------------------------------
function calTotCostPrice(obj)
{
		var printWidth = document.getElementById('txtPrintWidth').value;
		var printHeight = document.getElementById('txtPrintHeight').value;
	 	var costPerInch = $(obj).parent().parent().find('.costPerInch').val();
	 	var usage = $(obj).parent().parent().find('.usage').val();
	//	var totCost=qty*costPerInch*usage/100;
		var totCost=printWidth*printHeight*costPerInch*usage/100;
		//alert(totCost);
		$(obj).parent().parent().find('.totCost').text(totCost);
	
}
//----------------------------------------------------------------------
function loadItem(obj)
{
	 	var technique = $(obj).parent().parent().find('.technique').val();
	 	var sampleNo =$('#cboSampleNo').val();
	 	var sampleYear = $('#cboYear').val();
	 	var revNo = $('#cboRevisionNo').val();
	 	var combo = $('#cboCombo').val();
	 	var printName = $('#cboPrint').val();
		var url 		= "colorRecipes-db-get.php?requestType=loadItem";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"technique="+technique+"&sampleNo="+sampleNo+"&revNo="+revNo+"&combo="+combo+"&sampleYear="+sampleYear+"&printName="+printName,
			async:false,
			success:function(json){
				var itemId=json.itemId;
				 $(obj).parent().parent().find('.foil').val(json.itemId)
				 //	if(itemId){	
					  $(obj).parent().parent().find('.foil').change();
				//	}
			}
		});
}
//----------------------------------------------------------------------
function techniqueCalculations(obj)
{
	 	var foil = $(obj).parent().parent().find('.foil').val();
		var url 		= "colorRecipes-db-get.php?requestType=loadMeasurements";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"foil="+foil,
			async:false,
			success:function(json){
				foilWidth=json.width;
				foilHeight=json.height;
				foilcostPerMtr=json.price;
			}
		});
	
		var printWidth = $(obj).parent().parent().find('.printWidth').val();
		var printHeight = $(obj).parent().parent().find('.printHeight').val();
	 	var qty = $(obj).parent().parent().find('.qty').val();
		
		//------------------------------
		var pcs=Math.floor((foilHeight/printWidth))
		if(printWidth==0){
			pcs=0;
		}
		//alert(printWidth);
		$(obj).parent().parent().find('.pcsFrmHeight').text((foilHeight/printWidth).toFixed(4));
		$(obj).parent().parent().find('.pcs').text(pcs);
		var mtrs=((printHeight/pcs)*qty)/39.5;
		if(pcs==0){
			mtrs=0;
		}
		$(obj).parent().parent().find('.meters').text(mtrs.toFixed(4));
		$(obj).parent().parent().find('.cost').text((mtrs*foilcostPerMtr).toFixed(4));
		
		//-------------------
		var pcs=Math.floor((foilHeight/printHeight));
		if(printWidth==0){
			pcs=0;
		}
		$(obj).parent().parent().next().find('.pcsFrmHeight').text((foilHeight/printHeight).toFixed(4));
		$(obj).parent().parent().next().find('.pcs').text(pcs);
		var mtrs=((printWidth/pcs)*qty)/39.5;
		if(pcs==0){
			mtrs=0;
		}
		$(obj).parent().parent().next().find('.meters').text(mtrs.toFixed(4));
		$(obj).parent().parent().next().find('.cost').text((mtrs*foilcostPerMtr).toFixed(4));
}
//----------------------------------------------------------------------

function showItemName(obj) {
	var itemNameToShow = $(obj).parent().find('#cboItem option:selected').text();
	$(obj).parent().find('#cboItem').prop('title',itemNameToShow);
}
function alertx()
{
	$('#frmColorRecipieRevision #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmColorRecipieRevision #butDelete').validationEngine('hide')	;
}

function searchDetails()
{
	var sampleYear 	= $('#cboSampleYear').val();
	var sampleNo 	= $('#cboSampleNo').val();
	document.location.href = 'index.php?sampleYear='+sampleYear+'&sampleNo='+sampleNo;
}
//----------------------------------------------

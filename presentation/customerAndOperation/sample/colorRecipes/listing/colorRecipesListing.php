<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
date_default_timezone_set('Asia/Kolkata');
ini_set('allow_url_include',1);
//ini_set('display_errors',1);
$thisFilePath 	=  $_SERVER['PHP_SELF'];
 
$reportMenuId	='988';
$menuId			='433';
 
require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";

################################################################
################## new part ####################################
################################################################
///// create where part ///////////////////
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'Sample_No'=>'trn_sample_color_recipes.intSampleNo',
				'Sample_Year'=>'trn_sample_color_recipes.intSampleYear',
				'Revision_No'=>'trn_sample_color_recipes.intRevisionNo',
				'Combo'=>'trn_sample_color_recipes.strCombo',
				'Print_Name'=>'trn_sample_color_recipes.strPrintName',
				'Graphic_No'=>'trn_sampleinfomations.strGraphicRefNo',
				'Style_No'=>'trn_sampleinfomations.strStyleNo',
				'Customer_Name'=>'mst_customer.strName'
				);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
/*
if(!count($arr)>0)					 
	$where_string .= "AND substr(tb1.datdate,1,10) = '".date('Y-m-d')."'";*/
	
################## end code ####################################

$sql = "select * from(SELECT DISTINCT
							trn_sample_color_recipes.intSampleNo as Sample_No,
							trn_sample_color_recipes.intSampleYear as Sample_Year,
							trn_sample_color_recipes.intRevisionNo as Revision_No,
							trn_sample_color_recipes.strCombo as Combo ,
							trn_sample_color_recipes.strPrintName as Print_Name,
							trn_sampleinfomations.strGraphicRefNo as Graphic_No,
							
							trn_sampleinfomations.strStyleNo as Style_No,
							mst_customer.strName as Customer_Name  ,
							mst_part.strName  as Part_Name,
							'Report'
						FROM
						trn_sample_color_recipes
							Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sample_color_recipes.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sample_color_recipes.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_sample_color_recipes.intRevisionNo
							Inner Join mst_customer ON mst_customer.intId = trn_sampleinfomations.intCustomer
							Inner Join trn_sampleinfomations_printsize ON trn_sampleinfomations_printsize.intSampleNo = trn_sample_color_recipes.intSampleNo AND trn_sampleinfomations_printsize.intSampleYear = trn_sample_color_recipes.intSampleYear AND trn_sampleinfomations_printsize.intRevisionNo = trn_sample_color_recipes.intRevisionNo AND trn_sampleinfomations_printsize.strPrintName = trn_sample_color_recipes.strPrintName
							Inner Join mst_part ON mst_part.intId = trn_sampleinfomations_printsize.intPart
							$where_string
							) as t
						";
						
						
$formLink			= "?q=$menuId&graphicNo=&sampleNo={Sample_No}&sampleYear={Sample_Year}&revNo={Revision_No}&combo={Combo}&printName={Print_Name}";	 
$reportLink  		= "?q=$reportMenuId&no={Sample_No}&year={Sample_Year}&revNo={Revision_No}";

$col = array();
//SAMPLE NO
$col["title"] 	= "Sample No"; // caption of column
$col["name"] 	= "Sample_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "5";
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= $formLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

 
$cols[] = $col;	$col=NULL;
//SAMPLE YEAR
$col["title"] = "Year"; // caption of column
$col["name"] = "Sample_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;
//REVISION NO
$col["title"] = "Rev No"; // caption of column
$col["name"] = "Revision_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;
//COMBO
$col["title"] = "Combo"; // caption of column
$col["name"] = "Combo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$cols[] = $col;	$col=NULL;
//PRINT NAME
$col["title"] = "Print Name"; // caption of column
$col["name"] = "Print_Name"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$cols[] = $col;	$col=NULL;
//GRAPHIC NO
$col["title"] = "Graphic No"; // caption of column
$col["name"] = "Graphic_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "8";
$cols[] = $col;	$col=NULL;
//STYLE NO
$col["title"] = "Style No"; // caption of column
$col["name"] = "Style_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "8";
$cols[] = $col;	$col=NULL;
//CUSTOMER NAME
$col["title"] = "Customer"; // caption of column
$col["name"] = "Customer_Name"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "20";
$cols[] = $col;	$col=NULL;
//PART NAME
$col["title"] = "Part"; // caption of column
$col["name"] = "Part_Name"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "8";
$cols[] = $col;	$col=NULL;
//Report
$col["title"] = "Report"; // caption of column
$col["name"] = "Report"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Colour Recipes Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'Sample_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);



$out = $jq->render("list1");
?>
<title>Colour Recipes Listing</title>
 	<?php 
	//	include "include/listing.html";
	?>
 <form id="frmlisting" name="frmlisting" method="post" action="">

        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
</form>
 
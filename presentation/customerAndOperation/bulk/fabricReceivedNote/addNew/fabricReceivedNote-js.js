var exceedPOPermision	= false;
var excessFactor 		= 0;
var basepath			= 'presentation/customerAndOperation/bulk/fabricReceivedNote/addNew/';

$(document).ready(function(){	
	$("#frmReceivedNote").validationEngine();
	
	loadExcessFactor = getFRNexcessFactor();

	$("#frmReceivedNote #cboStyle").die('change').live('change',function(){
	 	$('#frmReceivedNote #cboSalesOrderNo').val('');
	 	$('#cboGraphicNo').val('');
	});

	$("#frmReceivedNote #cboSalesOrderNo").die('change').live('change',function(){
		if($("#frmReceivedNote #cboOrderNo").val()=='')
		loadOrderNos();
	});

	$("#frmReceivedNote #cboOrderNo").die('change').live('change',function(){
	 	$('#cboPONo').val('');
	 	$('#cboCustomer').val('');
	 	$('#frmReceivedNote #cboSalesOrderNo').val('');
		submitForm();
	});

	$('.qty').die('keyup').live('keyup',function(){
		calTotals();
	});	

	$("#frmReceivedNote #cboOrderYear").die('change').live('change',function(){
		 $('#cboGraphicNo').val('');
		 $('#cboStyle').val('');
		 $('#cboPONo').val('');
		 $('#cboOrderNo').val('');
		 $('#cboCustomer').val('');
		 $('#frmReceivedNote #cboSalesOrderNo').val('');
		 clearGrid();
		 loadAllCombo();
	});

	$("#frmReceivedNote .search").die('change').live('change',loadAllCombo);
		
	$('.delImg').die('click').live('click',function(){
		$(this).parent().parent().remove();
			calTotals();
	});

	$('#frmReceivedNote #butInsertRowPopup').die('click').live('click',function(){
		var row=this.parentNode.parentNode.rowIndex;
		var orderNo= $('#cboOrderNo').val();
		var orderYear= $('#cboOrderYear').val();
		var salesOrderNo = $('#frmReceivedNote #cboSalesOrderNo').val();
		var serialNo=$('#txtSerialNo').val();
		var year=$('#txtYear').val();
		var styleNo= $('#cboStyle').val();
		var graphicNo= $('#cboGraphicNo').val();
		var custPONo= $('#cboPONo').val();
		var locationType = $('#locationType').val();
		if(orderNo==''){
			alert("Please select the Order No.");
			return false;
		}
		closePopUp();
		popupWindow3('1');
		$('#popupContact1').load(basepath+'fabricReceivedNotePopup.php?orderNo='+orderNo+'&orderYear='+orderYear+'&locationType='+locationType+'&serialNo='+serialNo+'&year='+year+'&styleNo='+URLEncode(styleNo)+'&graphicNo='+URLEncode(graphicNo)+'&custPONo='+URLEncode(custPONo)+'&salesOrderNo='+URLEncode(salesOrderNo),function(){
				$('#frmFabricRecvNotePopup #butAdd').live('click', addClickedRows);
				$('#frmFabricRecvNotePopup #butClose1').live('click', disablePopup);
				$(".searchP").die('change').live('change',function(){
					loadSalesLineNoPartSizesComboes();
				});
				$("#chkAll").die('click').live('click',function(){
					checkUncheckRows(this);
				});
				$(".cutNoP").die('keyup').live('keyup',function(){
					uncheckAll();
					checkAlreadySelected();
				});

				  $('#frmFabricRecvNotePopup #imgSearchItems').die('click').live('click',function(){
					  	var cutNoP=$('#txtCutNo').val();
						if(cutNoP==''){
							alert("Please select the Cut No");
							return false;
						}
						var rowCount = document.getElementById('tblPopup').rows.length;
						for(var i=1;i<rowCount;i++)
						{
							document.getElementById('tblPopup').deleteRow(1);
						}
						var orderYear = $('#cboOrderYear').val();
						var orderNo = $('#cboOrderNo').val();
						var styleNo = $('#cboStyleP').val();
						var salesOrderId = $('#frmReceivedNote #cboSalesOrderNo').val();
						if(salesOrderId=='')
						var salesOrderId = $('#cboSalesOrderNoP').val();
						
						var lineNo = $('#cboLineNo').val();
						var part = $('#cboPartNo').val();
						var size = $('#cboSizes').val();
						var color = $('#cboColor').val();
					    var locationType = $('#locationType').val();
						
						var url 		= basepath+"fabricReceivedNote-db-get.php?requestType=loadPartDetails";
						
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							data:"orderNo="+orderNo+"&orderYear="+orderYear+"&locationType="+locationType+"&styleNo="+URLEncode(styleNo)+"&salesOrderId="+URLEncode(salesOrderId)+"&lineNo="+URLEncode(lineNo)+"&part="+part+"&size="+URLEncode(size)+"&color="+URLEncode(color),
							async:false,
							success:function(json){

								var length = json.arrCombo.length;
								var arrCombo = json.arrCombo;

								for(var i=0;i<length;i++)
								{
									var salesOrderId=arrCombo[i]['salesOrderId'];	
									var salesOrderNo=arrCombo[i]['salesOrderNo'];	
									var partId=arrCombo[i]['partId'];	
									var part=arrCombo[i]['part'];	
									var groundColorId=arrCombo[i]['groundColorId'];	
									var groundColor=arrCombo[i]['groundColor'];	
									var lineNo=arrCombo[i]['lineNo'];
									var size=arrCombo[i]['size'];	
									exceedPOPermision=arrCombo[i]['exceedPOPermision'];	
									var qty=parseFloat(arrCombo[i]['qty']);	
									var fdQty=parseFloat(arrCombo[i]['FDQty']);
									var recvQty=parseFloat(arrCombo[i]['RecvQty']);
									var confQty=parseFloat(arrCombo[i]['confQty']);
									var excessQty=parseFloat(arrCombo[i]['ExcessQty']);
									var maxQty=parseFloat(qty-recvQty-confQty+excessQty+fdQty);
									maxQty=Math.ceil(maxQty);
									
									if(maxQty<0){
										maxQty=0;
									}

									var content='<tr class="normalfnt">';
									content +='<td align="center" bgcolor="#FFFFFF" id=""><input type="checkbox" id="chkDisp" class="chk"/></td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderId+'" class="salesOrderNoP">'+salesOrderNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+partId+'" class="partP">'+part+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+groundColorId+'" class="bgColorP">'+groundColor+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+lineNo+'" class="lineP">'+lineNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+size+'" class="sizeP">'+size+'</td>';
									if(exceedPOPermision==false){
									content +='<td align="center" bgcolor="#FFFFFF" id="qtyP"><input  id="'+maxQty+'" class="validate[required,custom[integer],max['+maxQty+']] qtyP" style="width:80px;text-align:center" type="text" value="'+maxQty+'"/></td>';
									}
										else
									{
									content +='<td align="center" bgcolor="#FFFFFF" id="qtyP"><input  id="'+maxQty+'" class="validate[required,custom[integer]] qtyP" style="width:80px;text-align:center" type="text" value="'+maxQty+'"/></td>';
									}
									
									content +='<td align="center" bgcolor="#FFFFFF" id="'+maxQty+'" class="maxQty">'+maxQty+'</td>';
									content +='</tr>';
									add_new_row('#frmFabricRecvNotePopup #tblPopup',content);
								}
								
									checkAlreadySelected();
							}
							
						});
						
				  });
			});	
	});
	
	$('#frmFabricRecvNotePopup #butAddSizes').die('click').live('click',function() {
		
		var orderNo= $('#cboOrderNo').val();
		var orderYear= $('#cboOrderYear').val();
		var serialNo=$('#txtSerialNo').val();
		var year=$('#txtYear').val();
		var styleNo= $('#cboStyle').val();
		var graphicNo= $('#cboGraphicNo').val();
		var custPONo= $('#cboPONo').val();
		var salesOrderNo= $('#frmReceivedNote #cboSalesOrderNo').val();
		if(salesOrderNo=='')
			var salesOrderNo = $('#cboSalesOrderNoP').val();
		
		if(salesOrderNo==''){
			alert("Please select Sales Order No to add sizes");
			return false;
		}
		
		showWaiting();
		$('#divBackgroundImg').css('left','300px');
		$('#divBackgroundImg').css('top','100px');
		$('#divBackgroundImg').load(basepath+'fabricReceivedNoteSizesPopup.php?orderNo='+orderNo+'&orderYear='+orderYear+'&styleNo='+URLEncode(styleNo)+'&graphicNo='+URLEncode(graphicNo)+'&custPONo='+URLEncode(custPONo)+'&salesOrderNo='+URLEncode(salesOrderNo),function(){
				$('#frmFabricReceivedNoteSizesPopup #butAdd').die('click').live('click', addClickedRows);
				$('#frmFabricReceivedNoteSizesPopup #butClose2').die('click').live('click', hideWaiting);
		});
	});

	$("#frmFabricRecvNotePopup #cboSalesOrderNoP").die('change').live('change',function(){
		loadSizes();
		loadOrderQty();
	});

	$("#frmFabricReceivedNoteSizesPopup #cboSalesOrderNoPP").die('change').live('change',function(){
		var rowCount = document.getElementById('tblSizesPopup').rows.length;
		for(var i=1;i<rowCount;i++)
		{
			document.getElementById('tblSizesPopup').deleteRow(1);
		}
		var orderYear = $('#cboOrderYear').val();
		var orderNo = $('#cboOrderNo').val();
		var salesOrderId = $('#cboSalesOrderNoPP').val();
		
		
		var url 		= basepath+"fabricReceivedNote-db-get.php?requestType=loadSalesOrderQtyDetails";
		
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"orderNo="+orderNo+"&orderYear="+orderYear+"&salesOrderId="+salesOrderId, 
			async:false,
			success:function(json){
	
				var length = json.arrCombo.length;
				var arrCombo = json.arrCombo;
	
				for(var i=0;i<length;i++)
				{
					var size=arrCombo[i]['size'];
					var salesOrderQty=parseFloat(arrCombo[i]['salesOrderQty']);	
					var sizeQty=parseFloat(arrCombo[i]['sizeQty']);	
					var minQty=parseFloat(arrCombo[i]['minQty']);
					var recvQty=parseFloat(arrCombo[i]['recvQty']);
					
					if(sizeQty<0){
						sizeQty=0;
					}
					if(minQty<0){
						minQty=0;
					}
					if(recvQty<0){
						recvQty=0;
					}
	
					var content='<tr class="normalfnt">';
	
					if(recvQty==0){
						content +='<td align="center" bgcolor="#FFFFFF" id=""><img src="images/del.png" width="15" height="15" class="delImgPopup"/></td>';
					}
					else{
						content +='<td align="center" bgcolor="#FFFFFF" id=""></td>';
					}
					
					if(recvQty >0 ){																										 
						content +='<td align="center" bgcolor="#FFFFFF" id=""><input name="txtSize" type="text" id="txtSize" style="width:60px; text-align:center" value="'+size+'" class="size" disabled="disabled" /></td>';
					}
					else{
						content +='<td align="center" bgcolor="#FFFFFF" id=""><input name="txtSize" type="text" id="txtSize" style="width:60px; text-align:center" value="'+size+'" class="size" /></td>';
					}
					content +='<td align="center" bgcolor="#FFFFFF" id=""><input  name="txtSizeQty" type="text" id="txtSizeQty" style="width:60px;text-align:right" value="'+sizeQty+'"  class="validate[required,custom[number],max['+salesOrderQty+'],min['+minQty +']] sizeQty" /></td>';
					
					
					content +='<td align="center" bgcolor="#FFFFFF" id="">'+recvQty+'</td>';
					content +='</tr>';
					
					add_new_row('#frmFabricReceivedNoteSizesPopup #tblSizesPopup',content);
				}
				
				$('#divOrderQty').html(salesOrderQty);				
				var content1='<tr>';
				content1 +='<td colspan="4" align="left"  bgcolor="#FFFFFF" ><img src="images/Tadd.jpg" name="butInsertRowSizesPopup" width="78" height="24" class="mouseover" id="butInsertRowSizesPopup" /></td>';
				content1 +='</tr>';
				add_new_row('#frmFabricReceivedNoteSizesPopup #tblSizesPopup',content1);
			}			
		});
	});

	$('#frmFabricReceivedNoteSizesPopup #butInsertRowSizesPopup').die('click').live('click',function() {
		var rowCount = document.getElementById('tblSizesPopup').rows.length;
		document.getElementById('tblSizesPopup').insertRow(rowCount-1);
		rowCount = document.getElementById('tblSizesPopup').rows.length;
		//document.getElementById('tblSizesPopup').rows[rowCount-2].innerHTML = document.getElementById('tblSizesPopup').rows[rowCount-3].innerHTML;
		
		var contentS	='<td bgcolor="#FFFFFF" align="center"><img class="delImgPopup" width="15" height="15" src="images/del.png"></td>';
		contentS		+='<td id="" bgcolor="#FFFFFF" align="center"><input id="txtSize" class="size" type="text" value="" style="width:60px; text-align:center" name="txtSize"></td>';
		contentS		+='<td id="" bgcolor="#FFFFFF" align="center"><input id="txtSizeQty" class="validate[required,custom[number],min[]] sizeQty" type="text" value="0" style="width:60px; text-align:right" name="txtSizeQty"></td>';
		contentS		+='<td id="" bgcolor="#FFFFFF" align="right">0</td>';
		document.getElementById('tblSizesPopup').rows[rowCount-2].innerHTML = contentS;
		
		document.getElementById('tblSizesPopup').rows[rowCount-2].className="normalfnt";
		//document.getElementById('tblSizesPopup').rows[rowCount-2].cells[1].childNodes[0].value='';
		//document.getElementById('tblSizesPopup').rows[rowCount-2].cells[2].childNodes[0].value=0;
		//document.getElementById('tblSizesPopup').rows[rowCount-2].cells[2].childNodes[0].className = "validate[required,custom[number],min[0]] sizeQty";
	});

	$('#frmFabricReceivedNoteSizesPopup .delImgPopup').die('click').live('click',function(){
		var rowCount1 = document.getElementById('tblSizesPopup').rows.length;
		if(rowCount1>3)
		$(this).parent().parent().remove();
		//this.parentNode.parentNode.deleteRow(this.parentNode.parentNode.rowIndex);
		calTotals();
	});
   $('#frmReceivedNote #butSave').die('click').live('click',function(){
	var requestType = '';
	strGraphic	=	$('#cboGraphicNo').val();

	if(strGraphic=="")
	{
		alert("Graphic number is not selected");
	}

	var salesOrderNo	= $('#cboSalesOrderNo').val();
	if(salesOrderNo=="")
	{
		alert("sales order number is not selected");
	}

	if ($('#frmReceivedNote').validationEngine('validate'))
    { 
		showWaiting();
		var data = "requestType=save";		
			data+="&serialNo="+$('#txtSerialNo').val();
			data+="&Year="+$('#txtYear').val();
			data+="&orderNo="+$('#cboOrderNo').val();
			data+="&orderYear="+$('#cboOrderYear').val();
                        data+="&customer="+$('#cboCustomer').val();
			data+="&strStyle="+$('#cboStyle').val(); 
                        data+="&cboPONo="+$('#cboPONo').val();
			data+="&strGraphic="+$('#cboGraphicNo').val();// suvini nsoft3.12.4
			data+="&AODNo="+URLEncode($('#txtAODNo').val());
			data+="&AODDate="+$('#dtDate').val();
			data+="&remarks="+URLEncode($('#txtNote').val());
                     // alert(data)

			var rowCount = document.getElementById('tblMain').rows.length;
			if(rowCount==1){
				alert("No Items received");hideWaiting();
				return false;				
			}
			var row = 0;
			
			var arr="[";
			
			$('#tblMain .cutNo').each(function(){				
				var cutNo			= $(this).attr('id');
				var salesOrderId	= $(this).parent().find(".salesOrderNo").attr('id');
				var salesOrderNo	= URLEncode($(this).parent().find(".salesOrderNo").html());
				var partId			= $(this).parent().find(".part").attr('id');
				var part			= $(this).parent().find(".part").html();
				var bgColorId		= $(this).parent().find(".bgColor").attr('id');
				var bgColor			= $(this).parent().find(".bgColor").html();
				var line			= $(this).parent().find(".line").html();
				var size			= $(this).parent().find(".size").html();
				var qty				= $(this).parent().find(".qty").val();				
					
				if(qty>0){
					arr += "{";
					arr += '"cutNo":"'+URLEncode(cutNo) +'",' ;
					arr += '"salesOrderId":"'+salesOrderId +'",' ;
					arr += '"salesOrderNo":"'+salesOrderNo +'",' ;
					arr += '"partId":"'+partId +'",' ;
					arr += '"part":"'+URLEncode(part) +'",' ;
					arr += '"bgColorId":"'+bgColorId +'",' ;
					arr += '"bgColor":"'+URLEncode(bgColor) +'",' ;
					arr += '"line":"'+URLEncode(line) +'",' ;
					arr += '"size":"'+URLEncode(size) +'",' ;
					arr += '"qty":"'+qty +'"' ;
					arr +=  '},';					
				}
			});
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		var url = basepath+"fabricReceivedNote-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json", 
			type:'POST', 
			data:data,
			async:false,			
			success:function(json){
					$('#frmReceivedNote #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertx()",1000);
						$('#txtSerialNo').val(json.serialNo);
						$('#txtYear').val(json.year);
						if(json.confirmationMode==1){
							$('#butConfirm').show();
						}
						else{
							$('#butConfirm').hide();
						}
					}
				},
			error:function(xhr,status){
					
					$('#frmReceivedNote #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
			hideWaiting();
	}
   });
   
//------------------------------------------------------------------
  $('#frmFabricReceivedNoteSizesPopup #butSizeSave').die('click').live('click',function(){
	var requestType = '';
 	if ($('#frmFabricReceivedNoteSizesPopup').validationEngine('validate'))   
      { 
 			var orderQry=parseFloat($('#divOrderQty').html());
 			var totQty=0;
			var arr=[];
			var j=0;
			var dup=0;
			var blank=0;
			$('#tblSizesPopup .size').each(function(){
				
				var size	= $(this).val().trim();
				 	size	= size.toUpperCase();
				arr[j]=size;
				
				var qty	= parseFloat($(this).parent().parent().find(".sizeQty").val());
				if($(this).parent().parent().find(".sizeQty").val()==''){
					qty=0;
				}
				totQty+=qty;
				
				j++;;
			});
			
			var sorted_arr = arr.sort();
			for (var i = 0; i < arr.length - 1; i++) {
				if (sorted_arr[i + 1].trim() == sorted_arr[i].trim()) {
					dup++;
				}
				
				if (sorted_arr[i].trim() == '') {
					blank++;
				}
			}

			
 		if(dup>0){
				alert("Duplicate Sizes Existing");
				return false;				
		}
 		if(blank>0){
				alert("Please Enter Sizes");
				return false;				
		}
 		else if(orderQry!=totQty){
				alert("Order Qty should equal to the total Qty");
				return false;				
		}
 						

		var data = "requestType=savSizes";
 			data+="&orderNo="	+	$('#cboOrderNo').val();
			data+="&orderYear="	+	$('#cboOrderYear').val();
			data+="&salesOrderId="	+	$('#cboSalesOrderNoPP').val();

			var rowCount = document.getElementById('tblSizesPopup').rows.length;
			if(rowCount==1){
				alert("No Sizes to save");
				return false;				
			}
			var row = 0;
			
			var arr="[";
			
			$('#tblSizesPopup .size').each(function(){
				
				var size	= $(this).val();
				var qty	= $(this).parent().parent().find(".sizeQty").val();
				
					
					if((qty>0) && (size!='')){
				        arr += "{";
						arr += '"size":"'+	URLEncode(size.toUpperCase()) +'",' ;
						arr += '"qty":"'+		qty +'"' ;
						arr +=  '},';
						
					}
			});
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basepath+"fabricReceivedNote-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			data:data,
			async:false,
			
			success:function(json){
					$('#frmFabricReceivedNoteSizesPopup #butSizeSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertx()",1000);
						loadSizeCombo();
					}
				},
			error:function(xhr,status){
					
					$('#frmFabricReceivedNoteSizesPopup #butSizeSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
	}
   });

	$('#frmReceivedNote #butReport').die('click').live('click',function(){
		if($('#txtSerialNo').val()!=''){
			window.open('?q=931&serialNo='+$('#txtSerialNo').val()+'&year='+$('#txtYear').val()+'&style='+$('#cboStyle').val()+'&Graphic='+$('#cboGraphicNo').val());	
		}
		else{
			alert("There is no Fabric Received No to view");
		}
	});

	$('#frmReceivedNote #butConfirm').die('click').live('click',function(){
		if($('#txtSerialNo').val()!=''){
			window.open('?q=931&serialNo='+$('#txtSerialNo').val()+'&year='+$('#txtYear').val()+'&style='+$('#cboStyle').val()+'&Graphic='+$('#cboGraphicNo').val()+'&approveMode=1');	
		}
		else{
			alert("There is no Fabric Received No to confirm");
		}
	});

	$('#frmReceivedNote #butNew').die('click').live('click',function(){
		window.location.href = "?q=45";
		$('#frmReceivedNote').get(0).reset();
		$('#frmReceivedNote #txtSerialNo').val('');
		$('#frmReceivedNote #txtYear').val('');
		$('#frmReceivedNote #cboStyle').val('');
		$('#frmReceivedNote #txtAODNo').val('');
		$('#frmReceivedNote #txtNote').val('');
		$("#frmReceivedNote #cboStyle").change();
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmReceivedNote #dtDate').val(d);
	});
});

function alertx()
{
	$('#frmInvoice #butSave').validationEngine('hide')	;
}

function alertDelete()
{
	$('#frmInvoice #butDelete').validationEngine('hide')	;
}

function closePopUp(){
	
}

function addClickedRows()
{
	if ($('#frmFabricRecvNotePopup').validationEngine('validate'))   
    { 
	var rowCount = document.getElementById('tblPopup').rows.length;
	var cutNo=$('#txtCutNo').val();

	$('#tblPopup .salesOrderNoP').each(function(){
		var salesOrderIdP	= $(this).attr('id');
		var salesOrderNoP	= $(this).html();
		var partIdP			= $(this).parent().find(".partP").attr('id');
		var partP			= $(this).parent().find(".partP").html();
		var bgColorIdP		= $(this).parent().find(".bgColorP").attr('id');
		var bgColorP		= $(this).parent().find(".bgColorP").html();
		var lineP			= $(this).parent().find(".lineP").html();
		var sizeP			= $(this).parent().find(".sizeP").html();
		var qtyP			= $(this).parent().find(".qtyP").val();
		var qtyMax			= $(this).parent().find(".maxQty").html();
		var diasbleFlag		= $(this).parent().find(".chk").is(':disabled');
		var chkFlag			= $(this).parent().find(".chk").is(':checked');

		if((qtyP>0) && (diasbleFlag==false) && (chkFlag==true)){
			var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="images/del.png" width="15" height="15" /></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+cutNo+'" class="cutNo">'+cutNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderIdP+'" class="salesOrderNo">'+salesOrderNoP+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+partIdP+'" class="part">'+partP+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+bgColorIdP+'" class="bgColor">'+bgColorP+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+lineP+'" class="line">'+lineP+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+sizeP+'" class="size">'+sizeP+'</td>';
			
			if(exceedPOPermision==false){
				content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="qtyP" class="validate[required,custom[integer],max['+qtyMax+']] qty" style="width:80px;text-align:center" type="text" value="'+qtyP+'"/></td>';
			}
			else{
				content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="qtyP" class="validate[required,custom[integer]] qty" style="width:80px;text-align:center" type="text" value="'+qtyP+'"/></td>';
			}
				content +='</tr>';
			
				add_new_row('#frmReceivedNote #tblMain',content);
				calTotals();
	}
	});
	checkAlreadySelected();
	}
}

function loadGraphicNo()
{
	var orderYear 	= $('#cboOrderYear').val();
	var styleNo 	= $('#frmReceivedNote #cboStyle').val();
	var url 		= basepath+"fabricReceivedNote-db-get.php?requestType=loadGraphicNo";
	
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:"styleNo="+URLEncode(styleNo)+"&orderYear="+orderYear,
		async:false,
		success:function(json){				
				document.getElementById("cboGraphicNo").innerHTML = json.graphicNo;
				document.getElementById("txtQty").value = '';
		}
	});
}

function loadCustomerPONo()
{
	    var orderYear 	= $('#cboOrderYear').val();
	    var graphicNo 	= $('#cboGraphicNo').val();
	    var styleNo 	= $('#frmReceivedNote #cboStyle').val();
		var url 		= basepath+"fabricReceivedNote-db-get.php?requestType=loadCustomerPONo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"graphicNo="+URLEncode(graphicNo)+"&orderYear="+orderYear+"&styleNo="+URLEncode(styleNo),
			async:false,
			success:function(json){
					document.getElementById("cboPONo").innerHTML=json.customerPoNo;
			}
		});
}

function loadOrderNo()
{
	    var styleNo 	= $('#frmReceivedNote #cboStyle').val();
	    var graphicNo 	= $('#cboPONo').val();
	    var poNo 		= $('#cboPONo').val();
	    var orderYear 	= $('#cboOrderYear').val();
	    var poNo 		= $('#cboPONo').val();
	    var customer 	= $('#cboCustomer').val();
		var url 		= basepath+"fabricReceivedNote-db-get.php?requestType=loadOrderNo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"poNo="+URLEncode(poNo)+"&customer="+customer+"&orderYear="+orderYear+"&styleNo="+URLEncode(styleNo)+"&graphicNo="+URLEncode(graphicNo)+"&poNo="+URLEncode(poNo),
			async:false,
			success:function(json){
				
					document.getElementById("cboOrderNo").innerHTML=json.orderNo;
					document.getElementById("cboCustomer").value=json.customer;
					if(poNo==''){
					document.getElementById("cboPONo").innerHTML=json.PoNo;
					}
			}
		});
}
//--------------------------------------------------
function loadCustomerPO(){
	    var orderNo 	= $('#cboOrderNo').val();
	    var orderYear 	= $('#cboOrderYear').val();
		var url 		= basepath+"fabricReceivedNote-db-get.php?requestType=loadPoNo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"orderNo="+orderNo+"&orderYear="+orderYear,
			async:false,
			success:function(json){				
				document.getElementById("cboPONo").value=json.poNo;
				document.getElementById("cboCustomer").value=json.customer;
				if(orderNo==''){
					document.getElementById("cboOrderNo").innerHTML=json.orderNo;
				}
			}
		});
}

function loadPONoAndOrderNo(){
	    var styleNo 	= $('#frmReceivedNote #cboStyle').val();
	    var graphicNo 	= $('#cboPONo').val();
	    var poNo 		= $('#cboPONo').val();
	    var orderNo 	= $('#cboOrderNo').val();
	    var orderYear 	= $('#cboOrderYear').val();
	    var customer 	= $('#cboCustomer').val();
		var locationType = $('#locationType').val();
		var url 		= basepath+"fabricReceivedNote-db-get.php?requestType=loadPONoAndOrderNo";
		
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"customer="+customer+"&orderYear="+orderYear+"&locationType="+locationType+"&styleNo="+URLEncode(styleNo)+"&graphicNo="+URLEncode(graphicNo),
			async:false,
			success:function(json){
					document.getElementById("cboPONo").innerHTML=json.poNo;
					document.getElementById("cboOrderNo").innerHTML=json.orderNo;
			}
		});
}
//-----------------------------------------------
function loadSalesLineNoPartSizesComboes(){
	    var orderNo = $('#cboOrderNo').val();
	    var orderYear = $('#cboOrderYear').val();
    	var locationType = $('#locationType').val();
	    var styleNo = $('#cboStyleP').val();
	    var salesOrderNo = $('#cboSalesOrderNoP').val();
		var url 		= basepath+"fabricReceivedNote-db-get.php?requestType=loadSalesLineNoPartSizesComboes";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"orderNo="+orderNo+"&orderYear="+orderYear+"&locationType="+locationType+"&salesOrderNo="+URLEncode(salesOrderNo)+"&styleNo="+URLEncode(styleNo),
			async:false,
			success:function(json){
 				
					document.getElementById("cboGraphicP").innerHTML=json.graphicNo;
					document.getElementById("cboSalesOrderNoP").innerHTML=json.salesOrderNo;
					document.getElementById("cboLineNo").innerHTML=json.lineNo;
					document.getElementById("cboPartNo").innerHTML=json.partNo;
					document.getElementById("cboSizes").innerHTML=json.sizes;
					document.getElementById("cboColor").innerHTML=json.color;
			}
		});
}
//-----------------------------------------------
function loadQty(){
	    var orderNo = $('#cboOrderNo').val();
	    var salesOrderNo = '';
		var url 		= basepath+"fabricReceivedNote-db-get.php?requestType=loadQty";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"orderNo="+orderNo+"&salesOrderNo="+URLEncode(salesOrderNo),
			async:false,
			success:function(json){
				
					document.getElementById("txtQty").value=json.qty;
			}
		});
}
//------------------------------------------------
function submitForm(){
	window.location.href = "?q=45&orderNo="+$('#cboOrderNo').val()
						+'&salesOrderNo='+$('#frmReceivedNote #cboSalesOrderNo').val()
						+'&poNo='+$('#cboPONo').val()
						+'&customer='+$('#cboCustomer').val()
						+'&orderYear='+$('#cboOrderYear').val()
						+'&styleNo='+$('#frmReceivedNote #cboStyle').val()
						+'&graphicNo='+$('#cboGraphicNo').val()
 
						
}
//----------------------------------------------- 
function clearGrid()
{
	var rowCount = document.getElementById('tblMain').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMain').deleteRow(1);
	}
	
	$('#txtQty').val('');
}
//---------------------------------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//------------------------------------------------------------------------------
function checkAlreadySelected(){
		$('#tblMain .cutNo').each(function(){
			
		 var cutNo	= $(this).attr('id');
		 var salesOrderId	= $(this).parent().find(".salesOrderNo").attr('id');
		 var size	= $(this).parent().find(".size").html();
		
			  $('#tblPopup .salesOrderNoP').each(function(){
				
				var cutNoP=$('#txtCutNo').val();
				
				var salesOrderIdP	= $(this).attr('id');
				var sizeP	= $(this).parent().find(".sizeP").html();
				//alert(cutNo+"=="+cutNoP+"***"+salesOrderId+"=="+salesOrderIdP+"***"+size+"=="+sizeP+"***")
				if((cutNo==cutNoP) && (salesOrderId==salesOrderIdP) && (size==sizeP)){
					$(this).parent().find(".chk").prop('checked', true);
					$(this).parent().find(".chk").prop('disabled',true);
				}
			});
	});
}
//------------------------------------------------------------------------------
function uncheckAll(){
			  $('#tblPopup .salesOrderNoP').each(function(){
					$(this).parent().find(".chk").prop('checked', false);
					$(this).parent().find(".chk").prop('disabled',false);
			});
}
//------------------------------------------------------------------------------
function checkUncheckRows(obj)
{	
	var chkFlag	= $(obj).is(':checked');
	
	$('#tblPopup .salesOrderNoP').each(function(){
		var disabledFlag	= $(this).parent().find(".chk").is(':disabled');
		
		if((chkFlag==true) && (disabledFlag!=true)){
			$(this).parent().find(".chk").prop('checked', true);
		}
		else if((chkFlag!=true) && (disabledFlag==true)){
			$(this).parent().find(".chk").prop('checked', true);
		}
		else if(chkFlag!=true){
			$(this).parent().find(".chk").prop('checked', false);
		}
	});
}

function loadAllCombo()
{
	//######################
	//####### get values ###
	//######################
	var year 			= $('#cboOrderYear').val();
	var graphicNo 		= $('#cboGraphicNo').val();
	var styleId 		= $('#frmReceivedNote #cboStyle').val();
	var customerPONo	= $('#cboPONo').val();
	var orderNo			= $('#cboOrderNo').val();
	var customerId 		= $('#cboCustomer').val();
	var salesOrderNo 	= $('#frmReceivedNote #cboSalesOrderNo').val();
	
	//######################
	//####### create url ###
	//######################
	var url		= basepath+"fabricReceivedNote-db-get.php?requestType=loadAllComboDetails";
		url	   +="&year="+			year;	
		url	   +="&graphicNo="+		URLEncode(graphicNo);
		url	   +="&styleId="+		URLEncode(styleId);
		url	   +="&customerPONo="+	URLEncode(customerPONo);
		url	   +="&orderNo="+		orderNo;
		url	   +="&salesOrderNo="+		salesOrderNo;
		url	   +="&customerId="+	customerId;
	
	//######################
	//####### send data  ###
	//######################
	$.ajax({url:url,
			async:false,
			dataType:'json',
			success:function(json){
					$('#frmReceivedNote #cboStyle').html(json.styleNo);
					$('#cboGraphicNo').html(json.graphicNo);
					$('#cboPONo').html(json.customerPoNo);
					$('#cboOrderNo').html(json.orderNo);
					$('#frmReceivedNote #cboSalesOrderNo').html(json.salesOrderNo);
					$('#cboCustomer').html(json.customer);
			}
			});
}
//------------------------------------------------------------------------

function loadOrderNos()
{
	//######################
	//####### get values ###
	//######################
	var year 		= $('#cboOrderYear').val();
	var graphicNo 	= $('#cboGraphicNo').val();
	var styleId 	= $('#frmReceivedNote #cboStyle').val();
	var customerPONo= $('#cboPONo').val();
	var customerId 	= $('#cboCustomer').val();
	var salesOrderNo 	= $('#frmReceivedNote #cboSalesOrderNo').val();
	
	//######################
	//####### create url ###
	//######################
	var url		= basepath+"fabricReceivedNote-db-get.php?requestType=loadOrderNosToSalesOrderNos";
		url	   +="&year="+			year;	
		url	   +="&graphicNo="+		graphicNo;
		url	   +="&styleId="+		styleId;
		url	   +="&customerPONo="+	customerPONo;
		url	   +="&salesOrderNo="+		salesOrderNo;
		url	   +="&customerId="+	customerId;
	
	//######################
	//####### send data  ###
	//######################
	$.ajax({url:url,
			async:false,
			dataType:'json',
			success:function(json){
					$('#cboOrderNo').html(json.orderNo);
			}
			});
}

//------------------------------------------------------------------------
function loadSizes()
{
	var orderYear		= $('#cboOrderYear').val();
	var orderNo 		= $('#cboOrderNo').val();
	var salesOrderId 	= $('#cboSalesOrderNoP').val();
	
	var rowCount = document.getElementById('tblPopup').rows.length;
	for(var i=1;i<rowCount;i++)
	{
		document.getElementById('tblPopup').deleteRow(1);
	}
	
	var url 		= basepath+"fabricReceivedNote-db-get.php?requestType=loadSizes";
	
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:"orderYear="+orderYear+"&orderNo="+orderNo+"&salesOrderId="+URLEncode(salesOrderId),
		async:false,
		success:function(json){

			var length = json.arrCombo.length;
			var arrCombo = json.arrCombo;

			for(var i=0;i<length;i++)
			{
				var fabRcvQty=parseFloat(arrCombo[i]['fabRcvQty']);
				var size=arrCombo[i]['size'];
				var overCutPercentage=parseFloat(arrCombo[i]['overCutPercentage']);
				var orderQty=parseFloat(arrCombo[i]['orderQty']);	
				var minOrderQty=((fabRcvQty+0.5)*100)/(100+overCutPercentage);
				minOrderQty= Math.round(minOrderQty);
					
				var content='<tr class="normalfnt">';
				if(fabRcvQty==0){
                content +='<td align="center" bgcolor="#FFFFFF"><img src="images/del.png" width="15" height="15" class="delImgPopup" /></td>';
				}
				else{
                content +='<td align="center" bgcolor="#FFFFFF"></td>';
				}
                content+='<td align="center" bgcolor="#FFFFFF" id=""><input name="txtSize" type="text" id="txtSize" style="width:60px; text-align:center" value="'+size+'" class="size" /></td>';
               content+='<td align="center" bgcolor="#FFFFFF" id=""><input  name="txtSizeQty" type="text" id="txtSizeQty" style="width:60px; text-align:right" value="'+orderQty+'"  class="validate[required,custom[number],min['+minOrderQty+']] sizeQty" /></td>';
               content+=' <td align="center" bgcolor="#FFFFFF" id="">'+fabRcvQty+'</td>';
               content+=' </tr>';
			   add_new_row('#frmFabricReceivedNoteSizesPopup #tblSizesPopup',content);
				
			}
			
			
			if(length==0){
				var content='<tr class="normalfnt">';
                content +='<td align="center" bgcolor="#FFFFFF"><img src="images/del.png" width="15" height="15" class="delImgPopup" /></td>';
                content+='<td align="center" bgcolor="#FFFFFF" id=""><input name="txtSize" type="text" id="txtSize" style="width:60px; text-align:center" value="" class="size" /></td>';
               content+='<td align="center" bgcolor="#FFFFFF" id=""><input  name="txtSizeQty" type="text" id="txtSizeQty" style="width:60px; text-align:right" value=""  class="validate[required,custom[number],min[0]] sizeQty" /></td>';
               content+=' <td align="center" bgcolor="#FFFFFF" id="">0</td>';
               content+=' </tr>';
			   add_new_row('#frmFabricReceivedNoteSizesPopup #tblSizesPopup',content);
			}
			
			   var content='<tr class="normalfnt">';
			   content+='<td colspan="4" align="left"  bgcolor="#FFFFFF" ><img src="images/Tadd.jpg" name="butInsertRowPopup" width="78" height="24" class="mouseover" id="butInsertRowSizesPopup" /></td>';
			   content+='</tr>';
			   add_new_row('#frmFabricReceivedNoteSizesPopup #tblSizesPopup',content);
		}
	});
}
//-----------------------------------------------------------------------
function loadSizeCombo(){
	    var orderNo = $('#cboOrderNo').val();
	    var orderYear = $('#cboOrderYear').val();
	    var salesOrderNo = $('#frmReceivedNote #cboSalesOrderNo').val();
	    var size = $('#cboSizes').val();
		var locationType = $('#locationType').val();
		var url 		= basepath+"fabricReceivedNote-db-get.php?requestType=loadSizeCombo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"orderNo="+orderNo+"&orderYear="+orderYear+"&locationType="+locationType+"&salesOrderNo="+URLEncode(salesOrderNo),
			async:false,
			success:function(json){
					$('#cboSizes').html(json.combo);
					$('#cboSizes').val(size);
			}
		});
}
//-----------------------------------------------------------------------
function loadOrderQty(){
	    var orderNo 		= $('#cboOrderNo').val();
	    var orderYear 		= $('#cboOrderYear').val();
	    var salesOrderNo 	= $('#cboSalesOrderNoP').val();
		var url 			= basepath+"fabricReceivedNote-db-get.php?requestType=loadOrderQty";
		
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"orderNo="+orderNo+"&orderYear="+orderYear+"&salesOrderNo="+URLEncode(salesOrderNo),
			async:false,
			success:function(json){
					document.getElementById("divOrderQty").innerHTML=json.orderQty;
					$('#cboOrderNo').html(json.orderQt);
			}
		});
}

function calTotals(){
	var tot=0;
	var rowCount = document.getElementById('tblMain').rows.length;
			$('#tblMain .cutNo').each(function(){
				
				var qty	= parseFloat($(this).parent().find(".qty").val());
				if(isNaN(qty))
				qty=0;
				
				tot+=qty;
				
			});
	document.getElementById('divTotal').innerHTML=tot;
}
//----------------------------------
function getFRNexcessFactor(){
	var url 		= basepath+"fabricReceivedNote-db-get.php?requestType=loadFRNexcessFactor";
	var httpobj 	= $.ajax({url:url,async:false})
	return httpobj.responseText;
}
//----------------------------------

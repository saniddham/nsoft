<?php 
	session_start();
	//ini_set('display_errors',1);
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	require_once $_SESSION['ROOT_PATH']."class/cls_permisions.php";
	require_once $_SESSION['ROOT_PATH']."class/customerAndOperation/cls_textile_stores.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
	require_once $_SESSION['ROOT_PATH']."class/customerAndOperation/bulk/cls_bulk_get.php";
	
	
	//---------check Permission to save recive qty more than PO qty.------------	
	$objpermisionget	= new cls_permisions($db);
	$objtexttile 		= new cls_texttile();
	$exceedPOPermision 	= $objpermisionget->boolSPermision(5);
	$objcomfunc 		= new cls_commonFunctions_get($db);
	$obj_bulk_get 		= new Cls_bulk_get($db);
	
	//$excessFactor 	= $objtexttile->getFRNexcessFactor($company);
	//------------------------------------------------------------------------
	
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo 	 = $_REQUEST['serialNo'];
	$year 		 = $_REQUEST['Year'];
	//$style           = $_REQUEST['strStyle'];
	//$graphicNo 	 = $_REQUEST['strGraphic'];
	$cusromerPO 	 = $_REQUEST['cboPONo'];
	$orderNo 	 = $_REQUEST['orderNo'];
	$orderYear 	 = $_REQUEST['orderYear'];
        $customer 	 = $_REQUEST['customer'];
	$strStyle 	 = $_REQUEST['strStyle'];
	$strGraphic     = $_REQUEST['strGraphic'];
	$AODNo		= $_REQUEST['AODNo'];
	$AODDate 	 = $_REQUEST['AODDate'];
	$remarks 	 = $_REQUEST['remarks'];
	//echo $_REQUEST['arr'];
	$arr 		= json_decode($_REQUEST['arr'], true);

  
	$programName='Fabric Received Note';
	$programCode='P0045';

	$ApproveLevels = (int)getApproveLevel($programName);
	$maxStatus = $ApproveLevels+1;

//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		$savableFlag=1;
                
                       
//-------------------------------------------------------------------------------------------                        
		if($serialNo==''){
			$serialNo 	= getNextSerialNo();
			$year = date('Y');
			$editMode=0;
			$savedStatus=$maxStatus;
			$savedLevels=$ApproveLevels;
		}
		else{
			$editMode=1;
			$savableFlag=getSaveStatus($serialNo,$year);//check wether already confirmed
			$sql = "SELECT ware_fabricreceivedheader.intStatus, 
			ware_fabricreceivedheader.intApproveLevels 
			FROM ware_fabricreceivedheader 
			WHERE
			ware_fabricreceivedheader.intFabricReceivedNo =  '$serialNo' AND
			ware_fabricreceivedheader.intFabricReceivedYear =  '$year'";

			$result = $db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			$savedStatus=$row['intStatus'];
			$savedLevels=$row['intApproveLevels'];
		}
		
		    $editPermission=loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
		
			$orderStatus=getOrderStatus($orderNo,$orderYear);//check wether already confirmed
			//--------------------------
			$companyFlag=getPOCompanyValidation('FRN',$location,$company,$orderNo,$orderYear);//check wether already confirmed
			//--------------------------
			$locationFlag=getLocationValidation('FRN',$location,$company,$serialNo,$year);//check wether already confirmed
			//--------------------------
			$locationType 	= $objcomfunc->getLocationType2($location);
			//--------------------------
			$lc_flag		= $obj_bulk_get->get_order_lc_flag($orderNo,$orderYear,'RunQuery2');
 			$lc_status		= $obj_bulk_get->get_lc_status($orderNo,$orderYear,'RunQuery2');
			if($lc_flag	== 6) 
				$lc_flag = 1;
			else
				$lc_flag = 0;
		
/*		 if($companyFlag==1){
			// $rollBackFlag=1; //commented on 27_06_2013
			 $rollBackFlag=0; //commented on 27_06_2013
			// $rollBackMsg="Invalid Save Company"; //commented on 27_06_2013
		 }
		 else */
		 	
		 $plantId			= getPlantId($location);
		
		 if($plantId!='')
		 {
			$plantStatus	= getPlantStatus($plantId); 
		 }
		
		 if(( $locationFlag==1) && ($editMode==1)){
			 $rollBackFlag=1;
			 $rollBackMsg="Invalid Edit location";
		 }
		 else if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This Received No is already confirmed.cant edit.";
		 }
		 else if($editPermission==0){
			 $rollBackFlag=1;
			 $rollBackMsg="No Permission to save.";
		 }
		 else if($orderStatus!=1){
			 $rollBackFlag=1;
			 $rollBackMsg="This Order is revised or pending.";
		 }
		 else if($locationType!=1){
			 $rollBackFlag=1;
			 $rollBackMsg="This is not a production location.So can't receive fabrics.";
		 }
		/* else if($lc_flag==1 && $lc_status !=1){
			 $rollBackFlag=1;
			 $rollBackMsg="No Approved LC.So can't Receive Fabrics.";
		 }*/
		 else if($plantId=='')
		 {
			 $rollBackFlag=1;
			 $rollBackMsg="This location is not allocate to a plant.";
		 }
		 else if($plantStatus==0)
		 {
			 $rollBackFlag=1;
			 $rollBackMsg="This location is not allocate to an active plant.";
		 }
		else if($editMode==1){//edit existing grn
		$sql = "UPDATE `ware_fabricreceivedheader` SET intStatus ='$maxStatus', 
												       intApproveLevels ='$ApproveLevels', 
													  intOrderNo ='$orderNo', 
													  intOrderYear ='$orderYear', 
													  strAODNo ='$AODNo', 
													  strremarks ='$remarks', 
													  dtmdate ='$AODDate', 
													  intModifiedBy ='$userId', 
													  dtmModifiedDate =now(), 
													  intCompanyId ='$location' ,
													  strStyleNo = '$strStyle',
													  strGraphicNo = '$strGraphic'
				WHERE (`intFabricReceivedNo`='$serialNo') AND (`intFabricReceivedYear`='$year')";
		$result = $db->RunQuery2($sql);
		}
		else{
			//$sql = "DELETE FROM `ware_fabricreceivedheader` WHERE (`intFabricReceivedNo`='$serialNo') AND (`intFabricReceivedYear`='$year')";
			//$result1 = $db->RunQuery2($sql);

			$sql = "INSERT INTO `ware_fabricreceivedheader` (`intFabricReceivedNo`,`intFabricReceivedYear`,intOrderNo,intOrderYear,strAODNo,strRemarks,intStatus,intApproveLevels,dtmdate,dtmCreateDate,intCteatedBy,intCompanyId,strStyleNo,strGraphicNo) 
					VALUES ('$serialNo','$year','$orderNo','$orderYear','$AODNo','$remarks','$maxStatus','$ApproveLevels','$AODDate',now(),'$userId','$location','$strStyle','$strGraphic')";
					
			$result = $db->RunQuery2($sql);
		}
		//-----------delete and insert to detail table-----------------------
		if(($result) && ($rollBackFlag!=1)){
			$sql = "DELETE FROM `ware_fabricreceiveddetails` WHERE (`intFabricReceivedNo`='$serialNo') AND (`intFabricReceivedYear`='$year')";
			$result2 = $db->RunQuery2($sql);
			$saved=0;
			$toSave=0;
			$rollBackMsg="Maximum Qty for followings...";
                      //  $records = array("records"=>array("values"=>array()));
                          //  $record1 = array();
                      //  print_r($records);
                        //$record[][];
                       // $record = array("records"=>;
			foreach($arr as $arrVal)
			{     
                              //  $value = array();
				$cutNo 				= $arrVal['cutNo'];
				$salesOrderId 	 	= $arrVal['salesOrderId'];
				$salesOrderNo	 	= $arrVal['salesOrderNo'];
				$partId 	 		= $arrVal['partId'];
				$part 	 			= $arrVal['part'];
				$bgColorId 		 	= $arrVal['bgColorId'];
				$bgColor 	 		= $arrVal['bgColor'];
				$line 	 			= $arrVal['line'];
				$size 		 		= $arrVal['size'];
				$qty 		 		= round($arrVal['qty'],4);
				//$place = 'Stores';
			
                                //$type 	 = 'Received';
                    
                                  
			 	$soStatus		=loadSalesOrderStatus($orderNo,$orderYear,$salesOrderId);
				if($soStatus== -10){
					$rollBackFlag=1;
					$rollBackMsg ="Sales order is already closed";
				}
			 
			 	$fdQty			=loadDamageReturnedQty($location,$orderNo,$orderYear,$salesOrderId,$size);
			 	$receivedQty	=loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size);
			 	$nonstckConfQty	=loadNonstckConfQty($location,$orderNo,$orderYear,$salesOrderId,$size);
				$orderQty		=loadOrderQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade);
				
				$toleratePercentage	= $objtexttile->getSavedTolerencePercentage2($orderNo,$orderYear,$salesOrderId);
				$excessQty			=loadSizeWiseExcessQty($orderNo,$orderYear,$salesOrderId,$size,$toleratePercentage);
				
				$recvQtyArr[$orderNo][$orderYear][$salesOrderId][$size] 	+=$qty;
				$orderQtyArr[$orderNo][$orderYear][$salesOrderId][$size] 	=ceil($orderQty+$excessQty+$fdQty-$receivedQty-$nonstckConfQty);
				
				
				$maxRcvQty			=ceil($orderQty-$receivedQty-$nonstckConfQty+$excessQty+$fdQty);
				//echo $orderQty."--".$receivedQty."--".$nonstckConfQty."--".$excessQty."--".$fdQty;
				if($maxRcvQty<=0){
					$maxRcvQty=0;
				}
				
				if($exceedPOPermision==''){//no permision to exceed po qty
					if($recvQtyArr[$orderNo][$orderYear][$salesOrderId][$size]>$orderQtyArr[$orderNo][$orderYear][$salesOrderId][$size]){
						$rollBackFlag	=1;
						$rollBackMsg 	.="<br> "."(".$part."-".$salesOrderNo."-".$size.") =".$maxRcvQty;
					}
					//------check maximum FR Qty--------------------
					else if(round($qty)>round($maxRcvQty)){
						$rollBackFlag=1;
						$rollBackMsg .="<br> ".$cutNo."-".$part."-".$salesOrderNo."-".$size." =".$maxRcvQty;
					}
				}

				//----------------------------
				if($rollBackFlag!=1){
					
					$sql = "INSERT INTO `ware_fabricreceiveddetails` (`intFabricReceivedNo`,`intFabricReceivedYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`intGroundColor`,`strLineNo`,`strSize`,`dblQty`,maxRetryCount,retryCount) 
					VALUES ('$serialNo','$year','$cutNo','$salesOrderId','$partId','$bgColorId','$line','$size','$qty',3,0)";
					$result = $db->RunQuery2($sql);
					if($result==1){
					$saved++;
					}
				}
				$toSave++;
                              
                              // $record1[] =   $value;
                                //$record =  array('value'=> $value);
                                // print_r($record);
                                 // send_detais($record);
		}
              //  $record =  array('value'=> $record1);
                  //$record =  array('value'=> $record1);
              //$record[] = $value;
            // print_r($record);
		}
		//echo $rollBackFlag;
			//	print_r($recvQtyArr);
			//	print_r($orderQtyArr);
 	//$db->RunQuery2('Rollback');

		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
                        //generate_token();
                        
			$confirmationMode=getConfirmationMode($programCode,$userId,$ApproveLevels,$maxStatus);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
                        
			
                         
                         
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
			$response['confirmationMode'] = $confirmationMode;
                        
                        
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
	elseif($requestType=='savSizes'){
        $rollBackFlag = 0;
        global $editMode;
		
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
			$orderNo 	 = $_REQUEST['orderNo'];
			$orderYear 	 = $_REQUEST['orderYear'];	
			$salesOrderId 	 = $_REQUEST['salesOrderId'];
	
			$sql = "DELETE FROM `trn_ordersizeqty` WHERE (`intOrderNo`='$orderNo') AND (`intOrderYear`='$orderYear') AND (`intSalesOrderId`='$salesOrderId')";
			$result2 = $db->RunQuery2($sql);
			$saved=0;
			$toSave=0;
			$rollBackMsg="Minimum Qty for followings...";
			foreach($arr as $arrVal)
			{
				$size 		 = strtoupper($arrVal['size']);
				$qty 		 = round($arrVal['qty'],4);
				
			 	$receivedQty=loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size);
				$percentage=loadCutPercentage($orderNo,$orderYear,$salesOrderId);
				$toleratePercentage	= $objtexttile->getSavedTolerencePercentage2($orderNo,$orderYear,$salesOrderId);
				$excessQty=loadSizeWiseExcessQty($orderNo,$orderYear,$salesOrderId,$size,$toleratePercentage);
				$minOrderQty=ceil($receivedQty-$excessQty);

				//------check maximum FR Qty--------------------
				if($qty<$minOrderQty){
					$rollBackFlag=1;
					$rollBackMsg .="<br> ".$cutNo."-".$part."-".$salesOrderNo."-".$size." =".$minOrderQty;
				}

				//----------------------------
				if($rollBackFlag!=1){
					
					$sql = "INSERT INTO `trn_ordersizeqty` (`intOrderNo`,`intOrderYear`,`intSalesOrderId`,`strSize`,`dblQty`,maxRetryCount,retryCount,orderType) 
					VALUES ('$orderNo','$orderYear','$salesOrderId','$size','$qty',3,0,0)";
					$result = $db->RunQuery2($sql);
					if($result==1){
					$saved++;
					}
				}
				$toSave++;
		}
		//echo $rollBackFlag;
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$confirmationMode=getConfirmationMode($programCode,$userId,$ApproveLevels,$maxStatus);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
		
	}
	//----------------------------------------
	echo json_encode($response);
//----------------------------------------




//----------------------------------------
	function getNextSerialNo()
	{
		global $db;
		global $location;
		$sql = "SELECT
				sys_no.intFabricReceivedNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$location'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextNo = $row['intFabricReceivedNo'];
		
		$sql = "UPDATE `sys_no` SET intFabricReceivedNo=intFabricReceivedNo+1 WHERE (`intCompanyId`='$location')  ";
		$db->RunQuery2($sql);
		
		return $nextNo;
	}
	//-----------------------------------------------------------
	
	
	//-----------------------------------------------------------
function loadDamageReturnedQty($location,$orderNo,$orderYear,$salesOrderId,$size)
{
		global $db;
		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty*-1) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				/*ware_stocktransactions_fabric.intLocationId =  '$location' AND*/
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType in(  'Dispatched_F' ,'Dispatched_P' ,'Dispatched_CUT_RET')
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}

	//-----------------------------------------------------------
function loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size)
{
		global $db;

		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				/*ware_stocktransactions_fabric.intLocationId =  '$location' AND */
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType =  'Received' 
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		$RcvQty=$rows['dblQty'];
		
		
	//added by roshan 2014-Jan-06
	$sql1 = "SELECT
			ifnull(Sum(ware_stocktransactions_fabric.dblQty),0) as dispatchDamages
			FROM `ware_stocktransactions_fabric`
			WHERE
			ware_stocktransactions_fabric.intOrderNo 		= '$orderNo' AND
			ware_stocktransactions_fabric.intOrderYear 		= '$year' AND
			ware_stocktransactions_fabric.intSalesOrderId 	= '$salesOrderNo' AND
			ware_stocktransactions_fabric.strSize 			= '$size' AND
			ware_stocktransactions_fabric.strType IN ('Dispatched_P','Dispatched_F','Dispatched_CUT_RET')
			";
	$result1 	= 	$db->RunQuery2($sql1);
	$row1		=	mysqli_fetch_array($result1);
	$RcvQty		=	$RcvQty+$row1['dispatchDamages'];
	
	return $RcvQty;
}
//--------------------------------------------------------------
	function loadNonstckConfQty($location,$orderNo,$orderYear,$salesOrderId,$size)
{
		global $db;
	  	$sql = "SELECT
				Sum(ware_fabricreceiveddetails.dblQty) as dblQty 
				FROM
				ware_fabricreceivedheader
				Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
				WHERE
				ware_fabricreceivedheader.intStatus >  1 AND
				ware_fabricreceivedheader.intStatus <=  ware_fabricreceivedheader.intApproveLevels AND
				ware_fabricreceivedheader.intOrderNo =  '$orderNo' AND
				ware_fabricreceivedheader.intOrderYear =  '$orderYear' AND
				ware_fabricreceiveddetails.intSalesOrderId =  '$salesOrderId' AND
				ware_fabricreceiveddetails.strSize =  '$size'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//--------------------------------------------------------------
function loadOrderQty($location,$orderNo,$orderYear,$salesOrderNo,$size,$grade)
{
		global $db;
		  $sql = "SELECT
				trn_ordersizeqty.dblQty
				FROM trn_ordersizeqty
				WHERE
				trn_ordersizeqty.intOrderNo =  '$orderNo' AND
				trn_ordersizeqty.intOrderYear =  '$orderYear' AND
				trn_ordersizeqty.intSalesOrderId =  '$salesOrderNo' AND
				trn_ordersizeqty.strSize =  '$size'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//---------------------------------------------------------------
function loadSizeWiseExcessQty($orderNo,$orderYear,$salesOrderId,$size,$excessFactor)
{
	/////////////////////////????????????????????15/03/2013
		global $db;
		
		  $sql = "SELECT
			trn_ordersizeqty.dblQty,
			trn_orderdetails.dblOverCutPercentage,
			trn_orderdetails.dblDamagePercentage
			FROM trn_orderdetails 
			Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
			WHERE
			trn_orderdetails.intOrderNo =  '$orderNo' AND
			trn_orderdetails.intOrderYear =  '$orderYear' AND
			trn_orderdetails.intSalesOrderId =  '$salesOrderId' AND
			trn_ordersizeqty.strSize =  '$size' ";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
	//$value=$rows['dblQty']*($rows['dblOverCutPercentage']+$rows['dblDamagePercentage'])/100;
		$value=$rows['dblQty']*($rows['dblOverCutPercentage']+$excessFactor)/100;
		
		return val($value);
}
//-----------------------------------------------------------
	function loadCutPercentage($orderNo,$orderYear,$salesOrderId)
{
		global $db;
	        	$sql = "SELECT
				trn_orderdetails.dblOverCutPercentage 
				FROM trn_orderdetails 
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_orderdetails.intSalesOrderId =  '$salesOrderId' ";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblOverCutPercentage']);
}
//-----------------------------------------------------------
function getConfirmationMode($programCode,$intUser,$approveLevels,$status){
		global $db;
		$k=$approveLevels+2-$status;
		$sqlp = "SELECT 
		menupermision.intEdit, 
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		if($rowp['int'.$k.'Approval']==1) 
		 $confirmatonMode=1; 
		else
		 $confirmatonMode=0; 

		return $confirmatonMode;
	}
//-----------------------------------------------------------
	function getSaveStatus($serialNo,$year)
	{
		global $db;
		$sql = "SELECT ware_fabricreceivedheader.intStatus, ware_fabricreceivedheader.intApproveLevels FROM ware_fabricreceivedheader WHERE (`intFabricReceivedNo`='$serialNo') AND (`intFabricReceivedYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			
		return $editableFlag;
	}
//-----------------------------------------------------------
	function getOrderStatus($serialNo,$year)
	{
		global $db;
		$sql = "SELECT trn_orderheader.intStatus FROM trn_orderheader WHERE (`intOrderNo`='$serialNo') AND (`intOrderYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
 			
		return $status;
	}
//-----------------------------------------------------------
	function getPOCompanyValidation($type,$location,$company,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
		$sql = "SELECT
					mst_locations.intCompanyId
					FROM
					trn_orderheader
					Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
					WHERE
					trn_orderheader.intOrderNo =  '$serialNo' AND
					trn_orderheader.intOrderYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			if($company == $row['intCompanyId'])
			$flag=0;
 			
		return $flag;

}
//---------------------------------------------------
	function getLocationValidation($type,$location,$company,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
	  	$sql = "SELECT
					ware_fabricreceivedheader.intCompanyId
					FROM
					ware_fabricreceivedheader
					Inner Join mst_locations ON ware_fabricreceivedheader.intCompanyId = mst_locations.intId
					WHERE
					ware_fabricreceivedheader.intFabricReceivedNo =  '$serialNo' AND
					ware_fabricreceivedheader.intFabricReceivedYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			if($location == $row['intCompanyId'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}

function getPlantId($locationId)
{
	global $db;
	
	$sql 	= "SELECT intPlant FROM mst_locations WHERE intId = '$locationId' ";
	
	$result = $db->RunQuery2($sql);
	$row	= mysqli_fetch_array($result);
	
	return $row['intPlant'];
}

function getPlantStatus($plantId)
{
	global $db;
	
	$sql 	= "SELECT intStatus FROM mst_plant WHERE intPlantId = '$plantId' ";
	
	$result = $db->RunQuery2($sql);
	$row	= mysqli_fetch_array($result);
	
	return $row['intStatus'];
}

function loadSalesOrderStatus($orderNo,$orderYear,$salesOrderId){

	global $db;
	
	$sql 	= "SELECT
				trn_orderdetails.`STATUS`
				FROM `trn_orderdetails`
				WHERE
				trn_orderdetails.intOrderNo = '$orderNo' AND
				trn_orderdetails.intOrderYear = '$orderYear' AND
				trn_orderdetails.intSalesOrderId = '$salesOrderId'
				 ";
	
	$result = $db->RunQuery2($sql);
	$row	= mysqli_fetch_array($result);
	
	return $row['STATUS'];
	
}

/*function generate_token($record)
{
  
            $AUTH_USERNAME = "screenline";
            $AUTH_PASSWORD = "user123";
            
            $url = 'https://oneapp.ncinga.com/rest_services/post_service';
            $data = array('username'=>$AUTH_USERNAME,'password'=>$AUTH_PASSWORD,'type'=>'auth');
            $data_json = json_encode($data);
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  
            $response  = curl_exec($ch);
            curl_close($ch);        
            //$d        = json_decode($response);
            //print_r($d);
            insert_tbl($response);
            $token2 = select_token();
            send_detais($record,$token2);
            
            if(!$response){
                die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
            }
            
            curl_close($ch);
    
}

function insert_tbl($data)
{
    global $db;
    $date = date('Y-m-d h:i:s', time());
    
    $jsonArray      = json_decode($data, true);
    $userId         = $jsonArray['userId'];
    $generatedTime  = $jsonArray['generatedTime'];
    $expireDate     = $jsonArray['expireDate'];
    $token          = $jsonArray['token'];
   
    $sql_insert 	= "
                            INSERT INTO
                            tbl_token_details
                            (USER_ID,
                            GENERATED_TIME,
                            EXPIRED_DATE,
                            TOKEN,
                            UPDATED_TIME
                            )
                            VALUES
                            ('$userId',
                             $generatedTime,
                             $expireDate,   
                             '$token',
                             '$date'  
                            );";
	//echo $sql_insert;
	 $db->RunQuery2($sql_insert);
}
//--------------------------------------------------------
function send_detais($record,$token)
{ 
           
           // $token  = "eyJhbGciOiJIUzI1NiJ9.eyJ0ZW5hbnRfaWQiOiJzY2wiLCIkaW50X3Blcm1zIjpbInBlcjEiLCJwZXIyIl0sInN1YiI6Im9yZy5wYWM0ai5tb25nby5wcm9maWxlLk1vbmdvUHJvZmlsZSNzY3JlZW5saW5lIiwicm9sZXMiOlsiREFTSEJPQVJEX0FQUF9VU0VSIiwicm9sZTIiXSwibGFzdF9uYW1lIjoiTGluZSIsImZhY3RvcnlfaWQiOiJzY3JlZW5fbGluZSIsInVzZXJfY29kZSI6ImMyTnNjMk55WldWdVgyeHBibVZ6WTNKbFpXNXNhVzVsIiwidXNyIjoic2NyZWVubGluZSIsInBlcm1pc3Npb25zIjpbInBlcjEiLCJwZXIyIl0sIiRpbnRfcm9sZXMiOlsiREFTSEJPQVJEX0FQUF9VU0VSIiwicm9sZTIiXSwiZXhwIjoxNTc0NDk5MjE1LCJmaXJzdF9uYW1lIjoiU2NyZWVuIiwiaWF0IjoxNTQyOTYzMjE1fQ.Fj7y-tDBdpX7CcrAD3EzydLCKTKBRjSE_OyX7YyZe7g";
            
            //$token = "eyJhbGciOiJIUzI1NiJ9.eyJ0ZW5hbnRfaWQiOiJtZWciLCIkaW50X3Blcm1zIjpbInBlcjQiXSwic3ViIjoib3JnLnBhYzRqLm1vbmdvLnByb2ZpbGUuTW9uZ29Qcm9maWxlI3FjMSIsInJvbGVzIjpbIkVORF9MSU5FX1FDX1VTRVIiXSwibGFzdF9uYW1lIjoiVXNlciIsImZhY3RvcnlfaWQiOiJzZ3QiLCJ1c2VyX2NvZGUiOiJiV1ZuYzJkMGNXTXgiLCJ1c3IiOiJxYzEiLCJwZXJtaXNzaW9ucyI6WyJwZXI0Il0sIiRpbnRfcm9sZXMiOlsiRU5EX0xJTkVfUUNfVVNFUiJdLCJleHAiOjE1NDI3ODU0MjksImZpcnN0X25hbWUiOiJRQzEiLCJpYXQiOjE1NDI3ODE4Mjl9.6M3vFM40OJq3eGcTK9TLDLAchdlCoeauXlCX2zydh1o";
        
        
            $url        = 'https://oneapp.ncinga.com/rest_services/post_service';
           // $det = array('value'=>$record);
            $data       = array('token'=>$token,'records'=>array($record));
            $data_json  = json_encode($data);
            print_r($data_json);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  
            $response  = curl_exec($ch);
          // echo $response[0];
             $d = json_decode($response);
           
           print_r($d);
            
            /* if($d ->messageCode == 'NCN0003')
             {
                 //echo "kjk";
               generate_token($record);
               //send_detais($record,$token);
             }
           
            if(!$response){
                //echo "klkl";
                  //echo $response['messageCode'];
                die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
            }
            
            curl_close($ch);
}

function select_token()
{    
    global $db;
    $sql_token = "SELECT UPDATED_TIME, date(UPDATED_TIME),TOKEN
                          FROM tbl_token_details AS a
                          WHERE date(UPDATED_TIME)= (
                          SELECT MAX(date(UPDATED_TIME))
                          FROM tbl_token_details AS b )";
                          //echo $sql_token;
                        $result = $db->RunQuery2($sql_token);
                        $row	= mysqli_fetch_array($result);
                        $token  =  $row['TOKEN'];
                        return  $token;
                        
}*/
?>

 
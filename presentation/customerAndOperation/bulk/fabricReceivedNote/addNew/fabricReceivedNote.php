<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
ini_set('max_execution_time',60000); 
$location 			= $sessions->getLocationId();
$company 			= $sessions->getCompanyId();
$intUser  			= $sessions->getUserId();

include_once "class/cls_commonErrorHandeling_get.php";				$obj_commonErr					= new cls_commonErrorHandeling_get($db);
require_once "class/cls_permisions.php";							$objpermisionget				= new cls_permisions($db);
require_once "class/customerAndOperation/cls_textile_stores.php";	$objtexttile 					= new cls_texttile();
require_once "class/cls_commonFunctions_get.php";					$objcomfunc 					= new cls_commonFunctions_get($db);
require_once "class/tables/ware_fabricreceivedheader.php";			$obj_ware_fabricreceivedheader 	= new ware_fabricreceivedheader($db);
//include_once "class/tables/mst_locations.php";						$mst_locations					= new mst_locations($db);
//include_once "class/tables/mst_plant.php";							$mst_plant						= new mst_plant($db);

$exceedPOPermision 	= $objpermisionget->boolSPermision(5);
$locationType 		= $objcomfunc->getLocationType($location);

$programName		= 'Fabric Received Note';
$STICKER_MAKING      = '-STICKER_MAKING';
$programCode		= 'P0045';
$x_note				= '';
$x_aod				= '';
$serialNo 			= (!isset($_REQUEST["serialNo"])?'':$_REQUEST["serialNo"]);
$year 				= (!isset($_REQUEST["year"])?'':$_REQUEST["year"]);

if($serialNo==''){
	$orderNo 		= (!isset($_REQUEST["orderNo"])?'':$_REQUEST["orderNo"]);
	$orderYear 		= (!isset($_REQUEST["orderYear"])?'':$_REQUEST["orderYear"]);
	$salesOrderNo 	= (!isset($_REQUEST["salesOrderNo"])?'':$_REQUEST["salesOrderNo"]);
}
else{
$sql = "SELECT
ware_fabricreceivedheader.intOrderNo,
ware_fabricreceivedheader.intOrderYear,
ware_fabricreceivedheader.strAODNo,
ware_fabricreceivedheader.strStyleNo,
ware_fabricreceivedheader.strGraphicNo,
ware_fabricreceivedheader.strRemarks,
ware_fabricreceivedheader.intStatus,
ware_fabricreceivedheader.intApproveLevels,
ware_fabricreceivedheader.dtmdate, 
ware_fabricreceivedheader.intStatus , 
ware_fabricreceivedheader.intApproveLevels  
FROM ware_fabricreceivedheader
WHERE
ware_fabricreceivedheader.intFabricReceivedNo =  '$serialNo' AND
ware_fabricreceivedheader.intFabricReceivedYear =  '$year'";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$orderNo 	= $row['intOrderNo'];
$orderYear 	= $row['intOrderYear'];
$strStyleNo 	= $row['strStyleNo'];
$strGraphicNo 	= $row['strGraphicNo'];
$x_aod  	= $row['strAODNo'];
$x_note 	= $row['strRemarks'];
$AODDate	= $row['dtmdate'];
$status 	= $row['intStatus'];
$approveLevels 	= $row['intApproveLevels'];
$savedStat=$row['intApproveLevels'];
$intStatus=$row['intStatus'];
}

if($orderYear==''){
	//$orderYear=date('Y');
	$sql = "SELECT DISTINCT
			trn_orderheader.intOrderYear
			FROM trn_orderheader
			ORDER BY
			trn_orderheader.intOrderYear DESC limit 1";
			$result = $db->RunQuery($sql);
			$row=mysqli_fetch_array($result);
			$orderYear = $row['intOrderYear'];
}

$poNo		 	= (!isset($_REQUEST["poNo"])?'':$_REQUEST["poNo"]);
$customer 		= (!isset($_REQUEST["customer"])?'':$_REQUEST["customer"]);
//----------------
     $sql = "SELECT
trn_orderheader.strCustomerPoNo,
trn_orderheader.intCustomer
FROM trn_orderheader
WHERE
trn_orderheader.intOrderNo =  '$orderNo' AND
trn_orderheader.intOrderYear =  '$orderYear' AND intOrderNo>0"; 
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);

$x_poNo 	= $row['strCustomerPoNo'];
$x_custId 	= $row['intCustomer'];

if($x_poNo=='')
	$x_poNo	= $poNo;

$sql = "SELECT
		Sum(trn_orderdetails.intQty) AS qty, 
		trn_orderdetails.intSampleNo,
		trn_orderdetails.intSampleYear,
		trn_orderdetails.strCombo,
		trn_orderdetails.strPrintName,
		trn_orderdetails.intRevisionNo , 
		trn_orderdetails.strStyleNo , 
		trn_orderdetails.strGraphicNo 
		FROM trn_orderdetails  
		WHERE
		trn_orderdetails.intOrderNo='$orderNo' 
		AND trn_orderdetails.intOrderYear='$orderYear' AND intOrderNo>0 "; 
$sql .= " GROUP BY
		trn_orderdetails.intOrderNo,
		trn_orderdetails.intOrderYear"; 
  // echo $sql;
$sql='';
/*$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$x_styleNo 	= $row['strStyleNo'];
$x_graphicNo 	= $row['strGraphicNo'];
$x_sampleNo 	= $row['intSampleNo'];
$x_sampleYear 	= $row['intSampleYear'];
$x_revNo 		= $row['intRevisionNo'];
$x_combo 		= $row['strCombo'];
$x_print 		= $row['strPrintName'];*/


$sql = "SELECT
Sum(trn_orderdetails.intQty) AS qty  
FROM trn_orderdetails  
WHERE
trn_orderdetails.intOrderNo='$orderNo' 
AND trn_orderdetails.intOrderYear='$orderYear' AND trn_orderdetails.intOrderNo> 0 AND trn_orderdetails.SO_TYPE > -1
";

/*if($salesOrderNo!=''){
$sql .= " AND 
trn_orderdetails.strSalesOrderNo='$salesOrderNo'"; 
}
*/$sql .= " GROUP BY
trn_orderdetails.intOrderNo,
trn_orderdetails.intOrderYear"; 

$result = $db->RunQuery($sql);
$row	= mysqli_fetch_array($result);

$x_qty  = $row['qty'];

//-------------------------		
$x_styleNo 			= (!isset($_REQUEST["styleNo"])?'':$_REQUEST["styleNo"]);
$x_graphicNo 		= (!isset($_REQUEST["graphicNo"])?'':$_REQUEST["graphicNo"]);

/*if(($serialNo=='')&&($year=='')){
	$savedStat = (int)getApproveLevel($programName);
	$intStatus=$savedStat+1;
}

$editMode=loadEditMode($programCode,$intStatus,$savedStat,$intUser);
$confirmatonMode=loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser);*/

$status			= '';
$apprveLevels	= '';
if(($serialNo!='')&&($year!='')){
	$obj_ware_fabricreceivedheader->set($serialNo,$year);
	$status					= $obj_ware_fabricreceivedheader->getintStatus($serialNo,$year);
	$apprveLevels			= $obj_ware_fabricreceivedheader->getintApproveLevels($serialNo,$year);
}

$permition_arr			= $obj_commonErr->get_permision_withApproval_save($status,$apprveLevels,$intUser,$programCode,'RunQuery');
$permision_save			= $permition_arr['permision'];

$permition_arr			= $obj_commonErr->get_permision_withApproval_confirm($status,$apprveLevels,$intUser,$programCode,'RunQuery');
$permision_confirm		= $permition_arr['permision'];
?>

<body>
  <?php
  /*$mst_locations->set($location); 
  $plantId		= $mst_locations->getintPlant();
  
  if($plantId!='')
  {
  	$mst_plant->set($plantId);
  	$plantStatus	= $mst_plant->getintStatus(); 
  }*/
    
  if($locationType < 1){
	$str 		=  "This is not a Production location.So can't Receive Fabrics.";
	$maskClass	= "maskShow";
  ?>
    <div id="divMask" class="<?php echo $maskClass?> mask"> <?php echo $str; ?></div>
 <?php 	return false;
  }
  /* else if($plantId=='')
  {
	$str =  "This location is not allocate to a plant.";
	$maskClass="maskShow";
  ?>
    <div id="divMask" class="<?php echo $maskClass?> mask"> <?php echo $str; ?></div>
 <?php 	return false;
  }
  else if($plantStatus==0)
  {
	$str =  "This location is not allocate to an active plant.";
	$maskClass="maskShow";
  ?>
    <div id="divMask" class="<?php echo $maskClass?> mask"> <?php echo $str; ?></div>
 <?php 	return false;
  }*/
  ?>  

<form id="frmReceivedNote" name="frmReceivedNote" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL" style="width:970px">
		  <div class="trans_text">FABRIC RECEIVED NOTE</div>
		  <table width="976" border="0" align="center" bgcolor="#FFFFFF">
    <td width="970"><table width="100%" border="0">
    <tr><td><table width="100%">
      <tr>
        <td><fieldset class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td width="8%" height="22" class="normalfnt">Serial No</td>
  <td width="21%" class="normalfnt"><input  id="txtSerialNo" class="normalfnt" style="width:70px;text-align:right" type="text" value="<?php echo $serialNo ?>" readonly/><input  id="txtYear" class="normalfnt" style="width:40px;text-align:right" type="text" value="<?php echo $year ?>" readonly/></td>
  <td width="1%" class="normalfnt">&nbsp;</td>
  <td width="18%" class="normalfnt">&nbsp;</td>
  <td width="8%" class="normalfnt">&nbsp;</td>
  <td width="21%" class="normalfnt"><input  id="locationType"  type="hidden" value="<?php echo $locationType ?>"/></td>
  <td width="1%" class="normalfnt">&nbsp;</td>
  <td width="22%" align="right" class="normalfnt">&nbsp;</td>
</tr>        </table></fieldset></td>
      </tr>
    </table></td></tr>
    <tr><td><table width="100%">
      <tr>
        <td>
          <table  bgcolor="#E8FED8" class="tableBorder_allRound"  width="100%" border="0" cellpadding="0" cellspacing="0"  >
            <tr>
              <td width="11%" height="22" class="normalfnt">&nbsp;Year</td>
              <td width="17%" class="normalfnt"><select name="cboOrderYear" id="cboOrderYear" style="width:70px"<?php /*?> class="search"<?php */?>>
                <?php
				
					$sql = "SELECT DISTINCT
							trn_orderheader.intOrderYear
							FROM trn_orderheader
							ORDER BY
							trn_orderheader.intOrderYear DESC";
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						if(($i==0) &&($orderYear==''))
						$orderYear = $row['intOrderYear'];
						
						
						if($row['intOrderYear']==$orderYear)
						echo "<option value=\"".$row['intOrderYear']."\" selected=\"selected\">".$row['intOrderYear']."</option>";	
						else
						echo "<option value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";	
						$i++;
					}
				?>
                </select></td>
              <td width="10%" class="normalfnt">Style No</td>
              <td width="31%" class="normalfnt"><select name="cboStyle" id="cboStyle" style="width:130px" class="search">
                <?php
                $html1="<option value=\"\"></option>";
				$html='';
					$sql = "SELECT DISTINCT
							trn_orderdetails.strStyleNo
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
							Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId 
							WHERE
							trn_orderheader.intStatus =  '1'  AND trn_orderheader.PO_TYPE IN (2,0) AND
							trn_orderdetails.intOrderYear='$orderYear'  AND trn_orderheader.intOrderNo>=0 AND mst_locations.intCompanyId =  '$company'  ";
					if($orderNo>0){		
					$sql .= " AND trn_orderdetails.intOrderNo='$orderNo' ";	
					}
                    if($locationType==2) {
                        $sql.=" AND trn_orderdetails.SO_TYPE > -1 ";
                    } else if ($locationType==1){
                        $sql.=" AND (trn_orderdetails.TECHNIQUE_GROUP_ID NOT IN (5,6) OR trn_orderdetails.SO_TYPE = -1  )";
                    }
					$sql .= " 
							   
							ORDER BY trn_orderdetails.strStyleNo ASC";
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						$i++;
						if($row['strStyleNo']==$strStyleNo)
						$html .= "<option value=\"".$strStyleNo ."\" selected=\"selected\">".$strStyleNo."</option>";	
						else
						$html .= "<option value=\"".$row['strStyleNo']."\">".$row['strStyleNo']."</option>";	
						if(($i==1) && ($x_styleNo==''))
						{
							$x_styleNoT=$row['strStyleNo'];
						}
					}
					
					if(($orderNo!='') && ($x_styleNo==''))
					{
					$x_styleNo=$x_styleNoT;
					}
					
					if($orderNo==''){
						$html=$html1.$html;
					}
					else if($i>=1){
						$html=$html.$html1;
					}
					else if($i==0){
						$html=$html1;
					}
					echo $html;
				?></select></td>
              <td width="8%" class="normalfnt">Graphic No</td>
              <td width="23%" align="right" class="normalfnt"><select name="cboGraphicNo" id="cboGraphicNo" style="width:120px" class="search">
                <?php
                $html1="<option value=\"\"></option>";
				$html='';
					$sql = "SELECT DISTINCT
							trn_orderdetails.strGraphicNo
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
							Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId 
							WHERE
							trn_orderheader.intStatus =  '1' AND trn_orderheader.PO_TYPE IN (2,0)   AND 
							trn_orderdetails.intOrderYear='$orderYear'  AND trn_orderheader.intOrderNo>0   
							AND mst_locations.intCompanyId =  '$company' ";
					if($orderNo>0){		
					$sql .= " AND trn_orderdetails.intOrderNo='$orderNo'";	
					}
                    if($locationType==2) {
                        $sql.=" AND trn_orderdetails.SO_TYPE > -1 ";
                    } else if ($locationType==1){
                        $sql.=" AND (trn_orderdetails.TECHNIQUE_GROUP_ID NOT IN (5,6) OR trn_orderdetails.SO_TYPE = -1  )";
                    }
					if($strStyleNo!='')
					$sql .= " AND trn_orderdetails.strStyleNo='$strStyleNo' ";	
					else
					$sql .= " AND trn_orderdetails.strStyleNo='$x_styleNo' ";
					
					$sql .= " 
							 
							ORDER BY trn_orderdetails.strGraphicNo ASC";
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						$i++;
						if($row['strGraphicNo']==$strGraphicNo)
						$html .= "<option value=\"".$strGraphicNo."\" selected=\"selected\">".$strGraphicNo."</option>";	
						else
						$html .= "<option value=\"".$row['strGraphicNo']."\">".$row['strGraphicNo']."</option>";	
						if(($i==1) && ($x_graphicNo=='')){
							$x_graphicNoT=$row['strGraphicNo'];
						}
					}
					
					if(($orderNo!='') && ($x_graphicNo=='')){
					$x_graphicNo=$x_graphicNoT;
					}
					
					if($orderNo==''){
						$html=$html1.$html;
					}
					else if($i>=1){
						$html=$html.$html1;
					}
					else if($i==0){
						$html=$html1;
					}
					echo $html;
				?></select></td> </tr>
<tr>
              <td height="22" class="normalfnt">Customer PO</td>
              <td class="normalfnt"><select name="cboPONo" id="cboPONo" style="width:130px" class="search">
                <option value=""></option>
                <?php
					$sql = "SELECT DISTINCT
							trn_orderheader.strCustomerPoNo
							FROM trn_orderheader Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
							where intStatus=1   AND 
							trn_orderheader.intOrderYear='$orderYear' AND trn_orderheader.intOrderNo>0  
							AND trn_orderheader.strCustomerPoNo <> ''  AND mst_locations.intCompanyId =  '$company'
							ORDER BY
							trn_orderheader.strCustomerPoNo ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if(($row['strCustomerPoNo']==$x_poNo) && ($orderNo!=''))
						echo "<option value=\"".$row['strCustomerPoNo']."\" selected=\"selected\">".$row['strCustomerPoNo']."</option>";	
						else
						echo "<option value=\"".$row['strCustomerPoNo']."\">".$row['strCustomerPoNo']."</option>";	
					}
				?>
                </select></td>
              <td class="normalfnt">Customer</td>
              <td colspan="4" class="normalfnt"><select style="width:295px" name="cboCustomer" id="cboCustomer"   class="validate[required] search" >
                <option value=""></option>
                <?php
					$sql = "SELECT DISTINCT 
							trn_orderheader.intCustomer,
							mst_customer.strName
							FROM
							trn_orderheader
							Inner Join mst_customer ON trn_orderheader.intCustomer = mst_customer.intId 
							where  
							trn_orderheader.intOrderYear='$orderYear' AND trn_orderheader.intOrderNo>0  
							ORDER BY mst_customer.strName ASC";
							$pp=$sql;
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intCustomer']==$x_custId)
						echo "<option value=\"".$row['intCustomer']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intCustomer']."\">".$row['strName']."</option>";	
					}
				?>
                </select></td>
            </tr> 
<tr>
              <td height="22" class="normalfnt">Order No</td>
              <td class="normalfnt"><select name="cboOrderNo" id="cboOrderNo" style="width:130px" >
                <option value=""></option>
                <?php

					$sql = "SELECT DISTINCT trn_orderheader.intOrderNo 
					FROM trn_orderdetails 
					Inner Join trn_orderheader 
					ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear 
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo 
					AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo 
					left Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
					where trn_orderheader.intStatus = 1  AND trn_orderheader.PO_TYPE IN (2,0)
					AND mst_locations.intCompanyId =  '$company' ";
					if($orderYear!='')
					$sql.=" AND trn_orderheader.intOrderYear 	=  '$orderYear'  ";
					
				//	if($orderNo!=''){

					if($strGraphicNo!='')
					$sql.=" AND trn_orderdetails.strGraphicNo =  '$strGraphicNo'  ";
					else if($x_graphicNo!='')
					$sql.=" AND trn_orderdetails.strGraphicNo =  '$x_graphicNo'  ";
					if($strStyleNo!='')
					$sql.=" AND trn_orderdetails.strStyleNo =  '$strStyleNo'  ";
					else if($x_styleNo!='')
					$sql.=" AND trn_orderdetails.strStyleNo =  '$x_styleNo'  ";
					if($x_poNo!='')
					//$sql.=" AND trn_orderheader.strCustomerPoNo =  '$x_poNo' ";
					if($salesOrderNo!=''){		
					//$sql .= " AND trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'";		
					}
				//	}
					$sql .= "ORDER BY trn_orderheader.intOrderYear DESC, trn_orderheader.intOrderNo DESC";
					  //  echo $sql;
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intOrderNo']==$orderNo)
						echo "<option value=\"".$row['intOrderNo']."\" selected=\"selected\">".$row['intOrderNo']."</option>";	
						else
						echo "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";	
					}
				?>
              </select><?php // echo $sql; ?></td>
              <td class="normalfnt">Sales Order No</td>
              <td colspan="4" class="normalfnt"><select name="cboSalesOrderNo" id="cboSalesOrderNo" style="width:130px" >
                <option value=""></option>
                <?php

				 	$sql = "SELECT DISTINCT 
							trn_orderdetails.strSalesOrderNo,
							trn_orderdetails.SO_TYPE
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear  
							Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
							WHERE trn_orderheader.intStatus = 1 AND trn_orderheader.PO_TYPE IN (2,0)
							AND mst_locations.intCompanyId =  '$company' ";
							 
					if($orderYear!='')
					$sql.=" AND trn_orderheader.intOrderYear  =  '$orderYear'  ";
					if($strGraphicNo!='')
					$sql.=" AND trn_orderdetails.strGraphicNo =  '$strGraphicNo'  ";
					else if($x_graphicNo!='')
					$sql.=" AND trn_orderdetails.strGraphicNo =  '$x_graphicNo'  ";
					if($strStyleNo!='')
					$sql.=" AND trn_orderdetails.strStyleNo   =  '$strStyleNo'  ";
					else if($x_styleNo!='')
					$sql.=" AND trn_orderdetails.strStyleNo   =  '$x_styleNo'  ";
                    if($locationType==2) {
                        $sql.=" AND trn_orderdetails.SO_TYPE > -1 ";
                    } else if ($locationType==1){
                        $sql.=" AND (trn_orderdetails.TECHNIQUE_GROUP_ID NOT IN (5,6) OR trn_orderdetails.SO_TYPE = -1  )";
                    }

					if($x_poNo!='')
					//$sql.=" AND trn_orderheader.strCustomerPoNo =  '$x_poNo' ";
					if($orderNo!=''){		
					$sql .= " AND trn_orderheader.intOrderNo =  '$orderNo'";		
					}
					$sql .= "
							ORDER BY trn_orderdetails.strSalesOrderNo ASC";


					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
                        $soName = $row['strSalesOrderNo'];
					    if ($row['SO_TYPE'] == 2){
                            $soName = $soName.$STICKER_MAKING;
                        }
						if($row['strSalesOrderNo']==$salesOrderNo)
						echo "<option value=\"".$row['strSalesOrderNo']."\" selected=\"selected\">".$soName."</option>";
						else
						echo "<option value=\"".$row['strSalesOrderNo']."\">".$soName."</option>";
					}
				?>
              </select></td>
            </tr>            </table><table style="margin-top:5px"  bgcolor="" class="tableBorder_allRound"  width="100%" border="0" cellpadding="0" cellspacing="0"  >
            <tr>
              <td width="1%" class="normalfnt">&nbsp;</td>
              <td width="10%" height="22" class="normalfnt">AOD No</td>
              <td width="17%" class="normalfnt"><input name="txtAODNo" type="text" class="validate[required] normalfnt"  id="txtAODNo" style="width:103px;text-align:left" value="<?php echo $x_aod ?>"/></td>
              <td width="10%" class="normalfnt">AOD Date</td>
              <td class="normalfnt" colspan="2"><input name="dtDate" type="text" value="<?php if($serialNo){ echo substr($AODDate,0,10); }else { echo date("Y-m-d"); }?>" class="txtbox" id="dtDate" style="width:102px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              <td width="8%" class="normalfnt">Total Qty</td>
              <td width="23%" class="normalfnt"><input  id="txtQty" class="validate[required,custom[number]] invoQty normalfnt" style="width:120px;text-align:right" type="text" value="<?php echo $x_qty ?>" disabled="disabled"/></td>
                
            
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td height="22" class="normalfnt">Remarks</td>
                <td class="normalfnt" colspan="4"><textarea name="txtNote" id="txtNote" rows="3" style="width:440px"><?php echo $x_note; ?></textarea></td>
                <td class="normalfnt" valign="top">&nbsp;</td>
                <td class="normalfnt" valign="top">&nbsp;</td>
              </table>
       </td>
      </tr>
      <tr>
        <td class="normalfntRight" align="right"><img src="images/Tadd.jpg" name="butInsertRowPopup" width="78" height="24" class="mouseover" id="butInsertRowPopup" /></td></tr>
      </table></td></tr>
      <tr>
        <td><div style="width:960px;height:300px;overflow:scroll" class="tableBorder_allRound" >
          <table width="940" class="grid" id="tblMain" >
            <tr class="gridHeader">
              <td width="6%" >Del</td>
              <td width="12%" >Cut No</td>
              <td width="18%" >Sales Order No</td>
              <td width="13%" >Part</td>
              <td width="18%" >Background Color</td>
              <td width="12%" >Line No</td>
              <td width="9%" >Size</td>
              <td width="12%" > Qty</td>
              </tr>
              <?php
			  	$total	= 0;
				$totAmm	= 0;
				$result=loadDetails($serialNo,$year,$orderNo,$orderYear);
				while($row=mysqli_fetch_array($result))
				{
					$cutNo				= $row['strCutNo'];
					$salesOrderId		= $row['intSalesOrderId'];
					$salesOrderNo		= $row['strSalesOrderNo'];
					$partId				= $row['intPart'];
					$part				= $row['part'];
					$bgColoId			= $row['intGroundColor'];
					$bgColor			= $row['bgcolor'];
					$lineNo				= $row['strLineNo'];
					$size				= $row['strSize'];
					$qty				= $row['dblQty'];
					$orderQty			= $row['orderQty'];
					$grade				= '';
					$receivedQty		= loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade);
					$nonstckConfQty		= loadNonstckConfQty($location,$orderNo,$orderYear,$salesOrderId,$size);
				    $orderQty			= loadOrderQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade);
					
					$toleratePercentage	= $objtexttile->getSavedTolerencePercentage($orderNo,$orderYear,$salesOrderId);
					$excessQty			= loadSizeWiseExcessQty($orderNo,$orderYear,$salesOrderId,$size,$toleratePercentage);
				    $fdQty				= loadDamageReturnedQty($location,$orderNo,$orderYear,$salesOrderId,$size);

					$maxQty				= ceil($orderQty-$receivedQty-$nonstckConfQty+$excessQty+$fdQty);
			  ?>
<tr class="normalfnt">
  <td align="center" bgcolor="#FFFFFF" id=""><img class="delImg" src="images/del.png" width="15" height="15" /></td>
  <td align="center" bgcolor="#FFFFFF" id="<?php echo $cutNo; ?>" class="cutNo"><?php echo $cutNo; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $salesOrderId; ?>"  class="salesOrderNo"><?php echo $salesOrderNo; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $partId; ?>" class="part"><?php echo $part; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $bgColoId; ?>" class="bgColor"><?php echo $bgColor; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $lineNo; ?>" class="line"><?php echo $lineNo; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $size; ?>" class="size"><?php echo $size; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $maxQty; ?>"><input id="1" class="validate[required,custom[integer]<?php if($exceedPOPermision==''){ ?>,max[<?php echo $maxQty ?>] <?php } ?>] qty" type="text" value="<?php echo $qty; ?>" style="width:80px;text-align:center"></td>
            </tr>       
             <?php
			 $total		+= $qty;
				//	}
				}
			  ?>  
         </table>
        </div></td></tr>
     <tr>
        <td align="center" class=""><table width="100%" border="0">
          <tr>
            <td width="17%" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td width="38%">&nbsp;</td>
            <td width="12%" class="normalfnt">Total</td>
            <td width="9%" align="right" class="tableBorder normalfntRight"><span id="divTotal" style="text-align:right"><?php echo $total ?></span>&nbsp;</td>
            </tr>
          
          </table></td>
      </tr>      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound">
        <img src="images/Tnew.jpg" width="92" height="24" id="butNew" name="butNew"  class="mouseover"/>
		<img src="images/Tsave.jpg" width="92" height="24" id="butSave" name="butSave"  class="mouseover" <?php if($permision_save!=1){ ?>style="display:none"<?php } ?>/>
		<img class="mouseover" src="images/Tconfirm.jpg" width="92" height="24" id="butConfirm" name="butConfirm" <?php if($permision_confirm!=1){ ?>style="display:none"<?php } ?> />
        <img src="images/Treport.jpg" width="92" height="24" id="butReport" name="butReport"  class="mouseover"/><a href="main.php"><img src="images/Tclose.jpg" width="92" height="24"  class="mouseover"/></a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>


</body>
</html>
<?php
	//--------------------------------------------------------------
function loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade)
{
		global $db;
		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				/*ware_stocktransactions_fabric.intLocationId =  '$location' AND*/
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType =  'Received'
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return (float)($rows['dblQty']);

}
//--------------------------------------------------------------
	function loadNonstckConfQty($location,$orderNo,$orderYear,$salesOrderId,$size)
{
		global $db;
	  	$sql = "SELECT
				Sum(ware_fabricreceiveddetails.dblQty) as dblQty 
				FROM
				ware_fabricreceivedheader
				Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
				WHERE
				ware_fabricreceivedheader.intStatus >  1 AND
				ware_fabricreceivedheader.intStatus <=  ware_fabricreceivedheader.intApproveLevels AND
				ware_fabricreceivedheader.intOrderNo =  '$orderNo' AND
				ware_fabricreceivedheader.intOrderYear =  '$orderYear' AND
				ware_fabricreceiveddetails.intSalesOrderId =  '$salesOrderId' AND
				ware_fabricreceiveddetails.strSize =  '$size'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return (float)($rows['dblQty']);
}
//--------------------------------------------------------------
	
function loadDetails($serialNo,$year,$orderNo,$orderYear){
		global $db; 
	  	$sql = "SELECT
				trn_orderdetails.strSalesOrderNo, 
				trn_orderdetails.intQty as orderQty, 
				ware_fabricreceiveddetails.strCutNo,
				mst_part.strName as part,
				mst_colors_ground.strName as bgcolor,
				ware_fabricreceiveddetails.strLineNo,
				ware_fabricreceiveddetails.strSize,
				ware_fabricreceiveddetails.dblQty,
				ware_fabricreceiveddetails.intSalesOrderId,
				ware_fabricreceiveddetails.intPart,
				ware_fabricreceiveddetails.intGroundColor 
				FROM
				trn_orderdetails
				Inner Join ware_fabricreceivedheader ON ware_fabricreceivedheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricreceivedheader.intOrderYear = trn_orderdetails.intOrderYear
				Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear AND ware_fabricreceiveddetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
				left Join mst_part ON ware_fabricreceiveddetails.intPart = mst_part.intId
				left Join mst_colors_ground ON ware_fabricreceiveddetails.intGroundColor = mst_colors_ground.intId
				WHERE
				ware_fabricreceivedheader.intFabricReceivedNo =  '$serialNo' AND
				ware_fabricreceivedheader.intFabricReceivedYear =  '$year' 
				ORDER BY 
				ware_fabricreceiveddetails.strCutNo ASC, 
				trn_orderdetails.strSalesOrderNo ASC, 
				mst_part.strName ASC, 
				mst_colors_ground.strName ASC, 
				ware_fabricreceiveddetails.strLineNo ASC,
				ware_fabricreceiveddetails.strSize ASC 
				";

			return $result = $db->RunQuery($sql);
	}
//-----------------------------------------------------------
	function loadSizeWiseExcessQty($orderNo,$orderYear,$salesOrderId,$size,$excessFactor)
{
		global $db;
	        	$sql = "SELECT
				trn_ordersizeqty.dblQty,
				trn_orderdetails.dblOverCutPercentage,
				trn_orderdetails.dblDamagePercentage,
				DATEDIFF(date(trn_orderheader.dtmCreateDate),date('2017-05-18')) AS DiffDate
				FROM trn_orderdetails 
				INNER JOIN trn_orderheader on 
	trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo and trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
				Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_orderdetails.intSalesOrderId =  '$salesOrderId' AND
				trn_ordersizeqty.strSize =  '$size' ";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		if($rows['DiffDate']< 0)
		$value=$rows['dblQty']*($rows['dblOverCutPercentage']+$excessFactor)/100;
		else
		$value=$rows['dblQty']*($rows['dblOverCutPercentage']+$rows['dblDamagePercentage'])/100;
	
		return (float)$value;
}
//-----------------------------------------------------------
function loadDamageReturnedQty($location,$orderNo,$orderYear,$salesOrderId,$size)
{
		global $db;
		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty*-1) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				/*ware_stocktransactions_fabric.intLocationId =  '$location' AND*/
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType in(  'Dispatched_F' ,'Dispatched_P' ,'Dispatched_CUT_RET')
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return (float)($rows['dblQty']);
}

//-----------------------------------------------------------
function loadOrderQty($location,$orderNo,$orderYear,$salesOrderNo,$size,$grade)
{
		global $db;
		  $sql = "SELECT
				trn_ordersizeqty.dblQty
				FROM trn_ordersizeqty
				WHERE
				trn_ordersizeqty.intOrderNo =  '$orderNo' AND
				trn_ordersizeqty.intOrderYear =  '$orderYear' AND
				trn_ordersizeqty.intSalesOrderId =  '$salesOrderNo' AND
				trn_ordersizeqty.strSize =  '$size'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return (float)($rows['dblQty']);
}
//-----------------------------------------------------------
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//--------------------------------------------------------

?>

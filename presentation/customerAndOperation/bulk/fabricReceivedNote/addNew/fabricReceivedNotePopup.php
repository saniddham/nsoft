<?php
session_start();
$backwardseperator = "../../../../../";

include $backwardseperator."dataAccess/Connector.php";

$orderNo  		= $_REQUEST['orderNo'];
$orderYear  	= $_REQUEST['orderYear'];
$serialNo  		= $_REQUEST['serialNo'];
$year  			= $_REQUEST['orderYear'];
$styleNo  		= $_REQUEST['styleNo'];
$graphicNo  	= $_REQUEST['graphicNo'];
$custPONo  		= $_REQUEST['custPONo'];
$salesOrderNo  	= $_REQUEST['salesOrderNo'];
$STICKER_MAKING      = '-STICKER_MAKING';
$x_salesOrderNo	= '';
$locationType 		= $_REQUEST['locationType'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Items</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

</head>

<body>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>

    
<form id="frmFabricRecvNotePopup" name="frmFabricRecvNotePopup" method="post" action="">
<table width="500" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutD" style="width:700px">
		  <div class="trans_text">  Allocation</div>
		  <table width="650" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="600" border="0">
      <tr>
        <td><table width="650" border="0" cellpadding="0" cellspacing="0" class="tableBorder">
<tr>
            <td width="1%" height="22" class="normalfnt">&nbsp;</td>
            <td width="9%" class="normalfnt">&nbsp;</td>
            <td width="17%" class="normalfnt">Year</td>
            <td width="2%" class="normalfnt">: </td>
            <td width="24%" class="normalfnt"><div id="itemMain"><?php echo $year  ;
 ?></div></td>
            <td width="1%" align="right">&nbsp;</td>
            <td width="1%" align="right"><div id="itemIdMain" style="display:none"><?php echo $itemId; ?></div></td>
            <td width="13%" class="normalfnt">Order No</td>
            <td width="2%" class="normalfnt">: </td>
            <td width="19%" class="normalfnt"><div id="itemMain"><?php echo $orderNo  ;
 ?></div></td>
            <td width="7%" class="normalfnt">&nbsp;</td>
            <td width="4%" class="normalfnt">&nbsp;</td>
            </tr>  
            <tr id="rw1">
            <td width="1%" height="22" class="normalfnt">&nbsp;</td>
            <td width="9%" class="normalfnt">&nbsp;</td>
            <td width="17%" class="normalfnt">Customer PO</td>
            <td width="2%" class="normalfnt">: </td>
            <td width="24%" class="normalfnt"><div id="balToInvoice"><?php echo $custPONo;

 ?></div></td>
            <td width="1%" class="normalfnt"></td>
            <td width="1%" align="right">&nbsp;</td>
            <td width="13%" class="normalfnt">&nbsp;</td>
            <td width="2%" class="normalfnt">&nbsp;</td>
            <td width="19%" class="normalfnt"><div id="itemMain"></div><input  id="locationType"  type="hidden" value="<?php echo $locationType ?>"/></td>
            <td width="7%" class="normalfnt">&nbsp;</td>
            <td width="4%" class="normalfnt">&nbsp;</td>
            </tr>   
<tr id="rw1">
            <td width="1%" height="23" class="normalfnt">&nbsp;</td>
            <td width="9%" class="normalfnt">&nbsp;</td>
            <td width="17%" class="normalfnt">Style No </td>
            <td width="2%" class="normalfnt">&nbsp;</td>
            <td width="24%" class="normalfnt"><select name="cboStyleP" style="width:120px" id="cboStyleP" class="searchP">
              <?php
					 $sql = "SELECT DISTINCT 
							trn_orderdetails.strStyleNo
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
							WHERE 
							trn_orderheader.intStatus = '1' AND 
							trn_orderdetails.intOrderNo='$orderNo' 
							AND trn_orderdetails.intOrderYear='$orderYear'
							ORDER BY 
							trn_orderdetails.strSalesOrderNo ASC";
			
					$html = "<option value=\"\"></option>";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{		
						   if($styleNo==$row['strStyleNo'])
						   {
							 	$html .= "<option selected value=\"".$styleNo."\">".$styleNo."</option>";  
						   }else
						   {
								$html .= "<option value=\"".$row['strStyleNo']."\">".$row['strStyleNo']."</option>";
						   }
 					}
					echo $html;
				?>
            </select></td>
            <td width="1%" class="normalfnt"></td>
            <td width="1%" align="right">&nbsp;</td>
            <td width="13%" class="normalfnt">Graphic</td>
            <td width="2%" class="normalfnt">&nbsp;</td>
            <td width="19%" class="normalfnt"><select name="cboGraphicP" style="width:120px" id="cboGraphicP" class="searchP">
              <?php
					 $sql = "SELECT DISTINCT 
							trn_orderdetails.strGraphicNo
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
							WHERE 
							trn_orderheader.intStatus = '1' AND 
							trn_orderdetails.intOrderNo='$orderNo' 
							AND trn_orderdetails.intOrderYear='$orderYear'
							ORDER BY 
							trn_orderdetails.strGraphicNo ASC";
			
					$html = "<option value=\"\"></option>";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{		 if($graphicNo==$row['strGraphicNo'])
						   {
							 	$html .= "<option selected value=\"".$graphicNo."\">".$graphicNo."</option>";  
						   }else
						   {
						    
							$html .= "<option value=\"".$row['strGraphicNo']."\">".$row['strGraphicNo']."</option>";
						   }
 					}
					echo $html;
				?>
            </select></td>
            <td width="7%" class="normalfnt">&nbsp;</td>
            <td width="4%" class="normalfnt">&nbsp;</td>
            </tr>
            <tr id="rw1">
            <td width="1%" height="22" class="normalfnt">&nbsp;</td>
            <td width="9%" class="normalfnt">&nbsp;</td>
            <td width="17%" class="normalfnt">Sales Order No</td>
            <td width="2%" class="normalfnt">&nbsp;</td>
            <td width="24%" class="normalfnt"><select name="cboSalesOrderNoP" style="width:120px" id="cboSalesOrderNoP" class="searchP">
                  <?php
					 $sql = "SELECT DISTINCT 
							trn_orderdetails.strSalesOrderNo,
							trn_orderdetails.SO_TYPE
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
							WHERE 
							trn_orderheader.intStatus = '1' AND 
							trn_orderdetails.intOrderNo='$orderNo' 
							AND trn_orderdetails.intOrderYear='$orderYear' ";
					 if ($locationType ==2){
                         $sql.= "AND trn_orderdetails.SO_TYPE > -1 ";
                     } else if ($locationType ==1){
                         $sql.=" AND (trn_orderdetails.TECHNIQUE_GROUP_ID NOT IN (5,6) OR trn_orderdetails.SO_TYPE = -1  )";
                     }
					 $sql.= "ORDER BY 
							trn_orderdetails.strSalesOrderNo ASC";
			
					$html = "<option value=\"\"></option>";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
					    $soName = $row['strSalesOrderNo'];
                        if ($row['SO_TYPE'] == 2){
                            $soName = $soName.$STICKER_MAKING;
                        }
						if($salesOrderNo==$row['strSalesOrderNo']){
							$html .= "<option value=\"".$row['strSalesOrderNo']."\" selected=\"selected\">".$soName."</option>";
						}
						else{
							$html .= "<option value=\"".$row['strSalesOrderNo']."\">".$soName."</option>";
						}
					}
					echo $html;
				?>
            </select></td>
            <td width="1%" class="normalfnt"></td>
            <td width="1%" align="right">&nbsp;</td>
            <td width="13%" class="normalfnt">Line No</td>
            <td width="2%" class="normalfnt">&nbsp;</td>
            <td width="19%" class="normalfnt"><div id="itemMain">
              <select name="cboLineNo" style="width:120px" id="cboLineNo" class="">
                <?php
		$sql = "SELECT DISTINCT 
				trn_orderdetails.strLineNo
				FROM
				trn_orderdetails 
				Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
				WHERE 
				trn_orderheader.intStatus = '1' AND 
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' ";
        if ($locationType ==2){
            $sql.= " AND trn_orderdetails.SO_TYPE > -1 ";
        } else if ($locationType ==1){
            $sql.=" AND (trn_orderdetails.TECHNIQUE_GROUP_ID NOT IN (5,6) OR trn_orderdetails.SO_TYPE = -1  )";
        }
				if($salesOrderNo!=''){
				$sql .= " AND trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'";
				}
				$sql .= " ORDER BY trn_orderdetails.strLineNo ASC"; 
				
					$html = "<option value=\"\"></option>";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{	  
						$html .= "<option value=\"".$row['strLineNo']."\">".$row['strLineNo']."</option>";
					}
					echo $html;
				?>
              </select>
            </div></td>
            <td width="7%" class="normalfnt">&nbsp;</td>
            <td width="4%" class="normalfnt">&nbsp;</td>
            </tr>
<tr id="rw1">
            <td width="1%" height="22" class="normalfnt">&nbsp;</td>
            <td width="9%" class="normalfnt">&nbsp;</td>
            <td width="17%" class="normalfnt">Part No</td>
            <td width="2%" class="normalfnt">&nbsp;</td>
            <td width="24%" class="normalfnt"><select name="cboPartNo" style="width:120px" id="cboPartNo" class="">
			<?php
                    $sql = "SELECT DISTINCT
                            trn_orderdetails.intPart,
                            mst_part.strName
                            FROM
                            trn_orderdetails
                            Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
							WHERE 
							trn_orderheader.intStatus = '1' AND 
                            trn_orderdetails.intOrderNo =  '$orderNo' AND
                            trn_orderdetails.intOrderYear =  '$orderYear'  ";
                            if ($locationType ==2){
                                $sql.= " AND trn_orderdetails.SO_TYPE > -1 ";
                            } else if ($locationType ==1){
                                $sql.=" AND (trn_orderdetails.TECHNIQUE_GROUP_ID NOT IN (5,6) OR trn_orderdetails.SO_TYPE = -1  )";
                            }
							if($salesOrderNo!=''){
							$sql .= " AND trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'";
							}
							$sql .= " ORDER BY mst_part.strName ASC"; 
                             
                    $html = "<option value=\"\"></option>";
                    $result = $db->RunQuery($sql);
                    while($row=mysqli_fetch_array($result))
                    {
                            $html .= "<option value=\"".$row['intPart']."\">".$row['strName']."</option>";
                    }
					echo $html;
            ?>  
            </select></td>
            <td width="1%" class="normalfnt"></td>
            <td width="1%" align="right">&nbsp;</td>
            <td width="13%" class="normalfnt">Size</td>
            <td width="2%" class="normalfnt">&nbsp;</td>
            <td width="19%" class="normalfnt"><div id="itemMain">
              <select name="cboSizes" style="width:120px" id="cboSizes" class="">
               <?php
				$sql = "SELECT DISTINCT
						trn_ordersizeqty.strSize
						FROM
						trn_orderdetails
						Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId  
						Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
						WHERE 
						trn_orderheader.intStatus = '1' AND 
						trn_orderdetails.intOrderNo =  '$orderNo' AND
						trn_orderdetails.intOrderYear =  '$orderYear'  ";
						if($salesOrderNo!=''){
						$sql .= " AND trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'";
						}
                       if ($locationType ==2){
                           $sql.= " AND trn_orderdetails.SO_TYPE > -1 ";
                       } else if ($locationType ==1){
                           $sql.=" AND (trn_orderdetails.TECHNIQUE_GROUP_ID NOT IN (5,6) OR trn_orderdetails.SO_TYPE = -1  )";
                       }
						$sql .= " ORDER BY trn_ordersizeqty.strSize ASC"; 
				$html = "<option value=\"\"></option>";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
						$html .= "<option value=\"".$row['strSize']."\">".$row['strSize']."</option>";
				}
				echo $html;
			   ?>
              </select>
            </div>
              </td>
            <td width="7%" class="normalfnt"><img src="images/aad.png" width="19" height="19" alt="add new sizes" id="butAddSizes" name="butAddSizes" class="mouseover" /></td>
            <td width="4%" class="normalfnt">&nbsp;</td>
            </tr>
<tr id="rw1">
            <td width="1%" height="22" class="normalfnt">&nbsp;</td>
            <td width="9%" class="normalfnt">&nbsp;</td>
            <td width="17%" class="normalfnt">Cut No</td>
            <td width="2%" class="normalfnt">&nbsp;</td>
            <td width="24%" class="normalfnt"><span class="qtyCellP">
              <input name="txtCutNo" type="text" class="validate[required] cutNoP"  id="txtCutNo" style="width:120px;text-align:right" value="<?php if (isset($invoiceQty)) {
                  echo $invoiceQty;
              } else {
                  echo '';
              } ?>"/>
            </span></td>
            <td width="1%" class="normalfnt"></td>
            <td width="1%" align="right">&nbsp;</td>
            <td width="13%" class="normalfnt">Color</td>
            <td width="2%" class="normalfntRight"></td>
            <td width="19%" class="normalfntLeft"><select name="cboColor" style="width:120px" id="cboColor" class="">
              <?php
		$sql="SELECT DISTINCT  
			trn_sampleinfomations_details.intGroundColor,
			mst_colors_ground.strName as bgColor 
			FROM
			trn_orderdetails
			Inner Join trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
			Inner Join trn_sampleinfomations_details ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_details.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_details.intRevNo AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
			left Join mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
			WHERE
			trn_orderdetails.intOrderNo =  '$orderNo' AND
			trn_orderdetails.intOrderYear =  '$orderYear'"; 
			if($salesOrderNo!=''){
			$sql .= " AND 
					trn_orderdetails.strSalesOrderNo='$salesOrderNo'"; 
			}
              if ($locationType ==2){
                  $sql.= " AND trn_orderdetails.SO_TYPE > -1 ";
              } else if ($locationType ==1){
                  $sql.=" AND (trn_orderdetails.TECHNIQUE_GROUP_ID NOT IN (5,6) OR trn_orderdetails.SO_TYPE = -1  )";
              }
			$sql .= " ORDER BY 
					mst_colors_ground.strName ASC ";
				$html = "<option value=\"\"></option>";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
						$html .= "<option value=\"".$row['intGroundColor']."\">".$row['bgColor']."</option>";
				}
				echo $html;
			   ?>
            </select></td>
            <td width="7%" class="normalfntLeft">&nbsp;</td>
            <td width="4%" class="normalfntLeft"><img src="images/smallSearch.png" width="24" height="24" class="mouseover" id="imgSearchItems" /></td>
            </tr>          
</table></td>
      </tr>
      <?php //echo $pp
	  ?>
      <tr>
        <td><div style="width:700px;height:300px;overflow:scroll" >
          <table width="679" class="grid" id="tblPopup" >
                        <tr class="gridHeader">
              <td width="5%" ><input type="checkbox" name="chkAll" id="chkAll" /></td>
              <td width="15%" >Sales Order No</td>
              <td width="16%" >Part</td>
              <td width="25%" >Back Ground Color</td>
              <td width="9%">Line No</td>
              <td width="10%">Size</td>
              <td width="9%"> Qty</td>
              <td width="11%"> Bal Rcv Qty</td>
              </tr>

            </table>
          </div></td>
      </tr>
        <td align="center" class="tableBorder_allRound"><img src="images/Tadd.jpg" width="92" height="24"  alt="add" id="butAdd" name="butAdd" class="mouseover"/><img src="images/delete.png" width="92" height="24"  alt="add" id="butDelete" name="butDelete" class="mouseover" style="display:none"/><img src="images/Tclose.jpg" width="92" height="24" id="butClose1" name="butClose1" class="mouseover"  /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<div    style="width:900px; position: absolute;display:none;z-index:200"  id="popupContact2">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup2"></div>
</body>
</html>

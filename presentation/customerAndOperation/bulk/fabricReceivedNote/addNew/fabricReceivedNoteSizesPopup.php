<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];

include $backwardseperator."dataAccess/Connector.php";
require_once $_SESSION['ROOT_PATH']."class/customerAndOperation/cls_textile_stores.php";
require_once $_SESSION['ROOT_PATH']."class/cls_permisions.php";
	
//---------check Permission to edit size wise PO qty.------------	
$objpermisionget= new cls_permisions($db);
$editSizeWisePOqtyPermision 	= $objpermisionget->boolSPermision(7);
//------------------------------------------------------------------------


$objtexttile = new cls_texttile();

$programName='Fabric Received Note';
$programCode='P0045';

$orderNo  = $_REQUEST['orderNo'];
$orderYear  = $_REQUEST['orderYear'];
$serialNo  = isset($_REQUEST['serialNo'])?$_REQUEST['serialNo']:'';
$year  = $_REQUEST['orderYear'];
$styleNo  = isset($_REQUEST['styleNo'])?$_REQUEST['styleNo']:'';
$graphicNo  = isset($_REQUEST['graphicNo'])?$_REQUEST['graphicNo']:'';
$custPONo  = isset($_REQUEST['custPONo'])?$_REQUEST['custPONo']:'';
$salesOrderNo  = isset($_REQUEST['salesOrderNo'])?$_REQUEST['salesOrderNo']:'';

   $sql = "SELECT DISTINCT 
trn_orderdetails.intSalesOrderId
FROM trn_orderdetails
WHERE
trn_orderdetails.intOrderNo =  '$orderNo' AND
trn_orderdetails.intOrderYear =  '$orderYear'"; 
if($salesOrderNo!=''){
$sql .= " AND
trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'";
}
$sql .= "  order by intSalesOrderId asc";

//echo $sql;
$result = $db->RunQuery($sql);

$salesOrderIdString='';
$i=0;
while($row=mysqli_fetch_array($result))
{
	if($i==0){
	$selectedSalesID=$row['intSalesOrderId'];
	$salesOrderIdString = $row['intSalesOrderId'];
	}
	else{
	$salesOrderIdString .= ", ". $row['intSalesOrderId'];
	}
	$i++;
}
//echo $salesOrderIdString;
global $intStatus;
global $savedStat;
$editMode=loadEditMode($programCode,$intStatus,$savedStat,$intUser);

   $sql = "SELECT sum(intQty) as qty  
FROM trn_orderdetails
WHERE
trn_orderdetails.intOrderNo =  '$orderNo' AND
trn_orderdetails.intOrderYear =  '$orderYear'"; 
$sql .= " AND
trn_orderdetails.intSalesOrderId =  '$selectedSalesID'";
$sql .= "  order by intSalesOrderId asc";
//echo $sql;
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$qty=$row['qty'];

///////////////////04/04/2013/////////////////////////
 $sql = "SELECT
		sum(trn_ordersizeqty.dblQty) as QtySaved
		FROM trn_orderdetails 
		Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
		WHERE
		trn_ordersizeqty.intOrderNo =  '$orderNo' AND
		trn_ordersizeqty.intOrderYear =  '$orderYear' AND
		trn_ordersizeqty.intSalesOrderId =  '$selectedSalesID'";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$balQty=$qty-$row['QtySaved'];
$toleratePercentage	= $objtexttile->getSavedTolerencePercentage($orderNo,$orderYear,$selectedSalesID);
$excessQty=ceil(loadSalesOrderWiseExcessQty($orderNo,$orderYear,$selectedSalesID,$toleratePercentage));
//////////////////////////////////////////////////////////
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Items</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
</head>

<body>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>

    
<form id="frmFabricReceivedNoteSizesPopup" name="frmFabricReceivedNoteSizesPopup" method="post" action="">
<table width="400" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutD" style="width:400px">
		  <div class="trans_text"> Add Sizes</div>
		  <table width="400" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="400" border="0">
      <tr>
        <td><table width="400" border="0" cellpadding="0" cellspacing="0" class="tableBorder">
<tr>
            <td width="10" class="normalfnt">&nbsp;</td>
            <td width="88" class="normalfnt">Year</td>
            <td width="10" class="normalfnt">: </td>
            <td width="114" class="normalfnt"><div id="itemMain"><?php echo $year  ;
 ?></div></td>
            <td width="4" align="right">&nbsp;</td>
            <td width="4" align="right"><div id="itemIdMain" style="display:none"><?php echo $itemId; ?></div></td>
            <td width="79"  class="normalfnt">Style No </td>
            <td width="9"  class="normalfnt">: </td>
            <td width="80" class="normalfnt"><div id="itemMain"><?php echo $styleNo;
 ?></div></td>
            
            </tr>  
            <tr id="rw1">
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Graphic No</td>
            <td class="normalfnt">: </td>
            <td class="normalfnt"><div id="balToInvoice"><?php echo $graphicNo;
 ?></div></td>
            <td class="normalfnt"></td>
            <td align="right">&nbsp;</td>
            <td class="normalfnt">Customer PO</td>
            <td class="normalfnt">: </td>
            <td class="normalfnt"><div id="itemMain"><?php echo $custPONo;

 ?></div></td>
            </tr>   
<tr id="rw1">
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Order No</td>
            <td class="normalfnt">: </td>
            <td class="normalfnt"><?php echo $orderNo  ;
 ?></td>
            <td class="normalfnt"></td>
            <td  align="right">&nbsp;</td>
            <td class="normalfnt">Order Qty</td>
            <td class="normalfnt">:</td>
            <td class="normalfnt"><div id="divOrderQty"><?php //echo $qty  ;
 ?></div></td>
            </tr>
            <tr id="rw1">
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Sales Order No</td>
            <td class="normalfnt">: </td>
            <td class="normalfnt" colspan="5"><select name="cboSalesOrderNoPP" style="width:160px" id="cboSalesOrderNoPP" class="">
			<?php
    $sql = "SELECT DISTINCT trn_orderdetails.intSalesOrderId,
							trn_orderdetails.strSalesOrderNo,
							trn_sampleinfomations_details.intGroundColor,
							mst_colors_ground.strName as bgColor,
							mst_part.strName
							FROM
							trn_orderdetails
							Inner Join trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
							Inner Join trn_sampleinfomations_details ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_details.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_details.intRevNo AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
							left Join mst_part ON trn_orderdetails.intPart = mst_part.intId
							left Join mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
							WHERE
							trn_orderdetails.intOrderNo =  '$orderNo' AND
							trn_orderdetails.intOrderYear =  '$orderYear' ";
						if($salesOrderNo!=''){	
						$sql.= " AND
							trn_orderdetails.strSalesOrderNo =  '$salesOrderNo' ";
						}
							
						$sql .= " ORDER BY  intSalesOrderId asc, trn_orderdetails.strSalesOrderNo ASC, mst_part.strName ASC";
                             
                    $html = "<option value=\"\"></option>";
                    $result = $db->RunQuery($sql);
                    while($row=mysqli_fetch_array($result))
                    {
						/*if($row['strSalesOrderNo']==$salesOrderNo)
                            $html .= "<option value=\"".$row['intSalesOrderId']."\" selected=\"selected\">".$row['strSalesOrderNo']."/".$row['strName']."/".$row['bgColor']."</option>";
						else*/
                            $html .= "<option value=\"".$row['intSalesOrderId']."\">".$row['strSalesOrderNo']."/".$row['strName']."/".$row['bgColor']."</option>";
                    }
					echo $html;
            ?>  
            </select></td>
            <td class="normalfnt"></td>
            </tr>          
</table></td>
      </tr>
      <tr>
        <td align="center"><div style="width:380px;height:300px;overflow:scroll;" >
          <table width="100%" class="grid" id="tblSizesPopup" align="center" >
            <tr class="gridHeader">
              <td width="11%" height="22" >Delele</td>
              <td width="43%" >Size</td>
              <td width="46%">Qty</td>
              <td width="46%">Received Qty</td>
              </tr>
              <?php 
				 	/* $sql = "SELECT
							trn_ordersizeqty.strSize,
							trn_ordersizeqty.dblQty
							FROM trn_ordersizeqty 
							Inner Join trn_orderdetails ON trn_ordersizeqty.intOrderNo = trn_orderdetails.intOrderNo AND trn_ordersizeqty.intOrderYear = trn_orderdetails.intOrderYear AND trn_ordersizeqty.intSalesOrderId = trn_orderdetails.intSalesOrderId
							WHERE
							trn_ordersizeqty.intOrderNo =  '$orderNo' AND
							trn_ordersizeqty.intOrderYear =  '$year' ";
						//if($salesOrderNo!=''){	
						$sql .= " AND
							trn_orderdetails.intSalesOrderId =  '$selectedSalesID'";*/
					//	}
					// echo $sql;	
					$sql='';	
					$result = $db->RunQuery($sql);
					$r=0;
					$rcvQty=0;
					while($row=mysqli_fetch_array($result))
					{
						$r++;
					//	$Qty=getOrderQty($orderNo,$year,$salesOrderNo,$row['strSize']);
						$rcvQty=getRcvQty($orderNo,$year,$selectedSalesID,$row['strSize']);
						if($rcvQty==''){
							$rcvQty=0;
						}
						
						/////////////////////04/04/2013//////////////////////
						$toleratePercentage	= $objtexttile->getSavedTolerencePercentage($orderNo,$year,$selectedSalesID);
						$excessQty=ceil(loadSizeWiseExcessQty($orderNo,$year,$selectedSalesID,$row['strSize'],$toleratePercentage));
						$minQty=($rcvQty-$excessQty);
						$minQty=ceil($minQty);//04-04-2013
						
			  ?>
            <tr class="normalfnt">
              <td align="center" bgcolor="#FFFFFF"><?php if($rcvQty==0){  ?><img src="images/del.png" width="15" height="15" class="delImgPopup" /><?php } ?></td>
              <td align="center" bgcolor="#FFFFFF" id=""><input name="txtSize" type="text" id="txtSize" style="width:60px; text-align:center" value="<?php echo $row['strSize']?>" class="size" <?php if($rcvQty >0){ ?>disabled="disabled" <?php } ?>/></td>
              <td align="center" bgcolor="#FFFFFF" id=""><input  name="txtSizeQty" type="text" id="txtSizeQty" style="width:60px; text-align:right" value="<?php echo $row['dblQty']?>"  class="validate[required,custom[number],max[<?php echo $qty ?>],min[<?php echo $minQty ?>]] sizeQty" /></td>
              <td align="center" bgcolor="#FFFFFF" id=""><?php echo $rcvQty; ?></td>
              </tr>
              <?php 
					}
					if($r==0){
			  ?>
            <tr class="normalfnt">
              <td align="center" bgcolor="#FFFFFF"><?php if($rcvQty==0){  ?><img src="images/del.png" width="15" height="15" class="delImgPopup" /><?php } ?></td>
              <td align="center" bgcolor="#FFFFFF" id=""><input name="txtSize" type="text" id="txtSize" style="width:60px; text-align:center" value="" class="size" /></td>
              <td align="center" bgcolor="#FFFFFF" id=""><input  name="txtSizeQty" type="text" id="txtSizeQty" style="width:60px;  text-align:right" value="0"  class="validate[required,custom[number],max[<?php echo $qty ?>],min[<?php echo $minQty ?>]] sizeQty" /></td>
              <td align="right" bgcolor="#FFFFFF" id=""><?php echo $rcvQty; ?></td>
              </tr>
              <?php
					}
			  ?>
          <tr class="dataRow">
            <td colspan="4" align="left"  bgcolor="#FFFFFF" ><img src="images/Tadd.jpg" name="butInsertRowSizesPopup" width="78" height="24" class="mouseover" id="butInsertRowSizesPopup" /></td>
            </tr>
            </table>
          </div></td>
      </tr>
        <td align="center" class="tableBorder_allRound"><?php if(($editMode==1) && ($editSizeWisePOqtyPermision==true)){ ?><img src="images/Tsave.jpg" id="butSizeSave" name="butSizeSave" width="92" height="24"  alt="save sizes" class="mouseover" /><?php } ?><img src="images/delete.png" width="92" height="24"  alt="add" id="butDelete" name="butDelete" class="mouseover" style="display:none"/><img src="images/Tclose.jpg" width="92" height="24" id="butClose2" name="butClose2" class="mouseover"  /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>

</body>
</html>
<?php
//----------------------------------------------------------------------
function loadSalesOrderWiseExcessQty($orderNo,$orderYear,$salesOrderId,$excessFactor)
{
		global $db;
	         	$sql = "SELECT
			trn_orderdetails.intQty,
			trn_orderdetails.dblOverCutPercentage,
			trn_orderdetails.dblDamagePercentage
			FROM trn_orderdetails 
			WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_orderdetails.intSalesOrderId =  '$salesOrderId'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		$value=$rows['intQty']*($rows['dblOverCutPercentage']+$excessFactor)/100;
		return val($value);
}

//------------------------------------------------------------------------
function loadSizeWiseExcessQty($orderNo,$orderYear,$salesOrderId,$size,$excessFactor)
{
		global $db;
	       	$sql = "SELECT
				trn_ordersizeqty.dblQty,
				trn_orderdetails.dblOverCutPercentage,
				trn_orderdetails.dblDamagePercentage
				FROM trn_orderdetails 
				Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_orderdetails.intSalesOrderId =  '$salesOrderId' AND
				trn_ordersizeqty.strSize =  '$size' ";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		$value=$rows['dblQty']*($rows['dblOverCutPercentage']+$excessFactor)/100;
		return val($value);
}

//--------------------------------------------------------------
function loadSizeDetails($orderNo,$orderYear,$salesOrderIdString) 
{
		global $db;
		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				ware_stocktransactions_fabric.intLocationId =  '$location' AND
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType =  'Received'
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
	
}
	//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}

//-------------------------------------------------------------------------
function getRcvQty($orderNo,$year,$salesOrderNo,$size){
	global $db;
	 $sql1 = "SELECT
Sum(ware_fabricreceiveddetails.dblQty) AS RcvQty
FROM
ware_fabricreceivedheader
Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
WHERE
ware_fabricreceivedheader.intOrderNo =  '$orderNo' AND
ware_fabricreceivedheader.intOrderYear =  '$year' AND
ware_fabricreceivedheader.intStatus =  '1' AND
ware_fabricreceiveddetails.intSalesOrderId =  '$salesOrderNo' AND
ware_fabricreceiveddetails.strSize =  '$size'
GROUP BY
ware_fabricreceivedheader.intOrderNo,
ware_fabricreceivedheader.intOrderYear,
ware_fabricreceiveddetails.intSalesOrderId,
ware_fabricreceiveddetails.strSize";	
	$result1 = $db->RunQuery($sql1);
	$row1=mysqli_fetch_array($result1);
	$RcvQty=$row1['RcvQty'];
	
	
	//added by roshan 2014-Jan-06
	$sql1 = "SELECT
			ifnull(Sum(ware_stocktransactions_fabric.dblQty),0) as dispatchDamages
			FROM `ware_stocktransactions_fabric`
			WHERE
			ware_stocktransactions_fabric.intOrderNo 		= '$orderNo' AND
			ware_stocktransactions_fabric.intOrderYear 		= '$year' AND
			ware_stocktransactions_fabric.intSalesOrderId 	= '$salesOrderNo' AND
			ware_stocktransactions_fabric.strSize 			= '$size' AND
			ware_stocktransactions_fabric.strType IN ('Dispatched_P','Dispatched_F','Dispatched_CUT_RET')
			";
	$result1 	= 	$db->RunQuery($sql1);
	$row1		=	mysqli_fetch_array($result1);
	$RcvQty		=	$RcvQty+$row1['dispatchDamages'];
	
	return $RcvQty;
}

//------------------------------function load loadEditMode---------------------
?>
<?php

//////////////////////////////////////////////
//Create By:H.B.G Korala
//note://if final confirmation raised,insert into stocktransaction table.
/////////////////////////////////////////////

session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$userId = $_SESSION['userId'];
$location = $_SESSION['CompanyID'];
$company = $_SESSION['headCompanyId'];

include "{$backwardseperator}dataAccess/Connector.php";
require_once $_SESSION['ROOT_PATH'] . "class/customerAndOperation/bulk/cls_bulk_get.php";

$obj_bulk_get = new Cls_bulk_get($db);


$programName = 'Fabric Received Note';
$programCode = 'P0045';
$fabRcvApproveLevel = (int) getApproveLevel($programName);

/////////// parameters /////////////////////////////


$serialNo = $_REQUEST['serialNo'];
$year = $_REQUEST['year'];

if ($_REQUEST['status'] == 'approve') {
    $db->OpenConnection();
    $db->RunQuery2('Begin');

//-------------------------------------------------------------------------------------------                  

    $sql = "SELECT ware_fabricreceivedheader.intStatus, 
		ware_fabricreceivedheader.intApproveLevels , 
		ware_fabricreceivedheader.intCompanyId as location,
		mst_locations.intCompanyId as company,
                mst_locations.strName as locationName,
		ware_fabricreceivedheader.intOrderNo,
		ware_fabricreceivedheader.intOrderYear 
		FROM
		ware_fabricreceivedheader
		Inner Join mst_locations ON ware_fabricreceivedheader.intCompanyId = mst_locations.intId
		WHERE (intFabricReceivedNo='$serialNo') AND (`intFabricReceivedYear`='$year')";
    //echo $sql;
    $results = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($results);
    $status = $row['intStatus'];
    $savedLevels = $row['intApproveLevels'];
    $company = $row['company'];
    $location = $row['location'];
    $locationName = $row['locationName']; //fabric receivedlocation
    $orderNo = $row['intOrderNo'];
    $orderYear = $row['intOrderYear'];
//----------------------orderwise detais nciga------------------------------------------------

    $select_sql = "SELECT
	trn_orderdetails.intSalesOrderId,
	trn_orderdetails.strSalesOrderNo,
        mst_locations.strName as location,

	trn_orderdetails.dtPSD,
	trn_orderdetails.dtDeliveryDate,
	trn_orderdetails.dblDamagePercentage,
	trn_orderdetails.dblOverCutPercentage,
	mst_customer.strName,
        mst_customer_locations_header.strName as cus_loc,
	trn_orderheader.strCustomerPoNo,
	mst_brand.strName as brand
        FROM
        trn_orderdetails
        INNER JOIN trn_sampleinfomations_details ON trn_orderdetails.intSampleNo = trn_sampleinfomations_details.intSampleNo
        AND trn_orderdetails.intSampleYear = trn_sampleinfomations_details.intSampleYear
        AND trn_orderdetails.intRevisionNo = trn_sampleinfomations_details.intRevNo
        AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName
        AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
        INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
        AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
        AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo
        INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
        AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
        INNER JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
        INNER JOIN mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
        INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
        INNER JOIN mst_customer_locations_header ON mst_customer_locations_header.intId = trn_orderheader.intCustomerLocation
        WHERE
        trn_orderdetails.intOrderNo = $orderNo
        AND trn_orderdetails.intOrderYear = $orderYear
        GROUP BY
        trn_orderdetails.strSalesOrderNo";
    //echo 
    $results_sql = $db->RunQuery2($select_sql);
    $row_sql = mysqli_fetch_array($results_sql);
    $customer = $row_sql['strName'];
    $cusLocation = $row_sql['cus_loc'];
    $brand = $row_sql['brand'];
    $orderLocation = $row_sql['location'];
    $cusromerPO = $row_sql['strCustomerPoNo'];

    $dtPSD = $row_sql['dtPSD'];
    $dtDeliveryDate = $row_sql['dtDeliveryDate'];
    $dblDamagePercentage = $row_sql['dblDamagePercentage'];
    $dblOverCutPercentage = $row_sql['dblOverCutPercentage'];
    //-------------------token select nciga-----------------
    $token = select_token();

    //-----------------------------------------------------
    $lc_flag = $obj_bulk_get->get_order_lc_flag($orderNo, $orderYear, 'RunQuery2');
    if ($lc_flag == 6)
        $lc_flag = 1;
    else
        $lc_flag = 0;
    $lc_status = $obj_bulk_get->get_lc_status($orderNo, $orderYear, 'RunQuery2');
    //-------------------------------------------

    if ($status == 1) {
        $rollBackFlag = 1;
        $rollBackMsg = "Final confirmation of this Receive No is already raised";
    } else if ($lc_flag == 1 && $lc_status != 1) {
        $rollBackFlag = 1;
        $rollBackMsg = "No Approved LC.So can't Receive Fabrics.";
    }

    if (($status < 0) || ($status > 1)) {

        $sql = "UPDATE `ware_fabricreceivedheader` SET `intStatus`=intStatus-1 WHERE (intFabricReceivedNo='$serialNo') AND (`intFabricReceivedYear`='$year') and intStatus not in(0,1)";
        $result = $db->RunQuery2($sql);
        $sqlf = "SELECT ware_fabricreceivedheader.intStatus, 
		ware_fabricreceivedheader.intApproveLevels , 
		ware_fabricreceivedheader.intCompanyId as location,
		mst_locations.intCompanyId as company 
		FROM
		ware_fabricreceivedheader
		Inner Join mst_locations ON ware_fabricreceivedheader.intCompanyId = mst_locations.intId
		WHERE (intFabricReceivedNo='$serialNo') AND (`intFabricReceivedYear`='$year')";
        $resultsf = $db->RunQuery2($sqlf);
        $row = mysqli_fetch_array($resultsf);
        $status = $row['intStatus'];
        $savedLevels = $row['intApproveLevels'];


        $approveLevel = $savedLevels + 1 - $status;
        $sqlI = "INSERT INTO `ware_fabricreceivedheader_approvedby` (`intFabricReceivedNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate) 
			VALUES ('$serialNo','$year','$approveLevel','$userId',now())";
        $resultI = $db->RunQuery2($sqlI);

        //--------------------------------------------------------------------
        $recordList = array();
        $finalRecord = array();
        if ($status == 1) {//if final confirmation raised,insert to stock
            $sql1 = "SELECT
				ware_fabricreceivedheader.intOrderNo,
				ware_fabricreceivedheader.intOrderYear,
				ware_fabricreceiveddetails.strCutNo,
				ware_fabricreceiveddetails.intSalesOrderId,
                                trn_orderdetails.strSalesOrderNo,
				ware_fabricreceiveddetails.intPart, 
                                mst_part.strName as partName,
				ware_fabricreceiveddetails.strSize,
                                ware_fabricreceiveddetails.strLineNo,
				ware_fabricreceiveddetails.dblQty,
                                ware_fabricreceivedheader.strGraphicNo,
				ware_fabricreceivedheader.strStyleNo,
                                mst_colors_ground.strName as bgcolor
				
				FROM
                                trn_orderdetails
                                INNER JOIN ware_fabricreceivedheader ON ware_fabricreceivedheader.intOrderNo = trn_orderdetails.intOrderNo
                                AND ware_fabricreceivedheader.intOrderYear = trn_orderdetails.intOrderYear
                                INNER JOIN ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo
                                AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
                                AND ware_fabricreceiveddetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
                                INNER JOIN mst_part ON ware_fabricreceiveddetails.intPart = mst_part.intId
                                INNER JOIN mst_colors_ground ON ware_fabricreceiveddetails.intGroundColor = mst_colors_ground.intId
                                WHERE
				ware_fabricreceiveddetails.intFabricReceivedNo =  '$serialNo' AND
				ware_fabricreceiveddetails.intFabricReceivedYear =  '$year'";
            //echo $sql1;
            $result1 = $db->RunQuery2($sql1);
            $toSave = 0;
            $savedRcds = 0;
            while ($row = mysqli_fetch_array($result1)) {

                $value = array();

                $orderNo = $row['intOrderNo'];
                $orderYear = $row['intOrderYear'];
                $cutNo = $row['strCutNo'];
                $part = $row['intPart'];
                $partName = $row['partName'];
                $salesOrderId = $row['intSalesOrderId'];
                $SalesOrderNo = $row['strSalesOrderNo'];
                $size = $row['strSize'];
                $strLineNo = $row['strLineNo'];
                $strStyle = $row['strStyleNo'];
                $strGraphic = $row['strGraphicNo'];
                $bgColor = $row['bgcolor'];
                $qty = round($row['dblQty'], 4);
                $place = 'Stores';
                $type = 'Received';

                //---------------assign to array---------------------    
                $value["transactionId"] = $serialNo . "." . $year . "." . $cutNo . "." . $salesOrderId . "." . $size . ".";
                $value["customerLocation"] = "$cusLocation";
                $value["plantLocation"] = "$locationName"; //fabric receive loc
                $value["lineNo"] = "$strLineNo";
                $value["fabricReceivedNoteId"] = "$serialNo";
                $value["customer"] = "$customer";
                $value["brand"] = "$brand";
                $value["customerPO"] = "$cusromerPO";
                $value["customerSO"] = " $salesOrderId"; //change str so to int so 
                $value["orderNo"] = $orderNo;
                $value["style"] = "$strStyle";
                $value["graphicNo"] = "$strGraphic";
                $value["backGroundColor"] = $bgColor;
                $value["size"] = $size;
                $value["qty"] = $qty;
                $value["PSD"] = "$dtPSD";
                $value["deliveryDate"] = "$dtDeliveryDate";
                $value["year"] = $orderYear;
                $value["part"] = $partName;
                $value["cutNo"] = $cutNo;
                $value["shots"] = 2;
                $value["PDPercentage"] = $dblDamagePercentage;
                $value["overCutPercentage"] = $dblOverCutPercentage;
                $value["type"] = "gatepass";

                $soStatus = loadSalesOrderStatus($orderNo, $orderYear, $salesOrderId);
                if ($soStatus == -10) {
                    $rollBackFlag = 1;
                    $rollBackMsg = "Sales order is already closed";
                }


                if (!checkForSizeInOrder($orderNo, $orderYear, $salesOrderId, $size)) {
                    $rollBackFlag = 1;
                    $rollBackMsg = "size '$size' is not available in customer order.";
                }

                $sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','$location','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','$qty','$type','$userId',now())";
                $resultI = $db->RunQuery2($sql);
                $toSave++;
                if ($resultI) {
                    $savedRcds++;
                }
                $rollBackSql = $sql;

                $recordList = $value;
                $recordArray = array('value' => $recordList);
                array_push($finalRecord, new ArrayObject($recordArray));
            }
//                                 
        }
        //--------------------------------------------------------------------
        if ($status == $savedLevels) {//if first confirmation raised,requisition table shld update
        }
        //---------------------------------------------------------------------------
    }


    if ($rollBackFlag == 1) {
        $db->RunQuery2('Rollback');
        $response['type'] = 'fail';
        $response['msg'] = $rollBackMsg;
        $response['q'] = $rollBackSql;
    } else if (($savedRcds != $toSave) || ($savedRcds == 0)) {
        $rollBackFlag = 1;
        $rollBackMsg = "Data Saving Error";
    } else if (($savedRcds > 0) && ($toSave == $savedRcds)) {

        $db->RunQuery2('Commit');
        $response['type'] = 'pass';
        $response['msg'] = 'Approved successfully.';
        send_detais($finalRecord, $token); //send details nciga
    } else {
        $db->RunQuery2('Rollback');
        $response['type'] = 'fail';
        $response['msg'] = $db->errormsg;
        $response['q'] = $sql;
    }

    $db->CloseConnection();
    //----------------------------------------
    echo json_encode($response);
//----------------------------------------
} else if ($_REQUEST['status'] == 'reject') {
    $db->OpenConnection();
    $db->RunQuery2('Begin');

    //$grnApproveLevel = (int)getApproveLevel($programName);
    $sql = "SELECT ware_fabricreceivedheader.intStatus, 
			ware_fabricreceivedheader.intApproveLevels, 
			ware_fabricreceivedheader.intCompanyId as location,
			mst_locations.intCompanyId as company 
			FROM
			ware_fabricreceivedheader
			Inner Join mst_locations ON ware_fabricreceivedheader.intCompanyId = mst_locations.intId
			WHERE (intFabricReceivedNo='$serialNo') AND (`intFabricReceivedYear`='$year')";
    $results = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($results);
    $status = $row['intStatus'];
    $savedLevels = $row['intApproveLevels'];
    $company = $row['company'];
    $location = $row['location'];

    $sql = "UPDATE `ware_fabricreceivedheader` SET `intStatus`=0 WHERE (intFabricReceivedNo='$serialNo') AND (`intFabricReceivedYear`='$year')";
    $result = $db->RunQuery2($sql);

    $sql = "DELETE FROM `ware_fabricreceivedheader_approvedby` WHERE (`intFabricReceivedNo`='$serialNo') AND (`intYear`='$year')";
    $result2 = $db->RunQuery2($sql);

    $sqlI = "INSERT INTO `ware_fabricreceivedheader_approvedby` (`intFabricReceivedNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate) 
			VALUES ('$serialNo','$year','0','$userId',now())";
    $resultI = $db->RunQuery2($sqlI);

    $db->RunQuery2('Commit');
    $db->CloseConnection();
    //-----------------------------------------------------------------------------
}
else if ($_REQUEST['status'] == 'cancel') {
    $db->OpenConnection();
    $db->RunQuery2('Begin');

    //$grnApproveLevel = (int)getApproveLevel($programName);
    $sql = "SELECT ware_fabricreceivedheader.intStatus, 
			ware_fabricreceivedheader.intApproveLevels, 
			ware_fabricreceivedheader.intCompanyId as location,
			mst_locations.intCompanyId as company 
			FROM
			ware_fabricreceivedheader
			Inner Join mst_locations ON ware_fabricreceivedheader.intCompanyId = mst_locations.intId
			WHERE (intFabricReceivedNo='$serialNo') AND (`intFabricReceivedYear`='$year')";
    $results = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($results);
    $status = $row['intStatus'];
    $savedLevels = $row['intApproveLevels'];
    $company = $row['company'];
    $location = $row['location'];

    $cancelPermission = getCancelPermission(1, $programCode, $userId, $savedLevels, $status, $serialNo, $year);

    if ($cancelPermission == 1) {

        $sql = "UPDATE `ware_fabricreceivedheader` SET `intStatus`=-2 WHERE (intFabricReceivedNo='$serialNo') AND (`intFabricReceivedYear`='$year')";
        $result = $db->RunQuery2($sql);

        $sql = "DELETE FROM `ware_fabricreceivedheader_approvedby` WHERE (`intFabricReceivedNo`='$serialNo') AND (`intYear`='$year')";
        $result2 = $db->RunQuery2($sql);

        $sqlI = "INSERT INTO `ware_fabricreceivedheader_approvedby` (`intFabricReceivedNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate) 
			VALUES ('$serialNo','$year','-2','$userId',now())";
        $resultI = $db->RunQuery2($sqlI);

        //---------------------------------------------------------------------------------
        if ($status == 1) {//if final confirmation raised,delete from stock
            $sql1 = "SELECT
						ware_fabricreceivedheader.intOrderNo,
						ware_fabricreceivedheader.intOrderYear,
						ware_fabricreceiveddetails.strCutNo,
						ware_fabricreceiveddetails.intSalesOrderId,
						ware_fabricreceiveddetails.intPart, 
						ware_fabricreceiveddetails.strSize,
						ware_fabricreceiveddetails.dblQty 
					FROM
						ware_fabricreceiveddetails
						Inner Join ware_fabricreceivedheader ON ware_fabricreceiveddetails.intFabricReceivedNo = ware_fabricreceivedheader.intFabricReceivedNo AND ware_fabricreceiveddetails.intFabricReceivedYear = ware_fabricreceivedheader.intFabricReceivedYear
					WHERE
						ware_fabricreceiveddetails.intFabricReceivedNo =  '$serialNo' AND
						ware_fabricreceiveddetails.intFabricReceivedYear =  '$year'";
            $result1 = $db->RunQuery2($sql1);
            while ($row = mysqli_fetch_array($result1)) {
                $orderNo = $row['intOrderNo'];
                $orderYear = $row['intOrderYear'];
                $cutNo = $row['strCutNo'];
                $part = $row['intPart'];
                $salesOrderId = $row['intSalesOrderId'];
                $size = $row['strSize'];

                $sql = "DELETE FROM `ware_stocktransactions_fabric` WHERE `intDocumentNo` ='$serialNo' AND
																			`strType`='Received'";
                $resultI = $db->RunQuery2($sql);
            }
        }
    }

    $db->RunQuery2('Commit');
    $db->CloseConnection();
}

//--------------------------------------------------------------
function getCancelPermission($cancelMode, $programCode, $intUser, $savedLevels, $intStatus, $serialNo, $year) {
    global $db;
    $cancelPermission = 0;

    $sqlp = "select 
IFNULL(sum(dblQty),0) as dblQty,
IFNULL(sum(totRcvQty),0) as totRcvQty,
IFNULL(sum(totDispQty),0) as totDispQty

 from(SELECT
IFNULL((ware_fabricreceiveddetails.dblQty),0) as dblQty, 
(SELECT
Sum(IFNULL(frd.dblQty,0))
FROM
ware_fabricreceivedheader as frh
INNER JOIN ware_fabricreceiveddetails as frd 
ON frh.intFabricReceivedNo = frd.intFabricReceivedNo AND frh.intFabricReceivedYear = frd.intFabricReceivedYear
WHERE
frh.intStatus = 1 AND
frh.intOrderNo = ware_fabricreceivedheader.intOrderNo AND
frh.intOrderYear = ware_fabricreceivedheader.intOrderYear AND
frd.intSalesOrderId = ware_fabricreceiveddetails.intSalesOrderId AND
frd.strCutNo = ware_fabricreceiveddetails.strCutNo AND
frd.strSize = ware_fabricreceiveddetails.strSize) as totRcvQty,
(SELECT
Sum(IFNULL(fdd.dblSampleQty,0)+IFNULL(fdd.dblGoodQty,0)+IFNULL(fdd.dblEmbroideryQty,0)+IFNULL(fdd.dblPDammageQty,0)+IFNULL(fdd.dblFdammageQty,0)+IFNULL(fdd.dblCutRetQty,0))
FROM
ware_fabricdispatchheader as fdh
INNER JOIN ware_fabricdispatchdetails as fdd 
ON fdh.intBulkDispatchNo = fdd.intBulkDispatchNo AND fdh.intBulkDispatchNoYear = fdd.intBulkDispatchNoYear
WHERE
fdh.intStatus = 1 AND
fdh.intOrderNo = ware_fabricreceivedheader.intOrderNo AND
fdh.intOrderYear = ware_fabricreceivedheader.intOrderYear AND
fdd.intSalesOrderId = ware_fabricreceiveddetails.intSalesOrderId AND
fdd.strCutNo = ware_fabricreceiveddetails.strCutNo AND
fdd.strSize = ware_fabricreceiveddetails.strSize) as totDispQty , 
(SELECT date(max(fap.dtApprovedDate)) as dt
FROM ware_fabricreceivedheader_approvedby as fap
WHERE fap.intFabricReceivedNo = ware_fabricreceivedheader.intFabricReceivedNo 
AND fap.intYear = ware_fabricreceivedheader.intFabricReceivedYear 
AND fap.intApproveLevelNo = '1') as frnAppDate 
 FROM
ware_fabricreceiveddetails
INNER JOIN ware_fabricreceivedheader ON ware_fabricreceiveddetails.intFabricReceivedNo = ware_fabricreceivedheader.intFabricReceivedNo AND ware_fabricreceiveddetails.intFabricReceivedYear = ware_fabricreceivedheader.intFabricReceivedYear
WHERE
ware_fabricreceiveddetails.intFabricReceivedNo = '$serialNo' AND
ware_fabricreceiveddetails.intFabricReceivedYear = '$year' ) as tb";

    $resultp = $db->RunQuery2($sqlp);
    $row = mysqli_fetch_array($resultp);
    $fabricDispFlag = 1;
    if ((($row['totRcvQty'] - $row['totDispQty']) >= $row['dblQty']) && ($row['frnAppDate'] = date('Y-m-d')))
        $fabricDispFlag = 0;

    $sqlp = "SELECT
		menupermision.intCancel  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";

    $resultp = $db->RunQuery2($sqlp);
    $rowp = mysqli_fetch_array($resultp);
    if (($rowp['intCancel'] == 1) && ($cancelMode == 1) && ($intStatus = 1) && ($fabricDispFlag == 0)) {
        $cancelPermission = 1;
    }
    return $cancelPermission;
}

function checkForSizeInOrder($orderNo, $orderYear, $salesOrderId, $size) {

    global $db;
    $sql = "select * from trn_ordersizeqty 
				  where 
				  intOrderNo ='$orderNo'
				  and intOrderYear ='$orderYear' 
				  and intSalesOrderId ='$salesOrderId' 
				  and strSize ='$size' 
				  ";
    $result = $db->RunQuery2($sql);
    return $result;
}

function loadSalesOrderStatus($orderNo, $orderYear, $salesOrderId) {

    global $db;

    $sql = "SELECT
				trn_orderdetails.`STATUS`
				FROM `trn_orderdetails`
				WHERE
				trn_orderdetails.intOrderNo = '$orderNo' AND
				trn_orderdetails.intOrderYear = '$orderYear' AND
				trn_orderdetails.intSalesOrderId = '$salesOrderId'
				 ";

    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);

    return $row['STATUS'];
}

function send_detais($record, $token) {

    if ($token == "") { //if token null generate token
        generate_token($record);
    }

    $configs = include('../../../../../config/ncigaConfig.php');
    $url = $configs['URL'];
    $data = array('token' => $token, 'records' => $record);
    $data_json = json_encode($data);
    // print_r($data_json);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($ch);
    $nciga_response = json_decode($response); //get nciga response
    $message_code = $nciga_response->messageCode;

    if ($message_code == 'NCN0003') { //token expired
        generate_token($record);
        update_tbl($record, $message_code);
    } else if ($message_code == 'NCN0001') {//success
        update_tbl($record, $message_code);
    } else if ($message_code == 'NCN0002') {//invalid token
        generate_token($record);
        update_tbl($record, $message_code);
    } else if ($message_code == 'NCN0007') {//Failed to submit data
        update_tbl($record, $message_code);
        send_detais($record, $token);
    } else if ($message_code == 'NCN0005') {//Invalid URL
        update_tbl($record, $message_code);
        send_detais($record, $token);
    } else if ($message_code == 'NCN0006') {//Internal server Error , Please contact administrator
        update_tbl($record, $message_code);
        send_detais($record, $token);
    } else if ($message_code == 'NCN0008') {//Mandatory value missing
        update_tbl($record, $message_code);
        //send_detais($record, $token);
    }

    if (!$response) {

        die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    }

    curl_close($ch);
}

function generate_token($record) {


    $configs = include('../../../../../config/ncigaConfig.php');
    $url = $configs['URL'];
    $AUTH_USERNAME = $configs['AUTH_USERNAME'];
    $AUTH_PASSWORD = $configs['AUTH_PASSWORD'];
    $TYPE = $configs['TYPE'];
    $data = array('username' => $AUTH_USERNAME, 'password' => $AUTH_PASSWORD, 'type' => $TYPE);
    $data_json = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($ch);
    curl_close($ch);

    insert_tbl($response); //insert token 
    $token_new = select_token(); //get newly generated token
    send_detais($record, $token_new); //send details again

    if (!$response) {
        die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    }

    curl_close($ch);
}


function insert_tbl($data) {

    global $db;
    $date = date('Y-m-d H:i:s', time());

    $jsonArray = json_decode($data, true);
    $userId = $jsonArray['userId'];
    $generatedTime = $jsonArray['generatedTime'];
    $expireDate = $jsonArray['expireDate'];
    $token = $jsonArray['token'];

    $sql_insert = "
                            INSERT INTO
                            tbl_token_details
                            (USER_ID,
                            GENERATED_TIME,
                            EXPIRED_DATE,
                            TOKEN,
                            UPDATED_TIME
                            )
                            VALUES
                            ('$userId',
                             $generatedTime,
                             $expireDate,   
                             '$token',
                             '$date'  
                            );";

    $db->RunQuery2($sql_insert);
}

//--------------------------------------------------------
function select_token() {

    global $db;
    $sql_token = "SELECT UPDATED_TIME, date(UPDATED_TIME),TOKEN
                          FROM tbl_token_details AS a
                          WHERE (UPDATED_TIME)= (
                          SELECT MAX((UPDATED_TIME))
                          FROM tbl_token_details AS b )";

    $result = $db->RunQuery2($sql_token);
    $row = mysqli_fetch_array($result);
    $token = $row['TOKEN'];
    return $token;
}

function update_tbl($record, $message_code) {

    global $db;

    foreach ($record as $key => $val) {

        foreach ($val as $data) {

            $fabricReceivedNoteId = $data['fabricReceivedNoteId'];
            $year = $data['year'];
            $cutNo = $data['cutNo'];
            $customerSO = $data['customerSO'];
            $size = $data['size'];

            $sql_retryCount = "SELECT ware_fabricreceiveddetails.retryCount,
                                      ware_fabricreceiveddetails.maxRetryCount 
                                      FROM
                                      ware_fabricreceiveddetails
                                      WHERE
                                        
                                    ware_fabricreceiveddetails.intFabricReceivedNo = $fabricReceivedNoteId
                                    AND ware_fabricreceiveddetails.intFabricReceivedYear = $year
                                    AND ware_fabricreceiveddetails.strCutNo = '$cutNo' 
                                    AND ware_fabricreceiveddetails.intSalesOrderId = $customerSO 
                                    AND ware_fabricreceiveddetails.strSize = '$size' ";
             //echo $sql_retryCount;
            $result = $db->RunQuery2($sql_retryCount);
            $row = mysqli_fetch_array($result);
            $retryCount = $row['retryCount'];
            $maxRetryCount = $row['maxRetryCount'];



            $sql_update = "  UPDATE
                                    ware_fabricreceiveddetails";
            if ($message_code == 'NCN0001') {
                $sql_update .= " SET
                                    ware_fabricreceiveddetails.ncingaDeliveryStatus = 1";
            } else {
                $sql_update .= " SET
                                    
                                    ware_fabricreceiveddetails.retryCount = $retryCount+1";
            }
            $sql_update .= " WHERE
                                    ware_fabricreceiveddetails.intFabricReceivedNo = $fabricReceivedNoteId
                                    AND ware_fabricreceiveddetails.intFabricReceivedYear = $year
                                    AND ware_fabricreceiveddetails.strCutNo = '$cutNo'
                                    AND ware_fabricreceiveddetails.intSalesOrderId = $customerSO
                                    AND ware_fabricreceiveddetails.strSize = '$size'
                                    AND ware_fabricreceiveddetails.retryCount   < $maxRetryCount";

           //echo $sql_update;
            $db->RunQuery2($sql_update);
        }
    }
}

?>
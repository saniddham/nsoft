var basepath	= 'presentation/customerAndOperation/bulk/fabricReceivedNote/listing/';

$(document).ready(function() {
	$('#frmFabRecvReport').validationEngine();
	$('#frmFabRecvReport #imgApprove').live('click',function(){
			if(validateQuantities()==0){
	var val = $.prompt('Are you sure you want to approve this Fabric Receive Note ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					var url = basepath+"rptFabricReceivedNote-db-set.php"+window.location.search+'&status=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmFabRecvReport #imgApprove').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									//window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmFabRecvReport #imgApprove').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
						
						
					}
				}});
			}
	});
	
	$('#frmFabRecvReport #imgReject').live('click',function(){
	var val = $.prompt('Are you sure you want to reject this Fabric Receive Note ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
									if(validateRejecton()==0){ 
										var url = basepath+"rptFabricReceivedNote-db-set.php"+window.location.search+'&status=reject';
										var obj = $.ajax({url:url,async:false});
										window.location.href = window.location.href;
										window.opener.location.reload();//reload listing page
									}
									}
								}});
	});
	$('#frmFabRecvReport #imgCancel').live('click',function(){
	var val = $.prompt('Are you sure you want to Cancel this Fabric Receive Note ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
									if(validateCancel()==0){ 
										var url = basepath+"rptFabricReceivedNote-db-set.php"+window.location.search+'&status=cancel';
										var obj = $.ajax({url:url,async:false});
										window.location.href = window.location.href;
										window.opener.location.reload();//reload listing page
									}
									}
								}});
	});
});

function validateQuantities(){
		var ret=0;	
	    var serialNo = document.getElementById('divSerialNo').innerHTML;
		var url 		= basepath+"rptFabricReceivedNote-db-get.php?requestType=getValidation";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"serialNo="+serialNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",3000);
					  ret= 1;
					}
				}
			});
			return ret;
}

function validateRejecton(){
		var ret=0;	
	    var serialNo = document.getElementById('divSerialNo').innerHTML;
		var url 		= basepath+"rptFabricReceivedNote-db-get.php?requestType=validateRejecton";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"serialNo="+serialNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgReject').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx1()",3000);
					  ret= 1;
					}
				}
			});
			return ret;
}

function validateCancel(){
		var ret=0;	
	    var serialNo = document.getElementById('divSerialNo').innerHTML;
		var url 		= basepath+"rptFabricReceivedNote-db-get.php?requestType=validateCancel";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"serialNo="+serialNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgCancel').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					var t=setTimeout("alertx2()",3000);
					  ret= 1;
					}
				}
			});
			return ret;
}
function alertx()
{
	$('#frmFabRecvReport #imgApprove').validationEngine('hide')	;
}
function alertx1()
{
	$('#frmFabRecvReport #imgReject').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmFabRecvReport #imgCancel').validationEngine('hide')	;
}
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "libraries/jqgrid2/inc/jqgrid_dist.php";

$location 		= $sessions->getLocationId();
$company 		= $sessions->getCompanyId();
$intUser  		= $sessions->getUserId();

$approveLevel 	= (int)getMaxApproveLevel();
$programCode	= 'P0045';
$date			= date('Y-m-d');

//BEGIN - ADD DEFAULT WHERE STRING WHEN FORM LOADING {
$arr =  json_decode($_REQUEST['filters'],true);

$arr = $arr['rules'];

$where_string = '';
$where_array = array(
					'Status'=>'tb1.intStatus',
					'Fabric_Receive_No'=>"tb1.intFabricReceivedNo",
					'Fabric_Receive_Year'=>"tb1.intFabricReceivedYear",
					'strStyleNo'=>"ifnull(tb1.strStyleNo,(select  group_concat(strStyleNo separator ', ') from  (select distinct strStyleNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
							tb.intOrderYear =  tb1.intOrderYear

							))",
					'strGraphicNo'=>"
							ifnull(tb1.strGraphicNo,(select  group_concat(strGraphicNo separator ', ') from  (select distinct strGraphicNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
							tb.intOrderYear =  tb1.intOrderYear

							))",
					'Order_No'=>"concat(tb1.intOrderNo,'/',tb1.intOrderYear)",
					'strSalesOrderNo'=>"(select  group_concat(strSalesOrderNo separator ', ') from  (select distinct strSalesOrderNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
							tb.intOrderYear =  tb1.intOrderYear)",
					'strAODNo'=>"tb1.strAODNo",					
					'Date'=>'tb1.dtmCreateDate',
					'User'=>"sys_users.strUserName"
					);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Revised'=>'-1','Cancelled'=>'-2','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}

if(!count($arr)>0)					 
		$where_string .= "AND DATE(tb1.dtmCreateDate) = '".date('Y-m-d')."'";
//END }

////commented 2014-10-09 .bcs can cancel in any day.
$sql = "select * from(SELECT DISTINCT 
							if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected',if(tb1.intStatus=-2,'Canceled','Pending'))) as Status,
							tb1.intFabricReceivedNo as `Fabric_Receive_No`,
							tb1.intFabricReceivedYear as `Fabric_Receive_Year`,
							
							ifnull(tb1.strStyleNo,(select  group_concat(strStyleNo separator ', ') from  (select distinct strStyleNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
							tb.intOrderYear =  tb1.intOrderYear

							)) as strStyleNo,
							
							/*(select  group_concat(strStyleNo separator ', ') from  (select distinct strStyleNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
							tb.intOrderYear =  tb1.intOrderYear) as strStyleNo,*/
							
							ifnull(tb1.strGraphicNo,(select  group_concat(strGraphicNo separator ', ') from  (select distinct strGraphicNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
							tb.intOrderYear =  tb1.intOrderYear

							)) as strGraphicNo,
							
							/*(select  group_concat(strGraphicNo separator ', ') from  (select distinct strGraphicNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
							tb.intOrderYear =  tb1.intOrderYear

							)) as strGraphicNo,*/
							
							
							(SELECT
							group_concat(distinct strSalesOrderNo separator ', ')
							FROM ware_fabricreceiveddetails inner join trn_orderdetails on trn_orderdetails.intSalesOrderId=ware_fabricreceiveddetails.intSalesOrderId
							WHERE
							ware_fabricreceiveddetails.intFabricReceivedNo =  tb1.intFabricReceivedNo AND
							trn_orderdetails.intOrderNo = tb1.intOrderNo AND 
							trn_orderdetails.intOrderYear = tb1.intOrderYear AND
							ware_fabricreceiveddetails.intFabricReceivedYear =  tb1.intFabricReceivedYear) as strSalesOrderNo,
							
							tb1.strAODNo,
							tb1.dtmCreateDate as `Date`,
							sys_users.strUserName as User,
							tb1.intApproveLevels,
							tb1.intStatus,
							tb1.intOrderNo,
							tb1.intOrderYear, 
							concat(tb1.intOrderNo,'/',tb1.intOrderYear) as Order_No , 
							 
							IFNULL((SELECT
							Sum(trn_orderdetails.intQty)
							FROM trn_orderdetails
							WHERE
							trn_orderdetails.intOrderNo =  tb1.intOrderNo AND
							trn_orderdetails.intOrderYear =  tb1.intOrderYear AND trn_orderdetails.SO_TYPE > -1),0) as order_qty, 
							
							IFNULL((SELECT
							Sum(ware_fabricreceiveddetails.dblQty)
							FROM ware_fabricreceiveddetails
							WHERE
							ware_fabricreceiveddetails.intFabricReceivedNo =  tb1.intFabricReceivedNo AND
							ware_fabricreceiveddetails.intFabricReceivedYear =  tb1.intFabricReceivedYear),0) as Received_qty, 
							
							IFNULL((SELECT
							Sum(round(ware_fabricdispatchdetails.dblPdammageQty,2))
							FROM
							ware_fabricdispatchdetails
							Inner Join ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
							WHERE
							ware_fabricdispatchheader.intOrderNo =  tb1.intOrderNo AND
							ware_fabricdispatchheader.intOrderYear =  tb1.intOrderYear),0) as PD_qty, 
							
							IFNULL((SELECT
							Sum(round(ware_fabricdispatchdetails.dblCutRetQty,2))
							FROM
							ware_fabricdispatchdetails
							Inner Join ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
							WHERE
							ware_fabricdispatchheader.intOrderNo =  tb1.intOrderNo AND
							ware_fabricdispatchheader.intOrderYear =  tb1.intOrderYear),0) as cutRet_qty, 
							
							IFNULL((SELECT
							Sum(round(ware_fabricdispatchdetails.dblFdammageQty,2))
							FROM
							ware_fabricdispatchdetails
							Inner Join ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
							WHERE
							ware_fabricdispatchheader.intOrderNo =  tb1.intOrderNo AND
							ware_fabricdispatchheader.intOrderYear =  tb1.intOrderYear),0) as FD_qty, 
							
							IFNULL((
                                      SELECT
								concat(sys_users.strUserName,'(',max(ware_fabricreceivedheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_fabricreceivedheader_approvedby
								Inner Join sys_users ON ware_fabricreceivedheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_fabricreceivedheader_approvedby.intFabricReceivedNo  = tb1.intFabricReceivedNo AND
								ware_fabricreceivedheader_approvedby.intYear =  tb1.intFabricReceivedYear AND
								ware_fabricreceivedheader_approvedby.intApproveLevelNo =  '1'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.intStatus>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_fabricreceivedheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_fabricreceivedheader_approvedby
								Inner Join sys_users ON ware_fabricreceivedheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_fabricreceivedheader_approvedby.intFabricReceivedNo  = tb1.intFabricReceivedNo AND
								ware_fabricreceivedheader_approvedby.intYear =  tb1.intFabricReceivedYear AND
								ware_fabricreceivedheader_approvedby.intApproveLevelNo =  '$i'
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.intStatus>1) AND (tb1.intStatus<=tb1.intApproveLevels) AND ((SELECT
								max(ware_fabricreceivedheader_approvedby.dtApprovedDate)
								FROM
								ware_fabricreceivedheader_approvedby
								Inner Join sys_users ON ware_fabricreceivedheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_fabricreceivedheader_approvedby.intFabricReceivedNo  = tb1.intFabricReceivedNo AND
								ware_fabricreceivedheader_approvedby.intYear =  tb1.intFabricReceivedYear AND
								ware_fabricreceivedheader_approvedby.intApproveLevelNo =  ($i-1))<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*fase part*/
								 if($i>tb1.intApproveLevels,'-----',''))
								
								/*end IFNULL false part*/
								) as `".$approval."`, "; 
									
								}
								
						$sql .= " 'View' as `View` ,  
						 		IFNULL((SELECT concat(sys_users.strUserName,'(',max(ware_fabricreceivedheader_approvedby.dtApprovedDate),')' ) 
								FROM ware_fabricreceivedheader_approvedby Inner Join sys_users ON ware_fabricreceivedheader_approvedby.intApproveUser = sys_users.intUserId 
								WHERE ware_fabricreceivedheader_approvedby.intFabricReceivedNo = tb1.intFabricReceivedNo 
								AND ware_fabricreceivedheader_approvedby.intYear = tb1.intFabricReceivedYear AND ware_fabricreceivedheader_approvedby.intApproveLevelNo = '-2'), 
								IF(((SELECT menupermision.intCancel FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId WHERE menus.strCode = 'P0045' 
								AND menupermision.intUserId = '$intUser')=1 AND (tb1.intStatus=1) 
								/* AND 
								(( SELECT date(max(fap.dtApprovedDate)) as dt
								FROM ware_fabricreceivedheader_approvedby as fap
								WHERE fap.intFabricReceivedNo = tb1.intFabricReceivedNo 
								AND fap.intYear = tb1.intFabricReceivedYear 
								AND fap.intApproveLevelNo = '1')='$date')*/ ),'Cancel','')) as `Cancel`   
						FROM
							ware_fabricreceivedheader as tb1
							Inner Join trn_orderdetails ON tb1.intOrderNo = trn_orderdetails.intOrderNo AND tb1.intOrderYear = trn_orderdetails.intOrderYear
							Inner Join sys_users ON tb1.intCteatedBy = sys_users.intUserId 
							Inner Join mst_locations ON tb1.intCompanyId = mst_locations.intId 
							WHERE
								tb1.intCompanyId =  '$location'
								$where_string
							)  as t where 1=1
						";
					    	//echo $sql;
$col = array();

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected;Canceled:Canceled" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist

$cols[] = $col;	$col=NULL;
//Fabric Receive No
$col["title"] 	= "Fab Rcv No"; // caption of column
$col["name"] 	= "Fabric_Receive_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "4";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= "?q=45&serialNo={Fabric_Receive_No}&year={Fabric_Receive_Year}";	 
$col["linkoptions"] = "target='fabricReceivedNote.php'"; // extra params with <a> tag

$reportLink  		= "?q=931&serialNo={Fabric_Receive_No}&year={Fabric_Receive_Year}&style={strStyleNo}&Graphic={strGraphicNo}";
$reportLinkApprove  = "?q=931&serialNo={Fabric_Receive_No}&year={Fabric_Receive_Year}&style={strStyleNo}&Graphic={strGraphicNo}&approveMode=1";
$reportLinkCancel  	= "?q=931&serialNo={Fabric_Receive_No}&year={Fabric_Receive_Year}&style={strStyleNo}&Graphic={strGraphicNo}&cancelMode=1";

$cols[] = $col;	$col=NULL;


//Fabric Receive Year
$col["title"] = "Fab Rcv Year"; // caption of column
$col["name"] = "Fabric_Receive_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Style NO
$col["title"] = "Style No"; // caption of column
$col["name"] = "strStyleNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Graphic No
$col["title"] = "Graphic No"; // caption of column
$col["name"] = "strGraphicNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Order No"; // caption of column
$col["name"] = "Order_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Sales Order No"; // caption of column
$col["name"] = "strSalesOrderNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Order Qty
$col["title"] = "Order Qty"; // caption of column
$col["name"] = "order_qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Received Qty
$col["title"] = "FD Qty"; // caption of column
$col["name"] = "FD_qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

$col["title"] = "PD Qty"; // caption of column
$col["name"] = "PD_qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

$col["title"] = "Cut Returned Qty"; // caption of column
$col["name"] = "cutRet_qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;


//Received Qty
$col["title"] = "FRN Qty"; // caption of column
$col["name"] = "Received_qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "AOD No"; // caption of column
$col["name"] = "strAODNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//DATE
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//DATE
$col["title"] = "User"; // caption of column
$col["name"] = "User"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//FIRST APPROVAL
$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='rptFabricReceivedNote.php'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='rptFabricReceivedNote.php'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}


//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='rptFabricReceivedNote.php'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

//VIEW
$col["title"] = "Cancel"; // caption of column
$col["name"] = "Cancel"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkCancel;
$col['linkName']	= 'Cancel';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Fabric Received Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'Fabric_Receive_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>

<head>
	<?php 
		include "include/listing.html";
	?>
</head>
<body>
<form id="frmlisting" name="frmlisting" method="post" action="">

        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
</form>
</body>

<?php
function getMaxApproveLevel()
{
	global $db;
	
	$appLevel=0;
	$sqlp = "SELECT
			Max(ware_fabricreceivedheader.intApproveLevels) AS appLevel
			FROM ware_fabricreceivedheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
?>


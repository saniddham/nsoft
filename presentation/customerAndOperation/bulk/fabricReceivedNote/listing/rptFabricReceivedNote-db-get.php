<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
	session_start();
	$backwardseperator = "../../../../../";
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include_once "{$backwardseperator}dataAccess/Connector.php";
	
	
	require_once $_SESSION['ROOT_PATH']."class/cls_permisions.php";
	require_once $_SESSION['ROOT_PATH']."class/customerAndOperation/cls_textile_stores.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
	require_once $_SESSION['ROOT_PATH']."class/customerAndOperation/bulk/cls_bulk_get.php";
	
	//---------check Permission to save recive qty more than PO qty.------------	
	$objpermisionget	= new cls_permisions($db);
	$objtexttile 		= new cls_texttile();
	$exceedPOPermision 	= $objpermisionget->boolSPermision(5);
	$objcomfunc 		= new cls_commonFunctions_get($db);
	//$excessFactor 	= $objtexttile->getFRNexcessFactor($company);
	$obj_bulk_get 		= new Cls_bulk_get($db);
	//------------------------------------------------------------------------
	$programName='Fabric Received Note';
	$programCode='P0045';
	$fabRcvApproveLevel = (int)getApproveLevel($programName);
	

 if($requestType=='getValidation')
	{
		$errorFlg=0;
		$serialNo  = $_REQUEST['serialNo'];
		$serialNoArray=explode("/",$serialNo);
		
		$orderStatus=getOrderStatus($serialNoArray[0],$serialNoArray[1]);//check wether already confirmed
	
		$sql = "SELECT
		ware_fabricreceivedheader.intStatus, 
		ware_fabricreceivedheader.intApproveLevels ,
		ware_fabricreceivedheader.intOrderNo,
		ware_fabricreceivedheader.intOrderYear  
		FROM 
		ware_fabricreceivedheader 
		WHERE
		ware_fabricreceivedheader.intFabricReceivedNo =  '$serialNoArray[0]' AND
		ware_fabricreceivedheader.intFabricReceivedYear =  '$serialNoArray[1]'";

		$result 	= $db->RunQuery($sql);
		$errorFlg	= 0;
		$row		= mysqli_fetch_array($result);
		$orderNo	= $row['intOrderNo'];
		$orderYear	= $row['intOrderYear'];
	
	
		$confirmatonMode=loadConfirmatonMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);
		//--------------------------
		$locationType 	= $objcomfunc->getLocationType($location);
		//-------------------------------------------
		$lc_flag		= $obj_bulk_get->get_order_lc_flag($orderNo,$orderYear,'RunQuery');
		$lc_status		= $obj_bulk_get->get_lc_status($orderNo,$orderYear,'RunQuery');
		if($lc_flag	== 6) 
			$lc_flag = 1;
		else
			$lc_flag = 0;
		//--------------------------
		
		$plantId			= getPlantId($location);
		
		if($plantId!='')
		{
			$plantStatus	= getPlantStatus($plantId); 
		}
		
		if($orderStatus!=1){
			 $errorFlg	=1;
			 $msg		="This Order is revised or pending.";
		 }
		else if($row['intStatus']==1){// 
			$errorFlg 	= 1;
			$msg 		="Final confirmation of this Received No is already raised"; 
		}
		else if($row['intStatus']==0){// 
			$errorFlg 	= 1;
			$msg 		="This Received No is rejected"; 
		}
		else if($confirmatonMode==0){// 
			$errorFlg 	= 1;
			$msg 		="No Permission to Approve"; 
		}
		else if($locationType < 1){//
			$errorFlg 	= 1;
			$msg 		="This is not a production location.So can't receive fabrics.";
		}
		 else if($lc_flag==1 && $lc_status !=1){
			 $errorFlg	=1;
			 $msg		="No Approved LC.So can't Receive Fabrics.";
		 }
		 else if($plantId=='')
		 {
			 $errorFlg	= 1;
			 $msg		= "This location is not allocate to a plant.";
		 }
		 else if($plantStatus==0)
		 {
			 $errorFlg	= 1;
			 $msg		= "This location is not allocate to an active plant.";
		 }
		
		//-------------------------------------------
		$lc_flag		= $obj_bulk_get->get_order_lc_flag($orderNo,$orderYear,'RunQuery');
		$lc_status		= $obj_bulk_get->get_lc_status($orderNo,$orderYear,'RunQuery');
		//-------------------------------------------
		
		$sql = "SELECT 
			ware_fabricreceivedheader.intStatus,
			ware_fabricreceivedheader.intApproveLevels,
			ware_fabricreceivedheader.intOrderNo,
			ware_fabricreceivedheader.intOrderYear,
			ware_fabricreceiveddetails.intSalesOrderId,
			ware_fabricreceiveddetails.intSalesOrderId,
			ware_fabricreceiveddetails.intPart, 
			ware_fabricreceiveddetails.strSize, 
			ware_fabricreceiveddetails.dblQty  
			FROM
			ware_fabricreceiveddetails
			Inner Join ware_fabricreceivedheader ON ware_fabricreceiveddetails.intFabricReceivedNo = ware_fabricreceivedheader.intFabricReceivedNo AND ware_fabricreceiveddetails.intFabricReceivedYear = ware_fabricreceivedheader.intFabricReceivedYear
			WHERE
			ware_fabricreceiveddetails.intFabricReceivedNo =  '$serialNoArray[0]' AND
			ware_fabricreceiveddetails.intFabricReceivedYear =  '$serialNoArray[1]'";
				
		$result = $db->RunQuery($sql);
		$records=0;
		if($errorFlg!=1){
		$msg ="Maximum Qtys for items"; 
		while($row=mysqli_fetch_array($result))
		{ 
				
				$records++;
				$status=$row['intStatus'];
				$approveLevels=$row['intApproveLevels'];
				$orderNo=$row['intOrderNo'];
				$orderYear=$row['intOrderYear'];
				$salesOrderId=$row['intSalesOrderId'];
				$partId=$row['intPart'];
				$size=$row['strSize'];
				$qty=$row['dblQty'];
			
				//--------2015-01-06---------------
				$sql_t = "SELECT 
						sum(ware_fabricreceiveddetails.dblQty) as sumOrderQty  
					FROM
					ware_fabricreceiveddetails
					Inner Join ware_fabricreceivedheader ON ware_fabricreceiveddetails.intFabricReceivedNo = ware_fabricreceivedheader.intFabricReceivedNo AND ware_fabricreceiveddetails.intFabricReceivedYear = ware_fabricreceivedheader.intFabricReceivedYear
					WHERE
					ware_fabricreceiveddetails.intFabricReceivedNo =  '$serialNoArray[0]' AND
					ware_fabricreceiveddetails.intFabricReceivedYear =  '$serialNoArray[1]' AND
					ware_fabricreceiveddetails.intSalesOrderId =  '$salesOrderId' AND
					ware_fabricreceiveddetails.strSize =  '$size'";
						
				$result_t = $db->RunQuery($sql_t);
				$row_t=mysqli_fetch_array($result_t);
				$size_this_sum	=$row_t['sumOrderQty'];
				//---------------------------------
			 
			 	$fdQty=loadDamageReturnedQty($location,$orderNo,$orderYear,$salesOrderId,$size);
				$receivedQty=loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade);
				$nonstckConfQty=loadNonstckConfQty($location,$orderNo,$orderYear,$salesOrderId,$size);
				$orderQty=loadOrderQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade);
				$toleratePercentage	= $objtexttile->getSavedTolerencePercentage($orderNo,$orderYear,$salesOrderId);
				$excessQty=loadSizeWiseExcessQty($orderNo,$orderYear,$salesOrderId,$size,$toleratePercentage);
				$maxRcvQty=ceil($orderQty-$receivedQty-$nonstckConfQty+$excessQty+$fdQty);
				//echo $orderQty."-".$receivedQty."-".$nonstckConfQty."+".$excessQty."+".$fdQty."/";
				
				$recvQtyArr[$orderNo][$orderYear][$salesOrderId][$size] 	=$size_this_sum;
				$orderQtyArr[$orderNo][$orderYear][$salesOrderId][$size]	=ceil($orderQty+$excessQty+$fdQty-$receivedQty-$nonstckConfQty);
				
				if(($status<=$approveLevels) && ($status>1)){
				  $maxRcvQty=ceil($orderQty-$receivedQty-$nonstckConfQty+$excessQty+$size_this_sum+$fdQty);
				  $orderQtyArr[$orderNo][$orderYear][$salesOrderId][$size]	=ceil($orderQty+$excessQty+$fdQty-$receivedQty-$nonstckConfQty+$size_this_sum);
				}
				
				
				if($maxRcvQty<=0){
					$maxRcvQty=0;
				}

				//------check maximum FR Qty--------------------
				if(!checkForSizeInOrder($orderNo,$orderYear,$salesOrderId,$size)){
					$errorFlg=1;
					$msg ="size '$size' is not available in customer order.";
				}
				else if($status==0){
					$errorFlg=1;
					$msg ="This Received No is Rejected.";
				}
				else if($status==1){
					$errorFlg=1;
					$msg ="Final confirmation of this Received No is already raised";
				}
				
				else if(($size_this_sum>$maxRcvQty) && (!$exceedPOPermision)){
					$errorFlg=1;
					$msg .="<br> ".$orderNo."-".$orderYear."-".$salesOrderId."-".$size." =".$maxRcvQty;
				}
		}//end of while
		
			if($records==0){
				$errorFlg=1;
				$msg="No details to confirm";
			}
		}
		
		
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}
//--------------------------------------------
else if($requestType=='validateRejecton')
	{
		$errorFlg=0;
		$serialNo  = $_REQUEST['serialNo'];
		$serialNoArray=explode("/",$serialNo);
		
		$sql = "SELECT
		ware_fabricreceivedheader.intStatus, 
		ware_fabricreceivedheader.intApproveLevels 
		FROM ware_fabricreceivedheader
		WHERE
		ware_fabricreceivedheader.intFabricReceivedNo =  '$serialNoArray[0]' AND
		ware_fabricreceivedheader.intFabricReceivedYear =  '$serialNoArray[1]'";

		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$msg =""; 
		$row=mysqli_fetch_array($result);
		
		$rejectionMode=loadRejectionMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);
		
		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this Received No is already raised.So cant reject"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This Received No is already Rejected"; 
		}
		else if($rejectionMode==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="No Permission to reject this Received No"; 
		}
		else{//confirmation has been raised
			$errorFlg = 0;
			
		}

	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);

	}
else if($requestType=='validateCancel')
	{
		$errorFlg=0;
		$serialNo  = $_REQUEST['serialNo'];
		$serialNoArray=explode("/",$serialNo);
		
		$sql = "SELECT 
		(SELECT date(max(fap.dtApprovedDate)) as dt
		FROM ware_fabricreceivedheader_approvedby as fap
		WHERE fap.intFabricReceivedNo = ware_fabricreceivedheader.intFabricReceivedNo 
		AND fap.intYear = ware_fabricreceivedheader.intFabricReceivedYear 
		AND fap.intApproveLevelNo = '1') appDate, 
		ware_fabricreceivedheader.intStatus, 
		ware_fabricreceivedheader.intApproveLevels 
		FROM ware_fabricreceivedheader
		WHERE
		ware_fabricreceivedheader.intFabricReceivedNo =  '$serialNoArray[0]' AND
		ware_fabricreceivedheader.intFabricReceivedYear =  '$serialNoArray[1]'";

		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$msg =""; 
		$row=mysqli_fetch_array($result);
		
 	    $cancelPermission=getCancelPermission(1,$programCode,$userId,$row['intApproveLevels'],$row['intStatus'],$serialNoArray[0],$serialNoArray[1]);
		$cancelflag=getCancelFlag($serialNoArray[0],$serialNoArray[1]);
		if($row['intStatus']==-2){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This Received No is already Cancelled"; 
		}
/*		else if($row['appDate'] != date('Y-m-d')){//commented 2014-10-09 .bcs can cancel in any day.
			$errorFlg = 1;
			$msg ="This is not confirmed in today"; 
		}
*/		else if($cancelflag==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="No Receive balance to Cancel"; 
		}
		else if($cancelPermission==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="No Permission to cancel this Received No"; 
		}
		else{//confirmation has been raised
			$errorFlg = 0;
			
		}

	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);

	}
	

	//-----------------------------------------------------------
function loadDamageReturnedQty($location,$orderNo,$orderYear,$salesOrderId,$size)
{
		global $db;
		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty*-1) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				/*ware_stocktransactions_fabric.intLocationId =  '$location' AND*/
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType in(  'Dispatched_F' ,'Dispatched_P', 'Dispatched_CUT_RET')
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}

	//-----------------------------------------------------------
function loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade)
{
		global $db;
		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				/*ware_stocktransactions_fabric.intLocationId =  '$location' AND*/
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType =  'Received' 
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//--------------------------------------------------------------
	function loadNonstckConfQty($location,$orderNo,$orderYear,$salesOrderId,$size)
{
		global $db;
	  	$sql = "SELECT
				Sum(ware_fabricreceiveddetails.dblQty) as dblQty 
				FROM
				ware_fabricreceivedheader
				Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
				WHERE
				ware_fabricreceivedheader.intStatus >  1 AND
				ware_fabricreceivedheader.intStatus <=  ware_fabricreceivedheader.intApproveLevels AND
				ware_fabricreceivedheader.intOrderNo =  '$orderNo' AND
				ware_fabricreceivedheader.intOrderYear =  '$orderYear' AND
				ware_fabricreceiveddetails.intSalesOrderId =  '$salesOrderId' AND
				ware_fabricreceiveddetails.strSize =  '$size'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//--------------------------------------------------------------
function loadOrderQty($location,$orderNo,$orderYear,$salesOrderNo,$size,$grade)
{
		global $db;
		  $sql = "SELECT
				trn_ordersizeqty.dblQty
				FROM trn_ordersizeqty
				WHERE
				trn_ordersizeqty.intOrderNo =  '$orderNo' AND
				trn_ordersizeqty.intOrderYear =  '$orderYear' AND
				trn_ordersizeqty.intSalesOrderId =  '$salesOrderNo' AND
				trn_ordersizeqty.strSize =  '$size'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//--------------------------------------------------------------
	function loadSizeWiseExcessQty($orderNo,$orderYear,$salesOrderId,$size,$excessFactor)
{
		global $db;
	       	$sql = "SELECT
				trn_ordersizeqty.dblQty,
				trn_orderdetails.dblOverCutPercentage,
				trn_orderdetails.dblDamagePercentage,
				DATEDIFF(date(trn_orderheader.dtmCreateDate),date('2017-05-18')) AS DiffDate
				FROM trn_orderdetails 
				INNER JOIN trn_orderheader on 
				trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo and trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
				Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_orderdetails.intSalesOrderId =  '$salesOrderId' AND
				trn_ordersizeqty.strSize =  '$size' ";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		//$value=$rows['dblQty']*($rows['dblOverCutPercentage']+$excessFactor)/100;
		if($rows['DiffDate']< 0)
		$value=$rows['dblQty']*($rows['dblOverCutPercentage']+$excessFactor)/100;
		else
		$value=$rows['dblQty']*($rows['dblOverCutPercentage']+$rows['dblDamagePercentage'])/100;
		
		return val($value);
}
//-----------------------------------------------------------
	function getOrderStatus($serialNo,$year)
	{
		global $db;
	  	$sql = "SELECT
trn_orderheader.intStatus
FROM
ware_fabricreceivedheader
Inner Join trn_orderheader ON ware_fabricreceivedheader.intOrderNo = trn_orderheader.intOrderNo AND ware_fabricreceivedheader.intOrderYear = trn_orderheader.intOrderYear
WHERE
ware_fabricreceivedheader.intFabricReceivedNo =  '$serialNo' AND
ware_fabricreceivedheader.intFabricReceivedYear =  '$year'";
			$results = $db->RunQuery($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
 			
		return $status;
	}
//-----------------------------------------------------------
	function getLocationValidation($type,$location,$company,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
		$sql = "SELECT
					mst_locations.intCompanyId
					FROM
					trn_orderheader
					Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
					WHERE
					trn_orderheader.intOrderNo =  '$serialNo' AND
					trn_orderheader.intOrderYear =  '$year'";
			$results = $db->RunQuery($sql);
			$row = mysqli_fetch_array($results);
			if($company == $row['intCompanyId'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//--------------------------------------------------------
function getCancelPermission($cancelMode,$programCode,$intUser,$savedLevels,$intStatus,$serialNo,$year)
{
	global $db;
	$cancelPermission=0;

    $sqlp = "select 
IFNULL(sum(dblQty),0) as dblQty,
IFNULL(sum(totRcvQty),0) as totRcvQty,
IFNULL(sum(totDispQty),0) as totDispQty

 from(SELECT
IFNULL((ware_fabricreceiveddetails.dblQty),0) as dblQty, 
(SELECT
Sum(IFNULL(frd.dblQty,0))
FROM
ware_fabricreceivedheader as frh
INNER JOIN ware_fabricreceiveddetails as frd 
ON frh.intFabricReceivedNo = frd.intFabricReceivedNo AND frh.intFabricReceivedYear = frd.intFabricReceivedYear
WHERE
frh.intStatus = 1 AND
frh.intOrderNo = ware_fabricreceivedheader.intOrderNo AND
frh.intOrderYear = ware_fabricreceivedheader.intOrderYear AND
frd.intSalesOrderId = ware_fabricreceiveddetails.intSalesOrderId AND
frd.strCutNo = ware_fabricreceiveddetails.strCutNo AND
frd.strSize = ware_fabricreceiveddetails.strSize) as totRcvQty,
(SELECT
Sum(IFNULL(fdd.dblSampleQty,0)+IFNULL(fdd.dblGoodQty,0)+IFNULL(fdd.dblEmbroideryQty,0)+IFNULL(fdd.dblPDammageQty,0)+IFNULL(fdd.dblFdammageQty,0)+IFNULL(fdd.dblCutRetQty,0))
FROM
ware_fabricdispatchheader as fdh
INNER JOIN ware_fabricdispatchdetails as fdd 
ON fdh.intBulkDispatchNo = fdd.intBulkDispatchNo AND fdh.intBulkDispatchNoYear = fdd.intBulkDispatchNoYear
WHERE
fdh.intStatus = 1 AND
fdh.intOrderNo = ware_fabricreceivedheader.intOrderNo AND
fdh.intOrderYear = ware_fabricreceivedheader.intOrderYear AND
fdd.intSalesOrderId = ware_fabricreceiveddetails.intSalesOrderId AND
fdd.strCutNo = ware_fabricreceiveddetails.strCutNo AND
fdd.strSize = ware_fabricreceiveddetails.strSize) as totDispQty 
 FROM
ware_fabricreceiveddetails
INNER JOIN ware_fabricreceivedheader ON ware_fabricreceiveddetails.intFabricReceivedNo = ware_fabricreceivedheader.intFabricReceivedNo AND ware_fabricreceiveddetails.intFabricReceivedYear = ware_fabricreceivedheader.intFabricReceivedYear
WHERE
ware_fabricreceiveddetails.intFabricReceivedNo = '$serialNo' AND
ware_fabricreceiveddetails.intFabricReceivedYear = '$year' ) as tb";	
	
	 $resultp = $db->RunQuery($sqlp);
	 $row=mysqli_fetch_array($resultp);
	$fabricDispFlag=1;
	if(($row['totRcvQty']-$row['totDispQty']) >= $row['dblQty'])
	$fabricDispFlag=0;
	
	$sqlp = "SELECT
		menupermision.intCancel  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
	
	 $resultp = $db->RunQuery($sqlp);
	 $rowp=mysqli_fetch_array($resultp);
 	 if(($rowp['intCancel']==1)&&($cancelMode==1) && ($intStatus =1) && ($fabricDispFlag ==0)){
		 $cancelPermission=1;
	 }
	return $cancelPermission; 
}
 
 function getCancelFlag($serialNo,$year)
{
	global $db;
	$cancelPermission=0;

    $sqlp = "select 
IFNULL(sum(dblQty),0) as dblQty,
IFNULL(sum(totRcvQty),0) as totRcvQty,
IFNULL(sum(totDispQty),0) as totDispQty

 from(SELECT
IFNULL((ware_fabricreceiveddetails.dblQty),0) as dblQty, 
(SELECT
Sum(IFNULL(frd.dblQty,0))
FROM
ware_fabricreceivedheader as frh
INNER JOIN ware_fabricreceiveddetails as frd 
ON frh.intFabricReceivedNo = frd.intFabricReceivedNo AND frh.intFabricReceivedYear = frd.intFabricReceivedYear
WHERE
frh.intStatus = 1 AND
frh.intOrderNo = ware_fabricreceivedheader.intOrderNo AND
frh.intOrderYear = ware_fabricreceivedheader.intOrderYear AND
frd.intSalesOrderId = ware_fabricreceiveddetails.intSalesOrderId AND
frd.strCutNo = ware_fabricreceiveddetails.strCutNo AND
frd.strSize = ware_fabricreceiveddetails.strSize) as totRcvQty,
(SELECT
Sum(IFNULL(fdd.dblSampleQty,0)+IFNULL(fdd.dblGoodQty,0)+IFNULL(fdd.dblEmbroideryQty,0)+IFNULL(fdd.dblPDammageQty,0)+IFNULL(fdd.dblFdammageQty,0)+IFNULL(fdd.dblCutRetQty,0))
FROM
ware_fabricdispatchheader as fdh
INNER JOIN ware_fabricdispatchdetails as fdd 
ON fdh.intBulkDispatchNo = fdd.intBulkDispatchNo AND fdh.intBulkDispatchNoYear = fdd.intBulkDispatchNoYear
WHERE
fdh.intStatus = 1 AND
fdh.intOrderNo = ware_fabricreceivedheader.intOrderNo AND
fdh.intOrderYear = ware_fabricreceivedheader.intOrderYear AND
fdd.intSalesOrderId = ware_fabricreceiveddetails.intSalesOrderId AND
fdd.strCutNo = ware_fabricreceiveddetails.strCutNo AND
fdd.strSize = ware_fabricreceiveddetails.strSize) as totDispQty , 
(SELECT date(max(fap.dtApprovedDate)) as dt
FROM ware_fabricreceivedheader_approvedby as fap
WHERE fap.intFabricReceivedNo = ware_fabricreceivedheader.intFabricReceivedNo 
AND fap.intYear = ware_fabricreceivedheader.intFabricReceivedYear 
AND fap.intApproveLevelNo = '1') as frnAppDate 
 FROM
ware_fabricreceiveddetails
INNER JOIN ware_fabricreceivedheader ON ware_fabricreceiveddetails.intFabricReceivedNo = ware_fabricreceivedheader.intFabricReceivedNo AND ware_fabricreceiveddetails.intFabricReceivedYear = ware_fabricreceivedheader.intFabricReceivedYear
WHERE
ware_fabricreceiveddetails.intFabricReceivedNo = '$serialNo' AND
ware_fabricreceiveddetails.intFabricReceivedYear = '$year' ) as tb";	
	
	 $resultp = $db->RunQuery($sqlp);
	 $row=mysqli_fetch_array($resultp);
	$cancelFlag=0;
	if((($row['totRcvQty']-$row['totDispQty']) >= $row['dblQty']) && ($row['frnAppDate'] = date('Y-m-d')))
	$cancelFlag=1;
	return $cancelFlag;
}

function checkForSizeInOrder($orderNo,$orderYear,$salesOrderId,$size){

	global $db;
	$sql		="select * from trn_ordersizeqty 
				  where 
				  intOrderNo ='$orderNo'
				  and intOrderYear ='$orderYear' 
				  and intSalesOrderId ='$salesOrderId' 
				  and strSize ='$size' 
				  ";
	$result 	= $db->RunQuery($sql);
	return $result;
	
}
function getPlantId($locationId)
{
	global $db;
	
	$sql 	= "SELECT intPlant FROM mst_locations WHERE intId = '$locationId' ";
	
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	
	return $row['intPlant'];
}

function getPlantStatus($plantId)
{
	global $db;
	
	$sql 	= "SELECT intStatus FROM mst_plant WHERE intPlantId = '$plantId' ";
	
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	
	return $row['intStatus'];
}
?>

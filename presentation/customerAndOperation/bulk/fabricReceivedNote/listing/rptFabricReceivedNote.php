<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
ini_set('display_errors',0);

$intUser  			= $sessions->getUserId();
$serialNo 			= $_REQUEST['serialNo'];
$year 				= $_REQUEST['year'];
$style_no 			= $_REQUEST['style'];
$graphic_no 		= $_REQUEST['Graphic'];
$approveMode 		= (!isset($_REQUEST["approveMode"])?'':$_REQUEST["approveMode"]);
$cancelMode 		= (!isset($_REQUEST["cancelMode"])?'':$_REQUEST["cancelMode"]);

$programCode		= 'P0045';

   $sql = "SELECT 
ware_fabricreceivedheader.intFabricReceivedNo,
ware_fabricreceivedheader.intFabricReceivedYear, 
trn_orderdetails.strStyleNo,
trn_orderdetails.strGraphicNo, 
trn_orderheader.strCustomerPoNo,
ware_fabricreceivedheader.intOrderNo,
ware_fabricreceivedheader.intOrderYear,
ware_fabricreceivedheader.strAODNo,
ware_fabricreceivedheader.strRemarks,
ware_fabricreceivedheader.intStatus,
ware_fabricreceivedheader.intApproveLevels,
ware_fabricreceivedheader.dtmdate,
ware_fabricreceivedheader.dtmCreateDate,
sys_users.strUserName,
mst_customer.strName as customer ,
ware_fabricreceivedheader.intCompanyId 
FROM
ware_fabricreceivedheader  
Inner Join trn_orderheader ON ware_fabricreceivedheader.intOrderNo = trn_orderheader.intOrderNo AND ware_fabricreceivedheader.intOrderYear = trn_orderheader.intOrderYear
Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
left Join mst_customer ON trn_orderheader.intCustomer = mst_customer.intId 
left Join sys_users ON ware_fabricreceivedheader.intCteatedBy = sys_users.intUserId
WHERE
ware_fabricreceivedheader.intFabricReceivedNo =  '$serialNo' AND
ware_fabricreceivedheader.intFabricReceivedYear =  '$year'
";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$serialNo = $row['intFabricReceivedNo'];
					$SerialYear = $row['intFabricReceivedYear'];
					$style = $row['strStyleNo'];
					$graphicNo=$row['strGraphicNo'];
					$custPO = $row['strCustomerPoNo'];
					$orderNo = $row['intOrderNo'];
					$orderYear = $row['intOrderYear'];
					$AODNo = $row['strAODNo'];
					$AODDate = $row['dtmdate'];
					$customer = $row['customer'];
					$remarks = $row['strRemarks'];
					$date = $row['dtmCreateDate'];
					$intStatus = $row['intStatus'];
					$savedLevels = $row['intApproveLevels'];
					$user = $row['strUserName'];
					$locationId = $row['intCompanyId'];//this locationId use in report header(reportHeader.php)--------------------
				 }
				 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Fabric Received Report</title>
<?php include 'include/css.html'?>
<script type="text/javascript" src="presentation/customerAndOperation/bulk/fabricReceivedNote/listing/rptFabricReceivedNote-js.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:271px;
	top:172px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="images/pending.png"  /></div>
<?php
}
?>
<form id="frmFabRecvReport" name="frmFabRecvReport" method="post" action="rptFabricReceivedNote.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>FABRIC RECEIVED REPORT</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="10" align="center" bgcolor="#FFDFCB">
    <?php
 	$approvalPermision=getApprovalPermision($approveMode,$programCode,$intUser,$savedLevels,$intStatus);
	$rejectPermision=getRejectPermision($approveMode,$programCode,$intUser,$savedLevels,$intStatus);
	$cancelPermission=getCancelPermission($cancelMode,$programCode,$intUser,$savedLevels,$intStatus,$serialNo,$year);
	$cancelflag=getCancelFlag($serialNo,$year);
	
	?>
    <?php 
	if($approvalPermision==1) { ?>
   <a id="imgApprove"  class="button white medium" style="" name="imgApprove" <?php /*?>onclick="submit(1)"<?php */?>> APPROVE </a>
    <?php
 	}
	if($rejectPermision==1) { ?>
  <a id="imgReject"  class="button white medium" style="" name="imgReject" <?php /*?>onclick="submit(1)"<?php */?>> REJECT </a>
    <?php
 	}
	if($cancelflag==0 && $cancelMode==1 &&  $intStatus !=-2){
		echo "<font color='#F00000'>No Received balance to cancel</font>";
	}
	if($cancelPermission==1) { ?>
  <a id="imgCancel"  class="button white medium" style="" name="imgCancel" <?php /*?>onclick="submit(1)"<?php */?>> CANCEL </a>
    <?php
 	}
	?></td>
  </tr>
  <tr>
  <?php
 	if($intStatus==1)
	{
	?>
   <td colspan="10" class="APPROVE" >CONFIRMED</td>
   <?PHP
	}
	else if($intStatus==0)
	{
   ?>
   <td colspan="9" class="APPROVE" style="color:#F00">REJECTED</td>
   <?php
	}
	else if($intStatus==-2)
	{
   ?>
   <td colspan="9" class="APPROVE" style="color:#F00">CANCELED</td>
   <?php
	}
	else
	{
   ?>
   <td width="10%" colspan="9" class="APPROVE">PENDING</td>
   <?php
	}
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="12%"><span class="normalfnt"><strong>Fabric Received No</strong></span></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="18%"><span class="normalfnt"><?php echo $serialNo  ?>/<?php echo $SerialYear ?></span></td>
    <td width="8%" class="normalfnt"><strong>Style No</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="13%"><span class="normalfnt"><?php echo $style_no   ?></span></td>
    <td width="9%" class="normalfnt"><div id="divSerialNo" style="display:none"><?php echo $serialNo ?>/<?php echo $year ?></div>
      <strong>Graphic No</strong></td>
    <td width="1%"><strong>:</strong></td>
  <td width="16%"><span class="normalfnt"><?php echo $graphic_no ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Customer PO</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $custPO   ?></span></td>
    <td><span class="normalfnt"><strong>Order No</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $orderNo."/".$orderYear; ?></span></td>
    <td class="normalfnt"><strong>Customer</strong></td>
    <td><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $customer; ?></span></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>AOD Date</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $AODDate; ?></span></td>
    <td class="normalfnt"><strong>By</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $user; ?></span></td>
    <td class="normalfnt"><strong>AOD No</strong></td>
    <td><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $AODNo; ?></span></td>
  </tr>
  
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt" valign="top"><strong>Remarks</strong></td>
    <td align="center" valign="top"><strong>:</strong></td>
    <td colspan="4"><textarea cols="50" rows="6" class="textarea" style="width::300px" disabled="disabled"><?php echo $remarks  ?></textarea></td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="">
              <th width="10%" >Cut No</th>
              <th width="12%" >Sales Order No</th>
              <th width="11%" >Part</th>
              <th width="12%" >Background Color</th>
              <th width="16%" >Line No</th>
              <th width="18%" >Size</th>
              <th width="9%" >Qty</th>
              </tr>
              <?php 
	  	    $sql1 = "SELECT
trn_orderdetails.strSalesOrderNo,
ware_fabricreceiveddetails.strCutNo,
mst_part.strName as part,
mst_colors_ground.strName as bgcolor,
ware_fabricreceiveddetails.strLineNo,
ware_fabricreceiveddetails.strSize,
ware_fabricreceiveddetails.dblQty
FROM
trn_orderdetails
Inner Join ware_fabricreceivedheader ON ware_fabricreceivedheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricreceivedheader.intOrderYear = trn_orderdetails.intOrderYear
Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear AND ware_fabricreceiveddetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
left Join mst_part ON ware_fabricreceiveddetails.intPart = mst_part.intId
left Join mst_colors_ground ON ware_fabricreceiveddetails.intGroundColor = mst_colors_ground.intId
WHERE
ware_fabricreceivedheader.intFabricReceivedNo =  '$serialNo' AND
ware_fabricreceivedheader.intFabricReceivedYear =  '$SerialYear'  
ORDER BY 
ware_fabricreceiveddetails.strCutNo ASC, 
trn_orderdetails.strSalesOrderNo ASC, 
mst_part.strName ASC, 
mst_colors_ground.strName ASC, 
ware_fabricreceiveddetails.strLineNo ASC,
ware_fabricreceiveddetails.strSize ASC 
";
			$result1 = $db->RunQuery($sql1);
			$totQty=0;
			$totAmmount=0;
			while($row=mysqli_fetch_array($result1))
			{
				$cutNo=$row['strCutNo'];
				$salesOrderNo=$row['strSalesOrderNo'];
				$part=$row['part'];
				$bgColor=$row['bgcolor'];
				$lineNo=$row['strLineNo'];
				$size=$row['strSize'];
				$qty=$row['dblQty'];
	  ?>
            <tr class="normalfnt"   bgcolor="#FFFFFF">
             <td align="center" class="normalfntMid" id="<?php echo $cutNo; ?>" ><?php echo $cutNo; ?></td>
			<td align="center" class="normalfntMid" id="<?php echo $salesOrderNo; ?>" ><?php echo $salesOrderNo; ?></td>
			<td align="center" class="normalfntMid" id="<?php echo $part; ?>"><?php echo $part; ?></td>
			<td align="center" class="normalfntMid" id="<?php echo $bgColor; ?>" ><?php echo $bgColor; ?></td>
			<td align="center" class="normalfntMid" id="<?php echo $lineNo; ?>" ><?php echo $lineNo; ?></td>
			<td align="center" class="normalfntMid" id="<?php echo $size; ?>" ><?php echo $size; ?></td>
			<td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $qty; ?></td>
            </tr>              
      <?php 
			$totQty+=$qty;
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" ><?php echo $totQty ?></td>
              </tr>
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<?php 
 
 	if($intStatus>0)
	{

				for($i=1; $i<=$savedLevels; $i++)
				{
					   $sqlc = "SELECT
							ware_fabricreceivedheader_approvedby.intApproveUser,
							ware_fabricreceivedheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName, 
							ware_fabricreceivedheader_approvedby.intApproveLevelNo
							FROM
							ware_fabricreceivedheader_approvedby
							Inner Join sys_users ON ware_fabricreceivedheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_fabricreceivedheader_approvedby.intFabricReceivedNo =  '$serialNo' AND
							ware_fabricreceivedheader_approvedby.intYear =  '$SerialYear'  AND
							ware_fabricreceivedheader_approvedby.intApproveLevelNo =  '$i'  order by intApproveLevelNo asc
";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
						if($i==1)
						$desc="1st ";
						else if($i==2)
						$desc="2nd ";
						else if($i==3)
						$desc="3rd ";
						else
						$desc=$i."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
				?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
            </tr>
<?php
			}
	} 
	else if($intStatus==-2){
					 	  $sqlc = "SELECT
							ware_fabricreceivedheader_approvedby.intApproveUser,
							ware_fabricreceivedheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_fabricreceivedheader_approvedby.intApproveLevelNo
							FROM
							ware_fabricreceivedheader_approvedby
							Inner Join sys_users ON ware_fabricreceivedheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_fabricreceivedheader_approvedby.intFabricReceivedNo =  '$serialNo' AND
							ware_fabricreceivedheader_approvedby.intYear =  '$SerialYear'  AND
							ware_fabricreceivedheader_approvedby.intApproveLevelNo =  '-2'";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Canceled By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
	}
	else{
					 $sqlc = "SELECT
							ware_fabricreceivedheader_approvedby.intApproveUser,
							ware_fabricreceivedheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_fabricreceivedheader_approvedby.intApproveLevelNo
							FROM
							ware_fabricreceivedheader_approvedby
							Inner Join sys_users ON ware_fabricreceivedheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_fabricreceivedheader_approvedby.intFabricReceivedNo =  '$serialNo' AND
							ware_fabricreceivedheader_approvedby.intYear =  '$SerialYear'  AND
							ware_fabricreceivedheader_approvedby.intApproveLevelNo =  '0'";
					 $resultc = $db->RunQuery($sqlc);
					 $rowr=mysqli_fetch_array($resultc);
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Rejected By - </strong></span><span class="normalfnt"><?php echo $rowr['UserName']."(".$rowr['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
	}
?>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>

<?php
function getApprovalPermision($approveMode,$programCode,$intUser,$savedLevels,$intStatus)
{
	global $db;
	$approvalPermision=0;

	$nextAppLevel=$savedLevels+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$nextAppLevel."Approval  as appLevel
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
	
	 $resultp = $db->RunQuery($sqlp);
	 $rowp=mysqli_fetch_array($resultp);
 	 if(($rowp['appLevel']==1)&&($approveMode==1)&&($intStatus>1)){
		 $approvalPermision=1;
	 }

	return $approvalPermision; 
}
function getRejectPermision($approveMode,$programCode,$intUser,$savedLevels,$intStatus)
{
	global $db;
	$rejectPermision=0;

 	$sqlp = "SELECT
		menupermision.intReject  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
	
	 $resultp = $db->RunQuery($sqlp);
	 $rowp=mysqli_fetch_array($resultp);
 	 if(($rowp['intReject']==1)&&($approveMode==1) && ($intStatus > 1)){
		 $rejectPermision=1;
	 }
	return $rejectPermision; 
}
function getCancelPermission($cancelMode,$programCode,$intUser,$savedLevels,$intStatus,$serialNo,$year)
{
	global $db;
	$cancelPermission=0;

    $sqlp = "select 
IFNULL(sum(dblQty),0) as dblQty,
IFNULL(sum(totRcvQty),0) as totRcvQty,
IFNULL(sum(totDispQty),0) as totDispQty

 from(SELECT
IFNULL((ware_fabricreceiveddetails.dblQty),0) as dblQty, 
(SELECT
Sum(IFNULL(frd.dblQty,0))
FROM
ware_fabricreceivedheader as frh
INNER JOIN ware_fabricreceiveddetails as frd 
ON frh.intFabricReceivedNo = frd.intFabricReceivedNo AND frh.intFabricReceivedYear = frd.intFabricReceivedYear
WHERE
frh.intStatus = 1 AND
frh.intOrderNo = ware_fabricreceivedheader.intOrderNo AND
frh.intOrderYear = ware_fabricreceivedheader.intOrderYear AND
frd.intSalesOrderId = ware_fabricreceiveddetails.intSalesOrderId AND
frd.strCutNo = ware_fabricreceiveddetails.strCutNo AND
frd.strSize = ware_fabricreceiveddetails.strSize) as totRcvQty,
(SELECT
Sum(IFNULL(fdd.dblSampleQty,0)+IFNULL(fdd.dblGoodQty,0)+IFNULL(fdd.dblEmbroideryQty,0)+IFNULL(fdd.dblPDammageQty,0)+IFNULL(fdd.dblFdammageQty,0)+IFNULL(fdd.dblCutRetQty,0))
FROM
ware_fabricdispatchheader as fdh
INNER JOIN ware_fabricdispatchdetails as fdd 
ON fdh.intBulkDispatchNo = fdd.intBulkDispatchNo AND fdh.intBulkDispatchNoYear = fdd.intBulkDispatchNoYear
WHERE
fdh.intStatus = 1 AND
fdh.intOrderNo = ware_fabricreceivedheader.intOrderNo AND
fdh.intOrderYear = ware_fabricreceivedheader.intOrderYear AND
fdd.intSalesOrderId = ware_fabricreceiveddetails.intSalesOrderId AND
fdd.strCutNo = ware_fabricreceiveddetails.strCutNo AND
fdd.strSize = ware_fabricreceiveddetails.strSize) as totDispQty 
 FROM
ware_fabricreceiveddetails
INNER JOIN ware_fabricreceivedheader ON ware_fabricreceiveddetails.intFabricReceivedNo = ware_fabricreceivedheader.intFabricReceivedNo AND ware_fabricreceiveddetails.intFabricReceivedYear = ware_fabricreceivedheader.intFabricReceivedYear
WHERE
ware_fabricreceiveddetails.intFabricReceivedNo = '$serialNo' AND
ware_fabricreceiveddetails.intFabricReceivedYear = '$year' ) as tb";	
	
	 $resultp = $db->RunQuery($sqlp);
	 $row=mysqli_fetch_array($resultp);
	$fabricDispFlag=1;
	if(($row['totRcvQty']-$row['totDispQty']) >= $row['dblQty'])
	$fabricDispFlag=0;
	
	$sqlp = "SELECT
		menupermision.intCancel  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
	
	 $resultp = $db->RunQuery($sqlp);
	 $rowp=mysqli_fetch_array($resultp);
 	 if(($rowp['intCancel']==1)&&($cancelMode==1) && ($intStatus =1) && ($fabricDispFlag ==0)){
		 $cancelPermission=1;
	 }
	return $cancelPermission; 
}
 function getCancelFlag($serialNo,$year)
{
	global $db;
	$cancelPermission=0;

    $sqlp = "select 
IFNULL(sum(dblQty),0) as dblQty,
IFNULL(sum(totRcvQty),0) as totRcvQty,
IFNULL(sum(totDispQty),0) as totDispQty

 from(SELECT
IFNULL((ware_fabricreceiveddetails.dblQty),0) as dblQty, 
(SELECT
Sum(IFNULL(frd.dblQty,0))
FROM
ware_fabricreceivedheader as frh
INNER JOIN ware_fabricreceiveddetails as frd 
ON frh.intFabricReceivedNo = frd.intFabricReceivedNo AND frh.intFabricReceivedYear = frd.intFabricReceivedYear
WHERE
frh.intStatus = 1 AND
frh.intOrderNo = ware_fabricreceivedheader.intOrderNo AND
frh.intOrderYear = ware_fabricreceivedheader.intOrderYear AND
frd.intSalesOrderId = ware_fabricreceiveddetails.intSalesOrderId AND
frd.strCutNo = ware_fabricreceiveddetails.strCutNo AND
frd.strSize = ware_fabricreceiveddetails.strSize) as totRcvQty,
(SELECT
Sum(IFNULL(fdd.dblSampleQty,0)+IFNULL(fdd.dblGoodQty,0)+IFNULL(fdd.dblEmbroideryQty,0)+IFNULL(fdd.dblPDammageQty,0)+IFNULL(fdd.dblFdammageQty,0)+IFNULL(fdd.dblCutRetQty,0))
FROM
ware_fabricdispatchheader as fdh
INNER JOIN ware_fabricdispatchdetails as fdd 
ON fdh.intBulkDispatchNo = fdd.intBulkDispatchNo AND fdh.intBulkDispatchNoYear = fdd.intBulkDispatchNoYear
WHERE
fdh.intStatus = 1 AND
fdh.intOrderNo = ware_fabricreceivedheader.intOrderNo AND
fdh.intOrderYear = ware_fabricreceivedheader.intOrderYear AND
fdd.intSalesOrderId = ware_fabricreceiveddetails.intSalesOrderId AND
fdd.strCutNo = ware_fabricreceiveddetails.strCutNo AND
fdd.strSize = ware_fabricreceiveddetails.strSize) as totDispQty 
 FROM
ware_fabricreceiveddetails
INNER JOIN ware_fabricreceivedheader ON ware_fabricreceiveddetails.intFabricReceivedNo = ware_fabricreceivedheader.intFabricReceivedNo AND ware_fabricreceiveddetails.intFabricReceivedYear = ware_fabricreceivedheader.intFabricReceivedYear
WHERE
ware_fabricreceiveddetails.intFabricReceivedNo = '$serialNo' AND
ware_fabricreceiveddetails.intFabricReceivedYear = '$year' ) as tb";	
	
	 $resultp = $db->RunQuery($sqlp);
	 $row=mysqli_fetch_array($resultp);
	$cancelFlag=0;
	if(($row['totRcvQty']-$row['totDispQty']) >= $row['dblQty'])
	$cancelFlag=1;
	return $cancelFlag;
}
?>

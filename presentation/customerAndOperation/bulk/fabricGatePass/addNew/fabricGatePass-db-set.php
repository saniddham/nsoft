<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo 	 = $_REQUEST['serialNo'];
	$year 		 = $_REQUEST['Year'];
/*	$style  = $_REQUEST['style'];
	$graphicNo 	 = $_REQUEST['graphicNo'];
	$cusromerPO 	 = $_REQUEST['cusromerPO'];
*/	$orderNo 	 = $_REQUEST['orderNo'];
	$orderYear 	 = $_REQUEST['orderYear'];
	$gpDate 	 = $_REQUEST['gpDate'];
	$gpTo 	 = $_REQUEST['gpTo'];
	$remarks 	 = $_REQUEST['remarks'];

	$arr 		= json_decode($_REQUEST['arr'], true);
	
	$programName='Fabric Gate Pass';
	$programCode='P0047';

	$ApproveLevels = (int)getApproveLevel($programName);
	$maxStatus = $ApproveLevels+1;

//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		$savableFlag=1;
	    $rollBackFlag=0;
		
		if($serialNo==''){
			$serialNo 	= getNextSerialNo();
			$year = date('Y');
			$editMode=0;
			$savedStatus=$maxStatus;
			$savedLevels=$ApproveLevels;
		}
		else{
			$editMode=1;
			$savableFlag=getSaveStatus($serialNo,$year);//check wether already confirmed
			$sql = "SELECT
			ware_fabricgatepassheader.intStatus, 
			ware_fabricgatepassheader.intApproveLevels 
			FROM ware_fabricgatepassheader 
			WHERE
			ware_fabricgatepassheader.intFabricGatePassNo =  '$serialNo' AND
			ware_fabricgatepassheader.intFabricGatePassYear =  '$year'";

			$result = $db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			$savedStatus=$row['intStatus'];
			$savedLevels=$row['intApproveLevels'];
		}
		//--------------------------
		$locationFlag=getLocationValidation('fabGP',$location,$serialNo,$year);
		//--------------------------
		$editPermission=loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
		
		
		 if(( $locationFlag==1) && ($editMode==1)){
			 $rollBackFlag=1;
			 $rollBackMsg="Invalid Edit Location";
		 }
		 else if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This GP No is already confirmed.cant edit.";
		 }
		 else if($editPermission==0){
			 $rollBackFlag=1;
			 $rollBackMsg="No Permission to save.";
		 }
		else if($editMode==1){
		$sql = "UPDATE `ware_fabricgatepassheader` SET intStatus ='$maxStatus', 
												       intApproveLevels ='$ApproveLevels', 
													  intGatepassTo ='$gpTo', 
													  intOrderNo ='$orderNo', 
													  intOrderYear ='$orderYear', 
													  strremarks ='$remarks', 
													  dtmdate ='$gpDate', 
													  intModifiedBy ='$userId', 
													  dtmModifiedDate =now() 
				WHERE (`intFabricGatePassNo`='$serialNo') AND (`intFabricGatePassYear`='$year')";
		$result = $db->RunQuery2($sql);
		}
		else{
			//$sql = "DELETE FROM `ware_fabricgatepassheader` WHERE (`intFabricGatePassNo`='$serialNo') AND (`intFabricGatePassYear`='$year')";
			//$result1 = $db->RunQuery2($sql);

			$sql = "INSERT INTO `ware_fabricgatepassheader` (`intFabricGatePassNo`,`intFabricGatePassYear`,intGatepassTo,intOrderNo,intOrderYear,strRemarks,intStatus,intApproveLevels,dtmdate,dtmCreateDate,intCteatedBy,intCompanyId) 
					VALUES ('$serialNo','$year','$gpTo','$orderNo','$orderYear','$remarks','$maxStatus','$ApproveLevels','$gpDate',now(),'$userId','$location')";
			$result = $db->RunQuery2($sql);
		}
		//-----------delete and insert to detail table-----------------------
		if(($result) && ($rollBackFlag!=1)){
			$sql = "DELETE FROM `ware_fabricgatepassdetails` WHERE (`intFabricGatePassNo`='$serialNo') AND (`intFabricGatePassYear`='$year')";
			$result2 = $db->RunQuery2($sql);
			$saved=0;
			$toSave=0;
			$rollBackMsg="Maximum Qty for followings...";
			foreach($arr as $arrVal)
			{
				$cutNo = $arrVal['cutNo'];
				$salesOrderId 	 = $arrVal['salesOrderId'];
				$salesOrderNo	 = $arrVal['salesOrderNo'];
				$partId 	 = $arrVal['partId'];
				$part 	 = $arrVal['part'];
				$bgColorId 		 = $arrVal['bgColorId'];
				$bgColor 	 = $arrVal['bgColor'];
				$line 	 = $arrVal['line'];
				$size 		 = $arrVal['size'];
				$qty 		 = round($arrVal['qty'],4);
				//$place = 'Stores';
				//$type 	 = 'Received';
				$balQty=loadBalQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$partId,$size);
				$nonstckConfGpQty=loadNonstckConfGpQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$size);
				$orderQty=loadOrderQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade);
				$maxQty=$balQty-$nonstckConfGpQty;

				//------check maximum FR Qty--------------------
				if($qty>$maxQty){
					$rollBackFlag=1;
					$rollBackMsg .="<br> ".$cutNo."-".$part."-".$salesOrderNo."-".$size." =".$maxQty;
				}

				//----------------------------
				if($rollBackFlag!=1){
					
					$sql = "INSERT INTO `ware_fabricgatepassdetails` (`intFabricGatePassNo`,`intFabricGatePassYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`intGroundColor`,`strLineNo`,`strSize`,`dblQty`) 
					VALUES ('$serialNo','$year','$cutNo','$salesOrderId','$partId','$bgColorId','$line','$size','$qty')";
					$result = $db->RunQuery2($sql);
					if($result==1){
					$saved++;
					}
				}
				$toSave++;
		}
		}
		//echo $rollBackFlag;
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$confirmationMode=getConfirmationMode($programCode,$userId,$ApproveLevels,$maxStatus);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
//----------------------------------------
	echo json_encode($response);
//----------------------------------------




//----------------------------------------
	function getNextSerialNo()
	{
		global $db;
		global $location;
		$sql = "SELECT
				sys_no.intFabricGatePassNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$location'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextNo = $row['intFabricGatePassNo'];
		
		$sql = "UPDATE `sys_no` SET intFabricGatePassNo=intFabricGatePassNo+1 WHERE (`intCompanyId`='$location')  ";
		$db->RunQuery2($sql);
		
		return $nextNo;
	}
//-----------------------------------------------------------
function loadOrderQty($location,$orderNo,$orderYear,$salesOrderNo,$size,$grade)
{
		global $db;
		  $sql = "SELECT
				trn_ordersizeqty.dblQty
				FROM trn_ordersizeqty
				WHERE
				trn_ordersizeqty.intOrderNo =  '$orderNo' AND
				trn_ordersizeqty.intOrderYear =  '$orderYear' AND
				trn_ordersizeqty.intSalesOrderId =  '$salesOrderNo' AND
				trn_ordersizeqty.strSize =  '$size'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
	//--------------------------------------------------------------
	function loadBalQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$intPart,$size){
		global $db;
		   $sql = "SELECT
					Sum(ware_stocktransactions_fabric.dblQty) AS qty
					FROM ware_stocktransactions_fabric
					WHERE
					ware_stocktransactions_fabric.intLocationId =  '$location' AND
					ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
					ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
					ware_stocktransactions_fabric.strCutNo =  '$cutNo' AND
					ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
					ware_stocktransactions_fabric.strSize =  '$size' AND
					ware_stocktransactions_fabric.intPart =  '$intPart'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['qty']);
	}
//--------------------------------------------------------------
	function loadNonstckConfGpQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$size)
{
		global $db;
	  	$sql = "SELECT
				Sum(ware_fabricgatepassdetails.dblQty) as dblQty 
				FROM
				ware_fabricgatepassheader
				Inner Join ware_fabricgatepassdetails ON ware_fabricgatepassheader.intFabricGatePassNo = ware_fabricgatepassdetails.intFabricGatePassNo AND ware_fabricgatepassheader.intFabricGatePassYear = ware_fabricgatepassdetails.intFabricGatePassYear
				WHERE
				ware_fabricgatepassheader.intStatus >  1 AND
				ware_fabricgatepassheader.intStatus <=  ware_fabricgatepassheader.intApproveLevels AND
				ware_fabricgatepassheader.intOrderNo =  '$orderNo' AND
				ware_fabricgatepassheader.intOrderYear =  '$orderYear' AND 
				ware_fabricgatepassdetails.strCutNo =  '$cutNo' AND 
				ware_fabricgatepassdetails.intSalesOrderId =  '$salesOrderId' AND
				ware_fabricgatepassdetails.strSize =  '$size'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//--------------------------------------------------------------
function getConfirmationMode($programCode,$intUser,$approveLevels,$status){
		global $db;
		$k=$approveLevels+2-$status;
		$sqlp = "SELECT 
		menupermision.intEdit, 
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		if($rowp['int'.$k.'Approval']==1) 
		 $confirmatonMode=1; 
		else
		 $confirmatonMode=0; 

		return $confirmatonMode;
	}
//-----------------------------------------------------------
	function getSaveStatus($serialNo,$year)
	{
		global $db;
		$sql = "SELECT ware_fabricgatepassheader.intStatus, ware_fabricgatepassheader.intApproveLevels FROM ware_fabricgatepassheader WHERE (`intFabricGatePassNo`='$serialNo') AND (`intFabricGatePassYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			
		return $editableFlag;
	}
//-----------------------------------------------------------
	function getLocationValidation($type,$location,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
		$sql = "SELECT
					ware_fabricgatepassheader.intCompanyId
					FROM
					ware_fabricgatepassheader
					Inner Join mst_locations ON ware_fabricgatepassheader.intCompanyId = mst_locations.intId
					WHERE
					ware_fabricgatepassheader.intFabricGatePassNo =  '$serialNo' AND
					ware_fabricgatepassheader.intFabricGatePassYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			if($location == $row['intCompanyId'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//--------------------------------------------------------

?>

var basepath	= 'presentation/customerAndOperation/bulk/fabricGatePass/addNew/';
		
$(document).ready(function() {	
  		$("#frmFabricGatePass").validationEngine();
		
		$("#frmFabricGatePass #cboStyle").die('change').live('change',function(){
	       $('#cboSalesOrderNo').val('');
		   $('#cboGraphicNo').val('');
		});
		//-----------------------------------------------------------------
			$("#frmFabricGatePass #cboOrderYear").die('change').live('change',function(){
			 $('#cboGraphicNo').val('');
			 $('#cboStyle').val('');
			 $('#cboPONo').val('');
			 $('#cboOrderNo').val('');
			 $('#cboCustomer').val('');
			 $('#cboSalesOrderNo').val('');
			 clearGrid();
			 loadAllCombo();
			});
		//-----------------------------------------------------------------
		$("#frmFabricGatePass #cboSalesOrderNo").die('change').live('change',function(){
			if($("#cboOrderNo").val()=='')
			loadOrderNos();
		});
		//-----------------------------------------------------------------
		$("#frmFabricGatePass #cboOrderNo").die('change').live('change',function(){
			submitForm();
		});
		$("#frmFabricGatePass .search").die('change').live('change',loadAllCombo);
		
		$("#frmFabricGatePass .calTot").die('keyup').live('keyup',function(){
			validateTot(this);
		});
//-----------------------------------------------------------------
	$('#frmFabricGatePass #butInsertRowPopup').die('click').live('click',function(){
		var row=this.parentNode.parentNode.rowIndex;
		var orderNo= $('#cboOrderNo').val();
		var orderYear= $('#cboOrderYear').val();
		var salesOrderNo = $('#cboSalesOrderNo').val();
		var serialNo=$('#txtSerialNo').val();
		var year=$('#txtYear').val();
		var styleNo= $('#cboStyle').val();
		var graphicNo= $('#cboGraphicNo').val();
		var custPONo= $('#cboPONo').val();
		
		if(orderNo==''){
			alert("Please select the Order No.");
			return false;
		}
		
		popupWindow3('1');
		$('#popupContact1').load(basepath+'fabricGatePassPopup.php?orderNo='+orderNo+'&orderYear='+orderYear+'&serialNo='+serialNo+'&year='+year+'&styleNo='+URLEncode(styleNo)+'&graphicNo='+URLEncode(graphicNo)+'&custPONo='+URLEncode(custPONo)+'&salesOrderNo='+URLEncode(salesOrderNo),function(){
				//loadAlreadySaved();
				$("#frmGPPopup .searchP").die('change').live('change',function(){
					loadSalesLineNoPartSizesComboes();
				});
				$("#frmGPPopup #chkAll").die('click').live('click',function(){
					checkUncheckRows(this);
				});
				 //-------------------------------------------- 
				  $('#frmGPPopup #imgSearchItems').die('click').live('click',function(){
					  	var cutNoP=$('#txtCutNo').val();
						if(cutNoP==''){
							alert("Please select the Cut No");
							return false;
						}
						var rowCount = document.getElementById('tblPopup').rows.length;
						for(var i=1;i<rowCount;i++)
						{
							document.getElementById('tblPopup').deleteRow(1);
						}
						var orderYear = $('#cboOrderYear').val();
						var orderNo = $('#cboOrderNo').val();
						var styleNo = $('#cboStyleP').val();
						var salesOrderId = $('#cboSalesOrderNo').val();
						if(salesOrderId=='')
						var salesOrderId = $('#cboSalesOrderNoP').val();
						
						var lineNo = $('#cboLineNo').val();
						var part = $('#cboPartNo').val();
						var cutNo = $('#cboCutNo').val();
						var size = $('#cboSizes').val();
						var color = $('#cboColor').val();
						
						var url 		= basepath+"fabricGatePass-db-get.php?requestType=loadPartDetails";
						
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							data:"orderNo="+orderNo+"&orderYear="+orderYear+"&styleNo="+URLEncode(styleNo)+"&salesOrderId="+salesOrderId+"&lineNo="+URLEncode(lineNo)+"&part="+part+"&cutNo="+URLEncode(cutNo)+"&size="+URLEncode(size)+"&color="+URLEncode(color),  
							async:false,
							success:function(json){
								if(json.arrCombo){
								var length = json.arrCombo.length;
								var arrCombo = json.arrCombo;

								for(var i=0;i<length;i++)
								{
									var cutNo=arrCombo[i]['cutNo'];	
									var salesOrderId=arrCombo[i]['salesOrderId'];	
									var salesOrderNo=arrCombo[i]['salesOrderNo'];	
									var partId=arrCombo[i]['partId'];	
									var part=arrCombo[i]['part'];	
									var groundColorId=arrCombo[i]['groundColorId'];	
									var groundColor=arrCombo[i]['groundColor'];	
									var lineNo=arrCombo[i]['lineNo'];
									var size=arrCombo[i]['size'];	
								//	var qty=arrCombo[i]['qty'];	
									var balQty=parseFloat(arrCombo[i]['qty']);	
									var nonstckConfGpQty=parseFloat(arrCombo[i]['nonstckConfGpQty']);	
									balQty=balQty-nonstckConfGpQty;
										
									var content='<tr class="normalfnt">';
									content +='<td align="center" bgcolor="#FFFFFF" id=""><input type="checkbox" id="chkDisp" class="chk"/></td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+cutNo+'" class="cutNoP">'+cutNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderId+'" class="salesOrderNoP">'+salesOrderNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+partId+'" class="partP">'+part+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+groundColorId+'" class="bgColorP">'+groundColor+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+lineNo+'" class="lineP">'+lineNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+size+'" class="sizeP">'+size+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+balQty+'" class="balQtyP">'+balQty+'</td></tr>';
									add_new_row('#frmGPPopup #tblPopup',content);
									
									
								}
								
									checkAlreadySelected();
								}
									$('#frmGPPopup #butAdd').die('click').live('click', addClickedRows);
									$('#frmGPPopup #butClose1').die('click').live('click',disablePopup);

							}
							
						});
						
				  });
					//------------------
				
			});	
	});
//------------------------------------------------------------------
  $('#frmFabricGatePass #butSave').die('click').live('click',function(){
	var requestType = '';
	if ($('#frmFabricGatePass').validationEngine('validate'))   
    { 
		showWaiting();
		var data = "requestType=save";
		
			data+="&serialNo="		+	$('#txtSerialNo').val();
			data+="&Year="	+	$('#txtYear').val();
/*			data+="&style="	+	URLEncode($('#cboStyle').val());
			data+="&graphicNo="	+	URLEncode($('#cboGraphicNo').val());
			data+="&cusromerPO="	+	URLEncode($('#cboPONo').val());
*/			data+="&orderNo="	+	$('#cboOrderNo').val();
			data+="&orderYear="	+	$('#cboOrderYear').val();
			data+="&gpDate="	+	$('#dtDate').val();
			data+="&gpTo="	+	$('#cboGatePassTo').val();
			data+="&remarks="	+	URLEncode($('#txtNote').val());

			var rowCount = document.getElementById('tblMain').rows.length;
			if($('#cboGatePassTo').val()==''){
					alert("Please select the Gate pass To location");hideWaiting();
				return false;				
			}
			else if(rowCount==1){
				alert("No items to dispatch");hideWaiting();
				return false;				
			}
			var row = 0;
			
			var arr="[";
			
			$('#tblMain .cutNo').each(function(){
				
				var cutNo	= $(this).attr('id');
				var salesOrderId	= $(this).parent().find(".salesOrderNo").attr('id');
				var salesOrderNo	= $(this).parent().find(".salesOrderNo").html();
				var partId	= $(this).parent().find(".part").attr('id');
				var part	= $(this).parent().find(".part").html();
				var bgColorId	= $(this).parent().find(".bgColor").attr('id');
				var bgColor	= $(this).parent().find(".bgColor").html();
				var line	= $(this).parent().find(".line").html();
				var size	= $(this).parent().find(".size").html();
				var qty	= parseFloat($(this).parent().find(".qty").val());
				
					
					if(qty>0){
				        arr += "{";
						arr += '"cutNo":"'+	URLEncode(cutNo) +'",' ;
						arr += '"salesOrderId":"'+ salesOrderId +'",' ;
						arr += '"salesOrderNo":"'+ salesOrderNo +'",' ;
						arr += '"partId":"'+ partId +'",' ;
						arr += '"part":"'+ URLEncode(part) +'",' ;
						arr += '"bgColorId":"'+	bgColorId +'",' ;
						arr += '"bgColor":"'+ URLEncode(bgColor) +'",' ;
						arr += '"line":"'+ URLEncode(line) +'",' ;
						arr += '"size":"'+ URLEncode(size) +'",' ;
						arr += '"qty":"'+ qty +'"' ;
						arr +=  '},';
						
					}
			});
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basepath+"fabricGatePass-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json", 
			type:'POST', 
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmFabricGatePass #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						//loadCombo_frmSampleInfomations();
						var t=setTimeout("alertx()",1000);
						$('#txtSerialNo').val(json.serialNo);
						$('#txtYear').val(json.year);
						if(json.confirmationMode==1){
							$('#butConfirm').show();
						}
						else{
							$('#butConfirm').hide();
						}
					//	document.location.href =document.location.href ;
					}
				},
			error:function(xhr,status){
					
					$('#frmFabricGatePass #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
			hideWaiting();
	}
   });

$('#frmFabricGatePass #butReport').die('click').live('click',function(){
	if($('#txtSerialNo').val()!=''){
		window.open('?q=936&serialNo='+$('#txtSerialNo').val()+'&year='+$('#txtYear').val());	
	}
	else{
		alert("There is no Fabric Gate Pass No to view");
	}
});
//----------------------------------	
$('#frmFabricGatePass #butConfirm').die('click').live('click',function(){
	if($('#txtSerialNo').val()!=''){
		window.open('?q=936&serialNo='+$('#txtSerialNo').val()+'&year='+$('#txtYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no Fabric Gate Pass No to view");
	}
});
//-----------------------------------------------------
$('#frmFabricGatePass #butClose').die('click').live('click',function(){
});
//--------------refresh the form---------------
	$('#frmFabricGatePass #butNew').die('click').live('click',function(){
		window.location.href = "?q=47";
		$('#frmFabricGatePass').get(0).reset();
		$('#frmFabricGatePass #txtSerialNo').val('');
		$('#frmFabricGatePass #txtYear').val('');
		$('#frmFabricGatePass #cboStyle').val('');
	//	$('#frmFabricGatePass #txtAODNo').val('');
		$('#frmFabricGatePass #txtNote').val('');
		$("#frmFabricGatePass #cboStyle").change();
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmFabricGatePass #dtDate').val(d);
	});
	//----------------------------------------

});//----------end of ready --------

//-------------------------------------
function alertx()
{
	$('#frmInvoice #butSave').validationEngine('hide')	;
}
//----------------------------------------------- 
function alertDelete()
{
	$('#frmInvoice #butDelete').validationEngine('hide')	;
}
//----------------------------------------------- 
function closePopUp(){
	
}
//-----------------------------------------------
function addClickedRows()
{
	if ($('#frmGPPopup').validationEngine('validate'))   
    { 
	var rowCount = document.getElementById('tblPopup').rows.length;
	var cutNo=$('#txtCutNo').val();

	$('#tblPopup .salesOrderNoP').each(function(){
		var cutNo	= $(this).parent().find(".cutNoP").attr('id');
		var salesOrderIdP	= $(this).attr('id');
		var salesOrderNoP	= $(this).html();
		var partIdP	= $(this).parent().find(".partP").attr('id');
		var partP	= $(this).parent().find(".partP").html();
		var bgColorIdP	= $(this).parent().find(".bgColorP").attr('id');
		var bgColorP	= $(this).parent().find(".bgColorP").html();
		var lineP	= $(this).parent().find(".lineP").html();
		var sizeP	= $(this).parent().find(".sizeP").html();
		var balQtyP	= $(this).parent().find(".balQtyP").html();
		var diasbleFlag	= $(this).parent().find(".chk").is(':disabled');
		var chkFlag	= $(this).parent().find(".chk").is(':checked');
		
	//	alert(qtyP);		
		if((balQtyP>0) && (diasbleFlag==false) && (chkFlag==true)){
			//alert($('#frmGatePass #cboDispatchTo').val());
			var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="images/del.png" width="15" height="15" /></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+cutNo+'" class="cutNo">'+cutNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderIdP+'" class="salesOrderNo">'+salesOrderNoP+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+partIdP+'" class="part">'+partP+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+bgColorIdP+'" class="bgColor">'+bgColorP+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+lineP+'" class="line">'+lineP+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+sizeP+'" class="size">'+sizeP+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+balQtyP+'" class="balQty">'+balQtyP+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="qty" class="validate[required,custom[integer],max['+balQtyP+']] qty calTot" style="width:80px;text-align:center" type="text" value="0"/></td>';
			content +='</tr>';
			
			add_new_row('#frmFabricGatePass #tblMain',content);
		//----------------------------	
		$('.delImg').die('click').live('click',function(){
			$(this).parent().parent().remove();
		});
	}
	});
	checkAlreadySelected();
	//disablePopup();
	}
}
//-------------------------------------
function loadGraphicNo(){
	    var orderYear = $('#cboOrderYear').val();
	    var styleNo = $('#cboStyle').val();
		var url 		= basepath+"fabricGatePass-db-get.php?requestType=loadGraphicNo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"styleNo="+URLEncode(styleNo)+"&orderYear="+orderYear,
			async:false,
			success:function(json){				
				document.getElementById("cboGraphicNo").innerHTML	= json.graphicNo;
				document.getElementById("txtQty").value				= '';
			}
		});
}

//-----------------------------------------------
function loadCustomerPONo(){
	    var orderYear = $('#cboOrderYear').val();
	    var graphicNo = $('#cboGraphicNo').val();
	    var styleNo = $('#cboStyle').val();
		var url 		= basepath+"fabricGatePass-db-get.php?requestType=loadCustomerPONo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"graphicNo="+URLEncode(graphicNo)+"&orderYear="+orderYear+"&styleNo="+URLEncode(styleNo),
			async:false,
			success:function(json){
					document.getElementById("cboPONo").innerHTML=json.customerPoNo;
			}
		});
}

//-----------------------------------------------
function loadOrderNo(){
	    var styleNo = $('#cboPONo').val();
	    var graphicNo = $('#cboPONo').val();
	    var orderYear = $('#cboOrderYear').val();
	    var poNo = $('#cboPONo').val();
	    var customer = $('#cboCustomer').val();
		var url 		= basepath+"fabricGatePass-db-get.php?requestType=loadOrderNo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"poNo="+URLEncode(poNo)+"&customer="+customer+"&orderYear="+orderYear+"&styleNo="+URLEncode(styleNo)+"&graphicNo="+URLEncode(graphicNo)+"&poNo="+URLEncode(poNo),
			async:false,
			success:function(json){
				
					document.getElementById("cboOrderNo").innerHTML=json.orderNo;
					document.getElementById("cboCustomer").value=json.customer;
					if(poNo==''){
					document.getElementById("cboPONo").innerHTML=json.PoNo;
					}
			}
		});
}
//--------------------------------------------------
function loadCustomerPO(){
	    var orderNo = $('#cboOrderNo').val();
	    var orderYear = $('#cboOrderYear').val();
		var url 		= basepath+"fabricGatePass-db-get.php?requestType=loadPoNo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"orderNo="+orderNo+"&orderYear="+orderYear,
			async:false,
			success:function(json){
				
					document.getElementById("cboPONo").value=json.poNo;
					document.getElementById("cboCustomer").value=json.customer;
					if(orderNo==''){
					document.getElementById("cboOrderNo").innerHTML=json.orderNo;
					}
					//document.getElementById("cboSalesOrderNo").innerHTML=json.salesOrderNo;
			}
		});
}
//-----------------------------------------------
function loadPONoAndOrderNo(){
	    var styleNo = $('#cboStyle').val();
	    var graphicNo = $('#cboPONo').val();
	    var poNo = $('#cboPONo').val();
	    var orderNo = $('#cboOrderNo').val();
	    var orderYear = $('#cboOrderYear').val();
	    var customer = $('#cboCustomer').val();
		var url 		= basepath+"fabricGatePass-db-get.php?requestType=loadPONoAndOrderNo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"customer="+customer+"&orderYear="+orderYear+"&styleNo="+URLEncode(styleNo)+"&graphicNo="+URLEncode(graphicNo),
			async:false,
			success:function(json){
					//alert(json.poNo);
					document.getElementById("cboPONo").innerHTML=json.poNo;
					document.getElementById("cboOrderNo").innerHTML=json.orderNo;
			}
		});
}
//-----------------------------------------------
function loadSalesLineNoPartSizesComboes(){
	    var orderNo = $('#cboOrderNo').val();
	    var orderYear = $('#cboOrderYear').val();
	    var styleNo = $('#cboStyleP').val();
	    var salesOrderNo = $('#cboSalesOrderNoP').val();
		var url 		= basepath+"fabricGatePass-db-get.php?requestType=loadSalesLineNoPartSizesComboes";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"orderNo="+orderNo+"&orderYear="+orderYear+"&salesOrderNo="+URLEncode(salesOrderNo)+"&styleNo="+URLEncode(styleNo),
			async:false,
			success:function(json){
				
					document.getElementById("cboGraphicP").innerHTML=json.graphicNo;
					document.getElementById("cboSalesOrderNoP").innerHTML=json.salesOrderNo;
					document.getElementById("cboLineNo").innerHTML=json.lineNo;
					document.getElementById("cboPartNo").innerHTML=json.partNo;
					document.getElementById("cboSizes").innerHTML=json.sizes;
					document.getElementById("cboCutNo").innerHTML=json.cutNo;
					document.getElementById("cboColor").innerHTML=json.color;
			}
		});
}
//-----------------------------------------------
function loadQty(){
	    var orderNo = $('#cboOrderNo').val();
	    var salesOrderNo = '';
		var url 		= basepath+"fabricGatePass-db-get.php?requestType=loadQty";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"orderNo="+orderNo+"&salesOrderNo="+salesOrderNo,
			async:false,
			success:function(json){
				
					document.getElementById("txtQty").value=json.qty;
			}
		});
}
//------------------------------------------------
function submitForm(){
	window.location.href = "?q=47&orderNo="+$('#cboOrderNo').val()
						+'&salesOrderNo='+$('#cboSalesOrderNo').val()
						+'&poNo='+$('#cboPONo').val()
						+'&customer='+$('#cboCustomer').val()
						+'&orderYear='+$('#cboOrderYear').val()
						+'&styleNo='+$('#cboStyle').val()
						+'&graphicNo='+$('#cboGraphicNo').val()
}
//----------------------------------------------- 
function clearGrid()
{
	var rowCount = document.getElementById('tblMain').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMain').deleteRow(1);
	}
	
	$('#txtQty').val('');
}
//---------------------------------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//------------------------------------------------------------------------------
function checkAlreadySelected(){
		$('#tblMain .cutNo').each(function(){
			
		 var cutNo	= $(this).attr('id');
		 var salesOrderId	= $(this).parent().find(".salesOrderNo").attr('id');
		 var size	= $(this).parent().find(".size").html();
		
			  $('#tblPopup .salesOrderNoP').each(function(){
				
				var cutNoP=$(this).parent().find(".cutNoP").html();
				var salesOrderIdP	= $(this).attr('id');
				var sizeP	= $(this).parent().find(".sizeP").html();
				// alert(cutNo+"=="+cutNoP+"***"+salesOrderId+"=="+salesOrderIdP+"***"+size+"=="+sizeP+"***")
				if((cutNo==cutNoP) && (salesOrderId==salesOrderIdP) && (size==sizeP)){
					$(this).parent().find(".chk").prop('checked', true);
					$(this).parent().find(".chk").prop('disabled',true);
				}
			});
	});
}
//------------------------------------------------------------------------------
function checkUncheckRows(obj){
	
		var chkFlag=$(obj).is(':checked');
		
		  $('#tblPopup .salesOrderNoP').each(function(){
				var disabledFlag=$(this).parent().find(".chk").is(':disabled');
				if((chkFlag==true) && (disabledFlag!=true)){
					$(this).parent().find(".chk").prop('checked', true);
				}
				else if((chkFlag!=true) && (disabledFlag==true)){
					$(this).parent().find(".chk").prop('checked', true);
				}
				else if(chkFlag!=true){
					$(this).parent().find(".chk").prop('checked', false);
				}
		});
}
//------------------------------------------------------------------------
function validateTot(obj){
	var bal=$(obj).parent().parent().find(".balQty").html();
	var tot=parseFloat($(obj).parent().parent().find(".sqtyP").val())+parseFloat($(obj).parent().parent().find(".gqtyP").val())+parseFloat($(obj).parent().parent().find(".eqtyP").val())+parseFloat($(obj).parent().parent().find(".pqtyP").val())+parseFloat($(obj).parent().parent().find(".fqtyP").val());
//	alert(bal);
// 	alert(tot);
	if(bal<tot){
		alert("Input Quantities does not match with Balance Qty");
		$(obj).val(0);
		return false;
	}
	else{
		$(obj).parent().parent().find(".tot").html(tot.toFixed(2));
	}
}
//----------------------------------------------------------------------------
function loadAllCombo()
{
	//######################
	//####### get values ###
	//######################
	var year 		= $('#cboOrderYear').val();
	var graphicNo 	= $('#cboGraphicNo').val();
	var styleId 	= $('#cboStyle').val();
	var customerPONo= $('#cboPONo').val();
	var orderNo		= $('#cboOrderNo').val();
	var customerId 	= $('#cboCustomer').val();
	
	//######################
	//####### create url ###
	//######################
	var url		= basepath+"fabricGatePass-db-get.php?requestType=loadAllComboDetails";
		url	   +="&year="+			year;	
		url	   +="&graphicNo="+		URLEncode(graphicNo);
		url	   +="&styleId="+		URLEncode(styleId);
		url	   +="&customerPONo="+	URLEncode(customerPONo);
		url	   +="&orderNo="+		orderNo;
		url	   +="&customerId="+	customerId;
	
	//######################
	//####### send data  ###
	//######################
	$.ajax({url:url,
			async:false,
			dataType:'json',
			success:function(json){
					$('#cboStyle').html(json.styleNo);
					$('#cboGraphicNo').html(json.graphicNo);
					$('#cboPONo').html(json.customerPoNo);
					$('#cboOrderNo').html(json.orderNo);
					$('#cboSalesOrderNo').html(json.salesOrderNo);
					$('#cboCustomer').html(json.customer);
			}
			});
}
//---------------------------------------------------------------------

function loadOrderNos()
{
	//######################
	//####### get values ###
	//######################
	var year 		= $('#cboOrderYear').val();
	var graphicNo 	= $('#cboGraphicNo').val();
	var styleId 	= $('#cboStyle').val();
	var customerPONo= $('#cboPONo').val();
	var customerId 	= $('#cboCustomer').val();
	var salesOrderNo 	= $('#cboSalesOrderNo').val();
	
	//######################
	//####### create url ###
	//######################
	var url		= basepath+"fabricGatePass-db-get.php?requestType=loadOrderNosToSalesOrderNos";
		url	   +="&year="+			year;	
		url	   +="&graphicNo="+		graphicNo;
		url	   +="&styleId="+		styleId;
		url	   +="&customerPONo="+	customerPONo;
		url	   +="&salesOrderNo="+		salesOrderNo;
		url	   +="&customerId="+	customerId;
	
	//######################
	//####### send data  ###
	//######################
	$.ajax({url:url,
			async:false,
			dataType:'json',
			success:function(json){
					$('#cboOrderNo').html(json.orderNo);
			}
			});
}

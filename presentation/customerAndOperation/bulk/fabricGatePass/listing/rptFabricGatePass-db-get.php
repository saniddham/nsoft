<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
	session_start();
	$backwardseperator = "../../../../../";
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";

	$programName='Fabric Gate Pass';
	$programCode='P0047';
	$fabRcvApproveLevel = (int)getApproveLevel($programName);
	

 if($requestType=='getValidation')
	{
		$serialNo  = $_REQUEST['serialNo'];
		$serialNoArray=explode("/",$serialNo);
		$orderStatus=getOrderStatus($serialNoArray[0],$serialNoArray[1]);//check wether already confirmed
		
		$sql = "SELECT   
				ware_fabricgatepassheader.intStatus, 
				ware_fabricgatepassheader.intApproveLevels, 
				ware_fabricgatepassheader.intCompanyId, 
				ware_fabricgatepassheader.intOrderNo, 
				ware_fabricgatepassheader.intOrderYear, 
				trn_orderdetails.strSalesOrderNo,
				trn_orderdetails.intSalesOrderId, 
				ware_fabricgatepassdetails.strCutNo,
				ware_fabricgatepassdetails.intPart, 
				mst_part.strName as part,
				ware_fabricgatepassdetails.intGroundColor, 
				mst_colors_ground.strName as bgcolor,
				ware_fabricgatepassdetails.strLineNo,
				ware_fabricgatepassdetails.strSize,
				ware_fabricgatepassdetails.dblQty
				FROM
				trn_orderdetails
				Inner Join ware_fabricgatepassheader ON ware_fabricgatepassheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricgatepassheader.intOrderYear = trn_orderdetails.intOrderYear
				Inner Join ware_fabricgatepassdetails ON ware_fabricgatepassheader.intFabricGatePassNo = ware_fabricgatepassdetails.intFabricGatePassNo AND ware_fabricgatepassheader.intFabricGatePassYear = ware_fabricgatepassdetails.intFabricGatePassYear AND ware_fabricgatepassdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
				Inner Join mst_part ON ware_fabricgatepassdetails.intPart = mst_part.intId
				Inner Join mst_colors_ground ON ware_fabricgatepassdetails.intGroundColor = mst_colors_ground.intId
				WHERE
				ware_fabricgatepassheader.intFabricGatePassNo =  '$serialNoArray[0]' AND
				ware_fabricgatepassheader.intFabricGatePassYear =  '$serialNoArray[1]' ";
				
		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$msg1 =""; 
		$msg ="Maximum Qtys for items"; 
		while($row=mysqli_fetch_array($result))
		{
				$status=$row['intStatus'];
				$approveLevels=$row['intApproveLevels'];
				$location=$row['intCompanyId'];
				$orderNo=$row['intOrderNo'];
				$orderYear=$row['intOrderYear'];
				$salesOrderNo=$row['strSalesOrderNo'];
				$salesOrderId=$row['intSalesOrderId'];
				$cutNo=$row['strCutNo'];
				$partId=$row['intPart'];
				$part=$row['part'];
				$size=$row['strSize'];
				$qty=$row['dblQty'];
			//	$tot=val($row['dblSampleQty'])+val($row['dblGoodQty'])+val($row['dblEmbroideryQty'])+val($row['dblPDammageQty'])+val($row['dblFDammageQty']);
			
				$balQty=loadBalQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$partId,$size);
				$nonstckConfGpQty=loadNonstckConfGpQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$size);
				$orderQty=loadOrderQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade);
				$maxQty=$balQty-$nonstckConfGpQty;
				
				if($status<=$approveLevels){
				$maxQty=$balQty-$nonstckConfGpQty+$qty;
				}
				
				//echo $tot."pp".$maxQty;

				//------check maximum FR Qty--------------------
				$confirmatonMode=loadConfirmatonMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);
/*				 else if($orderStatus!=1){
					 $errorFlg=1;
					 $msg="This Order is revised or pending.";
				 }
*/				if($row['intStatus']==1){// 
					$errorFlg = 1;
					$msg ="Final confirmation of this GP No is already raised";
				}
				else if($row['intStatus']==0){// 
					$errorFlg = 1;
					$msg ="This GP No is rejected"; 
				}
				else if($confirmatonMode==0){// 
					$errorFlg = 1;
					$msg ="No Permission to Approve"; 
				}
				else if($qty>$maxQty){
					$errorFlg=1;
					$msg .="<br> ".$cutNo."-".$part."-".$salesOrderNo."-".$size." =".$maxQty;
				}
		}//end of while
	
	if($msg1!=''){
		$msg=$msg1;
	}
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}

else if($requestType=='validateRejecton')
	{
		$errorFlg=0;
		$serialNo  = $_REQUEST['serialNo'];
		$serialNoArray=explode("/",$serialNo);
		
		$sql = "SELECT
		ware_fabricgatepassheader.intStatus, 
		ware_fabricgatepassheader.intApproveLevels 
		FROM ware_fabricgatepassheader
		WHERE
		ware_fabricgatepassheader.intFabricGatePassNo =  '$serialNoArray[0]' AND
		ware_fabricgatepassheader.intFabricGatePassYear =  '$serialNoArray[1]'";

		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$msg =""; 
		$row=mysqli_fetch_array($result);
		
		$rejectionMode=loadRejectionMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);
		
		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this GP No is already raised.So cant reject"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This GP No is already Rejected"; 
		}
		else if($rejectionMode==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="No Permission to reject this GP No"; 
		}
		else{//confirmation has been raised
			$errorFlg = 0;
			
		}

	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);

	}

//-----------------------------------------------------------
	function getOrderStatus($serialNo,$year)
	{
		global $db;
	  	$sql = "SELECT
trn_orderheader.intStatus
FROM
ware_fabricreceivedheader
Inner Join trn_orderheader ON ware_fabricreceivedheader.intOrderNo = trn_orderheader.intOrderNo AND ware_fabricreceivedheader.intOrderYear = trn_orderheader.intOrderYear
WHERE
ware_fabricreceivedheader.intFabricReceivedNo =  '$serialNo' AND
ware_fabricreceivedheader.intFabricReceivedYear =  '$year'";
			$results = $db->RunQuery($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
 			
		return $status;
	}
//-----------------------------------------------------------
function loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade)
{
		global $db;
		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				ware_stocktransactions_fabric.intLocationId =  '$location' AND
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType =  'Received' 
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//-----------------------------------------------------------
function loadOrderQty($location,$orderNo,$orderYear,$salesOrderNo,$size,$grade)
{
		global $db;
		  $sql = "SELECT
				trn_ordersizeqty.dblQty
				FROM trn_ordersizeqty
				WHERE
				trn_ordersizeqty.intOrderNo =  '$orderNo' AND
				trn_ordersizeqty.intOrderYear =  '$orderYear' AND
				trn_ordersizeqty.intSalesOrderId =  '$salesOrderNo' AND
				trn_ordersizeqty.strSize =  '$size'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
	//--------------------------------------------------------------
	function loadBalQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$intPart,$size){
		global $db;
		   $sql = "SELECT
					Sum(ware_stocktransactions_fabric.dblQty) AS qty
					FROM ware_stocktransactions_fabric
					WHERE
					ware_stocktransactions_fabric.intLocationId =  '$location' AND
					ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
					ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
					ware_stocktransactions_fabric.strCutNo =  '$cutNo' AND
					ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
					ware_stocktransactions_fabric.strSize =  '$size' AND
					ware_stocktransactions_fabric.intPart =  '$intPart'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
	//	echo val($rows['qty']);
		return val($rows['qty']);
	}
//--------------------------------------------------------------
	function loadNonstckConfGpQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$size)
{
		global $db;
	  	$sql = "SELECT
				Sum(ware_fabricgatepassdetails.dblQty) as dblQty 
				FROM
				ware_fabricgatepassheader
				Inner Join ware_fabricgatepassdetails ON ware_fabricgatepassheader.intFabricGatePassNo = ware_fabricgatepassdetails.intFabricGatePassNo AND ware_fabricgatepassheader.intFabricGatePassYear = ware_fabricgatepassdetails.intFabricGatePassYear
				WHERE
				ware_fabricgatepassheader.intStatus >  1 AND
				ware_fabricgatepassheader.intStatus <=  ware_fabricgatepassheader.intApproveLevels AND
				ware_fabricgatepassheader.intOrderNo =  '$orderNo' AND
				ware_fabricgatepassheader.intOrderYear =  '$orderYear' AND 
				ware_fabricgatepassdetails.strCutNo =  '$cutNo' AND 
				ware_fabricgatepassdetails.intSalesOrderId =  '$salesOrderId' AND
				ware_fabricgatepassdetails.strSize =  '$size'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//--------------------------------------------------------

?>

<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId 		= $sessions->getLocationId();
$intUser  		= $sessions->getUserId();
$serialNo 		= $_REQUEST['serialNo'];
$year 			= $_REQUEST['year'];
$approveMode 	= (!isset($_REQUEST["approveMode"])?'':$_REQUEST["approveMode"]);

$programCode	= 'P0047';

  $sql = "SELECT 
ware_fabricgatepassheader.intFabricGatePassNo,
ware_fabricgatepassheader.intFabricGatePassYear, 
trn_orderdetails.strStyleNo,
trn_orderdetails.strGraphicNo, 
trn_orderheader.strCustomerPoNo,
ware_fabricgatepassheader.intOrderNo,
ware_fabricgatepassheader.intOrderYear,
ware_fabricgatepassheader.strRemarks,
ware_fabricgatepassheader.intStatus,
ware_fabricgatepassheader.intApproveLevels,
ware_fabricgatepassheader.dtmdate,
ware_fabricgatepassheader.dtmCreateDate,
sys_users.strUserName,
mst_customer.strName as customer ,
ware_fabricgatepassheader.intCompanyId , 
mst_locations.strName as locName 
FROM
ware_fabricgatepassheader  
Inner Join trn_orderheader ON ware_fabricgatepassheader.intOrderNo = trn_orderheader.intOrderNo AND ware_fabricgatepassheader.intOrderYear = trn_orderheader.intOrderYear
Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
left Join mst_customer ON trn_orderheader.intCustomer = mst_customer.intId 
left Join sys_users ON ware_fabricgatepassheader.intCteatedBy = sys_users.intUserId 
left Join mst_locations ON ware_fabricgatepassheader.intGatepassTo = mst_locations.intId 
WHERE
ware_fabricgatepassheader.intFabricGatePassNo =  '$serialNo' AND
ware_fabricgatepassheader.intFabricGatePassYear =  '$year'
";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$serialNo = $row['intFabricGatePassNo'];
					$SerialYear = $row['intFabricGatePassYear'];
					$style = $row['strStyleNo'];
					$graphicNo=$row['strGraphicNo'];
					$custPO = $row['strCustomerPoNo'];
					$orderNo = $row['intOrderNo'];
					$orderYear = $row['intOrderYear'];
					$date = $row['dtmdate'];
					$customer = $row['customer'];
					$remarks = $row['strRemarks'];
					//$date = $row['dtmCreateDate'];
					$intStatus = $row['intStatus'];
					$savedLevels = $row['intApproveLevels'];
					$user = $row['strUserName'];
					$locationId = $row['intCompanyId'];//this locationId use in report header(reportHeader.php)--------------------
					$locName = $row['locName'];
				 }
				 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Fabric Gate Pass Report</title>
<script type="text/javascript" src="presentation/customerAndOperation/bulk/fabricGatePass/listing/rptFabricGatePass-js.js?n=1"></script>
<style>
.break {
	page-break-before: always;
}
 @media print {
.noPrint {
	display: none;
}
}
#apDiv1 {
	position: absolute;
	left: 281px;
	top: 171px;
	width: 650px;
	height: 322px;
	z-index: 1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="images/pending.png"  /></div>
<?php
}
?>
<form id="frmFabGPReport" name="frmFabGPReport" method="post" action="rptFabricGatePass.php">
  <table width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="3"></td>
    </tr>
    <tr>
      <td width="20%"></td>
      <td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
      <td width="20%"></td>
    </tr>
    <tr>
      <td colspan="3"></td>
    </tr>
  </table>
  <div align="center">
    <div style="background-color:#FFF" ><strong>FABRIC GATE PASS REPORT</strong><strong></strong></div>
    <table width="900" border="0" align="center" bgcolor="#FFFFFF">
      <tr>
        <td><table width="100%">
            <tr>
              <td colspan="10" align="center" bgcolor="#FFDFCB"><?php
	if($intStatus>1)
	{
	
	//------------
	$k=$savedLevels+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
	
	 $resultp = $db->RunQuery($sqlp);
	 $rowp=mysqli_fetch_array($resultp);
	 $userPermission=0;
	 if($rowp['int'.$k.'Approval']==1){
		 $userPermission=1;
	 }
	//--------------	
	?>
                <?php if(($approveMode==1) and ($userPermission==1)) { ?>
                <img src="images/approve.png" align="middle" class="noPrint mouseover" id="imgApprove" /> <img src="images/reject.png" align="middle" class="noPrint mouseover" id="imgReject" />
                <?php
	}
	}
	?></td>
            </tr>
            <tr>
              <?php
 	if($intStatus==1)
	{
	?>
              <td colspan="10" class="APPROVE" >CONFIRMED</td>
              <?PHP
	}
	else if($intStatus==0)
	{
   ?>
              <td colspan="9" class="APPROVE" style="color:#F00">REJECTED</td>
              <?php
	}
	else
	{
   ?>
              <td width="10%" colspan="9" class="APPROVE">PENDING</td>
              <?php
	}
   ?>
            </tr>
            <tr>
              <td width="1%">&nbsp;</td>
              <td width="14%"><span class="normalfnt"><strong>Fabric Gate Pass No</strong></span></td>
              <td width="2%" align="center" valign="middle"><strong>:</strong></td>
              <td width="18%"><span class="normalfnt"><?php echo $serialNo  ?>/<?php echo $SerialYear ?></span></td>
              <td width="10%" class="normalfnt"><strong>Style No</strong></td>
              <td width="1%" align="center" valign="middle"><strong>:</strong></td>
              <td width="9%"><span class="normalfnt"><?php echo $style  ?></span></td>
              <td width="12%" class="normalfnt"><div id="divSerialNo" style="display:none"><?php echo $serialNo ?>/<?php echo $year ?></div>
                <strong>Graphic no</strong></td>
              <td width="1%"><strong>:</strong></td>
              <td width="13%"><span class="normalfnt"><?php echo $graphicNo
 ?></span></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="normalfnt"><strong>Customer PO</strong></td>
              <td align="center" valign="middle"><strong>:</strong></td>
              <td><span class="normalfnt"><?php echo $custPO   ?></span></td>
              <td><span class="normalfnt"><strong>Order No</strong></span></td>
              <td align="center" valign="middle"><strong>:</strong></td>
              <td><span class="normalfnt"><?php echo $orderNo."/".$orderYear; ?></span></td>
              <td class="normalfnt"><strong>Customer</strong></td>
              <td><strong>:</strong></td>
              <td><span class="normalfnt"><?php echo $customer; ?></span></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="normalfnt"><strong>Date</strong></td>
              <td align="center" valign="middle"><strong>:</strong></td>
              <td><span class="normalfnt"><?php echo $date; ?></span></td>
              <td class="normalfnt"><strong>By</strong></td>
              <td align="center" valign="middle"><strong>:</strong></td>
              <td><span class="normalfnt"><?php echo $user; ?></span></td>
              <td class="normalfnt"><strong>Gate Pass To</strong></td>
              <td>&nbsp;</td>
              <td><span class="normalfnt"><?php echo $locName; ?></span></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="normalfnt" valign="top"><strong>Remarks</strong></td>
              <td align="center" valign="top"><strong>:</strong></td>
              <td colspan="7"><textarea cols="50" rows="6" class="textarea" style="width::300px" disabled="disabled"><?php echo $remarks  ?></textarea></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><table width="100%">
            <tr>
              <td width="5%">&nbsp;</td>
              <td colspan="7" class="normalfnt"><table width="100%" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
                  <tr class="gridHeader">
                    <td width="12%" >Cut No</td>
                    <td width="17%" >Sales Order No</td>
                    <td width="12%" >Part</td>
                    <td width="22%" >Background Color</td>
                    <td width="15%" >Line No</td>
                    <td width="11%" >Size</td>
                    <td width="11%" >Qty</td>
                  </tr>
                  <?php 
	  	    $sql1 = "SELECT
trn_orderdetails.strSalesOrderNo,
ware_fabricgatepassdetails.strCutNo,
mst_part.strName as part,
mst_colors_ground.strName as bgcolor,
ware_fabricgatepassdetails.strLineNo,
ware_fabricgatepassdetails.strSize,
ware_fabricgatepassdetails.dblQty
FROM
trn_orderdetails
Inner Join ware_fabricgatepassheader ON ware_fabricgatepassheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricgatepassheader.intOrderYear = trn_orderdetails.intOrderYear
Inner Join ware_fabricgatepassdetails ON ware_fabricgatepassheader.intFabricGatePassNo = ware_fabricgatepassdetails.intFabricGatePassNo AND ware_fabricgatepassheader.intFabricGatePassYear = ware_fabricgatepassdetails.intFabricGatePassYear AND ware_fabricgatepassdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
left Join mst_part ON ware_fabricgatepassdetails.intPart = mst_part.intId
left Join mst_colors_ground ON ware_fabricgatepassdetails.intGroundColor = mst_colors_ground.intId
WHERE
ware_fabricgatepassheader.intFabricGatePassNo =  '$serialNo' AND
ware_fabricgatepassheader.intFabricGatePassYear =  '$SerialYear' 
";
			$result1 = $db->RunQuery($sql1);
			$totQty			= 0;
			$totAmmount		= 0;
			$total			= 0;
			while($row=mysqli_fetch_array($result1))
			{
				$cutNo=$row['strCutNo'];
				$salesOrderNo=$row['strSalesOrderNo'];
				$part=$row['part'];
				$bgColor=$row['bgcolor'];
				$lineNo=$row['strLineNo'];
				$size=$row['strSize'];
				$Qty=$row['dblQty'];
	  ?>
                  <tr class="normalfnt"   bgcolor="#FFFFFF">
                    <td align="center" class="normalfntMid" id="<?php echo $cutNo; ?>" ><?php echo $cutNo; ?></td>
                    <td align="center" class="normalfntMid" id="<?php echo $salesOrderNo; ?>" ><?php echo $salesOrderNo; ?></td>
                    <td align="center" class="normalfntMid" id="<?php echo $part; ?>"><?php echo $part; ?></td>
                    <td align="center" class="normalfntMid" id="<?php echo $bgColor; ?>" ><?php echo $bgColor; ?></td>
                    <td align="center" class="normalfntMid" id="<?php echo $lineNo; ?>" ><?php echo $lineNo; ?></td>
                    <td align="center" class="normalfntMid" id="<?php echo $size; ?>" ><?php echo $size; ?></td>
                    <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $Qty; ?></td>
                  </tr>
                  <?php 
			$total+=$Qty;
			}
	  ?>
                  <tr class="normalfnt"  bgcolor="#CCCCCC">
                    <td class="normalfnt" >&nbsp;</td>
                    <td class="normalfnt" >&nbsp;</td>
                    <td class="normalfnt" >&nbsp;</td>
                    <td class="normalfnt" >&nbsp;</td>
                    <td class="normalfnt" >&nbsp;</td>
                    <td class="normalfnt" >&nbsp;</td>
                    <td class="normalfntRight" ><?php echo $total ?></td>
                  </tr>
                </table></td>
              <td width="6%">&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <?php  
 	if($intStatus!=0)
	{

				for($i=1; $i<=$savedLevels; $i++)
				{
					   $sqlc = "SELECT
							ware_fabricgatepassheader_approvedby.intApproveUser,
							ware_fabricgatepassheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName, 
							ware_fabricgatepassheader_approvedby.intApproveLevelNo
							FROM
							ware_fabricgatepassheader_approvedby
							Inner Join sys_users ON ware_fabricgatepassheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_fabricgatepassheader_approvedby.intFabricGatePassNo =  '$serialNo' AND
							ware_fabricgatepassheader_approvedby.intYear =  '$SerialYear'  AND
							ware_fabricgatepassheader_approvedby.intApproveLevelNo =  '$i'  order by intApproveLevelNo asc
";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
						if($i==1)
						$desc="1st ";
						else if($i==2)
						$desc="2nd ";
						else if($i==3)
						$desc="3rd ";
						else
						$desc=$i."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
				?>
      <tr>
        <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
      </tr>
      <?php
			}
	}
	else{
					 $sqlc = "SELECT
							ware_fabricgatepassheader_approvedby.intApproveUser,
							ware_fabricgatepassheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_fabricgatepassheader_approvedby.intApproveLevelNo
							FROM
							ware_fabricgatepassheader_approvedby
							Inner Join sys_users ON ware_fabricgatepassheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_fabricgatepassheader_approvedby.intFabricGatePassNo =  '$serialNo' AND
							ware_fabricgatepassheader_approvedby.intYear =  '$SerialYear'  AND
							ware_fabricgatepassheader_approvedby.intApproveLevelNo =  '0'";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
					  ?>
      <tr>
        <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Rejected By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
      </tr>
      <?php
	}
?>
      <tr height="40">
        <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
      </tr>
    </table>
  </div>
</form>
</body>
</html>
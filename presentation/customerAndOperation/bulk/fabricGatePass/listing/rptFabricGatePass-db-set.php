<?php 
//////////////////////////////////////////////
//Create By:H.B.G Korala
//note://if final confirmation raised,insert into stocktransaction table.
/////////////////////////////////////////////

	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];

	include "{$backwardseperator}dataAccess/Connector.php";
	
	$programName='Fabric Gate Pass';
	$programCode='P0047';
	$fabRcvApproveLevel = (int)getApproveLevel($programName);
	
	/////////// parameters /////////////////////////////

	
	$serialNo = $_REQUEST['serialNo'];
	$year = $_REQUEST['year'];
	
	if($_REQUEST['status']=='approve')
	{
		$sql = "UPDATE `ware_fabricgatepassheader` SET `intStatus`=intStatus-1 WHERE (intFabricGatePassNo='$serialNo') AND (`intFabricGatePassYear`='$year') and intStatus not in(0,1)";
		$result = $db->RunQuery($sql);
		
		if($result){
			$sql = "SELECT ware_fabricgatepassheader.intStatus, 
			ware_fabricgatepassheader.intApproveLevels,
			ware_fabricgatepassheader.intCompanyId as location,
			mst_locations.intCompanyId as company 
			FROM
			ware_fabricgatepassheader
			Inner Join mst_locations ON ware_fabricgatepassheader.intCompanyId = mst_locations.intId
			WHERE (intFabricGatePassNo='$serialNo') AND (`intFabricGatePassYear`='$year')";
			$results = $db->RunQuery($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			$company = $row['company'];
			$location = $row['location'];
			
			$approveLevel=$savedLevels+1-$status;
			$sqlI = "INSERT INTO `ware_fabricgatepassheader_approvedby` (`intFabricGatePassNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate) 
				VALUES ('$serialNo','$year','$approveLevel','$userId',now())";
			$resultI = $db->RunQuery($sqlI);

		//--------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
			$sql1 = "SELECT
				ware_fabricgatepassheader.intOrderNo,
				ware_fabricgatepassheader.intOrderYear,
				ware_fabricgatepassdetails.strCutNo,
				ware_fabricgatepassdetails.intSalesOrderId,
				ware_fabricgatepassdetails.intPart, 
				ware_fabricgatepassdetails.strSize,
				ware_fabricgatepassdetails.dblQty
				FROM
				ware_fabricgatepassdetails
				Inner Join ware_fabricgatepassheader ON ware_fabricgatepassdetails.intFabricGatePassNo = ware_fabricgatepassheader.intFabricGatePassNo AND ware_fabricgatepassdetails.intFabricGatePassYear = ware_fabricgatepassheader.intFabricGatePassYear
				WHERE
				ware_fabricgatepassdetails.intFabricGatePassNo =  '$serialNo' AND
				ware_fabricgatepassdetails.intFabricGatePassYear =  '$year'";
				$result1 = $db->RunQuery($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$orderNo=$row['intOrderNo'];
					$orderYear=$row['intOrderYear'];
					$cutNo=$row['strCutNo'];
					$part=$row['intPart'];
					$salesOrderId=$row['intSalesOrderId'];
					$size=$row['strSize'];
					$Qty=round($row['dblQty']);
					$place ='Stores';
					
					if($Qty>0){
					$type = 'GatePass';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','-$Qty','$type','$userId',now())";
					$resultI = $db->RunQuery($sql);
					}
				}
			}
		//--------------------------------------------------------------------
			if($status==$savedLevels){//if first confirmation raised,requisition table shld update
			}
		//---------------------------------------------------------------------------
		}
		
	}
	else if($_REQUEST['status']=='reject')
	{
		//$grnApproveLevel = (int)getApproveLevel($programName);
			$sql = "SELECT ware_fabricgatepassheader.intStatus, 
			ware_fabricgatepassheader.intApproveLevels,
			ware_fabricgatepassheader.intCompanyId as location,
			mst_locations.intCompanyId as company 
			FROM
			ware_fabricgatepassheader
			Inner Join mst_locations ON ware_fabricgatepassheader.intCompanyId = mst_locations.intId
			WHERE (intFabricGatePassNo='$serialNo') AND (`intFabricGatePassYear`='$year')";
			$results = $db->RunQuery($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			$company = $row['company'];
			$location = $row['location'];
		
		$sql = "UPDATE `ware_fabricgatepassheader` SET `intStatus`=0 WHERE (intFabricGatePassNo='$serialNo') AND (`intFabricGatePassYear`='$year')";
		$result = $db->RunQuery($sql);
		
		$sql = "DELETE FROM `ware_fabricgatepassheader_approvedby` WHERE (`intFabricGatePassNo`='$serialNo') AND (`intYear`='$year')";
		$result2 = $db->RunQuery($sql);
		
		$sqlI = "INSERT INTO `ware_fabricgatepassheader_approvedby` (`intFabricGatePassNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate) 
			VALUES ('$serialNo','$year','0','$userId',now())";
		$resultI = $db->RunQuery($sqlI);
		
		//---------------------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
			$sql1 = "SELECT
				ware_fabricgatepassheader.intOrderNo,
				ware_fabricgatepassheader.intOrderYear,
				ware_fabricgatepassdetails.intSalesOrderId,
				ware_fabricgatepassdetails.strSize,
				ware_fabricgatepassdetails.dblQty 
				FROM
				ware_fabricgatepassdetails
				Inner Join ware_fabricgatepassheader ON ware_fabricgatepassdetails.intFabricGatePassNo = ware_fabricgatepassheader.intFabricGatePassNo AND ware_fabricgatepassdetails.intFabricGatePassYear = ware_fabricgatepassheader.intFabricGatePassYear
				WHERE
				ware_fabricgatepassdetails.intFabricGatePassNo =  '$serialNo' AND
				ware_fabricgatepassdetails.intFabricGatePassYear =  '$year'";
				$result1 = $db->RunQuery($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$orderNo=$row['intOrderNo'];
					$orderYear=$row['intOrderYear'];
					$salesOrderId=$row['intSalesOrderId'];
					$size=$row['strSize'];
					$qty=round($row['dblQty']);
					$place ='Stores';
					$type = 'CGatePass';
					
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`intSalesOrderId`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','$location','$serialNo','$year','$orderNo','$orderYear','$salesOrderId','$size','','$qty','$type','$userId',now())";
					$resultI = $db->RunQuery($sqlI);
				}
			}
		//---------------------------------------------------------------------------------
			if($savedLevels>=$status){//if atleast one confirmation processed, roll back requisition table-
			}
		//-----------------------------------------------------------------------------
	}
//--------------------------------------------------------------
?>
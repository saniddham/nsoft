<?php
session_start();
$backwardseperator = "../../../../../";
$companyId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";

$serialNo = $_REQUEST['serialNo'];
$year = $_REQUEST['year'];
$approveMode = $_REQUEST['approveMode'];

$programName='Fabric Received Note';
$programCode='P0045';
//$issueApproveLevel = (int)getApproveLevel($programName);

/*$gatePassNo = '100000';
$year = '2012';
$approveMode=1;
*/
  $sql = "SELECT 
  ware_stocktransactions_fabric.intLocationId, 
ware_stocktransactions_fabric.dtDate, 
sys_users.strUserName 
FROM
ware_stocktransactions_fabric
Inner Join trn_orderdetails ON ware_stocktransactions_fabric.intOrderNo = trn_orderdetails.intOrderNo AND ware_stocktransactions_fabric.intOrderYear = trn_orderdetails.intOrderYear AND ware_stocktransactions_fabric.intSalesOrderId = trn_orderdetails.intSalesOrderId
Inner Join sys_users ON ware_stocktransactions_fabric.intUser = sys_users.intUserId
WHERE
ware_stocktransactions_fabric.intDocumentNo =  '$serialNo' AND
ware_stocktransactions_fabric.intDocumentYear =  '$year'  AND 
ware_stocktransactions_fabric.strType='TransferFrom'";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$locationId = $row['intLocationId'];//this locationId use in report header(reportHeader.php)--------------------
					$date = $row['dtDate'];
					$by = $row['strUserName'];
					$intStatus = $row['intStatus'];
					$savedLevels = $row['intApproveLevels'];
					$user = $row['strUserName'];
				 }
				 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Fabric Transfer Report</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="rptFabricReceivedNote-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:253px;
	top:171px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="../../../../../images/pending.png"  /></div>
<?php
}
?>
<form id="frmFabricReceived" name="frmFabricReceived" method="post" action="rptFabricReceivedNote.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>FABRIC TRANSFER  REPORT</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
    <?php
	if($intStatus>1)
	{
	
	//------------
	$k=$savedLevels+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
	
	 $resultp = $db->RunQuery($sqlp);
	 $rowp=mysqli_fetch_array($resultp);
	 $userPermission=0;
	 if($rowp['int'.$k.'Approval']==1){
		 $userPermission=1;
	 }
	//--------------	
	?>
    <?php if(($approveMode==1) and ($userPermission==1)) { ?>
    <img src="../../../../../images/approve.png" align="middle" class="noPrint mouseover" id="imgApprove" />
    <img src="../../../../../images/reject.png" align="middle" class="noPrint mouseover" id="imgReject" />
    <?php
	}
	}
	?></td>
  </tr>

  <tr>
    <td width="2%">&nbsp;</td>
    <td width="17%" class="normalfnt"><strong>Fabric Received no</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="58%"><span class="normalfnt"><?php echo $serialNo."/".$year;  ?></span></td>
    <td width="4%">&nbsp;</td>
    <td width="4%" align="center" valign="middle">&nbsp;</td>
    <td width="4%">&nbsp;</td>
    <td width="4%" class="normalfnt">&nbsp;</td>
    <td width="6%">&nbsp;</td>
  </tr>

  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Fabric Received By</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $by  ?></span></td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Date</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $date  ?></span></td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="gridHeader">
              <td width="27%" >Order No</td>
              <td width="29%" >Sales Order No</td>
              <td width="18%" >Size</td>
              <td width="11%" >Grade</td>
              <td width="15%" >Qty</td>
              </tr>
              <?php 
	  	    $sql1 = "SELECT 
				trn_orderdetails.strSalesOrderNo, 
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize,
				ware_stocktransactions_fabric.intGrade,
				ware_stocktransactions_fabric.dblQty as Qty,
				trn_ordersizeqty.dblQty AS orderQty, 
				ware_stocktransactions_fabric.intOrderNo, 
				ware_stocktransactions_fabric.intOrderYear 
				FROM
				ware_stocktransactions_fabric
				Inner Join trn_orderdetails ON ware_stocktransactions_fabric.intOrderNo = trn_orderdetails.intOrderNo AND ware_stocktransactions_fabric.intOrderYear = trn_orderdetails.intOrderYear AND ware_stocktransactions_fabric.intSalesOrderId = trn_orderdetails.intSalesOrderId
				Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.strSalesOrderNo = trn_ordersizeqty.strSalesOrderNo AND ware_stocktransactions_fabric.strSize = trn_ordersizeqty.strSize AND ware_stocktransactions_fabric.intGrade = trn_ordersizeqty.intGrade
				WHERE
				ware_stocktransactions_fabric.intDocumentNo =  '$serialNo' AND
				ware_stocktransactions_fabric.intDocumentYear =  '$year'  AND 
ware_stocktransactions_fabric.strType='TransferFrom'";
			$result1 = $db->RunQuery($sql1);
			$totQty=0;
			$totAmmount=0;
			while($row=mysqli_fetch_array($result1))
			{
					$orderNo=$row['intOrderNo'];
					$orderYear=$row['intOrderYear'];
					$salesOrderNo=$row['strSalesOrderNo'];
					$salesOrderId=$row['intSalesOrderId'];
					$size=$row['strSize'];
					$grade=$row['intGrade'];
					$orderQty=$row['orderQty'];
					$qty=$row['Qty']*(-1);
				
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" >&nbsp;<?php echo $orderNo."/".$orderYear;?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $salesOrderNo?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $size?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $grade ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $qty ?>&nbsp;</td>
              </tr>
      <?php 
			$totQty+=$qty;
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $totQty ?>&nbsp;</td>
              </tr>
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<?php  
$intStatus=1;
 	if($intStatus!=0)
	{

				for($i=1; $i<=$savedLevels; $i++)
				{
					   $sqlc = "SELECT
							ware_gatepassheader_approvedby.intApproveUser,
							ware_gatepassheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName, 
							ware_gatepassheader_approvedby.intApproveLevelNo
							FROM
							ware_gatepassheader_approvedby
							Inner Join sys_users ON ware_gatepassheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_gatepassheader_approvedby.intGatePassNo =  '$gatePassNo' AND
							ware_gatepassheader_approvedby.intYear =  '$year'  AND
							ware_gatepassheader_approvedby.intApproveLevelNo =  '$i'  order by intApproveLevelNo asc
";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
						if($i==1)
						$desc="1st ";
						else if($i==2)
						$desc="2nd ";
						else if($i==3)
						$desc="3rd ";
						else
						$desc=$i."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
				?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Appoved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
            </tr>
<?php
			}
	}
	else{
					 $sqlc = "SELECT
							ware_gatepassheader_approvedby.intApproveUser,
							ware_gatepassheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_gatepassheader_approvedby.intApproveLevelNo
							FROM
							ware_gatepassheader_approvedby
							Inner Join sys_users ON ware_gatepassheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_gatepassheader_approvedby.intGatePassNo =  '$gatePassNo' AND
							ware_gatepassheader_approvedby.intYear =  '$year'  AND
							ware_gatepassheader_approvedby.intApproveLevelNo =  '0'";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Rejected By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
	}
?>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
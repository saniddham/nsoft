			
$(document).ready(function() {
	
  		$("#frmTransferNote").validationEngine();
		$("#cboPONo").change(function(){
			loadOrderNo();
			clearGrid();
		});
		$("#cboCustomer").change(function(){
			loadPONoAndOrderNo();
			clearGrid();
		});
		$("#cboOrderYear").change(function(){
			loadPONoAndOrderNo();
			clearGrid();
		});
		$("#cboOrderNo").change(function(){
			submitForm();
		});
/*		$("#cboSalesOrderNo").change(function(){
			submitForm();
		});
*/	
  $('#frmTransferNote #butSave').click(function(){
	var requestType = '';
	if ($('#frmTransferNote').validationEngine('validate'))   
    { 

		var data = "requestType=save";
		
			data+="&serialNo="		+	$('#txtSerialNo').val();
			data+="&Year="	+	$('#txtYear').val();
			data+="&orderNo="	+	$('#cboOrderNo').val();
			data+="&orderYear="	+	$('#cboOrderYear').val();

			var rowCount = document.getElementById('tblMain').rows.length;
			if(rowCount==1){
				alert("No Items transfered");
				return false;				
			}
			var row = 0;
			
			var arr="[";
			
			for(var i=1;i<rowCount;i++)
			{
				
					var salesId  = 	document.getElementById('tblMain').rows[i].cells[0].id;
					var salesNo  = 	document.getElementById('tblMain').rows[i].cells[0].innerHTML;
					var size = 	document.getElementById('tblMain').rows[i].cells[1].id;
					var grade = 	document.getElementById('tblMain').rows[i].cells[2].id;
					var Qty = 	document.getElementById('tblMain').rows[i].cells[6].childNodes[0].value;
					
					if(Qty>0){
					    arr += "{";
						arr += '"salesId":"'+	salesId +'",' ;
						arr += '"salesNo":"'+	salesNo +'",' ;
						arr += '"size":"'+	size +'",' ;
						arr += '"grade":"'+	grade +'",' ;
						arr += '"Qty":"'+		Qty +'"' ;
						arr +=  '},';
						
					}
			}
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = "fabricTransferNote-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmTransferNote #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertx()",1000);
						$('#txtSerialNo').val(json.serialNo);
						$('#txtYear').val(json.year);
						document.location.href = document.location.href;
					}
				},
			error:function(xhr,status){
					
					$('#frmTransferNote #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
//-----------------------------------------------
//--------------------------------------------
//permision for view
if(intViewx)
{
//$('#frmInvoice #cboSearch').removeAttr('disabled');
}
  
//-----------------------------------
$('#butReport').click(function(){
	if($('#txtSerialNo').val()!=''){
		window.open('../listing/rptFabricTransferNote.php?serialNo='+$('#txtSerialNo').val()+'&year='+$('#txtYear').val());	
	}
	else{
		alert("There is no Fabric Received No to view");
	}
});

});//----------end of ready --------

//-------------------------------------
function alertx()
{
	$('#frmInvoice #butSave').validationEngine('hide')	;
}
//----------------------------------------------- 
function alertDelete()
{
	$('#frmInvoice #butDelete').validationEngine('hide')	;
}
//----------------------------------------------- 
function closePopUp(){
	
}
//-----------------------------------------------
function loadOrderNo(){
	    var orderYear = $('#cboOrderYear').val();
	    var poNo = $('#cboPONo').val();
	    var customer = $('#cboCustomer').val();
		var url 		= "fabricTransferNote-db-get.php?requestType=loadOrderNo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"poNo="+poNo+"&customer="+customer+"&orderYear="+orderYear,
			async:false,
			success:function(json){
				
					document.getElementById("cboOrderNo").innerHTML=json.orderNo;
					document.getElementById("cboCustomer").value=json.customer;
					if(poNo==''){
					document.getElementById("cboPONo").innerHTML=json.PoNo;
					}
			}
		});
}
//--------------------------------------------------
function loadCustomerPO(){
	    var orderNo = $('#cboOrderNo').val();
	    var orderYear = $('#cboOrderYear').val();
		var url 		= "fabricTransferNote-db-get.php?requestType=loadPoNo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"orderNo="+orderNo+"&orderYear="+orderYear,
			async:false,
			success:function(json){
				
					document.getElementById("cboPONo").value=json.poNo;
					document.getElementById("cboCustomer").value=json.customer;
					if(orderNo==''){
					document.getElementById("cboOrderNo").innerHTML=json.orderNo;
					}
					//document.getElementById("cboSalesOrderNo").innerHTML=json.salesOrderNo;
			}
		});
}
//-----------------------------------------------
function loadPONoAndOrderNo(){
	    var poNo = $('#cboPONo').val();
	    var orderNo = $('#cboOrderNo').val();
	    var orderYear = $('#cboOrderYear').val();
	    var customer = $('#cboCustomer').val();
		var url 		= "fabricTransferNote-db-get.php?requestType=loadPONoAndOrderNo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"customer="+customer+"&orderYear="+orderYear,
			async:false,
			success:function(json){
				
					document.getElementById("cboPONo").innerHTML=json.poNo;
					document.getElementById("cboOrderNo").innerHTML=json.orderNo;
			}
		});
}
//-----------------------------------------------
function loadQty(){
	    var orderNo = $('#cboOrderNo').val();
	    var salesOrderNo = '';
		var url 		= "fabricTransferNote-db-get.php?requestType=loadQty";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"orderNo="+orderNo+"&salesOrderNo="+salesOrderNo,
			async:false,
			success:function(json){
				
					document.getElementById("txtQty").value=json.qty;
			}
		});
}
//------------------------------------------------
function submitForm(){
	window.location.href = "fabricTransferNote.php?orderNo="+$('#cboOrderNo').val()
						+'&salesOrderNo='+$('#cboSalesOrderNo').val()
						+'&poNo='+$('#cboPONo').val()
						+'&customer='+$('#cboCustomer').val()
						+'&orderYear='+$('#cboOrderYear').val()
}
//----------------------------------------------- 
function clearGrid()
{
	var rowCount = document.getElementById('tblMain').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMain').deleteRow(1);
	}
	
	$('#txtQty').val('');
}
  //-------------------------------------------------------
 

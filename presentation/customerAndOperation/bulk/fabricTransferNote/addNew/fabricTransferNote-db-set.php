<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo 	 = $_REQUEST['serialNo'];
	$year 		 = $_REQUEST['Year'];
	$orderNo  = $_REQUEST['orderNo'];
	$orderYear 	 = $_REQUEST['orderYear'];
	
	$arr 		= json_decode($_REQUEST['arr'], true);
	
	$programName='Fabric Transfer Note';
	$programCode='P0046';

//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		if($serialNo==''){
			$serialNo 	= getNextSerialNo();
			$year = date('Y');
			$editMode=0;
		}
		else{
			$editMode=1;
		}
		//-----------delete and insert to detail table-----------------------
			$saved=0;
			$toSave=0;
			$rollBackMsg="Maximum Qty for followings are...";
			foreach($arr as $arrVal)
			{
				$salesOrderNo = $arrVal['salesNo'];
				$salesOrderId = $arrVal['salesId'];
				$size 	 = $arrVal['size'];
				$grade 	 = $arrVal['grade'];
				$Qty 		 = $arrVal['Qty'];
				$placeF = 'Stores';
				$typeF 	 = 'TransferFrom';
				$placeT = 'Production';
				$typeT 	 = 'TransferTo';
		
				$storesQty=loadStoresQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade);
				$transferedQty=loadTransferedQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade);
				$receivedQty=loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade);
				$orderQty=loadOrderQty($location,$orderNo,$orderYear,$salesOrderNo,$size,$grade);
				$maxQty=$storesQty;

				//------check maximum FR Qty--------------------
				if($Qty>$maxQty){
					$rollBackFlag=1;
					$rollBackMsg .="<br> ".$salesOrderNo."-".$size." =".$maxQty;
				}

				//----------------------------
				if($rollBackFlag!=1){
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`intSalesOrderId`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$placeF','$location','$serialNo','$year','$orderNo','$orderYear','$salesOrderId','$size','$grade','-$Qty','$typeF','$userId',now())";
					$result = $db->RunQuery2($sql);
					if($result==1){
					$saved++;
					}
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`intSalesOrderId`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$placeT','$location','$serialNo','$year','$orderNo','$orderYear','$salesOrderId','$size','$grade','$Qty','$typeT','$userId',now())";
					$result = $db->RunQuery2($sql);
					if($result==1){
					$saved++;
					}
				}
				$toSave++;
		}
		//echo $rollBackFlag;
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved/2)){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
//----------------------------------------
	echo json_encode($response);
//----------------------------------------




//----------------------------------------
	function getNextSerialNo()
	{
		global $db;
		global $company;
		$sql = "SELECT
				sys_no.intFabricTransferdNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$company'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextNo = $row['intFabricTransferdNo'];
		
		$sql = "UPDATE `sys_no` SET intFabricTransferdNo=intFabricTransferdNo+1 WHERE (`intCompanyId`='$company')  ";
		$db->RunQuery2($sql);
		
		return $nextNo;
	}
	
	//--------------------------------------------------------------
function loadStoresQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade)
{
	
	$str="Received','FDAMAGE','CUTRETURN','TransferFrom','ReturnTo','Gatepass','GPTIN";
		global $db;
		 $sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				ware_stocktransactions_fabric.intLocationId =  '$location' AND
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strPlace =  'Stores' AND 
				ware_stocktransactions_fabric.strType IN ('$str') and intGrade='$grade'
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize,intGrade";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
		
}
	//--------------------------------------------------------------
function loadTransferedQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade)
{
		global $db;
		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				ware_stocktransactions_fabric.intLocationId =  '$location' AND
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType =  'TransferFrom' and intGrade='$grade'
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize,intGrade";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
		
}
	//--------------------------------------------------------------
function loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade)
{
		global $db;
		   $sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				ware_stocktransactions_fabric.intLocationId =  '$location' AND
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType =  'Received' and intGrade='$grade' 
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize ,intGrade";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//-----------------------------------------------------------
function loadOrderQty($location,$orderNo,$orderYear,$salesOrderNo,$size,$grade)
{
		global $db;
		$sql = "SELECT
				trn_ordersizeqty.dblQty
				FROM trn_ordersizeqty
				WHERE
				trn_ordersizeqty.intOrderNo =  '$orderNo' AND
				trn_ordersizeqty.intOrderYear =  '$orderYear' AND
				trn_ordersizeqty.strSalesOrderNo =  '$salesOrderNo' AND
				trn_ordersizeqty.strSize =  '$size' and intGrade='$grade'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//-----------------------------------------------------------
?>
<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];
$intUser  = $_SESSION["userId"];

include  	"{$backwardseperator}dataAccess/permisionCheck.inc";
//include  	"{$backwardseperator}dataAccess/Connector.php";

$programName='Fabric Transfer Note';
$programCode='P0046';

$serialNo = $_REQUEST['serialNo'];
$year = $_REQUEST['year'];
if($serialNo==''){
$orderNo = $_REQUEST['orderNo'];
$orderYear = $_REQUEST['orderYear'];
}
else{
$sql = "SELECT
ware_stocktransactions_fabric.intOrderNo,
ware_stocktransactions_fabric.intOrderYear
FROM ware_stocktransactions_fabric
WHERE
ware_stocktransactions_fabric.intDocumentNo =  '$serialNo' AND
ware_stocktransactions_fabric.intDocumentYear =  '$year'";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$orderNo 	= $row['intOrderNo'];
$orderYear 	= $row['intOrderYear'];
}
//$salesOrderNo = $_REQUEST['salesOrderNo'];
$poNo = $_REQUEST['poNo'];
$customer = $_REQUEST['customer'];
//----------------
   $sql = "SELECT
trn_orderheader.strCustomerPoNo,
trn_orderheader.intCustomer
FROM trn_orderheader
WHERE
trn_orderheader.intOrderNo =  '$orderNo' AND
trn_orderheader.intOrderYear =  '$orderYear'"; 
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);

$x_poNo = $row['strCustomerPoNo'];
$x_custId = $row['intCustomer'];
if($x_poNo=='')
$x_poNo=$poNo;
//----------------			
$sql = "SELECT
Sum(trn_orderdetails.intQty) AS qty, 
trn_orderdetails.intSampleNo,
trn_orderdetails.intSampleYear,
trn_orderdetails.strCombo,
trn_orderdetails.strPrintName,
trn_orderdetails.intRevisionNo 
FROM trn_orderdetails 
WHERE
trn_orderdetails.intOrderNo='$orderNo' 
AND trn_orderdetails.intOrderYear='$orderYear'  
"; 
/*if($salesOrderNo!=''){
$sql .= " AND 
trn_orderdetails.strSalesOrderNo='$salesOrderNo'"; 
}
*/$sql .= " GROUP BY
trn_orderdetails.intOrderNo,
trn_orderdetails.intOrderYear"; 
// echo $sql;

$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$x_sampleNo 	= $row['intSampleNo'];
$x_sampleYear 	= $row['intSampleYear'];
$x_revNo 		= $row['intRevisionNo'];
$x_combo 		= $row['strCombo'];
$x_print 		= $row['strPrintName'];
$x_qty = $row['qty'];
//-------------------------			
			
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Fabric Transfer Note</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="fabricTransferNote-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../../libraries/calendar/theme.css" />
<script src="../../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body>
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>


<form id="frmTransferNote" name="frmTransferNote" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL" style="width:800">
		  <div class="trans_text">FABRIC TRANSFER NOTE</div>
		  <table width="700" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
    <tr><td><table class="tableBorder_allRound" width="100%">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td width="8%" height="22" class="normalfnt">Serial No</td>
  <td width="21%" class="normalfnt"><input  id="txtSerialNo" class="normalfnt" style="width:70px;text-align:right" type="text" value="<?php echo $serialNo ?>" readonly="readonly"/><input  id="txtYear" class="normalfnt" style="width:40px;text-align:right" type="text" value="<?php echo $year ?>" readonly="readonly"/></td>
  <td width="1%" class="normalfnt">&nbsp;</td>
  <td width="18%" class="normalfnt">&nbsp;</td>
  <td width="8%" class="normalfnt">&nbsp;</td>
  <td width="21%" class="normalfnt">&nbsp;</td>
  <td width="1%" class="normalfnt">&nbsp;</td>
  <td width="22%" align="right" class="normalfnt">&nbsp;</td>
</tr>        </table></td>
      </tr>
    </table></td></tr>
    <tr><td><table width="100%">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td width="5%" height="22" class="normalfnt">Year</td>
  <td width="14%" class="normalfnt"><select name="cboOrderYear" id="cboOrderYear" style="width:70px">
    <?php
					$sql = "SELECT DISTINCT
							trn_orderheader.intOrderYear
							FROM trn_orderheader
							ORDER BY
							trn_orderheader.intOrderYear DESC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intOrderYear']==$x_year)
						echo "<option value=\"".$row['intOrderYear']."\" selected=\"selected\">".$row['intOrderYear']."</option>";	
						else
						echo "<option value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";	
					}
				?>
  </select></td>
  <td width="11%" class="normalfnt">Costomer PO</td>
  <td width="18%" class="normalfnt"><select name="cboPONo" id="cboPONo" style="width:103px">
    <option value=""></option>
    <?php
					$sql = "SELECT DISTINCT
							trn_orderheader.strCustomerPoNo
							FROM trn_orderheader 
							ORDER BY
							trn_orderheader.strCustomerPoNo ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['strCustomerPoNo']==$x_poNo)
						echo "<option value=\"".$row['strCustomerPoNo']."\" selected=\"selected\">".$row['strCustomerPoNo']."</option>";	
						else
						echo "<option value=\"".$row['strCustomerPoNo']."\">".$row['strCustomerPoNo']."</option>";	
					}
				?>
  </select></td>
  <td width="8%" class="normalfnt">Order No</td>
  <td width="21%" class="normalfnt"><select name="cboOrderNo" id="cboOrderNo" style="width:103px">
    <option value=""></option>
    <?php
					$sql = "SELECT DISTINCT
							trn_orderheader.intOrderNo, 
							trn_orderheader.intOrderYear 
							FROM trn_orderheader ORDER BY
							 trn_orderheader.intOrderNo DESC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intOrderNo']==$orderNo)
						echo "<option value=\"".$row['intOrderNo']."\" selected=\"selected\">".$row['intOrderNo']."</option>";	
						else
						echo "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";	
					}
				?>
  </select></td>
  <td width="1%" class="normalfnt">&nbsp;</td>
  <td width="22%" align="right" class="normalfnt">&nbsp;</td>
</tr>        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="5%" class="normalfnt">Qty</td>
            <td width="13%" align="left" class="normalfnt"><input  id="txtQty" class="validate[required,custom[number]] invoQty normalfnt" style="width:70px;text-align:right" type="text" value="<?php echo $x_qty ?>" readonly="readonly"/></td>
            <td width="44%">&nbsp;</td>
            <td width="11%" height="27" rowspan="2" class="normalfnt">Customer</td>
            <td width="27%" rowspan="2"><span class="normalfnt">
              <select style="width:191px" name="cboCustomer" id="cboCustomer"   class="validate[required]">
                <option value=""></option>
                <?php
					$sql = "SELECT DISTINCT 
							trn_orderheader.intCustomer,
							mst_customer.strName
							FROM
							trn_orderheader
							Inner Join mst_customer ON trn_orderheader.intCustomer = mst_customer.intId";
							$pp=$sql;
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intCustomer']==$x_custId)
						echo "<option value=\"".$row['intCustomer']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intCustomer']."\">".$row['strName']."</option>";	
					}
				?>
              </select>
            </span></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="92%">&nbsp;</td>
            <td width="8%"></td>
          </tr>
        </table></td>
      </tr>
    </table></td></tr>
      <tr>
        <td><div style="width:720px;height:300px;overflow:scroll" >
          <table width="700" class="grid" id="tblMain" >
            <tr class="gridHeader">
              <td width="25%" >Sales Order #</td>
              <td width="13%" >Size</td>
              <td width="12%" >Cut No</td>
              <td width="11%" >Order Qty</td>
              <td width="13%">Stores Bal Qty</td>
              <td width="14%">Transfered Qty</td>
              <td width="12%">Qty</td>
              </tr>
              <?php
				$totAmm=0;
				$result=loadDetails($serialNo,$year,$orderNo,$orderYear);
				while($row=mysqli_fetch_array($result))
				{
					$salesOrderNo=$row['strSalesOrderNo'];
					$salesOrderId=$row['intSalesOrderId'];
					$size=$row['strSize'];
					$grade=$row['intGrade'];
					$orderQty=$row['orderQty'];
					$receivedQty=loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade);
					$storesQty=loadStoresQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade);
					$transferedQty=loadTransferedQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade)*(-1);
					$qty=$row['Qty']*(-1);
					$maxQty=$storesQty;
					if($serialNo!=''){
					$maxQty=$storesQty;
					}
					if(($storesQty>0) ||($qty>0)){
			  ?>
<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" id="<?php echo $salesOrderId; ?>"><?php echo $salesOrderNo; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $size; ?>" ><?php echo $size; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $grade; ?>" class="item"><?php echo $grade; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $orderQty; ?>"><?php echo $orderQty; ?></td>
			<td align="right" bgcolor="#FFFFFF" id="<?php echo $receivedQty; ?>"  class="balToInvQty"><?php echo $storesQty; ?></td>
			<td align="right" bgcolor="#FFFFFF" id="<?php echo $transferedQty; ?>"  class="balToInvQty"><?php echo $transferedQty; ?></td>
        	<td align="right" bgcolor="#FFFFFF" id="<?php echo $maxQty; ?>"  class="balToInvQty"><input  id="txtQty" class="validate[required,custom[number],max[<?php echo $maxQty;?>]] invoQty normalfnt" style="width:70px;text-align:right" type="text" value="<?php echo $qty; ?>" /></td>
            </tr>       
             <?php
					}
				}
			  ?>  
         </table>
        </div></td>
</tr>
<?php
if($serialNo=='')
$editMode=1;
?>
<tr>
        <td align="center" class="tableBorder_allRound"><img src="../../../../../images/Tnew.jpg" width="92" height="24" id="butNew" name="butNew"  class="mouseover"/>
          <?php if($editMode==1){ ?><img src="../../../../../images/Tsave.jpg" width="92" height="24" id="butSave" name="butSave"  class="mouseover"/><?php } ?>          <img src="../../../../../images/Treport.jpg" width="92" height="24" id="butReport" name="butReport"  class="mouseover"/><img src="../../../../../planning/img/Tclose.jpg" width="92" height="24"  class="mouseover"/></td>
</tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>


</body>
</html>
<?php

	//--------------------------------------------------------------
function loadStoresQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade)
{
	
	$str="Received','FDAMAGE','CUTRETURN','TransferFrom','ReturnTo','Gatepass','GPTIN";
		global $db;
		 $sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				ware_stocktransactions_fabric.intLocationId =  '$location' AND
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND ware_stocktransactions_fabric.intGrade='$grade' AND
				ware_stocktransactions_fabric.strPlace =  'Stores' AND 
				ware_stocktransactions_fabric.strType IN ('$str') 
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
		
}
	//--------------------------------------------------------------
function loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade)
{
		global $db;
		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				ware_stocktransactions_fabric.intLocationId =  '$location' AND
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType =  'Received' AND
				ware_stocktransactions_fabric.intGrade = '$grade'
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
		
}
	//--------------------------------------------------------------
function loadTransferedQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade)
{
		global $db;
		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				ware_stocktransactions_fabric.intLocationId =  '$location' AND
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND intGrade='$grade' AND
				ware_stocktransactions_fabric.strType =  'TransferFrom'
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize , intGrade";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
		
}
	//--------------------------------------------------------------
	
function loadDetails($serialNo,$year,$orderNo,$orderYear){
		global $db; 
		if($serialNo==''){
		  $sql = "SELECT
				trn_orderdetails.strSalesOrderNo,
				trn_orderdetails.intSalesOrderId,
				trn_ordersizeqty.strSize,
				trn_ordersizeqty.intGrade,
				trn_ordersizeqty.dblQty as orderQty, 
				'0'  as Qty
				FROM
				trn_orderdetails
				Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.strSalesOrderNo = trn_ordersizeqty.strSalesOrderNo
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND 
				trn_ordersizeqty.dblQty >0 
				ORDER BY
				trn_orderdetails.strSalesOrderNo ASC,
				trn_ordersizeqty.strSize ASC 
				";
		}
		else{
		$sql = "SELECT 
				trn_orderdetails.strSalesOrderNo, 
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize,
				ware_stocktransactions_fabric.intGrade,
				ware_stocktransactions_fabric.dblQty as Qty,
				trn_ordersizeqty.dblQty AS orderQty
				FROM
				ware_stocktransactions_fabric
				Inner Join trn_orderdetails ON ware_stocktransactions_fabric.intOrderNo = trn_orderdetails.intOrderNo AND ware_stocktransactions_fabric.intOrderYear = trn_orderdetails.intOrderYear AND ware_stocktransactions_fabric.intSalesOrderId = trn_orderdetails.intSalesOrderId
				Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.strSalesOrderNo = trn_ordersizeqty.strSalesOrderNo AND ware_stocktransactions_fabric.strSize = trn_ordersizeqty.strSize AND ware_stocktransactions_fabric.intGrade = trn_ordersizeqty.intGrade
				WHERE
				ware_stocktransactions_fabric.intDocumentNo =  '$serialNo' AND
				ware_stocktransactions_fabric.intDocumentYear =  '$year'  
				AND ware_stocktransactions_fabric.strType='TransferFrom'";

		}

			return $result = $db->RunQuery($sql);
	}
?>

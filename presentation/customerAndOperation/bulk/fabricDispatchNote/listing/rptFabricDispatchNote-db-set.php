<?php 
//////////////////////////////////////////////
//Create By:H.B.G Korala
//note://if final confirmation raised,insert into stocktransaction table.
/////////////////////////////////////////////
ini_set('display_errors',0);
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];

	include "{$backwardseperator}dataAccess/Connector.php";
	include "{$backwardseperator}libraries/mail/mail.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_mail.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_permisions.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
	require_once $_SESSION['ROOT_PATH']."class/customerAndOperation/bulk/cls_bulk_get.php";
	require_once $_SESSION['ROOT_PATH']."class/common/azureDBconnection.php";

	$objMail 			= new cls_create_mail($db);
	$obj_permision		= new cls_permisions($db);
	$obj_commom			= new cls_commonFunctions_get($db);
	$obj_bulk_get 		= new Cls_bulk_get($db);
	$objAzure = new cls_azureDBconnection();
	
	$programName='Fabric Received Note';
	$programCode='P0045';
	$fabRcvApproveLevel = (int)getApproveLevel($programName);
	
	/////////// parameters /////////////////////////////

	
	$serialNo = $_REQUEST['serialNo'];
	$year = $_REQUEST['year'];
	
	if($_REQUEST['status']=='approve')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$sqlChk = "SELECT 
		ware_fabricdispatchheader.intOrderNo,
		ware_fabricdispatchheader.intStatus,
		ware_fabricdispatchheader.intApproveLevels,
		ware_fabricdispatchheader.intOrderYear, 
		ware_fabricdispatchheader.intCompanyId AS location, 
		ware_fabricdispatchheader.intCustLocation, 
		mst_locations.intCompanyId AS company,
		trn_orderheader.PO_TYPE 
		FROM 
		ware_fabricdispatchheader 
		Inner Join mst_locations ON ware_fabricdispatchheader.intCompanyId = mst_locations.intId
		INNER JOIN trn_orderheader ON ware_fabricdispatchheader.intOrderNo = trn_orderheader.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderheader.intOrderYear
		WHERE (intBulkDispatchNo='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
		$resultChk = $db->RunQuery2($sqlChk);
		$rowChk = mysqli_fetch_array($resultChk);
		$statusChk = $rowChk['intStatus'];
		$status=$rowChk['intStatus'];
		$levels=$rowChk['intApproveLevels'];
		$orderNo=$rowChk['intOrderNo'];
		$orderYear=$rowChk['intOrderYear'];
		$customer_location=$rowChk['intCustLocation'];
		$po_type =$rowChk['PO_TYPE'];
		
		//-------------------------------------------
		$lc_flag		= $obj_bulk_get->get_order_lc_flag($orderNo,$orderYear,'RunQuery2');
		if($lc_flag	== 6) 
			$lc_flag = 1;
		else
			$lc_flag = 0;
		$lc_status		= $obj_bulk_get->get_lc_status($orderNo,$orderYear,'RunQuery2');
		//-------------------------------------------
	
		//--------------------------
		$customer_email_flag	=	get_customer_email_flag($orderNo,$orderYear);
		//--------------------------
		$customer_location_email_flag	=	get_customer_location_email_flag($orderNo,$orderYear,$customer_location);
		//--------------------------
		
		if($statusChk==1){
			$rollBackFlag=1;
			$rollBackMsg ="Final confirmation of this Dispatch No is already raised";
		}
		/*else if(($customer_email_flag==0) ){// 
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Please enter customer's email address."; 
			$db->CloseConnection();	
			echo json_encode($response);
			return;	
		}*/else if(($customer_location_email_flag==0) ){// 
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Please enter customer location email address."; 
			$db->CloseConnection();	
			echo json_encode($response);
			return;	
		}else if($lc_flag==1 && $lc_status !=1){
			 $rollBackFlag=1;
			 $rollBackMsg="No approved LC.So can't dispatch fabrics.";
		 }
		 
		 //------------2017-05-15----------------------------------------
		$pm_details	=	get_plant_manager_details($serialNo,$year,$userId);
		$ch_details	=	get_cluster_head_details($serialNo,$year,$userId);
		if(($status==2 && $levels==2 && $pm_details['flag']!=1) || ($status==3 && $levels==3 && $pm_details['flag']!=1)){
			 $rollBackFlag=1;
			 $rollBackMsg="Only ".$pm_details['name']." can approve this.";
		 }
		 else if($status==2 && $levels==3 && $ch_details['flag']!=1){
			 $rollBackFlag=1;
			 $rollBackMsg="Only ".$ch_details['name']." can approve this.";
		 }
		 //-------------------------------------------------------------
		
		if($statusChk!=1){
			 
			 
		 $sql = "UPDATE `ware_fabricdispatchheader` SET `intStatus`=intStatus-1 WHERE (intBulkDispatchNo='$serialNo') AND (`intBulkDispatchNoYear`='$year') and intStatus not in(0,1)";
		 $result = $db->RunQuery2($sql);
		
		if($result){
			$sql = "SELECT ware_fabricdispatchheader.intStatus, 
			ware_fabricdispatchheader.intApproveLevels,  
			ware_fabricdispatchheader.intCompanyId AS location, 
			mst_locations.intCompanyId AS company 
			FROM 
			ware_fabricdispatchheader 
			Inner Join mst_locations ON ware_fabricdispatchheader.intCompanyId = mst_locations.intId
			WHERE (intBulkDispatchNo='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			$company = $row['company'];
			$location = $row['location'];
			
			$approveLevel=$savedLevels+1-$status;
			$sqlI = "INSERT INTO `ware_fabricdispatchheader_approvedby` (`intBulkDispatchNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate) 
				VALUES ('$serialNo','$year','$approveLevel','$userId',now())";
			$resultI = $db->RunQuery2($sqlI);

		//--------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
			$sql1 = "SELECT
				ware_fabricdispatchheader.intOrderNo,
				ware_fabricdispatchheader.intOrderYear,
				ware_fabricdispatchdetails.strCutNo,
				ware_fabricdispatchdetails.intSalesOrderId,
				ware_fabricdispatchdetails.intPart, 
				ware_fabricdispatchdetails.strSize,
				ware_fabricdispatchdetails.dblSampleQty,
				ware_fabricdispatchdetails.dblGoodQty,
				ware_fabricdispatchdetails.dblEmbroideryQty,
				ware_fabricdispatchdetails.dblPDammageQty,
				ware_fabricdispatchdetails.dblFDammageQty,
				ware_fabricdispatchdetails.dblCutRetQty 
				FROM
				ware_fabricdispatchdetails
				Inner Join ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
				WHERE
				ware_fabricdispatchdetails.intBulkDispatchNo =  '$serialNo' AND
				ware_fabricdispatchdetails.intBulkDispatchNoYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				$toSave =0;
				$savedRcds	=0;
				while($row=mysqli_fetch_array($result1))
				{
					$orderNo=$row['intOrderNo'];
					$orderYear=$row['intOrderYear'];
					$cutNo=$row['strCutNo'];
					$part=$row['intPart'];
					$salesOrderId=$row['intSalesOrderId'];
					$size=$row['strSize'];
					$sampleQty=round($row['dblSampleQty'],4);
					$goodQty=round($row['dblGoodQty'],4);
					$embroideryQty=round($row['dblEmbroideryQty'],4);
					$pDammageQty=round($row['dblPDammageQty'],4);
					$fDammageQty=round($row['dblFDammageQty'],4);
					$cutRetQty=round($row['dblCutRetQty'],4);
					$place ='Stores';
					
					$soStatus		=loadSalesOrderStatus($orderNo,$orderYear,$salesOrderId);
					if($soStatus== -10){
						$rollBackFlag=1;
						$rollBackMsg ="Sales order is already closed.";
					}
				
					if($sampleQty>0){
					$type = 'Dispatched_S';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','-$sampleQty','$type','$userId',now())";
					$resultI = $db->RunQuery2($sql);
					$toSave++;
					$savedRcds	+=$resultI;
					$rollBackSql =$sql;
					}
					if($goodQty>0){
					$type = 'Dispatched_G';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','-$goodQty','$type','$userId',now())";
					$resultI = $db->RunQuery2($sql);
					$toSave++;
					$savedRcds	+=$resultI;
					$rollBackSql =$sql;
					}
					if($embroideryQty>0){
					$type = 'Dispatched_E';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','-$embroideryQty','$type','$userId',now())";
					$resultI = $db->RunQuery2($sql);
					$toSave++;
					$savedRcds	+=$resultI;
					$rollBackSql =$sql;
					}
					if($pDammageQty>0){
					$type = 'Dispatched_P';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','-$pDammageQty','$type','$userId',now())";
					$resultI = $db->RunQuery2($sql);
					$toSave++;
					$savedRcds	+=$resultI;
					$rollBackSql =$sql;
					}
					if($fDammageQty>0){
					$type = 'Dispatched_F';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','-$fDammageQty','$type','$userId',now())";
					$resultI = $db->RunQuery2($sql);
					$toSave++;
					$savedRcds	+=$resultI;
					$rollBackSql =$sql;
					}
					if($cutRetQty>0){
					$type = 'Dispatched_CUT_RET';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','-$cutRetQty','$type','$userId',now())";
					$resultI = $db->RunQuery2($sql);
					$toSave++;
					$savedRcds	+=$resultI;
					$rollBackSql =$sql;
					}
				//BEGIN - ADD TO SALES ORDER INVOICE BALANCE TABLE {
					AddToBalanceTable($orderYear,$orderNo,$salesOrderId,$goodQty);	
				//END	- ADD TO SALES ORDER INVOICE BALANCE TABLE }
				
				}
			}
		//--------------------------------------------------------------------
			if($status==$savedLevels){//if first confirmation raised,requisition table shld update
			}
		//---------------------------------------------------------------------------
		}
	  }// end if statusChk!=1
	  
	if(($status==2 && $levels==2) || ($status==3 && $levels==3)) //exceed size wise only or exceed size wise and so wise
		sendToExtraApproval($serialNo,$year,$mainPath,$root_path,'plant_manager');
	if($status==2 && $levels==3) //exceed size wise and so wise
		sendToExtraApproval($serialNo,$year,$mainPath,$root_path,'cluster_head');

	$savedRcds	=round($savedRcds,2);
	$toSave		=round($toSave,2);
	
	if(($savedRcds==0) && ($status==1)){
		$rollBackFlag=1;
		$rollBackMsg ="Data Saving Error";
	}
	else if(($savedRcds!=$toSave) && ($status==1)){
		$rollBackFlag=1;
		$rollBackMsg ="Data Saving Error";
	}
	//---------------------------
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sql;
			
		}
		else if(($toSave==$savedRcds)){
		    if($status ==1 && $_SESSION['headCompanyId'] == 1 && $po_type != 1){
                updateAzureTable($serialNo,$year,$db,"SO_Shipment");
            }
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Approved successfully.';
			
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
 //----------------------------------------
	echo json_encode($response);
//----------------------------------------
		
		
		
	}
	else if($_REQUEST['status']=='reject')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		//$grnApproveLevel = (int)getApproveLevel($programName);
		$sql = "SELECT ware_fabricdispatchheader.intStatus,ware_fabricdispatchheader.intApproveLevels FROM ware_gatepassheader WHERE  (intBulkDispatchNo='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
		$results = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($results);
		$status = $row['intStatus'];
		$savedLevels = $row['intApproveLevels'];
		
		$sql = "UPDATE `ware_fabricdispatchheader` SET `intStatus`=0 WHERE (intBulkDispatchNo='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
		$result = $db->RunQuery2($sql);
		
		$sql = "SELECT max(ware_fabricdispatchheader_approvedby.intStatus) as intStatus FROM ware_fabricdispatchheader_approvedby WHERE  (intBulkDispatchNo='$serialNo') AND (`intYear`='$year')";
		$results = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($results);
		$maxStatus = $row['intStatus']+1;
		
		$sql = "UPDATE `ware_fabricdispatchheader_approvedby` 
		SET intStatus ='$maxStatus' 
		WHERE (`intBulkDispatchNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
		$result2 = $db->RunQuery2($sql);
		
		$sqlI = "INSERT INTO `ware_fabricdispatchheader_approvedby` (`intBulkDispatchNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate) 
			VALUES ('$serialNo','$year','0','$userId',now())";
		$resultI = $db->RunQuery2($sqlI);
		
		//---------------------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
			$sql1 = "SELECT
				ware_fabricdispatchheader.intOrderNo,
				ware_fabricdispatchheader.intOrderYear,
				ware_fabricdispatchdetails.strCutNo,
				ware_fabricdispatchdetails.intSalesOrderId,
				ware_fabricdispatchdetails.intPart, 
				ware_fabricdispatchdetails.strSize,
				ware_fabricdispatchdetails.dblSampleQty,
				ware_fabricdispatchdetails.dblGoodQty,
				ware_fabricdispatchdetails.dblEmbroideryQty,
				ware_fabricdispatchdetails.dblPDammageQty,
				ware_fabricdispatchdetails.dblFDammageQty,
				ware_fabricdispatchdetails.dblCutRetQty 
				FROM
				ware_fabricdispatchdetails
				Inner Join ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
				WHERE
				ware_fabricdispatchdetails.intBulkDispatchNo =  '$serialNo' AND
				ware_fabricdispatchdetails.intBulkDispatchNoYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$orderNo=$row['intOrderNo'];
					$orderYear=$row['intOrderYear'];
					$cutNo=$row['strCutNo'];
					$part=$row['intPart'];
					$salesOrderId=$row['intSalesOrderId'];
					$size=$row['strSize'];
					$sampleQty=round($row['dblSampleQty'],4);
					$goodQty=round($row['dblGoodQty'],4);
					$embroideryQty=round($row['dblEmbroideryQty'],4);
					$pDammageQty=round($row['dblPDammageQty'],4);
					$fDammageQty=round($row['dblFDammageQty'],4);
					$cutRetQty=round($row['dblCutRetQty'],4);
					$place ='Stores';
					
					if($sampleQty>0){
					$type = 'Dispatched_S';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','$sampleQty','$type','$userId',now())";
					$resultI = $db->RunQuery2($sql);
					}
					if($goodQty>0){
					$type = 'Dispatched_G';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','$goodQty','$type','$userId',now())";
					$resultI = $db->RunQuery2($sql);
					}
					if($embroideryQty>0){
					$type = 'Dispatched_E';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','$embroideryQty','$type','$userId',now())";
					$resultI = $db->RunQuery2($sql);
					}
					if($pDammageQty>0){
					$type = 'Dispatched_P';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','$pDammageQty','$type','$userId',now())";
					$resultI = $db->RunQuery2($sql);
					}
					if($fDammageQty>0){
					$type = 'Dispatched_F';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','$fDammageQty','$type','$userId',now())";
					$resultI = $db->RunQuery2($sql);
					}
				}
			}
		//---------------------------------------------------------------------------------
			if($savedLevels>=$status){//if atleast one confirmation processed, roll back requisition table-
			}
		//-----------------------------------------------------------------------------
			$db->RunQuery2('Commit');
			$db->CloseConnection();		
 //----------------------------------------
	echo json_encode($response);
//----------------------------------------
	}
//--------------------------------------------------------------


function AddToBalanceTable($orderYear,$orderNo,$salesOrderId,$goodQty)
{
	global $db;
	$booAvailable	= false;
	$sql = "SELECT COUNT(*) AS COUNT FROM finance_customer_invoice_balance 
			WHERE ORDER_YEAR = '$orderYear' 
			AND ORDER_NO = '$orderNo' 
			AND SALES_ORDER_ID = '$salesOrderId'";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	
	if($row["COUNT"]>0)
		$booAvailable	= true;
	
	if(!$booAvailable)
	{
		$sql = "INSERT INTO finance_customer_invoice_balance
					(ORDER_YEAR,
					ORDER_NO,
					SALES_ORDER_ID,
					DISPATCHED_GOOD_QTY,
					INVOICE_BALANCE)
				VALUES ('$orderYear',
					'$orderNo',
					'$salesOrderId',
					'$goodQty',
					'$goodQty');";
		$result = $db->RunQuery2($sql);
	}
	else
	{
		$sql = "UPDATE finance_customer_invoice_balance
				SET DISPATCHED_GOOD_QTY = DISPATCHED_GOOD_QTY + $goodQty ,
				  INVOICE_BALANCE = INVOICE_BALANCE + $goodQty
				WHERE ORDER_YEAR = '$orderYear'
					AND ORDER_NO = '$orderNo'
					AND SALES_ORDER_ID = '$salesOrderId';";
		$result = $db->RunQuery2($sql);
	}
}


//--------------------------------------------------------
function get_customer_email_flag($orderNo,$orderYear){
	
		global $db;
		  $sql = "SELECT
					mst_customer.strEmail
					FROM
					trn_orderheader
					INNER JOIN mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
					WHERE
					trn_orderheader.intOrderNo = '$orderNo' AND
					trn_orderheader.intOrderYear = '$orderYear'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		if($rows['strEmail']=='')
			return 0;
		else
			return 1;
	
}
//---------------------------------------------------------
function get_customer_location_email_flag($orderNo,$orderYear,$customer_location){
	
		global $db;
		/*$sql = "	SELECT
						mst_customer_locations.strEmailAddress
					FROM
						trn_orderheader
						INNER JOIN mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
						INNER JOIN mst_customer_locations ON mst_customer_locations.intCustomerId = trn_orderheader.intCustomer AND mst_customer_locations.intLocationId = trn_orderheader.intCustomerLocation
					WHERE
						trn_orderheader.intOrderNo 		= '$orderNo' AND
						trn_orderheader.intOrderYear 	= '$orderYear' AND
						trn_orderheader.intCustomerLocation 	= '$customer_location'";*/
						
		$sql = "	SELECT
						mst_customer_locations.strEmailAddress
					FROM
						trn_orderheader
						INNER JOIN mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
						INNER JOIN mst_customer_locations ON mst_customer_locations.intCustomerId = trn_orderheader.intCustomer AND mst_customer_locations.intLocationId = '$customer_location'
					WHERE
						trn_orderheader.intOrderNo 		= '$orderNo' AND
						trn_orderheader.intOrderYear 	= '$orderYear' AND
						mst_customer_locations.intLocationId 	= '$customer_location'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		if($rows['strEmailAddress']=='')
			return 0;
		else
			return 1;
	
}

function loadSalesOrderStatus($orderNo,$orderYear,$salesOrderId){
	global $db;
	$sql	="SELECT
			trn_orderdetails.`STATUS`
			FROM `trn_orderdetails`
			WHERE
			trn_orderdetails.intOrderNo = '$orderNo' AND
			trn_orderdetails.intOrderYear = '$orderYear' AND
			trn_orderdetails.intSalesOrderId = '$salesOrderId'
			";
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return $rows['STATUS'];
}

/*function have_customer_emails($customer_id)
{
	global $db;
	$sql = "SELECT
				mst_customer_locations.strEmailAddress
			FROM `mst_customer_locations`
			WHERE
				mst_customer_locations.intLocationId = $customer_id
			";
	$result = $db->RunQuery2($sql);
	$rows = mysqli_fetch_array($result);
	if($row['strEmailAddress']=='')
		return false;
	else
		return true;
}*/
//--------------------------------------------------------
function sendToExtraApproval($serialNo,$year,$mainPat,$root_path,$type){
	global $db;
	global $userId;
	global $objMail;
				
	if($type=='plant_manager')
		$receiver_details	=	get_plant_manager_details($serialNo,$year,$userId);
	else if($type=='cluster_head')
		$receiver_details	=	get_cluster_head_details($serialNo,$year,$userId);
	
	$to_name	=	$receiver_details['name'];
	$to_email	=	$receiver_details['email'];

	$header		="PLEASE CONFIRM DISPATCH NOTE ('$serialNo'/'$year')"; 
	 
	//send mails ////
	$requestArray = NULL;
	if($type=='plant_manager')
	$requestArray['program']='Dispatch Note (exceeds size wise cumulative pd%)';
	else if($type=='cluster_head')
	$requestArray['program']='Dispatch Note (exceeds s/o wise cumulative pd%)';

	$requestArray['field1']	='DISPATCH NO';
	$requestArray['field2']	='YEAR';
	$requestArray['field3']	='';
	$requestArray['field4']	='';
	$requestArray['field5']	='';
	$requestArray['value1']	=$serialNo;
	$requestArray['value2']	=$year;
	$requestArray['value3']	='';
	$requestArray['value4']	='';
	$requestArray['value5']	='';
	
	$requestArray['subject']="CONFIRM DISPATCH NOTE('$serialNo'/'$year')";
	
	$requestArray['statement1']="Approve this";
	$requestArray['statement2']="to approve this";
	//$_REQUEST['link']=base64_encode($mainPath."presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder.php?orderNo=$serialNo&orderYear=$year&approveMode=1"); 
	$requestArray['link']=base64_encode($_SESSION['MAIN_URL']."index.php?q=919&serialNo=$serialNo&year=$year"); 
	//$path=$mainPath.'presentation/mail_approval_template.php';
	$path=$root_path."presentation/mail_approval_template.php";
	//$objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
	$objMail->send_Response_Mail2($path,$requestArray,$_SESSION["systemUserName"],$_SESSION["email"],$header,$to_email,$to_name);
			

}
function get_plant_manager_details($serialNo,$year,$user){
	global $db;
	
	$sql="SELECT
			group_concat(sys_users.intUserId) as intUserId,
			group_concat(UPPER(sys_users.strUserName) SEPARATOR ' / ') as strUserName,
			group_concat(sys_users.strEmail) as strEmail,
			if(FIND_IN_SET('$user',group_concat(sys_users.intUserId)),1,0) as flag 
			FROM
			ware_fabricdispatchheader
			INNER JOIN mst_locations ON ware_fabricdispatchheader.intCompanyId = mst_locations.intId
			INNER JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId
			INNER JOIN mst_plant_dispatch_approve_users ON mst_plant.intPlantId = mst_plant_dispatch_approve_users.PLANT_ID 
			and mst_plant_dispatch_approve_users.APPROVE_LEVEL = 2 
			INNER JOIN sys_users ON mst_plant_dispatch_approve_users.`USER` = sys_users.intUserId
			WHERE
			 ware_fabricdispatchheader.intBulkDispatchNo = '$serialNo' AND
			 ware_fabricdispatchheader.intBulkDispatchNoYear = '$year' AND
			mst_plant_dispatch_approve_users.APPROVE_LEVEL = 2 
			GROUP BY
			mst_plant_dispatch_approve_users.PLANT_ID
		";
	$result 		= $db->RunQuery2($sql);
	$rows 			= mysqli_fetch_array($result);
	$data['id']		= $rows['intUserId'];
	$data['name']	= $rows['strUserName'];
	$data['email']	= $rows['strEmail'];
	$data['flag']	= $rows['flag'];
	return $data;
}

function get_cluster_head_details($serialNo,$year,$user){
	global $db;
 
	$sql="SELECT
			group_concat(sys_users.intUserId) as intUserId,
			group_concat(UPPER(sys_users.strUserName) SEPARATOR ' / ') as strUserName,
			group_concat(sys_users.strEmail) as strEmail,
			if(FIND_IN_SET('$user',group_concat(sys_users.intUserId)),1,0) as flag 
			FROM
			ware_fabricdispatchheader
			INNER JOIN mst_locations ON ware_fabricdispatchheader.intCompanyId = mst_locations.intId
			INNER JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId
			INNER JOIN mst_plant_dispatch_approve_users ON mst_plant.intPlantId = mst_plant_dispatch_approve_users.PLANT_ID 
			and mst_plant_dispatch_approve_users.APPROVE_LEVEL = 3 
			INNER JOIN sys_users ON mst_plant_dispatch_approve_users.`USER` = sys_users.intUserId
			WHERE
			 ware_fabricdispatchheader.intBulkDispatchNo = '$serialNo' AND
			 ware_fabricdispatchheader.intBulkDispatchNoYear = '$year' AND
			mst_plant_dispatch_approve_users.APPROVE_LEVEL = 3 
			GROUP BY
			mst_plant_dispatch_approve_users.PLANT_ID
		";
	$result 		= $db->RunQuery2($sql);
	$rows 			= mysqli_fetch_array($result);
	$data['id']		= $rows['intUserId'];
	$data['name']	= $rows['strUserName'];
	$data['email']	= $rows['strEmail'];
	$data['flag']	= $rows['flag'];
	return $data;
	
}

function updateAzureTable($serialNo,$serialYear,$db,$transactionType)
{

    global $objAzure;
    $environment = $objAzure->getEnvironment();
    $headerTable = 'SalesHeader'.$environment;
    $lineTable = 'SalesLine'.$environment;

    $azure_connection = $objAzure->connectAzureDB();
		$sql_header = "SELECT
						TH.intReviseNo,
						CONCAT(TH.intOrderNo,'/',TH.intOrderYear) AS poString,
						TH.intStatus,
						ware_fabricdispatchheader.strRemarks,
						TH.intApproveLevelStart,
						mst_customer_locations_header.strName AS customer_location,
						TH.strCustomerPoNo,
						ware_fabricdispatchheader.intCompanyId AS location,
						TH.intCustomer,
						TH.intPaymentTerm,
						TH.dtmCreateDate AS order_date,
						mst_customer.strName AS customer,
						mst_customer.strCode AS customerCode,
						TH.strCustomerPoNo,
						mst_financecurrency.strCode AS curr_code,
						TH.intMarketer AS marketer,
						(
							SELECT
								CAST(
									MAX(
										ware_stocktransactions_fabric.dtDate
									) AS DATE
								)
							FROM
								ware_stocktransactions_fabric
							WHERE
								ware_stocktransactions_fabric.intOrderNo = TH.intOrderNo
							AND ware_stocktransactions_fabric.intOrderYear = TH.intOrderYear
							AND ware_stocktransactions_fabric.strType LIKE '%Dispatched%' 
							GROUP BY
								ware_stocktransactions_fabric.intOrderNo,
								ware_stocktransactions_fabric.intOrderYear
						) AS lastDispatchDate
					FROM
					ware_fabricdispatchheader 
					INNER JOIN trn_orderheader TH ON TH.intOrderNo = ware_fabricdispatchheader.intOrderNo AND TH.intOrderYear = ware_fabricdispatchheader.intOrderYear
					INNER JOIN mst_customer ON mst_customer.intId = TH.intCustomer
					INNER JOIN mst_financecurrency ON mst_financecurrency.intId = TH.intCurrency
					LEFT JOIN mst_customer_locations_header ON ware_fabricdispatchheader.intCustLocation = mst_customer_locations_header.intId
					WHERE
						ware_fabricdispatchheader.intBulkDispatchNo = '$serialNo'
					AND ware_fabricdispatchheader.intBulkDispatchNoYear = '$serialYear'";

    $result_header = $db->RunQuery2($sql_header);
    while ($row_header = mysqli_fetch_array($result_header)) {
        $poString = trim($row_header['poString']);
        $customer = trim($row_header['customer']);
        $cus_location = trim($row_header['customer_location']);
        $customerCode = $row_header['customerCode'];
        $strCustomerPoNo = trim($row_header['strCustomerPoNo']);
        $strRemark = str_replace("'","''",$row_header['strRemarks']);
        $strRemark_sql = $db->escapeString($strRemark);
        $orderDate = $row_header['order_date'];
        $curr_code = ($row_header['curr_code'] == 'EURO')?"Eur":($row_header['curr_code'] == 'LKR'?"":$row_header['curr_code']);
        $payment_term = $row_header['intPaymentTerm'];
        $marketer = $row_header['marketer'];
        $location = $row_header['location'];
        $revise_no = $row_header['intReviseNo'];
        $lastDispatchDate = $row_header['lastDispatchDate'];
        $shipmentNo = $serialNo.'/'.$serialYear;
        $successHeader = 0;
        $postingDate = date('Y-m-d H:i:s');
        $i = 0;
        $canSend = true;

        $sql_select = "SELECT
						OD.strStyleNo,
						OD.strGraphicNo,
						OD.strSalesOrderNo,
						OD.intSalesOrderId,
						OD.strPrintName,
						OD.dtPSD AS PSD,
						OD.strCombo,
						OD.TECHNIQUE_GROUP_ID,
						OD.SO_TYPE,
						OD.intOrderNo,
						OD.intOrderYear,
						OD.linkedSoId,
						(
							SELECT
								TECHNIQUE_GROUP_NAME
							FROM
								mst_technique_groups
							WHERE
								mst_technique_groups.TECHNIQUE_GROUP_ID = OD.TECHNIQUE_GROUP_ID
						) AS technique,
						CONCAT(
							OD.intSampleNo,
							'/',
							OD.intSampleYear
						) AS sampleString,
						OD.dblPrice AS price,
						ware_fabricdispatchdetails.strCutNo,
						mst_part.strName AS partName,
						ware_fabricdispatchdetails.strLineNo,
						SUM(
							ware_fabricdispatchdetails.dblGoodQty
						) AS dblGoodQty
					FROM
						trn_orderdetails OD
					INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = OD.intOrderNo
					AND ware_fabricdispatchheader.intOrderYear = OD.intOrderYear
					INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo
					AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear
					AND ware_fabricdispatchdetails.intSalesOrderId = OD.intSalesOrderId
					LEFT JOIN mst_part ON ware_fabricdispatchdetails.intPart = mst_part.intId
					WHERE
						ware_fabricdispatchheader.intBulkDispatchNo = '$serialNo'
					AND ware_fabricdispatchheader.intBulkDispatchNoYear = '$serialYear'
					GROUP BY
						OD.intOrderNo,
						OD.intOrderYear,
						OD.intSalesOrderId";

        $result1 = $db->RunQuery2($sql_select);
        while ($row = mysqli_fetch_array($result1)) {
            $orderNo = $row['intOrderNo'];
            $orderYear = $row['intOrderYear'];
            $intSalesOrderId = $row['intSalesOrderId'];
            $strSalesOrderNo = trim($row['strSalesOrderNo']);
            $line_no = intval($row['strLineNo']);
            $strGraphicNo = trim($row['strGraphicNo']);
            $strCombo = trim($row['strCombo']);
            $sampleNo = trim($row['sampleString']);
            $strStyleNo = trim($row['strStyleNo']);
            $strPrintName = trim($row['strPrintName']);
            $poqty = $row['dblGoodQty'];
            $partName = trim($row['partName']);
            $brand = trim($row['brand']);
            $amount = round( $row['price'], 5);
            $psd = $row['PSD'];
            $technique = $row['technique'];
			if (strpos($technique, 'Plotter') !== false) {
				$technique='Plotter/Lazer cut';
			}
            $soType = $row['SO_TYPE'];
            $linkedSoId = $row['linkedSoId'];
            $successDetails = 0;
            $postingDate = date('Y-m-d H:i:s');

            if($poqty > 0 && $soType !=2) {
            	$i++;
            	if ($soType == -1){
                    $soDetails = getLinkedSoDetails($orderNo, $orderYear,$linkedSoId);
                    $intSalesOrderId = $linkedSoId;
                    $strSalesOrderNo = $soDetails['strSalesOrderNo'];
                    $amount =   round( $soDetails['dblPrice'] + $amount, 5);
				}
                $sql_azure_details = "INSERT into $lineTable (Transaction_type, Order_No, Document_Type, Sales_Order_No, Line_No, Shipment_No, Sales_Order_Name, Item_Code, Description, Item_Category, Item_SubCategory, Quantity, Original_Quantity, Amount, VAT_Prod_Posting_Group, Technique_Revenue_Code, Graphic_No, Style_No, [Print], Combo, Part, Brand, Sample_No, Production_Start_Date, Revised_Count) VALUES ('$transactionType','$poString', 'Order','$intSalesOrderId','$line_no', '$shipmentNo', '$strSalesOrderNo', 'PRINT','PRINT','PRINT','PRINT','$poqty','$poqty','$amount', 'VAT', '$technique', '$strGraphicNo','$strStyleNo','$strPrintName','$strCombo','$partName','$brand','$sampleNo','$psd', '$revise_no')";
                if ($azure_connection) {
                    $getResults = $objAzure->runQuery($azure_connection, $sql_azure_details);
                    if ($getResults != FALSE) {
                        $successDetails = 1;
                    }
                }
                $sql_details = "INSERT into trn_financemodule_salesline (Transaction_type, Order_No, Sales_Order_No, Line_No, Shipment_No, Sales_Order_Name, Item_Code, Description, Item_Category, Item_SubCategory, Quantity, Original_Quantity, Amount, VAT_Prod_Posting_Group, Technique_Revenue_Code, Graphic_No, Style_No, Print, Combo, Part, Brand, Sample_No, Production_Start_Date, Revised_Count, deliveryStatus, deliveryDate) VALUES ('$transactionType','$poString','$intSalesOrderId','$line_no', '$shipmentNo', '$strSalesOrderNo', 'PRINT','PRINT','PRINT','PRINT','$poqty', '$poqty','$amount', 'VAT', '$technique', '$strGraphicNo','$strStyleNo','$strPrintName','$strCombo','$partName','$brand','$sampleNo','$psd', '$revise_no', '$successDetails',NOW())";
                $result_details = $db->RunQuery2($sql_details);
            }
        }

        if($i>0) {
            $sql_azure_header = "INSERT into $headerTable (Transaction_type, Order_No, Document_Type, Shipment_No, Customer_Code, Customer_Name, Posting_Date, Order_Date, Currency_Code, Narration, Payment_Terms, Customer_PO_Number, Sales_Person, Location_Code, No_of_Lines, Last_Dispatched_Date, Revised_Count, Customer_Location) VALUES ('$transactionType','$poString','Order','$shipmentNo','$customerCode','$customer', '$postingDate', '$orderDate','$curr_code','$strRemark','$payment_term','$strCustomerPoNo', '$marketer', '$location', '$i', '$lastDispatchDate', '$revise_no', '$cus_location')";
            if ($azure_connection) {
                $getResults = $objAzure->runQuery($azure_connection, $sql_azure_header);
                if ($getResults != FALSE) {
                    $successHeader = 1;
                }
            }
            $sql = "INSERT into trn_financemodule_salesheader (Transaction_type, Order_No, Document_Type, Shipment_No, Customer_Code, Customer_Name, Posting_Date, Order_Date, Currency_Code, Narration, Payment_Terms, Customer_PO_Number, Sales_Person, Location_Code, No_of_Lines, Last_Dispatched_Date, Revised_Count, Customer_Location,  deliveryStatus, deliveryDate ) VALUES ('$transactionType','$poString','Order', '$shipmentNo', '$customerCode','$customer', '$postingDate', '$orderDate','$curr_code','$strRemark_sql','$payment_term','$strCustomerPoNo', '$marketer', '$location', '$i', '$lastDispatchDate', '$revise_no', '$cus_location', '$successHeader', NOW())";
            $result_header = $db->RunQuery2($sql);
        }

    }
}

function getLinkedSoDetails($orderNo, $year,$soId){

    global $db;

    $sql = "SELECT 
			trn_orderdetails.strSalesOrderNo,
			trn_orderdetails.dblPrice 
			FROM trn_orderdetails 
			WHERE
			trn_orderdetails.intOrderNo =  '$orderNo' AND
			trn_orderdetails.intOrderYear =  '$year' AND 
			trn_orderdetails.intSalesOrderId =  '$soId'
			";
    $result = $db->RunQuery2($sql);
    $row=mysqli_fetch_array($result);

    return $row;
}

?>


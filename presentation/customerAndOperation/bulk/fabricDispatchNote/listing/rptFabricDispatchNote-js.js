var basepath	= 'presentation/customerAndOperation/bulk/fabricDispatchNote/listing/'

$(document).ready(function() {
	$('#frmFabDispatchReport').validationEngine();
	
	$('#frmFabDispatchReport #imgApprove').click(function(){
	var val = $.prompt('Are you sure you want to approve this Dispatch Note ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
						showWaiting();
						if(validateQuantities()==0){
						
						var url = basepath+"rptFabricDispatchNote-db-set.php"+window.location.search+'&status=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmFabDispatchReport #imgApprove').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmFabDispatchReport #imgApprove').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
						
						}
						
					hideWaiting();	
					}
				}});
	});
	
	$('#frmFabDispatchReport #imgReject').click(function(){
	var val = $.prompt('Are you sure you want to reject this Fabric Dispatch Note ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
									if(validateRejecton()==0){ 
										var url = basepath+"rptFabricDispatchNote-db-set.php"+window.location.search+'&status=reject';
										var obj = $.ajax({url:url,async:false});
										window.location.href = window.location.href;
										window.opener.location.reload();//reload listing page
										//document.getElementById('txtApproveStatus').value = 0;
										//document.getElementById('frmSampleReport').submit();
									}
									}
								}});
	});
});

function validateQuantities(){
		var ret=0;	
	    var serialNo = document.getElementById('divSerialNo').innerHTML;
		var url 		= basepath+"rptFabricDispatchNote-db-get.php?requestType=getValidation";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"serialNo="+serialNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
						$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
						//alert(json.msg);
						var t=setTimeout("alertx()",3000);
					  	ret= 1;
					}
				}
			});
			return ret;
}

function validateRejecton(){
		var ret=0;	
	    var serialNo = document.getElementById('divSerialNo').innerHTML;
		var url 		= basepath+"rptFabricDispatchNote-db-get.php?requestType=validateRejecton";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"serialNo="+serialNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",3000);
					  ret= 1;
					}
				}
			});
			return ret;
}
function alertx()
{
	$('#frmFabDispatchReport #imgApprove').validationEngine('hide')	;
}
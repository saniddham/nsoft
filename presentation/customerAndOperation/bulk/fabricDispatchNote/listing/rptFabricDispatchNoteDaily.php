<?php


$thisFilePath =  $_SERVER['PHP_SELF'];
include_once '../../../../../dataAccess/LoginDBManager.php';
include_once "../../../../../libraries/mail/mail_bcc.php";		
$db =  new LoginDBManager();

//$serialNo = $_REQUEST['serialNo'];
//$year = $_REQUEST['year'];
$approveMode = $_REQUEST['approveMode'];

$programName='Dispatch Note';
$programCode='P0470';

/*$gatePassNo = '100000';
$year = '2012';
$approveMode=1;
*/

  $sql_cus = "SELECT
mst_customer.intId,
mst_customer.strCode,
mst_customer.strName,
mst_customer.strEmail,
mst_customer_locations.intLocationId,
mst_customer_locations.strEmailAddress AS custLocaEmail,
mst_customer_locations.strCCEmailAddress AS userCCemails,
mst_customer_locations_header.strName as custLocName
FROM
mst_customer
INNER JOIN mst_customer_locations ON mst_customer.intId = mst_customer_locations.intCustomerId
INNER JOIN mst_customer_locations_header ON mst_customer_locations.intLocationId = mst_customer_locations_header.intId
WHERE 
mst_customer.intMailAlertDispatchSummaryReport =  '1'

";
				 $result_cus = $db->RunQuery($sql_cus);
				 while($row_cus=mysqli_fetch_array($result_cus))
				 { 
					$customerId = $row_cus['intId'];
					$customerName = $row_cus['strName'];
					$customerEmail = $row_cus['strEmail'];
					$userCCemails = $row_cus['userCCemails'];
					$custLocation = $row_cus['intLocationId'];
					$custLocaEmail = $row_cus['custLocaEmail'];
					$custLocName = $row_cus['custLocName'];
				 
	ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Fabric Dispatch Report</title>


<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:272px;
	top:511px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}


 table .bordered{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}


.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}



.odd{background-color:#F5F5F5;}
.even{background-color:#FFFFFF;}
.mouseover {
	cursor: pointer;
}


.txtNumber{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:right;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.txtText{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:left;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}

.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntGrey {
	font-family: Verdana;
	font-size: 11px;
	color: #999;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:right;
}





</style>
</head>
</head>

<body><form id="frmFabDispatchReport" name="frmFabDispatchReport" method="post" action="rptFabricDispatchNote.php">
<table width="1200" cellpadding="0" cellspacing="0" align="center">
<tr>
<td colspan="3"></td>
</tr>
<tr>
    <td colspan="9" align="center" ><table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="tophead"><table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td width="8%" class="normalfnt"><img style="vertical-align:" src="http://accsee.com/screenline.jpg"  /></td>
            <td align="center" valign="top" width="75%" class="topheadBLACK"><?php
	 $SQL = "SELECT
*
FROM
mst_companies
Inner Join mst_country ON mst_companies.intCountryId = mst_country.intCountryID
WHERE
mst_companies.intId =  '1'
;";
	
	$result = $db->RunQuery($SQL);
	$row = mysqli_fetch_array($result);
	$companyName		= $row["strName"];
	$locationName		= $row["locationName"];
	$companyAddress1	= $row["strAddress"];
	$companyStreet		= $row["strStreet"];
	$companyCity		= $row["strCity"];
	$companyCountry		= $row["strCountryName"];
	$companyZipCode		= $row["strZip"];
	$companyPhone		= "(".$companyZipCode.")".$row["strPhoneNo"];
	$companyFax		 	= "(".$companyZipCode.")".$row["strFaxNo"];
	$companyEmail		= $row["strEMail"];
	$companyWeb		 	= $row["strWebSite"];
	
	?>
              <b><?php echo $companyName.""; ?></b>
              <p class="normalfntMid">
                <?php // (".$locationName.") --> Edited By Lasantha 14th of May 2012?>
                <?php echo $companyAddress1." ".$companyStreet." ".$companyCity." ".$companyCountry."."."<br><b>Tel : </b>".$companyPhone." <b>Fax : </b>".$companyFax." <br><b>E-Mail : </b>".$companyEmail." <b>Web : </b>".$companyWeb;?></p></td>
            <td width="9%" class="tophead"><img src="http://accsee.com/nsoft_logo.png" style="vertical-align:"  /></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ></div>
<table width="1200" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td colspan="2">
  <table width="100%">
  <tr>
    <td colspan="10" align="center"><span style="background-color:#FFF"><div align="left"></div>DISPATCH SUMMARY REPORT</span></td>
    </tr>
  <tr>
    <td width="5%">&nbsp;</td>
    <td width="11%" class="normalfnt">Customer</td>
    <td width="4%" align="center" valign="middle"><strong>:</strong></td>
    <td width="20%"><span class="normalfnt"><?php echo $customerName; ?></span></td>
    <td width="7%"><span class="normalfnt">Location</span></td>
    <td width="4%" align="center" valign="middle"><strong>:</strong></td>
    <td width="24%"><span class="normalfnt"><?php echo $custLocName; ?></span></td>
    <td width="6%" class="normalfnt">Date</td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="18%"><span class="normalfnt"><?php echo date('Y-m-d'); ?></span></td>
  </tr>
  
  </table>
  </td>
</tr>
<tr>
  <td colspan="2">
    <table width="100%">
      <tr>
        <td>
          
          <table width="100%"  class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <thead>
              <tr class="">
                <th width="11%" >Plant</th>
                <th width="7%" >Brand</th>
                <th width="6%" >Dispatch No</th>
                <th width="4%" >Order No</th>
                <th width="8%" >Sales Order No</th>
                <th width="9%" >Graphic No</th>
                <th width="9%" >Style No</th>
                <th width="10%" >Customer PO</th>
                <th width="4%" >Total In</th>
                <th width="5%" >Total Delivery</th>
                <th width="5%" >Sample Qty</th>
                <th width="4%" >Good Qty</th>
                <th width="5%" >EMD-D Qty</th>
                <th width="4%" >P-D Qty</th>
                <th width="4%" >F-D Qty</th>
                <th width="5%" >Total Qty</th>
                </tr>
              </thead>
            <tbody>
              <?php 
			  $curruntTime=date("Y-m-d H:i:s");
			  $curruntTimeFrom = date("Y-m-d H:i:s",strtotime(date('Y-m-j H:i:s')) - (24 * 60 * 60));//correct
			 //$curruntTimeFrom = date("Y-m-d H:i:s",strtotime(date('Y-m-j H:i:s')) - (2*60*24 * 60 * 60));//temp
			  
			  
			  
			  //comment by roshan for adding customer brand and dispatch no
			  
	  	      /*$sql1 = "SELECT 
trn_orderdetails.strStyleNo,
trn_orderdetails.strGraphicNo, 
trn_orderheader.strCustomerPoNo,
ware_fabricdispatchheader.intOrderNo,
ware_fabricdispatchheader.intOrderYear,
trn_orderdetails.strSalesOrderNo,
sum(ware_fabricdispatchdetails.dblSampleQty) as dblSampleQty,
sum(ware_fabricdispatchdetails.dblGoodQty) as dblGoodQty,
sum(ware_fabricdispatchdetails.dblEmbroideryQty) as dblEmbroideryQty,
sum(ware_fabricdispatchdetails.dblPDammageQty) as dblPDammageQty,
sum(ware_fabricdispatchdetails.dblFDammageQty) as dblFDammageQty 
FROM
trn_orderdetails 
Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear 
Inner Join ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear
Inner Join ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
left Join mst_part ON ware_fabricdispatchdetails.intPart = mst_part.intId
left Join mst_colors_ground ON ware_fabricdispatchdetails.intGroundColor = mst_colors_ground.intId 
Inner Join ware_fabricdispatchheader_approvedby ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchheader_approvedby.intBulkDispatchNo 
AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchheader_approvedby.intYear AND ware_fabricdispatchheader_approvedby.intApproveLevelNo=ware_fabricdispatchheader.intApproveLevels 
WHERE  
ware_fabricdispatchheader_approvedby.dtApprovedDate <'$curruntTime' AND  
ware_fabricdispatchheader_approvedby.dtApprovedDate >= '$curruntTimeFrom' AND  
ware_fabricdispatchheader.intStatus =  '1'  
AND trn_orderheader.intCustomer = '$intCustomer'

group by trn_orderdetails.strSalesOrderNo , 
ware_fabricdispatchheader.intOrderNo, 
ware_fabricdispatchheader.intOrderYear  
order by 
ware_fabricdispatchheader.intOrderYear asc , 
ware_fabricdispatchheader.intOrderNo asc , 
trn_orderdetails.strSalesOrderNo asc ,
trn_orderdetails.strStyleNo asc,
trn_orderheader.strCustomerPoNo asc 

";*/
			$sql1 = "SELECT
distinct 
ware_fabricdispatchheader.intBulkDispatchNo,
ware_fabricdispatchheader.intBulkDispatchNoYear,
ware_fabricdispatchheader_approvedby.dtApprovedDate,
trn_orderdetails.strStyleNo,
trn_orderdetails.strGraphicNo,
trn_orderheader.strCustomerPoNo,
ware_fabricdispatchheader.intOrderNo,
ware_fabricdispatchheader.intOrderYear,
trn_orderdetails.strSalesOrderNo,
Sum(ware_fabricdispatchdetails.dblSampleQty) AS dblSampleQty,
Sum(ware_fabricdispatchdetails.dblGoodQty) AS dblGoodQty,
Sum(ware_fabricdispatchdetails.dblEmbroideryQty) AS dblEmbroideryQty,
Sum(ware_fabricdispatchdetails.dblPDammageQty) AS dblPDammageQty,
Sum(ware_fabricdispatchdetails.dblFDammageQty) AS dblFDammageQty,
mst_brand.strName AS brandName,
trn_orderdetails.dblDamagePercentage,
mst_plant.strPlantName								 							AS plantName,
CONCAT(mst_plant.strPlantHeadName,'(', mst_plant.strPlantName  ,')',' - ',strPlantHeadContactNo) 						AS PlantHeadName,

ROUND(COALESCE((SELECT
  SUM(dblQty)
FROM ware_fabricreceivedheader FRH
  INNER JOIN ware_fabricreceiveddetails FRD
    ON FRD.intFabricReceivedNo = FRH.intFabricReceivedNo
      AND FRD.intFabricReceivedYear = FRH.intFabricReceivedYear
  INNER JOIN trn_orderdetails SUB_OD 
    ON SUB_OD.intOrderNo = FRH.intOrderNo 
      AND SUB_OD.intOrderYear = FRH.intOrderYear
      AND SUB_OD.intSalesOrderId = FRD.intSalesOrderId 
WHERE 
	FRH.intOrderNo = trn_orderheader.intOrderNo
    AND FRH.intOrderYear = trn_orderheader.intOrderYear
	AND SUB_OD.strGraphicNo = trn_orderdetails.strGraphicNo
	AND FRH.intStatus = 1),0))						AS TOTAL_IN,

ROUND(COALESCE((SELECT SUM(dblGoodQty)
FROM ware_fabricdispatchheader FDH
  INNER JOIN ware_fabricdispatchdetails FDD
    ON FDD.intBulkDispatchNo = FDH.intBulkDispatchNo
      AND FDD.intBulkDispatchNoYear = FDH.intBulkDispatchNoYear
  INNER JOIN trn_orderdetails SUB_OD 
    ON SUB_OD.intOrderNo = FDH.intOrderNo 
      AND SUB_OD.intOrderYear = FDH.intOrderYear
      AND SUB_OD.intSalesOrderId = FDD.intSalesOrderId
WHERE 
	FDH.intOrderNo = trn_orderheader.intOrderNo
	AND FDH.intOrderYear = trn_orderheader.intOrderYear
	AND SUB_OD.strGraphicNo = trn_orderdetails.strGraphicNo
	AND FDH.intStatus = 1),0))														AS TOTAL_DELEVERY
	
FROM
trn_orderdetails
Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
Inner Join ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear
Inner Join ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
Left Join mst_part ON ware_fabricdispatchdetails.intPart = mst_part.intId
Left Join mst_colors_ground ON ware_fabricdispatchdetails.intGroundColor = mst_colors_ground.intId
Inner Join ware_fabricdispatchheader_approvedby ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchheader_approvedby.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchheader_approvedby.intYear AND ware_fabricdispatchheader_approvedby.intApproveLevelNo = ware_fabricdispatchheader.intApproveLevels
Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_orderdetails.intRevisionNo
Inner Join mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
INNER JOIN mst_locations 
	ON mst_locations.intId = ware_fabricdispatchheader.intCompanyId
INNER JOIN mst_companies
	ON mst_companies.intId = mst_locations.intCompanyId
INNER JOIN mst_plant 
    ON mst_plant.intPlantId = mst_locations.intPlant
WHERE  
ware_fabricdispatchheader_approvedby.dtApprovedDate < NOW()  AND  
ware_fabricdispatchheader_approvedby.dtApprovedDate >= DATE_ADD(NOW(),INTERVAL -1 DAY )  AND  
ware_fabricdispatchheader.intStatus =  '1'  
AND trn_orderheader.intCustomer = $customerId 
AND ware_fabricdispatchheader.intCustLocation = $custLocation
GROUP BY
trn_orderdetails.strSalesOrderNo,
ware_fabricdispatchheader.intOrderNo,
ware_fabricdispatchheader.intOrderYear,
trn_sampleinfomations.intBrand,
ware_fabricdispatchheader.intBulkDispatchNo,
ware_fabricdispatchheader.intBulkDispatchNoYear
ORDER BY
mst_brand.strName,
ware_fabricdispatchheader.intOrderYear ASC,
ware_fabricdispatchheader.intOrderNo ASC,
trn_orderdetails.strSalesOrderNo ASC,
trn_orderdetails.strStyleNo ASC,
trn_orderheader.strCustomerPoNo ASC
";//echo($sql1);
			$result1 = $db->RunQuery($sql1);
			
			$totsampleQty=0;
			$totgoodQty=0;
			$totembroideryQty=0;
			$totpDammageQty=0;
			$totfDammageQty=0;
			$total=0;
			$totQty=0;
			$totAmmount=0;
			$i	= 0;
			$haveRecords = false;
			$STR_GRAHPICNO_LIST = '';
			$array_in	= array();
			while($row1=mysqli_fetch_array($result1))
			{
				$array_in[]	= $row1["PlantHeadName"];
				$haveRecords = true;
				$intBulkDispatchNo = $row1['intBulkDispatchNo'];
				$dblDamagePercentage = $row1['dblDamagePercentage'];
				$intBulkDispatchNoYear = $row1['intBulkDispatchNoYear'];
				$brandName = $row1['brandName'];
				$plantHeadName	= $row1["PlantHeadName"];
				$style = $row1['strStyleNo']; 
				
				/////////////////concat graphic no's///////////
						if($graphicNo!=$row1['strGraphicNo'])
						{
							$STR_GRAHPICNO_LIST .=$row1['strGraphicNo'] . ' / ';	
						}
				///////////////////////////////////////////////
				$graphicNo = $row1['strGraphicNo']; 
				$custPO = $row1['strCustomerPoNo'];
				$orderNo = $row1['intOrderNo'];
				$orderYear = $row1['intOrderYear'];
				
				$cutNo=$row1['strCutNo'];
				$salesOrderNo=$row1['strSalesOrderNo'];
				$part=$row1['part'];
				$bgColor=$row1['bgcolor'];
				$lineNo=$row1['strLineNo'];
				$size=$row1['strSize'];
				$sampleQty=$row1['dblSampleQty'];
				$goodQty=$row1['dblGoodQty'];
				$embroideryQty=$row1['dblEmbroideryQty'];
				$pDammageQty=$row1['dblPDammageQty'];
				$fDammageQty=$row1['dblFDammageQty'];
				$remarks=$row1['strRemarks'];
				$tot=(float)($sampleQty)+(float)($goodQty)+(float)($embroideryQty)+(float)($pDammageQty)+(float)($fDammageQty);
				
	  ?>
              <tr class="normalfnt"   bgcolor="#FFFFFF">
                <td align="center" class="normalfntMid" id="<?php echo $orderYear; ?>" ><?php echo $row1["plantName"]; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $orderYear; ?>" ><?php echo $brandName; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $orderYear; ?>" ><?php echo $intBulkDispatchNoYear.'/'.$intBulkDispatchNo; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $orderNo; ?>" ><?php echo $orderYear.'/'.$orderNo; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $salesOrderNo; ?>" ><?php echo $salesOrderNo; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $graphicNo; ?>" ><?php echo $graphicNo; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $style; ?>" ><?php echo $style; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $custPO; ?>" ><?php echo $custPO; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo number_format($row1["TOTAL_IN"])?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo number_format($row1["TOTAL_DELEVERY"])?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $sampleQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $goodQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $embroideryQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $pDammageQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $fDammageQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $tot; ?></td>
                </tr>              
              <?php 
			$totsampleQty+=$sampleQty;
			$totgoodQty+=$goodQty;
			$totembroideryQty+=$embroideryQty;
			$totpDammageQty+=$pDammageQty;
			$totfDammageQty+=$fDammageQty;
			$total+=$tot;
			
			
			
			}
	  ?>
              <tr class="normalfnt"  bgcolor="#CCCCCC">
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfntRight" >&nbsp;</td>
                <td class="normalfntRight" >&nbsp;</td>
                <td class="normalfntRight" ><b><?php echo $totsampleQty ?></b></td>
                <td class="normalfntRight" ><b><?php echo $totgoodQty ?></b></td>
                <td class="normalfntRight" ><b><?php echo $totembroideryQty ?></b></td>
                <td class="normalfntRight" ><b><?php echo $totpDammageQty ?></b></td>
                <td class="normalfntRight" ><b><?php echo $totfDammageQty ?></b></td>
                <td class="normalfntRight" ><b><?php echo $total ?></b></td>
                </tr>
              </tbody>
          </table>        </td>
        </tr>
      
      </table>
    </td>
</tr>

<tr height="40">
  <td width="317" align="center" class="normalfnt" ><?php
$plantHeadName = "";
$array_in1 = array_map("unserialize", array_unique(array_map("serialize", $array_in)));
echo "<span style=\"font-weight:bold;text-decoration:underline\">Contact Details</span></br>";
for($loop=0;$loop<count($array_in);$loop++)
{
	if($array_in1[$loop]!="")
		echo $array_in1[$loop].'</br>';
}
?></td>
  <td width="873" align="center" class="normalfntRight"><span class="normalfntMid">Printed Date:<strong> <?php echo date("Y/m/d  H:i:s") ?></strong></span></td>
</tr>

<tr height="40">
  <td width="317" align="center" class="normalfnt" >&nbsp;</td>
  <td width="873" align="center" class="normalfntRight">&nbsp;</td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php

 	    $body = ob_get_clean();
		
		if($haveRecords)
		{
			$nowDate = date('Y-m-d');
			$mailHeader= "DISPATCH SUMMARY ($nowDate) - $STR_GRAHPICNO_LIST";
			$mailcount++;
			$sql = "SELECT
						sys_users.strEmail,sys_users.strFullName
					FROM
					sys_mail_eventusers
						Inner Join sys_users ON sys_users.intUserId = sys_mail_eventusers.intUserId
					WHERE
						sys_mail_eventusers.intMailEventId =  '1010'
					";
			$result = $db->RunQuery($sql);
			$ccEmail = '';
			while($row=mysqli_fetch_array($result))
			{
				$ccEmail .= $row['strEmail'].',';
			}
			/*live*/
			sendMessage('nsoft@screenlineholdings.com','NSOFT - SCREENLINE HOLDINGS.',$custLocaEmail,$mailHeader,$body,$ccEmail.','.$userCCemails,'nsoftemail@gmail.com,dhammika@screenlineholdings.com,roshan@screenlineholdings.com');
			
			/*testing*/
			//sendMessage('nsoft@screenlineholdings.com','NSOFT - SCREENLINE HOLDINGS.','roshan@screenlineholdings.com;',$mailHeader,"[customer email list - $custLocaEmail]".$body,'','');
			
			/*sendMessage('nsoft@screenlineholdings.com','NSOFT - SCREENLINE HOLDINGS.','buddhikak@screenlineholdings.com',$mailHeader,"[customer email list - $custLocaEmail,$userCCemails]".$body,'','');*/
		echo $body;
		}	
		
}

echo 'DISPATCH CUSTOMER WISE REPORT -> MAIL COUNT : '.$mailcount.'<br>';
?>
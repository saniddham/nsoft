<?php
//ini_set('display_errors', '1');
	session_start();
	$user_id= $_SESSION['userId'];
	if($_SESSION['userId']=='')
        die("Temporary unavailable. Please contact the admin");

/**
 * Created by PhpStorm.
 * User: HASITHA
 * Date: 12/8/2017
 * Time: 12:32 PM
 */
//ini_set('display_errors', 'on');
//set_time_limit(1000);
//ini_set('max_execution_time',80000);
//session_start();



$backwardseperator = "../../../../../";

include_once $_SESSION['ROOT_PATH'].'dataAccess/Connector.php';
/*
 * CREATE DB CONNECTION
 */
//$servername = $_SESSION['Server'];
//$username = $_SESSION['UserName'];
//$password = $_SESSION['Password'];
//$dbname = $_SESSION['Database'];

$dispNo 				= $_REQUEST['serialNo'];
$dispYear 				= $_REQUEST['year'];



// Create connection
//$conn = new mysqli($servername, $username, $password, $dbname);
////mysql_set_charset('utf8',$conn);
//// Check connection
//if ($conn->connect_error) {
//    die("Connection failed: " . $conn->connect_error);
//}


 
$body	=ob_start();
include "rptFabricDispatchNote_header.php";

$result= getReportDispatch($dispNo,$dispYear);


include "rptFabricDispatchNote_details.php";

//mysqli_close($conn);


echo $body 			= ob_get_clean();




function getReportDispatch($dispNo,$dispYear) {

    global $db;
    $db->OpenConnection();
    $sql="
        SELECT
ware_fabricdispatchheader.intBulkDispatchNo,
ware_fabricdispatchheader.intBulkDispatchNoYear,
ware_fabricdispatchdetails.strSize,
ware_fabricdispatchdetails.strLineNo,
(SELECT 

	trn_orderheader.strCustomerPoNo

FROM
	trn_orderheader
INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
WHERE
trn_orderheader.intOrderNo = ware_fabricdispatchheader.intOrderNo AND
trn_orderheader.intOrderYear = ware_fabricdispatchheader.intOrderYear AND
trn_orderdetails.intSalesOrderId = ware_fabricdispatchdetails.intSalesOrderId 
) AS CustomerPO,
(SELECT 

	trn_orderdetails.ITEM_CODE_FROM_CUSTOMER

FROM
	trn_orderheader
INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
WHERE
trn_orderheader.intOrderNo = ware_fabricdispatchheader.intOrderNo AND
trn_orderheader.intOrderYear = ware_fabricdispatchheader.intOrderYear AND
trn_orderdetails.intSalesOrderId = ware_fabricdispatchdetails.intSalesOrderId 
) AS MaterialCode,

(SELECT 

	trn_orderdetails.strSalesOrderNo

FROM
	trn_orderheader
INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
WHERE
trn_orderheader.intOrderNo = ware_fabricdispatchheader.intOrderNo AND
trn_orderheader.intOrderYear = ware_fabricdispatchheader.intOrderYear AND
trn_orderdetails.intSalesOrderId = ware_fabricdispatchdetails.intSalesOrderId 
) AS SalesOrderNo,
ware_fabricdispatchdetails.intSalesOrderId,
ware_fabricdispatchdetails.dblGoodQty,
ware_fabricdispatchdetails.strCutNo,
(
	SELECT
GROUP_CONCAT(ware_fabricdispatchdetails_barcode_wise.BARCODE SEPARATOR '/')

FROM
ware_fabricdispatchdetails_barcode_wise
WHERE
ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND
ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear AND
ware_fabricdispatchdetails_barcode_wise.intSalesOrderId =  ware_fabricdispatchdetails.intSalesOrderId

) AS Barcode,

(SELECT 

	trn_orderdetails.dblPrice

FROM
	trn_orderheader
INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
WHERE
trn_orderheader.intOrderNo = ware_fabricdispatchheader.intOrderNo AND
trn_orderheader.intOrderYear = ware_fabricdispatchheader.intOrderYear AND
trn_orderdetails.intSalesOrderId = ware_fabricdispatchdetails.intSalesOrderId 
) AS PerValue
FROM
ware_fabricdispatchheader
INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
WHERE
 ware_fabricdispatchheader.intBulkDispatchNo = '$dispNo' AND ware_fabricdispatchheader.intBulkDispatchNoYear ='$dispYear'";


    $result =$db->RunQuery2($sql);
    return $result;
}




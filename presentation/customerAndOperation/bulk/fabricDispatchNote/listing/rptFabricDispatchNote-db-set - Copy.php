<?php 
//////////////////////////////////////////////
//Create By:H.B.G Korala
//note://if final confirmation raised,insert into stocktransaction table.
/////////////////////////////////////////////

	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];

	include "{$backwardseperator}dataAccess/Connector.php";
	
	$programName='Fabric Received Note';
	$programCode='P0045';
	$fabRcvApproveLevel = (int)getApproveLevel($programName);
	
	/////////// parameters /////////////////////////////

	
	$serialNo = $_REQUEST['serialNo'];
	$year = $_REQUEST['year'];
	
	if($_REQUEST['status']=='approve')
	{
		$sqlChk = "SELECT ware_fabricdispatchheader.intStatus, 
		ware_fabricdispatchheader.intApproveLevels,  
		ware_fabricdispatchheader.intCompanyId AS location, 
		mst_locations.intCompanyId AS company 
		FROM 
		ware_fabricdispatchheader 
		Inner Join mst_locations ON ware_fabricdispatchheader.intCompanyId = mst_locations.intId
		WHERE (intBulkDispatchNo='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
		$resultChk = $db->RunQuery($sqlChk);
		$rowChk = mysqli_fetch_array($resultChk);
		$statusChk = $rowChk['intStatus'];
		
		if($statusChk!=1){
			 
			 
		 $sql = "UPDATE `ware_fabricdispatchheader` SET `intStatus`=intStatus-1 WHERE (intBulkDispatchNo='$serialNo') AND (`intBulkDispatchNoYear`='$year') and intStatus not in(0,1)";
		 $result = $db->RunQuery($sql);
		
		if($result){
			$sql = "SELECT ware_fabricdispatchheader.intStatus, 
			ware_fabricdispatchheader.intApproveLevels,  
			ware_fabricdispatchheader.intCompanyId AS location, 
			mst_locations.intCompanyId AS company 
			FROM 
			ware_fabricdispatchheader 
			Inner Join mst_locations ON ware_fabricdispatchheader.intCompanyId = mst_locations.intId
			WHERE (intBulkDispatchNo='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
			$results = $db->RunQuery($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			$company = $row['company'];
			$location = $row['location'];
			
			$approveLevel=$savedLevels+1-$status;
			$sqlI = "INSERT INTO `ware_fabricdispatchheader_approvedby` (`intBulkDispatchNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate) 
				VALUES ('$serialNo','$year','$approveLevel','$userId',now())";
			$resultI = $db->RunQuery($sqlI);

		//--------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
			$sql1 = "SELECT
				ware_fabricdispatchheader.intOrderNo,
				ware_fabricdispatchheader.intOrderYear,
				ware_fabricdispatchdetails.strCutNo,
				ware_fabricdispatchdetails.intSalesOrderId,
				ware_fabricdispatchdetails.intPart, 
				ware_fabricdispatchdetails.strSize,
				ware_fabricdispatchdetails.dblSampleQty,
				ware_fabricdispatchdetails.dblGoodQty,
				ware_fabricdispatchdetails.dblEmbroideryQty,
				ware_fabricdispatchdetails.dblPDammageQty,
				ware_fabricdispatchdetails.dblFDammageQty,
				ware_fabricdispatchdetails.dblCutRetQty 
				FROM
				ware_fabricdispatchdetails
				Inner Join ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
				WHERE
				ware_fabricdispatchdetails.intBulkDispatchNo =  '$serialNo' AND
				ware_fabricdispatchdetails.intBulkDispatchNoYear =  '$year'";
				$result1 = $db->RunQuery($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$orderNo=$row['intOrderNo'];
					$orderYear=$row['intOrderYear'];
					$cutNo=$row['strCutNo'];
					$part=$row['intPart'];
					$salesOrderId=$row['intSalesOrderId'];
					$size=$row['strSize'];
					$sampleQty=round($row['dblSampleQty'],4);
					$goodQty=round($row['dblGoodQty'],4);
					$embroideryQty=round($row['dblEmbroideryQty'],4);
					$pDammageQty=round($row['dblPDammageQty'],4);
					$fDammageQty=round($row['dblFDammageQty'],4);
					$cutRetQty=round($row['dblCutRetQty'],4);
					$place ='Stores';
					
					if($sampleQty>0){
					$type = 'Dispatched_S';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','-$sampleQty','$type','$userId',now())";
					$resultI = $db->RunQuery($sql);
					}
					if($goodQty>0){
					$type = 'Dispatched_G';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','-$goodQty','$type','$userId',now())";
					$resultI = $db->RunQuery($sql);
					}
					if($embroideryQty>0){
					$type = 'Dispatched_E';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','-$embroideryQty','$type','$userId',now())";
					$resultI = $db->RunQuery($sql);
					}
					if($pDammageQty>0){
					$type = 'Dispatched_P';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','-$pDammageQty','$type','$userId',now())";
					$resultI = $db->RunQuery($sql);
					}
					if($fDammageQty>0){
					$type = 'Dispatched_F';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','-$fDammageQty','$type','$userId',now())";
					$resultI = $db->RunQuery($sql);
					}
					if($cutRetQty>0){
					$type = 'Dispatched_CUT_RET';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','-$cutRetQty','$type','$userId',now())";
					$resultI = $db->RunQuery($sql);
					}
					
					//BEGIN - ADD TO SALES ORDER INVOICE BALANCE TABLE {
					AddToBalanceTable($orderYear,$orderNo,$salesOrderId,$goodQty);	
					//END	- ADD TO SALES ORDER INVOICE BALANCE TABLE }
				}
			}
		//--------------------------------------------------------------------
			if($status==$savedLevels){//if first confirmation raised,requisition table shld update
			}
		//---------------------------------------------------------------------------
		}
	  }// end if statusChk!=1
		
	}
	else if($_REQUEST['status']=='reject')
	{
		//$grnApproveLevel = (int)getApproveLevel($programName);
		$sql = "SELECT ware_fabricdispatchheader.intStatus,ware_fabricdispatchheader.intApproveLevels FROM ware_gatepassheader WHERE  (intBulkDispatchNo='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
		$results = $db->RunQuery($sql);
		$row = mysqli_fetch_array($results);
		$status = $row['intStatus'];
		$savedLevels = $row['intApproveLevels'];
		
		$sql = "UPDATE `ware_fabricdispatchheader` SET `intStatus`=0 WHERE (intBulkDispatchNo='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
		$result = $db->RunQuery($sql);
		
		$sql = "DELETE FROM `ware_fabricdispatchheader_approvedby` WHERE (`intBulkDispatchNo`='$serialNo') AND (`intYear`='$year')";
		$result2 = $db->RunQuery($sql);
		
		$sqlI = "INSERT INTO `ware_fabricdispatchheader_approvedby` (`intBulkDispatchNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate) 
			VALUES ('$serialNo','$year','0','$userId',now())";
		$resultI = $db->RunQuery($sqlI);
		
		//---------------------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
			$sql1 = "SELECT
				ware_fabricdispatchheader.intOrderNo,
				ware_fabricdispatchheader.intOrderYear,
				ware_fabricdispatchdetails.strCutNo,
				ware_fabricdispatchdetails.intSalesOrderId,
				ware_fabricdispatchdetails.intPart, 
				ware_fabricdispatchdetails.strSize,
				ware_fabricdispatchdetails.dblSampleQty,
				ware_fabricdispatchdetails.dblGoodQty,
				ware_fabricdispatchdetails.dblEmbroideryQty,
				ware_fabricdispatchdetails.dblPDammageQty,
				ware_fabricdispatchdetails.dblFDammageQty,
				ware_fabricdispatchdetails.dblCutRetQty 
				FROM
				ware_fabricdispatchdetails
				Inner Join ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
				WHERE
				ware_fabricdispatchdetails.intBulkDispatchNo =  '$serialNo' AND
				ware_fabricdispatchdetails.intBulkDispatchNoYear =  '$year'";
				$result1 = $db->RunQuery($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$orderNo=$row['intOrderNo'];
					$orderYear=$row['intOrderYear'];
					$cutNo=$row['strCutNo'];
					$part=$row['intPart'];
					$salesOrderId=$row['intSalesOrderId'];
					$size=$row['strSize'];
					$sampleQty=round($row['dblSampleQty'],4);
					$goodQty=round($row['dblGoodQty'],4);
					$embroideryQty=round($row['dblEmbroideryQty'],4);
					$pDammageQty=round($row['dblPDammageQty'],4);
					$fDammageQty=round($row['dblFDammageQty'],4);
					$cutRetQty=round($row['dblCutRetQty'],4);
					$place ='Stores';
					
					if($sampleQty>0){
					$type = 'Dispatched_S';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','$sampleQty','$type','$userId',now())";
					$resultI = $db->RunQuery($sql);
					}
					if($goodQty>0){
					$type = 'Dispatched_G';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','$goodQty','$type','$userId',now())";
					$resultI = $db->RunQuery($sql);
					}
					if($embroideryQty>0){
					$type = 'Dispatched_E';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','$embroideryQty','$type','$userId',now())";
					$resultI = $db->RunQuery($sql);
					}
					if($pDammageQty>0){
					$type = 'Dispatched_P';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','$pDammageQty','$type','$userId',now())";
					$resultI = $db->RunQuery($sql);
					}
					if($fDammageQty>0){
					$type = 'Dispatched_F';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','$fDammageQty','$type','$userId',now())";
					$resultI = $db->RunQuery($sql);
					}
				}
			}
		//---------------------------------------------------------------------------------
			if($savedLevels>=$status){//if atleast one confirmation processed, roll back requisition table-
			}
		//-----------------------------------------------------------------------------
	}
//--------------------------------------------------------------

function AddToBalanceTable($orderYear,$orderNo,$salesOrderId,$goodQty)
{
	global $db;
	$booAvailable	= false;
	$sql = "SELECT COUNT(*) FROM finance_customer_invoice_balance 
			WHERE ORDER_YEAR = '$orderYear' 
			AND ORDER_NO = '$orderNo' 
			AND SALES_ORDER_ID = '$salesOrderId'";
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_row($result))
	{
		$booAvailable	= true;
	}
	
	if($booAvailable)
	{
		$sql = "INSERT INTO finance_customer_invoice_balance
					(ORDER_YEAR,
					ORDER_NO,
					SALES_ORDER_ID,
					DISPATCHED_GOOD_QTY,
					INVOICE_BALANCE)
				VALUES ('$orderYear',
					'$orderNo',
					'$salesOrderId',
					'$goodQty',
					'$goodQty');";
		$result = $db->RunQuery($sql);
	}
	else
	{
		$sql = "UPDATE finance_customer_invoice_balance
				SET DISPATCHED_GOOD_QTY = DISPATCHED_GOOD_QTY + $goodQty ,
				  INVOICE_BALANCE = INVOICE_BALANCE + $goodQty
				WHERE ORDER_YEAR = '$orderYear'
					AND ORDER_NO = '$orderNo'
					AND SALES_ORDER_ID = '$salesOrderId';";
		$result = $db->RunQuery($sql);
	}
}
?>
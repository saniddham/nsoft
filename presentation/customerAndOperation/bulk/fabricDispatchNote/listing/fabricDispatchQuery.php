<?php
// Developed By Krishantha Jayasingha

include_once "../../../../../config.php";
include_once "../../../../../dataAccess/DBManager2.php";                                                $db = new DBManager2();
include_once "../../../../../class/tables/mst_customer.php";                                            $mst_customer = new mst_customer($db);
$requestData= $_REQUEST;
$location 		=  $_SESSION['CompanyID'];
$headCompanyId 		=  $_SESSION['headCompanyId'];
$intUser  		= $_SESSION['userId'];

$programCode	= 'P1254';
$approveLevel 	= (int)getMaxApproveLevel();

$GetCountSql = getQuery($type = 'count');

$db->connect();
$db->begin();
// getting total number records without any search
//$sql = "SELECT COUNT(*) AS COUNT FROM ware_fabricdispatchheader";
$result1 = $db->RunQuery($GetCountSql);
//$query=mysqli_query($conn, $sql) or die("employee-grid-data.php: get employees");
$totalData = $result1[0]["COUNT"];
$GetAll = getQuery($type = 'all');
$result1 = $db->RunQuery($GetAll);
$data = array();
while( $row=mysqli_fetch_array($result1) ) {  // preparing an array
    $nestedData=array();
    $nestedData[] = $row["Status"];
    $nestedData[] = $row["Dispatch_No"];
    $nestedData[] = $row["Dispatch_Year"];
    $nestedData[] = $row["month"];
    //$nestedData[] = DateTime::createFromFormat('!m', $row["month"])->format('F').$row["month"];
    $nestedData[] = $row["Marketer"];
    $nestedData[] = $row["Customer"];
    $nestedData[] = $row["strGraphicNo"];
    $nestedData[] = $row["strStyleNo"];
    $nestedData[] = $row["Order_No"];
    $nestedData[] = $row["strSalesOrderNo"];
    $nestedData[] = $row["SampleQty"];
    $nestedData[] = $row["GoodQty"];
    $nestedData[] = $row["EmbroideryQty"];
    $nestedData[] = $row["PDammageQty"];
    $nestedData[] = $row["FdammageQty"];
    $nestedData[] = $row["dblCutRetQty"];
    $nestedData[] = $row["Date"];
    $nestedData[] = $row["1st_Approval"];
    $nestedData[] = $row["2nd_Approval"];
    $nestedData[] = $row["3rd_Approval"];
    $nestedData[] = '<a target="blank" href="?q=919&serialNo='.$row["Dispatch_No"].'&year='.$row["Dispatch_Year"].'">'.$row["View"].'<a/>';

    $data[] = $nestedData;
}


$json_data = array(
    "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
    "recordsTotal"    => intval( $totalData ),  // total number of records
    "recordsFiltered" => intval( $totalData ), // total number of records after searching, if there is no searching then totalFiltered = totalData
    "data"            => $data   // total data array
);

echo json_encode($json_data);  // send data as json format

function getMaxApproveLevel()
{
    global $db;

    $appLevel=0;
    $sqlp = "SELECT
			Max(ware_fabricdispatchheader.intApproveLevels) AS appLevel
			FROM ware_fabricdispatchheader";
    $resultp = $db->RunQuery($sqlp);
    $rowp=mysqli_fetch_array($resultp);

    return $rowp['appLevel'];
}

function getQuery($type){
    // Getting Global variables
    $programCode = $GLOBALS['programCode'];
    $intUser = $GLOBALS['intUser'];
    $approveLevel = $GLOBALS['approveLevel'];
    $requestData = $GLOBALS['requestData'];
    $location = $GLOBALS['location'];
    $headCompanyId = $GLOBALS['headCompanyId'];
    $sql = '';
    if($type == 'count'){
        $sql = "select  * from(SELECT COUNT(*) AS COUNT,";
    }
    else{
        $sql = "select * from(SELECT DISTINCT";
    }

    $sql .= "
							if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected','Pending')) as Status,
							tb1.intBulkDispatchNo as `Dispatch_No`,
							tb1.intBulkDispatchNoYear as `Dispatch_Year`,
							tb1.dtmCreateDate as `Date`,
							MONTHNAME(tb1.dtmCreateDate) as `month`,

							(select  group_concat(strStyleNo separator ', ')  from  (select distinct strStyleNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
                            tb.intOrderYear =  tb1.intOrderYear

							) as strStyleNo,

							(select  group_concat(strGraphicNo separator ', ') from  (select distinct strGraphicNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
							tb.intOrderYear =  tb1.intOrderYear

							) as strGraphicNo,


							marketing.strUserName as Marketer,


							customer.strName as Customer,

							(select  group_concat(strSalesOrderNo separator ', ') from  (select distinct strSalesOrderNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
                            tb.intOrderYear =  tb1.intOrderYear

							) as strSalesOrderNo,

							IFNULL((SELECT
								Sum(ware_fabricdispatchdetails.dblSampleQty)
								FROM
								ware_fabricdispatchdetails
								WHERE
								ware_fabricdispatchdetails.intBulkDispatchNo  =   tb1.intBulkDispatchNo AND
                                ware_fabricdispatchdetails.intBulkDispatchNoYear =  tb1.intBulkDispatchNoYear

							),0) as SampleQty,

							IFNULL((SELECT
								Sum(ware_fabricdispatchdetails.dblGoodQty)
								FROM
								ware_fabricdispatchdetails
								WHERE
								ware_fabricdispatchdetails.intBulkDispatchNo  =   tb1.intBulkDispatchNo AND
                                ware_fabricdispatchdetails.intBulkDispatchNoYear =  tb1.intBulkDispatchNoYear

							),0) as GoodQty,

							IFNULL((SELECT
								Sum(ware_fabricdispatchdetails.dblEmbroideryQty)
								FROM
								ware_fabricdispatchdetails
								WHERE
								ware_fabricdispatchdetails.intBulkDispatchNo  =   tb1.intBulkDispatchNo AND
                                ware_fabricdispatchdetails.intBulkDispatchNoYear =  tb1.intBulkDispatchNoYear

							),0) as EmbroideryQty,

							IFNULL((SELECT
								Sum(ware_fabricdispatchdetails.dblPDammageQty)
								FROM
								ware_fabricdispatchdetails
								WHERE
								ware_fabricdispatchdetails.intBulkDispatchNo  =   tb1.intBulkDispatchNo AND
                                ware_fabricdispatchdetails.intBulkDispatchNoYear =  tb1.intBulkDispatchNoYear

							),0) as PDammageQty,

							IFNULL((SELECT
								Sum(ware_fabricdispatchdetails.dblFdammageQty)
								FROM
								ware_fabricdispatchdetails
								WHERE
								ware_fabricdispatchdetails.intBulkDispatchNo  =   tb1.intBulkDispatchNo AND
                                ware_fabricdispatchdetails.intBulkDispatchNoYear =  tb1.intBulkDispatchNoYear

							),0) as FdammageQty,

							IFNULL((SELECT
								Sum(ware_fabricdispatchdetails.dblCutRetQty)
								FROM
								ware_fabricdispatchdetails
								WHERE
								ware_fabricdispatchdetails.intBulkDispatchNo  =   tb1.intBulkDispatchNo AND
                                ware_fabricdispatchdetails.intBulkDispatchNoYear =  tb1.intBulkDispatchNoYear

							),0) as dblCutRetQty,

							sys_users.strUserName as User,
							tb1.intApproveLevels,
							tb1.intStatus,
							concat(tb1.intOrderNo,'/',tb1.intOrderYear) as Order_No,
							IFNULL((
                            SELECT
								concat(sys_users.strUserName,'(',max(ware_fabricdispatchheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_fabricdispatchheader_approvedby
								Inner Join sys_users ON ware_fabricdispatchheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_fabricdispatchheader_approvedby.intBulkDispatchNo  = tb1.intBulkDispatchNo AND
                                ware_fabricdispatchheader_approvedby.intYear =  tb1.intBulkDispatchNoYear AND
                                ware_fabricdispatchheader_approvedby.intApproveLevelNo =  '1'
							),IF(((SELECT
								menupermision.int1Approval
								FROM menupermision
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
                                menupermision.intUserId =  '$intUser')=1 AND tb1.intStatus>1),'Approve', '')) as `1st_Approval`,  ";

    for($i=2; $i<=$approveLevel; $i++){

        if($i==2){
            $approval="2nd_Approval";
        }
        else if($i==3){
            $approval="3rd_Approval";
        }
        else {
            $approval=$i."th_Approval";
        }


        $sql .= "IFNULL(
/*condition*/
    (
    SELECT
								concat(sys_users.strUserName,'(',max(ware_fabricdispatchheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_fabricdispatchheader_approvedby
								Inner Join sys_users ON ware_fabricdispatchheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_fabricdispatchheader_approvedby.intBulkDispatchNo  = tb1.intBulkDispatchNo AND
                                ware_fabricdispatchheader_approvedby.intYear =  tb1.intBulkDispatchNoYear AND
                                ware_fabricdispatchheader_approvedby.intApproveLevelNo =  '$i'
							),
							/*end condition*/

							/*false part*/

							IF(
                                /*if condition */
                            ((SELECT
								menupermision.int".$i."Approval
								FROM menupermision
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
                                menupermision.intUserId =  '$intUser')=1 AND (tb1.intStatus>1) AND (tb1.intStatus<=tb1.intApproveLevels) AND ((SELECT
								max(ware_fabricdispatchheader_approvedby.dtApprovedDate)
								FROM
								ware_fabricdispatchheader_approvedby
								Inner Join sys_users ON ware_fabricdispatchheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_fabricdispatchheader_approvedby.intBulkDispatchNo  = tb1.intBulkDispatchNo AND
                                ware_fabricdispatchheader_approvedby.intYear =  tb1.intBulkDispatchNoYear AND
                                ware_fabricdispatchheader_approvedby.intApproveLevelNo =  ($i-1))<>'')),

								/*end if condition*/
								/*if true part*/
								'Approve',
								/*false part*/
								 if($i>tb1.intApproveLevels,'-----',''))

								/*end IFNULL false part*/
								) as `".$approval."`, ";

    }

    $sql .= " 'View' as `View`
						FROM
						ware_fabricdispatchheader as tb1
						Inner Join mst_locations ON tb1.intCompanyId = mst_locations.intId
						Inner Join sys_users ON tb1.intCteatedBy = sys_users.intUserId
						Inner Join trn_orderheader as tb2 ON tb1.intOrderNo = tb2.intOrderNo AND tb1.intOrderYear = tb2.intOrderYear
						Inner Join sys_users  AS marketing ON tb2.intMarketer = marketing.intUserId
						Inner Join mst_customer  AS customer ON tb2.intCustomer = customer.intId
						Inner Join (select distinct intOrderNo,intOrderYear,strGraphicNo from trn_orderdetails )  AS orderdetails ON tb1.intOrderNo = orderdetails.intOrderNo AND tb1.intOrderYear = orderdetails.intOrderYear
						 WHERE 1=1 AND mst_locations.intCompanyId ='$headCompanyId'";

    if(!empty($requestData["columns"])){
        $having ='';
        foreach($requestData["columns"] AS $columnData){
			//status pending
			if($columnData['name']=='Status' && $columnData['search']['value'] != "*"){

                $sql .= " AND tb1.intStatus > 1 AND tb1.intStatus <= (tb1.intApproveLevels+1) ";
            }

            //status all
			else if($columnData['name']=='Status' && $columnData['search']['value'] != ""){
                $sql .= " AND tb1.intStatus = '".$columnData['search']['value']."'";
            }

            if($columnData['name']=='Dispatch' && $columnData['search']['value'] != ""){
                $sql .= " AND tb1.intBulkDispatchNo LIKE '%".$columnData['search']['value']."%'";
            }
            //all month added  //Hasitha 2017/06/12
            if($columnData['name']=='Month' && $columnData['search']['value'] != "" && $columnData['search']['value'] !='All'){
                $sql .= " AND MONTHNAME(tb1.dtmCreateDate) LIKE '".$columnData['search']['value']."%'";
            }
            if($columnData['name']=='Marketer' && $columnData['search']['value'] != ""){
                $sql .= " AND marketing.strUserName LIKE '%".$columnData['search']['value']."%'";
            }
            if($columnData['name']=='Customer' && $columnData['search']['value'] != ""){
                $sql .= " AND customer.strName LIKE '%".$columnData['search']['value']."%'";
            }
            if($columnData['name']=='DispatchYear' && $columnData['search']['value'] != ""){
				
                $sql .= " AND tb1.intBulkDispatchNoYear LIKE '".$columnData['search']['value']."%'";
				
            }
       
            // Having Lsit

            if($columnData['name']=='GraphicNO' && $columnData['search']['value'] != ""){
                $having .= " AND strGraphicNo LIKE '%".$columnData['search']['value']."%'";
            }
            if($columnData['name']=='OrderNO' && $columnData['search']['value'] != ""){
                $having .= " AND Order_No LIKE '%".$columnData['search']['value']."%'";
            }
            if($columnData['name']=='StyleNo' && $columnData['search']['value'] != ""){
                $having .= " AND strStyleNo LIKE '%".$columnData['search']['value']."%'";
            }
            if($columnData['name']=='SalesOrderNo' && $columnData['search']['value'] != ""){
                $having .= " AND strSalesOrderNo LIKE '%".$columnData['search']['value']."%'";
            }
            if($columnData['name']=='SampleQty' && $columnData['search']['value'] != ""){
                $having .= " AND SampleQty LIKE '%".$columnData['search']['value']."%'";
            }
            if($columnData['name']=='GoodQty' && $columnData['search']['value'] != ""){
                $having .= " AND GoodQty LIKE '%".$columnData['search']['value']."%'";
            }
            if($columnData['name']=='EM-DQty' && $columnData['search']['value'] != ""){
                $having .= " AND EmbroideryQty LIKE '%".$columnData['search']['value']."%'";
            }
            if($columnData['name']=='P-DQty' && $columnData['search']['value'] != ""){
                $having .= " AND PDammageQty LIKE '%".$columnData['search']['value']."%'";
            }
            if($columnData['name']=='F-DQty' && $columnData['search']['value'] != ""){
                $having .= " AND FdammageQty LIKE '%".$columnData['search']['value']."%'";
            }
            if($columnData['name']=='Cutreturn' && $columnData['search']['value'] != ""){
                $having .= " AND dblCutRetQty LIKE '%".$columnData['search']['value']."%'";
            }
            if($columnData['name']=='Date' && $columnData['search']['value'] != ""){
                $having .= " AND Date LIKE '%".$columnData['search']['value']."%'";
            }
            if($columnData['name']=='1stApproval' && $columnData['search']['value'] != ""){
                $having .= " AND 1st_Approval LIKE '%".$columnData['search']['value']."%'";
            }
            if($columnData['name']=='2ndApproval' && $columnData['search']['value'] != ""){
                $having .= " AND 2nd_Approval LIKE '%".$columnData['search']['value']."%'";
            }
            if($columnData['name']=='3rdApproval' && $columnData['search']['value'] != ""){
                $having .= " AND 3rd_Approval LIKE '%".$columnData['search']['value']."%'";
            }

        }

    }
if($having != '') {
    $sql.=" HAVING 1=1 ".$having;
}
    if($type == 'count'){

    }
    else{
        $sql.=" LIMIT ".$requestData['start']." ,".$requestData['length']."";
    }

    $sql .=	" )  as t where 1=1";
    return $sql;

}

?>

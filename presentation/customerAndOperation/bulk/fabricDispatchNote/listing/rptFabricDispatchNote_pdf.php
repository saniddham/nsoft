<?php
session_start();

$backwardseperator = "../../../../../";

//BEGIN - SESSIONS VARIABLES {
$companyId 		= $_SESSION['CompanyID'];
$intUser  		= $_SESSION["userId"];
$mainPath 		= $_SESSION['mainPath'];
$thisFilePath 	= $_SERVER['PHP_SELF'];
//END 	- SESSIONS VARIABLES }

//BEGIN - INCLUDE FILES {
require_once ("{$backwardseperator}dataAccess/Connector.php");
require_once ("../../../../../libraries/fpdf/fpdf.php");
//END 	- INCLUDE FILES }

//BEGIN - SET PARAMETERS {
$serialNo 				= $_REQUEST['serialNo'];
$year 					= $_REQUEST['year'];
$approveMode 			= $_REQUEST['approveMode'];
$programName			= 'Dispatch Note';
$programCode			= 'P0470';
//$FabRecvApproveLevel 	= (int)getApproveLevel($programName);
//END 	- SET PARAMETERS }


//BEGIN - CLASS {
class PDF extends FPDF
{
	function Header()
	{
		global $rowCount;
		global $title;
		global $serialNo;
		global $year;
		global $style;
		global $graphicNo;
		global $custPO;
		global $orderNo;
		global $orderYear;
		global $customer;
		global $date;
		global $user;
		global $customerLocation;
		global $contactPerson;
		
		$h = 4;
		$border	= 0;
		$a = GetCompanyDetails();
		$this->Image('../../../../../images/logo_sm.png', 18, 12);		
		$this->SetFont('Times','B',10);$this->Cell(0,$h,$a[0],$border,1,'C');
		$this->SetFont('Times','',6);$this->Cell(0,$h,$a[1],$border,1,'C');
		$this->SetFont('Times','',6);$this->Cell(0,$h,$a[2],$border,1,'C');
		$this->SetFont('Times','',6);$this->Cell(0,$h,$a[3],$border,1,'C');
		$this->SetXY(10,30);$this->SetFont('Times','B',11);$this->Cell(0,7,$a[4],$border,1,'C');
		
		$this->SetXY(10,40);
		$this->SetFont('Times','B',6);$this->Cell(25,5,"Fabric Dispatch No",$border,0,'L');
		$this->SetFont('Times','B',6);$this->Cell(2,5,":",$border,0,'C');
		$this->SetFont('Times','',6);$this->Cell(30,5,$serialNo.'/'.$year,$border,0,'L');
		$this->SetFont('Times','B',6);$this->Cell(20,5,"Order No",$border,0,'L');
		$this->SetFont('Times','B',6);$this->Cell(2,5,":",$border,0,'C');
		$this->SetFont('Times','',6);$this->Cell(30,5,$orderNo.'/'.$orderYear,$border,0,'L');
		$this->SetFont('Times','B',6);$this->Cell(20,5,"Customer",$border,0,'L');
		$this->SetFont('Times','B',6);$this->Cell(2,5,":",$border,0,'C');
		$this->SetFont('Times','',6);$this->Cell(30,5,$customer,$border,0,'L');
		
		$this->SetXY(10,45);
		$this->SetFont('Times','B',6);$this->Cell(25,5,"Customer PO",$border,0,'L');
		$this->SetFont('Times','B',6);$this->Cell(2,5,":",$border,0,'C');
		$this->SetFont('Times','',6);$this->Cell(30,5,$custPO,$border,0,'L');
		$this->SetFont('Times','B',6);$this->Cell(20,5,"By",$border,0,'L');
		$this->SetFont('Times','B',6);$this->Cell(2,5,":",$border,0,'C');
		$this->SetFont('Times','',6);$this->Cell(30,5,$user,$border,0,'L');
		$this->SetFont('Times','B',6);$this->Cell(20,5,"Location",$border,0,'L');
		$this->SetFont('Times','B',6);$this->Cell(2,5,":",$border,0,'C');
		$this->SetFont('Times','',6);$this->Cell(30,5,$customerLocation,$border,0,'L');		
		
		$this->SetXY(10,50);
		$this->SetFont('Times','B',6);$this->Cell(25,5,"Contact Person",$border,0,'L');
		$this->SetFont('Times','B',6);$this->Cell(2,5,":",$border,0,'C');
		$this->SetFont('Times','',6);$this->Cell(30,5,$contactPerson,$border,0,'L');
		$this->SetFont('Times','B',6);$this->Cell(20,5,"Date",$border,0,'L');
		$this->SetFont('Times','B',6);$this->Cell(2,5,":",$border,0,'C');
		$this->SetFont('Times','',6);$this->Cell(30,5,$date,$border,0,'L');
		
	}
	
	function CreateTable($result,$rowCount,$fieldCount)
	{
		global $totvalue_1;
		global $totvalue_2;		
		global $totvalue_3;
		global $totvalue_4;
		global $totvalue_5;
		global $totvalue_6;
		global $totvalue_7;
		
		$count 			= 1;
		$booFirst		= true;
		$totvalue_1 	= 0;
		$totvalue_2 	= 0;
		$totvalue_3 	= 0;
		$totvalue_4 	= 0;
		$totvalue_5 	= 0;
		$totvalue_6 	= 0;
		$totvalue_7 	= 0;
		
		$this->Table_header();
		while($row=mysqli_fetch_array($result))
		{	
			$this->Table_Body($row,$count);	
			if($count == 30){
				$this->Table_Footer();
				$this->AddPage();
				$this->Table_header();
					$count 			= 0;	
					$totvalue_1 	= 0;
					$totvalue_2 	= 0;
					$totvalue_3 	= 0;
					$totvalue_4 	= 0;
					$totvalue_5 	= 0;
					$totvalue_6 	= 0;
					$totvalue_7 	= 0;
					$booFirst 		= false;		
			}			
			$count++;			
		}
		$this->Table_Footer();
	}
	
	function Table_header()
	{
		global $obj_getData;
		global $periodArray;
		$this->SetXY(5,55);
		$h = 5;
		$this->SetFont('Times','B',6);
		$this->Cell(15,$h,"Graphic No",'1',0,'C');
		$this->Cell(19,$h,"Style No",'1',0,'C');
		$this->Cell(20,$h,"S.O. No",'1',0,'C');
		$this->Cell(10,$h,"Part",'1',0,'C');
		$this->Cell(15,$h,"BG. Color",'1',0,'C');
		$this->Cell(10,$h,"Line No",'1',0,'C');
		$this->Cell(10,$h,"Cut No",'1',0,'C');
		$this->Cell(10,$h,"Size",'1',0,'C');
		$this->Cell(15,$h,"Sample Qty",'1',0,'C');
		$this->Cell(13,$h,"Good Qty",'1',0,'C');
		$this->Cell(15,$h,"EMD-D Qty",'1',0,'C');
		$this->Cell(10,$h,"P-D Qty",'1',0,'C');
		$this->Cell(10,$h,"F-D Qty",'1',0,'C');		
		$this->Cell(10,$h,"Cut Ret.",'1',0,'C');
		$this->Cell(12,$h,"Total Qty",'1',0,'C');
		$this->Cell(12,$h,"Remarks",'1',0,'C');
		$this->Ln();
	}
	
	function Table_Body($row,$count)
	{	
		$h	= 4.4;
		$this->SetFont('Times','',6);
		global $totvalue_1;
		global $totvalue_2;		
		global $totvalue_3;
		global $totvalue_4;
		global $totvalue_5;
		global $totvalue_6;
		global $totvalue_7;
		global $style;
		$this->SetX(5);
		$this->Cell(15,$h,$row["GraphicNo"],'1',0,'L');
		$this->Cell(19,$h,$row["StyleNo"],'1',0,'L');
		$this->Cell(20,$h,$row["strSalesOrderNo"],'1',0,'L');
		$this->Cell(10,$h,$row["part"],'1',0,'L');
		$this->Cell(15,$h,$row["bgcolor"],'1',0,'R');			
		$this->Cell(10,$h,$row["strLineNo"],'1',0,'L');
		$this->Cell(10,$h,$row["strCutNo"],'1',0,'L');
		$this->Cell(10,$h,$row["strSize"],'1',0,'R');
		$this->Cell(15,$h,number_format($row["dblSampleQty"],2),'1',0,'R');
		$this->Cell(13,$h,number_format($row["dblGoodQty"],2),'1',0,'R');
		$this->Cell(15,$h,number_format($row["dblEmbroideryQty"],2),'1',0,'R');
		$this->Cell(10,$h,number_format($row["dblPDammageQty"],2),'1',0,'R');
		$this->Cell(10,$h,number_format($row["dblFDammageQty"],2),'1',0,'R');		
		$this->Cell(10,$h,number_format($row["dblCutRetQty"],2),'1',0,'R');
		$this->Cell(12,$h,number_format($row["totalQty"],2),'1',0,'R');
		$this->Cell(12,$h,$row["strRemarks"],'1',0,'L');
		$this->Ln();
		
		$totvalue_1 	+= $row["dblSampleQty"];
		$totvalue_2 	+= $row["dblGoodQty"];
		$totvalue_3 	+= $row["dblEmbroideryQty"];
		$totvalue_4 	+= $row["dblPDammageQty"];
		$totvalue_5 	+= $row["dblFDammageQty"];
		$totvalue_7 	+= $row["dblCutRetQty"];
		$totvalue_6 	+= $row["totalQty"];
	}
	
	function Table_Footer()
	{
		$h	= 6;
		$this->SetFont('Times','B',6);
		global $totvalue_1;
		global $totvalue_2;		
		global $totvalue_3;
		global $totvalue_4;
		global $totvalue_5;
		global $totvalue_6;
		global $totvalue_7;
		
		global $headerRemarks;
		$this->SetX(5);
		$this->Cell(109,$h,'TOTAL','1',0,'C');
		$this->Cell(15,$h,number_format($totvalue_1,2),'1',0,'R');
		$this->Cell(13,$h,number_format($totvalue_2,2),'1',0,'R');
		$this->Cell(15,$h,number_format($totvalue_3,2),'1',0,'R');
		$this->Cell(10,$h,number_format($totvalue_4,2),'1',0,'R');
		$this->Cell(10,$h,number_format($totvalue_5,2),'1',0,'R');		
		$this->Cell(10,$h,number_format($totvalue_7,2),'1',0,'R');
		$this->Cell(12,$h,number_format($totvalue_6,2),'1',0,'R');
		$this->Cell(12,$h,$row["strRemarks"],'1',0,'L');
		$this->Ln();
		$this->Ln();
		//$this->SetXY(10,55);
		$this->SetFont('Times','B',8);$this->Cell(25,5,"Remarks",$border,0,'L');
		$this->SetFont('Times','B',8);$this->Cell(2,5,":",$border,0,'C');
		//$order   = array("\r\n", "\n", "\r");
		//$headerRemarks = str_replace($order,' ', $headerRemarks);
		$this->SetFont('Times','',7);$this->MultiCell(173,3,$headerRemarks,0);		
	}
	
	function Footer()
	{
		global $db;
		global $obj_company_get;
		global $session_companyId;
		global $headerRemarks;
		global $intStatus;
		global $serialNo;
		global $year;
		global $savedLevels;
		global $user;
		$x	= 0;
//
 	if($intStatus!=0)
	{
		for($i=1; $i<=$savedLevels; $i++)
		{
			   $sqlc = "SELECT
					ware_fabricdispatchheader_approvedby.intApproveUser,
					ware_fabricdispatchheader_approvedby.dtApprovedDate,
					sys_users.strUserName as UserName, 
					ware_fabricdispatchheader_approvedby.intApproveLevelNo
					FROM
					ware_fabricdispatchheader_approvedby
					Inner Join sys_users ON ware_fabricdispatchheader_approvedby.intApproveUser = sys_users.intUserId
					WHERE
					ware_fabricdispatchheader_approvedby.intBulkDispatchNo =  '$serialNo' AND
					ware_fabricdispatchheader_approvedby.intYear =  '$year'  AND
					ware_fabricdispatchheader_approvedby.intApproveLevelNo =  '$i'  order by intApproveLevelNo asc
	";					
			 $resultc = $db->RunQuery($sqlc);
			 $rowc = mysqli_fetch_array($resultc);
				if($i==1)
					$desc="1st ";
				else if($i==2)
					$desc="2nd ";
				else if($i==3)
					$desc="3rd ";
				else
					$desc=$i."th ";

			 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
			 if($rowc['UserName']=='')
				$desc2	= '---------------------------------';
			 
			 
				$this->SetXY(10,(-65+($savedLevels*-5))+$x);$this->SetFont('Times','',8);$this->Cell(60,5,$desc.' Approved By '.$desc2,0,'L');
				$x = $x+5;
	}
	}
	else{
				 $sqlc = "SELECT
						ware_fabricdispatchheader_approvedby.intApproveUser,
						ware_fabricdispatchheader_approvedby.dtApprovedDate,
						sys_users.strUserName as UserName,
						ware_fabricdispatchheader_approvedby.intApproveLevelNo
						FROM
						ware_fabricdispatchheader_approvedby
						Inner Join sys_users ON ware_fabricdispatchheader_approvedby.intApproveUser = sys_users.intUserId
						WHERE
						ware_fabricdispatchheader_approvedby.intBulkDispatchNo =  '$serialNo' AND
						ware_fabricdispatchheader_approvedby.intYear =  '$year'  AND
						ware_fabricdispatchheader_approvedby.intApproveLevelNo =  '0'";
				 $resultc = $db->RunQuery($sqlc);
				 $rowc=mysqli_fetch_array($resultc);
                            		
				$this->SetXY(10,-70);$this->SetFont('Times','',8);$this->Cell(60,5,'Rejected By'.$rowc['UserName']."(".$rowc['dtApprovedDate'].")",0,'L');

	}
		//
		/*$this->SetXY(10,-60);	
		$this->SetFont('Times','B',8);$this->Cell(20,5,"Remarks",'0',0,'L');
		$this->SetFont('Times','B',8);$this->Cell(2,5,":",'0',0,'C');
		$this->SetFont('Times','',8);$this->Cell(60,5,$headerRemarks,0,'L');*/

		$this->SetXY(30,-40);
		$this->SetFont('Times','B',8);$this->Cell(30,5,"Checked By",'0',0,'L');
		$this->SetFont('Times','B',8);$this->Cell(2,5,":",'0',0,'C');
		$this->SetFont('Times','',8);$this->Cell(60,5,'........................................',0,'L');
		$this->SetFont('Times','B',8);$this->Cell(30,5,"Confirmed By",'0',0,'L');
		$this->SetFont('Times','B',8);$this->Cell(2,5,":",'0',0,'C');
		$this->SetFont('Times','',8);$this->Cell(60,5,'........................................','0',0,'L');
		
		$this->SetXY(30,-50);
		$this->SetFont('Times','B',8);$this->Cell(30,5,"Delivered By",'0',0,'L');
		$this->SetFont('Times','B',8);$this->Cell(2,5,":",'0',0,'C');
		$this->SetFont('Times','',8);$this->Cell(60,5,'........................................',0,'L');
		$this->SetFont('Times','B',8);$this->Cell(30,5,"Prepared By",'0',0,'L');
		$this->SetFont('Times','B',8);$this->Cell(2,5,":",'0',0,'C');
		$this->SetFont('Times','',8);$this->Cell(60,5,$user,'0',0,'L');
		
		$this->SetY(-30);		
		$this->SetFont('Times','B',8);$this->Cell(0,2,'If You Have Any Discrepancies With Regard To Above Quantities, Please Reply Within Twenty Four Hours. Otherwise This Will Be Taken
As Confirm.',0,0,'C');
		
		$this->SetY(-15);		
		$this->SetFont('Times','',8);$this->Cell(0,2,'Printed Date '.date('Y/m/d'),0,0,'C');
				
		$this->SetY(-10);
		$this->SetFont('Times','I',8);
		$this->Cell(0,2,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}
	
	function SetPageWaterMark()
	{
		//Put the watermark
		$this->SetFont('Times','B',50);
		$this->SetTextColor(255,192,203);
		$this->RotatedText(35,190,'PENDING APPROVAL',45);
	}
	
	function RotatedText($x, $y, $txt, $angle)
	{
		//Text rotated around its origin
		$this->Rotate($angle,$x,$y);
		$this->Text($x,$y,$txt);
		$this->Rotate(0);
	}
}
//END 	- CLASS }
$pdf 		= new PDF('P','mm','letter');

$result 	= GetMainHeaderDetails($year,$serialNo);
$result 	= GetMainDetails($year,$serialNo);
$rowCount 	= mysqli_num_rows($result);
$fieldCount = mysqli_num_fields($result);
$pdf->AliasNbPages();
$pdf->AddPage();
if($intStatus>1)
	$pdf->SetPageWaterMark();

$pdf->SetTextColor(0,0,0);
$pdf->CreateTable($result,$rowCount,$fieldCount);
$pdf->Output('rptFabricDispatchNote_pdf.pdf','I');

//BEGIN - INTERNAL FUNCTIONS {
function GetMainHeaderDetails($year,$serialNo)
{
	global $db;
	global $locationId;
	global $headerRemarks;
	global $intStatus;
	global $savedLevels;
	global $style;
	global $graphicNo;
	global $custPO;
	global $orderNo;
	global $orderYear;
	global $customer;
	global $date;
	global $user;
	global $customerLocation;
	global $contactPerson;
	
	  $sql = "SELECT 
ware_fabricdispatchheader.intBulkDispatchNo,
ware_fabricdispatchheader.intBulkDispatchNoYear, 
trn_orderdetails.strStyleNo,
trn_orderdetails.strGraphicNo, 
trn_orderheader.strCustomerPoNo,
ware_fabricdispatchheader.intOrderNo,
ware_fabricdispatchheader.intOrderYear,
ware_fabricdispatchheader.strRemarks,
ware_fabricdispatchheader.intStatus,
ware_fabricdispatchheader.intApproveLevels,
ware_fabricdispatchheader.dtmdate,
ware_fabricdispatchheader.dtmCreateDate,
trn_orderheader.strContactPerson, 
sys_users.strUserName,
mst_customer.strName as customer ,
ware_fabricdispatchheader.intCompanyId , 
mst_customer_locations_header.strName as customerLocation 
FROM
ware_fabricdispatchheader  
Inner Join trn_orderheader ON ware_fabricdispatchheader.intOrderNo = trn_orderheader.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderheader.intOrderYear
Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
left Join mst_customer ON trn_orderheader.intCustomer = mst_customer.intId 
left Join sys_users ON ware_fabricdispatchheader.intCteatedBy = sys_users.intUserId 
left Join mst_customer_locations ON ware_fabricdispatchheader.intCustLocation = mst_customer_locations.intLocationId AND  trn_orderheader.intCustomer = mst_customer_locations.intCustomerId 
Inner Join mst_customer_locations_header ON mst_customer_locations.intLocationId = mst_customer_locations_header.intId 
WHERE
ware_fabricdispatchheader.intBulkDispatchNo =  '$serialNo' AND
ware_fabricdispatchheader.intBulkDispatchNoYear =  '$year'
";
	$result=$db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$locationId 		= $row['intCompanyId'];
		$headerRemarks 		= $row['strRemarks'];
		$intStatus 			= $row['intStatus'];
		$savedLevels 		= $row['intApproveLevels'];
		$style 				= $row['strStyleNo'];
		$graphicNo			= $row['strGraphicNo'];
		$custPO 			= $row['strCustomerPoNo'];
		$orderNo 			= $row['intOrderNo'];
		$orderYear 			= $row['intOrderYear'];
		$customer			= $row['customer'];
		$date 				= $row['dtmdate'];
		$user 				= $row['strUserName'];
		$customerLocation 	= $row['customerLocation'];
		$contactPerson 		= $row['strContactPerson'];
	}
}

function GetMainDetails($year,$serialNo)
{
	global $db;
	$sql = "SELECT
			trn_orderdetails.strSalesOrderNo,
			ware_fabricdispatchdetails.strCutNo,
			mst_part.strName as part,
			mst_colors_ground.strName as bgcolor,
			ware_fabricdispatchdetails.strLineNo,
			ware_fabricdispatchdetails.strSize,
			ware_fabricdispatchdetails.dblSampleQty,
			ware_fabricdispatchdetails.dblGoodQty,
			ware_fabricdispatchdetails.dblEmbroideryQty,
			ware_fabricdispatchdetails.dblPDammageQty,
			ware_fabricdispatchdetails.dblFDammageQty ,
			ware_fabricdispatchdetails.dblCutRetQty,
			(ware_fabricdispatchdetails.dblSampleQty+ware_fabricdispatchdetails.dblGoodQty+ware_fabricdispatchdetails.dblEmbroideryQty+ware_fabricdispatchdetails.dblPDammageQty+ware_fabricdispatchdetails.dblFDammageQty + ware_fabricdispatchdetails.dblCutRetQty) as totalQty,  
			ware_fabricdispatchdetails.strRemarks,
			(SELECT SI.strGraphicRefNo FROM trn_sampleinfomations SI WHERE SI.intSampleNo = trn_orderdetails.intSampleNo AND SI.intSampleYear = trn_orderdetails.intSampleYear AND SI.intRevisionNo = trn_orderdetails.intRevisionNo)  as GraphicNo,
			trn_orderdetails.strStyleNo  as StyleNo
			FROM
			trn_orderdetails
			Inner Join ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear
			Inner Join ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
			left Join mst_part ON ware_fabricdispatchdetails.intPart = mst_part.intId
			left Join mst_colors_ground ON ware_fabricdispatchdetails.intGroundColor = mst_colors_ground.intId
			WHERE
			ware_fabricdispatchheader.intBulkDispatchNo =  '$serialNo' AND
			ware_fabricdispatchheader.intBulkDispatchNoYear =  '$year' ";
	return $db->RunQuery($sql);
}

function GetCompanyDetails()
{
	global $db;
	global $locationId;
	
	$header_array	= array();
	$SQL = "SELECT
			mst_companies.strName,
			mst_locations.strName as locationName,
			mst_locations.strAddress,
			mst_locations.strStreet,
			mst_locations.strCity,
			mst_country.strCountryName,
			mst_locations.strPhoneNo,
			mst_locations.strFaxNo,
			mst_locations.strEmail,
			mst_companies.strWebSite,
			mst_locations.strZip
			FROM
			mst_locations
			Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
			Inner Join mst_country ON mst_companies.intCountryId = mst_country.intCountryID
			WHERE
			mst_locations.intId =  '$locationId';";
	$result = $db->RunQuery($SQL);
	$row = mysqli_fetch_array($result);
	$companyName		= $row["strName"];
	$locationName		= $row["locationName"];
	$companyAddress1	= $row["strAddress"];
	$companyStreet		= $row["strStreet"];
	$companyCity		= $row["strCity"];
	$companyCountry		= $row["strCountryName"];
	$companyZipCode		= $row["strZip"];
	$companyPhone		= "(".$companyZipCode.")".$row["strPhoneNo"];
	$companyFax		 	= "(".$companyZipCode.")".$row["strFaxNo"];
	$companyEmail		= $row["strEMail"];
	$companyWeb		 	= $row["strWebSite"];
	
	$header_array[0]	= $companyName;
	$header_array[1]	= $companyAddress1." ".$companyStreet." ".$companyCity." ".$companyCountry.".";
	$header_array[2]	= "Tel : ".$companyPhone." Fax : ".$companyFax;
	$header_array[3]	= "E-Mail : ".$companyEmail." Web : ".$companyWeb;
	$header_array[4]	= "DISPATCH REPORT";
	return $header_array;
}
//END 	- INTERNAL FUNCTIONS }
?>
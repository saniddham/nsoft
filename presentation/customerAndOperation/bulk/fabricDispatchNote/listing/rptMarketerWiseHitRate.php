<?php



date_default_timezone_set('Asia/Colombo');
$thisFilePath =  $_SERVER['PHP_SELF'];
include_once '../../../../../dataAccess/LoginDBManager.php';
include_once "../../../../../libraries/mail/mail.php";		
$db =  new LoginDBManager();
 ini_set('max_execution_time', 11111111) ;
//	ob_start();
session_start();
//$backwardseperator = "../../../../../";
$companyId = $_SESSION['CompanyID'];
$locationId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];

$toCurrency  	= 1;//1=USD,2=LKR
$current_year 	= date('Y');
$pervious_year	=$current_year-1;



$monthName=getMonthName($month);

$i=0;

	for($j=11; $j>=0 ; $j--){
		$year 	= $current_year;
		//$p=$j;
	    $month=date('m')-$j;
		$p=$month;
		if($month<=0){
			$p=12+$month;
			$year=$current_year-1;
		}
		$monthArr[11-$j]=$p;
		$monthNameArr[11-$j]=getMonthName($p);
		$yearArr[11-$j]=$year;
		$headerMonthArr[11-$j]=$year."-".substr($monthNameArr[11-$j],0,3);
	}
	
  
$noOfMonths = 12;
$currencyName='';
$currencyName1=getCurrencyName($toCurrency);

ob_start();
?>
<title>Sales Targets</title><style>.normalfntBlue {font-family: Verdana;font-size: 10px;color: #0B3960;margin: 0px;font-weight: normal;text-align:left;}.normalfnt {font-family: Verdana;font-size: 10px;color: #000000;margin: 0px;font-weight: normal;text-align:left;}.normalfntsm {font-family: Verdana;font-size: 10px;color: #000000;margin: 0px;font-weight: normal;text-align:left;}.normalfntMid {font-family: Verdana;font-size: 10px;color: #000000;margin: 0px;font-weight: normal;text-align:center;}.normalfntRight {font-family: Verdana;font-size: 10px;color: #000000;margin: 0px;font-weight: normal;text-align:right;}.compulsoryRed{color:#F00;}</style><div align="center"><div style="background-color:#FFF" ><div align="left"><img style="vertical-align:" src="http://accsee.com/nsoft_logo.png"  /></div>
Marketing Team Hit Rates-  (<?php echo $current_year.')  Process Time('.date('Y-m-d H:i:s').')' ?></div><div style="background-color:#FFF" ></div><table width="1500" border="0" align="center" bgcolor="#FFFFFF">
<tr><td><table width="1500"><tr><td width="10">&nbsp;</td><td colspan="7" class="normalfnt">
<table width="1500" border="1" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="" bgcolor="#FFFFFF">
              <td bgcolor="#AECE84" rowspan="2" class="normalfnt"><b>Marketer</b></td>
              <?php
			  for($j=0; $j<$noOfMonths ; $j++){
			  $bgCol='#FFFFFF';
			  ?><td bgcolor="#AECE84" colspan="3" class="normalfntBlue normalfntMid" align="center" ><?php echo $headerMonthArr[$j]; ?></td><?php
			  }
			  ?></tr>
              <tr>
              <?php
			  for($j=0; $j<$noOfMonths ; $j++){
			  $bgCol='#FFFFFF';
			  ?>
              <td class="normalfntBlue normalfntMid" align="center"  bgcolor="#AECE84" >Sample Development</td> 
              <td class="normalfntBlue normalfntMid" align="center"  bgcolor="#AECE84" >Order Received</td>
              <td class="normalfntBlue normalfntMid" align="center"  bgcolor="#AECE84" >Hit Rate</td>
    		  <?php
			  }
			  ?>
              </tr><?php 
			  
			//-------------------------------------
	  	   $sql1 = "SELECT
					mst_marketer_manager.strName,
					mst_marketer_manager.intMarketingManagerId,
					mst_marketer_manager.intStatus,
					mst_marketer_manager_target.dblAnualaTarget,
					mst_marketer_manager_target.dblMonthlyTarget,
					mst_marketer_manager.dblDayTarget
					FROM `mst_marketer_manager` 
					INNER JOIN mst_marketer_manager_target ON mst_marketer_manager.intMarketingManagerId = mst_marketer_manager_target.intMarketingManagerId
					WHERE mst_marketer_manager.intMarketingManagerId!='3' 
					ORDER BY
					mst_marketer_manager.intMarketingManagerId ASC
			";
			$result1 = $db->RunQuery($sql1);
			while($row=mysqli_fetch_array($result1))
			{
				$managerId=$row['intMarketingManagerId'];
				  for($j=0; $j<$noOfMonths ; $j++){
				$month=$monthArr[$j];
				$year=$yearArr[$j];
				  
				//$dataRow=getManagerProjectedAndActualValue($managerId,$month,$year,$toCurrency);  
				$plannedQty=$dataRow['orderQry'];
				$dispatchValue=$dataRow['dispQty'];
				
				$DayWiseManagerDispSumArr1[$j]+=$dispatchValue;  
				  }
				
			}
			//-------------------------------------
				  
	  	   $sql1 = "SELECT
					mst_marketer.intUserId,
					sys_users.strUserName
					FROM
					mst_marketer
					INNER JOIN sys_users ON mst_marketer.intUserId = sys_users.intUserId
					ORDER BY
					sys_users.strUserName ASC";
			$result1 = $db->RunQuery($sql1);
			
	  		 $anualManagerTargtTot =0;
	  		 $monthlyManagerTargtTot =0;
	  		 $dayManagerTargtTot =0;
			 $totManagerDispQty=0;
			
			while($row=mysqli_fetch_array($result1))
			{
				$managerId=$row['intUserId'];
			    $managerWiseTotSamples['$managerId']=0;
			    $managerWiseTotOrders['$managerId']=0;
	  ?><tr class="normalfnt"  bgcolor="#FFFFFF"><td class="normalfnt" ><?php echo $row['strUserName'];?></td>
              <?php 
			  for($j=0; $j<$noOfMonths ; $j++){
				  
				$month=$monthArr[$j];
				$year=$yearArr[$j];
				
				//-------------- changed by lahiru ---------------------------
				$sampCount 	= getSampCount($managerId,$month,$year);

			 	$orderCount = getOrdersCount($managerId,$month,$year);
				
				//-------------------------------------------------------------
				  
				$monthWiseManagerSampCountSumArr[$j]+=$sampCount;  
				$monthWiseManagerOrderCountSumArr[$j]+=$orderCount;  
				  
				  $bgCol='#FFFFFF';
				  $fntColor='#000000';
			
			  ?>
              <td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format($sampCount,0); ?></td>
              <td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format($orderCount,0); ?></td>
              <td class="normalfntRight" bgcolor="<?php echo $bgColV; ?>" ><font color="<?php echo $fntColor?>"><?php echo number_format(($orderCount/$sampCount)*100,0); ?></font></td>
              <?php
			  }
			  ?></tr><?php 
			}
	  ?><tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" >&nbsp;</td>
              <?php

			  for($j=0; $j<$noOfMonths ; $j++){
				  $fntVColor='#000000';
				  if($monthWiseManagerVarianceSumArr[$j]<0)
				  	$fntVColor='#C33E32'; 
			  ?>
              <td class="normalfntRight" ><b><?php echo number_format($monthWiseManagerSampCountSumArr[$j],0); ?></b></td>
              <td class="normalfntRight" ><b><?php echo number_format($monthWiseManagerOrderCountSumArr[$j],0); ?></b></td>
              <td class="normalfntRight" ><b><font color="<?php echo $fntVColor?>"><?php //echo number_format($monthWiseManagerVarianceSumArr[$j],0); ?></font></b></td>
              <?php
			  }
			  ?></tr>
              <!-----------Cum.status-------->
              <tr bgcolor="#FFFFFF">
              <td colspan="<?php echo $noOfMonths+4; ?>" height="20"></td>
              </tr>
             <!-----------------------------> 
            </table>
          </td>
        <td width="10">&nbsp;</td>
        </tr>
 
 <tr>
 <td colspan="8" height="15"></td>
 </tr>
            </table>
          
        <td width="10">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<tr height="90" >
  <td align="center" class="normalfntMid"></td>
</tr>
</table>
</div><?php
function getMonthName($month){
	global $db;
	
	$sqlp = "SELECT
			mst_month.strMonth
			FROM `mst_month`
			WHERE
			mst_month.intMonthId = $month
			";	
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	return $rowp['strMonth'];
}
function getCurrencyName($id){
	global $db;
	
 	$sqlp = "SELECT
mst_financecurrency.strCode
FROM
mst_financecurrency
WHERE
mst_financecurrency.intId = '$id'
			";	
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	return $rowp['strCode'];
}
function getSampCount($managerId,$month,$year){
	global $db;	
	  $sqlp = "select count(intSampleNo) as count from (SELECT DISTINCT 
trn_sampleinfomations.intSampleNo,
trn_sampleinfomations.intSampleYear, 
trn_sampleinfomations.intStatus,
trn_sampleinfomations.dtmCreateDate,
trn_sampleinfomations.intMarketer, 
(SELECT
IFNULL(Count(trn_sample_color_recipes.intSampleNo),0) AS rcds
FROM
trn_sample_color_recipes
WHERE  trn_sample_color_recipes.intSampleNo = trn_sampleinfomations.intSampleNo 
AND trn_sample_color_recipes.intSampleYear = trn_sampleinfomations.intSampleYear 
AND trn_sample_color_recipes.intRevisionNo = trn_sampleinfomations.intRevisionNo
) as recds    
FROM
trn_sampleinfomations
LEFT JOIN trn_orderdetails ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_orderdetails.intRevisionNo AND trn_sampleinfomations.strGraphicRefNo = trn_orderdetails.strGraphicNo
LEFT JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear AND trn_orderheader.intStatus = 1
WHERE
/*trn_orderdetails.intOrderNo IS NULL OR */
trn_sampleinfomations.intMarketer = trn_orderheader.intMarketer 
HAVING recds>0 
ORDER BY
trn_orderdetails.intOrderNo DESC) as tb1 
where 
tb1.intStatus = 1 AND
month(tb1.dtmCreateDate) = '$month' AND 
year(tb1.dtmCreateDate) = '$year' AND 
tb1.intMarketer = '$managerId' 

";	

	
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	return $rowp['count'];
	
}
function getOrdersCount($managerId,$month,$year){
	global $db;
			
	$sqlp = "SELECT
Count(trn_orderheader.intOrderNo) as count , 
(SELECT
IFNULL(Count(trn_sample_color_recipes.intSampleNo),0) AS rcds
FROM
trn_sample_color_recipes
WHERE  trn_sample_color_recipes.intSampleNo = trn_sampleinfomations.intSampleNo 
AND trn_sample_color_recipes.intSampleYear = trn_sampleinfomations.intSampleYear 
AND trn_sample_color_recipes.intRevisionNo = trn_sampleinfomations.intRevisionNo
) as recds 
FROM
trn_orderheader
INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_orderdetails.intRevisionNo AND trn_sampleinfomations.strGraphicRefNo = trn_orderdetails.strGraphicNo AND trn_sampleinfomations.intMarketer = trn_orderheader.intMarketer
WHERE
trn_orderheader.intMarketer = '$managerId' AND
trn_orderheader.intStatus = 1 AND 
month(trn_orderheader.dtmCreateDate) = '$month' AND 
year(trn_orderheader.dtmCreateDate) = '$year' 
GROUP BY 
trn_orderheader.intOrderNo, 
trn_orderheader.intOrderYear,
trn_orderdetails.strGraphicNo
HAVING recds >0

";	
if($month==6){
	//echo $sqlp;
}
	
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	return $rowp['count'];
	
}
    echo $body = ob_get_clean();
	

			$nowDate = date('Y-m-d');
	
			$mailHeader= "SALES PROJECTION REPORT ($nowDate)";
			$sql = "SELECT
						sys_users.strEmail,sys_users.strFullName
					FROM
					sys_mail_eventusers
						Inner Join sys_users ON sys_users.intUserId = sys_mail_eventusers.intUserId
					WHERE
						sys_mail_eventusers.intMailEventId =  '1012'
					";
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
			//	sendMessage('nsoft@screenlineholdings.com','NSOFT - SCREENLINE HOLDINGS.',$row['strEmail'],$mailHeader,$body);		
			}
			sendMessage('nsoft@screenlineholdings.com','NSOFT - SCREENLINE HOLDINGS.','roshan@screenlineholdings.com',$mailHeader,$body);
			
			//echo $body;	
echo 'REPORT -> OK <br>';
?>
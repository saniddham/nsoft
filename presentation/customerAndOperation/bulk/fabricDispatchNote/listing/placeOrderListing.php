<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "libraries/jqgrid2/inc/jqgrid_dist.php";

$location 		= $sessions->getLocationId();
$intUser  		= $sessions->getUserId();

$approveLevel 	= (int)getMaxApproveLevel();
$programCode	= 'P0427';

//---------------------------------------------------------------------

//BEGIN - ADD DEFAULT WHERE STRING WHEN FORM LOADING {
$arr =  json_decode($_REQUEST['filters'],true);

$arr = $arr['rules'];

$where_string = '';
$where_array = array(
					'company'=>'mst_companies.strName',
					'Status'=>'tb1.intStatus',
					'Technique'=>"if(mst_customer_po_techniques.ID >0 , mst_customer_po_techniques.NAME , 'None')",
					'Order_No'=>"tb1.intOrderNo",
					'Order_Year'=>"tb1.intOrderYear",
					'SalesOrderNo'=>"(select  group_concat(strSalesOrderNo separator ', ') from  (select distinct strSalesOrderNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
										tb.intOrderNo =  tb1.intOrderNo AND
										tb.intOrderYear =  tb1.intOrderYear)",
					'Style'=>"(select  group_concat(strStyleNo separator ', ') from  (select distinct strStyleNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
										tb.intOrderNo =  tb1.intOrderNo AND
										tb.intOrderYear =  tb1.intOrderYear)",
					'Graphic'=>"(select  group_concat(strGraphicNo separator ', ') from  (select distinct strGraphicNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
										tb.intOrderNo =  tb1.intOrderNo AND
										tb.intOrderYear =  tb1.intOrderYear)",
					'Customer_PO_No'=>"tb1.strCustomerPoNo",
					'Customer'=>"mst_customer.strName",
					'Location_Name'=>"mst_customer_locations_header.strName",
					'Date'=>'tb1.dtDate',
					'Delivery_Date'=>'tb1.dtDeliveryDate',
					);
							
$arr_status = array('Approved'=>'1','Rejected'=>'0','Revised'=>'-1','Cancelled'=>'-2','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}

if(!count($arr)>0){
	$where_string .= "AND tb1.dtDate = '".date('Y-m-d')."'";
	//$where_string .= "AND mst_companies.strName = '$inilocation'";
}
//END }

$sql = "select * from(SELECT DISTINCT 
							if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected',if(tb1.intStatus=-10,'Completed',if(tb1.intStatus=-1,'Cancel','Pending')))) as Status,
							tb1.intOrderNo as `Order_No`,
							tb1.intOrderYear as `Order_Year`,
							
							(select  group_concat(strSalesOrderNo separator ', ') from  (select distinct strSalesOrderNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
							tb.intOrderYear =  tb1.intOrderYear

							) as `SalesOrderNo`,
							
							(select  group_concat(strStyleNo separator ', ') from  (select distinct strStyleNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
							tb.intOrderYear =  tb1.intOrderYear

							) as `Style`,
							(select  group_concat(strGraphicNo separator ', ') from  (select distinct strGraphicNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
							tb.intOrderYear =  tb1.intOrderYear

							) as `Graphic`, 
							
							
							IFNULL((SELECT
								Sum(trn_orderdetails.intQty) 
								FROM trn_orderdetails
								WHERE
								trn_orderdetails.SO_TYPE > -1 AND
								trn_orderdetails.intOrderNo = tb1.intOrderNo AND
								trn_orderdetails.intOrderYear =  tb1.intOrderYear 
							),0) as Order_Qty, 
					
							((IFNULL((SELECT
								Sum(ware_fabricreceiveddetails.dblQty)
								FROM
								ware_fabricreceivedheader
								Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
								WHERE
								ware_fabricreceivedheader.intStatus =  '1' AND
								ware_fabricreceivedheader.intOrderNo =   tb1.intOrderNo AND
								ware_fabricreceivedheader.intOrderYear =  tb1.intOrderYear 
							),0))) as Received_Qty, 
							
							round(((IFNULL((SELECT
								Sum(ware_fabricreceiveddetails.dblQty)
								FROM
								ware_fabricreceivedheader
								Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
								WHERE
								ware_fabricreceivedheader.intStatus =  '1' AND
								ware_fabricreceivedheader.intOrderNo =   tb1.intOrderNo AND
								ware_fabricreceivedheader.intOrderYear =  tb1.intOrderYear 
							),0)))/(IFNULL((SELECT
								Sum(trn_orderdetails.intQty) 
								FROM trn_orderdetails
								WHERE
								trn_orderdetails.intOrderNo = tb1.intOrderNo AND
								trn_orderdetails.intOrderYear =  tb1.intOrderYear
							),0))*100,2) as Received_Progress , 
							
							tb1.strCustomerPoNo as `Customer_PO_No`,
							mst_customer.strName as `Customer`,
							tb1.dtDate as `Date`,
							tb1.dtDeliveryDate as `Delivery_Date`, 
							  
							IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(trn_orderheader_approvedby.dtApprovedDate),')' )
								FROM
								trn_orderheader_approvedby
								Inner Join sys_users ON trn_orderheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								trn_orderheader_approvedby.intOrderNo  = tb1.intOrderNo AND
								trn_orderheader_approvedby.intYear =  tb1.intOrderYear AND
								trn_orderheader_approvedby.intApproveLevelNo =  '1' AND
								trn_orderheader_approvedby.intStatus =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.intStatus>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(trn_orderheader_approvedby.dtApprovedDate),')' )
								FROM
								trn_orderheader_approvedby
								Inner Join sys_users ON trn_orderheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								trn_orderheader_approvedby.intOrderNo  = tb1.intOrderNo AND
								trn_orderheader_approvedby.intYear =  tb1.intOrderYear AND
								trn_orderheader_approvedby.intApproveLevelNo =  '$i' AND
								trn_orderheader_approvedby.intStatus =  '0' 
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.intStatus>1) AND (tb1.intStatus<=tb1.intApproveLevelStart) AND ((SELECT
								concat(sys_users.strUserName )
								FROM
								trn_orderheader_approvedby
								Inner Join sys_users ON trn_orderheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								trn_orderheader_approvedby.intOrderNo  = tb1.intOrderNo AND
								trn_orderheader_approvedby.intYear =  tb1.intOrderYear AND
								trn_orderheader_approvedby.intApproveLevelNo =  ($i-1) AND 
								trn_orderheader_approvedby.intStatus='0' )<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*fase part*/
								 if($i>tb1.intApproveLevelStart,'-----',''))
								
								/*end IFNULL false part*/
								) as `".$approval."`, "; 
									
								}
								
							$sql .= "IF(((SELECT
								menupermision.intRevise 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode =  '$programCode' AND
								menupermision.intUserId =  '$intUser' AND  
								tb1.intStatus<=tb1.intApproveLevelStart AND 
								tb1.intStatus>0) =1), 'Revise', '')  as `Revise`, 
							'View' as `View`   
						FROM
							trn_orderheader as tb1
							Inner Join mst_customer ON tb1.intCustomer = mst_customer.intId
							Inner Join sys_users ON tb1.intCreator = sys_users.intUserId 
							Inner Join mst_locations ON tb1.intLocationId = mst_locations.intId 
							WHERE
							tb1.intLocationId =  '$location' 
							$where_string
							)  as t where 1=1
						";
					 	$sql;
$col = array();

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected;Completed:Completed;Cancel:Cancel" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist

$cols[] = $col;	$col=NULL;
//ORDER NO
$col["title"] 	= "Order No"; // caption of column
$col["name"] 	= "Order_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']		= "?q=427&orderNo={Order_No}&orderYear={Order_Year}";	 
$col["linkoptions"] = "target='placeOrder.php'"; // extra params with <a> tag

$reportLink  		= "?q=896&orderNo={Order_No}&orderYear={Order_Year}";
$reportLinkApprove  = "?q=896&orderNo={Order_No}&orderYear={Order_Year}&approveMode=1";

$cols[] = $col;	$col=NULL;


//ORDER YEAR
$col["title"] = "Order year"; // caption of column
$col["name"] = "Order_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//SALES ORDER NO
$col["title"] = "Sales Order No"; // caption of column
$col["name"] = "SalesOrderNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//ORDER YEAR
$col["title"] = "Style No"; // caption of column
$col["name"] = "Style"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

$col["title"] = "Grahpic No"; // caption of column
$col["name"] = "Graphic"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//CUSTOMER PO NO
$col["title"] = "Customer PO No"; // caption of column
$col["name"] = "Customer_PO_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Customer"; // caption of column
$col["name"] = "Customer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//DATE
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//DELIVERY DATE
$col["title"] = "Delivery Date"; // caption of column
$col["name"] = "Delivery_Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Order Qty
$col["title"] = "Order Qty"; // caption of column
$col["name"] = "Order_Qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//Order Qty
$col["title"] = "Received Qty"; // caption of column
$col["name"] = "Received_Qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//Order Qty
$col["title"] = "Progress %"; // caption of column
$col["name"] = "Received_Progress"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//FIRST APPROVAL
$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='rptBulkOrder.php'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='rptBulkOrder.php'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}

//REVISE
$col["title"] = "Revise"; // caption of column
$col["name"] = "Revise"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col['link']	= "?q=896&orderNo={Order_No}&orderYear={Order_Year}&revise=1";	 

$col["linkoptions"] = "target='rptBulkOrder.php'"; // extra params with <a> tag
$col["search"] = false;
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Customer place Order Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'Order_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes

/*$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"}
     ]
}
SEARCH_JSON;*/

$grid["search"] = true; 
$grid["postData"] = array("filters" => $sarr ); 

// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);
$out = $jq->render("list1");
?>

<?php include 'include/listing.html'?>

<div align="center" style="margin:10px"><?php echo $out?></div>

<?php
function getMaxApproveLevel(){
	global $db;
	
	$appLevel=0;
	$sqlp = "SELECT
			Max(trn_orderheader.intApproveLevelStart) AS appLevel
			FROM trn_orderheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
?>
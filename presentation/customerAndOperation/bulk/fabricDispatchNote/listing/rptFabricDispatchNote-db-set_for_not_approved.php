<?php 
	ini_set('display_errors',0);
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];

	include "{$backwardseperator}dataAccess/Connector.php";

 	
	$serialNo 	= $_REQUEST['no'];
	$year 		= $_REQUEST['year'];
	
  		
 			$sql1 = "SELECT
				ware_fabricdispatchheader.intOrderNo,
				ware_fabricdispatchheader.intOrderYear,
				ware_fabricdispatchheader.intCompanyId,
				ware_fabricdispatchdetails.strCutNo,
				ware_fabricdispatchdetails.intSalesOrderId,
				ware_fabricdispatchdetails.intPart, 
				ware_fabricdispatchdetails.strSize,
				ware_fabricdispatchdetails.dblSampleQty,
				ware_fabricdispatchdetails.dblGoodQty,
				ware_fabricdispatchdetails.dblEmbroideryQty,
				ware_fabricdispatchdetails.dblPDammageQty,
				ware_fabricdispatchdetails.dblFDammageQty,
				ware_fabricdispatchdetails.dblCutRetQty 
				FROM
				ware_fabricdispatchdetails
				Inner Join ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
				WHERE
				ware_fabricdispatchdetails.intBulkDispatchNo =  '$serialNo' AND
				ware_fabricdispatchdetails.intBulkDispatchNoYear =  '$year' AND 
				ware_fabricdispatchheader.intStatus = '1'";
				$result1 = $db->RunQuery($sql1);
				$toSave =0;
				$savedRcds	=0;
				while($row=mysqli_fetch_array($result1))
				{
					$location=$row['intCompanyId'];
					$orderNo=$row['intOrderNo'];
					$orderYear=$row['intOrderYear'];
					$cutNo=$row['strCutNo'];
					$part=$row['intPart'];
					$salesOrderId=$row['intSalesOrderId'];
					$size=$row['strSize'];
					$sampleQty=round($row['dblSampleQty'],4);
					$goodQty=round($row['dblGoodQty'],4);
					$embroideryQty=round($row['dblEmbroideryQty'],4);
					$pDammageQty=round($row['dblPDammageQty'],4);
					$fDammageQty=round($row['dblFDammageQty'],4);
					$cutRetQty=round($row['dblCutRetQty'],4);
					$place ='Admin';
					
					$resp = checkForSavedAny($serialNo,$year);
					if($resp['type']=='pass'){
						$time_svd	= 	$resp['time'];
						$user_svd	= 	$resp['user'];
					}
					else{
						$time_svd	= 	'now()';
						$user_svd	= 	'2';
					}
					
					
					if($sampleQty>0){
 					$type = 'Dispatched_S';
					$resp_s	= checkForSavedAndStkBal($location,$serialNo,$year,$orderNo,$orderYear,$salesOrderId,$cutNo,$size,$type,$sampleQty);
					if($resp_s['type']=='pass'){
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','-$sampleQty','$type','$user_svd',$time_svd)";
					$resultI = $db->RunQuery($sql);
					$toSave++;
					$savedRcds	+=$resultI;
					$rollBackSql =$sql;
					echo "Saved res".$resultI.'=>'.$orderNo.','.$orderYear.','.$salesOrderId.','.$cutNo.','.$size.','.$type."</br>";
 					}
					else{
					echo $resp_s['msg']	."</br>";
					}
					}
					if($goodQty>0){
					$type = 'Dispatched_G';
					$resp_s	= checkForSavedAndStkBal($location,$serialNo,$year,$orderNo,$orderYear,$salesOrderId,$cutNo,$size,$type,$goodQty);
					if($resp_s['type']=='pass'){
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','-$goodQty','$type','$user_svd',$time_svd)";
					$resultI = $db->RunQuery($sql);
					$toSave++;
					$savedRcds	+=$resultI;
					$rollBackSql =$sql;
					echo "Saved res".$resultI.'=>'.$orderNo.','.$orderYear.','.$salesOrderId.','.$cutNo.','.$size.','.$type."</br>";
 					}
					else{
					echo $resp_s['msg']	."</br>";
					}
					}
					if($embroideryQty>0){
					$type = 'Dispatched_E';
					$resp_s	= checkForSavedAndStkBal($location,$serialNo,$year,$orderNo,$orderYear,$salesOrderId,$cutNo,$size,$type,$embroideryQty);
					if($resp_s['type']=='pass'){
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','-$embroideryQty','$type','$user_svd',$time_svd)";
					$resultI = $db->RunQuery($sql);
					$toSave++;
					$savedRcds	+=$resultI;
					$rollBackSql =$sql;
					echo "Saved res".$resultI.'=>'.$orderNo.','.$orderYear.','.$salesOrderId.','.$cutNo.','.$size.','.$type."</br>";
 					}
					else{
					echo $resp_s['msg']	."</br>";
					}
					}
					if($pDammageQty>0){
					$type = 'Dispatched_P';
					$resp_s	= checkForSavedAndStkBal($location,$serialNo,$year,$orderNo,$orderYear,$salesOrderId,$cutNo,$size,$type,$pDammageQty);
					if($resp_s['type']=='pass'){
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','-$pDammageQty','$type','$user_svd',$time_svd)";
					$resultI = $db->RunQuery($sql);
					$toSave++;
					$savedRcds	+=$resultI;
					$rollBackSql =$sql;
					echo "Saved res".$resultI.'=>'.$orderNo.','.$orderYear.','.$salesOrderId.','.$cutNo.','.$size.','.$type."</br>";
 					}
					else{
					echo $resp_s['msg']	."</br>";
					}
					}
					if($fDammageQty>0){
					$type = 'Dispatched_F';
					$resp_s	= checkForSavedAndStkBal($location,$serialNo,$year,$orderNo,$orderYear,$salesOrderId,$cutNo,$size,$type,$fDammageQty);
					if($resp_s['type']=='pass'){
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','-$fDammageQty','$type','$user_svd',$time_svd)";
					$resultI = $db->RunQuery($sql);
					$toSave++;
					$savedRcds	+=$resultI;
					$rollBackSql =$sql;
					echo "Saved res".$resultI.'=>'.$orderNo.','.$orderYear.','.$salesOrderId.','.$cutNo.','.$size.','.$type."</br>";
 					}
					else{
					echo $resp_s['msg']	."</br>";
					}
					}
					if($cutRetQty>0){
					$type = 'Dispatched_CUT_RET';
					$resp_s	= checkForSavedAndStkBal($location,$serialNo,$year,$orderNo,$orderYear,$salesOrderId,$cutNo,$size,$type,$cutRetQty);
					if($resp_s['type']=='pass'){
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','-$cutRetQty','$type','$user_svd',$time_svd)";
					$resultI = $db->RunQuery($sql);
					$toSave++;
					$savedRcds	+=$resultI;
					$rollBackSql =$sql;
					echo "Saved res".$resultI.'=>'.$orderNo.','.$orderYear.','.$salesOrderId.','.$cutNo.','.$size.','.$type."</br>";
 					}
					else{
					echo $resp_s['msg']	."</br>";
					}
					}
 				
				}
 //----------------------------------------

function checkForSavedAny($serialNo,$year){
	global $db;
	$sql	= "SELECT
ware_stocktransactions_fabric.intId,
ware_stocktransactions_fabric.intLocationId,
ware_stocktransactions_fabric.strPlace,
ware_stocktransactions_fabric.intToLocationId,
ware_stocktransactions_fabric.intParentDocumentNo,
ware_stocktransactions_fabric.intParentDocumentYear,
ware_stocktransactions_fabric.intDocumentNo,
ware_stocktransactions_fabric.intDocumentYear,
ware_stocktransactions_fabric.intOrderNo,
ware_stocktransactions_fabric.intOrderYear,
ware_stocktransactions_fabric.strCutNo,
ware_stocktransactions_fabric.intSalesOrderId,
ware_stocktransactions_fabric.intPart,
ware_stocktransactions_fabric.strSize,
ware_stocktransactions_fabric.intGrade,
ware_stocktransactions_fabric.dblQty,
ware_stocktransactions_fabric.strType,
ware_stocktransactions_fabric.intUser,
ware_stocktransactions_fabric.dtDate,
ware_stocktransactions_fabric.invoiced
FROM `ware_stocktransactions_fabric`
WHERE
ware_stocktransactions_fabric.strType LIKE '%Disp%' AND
ware_stocktransactions_fabric.intDocumentNo = '$serialNo' AND
ware_stocktransactions_fabric.intDocumentYear = '$year'";
	$result1 = $db->RunQuery($sql1);
	$row=mysqli_fetch_array($result1);
	
	if($row['intId'] > 0 ){
		$res['type']	= 'pass';
		$res['time']	= $row['dtDate'];
		$res['user']	= $row['intUser'];
	}
	
	return $res;

}

function checkForSavedAndStkBal($location,$serialNo,$year,$orderNo,$orderYear,$salesOrderId,$cutNo,$size,$type,$qty){
	
	$flag	=checkForSaved($serialNo,$year,$salesOrderId,$cutNo,$size,$type);
	if($flag==0){
		$stk	=checkForStock($location,$orderNo,$orderYear,$salesOrderId,$cutNo,$size);
		if($stk > $qty){
		$resp['type']='pass';
		$resp['msg']='Stock Exists';
		}
		else{
		$resp['type']='fail';
		$resp['msg']='No Stock for '.$orderNo.','.$orderYear.','.$salesOrderId.','.$cutNo.','.$size.','.$type.',Qty:'.$qty;
		}
	
	}
	else{//already saved
	$resp['type']='fail';
	$resp['msg']='Already Saved '.$orderNo.','.$orderYear.','.$salesOrderId.','.$cutNo.','.$size.','.$type;
	}
	
	return $resp;
}

function checkForSaved($serialNo,$year,$salesOrderId,$cutNo,$size,$type){
	global $db;
	
	$sql1	="
SELECT
ware_stocktransactions_fabric.intId,
ware_stocktransactions_fabric.intLocationId,
ware_stocktransactions_fabric.strPlace,
ware_stocktransactions_fabric.intToLocationId,
ware_stocktransactions_fabric.intParentDocumentNo,
ware_stocktransactions_fabric.intParentDocumentYear,
ware_stocktransactions_fabric.intDocumentNo,
ware_stocktransactions_fabric.intDocumentYear,
ware_stocktransactions_fabric.intOrderNo,
ware_stocktransactions_fabric.intOrderYear,
ware_stocktransactions_fabric.strCutNo,
ware_stocktransactions_fabric.intSalesOrderId,
ware_stocktransactions_fabric.intPart,
ware_stocktransactions_fabric.strSize,
ware_stocktransactions_fabric.intGrade,
ware_stocktransactions_fabric.dblQty,
ware_stocktransactions_fabric.strType,
ware_stocktransactions_fabric.intUser,
ware_stocktransactions_fabric.dtDate,
ware_stocktransactions_fabric.invoiced
FROM `ware_stocktransactions_fabric`
WHERE
ware_stocktransactions_fabric.intDocumentNo = '$serialNo' AND
ware_stocktransactions_fabric.intDocumentYear = '$year' AND
ware_stocktransactions_fabric.intSalesOrderId = '$salesOrderId' AND
ware_stocktransactions_fabric.strSize = '$size' AND
ware_stocktransactions_fabric.strCutNo = '$cutNo' AND
ware_stocktransactions_fabric.strType = '$type'";
	$result1 = $db->RunQuery($sql1);
	$row=mysqli_fetch_array($result1);
	
	if($row['intId'] > 0 ){
		$flag	=1;
	}
	else
		$flag	=0;
	
	return $flag;
	
}
		
function checkForStock($location,$intOrderNo,$intOrderYear,$salesOrderId,$cutNo,$size){
	global $db;
	 $sql1	="
SELECT
Sum(ware_stocktransactions_fabric.dblQty) as Qty 
FROM `ware_stocktransactions_fabric`
WHERE
ware_stocktransactions_fabric.intOrderNo = '$intOrderNo' AND
ware_stocktransactions_fabric.intOrderYear = '$intOrderYear' AND
ware_stocktransactions_fabric.intSalesOrderId = '$salesOrderId' AND
ware_stocktransactions_fabric.strSize = '$size' AND
ware_stocktransactions_fabric.strCutNo = '$cutNo' AND
ware_stocktransactions_fabric.intLocationId = '$location'
	";
	$result1 = $db->RunQuery($sql1);
	$row=mysqli_fetch_array($result1);
	
 	
	return $row['Qty'];
}

 
<?php
ini_set('display_errors',0);
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
	session_start();
	$backwardseperator = "../../../../../";
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include_once "{$backwardseperator}dataAccess/Connector.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
	//require_once $_SESSION['ROOT_PATH']."class/customerAndOperation/bulk/cls_bulk_get.php";

	$objcomfunc 	= new cls_commonFunctions_get($db);
	//$obj_bulk_get 	= new Cls_bulk_get($db);

	$programName='Dispatch Note';
	$programCode='P0470';
	$fabRcvApproveLevel = (int)getApproveLevel($programName);
	

 if($requestType=='getValidation')
	{
		$serialNo  = $_REQUEST['serialNo'];
		$serialNoArray=explode("/",$serialNo);
		
		$sql = "SELECT 
				ware_fabricdispatchheader.intOrderNo,
				ware_fabricdispatchheader.intStatus,
				ware_fabricdispatchheader.intApproveLevels,
				ware_fabricdispatchheader.intOrderYear, 
				ware_fabricdispatchheader.intCustLocation, 
				trn_orderdetails.strSalesOrderNo,
				trn_orderdetails.intSalesOrderId, 
				ware_fabricdispatchdetails.strCutNo,
				ware_fabricdispatchdetails.intPart, 
				mst_part.strName as part,
				ware_fabricdispatchdetails.intGroundColor, 
				mst_colors_ground.strName as bgcolor,
				ware_fabricdispatchdetails.strLineNo,
				ware_fabricdispatchdetails.strSize,
				ware_fabricdispatchdetails.dblSampleQty,
				ware_fabricdispatchdetails.dblGoodQty,
				ware_fabricdispatchdetails.dblEmbroideryQty,
				ware_fabricdispatchdetails.dblPDammageQty,
				ware_fabricdispatchdetails.dblFDammageQty, 
				ware_fabricdispatchdetails.dblCutRetQty
				
				FROM
				trn_orderdetails
				Inner Join ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear
				Inner Join ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
				Inner Join mst_part ON ware_fabricdispatchdetails.intPart = mst_part.intId
				Inner Join mst_colors_ground ON ware_fabricdispatchdetails.intGroundColor = mst_colors_ground.intId
				WHERE
				ware_fabricdispatchheader.intBulkDispatchNo =  '$serialNoArray[0]' AND
				ware_fabricdispatchheader.intBulkDispatchNoYear =  '$serialNoArray[1]' ";
				
		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$msg1 =""; 
		$msg ="Maximum Qtys for items"; 
		while($row=mysqli_fetch_array($result))
		{
				$status=$row['intStatus'];
				$levels=$row['intApproveLevels'];
				$orderNo=$row['intOrderNo'];
				$orderYear=$row['intOrderYear'];
				$customer_location=$row['intCustLocation'];
				$salesOrderNo=$row['strSalesOrderNo'];
				$salesOrderId=$row['intSalesOrderId'];
				$cutNo=$row['strCutNo'];
				$partId=$row['intPart'];
				$part=$row['part'];
				$size=$row['strSize'];
				$tot=val($row['dblSampleQty'])+val($row['dblGoodQty'])+val($row['dblEmbroideryQty'])+val($row['dblPDammageQty'])+val($row['dblFDammageQty'])+val($row['dblCutRetQty']);
			
				$balQty=loadBalQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$partId,$size);
				$nonStkConfQty=loadNonStockConfQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$partId,$size);
				$orderQty=loadOrderQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade);
				$maxQty=$balQty-$nonStkConfQty;
				if($row['intStatus']>1 && ($row['intStatus']<=($row['intApproveLevels']))){
				$maxQty=$balQty-$nonStkConfQty+$tot;
				}
				
				//echo $tot."pp".$maxQty;

				//-------------------------------------------
				/*
				$lc_flag		= $obj_bulk_get->get_order_lc_flag($orderNo,$orderYear,'RunQuery');
				if($lc_flag	== 6) 
					$lc_flag = 1;
				else
					$lc_flag = 0;
				$lc_status		= $obj_bulk_get->get_lc_status($orderNo,$orderYear,'RunQuery');
				*/
				//------check maximum FR Qty--------------------
				$confirmatonMode=loadConfirmatonMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);
				//--------------------------
				$locationType 	= $objcomfunc->getLocationType($location);
				//--------------------------
				$customer_email_flag	=	get_customer_email_flag($orderNo,$orderYear);
				//--------------------------
				$customer_location_email_flag	=	get_customer_location_email_flag($orderNo,$orderYear,$customer_location);
				//--------------------------
				
				$plantId		= getPlantId($location);
  
				if($plantId!='')
				{
					$plantStatus	= getPlantStatus($plantId); 
				}
				
				if($locationType < 1){//
					$errorFlg = 1;
					$msg ="This is not a production location.So can't dispatch fabrics.";
				}
				else if($plantId=='')
				{
					$errorFlg = 1;
					$msg = "This location is not allocate to a plant.";
				}
				else if($plantStatus!=1)
				{
					$errorFlg = 1;
					$msg = "This location is not allocate to an active plant.";
				}
				else if($status==0){
					$errorFlg=1;
					$msg ="This Dispatch No is Rejected.";
				}
				else if($status==1){
					$errorFlg=1;
					$msg ="Final confirmation of this Dispatch No is already raised";
				}
				else if($confirmatonMode==0){// 
					$errorFlg = 1;
					$msg ="No Permission to Approve"; 
				}
				/* else if($lc_flag==1 && $lc_status !=1){
					 $errorFlg	=1;
					 $msg		="No approved LC.So can't dispatch fabrics.";
				 }*/
				/*else if(($customer_email_flag==0) && ($status == ($levels+1))){// 
					$errorFlg = 1;
					$msg ="Please Enter Customer Email to Approve this"; 
				}*/
				else if($tot>$maxQty){
					$errorFlg=1;
					$msg .="<br> ".$cutNo."-".$part."-".$salesOrderNo."-".$size." =".$maxQty;
				}
		}//end of while
	
	if($msg1!=''){
		$msg=$msg1;
	}
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}

else if($requestType=='validateRejecton')
	{
		$errorFlg=0;
		$serialNo  = $_REQUEST['serialNo'];
		$serialNoArray=explode("/",$serialNo);
		
		$sql = "SELECT
		ware_fabricdispatchheader.intStatus, 
		ware_fabricdispatchheader.intApproveLevels 
		FROM ware_fabricdispatchheader
		WHERE
		ware_fabricdispatchheader.intBulkDispatchNo =  '$serialNoArray[0]' AND
		ware_fabricdispatchheader.intBulkDispatchNoYear =  '$serialNoArray[1]'";

		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$msg =""; 
		$row=mysqli_fetch_array($result);
		
		$rejectionMode=loadRejectionMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);
		
		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this Dispatch No is already raised.So cant reject"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This Dispatch No is already Rejected"; 
		}
		else if($rejectionMode==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="No Permission to reject this Dispatch No"; 
		}
		else{//confirmation has been raised
			$errorFlg = 0;
			
		}

	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);

	}

	

	//-----------------------------------------------------------
function loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade)
{
		global $db;
		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				ware_stocktransactions_fabric.intLocationId =  '$location' AND
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType =  'Received' 
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//-----------------------------------------------------------
function loadOrderQty($location,$orderNo,$orderYear,$salesOrderNo,$size,$grade)
{
		global $db;
		  $sql = "SELECT
				trn_ordersizeqty.dblQty
				FROM trn_ordersizeqty
				WHERE
				trn_ordersizeqty.intOrderNo =  '$orderNo' AND
				trn_ordersizeqty.intOrderYear =  '$orderYear' AND
				trn_ordersizeqty.intSalesOrderId =  '$salesOrderNo' AND
				trn_ordersizeqty.strSize =  '$size'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
	//--------------------------------------------------------------
	function loadBalQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$intPart,$size){
		global $db;
		   $sql = "SELECT
					Sum(ware_stocktransactions_fabric.dblQty) AS qty
					FROM ware_stocktransactions_fabric
					WHERE
					ware_stocktransactions_fabric.intLocationId =  '$location' AND
					ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
					ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
					ware_stocktransactions_fabric.strCutNo =  '$cutNo' AND
					ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
					ware_stocktransactions_fabric.strSize =  '$size' AND
					ware_stocktransactions_fabric.intPart =  '$intPart'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
	//	echo val($rows['qty']);
		return val($rows['qty']);
	}
	//--------------------------------------------------------------
	function loadNonStockConfQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$intPart,$size){
		global $db;
		   $sql = "SELECT
					sum(ware_fabricdispatchdetails.dblSampleQty+
					ware_fabricdispatchdetails.dblGoodQty+
					ware_fabricdispatchdetails.dblEmbroideryQty+
					ware_fabricdispatchdetails.dblPDammageQty+
					ware_fabricdispatchdetails.dblFdammageQty+
					ware_fabricdispatchdetails.dblCutRetQty) as qty
					FROM
					ware_fabricdispatchheader
					Inner Join ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear
					WHERE
					ware_fabricdispatchheader.intOrderNo =  '$orderNo' AND
					ware_fabricdispatchheader.intOrderYear =  '$orderYear' AND
					ware_fabricdispatchdetails.intSalesOrderId =  '$salesOrderId' AND
					ware_fabricdispatchdetails.strCutNo =  '$cutNo' AND
					ware_fabricdispatchdetails.strSize =  '$size' AND
					ware_fabricdispatchdetails.intPart =  '$intPart' AND
					ware_fabricdispatchheader.intStatus >  '1' AND
					ware_fabricdispatchheader.intApproveLevels >= ware_fabricdispatchheader.intStatus AND
					ware_fabricdispatchheader.intCompanyId =  '$location'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
	//	echo val($rows['qty']);
		return val($rows['qty']);
	}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//--------------------------------------------------------
function get_customer_email_flag($orderNo,$orderYear){
	
		global $db;
		  $sql = "SELECT
					mst_customer.strEmail
					FROM
					trn_orderheader
					INNER JOIN mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
					WHERE
					trn_orderheader.intOrderNo = '$orderNo' AND
					trn_orderheader.intOrderYear = '$orderYear'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		if($rows['strEmail']=='')
			return 0;
		else
			return 1;
	
}
//---------------------------------------------------------
function get_customer_location_email_flag($orderNo,$orderYear,$customer_location){
	
		global $db;
		  $sql = "	SELECT
						mst_customer_locations.strEmailAddress
					FROM
						trn_orderheader
						INNER JOIN mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
						INNER JOIN mst_customer_locations ON mst_customer_locations.intCustomerId = trn_orderheader.intCustomer AND mst_customer_locations.intLocationId = trn_orderheader.intCustomerLocation
					WHERE
						trn_orderheader.intOrderNo 		= '$orderNo' AND
						trn_orderheader.intOrderYear 	= '$orderYear' AND
						trn_orderheader.intCustomerLocation 	= '$customer_location'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		if($rows['strEmailAddress']=='')
			return 0;
		else
			return 1;
	
}

function getPlantId($locationId)
{
	global $db;
	
	$sql 	= "SELECT intPlant FROM mst_locations WHERE intId = '$locationId' ";
	
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	
	return $row['intPlant'];
}

function getPlantStatus($plantId)
{
	global $db;
	
	$sql 	= "SELECT intStatus FROM mst_plant WHERE intPlantId = '$plantId' ";
	
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	
	return $row['intStatus'];
}


?>

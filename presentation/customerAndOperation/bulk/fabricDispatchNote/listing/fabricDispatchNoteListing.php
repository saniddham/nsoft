<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "libraries/jqgrid2/inc/jqgrid_dist.php";

$location 		= $sessions->getLocationId();
$intUser  		= $sessions->getUserId();

$programCode	= 'P0470';

$approveLevel 	= (int)getMaxApproveLevel();

################################################################
################## new part ####################################
################################################################
///// create where part ///////////////////
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'Status'=>'tb1.intStatus',
				'Dispatch_No'=>'tb1.intBulkDispatchNo',
				'Dispatch_Year'=>'tb1.intBulkDispatchNoYear',
				'strStyleNo'=>'tb2.strStyleNo',
				'Order_No'=>'concat((tb1.intOrderNo),"/",(tb1.intOrderYear))',
				'strGraphicNo'=>'tb2.strGraphicNo',
				'strSalesOrderNo'=>'tb2.strSalesOrderNo',
				);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Revised'=>'-1','Cancelled'=>'-2','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}

if(!count($arr)>0)					 
		$where_string .= "AND DATE(tb1.dtmCreateDate) = '".date('Y-m-d')."'";
//END }
	
################## end code ####################################

$sql = "select * from(SELECT DISTINCT 
							if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected','Pending')) as Status,
							tb1.intBulkDispatchNo as `Dispatch_No`,
							tb1.intBulkDispatchNoYear as `Dispatch_Year`,
							tb1.dtmCreateDate as `Date`,
							
							(select  group_concat(strStyleNo separator ', ')  from  (select distinct strStyleNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
							tb.intOrderYear =  tb1.intOrderYear

							) as strStyleNo,
							
							(select  group_concat(strGraphicNo separator ', ') from  (select distinct strGraphicNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
							tb.intOrderYear =  tb1.intOrderYear

							) as strGraphicNo, 
							
							(SELECT
							group_concat(distinct strSalesOrderNo separator ', ')
							FROM ware_fabricdispatchdetails inner join trn_orderdetails on trn_orderdetails.intSalesOrderId=ware_fabricdispatchdetails.intSalesOrderId
							WHERE
							ware_fabricdispatchdetails.intBulkDispatchNo =  tb1.intBulkDispatchNo AND
							trn_orderdetails.intOrderNo = tb1.intOrderNo AND 
							trn_orderdetails.intOrderYear = tb1.intOrderYear AND
							ware_fabricdispatchdetails.intBulkDispatchNoYear =  tb1.intBulkDispatchNoYear) as strSalesOrderNo,
							
							IFNULL((SELECT
								Sum(ware_fabricdispatchdetails.dblSampleQty)
								FROM
								ware_fabricdispatchdetails 
								WHERE
								ware_fabricdispatchdetails.intBulkDispatchNo  =   tb1.intBulkDispatchNo AND
								ware_fabricdispatchdetails.intBulkDispatchNoYear =  tb1.intBulkDispatchNoYear

							),0) as SampleQty, 
							
							IFNULL((SELECT
								Sum(ware_fabricdispatchdetails.dblGoodQty)
								FROM
								ware_fabricdispatchdetails 
								WHERE
								ware_fabricdispatchdetails.intBulkDispatchNo  =   tb1.intBulkDispatchNo AND
								ware_fabricdispatchdetails.intBulkDispatchNoYear =  tb1.intBulkDispatchNoYear

							),0) as GoodQty, 
							
							IFNULL((SELECT
								Sum(ware_fabricdispatchdetails.dblEmbroideryQty)
								FROM
								ware_fabricdispatchdetails 
								WHERE
								ware_fabricdispatchdetails.intBulkDispatchNo  =   tb1.intBulkDispatchNo AND
								ware_fabricdispatchdetails.intBulkDispatchNoYear =  tb1.intBulkDispatchNoYear

							),0) as EmbroideryQty, 

							IFNULL((SELECT
								Sum(ware_fabricdispatchdetails.dblPDammageQty)
								FROM
								ware_fabricdispatchdetails 
								WHERE
								ware_fabricdispatchdetails.intBulkDispatchNo  =   tb1.intBulkDispatchNo AND
								ware_fabricdispatchdetails.intBulkDispatchNoYear =  tb1.intBulkDispatchNoYear

							),0) as PDammageQty, 

							IFNULL((SELECT
								Sum(ware_fabricdispatchdetails.dblFdammageQty)
								FROM
								ware_fabricdispatchdetails 
								WHERE
								ware_fabricdispatchdetails.intBulkDispatchNo  =   tb1.intBulkDispatchNo AND
								ware_fabricdispatchdetails.intBulkDispatchNoYear =  tb1.intBulkDispatchNoYear

							),0) as FdammageQty, 
							
							IFNULL((SELECT
								Sum(ware_fabricdispatchdetails.dblCutRetQty)
								FROM
								ware_fabricdispatchdetails 
								WHERE
								ware_fabricdispatchdetails.intBulkDispatchNo  =   tb1.intBulkDispatchNo AND
								ware_fabricdispatchdetails.intBulkDispatchNoYear =  tb1.intBulkDispatchNoYear

							),0) as dblCutRetQty, 
							
							sys_users.strUserName as User,
							tb1.intApproveLevels,
							tb1.intStatus,
							concat(tb1.intOrderNo,'/',tb1.intOrderYear) as Order_No,  
							IFNULL((
                                      SELECT
								concat(sys_users.strUserName,'(',max(ware_fabricdispatchheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_fabricdispatchheader_approvedby
								Inner Join sys_users ON ware_fabricdispatchheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_fabricdispatchheader_approvedby.intBulkDispatchNo  = tb1.intBulkDispatchNo AND
								ware_fabricdispatchheader_approvedby.intYear =  tb1.intBulkDispatchNoYear AND
								ware_fabricdispatchheader_approvedby.intApproveLevelNo =  '1'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.intStatus>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_fabricdispatchheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_fabricdispatchheader_approvedby
								Inner Join sys_users ON ware_fabricdispatchheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_fabricdispatchheader_approvedby.intBulkDispatchNo  = tb1.intBulkDispatchNo AND
								ware_fabricdispatchheader_approvedby.intYear =  tb1.intBulkDispatchNoYear AND
								ware_fabricdispatchheader_approvedby.intApproveLevelNo =  '$i'
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.intStatus>1) AND (tb1.intStatus<=tb1.intApproveLevels) AND ((SELECT
								max(ware_fabricdispatchheader_approvedby.dtApprovedDate)
								FROM
								ware_fabricdispatchheader_approvedby
								Inner Join sys_users ON ware_fabricdispatchheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_fabricdispatchheader_approvedby.intBulkDispatchNo  = tb1.intBulkDispatchNo AND
								ware_fabricdispatchheader_approvedby.intYear =  tb1.intBulkDispatchNoYear AND
								ware_fabricdispatchheader_approvedby.intApproveLevelNo =  ($i-1))<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*false part*/
								 if($i>tb1.intApproveLevels,'-----',''))
								
								/*end IFNULL false part*/
								) as `".$approval."`, "; 
									
								}
								
						$sql .= " 'View' as `View`   
						FROM
						ware_fabricdispatchheader as tb1 
						Inner Join trn_orderdetails as tb2 ON tb1.intOrderNo = tb2.intOrderNo AND tb1.intOrderYear = tb2.intOrderYear
						Inner Join sys_users ON tb1.intCteatedBy = sys_users.intUserId 
						WHERE
						tb1.intCompanyId =  '$location'  
						$where_string
						
						Order by tb1.intBulkDispatchNoYear DESC, tb1.intBulkDispatchNo DESC
						$limit	)  as t where 1=1
						";
					     // 	echo $sql;
$col = array();

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist

$cols[] = $col;	$col=NULL;
//Fabric Receive No
$col["title"] 	= "Dispatch No"; // caption of column
$col["name"] 	= "Dispatch_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= "?q=470&serialNo={Dispatch_No}&year={Dispatch_Year}";	 
$col["linkoptions"] = "target='fabricDispatchNoteListing.php'"; // extra params with <a> tag

$reportLink  = "?q=919&serialNo={Dispatch_No}&year={Dispatch_Year}";
$reportLinkApprove  = "?q=919&serialNo={Dispatch_No}&year={Dispatch_Year}&approveMode=1";

$cols[] = $col;	$col=NULL;


//Fabric Receive Year
$col["title"] = "Dispatch Year"; // caption of column
$col["name"] = "Dispatch_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//CUSTOMER PO NO
$col["title"] = "Style No"; // caption of column
$col["name"] = "strStyleNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Graphic No"; // caption of column
$col["name"] = "strGraphicNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Order No"; // caption of column
$col["name"] = "Order_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Sales Order No"; // caption of column
$col["name"] = "strSalesOrderNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Sample Qty"; // caption of column
$col["name"] = "SampleQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Good Qty"; // caption of column
$col["name"] = "GoodQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "EM-D Qty"; // caption of column
$col["name"] = "EmbroideryQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "P-D Qty"; // caption of column
$col["name"] = "PDammageQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "F-D Qty"; // caption of column
$col["name"] = "FdammageQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Cut Return Qty"; // caption of column
$col["name"] = "dblCutRetQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//DATE
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//FIRST APPROVAL
$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
//$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;

$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='rptFabricDispatchNote.php'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
//$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='rptFabricDispatchNote.php'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}


//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='rptFabricDispatchNote.php'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Fabric Dispatch Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'Dispatch_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);
$out = $jq->render("list1");
?>

<?php include 'include/listing.html'?>

<div align="center" style="margin:10px"><?php echo $out?></div>
<?php
function getMaxApproveLevel()
{
	global $db;

	$appLevel=0;
	$sqlp = "SELECT
			Max(ware_fabricdispatchheader.intApproveLevels) AS appLevel
			FROM ware_fabricdispatchheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
?>
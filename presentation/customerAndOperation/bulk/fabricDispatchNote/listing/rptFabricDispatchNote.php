<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
$intUser  = $_SESSION["userId"];

$serialNo 				= $_REQUEST['serialNo'];
$year 					= $_REQUEST['year'];
$approveMode 			= (!isset($_REQUEST['approveMode'])?'':$_REQUEST['approveMode']);

$programName			= 'Dispatch Note';
$programCode			= 'P0470';
//$FabRecvApproveLevel	= (int)getApproveLevel($programName);

$hide_cum_pd	=get_permission_hide_pd();

  $sql = "SELECT 
ware_fabricdispatchheader.intBulkDispatchNo,
ware_fabricdispatchheader.intBulkDispatchNoYear, 
trn_orderdetails.strStyleNo,
trn_orderdetails.strGraphicNo, 
trn_orderheader.strCustomerPoNo,
ware_fabricdispatchheader.intOrderNo,
ware_fabricdispatchheader.intOrderYear,
ware_fabricdispatchheader.strRemarks,
ware_fabricdispatchheader.intStatus,
ware_fabricdispatchheader.intApproveLevels,
ware_fabricdispatchheader.dtmdate,
ware_fabricdispatchheader.dtmCreateDate,
trn_orderheader.strContactPerson, 
sys_users.strUserName,
mst_customer.strName as customer ,
ware_fabricdispatchheader.intCompanyId , 
mst_customer_locations_header.strName as customerLocation ,
mst_customer_locations_header.ADDRESS as customerLocationAddress,
mst_customer.strAddress as customerAddress 
FROM
ware_fabricdispatchheader  
Inner Join trn_orderheader ON ware_fabricdispatchheader.intOrderNo = trn_orderheader.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderheader.intOrderYear
Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
left Join mst_customer ON trn_orderheader.intCustomer = mst_customer.intId 
left Join sys_users ON ware_fabricdispatchheader.intCteatedBy = sys_users.intUserId 
left Join mst_customer_locations ON ware_fabricdispatchheader.intCustLocation = mst_customer_locations.intLocationId AND  trn_orderheader.intCustomer = mst_customer_locations.intCustomerId 
Inner Join mst_customer_locations_header ON mst_customer_locations.intLocationId = mst_customer_locations_header.intId 
WHERE
ware_fabricdispatchheader.intBulkDispatchNo =  '$serialNo' AND
ware_fabricdispatchheader.intBulkDispatchNoYear =  '$year' ";


				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 { 
					$serialNo = $row['intBulkDispatchNo'];
					$SerialYear = $row['intBulkDispatchNoYear'];
					$style = $row['strStyleNo'];
					$graphicNo=$row['strGraphicNo'];
					$custPO = $row['strCustomerPoNo'];
					$orderNo = $row['intOrderNo'];
					$orderYear = $row['intOrderYear'];
					$date = $row['dtmdate'];
					$customer = $row['customer'];
					$customerLocation = $row['customerLocation'];
					$customerLocationAddress = $row['customerLocationAddress'];
					$customerAddress = $row['customerAddress'];
					$remarks1 = $row['strRemarks'];
					//$date = $row['dtmCreateDate'];
					$intStatus = $row['intStatus'];
					$savedLevels = $row['intApproveLevels'];
					$user = $row['strUserName'];
					$contactPerson = $row['strContactPerson'];
					$locationId = $row['intCompanyId'];//this locationId use in report header(reportHeader.php)--------------------
				 }
				 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Fabric Dispatch Report</title>
<script type="text/javascript" src="presentation/customerAndOperation/bulk/fabricDispatchNote/listing/rptFabricDispatchNote-js.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:283px;
	top:171px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="images/pending.png"  /></div>
<?php
}
?>
<form id="frmFabDispatchReport" name="frmFabDispatchReport" method="post" action="rptFabricDispatchNote.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="6"></td>
</tr>
<tr>
<td width="12%"></td>
<td width="58%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="6%"><a target="rptFabricDispatchNote_pdf.php" href="presentation/customerAndOperation/bulk/fabricDispatchNote/listing/rptFabricDispatchNote_pdf.php?serialNo=<?php echo $serialNo?>&year=<?php echo $year?>"><img src="images/pdf_icon.png" class="noPrint"/></a></td>
<td width="6%"><a target="rptFabricDispatchNote_excel.php" href="presentation/customerAndOperation/bulk/fabricDispatchNote/listing/rptFabricDispatchNote_excel.php?serialNo=<?php echo $serialNo?>&year=<?php echo $year?>"><img src="images/excel.PNG" class="noPrint"  style="width: 50px; width: 50px;<?php if($intStatus!=1){ ?>display: none <?php } ?>"/></a></td>
<td width="5%"><a target="rptFabricDispatchNote_excel_csv.php" href="presentation/customerAndOperation/bulk/fabricDispatchNote/listing/rptFabricDispatchNote_excel_csv.php?serialNo=<?php echo $serialNo?>&year=<?php echo $year?>&type=CSV"><img src="images/csv.PNG" class="noPrint" style="width: 50px; width: 50px;<?php if($intStatus!=1){ ?>display: none <?php } ?>" /></a></td>
<td width="13%">&nbsp;</td>

</tr>

<tr>
<td colspan="6"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong> DISPATCH REPORT</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="10" align="center" bgcolor="#FFDFCB">
    <?php
	if($intStatus>1)
	{
	
	//------------
	$k=$savedLevels+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
	
	 $resultp = $db->RunQuery($sqlp);
	 $rowp=mysqli_fetch_array($resultp);
	 $userPermission=0;
	 if($rowp['int'.$k.'Approval']==1){
		 $userPermission=1;
	 }
	//--------------	
	?>
    <?php if(($approveMode==1) and ($userPermission==1)) { ?>
    <img src="images/approve.png" align="middle" class="noPrint mouseover" id="imgApprove" />
    <img src="images/reject.png" align="middle" class="noPrint mouseover" id="imgReject" />
    <?php
	}
	}
	?></td>
  </tr>
  <tr>
  <?php
 	if($intStatus==1)
	{
	?>
   <td colspan="10" class="APPROVE" >CONFIRMED</td>
   <?PHP
	}
	else if($intStatus==0)
	{
   ?>
   <td colspan="10" class="APPROVE" style="color:#F00">REJECTED</td>
   <?php
	}
	else if($intStatus==-10 || $intStatus==-2)
	{
   ?>
   <td colspan="10" class="APPROVE" style="color:#F00">CANCELLED</td>
   <?php
	}
	else
	{
   ?>
   <td colspan="10" class="APPROVE">PENDING</td>
   <?php
	}
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="15%"><span class="normalfnt"><strong>Fabric Dispatch No</strong></span></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="12%"><span class="normalfnt"><?php echo $serialNo  ?>/<?php echo $SerialYear ?></span></td>
    <td width="12%" class="normalfnt"><strong>Order No</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="13%"><span class="normalfnt"><?php echo $orderNo."/".$orderYear; ?></span></td>
    <td width="9%" class="normalfnt"><div id="divSerialNo" style="display:none"><?php echo $serialNo ?>/<?php echo $year ?></div>
      <strong>Customer</strong></td>
    <td width="1%"><strong>:</strong></td>
  <td width="16%"><span class="normalfnt"><?php echo $customer; ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Customer PO</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $custPO   ?></span></td>
    <td class="normalfnt"><strong>By</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $user; ?></span></td>
    <td class="normalfnt"><strong>Location</strong></td>
    <td><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $customerLocation; ?></span></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Date</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $date; ?></span></td>
    <td class="normalfnt"><strong>Contact Person</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $contactPerson; ?></span></td>
	  <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
<tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Billing Address</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td class="normalfnt" colspan="5"><?php echo $customerAddress; ?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>  
<tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Delivery Address</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td class="normalfnt" colspan="5"><?php echo $customerLocationAddress; ?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="">
              <th width="8%" >Style No</th>
              <th width="10%" >Graphic No</th>
              <th width="9%" >Sales Order No</th>
              <th width="4%" >Part</th>
              <th width="11%" >Background Color</th>
              <th width="4%" >Line No</th>
              <th width="4%" >Cut No</th>
              <th width="4%" >Size</th>
              <th width="7%" >Sample Qty</th>
              <th width="6%" >Good Qty</th>
              <th width="6%" >EMD-D Qty</th>
              <th width="6%" >P-D Qty</th>
              <th width="5%" >F-D Qty</th>
              <th width="5%" >Cut Return Qty</th>
              <th width="5%" >Total Qty</th>
              <th width="11%" <?php if($hide_cum_pd==1){?>style="display:none" <?php } ?> >Cumulative PD% (So/Size wise)</th>
              <th width="11%" <?php if($hide_cum_pd==1){?>style="display:none" <?php } ?> >Cumulative PD% (So wise)</th>
               <th width="11%" >Remarks</th>
              </tr>
              <?php 
	  	    $sql1 = "SELECT 
trn_orderdetails.strStyleNo,
trn_orderdetails.strGraphicNo, 
trn_orderdetails.strSalesOrderNo,
trn_orderdetails.intSalesOrderId,
ware_fabricdispatchdetails.strCutNo,
mst_part.strName as part,
mst_colors_ground.strName as bgcolor,
ware_fabricdispatchdetails.strLineNo,
ware_fabricdispatchdetails.strSize,
ware_fabricdispatchdetails.dblSampleQty,
ware_fabricdispatchdetails.dblGoodQty,
ware_fabricdispatchdetails.dblEmbroideryQty,
ware_fabricdispatchdetails.dblPDammageQty,
ware_fabricdispatchdetails.dblFDammageQty ,  
IFNULL(ware_fabricdispatchdetails.dblCutRetQty,0) as dblCutRetQty ,  
ware_fabricdispatchdetails.strRemarks  
FROM
trn_orderdetails
Inner Join ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear
Inner Join ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
left Join mst_part ON ware_fabricdispatchdetails.intPart = mst_part.intId
left Join mst_colors_ground ON ware_fabricdispatchdetails.intGroundColor = mst_colors_ground.intId
WHERE
ware_fabricdispatchheader.intBulkDispatchNo =  '$serialNo' AND
ware_fabricdispatchheader.intBulkDispatchNoYear =  '$SerialYear' 
";
			$result1 = $db->RunQuery($sql1);
			$totQty				= 0;
			$totAmmount			= 0;
			$totsampleQty		= 0;
			$totgoodQty			= 0;
			$totembroideryQty	= 0;
			$totpDammageQty		= 0;
			$totfDammageQty		= 0;
			$totcutRetQty		= 0;
			$total				= 0;
			while($row=mysqli_fetch_array($result1))
			{
				$cutNo			=$row['strCutNo'];
				$styleNo		=$row['strStyleNo'];
				$graphicNo		=$row['strGraphicNo'];
				$salesOrderNo	=$row['strSalesOrderNo'];
				$salesOrderId	=$row['intSalesOrderId'];
				$part			=$row['part'];
				$bgColor		=$row['bgcolor'];
				$lineNo			=$row['strLineNo'];
				$size			=$row['strSize'];
				$sampleQty		=$row['dblSampleQty'];
				$goodQty		=$row['dblGoodQty'];
				$embroideryQty	=$row['dblEmbroideryQty'];
				$pDammageQty	=$row['dblPDammageQty'];
				$fDammageQty	=$row['dblFDammageQty'];
				$cutRetQty		=$row['dblCutRetQty'];
				$remarks		=$row['strRemarks'];
				$tot=(float)($sampleQty)+(float)($goodQty)+(float)($embroideryQty)+(float)($pDammageQty)+(float)($fDammageQty)+(float)($cutRetQty);
				$data	=get_cumulative_pd($orderNo,$orderYear,$salesOrderId,$size);
				$extra_flg			=$data['extra_app_levels']; 
				$extra_flg_s		=$data['extra_app_levels2']; 
				$cum_pd				=$data['pd_percentage'];
				$cum_pd_so			=$data['pd_percentage_so'];
	  ?>
            <tr class="normalfnt"   bgcolor="#FFFFFF">
              <td align="center" class="normalfntMid" id="<?php echo $styleNo; ?>" ><?php echo $styleNo; ?></td>
              <td align="center" class="normalfntMid" id="<?php echo $graphicNo; ?>" ><?php echo $graphicNo; ?></td>
             <td align="center" class="normalfntMid" id="<?php echo $salesOrderNo; ?>" ><?php echo $salesOrderNo; ?></td>
			<td align="center" class="normalfntMid" id="<?php echo $part; ?>"><?php echo $part; ?></td>
			<td align="center" class="normalfntMid" id="<?php echo $bgColor; ?>" ><?php echo $bgColor; ?></td>
			<td align="center" class="normalfntMid" id="<?php echo $lineNo; ?>" ><?php echo $lineNo; ?></td>
			<td align="center" class="normalfntMid" id="<?php echo $size; ?>" ><?php echo $cutNo; ?></td>
			<td align="center" class="normalfntMid" id="<?php echo $size; ?>" ><?php echo $size; ?></td>
			<td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $sampleQty; ?></td>
			<td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $goodQty; ?></td>
			<td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $embroideryQty; ?></td>
			<td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $pDammageQty; ?></td>
			<td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $fDammageQty; ?></td>
			<td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $cutRetQty; ?></td>
			<td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $tot; ?></td>
			<td align="center" <?php if($hide_cum_pd==1){?>style="display:none"<?php }?>  class="normalfntRight" id="<?php echo $cum_pd; ?>"><font <?php if($extra_flg>0){ ?>color=#DD0509 <?php } else {?> color=#000000 <?php } ?> ><?php echo $cum_pd; ?></font></td>
			<td align="center" <?php if($hide_cum_pd==1){?>style="display:none"<?php }?>  class="normalfntRight" id="<?php echo $cum_pd_so; ?>"><font <?php if($extra_flg_s>0){ ?>color=#DD0509 <?php } else {?> color=#000000 <?php } ?> ><?php echo $cum_pd_so; ?></font></td>
           <td align="center" class="normalfntRight" id="<?php echo $remarks; ?>"><?php echo $remarks; ?></td>
            </tr>              
      <?php 
			$totsampleQty+=$sampleQty;
			$totgoodQty+=$goodQty;
			$totembroideryQty+=$embroideryQty;
			$totpDammageQty+=$pDammageQty;
			$totfDammageQty+=$fDammageQty;
			$totcutRetQty+=$cutRetQty;
			$total+=$tot;
			}
	  ?>
            <tr class="normalfnt">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" ><b><?php echo $totsampleQty ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totgoodQty ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totembroideryQty ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totpDammageQty ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totfDammageQty ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totcutRetQty ?></b></td>
              <td class="normalfntRight" ><b><?php echo $total ?></b></td>
              <td class="normalfntRight" ></td>
              <td class="normalfntRight" ></td>
                <td class="normalfntRight" ></td>
              </tr>
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<?php  
 	if($intStatus!=0)
	{

				for($i=1; $i<=$savedLevels; $i++)
				{
					   $sqlc = "SELECT
							ware_fabricdispatchheader_approvedby.intApproveUser,
							ware_fabricdispatchheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName, 
							ware_fabricdispatchheader_approvedby.intApproveLevelNo
							FROM
							ware_fabricdispatchheader_approvedby
							Inner Join sys_users ON ware_fabricdispatchheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_fabricdispatchheader_approvedby.intBulkDispatchNo =  '$serialNo' AND
							ware_fabricdispatchheader_approvedby.intYear =  '$SerialYear'  AND
							ware_fabricdispatchheader_approvedby.intApproveLevelNo =  '$i'  order by intApproveLevelNo asc
";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
						if($i==1)
						$desc="1st ";
						else if($i==2)
						$desc="2nd ";
						else if($i==3)
						$desc="3rd ";
						else
						$desc=$i."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
				?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
            </tr>
<?php
			}
	}
	else{
					 $sqlc = "SELECT
							ware_fabricdispatchheader_approvedby.intApproveUser,
							ware_fabricdispatchheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_fabricdispatchheader_approvedby.intApproveLevelNo
							FROM
							ware_fabricdispatchheader_approvedby
							Inner Join sys_users ON ware_fabricdispatchheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_fabricdispatchheader_approvedby.intBulkDispatchNo =  '$serialNo' AND
							ware_fabricdispatchheader_approvedby.intYear =  '$SerialYear'  AND
							ware_fabricdispatchheader_approvedby.intApproveLevelNo =  '0'";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
					  ?>
            <tr>
                <td bgcolor="#FFFFFF" height="21"> </td>
            </tr>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Rejected By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
	}
?>

<tr>
                <td><table width="100%">
  <tr>
    <td width="7%" valign="top" bgcolor="#FFFFFF" class="normalfnt"><strong>Remarks</strong></td>
    <td width="2%" valign="top" bgcolor="#FFFFFF" class="normalfnt"><strong>:</strong></td>
    <td width="91%" colspan="7" bgcolor="#FFFFFF"><span class="normalfnt"><textarea cols="50" rows="6" class="textarea" style="width::300px" disabled="disabled"><?php echo $remarks1  ?></textarea></span></td>
    </tr>
                </table></td>
      </tr>            

                <tr>
                    <td bgcolor="#FFFFFF" height="21"></td>
      </tr>
                <tr>
                <td bgcolor="#FFFFFF"><table width="100%" >
                <tr>
                <td width="10%" bgcolor="#FFFFFF"><span class="normalfnt"><strong>Delivered By</strong></span></td>
                <td width="1%" bgcolor="#FFFFFF"><span class="normalfnt"><strong>:</strong></span></td>
                <td width="40%" bgcolor="#FFFFFF"><span class="normalfnt">-------------------------------------</span></td>
                <td width="11%" bgcolor="#FFFFFF"><span class="normalfnt"><strong>Prepared By</strong></span></td>
                <td width="1%" bgcolor="#FFFFFF"><span class="normalfnt"><strong>:</strong></span></td>
                <td width="37%" bgcolor="#FFFFFF"><span class="normalfnt"><?php echo $user ?></span></td>
                </tr>
                <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong>Checked By</strong></span></td>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong>:</strong></span></td>
                <td bgcolor="#FFFFFF"><span class="normalfnt">-------------------------------------</span></td>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong>Confirmed By</strong></span></td>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong>:</strong></span></td>
                <td width="37%" bgcolor="#FFFFFF"><span class="normalfnt">--------------------------------</span></td>
                </tr>
                </table></td>
            </tr>
  <tr><td bgcolor="#FFFFFF"><span class="normalfnt"><strong>If You Have Any Discrepancies With Regard To Above Quantities, Please Reply Within Twenty Four Hours. Otherwise This Will Be Taken
As Confirm.</strong></span></td></tr>          
            
            
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php
function get_cumulative_pd($orderNo,$orderYear,$salesOrderId,$size){
	global $db;	
	
$sqlc	="SELECT
			dh.intOrderNo,
			dh.intOrderYear,
			(select sum(od.intQty) from trn_orderdetails as od where od.intOrderNo=dh.intOrderNo and od.intOrderYear=dh.intOrderYear and od.intSalesOrderId=dd.intSalesOrderId) as orderQty,
			Sum(ifnull(dd.dblPDammageQty,0)) AS damage,
			Sum(ifnull(dd.dblGoodQty,0)+ifnull(dd.dblEmbroideryQty,0)
			+ifnull(dd.dblPDammageQty,0)+ifnull(dd.dblFdammageQty,0)
			+ifnull(dd.dblSampleQty,0)+ifnull(dd.dblCutRetQty,0)) AS dblDispQty,
			od.dblDamagePercentage 
			FROM
			ware_fabricdispatchdetails AS dd
			INNER JOIN ware_fabricdispatchheader AS dh ON dd.intBulkDispatchNo = dh.intBulkDispatchNo AND dd.intBulkDispatchNoYear = dh.intBulkDispatchNoYear
			INNER JOIN trn_orderdetails AS od ON dh.intOrderNo = od.intOrderNo AND dh.intOrderYear = od.intOrderYear AND dd.intSalesOrderId = od.intSalesOrderId
			WHERE
			dh.intOrderNo = '$orderNo' AND
			dh.intOrderYear = '$orderYear'  AND
			dd.intSalesOrderId = '$salesOrderId'  AND
			dd.strSize = '$size' 
			group by 
			dd.intSalesOrderId,
			dd.strSize ";
	$resultc 			= $db->RunQuery($sqlc);
	$row				=mysqli_fetch_array($resultc);	
	
	$sql_so	="SELECT
			dh.intOrderNo,
			dh.intOrderYear,
			(select sum(od.intQty) from trn_orderdetails as od where od.intOrderNo=dh.intOrderNo and od.intOrderYear=dh.intOrderYear and od.intSalesOrderId=dd.intSalesOrderId) as orderQty,
			Sum(ifnull(dd.dblPDammageQty,0)) AS damage,
			Sum(ifnull(dd.dblGoodQty,0)+ifnull(dd.dblEmbroideryQty,0)
			+ifnull(dd.dblPDammageQty,0)+ifnull(dd.dblFdammageQty,0)
			+ifnull(dd.dblSampleQty,0)+ifnull(dd.dblCutRetQty,0)) AS dblDispQty,
			od.dblDamagePercentage 
			FROM
			ware_fabricdispatchdetails AS dd
			INNER JOIN ware_fabricdispatchheader AS dh ON dd.intBulkDispatchNo = dh.intBulkDispatchNo AND dd.intBulkDispatchNoYear = dh.intBulkDispatchNoYear
			INNER JOIN trn_orderdetails AS od ON dh.intOrderNo = od.intOrderNo AND dh.intOrderYear = od.intOrderYear AND dd.intSalesOrderId = od.intSalesOrderId
			WHERE
			dh.intOrderNo = '$orderNo' AND
			dh.intOrderYear = '$orderYear'  AND
			dd.intSalesOrderId = '$salesOrderId'
			group by 
			dd.intSalesOrderId ";
	$result_so 			= $db->RunQuery($sql_so);
	$row_s				=mysqli_fetch_array($result_so);
	$extra_app_levels	=0;
	$extra_app_levels2	=0;
	$pd_percentage		= round($row['damage']/$row['dblDispQty']*100,2);
	$pd_percentage_so   = round($row_s['damage']/$row_s['dblDispQty']*100,2); 
	if($pd_percentage > $row['dblDamagePercentage'])
		$extra_app_levels= 1;
	if($pd_percentage_so > $row_s['dblDamagePercentage'])
		$extra_app_levels2= 1;
	
	$data['extra_app_levels']	=$extra_app_levels;
	$data['extra_app_levels2']	=$extra_app_levels2;
	$data['pd_percentage']	=$pd_percentage;
	$data['pd_percentage_so']	=$pd_percentage_so;
	return $data;
}
		
function get_permission_hide_pd(){
	global $db;
	global $intUser;
	$sql = "SELECT
				menus_special.intStatus
FROM
				menus_special
				Inner Join menus_special_permision ON menus_special.intId = menus_special_permision.intSpMenuId
WHERE
menus_special_permision.intUser =  '$intUser' AND
menus_special.strPermisionType =  'Permission hide cumulative pd percentage'";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		return $row['intStatus'];
}

?>

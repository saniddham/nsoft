<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
ini_set('display_errors',1);
session_start();
$backwardseperator 		= "../../../../../";
$companyId 				= $_SESSION['CompanyID'];
$intUser  				= $_SESSION["userId"];
$mainPath 				= $_SESSION['mainPath'];
$thisFilePath 			= $_SERVER['PHP_SELF'];

$serialNo 				= $_REQUEST['serialNo'];
$serialYear 			= $_REQUEST['year'];
$approveMode 			= $_REQUEST['approveMode'];

$programName			='Dispatch Note';
$programCode			='P0470';


include_once	"../../../../../dataAccess/DBManager2.php";								$db										= new DBManager2();
include_once	"../../../../../class/dateTime.php";									$cls_dateTime							= new cls_dateTime($db);
include_once	"../../../../../class/tables/ware_fabricdispatchheader.php";			$ware_fabricdispatchheader				= new ware_fabricdispatchheader($db);
include_once	"../../../../../class/tables/ware_fabricdispatchdetails.php";			$ware_fabricdispatchdetails				= new ware_fabricdispatchdetails($db);
include_once	"../../../../../class/tables/ware_fabricdispatchheader_approvedby.php";	$ware_fabricdispatchheader_approvedby	= new ware_fabricdispatchheader_approvedby($db);
include_once	"../../../../../class/tables/sys_users.php";							$sys_users								= new sys_users($db);
include_once	"../../../../../class/tables/trn_orderheader.php";						$trn_orderheader						= new trn_orderheader($db);
include_once	"../../../../../class/tables/trn_orderdetails.php";						$trn_orderdetails						= new trn_orderdetails($db);
include_once	"../../../../../class/tables/mst_customer.php";							$mst_customer							= new mst_customer($db);
include_once	"../../../../../class/tables/mst_customer_locations.php";				$mst_customer_locations					= new mst_customer_locations($db);
include_once	"../../../../../class/tables/mst_customer_locations_header.php";		$mst_customer_locations_header			= new mst_customer_locations_header($db);
include_once	"../../../../../class/tables/menupermision.php";						$menupermision							= new menupermision($db);
include_once	"../../../../../class/tables/mst_part.php";								$mst_part								= new mst_part($db);
include_once	"../../../../../class/tables/mst_colors_ground.php";					$mst_colors_ground						= new mst_colors_ground($db);

$db->connect();

//-------------------GET HEADER DETAILS-------------------------
 	$ware_fabricdispatchheader->set($serialNo,$serialYear);
	$locationId			=$ware_fabricdispatchheader->getintCompanyId();
  	$intOrderNo			=$ware_fabricdispatchheader->getintOrderNo();
  	$intOrderYear		=$ware_fabricdispatchheader->getintOrderYear();
  	$strRemarks			=$ware_fabricdispatchheader->getstrRemarks();
  	$intStatus			=$ware_fabricdispatchheader->getintStatus();
  	$intApproveLevels	=$ware_fabricdispatchheader->getintApproveLevels();
  	$dtmdate			=$ware_fabricdispatchheader->getdtmdate();
  	$dtmCreateDate		=$ware_fabricdispatchheader->getdtmCreateDate();
  	$intCteatedBy		=$ware_fabricdispatchheader->getintCteatedBy();
	$intCompanyId		=$ware_fabricdispatchheader->getintCompanyId();
  	$intCustLocation	=$ware_fabricdispatchheader->getintCustLocation();
	
	$trn_orderheader->set($intOrderNo,$intOrderYear);
	$intCustomer		=$trn_orderheader->getintCustomer();
  	$strCustomerPoNo	=$trn_orderheader->getstrCustomerPoNo();
	$strContactPerson	=$trn_orderheader->getstrContactPerson();
	
	$mst_customer->set($intCustomer);
  	$customerName		=$mst_customer->getstrName();
	
	$mst_customer_locations->set($intCustLocation,$intCustomer);
	$cust_loc_id		=$mst_customer_locations->getintLocationId();
	
	$mst_customer_locations_header->set($cust_loc_id);
	$strCustLocation	=$mst_customer_locations_header->getstrName();
	
	$sys_users->set($intCteatedBy);
	$strCteatedBy		=$sys_users->getstrUserName();
//---------------------------------------------------
 
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Fabric Dispatch Report</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../../../../css/button.css"/>
 
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="rptFabricDispatchNote-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:283px;
	top:171px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="../../../../../images/pending.png"  /></div>
<?php
}
?>
<form id="frmFabDispatchReport" name="frmFabDispatchReport" method="post" action="rptFabricDispatchNote.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../../../reportHeader.php'?></td>
<td width="20%"><a target="rptFabricDispatchNote_pdf.php" href="rptFabricDispatchNote_pdf.php?serialNo=<?php echo $serialNo?>&year=<?php echo $serialYear?>"><img src="../../../../../images/pdf_icon.png" class="noPrint"/></a></td>
</tr>
 <tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong> DISPATCH REPORT</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="10" align="center" bgcolor="#FFDFCB">
    <?php
	if($intStatus>1)
	{
 		$k				=$intApproveLevels+2-$intStatus;
		$function		=	'getint'.$k.'Approval';
		$menupermision->set($programCode,$intUser);
		$userPermission	=$menupermision->$function();

		if(($approveMode==1) and ($userPermission==1)) { ?>
        <a class="button white medium" id="imgApprove" name="imgApprove">Approve</a>
        <a class="button white medium" id="imgReject" name="imgReject">Reject</a>
        <?php  
		}
	}
	?></td>
  </tr>
  <tr>
  <?php
 	if($intStatus==1){?>  <td colspan="10" class="APPROVE" >CONFIRMED</td> <?php } else if($intStatus==0) { ?>
   <td colspan="10" class="APPROVE" style="color:#F00">REJECTED</td>
     <?php } else { ?> <td colspan="10" class="APPROVE">PENDING</td> <?php } ?>
  </tr>
  
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="15%"><span class="normalfnt"><strong>Fabric Dispatch No</strong></span></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="12%"><span class="normalfnt"><?php echo $serialNo  ?>/<?php echo $serialYear ?></span></td>
    <td width="12%" class="normalfnt"><strong>Order No</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="13%"><span class="normalfnt"><?php echo $intOrderNo."/".$intOrderYear; ?></span></td>
    <td width="9%" class="normalfnt"><div id="divSerialNo" style="display:none"><?php echo $serialNo ?>/<?php echo $serialYear ?></div>
      <strong>Customer</strong></td>
    <td width="1%"><strong>:</strong></td>
  <td width="16%"><span class="normalfnt"><?php echo $customerName; ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Customer PO</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $strCustomerPoNo   ?></span></td>
    <td class="normalfnt"><strong>By</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $strCteatedBy; ?></span></td>
    <td class="normalfnt"><strong>Location</strong></td>
    <td><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $strCustLocation; ?></span></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Date</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $dtmdate; ?></span></td>
    <td class="normalfnt"><strong>Contact Person</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $strContactPerson; ?></span></td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
<tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>  
  
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr >
              <th width="8%" ><strong>Style No</strong></th>
              <th width="10%" ><strong>Graphic No</strong></th>
              <th width="9%" ><strong>Sales Order No</strong></th>
              <th width="4%" ><strong>Part</strong></th>
              <th width="11%" ><strong>Background Color</strong></th>
              <th width="4%" ><strong>Line No</strong></th>
              <th width="4%" ><strong>Cut No</strong></th>
              <th width="4%" ><strong>Size</strong></th>
              <th width="7%" ><strong>Sample Qty</strong></th>
              <th width="6%" ><strong>Good Qty</strong></th>
              <th width="6%" ><strong>EMD-D Qty</strong></th>
              <th width="6%" ><strong>P-D Qty</strong></th>
              <th width="5%" ><strong>F-D Qty</strong></th>
              <th width="5%" ><strong>Cut Return Qty</strong></th>
              <th width="5%" ><strong>Total Qty</strong></th>
              <th width="11%" ><strong>Remarks</strong></th>
              </tr>
              <?php 
				$totQty				=0;
				$totAmmount			=0;
				
				$where				=	"intBulkDispatchNo = '$serialNo' AND intBulkDispatchNoYear = '$serialYear'";
				
				$details_results	=	$ware_fabricdispatchdetails->select('*',NULL,$where,NULL,NULL);
				while($row=mysqli_fetch_array($details_results))
				{ 
				
					$cutNo				=$row['strCutNo'];
					$salesOrder			=$row['intSalesOrderId'];
					$size				=$row['strSize'];
					$sampleQty			=$row['dblSampleQty'];
					$goodQty			=$row['dblGoodQty'];
					$embroideryQty		=$row['dblEmbroideryQty'];
					$pDammageQty		=$row['dblPDammageQty'];
					$fDammageQty		=$row['dblFDammageQty'];
					$cutRetQty			=$row['dblCutRetQty'];
					$lineNo				=$row['strLineNo'];
					$intPart			=$row['intPart'];
					$remarks			=$row['strRemarks'];
					$intGroundColor		=$row['intGroundColor'];
				
					$trn_orderdetails->set($intOrderNo,$intOrderYear,$salesOrder);
					$styleNo			=$trn_orderdetails->getstrStyleNo();
					$graphicNo			=$trn_orderdetails->getstrGraphicNo();
					$salesOrderNo		=$trn_orderdetails->getstrSalesOrderNo();
					
					$mst_part->set($intPart);
					$part				=$mst_part->getstrName();
					$mst_colors_ground->set($intGroundColor);
					$bgColor			=$mst_colors_ground->getstrName();
				
 					//$tot				=val($sampleQty)+val($goodQty)+val($embroideryQty)+val($pDammageQty)+val($fDammageQty)+val($cutRetQty);
	  ?>
           	 <tr class="normalfnt"   bgcolor="#FFFFFF">
                <td align="center" class="normalfntMid" id="<?php echo $styleNo; ?>" ><?php echo $styleNo; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $graphicNo; ?>" ><?php echo $graphicNo; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $salesOrderNo; ?>" ><?php echo $salesOrderNo; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $part; ?>"><?php echo $part; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $bgColor; ?>" ><?php echo $bgColor; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $lineNo; ?>" ><?php echo $lineNo; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $cutNo; ?>" ><?php echo $cutNo; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $size; ?>" ><?php echo $size; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $sampleQty; ?>"><?php echo $sampleQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $goodQty; ?>"><?php echo $goodQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $embroideryQty; ?>"><?php echo $embroideryQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $pDammageQty; ?>"><?php echo $pDammageQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $fDammageQty; ?>"><?php echo $fDammageQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $cutRetQty; ?>"><?php echo $cutRetQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $tot; ?>"><?php echo $tot; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $remarks; ?>"><?php echo $remarks; ?></td>
            </tr>              
      <?php 
			$totsampleQty+=$sampleQty;
			$totgoodQty+=$goodQty;
			$totembroideryQty+=$embroideryQty;
			$totpDammageQty+=$pDammageQty;
			$totfDammageQty+=$fDammageQty;
			$totcutRetQty+=$cutRetQty;
			$total+=$tot;
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" ><b><?php echo $totsampleQty ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totgoodQty ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totembroideryQty ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totpDammageQty ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totfDammageQty ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totcutRetQty ?></b></td>
              <td class="normalfntRight" ><b><?php echo $total ?></b></td>
              <td class="normalfntRight" ></td>
              </tr>
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<?php 
//---APPROVED BY DETAILS-------------------------------------
	$ware_fabricdispatchheader->set($serialNo,$serialYear);
	$header_app_levels	=	$ware_fabricdispatchheader->getintApproveLevels();
	$header_status		=	$ware_fabricdispatchheader->getintStatus();
	$where				=	"intBulkDispatchNo = '$serialNo' AND intYear = '$serialYear'";
	$order				=	"dtApprovedDate ASC";
	
	$app_by_results 	=	$ware_fabricdispatchheader_approvedby->select('*',NULL,$where,$order,NULL);
	 while($row=mysqli_fetch_array($app_by_results))
	 { 
	 	$app_by_level		=	$row['intApproveLevelNo'];
	 	$intStatus			=	$row['intStatus'];
		//$ware_fabricdispatchheader_approvedby->set($serialNo,$serialYear,$app_by_level,$intStatus);
		$app_by_user_id		=   $row['intApproveUser'];
		$app_by_date		=   $row['dtApprovedDate'];
 		
		$app_by_user_name	=	$sys_users->set($app_by_user_id);
		$app_by_user_name	=	$sys_users->getstrUserName();
	
		if($app_by_level==-10)
			$desc	= 'Completed By';
		else if($app_by_level==-2)
			$desc	= 'Canceled By';
		else if($app_by_level==-1)
			$desc	= 'Revised By';
		else if($app_by_level==-0)
			$desc	= 'Rejected By';
		else 
			$desc	= $app_by_level.ordinal_suffix($app_by_level).' Approved By';
	?>	
		<tr>
		<td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> - </strong></span><span class="normalfnt"><?php echo $app_by_user_name."(".$app_by_date.")" ;?></span></td>
		</tr>
	<?php	
	 }
	 ?>
     
     <?php
 	 	for($i=$header_status; $i >1 ; $i--){
 			$to_app_by_level	=	(int)$header_app_levels+2-(int)$i;
			$desc				= $to_app_by_level.ordinal_suffix($to_app_by_level).' Approved By';
	 ?>
		<tr>
		<td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> - </strong></span><span class="normalfnt">----------------</span></td>
		</tr>
     <?php
		}
		
//---END OF APPROVED BY DETAILS-------------------------------------
	 ?>
    <tr><td>
    <table width="100%">
      <tr>
        <td width="7%" valign="top" bgcolor="#FFFFFF" class="normalfnt"><strong>Remarks</strong></td>
        <td width="2%" valign="top" bgcolor="#FFFFFF" class="normalfnt"><strong>:</strong></td>
        <td width="91%" colspan="7" bgcolor="#FFFFFF"><span class="normalfnt"><textarea cols="50" rows="6" class="textarea" style="width::300px" disabled="disabled"><?php echo $remarks1  ?></textarea></span></td>
        </tr>
    </table>
    </td>
    </tr>            

    <tr>
        <td bgcolor="#FFFFFF" height="21"></td>
    </tr>
    <tr>
    <td bgcolor="#FFFFFF"><table width="100%" >
    <tr>
    <td width="10%" bgcolor="#FFFFFF"><span class="normalfnt"><strong>Delivered By</strong></span></td>
    <td width="1%" bgcolor="#FFFFFF"><span class="normalfnt"><strong>:</strong></span></td>
    <td width="40%" bgcolor="#FFFFFF"><span class="normalfnt">-------------------------------------</span></td>
    <td width="11%" bgcolor="#FFFFFF"><span class="normalfnt"><strong>Prepared By</strong></span></td>
    <td width="1%" bgcolor="#FFFFFF"><span class="normalfnt"><strong>:</strong></span></td>
    <td width="37%" bgcolor="#FFFFFF"><span class="normalfnt"><?php echo $user ?></span></td>
    </tr>
    <tr>
    <td bgcolor="#FFFFFF"><span class="normalfnt"><strong>Checked By</strong></span></td>
    <td bgcolor="#FFFFFF"><span class="normalfnt"><strong>:</strong></span></td>
    <td bgcolor="#FFFFFF"><span class="normalfnt">-------------------------------------</span></td>
    <td bgcolor="#FFFFFF"><span class="normalfnt"><strong>Confirmed By</strong></span></td>
    <td bgcolor="#FFFFFF"><span class="normalfnt"><strong>:</strong></span></td>
    <td width="37%" bgcolor="#FFFFFF"><span class="normalfnt">--------------------------------</span></td>
    </tr>
    </table></td>
    </tr>
  <tr><td bgcolor="#FFFFFF"><span class="normalfnt"><strong>If You Have Any Discrepancies With Regard To Above Quantities, Please Reply Within Twenty Four Hours. Otherwise This Will Be Taken
As Confirm.</strong></span></td></tr>          
             
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo $cls_dateTime->getCurruntDateTime(); ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php

	$db->disconnect();	
	
function ordinal_suffix($num){
	if($num < 11 || $num > 13){
		 switch($num % 10){
			case 1: return 'st';
			case 2: return 'nd';
			case 3: return 'rd';
		}
	}
	return 'th';
}

?>
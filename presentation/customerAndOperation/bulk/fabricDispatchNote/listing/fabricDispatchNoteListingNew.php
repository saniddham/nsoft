<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
    <link rel="stylesheet" type="text/css" href="libraries/dataTables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="libraries/dataTables/css/buttons.dataTables.min.css">
<!--    <script type="text/javascript" src="http://www.bacubacu.com/colresizable/js/colResizable-1.5.min.js"></script>-->
    <script type="text/javascript" src="libraries/dataTables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="libraries/dataTables/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="libraries/dataTables/buttons.flash.min.js"></script>
    <script type="text/javascript" src="libraries/dataTables/jszip.min.js"></script>
    <script type="text/javascript" src="libraries/dataTables/pdfmake.min.js"></script>
    <script type="text/javascript" src="libraries/dataTables/vfs_fonts.js"></script>
    <script type="text/javascript" src="libraries/dataTables/buttons.html5.min.js"></script>
    <script type="text/javascript" src="libraries/dataTables/buttons.print.min.js "></script>
    <script type="text/javascript" src="libraries/dataTables/buttons.colVis.min.js"></script>





<style>
    table.dataTable thead th, table.dataTable thead td {
        padding: 2px 3px;
        border-bottom: 1px solid #111;
        font-size: 12px;
    }
    table.dataTable tbody td {
        padding: 0px 10px;
        font-size: 12px;
    }
    .dataTables_wrapper {
        font-family: tahoma;
        font-size: 13px;
        position: relative;
        clear: both;
        *zoom: 1;
        zoom: 1;
    }

    .dataTables_wrapper .dataTables_filter {
        float: left;
        text-align: right;
        padding-left: 20px;
    }
    .dataTables_wrapper .dataTables_paginate {
        float: left;
        text-align: right;
        padding-top: 0.25em;
    }
    table.dataTable thead th, table.dataTable thead td {
        padding: 2px 0px;
        border-bottom: 1px solid #111;
        font-size: 12px;
    }
    .dataTables_wrapper .dataTables_processing {
        position: fixed;
    }
    .dataTables_wrapper .dataTables_filter {
        text-align: right;
        display: none;
    }
    table.dataTable thead td {
        color: #1e0202;
        font-weight: bold;
        background-color: #fff;
    }
    .titlesOFtable{
        display: -moz-popup;
    }
    .dataTables_wrapper {
        position: relative;
        clear: both;
        width: auto;
        height : 500px;
        margin-left: 0px;
        border-bottom: 1px solid black;
        border-top: 1px solid black;
        border-left: 1px solid black;
        border-right: 1px solid black;
        zoom: 1;
        overflow: scroll;
    }



</style>


<body onload="LoadOnce()">
    <table id="employee-grid"   cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
        <thead>
        <tr>
            <td name="Status">Status</td>
            <td>Dispatch NO</td>
            <td>Dispatch Year</td>
            <td>Month</td>
            <td>Marketer</td>
            <td>Customer</td>
            <td>Graphic NO</td>
            <td>Style No</td>
            <td>Order NO</td>
            <td>Sales Order No</td>
            <td>Sample Qty</td>
            <td>Good Qty</td>
            <td>EM-D Qty</td>
            <td>P-D Qty</td>
            <td>F-D Qty</td>
            <td>Cut return</td>
            <td>Date</td>
            <td>1st Approval</td>
            <td>2nd Approval</td>
            <td>3rd Approval</td>
            <td>Report</td>
        </tr>
        <tr role="row">
            <th class="sorting">Status</th>
            <th>Dispatch NO</th>
            <th>Dispatch Year</th>
            <th>Month</th>
            <th>Marketer</th>
            <th>Customer</th>
            <th>Graphic NO</th>
            <th>Style No</th>
            <th>Order NO</th>
            <th>Sales Order No</th>
            <th>Sample Qty</th>
            <th>Good Qty</th>
            <th>EM-D Qty</th>
            <th>P-D Qty</th>
            <th>F-D Qty</th>
            <th>Cut return</th>
            <th>Date</th>
            <th>1st Approval</th>
            <th>2nd Approval</th>
            <th>3rd Approval</th>
            <th>Report</th>
        </tr>
        </thead>
    </table>
	
	<?php
$sql = "SELECT
			sys_users.strUserName
			FROM
			mst_marketer
			INNER JOIN sys_users ON mst_marketer.intUserId = sys_users.intUserId
			where
			sys_users.intUserId  <> 2
			order by sys_users.strUserName asc";
			$marketers = $db->RunQuery($sql);
			
$sql1 = 'SELECT DISTINCT
   			( REPLACE ( REPLACE( REPLACE( mst_customer.strName, "  ", " " ), "  ", " " ), "  ", " " ) ) as strName
 			FROM
			mst_customer
 			order by mst_customer.strName asc
			';
$customer = $db->RunQuery($sql1);

	?>
</body>
    <script type="text/javascript" language="javascript" >
        function LoadOnce() {

            if (location.href.indexOf('reload')==-1)
            {
                location.href=location.href+'?reload';

            }


        }
        $(document).ready(function() {
			// reload function
//            if (location.href.indexOf('reload')==-1)
//            {
//                location.href=location.href+'?reload';
//
//            }
			var monthVal = '';
			var onchanged = 1;
            $('#employee-grid thead th').each( function () {
                var title = $(this).text();
                var dropdown = '';
				
				
                if(title === 'Dispatch Year') {
                    for (i = <?php echo Date('Y'); ?>; i >= 2012; i--) {
						if(i===<?php echo Date('Y'); ?>){
                        dropdown += "<option value="+[i] + " selected='selected'>"+[i] +"</option>";
						}
						else{
						dropdown += "<option value="+[i] + ">"+[i] +"</option>";	
						}
                    }

                    $(this).html( '<span class="titlesOFtable">'+title+'</span><select name="DispatchYear" id="DispatchYear" style="width: 109px;" type="search"></option>'+dropdown+'</select>' );
                }
				
			else if(title === 'Month') {
					var dropdown = '';
					//Month Array
					var monthNames = ["All","January", "February", "March", "April", "May", "June",
						"July", "August", "September", "October", "November", "December"
					];
					var d = new Date();
                    for (i = 0; i <= 11; i++) {
						if(i===d.getMonth()){
							monthVal = monthNames[i];
							dropdown += "<option selected='selected' value="+monthNames[i]+">"+monthNames[i]+"</option>";
						}else{
							dropdown += "<option value="+monthNames[i]+">"+monthNames[i]+"</option>";
						}
				  }

                    $(this).html( '<span class="titlesOFtable">'+title+'</span><select name="Month" id="Month" style="width: 109px;" type="search">'+dropdown+'</select>' );
                }
				
				else if(title === 'Marketer') {
					var dropdown = '';
                    <?php while( $row=mysqli_fetch_array($marketers) ) { ?>
                        dropdown += "<option value='<?php echo $row['strUserName'];?>'><?php echo $row['strUserName'];?></option>";
				   <?php } ?>

                    $(this).html( '<span class="titlesOFtable">'+title+'</span><select name="Marketer" style="width: 109px;" type="search"><option value="">All</option>'+dropdown+'</select>' );
                }
				
				else if(title === 'Customer') {
					var dropdown = '';
                    <?php while( $row=mysqli_fetch_array($customer) ) { ?>
                        dropdown += "<option value='<?php echo $row['strName'];?>'><?php echo $row['strName'];?></option>";
				   <?php } ?>

                    $(this).html( '<span class="titlesOFtable">'+title+'</span><select name="Customer" style="width: 109px;" type="search"><option value="">All</option>'+dropdown+'</select>' );
                }
				
               else if(title === 'Status') {

                    $(this).html( '<span class="titlesOFtable">'+title+'</span><select name="Status" id="Status" type="search"><option value="">All</option><option value="1">Approve</option><option value="0">Rejected</option><option value="*">Pending</option></select>' );
                }
                else{
                    $(this).html( '<span class="titlesOFtable">'+title+'</span><input type="search"  id="'+title+'" name="'+title+'" placeholder="'+title+'" />' );
                }
				
            } );
            $(window).resize(function() {
                console.log($(window).height());
                $('.dataTables_scrollBody').css('height', ($(window).height() - 200));
            });
			
			 if (onchanged==2)
            {
                monthVal = '';
				var dateval = '';

            }
			else{
				var dateval = <?php echo Date('Y'); ?>;
			}
            var dataTable = $('#employee-grid').DataTable( {
                "serverSide": true,
                "regex":true,
                "pageLength": 25,
                "autoWidth": false,
                "processing": true,
				"bFilter": true,

                "ajax":{
                    url :"presentation/customerAndOperation/bulk/fabricDispatchNote/listing/fabricDispatchQuery.php", // json datasource
                    type: "post",  // method  , by default get
                    error: function(){  // error handling
                        $(".employee-grid-error").html("");
                        $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                        $("#employee-grid_processing").css("display","none");

                    },

				"data":{
					//(dateval == '') ? 'columns[2][search][value]' : <?php echo Date('Y'); ?>,
					//(monthVal == '') ? 'columns[3][search][value]' : monthVal
					
					//columns[i][search][value]
				}
                },
                "columns": [
                    { "name": "Status" },
                    { "name": "Dispatch" },
                    { "name": "DispatchYear"},
                    { "name": "Month" },
                    { "name": "Marketer" },
                    { "name": "Customer" } ,
                    { "name": "GraphicNO" },
                    { "name": "StyleNo" },
                    { "name": "OrderNO" },
                    { "name": "SalesOrderNo" },
                    { "name": "SampleQty" },
                    { "name": "GoodQty" },
                    { "name": "EM-DQty" },
                    { "name": "P-DQty" },
                    { "name": "F-DQty" },
                    { "name": "Cutreturn" },
                    { "name": "Date" },
                    { "name": "1stApproval" },
                    { "name": "2ndApproval" },
                    { "name": "3rdApproval" },
                    { "name": "Report" }

                ],
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'print',
                    {
                        extend: 'excelHtml5',
                        title: 'FabricDispatchSummary',
                        exportOptions: {
                            columns: ':visible',
                            format: {
                                header: function ( data, column, row )
                                {
                                    //removing unneccery column charactors
                                    return data.substring(data.indexOf("itlesOFtable")+14,data.indexOf("</span>"));
                                }
                            }
                        }
                    },
                    {
                        extend: 'pdf',
                        title: 'FabricDispatchSummary',
                        exportOptions: {
                            columns: ':visible',
                            format: {
                                header: function ( data, column, row )
                                {
                                    //removing unneccery column charactors
                                    return data.substring(data.indexOf("itlesOFtable")+14,data.indexOf("</span>"));
                                }
                            }
                        }
                    },
                    {
                        extend: 'csv',
                        title: 'FabricDispatchSummary',
                        exportOptions: {
                            columns: ':visible',
                            format: {
                                header: function ( data, column, row )
                                {
                                    //removing unneccery column charactors
                                    return data.substring(data.indexOf("itlesOFtable")+14,data.indexOf("</span>"));
                                }
                            }
                        }
                    },
                    'colvis'
                ]






            });
/* recre filter option Autohr :Hasitha Charaka
*
* */

            dataTable.column( 'Status:name' ).search(dateval).draw();
            dataTable.column( 'DispatchYear:name' ).search(dateval).draw();
            dataTable.column( 'Month:name' ).search(monthVal).draw();
            var x=$('#DispatchYear');
            x.on( 'change', function () {
                dataTable
                    .columns(2)
                    .search(this.value,false,false)
                    .draw();
            } );

            $('#Month').on( 'change', function () {

                dataTable
                    .columns( 3 )
                    .search( this.value )
                    .draw();
            } );

            $('#Status').on( 'change', function () {

                dataTable
                    .columns( 0)
                    .search( this.value )
                    .draw();
            } );


            dataTable.columns().every( function () {
                var that = this;

                $( 'input', this.header() ).on( 'on keyup', function () {
                    if ( that.search() !== this.value ) {

                        that
                            .search( this.value )
                            .draw();
                    }
                } );

                $( 'select', this.header() ).on( 'change', function () {
                    onchanged = 2;
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );

            } );



        } );


    </script>



<?php include 'include/listing.html'?>

    <div align="center" style="margin:10px"><?php echo $out?></div>
<?php

?>
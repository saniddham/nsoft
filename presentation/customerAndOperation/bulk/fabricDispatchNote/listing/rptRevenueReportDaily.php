<?php



date_default_timezone_set('Asia/Colombo');
$thisFilePath =  $_SERVER['PHP_SELF'];
include_once '../../../../../dataAccess/LoginDBManager.php';
//include_once "../../../../../libraries/mail/mail.php";	
//
include_once "../../../../../libraries/mail/mail_bcc.php";	
	
$db =  new LoginDBManager();
 ini_set('max_execution_time', 11111111) ;
	ob_start();
session_start();
//$backwardseperator = "../../../../../";
$companyId = $_SESSION['CompanyID'];
$locationId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];

$toCurrency  	= 1;//1=USD,2=LKR
$month  		= date('m');
//$month  		= 7;
$year  			= date('Y');
$startDate		=1;


$monthName=getMonthName($month);
$ts = strtotime($monthName." ".$year);
$endDate= date('t', $ts); 
$startDate = date("Y-m-d", mktime(0, 0, 0, $month, $startDate, $year));
$endDate = date("Y-m-d", mktime(0, 0, 0, $month, $endDate, $year));

$i=0;
while ($temp < $endDate) {
 $temp = date("Y-m-d", mktime(0, 0, 0, $month, 1+$i, $year));
 $weekday = date('l', strtotime($temp)); // note: first arg to date() is lower-case L
 $printDateArr[$i]=(1+$i)."-".substr($monthName,0,3);
 $dateArr[$i]=(1+$i);
 $fullDateArr[$i]=$temp;
 $dateNameArray[$i]=$weekday; 
 $i++;
}
$noOfDays = $i;
$currencyName='';
$currencyName1=getCurrencyName($toCurrency);
?>
<title>Revenue Report</title><style>.normalfntBlue {font-family: Verdana;font-size: 10px;color: #0B3960;margin: 0px;font-weight: normal;text-align:left;}.normalfnt {font-family: Verdana;font-size: 10px;color: #000000;margin: 0px;font-weight: normal;text-align:left;}.normalfntsm {font-family: Verdana;font-size: 10px;color: #000000;margin: 0px;font-weight: normal;text-align:left;}.normalfntMid {font-family: Verdana;font-size: 10px;color: #000000;margin: 0px;font-weight: normal;text-align:center;}.normalfntRight {font-family: Verdana;font-size: 10px;color: #000000;margin: 0px;font-weight: normal;text-align:right;}.compulsoryRed{color:#F00;}</style><div align="center"><div style="background-color:#FFF" ><strong><div align="left"><img style="vertical-align:" src="http://accsee.com/nsoft_logo.png"  /></div>REVENUE REPORT -  (<?php echo $startDate ." - ".date('Y-m-d').')  Process Time('.date('Y-m-d H:i:s').')' ?></strong><strong></strong></div><div style="background-color:#FFF" ><strong>(Currency - <?php echo $currencyName1?>)</strong><strong></strong></div><table width="1500" border="0" align="center" bgcolor="#FFFFFF"><tr><td><table width="1500"><tr><td width="10">&nbsp;</td><td colspan="7" class="normalfnt"><table width="1500" border="1" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0"><tr class="" bgcolor="#FFFFFF"><td width="50" colspan="4" class="normalfnt" ><b>Plant Wise Revenue Report</b></td><?php
			  for($j=0; $j<$noOfDays ; $j++){ 
			  $bgCol='#FFFFFF';
			  if($dateNameArray[$j]=='Sunday')
			  $bgCol='#FF99CC';
			  
			  ?><td width="80" class="normalfntMid" bgcolor="<?php echo $bgCol ; ?>" ><?php echo substr($dateNameArray[$j],0,3) ?></td><?php
			  }
			  ?>
<td width="80" class="normalfntMid">Total</td></tr><tr class="" bgcolor="#FFFFFF"><td bgcolor="#006699"><font color="#FFFFFF">Plant</font></td><td bgcolor="#006699"><font color="#FFFFFF">Annual Target</font></td><td bgcolor="#006699" ><font color="#FFFFFF">Monthly Target</font></td><td bgcolor="#006699"><font color="#FFFFFF">Day Target</font></td><?php
			  for($j=0; $j<$noOfDays ; $j++){
			  $bgCol='#FFFFFF';
			  if($dateNameArray[$j]=='Sunday')
			  $bgCol='#FF99CC';
			  $cumulativeDay[$j]=1;
			  ?><td  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php echo $bgCol ; ?>" ><?php echo $printDateArr[$j]; ?></td>
              <?php
			  }
			  ?><td width="80">&nbsp;</td></tr><?php 
	  	   $sql1 = "SELECT
			mst_plant.intPlantId,
			mst_plant.strPlantName,
			mst_plant.dblAnualaTarget,
			mst_plant.dblMonthlyTarget,
			mst_plant.dblDayTarget
			FROM `mst_plant`
			where mst_plant.intStatus=1
			ORDER BY
			mst_plant.intPlantId ASC
			";
			$result1 = $db->RunQuery($sql1);
	  		 $anualTargtTot =0;
	  		 $monthlyTargtTot =0;
	  		 $dayTargtTot =0;
			 $totPlantDispQty=0;
			while($row=mysqli_fetch_array($result1))
			{
				$plantId=$row['intPlantId'];
			    $plantWiseTotDispQty['$plantId']=0;
	  ?><tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" ><?php echo $row['strPlantName'];?></td>
              <td class="normalfntRight" align="right" ><?php echo number_format($row['dblAnualaTarget'],0);?></td>
              <td class="normalfntRight" align="right" ><?php echo number_format($row['dblMonthlyTarget'],0);?></td>
              <td class="normalfntRight" align="right" ><?php echo number_format($row['dblDayTarget'],0);?></td><?php 
			  for($j=0; $j<$noOfDays ; $j++){
				$date=$fullDateArr[$j];
				
				$dayType='';
				$sqlr = "SELECT
						mst_plant_calender.dtDate,
						mst_plant_calender.intDayType
						FROM
						mst_locations
						INNER JOIN mst_plant_calender ON mst_locations.intPlant = mst_plant_calender.intPlantId
						WHERE
						mst_plant_calender.intPlantId = '$plantId' AND
						mst_plant_calender.dtDate = '$date'";
				$resultr = $db->RunQuery($sqlr);
				$rorw=mysqli_fetch_array($resultr);
				$dayType=$rorw['intDayType'];
				  
				$dispatchValue=getDispatchQtyValue($plantId,$date,$toCurrency);  
				$DayWiseDispSumArr[$j]+=$dispatchValue;  
				  
			  $bgCol='#FFFFFF';
			  if($dateNameArray[$j]=='Sunday')
			  $bgCol='#FF99CC';
			  if($dayType=='0')//holiday
			  $bgCol='#FFFF33';
			  if($dayType==1)//working day
			  $bgCol='#FFFFFF';
				
			  if(($dateNameArray[$j]=='Sunday') || ($dayType=='0')){
				  $cumulativeDay[$j]=0;
			  }
				
			 $plantWiseTotDispQty['$plantId'] +=round($dispatchValue);
			 $totPlantDispQty +=round($dispatchValue);
	  
			  ?><td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>"><?php echo number_format($dispatchValue,0); ?></td><?php
			  }
			  ?><td width="80" class="normalfntRight"><?php echo number_format($plantWiseTotDispQty['$plantId'],0);?></td></tr><?php 
	  		 $anualTargtTot +=$row['dblAnualaTarget'];
	  		 $monthlyTargtTot +=$row['dblMonthlyTarget'];
	  		 $dayTargtTot +=$row['dblDayTarget'];
			}
	  ?><td class="normalfntRight"  ></td><tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" ></td>
              <td class="normalfntRight" ><b><?php echo number_format($anualTargtTot,0); ?></b></td>
              <td class="normalfntRight" ><b><?php echo number_format($monthlyTargtTot,0); ?></b></td>
              <td class="normalfntRight" ><b><?php echo number_format($dayTargtTot,0); ?></b></td><?php
			  for($j=0; $j<$noOfDays ; $j++){
			  ?><td class="normalfntRight" ><b><?php echo number_format($DayWiseDispSumArr[$j],0); ?></b></td><?php
			  }
			  ?><td class="normalfntRight"  ><?php echo number_format($totPlantDispQty,0); ?></td></tr>
              <!-----------Cum.status-------->
              <tr bgcolor="#FFFFFF">
              <td colspan="<?php echo $noOfDays+4; ?>" height="20"></td>
              </tr>
              <tr bgcolor="#FFFFFF">
              <td colspan="4" align="right" class="normalfntRight" >Cum.Status</td>
              <td colspan="<?php echo $noOfDays; ?>" ></td><td width="80">&nbsp;</td>
              </tr>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfntRight" colspan="4"  align="right">Cum. Target</td><?php 
			  $k=0;
			  for($j=0; $j<$noOfDays ; $j++){
				$date=$fullDateArr[$j];
				
				if($cumulativeDay[$j]==0){//sunday or existing holiday atleast in one plant
					$cumulativeTgt=0;
				}
				else{
					$cumulativeTgt=$dayTargtTot*($k+1);
					$k++;
				}
				  
			  $bgCol='#FFFFFF';
				  
			  ?><td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format($cumulativeTgt,0); ?></td><?php
			  }
			  ?><td width="80">&nbsp;</td></tr>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfntRight" colspan="4" align="right" >Cum. Hit</td><?php 
			  for($j=0; $j<$noOfDays ; $j++){
				$date=$fullDateArr[$j];
				  
			  $bgCol='#FFFFFF';
			  $sum=0;
			  for($k=0; $k<=$j ; $k++){
				$sum+=$DayWiseDispSumArr[$k];
			  }
			  $cumulativeDisp[$j]=$sum;
			  ?><td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format($cumulativeDisp[$j],0); ?></td><?php
			  }
			  ?><td width="80">&nbsp;</td></tr>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfntRight" colspan="4" align="right" >Cum. Hit%</td><?php 
			  $x=0;
			  for($j=0; $j<$noOfDays ; $j++){
				$date=$fullDateArr[$j];
				
				if($cumulativeDay[$j]==0){//sunday or existing holiday atleast in one plant
					$cumulativeHitPerc=0;
				}
				else{
					$cumulativeHitPerc=$cumulativeDisp[$j]/($dayTargtTot*($x+1))*100;
					$x++;
				}
			  
			  ?><td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format($cumulativeHitPerc,0); ?></td><?php
			  }
			  ?><td width="80">&nbsp;</td></tr>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfntRight" colspan="4" align="right" >Dev.</td><?php 
			  $x=0;
			  for($j=0; $j<$noOfDays ; $j++){
				$date=$fullDateArr[$j];
				
				  
			  $bgCol='#FFFFFF';
			  $cls="normalfntRight";
			  
				if($cumulativeDay[$j]==0){//sunday or existing holiday atleast in one plant
					$dev1=0;
				}
				else{
					$dev1=$cumulativeDisp[$j]-$dayTargtTot*($x+1);
					$x++;
				}
			  
			  
			  $dev= number_format(abs($dev1),0);
			  if($dev1<0){
				 $dev="(".$dev .")";
				 $cls="compulsoryRed";
			   //  $bgCol='#FF3333';
			  }
				   
			  ?><td class="<?php echo $cls; ?>"  bgcolor="<?php echo $bgCol; ?>"  align="right"><?php echo $dev ?></td><?php
			  }
			  ?><td width="80">&nbsp;</td></tr>
             <!-----------------------------> 
              <!-----------Managers revenue-------->
              <tr bgcolor="#FFFFFF">
              <td colspan="<?php echo $noOfDays+4; ?>" height="20"></td>
              </tr>
            <tr class="" bgcolor="#FFFFFF">
              <td bgcolor="#006699"><font color="#FFFFFF">Cluster</font></td>
              <td bgcolor="#006699"><font color="#FFFFFF">Annual Target</font></td>
              <td bgcolor="#006699" ><font color="#FFFFFF">Monthly Target</font></td>
              <td bgcolor="#006699"><font color="#FFFFFF">Day Target</font></td><?php
			  for($j=0; $j<$noOfDays ; $j++){
			  $bgCol='#FFFFFF';
			  if($dateNameArray[$j]=='Sunday')
			  $bgCol='#FF99CC';
			  ?><td  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php echo $bgCol ; ?>" ><?php echo $printDateArr[$j]; ?></td><?php
			  }
			  ?><td width="80">&nbsp;</td></tr><?php 
			  
			//-------------------------------------
	  	   $sql1 = "SELECT
					mst_marketer_manager.strName,
					mst_marketer_manager.intMarketingManagerId,
					mst_marketer_manager.intStatus,
					mst_marketer_manager.dblAnualaTarget,
					mst_marketer_manager.dblMonthlyTarget,
					mst_marketer_manager.dblDayTarget
					FROM `mst_marketer_manager` 
					WHERE mst_marketer_manager.intMarketingManagerId!='3' 
					ORDER BY
					mst_marketer_manager.intMarketingManagerId ASC
			";
			$result1 = $db->RunQuery($sql1);
			while($row=mysqli_fetch_array($result1))
			{
				$managerId=$row['intMarketingManagerId'];
				  for($j=0; $j<$noOfDays ; $j++){
				$date=$fullDateArr[$j];
				  
				$dispatchValue=getClusterDispatchQtyValue($managerId,$date,$toCurrency);  
				$DayWiseClustDispSumArr1[$j]+=$dispatchValue;  
				  }
				
			}
			//-------------------------------------
				  
	  	   $sql1 = "SELECT
					mst_marketer_manager.strName,
					mst_marketer_manager.intMarketingManagerId,
					mst_marketer_manager.intStatus,
					mst_marketer_manager.dblAnualaTarget,
					mst_marketer_manager.dblMonthlyTarget,
					mst_marketer_manager.dblDayTarget
					FROM `mst_marketer_manager` 
					ORDER BY
					mst_marketer_manager.intMarketingManagerId ASC
			";
			$result1 = $db->RunQuery($sql1);
			
	  		 $anualClustTargtTot =0;
	  		 $monthlyClustTargtTot =0;
	  		 $dayClustTargtTot =0;
			 $totClustDispQty=0;
			
			while($row=mysqli_fetch_array($result1))
			{
				$managerId=$row['intMarketingManagerId'];
			    $clustWiseTotDispQty['$managerId']=0;
	  ?><tr class="normalfnt"  bgcolor="#FFFFFF"><td class="normalfnt" ><?php echo $row['strName'];?></td>
              <td class="normalfntRight" align="right" ><?php echo number_format($row['dblAnualaTarget'],0);?></td>
              <td class="normalfntRight" align="right" ><?php echo number_format($row['dblMonthlyTarget'],0);?></td>
              <td class="normalfntRight" align="right" ><?php echo number_format($row['dblDayTarget'],0);?></td><?php 
			  for($j=0; $j<$noOfDays ; $j++){
				$date=$fullDateArr[$j];
				  
				$dispatchValue=getClusterDispatchQtyValue($managerId,$date,$toCurrency);  
				$DayWiseClustDispSumArr[$j]+=$dispatchValue;  
				  
				  $bgCol='#FFFFFF';
				  if($dateNameArray[$j]=='Sunday')
				  $bgCol='#FF99CC';
			  
			 $clustWiseTotDispQty['$managerId'] +=round($dispatchValue);
			 $totClustDispQty +=round($dispatchValue);
			
			  ?><td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format($dispatchValue,0); ?></td>
              <?php
			  }
			  ?><td width="80" class="normalfntRight"><?php echo number_format($clustWiseTotDispQty['$managerId'],0) ; ?></td></tr><?php 
	  		 $anualClustTargtTot +=$row['dblAnualaTarget'];
	  		 $monthlyClustTargtTot +=$row['dblMonthlyTarget'];
	  		 $dayClustTargtTot +=$row['dblDayTarget'];
			}
	  ?><tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" ><b><?php echo number_format($anualClustTargtTot,0); ?></b></td>
              <td class="normalfntRight" ><b><?php echo number_format($monthlyClustTargtTot,0); ?></b></td>
              <td class="normalfntRight" ><b><?php echo number_format($dayClustTargtTot,0); ?></b></td><?php
			  for($j=0; $j<$noOfDays ; $j++){
			  ?><td class="normalfntRight" ><b><?php echo number_format($DayWiseClustDispSumArr[$j],0); ?></b></td><?php
			  }
			  ?><td width="80" class="normalfntRight"><?php echo number_format($totClustDispQty,0) ; ?></td></tr>
              <!-----------Cum.status-------->
              <tr bgcolor="#FFFFFF">
              <td colspan="<?php echo $noOfDays+4; ?>" height="20"></td>
              </tr>
              <tr bgcolor="#FFFFFF">
              <td colspan="4" align="right" class="normalfntRight" >Cum.Status</td>
              <td colspan="<?php echo $noOfDays; ?>" ></td>
              <td width="80">&nbsp;</td></tr>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfntRight" colspan="4"  align="right">Cum. Target </td><?php 
			  $k=0;
			  for($j=0; $j<$noOfDays ; $j++){
				$date=$fullDateArr[$j];
				
				  
			  $bgCol='#FFFFFF';
				
				if($cumulativeDay[$j]==0){//sunday or existing holiday atleast in one plant
					$cumulativeClustTgt=0;
				}
				else{
					$cumulativeClustTgt=$dayClustTargtTot*($k+1);
					$k++;
				}
				
				  
			  ?><td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format($cumulativeClustTgt,0); ?></td><?php
			  }
			  ?><td width="80">&nbsp;</td></tr>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfntRight" colspan="4"  align="right">Cum. Hit </td>
              <?php 
			  $k=0;
			  for($j=0; $j<$noOfDays ; $j++){
				$date=$fullDateArr[$j];
				
				  
			  $bgCol='#FFFFFF';
				
				
				  $sum=0;
				  for($k=0; $k<=$j ; $k++){
					$sum+=$DayWiseClustDispSumArr1[$k];
				  }
				  $cumulativeClustDisp[$j]=$sum;
					
				  
			  ?><td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format($cumulativeClustDisp[$j],0); ?></td><?php
			  }
			  ?><td width="80">&nbsp;</td></tr>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfntRight" colspan="4"  align="right">Cum. Hit% </td>
              <?php 
			  $x=0;
			  for($j=0; $j<$noOfDays ; $j++){
				$date=$fullDateArr[$j];
				
				  
			  $bgCol='#FFFFFF';
				
				
				
				  //----------------
				if($cumulativeDay[$j]==0){//sunday or existing holiday atleast in one plant
					$cumulativeHitPercClust=0;
				}
				else{
					$cumulativeHitPercClust=$cumulativeClustDisp[$j]/($dayClustTargtTot*($x+1))*100;
					$x++;
				}
				  //-------------
					
				  
			  ?><td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo number_format($cumulativeHitPercClust,0); ?></td><?php
			  }
			  ?><td width="80">&nbsp;</td></tr>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfntRight" colspan="4" align="right" >Dev.</td><?php 
			  $k=0;
			  for($j=0; $j<$noOfDays ; $j++){
				$date=$fullDateArr[$j];
				
				$dayType='';
				$sqlr = "SELECT
						mst_plant_calender.dtDate,
						mst_plant_calender.intDayType
						FROM
						mst_locations
						INNER JOIN mst_plant_calender ON mst_locations.intPlant = mst_plant_calender.intPlantId
						WHERE
						mst_plant_calender.intPlantId = '$managerId' AND
						mst_plant_calender.dtDate = '$date'";
				$resultr = $db->RunQuery($sqlr);
				$rorw=mysqli_fetch_array($resultr);
				$dayType=$rorw['intDayType'];
				  
				$dispatchValue=getClusterDispatchQtyValue($managerId,$date,$toCurrency);  
				//$DayWiseClustDispSumArr[$j]+=$dispatchValue;  
				  
			  $bgCol='#FFFFFF';
			  $cls="normalfntRight";
			  $dev1=$DayWiseClustDispSumArr1[$j]-$dayClustTargtTot*($j+1);
			  
				if($cumulativeDay[$j]==0){//sunday or existing holiday atleast in one plant
					$dev1=0;
				}
				else{
					$dev1=$cumulativeClustDisp[$j]-$dayClustTargtTot*($k+1);
					$k++;
				}
			  
			  $dev= number_format(abs($dev1),0);
			  if($dev1<0){
				 $dev="(".$dev .")";
				 $cls="compulsoryRed";
			   //  $bgCol='#FF3333';
			  }
				  ;
			  ?><td class="<?php echo $cls; ?>" bgcolor="<?php echo $bgCol; ?>" align="right" ><?php echo $dev ?></td><?php
			  }
			  ?><td width="80">&nbsp;</td></tr>
             <!-----------------------------> 
            </table>
          </td>
        <td width="10">&nbsp;</td>
        </tr>
 
 <tr>
 <td colspan="8" height="15"></td>
 </tr>
            </table>
          </td>
        <td width="10">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<tr height="90" >
  <td align="center" class="normalfntMid"></td>
</tr>
</table>
</div><?php
function getMonthName($month){
	global $db;
	
	$sqlp = "SELECT
			mst_month.strMonth
			FROM `mst_month`
			WHERE
			mst_month.intMonthId = $month
			";	
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	return $rowp['strMonth'];
}
function getCurrencyName($id){
	global $db;
	
 	$sqlp = "SELECT
mst_financecurrency.strCode
FROM
mst_financecurrency
WHERE
mst_financecurrency.intId = '$id'
			";	
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	return $rowp['strCode'];
}
function getDispatchQtyValue($plantId,$date,$toCurrency){
	global $db;	
	$sqlp = "select 

sum(tb1.Qty1) as Qty

 from (SELECT 
Sum(ware_stocktransactions_fabric.dblQty*-1*trn_orderdetails.dblPrice) AS Qty1,
date(trn_orderheader.dtDate) as date,
mst_locations.intCompanyId 
FROM
ware_stocktransactions_fabric
left JOIN mst_locations ON ware_stocktransactions_fabric.intLocationId = mst_locations.intId
left JOIN trn_orderheader ON ware_stocktransactions_fabric.intOrderNo = trn_orderheader.intOrderNo AND ware_stocktransactions_fabric.intOrderYear = trn_orderheader.intOrderYear
left JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_stocktransactions_fabric.intSalesOrderId = trn_orderdetails.intSalesOrderId
left JOIN mst_financeexchangerate ON trn_orderheader.intCurrency = mst_financeexchangerate.intCurrencyId AND date(trn_orderheader.dtDate) = mst_financeexchangerate.dtmDate AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId
WHERE
mst_locations.intPlant = $plantId				 AND
date(ware_stocktransactions_fabric.dtDate) = '$date' AND
  ware_stocktransactions_fabric.strType = 'Dispatched_G'
  GROUP BY date(trn_orderheader.dtDate),mst_locations.intCompanyId) as tb1 
left JOIN mst_financeexchangerate ON $toCurrency = mst_financeexchangerate.intCurrencyId 
AND tb1.date = mst_financeexchangerate.dtmDate 
AND tb1.intCompanyId = mst_financeexchangerate.intCompanyId";	

	
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	return $rowp['Qty'];
	
}
function getClusterDispatchQtyValue($managerId,$date,$toCurrency){
	global $db;
			
	$sqlp = "select 

sum(tb1.Qty1) as Qty

 from (SELECT 
Sum(ware_stocktransactions_fabric.dblQty*-1*trn_orderdetails.dblPrice) AS Qty1,
date(trn_orderheader.dtDate) as date,
mst_locations.intCompanyId 
FROM
ware_stocktransactions_fabric
left JOIN mst_locations ON ware_stocktransactions_fabric.intLocationId = mst_locations.intId
left JOIN trn_orderheader ON ware_stocktransactions_fabric.intOrderNo = trn_orderheader.intOrderNo AND ware_stocktransactions_fabric.intOrderYear = trn_orderheader.intOrderYear
left JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_stocktransactions_fabric.intSalesOrderId = trn_orderdetails.intSalesOrderId
left JOIN mst_marketer ON trn_orderheader.intMarketer = mst_marketer.intUserId
left JOIN mst_financeexchangerate ON trn_orderheader.intCurrency = mst_financeexchangerate.intCurrencyId AND date(trn_orderheader.dtDate) = mst_financeexchangerate.dtmDate AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId
WHERE
mst_marketer.intManager = $managerId				 AND
date(ware_stocktransactions_fabric.dtDate) = '$date' AND
  ware_stocktransactions_fabric.strType = 'Dispatched_G'
  GROUP BY date(trn_orderheader.dtDate),mst_locations.intCompanyId) as tb1 
left JOIN mst_financeexchangerate ON $toCurrency = mst_financeexchangerate.intCurrencyId 
AND tb1.date = mst_financeexchangerate.dtmDate 
AND tb1.intCompanyId = mst_financeexchangerate.intCompanyId";	

	
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	return $rowp['Qty'];
	
}

    $body = ob_get_clean();
	

			$nowDate = date('Y-m-d');
	
			$mailHeader= "REVENUE REPORT ($nowDate)";
			//$mailHeader= "(Bug Fixed)REVENUE REPORT (JULY)";
			$sql = "SELECT
						sys_users.strEmail,sys_users.strFullName
					FROM
					sys_mail_eventusers
						Inner Join sys_users ON sys_users.intUserId = sys_mail_eventusers.intUserId
					WHERE
						sys_mail_eventusers.intMailEventId =  '1012'
					";
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				//sendMessage('nsoft@screenlineholdings.com','NSOFT - SCREENLINE HOLDINGS.',$row['strEmail'],$mailHeader,$body);	
				$toEmails .= $row['strEmail'].',';	
			}
			
			/*live*/
	/*		$notice = "Special Note : This report re-generated for calculate actual revenue including addional amount** <br>
			<br>
			** When create a new company , we have to update exchage rates for old dates. Otherwise revenue will not calculate for old orders. Now we fixed it .
			
			";*/
			sendMessage('nsoft@screenlineholdings.com','NSOFT - SCREENLINE HOLDINGS.',$toEmails,$mailHeader,$notice.$body,'brahman@screenlineholdings.com,mihindu@screenlineholdings.com','nsoftemail@gmail.com,roshan@screenlineholdings.com');
			
			/*testing*/
			//sendMessage('nsoft@screenlineholdings.com','NSOFT - SCREENLINE HOLDINGS.','roshan@screenlineholdings.com;',$mailHeader,$notice.$body,'','');
			
			/*sendMessage('nsoft@screenlineholdings.com','NSOFT - SCREENLINE HOLDINGS.','buddhikak@screenlineholdings.com',$mailHeader,"[customer email list - $custLocaEmail,$userCCemails]".$body,'','');*/
			
			echo $body;	
echo 'REVENUE REPORT -> OK <br>';
?>
<?php

ini_set('display_errors',0);
//session variables

session_start();
$loginUserId	= $_SESSION['userId'];
$logLocationId	= $_SESSION['CompanyID'];

//connection
include "../../../../../dataAccess/DBManager2.php";							$db							= new DBManager2();

//class files
include_once "../../../../../class/error_handler.php";						$error_handler 				= new error_handler(); 
include_once "../../../../../class/tables/ware_fabricdispatchheader.php";	$ware_fabricdispatchheader 	= new ware_fabricdispatchheader($db);
include_once "../../../../../class/tables/ware_fabricdispatchdetails.php";	$ware_fabricdispatchdetails	= new ware_fabricdispatchdetails($db);
include_once "../../../../../class/tables/ware_fabricdispatchheader_approvedby.php";	$ware_fabricdispatchheader_approvedby	= new ware_fabricdispatchheader_approvedby($db);
include_once "../../../../../class/tables/ware_stocktransactions_fabric.php";$ware_stocktransactions_fabric = new ware_stocktransactions_fabric($db);
include_once "../../../../../class/tables/menupermision.php";				$menupermision				= new menupermision($db);
include_once "../../../../../class/tables/mst_locations.php";				$mst_locations				= new mst_locations($db);
include_once "../../../../../class/tables/trn_orderheader.php";				$trn_orderheader			= new trn_orderheader($db);
include_once "../../../../../class/tables/mst_customer_locations.php";		$mst_customer_locations		= new mst_customer_locations($db);
include_once "../../../../../class/tables/trn_lc_header.php";				$trn_lc_header				= new trn_lc_header($db);
include_once "../../../../../class/dateTime.php";							$dateTime					= new dateTimes($db);
include_once "../../../../../class/tables/finance_customer_invoice_balance.php"; $finance_customer_invoice_balance = new finance_customer_invoice_balance($db);
include_once "../../../../../class/tables/sys_approvelevels.php";			$sys_approvelevels			= new sys_approvelevels($db);
include_once "../../../../../class/tables/sys_no.php";						$sys_no						= new sys_no($db);

//Parameters
$requestType 		= 	$_REQUEST['requestType'];
$dispatchNo			=	$_REQUEST['serialNo'];
$dispatchYear		= 	$_REQUEST['year'];
$programCode		=	"P0470";
$dispathTypeArray 	=	array(
					'dblSampleQty'=>'Dispatched_S',
					'dblGoodQty'=>'Dispatched_G',
					'dblEmbroideryQty'=>'Dispatched_E',
					'dblPDammageQty'=>'Dispatched_P',
					'dblFdammageQty'=>'Dispatched_F',
					'dblCutRetQty'=>'Dispatched_CUT_RET'
					);
 
if($requestType=='save')
{
	//VALIDATIONS
	//1.check valid status of header table.
	//2.check user permision for save .
	//3.check if this is a prodcution location.
	//4.if edit-validate saved location id with session location id.
	//5.distpatch to customer location should updated emails.--------------------do not check here
	//6.check for lc status and approval.
	//7.check fabric balance each detail row wise .	
	
	//TABLE INSERT/UPDATE/DELETE
	//1.update sys no table
	//2.Insert to header tables.  
	//3.Update header tables.  
	//4.delete details
	//5.insert to details
	//6.update to max status for status 0 rcds- approved by table.
	
	$dispatchNo 	= $_REQUEST['serialNo'];
	$dispatchYear 	= $_REQUEST['Year'];
	$custLocation 	= $_REQUEST['custLocation'];
	$orderNo 	 	= $_REQUEST['orderNo'];
	$orderYear 	 	= $_REQUEST['orderYear'];
	$dispatchDate 	= $_REQUEST['dispatchDate'];
	$remarks 	 	= $_REQUEST['remarks'];
	$arr 			= json_decode($_REQUEST['arr'], true);
	$edit_mode		= 0;
	$trn_orderheader->set($orderNo,$orderYear);
	
	try{
		//open db connnection
		$db->connect();
		
		//set primary values to header object
		$menupermision->set($programCode,$loginUserId);
		
		//get dispatch no and update sys table
		if($dispatchNo =='' || $dispatchYear == ''){
			$edit_mode		= 1;
			$sys_no->set($logLocationId);
			$dispatchNo		= $sys_no->getintBulkDispatchNo();
			$sys_no->setintBulkDispatchNo($sys_no->getintBulkDispatchNo()+1);
			$sys_no->commit();
			$dispatchYear	= $dateTime->getCurruntYear();
		}
		else
			$ware_fabricdispatchheader->set($dispatchNo,$dispatchYear);
		
		####################################################
		## 1.check valid status of header table 		  ##
		####################################################
		//get current status
		$sys_approvelevels->set($programCode);
		$approveLevels 		= (int)$sys_approvelevels->getintApprovalLevel();
		$dispatchStatus		=$approveLevels+1;		
		
		//validate dispatch Status
		if($edit_mode==1)
		validateDispatchStatusForSave($dispatchNo,$dispatchYear);
		
		####################################################
		## 2.check user permision for approval  		  ##
		####################################################
		//get approve levels
		
		//create approval level
		$approvalFieldName 	= "intEdit";
		if(!$menupermision->$approvalFieldName()){
			throw new Exception("No permision for Save");
		}
		
		####################################################
		## 3.check if this is a prodcution location .     ##
		####################################################
		//set location id
		$mst_locations->set($logLocationId);
		if($mst_locations->getintProduction()!=1)
			throw new Exception("This is not a production location");	

		##############################################################
		## 4.validate saved location id with session location id .	##
		##############################################################
 		//get saved company id
		 
		if($edit_mode==1 &&  $ware_fabricdispatchheader->getintCompanyId() != $logLocationId)
			throw new Exception("This is not saved location");	
		
		####################################################
		## 6.check for lc status and approval .     ##
		####################################################
			
		if($trn_orderheader->getLC_STATUS()==6 ){ 
			$trn_lc_header->set($trn_orderheader->getintOrderNo(),$trn_orderheader->getintOrderYear());
			if($trn_lc_header->getSTATUS() !=1) {
				throw new Exception("No approved LC.So can't dispatch fabrics.");	
			}
		}
		
		####################################################
		## 7.check fabric balance each detail row wise .  ##
		####################################################
		//get dispatch details
		$result = $ware_fabricdispatchdetails->select("*",null,"intBulkDispatchNo=$dispatchNo and intBulkDispatchNoYear=$dispatchYear");
		$ware_fabricdispatchheader->set($dispatchNo,$dispatchYear);
		//check row wise fabric balances
		while($row=mysqli_fetch_array($result)){
			$docNo 			= $dispatchNo;
			$docYear 		= $dispatchYear;
			$orderNo 		= $ware_fabricdispatchheader->getintOrderNo();
			$orderYear 		= $ware_fabricdispatchheader->getintOrderYear();
			$salesId 		= $row['intSalesOrderId'];
			$cutNo 			= $row['strCutNo'];
			$partId 		= $row['intPart'];
			$size 			= $row['strSize'];
			$ware_fabricdispatchdetails->set($dispatchNo,$dispatchYear,$cutNo,$salesId,$size);
			foreach($dispathTypeArray as $k=>$v)
			{
 				$qty 			= $row[$k];
				//insert to transaction
				if($qty>0){
					$result_in	= $ware_stocktransactions_fabric->insert_rec($docNo,$docYear,$orderNo,$orderYear,$cutNo,$salesId,$partId,$size,-$qty,$v,$dateTime->getCurruntDateTime());	
					if(!$result_in['0']) 
						throw new Exception($result_in['1']);
				}
				//update invoice balance table
				if($k=='dblGoodQty' && $qty>0){
					$finance_customer_invoice_balance->set($orderYear,$orderNo,$salesId);
					AddToBalanceTable($ware_fabricdispatchheader->getintOrderYear(),$ware_fabricdispatchheader->getintOrderNo(),$ware_fabricdispatchdetails->getintSalesOrderId(),$qty);
				}
			}

			//check current stock balance qty
			if($ware_stocktransactions_fabric->getFabricBalanceSumQty($logLocationId,$orderNo,$orderYear,$salesId,$cutNo,$partId,$size)<0)
				throw new Exception("There is no fabric in house for dispatch.Cut No=$cutNo / Size = $size");	
		}
		
		//if final approval
		if($ware_fabricdispatchheader->getintStatus()!=2){//purpose : if not final approval , rollback transaction insert
			$db->disconnect();
			$db->connect();
		}
		
		#################################################
		## update status = status-1.				   ##	
		#################################################
		
		$ware_fabricdispatchheader->setintStatus($ware_fabricdispatchheader->getintStatus()-1);	
		$result_set	= $ware_fabricdispatchheader->commit();
		if(!$result_set['0']) 
			throw new Exception($result_set['1']);
			
		
 		#################################################
		## insert to approved by table				   ##	
		#################################################
		
		insertToApprovedByTable($dispatchNo,$dispatchYear,($ware_fabricdispatchheader->getintApproveLevels()+1-$ware_fabricdispatchheader->getintStatus()),$dateTime->getCurruntDateTime(),$loginUserId);

  		
		$response['type']	= 'pass';
		if($ware_fabricdispatchheader->getintStatus()==1)
		$response['msg'] 	= 'Final Approval raised successfully.';
		else
		$response['msg'] 	= 'Approval '.($ware_fabricdispatchheader->getintApproveLevels()+1-$ware_fabricdispatchheader->getintStatus()).' raised successfully.';
	
		$db->commit();
		
	}catch(Exception $e){
		$response['msg'] 		=  $e->getMessage();;
		$response['error'] 		=  $error_handler->jTraceEx($e);
		$response['type'] 		=  'fail';
		$response['sql']		=  $db->getSql();
		$response['mysql_error']=  $db->getMysqlError();
	}
	
	echo json_encode($response);
	$db->disconnect();
}
 
else if($requestType=='approve')
{
	//VALIDATIONS
	//1.check valid status of header table.
	//2.check user permision for approval .
	//3.check if this is a prodcution location.
	//4.validate saved location id with session location id.
	//5.distpatch to customer location should updated emails.
	//6.check for lc status and approval.
	//7.check fabric balance each detail row wise .	
	
	//TABLE INSERT/UPDATE/DELETE
	//1.Insert to transaction table. (if final approval)
	//2.Add to sales order invoice balance. (if final approval)
	//3.Update header table status.
	//4.insert to approved by table.
	
	try{
		//open db connnection
		$db->connect();
		
		//set primary values to header object
		$ware_fabricdispatchheader->set($dispatchNo,$dispatchYear);
		$menupermision->set($programCode,$loginUserId);
		
		####################################################
		## 1.check valid status of header table 		  ##
		####################################################
		//get current status
		$dispatchStatus 	= $ware_fabricdispatchheader->getintStatus();
		
		//validate dispatch Status
		validateDispatchStatusForApprove($dispatchNo,$dispatchYear,$dispatchStatus);
		
		
		####################################################
		## 2.check user permision for approval  		  ##
		####################################################
		//get approve levels
		$approveLevels 		= $ware_fabricdispatchheader->getintApproveLevels();
		
		//create approval level
		$x					= ($approveLevels+2)-$dispatchStatus;
		$approvalFieldName 	= "getint{$x}Approval";
		if(!$menupermision->$approvalFieldName()){
			throw new Exception("No permision for {$x} approval");
		}
		
 
		####################################################
		## 3.check if this is a prodcution location .     ##
		####################################################
		//set location id
		$mst_locations->set($logLocationId);
		if($mst_locations->getintProduction()!=1)
			throw new Exception("This is not a production location");	

		
		####################################################
		## 4.validate saved location id with session location id .     ##
		####################################################
 		//get saved company id
		if($ware_fabricdispatchheader->getintCompanyId() != $logLocationId)
			throw new Exception("This is not saved location");	
		
		
		####################################################
		## 5.distpatch to customer location should updated emails .     ##
		####################################################
		
		$trn_orderheader->set($ware_fabricdispatchheader->getintOrderNo(),$ware_fabricdispatchheader->getintOrderYear());
		$mst_customer_locations->set($ware_fabricdispatchheader->getintCustLocation(),$trn_orderheader->getintCustomer());
		if($mst_customer_locations->getstrEmailAddress()=='') 
		throw new Exception("Please update customer emails before approval.");	
				
		
		####################################################
		## 6.check for lc status and approval .     ##
		####################################################
			
		if($trn_orderheader->getLC_STATUS()==6 ){ 
			$trn_lc_header->set($trn_orderheader->getintOrderNo(),$trn_orderheader->getintOrderYear());
			if($trn_lc_header->getSTATUS() !=1) {
				throw new Exception("No approved LC.So can't dispatch fabrics.");	
			}
		}

		
		####################################################
		## 7.check fabric balance each detail row wise .  ##
		####################################################
		//get dispatch details
		$result = $ware_fabricdispatchdetails->select("*",null,"intBulkDispatchNo=$dispatchNo and intBulkDispatchNoYear=$dispatchYear");
		$ware_fabricdispatchheader->set($dispatchNo,$dispatchYear);
		//check row wise fabric balances
		while($row=mysqli_fetch_array($result)){
			$docNo 			= $dispatchNo;
			$docYear 		= $dispatchYear;
			$orderNo 		= $ware_fabricdispatchheader->getintOrderNo();
			$orderYear 		= $ware_fabricdispatchheader->getintOrderYear();
			$salesId 		= $row['intSalesOrderId'];
			$cutNo 			= $row['strCutNo'];
			$partId 		= $row['intPart'];
			$size 			= $row['strSize'];
			$ware_fabricdispatchdetails->set($dispatchNo,$dispatchYear,$cutNo,$salesId,$size);
			foreach($dispathTypeArray as $k=>$v)
			{
 				$qty 			= $row[$k];
				//insert to transaction
				if($qty>0){
					$result_in	= $ware_stocktransactions_fabric->insert_rec($docNo,$docYear,$orderNo,$orderYear,$cutNo,$salesId,$partId,$size,-$qty,$v,$dateTime->getCurruntDateTime());	
					if(!$result_in['0']) 
						throw new Exception($result_in['1']);
				}
				//update invoice balance table
				if($k=='dblGoodQty' && $qty>0){
					$finance_customer_invoice_balance->set($orderYear,$orderNo,$salesId);
					AddToBalanceTable($ware_fabricdispatchheader->getintOrderYear(),$ware_fabricdispatchheader->getintOrderNo(),$ware_fabricdispatchdetails->getintSalesOrderId(),$qty);
				}
			}

			//check current stock balance qty
			if($ware_stocktransactions_fabric->getFabricBalanceSumQty($logLocationId,$orderNo,$orderYear,$salesId,$cutNo,$partId,$size)<0)
				throw new Exception("There is no fabric in house for dispatch.Cut No=$cutNo / Size = $size");	
		}
		
		//if final approval
		if($ware_fabricdispatchheader->getintStatus()!=2){//purpose : if not final approval , rollback transaction insert
			$db->disconnect();
			$db->connect();
		}
		
		#################################################
		## update status = status-1.				   ##	
		#################################################
		
		$ware_fabricdispatchheader->setintStatus($ware_fabricdispatchheader->getintStatus()-1);	
		$result_set	= $ware_fabricdispatchheader->commit();
		if(!$result_set['0']) 
			throw new Exception($result_set['1']);
			
		
 		#################################################
		## insert to approved by table				   ##	
		#################################################
		
		insertToApprovedByTable($dispatchNo,$dispatchYear,($ware_fabricdispatchheader->getintApproveLevels()+1-$ware_fabricdispatchheader->getintStatus()),$dateTime->getCurruntDateTime(),$loginUserId);

  		
		$response['type']	= 'pass';
		if($ware_fabricdispatchheader->getintStatus()==1)
		$response['msg'] 	= 'Final Approval raised successfully.';
		else
		$response['msg'] 	= 'Approval '.($ware_fabricdispatchheader->getintApproveLevels()+1-$ware_fabricdispatchheader->getintStatus()).' raised successfully.';
	
		$db->commit();
		
	}catch(Exception $e){
		$response['msg'] 		=  $e->getMessage();;
		$response['error'] 		=  $error_handler->jTraceEx($e);
		$response['type'] 		=  'fail';
		$response['sql']		=  $db->getSql();
		$response['mysql_error']=  $db->getMysqlError();
	}
	
	echo json_encode($response);
	$db->disconnect();
}

else if($requestType=='reject')
{
	//VALIDATIONS
	//1.check valid status of header table.
	//2.check user permision for reject .
	
	//TABLE INSERT/UPDATE/DELETE
 	//1.Update header table status.
	//2.insert to approved by table.
	
	try{
		//open db connnection
		$db->connect();
		
		//set primary values to header object
		$ware_fabricdispatchheader->set($dispatchNo,$dispatchYear);
		$menupermision->set($programCode,$loginUserId);
		
		####################################################
		## 1.check valid status of header table 		  ##
		####################################################
		//get current status
		$dispatchStatus 	= $ware_fabricdispatchheader->getintStatus();
		
		//validate dispatch Status
		validateDispatchStatusForReject($dispatchNo,$dispatchYear,$dispatchStatus);
		
		
		####################################################
		## 2.check user permision for approval  		  ##
		####################################################
		//get approve levels
		$approveLevels 		= $ware_fabricdispatchheader->getintApproveLevels();
		
		//create approval level
		$x					= ($approveLevels+2)-$dispatchStatus;
		$approvalFieldName 	= "getint{$x}Approval";
		if(!$menupermision->$approvalFieldName()){
			throw new Exception("No permision to Reject");
		}
		
		
		#################################################
		## update status = 0				   ##	
		#################################################
		
		$ware_fabricdispatchheader->setintStatus(0);	
		$result_set	= $ware_fabricdispatchheader->commit();
		if(!$result_set['0']) 
			throw new Exception($result_set['1']);
			
		
 		#################################################
		## insert to approved by table				   ##	
		#################################################
		
		insertToApprovedByTable($dispatchNo,$dispatchYear,0,$dateTime->getCurruntDateTime(),$loginUserId);

  		
		$response['type']	= 'pass';
		$response['msg'] 	= 'Rejected successfully.';
	
		$db->commit();
		
	}catch(Exception $e){
		$response['msg'] 		=  $e->getMessage();;
		$response['error'] 		=  $error_handler->jTraceEx($e);
		$response['type'] 		=  'fail';
		$response['sql']		=  $db->getSql();
		$response['mysql_error']=  $db->getMysqlError();
	}
	
	echo json_encode($response);
	$db->disconnect();
}



//for validate dispatch status
function validateDispatchStatusForApprove($dispatchNo,$dispatchYear,$dispatchStatus)
{
	if($dispatchStatus==1)
		throw new Exception("This dispatch No ($dispatchNo/$dispatchYear) is already approved");
	else if($dispatchStatus==0)
		throw new Exception("Please save this before approval");
	else if($dispatchStatus==-2)
		throw new Exception("This is cancelled.Cannot approve.");
	else if($dispatchStatus==-10)
		throw new Exception("This is completed.Cannot approve.");
	else if($dispatchStatus==-1)
		throw new Exception("This is revised.Cannot approve.");
	else
		return true;
}
 
//insert to approved by
function insertToApprovedByTable($dispatchNo,$dispatchYear,$level,$time,$user){
	global $db;
	global $ware_fabricdispatchheader_approvedby;
	
	$data =array(
		'intBulkDispatchNo'		=>$dispatchNo,
		'intYear'				=>$dispatchYear,
		'intApproveLevelNo'		=>$level,
		'intApproveUser'		=>$user,
		'dtApprovedDate'		=>'"'.$time.'"',
		'intStatus'				=>'0' 
	);
	 
	$result_insert	=  $ware_fabricdispatchheader_approvedby->insert($data);
	if(!$result_insert['0']) 
		throw new Exception($result_insert['1']);
	else
		return true;
}


function AddToBalanceTable($orderYear,$orderNo,$salesOrderId,$goodQty)
{
  	global $db;
	global $finance_customer_invoice_balance;
	
	$booAvailable	= false;
	$finance_customer_invoice_balance->set($orderYear,$orderNo,$salesOrderId);
	
	if($finance_customer_invoice_balance->getORDER_NO())
	$booAvailable	= true;
	
	if(!$booAvailable)
	{
		$data =array(
			'ORDER_YEAR'			=>$orderYear,
			'ORDER_NO'				=>$orderNo,
			'SALES_ORDER_ID'		=>$salesOrderId,
			'DISPATCHED_GOOD_QTY'	=>$goodQty,
			'INVOICE_BALANCE'		=>$goodQty 
		);
		
		$result	=  $finance_customer_invoice_balance->insert($data);
  	}
	else
	{
		$data =array(
			'DISPATCHED_GOOD_QTY'	=>'+'.$goodQty,
			'INVOICE_BALANCE'		=>'+'.$goodQty 
		);
		$result	=  $finance_customer_invoice_balance->upgrade($data, " ORDER_YEAR = $orderYear AND ORDER_NO = $orderNo ");
 		
	}
	
	if(!$result['0']) 
		throw new Exception($result['1']);
	else
		return true;
	
}

//for validate dispatch status
function validateDispatchStatusForReject($dispatchNo,$dispatchYear,$dispatchStatus)
{
	if($dispatchStatus==1)
		throw new Exception("This dispatch No ($dispatchNo/$dispatchYear) is final approved. Can't reject");
	else if($dispatchStatus==0)
		throw new Exception("This dispatch No ($dispatchNo/$dispatchYear) is already rejected");
	else if($dispatchStatus==-2)
		throw new Exception("This is cancelled.Cannot reject.");
	else if($dispatchStatus==-10)
		throw new Exception("This is completed.Cannot reject.");
	else if($dispatchStatus==-1)
		throw new Exception("This is revised.Cannot reject.");
	else
		return true;
}

//for validate dispatch status
function validateDispatchStatusForSave($dispatchNo,$dispatchYear)
{
	global $db;
	global $ware_fabricdispatchheader;
	
	
	if($ware_fabricdispatchheader->getintStatus()==1)
		throw new Exception("This dispatch No ($dispatchNo/$dispatchYear) is final approved. Can't save");
	else if($ware_fabricdispatchheader->getintStatus()> 1 && $ware_fabricdispatchheader->getintStatus() <= $ware_fabricdispatchheader->getintApproveLevels())
		throw new Exception("This dispatch No ($dispatchNo/$dispatchYear) is approved. Can't save");
	else if($ware_fabricdispatchheader->getintStatus()==-2)
		throw new Exception("This is cancelled.Cannot save.");
	else if($ware_fabricdispatchheader->getintStatus()==-10)
		throw new Exception("This is completed.Cannot save.");
	else
		return true;
}
?>
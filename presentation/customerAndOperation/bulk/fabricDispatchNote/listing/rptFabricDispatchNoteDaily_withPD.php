<?php


$thisFilePath =  $_SERVER['PHP_SELF'];
include_once '../../../../../dataAccess/LoginDBManager.php';
include_once "../../../../../libraries/mail/mail_bcc.php";		
$db =  new LoginDBManager();

//$serialNo = $_REQUEST['serialNo'];
//$year = $_REQUEST['year'];
$approveMode = $_REQUEST['approveMode'];

$programName='Dispatch Note';
$programCode='P0470';

/*$gatePassNo = '100000';
$year = '2012';
$approveMode=1;
*/
ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Fabric Dispatch Report</title>


<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:272px;
	top:511px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}


 table .bordered{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}


.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}



.odd{background-color:#F5F5F5;}
.even{background-color:#FFFFFF;}
.mouseover {
	cursor: pointer;
}


.txtNumber{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:right;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.txtText{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:left;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}

.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntGrey {
	font-family: Verdana;
	font-size: 11px;
	color: #999;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:right;
}
</style>
</head>
<body><form id="frmFabDispatchReport" name="frmFabDispatchReport" method="post" action="rptFabricDispatchNote.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
    <td colspan="9" align="center" ><table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="tophead"><table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td width="12%">&nbsp;</td>
            <td width="17%" class="normalfnt"><img style="vertical-align:" src="http://accsee.com/screenline.jpg"  /></td>
            <td align="center" valign="top" width="55%" class="topheadBLACK"><?php
			
	 $SQL = "SELECT
*
FROM
mst_companies
Inner Join mst_country ON mst_companies.intCountryId = mst_country.intCountryID
WHERE
mst_companies.intId =  '1'
;";
	
	$result = $db->RunQuery($SQL);
	$row = mysqli_fetch_array($result);
	$companyName		= $row["strName"];
	$locationName		= $row["locationName"];
	$companyAddress1	= $row["strAddress"];
	$companyStreet		= $row["strStreet"];
	$companyCity		= $row["strCity"];
	$companyCountry		= $row["strCountryName"];
	$companyZipCode		= $row["strZip"];
	$companyPhone		= "(".$companyZipCode.")".$row["strPhoneNo"];
	$companyFax		 	= "(".$companyZipCode.")".$row["strFaxNo"];
	$companyEmail		= $row["strEMail"];
	$companyWeb		 	= $row["strWebSite"];
	
	?>
              <b><?php echo $companyName.""; ?></b>
              <p class="normalfntMid">
                <?php // (".$locationName.") --> Edited By Lasantha 14th of May 2012?>
                <?php echo $companyAddress1." ".$companyStreet." ".$companyCity." ".$companyCountry."."."<br><b>Tel : </b>".$companyPhone." <b>Fax : </b>".$companyFax." <br><b>E-Mail : </b>".$companyEmail." <b>Web : </b>".$companyWeb;?></p></td>
            <td width="16%" class="tophead"><div align="left"><img style="vertical-align:" src="http://accsee.com/nsoft_logo.png"  /></div></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ></div>
<table align="center" width="747"><tr><td width="119">&nbsp;</td><td width="616" align="center">DISPATCH SUMMARY REPORT</td></tr></table>

<table width="1048" align="center">
<tr>
<td width="1040"><table bgcolor="#999999" width="109%" border="1"  class="" id="" cellspacing="0" cellpadding="0" >
  <thead>
    <tr class="">
      <th colspan="15" bgcolor="#C5D5FE" >All Customer Summary</th>
      </tr>
    <tr class="normalfntMid">
      <th width="10%" bgcolor="#C5D5FE" >Plant</th>
      <th width="10%" bgcolor="#C5D5FE" >Brand</th>
      <th width="10%" bgcolor="#C5D5FE" >Order No</th>
      <th width="13%" bgcolor="#C5D5FE" >Sales Order No</th>
      <th width="13%" bgcolor="#C5D5FE" >Graphic No</th>
      <th width="10%" bgcolor="#C5D5FE" >Style No</th>
      <th width="12%" bgcolor="#C5D5FE" >Customer PO</th>
      <th width="9%" bgcolor="#C5D5FE" >Size</th>
      <th width="9%" bgcolor="#C5D5FE" >Sample Qty</th>
      <th width="7%" bgcolor="#C5D5FE" >Good Qty</th>
      <th width="8%" bgcolor="#C5D5FE" >EMD-D Qty</th>
      <th width="7%" bgcolor="#C5D5FE" >P-D Qty</th>
      <th width="6%" bgcolor="#C5D5FE" >F-D Qty</th>
      <th width="8%" bgcolor="#C5D5FE" >Total Qty</th>
      <th width="8%" bgcolor="#C5D5FE" >PD %</th>
    </tr>
  </thead>
  <tbody>
<?php
$curruntTime=date("Y-m-d H:i:s");
$curruntTimeFrom = date("Y-m-d H:i:s",strtotime(date('Y-m-j H:i:s')) - (24 * 60 * 60));//correct
$sql1 = "SELECT
	distinct 
	ware_fabricdispatchheader_approvedby.dtApprovedDate,
	trn_orderdetails.strStyleNo,
	trn_orderdetails.strGraphicNo,
	trn_orderheader.strCustomerPoNo,
	ware_fabricdispatchheader.intOrderNo,
	ware_fabricdispatchheader.intOrderYear,
	trn_orderdetails.strSalesOrderNo,
	ware_fabricdispatchdetails.strSize,
	Sum(ware_fabricdispatchdetails.dblSampleQty) 				AS dblSampleQty,
	Sum(ware_fabricdispatchdetails.dblGoodQty) 					AS dblGoodQty,
	Sum(ware_fabricdispatchdetails.dblEmbroideryQty) 			AS dblEmbroideryQty,
	Sum(ware_fabricdispatchdetails.dblPDammageQty) 				AS dblPDammageQty,
	Sum(ware_fabricdispatchdetails.dblFDammageQty) 				AS dblFDammageQty,
	mst_brand.strName 											AS brandName,
	trn_orderdetails.dblDamagePercentage,
	
	(SELECT GROUP_CONCAT(DISTINCT SUB_P.strPlantName)
	FROM ware_fabricdispatchheader SUB_FDH
	INNER JOIN mst_locations SUB_LO
	  ON SUB_LO.intId = SUB_FDH.intCompanyId
	INNER JOIN mst_plant SUB_P
	  ON SUB_P.intPlantId = SUB_LO.intPlant
	WHERE SUB_FDH.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo
		AND SUB_FDH.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear)AS PLANT
		
FROM trn_orderdetails	
INNER JOIN trn_orderheader 
	ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo 
	AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
INNER JOIN ware_fabricdispatchheader 
	ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo 
	AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear
INNER JOIN ware_fabricdispatchdetails 
	ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo 
	AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear 
	AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
INNER JOIN mst_part 
	ON ware_fabricdispatchdetails.intPart = mst_part.intId
INNER JOIN mst_colors_ground 
	ON ware_fabricdispatchdetails.intGroundColor = mst_colors_ground.intId
INNER JOIN ware_fabricdispatchheader_approvedby 
	ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchheader_approvedby.intBulkDispatchNo 
	AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchheader_approvedby.intYear 
	AND ware_fabricdispatchheader_approvedby.intApproveLevelNo = ware_fabricdispatchheader.intApproveLevels
INNER JOIN trn_sampleinfomations 
	ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo 
	AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear 
	AND trn_sampleinfomations.intRevisionNo = trn_orderdetails.intRevisionNo
INNER JOIN mst_brand 
	ON mst_brand.intId = trn_sampleinfomations.intBrand
INNER JOIN mst_customer
	ON mst_customer.intId = trn_orderheader.intCustomer
WHERE  
	ware_fabricdispatchheader_approvedby.dtApprovedDate < NOW()   
	AND ware_fabricdispatchheader_approvedby.dtApprovedDate >= DATE_ADD(NOW(),INTERVAL -1 DAY )    
	AND ware_fabricdispatchheader.intStatus =  '1'  
GROUP BY
	ware_fabricdispatchheader.intOrderNo,
	ware_fabricdispatchheader.intOrderYear,
	trn_orderdetails.strGraphicNo,
	ware_fabricdispatchdetails.strSize
ORDER BY
	ware_fabricdispatchheader.intOrderYear ASC,
	ware_fabricdispatchheader.intOrderNo ASC,
	trn_orderdetails.strSalesOrderNo ASC,
	trn_orderdetails.strStyleNo ASC,
	trn_orderheader.strCustomerPoNo ASC";
	
	//echo $sql1;
			$result1 = $db->RunQuery($sql1);
			$totsampleQty		= 0;
			$totgoodQty			= 0;
			$totembroideryQty	= 0;
			$totpDammageQty		= 0;
			$totfDammageQty		= 0;
			$total				= 0;
			$totQty				= 0;
			$totAmmount			= 0;			
			$haveRecords 		= false;
			$i=0;
			while($row1=mysqli_fetch_array($result1))
			{
				
				$haveRecords 			= true;
				$intBulkDispatchNo 		= $row1['intBulkDispatchNo'];
				$dblDamagePercentage 	= $row1['dblDamagePercentage'];
				$intBulkDispatchNoYear 	= $row1['intBulkDispatchNoYear'];
				$brandName 				= $row1['brandName'];				
				$style 					= $row1['strStyleNo']; 
				$graphicNo 				= $row1['strGraphicNo']; 
				$custPO 				= $row1['strCustomerPoNo'];
				$orderNo 				= $row1['intOrderNo'];
				$orderYear 				= $row1['intOrderYear'];				
				$cutNo					= $row1['strCutNo'];
				$salesOrderNo			= $row1['strSalesOrderNo'];
				$part					= $row1['part'];
				$bgColor				= $row1['bgcolor'];
				$lineNo					= $row1['strLineNo'];
				$size					= $row1['strSize'];
				$sampleQty				= $row1['dblSampleQty'];
				$goodQty				= $row1['dblGoodQty'];
				$embroideryQty			= $row1['dblEmbroideryQty'];
				$pDammageQty			= $row1['dblPDammageQty'];
				$fDammageQty			= $row1['dblFDammageQty'];
				$remarks				= $row1['strRemarks'];
				$plant					= $row1["PLANT"];
				$tot					= (float)($sampleQty)+(float)($goodQty)+(float)($embroideryQty)+(float)($pDammageQty)+(float)($fDammageQty);
				if( 1 >= $pDammageQty/$tot*100)
					continue;
	  ?>
      
      
       <?php 
		if($graphicNo != $graphicNo2 || $salesOrderNo != $salesOrderNo2 || $orderno !=$orderno2 || $orderYear != $orderYear2 || $plant2!=$plant  )
		{
			if(++$i>1)
			{
			/*$graphicNo2 = '';
			$salesOrderNo2 = '' ;
			$orderNo2 = '';
			$orderyear2 = '';
			$plant2 = '';*/
			
	?>
    <tr>
    	<td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo $s_totalSampleQty; ?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo $s_totalGoodQty; ?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo $s_embdQty; ?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo $s_pdQty; ?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo $s_fdQty; ?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo $s_total ; ?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" style="color:<?php 
		if($s_pdQty/$s_total*100 >1)
			echo "red";
		?>"><b><?php echo number_format($s_pdQty/$s_total*100,2) ?>%</b></td>
        <?php 
			$s_totalSampleQty = 0;
			$s_totalGoodQty	  = 0;
			$s_embdQty		  = 0;
			$s_pdQty		  = 0;
			$s_fdQty		  = 0;
			$s_total 		  = 0; 
		?>
    </tr>
    <tr>
    	<td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF"><b>&nbsp;</b></td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        
    </tr>
    <?php 
			}
		}
	?>
      
      
    <tr class="normalfnt"   bgcolor="#FFFFFF">
      <td align="center" class="normalfnt" ><?php echo $plant?></td>
      <td align="center" class="normalfntMid" id="<?php echo $orderYear; ?>2" ><?php echo $brandName; ?></td>
      <td align="center" class="normalfntMid" id="<?php echo $orderNo; ?>2" ><?php echo $orderYear.'/'.$orderNo; ?></td>
      <td align="center" class="normalfntMid" id="<?php echo $salesOrderNo; ?>2" ><?php echo $salesOrderNo; ?></td>
      <td align="center" class="normalfntMid" id="<?php echo $graphicNo; ?>2" ><?php echo $graphicNo; ?></td>
      <td align="center" class="normalfntMid" id="<?php echo $style; ?>2" ><?php echo $style; ?></td>
      <td align="center" class="normalfntMid" id="<?php echo $custPO; ?>2" ><?php echo $custPO; ?></td>
      <td align="center" class="normalfntMid" id="<?php echo $qty; ?>2"><?php echo $size; ?></td>
      <td align="center" class="normalfntRight" id="<?php echo $qty; ?>2"><?php echo $sampleQty; ?></td>
      <td align="center" class="normalfntRight" id="<?php echo $qty; ?>2"><?php echo $goodQty; ?></td>
      <td align="center" class="normalfntRight" id="<?php echo $qty; ?>2"><?php echo $embroideryQty; ?></td>
      <td align="center" class="normalfntRight" id="<?php echo $qty; ?>2"><?php echo $pDammageQty; ?></td>
      <td align="center" class="normalfntRight" id="<?php echo $qty; ?>2"><?php echo $fDammageQty; ?></td>
      <td align="center" class="normalfntRight" id="<?php echo $qty; ?>2"><?php echo $tot; ?></td>
      <td align="center" class="normalfntRight" style="color:<?php				
				/*
				Mr.Nishantha requested , if Production damage percentage(PD) is exceeded than 2% , it should be red mark.dont get PD % from order tables , becouse 2% is for this report only.
				*/
				if( 1 <$pDammageQty/$tot*100)
					echo "red";
					
				 ?>" id="<?php echo $qty; ?>2"><?php echo number_format($pDammageQty/$tot*100,2); ?>%</td>
    </tr>
   
   <!-- <tr>
    	<td>
    		<table width="100%"  class="bordered" id="" cellspacing="0" cellpadding="0" >
              <thead>
                <tr class="">
                  <th width="10%" >Size</th>
                  <th width="10%" ></th>
                </tr>
            </table>
    	</td>
    </tr>-->
<?php 
			$graphicNo2 = $graphicNo;
			$salesOrderNo2 = $salesOrderNo;
			$orderNo2 = $orderNo;
			$orderYear2 = $orderYear;
			$plant2 = $plant;
			
			$totsampleQty		+= $sampleQty;
			$totgoodQty			+= $goodQty;
			$totembroideryQty	+= $embroideryQty;
			$totpDammageQty		+= $pDammageQty;
			$totfDammageQty		+= $fDammageQty;
			$total				+= $tot;
			
			$s_totalSampleQty += $sampleQty;
			$s_totalGoodQty	  += $goodQty;
			$s_embdQty		  += $embroideryQty;
			$s_pdQty		  += $pDammageQty;
			$s_fdQty		  += $fDammageQty;
			$s_total 		  += $tot;
			}
			
?>
    <tr class="normalfnt"  bgcolor="#CCCCCC">
      <td class="normalfnt" >&nbsp;</td>
      <td class="normalfnt" >&nbsp;</td>
      <td class="normalfnt" >&nbsp;</td>
      <td class="normalfnt" >&nbsp;</td>
      <td class="normalfnt" >&nbsp;</td>
      <td class="normalfnt" >&nbsp;</td>
      <td class="normalfnt" >&nbsp;</td>
      <td class="normalfntRight" >&nbsp;</td>
      <td class="normalfntRight" ><b><?php echo $totsampleQty ?></b></td>
      <td class="normalfntRight" ><b><?php echo $totgoodQty ?></b></td>
      <td class="normalfntRight" ><b><?php echo $totembroideryQty ?></b></td>
      <td class="normalfntRight" ><b><?php echo $totpDammageQty ?></b></td>
      <td class="normalfntRight" ><b><?php echo $totfDammageQty ?></b></td>
      <td class="normalfntRight" ><b><?php echo $total ?></b></td>
      <td class="normalfntRight" ><b><?php echo number_format($totpDammageQty/$total*100,2) ?>%</b></td>
    </tr>
  </tbody>
</table></td>
</tr>
</table>
<hr>
<?php 
$body_header = ob_get_clean();
 
    $sql_cus = "SELECT
mst_customer.intId,
mst_customer.strCode,
mst_customer.strName,
mst_customer.strEmail,
mst_customer_locations.intLocationId,
mst_customer_locations.strEmailAddress AS custLocaEmail,
mst_customer_locations_header.strName as custLocName
FROM
mst_customer
INNER JOIN mst_customer_locations ON mst_customer.intId = mst_customer_locations.intCustomerId
INNER JOIN mst_customer_locations_header ON mst_customer_locations.intLocationId = mst_customer_locations_header.intId
WHERE 

mst_customer.intMailAlertDispatchSummaryReport =  '1'
ORDER BY mst_customer.strName
";
				 $result_cus = $db->RunQuery($sql_cus);
				 while($row_cus=mysqli_fetch_array($result_cus))
				 { 
					$customerId = $row_cus['intId'];
					$customerName = $row_cus['strName'];
					$customerEmail = $row_cus['strEmail'];
					$custLocation = $row_cus['intLocationId'];
					$custLocaEmail = $row_cus['custLocaEmail'];
					$custLocName = $row_cus['custLocName'];
				ob_start();
?>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="10" align="center"></td>
    </tr>
    
  <tr>
    <td width="5%">&nbsp;</td>
    <td width="11%" class="normalfnt">Customer</td>
    <td width="4%" align="center" valign="middle"><strong>:</strong></td>
    <td width="20%"><span class="normalfnt"><?php echo $customerName; ?></span></td>
    <td width="7%"><span class="normalfnt">Location</span></td>
    <td width="4%" align="center" valign="middle"><strong>:</strong></td>
    <td width="24%"><span class="normalfnt"><?php echo $custLocName; ?></span></td>
    <td width="6%" class="normalfnt">Date</td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="18%"><span class="normalfnt"><?php echo date('Y-m-d'); ?></span></td>
  </tr>
  
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td>
          
          <table width="100%"  class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <thead>
              <tr class="">
                <th width="10%" >Plant</th>
                <th width="10%" >Brand</th>
                <th width="10%" >Dispatch No</th>
                <th width="10%" >Order No</th>
                <th width="13%" >Sales Order No</th>
                <th width="13%" >Graphic No</th>
                <th width="10%" >Style No</th>
                <th width="12%" >Customer PO</th>
                <th width="9%" >Sample Qty</th>
                <th width="7%" >Good Qty</th>
                <th width="8%" >EMD-D Qty</th>
                <th width="7%" >P-D Qty</th>
                <th width="6%" >F-D Qty</th>
                <th width="8%" >Total Qty</th>
                <th width="8%" >PD %</th>
                </tr>
              </thead>
            <tbody>
              <?php 
			  $curruntTime=date("Y-m-d H:i:s");
			  $curruntTimeFrom = date("Y-m-d H:i:s",strtotime(date('Y-m-j H:i:s')) - (24 * 60 * 60));//correct
			 //$curruntTimeFrom = date("Y-m-d H:i:s",strtotime(date('Y-m-j H:i:s')) - (2*60*24 * 60 * 60));//temp
			  
			  
			  
			  //comment by roshan for adding customer brand and dispatch no
			  
	  	      /*$sql1 = "SELECT 
trn_orderdetails.strStyleNo,
trn_orderdetails.strGraphicNo, 
trn_orderheader.strCustomerPoNo,
ware_fabricdispatchheader.intOrderNo,
ware_fabricdispatchheader.intOrderYear,
trn_orderdetails.strSalesOrderNo,
sum(ware_fabricdispatchdetails.dblSampleQty) as dblSampleQty,
sum(ware_fabricdispatchdetails.dblGoodQty) as dblGoodQty,
sum(ware_fabricdispatchdetails.dblEmbroideryQty) as dblEmbroideryQty,
sum(ware_fabricdispatchdetails.dblPDammageQty) as dblPDammageQty,
sum(ware_fabricdispatchdetails.dblFDammageQty) as dblFDammageQty 
FROM
trn_orderdetails 
Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear 
Inner Join ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear
Inner Join ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
left Join mst_part ON ware_fabricdispatchdetails.intPart = mst_part.intId
left Join mst_colors_ground ON ware_fabricdispatchdetails.intGroundColor = mst_colors_ground.intId 
Inner Join ware_fabricdispatchheader_approvedby ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchheader_approvedby.intBulkDispatchNo 
AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchheader_approvedby.intYear AND ware_fabricdispatchheader_approvedby.intApproveLevelNo=ware_fabricdispatchheader.intApproveLevels 
WHERE  
ware_fabricdispatchheader_approvedby.dtApprovedDate <'$curruntTime' AND  
ware_fabricdispatchheader_approvedby.dtApprovedDate >= '$curruntTimeFrom' AND  
ware_fabricdispatchheader.intStatus =  '1'  
AND trn_orderheader.intCustomer = '$intCustomer'

group by trn_orderdetails.strSalesOrderNo , 
ware_fabricdispatchheader.intOrderNo, 
ware_fabricdispatchheader.intOrderYear  
order by 
ware_fabricdispatchheader.intOrderYear asc , 
ware_fabricdispatchheader.intOrderNo asc , 
trn_orderdetails.strSalesOrderNo asc ,
trn_orderdetails.strStyleNo asc,
trn_orderheader.strCustomerPoNo asc 

";*/
			$sql1 = "SELECT
distinct 
mst_plant.strPlantName								 							AS plantName,
ware_fabricdispatchheader.intBulkDispatchNo,
ware_fabricdispatchheader.intBulkDispatchNoYear,
ware_fabricdispatchheader_approvedby.dtApprovedDate,
trn_orderdetails.strStyleNo,
trn_orderdetails.strGraphicNo,
trn_orderheader.strCustomerPoNo,
ware_fabricdispatchheader.intOrderNo,
ware_fabricdispatchheader.intOrderYear,
trn_orderdetails.strSalesOrderNo,
Sum(ware_fabricdispatchdetails.dblSampleQty) AS dblSampleQty,
Sum(ware_fabricdispatchdetails.dblGoodQty) AS dblGoodQty,
Sum(ware_fabricdispatchdetails.dblEmbroideryQty) AS dblEmbroideryQty,
Sum(ware_fabricdispatchdetails.dblPDammageQty) AS dblPDammageQty,
Sum(ware_fabricdispatchdetails.dblFDammageQty) AS dblFDammageQty,
mst_brand.strName AS brandName,
trn_orderdetails.dblDamagePercentage
FROM
trn_orderdetails
Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
Inner Join ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear
Inner Join ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
Left Join mst_part ON ware_fabricdispatchdetails.intPart = mst_part.intId
Left Join mst_colors_ground ON ware_fabricdispatchdetails.intGroundColor = mst_colors_ground.intId
Inner Join ware_fabricdispatchheader_approvedby ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchheader_approvedby.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchheader_approvedby.intYear AND ware_fabricdispatchheader_approvedby.intApproveLevelNo = ware_fabricdispatchheader.intApproveLevels
Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_orderdetails.intRevisionNo
Inner Join mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
INNER JOIN mst_locations 
	ON mst_locations.intId = ware_fabricdispatchheader.intCompanyId
INNER JOIN mst_plant 
    ON mst_plant.intPlantId = mst_locations.intPlant
WHERE  
ware_fabricdispatchheader_approvedby.dtApprovedDate < NOW()  AND  
ware_fabricdispatchheader_approvedby.dtApprovedDate >= DATE_ADD(NOW(),INTERVAL -1 DAY )  AND  
ware_fabricdispatchheader.intStatus =  '1'  
AND trn_orderheader.intCustomer = $customerId 
AND ware_fabricdispatchheader.intCustLocation = $custLocation
GROUP BY
trn_orderdetails.strSalesOrderNo,
ware_fabricdispatchheader.intOrderNo,
ware_fabricdispatchheader.intOrderYear,
trn_sampleinfomations.intBrand,
ware_fabricdispatchheader.intBulkDispatchNo,
ware_fabricdispatchheader.intBulkDispatchNoYear
ORDER BY
ware_fabricdispatchheader.intOrderYear ASC,
ware_fabricdispatchheader.intOrderNo ASC,
trn_orderdetails.strSalesOrderNo ASC,
trn_orderdetails.strStyleNo ASC,
trn_orderheader.strCustomerPoNo ASC
";
			$result1 = $db->RunQuery($sql1);
			
			$totsampleQty=0;
			$totgoodQty=0;
			$totembroideryQty=0;
			$totpDammageQty=0;
			$totfDammageQty=0;
			$total=0;
			$totQty=0;
			$totAmmount=0;
			
			$haveRecords = false;
			$STR_GRAHPICNO_LIST = '';
			while($row1=mysqli_fetch_array($result1))
			{
				$haveRecords = true;	
				$intBulkDispatchNo = $row1['intBulkDispatchNo'];
				$dblDamagePercentage = $row1['dblDamagePercentage'];
				$intBulkDispatchNoYear = $row1['intBulkDispatchNoYear'];
				$brandName = $row1['brandName'];
				
				$style = $row1['strStyleNo']; 
				
				/////////////////concat graphic no's///////////
						if($graphicNo!=$row1['strGraphicNo'])
						{
							$STR_GRAHPICNO_LIST .=$row1['strGraphicNo'] . ' / ';	
						}
				///////////////////////////////////////////////
				$graphicNo = $row1['strGraphicNo']; 
				$custPO = $row1['strCustomerPoNo'];
				$orderNo = $row1['intOrderNo'];
				$orderYear = $row1['intOrderYear'];
				
				$cutNo=$row1['strCutNo'];
				$salesOrderNo=$row1['strSalesOrderNo'];
				$part=$row1['part'];
				$bgColor=$row1['bgcolor'];
				$lineNo=$row1['strLineNo'];
				$size=$row1['strSize'];
				$sampleQty=$row1['dblSampleQty'];
				$goodQty=$row1['dblGoodQty'];
				$embroideryQty=$row1['dblEmbroideryQty'];
				$pDammageQty=$row1['dblPDammageQty'];
				$fDammageQty=$row1['dblFDammageQty'];
				$remarks=$row1['strRemarks'];
				$tot=(float)($sampleQty)+(float)($goodQty)+(float)($embroideryQty)+(float)($pDammageQty)+(float)($fDammageQty);
				
	  ?>
              <tr class="normalfnt"   bgcolor="#FFFFFF">
                <td align="center" class="normalfnt" id="<?php echo $orderYear; ?>" ><?php echo $row1["plantName"]?></td>
                <td align="center" class="normalfntMid" id="<?php echo $orderYear; ?>" ><?php echo $brandName; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $orderYear; ?>" ><?php echo $intBulkDispatchNoYear.'/'.$intBulkDispatchNo; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $orderNo; ?>" ><?php echo $orderYear.'/'.$orderNo; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $salesOrderNo; ?>" ><?php echo $salesOrderNo; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $graphicNo; ?>" ><?php echo $graphicNo; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $style; ?>" ><?php echo $style; ?></td>
                <td align="center" class="normalfntMid" id="<?php echo $custPO; ?>" ><?php echo $custPO; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $sampleQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $goodQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $embroideryQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $pDammageQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $fDammageQty; ?></td>
                <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo $tot; ?></td>
                <td align="center" class="normalfntRight" style="color:<?php
				
				/*
				Mr.Nishantha requested , if Production damage percentage(PD) is exceeded than 2% , it should be red mark.dont get PD % from order tables , becouse 2% is for this report only.
				*/
				/* Mr.Nishantha requested set it to 1% 2013-07-03 */
				if( 1 <$pDammageQty/$tot*100)
					echo "red";
					
				 ?>" id="<?php echo $qty; ?>"><?php echo number_format($pDammageQty/$tot*100,2); ?>%</td>
                </tr>              
              <?php 
			$totsampleQty+=$sampleQty;
			$totgoodQty+=$goodQty;
			$totembroideryQty+=$embroideryQty;
			$totpDammageQty+=$pDammageQty;
			$totfDammageQty+=$fDammageQty;
			$total+=$tot;
			
			
			
			}
	  ?>
              <tr class="normalfnt"  bgcolor="#CCCCCC">
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfnt" >&nbsp;</td>
                <td class="normalfntRight" ><b><?php echo $totsampleQty ?></b></td>
                <td class="normalfntRight" ><b><?php echo $totgoodQty ?></b></td>
                <td class="normalfntRight" ><b><?php echo $totembroideryQty ?></b></td>
                <td class="normalfntRight" ><b><?php echo $totpDammageQty ?></b></td>
                <td class="normalfntRight" ><b><?php echo $totfDammageQty ?></b></td>
                <td class="normalfntRight" ><b><?php echo $total ?></b></td>
                <td class="normalfntRight" ><b><?php echo number_format($totpDammageQty/$total*100,2) ?>%</b></td>
                </tr>
              </tbody>
          </table>        </td>
        </tr>
      
      </table>
    </td>
</tr>
</table>

<?php
	if($haveRecords)
		 $body .= ob_get_clean();
	else
		ob_get_clean();	

}
		 $foot = "</div>        
				</form>
				</body>
				</html>";
 	   $body = $body_header.$body.$foot;
	  
		echo $body;
		//if($haveRecords)
		//{
			$nowDate = date('Y-m-d');
			$mailHeader= "DISPATCH SUMMARY ($nowDate) - All Customer";
			
			$mailcount++;
			$sql = "SELECT
						sys_users.strEmail,sys_users.strFullName
					FROM
					sys_mail_eventusers
						Inner Join sys_users ON sys_users.intUserId = sys_mail_eventusers.intUserId
					WHERE
						sys_mail_eventusers.intMailEventId =  '1010'
					";
			$result = $db->RunQuery($sql);
			$ccEmail = '';
			while($row=mysqli_fetch_array($result))
			{
				//sendMessage('nsoft@screenlineholdings.com','NSOFT - SCREENLINE HOLDINGS.',$row['strEmail'],'COPY - '.$mailHeader,$body);		
				$userEmail .= $row['strEmail'].',';
				//$mailcount++;
			}
			
			/*Live*/
			sendMessage('nsoft@screenlineholdings.com','NSOFT - SCREENLINE HOLDINGS.',$userEmail,$mailHeader,$body,'','nsoftemail@gmail.com,dhammika@screenlineholdings.com,roshan@screenlineholdings.com;brahman@screenlineholdings.com;mihindu@screenlineholdings.com');
			
			/*for testing*/
			/*sendMessage('nsoft@screenlineholdings.com','NSOFT - SCREENLINE HOLDINGS.','roshan@screenlineholdings.com',$mailHeader,$body,'','');*/
			
		
		//send to creator
//	$mailHeader= "COPY - $emailHeaderC";
//	sendMessage($enterUserEmail,$enterUserName,$creatorEmail,$mailHeader,$body);
//	}
	
	
//}
echo 'DISPATCH CUSTOMER WISE REPORT -> MAIL COUNT : '.$mailcount.'<br>';
?>
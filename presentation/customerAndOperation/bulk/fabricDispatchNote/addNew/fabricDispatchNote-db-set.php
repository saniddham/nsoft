<?php 
	session_start();
	//ini_set('display_errors',1);
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";	
	require_once "{$backwardseperator}class/cls_commonFunctions_get.php";
	require_once "{$backwardseperator}class/customerAndOperation/bulk/cls_bulk_get.php";
	include_once "{$backwardseperator}class/cls_commonFunctions_get.php";        $cls_commonFunctions_get   = 	new cls_commonFunctions_get($db);
	
	//--------------------------------------------------
	
	$objcomfunc 	= new cls_commonFunctions_get($db);
	$obj_bulk_get 	= new Cls_bulk_get($db);
	
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo 	 = $_REQUEST['serialNo'];
	$year 		 = $_REQUEST['Year'];
/*	$style  = $_REQUEST['style'];
	$graphicNo 	 = $_REQUEST['graphicNo'];
	$cusromerPO 	 = $_REQUEST['cusromerPO'];
*/	$custLocation 	 = $_REQUEST['custLocation'];
	$orderNo 	 = $_REQUEST['orderNo'];
	$orderYear 	 = $_REQUEST['orderYear'];
//	$AODNo 	 = $_REQUEST['AODNo'];
	$dispatchDate 	 = $_REQUEST['dispatchDate'];
	$remarks 	 = $_REQUEST['remarks'];
	//echo $_REQUEST['arr'];
	$arr 		= json_decode($_REQUEST['arr'], true);
	
	$programName='Dispatch Note';
	$programCode='P0470';
	
	$ApproveLevels = (int)getApproveLevel($programName);
	$maxStatus = $ApproveLevels+1;
//----------------------

//--------------------------save temp barcode wise data in temp tbl--------------------------------------------------
if($requestType=='save_bar_code_wise_data_temp')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	
	$serialNo 	 	= $_REQUEST['serialNo'];
	$Year 		 	= $_REQUEST['Year'];
	$tmpdisNo 		= $_REQUEST['tmpdisNo'];
	
	$getArrBody 	=  json_decode($_REQUEST['arrBody'],true);
	
	if($tmpdisNo=="")
	{ 
		$temp_dispatch_no 	= $cls_commonFunctions_get->GetSystemMaxNoNew('TEMP_DISPATCH_NO',$location,RunQuery2);
		$tmp_dispatch 		= $temp_dispatch_no ["max_no"];
		
	}
	else
	{	 $tmp_dispatch 		= $tmpdisNo;
	}
	
	  foreach ($getArrBody AS $arrBody)
	  {
			$cut_no 	= $arrBody['cut_no'];
			$sales_no 	= $arrBody['sales_no'];
			$size 		= $arrBody['size'];
			$bcode 		= $arrBody['bcode'];
			$part 		= $arrBody['part'];
			$bgColor 	= $arrBody['bgColor'];
			$line 		= $arrBody['line'];
			$samp_qty 	= $arrBody['samp_qty'];
			$good_qty 	= $arrBody['good_qty'];
			$emb_qty 	= $arrBody['emb_qty'];
			$pd_qty 	= $arrBody['pd_qty'];
			$fd_qty 	= $arrBody['fd_qty'];
			$cut 		= $arrBody['cut'];
			$remark 	= $arrBody['remark'];
			
			if($fd_qty=="")
				$fd_qty = 0;
			if($samp_qty=="")
				$samp_qty = 0;
			if($good_qty=="")
				$good_qty = 0;
			if($emb_qty=="")
				$emb_qty = 0;
			if($pd_qty=="")
				$pd_qty = 0;
			if($cut=="")
				$cut = 0;
			
				
			
			$result = save_data_temporary($tmp_dispatch,$serialNo,$Year,$cut_no,$sales_no,$size,$bcode,$part,$bgColor,$line,$samp_qty,$good_qty,$emb_qty,$pd_qty,$fd_qty,$cut, $remark);   
	 
			 if($serialNo!="")
				{ 
					$result_select 		= get_data_from_temp($tmpdisNo);
					while($row_select	= mysqli_fetch_array($result_select))
						{  
	  					save_barcode_wise_data($serialNo,$Year,$tmpdisNo,$row_select);
						}
				}
	  }
		  
		if($result)
				{
					$response['tmpdisno'] 	= $tmp_dispatch;
					$response['type'] 		= "pass";	
					$response['msg'] 		= "save in temporary table";	
				}
				
				else
				{
					$response['type'] 		= "fail";	
					$response['msg'] 		= "max number not generated";
				}
			
	$db->RunQuery2('Commit');

}
//----------------------load to the main interface--------------------------
if($requestType=='load_to_the_main_interface')
{	
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	
	$serialNo 	 	= $_REQUEST['serialNo'];
	$Year 		 	= $_REQUEST['Year'];
	$tmpdisNo 		= $_REQUEST['tmpdisNo'];
	$getArrBody 	=  json_decode($_REQUEST['arrBody'],true);
	
	foreach ($getArrBody AS $arrBody)
	  {
			$cut_no 	= $arrBody['cut_no'];
			$sales_no 	= $arrBody['sales_no'];
			$size 		= $arrBody['size'];
			$bcode 		= $arrBody['bcode'];
			$part 		= $arrBody['part'];
			$bgColor 	= $arrBody['bgColor'];
			$line 		= $arrBody['line'];
			$samp_qty 	= $arrBody['samp_qty'];
			$good_qty 	= $arrBody['good_qty'];
			$emb_qty 	= $arrBody['emb_qty'];
			$pd_qty 	= $arrBody['pd_qty'];
			$fd_qty 	= $arrBody['fd_qty'];
			$cut 		= $arrBody['cut'];
			$remark 	= $arrBody['remark'];
			
	
	$result 		= get_quantities_from_temp($tmpdisNo,$sales_no,$cut_no,$size);
	
	while($row_select	= mysqli_fetch_array($result))
	{ 
	   $sample_qty 	= $row_select['sumQtyS'];
	   $good_qty 	= $row_select['sumQtyG'];
	   $EM_qty 		= $row_select['sumQtyEM'];
	   $cutR_qty 	= $row_select['sumQtycutR'];
	   $FD_qty 		= $row_select['sumQtyFD'];
	   $PD_qty 		= $row_select['sumQtyPD'];
	   
	}
	$tot =  $sample_qty +$good_qty +$EM_qty+$cutR_qty+$FD_qty+$PD_qty;
	
			$response['sample_qty'] =   $sample_qty;
			$response['good_qty'] 	=   $good_qty;	
			$response['EM_qty'] 	=   $EM_qty;	
			$response['cutR_qty'] 	=   $cutR_qty;	
			$response['FD_qty'] 	=   $FD_qty;
			$response['PD_qty'] 	=   $PD_qty;
			$response['tot'] 		=   $tot;
	  }

	$db->RunQuery2('Commit');
	
}


//-------------------save bar code wise data permanenly---------------------

if($requestType=='save_bar_code_wise_data')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	
	$serialNo 	 	= $_REQUEST['serialNo'];
	$Year 		 	= $_REQUEST['Year'];
	$tmpdisNo 		= $_REQUEST['tmpdisNo'];
	
	$result_select 		= get_data_from_temp($tmpdisNo);
	while($row_select	= mysqli_fetch_array($result_select))
	{  
	  save_barcode_wise_data($serialNo,$Year,$tmpdisNo,$row_select);
	}
	$db->RunQuery2('Commit');
	
}
//-------------------delete barcode wise data from pop up interface----------------------------------
if($requestType=="delete_barcode_wise_data")
{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$serialNo 	 	= $_REQUEST['serialNo'];
		$Year 		 	= $_REQUEST['Year'];
		$tmpdisNo 		= $_REQUEST['tmpdisNo'];
		$cut_no 		= $_REQUEST['cut_no'];
		$sales_no 		= $_REQUEST['sales_no'];
		$size 			= $_REQUEST['size'];
		$bcode 			= $_REQUEST['bcode'];
		
		delete_bar_code_data($serialNo,$Year,$tmpdisNo,$cut_no,$sales_no,$size,$bcode);
		
		$db->RunQuery2('Commit');					
		
}
//---------------------dlt barcode from main interface-------------------------------
if($requestType=="delete_barcode_wise_data_main")
{		
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		$serialNo 	 	= $_REQUEST['serialNo'];
		$Year 		 	= $_REQUEST['Year'];
		$tmpdisNo 		= $_REQUEST['tmpdisNo'];
		$cut_no 		= $_REQUEST['cut_no'];
		$sales_no 		= $_REQUEST['sales_no'];
		$size 			= $_REQUEST['size'];
		
		delete_bar_code_data_main($serialNo,$Year,$tmpdisNo,$cut_no,$sales_no,$size);
		
		$db->RunQuery2('Commit');
}

//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		$savableFlag=1;
	    $rollBackFlag=0;
        $salesOrderArray = array();
		if($serialNo==''){
			$serialNo 	= getNextSerialNo();
			$year = date('Y');
			$editMode=0;
			$savedStatus=$maxStatus;
			$savedLevels=$ApproveLevels;
		} else {
			$editMode=1;
			$savableFlag=getSaveStatus($serialNo,$year);//check wether already confirmed
			$sql = "SELECT
			ware_fabricdispatchheader.intStatus, 
			ware_fabricdispatchheader.intApproveLevels ,
			ware_fabricdispatchheader.intOrderNo, 
			ware_fabricdispatchheader.intOrderYear 
			FROM ware_fabricdispatchheader 
			WHERE
			ware_fabricdispatchheader.intBulkDispatchNo =  '$serialNo' AND
			ware_fabricdispatchheader.intBulkDispatchNoYear =  '$year'";

			$result = $db->RunQuery2($sql);
			$row	=mysqli_fetch_array($result);
			$savedStatus=$row['intStatus'];
			$savedLevels=$row['intApproveLevels'];
			
			$orderNo	=$row['intOrderNo'];
			$orderYear	=$row['intOrderYear'];
			
		}
	
		$editPermission =loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
		//---------------------------
		$lc_flag		= $obj_bulk_get->get_order_lc_flag($orderNo,$orderYear,'RunQuery2');
		if($lc_flag	== 6) 
			$lc_flag = 1;
		else
			$lc_flag = 0;
		$lc_status		= $obj_bulk_get->get_lc_status($orderNo,$orderYear,'RunQuery2');
		//--------------------------
		$locationFlag	=getLocationValidation('fabDispatch',$location,$serialNo,$year);
		//--------------------------
		$locationType 	= $objcomfunc->getLocationType2($location);
		//--------------------------
		
  		$plantId		= getPlantId($location);
  
		if($plantId!='')
		{
			$plantStatus	= getPlantStatus($plantId); 
		}
		
		 if(( $locationFlag==1) && ($editMode==1)){
			 $rollBackFlag=1;
			 $rollBackMsg="Invalid Edit Location";
		 }
		 else if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This Dispatch No is already confirmed.cant edit.";
		 }
		 else if($editPermission==0){
			 $rollBackFlag=1;
			 $rollBackMsg="No Permission to save.";
		 }
		 else if($locationType!=1){
			 $rollBackFlag=1;
			 $rollBackMsg="This is not a production location.So can't dispatch fabrics.";
		 }
		 else if($lc_flag==1 && $lc_status !=1){
			 $rollBackFlag=1;
			 $rollBackMsg="No approved LC.So can't dispatch fabrics.";
		 }
		 else if($plantId=='')
		 {
			 $rollBackFlag=1;
			 $rollBackMsg="This location is not allocate to a plant.";
		 }
		 else if($plantStatus!=1)
		 {
			 $rollBackFlag=1;
			 $rollBackMsg="This location is not allocate to an active plant.";
		 }
		else if($editMode==1){
		$sql = "UPDATE `ware_fabricdispatchheader` SET intStatus ='$maxStatus', 
												       intApproveLevels ='$ApproveLevels', 
													  intCustLocation ='$custLocation', 
													  intOrderNo ='$orderNo', 
													  intOrderYear ='$orderYear', 
													  strremarks ='$remarks', 
													  dtmdate ='$dispatchDate', 
													  intModifiedBy ='$userId', 
													  dtmModifiedDate =now() 
				WHERE (`intBulkDispatchNo`='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
		$result = $db->RunQuery2($sql);
		} else {
			//$sql = "DELETE FROM `ware_fabricdispatchheader` WHERE (`intBulkDispatchNo`='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
			//$result1 = $db->RunQuery2($sql);

			$sql = "INSERT INTO `ware_fabricdispatchheader` (`intBulkDispatchNo`,`intBulkDispatchNoYear`,intCustLocation,intOrderNo,intOrderYear,strRemarks,intStatus,intApproveLevels,dtmdate,dtmCreateDate,intCteatedBy,intCompanyId) 
					VALUES ('$serialNo','$year','$custLocation','$orderNo','$orderYear','$remarks','$maxStatus','$ApproveLevels','$dispatchDate',now(),'$userId','$location')";
			$result = $db->RunQuery2($sql);
		}
		//-----------delete and insert to detail table-----------------------
		if(($result) && ($rollBackFlag!=1)){

			$salesOrderQuery = "SELECT trn_orderdetails.intSalesOrderId as SOId, trn_orderdetails.strSalesOrderNo as SOName, trn_orderdetails.intQty as SOQty FROM trn_orderdetails  
			WHERE trn_orderdetails.intOrderNo = '$orderNo' 
			AND trn_orderdetails.intOrderYear = '$orderYear'";
            $salesResult = $db->RunQuery2($salesOrderQuery);
            while($row=mysqli_fetch_array($salesResult))
            {

                $entry = array();
                $salesId = $row['SOId'] ;
                $entry['SOName'] = $row['SOName'] ;
                $entry['SOQty'] = $row['SOQty'] ;
                $entry['dispachedG'] = 0 ;
                $entry['pendingDispatchedQty'] = 0;
                $entry['balanceQty'] = $row['SOQty'];
                $salesOrderArray[$salesId] = $entry;
            }
            $sqlValidation = "SELECT trn_orderdetails.intSalesOrderId as SOId, SUM(ware_fabricdispatchdetails.dblGoodQty)  as dispachedG FROM ware_fabricdispatchheader
			INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo 
			AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear 
			INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = ware_fabricdispatchheader.intOrderNo AND trn_orderdetails.intOrderYear = ware_fabricdispatchheader.intOrderYear
			WHERE trn_orderdetails.intOrderNo = '$orderNo' AND trn_orderdetails.intOrderYear = '$orderYear' AND ware_fabricdispatchheader.intStatus =1 GROUP BY trn_orderdetails.intSalesOrderId";

            $validatingResult = $db->RunQuery2($sqlValidation);
            while($row=mysqli_fetch_array($validatingResult))
            {


                $salesId = $row['SOId'] ;
                $salesOrderArray[$salesId]['dispachedG'] = $row['dispachedG'];
                $salesOrderArray[$salesId]['balanceQty'] =$salesOrderArray[$salesId]['balanceQty']- $row['dispachedG'];
                if ($salesOrderArray[$salesId]['balanceQty'] < 0){
                    $salesOrderArray[$salesId]['balanceQty'] = 0;
				}
            }
			$sql = "DELETE FROM `ware_fabricdispatchdetails` WHERE (`intBulkDispatchNo`='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
			$result2 = $db->RunQuery2($sql);
			$saved=0;
			$toSave=0;
            $rollBackMsg="Maximum Qty for followings...";
            $gQtyValidationMsg = "";
			foreach($arr as $arrVal)
			{
				$cutNo = $arrVal['cutNo'];
				$salesOrderId 	 = $arrVal['salesOrderId'];
				$salesOrderNo	 = $arrVal['salesOrderNo'];
				$partId 	 = $arrVal['partId'];
				$part 	 = $arrVal['part'];
				$bgColorId 		 = $arrVal['bgColorId'];
				$bgColor 	 = $arrVal['bgColor'];
				$line 	 = $arrVal['line'];
				$size 		 = $arrVal['size'];
				$sqty 		 = round($arrVal['sqty'],4);
				$gqty 		 = round($arrVal['gqty'],4);
				$eqty 		 = round($arrVal['eqty'],4);
				$pqty 		 = round($arrVal['pqty'],4);
				$fqty 		 = round($arrVal['fqty'],4);
				$cutRetQty	 = round($arrVal['cutRetQty'],4);
				$remarks 		 = $arrVal['remarks'];
				$tot=val($sqty)+val($gsqty)+val($eqty)+val($pqty)+val($fqty)+val($cutRetQty);
                $salesOrderArray[$salesOrderId]['pendingDispatchedQty'] += val($gqty);
				//$place = 'Stores';
				//$type 	 = 'Received';
				$balQty=loadBalQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$partId,$size);
				$nonStkConfQty=loadNonStockConfQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$partId,$size);
				$orderQty=loadOrderQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade);
				$maxQty=$balQty-$nonStkConfQty;

				$soStatus		=loadSalesOrderStatus($orderNo,$orderYear,$salesOrderId);
				if($soStatus== -10){
					$rollBackFlag=1;
					$rollBackMsg ="Sales order is already closed";
				}
				//------check maximum FR Qty--------------------
				if($tot>$maxQty){
					$rollBackFlag=1;
					$rollBackMsg .="<br> ".$cutNo."-".$part."-".$salesOrderNo."-".$size." =".$maxQty;
				}

				//----------------------------
				if($rollBackFlag!=1){
					
					$sql = "INSERT INTO `ware_fabricdispatchdetails` (`intBulkDispatchNo`,`intBulkDispatchNoYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`intGroundColor`,`strLineNo`,`strSize`,`dblSampleQty`,`dblGoodQty`,`dblEmbroideryQty`,`dblPDammageQty`,`dblFdammageQty`,`dblCutRetQty`,`strRemarks`) 
					VALUES ('$serialNo','$year','$cutNo','$salesOrderId','$partId','$bgColorId','$line','$size','$sqty','$gqty','$eqty','$pqty','$fqty','$cutRetQty','$remarks')";
					$result = $db->RunQuery2($sql);
					if($result==1){
					$saved++;
					}
				}
				$toSave++;
		}
		}
		//echo $rollBackFlag;
		
		//update extra approval levels according to the pd% validation----------------------------------------
		/*$data_order	=get_order_details($orderNo,$orderYear);
		if(($data_order['customer_id'] == '28' || $data_order['customer_id'] == '105' ) && $data_order['DiffDate'] >='0' ){
			$extra_app_levels_1	=get_extra_app_level_1($orderNo,$orderYear,$serialNo,$year);
			$extra_app_levels_2	=get_extra_app_level_2($orderNo,$orderYear,$serialNo,$year);
			$extra_app_levels	= $extra_app_levels_1+$extra_app_levels_2;
			$sql = "UPDATE `ware_fabricdispatchheader` SET intStatus =intStatus+$extra_app_levels, 
														   intApproveLevels= intApproveLevels+$extra_app_levels
					WHERE (`intBulkDispatchNo`='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
			$result = $db->RunQuery2($sql);
		}*/
		$data_order		=get_order_details($orderNo,$orderYear);
		$flag_cut_ret	=check_recut_dispatch($serialNo,$year);
		if(($data_order['customer_id'] == '28' || $data_order['customer_id'] == '105' ) && $data_order['DiffDate'] >='0' && $flag_cut_ret==0){
			$extra_app_levels_1	=get_extra_app_level_1($orderNo,$orderYear,$serialNo,$year);
			$extra_app_levels_2	=get_extra_app_level_2($orderNo,$orderYear,$serialNo,$year);
			if($extra_app_levels_2==1)
				$extra_app_levels_1=1;
			$extra_app_levels	= $extra_app_levels_1+$extra_app_levels_2;
			$sql = "UPDATE `ware_fabricdispatchheader` SET intStatus =intStatus+$extra_app_levels, 
														   intApproveLevels= intApproveLevels+$extra_app_levels
					WHERE (`intBulkDispatchNo`='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
			$result = $db->RunQuery2($sql);
		}
		//----------------------------------------------------------------------------------------------------
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$confirmationMode=getConfirmationMode($programCode,$userId,$ApproveLevels,$maxStatus);
			if($extra_app_levels==1)
				$msg1	=" But this exceeds size wise cumulative pd%";
			else if ($extra_app_levels==2)
				$msg1	=" But this exceeds size wise and sales order wise cumulative pd%";
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$msgQtyValidation = '';
			$detectExceededGoodQty = false;
            foreach($salesOrderArray as $salesItem) {
                if ($salesItem['pendingDispatchedQty'] > $salesItem['balanceQty']){
                	if(!$detectExceededGoodQty){
                        $msgQtyValidation = " But Dispatched Good Qty is greater than Order Quatity for Sales Order ".$salesItem['SOName'];
					} else {
                        $msgQtyValidation.=", ".$salesItem['SOName'];
					}

                    $detectExceededGoodQty = true;
				}
            }

			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.'.$msg1.$msgQtyValidation;
			else
			$response['msg'] 		= 'Saved successfully.'.$msg1.$msgQtyValidation;
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
//----------------------------------------
	echo json_encode($response);
//----------------------------------------




//----------------------------------------
	function getNextSerialNo()
	{
		global $db;
		global $location;
		$sql = "SELECT
				sys_no.intBulkDispatchNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$location'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextNo = $row['intBulkDispatchNo'];
		
		$sql = "UPDATE `sys_no` SET intBulkDispatchNo=intBulkDispatchNo+1 WHERE (`intCompanyId`='$location')  ";
		$db->RunQuery2($sql);
		
		return $nextNo;
	}
	//-----------------------------------------------------------
//-----------------------------------------------------------
function loadOrderQty($location,$orderNo,$orderYear,$salesOrderNo,$size,$grade)
{
		global $db;
		  $sql = "SELECT
				trn_ordersizeqty.dblQty
				FROM trn_ordersizeqty
				WHERE
				trn_ordersizeqty.intOrderNo =  '$orderNo' AND
				trn_ordersizeqty.intOrderYear =  '$orderYear' AND
				trn_ordersizeqty.intSalesOrderId =  '$salesOrderNo' AND
				trn_ordersizeqty.strSize =  '$size'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
	//--------------------------------------------------------------
	function loadBalQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$intPart,$size){
		global $db;
		   $sql = "SELECT
					Sum(ware_stocktransactions_fabric.dblQty) AS qty
					FROM ware_stocktransactions_fabric
					WHERE
					ware_stocktransactions_fabric.intLocationId =  '$location' AND
					ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
					ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
					ware_stocktransactions_fabric.strCutNo =  '$cutNo' AND
					ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
					ware_stocktransactions_fabric.strSize =  '$size' AND
					ware_stocktransactions_fabric.intPart =  '$intPart'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['qty']);
	}
//-----------------------------------------------------------
function getConfirmationMode($programCode,$intUser,$approveLevels,$status){
		global $db;
		$k=$approveLevels+2-$status;
		$sqlp = "SELECT 
		menupermision.intEdit, 
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		if($rowp['int'.$k.'Approval']==1) 
		 $confirmatonMode=1; 
		else
		 $confirmatonMode=0; 

		return $confirmatonMode;
	}
	//--------------------------------------------------------------
	function loadNonStockConfQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$intPart,$size){
		global $db;
		   $sql = "SELECT
					sum(ware_fabricdispatchdetails.dblSampleQty+
					ware_fabricdispatchdetails.dblGoodQty+
					ware_fabricdispatchdetails.dblEmbroideryQty+
					ware_fabricdispatchdetails.dblPDammageQty+
					ware_fabricdispatchdetails.dblFdammageQty+
					ware_fabricdispatchdetails.dblCutRetQty) as qty
					FROM
					ware_fabricdispatchheader
					Inner Join ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear
					WHERE
					ware_fabricdispatchheader.intOrderNo =  '$orderNo' AND
					ware_fabricdispatchheader.intOrderYear =  '$orderYear' AND
					ware_fabricdispatchdetails.intSalesOrderId =  '$salesOrderId' AND
					ware_fabricdispatchdetails.strCutNo =  '$cutNo' AND
					ware_fabricdispatchdetails.strSize =  '$size' AND
					ware_fabricdispatchdetails.intPart =  '$intPart' AND
					ware_fabricdispatchheader.intStatus >  '1' AND
					ware_fabricdispatchheader.intApproveLevels >= ware_fabricdispatchheader.intStatus AND
					ware_fabricdispatchheader.intCompanyId =  '$location'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
	//	echo val($rows['qty']);
		return val($rows['qty']);
	}
//-----------------------------------------------------------
	function getSaveStatus($serialNo,$year)
	{
		global $db;
		$sql = "SELECT ware_fabricdispatchheader.intStatus, ware_fabricdispatchheader.intApproveLevels FROM ware_fabricdispatchheader WHERE (`intBulkDispatchNo`='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			
		return $editableFlag;
	}
//-----------------------------------------------------------
	function getLocationValidation($type,$location,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
	 	$sql = "SELECT
					ware_fabricdispatchheader.intCompanyId
					FROM
					ware_fabricdispatchheader
					Inner Join mst_locations ON ware_fabricdispatchheader.intCompanyId = mst_locations.intId
					WHERE
					ware_fabricdispatchheader.intBulkDispatchNo =  '$serialNo' AND
					ware_fabricdispatchheader.intBulkDispatchNoYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			if($location == $row['intCompanyId'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function load loadEditMode----
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}

function getPlantId($locationId)
{
	global $db;
	
	$sql 	= "SELECT intPlant FROM mst_locations WHERE intId = '$locationId' ";
	
	$result = $db->RunQuery2($sql);
	$row	= mysqli_fetch_array($result);
	
	return $row['intPlant'];
}

function getPlantStatus($plantId)
{
	global $db;
	
	$sql 	= "SELECT intStatus FROM mst_plant WHERE intPlantId = '$plantId' ";
	
	$result = $db->RunQuery2($sql);
	$row	= mysqli_fetch_array($result);
	
	return $row['intStatus'];
}
//--------------------------------------------------------
function loadSalesOrderStatus($orderNo,$orderYear,$salesOrderId){

	global $db;
	
	$sql 	= "SELECT
				trn_orderdetails.`STATUS`
				FROM `trn_orderdetails`
				WHERE
				trn_orderdetails.intOrderNo = '$orderNo' AND
				trn_orderdetails.intOrderYear = '$orderYear' AND
				trn_orderdetails.intSalesOrderId = '$salesOrderId'
				 ";
	
	$result = $db->RunQuery2($sql);
	$row	= mysqli_fetch_array($result);
	
	return $row['STATUS'];
	
}

function get_extra_app_level_1($serialno,$year,$dispNo,$dispYear){
	
	global $db;
	
	     $sql	="SELECT
			dh.intOrderNo,
			dh.intOrderYear,
			(select sum(od.intQty) from trn_orderdetails as od where od.intOrderNo=dh.intOrderNo and od.intOrderYear=dh.intOrderYear and od.intSalesOrderId=dd.intSalesOrderId) as orderQty,
			Sum(ifnull(dd.dblPDammageQty,0)) AS damage,
			Sum(ifnull(dd.dblGoodQty,0)+ifnull(dd.dblEmbroideryQty,0)
			+ifnull(dd.dblPDammageQty,0)+ifnull(dd.dblFdammageQty,0)
			+ifnull(dd.dblSampleQty,0)+ifnull(dd.dblCutRetQty,0)) AS dblDispQty,
			od.dblDamagePercentage
			FROM
			ware_fabricdispatchdetails AS dd 
			INNER JOIN ware_fabricdispatchdetails as dd1 on dd.intSalesOrderId = dd1.intSalesOrderId and dd.strSize=dd1.strSize and dd1.intBulkDispatchNo ='$dispNo' 
			and dd1.intBulkDispatchNoYear ='$dispYear'
			INNER JOIN ware_fabricdispatchheader AS dh ON dd.intBulkDispatchNo = dh.intBulkDispatchNo AND dd.intBulkDispatchNoYear = dh.intBulkDispatchNoYear
			INNER JOIN trn_orderdetails AS od ON dh.intOrderNo = od.intOrderNo AND dh.intOrderYear = od.intOrderYear AND dd.intSalesOrderId = od.intSalesOrderId
			WHERE
			dh.intOrderNo = '$serialno' AND
			dh.intOrderYear = '$year' and 
      		dh.intStatus > 0
			group by 
			dd.intSalesOrderId,
			dd.strSize";
	$result = $db->RunQuery2($sql);
	$extra_app_levels=0;
	while($row	= mysqli_fetch_array($result)){
		$pd_percentage	= $row['damage']/$row['dblDispQty']*100;
		if($pd_percentage > $row['dblDamagePercentage'])
			$extra_app_levels= 1;
	}
	
	return $extra_app_levels;
 	
}

function get_extra_app_level_2($serialno,$year,$dispNo,$dispYear){
	
	global $db;
	
	     $sql	="SELECT
			dh.intOrderNo,
			dh.intOrderYear,
			(select sum(od.intQty) from trn_orderdetails as od where od.intOrderNo=dh.intOrderNo and od.intOrderYear=dh.intOrderYear and od.intSalesOrderId=dd.intSalesOrderId) as orderQty,
			Sum(ifnull(dd.dblPDammageQty,0)) AS damage,
			Sum(ifnull(dd.dblGoodQty,0)+ifnull(dd.dblEmbroideryQty,0)
			+ifnull(dd.dblPDammageQty,0)+ifnull(dd.dblFdammageQty,0)
			+ifnull(dd.dblSampleQty,0)+ifnull(dd.dblCutRetQty,0)) AS dblDispQty,
			od.dblDamagePercentage
			FROM
			ware_fabricdispatchdetails AS dd 
			INNER JOIN (select * from ware_fabricdispatchdetails where ware_fabricdispatchdetails.intBulkDispatchNo ='$dispNo' 
			and ware_fabricdispatchdetails.intBulkDispatchNoYear ='$dispYear' group by ware_fabricdispatchdetails.intSalesOrderId) as dd1 on dd.intSalesOrderId = dd1.intSalesOrderId 
			INNER JOIN ware_fabricdispatchheader AS dh ON dd.intBulkDispatchNo = dh.intBulkDispatchNo AND dd.intBulkDispatchNoYear = dh.intBulkDispatchNoYear
			INNER JOIN trn_orderdetails AS od ON dh.intOrderNo = od.intOrderNo AND dh.intOrderYear = od.intOrderYear AND dd.intSalesOrderId = od.intSalesOrderId
			WHERE
			dh.intOrderNo = '$serialno' AND
			dh.intOrderYear = '$year' and 
      		dh.intStatus > 0 
			group by 
			dd.intSalesOrderId";
	$result = $db->RunQuery2($sql);
	$extra_app_levels=0;
	while($row	= mysqli_fetch_array($result)){
		$pd_percentage	= $row['damage']/$row['dblDispQty']*100;
		if($pd_percentage > $row['dblDamagePercentage'])
			$extra_app_levels= 1;
	}
	
	return $extra_app_levels;
 	
}

function get_order_details($orderNo,$orderYear){
	global $db;
	$sql		= "SELECT
	trn_orderheader.intCustomer AS customer_id,
	trn_orderheader.intMarketer AS marketer_id,
	date(trn_orderheader.dtmCreateDate) AS order_created_on,
	DATEDIFF(date(trn_orderheader.dtmCreateDate),date('2017-05-18')) AS DiffDate  
	FROM `trn_orderheader`
	WHERE
	trn_orderheader.intOrderNo = '$orderNo' AND
	trn_orderheader.intOrderYear = '$orderYear'
	";
	$result 	= $db->RunQuery2($sql);

	return $row	= mysqli_fetch_array($result);
	
}

function check_recut_dispatch($serialno,$year){
	
	global $db;
	
	$sql	= "SELECT sum(ifnull(ware_fabricdispatchdetails.dblSampleQty,0)+
				ifnull(ware_fabricdispatchdetails.dblGoodQty,0)+
				ifnull(ware_fabricdispatchdetails.dblEmbroideryQty,0)+
				ifnull(ware_fabricdispatchdetails.dblPDammageQty,0)+
				ifnull(ware_fabricdispatchdetails.dblFdammageQty,0)) as qty
				FROM `ware_fabricdispatchdetails`
				WHERE
				ware_fabricdispatchdetails.intBulkDispatchNo = '$serialno' AND
				ware_fabricdispatchdetails.intBulkDispatchNoYear = '$year'";
	$result = $db->RunQuery2($sql);
	$row	= mysqli_fetch_array($result);
	
	if($row['qty']>0)//not a cut return
		return 0;
	else
		return 1;
	
}
// save bar code wise data in temp table
function save_data_temporary($tmp_dispatch,$serialNo,$Year,$cut_no,$sales_no,$size,$bcode,$part,$bgColor,$line,$samp_qty,$good_qty,$emb_qty,$pd_qty,$fd_qty,$cut, $remark)
{  
		global $db;
		
		$sql_select = "select * 
						from
						temp_dispatch_barcode_wise
						where
						temp_dispatch_barcode_wise.ID = $tmp_dispatch
						AND temp_dispatch_barcode_wise.BARCODE = TRIM('$bcode')
						AND temp_dispatch_barcode_wise.strCutNo ='$cut_no'
						AND temp_dispatch_barcode_wise.intSalesOrderId = $sales_no
						AND temp_dispatch_barcode_wise.strSize = '$size' ";
						
						$result_sel = $db->RunQuery2($sql_select);
						$row_sel	= mysqli_fetch_array($result_sel);
						$id  		= $row_sel['ID'];
						
		if($id=="")
		{
		$sql="INSERT INTO temp_dispatch_barcode_wise
						(ID,
						BARCODE,
						strCutNo,
						intSalesOrderId,
						intPart,
						intGroundColor,
						strLineNo,
						strSize,
						dblSampleQty,
						dblGoodQty,
						dblEmbroideryQty,
						dblPDammageQty,
						dblFdammageQty,
						dblCutRetQty,
						strRemarks,
						invoiced,
						strReferenceNo
						)                       
						VALUES
						($tmp_dispatch,
						TRIM('$bcode'),
						'$cut_no',
						$sales_no,
						$part  ,
						$bgColor,
						$line,
						'$size',
						$samp_qty,
						$good_qty,
						$emb_qty,
						$pd_qty,
						$fd_qty,
						$cut,
						'$remark',
						1,
						'null'
						)"; 
						
				$result = $db->RunQuery2($sql);
				return $result;
		}
		else
		
		{ $sql_update = "
		UPDATE temp_dispatch_barcode_wise
		SET 
		ID = $tmp_dispatch,
		BARCODE = TRIM('$bcode'),
		strCutNo = '$cut_no',
		intSalesOrderId = $sales_no ,
		intPart= $part,
		intGroundColor= $bgColor,
		strLineNo= $line,
		strSize= '$size',
		dblSampleQty= $samp_qty,
		dblGoodQty= $good_qty,
		dblEmbroideryQty= $emb_qty,
		dblPDammageQty= $pd_qty,
		dblFdammageQty= $fd_qty,
		dblCutRetQty= $cut,
		strRemarks= '$remark',
		invoiced= 1,
		strReferenceNo= 'null'
		WHERE 
		temp_dispatch_barcode_wise.ID = $tmp_dispatch
		AND temp_dispatch_barcode_wise.BARCODE = '$bcode'
		AND temp_dispatch_barcode_wise.strCutNo ='$cut_no'
		AND temp_dispatch_barcode_wise.intSalesOrderId = $sales_no
		AND temp_dispatch_barcode_wise.strSize = '$size' ";
		
		$result = $db->RunQuery2($sql_update);
		return $result;
		}
}

function get_data_from_temp($tmpdisNo)
{   
	global $db;
	
	$sql_select 	= "select * from temp_dispatch_barcode_wise where temp_dispatch_barcode_wise.ID = '$tmpdisNo'";
	
	$result_select 	= $db->RunQuery2($sql_select);
	return $result_select;
	
}

function  save_barcode_wise_data($serialNo,$Year,$tmpdisNo,$row_select)//perment tbl
{   
			global $db;
		
			$barcode 			= $row_select['BARCODE'];
			$strCutNo 			= $row_select['strCutNo'];
			$intSalesOrderId 	= $row_select['intSalesOrderId'];
			$intPart 			= $row_select['intPart'];
			$intGroundColor 	= $row_select['intGroundColor'];
			$strLineNo 			= $row_select['strLineNo'];
			$strSize 			= $row_select['strSize'];
			$dblSampleQty 		= $row_select['dblSampleQty'];
			$dblGoodQty 		= $row_select['dblGoodQty'];
			$dblEmbroideryQty	= $row_select['dblEmbroideryQty'];
			$dblPDammageQty 	= $row_select['dblPDammageQty'];
			$dblFdammageQty 	= $row_select['dblFdammageQty'];
			$dblCutRetQty 		= $row_select['dblCutRetQty'];
			$strRemarks 		= $row_select['strRemarks'];
			$invoiced 			= $row_select['invoiced'];
			$strReferenceNo 	= $row_select['strReferenceNo'];
			
			$sql_select = "select * 
			FROM
			ware_fabricdispatchdetails_barcode_wise
			WHERE
			ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNo = $serialNo
			AND ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNoYear = $Year
			AND ware_fabricdispatchdetails_barcode_wise.BARCODE = '$barcode'
			AND ware_fabricdispatchdetails_barcode_wise.strCutNo = '$strCutNo'
			AND ware_fabricdispatchdetails_barcode_wise.intSalesOrderId = $intSalesOrderId
			AND ware_fabricdispatchdetails_barcode_wise.strSize = '$strSize'";
			
			$result_sel = $db->RunQuery2($sql_select);
			$row_sel	= mysqli_fetch_array($result_sel);
			$barcode_sel = $row_sel['BARCODE'];
			
		if($barcode_sel=="")
			{
					$sql_barcode = "INSERT INTO ware_fabricdispatchdetails_barcode_wise
									(intBulkDispatchNo,
									intBulkDispatchNoYear,
									BARCODE,
									strCutNo,
									intSalesOrderId,
									intPart,
									intGroundColor,
									strLineNo,
									strSize,
									dblSampleQty,
									dblGoodQty,
									dblEmbroideryQty,
									dblPDammageQty,
									dblFdammageQty,
									dblCutRetQty,
									strRemarks,
									invoiced,
									strReferenceNo,
									tempId
									)
									VALUES
									($serialNo,
									$Year,
									TRIM('$barcode') ,
									'$strCutNo',
									$intSalesOrderId,
									$intPart,
									$intGroundColor,
									$strLineNo,
									'$strSize ',
									$dblSampleQty,
									$dblGoodQty,
									$dblEmbroideryQty,
									$dblPDammageQty,
									$dblFdammageQty,
									$dblCutRetQty,
									'$strRemarks',
									$invoiced,
									'$strReferenceNo',
									$tmpdisNo
									)"; 
			
						 $db->RunQuery2($sql_barcode);
			}
			else
			{  
							$sql_update = "
					UPDATE ware_fabricdispatchdetails_barcode_wise
					SET 
					intBulkDispatchNo	= $serialNo,
					intBulkDispatchNoYear  = $Year,
					BARCODE 	= TRIM('$barcode'),
					strCutNo 	= '$strCutNo',
					intSalesOrderId = $intSalesOrderId ,
					intPart			= $intPart,
					intGroundColor	= $intGroundColor,
					strLineNo		= $strLineNo,
					strSize			= '$strSize',
					dblSampleQty	= $dblSampleQty,
					dblGoodQty		= $dblGoodQty,
					dblEmbroideryQty	=$dblEmbroideryQty,
					dblPDammageQty		= $dblPDammageQty,
					dblFdammageQty		= $dblFdammageQty,
					dblCutRetQty		= $dblCutRetQty,
					strRemarks			= '$strRemarks',
					invoiced			= $invoiced,
					strReferenceNo		= '$strReferenceNo'
					WHERE 
					ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNo = $serialNo
					AND ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNoYear = $Year
					AND ware_fabricdispatchdetails_barcode_wise.BARCODE = '$barcode'
					AND ware_fabricdispatchdetails_barcode_wise.strCutNo = '$strCutNo'
					AND ware_fabricdispatchdetails_barcode_wise.intSalesOrderId = $intSalesOrderId
					AND ware_fabricdispatchdetails_barcode_wise.strSize = '$strSize' ";
				
					$result = $db->RunQuery2($sql_update);
			}
}

function get_quantities_from_temp($tmpdisNo,$sales_no,$cut_no,$size)
{
		global $db;
	
			$sql = "SELECT
		SUM(temp_dispatch_barcode_wise.dblSampleQty) as sumQtyS,
		SUM(temp_dispatch_barcode_wise.dblGoodQty) as sumQtyG,
		SUM(temp_dispatch_barcode_wise.dblEmbroideryQty)as sumQtyEM,
		SUM(temp_dispatch_barcode_wise.dblCutRetQty) as sumQtycutR,
		SUM(temp_dispatch_barcode_wise.dblFdammageQty) as sumQtyFD,
		SUM(temp_dispatch_barcode_wise.dblPDammageQty) as sumQtyPD
		FROM
		temp_dispatch_barcode_wise
		WHERE
		temp_dispatch_barcode_wise.ID = $tmpdisNo
		AND temp_dispatch_barcode_wise.intSalesOrderId = $sales_no
		AND temp_dispatch_barcode_wise.strCutNo = '$cut_no'
		AND temp_dispatch_barcode_wise.strSize = '$size'";
		//echo $sql;
		 $result = $db->RunQuery2($sql);
		 return $result;
		 
}

function delete_bar_code_data($serialNo,$Year,$tmpdisNo,$cut_no,$sales_no,$size,$bcode) //delete from barcode interface which saved in tmp tbl
{
		global $db;
		$sql_del_tmp = "DELETE 
						FROM temp_dispatch_barcode_wise
						WHERE 
						temp_dispatch_barcode_wise.ID = $tmpdisNo
						AND  temp_dispatch_barcode_wise.BARCODE = '$bcode'
						AND temp_dispatch_barcode_wise.intSalesOrderId = $sales_no
						AND temp_dispatch_barcode_wise.strCutNo = '$cut_no'
						AND temp_dispatch_barcode_wise.strSize = '$size'";
					
						 $result = $db->RunQuery2($sql_del_tmp);
			if($result)
			{			 
				delete_from_main_tbl($serialNo,$Year,$tmpdisNo,$cut_no,$sales_no,$size,$bcode);	
			}
}

function delete_from_main_tbl($serialNo,$Year,$tmpdisNo,$cut_no,$sales_no,$size,$bcode) //delete from barcode interface which saved in main tbl
{			
			global $db;
			$sql_select = "select 
			* 
			FROM
			ware_fabricdispatchdetails_barcode_wise
			WHERE
			ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNo = $serialNo
			AND ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNoYear = $Year
			AND ware_fabricdispatchdetails_barcode_wise.intSalesOrderId = $sales_no
			AND ware_fabricdispatchdetails_barcode_wise.strCutNo = '$cut_no'
			AND ware_fabricdispatchdetails_barcode_wise.strSize = '$size' 
			AND ware_fabricdispatchdetails_barcode_wise.BARCODE = '$bcode' ";
			
			$result_select 	= $db->RunQuery2($sql_select);
			$row_select		= mysqli_fetch_array($result_select);
			
			if($row_select['BARCODE'] !="")
			{
				$sql_delete = "DELETE FROM 
				ware_fabricdispatchdetails_barcode_wise
				WHERE
				
				ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNo = $serialNo
				AND ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNoYear = $Year
				AND ware_fabricdispatchdetails_barcode_wise.BARCODE = '$bcode'
				AND ware_fabricdispatchdetails_barcode_wise.strCutNo = '$cut_no'
				AND ware_fabricdispatchdetails_barcode_wise.intSalesOrderId = $sales_no
				AND ware_fabricdispatchdetails_barcode_wise.strSize = '$size'";
				
				$result_delete 	= $db->RunQuery2($sql_delete);
			}
}

function delete_bar_code_data_main($serialNo,$Year,$tmpdisNo,$cut_no,$sales_no,$size) //delete from main interface 
{	
	global $db;
	if($serialNo=="" && $Year=="") //not saves in main barcode tbl
	{
		 $sql_delete = "DELETE 
						FROM temp_dispatch_barcode_wise
						WHERE 
						temp_dispatch_barcode_wise.ID = $tmpdisNo
						AND temp_dispatch_barcode_wise.intSalesOrderId = $sales_no
						AND temp_dispatch_barcode_wise.strCutNo = '$cut_no'
						AND temp_dispatch_barcode_wise.strSize = '$size'";
						
						 $db->RunQuery2($sql_delete);
	}
	else //saved in main barcode table
	{ 
	 	$sql_delete = "DELETE 
						FROM temp_dispatch_barcode_wise
						WHERE 
						temp_dispatch_barcode_wise.ID = $tmpdisNo
						AND temp_dispatch_barcode_wise.intSalesOrderId = $sales_no
						AND temp_dispatch_barcode_wise.strCutNo = '$cut_no'
						AND temp_dispatch_barcode_wise.strSize = '$size'";
						
						 $db->RunQuery2($sql_delete);
						 
		$sql_delete_main = "DELETE 
							FROM 
							ware_fabricdispatchdetails_barcode_wise
							WHERE
							
							ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNo = $serialNo
							AND ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNoYear = $Year
							AND ware_fabricdispatchdetails_barcode_wise.strCutNo = '$cut_no'
							AND ware_fabricdispatchdetails_barcode_wise.intSalesOrderId = $sales_no
							AND ware_fabricdispatchdetails_barcode_wise.strSize = '$size'";
				
						$result_delete 	= $db->RunQuery2($sql_delete_main);
	}
}

?>

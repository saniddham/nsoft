<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$location 			= $sessions->getLocationId();
$company 			= $sessions->getCompanyId();
$intUser  			= $sessions->getUserId();

require_once "class/cls_commonFunctions_get.php";				$objcomfunc 					= new cls_commonFunctions_get($db);
include_once "class/cls_commonErrorHandeling_get.php";			$obj_commonErr					= new cls_commonErrorHandeling_get($db);
include_once "class/tables/ware_fabricdispatchheader.php";		$obj_ware_fabricdispatchheader	= new ware_fabricdispatchheader($db);
include_once "class/tables/mst_locations.php";					$mst_locations					= new mst_locations($db);
include_once "class/tables/mst_plant.php";						$mst_plant						= new mst_plant($db);

$locationType 		= $objcomfunc->getLocationType($location);

$programName		= 'Dispatch Note';
$programCode		= 'P0470';
$STICKER_MAKING      = '-STICKER_MAKING';
$serialNo 			= (!isset($_REQUEST['serialNo'])?'':$_REQUEST['serialNo']);
$year 				= (!isset($_REQUEST['year'])?'':$_REQUEST['year']);
$x_note				= '';

if($serialNo=='')
{
	$orderNo 		= (!isset($_REQUEST['orderNo'])?'':$_REQUEST['orderNo']);
	$orderYear 		= (!isset($_REQUEST['orderYear'])?'':$_REQUEST['orderYear']);
	$salesOrderNo 	= (!isset($_REQUEST['salesOrderNo'])?'':$_REQUEST['salesOrderNo']);
}
else{
	$sql = "SELECT
	ware_fabricdispatchheader.intOrderNo,
	ware_fabricdispatchheader.intOrderYear,
	ware_fabricdispatchheader.strRemarks,
	ware_fabricdispatchheader.intStatus,
	ware_fabricdispatchheader.intApproveLevels,
	ware_fabricdispatchheader.dtmdate, 
	ware_fabricdispatchheader.intCustLocation, 
	ware_fabricdispatchheader.intStatus , 
	ware_fabricdispatchheader.intApproveLevels  
	FROM ware_fabricdispatchheader
	WHERE
	ware_fabricdispatchheader.intBulkDispatchNo =  '$serialNo' AND
	ware_fabricdispatchheader.intBulkDispatchNoYear =  '$year'";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	$orderNo 			= $row['intOrderNo'];
	$orderYear 			= $row['intOrderYear'];
	$x_note 			= $row['strRemarks'];
	$AODDate			= $row['dtmdate'];
	$x_customerLocation	= $row['intCustLocation'];
	$status 			= $row['intStatus'];
	$approveLevels 		= $row['intApproveLevels'];
	$savedStat			= $row['intApproveLevels'];
	$intStatus			= $row['intStatus'];
}

if($orderYear==''){
	//$orderYear=date('Y');
	$sql 			= "SELECT DISTINCT
					trn_orderheader.intOrderYear
					FROM trn_orderheader
					ORDER BY
					trn_orderheader.intOrderYear DESC limit 1";
	$result 	= $db->RunQuery($sql);
	$row		=mysqli_fetch_array($result);
	$orderYear 	= $row['intOrderYear'];

}

$poNo 					= (!isset($_REQUEST['poNo'])?'':$_REQUEST['poNo']);
$customer 				= (!isset($_REQUEST['customer'])?'':$_REQUEST['customer']);

   $sql = "SELECT
trn_orderheader.strCustomerPoNo,
trn_orderheader.intCustomer
FROM trn_orderheader
WHERE
trn_orderheader.intOrderNo =  '$orderNo' AND trn_orderheader.intOrderNo >0   AND 
trn_orderheader.intOrderYear =  '$orderYear'"; 
$result = $db->RunQuery($sql);
$row = mysqli_fetch_array($result);

$x_poNo 	= $row['strCustomerPoNo'];
$x_custId 	= $row['intCustomer'];

if($x_poNo=='')
	$x_poNo = $poNo;
//----------------			
$sql = "SELECT
Sum(trn_orderdetails.intQty) AS qty, 
trn_orderdetails.intSampleNo,
trn_orderdetails.intSampleYear,
trn_orderdetails.strCombo,
trn_orderdetails.strPrintName,
trn_orderdetails.intRevisionNo , 
trn_orderdetails.strStyleNo , 
trn_orderdetails.strGraphicNo 
FROM trn_orderdetails  
WHERE
trn_orderdetails.intOrderNo='$orderNo' AND trn_orderdetails.intOrderNo >0 
AND trn_orderdetails.intOrderYear='$orderYear'  "; 

$sql .= " GROUP BY
trn_orderdetails.intOrderNo,
trn_orderdetails.intOrderYear"; 

$sql='';
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$x_styleNo 		= $row['strStyleNo'];
$x_graphicNo 	= $row['strGraphicNo'];
$x_sampleNo 	= $row['intSampleNo'];
$x_sampleYear 	= $row['intSampleYear'];
$x_revNo 		= $row['intRevisionNo'];
$x_combo 		= $row['strCombo'];
$x_print 		= $row['strPrintName'];

$sql = "SELECT
Sum(trn_orderdetails.intQty) AS qty  
FROM trn_orderdetails  
WHERE
trn_orderdetails.intOrderNo='$orderNo'  AND trn_orderdetails.intOrderNo >0 
AND trn_orderdetails.intOrderYear='$orderYear' AND trn_orderdetails.SO_TYPE > -1 
"; 

/*if($salesOrderNo!=''){
$sql .= " AND 
trn_orderdetails.strSalesOrderNo='$salesOrderNo'"; 
}
*/$sql .= " GROUP BY
trn_orderdetails.intOrderNo,
trn_orderdetails.intOrderYear"; 
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);

$x_qty = $row['qty'];
//-------------------------	
$x_styleNo 		= (!isset($_REQUEST['styleNo'])?'':$_REQUEST['styleNo']);
$x_graphicNo 	= (!isset($_REQUEST['graphicNo'])?'':$_REQUEST['graphicNo']);
		
/*if(($serialNo=='')&&($year=='')){
	$savedStat = (int)getApproveLevel($programName);
	$intStatus=$savedStat+1;
}

$editMode=loadEditMode($programCode,$intStatus,$savedStat,$intUser);
$confirmatonMode=loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser);*/

$status			= '';
$apprveLevels	= '';

if(($serialNo!='')&&($year!='')){
	
	//$savedStat = (int)getApproveLevel($programName);
	//$intStatus=$savedStat+1;
	$obj_ware_fabricdispatchheader->set($serialNo,$year);
	$status					= $obj_ware_fabricdispatchheader->getintStatus($serialNo,$year);
	$apprveLevels			= $obj_ware_fabricdispatchheader->getintApproveLevels($serialNo,$year);
}

//$editMode=loadEditMode($programCode,$intStatus,$savedStat,$intUser);
//$confirmatonMode=loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser);

$permition_arr			= $obj_commonErr->get_permision_withApproval_save($status,$apprveLevels,$intUser,$programCode,'RunQuery');
$permision_save			= $permition_arr['permision'];

$permition_arr			= $obj_commonErr->get_permision_withApproval_confirm($status,$apprveLevels,$intUser,$programCode,'RunQuery');
$permision_confirm		= $permition_arr['permision'];

//--------------------////suvini
if($serialNo!="" && $year!="")
{
	$sql_disp_tmp =" 
					SELECT
					ware_fabricdispatchdetails_barcode_wise.tempId
					FROM
					ware_fabricdispatchdetails_barcode_wise
					WHERE
					ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNo = $serialNo
					AND
					ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNoYear = $year ";
					
					$result 		= 	$db->RunQuery($sql_disp_tmp);
					$row			=	mysqli_fetch_array($result);
					$tmp_disp_no 	= 	$row['tempId'];
}	
?>
<head>
<title>Fabric Dispatch Note</title>
</head>

<body>

  <?php
  $mst_locations->set($location); 
  $plantId		= $mst_locations->getintPlant();
  
  if($plantId!='')
  {
  	$mst_plant->set($plantId);
  	$plantStatus	= $mst_plant->getintStatus(); 
  }
 
  if($locationType < 1)
  { 
	$str =  "This is not a Production location.So can't Dispatch Fabrics.";
	$maskClass="maskShow";
  ?>
    <div id="divMask" class="<?php echo $maskClass?> mask"> <?php echo $str; ?></div>
 <?php 	return false;
  }
  else if($plantId=='')
  {
	$str =  "This location is not allocate to a plant.";
	$maskClass="maskShow";
  ?>
    <div id="divMask" class="<?php echo $maskClass?> mask"> <?php echo $str; ?></div>
 <?php 	return false;
  }
  else if($plantStatus!=1)
  {
	$str =  "This location is not allocate to an active plant.";
	$maskClass="maskShow";
  ?>
    <div id="divMask" class="<?php echo $maskClass?> mask"> <?php echo $str; ?></div>
 <?php 	return false;
  }
  ?>  
<style type="text/css">
.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<form id="frmFabricDispatchNote" name="frmFabricDispatchNote" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center" style="width:1100px">
		<div class="trans_layoutL" style="width:1100px">
		  <div class="trans_text" style="width:1100px">FABRIC DISPATCH NOTE</div>
		  <table width="1100" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
    <tr><td><table width="100%">
      <tr>
        <td><fieldset class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td width="8%" class="normalfnt">Serial No</td>
  <td width="21%" class="normalfnt"><input  id="txtSerialNo" class="normalfnt" style="width:70px;text-align:right" type="text" value="<?php echo $serialNo ?>" readonly/>
  <input  id="txtYear" class="normalfnt" style="width:40px;text-align:right" type="text" value="<?php echo $year ?>" readonly/></td>
  <td  style="display:none;"> temp_dispatch_no</td>
  <td width="21%" class="normalfnt" style="display:none;"><input  id="tmpdisNo" class="normalfnt" style="width:70px;text-align:right" type="text" value="<?php echo $tmp_disp_no ?>" readonly/>
  
  <td width="1%" class="normalfnt">&nbsp;</td>
  <td width="18%" class="normalfnt">&nbsp;</td>
  <td width="8%" class="normalfnt">&nbsp;</td>
  <td width="21%" class="normalfnt">&nbsp;</td>
  <td width="1%" class="normalfnt">&nbsp;</td>
  <td width="22%" align="right" class="normalfnt">&nbsp;<input  id="locationType"  type="hidden" value="<?php echo $locationType ?>"/></td>
</tr>        </table></fieldset></td>
      </tr>
    </table></td></tr>
    <tr><td><table width="100%">
      <tr>
        <td><table  bgcolor="#E8FED8" class="tableBorder_allRound"  width="100%" border="0" cellpadding="0" cellspacing="0"  >
            <tr height="21">
              <td width="11%" class="normalfnt">&nbsp;Year</td>
              <td width="23%" class="normalfnt"><select name="cboOrderYear" id="cboOrderYear" style="width:70px" <?php /*?>class="search"<?php */?>>
                <?php
					$sql = "SELECT DISTINCT
							trn_orderheader.intOrderYear
							FROM trn_orderheader
							ORDER BY
							trn_orderheader.intOrderYear DESC";
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						
						if(($i==0) &&($orderYear==''))
						$orderYear = $row['intOrderYear'];
						
						if($row['intOrderYear']==$orderYear)
						echo "<option value=\"".$row['intOrderYear']."\" selected=\"selected\">".$row['intOrderYear']."</option>";	
						else
						echo "<option value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";	
						$i++;
					}
				?>
                </select></td>
              <td width="11%" class="normalfnt">Style No</td>
              <td width="23%" class="normalfnt"><select name="cboStyle" id="cboStyle" style="width:130px" class="search">
                <?php
                $html1="<option value=\"\"></option>";
				$html='';
					$sql = "SELECT DISTINCT
							trn_orderdetails.strStyleNo
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
							Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
							WHERE
							trn_orderheader.intStatus =  '1' AND trn_orderheader.PO_TYPE IN (2,0)   AND 
							trn_orderdetails.intOrderYear='$orderYear'  AND mst_locations.intCompanyId =  '$company'  ";
					if($orderNo>0){		
					$sql .= " AND trn_orderdetails.intOrderNo='$orderNo' ";	
					}
					$sql .= " 
							
							ORDER BY trn_orderdetails.strStyleNo ASC";
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						$i++;
						if($row['strStyleNo']==$x_styleNo)
						$html .= "<option value=\"".$row['strStyleNo']."\" selected=\"selected\">".$row['strStyleNo']."</option>";	
						else
						$html .= "<option value=\"".$row['strStyleNo']."\">".$row['strStyleNo']."</option>";	
						if(($i==1) && ($x_styleNo=='')){
							$x_styleNoT=$row['strStyleNo'];
						}
					}
					
					if(($orderNo!='') && ($x_styleNo=='')){
					$x_styleNo=$x_styleNoT;
					}
					
					if($orderNo==''){
						$html=$html1.$html;
					}
					else if($i>=1){
						$html=$html.$html1;
					}
					else if($i==0){
						$html=$html1;
					}
					echo $html;
				?></select></td>
              <td width="8%" class="normalfnt">Graphic No</td>
              <td width="24%" align="right" class="normalfnt"><select name="cboGraphicNo" id="cboGraphicNo" style="width:120px" class="search">
                <?php
                $html1="<option value=\"\"></option>";
				$html='';
					$sql = "SELECT DISTINCT
							trn_orderdetails.strGraphicNo
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
							Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
							WHERE
							trn_orderheader.intStatus =  '1' AND trn_orderheader.PO_TYPE IN (2,0)   AND 
							trn_orderdetails.intOrderYear='$orderYear' AND mst_locations.intCompanyId =  '$company'  ";
					if($orderNo>0){		
					$sql .= " AND trn_orderdetails.intOrderNo='$orderNo' ";	
					}
					if($x_styleNo!=''){		
					$sql .= " AND trn_orderdetails.strStyleNo='$x_styleNo' ";	
					}
					
					$sql .= " 
							
							ORDER BY trn_orderdetails.strGraphicNo ASC";
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						$i++;
						if($row['strGraphicNo']==$x_graphicNo)
						$html .= "<option value=\"".$row['strGraphicNo']."\" selected=\"selected\">".$row['strGraphicNo']."</option>";	
						else
						$html .= "<option value=\"".$row['strGraphicNo']."\">".$row['strGraphicNo']."</option>";	
						if(($i==1) && ($x_graphicNo=='')){
							$x_graphicNoT=$row['strGraphicNo'];
						}
					}
					
					if(($orderNo!='') && ($x_graphicNo=='')){
					$x_graphicNo=$x_graphicNoT;
					}
					
					if($orderNo==''){
						$html=$html1.$html;
					}
					else if($i>=1){
						$html=$html.$html1;
					}
					else if($i==0){
						$html=$html1;
					}
					echo $html;
				?></select></td> </tr>
<tr height="21">
              <td class="normalfnt">Customer PO</td>
              <td class="normalfnt"><select name="cboPONo" id="cboPONo" style="width:130px" class="search">
                <option value=""></option>
                <?php
					$sql = "SELECT DISTINCT
							trn_orderheader.strCustomerPoNo
							FROM trn_orderheader Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
							where trn_orderheader.intStatus=1  AND trn_orderheader.PO_TYPE IN (2,0) 
							AND trn_orderheader.strCustomerPoNo <> ''   AND 
							trn_orderheader.intOrderYear='$orderYear' AND mst_locations.intCompanyId =  '$company'
							ORDER BY
							trn_orderheader.strCustomerPoNo ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if(($row['strCustomerPoNo']==$x_poNo) && ($orderNo!=''))
						echo "<option value=\"".$row['strCustomerPoNo']."\" selected=\"selected\">".$row['strCustomerPoNo']."</option>";	
						else
						echo "<option value=\"".$row['strCustomerPoNo']."\">".$row['strCustomerPoNo']."</option>";	
					}
				?>
                </select></td>
              <td class="normalfnt">Customer</td>
              <td colspan="4" class="normalfnt"><select style="width:295px" name="cboCustomer" id="cboCustomer"   class="validate[required] search" >
                <option value=""></option>
                <?php
					$sql = "SELECT DISTINCT 
							trn_orderheader.intCustomer,
							mst_customer.strName
							FROM
							trn_orderheader
							Inner Join mst_customer ON trn_orderheader.intCustomer = mst_customer.intId  
							where  
							trn_orderheader.intOrderYear='$orderYear' 
							ORDER BY mst_customer.strName ASC";
							$pp=$sql;
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intCustomer']==$x_custId)
						echo "<option value=\"".$row['intCustomer']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intCustomer']."\">".$row['strName']."</option>";	
					}
				?>
                </select></td>
            </tr>  
<tr>
              <td height="22" class="normalfnt">Order No</td>
              <td class="normalfnt"><select name="cboOrderNo" id="cboOrderNo" style="width:130px" >
                <option value=""></option>
                <?php
					$sql = "SELECT DISTINCT
							trn_orderheader.intOrderNo, 
							trn_orderheader.intOrderYear 
							FROM trn_orderheader  
							Inner Join trn_orderdetails ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear  
							Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
							where trn_orderheader.intStatus = 1 AND trn_orderheader.PO_TYPE IN (2,0) and trn_orderheader.TENTATIVE < 2
							AND mst_locations.intCompanyId =  '$company' ";
					if($orderYear!='')
					$sql.=" AND trn_orderheader.intOrderYear 	=  '$orderYear'  ";
					if($orderNo!=''){
					if($x_graphicNo!='')
					$sql.=" AND trn_orderdetails.strGraphicNo =  '$x_graphicNo'  ";
					if($x_styleNo!='')
					$sql.=" AND trn_orderdetails.strStyleNo 		=  '$x_styleNo'  ";
					if($x_poNo!='')
					//$sql.=" AND trn_orderheader.strCustomerPoNo =  '$x_poNo' ";
					if($salesOrderNo!=''){		
					//$sql .= " AND trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'";		
					}
					}
					$sql .= "ORDER BY trn_orderheader.intOrderYear DESC, trn_orderheader.intOrderNo DESC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intOrderNo']==$orderNo)
						echo "<option value=\"".$row['intOrderNo']."\" selected=\"selected\">".$row['intOrderNo']."</option>";	
						else
						echo "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";	
					}
				?>
              </select></td>
              <td class="normalfnt">Sales Order No</td>
              <td colspan="4" class="normalfnt"><select name="cboSalesOrderNo" id="cboSalesOrderNo" style="width:130px" >
                <option value=""></option>
                <?php
				 	$sql = "SELECT DISTINCT 
							trn_orderdetails.strSalesOrderNo,
							trn_orderdetails.SO_TYPE
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear  
							Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
							WHERE trn_orderheader.intStatus = 1 AND trn_orderheader.PO_TYPE IN (2,0) 
							AND mst_locations.intCompanyId =  '$company'  ";
							 
					if($orderYear!='')
					$sql.=" AND trn_orderheader.intOrderYear 	=  '$orderYear'  ";
					if($x_graphicNo!='')
					$sql.=" AND trn_orderdetails.strGraphicNo =  '$x_graphicNo'  ";
					if($x_styleNo!='')
					$sql.=" AND trn_orderdetails.strStyleNo 		=  '$x_styleNo'  ";
                    if($locationType==2)
                    $sql.=" AND trn_orderdetails.SO_TYPE > -1 ";
					if($x_poNo!='')
					//$sql.=" AND trn_orderheader.strCustomerPoNo =  '$x_poNo' ";
					if($orderNo!=''){		
					$sql .= " AND trn_orderheader.intOrderNo =  '$orderNo'";		
					}
					$sql .= "
							ORDER BY trn_orderdetails.strSalesOrderNo ASC";
							
					//echo $sql;	
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
                        $soName = $row['strSalesOrderNo'];
                        if ($row['SO_TYPE'] == 2){
                            $soName = $soName.$STICKER_MAKING;
                        }
						if($row['strSalesOrderNo']==$salesOrderNo)
						echo "<option value=\"".$row['strSalesOrderNo']."\" selected=\"selected\">".$soName."</option>";
						else
						echo "<option value=\"".$row['strSalesOrderNo']."\">".$soName."</option>";
					}
				?>
              </select></td>
            </tr>              </table></td>
      </tr>
    </table></td></tr>    <tr><td><table width="100%">
      <tr>
        <td><fieldset  class="tableBorder_allRound">
          <table width="100%" border="0" cellpadding="0" cellspacing="0" >
            <tr height="21">
              <td width="10%" class="normalfnt">Location</td>
              <td width="24%" class="normalfnt"><select name="cboLocation" id="cboLocation" class="validate[required]"  style="width:133px">
                <?php
				if($orderNo!='')
				{
					$sql = "SELECT
								mst_customer_locations.intLocationId,
								mst_customer_locations_header.strName
							FROM
								mst_customer_locations
								Inner Join mst_customer_locations_header ON mst_customer_locations.intLocationId = mst_customer_locations_header.intId
							WHERE
								mst_customer_locations.intCustomerId =  '$x_custId' order by strName";
					$result = $db->RunQuery($sql);
					echo  "<option value=\"\"></option>";
					while($row=mysqli_fetch_array($result))
					{
						if($row['intLocationId']==$x_customerLocation)
							echo "<option  selected=\"selected\" value=\"".$row['intLocationId']."\">".$row['strName']."</option>";
						else
							echo "<option value=\"".$row['intLocationId']."\">".$row['strName']."</option>";
					}	
				}
			?>
              </select></td>
              <td width="11%" class="normalfnt">Date</td>
              <td width="24%" class="normalfnt"><input name="dtDate" disabled type="text" value="<?php if($serialNo){ echo substr($AODDate,0,10); }else { echo date("Y-m-d"); }?>" class="txtbox" id="dtDate" style="width:80px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              <td width="9%" class="normalfnt">Qty</td>
              <td width="22%" class="normalfnt"><input  id="txtQty" class="validate[required,custom[number]] invoQty normalfnt" style="width:80px;text-align:right" type="text" value="<?php echo $x_qty ?>" readonly/></td>
              <td width="0%" align="right" class="normalfnt">&nbsp;</td> 
            
               
            <tr height="21">
              <td class="normalfnt">Remarks</td>
              <td colspan="4" class="normalfnt"><textarea name="txtNote" id="txtNote" rows="3"  maxlength="250" style="width:425px"><?php echo $x_note; ?></textarea></td>
              <td align="right" class="normalfnt" valign="top">&nbsp;</td>
              <td align="right" class="normalfnt" valign="top">&nbsp;</td>
              </tr>  
            
              </table>
        </fieldset></td>
      </tr>
      <tr>
        <td class="normalfntRight" align="right"><img src="images/Tadd.jpg" name="butInsertRowPopup" width="78" height="24" class="mouseover" id="butInsertRowPopup" /></td></tr>
      </table></td></tr>
      <tr>
        <td><div style="width:1100px;height:300px;overflow:scroll" >
          <table width="1100" class="bordered" id="tblMain" >
            <tr class="">
              <th width="3%" >Del</th>
              <th width="11%" >S/O</th>
              <th width="6%" >Part</th>
              <th width="13%" >Bg Color</th>
              <th width="6%" >Line No</th>
              <th width="6%" >Cut No</th>
              <th width="6%" >Size</th>
              <th width="6%" > Bal Qty</th>
              <th width="8%" >Bundle Data</th>
              <th width="8%" > Sample Qty</th>
              <th width="8%" >Good Qty</th>
              <th width="8%" > EMD-D Qty</th>
              <th width="8%" >P-D Qty</th>
              <th width="8%" > F-D Qty</th>
              <th width="3%" >Cut Return</th>
              <th width="3%" > Tot Qty</th>
              <th width="3%" > Remarks</th>
              </tr>
              <?php
				$totAmm=0;
				$result=loadDetails($serialNo,$year,$orderNo,$orderYear);
				while($row=mysqli_fetch_array($result))
				{
					$cutNo=$row['strCutNo'];
					$salesOrderId=$row['intSalesOrderId'];
					$salesOrderNo=$row['strSalesOrderNo'];
					$partId=$row['intPart'];
					$part=$row['part'];
					$bgColoId=$row['intGroundColor'];
					$bgColor=$row['bgcolor'];
					$lineNo=$row['strLineNo'];
					$size=$row['strSize'];
					$sampleQty=(float)($row['dblSampleQty']);
					$goodQty=(float)($row['dblGoodQty']);
					$embroideryQty=(float)($row['dblEmbroideryQty']);
					$pDammageQty=(float)($row['dblPDammageQty']);
					$fdammageQty=(float)($row['dblFdammageQty']);
					$cutRetQty=(float)($row['dblCutRetQty']);
					$remarks=$row['strRemarks'];
					$tot=(float)($sampleQty)+(float)($goodQty)+(float)($embroideryQty)+(float)($pDammageQty)+(float)($fdammageQty)+(float)($cutRetQty);
					$totAmm+=$tot;
					$orderQty=$row['orderQty'];
					$orderQty=1000000;
					
					
					$balQty=loadBalQty($location,$orderNo,$orderYear,$row['strCutNo'],$row['intSalesOrderId'],$row['intPart'],$row['strSize']);
					$nonStkConfQty=loadNonStockConfQty($location,$orderNo,$orderYear,$row['strCutNo'],$row['intSalesOrderId'],$row['intPart'],$row['strSize']);
					//$qty=$row['Qty'];
					$maxQty=$balQty-$nonStkConfQty;
				//	if(($orderQty>0) ||($qty>0)){
			  ?>
<tr class="normalfnt">
  <td align="center" bgcolor="#FFFFFF" id=""><img class="delImg" src="images/del.png" width="15" height="15" /></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $salesOrderId; ?>"  class="salesOrderNo"><?php echo $salesOrderNo; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $partId; ?>" class="part"><?php echo $part; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $bgColoId; ?>" class="bgColor"><?php echo $bgColor; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $lineNo; ?>" class="line"><?php echo $lineNo; ?></td>
  <td align="center" bgcolor="#FFFFFF" id="<?php echo $cutNo; ?>" class="cutNo"><?php echo $cutNo; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $size; ?>" class="size"><?php echo $size; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $balQty; ?>" class="balQty"><?php echo $balQty; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="" class="btAddBundles"><img src="images/add_new.png" id="addBundles"  class="cls_addBundles" width="15" height="15" alt=""/></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $maxQty; ?>"><input id="1" class="validate[required,custom[integer],max[<?php echo $tot ?>]] sqtyP calTot" type="text" value="<?php echo $sampleQty; ?>" style="width:70px;text-align:center"></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $tot; ?>"><input id="<?php echo $tot; ?>" class="validate[required,custom[integer],max[<?php echo $tot ?>]] gqtyP calTot" type="text" value="<?php echo $goodQty; ?>" style="width:70px;text-align:center" disabled="disabled"></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $maxQty; ?>"><input id="1" class="validate[required,custom[integer],max[<?php echo $tot ?>]] eqtyP calTot" type="text" value="<?php echo $embroideryQty; ?>" style="width:70px;text-align:center"></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $maxQty; ?>"><input id="1" class="validate[required,custom[integer],max[<?php echo $tot ?>]] pqtyP calTot" type="text" value="<?php echo $pDammageQty; ?>" style="width:70px;text-align:center"></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $maxQty; ?>"><input id="1" class="validate[required,custom[integer],max[<?php echo $tot ?>]] fqtyP calTot" type="text" value="<?php echo $fdammageQty; ?>" style="width:70px;text-align:center"></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $maxQty; ?>"><input id="1" class="validate[required,custom[integer],max[<?php echo $tot ?>]] cutRetQtyP calTot" type="text" value="<?php echo $cutRetQty; ?>" style="width:70px;text-align:center"></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $size; ?>" class="tot"><?php echo $tot; ?></td>
			<td align="center" bgcolor="#FFFFFF" id=""><input id="" class="remarks" type="text" value="<?php echo $remarks; ?>" style="width:120px;text-align:center"></td>
            </tr>       
             <?php
				//	}
				}
			  ?>  
         </table>
        </div></td></tr>
     <tr>
        <td align="center" class=""><table width="100%" border="0">
          <tr>
            <td width="17%" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td width="38%">&nbsp;</td>
            <td width="12%" class="normalfnt">Total</td>
            <td width="9%" align="right" class="tableBorder normalfntRight"><span id="divTotal" style="text-align:right"><?php echo $totAmm ?></span>&nbsp;</td>
            </tr>
          
          </table></td>
      </tr>       </tr>
      <tr>
        <td align="center" class="tableBorder_allRound">
        <img src="images/Tnew.jpg" width="92" height="24" id="butNew" name="butNew"  class="mouseover"/>
        <img src="images/Tsave.jpg" width="92" height="24" id="butSave" name="butSave"  class="mouseover"/>
        <img class="mouseover" src="images/Tconfirm.jpg" width="92" height="24" id="butConfirm" name="butConfirm" style=" <?php if(($intStatus>1) and ($confirmatonMode==1)){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>  " />
        <img src="images/Treport.jpg" width="92" height="24" id="butReport" name="butReport"  class="mouseover"/><a href="main.php"><img src="images/Tclose.jpg" width="92" height="24"  class="mouseover"/></a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<div style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</body>
</html>
<?php
function loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade)
{
		global $db;
		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				ware_stocktransactions_fabric.intLocationId =  '$location' AND
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType =  'Received'
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return (float)($rows['dblQty']);
		
}
	//--------------------------------------------------------------
	
function loadDetails($serialNo,$year,$orderNo,$orderYear){
		global $db; 
	   	$sql = "SELECT
				trn_orderdetails.strSalesOrderNo, 
				trn_orderdetails.intQty as orderQty, 
				ware_fabricdispatchdetails.strCutNo,
				mst_part.strName as part,
				mst_colors_ground.strName as bgcolor,
				ware_fabricdispatchdetails.strLineNo,
				ware_fabricdispatchdetails.strSize,
				ware_fabricdispatchdetails.dblSampleQty,
				ware_fabricdispatchdetails.dblGoodQty,
				ware_fabricdispatchdetails.dblEmbroideryQty,
				ware_fabricdispatchdetails.dblPDammageQty,
				ware_fabricdispatchdetails.dblFdammageQty,
				ware_fabricdispatchdetails.dblCutRetQty,
				ware_fabricdispatchdetails.intSalesOrderId,
				ware_fabricdispatchdetails.intPart,
				ware_fabricdispatchdetails.intGroundColor , 
				ware_fabricdispatchdetails.strRemarks 
				FROM
				trn_orderdetails
				Inner Join ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear
				Inner Join ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
				left Join mst_part ON ware_fabricdispatchdetails.intPart = mst_part.intId
				left Join mst_colors_ground ON ware_fabricdispatchdetails.intGroundColor = mst_colors_ground.intId
				WHERE
				ware_fabricdispatchheader.intBulkDispatchNo =  '$serialNo' AND
				ware_fabricdispatchheader.intBulkDispatchNoYear =  '$year' ";

			return $result = $db->RunQuery($sql);
	}
//--------------------------------------------------------------
function loadBalQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$intPart,$size){
	global $db;
	   $sql = "SELECT
				Sum(ware_stocktransactions_fabric.dblQty) AS qty
				FROM ware_stocktransactions_fabric
				WHERE
				ware_stocktransactions_fabric.intLocationId =  '$location' AND
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.strCutNo =  '$cutNo' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.intPart =  '$intPart'";

	$result = $db->RunQuery($sql);
	$rows = mysqli_fetch_array($result);
	return (float)($rows['qty']);
}
	//--------------------------------------------------------------
	function loadNonStockConfQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$intPart,$size){
		global $db;
		   $sql = "SELECT
					sum(ware_fabricdispatchdetails.dblSampleQty+
					ware_fabricdispatchdetails.dblGoodQty+
					ware_fabricdispatchdetails.dblEmbroideryQty+
					ware_fabricdispatchdetails.dblPDammageQty+
					ware_fabricdispatchdetails.dblFdammageQty+
					ware_fabricdispatchdetails.dblCutRetQty) as qty
					FROM
					ware_fabricdispatchheader
					Inner Join ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear
					WHERE
					ware_fabricdispatchheader.intOrderNo =  '$orderNo' AND
					ware_fabricdispatchheader.intOrderYear =  '$orderYear' AND
					ware_fabricdispatchdetails.intSalesOrderId =  '$salesOrderId' AND
					ware_fabricdispatchdetails.strCutNo =  '$cutNo' AND
					ware_fabricdispatchdetails.strSize =  '$size' AND
					ware_fabricdispatchdetails.intPart =  '$intPart' AND
					ware_fabricdispatchheader.intStatus >  '1' AND
					ware_fabricdispatchheader.intApproveLevels >= ware_fabricdispatchheader.intStatus AND
					ware_fabricdispatchheader.intCompanyId =  '$location'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
	//	echo (float)($rows['qty']);
		return (float)($rows['qty']);
	}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//--------------------------------------------------------

?>

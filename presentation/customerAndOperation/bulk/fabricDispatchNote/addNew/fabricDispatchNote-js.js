var basepath	= 'presentation/customerAndOperation/bulk/fabricDispatchNote/addNew/';
//var arrParent 	= [];
var bundleRow ='';

arrParent 		= ( typeof arrParent != 'undefined' && arrParent instanceof Array ) ? arrParent : [];
arrParent_key 	= ( typeof arrParent_key != 'undefined' && arrParent_key instanceof Array ) ? arrParent_key : [];
arrIndex 		= ( typeof arrIndex != 'undefined' && arrIndex instanceof Array ) ? arrIndex : [];

$(document).ready(function(){	
	$("#frmFabricDispatchNote").validationEngine();
	$("#frmFabricDispatchNote #cboStyle").die('change').live('change',function(){
	   	$('#cboSalesOrderNo').val('');
	   	$('#cboGraphicNo').val('');
	});

	$("#frmFabricDispatchNote #cboOrderYear").die('change').live('change',function(){
		$('#cboGraphicNo').val('');
		$('#cboStyle').val('');
		$('#cboPONo').val('');
		$('#cboOrderNo').val('');
		$('#cboCustomer').val('');
		$('#cboSalesOrderNo').val('');
		clearGrid();
		loadAllCombo();
	});

	$("#frmFabricDispatchNote #cboSalesOrderNo").die('change').live('change',function(){
		if($("#cboOrderNo").val()=='')
		loadOrderNos();
	});

	$("#frmFabricDispatchNote #cboOrderNo").die('change').live('change',function(){
		submitForm();
	});
	
	$("#frmFabricDispatchNote .search").die('change').live('change',loadAllCombo);
	
	$("#frmFabricDispatchNote .calTot").die('keyup').live('keyup',function(){
		validateTot(this);
		calTotals();
	});
	
	$('#frmFabricRecvNotePopup #butAdd').die('click').live('click', addClickedRows);
	$('#frmFabricRecvNotePopup #butClose1').die('click').live('click', disablePopup);
	$("#frmFabricRecvNotePopup .searchP").die('change').live('change',function(){
		//loadSalesLineNoPartSizesComboes();
	});
	$("#frmFabricRecvNotePopup #chkAll").die('click').live('click',function(){
		checkUncheckRows(this);
	});
	
	
	
	//-------------------------------------------------------------------------
	$('#frmBarcodeWiseDispPopup #butAddB').die('click').live('click', function()
	{
		 saveBarCodeWiseData();
		 loadBarCodeWiseData();
		 
	}); 
	//$('#frmBarcodeWiseDispPopup #butAddB').die('click').live('click',loadBarCodeWiseData );
	$('#frmBarcodeWiseDispPopup #butCloseB').die('click').live('click', disablePopup);
	//-------------------------------------------------------------------------
	
	$('#frmFabricDispatchNote .delImg').die('click').live('click',function(){
		
		var serialNo 		= $('#txtSerialNo').val();
	 	var Year	  		= $('#txtYear').val();
		var tmpdisNo		= $('#tmpdisNo').val();
		var cut_no 			= $(this).parent().parent().find(".cutNo").attr('id');
		var sales_no		= $(this).parent().parent().find(".salesOrderNo").attr('id');
		var size			= $(this).parent().parent().find(".size").attr('id');
		var url				= basepath+"fabricDispatchNote-db-set.php?requestType=delete_barcode_wise_data_main&cut_no="+cut_no+"&sales_no="+sales_no
	+"&size="+size+"&tmpdisNo="+tmpdisNo+"&serialNo="+serialNo+"&Year="+Year;
	
	var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST', 
			async:false,
			success:function(json)
			{
				//$('#tmpdisNo').val(json.tmpdisno);
			}
				});
		
		$(this).parent().parent().remove();
		calTotals();
	});
	
	
	$('#frmBarcodeWiseDispPopup .delImgp').die('click').live('click',function(){
		
		var serialNo 		= $('#txtSerialNo').val();
	 	var Year	  		= $('#txtYear').val();
		var tmpdisNo		= $('#tmpdisNo').val();
		var rowCount 		= document.getElementById('tblPOP').rows.length;
		var cut_no 			= $(this).parent().parent().find(".cut_noP").attr('id');
		var sales_no		= $(this).parent().parent().find(".salesP").attr('id');
		var size			= $(this).parent().parent().find(".sizeP").attr('id');
		var part			= $(this).parent().parent().find(".partP").attr('id');
		var bgColor			= $(this).parent().parent().find(".bgColorP").attr('id');
		var line			= $(this).parent().parent().find(".lineP").attr('id');
		var bcode			= $(this).parent().parent().find(".bc").val();
		var samp_qty		= $(this).parent().parent().find(".sampP").val();
		var good_qty		= $(this).parent().parent().find(".goodP").val();
		var emb_qty			= $(this).parent().parent().find(".embP").val();
		var pd_qty			= $(this).parent().parent().find(".pdP").val();
		var fd_qty			= $(this).parent().parent().find(".fdP").val();
		var cut				= $(this).parent().parent().find(".cutP").val();
		var tot				= $(this).parent().parent().find(".totP").val();
		var remark			= $(this).parent().parent().find("#txtNoteP").val();
		
		var url				= basepath+"fabricDispatchNote-db-set.php?requestType=delete_barcode_wise_data&cut_no="+cut_no+"&sales_no="+sales_no
	+"&size="+size+"&bcode="+bcode+"&tmpdisNo="+tmpdisNo+"&serialNo="+serialNo+"&Year="+Year;
	
	var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST', 
			async:false,
			success:function(json)
			{
				//$('#tmpdisNo').val(json.tmpdisno);
			}
				});
				
						if(rowCount!=2)
							{
								$(this).parent().parent().remove();
							}

						if(rowCount==2)
							{

								$(this).parent().parent().remove();
								$('#tblPOP').html( $('#tblPOP').html() + $('#tbl_div').html());
							}
	});
	

	$("#frmBarcodeWiseDispPopup .totP").die('keyup').live('keyup',function(){
   		//checkQtyParent(this);	
		var samp 	= parseInt($(this).closest("tr").find(".sampP").val()) || 0;
		var goodP 	= parseInt($(this).closest("tr").find(".goodP").val()) || 0;
		var embP 	=	parseInt($(this).closest("tr").find(".embP").val()) || 0;
		var pdP 	= parseInt($(this).closest("tr").find(".pdP").val()) || 0;
		var fdP 	= parseInt($(this).closest("tr").find(".fdP").val()) || 0;
		var cutP 	=parseInt($(this).closest("tr").find(".cutP").val()) || 0;
		
		var tot 	= samp+goodP+embP+pdP+fdP+cutP;
		 $(this).val(tot)
		
	});
	
	$('#butAddRow').die('click').live('click',function(){
		
		var rowCount 		= document.getElementById('tblPOP').rows.length;
		document.getElementById('tblPOP').insertRow(rowCount);
		document.getElementById('tblPOP').rows[rowCount].innerHTML = document.getElementById('tblPOP').rows[rowCount-1].innerHTML;
		var cellCount 		= document.getElementById('tblPOP').rows[rowCount].cells.length;
		var obj 			= document.getElementById('tblPOP').rows[rowCount] ;
		
		($(obj).find('.bc').val('0')) ;
		($(obj).find('.sampP').html('0'));
		($(obj).find('.goodP').html('0'));
		($(obj).find('.embP').val('0'));
		($(obj).find('.pdP').val('0'));
		($(obj).find('.fdP').val('0'));
		($(obj).find('.cutP').val('0'));
		($(obj).find('.totP').val('0'));
		($(obj).find('#txtNoteP').val(''));
	
	});
	
	$('#frmFabricDispatchNote .cls_addBundles').die('click').live('click',function(){
 		//alert('ok');
	});
	
		
//-----------------------------------------------------------------
	$('#frmFabricDispatchNote #butInsertRowPopup').die('click').live('click',function(){
		closePopUp();
		
		var row				= this.parentNode.parentNode.rowIndex;
		var orderNo			= $('#cboOrderNo').val();
		var orderYear		= $('#cboOrderYear').val();
		var salesOrderNo 	= $('#cboSalesOrderNo').val();
		var serialNo		= $('#txtSerialNo').val();
		var year			= $('#txtYear').val();
		var styleNo			= $('#cboStyle').val();
		var graphicNo		= $('#cboGraphicNo').val();
		var custPONo		= $('#cboPONo').val();
        var locationType = $('#locationType').val();
		
		if(orderNo==''){
			alert("Please select the Order No.");
			return false;
		}
		
		popupWindow3('1');
		$('#popupContact1').load(basepath+'fabricDispatchNotePopup.php?orderNo='+orderNo+'&orderYear='+orderYear+'&locationType='+locationType+'&serialNo='+serialNo+'&year='+year+'&styleNo='+URLEncode(styleNo)+'&graphicNo='+URLEncode(graphicNo)+'&custPONo='+URLEncode(custPONo)+'&salesOrderNo='+URLEncode(salesOrderNo),function(){
				//loadAlreadySaved();
				 //-------------------------------------------- 
				  $('#frmFabricRecvNotePopup #imgSearchItems').click(function(){
					  	var cutNoP=$('#txtCutNo').val();
						if(cutNoP==''){
							alert("Please select the Cut No");
							return false;
						}
						var rowCount = document.getElementById('tblPopup').rows.length;
						for(var i=1;i<rowCount;i++)
						{
							document.getElementById('tblPopup').deleteRow(1);
						}
						var orderYear 		= $('#cboOrderYear').val();
						var orderNo 		= $('#cboOrderNo').val();
						var styleNo 		= $('#cboStyleP').val();
						var salesOrderId 	= $('#cboSalesOrderNoP').val();
						var lineNo 			= $('#cboLineNo').val();
						var part 			= $('#cboPartNo').val();
						var cutNo 			= $('#cboCutNo').val();
						var size 			= $('#cboSizes').val();
						var color 			= $('#cboColor').val();
						var flagViewZeero	= $("#chkZeeroBal").attr('checked')
						
						var url 			= basepath+"fabricDispatchNote-db-get.php?requestType=loadPartDetails";
						
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							data:"orderNo="+orderNo+"&orderYear="+orderYear+"&styleNo="+URLEncode(styleNo)+"&salesOrderId="+salesOrderId+"&lineNo="+URLEncode(lineNo)+"&part="+part+"&cutNo="+URLEncode(cutNo)+"&size="+URLEncode(size)+"&color="+URLEncode(color),  
							async:false,
							success:function(json){

								var length = json.arrCombo.length;
								var arrCombo = json.arrCombo;

								for(var i=0;i<length;i++)
								{
									var cutNo=arrCombo[i]['cutNo'];	
									var salesOrderId=arrCombo[i]['salesOrderId'];	
									var salesOrderNo=arrCombo[i]['salesOrderNo'];	
									var partId=arrCombo[i]['partId'];	
									var part=arrCombo[i]['part'];	
									var groundColorId=arrCombo[i]['groundColorId'];	
									var groundColor=arrCombo[i]['groundColor'];	
									var lineNo=arrCombo[i]['lineNo'];
									var size=arrCombo[i]['size'];	
								 	var qty=arrCombo[i]['qty'];	
								 	var nonStkConfQty=arrCombo[i]['nonStkConfQty'];	
									var balQty=parseFloat(qty)-parseFloat(nonStkConfQty);	
									
									
										
									var content='<tr class="normalfnt">';
									content +='<td align="center" bgcolor="#FFFFFF" id=""><input type="checkbox" id="chkDisp" class="chk"/></td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+cutNo+'" class="cutNoP">'+cutNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderId+'" class="salesOrderNoP">'+salesOrderNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+partId+'" class="partP">'+part+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+groundColorId+'" class="bgColorP">'+groundColor+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+lineNo+'" class="lineP">'+lineNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+size+'" class="sizeP">'+size+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+balQty+'"><input id="1" class="validate[required,custom[integer],max['+balQty+']]  qtyP" type="text" value="'+balQty+'" style="width:70px;text-align:center"></td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+balQty+'" class="balQtyP">'+balQty+'</td></tr>';
									if((balQty>0) ||((flagViewZeero==true)))
									add_new_row('#frmFabricRecvNotePopup #tblPopup',content);
									
									
								}
								
									checkAlreadySelected();
							}
							
						});
						
				  });
					//------------------
				
			});	
	});
	
//-----------------------------------------------------------------
	$('.cls_addBundles').die('click').live('click',function()
	{
		bundleRow		=this;
		var serialNo 	= $('#txtSerialNo').val();
	 	var Year	  	= $('#txtYear').val();
		var tmpdisNo	= $('#tmpdisNo').val();
		var sales_no 	= $(this).parent().parent().find(".salesOrderNo").attr('id');
		var cut_no 		= $(this).parent().parent().find(".cutNo").attr('id');
		var size 		= $(this).parent().parent().find(".size").attr('id');
		var part 		= $(this).parent().parent().find(".part").attr('id');
		var bgColor 	= $(this).parent().parent().find(".bgColor").attr('id');
		var line 		= $(this).parent().parent().find(".line").attr('id');
		var indexRow_main 	= this.parentNode.parentNode.rowIndex;
		
 		popupWindow3('1');

		$('#popupContact1').load(basepath+'barcode_wise_dispatch_popup.php?sales_no='+sales_no+'&cut_no='+cut_no+'&size='+size+'&indexRow_main='+indexRow_main+'&part='+part+'&bgColor='+bgColor+'&line='+line+"&serialNo="+serialNo+"&Year="+Year+"&tmpdisNo="+tmpdisNo,function(){
				$('.bc').die('change').live('change',function(){
					//alert('ok');
					//alert($(this).closest('tr').next().html());
					var $curRow = $(this).closest('tr'),
					$newRow = $curRow.clone(true);
					console.log($newRow);
					$curRow.after($newRow);
					console.log('added');
					$(this).closest('tr').next().find('.bc').focus();
					$(this).closest('tr').next().find('.bc').val('');
				});
				//$('#butAdd').click(addClickedRows);
				//$('#butClose1').click(disablePopup);
				
		});	
	}); 
//------------------------------------------------------------------
	
//------------------------------------------------------------------
  $('#frmFabricDispatchNote #butSave').die('click').live('click',function(){
	var requestType = '';
	if ($('#frmFabricDispatchNote').validationEngine('validate'))   
    { 
		showWaiting();
		var data = "requestType=save";
		
			data+="&serialNo="		+	$('#txtSerialNo').val();
			data+="&Year="	+	$('#txtYear').val();
			data+="&custLocation="	+	$('#cboLocation').val();
			data+="&orderNo="	+	$('#cboOrderNo').val();
			data+="&orderYear="	+	$('#cboOrderYear').val();
			//data+="&AODNo="	+	$('#txtAODNo').val();
			data+="&dispatchDate="	+	$('#dtDate').val();
			data+="&remarks="	+	URLEncode($('#txtNote').val());

			var rowCount = document.getElementById('tblMain').rows.length;
			if(rowCount==1){
				alert("No items to dispatch");hideWaiting();
				return false;				
			}
			var row = 0;
			
			var arr="[";
			
			$('#tblMain .cutNo').each(function(){
				
				var cutNo	= $(this).attr('id');
				var salesOrderId	= $(this).parent().find(".salesOrderNo").attr('id');
				var salesOrderNo	= $(this).parent().find(".salesOrderNo").html();
				var partId	= $(this).parent().find(".part").attr('id');
				var part	= $(this).parent().find(".part").html();
				var bgColorId	= $(this).parent().find(".bgColor").attr('id');
				var bgColor	= $(this).parent().find(".bgColor").html();
				var line	= $(this).parent().find(".line").html();
				var size	= $(this).parent().find(".size").html();
				var sqtyP	= parseFloat($(this).parent().find(".sqtyP").val());
				var gqtyP	= parseFloat($(this).parent().find(".gqtyP").val());
				var eqtyP	= parseFloat($(this).parent().find(".eqtyP").val());
				var pqtyP	= parseFloat($(this).parent().find(".pqtyP").val());
				var fqtyP	= parseFloat($(this).parent().find(".fqtyP").val());
				var cutRetQtyP	= parseFloat($(this).parent().find(".cutRetQtyP").val());
				var remarks	= $(this).parent().find(".remarks").val();
				var arrKey1 =cutNo+'/'+salesOrderId+'/'+size;
				
				//alert(arrParent['dd/1/S']['111'][0]);
				//alert(arrParent[cutNo+'/'+salesOrderId+'/'+size][111][0]);
				
				var tot=sqtyP+gqtyP+eqtyP+pqtyP+fqtyP+cutRetQtyP;
					
					if(tot>0){
				        arr += "{";
						arr += '"cutNo":"'+	URLEncode(cutNo) +'",' ;
						arr += '"salesOrderId":"'+ salesOrderId +'",' ;
						arr += '"salesOrderNo":"'+ URLEncode(salesOrderNo) +'",' ;
						arr += '"partId":"'+ partId +'",' ;
						arr += '"part":"'+ URLEncode(part) +'",' ;
						arr += '"bgColorId":"'+	bgColorId +'",' ;
						arr += '"bgColor":"'+ URLEncode(bgColor) +'",' ;
						arr += '"line":"'+ URLEncode(line) +'",' ;
						arr += '"size":"'+ URLEncode(size) +'",' ;
						arr += '"sqty":"'+ sqtyP +'",' ;
						arr += '"gqty":"'+ gqtyP +'",' ;
						arr += '"eqty":"'+ eqtyP +'",' ;
						arr += '"pqty":"'+ pqtyP +'",' ;
						arr += '"fqty":"'+ fqtyP +'",' ;
						arr += '"cutRetQty":"'+ cutRetQtyP +'",' ;
						arr += '"remarks":"'+ URLEncode(remarks) +'"' ;
						arr +=  '},';
						
					}
			});
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basepath+"fabricDispatchNote-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			type:'POST', 
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmFabricDispatchNote #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						//loadCombo_frmSampleInfomations();
						var t=setTimeout("alertx()",1000);
						$('#txtSerialNo').val(json.serialNo);
						$('#txtYear').val(json.year);
						if(json.confirmationMode==1){
							$('#butConfirm').show();
						}
						else{
							$('#butConfirm').hide();
						}
					//	document.location.href =document.location.href ;
					}
				},
			error:function(xhr,status){
					
					$('#frmFabricDispatchNote #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
			hideWaiting();
	}
	  
	  
	 //---------------save bar code vals permanenly------- 
	 
	 	var serialNo 	= 	$('#txtSerialNo').val();
	 	var Year	  	=	$('#txtYear').val();
		var tmpdisNo	=	$('#tmpdisNo').val();
		
		save_bar_code_val(serialNo,Year,tmpdisNo);
			
	//--------------------  
		
   });

$('#frmFabricDispatchNote #butReport').die('click').live('click',function(){
	if($('#txtSerialNo').val()!=''){
		window.open('?q=919&serialNo='+$('#txtSerialNo').val()+'&year='+$('#txtYear').val());	
	}
	else{
		alert("There is no Dispatch No to view");
	}
});
//----------------------------------	
$('#frmFabricDispatchNote #butConfirm').die('click').live('click',function(){
	if($('#txtSerialNo').val()!=''){
		window.open('?q=919&serialNo='+$('#txtSerialNo').val()+'&year='+$('#txtYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no Dispatch No to confirm");
	}
});

$('#frmFabricDispatchNote #butNew').die('click').live('click',function(){
		window.location.href = "?q=470";
		$('#frmFabricDispatchNote').get(0).reset();
		$('#frmFabricDispatchNote #txtSerialNo').val('');
		$('#frmFabricDispatchNote #txtYear').val('');
		$('#frmFabricDispatchNote #cboStyle').val('');
		$('#frmFabricDispatchNote #txtNote').val('');
		$("#frmFabricDispatchNote #cboStyle").change();
		var currentTime = new Date();
		var month 		= currentTime.getMonth()+1 ;
		var day 		= currentTime.getDate();
		var year 		= currentTime.getFullYear();
		
		if(day<10)
			day		= '0'+day;
		
		if(month<10)
			month	= '0'+month;
			
		d	= year+'-'+month+'-'+day;
		
		$('#frmFabricDispatchNote #dtDate').val(d);
	});
});

function alertx()
{
	$('#frmInvoice #butSave').validationEngine('hide')	;
}
//----------------------------------------------- 
function alertDelete()
{
	$('#frmInvoice #butDelete').validationEngine('hide')	;
}
//----------------------------------------------- 
function closePopUp(){
	
}
//-----------------------------------------------
function addClickedRows()
{
	var err=0;
	var rcds=0
	var tot=0;
	
	if ($('#frmFabricRecvNotePopup').validationEngine('validate'))   
    { 
	var rowCount = document.getElementById('tblPopup').rows.length;
	var cutNo=$('#txtCutNo').val();

	$('#tblPopup .salesOrderNoP').each(function(){
		var cutNo	= $(this).parent().find(".cutNoP").attr('id');
		var salesOrderIdP	= $(this).attr('id');
		var salesOrderNoP	= $(this).html();
		var partIdP	= $(this).parent().find(".partP").attr('id');
		var partP	= $(this).parent().find(".partP").html();
		var bgColorIdP	= $(this).parent().find(".bgColorP").attr('id');
		var bgColorP	= $(this).parent().find(".bgColorP").html();
		var lineP	= $(this).parent().find(".lineP").html();
		var sizeP	= $(this).parent().find(".sizeP").html();
		var balQtyP	= parseFloat($(this).parent().find(".balQtyP").html());
		var qtyP	= parseFloat($(this).parent().find(".qtyP").val());
		var diasbleFlag	= $(this).parent().find(".chk").is(':disabled');
		var chkFlag	= $(this).parent().find(".chk").is(':checked');
		
		if(qtyP>balQtyP){
			  err=1;
		}

		if((balQtyP>0) && (diasbleFlag==false) && (chkFlag==true) && (qtyP>0) ){
			rcds++;
			
			var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="images/del.png" width="15" height="15" /></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderIdP+'" class="salesOrderNo">'+salesOrderNoP+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+partIdP+'" class="part">'+partP+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+bgColorIdP+'" class="bgColor">'+bgColorP+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+lineP+'" class="line">'+lineP+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+cutNo+'" class="cutNo">'+cutNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+sizeP+'" class="size">'+sizeP+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+balQtyP+'" class="balQty">'+balQtyP+'</td>';
			
			content +='<td align="center" bgcolor="#FFFFFF" id="" class="btAddBundles"><img src="images/add_new.png" id="addBundles" class="cls_addBundles"  width="15" height="15" alt="" /></td>';
			
			content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="sqtyP" class="validate[required,custom[integer],max['+qtyP+']] sqtyP calTot" style="width:70px;text-align:center" type="text" value="0"/></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id='+qtyP+'><input  id="'+qtyP+'" class="validate[required,custom[integer],max['+qtyP+']] gqtyP calTot" style="width:70px;text-align:center" type="text" value="'+qtyP+'" disabled="disabled"/></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="eqtyP" class="validate[required,custom[integer],max['+qtyP+']] eqtyP calTot" style="width:70px;text-align:center" type="text" value="0"/></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="pqtyP" class="validate[required,custom[integer],max['+qtyP+']] pqtyP calTot" style="width:70px;text-align:center" type="text" value="0"/></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="fqtyP" class="validate[required,custom[integer],max['+qtyP+']] fqtyP calTot" style="width:70px;text-align:center" type="text" value="0"/></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="cutRetQtyP" class="validate[required,custom[integer],max['+qtyP+']] cutRetQtyP calTot" style="width:70px;text-align:center" type="text" value="0"/></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="" class="tot">'+qtyP+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id=""><input id="" class="remarks" type="text" value="" style="width:120px;text-align:center"></td>';
			content +='</tr>';
			
			tot+=qtyP;
			
			add_new_row('#frmFabricDispatchNote #tblMain',content);
	}
	});
	
	$('#divTotal').html(tot);
	checkAlreadySelected();
	//disablePopup();
	}
	 if(err==1){
		alert('Qty cant exceed the Balance Qty');
		return false;	
	}
}
//-------------------------------------
function loadGraphicNo(){
	    var orderYear = $('#cboOrderYear').val();
	    var styleNo = $('#cboStyle').val();
		var url 		= basepath+"fabricDispatchNote-db-get.php?requestType=loadGraphicNo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"styleNo="+URLEncode(styleNo)+"&orderYear="+orderYear,
			async:false,
			success:function(json){
				
					document.getElementById("cboGraphicNo").innerHTML=json.graphicNo;
				//	document.getElementById("cboCustomer").value='';
				//	document.getElementById("cboPONo").innerHTML='';
				//	document.getElementById("cboOrderNo").innerHTML='';
					document.getElementById("txtQty").value='';
					
					
					
			}
		});
}

//-----------------------------------------------
function loadCustomerPONo(){
	    var orderYear = $('#cboOrderYear').val();
	    var graphicNo = $('#cboGraphicNo').val();
	    var styleNo = $('#cboStyle').val();
		var url 		= basepath+"fabricDispatchNote-db-get.php?requestType=loadCustomerPONo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"graphicNo="+URLEncode(graphicNo)+"&orderYear="+orderYear+"&styleNo="+URLEncode(styleNo),
			async:false,
			success:function(json){
					document.getElementById("cboPONo").innerHTML=json.customerPoNo;
			}
		});
}

//-----------------------------------------------
function loadOrderNo(){
	    var styleNo = $('#cboStyle').val();
	    var graphicNo = $('#cboPONo').val();
	    var poNo = $('#cboPONo').val();
	    var orderYear = $('#cboOrderYear').val();
	    var poNo = $('#cboPONo').val();
	    var customer = $('#cboCustomer').val();
		var url 		= basepath+"fabricDispatchNote-db-get.php?requestType=loadOrderNo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"poNo="+URLEncode(poNo)+"&customer="+customer+"&orderYear="+orderYear+"&styleNo="+URLEncode(styleNo)+"&graphicNo="+URLEncode(graphicNo)+"&poNo="+URLEncode(poNo),
			async:false,
			success:function(json){
				
					document.getElementById("cboOrderNo").innerHTML=json.orderNo;
					document.getElementById("cboCustomer").value=json.customer;
					if(poNo==''){
					document.getElementById("cboPONo").innerHTML=json.PoNo;
					}
			}
		});
}
//--------------------------------------------------
function loadCustomerPO(){
	    var orderNo = $('#cboOrderNo').val();
	    var orderYear = $('#cboOrderYear').val();
		var url 		= basepath+"fabricDispatchNote-db-get.php?requestType=loadPoNo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"orderNo="+orderNo+"&orderYear="+orderYear,
			async:false,
			success:function(json){
				
					document.getElementById("cboPONo").value=json.poNo;
					document.getElementById("cboCustomer").value=json.customer;
					if(orderNo==''){
					document.getElementById("cboOrderNo").innerHTML=json.orderNo;
					}
					//document.getElementById("cboSalesOrderNo").innerHTML=json.salesOrderNo;
			}
		});
}
//-----------------------------------------------
function loadPONoAndOrderNo(){
	    var styleNo = $('#cboStyle').val();
	    var graphicNo = $('#cboPONo').val();
	    var poNo = $('#cboPONo').val();
	    var orderNo = $('#cboOrderNo').val();
	    var orderYear = $('#cboOrderYear').val();
	    var customer = $('#cboCustomer').val();
		var url 		= basepath+"fabricDispatchNote-db-get.php?requestType=loadPONoAndOrderNo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"customer="+customer+"&orderYear="+orderYear+"&styleNo="+URLEncode(styleNo)+"&graphicNo="+URLEncode(graphicNo),
			async:false,
			success:function(json){
					//alert(json.poNo);
					document.getElementById("cboPONo").innerHTML=json.poNo;
					document.getElementById("cboOrderNo").innerHTML=json.orderNo;
			}
		});
}
//-----------------------------------------------
function loadSalesLineNoPartSizesComboes(){
	    var orderNo = $('#cboOrderNo').val();
	    var orderYear = $('#cboOrderYear').val();
	    var styleNo = $('#cboStyleP').val();
	    var salesOrderNo = $('#cboSalesOrderNoP').val();
		var url 		= basepath+"fabricDispatchNote-db-get.php?requestType=loadSalesLineNoPartSizesComboes";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"orderNo="+orderNo+"&orderYear="+orderYear+"&salesOrderNo="+URLEncode(salesOrderNo)+"&styleNo="+URLEncode(styleNo),
			async:false,
			success:function(json){
				
					document.getElementById("cboGraphicP").innerHTML=json.graphicNo;
					document.getElementById("cboSalesOrderNoP").innerHTML=json.salesOrderNo;
					document.getElementById("cboLineNo").innerHTML=json.lineNo;
					document.getElementById("cboPartNo").innerHTML=json.partNo;
					document.getElementById("cboSizes").innerHTML=json.sizes;
					document.getElementById("cboColor").innerHTML=json.color;
			}
		});
}
//-----------------------------------------------
function loadQty(){
	    var orderNo = $('#cboOrderNo').val();
	    var salesOrderNo = '';
		var url 		= basepath+"fabricDispatchNote-db-get.php?requestType=loadQty";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"orderNo="+orderNo+"&salesOrderNo="+salesOrderNo,
			async:false,
			success:function(json){
				
					document.getElementById("txtQty").value=json.qty;
			}
		});
}
//------------------------------------------------
function submitForm(){
	window.location.href = "?q=470&orderNo="+$('#cboOrderNo').val()
						+'&salesOrderNo='+$('#cboSalesOrderNo').val()
						+'&poNo='+$('#cboPONo').val()
						+'&customer='+$('#cboCustomer').val()
						+'&orderYear='+$('#cboOrderYear').val()
						+'&styleNo='+$('#cboStyle').val()
						+'&graphicNo='+$('#cboGraphicNo').val()
}
//----------------------------------------------- 
function clearGrid()
{
	var rowCount = document.getElementById('tblMain').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMain').deleteRow(1);
	}
	
	$('#txtQty').val('');
}
//---------------------------------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//------------------------------------------------------------------------------
function checkAlreadySelected(){
		$('#tblMain .cutNo').each(function(){
			
		 var cutNo	= $(this).attr('id');
		 var salesOrderId	= $(this).parent().find(".salesOrderNo").attr('id');
		 var size	= $(this).parent().find(".size").html();
		
			  $('#tblPopup .salesOrderNoP').each(function(){
				
				var cutNoP=$(this).parent().find(".cutNoP").html();
				var salesOrderIdP	= $(this).attr('id');
				var sizeP	= $(this).parent().find(".sizeP").html();
				//alert(cutNo+"=="+cutNoP+"***"+salesOrderId+"=="+salesOrderIdP+"***"+size+"=="+sizeP+"***")
				if((cutNo==cutNoP) && (salesOrderId==salesOrderIdP) && (size==sizeP)){
					$(this).parent().find(".chk").attr('checked', true);
					$(this).parent().find(".chk").attr('disabled',true);
				}
			});
	});
}
//------------------------------------------------------------------------------
function checkUncheckRows(obj){
	
		var chkFlag=$(obj).is(':checked');
		
		  $('#tblPopup .salesOrderNoP').each(function(){
				var disabledFlag=$(this).parent().find(".chk").is(':disabled');
				if((chkFlag==true) && (disabledFlag!=true)){
					$(this).parent().find(".chk").prop('checked', true);
				}
				else if((chkFlag!=true) && (disabledFlag==true)){
					$(this).parent().find(".chk").prop('checked', true);
				}
				else if(chkFlag!=true){
					$(this).parent().find(".chk").prop('checked', false);
				}
		});
}
//------------------------------------------------------------------------
function validateTot(obj){
	var bal=$(obj).parent().parent().find(".balQty").html();
	var iniitialGoodQty=$(obj).parent().parent().find(".gqtyP").attr('id');
	
		var sqtyP	= parseFloat($(obj).parent().parent().find(".sqtyP").val());
		var gqtyP	= parseFloat($(obj).parent().parent().find(".gqtyP").val());
		var eqtyP	= parseFloat($(obj).parent().parent().find(".eqtyP").val());
		var pqtyP	= parseFloat($(obj).parent().parent().find(".pqtyP").val());
		var fqtyP	= parseFloat($(obj).parent().parent().find(".fqtyP").val());
		var cutRetQtyP	= parseFloat($(obj).parent().parent().find(".cutRetQtyP").val());
		
		if(isNaN(sqtyP))
		sqtyP=0;
		if(isNaN(gqtyP))
		gqtyP=0;
		if(isNaN(eqtyP))
		eqtyP=0;
		if(isNaN(pqtyP))
		pqtyP=0;
		if(isNaN(fqtyP)) 
		fqtyP=0;
		if(isNaN(cutRetQtyP))
		cutRetQtyP=0;
	
	
	var tot1=sqtyP+eqtyP+pqtyP+fqtyP+cutRetQtyP;
	//alert(sqtyP+"-"+eqtyP+"-"+pqtyP+"-"+fqtyP);
	$(obj).parent().parent().find(".gqtyP").val(iniitialGoodQty-tot1);
	
	var gqtyP	= parseFloat($(obj).parent().find(".gqtyP").val());
	if(isNaN(gqtyP))
	gqtyP=0;

	var tot=sqtyP+eqtyP+pqtyP+fqtyP+gqtyP+cutRetQtyP;

	if($(obj).parent().parent().find(".gqtyP").val()<0){
		alert("invalid Quantities");
		$(obj).val(0);

		
		var sqtyP	= parseFloat($(obj).parent().parent().find(".sqtyP").val());
		var gqtyP	= parseFloat($(obj).parent().parent().find(".gqtyP").val());
		var eqtyP	= parseFloat($(obj).parent().parent().find(".eqtyP").val());
		var pqtyP	= parseFloat($(obj).parent().parent().find(".pqtyP").val());
		var fqtyP	= parseFloat($(obj).parent().parent().find(".fqtyP").val());
		var cutRetQtyP	= parseFloat($(obj).parent().parent().find(".cutRetQtyP").val());
		
		if(isNaN(sqtyP))
		sqtyP=0;
		if(isNaN(gqtyP))
		gqtyP=0;
		if(isNaN(eqtyP))
		eqtyP=0;
		if(isNaN(pqtyP))
		pqtyP=0;
		if(isNaN(fqtyP))
		fqtyP=0;
		if(isNaN(cutRetQtyP))
		cutRetQtyP=0;
	
	
	var tot1=sqtyP+eqtyP+pqtyP+fqtyP+cutRetQtyP;
	//alert(tot1);
		  
		$(obj).parent().parent().find(".gqtyP").val(iniitialGoodQty-tot1);
		if(isNaN(gqtyP))
		gqtyP=0;
		$(obj).val(0);
		return false;
	}
	
	var tot=sqtyP+eqtyP+pqtyP+fqtyP+gqtyP+cutRetQtyP;
	
	if(iniitialGoodQty<tot){
		alert("Input Quantities does not match with total Qty");
		$(obj).val(0);
		return false;
	}
	else{
		//$(obj).parent().parent().find(".tot").html(tot.toFixed(2));
	}
	
}
//-------------------------------------------------------------------------
function calTotals(){
	var tot=0;
	var rowCount = document.getElementById('tblMain').rows.length;
	$('#tblMain .cutNo').each(function(){
		
		var sqtyP	= parseFloat($(this).parent().find(".sqtyP").val());
		var gqtyP	= parseFloat($(this).parent().find(".gqtyP").val());
		var eqtyP	= parseFloat($(this).parent().find(".eqtyP").val());
		var pqtyP	= parseFloat($(this).parent().find(".pqtyP").val());
		var fqtyP	= parseFloat($(this).parent().find(".fqtyP").val());
		var cutRetQtyP	= parseFloat($(this).parent().find(".cutRetQtyP").val());
		
		if(isNaN(sqtyP)){
		sqtyP=0;
		$(this).parent().find(".sqtyP").val(0);
		}
		if(isNaN(gqtyP)){
		gqtyP=0;
		$(this).parent().find(".gqtyP").val(0);
		}
		if(isNaN(eqtyP)){
		eqtyP=0;
		$(this).parent().find(".eqtyP").val(0);
		}
		if(isNaN(pqtyP)){
		pqtyP=0;
		$(this).parent().find(".pqtyP").val(0);
		}
		if(isNaN(fqtyP)){
		fqtyP=0;
		$(this).parent().find(".fqtyP").val(0);
		}
		if(isNaN(cutRetQtyP)){
		cutRetQtyP=0;
		$(this).parent().find(".cutRetQtyP").val(0);
		}

		tot+=sqtyP+gqtyP+eqtyP+pqtyP+fqtyP+cutRetQtyP;
//alert(sqtyP+"-"+gqtyP+"-"+eqtyP+"-"+pqtyP+"-"+fqtyP);
	});
//	alert(tot);
	
	document.getElementById('divTotal').innerHTML=tot;
}
//----------------------------------------------------------------------------
function loadAllCombo()
{
	//######################
	//####### get values ###
	//######################
	var year 		= $('#cboOrderYear').val();
	var graphicNo 	= $('#cboGraphicNo').val();
	var styleId 	= $('#cboStyle').val();
	var customerPONo= $('#cboPONo').val();
	var orderNo		= $('#cboOrderNo').val();
	var customerId 	= $('#cboCustomer').val();
	
	//######################
	//####### create url ###
	//######################
	var url		= basepath+"fabricDispatchNote-db-get.php?requestType=loadAllComboDetails";
		url	   +="&year="+			year;	
		url	   +="&graphicNo="+		URLEncode(graphicNo);
		url	   +="&styleId="+		URLEncode(styleId);
		url	   +="&customerPONo="+	URLEncode(customerPONo);
		url	   +="&orderNo="+		orderNo;
		url	   +="&customerId="+	customerId;
	
	//######################
	//####### send data  ###
	//######################
	$.ajax({url:url,
			async:false,
			dataType:'json',
			success:function(json){
					$('#cboStyle').html(json.styleNo);
					$('#cboGraphicNo').html(json.graphicNo);
					$('#cboPONo').html(json.customerPoNo);
					$('#cboOrderNo').html(json.orderNo);
					$('#cboSalesOrderNo').html(json.salesOrderNo);
					$('#cboCustomer').html(json.customer);
			}
			});
}
//---------------------------------------------------------------------

function loadOrderNos()
{
	//######################
	//####### get values ###
	//######################
	var year 		= $('#cboOrderYear').val();
	var graphicNo 	= $('#cboGraphicNo').val();
	var styleId 	= $('#cboStyle').val();
	var customerPONo= $('#cboPONo').val();
	var customerId 	= $('#cboCustomer').val();
	var salesOrderNo 	= $('#cboSalesOrderNo').val();
	
	//######################
	//####### create url ###
	//######################
	var url		= basepath+"fabricDispatchNote-db-get.php?requestType=loadOrderNosToSalesOrderNos";
		url	   +="&year="+			year;	
		url	   +="&graphicNo="+		graphicNo;
		url	   +="&styleId="+		styleId;
		url	   +="&customerPONo="+	customerPONo;
		url	   +="&salesOrderNo="+		salesOrderNo;
		url	   +="&customerId="+	customerId;
	
	//######################
	//####### send data  ###
	//######################
	$.ajax({url:url,
			async:false,
			dataType:'json',
			success:function(json){
					$('#cboOrderNo').html(json.orderNo);
			}
			});
}

function saveBarCodeWiseData()
{ 			
		

      	var serialNo 	= 	$('#txtSerialNo').val();
	 	var Year	  	=	$('#txtYear').val();
		var tmpdisNo	=	$('#tmpdisNo').val();
		
		var arrBody="[";
		$('#frmBarcodeWiseDispPopup .barcode .cut_noP').each(function() //loop barcode wise
			{
				
				var cut_no			= $(this).attr('id');;
				var sales_no		= $(this).parent().parent().find(".salesP").attr('id');
				var size			= $(this).parent().parent().find(".sizeP").attr('id');
				var part			= $(this).parent().parent().find(".partP").attr('id');
				var bgColor			= $(this).parent().parent().find(".bgColorP").attr('id');
				var line			= $(this).parent().parent().find(".lineP").attr('id');
				var bcode			= $(this).parent().parent().find(".bc").val();
				var samp_qty		= $(this).parent().parent().find(".sampP").val();
				var good_qty		= $(this).parent().parent().find(".goodP").val();
				var emb_qty			= $(this).parent().parent().find(".embP").val();
				var pd_qty			= $(this).parent().parent().find(".pdP").val();
				var fd_qty			= $(this).parent().parent().find(".fdP").val();
				var cut				= $(this).parent().parent().find(".cutP").val();
				var tot				= $(this).parent().parent().find(".totP").val();
				var remark			= $(this).parent().parent().find("#txtNoteP").val();
				var index_main 		= $(this).parent().parent().find(".indexMP").attr('id');
				
				
				arrBody += "{";
				arrBody += '"cut_no":"'+cut_no+'",' ;
				arrBody += '"sales_no":"'+sales_no+'",' ;
				arrBody += '"size":"'+size+'",' ;
				arrBody += '"bcode":"'+bcode+'",' ;
				arrBody += '"part":"'+part+'",' ;
				arrBody += '"bgColor":"'+bgColor+'",' ;
				arrBody += '"line":"'+line+'",' ;
				arrBody += '"samp_qty":"'+samp_qty+'",' ;
				arrBody += '"good_qty":"'+good_qty+'",' ;
				arrBody += '"emb_qty":"'+emb_qty+'",' ;
				arrBody += '"pd_qty":"'+pd_qty+'",' ;
				arrBody += '"fd_qty":"'+fd_qty+'",' ;
				arrBody += '"cut":"'+cut+'",' ;
				arrBody += '"remark":"'+remark+'" ';
				arrBody += "},";
				
				
    });
				arrBody 		= arrBody.substr(0,arrBody.length-1);
				
				arrBody += " ]";
				
			
			var url		= basepath+"fabricDispatchNote-db-set.php";
			var data    = "requestType=save_bar_code_wise_data_temp&serialNo="+serialNo+"&Year="+Year+"&arrBody="+arrBody+"&tmpdisNo="+tmpdisNo;
	
	var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST', 
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			async:false,
			success:function(json)
			{ 
					//alert(json.tmpdisno)
				$('#tmpdisNo').val(json.tmpdisno);
			}
				});
 		
}

function save_bar_code_val(serialNo,Year,tmpdisNo)
{
	var url		= basepath+"fabricDispatchNote-db-set.php";
	var data    = "requestType=save_bar_code_wise_data&serialNo="+serialNo+"&Year="+Year+"&tmpdisNo="+tmpdisNo;
	
	var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST', 
			data:data,
			async:false,
			success:function(json)
			{
				//alert(json.msg);
				//$('#tmpdisNo').val(json.tmpdisno);
			}
				});
	
}

function loadBarCodeWiseData()
{ 	
	var arrBody  	= "";

      	var serialNo 	= 	$('#txtSerialNo').val();
	 	var Year	  	=	$('#txtYear').val();
		var tmpdisNo	=	$('#tmpdisNo').val();
		
		
		$('#frmBarcodeWiseDispPopup .barcode .cut_noP').each(function() //loop barcode wise
			{
				var cut_no			= $(this).attr('id');;
				var sales_no		= $(this).parent().parent().find(".salesP").attr('id');
				var size			= $(this).parent().parent().find(".sizeP").attr('id');
				var part			= $(this).parent().parent().find(".partP").attr('id');
				var bgColor			= $(this).parent().parent().find(".bgColorP").attr('id');
				var line			= $(this).parent().parent().find(".lineP").attr('id');
				var bcode			= $(this).parent().parent().find(".bc").val();
				var samp_qty		= $(this).parent().parent().find(".sampP").val();
				var good_qty		= $(this).parent().parent().find(".goodP").val();
				var emb_qty			= $(this).parent().parent().find(".embP").val();
				var pd_qty			= $(this).parent().parent().find(".pdP").val();
				var fd_qty			= $(this).parent().parent().find(".fdP").val();
				var cut				= $(this).parent().parent().find(".cutP").val();
				var tot				= $(this).parent().parent().find(".totP").val();
				var remark			= $(this).parent().parent().find("#txtNoteP").val();
				var index_main 		= $(this).parent().parent().find(".indexMP").attr('id');
				
				
				arrBody += "{";
				arrBody += '"cut_no":"'+cut_no+'",' ;
				arrBody += '"sales_no":"'+sales_no+'",' ;
				arrBody += '"size":"'+size+'",' ;
				arrBody += '"bcode":"'+bcode+'",' ;
				arrBody += '"part":"'+part+'",' ;
				arrBody += '"bgColor":"'+bgColor+'",' ;
				arrBody += '"line":"'+line+'",' ;
				arrBody += '"samp_qty":"'+samp_qty+'",' ;
				arrBody += '"good_qty":"'+good_qty+'",' ;
				arrBody += '"emb_qty":"'+emb_qty+'",' ;
				arrBody += '"pd_qty":"'+pd_qty+'",' ;
				arrBody += '"fd_qty":"'+fd_qty+'",' ;
				arrBody += '"cut":"'+cut+'",' ;
				arrBody += '"remark":"'+remark+'" ';
				arrBody += "},";
				
    });
				arrBody 		= arrBody.substr(0,arrBody.length-1);
				var arrBody	= '['+arrBody+']';
			
			var url		= basepath+"fabricDispatchNote-db-set.php";
			var data    = "requestType=load_to_the_main_interface&serialNo="+serialNo+"&Year="+Year
	+"&arrBody="+arrBody+"&tmpdisNo="+tmpdisNo;
			
	var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:data,
			type:'POST', 
			async:false,
			success:function(json)
			{   
					
				$('#tblMain .salesOrderNo').each(function()
				{   
					 $(bundleRow).parent().parent().find(".sqtyP").val(json.sample_qty);
					 $(bundleRow).parent().parent().find(".gqtyP").val(json.good_qty);
					 $(bundleRow).parent().parent().find(".eqtyP").val(json.EM_qty);
					 $(bundleRow).parent().parent().find(".pqtyP").val(json.PD_qty);
					 $(bundleRow).parent().parent().find(".fqtyP").val(json.FD_qty);
					 $(bundleRow).parent().parent().find(".cutRetQtyP").val(json.cutR_qty);
					 $(bundleRow).parent().parent().find(".tot").html(json.tot);
				});
			}
				});
}


//------------------------------------------------------------------------

<?php
//ini_set('display_errors',1);
session_start();
$backwardseperator 	= "../../../../../";
include $backwardseperator."dataAccess/Connector.php";

$location 			= $_SESSION['CompanyID'];
$company 			= $_SESSION['headCompanyId'];
$orderNo  			= $_REQUEST['orderNo'];
$orderYear  		= $_REQUEST['orderYear'];
$serialNo  			= $_REQUEST['serialNo'];
$year 				= $_REQUEST['orderYear'];
$styleNo  			= $_REQUEST['styleNo'];
$graphicNo  		= $_REQUEST['graphicNo'];
$custPONo  			= $_REQUEST['custPONo'];
$sales_no  			= $_REQUEST['sales_no'];
$cut_no  			= $_REQUEST['cut_no'];
$size  				= $_REQUEST['size'];
$indexRow_main		= $_REQUEST['indexRow_main'];
$part				= $_REQUEST['part'];
$bgColor			= $_REQUEST['bgColor'];
$line				= $_REQUEST['line'];
$serialNo			= $_REQUEST['serialNo'];
$Year				= $_REQUEST['Year'];
$tmpdisNo			= $_REQUEST['tmpdisNo'];
$x_salesOrderNo 	= '';


$dispatch_status	=  get_dispatch_status($serialNo,$Year);
$row_select			=  mysqli_fetch_array($dispatch_status);
$status				=  $row_select['intStatus'];
$level				=  $row_select['intApproveLevels'];

if($status <= $level&& $status>0)
{ 
    $display = "display:none";
}
else
{
    $display = "";
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>barcode wise dispatch</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />

</head>

<body>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<form id="frmBarcodeWiseDispPopup" name="frmBarcodeWiseDispPopup" method="post" action="">
  <table width="600" border="0" align="center" bgcolor="#FFFFFF">
  </table>
  <div align="center">
    <div class="trans_layoutD" style="width:800px">
      <div class="trans_text"> Package wise Dispatches</div>
      <table width="650" border="0" align="center"  class="bordered barcode" id="tblPOP">
   <thead>
   <tr>
    <th>Del</th>
   	<th>Barcode</th>
    <th style="display:none;"></th>
    <th style="display:none;">sale_no</th>
   <th style="display:none;">cut_no</th>
   <th style="display:none;">size</th>
   	<th>Samp Qty</th>
   	<th>Good Qty</th>
   	<th>EMB-D Qty</th>
   	<th>P-D Qty</th>
   	<th>F-D Qty</th>
   	<th>Cut Ret Qty</th>
   	<th>Tot Qty</th>
   	<th>Remarks</th>
   	</tr>
   </thead>
   <tbody>
   <?php
   	if($tmpdisNo!=""&&$serialNo!="")
   		$result_tmp = get_bar_code_data($serialNo,$Year,$sales_no,$cut_no,$size);
	else if($tmpdisNo!="")
	 	$result_tmp = get_bar_code_data_tmp($tmpdisNo,$sales_no,$cut_no,$size);
	 $i=0;
		while( $row_select	=  mysqli_fetch_array($result_tmp))
		{  $i++;
		
			$barcode 			= $row_select['BARCODE'];
			$strCutNo 			= $row_select['strCutNo'];
			$intSalesOrderId 	= $row_select['intSalesOrderId'];
			$intPart 			= $row_select['intPart'];
			$intGroundColor 	= $row_select['intGroundColor'];
			$strLineNo 			= $row_select['strLineNo'];
			$strSize 			= $row_select['strSize'];
			$dblSampleQty 		= $row_select['dblSampleQty'];
			$dblGoodQty 		= $row_select['dblGoodQty'];
			$dblEmbroideryQty	= $row_select['dblEmbroideryQty'];
			$dblPDammageQty 	= $row_select['dblPDammageQty'];
			$dblFdammageQty 	= $row_select['dblFdammageQty'];
			$dblCutRetQty 		= $row_select['dblCutRetQty'];
			$strRemarks 		= $row_select['strRemarks'];
			$invoiced 			= $row_select['invoiced'];
			$strReferenceNo 	= $row_select['strReferenceNo']; 
			$totQty				=(float)($dblSampleQty)+(float)($dblGoodQty)+(float)($dblEmbroideryQty)+(float)($dblPDammageQty)+(float)($dblFdammageQty)+(float)($dblCutRetQty);
			
			?>
<tr>
    <td align="center" bgcolor="#FFFFFF" id="" >
   
    
    <img class="delImgp" src="images/del.png" width="15" height="15" style="<?php echo $display; ?>" />
	
	</td>
    <td><input type="text" class="bc" name="bc" id="bc" style="width: 125px"  value="<?php echo $barcode ;?>"></td>
    <td style="display:none;"><input type="hidden" class="indexMP" name="indexM" id="<?php echo $indexRow_main;?>" style="width: 125px" value="<?php echo $indexRow_main;?>"> </td>
    <td style="display:none;"><input type="hidden" class="salesP" name="sales" id="<?php echo $sales_no?>" style="width: 125px" value="<?php echo $sales_no?>"></td>
    <td style="display:none;"><input type="hidden" class="cut_noP" name="cut_no" id="<?php echo $cut_no?>" style="width: 125px" value="<?php echo $cut_no?>" ></td>
    <td style="display:none;"><input type="hidden" class="sizeP" name="size" id="<?php echo $size?>" style="width: 125px" value="<?php echo $size?>" ></td>
    </td>
    <td style="display:none;"><input type="hidden" class="partP" name="part" id="<?php echo $part?>" style="width: 125px" value="<?php echo $part?>" ></td>
    </td>
    <td style="display:none;"><input type="hidden" class="bgColorP" name="bgColor" id="<?php echo $bgColor?>" style="width: 125px" value="<?php echo $bgColor?>" ></td>
    </td>
    <td style="display:none;"><input type="hidden" class="lineP" name="line" id="<?php echo $line?>" style="width: 125px" value="<?php echo $line?>" ></td>
    <td><input type="text" class="sampP" name="number" id="sampP" style="width: 50px" value="<?php echo $dblSampleQty?>"></td>
    <td><input type="text" class="goodP" name="number" id="goodP"  style="width: 50px" value="<?php echo $dblGoodQty?>"></td>
    <td><input type="text" class="embP" name="number" id="embP"  style="width: 50px" value="<?php echo $dblEmbroideryQty?>"></td>
    <td><input type="text" class="pdP" name="number" id="pdP"  style="width: 50px" value="<?php echo $dblPDammageQty?>"></td>
    <td><input type="text" class="fdP" name="number" id="fdP"  style="width: 50px" value="<?php echo $dblFdammageQty?>"></td>
    <td><input type="text" class="cutP" name="number" id="cutP"  style="width: 50px" value="<?php echo $dblCutRetQty?>"></td>
    <td><input type="text" class="totP" name="tot_number" id="totP"  style="width: 50px" value="<?php echo $totQty?>" readonly="readonly"></td>
    <td><textarea name="txtNote" id="txtNoteP" rows="1" style="width:200px"><?php echo $strRemarks?></textarea></td>
</tr>
  <?php
		}
  if ($i==0)
{ 
?>
<tr>
    <td align="center" bgcolor="#FFFFFF" id=""><img class="delImgp" src="images/del.png" width="15" height="15" style="<?php echo $display; ?>"/></td>
    <td><input type="text" class="bc" name="bc" id="bc" style="width: 125px"  value="<?php  ?>"></td>
    <td style="display:none;"><input type="hidden" class="indexMP" name="indexM" id="<?php echo $indexRow_main;?>" style="width: 125px" value="<?php echo $indexRow_main;?>"> </td>
    <td style="display:none;"><input type="hidden" class="salesP" name="sales" id="<?php echo $sales_no?>" style="width: 125px" value="<?php echo $sales_no?>"></td>
    <td style="display:none;"><input type="hidden" class="cut_noP" name="cut_no" id="<?php echo $cut_no?>" style="width: 125px" value="<?php echo $cut_no?>" ></td>
    <td style="display:none;"><input type="hidden" class="sizeP" name="size" id="<?php echo $size?>" style="width: 125px" value="<?php echo $size?>" ></td>
    </td>
    <td style="display:none;"><input type="hidden" class="partP" name="part" id="<?php echo $part?>" style="width: 125px" value="<?php echo $part?>" ></td>
    </td>
    <td style="display:none;"><input type="hidden" class="bgColorP" name="bgColor" id="<?php echo $bgColor?>" style="width: 125px" value="<?php echo $bgColor?>" ></td>
    </td>
    <td style="display:none;"><input type="hidden" class="lineP" name="line" id="<?php echo $line?>" style="width: 125px" value="<?php echo $line?>" ></td>
    <td><input type="text" class="sampP" name="number" id="sampP" style="width: 50px" value="0"></td>
    <td><input type="text" class="goodP" name="number" id="goodP"  style="width: 50px" value="0"></td>
    <td><input type="text" class="embP" name="number" id="embP"  style="width: 50px" value="0"></td>
    <td><input type="text" class="pdP" name="number" id="pdP"  style="width: 50px" value="0"></td>
    <td><input type="text" class="fdP" name="number" id="fdP"  style="width: 50px" value="0"></td>
    <td><input type="text" class="cutP" name="number" id="cutP"  style="width: 50px" value="0"></td>
    <td><input type="text" class="totP" name="tot_number" id="totP"  style="width: 50px" value="<?php ?>" readonly="readonly"></td>
    <td><textarea name="txtNote" id="txtNoteP" rows="1" style="width:200px"><?php ?></textarea></td>
</tr>
	
<?php	
} 
?>

</tbody>
</table>
<td>&nbsp;</td>

<tr>
<td class="tableBorder_allRound"  align="left"> 
<img src="images/Tnew.jpg" width="92" height="24"  alt="Click here to an empty row" id="butAddRow" name="butAddRow" class="mouseover" align="left"></img>
</td>
</tr>

<tr>
    
	<td align="center" class="tableBorder_allRound">
      <img src="images/Tadd.jpg" width="92" height="24"  alt="add" id="butAddB" name="butAddB" class="mouseover"/>
      <img src="images/Tclose.jpg" width="92" height="24" id="butCloseB" name="butCloseB" class="mouseover"  />
    </td>
  
</tr>   
    </table>
    </div>
  </div>
  
<table id ="tbl_div" width=100% style="display:none">
<tr>
<td align="center" bgcolor="#FFFFFF" id=""><img class="delImgp" src="images/del.png" width="15" height="15" style="<?php echo $display; ?>"/></td>
<td><input type="text" class="bc" name="bc" id="bc" style="width: 125px"  value="<?php  ?>"></td>
<td style="display:none;"><input type="hidden" class="indexMP" name="indexM" id="<?php echo $indexRow_main;?>" style="width: 125px" value="<?php echo $indexRow_main;?>"> </td>
<td style="display:none;"><input type="hidden" class="salesP" name="sales" id="<?php echo $sales_no?>" style="width: 125px" value="<?php echo $sales_no?>"></td>
<td style="display:none;"><input type="hidden" class="cut_noP" name="cut_no" id="<?php echo $cut_no?>" style="width: 125px" value="<?php echo $cut_no?>" ></td>
<td style="display:none;"><input type="hidden" class="sizeP" name="size" id="<?php echo $size?>" style="width: 125px" value="<?php echo $size?>" ></td>
</td>
<td style="display:none;"><input type="hidden" class="partP" name="part" id="<?php echo $part?>" style="width: 125px" value="<?php echo $part?>" ></td>
</td>
<td style="display:none;"><input type="hidden" class="bgColorP" name="bgColor" id="<?php echo $bgColor?>" style="width: 125px" value="<?php echo $bgColor?>" ></td>
</td>
<td style="display:none;"><input type="hidden" class="lineP" name="line" id="<?php echo $line?>" style="width: 125px" value="<?php echo $line?>" ></td>
<td><input type="text" class="sampP" name="number" id="sampP" style="width: 50px" value="0"></td>
<td><input type="text" class="goodP" name="number" id="goodP"  style="width: 50px" value="0"></td>
<td><input type="text" class="embP" name="number" id="embP"  style="width: 50px" value="0"></td>
<td><input type="text" class="pdP" name="number" id="pdP"  style="width: 50px" value="0"></td>
<td><input type="text" class="fdP" name="number" id="fdP"  style="width: 50px" value="0"></td>
<td><input type="text" class="cutP" name="number" id="cutP"  style="width: 50px" value="0"></td>
<td><input type="text" class="totP" name="tot_number" id="totP"  style="width: 50px" value="<?php ?>" readonly="readonly"></td>
<td><textarea name="txtNote" id="txtNoteP" rows="1" style="width:200px"><?php ?></textarea></td>
</tr>
</table>

</form>
</body>
</html>
<?php
//--------------------function------------------------------------------------
//---------------------get from permanent table----------------------------------
function get_bar_code_data($serialNo,$Year,$sales_no,$cut_no,$size)
{
	global $db;
	$sql = "select * from 
	ware_fabricdispatchdetails_barcode_wise 
	where 
	ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNo =$serialNo 
	AND ware_fabricdispatchdetails_barcode_wise.intBulkDispatchNoYear = $Year
	AND ware_fabricdispatchdetails_barcode_wise.strCutNo ='$cut_no'
	AND ware_fabricdispatchdetails_barcode_wise.intSalesOrderId = $sales_no
	AND ware_fabricdispatchdetails_barcode_wise.strSize = '$size'";	
	//echo $sql;
	$result 	=	$db->RunQuery($sql);
	return $result;
	
}

//-------------------get tmp table--------------------------------
function get_bar_code_data_tmp($tmpdisNo,$sales_no,$cut_no,$size)
{	
	global $db;
	$sql = "select 
	* 
	from temp_dispatch_barcode_wise 
	where 
	temp_dispatch_barcode_wise.ID = $tmpdisNo
	AND temp_dispatch_barcode_wise.strCutNo = '$cut_no'
	AND temp_dispatch_barcode_wise.intSalesOrderId =$sales_no
	AND temp_dispatch_barcode_wise.strSize = '$size'
	";	
	//echo $sql;
	$result 		=	$db->RunQuery($sql);
	return $result;
}

function get_dispatch_status($serialNo,$Year)
{  
		
	global $db;
	$sql_status = "SELECT
	ware_fabricdispatchheader.intApproveLevels,
	ware_fabricdispatchheader.intStatus
	FROM
	ware_fabricdispatchheader
	WHERE
	ware_fabricdispatchheader.intBulkDispatchNo = $serialNo  AND
	ware_fabricdispatchheader.intBulkDispatchNoYear = $Year";
	
	$result 		=	$db->RunQuery($sql_status);
	return $result;
	
	
}
?>
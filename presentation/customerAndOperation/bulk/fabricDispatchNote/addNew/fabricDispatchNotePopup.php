<?php
ini_set('display_errors',0);
session_start();
$backwardseperator 	= "../../../../../";
include $backwardseperator."dataAccess/Connector.php";

$location 			= $_SESSION['CompanyID'];
$company 			= $_SESSION['headCompanyId'];
$orderNo  			= $_REQUEST['orderNo'];
$orderYear  		= $_REQUEST['orderYear'];
$serialNo  			= $_REQUEST['serialNo'];
$year 				= $_REQUEST['orderYear'];
$styleNo  			= $_REQUEST['styleNo'];
$graphicNo  		= $_REQUEST['graphicNo'];
$custPONo  			= $_REQUEST['custPONo'];
$salesOrderNo  		= $_REQUEST['salesOrderNo'];
$locationType 		= $_REQUEST['locationType'];
$STICKER_MAKING      = '-STICKER_MAKING';
$x_salesOrderNo 	= '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Items</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />

</head>

<body>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<form id="frmFabricRecvNotePopup" name="frmFabricRecvNotePopup" method="post" action="">
  <table width="500" border="0" align="center" bgcolor="#FFFFFF">
  </table>
  <div align="center">
    <div class="trans_layoutD" style="width:720px">
      <div class="trans_text"> Allocation</div>
      <table width="650" border="0" align="center" bgcolor="#FFFFFF">
        
          <td><table width="600" border="0">
              <tr>
                <td><table width="650" border="0" cellpadding="0" cellspacing="0" class="tableBorder">
                    <tr>
                      <td width="3%" height="22" class="normalfnt">&nbsp;</td>
                      <td width="16%" class="normalfnt">Year</td>
                      <td width="2%" class="normalfnt">: </td>
                      <td width="27%" class="normalfnt"><div id="itemMain"><?php echo $year  ;
 ?></div></td>
                      <td width="2%" align="right">&nbsp;</td>
                      <td width="2%" align="right"><div id="itemIdMain" style="display:none"><?php echo $itemId; ?></div></td>
                      <td width="16%" class="normalfnt">Order No</td>
                      <td width="2%" class="normalfnt">: </td>
                      <td width="26%" class="normalfnt"><div id="itemMain"><?php echo $orderNo  ;
 ?></div></td>
                      <td width="4%"></td>
                    </tr>
                    <tr id="rw1">
                      <td width="3%" height="22" class="normalfnt">&nbsp;</td>
                      <td width="16%" class="normalfnt">Customer PO</td>
                      <td width="2%" class="normalfnt">: </td>
                      <td width="27%" class="normalfnt"><div id="balToInvoice"><?php echo $custPONo;

 ?></div></td>
                      <td width="2%" class="normalfnt"></td>
                      <td width="2%" align="right">&nbsp;</td>
                      <td width="16%" class="normalfnt">&nbsp;</td>
                      <td width="2%" class="normalfnt">&nbsp;</td>
                      <td width="26%" class="normalfnt"><div id="itemMain"></div></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td height="22" class="normalfnt">&nbsp;</td>
                      <td class="normalfnt">Style No</td>
                      <td class="normalfnt">&nbsp;</td>
                      <td class="normalfnt"><select name="cboStyleP" style="width:120px" id="cboStyleP" class="searchP">
                          <?php
					 $sql = "SELECT DISTINCT 
							trn_orderdetails.strStyleNo
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
							WHERE 
							trn_orderheader.intStatus = '1' AND 
							trn_orderdetails.intOrderNo='$orderNo' 
							AND trn_orderdetails.intOrderYear='$orderYear'
							ORDER BY 
							trn_orderdetails.strSalesOrderNo ASC";
			
					$html = "<option value=\"\"></option>";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{		
						 
							$html .= "<option value=\"".$row['strStyleNo']."\">".$row['strStyleNo']."</option>";
 					}
					echo $html;
				?>
                        </select></td>
                      <td class="normalfnt"></td>
                      <td align="right">&nbsp;</td>
                      <td class="normalfnt">Graphic</td>
                      <td class="normalfnt">&nbsp;</td>
                      <td class="normalfnt"><select name="cboGraphicP" style="width:120px" id="cboGraphicP" class="searchP">
                          <?php
					 $sql = "SELECT DISTINCT 
							trn_orderdetails.strGraphicNo
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
							WHERE 
							trn_orderheader.intStatus = '1' AND 
							trn_orderdetails.intOrderNo='$orderNo' 
							AND trn_orderdetails.intOrderYear='$orderYear'
							ORDER BY 
							trn_orderdetails.strGraphicNo ASC";
			
					$html = "<option value=\"\"></option>";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{		
						 
							$html .= "<option value=\"".$row['strGraphicNo']."\">".$row['strGraphicNo']."</option>";
 					}
					echo $html;
				?>
                        </select></td>
                      <td></td>
                    </tr>
                    <tr id="rw1">
                      <td width="3%" height="22" class="normalfnt">&nbsp;</td>
                      <td width="16%" class="normalfnt">Sales Order No</td>
                      <td width="2%" class="normalfnt">&nbsp;</td>
                      <td width="27%" class="normalfnt"><select name="cboSalesOrderNoP" style="width:120px" id="cboSalesOrderNoP" class="searchP">
                          <?php
					 $sql = "SELECT DISTINCT 
							trn_orderdetails.strSalesOrderNo,
							trn_orderdetails.SO_TYPE
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
							WHERE 
							trn_orderheader.intStatus = '1' AND 
							trn_orderdetails.intOrderNo='$orderNo' 
							AND trn_orderdetails.intOrderYear='$orderYear' ";
					 if ($locationType ==2){
                         $sql.= "AND trn_orderdetails.SO_TYPE > -1 ";
                     }
					 $sql.= "ORDER BY 
							trn_orderdetails.strSalesOrderNo ASC";
			
					$html = "<option value=\"\"></option>";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
                        $soName = $row['strSalesOrderNo'];
                        if ($row['SO_TYPE'] == 2){
                            $soName = $soName.$STICKER_MAKING;
                        }
						if($row['strSalesOrderNo']==$salesOrderNo){
							$html .= "<option value=\"".$row['strSalesOrderNo']."\" selected=\"selected\">".$soName."</option>";
						}
						else{
							$html .= "<option value=\"".$row['strSalesOrderNo']."\">".$soName."</option>";
						}
					}
					echo $html;
				?>
                        </select></td>
                      <td width="2%" class="normalfnt"></td>
                      <td width="2%" align="right">&nbsp;</td>
                      <td width="16%" class="normalfnt">Line No</td>
                      <td width="2%" class="normalfnt">&nbsp;</td>
                      <td width="26%" class="normalfnt"><div id="itemMain">
                          <select name="cboLineNo" style="width:120px" id="cboLineNo" class="">
                            <?php
		$sql = "SELECT DISTINCT 
				trn_orderdetails.strLineNo
				FROM
				trn_orderdetails
				Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
				WHERE 
				trn_orderheader.intStatus = '1' AND 
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear'  ";
				if($salesOrderNo!=''){
				$sql .= " AND trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'";
				}
				$sql .= " 
				ORDER BY trn_orderdetails.strLineNo ASC"; 
				
					$html = "<option value=\"\"></option>";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{		
						$html .= "<option value=\"".$row['strLineNo']."\">".$row['strLineNo']."</option>";
					}
					echo $html;
				?>
                          </select>
                        </div></td>
                      <td></td>
                    </tr>
                    <tr id="rw1">
                      <td width="3%" height="22" class="normalfnt">&nbsp;</td>
                      <td width="15%" class="normalfnt">Part No</td>
                      <td width="2%" class="normalfnt">: </td>
                      <td width="29%" class="normalfnt"><select name="cboPartNo" style="width:120px" id="cboPartNo" class="">
                          <?php
                    $sql = "SELECT DISTINCT
                            trn_orderdetails.intPart,
                            mst_part.strName
                            FROM
                            trn_orderdetails
                            Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
							WHERE
							trn_orderheader.intStatus =  '1' AND 
                            trn_orderdetails.intOrderNo =  '$orderNo' AND
                            trn_orderdetails.intOrderYear =  '$orderYear'   ";
							if($salesOrderNo!=''){
							$sql .= " AND trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'";
							}
							$sql .= " 
							ORDER BY mst_part.strName asc"; 
                             
                    $html = "<option value=\"\"></option>";
                    $result = $db->RunQuery($sql);
                    while($row=mysqli_fetch_array($result))
                    {
                            $html .= "<option value=\"".$row['intPart']."\">".$row['strName']."</option>";
                    }
					echo $html;
            ?>
                        </select></td>
                      <td width="1%" class="normalfnt"></td>
                      <td width="1%" align="right">&nbsp;</td>
                      <td width="14%" class="normalfnt">Size</td>
                      <td width="2%" class="normalfnt">&nbsp;</td>
                      <td width="28%" class="normalfnt"><div id="itemMain">
                          <select name="cboSizes" style="width:120px" id="cboSizes" class="">
                            <?php
				$sql = "SELECT DISTINCT
						trn_ordersizeqty.strSize
						FROM
						trn_orderdetails
						Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId 
						Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
						WHERE
						trn_orderheader.intStatus =  '1' AND 
						trn_orderdetails.intOrderNo =  '$orderNo' AND
						trn_orderdetails.intOrderYear =  '$orderYear'   ";
						if($salesOrderNo!=''){
						$sql .= " AND trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'";
						}
						$sql .= " 
						ORDER BY trn_ordersizeqty.strSize ASC"; 
				$html = "<option value=\"\"></option>";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
						$html .= "<option value=\"".$row['strSize']."\">".$row['strSize']."</option>";
				}
				echo $html;
			   ?>
                          </select>
                        </div></td>
                      <td></td>
                    </tr>
                    <tr id="rw1">
                      <td width="3%" height="22" class="normalfnt">&nbsp;</td>
                      <td width="15%" class="normalfnt">Cut No</td>
                      <td width="2%" class="normalfnt">: </td>
                      <td width="29%" class="normalfnt"><select name="cboCutNo" style="width:120px" id="cboCutNo" class="">
                          <?php
				$sql = "SELECT DISTINCT   
						ware_fabricreceiveddetails.strCutNo
						FROM
						ware_fabricreceivedheader
						Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear 
						Inner Join trn_orderdetails ON ware_fabricreceivedheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricreceivedheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_fabricreceiveddetails.intSalesOrderId = trn_orderdetails.intSalesOrderId AND ware_fabricreceiveddetails.strLineNo = trn_orderdetails.strLineNo AND ware_fabricreceiveddetails.intPart = trn_orderdetails.intPart
						WHERE
						ware_fabricreceivedheader.intOrderNo =  '$orderNo' AND
						ware_fabricreceivedheader.intOrderYear =  '$orderYear'   ";
						if($salesOrderNo!=''){
						$sql .= " AND trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'";
						}
						$sql .= " 
						ORDER BY ware_fabricreceiveddetails.strCutNo ASC"; 
				$html = "<option value=\"\"></option>";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
						$html .= "<option value=\"".$row['strCutNo']."\">".$row['strCutNo']."</option>";
				}
				echo $html;
			   ?>
                        </select></td>
                      <td width="1%" class="normalfnt"></td>
                      <td width="1%" align="right">&nbsp;</td>
                      <td width="14%" class="normalfnt">Color</td>
                      <td width="2%" class="normalfntRight"></td>
                      <td width="28%" class="normalfntLeft"><select name="cboColor" style="width:120px" id="cboColor" class="">
                          <?php
		$sql="SELECT DISTINCT  
			trn_sampleinfomations_details.intGroundColor,
			mst_colors_ground.strName as bgColor 
			FROM
			trn_orderdetails
			Inner Join trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
			Inner Join trn_sampleinfomations_details ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_details.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_details.intRevNo AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
			left Join mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
			WHERE
			trn_orderdetails.intOrderNo =  '$orderNo' AND
			trn_orderdetails.intOrderYear =  '$orderYear'"; 
			if($salesOrderNo!=''){
			$sql .= " AND 
					trn_orderdetails.strSalesOrderNo='$salesOrderNo'"; 
			}
			$sql .= " ORDER BY 
					mst_colors_ground.strName ASC ";
				$html = "<option value=\"\"></option>";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
						$html .= "<option value=\"".$row['intGroundColor']."\">".$row['bgColor']."</option>";
				}
				echo $html;
			   ?>
                        </select></td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td height="22" class="normalfnt">&nbsp;</td>
                      <td class="normalfnt">View 0 Balances</td>
                      <td width="2%" class="normalfnt">: </td>
                      <td class="normalfnt"><input type="checkbox" name="chkZeeroBal" id="chkZeeroBal" checked="checked" /></td>
                      <td class="normalfnt"></td>
                      <td align="right">&nbsp;</td>
                      <td class="normalfnt">&nbsp;</td>
                      <td class="normalfntRight"></td>
                      <td class="normalfntLeft">&nbsp;</td>
                      <td><span class="normalfntLeft"><img src="images/smallSearch.png" width="24" height="24" class="mouseover" id="imgSearchItems" /></span></td>
                    </tr>
                  </table></td>
              </tr>
              <?php //echo $pp
	  ?>
              <tr>
                <td><div style="width:700px;height:300px;overflow:scroll" >
                    <table width="679" class="bordered" id="tblPopup" >
                      <tr class="">
                        <th width="4%" ><input type="checkbox" name="chkAll" id="chkAll" /></th>
                        <th width="7%" >Cut No</th>
                        <th width="16%" >Sales Order No</th>
                        <th width="9%" >Part</th>
                        <th width="21%" >Back Ground Color</th>
                        <th width="12%">Line No</th>
                        <th width="9%">Size</th>
                        <th width="13%">Qty</th>
                        <th width="13%"> Bal Disp Qty</th>
                      </tr>
                    </table>
                  </div></td>
              </tr>
              
                <td align="center" class="tableBorder_allRound"><img src="images/Tadd.jpg" width="92" height="24"  alt="add" id="butAdd" name="butAdd" class="mouseover"/><img src="images/delete.png" width="92" height="24"  alt="add" id="butDelete" name="butDelete" class="mouseover" style="display:none"/><img src="images/Tclose.jpg" width="92" height="24" id="butClose1" name="butClose1" class="mouseover"  /></td>
              </tr>
            </table></td>
        </tr>
      </table>
    </div>
  </div>
</form>
</body>
</html>

<?php
$backwardseperator = "../../../../../";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BULK COSTING</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
<form id="frmCompanyDetails" name="frmCompanyDetails" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">BULK COSTING</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td colspan="2" rowspan="3" class="normalfnt"><table width="94%" border="0" class="tableBorder_allRound" bgcolor="#E4F9FC">
              <tr>
                <td width="47%" height="22" class="normalfnt">Style No</td>
                <td width="53%"><select name="select" id="select" style="width:140px">
                </select></td>
              </tr>
              <tr>
                <td class="normalfnt">Order No</td>
                <td align="left"><select name="select7" id="select7" style="width:140px">
                </select></td>
              </tr>
              <tr>
                <td height="19" class="normalfnt">Print Name</td>
                <td><select name="select8" id="select8" style="width:140px">
                </select></td>
              </tr>
              </table></td>
            <td colspan="2" rowspan="7" align="center" class="tableBorder_allRound"><img src="../../../../../images/sample_garment.png" width="100" height="92" /></td>
            <td width="5%" height="22" class="normalfnt">&nbsp;</td>
            <td width="12%" class="normalfnt">Graphic Size <span class="normalfntGrey">(Inch)</span></td>
            <td width="18%" align="left"><table width="100%" border="0">
              <tr class="normalfnt">
                <td width="19%" align="center">W</td>
                <td width="25%"><input name="textfield2" type="text" disabled="disabled" id="textfield2" style="width:30px" /></td>
                <td width="15%" align="center">H</td>
                <td width="41%"><input name="textfield3" type="text" disabled="disabled" id="textfield3" style="width:30px" /></td>
              </tr>
            </table></td>
            </tr>
          <tr>
            <td class="normalfnt">&nbsp;</td>
            <td height="19" class="normalfnt">Graphic Placement</td>
            <td><input name="textfield" type="text" disabled="disabled" id="textfield" style="width:140px" /></td>
            </tr>
          <tr>
            <td height="19" class="normalfnt">&nbsp;</td>
            <td width="12%" class="normalfnt">Combo</td>
            <td width="18%" align="left"><select disabled="disabled" name="select2" id="select2" style="width:140px">
            </select></td>
            </tr>
          <tr>
            <td width="17%" height="22" class="normalfnt">Order Qty</td>
            <td width="22%" class="normalfntGrey"> (Auto)</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Technique</td>
            <td align="left"><select disabled="disabled" name="select3" id="select3" style="width:140px">
            </select></td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">Wash conditions</td>
            <td><select disabled="disabled" name="select5" id="select5" style="width:140px">
              </select></td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Print process</td>
            <td align="left"><select disabled="disabled" name="select4" id="select4" style="width:140px">
            </select></td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">Qty</td>
            <td><input type="text" name="textfield5" id="textfield5" style="width:50px" /></td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Fabric</td>
            <td align="left"><input name="textfield4" type="text" disabled="disabled" id="textfield4" style="width:140px" /></td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">Type</td>
            <td><select name="select6" id="select6" style="width:140px">
              <option>Sample</option>
              <option>1st Day</option>
            </select></td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">No of Colors</td>
            <td align="left"><input name="textfield10" type="text" disabled="disabled" id="textfield10" style="width:30px" /></td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2" align="center">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td align="right"><img src="../../../../../images/Tadd.jpg" width="92" height="24" /></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="grid" >
            <tr class="gridHeader">
              <td width="6%" height="22" >Del</td>
              <td width="17%" >Main Category</td>
              <td width="21%" >Sub Category</td>
              <td width="21%" >Item Description</td>
              <td width="7%">UOM</td>
              <td width="12%"> Consumption</td>
              <td width="8%">Total Qty</td>
              <td width="8%">Stock</td>
              </tr>
            <tr class="normalfnt">
              <td align="center" bgcolor="#FFFFFF"><img src="../../../../../images/del.png" width="15" height="15" /></td>
              <td bgcolor="#FFFFFF">&nbsp;</td>
              <td bgcolor="#FFFFFF">&nbsp;</td>
              <td bgcolor="#FFFFFF">&nbsp;</td>
              <td bgcolor="#FFFFFF">&nbsp;</td>
              <td bgcolor="#FFFFFF"><input name="textfield7" type="text" id="textfield7" style="width:90px" value="Load from sample" /></td>
              <td bgcolor="#FFFFFF">&nbsp;</td>
              <td bgcolor="#FFFFFF"><a href="#">Allocate</a></td>
              </tr>
            <tr class="normalfnt">
              <td align="center" bgcolor="#FFFFFF"><img src="../../../../../images/del.png" width="15" height="15" /></td>
              <td bgcolor="#FFFFFF">&nbsp;</td>
              <td bgcolor="#FFFFFF">&nbsp;</td>
              <td bgcolor="#FFFFFF">&nbsp;</td>
              <td bgcolor="#FFFFFF">&nbsp;</td>
              <td bgcolor="#FFFFFF">&nbsp;</td>
              <td bgcolor="#FFFFFF">&nbsp;</td>
              <td bgcolor="#FFFFFF"><a href="#">Purchase</a></td>
            </tr>
            </table>
          </div></td>
      </tr>
      <tr>
        <td align="center" class="">&nbsp;</td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><img src="../../../../../images/Tnew.jpg" width="92" height="24" /><img src="../../../../../images/Tsave.jpg" width="92" height="24" /><img src="../../../../../images/Tconfirm.jpg" width="92" height="24" /><img src="../../../../../images/Treport.jpg" width="92" height="24" /><img src="../../../../../planning/img/Tclose.jpg" width="92" height="24" /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>

<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
date_default_timezone_set('Asia/Kolkata');

include_once "libraries/jqgrid2/inc/jqgrid_dist.php";

$company = $sessions->getCompanyId();
$location=$sessions->getLocationId();

$programName	='Bulk Daily Production';
$programCode	='P1285';

$menuSpecialId	='21';

$reportMenuId	='1287';
$menuId			='1285';
$intUser  		= $_SESSION["userId"];

//$ProductionDate 	= (!isset($_REQUEST["date"])?date("Y-m-d"):$_REQUEST["date"]);
//$location           = (!isset($_REQUEST["cboLocation"])?'':$_REQUEST["cboLocation"]);

//$userDepartment	=getUserDepartment($intUser);
//$approveLevel 	= (int)getMaxApproveLevel();
//--------------------------------------------------------------------------------

//$sql1 = "SELECT intId,strName FROM 
//			mst_companies
//			ORDER BY
//			mst_companies.strName ASC";
//$result1 = $db-> RunQuery($sql1);
//
//$str1 = "";
////$str1   = ":All                                                                                                               ;" ;
//$j = 0;
//while ($row = mysqli_fetch_array($result1)) {
//    if ($company == $row['intId']) {
//        $inilocId = $row['intId'];
//        $inilocation = $row['strName'];
//    } else {
//        $locId = $row['intId'];
//        $location = $row['strName'];
//        $str1 .= $location . ":" . $location . ";";
//    }
//    $j++;
//}
//$str2 = $inilocation . ":" . $inilocation . ";";
//$str = $str2 . $str1;
//$str .= ":All";
$sql1 = " SELECT mst_locations.intId AS locationId,
							concat(mst_companies.strName ,' - ',mst_locations.strName) AS company
							FROM
							mst_locations							
							Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
							WHERE
							mst_locations.intStatus=1 ORDER BY mst_companies.strName ASC";
$result1 = $db-> RunQuery($sql1);

$str1 = "";
//$str1   = ":All                                                                                                               ;" ;
$j = 0;
while ($row = mysqli_fetch_array($result1)) {
    if ($location == $row['locationId']) {
        $inilocId = $row['locationId'];
        $inilocation = $row['company'];
    } else {
        $locId = $row['locationId'];
        $location1 = $row['company'];
        $str1 .= $location1 . ":" . $location1 . ";";
    }
    $j++;
}
$str2 = $inilocation . ":" . $inilocation . ";";
$str = $str2 . $str1;
$str .= ":All";


//----------Date----------------------------------------------------------------------
$str_date = "" ;
$sql1 = "SELECT DISTINCT
	dateOfProduction
FROM
	trn_bulkdailyproduction
WHERE
	intSavedStatus = 1";
$result1 = $db->RunQuery($sql1);
$dt   = "" ;
$date = array("All");
while($row=mysqli_fetch_array($result1))
{
    array_push($date,$row['dateOfProduction']);
}
foreach ($date AS $d){
    $date2 = $d;
    if($s != "All"){
        $date1 = $d;
    }
    else{
        $date1 = '';
    }
    $dt  .= $date1.":".$date2.";" ;
}
$str_date = $dt;
$str_date .= ":" ;
//------------------------------------------------------------------------------------------


///// create where part ///////////////////
$arr =  json_decode($_REQUEST['filters'],true);

$arr = $arr['rules'];
$where_string = '';
$where_array = array(

        'company'=>'concat(mst_companies.strName ' - 'mst_locations.strName)',
        'Date'=>'tb1.dateOfProduction',
        'section'=>'mst_section.strName',


    );

foreach ($arr as $k => $v) {
    if ($where_array[$v['field']]) {
        $where_string .= "AND  " . $where_array[$v['field']] . " like '%" . $v['data'] . "%' ";
    }
}

if (!count($arr) > 0) {
//    $where_string_date = " AND trn_bulkdailyproduction.dateOfProduction = '" . date('Y-m-d') . "'";
//    $where_string_comp = " AND trn_bulkdailyproduction.strGraphicNo = '$inilocation'";
//
//
    $where_string .= " AND tb1.productionLocation = '" . $inilocId. "'";
//    $where_string .= " AND tb1.dateOfProduction = '". date('Y-m-d') ."'";
    $where_string .= " AND 1=1 ";
}
//else {
//    foreach ($arr as $k => $v) {
//        if ($v['field'] == 'company') {
//            $where_string_comp = " AND trn_bulkdailyproduction.productionLocation = '" . $v['data'] . "'";
//
//        } else if ($v['field'] == '')
//            $where_string_date = " AND trn_bulkdailyproduction.dateOfProduction= '" . $v['data'] . "'";
//    }
//
//}

################## end code ####################################

$sql = "select * from(SELECT DISTINCT  
							tb1.strGraphicNo as Graphic_No,
							concat(mst_companies.strName ,' - ',mst_locations.strName) AS company,
							tb1.dateOfProduction as Date,
							tb1.intOrderNo as Order_No,
                                                        tb1.intOrderYear AS Order_Year,
							tb1.intBulkProductionNo as Bulk_No,
							tb1.intBulkProductionYear as Bulk_Year,
							tb1.intCustomer,
							mst_customer.strName as Customer,                                                        
							mst_module.strName as module,
							tb1.intSection,
                                                        mst_section.strName as section,
                                                        tb1.strSalesOrderNo as salesorderno,
                                                        tb1.strStyleNo as styleno,
                                                        tb1.strPrintName as print,
                                                        IF (tb1.intPrintColor =- 1,'All',concat(IF (tb1.intPrintColor =- 10,'N/A',mst_colors.strName),'-',
                                                        IF (tb1.intInkTypeId =- 10,'N/A',mst_inktypes.strName))) AS color,
                                                        tb1.intTodayProductionQty as qty,
                                                        tb1.intUser,
                                                        sys_users.strUserName as user,
                                                        
                                                        
                                                        ";
				
$sql .= "'View' as `View`   
						FROM
							(select * from trn_bulkdailyproduction) as tb1 			
							INNER JOIN mst_locations ON tb1.productionLocation = mst_locations.intId						
							Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
							Inner Join trn_orderdetails ON tb1.strGraphicNo = trn_orderdetails.strGraphicNo 
							Inner Join mst_customer ON tb1.intCustomer=mst_customer.intId
                                                        INNER JOIN mst_section ON tb1.intSection = mst_section.intId
                                                        INNER JOIN mst_module ON tb1.strModuleCode = mst_module.intId
                                                        LEFT JOIN mst_colors ON mst_colors.intId = tb1.intPrintColor
                                                        LEFT JOIN mst_inktypes ON mst_inktypes.intId = tb1.intInkTypeId
                                                        INNER JOIN sys_users ON sys_users.intUserId = tb1.intUser
                                                        
						   
							WHERE 1=1  $where_string 
							
							)  as t where 1=1  
						";
//echo $sql;


$formLink = "?q=$menuId&bulkNo={Bulk_No}&year={Bulk_Year}";
$reportLink = "?q=$reportMenuId&bulkNo={Bulk_No}&year={Bulk_Year}";

$col = array();



$col["title"] = "Company"; // caption of column
$col["name"] = "company"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";
$col["stype"] 	= "select";
$str = $str ;
$col["editoptions"] 	= array("value"=> $str);
$cols[] = $col;	$col=NULL;


//Production  Date
$col["title"] ="Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;
$col=NULL;


/////////////////////////////////////////////////Listing/////////////////////////////////////////////////////////////////////////////////
//Section
$col["title"] = "Section"; // caption of column
$col["name"] = "section"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Section
$col["title"] = "Module Code"; // caption of column
$col["name"] = "module"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//Graphic No
$col["title"] = "Graphic No"; // caption of column
$col["name"] = "Graphic_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Customer
$col["title"] = "Customer"; // caption of column
$col["name"] = "Customer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Bulk Production No
$col["title"] = "Bulk Production No"; // caption of column
$col["name"] = "Bulk_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";
$col['link'] = "?q=1285&bulkNo={Bulk_No}&bulkYear={Bulk_Year}";
$col["linkoptions"] = "target='bulkDailyProduction.php'"; 
$cols[] = $col;	$col=NULL;

//Bulk Production Year
$col["title"] = "Bulk Production Year"; // caption of column
$col["name"] = "Bulk_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//Order No
$col["title"] = "Order No"; // caption of column
$col["name"] = "Order_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//order year
$col["title"] = "Order Year"; // caption of column
$col["name"] = "Order_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";

$cols[] = $col;	$col=NULL;

//salesorderNO
$col["title"] = "Sales Order No"; // caption of column
$col["name"] = "salesorderno"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";

$cols[] = $col;	$col=NULL;

//styleNo
$col["title"] = "Style No"; // caption of column
$col["name"] = "styleno"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";

$cols[] = $col;	$col=NULL;

//styleNo
$col["title"] = "Print"; // caption of column
$col["name"] = "print"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//styleNo
$col["title"] = "Print Color - Ink Type"; // caption of column
$col["name"] = "color"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//styleNo
$col["title"] = "Production Qty"; // caption of column
$col["name"] = "qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//styleNo
$col["title"] = "Entered By"; // caption of column
$col["name"] = "user"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";

$cols[] = $col;	$col=NULL;

//Report
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='bulkDailyProductionListing.php'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;




$jq = new jqgrid('',$db);

$grid["caption"] 		= "BulkProduction Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'Date'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes

$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(
        "add"=>false, // allow/disallow add
        "edit"=>false, // allow/disallow edit
        "delete"=>false, // allow/disallow delete
        "rowactions"=>false, // show/hide row wise edit/del/save option
        "search" => "advance", // show single/multi field search condition (e.g. simple or advance)
        "export"=>true
    )
);

$out = $jq->render("list1");

?>


<title>Bulk Production Listing</title>
<?php echo $out ?>




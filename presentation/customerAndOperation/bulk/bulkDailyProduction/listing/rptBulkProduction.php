<?php (define('UNLOCKPAGE', true) ? die('<<< Access denied >>>') : ''); ?>
<?php
$companyId = $_SESSION['CompanyID'];
$intUser = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath = $_SERVER['PHP_SELF'];
$bulkProductionUrl = "?q=1285";

$bulkProductionNo = $_REQUEST['bulkNo'];
$year = $_REQUEST['year'];


$approveMode = $_REQUEST['approveMode'];
$mode = $_REQUEST['mode'];

$programName = 'Bulk Daily Production';
$programCode = 'P1285';
$spMenuId = 76;

$rowcount1 = 0;
$sql = "  SELECT
	mst_module.strName AS module,
	CONCAT(
		mst_locations.strName,
		'-',
		mst_companies.strName
	) AS location,
	trn_bulkdailyproduction.intOrderNo,
	trn_bulkdailyproduction.intOrderYear,
	trn_bulkdailyproduction.strGraphicNo,
	trn_orderdetails.strSalesOrderNo,
	trn_bulkdailyproduction.strStyleNo,
	trn_bulkdailyproduction.strCombo AS combo,
	trn_bulkdailyproduction.intShots,
	trn_bulkdailyproduction.noOfUps,
	trn_bulkdailyproduction.intTotalFabricInQty,
	trn_bulkdailyproduction.intTodayProductionQty,
	sys_users.strUserName AS userName,
	mst_customer.strName AS customer,
	trn_bulkdailyproduction.dateOfProduction,
	mst_section.strName as section,
        if(trn_bulkdailyproduction.intPrintColor=-1,'All',CONCAT(mst_colors.strName,'-',mst_inktypes.strName))AS color,
        trn_bulkdailyproduction.productionLocation as locationId,
        trn_bulkdailyproduction.strPrintName AS print
FROM
	trn_bulkdailyproduction
INNER JOIN mst_locations ON mst_locations.intId = trn_bulkdailyproduction.productionLocation
INNER JOIN mst_companies ON mst_companies.intId = mst_locations.intCompanyId
INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_bulkdailyproduction.intOrderNo
AND trn_orderdetails.intOrderYear = trn_bulkdailyproduction.intOrderYear
LEFT JOIN mst_colors ON mst_colors.intId = trn_bulkdailyproduction.intPrintColor
INNER JOIN sys_users ON sys_users.intUserId = trn_bulkdailyproduction.intUser
INNER JOIN mst_customer ON mst_customer.intId = trn_bulkdailyproduction.intCustomer
INNER JOIN mst_section ON mst_section.intId = trn_bulkdailyproduction.intSection
INNER JOIN mst_module ON mst_module.intId = trn_bulkdailyproduction.strModuleCode
LEFT JOIN mst_inktypes ON mst_inktypes.intId = trn_bulkdailyproduction.intInkTypeId
WHERE
	trn_bulkdailyproduction.intBulkProductionNo = '$bulkProductionNo'
AND trn_bulkdailyproduction.intBulkProductionYear = '$year'
AND trn_bulkdailyproduction.intSavedStatus = 1 
AND trn_bulkdailyproduction.intchkProduction<>-2";
//echo $sql;

$result = $db->RunQuery($sql);
$rowcount1 = mysqli_num_rows($result);
while ($row = mysqli_fetch_array($result)) {
    $module = $row['module'];
    $location = $row['location'];
    $orderNo = $row['intOrderNo'];
    $orderYear = $row['intOrderYear'];
    $graphicNo = $row['strGraphicNo'];
    $salesOrderNo = $row['strSalesOrderNo'];
    $styleNo = $row['strStyleNo'];
    $combo = $row['combo'];
    $shots = $row['intShots'];
    $ups = $row['noOfUps'];
    $fabricInQty = $row['intTotalFabricInQty'];
    $todayQty = $row['intTodayProductionQty'];
    $user = $row['userName'];
    $customer = $row['customer'];
    $dateOfProduction = $row['dateOfProduction'];
    $printColor = $row['printColor'];
    $inktype = $row['inktype'];
    $section = $row['section'];
    $locationId = $row['locationId']; //Id for report Header
    $print = $row['print'];





    $bulkProductionUrl = $bulkProductionUrl . "&bulkNo=" . $bulkProductionNo . "&bulkYear=" . $year;
 
   
    $checkSpecialPermision = loadSpecialPermissionMode($spMenuId, $intUser);
    ?>
    <head>
        <title>Bulk Daily Production Report</title>


        <style>
            .break { page-break-before: always; }

            @media print {
                .noPrint
                {
                    display:none;
                }
            }
            #apDiv1 {
                position:absolute;
                left:255px;
                top:170px;
                width:650px;
                height:322px;
                z-index:1;
            }
            .APPROVE {
                font-size: 18px;
                font-weight: bold;
            }
        </style>
        
    </head>
    
    <body>
        <script src="presentation/customerAndOperation/bulk/bulkDailyProduction/addNew/bulkDailyProduction-js.js" ></script>
        <form id="frmBulkReport" name="frmBulkReport" method="post" action="rptBulkProduction.php">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td width="20%"></td>
                    <td width="60%" height="80" valign="top"><?php require_once 'reportHeader.php' ?></td>
                    <td width="20%"></td>
                </tr>

                <tr>
                    <td colspan="3"></td>
                </tr>
            </table>
            <div align="center">
                <div style="background-color:#FFF" ><strong>Bulk Daily Production Report</strong><strong></strong></div>

                <table width="900" border="0" align="center" bgcolor="#FFFFFF">
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="1%">&nbsp;</td>

                                    <td width="11%"><span class="normalfnt">Bulk Production No</span></td>
                                    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
                                    <td width="31%"><a href="<?php echo $bulkProductionUrl; ?>" target="bulkDailyProduction.php"><span class="normalfnt"><div id="divBulkNo"><?php echo $bulkProductionNo; ?>/<?php echo $year; ?></div></span></td>
                                    <td><span class="normalfnt">Production Date</span></td>
                                    <td align="center" valign="middle"><strong>:</strong></td>
                                    <td width="10%"><span class="normalfnt"><?php echo $dateOfProduction; ?></span><span class="normalfnt"></span></td>
                                    <td><span class="normalfnt">Production Location</span></td>
                                    <td align="center" valign="middle"><strong>:</strong></td>
                                    <td colspan="3"><span class="normalfnt"><?php echo $location; ?></span><span class="normalfnt"></span></td>



                                </tr>
                                <tr>
                                    <td>&nbsp;</td>


                                </tr>


                                <tr>
                                    <td>&nbsp;</td>
                                    <td class="normalfnt">Entered By</td>
                                    <td align="center" valign="middle"><strong>:</strong></td>
                                    <td><span class="normalfnt"><?php echo $user; ?></span></td>
                                    <?php //} ?>
                                    <td width="4%">&nbsp;</td>
                                    <td width="8%">&nbsp;</td>
                                </tr>
                                <?php
                                // }
                            }
                            ?>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td colspan="7" class="normalfnt">
                                    <table width="100%" class="rptBordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
                                        <tr >
                                            <th width="9%">Section</th>
                                            <th width="9%">Module</th>
                                            <th width="9%">Customer</th>
                                            <th width="9%">Graphic No</th>    
                                            <th width="9%">Order No</th>
                                            <th width="9%">Sales Order No</th>
                                            <th width="9%">Style No</th>
                                            <th width="17%">Ground Colour</th>
                                            <th width="10%">Print</th>
                                            <th width="8%">Combo</th>
                                            <th width="40%">Print Colour-Ink Type</th>
                                            <th width="8%">Shots per pcs- Planned</th>
                                            <th width="8%">Ups - Planned</th>
                                            <th width="8%">Shots per pcs- Actual</th>
                                            <th width="8%">Ups - Actual</th>
                                            <th width="10%">Cum.Fab-in Qty-Actual</th>
                                            <th width="10%">Cum.Dispatched Qty-Actual</th>
                                            <th width="10%">Balance to Dispatch</th>
                                            <th width="10%">Cum. Complete Prod. Qty-Actual (@ today 00:01) </th>
                                            <th width="10%">Bal to Production</th>
                                            <th width="10%">Day's Production Qty-Actual</th>
                                            <th width="10%">Production Status</th>
                                            <th width="10%">Remarks</th>
                                            <?php if ($checkSpecialPermision == 'true') { ?>
                                                <th width="10%">Unit Price</th>
                                                <th width="10%">Day's Production Value</th>
                                            <?php } ?>

                                        </tr>
                                        <?php
                                       
                                        $sql1 = "SELECT 
	mst_module.strName AS module,
	CONCAT(
		mst_locations.strName,
		'-',
		mst_companies.strName
	) AS location,
	trn_bulkdailyproduction.intOrderNo,
	trn_bulkdailyproduction.intOrderYear,
	trn_bulkdailyproduction.strGraphicNo,
	trn_orderdetails.strSalesOrderNo,
	trn_bulkdailyproduction.strStyleNo,
	trn_bulkdailyproduction.strCombo AS combo,
	trn_bulkdailyproduction.intShots,
	trn_bulkdailyproduction.noOfUps,
	trn_bulkdailyproduction.intTotalFabricInQty,
	trn_bulkdailyproduction.intTodayProductionQty,
	sys_users.strUserName AS userName,
	mst_customer.strName AS customer,
	trn_bulkdailyproduction.dateOfProduction,
	mst_section.strName as section,
        trn_bulkdailyproduction.intPrintColor,
        trn_bulkdailyproduction.intInkTypeId,
        IF (trn_bulkdailyproduction.intPrintColor =- 1,'All',concat(IF (trn_bulkdailyproduction.intPrintColor =- 10,'N/A',mst_colors.strName),'-',
        IF (trn_bulkdailyproduction.intInkTypeId =- 10,'N/A',mst_inktypes.strName))) AS color,
        trn_bulkdailyproduction.productionLocation as locationId,
        trn_bulkdailyproduction.strPrintName AS print,
        trn_bulkdailyproduction.plannedUps,
        trn_bulkdailyproduction.plannedShots,
        trn_bulkdailyproduction.intTotalDispatchedQty,
        trn_bulkdailyproduction.intTotalProductionMarkedQty,
        trn_bulkdailyproduction.intchkProduction,
        mst_colors_ground.strName as groundColor,
        trn_bulkdailyproduction.intSavedStatus,
        trn_bulkdailyproduction.strRemarks
FROM
	trn_bulkdailyproduction 
INNER JOIN mst_locations ON mst_locations.intId = trn_bulkdailyproduction.productionLocation
INNER JOIN mst_companies ON mst_companies.intId = mst_locations.intCompanyId
INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_bulkdailyproduction.intOrderNo
AND trn_orderdetails.intOrderYear = trn_bulkdailyproduction.intOrderYear
LEFT JOIN mst_colors ON mst_colors.intId = trn_bulkdailyproduction.intPrintColor
INNER JOIN sys_users ON sys_users.intUserId = trn_bulkdailyproduction.intUser
INNER JOIN mst_customer ON mst_customer.intId = trn_bulkdailyproduction.intCustomer
INNER JOIN mst_section ON mst_section.intId = trn_bulkdailyproduction.intSection
INNER JOIN mst_module ON mst_module.intId = trn_bulkdailyproduction.strModuleCode
LEFT JOIN mst_inktypes ON mst_inktypes.intId = trn_bulkdailyproduction.intInkTypeId
INNER JOIN mst_colors_ground ON mst_colors_ground.intId=trn_bulkdailyproduction.intGroundColor
AND trn_orderdetails.intSalesOrderId=trn_bulkdailyproduction.intSalesOrderNo
WHERE
	trn_bulkdailyproduction.intBulkProductionNo = '$bulkProductionNo'
AND trn_bulkdailyproduction.intBulkProductionYear = '$year'
AND trn_bulkdailyproduction.intSavedStatus = 1 
AND trn_bulkdailyproduction.intchkProduction<>-2 ORDER BY trn_bulkdailyproduction.Id";
//                                        echo $sql1;
                                        $result1 = $db->RunQuery($sql1);
                                        //$recordCount=mysqli_num_rows($result);
                                         $recordCount = 0;
                                         $records = array();
                                        while ($row = mysqli_fetch_array($result1)) {
                                            //$row= mysqli_fetch_array($result);
                                             $showExcelReportStatus = 1;
                                             $module = $row['module'];

                                            $orderNo = $row['intOrderNo'];
                                            $orderYear = $row['intOrderYear'];
                                            $graphicNo = $row['strGraphicNo']; 
                                            $todayQty = $row['intTodayProductionQty'];
                                            $customer = $row['customer'];
                                            $dateOfProduction = $row['dateOfProduction'];
                                            $printColor = $row['printColor'];
                                            $inktype = $row['inktype'];
                                            $section = $row['section'];
                                            $locationId = $row['locationId']; //Id for report Header
                                            $salesOrderNo = $row['strSalesOrderNo'];
                                            $styleNo = $row['strStyleNo'];
                                            $print = $row['print'];
                                            $combo = $row['combo'];
                                            $fabricInQTY = $row['intTotalFabricInQty'];
                                            $dispatchQty=$row['intTotalDispatchedQty'];
                                            $printColor = $row['color'];
                                            $noOfShots = $row['intShots'];
                                            $noOfUps = $row['noOfUps'];
                                            $groundColor = $row['groundColor'];
                                            $savedStatus = $row['intSavedStatus'];
                                            $plannedUps=$row['plannedUps'];
                                            $plannedShots=$row['plannedShots'];
                                            $color = "#B8DCDC";
                                            $reportStatus = '';
                                            $showExcelReportStatus = 0;
                                            $printName=explode('-',$print);
                                            $price = getPrice($orderNo, $orderYear, $salesOrderNo, $graphicNo, $combo,$printName[0]);
                                            $value=
                                            $remarks=$row['strRemarks'];
                                           // $totalProductionQty=getActualProductionQty($orderNo, $salesOrderNo, $graphicNo, $combo, $print, $locationId);
?>


<?php
                                                $details = array();
                                                if ($savedStatus == 1) {
                                                $color = "#00A6A6";
                                                $reportStatus = 'Process Completed';
                                               
                                                $detalList = array();
                                                $detalList['OrderNo'] = $orderNo;
                                                $detalList['OrderYear'] = $orderYear;
                                                $detalList['BulkProductionNo'] = $bulkProductionNo;
                                                $detalList['BulkProductionYear'] = $year;
                                                $detalList['date'] = $dateOfProduction;
                                                $detalList['customer'] = $customer;
                                                $detalList['location'] = $location;
                                                $detalList['styleNo'] = $styleNo;
                                                $detalList['graphicNo'] = $graphicNo;
                                                $detalList['section'] = $section;
                                                $detalList['moduleCode'] = $module;
                                                $detalList['strSalesOrderNo'] = $salesOrderNo;
                                                $detalList['fabricInQty'] = $fabricInQTY;
                                                $detalList['dispatchQty'] = $dispatchQty;
                                                $detalList['noOfUps'] = $noOfUps;
                                                $detalList['noOfShots'] = $noOfShots;
                                                $detalList['plannedUPs'] =$plannedUps;
                                                $detalList['plannedShots'] = $plannedShots;
                                                $detalList['combo'] = $combo;
                                                $detalList['part'] = $print;
                                                $detalList['printColor'] = $printColor;
                                                $detalList['GroundColor'] = $groundColor;
                                                $detalList['cummProductionQty'] = getActualProductionQty($orderNo, $salesOrderNo, $graphicNo, $combo, $print, $locationId);
                                                $qty=getActualProductionQty($orderNo, $salesOrderNo, $graphicNo, $combo, $print, $locationId);
                                                $detalList['balQty']=$fabricInQTY-$qty;
                                                $detalList['todayQty']=$todayQty;
                                                $detalList['remarks']=$remarks;   
                                                $detalList['permission']=$checkSpecialPermision;                                                
                                                if($qty>$row['intTotalDispatchedQty']) { $detalList['baltoDispatch']=$qty- $row['intTotalDispatchedQty'];}else{ $detalList['baltoDispatch']=$qty;  }
                                                if ($checkSpecialPermision == 'true') { 
                                                    $detalList['unitprice']=$price;
                                                    if($row['intchkProduction'] == 0){ $detalList['value']=number_format($row['intTodayProductionQty'] * $price, 2);}else{
                                                        $detalList['unitprice']=0;
                                                    }
                                                }
                                            } else {
                                                $color = "#9D0000";
                                                $reportStatus = 'Process Not Completed';
                                                }
                                            
                                            ?>
                                            <tr class="normalfnt">
                                                        <?php for($i=0;$i<sizeof($recordCount);$i++) { ?>
                                                <td class="normalfnt">&nbsp;<?php echo $row['section']; ?>&nbsp;</td>
                                                <td class="normalfnt">&nbsp;<?php echo $row['module'] ?>&nbsp;</td>
                                                <td class="normalfnt">&nbsp;<?php echo $row['customer'] ?>&nbsp;</td>
                                                <td class="normalfnt">&nbsp;<?php echo $row['strGraphicNo'] ?>&nbsp;</td>
                                                <td class="normalfnt">&nbsp;<?php echo $row['intOrderNo'] . '/' . $row['intOrderYear'] ?>&nbsp;</td>
                                                <td class="normalfnt">&nbsp;<?php echo $row['strSalesOrderNo'] ?>&nbsp;</td>
                                                <td class="normalfnt">&nbsp;<?php echo $row['strStyleNo'] ?></td>
                                                <td class="normalfnt">&nbsp;<?php echo $row['groundColor']; ?>&nbsp;</td>                                                                                                                   
                                                <td class="normalfnt">&nbsp;<?php echo $row['print'] ?>&nbsp;</td>
                                                <td class="normalfnt">&nbsp;<?php echo $row['combo'] ?>&nbsp;</td>
                                                <td class="normalfnt">&nbsp;<?php echo $row['color'] ?>&nbsp;</td>                                           
                                                <td class="normalfnt">&nbsp;<?php echo $row['intShots'] ?>&nbsp;</td>
                                                <td class="normalfnt">&nbsp;<?php echo $row['noOfUps'] ?>&nbsp;</td>
                                                <td class="normalfnt">&nbsp;<?php echo $row['plannedUps'] ?>&nbsp;</td>
                                                <td class="normalfnt">&nbsp;<?php echo $row['plannedShots'] ?>&nbsp;</td>                                            
                                                <td class="normalfnt">&nbsp;<?php echo $row['intTotalFabricInQty'] ?>&nbsp;</td>
                                                <td class="normalfnt">&nbsp;<?php echo $row['intTotalDispatchedQty']; ?>&nbsp;</td>
                                                <td class="normalfnt">&nbsp;<?php $qty = getActualProductionQty($orderNo, $row['strSalesOrderNo'], $graphicNo, $combo, $print, $locationId);  if($qty>$row['intTotalDispatchedQty']) {  echo $qty- $row['intTotalDispatchedQty'];}else{ echo $qty;  }?>&nbsp;</td>
                                                <td class="normalfnt">&nbsp;<?php    $qty = getActualProductionQty($orderNo, $row['strSalesOrderNo'], $graphicNo, $combo, $print, $locationId);echo $qty; ?>&nbsp;</td>
                                                <td class="normalfnt">&nbsp;<?php echo $row['intTotalFabricInQty'] - $qty; ?>&nbsp;</td>
                                                <td class="normalfnt">&nbsp;<?php echo $row['intTodayProductionQty']; ?>&nbsp;</td>
                                                <td class="normalfnt">&nbsp;<?php
                                                    if ($row['intchkProduction'] == 0) {
                                                        echo 'Production Completed';
                                                    } else {
                                                        echo 'Production InComplete';
                                                    }
                                                    ?>&nbsp;</td>
                                                <td class="normalfnt">&nbsp;<?php echo $row['strRemarks']; ?>&nbsp;</td>
                                                <?php if ($checkSpecialPermision == 'true') { 
                                                   ?>
                                                    <td class="normalfnt">&nbsp;<?php  echo $price;?>&nbsp;</td>
                                                    <td class="normalfnt">&nbsp;<?php if($row['intchkProduction'] == 0){ echo number_format($row['intTodayProductionQty'] * $price, 2);}else{
                                                        echo '';
                                                    }
                                                    
                                                        ?>&nbsp;</td>
                                                    
                                                <?php } ?>

                                            </tr>
                                            <?php    
                                        array_push($records, $detalList);
                                        
                                        $recordCount++;
                                          }
                                        }
                                        $_SESSION['BULK_DAILY_PRODUCTION'] = $records;
                                        ?>
                                    </table>
                                </td>   
                            </tr>
                            <tr>
                                   <img style="display:<?php if($showExcelReportStatus==1){ echo "inline";}?> " src="images/Treport_exel.png" alt="Save" name="butReport" class="mouseover" id="butReport" tabindex="24"  border="0">

                            </tr>


                        </table>
                    </td>
                </tr>
                
 
            
                <tr height="40" >
                    <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
                </tr>

            </table>

        </div>
    
       
        <?php

        function getActualProductionQty($orderNo, $salesOrderNo, $graphicNo, $combo, $print, $locationId) {
            global $db;

            $sql = "SELECT DISTINCT
	IFNULL(SUM(trn_bulkdailyproduction.intTodayProductionQty),
		0
	) AS qty
FROM
	trn_bulkdailyproduction
WHERE
	trn_bulkdailyproduction.strGraphicNo = '$graphicNo'
AND trn_bulkdailyproduction.intOrderNo = '$orderNo'
AND trn_bulkdailyproduction.strSalesOrderNo = '$salesOrderNo'
AND trn_bulkdailyproduction.productionLocation = '$locationId'
AND trn_bulkdailyproduction.strCombo = '$combo'
AND trn_bulkdailyproduction.strPrintName = '$print'
AND trn_bulkdailyproduction.intchkProduction = '-2'";

            $result = $db->RunQuery($sql);
            $row = mysqli_fetch_array($result);
            $productionQty = $row['qty'];
            return $productionQty;
        }

        function getPrice($orderNo, $orderYear, $salesOrderNo, $graphicNo, $combo, $print) {
            global $db;
            $sql = "SELECT DISTINCT
	trn_orderdetails.dblPrice
FROM
	trn_orderdetails
INNER JOIN trn_bulkdailyproduction ON trn_bulkdailyproduction.intOrderNo = trn_orderdetails.intOrderNo
AND trn_bulkdailyproduction.intOrderYear = trn_orderdetails.intOrderYear
WHERE
	trn_orderdetails.intOrderNo = '$orderNo'
AND trn_orderdetails.intOrderYear = '$orderYear'
AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo'
AND trn_orderdetails.strGraphicNo = '$graphicNo'
AND trn_orderdetails.strCombo = '$combo'
AND trn_orderdetails.strPrintName = '$print'
AND trn_bulkdailyproduction.intchkProduction =-2";
//            echo $sql;
            $result = $db->RunQuery($sql);
            $row = mysqli_fetch_array($result);
            $price = $row['dblPrice'];
            return $price;
        }

        function loadSpecialPermissionMode($spMenuId, $intUser) {
            global $db;

            $sqlp = "SELECT IF(EXISTS(SELECT * FROM menus_special_permision WHERE intSpMenuId = '$spMenuId' AND intUser = '$intUser') ,'true', 'false') AS validUser";

            $resultp = $db->RunQuery($sqlp);
            $rowp = mysqli_fetch_assoc($resultp);
            $status = $rowp['validUser'];
            return $status;
        }
        ?>

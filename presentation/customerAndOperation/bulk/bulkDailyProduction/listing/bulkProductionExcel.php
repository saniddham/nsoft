<?php
session_start();
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=bulk_production_report.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");

$records = $_SESSION['BULK_DAILY_PRODUCTION'];
foreach ($records as $record){
    $checkSpecialPermision = $record['permission'];
}

?>
<div align="center">
    <div class="trans_layoutL">
        <div class="trans_text" align="center"><strong><bold>Bulk Daily Production Report</bold></strong></div>
        <table width="100%" border="1" align="center" bgcolor="#FFFFFF">

            <tr></tr>
            <tr>
                <td height="22" colspan="10" class="normalfnt">
                    <table width="100%" border="1" cellspacing="0" cellpadding="0" class="bordered">
                        <tr>

                            <th width="10%" class="normalfnt">Bulk Production Year</th>
                            <th width="10%" class="normalfnt">Bulk Production No</th>
                            <th width="10%" class="normalfnt">Production Date</th>
                            <th width="10%" class="normalfnt">Production Location</th>
                            <th width="10%" class="normalfnt">Section</th>
                            <th width="10%" class="normalfnt">Module</th>
                            <th width="10%" class="normalfnt">Order Year</th>
                            <th width="10%" class="normalfnt">Order No</th>
                            <th width="10%" class="normalfnt">Customer</th>
                            <th width="18%" class="normalfnt">Graphic No</th>
                            <th width="10%" class="normalfnt">Sales Order No</th>
                            <th width="10%" class="normalfnt">Style No</th>
                            <th width="11%" class="normalfnt">Combo</th>
                            <th width="8%" class="normalfnt">Ground Color</th>
                            <th width="9%" class="normalfnt">Print Color</th>
                            <th width="4%" class="normalfnt">Print-Part</th>
                            <th width="20%" class="normalfnt">UPS</No></th>
                            <th width="10%" class="normalfnt">Shots</th>
                            <th width="20%" class="normalfnt">Planned UPS</No></th>
                            <th width="10%" class="normalfnt">Planned Shots</th>
                            <th width="8%" class="normalfnt">Fabric In Qty</th>
                            <th width="8%" class="normalfnt">Dispatch Qty</th>
                            <th width="8%" class="normalfnt">Balance to Dispatch Qty</th>
                            <th width="8%" class="normalfnt">Total Production Qty</th>
                            <th width="8%" class="normalfnt">Balance to Production Qty</th>                            
                            <th width="8%" class="normalfnt">Day's Production Qty</th>
                            <?php if($checkSpecialPermision=='true'){?>
                            <th width="8%" class="normalfnt">Unit Price</th>
                            <th width="8%" class="normalfnt">Value</th>
                            <?php }?>
                            <th width="8%" class="normalfnt">Remarks</th>

                        </tr>
                        <tr>
                            <?php foreach ($records as $record) {?>
                            <td align="center"><?php echo $record['BulkProductionYear']; ?></td>
                            <td align="center"><?php echo $record['BulkProductionNo']; ?></td>                            
                            <td align="center"><?php echo $record['date']; ?></td>
                            <td align="center"><?php echo $record['location']; ?></td>
                            <td align="center"><?php echo $record['section']; ?></td>
                            <td align="center"><?php echo $record['moduleCode']; ?></td>
                            <td align="center"><?php echo $record['OrderYear']; ?></td>
                            <td align="center"><?php echo $record['OrderNo']; ?></td>
                            <td align="center"><?php echo $record['customer']; ?></td>
                            <td align="center"><?php echo $record['graphicNo']; ?></td>
                            <td align="center"><?php echo $record['strSalesOrderNo']; ?></td>
                            <td align="center"><?php echo $record['styleNo']; ?></td>
                            <td align="center"><?php echo $record['combo']; ?></td>
                            <td align="center"><?php echo $record['GroundColor']; ?></td>
                            <td align="center"><?php echo $record['printColor']; ?></td>
                            <td align="center"><?php echo $record['part']; ?></td>
                            <td align="center"><?php echo $record['noOfUps']; ?></td>
                            <td align="center"><?php echo $record['noOfShots']; ?></td>
                            <td align="center"><?php echo $record['plannedUPs']; ?></td>
                            <td align="center"><?php echo $record['plannedShots']; ?></td>
                            <td align="center"><?php echo $record['fabricInQty']; ?></td>
                            <td align="center"><?php echo $record['dispatchQty']; ?></td>
                            <td align="center"><?php echo $record['baltoDispatch']; ?></td>
                            <td align="center"><?php echo $record['cummProductionQty']; ?></td>
                            <td align="center"><?php echo $record['balQty']; ?></td>
                            <td align="center"><?php echo $record['todayQty']; ?></td>
                             <?php if($checkSpecialPermision=='true'){?>
                            <td align="center"><?php echo $record['unitprice']; ?></td>
                            <td align="center"><?php echo $record['value']; ?></td>
                            <?php }?>
                            <td align="center"><?php echo $record['remarks']; ?></td>
                        </tr>
                        <?php }?>
                        <tr></tr>

                    </table>
            <tr></tr>
        </table>
        </td>
        </tr>
        </table>
    </div>
</div>





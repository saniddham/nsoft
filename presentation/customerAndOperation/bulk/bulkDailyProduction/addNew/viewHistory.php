<?php
session_start();
$mainPath = $_SESSION['mainPath'];
$backwardseperator = "../../../../../";
$requestType = $_REQUEST['requestType'];

include "{$backwardseperator}dataAccess/Connector.php";
$companyId = $_SESSION['CompanyID'];
$intUser = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath = $_SERVER['PHP_SELF'];

$getorderNo=explode('/',$_REQUEST['orderNo']);
$orderNo=$getorderNo[0];
$orderYear=$getorderNo[1];
$graphicNo=$_REQUEST['graphicNo'];
$salesOrderNo=$_REQUEST['salesOrderNo'];
$combo=$_REQUEST['combo'];
$printPart=$_REQUEST['print'];
$locationId=$_REQUEST['location'];
$date=$_REQUEST['dateFrom'];

$sql="SELECT
	Concat(mst_companies.strName,'-',mst_locations.strName) as location
FROM
	mst_locations
LEFT JOIN mst_companies ON mst_companies.intId = mst_locations.intCompanyId
WHERE
	mst_locations.intId = '$locationId'";
$result = $db->RunQuery($sql);
$row= mysqli_fetch_array($result);
$location=$row['location'];

?>
<head>
    <title>Bulk Daily Production History</title>


    <style>
        .break { page-break-before: always; }

        @media print {
            .noPrint
            {
                display:none;
            }
        }
        #apDiv1 {
            position:absolute;
            left:255px;
            top:170px;
            width:650px;
            height:322px;
            z-index:1;
        }
        .APPROVE {
            font-size: 18px;
            font-weight: bold;
        }
    </style>

</head>

<body>
    <form id="frmBulkHistory" name="frmBulkHistory" method="post" action="">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="3"></td>
            </tr>
         <tr>
                    <td width="20%"></td>
                    <td width="60%" height="80" valign="top"><?php require_once 'reportHeader.php' ?></td>
                    <td width="20%"></td>
                </tr>

            <tr>
                <td colspan="3"></td>
            </tr>
        </table>
        <div align="center">
        <div style="background-color:#FFF" ><strong>Bulk Daily Production History</strong><strong></strong></div>
        </div>
        <div>
            &nbsp;  &nbsp;  &nbsp;  &nbsp;
        </div>
        
        <table width="900" border="0" align="center" bgcolor="#FFFFFF" >
            <tr>
                <td>&nbsp;</td>
                <td class="normalfnt">Order No</td>
                <td align="center" valign="middle">:</td>
                <td class="normalfnt"><?php echo $orderNo.'/'.$orderYear; ?></td>
                 <td>&nbsp;</td>
                <td class="normalfnt">Sales Order No</td>
                <td align="center" valign="middle">:</td>
                <td class="normalfnt"><?php echo $salesOrderNo; ?></td>
                 <td>&nbsp;</td>
                <td><span class="normalfnt">Graphic No</span></td>
                <td align="center" valign="middle">:</td>
                <td><span class="normalfnt"><?php echo $graphicNo; ?></span></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="normalfnt"  bgcolor="#FFFFFF">Combo</td>
                <td align="center" valign="middle">:</td>
                <td class="normalfnt"><?php echo $combo; ?></td>
                 <td>&nbsp;</td>
                <td class="normalfnt">Print-Part</td>
                <td align="center" valign="middle">:</td>
                <td class="normalfnt"><?php echo $printPart; ?></td>
                 <td>&nbsp;</td>
                <td><span class="normalfnt">Production Marked Location</span></td>
                <td align="center" valign="middle">:</td>
                <td><span class="normalfnt"><?php echo $location; ?></span></td>
            </tr>
        </table>
        <div>
            &nbsp;  &nbsp;  &nbsp;  &nbsp;
        </div>
        <div align="center">          
            <table width="900" border="0" align="center" bgcolor="#FFFFFF"> 
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td colspan="7" class="normalfnt">
                                    <table width="100%" class="rptBordered" id="tblMainGrid" cellspacing="0" cellpadding="0"border="1" >
                                        <tr >
                                            <th width="8%" >Bulk Production No</th>
                                            <th width="8%" >Production Date</th>
                                            <th width="10%">Print Colour</th>
                                            <th width="9%">Ink Type</th>
                                            <th width="17%" >Today's Production Qty</th>
                                            <th width="5%" >Cumm. Production Qty</th>
                                            <th width="8%" >To date Fabric-In Qty</th>
                                            <th width="8%" >To date Fabric-Out Qty</th>
                                        </tr>
<?php
$sql1 = "SELECT 
	trn_bulkdailyproduction.dateOfProduction,        
        trn_bulkdailyproduction.intBulkProductionNo,
        trn_bulkdailyproduction.intBulkProductionYear,
        IF (
               trn_bulkdailyproduction.intPrintColor =- 1,
               'All',
               IF(trn_bulkdailyproduction.intPrintColor =- 10,'N/A',
               mst_colors.strName)
        ) AS PrintColor,
        IF (
               trn_bulkdailyproduction.intInkTypeId =0,
               'All',
               IF(trn_bulkdailyproduction.intInkTypeId =- 10,'N/A',
               mst_inktypes.strName )
               
       ) AS InkType,
	trn_bulkdailyproduction.intTotalFabricInQty AS FabricInQty,
	trn_bulkdailyproduction.intTotalDispatchedQty AS FabricOutQty,
	trn_bulkdailyproduction.intTodayProductionQty AS todayQty, 
        trn_bulkdailyproduction.intchkProduction
FROM
	trn_bulkdailyproduction
LEFT JOIN mst_colors ON mst_colors.intId = trn_bulkdailyproduction.intPrintColor
LEFT JOIN mst_inktypes ON mst_inktypes.intId = trn_bulkdailyproduction.intInkTypeId
LEFT JOIN trn_orderdetails ON trn_orderdetails.intSalesOrderId=trn_bulkdailyproduction.intSalesOrderNo
AND trn_bulkdailyproduction.intOrderNo = trn_orderdetails.intOrderNo
AND trn_bulkdailyproduction.intOrderYear = trn_orderdetails.intOrderYear
WHERE
	trn_bulkdailyproduction.intOrderNo = '$orderNo'
AND trn_bulkdailyproduction.intOrderYear = '$orderYear'
AND trn_bulkdailyproduction.strGraphicNo='$graphicNo'
AND trn_bulkdailyproduction.strSalesOrderNo='$salesOrderNo'
AND trn_bulkdailyproduction.strCombo='$combo'
AND trn_bulkdailyproduction.strPrintName='$printPart'
AND trn_bulkdailyproduction.productionLocation='$locationId'
AND trn_bulkdailyproduction.intchkProduction <>-2 ORDER BY trn_bulkdailyproduction.dateOfProduction,trn_bulkdailyproduction.Id ASC";
          // echo $sql1;                         
$result1 = $db->RunQuery($sql1);
$rowcount = count(mysqli_num_rows($result1));

while ($row = mysqli_fetch_array($result1)) {

    $dateOfProduction = $row['dateOfProduction'];
    $printColor = $row['PrintColor'];
    $inkType = $row['InkType'];
    $fabricInQTY = $row['FabricInQty'];
    $dispatchQty = $row['FabricOutQty'];
    $date=getMaxDate($graphicNo, $orderNo, $salesOrderNo, $combo,$printPart,$locationId);
    $totproductionQty = getActualProductionQty($orderNo, $salesOrderNo, $graphicNo, $combo, $printPart, $locationId,$date);
    $todayQty = $row['todayQty'];
    $serialNo=$row['intBulkProductionNo'];
    ?>
                                            <tr class="normalfnt">
                                            <?php for ($i = 0; $i < sizeof($rowcount); $i++) { ?>
                                                <td class="normalfnt">&nbsp;<a href="?q=1285&bulkNo=<?php echo$row['intBulkProductionNo'];?> &bulkYear=<?php echo$row['intBulkProductionYear'];?>"><?php echo $row['intBulkProductionNo'] ?>&nbsp;
                                                    </td>
                                                    <td class="normalfnt">&nbsp;<?php echo $row['dateOfProduction'] ?>&nbsp;
                                                    </td>
                                                    <td class="normalfnt">&nbsp;<?php echo $printColor ?>&nbsp;</td>
                                                    <td class="normalfnt">&nbsp;<?php echo $inkType ?>&nbsp;</td>
                                                    <td class="normalfnt">&nbsp;<?php echo $todayQty ?>&nbsp;</td>
                                                    <td class="normalfnt">&nbsp;<?php if($row['intchkProduction']==0){echo $totproductionQty;}else{echo 'Incomplete Production';} ?>&nbsp;</td>
                                                    <td class="normalfnt">&nbsp;<?php echo $fabricInQTY ?>&nbsp;</td>
                                                    <td class="normalfnt">&nbsp;<?php echo $dispatchQty ?>&nbsp;</td>
                                                </tr>
        <?php
    }
}
?>

                                    </table>

                                </td>
                                <td width="6%">&nbsp;</td>
                            </tr>

                        </table>
                    </td>
                </tr>


                <tr height="40" >
                    <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
                </tr>
            </table>
        </div>
<?php
function getActualProductionQty($orderNo, $salesOrderNo, $graphicNo, $combo, $print, $location,$date) {
    global $db;
    $today = date("Y-m-d");
    if($date!=$today){
    $sql = "SELECT DISTINCT
	IFNULL(SUM(trn_bulkdailyproduction.intTodayProductionQty),
		0
	) AS qty
FROM
	trn_bulkdailyproduction
WHERE
	trn_bulkdailyproduction.strGraphicNo = '$graphicNo'
AND trn_bulkdailyproduction.intOrderNo = '$orderNo'
AND trn_bulkdailyproduction.strSalesOrderNo = '$salesOrderNo'
AND trn_bulkdailyproduction.productionLocation = '$location'
AND trn_bulkdailyproduction.strCombo = '$combo'
AND trn_bulkdailyproduction.strPrintName = '$print'
AND trn_bulkdailyproduction.dateOfProduction <= '$date'    
AND trn_bulkdailyproduction.intchkProduction = '-2'";
    }else{
        $sql = "SELECT DISTINCT
	IFNULL(SUM(trn_bulkdailyproduction.intTodayProductionQty),
		0
	) AS qty
FROM
	trn_bulkdailyproduction
WHERE
	trn_bulkdailyproduction.strGraphicNo = '$graphicNo'
AND trn_bulkdailyproduction.intOrderNo = '$orderNo'
AND trn_bulkdailyproduction.strSalesOrderNo = '$salesOrderNo'
AND trn_bulkdailyproduction.productionLocation = '$location'
AND trn_bulkdailyproduction.strCombo = '$combo'
AND trn_bulkdailyproduction.strPrintName = '$print'
AND trn_bulkdailyproduction.dateOfProduction < '$date'    
AND trn_bulkdailyproduction.intchkProduction = '-2'";
    }
   
//AND trn_bulkdailyproduction.intPrintColor IN (" . implode(',', $printColor) . ") AND trn_bulkdailyproduction.intInkTypeId IN (" . implode(',', $inktype) . ")";
    //echo $sql;

    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $productionQty = $row['qty'];
    return $productionQty;
}
function getMaxDate($graphicNo, $orderNo, $salesOrderNo, $combo, $print,$location){
    global $db;
    $sql="SELECT
	MAX(dateOfProduction) as date
FROM
	trn_bulkdailyproduction
WHERE
	trn_bulkdailyproduction.strGraphicNo = '$graphicNo'
AND trn_bulkdailyproduction.intOrderNo = '$orderNo'
AND trn_bulkdailyproduction.strSalesOrderNo = '$salesOrderNo'
AND trn_bulkdailyproduction.productionLocation = '$location'
AND trn_bulkdailyproduction.strCombo = '$combo'
AND trn_bulkdailyproduction.strPrintName = '$print'";
   // echo $sql;
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $prevDate = $row['date'];
    return $prevDate;
    
}
?>
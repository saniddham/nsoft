//---------------------------------------------------------------------

//################################################################
//Bulk Production
//#################################################################

//-----------------------------------------------------------------

var basepath = 'presentation/customerAndOperation/bulk/bulkDailyProduction/addNew/';
var basepath1 = 'presentation/customerAndOperation/bulk/bulkDailyProduction/listing/';

var msgAlert = '';
var errsplit = 0;


$(document).ready(function () {

    $('#frmBulkDailyProduction').validationEngine();

    $('#frmBulkDailyProduction #cboOrderNo').focus();


 $('.delImg').die('click').live('click',function(){

        var rowCount1 	= document.getElementById('tblProduction').rows.length;
        if(rowCount1<=3)
        {
            alert("Can't delete.Atleast one sales order should be exists in a bulk order");
            return false;
        }else{
             $(this).parent().parent().remove();
        }
        if($(obj).parent().parent().parent().find('tr').length>3)
        {
        $(this).parent().parent().remove();
            $('#tblPOs >tbody >tr:eq(1)').find('.clsCopy').html('&nbsp;');
        }


        var viewSave_parent	=1;
        $('#tblPOs >tbody >tr').not(':last').not(':first').each(function(){
            //alert($(this).find('#txtCustoPOsplited').val()+'='+$('#txtCustomerPO').val());
            if($('#txtCustomerPO').val()!=$(this).find('#txtCustoPOsplited').val()){
                viewSave_parent	=0;
            }
    });
        if(viewSave_parent==1){
            $('.cls_insertRow').show();
            $('#butSave').show();
            $('#butConfirm').show();
            $('#butSaveChild').hide();
        }
    });

    $('#frmBulkDailyProduction #tblProduction #butInsertRow').live('click', function () {

        
        var rowCount = document.getElementById('tblProduction').rows.length;
        document.getElementById('tblProduction').insertRow(rowCount - 1);
        rowCount = document.getElementById('tblProduction').rows.length;
        document.getElementById('tblProduction').rows[rowCount - 2].innerHTML = document.getElementById('tblProduction').rows[rowCount - 3].innerHTML;
        document.getElementById('tblProduction').rows[rowCount - 2].className = "normalfnt";
        document.getElementById('tblProduction').rows[rowCount - 2].id = 'dataRow';

        var a = rowCount -2;
        

        var str1		= '<select data-placeholder="Select Some Options..." id="cboPrintColor" class="txtConcernRaisedBy'+a+'  cons msCombo chosen-select clsorder" multiple style="width:188px; height:50px" >'+'</select>';
        var str2        = '<select class="js-example-data-ajax normalfnt" id="cboDesc" name="cboDesc"> <option value="" selected="selected">Please Clear Cache </option> </select>';
        
        
        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') #cboPrintColorCloumn').html(str1);
        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') #cboDescCollumn').html(str2);
        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') #cboDesc').attr('disabled', false);
        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') #cboCustomer').attr('disabled', false);
        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') #cboGraphicNo').attr('disabled', false);
        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') #cboOrderNo').attr('disabled', false);
        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') #cboSalesOrderNo').attr('disabled', false);
        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') #cboPart').attr('disabled', false);
        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') #cboCombo').attr('disabled', false);
        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') #cboPrintColor').attr('disabled', false);
        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') #cboGroundColor').attr('disabled', false);
        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') #plannedUps').attr('disabled', false);
        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') #plannedShots').attr('disabled', false);
        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') #txtFabricInQty').attr('disabled', false);
        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') #txtProductionInQty').attr('disabled', false);
        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') #txtDispatchQty').attr('disabled', false);
        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') #txtActualProductionQty').attr('disabled', false);
        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') #txtNoOfShots').attr('disabled', false);
        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') #txtNoUps').attr('disabled', false);
        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') #cboSection').val('');
        
//        //if((jQuery("#cboPrintColor").val() == "-1")) {
//			//jQuery('#cboPrintColorCloumn').attr("disabled", true);
//                        $('#cboPrintColorCloumn').multiselect('disable');
//		}
//		else{
//			jQuery('#cboPrintColorCloumn').attr("disabled", false);
//		}
       // jQuery("#stateSelection").multiSelect({ oneOrMoreSelected: '',noneSelected : 'Select options (Default = ALL)' });
        jQuery("#frmBulkDailyProduction .chosen-select").chosen({allow_single_deselect: false , no_results_text: "Oops, nothing found!"});

        $(this).parent().parent().parent().find('tr:eq(' + ($(this).parent().parent().index() - 1) + ') .js-example-data-ajax').select2({

            placeholder: "Search for a repository",

            ajax: {
                url: "presentation/customerAndOperation/bulk/bulkDailyProduction/addNew/bulkDailyProduction-db-get.php",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        val: (params.term == null) ? 'ALLSELECT' : params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 10) < data.total_count
                        }
                    };
                },
                cache: true
            },
            placeholder: 'Search for a repository',
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 0,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });
    });

    if ($(obj).parent().parent().parent().find('tr').length > 3) {
        $(this).parent().parent().remove();
        $('#tblProduction >tbody >tr:eq(1)').find('.clsCopy').html('&nbsp;');
    }


    var viewSave_parent = 1;
    $('#tblProduction >tbody >tr').not(':last').not(':first').each(function () {
        //alert($(this).find('#txtCustoPOsplited').val()+'='+$('#txtCustomerPO').val());
        if ($('#txtCustomerPO').val() != $(this).find('#txtCustoPOsplited').val()) {
            viewSave_parent = 0;
        }
    });
    if (viewSave_parent == 1) {
        $('.cls_insertRow').show();
        $('#butSave').show();
        $('#butConfirm').show();
        $('#butSaveChild').hide();
    }
});





//add rows
//-----------------------------------------------------------------------------------------------------
  $('#frmBulkDailyProduction  #tblProduction #butView').click(function(){
	  
	if ($('#frmBulkDailyProduction').validationEngine('validate'))   
        { 
	var graphicNo=$(this).parent().parent().find('#cboGraphicNo').val();
        var salesOrderNo=$(this).parent().parent().find('#cboSalesOrderNo').val();
	var dateFrom=$('#cboDate').val();
        $(this).parent().parent().find('#cboLocation').val();
	var url='index.php?q=1292';
		url+='&orderNo='+$(this).parent().parent().find('#cboOrderNo').val();
		url+='&graphicNo='+URLEncode(graphicNo);
		url+='&salesOrderNo='+URLEncode(salesOrderNo);;
		url+='&combo='+$(this).parent().parent().find('#cboCombo').val();
		url+='&print='+$(this).parent().parent().find('#cboPart').val();
		url+='&location='+$('#frmBulkDailyProduction #cboLocation').val();
		url+='&dateFrom='+dateFrom;
		window.open(url);
	}
  });


//----------------------------------------------------------------------------------------------------
$('#frmBulkDailyProduction #cboDesc').die('change').live('change', function () {

    loadCustomer($(this));
});
//-----------------------------------------------------------------------------------------------------
//laodModule
$('#frmBulkDailyProduction #cboSection').die('change').live('change', function () {
    var section = $(this).val();
    var row = this.parentNode.parentNode.rowIndex;
    if (section != '') {
        var url = basepath + "bulkDailyProduction-db-get.php?requestType=loadModules&section=" + section;
        var httpobj = $.ajax({url: url, async: false})
        $(this).parent().parent().find('#cboModule').html(httpobj.responseText);
       
    }

});

//------------------------------------------------------------------------------------------

//load customer
function loadCustomer(objThis) {
 
    var desc=objThis.parent().parent().find("#cboDesc").val();
    var url = basepath + "bulkDailyProduction-db-get.php?requestType=loadCustomer&desc=" + URLEncode(desc);
    var obj = $.ajax({url: url, async: false});
    objThis.parent().parent().find("#cboCustomer").html(obj.responseText);
//    $('#cboCustomer').html(obj.responseText);
}

//--------------------------------------------------------------------------------------------
//load graphic no
$('#frmBulkDailyProduction #cboCustomer').die('change').live('change', function () {
    var intCustomer = $(this).val();
    var desc=$(this).parent().parent().find('#cboDesc').val();

    var row = this.parentNode.parentNode.rowIndex;

    $(this).parent().parent().find('#cboOrderNo').val('');
    $(this).parent().parent().find('#cboSalesOrderNo').val('');
    $(this).parent().parent().find('#cboStyleNo').val('');
    $(this).parent().parent().find('#cboGroundColor').val('');
    $(this).parent().parent().find('#cboPart').val('');
    $(this).parent().parent().find('#cboCombo').val('');
    $(this).parent().parent().find('#cboPrintColor').val('');
    $(this).parent().parent().find('#txtFabricInQty').val(0);
    $(this).parent().parent().find('#txtProductionInQty').val(0);
    $(this).parent().parent().find('#txtDispatchQty').val(0);
    $(this).parent().parent().find('#txtBalToProdQty').val(0);
    $(this).parent().parent().find('#txtActualProductionQty').val(0);
    $(this).parent().parent().find('#txtNoOfShots').val(0);
    $(this).parent().parent().find('#txtNoUps').val(0);

    if (intCustomer != '') {
        var url = basepath + "bulkDailyProduction-db-get.php?requestType=loadGraphicNo&intCustomer=" + intCustomer+"&desc="+URLEncode(desc);
        var httpobj = $.ajax({url: url, async: false})
        $(this).parent().parent().find('#cboGraphicNo').html(httpobj.responseText);

    }

});


//----------------------------------------------------------------------------------------------

//load order no

$('#frmBulkDailyProduction #cboGraphicNo').die('change').live('change', function () {
    var intCustomer = $(this).parent().parent().find('#cboCustomer').val();
    var graphicNo = $(this).val();
    var row = this.parentNode.parentNode.rowIndex;

    $(this).parent().parent().find('#cboSalesOrderNo').val('');
    $(this).parent().parent().find('#cboStyleNo').val('');
    $(this).parent().parent().find('#cboGroundColor').val('');
    $(this).parent().parent().find('#cboPart').val('');
    $(this).parent().parent().find('#cboCombo').val('');
    $(this).parent().parent().find('#cboPrintColor').val('');
    $(this).parent().parent().find('#txtFabricInQty').val(0);
    $(this).parent().parent().find('#txtProductionInQty').val(0);
    $(this).parent().parent().find('#txtDispatchQty').val(0);
    $(this).parent().parent().find('#txtBalToProdQty').val(0);
    $(this).parent().parent().find('#txtActualProductionQty').val(0);
    $(this).parent().parent().find('#txtNoOfShots').val(0);
    $(this).parent().parent().find('#txtNoUps').val(0);

    if (graphicNo != '') {
        var url = basepath + "bulkDailyProduction-db-get.php?requestType=loadOrderNo&intCustomer=" + intCustomer + "&graphicNo=" + URLEncode(graphicNo);
        var httpobj = $.ajax({url: url, async: false})
        $(this).parent().parent().find('#cboOrderNo').html(httpobj.responseText);    
    }else{
        var url = basepath + "bulkDailyProduction-db-get.php?requestType=loadOrderNo&intCustomer=" + intCustomer + "&graphicNo=" + URLEncode(graphicNo);
        var httpobj = $.ajax({url: url, async: false})
        $(this).parent().parent().find('#cboOrderNo').html(httpobj.responseText);
   
    }


});

//----------------------------------------------------------------------------------------------
$('#frmBulkDailyProduction #cboOrderNo').die('change').live('change', function () {
    var intCustomer = $(this).parent().parent().find('#cboCustomer').val();
    var graphicNo = $(this).parent().parent().find('#cboGraphicNo').val();
    var orderNo = $(this).parent().parent().find('#cboOrderNo').val();
    var row = this.parentNode.parentNode.rowIndex;

    $(this).parent().parent().find('#cboStyleNo').val('');
    $(this).parent().parent().find('#cboGroundColor').val('');
    $(this).parent().parent().find('#cboPart').val('');
    $(this).parent().parent().find('#cboCombo').val('');
    $(this).parent().parent().find('#cboPrintColor').val('');
    $(this).parent().parent().find('#txtFabricInQty').val(0);
    $(this).parent().parent().find('#txtProductionInQty').val(0);
    $(this).parent().parent().find('#txtDispatchQty').val(0);
    $(this).parent().parent().find('#txtBalToProdQty').val(0);
    $(this).parent().parent().find('#txtActualProductionQty').val(0);
    $(this).parent().parent().find('#txtNoOfShots').val(0);
    $(this).parent().parent().find('#txtNoUps').val(0);


    if (orderNo != '') {
        var url = basepath + "bulkDailyProduction-db-get.php?requestType=loadViewOrder&intCustomer=" + intCustomer + "&graphicNo=" + URLEncode(graphicNo) + "&orderNo=" + orderNo;
        var httpobj = $.ajax({url: url, async: false})
        $(this).parent().parent().find('#viewOrder').html(httpobj.responseText);
        

    }

});
//---------------------------------------------------------------------------------------
//load salesorderno
$('#frmBulkDailyProduction #cboOrderNo').die('change').live('change', function () {
    var intCustomer = $(this).parent().parent().find('#cboCustomer').val();
    var graphicNo = $(this).parent().parent().find('#cboGraphicNo').val();
    var orderNo = $(this).parent().parent().find('#cboOrderNo').val();
    var row = this.parentNode.parentNode.rowIndex;

    $(this).parent().parent().find('#cboStyleNo').val('');
    $(this).parent().parent().find('#cboGroundColor').val('');
    $(this).parent().parent().find('#cboPart').val('');
    $(this).parent().parent().find('#cboCombo').val('');
    $(this).parent().parent().find('#cboPrintColor').val('');
    $(this).parent().parent().find('#txtFabricInQty').val(0);
    $(this).parent().parent().find('#txtProductionInQty').val(0);
    $(this).parent().parent().find('#txtDispatchQty').val(0);
    $(this).parent().parent().find('#txtBalToProdQty').val(0);
    $(this).parent().parent().find('#txtActualProductionQty').val(0);
    $(this).parent().parent().find('#txtNoOfShots').val(0);
    $(this).parent().parent().find('#txtNoUps').val(0);


    if (orderNo != '') {
        var url = basepath + "bulkDailyProduction-db-get.php?requestType=loadSalesOrderNo&intCustomer=" + intCustomer + "&graphicNo=" + URLEncode(graphicNo) + "&orderNo=" + orderNo;
        var httpobj = $.ajax({url: url, async: false})
        $(this).parent().parent().find('#cboSalesOrderNo').html(httpobj.responseText);
        

    }

});


//----------------------------------------------------------------------------------------------
//load style no
$('#frmBulkDailyProduction #cboSalesOrderNo').die('change').live('change', function () {
    var intCustomer = $(this).parent().parent().find('#cboCustomer').val();
    var graphicNo = $(this).parent().parent().find('#cboGraphicNo').val();
    var orderNo = $(this).parent().parent().find('#cboOrderNo').val();
    var salesOrderNo = $(this).parent().parent().find('#cboSalesOrderNo').val();
    var styleNo = $(this).parent().parent().find('#cboStyleNo').val();

    var row = this.parentNode.parentNode.rowIndex;

    $(this).parent().parent().find('#cboGroundColor').val('');
    $(this).parent().parent().find('#cboPart').val('');
    $(this).parent().parent().find('#cboCombo').val('');
    $(this).parent().parent().find('#cboPrintColor').val('');
    $(this).parent().parent().find('#txtFabricInQty').val(0);
    $(this).parent().parent().find('#txtProductionInQty').val(0);
    $(this).parent().parent().find('#txtDispatchQty').val(0);
    $(this).parent().parent().find('#txtBalToProdQty').val(0);
    $(this).parent().parent().find('#txtActualProductionQty').val(0);
    $(this).parent().parent().find('#txtNoOfShots').val(0);
    $(this).parent().parent().find('#txtNoUps').val(0);


    if (salesOrderNo != '') {
        var url = basepath + "bulkDailyProduction-db-get.php?requestType=loadStyleNo&intCustomer=" + intCustomer + "&graphicNo=" + URLEncode(graphicNo) + "&orderNo=" + orderNo + "&salesOrderNo=" + URLEncode(salesOrderNo);
        var httpobj = $.ajax({url: url, async: false})
        $(this).parent().parent().find('#cboStyleNo').html(httpobj.responseText);
    }
    if(salesOrderNo != ''){
        var url = basepath + "bulkDailyProduction-db-get.php?requestType=loadGroundColor&intCustomer=" + intCustomer + "&graphicNo=" + URLEncode(graphicNo) + "&orderNo=" + orderNo + "&salesOrderNo=" + URLEncode(salesOrderNo) + "&styleNo=" + URLEncode(styleNo);
        var httpobj = $.ajax({url: url, async: false})
        $(this).parent().parent().find('#cboGroundColor').html(httpobj.responseText);
    }
   


});

//----------------------------------------------------------------------------------------------
//load ground color
$('#frmBulkDailyProduction #cboStyleNo').die('change').live('change', function () {
    var intCustomer = $(this).parent().parent().find('#cboCustomer').val();
    var graphicNo = $(this).parent().parent().find('#cboGraphicNo').val();
    var orderNo = $(this).parent().parent().find('#cboOrderNo').val();
    var salesOrderNo = $(this).parent().parent().find('#cboSalesOrderNo').val();
    var styleNo = $(this).parent().parent().find('#cboStyleNo').val();

    var row = this.parentNode.parentNode.rowIndex;

    $(this).parent().parent().find('#cboPart').val('');
    $(this).parent().parent().find('#cboCombo').val('');
    $(this).parent().parent().find('#cboPrintColor').val('');
    $(this).parent().parent().find('#txtFabricInQty').val(0);
    $(this).parent().parent().find('#txtProductionInQty').val(0);
    $(this).parent().parent().find('#txtDispatchQty').val(0);
    $(this).parent().parent().find('#txtBalToProdQty').val(0);
    $(this).parent().parent().find('#txtActualProductionQty').val(0);
    $(this).parent().parent().find('#txtNoOfShots').val(0);
    $(this).parent().parent().find('#txtNoUps').val(0);


    if (styleNo != '') {
        var url = basepath + "bulkDailyProduction-db-get.php?requestType=loadGroundColor&intCustomer=" + intCustomer + "&graphicNo=" + URLEncode(graphicNo) + "&orderNo=" + orderNo + "&salesOrderNo=" + URLEncode(salesOrderNo) + "&styleNo=" + URLEncode(styleNo);
        var httpobj = $.ajax({url: url, async: false})
        $(this).parent().parent().find('#cboGroundColor').html(httpobj.responseText);

    }
    
});
//----------------------------------------------------------------------------------------------
// load part
$('#frmBulkDailyProduction #cboGroundColor').die('change').live('change', function () {
    var intCustomer = $(this).parent().parent().find('#cboCustomer').val();
    var graphicNo = $(this).parent().parent().find('#cboGraphicNo').val();
    var orderNo = $(this).parent().parent().find('#cboOrderNo').val();
    var salesOrderNo = $(this).parent().parent().find('#cboSalesOrderNo').val();
    var styleNo = $(this).parent().parent().find('#cboStyleNo').val();
    var groundColor = $(this).parent().parent().find('#cboGroundColor').val();


    var row = this.parentNode.parentNode.rowIndex;

    $(this).parent().parent().find('#cboCombo').val('');
    // $(this).parent().parent().find('#cboPrintColor').val('');
    $(this).parent().parent().find('#txtFabricInQty').val(0);
    $(this).parent().parent().find('#txtProductionInQty').val(0);
    $(this).parent().parent().find('#txtDispatchQty').val(0);
    $(this).parent().parent().find('#txtBalToProdQty').val(0);
    $(this).parent().parent().find('#txtActualProductionQty').val(0);
    $(this).parent().parent().find('#txtNoOfShots').val(0);
    $(this).parent().parent().find('#txtNoUps').val(0);


    if (groundColor != '') {
        var url = basepath + "bulkDailyProduction-db-get.php?requestType=loadPrintPart&intCustomer=" + intCustomer + "&graphicNo=" + URLEncode(graphicNo) + "&orderNo=" + orderNo + "&salesOrderNo=" + URLEncode(salesOrderNo) + "&styleNo=" + URLEncode(styleNo) + "&groundColor=" + URLEncode(groundColor);
        var httpobj = $.ajax({url: url, async: false})
        $(this).parent().parent().find('#cboPart').html(httpobj.responseText);
    }

});


//---------------------------------------------------------------------------------------------------
//load combo
$('#frmBulkDailyProduction #cboPart').die('change').live('change', function () {

    var intCustomer = $(this).parent().parent().find('#cboCustomer').val();
    var graphicNo = $(this).parent().parent().find('#cboGraphicNo').val();
    var orderNo = $(this).parent().parent().find('#cboOrderNo').val();
    var salesOrderNo = $(this).parent().parent().find('#cboSalesOrderNo').val();
    var styleNo = $(this).parent().parent().find('#cboStyleNo').val();
    var groundColor = $(this).parent().parent().find('#cboGroundColor').val();
    var part = $(this).parent().parent().find('#cboPart').val();

    var row = this.parentNode.parentNode.rowIndex;

    $(this).parent().parent().find('#cboPrintColor').val('');
    $(this).parent().parent().find('#txtFabricInQty').val(0);
    $(this).parent().parent().find('#txtProductionInQty').val(0);
    $(this).parent().parent().find('#txtDispatchQty').val(0);
    $(this).parent().parent().find('#txtActualProductionQty').val(0);
    $(this).parent().parent().find('#txtNoOfShots').val(0);
    $(this).parent().parent().find('#txtNoUps').val(0);

    if (part != '') {
        var url = basepath + "bulkDailyProduction-db-get.php?requestType=loadCombo&intCustomer" + intCustomer + "&graphicNo=" + URLEncode(graphicNo) + "&orderNo=" + orderNo + "&salesOrderNo=" + URLEncode(salesOrderNo) + "&styleNo=" + URLEncode(styleNo) + "&groundColor=" + URLEncode(groundColor) + "&part=" + URLEncode(part);
        var httpobj = $.ajax({url: url, async: false})
        $(this).parent().parent().find('#cboCombo').html(httpobj.responseText);


    }

});
$("#frmBulkDailyProduction .chosen-select").chosen({allow_single_deselect: false , no_results_text: "Oops, nothing found!",width:"100%"});

// $("#cboCombo").die('change').live('change',function()
// {
//
// });



//---------------------------------------------------------------------------------------------------
$('#frmBulkDailyProduction #cboCombo').die('change').live('change', function () {
    var ob =  $(this).parent().parent().find('#cboPart').val();
    var intCustomer = $(this).parent().parent().find('#cboCustomer').val();
    var graphicNo = $(this).parent().parent().find('#cboGraphicNo').val();
    var orderNo = $(this).parent().parent().find('#cboOrderNo').val();
    var salesOrderNo = $(this).parent().parent().find('#cboSalesOrderNo').val();
    var styleNo = $(this).parent().parent().find('#cboStyleNo').val();
    var groundColor = $(this).parent().parent().find('#cboGroundColor').val();
    var part = $(this).parent().parent().find('#cboPart').val();
    var combo = $(this).parent().parent().find('#cboCombo').val();
    var chkStatus=$(this).parent().parent().find('#chkProduction').val();
    var row = this.parentNode.parentNode.rowIndex;
    
    var obj = $(this);


    var url = basepath + "bulkDailyProduction-db-get.php?requestType=loadPrintColor&intCustomer" + intCustomer + "&graphicNo=" + URLEncode(graphicNo) + "&orderNo=" + orderNo + "&salesOrderNo=" + URLEncode(salesOrderNo) + "&styleNo=" + URLEncode(styleNo) + "&groundColor=" + URLEncode(groundColor) + "&part=" + part + "&combo=" +combo;
    $.ajax({
        url:url,
        dataType:'json',
        async:false,
        success:function(json)
        {
          obj.parent().parent().find('#cboPrintColor').html(json.colorsHTML);
          obj.parent().parent().find('.clsorder').trigger("chosen:updated");
        }
    });

    if (combo != '') {
        var url = basepath + "bulkDailyProduction-db-get.php?requestType=loadUps&intCustomer" + intCustomer + "&graphicNo=" + URLEncode(graphicNo) + "&orderNo=" + orderNo + "&salesOrderNo=" + URLEncode(salesOrderNo) + "&styleNo=" + URLEncode(styleNo) + "&groundColor=" + URLEncode(groundColor) + "&part=" + URLEncode(part) + "&combo=" + URLEncode(combo);
        loadUps(url,$(this));
    }

});
function loadShots(url,ob) {
    var obj2 =  $.ajax({
        url:url,
        dataType: "json",
        async:false,
        success:function(json){
            ob.parent().parent().find('#plannedShots').val(json.shots);
            ob.parent().parent().find('#txtNoOfShots').val(json.shots);
        }
    });
}      
          
          


$('#frmBulkDailyProduction #cboPrintColor').die('change').live('change', function () {

    var intCustomer = $(this).parent().parent().find('#cboCustomer').val();
    var graphicNo = $(this).parent().parent().find('#cboGraphicNo').val();
    var orderNo = $(this).parent().parent().find('#cboOrderNo').val();
    var salesOrderNo = $(this).parent().parent().find('#cboSalesOrderNo').val();
    var styleNo = $(this).parent().parent().find('#cboStyleNo').val();
    var groundColor = $(this).parent().parent().find('#cboGroundColor').val();
    var part = $(this).parent().parent().find('#cboPart').val();
    var combo = $(this).parent().parent().find('#cboCombo').val();
    var chkStatus=$(this).parent().parent().find('#chkProduction').val();
    var printColor=$(this).parent().parent().find('#cboPrintColor').val();

    var row = this.parentNode.parentNode.rowIndex;

    $(this).parent().parent().find('#txtFabricInQty').val(0);
    $(this).parent().parent().find('#txtProductionInQty').val(0);
    $(this).parent().parent().find('#txtDispatchQty').val(0);
    $(this).parent().parent().find('#txtActualProductionQty').val(0);

    if (printColor != '') {
        var url = basepath + "bulkDailyProduction-db-get.php?requestType=loadQty&intCustomer" + intCustomer + "&graphicNo=" + URLEncode(graphicNo) + "&orderNo=" + orderNo + "&salesOrderNo=" + URLEncode(salesOrderNo) + "&styleNo=" + URLEncode(styleNo) + "&groundColor=" +URLEncode(groundColor) + "&part=" + part+ "&combo=" + combo+"&chkstatus="+chkStatus+"&printColor="+printColor;
        loadQty(url,$(this));
    }
    if (printColor != '') {
        var url = basepath + "bulkDailyProduction-db-get.php?requestType=loadShots&intCustomer" + intCustomer + "&graphicNo=" + URLEncode(graphicNo) + "&orderNo=" + orderNo + "&salesOrderNo=" + URLEncode(salesOrderNo) + "&styleNo=" + URLEncode(styleNo) + "&groundColor=" + URLEncode(groundColor) + "&part=" + part + "&combo=" + combo+"&printColor="+printColor;
        loadShots(url,$(this));
    }

});

//function getCount(url,ob) {
//    var obj2 =  $.ajax({
//        url:url,
//        dataType: "json",
//        async:false,
//        success:function(json){
//            alert(val(json.count));
//            ob.parent().parent().find('#cboPrintColor').val(json.count);
//            var Rcount= ob.parent().parent().find($("#frmBulkDailyProduction #cboPrintColor :selected").length);
//            alert(Rcount);
//            
//        }
//    });
//}

function loadUps(url,ob) {
    var obj2 =  $.ajax({
        url:url,
        dataType: "json",
        async:false,
        success:function(json){
            ob.parent().parent().find('#plannedUps').val(json.ups);
            ob.parent().parent().find('#txtNoUps').val(json.ups)
        }
    });
}
function loadQty(url,ob){
    //alert(url);
    var obj2 =  $.ajax({
        url:url,
        dataType: "json",
        async:false,
        success:function(json){
            ob.parent().parent().find('#txtFabricInQty').val(json.fabricInQty);
            ob.parent().parent().find('#txtProductionInQty').val(json.productionQty);
            ob.parent().parent().find('#txtDispatchQty').val(json.dispatchQty);
            ob.parent().parent().find('#txtBalQty').val(json.balQty);

        }
    });
}

//---------------------------------------------------------------------------------------------------
$('#butReport').live('click', viewExcelReport);
//
function viewExcelReport(){
    
    var url =basepath1+'bulkProductionExcel.php';
    window.open(url);
}
$('#frmBulkDailyProduction #butNewReport').die('click').live('click',function() {
 if ($('#txtSerialNo').val() != ''){
      var date=$('#cboDate').val().split('-');
      var year=date[0];
    window.open('?q=1287&bulkNo=' + $('#txtSerialNo').val() + '&year=' + year);
  }
  else{
    alert("There is no Bulk Production No to view");
  }
    });

//-------------------------------------------------------------------------
$('#frmBulkDailyProduction #butSave').die('click').live('click', function () {

    var requestType = '';
    if ($('#frmBulkDailyProduction').validationEngine('validate')) {

        var data = "requestType=save";
        var date=$('#cboDate').val().split('-');
        var year=date[0];

        data +="&serialNo="+$('#txtSerialNo').val();
        data += "&Year=" + year;
        data += "&graphicNo=" + $('#cboGraphicNo').val();
        data += "&orderNo=" + $('#cboOrderNo').val();
        data += "&salesOrderNo=" + $('#cboSalesOrderNo').val();
        data += "&part=" + $('#cboPart').val();
        data += "&combo=" + $('#cboCombo').val();
        data += "&groundColor=" + $('#cboGroundColor').val();
        data += "&location=" + $('#cboLocation').val();
        data += "&printColor=" + $('#cboPrintColor').val();
        data += "&fQty=" + $('#txtFabricInQty').val();
        data += "&dispatchQty=" + $('#txtDispatchQty').val();
        data += "&productionInQty=" + $('#txtProductionInQty').val();
        data += "&actualQty=" + $('#txtActualProductionQty').val();
        data += "&balQty=" + $('#txtBalQty').val();
        data += "&noOfShots=" + $('#txtNoOfShots').val();
        data += "&noOfUps=" + $('#txtNoUps').val();
        data += "&styleNo=" + $('#cboStyleNo').val();
        data +="&dateOfProduction=" +$('#cboDate').val();



        var rowCount = document.getElementById('tblProduction').rows.length;

        if (rowCount == 1) {
            alert("No production items to save");
           // hideWaiting();
            return false;
        }
        var row = 0;

        var arr = "[";
        var salesIds = '';
        $('#tblProduction >tbody >tr').not(':last').not(':first').each(function () {
                if (!$(this).attr('id'))
                var salesOrderId = '';
            else
                var salesOrderId = $(this).find('#cboSalesOrderNo').val();
             
            var graphicNo =$(this).find('#cboGraphicNo').val();
            var fabricInQty = $(this).find('#txtFabricInQty').val();
            var dispatchQty = $(this).find('#txtDispatchQty').val();
            var actualQty = $(this).find('#txtActualProductionQty').val();
            var productionQty = $(this).find('#txtProductionInQty').val();
            var balQty = $(this).find('#txtBalQty').val();
            var orderNo = $(this).find('#cboOrderNo').val();
            var salesOrderNo = $(this).find('#cboSalesOrderNo').val();
            var part = $(this).find('#cboPart').val();
            var combo = $(this).find('#cboCombo').val();
            var groundColor = $(this).find('#cboGroundColor').val();
            var printColor = $(this).find('#cboPrintColor').val();
            var location = $(this).find('#cboLocation').val();
            var noOfShots = $(this).find('#txtNoOfShots').val();
            var noOfUps = $(this).find('#txtNoUps').val();
            var styleNo =$(this).find('#cboStyleNo').val();
            var dateOfProduction= $('#cboDate').val();
            var customer=$(this).find('#cboCustomer').val();
            var chkProduction	= 	$(this).find("#chkProduction").attr("checked") ? 1 : 0;
            var module=$(this).find('#cboModule').val();
            var section=$(this).find('#cboSection').val();
            var plannedUps=$(this).find('#plannedUps').val();
            var plannedShots=$(this).find('#plannedShots').val();
            var strRemarks=$(this).find('#strRemarks').val()  
                    salesIds += salesOrderId + ',';
                   

            arr += "{";

            arr += '"graphicNo":"' + URLEncode(graphicNo) + '",';
            arr += '"orderNo":"' + orderNo + '",';
            arr += '"salesOrderNo":"' + URLEncode(salesOrderNo) + '",';
            arr += '"part":"' + URLEncode(part) + '",';
            arr += '"combo":"' + URLEncode(combo) + '",';
            arr += '"groundColor":"' + URLEncode(groundColor) + '",';
            arr += '"noOfUps":"' + noOfUps + '",';
            arr += '"noOfShots":"' + noOfShots + '",';
            arr += '"plannedNoOfUps":"' + plannedUps + '",';
            arr += '"plannedNoOfShots":"' + plannedShots + '",';
            arr += '"printColor":"' + printColor+ '",';
            arr += '"module":"' + module+ '",';
            arr += '"section":"' + section+ '",';

            arr += '"fabricInQty":"' + fabricInQty + '",';
            arr += '"productionInQty":"' + productionQty + '",';
            arr += '"dispatchQty":"' + dispatchQty + '",';
            arr += '"actualQty":"' + actualQty + '",';
            arr += '"balQty":"' + balQty + '",';

            arr += '"dateOfProduction":"' + dateOfProduction + '",';
            arr += '"location":"' + location + '",';
            arr += '"styleNo":"' + styleNo + '",';
            arr += '"customer":"' + customer + '",';
            arr += '"chkProduction":"'+chkProduction+'",';
            arr += '"strRemarks":"'+strRemarks+'"';
            
            arr += '},';


                       
        
          
        });
    
        arr = arr.substr(0, arr.length - 1);
       // salesIds=salesIds.substr(0,salesIds.length-1);
        arr += " ]";
       
        // data += "&arr=" + arr;salesIds
        data += "&arr=" + arr + '&salesIds=' + salesIds;
        
        ///////////////////////////// save main infomations /////////////////////////////////////////
        var url = basepath + "bulkDailyProduction-db-set.php";

        var obj = $.ajax({
            url: url,

            dataType: "json",
            type: 'POST',
            data: data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
            //data:'{"requestType":"addsampleInfomations"}',
            async: false,

            success: function (json) {

                $('#frmBulkDailyProduction #butSave').validationEngine('showPrompt', json.msg, json.type /*'pass'*/);
                if (json.type == 'pass') {

                    var t = setTimeout("alertx()", 1000);
                    $('#txtSerialNo').val(json.serialNo);
                    $('#txtYear').val(json.year);                   
                    
                    window.location.href = '?q=1285&bulkNo='+json.serialNo+'&bulkYear='+json.year;
                    $('#butDayEnd').show();
                }
            },
            error: function (xhr, status) {

                $('#frmBulkDailyProduction #butSave').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
                //var t = setTimeout("alertx()", 3000);
                $('#butDayEnd').hide();


            }
        });
     //   hideWaiting();
    }
});

//--------------------------------------------------------------------------------------------------
$('#frmBulkDailyProduction #butDayEnd').die('click').live('click', function () {

    var requestType = '';
    if ($('#frmBulkDailyProduction').validationEngine('validate')) {
       // showWaiting();
        var data = "requestType=DayEnd";
        var date=$('#cboDate').val().split('-');
        var year=date[0];
             data +="&serialNo="+$('#txtSerialNo').val();
             data += "&Year=" + year;
        var url = basepath + "bulkDailyProduction-db-set.php";
        var obj = $.ajax({
            url: url,

            dataType: "json",
            type: 'POST',
            data: data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
            //data:'{"requestType":"addsampleInfomations"}',
            async: false,

            success: function (json) {
                $('#frmBulkDailyProduction #butDayEnd').validationEngine('showPrompt', json.msg, json.type /*'pass'*/);
                if (json.type == 'pass') {

                    var t = setTimeout("alertx()", 1000);                    
                    $('#txtSerialNo').val(json.serialNo);
                    $('#txtYear').val(json.year);
                    $('#butRevise').show();
                    $('#butSave').hide();
                    $("#frmBulkDailyProduction *").attr("disabled","disabled");

                }
            },
            error: function (xhr, status) {

                $('#frmBulkDailyProduction #butDayEnd').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
                var t = setTimeout("alertx()", 3000);
                $('#butRevise').hide();

            }
        });
       // hideWaiting();
    }
});

//--------------------------------------------------------------------------------------------------
$('#frmBulkDailyProduction #butNew').click(function () {
    document.location.href = '?q=1285';
});

//--------------------------------------------------------------------------------------------------

$('#frmBulkDailyProduction #butClose').click(function () {

});

//--------------------------------------------------------------------------------------------------
$('#frmBulkDailyProduction #butRevise').die('click').live('click', function () {
    $('#butDayEnd').show();
    $('#butSave').show();
    $('#butRevise').hide();
   // $("#frmBulkDailyProduction #txtActualProductionQty").removeAttr("disabled", true);
    $("#frmBulkDailyProduction *").removeAttr("disabled",true);

});



$('#frmBulkDailyProduction #butCopy').click(function() {
        $('#txtSerialNo').val('');
        $('#txtYear').val('');
        $("#chkProduction").prop("checked", false);
        var rowIndexForSo = $('#frmBulkDailyProduction #tblProduction #butInsertRow').parent().parent().index()-1;
        $('#cboDate').prop( "disabled", false );
        for (var index = 1; index <=rowIndexForSo; index ++){
            $('#frmBulkDailyProduction #tblProduction ').parent().parent().parent().find('tr:eq('+ index +') #cboSection').attr('disabled',false);
        }

        var currentTime = new Date();
        var month 		= currentTime.getMonth()+1 ;
        var day 		= currentTime.getDate();
        var year 		= currentTime.getFullYear();
        var	d			= year+'-'+month+'-'+day;
        $('#dtDate').val(d);
        $('#dtDate').prop( "disabled", false );

        var errFlag=0;
        var qty=$('#txtActualProductionQty').val();
            if(qty !='')
                errFlag=errFlag+0;
            else
                errFlag=1;

        
        if(errFlag==0){
            alert('copied successfully.')
        }
        else
            alert('failed to copy');


        $('#butSave').show();
    });


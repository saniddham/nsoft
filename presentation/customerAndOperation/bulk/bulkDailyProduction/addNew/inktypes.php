<?php
/**
 * Created by PhpStorm.
 * User: Nadeeshani
 * Date: 10/23/2019
 * Time: 10:48 AM
 */

session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];

include $backwardseperator."dataAccess/Connector.php";
$order	=	$_REQUEST['orderNo'];
$getOrderNo=explode('/',$order);
$orderNo=$getOrderNo[0];


?>
<form id="frmInkTypes" name="frmInkTypes" method="post" action="inktypes.php">
<table width="450" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutS">
		  <div class="trans_text"> Ink Types</div>
		  <table width="450" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="451" border="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><div id="divColors" style="width:450px;height:300px;overflow:scroll" >
        <?php


        ?>
                       <table width="395" class="bordered" id="tblProcess">
                            <tr>
                                 <th width="10%">SELECT</th>
                                <th width="40%">Print Color</th>                               
                                <th width="50%">Ink Types</th>
                            </tr>
                            <?php
                            $sql_r	  = "SELECT DISTINCT
	trn_sampleinfomations_details.intSampleNo,
	trn_sampleinfomations_details.intSampleYear,
	MAX(
		trn_sampleinfomations_details.intRevNo
	) as RevNo,
	trn_sampleinfomations_details.strPrintName,
	trn_sampleinfomations_details.strComboName
FROM
	trn_sampleinfomations_details
INNER JOIN trn_orderdetails ON trn_orderdetails.intSampleNo = trn_sampleinfomations_details.intSampleNo
INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
WHERE
	trn_orderdetails.intOrderNo = '$orderNo'";
                            //echo $sql_r;
                            $result_r = $db->RunQuery($sql_r);
                            $row_r=mysqli_fetch_array($result_r);
                            $sampNo = $row_r['intSampleNo'];
                            $sampYear = $row_r['intSampleYear'];
                            $revNo = $row_r['RevNo'];
                            $print = $row_r['strPrintName'];
                            $combo = $row_r['strComboName'];

				 	$sql = "SELECT
	trn_sampleinfomations_details_technical.intColorId,
	trn_sampleinfomations_details_technical.intInkTypeId AS intInkType,
	mst_inktypes.strName AS inkTypeName,
	mst_colors.strName AS colorName
FROM
	trn_sampleinfomations_details_technical
INNER JOIN mst_inktypes ON mst_inktypes.intId = trn_sampleinfomations_details_technical.intInkTypeId
INNER JOIN mst_colors ON mst_colors.intId = trn_sampleinfomations_details_technical.intColorId
INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_details_technical.intSampleNo
AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear
AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_details_technical.intRevNo
AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_details_technical.strPrintName
AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_details_technical.strComboName
AND trn_sampleinfomations_details.intColorId = trn_sampleinfomations_details_technical.intColorId
WHERE
	trn_sampleinfomations_details_technical.intSampleNo = '$sampNo'
AND trn_sampleinfomations_details_technical.intSampleYear = '$sampYear'
AND trn_sampleinfomations_details_technical.intRevNo = '$revNo'
AND trn_sampleinfomations_details_technical.strPrintName = '$print'
AND trn_sampleinfomations_details_technical.strComboName = '$combo'
GROUP BY
	trn_sampleinfomations_details_technical.intColorId,
	trn_sampleinfomations_details_technical.intInkTypeId";
				 	//echo $sql;
                            
                    $result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{

			  ?>
            	<tr class="normalfnt" >
              		
                    <td  bgcolor="#FFFFFF" align="center"><input align="center" class="chkProcess" value="<?Php echo $row['intColorId']; ?>" type="checkbox" id="chkDisp" <?php if($row['inkTypeName']!=''){ ?> checked="checked" <?php } ?>/></div>
                        <div class="cls_pa_routing" style="display:none"><?php echo $routing_id; ?></td>
                        <td align="center" bgcolor="#FFFFFF"><?php echo $row['colorName']; ?></td>
                        <td><?php echo $row['inkTypeName'];?></td>
                </tr>

              <?php
					}
			  ?>
            </table>
            </td></tr>
            </table>
          </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound">
       
        <img src="images/Tadd.jpg" width="92" height="24"  alt="add" id="butAdd" name="butAdd" class="mouseover"/>
        <img src="images/Tclose.jpg" width="92" height="24" id="butClose1" name="butClose1" class="mouseover"  /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
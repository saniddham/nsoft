<?php

session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$userId = $_SESSION['userId'];
$location = $_SESSION['CompanyID'];
$company = $_SESSION['headCompanyId'];
$requestType = $_REQUEST['requestType'];
include "{$backwardseperator}dataAccess/Connector.php";


$programName = 'Bulk Daily Production';
$programCode = 'P1285';

require_once "{$backwardseperator}class/customerAndOperation/cls_textile_stores.php";

$requestType = isset($_REQUEST['requestType']) ? $_REQUEST['requestType'] : null;
if (isset($_REQUEST["val"])) {
    //$val = preg_replace('/[^A-Za-z0-9\-]/', '', $_REQUEST["val"]);
    $val=$_REQUEST["val"];
    if (!isset($_REQUEST["page"])) {
        $_REQUEST["page"] = 1;
    }
    if ($val == 'ALLSELECT') {
        $val = '';
    }

    $page = $_REQUEST["page"];
    $year = $_REQUEST["sampleYear"];
    $start = 10 * $_REQUEST["page"] - 10;
    $end = $_REQUEST["page"] * 10;

    $sql = "SELECT DISTINCT
	CONCAT(
		trn_orderdetails.strGraphicNo,
		' | ',
		trn_orderdetails.strSalesOrderNo,
		' | ',
		trn_orderdetails.strStyleNo
	) AS searchString
FROM
	trn_orderdetails
INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
WHERE
	(trn_orderdetails.strGraphicNo LIKE '%$val%' OR trn_orderdetails.strSalesOrderNo LIKE '%$val%' OR trn_orderdetails.strStyleNo LIKE '%$val%')
AND mst_locations.intCompanyId = '$company'
		";
    $limitsql = $sql . " LIMIT $start,$end";
//echo $sql;
    $total = $db->RunQuery($sql);
    $rowcount = mysqli_num_rows($total);
    $data['total_count'] = $rowcount;
    $data['items'] = array();

    $result = $db->RunQuery($limitsql);
    if ($_REQUEST["page"] == 1){
        $valUes['id'] = " ";
        $valUes['sampleNO'] = " ";
        array_push($data['items'], $valUes);
    }
    while ($row = mysqli_fetch_array($result)) {
        $valUes['id'] = $row['searchString'];
        $valUes['sampleNO'] = $row['searchString'];

        array_push($data['items'], $valUes);
    }
   
    echo json_encode($data);
    die;
}

///////////////
elseif ($requestType == 'loadCustomer') {
    $desc = $_REQUEST['desc'];
    $splitedString = explode(' | ', $desc);


    if ($desc != '') {
        $para = " and trn_orderdetails.strGraphicNo LIKE  '$splitedString[0]' || trn_orderdetails.strSalesOrderNo LIKE  '$splitedString[1]' || trn_orderdetails.strStyleNo LIKE  '$splitedString[2]'";
        $sql = "SELECT DISTINCT
	mst_customer.intId,
	mst_customer.strName
FROM
	mst_customer
INNER JOIN trn_orderheader ON trn_orderheader.intCustomer = mst_customer.intId
INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
WHERE
	mst_customer.intStatus = 1
					 $para
				ORDER BY mst_customer.strName
				";
        echo $sql;
        $result = $db->RunQuery($sql);
        echo "<option value=\"\"></option>";
        while ($row = mysqli_fetch_array($result)) {
            echo "<option value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
        }
    } else {
        $sql = "SELECT DISTINCT
                                                                        mst_customer.intId,
                                                                        mst_customer.strName
                                                                    FROM
                                                                        mst_customer
                                                                    WHERE
                                                                         mst_customer.intStatus=1 ORDER BY mst_customer.strName
				";
        $result = $db->RunQuery($sql);
        echo "<option value=\"\"></option>";
        while ($row = mysqli_fetch_array($result)) {
            echo "<option value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
        }
    }
}
//////////////
else if ($requestType == 'loadGraphicNo') {
    $intCustomer = $_REQUEST['intCustomer'];
    $desc = $_REQUEST['desc'];
    $splitedString = explode(' | ', $desc);
    if ($desc != '') {
        $sqlp = "SELECT DISTINCT
	trn_orderdetails.strGraphicNo
FROM
	trn_orderdetails
INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
WHERE
	mst_customer.intStatus = 1
AND trn_orderdetails.strGraphicNo LIKE '%$splitedString[0]%'
AND trn_orderheader.intCustomer = '$intCustomer'
AND mst_locations.intCompanyId = '$company'
ORDER BY
	mst_customer.strName";
        
    echo $sqlp;
        $html = "<option value=\"\"></option>";
        $resultp = $db->RunQuery($sqlp);
        while ($row = mysqli_fetch_array($resultp)) {

            $html .= "<option value=\"" . $row['strGraphicNo'] . "\">" . $row['strGraphicNo'] . "</option>";
        }
    } else {
        $sql = "SELECT DISTINCT
	trn_orderdetails.strGraphicNo
FROM
	trn_orderdetails
INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
WHERE
	mst_customer.intStatus = 1
AND trn_orderheader.intCustomer = '$intCustomer'
AND mst_locations.intCompanyId = '$company'
ORDER BY
	mst_customer.strName";
        echo $sql;

        $html = "<option value=\"\"></option>";
        $result = $db->RunQuery($sql);
        while ($row = mysqli_fetch_array($result)) {

            $html .= "<option value=\"" . $row['strGraphicNo'] . "\">" . $row['strGraphicNo'] . "</option>";
        }
    }
    echo($html);
} //------------------------------
else if ($requestType == 'loadOrderNo') {
    $graphicNo = $_REQUEST['graphicNo'];
    $intCustomer = $_REQUEST['intCustomer'];
    if($graphicNo!=''){
        $para = "AND trn_orderdetails.strGraphicNo = '$graphicNo'";
    $sql = "SELECT DISTINCT
                                                                        CONCAT(
                                                                                trn_orderdetails.intOrderNo,
                                                                                '/',
                                                                                trn_orderdetails.intOrderYear
                                                                        ) AS intOrderNo,
                                                                        trn_orderheader.intStatus,
                                                                        trn_orderheader.PO_TYPE
                                                                FROM
                                                                        trn_orderdetails
                                                                INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo
                                                                INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
                                                                WHERE
                                                                        trn_orderheader.PO_TYPE != 1      
                                                                         AND mst_locations.intCompanyId = '$company'
                                                                AND trn_orderheader.intStatus NOT IN (- 10 ,- 2 ,4) 
                                                                AND trn_orderheader.intCustomer = '$intCustomer' $para";
    echo $sql;
    }else{
         $html = "<option selected=\"\" value=\"\"></option>";
    }
   // echo $sql;
//    $sql="SELECT DISTINCT
//                                                                        CONCAT(
//                                                                                trn_orderdetails.intOrderNo,
//                                                                                '/',
//                                                                        trn_orderdetails.intOrderYear 
//                                                                       ) AS intOrderNo
//                                                               FROM
//                                                                        trn_orderdetails 
//                                                               INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo 
//                                                               WHERE
//                                                                        trn_orderdetails.strGraphicNo = '$graphicNo' ";
    // echo $sql;
    $html = "<option value=\"\"></option>";
    $result = $db->RunQuery($sql);
    while ($row = mysqli_fetch_array($result)) {
        $html .= "<option value=\"" . $row['intOrderNo'] . "\">" . $row['intOrderNo'] . "</option>";
    }
    echo($html);
} //------------------------------
else if ($requestType == 'loadSalesOrderNo') {
    $graphicNo = $_REQUEST['graphicNo'];
    $orderNoArr = explode('/', $_REQUEST['orderNo']);
    $orderNo = $orderNoArr[0];
    $orderYear = $orderNoArr[1];

    
         $sql = "SELECT DISTINCT
            trn_orderdetails.strSalesOrderNo,
            trn_orderdetails.SO_TYPE
            FROM
              trn_orderdetails
            WHERE
              trn_orderdetails.strGraphicNo = '$graphicNo'
            AND trn_orderdetails.intOrderNo = '$orderNo'";

    $html = "<option value=\"\"></option>";
    $result = $db->RunQuery($sql);
    while ($row = mysqli_fetch_array($result)) {

        $soName = $row['strSalesOrderNo'];
        $soValue = $row['strSalesOrderNo'];
        if ($row['SO_TYPE']== 2){
            $soName.="-STICKER_MAKING";
        }
        $html .= "<option value=\"" . $soValue . "\">" . $soName . "</option>";
    }
    echo($html);
} else if ($requestType == 'loadStyleNo') {
    $graphicNo = $_REQUEST['graphicNo'];
    $orderNoArr = explode('/', $_REQUEST['orderNo']);
    $orderNo = $orderNoArr[0];
    $orderYear = $orderNoArr[1];
    $salesOrderNo =$_REQUEST['salesOrderNo'];


    $sql = "SELECT DISTINCT
          IFNULL(trn_orderdetails.strStyleNo,'null') as strStyleNo
          FROM
              trn_orderdetails
          INNER JOIN trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo
          AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear
          AND trn_orderdetails.strGraphicNo = trn_sampleinfomations.strGraphicRefNo
          WHERE
              trn_orderdetails.strGraphicNo = '$graphicNo'
              AND trn_orderdetails.intOrderNo = '$orderNo'
              AND trn_orderdetails.strSalesOrderNo='$salesOrderNo'";
    echo $sql;
    $html = "<option value=\"\"></option>";
    $result = $db->RunQuery($sql);
    while ($row = mysqli_fetch_array($result)) {

        $html .= "<option value=\"" . $row['strStyleNo'] . "\">" . $row['strStyleNo'] . "</option>";
    }
    echo($html);
} //----------------------------------
else if ($requestType == 'loadGroundColor') {

    $graphicNo = $_REQUEST['graphicNo'];
    $orderNoArr = explode('/', $_REQUEST['orderNo']);
    $orderNo = $orderNoArr[0];
    $orderYear = $orderNoArr[1];
    $salesOrderNo =$_REQUEST['salesOrderNo'];


    $sql = "SELECT DISTINCT
	trn_sampleinfomations_details.intGroundColor,
	mst_colors_ground.strName
FROM
	trn_sampleinfomations_details
INNER JOIN trn_orderdetails ON trn_orderdetails.intSampleNo = trn_sampleinfomations_details.intSampleNo
AND trn_sampleinfomations_details.intSampleYear = trn_orderdetails.intSampleYear
AND trn_sampleinfomations_details.intRevNo = trn_orderdetails.intRevisionNo
AND trn_sampleinfomations_details.strPrintName = trn_orderdetails.strPrintName
AND trn_sampleinfomations_details.strComboName = trn_orderdetails.strCombo
INNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
WHERE
trn_orderdetails.intOrderNo = '$orderNo'
AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo'
AND trn_orderdetails.intOrderYear = '$orderYear'
            ";

//var_dump($sql);
    $html .= "<option value=\"\"></option>";
    $result = $db->RunQuery($sql);
    while ($row = mysqli_fetch_array($result)) {

        $html .= "<option value=\"" . $row['intGroundColor'] . "\">" . $row['strName'] . "</option>";
    }
    echo($html);
} //----------------------------------
else if ($requestType == 'loadPrintPart') {

    $graphicNo = $_REQUEST['graphicNo'];
    $orderNoArr = explode('/', $_REQUEST['orderNo']);
    $orderNo = $orderNoArr[0];
    $orderYear = $orderNoArr[1];
    $salesOrderNo =$_REQUEST['salesOrderNo'];


    $sql = "SELECT DISTINCT
	CONCAT(
		trn_orderdetails.strPrintName,'-',
		mst_part.strName
	) as strPrintName
FROM
	trn_orderdetails
INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_orderdetails.intSampleNo
AND trn_sampleinfomations_details.intSampleYear = trn_orderdetails.intSampleYear
INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
WHERE
	trn_orderdetails.strGraphicNo = '$graphicNo'
AND trn_orderdetails.intOrderNo = '$orderNo'
AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo'";
    echo $sql;

    $html .= "<option value=\"\"></option>";
    $result = $db->RunQuery($sql);
    while ($row = mysqli_fetch_array($result)) {

        $html .= "<option value=\"" . $row['strPrintName'] . "\">" . $row['strPrintName'] . "</option>";
    }
    echo($html);
} //--------------------------------------------------------------
else if ($requestType == 'loadCombo') {
    $graphicNo = $_REQUEST['graphicNo'];
    $orderNoArr = explode('/', $_REQUEST['orderNo']);
    $orderNo = $orderNoArr[0];
    $orderYear = $orderNoArr[1];
    $salesOrderNo =$_REQUEST['salesOrderNo'];
    $printPart = explode('-', $_REQUEST['part']);
    $print=$printPart[0];
    $groundColor = $_REQUEST['groundColor'];



    $sql = "SELECT DISTINCT

	trn_orderdetails.strCombo
FROM
	trn_orderdetails INNER JOIN trn_sampleinfomations_details
	ON trn_orderdetails.intSampleNo=trn_sampleinfomations_details.intSampleNo 
	AND trn_orderdetails.intSampleYear=trn_sampleinfomations_details.intSampleYear 
	AND trn_orderdetails.intRevisionNo=trn_sampleinfomations_details.intRevNo 
	AND trn_orderdetails.strCombo=trn_sampleinfomations_details.strComboName 
	AND trn_orderdetails.strPrintName=trn_sampleinfomations_details.strPrintName 
WHERE
	trn_orderdetails.strGraphicNo = '$graphicNo'
    AND trn_orderdetails.intOrderNo = '$orderNo'
    AND trn_sampleinfomations_details.intGroundColor = '$groundColor'
    AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo' AND trn_orderdetails.strPrintName='$print'";
    //echo $sql;
    echo "<option value=\"\"></option>";
    $result = $db->RunQuery($sql);
    while ($row = mysqli_fetch_array($result)) {
        $html .= "<option value=\"" . $row['strCombo'] . "\">" . $row['strCombo'] . "</option>";
    }
    echo($html);
    //$response['combo']=$html;
    // echo json_encode($response);
} //-------------------------------------------------------------- 
else if ($requestType == 'loadPrintColor') {
    $getOrderNo = $_REQUEST['orderNo'];
    $orderNoArr = explode('/', $getOrderNo);
    $orderNo = $orderNoArr[0];
    $orderYear = $orderNoArr[1];
    $combo = $_REQUEST['combo'];
    $printPart = explode('-', $_REQUEST['part']);
    $print=$printPart[0];
    $graphicNo = $_REQUEST['graphicNo'];
    $salesOrderNo =$_REQUEST['salesOrderNo'];

    $rev = getMaxRev($orderNo, $graphicNo, $salesOrderNo, $print, $combo);
    $salesOrderId=getSalesOrderId($graphicNo, $orderNo, $salesOrderNo, $combo, $print);
    $checkSOTYPE= getPressingSalesOrder($orderNo, $orderYear, $salesOrderId);
    if($checkSOTYPE=='-1'){
    $html .= "<option value=\"-10/-10\" >N/A-N/A</option>";
    }else{
    $sql="SELECT DISTINCT intColorId, mst_colors.strName as color,
                    intInkTypeId,mst_inktypes.strName as inktype, bh.STATUS FROM 
                    trn_sample_color_recipes_bulk INNER JOIN trn_sample_color_recipes_bulk_header bh ON 
                    bh.intSampleNo = trn_sample_color_recipes_bulk.intSampleNo AND 
                    bh.intSampleYear = trn_sample_color_recipes_bulk.intSampleYear AND 
                    bh.intRevisionNo = trn_sample_color_recipes_bulk.intRevisionNo AND 
                    bh.strCombo = trn_sample_color_recipes_bulk.strCombo AND 
                    bh.strPrintName = trn_sample_color_recipes_bulk.strPrintName AND 
                    bh.recipeRevision = trn_sample_color_recipes_bulk.bulkRevNumber 
                    INNER JOIN trn_orderdetails ON bh.intSampleNo = trn_orderdetails.intSampleNo AND 
                    bh.intSampleYear = trn_orderdetails.intSampleYear AND 
                    bh.intRevisionNo = trn_orderdetails.intRevisionNo AND 
                    bh.strCombo = trn_orderdetails.strCombo AND 
                    bh.strPrintName = trn_orderdetails.strPrintName  AND 
                    bh.recipeRevision = (
                    SELECT MAX(recipeRevision) 
                    FROM trn_sample_color_recipes_bulk_header z 
                    WHERE bh.intSampleNo = z.intSampleNo AND 
                    bh.intSampleYear = z.intSampleYear AND 
                    bh.intRevisionNo = z.intRevisionNo AND 
                    bh.strCombo = z.strCombo AND 
                    bh.strPrintName = z.strPrintName
                    )
                    INNER JOIN mst_colors ON trn_sample_color_recipes_bulk.intColorId = mst_colors.intId INNER JOIN 
                    mst_inktypes ON trn_sample_color_recipes_bulk.intInkTypeId = mst_inktypes.intId
                    WHERE
                        trn_orderdetails.intOrderNo = '$orderNo'
                    AND trn_orderdetails.strGraphicNo = '$graphicNo'
                    AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo'
                    AND trn_orderdetails.strPrintName = '$print'
                    AND trn_orderdetails.strCombo = '$combo'";

    $result = $db->RunQuery($sql);
    $rowcountSample =0;
    $rowcountBulk = mysqli_num_rows($result);
    if ($rowcountBulk > 0){
            while ($row = mysqli_fetch_array($result)) {
                if ($row['STATUS'] ==1) {
                    $html .= "<option value=\"" . $row['intColorId'] . '/' . $row['intInkTypeId'] . "\">" . $row['color'] . ' - ' . $row['inktype'] . "</option>";
                } else {
                    $rowcountBulk =-1;
                }
            }
    } else {
        $sql="SELECT DISTINCT
                            IFNULL(
                                trn_sampleinfomations_details.intColorId,
                                '-10'
                            ) AS intColorId,
                            IFNULL(mst_colors.strName, 'N/A') AS color,
                            IFNULL(
                                trn_sampleinfomations_details_technical.intInkTypeId,
                                '-10'
                            ) AS intInkTypeId,
                            IFNULL(mst_inktypes.strName, 'N/A') AS inktype
                        FROM
                            trn_sampleinfomations_details
                        INNER JOIN trn_orderdetails ON trn_sampleinfomations_details.intSampleNo = trn_orderdetails.intSampleNo
                        AND trn_sampleinfomations_details.intSampleYear = trn_orderdetails.intSampleYear
                        AND trn_sampleinfomations_details.intRevNo = trn_orderdetails.intRevisionNo
                        AND trn_sampleinfomations_details.strComboName = trn_orderdetails.strCombo
                        AND trn_sampleinfomations_details.strPrintName = trn_orderdetails.strPrintName
                        INNER JOIN trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = trn_sampleinfomations_details.intSampleNo
                        AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear
                        AND trn_sampleinfomations_details.intColorId = trn_sampleinfomations_details_technical.intColorId
                        AND trn_sampleinfomations_details_technical.strComboName = trn_sampleinfomations_details.strComboName
                        AND trn_sampleinfomations_details_technical.strPrintName = trn_sampleinfomations_details.strPrintName
                        AND trn_sampleinfomations_details_technical.intRevNo = trn_sampleinfomations_details.intRevNo
                        LEFT JOIN mst_colors ON trn_sampleinfomations_details.intColorId = mst_colors.intId
                        LEFT JOIN mst_inktypes ON trn_sampleinfomations_details_technical.intInkTypeId = mst_inktypes.intId
                        WHERE
                            trn_orderdetails.intOrderNo = '$orderNo'
                        AND trn_orderdetails.strGraphicNo = '$graphicNo'
                        AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo'
                        AND trn_orderdetails.strPrintName = '$print'
                        AND trn_orderdetails.strCombo = '$combo'";
        $result = $db->RunQuery($sql);
        $rowcountSample = mysqli_num_rows($result);
        if ($rowcountSample > 0){
            while ($row = mysqli_fetch_array($result)) {
                $html .= "<option value=\"" . $row['intColorId'] . '/' . $row['intInkTypeId'] . "\">" . $row['color'] . ' - ' . $row['inktype'] . "</option>";
            }
        }
    }

    $sql="SELECT DISTINCT
                            trn_sampleinfomations_details.intColorId AS intColorId,
                            mst_colors.strName AS color,
                            '-10' AS intInkTypeId,
                             'N/A' AS inktype
                        FROM
                            trn_sampleinfomations_details
                        INNER JOIN trn_orderdetails ON trn_sampleinfomations_details.intSampleNo = trn_orderdetails.intSampleNo
                        AND trn_sampleinfomations_details.intSampleYear = trn_orderdetails.intSampleYear
                        AND trn_sampleinfomations_details.intRevNo = trn_orderdetails.intRevisionNo
                        AND trn_sampleinfomations_details.strComboName = trn_orderdetails.strCombo
                        AND trn_sampleinfomations_details.strPrintName = trn_orderdetails.strPrintName
                        LEFT JOIN trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = trn_sampleinfomations_details.intSampleNo
                        AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear
                        AND trn_sampleinfomations_details.intColorId = trn_sampleinfomations_details_technical.intColorId
                        AND trn_sampleinfomations_details_technical.strComboName = trn_sampleinfomations_details.strComboName
                        AND trn_sampleinfomations_details_technical.strPrintName = trn_sampleinfomations_details.strPrintName
                        AND trn_sampleinfomations_details_technical.intRevNo = trn_sampleinfomations_details.intRevNo
                        LEFT JOIN mst_colors ON trn_sampleinfomations_details.intColorId = mst_colors.intId
                        WHERE
                            trn_orderdetails.intOrderNo = '$orderNo'
                        AND trn_orderdetails.strGraphicNo = '$graphicNo'
                        AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo'
                        AND trn_orderdetails.strPrintName = '$print'
                        AND trn_orderdetails.strCombo = '$combo' 
                        AND trn_sampleinfomations_details_technical.intColorId IS NULL";
    $rowcount = $rowcountBulk  + $rowcountSample;
    if ($rowcount != -1){
        $result = $db->RunQuery($sql);
        $rowcountColor = mysqli_num_rows($result);
        if ($rowcountColor >= 0){
            while ($row = mysqli_fetch_array($result)) {
                $html .= "<option value=\"" . $row['intColorId'] . '/' . $row['intInkTypeId'] . "\">" . $row['color'] . ' - ' . $row['inktype'] . "</option>";
            }
        }
        if ($rowcount + $rowcountColor > 0){
            $html .= "<option value=\"-1/0\" >ALL</option>";
        }
    }
    }
//    var_dump($sql);
    $response['colorsHTML'] = $html;
    $response['count'] = $rowcount;
    echo json_encode($response);
}
////////////////////////////////////////////////////////////////////
else if ($requestType == 'loadQty') {
    $graphicNo = $_REQUEST['graphicNo'];
    $getOrderNo = $_REQUEST['orderNo'];
    $orderNoArr = explode('/', $getOrderNo);
    $orderNo = $orderNoArr[0];
    $orderYear = $orderNoArr[1];
    $salesOrderNo =$_REQUEST['salesOrderNo'];
    $combo = $_REQUEST['combo'];
    $printPart = explode('-', $_REQUEST['part']);
    $print=$printPart[0];

    //first array
    $getprintcolorInktype = $_REQUEST['printColor'];


    $count = count(explode(',', $getprintcolorInktype));
    $exploded = explodeX(array(',', '/'), $getprintcolorInktype);
    $inktype = array();
    $printColor = array();
    foreach ($exploded as $k => $v) {
        if ($k % 2 == 0) {
            $printColor[] = $v;
        } else {
            $inktype[] = $v;
           }
    }


    $salesOrderIds= getSalesOrderIds($graphicNo, $orderNo, $salesOrderNo, $combo, $print);
    $date=getMaxDate($graphicNo, $orderNo, $salesOrderNo, $combo, $_REQUEST['part'],$location);    
    $fabricInQty = getFabricInQty($orderNo, $salesOrderIds,$location);
    $response['fabricInQty'] = $fabricInQty;
    $totProductionQty = getActualProductionQty($getOrderNo, $salesOrderNo, $graphicNo, $combo, $_REQUEST['part'], $location, $printColor, $inktype,$date);
    $response['productionQty'] = $totProductionQty;
    $totDispatchQty = getDispatchQty($orderNo, $salesOrderIds,$location);
    $response['dispatchQty'] = $totDispatchQty;
    $balQty = ($fabricInQty - $totProductionQty);
    $response['balQty'] = $balQty;


    echo json_encode($response);
} else if ($requestType == 'loadModules') {
    $section = $_REQUEST['section'];


    $sql = "SELECT intId,strName FROM mst_module WHERE mst_module.intSection='$section' AND mst_module.intLocation='$location'  AND mst_module.intStatus<>0";

    $html = "<option value=\"\"></option>";
    $result = $db->RunQuery($sql);
    while ($row = mysqli_fetch_array($result)) {
        $html .= "<option  value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
    }
    echo($html);
} else if ($requestType == 'loadShots') {

    $graphicNo = $_REQUEST['graphicNo'];
    $getOrderNo = $_REQUEST['orderNo'];
    $orderNoArr = explode('/', $getOrderNo);
    $orderNo = $orderNoArr[0];
    $orderYear = $orderNoArr[1];
    $salesOrderNo =$_REQUEST['salesOrderNo'];
    $combo = $_REQUEST['combo'];
    $printPart = explode('-', $_REQUEST['part']);
    $print=$printPart[0];
    //$salesOrderId = getSalesOrderId($graphicNo, $orderNo, $salesOrderNo, $combo, $print);
    $getprintcolorInktype = $_REQUEST['printColor'];

  
    $count = count(explode(',', $getprintcolorInktype));
    $exploded = explodeX(array(',', '/'), $getprintcolorInktype);
    $inktype = array();
    $printColor = array();
    foreach ($exploded as $k => $v) {
        if ($k % 2 == 0) {
            $printColor[] = $v;
        } else {
            $inktype[] = $v;
           }
    }

    if($getprintcolorInktype=='-1/0'){
        $shots = getShotsForAll($orderNo, $salesOrderNo,$graphicNo,$combo,$print);
    }else{
        $shots = getShots($orderNo, $salesOrderNo,$graphicNo,$combo,$print,$printColor,$inktype);
    }
    
    $html .= $shots;
    $response['shots'] = $html;


    echo json_encode($response);
} else if ($requestType == 'loadUps') {

    $graphicNo = $_REQUEST['graphicNo'];
    $getOrderNo = $_REQUEST['orderNo'];
    $orderNoArr = explode('/', $getOrderNo);
    $orderNo = $orderNoArr[0];
    $orderYear = $orderNoArr[1];
    $salesOrderNo =$_REQUEST['salesOrderNo'];
    $combo = $_REQUEST['combo'];
    $printPart = explode('-', $_REQUEST['part']);
    $print=$printPart[0];


//    $salesOrderId = getSalesOrderId($graphicNo, $orderNo, $salesOrderNo, $combo, $print);
//    var_dump($salesOrderId);
    $ups = getUps($graphicNo, $orderNo, $orderYear,$salesOrderNo);
    $html .= $ups;
    $response['ups'] = $html;


    echo json_encode($response);
}

//---------------------------------------------------------------
////--------------------------------------------------------
function getFabricInQty($orderNo, $salesOrderId,$location) {
    global $db;

    $sql = "SELECT 
	IFNULL(SUM(ware_fabricreceiveddetails.dblQty),0) AS fabricInQty
FROM
	ware_fabricreceiveddetails
INNER JOIN ware_fabricreceivedheader ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo
AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = ware_fabricreceivedheader.intOrderNo
AND trn_orderdetails.intOrderYear = ware_fabricreceivedheader.intOrderYear
AND trn_orderdetails.intSalesOrderId=ware_fabricreceiveddetails.intSalesOrderId
LEFT JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
AND trn_orderheader.intLocationId=ware_fabricreceivedheader.intCompanyId
WHERE
	ware_fabricreceivedheader.intOrderNo = '$orderNo'
AND ware_fabricreceiveddetails.intSalesOrderId IN $salesOrderId
AND ware_fabricreceivedheader.intStatus = 1
AND ware_fabricreceivedheader.intCompanyId='$location'
";

    $result = $db->RunQuery($sql);
    $rows = mysqli_fetch_array($result);
    $fabricInQty = $rows['fabricInQty'];
    return $fabricInQty;
}

//--------------------------------------------------------
function getDispatchQty($orderNo, $salesOrderId,$location) {
    global $db;

    $sql = "SELECT
	IFNULL(
		sum(
			ware_fabricdispatchdetails.dblGoodQty
		),
		0
	) AS dispatchQty
FROM
	ware_fabricdispatchdetails
INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo
AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = ware_fabricdispatchheader.intOrderNo
AND trn_orderdetails.intOrderYear = ware_fabricdispatchheader.intOrderYear 
AND trn_orderdetails.intSalesOrderId = ware_fabricdispatchdetails.intSalesOrderId
LEFT JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
AND trn_orderheader.intLocationId=ware_fabricdispatchheader.intCompanyId
WHERE
	ware_fabricdispatchheader.intOrderNo = '$orderNo'
AND trn_orderdetails.intSalesOrderId IN $salesOrderId
AND ware_fabricdispatchheader.intStatus = 1
AND ware_fabricdispatchheader.intCompanyId='$location'
";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $dispatchQty = $row['dispatchQty'];
    return $dispatchQty;
}

////--------------------------------------------------------
function getActualProductionQty($orderNo, $salesOrderNo, $graphicNo, $combo, $print, $location, $printColor, $inktype,$date) {
    global $db;
    $today = date("Y-m-d");
    if($date!=$today){
    $sql = "SELECT DISTINCT
	IFNULL(SUM(trn_bulkdailyproduction.intTodayProductionQty),
		0
	) AS qty
FROM
	trn_bulkdailyproduction
WHERE
	trn_bulkdailyproduction.strGraphicNo = '$graphicNo'
AND trn_bulkdailyproduction.intOrderNo = '$orderNo'
AND trn_bulkdailyproduction.strSalesOrderNo = '$salesOrderNo'
AND trn_bulkdailyproduction.productionLocation = '$location'
AND trn_bulkdailyproduction.strCombo = '$combo'
AND trn_bulkdailyproduction.strPrintName = '$print'
AND trn_bulkdailyproduction.dateOfProduction <= '$date'    
AND trn_bulkdailyproduction.intchkProduction = '-2'";
    }else{
        $sql = "SELECT DISTINCT
	IFNULL(SUM(trn_bulkdailyproduction.intTodayProductionQty),
		0
	) AS qty
FROM
	trn_bulkdailyproduction
WHERE
	trn_bulkdailyproduction.strGraphicNo = '$graphicNo'
AND trn_bulkdailyproduction.intOrderNo = '$orderNo'
AND trn_bulkdailyproduction.strSalesOrderNo = '$salesOrderNo'
AND trn_bulkdailyproduction.productionLocation = '$location'
AND trn_bulkdailyproduction.strCombo = '$combo'
AND trn_bulkdailyproduction.strPrintName = '$print'
AND trn_bulkdailyproduction.dateOfProduction < '$date'    
AND trn_bulkdailyproduction.intchkProduction = '-2'";
    }
   
//AND trn_bulkdailyproduction.intPrintColor IN (" . implode(',', $printColor) . ") AND trn_bulkdailyproduction.intInkTypeId IN (" . implode(',', $inktype) . ")";
    //echo $sql;

    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $productionQty = $row['qty'];
    return $productionQty;
}
function getShotsForAll($orderNo, $salesOrderNo,$graphicNo,$combo,$print){
    
    global $db;
    $bulkRev=getBulkRecipeRevision($orderNo, $salesOrderNo,$graphicNo,$combo,$print);
    if($bulkRev==''){
    $sqlp = "SELECT DISTINCT
	IFNULL(
		SUM(
			trn_sampleinfomations_details_technical.intNoOfShots
		),
		0
	) AS intNoOfShots
FROM
	`trn_sampleinfomations_details_technical`
INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_details_technical.intSampleNo
AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear
AND trn_sampleinfomations_details_technical.strComboName = trn_sampleinfomations_details.strComboName
AND trn_sampleinfomations_details_technical.intRevNo = trn_sampleinfomations_details.intRevNo
AND trn_sampleinfomations_details_technical.strPrintName = trn_sampleinfomations_details.strPrintName
AND trn_sampleinfomations_details.intColorId = trn_sampleinfomations_details_technical.intColorId
INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo
AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_details.intSampleYear
AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_details.intRevNo
INNER JOIN trn_orderdetails ON trn_sampleinfomations_details.intSampleNo = trn_orderdetails.intSampleNo
AND trn_sampleinfomations_details.intSampleYear = trn_orderdetails.intSampleYear
AND trn_orderdetails.intRevisionNo = trn_sampleinfomations_details.intRevNo
AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName
AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
WHERE
	trn_orderdetails.strGraphicNo = '$graphicNo'
AND trn_orderdetails.intOrderNo = '$orderNo'
AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo'
AND trn_orderdetails.strCombo = '$combo'
AND trn_orderdetails.strPrintName = '$print'
AND trn_sampleinfomations_details.COLOUR_ADDED_BY_TECHNICAL = 0";
    }else{
        $sqlp="SELECT
	IFNULL(trn_sample_color_recipes_bulk.intNumOfShots,0) AS intNoOfShots
FROM
	trn_sample_color_recipes_bulk
INNER JOIN trn_sample_color_recipes_bulk_header ON trn_sample_color_recipes_bulk_header.intSampleNo = trn_sample_color_recipes_bulk.intSampleNo
AND trn_sample_color_recipes_bulk_header.intSampleYear = trn_sample_color_recipes_bulk.intSampleYear
AND trn_sample_color_recipes_bulk_header.intRevisionNo = trn_sample_color_recipes_bulk.intRevisionNo
AND trn_sample_color_recipes_bulk_header.strCombo = trn_sample_color_recipes_bulk.strCombo
AND trn_sample_color_recipes_bulk_header.strPrintName = trn_sample_color_recipes_bulk.strPrintName
AND trn_sample_color_recipes_bulk_header.recipeRevision = trn_sample_color_recipes_bulk.bulkRevNumber
INNER JOIN trn_orderdetails ON trn_sample_color_recipes_bulk_header.intSampleNo = trn_orderdetails.intSampleNo
AND trn_sample_color_recipes_bulk_header.intSampleYear = trn_orderdetails.intSampleYear
AND trn_sample_color_recipes_bulk_header.intRevisionNo = trn_orderdetails.intRevisionNo
AND trn_sample_color_recipes_bulk_header.strCombo = trn_orderdetails.strCombo
AND trn_sample_color_recipes_bulk_header.strPrintName = trn_orderdetails.strPrintName
WHERE
	trn_orderdetails.strGraphicNo = '$graphicNo'
AND trn_orderdetails.intOrderNo = '$orderNo'
AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo'
AND trn_orderdetails.strCombo = '$combo'
AND trn_orderdetails.strPrintName = '$print'
AND trn_sample_color_recipes_bulk.bulkRevNumber = '$bulkRev'
AND trn_sample_color_recipes_bulk_header.`STATUS` = 1  GROUP BY trn_sample_color_recipes_bulk.intColorId,trn_sample_color_recipes_bulk.intInkTypeId,trn_sample_color_recipes_bulk.bulkRevNumber,
trn_sample_color_recipes_bulk.strCombo,trn_sample_color_recipes_bulk.strPrintName,trn_sample_color_recipes_bulk.intSampleNo";
    }
//echo $sqlp;
    $resultP = $db->RunQuery($sqlp);
    $shots =0;
    while ($rowP = mysqli_fetch_array($resultP)){
        $shots += $rowP['intNoOfShots'];
    }
    return $shots;
}
function getShots($orderNo, $salesOrderNo,$graphicNo,$combo,$print,$printColor,$inktype) {

    global $db;
    $bulkRev=getBulkRecipeRevision($orderNo, $salesOrderNo,$graphicNo,$combo,$print);
    if($bulkRev==''){
    $sqlp = "SELECT 
	IFNULL(
		SUM(
			trn_sampleinfomations_details_technical.intNoOfShots
		),
		0
	) AS intNoOfShots
FROM
	`trn_sampleinfomations_details_technical`
INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_details_technical.intSampleNo
AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear
AND trn_sampleinfomations_details_technical.strComboName = trn_sampleinfomations_details.strComboName
AND trn_sampleinfomations_details_technical.intRevNo = trn_sampleinfomations_details.intRevNo
AND trn_sampleinfomations_details_technical.strPrintName = trn_sampleinfomations_details.strPrintName
AND trn_sampleinfomations_details.intColorId = trn_sampleinfomations_details_technical.intColorId
INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo
AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_details.intSampleYear
AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_details.intRevNo
INNER JOIN trn_orderdetails ON trn_sampleinfomations_details.intSampleNo = trn_orderdetails.intSampleNo
AND trn_sampleinfomations_details.intSampleYear = trn_orderdetails.intSampleYear
AND trn_orderdetails.intRevisionNo = trn_sampleinfomations_details.intRevNo
AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName
AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
WHERE
	trn_orderdetails.strGraphicNo = '$graphicNo'
AND trn_orderdetails.intOrderNo = '$orderNo'
AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo'
AND trn_orderdetails.strCombo = '$combo'
AND trn_orderdetails.strPrintName = '$print'
AND trn_sampleinfomations_details_technical.intColorId IN (" . implode(',', $printColor) . ")
AND trn_sampleinfomations_details_technical.intInkTypeId IN (" . implode(',', $inktype) . ")
AND trn_sampleinfomations_details.COLOUR_ADDED_BY_TECHNICAL = 0";
    }else{
        $sqlp="SELECT  IFNULL(trn_sample_color_recipes_bulk.intNumOfShots,0) AS intNoOfShots
FROM
	trn_sample_color_recipes_bulk
INNER JOIN trn_sample_color_recipes_bulk_header ON trn_sample_color_recipes_bulk_header.intSampleNo = trn_sample_color_recipes_bulk.intSampleNo
AND trn_sample_color_recipes_bulk_header.intSampleYear = trn_sample_color_recipes_bulk.intSampleYear
AND trn_sample_color_recipes_bulk_header.intRevisionNo = trn_sample_color_recipes_bulk.intRevisionNo
AND trn_sample_color_recipes_bulk_header.strCombo = trn_sample_color_recipes_bulk.strCombo
AND trn_sample_color_recipes_bulk_header.strPrintName = trn_sample_color_recipes_bulk.strPrintName
AND trn_sample_color_recipes_bulk_header.recipeRevision = trn_sample_color_recipes_bulk.bulkRevNumber
INNER JOIN trn_orderdetails ON trn_sample_color_recipes_bulk_header.intSampleNo = trn_orderdetails.intSampleNo
AND trn_sample_color_recipes_bulk_header.intSampleYear = trn_orderdetails.intSampleYear
AND trn_sample_color_recipes_bulk_header.intRevisionNo = trn_orderdetails.intRevisionNo
AND trn_sample_color_recipes_bulk_header.strCombo = trn_orderdetails.strCombo
AND trn_sample_color_recipes_bulk_header.strPrintName = trn_orderdetails.strPrintName
WHERE
	trn_orderdetails.strGraphicNo = '$graphicNo'
AND trn_orderdetails.intOrderNo = '$orderNo'
AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo'
AND trn_orderdetails.strCombo = '$combo'
AND trn_orderdetails.strPrintName = '$print'
AND trn_sample_color_recipes_bulk.bulkRevNumber = '$bulkRev'
AND trn_sample_color_recipes_bulk.intColorId IN (" . implode(',', $printColor) . ")
AND trn_sample_color_recipes_bulk.intInkTypeId IN (" . implode(',', $inktype) . ")
AND trn_sample_color_recipes_bulk_header.`STATUS` = 1 GROUP BY trn_sample_color_recipes_bulk.intColorId,trn_sample_color_recipes_bulk.intInkTypeId,trn_sample_color_recipes_bulk.bulkRevNumber,
trn_sample_color_recipes_bulk.strCombo,trn_sample_color_recipes_bulk.strPrintName,trn_sample_color_recipes_bulk.intSampleNo";
    }
//echo $sqlp;
    
    $resultP = $db->RunQuery($sqlp);
    $shots =0;
    while ($rowP = mysqli_fetch_array($resultP)){
        $shots += $rowP['intNoOfShots'];
    }
    return $shots;
}

function getBulkRecipeRevision($orderNo, $salesOrderNo,$graphicNo,$combo,$print){
    global $db;
    $sql="SELECT 
	max(trn_sample_color_recipes_bulk.bulkRevNumber) as recipeRevision
FROM
	trn_sample_color_recipes_bulk
INNER JOIN trn_sample_color_recipes_bulk_header ON trn_sample_color_recipes_bulk_header.intSampleNo = trn_sample_color_recipes_bulk.intSampleNo
AND trn_sample_color_recipes_bulk_header.intSampleYear = trn_sample_color_recipes_bulk.intSampleYear
AND trn_sample_color_recipes_bulk_header.intRevisionNo = trn_sample_color_recipes_bulk.intRevisionNo
AND trn_sample_color_recipes_bulk_header.strCombo = trn_sample_color_recipes_bulk.strCombo
AND trn_sample_color_recipes_bulk_header.strPrintName = trn_sample_color_recipes_bulk.strPrintName
AND trn_sample_color_recipes_bulk_header.recipeRevision = trn_sample_color_recipes_bulk.bulkRevNumber
INNER JOIN trn_orderdetails ON trn_sample_color_recipes_bulk_header.intSampleNo = trn_orderdetails.intSampleNo
AND trn_sample_color_recipes_bulk_header.intSampleYear = trn_orderdetails.intSampleYear
AND trn_sample_color_recipes_bulk_header.intRevisionNo = trn_orderdetails.intRevisionNo
AND trn_sample_color_recipes_bulk_header.strCombo = trn_orderdetails.strCombo
AND trn_sample_color_recipes_bulk_header.strPrintName = trn_orderdetails.strPrintName

WHERE
	trn_orderdetails.strGraphicNo = '$graphicNo'
AND trn_orderdetails.intOrderNo = '$orderNo'
AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo'
AND trn_orderdetails.strCombo = '$combo'
AND trn_orderdetails.strPrintName = '$print'
AND trn_sample_color_recipes_bulk_header.`STATUS` = 1";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['recipeRevision'];
}

function getUps($graphicNo, $orderNo,$orderYear, $salesOrderNo) {
    global $db;
    $sql="SELECT
	trn_orderdetails.intSampleNo,
trn_orderdetails.intSampleYear,
trn_orderdetails.strCombo,
trn_orderdetails.strPrintName,
MAX(trn_orderdetails.intRevisionNo) as revision
FROM
	trn_orderdetails
WHERE
	trn_orderdetails.strGraphicNo = '$graphicNo'
AND trn_orderdetails.intOrderNo = '$orderNo'
AND trn_orderdetails.intOrderYear='$orderYear'
AND trn_orderdetails.strSalesOrderNo='$salesOrderNo'";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    
    $sampleNo=$row['intSampleNo'];
    $sampleYear=$row['intSampleYear'];
    $combo=$row['strCombo'];
    $print=$row['strPrintName'];
    $maxRev=$row['revision'];
    $sqlP = "SELECT
	MAX(
		trn_sampleinfomations_combo_print_routing.REVISION
	),
	IFNULL(SUM(
		trn_sampleinfomations_combo_print_routing.NO_OF_UPS),
		0
	) AS ups
FROM
	`trn_sampleinfomations_combo_print_routing`
WHERE
	trn_sampleinfomations_combo_print_routing.SAMPLE_NO = '$sampleNo'
AND trn_sampleinfomations_combo_print_routing.SAMPLE_YEAR = '$sampleYear'
AND trn_sampleinfomations_combo_print_routing.PRINT = '$print'
AND trn_sampleinfomations_combo_print_routing.COMBO = '$combo' AND trn_sampleinfomations_combo_print_routing.REVISION = '$maxRev' ";
    //echo $sqlP;

    $resultP = $db->RunQuery($sqlP);
    $rowP = mysqli_fetch_array($resultP);
    $ups = $rowP['ups'];
    return $ups;
}

function explodeX($delimiters, $string) {
    return explode(chr(1), str_replace($delimiters, chr(1), $string));
}

function getMaxDate($graphicNo, $orderNo, $salesOrderNo, $combo, $print,$location){
    global $db;
    $sql="SELECT
	MAX(dateOfProduction) as date
FROM
	trn_bulkdailyproduction
WHERE
	trn_bulkdailyproduction.strGraphicNo = '$graphicNo'
AND trn_bulkdailyproduction.intOrderNo = '$orderNo'
AND trn_bulkdailyproduction.strSalesOrderNo = '$salesOrderNo'
AND trn_bulkdailyproduction.productionLocation = '$location'
AND trn_bulkdailyproduction.strCombo = '$combo'
AND trn_bulkdailyproduction.strPrintName = '$print'";
   // echo $sql;
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $prevDate = $row['date'];
    return $prevDate;
    
}
function getMaxRev($orderNo, $graphicNo, $salesorderNo, $print, $combo) {
    global $db;
    $get_rev = "SELECT
	MAX(
		trn_orderdetails.intRevisionNo
	) AS revision
FROM
trn_orderdetails
WHERE
	trn_orderdetails.intOrderNo = '$orderNo'
AND trn_orderdetails.strGraphicNo = '$graphicNo'
AND trn_orderdetails.strSalesOrderNo = '$salesorderNo'
AND trn_orderdetails.strPrintName = '$print'
AND trn_orderdetails.strCombo = '$combo'";
    // echo $get_rev;
    $result_rev = $db->RunQuery($get_rev);
    $row = mysqli_fetch_array($result_rev);
    $rev = $row['revision'];
    return $rev;
}

function getSalesOrderId($graphicNo, $orderNo, $salesOrderNo, $combo, $print) {
    global $db;
    $sql = "SELECT
	trn_orderdetails.intSalesOrderId
FROM
	trn_orderdetails
WHERE
	trn_orderdetails.strGraphicNo = '$graphicNo'
AND trn_orderdetails.intOrderNo = '$orderNo'
AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo'
AND trn_orderdetails.strCombo = '$combo'
AND trn_orderdetails.strPrintName = '$print'";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $salesOrderId = $row['intSalesOrderId'];
    return $salesOrderId;
}


function getSalesOrderIds($graphicNo, $orderNo, $salesOrderNo, $combo, $print) {
    global $db;
    $sql = "SELECT
	trn_orderdetails.intSalesOrderId
FROM
	trn_orderdetails
WHERE
	trn_orderdetails.strGraphicNo = '$graphicNo'
AND trn_orderdetails.intOrderNo = '$orderNo'
AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo'
AND trn_orderdetails.strCombo = '$combo'
AND trn_orderdetails.strPrintName = '$print'";
    $result = $db->RunQuery($sql);
    $soIds = "(";
    while ($row = mysqli_fetch_array($result)) {

        $soId = $row['intSalesOrderId'];
        $soIds.=$soId.",";
    }
    $soIds = rtrim($soIds,',');
    $soIds.=")";
    return $soIds;
}

function getSampleNo($graphicNo, $orderNo, $salesOrderNo, $combo, $print,$revision){
    global $db;
    $sql="SELECT
	concat(trn_orderdetails.intSampleNo,trn_orderdetails.intSampleYear) as sample
FROM
	trn_orderdetails
WHERE
	trn_orderdetails.intOrderNo = '$orderNo'
AND trn_orderdetails.strGraphicNo = '$graphicNo'
AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo'
AND trn_orderdetails.strPrintName = '$print'
AND trn_orderdetails.strCombo = '$combo'
AND trn_orderdetails.intRevisionNo = '$revision'";
     $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $sampleNo = $row['sample'];
    return $sampleNo;
    
}

function chkSample($sampleNo,$sampleYear){
    global $db;
    $sql="SELECT
	EXISTS (
		SELECT
			intSampleNo,
			intSampleYear
		FROM
			trn_sampleinfomations_details_technical
		WHERE
			intSampleNo = '$sampleNo'
		AND intSampleYear = '$sampleYear'
	) AS chkSample";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $chkSample = $row['chkSample'];
    return $sampleNo;
    
}
function getPressingSalesOrder($orderNo, $orderYear, $salesOrderId)
{
    global $db;

    $sqlp = "SELECT SO_TYPE 
    FROM
    trn_orderdetails
    WHERE
    trn_orderdetails.intOrderNo =  '$orderNo' AND
    trn_orderdetails.intOrderYear =  '$orderYear' AND
    trn_orderdetails.intSalesOrderId  =  '$salesOrderId'";

    $resultp = $db->RunQuery($sqlp);
    $rowp = mysqli_fetch_array($resultp);

    return $rowp['SO_TYPE'];
}

?>

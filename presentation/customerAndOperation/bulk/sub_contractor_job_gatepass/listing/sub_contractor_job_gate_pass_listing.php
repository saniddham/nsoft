<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$location 			= $_SESSION['CompanyID'];
$programCode		= 'P0874';
$intUser  			= $_SESSION["userId"];

require_once "class/tables/trn_order_sub_contract_gate_pass_approved_by.php";
require_once "libraries/jqgrid2/inc/jqgrid_dist.php";

$obj_approve_by		= new trn_order_sub_contract_gate_pass_approved_by($db);

$select				= "Max(LEVELS) AS APP_LEVELS";
$db->connect();
$header_result		= $obj_approve_by->select($select,NULL,$where=NULL,NULL,NULL);	
$header_array		= mysqli_fetch_array($header_result);
$db->disconnect();
$approveLevel 		= $header_array['APP_LEVELS'];

//BEGIN - ADD DEFAULT WHERE STRING WHEN FORM LOADING {
$arr =  json_decode($_REQUEST['filters'],true);

$arr = $arr['rules'];

$where_string = '';
$where_array = array(
					'Status'=>'tb1.STATUS',
					'Fabric_GP_No'=>'tb1.SUB_CONTRACT_GP_NO',
					'Fabric_GP_Year'=>'tb1.SUB_CONTRACT_GP_YEAR',
					'sub_contract_no'=>"CONCAT(tb2.SUB_CONTRACT_NO,'/',tb2.SUB_CONTRACT_YEAR)",
					'Order_No'=>"concat(tb2.ORDER_NO,'/',tb2.ORDER_YEAR)",
					'locationName'=>"mst_locations.strName",
					'Date'=>'tb1.CREATED_DATE'
					);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Revised'=>'-1','Cancelled'=>'-2','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}

if(!count($arr)>0)					 
		$where_string .= "AND tb1.DATE = '".date('Y-m-d')."'";
//END }

$sql = "select * from(SELECT DISTINCT 
							if(tb1.STATUS=1,'Approved',if(tb1.STATUS=0,'Rejected',if(tb1.STATUS=-2,'Canceled','Pending'))) as Status,
							tb1.SUB_CONTRACT_GP_NO as `Fabric_GP_No`,
							tb1.SUB_CONTRACT_GP_YEAR as `Fabric_GP_Year`,
							CONCAT(tb2.SUB_CONTRACT_NO,'/',tb2.SUB_CONTRACT_YEAR) AS sub_contract_no,
							tb1.DATE as `Date`,
							(select  group_concat(strStyleNo separator ', ') from  (select distinct strStyleNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb2.ORDER_NO AND
							tb.intOrderYear =  tb2.ORDER_YEAR

							) as strStyleNo,
							(select  group_concat(strGraphicNo separator ', ') from  (select distinct strGraphicNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb2.ORDER_NO AND
							tb.intOrderYear =  tb2.ORDER_YEAR

							) as strGraphicNo, 
							sys_users.strUserName as User,
							tb1.LEVELS,
							concat(tb2.ORDER_NO,'/',tb2.ORDER_YEAR) as Order_No ,  
							mst_locations.strName as locationName ,
							IFNULL((
                                      SELECT
								concat(sys_users.strUserName,'(',max(trn_order_sub_contract_gate_pass_approved_by.DATE),')' )
								FROM
								trn_order_sub_contract_gate_pass_approved_by
								Inner Join sys_users ON trn_order_sub_contract_gate_pass_approved_by.USER = sys_users.intUserId
								WHERE
								trn_order_sub_contract_gate_pass_approved_by.SUB_CONTRACT_GP_NO  = tb1.SUB_CONTRACT_GP_NO AND
								trn_order_sub_contract_gate_pass_approved_by.SUB_CONTRACT_GP_YEAR =  tb1.SUB_CONTRACT_GP_YEAR AND
								trn_order_sub_contract_gate_pass_approved_by.LEVELS =  '1'  AND
								trn_order_sub_contract_gate_pass_approved_by.STATUS =  '0' 
								
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(trn_order_sub_contract_gate_pass_approved_by.DATE),')' )
								FROM
								trn_order_sub_contract_gate_pass_approved_by
								Inner Join sys_users ON trn_order_sub_contract_gate_pass_approved_by.USER = sys_users.intUserId
								WHERE
								trn_order_sub_contract_gate_pass_approved_by.SUB_CONTRACT_GP_NO  = tb1.SUB_CONTRACT_GP_NO AND
								trn_order_sub_contract_gate_pass_approved_by.SUB_CONTRACT_GP_YEAR =  tb1.SUB_CONTRACT_GP_YEAR AND
								trn_order_sub_contract_gate_pass_approved_by.LEVELS =  '$i'   AND
								trn_order_sub_contract_gate_pass_approved_by.STATUS =  '0' 
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.STATUS>1) AND (tb1.STATUS<=tb1.LEVELS) AND ((SELECT
								max(trn_order_sub_contract_gate_pass_approved_by.DATE)
								FROM
								trn_order_sub_contract_gate_pass_approved_by
								Inner Join sys_users ON trn_order_sub_contract_gate_pass_approved_by.USER = sys_users.intUserId
								WHERE
								trn_order_sub_contract_gate_pass_approved_by.SUB_CONTRACT_GP_NO  = tb1.SUB_CONTRACT_GP_NO AND
								trn_order_sub_contract_gate_pass_approved_by.SUB_CONTRACT_GP_YEAR =  tb1.SUB_CONTRACT_GP_YEAR AND
								trn_order_sub_contract_gate_pass_approved_by.LEVELS =  ($i-1))<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*fase part*/
								 if($i>tb1.LEVELS,'-----',''))
								
								/*end IFNULL false part*/
								) as `".$approval."`, "; 
									
								}
								
						$sql .= "IFNULL((
                                      SELECT
								concat(sys_users.strUserName,'(',max(trn_order_sub_contract_gate_pass_approved_by.DATE),')' )
								FROM
								trn_order_sub_contract_gate_pass_approved_by
								Inner Join sys_users ON trn_order_sub_contract_gate_pass_approved_by.USER = sys_users.intUserId
								WHERE
								trn_order_sub_contract_gate_pass_approved_by.SUB_CONTRACT_GP_NO  = tb1.SUB_CONTRACT_GP_NO AND
								trn_order_sub_contract_gate_pass_approved_by.SUB_CONTRACT_GP_YEAR =  tb1.SUB_CONTRACT_GP_YEAR AND
								trn_order_sub_contract_gate_pass_approved_by.LEVELS =  '-2' AND
								trn_order_sub_contract_gate_pass_approved_by.STATUS =  '0'
							),IF(((SELECT
								menupermision.intCancel 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS=1),'Cancel', '')) as `Cancel`,  ";
								
						$sql .= " 'View' as `View`   
						FROM
							trn_order_sub_contract_gate_pass_header as tb1
							INNER JOIN trn_order_sub_contract_header as tb2 ON tb1.SUB_CONTRACT_NO = tb2.SUB_CONTRACT_NO AND tb1.SUB_CONTRACT_YEAR = tb2.SUB_CONTRACT_YEAR
							LEFT JOIN mst_subcontractor ON tb2.SUB_CONTRACTOR_ID = mst_subcontractor.SUB_CONTRACTOR_ID
							LEFT JOIN mst_locations ON mst_subcontractor.INTER_LOCATION_ID = mst_locations.intId
							LEFT JOIN mst_companies ON mst_companies.intId = mst_locations.intCompanyId
							Inner Join sys_users ON tb1.CREATED_BY = sys_users.intUserId 
							WHERE
							tb1.LOCATION_ID =  '$location'
							$where_string
							)  as t where 1=1
						";
					     //	echo $sql;
					   
$formLink  					= "?q=874&serial_no={Fabric_GP_No}&serial_year={Fabric_GP_Year}";
$reportLink  				= "?q=906&serial_no={Fabric_GP_No}&serial_year={Fabric_GP_Year}";
$reportLinkApprove  		= "?q=906&serial_no={Fabric_GP_No}&serial_year={Fabric_GP_Year}&mode=Confirm";
$reportLinkCancel	  		= "?q=906&serial_no={Fabric_GP_No}&serial_year={Fabric_GP_Year}&mode=Cancel";
					   
$col  = array();
$cols = array();

//STATUS
$col["title"] 				= "Status"; // caption of column
$col["name"] 				= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "3";
//edittype
$col["stype"] 				= "select";
$str 						= ":All;Pending:Pending;Approved:Approved;Rejected:Rejected;Cancel:Cancel" ;
$col["editoptions"] 		=  array("value"=> $str);
//searchOper
$col["align"] 				= "center";

$cols[] = $col;	$col=NULL;
//Fabric Receive No
$col["title"] 				= "GP No"; // caption of column
$col["name"] 				= "Fabric_GP_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "3";
//searchOper
$col["align"] 				= "center";
$col['link']				= $formLink;	 
$col["linkoptions"] 		= "target='sub_contractor_job_gate_pass.php'"; // extra params with <a> tag


$cols[] = $col;	$col=NULL;


//Fabric Receive Year
$col["title"] 				= "GP Year"; // caption of column
$col["name"] 				= "Fabric_GP_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "3";
$col["align"] 				= "center";
$cols[] = $col;	$col=NULL;

//Fabric Receive Year
$col["title"] 				= "Sub Contract No"; // caption of column
$col["name"] 				= "sub_contract_no"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "3";
$col["align"] 				= "center";
$cols[] = $col;	$col=NULL;

//CUSTOMER PO NO
$col["title"] 				= "Style No"; // caption of column
$col["name"] 				= "strStyleNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "4";
$col["align"] 				= "center";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] 				= "Graphic No"; // caption of column
$col["name"] 				= "strGraphicNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"]				= "4";
$col["align"] 				= "center";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] 				= "Order No"; // caption of column
$col["name"] 				= "Order_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "5";
$col["align"] 				= "center";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] 				= "Gate Pass To"; // caption of column
$col["name"] 				= "locationName"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "6";
$col["align"] 				= "left";
$cols[] = $col;	$col=NULL;

//DATE
$col["title"] 				= "Date"; // caption of column
$col["name"] 				= "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "5";
$col["align"] 				= "center";
$cols[] = $col;	$col=NULL;


//FIRST APPROVAL
$col["title"] 				= "1st Approval"; // caption of column
$col["name"] 				= "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "5";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= $reportLinkApprove;

$col['linkName']			= 'Approve';
$col["linkoptions"] 		= "target='rpt_sub_contractor_job_gate_pass.php'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap			="2nd Approval";
	$ap1		="2nd_Approval";
	}
	else if($i==3){
	$ap			="3rd Approval";
	$ap1		="3rd_Approval";
	}
	else {
	$ap			=$i."th Approval";
	$ap1		=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] 			= $ap; // caption of column
$col["name"] 			= $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "5";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= $reportLinkApprove;
$col['linkName']		= 'Approve';
$col["linkoptions"] 	= "target='rpt_sub_contractor_job_gate_pass.php'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}

//FIRST APPROVAL
$col["title"] 				= "Cancel"; // caption of column
$col["name"] 				= "Cancel"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "5";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= $reportLinkCancel;

$col['linkName']			= 'Cancel';
$col["linkoptions"] 		= "target='rpt_sub_contractor_job_gate_pass.php'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;


//VIEW
$col["title"] 				= "Report"; // caption of column
$col["name"] 				= "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "3";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= $reportLink;
$col["linkoptions"] 		= "target='rpt_sub_contractor_job_gate_pass.php'"; // extra params with <a> tag
$cols[] 					= $col;	
$col						= NULL;

$d	=date('Y-m-d');
$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"},{"field":"DATE","op":"eq","data":"$d"}
     ]

}
SEARCH_JSON;

$jq = new jqgrid('',$db);
$grid["caption"] 		= "Sub Contractor Gate Pass Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'Fabric_GP_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<head>
	<?php 
		// include "include/listing.html";
	?>
</head>
<body>
<form id="frmlisting" name="frmlisting" method="post" action="">

        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
</form>
</body>
$(document).ready(function() {
	$('#frmSubJobGPReport #butRptConfirm').live('click',urlApprove);
	$('#frmSubJobGPReport #butRptReject').live('click',urlReject);
	$('#frmSubJobGPReport #butRptCancel').live('click',urlCancel);
});

function urlApprove()
{
	var val = $.prompt('Are you sure you want to approve this Gate Pass ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "presentation/customerAndOperation/bulk/sub_contractor_job_gatepass/listing/rpt_sub_contractor_job_gate_pass-db.php"+window.location.search+'&requestType=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmSubJobGPReport #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertxR1()",10000);
									window.location.href = window.location.href;
									window.opener.location.reload();
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSubJobGPReport #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertxR1()",3000);
								return;
							}		
						});
					hideWaiting();
					}				
			}});
}

function urlReject()
{
	var val = $.prompt('Are you sure you want to reject this Gate Pass ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "presentation/customerAndOperation/bulk/sub_contractor_job_gatepass/listing/rpt_sub_contractor_job_gate_pass-db.php"+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,						
						success:function(json){
								$('#frmSubJobGPReport #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertxR2()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){								
								$('#frmSubJobGPReport #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertxR2()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}

function urlCancel()
{
	var val = $.prompt('Are you sure you want to cancel this Gate Pass ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "presentation/customerAndOperation/bulk/sub_contractor_job_gatepass/listing/rpt_sub_contractor_job_gate_pass-db.php"+window.location.search+'&requestType=cancel';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,						
						success:function(json){
								$('#frmSubJobGPReport #butRptCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertxR3()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSubJobGPReport #butRptCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertxR3()",3000);
								return;
							}		
						});
					hideWaiting();
					}				
			}});
}

function alertxR1()
{
	$('#frmSubJobGPReport #butRptConfirm').validationEngine('hide')	;
}

function alertxR2()
{
	$('#frmSubJobGPReport #butRptReject').validationEngine('hide')	;
}

function alertxR2()
{
	$('#frmSubJobGPReport #butRptCancel').validationEngine('hide')	;
}
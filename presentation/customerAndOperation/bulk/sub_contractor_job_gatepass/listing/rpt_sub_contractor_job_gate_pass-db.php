<?php
	session_start();
	ini_set('display_errors',0);
	$backwardseperator	= "../../../../../";
	$mainPath			= $_SESSION['mainPath'];
	$userId 			= $_SESSION['userId'];
	$location 			= $_SESSION['CompanyID'];
	$company 			= $_SESSION['headCompanyId'];

	$requestType 		= $_REQUEST['requestType'];
	
	include_once		"../../../../../dataAccess/DBManager2.php";										$db						= new DBManager2();
	require_once		"../../../../../class/cls_commonFunctions_get.php";         					$obj_common				= new cls_commonFunctions_get($db);
	require_once		"../../../../../class/cls_commonErrorHandeling_get.php";						$obj_commonErr			= new cls_commonErrorHandeling_get($db);
 	require_once		"../../../../../class/tables/trn_order_sub_contract_gate_pass_header.php";		$obj_header				= new trn_order_sub_contract_gate_pass_header($db);
	require_once		"../../../../../class/tables/trn_order_sub_contract_gate_pass_details.php";		$obj_detail				= new trn_order_sub_contract_gate_pass_details($db);
	require_once		"../../../../../class/tables/trn_order_sub_contract_gate_pass_approved_by.php";	$obj_approve_by			= new trn_order_sub_contract_gate_pass_approved_by($db);
	require_once		"../../../../../class/tables/ware_stocktransactions_fabric.php";				$obj_stock_fabric		= new ware_stocktransactions_fabric($db);
	require_once		"../../../../../class/tables/trn_orderdetails.php";								$obj_order_details		= new trn_orderdetails($db);
	require_once		"../../../../../class/tables/trn_order_sub_contract_size_wise_qty.php";			$obj_sub_od_size_wise	= new trn_order_sub_contract_size_wise_qty($db);
	
	include 			"../../../../../class/error_handler.php";										$error_handler			= new error_handler(); 
	include 			"../../../../../class/tables/menupermision.php";								$menupermision			= new menupermision($db);
	include_once 		"../../../../../class/dateTime.php";											$obj_dateTime			= new dateTimes($db);
	
	$programName		= 'Sub Contract Gate Pass';
	$programCode		= 'P0874';
	
	$roll_back_flag		= false;
	$display_message	= '';
	$error_sql			= '';
 	
	//---------------------------		
	if($requestType=='approve'){
		
		//validations
		//1. permission check.
		//2. save location and approval location should be same.
		//3. GP Qty <= Sub Order Qty- Already GP Qty
		//4. GP QTY <= Stock Qty
		
		try{
			$db->connect();
			
			//requested parameters
			$serial_no			= $_REQUEST['serial_no'];
			$serial_year		= $_REQUEST['serial_year'];
			
			//1) select header status
			$header_status		= $obj_header->getStatus($serial_no,$serial_year);
			
			//2) validate before approve 
				//a) permision check
			$response_validate	= validate_header_approval_permision($serial_no,$serial_year);
				//b) validate location
			$response_validate	= validate_location($serial_no,$serial_year);
			
			//3) update header status
			$response_update	= upgrade_header_status(-1,$serial_no,$serial_year);
			
			//4)insert to approve by table
			$status				= $obj_header->getStatus($serial_no,$serial_year);
			$levels				= $obj_header->getApproveLevels($serial_no,$serial_year);
			$levels       		= $levels+1-$status;
			$response_insert	= insert_to_approved_by($serial_no,$serial_year,$levels);
			
			//1) get updated header status
			$header_status		= $obj_header->getStatus($serial_no,$serial_year);
			//4)get gate pass details
			$details_result		= get_gate_pass_detais_results($serial_no,$serial_year);
			while($row=mysqli_fetch_array($details_result))
			{
				$toLocation			= $row['LOCATION_TO_ID'];
				$orderNo			= $row['intOrderNo'];
				$orderYear			= $row['intOrderYear'];
				$cutNo				= $row['CUT_NO'];
				$part				= $row['intPart'];
				$salesOrderId		= $row['intSalesOrderId'];
				$subContractNo		= $row['SUB_CONTRACT_NO'];
				$subContractYear	= $row['SUB_CONTRACT_YEAR'];
				$subContractJob		= $row['SUB_CONTR_JOB_ID'];
				$size				= $row['SIZE'];
				$qty				= round($row['QTY'],4);
				$place 				= 'Stores';
				$type 				= 'SUB_GATE_PASS';
				
				//validate - Stock balance and Sub Order Balance
				
				$response_validate	= validate_details_approval($serial_no,$serial_year,$subContractNo,$subContractYear,$orderNo,$orderYear,$salesOrderId,$subContractJob,$size,$cutNo,$qty);
				 
				if($header_status ==1){
					//substract from locarion stock
					$response_insert	= insert_to_transaction_fabric($location,$place,$toLocation,$serial_no,$serial_year,$orderNo,$orderYear,$cutNo,$salesOrderId,$part,$size,-$qty,$type,$userId);
					
					 if($row['COMPANY_TO_TYPE']==0) {//inter company
						//add to sub contract location stock
						$type 		  		='SUB_GATE_PASS_IN';
						//$response_insert	= insert_to_transaction_fabric($toLocation,$place,0,$serial_no,$serial_year,$orderNo,$orderYear,$cutNo,$salesOrderId,$part,$size,$qty,$type,$userId);
					
					 }
				}
			} //end of while
		
			$status				= $obj_header->getStatus($serial_no,$serial_year);
			$levels				= $obj_header->getApproveLevels($serial_no,$serial_year);
			if($status ==1 ){
				$response['type'] 	= 'pass';
				$response['msg']	= 'Final Approval Raised successfully.';
			}
			else{ 
				$response['type']	= 'pass';
				$response['msg'] 	= 'Approve level '.($levels+1-$status).' raised successfully.';
			}
		
			$db->commit();
 			
		}catch(Exception $e){
			$db->rollback();
			$response['msg'] 	=  $e->getMessage();;
			$response['error'] 	=  $error_handler->jTraceEx($e);
			$response['type']	=  'fail';
			$response['sql']	=  $db->getSql();
		}
	
	$db->disconnect();		
	echo json_encode($response);
	
	}
	
	else if($requestType=='reject'){
		
		//validations
		//1. check rejection permission
		
		try{
			$db->connect();
			//requested parameters
			$serial_no					= $_REQUEST['serial_no'];
			$serial_year				= $_REQUEST['serial_year'];
			
			//1) select header status
			$header_status		= $obj_header->getStatus($serial_no,$serial_year);
			
			//2) validate before approve
			$response_validate	= validate_rejection($serial_no,$serial_year);

			//3) update header status
			$response_update	= update_header_status(0,$serial_no,$serial_year);

 			//5) insert record to approved by table
			$response_insert	= insert_to_approved_by($serial_no,$serial_year,0);
			
			
			$status				= $obj_header->getStatus($serial_no,$serial_year);
			if($status ==0 ){
				$response['type'] 	= 'pass';
				$response['msg'] 	= 'Rejection Raised successfully.';
			}
			
			$db->commit();
			
		}catch(Exception $e){
			$db->rollback();
			$response['msg'] 	=  $e->getMessage();;
			$response['error'] 	=  $error_handler->jTraceEx($e);
			$response['type']	=  'fail';
			$response['sql']	=  $db->getSql();
		}
	 
	   $db->disconnect();		
		echo json_encode($response);
	
	}
	
	if($requestType=='cancel'){
		
		//validations
		//1. check rejection permission
		
		try{
			$db->connect();
			
			//requested parameters
			$serial_no					= $_REQUEST['serial_no'];
			$serial_year				= $_REQUEST['serial_year'];

			//1) validate before approve
			$response_validate	= validate_cancel($serial_no,$serial_year);
		
			//2) update header status
			$response_update	= update_header_status(-2,$serial_no,$serial_year);

 			//3) insert record to approved by table
			$response_insert	= insert_to_approved_by($serial_no,$serial_year,-2);
			
			//4)get gate pass details
			$details_result		= get_gate_pass_detais_results($serial_no,$serial_year);
			while($row=mysqli_fetch_array($details_result))
			{
				$toLocation			= $row['LOCATION_TO_ID'];
				$orderNo			= $row['intOrderNo'];
				$orderYear			= $row['intOrderYear'];
				$cutNo				= $row['CUT_NO'];
				$part				= $row['intPart'];
				$salesOrderId		= $row['intSalesOrderId'];
				$subContractNo		= $row['SUB_CONTRACT_NO'];
				$subContractYear	= $row['SUB_CONTRACT_YEAR'];
				$subContractJob		= $row['SUB_CONTR_JOB_ID'];
				$size				= $row['SIZE'];
				$qty				= round($row['QTY'],4);
				$place 				= 'Stores';
				$type 				= 'C_SUB_GATE_PASS';
				
				//validate - no validations
				
					//add from locarion stock
					$response_insert	= insert_to_transaction_fabric($location,$place,0,$serial_no,$serial_year,$orderNo,$orderYear,$cutNo,$salesOrderId,$part,$size,$qty,$type,$userId);
					
					 if($row['COMPANY_TO_TYPE']==0) {//inter company
						//substract to sub contract location stock
						$type 		  		='C_SUB_GATE_PASS_IN';
						//$response_insert	= insert_to_transaction_fabric($toLocation,$place,$location,$serial_no,$serial_year,$orderNo,$orderYear,$cutNo,$salesOrderId,$part,$size,-$qty,$type,$userId);
					
					 }
			} //end of while
 			
			$status				= $obj_header->getStatus($serial_no,$serial_year);
			if($status ==-2 ){
				$response['type'] 				= 'pass';
				$response['msg'] 				= 'Cancelled successfully.';
			}
			$db->commit();
			
		}catch(Exception $e){
			$db->rollback();
			$response['msg'] 	=  $e->getMessage();;
			$response['error'] 	=  $error_handler->jTraceEx($e);
			$response['type']	=  'fail';
			$response['sql']	=  $db->getSql();
		}
		
		$db->disconnect();		
		echo json_encode($response);
	
	}
	
	
	
	
	function validate_header_approval_permision($serial_no,$serial_year){
		
		global $obj_header;
		global $obj_commonErr;
		global $userId;
		global $programCode;
		$type				= true;
		
		$where				="SUB_CONTRACT_GP_NO = '$serial_no' AND SUB_CONTRACT_GP_YEAR= '$serial_year'";
		$header_result		= $obj_header->select('*',NULL,$where,NULL,NULL);
		$header_array		=mysqli_fetch_array($header_result);
		
		$permision_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$userId,$programCode,'RunQuery');
		$permision_confirm	= $permision_arr['permision'];
		
		if($permision_confirm != 1) 
			throw new Exception($permision_arr['msg']);
 		else
			return true;
	}
	
	function validate_location($serial_no,$serial_year){
		
		global $obj_header;
		global $obj_commonErr;
		global $userId;
		global $programCode;
		global $location;
		
		$type				= true;
		
		$where				= "SUB_CONTRACT_GP_NO = '$serial_no' AND SUB_CONTRACT_GP_YEAR= '$serial_year'";
		$header_result		= $obj_header->select('*',NULL,$where,NULL,NULL);
		$header_array		= mysqli_fetch_array($header_result);
 		
		if($location != $header_array['LOCATION_ID']) 
			throw new Exception("Invalid Approval location");
 		else
			return true;
	}
	
	function  validate_details_approval($serial_no,$serial_year,$subContractNo,$subContractYear,$orderNo,$orderYear,$salesOrderId,$subContractJob,$size,$cutNo,$qty){
		
		global $obj_stock_fabric;
		global $obj_order_details;
		global $obj_detail;
		global $obj_sub_od_size_wise;
		
		
		global $userId;
		global $programCode;
		global $location;
		
		$type				= true;
		
		//stock qty
		$select				= " IFNULL(sum(dblQty),0) AS STOCK ";
		$where				= "intOrderNo = '$orderNo' AND intOrderYear = '$orderYear' AND intSalesOrderId = '$salesOrderId' AND strSize = '$size' AND strCutNo = '$cutNo' AND intLocationId = '$location'";
		$stk_result			= $obj_stock_fabric->select($select,NULL,$where,NULL,NULL);
		$stk_row			= mysqli_fetch_array($stk_result);
		$stkBal				=$stk_row['STOCK'];
		
		//sales order no
		$select				= " strSalesOrderNo";
		$where				= "intOrderNo = '$orderNo' AND intOrderYear = '$orderYear' AND intSalesOrderId = '$salesOrderId'";
		$od_result			= $obj_order_details->select($select,NULL,$where,NULL,NULL);
		$od_row				= mysqli_fetch_array($od_result);
		$salesOrder			= $od_row['strSalesOrderNo'];
		
		//sub contract qty
		$select				= " IFNULL(trn_order_sub_contract_size_wise_qty.QTY,0) AS qty";
		$join				= "	INNER JOIN trn_order_sub_contract_header ON trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_NO = trn_order_sub_contract_header.SUB_CONTRACT_NO AND trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_YEAR = trn_order_sub_contract_header.SUB_CONTRACT_YEAR";
		$where				= "	trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_NO= '$subContractNo' AND
								trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_YEAR = '$subContractYear' AND
								trn_order_sub_contract_size_wise_qty.SALES_ORDER_ID = '$salesOrderId' AND
								trn_order_sub_contract_size_wise_qty.SIZE = '$size'";
		$sc_result			= $obj_sub_od_size_wise->select($select,$join,$where,NULL,NULL);
		$sc_row				= mysqli_fetch_array($sc_result);
		$subContQty			=$sc_row['qty'];
		
		//alredy GP Qty
		$select				= " IFNULL(Sum(trn_order_sub_contract_gate_pass_details.QTY),0) AS qty";
		$join				= "	INNER JOIN trn_order_sub_contract_gate_pass_header ON trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_NO = trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_NO AND trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_YEAR = trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_YEAR";
		$where				= "	trn_order_sub_contract_gate_pass_header.`STATUS` = 1 AND
								trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_NO = '$subContractNo' AND
								trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_YEAR = '$subContractYear' AND
								trn_order_sub_contract_gate_pass_details.SALES_ORDER_ID = '$salesOrderId' AND
								trn_order_sub_contract_gate_pass_details.SIZE = '$size' AND
								trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_NO <> '$serial_no' AND
								trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_YEAR <> '$serial_year'";
		$sc_result			= $obj_detail->select($select,$join,$where,NULL,NULL);
		$sc_row				= mysqli_fetch_array($sc_result);
		$gpOtherQty			=$sc_row['qty'];
		
		//size wise this GP Qty
		$select				= " IFNULL(Sum(trn_order_sub_contract_gate_pass_details.QTY),0) AS qty";
		$join				= "	INNER JOIN trn_order_sub_contract_gate_pass_header ON trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_NO = trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_NO AND trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_YEAR = trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_YEAR";
		$where				= "	trn_order_sub_contract_gate_pass_details.SALES_ORDER_ID = '$salesOrderId' AND
								trn_order_sub_contract_gate_pass_details.SIZE = '$size' AND
								trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_NO = '$serial_no' AND
								trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_YEAR = '$serial_year'";
		$sc_result			= $obj_detail->select($select,$join,$where,NULL,NULL);
		$sc_row				= mysqli_fetch_array($sc_result);
		$thisGPQty			=$sc_row['qty'];

		$balToGP			= $subContQty-$gpOtherQty;

		if($thisGPQty > $subContQty-$gpOtherQty){ 
			$message		= 'There is no sub contract order balance for '.$od_row['strSalesOrderNo']."/".$size.". Existing balance is ".$balToGP;
			throw new Exception($message);
		}
		else if($qty > $stkBal) {
			$message		= 'There is no stock balance for '.$od_row['strSalesOrderNo']."/".$size."/".$cutNo.". Existing balance is ".$stkBal;
			throw new Exception($message);
		}
		//else
			return true;
		
		
	}
	
	function validate_rejection($serial_no,$serial_year){
		
		global $obj_header;
		global $obj_commonErr;
		global $userId;
		global $programCode;
		$type				= true;
		
		$where				="SUB_CONTRACT_GP_NO = '$serial_no' AND SUB_CONTRACT_GP_YEAR= '$serial_year'";
		$header_result		= $obj_header->select('*',NULL,$where,NULL,NULL);
		$header_array		=mysqli_fetch_array($header_result);
		
		$permision_arr		= $obj_commonErr->get_permision_withApproval_reject($header_array['STATUS'],$header_array['LEVELS'],$userId,$programCode,'RunQuery');
		$permision_reject	= $permision_arr['permision'];
		
		if($permision_reject != 1) 
			throw new Exception($permision_arr['msg']);
 		else
			return true;
		
	}
	
	function validate_cancel($serial_no,$serial_year){
		
		global $obj_header;
		global $obj_commonErr;
		global $userId;
		global $programCode;
		$type				= true;
		
		$where				="SUB_CONTRACT_GP_NO = '$serial_no' AND SUB_CONTRACT_GP_YEAR= '$serial_year'";
		$header_result		= $obj_header->select('*',NULL,$where,NULL,NULL);
		$header_array		=mysqli_fetch_array($header_result);
		
		$permision_arr		= $obj_commonErr->get_permision_withApproval_cancel($header_array['STATUS'],$header_array['LEVELS'],$userId,$programCode,'RunQuery');
		$permision_cancel	= $permision_arr['permision'];

		if($permision_cancel != 1) 
			throw new Exception($permision_arr['msg']);
 		else
			return true;
	}
	
	function upgrade_header_status($status,$serial_no,$serial_year){

		global $obj_header;
		
		$where = "SUB_CONTRACT_GP_NO = '$serial_no' AND SUB_CONTRACT_GP_YEAR= '$serial_year'";
		
		$data  =array(	'STATUS'		=> $status
		);
		$result_update_h		= $obj_header->upgrade($data,$where);
 		
		if(!$result_update_h['status']) 
			throw new Exception($result_update_h['msg']);
 		else
			return true;
		
	}
	
	function insert_to_approved_by($serial_no,$serial_year,$levels){
		
		global $obj_header;
		global $obj_approve_by;
		global $userId;
		global $obj_dateTime;
		
		$where = "SUB_CONTRACT_GP_NO = '$serial_no' AND SUB_CONTRACT_GP_YEAR= '$serial_year'";
 
		$data 				= array('SUB_CONTRACT_GP_NO'	=>$serial_no,
									'SUB_CONTRACT_GP_YEAR'	=>$serial_year,
									'LEVELS'				=>$levels,
									'USER'					=>$userId,
									'DATE'					=>$obj_dateTime->getCurruntDateTime(),
									'STATUS'				=>'0'
							);
		$result_insert_a		= $obj_approve_by->insert($data);

		if(!$result_insert_a['status']) 
			throw new Exception($result_insert_a['msg']);
 		else
			return true;
		
	}
	
	
	function get_gate_pass_detais_results($serial_no,$serial_year){
		
		global $obj_header;
		
		$select_det		=" IFNULL(mst_subcontractor.INTER_LOCATION_ID,0) as LOCATION_TO_ID,
							trn_orderdetails.intOrderNo,
							trn_orderdetails.intOrderYear,
							mst_subcontractor.INTER_COMPANY AS COMPANY_TO_TYPE,
							trn_order_sub_contract_gate_pass_details.CUT_NO,
							trn_orderdetails.intPart,
							trn_orderdetails.intSalesOrderId,
							trn_order_sub_contract_header.SUB_CONTRACT_NO,
							trn_order_sub_contract_header.SUB_CONTRACT_YEAR,
							trn_order_sub_contract_details.SUB_CONTR_JOB_ID, 
							trn_order_sub_contract_gate_pass_details.SIZE,
							trn_order_sub_contract_gate_pass_details.QTY";
		$join_det		="
							INNER JOIN trn_order_sub_contract_gate_pass_details ON trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_NO = trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_NO AND trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_YEAR = trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_YEAR
							INNER JOIN trn_order_sub_contract_header ON trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_NO = trn_order_sub_contract_header.SUB_CONTRACT_NO AND trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_YEAR = trn_order_sub_contract_header.SUB_CONTRACT_YEAR
							INNER JOIN trn_order_sub_contract_details ON trn_order_sub_contract_header.SUB_CONTRACT_NO = trn_order_sub_contract_details.SUB_CONTRACT_NO AND trn_order_sub_contract_header.SUB_CONTRACT_YEAR = trn_order_sub_contract_details.SUB_CONTRACT_YEAR AND trn_order_sub_contract_gate_pass_details.SALES_ORDER_ID = trn_order_sub_contract_details.SALES_ORDER_ID AND trn_order_sub_contract_gate_pass_details.SUB_CONTR_JOB_ID = trn_order_sub_contract_details.SUB_CONTR_JOB_ID
							INNER JOIN trn_orderdetails ON trn_order_sub_contract_header.ORDER_NO = trn_orderdetails.intOrderNo AND trn_order_sub_contract_header.ORDER_YEAR = trn_orderdetails.intOrderYear AND trn_order_sub_contract_details.SALES_ORDER_ID = trn_orderdetails.intSalesOrderId
							LEFT JOIN mst_subcontractor ON trn_order_sub_contract_header.SUB_CONTRACTOR_ID = mst_subcontractor.SUB_CONTRACTOR_ID
						";
		$where_det		="
							trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_NO = '$serial_no' AND
							trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_YEAR = '$serial_year'
						";							
		$details_result	= $obj_header->select($select_det,$join_det,$where_det,NULL,NULL);
		
 		if(!$details_result)
			throw new Exception("No details");
 		else
			return $details_result;
		
	}
	
	
	function insert_to_transaction_fabric($location,$place,$toLocation,$serial_no,$serial_year,$orderNo,$orderYear,$cutNo,$salesOrderId,$part,$size,$qty,$type,$userId){
		global $obj_stock_fabric;
		global $obj_dateTime;
		$data =array(
				'intLocationId'			=>$location,
				'strPlace'				=>$place,
				'intToLocationId'		=>$toLocation,
				'intDocumentNo'			=>$serial_no,
				'intDocumentYear'		=>$serial_year,
				'intOrderNo'			=>$orderNo,
				'intOrderYear'			=>$orderYear,
				'strCutNo'				=>$cutNo,
				'intSalesOrderId'		=>$salesOrderId,
				'intPart'				=>$part,
				'strSize'				=>$size,
				'dblQty'				=>$qty,
				'strType'				=>$type,
				'intUser'				=>$userId,
				'dtDate'				=>$obj_dateTime->getCurruntDateTime()
		);
		$result_insert_t		= $obj_stock_fabric->insert($data);
		
		if(!$result_insert_t['status']) 
			throw new Exception($result_insert_t['msg']);
 		else
			return true;
	}
	
	function update_header_status($status,$serial_no,$serial_year){

		global $obj_header;
		
		$where = "SUB_CONTRACT_GP_NO = '$serial_no' AND SUB_CONTRACT_GP_YEAR= '$serial_year'";
		
		$data  =array(	'STATUS'		=> $status 
		);
		$result_update_h		= $obj_header->update($data,$where);
 		
		if(!$result_update_h['status']) 
			throw new Exception($result_update_h['msg']);
 		else
			return true;
		
	}

	
?>
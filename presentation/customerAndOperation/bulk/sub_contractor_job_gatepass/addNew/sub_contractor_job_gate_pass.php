<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$location 				= $sessions->getLocationId();
$company 				= $sessions->getCompanyId();
$userId 				= $sessions->getUserId();

//BEGIN - INCLUDE FILES {
require_once 			"class/tables/trn_order_sub_contract_gate_pass_header.php";				$obj_gatepass_header		= new trn_order_sub_contract_gate_pass_header($db);		
require_once 			"class/tables/trn_order_sub_contract_gate_pass_details.php";			$obj_gatepass_detail		= new trn_order_sub_contract_gate_pass_details($db);
require_once 			"class/tables/trn_order_sub_contract_header.php";						$obj_sub_contract_header	= new trn_order_sub_contract_header($db);
require_once 			"class/tables/trn_orderheader.php";										$obj_trn_orderheader		= new trn_orderheader($db);
require_once 			"class/cls_commonErrorHandeling_get.php";								$obj_commonErr				= new cls_commonErrorHandeling_get($db);
require_once 			"class/tables/trn_order_sub_contract_gate_pass_approved_by.php";
require_once 			"class/tables/ware_stocktransactions_fabric.php";						$obj_transactions_fabric	= new ware_stocktransactions_fabric($db);
require_once 			"class/tables/trn_order_sub_contract_size_wise_qty.php";				$obj_size_wise_qty			= new trn_order_sub_contract_size_wise_qty($db);
//END 	}

$programCode			= 'P0874';

$serial_no 				= (!isset($_REQUEST['serial_no'])?'':$_REQUEST['serial_no']);
$serial_year 			= (!isset($_REQUEST['serial_year'])?'':$_REQUEST['serial_year']);
?>
<title>Sub Contract Gate Pass</title>
<body>
<?php
	$db->connect();
	
	$select_h				="  trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_NO,
								trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_YEAR,
								trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_NO,
								trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_YEAR,
								trn_order_sub_contract_header.ORDER_NO,
								trn_order_sub_contract_header.ORDER_YEAR,
								mst_subcontractor.SUB_CONTRACTOR_NAME,
								mst_locations.strName AS LOCATION,
								trn_order_sub_contract_gate_pass_header.DATE,
								trn_orderheader.strCustomerPoNo,
								mst_customer.strName AS CUSTOMER,
								trn_order_sub_contract_gate_pass_header.REMARKS,
								trn_order_sub_contract_gate_pass_header.CREATED_BY,  
								trn_order_sub_contract_gate_pass_header.CREATED_DATE,
								sys_users.strUserName AS USER_NAME,
								trn_orderheader.intCustomer,
								trn_order_sub_contract_header.SUB_CONTRACTOR_ID,
								trn_order_sub_contract_gate_pass_header.`STATUS`,
								trn_order_sub_contract_gate_pass_header.LEVELS, 
								trn_order_sub_contract_gate_pass_header.LOCATION_ID,
								SUM(trn_order_sub_contract_details.QTY) AS TotQTY,
								mst_financecurrency.strDescription AS CURRENCY,
								trn_orderheader.strContactPerson AS CONTACT_PERSON,
								mst_financepaymentsterms.strDescription AS PAYMENT_TERM,
								trn_orderdetails.intQty AS ORDER_QTY";
								
		$join_h				="	INNER JOIN trn_order_sub_contract_header ON trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_NO = trn_order_sub_contract_header.SUB_CONTRACT_NO AND trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_YEAR = trn_order_sub_contract_header.SUB_CONTRACT_YEAR
								INNER JOIN mst_subcontractor ON trn_order_sub_contract_header.SUB_CONTRACTOR_ID = mst_subcontractor.SUB_CONTRACTOR_ID
								INNER JOIN mst_locations ON mst_locations.intId = mst_subcontractor.INTER_LOCATION_ID
								INNER JOIN trn_orderheader ON trn_order_sub_contract_header.ORDER_NO = trn_orderheader.intOrderNo AND trn_order_sub_contract_header.ORDER_YEAR = trn_orderheader.intOrderYear
								INNER JOIN mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
								INNER JOIN sys_users ON trn_order_sub_contract_gate_pass_header.CREATED_BY = sys_users.intUserId
								INNER JOIN trn_order_sub_contract_details ON trn_order_sub_contract_header.SUB_CONTRACT_NO = trn_order_sub_contract_details.SUB_CONTRACT_NO AND trn_order_sub_contract_header.SUB_CONTRACT_YEAR = trn_order_sub_contract_details.SUB_CONTRACT_YEAR
								INNER JOIN mst_financecurrency ON trn_orderheader.intCurrency = mst_financecurrency.intId
								INNER JOIN mst_financepaymentsterms ON trn_orderheader.intPaymentTerm = mst_financepaymentsterms.intId
								INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_order_sub_contract_details.SALES_ORDER_ID
								";
		$where_h			="
								trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_NO = '$serial_no' AND
								trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_YEAR = '$serial_year'
							";	
													
		$header_result		= $obj_gatepass_header->select($select_h,$join_h,$where_h,NULL,NULL);
		 while($header_array=mysqli_fetch_array($header_result))
		 {
			$serialNo 		= $header_array['SUB_CONTRACT_GP_NO'];
			$SerialYear 	= $header_array['SUB_CONTRACT_GP_YEAR'];
			$sub_order_no	= $header_array['SUB_CONTRACT_NO'];
			$sub_order_year	= $header_array['SUB_CONTRACT_YEAR'];
			$custPO 		= $header_array['strCustomerPoNo'];
			$currency		= $header_array['CURRENCY'];
			$orderNo 		= $header_array['ORDER_NO'];
			$orderYear 		= $header_array['ORDER_YEAR'];
			$date 			= $header_array['DATE'];
			$customer 		= $header_array['CUSTOMER'];
			$remarks 		= $header_array['REMARKS'];
			$intStatus		= $header_array['STATUS'];
			$savedLevels 	= $header_array['LEVELS'];
			$user 			= $header_array['USER_NAME'];
			$locationId 	= $header_array['LOCATION_ID'];
			$totQty			= $header_array['TotQTY'];
			$locName 		= $header_array['SUB_CONTRACTOR_NAME'];
			$subConLocation	= $header_array['LOCATION'];
			$contactPerson	= $header_array['CONTACT_PERSON'];
			$paymentTerm	= $header_array['PAYMENT_TERM'];
			$orderQty		= $header_array['ORDER_QTY'];
		 }
		
	$orderResult	= $obj_trn_orderheader->select('mst_customer_locations_header.strName AS customerLocation,
													sys_users.strFullName AS marketer',
													'INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
													INNER JOIN mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
													INNER JOIN mst_financecurrency ON trn_orderheader.intCurrency = mst_financecurrency.intId
													INNER JOIN mst_financepaymentsterms ON trn_orderheader.intPaymentTerm = mst_financepaymentsterms.intId
													INNER JOIN sys_users ON trn_orderheader.intMarketer = sys_users.intUserId  
													INNER JOIN mst_customer_locations_header ON trn_orderheader.intCustomerLocation = mst_customer_locations_header.intId ',
													"trn_orderheader.intOrderNo='$orderNo' AND trn_orderheader.intOrderYear='$orderYear'",NULL,NULL,NULL);
													
	while($order_array = mysqli_fetch_array($orderResult))
	{
		
		$marketer			    	= $order_array['marketer'];
		$customerLocation			= $order_array['customerLocation'];
	}
			
	$permition_arr		= $obj_commonErr->get_permision_withApproval_save($intStatus,$savedLevels,$userId,$programCode,'RunQuery');
	$permision_save		= $permition_arr['permision'];
	
	$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($intStatus,$savedLevels,$userId,$programCode,'RunQuery');
	$permision_confirm	= $permition_arr['permision'];

		?>
<form id="frmSubContrJobGatePass" name="frmSubContrJobGatePass" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL" style="width:900">
		  <div class="trans_text">SUB CONTRACTOR JOB GATE PASS</div>
		  <table width="880" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><fieldset class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td width="12%" class="normalfnt">Serial No</td>
  <td width="17%" class="normalfnt"><input  id="txtSerialNo" class="normalfnt" style="width:70px;text-align:right" type="text" value="<?php echo $serial_no ?>" readonly disabled="disabled"/> <input  id="txtSerialYear" class="normalfnt" style="width:40px;text-align:right" type="text" value="<?php echo $serial_year ?>" readonly disabled="disabled"/></td>
  <td width="1%" class="normalfnt">&nbsp;</td>
  <td width="18%" class="normalfnt">&nbsp;</td>
  <td width="8%" class="normalfnt">&nbsp;</td>
  <td width="21%" class="normalfnt">&nbsp;</td>
  <td width="1%" class="normalfnt">&nbsp;</td>
  <td width="22%" align="right" class="normalfnt">&nbsp;</td>
</tr>        </table></fieldset></td>
      </tr>
      <tr>
        <td><fieldset  class="tableBorder_allRound">
          <table width="100%" border="0" cellpadding="0" cellspacing="0" >
            <tr>
              <td height="22" class="normalfnt">Sub Contract No</td>
              <td class="normalfnt"><select name="cboSerialYear" id="cboSerialYear" style="width:70px" class="validate[required]">
              <option value=""></option>
                <?php
					$subYear	= $obj_sub_contract_header->select('DISTINCT(SUB_CONTRACT_YEAR)',NULL,"STATUS=1",NULL,NULL);
					//$row	= mysqli_fetch_array($orderYear);
					//print_r($row);
					while($row	= mysqli_fetch_array($subYear))
					{
						if($row['SUB_CONTRACT_YEAR']==$sub_order_year)
						echo "<option value=\"".$row['SUB_CONTRACT_YEAR']."\" selected=\"selected\">".$row['SUB_CONTRACT_YEAR']."</option>";	
						else
						echo "<option value=\"".$row['SUB_CONTRACT_YEAR']."\" >".$row['SUB_CONTRACT_YEAR']."</option>";	
					}
				?>
              </select>
                <select name="cboSerialNo" id="cboSerialNo" style="width:118px" class="validate[required]">
                  <option value=""></option>
                  <?php
                  	
					$result = $obj_sub_contract_header->select('SUB_CONTRACT_NO',NULL,"STATUS=1",NULL,NULL);
					while($row = mysqli_fetch_array($result))
					{
						if($row['SUB_CONTRACT_NO']==$sub_order_no)
						echo "<option value=\"".$row['SUB_CONTRACT_NO']."\" selected=\"selected\">".$row['SUB_CONTRACT_NO']."</option>";	
						else
						echo "<option value=\"".$row['SUB_CONTRACT_NO']."\" >".$row['SUB_CONTRACT_NO']."</option>";	
					}
				?>
                </select></td>
              <td class="normalfnt">Order No</td>
              <td class="normalfnt" colspan="2"><select name="cboOrderYear" id="cboOrderYear" style="width:70px" class="validate[required]">
                <?php
					$orderYearArr = $obj_trn_orderheader->select('DISTINCT(intOrderYear)',NULL,'intStatus=1',NULL,NULL);
					//print_r($result);
					while($row	= mysqli_fetch_array($orderYearArr))
					{
						if($row['intOrderYear']==$orderYear)
						echo "<option value=\"".$row['intOrderYear']."\" selected=\"selected\">".$row['intOrderYear']."</option>";	
						else
						echo "<option value=\"".$row['intOrderYear']."\" >".$row['intOrderYear']."</option>";	
					}
				?>
              </select>
                <select name="cboOrderNo" id="cboOrderNo" style="width:118px" class="validate[required]">
                  <option value=""></option>
                  <?php
					$orderNoArr = $obj_trn_orderheader->select('intOrderNo',' INNER JOIN mst_locations ON trn_orderheader.intLocationId = mst_locations.intId ',"trn_orderheader.intStatus = 1 AND
mst_locations.intCompanyId ='$company' ",NULL,NULL);
					//print_r($result);
					while($row = mysqli_fetch_array($orderNoArr))
					{
						if($row['intOrderNo']==$orderNo)
						echo "<option value=\"".$row['intOrderNo']."\" selected=\"selected\">".$row['intOrderNo']."</option>";	
						else
						echo "<option value=\"".$row['intOrderNo']."\" >".$row['intOrderNo']."</option>";	
					}
				?>
                </select></td>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt">Date</td>
                <td align="right" class="normalfnt"><input name="dtDate" type="text" value="<?php  echo date("Y-m-d");?>" class="txtbox" id="dtDate" style="width:80px;" disabled="disabled" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/>
                <input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
            <tr>
              <td width="12%" height="22" class="normalfnt tdSubCon">Sub Contractor</td>
              <td class="normalfnt"><input type="text" id="txtGatepassTo" name="txtGatepassTo" disabled="disabled" style="width:200px" value="<?php echo $locName ?>" /></td>
              <td width="12%" height="22"  id="tdLocation"  class="normalfnt">Location</td>
               <td><input type="text"  disabled="disabled" id="txtSubLocation" name="txtLocation" value="<?php echo $subConLocation ?>" /></td>
			  <td class="normalfnt" colspan="2">&nbsp;</td>
              
              <td width="7%" class="normalfnt">Total Qty</td>
              <td width="13%" align="right" class="normalfnt"><input  id="txtSubQty" class="validate[required,custom[number]] invoQty normalfnt"  style="width:80px;text-align:right" type="text" value="<?php echo $totQty ?>" disabled="disabled"/></td> 
            
              
            <tr>
              <td class="normalfnt">Remarks</td>
              <td height="22" class="normalfnt" colspan="4"><textarea name="txtNote" id="txtNote"   rows="3" style="width:510px"><?php echo $remarks; ?></textarea></td>
              <td width="7%" valign="top" class="normalfnt">&nbsp;</td>
              <td class="normalfnt" valign="top">&nbsp;</td>
            </tr>  
            
              </table>
        </fieldset></td>
      </tr>
    </table></td></tr>    <tr><td><table width="100%">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0"  id="tblHeader1"  class="tableBorder_allRound">
          
          <tr height="22" >
            <td width="13%" class="normalfnt">Customer PO #</td>
            <td colspan="3"><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                  <td width="45%"><input value="<?php echo $custPO?>" name="txtCustomerPO" type="text" id="txtCustomerPO" style="width:220px" disabled/></td>
                  <td width="17%" class="normalfnt">Currency</td>
                  <td width="38%"><input value="<?php echo $currency ?>" name="txtCurrency" type="text" id="txtCurrency" style="width:190px" disabled/></td>
                </tr>
            </table></td>
            <td width="11%"><span class="normalfnt">Contact Person</span></td>
            <td width="14%"><input value="<?php echo $contactPerson; ?>" name="txtContactPerson" type="text" id="txtContactPerson" style="width:120px" disabled/></td>
          </tr>
          <tr height="22" >
            <td class="normalfnt">Customer</td>
            <td colspan="3"><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr >
                  <td width="45%"><input value="<?php echo $customer; ?>" name="txtCustomer" type="text" id="txtCustomer" style="width:220px" disabled/></td>
                  <td width="17%" class="normalfnt">Payment Term</td>
                  <td width="38%"><input value="<?php echo $paymentTerm ?>" name="txtPaymentTerm" type="text" id="txtPaymentTerm" style="width:190px" disabled/></td>
                </tr>
            </table></td>
            <td class="normalfnt">Received Qty</td>
            <td id="OrderQTY" name="txtOrderQTY"  class="normalfnt"><input value="<?php echo $orderQty ?>" name="txtOrderQTY" type="text" id="txtOrderQTY" style="width:120px" disabled/></td>
          </tr>
          <tr height="22">
            <td class="normalfnt">Location</td>
            <td colspan="3"><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr >
                <td width="45%"><input value="<?php echo $customerLocation ?>" name="txtLocation" type="text" id="txtLocation" style="width:220px" disabled/></td>
                <td width="17%" class="normalfnt">Marketer</td>
                <td width="38%"><input value="<?php echo $marketer ?>" name="txtMarketer" type="text" id="txtMarketer" style="width:190px" disabled/></td>
                </tr>
              </table></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          </table></td>
      </tr>
    </table></td></tr>
    <tr><td><table width="100%">
      <tr>
        <td class="normalfntRight" align="right">
        <a id="butInsertRowPopup" name="butInsertRowPopup"  class="button green small">Add New</a>
        </td></tr>
      </table></td></tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="880" id="tblMain"  class="bordered" >
            <tr>
              <th width="4%" >Del</th>
              <th width="11%" >Sales Order No</th>
              <th width="11%" >Job Type</th>
              <th width="9%" >Cut No</th>
              <th width="10%" >Part</th>
              <th width="10%" >Combo</th>
              <th width="8%" >Size</th>
              <th width="11%" >Stock Bal</th>
              <th width="12%" >Bal To GP</th>
              <th width="14%" >Qty</th>
              </tr>
              <tbody id="tblBody" >
               <?php
			  
              $select_d 	="  trn_order_sub_contract_gate_pass_details.SALES_ORDER_ID,
								trn_order_sub_contract_gate_pass_details.SUB_CONTR_JOB_ID,
								trn_order_sub_contract_gate_pass_details.CUT_NO,
								trn_order_sub_contract_gate_pass_details.SIZE,
								trn_order_sub_contract_gate_pass_details.QTY,
								trn_orderdetails.strSalesOrderNo,
								trn_orderdetails.strCombo,
								trn_orderdetails.intPart,
								mst_part.strName AS part,
								mst_sub_contract_job_types.`NAME`,
								trn_order_sub_contract_header.SUB_CONTRACT_NO,
								trn_order_sub_contract_header.SUB_CONTRACT_YEAR,
								trn_order_sub_contract_header.ORDER_NO,
								trn_order_sub_contract_header.ORDER_YEAR
							";
							
			$join_d		="	INNER JOIN trn_orderdetails ON trn_order_sub_contract_gate_pass_details.SALES_ORDER_ID = trn_orderdetails.intSalesOrderId
INNER JOIN trn_order_sub_contract_gate_pass_header ON trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_NO = trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_NO AND trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_YEAR = trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_YEAR
INNER JOIN trn_order_sub_contract_header ON trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_NO = trn_order_sub_contract_header.SUB_CONTRACT_NO AND trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_YEAR = trn_order_sub_contract_header.SUB_CONTRACT_YEAR AND trn_orderdetails.intOrderNo = trn_order_sub_contract_header.ORDER_NO AND trn_orderdetails.intOrderYear = trn_order_sub_contract_header.ORDER_YEAR
INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
INNER JOIN mst_sub_contract_job_types ON trn_order_sub_contract_gate_pass_details.SUB_CONTR_JOB_ID = mst_sub_contract_job_types.ID

							";
			$where_d	="
							trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_NO = '$serial_no' AND
							trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_YEAR = '$serial_year'
						";	
												
		$details_result	= $obj_gatepass_detail->select($select_d,$join_d,$where_d,NULL,NULL);
			$totQty=0;
			$totAmmount=0;
			while($row=mysqli_fetch_array($details_result))
			{
				$cutNo			=$row['CUT_NO'];
				$salesOrderNo	=$row['strSalesOrderNo'];
				$salesOrderID	=$row['SALES_ORDER_ID'];
				$partID			=$row['intPart'];
				$part			=$row['part'];
				$jobTypeID		=$row['SUB_CONTR_JOB_ID'];
				$jobType		=$row['NAME'];
				$size			=$row['SIZE'];
				$Qty			=$row['QTY'];
				$combo			=$row['strCombo'];
				//$stockBal		=$row['stockBal'];
				$subConNo		=$row['SUB_CONTRACT_NO'];
				$subConYear		=$row['SUB_CONTRACT_YEAR'];
				$orderNo		=$row['ORDER_NO'];
				$orderYear		=$row['ORDER_YEAR'];
				
				if($serial_no != '')
				{
	  ?>
            <tr class="td_saleOrderID" id="<?php echo $salesOrderID; ?>">
            <td style="text-align:center"><img class="mouseover removeRow" src="images/del.png" ></td>
            <td class="cls_td_salesOrder" id="<?php echo $salesOrderID; ?>" ><?php echo $salesOrderNo; ?></td>
            <td class="cls_td_jobType" id="<?php echo $jobTypeID; ?>" ><?php echo $jobType; ?></td>
			<td class="cls_td_cutNo" id="<?php echo $cutNo; ?>" ><?php echo $cutNo; ?></td>
			<td class="cls_td_part" id="<?php echo $partID; ?>"><?php echo $part; ?></td>
			<td style="text-align:center" class="cls_td_combo" id="<?php echo $combo; ?>" ><?php echo $combo; ?></td>
			<td style="text-align:center" class="cls_td_size" id="<?php echo $size; ?>" ><?php echo $size; ?></td>
            <?php 
			$stockBal	= $obj_transactions_fabric->select('SUM(dblQty) AS stockQTY',NULL,"intSalesOrderId='$salesOrderID' AND strSize='$size' AND strCutNo='$cutNo' AND intOrderNo='$orderNo' AND intOrderYear='$orderYear' AND intLocationId='$location'",NULL,NULL);
					$row1		= mysqli_fetch_array($stockBal);
			?>
			<td style="text-align:right" class="cls_td_Stockqty" id="<?php echo $row1['stockQTY']; ?>" ><?php echo $row1['stockQTY']; ?></td>
            <?php
			$qtyResult	= $obj_size_wise_qty->select('trn_order_sub_contract_size_wise_qty.QTY',NULL,
					"trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_NO = '$subConNo' AND
					trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_YEAR = '$subConYear' AND
					trn_order_sub_contract_size_wise_qty.SALES_ORDER_ID = '$salesOrderID' AND
					trn_order_sub_contract_size_wise_qty.SIZE = '$size'",NULL,NULL);
					$rowQty			= mysqli_fetch_array($qtyResult);
					$sizeWiseQty	= $rowQty['QTY'];
				//die ($sizeWiseQty);	
					$gpQtyResult	= $obj_gatepass_detail->select('SUM(trn_order_sub_contract_gate_pass_details.QTY) as gpQty',
					' INNER JOIN trn_order_sub_contract_gate_pass_header ON trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_NO = trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_NO AND trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_YEAR = trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_YEAR ',
					"trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_NO = '$subConNo' AND
trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_YEAR = '$subConYear' AND
trn_order_sub_contract_gate_pass_details.SALES_ORDER_ID = '$salesOrderID' AND
trn_order_sub_contract_gate_pass_details.SIZE = '$size' AND trn_order_sub_contract_gate_pass_header.STATUS = 1",NULL,NULL);
					$rowGP			= mysqli_fetch_array($gpQtyResult);
					$gpQty			= $rowGP['gpQty'];
					
					$balToGp		= $sizeWiseQty - $gpQty;
			?>
			<td style="text-align:right" class="cls_td_GPqty" id="<?php echo $balToGp; ?>"><?php echo $balToGp; ?></td>
            <td style="text-align:right" class="normalfntRight" id="<?php echo $Qty; ?>"><input id="txtQty"  class="validate[required] txtQty" type="text" value="<?php echo $Qty; ?>" style="width:80px;text-align:right" /></td>
            </tr>              
      <?php 
				}
			}
	  ?>
              
              </tbody>
 
         </table>
        </div></td></tr>
      </tr>

      <tr>
        <td align="center" class="tableBorder_allRound">
        <a name="butNew"  class="button white medium" id="butNew">New</a>
        <a id="butSave" name="butSave"  class="button white medium" style=" <?php if(($permision_save==1)){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>">Save</a>
         <a id="butConfirm" name="butConfirm" style=" <?php if(($permision_confirm==1)){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>" class="button white medium">Approve</a>
         <a id="butReport" name="butReport"  class="button white medium">Report</a>
         <a href="<?php echo $mainPage; ?>"  class="button white medium">Close</a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
<div style="width:500px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</html>
<?php
$db->disconnect();
?>


<?php
ini_set('display_errors',0);
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$requestType 	= $_REQUEST['requestType'];
	
	//include_once "{$backwardseperator}dataAccess/Connector2.php";
	include_once "../../../../../dataAccess/DBManager2.php";											$db						= new DBManager2();
	include_once "../../../../../class/tables/trn_order_sub_contract_header.php";
	include_once "../../../../../class/tables/trn_order_sub_contract_details.php";
	include_once "../../../../../class/tables/trn_order_sub_contract_gate_pass_header.php";
	include_once "../../../../../class/tables/trn_order_sub_contract_gate_pass_details.php";
	require_once "../../../../../class/tables/trn_order_sub_contract_gate_pass_approved_by.php";		$obj_approve_by			= new trn_order_sub_contract_gate_pass_approved_by($db);
	include_once "../../../../../class/tables/ware_stocktransactions_fabric.php";
	include_once "../../../../../class/cls_commonFunctions_get.php";
	require_once "../../../../../class/cls_commonErrorHandeling_get.php";
	include_once "../../../../../class/tables/trn_orderheader.php";
	include_once "../../../../../class/tables/trn_order_sub_contract_size_wise_qty.php";
	include_once "../../../../../class/dateTime.php";
	
	$obj_sub_contract_header			= new trn_order_sub_contract_header($db);
	$obj_sub_contract_details			= new trn_order_sub_contract_details($db);
	$obj_sub_contract_gate_pass_header	= new trn_order_sub_contract_gate_pass_header($db);
	$obj_sub_contract_gate_pass_detail	= new trn_order_sub_contract_gate_pass_details($db);
	$obj_transactions_fabric			= new ware_stocktransactions_fabric($db);
	$obj_trn_orderheader				= new trn_orderheader($db);
	$obj_common							= new cls_commonFunctions_get($db);
	$obj_commonErr						= new cls_commonErrorHandeling_get($db);
	$obj_size_wise_qty					= new trn_order_sub_contract_size_wise_qty($db);
	$obj_dateTime						= new dateTimes($db);
	
	$programName='Fabric Gate Pass';
	$programCode='P0874';
	$savedStatus		= true;
	$savedMasseged		= '';
	$error_sql			= '';
	/////////// type of print load part /////////////////////
						
if($requestType	== 'loadSubcontractDetails')
{ $db->connect();
	$subConNo	= $_REQUEST['serialNo'];
	$subConYear	= $_REQUEST['serialYear'];
	//$orderNo	= $_REQUEST['orderNo'];
	//$orderYear  = $_REQUEST['orderYear'];
	if($subConNo == '')
	{//die('ok');
		return;
	}
	$result = $obj_sub_contract_header->select('Sum(trn_order_sub_contract_details.QTY) AS QTY,
												trn_order_sub_contract_header.DATE,
												trn_order_sub_contract_header.REMARKS,
												mst_subcontractor.SUB_CONTRACTOR_NAME AS SUB_CONTRACTOR,
												mst_subcontractor.SUB_CONTRACTOR_ID,
												mst_subcontractor.INTER_COMPANY AS COMPANY_TO_TYPE,
												mst_subcontractor.INTER_COMPANY_ID AS COMPANY_TO,
												mst_subcontractor.INTER_LOCATION_ID AS LOCATION_TO,
												mst_locations.strName AS LOCATION,
												trn_order_sub_contract_header.ORDER_NO,
												trn_order_sub_contract_header.ORDER_YEAR',
												" INNER JOIN trn_order_sub_contract_details ON trn_order_sub_contract_header.SUB_CONTRACT_NO = trn_order_sub_contract_details.SUB_CONTRACT_NO AND trn_order_sub_contract_header.SUB_CONTRACT_YEAR = trn_order_sub_contract_details.SUB_CONTRACT_YEAR
INNER JOIN mst_subcontractor ON trn_order_sub_contract_header.SUB_CONTRACTOR_ID = mst_subcontractor.SUB_CONTRACTOR_ID
INNER JOIN mst_locations ON mst_locations.intId = mst_subcontractor.INTER_LOCATION_ID ",
												"trn_order_sub_contract_header.SUB_CONTRACT_NO = '$subConNo' AND trn_order_sub_contract_header.SUB_CONTRACT_YEAR = '$subConYear'",NULL,NULL);
	while($row = mysqli_fetch_array($result))
	{
		$data['type']			= $row['COMPANY_TO_TYPE'];
		$data['subContractor']	= $row['SUB_CONTRACTOR'];
		$data['location']		= $row['LOCATION'];
		$data['remark']			= $row['REMARKS'];
		$data['qty']			= $row['QTY'];
		$data['date']			= $row['DATE'];
		$data['orderNo']		= $row['ORDER_NO'];
		$data['orderYear']		= $row['ORDER_YEAR'];
		
	}
	$orderNo		= $data['orderNo'];
	$orderYear		= $data['orderYear'];
	$orderResult	= $obj_trn_orderheader->select('strCustomerPoNo AS poNo,
													trn_orderheader.strContactPerson as contactPerson,
													SUM(trn_orderdetails.intQty) as totQty,
													mst_customer.strName AS customer,
													mst_customer_locations_header.strName AS customerLocation,
													mst_financecurrency.strDescription AS currency,
													mst_financepaymentsterms.strDescription as paymentTerm,
													sys_users.strFullName AS marketer',
													'INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
													INNER JOIN mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
													INNER JOIN mst_financecurrency ON trn_orderheader.intCurrency = mst_financecurrency.intId
													INNER JOIN mst_financepaymentsterms ON trn_orderheader.intPaymentTerm = mst_financepaymentsterms.intId
													INNER JOIN sys_users ON trn_orderheader.intMarketer = sys_users.intUserId  
													INNER JOIN mst_customer_locations_header ON trn_orderheader.intCustomerLocation = mst_customer_locations_header.intId ',
													"trn_orderheader.intOrderNo='$orderNo' AND trn_orderheader.intOrderYear='$orderYear'",NULL,NULL,NULL);
													
	while($row = mysqli_fetch_array($orderResult))
	{
		$data['poNo']				= $row['poNo'];
		$data['contactPerson']		= $row['contactPerson'];
		$data['totQty']				= $row['totQty'];
		$data['customer']			= $row['customer'];
		$data['currency']			= $row['currency'];
		$data['paymentTerm']		= $row['paymentTerm'];
		$data['marketer']	    	= $row['marketer'];
		$data['customerLocation']	= $row['customerLocation'];
		
		$arrCombo[] = $data;
	}
	$db->disconnect();
	$response['arrCombo'] 	= $arrCombo;
	echo json_encode($response);
	
}
else if($requestType == 'getCurrentYear')
{
	$currYear	= $obj_dateTime->getCurruntYear();
	$response['currentYear'] = $currYear;
	echo json_encode($response);
}
else if($requestType == 'loadOrderNo')
{
	   		
	$db->connect();
	
	$orderYear  	= $_REQUEST['orderYear'];
	//$subConNo  		= $_REQUEST['subConNo'];
	//$subConYear 	= $_REQUEST['subConYear'];
	
	$orderNoResult	= $obj_trn_orderheader->select('DISTINCT(intOrderNo)',' INNER JOIN mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
 ',"trn_orderheader.intStatus = 1 AND intOrderYear='$orderYear'",NULL,NULL);
	$html .= "<option value=\"\"></option>";	
	while($row = mysqli_fetch_array($orderNoResult))
	{
		$html .= "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
		$response['OrderNo'] = $html;
	}
	$db->disconnect();
	
	echo json_encode($response);
}

else if($requestType == 'loadFirstSubConNo')
{
	$db->connect();
	
	$subConYear  	= $_REQUEST['subConYear'];
	
	$where			= "trn_order_sub_contract_header.SUB_CONTRACT_YEAR = '$subConYear' AND STATUS=1"; 
		
	$SubConResult	= $obj_sub_contract_header->select('DISTINCT(SUB_CONTRACT_NO)',NULL,$where,NULL,NULL);
	
	$html .= "<option value=\"\"></option>";	
	while($row = mysqli_fetch_array($SubConResult))
	{
		$html .= "<option value=\"".$row['SUB_CONTRACT_NO']."\">".$row['SUB_CONTRACT_NO']."</option>";
		
	}
	$response['serialNo'] = $html;
	$db->disconnect();
	
	echo json_encode($response);
}


else if($requestType == 'loadSubConNo')
{
	$db->connect();
	
	$subConYear  	= $_REQUEST['subConYear'];
	$orderNo		= $_REQUEST['orderNo'];
	$orderYear		= $_REQUEST['orderYear'];
	
	$where			= "trn_order_sub_contract_header.SUB_CONTRACT_YEAR = '$subConYear'"; 
	if($orderNo != '')				
		$where		   .= "AND	trn_order_sub_contract_header.ORDER_YEAR = '$orderYear'
						   AND	trn_order_sub_contract_header.ORDER_NO = '$orderNo' ";
	
	$SubConResult	= $obj_sub_contract_header->select('DISTINCT(SUB_CONTRACT_NO)',NULL,$where,NULL,NULL);
	
	while($row = mysqli_fetch_array($SubConResult))
	{
		$response['serialNo']	=$row['SUB_CONTRACT_NO'];
	}
	$db->disconnect();
	
	echo json_encode($response);
}
//load popup data according to salese ordr
else if($requestType == 'searchSaleseOrder')
{
	$db->connect();
	$salesOrderID				= $_REQUEST['saleseOrderID'];
	$orderNo	    			= $_REQUEST['orderNo'];
	$orderYear  				= $_REQUEST['orderYear'];
	$subConNo					= $_REQUEST['subConNo'];
	$subConYear					= $_REQUEST['subConYear'];
	
	$content	= '';
	$response['gridDetail']	= '';
	$select_r	= 'trn_orderdetails.strSalesOrderNo AS saleOrderNo,
								trn_orderdetails.intSalesOrderId AS saleOrderID,
								trn_order_sub_contract_size_wise_qty.SIZE AS size,
								trn_orderdetails.strCombo AS combo,
								ware_fabricreceiveddetails.strCutNo AS cutNo,
								mst_sub_contract_job_types.`NAME` AS jobType,
								ware_fabricreceiveddetails.intPart AS partID,
								mst_part.strName AS part,
								trn_order_sub_contract_details.SUB_CONTR_JOB_ID AS subConJobID
								';
	$join_r		=  ' INNER JOIN trn_order_sub_contract_size_wise_qty ON trn_order_sub_contract_header.SUB_CONTRACT_NO = trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_NO AND trn_order_sub_contract_header.SUB_CONTRACT_YEAR = trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_YEAR
								INNER JOIN trn_orderdetails ON trn_orderdetails.intSalesOrderId = trn_order_sub_contract_size_wise_qty.SALES_ORDER_ID AND trn_order_sub_contract_header.ORDER_NO = trn_orderdetails.intOrderNo AND trn_order_sub_contract_header.ORDER_YEAR = trn_orderdetails.intOrderYear
								INNER JOIN ware_fabricreceivedheader ON trn_order_sub_contract_header.ORDER_NO = ware_fabricreceivedheader.intOrderNo AND trn_order_sub_contract_header.ORDER_YEAR = ware_fabricreceivedheader.intOrderYear
								INNER JOIN ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear AND trn_order_sub_contract_size_wise_qty.SALES_ORDER_ID = ware_fabricreceiveddetails.intSalesOrderId AND trn_order_sub_contract_size_wise_qty.SIZE = ware_fabricreceiveddetails.strSize
								INNER JOIN trn_order_sub_contract_details ON trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_NO = trn_order_sub_contract_details.SUB_CONTRACT_NO AND trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_YEAR = trn_order_sub_contract_details.SUB_CONTRACT_YEAR AND trn_order_sub_contract_size_wise_qty.SALES_ORDER_ID = trn_order_sub_contract_details.SALES_ORDER_ID
								INNER JOIN mst_sub_contract_job_types ON trn_order_sub_contract_details.SUB_CONTR_JOB_ID = mst_sub_contract_job_types.ID
								INNER JOIN mst_part ON ware_fabricreceiveddetails.intPart = mst_part.intId ';
								
	$where_r	= "trn_order_sub_contract_header.SUB_CONTRACT_NO = '$subConNo' AND
								trn_order_sub_contract_header.SUB_CONTRACT_YEAR = '$subConYear' AND
								trn_order_sub_contract_header.ORDER_NO = '$orderNo' AND
								trn_order_sub_contract_header.ORDER_YEAR = '$orderYear'";
								
	if($salesOrderID != '')
	{							
	$where_r	.=	" AND trn_orderdetails.intSalesOrderId = '$salesOrderID'";	
	}
							
	
	$tblResult	= $obj_sub_contract_header->select($select_r,$join_r,$where_r,NULL,NULL);
	//die('ok');						
	while($row	= mysqli_fetch_array($tblResult))
	{	
		$salesOrderID	= $row['saleOrderID'];
		$saleseOrderNo	= $row['saleOrderNo'];
		$jobID			= $row['subConJobID'];
		$jobType		= $row['jobType'];
		$cutNo			= $row['cutNo'];
		$partID			= $row['partID'];
		$part			= $row['part'];
		$combo			= $row['combo'];
		$size			= $row['size'];
	//get balance to gatepass	         
	     $qtyResult		= $obj_size_wise_qty->select('trn_order_sub_contract_size_wise_qty.QTY',NULL,
					"trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_NO = '$subConNo' AND
					trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_YEAR = '$subConYear' AND
					trn_order_sub_contract_size_wise_qty.SALES_ORDER_ID = '$salesOrderID' AND
					trn_order_sub_contract_size_wise_qty.SIZE = '$size'",NULL,NULL);
					$rowQty			= mysqli_fetch_array($qtyResult);
					$sizeWiseQty	= $rowQty['QTY'];
					$gpQtyResult	= $obj_sub_contract_gate_pass_detail->select('SUM(trn_order_sub_contract_gate_pass_details.QTY) as gpQty',
					' INNER JOIN trn_order_sub_contract_gate_pass_header ON trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_NO = trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_NO AND trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_YEAR = trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_YEAR ',
					"trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_NO = '$subConNo' AND
trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_YEAR = '$subConYear' AND
trn_order_sub_contract_gate_pass_details.SALES_ORDER_ID = '$salesOrderID' AND
trn_order_sub_contract_gate_pass_details.SIZE = '$size' AND trn_order_sub_contract_gate_pass_header.STATUS = 1",NULL,NULL);
					$rowGP			= mysqli_fetch_array($gpQtyResult);
					$gpQty			= $rowGP['gpQty'];
					$balToGp		= $sizeWiseQty - $gpQty;
//end balance to gatepass			
//get stock balance
$stockBal	= $obj_transactions_fabric->select('SUM(dblQty) AS stockQTY',NULL,"intSalesOrderId='$salesOrderID' AND strSize='$size' AND strCutNo='$cutNo' AND intOrderNo='$orderNo' AND intOrderYear='$orderYear' AND intLocationId='$location'",NULL,NULL);
					$row1		= mysqli_fetch_array($stockBal);
					$stockQty	= $row1['stockQTY'];
//end stock balance
/*if($balToGp == 0)
{
	//$content .= '';
}
else
{*/
		$content .= "<tr><td style=\"text-align:center\"><input name=\"chkOrder\" id=\"chkOrder\" class=\"chkOrder\" type=\"checkbox\" value=\"\" /></td>";
		$content .= "<td id=\"".$salesOrderID."\" class=\"td_salesOrder\" >".$saleseOrderNo."</td>";
        $content .= "<td id=\"".$jobID."\" class=\"td_jobType\" >".$jobType."</td>";
        $content .= "<td id=\"".$cutNo."\" class=\"td_cutNo\" >".$cutNo."</td>";
        $content .= "<td id=\"".$partID."\" class=\"td_part\" >".$part."</td>";
        $content .= "<td id=\"".$combo."\" class=\"td_combo\" >".$combo."</td>";
        $content .= "<td id=\"".$size."\" class=\"td_size\" >".$size."</td>";
		$content .= "<td id=\"".$balToGp."\" class=\"td_qty\" style=\"text-align:right\" >".$balToGp."</td>";
		$content .= "<td id=\"".$stockQty."\" class=\"td_Stockqty\" style=\"text-align:right\" >".$stockQty."</td></tr>";
}
	//}
	$response['gridDetail'] = $content;
	echo json_encode($response);
	$db->disconnect();
}
//start save 
else if($requestType == 'saveData')
{
	$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
	$arrDetail			= json_decode($_REQUEST['arrDetail'],true);
	
	$db->connect();
	
	$gatePassNo			= $arrHeader['gatepassNo'];
	$gatepassYear		= $arrHeader['gatepassYear'];
	$subConNo			= $arrHeader['subConNo'];
	$subConYear			= $arrHeader['subConYear'];
	$orderNo			= $arrHeader['subConYear'];
	$date				= $obj_dateTime->getCurruntDate();
	$remarks			= $obj_common->replace($arrHeader['remarks']);
	
	//BEGIN - PERMISSION VALIDATE BEFORE SAVING {
	$result = validateBeforeSave($gatePassNo,$gatepassYear,$programCode,$userId);
	if(!$result['type'] && $savedStatus)
	{
		$savedStatus		= false;
		$savedMasseged 		= $result['msg'];
	}
//END	- PERMISSION VALIDATE BEFORE SAVING }

	
	if($gatePassNo =='' && $gatepassYear =='')
	{
		$sysNo_arry 		= $obj_common->GetSystemMaxNoNew('SUB_CONTRACT_GATE_PASS_NO',$company,'RunQuery');
	
		if(!$sysNo_arry["type"] && $savedStatus)
		{
			
			$savedStatus	= false;
			$savedMasseged 	= $sysNo_arry["errorMsg"];
			$error_sql		= $sysNo_arry["errorSql"];	
		}
		
		$gatePassNo			= $sysNo_arry["max_no"];
		$gatepassYear		= date('Y');
		//INSERT DATA TO HEADER FUNCTION CALL
		$result					= insertHeader($gatePassNo,$gatepassYear,$subConNo,$subConYear,$date,$remarks,$programCode,$userId,$location);
		if(!$result['type'] && $savedStatus)
		{
			
			$savedStatus		= false;
			$savedMasseged 		= $result['msg'];
			$error_sql			= $result['sql'];	
		}
		//BEGIN - INSERT DETAILS {
		$result					= insertDetails($gatePassNo,$gatepassYear,$arrHeader,$arrDetail);
		if(!$result['type'] && $savedStatus)
		{
			
			$savedStatus		= false;
			$savedMasseged 		= $result['msg'];
			$error_sql			= $result['sql'];	
		}
	    //END 	- INSERT DETAILS }

	}//END INSERT HEADER
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 			= "pass";
		$response['msg'] 			= "Saved successfully.";
		$response['gatePassNo'] 	= $gatePassNo;
		$response['gatePassYear'] 	= $gatepassYear;
	}
	else
	{
		$db->rollback();
		$response['type'] 	= "fail";
		$response['msg'] 	= $savedMasseged;
		$response['sql']	= $error_sql;
	}
	
	echo json_encode($response);
	
	$db->disconnect();//close connection.
}
else if($requestType == 'updateData')
{
	$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
	$arrDetail			= json_decode($_REQUEST['arrDetail'],true);
	
	$db->connect(); //open connection.
	
	$gatePassNo			= $arrHeader['gatepassNo'];
	$gatepassYear		= $arrHeader['gatepassYear'];
	$subConNo			= $arrHeader['subConNo'];
	$subConYear			= $arrHeader['subConYear'];
	$date				= $arrHeader['date'];
	$remarks			= $obj_common->replace($arrHeader['remarks']);

//BEGIN - PERMISSION VALIDATE BEFORE UPDATION {
	$result = validateBeforeSave($gatePassNo,$gatepassYear,$programCode,$userId);
	if(!$result['type'] && $savedStatus)
	{
		$savedStatus		= false;
		$savedMasseged 		= $result['msg'];
	}
//END	- PERMISSION VALIDATE BEFORE UPDATION }

//BEGIN - INSERT DATE IN SEPERATE FUNCTION {	
	$result					= updateHeader($gatePassNo,$gatepassYear,$subConNo,$subConYear,$date,$remarks,$programCode,$userId,$location);
	if(!$result['type'] && $savedStatus)
	{
		$savedStatus		= false;
		$savedMasseged 		= $result['msg'];
		$error_sql			= $result['sql'];	
	}
//END 	- INSERT DATE IN SEPERATE FUNCTION }		

//BEGIN - DELETE DETAILS {
	$result					= deleteDetails($gatePassNo,$gatepassYear,$subConNo,$subConYear);
	if(!$result['type'] && $savedStatus)
	{
		$savedStatus		= false;
		$savedMasseged 		= $result['msg'];
		$error_sql			= $result['sql'];	
	}
//END 	- DELETE DETAILS }

//BEGIN - INSERT DETAILS {
	$result					= insertDetails($gatePassNo,$gatepassYear,$arrHeader,$arrDetail);
	if(!$result['type'] && $savedStatus)
	{
		$savedStatus		= false;
		$savedMasseged 		= $result['msg'];
		$error_sql			= $result['sql'];	
	}
//END 	- INSERT DETAILS }

//BEGIN - UPDATE APPROVED BY STATUS AS 0
	$result					= updateMaxStatus($gatePassNo,$gatepassYear);
	if(!$result['type'] && $savedStatus)
	{
		$savedStatus		= false;
		$savedMasseged 		= $result['msg'];
		$error_sql			= $result['sql'];	
	}
//END 	- UPDATE APPROVED BY STATUS AS 0

	if($savedStatus)
	{
		$db->commit();
		$response['type'] 			= "pass";
		$response['msg'] 			= "Update successfully.";
		$response['gatePassNo'] 	= $gatePassNo;
		$response['gatePassYear'] 	= $gatepassYear;
	}
	else
	{
		$db->rollback();
		$response['type'] 			= "fail";
		$response['msg'] 			= $savedMasseged;
		$response['sql']			= $error_sql;
	}
	echo json_encode($response);
	$db->disconnect();
}
//save,update, delete functions
function insertHeader($gatePassNo,$gatepassYear,$subConNo,$subConYear,$date,$remarks,$programCode,$userId,$location)
{
	//global $obj_trn_order_sub_contract_header;
	global $obj_sub_contract_gate_pass_header;
	global $obj_common;
	global $db;
	global $obj_dateTime;
	$response['type']	= true;
	
//BEGIN - GET APPROVE LEVELS FROM SYS_APPROVELEVELS TABLE. {
	$result1 = $obj_common->getApproveLevels_new1($programCode,'RunQuery');
	if(!$result1['type'] && $response['type'])
	{
		$response['type']	= false;
		$response['msg']	= $result1['msg'];
	}
	else{
		$approveLevel		= $result1['ApproLevel'];
		$status				= $result1['ApproLevel']+1;
	}
	//$dateNow	= date('Y-m-d');
	
//END 	- GET APPROVE LEVELS FROM SYS_APPROVELEVELS TABLE. }

	$data  	= array('SUB_CONTRACT_GP_NO'=>$gatePassNo,
					'SUB_CONTRACT_GP_YEAR'=>$gatepassYear,
					'SUB_CONTRACT_NO'=>$subConNo,
					'SUB_CONTRACT_YEAR'=>$subConYear,
					'REMARKS'=>$remarks,
					'STATUS'=>$status,
					'LEVELS'=>$approveLevel,
					'DATE'=>$date,
					'CREATED_BY'=>$userId,
					'LOCATION_ID'=>$location,
					'CREATED_DATE'=>$obj_dateTime->getCurruntDateTime()				
					);
	
	$result1		= $obj_sub_contract_gate_pass_header->insert($data); // insert data to trn_order_sub_contract_header table
	if(!$result1['status'] && $response['type'])
	{
		$response['type']	= false;
		$response['msg']	= $result1['msg'];
		$response['sql']	= $result1['sql'];
	}
	return $response;
}

function insertDetails($gatePassNo,$gatepassYear,$arrHeader,$arrDetail)
{
	global $obj_sub_contract_gate_pass_detail;
	global $obj_sub_contract_header;
	global $obj_sub_contract_details;
	global $obj_transactions_fabric;
	global $obj_size_wise_qty;
	global $location;
	
	$response['type']	= true;
	
	$subConNo	= $arrHeader['subConNo'];
	$subConYear	= $arrHeader['subConYear'];
	$orderNo	= $arrHeader['orderNo'];
	$orderYear	= $arrHeader['orderYear'];
	
	foreach($arrDetail as $row)
	{
		$salesOrderID	= $row['saleOrderID'];
		$size			= $row['size'];
		$cutNo			= $row['cutNo'];
		$Qty			= $row['qty'];

		$qty[$salesOrderID][$size]+=$Qty;
//		echo $qty[$salesOrderID][$size];
		if($Qty == '')
		{
			$response['type']	= false;
			$response['msg']	= "Enter gate pass qty";
		}
//start - get bal to gatepass qty	
	else
	{	
		$qtyResult	= $obj_size_wise_qty->select('trn_order_sub_contract_size_wise_qty.QTY',NULL,
					"trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_NO = '$subConNo' AND
					trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_YEAR = '$subConYear' AND
					trn_order_sub_contract_size_wise_qty.SALES_ORDER_ID = '$salesOrderID' AND
					trn_order_sub_contract_size_wise_qty.SIZE = '$size'",NULL,NULL);
					$rowQty			= mysqli_fetch_array($qtyResult);
					$sizeWiseQty	= $rowQty['QTY'];
				//die ($sizeWiseQty);	
					$gpQtyResult	= $obj_sub_contract_gate_pass_detail->select('SUM(trn_order_sub_contract_gate_pass_details.QTY) as gpQty',
					' INNER JOIN trn_order_sub_contract_gate_pass_header ON trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_NO = trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_NO AND trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_YEAR = trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_YEAR ',
					"trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_NO = '$subConNo' AND
trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_YEAR = '$subConYear' AND
trn_order_sub_contract_gate_pass_details.SALES_ORDER_ID = '$salesOrderID' AND
trn_order_sub_contract_gate_pass_details.SIZE = '$size' AND trn_order_sub_contract_gate_pass_header.STATUS = 1",NULL,NULL);
					$rowGP			= mysqli_fetch_array($gpQtyResult);
					$gpQty			= $rowGP['gpQty'];
					$balToGp		= $sizeWiseQty - $gpQty; 
//end - get bal to gatepass qty					
//start - get stock balance		
		$stockBal	= $obj_transactions_fabric->select('SUM(dblQty) AS stockQty',NULL,"intSalesOrderId='$salesOrderID' AND strSize='$size' AND strCutNo='$cutNo' AND intOrderNo='$orderNo' AND intOrderYear='$orderYear' AND intLocationId='$location'",NULL,NULL);
		$rowStock		= mysqli_fetch_array($stockBal);
		$GPQty			= $qty[$salesOrderID][$size];
		$balStock		= $rowStock['stockQty'];
//start - get stock balance			
		if(round($GPQty) > round($balToGp))
		{
			$response['type']	= false;
			$response['msg']	= "Gate pass qty exceeds balance to gate pass qty";
		}
		
		else if($Qty > $balStock)
		{
			$response['type']	= false;
			$response['msg']	= "Gate pass qty exceeds stock balance.";
		}
		else
		{
			$data  	= array('SUB_CONTRACT_GP_NO'=>$gatePassNo,
							'SUB_CONTRACT_GP_YEAR'=>$gatepassYear,
							'SALES_ORDER_ID'=>$row['saleOrderID'],
							'SUB_CONTR_JOB_ID'=>$row['jobType'],
							'CUT_NO'=>$row['cutNo'],
							'SIZE'=>$row['size'],
							'QTY'=>$row['qty'],
							'BAL_TO_RETURN_QTY'=>$row['qty']);
							
			$result1		= $obj_sub_contract_gate_pass_detail->insert($data); // insert data to trn_order_sub_contract_details table			
			if(!$result1['status'] && $response['type'])
			{
				$response['type']	= false;
				$response['msg']	= $result1['msg'];
				$response['sql']	= $result1['sql'];
			}
			
		}
		}
	}
	return $response;
}

function updateHeader($gatePassNo,$gatepassYear,$subConNo,$subConYear,$date,$remarks,$programCode,$userId,$location)
{
	global $obj_sub_contract_gate_pass_header;
	global $obj_common;
	global $db;
	
	$response['type']	= true;
		
//BEGIN - GET APPROVE LEVELS FROM SYS_APPROVELEVELS TABLE. {
	$result1 = $obj_common->getApproveLevels_new1($programCode,'RunQuery');
	if(!$result1['type'])
	{
		$response['type']	= false;
		$response['msg']	= $result1['msg'];
	}
	else{
		$approveLevel		= $result1['ApproLevel'];
		$status				= $result1['ApproLevel']+1;
	}
	$modDate	=	date('Y-m-d');
//END 	- GET APPROVE LEVELS FROM SYS_APPROVELEVELS TABLE. }

	$data  	= array(
					'REMARKS'=>$remarks,
					'STATUS'=>$status,
					'LEVELS'=>$approveLevel,
					'DATE'=>$date,
					'MODIFIED_DATE'=>$modDate,
					'MODIFIED_BY'=>$userId,
					'LOCATION_ID'=>$location				
					);
	$where	= 'SUB_CONTRACT_GP_NO = "'.$gatePassNo.'" AND SUB_CONTRACT_GP_YEAR = "'.$gatepassYear.'" AND SUB_CONTRACT_NO ="'.$subConNo.'" AND SUB_CONTRACT_YEAR="'.$subConYear.'"';
		
	$result1	= $obj_sub_contract_gate_pass_header->update($data,$where);	
	if(!$result1['status'])
	{
		$response['type']	= false;
		$response['msg']	= $result1['msg'];
		$response['sql']	= $result1['sql'];
	}
	return $response;
}
function deleteDetails($gatePassNo,$gatepassYear,$subConNo,$subConYear)
{
	global $obj_sub_contract_gate_pass_detail;
	$response['type']	= true;
	
	$where	= 'SUB_CONTRACT_GP_NO="'.$gatePassNo.'" AND SUB_CONTRACT_GP_YEAR = "'.$gatepassYear.'" ' ;
	
	$result1 = $obj_sub_contract_gate_pass_detail->delete($where);
	if(!$result1['status'] && ($response['type']=true))
	{
		$response['type']	= false;
		$response['msg']	= $result1['msg'];
		$response['sql']	= $result1['sql'];
	}
	return $response;
}

function validateBeforeSave($gatePassNo,$gatepassYear,$programCode,$userId)
{
	global $obj_commonErr;
	global $obj_sub_contract_gate_pass_header;
	
	$response['type']	= true;
	
	$where	= 'SUB_CONTRACT_GP_NO="'.$gatePassNo.'" AND SUB_CONTRACT_GP_YEAR = "'.$gatepassYear.'" ' ;
	
	$header_result	= $obj_sub_contract_gate_pass_header->select('*',NULL,$where,NULL,NULL); // get header data
	$headerRow		= mysqli_fetch_array($header_result);
	
	$result1  		= $obj_commonErr->get_permision_withApproval_save($headerRow['STATUS'],$headerRow['LEVELS'],$userId,$programCode,'RunQuery'); // get revise permission
	if($result1['type']=='fail')
	{
		$response['type']	= false;
		$response['msg']	= $result1['msg'];
	}
	return $response;
}

function updateMaxStatus($gatePassNo,$gatepassYear)
{
	global $obj_approve_by;
	$responce['type']	= true;
	
	$cols		= " MAX(STATUS) AS MAX_STATUS ";
	
	$where		= " SUB_CONTRACT_GP_NO = '".$gatePassNo."' AND
					SUB_CONTRACT_GP_YEAR = '".$gatepassYear."' ";
	
	$result 	= $obj_approve_by->select($cols,$join = null,$where, $order = null, $limit = null);	
	$row		= mysqli_fetch_array($result);
	
	$maxStatus	= $row['MAX_STATUS']+1;
	
	$data  		= array('STATUS' => $maxStatus);
	
	$where		= 'SUB_CONTRACT_GP_NO = "'.$gatePassNo.'" AND SUB_CONTRACT_GP_YEAR = "'.$gatepassYear.'" AND STATUS = 0 ' ;
	$result_arr	= $obj_approve_by->update($data,$where);
	if(!$result_arr['status'])
	{
		$responce['type']	= false;
		$responce['msg']	= $result_arr['msg'];
		$responce['sql']	= $result_arr['sql'];
	}
	return $responce;
}
?>



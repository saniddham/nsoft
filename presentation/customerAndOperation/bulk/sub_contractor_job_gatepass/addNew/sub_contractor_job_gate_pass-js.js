$(document).ready(function() {
	setCurrentYear();
/*	$('#frmSubContrJobGatePass #cboOrderYear').val(currentYear);
	$('#frmSubContrJobGatePass #cboSerialYear').val(currentYear);*/
	if($('#txtSerialNo').val() == '')
	{
		sortOrderNo();
		loadFirstSubConNo();
	}
	$("#frmSubContrJobGatePass").validationEngine();	
	$('#frmPopup #chkAll').die('click').live('click',function(){
		CheckAll_Approve($(this).is(':checked'));
	});
	$('#frmSubContrJobGatePass #butInsertRowPopup').die('click').live('click',loadPopup);
	$('#frmPopup #butAdd').die('click').live('click',addClickedRows);
	$('#frmPopup #imgSearchItems').die('click').live('click',searchSaleseOrder);
	$('#frmSubContrJobGatePass #cboOrderYear').die('change').live('change',sortOrderNo);
	$('#frmSubContrJobGatePass #cboSerialYear').die('change').live('change',loadSubConNo);
	$('#frmSubContrJobGatePass #cboOrderNo').change(function(e) {
		clearData();
		loadSubConNo();
	});
	$('#frmSubContrJobGatePass #butNew').die('click').live('click',clearAll);
	$("#frmSubContrJobGatePass #cboSerialNo").die('change').live('change',loadSubcontract);
	//$('#frmSubContrJobGatePass #cboOrderYear').change(sortOrderNo);
	$('#frmSubContrJobGatePass #butSave').die('click').live('click',saveDetails);
	$('#frmSubContrJobGatePass .removeRow').die('click').live('click',deleteRow);
	$('#frmSubContrJobGatePass #butConfirm').die('click').live('click',Confirm);
	$('#frmSubContrJobGatePass #butReport').die('click').live('click',loadReport);				
});

	function loadSubcontract()
	{ 	showWaiting();
		var serialYear	= $('#frmSubContrJobGatePass #cboSerialYear').val();	
		var serialNo	= $('#frmSubContrJobGatePass #cboSerialNo').val();
		var url 		= "presentation/customerAndOperation/bulk/sub_contractor_job_gatepass/addNew/sub_contractor_job_gate_pass-db.php?requestType=loadSubcontractDetails";
						
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							data:"serialYear="+serialYear+"&serialNo="+serialNo,  
							async:false,
							success:function(json){
								clearData();
								clearGrid();
								var length = json.arrCombo.length;
								
								var arrCombo = json.arrCombo;
									
								for(var i=0;i<length;i++)
								{
									if(arrCombo[i]['type'] == 1)
									{
										$('#txtSubLocation').hide();
										$('#tdLocation').hide();
										$('#txtGatepassTo').val(arrCombo[i]['subContractor']);	
																				
									}
									else
									{
										$('#txtSubLocation').show();
										$('#tdLocation').show();
										$('#txtGatepassTo').val(arrCombo[i]['subContractor']);
										$('#txtSubLocation').val(arrCombo[i]['location']);
																				
									}
									$('#frmSubContrJobGatePass #cboOrderYear').val(arrCombo[i]['orderYear']);
									sortOrderNo();
									$('#frmSubContrJobGatePass #cboOrderNo').val(arrCombo[i]['orderNo']);
									$('#dtDate').val(arrCombo[i]['date']);
									$('#txtNote').val(arrCombo[i]['remark']);
									$('#txtSubQty').val(arrCombo[i]['qty']);
									$('#txtCustomerPO').val(arrCombo[i]['poNo']);
									$('#txtCurrency').val(arrCombo[i]['currency']);
									$('#txtContactPerson').val(arrCombo[i]['contactPerson']);
									$('#txtCustomer').val(arrCombo[i]['customer']);
									$('#txtPaymentTerm').val(arrCombo[i]['paymentTerm']);
									$('#txtOrderQTY').val(arrCombo[i]['totQty']);
									$('#txtLocation').val(arrCombo[i]['customerLocation']);
									$('#txtMarketer').val(arrCombo[i]['marketer']);
		
								}
								hideWaiting();
							},
							error:function(xhr,status)
							{
								hideWaiting();
								return;;
							}
						});
	}
	//end sub contractor order details
	
	function loadPopup()
	{
		//var row=this.parentNode.parentNode.rowIndex;
		var orderNo		= $('#frmSubContrJobGatePass #cboOrderNo').val();
		var orderYear	= $('#frmSubContrJobGatePass #cboOrderYear').val();
		var subConNo	= $('#frmSubContrJobGatePass #cboSerialNo').val();
		var subConYear	= $('#frmSubContrJobGatePass #cboSerialYear').val();
		//var locationID	= $('#txtSubLocation').val();
		
		if(orderNo==''){
			alert("Please select the Order No.");
			return false;
		}
		
		popupWindow3('1');
		$('#popupContact1').load('presentation/customerAndOperation/bulk/sub_contractor_job_gatepass/addNew/sub_contractor_job_gate_pass_popup.php?orderNo='+orderNo+'&orderYear='+orderYear+'&subConNo='+subConNo+'&subConYear='+subConYear,function(){
				searchSaleseOrder();
				$('#frmPopup #butClose1').die('click').live('click',disablePopup);
			});
	}
	
	function addClickedRows()
	{
		$('.chkOrder:checked').each(function(){
				
				var popObj				= $(this).parent().parent();
				var saleOrderID			= popObj.find('.td_salesOrder').attr('id');
				var salesOrderNo		= popObj.find('.td_salesOrder').html();
				var jobTypeID			= popObj.find('.td_jobType').attr('id');
				var jobTypeName 		= popObj.find('.td_jobType').html();
				var cutNo				= popObj.find('.td_cutNo').attr('id');
				var part				= popObj.find('.td_part').html();
				var partID				= popObj.find('.td_part').attr('id');
				var combo				= popObj.find('.td_combo').attr('id');	
				var size				= popObj.find('.td_size').attr('id');	
				var qty					= popObj.find('.td_qty').attr('id');
				var stockQty			= popObj.find('.td_Stockqty').attr('id');			
				//var GPqty				= $('#				
				var found 				= false;
				$('#frmSubContrJobGatePass #tblMain #tblBody >tr').each(function(){
					var mainObj		= $(this).attr('id');
					var mainSize	= $(this).find('.cls_td_size').attr('id');
					var mainCutNo	= $(this).find('.cls_td_cutNo').attr('id');					
                  
					if(saleOrderID == mainObj && size == mainSize && cutNo == mainCutNo )
						found = true;
				});
				
				if(!found)
				{					
					var x = "<tr class =\"td_saleOrderID\" id=\""+saleOrderID+"\">"+
					"<td style=\"text-align:center\"><img class=\"mouseover removeRow\" src=\"images/del.png\" /></td>"+
					"<td class=\"cls_td_salesOrder\" id=\""+saleOrderID+"\">"+salesOrderNo+"</td>"+
					"<td class=\"cls_td_jobType\" id=\""+jobTypeID+"\">"+jobTypeName+"</td>"+
					"<td class=\"cls_td_cutNo\" id=\""+cutNo+"\">"+cutNo+"</td>"+
					"<td class=\"cls_td_part\" id=\""+partID+"\">"+part+"</td>"+
					"<td class =\"cls_td_combo\" id=\""+combo+"\" style=\"text-align:center\">"+combo+"</td>"+
					"<td class =\"cls_td_size\" id=\""+size+"\" style=\"text-align:center\">"+size+"</td>"+
					"<td class =\"cls_td_Stockqty\" style=\"text-align:right\" id=\""+stockQty+"\">"+stockQty+"</td>"+
					"<td class =\"cls_td_GPqty\" style=\"text-align:right\" id=\""+qty+"\">"+qty+"</td>"+
					"<td style=\"text-align:right\"><input  id=\"txtQty\"  class=\"validate[required] txtQty\"  style=\"width:80px;text-align:right\" type=\"text\"/></td></tr>";
					
					$("#frmSubContrJobGatePass #tblMain #tblBody:last").append(x); 
				}
			});
			disablePopup();
	}
	
	function sortOrderNo()
	{
		var subConNo	= $('#frmSubContrJobGatePass #cboSerialNo').val();
		var subConYear	= $('#frmSubContrJobGatePass #cboSerialYear').val();
		var orderYear	= $('#frmSubContrJobGatePass #cboOrderYear').val();
		var url 		= "presentation/customerAndOperation/bulk/sub_contractor_job_gatepass/addNew/sub_contractor_job_gate_pass-db.php?requestType=loadOrderNo";
					
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"orderYear="+orderYear+"&subConNo="+subConNo+"&subConYear="+subConYear,  
			async:false,
			success:function(json){
			
				$('#frmSubContrJobGatePass #cboOrderNo').html(json.OrderNo);
			}
		})
	}
	
	function loadFirstSubConNo()
	{
		var subConYear	= $('#frmSubContrJobGatePass #cboSerialYear').val();
	
		var url 		= "presentation/customerAndOperation/bulk/sub_contractor_job_gatepass/addNew/sub_contractor_job_gate_pass-db.php?requestType=loadFirstSubConNo";
					
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"subConYear="+subConYear,  
			async:false,
			success:function(json){
			var subCoNo = json.serialNo;
				$('#frmSubContrJobGatePass #cboSerialNo').html(subCoNo);
			}
		})			
	}
	
	function loadSubConNo()
	{
		var subConYear	= $('#frmSubContrJobGatePass #cboSerialYear').val();
		var orderNo		= $('#frmSubContrJobGatePass #cboOrderNo').val();
		var orderYear	= $('#frmSubContrJobGatePass #cboOrderYear').val();
		
		var url 		= "presentation/customerAndOperation/bulk/sub_contractor_job_gatepass/addNew/sub_contractor_job_gate_pass-db.php?requestType=loadSubConNo";
					
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"subConYear="+subConYear+"&orderNo="+orderNo+"&orderYear="+orderYear,  
			async:false,
			success:function(json){
				var subCoNo = json.serialNo;
				$('#frmSubContrJobGatePass #cboSerialNo').val(subCoNo);
				loadSubcontract();			
			}
		})		
	}
	
	function CheckAll_Approve(checked)
	{
		$('.chkOrder').each(function(){
			if(!$(this).is(':disabled')){
				if(checked)
					$(this).attr('checked','checked');
				else
					$(this).removeAttr('checked');
			}
		});
	}	

	function saveDetails()
	{
		showWaiting();
		if(!$('#frmSubContrJobGatePass').validationEngine('validate'))
		{
			var t = setTimeout("alertx('#frmSubContrJobGatePass')",4000);
			hideWaiting();
			return;
		}
	
		var gatepassNo		= $('#txtSerialNo').val();
		var gatepassYear	= $('#txtSerialYear').val();
		var subConNo		= $('#frmSubContrJobGatePass #cboSerialNo').val();
		var subConYear		= $('#frmSubContrJobGatePass #cboSerialYear').val();
		var orderNo			= $('#frmSubContrJobGatePass #cboOrderNo').val();
		var orderYear		= $('#frmSubContrJobGatePass #cboOrderYear').val();
		var remarks			= $('#txtNote').val();
		var date			= $('#dtDate').val();

		var requestType 	= ($('#frmSubContrJobGatePass #txtSerialNo').val()==""?'saveData':'updateData');
		var url 			= "presentation/customerAndOperation/bulk/sub_contractor_job_gatepass/addNew/sub_contractor_job_gate_pass-db.php?requestType="+requestType;
		var arrHeader = "{";
							arrHeader += '"gatepassNo":"'+gatepassNo+'",' ;
							arrHeader += '"gatepassYear":"'+gatepassYear+'",' ;
							arrHeader += '"subConNo":"'+subConNo+'",' ;
							arrHeader += '"subConYear":"'+subConYear+'",';
							arrHeader += '"orderNo":"'+orderNo+'",' ;
							arrHeader += '"orderYear":"'+orderYear+'",';
							arrHeader += '"date":"'+date+'",';
							arrHeader += '"remarks":'+URLEncode_json(remarks)+'' ;
 			arrHeader  += "}";
		
		var arrDetail	 = "[";
		$('#frmSubContrJobGatePass #tblMain #tblBody >tr').each(function(){
			var saleOrderID		= $(this).attr('id');
			var jobType			= $(this).find('.cls_td_jobType').attr('id');
			var cutNo			= $(this).find('.cls_td_cutNo').attr('id');
			var size			= $(this).find('.cls_td_size').attr('id');	
			var qty				= $(this).find('.txtQty').val();	
			var GpQty			= $(this).find('.cls_td_GPqty').attr('id');	
			var stockQty		= $(this).find('.cls_td_Stockqty').attr('id');	
			
				arrDetail 	+= "{";
				arrDetail 	+= '"saleOrderID":"'+saleOrderID+'",' ;
				arrDetail 	+= '"jobType":"'+jobType+'",';
				arrDetail 	+= '"cutNo":"'+cutNo+'",';
				arrDetail 	+= '"size":"'+size+'",';
				arrDetail 	+= '"qty":"'+qty+'",' ;
				arrDetail 	+= '"GpQty":"'+GpQty+'",' ;
				arrDetail 	+= '"stockQty":"'+stockQty+'"' ;
				arrDetail 	+=  "},";
		});
		arrDetail 	 = arrDetail.substr(0,arrDetail.length-1);
		arrDetail 	+= "]";
		
		var data   ="arrHeader="+arrHeader;
		data   	  +="&arrDetail="+arrDetail;
		
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmSubContrJobGatePass #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					
					if(json.type=='pass')
					{
						$('#frmSubContrJobGatePass #txtSerialNo').val(json.gatePassNo);
						$('#frmSubContrJobGatePass #txtSerialYear').val(json.gatePassYear);
						var t=setTimeout("alertx()",1000);
						$('#frmSubContrJobGatePass #butConfirm').show();
						
						hideWaiting();
						return;
					}
					else
					{
						hideWaiting();
					}
				},
				error:function(xhr,status)
				{
					$('#frmSubContrJobGatePass #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					hideWaiting();
					return;;
				}
		});
	}
	
	function clearData()
	{
	 	$('#frmSubContrJobGatePass #txtGatepassTo').val('');
		$('#frmSubContrJobGatePass #txtLocation').val('');
		$('#frmSubContrJobGatePass #txtSubQty').val('');
		$('#frmSubContrJobGatePass #txtNote').val('');
		$('#frmSubContrJobGatePass #txtCustomerPO').val('');
		$('#frmSubContrJobGatePass #txtCurrency').val('');
		$('#frmSubContrJobGatePass #txtContactPerson').val('');
		$('#frmSubContrJobGatePass #txtCustomer').val('');
		$('#frmSubContrJobGatePass #txtPaymentTerm').val('');
		$('#frmSubContrJobGatePass #txtOrderQTY').val('');
		$('#frmSubContrJobGatePass #txtSubLocation').val('');
		$('#frmSubContrJobGatePass #txtMarketer').val('');
	}
	
	function clearAll()
	{
		window.location.href = '?q=874';	
		loadSubConNo();
		sortOrderNo();
	}
	
	function alertx()
	{
		$('#frmSubContrJobGatePass #butSave').validationEngine('hide')	;
	}

	function deleteRow()
	{
		$(this).parent().parent().remove();
 	}
	
	function clearGrid()
	{
		var rowCount = document.getElementById('tblMain').rows.length;
		for(var i=1;i<rowCount;i++)
		{
				document.getElementById('tblMain').deleteRow(1);
		}		
	}
	
	function Confirm()
	{
		var url  = "?q=906&serial_no="+$('#frmSubContrJobGatePass #txtSerialNo').val();
			url += "&serial_year="+$('#frmSubContrJobGatePass #txtSerialYear').val();
			url += "&mode=Confirm";
		window.open(url,'rpt_sub_contractor_job_gate_pass.php');
	}
	
	function loadReport()
	{
		if($('#frmSubContrJobGatePass #txtSerialNo').val()=='')
		{
			$('#frmSubContrJobGatePass #butReport').validationEngine('showPrompt','No Gate pass No to view Report','fail');
			return;	
		}
		var url  = "?q=906&serial_no="+$('#frmSubContrJobGatePass #txtSerialNo').val();
			url += "&serial_year="+$('#frmSubContrJobGatePass #txtSerialYear').val();
		window.open(url,'rpt_sub_contractor_job_gate_pass.php');
	}
	
	function searchSaleseOrder()
	{
		$("#tblPopup tr:gt(0)").remove();
		var saleseOrderID	= $('#frmPopup #cboSalesOrderNoP').val();
		var subConNo		= $('#frmSubContrJobGatePass #cboSerialNo').val();
		var subConYear		= $('#frmSubContrJobGatePass #cboSerialYear').val();
		var orderNo			= $('#frmSubContrJobGatePass #cboOrderNo').val();
		var orderYear		= $('#frmSubContrJobGatePass #cboOrderYear').val();
		
		var url 			= "presentation/customerAndOperation/bulk/sub_contractor_job_gatepass/addNew/sub_contractor_job_gate_pass-db.php?requestType=searchSaleseOrder";
					
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"saleseOrderID="+saleseOrderID+"&subConNo="+subConNo+"&subConYear="+subConYear+"&orderNo="+orderNo+"&orderYear="+orderYear,  
			async:false,
			success:function(json){
				$('#frmPopup #tblPopup #tblContent').html(json.gridDetail);				
			}
		});	
	}
	function setCurrentYear()
	{
		var url 			= "presentation/customerAndOperation/bulk/sub_contractor_job_gatepass/addNew/sub_contractor_job_gate_pass-db.php?requestType=getCurrentYear";
					
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			async:false,
			success:function(json){
				$('#frmSubContrJobGatePass #cboOrderYear').val(json.currentYear);
				$('#frmSubContrJobGatePass #cboSerialYear').val(json.currentYear);			}
		});	
	}
<?php
ini_set('display_errors',0);
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];
//die($company);
//include_once "{$backwardseperator}dataAccess/Connector2.php";
include_once "../../../../../dataAccess/DBManager2.php";						$db							= new DBManager2();
include_once "../../../../../class/tables/trn_order_sub_contract_header.php";
include_once "../../../../../class/tables/trn_order_sub_contract_details.php";
include_once "../../../../../class/tables/ware_stocktransactions_fabric.php";
include_once "../../../../../class/tables/trn_order_sub_contract_gate_pass_details.php";
include_once "../../../../../class/tables/trn_order_sub_contract_size_wise_qty.php";
include_once "../../../../../class/tables/trn_orderdetails.php";

$orderNo	    			= $_REQUEST['orderNo'];
$orderYear  				= $_REQUEST['orderYear'];
$subConNo					= $_REQUEST['subConNo'];
$subConYear					= $_REQUEST['subConYear'];
//die($subConNo);
$obj_sub_contract_header	= new trn_order_sub_contract_header($db);
$obj_sub_contract_detail	= new trn_order_sub_contract_details($db);
$obj_transactions_fabric	= new ware_stocktransactions_fabric($db);
$obj_size_wise_qty			= new trn_order_sub_contract_size_wise_qty($db);
$obj_sub_con_gp_details		= new trn_order_sub_contract_gate_pass_details($db);
$obj_trn_orderdetails		= new trn_orderdetails($db);
$db->connect();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Items</title>
</head>

<body>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<form id="frmPopup" name="frmPopup" method="post" action="">
  <table width="500" border="0" align="center" bgcolor="#FFFFFF">
  </table>
  <div align="center">
    <div class="trans_layoutD" style="width:700">
      <div class="trans_text"> Allocation</div>
      <table width="600" border="0" align="center" bgcolor="#FFFFFF">
        
          <td><table width="600" align="center" border="0">
              <tr>
                <td><table width="600" border="0" cellpadding="0" cellspacing="0" class="tableBorder">
                    <tr>
                    <tr id="rw1">
                      <td width="3%" height="22" class="normalfnt">&nbsp;</td>
                      <td width="16%" class="normalfnt">Sales Order No</td>
                      <td width="2%" class="normalfnt">&nbsp;</td>
                      <td width="27%" class="normalfnt"><select name="cboSalesOrderNoP" style="width:120px" id="cboSalesOrderNoP" class="searchP">
                          <option value=""></option>
                          <?php
				  
				  $result = $obj_trn_orderdetails->select('DISTINCT(strSalesOrderNo),intSalesOrderId',NULL,"intOrderNo='$orderNo' AND intOrderYear='$orderYear'",NULL,NULL);
					while($row = mysqli_fetch_array($result))
					{	
					echo "<option value=\"".$row['intSalesOrderId']."\" >".$row['strSalesOrderNo']."</option>";		
					}
				?>
                        </select></td>
                      <td width="26%" class="normalfntLeft">&nbsp;</td>
                      <td><img src="images/smallSearch.png" width="24" height="24" class="mouseover" id="imgSearchItems" /></td>
                    </tr>
                  </table></td>
              </tr>
              <?php //echo $pp
	  ?>
              <tr>
                <td><div style="width:600px;height:300px;overflow:scroll" >
                    <table align="center" class="bordered" id="tblPopup" >
                      <tr class="">
                        <th width="5%" ><input type="checkbox"  name="chkAll" id="chkAll" /></th>
                        <th width="12%" >Sales Order No</th>
                        <th width="12%" >Job Type</th>
                        <th width="9%" >Cut No</th>
                        <th width="11%" >Part</th>
                        <th width="12%" >Combo</th>
                        <th width="11%" >Size</th>
                        <th width="10%">GP QTY</th>
                        <th width="18%">Stock QTY</th>
                      </tr>
                      <tbody id="tblContent">
                      </tbody>
                    </table>
                  </div></td>
              </tr>
              
                <td align="center" class="tableBorder_allRound"><a id="butAdd" name="butAdd"  class="button white medium">Add</a> <a id="butClose1" name="butClose1"  class="button white medium">Close</a></td>
              </tr>
            </table></td>
        </tr>
      </table>
    </div>
  </div>
</form>
</body>
</html>

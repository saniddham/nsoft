<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
ini_set('display_errors',0);
include "libraries/jqgrid2/inc/jqgrid_dist.php";
require_once "class/customerAndOperation/cls_textile_stores.php";
	
$objtexttile		= new cls_texttile($db);
$orderStatusString	= $objtexttile->getOrderSatatusForKPIreport();


$sampNo  			= $_REQUEST['sampNo'];
$sampYear  			= $_REQUEST['sampYear'];
$graphicNo  		= $_REQUEST['graphicNo'];
$fromDate  			= $_REQUEST['fromDate'];
$toDate 			= $_REQUEST['toDate'];
$booFirstTime		= false;
$approveLevel = (int)getMaxApproveLevel();

//BEGIN - {
if(!$booFirstTime)
{	
	$booFirstTime		= true;
	$customer  			= $_REQUEST['customer'];
}
$arr =  json_decode($_REQUEST['filters'],true);
$arr = $arr['rules'];
$where_string = '';
$where_array = array(
				'Status'=>'tb1.intStatus',
				'Dispatch_No'=>'tb1.intBulkDispatchNo',
				'Dispatch_Year'=>'tb1.intBulkDispatchNoYear',
				'strStyleNo'=>'tb2.strStyleNo',
				'Order_No'=>'concat(tb1.intOrderNo,'/',tb1.intOrderYear)',
				'strGraphicNo'=>'tb2.strGraphicNo',
				'strSalesOrderNo'=>'tb2.strSalesOrderNo'
				);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
		$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
{
	//$where_string .= "AND trn_sampleinfomations.dtDate = '".date('Y-m-d')."'";
	//if($customer!='')
		$where_string .= "  AND trn_sampleinfomations.intCustomer = '$customer' ";
	/*if($sampNo!='')
		$where_string .= "  AND trn_sampleinfomations.intSampleNo = '$sampNo' ";
	if($sampYear!='')
		$where_string .= "  AND trn_sampleinfomations.intSampleYear = '$sampYear' ";
	if($graphicNo!='')
		$where_string .= "  AND trn_sampleinfomations.strGraphicRefNo = '$graphicNo' ";
	if($fromDate!='')
		$where_string .= "  AND trn_sampleinfomations.dtDate >= '$fromDate' ";
	if($toDate!='')
		$where_string .= "  AND trn_sampleinfomations.dtDate <= '$toDate' ";*/
}
//END - }

$sql = "select * from(SELECT DISTINCT 
trn_sampleinfomations.dtDate,
IFNULL(mst_brand.strName,'') as brandVSnone, 
mst_customer.strName as customer,
trn_sampleinfomations.intSampleNo,
trn_sampleinfomations.intSampleYear,
trn_sampleinfomations.strGraphicRefNo as strGraphicNo,
concat(trn_sampleinfomations.strGraphicRefNo,'/',trn_sampleinfomations.strStyleNo) as graphicAndStyle, 
trn_sampleinfomations.strStyleNo,
tb1.strSalesOrderNo, 
trn_sampleinfomations.intRevisionNo,
tb2.strComboName as strCombo,
tb2.strPrintName,

IFNULL((SELECT
							Sum(trn_orderdetails.intQty)
							FROM trn_orderdetails 
							INNER Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo 
							AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
							WHERE
							trn_orderdetails.intOrderNo =  tb1.intOrderNo AND
							trn_orderdetails.intOrderYear =  tb1.intOrderYear AND 
							trn_orderdetails.strStyleNo =  tb1.strStyleNo AND 
							trn_orderdetails.strGraphicNo =  tb1.strGraphicNo AND 
							trn_orderdetails.strSalesOrderNo =  tb1.strSalesOrderNo  AND 
							trn_orderheader.intStatus IN ($orderStatusString) 
							),0) as orderQty,

((IFNULL((SELECT
							Sum(ware_fabricreceiveddetails.dblQty)
							FROM
							ware_fabricreceivedheader
							Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
							WHERE
							ware_fabricreceivedheader.intStatus =  '1' AND
							ware_fabricreceivedheader.intOrderNo =   tb1.intOrderNo AND
							ware_fabricreceivedheader.intOrderYear =  tb1.intOrderYear 
							),0))) as Received_Qty, 

IFNULL((SELECT
							Sum(trn_sampleinfomations_details_technical.intNoOfShots)
							FROM trn_sampleinfomations_details_technical
							WHERE
							trn_sampleinfomations_details_technical.intSampleNo =  tb2.intSampleNo AND
							trn_sampleinfomations_details_technical.intSampleYear =  tb2.intSampleYear AND 
							trn_sampleinfomations_details_technical.intRevNo =  tb2.intRevNo AND
							trn_sampleinfomations_details_technical.strPrintName =  tb2.strPrintName AND 
							trn_sampleinfomations_details_technical.strComboName =  tb2.strComboName  
							),0) as shots,

IFNULL((SELECT
							Sum(trn_sampleinfomations_prices.dblRMCost)
							FROM trn_sampleinfomations_prices
							WHERE
							trn_sampleinfomations_prices.intSampleNo =  tb3.intSampleNo AND
							trn_sampleinfomations_prices.intSampleYear =  tb3.intSampleYear AND 
							trn_sampleinfomations_prices.intRevisionNo =  tb3.intRevisionNo AND
							trn_sampleinfomations_prices.strCombo =  tb3.strCombo AND 
							trn_sampleinfomations_prices.strPrintName =  tb3.strPrintName  
							),0) as rmCost,
'' as labourCost, 

IFNULL((SELECT
							Sum(trn_sampleinfomations_prices.dblPrice)
							FROM trn_sampleinfomations_prices
							WHERE
							trn_sampleinfomations_prices.intSampleNo =  tb3.intSampleNo AND
							trn_sampleinfomations_prices.intSampleYear =  tb3.intSampleYear AND 
							trn_sampleinfomations_prices.intRevisionNo =  tb3.intRevisionNo AND
							trn_sampleinfomations_prices.strCombo =  tb3.strCombo AND 
							trn_sampleinfomations_prices.strPrintName =  tb3.strPrintName  
							),0) as quotedPrice,

'' as margin, 
'' as targetPrice , 
'' as status, 
'' as comment, 
'' as rmAndLabourCost,

concat(mst_customer.strName,'/',trn_sampleinfomations.intSampleNo,'/',trn_sampleinfomations.intSampleYear,'/',trn_sampleinfomations.strGraphicRefNo,'/',trn_sampleinfomations.strStyleNo,'/',tb1.strSalesOrderNo) as orderBy 

FROM
trn_sampleinfomations 

INNER Join trn_sampleinfomations_details_technical as tb2 ON trn_sampleinfomations.intSampleNo = tb2.intSampleNo AND trn_sampleinfomations.intSampleYear = tb2.intSampleYear AND trn_sampleinfomations.intRevisionNo = tb2.intRevNo

LEFT Join trn_sampleinfomations_prices as tb3 ON tb2.intSampleNo = tb3.intSampleNo AND tb2.intSampleYear = tb3.intSampleYear AND tb2.intRevNo = tb3.intRevisionNo AND tb2.strPrintName = tb3.strPrintName 
AND tb2.strComboName = tb3.strCombo 

LEFT Join trn_orderdetails as tb1 ON tb1.intSampleNo = trn_sampleinfomations.intSampleNo AND tb1.intSampleYear = trn_sampleinfomations.intSampleYear AND tb1.intRevisionNo = trn_sampleinfomations.intRevisionNo AND tb1.strGraphicNo = trn_sampleinfomations.strGraphicRefNo AND tb1.strStyleNo = trn_sampleinfomations.strStyleNo  AND tb1.strPrintName = tb2.strPrintName AND tb1.strCombo = tb2.strComboName 

LEFT Join trn_orderheader ON tb1.intOrderNo = trn_orderheader.intOrderNo AND tb1.intOrderYear = trn_orderheader.intOrderYear

LEFT Join mst_customer ON trn_sampleinfomations.intCustomer = mst_customer.intId 


LEFT Join mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
WHERE  

1 = 1
$where_string
limit 20)  as t where 1=1
						";
					    // echo $sql;
$col = array();

//Order No
$col["title"] = "Date of Enquiry"; // caption of column
$col["name"] = "dtDate"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";

//$reportLink  = "../../placeOrder/listing/rptBulkOrder.php?orderNo={intOrderNo}&orderYear={intOrderYear}";
//$col['link']	= $reportLink;	 
//$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag


$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Brand(VS/Non VS)
"; // caption of column
$col["name"] = "brandVSnone"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Customer"; // caption of column
$col["name"] = "customer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Sample No"; // caption of column
$col["name"] = "intSampleNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Sample Year"; // caption of column
$col["name"] = "intSampleYear"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Graphic/Style"; // caption of column
$col["name"] = "graphicAndStyle"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Sales Order No"; // caption of column
$col["name"] = "strSalesOrderNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;


//Order No
$col["title"] = "Revision No"; // caption of column
$col["name"] = "intRevisionNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Combo"; // caption of column
$col["name"] = "strCombo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Print Name"; // caption of column
$col["name"] = "strPrintName"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;


//Order No
$col["title"] = "Order Qty"; // caption of column
$col["name"] = "orderQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Recv Qty"; // caption of column
$col["name"] = "Received_Qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;


//Order No
$col["title"] = "No Of Shots"; // caption of column
$col["name"] = "shots"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "RM Cost"; // caption of column
$col["name"] = "rmCost"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Labor Cost"; // caption of column
$col["name"] = "labourCost"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Quoted Price"; // caption of column
$col["name"] = "quotedPrice"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Margin %"; // caption of column
$col["name"] = "margin"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Target Price"; // caption of column
$col["name"] = "targetPrice"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Status"; // caption of column
$col["name"] = "status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Comment"; // caption of column
$col["name"] = "comment"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;



$jq = new jqgrid('',$db);

$grid["export"] = array("format"=>"excel", "filename"=>"my-file", "sheetname"=>"KPI Costing");
$grid["caption"] 		= "KPI Costing Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= ''; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);




$out = $jq->render("list1");
?>
<head>
<?php include 'include/javascript.html'?>
<?php include 'include/listing.html'?>
</head>
<body>
<form id="frmListing" name="frmLising">
<div align="center" style="margin:10px"><?php echo $out?></div>
</form>
</body>
<?php
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(ware_fabricreceivedheader.intApproveLevels) AS appLevel
			FROM ware_fabricreceivedheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
?>


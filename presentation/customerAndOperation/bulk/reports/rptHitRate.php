<?php
 	session_start();

//ini_set('display_errors',1);
include "../../../../dataAccess/DBManager2.php";
include "../../../../class/cls_dateTime.php";

$db				=  new DBManager2(1);
$cls_dateTime	=  new cls_dateTime($db);
$date			=  $cls_dateTime->getCurruntDate();

		$db->connect();
  	
	$type = $_REQUEST['type'];
 	
 		$sampleYear = $_REQUEST['cboSampleYear'];
		$sampleNo = $_REQUEST['cboSampleNo'];
		$graphic = $_REQUEST['cboGraphic'];
		$sampleDate = $_REQUEST['dtSampleDate'];
		$customer = $_REQUEST['cboCustomer'];
		$marketer = $_REQUEST['cboMarketer'];
 		

		$sql= getHitRatequery($sampleYear,$sampleNo,$sampleDate,$graphic,$customer,$marketer);
		//$result = $db->RunQuery($sql);
		$result	= $db->RunQuery($sql);
		$fileName = "Sample Hit Rate Report ".$date.".xls";
  				header("Content-type: application/vnd.ms-excel");
				header("Content-Disposition: attachment; filename=\"".$fileName."\"" );
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
				header("Pragma: public");
				?>
				
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td bgcolor="#99CCFF">Sample Year</td>
					<td bgcolor="#99CCFF">Sample No</td>
					<td bgcolor="#99CCFF">Sample Date</td>
					<td bgcolor="#99CCFF">Graphic</td>
					<td bgcolor="#99CCFF">Customer</td>
					<td bgcolor="#99CCFF">Marketer</td>
					<td bgcolor="#99CCFF">Sample Dispatch Qty</td>
					<td bgcolor="#99CCFF">Order Qty</td>
				  </tr>
				<?php
				
				foreach($result as $row)
				//while($row = mysqli_fetch_array($result))
				{
				?>
				  <tr>
					<td align="left"><?php echo $row['SAMPLE_YEAR']; ?></td>
					<td align="left"><?php echo $row['SAMPLE_NO']; ?></td>
					<td align="center" nowrap="nowrap"><?php echo $row['SAMPLE_DATE']; ?></td>
					<td align="left" nowrap="nowrap"><?php echo $row['GRAPHIC']; ?></td>
					<td align="left" nowrap="nowrap"><?php echo $row['CUSTOMER']; ?></td>
					<td align="left" nowrap="nowrap"><?php echo $row['MARKETER']; ?></td>
					<td align="right"><?php echo $row['DISPATCH_QTY']; ?></td>
					<td align="right"><?php echo $row['ORDER_QTY']; ?></td>
				  </tr>
				<?php
				}
			return;
 		
   	
 function getHitRatequery($sampleYear,$sampleNo,$sampleDate,$graphic,$customer,$marketer){
	 
	 $sql	="
			SELECT  
			SUB_SAMPLE_AND_ORDER.SAMPLE_YEAR,
			SUB_SAMPLE_AND_ORDER.SAMPLE_NO,
			(SELECT MAX(trn_sampleinfomations.dtDate) FROM trn_sampleinfomations  WHERE SUB_SAMPLE_AND_ORDER.SAMPLE_YEAR=trn_sampleinfomations.intSampleYear
			 AND SUB_SAMPLE_AND_ORDER.SAMPLE_NO=trn_sampleinfomations.intSampleNo ) AS SAMPLE_DATE,
			   SUB_SAMPLE_AND_ORDER.GRAPHIC ,
			SUB_SAMPLE_AND_ORDER.CUSTOMER,
			SUB_SAMPLE_AND_ORDER.MARKETER,
			SUM(IFNULL(SUB_SAMPLE_AND_ORDER.DISPATCH_QTY,0)) AS DISPATCH_QTY,
			SUM(IFNULL(SUB_SAMPLE_AND_ORDER.ORDER_QTY,0)) AS ORDER_QTY   
			
			FROM   ( 
			
			SELECT  SUB_SAMPLE.* ,
			SUM(IFNULL(ORDER_D.intQty,0)) AS ORDER_QTY 
			 FROM  (
			
			SELECT
			TSI.intSampleYear AS SAMPLE_YEAR,
			TSI.intSampleNo AS SAMPLE_NO,
			TSI.intRevisionNo AS REVISION,
			TSI.strGraphicRefNo AS GRAPHIC,
			C.strName AS CUSTOMER,
			U.strFullName AS MARKETER,
			SUM(IFNULL(TSD.dblDispatchQty,0)) AS DISPATCH_QTY,
			TSI.intCustomer AS CUSTOMER_ID,
			TSI.intMarketer AS MARKETER_ID 
			FROM
			trn_sampleinfomations AS TSI
			LEFT JOIN trn_sampledispatchdetails AS TSD ON TSI.intSampleNo = TSD.intSampleNo AND TSI.intSampleYear = TSD.intSampleYear AND TSI.intRevisionNo = TSD.intRevisionNo
			INNER JOIN mst_customer AS C ON TSI.intCustomer = C.intId
			INNER JOIN sys_users AS U ON TSI.intMarketer = U.intUserId 
			 WHERE TSI.intStatus=1 	 
	 ";
	 if($sampleNo!='')
	 $sql	.=" AND TSI.intSampleNo = '$sampleNo'  ";
	 if($sampleYear!='')
	 $sql	.=" AND TSI.intSampleYear = '$sampleYear'  ";
	 if($sampleDate!='')
	 $sql	.=" AND TSI.dtDate = '$sampleDate'  ";
	 if($graphic!='')
	 $sql	.=" AND TSI.strGraphicRefNo = '$graphic'  ";
	 if($customer!='')
	 $sql	.=" AND TSI.intCustomer = '$customer'  ";
	 if($marketer!='')
	 $sql	.=" AND TSI.intMarketer = '$marketer'  ";
 	
	 $sql	.=" 
				GROUP BY
				TSI.intSampleNo,
				TSI.intSampleYear,
				TSI.intRevisionNo 
				
				) AS SUB_SAMPLE 
				LEFT JOIN 
				(
					SELECT TOD.* FROM 
					trn_orderdetails AS TOD
					INNER JOIN trn_orderheader AS TOH 
					ON TOD.intOrderNo = TOH.intOrderNo 
					AND TOD.intOrderYear = TOH.intOrderYear 
					WHERE TOH.intStatus <= TOH.intApproveLevelStart 
					AND TOH.intStatus>=0 
				) AS ORDER_D
				 
				ON SUB_SAMPLE.SAMPLE_NO = ORDER_D.intSampleNo 
				AND SUB_SAMPLE.SAMPLE_YEAR = ORDER_D.intSampleYear 
				AND SUB_SAMPLE.REVISION = ORDER_D.intRevisionNo
				GROUP BY
				SUB_SAMPLE.SAMPLE_NO,
				SUB_SAMPLE.SAMPLE_YEAR,
				SUB_SAMPLE.REVISION  ) AS SUB_SAMPLE_AND_ORDER
				
				GROUP BY
				SUB_SAMPLE_AND_ORDER.SAMPLE_NO,
				SUB_SAMPLE_AND_ORDER.SAMPLE_YEAR, 
				SUB_SAMPLE_AND_ORDER.GRAPHIC,
				SUB_SAMPLE_AND_ORDER.CUSTOMER_ID,
				SUB_SAMPLE_AND_ORDER.MARKETER_ID 
				ORDER BY 
				SUB_SAMPLE_AND_ORDER.CUSTOMER ASC,
				SUB_SAMPLE_AND_ORDER.SAMPLE_YEAR ASC,
				SUB_SAMPLE_AND_ORDER.SAMPLE_NO ASC 	 ";
	
	 
	return $sql; 
 }
 	$db->disconnect();

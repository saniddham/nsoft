<?php (define('UNLOCKPAGE', true) ? die('<<< Access denied >>>') : ''); ?>
<?php
$location = $sessions->getLocationId();
$company = $sessions->getCompanyId();
$intUser = $sessions->getUserId();

include_once "class/cls_commonErrorHandeling_get.php";

$obj_common_err = new cls_commonErrorHandeling_get($db);

$year = (!isset($_SESSION["Year"]) ? '' : $_SESSION["Year"]);
$month = (!isset($_SESSION["Month"]) ? '' : $_SESSION["Month"]);
?>
<title>Warehouse - Reports</title>
<!--<form id="frmPayrollReport" name="frmPayrollReport" autocomplete="off">
--><div align="center">
    <div class="trans_layoutL" style="width:700px">
        <div class="trans_text">Bulk Reports</div>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            
            
            <!--BEGIN - PO Summery-->
            <tr>
                <td><table align="left" width="600" class="main_header_table" id="tblFabricMovementHead" cellpadding="2" cellspacing="0">
                        <tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Fabric Movement Report.</u></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td><form name="frmFabricMovement" id="frmFabricMovement" autocomplete="off">
                        <table width="599" align="left" border="0" id="tblFabricMovementBody" class="main_table" cellspacing="0" style="display:none;">
                            <thead>
                                <tr>
                                    <td colspan="5" style="text-align:left">Fabric Movement Selection Criteria<img src="images/close_small.png" align="right" class="mouseover"/></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="73" class="normalfnt">Order No<span class="compulsoryRed"> *</span></td>
                                    <td><input type="text" style="width:100px" name="cboOrderNo" id="cboOrderNo" class="search1 validate[required]" value=""></td>
                                    <td>Year</td>
                                    <td><input type="text"  style="width:70px" name="cboOrderYear" id="cboOrderYear" class="search1 validate[required]" value="" /></td>
                                    <td><img src="images/smallSearch.png" width="24" height="24" alt="search sizes and cut nos" id="butSearch" class="mouseover" /></td>
                                </tr>
                                <tr>
                                    <td class="normalfnt">Size</td>
                                    <td width="106" ><select style="width:100px" name="cboSize" id="cboSize">
                                            <option value=""></option>
                                            <?php
                                            $sql = "SELECT DISTINCT
                trn_ordersizeqty.strSize
                FROM trn_ordersizeqty
                WHERE
                trn_ordersizeqty.intOrderNo =  '$orderNo' AND
                trn_ordersizeqty.intOrderYear =  '$orderYear'
                ORDER BY
                trn_ordersizeqty.strSize ASC
                            ";
                                            $result = $db->RunQuery($sql);
                                            while ($row = mysqli_fetch_array($result)) {
                                                if ($sizeVal == $row['strSize'])
                                                    echo "<option selected value=\"" . $row['strSize'] . "\">" . $row['strSize'] . "</option>";
                                                else
                                                    echo "<option value=\"" . $row['strSize'] . "\">" . $row['strSize'] . "</option>";
                                            }
                                            ?>
                                        </select></td>
                                    <td width="32" >&nbsp;</td>
                                    <td width="73" >&nbsp;</td>
                                    <td width="305" >&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="normalfnt">Cut No</td>
                                    <td ><select style="width:100px" name="cboCutNo" id="cboCutNo">
                                            <option value=""></option>
                                            <?php
                                            $sql = "SELECT DISTINCT
                ware_fabricreceiveddetails.strCutNo
                FROM
                ware_fabricreceivedheader
                Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
                WHERE
                ware_fabricreceivedheader.intOrderNo =  '$orderNo' AND
                ware_fabricreceivedheader.intOrderYear =  '$orderYear' 
                Order by ware_fabricreceiveddetails.strCutNo ASC
                            ";
                                            $result = $db->RunQuery($sql);
                                            while ($row = mysqli_fetch_array($result)) {
                                                if ($cutNum == $row['strCutNo'])
                                                    echo "<option selected value=\"" . $row['strCutNo'] . "\">" . $row['strCutNo'] . "</option>";
                                                else
                                                    echo "<option value=\"" . $row['strCutNo'] . "\">" . $row['strCutNo'] . "</option>";
                                            }
                                            ?>
                                        </select></td>
                                    <td >&nbsp;</td>
                                    <td >&nbsp;</td>
                                    <td >&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="5" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                            <tr>
                                                <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </tbody>
                        </table></form></td>
            </tr>
            <!--END - PO Summery--> 
            <!--BEGIN - PO-GRN Summery-->
            <tr>
                <td><table align="left" width="600" class="main_header_table" id="tblOrderWiseFabInOutHead" cellpadding="2" cellspacing="0">
                        <tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Order Wise Fabric IN - OUT.</u></td>
                        </tr>
                    </table></td>
            </tr>
            <!--END - PO-GRN Summery--> 
            <!--BEGIN - PO-GRN Summery-->
            <tr>
                <td><form name="frmOrderWiseFabInOut" id="frmOrderWiseFabInOut" autocomplete="off">
                        <table width="700" align="left" border="0" id="tblOrderWiseFabInOutBody" class="main_table" cellspacing="0"  style="display:none;">
                            <thead>
                                <tr>
                                    <td colspan="4" style="text-align:left">Order Wise Fabric IN - OUT Selection Criteria<img src="images/close_small.png" align="right" class="mouseover"/></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="26%" class="normalfnt">Year<span class="compulsoryRed"> *</span></td>
                                    <td width="72%"><select name="cboOrderYear" id="cboOrderYear" style="width:70px"  class="validate[required] txtText search" >
                                            <option value=""></option>
                                            <?php
                                            $sql = "SELECT DISTINCT
							trn_orderheader.intOrderYear
							FROM trn_orderheader
							ORDER BY
							trn_orderheader.intOrderYear DESC";
                                            $result = $db->RunQuery($sql);
                                            $i = 0;
                                            while ($row = mysqli_fetch_array($result)) {
                                                if (($i == 0) && ($x_year == ''))
                                                    $year = $row['intOrderYear'];

                                                //if($row['intOrderYear']==$x_year)
                                                //echo "<option value=\"".$row['intOrderYear']."\" selected=\"selected\">".$row['intOrderYear']."</option>";	
                                                //else
                                                echo "<option value=\"" . $row['intOrderYear'] . "\">" . $row['intOrderYear'] . "</option>";
                                                $i++;
                                            }
                                            ?>
                                        </select></td>
                                    <td width="26%">&nbsp;</td>
                                    <td width="10%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="normalfnt">Style No</td>
                                    <td width="72%"><select name="cboStyle" id="cboStyle" style="width:180px"  class="txtText search" >
                                            <option value=""></option>
                                        </select></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="normalfnt">Graphic No</td>
                                    <td width="72%"><select name="cboGraphicNo" style="width:180px" id="cboGraphicNo" class="txtText search" >
                                            <option value=""></option>
                                        </select></td>
                                    <td></td>
                                    <td></td>
                                </tr>          <tr>
                                    <td class="normalfnt">Costomer PO</td>
                                    <td width="72%"><select name="cboPONo" id="cboPONo" style="width:180px"  class="txtText search" >
                                            <option value=""></option>
                                        </select></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="normalfnt">Customer</td>
                                    <td><select name="cboCustomer" id="cboCustomer" style="width:180px"  class="txtText" >
                                            <option value=""></option>
                                        </select></td>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="normalfnt">Order No<span class="compulsoryRed"> *</span></td>
                                    <td colspan="1"><input name="cboOrderNo" id="cboOrderNo" style="width:90px"  class="validate[required] txtText" /></td>
                                    <td width="22"><input type="checkbox" name="hsStcker"  id="hsStcker" value="0" > View Intermediate Sales Orders</td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="normalfntRight"><table width="700" align="left" border="0" id="" class="main_table" cellspacing="0" <?php /* ?>style="display:none;"<?php */ ?>>
                                            <tr>
                                                <td width="180" align="right">Color Wise</td>
                                                <td width="22"><input name="radio" type="radio" class="mouseover" id="chkColor" value="1" checked="checked" /></td>
                                                <td width="61" align="right">Cut wise</td>
                                                <td width="22"><input name="radio" type="radio" class="mouseover" id="chkCutNo"/></td>
                                                <td width="80" align="right">Size wise</td>
                                                <td width="22"><input name="radio" type="radio" class="mouseover" id="chkSize"/></td>
                                                <td width="62" align="right">Part wise</td>
                                                <td width="22"><input name="radio" type="radio" class="mouseover" id="chkPart"/></td>
                                                <td width="123" align="right">Sales Order No wise</td>
                                                <td width="22"><input name="radio" type="radio" class="mouseover" id="chkSalesOrder"/></td>
                                                <td width="123" align="right">Sales Order No-Size wise</td>
                                                <td width="22"><input name="radio" type="radio" class="mouseover" id="chkSalesOrderSize"/></td>
                                            </tr>
                                        </table></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                            <tr>
                                                <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </tbody>
                        </table></form></td>
            </tr>
            <!--END - PO-GRN Summery--> 
            <!--BEGIN - PO-Item Summery-->
            <tr>
                <td><table align="left" width="600" class="main_header_table" id="tblCustomerWiseFabInOutHead" cellpadding="2" cellspacing="0">
                        <tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Customer Wise Fabric IN - OUT.</u></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td><form name="frmCustomerWiseFabInOut" id="frmCustomerWiseFabInOut" autocomplete="off">
                        <table width="700" align="left" border="0" id="tblCustomerWiseFabInOutBody" class="main_table" cellspacing="0" style="display:none;">
                            <thead>
                                <tr>
                                    <td colspan="4" style="text-align:left">Customer Wise Fabric IN - OUT Selection Criteria<img src="images/close_small.png" align="right" class="mouseover"/></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="18%" class="normalfnt">Customer</td>
                                    <td width="62%"><select name="cboCustomer" id="cboCustomer" style="width:180px"  class="validate[required] txtText" >
                                            <option value=""></option>
                                            <?php
                                            $sql = "SELECT DISTINCT 
							trn_orderheader.intCustomer,
							mst_customer.strName
							FROM
							trn_orderheader
							Inner Join mst_customer ON trn_orderheader.intCustomer = mst_customer.intId 
							ORDER BY mst_customer.strName ASC";
                                            $pp = $sql;
                                            $result = $db->RunQuery($sql);
                                            while ($row = mysqli_fetch_array($result)) {
                                                if ($row['intCustomer'] == $x_custId)
                                                    echo "<option value=\"" . $row['intCustomer'] . "\" selected=\"selected\">" . $row['strName'] . "</option>";
                                                else
                                                    echo "<option value=\"" . $row['intCustomer'] . "\">" . $row['strName'] . "</option>";
                                            }
                                            ?>
                                        </select></td>
                                    <td width="4%">&nbsp;</td>
                                    <td width="16%"></td>
                                </tr>

                                <tr>
                                    <td class="normalfnt">Date</td>
                                    <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="4%" class="normalfnt"><input name="chkDate" type="checkbox" id="chkDate"  /></td>
                                                <td width="6%" class="normalfnt">From</td>
                                                <td width="19%"><input name="dtDate" type="text" disabled="disabled"  class="txtbox" id="txtFromDate" style="width:80px;"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                                <td width="4%" class="normalfnt">To</td>
                                                <td width="67%"><input name="dtDate2" type="text" disabled="disabled"  class="txtbox" id="txtToDate" style="width:80px;"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                            </tr>    </table></td>
                                </tr>		          <tr>
                                    <td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                            <tr>
                                                <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </tbody>
                        </table></form></td>
            </tr>


            <!--BEGIN - cust Delivery Report-->
            <tr>
                <td><table align="left" width="600" class="main_header_table" id="tblCustomerDispatchHead" cellpadding="2" cellspacing="0">
                        <tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Fabric Delivery Report.</u></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td><form name="frmCustomerDispatch" id="frmCustomerDispatch" autocomplete="off">
                        <table width="700" align="left" border="0" id="tblCustomerDispatchBody" class="main_table" cellspacing="0" style="display:none;">
                            <thead>
                                <tr>
                                    <td colspan="4" style="text-align:left">Fabric Delivery Criteria<img src="images/close_small.png" align="right" class="mouseover"/></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="10%" class="normalfnt">Location</td>
                                    <td width="69%"><select name="cboLocation" id="cboLocation" style="width:180px"  class="validate[required] txtText" >
                                            <option value=""></option>
                                            <?php
                                            $sql = "SELECT
							mst_locations.intId,
							mst_locations.strName
							FROM `mst_locations`
							WHERE
							mst_locations.intStatus = 1
							ORDER BY
							mst_locations.strName ASC";
                                            $pp = $sql;
                                            $result = $db->RunQuery($sql);
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo "<option value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                            }
                                            ?>
                                        </select></td>
                                    <td width="3%">&nbsp;</td>
                                    <td width="18%"></td>
                                </tr>

                                <tr>
                                    <td colspan="4" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="9%" class="normalfnt" valign="top">Date</td>
                                                <td width="3%" class="normalfnt" valign="top"><input name="chkDate" type="checkbox" id="chkDate" class="validate[required] checkbox" /></td>
                                                <td width="5%" class="normalfnt" valign="top">From</td>
                                                <td width="12%" valign="top"><input name="dtDate11" type="text" disabled="disabled"  class="txtbox" id="txtFromDate11" style="width:80px;"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                                <td width="1%" class="normalfnt" valign="top">:</td>
                                                <td width="3%" class="normalfnt" valign="top"><input type="text" disabled="disabled" name="txtFH" id="txtFH"  style="width:15px;" max="24" />HH</td>
                                                <td width="1%" class="normalfnt" valign="top">:</td>
                                                <td width="3%" class="normalfnt" valign="top"><input type="text" disabled="disabled" name="txtFM" id="txtFM"  style="width:15px;" max="59" />MM</td>
                                                <td width="3%" class="normalfnt" valign="top">&nbsp;</td>
                                                <td width="3%" class="normalfnt" valign="top">To</td>
                                                <td width="12%" valign="top"><input name="dtDate11" type="text" disabled="disabled"  class="txtbox" id="txtToDate11" style="width:80px;"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                                <td width="1%" class="normalfnt" valign="top">:</td>
                                                <td width="3%" class="normalfnt" valign="top"><input type="text" disabled="disabled" name="txtTH" id="txtTH"  style="width:15px;" max="24" />HH</td>
                                                <td width="1%" class="normalfnt" valign="top">:</td>
                                                <td width="3%" class="normalfnt" valign="top"><input type="text" disabled="disabled" name="txtTM" id="txtTM"  style="width:15px;" max="59" />MM</td>
                                                <td width="49%" class="normalfnt" valign="top">&nbsp;</td>
                                            </tr>    </table></td>
                                </tr>		          <tr>
                                    <td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                            <tr>
                                                <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </tbody>
                        </table></form></td>
            </tr>
            <!--END - cust Delivery Report-->

            <?php
            $kpiPermision = $obj_common_err->Load_special_menupermision(34, $intUser, 'RunQuery');
            if ($kpiPermision == 1) {
                ?>  
                <!--BEGIN - KPI ery-->
                <tr  style="display:none">
                    <td><table align="left" width="600" class="main_header_table" id="tblKPIHead" cellpadding="2" cellspacing="0">
                            <tr>
                                <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u><span style="text-align:left">KPIs for Costing</span>.</u></td>
                            </tr>
                        </table></td>
                </tr>
                <tr style="display:none">
                    <td><form name="frmKPI" id="frmKPI" autocomplete="off">
                            <table width="700" align="left" border="0" id="tblKPIBody" class="main_table" cellspacing="0" style="display:none;">
                                <thead>
                                    <tr>
                                        <td colspan="4" style="text-align:left">KPIs for Costing Selection Criteria<img src="images/close_small.png" align="right" class="mouseover"/></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td width="18%" class="normalfnt">Customer</td>
                                        <td width="45%"><select name="cboCustomer" id="cboCustomer" style="width:180px"  class="txtText" >
                                                <option value=""></option>
                                                <?php
                                                $sql = "SELECT DISTINCT 
							trn_orderheader.intCustomer,
							mst_customer.strName
							FROM
							trn_orderheader
							Inner Join mst_customer ON trn_orderheader.intCustomer = mst_customer.intId 
							ORDER BY mst_customer.strName ASC";
                                                $pp = $sql;
                                                $result = $db->RunQuery($sql);
                                                while ($row = mysqli_fetch_array($result)) {
                                                    if ($row['intCustomer'] == $x_custId)
                                                        echo "<option value=\"" . $row['intCustomer'] . "\" selected=\"selected\">" . $row['strName'] . "</option>";
                                                    else
                                                        echo "<option value=\"" . $row['intCustomer'] . "\">" . $row['strName'] . "</option>";
                                                }
                                                ?>
                                            </select></td>
                                        <td width="8%">&nbsp;</td>
                                        <td width="29%"></td>
                                    </tr>
                                    <tr>
                                        <td width="18%" class="normalfnt">Sample No</td>
                                        <td width="45%"><select name="cboSampYear" id="cboSampYear" style="width:60px"  class="txtText" >
                                                <option value=""></option>
                                                <?php
                                                $sql = "SELECT DISTINCT
							trn_sampleinfomations.intSampleYear
							FROM trn_sampleinfomations
							ORDER BY
							trn_sampleinfomations.intSampleNo DESC
							";
                                                $pp = $sql;
                                                $result = $db->RunQuery($sql);
                                                while ($row = mysqli_fetch_array($result)) {
                                                    if ($row['intSampleYear'] == $x_custId)
                                                        echo "<option value=\"" . $row['intSampleYear'] . "\" selected=\"selected\">" . $row['intSampleYear'] . "</option>";
                                                    else
                                                        echo "<option value=\"" . $row['intSampleYear'] . "\">" . $row['intSampleYear'] . "</option>";
                                                }
                                                ?>
                                            </select><select name="cboSampNo" id="cboSampNo" style="width:120px"  class="txtText" >
                                                <option value=""></option>
                                                <?php
                                                $sql = "/*SELECT DISTINCT
							trn_sampleinfomations.intSampleNo
							FROM trn_sampleinfomations
							ORDER BY
							trn_sampleinfomations.intSampleNo DESC*/
							";
                                                $pp = $sql;
                                                $result = $db->RunQuery($sql);
                                                while ($row = mysqli_fetch_array($result)) {
                                                    if ($row['intSampleNo'] == $x_custId)
                                                        echo "<option value=\"" . $row['intSampleNo'] . "\" selected=\"selected\">" . $row['intSampleNo'] . "</option>";
                                                    else
                                                        echo "<option value=\"" . $row['intSampleNo'] . "\">" . $row['intSampleNo'] . "</option>";
                                                }
                                                ?>
                                            </select></td>
                                        <td width="8%">Graphic</td>
                                        <td width="29%"><select name="cboGraphicNo" id="cboGraphicNo" style="width:180px"  class="txtText" >
                                                <option value=""></option>
                                                <?php
                                                $sql = "/*SELECT DISTINCT
							trn_sampleinfomations.strGraphicRefNo
							FROM trn_sampleinfomations
							ORDER BY
							trn_sampleinfomations.strGraphicRefNo ASC*/";
                                                $pp = $sql;
                                                $result = $db->RunQuery($sql);
                                                while ($row = mysqli_fetch_array($result)) {
                                                    if ($row['strGraphicRefNo'] == $x_custId)
                                                        echo "<option value=\"" . $row['strGraphicRefNo'] . "\" selected=\"selected\">" . $row['strGraphicRefNo'] . "</option>";
                                                    else
                                                        echo "<option value=\"" . $row['strGraphicRefNo'] . "\">" . $row['strGraphicRefNo'] . "</option>";
                                                }
                                                ?>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="normalfnt">Date</td>
                                        <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="4%" class="normalfnt"><input name="chkDate" type="checkbox" id="chkDate"  /></td>
                                                    <td width="6%" class="normalfnt">From</td>
                                                    <td width="19%"><input name="dtDate10" type="text" disabled="disabled"  class="txtbox" id="txtFromDate10" style="width:80px;"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                                    <td width="4%" class="normalfnt">To</td>
                                                    <td width="67%"><input name="dtDate10" type="text" disabled="disabled"  class="txtbox" id="txtToDate10" style="width:80px;"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                                </tr>    </table></td>
                                    </tr>		          <tr>
                                        <td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                                <tr>
                                                    <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                                                </tr>
                                            </table></td>
                                    </tr>
                                </tbody>
                            </table></form></td>
                </tr>
                <?php
            }
            ?>  


            <!--BEGIN - PO-Item Summery-->
            <tr style="display:none">
                <td><table align="left" width="600" class="main_header_table" id="tblFabricCheckStatusHead" cellpadding="2" cellspacing="0">
                        <tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Fabric Check Status.</u></td>
                        </tr>
                    </table></td>
            </tr>


            <tr style="display:none">
                <td><form name="frmFabricCheckStatus" id="frmFabricCheckStatus" autocomplete="off">
                        <table width="700" align="left" border="0" id="tblFabricCheckStatusBody" class="main_table" cellspacing="0" style="display:none;">
                            <thead>
                                <tr>
                                    <td style="text-align:left">Fabric Check Status Selection Criteria<img src="images/close_small.png" align="right" class="mouseover"/></td>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <tr>
                                <td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                        <tr>		                <td width="100%" align="center" bgcolor=""><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24"/></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            </tbody>
                        </table></form></td>
            </tr>



            <tr>
                <td style="display: none"><table align="left" width="600" class="main_header_table" id="tblOrderWiseFabInOutHead2" cellpadding="2" cellspacing="0">
                        <tr>
                            <td width="544"><img src="images/report_go.png" alt="" class="mouseover" />&nbsp;<u>Dispatch Wise Invoice Summary</u></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td><form name="frmOrderWiseFabInOut2" id="frmOrderWiseFabInOut2" autocomplete="off">
                        <table width="700" align="left" border="0" id="tblOrderWiseFabInOutBody2" class="main_table" cellspacing="0" style="display:none;">
                            <thead>
                                <tr>
                                    <td colspan="4" style="text-align:left">Order Wise Fabric IN - OUT Selection Criteria<img src="images/close_small.png" alt="" align="right" class="mouseover"/></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="26%" class="normalfnt">Year<span class="compulsoryRed"> *</span></td>
                                    <td width="72%"><select name="cboOrderYear2" id="cboOrderYear2" style="width:70px"  class="validate[required] txtText search" >
                                            <?php
                                            $sql = "SELECT DISTINCT
							trn_orderheader.intOrderYear
							FROM trn_orderheader
							ORDER BY
							trn_orderheader.intOrderYear DESC";
                                            $result = $db->RunQuery($sql);
                                            $i = 0;
                                            while ($row = mysqli_fetch_array($result)) {
                                                if (($i == 0) && ($x_year == ''))
                                                    $year = $row['intOrderYear'];

                                                if ($row['intOrderYear'] == $x_year)
                                                    echo "<option value=\"" . $row['intOrderYear'] . "\" selected=\"selected\">" . $row['intOrderYear'] . "</option>";
                                                else
                                                    echo "<option value=\"" . $row['intOrderYear'] . "\">" . $row['intOrderYear'] . "</option>";
                                                $i++;
                                            }
                                            ?>
                                        </select></td>
                                    <td width="26%">&nbsp;</td>
                                    <td width="10%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="normalfnt">Style No</td>
                                    <td width="72%"><select name="cboStyle2" id="cboStyle2" style="width:180px"  class="txtText search" >
                                            <option value=""></option>
                                            <?php
                                            $sql = "SELECT DISTINCT
							trn_orderdetails.strStyleNo
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
							WHERE
							trn_orderheader.intStatus =  '1' 
							ORDER BY trn_orderdetails.strStyleNo ASC";
                                            $result = $db->RunQuery($sql);
                                            while ($row = mysqli_fetch_array($result)) {
                                                if ($row['strStyleNo'] == $x_styleNo)
                                                    echo "<option value=\"" . $row['strStyleNo'] . "\" selected=\"selected\">" . $row['strStyleNo'] . "</option>";
                                                else
                                                    echo "<option value=\"" . $row['strStyleNo'] . "\">" . $row['strStyleNo'] . "</option>";
                                            }
                                            ?>
                                        </select></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="normalfnt">Graphic No</td>
                                    <td width="72%"><select name="cboGraphicNo2" style="width:180px" id="cboGraphicNo2" class="txtText search" >
                                            <option value=""></option>
                                            <?php
                                            $sql = "SELECT DISTINCT
							trn_orderdetails.strGraphicNo
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
							WHERE
							trn_orderheader.intStatus =  '1' 
							ORDER BY trn_orderdetails.strGraphicNo ASC";
                                            $result = $db->RunQuery($sql);
                                            while ($row = mysqli_fetch_array($result)) {
                                                if ($row['strGraphicNo'] == $x_graphicNo)
                                                    echo "<option value=\"" . $row['strGraphicNo'] . "\" selected=\"selected\">" . $row['strGraphicNo'] . "</option>";
                                                else
                                                    echo "<option value=\"" . $row['strGraphicNo'] . "\">" . $row['strGraphicNo'] . "</option>";
                                            }
                                            ?>
                                        </select></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="normalfnt">Costomer PO</td>
                                    <td width="72%"><select name="cboPONo2" id="cboPONo2" style="width:180px"  class="txtText search" >
                                            <option value=""></option>
                                            <?php
                                            $sql = "SELECT DISTINCT
							trn_orderheader.strCustomerPoNo
							FROM trn_orderheader 
							where intStatus=1 
							AND trn_orderheader.strCustomerPoNo <> '' 
							ORDER BY
							trn_orderheader.strCustomerPoNo ASC";
                                            $result = $db->RunQuery($sql);
                                            while ($row = mysqli_fetch_array($result)) {
                                                if ($row['strCustomerPoNo'] == $x_poNo)
                                                    echo "<option value=\"" . $row['strCustomerPoNo'] . "\" selected=\"selected\">" . $row['strCustomerPoNo'] . "</option>";
                                                else
                                                    echo "<option value=\"" . $row['strCustomerPoNo'] . "\">" . $row['strCustomerPoNo'] . "</option>";
                                            }
                                            ?>
                                        </select></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="normalfnt">Customer</td>
                                    <td><select name="cboCustomer2" id="cboCustomer2" style="width:180px"  class="txtText" >
                                            <option value=""></option>
                                            <?php
                                            $sql = "SELECT DISTINCT 
							trn_orderheader.intCustomer,
							mst_customer.strName
							FROM
							trn_orderheader
							Inner Join mst_customer ON trn_orderheader.intCustomer = mst_customer.intId 
							ORDER BY mst_customer.strName ASC";
                                            $pp = $sql;
                                            $result = $db->RunQuery($sql);
                                            while ($row = mysqli_fetch_array($result)) {
                                                if ($row['intCustomer'] == $x_custId)
                                                    echo "<option value=\"" . $row['intCustomer'] . "\" selected=\"selected\">" . $row['strName'] . "</option>";
                                                else
                                                    echo "<option value=\"" . $row['intCustomer'] . "\">" . $row['strName'] . "</option>";
                                            }
                                            ?>
                                        </select></td>
                                    <td>&nbsp;</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="normalfnt">Order No<span class="compulsoryRed"> *</span></td>
                                    <td colspan="3"><select name="cboOrderNo2" id="cboOrderNo2" style="width:120px"  class="validate[required] txtText search" >
                                            <option value=""></option>
                                            <?php
                                            $sql = "SELECT DISTINCT
							trn_orderheader.intOrderNo, 
							trn_orderheader.intOrderYear 
							FROM trn_orderheader 
							where intStatus = 1 AND  
							 intOrderYear = $year AND  
							trn_orderheader.intLocationId='$location' 
							ORDER BY intOrderYear DESC, intOrderNo DESC";
                                            $result = $db->RunQuery($sql);
                                            while ($row = mysqli_fetch_array($result)) {
                                                if ($row['intOrderNo'] == $orderNo)
                                                    echo "<option value=\"" . $row['intOrderNo'] . "\" selected=\"selected\">" . $row['intOrderNo'] . "</option>";
                                                else
                                                    echo "<option value=\"" . $row['intOrderNo'] . "\">" . $row['intOrderNo'] . "</option>";
                                            }
                                            ?>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="normalfntRight"><table width="700" align="left" border="0" id="tblOrderWiseFabInOutBody2" class="main_table" cellspacing="0" <?php /* ?>style="display:none;"<?php */ ?>>
                                            <tr>
                                                <td width="201" align="right">Color Wise</td>
                                                <td width="22"><span class="normalfnt">
                                                        <input name="radio" type="radio" class="mouseover" id="chkColor2" value="1" checked="checked" />
                                                    </span></td>
                                                <td width="66" align="right">Cut wise</td>
                                                <td width="22"><span class="normalfnt">
                                                        <input name="radio" type="radio" class="mouseover" id="chkCutNo2"/>
                                                    </span></td>
                                                <td width="65" align="right">Size wise</td>
                                                <td width="22"><input name="radio" type="radio" class="mouseover" id="chkSize2"/></td>
                                                <td width="288">&nbsp;</td>
                                            </tr>
                                        </table></td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                            <tr>
                                                <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew2" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports2" tabindex="24"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose2" tabindex="27"/></a></td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </tbody>
                        </table>
                    </form></td>
            </tr>
            <!--END - PO-GRN Summery--> 
            <!--Department wise Issue Details-->
<!--            <tr>-->
<!--                <td><table align="left" width="600" class="main_header_table" id="tblHitRateHead" cellpadding="2" cellspacing="0">-->
<!--                        <tr>-->
<!--                            <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Sample hit rate -(excel).</u></td>-->
<!--                        </tr>-->
<!--                    </table></td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td><form name="frmSampleHitRate" id="frmSampleHitRate" autocomplete="off">-->
<!--                        <table width="700" align="left" border="0" id="tblHitRateBody" class="main_table" cellspacing="0" style="display:none;">-->
<!--                            <thead>-->
<!--                                <tr>-->
<!--                                    <td colspan="7" style="text-align:left">Sample hit rate selection criteria -(excel)</td>-->
<!--                                </tr>-->
<!--                            </thead>-->
<!--                            <tbody>-->
<!--                                <tr>-->
<!--                                    <td width="15%" class="normalfnt">Customer</td>-->
<!--                                    <td colspan="3	"><select name="cboCustomer" id="cboCustomer" style="width:220px"  class="txtText validate[required]"  >-->
<!--                                            <option value=""></option>-->
<!--                                            --><?php
//                                            $sql = "SELECT
//							mst_customer.intId,
//							mst_customer.strName
//							FROM mst_customer
//							WHERE
//							mst_customer.intStatus =  '1'
//							order by strName asc";
//                                            $result = $db->RunQuery($sql);
//                                            while ($row = mysqli_fetch_array($result)) {
//                                                echo "<option value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
//                                            }
//                                            ?>
<!--                                        </select></td>-->
<!--                                    <td width="15%" class="normalfnt">Marketer</td>-->
<!--                                    <td width="32%" class="normalfnt"><select name="cboMarketer" style="width:200px" id="cboMarketer">-->
<!--                                            <option value=""></option>-->
<!--                                            --><?php
//                                            $sql = "SELECT
//							mst_marketer.intUserId,
//							sys_users.strFullName
//							FROM
//							mst_marketer
//							INNER JOIN sys_users ON mst_marketer.intUserId = sys_users.intUserId
//							WHERE
//							sys_users.intStatus = 1
//							order by strFullName asc
//							";
//                                            $result = $db->RunQuery($sql);
//                                            while ($row = mysqli_fetch_array($result)) {
//                                                echo "<option value=\"" . $row['intUserId'] . "\">" . $row['strFullName'] . "</option>";
//                                            }
//                                            ?>
<!--                                        </select></td>-->
<!--                                    <td width="1%" class="normalfnt"></td>-->
<!--                                </tr> -->
<!--                                <tr>-->
<!--                                    <td class="normalfnt">Graphic</td>-->
<!--                                    <td colspan="3"><select name="cboGraphic" id="cboGraphic" style="width:220px"  class="txtText"  >-->
<!---->
<!--                                            <option value=""></option>-->
<!--                                            --><?php
//                                            $sql = "SELECT DISTINCT
//							trn_sampleinfomations.strGraphicRefNo
//							FROM
//							trn_sampleinfomations
//							order by strGraphicRefNo asc
//
//							";
//                                            $result = $db->RunQuery($sql);
//                                            while ($row = mysqli_fetch_array($result)) {
//                                                echo "<option value=\"" . $row['strGraphicRefNo'] . "\">" . $row['strGraphicRefNo'] . "</option>";
//                                            }
//                                            ?>
<!--                                        </select></td>-->
<!--                                    <td class="normalfnt">Sample Date</td>-->
<!--                                    <td class="normalfnt"><input name="dtSampleDate" type="text"   class="txtbox" id="dtSampleDate" style="width:80px;"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>-->
<!--                                    <td class="normalfnt"></td>-->
<!--                                </tr> -->
<!--                                <tr>-->
<!--                                    <td class="normalfnt">Sample No</td>-->
<!--                                    <td colspan="3"><select name="cboSampleYear" id="cboSampleYear" style="width:50px"  class="txtText"  >-->
<!--                                            <option value=""></option>-->
<!--                                            --><?php
//                                            $sql = "SELECT DISTINCT
//							trn_sampleinfomations.intSampleYear
//							FROM
//							trn_sampleinfomations
//							order by intSampleYear asc
//							";
//                                            $result = $db->RunQuery($sql);
//                                            while ($row = mysqli_fetch_array($result)) {
//                                                echo "<option value=\"" . $row['intSampleYear'] . "\">" . $row['intSampleYear'] . "</option>";
//                                            }
//                                            ?>
<!--                                        </select>-->
<!--                                        <select name="cboSampleNo" id="cboSampleNo" style="width:165px"  class="txtText"  >-->
<!--                                            <option value=""></option>-->
<!--                                        </select></td>-->
<!--                                    <td class="normalfnt">&nbsp;</td>-->
<!--                                    <td class="normalfnt">&nbsp;</td>-->
<!--                                    <td class="normalfnt"></td>-->
<!--                                </tr> -->
<!--                                <tr>-->
<!--                                    <td class="normalfnt">&nbsp;</td>-->
<!--                                    <td >&nbsp;</td>-->
<!--                                    <td >&nbsp;</td>-->
<!--                                    <td >&nbsp;</td>-->
<!--                                    <td class="normalfnt"></td>-->
<!--                                    <td class="normalfnt"></td>-->
<!--                                    <td class="normalfnt"></td>-->
<!--                                </tr>-->
<!--                                <tr>-->
<!--                                    <td colspan="7" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">-->
<!--                                            <tr>-->
<!--                                                <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport_exel.png" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport_excel('sampleHitRate', 'frmSampleHitRate', 'excel')"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>-->
<!--                                            </tr>-->
<!--                                        </table></td>-->
<!--                                </tr>-->
<!--                            </tbody>-->
<!--                        </table></form></td>-->
<!--            </tr>-->


            <!--END - Department wise Issue Details--> 
            <?php
            if($intUser==905 || $intUser==2 )
            {
                
            ?>
            <!--daily order approved details -->    
            <tr>
                <td><table align="left" width="600" class="main_header_table" id="tblOrderDetailsHead" cellpadding="2" cellspacing="0">
                        <tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Daily Approved Order Details -(excel).</u></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td><form name="frmOrderDetails" id="frmOrderDetails" autocomplete="off">
                        <table width="700" align="left" border="0" id="tblOrderDetailsBody" class="main_table" cellspacing="0" style="display:none;">
                            <thead>
                                <tr>
                                    <td colspan="7" style="text-align:left">Daily Approved Order Details -(excel)</td>
                                </tr>
                            </thead>
                            <tbody>




                                <tr>
                                    <td colspan="7" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                            <tr>
                                                <td width="100%" align="center" bgcolor="">

                                                    <img  style="display:inline" border="0" src="images/Treport_exel.png" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport_excel('orderDetail', 'frmOrderDetails', 'excel')"/><a href="main.php">
                                                        <img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </tbody>
                        </table></form></td>
            </tr>

  <!--end of daily order approved details -->   
  <!--daily fabric received approved details -->
   <tr>
                <td><table align="left" width="600" class="main_header_table" id="tblFabricDetailsHead" cellpadding="2" cellspacing="0">
                        <tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Daily Fabric received Details -(excel).</u></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td><form name="frmFabricDetails" id="frmFabricDetails" autocomplete="off">
                        <table width="700" align="left" border="0" id="tblFbricDetailsBody" class="main_table" cellspacing="0" style="display:none;">
                            <thead>
                                <tr>
                                    <td colspan="7" style="text-align:left">Daily Fabric Received Details -(excel)</td>
                                </tr>
                            </thead>
                            <tbody>




                                <tr>
                                    <td colspan="7" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                            <tr>
                                                <td width="100%" align="center" bgcolor="">

                                                    <img  style="display:inline" border="0" src="images/Treport_exel.png" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport_excel('fabricDetail', 'frmFabricDetails', 'excel')"/><a href="main.php">
                                                        <img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </tbody>
                        </table></form></td>
            </tr>

<?php
            }
?>

        </table>
    </div>
</div>
<!--</form>
-->
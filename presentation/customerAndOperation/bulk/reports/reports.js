var basepath = 'presentation/customerAndOperation/bulk/reports/';

$(document).ready(function () {
    //------------------------
    $('#tblFabricMovementHead').die('click').live('click', function () {

        headShowHide('tblFabricMovementHead', 'tblFabricMovementBody');

    });

    $('#tblFabricMovementBody thead').die('click').live('click', function () {

        bodyShowHide('tblFabricMovementHead', 'tblFabricMovementBody');

    });
    //----------------------

    $('#tblOrderWiseFabInOutHead').die('click').live('click', function () {

        headShowHide('tblOrderWiseFabInOutHead', 'tblOrderWiseFabInOutBody');

    });

    $('#tblOrderWiseFabInOutBody thead').die('click').live('click', function () {

        bodyShowHide('tblOrderWiseFabInOutHead', 'tblOrderWiseFabInOutBody');

    });

    //----------------------

    $('#tblFabricCheckStatusHead').die('click').live('click', function () {

        headShowHide('tblFabricCheckStatusHead', 'tblFabricCheckStatusBody');

    });

    $('#tblFabricCheckStatusBody thead').die('click').live('click', function () {

        bodyShowHide('tblFabricCheckStatusHead', 'tblFabricCheckStatusBody');

    });
    //----------------------

    $('#tblCustomerDispatchHead').die('click').live('click', function () {

        headShowHide('tblCustomerDispatchHead', 'tblCustomerDispatchBody');

    });

    $('#tblCustomerDispatchBody thead').die('click').live('click', function () {

        bodyShowHide('tblCustomerDispatchHead', 'tblCustomerDispatchBody');

    });
    //----------------------

    /*	$('#tblOrderWiseFabInOutHead2').click(function(){
     
     headShowHide('tblOrderWiseFabInOutHead2','tblOrderWiseFabInOutBody2');
     
     });
     */
    $('#tblOrderWiseFabInOutBody2 thead').die('click').live('click', function () {

        bodyShowHide('tblOrderWiseFabInOutHead2', 'tblOrderWiseFabInOutBody2');

    });

    //----------------------- 
    $('#tblCustomerWiseFabInOutHead').die('click').live('click', function () {

        headShowHide('tblCustomerWiseFabInOutHead', 'tblCustomerWiseFabInOutBody');

    });

    $('#tblCustomerWiseFabInOutBody thead').die('click').live('click', function () {

        bodyShowHide('tblCustomerWiseFabInOutHead', 'tblCustomerWiseFabInOutBody');

    });

    //----------------------- 
    $('#tblKPIHead').die('click').live('click', function () {
        headShowHide('tblKPIHead', 'tblKPIBody');

    });

    $('#tblKPIBody thead').die('click').live('click', function () {

        bodyShowHide('tblKPIHead', 'tblKPIBody');

    });
    //----------------------- 

    $('#tblHitRateHead').die('click').live('click', function () {
        headShowHide('tblHitRateHead', 'tblHitRateBody');

    });

    $('#tblHitRateBody thead').die('click').live('click', function () {

        bodyShowHide('tblHitRateHead', 'tblHitRateBody');

    });
    //----------------------- 
    $('#tblOrderDetailsHead').die('click').live('click', function () {
        headShowHide('tblOrderDetailsHead', 'tblOrderDetailsBody');

    });

    $('#tblOrderDetailsBody thead').die('click').live('click', function () {

        bodyShowHide('tblOrderDetailsHead', 'tblOrderDetailsBody');

    });
    //-----------------------------------
    $('#tblFabricDetailsHead').die('click').live('click', function () {
        headShowHide('tblFabricDetailsHead', 'tblFbricDetailsBody');

    });

    $('#tblFbricDetailsBody thead').die('click').live('click', function () {

        bodyShowHide('tblFabricDetailsHead', 'tblFbricDetailsBody');

    });
    //------------------------------------------------------------

    ///////////////////////Order Wise Fabric In - Out///////////////////////////////
    //$("#frmOrderWiseFabInOut").validationEngine();
    $("#frmOrderWiseFabInOut #cboOrderYear").die('change').live('change', function () {
        //	submitForm();
        $('#frmOrderWiseFabInOut #cboGraphicNo').val('');
        $('#frmOrderWiseFabInOut #cboStyle').val('');
        $('#frmOrderWiseFabInOut #cboPONo').val('');
        $('#frmOrderWiseFabInOut #cboOrderNo').val('');
        $('#frmOrderWiseFabInOut #cboCustomer').val('');
        loadAllCombo();
    });
    $("#frmOrderWiseFabInOut #cboOrderNo").die('change').live('change', function () {
        //	submitForm();
    });
    $("#frmOrderWiseFabInOut #imgSearchItems").die('click').live('click', function () {
        loadReport();
    });
    $("#frmOrderWiseFabInOut #butNew").die('click').live('click', function () {
        loadNewCombo();
        //window.location.href = window.location.href;
        //$('#tblOrderWiseFabInOutHead').show('slow');
        //$('#tblOrderWiseFabInOutBody').hide('slow');
    });
    $("#frmOrderWiseFabInOut .search").die('change').live('change', loadAllCombo);

    ///////////////////////////////////////////////////////// 
    //-------------------------------------------- 
    $('#frmFabricMovement #butSearch').die('click').live('click', function () {
        loadCutNos();
        loadSizes();
    });

////////////////////////////////////////////// 

    $("#frmCompanyStockBalance #butDownload").die('click').live('click', function () {
        if ($("#frmCompanyStockBalance").validationEngine('validate')) {
            //clearRows();
            downloadFile();
        }

    });
/////////////////////////////////////
    //-------------------------------------------- 
////////////////STYLE STOCK MOVEMENT/////////////////
    //-------------------------------------------- 
    $('#frmFabricMovement #butReports').die('click').live('click', function () {

        if ($('#frmFabricMovement').validationEngine('validate'))
        {
            var url = '?q=904';

            url += '&orderNo=' + $('#frmFabricMovement #cboOrderNo').val();
            url += '&orderYear=' + $('#frmFabricMovement #cboOrderYear').val();
            url += '&cutNo=' + $('#frmFabricMovement #cboCutNo').val();
            url += '&size=' + $('#frmFabricMovement #cboSize').val();
            window.open(url, 'rptMovement.php');
        }
    });
////////////////END OF STYLE STOCK MOVEMENT/////////////////
    //-------------------------------------------- 
///////////////ORDER NO WISE FABRIC IN OUT/////////////////
    //-------------------------------------------- 
    $('#frmOrderWiseFabInOut #butReports').die('click').live('click', function () {
        var checkHsShowStastus = $("#frmOrderWiseFabInOut #hsStcker").is(":checked")? 1:0;
        if ($('#frmOrderWiseFabInOut').validationEngine('validate'))
        {
            var url = '?q=948';
            url += '&orderNo=' + $('#frmOrderWiseFabInOut #cboOrderNo').val();
            url += '&orderYear=' + $('#frmOrderWiseFabInOut #cboOrderYear').val();
            url += '&styleNo=' + $('#frmOrderWiseFabInOut #cboStyle').val();
            url += '&hsStickerEnable=' + checkHsShowStastus;

            if ($('#frmOrderWiseFabInOut #chkColor').attr('checked'))
                url += '&option=1';
            else if ($('#frmOrderWiseFabInOut #chkCutNo').attr('checked'))
                url += '&option=2';
            else if ($('#frmOrderWiseFabInOut #chkSize').attr('checked'))
                url += '&option=3';
            else if ($('#frmOrderWiseFabInOut #chkPart').attr('checked'))
                url += '&option=4';
            else if ($('#frmOrderWiseFabInOut #chkSalesOrder').attr('checked'))
                url += '&option=5';
            else if ($('#frmOrderWiseFabInOut #chkSalesOrderSize').attr('checked')) {
                url = '';
                url += '?q=944';
                url += '&orderNo=' + $('#frmOrderWiseFabInOut #cboOrderNo').val();
                url += '&orderYear=' + $('#frmOrderWiseFabInOut #cboOrderYear').val();
                url += '&styleNo=' + $('#frmOrderWiseFabInOut #cboStyle').val();
                url += '&hsStickerEnable=' + checkHsShowStastus;
                url += '&option=6';
            }
            window.open(url, 'rptOrderNoWiseFabricInOut.php');
        }
    });
////////////////END OF ORDER NO WISE FABRIC IN OUT//////////////////

///////////////CUSTOMER WISE FABRIC IN OUT///////////////////////// 

    $("#frmCustomerWiseFabInOut #butReports").die('click').live('click', function () {
        if ($('#frmCustomerWiseFabInOut').validationEngine('validate'))
        {
            var url = '?q=942';
            url += '&customer=' + $('#frmCustomerWiseFabInOut #cboCustomer').val();
            url += '&fromDate=' + $('#frmCustomerWiseFabInOut #txtFromDate').val();
            url += '&toDate=' + $('#frmCustomerWiseFabInOut #txtToDate').val();
            window.open(url);
        }
    });

/////////////////////////FABRIC CHECK STATUS///////////////////////// 

    $("#frmFabricCheckStatus #butReports").die('click').live('click', function () {
        if ($('#frmFabricCheckStatus').validationEngine('validate'))
        {
            var url = '?q=935';

            window.open(url);
        }
    });

/////////////////////////KPI REPORTS///////////////////////// 

    $('#frmKPI #cboCustomer').die('change').live('change', function () {
        loadSampleNos();
        loadGraphicNos();
    });

    $('#frmKPI #cboSampYear').die('change').live('change', function () {
        loadSampleNos();
        loadGraphicNos();
    });

    $('#frmKPI #cboSampNo').die('change').live('change', function () {
        loadGraphicNos();
    });

    $('#frmKPI #cboGraphicNo').die('change').live('change', function () {
        //loadSampleNos();
        //loadSampleYear();
    });

    $("#frmKPI #butNew").die('click').live('click', function () {
        $('#frmKPI #cboCustomer').val('');
        $('#frmKPI #cboSampNo').val('');
        $('#frmKPI #cboSampYear').val('');
        $('#frmKPI #cboGraphicNo').val('');
        $('#frmKPI #txtFromDate10').val('');
        $('#frmKPI #txtToDate10').val('');
        loadSampleNos();
        loadGraphicNos();
    });


    $("#frmKPI #butReports").die('click').live('click', function () {
        if ($('#frmKPI').validationEngine('validate'))
        {
            var url = '?q=886';
            url += '&customer=' + $('#frmKPI #cboCustomer').val();
            url += '&sampNo=' + $('#frmKPI #cboSampNo').val();
            url += '&sampYear=' + $('#frmKPI #cboSampYear').val();
            url += '&graphicNo=' + $('#frmKPI #cboGraphicNo').val();
            url += '&fromDate=' + $('#frmKPI #txtFromDate10').val();
            url += '&toDate=' + $('#frmKPI #txtToDate10').val();
            window.open(url);
        }
    });







    $('#frmCustomerWiseFabInOut #chkDate').die('click').live('click', function () {
        var val = $(this).attr('checked');
        if (!val)
        {
            $('#frmCustomerWiseFabInOut #txtFromDate').val('');
            $('#frmCustomerWiseFabInOut #txtFromDate').attr('disabled', 'true');
            $('#frmCustomerWiseFabInOut #txtToDate').val('');
            $('#frmCustomerWiseFabInOut #txtToDate').attr('disabled', 'true');
        } else
        {
            $('#frmCustomerWiseFabInOut #txtFromDate').removeAttr('disabled');
            $('#frmCustomerWiseFabInOut #txtToDate').removeAttr('disabled');
        }
    });
//////////////END OF CUSTOMER WISE FABRIC IN OUT////////////////////////////

    $('#frmKPI #chkDate').die('click').live('click', function () {
        var val = $(this).attr('checked');
        if (!val)
        {
            $('#frmKPI #txtFromDate10').val('');
            $('#frmKPI #txtFromDate10').attr('disabled', 'true');
            $('#frmKPI #txtToDate10').val('');
            $('#frmKPI #txtToDate10').attr('disabled', 'true');
        } else
        {
            $('#frmKPI #txtFromDate10').removeAttr('disabled');
            $('#frmKPI #txtToDate10').removeAttr('disabled');
        }
    });

//--------------------------------------------	  
    $('#frmCustomerDispatch #chkDate').die('click').live('click', function () {
        var val = $(this).attr('checked');
        if (!val)
        {
            $('#frmCustomerDispatch #txtFromDate11').val('');
            $('#frmCustomerDispatch #txtFromDate11').attr('disabled', 'true');
            $('#frmCustomerDispatch #txtToDate11').val('');
            $('#frmCustomerDispatch #txtToDate11').attr('disabled', 'true');
            $('#frmCustomerDispatch #txtFH').val('');
            $('#frmCustomerDispatch #txtFH').attr('disabled', 'true');
            $('#frmCustomerDispatch #txtFM').val('');
            $('#frmCustomerDispatch #txtFM').attr('disabled', 'true');
            //$('#frmCustomerDispatch #txtFS').val('');  
            //$('#frmCustomerDispatch #txtFS').attr('disabled','true');
            $('#frmCustomerDispatch #txtTH').val('');
            $('#frmCustomerDispatch #txtTH').attr('disabled', 'true');
            $('#frmCustomerDispatch #txtTM').val('');
            $('#frmCustomerDispatch #txtTM').attr('disabled', 'true');
            //$('#frmCustomerDispatch #txtTS').val('');  
            //$('#frmCustomerDispatch #txtTS').attr('disabled','true');
        } else
        {
            var printDate = formatDate();

            $('#frmCustomerDispatch #txtFromDate11').removeAttr('disabled');
            $('#frmCustomerDispatch #txtFromDate11').val(printDate);
            $('#frmCustomerDispatch #txtToDate11').removeAttr('disabled');
            $('#frmCustomerDispatch #txtToDate11').val(printDate);
            $('#frmCustomerDispatch #txtFH').removeAttr('disabled');
            $('#frmCustomerDispatch #txtFM').removeAttr('disabled');
            //$('#frmCustomerDispatch #txtFS').removeAttr('disabled');
            $('#frmCustomerDispatch #txtTH').removeAttr('disabled');
            $('#frmCustomerDispatch #txtTM').removeAttr('disabled');
            //$('#frmCustomerDispatch #txtTS').removeAttr('disabled');
        }
    });

    $("#frmCustomerDispatch #butReports").die('click').live('click', function () {
        if ($('#frmCustomerDispatch').validationEngine('validate'))
        {
            var url = '?q=937';
            url += '&location=' + $('#frmCustomerDispatch #cboLocation').val();
            url += '&dtFrom=' + $('#frmCustomerDispatch #txtFromDate11').val();
            url += '&dtTo=' + $('#frmCustomerDispatch #txtToDate11').val();
            url += '&dtFH=' + $('#frmCustomerDispatch #txtFH').val();
            url += '&dtFM=' + $('#frmCustomerDispatch #txtFM').val();
            url += '&dtTH=' + $('#frmCustomerDispatch #txtTH').val();
            url += '&dtTM=' + $('#frmCustomerDispatch #txtTM').val();
            window.open(url);
        }
    });

    //-------------------------------------------- 
    $('#frmSampleHitRate #cboSampleYear').change(function () {
        var customer = $('#frmSampleHitRate #cboCustomer').val();
        var year = $('#frmSampleHitRate #cboSampleYear').val();
        var graphic = $('#frmSampleHitRate #cboGraphic').val();
        var url = "controller.php?q=61&requestType=loadSampleNos&sampYear=" + year + "&graphic=" + graphic + "&customer=" + customer;
        ;
        var httpobj = $.ajax({url: url, async: false})
        $('#frmSampleHitRate #cboSampleNo').html(httpobj.responseText);

    });
    //-------------------------------------------- 
    $('#frmSampleHitRate #cboGraphic').change(function () {
        $('#frmSampleHitRate #cboSampleYear').val('');
        $('#frmSampleHitRate #cboSampleNo').val('');
    });
    //-------------------------------------------- 
    $('#frmSampleHitRate #cboCustomer').change(function () {
        var customer = $('#frmSampleHitRate #cboCustomer').val();
        var url = "controller.php?q=61&requestType=loadGraphicNos&customer=" + customer;
        var httpobj = $.ajax({url: url, async: false})
        $('#frmSampleHitRate #cboGraphic').html(httpobj.responseText);
        $('#frmSampleHitRate #cboSampleYear').val('');
        $('#frmSampleHitRate #cboSampleNo').val('');
    });
    //-------------------------------------------- 
    $('#frmSampleHitRate #butNew').click(function () {
        $('#frmSampleHitRate #cboSampleYear').val('');
        $('#frmSampleHitRate #cboSampleNo').val('');
        $('#frmSampleHitRate #cboCustomer').val('');
        $('#frmSampleHitRate #cboMarketer').val('');
        $('#frmSampleHitRate #cboGraphic').val('');
        $('#frmSampleHitRate #dtSampleDate').val('');

    });

    //-------------------------------------------- 
//--------------------------------------------
});


function loadOrderNos()
{
    var orderYear = $('#frmFabricMovement #cboOrderYear').val();
    var url = "controller.php?q=61&requestType=loadOrderNos&orderYear=" + orderYear;
    var httpobj = $.ajax({url: url, async: false})
    $('#frmFabricMovement #cboOrderNo').html(httpobj.responseText);
}
function loadCutNos()
{
    var orderNo = $('#frmFabricMovement #cboOrderNo').val();
    var orderYear = $('#frmFabricMovement #cboOrderYear').val();
    var url = "controller.php?q=61&requestType=loadCutNos&orderNo=" + orderNo + "&orderYear=" + orderYear;
    var httpobj = $.ajax({url: url, async: false})
    $('#frmFabricMovement #cboCutNo').html(httpobj.responseText);
}
function loadSizes()
{
    var orderNo = $('#frmFabricMovement #cboOrderNo').val();
    var orderYear = $('#frmFabricMovement #cboOrderYear').val();
    var url = "controller.php?q=61&requestType=loadSizes&orderNo=" + orderNo + "&orderYear=" + orderYear;
    var httpobj = $.ajax({url: url, async: false})
    $('#frmFabricMovement #cboSize').html(httpobj.responseText);
}

////////////////////////////////////////////
function ViewReport(type, formId)
{
    if ($('#' + formId).validationEngine('validate')) {
        var url = "report.php?type=" + type + "&" + $('#' + formId).serialize();
        window.open(url)
    }
}


///////////////////////////////////	
function downloadFile() {
    var date = $('#frmCompanyStockBalance #dtDateToS').val();
    window.open('excellReports/companyStockBalance/companyStockBalance-xlsx.php?company=' + $('#frmCompanyStockBalance #cboCompany').val() + '&date=' + date + '&location=' + $('#frmCompanyStockBalance #cboLocation').val() + '&currency=' + $('#frmCompanyStockBalance #cboCurrency').val());

}

////////////////////////////////////
function headShowHide(headId, bodyId) {

    $('#tblFabricMovementBody').hide('slow');
    $('#tblOrderWiseFabInOutBody').hide('slow');
    $('#tblCustomerWiseFabInOutBody').hide('slow');
    $('#tblKPIBody').hide('slow');
    $('#tblFabricCheckStatusBody').hide('slow');
    $('#tblCustomerDispatchBody').hide('slow');


    $('#tblFabricMovementHead').show('slow');
    $('#tblOrderWiseFabInOutHead').show('slow');
    $('#tblCustomerWiseFabInOutHead').show('slow');
    $('#tblKPIHead').show('slow');
    $('#tblFabricCheckStatusHead').show('slow');
    $('#tblCustomerDispatchHead').show('slow');



    $('#' + headId).hide('slow');
    $('#' + bodyId).show('slow');

}
//------------------------------------
function bodyShowHide(headId, bodyId) {
    $('#' + headId).show('slow');
    $('#' + bodyId).hide('slow');
}
////////////////////////////////////////

///////////////////////Order Wise Fabric In - Out///////////////////////////////
//------------------------------------------------------------------------
function submitForm() {
    window.location.href = "orderNoWiseFabricInOut/orderNoWiseFabricInOut.php?orderNo=" + $('#cboOrderNo').val()
            + '&salesOrderNo=' + $('#frmOrderWiseFabInOut #cboSalesOrderNo').val()
            + '&poNo=' + $('#frmOrderWiseFabInOut #cboPONo').val()
            + '&customer=' + $('#frmOrderWiseFabInOut #cboCustomer').val()
            + '&orderYear=' + $('#frmOrderWiseFabInOut #cboOrderYear').val()
}
//------------------------------------------------------------------------
function loadAllCombo()
{

    //######################
    //####### get values ###
    //######################
    var year = $('#frmOrderWiseFabInOut #cboOrderYear').val();
    var graphicNo = $('#frmOrderWiseFabInOut #cboGraphicNo').val();
    var styleId = $('#frmOrderWiseFabInOut #cboStyle').val();
    var customerPONo = $('#frmOrderWiseFabInOut #cboPONo').val();
    var orderNo = $('#frmOrderWiseFabInOut #cboOrderNo').val();
    var customerId = $('#frmOrderWiseFabInOut #cboCustomer').val();

    //######################
    //####### create url ###
    //######################
    var url = "presentation/customerAndOperation/bulk/reports/orderNoWiseFabricInOut/orderNoWiseFabricInOut-db-get.php?requestType=loadAllComboDetails";
    url += "&year=" + year;
    url += "&graphicNo=" + graphicNo;
    url += "&styleId=" + styleId;
    url += "&customerPONo=" + customerPONo;
    url += "&orderNo=" + orderNo;
    url += "&customerId=" + customerId;

    //######################
    //####### send data  ###
    //######################
    $.ajax({url: url,
        async: false,
        dataType: 'json',
        success: function (json) {
            $('#frmOrderWiseFabInOut #cboStyle').html(json.styleNo);
            $('#frmOrderWiseFabInOut #cboGraphicNo').html(json.graphicNo);
            $('#frmOrderWiseFabInOut #cboPONo').html(json.customerPoNo);
            $('#frmOrderWiseFabInOut #cboOrderNo').html(json.orderNo);
            $('#frmOrderWiseFabInOut #cboCustomer').html(json.customer);
        }
    });
}
//------------------------------------------------------------------------
//------------------------------------------------------------------------
function loadNewCombo()
{

    //######################
    //####### get values ###
    //######################
    var year = $('#frmOrderWiseFabInOut #cboOrderYear').val();
    var graphicNo = '';
    var styleId = '';
    var customerPONo = '';
    var orderNo = '';
    var customerId = '';

    //######################
    //####### create url ###
    //######################
    var url = "presentation/customerAndOperation/bulk/reports/orderNoWiseFabricInOut/orderNoWiseFabricInOut-db-get.php?requestType=loadAllComboDetails";
    url += "&year=" + year;
    url += "&graphicNo=" + graphicNo;
    url += "&styleId=" + styleId;
    url += "&customerPONo=" + customerPONo;
    url += "&orderNo=" + orderNo;
    url += "&customerId=" + customerId;

    //######################
    //####### send data  ###
    //######################
    $.ajax({url: url,
        async: false,
        dataType: 'json',
        success: function (json) {
            $('#frmOrderWiseFabInOut #cboStyle').html(json.styleNo);
            $('#frmOrderWiseFabInOut #cboGraphicNo').html(json.graphicNo);
            $('#frmOrderWiseFabInOut #cboPONo').html(json.customerPoNo);
            //$('#frmOrderWiseFabInOut #cboOrderNo').html(json.orderNo);
            $('#frmOrderWiseFabInOut #cboCustomer').html(json.customer);
        }
    });
}
//------------------------------------------------------------------------
function loadReport()
{
    if ($('#frmOrderWiseFabInOut').validationEngine('validate'))
    {
        var url = 'orderNoWiseFabricInOut/rptOrderNoWiseFabricInOut.php?';

        url += 'orderNo=' + $('#frmOrderWiseFabInOut #cboOrderNo').val();
        url += '&orderYear=' + $('#frmOrderWiseFabInOut #cboOrderYear').val();
        url += '&styleNo=' + $('#frmOrderWiseFabInOut #cboStyle').val();

        if ($('#frmOrderWiseFabInOut #chkColor').attr('checked'))
            url += '&option=1';
        else if ($('#frmOrderWiseFabInOut #chkCutNo').attr('checked'))
            url += '&option=2';
        else if ($('#frmOrderWiseFabInOut #chkSize').attr('checked'))
            url += '&option=3';
        else if ($('#frmOrderWiseFabInOut #chkPart').attr('checked'))
            url += '&option=4';
        else if ($('#frmOrderWiseFabInOut #chkSalesOrder').attr('checked'))
            url += '&option=5';
        else if ($('#frmOrderWiseFabInOut #chkSalesOrderSize').attr('checked')) {
            url = '';
            url += 'orderNoWiseFabricInOut/rptOrderNoWiseFabricInOut_sales_size_wise.php?';
            url += 'orderNo=' + $('#frmOrderWiseFabInOut #cboOrderNo').val();
            url += '&orderYear=' + $('#frmOrderWiseFabInOut #cboOrderYear').val();
            url += '&styleNo=' + $('#frmOrderWiseFabInOut #cboStyle').val();
            url += '&option=6';
        }

        window.open(url);
    }
}


function loadSampleNos() {
    var customer = $('#frmKPI #cboCustomer').val();
    var sampYear = $('#frmKPI #cboSampYear').val();

    var url = "controller.php?q=61&requestType=loadSampleNos";
    url += "&customer=" + customer;
    url += "&sampYear=" + sampYear;
    var httpobj = $.ajax({url: url, async: false})
    $('#frmKPI #cboSampNo').html(httpobj.responseText);
}

function loadGraphicNos() {
    var customer = $('#frmKPI #cboCustomer').val();
    var sampNo = $('#frmKPI #cboSampNo').val();
    var sampYear = $('#frmKPI #cboSampYear').val();

    var url = "controller.php?q=61&requestType=loadGraphicNos";
    url += "&customer=" + customer;
    url += "&sampNo=" + sampNo;
    url += "&sampYear=" + sampYear;
    var httpobj = $.ajax({url: url, async: false})
    $('#frmKPI #cboGraphicNo').html(httpobj.responseText);
}

function ViewReport_excel(type, formId, format)
{

    if (type == 'sampleHitRate') {

        var url = "controller.php?q=61&requestType=loadHitRateRecords";
        url += "&" + $('#' + formId).serialize();
        /*	var httpobj 	= $.ajax({url:url,async:false})
         alert(httpobj.responseText);
         return httpobj.responseText;*/

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'post',
            data: '',
            async: false,
            success: function (json) {
                $('#frmSampleHitRate #butReports').validationEngine('showPrompt', json.msg, json.type /*'pass'*/);
                if (json.type == 'pass')
                {
                    var url = "presentation/customerAndOperation/bulk/reports/rptHitRate.php?type=" + type + "&report_type=" + format + "&" + $('#' + formId).serialize();
                    window.open(url)
                    return;
                } else
                {
                    hideWaiting();
                }
            },
            error: function (xhr, status)
            {
                $('#frmSampleHitRate #butReports').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
                var t = setTimeout("alertx()", 2000);
                hideWaiting();
                return;
                ;
            }
        });
    } else if (type == 'orderDetail') {

        var url = "controller.php?q=61&requestType=loadOrderDetails";
        url += "&" + $('#' + formId).serialize();
        /*	var httpobj 	= $.ajax({url:url,async:false})
         alert(httpobj.responseText);
         return httpobj.responseText;*/

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'post',
            data: '',
            async: false,
            success: function (json) {
                $('#frmOrderDetails #butReports').validationEngine('showPrompt', json.msg, json.type /*'pass'*/);
                if (json.type == 'pass')
                {
                    var url = "presentation/customerAndOperation/bulk/reports/rptDailyOrderDetails.php?type=" + type + "&report_type=" + format + "&" + $('#' + formId).serialize();
                    window.open(url)
                    return;
                } else
                {
                    hideWaiting();
                }
            },
            error: function (xhr, status)
            {
                $('#frmOrderDetails #butReports').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
                var t = setTimeout("alertx()", 2000);
                hideWaiting();
                return;
                ;
            }
        });
    } else if (type == 'fabricDetail') {

        var url = "controller.php?q=61&requestType=loadFabricDetails";
        url += "&" + $('#' + formId).serialize();
        /*	var httpobj 	= $.ajax({url:url,async:false})
         alert(httpobj.responseText);
         return httpobj.responseText;*/

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'post',
            data: '',
            async: false,
            success: function (json) {
                $('#frmFabricDetails #butReports').validationEngine('showPrompt', json.msg, json.type /*'pass'*/);
                if (json.type == 'pass')
                {
                    var url = "presentation/customerAndOperation/bulk/reports/rptDailyFabricReceivedDetails.php?type=" + type + "&report_type=" + format + "&" + $('#' + formId).serialize();
                    window.open(url)
                    return;
                } else
                {
                    hideWaiting();
                }
            },
            error: function (xhr, status)
            {
                $('#frmFabricDetails #butReports').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
                var t = setTimeout("alertx()", 2000);
                hideWaiting();
                return;
                ;
            }
        });
    }
    var t = setTimeout("alertx()", 2000);

}

function alertx()
{
    $('#frmSampleHitRate #butReports').validationEngine('hide');
}

function formatDate() {
    var d = new Date(),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

///////////////////////////////////////////////////////////
<?php 
	include "config.php";
	$path		= MAIN_URL;
	$sql	= " SELECT
				(
				SELECT MAX(AP.dtApprovedDate)
				FROM trn_orderheader_approvedby AS AP
				WHERE
				AP.intOrderNo=trn_orderheader_approvedby.intOrderNo AND
				AP.intYear=trn_orderheader_approvedby.intYear
				) AS LAST_REC_DATE,
				
				 
				(
				SELECT MAX(AP1.dtApprovedDate) AS MX
				FROM trn_orderheader_approvedby AS AP1
				WHERE
				AP1.intOrderNo=trn_orderheader_approvedby.intOrderNo AND
				AP1.intYear=trn_orderheader_approvedby.intYear
				AND AP1.dtApprovedDate < LAST_REC_DATE
				ORDER BY AP1.dtApprovedDate DESC LIMIT 1
				) AS PRE_REC_DATE,
				
				
				(
				SELECT  
				AP2.intApproveLevelNo
				FROM trn_orderheader_approvedby AS AP2
				WHERE
				AP2.intOrderNo=trn_orderheader_approvedby.intOrderNo AND
				AP2.intYear=trn_orderheader_approvedby.intYear
				AND AP2.dtApprovedDate = PRE_REC_DATE
				ORDER BY AP2.dtApprovedDate DESC LIMIT 1
				) AS PRE_APP_LEVEL,
				
				
				trn_orderheader_approvedby.intApproveLevelNo,
				trn_orderheader_approvedby.intOrderNo,
				trn_orderheader_approvedby.intYear,
				trn_orderheader_approvedby.intApproveUser,
				trn_orderheader_approvedby.dtApprovedDate,
				trn_orderheader_approvedby.intStatus
				FROM `trn_orderheader_approvedby`
				HAVING (intApproveLevelNo=0 /*OR (intApproveLevelNo =1 AND PRE_APP_LEVEL =0)*/)
				AND (LAST_REC_DATE= dtApprovedDate) 
				AND DATE(LAST_REC_DATE) >= '2015-09-18'
				";
	$result	= $db->RunQuery($sql);
	while($row	= mysqli_fetch_array($result))
	{
		$orderNo	= $row['intOrderNo'];
		$orderYear	= $row['intYear'];
		$orderArr	= NULL;
		
		$sql_email		= " SELECT trn_orderheader.intCreator as marketingUserId, dtmCreateDate, sys_users.strUserName as toUser FROM trn_orderheader left join 
 sys_users on sys_users.intUserId = trn_orderheader.intCreator 
							WHERE
							trn_orderheader.intOrderNo = '$orderNo' AND
							trn_orderheader.intOrderYear = '$orderYear' ";
								
		$result_email	= $db->RunQuery($sql_email);
		while($row_email	= mysqli_fetch_array($result_email))
		{
			$con_orderNo	= $orderNo.'/'.$orderYear;
			$userIdList		= $row_email['marketingUserId'];
								
			$strEmailAddress = $obj_comm->getEmailList($userIdList);
			$url			 = $path."?q=896&orderNo=$orderNo&orderYear=$orderYear&approveMode=1";
			ob_start();
            $creator = $row_email['toUser'];
            $createdDate=$row_email['dtmCreateDate'];
		include "rptPORejectReminder.php";		
		$body = ob_get_clean();
		ob_end_clean();

		//$body =  $strEmailAddress.'<br>'. $strCCEmailAddress . '<br>'.  $body;
		echo $body."<br>";

		$FROM_NAME			= "NSOFT - SCREENLINE HOLDINGS";
		$FROM_EMAIL			= "";
		 
		$nowDate = date('Y-m-d');
		$mailHeader= "PO REJECT REMINDER - $con_orderNo";
		//Original
		$mail_TO	= $strEmailAddress;
		$mail_CC	= '';
		$mail_BCC	= '';
	
		insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
		
		}
	}
?>
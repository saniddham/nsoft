<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
ini_set('display_errors',0);

$companyId 		= $sessions->getLocationId();
$intUser  		= $sessions->getUserId();

$customer 		= $_REQUEST['customer'];
$from 			= $_REQUEST['fromDate'];
$to 			= $_REQUEST['toDate'];
$programCode	= 'P0528';

$sql = "SELECT
		mst_customer.strName
		FROM mst_customer
		WHERE
		mst_customer.intId =  '$customer'"; 
$result = $db->RunQuery($sql);
$row = mysqli_fetch_array($result);

$locationId 	 = $companyId;//this locationId use in report header(reportHeader.php)--------------------
$x_customer 	 = $row['strName'];

$dateString		 = '';

if($from!=''){
	$dateString .='From '.$from;
}
if($to!=''){
	$dateString .=' To '.$to;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer wise Fabric In - Out Report</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />


<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:272px;
	top:511px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>

<form id="frmRptOrderNoWiseFabricInOut" name="frmRptOrderNoWiseFabricInOut" method="post" action="rptOrderNoWiseFabricInOut.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>CUSTOMER WISE FABRIC IN - OUT  REPORT</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="9%" class="normalfnt"><strong>Customer</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="41%"><span class="normalfnt"><?php echo $x_customer; ?></span></td>
    <td width="8%" class="normalfnt">&nbsp;</td>
    <td width="1%" align="center" valign="middle">&nbsp;</td>
    <td width="13%">&nbsp;</td>
    <td width="9%" class="normalfnt">&nbsp;</td>
    <td width="1%">&nbsp;</td>
    <td width="16%">&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Date Range</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $dateString; ?></span></td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="3" class="normalfnt">
          <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
          <thead>
            <tr class="">
              <th width="10%" >Date</th>
              <th width="8%" >Received No</th>
              <th width="4%" >Qty</th>
              
              
              
              <th width="5%" >&nbsp;</th>
              <th width="10%" >Date</th>
              <th width="11%" >Dispatch No</th>
              <th width="4%" >CUT RETURN</th>
              <th width="4%" >FD</th>
              <th width="4%" >PD</th>
              <th width="3%" >ED</th>
              <th width="4%" >SAMPLE</th>
              <th width="5%" >GOOD</th>
              </tr>
              </thead>
              <tbody>
              <?php 
              $totQty=0;
              $totQtyCutRet=0;
              $totQtyFD=0;
              $totQtyPD =0;
              $totQtyED =0;
              $totQtySamp=0;
              $totQtyGood =0;
			  $totQtyTotal=0;
			  
			  
			  $receivedRecords=getRecvRecords($customer,$from,$to);
			  $dispNoString="'*'";
			  
			  $result=getRowsReceived($customer,$from,$to);
			  while($row=mysqli_fetch_array($result))
			  {
				$date=$row['dtmdate'];
				$recvNo=$row['recvNo'];
				$AODNo=$row['strAODNo'];
				$para=$row['para'];
				$qty=$row['Qty'];
	  ?>
      
  <!--------------------------------------------------------------------------------------------------------------->          
            <tr class="normalfnt"   bgcolor="#FFFFFF">
             <td align="center" class="normalfntMid" id="<?php echo $date; ?>" ><?php echo $date; ?></td>
			<td align="center" class="normalfntMid" id="<?php echo $recvNo; ?>" ><?php echo $recvNo; ?></td>
			<td align="center" class="normalfntRight" id="<?php echo $qty; ?>" ><?php echo $qty; ?></td>
            
			<td align="center" class="normalfntRight" id="" ></td>
            
            <?php  
			  $rcds=0;
			  $result2=getRowsDispatch($customer,$from,$to,$dispNoString);
			  while(($row1=mysqli_fetch_array($result2)) && ($rcds<1))
			  {
				$date=$row1['date'];
				$dispNo=$row1['dispatNo'];
				$para=$row1['para'];
				$total=$row1['Total'];
				$cutRet=$row1['dblCutRetQty'];
				$fd=$row1['dblFdammageQty'];
				$pd=$row1['dblPDammageQty'];
				$ed=$row1['dblEmbroideryQty'];
				$samp=$row1['dblSampleQty'];
				$good=$row1['dblGoodQty'];
				$dispNoString.=",'".$dispNo."'";
				$dispNoString1=$dispNoString;
			?>
            
			<td align="center" class="normalfntMid" id="<?php echo $date; ?>" ><?php echo $date; ?></td>
			<td align="center" class="normalfntMid" id="<?php echo $dispNo; ?>" ><?php echo $dispNo; ?></td>
			<td class="normalfntRight" id="<?php echo $fd; ?>"><span class="normalfntMid"><?php echo $cutRet; ?></span></td>
			<td class="normalfntRight" id="<?php echo $fd; ?>"><span class="normalfntMid"><?php echo $fd; ?></span></td>
			<td class="normalfntRight" id="<?php echo $pd; ?>"><span class="normalfntMid"><?php echo $pd; ?></span></td>
			<td class="normalfntRight" id="<?php echo $ed; ?>"><span class="normalfntMid"><?php echo $ed; ?></span></td>
			<td class="normalfntRight" id="<?php echo $samp; ?>"><span class="normalfntMid"><?php echo $samp; ?></span></td>
			<td class="normalfntRight" id="<?php echo $good; ?>"><span class="normalfntMid"><?php echo $good; ?></span></td>
            
            <?php 
				$rcds++; 
			  }
			?>
            <!--------------------------------------------------------------------------->          
            <?php 
			if($rcds==0){
				$date='';
				$dispNo='';
				$para='';
				$total=0;
				$fd=0;
				$pd=0;
				$ed=0;
				$samp=0;
				$good=0;
				//$dispNoString.=",'".$dispNo."'";
				//$dispNoString1=$dispNoString;
			?>
			<td align="center" class="normalfntMid" id="" ></td>
			<td align="center" class="normalfntMid" id="" >&nbsp;</td>
			<td align="center" class="normalfntMid" id="" >&nbsp;</td>
			<td align="center" class="normalfntMid" id="" ></td>
			<td align="center" class="normalfntMid" id="" >&nbsp;</td>
			<td align="center" class="normalfntMid" id="" ></td>
			<td align="center" class="normalfntMid" id="" >&nbsp;</td>
			<td align="center" class="normalfntMid" id="" ></td>
            <?php 
			  }
			?>
            </tr>
            <!--------------------------------------------------------------------------->          
            
            
            
      <?php 
			  $totQty+=$qty;
              $totQtyTotal+=$total;
              $totQtyFD+=$fd;
              $totQtyPD+=$pd;
              $totQtyED+=$ed;
              $totQtySamp+=$samp;
              $totQtyGood+=$good;
			
			}
	  ?>
  <!--------------------------------------------------------------------------------------------------------------->          
            <?php  
			  $result2=getRowsDispatch($customer,$from,$to,$dispNoString1);
			  while($row1=mysqli_fetch_array($result2))
			  {
				$date=$row1['date'];
				$dispNo=$row1['dispatNo'];
				$para=$row1['para'];
				$total=$row1['Total'];
				$cutRet=$row1['dblCutRetQty'];
				$fd=$row1['dblFdammageQty'];
				$pd=$row1['dblPDammageQty'];
				$ed=$row1['dblEmbroideryQty'];
				$samp=$row1['dblSampleQty'];
				$good=$row1['dblGoodQty'];
				$dispNoString.=",'".$dispNo."'";
			?>
            <tr class="normalfnt"   bgcolor="#FFFFFF">
             <td align="center" class="normalfntMid" id="" ></td>
			<td align="center" class="normalfntMid" id="" ></td>
			<td align="center" class="normalfntRight" id="" ></td>
            
			<td align="center" class="normalfntRight" id="" ></td>

			<td align="center" class="normalfntMid" id="<?php echo $date; ?>" ><?php echo $date; ?></td>
			<td align="center" class="normalfntMid" id="<?php echo $dispNo; ?>" ><?php echo $dispNo; ?></td>
			<td class="normalfntRight" id="<?php echo $cutRet; ?>"><span class="normalfntMid"><?php echo $cutRet; ?></span></td>
			<td class="normalfntRight" id="<?php echo $fd; ?>"><span class="normalfntMid"><?php echo $fd; ?></span></td>
			<td class="normalfntRight" id="<?php echo $pd; ?>"><span class="normalfntMid"><?php echo $pd; ?></span></td>
			<td class="normalfntRight" id="<?php echo $ed; ?>"><span class="normalfntMid"><?php echo $ed; ?></span></td>
			<td class="normalfntRight" id="<?php echo $samp; ?>"><span class="normalfntMid"><?php echo $samp; ?></span></td>
			<td class="normalfntRight" id="<?php echo $good; ?>"><span class="normalfntMid"><?php echo $good; ?></span></td>
            </tr>              
            <?php 
              $totQtyTotal+=$total;
              $totQtyCutRet+=$cutRet;
              $totQtyFD+=$fd;
              $totQtyPD+=$pd;
              $totQtyED+=$ed;
              $totQtySamp+=$samp;
              $totQtyGood+=$good;
			  }
			?>
 <!---------------------------------------------------------------------------------------------------------------->           
      
      
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" ><span class="normalfntRight"><?php echo $totQty ?></span></td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;</td>
               <td class="normalfntRight" ><?php echo $totQtyCutRet ?></td>
              <td class="normalfntRight" ><?php echo $totQtyFD ?></td>
              <td class="normalfntRight" ><?php echo $totQtyPD ?></td>
              <td class="normalfntRight" ><?php echo $totQtyED ?></td>
              <td class="normalfntRight" ><?php echo $totQtySamp ?></td>
              <td class="normalfntRight" ><?php echo $totQtyGood ?></td>
              </tr>
              </tbody>
            </table>
          </td>
        <td width="5%">&nbsp;</td>
        <td width="1%" colspan="5"></td>
        </tr>
      
      </table>
    </td>
</tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php
//----------------------------------------------
function getRecvRecords($customer,$from,$to){
	global $db;
	
	$sql = "SELECT DISTINCT
ware_fabricreceivedheader.intFabricReceivedNo,
ware_fabricreceivedheader.intFabricReceivedYear
FROM
ware_fabricreceivedheader
Inner Join trn_orderheader ON ware_fabricreceivedheader.intOrderNo = trn_orderheader.intOrderNo AND ware_fabricreceivedheader.intOrderYear = trn_orderheader.intOrderYear
WHERE
trn_orderheader.intCustomer =  '$customer'  AND 
ware_fabricreceivedheader.intStatus='1' ";
if($from!='')
$sql .= " AND ware_fabricreceivedheader.dtmdate >=  '$from'";	
if($to!='')
$sql .= " AND ware_fabricreceivedheader.dtmdate <=  '$to'";	
	
		$result =  $db->RunQuery($sql);
		$no =  	mysqli_num_rows($result);
		return $no;
}
//----------------------------------------------
function getRowsReceived($customer,$from,$to){
	global $db;
	

	$sql = "SELECT
			ware_fabricreceivedheader.dtmdate,
			concat(ware_fabricreceivedheader.intFabricReceivedNo,'/',ware_fabricreceivedheader.intFabricReceivedYear) as recvNo, 
			ware_fabricreceivedheader.strAODNo,
			Sum(ware_fabricreceiveddetails.dblQty) as Qty
			FROM
			ware_fabricreceivedheader
			Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
			Inner Join trn_orderheader ON ware_fabricreceivedheader.intOrderNo = trn_orderheader.intOrderNo AND ware_fabricreceivedheader.intOrderYear = trn_orderheader.intOrderYear
			WHERE
			trn_orderheader.intCustomer =  '$customer' AND 
			ware_fabricreceivedheader.intStatus='1'  ";
			if($from!='')
			$sql .= " AND ware_fabricreceivedheader.dtmdate >=  '$from'";	
			if($to!='')
			$sql .= " AND ware_fabricreceivedheader.dtmdate <=  '$to'";	
			$sql .= "GROUP BY
			ware_fabricreceivedheader.intFabricReceivedNo,
			ware_fabricreceivedheader.intFabricReceivedYear, ";
			$sql .= "ware_fabricreceivedheader.dtmdate";	
	
		 $result = $db->RunQuery($sql);
		// $row=mysqli_fetch_array($result);
		// return $row;
		return $result;
}
//----------------------------------------------
function getRowsDispatch($customer,$from,$to,$dispNoString){
	global $db;
	
	  $sql = "SELECT
			ware_fabricdispatchheader.dtmdate as date,
			concat(ware_fabricdispatchheader.intBulkDispatchNo,'/',ware_fabricdispatchheader.intBulkDispatchNoYear) as dispatNo, 
			(ware_fabricdispatchdetails.dblFdammageQty+ ware_fabricdispatchdetails.dblPDammageQty+ ware_fabricdispatchdetails.dblEmbroideryQty+ ware_fabricdispatchdetails.dblSampleQty+ ware_fabricdispatchdetails.dblGoodQty+ IFNULL(ware_fabricdispatchdetails.dblCutRetQty,0)) AS Total,
			IFNULL(ware_fabricdispatchdetails.dblCutRetQty,0) AS dblCutRetQty,
			ware_fabricdispatchdetails.dblFdammageQty,
			ware_fabricdispatchdetails.dblPDammageQty,
			ware_fabricdispatchdetails.dblEmbroideryQty,
			ware_fabricdispatchdetails.dblSampleQty,
			ware_fabricdispatchdetails.dblGoodQty 
			FROM
			ware_fabricdispatchheader
			Inner Join ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear 
			Inner Join trn_orderheader ON ware_fabricdispatchheader.intOrderNo = trn_orderheader.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderheader.intOrderYear
			WHERE
			trn_orderheader.intCustomer =  '$customer' AND 
			ware_fabricdispatchheader.intStatus='1'   AND
			concat(ware_fabricdispatchheader.intBulkDispatchNo,'/',ware_fabricdispatchheader.intBulkDispatchNoYear)  NOT IN (".$dispNoString.") ";
			if($from!='')
			$sql .= " AND ware_fabricdispatchheader.dtmdate >=  '$from'";	
			if($to!='')
			$sql .= " AND ware_fabricdispatchheader.dtmdate <=  '$to'";	
			$sql .= "GROUP BY
			ware_fabricdispatchheader.intBulkDispatchNo,
			ware_fabricdispatchheader.intBulkDispatchNoYear, ";
	$sql .= "ware_fabricdispatchheader.dtmdate";	
	//echo $sql;
	
		 $result = $db->RunQuery($sql);
	//	 $row=mysqli_fetch_array($result);
	//	 return $row;
		// $row=mysqli_fetch_array($result);
		// return $row;
		return $result;
}
//----------------------------------------------
?>
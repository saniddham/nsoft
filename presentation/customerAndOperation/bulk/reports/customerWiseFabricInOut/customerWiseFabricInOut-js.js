$(document).ready(function() {
	
  		$("#frmOrderWiseFabricInOut").validationEngine();
		
		$("#imgSearchItems").live('click',function(){
			loadReport();
		});
		
		
	  $('#chkDate').click(function(){
		  var val = $(this).attr('checked');
		  if(!val)
		  {
			$('#txtFromDate').val('');  
			$('#txtFromDate').attr('disabled','true');
			$('#txtToDate').val('');  
			$('#txtToDate').attr('disabled','true');
		  }
		  else
		  {
			$('#txtFromDate').removeAttr('disabled');
			$('#txtToDate').removeAttr('disabled');
		  }
	  });
		
});//end of ready





//------------------------------------------------------------------------
function loadReport()
{
	if ($('#frmCustomerWiseFabricInOut').validationEngine('validate'))   
    { 
		var url='rptCustomerWiseFabricInOut.php?';
		
		url+='customer='+	$('#frmCustomerWiseFabricInOut #cboCustomer').val();
		url+='&fromDate='+	$('#frmCustomerWiseFabricInOut #txtFromDate').val();
		url+='&toDate='+	$('#frmCustomerWiseFabricInOut #txtToDate').val();
		
		

		window.open(url);
	}
}
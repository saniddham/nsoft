<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];
$intUser  = $_SESSION["userId"];

//include  	"{$backwardseperator}dataAccess/permisionCheck.inc";
include  	"{$backwardseperator}dataAccess/Connector.php";


$programName='Customer Wise Fabric IN-OUT';
$programCode='P0528';

			
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Order Wise Fabric In - Out</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="customerWiseFabricInOut-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../../libraries/calendar/theme.css" />
<script src="../../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body>
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>


<form id="frmCustomerWiseFabricInOut" name="frmCustomerWiseFabricInOut" method="get" action="customerWiseFabricInOut.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutS">
		  <div class="trans_text">Customer Wise Fabric IN - OUT</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="34%" height="27" class="normalfnt">Customer</td>
            <td width="51%"><select name="cboCustomer" id="cboCustomer" style="width:180px"  class="txtText" >
              <option value=""></option>
              <?php
					$sql = "SELECT DISTINCT 
							trn_orderheader.intCustomer,
							mst_customer.strName
							FROM
							trn_orderheader
							Inner Join mst_customer ON trn_orderheader.intCustomer = mst_customer.intId 
							ORDER BY mst_customer.strName ASC";
							$pp=$sql;
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intCustomer']==$x_custId)
						echo "<option value=\"".$row['intCustomer']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intCustomer']."\">".$row['strName']."</option>";	
					}
				?>
              </select></td>
            <td width="5%">&nbsp;</td>
            <td width="10%"></td>
          </tr>
<tr>
            <td height="27" class="normalfnt">Date</td>
            <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10%" class="normalfnt"><input name="chkDate" type="checkbox" id="chkDate"  /></td>
                <td width="12%" class="normalfnt">From</td>
                <td width="35%"><input name="dtDate" type="text" disabled="disabled"  class="txtbox" id="txtFromDate" style="width:80px;"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                <td width="8%" class="normalfnt">To</td>
                <td width="35%"><input name="dtDate2" type="text" disabled="disabled"  class="txtbox" id="txtToDate" style="width:80px;"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              </tr>
            </table></td>
          </tr>          <tr>
            <td colspan="4"><table width="100%" border="0" cellpadding="0" cellspacing="0"><tr>
              <td width="30%" height="27" class="normalfnt"></td>
              <td height="27" align="center" ><img src="../../../../../images/Treport.jpg" width="92" height="24" class="mouseover" id="imgSearchItems" name="imgSearchItems"  /></td>
              <td width="26%" height="27" class="normalfnt">&nbsp;</td>
              <td width="15%" height="27" class="normalfnt">&nbsp;</td>
              </tr></table></td>
          </tr>
        </table></td>
      </tr>
<tr>
      </tr>      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="92%">&nbsp;</td>
            <td width="8%">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td></td>
      </tr>
  </table></td></table></div></div>
</form>
	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>


</body>
</html>

<?php

$thisFilePath =  $_SERVER['PHP_SELF'];
?>
<style type="text/css">

.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntWhite {
	font-family: Verdana;
	font-size: 11px;
	color: #FFF;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.sampleNo{
	color: #039; font-weight: bold; font-size: 12px; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;	
}
.part{
	color: #096CBD;
	font-weight: bold;
	font-size: 13px;
	font-family: "Comic Sans MS", cursive;
}
.tableBorder_allRound{
	
	border: 1px solid #CCCCCC;
	border-radius-bottomright:10px;
	border-radius-bottomleft:10px;
	border-radius-topright:10px;
	border-radius-topleft:10px;
}
</style>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Marketing Confirm Email</title>
<style type="text/css">
.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntWhite {
	font-family: Verdana;
	font-size: 11px;
	color: #FFF;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.sampleNo{
	color: #039; font-weight: bold; font-size: 12px; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;	
}
.part{
	color: #096CBD;
	font-weight: bold;
	font-size: 12px;
	font-family: "Comic Sans MS", cursive;
}
.tableBorder_allRound{
	
	border: 1px solid #CCCCCC;
	-moz-border-radius-bottomright:10px;
	-moz-border-radius-bottomleft:10px;
	-moz-border-radius-topright:10px;
	-moz-border-radius-topleft:10px;
}
.tblBorder{
	border-top:solid;
	border-top-width:thin;
	border-bottom:solid;
	border-bottom-width:thin;
	border-left:solid;
	border-left-width:thin;
	border-right:solid;
	border-right-width:thin;
	border-color:#dce9f9;
}
</style>
</head>

<body>
<table  width="595" border="0" cellspacing="0" cellpadding="2" class="tblBorder">
  <tr>
    <td colspan="3" class="normalfnt" style="color:#3E437D">&nbsp;</td>
  </tr>
  <tr>
    <td width="120" class="normalfnt">Dear <strong><?php echo $creator;?></strong>,</td>
    <td width="454">&nbsp;</td>
    <td width="5">&nbsp;</td>
  </tr>
   <tr>
    <td width="120"></td>
    <td width="454" class="normalfnt" style="color:#00C"><b><?php echo $con_orderNo;?></b></td>
    <td width="5">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Above Purchase Order  has been Rejected.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Sample Numbers in this PO:</td>
    <td>&nbsp;</td>
  </tr>
  <?php 
		 $sql_sampleOrder	= "SELECT GROUP_CONCAT(DISTINCT(CONCAT(trn_orderdetails.intSampleNo,'-',trn_orderdetails.intSampleYear,'-',trn_orderdetails.intRevisionNo))) 
							AS concat_sample_order
							FROM 
							trn_orderdetails
							WHERE
							trn_orderdetails.intOrderNo = '$orderNo' AND
							trn_orderdetails.intOrderYear = '$orderYear'
							GROUP BY
							trn_orderdetails.intSampleNo,
							trn_orderdetails.intSampleYear";
							
		$result_sampleOrder	= $db->RunQuery($sql_sampleOrder);

  		while($row_sampleOrder = mysqli_fetch_array($result_sampleOrder))
		{
  ?>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><?php echo $row_sampleOrder['concat_sample_order']?></td>
    <td>&nbsp;</td>
  </tr>
  <?php }?>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">click <a  href="<?php echo $url?>">here</a> to View this <span class="normalfnt1">Purchase Order</span>.</td>
    <td>&nbsp;</td>
  </tr>
 
  <tr>
    <td height="50" colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="normalfnt">
        <tr>
                                                </tr>
    </table></td>
  </tr>
   <tr>
  <td colspan="3">
  <?php
			
			$sqlApprovedBy	= " SELECT
								trn_orderheader_approvedby.dtApprovedDate AS dtApprovedDate ,
								sys_users.strUserName as UserName,
								trn_orderheader_approvedby.intApproveLevelNo AS intApproveLevelNo
								FROM
								trn_orderheader_approvedby
								INNER JOIN sys_users ON trn_orderheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								trn_orderheader_approvedby.intOrderNo = '$orderNo' AND 
								trn_orderheader_approvedby.intYear = '$orderYear'";
			$resultA		= $db->RunQuery($sqlApprovedBy);
			include "presentation/report_approvedBy_details.php";
 	?>
  </td>
  </tr>
  
  <tr>
    <td class="normalfnt">Thanks,</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="normalfnt"><strong>admin</strong><br />
    ...................</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="normalfnt">(This is a<strong> <span style="color:#0000FF">Nsoft</span> </strong>system generated email.)</td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>

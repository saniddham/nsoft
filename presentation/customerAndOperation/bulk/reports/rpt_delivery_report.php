<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
ini_set('display_errors',0);

include_once "class/cls_commonFunctions_get.php";
include_once "class/customerAndOperation/bulk/reports/cls_delivery_report_get.php";
 
$obj_common		= new cls_commonFunctions_get($db);
$obj_disp_get	= new cls_delivery_report_get($db);

$location		= $_REQUEST["location"];
$dateFrom		= $_REQUEST["dtFrom"];
$dtFH			= str_pad($_REQUEST["dtFH"],2,0, STR_PAD_LEFT);
$dtFM			= str_pad($_REQUEST["dtFM"],2,0, STR_PAD_LEFT);

$dateTo			= $_REQUEST["dtTo"];
$dtTH			= str_pad($_REQUEST["dtTH"],2,0, STR_PAD_LEFT);
$dtTM			= str_pad($_REQUEST["dtTM"],2,0, STR_PAD_LEFT);

$dateFrom		= $dateFrom." ".$dtFH.':'.$dtFM.':'.'00';
$dateTo			= $dateTo." ".$dtTH.':'.$dtTM.':'.'00';

$res			= $obj_common->loadLocationName($location,'RunQuery');
$row 			= mysqli_fetch_array($res);
$rptLocationName= $row['strName'];
?>
<title>Customer Delivery report</title>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:272px;
	top:511px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}


 table .bordered{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}


.bordered {
	border: solid #ccc 1px;
/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}

.bordered tr:hover {
    background: #fbf8e9;
/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
    transition: all 0.1s ease-in-out;     
}    
    
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);
/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {
/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
    border-radius: 6px 0 0 0;
}

.bordered th:last-child {
/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
    border-radius: 0 6px 0 0;
}

.bordered th:only-child{
/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {
/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {
/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
    border-radius: 0 0 6px 0;
}



.odd{background-color:#F5F5F5;}
.even{background-color:#FFFFFF;}
.mouseover {
	cursor: pointer;
}


.txtNumber{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:right;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.txtText{
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align:left;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}

.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}

.normalfntGrey {
	font-family: Verdana;
	font-size: 11px;
	color: #999;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:right;
}





</style>
</head>
</head>

<body><form id="frmFabDispatchReport" name="frmFabDispatchReport" method="post" action="rptFabricDispatchNote.php">
<table width="1200" cellpadding="0" cellspacing="0" align="center">
<tr>
<td colspan="3"></td>
</tr>
<tr>
    <td colspan="9" align="center" ><table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="tophead"><table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td width="8%" class="normalfnt"><img style="vertical-align:" src="http://accsee.com/screenline.jpg"  /></td>
            <td align="center" valign="top" width="75%" class="topheadBLACK"><?php
/*	 $SQL = "SELECT
*
FROM
mst_companies
Inner Join mst_country ON mst_companies.intCountryId = mst_country.intCountryID
WHERE
mst_companies.intId =  '1'
;";
*/	
	//$result 			= $db->RunQuery($SQL);
	$result				= $obj_common->loadLoctionComanyDetails($location,'RunQuery');
	$row 				= mysqli_fetch_array($result);
	$companyName		= $row["strName"];
	$companyAddress1	= $row["strAddress"];
	$companyStreet		= $row["strStreet"];
	$companyCity		= $row["strCity"];
	$companyCountry		= $row["strCountryName"];
	$companyZipCode		= $row["strZip"];
	$companyPhone		= "(".$companyZipCode.")".$row["strPhoneNo"];
	$companyFax		 	= "(".$companyZipCode.")".$row["strFaxNo"];
	$companyEmail		= $row["strEmail"];
	$companyWeb		 	= $row["strWebSite"];
	
	?>
              <b><?php echo $companyName.""; ?></b>
              <p class="normalfntMid">
                <?php echo $companyAddress1." ".$companyStreet." ".$companyCity." ".$companyCountry."."."<br><b>Tel : </b>".$companyPhone." <b>Fax : </b>".$companyFax." <br><b>E-Mail : </b>".$companyEmail." <b>Web : </b>".$companyWeb;?></p></td>
            <td width="9%" class="tophead"><img src="http://accsee.com/nsoft_logo.png" style="vertical-align:"  /></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ></div>
<table width="1300" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td colspan="2">
  <table width="100%">
  <tr>
    <td colspan="8" align="center"><span style="background-color:#FFF"><div align="left"></div>CUSTOMER DELIVERY REPORT</span></td>
    </tr>
  <tr>
    <td width="2%">&nbsp;</td>
    <td width="6%"><span class="normalfnt">Location</span></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="33%"><span class="normalfnt"><?php echo $rptLocationName; ?></span></td>
    <td width="6%" class="normalfnt">Date Range</td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="51%"><span class="normalfnt">From : (<?php echo $dateFrom; ?>)  To : (<?php echo $dateTo; ?>)</span></td>
  </tr>
  
  </table>
  </td>
</tr>
<tr>
  <td colspan="2">
    <table width="100%">
      <tr>
        <td>
          
          <table width="100%"  class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <thead>
              <tr class="">
                <th width="12%" >Customer</th>
                <th width="6%" >Marketer</th>
                <th width="3%" >Style No</th>
                <th width="5%" >Graphic No</th>
                <th width="6%" >Colour</th>
                <th width="3%" >Part</th>
                <th width="4%" >Brand</th>
                <th width="4%" >Technique Group</th>
                <th width="4%" >Order Qty</th>
                <th width="3%" >Tot In Qty</th>
                <th width="4%" >Today Disp Qty</th>
                <th width="4%" >Tot Disp Qty</th>
                <th width="4%" >Today PD</th>
                <th width="4%" >Today PD%</th>
                <th width="3%" >Tot PD</th>
                <th width="3%" >Tot PD%</th>
                <th width="4%" >Total good Qty</th>
                <th width="5%" >Amount</th>
                <th width="5%" >HeatSeal Amount</th>
                <th width="5%" >Balance</th>
                <th width="4%" >Order NO</th>
                <th width="6%" >Customer PO</th>
                <th width="8%" >Sales Order NO</th>
                </tr>
              </thead>
            <tbody>
              <?php 
			$result1=$obj_disp_get->get_customers_delivery_rpt_result($location,$dateFrom,$dateTo);
			while($row1=mysqli_fetch_array($result1))
			{
				$customer		= $row1['customer'];
				$marketer		= $row1['marketer'];
				$style			= $row1['style'];
				$graphic	 	= $row1['graphic'];
				$color			= $row1['color'];
				$part			= $row1['part'];
				$brand			= $row1['brand'];
                $technique		= $row1['technique'];
				$orderQty		= $row1['totOrderQty'];
				$totInQty 		= $row1["totInQty"];
				$todayDispQty	= $row1["todayDispQty"];
				$totDispQty		= $row1['totDispQty'];
				$balQty	 		= $totInQty-$totDispQty;
				$orderNo	 	= $row1['intOrderNo'] . '/'. $row1['intOrderYear'];
				$customerPO		= $row1['customerPO'];
				$salesOrderNo	= $row1['strSalesOrderNo'];
				
				$todayPD		 =	$row1['todayPD'];
				$todayPG         =  $row1['todayPG'];
				$totalGoodAmount =  $row1['totalGoodAmount'];
                $soType          =  $row1['SO_TYPE'];
                if ($row['SO_TYPE'] != 0 ) {
                    unset($row1['sticker_price']);
                }
                $stickerPrice    =  $row1['sticker_price'];
                $dblPrice = $row1['dbl_price'];
				$todayPD_prec	 =	$row1['todayPD_prec'];
				$totPD			 =	$row1['totPD'];
				$totPD_prec		 =	$row1['totPD_prec'];
				
				
	?>
    <tr>
        <td bgcolor="#FFFFFF" class="normalfnt" nowrap><?php echo $customer; ?></td>
        <td bgcolor="#FFFFFF" class="normalfnt" nowrap><?php echo $marketer; ?></td>
        <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $style; ?></td>
        <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $graphic; ?></td>
        <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $color; ?></td>
        <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $part; ?></td>
        <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $brand; ?></td>
        <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $technique; ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><?php echo number_format($orderQty); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><?php echo number_format($totInQty); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><?php echo number_format($todayDispQty); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><?php echo number_format($totDispQty); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><?php echo number_format($todayPD); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><?php echo number_format($todayPD_prec,2); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><?php echo number_format($totPD); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><?php echo number_format($totPD_prec,2); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><?php echo number_format($todayPG,2); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><?php echo number_format($totalGoodAmount,2); ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><?php
            $heatSealAmount = 0;
            if (isset($stickerPrice)){
                $heatSealAmount = ($dblPrice -$stickerPrice)*$todayPG;
                echo number_format($heatSealAmount);
            } else {
                echo 0;
            }
             ?></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><?php echo number_format($balQty); ?></td>
        <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $orderNo; ?></td>
        <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $customerPO; ?></td>
        <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $salesOrderNo; ?></td>
        <?php 
			$tot_OrderQty 	+=$orderQty;
			$tot_InQty	 	+=$totInQty;
			$tot_DispQty		+=$totDispQty;
			$tot_todayDispQty	+=$todayDispQty;
			$tot_BalQty		+=$balQty;
			$tot_GoodQty +=  $todayPG;
			$sum_GoodAmount += $totalGoodAmount;
            $tot_HeatSealAmount 	+= $heatSealAmount;
		?>
    </tr>
    <?php
			}
	?>
    <tr>
        <td bgcolor="#FFFFFF" class="normalfntRight">&nbsp;</td>
        <td bgcolor="#FFFFFF" class="normalfntRight" colspan="7"><b>Total</b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo number_format($tot_OrderQty); ?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo number_format($tot_InQty); ?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo number_format($tot_todayDispQty); ?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo number_format($tot_DispQty); ?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight">&nbsp;</td>
        <td bgcolor="#FFFFFF" class="normalfntRight">&nbsp;</td>
        <td bgcolor="#FFFFFF" class="normalfntRight">&nbsp;</td>
        <td bgcolor="#FFFFFF" class="normalfntRight">&nbsp;</td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo number_format($tot_GoodQty); ?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo number_format($sum_GoodAmount); ?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo number_format($tot_HeatSealAmount); ?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight"><b><?php echo number_format($tot_BalQty); ?></b></td>
        <td bgcolor="#FFFFFF" class="normalfntRight" colspan="3"></td>
         </tr>
               </tbody>
          </table>        </td>
        </tr>
      
      </table>
    </td>
</tr>

 

<tr height="40">
  <td width="317" align="center" class="normalfnt" >&nbsp;</td>
  <td width="873" align="center" class="normalfntRight">&nbsp;</td>
</tr>
</table>
</div>        
</form>
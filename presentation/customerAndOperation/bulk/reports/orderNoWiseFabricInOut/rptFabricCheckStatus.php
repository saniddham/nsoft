<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
ini_set('display_errors',0);
include_once "libraries/jqgrid2/inc/jqgrid_dist.php";

$approveLevel = (int)getMaxApproveLevel();

$sql = "select * from(SELECT DISTINCT 
					tb1.intOrderNo,
					tb1.intOrderYear,
					tb1.strStyleNo,
					tb1.strGraphicNo,
					tb2.dtDate, 
					
					(SELECT
						Sum(trn_orderdetails.intQty) 
						FROM 
						trn_orderdetails 
						WHERE  
						trn_orderdetails.intOrderNo = tb1.intOrderNo AND 
						trn_orderdetails.intOrderYear = tb1.intOrderYear AND 
						trn_orderdetails.strStyleNo = tb1.strStyleNo AND 
						trn_orderdetails.strGraphicNo = tb1.strGraphicNo
					) as orderQty,	
					
					(SELECT
						Sum(ware_bulkallocation_header.int25PlanGarmentQty)
						FROM
						ware_bulkallocation_header
						Inner Join trn_orderdetails ON ware_bulkallocation_header.intOrderNo = trn_orderdetails.intOrderNo AND ware_bulkallocation_header.intOrderYear = trn_orderdetails.intOrderYear AND ware_bulkallocation_header.intSalesOrderId = trn_orderdetails.intSalesOrderId
						WHERE  
						trn_orderdetails.intOrderNo = tb1.intOrderNo AND 
						trn_orderdetails.intOrderYear = tb1.intOrderYear AND 
						trn_orderdetails.strStyleNo = tb1.strStyleNo AND 
						trn_orderdetails.strGraphicNo = tb1.strGraphicNo
					) as SampleQty,	
					
					(SELECT
						Sum(ware_fabricreceiveddetails.dblQty)
						FROM
						ware_fabricreceiveddetails
						Inner Join ware_fabricreceivedheader ON ware_fabricreceiveddetails.intFabricReceivedNo = ware_fabricreceivedheader.intFabricReceivedNo AND ware_fabricreceiveddetails.intFabricReceivedYear = ware_fabricreceivedheader.intFabricReceivedYear
						Inner Join trn_orderdetails ON ware_fabricreceivedheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricreceivedheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_fabricreceiveddetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
						WHERE
						ware_fabricreceivedheader.intStatus =  '1' AND 
						trn_orderdetails.intOrderNo = tb1.intOrderNo AND 
						trn_orderdetails.intOrderYear = tb1.intOrderYear AND 
						trn_orderdetails.strStyleNo = tb1.strStyleNo AND 
						trn_orderdetails.strGraphicNo = tb1.strGraphicNo
					) as FabricRcvQty,	
					
					round(((SELECT 					sum(ware_bulkallocation_details.dbl25AllocatedQty+ware_bulkallocation_details.dbl75AllocatedQty) 					
					FROM
					ware_bulkallocation_details
					Inner Join mst_item ON ware_bulkallocation_details.intItemId = mst_item.intId
					Inner Join trn_orderdetails ON ware_bulkallocation_details.intOrderNo = trn_orderdetails.intOrderNo AND ware_bulkallocation_details.intOrderYear = trn_orderdetails.intOrderYear AND ware_bulkallocation_details.intSalesOrderId = trn_orderdetails.intSalesOrderId
					WHERE
					mst_item.intMainCategory =  '1' AND 
					trn_orderdetails.intOrderNo = tb1.intOrderNo AND 
					trn_orderdetails.intOrderYear = tb1.intOrderYear AND 
					trn_orderdetails.strStyleNo = tb1.strStyleNo AND 
					trn_orderdetails.strGraphicNo = tb1.strGraphicNo
					)/(select sum(trn_sample_item_consumption.dblQty*trn_orderdetails.intQty) 	FROM
						trn_orderdetails
						Inner Join trn_sample_item_consumption ON trn_sample_item_consumption.intSampleNo = trn_orderdetails.intSampleNo AND trn_sample_item_consumption.intSampleYear = trn_orderdetails.intSampleYear AND trn_sample_item_consumption.intRevisionNo = trn_orderdetails.intRevisionNo AND trn_sample_item_consumption.strCombo = trn_orderdetails.strCombo AND trn_sample_item_consumption.strPrintName = trn_orderdetails.strPrintName 
Inner Join mst_item ON mst_item.intId = trn_sample_item_consumption.intItem
						WHERE
						mst_item.intMainCategory =  '1' AND 
						trn_orderdetails.intOrderNo = tb1.intOrderNo AND 
						trn_orderdetails.intOrderYear =tb1.intOrderYear AND 
						trn_orderdetails.strStyleNo = tb1.strStyleNo AND 
						trn_orderdetails.strGraphicNo = tb1.strGraphicNo))*100,4)
					 as allocatedRM,	
					
					(round(((SELECT 					sum(ware_issuedetails.dblQty-ware_issuedetails.dblReturnQty) 					
					FROM
					ware_issuedetails
					Inner Join ware_issueheader ON ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo AND ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear
					Inner Join trn_orderdetails ON ware_issuedetails.strOrderNo = trn_orderdetails.intOrderNo AND ware_issuedetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_issuedetails.strStyleNo = trn_orderdetails.intSalesOrderId
					Inner Join mst_item ON ware_issuedetails.intItemId = mst_item.intId
					WHERE
					ware_issueheader.intStatus =  '1' AND 
					mst_item.intMainCategory =  '1' AND 
					trn_orderdetails.intOrderNo = tb1.intOrderNo AND 
					trn_orderdetails.intOrderYear = tb1.intOrderYear AND 
					trn_orderdetails.strStyleNo = tb1.strStyleNo AND 
					trn_orderdetails.strGraphicNo = tb1.strGraphicNo
					)/(select sum(trn_sample_item_consumption.dblQty*trn_orderdetails.intQty) 	FROM
						trn_orderdetails
						Inner Join trn_sample_item_consumption ON trn_sample_item_consumption.intSampleNo = trn_orderdetails.intSampleNo AND trn_sample_item_consumption.intSampleYear = trn_orderdetails.intSampleYear AND trn_sample_item_consumption.intRevisionNo = trn_orderdetails.intRevisionNo AND trn_sample_item_consumption.strCombo = trn_orderdetails.strCombo AND trn_sample_item_consumption.strPrintName = trn_orderdetails.strPrintName 
Inner Join mst_item ON mst_item.intId = trn_sample_item_consumption.intItem
						WHERE
						mst_item.intMainCategory =  '1' AND 
						trn_orderdetails.intOrderNo = tb1.intOrderNo AND 
						trn_orderdetails.intOrderYear =tb1.intOrderYear AND 
						trn_orderdetails.strStyleNo = tb1.strStyleNo AND 
						trn_orderdetails.strGraphicNo = tb1.strGraphicNo))*100,4) 
					)	as issuedRM,	
					
					round((SELECT
						Sum(ware_fabricdispatchdetails.dblGoodQty)
						FROM
						ware_fabricdispatchdetails
						Inner Join ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
						Inner Join trn_orderdetails ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
						WHERE
						ware_fabricdispatchheader.intStatus =  '1' AND 
						trn_orderdetails.intOrderNo = tb1.intOrderNo AND 
						trn_orderdetails.intOrderYear = tb1.intOrderYear AND 
						trn_orderdetails.strStyleNo = tb1.strStyleNo AND 
						trn_orderdetails.strGraphicNo = tb1.strGraphicNo
					),4) as FabricDispGoodQty,	
					
					round((SELECT
						Sum(ware_fabricdispatchdetails.dblSampleQty)
						FROM
						ware_fabricdispatchdetails
						Inner Join ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
						Inner Join trn_orderdetails ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
						WHERE
						ware_fabricdispatchheader.intStatus =  '1' AND 
						trn_orderdetails.intOrderNo = tb1.intOrderNo AND 
						trn_orderdetails.intOrderYear = tb1.intOrderYear AND 
						trn_orderdetails.strStyleNo = tb1.strStyleNo AND 
						trn_orderdetails.strGraphicNo = tb1.strGraphicNo
					),4) as FabricDispSampQty,	
					
					round((SELECT
						Sum(ware_fabricdispatchdetails.dblEmbroideryQty)
						FROM
						ware_fabricdispatchdetails
						Inner Join ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
						Inner Join trn_orderdetails ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
						WHERE
						ware_fabricdispatchheader.intStatus =  '1' AND 
						trn_orderdetails.intOrderNo = tb1.intOrderNo AND 
						trn_orderdetails.intOrderYear = tb1.intOrderYear AND 
						trn_orderdetails.strStyleNo = tb1.strStyleNo AND 
						trn_orderdetails.strGraphicNo = tb1.strGraphicNo
					),4) as FabricDispEmdQty,	
					
					round((SELECT
						Sum(ware_fabricdispatchdetails.dblPDammageQty)
						FROM
						ware_fabricdispatchdetails
						Inner Join ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
						Inner Join trn_orderdetails ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
						WHERE
						ware_fabricdispatchheader.intStatus =  '1' AND 
						trn_orderdetails.intOrderNo = tb1.intOrderNo AND 
						trn_orderdetails.intOrderYear = tb1.intOrderYear AND 
						trn_orderdetails.strStyleNo = tb1.strStyleNo AND 
						trn_orderdetails.strGraphicNo = tb1.strGraphicNo
					),4) as FabricDispPdQty,	
					
					
					round((SELECT
						Sum(ware_fabricdispatchdetails.dblFdammageQty)
						FROM
						ware_fabricdispatchdetails
						Inner Join ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
						Inner Join trn_orderdetails ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
						WHERE
						ware_fabricdispatchheader.intStatus =  '1' AND 
						trn_orderdetails.intOrderNo = tb1.intOrderNo AND 
						trn_orderdetails.intOrderYear = tb1.intOrderYear AND 
						trn_orderdetails.strStyleNo = tb1.strStyleNo AND 
						trn_orderdetails.strGraphicNo = tb1.strGraphicNo
					),4) as FabricDispFdQty,	
			
					round((SELECT
						Sum(ware_fabricdispatchdetails.dblCutRetQty)
						FROM
						ware_fabricdispatchdetails
						Inner Join ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
						Inner Join trn_orderdetails ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
						WHERE
						ware_fabricdispatchheader.intStatus =  '1' AND 
						trn_orderdetails.intOrderNo = tb1.intOrderNo AND 
						trn_orderdetails.intOrderYear = tb1.intOrderYear AND 
						trn_orderdetails.strStyleNo = tb1.strStyleNo AND 
						trn_orderdetails.strGraphicNo = tb1.strGraphicNo
					),4) as cutRetQty,	
					
					round((SELECT
						Sum(ware_fabricdispatchdetails.dblGoodQty+ware_fabricdispatchdetails.dblSampleQty+ware_fabricdispatchdetails.dblEmbroideryQty+ware_fabricdispatchdetails.dblPDammageQty+ware_fabricdispatchdetails.dblFdammageQty+ware_fabricdispatchdetails.dblCutRetQty)
						FROM
						ware_fabricdispatchdetails
						Inner Join ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
						Inner Join trn_orderdetails ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
						WHERE
						ware_fabricdispatchheader.intStatus =  '1' AND 
						trn_orderdetails.intOrderNo = tb1.intOrderNo AND 
						trn_orderdetails.intOrderYear = tb1.intOrderYear AND 
						trn_orderdetails.strStyleNo = tb1.strStyleNo AND 
						trn_orderdetails.strGraphicNo = tb1.strGraphicNo
					),4) as FabricDispTotQty,	
					
					round(((SELECT
						Sum(ware_fabricdispatchdetails.dblGoodQty+ware_fabricdispatchdetails.dblSampleQty+ware_fabricdispatchdetails.dblEmbroideryQty+ware_fabricdispatchdetails.dblPDammageQty+ware_fabricdispatchdetails.dblFdammageQty+ware_fabricdispatchdetails.dblCutRetQty)
						FROM
						ware_fabricdispatchdetails
						Inner Join ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
						Inner Join trn_orderdetails ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
						WHERE
						ware_fabricdispatchheader.intStatus =  '1' AND 
						trn_orderdetails.intOrderNo = tb1.intOrderNo AND 
						trn_orderdetails.intOrderYear = tb1.intOrderYear AND 
						trn_orderdetails.strStyleNo = tb1.strStyleNo AND 
						trn_orderdetails.strGraphicNo = tb1.strGraphicNo
					)/(SELECT
						Sum(ware_fabricreceiveddetails.dblQty)
						FROM
						ware_fabricreceiveddetails
						Inner Join ware_fabricreceivedheader ON ware_fabricreceiveddetails.intFabricReceivedNo = ware_fabricreceivedheader.intFabricReceivedNo AND ware_fabricreceiveddetails.intFabricReceivedYear = ware_fabricreceivedheader.intFabricReceivedYear
						Inner Join trn_orderdetails ON ware_fabricreceivedheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricreceivedheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_fabricreceiveddetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
						WHERE
						ware_fabricreceivedheader.intStatus =  '1' AND 
						trn_orderdetails.intOrderNo = tb1.intOrderNo AND 
						trn_orderdetails.intOrderYear = tb1.intOrderYear AND 
						trn_orderdetails.strStyleNo = tb1.strStyleNo AND 
						trn_orderdetails.strGraphicNo = tb1.strGraphicNo
					)*100
					),2) as FabricDispProgress,	
					
					(''
					) as InvoiceValue,	
																			if(tb2.intStatus=1,'Approved',if(tb2.intStatus=0,'Rejected',if(tb2.intStatus=-10,'Revised','Pending'))) as Status 

					FROM
					trn_orderdetails as tb1
					Inner Join trn_orderheader as tb2 ON tb1.intOrderNo = tb2.intOrderNo AND tb1.intOrderYear = tb2.intOrderYear )  as t where 1=1
						";
					  //  	echo $sql;
$col = array();

//Order No
$col["title"] = "Order No"; // caption of column
$col["name"] = "intOrderNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "left";

$reportLink  = "../../placeOrder/listing/rptBulkOrder.php?orderNo={intOrderNo}&orderYear={intOrderYear}";
$col['link']	= $reportLink;	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag


$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Order Year"; // caption of column
$col["name"] = "intOrderYear"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Date"; // caption of column
$col["name"] = "dtDate"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Graphic No"; // caption of column
$col["name"] = "strGraphicNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Style No"; // caption of column
$col["name"] = "strStyleNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Order Qty"; // caption of column
$col["name"] = "orderQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;


//Order No
$col["title"] = "Status"; // caption of column
$col["name"] = "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Sample Qty"; // caption of column
$col["name"] = "SampleQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Fabric Rcv Qty"; // caption of column
$col["name"] = "FabricRcvQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Allocated RM%"; // caption of column
$col["name"] = "allocatedRM"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Issued RM%"; // caption of column
$col["name"] = "issuedRM"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Fabric Disp Good Qty"; // caption of column
$col["name"] = "FabricDispGoodQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Fabric Disp Samp Qty"; // caption of column
$col["name"] = "FabricDispSampQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Fabric Disp(Emd)Qty"; // caption of column
$col["name"] = "FabricDispEmdQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Fabric Disp(Pd)Qty"; // caption of column
$col["name"] = "FabricDispPdQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Fabric Disp(Fd)Qty"; // caption of column
$col["name"] = "FabricDispFdQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Cut Returned Qty"; // caption of column
$col["name"] = "cutRetQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;


//Order No
$col["title"] = "Fabric Disp(Tot)Qty"; // caption of column
$col["name"] = "FabricDispTotQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Fabric Disp Progress"; // caption of column
$col["name"] = "FabricDispProgress"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Invoice Value"; // caption of column
$col["name"] = "InvoiceValue"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;



$jq = new jqgrid('',$db);

$grid["caption"] 		= "Fabric Check Status Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'intOrderNo'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);




$out = $jq->render("list1");
?>

<head>
	<?php 
		include "include/listing.html";
	?>
</head>
<body>	
<form id="frmlisting" name="frmlisting" method="post" action="">

        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
</form>
</body>
<?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(ware_fabricreceivedheader.intApproveLevels) AS appLevel
			FROM ware_fabricreceivedheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
?>


$(document).ready(function() {
	
  		$("#frmOrderWiseFabricInOut").validationEngine();
		
		$("#frmOrderWiseFabricInOut #cboOrderNo").live('change',function(){
		//	submitForm();
		});
		
		$("#frmOrderWiseFabricInOut #imgSearchItems").live('click',function(){
			loadReport();
		});
		
		$("#frmOrderWiseFabricInOut #butNew").live('click',function(){
			window.location.href = "orderNoWiseFabricInOut.php";
		});
		
		$("#frmOrderWiseFabricInOut .search").live('change',loadAllCombo);
		
});//end of ready





//------------------------------------------------------------------------
function submitForm(){
	window.location.href = "orderNoWiseFabricInOut.php?orderNo="+$('#cboOrderNo').val()
						+'&salesOrderNo='+$('#frmOrderWiseFabricInOut #cboSalesOrderNo').val()
						+'&poNo='+$('#frmOrderWiseFabricInOut #cboPONo').val()
						+'&customer='+$('#frmOrderWiseFabricInOut #cboCustomer').val()
						+'&orderYear='+$('#frmOrderWiseFabricInOut #cboOrderYear').val()
}
//------------------------------------------------------------------------
function loadAllCombo()
{
	//######################
	//####### get values ###
	//######################
	var year 		= $('#frmOrderWiseFabricInOut #cboOrderYear').val();
	var graphicNo 	= $('#frmOrderWiseFabricInOut #cboGraphicNo').val();
	var styleId 	= $('#frmOrderWiseFabricInOut #cboStyle').val();
	var customerPONo= $('#frmOrderWiseFabricInOut #cboPONo').val();
	var orderNo		= $('#frmOrderWiseFabricInOut #cboOrderNo').val();
	var customerId 	= $('#frmOrderWiseFabricInOut #cboCustomer').val();
	
	//######################
	//####### create url ###
	//######################
	var url		="orderNoWiseFabricInOut-db-get.php?requestType=loadAllComboDetails";
		url	   +="&year="+			year;	
		url	   +="&graphicNo="+		graphicNo;
		url	   +="&styleId="+		styleId;
		url	   +="&customerPONo="+	customerPONo;
		url	   +="&orderNo="+		orderNo;
		url	   +="&customerId="+	customerId;
	
	//######################
	//####### send data  ###
	//######################
	$.ajax({url:url,
			async:false,
			dataType:'json',
			success:function(json){
					$('#frmOrderWiseFabricInOut #cboStyle').html(json.styleNo);
					$('#frmOrderWiseFabricInOut #cboGraphicNo').html(json.graphicNo);
					$('#frmOrderWiseFabricInOut #cboPONo').html(json.customerPoNo);
					$('#frmOrderWiseFabricInOut #cboOrderNo').html(json.orderNo);
					$('#frmOrderWiseFabricInOut #cboCustomer').html(json.customer);
			}
			});
}
//------------------------------------------------------------------------
function loadReport()
{
	if ($('#frmOrderWiseFabricInOut').validationEngine('validate'))   
    { 
		var url='rptOrderNoWiseFabricInOut.php?';
		
		url+='orderNo='+	$('#frmOrderWiseFabricInOut #cboOrderNo').val();
		url+='&orderYear='+	$('#frmOrderWiseFabricInOut #cboOrderYear').val();
		
		if($('#frmOrderWiseFabricInOut #chkColor').attr('checked'))
		url+='&option=1';
		else if($('#frmOrderWiseFabricInOut #chkCutNo').attr('checked'))
		url+='&option=2';
		else if($('#frmOrderWiseFabricInOut #chkSize').attr('checked'))
		url+='&option=3';

		window.open(url);
	}
}
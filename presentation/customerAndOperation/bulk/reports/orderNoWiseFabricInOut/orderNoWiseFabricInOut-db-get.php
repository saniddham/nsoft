<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once "{$backwardseperator}class/customerAndOperation/cls_textile_stores.php";
	
	$programName='Order Wise Fabric IN-OUT';
	$programCode='P0527';
	
	/////////// type of print load part /////////////////////
if($requestType=='loadAllComboDetails')
	{
		$year				= $_REQUEST['year'];
		$graphicNo			= $_REQUEST['graphicNo'];
		$styleId			= $_REQUEST['styleId'];
		$customerPONo		= $_REQUEST['customerPONo'];
		$orderNo			= $_REQUEST['orderNo'];
		$customerId			= $_REQUEST['customerId'];
		$locationFlag		= 0;
		$companyFlag		= 0;
		echo loadAllComboDetails($year,$graphicNo,$styleId,$customerPONo,$orderNo,$customerId,$locationFlag,$location,$companyFlag,$company); 	
	}
	
//--------------------------------------------------------------
	function loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size)
{
		global $db;
	 	$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				ware_stocktransactions_fabric.intLocationId =  '$location' AND
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType =  'Received' 
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//--------------------------------------------------------------
	function loadExcessQty($orderNo,$orderYear,$salesOrderId,$partId,$size)
{
		global $db;
	       	$sql = "SELECT
				trn_ordersizeqty.dblQty,
				trn_orderdetails.dblOverCutPercentage,
				trn_orderdetails.dblDamagePercentage
				FROM trn_orderdetails 
				Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_orderdetails.intSalesOrderId =  '$salesOrderId' AND
				trn_orderdetails.intPart =  '$partId' AND 
				trn_ordersizeqty.strSize =  '$size' ";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		//$value=$rows['dblQty']*($rows['dblOverCutPercentage']+$rows['dblDamagePercentage'])/100;
		$value=$rows['dblQty']*($rows['dblOverCutPercentage'])/100;
		return val($value);
}

function loadAllComboDetails($year,$graphicNo,$styleId,$customerPONo,$orderNo,$customerId,$locationFlag,$location,$companyFlag,$company) 	
{
	$obj = new cls_texttile();
	echo $obj->loadAllSearchComboDetails($year,$graphicNo,$styleId,$customerPONo,$orderNo,$customerId,$locationFlag,$location,$companyFlag,$company); 	
}
//------------------------------------------------------------------------------------
?>
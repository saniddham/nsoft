<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];
$intUser  = $_SESSION["userId"];

//include  	"{$backwardseperator}dataAccess/permisionCheck.inc";
include  	"{$backwardseperator}dataAccess/Connector.php";


$programName='Order Wise Fabric IN-OUT';
$programCode='P0527';

$orderNo = $_REQUEST['orderNo'];
$orderYear = $_REQUEST['orderYear'];
//----------------
   $sql = "SELECT
trn_orderheader.strCustomerPoNo,
trn_orderheader.intCustomer
FROM trn_orderheader
WHERE
trn_orderheader.intOrderNo =  '$orderNo' AND
trn_orderheader.intOrderYear =  '$orderYear'"; 
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);

$x_poNo = $row['strCustomerPoNo'];
$x_custId = $row['intCustomer'];
if($x_poNo=='')
$x_poNo=$poNo;
//----------------			
 $sql = "SELECT
Sum(trn_orderdetails.intQty) AS qty, 
trn_orderdetails.intSampleNo,
trn_orderdetails.intSampleYear,
trn_orderdetails.strCombo,
trn_orderdetails.strPrintName,
trn_orderdetails.intRevisionNo , 
trn_orderdetails.strStyleNo , 
trn_orderdetails.strGraphicNo 
FROM trn_orderdetails  
WHERE
trn_orderdetails.intOrderNo='$orderNo' 
AND trn_orderdetails.intOrderYear='$orderYear'  
GROUP BY
trn_orderdetails.intOrderNo,
trn_orderdetails.intOrderYear"; 
 // echo $sql;

$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$x_styleNo 	= $row['strStyleNo'];
$x_graphicNo 	= $row['strGraphicNo'];
$x_sampleNo 	= $row['intSampleNo'];
$x_sampleYear 	= $row['intSampleYear'];
$x_revNo 		= $row['intRevisionNo'];
$x_combo 		= $row['strCombo'];
$x_print 		= $row['strPrintName'];
$x_qty = $row['qty'];
//-------------------------			
$mainPage=$backwardseperator."main.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Order Wise Fabric In - Out</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="orderNoWiseFabricInOut-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../../libraries/calendar/theme.css" />
<script src="../../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body>
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>


<form id="frmOrderWiseFabricInOut" name="frmOrderWiseFabricInOut" method="get" action="orderNoWiseFabricInOut.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutS">
		  <div class="trans_text">Order Wise Fabric IN - OUT</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
          <td width="34%" height="27" class="normalfnt">Year<span class="compulsoryRed"> *</span></td>
            <td width="51%"><select name="cboOrderYear" id="cboOrderYear" style="width:70px"  class="validate[required] txtText search" >
                <?php
					$sql = "SELECT DISTINCT
							trn_orderheader.intOrderYear
							FROM trn_orderheader
							ORDER BY
							trn_orderheader.intOrderYear DESC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intOrderYear']==$x_year)
						echo "<option value=\"".$row['intOrderYear']."\" selected=\"selected\">".$row['intOrderYear']."</option>";	
						else
						echo "<option value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";	
					}
				?>
            </select></td>
            <td width="5%">&nbsp;</td>
            <td width="10%">&nbsp;</td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Style No</td>
            <td width="51%"><select name="cboStyle" id="cboStyle" style="width:180px"  class="txtText search" >
              <option value=""></option>
                <?php
					$sql = "SELECT DISTINCT
							trn_orderdetails.strStyleNo
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
							WHERE
							trn_orderheader.intStatus =  '1' 
							ORDER BY trn_orderdetails.strStyleNo ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['strStyleNo']==$x_styleNo)
						echo "<option value=\"".$row['strStyleNo']."\" selected=\"selected\">".$row['strStyleNo']."</option>";	
						else
						echo "<option value=\"".$row['strStyleNo']."\">".$row['strStyleNo']."</option>";	
					}
				?>
              </select></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Graphic No</td>
            <td width="51%"><select name="cboGraphicNo" style="width:180px" id="cboGraphicNo" class="txtText search" >
              <option value=""></option>
                <?php
					$sql = "SELECT DISTINCT
							trn_orderdetails.strGraphicNo
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
							WHERE
							trn_orderheader.intStatus =  '1' 
							ORDER BY trn_orderdetails.strGraphicNo ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['strGraphicNo']==$x_graphicNo)
						echo "<option value=\"".$row['strGraphicNo']."\" selected=\"selected\">".$row['strGraphicNo']."</option>";	
						else
						echo "<option value=\"".$row['strGraphicNo']."\">".$row['strGraphicNo']."</option>";	
					}
				?>
              </select></td>
            <td></td>
            <td></td>
          </tr>          <tr>
          <td height="27" class="normalfnt">Costomer PO</td>
            <td width="51%"><select name="cboPONo" id="cboPONo" style="width:180px"  class="txtText search" >
                <option value=""></option>
                <?php
					$sql = "SELECT DISTINCT
							trn_orderheader.strCustomerPoNo
							FROM trn_orderheader 
							where intStatus=1 
							AND trn_orderheader.strCustomerPoNo <> '' 
							ORDER BY
							trn_orderheader.strCustomerPoNo ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['strCustomerPoNo']==$x_poNo)
						echo "<option value=\"".$row['strCustomerPoNo']."\" selected=\"selected\">".$row['strCustomerPoNo']."</option>";	
						else
						echo "<option value=\"".$row['strCustomerPoNo']."\">".$row['strCustomerPoNo']."</option>";	
					}
				?>
            </select></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Customer</td>
            <td><select name="cboCustomer" id="cboCustomer" style="width:180px"  class="txtText" >
                <option value=""></option>
                <?php
					$sql = "SELECT DISTINCT 
							trn_orderheader.intCustomer,
							mst_customer.strName
							FROM
							trn_orderheader
							Inner Join mst_customer ON trn_orderheader.intCustomer = mst_customer.intId 
							ORDER BY mst_customer.strName ASC";
							$pp=$sql;
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intCustomer']==$x_custId)
						echo "<option value=\"".$row['intCustomer']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intCustomer']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td>&nbsp;</td>
            <td></td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Order No<span class="compulsoryRed"> *</span></td>
            <td colspan="3"><select name="cboOrderNo" id="cboOrderNo" style="width:120px"  class="validate[required] txtText search" >
                <option value=""></option>
                <?php
					$sql = "SELECT DISTINCT
							trn_orderheader.intOrderNo, 
							trn_orderheader.intOrderYear 
							FROM trn_orderheader 
							where intStatus = 1 AND 
							trn_orderheader.intLocationId='$location' 
							ORDER BY intOrderYear DESC, intOrderNo DESC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intOrderNo']==$orderNo)
						echo "<option value=\"".$row['intOrderNo']."\" selected=\"selected\">".$row['intOrderNo']."</option>";	
						else
						echo "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";	
					}
				?>
              </select></td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">&nbsp;</td>
            <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="26%" class="normalfntRight">Color Wise</td>
                <td width="8%" class="normalfnt">&nbsp;<input name="radio" type="radio" class="mouseover" id="chkColor" value="1" checked="checked" /></td>
                <td width="26%" class="normalfntRight">Cut wise</td>
                <td width="7%" class="normalfnt">&nbsp;<input name="radio" type="radio" class="mouseover" id="chkCutNo"/></td>
                <td width="23%" class="normalfntRight">Size wise</td>
                <td width="10%">&nbsp;<input name="radio" type="radio" class="mouseover" id="chkSize"/></td>
              </tr>
            </table></td>
          </tr>
          <tr>
          <td colspan="4"><table width="100%" border="0" cellpadding="0" cellspacing="0"><tr>
          <td height="27" align="center" colspan="4" ><img src="../../../../../images/Tnew.jpg" width="92" height="24" id="butNew" name="butNew"  class="mouseover"/><img src="../../../../../images/Treport.jpg" width="92" height="24" class="mouseover" id="imgSearchItems" name="imgSearchItems"  /><a href="<?php echo $mainPage; ?>"><img src="../../../../../planning/img/Tclose.jpg" width="92" height="24"  class="mouseover"/></a></td>
          </tr></table></td>
          </tr>
        </table></td>
      </tr>
<tr>
      </tr>      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="92%">&nbsp;</td>
            <td width="8%">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td></td>
      </tr>
  </table></td></table></div></div>
</form>
	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>


</body>
</html>

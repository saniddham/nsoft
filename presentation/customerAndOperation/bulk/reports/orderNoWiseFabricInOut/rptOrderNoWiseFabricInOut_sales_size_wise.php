<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
ini_set('display_errors',0);

$orderNo 		= (!isset($_REQUEST["orderNo"])?'':$_REQUEST["orderNo"]);
$orderYear 		= (!isset($_REQUEST["orderYear"])?'':$_REQUEST["orderYear"]);
$styleNo 		= (!isset($_REQUEST["styleNo"])?'':$_REQUEST["styleNo"]);
$option 		= (!isset($_REQUEST["option"])?'':$_REQUEST["option"]);
$hsStickerEnable = (!isset($_REQUEST["hsStickerEnable"])?0:$_REQUEST["hsStickerEnable"]);
$hsStickerEnable = (int)$hsStickerEnable;
$STICKER_MAKING      = '-STICKER_MAKING';
$intStatus		= 0;

if($option==1){		
$selection='Color Wise';
$selection1='COLOR WISE';
}
else if($option==2)	{	
$selection='Cut No Wise';
$selection1='CUT NO WISE';
}
else if($option==3)	{	
$selection='Size Wise';
$selection1='SIZE WISE';
}
else if($option==4)	{	
$selection='Part Wise';
$selection1='PART WISE';
}
else if($option==6)	{	
$selection='Sales Order/Size  Wise';
$selection1='SALES ORDER/SIZE WISE';
}

$programCode		= 'P0527';


$sql = "SELECT
		trn_orderheader.strCustomerPoNo,
		trn_orderheader.intLocationId, 
		trn_orderheader.intCustomer, 
		mst_customer.strName as customer,
		sys_users.strUserName as user, 
		trn_orderheader.strRemark 
		FROM trn_orderheader 
		Inner Join mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
		Inner Join sys_users ON trn_orderheader.intCreator = sys_users.intUserId 
		WHERE
		trn_orderheader.intOrderNo =  '$orderNo' AND
		trn_orderheader.intOrderYear =  '$orderYear'"; 
$result = $db->RunQuery($sql);
$row	= mysqli_fetch_array($result);

$locationId = $row['intLocationId'];//this locationId use in report header(reportHeader.php)--------------------
$x_poNo 	= $row['strCustomerPoNo'];
$x_customer = $row['customer'];
$x_user 	= $row['user'];
$x_remark 	= $row['strRemark'];


if($x_poNo=='')
$x_poNo=$poNo;
//----------------			
 $sql = "SELECT
Sum(trn_orderdetails.intQty) AS qty, 
trn_orderdetails.intSampleNo,
trn_orderdetails.intSampleYear,
trn_orderdetails.strCombo,
trn_orderdetails.strPrintName,
trn_orderdetails.intRevisionNo , 
trn_orderdetails.strStyleNo , 
trn_orderdetails.strGraphicNo 
FROM trn_orderdetails  
WHERE
trn_orderdetails.intOrderNo='$orderNo' 
AND trn_orderdetails.intOrderYear='$orderYear' AND  trn_orderdetails.SO_TYPE < 2  ";
if($styleNo!=''){
 $sql .= " AND trn_orderdetails.strStyleNo='$styleNo'  ";
}
 $sql .= " 
GROUP BY
trn_orderdetails.intOrderNo,
trn_orderdetails.intOrderYear"; 
 // echo $sql;

$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$x_styleNo 	= $row['strStyleNo'];
$x_graphicNo 	= $row['strGraphicNo'];
$x_sampleNo 	= $row['intSampleNo'];
$x_sampleYear 	= $row['intSampleYear'];
$x_revNo 		= $row['intRevisionNo'];
$x_combo 		= $row['strCombo'];
$x_print 		= $row['strPrintName'];
$x_qty = $row['qty'];
//-------------------------			
				 
//----Style No---------------------
$sql = "SELECT DISTINCT 
trn_orderdetails.strStyleNo 
FROM trn_orderdetails  
WHERE
trn_orderdetails.intOrderNo='$orderNo' 
AND trn_orderdetails.intOrderYear='$orderYear'  ";
if($styleNo!=''){
 $sql .= " AND trn_orderdetails.strStyleNo='$styleNo'  ";
}

$result = $db->RunQuery($sql);
$i=0;
while($row=mysqli_fetch_array($result))
{
 if($i==0){
	 $str_style=$row['strStyleNo'];
 }
 else{
	 $str_style .='/'.$row['strStyleNo'];
 }
 
 $i++;
}

if($i>1){
	$str_style='('.$str_style.')';
}
//----Graphic No-------------------
$sql = "SELECT DISTINCT 
trn_orderdetails.strGraphicNo 
FROM trn_orderdetails  
WHERE
trn_orderdetails.intOrderNo='$orderNo' 
AND trn_orderdetails.intOrderYear='$orderYear'    ";
if($styleNo!=''){
 $sql .= " AND trn_orderdetails.strStyleNo='$styleNo'  ";
}

$result = $db->RunQuery($sql);
$i=0;
while($row=mysqli_fetch_array($result))
{
 if($i==0){
	 $str_graphic=$row['strGraphicNo'];
 }
 else{
	 $str_graphic .='/'.$row['strGraphicNo'];
 }
 
 $i++;
}

if($i>1){
	$str_graphic='('.$str_graphic.')';
}
//-------------------------
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Order No wise /<?php echo $selection; ?> Fabric In - Out Report</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />


<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:272px;
	top:511px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="images/pending.png"  /></div>
<?php
}
?>
<form id="frmRptOrderNoWiseFabricInOut" name="frmRptOrderNoWiseFabricInOut" method="post" action="rptOrderNoWiseFabricInOut.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>ORDER NO WISE /<?php echo $selection1; ?> FABRIC IN - OUT  REPORT</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="12%" class="normalfnt"><strong>Order No</strong></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="18%"><span class="normalfnt"><?php echo $orderNo  ?>/<?php echo $orderYear ?></span></td>
    <td width="8%" class="normalfnt"><strong>Style No</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="13%"><span class="normalfnt"><?php echo $str_style  ?></span></td>
    <td width="9%" class="normalfnt">
      <strong>Graphic No</strong></td>
    <td width="1%"><strong>:</strong></td>
  <td width="16%"><span class="normalfnt"><?php echo $str_graphic
 ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Customer PO</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $x_poNo   ?></span></td>
    <td class="normalfnt"><strong>Customer</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $x_customer; ?></span></td>
      <?php
      if ($hsStickerEnable!=1){
          ?>
          <td class="normalfnt"> <strong>Order Qty</strong></td>
          <td align="center" valign="middle"><strong>:</strong></td>
          <td><span class="normalfnt"><?php echo $x_qty ?></span></td>
          <?php
      }
      ?>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>By</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $x_user; ?></span></td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt" valign="top"><strong>Remarks</strong></td>
    <td align="center" valign="top"><strong>:</strong></td>
    <td colspan="4"><textarea cols="50" rows="4" class="textarea" style="width::300px" disabled="disabled"><?php echo $x_remark  ?></textarea></td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="3" class="normalfnt">
          <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="">
              <th width="10%" >Date</th>
              <th width="8%" >Received No</th>
              <th width="6%" >AOD No</th>
              <th width="9%" > <?php if($option==1){ echo "Color"; }else if($option==2) { echo "CUT NO";}  else if($option==3) { echo "SIZE";} else if($option==4) { echo "PART";}  else if($option==6) { echo "Sales Order No";}  ?></th>
              
              <?php
			  if($option==4){
			  ?>
              <th width="6%" >Cut No</th>
              <th width="6%" >Size</th>
              <?php
			  }
			  ?>
              <?php
			  if($option==6){
			  ?>
              <th width="6%" >Size</th>
              <?php
			  }
			  ?>
              
              <th width="4%" >Qty</th>
              
              
              
              <th width="5%" >&nbsp;</th>
              <th width="10%" >Date</th>
              <th width="11%" >Dispatch No</th>
              <th width="13%" ><?php if($option==1){ echo "Color"; }else if($option==2) { echo "CUT NO";}  else if($option==3) { echo "SIZE";} else if($option==4) { echo "PART";}  else if($option==6) { echo "Sales Order No";}  ?></th>
              <?php
			  if($option==4){
			  ?>
              <th width="6%" >Cut No</th>
              <th width="6%" >Size</th>
              <?php
			  }
			  ?>
              <?php
			  if($option==6){
			  ?>
              <th width="6%" >Size</th>
              <?php
			  }
			  ?>
              <th width="4%" >Total</th>
              <th width="4%" >CUT RET</th>
              <th width="4%" >FD</th>
              <th width="4%" >PD</th>
              <th width="3%" >ED</th>
              <th width="4%" >SAMPLE</th>
              <th width="5%" >GOOD</th>
              </tr>
              <?php 
			  $totQtyP=0;
              $totQty=0;
	
			  $totQtyCutRetP =0;
			  $totQtyFDP=0;
			  $totQtyPDP =0;
			  $totQtyEDP =0;
			  $totQtySampP=0;
			  $totQtyGoodP =0;
			  $totQtyTotalP=0;
			  
              $totQtyCutRet=0;
              $totQtyFD=0;
              $totQtyPD =0;
              $totQtyED =0;
              $totQtySamp=0;
              $totQtyGood =0;
			  $totQtyTotal=0;
			  
			  
			 // $receivedRecords=getRecvRecords($orderNo,$orderYear);
			  $dispNoString="'*'";
			  $paraString="'*'";
			  $notInString="'*'";
			  $tmpPara='';
			  $tmpParaId='';
			  $tmpColorId='';
			  $tmpPartId='';
			  
			  
			  $result=getRowsReceived($orderNo,$orderYear,$styleNo,$option,$hsStickerEnable);
			  $i=0;
			  while($row=mysqli_fetch_array($result))//begining of received
			  {
				  $i++;
				$date=$row['dtmdate'];
				$dateRcvd=$row['dtmdate'];
				$recvNo=$row['recvNo'];
				$receivedNo=$row['intFabricReceivedNo'];
				$receivedYear=$row['intFabricReceivedYear'];
				$AODNo=$row['strAODNo'];
				$para=$row['para'];
				$paraId=$row['paraId'];
				$colorId=$row['colorId']; 
				$color=$row['color']; 
				$partId=$row['partId']; 
				$part=$row['partName']; 
				$paraB=$para."/".$row['colorId']."/".$row['partId']; 
				
				if($option==4){
				$cutNo=$row['cutNo'];
				$size=$row['size'];
				$cutNor=$row['cutNo'];
				$sizer=$row['size'];
				}
				if($option==6){//sales order no
				$size=$row['size'];
				$sizer=$row['size'];
				$paraB=$paraId."/".$row['colorId']."/".$row['partId'];
                $row['para'] = str_replace("-PRESSING","",$row['para']);
                if ($hsStickerEnable ==1){
                    $row['para'].=$STICKER_MAKING;
                }
				$para=$row['para']."/".$paraId;
				}
				$qty=$row['Qty'];
			//	$totQtyP=0;
			  $receivedRecords=getRecvRecords($orderNo,$orderYear,$styleNo,$option,$paraId);
      		  $dispatched=0;
  ?><!---------------------------------------------------------------------------------------------------------------> 
	  <?php 
  			 if(($tmpPara!=$paraB) && ($tmpPara==$paracc)){
  			  $result2=getRowsDispatch($orderNo,$orderYear,$styleNo,$option,$notInString,$tmpParaId,$tmpColorId,$tmpPartId, $hsStickerEnable);
			  while($row1=mysqli_fetch_array($result2))
			  { 
				 $dispatched++;
				$date=$row1['date'];
				$dispNo=$row1['dispatNo'];
				$diapatchNo=$row1['intBulkDispatchNo'];
				$diapatchYear=$row1['intBulkDispatchNoYear'];

                $row1['para'] = str_replace("-PRESSING","",$row1['para']);
                if ($hsStickerEnable ==1){
                      $row1['para'].=$STICKER_MAKING;
                }
				$parac=$row1['para'];
				$para2=$row1['para2'];
				if($option==4){//sales order no
				$cutNo=$row1['cutNo'];
				$size=$row1['size'];
				$cutNoD=$row1['cutNo'];
				$sizeD=$row1['size'];
				}
				if($option==6){//sales order no
				$size=$row1['size'];
				$sizeD=$row1['size'];
				}
				$paracc=$row1['para']."/".$row1['colorId']."/".$row1['partId'];
				$total=$row1['Total'];
				$cutRet=$row1['dblCutRetQty'];
				$fd=$row1['dblFdammageQty'];
				$pd=$row1['dblPDammageQty'];
				$ed=$row1['dblEmbroideryQty'];
				$samp=$row1['dblSampleQty'];
				$good=$row1['dblGoodQty'];
				if($option==4){
				$notInString.=",'".$dispNo."/".$para2."/".$cutNo."/".$size."/".$tmpColorId."/".$tmpPartId."'";
				}
				else if($option==6){
				$notInString.=",'".$dispNo."/".$para2."/".$size."/".$tmpColorId."/".$tmpPartId."'";
				}
				else{
				$notInString.=",'".$dispNo."/".$para2."/".$tmpColorId."/".$tmpPartId."'";
				}
				if($option==6){
				$parac	=$parac."/".$para2;
				}
			//	$paraB=$parac."/".$row1['colorId']."/".$row1['partId']; 
				
			?>
            <tr class="normalfnt"   bgcolor="#FFFFFF">
             <td align="center" class="normalfntMid" id="" ></td>
			<td align="center" class="normalfntMid" id="" ></td>
			<td align="center" class="normalfntMid" id=""></td>
			<td align="center" class="normalfntMid" id="" ></td>
              <?php
			  if($option==4){
			  ?>
              <td  align="center" class="normalfntMid" ></td>
              <td align="center" class="normalfntMid" ></td>
              <?php
			  }
			  ?>
              <?php
			  if($option==6){
			  ?>
              <td align="center" class="normalfntMid" ></td>
              <?php
			  }
			  ?>
			<td align="center" class="normalfntRight" id="" ></td>
            
			<td align="center" class="normalfntRight" id="" ></td>

			<td align="center" class="normalfntMid" id="<?php echo $date; ?>" ><?php echo $date; ?></td>
			<td align="center" class="normalfntMid" id="<?php echo $dispNo; ?>" ><a href='<?php echo "?q=919&serialNo=$diapatchNo&year=$diapatchYear" ?>' target='_blank'><?php echo $dispNo; ?></a></td>
			<td align="center" class="normalfntMid" id="<?php echo $para; ?>"><span class="normalfntMid"><?php echo $parac; ?></span></td>
              <?php
			  if($option==4){
			  ?>
              <td  align="center" class="normalfntMid" ><?php echo $cutNo; ?></td>
              <td align="center" class="normalfntMid" ><?php echo $size; ?></td>
              <?php
			  }
			  ?>
              <?php
			  if($option==6){
			  ?>
              <td align="center" class="normalfntMid" ><?php echo $size; ?></td>
              <?php
			  }
			  ?>
			<td align="center" class="normalfntRight" id="<?php echo $total; ?>"><span class="normalfntMid"><?php echo $total; ?></span></td>
			<td class="normalfntRight" id="<?php echo $cutRet; ?>"><span class="normalfntMid"><?php echo $cutRet; ?></span></td>
			<td class="normalfntRight" id="<?php echo $fd; ?>"><span class="normalfntMid"><?php echo $fd; ?></span></td>
			<td class="normalfntRight" id="<?php echo $pd; ?>"><span class="normalfntMid"><?php echo $pd; ?></span></td>
			<td class="normalfntRight" id="<?php echo $ed; ?>"><span class="normalfntMid"><?php echo $ed; ?></span></td>
			<td class="normalfntRight" id="<?php echo $samp; ?>"><span class="normalfntMid"><?php echo $samp; ?></span></td>
			<td class="normalfntRight" id="<?php echo $good; ?>"><span class="normalfntMid"><?php echo $good; ?></span></td>
            </tr>              
	  <?php 
			//-------------------
              $totQtyTotalP+=$total;
			  
              $totQtyCutRetP+=$cutRet;
              $totQtyFDP+=$fd;
              $totQtyPDP+=$pd;
              $totQtyEDP+=$ed;
              $totQtySampP+=$samp;
              $totQtyGoodP+=$good;
			//-------------------
	  
              $totQtyTotal+=$total;
              $totQtyCutRet+=$cutRet;
              $totQtyFD+=$fd;
              $totQtyPD+=$pd;
              $totQtyED+=$ed;
              $totQtySamp+=$samp;
              $totQtyGood+=$good;
			//-------------------
	  
	  
			 }
			 }
	  ?>
  <!---------------------------------------------------------------------------------------------------------------> 
	  <?php 
  			 if(($tmpPara!=$paraB) && ($i!=1)){
	  ?>
            <tr class="normalfnt"   bgcolor="#DDDDDD">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <?php
			  if($option==4){
			  ?>
              <td  align="center" class="normalfntMid" ></td>
              <td align="center" class="normalfntMid" ></td>
              <?php
			  }
			  ?>
              <?php
			  if($option==6){
			  ?>
              <td  align="center" class="normalfntMid" ></td>
              <?php
			  }
			  ?>
              <td class="normalfntRight" ><span class="normalfntRight"><b><?php echo $totQtyP ?></b></span></td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;</td>
              <?php
			  if($option==4){
			  ?>
              <td  align="center" class="normalfntMid" ></td>
              <td align="center" class="normalfntMid" ></td>
              <?php
			  }
			  ?>
              <?php
			  if($option==6){
			  ?>
              <td align="center" class="normalfntMid" ></td>
              <?php
			  }
			  ?>
              <td class="normalfntRight" ><b><?php echo $totQtyTotalP ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totQtyCutRetP ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totQtyFDP ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totQtyPDP ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totQtyEDP ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totQtySampP ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totQtyGoodP ?></b></td>
          	</tr>
	  <?php 
			 	  $totQtyP=0;
				  $totQtyCutRetP=0;
				  $totQtyFDP=0;
				  $totQtyPDP =0;
				  $totQtyEDP =0;
				  $totQtySampP=0;
				  $totQtyGoodP =0;
				  $totQtyTotalP=0;
			  
              }
			  
	  ?>
	  <?php 
//	  echo $tmpPara."--".$paraB;
  			 if($tmpPara!=$paraB){
	  ?>
            <tr class="normalfnt"   bgcolor="#E1F5FF">
             <td colspan="<?php if($option==4){echo "20";} else if($option==6){echo "18";} else{echo "16";}  ?>" align="center" class="normalfntMid" id="" ><?php echo /*$tmpPara."+".$paraB."--".*/$color."-".$part; ?></td>
          	</tr>
	  <?php 
			  $totQtyP=0;
              }
	  ?>
        
        <?php
			  $totQtyP +=$qty;
			  $totQty +=$qty;
		?>
            <tr class="normalfnt"   bgcolor="#FFFFFF">
             <td align="center" class="normalfntMid" id="<?php echo $date; ?>" ><?php echo $dateRcvd; ?></td>
			<td align="center" class="normalfntMid" id="<?php echo $recvNo; ?>" ><a href='<?php echo "?q=931&serialNo=$receivedNo&year=$receivedYear" ?>' target='_blank'><?php echo $recvNo; ?></a></td>
			<td align="center" class="normalfntMid" id="<?php echo $AODNo; ?>"><?php echo $AODNo; ?></td>
			<td align="center" class="normalfntMid" id="<?php echo $para; ?>" ><?php echo $para; ?></td>
              <?php
			  if($option==4){
			  ?>
              <td  align="center" class="normalfntMid" ><?php echo $cutNor; ?></td>
              <td align="center" class="normalfntMid" ><?php echo $sizer; ?></td>
              <?php
			  }
			  ?>
              <?php
			  if($option==6){
			  ?>
              <td align="center" class="normalfntMid" ><?php echo $sizer; ?></td>
              <?php
			  }
			  ?>
			<td align="center" class="normalfntRight" id="<?php echo $qty; ?>" ><?php echo $qty; ?></td>
            
			<td align="center" class="normalfntRight" id="" ></td>
            
            <?php  
			  $rcds=0;
			 // $paraId=' 39';
			  $result2=getRowsDispatch($orderNo,$orderYear,$styleNo,$option,$notInString,$paraId,$colorId,$partId,$hsStickerEnable);
			  while(($row1=mysqli_fetch_array($result2)) && ($rcds<1))
			  {
				$dispatched++;
				$date=$row1['date'];
				$dispNo=$row1['dispatNo'];
				$diapatchNo=$row1['intBulkDispatchNo'];
				$diapatchYear=$row1['intBulkDispatchNoYear'];
				$row1['para'] = str_replace("-PRESSING","",$row1['para']);
				if ($hsStickerEnable ==1){
				    $row1['para'].=$STICKER_MAKING;
				}
				$parac=$row1['para'];
				$paracc=$row1['para']."/".$row1['colorId']."/".$row1['partId'];
				$para2=$row1['para2'];
				
				if($option==4){//sales order no
				$cutNo=$row1['cutNo'];
				$size=$row1['size'];
				$cutNoD=$row1['cutNo'];
				$sizeD=$row1['size'];
				}
				if($option==6){
				$size=$row1['size'];
				$sizeD=$row1['size'];
				$paracc=$row1['para2']."/".$row1['colorId']."/".$row1['partId'];
				}
				$total=$row1['Total'];
				$cutRet=$row1['dblCutRetQty'];
				$fd=$row1['dblFdammageQty'];
				$pd=$row1['dblPDammageQty'];
				$ed=$row1['dblEmbroideryQty'];
				$samp=$row1['dblSampleQty'];
				$good=$row1['dblGoodQty'];
				$dispNoString.=",'".$dispNo."'";
				$dispNoString1=$dispNoString;
				$paraString.=",'".$para."'";
				$paraString1=$paraString;
				
				if($option==4){
				$notInString.=",'".$dispNo."/".$para2."/".$cutNo."/".$size."/".$colorId."/".$partId."'";
				}
				else if($option==6){
				$notInString.=",'".$dispNo."/".$para2."/".$size."/".$colorId."/".$partId."'";
				}
				else{
				$notInString.=",'".$dispNo."/".$para2."/".$colorId."/".$partId."'";
				}
				$notInString1=$notInString;
			  if($option==6){
				$para=$parac."/".$para2;
			  }
			?>
            
			<td align="center" class="normalfntMid" id="<?php echo $date; ?>" ><?php echo $date; ?></td>
			<td align="center" class="normalfntMid" id="<?php echo $dispNo; ?>" ><a href='<?php echo "?q=919&serialNo=$diapatchNo&year=$diapatchYear" ?>' target='_blank'><?php echo $dispNo; ?></a></td>
			<td align="center" class="normalfntMid" id="<?php echo $parac; ?>"><span class="normalfntMid"><?php echo $para; ?></span></td>
              <?php
			  if($option==4){
			  ?>
              <td  align="center" class="normalfntMid" ><?php echo $cutNo; ?></td>
              <td align="center" class="normalfntMid" ><?php echo $size; ?></td>
              <?php
			  }
			  ?>
              <?php
			  if($option==6){
			  ?>
              <td align="center" class="normalfntMid" ><?php echo $size; ?></td>
              <?php
			  }
			  ?>
			<td align="center" class="normalfntRight" id="<?php echo $total; ?>"><span class="normalfntMid"><?php echo $total; ?></span></td>
			<td class="normalfntRight" id="<?php echo $fdcutRet ?>"><span class="normalfntMid"><?php echo $cutRet; ?></span></td>
			<td class="normalfntRight" id="<?php echo $fd; ?>"><span class="normalfntMid"><?php echo $fd; ?></span></td>
			<td class="normalfntRight" id="<?php echo $pd; ?>"><span class="normalfntMid"><?php echo $pd; ?></span></td>
			<td class="normalfntRight" id="<?php echo $ed; ?>"><span class="normalfntMid"><?php echo $ed; ?></span></td>
			<td class="normalfntRight" id="<?php echo $samp; ?>"><span class="normalfntMid"><?php echo $samp; ?></span></td>
			<td class="normalfntRight" id="<?php echo $good; ?>"><span class="normalfntMid"><?php echo $good; ?></span></td>
            
            <?php 
				$rcds++; 
			//-------------------
              $totQtyTotalP+=$total;
              $totQtyCutRetP+=$cutRet;
              $totQtyFDP+=$fd;
              $totQtyPDP+=$pd;
              $totQtyEDP+=$ed;
              $totQtySampP+=$samp;
              $totQtyGoodP+=$good;
			//-------------------
	  
              $totQtyTotal+=$total;
              $totQtyCutRet+=$cutRet;
              $totQtyFD+=$fd;
              $totQtyPD+=$pd;
              $totQtyED+=$ed;
              $totQtySamp+=$samp;
              $totQtyGood+=$good;
			//-------------------
				
				//$tmpPara=$paraB;
				$tmpParaId=$paraId;
				$tmpColorId=$colorId;
				$tmpPartId=$partId;
				
			  }
			?>
            <!--------------------------------------------------------------------------->          
            <?php 
			if($rcds==0){
				$date='';
				$dispNo='';
				$para='';
				$total=0;
				$cutRet=0;
				$fd=0;
				$pd=0;
				$ed=0;
				$samp=0;
				$good=0;
				//$dispNoString.=",'".$dispNo."'";
				//$dispNoString1=$dispNoString;
			?>
			<td align="center" class="normalfntMid" id="" ></td>
			<td align="center" class="normalfntMid" id="" ></td>
			<td align="center" class="normalfntMid" id="" >&nbsp;</td>
              <?php
			  if($option==4){
			  ?>
              <td  align="center" class="normalfntMid" ></td>
              <td align="center" class="normalfntMid" ></td>
              <?php
			  }
			  ?>
              <?php
			  if($option==6){
			  ?>
              <td align="center" class="normalfntMid" ></td>
              <?php
			  }
			  ?>
			<td align="center" class="normalfntMid" id="" ></td>
			<td align="center" class="normalfntMid" id="" >&nbsp;</td>
			<td align="center" class="normalfntMid" id="" ></td>
			<td align="center" class="normalfntMid" id="" >&nbsp;</td>
			<td align="center" class="normalfntMid" id="" ></td>
			<td align="center" class="normalfntMid" id="" >&nbsp;</td>
			<td align="center" class="normalfntMid" id="" ></td>
            <?php 
			  }
			?>
            </tr>
            <!--------------------------------------------------------------------------->          
            
            
            
      <?php 
/*			//-------------------
			  $totQtyP+=$qty;
              $totQtyTotalP+=$total;
              $totQtyFDP+=$fd;
              $totQtyPDP+=$pd;
              $totQtyEDP+=$ed;
              $totQtySampP+=$samp;
              $totQtyGoodP+=$good;
			//-------------------
	  
			  $totQty+=$qty;
              $totQtyTotal+=$total;
              $totQtyFD+=$fd;
              $totQtyPD+=$pd;
              $totQtyED+=$ed;
              $totQtySamp+=$samp;
              $totQtyGood+=$good;
			//-------------------
			  
*/			  $tmpPara=$paraB;
			}
	  ?>
  <!--------------------------------------------------------------------------------------------------------------->          
            <?php  
			  $result2=getRowsDispatch($orderNo,$orderYear,$styleNo,$option,$notInString1,$paraId,$colorId,$partId,$hsStickerEnable);
			  while($row1=mysqli_fetch_array($result2))
			  {
				 $dispatched++;
				$date=$row1['date'];
				$dispNo=$row1['dispatNo'];
				$diapatchNo=$row1['intBulkDispatchNo'];
				$diapatchYear=$row1['intBulkDispatchNoYear'];
                $row1['para'] = str_replace("-PRESSING","",$row1['para']);
                if ($hsStickerEnable ==1){
                    $row1['para'].=$STICKER_MAKING;
                }
				$para=$row1['para'];
				$para2=$row1['para2'];
				if($option==4){//sales order no
				$para=$row1['partName'];
				$para2=$row1['partId'];
				}
				if($option==4){//sales order no
				$cutNo=$row1['cutNo'];
				$size=$row1['size'];
				$cutNoD=$row1['cutNo'];
				$sizeD=$row1['size'];
				}
				if($option==6){//sales order no
				$size=$row1['size'];
				$sizeD=$row1['size'];
				}
				$total=$row1['Total'];
				$cutRet=$row1['dblCutRetQty'];
				$fd=$row1['dblFdammageQty'];
				$pd=$row1['dblPDammageQty'];
				$ed=$row1['dblEmbroideryQty'];
				$samp=$row1['dblSampleQty'];
				$good=$row1['dblGoodQty'];
				if($option==4){
				$notInString.=",'".$dispNo."/".$para2."/".$cutNo."/".$size."/".$colorId."/".$partId."'";
				}
				else if($option==6){
				$notInString.=",'".$dispNo."/".$para2."/".$size."/".$colorId."/".$partId."'";
				}
				else{
				$notInString.=",'".$dispNo."/".$para2."/".$colorId."/".$partId."'";
				}
			  if($option==6){
				$para=$para."/".$para2; 	
			  }
			  
			  
/*				$parac=$row1['para'];
				$paracc=$row1['para']."/".$row1['colorId']."/".$row1['partId'];
				if($option==6){
				$paracc=$row1['para2']."/".$row1['colorId']."/".$row1['partId'];
				}
			  if($option==6){
				$para=$parac."/".$para2;
			  }
			  
*/			?>
            <tr class="normalfnt"   bgcolor="#FFFFFF">
             <td align="center" class="normalfntMid" id="" ></td>
			<td align="center" class="normalfntMid" id="" ></td>
			<td align="center" class="normalfntMid" id=""></td>
			<td align="center" class="normalfntMid" id="" ></td>
              <?php
			  if($option==4){
			  ?>
              <td  align="center" class="normalfntMid" ></td>
              <td align="center" class="normalfntMid" ></td>
              <?php
			  }
			  ?>
              <?php
			  if($option==6){
			  ?>
              <td  align="center" class="normalfntMid" ></td>
              <?php
			  }
			  ?>
			<td align="center" class="normalfntRight" id="" ></td>
            
			<td align="center" class="normalfntRight" id="" ></td>

			<td align="center" class="normalfntMid" id="<?php echo $date; ?>" ><?php echo $date; ?></td>
			<td align="center" class="normalfntMid" id="<?php echo $dispNo; ?>" ><a href='<?php echo "?q=919&serialNo=$diapatchNo&year=$diapatchYear" ?>' target='_blank'><?php echo $dispNo; ?></a></td>
			<td align="center" class="normalfntMid" id="<?php echo $para; ?>"><span class="normalfntMid"><?php echo $para; ?></span></td>
              <?php
			  if($option==4){
			  ?>
              <td  align="center" class="normalfntMid" ><?php echo $cutNo; ?></td>
              <td align="center" class="normalfntMid" ><?php echo $size; ?></td>
              <?php
			  }
			  ?>
              <?php
			  if($option==6){
			  ?>
              <td align="center" class="normalfntMid" ><?php echo $size; ?></td>
              <?php
			  }
			  ?>
			<td align="center" class="normalfntRight" id="<?php echo $total; ?>"><span class="normalfntMid"><?php echo $total; ?></span></td>
			<td class="normalfntRight" id="<?php echo $fd; ?>"><span class="normalfntMid"><?php echo $cutRet; ?></span></td>
			<td class="normalfntRight" id="<?php echo $fd; ?>"><span class="normalfntMid"><?php echo $fd; ?></span></td>
			<td class="normalfntRight" id="<?php echo $pd; ?>"><span class="normalfntMid"><?php echo $pd; ?></span></td>
			<td class="normalfntRight" id="<?php echo $ed; ?>"><span class="normalfntMid"><?php echo $ed; ?></span></td>
			<td class="normalfntRight" id="<?php echo $samp; ?>"><span class="normalfntMid"><?php echo $samp; ?></span></td>
			<td class="normalfntRight" id="<?php echo $good; ?>"><span class="normalfntMid"><?php echo $good; ?></span></td>
            </tr>              
            <?php 
			//-------------------
              $totQtyTotalP+=$total;
              $totQtyCutRetP+=$cutRet;
              $totQtyFDP+=$fd;
              $totQtyPDP+=$pd;
              $totQtyEDP+=$ed;
              $totQtySampP+=$samp;
              $totQtyGoodP+=$good;
			//-------------------
              $totQtyTotal+=$total;
              $totQtyCutRet+=$cutRet;
              $totQtyFD+=$fd;
              $totQtyPD+=$pd;
              $totQtyED+=$ed;
              $totQtySamp+=$samp;
              $totQtyGood+=$good;
			  
				//$tmpPara=$paraB;
			  }
			?>
       
	  <?php 
  			 if(($i!=1)){
	  ?>
            <tr class="normalfnt"   bgcolor="#DDDDDD">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <?php
			  if($option==4){
			  ?>
              <td  align="center" class="normalfntMid" ></td>
              <td align="center" class="normalfntMid" ></td>
              <?php
			  }
			  ?>
              <?php
			  if($option==6){
			  ?>
              <td  align="center" class="normalfntMid" ></td>
              <?php
			  }
			  ?>
              <td class="normalfntRight" ><span class="normalfntRight"><b><?php echo $totQtyP ?></b></span></td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;</td>
              <?php
			  if($option==4){
			  ?>
              <td  align="center" class="normalfntMid" ></td>
              <td align="center" class="normalfntMid" ></td>
              <?php
			  }
			  ?>
              <?php
			  if($option==6){
			  ?>
              <td align="center" class="normalfntMid" ></td>
              <?php
			  }
			  ?>
              <td class="normalfntRight" ><b><?php echo $totQtyTotalP ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totQtyCutRet ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totQtyFDP ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totQtyPDP ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totQtyEDP ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totQtySampP ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totQtyGoodP ?></b></td>
          	</tr>
	  <?php
	  
			  
              }
	  ?>
      
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <?php
			  if($option==4){
			  ?>
              <td  align="center" class="normalfntMid" ></td>
              <td align="center" class="normalfntMid" ></td>
              <?php
			  }
			  ?>
              <?php
			  if($option==6){
			  ?>
              <td align="center" class="normalfntMid" ></td>
              <?php
			  }
			  ?>
              <td class="normalfntRight" ><span class="normalfntRight"><b><?php echo $totQty ?></b></span></td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;</td>
              <?php
			  if($option==4){
			  ?>
              <td  align="center" class="normalfntMid" ></td>
              <td align="center" class="normalfntMid" ></td>
              <?php
			  }
			  ?>
              <?php
			  if($option==6){
			  ?>
              <td align="center" class="normalfntMid" ></td>
              <?php
			  }
			  ?>
              <td class="normalfntRight" ><b><?php echo $totQtyTotal ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totQtyCutRet ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totQtyFD ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totQtyPD ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totQtyED ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totQtySamp ?></b></td>
              <td class="normalfntRight" ><b><?php echo $totQtyGood ?></b></td>
              </tr>
              
              
            </table>
          </td>
        <td width="5%">&nbsp;</td>
        <td width="1%" colspan="5"></td>
        </tr>
      
      </table>
    </td>
</tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php
//----------------------------------------------
function getRecvRecords($orderNo,$orderYear,$styleNo,$option,$paraId){
	global $db;
	
	$sql = "SELECT DISTINCT
ware_fabricreceivedheader.intFabricReceivedNo,
ware_fabricreceivedheader.intFabricReceivedYear
FROM  
trn_orderdetails
INNER JOIN ware_fabricreceivedheader ON trn_orderdetails.intOrderNo = ware_fabricreceivedheader.intOrderNo AND trn_orderdetails.intOrderYear = ware_fabricreceivedheader.intOrderYear
INNER JOIN ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear AND trn_orderdetails.intSalesOrderId = ware_fabricreceiveddetails.intSalesOrderId
WHERE
ware_fabricreceivedheader.intOrderNo =  '$orderNo' AND
ware_fabricreceivedheader.intOrderYear =  '$orderYear' AND 
ware_fabricreceivedheader.intStatus='1' AND ";	
if($styleNo!='')		
$sql .= "  trn_orderdetails.strStyleNo='$styleNo' AND ";
	
if($option==1)		
$sql .= "ware_fabricreceiveddetails.intGroundColor='$paraId' ";
else if($option==2)	
$sql .= "ware_fabricreceiveddetails.strCutNo='$paraId' ";
else if($option==3)	
$sql .= "ware_fabricreceiveddetails.strSize='$paraId' ";
else if($option==4)	
$sql .= "ware_fabricreceiveddetails.intPart='$paraId' ";
else if($option==6)	
$sql .= "ware_fabricreceiveddetails.intSalesOrderId='$paraId' ";
	
		$result =  $db->RunQuery($sql);
		$no =  	mysqli_num_rows($result);
		return $no;
}
//----------------------------------------------
function getRowsReceived($orderNo,$orderYear,$styleNo,$option,$hsStickerEnable){
	global $db;
	

	$sql = "SELECT
			ware_fabricreceivedheader.dtmdate,
			ware_fabricreceivedheader.intFabricReceivedNo,
			ware_fabricreceivedheader.intFabricReceivedYear, 
			ware_fabricreceiveddetails.intGroundColor as colorId, 
			mst_colors_ground.strName as color, 
			ware_fabricreceiveddetails.intPart as partId,
			mst_part.strName as partName, 
			
			concat(ware_fabricreceivedheader.intFabricReceivedNo,'/',ware_fabricreceivedheader.intFabricReceivedYear) as recvNo, 
			ware_fabricreceivedheader.strAODNo, ";
	if($option==4)	{
	$sql .= "ware_fabricreceiveddetails.strCutNo as cutNo, ";
	$sql .= "ware_fabricreceiveddetails.strSize as size, ";
	}
	if($option==6)	{ 
	$sql .= "ware_fabricreceiveddetails.strSize as size, ";
	}
			
	if($option==1){		
	$sql .= "mst_colors_ground.strName as para, ";
	$sql .= "ware_fabricreceiveddetails.intGroundColor as paraId, ";
	}
	else if($option==2)	{
	$sql .= "ware_fabricreceiveddetails.strCutNo as para, ";
	$sql .= "ware_fabricreceiveddetails.strCutNo as paraId, ";
	}
	else if($option==3)	{
	$sql .= "ware_fabricreceiveddetails.strSize as para, ";
	$sql .= "ware_fabricreceiveddetails.strSize as paraId, ";
	}
	else if($option==4)	{
	$sql .= "mst_part.strName as para, ";
	$sql .= "ware_fabricreceiveddetails.intPart as paraId, ";
	}
	else if($option==6)	{
	$sql .= "(SELECT
			trn_orderdetails.strSalesOrderNo
			FROM trn_orderdetails
			WHERE
			trn_orderdetails.intOrderNo =  ware_fabricreceivedheader.intOrderNo AND
			trn_orderdetails.intOrderYear =  ware_fabricreceivedheader.intOrderYear AND
			trn_orderdetails.intSalesOrderId =  ware_fabricreceiveddetails.intSalesOrderId) as para, ";
	$sql .= "ware_fabricreceiveddetails.intSalesOrderId as paraId, ";
	}
		
	$sql .= "Sum(ware_fabricreceiveddetails.dblQty) as Qty
			FROM
			ware_fabricreceivedheader
			Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
			INNER JOIN trn_orderdetails ON ware_fabricreceivedheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricreceivedheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_fabricreceiveddetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
			Inner Join mst_colors_ground ON ware_fabricreceiveddetails.intGroundColor = mst_colors_ground.intId 
			left Join mst_part ON ware_fabricreceiveddetails.intPart = mst_part.intId
			WHERE
			ware_fabricreceivedheader.intOrderNo =  '$orderNo' AND
			ware_fabricreceivedheader.intOrderYear =  '$orderYear' AND 
			ware_fabricreceivedheader.intStatus='1'  ";

    if ($hsStickerEnable != 1){
        $sql .= " AND  trn_orderdetails.SO_TYPE != 2 ";
    } else {
        $sql .= " AND  trn_orderdetails.SO_TYPE = 2 ";
    }
	if($styleNo!='')		
	$sql .= " AND trn_orderdetails.strStyleNo='$styleNo' ";
			
	$sql .= "GROUP BY
			ware_fabricreceiveddetails.intFabricReceivedNo,
			ware_fabricreceiveddetails.intFabricReceivedYear, ";
			
	$sql .= "ware_fabricreceiveddetails.intGroundColor, ";
	$sql .= "ware_fabricreceiveddetails.intPart, ";
	if($option==2)	
	$sql .= "ware_fabricreceiveddetails.strCutNo, ";
	else if($option==3)	
	$sql .= "ware_fabricreceiveddetails.strSize, ";
	else if($option==4)	{
	$sql .= "ware_fabricreceiveddetails.strCutNo, ";
	$sql .= "ware_fabricreceiveddetails.strSize, ";
	}
	else if($option==6)	{
	$sql .= "ware_fabricreceiveddetails.intSalesOrderId, ";
	$sql .= "ware_fabricreceiveddetails.strSize, ";
	}
			
	$sql .= "ware_fabricreceiveddetails.intFabricReceivedNo";	
	
	
	$sql .= " Order By ";
	$sql .= " mst_colors_ground.strName asc ,";
	$sql .= " mst_part.strName asc ,";
/*	if($option==1)		
	$sql .= " mst_colors_ground.strName asc ,";
	else */if($option==2)	
	$sql .= "ware_fabricreceiveddetails.strCutNo asc , ";
	else if($option==3)	
	$sql .= "ware_fabricreceiveddetails.strSize asc , ";
	else if($option==4)	{
	$sql .= "ware_fabricreceiveddetails.strCutNo asc , ";
	$sql .= "ware_fabricreceiveddetails.strSize asc , ";
	}
	else if($option==6)	{
	$sql .= "ware_fabricreceiveddetails.intSalesOrderId asc , ";
	$sql .= "ware_fabricreceiveddetails.strSize asc , ";
	}
	//if($option==1)		
	//$sql .= " mst_colors_ground.strName asc ,";
	
	
	$sql .= "ware_fabricreceiveddetails.intFabricReceivedNo asc , ";
	$sql .= "ware_fabricreceiveddetails.intFabricReceivedYear asc ";
	//$sql .= "ware_fabricreceivedheader.dtmdate asc ";	
	
	 

		 $result = $db->RunQuery($sql);
		// $row=mysqli_fetch_array($result);
		// return $row;
		return $result;
}

//----------------------------------------------
function getRowsDispatch($orderNo,$orderYear,$styleNo,$option,$notInString,$paraId,$colorId,$partId,$hsStickerEnable){
	global $db;
	
	  $sql = "SELECT
			ware_fabricdispatchheader.dtmdate as date,
			ware_fabricdispatchheader.intBulkDispatchNo,
			ware_fabricdispatchheader.intBulkDispatchNoYear, 
			concat(ware_fabricdispatchheader.intBulkDispatchNo,'/',ware_fabricdispatchheader.intBulkDispatchNoYear) as dispatNo, 
			ware_fabricdispatchdetails.intGroundColor as colorId,
			mst_colors_ground.strName as color, 
			ware_fabricdispatchdetails.intPart as partId,
			mst_part.strName as partName, "; 
			
	if($option==4)	{
	$sql .= "ware_fabricdispatchdetails.strCutNo as cutNo, ";
	$sql .= "ware_fabricdispatchdetails.strSize as size, ";
	}

	if($option==6)	{
	$sql .= "ware_fabricdispatchdetails.strSize as size, ";
	}
			
	if($option==1){		
	$sql .= "mst_colors_ground.strName as para,
	 		ware_fabricdispatchdetails.intGroundColor as para2, ";
	}
	else if($option==2)	{
	$sql .= "ware_fabricdispatchdetails.strCutNo as para,
	  		ware_fabricdispatchdetails.strCutNo as para2, ";
	}
	else if($option==3)	{
	$sql .= "ware_fabricdispatchdetails.strSize as para,
			 ware_fabricdispatchdetails.strSize as para2, ";
	}
	else if($option==4)	{
	$sql .= "concat(mst_part.strName) as para, ";
	$sql .= "concat(ware_fabricdispatchdetails.intPart) as para2, ";
	}
	else if($option==6)	{
	$sql .= "(SELECT
			trn_orderdetails.strSalesOrderNo
			FROM trn_orderdetails
			WHERE
			trn_orderdetails.intOrderNo =  ware_fabricdispatchheader.intOrderNo AND
			trn_orderdetails.intOrderYear =  ware_fabricdispatchheader.intOrderYear AND
			trn_orderdetails.intSalesOrderId =  ware_fabricdispatchdetails.intSalesOrderId) as para, ";
	$sql .= "ware_fabricdispatchdetails.intSalesOrderId  as para2, ";
	}
			
	$sql .= "sum(IFNULL(ware_fabricdispatchdetails.dblFdammageQty,0)+ IFNULL(ware_fabricdispatchdetails.dblPDammageQty,0)+ IFNULL(ware_fabricdispatchdetails.dblEmbroideryQty,0)+ IFNULL(ware_fabricdispatchdetails.dblSampleQty,0)+ IFNULL(ware_fabricdispatchdetails.dblGoodQty,0)+ IFNULL(ware_fabricdispatchdetails.dblCutRetQty,0)) AS Total,
			sum(IFNULL(ware_fabricdispatchdetails.dblFdammageQty,0)) as dblFdammageQty,
			sum(IFNULL(ware_fabricdispatchdetails.dblPDammageQty,0)) as dblPDammageQty,
			sum(IFNULL(ware_fabricdispatchdetails.dblEmbroideryQty,0)) as dblEmbroideryQty,
			sum(IFNULL(ware_fabricdispatchdetails.dblSampleQty,0)) as dblSampleQty,
			sum(IFNULL(ware_fabricdispatchdetails.dblGoodQty,0)) as  dblGoodQty, 
			sum(IFNULL(ware_fabricdispatchdetails.dblCutRetQty,0)) as  dblCutRetQty  
			FROM
			ware_fabricdispatchheader
			Inner Join ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear
			INNER JOIN trn_orderdetails ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
			Inner Join mst_colors_ground ON ware_fabricdispatchdetails.intGroundColor = mst_colors_ground.intId 
			left Join mst_part ON ware_fabricdispatchdetails.intPart = mst_part.intId
			WHERE
			ware_fabricdispatchheader.intOrderNo =  '$orderNo' AND
			ware_fabricdispatchheader.intOrderYear =  '$orderYear' AND 
			ware_fabricdispatchheader.intStatus='1' ";
    if ($hsStickerEnable != 1){
        $sql .= " AND  trn_orderdetails.SO_TYPE != 2 ";
    } else {
        $sql .= " AND  trn_orderdetails.SO_TYPE = 2 ";
    }
	if($styleNo!='')		
	$sql .= " AND trn_orderdetails.strStyleNo='$styleNo' ";
	
	//if($notInString!="'*'"){
/*	if($option==1){	
	 $sql .= "  AND
			concat(ware_fabricdispatchheader.intBulkDispatchNo,'/',ware_fabricdispatchheader.intBulkDispatchNoYear,'/',ware_fabricdispatchdetails.intGroundColor)  NOT IN (".$notInString.") AND ware_fabricdispatchdetails.intGroundColor='$paraId' ";
	}
	else*/ if($option==2){	
	 $sql .= "  AND
			concat(ware_fabricdispatchheader.intBulkDispatchNo,'/',ware_fabricdispatchheader.intBulkDispatchNoYear,'/',ware_fabricdispatchdetails.strCutNo,'/',ware_fabricdispatchdetails.intGroundColor,'/',ware_fabricdispatchdetails.intPart)  NOT IN (".$notInString.")  AND ware_fabricdispatchdetails.strCutNo='$paraId'  AND ware_fabricdispatchdetails.intGroundColor='$colorId' AND ware_fabricdispatchdetails.intPart='$partId' ";
	}
	else if($option==3){	
	 $sql .= "  AND
			concat(ware_fabricdispatchheader.intBulkDispatchNo,'/',ware_fabricdispatchheader.intBulkDispatchNoYear,'/',ware_fabricdispatchdetails.strSize,'/',ware_fabricdispatchdetails.intGroundColor,'/',ware_fabricdispatchdetails.intPart)  NOT IN (".$notInString.") AND ware_fabricdispatchdetails.strSize='$paraId'  AND ware_fabricdispatchdetails.intGroundColor='$colorId' AND ware_fabricdispatchdetails.intPart='$partId' ";
	}
	else if($option==4){	
	 $sql .= "  AND
			concat(ware_fabricdispatchheader.intBulkDispatchNo,'/',ware_fabricdispatchheader.intBulkDispatchNoYear,'/',ware_fabricdispatchdetails.intPart,'/',ware_fabricdispatchdetails.strCutNo,'/',ware_fabricdispatchdetails.strSize,'/',ware_fabricdispatchdetails.intGroundColor,'/',ware_fabricdispatchdetails.intPart)  NOT IN (".$notInString.") AND ware_fabricdispatchdetails.intPart='$paraId'  AND ware_fabricdispatchdetails.intGroundColor='$colorId'  AND ware_fabricdispatchdetails.intPart='$partId'";
	}
	else if($option==6){	
	 $sql .= "  AND
			concat(ware_fabricdispatchheader.intBulkDispatchNo,'/',ware_fabricdispatchheader.intBulkDispatchNoYear,'/',ware_fabricdispatchdetails.intSalesOrderId,'/',ware_fabricdispatchdetails.strSize,'/',ware_fabricdispatchdetails.intGroundColor,'/',ware_fabricdispatchdetails.intPart)  NOT IN (".$notInString.") AND ware_fabricdispatchdetails.intSalesOrderId='$paraId'  AND ware_fabricdispatchdetails.intGroundColor='$colorId' AND ware_fabricdispatchdetails.intPart='$partId'";
	}
	
	if($option==1){	
	 $sql .= "  AND
			concat(ware_fabricdispatchheader.intBulkDispatchNo,'/',ware_fabricdispatchheader.intBulkDispatchNoYear,'/',ware_fabricdispatchdetails.intGroundColor,'/',ware_fabricdispatchdetails.intGroundColor,'/',ware_fabricdispatchdetails.intPart)  NOT IN (".$notInString.") AND ware_fabricdispatchdetails.intGroundColor='$paraId'  AND ware_fabricdispatchdetails.intPart='$partId'";
	}
	else{
	// $sql .= "  AND ware_fabricdispatchdetails.intGroundColor='$colorId' ";
	}
	
//	}
			
	$sql .= "GROUP BY
			ware_fabricdispatchdetails.intBulkDispatchNo,
			ware_fabricdispatchdetails.intBulkDispatchNoYear, ";
			
	$sql .= "ware_fabricdispatchdetails.intGroundColor, ";
	$sql .= "ware_fabricdispatchdetails.intPart, ";
	if($option==2)	
	$sql .= "ware_fabricdispatchdetails.strCutNo, ";
	else if($option==3)	
	$sql .= "ware_fabricdispatchdetails.strSize, ";
	else if($option==4)	{
	$sql .= "ware_fabricdispatchdetails.strCutNo, ";
	$sql .= "ware_fabricdispatchdetails.strSize, ";
	}
	else if($option==6){	
	$sql .= "ware_fabricdispatchdetails.intSalesOrderId, ";
	$sql .= "ware_fabricdispatchdetails.strSize, ";
	}
			
	$sql .= "ware_fabricdispatchdetails.intBulkDispatchNo";	
	
	
	$sql .= " Order By ";
	$sql .= " mst_colors_ground.strName asc ,";
	$sql .= " mst_part.strName asc ,";
/*	if($option==1)		
	$sql .= " mst_colors_ground.strName asc ,";
	else */if($option==2)	
	$sql .= "ware_fabricdispatchdetails.strCutNo asc , ";
	else if($option==3)	
	$sql .= "ware_fabricdispatchdetails.strSize asc , ";
	else if($option==4){	
	$sql .= "ware_fabricdispatchdetails.strCutNo asc , ";
	$sql .= "ware_fabricdispatchdetails.strSize asc , ";
	}
	else if($option==6)	{
	$sql .= "ware_fabricdispatchdetails.intSalesOrderId asc , ";
	$sql .= "ware_fabricdispatchdetails.strSize asc , ";
	}
	//if($option==1)		
	//$sql .= " mst_colors_ground.strName asc ,";
	
	$sql .= "ware_fabricdispatchdetails.intBulkDispatchNo asc , ";
	$sql .= "ware_fabricdispatchdetails.intBulkDispatchNoYear asc  ";
	//$sql .= "ware_fabricdispatchheader.dtmdate asc ";	
	
	
	
	 //   echo  $sql;
	
		 $result = $db->RunQuery($sql);
	//	 $row=mysqli_fetch_array($result);
	//	 return $row;
		// $row=mysqli_fetch_array($result);
		// return $row;
		return $result;
}
?>
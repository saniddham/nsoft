<?php

//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
session_start();
$requestType = $_REQUEST['requestType'];
//$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$userId = $_SESSION['userId'];
$location = $_SESSION['CompanyID'];
$company = $_SESSION['headCompanyId'];
//include "{$backwardseperator}dataAccess/Connector.php";


if ($requestType == 'loadOrderNos') {
    $db->connect();
    $orderYear = $_REQUEST['orderYear'];
    $sql = "SELECT DISTINCT
				trn_orderheader.intOrderNo
				FROM trn_orderheader
				WHERE
				trn_orderheader.intOrderYear =  '$orderYear'
				ORDER BY
				trn_orderheader.intOrderNo DESC";
    $result = $db->RunQuery($sql);
    $html = "<option value=\"\"></option>";
    //while($row=mysqli_fetch_array($result))
    foreach ($result as $row) {
        $html .= "<option value=\"" . $row['intOrderNo'] . "\">" . $row['intOrderNo'] . "</option>";
    }
    echo $html;
    echo json_encode($response);
    $db->disconnect();
} else if ($requestType == 'loadSizes') {
    $db->connect();
    $orderNo = $_REQUEST['orderNo'];
    $orderYear = $_REQUEST['orderYear'];
    $sql = "SELECT DISTINCT
                trn_ordersizeqty.strSize
                FROM trn_ordersizeqty
                WHERE
                trn_ordersizeqty.intOrderNo =  '$orderNo' AND
                trn_ordersizeqty.intOrderYear =  '$orderYear'
                ORDER BY
                trn_ordersizeqty.strSize ASC";
    $result = $db->RunQuery($sql);
    $html = "<option value=\"\"></option>";
    //while($row=mysqli_fetch_array($result))
    foreach ($result as $row) {
        $html .= "<option value=\"" . $row['strSize'] . "\">" . $row['strSize'] . "</option>";
    }
    echo $html;
    echo json_encode($response);
    $db->disconnect();
} else if ($requestType == 'loadCutNos') {
    $db->connect();
    $orderNo = $_REQUEST['orderNo'];
    $orderYear = $_REQUEST['orderYear'];
    $sql = "SELECT DISTINCT
                ware_fabricreceiveddetails.strCutNo
                FROM
                ware_fabricreceivedheader
                Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
                WHERE
                ware_fabricreceivedheader.intOrderNo =  '$orderNo' AND
                ware_fabricreceivedheader.intOrderYear =  '$orderYear' 
                Order by ware_fabricreceiveddetails.strCutNo ASC";
    $result = $db->RunQuery($sql);
    $html = "<option value=\"\"></option>";
    //while($row=mysqli_fetch_array($result))
    foreach ($result as $row) {
        $html .= "<option value=\"" . $row['strCutNo'] . "\">" . $row['strCutNo'] . "</option>";
    }
    echo $html;
    echo json_encode($response);
    $db->disconnect();
} else if ($requestType == 'loadSampleNos') {
    $db->connect();
    $customer = $_REQUEST['customer'];
    $sampYear = $_REQUEST['sampYear'];
    $graphic = $_REQUEST['graphic'];

    $sql = "SELECT DISTINCT
trn_sampleinfomations.intSampleNo
FROM trn_sampleinfomations
WHERE 
intStatus=1 ";
    if ($customer != '') {
        $sql .= " AND trn_sampleinfomations.intCustomer =  '$customer'";
    }
//if($sampYear!=''){
    $sql .= " AND trn_sampleinfomations.intSampleYear =  '$sampYear'";
//}
    if ($graphic != '') {
        $sql .= " AND trn_sampleinfomations.strGraphicRefNo =  '$graphic'";
    }

    $sql .= " ORDER BY
				trn_sampleinfomations.intSampleNo DESC";
    //echo $sql;
    $result = $db->RunQuery($sql);
    $html = "<option value=\"\"></option>";
    //while($row=mysqli_fetch_array($result))
    foreach ($result as $row) {
        $html .= "<option value=\"" . $row['intSampleNo'] . "\">" . $row['intSampleNo'] . "</option>";
    }
    echo $html;
    echo json_encode($response);
    $db->disconnect();
} else if ($requestType == 'loadGraphicNos') {
    $db->connect();
    $customer = $_REQUEST['customer'];
    $sampNo = $_REQUEST['sampNo'];
    $sampYear = $_REQUEST['sampYear'];

    $sql = "SELECT DISTINCT
trn_sampleinfomations.strGraphicRefNo
FROM trn_sampleinfomations
WHERE 
trn_sampleinfomations.intStatus=1 ";
    if ($customer != '') {
        $sql .= " AND trn_sampleinfomations.intCustomer =  '$customer'";
    }
    if ($sampNo != '') {
        $sql .= " AND trn_sampleinfomations.intSampleNo =  '$sampNo'";
    }
    if ($sampYear != '') {
        $sql .= " AND trn_sampleinfomations.intSampleYear =  '$sampYear'";
    }
    $sql .= " ORDER BY
				trn_sampleinfomations.strGraphicRefNo asc";

    $result = $db->RunQuery($sql);
    $html = "<option value=\"\"></option>";
    //while($row=mysqli_fetch_array($result))
    foreach ($result as $row) {
        $html .= "<option value=\"" . $row['strGraphicRefNo'] . "\">" . $row['strGraphicRefNo'] . "</option>";
    }
    echo $html;
    echo json_encode($response);
} else if ($requestType == 'loadHitRateRecords') {
    $db->connect();
    $sampleYear = $_REQUEST['cboSampleYear'];
    $sampleNo = $_REQUEST['cboSampleNo'];
    $graphic = $_REQUEST['cboGraphic'];
    $sampleDate = $_REQUEST['dtSampleDate'];
    $customer = $_REQUEST['cboCustomer'];
    $marketer = $_REQUEST['cboMarketer'];

    $sql = "
			SELECT  
			SUB_SAMPLE_AND_ORDER.SAMPLE_YEAR,
			SUB_SAMPLE_AND_ORDER.SAMPLE_NO,
			(SELECT MAX(trn_sampleinfomations.dtDate) FROM trn_sampleinfomations  WHERE SUB_SAMPLE_AND_ORDER.SAMPLE_YEAR=trn_sampleinfomations.intSampleYear
			 AND SUB_SAMPLE_AND_ORDER.SAMPLE_NO=trn_sampleinfomations.intSampleNo ) AS SAMPLE_DATE,
			   SUB_SAMPLE_AND_ORDER.GRAPHIC ,
			SUB_SAMPLE_AND_ORDER.CUSTOMER,
			SUB_SAMPLE_AND_ORDER.MARKETER,
			SUM(IFNULL(SUB_SAMPLE_AND_ORDER.DISPATCH_QTY,0)) AS DISPATCH_QTY,
			SUM(IFNULL(SUB_SAMPLE_AND_ORDER.ORDER_QTY,0)) AS ORDER_QTY   
			
			FROM   ( 
			
			SELECT  SUB_SAMPLE.* ,
			SUM(IFNULL(ORDER_D.intQty,0)) AS ORDER_QTY 
			 FROM  (
			
			SELECT
			TSI.intSampleYear AS SAMPLE_YEAR,
			TSI.intSampleNo AS SAMPLE_NO,
			TSI.intRevisionNo AS REVISION,
			TSI.strGraphicRefNo AS GRAPHIC,
			C.strName AS CUSTOMER,
			U.strFullName AS MARKETER,
			SUM(IFNULL(TSD.dblDispatchQty,0)) AS DISPATCH_QTY,
			TSI.intCustomer AS CUSTOMER_ID,
			TSI.intMarketer AS MARKETER_ID 
			FROM
			trn_sampleinfomations AS TSI
			LEFT JOIN trn_sampledispatchdetails AS TSD ON TSI.intSampleNo = TSD.intSampleNo AND TSI.intSampleYear = TSD.intSampleYear AND TSI.intRevisionNo = TSD.intRevisionNo
			LEFT JOIN mst_customer AS C ON TSI.intCustomer = C.intId
			LEFT JOIN sys_users AS U ON TSI.intMarketer = U.intUserId 
			 WHERE TSI.intStatus=1 	 
	 ";
    if ($sampleNo != '')
        $sql .= " AND TSI.intSampleNo = '$sampleNo'  ";
    if ($sampleYear != '')
        $sql .= " AND TSI.intSampleYear = '$sampleYear'  ";
    if ($sampleDate != '')
        $sql .= " AND TSI.dtDate = '$sampleDate'  ";
    if ($graphic != '')
        $sql .= " AND TSI.strGraphicRefNo = '$graphic'  ";
    if ($customer != '')
        $sql .= " AND TSI.intCustomer = '$customer'  ";
    if ($marketer != '')
        $sql .= " AND TSI.intMarketer = '$marketer'  ";

    $sql .= " 
				GROUP BY
				TSI.intSampleNo,
				TSI.intSampleYear,
				TSI.intRevisionNo 
				
				) AS SUB_SAMPLE 
				LEFT JOIN 
				(
					SELECT TOD.* FROM 
					trn_orderdetails AS TOD
					INNER JOIN trn_orderheader AS TOH 
					ON TOD.intOrderNo = TOH.intOrderNo 
					AND TOD.intOrderYear = TOH.intOrderYear 
					WHERE TOH.intStatus <= TOH.intApproveLevelStart 
					AND TOH.intStatus>=0 
				) AS ORDER_D
				 
				ON SUB_SAMPLE.SAMPLE_NO = ORDER_D.intSampleNo 
				AND SUB_SAMPLE.SAMPLE_YEAR = ORDER_D.intSampleYear 
				AND SUB_SAMPLE.REVISION = ORDER_D.intRevisionNo
				GROUP BY
				SUB_SAMPLE.SAMPLE_NO,
				SUB_SAMPLE.SAMPLE_YEAR,
				SUB_SAMPLE.REVISION  ) AS SUB_SAMPLE_AND_ORDER
				
				GROUP BY
				SUB_SAMPLE_AND_ORDER.SAMPLE_NO,
				SUB_SAMPLE_AND_ORDER.SAMPLE_YEAR, 
				SUB_SAMPLE_AND_ORDER.GRAPHIC,
				SUB_SAMPLE_AND_ORDER.CUSTOMER_ID,
				SUB_SAMPLE_AND_ORDER.MARKETER_ID 
				ORDER BY 
				SUB_SAMPLE_AND_ORDER.CUSTOMER ASC,
				SUB_SAMPLE_AND_ORDER.SAMPLE_YEAR ASC,
				SUB_SAMPLE_AND_ORDER.SAMPLE_NO ASC 	 ";


    $result = $db->RunQuery($sql);

    $r = $db->numRows();

    if ($r <= 0) {
        $resp['type'] = 'fail';
        $resp['msg'] = 'No data to export';
    } else {
        $resp['type'] = 'pass';
        $resp['msg'] = '';
    }

    echo json_encode($resp);
    $db->disconnect();
} else if ($requestType == 'loadOrderDetails') {
    $db->connect();


    $sql = "SELECT
	mst_customer.strName AS customer,
	mst_customer_locations_header.strName AS customerLocation,
	mst_companies.strName AS company,
	mst_locations.strName AS companyLocation,
	mst_brand.strName AS brand,
	trn_orderheader.strCustomerPoNo AS customerPO,
	trn_orderdetails.intSalesOrderId AS customerSO,
	trn_orderdetails.intOrderNo AS orderNo,
	trn_orderdetails.strLineNo AS lineNo,
	trn_orderheader.dtDeliveryDate AS poDeliveryDate,
	trn_orderdetails.strGraphicNo AS graphicNo,
	trn_orderdetails.intSampleNo AS sampleNo,
	trn_orderdetails.strStyleNo AS style,
	mst_colors_ground.strName AS backGroundColor,
	trn_ordersizeqty.strSize AS size,
	trn_orderdetails.dtPSD AS PSD,
	trn_orderdetails.dtDeliveryDate AS deliveryDate,
	trn_ordersizeqty.dblQty AS qty,
	trn_orderdetails.intQty AS poQty,
	0 AS actualTotalQty,
	trn_orderheader.dtDate AS date,
	trn_orderdetails.intOrderYear AS YEAR,
	mst_part.strName AS part,
	trn_orderdetails.dblDamagePercentage AS PDPercentage,
	trn_orderdetails.dblDamagePercentage AS damagePercentage,
	trn_ordersizeqty.ncingaDeliveryStatus
FROM
	trn_orderheader
INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
INNER JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
INNER JOIN mst_customer_locations_header ON mst_customer_locations_header.intId = trn_orderheader.intCustomerLocation
INNER JOIN mst_companies ON mst_companies.intId = mst_locations.intCompanyId
INNER JOIN mst_part ON mst_part.intId = trn_orderdetails.intPart
INNER JOIN trn_ordersizeqty ON trn_ordersizeqty.intOrderNo = trn_orderdetails.intOrderNo
AND trn_ordersizeqty.intOrderYear = trn_orderdetails.intOrderYear
AND trn_ordersizeqty.intSalesOrderId = trn_orderdetails.intSalesOrderId
INNER JOIN trn_sampleinfomations_details ON trn_orderdetails.intSampleNo = trn_sampleinfomations_details.intSampleNo
AND trn_orderdetails.intSampleYear = trn_sampleinfomations_details.intSampleYear
AND trn_orderdetails.intRevisionNo = trn_sampleinfomations_details.intRevNo
AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName
AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo
INNER JOIN mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
INNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
INNER JOIN trn_orderheader_approvedby ON trn_orderheader.intOrderNo = trn_orderheader_approvedby.intOrderNo
AND trn_orderheader_approvedby.intYear = trn_orderheader.intOrderYear
WHERE
	trn_orderheader.intStatus = 1
AND date(
	trn_orderheader_approvedby.dtApprovedDate) = '2018-12-18'
AND trn_orderheader_approvedby.intApproveLevelNo = 3
AND trn_orderheader_approvedby.intStatus = 0
GROUP BY
	trn_orderdetails.intOrderNo,
	trn_orderdetails.intOrderYear,
	trn_orderdetails.intSalesOrderId,
	trn_ordersizeqty.strSize ";


    $result = $db->RunQuery($sql);

    $r = $db->numRows();

    if ($r <= 0) {
        $resp['type'] = 'fail';
        $resp['msg'] = 'No data to export';
    } else {
        $resp['type'] = 'pass';
        $resp['msg'] = '';
    }

    echo json_encode($resp);
    $db->disconnect();
} else if ($requestType == 'loadFabricDetails') {
    $db->connect();


    $sql = "SELECT
	mst_customer.strName AS customer,
	mst_customer_locations_header.strName AS customerLocation,
	mst_companies.strName AS company,
	mst_locations.strName AS companyLocation,
	mst_brand.strName AS brand,
	trn_orderheader.strCustomerPoNo AS customerPO,
	trn_orderdetails.intSalesOrderId AS customerSO,
	trn_orderdetails.intOrderNo AS orderNo,
	trn_orderdetails.strLineNo AS lineNo,
	trn_orderheader.dtDeliveryDate AS poDeliveryDate,
	trn_orderdetails.strGraphicNo AS graphicNo,
	trn_orderdetails.intSampleNo AS sampleNo,
	trn_orderdetails.strStyleNo AS style,
	mst_colors_ground.strName AS backGroundColor,
	trn_ordersizeqty.strSize AS size,
	trn_orderdetails.dtPSD AS PSD,
	trn_orderdetails.dtDeliveryDate AS deliveryDate,
	trn_ordersizeqty.dblQty AS qty,
	trn_orderdetails.intQty AS poQty,
	0 AS actualTotalQty,
	trn_orderheader.dtDate AS date,
	trn_orderdetails.intOrderYear AS YEAR,
	mst_part.strName AS part,
	trn_orderdetails.dblDamagePercentage AS PDPercentage,
	trn_orderdetails.dblDamagePercentage AS damagePercentage,
	trn_ordersizeqty.ncingaDeliveryStatus
FROM
	trn_orderheader
INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
INNER JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
INNER JOIN mst_customer_locations_header ON mst_customer_locations_header.intId = trn_orderheader.intCustomerLocation
INNER JOIN mst_companies ON mst_companies.intId = mst_locations.intCompanyId
INNER JOIN mst_part ON mst_part.intId = trn_orderdetails.intPart
INNER JOIN trn_ordersizeqty ON trn_ordersizeqty.intOrderNo = trn_orderdetails.intOrderNo
AND trn_ordersizeqty.intOrderYear = trn_orderdetails.intOrderYear
AND trn_ordersizeqty.intSalesOrderId = trn_orderdetails.intSalesOrderId
INNER JOIN trn_sampleinfomations_details ON trn_orderdetails.intSampleNo = trn_sampleinfomations_details.intSampleNo
AND trn_orderdetails.intSampleYear = trn_sampleinfomations_details.intSampleYear
AND trn_orderdetails.intRevisionNo = trn_sampleinfomations_details.intRevNo
AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName
AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo
INNER JOIN mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
INNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
INNER JOIN trn_orderheader_approvedby ON trn_orderheader.intOrderNo = trn_orderheader_approvedby.intOrderNo
AND trn_orderheader_approvedby.intYear = trn_orderheader.intOrderYear
WHERE
	trn_orderheader.intStatus = 1
AND date(
	trn_orderheader_approvedby.dtApprovedDate) = '2018-12-18'
AND trn_orderheader_approvedby.intApproveLevelNo = 3
AND trn_orderheader_approvedby.intStatus = 0
GROUP BY
	trn_orderdetails.intOrderNo,
	trn_orderdetails.intOrderYear,
	trn_orderdetails.intSalesOrderId,
	trn_ordersizeqty.strSize ";


    $result = $db->RunQuery($sql);

    $r = $db->numRows();

    if ($r <= 0) {
        $resp['type'] = 'fail';
        $resp['msg'] = 'No data to export';
    } else {
        $resp['type'] = 'pass';
        $resp['msg'] = '';
    }

    echo json_encode($resp);
    $db->disconnect();
}
?>

<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['CompanyID'];
$locationId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";
//---------------------------------------------------------
$toCurrency  = $_REQUEST["currency"];
$month  = $_REQUEST["month"];
$year  = $_REQUEST["year"];
//---------------------------------------------------------
/*$toCurrency = 1;
$month=3;
$year = date("Y");*/

$startDate=1;
$monthName=getMonthName($month);
$ts = strtotime($monthName." ".$year);
$endDate= date('t', $ts); 

$startDate = date("Y-m-d", mktime(0, 0, 0, $month, $startDate, $year));
$endDate = date("Y-m-d", mktime(0, 0, 0, $month, $endDate, $year));

$i=0;
while ($temp < $endDate) {
 $temp = date("Y-m-d", mktime(0, 0, 0, $month, 1+$i, $year));
 $weekday = date('l', strtotime($temp)); // note: first arg to date() is lower-case L
 $printDateArr[$i]=(1+$i)."-".substr($monthName,0,3);
 $dateArr[$i]=(1+$i);
 $fullDateArr[$i]=$temp;
 $dateNameArray[$i]=$weekday; 
 $i++;
}
$noOfDays = $i;
$currencyName='';
$currencyName1=getCurrencyName($toCurrency);

//-------------------------------------------------------------------------
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Revenue Report</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:255px;
	top:170px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
.h {
	color: #F00;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<?php
}
?>
<form id="frmIssueReport" name="frmIssueReport" method="post" action="rptIssue.php">
<table width="1500" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>REVENUE REPORT - <?php echo date("Y/m/d H:i:s") ?></strong><strong></strong></div>
<div style="background-color:#FFF" ><strong>(Currency - <?php echo $currencyName1?>)</strong><strong></strong></div>
<table width="1500" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
    <table width="1500">
      <tr>
        <td width="10">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="1500" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="" bgcolor="#FFFFFF">
              <td width="50" colspan="4" class="normalfnt" ><b>Plant Wise Revenue Report</b></td>
              <?php
			  for($j=0; $j<$noOfDays ; $j++){ 
			  $bgCol='#FFFFFF';
			  if($dateNameArray[$j]=='Sunday')
			  $bgCol='#FF99CC';
			  
			  ?>
              <td width="80" class="normalfntMid" bgcolor="<?php echo $bgCol ; ?>" ><?php echo substr($dateNameArray[$j],0,3) ?></td>
              <?php
			  }
			  ?>
              </tr>
            <tr class="" bgcolor="#FFFFFF">
              <td bgcolor="#006699"><font color="#FFFFFF">Plant</font></td>
              <td bgcolor="#006699"><font color="#FFFFFF">Annual Target</font></td>
              <td bgcolor="#006699" ><font color="#FFFFFF">Monthly Target</font></td>
              <td bgcolor="#006699"><font color="#FFFFFF">Day Target</font></td>
              <?php
			  for($j=0; $j<$noOfDays ; $j++){
			  $bgCol='#FFFFFF';
			  if($dateNameArray[$j]=='Sunday')
			  $bgCol='#FF99CC';
			  $cumulativeDay[$j]=1;
			  ?>
              <td  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php echo $bgCol ; ?>" ><?php echo $printDateArr[$j]; ?></td>
              <?php
			  }
			  ?>
              </tr>
              
              <?php 
	  	   $sql1 = "SELECT
			mst_plant.intPlantId,
			mst_plant.strPlantName,
			mst_plant.dblAnualaTarget,
			mst_plant.dblMonthlyTarget,
			mst_plant.dblDayTarget
			FROM `mst_plant`
			ORDER BY
			mst_plant.strPlantName ASC
			";
			$result1 = $db->RunQuery($sql1);
			
	  		 $anualTargtTot =0;
	  		 $monthlyTargtTot =0;
	  		 $dayTargtTot =0;
			
			while($row=mysqli_fetch_array($result1))
			{
				$plantId=$row['intPlantId'];
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" >&nbsp;<?php echo $row['strPlantName'];?>&nbsp;</td>
              <td class="normalfntRight" align="right" ><?php echo $currencyName; ?>&nbsp;<?php echo number_format($row['dblAnualaTarget'],2);?>&nbsp;</td>
              <td class="normalfntRight" align="right" ><?php echo $currencyName; ?>&nbsp;<?php echo number_format($row['dblMonthlyTarget'],2);?>&nbsp;</td>
              <td class="normalfntRight" align="right" ><?php echo $currencyName; ?>&nbsp;<?php echo number_format($row['dblDayTarget'],2);?>&nbsp;</td>
              <?php 
			  for($j=0; $j<$noOfDays ; $j++){
				$date=$fullDateArr[$j];
				
				$dayType='';
				$sqlr = "SELECT
						mst_plant_calender.dtDate,
						mst_plant_calender.intDayType
						FROM
						mst_locations
						INNER JOIN mst_plant_calender ON mst_locations.intPlant = mst_plant_calender.intPlantId
						WHERE
						mst_plant_calender.intPlantId = '$plantId' AND
						mst_plant_calender.dtDate = '$date'";
				$resultr = $db->RunQuery($sqlr);
				$rorw=mysqli_fetch_array($resultr);
				$dayType=$rorw['intDayType'];
				  
				$dispatchValue=getDispatchQtyValue($plantId,$date,$toCurrency);  
				$DayWiseDispSumArr[$j]+=$dispatchValue;  
				  
			  $bgCol='#FFFFFF';
			  if($dateNameArray[$j]=='Sunday')
			  $bgCol='#FF99CC';
			  if($dayType=='0')//holiday
			  $bgCol='#FFFF33';
			  if($dayType==1)//working day
			  $bgCol='#FFFFFF';
				
			  if(($dateNameArray[$j]=='Sunday') || ($dayType=='0')){
				  $cumulativeDay[$j]=0;
			  }
				  
			  ?>
              <td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo $currencyName; ?>&nbsp;<?php echo number_format($dispatchValue,2); ?>&nbsp;</td>
              <?php
			  }
			  ?>
              </tr>
      <?php 
	  		 $anualTargtTot +=$row['dblAnualaTarget'];
	  		 $monthlyTargtTot +=$row['dblMonthlyTarget'];
	  		 $dayTargtTot +=$row['dblDayTarget'];
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" ><b><?php echo $currencyName; ?>&nbsp;<?php echo number_format($anualTargtTot,2); ?>&nbsp;</b></td>
              <td class="normalfntRight" ><b><?php echo $currencyName; ?>&nbsp;<?php echo number_format($monthlyTargtTot,2); ?>&nbsp;</b></td>
              <td class="normalfntRight" ><b><?php echo $currencyName; ?>&nbsp;<?php echo number_format($dayTargtTot,2); ?>&nbsp;</b></td>
              <?php
			  for($j=0; $j<$noOfDays ; $j++){
			  ?>
              <td class="normalfntRight" ><b><?php echo $currencyName; ?>&nbsp;<?php echo number_format($DayWiseDispSumArr[$j],2); ?>&nbsp;</b></td>
              <?php
			  }
			  ?>
              </tr>
              <!-----------Cum.status-------->
              <tr bgcolor="#FFFFFF">
              <td colspan="<?php echo $noOfDays+4; ?>" height="20"></td>
              </tr>
              <tr bgcolor="#FFFFFF">
              <td colspan="4" align="right" class="normalfntRight" >Cum.Status&nbsp;</td>
              <td colspan="<?php echo $noOfDays; ?>" ></td>
              </tr>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfntRight" colspan="4"  align="right">&nbsp;Cum. Target &nbsp;</td>
              <?php 
			  $k=0;
			  for($j=0; $j<$noOfDays ; $j++){
				$date=$fullDateArr[$j];
				
				if($cumulativeDay[$j]==0){//sunday or existing holiday atleast in one plant
					$cumulativeTgt=0;
				}
				else{
					$cumulativeTgt=$dayTargtTot*($k+1);
					$k++;
				}
				  
			  $bgCol='#FFFFFF';
				  
			  ?>
              <td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo $currencyName; ?>&nbsp;<?php echo number_format($cumulativeTgt,2); ?>&nbsp;</td>
              <?php
			  }
			  ?>
              </tr>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfntRight" colspan="4" align="right" >&nbsp;Cum. Hit&nbsp;</td>
              <?php 
			  for($j=0; $j<$noOfDays ; $j++){
				$date=$fullDateArr[$j];
				
				$dayType='';
				$sqlr = "SELECT
						mst_plant_calender.dtDate,
						mst_plant_calender.intDayType
						FROM
						mst_locations
						INNER JOIN mst_plant_calender ON mst_locations.intPlant = mst_plant_calender.intPlantId
						WHERE
						mst_plant_calender.intPlantId = '$plantId' AND
						mst_plant_calender.dtDate = '$date'";
				$resultr = $db->RunQuery($sqlr);
				$rorw=mysqli_fetch_array($resultr);
				$dayType=$rorw['intDayType'];
				  
				$dispatchValue=getDispatchQtyValue($plantId,$date,$toCurrency); 
				$DayWiseDispSumArr[$j]+=$dispatchValue;  
				  
			  $bgCol='#FFFFFF';
			  $sum=0;
			  for($k=0; $k<=$j ; $k++){
				$sum+=$DayWiseDispSumArr[$k];
			  }
			  $cumulativeDisp[$j]=$sum;
			  ?>
              <td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo $currencyName; ?>&nbsp;<?php echo number_format($cumulativeDisp[$j],2); ?>&nbsp;</td>
              <?php
			  }
			  ?>
              </tr>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfntRight" colspan="4" align="right" >&nbsp;Cum. Hit%&nbsp;</td>
              <?php 
			  $x=0;
			  for($j=0; $j<$noOfDays ; $j++){
				$date=$fullDateArr[$j];
				
				$dayType='';
				$sqlr = "SELECT
						mst_plant_calender.dtDate,
						mst_plant_calender.intDayType
						FROM
						mst_locations
						INNER JOIN mst_plant_calender ON mst_locations.intPlant = mst_plant_calender.intPlantId
						WHERE
						mst_plant_calender.intPlantId = '$plantId' AND
						mst_plant_calender.dtDate = '$date'";
				$resultr = $db->RunQuery($sqlr);
				$rorw=mysqli_fetch_array($resultr);
				$dayType=$rorw['intDayType'];
				  
				$dispatchValue=getDispatchQtyValue($plantId,$date,$toCurrency); 
				$DayWiseDispSumArr[$j]+=$dispatchValue;  
				  
			  $bgCol='#FFFFFF';
			  $sum=0;
			  for($k=0; $k<=$j ; $k++){
				$sum+=$DayWiseDispSumArr[$k];
			  }
			  $cumulativeDisp[$j]=$sum;
			  
			  
				if($cumulativeDay[$j]==0){//sunday or existing holiday atleast in one plant
					$cumulativeHitPerc=0;
				}
				else{
					$cumulativeHitPerc=$cumulativeDisp[$j]/($dayTargtTot*($k+1))*100;
					$x++;
				}
			  
			  ?>
              <td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo $currencyName; ?>&nbsp;<?php echo number_format($cumulativeHitPerc,2); ?>&nbsp;</td>
              <?php
			  }
			  ?>
              </tr>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfntRight" colspan="4" align="right" >&nbsp;Dev.&nbsp;</td>
              <?php 
			  $x=0;
			  for($j=0; $j<$noOfDays ; $j++){
				$date=$fullDateArr[$j];
				
				$dayType='';
				$sqlr = "SELECT
						mst_plant_calender.dtDate,
						mst_plant_calender.intDayType
						FROM
						mst_locations
						INNER JOIN mst_plant_calender ON mst_locations.intPlant = mst_plant_calender.intPlantId
						WHERE
						mst_plant_calender.intPlantId = '$plantId' AND
						mst_plant_calender.dtDate = '$date'";
				$resultr = $db->RunQuery($sqlr);
				$rorw=mysqli_fetch_array($resultr);
				$dayType=$rorw['intDayType'];
				  
				$dispatchValue=getDispatchQtyValue($plantId,$date,$toCurrency);  
				$DayWiseDispSumArr[$j]+=$dispatchValue;  
				  
			  $bgCol='#FFFFFF';
			  $cls="normalfntRight";
			  
				if($cumulativeDay[$j]==0){//sunday or existing holiday atleast in one plant
					$dev1=0;
				}
				else{
					$dev1=$cumulativeDisp[$j]-$dayTargtTot*($x+1);
					$x++;
				}
			//  $dev1=$cumulativeDisp[$j]-$dayTargtTot*($j+1);
			  
			  
			  $dev= number_format(abs($dev1),2);
			  if($dev1<0){
				 $dev="(".$dev .")";
				 $cls="compulsoryRed";
			   //  $bgCol='#FF3333';
			  }
				   
			  ?>
              <td class="<?php echo $cls; ?>" bgcolor="<?php echo $bgCol; ?>" ><?php echo $currencyName; ?>&nbsp;<?php echo $dev ?>&nbsp;</td>
              <?php
			  }
			  ?>
              </tr>
             <!-----------------------------> 
              <!-----------Managers revenue-------->
              <tr bgcolor="#FFFFFF">
              <td colspan="<?php echo $noOfDays+4; ?>" height="20"></td>
              </tr>
            <tr class="" bgcolor="#FFFFFF">
              <td bgcolor="#006699"><font color="#FFFFFF">Cluster</font></td>
              <td bgcolor="#006699"><font color="#FFFFFF">Annual Target</font></td>
              <td bgcolor="#006699" ><font color="#FFFFFF">Monthly Target</font></td>
              <td bgcolor="#006699"><font color="#FFFFFF">Day Target</font></td>
              <?php
			  for($j=0; $j<$noOfDays ; $j++){
			  $bgCol='#FFFFFF';
			  if($dateNameArray[$j]=='Sunday')
			  $bgCol='#FF99CC';
			  ?>
              <td  class="normalfntBlue normalfntMid" align="center" bgcolor="<?php echo $bgCol ; ?>" ><?php echo $printDateArr[$j]; ?></td>
              <?php
			  }
			  ?>
              </tr>
              
              <?php 
	  	   $sql1 = "SELECT
					mst_marketer_manager.strName,
					mst_marketer_manager.intMarketingManagerId,
					mst_marketer_manager.intStatus,
					mst_marketer_manager.dblAnualaTarget,
					mst_marketer_manager.dblMonthlyTarget,
					mst_marketer_manager.dblDayTarget
					FROM `mst_marketer_manager`
					ORDER BY
					mst_marketer_manager.intMarketingManagerId ASC
			";
			$result1 = $db->RunQuery($sql1);
			
	  		 $anualClustTargtTot =0;
	  		 $monthlyClustTargtTot =0;
	  		 $dayClustTargtTot =0;
			
			while($row=mysqli_fetch_array($result1))
			{
				$managerId=$row['intMarketingManagerId'];
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" >&nbsp;<?php echo $row['strName'];?>&nbsp;</td>
              <td class="normalfntRight" align="right" ><?php echo $currencyName; ?>&nbsp;<?php echo number_format($row['dblAnualaTarget'],2);?>&nbsp;</td>
              <td class="normalfntRight" align="right" ><?php echo $currencyName; ?>&nbsp;<?php echo number_format($row['dblMonthlyTarget'],2);?>&nbsp;</td>
              <td class="normalfntRight" align="right" ><?php echo $currencyName; ?>&nbsp;<?php echo number_format($row['dblDayTarget'],2);?>&nbsp;</td>
              <?php 
			  for($j=0; $j<$noOfDays ; $j++){
				$date=$fullDateArr[$j];
				  
				$dispatchValue=getClusterDispatchQtyValue($managerId,$date,$toCurrency);  
				$DayWiseClustDispSumArr[$j]+=$dispatchValue;  
				  
				  $bgCol='#FFFFFF';
				  if($dateNameArray[$j]=='Sunday')
				  $bgCol='#FF99CC';
			  
			  ?>
              <td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo $currencyName; ?>&nbsp;<?php echo number_format($dispatchValue,2); ?>&nbsp;</td>
              <?php
			  }
			  ?>
              </tr>
      <?php 
	  		 $anualClustTargtTot +=$row['dblAnualaTarget'];
	  		 $monthlyClustTargtTot +=$row['dblMonthlyTarget'];
	  		 $dayClustTargtTot +=$row['dblDayTarget'];
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" ><b><?php echo $currencyName; ?>&nbsp;<?php echo number_format($anualClustTargtTot,2); ?>&nbsp;</b></td>
              <td class="normalfntRight" ><b><?php echo $currencyName; ?>&nbsp;<?php echo number_format($monthlyClustTargtTot,2); ?>&nbsp;</b></td>
              <td class="normalfntRight" ><b><?php echo $currencyName; ?>&nbsp;<?php echo number_format($dayClustTargtTot,2); ?>&nbsp;</b></td>
              <?php
			  for($j=0; $j<$noOfDays ; $j++){
			  ?>
              <td class="normalfntRight" ><b><?php echo $currencyName; ?>&nbsp;<?php echo number_format($DayWiseClustDispSumArr[$j],2); ?>&nbsp;</b></td>
              <?php
			  }
			  ?>
              </tr>
              <!-----------Cum.status-------->
              <tr bgcolor="#FFFFFF">
              <td colspan="<?php echo $noOfDays+4; ?>" height="20"></td>
              </tr>
              <tr bgcolor="#FFFFFF">
              <td colspan="4" align="right" class="normalfntRight" >Cum.Status&nbsp;</td>
              <td colspan="<?php echo $noOfDays; ?>" ></td>
              </tr>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfntRight" colspan="4"  align="right">&nbsp;Cum. Target &nbsp;</td>
              <?php 
			  $k=0;
			  for($j=0; $j<$noOfDays ; $j++){
				$date=$fullDateArr[$j];
				
				  
			  $bgCol='#FFFFFF';
				
				if($cumulativeDay[$j]==0){//sunday or existing holiday atleast in one plant
					$cumulativeClustTgt=0;
				}
				else{
					$cumulativeClustTgt=$dayClustTargtTot*($k+1);
					$k++;
				}
				
				  
			  ?>
              <td class="normalfntRight" bgcolor="<?php echo $bgCol; ?>" ><?php echo $currencyName; ?>&nbsp;<?php echo number_format($cumulativeClustTgt,2); ?>&nbsp;</td>
              <?php
			  }
			  ?>
              </tr>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfntRight" colspan="4" align="right" >&nbsp;Dev.&nbsp;</td>
              <?php 
			  $k=0;
			  for($j=0; $j<$noOfDays ; $j++){
				$date=$fullDateArr[$j];
				
				$dayType='';
				$sqlr = "SELECT
						mst_plant_calender.dtDate,
						mst_plant_calender.intDayType
						FROM
						mst_locations
						INNER JOIN mst_plant_calender ON mst_locations.intPlant = mst_plant_calender.intPlantId
						WHERE
						mst_plant_calender.intPlantId = '$managerId' AND
						mst_plant_calender.dtDate = '$date'";
				$resultr = $db->RunQuery($sqlr);
				$rorw=mysqli_fetch_array($resultr);
				$dayType=$rorw['intDayType'];
				  
				$dispatchValue=getClusterDispatchQtyValue($managerId,$date,$toCurrency);  
				$DayWiseClustDispSumArr[$j]+=$dispatchValue;  
				  
			  $bgCol='#FFFFFF';
			  $cls="normalfntRight";
			  $dev1=$DayWiseClustDispSumArr[$j]-$dayClustTargtTot*($j+1);
			  
				if($cumulativeDay[$j]==0){//sunday or existing holiday atleast in one plant
					$dev1=0;
				}
				else{
					$dev1=$DayWiseClustDispSumArr[$j]-$dayClustTargtTot*($k+1);
					$k++;
				}
			  
			  $dev= number_format(abs($dev1),2);
			  if($dev1<0){
				 $dev="(".$dev .")";
				 $cls="compulsoryRed";
			   //  $bgCol='#FF3333';
			  }
				  ;
			  ?>
              <td class="<?php echo $cls; ?>" bgcolor="<?php echo $bgCol; ?>" align="right" ><?php echo $currencyName; ?>&nbsp;<?php echo $dev ?>&nbsp;</td>
              <?php
			  }
			  ?>
              </tr>
             <!-----------------------------> 
            </table>
          </td>
        <td width="10">&nbsp;</td>
        </tr>
 
 <tr>
 <td colspan="8" height="15"></td>
 </tr>
            </table>
          </td>
        <td width="10">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<?php  
?>
<tr height="90" >
  <td align="center" class="normalfntMid"></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php
//daysArray(4);

//------------------------------function getMonthName-------------------
function getMonthName($month){
	global $db;
	
	$sqlp = "SELECT
			mst_month.strMonth
			FROM `mst_month`
			WHERE
			mst_month.intMonthId = $month
			";	
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	return $rowp['strMonth'];
}
//------------------------------function getCurrencyName-------------------
function getCurrencyName($id){
	global $db;
	
 	$sqlp = "SELECT
mst_financecurrency.strCode
FROM
mst_financecurrency
WHERE
mst_financecurrency.intId = '$id'
			";	
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	return $rowp['strCode'];
}
//---------------------------------------------------------------------------
function getDispatchQtyValue($plantId,$date,$toCurrency){
	global $db;

//(qty*price*existingExchngRate)/$toCurrRate
	
 /*	$sqlp = "SELECT 
	round(  sum( (ware_grnheader.dblExchangeRate *ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate)/$toCurrencyRate),4   ) AS value, 

	Sum(ware_stocktransactions_fabric.dblQty*-1) as Qty
	FROM
	ware_stocktransactions_fabric
	INNER JOIN mst_locations ON ware_stocktransactions_fabric.intLocationId = mst_locations.intId
	WHERE
	mst_locations.intPlant = $managerId AND
	date(ware_stocktransactions_fabric.dtDate) = '$date' AND
	ware_stocktransactions_fabric.strType = 'Dispatched_G'
			";	*/
			
	$sqlp = "select 

(tb1.Qty1/mst_financeexchangerate.dblBuying) as Qty

 from (SELECT 
Sum(ware_stocktransactions_fabric.dblQty*-1*trn_orderdetails.dblPrice*mst_financeexchangerate.dblBuying) AS Qty1,
date(ware_stocktransactions_fabric.dtDate) as date,
mst_locations.intCompanyId 
FROM
ware_stocktransactions_fabric
left JOIN mst_locations ON ware_stocktransactions_fabric.intLocationId = mst_locations.intId
left JOIN trn_orderheader ON ware_stocktransactions_fabric.intOrderNo = trn_orderheader.intOrderNo AND ware_stocktransactions_fabric.intOrderYear = trn_orderheader.intOrderYear
left JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_stocktransactions_fabric.intSalesOrderId = trn_orderdetails.intSalesOrderId
left JOIN mst_financeexchangerate ON trn_orderheader.intCurrency = mst_financeexchangerate.intCurrencyId AND date(ware_stocktransactions_fabric.dtDate) = mst_financeexchangerate.dtmDate AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId
WHERE
mst_locations.intPlant = $plantId				 AND
date(ware_stocktransactions_fabric.dtDate) = '$date' AND
  ware_stocktransactions_fabric.strType = 'Dispatched_G') as tb1 
left JOIN mst_financeexchangerate ON $toCurrency = mst_financeexchangerate.intCurrencyId 
AND tb1.date = mst_financeexchangerate.dtmDate 
AND tb1.intCompanyId = mst_financeexchangerate.intCompanyId";	

	
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	return $rowp['Qty'];
	
}

//---------------------------------------------------------------------------
function getClusterDispatchQtyValue($managerId,$date,$toCurrency){
	global $db;

//(qty*price*existingExchngRate)/$toCurrRate
	
 /*	$sqlp = "SELECT 
	round(  sum( (ware_grnheader.dblExchangeRate *ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate)/$toCurrencyRate),4   ) AS value, 

	Sum(ware_stocktransactions_fabric.dblQty*-1) as Qty
	FROM
	ware_stocktransactions_fabric
	INNER JOIN mst_locations ON ware_stocktransactions_fabric.intLocationId = mst_locations.intId
	WHERE
	mst_locations.intPlant = $managerId AND
	date(ware_stocktransactions_fabric.dtDate) = '$date' AND
	ware_stocktransactions_fabric.strType = 'Dispatched_G'
			";	*/
			
	$sqlp = "select 

(tb1.Qty1/mst_financeexchangerate.dblBuying) as Qty

 from (SELECT 
Sum(ware_stocktransactions_fabric.dblQty*-1*trn_orderdetails.dblPrice*mst_financeexchangerate.dblBuying) AS Qty1,
date(ware_stocktransactions_fabric.dtDate) as date,
mst_locations.intCompanyId 
FROM
ware_stocktransactions_fabric
left JOIN mst_locations ON ware_stocktransactions_fabric.intLocationId = mst_locations.intId
left JOIN trn_orderheader ON ware_stocktransactions_fabric.intOrderNo = trn_orderheader.intOrderNo AND ware_stocktransactions_fabric.intOrderYear = trn_orderheader.intOrderYear
left JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_stocktransactions_fabric.intSalesOrderId = trn_orderdetails.intSalesOrderId
left JOIN mst_marketer ON trn_orderheader.intMarketer = mst_marketer.intUserId
left JOIN mst_financeexchangerate ON trn_orderheader.intCurrency = mst_financeexchangerate.intCurrencyId AND date(ware_stocktransactions_fabric.dtDate) = mst_financeexchangerate.dtmDate AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId
WHERE
mst_marketer.intManager = $managerId				 AND
date(ware_stocktransactions_fabric.dtDate) = '$date' AND
  ware_stocktransactions_fabric.strType = 'Dispatched_G') as tb1 
left JOIN mst_financeexchangerate ON $toCurrency = mst_financeexchangerate.intCurrencyId 
AND tb1.date = mst_financeexchangerate.dtmDate 
AND tb1.intCompanyId = mst_financeexchangerate.intCompanyId";	

	
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	return $rowp['Qty'];
	
}
?>	

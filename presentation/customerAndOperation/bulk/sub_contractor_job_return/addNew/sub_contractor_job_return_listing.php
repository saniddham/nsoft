<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$location 			= $sessions->getLocationId();

$programCode		= 'P0877';
$intUser  			= $_SESSION["userId"];

include_once 		"class/tables/trn_order_sub_contract_return_header.php";		$obj_trn_order_sub_contract_return_header		= new trn_order_sub_contract_return_header($db);
include_once 		"libraries/jqgrid2/inc/jqgrid_dist.php";

$select				= "MAX(LEVELS) AS MAX_LEVEL";
$header_result		= $obj_trn_order_sub_contract_return_header->select($select,NULL,$where,NULL,NULL);
$header_array		= mysqli_fetch_array($header_result);
$approveLevel 		= $header_array['MAX_LEVEL'];

//BEGIN - ADD DEFAULT WHERE STRING WHEN FORM LOADING {
$arr =  json_decode($_REQUEST['filters'],true);

$arr = $arr['rules'];

$where_string = '';
$where_array = array(
					'Status'=>'RH.STATUS',
					'CONCAT_RETURN_NO'=>"CONCAT(RH.SUB_CONTRACT_RETURN_NO,'/',RH.SUB_CONTRACT_RETURN_YEAR)",
					'CONCAT_GP_NO'=>"CONCAT(GH.SUB_CONTRACT_GP_NO,'/',GH.SUB_CONTRACT_GP_YEAR)",
					'CONCAT_SC_NO'=>"CONCAT(SH.SUB_CONTRACT_NO,'/',SH.SUB_CONTRACT_YEAR)",
					'CONCAT_ORDER_NO'=>"CONCAT(SH.ORDER_NO,'/',SH.ORDER_YEAR)",
					'RETURN_DATE'=>'RH.DATE'
					);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Revised'=>'-1','Cancelled'=>'-2','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}

if(!count($arr)>0)					 
		$where_string .= "AND RH.DATE = '".date('Y-m-d')."'";
//END }

$sql = "SELECT * FROM
		(SELECT
		  IF(RH.STATUS=1,'Approved',IF(RH.STATUS=0,'Rejected',IF(RH.STATUS=-2,'Canceled','Pending'))) AS Status,
		  RH.SUB_CONTRACT_RETURN_NO   AS SUB_CONTRACT_RETURN_NO,
		  RH.SUB_CONTRACT_RETURN_YEAR AS SUB_CONTRACT_RETURN_YEAR,
		  CONCAT(RH.SUB_CONTRACT_RETURN_NO,'/',RH.SUB_CONTRACT_RETURN_YEAR)	AS CONCAT_RETURN_NO,
		  GH.SUB_CONTRACT_GP_NO       AS SUB_CONTRACT_GP_NO,
		  GH.SUB_CONTRACT_GP_YEAR     AS SUB_CONTRACT_GP_YEAR,
		  CONCAT(GH.SUB_CONTRACT_GP_NO,'/',GH.SUB_CONTRACT_GP_YEAR)			AS CONCAT_GP_NO,
		  SH.SUB_CONTRACT_NO          AS SUB_CONTRACT_NO,
		  SH.SUB_CONTRACT_YEAR        AS SUB_CONTRACT_YEAR,
		  CONCAT(SH.SUB_CONTRACT_NO,'/',SH.SUB_CONTRACT_YEAR)				AS CONCAT_SC_NO,
		  SH.ORDER_NO                 AS ORDER_NO,
		  SH.ORDER_YEAR               AS ORDER_YEAR,
		  CONCAT(SH.ORDER_NO,'/',SH.ORDER_YEAR)								AS CONCAT_ORDER_NO,
		  (SELECT
			 GROUP_CONCAT(strStyleNo SEPARATOR ', ')
		   FROM (SELECT DISTINCT
				   strStyleNo,
				   intOrderNo,
				   intOrderYear
				 FROM trn_orderdetails) AS tb
		   WHERE tb.intOrderNo = SH.ORDER_NO
			   AND tb.intOrderYear = SH.ORDER_YEAR) AS strStyleNo,
		  (SELECT
			 GROUP_CONCAT(strGraphicNo SEPARATOR ', ')
		   FROM (SELECT DISTINCT
				   strGraphicNo,
				   intOrderNo,
				   intOrderYear
				 FROM trn_orderdetails) AS tb
		   WHERE tb.intOrderNo = SH.ORDER_NO
			   AND tb.intOrderYear = SH.ORDER_YEAR) AS strGraphicNo,
		  RH.DATE                     AS RETURN_DATE,
		  IFNULL((
				  SELECT
			concat(SU.strUserName,'(',max(AB.DATE),')' )
			FROM
			trn_order_sub_contract_return_approved_by AB
			Inner Join sys_users SU ON AB.USER = SU.intUserId
			WHERE
				AB.SUB_CONTRACT_RETURN_NO  = RH.SUB_CONTRACT_RETURN_NO 
				AND AB.SUB_CONTRACT_RETURN_YEAR = RH.SUB_CONTRACT_RETURN_YEAR 
				AND AB.LEVELS =  '1'
				AND AB.STATUS =  '0'			
		),IF(((SELECT
			MP.int1Approval 
			FROM menupermision  MP
			Inner Join menus M ON MP.intMenuId = M.intId
			WHERE
			M.strCode = '$programCode' AND
			MP.intUserId =  '$intUser')=1 AND RH.STATUS>1),'Approve', '')) as `1st_Approval`, ";
			
for($i=2; $i<=$approveLevel; $i++)
{							
if($i==2){
$approval="2nd_Approval";
}
else if($i==3){
$approval="3rd_Approval";
}
else {
$approval=$i."th_Approval";
}

$sql .= "IFNULL(
(
					SELECT
concat(SU.strUserName,'(',max(AB.DATE),')' )
FROM
trn_order_sub_contract_return_approved_by AB
Inner Join sys_users SU ON AB.USER = SU.intUserId
WHERE
AB.SUB_CONTRACT_RETURN_NO  = RH.SUB_CONTRACT_RETURN_NO AND
AB.SUB_CONTRACT_RETURN_YEAR = RH.SUB_CONTRACT_RETURN_YEAR AND
AB.LEVELS = '$i'   AND
AB.STATUS = '0' 
),
IF(
((SELECT
MP.int".$i."Approval 
FROM menupermision MP
Inner Join menus M 
	ON MP.intMenuId = M.intId
WHERE
M.strCode = '$programCode' AND
MP.intUserId =  '$intUser')=1 AND (RH.STATUS>1) AND (RH.STATUS<=RH.LEVELS) AND ((SELECT
max(AB.DATE)
FROM
trn_order_sub_contract_return_approved_by AB
Inner Join sys_users SU 
	ON AB.USER = SU.intUserId
WHERE
AB.SUB_CONTRACT_RETURN_NO  = RH.SUB_CONTRACT_RETURN_NO AND
AB.SUB_CONTRACT_RETURN_YEAR = RH.SUB_CONTRACT_RETURN_YEAR AND
AB.LEVELS =  ($i-1))<>'')),'Approve',
if($i>RH.LEVELS,'-----',''))
) as `".$approval."`, "; 
}

$sql .= "IFNULL((
                                      SELECT
								concat(SU.strUserName,'(',max(AB.DATE),')' )
								FROM
								trn_order_sub_contract_return_approved_by AB
								Inner Join sys_users SU ON AB.USER = SU.intUserId
								WHERE
								AB.SUB_CONTRACT_RETURN_NO  = RH.SUB_CONTRACT_RETURN_NO AND
								AB.SUB_CONTRACT_RETURN_YEAR = RH.SUB_CONTRACT_RETURN_YEAR AND
								AB.LEVELS =  '-2' AND
								AB.STATUS =  '0'
							),IF(((SELECT
								MP.intCancel 
								FROM menupermision MP
								Inner Join menus M ON MP.intMenuId = M.intId
								WHERE
								M.strCode = '$programCode' AND
								MP.intUserId =  '$intUser')=1 AND RH.STATUS=1),'Cancel', '')) as `Cancel`,  ";	
		  
$sql .="'View'	AS VIEW
		FROM trn_order_sub_contract_return_header RH
		  INNER JOIN trn_order_sub_contract_gate_pass_header GH
			ON GH.SUB_CONTRACT_GP_NO = RH.SUB_CONTRACT_GP_NO
			  AND GH.SUB_CONTRACT_GP_YEAR = RH.SUB_CONTRACT_GP_YEAR
		  INNER JOIN trn_order_sub_contract_header SH
			ON SH.SUB_CONTRACT_NO = GH.SUB_CONTRACT_NO
			  AND SH.SUB_CONTRACT_YEAR = GH.SUB_CONTRACT_YEAR
			WHERE RH.LOCATION_ID = $location
				$where_string
			)AS T WHERE 1=1"; //echo $sql;
					   
$formLink  					= "?q=877&serial_no={SUB_CONTRACT_RETURN_NO}&serial_year={SUB_CONTRACT_RETURN_YEAR}";
$reportLink  				= "?q=905&serial_no={SUB_CONTRACT_RETURN_NO}&serial_year={SUB_CONTRACT_RETURN_YEAR}";
$reportLinkApprove  		= "?q=905&serial_no={SUB_CONTRACT_RETURN_NO}&serial_year={SUB_CONTRACT_RETURN_YEAR}&mode=Confirm";
$reportLinkCancel	  		= "?q=905&serial_no={SUB_CONTRACT_RETURN_NO}&serial_year={SUB_CONTRACT_RETURN_YEAR}&mode=Cancel";
					   
$col = array();

$col["title"] 				= "Status";
$col["name"] 				= "Status";
$col["width"] 				= "3";
$col["stype"] 				= "select";
$str 						= ":All;Pending:Pending;Approved:Approved;Rejected:Rejected;Revised:Revised" ;
$col["editoptions"] 		=  array("value"=> $str);
$col["align"] 				= "center";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "GP Return No";
$col["name"] 				= "SUB_CONTRACT_RETURN_NO";
$col["width"] 				= "3";
$col["align"] 				= "center";
$col['hidden']				= true;	 
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "GP Return Year";
$col["name"] 				= "SUB_CONTRACT_RETURN_YEAR";
$col["width"] 				= "3";
$col["align"] 				= "center";
$col['hidden']				= true;
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "GP Return No";
$col["name"] 				= "CONCAT_RETURN_NO";
$col["width"] 				= "3";
$col["align"] 				= "center";
$col['link']				= $formLink;	 
$col["linkoptions"] 		= "target='sub_contractor_job_return.php'";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "GP No";
$col["name"] 				= "CONCAT_GP_NO";
$col["width"] 				= "3";
$col["align"] 				= "center";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Sub Contract No";
$col["name"] 				= "CONCAT_SC_NO";
$col["width"] 				= "3";
$col["align"] 				= "center";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Order No";
$col["name"] 				= "CONCAT_ORDER_NO"; 
$col["width"] 				= "5";
$col["align"] 				= "center";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Style No";
$col["name"] 				= "strStyleNo";
$col["width"] 				= "4";
$col["align"] 				= "center";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Graphic No";
$col["name"] 				= "strGraphicNo";
$col["width"]				= "4";
$col["align"] 				= "center";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Date";
$col["name"] 				= "RETURN_DATE";
$col["width"] 				= "5";
$col["align"] 				= "center";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "1st Approval";
$col["name"] 				= "1st_Approval";
$col["width"] 				= "5";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= $reportLinkApprove;
$col['linkName']			= 'Approve';
$col["linkoptions"] 		= "target='rpt_sub_contractor_job_return.php'";
$cols[] 					= $col;	
$col						= NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
		$ap			= "2nd Approval";
		$ap1		= "2nd_Approval";
	}
	else if($i==3){
		$ap			= "3rd Approval";
		$ap1		= "3rd_Approval";
	}
	else {
		$ap			= $i."th Approval";
		$ap1		= $i."th_Approval";
	}
//SECOND APPROVAL

$col["title"] 				= $ap;
$col["name"] 				= $ap1;
$col["width"] 				= "5";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= $reportLinkApprove;
$col['linkName']			= 'Approve';
$col["linkoptions"] 		= "target='rpt_sub_contractor_job_return.php'";
$cols[] 					= $col;	
$col						= NULL;
}

$col["title"] 				= "Cancel"; // caption of column
$col["name"] 				= "Cancel"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "5";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= $reportLinkCancel;
$col['linkName']			= 'Cancel';
$col["linkoptions"] 		= "target='rpt_sub_contractor_job_return.php'"; // extra params with <a> tag
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Report";
$col["name"] 				= "VIEW";
$col["width"] 				= "3";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= $reportLink;
$col["linkoptions"] 		= "target='rpt_sub_contractor_job_return.php'";
$cols[] 					= $col;	
$col						= NULL;

$d	=date('Y-m-d');
/*$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"},{"field":"DATE","op":"eq","data":"$d"}
     ]

}
SEARCH_JSON;*/


$jq = new jqgrid('',$db);

$grid["caption"] 		= "Sub Contractor Gate Pass Return Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'RETURN_DATE'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 

/*$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"STATUS","op":"eq","data":"Pending"}
     ]
}
SEARCH_JSON;

$grid["postData"] = array("filters" => $sarr); */

$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
						"add"=>false, // allow/disallow add
						"edit"=>false, // allow/disallow edit
						"delete"=>false, // allow/disallow delete
						"rowactions"=>false, // show/hide row wise edit/del/save option
						"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
						"export"=>true
						) 
				);

$out = $jq->render("list1");
?>

<?php echo $out?>
<?php
ini_set('display_errors',0);
session_start();
//session variables
$backwardseperator 			= "../../../../../";
$thisFilePath 				= $_SERVER['PHP_SELF'];
$userId 					= $_SESSION['userId'];
$locationId 				= $_SESSION['CompanyID'];
$companyId 					= $_SESSION['headCompanyId'];

$requestType 				= $_REQUEST['requestType']; // request parameter
$programName				= 'Sub Cotractor Job Return'; // program name
$programCode				= 'P0877'; // program code

require_once "../../../../../dataAccess/DBManager2.php"; //connection
//classes
include_once "../../../../../class/error_handler.php";									
include_once "../../../../../class/tables/menupermision.php";
include_once "../../../../../class/tables/sys_no.php";
include_once "../../../../../class/tables/sys_approvelevels.php";
include_once "../../../../../class/cls_commonFunctions_get.php";
include_once "../../../../../class/tables/trn_order_sub_contract_gate_pass_header.php";	
include_once "../../../../../class/tables/trn_order_sub_contract_return_header.php";
include_once "../../../../../class/tables/trn_order_sub_contract_return_details.php";
include_once "../../../../../class/tables/trn_order_sub_contract_gate_pass_details.php";	
include_once "../../../../../class/tables/trn_order_sub_contract_return_approved_by.php";
require_once "../../../../../class/cls_commonErrorHandeling_get.php";	
include_once "../../../../../class/tables/ware_stocktransactions_fabric.php";	
include_once "../../../../../class/dateTime.php";	

//objects
$db												= new DBManager2();
$error_handler 									= new error_handler();
$menupermision									= new menupermision($db);
$sys_no											= new sys_no($db);
$sys_approvelevels								= new sys_approvelevels($db);
$obj_commonFunctions_get						= new cls_commonFunctions_get($db);
$obj_trn_order_sub_contract_gate_pass_header	= new trn_order_sub_contract_gate_pass_header($db);
$obj_trn_order_sub_contract_gate_pass_details	= new trn_order_sub_contract_gate_pass_details($db);
$obj_trn_order_sub_contract_return_header		= new trn_order_sub_contract_return_header($db);
$obj_trn_order_sub_contract_return_details		= new trn_order_sub_contract_return_details($db);
$obj_trn_order_sub_contract_return_approved_by	= new trn_order_sub_contract_return_approved_by($db);
$obj_commonErr									= new cls_commonErrorHandeling_get($db);
$obj_ware_stocktransactions_fabric				= new ware_stocktransactions_fabric($db);
$dateTimes										= new dateTimes($db);

if($requestType=='loadGatePassNo')
{
	$gatePassYear	= $_REQUEST['gatePassYear'];
	$db->connect(); // open connection
	
	$result		= getGatePassNo($gatePassYear); // get gatepass no's
	$html		= "<option value=\"\"></option>";
	while($row	= mysqli_fetch_array($result))
	{
		$html.= "<option value=\"".$row['SUB_CONTRACT_GP_NO']."\">".$row['SUB_CONTRACT_GP_NO']."</option>";//create combo values
	}
	
	$response['html']	= $html; //combo equal to response array
	echo json_encode($response);
	$db->disconnect();
}
else if($requestType=='loadDetails')
{
	$gatePassYear	= $_REQUEST['gatePassYear'];
	$gatePassNo		= $_REQUEST['gatePassNo'];
	$db->connect(); // open connection
	
	$header_arr		= getGatePassHeaderData($gatePassNo,$gatePassYear); // get gate pass header data
	
	$response['orderNo']		= $header_arr['ORDER_NO'];
	$response['orderYear']		= $header_arr['ORDER_YEAR'];
	$response['subConNo']		= $header_arr['SUB_CONTRACT_NO'];
	$response['subConYear']		= $header_arr['SUB_CONTRACT_YEAR'];
	$response['arrDetailData']	= getGatePassDetails($gatePassNo,$gatePassYear); // get gate pass detail data 
	
	echo json_encode($response);
	$db->disconnect();
}
else if($requestType=='saveData')
{
	$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
	$arrDetails 		= json_decode($_REQUEST['arrDetails'],true);

	$serialNo			= $arrHeader['serialNo'];
	$serialYear			= $arrHeader['serialYear'];
	$date				= $arrHeader['date'];
	$gatePassNo			= $arrHeader['gatePassNo'];
	$gatePassYear		= $arrHeader['gatePassYear'];
	$remarks			= $obj_commonFunctions_get->replace($arrHeader["remarks"]);
	$editMode			= false;
	
	//check user permision for save 
	//check save or update
	
	//if save
	//get sys max no
	//save header data
	
	//if update
	//check valid status of header table
	//update header
	//update max status
	//delete Detail data
	//validate return qty exceed gate pass qty
	//save detail data
	
	try
	{
		$db->connect();//open connection.
		
		//check save permission	
		checkUserPermision($programCode,$userId);
		
		//check wether sane or update
		if($serialNo=='' && $serialYear=='')
		{
			//get sys max no
			$serialNo		= getSysMaxNo('SUB_CONTRACT_RETURN_NO',$locationId);
			$serialYear		= date('Y');
			
			// set sys_approvelevels values
			$sys_approvelevels->set($programCode); 
			//get approve levels
			$approveLevels	= $sys_approvelevels->getintApprovalLevel();
			//set status status = approve level + 1
			$status			= $approveLevels+1;
			//save header data
			saveHeader($serialNo,$serialYear,$date,$gatePassNo,$gatePassYear,$remarks,$locationId,$companyId,$status,$approveLevels,$userId);
		
		}
		else
		{
			// set trn_order_sub_contract_return_header values
			$obj_trn_order_sub_contract_return_header->set($serialNo,$serialYear);
			
			//get curren gatepass return status
			$status				= $obj_trn_order_sub_contract_return_header->getSTATUS();
			
			//get curren approve level of gatepass return
			$approveLevels		= $obj_trn_order_sub_contract_return_header->getLEVELS();
			
			//check valid status of header table
			validateGPReturnStatus($status,$approveLevels);
			
			// set sys_approvelevels values
			$sys_approvelevels->set($programCode);
			//get approve levels
			$approveLevels		= $sys_approvelevels->getintApprovalLevel();
			//set status status = approve level + 1
			$status				= $approveLevels+1;
			
			//save header data
			updateHeader($serialNo,$serialYear,$date,$gatePassNo,$gatePassYear,$remarks,$locationId,$companyId,$status,$approveLevels,$userId);
			
			//update max status in approve by table
			updateMaxStatus($serialNo,$serialYear);
			
			//delete sub contract gatepass return detail data
			deleteDetail($serialNo,$serialYear);
			
			$editMode			= true;
		}
		//detail data array
		foreach($arrDetails as $array_loop)
		{
			$salesOrderId	= $array_loop['salesOrderId'];
			$subJobId		= $array_loop['subJobId'];
			$cutNo			= $array_loop['cutNo'];
			$size			= $array_loop['size'];
			$returnQty		= $array_loop['returnQty'];
			
			//validate return qty exceed gate pass qty
			validateQtyBeforeSave($serialNo,$serialYear,$salesOrderId,$cutNo,$size,$returnQty);
			
			//save detail data
			saveDetail($serialNo,$serialYear,$salesOrderId,$subJobId,$cutNo,$size,$returnQty);
		}
		
		$db->commit();
		$response['type'] 			= "pass";
		if($editMode)
			$response['msg'] 		= "Updated Successfully.";
		else
			$response['msg'] 		= "Saved Successfully.";
			
		$response['serialNo']		= $serialNo;
		$response['serialYear']		= $serialYear;
	}
	catch(Exception $e)
	{
		$db->rollback();
		$response['msg'] 	=  $e->getMessage();;
		$response['error'] 	=  $error_handler->jTraceEx($e);
		$response['type'] 	=  'fail';
		$response['sql']	=  $db->getSql();
	}
	echo json_encode($response);
	$db->disconnect();
	
}
function getGatePassNo($gatePassYear)
{
	global $db;
	global $locationId;
	global $obj_trn_order_sub_contract_gate_pass_header;
	
	$cols	= " DISTINCT trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_NO ";
	
	$join	= " INNER JOIN trn_order_sub_contract_gate_pass_details SCGPD ON SCGPD.SUB_CONTRACT_GP_NO=trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_NO AND
				SCGPD.SUB_CONTRACT_GP_YEAR=trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_YEAR ";
	
	$where	= " trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_YEAR = '".$gatePassYear."' AND
				trn_order_sub_contract_gate_pass_header.STATUS = 1 AND
				trn_order_sub_contract_gate_pass_header.LOCATION_ID = '".$locationId."' AND
				SCGPD.BAL_TO_RETURN_QTY > 0 ";
	
	$result = $obj_trn_order_sub_contract_gate_pass_header->select($cols,$join,$where,$order = null, $limit = null);	
	return $result;
}
function getGatePassHeaderData($gatePassNo,$gatePassYear)
{
	global $db;
	global $obj_trn_order_sub_contract_gate_pass_header;
	
	$cols	= " trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_NO,
				trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_YEAR,
				SCH.ORDER_NO,
				SCH.ORDER_YEAR ";
	
	$join	= " INNER JOIN trn_order_sub_contract_header SCH ON SCH.SUB_CONTRACT_NO=trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_NO AND
				SCH.SUB_CONTRACT_YEAR=trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_YEAR ";
	
	$where	= "	trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_NO = '".$gatePassNo."' AND
				trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_YEAR = '".$gatePassYear."' ";
	
	$result = $obj_trn_order_sub_contract_gate_pass_header->select($cols,$join,$where,$order = null, $limit = null);	
	$row	= mysqli_fetch_array($result);
	
	return $row;
}
function getGatePassDetails($gatePassNo,$gatePassYear)
{
	global $db;
	global $obj_trn_order_sub_contract_gate_pass_details;
	
	$cols	= " SALES_ORDER_ID,
				OD.strSalesOrderNo,
				SUB_CONTR_JOB_ID,
				SCJT.NAME AS SUB_CON_JOB_TYPE,
				CUT_NO,
				SIZE,
				QTY,
				BAL_TO_RETURN_QTY ";
	
	$join	= " INNER JOIN trn_order_sub_contract_gate_pass_header SCGPH ON SCGPH.SUB_CONTRACT_GP_NO=trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_NO AND
				SCGPH.SUB_CONTRACT_GP_YEAR=trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_YEAR
				INNER JOIN trn_order_sub_contract_header SCH ON SCH.SUB_CONTRACT_NO=SCGPH.SUB_CONTRACT_NO AND
				SCH.SUB_CONTRACT_YEAR=SCGPH.SUB_CONTRACT_YEAR
				INNER JOIN trn_orderdetails OD ON OD.intOrderNo=SCH.ORDER_NO AND
				OD.intOrderYear=SCH.ORDER_YEAR AND
				OD.intSalesOrderId=trn_order_sub_contract_gate_pass_details.SALES_ORDER_ID
				INNER JOIN mst_sub_contract_job_types SCJT ON SCJT.ID=trn_order_sub_contract_gate_pass_details.SUB_CONTR_JOB_ID ";
	
	$where	= "	trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_NO = '".$gatePassNo."' AND
				trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_YEAR = '".$gatePassYear."' ";
	
	$result = $obj_trn_order_sub_contract_gate_pass_details->select($cols,$join,$where,$order = null, $limit = null);
	while($row = mysqli_fetch_array($result))
	{
		$data['SALES_ORDER_ID']		= $row['SALES_ORDER_ID'];
		$data['SALES_ORDER_NO']		= $row['strSalesOrderNo'];
		$data['SUB_CONTR_JOB_ID']	= $row['SUB_CONTR_JOB_ID'];
		$data['SUB_CON_JOB_TYPE']	= $row['SUB_CON_JOB_TYPE'];
		$data['CUT_NO']				= $row['CUT_NO'];
		$data['SIZE']				= $row['SIZE'];
		$data['QTY']				= $row['QTY'];
		$data['RETURNED_QTY']		= $row['QTY']-$row['BAL_TO_RETURN_QTY'];
		
		$arrDetailData[] 			= $data;
	}
	return $arrDetailData;
}
function checkUserPermision($programCode,$userId)
{
	global $menupermision;
	
	// set menupermission values
	$menupermision->set($programCode,$userId);
	
	if($menupermision->getintEdit()!=1)
		throw new Exception("No Permision to Edit");
	else
		return true;
}
function getSysMaxNo($fieldName,$locationId)
{
	global $sys_no;
	// set sys no values
	$sys_no->set($locationId);
	
	$maxNo		= $sys_no->getSUB_CONTRACT_RETURN_NO();
	
	$data  		= array(''.$fieldName.'' => '+1');
	$where		= " intCompanyId='".$locationId."' ";
	
	$resultArr	= $sys_no->upgrade($data,$where);
	
	if($maxNo=='' || $maxNo==0)
		throw new Exception("System No not initialized.");
	else if(!$resultArr['status'])
		throw new Exception("System No update error.");
	else
		return $maxNo;
}
function saveHeader($serialNo,$serialYear,$date,$gatePassNo,$gatePassYear,$remarks,$locationId,$companyId,$status,$approveLevels,$userId)
{
	global $db;
	global $obj_trn_order_sub_contract_return_header;
	global $dateTimes;

	$data  	= array(
					'SUB_CONTRACT_RETURN_NO'	=> $serialNo,
					'SUB_CONTRACT_RETURN_YEAR'	=> $serialYear,
					'SUB_CONTRACT_GP_NO'		=> $gatePassNo,
					'SUB_CONTRACT_GP_YEAR'		=> $gatePassYear,
					'COMPANY_ID'				=> $companyId,
					'LOCATION_ID'				=> $locationId,
					'REMARKS'					=> $remarks,
					'STATUS'					=> $status,
					'LEVELS'					=> $approveLevels,
					'DATE'						=> $date,					
					'CREATED_DATE'				=> $dateTimes->getCurruntDate(),
					'CREATED_BY'				=> $userId					
					);
	
	$result_arr		= $obj_trn_order_sub_contract_return_header->insert($data); // insert data to trn_order_sub_contract_return_header table
	
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	else
		return true;
}
function validateGPReturnStatus($status,$approveLevels)
{
	if($status==-2)
		throw new Exception("Cancelled.Can't Edit");
	else if(($status<=$approveLevels) && ($status != 0) && ($status != ''))
		throw new Exception("Approval level ".($approveLevels+1-$status)." Raised .Can't Edit");
	else if($dispatchStatus==-10)
		throw new Exception("Completed.Can't Edit");
	else
		return true;
}
function updateHeader($serialNo,$serialYear,$date,$gatePassNo,$gatePassYear,$remarks,$locationId,$companyId,$status,$approveLevels,$userId)
{
	global $db;
	global $obj_trn_order_sub_contract_return_header;
	
	$data  	= array(
					'SUB_CONTRACT_GP_NO'	=> $gatePassNo,
					'SUB_CONTRACT_GP_YEAR'	=> $gatePassYear,
					'COMPANY_ID'			=> $companyId,
					'LOCATION_ID'			=> $locationId,
					'REMARKS'				=> $remarks,
					'STATUS'				=> $status,
					'LEVELS'				=> $approveLevels,
					'DATE'					=> $date,
					'MODIFIED_BY'			=> $userId							
					);
	
	$where		= 'SUB_CONTRACT_RETURN_NO = "'.$serialNo.'" AND SUB_CONTRACT_RETURN_YEAR = "'.$serialYear.'" ' ;
		
	$result_arr	= $obj_trn_order_sub_contract_return_header->update($data,$where);
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	else
		return true;
}
function updateMaxStatus($serialNo,$serialYear)
{
	global $db;
	global $obj_trn_order_sub_contract_return_approved_by;
	
	$cols		= " MAX(STATUS) AS MAX_STATUS ";
	$where		= " SUB_CONTRACT_RETURN_NO = '".$serialNo."' AND
					SUB_CONTRACT_RETURN_YEAR = '".$serialYear."' ";
	
	$result 	= $obj_trn_order_sub_contract_return_approved_by->select($cols,$join = null,$where, $order = null, $limit = null);	
	$row		= mysqli_fetch_array($result);
	
	$maxStatus	= $row['MAX_STATUS']+1;
	
	$data  		= array('STATUS' => $maxStatus);
	
	$where		= ' SUB_CONTRACT_RETURN_NO = "'.$serialNo.'" AND 
					SUB_CONTRACT_RETURN_YEAR = "'.$serialYear.'" AND
					STATUS = 0 ' ;
					
	$result_arr	= $obj_trn_order_sub_contract_return_approved_by->update($data,$where);
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	else
		return true;	
}
function deleteDetail($serialNo,$serialYear)
{
	global $db;
	global $obj_trn_order_sub_contract_return_details;
	
	$where		= 'SUB_CONTRACT_RETURN_NO = "'.$serialNo.'" AND SUB_CONTRACT_RETURN_YEAR = "'.$serialYear.'" ' ;
	
	$result_arr = $obj_trn_order_sub_contract_return_details->delete($where);
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	else
		return true;	
}
function saveDetail($serialNo,$serialYear,$salesOrderId,$subJobId,$cutNo,$size,$returnQty)
{
	global $db;
	global $obj_trn_order_sub_contract_return_details;
	
	$data  	= array(
					'SUB_CONTRACT_RETURN_NO'	=> $serialNo,
					'SUB_CONTRACT_RETURN_YEAR'	=> $serialYear,
					'SALES_ORDER_ID'			=> $salesOrderId,
					'SUB_CONTR_JOB_ID'			=> $subJobId,
					'CUT_NO'					=> $cutNo,
					'SIZE'						=> $size,
					'QTY'						=> $returnQty			
					);
	
	$result_arr		= $obj_trn_order_sub_contract_return_details->insert($data); // insert data to trn_order_sub_contract_return_details table
	if(!$result_arr['status'])
		throw new Exception($result_arr['msg']);
	else
		return true;
}
function validateQtyBeforeSave($serialNo,$serialYear,$salesOrderId,$cutNo,$size,$returnQty)
{
	global $db;
	global $obj_trn_order_sub_contract_return_header;
	global $obj_trn_order_sub_contract_gate_pass_details;
	
	// set trn_order_sub_contract_return_header values
	$obj_trn_order_sub_contract_return_header->set($serialNo,$serialYear);
	
	$gatePassNo		= $obj_trn_order_sub_contract_return_header->getSUB_CONTRACT_GP_NO();
	$gatePassYear	= $obj_trn_order_sub_contract_return_header->getSUB_CONTRACT_GP_YEAR();
	
	$obj_trn_order_sub_contract_gate_pass_details->set($gatePassNo,$gatePassYear,$salesOrderId,$cutNo,$size);
	
	$balToReturnQty	= $obj_trn_order_sub_contract_gate_pass_details->getBAL_TO_RETURN_QTY();
	$salesOrderNo	= $obj_trn_order_sub_contract_gate_pass_details->getstrSalesOrderNo();
	$cutNo			= $obj_trn_order_sub_contract_gate_pass_details->getCUT_NO();
	$size			= $obj_trn_order_sub_contract_gate_pass_details->getSIZE();
	
	if($returnQty>$balToReturnQty)
		throw new Exception("Some Qty exceed balance to Return Qty.<br>Sales Order No : ".$salesOrderNo."<br>Cut No : ".$cutNo."<br>Size : ".$size."<br>Balance to Return Qty : ".$balToReturnQty."");
	else
		return true;
}

if($requestType=='approve')
{
	$serial_no		= $_REQUEST['serial_no'];
	$serial_year	= $_REQUEST['serial_year'];
	$savedStatus	= true;
	$db->connect();

	$result		= validateBeforeApprove($serial_no,$serial_year,$programCode,$userId); // check approve permission function
	if($result['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $result["msg"];
	}
	
	$result		= validateWiehRetuenToBalQty($serial_no,$serial_year); // checking trn_order_sub_contract_return_details:QTY should less than or equal to trn_order_sub_contract_gate_pass_details:BAL_TO_RETURN_QTY
	if(!$result['type'] && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $result["msg"];
	}
	
	$result		= updateHeaderStatus($serial_no,$serial_year,''); // update header status function	
	if(!$result['status'] && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $result['msg'];
		$error_sql		= $result['sql'];	
	}	
	
	$result		= approvedData($serial_no,$serial_year,$userId); // update approve by table
	if(!$result['status'] && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $result['msg'];
		$error_sql		= $result['sql'];	
	}
	
	$obj_trn_order_sub_contract_return_header->set($serial_no,$serial_year);
	$status			= $obj_trn_order_sub_contract_return_header->getSTATUS();
	$gatePassNo		= $obj_trn_order_sub_contract_return_header->getSUB_CONTRACT_GP_NO();
	$gatePassYear	= $obj_trn_order_sub_contract_return_header->getSUB_CONTRACT_GP_YEAR();
	
	if($status==1)
	{
		$result		= updateGataPassBalance($serial_no,$serial_year,$gatePassNo,$gatePassYear);
		if(!$result['type'] && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $result['msg'];
			$error_sql		= $result['sql'];	
		}
		
		$result		= updateStockTransaction($serial_no,$serial_year,$userId);
		if(!$result['type'] && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $result['msg'];
			$error_sql		= $result['sql'];	
		}
	}
	
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 	= "pass";
		$response['msg'] 	= "Approved Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 	= "fail";
		$response['msg'] 	= $savedMasseged;
		$response['sql']	= $error_sql;
	}
	$db->disconnect();
	echo json_encode($response);
}

if($requestType=='reject')
{
	$serial_no		= $_REQUEST['serial_no'];
	$serial_year	= $_REQUEST['serial_year'];
	$savedStatus	= true;
	$db->connect();
	
	$result		= validateBeforeApprove($serial_no,$serial_year,$programCode,$userId); // check approve permission function
	if($result['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $result["msg"];
	}
	
	$result		= updateHeaderStatus($serial_no,$serial_year,'0'); // update header status function	
	if(!$result['status'] && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $result['msg'];
		$error_sql		= $result['sql'];	
	}
	
	$result		= insertApprovedBy($serial_no,$serial_year,$userId,'0'); // update approve by table
	if(!$result['status'] && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $result['msg'];
		$error_sql		= $result['sql'];	
	}
	
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 	= "pass";
		$response['msg'] 	= "Reject successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 	= "fail";
		$response['msg'] 	= $savedMasseged;
		$response['sql']	= $error_sql;
	}
	$db->disconnect();
	echo json_encode($response);
}

if($requestType=='cancel')
{
	$serial_no		= $_REQUEST['serial_no'];
	$serial_year	= $_REQUEST['serial_year'];
	$savedStatus	= true;
	$db->connect();
	
	$result		= validateBeforeCancel($serial_no,$serial_year,$programCode,$userId); // check cancel permission function
	if($result['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $result["msg"];
	}
	
	$result		= validateStockBeforeCancel($serial_no,$serial_year); // check cancel permission function
	if(!$result['type'] && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $result["msg"];
	}
		
	$result		= updateHeaderStatus($serial_no,$serial_year,'-2'); // update header status function	
	if(!$result['status'] && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $result['msg'];
		$error_sql		= $result['sql'];	
	}
	
	$result		= insertApprovedBy($serial_no,$serial_year,$userId,'-2'); // update approve by table
	if(!$result['status'] && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $result['msg'];
		$error_sql		= $result['sql'];	
	}
	
	$obj_trn_order_sub_contract_return_header->set($serial_no,$serial_year);
	$gatePassNo		= $obj_trn_order_sub_contract_return_header->getSUB_CONTRACT_GP_NO();
	$gatePassYear	= $obj_trn_order_sub_contract_return_header->getSUB_CONTRACT_GP_YEAR();
	
	$result		= reviseGataPassBalance($serial_no,$serial_year,$gatePassNo,$gatePassYear);
	if(!$result['type'] && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $result['msg'];
		$error_sql		= $result['sql'];	
	}
	
	$result		= reviseStockTransaction($serial_no,$serial_year,$userId);
	if(!$result['type'] && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $result['msg'];
		$error_sql		= $result['sql'];	
	}
	
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 	= "pass";
		$response['msg'] 	= "Canceled successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 	= "fail";
		$response['msg'] 	= $savedMasseged;
		$response['sql']	= $error_sql;
	}
	$db->disconnect();
	echo json_encode($response);
}

function updateHeaderStatus($serial_no,$serial_year,$status)
{
	global $obj_trn_order_sub_contract_return_header;
	
	$where	= 'SUB_CONTRACT_RETURN_NO = "'.$serial_no.'" AND SUB_CONTRACT_RETURN_YEAR = "'.$serial_year.'" ' ;
	
	if($status=='') 
	{
		$data  		= array('STATUS' => -1);
		$resultArr	= $obj_trn_order_sub_contract_return_header->upgrade($data,$where); // upgrade header status
	}
	else
	{
		$data  		= array('STATUS' => $status);
		$resultArr	= $obj_trn_order_sub_contract_return_header->update($data,$where); // update header status
		
	}
	return $resultArr;
}

function approvedData($serial_no,$serial_year,$userId)
{
	global $obj_trn_order_sub_contract_return_header;
	global $obj_trn_order_sub_contract_return_approved_by;
	global $dateTimes;
	
	$where	= 'SUB_CONTRACT_RETURN_NO = "'.$serial_no.'" AND SUB_CONTRACT_RETURN_YEAR = "'.$serial_year.'" ' ;
	
	$header_result	= $obj_trn_order_sub_contract_return_header->select('*',NULL,$where,NULL,NULL); // get header data
	$headerRow		= mysqli_fetch_array($header_result);
	
	$approval		= $headerRow['LEVELS']+1-$headerRow['STATUS'];
	
	$data  	= array('SUB_CONTRACT_RETURN_NO'	=> $serial_no,
					'SUB_CONTRACT_RETURN_YEAR'	=> $serial_year,
					'LEVELS'					=> $approval,
					'USER'						=> $userId,
					'DATE'						=> $dateTimes->getCurruntDateTime(),
					'STATUS'					=> 0
					);
	
	$resultArr		= $obj_trn_order_sub_contract_return_approved_by->insert($data); // insert data to approve by table
	return $resultArr;
}

function insertApprovedBy($serial_no,$serial_year,$userId,$status)
{
	global $obj_trn_order_sub_contract_return_approved_by;
	global $dateTimes;
	
	$data  	= array('SUB_CONTRACT_RETURN_NO'	=> $serial_no,
					'SUB_CONTRACT_RETURN_YEAR'	=> $serial_year,
					'LEVELS'					=> $status,
					'USER'						=> $userId,
					'DATE'						=> $dateTimes->getCurruntDateTime(),
					'STATUS'					=> 0
					);
	
	$resultArr		= $obj_trn_order_sub_contract_return_approved_by->insert($data); // insert data to approve by table
	return $resultArr;
}

function validateBeforeApprove($serial_no,$serial_year,$programCode,$userId)
{
	global $obj_commonErr;
	global $obj_trn_order_sub_contract_return_header;
	
	$where	= 'SUB_CONTRACT_RETURN_NO = "'.$serial_no.'" AND SUB_CONTRACT_RETURN_YEAR = "'.$serial_year.'" ' ;
	
	$header_result	= $obj_trn_order_sub_contract_return_header->select('*',NULL,$where,NULL,NULL); // get header data
	$headerRow		= mysqli_fetch_array($header_result);
	
	$validateArr 	= $obj_commonErr->get_permision_withApproval_confirm($headerRow['STATUS'],$headerRow['LEVELS'],$userId,$programCode,'RunQuery'); // get approve permission
	return  $validateArr;
}

function validateBeforeCancel($serial_no,$serial_year,$programCode,$userId)
{
	global $obj_commonErr;
	global $obj_trn_order_sub_contract_return_header;
	
	$where	= 'SUB_CONTRACT_RETURN_NO = "'.$serial_no.'" AND SUB_CONTRACT_RETURN_YEAR = "'.$serial_year.'" ' ;
	
	$header_result	= $obj_trn_order_sub_contract_return_header->select('*',NULL,$where,NULL,NULL); // get header data
	$headerRow		= mysqli_fetch_array($header_result);
	
	$validateArr 	= $obj_commonErr->get_permision_withApproval_cancel($headerRow['STATUS'],$headerRow['LEVELS'],$userId,$programCode,'RunQuery'); // get approve permission
	return  $validateArr;
}

function updateGataPassBalance($serial_no,$serial_year,$gatePassNo,$gatePassYear)
{
	global $obj_trn_order_sub_contract_return_details;
	global $obj_trn_order_sub_contract_gate_pass_details;
	
	$response['type']	= true;
	
	$cols	= 'QTY,SALES_ORDER_ID,CUT_NO,SIZE';
	
	$where	= 'SUB_CONTRACT_RETURN_NO = "'.$serial_no.'" AND SUB_CONTRACT_RETURN_YEAR = "'.$serial_year.'" ' ;
	
	$result1	= $obj_trn_order_sub_contract_return_details->select($cols,NULL,$where,NULL,NULL); // get header data
	while($row	= mysqli_fetch_array($result1))
	{
		$data  		= array('BAL_TO_RETURN_QTY' => -$row['QTY']);
		
		$where		= "SUB_CONTRACT_GP_NO = '$gatePassNo' 
						AND SUB_CONTRACT_GP_YEAR = '$gatePassYear' 
						AND SALES_ORDER_ID = '".$row['SALES_ORDER_ID']."' 
						AND CUT_NO = '".$row['CUT_NO']."'
						AND SIZE = '".$row['SIZE']."'";
		$resultArr	= $obj_trn_order_sub_contract_gate_pass_details->upgrade($data,$where);
		if(!$resultArr['status'] && $response['type'])
		{
			$response['type']	= false;
			$response['msg'] 	= $resultArr['msg'];
			$response['sql']	= $resultArr['sql'];	
		}
	}	
	return $response;	
}

function updateStockTransaction($serial_no,$serial_year,$userId)
{
	global $obj_trn_order_sub_contract_return_details;
	global $obj_ware_stocktransactions_fabric;
	global $dateTimes;
	
	$response['type']	= true;
	
	$cols	= 'QTY,
			  SALES_ORDER_ID,
			  CUT_NO,
			  SIZE,
			  SH.ORDER_NO,
			  SH.ORDER_YEAR,
			  OD.intPart,
			  RH.LOCATION_ID';
			  
	$join	= "INNER JOIN trn_order_sub_contract_return_header RH
				ON RH.SUB_CONTRACT_RETURN_NO = trn_order_sub_contract_return_details.SUB_CONTRACT_RETURN_NO
				  AND RH.SUB_CONTRACT_RETURN_YEAR = trn_order_sub_contract_return_details.SUB_CONTRACT_RETURN_YEAR
			  INNER JOIN trn_order_sub_contract_gate_pass_header GH
				ON GH.SUB_CONTRACT_GP_NO = RH.SUB_CONTRACT_GP_NO
				  AND GH.SUB_CONTRACT_GP_YEAR = RH.SUB_CONTRACT_GP_YEAR
			  INNER JOIN trn_order_sub_contract_header SH
				ON SH.SUB_CONTRACT_NO = GH.SUB_CONTRACT_NO
				  AND SH.SUB_CONTRACT_YEAR = GH.SUB_CONTRACT_YEAR
			  INNER JOIN trn_orderdetails OD
				ON OD.intOrderNo = SH.ORDER_NO
				  AND OD.intOrderYear = SH.ORDER_YEAR
				  AND OD.intSalesOrderId = trn_order_sub_contract_return_details.SALES_ORDER_ID";
	
	$where	= "RH.SUB_CONTRACT_RETURN_NO = '$serial_no'
   			     AND RH.SUB_CONTRACT_RETURN_YEAR = '$serial_year'";
	
	$result1	= $obj_trn_order_sub_contract_return_details->select($cols,$join,$where,NULL,NULL); // get header data
	while($row	= mysqli_fetch_array($result1))
	{
		$data  	= array('intLocationId'			=> $row['LOCATION_ID'],
					'strPlace'					=> "Stores",
					'intToLocationId'			=> 0,
					'intDocumentNo'				=> $serial_no,
					'intDocumentYear'			=> $serial_year,
					'intOrderNo'				=> $row['ORDER_NO'],
					'intOrderYear'				=> $row['ORDER_YEAR'],
					'strCutNo'					=> $row['CUT_NO'],
					'intSalesOrderId'			=> $row['SALES_ORDER_ID'],
					'intPart'					=> $row['intPart'],
					'strSize'					=> $row['SIZE'],
					'dblQty'					=> $row['QTY'],
					'strType'					=> "SUB_GATE_PASS_RETURN",
					'intUser'					=> $userId,
					'dtDate'					=> $dateTimes->getCurruntDateTime(),
					'invoiced'					=> 0
					);
	
		$resultArr		= $obj_ware_stocktransactions_fabric->insert($data);
		if(!$resultArr['status'] && $response['type'])
		{
			$response['type']	= false;
			$response['msg'] 	= $resultArr['msg'];
			$response['sql']	= $resultArr['sql'];	
		}
	}	
	return $response;	
}

function reviseGataPassBalance($serial_no,$serial_year,$gatePassNo,$gatePassYear)
{
	global $obj_trn_order_sub_contract_return_details;
	global $obj_trn_order_sub_contract_gate_pass_details;
	
	$response['type']	= true;
	
	$cols	= 'QTY,SALES_ORDER_ID,CUT_NO,SIZE';
	
	$where	= 'SUB_CONTRACT_RETURN_NO = "'.$serial_no.'" AND SUB_CONTRACT_RETURN_YEAR = "'.$serial_year.'" ' ;
	
	$result1	= $obj_trn_order_sub_contract_return_details->select($cols,NULL,$where,NULL,NULL); // get header data
	while($row	= mysqli_fetch_array($result1))
	{
		$data  		= array('BAL_TO_RETURN_QTY' => "+".$row['QTY']);
		
		$where		= "SUB_CONTRACT_GP_NO = '$gatePassNo' 
						AND SUB_CONTRACT_GP_YEAR = '$gatePassYear' 
						AND SALES_ORDER_ID = '".$row['SALES_ORDER_ID']."' 
						AND CUT_NO = '".$row['CUT_NO']."'
						AND SIZE = '".$row['SIZE']."'";
		$resultArr	= $obj_trn_order_sub_contract_gate_pass_details->upgrade($data,$where);
		if(!$resultArr['status'] && $response['type'])
		{
			$response['type']	= false;
			$response['msg'] 	= $resultArr['msg'];
			$response['sql']	= $resultArr['sql'];	
		}
	}	
	return $response;	
}

function reviseStockTransaction($serial_no,$serial_year,$userId)
{
	global $obj_trn_order_sub_contract_return_details;
	global $obj_ware_stocktransactions_fabric;
	global $dateTimes;
	
	$response['type']	= true;
	
	$cols	= 'RH.LOCATION_ID,
			  QTY,
			  SALES_ORDER_ID,
			  CUT_NO,
			  SIZE,
			  SH.ORDER_NO,
			  SH.ORDER_YEAR,
			  OD.intPart';
			  
	$join	= "INNER JOIN trn_order_sub_contract_return_header RH
				ON RH.SUB_CONTRACT_RETURN_NO = trn_order_sub_contract_return_details.SUB_CONTRACT_RETURN_NO
				  AND RH.SUB_CONTRACT_RETURN_YEAR = trn_order_sub_contract_return_details.SUB_CONTRACT_RETURN_YEAR
			  INNER JOIN trn_order_sub_contract_gate_pass_header GH
				ON GH.SUB_CONTRACT_GP_NO = RH.SUB_CONTRACT_GP_NO
				  AND GH.SUB_CONTRACT_GP_YEAR = RH.SUB_CONTRACT_GP_YEAR
			  INNER JOIN trn_order_sub_contract_header SH
				ON SH.SUB_CONTRACT_NO = GH.SUB_CONTRACT_NO
				  AND SH.SUB_CONTRACT_YEAR = GH.SUB_CONTRACT_YEAR
			  INNER JOIN trn_orderdetails OD
				ON OD.intOrderNo = SH.ORDER_NO
				  AND OD.intOrderYear = SH.ORDER_YEAR
				  AND OD.intSalesOrderId = trn_order_sub_contract_return_details.SALES_ORDER_ID";
	
	$where	= "RH.SUB_CONTRACT_RETURN_NO = '$serial_no'
   			     AND RH.SUB_CONTRACT_RETURN_YEAR = '$serial_year'";
	
	$result1	= $obj_trn_order_sub_contract_return_details->select($cols,$join,$where,NULL,NULL); // get header data
	while($row	= mysqli_fetch_array($result1))
	{
		$data  	= array('intLocationId'			=> $row['LOCATION_ID'],
					'strPlace'					=> "Stores",
					'intToLocationId'			=> 0,
					'intDocumentNo'				=> $serial_no,
					'intDocumentYear'			=> $serial_year,
					'intOrderNo'				=> $row['ORDER_NO'],
					'intOrderYear'				=> $row['ORDER_YEAR'],
					'strCutNo'					=> $row['CUT_NO'],
					'intSalesOrderId'			=> $row['SALES_ORDER_ID'],
					'intPart'					=> $row['intPart'],
					'strSize'					=> $row['SIZE'],
					'dblQty'					=> -$row['QTY'],
					'strType'					=> "SUB_GATE_PASS_RETURN_C",
					'intUser'					=> $userId,
					'dtDate'					=> $dateTimes->getCurruntDateTime(),
					'invoiced'					=> 0
					);
	
		$resultArr		= $obj_ware_stocktransactions_fabric->insert($data);
		if(!$resultArr['status'] && $response['type'])
		{
			$response['type']	= false;
			$response['msg'] 	= $resultArr['msg'];
			$response['sql']	= $resultArr['sql'];	
		}
	}	
	return $response;	
}

function validateWiehRetuenToBalQty($serial_no,$serial_year)
{
	global $obj_trn_order_sub_contract_return_details;
	$response['type']	= true;

	$cols	= "GH.SUB_CONTRACT_GP_NO   AS SUB_CONTRACT_GP_NO,
			   GH.SUB_CONTRACT_GP_YEAR AS SUB_CONTRACT_GP_YEAR,
			   SUB_CONTR_JOB_ID        AS SUB_CONTR_JOB_ID,
			   SALES_ORDER_ID          AS SALES_ORDER_ID,
			   OD.strSalesOrderNo  	   AS SALES_ORDER_NO,
			   CUT_NO                  AS CUT_NO,
			   SIZE                    AS SIZE,
			   QTY                     AS QTY ";
	
	$join 	= "INNER JOIN trn_order_sub_contract_return_header H
				ON H.SUB_CONTRACT_RETURN_NO = trn_order_sub_contract_return_details.SUB_CONTRACT_RETURN_NO
				  AND H.SUB_CONTRACT_RETURN_YEAR = trn_order_sub_contract_return_details.SUB_CONTRACT_RETURN_YEAR
			  INNER JOIN trn_order_sub_contract_gate_pass_header GH
				ON GH.SUB_CONTRACT_GP_NO = H.SUB_CONTRACT_GP_NO
				  AND GH.SUB_CONTRACT_GP_YEAR = H.SUB_CONTRACT_GP_YEAR
			  INNER JOIN trn_order_sub_contract_header SH
				ON SH.SUB_CONTRACT_NO = GH.SUB_CONTRACT_NO
				  AND SH.SUB_CONTRACT_YEAR = GH.SUB_CONTRACT_YEAR
			  INNER JOIN trn_orderdetails OD
				ON OD.intOrderNo = SH.ORDER_NO
				  AND OD.intOrderYear = SH.ORDER_YEAR
				  AND OD.intSalesOrderId = trn_order_sub_contract_return_details.SALES_ORDER_ID ";
	
	$where	= 'H.SUB_CONTRACT_RETURN_NO = "'.$serial_no.'" AND H.SUB_CONTRACT_RETURN_YEAR = "'.$serial_year.'" ' ;
	
	$result1	= $obj_trn_order_sub_contract_return_details->select($cols,$join,$where,NULL,NULL); // get header data
	while($row	= mysqli_fetch_array($result1))
	{
		$returnQty		= $row['QTY'];
		$gatePassQty	= checkWithGatePassReturnQty($row['SUB_CONTRACT_GP_NO'],$row['SUB_CONTRACT_GP_YEAR'],$row['SALES_ORDER_ID'],$row['CUT_NO'],$row['SIZE']);
		
		if($returnQty > $gatePassQty && $response['type'])
		{
			$response['type']	= false;
			$response['msg'] 	= "Return Qty should equal or less than GataPass return balance. ".
								  "<br/> Return Qty : ".number_format($returnQty,2)."".
								  "<br/> Gate Pass Qty : ".number_format($gatePassQty,2)."".
								  "<br/> Sales Order No : ".$row['SALES_ORDER_NO']."".
								  "<br/> Cut No : ".$row['CUT_NO']."".
								  " | Size : ".$row['SIZE']."";
		}
	}	
	return $response;
}

function checkWithGatePassReturnQty($gatePassNo,$gatePassYear,$salesId,$cutNo,$size)
{
	global $obj_trn_order_sub_contract_gate_pass_details;
	$amount	= 0;
	
	$cols	= "BAL_TO_RETURN_QTY 	AS BAL_TO_RETURN_QTY ";

	$where	= "SUB_CONTRACT_GP_NO = '".$gatePassNo."' 
			   AND SUB_CONTRACT_GP_YEAR = '".$gatePassYear."' 
			   AND SALES_ORDER_ID = '".$salesId."'
			   AND CUT_NO = '".$cutNo."'
			   AND SIZE = '".$size."'" ;
	
	$result1	= $obj_trn_order_sub_contract_gate_pass_details->select($cols,$join = null,$where,NULL,NULL);
	while($row1	= mysqli_fetch_array($result1))
	{
		$amount = $row1['BAL_TO_RETURN_QTY'];
	}
	return $amount;
}

function validateStockBeforeCancel($serial_no,$serial_year)
{
	global $obj_trn_order_sub_contract_return_details;
	$response['type']	= true;
	
	$cols	= "SH.ORDER_NO				AS ORDER_NO,
		       SH.ORDER_YEAR			AS ORDER_YEAR,
			   SUB_CONTR_JOB_ID        	AS SUB_CONTR_JOB_ID,
			   SALES_ORDER_ID          	AS SALES_ORDER_ID,
			   OD.strSalesOrderNo  	   	AS SALES_ORDER_NO,
			   CUT_NO                  	AS CUT_NO,
			   SIZE                    	AS SIZE,
			   H.LOCATION_ID			AS LOCATION_ID,
			   QTY                     	AS QTY ";
	
	$join 	= "INNER JOIN trn_order_sub_contract_return_header H
				ON H.SUB_CONTRACT_RETURN_NO = trn_order_sub_contract_return_details.SUB_CONTRACT_RETURN_NO
				  AND H.SUB_CONTRACT_RETURN_YEAR = trn_order_sub_contract_return_details.SUB_CONTRACT_RETURN_YEAR
			  INNER JOIN trn_order_sub_contract_gate_pass_header GH
				ON GH.SUB_CONTRACT_GP_NO = H.SUB_CONTRACT_GP_NO
				  AND GH.SUB_CONTRACT_GP_YEAR = H.SUB_CONTRACT_GP_YEAR
			  INNER JOIN trn_order_sub_contract_header SH
				ON SH.SUB_CONTRACT_NO = GH.SUB_CONTRACT_NO
				  AND SH.SUB_CONTRACT_YEAR = GH.SUB_CONTRACT_YEAR
			  INNER JOIN trn_orderdetails OD
				ON OD.intOrderNo = SH.ORDER_NO
				  AND OD.intOrderYear = SH.ORDER_YEAR
				  AND OD.intSalesOrderId = trn_order_sub_contract_return_details.SALES_ORDER_ID ";
	
	$where	= 'H.SUB_CONTRACT_RETURN_NO = "'.$serial_no.'" AND H.SUB_CONTRACT_RETURN_YEAR = "'.$serial_year.'" ' ;
	
	$result1	= $obj_trn_order_sub_contract_return_details->select($cols,$join,$where,NULL,NULL);
	while($row = mysqli_fetch_array($result1))
	{
		$returnQty		= $row['QTY'];
		$stockBalance	= getStockBalance($row['ORDER_NO'],$row['ORDER_YEAR'],$row['SALES_ORDER_ID'],$row['CUT_NO'],$row['SIZE'],$row['LOCATION_ID']);
		
		if($returnQty > $stockBalance && $response['type'])
		{
			$response['type']	= false;
			$response['msg'] 	= "No enough stock balance available to 'Cancel'. ".
								  "<br/> Return Qty : ".number_format($returnQty,2)."".
								  "<br/> Stock Balance : ".number_format($stockBalance,2)."".
								  "<br/> Sales Order No : ".$row['SALES_ORDER_NO']."".
								  "<br/> Cut No : ".$row['CUT_NO']."".
								  " | Size : ".$row['SIZE']."";
		}
	}
	//die(print_r($response));
	return $response;
}

function getStockBalance($orderNo,$orderYear,$salesId,$cutNo,$size,$locationId)
{ 
	global $obj_ware_stocktransactions_fabric;
	$amount	= 0;
	
	$cols	= "SUM(dblQty)	AS STOCK_QTY ";

	$where	= "intLocationId = '$locationId'
				AND intOrderNo = '$orderNo'
				AND intOrderYear = '$orderYear'
				AND intSalesOrderId = '$salesId'
				AND strCutNo = '$cutNo'
				AND strSize = '$size'" ;
	
	$result1	= $obj_ware_stocktransactions_fabric->select($cols,$join = null,$where,NULL,NULL);
	while($row1	= mysqli_fetch_array($result1))
	{
		$amount = $row1['STOCK_QTY'];
	}
	return $amount;
}
?>
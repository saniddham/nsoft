<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php 
$location 				= $_SESSION['CompanyID'];
$userId  				= $_SESSION["userId"];

$status					= '';
$levels					= '';
$sub_con_GP_year		= '';
$sub_con_GP_no			= '';
$order_no				= '';
$order_year				= '';
$sub_con_no				= '';
$sub_con_year			= '';

include_once "class/cls_commonErrorHandeling_get.php";						$obj_commonErr									= new cls_commonErrorHandeling_get($db);
include_once "class/customerAndOperation/cls_customer_and_operation.php";	$obj_customer_and_operation						= new cls_customer_and_operation($db);
include_once "class/tables/trn_order_sub_contract_return_header.php";		$obj_trn_order_sub_contract_return_header		= new trn_order_sub_contract_return_header($db);
include_once "class/tables/trn_order_sub_contract_return_details.php";		$obj_trn_order_sub_contract_return_details		= new trn_order_sub_contract_return_details($db);
include_once "class/tables/trn_order_sub_contract_gate_pass_header.php";	$obj_trn_order_sub_contract_gate_pass_header	= new trn_order_sub_contract_gate_pass_header($db);

$programCode			= 'P0877';
$serialNo				= (!isset($_REQUEST['serial_no'])?'':$_REQUEST['serial_no']);
$serialYear				= (!isset($_REQUEST['serial_year'])?'':$_REQUEST['serial_year']);
$remarks				= '';
?>
<title>Sub Contract GatePass Return</title>
<form id="frmSubContractReturn" name="frmSubContractReturn" method="post" action="">
<?php
			$db->connect();
			
			if($serialNo!='' && $serialYear!='')
			{
				$obj_trn_order_sub_contract_return_header->set($serialNo,$serialYear);
				
				$status					= $obj_trn_order_sub_contract_return_header->getSTATUS();
				$levels					= $obj_trn_order_sub_contract_return_header->getLEVELS();
				$sub_con_GP_no			= $obj_trn_order_sub_contract_return_header->getSUB_CONTRACT_GP_NO();
				$sub_con_GP_year		= $obj_trn_order_sub_contract_return_header->getSUB_CONTRACT_GP_YEAR();
				$order_no				= $obj_trn_order_sub_contract_return_header->getORDER_NO();
				$order_year				= $obj_trn_order_sub_contract_return_header->getORDER_YEAR();
				$sub_con_no				= $obj_trn_order_sub_contract_return_header->getSUB_CONTRACT_NO();
				$sub_con_year			= $obj_trn_order_sub_contract_return_header->getSUB_CONTRACT_YEAR();
				$remarks				= $obj_trn_order_sub_contract_return_header->getREMARKS();				
				$detail_result			= $obj_customer_and_operation->getSubContractReturnDetail($serialNo,$serialYear,'RunQuery'); // load detail data
			}

			$permition_arr		= $obj_commonErr->get_permision_withApproval_save($status,$levels,$userId,$programCode,'RunQuery');
			$permision_save		= $permition_arr['permision']; // check approve permission

			$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($status,$levels,$userId,$programCode,'RunQuery');
			$permision_confirm	= $permition_arr['permision']; // check reject permission

			$permition_arr		= $obj_commonErr->get_permision_withApproval_cancel($status,$levels,$userId,$programCode,'RunQuery');
			$permision_cancel	= $permition_arr['permision']; // check cancel permission
			
			$db->disconnect();	
		?>
       
<div align="center">
	<div class="trans_layoutL">
	<div class="trans_text">Sub Contract GatePass Return</div>
    	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        	<tr>
            	<td>
                	<table width="100%" border="0">
                        <tr class="normalfnt">
                        	<td width="15%">Serial No</td>
                        	<td width="47%"><input name="txtReturnYear" type="text" disabled="disabled" id="txtReturnYear" style="width:60px" value="<?php echo $serialYear ?>" />&nbsp;<input name="txtReturnNo" type="text" disabled="disabled" id="txtReturnNo" style="width:90px"value="<?php echo $serialNo ?>" /></td>
                        	<td width="14%">Date</td>
                        	<td width="24%"><input name="dtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="dtDate" style="width:100px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                        </tr>
                        <tr class="normalfnt">
                        	<td width="15%">Gate Pass No <span class="compulsoryRed">*</span></td>
                        	<td width="47%"><select name="cboGPYear" id="cboGPYear" style="width:64px" class="validate[required]">
                            <option value=""></option>
                            <?php
							$result		= getGatePassYear();
							while($row	= mysqli_fetch_array($result))
							{
								if($row['SUB_CONTRACT_GP_YEAR']==$sub_con_GP_year)
									echo "<option selected=\"selected\" value=\"".$row['SUB_CONTRACT_GP_YEAR']."\">".$row['SUB_CONTRACT_GP_YEAR']."</option>";
								else if($row['SUB_CONTRACT_GP_YEAR']==date('Y'))
									echo "<option selected=\"selected\" value=\"".$row['SUB_CONTRACT_GP_YEAR']."\">".$row['SUB_CONTRACT_GP_YEAR']."</option>";
								else
									echo "<option value=\"".$row['SUB_CONTRACT_GP_YEAR']."\">".$row['SUB_CONTRACT_GP_YEAR']."</option>";			 
							}
							?>
                        	</select>&nbsp;<select name="cboGPNo" id="cboGPNo" style="width:94px" class="validate[required]">
                            <option value=""></option>
                            <?php
							$result		= getGatePassNo($sub_con_GP_year);
							while($row	= mysqli_fetch_array($result))
							{
								if($row['SUB_CONTRACT_GP_NO']==$sub_con_GP_no)
									echo "<option selected=\"selected\" value=\"".$row['SUB_CONTRACT_GP_NO']."\">".$row['SUB_CONTRACT_GP_NO']."</option>";
								else
									echo "<option value=\"".$row['SUB_CONTRACT_GP_NO']."\">".$row['SUB_CONTRACT_GP_NO']."</option>";
							}
							?>
                        	</select></td>
                        	<td width="14%">Order No</td>
                        	<td width="24%" class="clsOrderNo"><a href="?q=896&orderNo=<?php echo $order_no;?>&orderYear=<?php echo $order_year; ?>" target="rptBulkOrder.php"><?php echo ($order_no==''?'':$order_no.'/'.$order_year); ?></a></td>
                        </tr>
                        <tr class="normalfnt">
                          <td valign="top">Remarks</td>
                          <td><textarea name="txtRemarks" id="txtRemarks" cols="45" rows="3"><?php echo $remarks;?></textarea></td>
                          <td valign="top">Sub Contract No</td>
                          <td valign="top" class="clsSubContractNo"><a href="?q=907&serialNo=<?php echo $sub_con_no;?>&serialYear=<?php echo $sub_con_year; ?>" target="rpt_sub_contract_po.php"><?php echo ($sub_con_no==''?'':$sub_con_no.'/'.$sub_con_year);?></a></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
            	<td>
                <div style="height:280px;overflow:scroll" >
                    <table width="100%" class="bordered" id="tblMain" >
                    <thead>
                        <tr>
                            <th width="20%" >Sales Order No</th>
                            <th width="20%" >Job Type</th>
                            <th width="14%" >Cut No</th>
                            <th width="10%" >Size</th>
                            <th width="12%" >GP Qty</th>
                            <th width="12%" >Ruturned Qty</th>
                            <th width="12%" >Qty</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
					if($serialNo!='' && $serialYear!='')
					{
						while($row=mysqli_fetch_array($detail_result))
						{
							$returnedQty	= $row['GATEPASS_QTY']-$row['BAL_TO_RETURN_QTY'];
						?>
							<tr class="normalfnt">
								<td style="text-align:left" id="<?php echo $row['SALES_ORDER_ID']; ?>" class="clsSalesOrderNo"><?php echo $row['strSalesOrderNo']; ?></td>
								<td style="text-align:left" id="<?php echo $row['SUB_CONTR_JOB_ID']; ?>" class="clsSubConJobType"><?php echo $row['subConJobType']; ?></td>
								<td style="text-align:left" class="clsCutNo"><?php echo $row['CUT_NO']; ?></td>
								<td style="text-align:left" class="clsSize"><?php echo $row['SIZE']; ?></td>
								<td style="text-align:right" class="clsGPQty" ><?php echo $row['GATEPASS_QTY']; ?></td>
								<td style="text-align:right" class="clsReturnedQty" ><?php echo $returnedQty; ?></td>
								<td style="text-align:center"><input name="txtReturnQty" type="text" id="txtReturnQty" style="width:100%;text-align:right" value="<?php echo $row['qty']; ?>" class="clsReturnQty validate[custom[number]]" /></td>
							</tr>
					<?php
						}
					}
					?>
                    </tbody>
                    </table>
                </div>
                </td>
            </tr>
            <tr>
                <td height="34">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                        <tr>
                        	<td align="center"><a class="button white medium" id="butNew">New</a><a class="button white medium" id="butSave" <?php if($permision_save!=1){ ?>style="display:none"<?php } ?>>Save</a><a class="button white medium" id="butConfirm"  <?php if($permision_confirm!=1){ ?> style="display:none"<?php } ?>>Approve</a><a class="button white medium" id="butCancel" <?php if($permision_cancel!=1){ ?>  style="display:none"<?php } ?>>Cancel</a><a class="button white medium" id="butReport">Report</a><a href="main.php" class="button white medium" id="butClose">Close</a></td>
                        </tr>
                    </table>
                </td>	
            </tr>
        </table>
    </div>
</div>
</form>
<?php
function getGatePassYear()
{
	global $db;
	global $obj_trn_order_sub_contract_gate_pass_header;
	$db->connect();
	
	$cols	= " DISTINCT trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_YEAR ";
	
	$join	= " INNER JOIN trn_order_sub_contract_gate_pass_details SCGPD ON SCGPD.SUB_CONTRACT_GP_NO=trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_NO AND
				SCGPD.SUB_CONTRACT_GP_YEAR=trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_YEAR ";
	
	$where	= " STATUS = 1 AND
				SCGPD.BAL_TO_RETURN_QTY > 0 ";
	
	$result = $obj_trn_order_sub_contract_gate_pass_header->select($cols,$join,$where,$order = null, $limit = null);	
	$db->disconnect();
	
	return $result;
}
function getGatePassNo($gatePassYear)
{
	global $db;
	global $location;
	global $obj_trn_order_sub_contract_gate_pass_header;
	$db->connect();
	
	$cols	= " DISTINCT trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_NO ";
	
	$join	= " INNER JOIN trn_order_sub_contract_gate_pass_details SCGPD ON SCGPD.SUB_CONTRACT_GP_NO=trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_NO AND
				SCGPD.SUB_CONTRACT_GP_YEAR=trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_YEAR ";
				
	$where	= " trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_YEAR = '".$gatePassYear."' AND
				STATUS = 1 AND
				LOCATION_ID = '".$location."' AND
				SCGPD.BAL_TO_RETURN_QTY > 0 ";
	
	$result = $obj_trn_order_sub_contract_gate_pass_header->select($cols,$join,$where,$order = null, $limit = null);	
	$db->disconnect();
	
	return $result;
	
}
?>
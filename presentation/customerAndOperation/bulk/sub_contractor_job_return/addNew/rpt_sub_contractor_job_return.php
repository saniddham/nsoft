<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$intUser  				= $sessions->getUserId();

$serial_no 				= $_REQUEST['serial_no'];
$serial_year 			= $_REQUEST['serial_year'];
$mode 					= $_REQUEST['mode'];

require_once 			"class/tables/trn_order_sub_contract_return_header.php";
require_once 			"class/tables/trn_order_sub_contract_return_details.php";
require_once 			"class/cls_commonErrorHandeling_get.php";
require_once 			"class/tables/trn_order_sub_contract_return_approved_by.php";

$obj_header				= new trn_order_sub_contract_return_header($db);
$obj_detail				= new trn_order_sub_contract_return_details($db);
$obj_commonErr			= new cls_commonErrorHandeling_get($db);
$obj_approve_by			= new trn_order_sub_contract_return_approved_by($db);

$programCode			= 'P0877';

$select_h				="  trn_order_sub_contract_return_header.SUB_CONTRACT_RETURN_NO,
							trn_order_sub_contract_return_header.SUB_CONTRACT_RETURN_YEAR,
							trn_order_sub_contract_return_header.SUB_CONTRACT_GP_NO,
							  trn_order_sub_contract_return_header.SUB_CONTRACT_GP_YEAR,
							  GH.SUB_CONTRACT_NO,
							  GH.SUB_CONTRACT_YEAR,
							  trn_order_sub_contract_header.ORDER_NO,
							  trn_order_sub_contract_header.ORDER_YEAR,
							  mst_subcontractor.SUB_CONTRACTOR_NAME,
							  trn_order_sub_contract_return_header.DATE,
							  trn_orderheader.strCustomerPoNo,
							  mst_customer.strName                                      AS CUSTOMER,
							  trn_order_sub_contract_return_header.REMARKS,
							  trn_order_sub_contract_return_header.CREATED_BY,
							  trn_order_sub_contract_return_header.CREATED_DATE,
							  sys_users.strUserName                                     AS USER_NAME,
							  trn_orderheader.intCustomer,
							  trn_order_sub_contract_header.SUB_CONTRACTOR_ID,
							  trn_order_sub_contract_return_header.`STATUS`,
							  trn_order_sub_contract_return_header.LEVELS,
							  trn_order_sub_contract_return_header.LOCATION_ID";
							
	$join_h				="INNER JOIN trn_order_sub_contract_gate_pass_header GH
							ON GH.SUB_CONTRACT_GP_NO = trn_order_sub_contract_return_header.SUB_CONTRACT_GP_NO
							  AND GH.SUB_CONTRACT_GP_YEAR = trn_order_sub_contract_return_header.SUB_CONTRACT_GP_YEAR
						  INNER JOIN trn_order_sub_contract_header
							ON GH.SUB_CONTRACT_NO = trn_order_sub_contract_header.SUB_CONTRACT_NO
							  AND GH.SUB_CONTRACT_YEAR = trn_order_sub_contract_header.SUB_CONTRACT_YEAR
						  INNER JOIN mst_subcontractor
							ON trn_order_sub_contract_header.SUB_CONTRACTOR_ID = mst_subcontractor.SUB_CONTRACTOR_ID
						  INNER JOIN trn_orderheader
							ON trn_order_sub_contract_header.ORDER_NO = trn_orderheader.intOrderNo
							  AND trn_order_sub_contract_header.ORDER_YEAR = trn_orderheader.intOrderYear
						  INNER JOIN mst_customer
							ON trn_orderheader.intCustomer = mst_customer.intId
						  INNER JOIN sys_users
							ON trn_order_sub_contract_return_header.CREATED_BY = sys_users.intUserId";
	
	$where_h			="	trn_order_sub_contract_return_header.SUB_CONTRACT_RETURN_NO = '$serial_no' AND
							trn_order_sub_contract_return_header.SUB_CONTRACT_RETURN_YEAR = '$serial_year'";	
												
	$header_result		= $obj_header->select($select_h,$join_h,$where_h,NULL,NULL);
	 while($header_array=mysqli_fetch_array($header_result))
	 {
		$header_array2	=$header_array;
		$serialNo 		= $header_array['SUB_CONTRACT_RETURN_NO'];
		$serialYear 	= $header_array['SUB_CONTRACT_RETURN_YEAR'];
		$gatePassNo 	= $header_array['SUB_CONTRACT_GP_NO'];
		$gatePassYear 	= $header_array['SUB_CONTRACT_GP_YEAR'];
		$sub_order_no	= $header_array['SUB_CONTRACT_NO'];
		$sub_order_year	= $header_array['SUB_CONTRACT_YEAR'];
		$custPO 		= $header_array['strCustomerPoNo'];
		$orderNo 		= $header_array['ORDER_NO'];
		$orderYear 		= $header_array['ORDER_YEAR'];
		$date 			= $header_array['DATE'];
		$customer 		= $header_array['CUSTOMER'];
		$remarks 		= $header_array['REMARKS'];
		$intStatus		= $header_array['STATUS'];
		$savedLevels 	= $header_array['LEVELS'];
		$user 			= $header_array['USER_NAME'];
		$locationId 	= $header_array['LOCATION_ID'];//this locationId use in report header(reportHeader.php)--------------------
		$locName 		= $header_array['SUB_CONTRACTOR_NAME'];
	 }
	
	$header_array		= $header_array2 ;		 
 	
	$permition_arr		= $obj_commonErr->get_permision_withApproval_save($intStatus,$savedLevels,$intUser,$programCode,'RunQuery');
	$permision_save		= $permition_arr['permision'];
	$permition_arr		= $obj_commonErr->get_permision_withApproval_cancel($intStatus,$savedLevels,$intUser,$programCode,'RunQuery');
	$permision_cancel	= $permition_arr['permision'];
	$permition_arr		= $obj_commonErr->get_permision_withApproval_reject($intStatus,$savedLevels,$intUser,$programCode,'RunQuery');
	$permision_reject	= $permition_arr['permision'];
	$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($intStatus,$savedLevels,$intUser,$programCode,'RunQuery');
	$permision_confirm	= $permition_arr['permision'];


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sub Contractor Gate Pass Return Report</title>
<script type="application/javascript" src="presentation/customerAndOperation/bulk/sub_contractor_job_return/addNew/sub_contractor_job_return_js.js"></script>
<style>
.break {
	page-break-before: always;
}
 @media print {
.noPrint {
	display: none;
}
}
#apDiv1 {
	position: absolute;
	left: 282px;
	top: 153px;
	width: 650px;
	height: 301px;
	z-index: 1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="images/pending.png"  /></div>
<?php
}
?>
<form id="frmSubJobGPReturnReport" name="frmSubJobGPReturnReport">
  <table width="800" cellpadding="0" cellspacing="0" align="center">
    <tr>
      <td><?php include 'reportHeader.php'?></td>
    </tr>
    <tr>
      <td class="reportHeader" align="center">SUB CONTRACTOR GATE PASS RETURN REPORT</td>
    </tr>
    <tr>
      <td class="reportHeader" align="center">&nbsp;</td>
    </tr>
    <?php
		include "presentation/report_approve_status_and_buttons.php"
     ?>
    <tr>
      <td><table width="900" border="0" align="center" bgcolor="#FFFFFF">
          <tr>
            <td><table width="100%">
                <tr>
                  <td width="1%">&nbsp;</td>
                  <td width="11%"><span class="normalfnt"><strong>Return No</strong></span></td>
                  <td width="1%" align="center" valign="middle"><strong>:</strong></td>
                  <td width="14%"><span class="normalfnt"><?php echo $serial_no  ?>/<?php echo $serial_year ?></span></td>
                  <td width="14%" class="normalfnt"><strong>Sub Contract No</strong></td>
                  <td width="1%" align="center" valign="middle"><strong>:</strong></td>
                  <td width="10%"><span class="normalfnt"><?php echo $sub_order_no  ?>/<?php echo $sub_order_year ?></span></td>
                  <td width="11%" class="normalfnt"><div id="divSerialNo" style="display:none"><?php echo $serialNo ?>/<?php echo $year ?></div>
                    <strong>Order No</strong></td>
                  <td width="1%"><strong>:</strong></td>
                  <td width="17%"><span class="normalfnt"><?php echo $orderNo ?>/<?php echo $orderYear ?></span></td>
                </tr>
                <tr>
                  <td width="1%">&nbsp;</td>
                  <td width="11%" class="normalfnt"><b>Gate Pass No</b></td>
                  <td width="1%" align="center" valign="middle"><strong>:</strong></td>
                  <td width="14%"><span class="normalfnt"><?php echo $gatePassNo.'/'.$gatePassYear; ?></span></td>
                  <td width="14%" class="normalfnt"><strong>Customer PO</strong></td>
                  <td width="1%" align="center" valign="middle"><strong>:</strong></td>
                  <td width="10%"><span class="normalfnt"><?php echo $custPO   ?></span></td>
                  <td width="11%" class="normalfnt"><div id="divSerialNo" style="display:none"></div>
                    <strong>Customer</strong></td>
                  <td width="1%"><strong>:</strong></td>
                  <td width="17%"><span class="normalfnt"><?php echo $customer; ?></span></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td class="normalfnt"><strong>Date</strong></td>
                  <td align="center" valign="middle"><strong>:</strong></td>
                  <td><span class="normalfnt"><?php echo $date; ?></span></td>
                  <td  class="normalfnt"><strong></strong></td>
                  <td align="center" valign="middle"><strong></strong></td>
                  <td><span class="normalfnt">
                    <?php //echo $user; ?>
                    </span></td>
                  <td class="normalfnt">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td class="normalfnt" valign="top"><strong>Remarks</strong></td>
                  <td align="center" valign="top"><strong>:</strong></td>
                  <td colspan="7"><textarea cols="50" rows="4" class="textarea" style="width::300px" disabled="disabled"><?php echo $remarks  ?></textarea></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td><table width="100%">
                <tr>
                  <td width="5%">&nbsp;</td>
                  <td colspan="7" class="normalfnt"><table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
                      <tr class="">
                        <th width="17%" >Sales Order No</th>
                        <th width="22%" >Background Color</th>
                        <th width="12%" >Part</th>
                        <th width="12%" >Cut No</th>
                        <th width="11%" >Size</th>
                        <th width="15%" >Line No</th>
                        <th width="11%" >Qty</th>
                      </tr>
                      <?php 
			$select_d 	="  trn_orderdetails.strSalesOrderNo,
						  	trn_order_sub_contract_return_details.CUT_NO,
						 	mst_part.strName                             AS part,
						  	mst_colors_ground.strName                    AS bgcolor,
						  	trn_orderdetails.strLineNo,
						  	trn_order_sub_contract_return_details.SIZE,
						  	trn_order_sub_contract_return_details.QTY ";
							
			$join_d		="INNER JOIN trn_order_sub_contract_return_header
							ON trn_order_sub_contract_return_header.SUB_CONTRACT_RETURN_NO = trn_order_sub_contract_return_details.SUB_CONTRACT_RETURN_NO
							  AND trn_order_sub_contract_return_header.SUB_CONTRACT_RETURN_YEAR = trn_order_sub_contract_return_details.SUB_CONTRACT_RETURN_YEAR
						  INNER JOIN trn_order_sub_contract_gate_pass_header
							ON trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_NO = trn_order_sub_contract_return_header.SUB_CONTRACT_GP_NO
							  AND trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_GP_YEAR = trn_order_sub_contract_return_header.SUB_CONTRACT_GP_YEAR
						  INNER JOIN trn_order_sub_contract_header
							ON trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_NO = trn_order_sub_contract_header.SUB_CONTRACT_NO
							  AND trn_order_sub_contract_gate_pass_header.SUB_CONTRACT_YEAR = trn_order_sub_contract_header.SUB_CONTRACT_YEAR
						  INNER JOIN trn_orderdetails
							ON trn_order_sub_contract_header.ORDER_NO = trn_orderdetails.intOrderNo
							  AND trn_order_sub_contract_header.ORDER_YEAR = trn_orderdetails.intOrderYear
							  AND trn_order_sub_contract_return_details.SALES_ORDER_ID = trn_orderdetails.intSalesOrderId
						  INNER JOIN mst_subcontractor
							ON trn_order_sub_contract_header.SUB_CONTRACTOR_ID = mst_subcontractor.SUB_CONTRACTOR_ID
						   INNER JOIN ware_fabricreceivedheader FH
							ON FH.intOrderNo = trn_order_sub_contract_header.ORDER_NO
							AND FH.intOrderYear = trn_order_sub_contract_header.ORDER_YEAR
						  INNER JOIN ware_fabricreceiveddetails FD
							ON FD.intFabricReceivedNo = FH.intFabricReceivedNo
							  AND FD.intFabricReceivedYear = FH.intFabricReceivedYear
							  AND FD.strCutNo = trn_order_sub_contract_return_details.CUT_NO
							  AND FD.intSalesOrderId = trn_order_sub_contract_return_details.SALES_ORDER_ID
							  AND FD.strSize = trn_order_sub_contract_return_details.SIZE
						  LEFT JOIN mst_part
							ON trn_orderdetails.intPart = mst_part.intId
						  LEFT JOIN mst_colors_ground
							ON FD.intGroundColor = mst_colors_ground.intId";
			
			$where_d	="	trn_order_sub_contract_return_header.SUB_CONTRACT_RETURN_NO = '$serial_no' AND
							trn_order_sub_contract_return_header.SUB_CONTRACT_RETURN_YEAR = '$serial_year' ";	
												
		$details_result	= $obj_detail->select($select_d,$join_d,$where_d,NULL,NULL);
			$totQty=0;
			$totAmmount=0;
			while($row=mysqli_fetch_array($details_result))
			{
				$cutNo			=$row['CUT_NO'];
				$salesOrderNo	=$row['strSalesOrderNo'];
				$part			=$row['part'];
				$bgColor		=$row['bgcolor'];
				$lineNo			=$row['strLineNo'];
				$size			=$row['SIZE'];
				$Qty			=$row['QTY'];
	  ?>
                      <tr class="normalfnt"   bgcolor="#FFFFFF">
                        <td align="center" class="normalfnt" id="<?php echo $salesOrderNo; ?>" ><?php echo $salesOrderNo; ?></td>
                        <td align="center" class="normalfnt" id="<?php echo $bgColor; ?>" ><?php echo $bgColor; ?></td>
                        <td align="center" class="normalfnt" id="<?php echo $part; ?>"><?php echo $part; ?></td>
                        <td align="center" class="normalfnt" id="<?php echo $cutNo; ?>" ><?php echo $cutNo; ?></td>
                        <td align="center" class="normalfnt" id="<?php echo $size; ?>" ><?php echo $size; ?></td>
                        <td align="center" class="normalfnt" id="<?php echo $lineNo; ?>" ><?php echo $lineNo; ?></td>                        
                        <td align="center" class="normalfntRight" id="<?php echo $qty; ?>"><?php echo number_format($Qty,2); ?></td>
                      </tr>
                      <?php 
			$total+=$Qty;
			}
	  ?>
                      <tr class="normalfnt" style="font-weight:bold">
                        <td colspan="6" style="text-align:right">&nbsp;Total&nbsp;</td>
                        <td style="text-align:right"><?php echo number_format($total,2) ?></td>
                      </tr>
                    </table></td>
                  <td width="6%">&nbsp;</td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td><?php
			$creator		= $header_array['USER_NAME'];
			$createdDate	= $header_array['CREATED_DATE'];
			$select_h		=" trn_order_sub_contract_return_approved_by.DATE AS dtApprovedDate ,
							   sys_users.strUserName as UserName,
							   trn_order_sub_contract_return_approved_by.LEVELS as intApproveLevelNo
";
							
			$join_h	 		=" INNER JOIN sys_users ON trn_order_sub_contract_return_approved_by.USER = sys_users.intUserId
							";
			$where_h		="
							trn_order_sub_contract_return_approved_by.SUB_CONTRACT_RETURN_NO = '$serial_no' AND
							trn_order_sub_contract_return_approved_by.SUB_CONTRACT_RETURN_YEAR = '$serial_year' 
							order by trn_order_sub_contract_return_approved_by.DATE ASC
							";	
												
			$resultA		= $obj_approve_by->select($select_h,$join_h,$where_h,NULL,NULL);
			include "presentation/report_approvedBy_details.php"
 	?></td>
          </tr>
          <tr height="40">
            <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
          </tr>
        </table></td>
    </tr>
  </table>
</form>
</body>
</html>
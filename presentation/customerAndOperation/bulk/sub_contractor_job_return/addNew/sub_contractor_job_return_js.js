$(document).ready(function(){
	
	$('#frmSubContractReturn').validationEngine();	
	
	$('#frmSubContractReturn #cboGPYear').die('change').live('change',loadGatePassNo);
	$('#frmSubContractReturn #cboGPNo').die('change').live('change',loadDetails);
	$('#frmSubContractReturn #butNew').die('click').live('click',clearAll);
	$('#frmSubContractReturn #butSave').die('click').live('click',saveData);
	$('#frmSubContractReturn .clsReturnQty').die('keyup').live('keyup',validateReturnQty);
	
	if($('#frmSubContractReturn #txtReturnNo').val()=='')
		$('#frmSubContractReturn #cboGPYear').change();
	
	$('#frmSubContractReturn #butConfirm').die('click').live('click',approveReport);
	$('#frmSubContractReturn #butReport').die('click').live('click',viewReport);
	$('#frmSubContractReturn #butCancel').die('click').live('click',cancelReport);
	
	$('#frmSubJobGPReturnReport #butRptConfirm').die('click').live('click',approve);
	$('#frmSubJobGPReturnReport #butRptReject').die('click').live('click',reject);
	$('#frmSubJobGPReturnReport #butRptCancel').die('click').live('click',cancel);
});

function approveReport()
{
	if($('#frmSubContractReturn #txtReturnNo').val()=="") return;
	
	var url = "?q=905&serial_no="+$('#frmSubContractReturn #txtReturnNo').val()+"&serial_year="+$('#frmSubContractReturn #txtReturnYear').val()+"&mode=Confirm";
	window.open(url,'rpt_sub_contractor_job_return.php');
}

function viewReport()
{
	if($('#frmSubContractReturn #txtReturnNo').val()=="") return;
	
	var url = "?q=905&serial_no="+$('#frmSubContractReturn #txtReturnNo').val()+"&serial_year="+$('#frmSubContractReturn #txtReturnYear').val()+"";
	window.open(url,'rpt_sub_contractor_job_return.php');
}


function cancelReport()
{
	if($('#frmSubContractReturn #txtReturnNo').val()=="") return;
	
	var url = "?q=905&serial_no="+$('#frmSubContractReturn #txtReturnNo').val()+"&serial_year="+$('#frmSubContractReturn #txtReturnYear').val()+"&mode=Cancel";
	window.open(url,'rpt_sub_contractor_job_return.php');
}

function loadGatePassNo()
{
	$("#tblMain tr:gt(0)").remove();
	if($('#frmSubContractReturn #cboGPYear').val()=='')
	{
		$('#frmSubContractReturn #cboGPNo').html('');
		$('#frmSubContractReturn .clsOrderNo').html('');
		$('#frmSubContractReturn .clsSubContractNo').html('');
		return;
	}
	
	var url 	= "presentation/customerAndOperation/bulk/sub_contractor_job_return/addNew/sub_contractor_job_return_db.php?requestType=loadGatePassNo";
	var data 	= "gatePassYear="+$('#frmSubContractReturn #cboGPYear').val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmSubContractReturn #cboGPNo').html(json.html);
			}
	});		
}

function loadDetails()
{
	$("#tblMain tr:gt(0)").remove();
	if($('#frmSubContractReturn #cboGPNo').val()=='')
	{
		$('#frmSubContractReturn .clsOrderNo').html('');
		$('#frmSubContractReturn .clsSubContractNo').html('');
		return;
	}
	showWaiting();
	var url 	= "presentation/customerAndOperation/bulk/sub_contractor_job_return/addNew/sub_contractor_job_return_db.php?requestType=loadDetails";
	var data 	= "gatePassYear="+$('#frmSubContractReturn #cboGPYear').val()+"&gatePassNo="+$('#frmSubContractReturn #cboGPNo').val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmSubContractReturn .clsOrderNo').html('<a href="?q=896&orderNo='+json.orderNo+'&orderYear='+json.orderYear+'" target="rptBulkOrder.php">'+json.orderNo+'/'+json.orderYear+'</a>');
				$('#frmSubContractReturn .clsSubContractNo').html('<a href="?q=907&serialNo='+json.subConNo+'&serialYear='+json.subConYear+'" target="rpt_sub_contract_po.php">'+json.subConNo+'/'+json.subConYear+'</a>');
				
				var lengthDetail   = json.arrDetailData.length;
				var arrDetailData  = json.arrDetailData;	
				for(var i=0;i<lengthDetail;i++)
				{
					var salesOrderId	= arrDetailData[i]['SALES_ORDER_ID'];
					var salesOrderNo	= arrDetailData[i]['SALES_ORDER_NO'];	
					var subJobId		= arrDetailData[i]['SUB_CONTR_JOB_ID'];
					var subJobType		= arrDetailData[i]['SUB_CON_JOB_TYPE'];
					var cutNo			= arrDetailData[i]['CUT_NO'];
					var size			= arrDetailData[i]['SIZE'];
					var qty				= arrDetailData[i]['QTY'];
					var returnedQty		= arrDetailData[i]['RETURNED_QTY'];
					
					createGrid(salesOrderId,salesOrderNo,subJobId,subJobType,cutNo,size,qty,returnedQty);
				}	
			}
	});
	hideWaiting();	
}

function createGrid(salesOrderId,salesOrderNo,subJobId,subJobType,cutNo,size,qty,returnedQty)
{
	var tbl 		= document.getElementById('tblMain');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);
	
	var cell 		= row.insertCell(0);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsSalesOrderNo';
	cell.innerHTML 	= salesOrderNo;
	cell.id		 	= salesOrderId;
	
	var cell 		= row.insertCell(1);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsSubConJobType';
	cell.innerHTML 	= subJobType;
	cell.id		 	= subJobId;
	
	var cell 		= row.insertCell(2);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsCutNo';
	cell.innerHTML 	= cutNo;
	
	var cell 		= row.insertCell(3);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsSize';
	cell.innerHTML 	= size;
	
	var cell 		= row.insertCell(4);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsGPQty';
	cell.innerHTML 	= qty;
	
	var cell 		= row.insertCell(5);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsReturnedQty';
	cell.innerHTML 	= returnedQty;
	
	var cell 		= row.insertCell(6);
	cell.setAttribute("style",'text-align:center');
	cell.innerHTML 	= "<input name=\"txtReturnQty\" type=\"text\" id=\"txtReturnQty\" style=\"width:100%;text-align:right\" value=\"\" class=\"clsReturnQty validate[custom[number]]\" />";
}
function saveData()
{
	var serialNo		= $('#frmSubContractReturn #txtReturnNo').val();
	var serialYear		= $('#frmSubContractReturn #txtReturnYear').val();
	var date			= $('#frmSubContractReturn #dtDate').val();
	var gatePassNo		= $('#frmSubContractReturn #cboGPNo').val();
	var gatePassYear	= $('#frmSubContractReturn #cboGPYear').val();
	var remarks			= $('#frmSubContractReturn #txtRemarks').val();
	
	if($('#frmSubContractReturn').validationEngine('validate'))
	{
		showWaiting();
		var data = "requestType=saveData";
		
		var arrHeader = "{";
							arrHeader += '"serialNo":"'+serialNo+'",' ;
							arrHeader += '"serialYear":"'+serialYear+'",' ;
							arrHeader += '"date":"'+date+'",' ;
							arrHeader += '"gatePassNo":"'+gatePassNo+'",' ;
							arrHeader += '"gatePassYear":"'+gatePassYear+'",' ;
							arrHeader += '"remarks":'+URLEncode_json(remarks)+'';
		
			arrHeader += "}";
		
		var arrDetails	= "";	
		var chkStatus	= false;
		$('#tblMain .clsReturnQty').each(function(index, element){
        	
			var salesOrderId	= $(this).parent().parent().find('.clsSalesOrderNo').attr('id');
			var subJobId		= $(this).parent().parent().find('.clsSubConJobType').attr('id');
			var cutNo			= $(this).parent().parent().find('.clsCutNo').html();
			var size			= $(this).parent().parent().find('.clsSize').html();
			var returnQty		= parseFloat($(this).val());   
			
			if(returnQty>0)
			{
				chkStatus	= true;
				
				arrDetails += "{";
				arrDetails += '"salesOrderId":"'+ salesOrderId +'",' ;
				arrDetails += '"subJobId":"'+ subJobId +'",' ;
				arrDetails += '"cutNo":"'+ cutNo +'",' ;
				arrDetails += '"size":"'+ size +'",' ;
				arrDetails += '"returnQty":"'+ returnQty +'"' ;
				arrDetails += "},";
			}
        });
		if(!chkStatus)
		{
			$('#frmSubContractReturn #butSave').validationEngine('showPrompt','No Return Qty to save.','fail');
			hideWaiting();	
			return;
		}
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		var arrHeader	= arrHeader;
		var arrDetails	= '['+arrDetails+']';
		data+="&arrHeader="+arrHeader+"&arrDetails="+arrDetails;
		
		var url = "presentation/customerAndOperation/bulk/sub_contractor_job_return/addNew/sub_contractor_job_return_db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmSubContractReturn #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t = setTimeout("alertx()",1000);
						$('#frmSubContractReturn #txtReturnNo').val(json.serialNo);
						$('#frmSubContractReturn #txtReturnYear').val(json.serialYear);
						$('#frmSubContractReturn #butConfirm').show();
						hideWaiting();
						return;
					}
					else
					{
						hideWaiting();
					}
				},
				error:function(xhr,status){
						
						$('#frmSubContractReturn #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
				}		
		});
	}	
}

function validateReturnQty()
{
	var gpQty			= parseFloat($(this).parent().parent().find('.clsGPQty').html());
	var returnedQty		= parseFloat($(this).parent().parent().find('.clsReturnedQty').html());
	var returnQty		= parseFloat($(this).val());
	
	var balToReturn		= (gpQty==''?0:gpQty)-(returnedQty==''?0:returnedQty);
	if(returnQty>balToReturn)
		$(this).val(balToReturn);
}

function clearAll()
{
	document.location.href = "?q=877";
}

function alertx()
{
	$('#frmSubContractReturn #butSave').validationEngine('hide');
}

function approve()
{
	var val = $.prompt('Are you sure you want to approve this sub contract return ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
			showWaiting();
			var url = "presentation/customerAndOperation/bulk/sub_contractor_job_return/addNew/sub_contractor_job_return_db.php"+window.location.search+'&requestType=approve';
			var obj = $.ajax({
				url:url,
				type:'post',
				dataType: "json",  
				data:'',
				async:false,						
				success:function(json){
					$('#frmSubJobGPReturnReport #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertx1()",1000);
						window.location.href = window.location.href;
						window.opener.location.reload();
						return;
					}
					 hideWaiting();
				},
				error:function(xhr,status){						
						$('#frmSubJobGPReturnReport #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertx1()",3000);
						 hideWaiting();
				}		
			});	
		}			  
	}});
}

function reject()
{
	var val = $.prompt('Are you sure you want to reject this sub contract return ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = "presentation/customerAndOperation/bulk/sub_contractor_job_return/addNew/sub_contractor_job_return_db.php"+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,						
						success:function(json){
								$('#frmSubJobGPReturnReport #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx2()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSubJobGPReturnReport #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx2()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}

function cancel()
{
	var val = $.prompt('Are you sure you want to cancel this sub contract return ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = "presentation/customerAndOperation/bulk/sub_contractor_job_return/addNew/sub_contractor_job_return_db.php"+window.location.search+'&requestType=cancel';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,						
						success:function(json){
								$('#frmSubJobGPReturnReport #butRptCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx2()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSubJobGPReturnReport #butRptCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx2()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}
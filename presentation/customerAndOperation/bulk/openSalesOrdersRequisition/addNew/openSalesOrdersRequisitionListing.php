<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php

$session_locationId = $sessions->getLocationId();
$session_companyId 	= $sessions->getCompanyId();
$intUser  			= $sessions->getUserId();
$hrDb				= $sessions->getHrDatabase();

$programCode		= 'P1171';

include_once 		"class/tables/trn_sales_order_open_requisition_header.php";	$trn_sales_order_open_requisition_header = new trn_sales_order_open_requisition_header($db);
include_once 		"libraries/jqgrid2/inc/jqgrid_dist.php";

$select				= "MAX(APPROVE_LEVELS) AS MAX_LEVEL";
$header_result		= $trn_sales_order_open_requisition_header->select($select,NULL,NULL,NULL,NULL);
$header_array		= mysqli_fetch_array($header_result);
$approveLevel 		= $header_array['MAX_LEVEL'];

//BEGIN - ADD DEFAULT WHERE STRING WHEN FORM LOADING {
$arr =  json_decode($_REQUEST['filters'],true);

$arr = $arr['rules'];

$where_string = '';
$where_array = array(
					'Status'=>'APH.STATUS',
					'RAISED_BY'=>'sys_users.strUserName',
					'CREATED_DATE'=>'DATE(CREATED_DATE)'
					);
$arr_status 	= array('Approved'=>'1','Rejected'=>'0','Revised'=>'-1','Cancelled'=>'-2','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}

if(!count($arr)>0)					 
		$where_string .= "AND DATE(CREATED_DATE) = '".date('Y-m-d')."'";
//END }

$sql = "SELECT SUB_1.* FROM
			(SELECT
				CONCAT(APH.ORDER_YEAR,' / ',APH.ORDER_NO) AS ORDER_NO,
				CONCAT(APH.REQUISITION_YEAR,' / ',APH.REQUISITION_NO) AS REQUISITION_NO_STR,
				APH.REQUISITION_NO,
				APH.REQUISITION_YEAR,
				DATE(APH.CREATED_DATE) AS CREATED_DATE,
				APH.APPROVE_LEVELS,
				APH.CREATED_BY,
				sys_users.strUserName AS RAISED_BY,
				if(APH.STATUS=1,'Approved',if(APH.STATUS=0,'Rejected','Pending')) as Status,
				sys_users.strUserName AS CREATOR,
				IFNULL((
					SELECT
					concat(sys_users.strUserName,'(',max(trn_sales_order_open_requisition_approvedby.APPROVE_DATE),')' )
					FROM
					trn_sales_order_open_requisition_approvedby
					Inner Join sys_users ON trn_sales_order_open_requisition_approvedby.APPROVE_BY = sys_users.intUserId
					WHERE
					trn_sales_order_open_requisition_approvedby.SERIAL_NO  = APH.REQUISITION_NO AND
					trn_sales_order_open_requisition_approvedby.SERIAL_YEAR =  APH.REQUISITION_YEAR AND
					trn_sales_order_open_requisition_approvedby.APPROVE_LEVELS = '1' AND
					trn_sales_order_open_requisition_approvedby.STATUS =  '0'
				),IF(((SELECT
					menupermision.int1Approval 
					FROM menupermision 
					Inner Join menus ON menupermision.intMenuId = menus.intId
					WHERE
					menus.strCode = '$programCode' AND
					menupermision.intUserId =  '$intUser')=1 AND APH.STATUS>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
								$approval	= "2nd_Approval";
							}
							else if($i==3){
								$approval	= "3rd_Approval";
							}
							else {
								$approval	= $i."th_Approval";
							}
							
							
						$sql .= "IFNULL((
								SELECT
								concat(sys_users.strUserName,'(',max(trn_sales_order_open_requisition_approvedby.APPROVE_DATE),')' )
								FROM
								trn_sales_order_open_requisition_approvedby
								Inner Join sys_users ON trn_sales_order_open_requisition_approvedby.APPROVE_BY = sys_users.intUserId
								WHERE
								trn_sales_order_open_requisition_approvedby.SERIAL_NO  = APH.REQUISITION_NO AND
								trn_sales_order_open_requisition_approvedby.SERIAL_YEAR =  APH.REQUISITION_YEAR AND
								trn_sales_order_open_requisition_approvedby.APPROVE_LEVELS =  '$i' AND
								trn_sales_order_open_requisition_approvedby.STATUS =  '0' 
							),
							IF(
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (APH.STATUS>1) AND (APH.STATUS<=APH.APPROVE_LEVELS) AND ((SELECT
								concat(sys_users.strUserName )
								FROM
								trn_sales_order_open_requisition_approvedby
								Inner Join sys_users ON trn_sales_order_open_requisition_approvedby.APPROVE_BY = sys_users.intUserId
								WHERE
								trn_sales_order_open_requisition_approvedby.SERIAL_NO  = APH.REQUISITION_NO AND
								trn_sales_order_open_requisition_approvedby.SERIAL_YEAR =  APH.REQUISITION_YEAR AND
								trn_sales_order_open_requisition_approvedby.APPROVE_LEVELS =  ($i-1) AND 
								trn_sales_order_open_requisition_approvedby.STATUS='0' )<>'')),
								
								'Approve',
								 if($i>APH.APPROVE_LEVELS,'-----',''))
								
								) as `".$approval."`, "; 
									
								}
								
							$sql .= "IFNULL((
								SELECT
								concat(sys_users.strUserName,'(',max(trn_sales_order_open_requisition_approvedby.APPROVE_DATE),')' )
								FROM
								trn_sales_order_open_requisition_approvedby
								Inner Join sys_users ON trn_sales_order_open_requisition_approvedby.APPROVE_BY = sys_users.intUserId
								WHERE
								trn_sales_order_open_requisition_approvedby.SERIAL_NO  = APH.REQUISITION_NO AND
								trn_sales_order_open_requisition_approvedby.SERIAL_YEAR =  APH.REQUISITION_YEAR AND
								trn_sales_order_open_requisition_approvedby.APPROVE_LEVELS =  '0' AND
								trn_sales_order_open_requisition_approvedby.STATUS =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND APH.STATUS<=APH.APPROVE_LEVELS AND 
								APH.STATUS>1),'Reject', '')) as `Reject`,";

							$sql .= "'View' as `View`   
				
				FROM trn_sales_order_open_requisition_header as APH 
				INNER JOIN sys_users ON sys_users.intUserId = APH.CREATED_BY
				WHERE APH.LOCATION_ID='$session_locationId'
				$where_string
		)  
		AS SUB_1 WHERE 1=1";
//echo $sql;
$jq = new jqgrid('',$db);	

$cols	= array();
$col	= array();

$col["title"] 				= "Status";
$col["name"] 				= "Status";
$col["width"] 				= "2"; 						
$col["align"] 				= "center";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["stype"] 				= "select";
$str 						= ":All;Pending:Pending;Approved:Approved;Rejected:Rejected" ;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "REQUISITION_NO";
$col["name"] 				= "REQUISITION_NO";	
$col["sortable"] 			= true; 					
$col["search"] 				= true;
$col["hidden"]  			= true;				
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "REQUISITION_YEAR";
$col["name"] 				= "REQUISITION_YEAR";	
$col["sortable"] 			= true; 					
$col["search"] 				= true;
$col["hidden"]  			= true;				
$cols[] 					= $col;	
$col						= NULL;


$col["title"] 				= "REQUISITION_YEAR NO";
$col["name"] 				= "REQUISITION_NO_STR";	
$col["width"] 				= "2";
$col["align"] 				= "center";
$col["sortable"] 			= true; 					
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col['link']				= '?q=1171&serialNo={REQUISITION_NO}&serialYear={REQUISITION_YEAR}';
$col["linkoptions"] 		= "target='openSalesOrdersRequisition.php'";
$cols[] 					= $col;	
$col						= NULL;


$col["title"] 				= "Order No";
$col["name"] 				= "ORDER_NO";
$col["width"] 				= "2";
$col["align"] 				= "center";
$col["sortable"] 			= true; 					
$col["search"] 				= true; 					

$cols[] 					= $col;	
$col						= NULL;


$col["title"] 				= "Date";
$col["name"] 				= "CREATED_DATE";
$col["width"] 				= "3";
$col["align"] 				= "center";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Raised By";
$col["name"] 				= "RAISED_BY";
$col["width"] 				= "2";
$col["align"] 				= "center";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "1st Approval";
$col["name"] 				= "1st_Approval";
$col["width"] 	= "2";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']				= '?q=1172&serialNo={REQUISITION_NO}&serialYear={REQUISITION_YEAR}&mode=Confirm';
$col["linkoptions"] 		= "target='openSalesOrdersRequisition-report.php.php'";

$reportLink  = "?q=1172&serialNo={REQUISITION_NO}&serialYear={REQUISITION_YEAR}";
$reportLinkApprove  = "?q=1172&serialNo={REQUISITION_NO}&serialYear={REQUISITION_YEAR}&mode='Confirm'";

$cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++)
{
	if($i==2){
		$ap		= "2nd Approval";
		$ap1	= "2nd_Approval";
	}
	else if($i==3){
		$ap		= "3rd Approval";
		$ap1	= "3rd_Approval";
	}
	else {
		$ap		= $i."th Approval";
		$ap1	= $i."th_Approval";
	}

$col["title"] 				= $ap; 
$col["name"] 				= $ap1; 
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= '?q=1172&serialNo={REQUISITION_NO}&serialYear={REQUISITION_YEAR}&mode=Confirm';
$col["linkoptions"] 		= "target='openSalesOrdersRequisition-report.php.php'";
$col['linkName']			= 'Approve';
$cols[] 					= $col;	
$col						= NULL;
}

$col["title"] 				= "Report";
$col["name"] 				= "View";
$col["width"] 				= "2";
$col["align"] 				= "center"; 
$col["sortable"]			= false;
$col["editable"] 			= false; 	
$col["search"] 				= false; 
$col['link']				= '?q=1172&serialNo={REQUISITION_NO}&serialYear={REQUISITION_YEAR}';
$col["linkoptions"] 		= "target='openSalesOrdersRequisition-report.php.php'";
$col['linkName']			= 'View';
$cols[] 					= $col;	
$col						= NULL;

$grid["caption"] 			= "Open Sales Orders - Requisition Listing";
$grid["multiselect"] 		= false;
$grid["rowNum"] 			= 20; 
$grid["sortname"] 			= 'REQUISITION_NO,REQUISITION_YEAR'; 
$grid["sortorder"] 			= "DESC"; 
$grid["autowidth"] 			= true; 
$grid["multiselect"] 		= false; 
$grid["search"] 			= true; 
$grid["postData"] 			= array("filters" => $sarr ); 
$grid["export"] 			= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Open Sales Orders Requisition Listing</title>
<?php
echo $out;

?>
 
	loadSavedDetails($('#frmOpenOrdersRequisition #txtSerialNo').val(),$('#frmOpenOrdersRequisition #txtYear').val());
 
$(document).ready(function(){	

	
	$("#frmOpenOrdersRequisition #cboOrderYear").die('change').live('change',function(){
		loadOrderNos();
	});
	$("#frmOpenOrdersRequisition #cboOrderNo").die('change').live('change',function(){
		loadSalesOrderDetails();
	});
	$("#frmOpenOrdersRequisition #butSave").die('click').live('click',function(){
		saveMain();
	});
	$("#frmOpenOrdersRequisition #butNew").die('click').live('click',function(){
		loadNewPage();
	});
	$("#frmOpenOrdersRequisition #butConfirm").die('click').live('click',function(){
		viewReport('Confirm');
	});
	$("#frmOpenOrdersRequisition #butReport").die('click').live('click',function(){
		viewReport('');
	});
	$("#frmOpenOrdersRequisitionRpt #butRptConfirm").die('click').live('click',function(){
		approve();
	});
	$("#frmOpenOrdersRequisitionRpt #butRptReject").die('click').live('click',function(){
		reject();
	});
	
});

function loadOrderNos(){
	
	var orderYear 	= $('#frmOpenOrdersRequisition #cboOrderYear').val();
	
	if(orderYear==''){
		$('#frmOpenOrdersRequisition #cboOrderYear').val('');
		$('#frmOpenOrdersRequisition #cboOrderNo').html('');
		return;
	}
	var url   		= "controller.php?q=1171&requestType=loadOrderNos";
	var data		= "orderYear="+orderYear;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmOpenOrdersRequisition #cboOrderNo').html(json.orderNoCombo);
			}
	});	
}

function loadOrders(){
	
	var orderYear 	= $('#frmOpenOrdersRequisition #cboOrderYear').val();
	var orderNo	 	= $('#frmOpenOrdersRequisition #cboOrderNo').val();
	
	if(orderYear==''){
		$('#frmOpenOrdersRequisition #cboOrderYear').val('');
		$('#frmOpenOrdersRequisition #cboOrderNo').val('');
		return;
	}
	if(orderNo==''){
		$('#frmOpenOrdersRequisition #cboOrderYear').val('');
		$('#frmOpenOrdersRequisition #cboOrderNo').val('');
		return;
	}
	var url   		= "controller.php?q=1171&requestType=loadOrders";
	var data		= "orderYear="+orderYear;
		data		+= "&orderNo="+orderNo;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmOpenOrdersRequisition #cboOrderYear').html(json.orderYearCombo);
				$('#frmOpenOrdersRequisition #cboOrderNo').html(json.orderNoCombo);
			}
	});	
}

function loadSalesOrderDetails(){
	
	var orderYear 	= $('#frmOpenOrdersRequisition #cboOrderYear').val();
	var orderNo	 	= $('#frmOpenOrdersRequisition #cboOrderNo').val();
	
	if(orderYear==''){
		$('#frmOpenOrdersRequisition #cboOrderYear').val('');
		$('#frmOpenOrdersRequisition #cboOrderNo').val('');
		return;
	}
	if(orderNo==''){
		$('#frmOpenOrdersRequisition #cboOrderYear').val('');
		$('#frmOpenOrdersRequisition #cboOrderNo').val('');
		return;
	}
	
	var url   		= "controller.php?q=1171&requestType=loadSalesOrderDetails";
	var data		= "orderYear="+orderYear;
		data		+= "&orderNo="+orderNo;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmOpenOrdersRequisition #tblSalesOrders').html(json.tableHtml);
			}
	});	
}

function loadSavedDetails(serialNo,serialYear){
	
	$('#frmOpenOrdersRequisition #txtSerialNo').val(serialNo);
	$('#frmOpenOrdersRequisition #txtYear').val(serialYear);
	
	loadHeader(serialNo,serialYear);
	loadDetails(serialNo,serialYear);
	
}

function loadHeader(serialNo,serialYear){

	var url   		= "controller.php?q=1171&requestType=loadHeader";
	var data		= "serialNo="+serialNo;
		data		+= "&serialYear="+serialYear;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmOpenOrdersRequisition #cboOrderNo').html(json.orderNoCombo);
				$('#frmOpenOrdersRequisition #cboOrderYear').html(json.orderYearCombo);
				$('#frmOpenOrdersRequisition #dtDate').val(json.date);
			}
	});	
	
}


function loadDetails(serialNo,serialYear){
	
	var url   		= "controller.php?q=1171&requestType=loadSalesOrderDetails";
	var data		= "serialNo="+serialNo;
		data		+= "&serialYear="+serialYear;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmOpenOrdersRequisition #tblSalesOrders').html(json.tableHtml);
			}
	});	
}

function saveMain(){
	
	var serialNo	=$('#frmOpenOrdersRequisition #txtSerialNo').val();
	var serialYear	= $('#frmOpenOrdersRequisition #txtYear').val();

	if(serialNo=='' && serialYear=='')
		save();
	else
		update();
	
}

function save(){
	
	showWaiting();
	
	var chkStatus		= false;
	var arrDetails		= "";
	var numberValidate 	= 0;
	
	var orderNo		= $('#frmOpenOrdersRequisition #cboOrderNo').val();
	var orderYear	= $('#frmOpenOrdersRequisition #cboOrderYear').val();

	var data = "requestType=save";
	
	var arrHeader 	= "{";
							arrHeader += '"orderNo":"'+orderNo+'",' ;
							arrHeader += '"orderYear":'+orderYear+'';
							
		arrHeader  += "}";
		
	$('#frmOpenOrdersRequisition #tblSalesOrders .requisition:checked').each(function(){
		var salesOrder		= $(this).parent().parent().find('.salesOrder').attr('id');
			arrDetails += "{";
			arrDetails += '"orderNo":"'+ orderNo +'",' ;
			arrDetails += '"orderYear":"'+ orderYear +'",' ;
			arrDetails += '"salesOrder":'+salesOrder+'';
			arrDetails += "},";

	});
	
	arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
	var arrDetails	= '['+arrDetails+']';
	
	data		   +="&arrHeader="+arrHeader;
	data		   +="&arrDetails="+arrDetails;

	var url   = "controller.php?q=1171";	
	$.ajax({
			url:url,
			dataType:'json',
			type:'post',
			data:data,
			async:false,
			success:function(json){
				$('#frmOpenOrdersRequisition #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				
				if(json.type=='pass')
				{
					hideWaiting();
					$('#frmOpenOrdersRequisition #txtSerialNo').val(json.serialNo);
					$('#frmOpenOrdersRequisition #txtYear').val(json.serialYear);
					if(json.savePerm==1){
						$('#frmOpenOrdersRequisition #butSave').show();
					}
					else{
						$('#frmOpenOrdersRequisition #butSave').hide();
					}
					if(json.approvePerm==1){
						$('#frmOpenOrdersRequisition #butConfirm').show();
					}
					else{
						$('#frmOpenOrdersRequisition #butConfirm').hide();
					}
					return;
				}
				else
				{
					hideWaiting();
				}
			},
			error:function(xhr,status)
			{
				$('#frmOpenOrdersRequisition #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				hideWaiting();
				return;;
			}
	});
	hideWaiting();	
}

function update(){

	showWaiting();
	
	var chkStatus		= false;
	var arrDetails		= "";
	var numberValidate 	= 0;
	
	var serialNo	= $('#frmOpenOrdersRequisition #txtSerialNo').val();
	var serialYear	= $('#frmOpenOrdersRequisition #txtYear').val();
	var orderNo		= $('#frmOpenOrdersRequisition #cboOrderNo').val();
	var orderYear	= $('#frmOpenOrdersRequisition #cboOrderYear').val();

	var data = "requestType=update";
	
	var arrHeader 	= "{";
							arrHeader += '"serialNo":"'+serialNo+'",' ;
							arrHeader += '"serialYear":"'+serialYear+'",' ;
							arrHeader += '"orderNo":"'+orderNo+'",' ;
							arrHeader += '"orderYear":'+orderYear+'';
							
		arrHeader  += "}";
		
	$('#frmOpenOrdersRequisition #tblSalesOrders .requisition:checked').each(function(){
		var salesOrder	= $(this).parent().parent().find('.salesOrder').attr('id');
			arrDetails += "{";
			arrDetails += '"orderNo":"'+ orderNo +'",' ;
			arrDetails += '"orderYear":"'+ orderYear +'",' ;
			arrDetails += '"salesOrder":'+salesOrder+'';
			arrDetails += "},";

	});
	
	arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
	var arrDetails	= '['+arrDetails+']';
	
	data		   +="&arrHeader="+arrHeader;
	data		   +="&arrDetails="+arrDetails;

	var url   = "controller.php?q=1171";	
	$.ajax({
			url:url,
			dataType:'json',
			type:'post',
			data:data,
			async:false,
			success:function(json){
				$('#frmOpenOrdersRequisition #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				
				if(json.type=='pass')
				{
					hideWaiting();
					
					$('#frmOpenOrdersRequisition #txtSerialNo').val(json.serialNo);
					$('#frmOpenOrdersRequisition #txtYear').val(json.serialYear);
					if(json.savePerm==1){
						$('#frmOpenOrdersRequisition #butSave').show();
					}
					else{
						$('#frmOpenOrdersRequisition #butSave').hide();
					}
					if(json.approvePerm==1){
						$('#frmOpenOrdersRequisition #butConfirm').show();
					}
					else{
						$('#frmOpenOrdersRequisition #butConfirm').hide();
					}
					return;
				}
				else
				{
					hideWaiting();
				}
			},
			error:function(xhr,status)
			{
				$('#frmOpenOrdersRequisition #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				hideWaiting();
				return;;
			}
	});
	hideWaiting();	
	
}

function viewReport(type){
	
	if($('#frmOpenOrdersRequisition #txtSerialNo').val()=="") 
		return;
	var url = "?q=1172&serialNo="+$('#frmOpenOrdersRequisition #txtSerialNo').val()+"&serialYear="+$('#frmOpenOrdersRequisition #txtYear').val()+"&mode="+type;
	window.open(url,'openSalesOrdersRequisition-report.php');
	
	
}

function approve()
{
	var val = $.prompt('Are you sure you want to approve this Sales Order Opening Requisition ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
			showWaiting();
			var url = "controller.php?q=1171&"+window.location.search+'&requestType=approve';
			var obj = $.ajax({
				url:url,
				type:'post',
				dataType: "json",  
				data:'',
				async:false,						
				success:function(json){
					$('#frmOpenOrdersRequisitionRpt #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertR1()",1000);
						window.location.href = window.location.href;
						window.opener.location.reload();
						return;
					}
					 hideWaiting();
				},
				error:function(xhr,status){						
						$('#frmOpenOrdersRequisitionRpt #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertR1()",3000);
						 hideWaiting();
				}		
			});	
		}			  
	}});
}

function reject()
{
	var val = $.prompt('Are you sure you want to reject this Sales Order Opening Requisition ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = "controller.php?q=1171&"+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,						
						success:function(json){
								$('#frmOpenOrdersRequisitionRpt #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertR2()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmOpenOrdersRequisitionRpt #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertR2()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}

function loadNewPage(){
	
	window.location.href = "?q=1171";
	
}

function alertR1()
{
	$('#frmOpenOrdersRequisitionRpt #butRptConfirm').validationEngine('hide');
}
function alertR2()
{
	$('#frmOpenOrdersRequisitionRpt #butRptReject').validationEngine('hide');
}
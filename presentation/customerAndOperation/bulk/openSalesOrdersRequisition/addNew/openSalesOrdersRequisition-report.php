<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>

<?php
ini_set('display_errors',1);
$userId  		= $sessions->getUserId();
$companyId  	= $sessions->getCompanyId();
$locationId		= $sessions->getlocationId();

$serialNo 		= $_REQUEST['serialNo'];
$serialYear 	= $_REQUEST['serialYear'];
$mode 			= $_REQUEST['mode'];
$programCode	= 'P1171';


include_once "class/tables/menupermision.php"; 						$menupermision						= new menupermision($db);
include_once "class/tables/mst_locations.php";						$mst_locations 						= new mst_locations($db);
include_once "class/dateTime.php";									$dateTimes 							= new dateTimes($db);
include_once "class/tables/sys_users.php";							$sys_users							= new sys_users($db);
include_once "class/tables/trn_sales_order_open_requisition_approvedby.php";	$trn_sales_order_open_requisition_approvedby	= new trn_sales_order_open_requisition_approvedby($db);
include_once "class/tables/trn_sales_order_open_requisition_details.php";	$trn_sales_order_open_requisition_details	= new trn_sales_order_open_requisition_details($db);
include_once "class/tables/trn_sales_order_open_requisition_header.php";	$trn_sales_order_open_requisition_header	= new trn_sales_order_open_requisition_header($db);
include_once "class/tables/mst_part.php";									$mst_part	= new mst_part($db);
include_once "class/tables/mst_technique_groups.php";						$mst_technique_groups	= new mst_technique_groups($db);


$db->connect();$db->begin();

$trn_sales_order_open_requisition_header->set($serialYear,$serialNo);
$menupermision->set($programCode,$userId);

$status					= $trn_sales_order_open_requisition_header->getSTATUS();
$levels					= $trn_sales_order_open_requisition_header->getAPPROVE_LEVELS();
$cretedBy				= $trn_sales_order_open_requisition_header->getCREATED_BY();
$date					= substr($trn_sales_order_open_requisition_header->getCREATED_DATE(),0,10);
$orderNo				= $trn_sales_order_open_requisition_header->getORDER_NO();
$orderYear				= $trn_sales_order_open_requisition_header->getORDER_YEAR();

$header_array["STATUS"]	= $status;
$header_array['LEVELS'] = $levels;

$db->connect();$db->begin();
$detail_result			= $trn_sales_order_open_requisition_details->getSavedDetails($serialNo,$serialYear);

$perRejArr				= $menupermision->checkMenuPermision('Reject',$status,$levels);
$perApproArr			= $menupermision->checkMenuPermision('Approve',$status,$levels);

$permision_reject		= ($perRejArr['type']?1:0);
$permision_confirm		= ($perApproArr['type']?1:0);


?>
<title>Open Sales Order - Requisition Report</title>
<script type="text/javascript" src="presentation/customerAndOperation/bulk/openSalesOrdersRequisition/addNew/openSalesOrdersRequisition-js.js"></script>
<style>
.break {
	page-break-before: always;
}
 @media print {
.noPrint {
	display: none;
}
}
#apDiv1 {
	position: absolute;
	left: 309px;
	top: 248px;
	width: 650px;
	height: 259px;
	z-index: 1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
<?php
 if($status>1)//pending
{
?>
<div id="apDiv1"><img src="images/pending.png"  /></div>
<?php
}
?>
<form id="frmOpenOrdersRequisitionRpt" name="frmOpenOrdersRequisitionRpt">
<table width="900" border="0" align="center">
<tr>
<td><?php 	$db->connect();$db->begin();
				include 'reportHeader.php';
			$db->disconnect();	
			
			$locationId 	= $sessions->getLocationId();		
			?></td>
</tr>
<tr>
<td class="reportHeader" align="center">OPEN SALES ORDERS - REQUISITION REPORT</td>
</tr>
<tr>
<td class="reportHeader" align="center">&nbsp;</td>
</tr>
<?php
include "presentation/report_approve_status_and_buttons.php"
?>
<tr>
	<td>
        <table width="100%" border="0">
        <tr class="normalfnt">
        	<td width="12%">Serial No</td>
        	<td width="1%" style="text-align:center">:</td>
        	<td width="42%"><?php echo $serialYear.' / '.$serialNo; ?></td>
        	<td width="12%"></td>
        	<td width="1%" style="text-align:center"></td>
        	<td width="32%"></td>
        </tr>
        <tr class="normalfnt">
        	<td width="12%">Order No</td>
        	<td width="1%" style="text-align:center">:</td>
        	<td width="42%"><?php echo $orderYear.' / '.$orderNo; ?></td>
        	<td width="12%">Date</td>
        	<td width="1%" style="text-align:center">:</td>
        	<td width="32%"><?php echo $date; ?></td>
        </tr>
        </table>
    </td>
</tr>
<tr>
	<td>
    	<table width="100%" class="bordered" id="tblMainGrid" border="0">
        <thead>
        <tr class="">
          <th nowrap="nowrap" >Status</th>
          <th nowrap="nowrap" >Sales Order No </th>
          <th nowrap="nowrap" >Graphic No</th>
          <th nowrap="nowrap" >Sample No</th>
          <th nowrap="nowrap" >Style</th>
          <th nowrap="nowrap" >Combo</th>
          <th nowrap="nowrap" >Part</th>
          <th nowrap="nowrap" >Print</th>
          <th nowrap="nowrap" >Revision</th>
          <th nowrap="nowrap">Order Qty</th>
          <th nowrap="nowrap" >Price</th>
          <th nowrap="nowrap" >Value</th>
          <th nowrap="nowrap" >Technique Group</th> 
          </tr>
        </thead>
        <tbody>
        <?php
		while($row=mysqli_fetch_array($detail_result))
		{
			$db->connect();$db->begin();
			$mst_part->set($row['intPart']);
			$part		= $mst_part->getstrName();
			$mst_technique_groups->set($row['TECHNIQUE_GROUP_ID']);
			$techGrp	= $mst_technique_groups->getTECHNIQUE_GROUP_NAME();
			if($row['SAVED_STATUS']==1)
				$chk_saved = ' checked="checked"';
			else
				$chk_saved = '';
		?>	
            <tr class="normalfnt" id="dataRow" >
                <td height="49" align="center" nowrap="nowrap" bgcolor="#FFFFFF"><?php echo $row['STATUS']; ?></td>
                <td bgcolor="#FFFFFF" align="center" nowrap="nowrap"><?php echo $row['strSalesOrderNo']; ?></td>
                <td bgcolor="#FFFFFF" align="center" nowrap="nowrap"><?php echo $row['strGraphicNo']; ?></td>
                <td bgcolor="#FFFFFF" align="center" nowrap="nowrap"><?php echo $row['intSampleYear']."/".$row['intSampleNo']; ?></td>
                <td bgcolor="#FFFFFF" align="center" nowrap="nowrap"><?php echo $row['strStyleNo']; ?></td>
                <td bgcolor="#FFFFFF" align="center" nowrap="nowrap"><?php echo $row['strCombo']; ?></td>
                <td bgcolor="#FFFFFF" align="center" nowrap="nowrap"><?php echo $part; ?></td>
                <td bgcolor="#FFFFFF" align="center" nowrap="nowrap"><?php echo $row['strPrintName']; ?></td>
                <td bgcolor="#FFFFFF" align="center" nowrap="nowrap"><?php echo $row['intRevisionNo']; ?></td>
                <td bgcolor="#FFFFFF" align="right" nowrap="nowrap"><?php echo $row['intQty']; ?></td>
                <td bgcolor="#FFFFFF" align="right" nowrap="nowrap"><?php echo $row['dblPrice']; ?></td>
                <td bgcolor="#FFFFFF" align="right" nowrap="nowrap"><?php echo $row['intQty']*$row['dblPrice']; ?></td>
                <td bgcolor="#FFFFFF" align="center" nowrap="nowrap"><?php echo $techGrp; ?></td>
              </tr>		<?php
        }
		?>
        </tbody>
        </table>
    </td>
</tr>
<tr>
            <td><?php
			$db->connect();$db->begin();
			
 			$sys_users->set($cretedBy);
			$creator		= $sys_users->getstrUserName();
			$createdDate	= $trn_sales_order_open_requisition_header->getCREATED_DATE();
												
			$resultA		= $trn_sales_order_open_requisition_approvedby->getApproveByData($serialNo,$serialYear);
			include "presentation/report_approvedBy_details.php";
 	?></td>
    </tr>
<tr>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>&nbsp;</td>
</tr>
<?php
    if($mode=='Confirm')
    {
    ?>
    <tr height="40">
    <?php
    $url  = "presentation/send_to_approval_latest.php";
    $url .= "?status=$status";										// * set recent status
    $url .= "&approveLevels=$levels";								// * set approve levels in order header
    $url .= "&programCode=$programCode";								// * program code (ex:P10001)
    $url .= "&program=Actual Production Report";									// * program name (ex:Purchase Order)
    $url .= "&companyId=".$companyId;									// * created company id
    $url .= "&createUserId=".$trn_sales_order_open_requisition_header->getCREATED_BY();;
    
    $url .= "&subject=Actual Production for Approval (".$serialYear."/".$serialNo.")";	
    
    $url .= "&statement1=Please Approve this";	
    $url .= "&statement2=to Approve this";	
    $url .= "&link=".base64_encode(MAIN_URL."?q=1039&serialNo=".$serialNo."&serialYear=".$serialYear."&mode=Confirm");
    ?>
    <td align="center" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:150px;border:none"></iframe></td>
    </tr>
    <?php
    }
    ?>
    </tr>
    <tr height="40">
    	<td align="center" class="normalfntMid"><strong>Printed Date: <?php echo $dateTimes->getCurruntDateTime(); ?></strong></td>
    </tr>
</table>
</form>
<?php
$db->disconnect();
?>
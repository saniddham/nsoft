<?php 
//////////////////////////////////////////////
//Create By:H.B.G Korala
//note://if final confirmation raised,insert into stocktransaction table.
/////////////////////////////////////////////

	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];

	include "{$backwardseperator}dataAccess/Connector.php";
	
	$programName='Fabric Gate Pass Transfer In';
	$programCode='P0048';
	$fabRcvApproveLevel = (int)getApproveLevel($programName);
	
	/////////// parameters /////////////////////////////

	
	$serialNo = $_REQUEST['serialNo'];
	$year = $_REQUEST['year'];
	
	if($_REQUEST['status']=='approve')
	{
		$sql = "UPDATE `ware_fabricgptransferinheader` SET `intStatus`=intStatus-1 WHERE (intFabricGPTransfInNo='$serialNo') AND (`intFabricGPTransfInYear`='$year') and intStatus not in(0,1)";
		$result = $db->RunQuery($sql);
		
		if($result){
			$sql = "SELECT ware_fabricgptransferinheader.intStatus, 
			ware_fabricgptransferinheader.intApproveLevels , 
			ware_fabricgptransferinheader.intCompanyId as location,
			mst_locations.intCompanyId as company
			FROM
			ware_fabricgptransferinheader
			Inner Join mst_locations ON ware_fabricgptransferinheader.intCompanyId = mst_locations.intId
			WHERE (intFabricGPTransfInNo='$serialNo') AND (`intFabricGPTransfInYear`='$year')";
			$results = $db->RunQuery($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			$location = $row['location'];
			$company = $row['company'];
			
			$approveLevel=$savedLevels+1-$status;
			$sqlI = "INSERT INTO `ware_fabricgptransferinheader_approvedby` (`intFabricGPTransfInNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate) 
				VALUES ('$serialNo','$year','$approveLevel','$userId',now())";
			$resultI = $db->RunQuery($sqlI);

		//--------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
			$sql1 = "SELECT
				ware_fabricgptransferinheader.intOrderNo,
				ware_fabricgptransferinheader.intOrderYear,
				ware_fabricgptransferindetails.strCutNo,
				ware_fabricgptransferindetails.intSalesOrderId,
				ware_fabricgptransferindetails.intPart, 
				ware_fabricgptransferindetails.strSize,
				ware_fabricgptransferindetails.dblQty
				FROM
				ware_fabricgptransferindetails
				Inner Join ware_fabricgptransferinheader ON ware_fabricgptransferindetails.intFabricGPTransfInNo = ware_fabricgptransferinheader.intFabricGPTransfInNo AND ware_fabricgptransferindetails.intFabricGPTransfInYear = ware_fabricgptransferinheader.intFabricGPTransfInYear
				WHERE
				ware_fabricgptransferindetails.intFabricGPTransfInNo =  '$serialNo' AND
				ware_fabricgptransferindetails.intFabricGPTransfInYear =  '$year'";
				$result1 = $db->RunQuery($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$orderNo=$row['intOrderNo'];
					$orderYear=$row['intOrderYear'];
					$cutNo=$row['strCutNo'];
					$part=$row['intPart'];
					$salesOrderId=$row['intSalesOrderId'];
					$size=$row['strSize'];
					$Qty=round($row['dblQty'],4);
					$place ='Stores';
					
					if($Qty>0){
					$type = 'GatePassTIN';
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','0','$serialNo','$year','$orderNo','$orderYear','$cutNo','$salesOrderId','$part','$size','','$Qty','$type','$userId',now())";
					$resultI = $db->RunQuery($sql);
					}
				}
			}
		//--------------------------------------------------------------------
			if($status==$savedLevels){//if first confirmation raised,requisition table shld update
			}
		//---------------------------------------------------------------------------
		}
		
	}
	else if($_REQUEST['status']=='reject')
	{
		//$grnApproveLevel = (int)getApproveLevel($programName);
		$sql = "SELECT ware_fabricgptransferinheader.intStatus,ware_fabricgptransferinheader.intApproveLevels FROM ware_gatepassheader WHERE  (intFabricGPTransfInNo='$serialNo') AND (`intFabricGPTransfInYear`='$year')";
		$results = $db->RunQuery($sql);
		$row = mysqli_fetch_array($results);
		$status = $row['intStatus'];
		$savedLevels = $row['intApproveLevels'];
		
		$sql = "UPDATE `ware_fabricgptransferinheader` SET `intStatus`=0 WHERE (intFabricGPTransfInNo='$serialNo') AND (`intFabricGPTransfInYear`='$year')";
		$result = $db->RunQuery($sql);
		
		$sql = "DELETE FROM `ware_fabricgptransferinheader_approvedby` WHERE (`intFabricGPTransfInNo`='$serialNo') AND (`intYear`='$year')";
		$result2 = $db->RunQuery($sql);
		
		$sqlI = "INSERT INTO `ware_fabricgptransferinheader_approvedby` (`intFabricGPTransfInNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate) 
			VALUES ('$serialNo','$year','0','$userId',now())";
		$resultI = $db->RunQuery($sqlI);
		
		//---------------------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
			$sql1 = "SELECT
				ware_fabricgptransferinheader.intOrderNo,
				ware_fabricgptransferinheader.intOrderYear,
				ware_fabricgptransferindetails.intSalesOrderId,
				ware_fabricgptransferindetails.strSize,
				ware_fabricgptransferindetails.dblQty 
				FROM
				ware_fabricgptransferindetails
				Inner Join ware_fabricgptransferinheader ON ware_fabricgptransferindetails.intFabricGPTransfInNo = ware_fabricgptransferinheader.intFabricGPTransfInNo AND ware_fabricgptransferindetails.intFabricGPTransfInYear = ware_fabricgptransferinheader.intFabricGPTransfInYear
				WHERE
				ware_fabricgptransferindetails.intFabricGPTransfInNo =  '$serialNo' AND
				ware_fabricgptransferindetails.intFabricGPTransfInYear =  '$year'";
				$result1 = $db->RunQuery($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$orderNo=$row['intOrderNo'];
					$orderYear=$row['intOrderYear'];
					$salesOrderId=$row['intSalesOrderId'];
					$size=$row['strSize'];
					$qty=-round($row['dblQty'],4);
					$place ='Stores';
					$type = 'CGatePassTIN';
					
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`intSalesOrderId`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','$location','$serialNo','$year','$orderNo','$orderYear','$salesOrderId','$size','','$qty','$type','$userId',now())";
					$resultI = $db->RunQuery($sqlI);
				}
			}
		//---------------------------------------------------------------------------------
			if($savedLevels>=$status){//if atleast one confirmation processed, roll back requisition table-
			}
		//-----------------------------------------------------------------------------
	}
//--------------------------------------------------------------
?>
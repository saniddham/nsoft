var basepath	= 'presentation/customerAndOperation/bulk/fabricGatePassTransferIn/listing/';

$(document).ready(function() {
	$('#frmFabGPTransferinReport').validationEngine();
	
	$('#frmFabGPTransferinReport #imgApprove').click(function(){
			if(validateQuantities()==0){
	var val = $.prompt('Are you sure you want to approve this Transferin note ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
						
						var url = basepath+"rptFabricGatePassTransferIn-db-set.php"+window.location.search+'&status=approve';
						var obj = $.ajax({url:url,async:false});
						window.location.href = window.location.href;
						window.opener.location.reload();//reload listing page
						//$('txtApproveStatus').val('1');
						//document.getElementById('txtApproveStatus').value = 1;
						//document.getElementById('frmSampleReport').submit();
					}
				}});
			}
	});
	
	$('#frmFabGPTransferinReport #imgReject').click(function(){
	var val = $.prompt('Are you sure you want to reject this Transferin note ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
									if(validateRejecton()==0){ 
										var url = basepath+"rptFabricGatePassTransferIn-db-set.php"+window.location.search+'&status=reject';
										var obj = $.ajax({url:url,async:false});
										window.location.href = window.location.href;
										window.opener.location.reload();//reload listing page
									}
									}
								}});
	});
});

function validateQuantities(){
		var ret=0;	
	    var serialNo = document.getElementById('divSerialNo').innerHTML;
		var url 		= basepath+"rptFabricGatePassTransferIn-db-get.php?requestType=getValidation";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"serialNo="+serialNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",1000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//---------------------------------------------------
function validateRejecton(){
		var ret=0;	
	    var serialNo = document.getElementById('divSerialNo').innerHTML;
		var url 		= basepath+"rptFabricGatePassTransferIn-db-get.php?requestType=validateRejecton";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"serialNo="+serialNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",1000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//---------------------------------------------------
<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
	session_start();
	$backwardseperator = "../../../../../";
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";

	$programName='Fabric Gate Pass Transfer In';
	$programCode='P0048';
	$fabRcvApproveLevel = (int)getApproveLevel($programName);
	

 if($requestType=='getValidation')
	{
		$serialNo  = $_REQUEST['serialNo'];
		$serialNoArray=explode("/",$serialNo);
		
	   	 $sql = "SELECT  
				ware_fabricgptransferinheader.intGPNo,
				ware_fabricgptransferinheader.intStatus,
				ware_fabricgptransferinheader.intApproveLevels,
				ware_fabricgptransferinheader.intGPYear,
				trn_orderdetails.strStyleNo,
				trn_orderdetails.strGraphicNo, 
				ware_fabricgptransferindetails.intSalesOrderId,
				ware_fabricgptransferindetails.strCutNo, 
				ware_fabricgptransferinheader.intOrderNo, 
				ware_fabricgptransferinheader.intOrderYear, 
				trn_orderdetails.strSalesOrderNo,
				trn_orderdetails.intSalesOrderId, 
				ware_fabricgptransferindetails.strCutNo,
				ware_fabricgptransferindetails.intPart, 
				mst_part.strName as part,
				ware_fabricgptransferindetails.intGroundColor, 
				mst_colors_ground.strName as bgcolor,
				ware_fabricgptransferindetails.strLineNo,
				ware_fabricgptransferindetails.strSize,
				ware_fabricgptransferindetails.dblQty
				FROM
				trn_orderdetails
				Inner Join ware_fabricgptransferinheader ON ware_fabricgptransferinheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricgptransferinheader.intOrderYear = trn_orderdetails.intOrderYear
				Inner Join ware_fabricgptransferindetails ON ware_fabricgptransferinheader.intFabricGPTransfInNo = ware_fabricgptransferindetails.intFabricGPTransfInNo AND ware_fabricgptransferinheader.intFabricGPTransfInYear = ware_fabricgptransferindetails.intFabricGPTransfInYear AND ware_fabricgptransferindetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
				Inner Join mst_part ON ware_fabricgptransferindetails.intPart = mst_part.intId
				Inner Join mst_colors_ground ON ware_fabricgptransferindetails.intGroundColor = mst_colors_ground.intId
				WHERE
				ware_fabricgptransferinheader.intFabricGPTransfInNo =  '$serialNoArray[0]' AND
				ware_fabricgptransferinheader.intFabricGPTransfInYear =  '$serialNoArray[1]' ";
				
		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$msg1 =""; 
		$msg ="Maximum Qtys for items"; 
		while($row=mysqli_fetch_array($result))
		{
				$status=$row['intStatus'];
				$approveLevels=$row['intApproveLevels'];
				$gpNo=$row['intGPNo'];
				$gpYear=$row['intGPYear'];
				$styleNo=$row['strStyleNo'];
				$graphicNo=$row['strGraphicNo'];
				$customerPO=$row['strCustomerPoNo'];
				$orderNo=$row['intOrderNo'];
				$orderYear=$row['intOrderYear'];
				$salesOrderNo=$row['strSalesOrderNo'];
				$salesOrderId=$row['intSalesOrderId'];
				$cutNo=$row['strCutNo'];
				$partId=$row['intPart'];
				$part=$row['part'];
				$line=$row['strLineNo'];
				$bgColorId=$row['intGroundColor'];
				$size=$row['strSize'];
				$qty=$row['dblQty'];
			//	$tot=val($row['dblSampleQty'])+val($row['dblGoodQty'])+val($row['dblEmbroideryQty'])+val($row['dblPDammageQty'])+val($row['dblFDammageQty']);
			
				$gatePassed=loadGatePassedQty($location,$gpNo,$gpYear,$orderNo,$orderYear,$cutNo,$salesOrderId,$size,$partId,$line,$bgColorId);
				$transferedQty=loadTransferedQty($location,$gpNo,$gpYear,$orderNo,$orderYear,$cutNo,$salesOrderId,$size,$partId,$line,$bgColorId);
				//echo $gatePassed.'-'.$transferedQty;
				$maxQty=$gatePassed-$transferedQty;
				if(($status>1) && ($status<=$approveLevels)){
				$maxQty=$gatePassed-$transferedQty+$qty;
				}

				//------check maximum transferin Qty--------------------
				$confirmatonMode=loadConfirmatonMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);
				
				if($status==0){
					$errorFlg=1;
					$msg ="This Gate Pass Transferin No is Rejected.";
				}
				else if($status==1){
					$errorFlg=1;
					$msg ="Final confirmation of this Gate Pass Transferin No is already raised";
				}
				else if($confirmatonMode==0){// 
					$errorFlg = 1;
					$msg ="No Permission to Approve"; 
				}
				else if($qty>$maxQty){
					$errorFlg=1;
					$msg .="<br> ".$cutNo."-".$part."-".$salesOrderNo."-".$size." =".$maxQty;
				}
		}//end of while
	
	if($msg1!=''){
		$msg=$msg1;
	}
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}
else if($requestType=='validateRejecton')
	{
		$errorFlg=0;
		$serialNo  = $_REQUEST['serialNo'];
		$serialNoArray=explode("/",$serialNo);
		
		  $sql = "SELECT
		ware_fabricgptransferinheader.intStatus, 
		ware_fabricgptransferinheader.intApproveLevels 
		FROM ware_fabricgptransferinheader
		WHERE
		ware_fabricgptransferinheader.intFabricGPTransfInNo =  '$serialNoArray[0]' AND
		ware_fabricgptransferinheader.intFabricGPTransfInYear =  '$serialNoArray[1]'";

		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$msg =""; 
		$row=mysqli_fetch_array($result);
		
		$rejectionMode=loadRejectionMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);
		
		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this GP Transferin No is already raised.So cant reject"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This GP No is already Rejected"; 
		}
		else if($rejectionMode==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="No Permission to reject this GP Transferin No"; 
		}
		else{//confirmation has been raised
			$errorFlg = 0;
			
		}
//$errorFlg = 1;
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);

	}

	

	//--------------------------------------------------------------
function loadGatePassedQty($toLocation,$gpNo,$gpYear,$x_orderNo,$x_orderYear,$cutNo,$salesOrderId,$size,$partId,$line,$bgColId)
{
		global $db;
		   $sql = "SELECT
				ware_fabricgatepassdetails.dblQty 
				FROM 
				ware_fabricgatepassheader
				Inner Join ware_fabricgatepassdetails ON ware_fabricgatepassheader.intFabricGatePassNo = ware_fabricgatepassdetails.intFabricGatePassNo AND ware_fabricgatepassheader.intFabricGatePassYear = ware_fabricgatepassdetails.intFabricGatePassYear
				WHERE
				ware_fabricgatepassheader.intFabricGatePassNo =  '$gpNo' AND
				ware_fabricgatepassheader.intFabricGatePassYear =  '$gpYear' AND 
				ware_fabricgatepassheader.intOrderNo =  '$x_orderNo' AND
				ware_fabricgatepassheader.intOrderYear =  '$x_orderYear' AND 
				ware_fabricgatepassdetails.strCutNo =  '$cutNo' AND
				ware_fabricgatepassdetails.intSalesOrderId =  '$salesOrderId' AND
				ware_fabricgatepassdetails.intPart =  '$partId' AND
				ware_fabricgatepassdetails.strSize =  '$size' AND
				ware_fabricgatepassdetails.intGroundColor =  '$bgColId' AND
				ware_fabricgatepassdetails.strLineNo =  '$line' AND 
				ware_fabricgatepassheader.intGatepassTo =  '$toLocation' AND 
				ware_fabricgatepassheader.intStatus='1'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
		
}
	//--------------------------------------------------------------
function loadTransferedQty($trnfLocation,$gpNo,$gpYear,$orderNo,$orderYear,$cutNo,$salesOrderId,$size,$partId,$line,$bgColId)
{
		global $db;
	    	$sql = "SELECT
				Sum(ware_fabricgptransferindetails.dblQty) AS dblQty
				FROM
				ware_fabricgptransferinheader
				Inner Join ware_fabricgptransferindetails ON ware_fabricgptransferinheader.intFabricGPTransfInNo = ware_fabricgptransferindetails.intFabricGPTransfInNo AND ware_fabricgptransferinheader.intFabricGPTransfInYear = ware_fabricgptransferindetails.intFabricGPTransfInYear
				WHERE
				ware_fabricgptransferinheader.intGPNo =  '$gpNo' AND 
				ware_fabricgptransferinheader.intGPYear =  '$gpYear' AND 
				ware_fabricgptransferinheader.intOrderNo =  '$orderNo' AND
				ware_fabricgptransferinheader.intOrderYear =  '$orderYear' AND
				ware_fabricgptransferindetails.strCutNo =  '$cutNo' AND
				ware_fabricgptransferindetails.intSalesOrderId =  '$salesOrderId' AND
				ware_fabricgptransferindetails.intPart =  '$partId' AND
				ware_fabricgptransferindetails.intGroundColor =  '$bgColId' AND
				ware_fabricgptransferindetails.strLineNo =  '$line' AND
				ware_fabricgptransferindetails.strSize =  '$size' AND  
				ware_fabricgptransferinheader.intStatus >= '1'  AND 
				ware_fabricgptransferinheader.intStatus <= ware_fabricgptransferinheader.intApproveLevels  AND 
				ware_fabricgptransferinheader.intCompanyId ='$trnfLocation' 
				";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
		
}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//--------------------------------------------------------

?>

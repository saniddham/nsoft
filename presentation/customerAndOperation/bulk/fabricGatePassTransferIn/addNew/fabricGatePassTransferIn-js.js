var basepath	= 'presentation/customerAndOperation/bulk/fabricGatePassTransferIn/addNew/';

$(document).ready(function() {
  		$("#frmFabricGpTransferIn").validationEngine();
		$("#frmFabricGpTransferIn #cboGPNo").die('change').live('change',function(){
			submitForm();
		});
		$("#frmFabricGpTransferIn #cboGPYear").die('change').live('change',function(){
			submitForm();
		});
		
	
  $('#frmFabricGpTransferIn #butSave').die('click').live('click',function(){
	var requestType = '';
	if ($('#frmFabricGpTransferIn').validationEngine('validate'))   
    { 
		showWaiting();
		var data = "requestType=save";
		
			data+="&serialNo="		+	$('#txtSerialNo').val();
			data+="&Year="	+	$('#txtYear').val();
			data+="&orderNo="	+	$('#cboOrderNo').val();
			data+="&orderYear="	+	$('#cboOrderYear').val();
			data+="&gpNo="	+	$('#cboGPNo').val();
			data+="&gpYear="	+	$('#cboGPYear').val();
			data+="&date="	+	$('#dtDate').val();
			data+="&remarks="	+	URLEncode($('#txtRemarks').val());

			var rowCount = document.getElementById('tblMain').rows.length;
			if($('#cboGPNo').val()==''){
				alert("Please select the Gate Pass No");hideWaiting();
				return false;				
			}
			else if(rowCount==1){
				alert("No Items Transfered");hideWaiting();
				return false;				
			}
			var row = 0;
			var err=0;
			
			var arr="[";
			
			$('#tblMain .cutNo').each(function(){
				
				var cutNo	= $(this).attr('id');
				var salesOrderId	= $(this).parent().find(".salesOrderNo").attr('id');
				var salesOrderNo	= $(this).parent().find(".salesOrderNo").html();
				var partId	= $(this).parent().find(".part").attr('id');
				var part	= $(this).parent().find(".part").html();
				var bgColorId	= $(this).parent().find(".bgCol").attr('id');
				var bgColor	= $(this).parent().find(".bgCol").html();
				var line	= $(this).parent().find(".line").html();
				var size	= $(this).parent().find(".size").html();
				var qty	= $(this).parent().find(".qty").val();
	
					if(qty>0){
				        arr += "{";
						arr += '"cutNo":"'+	URLEncode(cutNo) +'",' ;
						arr += '"salesOrderId":"'+	salesOrderId +'",' ;
						arr += '"salesOrderNo":"'+	salesOrderNo +'",' ;
						arr += '"partId":"'+	partId +'",' ;
						arr += '"part":"'+	URLEncode(part) +'",' ;
						arr += '"bgColorId":"'+	bgColorId +'",' ;
						arr += '"bgColor":"'+	URLEncode(bgColor) +'",' ;
						arr += '"line":"'+	URLEncode(line) +'",' ;
						arr += '"size":"'+	URLEncode(size) +'",' ;
						arr += '"qty":"'+		qty +'"' ;
						arr +=  '},';
						
					}
					if(qty==0){
						err=1;
					}
			});
			
			if(err==1){
			//	alert("Please select quantities");hideWaiting();
			//	return false;
			}
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basepath+"fabricGatePassTransferIn-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			type:'POST', 
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmFabricGpTransferIn #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertx()",1000);
						$('#txtSerialNo').val(json.serialNo);
						$('#txtYear').val(json.year);
						if(json.confirmationMode==1){
							$('#butConfirm').show();
						}
						else{
							$('#butConfirm').hide();
						}
					//	document.location.href = document.location.href; 
					}
				},
			error:function(xhr,status){
					
					$('#frmFabricGpTransferIn #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
			hideWaiting();
	}
   });

$('#frmFabricGpTransferIn #butReport').die('click').live('click',function(){
	if($('#txtSerialNo').val()!=''){
		window.open('?q=939&serialNo='+$('#txtSerialNo').val()+'&year='+$('#txtYear').val());	
	}
	else{
		alert("There is no Fabric Gate Pass Transferin No to view");
	}
});
//----------------------------------	
$('#frmFabricGpTransferIn #butConfirm').die('click').live('click',function(){ 
	if($('#txtSerialNo').val()!=''){
		window.open('?q=939&serialNo='+$('#txtSerialNo').val()+'&year='+$('#txtYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no Fabric Gate Pass Transferin No to view");
	}
});
//-----------------------------------------------------
$('#butClose').die('click').live('click',function(){
});

//--------------refresh the form---------------
	$('#frmFabricGpTransferIn #butNew').die('click').live('click',function(){
		window.location.href = "?q=48";
		$('#frmFabricGpTransferIn').get(0).reset();
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmFabricGpTransferIn #dtDate').val(d);
	});
	//----------------------------------------

});//----------end of ready --------

//-------------------------------------
function alertx()
{
	$('#frmInvoice #butSave').validationEngine('hide')	;
}
//----------------------------------------------- 
function alertDelete()
{
	$('#frmInvoice #butDelete').validationEngine('hide')	;
}
//----------------------------------------------- 
function closePopUp(){
	
}
//-----------------------------------------------
function loadOrderNo(){
	    var orderYear = $('#cboOrderYear').val();
	    var poNo = $('#cboPONo').val();
	    var customer = $('#cboCustomer').val();
		var url 		= "fabricGatePass-db-get.php?requestType=loadOrderNo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"poNo="+poNo+"&customer="+customer+"&orderYear="+orderYear,
			async:false,
			success:function(json){
				
					document.getElementById("cboOrderNo").innerHTML=json.orderNo;
					document.getElementById("cboCustomer").value=json.customer;
					if(poNo==''){
					document.getElementById("cboPONo").innerHTML=json.PoNo;
					}
			}
		});
}
//--------------------------------------------------
function loadCustomerPO(){
	    var orderNo = $('#cboOrderNo').val();
	    var orderYear = $('#cboOrderYear').val();
		var url 		= "fabricGatePass-db-get.php?requestType=loadPoNo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"orderNo="+orderNo+"&orderYear="+orderYear,
			async:false,
			success:function(json){
				
					document.getElementById("cboPONo").value=json.poNo;
					document.getElementById("cboCustomer").value=json.customer;
					if(orderNo==''){
					document.getElementById("cboOrderNo").innerHTML=json.orderNo;
					}
					//document.getElementById("cboSalesOrderNo").innerHTML=json.salesOrderNo;
			}
		});
}
//-----------------------------------------------
function loadPONoAndOrderNo(){
	    var poNo = $('#cboPONo').val();
	    var orderNo = $('#cboOrderNo').val();
	    var orderYear = $('#cboOrderYear').val();
	    var customer = $('#cboCustomer').val();
		var url 		= "fabricGatePass-db-get.php?requestType=loadPONoAndOrderNo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"customer="+customer+"&orderYear="+orderYear,
			async:false,
			success:function(json){
				
					document.getElementById("cboPONo").innerHTML=json.poNo;
					document.getElementById("cboOrderNo").innerHTML=json.orderNo;
			}
		});
}
//-----------------------------------------------
function loadQty(){
	    var orderNo = $('#cboOrderNo').val();
	    var salesOrderNo = '';
		var url 		= "fabricGatePass-db-get.php?requestType=loadQty";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"orderNo="+orderNo+"&salesOrderNo="+salesOrderNo,
			async:false,
			success:function(json){
				
					document.getElementById("txtQty").value=json.qty;
			}
		});
}
//------------------------------------------------
function submitForm(){
	window.location.href = "?q=48&gpNo="+$('#cboGPNo').val()
						+'&gpYear='+$('#cboGPYear').val()
}
//----------------------------------------------- 
function clearGrid()
{
	var rowCount = document.getElementById('tblMain').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMain').deleteRow(1);
	}
	
	$('#txtQty').val('');
}
  //-------------------------------------------------------
 

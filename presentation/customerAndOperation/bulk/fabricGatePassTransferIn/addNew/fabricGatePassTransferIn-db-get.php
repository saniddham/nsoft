<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	$programName='Fabric Gate Pass Transfer In';
	$programCode='P0048';
	
	/////////// type of print load part /////////////////////
if($requestType=='loadOrderNo')
	{
		$orderYear  = $_REQUEST['orderYear'];
		$poNo  = $_REQUEST['poNo'];
		$customer  = $_REQUEST['customer'];
		
		$sql = "SELECT
				trn_orderheader.intOrderNo,
				trn_orderheader.intCustomer,
				trn_orderheader.intOrderYear
				FROM trn_orderheader
				WHERE
				trn_orderheader.intOrderYear='$orderYear'";
				if($poNo!=''){
		$sql .= " AND trn_orderheader.strCustomerPoNo='$poNo'";			
				}
				if($customer!=''){
		$sql .= " AND trn_orderheader.intCustomer='$customer'";			
				}
		$sql .= " ORDER BY
				 trn_orderheader.intOrderNo DESC"; 
				 
				
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
			$customer=$row['intCustomer'];
		}
			$response['orderNo'] = $html;
			if($poNo==''){
				$response['customer'] = '';
			}
			else{
				$response['customer'] = $customer;
			}
		if($poNo==''){
			//	$response['orderNo'] = '';
				$response['customer'] = '';
				
			 $sql = "SELECT
					trn_orderheader.strCustomerPoNo 
					FROM trn_orderheader
					WHERE
					trn_orderheader.intOrderYear='$orderYear' 
					ORDER BY 
					trn_orderheader.strCustomerPoNo ASC";
			$html = "<option value=\"\"></option>";
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				$html .= "<option value=\"".$row['strCustomerPoNo']."\">".$row['strCustomerPoNo']."</option>";
			}
				$response['PoNo'] = $html;
		}
		
		echo json_encode($response);
	}
	//------------------------------
	else if($requestType=='loadPoNo')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderYear  = $_REQUEST['orderYear'];
		
		$sql = "SELECT
				trn_orderheader.strCustomerPoNo,
				trn_orderheader.intCustomer 
				FROM trn_orderheader 
				WHERE
				trn_orderheader.intOrderNo='$orderNo' 
				AND trn_orderheader.intOrderYear='$orderYear'
				ORDER BY 
				trn_orderheader.strCustomerPoNo ASC";

		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$response['poNo'] = $row['strCustomerPoNo'];
				$response['customer'] = $row['intCustomer'];
		}
		if($orderNoArray[0]==''){
				$response['poNo'] = '';
				$response['customer'] = '';
	
		//---order no
			 $sql = "SELECT
					trn_orderheader.intOrderNo,
					trn_orderheader.intCustomer,
					trn_orderheader.intOrderYear
					FROM trn_orderheader
					WHERE
					trn_orderheader.intOrderYear='$orderYear'
					ORDER BY
				    trn_orderheader.intOrderNo DESC"; 
					
			$html = "<option value=\"\"></option>";
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				$html .= "<option value=\"".$row['intOrderNo']."/".$row['intOrderYear']."\">".$row['intOrderNo']."/".$row['intOrderYear']."</option>";
			}
				$response['orderNo'] = $html;
					
		}
		//---sales order no
		 $sql = "SELECT
				trn_orderdetails.strSalesOrderNo
				FROM
				trn_orderdetails
				WHERE
				trn_orderdetails.intOrderNo='$orderNo' 
				AND trn_orderdetails.intOrderYear='$orderYear'
				ORDER BY 
				trn_orderdetails.strSalesOrderNo ASC";

		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['strSalesOrderNo']."\">".$row['strSalesOrderNo']."</option>";
		}
				$response['salesOrderNo'] = $html;
		
		
		
		echo json_encode($response);
	}
	//----------------------------------
	else if($requestType=='loadPONoAndOrderNo')
	{
		$customer  = $_REQUEST['customer'];
		$orderYear  = $_REQUEST['orderYear'];
		$sql = "SELECT
				trn_orderheader.strCustomerPoNo,
				trn_orderheader.intCustomer 
				FROM trn_orderheader 
				WHERE 
				trn_orderheader.intOrderYear='$orderYear'"; 
				
		if($customer!=''){
		$sql .= " AND  
				trn_orderheader.intCustomer='$customer'"; 
		}
		$sql .= " ORDER BY 
				trn_orderheader.strCustomerPoNo ASC";
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['strCustomerPoNo']."\">".$row['strCustomerPoNo']."</option>";
		}
				$response['poNo'] = $html;
				
		$sql = "SELECT
				trn_orderheader.intOrderNo,
				trn_orderheader.intOrderYear
				FROM trn_orderheader 
				WHERE 
				trn_orderheader.intOrderYear='$orderYear'"; 
		if($customer!=''){
		$sql .= " AND
				trn_orderheader.intCustomer='$customer'"; 
		}
		$sql .= " ORDER BY 
				trn_orderheader.intOrderNo DESC";
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['intOrderNo']."/".$row['intOrderYear']."\">".$row['intOrderNo']."/".$row['intOrderYear']."</option>";
		}
				$response['orderNo'] = $html;
		
		
		echo json_encode($response);
	}
	
//-----------------------------------
	else if($requestType=='loadQty')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderYear  = $_REQUEST['orderYear'];
		$salesOrderNo  = $_REQUEST['salesOrderNo'];
		
		$sql = "SELECT
				Sum(trn_orderdetails.intQty) AS qty
				FROM trn_orderdetails 
				WHERE
				trn_orderdetails.intOrderNo='$orderNo' 
				AND trn_orderdetails.intOrderYear='$orderYear'  
"; 
		if($salesOrderNo!=''){
		$sql .= " AND 
				trn_orderdetails.strSalesOrderNo='$salesOrderNo'"; 
		}
		$sql .= " GROUP BY
				trn_orderdetails.intOrderNo,
				trn_orderdetails.intOrderYear"; 
		
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$response['qty'] = $row['qty'];
		
		echo json_encode($response);
	}
//--------------------------------------------------------------------






?>
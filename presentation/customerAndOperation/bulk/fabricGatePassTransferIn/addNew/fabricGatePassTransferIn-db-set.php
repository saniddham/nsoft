<?php 

ini_set('display_errors',0);
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo 	 = $_REQUEST['serialNo'];
	$year 		 = $_REQUEST['Year'];
	$orderNo 	 = $_REQUEST['orderNo'];
	$orderYear 	 = $_REQUEST['orderYear'];
/*	$styleNo 	 = $_REQUEST['styleNo'];
	$graphicNo 	 = $_REQUEST['graphicNo'];
	$customerPO  = $_REQUEST['customerPO'];
*/	$gpNo 	 = $_REQUEST['gpNo'];
	$gpYear 	 = $_REQUEST['gpYear'];
	$date 	 = $_REQUEST['date'];
	$remarks 	 = $_REQUEST['remarks'];

	$arr 		= json_decode($_REQUEST['arr'], true);
	
	$programName='Fabric Gate Pass Transfer In';
	$programCode='P0048';

	$ApproveLevels = (int)getApproveLevel($programName);
	$maxStatus = $ApproveLevels+1;

//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		$savableFlag=1;
		
		if($serialNo==''){
			$serialNo 	= getNextSerialNo();
			$year = date('Y');
			$editMode=0;
			$savedStatus=$maxStatus;
			$savedLevels=$ApproveLevels;
		}
		else{
			$editMode=1;
			$savableFlag=getSaveStatus($gpNo,$gpYear);//check wether already confirmed
			$sql = "SELECT
			ware_fabricgptransferinheader.intStatus, 
			ware_fabricgptransferinheader.intApproveLevels 
			FROM ware_fabricgptransferinheader 
			WHERE
			ware_fabricgptransferinheader.intFabricGPTransfInNo =  '$serialNo' AND
			ware_fabricgptransferinheader.intFabricGPTransfInYear =  '$year'";

			$result = $db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			$savedStatus=$row['intStatus'];
			$savedLevels=$row['intApproveLevels'];
		}
		    $editPermission=loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
		
		//--------------------------
		$locationFlag=getLocationValidation('FTIN',$location,$company,$gpNo,$gpYear);//check wether already confirmed
		//--------------------------
		
		 if( $locationFlag==1){
			 $rollBackFlag=1;
			 $rollBackMsg="Invalid Save Location";
		 }
		 else if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This Received No is already confirmed.Cant edit.";
		 }
		 else if($editPermission==0){
			 $rollBackFlag=1;
			 $rollBackMsg="No Permission to save.";
		 }
		 else if($editMode==1){
		     $sql = "UPDATE `ware_fabricgptransferinheader` SET intStatus ='$maxStatus', 
												       intApproveLevels ='$ApproveLevels',
													  intGPNo ='$gpNo', 
													  intGPYear ='$gpYear', 
													  intOrderNo ='$orderNo', 
													  intOrderYear ='$orderYear', 
													  strremarks ='$remarks', 
													  dtmdate ='$date', 
													  intModifiedBy ='$userId', 
													  dtmModifiedDate =now(), 
													  intCompanyId= $location 
				WHERE (`intFabricGPTransfInNo`='$serialNo') AND (`intFabricGPTransfInYear`='$year')";
		$result = $db->RunQuery2($sql);
		}
		else{
			//$sql = "DELETE FROM `ware_fabricgptransferinheader` WHERE (`intFabricGPTransfInNo`='$serialNo') AND (`intFabricGPTransfInYear`='$year')";
			//$result1 = $db->RunQuery2($sql);

			$sql = "INSERT INTO `ware_fabricgptransferinheader` (`intFabricGPTransfInNo`,`intFabricGPTransfInYear`,intGPNo,intGPYear,intOrderNo,intOrderYear,strRemarks,intStatus,intApproveLevels,dtmdate,dtmCreateDate,intCteatedBy,intCompanyId) 
					VALUES ('$serialNo','$year','$gpNo','$gpYear','$orderNo','$orderYear','$remarks','$maxStatus','$ApproveLevels','$date',now(),'$userId','$location')";
			$result = $db->RunQuery2($sql);
		}
		//-----------delete and insert to detail table-----------------------
		if(($result) && ($rollBackFlag!=1)){
			$sql = "DELETE FROM `ware_fabricgptransferindetails` WHERE (`intFabricGPTransfInNo`='$serialNo') AND (`intFabricGPTransfInYear`='$year')";
			$result2 = $db->RunQuery2($sql);
			$saved=0;
			$toSave=0;
			$rollBackMsg="Maximum Qty for followings...";
			foreach($arr as $arrVal)
			{
				$cutNo = $arrVal['cutNo'];
				$salesOrderId 	 = $arrVal['salesOrderId'];
				$salesOrderNo	 = $arrVal['salesOrderNo'];
				$partId 	 = $arrVal['partId'];
				$part 	 = $arrVal['part'];
				$bgColorId 		 = $arrVal['bgColorId'];
				$bgColor 	 = $arrVal['bgColor'];
				$line 	 = $arrVal['line'];
				$size 		 = $arrVal['size'];
				$qty 		 = round($arrVal['qty'],4);
				
				$gatePassed=loadGatePassedQty($location,$gpNo,$gpYear,$orderNo,$orderYear,$cutNo,$salesOrderId,$size,$partId,$line,$bgColorId);
				$transferedQty=loadTransferedQty($location,$gpNo,$gpYear,$orderNo,$orderYear,$cutNo,$salesOrderId,$size,$partId,$line,$bgColorId);
				$maxQty=$gatePassed-$transferedQty;

				//------check maximum FR Qty--------------------
				if($qty>$maxQty){
					$rollBackFlag=1;
					$rollBackMsg .="<br> ".$cutNo."-".$part."-".$salesOrderNo."-".$size." =".$maxQty ;
				}

				//----------------------------
				if($rollBackFlag!=1){
					
					$sql = "INSERT INTO `ware_fabricgptransferindetails` (`intFabricGPTransfInNo`,`intFabricGPTransfInYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`intGroundColor`,`strLineNo`,`strSize`,`dblQty`) 
					VALUES ('$serialNo','$year','$cutNo','$salesOrderId','$partId','$bgColorId','$line','$size','$qty')";
					$result = $db->RunQuery2($sql);
					if($result==1){
					$saved++;
					}
				}
				$toSave++;
		}
		}
		//echo $rollBackFlag;
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$confirmationMode=getConfirmationMode($programCode,$userId,$ApproveLevels,$maxStatus);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
//----------------------------------------
	echo json_encode($response);
//----------------------------------------




//----------------------------------------
	function getNextSerialNo()
	{
		global $db;
		global $location;
		$sql = "SELECT
				sys_no.intFabricGPTransfInNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$location'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextNo = $row['intFabricGPTransfInNo'];
		
		$sql = "UPDATE `sys_no` SET intFabricGPTransfInNo=intFabricGPTransfInNo+1 WHERE (`intCompanyId`='$location')  ";
		$db->RunQuery2($sql);
		
		return $nextNo;
	}
	//--------------------------------------------------------------
function loadGatePassedQty($toLocation,$gpNo,$gpYear,$x_orderNo,$x_orderYear,$cutNo,$salesOrderId,$size,$partId,$line,$bgColId)
{
		global $db;
		  $sql = "SELECT
				ware_fabricgatepassdetails.dblQty 
				FROM 
				ware_fabricgatepassheader
				Inner Join ware_fabricgatepassdetails ON ware_fabricgatepassheader.intFabricGatePassNo = ware_fabricgatepassdetails.intFabricGatePassNo AND ware_fabricgatepassheader.intFabricGatePassYear = ware_fabricgatepassdetails.intFabricGatePassYear
				WHERE
				ware_fabricgatepassheader.intFabricGatePassNo =  '$gpNo' AND
				ware_fabricgatepassheader.intFabricGatePassYear =  '$gpYear' AND 
				ware_fabricgatepassheader.intOrderNo =  '$x_orderNo' AND
				ware_fabricgatepassheader.intOrderYear =  '$x_orderYear' AND 
				ware_fabricgatepassdetails.strCutNo =  '$cutNo' AND
				ware_fabricgatepassdetails.intSalesOrderId =  '$salesOrderId' AND
				ware_fabricgatepassdetails.intPart =  '$partId' AND
				ware_fabricgatepassdetails.strSize =  '$size' AND
				ware_fabricgatepassdetails.intGroundColor =  '$bgColId' AND
				ware_fabricgatepassdetails.strLineNo =  '$line' AND 
				ware_fabricgatepassheader.intGatepassTo =  '$toLocation' AND 
				ware_fabricgatepassheader.intStatus='1'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
		
}
	//--------------------------------------------------------------
function loadTransferedQty($trnfLocation,$gpNo,$gpYear,$orderNo,$orderYear,$cutNo,$salesOrderId,$size,$partId,$line,$bgColId)
{
		global $db;
	  	$sql = "SELECT
				Sum(ware_fabricgptransferindetails.dblQty) AS dblQty
				FROM
				ware_fabricgptransferinheader
				Inner Join ware_fabricgptransferindetails ON ware_fabricgptransferinheader.intFabricGPTransfInNo = ware_fabricgptransferindetails.intFabricGPTransfInNo AND ware_fabricgptransferinheader.intFabricGPTransfInYear = ware_fabricgptransferindetails.intFabricGPTransfInYear
				WHERE 
				ware_fabricgptransferinheader.intGPNo =  '$gpNo' AND 
				ware_fabricgptransferinheader.intGPYear =  '$gpYear' AND 
				ware_fabricgptransferinheader.intOrderNo =  '$orderNo' AND
				ware_fabricgptransferinheader.intOrderYear =  '$orderYear' AND
				ware_fabricgptransferindetails.strCutNo =  '$cutNo' AND
				ware_fabricgptransferindetails.intSalesOrderId =  '$salesOrderId' AND
				ware_fabricgptransferindetails.intPart =  '$partId' AND
				ware_fabricgptransferindetails.intGroundColor =  '$bgColId' AND
				ware_fabricgptransferindetails.strLineNo =  '$line' AND
				ware_fabricgptransferindetails.strSize =  '$size' AND  
				ware_fabricgptransferinheader.intCompanyId =  '$trnfLocation' AND  
				ware_fabricgptransferinheader.intStatus >= '1'  AND 
				ware_fabricgptransferinheader.intStatus <= ware_fabricgptransferinheader.intApproveLevels  AND 
				ware_fabricgptransferinheader.intCompanyId ='$trnfLocation' 
				";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
		
}
//-----------------------------------------------------------
function getConfirmationMode($programCode,$intUser,$approveLevels,$status){
		global $db;
		$k=$approveLevels+2-$status;
		$sqlp = "SELECT 
		menupermision.intEdit, 
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		if($rowp['int'.$k.'Approval']==1) 
		 $confirmatonMode=1; 
		else
		 $confirmatonMode=0; 

		return $confirmatonMode;
	}
//-----------------------------------------------------------
	function getSaveStatus($serialNo,$year)
	{
		global $db;
		$sql = "SELECT ware_fabricgptransferinheader.intStatus, ware_fabricgptransferinheader.intApproveLevels FROM ware_fabricgptransferinheader WHERE (`intFabricGPTransfInNo`='$serialNo') AND (`intFabricGPTransfInYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			
		return $editableFlag;
	}
//-----------------------------------------------------------
	function getLocationValidation($type,$location,$company,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
		  $sql = "SELECT
					ware_fabricgatepassheader.intGatepassTo
					FROM ware_fabricgatepassheader
					WHERE
					ware_fabricgatepassheader.intFabricGatePassNo =  '$serialNo' AND
					ware_fabricgatepassheader.intFabricGatePassYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			if($location == $row['intGatepassTo'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//--------------------------------------------------------

?>

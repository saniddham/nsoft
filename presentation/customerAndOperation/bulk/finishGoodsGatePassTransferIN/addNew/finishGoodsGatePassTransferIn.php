<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];
$intUser  = $_SESSION["userId"];

include  	"{$backwardseperator}dataAccess/permisionCheck.inc";


$docNo 		= $_REQUEST['docNo'];
$docYear	= $_REQUEST['docYear'];
$gpNo 		= $_REQUEST['gpNo'];
$gpYear 	= $_REQUEST['gpYear'];

$sql = "SELECT
		ware_stocktransactions_fabric_complete.intOrderNo,
		ware_stocktransactions_fabric_complete.intOrderYear,
		trn_orderheader.strCustomerPoNo,
		mst_locations.strName as location,
		ware_stocktransactions_fabric_complete.intToLocationId as intLocationId,
		mst_customer.strName as customer
		FROM
		ware_stocktransactions_fabric_complete
		Inner Join mst_locations ON ware_stocktransactions_fabric_complete.intLocationId = mst_locations.intId
		Inner Join trn_orderheader ON trn_orderheader.intOrderNo = ware_stocktransactions_fabric_complete.intOrderNo AND trn_orderheader.intOrderYear = ware_stocktransactions_fabric_complete.intOrderYear
		Inner Join mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
		WHERE
		ware_stocktransactions_fabric_complete.intDocumentNo =  '$gpNo' AND
		ware_stocktransactions_fabric_complete.intDocumentYear =  '$gpYear'
		";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$orderNo 		= $row['intOrderNo'];	
	$orderYear 		= $row['intOrderYear'];	
	$toLocation 	= $row['location'];	
	$toLocationId 	= $row['intLocationId'];	
	$customer 		= $row['customer'];	
	$strCustomerPoNo= $row['strCustomerPoNo'];	
}
/*	if($serialNo==''){
		
	}
	else{
		$sql = "SELECT
		ware_stocktransactions_fabric.intOrderNo,
		ware_stocktransactions_fabric.intOrderYear
		FROM ware_stocktransactions_fabric
		WHERE
		ware_stocktransactions_fabric.intDocumentNo =  '$serialNo' AND
		ware_stocktransactions_fabric.intDocumentYear =  '$year'";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$orderNo 	= $row['intOrderNo'];
		$orderYear 	= $row['intOrderYear'];
	}*/
////$salesOrderNo = $_REQUEST['salesOrderNo'];//
//$poNo = $_REQUEST['poNo'];/
//$customer = $_REQUEST['customer'];
//----------------
/*   $sql = "SELECT
trn_orderheader.strCustomerPoNo,
trn_orderheader.intCustomer
FROM trn_orderheader
WHERE
trn_orderheader.intOrderNo =  '$orderNo' AND
trn_orderheader.intOrderYear =  '$orderYear'"; 
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);

$x_poNo = $row['strCustomerPoNo'];
$x_custId = $row['intCustomer'];
if($x_poNo=='')
$x_poNo=$poNo;*/


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Finish Goods GatePass Transfer In</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="finishGoodsGatePassTransferIn-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../../libraries/calendar/theme.css" />
<script src="../../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body>
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>


<form id="frmFinishGoodsGatepassTransferIn" name="frmFinishGoodsGatepassTransferIn" method="post" action="frmFinishGoodsGatepassTransferIn.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL" style="width:800">
		  <div class="trans_text">FINISH GOODS GATEPASS-TRANSFER IN</div>
		  <table width="725" border="0" align="center" bgcolor="#FFFFFF">
    <td width="719"><table width="86%" border="0">
    <tr><td align="center"><table class="tableBorder_allRound" width="95%">
      <tr>
        <td><table width="99%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td width="15%" height="22" class="normalfnt">Serial No</td>
  <td width="39%" class="normalfnt"><input  id="txtDocumentNo" class="normalfnt" style="width:70px;text-align:right" type="text" value="<?php echo $serialNo ?>" readonly="readonly"/><input  id="txtDocumentYear" class="normalfnt" style="width:40px;text-align:right" type="text" value="<?php echo $year ?>" readonly="readonly"/></td>
  <td width="11%" align="center" class="normalfntMid">GP Year</td>
  <td width="12%" class="normalfnt"><select name="cboGPYear" id="cboGPYear" style="width:70px">
    <?php
					$sql = "SELECT DISTINCT
							trn_orderheader.intOrderYear
							FROM trn_orderheader
							ORDER BY
							trn_orderheader.intOrderYear DESC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intOrderYear']==$x_year)
						echo "<option value=\"".$row['intOrderYear']."\" selected=\"selected\">".$row['intOrderYear']."</option>";	
						else
						echo "<option value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";	
					}
				?>
  </select></td>
  <td width="9%" class="normalfntMid">GP No</td>
  <td width="14%" bgcolor="#FFFFFF" class="normalfnt"><select name="cboGPNo" id="cboGPNo" style="width:90px">
    <?php
					$year =  date('Y');
					$sql = "SELECT DISTINCT
								ware_stocktransactions_fabric_complete.intDocumentNo as gpNo
							FROM
								ware_stocktransactions_fabric_complete
							WHERE
								ware_stocktransactions_fabric_complete.intToLocationId =  '$location' AND
								intOrderYear = $year and
								ware_stocktransactions_fabric_complete.strType in( 'COMPLETE-GPOUT' ,'PDAMAGE-GPOUT','FDAMAGE-GPOUT' )
							";
					$result = $db->RunQuery($sql);
					echo "<option value=\"\"></option>";
					while($row=mysqli_fetch_array($result))
					{
						if($row['gpNo']==$gpNo)
							echo "<option selected=\"selected\" value=\"".$row['gpNo']."\" >".$row['gpNo']."</option>";	
						else
							echo "<option value=\"".$row['gpNo']."\">".$row['gpNo']."</option>";	
					}
				?>
  </select></td>
  </tr>        </table></td>
      </tr>
    </table></td></tr>
    <tr>
      <td align="center"><table bgcolor="#DEF2FE" width="689" border="0" cellpadding="0" cellspacing="0" class="tableBorder_allRound">
        <tr>
          <td width="18" class="normalfnt">&nbsp;</td>
          <td width="69" height="27" class="normalfnt">Order Year</td>
          <td width="72" class="normalfnt"><strong>:<span id="spanOrderYear"><?php echo $orderYear; ?></span></strong></td>
          <td width="82" class="normalfnt">Costomer PO</td>
          <td width="140" class="normalfnt"><strong>:<?php echo $strCustomerPoNo; ?></strong></td>
          <td width="106" class="normalfnt">Order No</td>
          <td width="200" class="normalfnt"><strong>:<span id="spanOrderNo"><?php echo $orderNo; ?></span></strong></td>
        </tr>
        <tr>
          <td class="normalfnt">&nbsp;</td>
          <td height="22" class="normalfnt">Customer</td>
          <td colspan="3" class="normalfnt"><strong>:<?php echo $customer; ?></strong></td>
          <td class="normalfnt">GatePass From</td>
          <td class="normalfnt"><strong>:<?php echo $toLocation; ?></strong></td>
        </tr>
      </table></td>
    </tr>
    <tr><td align="center" class="normalfntsm">&nbsp;</td></tr>
      <tr>
        <td><div style="width:700px;height:350px;overflow:scroll" >
          <table width="100%" class="grid" id="tblMain" >
            <tr class="gridHeader">
              <td width="23%" height="20" >Sales Order #</td>
              <td width="11%" >Size</td>
              <td width="17%" >Cut No</td>
              <td width="11%">Good Qty</td>
              <td width="13%">Factory Damage Qty</td>
              <td width="12%">Production Damage Qty</td>
              <td width="13%">Cut Return Qty</td>
              </tr>
              <?php
				$totAmm=0;
				$result=loadDetails($gpNo,$gpYear,$toLocationId);
				while($row=mysqli_fetch_array($result))
				{
					$salesOrderNo		=	$row['strSalesOrderNo'];
					$damageQty			=	$row['damageQty'];
					$salesOrderId		=	$row['intSalesOrderId'];
					$size				=	$row['strSize'];
					$grade				=	$row['intGrade'];
					
					$goodQTy		=val($row['goodQTy']);
					$fdQty			=val($row['fdQty']);
					$pdQty			=val($row['pdQty']);
					$cutReturnQty	=val($row['cutReturnQty']);
					
					if(($goodQTy>0) ||($fdQty>0)||($pdQty>0)||($cutReturnQty>0)){
			  ?>
<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" id="<?php echo $salesOrderId; ?>"><?php echo $salesOrderNo; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $size; ?>" ><?php echo $size; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $grade; ?>" class="item"><?php echo $grade; ?></td>
			<td align="right" bgcolor="#FFFFFF" id="<?php echo $goodQTy; ?>"  class="balToInvQty"><input  id="txtGoodQty" class="validate[custom[number],max[<?php echo $goodQTy;?>],min[0]] invoQty normalfnt" style="width:70px;text-align:right" type="text" value="<?php /*echo $goodQTy*/;  ?>" /></td>
			<td align="right" bgcolor="#FFFFFF" id="<?php echo $fdQty; ?>"  class="balToInvQty"><input  id="txtFDQty" class="validate[custom[number],max[<?php echo $fdQty;?>]] invoQty normalfnt" style="width:70px;text-align:right" type="text" value="<?php /*echo $fdQty*/;  ?>" /></td>
			<td align="right" bgcolor="#FFFFFF" id="<?php echo $pdQty; ?>"  class="balToInvQty"><input  id="txtPDQty" class="validate[custom[number],max[<?php echo $pdQty;?>]] invoQty normalfnt" style="width:70px;text-align:right" type="text" value="<?php  /*echo $pdQty*/ ?>" /></td>
			<td align="right" bgcolor="#FFFFFF" id="<?php echo $cutReturnQty; ?>"  class="balToInvQty"><input  id="txtCutReturnQty" class="validate[custom[number],max[<?php echo $cutReturnQty;?>]] invoQty normalfnt" style="width:70px;text-align:right" type="text" value="<?php  /*echo $cutReturnQty;*/ ?>" /></td>
            </tr>       
             <?php
					}
				}
			  ?>  
         </table>
        </div></td>
</tr>
<?php
if($serialNo=='')
$editMode=1;
?>
<tr>
        <td align="center" class="tableBorder_allRound"><img src="../../../../../images/Tnew.jpg" width="92" height="24" id="butNew" name="butNew"  class="mouseover"/>
          <?php if($editMode==1){ ?><img src="../../../../../images/Tsave.jpg" width="92" height="24" id="butSave" name="butSave"  class="mouseover"/><?php } ?>          <img src="../../../../../images/Treport.jpg" width="92" height="24" id="butReport" name="butReport"  class="mouseover"/><img src="../../../../../planning/img/Tclose.jpg" width="92" height="24"  class="mouseover"/></td>
</tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>


</body>
</html>
<?php
	//--------------------------------------------------------------
function loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size)
{
		global $db;
		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				ware_stocktransactions_fabric.intLocationId =  '$location' AND
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType =  'Received'
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
		
}
	//--------------------------------------------------------------
	
function loadDetails($gpNo,$gpYear,$toLocationId){
		global $db; 
		global $location;
		if($serialNo==''){
		$sql = "	SELECT DISTINCT
						trn_orderdetails.strSalesOrderNo,A.intSalesOrderId,
						A.strSize,
						A.intGrade,
						(
							SELECT
						Sum(ware_stocktransactions_fabric_complete.dblQty*-1)
						FROM ware_stocktransactions_fabric_complete
					WHERE
					
						ware_stocktransactions_fabric_complete.intOrderNo = A.intOrderNo AND
						ware_stocktransactions_fabric_complete.intOrderYear = A.intOrderYear AND
						ware_stocktransactions_fabric_complete.intToLocationId =  A.intToLocationId  AND
						ware_stocktransactions_fabric_complete.intSalesOrderId =  A.intSalesOrderId AND
						ware_stocktransactions_fabric_complete.strSize = A.strSize AND
						ware_stocktransactions_fabric_complete.intGrade = A.intGrade AND
						ware_stocktransactions_fabric_complete.strType in ('COMPLETE', 'COMPLETE-GPIN','COMPLETE-GPOUT','COMPLETE-DISPATCH')
					
					
					) AS goodQTy,
					
						(SELECT
					Sum(ware_stocktransactions_fabric_complete.dblQty*-1)
					FROM ware_stocktransactions_fabric_complete
					WHERE
					
					ware_stocktransactions_fabric_complete.intOrderNo = A.intOrderNo AND
					ware_stocktransactions_fabric_complete.intOrderYear = A.intOrderYear AND
					ware_stocktransactions_fabric_complete.intToLocationId =  A.intToLocationId  AND
					ware_stocktransactions_fabric_complete.intSalesOrderId =  A.intSalesOrderId AND
					ware_stocktransactions_fabric_complete.strSize = A.strSize AND
					ware_stocktransactions_fabric_complete.intGrade = A.intGrade AND
					ware_stocktransactions_fabric_complete.strType in ('FDAMAGE', 'FDAMAGE-GPIN','FDAMAGE-GPOUT','FDAMAGE-DISPATCH'  )
					
					
					) AS fdQty,
					
						(SELECT
					Sum(ware_stocktransactions_fabric_complete.dblQty*-1)
					FROM ware_stocktransactions_fabric_complete
					WHERE
					
					ware_stocktransactions_fabric_complete.intOrderNo = A.intOrderNo AND
					ware_stocktransactions_fabric_complete.intOrderYear = A.intOrderYear AND
					ware_stocktransactions_fabric_complete.intToLocationId =  A.intToLocationId  AND
					ware_stocktransactions_fabric_complete.intSalesOrderId =  A.intSalesOrderId AND
					ware_stocktransactions_fabric_complete.strSize = A.strSize AND
					ware_stocktransactions_fabric_complete.intGrade = A.intGrade AND
					ware_stocktransactions_fabric_complete.strType in ('PDAMAGE', 'PDAMAGE-GPIN','PDAMAGE-GPOUT','PDAMAGE-DISPATCH'  )
					
					
					) AS pdQty,
					
						(SELECT
					Sum(ware_stocktransactions_fabric_complete.dblQty*-1)
					FROM ware_stocktransactions_fabric_complete
					WHERE
					
					ware_stocktransactions_fabric_complete.intOrderNo = A.intOrderNo AND
					ware_stocktransactions_fabric_complete.intOrderYear = A.intOrderYear AND
					ware_stocktransactions_fabric_complete.intToLocationId =  A.intToLocationId  AND
					ware_stocktransactions_fabric_complete.intSalesOrderId =  A.intSalesOrderId AND
					ware_stocktransactions_fabric_complete.strSize = A.strSize AND
					ware_stocktransactions_fabric_complete.intGrade = A.intGrade AND
					ware_stocktransactions_fabric_complete.strType in ('CUTRETURN', 'CUTRETURN-GPIN','CUTRETURN-GPOUT','CUTRETURN-DISPATCH'  )
					
					
					) AS cutReturnQty
					
					FROM ware_stocktransactions_fabric_complete as A
					Inner Join trn_orderdetails ON trn_orderdetails.intOrderNo = A.intOrderNo AND trn_orderdetails.intOrderYear = A.intOrderYear AND trn_orderdetails.intSalesOrderId = A.intSalesOrderId
					WHERE
					A.intDocumentNo =  '$gpNo' AND
					A.intDocumentYear =  '$gpYear' and A.intToLocationId = $toLocationId
				";
		}
		else{
		
		}

			return $result = $db->RunQuery($sql);
	}
?>

<?php
session_start();
$backwardseperator = "../../../../../";
$backwardseperator = "../../../../../";
$location = $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];

$mainPath = $_SESSION['mainPath'];
$intUser  = $_SESSION["userId"];
$thisFilePath =  $_SERVER['PHP_SELF'];

$programName='Fabric Transfer Note';
$programCode='P0046';

//$formName		  	= 'frmItem';
include  	"{$backwardseperator}dataAccess/permisionCheck.inc";


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Fabric Return To Stores Listing</title>

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />


</head>

<body>
<form id="frmFabricGatePassListing" name="frmFabricGatePassListing" method="get" action="fabricGatePassListing.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<?php
$stat = $_GET['cboStatus'];
if($stat==''){
$stat='2';
}
$gpApproveLevel = (int)getApproveLevel($programName);
?>

<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">FABRIC RETURN TO STORES
		    <select style="width:100px" name="cboStatus" id="cboStatus" onchange="submit();">
             <option <?php if($stat==2){ ?> selected="selected" <?php } ?>value="2">Pending</option>
              <option <?php if($stat==1){ ?> selected="selected" <?php } ?>value="1">Confirmed</option>
              <option <?php if($stat==0){ ?> selected="selected" <?php } ?>value="0">Rejected</option>
            <option  <?php if($stat==3){ ?> selected="selected" <?php } ?> value="3">All</option>
	        </select>
LISTING</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableBorder_allRound">
          <tr>
            <td width="10%" height="22" class="normalfnt">&nbsp;</td>
            <td width="22%">&nbsp;</td>
            <td width="17%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="11%" class="normalfnt">&nbsp;</td>
            <td width="15%">&nbsp;</td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="grid" >
            <tr class="gridHeader">
              <td width="8%" height="21" >Serial No</td>
              <td width="8%" height="21" >Transferin No</td>
              <td width="11%" height="21" >Order No</td>
              <td width="16%">Date</td>
              <td width="13%">User</td>
              <?php
//$grnApproveLevel = (int)getApproveLevel($programName);
				for($i=1; $i<=$gpApproveLevel; $i++)
				{
					if($i==1)
					$ap="1st Approval";
					else if($i==2)
					$ap="2nd Approval";
					else if($i==3)
					$ap="3rd Approval";
					else
					$ap=$i."th Approval";
					
					
					
			  ?>
              <td width="12%"><?php echo $ap ?></td>
              <?php
				}
			  ?>
              <td width="11%"> Report</td>
              </tr>
                 <?php
				 
				 $stat=3;//--------------------------temp
				 if($stat=='3')//all
				 $sqlCon="";
				 else if($stat==0)//rej
				 $sqlCon="and ware_gatepassheader.intStatus=0";
				 else if($stat==1)//conf
				 $sqlCon="and ware_gatepassheader.intStatus=1";
				 else if($stat==2)//pending
				 $sqlCon="and ware_gatepassheader.intStatus<=ware_gatepassheader.intApproveLevels+1 and ware_gatepassheader.intStatus>1";
				 
	 	 		 $sql = "SELECT
ware_stocktransactions_fabric.intOrderNo,
ware_stocktransactions_fabric.intOrderYear,
ware_stocktransactions_fabric.intSalesOrderId,
trn_orderdetails.strSalesOrderNo,
ware_stocktransactions_fabric.strSize,
ware_stocktransactions_fabric.intGrade,
ware_stocktransactions_fabric.dblQty,
ware_stocktransactions_fabric.dtDate, 
sys_users.strUserName,
mst_locations.strName as gatePassTo, 
ware_stocktransactions_fabric.intDocumentNo,
ware_stocktransactions_fabric.intDocumentYear ,
ware_stocktransactions_fabric.intParentDocumentNo,
ware_stocktransactions_fabric.intParentDocumentYear 
FROM
ware_stocktransactions_fabric
Inner Join trn_orderdetails ON ware_stocktransactions_fabric.intOrderNo = trn_orderdetails.intOrderNo AND ware_stocktransactions_fabric.intOrderYear = trn_orderdetails.intOrderYear AND ware_stocktransactions_fabric.intSalesOrderId = trn_orderdetails.intSalesOrderId
Inner Join sys_users ON ware_stocktransactions_fabric.intUser = sys_users.intUserId
Inner Join mst_locations ON ware_stocktransactions_fabric.intToLocationId = mst_locations.intId
WHERE
ware_stocktransactions_fabric.intLocationId =  '$location'
AND 
ware_stocktransactions_fabric.strType =  'ReturnFrom' 
GROUP BY 
ware_stocktransactions_fabric.intDocumentNo,
ware_stocktransactions_fabric.intDocumentYear 
 
";
//echo $sql;
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					 $serialNo=$row['intDocumentNo'];
					 $year=$row['intDocumentYear'];
					 $pearentNo=$row['intParentDocumentNo'];
					 $pearentYear=$row['intParentDocumentYear'];
					 $orderNo=$row['intOrderNo'];
					 $orderYear=$row['intOrderYear'];
					 $date=$row['dtDate'];
					 $user=$row['strUserName'];
					 $status=$row['intStatus'];
					 $savedLevels=$row['intApproveLevels'];
	  			 ?>
            <tr class="normalfnt">
              <?php
				$editMode=0;
				if($row['intStatus']==0)
				$editMode=1;
				else if($row['intApproveLevels']+1==$row['intStatus'])
				$editMode=1;
				//user can edit only saved and rejected return notes ...... 
				$editMode=1;//-------------------------------temp
			  ?>
              <td align="center" bgcolor="#FFFFFF"><?php if($editMode==1){ ?><a href="../addNew/fabricReturnToStores.php?serialNo=<?php echo $serialNo?>&year=<?php echo $year?>" target="_blank"> <?php } ?><?php echo $serialNo."/".$year;?><?php if($editMode==1){ ?></a><?php } ?></td>
              <td bgcolor="#FFFFFF"><?php echo $pearentNo."/".$pearentYear;?></td>
              <td bgcolor="#FFFFFF"><?php echo $orderNo."/".$orderYear;?></td>
              <td bgcolor="#FFFFFF"><?php echo $date?></td>
              <td bgcolor="#FFFFFF"><?php echo $user?></td>
              <?php
			//  $grnApproveLevel = (int)getApproveLevel($programName);
			  $tempUser="";
				for($i=1; $i<=$gpApproveLevel; $i++)
				{

					 $sqlc = "SELECT
							ware_gatepassheader_approvedby.intApproveUser,
							ware_gatepassheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_gatepassheader_approvedby.intApproveLevelNo
							FROM
							ware_gatepassheader_approvedby
							Inner Join sys_users ON ware_gatepassheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_gatepassheader_approvedby.intGatePassNo  =  '$issueNo' AND
							ware_gatepassheader_approvedby.intYear =  '$year' AND
							ware_gatepassheader_approvedby.intApproveLevelNo =  '$i'
";
					//$grnApproveLevel = (int)getApproveLevel($programName);
					
					$resultc = $db->RunQuery($sqlc);
					$rowc=mysqli_fetch_array($resultc);

					$k=$savedLevels+2-$status;//next approve level
					 $sqlp = "SELECT
							menupermision.int".$k."Approval 
							FROM menupermision 
							Inner Join menus ON menupermision.intMenuId = menus.intId
							WHERE
							menus.strCode =  '$programCode' AND
							menupermision.intUserId =  '$intUser'";	
							
					 $resultp = $db->RunQuery($sqlp);
					 $rowp=mysqli_fetch_array($resultp);
					 
					$app=0; 
					if($status==0)
						$app=0; 
					else if($rowp['int'.$i.'Approval']==1){
					 if($i==1)
						$app=1; 
					 else if($tempUser!='')
						$app=1; 
					}

				?>
              <td bgcolor="#FFFFFF" align="center"><?php echo $rowc['UserName']?>
			  <?php if($rowc['UserName']==''){ ?>
			  <?php if(($app==1) && ($savedLevels>=$i)){?>
              <a href="fabricReturnToStores.php?serialNo=<?php echo $serialNo?>&year=<?php echo $year?>&approveMode=1" target="_blank"><span class="normalfntBlue"><strong >Approve</strong></span></a>
			  <?php } ?>
              <?php }
			  else{
				   echo "(".$rowc['dtApprovedDate'].")"; 
				   } ?>
                   <?php
				   if($i>$savedLevels){
				   		echo "-------";
				   }else if(($status==0) && ($i==$savedLevels)){
					  echo "<strong class=\"compulsoryRed\">Rejected</strong>" ;
				   }
				   ?>
               </td>
              <?php
$tempUser=	$rowc['UserName'];		  
				}
				?>
              
              <td bgcolor="#FFFFFF"><a href="fabricReturnToStores.php?serialNo=<?php echo $serialNo?>&year=<?php echo $year?>" target="_blank"><img src="../../../../../images/view.png" width="91" height="19"  class="mouseover" /></a></td>
              </tr>
              	<?php 
        		 } 
       			 ?>
            </table>
          </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><img src="../../../../../planning/img/Tclose.jpg" width="92" height="24"  class="mouseover"/></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>

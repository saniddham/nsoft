<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$location 				= $sessions->getLocationId();
$company 				= $sessions->getCompanyId();
$intUser  				= $sessions->getUserId();

include_once "class/customerAndOperation/bulk/LC/cls_lc_get.php";		$obj_lc_get				= new Cls_lc_get($db);
include_once "class/cls_commonFunctions_get.php";						$obj_common				= new cls_commonFunctions_get($db);
include_once "class/cls_commonErrorHandeling_get.php";					$obj_commonErrHandle	= new cls_commonErrorHandeling_get($db);

$programCode			= 'P0834';
$serialNo				= (!isset($_REQUEST["serialNo"])?'':$_REQUEST["serialNo"]);
$serialYear				= (!isset($_REQUEST["serialYear"])?'':$_REQUEST["serialYear"]);

$header_arr				= $obj_lc_get->get_header($serialNo,$serialYear,'RunQuery');

$intStatus				= $header_arr['STATUS'];
$levels					= $header_arr['LEVELS'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_save($intStatus,$levels,$intUser,$programCode,'RunQuery');
$permision_save			= $permition_arr['permision'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_cancel($intStatus,$levels,$intUser,$programCode,'RunQuery');
$permision_cancel		= $permition_arr['permision'];

$permition_arr			= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$levels,$intUser,$programCode,'RunQuery');
$permision_confirm		= $permition_arr['permision'];

$baseCurrency			= $obj_lc_get->getCompanyBaseCurrency($company);

?>
<title>Letter of Credit</title>
<form id="frm_lc" name="frm_lc" autocomplete="off" method="post">
<div align="center">
<div class="trans_layoutS" style="width:750px" >
		  <div class="trans_text" align="center">Letter of Credit</div>
		        <table width="100%">
                	<tr>
                    	<td width="100%" colspan="4"><table width="100%" border="0" cellspacing="2" cellpadding="2" class="normalfnt">
                    	  <tr>
                    	    <td width="15%">Serial No</td>
                    	    <td width="38%"><input type="text" name="txtSerialLCYear" style="width:60px" id="txtSerialLCYear" value="<?php echo $serialYear; ?>" disabled="disabled" />&nbsp;<input type="text" style="width:80px" name="txtSerialLCNo" id="txtSerialLCNo" value="<?php echo $serialNo; ?>"  disabled="disabled"/></td>
                    	    <td width="15%">Date <span class="compulsoryRed">*</span></td>
                    	    <td width="32%"><input name="txtDate" type="text" value="<?php  echo ($header_arr['LC_DATE']!=''?$header_arr['LC_DATE']:date("Y-m-d")); ?>" class="txtbox" id="txtDate" style="width:80px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" tabindex="7"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                  	    </tr>
                    	  <tr>
                    	    <td>Order No <span class="compulsoryRed">*</span></td>
                    	    <td><select id="cboOrderYear" name="cboOrderYear" style="width:62px" class="validate[required]">
                           	<option value=""></option>
							<?php 
								$result	= $obj_lc_get->getOrderYear($company);
								while($row = mysqli_fetch_array($result))
								{
									if($header_arr['ORDER_YEAR']==$row['intOrderYear'])
										echo "<option selected=\"selected\" value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";
									else if(date('Y')==$row['intOrderYear'])
										echo "<option selected=\"selected\" value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";
									else
										echo "<option value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";
								
								}
                            ?>
                  	      </select>&nbsp;<select id="cboOrderNo" name="cboOrderNo" style="width:82px" class="validate[required]">
                    	     <?php 
								$result	= $obj_lc_get->getOrderNo($header_arr['ORDER_YEAR'],$header_arr['CUSTOMER_ID'],$company);
								while($row = mysqli_fetch_array($result))
								{
									if($header_arr['ORDER_NO']==$row['intOrderNo'])
										echo "<option selected=\"selected\" value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
									else
										echo "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
								
								}
                            ?>   
                  	        </select></td>
                    	    <td>Customer <span class="compulsoryRed">*</span></td>
                    	    <td><select name="cboCustomer" id="cboCustomer" style="width:230px" class="validate[required]">
                    	    <option value=""></option>
							<?php
								$result = $obj_lc_get->getCustomer();
								while($row = mysqli_fetch_array($result))
								{
									if($header_arr['CUSTOMER_ID']==$row['intId'])
										echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
									else
										echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                            ?>
                  	      	</select></td>
                  	    </tr>
                    	  <tr>
                    	    <td>LC No <span class="compulsoryRed">*</span></td>
                    	    <td><input type="text" name="txtLCNo" id="txtLCNo" maxlength="50" class="validate[required,maxSize[50]]" style="width:230px" value="<?php echo htmlentities($header_arr['LC_NO']); ?>"/></td>
                    	    <td>LC Amount <span class="compulsoryRed">*</span></td>
                    	    <td><input type="text" name="txtAmount" id="txtAmount" class="validate[required,custom[number],min[0]]" style="width:80px;text-align:right" value="<?php echo $header_arr['LC_AMOUNT']; ?>"/>&nbsp;<select id="cboCurrency" name="cboCurrency" style="width:62px" class="validate[required]">
                                <?php
									$result = $obj_lc_get->getCurrency(); 
									while($row = mysqli_fetch_array($result))
									{
										if($header_arr['CURRENCY']==$row['intId'])
											echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strCode']."</option>";
										else if($baseCurrency=$row['intId'])
											echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strCode']."</option>";
										else
											echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
									}
									
								?>
                            </select></td>
                  	    </tr>
                    	  <tr>
                    	    <td rowspan="2" valign="top">Remark</td>
                    	    <td rowspan="2" valign="top"><textarea id="txtRemark" name="txtRemark" style="width:230px;height:40px"><?php echo htmlentities($header_arr['REMARKS']); ?></textarea></td>
                    	    <td valign="top">Bank <span class="compulsoryRed">*</span></td>
                    	    <td valign="top"><select id="cboBank" name="cboBank" style="width:230px" class="validate[required]">
                  	      	<option value=""></option>
                            <?php
							$result		= $obj_lc_get->getBank();
							while($row = mysqli_fetch_array($result))
							{
								if($header_arr['BANK_ID']==$row['BANK_ID'])
										echo "<option selected=\"selected\" value=\"".$row['BANK_ID']."\">".$row['BANK_NAME']."</option>";
									else
										echo "<option value=\"".$row['BANK_ID']."\">".$row['BANK_NAME']."</option>";
							}
							?>
                          </select></td>
                  	    </tr>
                    	  <tr>
                    	    <td valign="top">Bank Ref. No</td>
                    	    <td valign="top"><input type="text" name="txtBankRefNo" id="txtBankRefNo" maxlength="50" style="width:230px" value="<?php echo htmlentities($header_arr['BANK_REF_NO']); ?>" /></td>
                  	    </tr>
                    	  <tr>
                    	    <td valign="top">&nbsp;</td>
                    	    <td valign="top">&nbsp;</td>
                    	    <td valign="top">&nbsp;</td>
                    	    <td valign="top">&nbsp;</td>
                  	    </tr>
       	              </table></td>
                      </tr>
                      <tr>
            		<td>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
            			<tr>
            				<td align="center"><a class="button white medium" id="butNew">New</a><a class="button white medium" id="butSave" <?php if($permision_save!=1){ ?>style="display:none"<?php } ?>>Save</a><a class="button white medium" id="butConfirm"  <?php if($permision_confirm!=1){ ?> style="display:none"<?php } ?>>Approve</a><a class="button white medium" id="butCancel" <?php if($permision_cancel!=1){ ?>  style="display:none"<?php } ?>>Cancel</a><a class="button white medium" id="butReport">Report</a><a href="main.php" class="button white medium" id="butSave">Close</a></td>
            			</tr>
            		</table>
                    </td>
            	</tr>
                </table>
          </div>
</div>
</form>
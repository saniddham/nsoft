var basepath	= 'presentation/customerAndOperation/bulk/LC/addNew/';

$(document).ready(function(e) {
    $("#frm_lc").validationEngine();
	$('#frm_lc #butSave').die('click').live('click',Save);
	$('#frm_lc #cboOrderYear').die('change').live('change',loadOrder);
	$('#frm_lc #cboOrderNo').die('change').live('change',getCustomer);
	$('#frm_lc #cboCustomer').die('change').live('change',getOrderNo);
	$('#frm_lc #butReport').die('click').live('click',viewReport);
	$('#frm_lc #butConfirm').die('click').live('click',approve);
	$('#frm_lc #butNew').die('click').live('click',clearForm);
	$('#frm_lc #butCancel').die('click').live('click',cancel);
	
	if($('#frm_lc #txtSerialLCYear').val()=='' && $('#frm_lc #txtSerialLCNo').val()=='')
		$('#frm_lc #cboOrderYear').change();
});
function loadOrder()
{
	if($('#frm_lc #cboOrderYear').val()=='')
	{
		$('#frm_lc #cboOrderNo').html('');
		return;
	}
	var url 	= basepath+"lc_db.php?RequestType=loadOrder";
	var data 	= "orderYear="+$('#frm_lc #cboOrderYear').val();
	data   	   += "&customer="+$('#frm_lc #cboCustomer').val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frm_lc #cboOrderNo').html(json.orderNoCombo);
			}
	});		
}
function getCustomer()
{
	if($('#frm_lc #cboOrderNo').val()=='')
	{
		return;
	}
	var url 	= basepath+"lc_db.php?RequestType=getCustomer";
	var data 	= "orderYear="+$('#frm_lc #cboOrderYear').val();
	data	   += "&orderNo="+$('#frm_lc #cboOrderNo').val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frm_lc #cboCustomer').val(json.customerId);
			}
	});		
}
function Save()
{
	if(!$('#frm_lc').validationEngine('validate')){
		var t = setTimeout("hideValidation('#frm_lc')",4000);
		return;
	}
		
	var headerArray = "{";
						headerArray += '"RequestType":"URLSave",' ;				
						headerArray += '"SerialLCYear":"'+$('#frm_lc #txtSerialLCYear').val()+'",';
						headerArray += '"SerialLCNo":"'+$('#frm_lc #txtSerialLCNo').val()+'",';
						headerArray += '"LCDate":"'+$('#frm_lc #txtDate').val()+'",';
						headerArray += '"LCNo":'+URLEncode_json($('#frm_lc #txtLCNo').val())+',';
						headerArray += '"CustomerId":"'+$('#frm_lc #cboCustomer').val()+'",';
						headerArray += '"OrderNo":"'+$('#frm_lc #cboOrderNo').val()+'",';
						headerArray += '"OrderYear":"'+$('#frm_lc #cboOrderYear').val()+'",';
						headerArray += '"BankID":"'+$('#frm_lc #cboBank').val()+'",';
						headerArray += '"CurrencyId":"'+$('#frm_lc #cboCurrency').val()+'",';
						headerArray += '"LCAmount":"'+$('#frm_lc #txtAmount').val()+'",';
						headerArray += '"BankRefNo":'+URLEncode_json($('#frm_lc #txtBankRefNo').val())+',';
						headerArray += '"Remarks":'+URLEncode_json($('#frm_lc #txtRemark').val())+'';
		headerArray += "}";
		
	var url 	= basepath+"lc_db.php?"
	var data  	= "HeaderArray="+headerArray;
	
	$.ajax({
		url:url,
		dataType:'json',
		type:'post',
		data:data,
		async:false,
		success:function(json){
			$('#frm_lc #butSave').validationEngine('showPrompt',json.msg,json.type);
			if(json.type=='pass')
			{			
				$('#frm_lc #txtSerialLCNo').val(json.serialNo);
				$('#frm_lc #txtSerialLCYear').val(json.serialYear);
				if(json.approvePermission=='1')
					$('#frm_lc #butConfirm').show();
				else
					$('#frm_lc #butConfirm').hide();
			}
			else
			{
				$('#frm_lc #butSave').validationEngine('showPrompt',json.msg,json.type);
			}
		},
		error:function(xhr,status)
		{
		}		
	});
}
function getOrderNo()
{
	var customer = $('#frm_lc #cboCustomer').val();
	if(customer=='')
	{
		$('#frm_lc #cboOrderYear').val('');
		$('#frm_lc #cboOrderNo').html('');
		return;
	}
	
	var url 	= basepath+"lc_db.php?RequestType=getOrderNo";
	var data 	= "customer="+customer;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frm_lc #cboOrderYear').val(json.customerYear);
				$('#frm_lc #cboOrderYear').change();
			}
	});		
	
}
function hideValidation(para)
{
	$(para).validationEngine('hide');
}

function viewReport()
{
	if($('#frm_lc #txtSerialLCNo').val()==""){
		alert('No saved details appear.');
		return;
	}
	var url = "?q=951&serialNo="+$('#frm_lc #txtSerialLCNo').val()+"&serialYear="+$('#frm_lc #txtSerialLCYear').val();
	window.open(url,'rpt_lc_.php');
}

function approve()
{
	if($('#frm_lc #txtSerialLCNo').val()==""){
		alert('No saved details appear.');
		return;
	}
	var url = "?q=951&serialNo="+$('#frm_lc #txtSerialLCNo').val()+"&serialYear="+$('#frm_lc #txtSerialLCYear').val()+"&approveMode=1&mode=Confirm";
	window.open(url,'rpt_lc_.php');
}

function cancel()
{
	if($('#frm_lc #txtSerialLCNo').val()==""){
		alert('No saved details appear.');
		return;
	}
	var url = "?q=951&serialNo="+$('#frm_lc #txtSerialLCNo').val()+"&serialYear="+$('#frm_lc #txtSerialLCYear').val()+"&approveMode=1&mode=Cancel";
	window.open(url,'rpt_lc_.php');
}
function clearForm()
{
	window.location = '?q=834';
}

<?php
$backwardseperator = "../../../../../";
include "{$backwardseperator}dataAccess/Connector.php";
include "../../../../../class/cls_commonFunctions_get.php";
include "../../../../../class/cls_commonErrorHandeling_get.php";
include "../../../../../class/customerAndOperation/bulk/LC/cls_lc_get.php";
include "../../../../../class/customerAndOperation/bulk/LC/cls_lc_set.php";

session_start();
$mainPath 						= $_SESSION['mainPath'];
$userId							= $_SESSION["userId"];
$companyId 						= $_SESSION['headCompanyId'];
$locationId 					= $_SESSION['CompanyID'];

$requestType					= $_REQUEST["RequestType"];
$programCode					= "P0834";
$haderArray						= json_decode($_REQUEST['HeaderArray'],true);
$obj_commonFunctions_get		= new cls_commonFunctions_get($db);
$obj_commonErrorHandeling_get 	= new cls_commonErrorHandeling_get($db);
$obj_lc_get						= new Cls_lc_get($db);
$obj_lc_set						= new Cls_lc_set($db);

if($haderArray['RequestType']=="URLSave")
{
	$db->begin();
	$savedStatus	= true;	
	
	$serialNo		= $haderArray["SerialLCNo"];
	$serialYear		= $haderArray["SerialLCYear"];
	
	if($serialNo=="")
	{
		$result	= $obj_commonFunctions_get->GetSystemMaxNo('CUSTMER_LC',$locationId);
		if($result["rollBackFlag"]=='0')
		{
			$serialNo	= $result["max_no"];
			$serialYear	= date('Y');
		}
		else
		{
			$savedStatus = false;
			$message	 = $result["msg"];
			$errorSql	 = $result["q"];
		}
		
	}
	
	$lcNo			= $obj_commonFunctions_get->replace($haderArray["LCNo"]);	
	$bankRefNo		= $obj_commonFunctions_get->replace($haderArray["BankRefNo"]);
	$customerId		= $haderArray["CustomerId"];
	$orderNo		= $haderArray["OrderNo"];
	$orderYear		= $haderArray["OrderYear"];
	$bankId			= $haderArray["BankID"];
	$currencyId		= $haderArray["CurrencyId"];
	$lcAmount		= $haderArray["LCAmount"];
	$lcDate			= $haderArray["LCDate"];
	$remarks		= $obj_commonFunctions_get->replace($haderArray["Remarks"]);
	$approveLevels  = $obj_commonFunctions_get->getApproveLevels_new($programCode); 
	$approveLevels	= $approveLevels['ApproLevel'];
	
	if($haderArray["SerialLCNo"]=="")
	{
		if($savedStatus)
			$result = $obj_lc_set->insertHeader($serialNo,$serialYear,$lcNo,$bankRefNo,$customerId,$orderNo,$orderYear,$bankId,$currencyId,$lcAmount,$lcDate,$remarks,$status,$approveLevels,$userId,'RunQuery2');
			if(!$result['type'])
			{
				$savedStatus	= false;
				$message	 	= $result["msg"];
				$errorSql	 	= $result["sql"];
			}
			else
				$message		= $result['msg'];
	}
	else
	{
		$header_arr 			= $obj_lc_get->get_header($serialNo,$serialYear,'RunQuery2');
		$result 				= $obj_commonErrorHandeling_get->get_permision_withApproval_save($header_arr["STATUS"],$header_arr["LEVELS"],$userId,$programCode,'RunQuery2');
		
		if($result['type']=='fail')
		{
			$savedStatus	= false;
			$message	 	= $result["msg"];
			$errorSql	 	= "";
		}
		
		if($savedStatus)
			$result = $obj_lc_set->updateHeader($serialNo,$serialYear,$lcNo,$bankRefNo,$customerId,$orderNo,$orderYear,$bankId,$currencyId,$lcAmount,$lcDate,$remarks,$userId,'RunQuery2');
			if(!$result['type'])
			{
				$savedStatus	= false;
				$message		= $result["msg"];
				$errorSql	 	= $result["sql"];
			}
			else
				$message		= $result['msg'];
	}
	
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= $message;
		$response['serialNo'] 	= $serialNo;
		$response['serialYear'] = $serialYear;
		$header_arr 			= $obj_lc_get->get_header($serialNo,$serialYear,'RunQuery');
		$response['approvePermission'] = $obj_commonFunctions_get->get_permision_confirm($header_arr["STATUS"],$header_arr["LEVELS"],$userId,$programCode);
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $message;
		$response['sql'] 		= $errorSql;
	}
	echo json_encode($response);
}
else if($requestType=="loadOrder")
{
	$orderYear	= $_REQUEST["orderYear"];
	$customer	= $_REQUEST["customer"];	
	
	$html		= "<option value=\"\"></option>";
	$result		= $obj_lc_get->getOrderNo($orderYear,$customer,$companyId);
	
	while($row = mysqli_fetch_array($result))
	{
		$html.="<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
	}
	$response['orderNoCombo']	= $html;
	echo json_encode($response);
}
else if($requestType=="getCustomer")
{
	$orderYear		= $_REQUEST["orderYear"];
	$orderNo		= $_REQUEST["orderNo"];
	
	$getCusId		= $obj_lc_get->getCustomerId($orderYear,$orderNo);
	
	$response['customerId']	= $getCusId;
	echo json_encode($response);
}
else if($requestType=="getOrderNo")
{
	$customer		= $_REQUEST["customer"];
	
	$getCusYear		= $obj_lc_get->getCustomerYear($customer,$companyId);
	
	$response['customerYear']	= $getCusYear;
	echo json_encode($response);
}
?>
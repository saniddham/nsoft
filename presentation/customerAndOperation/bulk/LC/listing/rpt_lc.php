<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId 			= $sessions->getCompanyId();
$locationId			= $sessions->getLocationId();
$intUser			= $sessions->getUserId();

require_once "class/cls_commonFunctions_get.php";
require_once "class/cls_commonErrorHandeling_get.php";
require_once "class/customerAndOperation/bulk/LC/cls_lc_get.php";
require_once "class/customerAndOperation/bulk/LC/cls_lc_set.php";

$obj_common			= new cls_commonFunctions_get($db);
$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_cls_lc_get		= new Cls_lc_get($db);
$obj_cls_lc_set		= new Cls_lc_set($db);

$programCode		= 'P0834';

$serialNo			= $_REQUEST["serialNo"];
$serialYear			= $_REQUEST["serialYear"];
$approveMode		= (!isset($_REQUEST["approveMode"])?'':$_REQUEST["approveMode"]);
$mode				= (!isset($_REQUEST["mode"])?'':$_REQUEST["mode"]);

$header_array 		= $obj_cls_lc_get->get_header($serialNo,$serialYear,'RunQuery');

$permition_arr		= $obj_commonErr->get_permision_withApproval_save($header_array['STATUS'],$header_array['LEVELS'],$intUser,$programCode,'RunQuery');
$permision_save		= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_cancel($header_array['STATUS'],$header_array['LEVELS'],$intUser,$programCode,'RunQuery');
$permision_cancel	= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_reject($header_array['STATUS'],$header_array['LEVELS'],$intUser,$programCode,'RunQuery');
$permision_reject	= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$intUser,$programCode,'RunQuery');
$permision_confirm	= $permition_arr['permision'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LC Report</title>
<script type="text/javascript" src="presentation/customerAndOperation/bulk/LC/listing/rpt_lc-js.js"></script>
 </head>
<body>

 <form id="frmRptLC" name="frmRptLC" method="post" action="rpr_lc_.php">
  <table width="900" align="center">
    <tr>
      <td><?php include 'reportHeader.php'?></td>
    </tr>
    <tr>
      <td class="reportHeader" align="center">Letter Of Credits</td>
    </tr>
	<?php
		include "presentation/report_approve_status_and_buttons.php"
     ?>
<tr>
      <td><table width="100%" border="0" cellspacing="2" cellpadding="2" class="normalfnt">
                    	  <tr>
                    	    <td width="15%">Serial No</td>
                    	    <td width="2%">:</td>
                    	    <td width="26%"><?php echo $serialYear.'/'.$serialNo; ?></td>
                    	    <td width="19%">Date</td>
                    	    <td width="2%">:</td>
                    	    <td width="36%"><?php  echo $header_array['LC_DATE']?></td>
                  	    </tr>
                    	  <tr>
                    	    <td>Order No</td>
                    	    <td>:</td>
                    	    <td><?php echo $header_array['ORDER_YEAR'].' / '.$header_array['ORDER_NO'];?>   
                  	        </td>
                    	    <td>Customer</td>
                    	    <td>:</td>
                    	    <td><?php echo $header_array['CUSTOMER']; ?></td>
                  	    </tr>
                    	  <tr>
                    	    <td>LC No</td>
                    	    <td>:</td>
                    	    <td><?php echo $header_array['LC_NO']; ?></td>
                    	    <td>LC Amount</td>
                    	    <td>:</td>
                    	    <td><?php echo $header_array['CURRENCY'].' : '. $header_array['LC_AMOUNT'];?>
                            </td>
                  	    </tr>
                    	  <tr>
                    	    <td rowspan="2" valign="top">Remark</td>
                    	    <td rowspan="2" valign="top">:</td>
                    	    <td rowspan="2" valign="top"><?php echo $header_array['REMARKS']; ?></td>
                    	    <td valign="top">Bank</td>
                    	    <td valign="top">:</td>
                    	    <td valign="top"><?php echo $header_array['BANK_NAME']?></td>
                  	    </tr>
                    	  <tr>
                    	    <td valign="top">Bank Ref. No</td>
                    	    <td valign="top">:</td>
                    	    <td valign="top"><?php echo $header_array['BANK_REF_NO']; ?></td>
                  	    </tr>
                    	  <tr>
                    	    <td valign="top">&nbsp;</td>
                    	    <td valign="top">&nbsp;</td>
                    	    <td valign="top">&nbsp;</td>
                    	    <td valign="top">&nbsp;</td>
                    	    <td valign="top">&nbsp;</td>
                    	    <td valign="top">&nbsp;</td>
                  	    </tr>
       	              </table></td>
    </tr>
    <tr>
    <td>
    <?php
			$creator		= $header_array['USER'];
			$createdDate	= $header_array['CREATED_DATE'];
 			$resultA 		= $obj_cls_lc_get->get_Report_approval_details_result($serialYear,$serialNo,'RunQuery');
			include "presentation/report_approvedBy_details.php"
 	?>
    </td>
    </tr>
    <tr>
    <td align="center" class="normalfntMid"><strong>Printed Date : </strong><?php echo date('Y-m-d H:i:s');?></td>
    </tr>
  </table>
</form>
</body>
</html>
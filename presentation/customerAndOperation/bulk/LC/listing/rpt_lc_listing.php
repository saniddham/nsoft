<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "libraries/jqgrid2/inc/jqgrid_dist.php";
require_once "class/cls_commonFunctions_get.php";
require_once "class/cls_commonErrorHandeling_get.php";
require_once "class/customerAndOperation/bulk/LC/cls_lc_get.php";

$obj_common			= new cls_commonFunctions_get($db);
$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_cls_lc_get		= new Cls_lc_get($db);

$company 			= $sessions->getCompanyId();
$location   		= $sessions->getLocationId();
$intUser  			= $sessions->getUserId();

$programCode		='P0834';

$approveLevel 		= $obj_common->get_approve_levels($programCode,'RunQuery');


						//echo $sql;
//BEGIN - ADD DEFAULT WHERE STRING WHEN FORM LOADING {
$arr =  json_decode($_REQUEST['filters'],true);

$arr = $arr['rules'];

$where_string = '';
$where_array = array(
					'Status'=>'tb1.STATUS',
					'LC_SERIAL_NO'=>"tb1.LC_SERIAL_NO",
					'LC_SERIAL_YEAR'=>"tb1.LC_SERIAL_YEAR",
					'ORDER_NO'=>"CONCAT(tb1.ORDER_NO,' / ',tb1.ORDER_YEAR)",
					'CUSTOMER'=>"mst_customer.strName",
					'CURRENCY'=>'mst_financecurrency.strCode',
					'LC_AMOUNT'=>'tb1.LC_AMOUNT',
					'USER'=>'sys_users.strUserName',
					'LC_DATE'=>'tb1.LC_DATE'
					);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Revised'=>'-1','Cancelled'=>'-2','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
	
if(!count($arr)>0)					 
		$where_string .= "AND tb1.LC_DATE = '".date('Y-m-d')."'";
//END }

$sql				= $obj_cls_lc_get->get_listing_sql($programCode,$approveLevel,$intUser,$where_string,'RunQuery');
		 
$col = array();

$formLink  				= "?q=834&serialNo={LC_SERIAL_NO}&serialYear={LC_SERIAL_YEAR}";
$reportLink  			= "?q=951&serialNo={LC_SERIAL_NO}&serialYear={LC_SERIAL_YEAR}";
$reportLinkApprove  	= "?q=951&serialNo={LC_SERIAL_NO}&serialYear={LC_SERIAL_YEAR}&approveMode=1&mode=Confirm";
$reportLinkCancel   	= "?q=951&serialNo={LC_SERIAL_NO}&serialYear={LC_SERIAL_YEAR}&approveMode=1&mode=Cancel";

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected;Cancelled:Cancelled" ;
$col["editoptions"] =  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;	$col=NULL;

 
//PO No
$col["title"] 	= "Serial No"; // caption of column
$col["name"] 	= "LC_SERIAL_NO"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= $formLink;
$col["linkoptions"] = "target='lc.php'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;



//PO Year
$col["title"] = "Serial Year"; // caption of column
$col["name"] = "LC_SERIAL_YEAR"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

$col["title"] 	= "Order No"; // caption of column
$col["name"] 	= "ORDER_NO"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
$col["align"] 	= "center";
$cols[] = $col;	$col=NULL;

//PO Year
$col["title"] = "Customer"; // caption of column
$col["name"] = "CUSTOMER"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//PO Year
$col["title"] = "Currency"; // caption of column
$col["name"] = "CURRENCY"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//PO Year
$col["title"] = "Amount"; // caption of column
$col["name"] = "LC_AMOUNT"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Delivery Date
$col["title"] = "User"; // caption of column
$col["name"] = "USER"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;


//Supplier
$col["title"] = "Date"; // caption of column
$col["name"] = "LC_DATE"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

 
//FIRST APPROVAL
$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}

//FIRST APPROVAL
$col["title"] = "Cancel"; // caption of column
$col["name"] = "CANCEL"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkCancel;
$col['linkName']	= 'Cancel';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "LC Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'LC_SERIAL_NO'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);
$out = $jq->render("list1");
?>

<head>
	<?php 
		include "include/listing.html";
	?>
</head>
<body>
<form id="frmlisting" name="frmlisting" method="post" action="">

        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
</form>
<?php
	session_start();
	//ini_set('display_errors',1);
	$backwardseperator 	= "../../../../../";
	$mainPath 			= $_SESSION['mainPath'];
	$userId 			= $_SESSION['userId'];
	$location 			= $_SESSION['CompanyID'];
	$company 			= $_SESSION['headCompanyId'];

	include_once  	"{$backwardseperator}dataAccess/Connector.php";
 	require_once "../../../../../class/cls_commonFunctions_get.php";
	require_once "../../../../../class/cls_commonErrorHandeling_get.php";
	require_once "../../../../../class/customerAndOperation/bulk/LC/cls_lc_get.php";
	require_once "../../../../../class/customerAndOperation/bulk/LC/cls_lc_set.php";
  	
	$obj_common			= new cls_commonFunctions_get($db);
	$obj_commonErr		= new cls_commonErrorHandeling_get($db);
	$obj_cls_lc_get		= new Cls_lc_get($db);
	$obj_cls_lc_set		= new Cls_lc_set($db);
  
	$requestType		= $_REQUEST['requestType'];
 	
	$programName		='LC';
	$programCode		='P0834';

 	//---------------------------	
	
if($requestType=='approve'){
	
		$serialNo				= $_REQUEST['serialNo'];
		$serialYear				= $_REQUEST['serialYear'];
	
		$db->begin();
		
		$rollBack_flag 			= 0;
		$rollBack_msg			= '';
		$rollBack_sql			= '';
	
		$header_array			= $obj_cls_lc_get->get_header($serialNo,$serialYear,'RunQuery2');
		$permition_arr			= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$userId,$programCode,'RunQuery2');
		$permision_confirm		= $permition_arr['permision']; 
		
		//VALIDATION--------------------------------------
		//1. check for approval permision
 		if($permition_arr['type']  != 'pass'){
			$rollBack_flag		=	1;
			$rollBack_msg	 	=	$permition_arr['msg'];	
			$rollBack_sql		=	'';
 		}
		//2. check for order status
 		else if($header_array['ORDER_STATUS']  != 1){
			$rollBack_flag		=	1;
			$rollBack_msg	 	=	"Order Not Approved. Can't raise LC";	
			$rollBack_sql		=	'';
 		}
		
		//SAVING--------------------------------------
		if($rollBack_flag != 1){
		//3. update header status
			$status				= $header_array['STATUS']-1;
			$response_1			= $obj_cls_lc_set->update_header_status($serialYear,$serialNo,$status,'RunQuery2');
			if($response_1['type']	 != 'pass'){
				$rollBack_flag	=1;
				$rollBack_msg	=$response_1['msg'];
				$rollBack_sql	=$response_1['sql'];
			}
		}
		/*if($rollBack_flag != 1){
		//3. update header status
		
			$maxAppByStatus		= (int)$obj_cls_lc_get->get_max_approved_by_status($serialYear,$serialNo,'RunQuery2');
			$response_1			= $obj_cls_lc_set->approved_by_update($serialYear,$serialNo,$maxAppByStatus,'RunQuery2');
			if($response_1['type']	 != 'pass'){
				$rollBack_flag	=1;
				$rollBack_msg	=$response_1['msg'];
				$rollBack_sql	=$response_1['sql'];
			}
		}*/
		if($rollBack_flag != 1){
		//4. approve by insert
			if($rollBack_flag != 1){
				$header_array	= $obj_cls_lc_get->get_header($serialNo,$serialYear,'RunQuery2');
				$levels			= $header_array['LEVELS']+1-$header_array['STATUS'];
				$response_2		= $obj_cls_lc_set->approved_by_insert($serialYear,$serialNo,$userId,$levels,'RunQuery2');
				if($response_2['type']	 != 'pass'){
					$rollBack_flag		=1;
					$rollBack_msg		=$response_2['msg'];
					$rollBack_sql		=$response_2['sql'];
				}
			}
		}
   
		$header_array	= $obj_cls_lc_get->get_header($serialNo,$serialYear,'RunQuery2');
	
	
		//$rollBack_flag = 1;

		if($rollBack_flag==1){
			$db->rollback();
			$response['type'] 				= 'fail';
			$response['msg'] 				= $rollBack_msg;
			$response['q'] 					= $rollBack_sql;
		}
		else if($header_array['STATUS'] ==1 ){
			$db->commit();
			$response['type'] 				= 'pass';
			$response['msg'] 				= 'Final Approval Raised successfully.';
		}
		else if($header_array['STATUS'] > 1  && $header_array['STATUS'] <= $header_array['LEVELS']){
			$db->commit();
			$response['type'] 				= 'pass';
			$response['msg'] 				= 'Approval '.($header_array['LEVELS']+2-$header_array['STATUS']).' raised successfully.';
		}
		else{
			$db->rollback();
			$response['type'] 				= 'fail';
			$response['msg'] 				= $db->errormsg;
			$response['q'] 					= $sql;
		}
	
		$db->CloseConnection();		
		echo json_encode($response);
	
	}
	else if($requestType=='reject'){
		
		$rollBack_flag 			= 0;
		$rollBack_msg			= '';
		$rollBack_sql			= '';
		
		$serialNo				= $_REQUEST['serialNo'];
		$serialYear				= $_REQUEST['serialYear'];
		
		$db->begin();
	
		$header_array			= $obj_cls_lc_get->get_header($serialNo,$serialYear,'RunQuery2');
		$permition_arr			= $obj_commonErr->get_permision_withApproval_reject($header_array['STATUS'],$header_array['LEVELS'],$userId,$programCode,'RunQuery2');
		$permision_confirm		= $permition_arr['permision']; 
		//--------------------------
		//$permition_arr['type'];
		if($permition_arr['type']  != 'pass'){
			$rollBack_flag		=	1;
			$rollBack_msg	 	=	$permition_arr['msg'];	
			$rollBack_sql		=	'';
 		}
		
		if($rollBack_flag != 1){
		//1. update header status
			$status				=0;
			$response_1			= $obj_cls_lc_set->update_header_status($serialYear,$serialNo,$status,'RunQuery2');
			if($response_1['type']	 != 'pass'){
				$rollBack_flag	=1;
				$rollBack_msg	=$response_1['msg'];
				$rollBack_sql	=$response_1['sql'];
			}
		}
			
		if($rollBack_flag != 1){
		//2. approve by insert
			if($rollBack_flag != 1){
				$response_2		= $obj_cls_lc_set->approved_by_insert($serialYear,$serialNo,$userId,0,'RunQuery2');
				if($response_2['type']	 != 'pass'){
					$rollBack_flag		=1;
					$rollBack_msg		.=$response_2['msg'];
					$rollBack_sql		.=$response_2['sql'];
				}
			}
		}
		
		if($rollBack_flag==1){
			$db->rollback();
			$response['type'] 				= 'fail';
			$response['msg'] 				= $rollBack_msg;
			$response['q'] 					= $rollBack_sql;
		}
		else if($toSave==$saved){
			$header_array					= $obj_cls_lc_get->get_header($serialNo,$serialYear,'RunQuery2');
			$db->commit();
			$response['type'] 				= 'pass';
			if($header_array['STATUS']==0)
			$response['msg'] 				= 'Rejected successfully.';
			else
			$response['msg'] 				= 'Rejection Failed.';
		}
		else{
			$db->rollback();
			$response['type'] 				= 'fail';
			$response['msg'] 				= $db->errormsg;
			$response['q'] 					= $sql;
		}
	
		$db->CloseConnection();		
		echo json_encode($response);
	
	}
	else if($requestType=='cancel'){
		
		$rollBack_flag 			= 0;
		$rollBack_msg			= '';
		$rollBack_sql			= '';
		
		$serialNo				= $_REQUEST['serialNo'];
		$serialYear				= $_REQUEST['serialYear'];
		
		$db->begin();
	
		$header_array			= $obj_cls_lc_get->get_header($serialNo,$serialYear,'RunQuery2');
		$permition_arr			= $obj_commonErr->get_permision_withApproval_cancel($header_array['STATUS'],$header_array['LEVELS'],$userId,$programCode,'RunQuery2');
		$permision_confirm		= $permition_arr['permision']; 
		//--------------------------
		//$permition_arr['type'];
		if($permition_arr['type']  != 'pass'){
			$rollBack_flag		=	1;
			$rollBack_msg	 	=	$permition_arr['msg'];	
			$rollBack_sql		=	'';
 		}
		
		if($rollBack_flag != 1){
		//1. update header status
			$status				=-2;
			$response_1			= $obj_cls_lc_set->update_header_status($serialYear,$serialNo,$status,'RunQuery2');
			if($response_1['type']	 != 'pass'){
				$rollBack_flag	=1;
				$rollBack_msg	=$response_1['msg'];
				$rollBack_sql	=$response_1['sql'];
			}
		}
			
		if($rollBack_flag != 1){
		//2. approve by insert
			if($rollBack_flag != 1){
				$response_2		= $obj_cls_lc_set->approved_by_insert($serialYear,$serialNo,$userId,-2,'RunQuery2');
				if($response_2['type']	 != 'pass'){
					$rollBack_flag		=1;
					$rollBack_msg		.=$response_2['msg'];
					$rollBack_sql		.=$response_2['sql'];
				}
			}
		}
		
		if($rollBack_flag==1){
			$db->rollback();
			$response['type'] 				= 'fail';
			$response['msg'] 				= $rollBack_msg;
			$response['q'] 					= $rollBack_sql;
		}
		else if($toSave==$saved){
			$header_array					= $obj_cls_lc_get->get_header($serialNo,$serialYear,'RunQuery2');
			$db->commit();
			$response['type'] 				= 'pass';
			if($header_array['STATUS']==-2)
			$response['msg'] 				= 'Cancelled successfully.';
			else
			$response['msg'] 				= 'Cancellation Failed.';
		}
		else{
			$db->rollback();
			$response['type'] 				= 'fail';
			$response['msg'] 				= $db->errormsg;
			$response['q'] 					= $sql;
		}
	
		$db->CloseConnection();		
		echo json_encode($response);
	
	}

 ?>
<?php
	session_start();
	$backwardseperator 		= "../../../../";
	$mainPath 				= $_SESSION['mainPath'];
	$sessionUserId 			= $_SESSION['userId'];
	$thisFilePath 			=  $_SERVER['PHP_SELF'];
	$locationId				= $_SESSION["CompanyID"];

	include  	"{$backwardseperator}dataAccess/Connector.php";
	
	$orderNo  		= $_REQUEST['orderNo'];
	$orderYear		= $_REQUEST['orderYear'];
	$salesOrderId		= $_REQUEST['salesOrderId'];
	$qty = $_REQUEST['qty'];
	
	$sql="SELECT
			trn_orderdetails.intSampleNo,
			trn_orderdetails.intSampleYear,
			trn_orderdetails.strCombo,
			trn_orderdetails.strPrintName,
			trn_orderdetails.intRevisionNo
			FROM `trn_orderdetails`
			WHERE
			trn_orderdetails.intOrderNo = '$orderNo' AND
			trn_orderdetails.intOrderYear = '$orderYear' ";
			$sql.=" AND
			trn_orderdetails.intSalesOrderId = '$salesOrderId'
			";
			 //echo $sql;
		  $result = $db->RunQuery($sql);
	//	 echo $db->errormsg;
		 while($row=mysqli_fetch_array($result))
		 {
	
		$sampleNo  		= $row['intSampleNo'];
		$sampleYear		= $row['intSampleYear'];
		$revNo			= $row['intRevisionNo'];
		$combo			= $row['strCombo'];
		$printName		= $row['strPrintName'];
	}
	
	$colorId		= $_REQUEST['colorId'];
	$techniqueId	= $_REQUEST['techniqueId'];
	$inkTypeId		= $_REQUEST['inkTypeId'];
    $recipeRevision = $_REQUEST['recipeRev'];
    $stockBal		= $_REQUEST['balanceQty'];
    $sumQty         = $_REQUEST['totalWeight'];
    $inkId        =  trim($_REQUEST['inkId']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="css.css" rel="stylesheet" type="text/css" />
</head>

<body onload="keyBoard();">
<table bgcolor="#999" width="1010" height="500" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td colspan="2">&nbsp;</td>
    <td hidden colspan="2"><input id="sampleNo" value="<?php echo $sampleNo; ?>"></input></td>
    <td hidden colspan="2"><input id="sampleYear" value="<?php echo $sampleYear; ?>"></input></td>
    <td hidden colspan="2"><input id="combo" value="<?php echo $combo; ?>"></input></td>
    <td hidden colspan="2"><input id="print" value="<?php echo $printName; ?>"></input></td>
    <td hidden colspan="2"><input id="revision" value="<?php echo $revNo; ?>"></input></td>
    <td hidden colspan="2"><input id="bulkRevision" value="<?php echo $recipeRevision; ?>"></input></td>
    <td hidden colspan="2"><input id="inkTypeId" value="<?php echo $inkTypeId; ?>"></input></td>
    <td hidden colspan="2"><input id="techniqueTypeId" value="<?php echo $techniqueId; ?>"></input></td>
    <td hidden colspan="2"><input id="colorId" value="<?php echo $colorId; ?>"></input></td>
    <td hidden colspan="2"><input id="inkId" value="<?php echo $inkId; ?>"></input></td>
 </tr>
    <tr>

        <td width="25%" align="right">Ink Create Qty : </td>
        <td width="25%"><input type="text" style="height:50px;margin-top:5px; width:95px; text-align:center" class="hex txtQty" id="totalInkQty" aria-haspopup="true" role="textbox" value="<?php echo $qty ?>" /></td>
    </tr>
  <tr>
   <form id="frmInkCreation" name="frmInkCreation" method="post" action="" autocomplete="off">
    <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="3"><div style="width:100%;color:#FFF"><div id="msg" align="center" style="width:100%;height:30px;color:#3F6"></div>
          <table id="tblPopUpGrid" width="100%" border="0" cellspacing="0" cellpadding="0" class="tableBorder_allRound">
          <thead>
            <tr>
              <td width="2%" height="32">&nbsp;</td>
              <td width="41%" align="center" bgcolor="#006666"><strong>ITEM</strong></td>
              <td align="center" bgcolor="#006666"><strong>Propotion</strong></td>
              <td align="center" bgcolor="#006666"><strong>Expected consumption(g)</strong></td>
              <td align="center" bgcolor="#006666"><strong>Stock Bal</strong></td>
              <td width="2%"></td>
            </tr>
            </thead>
            <tbody>
            	<?PHp



					if ($recipeRevision > 0){
                        $sql = "SELECT
							trn_sample_color_recipes_bulk.intItem,
							mst_item.strName,
							trn_sample_color_recipes_bulk.dblWeight
							FROM
							trn_sample_color_recipes_bulk
							Inner Join mst_item ON mst_item.intId = trn_sample_color_recipes_bulk.intItem
							WHERE
							trn_sample_color_recipes_bulk.intSampleNo =  '$sampleNo' AND
							trn_sample_color_recipes_bulk.intSampleYear =  '$sampleYear' AND
							trn_sample_color_recipes_bulk.intRevisionNo =  '$revNo' AND
							trn_sample_color_recipes_bulk.strCombo =  '$combo' AND
							trn_sample_color_recipes_bulk.strPrintName =  '$printName' AND
							trn_sample_color_recipes_bulk.intColorId =  '$colorId' AND
							trn_sample_color_recipes_bulk.intTechniqueId =  '$techniqueId' AND
							trn_sample_color_recipes_bulk.intInkTypeId =  '$inkTypeId' AND 
							trn_sample_color_recipes_bulk.bulkRevNumber =  '$recipeRevision'
							AND mst_item.intStatus	=	1
							";

                    } else {
                        $sql = "SELECT
							trn_sample_color_recipes.intItem,
							mst_item.strName,
							trn_sample_color_recipes.dblWeight,
							trn_sample_color_recipes.intBulkItem
							FROM
							trn_sample_color_recipes
							Inner Join mst_item ON mst_item.intId = trn_sample_color_recipes.intItem
							WHERE
							trn_sample_color_recipes.intSampleNo =  '$sampleNo' AND
							trn_sample_color_recipes.intSampleYear =  '$sampleYear' AND
							trn_sample_color_recipes.intRevisionNo =  '$revNo' AND
							trn_sample_color_recipes.strCombo =  '$combo' AND
							trn_sample_color_recipes.strPrintName =  '$printName' AND
							trn_sample_color_recipes.intColorId =  '$colorId' AND
							trn_sample_color_recipes.intTechniqueId =  '$techniqueId' AND
							trn_sample_color_recipes.intInkTypeId =  '$inkTypeId'
							AND mst_item.intStatus	=	1
							";

                    }

					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{ 
					
					$stockBal=colorRoom_stockBal($locationId,$row['intItem']);
					$allocatedQty=getAllocatedItemQty($orderNo,$orderYear,$salesOrderId,$colorId,$techniqueId,$inkTypeId,$row['intItem']);
					?>
                    	<tr id="<?php echo $row['intItem']; ?>"><td>&nbsp;</td>
                          <td style="color:black" bgcolor="#CCCCCC" align="center"><?php echo $row['strName']; ?></td>
                    	  <td width="8%" bgcolor="#CCCCCC"><input disabled="disabled" value="<?php echo round($row['dblWeight']/$sumQty,8); ?>" style="width:100px;height:50px; text-align:right" class="" type="text" name="itemPropotion" id="itemPropotion" /></td>
                    	  <td width="7%" bgcolor="#CCCCCC"><input disabled="disabled" value="<?php echo round($row['dblWeight']/$sumQty,8)*$qty; ?>" style="width:100px;height:50px; text-align:right" class="itemQty" type="text" name="itemQty" id="itemQty" /></td>
                    	  <td width="6%" bgcolor="#CCCCCC"><input disabled="disabled" value="<?php echo $stockBal; ?>" style="width:100px;height:50px; text-align:right" class="txtQty6" type="text" name="itemBalance" id="itemBalance" /></td>
                          <td></td>
                        </tr>
                    <?php	
					}
				?>
            </tbody>
            </table>
        </div></td>
          <td colspan="5" align="center">
              <div id="msg_parent" align="center" style="width:100%;height:30px;color:#3F6"></div>
          </td>
      </tr>
      <tr height="20"></tr>
      <tr>
          <?php
          $reviseUrl ="../../sample/colorRecipes/touchScreenRecipeRevision/index.php?";
          $reviseUrl.="sampleYear=".$sampleYear."&sampleNo=".$sampleNo;
          ?>
        
          <td colspan="3" align="center">
              <a href="<?php echo $reviseUrl ?>" target="_blank"><div style="height:28px" align="right" id="butRevise" class="button orange" >Revise</div></a>
              <div style="height:28px" align="right" id="butClose_popup" class="button pink" >CLOSE</div>
              <div style="height:28px" align="right" id="butCreateInk" class="button green" >CREATE INK</div>
              <div style="height:28px" align="right" id="btnIssueInkPopUp" class="button green" >ISSUE INK</div>
              <div style="height:28px" align="right" id="btnReturnInkPopUp" class="button green" >RETURN INK</div>
          </td>
        
      </tr>

    </table></td>
   </form>
  </tr>
</table>
</body>
</html>
<?php
	//--------------------------------------------------------------
	function colorRoom_stockBal($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(IFNULL(ware_sub_stocktransactions_bulk.dblQty,0)) AS stockBal
				FROM ware_sub_stocktransactions_bulk  
				INNER JOIN mst_substores ON ware_sub_stocktransactions_bulk.intSubStores = mst_substores.intId
				WHERE
				ware_sub_stocktransactions_bulk.intItemId =  '$item' AND
				ware_sub_stocktransactions_bulk.intLocationId =  '$location' AND 
				mst_substores.intColorRoom = '1' 
				GROUP BY
				ware_sub_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getAllocatedItemQty($orderNo,$orderYear,$salesOrderId,$colorId,$techniqueId,$inkTypeId,$itemId){
		
		global $db;
		$sql="SELECT
			Sum(IFNULL(ware_sub_stocktransactions_bulk.dblQty*-1,0)) as qty 
			FROM
			ware_sub_stocktransactions_bulk
			WHERE
			ware_sub_stocktransactions_bulk.intOrderNo = '$orderNo' AND
			ware_sub_stocktransactions_bulk.intOrderYear = '$orderYear' AND
			ware_sub_stocktransactions_bulk.intSalesOrderId = '$salesOrderId' AND
			ware_sub_stocktransactions_bulk.intColorId = '$colorId' AND
			ware_sub_stocktransactions_bulk.intTechnique = '$techniqueId' AND
			ware_sub_stocktransactions_bulk.intInkType = '$inkTypeId' AND
			ware_sub_stocktransactions_bulk.intItemId = '$itemId' ";
			
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['qty']);	
		
	}

?>
<?php
	session_start();
	$backwardseperator 		= "../../../../";
	$mainPath 				= $_SESSION['mainPath'];
	$sessionUserId 			= $_SESSION['userId'];
	$thisFilePath 			=  $_SERVER['PHP_SELF'];
	$locationId				= $_SESSION["CompanyID"];

	include  	"{$backwardseperator}dataAccess/Connector.php";
	
	$sampleNo  		= $_REQUEST['sampleNo'];
	$sampleYear		= $_REQUEST['sampleYear'];
	$revNo			= $_REQUEST['revNo'];
	$combo			= $_REQUEST['combo'];
	$printName		= $_REQUEST['printName'];
	
	$colorId		= $_REQUEST['colorId'];
	$techniqueId	= $_REQUEST['techniqueId'];
	$inkTypeId		= $_REQUEST['inkTypeId'];


	/////////////////////////
	$sql ="SELECT
trn_sampleinfomations.dtDate,
trn_sampleinfomations.strGraphicRefNo,
mst_customer.strName AS customer,
trn_sampleinfomations.strStyleNo,
mst_brand.strName AS brand,
trn_sampleinfomations.verivide_d65,
trn_sampleinfomations.verivide_tl84,
trn_sampleinfomations.verivide_cw,
trn_sampleinfomations.verivide_f,
trn_sampleinfomations.verivide_uv,
trn_sampleinfomations.macbeth_dl,
trn_sampleinfomations.macbeth_cw,
trn_sampleinfomations.macbeth_inca,
trn_sampleinfomations.macbeth_tl84,
trn_sampleinfomations.macbeth_uv,
trn_sampleinfomations.macbeth_horizon,
trn_sampleinfomations.dblCuringCondition_temp,
trn_sampleinfomations.dblCuringCondition_beltSpeed,
trn_sampleinfomations.dblPressCondition_temp,
trn_sampleinfomations.dblPressCondition_pressure,
trn_sampleinfomations.dblPressCondition_time,
trn_sampleinfomations.strMeshCount,
trn_sampleinfomations.strAdditionalInstructions,
trn_sampleinfomations.strAdditionalInstructionsTech
FROM
trn_sampleinfomations
Inner Join mst_customer ON mst_customer.intId = trn_sampleinfomations.intCustomer
Inner Join mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
WHERE
trn_sampleinfomations.intSampleNo 	=  '$sampleNo' AND
trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
trn_sampleinfomations.intRevisionNo =  '$revNo'
";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="css.css" rel="stylesheet" type="text/css" />
<style type="text/css">
.yello2{
	color:#FF0;
}
</style>
</head>

<body>
<table bgcolor="#000000" width="1010" height="709" border="0" cellspacing="0" cellpadding="0">

  <tr>
   	<td style="vertical-align:top"><table  align="center" style="vertical-align:top;color:white" width="100%" border="0" cellspacing="0" cellpadding="0">
   	  <tr>
   	    <td>&nbsp;</td>
   	    <td>&nbsp;</td>
   	    <td >&nbsp;</td>
   	    <td>&nbsp;</td>
   	    <td>&nbsp;</td>
   	    <td>&nbsp;</td>
   	    <td>&nbsp;</td>
 	    </tr>
   	  <tr>
   	    <td>&nbsp;</td>
   	    <td>Graphic No</td>
   	    <td >:</td>
   	    <td class="yello2"><?php echo $row['strGraphicRefNo']; ?></td>
   	    <td>Create Date</td>
   	    <td>:</td>
   	    <td class="yello2"><?php echo $row['dtDate']; ?></td>
 	    </tr>
   	  <tr>
   	    <td>&nbsp;</td>
   	    <td>Style No</td>
   	    <td >:</td>
   	    <td class="yello2"><?php echo $row['strStyleNo']; ?></td>
   	    <td>Customer</td>
   	    <td>:</td>
   	    <td class="yello2"><?php echo $row['customer']; ?></td>
 	    </tr>
   	  <tr>
   	    <td>&nbsp;</td>
   	    <td>&nbsp;</td>
   	    <td >&nbsp;</td>
   	    <td>&nbsp;</td>
   	    <td>Brand</td>
   	    <td>:</td>
   	    <td class="yello2"><?php echo $row['brand']; ?></td>
 	    </tr>
   	  <tr>
   	    <td>&nbsp;</td>
   	    <td>&nbsp;</td>
   	    <td >&nbsp;</td>
   	    <td>&nbsp;</td>
   	    <td>&nbsp;</td>
   	    <td>&nbsp;</td>
   	    <td>&nbsp;</td>
 	    </tr>
   	  <tr>
   	    <td colspan="7"><table width="762" border="1" align="center"  cellspacing="0" cellpadding="0">
   	      <tr>
   	        <td colspan="2" align="center" class="normalfnt" style="color:"><strong>Technical Data</strong></td>
 	        </tr>
   	      <tr>
   	        <td width="129" height="47"><span class="normalfnt">Curing Condition</span></td>
   	        <td width="621"><table width="94%" height="38" border="0" cellpadding="0" cellspacing="0" >
   	          <tr>
   	            <td width="20%" class="normalfntMid"><span class="normalfnt"><strong>TEMP</strong></span></td>
   	            <td width="35%" class="normalfntMid"><span class="yello2"><?php echo $row['dblCuringCondition_temp']; ?></span></td>
   	            <td width="23%" class="normalfntMid"><span class="normalfnt"><strong>BELT SPEED </strong></span></td>
   	            <td width="22%" class="normalfntMid"><span class="yello2"><?php echo $row['dblCuringCondition_beltSpeed']; ?></span></td>
 	            </tr>
 	          </table></td>
 	        </tr>
   	      <tr>
   	        <td height="49"><span class="normalfnt">Press Condition</span></td>
   	        <td><table width="93%" height="38" border="0" cellpadding="0" cellspacing="0" >
   	          <tr class="normalfntMid">
   	            <td width="17%"><span class="normalfnt"><strong>TEMP</strong></span></td>
   	            <td width="20%"><span class="yello2"><?php echo $row['dblPressCondition_temp']; ?></span></td>
   	            <td width="15%"><span class="normalfnt"><strong>Pressure </strong></span></td>
   	            <td width="19%"><span class="yello2"><?php echo $row['dblPressCondition_pressure']; ?></span></td>
   	            <td width="13%"><span class="normalfnt"><strong>Time </strong></span></td>
   	            <td width="16%"><span class="yello2"><?php echo $row['dblPressCondition_time']; ?></span></td>
 	            </tr>
 	          </table></td>
 	        </tr>
   	      <tr>
   	        <td><span class="normalfnt">Mesh Count</span></td>
   	        <td><span class="yello2"><?php echo $row['strMeshCount']; ?></span></td>
 	        </tr>
   	      </table></td>
   	    </tr>
   	  <tr>
   	    <td>&nbsp;</td>
   	    <td>&nbsp;</td>
   	    <td >&nbsp;</td>
   	    <td>&nbsp;</td>
   	    <td>&nbsp;</td>
   	    <td>&nbsp;</td>
   	    <td>&nbsp;</td>
 	    </tr>
   	  <tr>
   	    <td colspan="7"><table width="100%" border="1" cellspacing="0" cellpadding="0">
   	      <tr>
   	        <td colspan="11" align="center" style="color:">Light Box</td>
   	        </tr>
   	      <tr>
   	        <td colspan="5" align="center">Verivide</td>
   	        <td colspan="6" align="center">Macbeth</td>
   	        </tr>
   	      <tr>
   	        <td width="6%" align="center">D65</td>
   	        <td width="4%" align="center">TL84</td>
   	        <td width="9%" align="center">CW</td>
   	        <td width="9%" align="center">F</td>
   	        <td width="9%" align="center">UV</td>
   	        <td width="9%" align="center">DL</td>
   	        <td width="9%" align="center">CW</td>
   	        <td width="9%" align="center">Inca</td>
   	        <td width="9%" align="center">TL84</td>
   	        <td width="9%" align="center">UV</td>
   	        <td width="11%" align="center">Horizon</td>
   	        </tr>
   	      <tr>
   	        <td align="center"><span class="yello2"><?php echo ($row['verivide_d65']==1?'*':''); ?></span></td>
   	        <td align="center"><span class="yello2"><?php echo ($row['verivide_tl84']==1?'*':''); ?></span></td>
   	        <td align="center"><span class="yello2"><?php echo ($row['verivide_cw']==1?'*':''); ?></span></td>
   	        <td align="center"><span class="yello2"><?php echo ($row['verivide_f']==1?'*':''); ?></span></td>
   	        <td align="center"><span class="yello2"><?php echo ($row['verivide_uv']==1?'*':''); ?></span></td>
   	        <td align="center"><span class="yello2"><?php echo ($row['macbeth_dl']==1?'*':''); ?></span></td>
   	        <td align="center"><span class="yello2"><?php echo ($row['macbeth_cw']==1?'*':''); ?></span></td>
   	        <td align="center"><span class="yello2"><?php echo ($row['macbeth_inca']==1?'*':''); ?></span></td>
   	        <td align="center"><span class="yello2"><?php echo ($row['macbeth_tl84']==1?'*':''); ?></span></td>
   	        <td align="center"><span class="yello2"><?php echo ($row['macbeth_uv']==1?'*':''); ?></span></td>
   	        <td align="center"><span class="yello2"><?php echo ($row['macbeth_horizon']==1?'*':''); ?></span></td>
   	        </tr>
   	      </table></td>
 	    </tr>
   	  <tr>
   	    <td width="9%">&nbsp;</td>
   	    <td width="13%">&nbsp;</td>
   	    <td width="2%" >&nbsp;</td>
   	    <td width="23%">&nbsp;</td>
   	    <td width="11%">&nbsp;</td>
   	    <td width="3%">&nbsp;</td>
   	    <td width="39%">&nbsp;</td>
 	    </tr>
   	  <tr>
   	    <td>&nbsp;</td>
   	    <td class="green">Marketing Comments</td>
   	    <td colspan="5" style="color:#FFF;"><textarea style="width:600px" name="textarea" id="textarea" cols="100" rows="6"><?php echo $row['strAdditionalInstructions']; ?></textarea></td>
 	    </tr>
   	  <tr>
   	    <td>&nbsp;</td>
   	    <td class="">&nbsp;</td>
   	    <td colspan="5">&nbsp;</td>
   	    </tr>
   	  <tr>
   	    <td>&nbsp;</td>
   	    <td class="green">Technical Comments</td>
   	    <td colspan="5"><span style="color:#FFF;">
   	      <textarea name="textarea2" id="textarea2" style="width:600px" cols="100" rows="6"><?php echo $row['strAdditionalInstructionsTech']; ?></textarea>
   	      </span></td>
 	    </tr>
   	  <tr>
   	    <td>&nbsp;</td>
   	    <td>&nbsp;</td>
   	    <td>&nbsp;</td>
   	    <td>&nbsp;</td>
   	    <td>&nbsp;</td>
   	    <td>&nbsp;</td>
   	    <td>&nbsp;</td>
 	    </tr>
   	  <tr>
   	    <td colspan="7" align="center">&nbsp;</td>
 	    </tr>
   	  <tr>
   	    <td colspan="7" align="center"><div style="height:28px" align="right" id="butClose_popup" class="button pink" >CLOSE</div>&nbsp;</td>
 	    </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
<?php
	session_start();
 	include "../../../../dataAccess/Connector.php";
	$requestType = $_REQUEST['requestType'];
	$userId		 = $_SESSION["userId"];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	
	
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	if($requestType=='saveDetails')
	{
		$toSave=0;
		$saved=0;
		$arrRecipe 		= json_decode($_REQUEST['arrRecipe'], true);
		
			$orderNo 		= $arrRecipe[0]['orderNo'];
			$orderYear 	= $arrRecipe[0]['orderYear'];
			$salesOrderId 	= $arrRecipe[0]['salesOrderId'];
			$revNo 			= $arrRecipe[0]['revisionNo'];
			$combo 			= $arrRecipe[0]['combo'];
			$printName 		= $arrRecipe[0]['printName'];
			$weight 		= $arrRecipe[0]['weight'];
			
			$color 			= $arrRecipe[0]['color'];
			$techId 		= $arrRecipe[0]['techId'];
			$inkTypeId 		= $arrRecipe[0]['inkTypeId'];
			
			$item 		= $arrRecipe[0]['itemId'];
			
			//-------------------------------------
			$sqlM="SELECT
			(IFNULL(max(ware_sub_color_stocktransactions_bulk.intSavedOrder),0)+1)as saveNo 
			FROM `ware_sub_color_stocktransactions_bulk`
			WHERE
			ware_sub_color_stocktransactions_bulk.intOrderNo = '$orderNo' AND
			ware_sub_color_stocktransactions_bulk.intOrderYear = '$orderYear'	";
			$resultM = $db->RunQuery2($sqlM);
			$rowM=mysqli_fetch_array($resultM);
			$saveNo=$rowM['saveNo'];
			//-------------------------------------
		
		$SavedQty=0;
		$toSave=0;
		$tot=0;
		foreach($arrRecipe as $x)
		{
			$itemId 		= $x['itemId'];
			$Qty 		= $x['weight'];
			$tot +=$Qty;

			//-----------------
		$resultG = getGrnWiseColorRoom_stockBal($location,$itemId);
		while($rowG=mysqli_fetch_array($resultG)){
				$subStores=$rowG['intSubStores'];
				if(($Qty>0) && ($rowG['stockBal']>0)){
					if($Qty<=$rowG['stockBal']){
					$saveQty=$Qty;
					$Qty=0;
					}
					else if($Qty>$rowG['stockBal']){
					$saveQty=$rowG['stockBal'];
					$Qty=$Qty-$saveQty;
					}
					$grnNo=$rowG['intGRNNo'];
					$grnYear=$rowG['intGRNYear'];
					$grnDate=$rowG['dtGRNDate'];
					$grnRate=$rowG['dblGRNRate'];	
					$currency=loadCurrency($grnNo,$grnYear,$item);
					$saveQty=round($saveQty,4);		
					if($saveQty>0){	
					
					$toSave++;
					
					$sqlI = "INSERT INTO `ware_sub_stocktransactions_bulk` (intSavedOrder,`intCompanyId`,`intLocationId`,`intSubStores`,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,`intOrderNo`,`intOrderYear`,`intSalesOrderId`,`intColorId`,`intTechnique`,`intInkType`,intItemId,dblQty,strType,intUser,dtDate) 
						VALUES ('$saveNo','$company','$location','$subStores','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$orderNo','$orderYear','$salesOrderId','".$x['color']."','".$x['techId']."','".$x['inkTypeId']."','$itemId','-$saveQty','INK_ALLOC','$userId',now())";
					$resultI = $db->RunQuery2($sqlI);
					if($resultI){
						$saved+=$resultI;
						$SavedQty+=$saveQty;
					}
					else{
						$rollBackFlag=1;
						$sqlM=$sqlI;
					}
					if(((!$resultI) && ($rollBackFlag!=1))  ){
						$rollBackFlag=1;
						$sqlM=$sqlI;
						$rollBackMsg = "Saving error!";
					}
					
					}
				}
		}
			//-----------------
			//$saved+=$result;
		}
		$toSave++;
		$sql = "INSERT INTO `ware_sub_color_stocktransactions_bulk` 
		(`intSavedOrder`,`intCompanyId`,`intLocationId`,`intSubStores`,`intColorId`,`intTechnique`,`intInkType`,`dblQty`,`strType`,
			`intOrderNo`,`intOrderYear`,`intSalesOrderId`,`intUser`,`dtDate`) 
		VALUES ('".$saveNo."','".$company."','".$location."','".$subStores."','".$x['color']."','".$x['techId']."','".$x['inkTypeId']."','".$SavedQty."','INK_ALLOC','".$orderNo."','".$orderYear."','".$salesOrderId."','".$_SESSION["userId"]."',now())";
		$result 	= $db->RunQuery2($sql);
		if($result){
			$saved+=$result;
		}
		else{
			$sqlM=$sql; 
		}
		
		if(round($SavedQty,4)!=round($tot,4)){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= 'No item stock balance to allocate'.$SavedQty.'--'.$tot;
			$response['q'] 			= '';
		}
		else if(($toSave==$saved) && ($SavedQty>0) &&($rollBackFlag!=1)){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM."/".$toSave."==".$saved;
		}
			
	}
	else if($requestType=="saveItem")
	{
		$rollBackFlag	= 0;
		$itemId 		= $_REQUEST['id'];
		$sampleNo 		= trim($_REQUEST['sampleNo']);
		$sampleYear 	= trim($_REQUEST['sampleYear']);
		$revNo 			= trim($_REQUEST['revNo']);
		$combo 			= trim($_REQUEST['combo']);
		$printName 		= trim($_REQUEST['printName']);
		$colorId 		= $_REQUEST['colorId'];
		$techniqueId 	= $_REQUEST['techniqueId'];
		$inkTypeId 		= $_REQUEST['inkTypeId'];
		
		$sql = "SELECT * FROM trn_sample_color_recipes
				WHERE intSampleNo='$sampleNo' AND intSampleYear='$sampleYear' AND
				intRevisionNo='$revNo' AND strCombo='$combo' AND strPrintName='$printName' AND 
				intColorId='$colorId' AND intTechniqueId='$techniqueId' AND 
				intInkTypeId='$inkTypeId' AND intItem='$itemId' ";
		$result = $db->RunQuery2($sql);
		if(mysqli_num_rows($result)>0)
		{
			$rollBackFlag	= 1;
			$rollBackMsg	= "selected Item already exist.";
		}
		else
		{
			$sqlIns = "INSERT INTO trn_sample_color_recipes 
						(
						intSampleNo, intSampleYear, intRevisionNo, 
						strCombo, strPrintName, intColorId, 
						intTechniqueId, intInkTypeId, intItem, 
						intBulkItem, dblWeight, intUser, 
						dtDate
						)
						VALUES
						(
						'$sampleNo', '$sampleYear', '$revNo', 
						'$combo', '$printName', '$colorId', 
						'$techniqueId', '$inkTypeId', '$itemId', 
						1, 0, '$userId', 
						now()
						); ";
			$resultIns = $db->RunQuery2($sqlIns);
			if(!$resultIns)
			{
				$rollBackFlag	= 1;
				$rollBackMsg	= $db->errormsg;
			}
		}
		if($rollBackFlag==1)
		{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
		}
		if($rollBackFlag==0)
		{
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
		}
	}
	else if($requestType=='RemoveItem')
	{
		$rollBackFlag	= 0;
		$itemId 		= $_REQUEST['id'];
		$sampleNo 		= trim($_REQUEST['sampleNo']);
		$sampleYear 	= trim($_REQUEST['sampleYear']);
		$revNo 			= trim($_REQUEST['revNo']);
		$combo 			= trim($_REQUEST['combo']);
		$printName 		= trim($_REQUEST['printName']);
		$colorId 		= $_REQUEST['colorId'];
		$techniqueId 	= $_REQUEST['techniqueId'];
		$inkTypeId 		= $_REQUEST['inkTypeId'];
		
		$sql = "DELETE FROM trn_sample_color_recipes 
				WHERE
				intSampleNo = '$sampleNo' AND 
				intSampleYear = '$sampleYear' AND 
				intRevisionNo = '$revNo' AND 
				strCombo = '$combo' AND 
				strPrintName = '$printName' AND 
				intColorId = '$colorId' AND 
				intTechniqueId = '$techniqueId' AND 
				intInkTypeId = '$inkTypeId' AND 
				intItem = '$itemId' ;";
		$result = $db->RunQuery2($sql);
		if(!$result)
		{
			$rollBackFlag	= 1;
			$rollBackMsg	= $db->errormsg;
		}
		if($rollBackFlag==1)
		{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
		}
		if($rollBackFlag==0)
		{
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['q'] 			= $sql;
		}
	}
    else if($requestType=='createInk')
    {
        $rollBackFlag	= 0;
        $orderNo 		= trim($_REQUEST['orderNo']);
        $orderYear 		= trim($_REQUEST['orderYear']);
        $soId 		    = trim($_REQUEST['soId']);
        $sampleNo 		= trim($_REQUEST['sampleNo']);
        $sampleYear 	= trim($_REQUEST['sampleYear']);
        $revNo 			= trim($_REQUEST['revNo']);
        $combo 			= trim($_REQUEST['combo']);
        $printName 		= trim($_REQUEST['printName']);
        $graphic		= trim($_REQUEST['graphic']);
        $bulkRev 		= trim($_REQUEST['bulkRev']);
        $colorId 		= trim($_REQUEST['colorId']);
        $techniqueId 	= trim($_REQUEST['techniqueId']);
        $inkTypeId 		= trim($_REQUEST['inkTypeId']);
        $quantity 		= trim($_REQUEST['inkQty']);
        $inkId 		    = trim($_REQUEST['inkId']);
        $items 		    = json_decode($_REQUEST['items'], true);
        $recipeStatus = getBulkRecipeStatus($sampleNo, $sampleYear, $revNo, $printName, $combo, $bulkRev);
        if ($recipeStatus['status'] == 0){
            $rollBackFlag = 1;
            $rollBackMsg = $recipeStatus['msg'];
        } else {
            if ($inkId > 0){
                $sql = "INSERT INTO bulk_ink_quantity (`inkId`,`orderNo`,`orderYear`,`salesOrderId`,`sampleNo`,`sampleYear`,`revNo`,`print`,`combo`,`recipeRevision`,`quantity`,`strType`,`createdBy`,`locationId`,`createdDate`) 
                    VALUES ('$inkId','$orderNo','$orderYear','$soId','$sampleNo','$sampleYear','$revNo','$printName','$combo','$bulkRev','$quantity','CREATE','$userId','$location', now())";
                $result = $db->RunQuery2($sql);
                if (!$result){
                }
            } else {
                $inkCode = getMaxInkCode();
                $sql = "INSERT INTO bulk_colorroom_inks (`createdBy`,`inkCode`,`initiallyCreatedGraphic`,`colorId`,`inkTypeId`,`techniqueId`,`createdDate`) 
                    VALUES ('$userId','$inkCode','$graphic','$colorId','$inkTypeId','$techniqueId', now())";
                $result = $db->RunQuery2($sql);
                $inkId = $db->insertId;
                if (!$result){
                    var_dump($sql);
                }

                $sql = "INSERT INTO bulk_ink_quantity (`inkId`,`orderNo`,`orderYear`,`salesOrderId`,`sampleNo`,`sampleYear`,`revNo`,`print`,`combo`,`recipeRevision`,`quantity`,`strType`,`createdBy`,`locationId`,`createdDate`) 
                    VALUES ('$inkId','$orderNo','$orderYear','$soId','$sampleNo','$sampleYear','$revNo','$printName','$combo','$bulkRev','$quantity','CREATE','$userId','$location', now())";
                $result = $db->RunQuery2($sql);
                if (!$result){
                    var_dump($sql);
                }

                $sql = "INSERT INTO bulk_ink_item_composition values ";
                foreach ($items as $itemToInsert){
                    $itemId = $itemToInsert['itemId'];
                    $propotion = $itemToInsert['propotion'];
                    $sql.="('$inkId','$itemId','$propotion'),";
                }
                $sql = substr($sql, 0, -1);
                $result = $db->RunQuery2($sql);
                if (!$result){
                    var_dump($sql);
                }
            }

            if(!$result)
            {
                $rollBackFlag	= 1;
                $rollBackMsg	= $db->errormsg;
            }
        }

        if($rollBackFlag==1)
        {
            $db->RunQuery2('Rollback');
            $response['type'] 		= 'fail';
            $response['msg'] 		= $rollBackMsg;
        }
        if($rollBackFlag==0)
        {
            $db->RunQuery2('Commit');
            $response['type'] 	= 'pass';
            $response['msg']    =  'Successfully Created '.$quantity.' of Ink';
        }
    }
    else if($requestType=='issueInk')
    {
        $rollBackFlag	= 0;
        $orderNo 		= trim($_REQUEST['orderNo']);
        $orderYear 		= trim($_REQUEST['orderYear']);
        $soId 		    = trim($_REQUEST['soId']);
        $sampleNo 		= trim($_REQUEST['sampleNo']);
        $sampleYear 	= trim($_REQUEST['sampleYear']);
        $revNo 			= trim($_REQUEST['revNo']);
        $combo 			= trim($_REQUEST['combo']);
        $printName 		= trim($_REQUEST['printName']);
        $graphic		= trim($_REQUEST['graphic']);
        $bulkRev 		= trim($_REQUEST['bulkRev']);
        $colorId 		= trim($_REQUEST['colorId']);
        $techniqueId 	= trim($_REQUEST['techniqueId']);
        $inkTypeId 		= trim($_REQUEST['inkTypeId']);
        $quantity 		= trim($_REQUEST['inkQty']);
        $quantity       = $quantity*(-1);
        $inkId          = trim($_REQUEST['inkId']);


        $inkStockStatus = getInkStockBalanceStatus($inkId,$quantity*(-1),$location);

        if ($inkStockStatus['status'] == 0){
            $rollBackFlag = 1;
            $rollBackMsg = $inkStockStatus['msg'];
        } else {
            $sql = "INSERT INTO bulk_ink_quantity (`inkId`,`orderNo`,`orderYear`,`salesOrderId`,`sampleNo`,`sampleYear`,`revNo`,`print`,`combo`,`recipeRevision`,`quantity`,`strType`,`createdBy`,`locationId`,`createdDate`) 
                VALUES ('$inkId','$orderNo','$orderYear','$soId','$sampleNo','$sampleYear','$revNo','$printName','$combo','$bulkRev','$quantity','ISSUE','$userId','$location', now())";
            $result = $db->RunQuery2($sql);

            if(!$result)
            {
                $rollBackFlag	= 1;
                $rollBackMsg	= $db->errormsg;
            }
        }

        if($rollBackFlag==1)
        {
            $db->RunQuery2('Rollback');
            $response['type'] 		= 'fail';
            $response['msg'] 		= $rollBackMsg;
        }
        if($rollBackFlag==0)
        {
            $db->RunQuery2('Commit');
            $response['type'] 		= 'pass';
            $response['msg']        = "Issued Ink Successfully";
            $response['q'] 			= $sql;
        }
    }
    else if($requestType=='returnInk')
    {
        $rollBackFlag	= 0;
        $orderNo 		= trim($_REQUEST['orderNo']);
        $orderYear 		= trim($_REQUEST['orderYear']);
        $soId 		    = trim($_REQUEST['soId']);
        $sampleNo 		= trim($_REQUEST['sampleNo']);
        $sampleYear 	= trim($_REQUEST['sampleYear']);
        $revNo 			= trim($_REQUEST['revNo']);
        $combo 			= trim($_REQUEST['combo']);
        $printName 		= trim($_REQUEST['printName']);
        $graphic		= trim($_REQUEST['graphic']);
        $bulkRev 		= trim($_REQUEST['bulkRev']);
        $colorId 		= trim($_REQUEST['colorId']);
        $techniqueId 	= trim($_REQUEST['techniqueId']);
        $inkTypeId 		= trim($_REQUEST['inkTypeId']);
        $returnGoodQty	= trim($_REQUEST['goodInkQty']);
        $returnWasteQty	= trim($_REQUEST['wasteInkQty']);
        $inkId          = trim($_REQUEST['inkId']);


        $inkReturnStatus = getInkStockReturnStatus($inkId, $location, $orderNo, $orderYear, $returnGoodQty+$returnWasteQty);
        if ($inkReturnStatus['status'] == 0){
            $rollBackFlag = 1;
            $rollBackMsg = $inkReturnStatus['msg'];
        } else {
            $sql = "INSERT INTO bulk_ink_quantity (`inkId`,`orderNo`,`orderYear`,`salesOrderId`,`sampleNo`,`sampleYear`,`revNo`,`print`,`combo`,`recipeRevision`,`quantity`,`strType`,`createdBy`,`locationId`,`createdDate`) 
                VALUES ('$inkId','$orderNo','$orderYear','$soId','$sampleNo','$sampleYear','$revNo','$printName','$combo','$bulkRev','$returnGoodQty','REUSABLE','$userId','$location', now())";
            $result = $db->RunQuery2($sql);

            if(!$result)
            {
                $rollBackFlag	= 1;
                $rollBackMsg	= $db->errormsg;
            }
        }

        if($rollBackFlag==1)
        {
            $db->RunQuery2('Rollback');
            $response['type'] 		= 'fail';
            $response['msg'] 		= $rollBackMsg;
        }
        if($rollBackFlag==0)
        {
            $db->RunQuery2('Commit');
            $response['type'] 		= 'pass';
            $response['msg']        = "Issued Ink Successfully";
            $response['q'] 			= $sql;
        }
    }
	$db->CloseConnection();		
	echo json_encode($response);

	//--------------------------------------------------------------
	function getGrnWiseColorRoom_stockBal($location,$item)
	{
		global $db;
	       $sql = "SELECT
			Sum(ware_sub_stocktransactions_bulk.dblQty) AS stockBal,
			ware_sub_stocktransactions_bulk.intSubStores,
			ware_sub_stocktransactions_bulk.intGRNNo,
			ware_sub_stocktransactions_bulk.intGRNYear,
			ware_sub_stocktransactions_bulk.dtGRNDate,
			ware_sub_stocktransactions_bulk.dblGRNRate
			FROM ware_sub_stocktransactions_bulk 
			INNER JOIN mst_substores ON ware_sub_stocktransactions_bulk.intSubStores = mst_substores.intId
			WHERE
			ware_sub_stocktransactions_bulk.intItemId =  '$item' AND
			ware_sub_stocktransactions_bulk.intLocationId =  '$location' AND 
			mst_substores.intColorRoom = '1' 
			GROUP BY
			ware_sub_stocktransactions_bulk.intItemId,
			ware_sub_stocktransactions_bulk.intGRNNo,
			ware_sub_stocktransactions_bulk.intGRNYear, 
			ware_sub_stocktransactions_bulk.dblGRNRate 
			ORDER BY
			ware_sub_stocktransactions_bulk.dtGRNDate ASC";

	$result = $db->RunQuery2($sql);
	return $result;	
	}

function getBulkRecipeStatus($sampleNo, $sampleYear, $revNum, $print, $combo, $bulkRecipe)
{
    global $db;
    $sql = "SELECT STATUS FROM trn_sample_color_recipes_bulk_header WHERE
			intSampleNo =  '$sampleNo' AND
			intSampleYear =  '$sampleYear' AND strCombo='$combo' AND 
			intRevisionNo = '$revNum'  AND strPrintName='$print' AND recipeRevision='$bulkRecipe'
			";

    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $response['status'] = 1;
    $response['msg'] = "Bulk Revision is Approve";
    if ($row['STATUS'] !=1){
        $response['status'] = 0;
        $response['msg'] = "Latest Bulk Revision Is Not In Approved Status";
    }
    return $response;
}

function getInkStockBalanceStatus($inkId, $requestedQty , $locationId)
{
    global $db;
    $sql = "SELECT SUM(quantity) as balanceQty FROM bulk_ink_quantity WHERE `inkId` ='$inkId' AND `locationId` = '$locationId'";
    $result = $db->RunQuery2($sql);
    $row=mysqli_fetch_array($result);
    $balancedQty = $row['balanceQty'];
    $response['status'] = 1;
    if ($balancedQty < $requestedQty){
        $response['status'] = 0;
        $response['msg'] = "Issuing Ink Qty Exceeds Balanced Qty";
    }
    return $response;
}


function getInkStockReturnStatus($inkId , $locationId, $orderNo, $orderYear, $returnedQty)
{
    global $db;
    $sql = "SELECT SUM(quantity) as reuturnable FROM bulk_ink_quantity WHERE `inkId` ='$inkId' AND `locationId` = '$locationId' AND 
            `orderNo`='$orderNo' AND `orderYear`='$orderYear' AND  `strType` IN  ('ISSUE','REUSABLE')";
    $result = $db->RunQuery2($sql);
    $row=mysqli_fetch_array($result);
    $issuedQty = $row['reuturnable'];
    $issuedQty = $issuedQty*(-1);
    $response['status'] = 1;
    if ($returnedQty > $issuedQty){
        $response['status'] = 0;
        $response['msg'] = "Total Retunable qty must be less than issued qty";
    }
    return $response;
}

function getMaxInkCode()
{
    global $db;
    $sql = "SELECT MAX(inkId) as maxId FROM bulk_colorroom_inks";

    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $maxCode = $row['maxId'];
    if ($maxCode == NULL){
        $maxCode =0;
    }
    $nextCode = "INK00".$maxCode;
    return $nextCode;
}
function loadCurrency($grnNo,$grnYear,$item)
{
	global $db;
	
	if(($grnNo!=0) && ($grnYear!=0)){
	  $sql = "SELECT
			trn_poheader.intCurrency as currency 
			FROM
			ware_grnheader
			Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
			WHERE
			ware_grnheader.intGrnNo =  '$grnNo' AND
			ware_grnheader.intGrnYear =  '$grnYear'";
	}
	else{
	$sql = "SELECT
			mst_item.intCurrency as currency 
			FROM mst_item
			WHERE
			mst_item.intId =  '$item'";
	}

	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);	
	return $row['currency'];;	
}
	//--------------------------------------------------------------

 
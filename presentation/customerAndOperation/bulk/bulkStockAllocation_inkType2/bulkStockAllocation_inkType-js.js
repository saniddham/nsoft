// JavaScript Document
var colorId;
var techniqueId;
var inkTypeId;

$(document).ready(function() {
	
$('.technique').change();

$('.search').click(function(){
	var qty = $('#txtQty').val();
	if(qty<=0){
		alert("Please enter Fabric Qty");
		return false;
	}
});

$('#butSearch').live('click',searchDetails);	
$('#cboStyleNo').change(function(){
		
	loadSampleNos();
	
	var intSampleNo = $('#cboSampleNo').val();
	//$('#frmSampleInfomations').get(0).reset();
	$('#cboSampleNo').val(intSampleNo);
	$('#divPicture').html('');
	return;
});
$('#cboSampleNo').change(function(){
		
		loadRevisionNo();
		
		var intSampleNo = $('#cboSampleNo').val();
		//$('#frmSampleInfomations').get(0).reset();
		$('#cboSampleNo').val(intSampleNo);
		$('#divPicture').html('');
		return;
});

$('#cboRevisionNo').change(function(){
	loadCombo();
		
});
$('#cboCombo').change(function(){
	loadPrint();
		
});

		//----------------------------
		$('.costPerInch').keyup(function(){
			calTotCostPrice(this);
		});
		//----------------------------
		$('.usage').keyup(function(){
			calTotCostPrice(this);
		});
		//----------------------------
		$('.technique').change(function(){
			loadItem(this);
		});
		//----------------------------
		$('.foil').change(function(){
			techniqueCalculations(this);
		});
		//----------------------------
		$('.printWidth').keyup(function(){
			techniqueCalculations(this);
		});
		//----------------------------
		$('.printHeight').keyup(function(){
			techniqueCalculations(this);
		});
		//----------------------------
		$('.qty').keyup(function(){
			techniqueCalculations(this);
		});
		//----------------------------
		$('.delImg').click(function(){
			$(this).parent().parent().remove();
		});
		//----------------------------	

function loadRevisionNo()
{
	var url = "bulkStockAllocation_inkType-db-get.php?requestType=loadRevisionNo&sampleNo="+$('#cboSampleNo').val()+"&sampleYear="+$('#cboYear').val();
	var obj = $.ajax({url:url,async:false});	
	$('#cboRevisionNo').html(obj.responseText);
	$('#cboCombo').html('');	
	$('#cboPrint').html('');	
}
function loadSampleNos()
{
	var url = "bulkStockAllocation_inkType-db-get.php?requestType=loadSampleNo&styleNo="+URLEncode($('#cboStyleNo').val())+"&sampleYear="+$('#cboYear').val();
	var obj = $.ajax({url:url,async:false});	
	$('#cboSampleNo').html(obj.responseText);	
	$('#cboRevisionNo').html('');	
	$('#cboCombo').html('');	
	$('#cboPrint').html('');	
	
}
function loadCombo()
{
	var url = "bulkStockAllocation_inkType-db-get.php?requestType=loadCombo&sampleNo="+($('.txtOrderNo').val())+"&sampleYear="+$('#cboOrderYear').val()+"&revNo="+$('#cboRevisionNo').val();
	var obj = $.ajax({url:url,async:false});	
	$('#cboCombo').html(obj.responseText);	
	$('#cboPrint').html('');
}
function loadPrint()
{
	var url = "bulkStockAllocation_inkType-db-get.php?requestType=loadPrint&sampleNo="+($('.txtOrderNo').val())+"&sampleYear="+$('#cboOrderYear').val()+"&revNo="+$('#cboRevisionNo').val()+"&combo="+$('#cboCombo').val();
	var obj = $.ajax({url:url,async:false});	
	$('#cboPrint').html(obj.responseText);	
}
	
	
/*data load part*/
$('#cboSalesOrderNo').live('change',function(){
	if($('#txtQty').val()<=0){
		$('#cboSalesOrderNo').val('');
		alert("Please enter fabric Qty");
		return false;
	}
	else{
	window.location.href = "index.php?orderNo="+$('.txtOrderNo').val()		
						+'&orderYear='+$('#cboOrderYear').val()
						+'&salesOrderId='+$('#cboSalesOrderNo').val()	
						+'&qty='+$('#txtQty').val()	
						/*+'&revNo='+$('#cboRevisionNo').val()	
						+'&combo='+$('#cboCombo').val()	
						+'&printName='+$('#cboPrint').val()	*/
	}
});

$('#butAddRow').live('click',function(){
	$(this).parent().parent().before('<tr>'+$(this).parent().parent().parent().find('tr:eq(0)').html()+'</tr>');
});
	
//----------------------------
	
//if ($('#frmSampleInfomations').validationEngine('validate'))   

$('#butSave2').live('click',function(){
	var arrRecipe = '';
	var arrFoil = '';
	var arrSpecial = '';
	var data='';
	var i =0;
	
	var orderNo 		= 	$('.txtOrderNo').val();
	var orderYear 		= 	$('#cboOrderYear').val();
	var salesOrderId = $('#txtSalesOrderID').text();
	var revisionNo 		= 	$('#txtRevNo').text();
	var combo 			=	$('#txtCombo').text();
	var printName 		=	$('#txtPrintName').text();
	
	$('#tblPopUpGrid >tbody>tr').each(function(){
		var itemId 		= $(this).attr('id');
		
		if(itemId!='')
		{
			alert($(this).parent().parent().parent().parent().parent().parent().parent().parent().find('td:eq(0)').attr('id'));
			var colorId 	= $(this).parent().parent().parent().parent().parent().parent().find('td:eq(1)').attr('id');
			var techniqueId = $(this).parent().parent().parent().parent().parent().parent().find('td:eq(2)').attr('id');
			var inkTypeId 	= $(this).parent().parent().parent().parent().parent().parent().find('td:eq(3)').attr('id');
			var weight 		= $(this).parent().parent().find('#txtWeight').val();

			arrRecipe +=(i++ == 0?'':',' )+'{"color":"'+colorId+'","techId":"'+techniqueId+'","inkTypeId":"'+inkTypeId+'","weight":"'+weight+'","orderNo":"'+orderNo+'","orderYear":"'+orderYear+'","salesOrderId":"'+salesOrderId+'","revisionNo":"'+revisionNo+'","combo":"'+combo+'","printName":"'+printName+'","itemId":"'+itemId+'"}';
		}
	});
	//-----------------
	
	//alert(arrRecipe);
		// SAVE AJAX PART //
		data  = 'arrRecipe=['+ arrRecipe +']';
		
		var url 	= "bulkStockAllocation_inkType-db-set.php?requestType=saveDetails";
		$.ajax({
			type: "POST",
			url:url,
			data:data,
			async:false,
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmColorRecipies #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertx()",1000);
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmColorRecipies #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
   });
	
	$('#butNew').click(function(){
		window.location.href = 'bulkStockAllocation_inkType.php';
	});

});

//----------------------------------------------------------------------
function calTotCostPrice(obj)
{
		var printWidth = document.getElementById('txtPrintWidth').value;
		var printHeight = document.getElementById('txtPrintHeight').value;
	 	var costPerInch = $(obj).parent().parent().find('.costPerInch').val();
	 	var usage = $(obj).parent().parent().find('.usage').val();
	//	var totCost=qty*costPerInch*usage/100;
		var totCost=printWidth*printHeight*costPerInch*usage/100;
		//alert(totCost);
		$(obj).parent().parent().find('.totCost').text(totCost);
	
}
//----------------------------------------------------------------------
function loadItem(obj)
{
	 	var technique = $(obj).parent().parent().find('.technique').val();
	 	var sampleNo =$('#cboSampleNo').val();
	 	var sampleYear = $('#cboYear').val();
	 	var revNo = $('#cboRevisionNo').val();
	 	var combo = $('#cboCombo').val();
	 	var printName = $('#cboPrint').val();
		var url 		= "bulkStockAllocation_inkType-db-get.php?requestType=loadItem";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"technique="+technique+"&sampleNo="+sampleNo+"&revNo="+revNo+"&combo="+combo+"&sampleYear="+sampleYear+"&printName="+printName,
			async:false,
			success:function(json){
				var itemId=json.itemId;
				 $(obj).parent().parent().find('.foil').val(json.itemId)
				 //	if(itemId){	
					  $(obj).parent().parent().find('.foil').change();
				//	}
			}
		});
}
//----------------------------------------------------------------------
function techniqueCalculations(obj)
{
	 	var foil = $(obj).parent().parent().find('.foil').val();
		var url 		= "bulkStockAllocation_inkType-db-get.php?requestType=loadMeasurements";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"foil="+foil,
			async:false,
			success:function(json){
				foilWidth=json.width;
				foilHeight=json.height;
				foilcostPerMtr=json.price;
			}
		});
	
		var printWidth = $(obj).parent().parent().find('.printWidth').val();
		var printHeight = $(obj).parent().parent().find('.printHeight').val();
	 	var qty = $(obj).parent().parent().find('.qty').val();
		
		//------------------------------
		var pcs=Math.floor((foilHeight/printWidth))
		if(printWidth==0){
			pcs=0;
		}
		//alert(printWidth);
		$(obj).parent().parent().find('.pcsFrmHeight').text((foilHeight/printWidth).toFixed(4));
		$(obj).parent().parent().find('.pcs').text(pcs);
		var mtrs=((printHeight/pcs)*qty)/39.5;
		if(pcs==0){
			mtrs=0;
		}
		$(obj).parent().parent().find('.meters').text(mtrs.toFixed(4));
		$(obj).parent().parent().find('.cost').text((mtrs*foilcostPerMtr).toFixed(4));
		
		//-------------------
		var pcs=Math.floor((foilHeight/printHeight));
		if(printWidth==0){
			pcs=0;
		}
		$(obj).parent().parent().next().find('.pcsFrmHeight').text((foilHeight/printHeight).toFixed(4));
		$(obj).parent().parent().next().find('.pcs').text(pcs);
		var mtrs=((printWidth/pcs)*qty)/39.5;
		if(pcs==0){
			mtrs=0;
		}
		$(obj).parent().parent().next().find('.meters').text(mtrs.toFixed(4));
		$(obj).parent().parent().next().find('.cost').text((mtrs*foilcostPerMtr).toFixed(4));
}
//----------------------------------------------------------------------
function alertx()
{
	$('#frmColorRecipies #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmColorRecipies #butDelete').validationEngine('hide')	;
}

function searchDetails()
{
	var orderYear 	= $('#cboOrderYear').val();
	var orderNo 	= $('#num').val();
	document.location.href = 'index.php?orderYear='+orderYear+'&orderNo='+orderNo+'&qty='+$('#txtQty').val();	

}
//----------------------------------------------

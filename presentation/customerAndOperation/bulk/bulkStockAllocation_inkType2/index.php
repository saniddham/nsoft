<?php
ini_set('display_errors',0);
	session_start();
	//print_r($_SESSION);
	
	$backwardseperator 		= "../../../../";
	$mainPath 				= $_SESSION['mainPath'];
	$sessionUserId 			= $_SESSION['userId'];
	$thisFilePath 			=  $_SERVER['PHP_SELF'];
	$locationId				= $_SESSION["CompanyID"];

	include  	"{$backwardseperator}dataAccess/permisionCheck_touch2.inc";
	
	
	$orderNo  		= $_REQUEST['orderNo'];
	$orderYear		= $_REQUEST['orderYear'];
	$salesOrderId		= $_REQUEST['salesOrderId'];
	$qty		= $_REQUEST['qty'];
	
	$sql = "SELECT
		trn_orderdetails.strSalesOrderNo,
		mst_part.strName,
		trn_orderdetails.intSalesOrderId,
		trn_orderdetails.strGraphicNo
		FROM
		trn_orderdetails
		INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
		WHERE
		trn_orderdetails.intOrderNo = '$orderNo' AND
		trn_orderdetails.intOrderYear = '$orderYear' AND 
		trn_orderdetails.intSalesOrderId = '$salesOrderId' 
	";
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	$SalesOrderNo =$row['strSalesOrderNo']."/".$row['strName'];
	$graphic = $row['strGraphicNo'];
	
	$sql="SELECT
			trn_orderdetails.intSampleNo,
			trn_orderdetails.intSampleYear,
			trn_orderdetails.strCombo,
			trn_orderdetails.strPrintName,
			trn_orderdetails.intRevisionNo,
			sum(trn_orderdetails.intQty) as orderQty 
			FROM `trn_orderdetails`
			WHERE
			trn_orderdetails.intOrderNo = '$orderNo' AND
			trn_orderdetails.intOrderYear = '$orderYear' ";
			if($salesOrderId!=''){
			$sql.=" AND
			trn_orderdetails.intSalesOrderId = '$salesOrderId'
			";
			}
			//echo $sql;
		  $result = $db->RunQuery($sql);
	//	 echo $db->errormsg;
		 while($row=mysqli_fetch_array($result))
		 {
		$salesOrderQty = $row['orderQty'];	
	
		$sampleNo  		= $row['intSampleNo'];
		$sampleYear		= $row['intSampleYear'];
		$revNo			= $row['intRevisionNo'];
		$combo			= $row['strCombo'];
		$printName		= $row['strPrintName'];
	}
	
	
	$mode = 'first';
	if(isset($sampleNo) && isset($sampleYear) && isset($revNo) && isset($combo) && isset($printName) && isset($salesOrderId))
	{
		$mode = 'last';
	}
	else if(isset($orderNo) && isset($orderYear))
	{
		$mode = "second";	
	}
	
	//echo  $mode;
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Color Room Item Allocations</title>

	<link href="css.css" rel="stylesheet" />
	<!-- jQuery & jQuery UI + theme (required) -->
	<link href="other/jquery-ui.css" rel="stylesheet">
	<script src="other/jquery.js"></script>
	<script src="other/jquery-ui.min.js"></script>

	<!-- keyboard widget css & script (required) -->
	<link href="css/keyboard.css" rel="stylesheet">
	<script src="js/jquery.keyboard.js"></script>

	<!-- keyboard extensions (optional) -->
	<script src="js/jquery.mousewheel.js"></script>
	<script src="js/jquery.keyboard.extension-typing.js"></script>
	<script src="js/jquery.keyboard.extension-autocomplete.js"></script>

	<!-- demo only -->
	<link href="demo/demo.css" rel="stylesheet">
	<script src="demo/demo.js"></script>
	<script src="other/jquery.jatt.min.js"></script> <!-- tooltips -->

	<!-- theme switcher -->
	<script src="other/themeswitchertool.htm"></script>

	<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
    <script type="application/javascript" src="index.js"></script>
    <script type="application/javascript" src="js/jquery.validationEngine.js"></script>
    <script type="application/javascript" src="js/jquery.validationEngine-en.js"></script>
    <script>
		$(function(){
			$('#keyboard').keyboard();
		});
		var projectName = '/<?php echo $_SESSION["projectName"]; ?>' ;
	</script>
  
    <script type="application/javascript" src="bulkStockAllocation_inkType-js.js"></script>
</head>

<body style="background-color:#483D8B">
<table width="1024" height="199" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="135" height="48" class="label" >&nbsp;ORDER YEAR</td>
      <td width="149"><select name="cboOrderYear" class="down"   id="cboOrderYear" >
      	<?php
				    $d = date('Y');
					if($orderYear=='')
						$orderYear = $d;
				  	for($d;$d>=2012;$d--)
					{
						if($d==$orderYear)
							echo "<option selected=\"selected\" id=\"$d\" >$d</option>";	
						else
							echo "<option id=\"$d\" >$d</option>";	
					}
		?>
      </select></td>
      <td width="108" class="label">ORDER NO</td>
      <td width="118"><input type="text" style="height:50px;margin-top:5px; width:95px" class="hex txtOrderNo" id="num" aria-haspopup="true" role="textbox" value="<?php echo $orderNo; ?>" /></td>
      <td width="48" class="label">QTY</td>
      <td width="108"><input type="text" style="height:50px;margin-top:5px; width:95px; text-align:right" class="hex txtQty" id="txtQty" aria-haspopup="true" role="textbox" value="<?php echo $qty; ?>" /></td>
      <td width="110">&nbsp;<a id="butSearch" class="orange button search" style="height:35px;margin-top:5px">Search</a></td>
      <td>&nbsp;<a id="butSapmle" href="../../sample/colorRecipes/touchScreen/index.php" class="white button" style="height:35px;margin-top:5px">SAMPLE</a></td>
      <td align="right"><a id="butSearch" href="logout.php" class="pink button logout" style="height:35px;margin-top:5px; width:65px">LOG OUT</a></td> 
      <td width="1"></td>
    </tr>
    
    <?php 
			
$sql = "SELECT
		 DISTINCT 
		trn_orderheader.intOrderNo,
		trn_orderheader.intOrderYear
		FROM
		trn_orderheader
		Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
		WHERE
		trn_orderdetails.intSampleNo =  '$sampleNo' AND
		trn_orderdetails.intSampleYear =  '$sampleYear' AND 
		trn_orderheader.intApproveLevelStart-trn_orderheader.intStatus>=2
				";
$string='';				
				$result = $db->RunQuery($sql);
				$i=0;
				while($row=mysqli_fetch_array($result))
				{
					$i++;
					$string .=$row['intOrderNo']."/".$row['intOrderYear']."&nbsp;";
					
				}?>
                
    <?php 
		if($i>0)
		{
			?>
    
    <?Php
		}
	?>
    
    <?php
		if($mode=='second')
		{
	?>
    <tr>
      <td height="17" colspan="10"><table style="margin-top:150px" align="center" width="150" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center">Sales Order No</td>
        </tr>
        <tr style="margin-top:250px">
          <td><select name="cboSalesOrderNo" size="1" multiple="multiple" id="cboSalesOrderNo" style="height:200px;width:300px">
            <?php
				    $sql = "SELECT
						trn_orderdetails.strSalesOrderNo,
						mst_part.strName,
						trn_orderdetails.intSalesOrderId
						FROM
						trn_orderdetails
						INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
						WHERE
						trn_orderdetails.intOrderNo = '$orderNo' AND
						trn_orderdetails.intOrderYear = '$orderYear'
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($salesOrderId==$row['intSalesOrderId'])
						echo "<option selected=\"selected\" value=\"".$row['intSalesOrderId']."\">".$row['strSalesOrderNo']."/".$row['strName']."</option>";
					else
						echo "<option value=\"".$row['intSalesOrderId']."\">".$row['strSalesOrderNo']."/".$row['strName']."</option>";
				}
				  ?>
          </select></td>
        </tr>
      </table></td>
    </tr>
    <?PHp
		die();
		}
	?>
    
    
    <?php
	if($mode=='last'){
        $sql = "SELECT max(recipeRevision) as maxRevision from trn_sample_color_recipes_bulk_header
                    WHERE intSampleNo='$sampleNo' AND intSampleYear='$sampleYear' AND intRevisionNo='$revNo' AND strCombo='$combo' AND strPrintName='$printName' ";
        $results = $db->RunQuery($sql);
        $maxRow = mysqli_fetch_array($results);
        $bulkRevision = 0;
        if ($maxRow['maxRevision'] !=''){
            $bulkRevision = $maxRow['maxRevision'];
        }
 	?>
    
    <tr>
      <td height="17" colspan="9"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="32%"><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:264px;height:148px;overflow:hidden" >
            <?php
			if($sampleNo!='')
			{
				echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"../../../../documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revNo-".substr($printName,6,1).".png\" />";	
			}
			 ?>
          </div></td>
          <td width="68%">	
          <?php  if(($mode=='last')||($mode=='second'))
	  {
	   ?><table style="margin-left:50px;font-size:12px" width="53%" border="0" cellspacing="0" cellpadding="0" >
            <tr>
              <td width="37%" >Sample No</td>
              <td width="2%" >:</td>
              <td width="55%" id="txtSampNo"><?php echo $sampleNo; ?></td>
              <td width="5%" rowspan="3" id="">&nbsp;</td>
              </tr>
            <tr>
              <td>Sample Year</td>
              <td >:</td>
              <td id="txtSampleYear"><?php echo $sampleYear; ?></td>
              </tr>
            <tr>
              <td >Revision No</td>
              <td >:</td>
              <td id="txtRevNo"><?php echo $revNo; ?></td>
              <td width="1%" rowspan="3" id="txtRevNo">&nbsp;</td>
              </tr>
            <tr>
              <td>Combo</td>
              <td >:</td>
              <td id="txtCombo"><?php echo $combo; ?></td>
              </tr>
            <tr>
              <td>Print</td>
              <td >:</td>
              <td id="txtPrintName"><?php echo $printName; ?></td>
            </tr>

              <tr>
                  <td>Bulk Revision</td>
                  <td >:</td>
                  <td id="txtRecipeRevision"><?php echo $bulkRevision; ?></td>
              </tr>
            <?php
	  }
	  ?>
            <?php
       if(($mode=='last'))
	  {
		?>
            <tr>
              <td>Sales Order No</td>
              <td >:</td>
              <td id="txtSalesOrderNo"><?php echo $SalesOrderNo; ?></td>
              <td id="txtSalesOrderID" style="display:none"><?php echo $salesOrderId; ?></td>
            </tr>
           <tr>
              <td>Graphic No</td>
              <td >:</td>
              <td id="txtGraphicNo"><?php echo $graphic; ?></td>
           </tr>
            <tr>
              <td>Order Qty</td>
              <?php
			  $sql=" select sum(trn_orderdetails.intQty) as orderQty 
			FROM `trn_orderdetails`
			WHERE
			trn_orderdetails.intOrderNo = '$orderNo' AND
			trn_orderdetails.intOrderYear = '$orderYear' ";
 				$result = $db->RunQuery($sql);
				$row=mysqli_fetch_array($result);
				$orderQty = $row['orderQty'];
			  ?>
              <td >:</td>
              <td id="txtOrderQty"><?php echo $orderQty; ?></td>
              <td id="orderQty" style="display:none"><?php echo $orderQty; ?></td>
              </tr>
            <tr>
              <td>Completed Qty</td>
              <?php
			  $sql="SELECT
					Sum(trn_inktype_production_allocation.dblOrderQty)  as dblCompletedQty 
					FROM `trn_inktype_production_allocation`
					WHERE
					trn_inktype_production_allocation.intOrderYear = '$orderYear' AND
					trn_inktype_production_allocation.intOrderNo = '$orderNo'";
				$result = $db->RunQuery($sql);
				$row=mysqli_fetch_array($result);
			  ?>
              <td >:</td>
              <td id="txtCompletedQty"><?php echo $row['dblCompletedQty']; ?></td>
              <td id="completedQty" style="display:none"><?php echo $row['dblCompletedQty']; ?></td>
              </tr>
            <?php
	  }
	  ?>
   
   
            
          </table></td>
        </tr>
      </table></td>
    </tr>
    
  <tr style="margin-top:250px">
      <td colspan="9"><table class="tableBorder_allRound"  style="background:#FFF;margin-top:5px" width="100%" border="0" cellspacing="1" cellpadding="0" id="tblParent">
        <tr style="color:#FF0">
          <td class="tableBorder_allRound" width="10%" align="center" bgcolor="#333333">COLOR</td>
          <td class="tableBorder_allRound" width="20%" align="center" bgcolor="#333333">TECHNIQUE</td>
          <td class="tableBorder_allRound" width="21%" align="center" bgcolor="#333333">INK TYPE</td>
          <td class="tableBorder_allRound" width="8%" align="center" bgcolor="#333333">CON PC</td>
          <td class="tableBorder_allRound" width="14%" align="center" bgcolor="#333333">EXP CONSUMP</td>
          <td class="tableBorder_allRound" width="10%" align="center" bgcolor="#333333">QTY</td>
          <td class="tableBorder_allRound" width="7%" align="center" bgcolor="#333333">ITEM</td>
          <td class="tableBorder_allRound" width="10%" align="center" bgcolor="#333333">STOCK QTY(g)</td>
          <td class="tableBorder_allRound" width="10%" align="center" bgcolor="#333333">ISSUE</td>
          <td class="tableBorder_allRound" width="10%" align="center" bgcolor="#333333">RETURN</td>
        </tr>
        <?Php
             if ($bulkRevision > 0){
                 $sql = "SELECT
                    trn_sample_color_recipes_bulk.intNumOfShots as intNoOfShots,
                    trn_sample_color_recipes_bulk.intColorId,
                    trn_sample_color_recipes_bulk.intInkTypeId as intInkType,
                    mst_inktypes.strName AS inkTypeName,
                    mst_colors.strName AS colorName,
                    trn_sample_color_recipes_bulk.intTechniqueId as intTechnique,
                    mst_techniques.strName AS technique,
                    trn_sampleinfomations_details_technical.dblColorWeight
                FROM
                    trn_sample_color_recipes_bulk
                    Inner Join mst_inktypes ON mst_inktypes.intId = trn_sample_color_recipes_bulk.intInkTypeId
                    Inner Join mst_colors ON mst_colors.intId = trn_sample_color_recipes_bulk.intColorId
                    Inner Join mst_techniques ON mst_techniques.intId = trn_sample_color_recipes_bulk.intTechniqueId						
                    Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo=trn_sample_color_recipes_bulk.intSampleNo 
                    AND trn_sampleinfomations_details_technical.intSampleYear = trn_sample_color_recipes_bulk.intSampleYear
                    AND trn_sampleinfomations_details_technical.intRevNo = trn_sample_color_recipes_bulk.intRevisionNo
                    AND trn_sampleinfomations_details_technical.strPrintName = trn_sample_color_recipes_bulk.strPrintName 
                    AND trn_sampleinfomations_details_technical.strComboName = trn_sample_color_recipes_bulk.strCombo
                WHERE
                    trn_sample_color_recipes_bulk.intSampleNo =  '$sampleNo' AND
                    trn_sample_color_recipes_bulk.intSampleYear =  '$sampleYear' AND
                    trn_sample_color_recipes_bulk.intRevisionNo =  '$revNo' AND
                    trn_sample_color_recipes_bulk.strPrintName =  '$printName' AND
                    trn_sample_color_recipes_bulk.strCombo =  '$combo' AND 
                    trn_sample_color_recipes_bulk.bulkRevNumber =  '$bulkRevision' 
                    group by 
                    trn_sample_color_recipes_bulk.intColorId,
                    trn_sample_color_recipes_bulk.intTechniqueId,
                    trn_sample_color_recipes_bulk.intInkTypeId
            ";
             } else {
                 $sql = "SELECT
						trn_sampleinfomations_details_technical.intNoOfShots,
						trn_sampleinfomations_details_technical.dblColorWeight,
						trn_sampleinfomations_details_technical.intColorId,
						trn_sampleinfomations_details_technical.intInkTypeId as intInkType,
						mst_inktypes.strName AS inkTypeName,
						mst_colors.strName AS colorName,
						trn_sampleinfomations_details.intTechniqueId as intTechnique,
						mst_techniques.strName AS technique
					FROM
						trn_sampleinfomations_details_technical
						Inner Join mst_inktypes ON mst_inktypes.intId = trn_sampleinfomations_details_technical.intInkTypeId
						Inner Join mst_colors ON mst_colors.intId = trn_sampleinfomations_details_technical.intColorId
						Inner Join trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_details_technical.intSampleNo AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_details_technical.intRevNo AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_details_technical.strPrintName AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_details_technical.strComboName AND trn_sampleinfomations_details.intColorId = trn_sampleinfomations_details_technical.intColorId
						Inner Join mst_techniques ON mst_techniques.intId = trn_sampleinfomations_details.intTechniqueId
					WHERE
						trn_sampleinfomations_details_technical.intSampleNo 	=  '$sampleNo' AND
						trn_sampleinfomations_details_technical.intSampleYear 	=  '$sampleYear' AND
						trn_sampleinfomations_details_technical.intRevNo 		=  '$revNo' AND
						trn_sampleinfomations_details_technical.strPrintName 	=  '$printName' AND
						trn_sampleinfomations_details_technical.strComboName 	=  '$combo'
				";
             }
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					$sql2 = "SELECT * FROM `trn_sampleinfomations_details_technical` WHERE `intSampleNo` = '$sampleNo' AND `intSampleYear` = '$sampleYear' AND `intRevNo` = '$revNo' AND `strPrintName` = '$printName' AND `strComboName` = '$combo' AND `intColorId` = '".$row['intColorId']."' AND `intInkTypeId` = '10000'";
					
					$colorId=$row['intColorId'];
					$techniqueId=$row['intTechnique'];
					$inkTypeId=$row['intInkType'];
					
					$result2 = $db->RunQuery($sql2);
					if(mysqli_num_rows($result2))
						$haveUncerCoat = true;
					else
						$haveUncerCoat = false;
						
						
						
				$itemResponse=getOrderWiseInkStockBal($locationId,$sampleNo,$sampleYear,$revNo,$printName,$combo,$bulkRevision,$colorId,$techniqueId,$inkTypeId);
				$stockBal = $itemResponse['balanceQty'];
                $totalSum = $itemResponse['totalQty'];
                $inkId = $itemResponse['inkId'];
				?>
        <tr>
              <td bgcolor="#333333" id="<?Php echo $row['intColorId']; ?>"><?php echo $row['colorName'] ?></td>
              <td bgcolor="#333333" id="<?php echo $row['intTechnique']; ?>"><?php echo $row['technique'] ?></td>
              <td bgcolor="#333333" id="<?php echo $row['intInkType']; ?>"><?php echo $row['inkTypeName'] ?></td>
              <td align="right" bgcolor="#333333" class="normalfntMid" id="<?php echo $row['dblColorWeight']; ?>"><?php echo $row['dblColorWeight'] ?></td>
              <td align="right" bgcolor="#333333" class="normalfntMid" id="<?php echo $qty*$row['dblColorWeight']; ?>"><?php echo $qty*$row['dblColorWeight'] ?></td>
              <td align="center" bgcolor="#333333" class="normalfntMid"><input id="txtProdQty" class="hex txtQty ui-keyboard-input ui-widget-content ui-corner-all" type="text" value="<?php echo $qty*$row['dblColorWeight']; ?>" role="textbox" aria-haspopup="true" style="height:30px;margin-top:5px; width:95px; alignment-adjust:before-edge; text-align:right"></td> 
			 </td>
              <td style="height:20px"  bgcolor="#333333" class="normalfntMid"><a  id="butAdd" class="green button " style="height:16px;margin:2px 2px 2px 2px;">View</a></td>
              <td align="right" bgcolor="#333333" class="normalfntRight" id="stockBal"><?php echo $stockBal; ?></td>
              <td style="height:20px"  bgcolor="#333333" class="normalfntMid"><a  id="issueInkMain" class="green button " style="height:16px;margin:2px 2px 2px 2px;">ISSUE</a></td>
              <td style="height:20px"  bgcolor="#333333" class="normalfntMid"><a  id="returnInkMain" class="green button " style="height:16px;margin:2px 2px 2px 2px;">RETURN</a></td>
              <td align="right" bgcolor="#333333" hidden class="normalfntRight" id="totalItemQty"><?php echo $totalSum; ?></td>
              <td align="right" bgcolor="#333333" hidden class="normalfntRight" id="inkIdMain"><?php echo $inkId; ?></td>
        </tr>
        <?php
				}
		?>
    </table></td>
    </tr>
        <?php
				}
		?>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td width="125">&nbsp;</td>
      <td width="122">&nbsp;</td>
    </tr>
  </table>
			
		</div>
		<p>
		  
</p>
	
</body>
</html>
<?php

function getOrderWiseInkStockBal($locationId,$sampleNo,$sampleYear,$revNo, $printName, $combo, $recipeRevision,$colorId,$techniqueId,$inkTypeId){
	global $db;

	$sqlItem = "";
    $inkId = -1;
	if ($recipeRevision > 0 ){
        $sqlItem = "SELECT
							trn_sample_color_recipes_bulk.intItem,
							mst_item.strName,
							trn_sample_color_recipes_bulk.dblWeight
							FROM
							trn_sample_color_recipes_bulk
							Inner Join mst_item ON mst_item.intId = trn_sample_color_recipes_bulk.intItem
							WHERE
							trn_sample_color_recipes_bulk.intSampleNo =  '$sampleNo' AND
							trn_sample_color_recipes_bulk.intSampleYear =  '$sampleYear' AND
							trn_sample_color_recipes_bulk.intRevisionNo =  '$revNo' AND
							trn_sample_color_recipes_bulk.strCombo =  '$combo' AND
							trn_sample_color_recipes_bulk.strPrintName =  '$printName' AND
							trn_sample_color_recipes_bulk.intColorId =  '$colorId' AND
							trn_sample_color_recipes_bulk.intTechniqueId =  '$techniqueId' AND
							trn_sample_color_recipes_bulk.intInkTypeId =  '$inkTypeId' AND 
							trn_sample_color_recipes_bulk.bulkRevNumber =  '$recipeRevision'
							AND mst_item.intStatus	=	1
							";
        $sql1 = "SELECT
							sum(trn_sample_color_recipes_bulk.dblWeight) as sumQty
							FROM
							trn_sample_color_recipes_bulk
							Inner Join mst_item ON mst_item.intId = trn_sample_color_recipes_bulk.intItem
							WHERE
							trn_sample_color_recipes_bulk.intSampleNo =  '$sampleNo' AND
							trn_sample_color_recipes_bulk.intSampleYear =  '$sampleYear' AND
							trn_sample_color_recipes_bulk.intRevisionNo =  '$revNo' AND
							trn_sample_color_recipes_bulk.strCombo =  '$combo' AND
							trn_sample_color_recipes_bulk.strPrintName =  '$printName' AND
							trn_sample_color_recipes_bulk.intColorId =  '$colorId' AND
							trn_sample_color_recipes_bulk.intTechniqueId =  '$techniqueId' AND
							trn_sample_color_recipes_bulk.intInkTypeId =  '$inkTypeId' AND 
							trn_sample_color_recipes_bulk.bulkRevNumber =  '$recipeRevision' 
							AND mst_item.intStatus	=	1
							";
        } else {
        $sqlItem = "SELECT
							trn_sample_color_recipes.intItem,
							mst_item.strName,
							trn_sample_color_recipes.dblWeight,
							trn_sample_color_recipes.intBulkItem
							FROM
							trn_sample_color_recipes
							Inner Join mst_item ON mst_item.intId = trn_sample_color_recipes.intItem
							WHERE
							trn_sample_color_recipes.intSampleNo =  '$sampleNo' AND
							trn_sample_color_recipes.intSampleYear =  '$sampleYear' AND
							trn_sample_color_recipes.intRevisionNo =  '$revNo' AND
							trn_sample_color_recipes.strCombo =  '$combo' AND
							trn_sample_color_recipes.strPrintName =  '$printName' AND
							trn_sample_color_recipes.intColorId =  '$colorId' AND
							trn_sample_color_recipes.intTechniqueId =  '$techniqueId' AND
							trn_sample_color_recipes.intInkTypeId =  '$inkTypeId'
							AND mst_item.intStatus	=	1
							";

        $sql1 = "SELECT
							sum(trn_sample_color_recipes.dblWeight) as sumQty
							FROM
							trn_sample_color_recipes
							Inner Join mst_item ON mst_item.intId = trn_sample_color_recipes.intItem
							WHERE
							trn_sample_color_recipes.intSampleNo =  '$sampleNo' AND
							trn_sample_color_recipes.intSampleYear =  '$sampleYear' AND
							trn_sample_color_recipes.intRevisionNo =  '$revNo' AND
							trn_sample_color_recipes.strCombo =  '$combo' AND
							trn_sample_color_recipes.strPrintName =  '$printName' AND
							trn_sample_color_recipes.intColorId =  '$colorId' AND
							trn_sample_color_recipes.intTechniqueId =  '$techniqueId' AND
							trn_sample_color_recipes.intInkTypeId =  '$inkTypeId'
							AND mst_item.intStatus	=	1
							";
    }
    $result1 = $db->RunQuery($sql1);
    $row1=mysqli_fetch_array($result1);
    $sumQty=$row1['sumQty'];
    $sql = "SELECT bulk_colorroom_inks.inkId as inkId FROM bulk_colorroom_inks ";

    $result = $db->RunQuery($sqlItem);
    while($row=mysqli_fetch_array($result))
    {
        $itemId =  $row['intItem'];
        $weight =  $row['dblWeight'];
        $propotion=  round($weight/$sumQty,8);
        $tableName = "bic".$itemId;
        $sql.= "INNER JOIN bulk_ink_item_composition `$tableName` ON `$tableName`.inkId = bulk_colorroom_inks.inkId 
            AND `$tableName`.item = '$itemId' AND `$tableName`.ratio = '$propotion' ";
    }
    $sql.= " WHERE 1=1";

    $result = $db->RunQuery($sql);
    $rowcount = mysqli_num_rows($result);
    if ($rowcount > 0){
        $row=mysqli_fetch_array($result);
        $inkId=$row['inkId'];
        $sql = "SELECT SUM(quantity) as balanceQty FROM bulk_ink_quantity WHERE `inkId` ='$inkId' AND `locationId` = '$locationId'";
        $result = $db->RunQuery($sql);
        $row=mysqli_fetch_array($result);
        $response['balanceQty']=$row['balanceQty'];
    } else {
        $response['balanceQty']=0;
    }
    $response['totalQty']=$sumQty;
    $response['inkId']=$inkId;
	return $response;
}


?>

// JavaScript Document
var colorId;
var techniqueId;
var inkTypeId;
var inkId;
var inkRow;

$(document).ready(function(){
	$('#butSearch').live('click',searchDetails);
	$('#butAdd').live('click',viewPopup);//viewPopup_comments
	$('#btnIssueInkPopUp').live('click',issueInkPopup);
	$('#btnReturnInkPopUp').live('click',returnInkPopup);
	$('#butViewDetails').live('click',viewPopup_comments);//viewPopup_comments
	//$('#cboItemTypeMain').live('change',loadItems);
	//$('#butClose_popup').live('click','hidePopup');
	$('#cboItemTypeMain').live('change',loadSubCategory);
	$('#cboItemTypeMain2').live('change',loadSubCategory2);
	
	$('#cboItemTypeSub').live('change',loadItems);
	$('#butSearchItem').live('click',searchItems);
	$('#butSearchItem2').live('click',searchItems2);
	
	$('#butAddItemToGrid').live('click',addItemToGrid);
	$('#butAddItemToGrid2').live('click',addItemToGrid2);
	$('#butAddItemToGrid3').live('click',addItemToGrid3);
	
	$('#butSave').live('click',saveDetails);
	$('#butCal').live('click',cal);
	
	$('#removeRow').live('click',removeRow);
	$('#butAddUnderCoat').live('click',addUndercoat);
	$('.txtShots').live('blur',updateShots);
	$('#butAddAntiMigration').live('click',butAddAntiMigration);
	//butViewAddItem
	$('#butViewAddItem').live('click',butViewAddItem);
	$('#butViewCopyItem').live('click',butViewCopyItem);
	$('#butCreateInk').live('click',createInk);
	$('#butIssueInk').live('click',issueInk);
	$('#butReturnInk').live('click',returnInk);
    $('#issueInkMain').live('click',issueInkFromMain);
	$('#returnInkMain').live('click',returnInkFromMain);
	
});

function butViewCopyItem()
{
	showWaiting();
	colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
	techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
	inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
	
	$('#divBackgroundImg').css('left','10px');
	$('#divBackgroundImg').css('top','10px');
	
	var url = "popup_copyRecipes.php";
		url +="?sampleYear="+$('#cboOrderYear').val();
		url +="&sampleNo="+$('.txtOrderNo').val();
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
		url +="&colorId="+colorId;
		url +="&techniqueId="+techniqueId;
		url +="&inkTypeId="+inkTypeId;
	//alert(url);
	$('#divBackgroundImg').load(url,function(){
		$('#butClose_popup').click(function(){
			hideWaiting();
		});	
		$('#txtSearchItem').keyboard();
		//$('#txtQty').addClass('hex txtOrderNo ui-keyboard-input ui-widget-content ui-corner-all');
		$('#txtQty').keyboard();
		
		
	});
}
function butViewAddItem()
{
	showWaiting();
	colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
	techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
	inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
	
	$('#divBackgroundImg').css('left','10px');
	$('#divBackgroundImg').css('top','10px');
	
	var url = "popup_addItems.php";
		url +="?sampleYear="+$('#cboOrderYear').val();
		url +="&sampleNo="+$('.txtOrderNo').val();
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
		url +="&colorId="+colorId;
		url +="&techniqueId="+techniqueId;
		url +="&inkTypeId="+inkTypeId;
	//alert(url);
	$('#divBackgroundImg').load(url,function(){
		$('#butClose_popup').click(function(){
			hideWaiting();
		});	
		$('#txtSearchItem').keyboard();
		//$('#txtQty').addClass('hex txtOrderNo ui-keyboard-input ui-widget-content ui-corner-all');
		$('#txtQty').keyboard();
		
		
	});
}
function butAddAntiMigration()
{
	var url = "bulkStockAllocation_inkType-db-set.php?requestType=butAddAntiMigration";
		url +="&sampleYear="+$('#cboOrderYear').val();
		url +="&sampleNo="+$('.txtOrderNo').val();
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
	$.ajax({url:url,async:false});	
	document.location.href = document.location.href;
}

function createInk()
{
	var data = "requestType=createInk";
	var items = "";
	var totalInkQty =  $('#totalInkQty').val();
	var orderNo =  $('#num').val();
	var orderYear =  $('#cboOrderYear').val();
	var salesOrderId =  $('#txtSalesOrderID').text();
	var sampleNo =  $('#txtSampNo').text();
	var sampleYear =  $('#txtSampleYear').text();
	var revisionNo =  $('#txtRevNo').text();
	var combo =  $('#txtCombo').text();
	var printName =  $('#txtPrintName').text();
	var bulkRevNo =  $('#txtRecipeRevision').text();
	var colorId =  $('#colorId').val();
	var techniqueId =  $('#techniqueTypeId').val();
	var inkTypeId =  $('#inkTypeId').val();
	var graphic =  $('#txtGraphicNo').text();
	var inkId =  $('#inkId').val();

	$('#tblPopUpGrid >tbody>tr').each(function () {
		var itemId = $(this).attr('id');
		var itemProp = $(this).find('#itemPropotion').val();
		var itemQty = $(this).find('#itemQty').val();
		items += '{"itemId":"' + itemId + '","propotion":"' + itemProp + '","qty":"' + itemQty + '"},';
	});
	items = items.substr(0, items.length - 1);
	data =  data + "&items=[" + items + ']';
	data =  data + "&inkQty=" + totalInkQty;
	data =  data + "&orderNo=" + orderNo;
	data =  data + "&orderYear=" + orderYear;
	data =  data + "&soId=" + salesOrderId;
	data =  data + "&sampleNo=" + sampleNo;
	data =  data + "&sampleYear=" + sampleYear;
	data =  data + "&revNo=" + revisionNo;
	data =  data + "&combo=" + combo;
	data =  data + "&printName=" + printName;
	data =  data + "&bulkRev=" + bulkRevNo;
	data =  data + "&colorId=" + colorId;
	data =  data + "&techniqueId=" + techniqueId;
	data =  data + "&inkTypeId=" + inkTypeId;
	data =  data + "&graphic=" + graphic;
	data =  data + "&inkId=" + inkId;
	var url = "bulkStockAllocation_inkType-db-set.php";
	var obj = $.ajax({
		url:url,
		type:'post',
		dataType: "json",
		data:data,
		async:false,

		success:function(json){
			// $('#frmRptRequisition #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
			if(json.type=='pass')
			{
				$('#butCreateInk').validationEngine('showPrompt', json.msg,json.type);
				var t=setTimeout("alertxR()",3000);
				return;
			} else {
				$('#butCreateInk').validationEngine('showPrompt', json.msg,json.type);
			}
		},
		error:function(xhr,status){

			hideWaiting();
			$('#butCreateInk').validationEngine('showPrompt', errormsg(xhr.status),'fail');
			var t=setTimeout("alertxR()",3000);
		}
	});

}

function issueInk()
{
	var data = "requestType=issueInk";
	var orderNo =  $('#num').val();
	var orderYear =  $('#cboOrderYear').val();
	var salesOrderId =  $('#txtSalesOrderID').text();
	var sampleNo =  $('#txtSampNo').text();
	var sampleYear =  $('#txtSampleYear').text();
	var revisionNo =  $('#txtRevNo').text();
	var combo =  $('#txtCombo').text();
	var printName =  $('#txtPrintName').text();
	var bulkRevNo =  $('#txtRecipeRevision').text();
	var graphic =  $('#txtGraphicNo').text();
	var totalInkQty =  $('#issueQty').val();

	data =  data + "&inkQty=" + totalInkQty;
	data =  data + "&orderNo=" + orderNo;
	data =  data + "&orderYear=" + orderYear;
	data =  data + "&soId=" + salesOrderId;
	data =  data + "&sampleNo=" + sampleNo;
	data =  data + "&sampleYear=" + sampleYear;
	data =  data + "&revNo=" + revisionNo;
	data =  data + "&combo=" + combo;
	data =  data + "&printName=" + printName;
	data =  data + "&bulkRev=" + bulkRevNo;
	data =  data + "&colorId=" + colorId;
	data =  data + "&techniqueId=" + techniqueId;
	data =  data + "&inkTypeId=" + inkTypeId;
	data =  data + "&graphic=" + graphic;
	data =  data + "&inkId=" + inkId;
	var url = "bulkStockAllocation_inkType-db-set.php";
	var obj = $.ajax({
		url:url,
		type:'post',
		dataType: "json",
		data:data,
		async:false,

		success:function(json){
			// $('#frmRptRequisition #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
			if(json.type=='pass')
			{
				$('#butIssueInk').validationEngine('showPrompt', json.msg,json.type);
				var t=setTimeout("alertxR()",3000);
				return;
			} else {
				$('#butIssueInk').validationEngine('showPrompt', json.msg,json.type);
			}
		},
		error:function(xhr,status){

			hideWaiting();
			$('#butIssueInk').validationEngine('showPrompt', errormsg(xhr.status),'fail');
			var t=setTimeout("alertxR()",2000);
		}
	});

}

function returnInk()
{
	var data = "requestType=returnInk";
	var orderNo =  $('#num').val();
	var orderYear =  $('#cboOrderYear').val();
	var salesOrderId =  $('#txtSalesOrderID').text();
	var sampleNo =  $('#txtSampNo').text();
	var sampleYear =  $('#txtSampleYear').text();
	var revisionNo =  $('#txtRevNo').text();
	var combo =  $('#txtCombo').text();
	var printName =  $('#txtPrintName').text();
	var bulkRevNo =  $('#txtRecipeRevision').text();
	var graphic =  $('#txtGraphicNo').text();
	var wasteInkQty =  $('#returnWasteQty').val();
	var returnGoodInkQty =  $('#returnGoodQty').val();

	data =  data + "&goodInkQty=" + returnGoodInkQty;
	data =  data + "&wasteInkQty=" + wasteInkQty;
	data =  data + "&orderNo=" + orderNo;
	data =  data + "&orderYear=" + orderYear;
	data =  data + "&soId=" + salesOrderId;
	data =  data + "&sampleNo=" + sampleNo;
	data =  data + "&sampleYear=" + sampleYear;
	data =  data + "&revNo=" + revisionNo;
	data =  data + "&combo=" + combo;
	data =  data + "&printName=" + printName;
	data =  data + "&bulkRev=" + bulkRevNo;
	data =  data + "&colorId=" + colorId;
	data =  data + "&techniqueId=" + techniqueId;
	data =  data + "&inkTypeId=" + inkTypeId;
	data =  data + "&graphic=" + graphic;
	data =  data + "&inkId=" + inkId;
	var url = "bulkStockAllocation_inkType-db-set.php";
	var obj = $.ajax({
		url:url,
		type:'post',
		dataType: "json",
		data:data,
		async:false,

		success:function(json){
			// $('#frmRptRequisition #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
			if(json.type=='pass')
			{
				$('#butReturnInk').validationEngine('showPrompt', json.msg,json.type);
				var t=setTimeout("alertxR()",3000);
				return;
			} else {
				$('#butReturnInk').validationEngine('showPrompt', json.msg,json.type);
			}
		},
		error:function(xhr,status){

			hideWaiting();
			$('#butReturnInk').validationEngine('showPrompt', errormsg(xhr.status),'fail');
			var t=setTimeout("alertxR()",2000);
		}
	});

}

function issueInkFromMain()
{

    colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
    techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
    inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
    inkId = $(this).parent().parent().find('#inkIdMain').text();
    issueInkPopup();
}

function returnInkFromMain()
{

	colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
	techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
	inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
	inkId = $(this).parent().parent().find('#inkIdMain').text();
	returnInkPopup();
}

function updateShots()
{
	colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
	techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
	inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
	
	var url = "bulkStockAllocation_inkType-db-set.php?requestType=saveShots";
		url +="&sampleYear="+$('#cboOrderYear').val();
		url +="&sampleNo="+$('.txtOrderNo').val();
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
		url +="&colorId="+colorId;
		url +="&techniqueId="+techniqueId;
		url +="&inkTypeId="+inkTypeId;
		url +="&shots="+$(this).val();
	$.ajax({url:url,async:false});
}
function addUndercoat()
{
	showWaiting();
	colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
	techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
	inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
	
	$('#divBackgroundImg').css('left','10px');
	$('#divBackgroundImg').css('top','10px');
	
	var url 	= "bulkStockAllocation_inkType-db-set.php?requestType=addNewUndercoat";
		url +="&sampleYear="+$('#cboOrderYear').val();
		url +="&sampleNo="+$('.txtOrderNo').val();
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
		url +="&colorId="+colorId;
		url +="&techniqueId="+techniqueId;
		url +="&inkTypeId="+inkTypeId;
		
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				document.location.href = document.location.href;
	}
	});		
}


function removeRow()
{
	var id 				= $(this).parent().attr('id');
	var sampleNo 		= $('#txtSampNo').text();
	var sampleYear 		= $('#txtSampleYear').text();
	var revNo 			= $('#txtRevNo').text();
	var combo 			= $('#txtCombo').text();
	var printName 		= $('#txtPrintName').text();
	var obj				= this;
	
	var url = "bulkStockAllocation_inkType-db-set.php?requestType=RemoveItem";
		$.ajax({
		url:url,
		async:false,
		dataType:'json',
		data:"&id="+id+"&sampleNo="+sampleNo+"&sampleYear="+sampleYear+"&revNo="+revNo+"&combo="+combo+"&printName="+printName+"&colorId="+colorId+"&techniqueId="+techniqueId+"&inkTypeId="+inkTypeId,
		success:function(json){
					
			if(json.type=='fail')
			{
				$('#msg').html(json.msg);	
			}
			else
			{
				$('#msg').html('');
				$(obj).parent().remove();
			}
		}
	});	
		
}
function cal()
{
	var y =$(this).parent().parent().find('.txtQty').val();
	var x = $(this).parent().parent().find('.txtQty2').val();
	
	if(y=='')
		y = 0;
	if(x=='')
		x=0;
	var stock=parseFloat($(this).parent().parent().find('.txtQty6').val())-parseFloat(y);
	$(this).parent().parent().find('.txtQty2').val(parseFloat(x)+parseFloat(y));
	$(this).parent().parent().find('.txtQty').val('');
	$(this).parent().parent().find('.txtQty6').val(stock);
	$(this).parent().parent().find('.txtQty5').val(parseFloat($(this).parent().parent().find('.txtQty5').val())+parseFloat(y));
	

}
function addItemToGrid()
{
	//alert(1);
	var itemName 		= $('#cboItemList option:selected').text();
	var id 				= $('#cboItemList').val();
	var stockBal		= 0;
	var sampleNo 		= $('#txtSampNo').text();
	var sampleYear 		= $('#txtSampleYear').text();
	var revNo 			= $('#txtRevNo').text();
	var combo 			= $('#txtCombo').text();
	var printName 		= $('#txtPrintName').text();
	var saveItemStatus  = true;
	if(id!='')
	{
		var url = "bulkStockAllocation_inkType-db-get.php?requestType=getStockBalance&id="+id;
			$.ajax({
			url:url,
			async:false,
			dataType:'json',
			success:function(json){
						
					stockBal =json.stockBal ;
			}
		});
		var url = "bulkStockAllocation_inkType-db-set.php?requestType=saveItem";
			$.ajax({
			type: "POST",
			url:url,
			async:false,
			dataType:'json',
			data:"&id="+id+"&sampleNo="+sampleNo+"&sampleYear="+sampleYear+"&revNo="+revNo+"&combo="+combo+"&printName="+printName+"&colorId="+colorId+"&techniqueId="+techniqueId+"&inkTypeId="+inkTypeId,
			success:function(json){
						
				if(json.type=='fail')
				{
					$('#msg').html(json.msg);
					saveItemStatus = false;
				}
				else
				{
					$('#msg').html('');
				}
			}
		});	
	
		if(saveItemStatus)
		{	
			$('#tblPopUpGrid').append('<tr id="'+id+'"><td>&nbsp;</td><td id="removeRow" align="center" bgcolor="#CCCCCC" style="color:#F00">DEL</td><td style="color:black" bgcolor="#CCCCCC">'+itemName+'</td> <td width="8%" bgcolor="#CCCCCC"><input disabled="disabled" value="0" style="width:100px;height:50px; text-align:right" class="txtQty1" type="text" name="txtQty1" id="txtQty1" /></td><td width="7%" bgcolor="#CCCCCC"><input disabled="disabled" value="0" style="width:100px;height:50px; text-align:right" class="txtQty4" type="text" name="txtQty4" id="txtQty4" /></td><td width="7%" bgcolor="#CCCCCC"><input disabled="disabled" value="0" style="width:100px;height:50px; text-align:right" class="txtQty5" type="text" name="txtQty5" id="txtQty5" /></td><td width="10%" bgcolor="#CCCCCC"><input disabled="disabled" value="0" style="width:100px;height:50px; text-align:right" class="txtQty2" type="text" name="txtQty2" id="txtQty2" /></td><td width="10%" bgcolor="#CCCCCC"><input value="" style="width:100px;height:50px; text-align:right" class="hex txtQty ui-keyboard-input ui-widget-content ui-corner-all" type="text"  id="txtQty"  aria-haspopup="true" /></td><td width="15%" bgcolor="#CCCCCC"><div id="butCal" style="width:50px;height:35px" class="button green" >+</div></td><td width="6%" bgcolor="#CCCCCC"><input disabled="disabled" value="'+stockBal+'" style="width:100px;height:50px; text-align:right" class="txtQty6" type="text" name="txtQty6" id="txtQty6" /></td></tr>');
			$('.txtQty').keyboard();
		}
	}
}
function addItemToGrid2()
{
	var id 			= $('#cboItemList').val();
	var url = "bulkStockAllocation_inkType-db-set.php?requestType=addNewItem2&mainId="+$('#cboItemTypeMain2').val()+'&subId='+$('#cboItemTypeSub2').val()+'&itemId='+id;
	$.ajax({
		type: "POST",
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				if(json.status==1)
					$('#msg').html('SAVED SUCCESSFULLY.');
				else
					$('#msg').html('');
	}
	});
}

function addItemToGrid3()
{
	var sampleNo		= $('#txtOrderNo').val();
	var sampleYear		= $('#txtSampleYear').val();
	
	var rev1 			= $('#cboRevisionNo1').val();
	var combo1 			= $('#cboCombo1').val();
	var print1 			= $('#cboPrint1').val();
	
	var rev2 			= $('#cboRevisionNo2').val();
	var combo2 			= $('#cboCombo2').val();
	var print2 			= $('#cboPrint2').val();
	
	var url 		 = "bulkStockAllocation_inkType-db-set.php?requestType=copyRecipes";
		url			+= "&sampleNo="+sampleNo;
		url			+= "&sampleYear="+sampleYear;
		
		url			+= "&rev1="+rev1;
		url			+= "&rev2="+rev2;
		url			+= "&combo1="+combo1;
		url			+= "&combo2="+combo2;
		url			+= "&print1="+print1;
		url			+= "&print2="+print2;
		
	$.ajax({
		type: "POST",
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				if(json.status==1)
					$('#msg').html(json.msg);
				else
					$('#msg').html('');
	}
	});
}

function searchItems()
{
	var url = "bulkStockAllocation_inkType-db-get.php?requestType=searchItems&mainId="+$('#cboItemTypeMain').val()+'&subId='+$('#cboItemTypeSub').val()+'&like='+$('#txtSearchItem').val();
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				
				$('#cboItemList').html(json.itemList);
	}
	});		
}

function searchItems2()
{
	var url = "bulkStockAllocation_inkType-db-get.php?requestType=searchItems2&mainId="+$('#cboItemTypeMain2').val()+'&subId='+$('#cboItemTypeSub2').val()+'&like='+$('#txtSearchItem').val();
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				$('#cboItemList').html(json.itemList);
	}
	});		
}

function loadItems()
{
	$('#txtSearchItem').val('');
	var url = "bulkStockAllocation_inkType-db-get.php?requestType=loadItems&mainId="+$('#cboItemTypeMain').val()+'&subId='+$('#cboItemTypeSub').val();
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				$('#cboItemList').html(json.itemList);
	}
	});	
}
function loadSubCategory()
{
	var url = "bulkStockAllocation_inkType-db-get.php?requestType=loadSubCategoryList&mainId="+$('#cboItemTypeMain').val();
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				$('#cboItemTypeSub').html(json.subList);
				$('#cboItemList').html(json.itemList);
	}
	});	
}

function loadSubCategory2()
{
	var url = "bulkStockAllocation_inkType-db-get.php?requestType=loadSubCategoryList&mainId="+$('#cboItemTypeMain2').val();
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				$('#cboItemTypeSub2').html(json.subList);
				
	}
	});	
}

function searchDetails()
{
	var orderYear 	= $('#cboOrderYear').val();
	var orderNo 	= $('#num').val();
	document.location.href = 'index.php?orderYear='+orderYear+'&orderNo='+orderNo+'&qty='+$('#txtQty').val();	

}

/*function hidePopup()
{
	alert(1);
}*/

function viewPopup_comments()
{
	showWaiting();
	colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
	techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
	inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
	
	$('#divBackgroundImg').css('left','10px');
	$('#divBackgroundImg').css('top','10px');
	
	var url = "popup_comments.php";
		url +="?sampleYear="+$('#cboOrderYear').val();
		url +="&sampleNo="+$('.txtOrderNo').val();
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
		url +="&colorId="+colorId;
		url +="&techniqueId="+techniqueId;
		url +="&inkTypeId="+inkTypeId;
	//alert(url);
	$('#divBackgroundImg').load(url,function(){
		$('#butClose_popup').click(function(){
			hideWaiting();
		});	
		$('#txtSearchItem').keyboard();
		//$('#txtQty').addClass('hex txtOrderNo ui-keyboard-input ui-widget-content ui-corner-all');
		$('#txtQty').keyboard();
		
		
	});
}

function viewPopup()
{
	showWaiting();
	colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
	techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
	inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
	qty = $(this).parent().parent().find('#txtProdQty').val();
    var balanceQty = $(this).parent().parent().find('#stockBal').text();
    var totalWeight = $(this).parent().parent().find('#totalItemQty').text();
	inkId = $(this).parent().parent().find('#inkIdMain').text();
	inkRow = $(this).parent().parent().closest('tr')[0].sectionRowIndex;
 	
	$('#divBackgroundImg').css('left','10px');
	$('#divBackgroundImg').css('top','10px');
	
	var url = "popup.php";
		url +="?orderYear="+$('#cboOrderYear').val();
		url +="&orderNo="+$('.txtOrderNo').val();
		url +="&salesOrderId="+$('#txtSalesOrderID').text();
		url +="&qty="+qty;
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
		url +="&colorId="+colorId;
		url +="&techniqueId="+techniqueId;
		url +="&inkTypeId="+inkTypeId;
        url +="&balanceQty="+balanceQty;
		url +="&inkId="+inkId;
        url +="&totalWeight="+totalWeight;
		url +="&recipeRev="+$('#txtRecipeRevision').text();
	//alert(url);
	$('#divBackgroundImg').load(url,function(){
		$('#butClose_popup').click(function(){
			hideWaiting();
		});
		$('#txtSearchItem').keyboard();
		//$('#txtQty').addClass('hex txtOrderNo ui-keyboard-input ui-widget-content ui-corner-all');
		$('#txtQty').keyboard();
		$('.txtQty').keyboard();
		$("#frmInkCreation").validationEngine();

	});
}

function issueInkPopup()
{
	showWaiting();

	var url = "issuePopup.php";
	url +="?inkId="+inkId;
	url +="&colorId="+colorId;
	url +="&techniqueTypeId="+techniqueId;
	url +="&inkTypeId="+inkTypeId;
	$('#divBackgroundImg').css('left','400px');
	$('#divBackgroundImg').css('top','150px');
	$('#divBackgroundImg').load(url,function(){
		$('#butCloseIssue_popup').click(function(){
			window.location.href = window.location.href;
			window.opener.location.reload();
		});
		$('#txtSearchItem').keyboard();
		//$('#txtQty').addClass('hex txtOrderNo ui-keyboard-input ui-widget-content ui-corner-all');
		$('#issueQty').keyboard();
		$('.txtQty').keyboard();

	});
}

function returnInkPopup()
{
	showWaiting();

	var url = "returnPopup.php";
	url +="?inkId="+inkId;
	url +="&colorId="+colorId;
	url +="&techniqueTypeId="+techniqueId;
	url +="&inkTypeId="+inkTypeId;
	$('#divBackgroundImg').css('left','400px');
	$('#divBackgroundImg').css('top','150px');
	$('#divBackgroundImg').load(url,function(){
		$('#butCloseReturn_popup').click(function(){
			window.location.href = window.location.href;
			window.opener.location.reload();
		});
		//$('#txtQty').addClass('hex txtOrderNo ui-keyboard-input ui-widget-content ui-corner-all');
		$('#returnQty').keyboard();
		$('.txtQty').keyboard();

	});
}

function saveDetails(){
	//alert(colorId);
	var arrRecipe = '';
	var data='';
	var i =0;
	
	var orderNo 		= 	$('.txtOrderNo').val();
	var orderYear 		= 	$('#cboOrderYear').val();
	var salesOrderId = $('#txtSalesOrderID').text();
	var revisionNo 		= 	$('#txtRevNo').text();
	var combo 			=	$('#txtCombo').text();
	var printName 		=	$('#txtPrintName').text();
	var errFlag=1;
	var tot=0;
	var weight=0;
	$('#tblPopUpGrid >tbody>tr').each(function(){
		//var itemId 		= $(this).val();
		
		//if(itemId!='')
		//{
			//var colorId 	= colorId
			//var techniqueId = technique
			//var inkTypeId 	= $(this).parent().parent().parent().parent().parent().parent().find('td:eq(3)').attr('id');
			 weight 		= $(this).find('.txtQty2').val();
			var itemId		= $(this).attr('id');
			if((weight!='') &&(weight!=0))
			{
			arrRecipe +=(i++ == 0?'':',' )+'{"color":"'+colorId+'","techId":"'+techniqueId+'","inkTypeId":"'+inkTypeId+'","weight":"'+weight+'","orderNo":"'+orderNo+'","orderYear":"'+orderYear+'","salesOrderId":"'+salesOrderId+'","revisionNo":"'+revisionNo+'","combo":"'+combo+'","printName":"'+printName+'","itemId":"'+itemId+'"}';
			errFlag=0;
			}
		//}
		tot+=parseFloat(weight);
	});

	document.getElementById('tblParent').rows[inkRow].cells[5].childNodes[0].value=(tot);
	var inkStock=parseFloat(document.getElementById('tblParent').rows[inkRow].cells[7].innerHTML);
	document.getElementById('tblParent').rows[inkRow].cells[7].innerHTML=inkStock+(tot);

		if(errFlag==1){
			alert("Please add WEIGHT to save.");
			return false;
		}

		data  = 'arrRecipe=['+ arrRecipe +']';
		
		var url 	= "bulkStockAllocation_inkType-db-set.php?requestType=saveDetails";
		$.ajax({
			type: "POST",
			url:url,
			data:data,
			async:false,
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
				$('#msg').html(json.msg);
					if(json.type=='pass'){
						$('#butSave').hide();
					}
				},
			error:function(xhr,status){
					
					
				}		
			});
			//hideWaiting();
   }

function alertxR()
{
	$('#butCreateInk').validationEngine('hide')	;
	window.location.href = window.location.href;
	window.opener.location.reload();
}
	

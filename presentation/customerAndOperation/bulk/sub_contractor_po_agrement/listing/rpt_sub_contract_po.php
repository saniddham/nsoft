<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$userId				= $sessions->getUserId();
$locationId			= $sessions->getLocationId();

include_once "class/tables/trn_order_sub_contract_header.php";
include_once "class/tables/trn_order_sub_contract_details.php";
include_once "class/tables/trn_order_sub_contract_approved_by.php";
require_once "class/cls_commonErrorHandeling_get.php";

$obj_trn_order_sub_contract_header 		= new trn_order_sub_contract_header($db);
$obj_trn_order_sub_contract_details 	= new trn_order_sub_contract_details($db);
$obj_trn_order_sub_contract_approved_by = new trn_order_sub_contract_approved_by($db);
$obj_commonErr							= new cls_commonErrorHandeling_get($db);

$programCode		= 'P0871';

$serialNo			= $_REQUEST["serialNo"];
$serialYear			= $_REQUEST["serialYear"];
$mode				= $_REQUEST["Mode"];

$header_array 		= get_header($serialNo,$serialYear); // get header data
$detail_result 		= get_Detail($serialNo,$serialYear); // get detail data

$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$userId,$programCode,'RunQuery');
$permision_confirm	= $permition_arr['permision']; // check approve permission

$permition_arr		= $obj_commonErr->get_permision_withApproval_reject($header_array['STATUS'],$header_array['LEVELS'],$userId,$programCode,'RunQuery');
$permision_reject	= $permition_arr['permision']; // check reject permission

$permition_arr		= $obj_commonErr->get_permision_withApproval_revise($header_array['STATUS'],$header_array['LEVELS'],$userId,$programCode,'RunQuery');
$permision_revise	= $permition_arr['permision']; // check revise permission
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sub Contract PO Report</title>

<script type="application/javascript" src="presentation/customerAndOperation/bulk/sub_contractor_po_agrement/addNew/sub_contract_po.js"></script>
</head>
<body>
<form id="frmRptSubContractPO" name="frmRptSubContractPO" method="post" autocomplete="off">
<table width="900" align="center">
	<tr>
    	<td colspan="3"><?php include 'reportHeader.php'; ?></td>
    </tr>
    <tr>
    	<td colspan="3" style="text-align:center">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="3" style="text-align:center"><strong>SUB CONTRACT PO AGREEMENT</strong></td>
    </tr>
    <tr>
    	<td>
        <table width="100%" border="0">
        <?php
			include "presentation/report_approve_status_and_buttons.php"
    	?>	
        </table>
        </td>
    </tr>
    
    <tr>
    	<td colspan="3">
        <table width="100%" border="0">
            <tr class="normalfnt">
                <td width="16%">SubContract No</td>
                <td width="1%">:</td>
                <td width="33%"><?php echo $serialNo.' / '.$serialYear; ?></td>
                <td width="16%">Date</td>
                <td width="1%">:</td>
                <td width="33%"><?php echo $header_array['DATE']; ?></td>
            </tr>
            <tr class="normalfnt">
              <td>Order No</td>
              <td>:</td>
              <td><?php echo $header_array['ORDER_NO'].' / '.$header_array['ORDER_YEAR']; ?></td>
              <td>Company Type</td>
              <td>:</td>
              <td><?php echo $header_array['COMPANY_TYPE']; ?></td>
            </tr>
            <tr class="normalfnt">
              <td>Sub Contractor</td>
              <td>:</td>
              <td><?php echo $header_array['SUB_CONTRACTOR']; ?></td>
              <td>Remarks</td>
              <td>:</td>
              <td rowspan="2" valign="top"><?php echo $header_array['REMARKS']; ?></td>
            </tr>
            <tr class="normalfnt">
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
    <tr>
    	<td colspan="3">
        	<table width="100%" border="0" class="rptBordered" id="tblMain">
            	<thead> 
                <tr class="normalfnt">
                  <th width="14%">Sales Order No</th>
                	<th width="13%">Graphic No</th>
                    <th width="14%">Style No</th>
                    <th width="16%">Job Type</th>
                    <th width="8%">Combo</th>
                    <th width="8%">Part</th>
                    <th width="8%">Qty</th>
                    <th width="9%">Price</th>
                    <th width="10%">Amount</th>
                </tr>
                </thead>
                <tbody>
                <?php
				$total	= 0;
				while($row = mysqli_fetch_array($detail_result))
				{
					$amount	= $row['QTY']*$row['PRICE'];
					$total	= $total + $amount;
				?>
                	<tr class="normalfnt">
                	  <td style="text-align:left"><?php echo $row['strSalesOrderNo']; ?></td>
                    	<td style="text-align:left"><?php echo $row['strGraphicNo']; ?></td>
                        <td style="text-align:left"><?php echo $row['strStyleNo']; ?></td>
                        <td style="text-align:left"><?php echo $row['JOB_TYPE']; ?></td>
                        <td style="text-align:left"><?php echo $row['strCombo']; ?></td>
                        <td style="text-align:right"><?php echo $row['intPart']; ?></td>
                        <td style="text-align:right"><?php echo $row['QTY']; ?></td>
                        <td style="text-align:right"><?php echo number_format($row['PRICE'],2); ?></td>
                        <td style="text-align:right"><?php echo number_format($amount,2); ?></td>
                    </tr>
                <?php
				}
				?>
                </tbody>
                <tfoot>
                	<tr class="normalfnt">
                	  <td colspan="8" style="text-align:right"><b>Total :&nbsp;</b></td>
                    	<td style="text-align:right"><?php echo number_format($total,2); ?></td>
                    </tr>
                </tfoot>
            </table>
        </td>
    </tr>
    <tr>
    	<td colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">
			<?php
            $creator		= $header_array['USER'];
            $createdDate	= $header_array['CREATED_DATE'];
            $resultA 		= getRptApproveDetails($serialNo,$serialYear); // get sub contract approve by details
            include "presentation/report_approvedBy_details.php"
            ?>
        </td>
    </tr>
    <tr>
    <td colspan="3" align="center" class="normalfntMid"><strong>Printed Date : </strong><?php echo date('Y-m-d H:i:s');?></td>
    </tr>
</table>
</form>
</body>
<?php
function get_header($serialNo,$serialYear)
{
	global $db;
	global $obj_trn_order_sub_contract_header;
	
	$cols	= "	SUB_CONTRACT_NO,
				SUB_CONTRACT_YEAR,
				ORDER_NO,
				ORDER_YEAR,
				trn_order_sub_contract_header.STATUS,
				LEVELS AS LEVELS,
				DATE,
				trn_order_sub_contract_header.CREATED_DATE,
				sys_users.strUserName AS USER,
				IF(COMPANY_TO_TYPE=0,'Internal','External') AS COMPANY_TYPE,
				IF(COMPANY_TO_TYPE=0,MC.strName,MS.SUB_CONTRACTOR_NAME) AS SUB_CONTRACTOR,
				REMARKS ";
	
	$join	= " INNER JOIN sys_users ON trn_order_sub_contract_header.CREATED_BY = sys_users.intUserId
				LEFT JOIN mst_companies MC ON MC.intId = trn_order_sub_contract_header.SUB_CONTRACTOR_ID
				LEFT JOIN mst_subcontractor MS ON MS.SUB_CONTRACTOR_ID = trn_order_sub_contract_header.SUB_CONTRACTOR_ID ";
	
	$where	= " SUB_CONTRACT_NO = '".$serialNo."' AND
				SUB_CONTRACT_YEAR = '".$serialYear."' ";
	
	$result = $obj_trn_order_sub_contract_header->select($cols,$join,$where, $order = null, $limit = null);	
	$row	= mysqli_fetch_array($result);

	return $row;
}
function get_Detail($serialNo,$serialYear)
{
	global $db;
	global $obj_trn_order_sub_contract_details;
	
	$cols	= "	SCJT.NAME AS JOB_TYPE,
				OD.strSalesOrderNo,
				OD.strStyleNo,
				OD.strGraphicNo,
				OD.strCombo,
				OD.intPart,
				trn_order_sub_contract_details.QTY,
				trn_order_sub_contract_details.PRICE ";
	
	$join	= " INNER JOIN trn_order_sub_contract_header SCH ON SCH.SUB_CONTRACT_NO=trn_order_sub_contract_details.SUB_CONTRACT_NO AND
				SCH.SUB_CONTRACT_YEAR=trn_order_sub_contract_details.SUB_CONTRACT_YEAR
				INNER JOIN trn_orderdetails OD ON OD.intOrderNo=SCH.ORDER_NO AND
				OD.intOrderYear=SCH.ORDER_YEAR AND
				OD.intSalesOrderId=trn_order_sub_contract_details.SALES_ORDER_ID
				INNER JOIN mst_sub_contract_job_types SCJT ON SCJT.ID=trn_order_sub_contract_details.SUB_CONTR_JOB_ID ";
				
	$where	= " trn_order_sub_contract_details.SUB_CONTRACT_NO = '".$serialNo."' AND
				trn_order_sub_contract_details.SUB_CONTRACT_YEAR = '".$serialYear."' ";
	
	$result = $obj_trn_order_sub_contract_details->select($cols,$join,$where, $order = null, $limit = null);	
	return $result;	
}
function getRptApproveDetails($serialNo,$serialYear)
{
	global $db;
	global $obj_trn_order_sub_contract_approved_by;
	
	$db->connect();
	
	$cols	= "	trn_order_sub_contract_approved_by.APPROVE_DATE AS dtApprovedDate ,
				sys_users.strUserName AS UserName,
				trn_order_sub_contract_approved_by.APPROVE_LEVEL AS intApproveLevelNo ";
	
	$join	= " INNER JOIN sys_users ON trn_order_sub_contract_approved_by.APPROVE_BY = sys_users.intUserId ";
				
	$where	= " trn_order_sub_contract_approved_by.SUB_CONTRACT_NO = '".$serialNo."' AND
				trn_order_sub_contract_approved_by.SUB_CONTRACT_YEAR = '".$serialYear."' ";
	
	$order	= " trn_order_sub_contract_approved_by.APPROVE_DATE ASC";
				
	$result = $obj_trn_order_sub_contract_approved_by->select($cols,$join,$where,$order, $limit = null);	
	$db->disconnect();
	
	return $result;	
}
?>
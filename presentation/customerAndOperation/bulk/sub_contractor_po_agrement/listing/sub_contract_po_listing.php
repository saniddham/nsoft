<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$session_locationId = $sessions->getLocationId();
$session_companyId 	= $sessions->getCompanyId();
$intUser  			= $sessions->getUserId();

$programCode		= 'P0871';

require_once("libraries/jqgrid2/inc/jqgrid_dist.php");

//BEGIN - ADD DEFAULT WHERE STRING WHEN FORM LOADING {
$arr =  json_decode($_REQUEST['filters'],true);

$arr = $arr['rules'];

$where_string = '';
$where_array = array(
					'Status'=>'tb1.STATUS',
					'CONCAT_SUB_CONTRACT_NO'=>"CONCAT(tb1.SUB_CONTRACT_NO,' / ',tb1.SUB_CONTRACT_YEAR)",
					'CONCAT_ORDER_NO'=>"CONCAT(tb1.ORDER_NO,' / ',tb1.ORDER_YEAR)",
					'companyType'=>"if(tb1.COMPANY_TO_TYPE=0,'Internal','External')",
					'subContractor'=>"if(tb1.COMPANY_TO_TYPE=0,MC.strName,MS.SUB_CONTRACTOR_NAME)",
					'REMARKS'=>'tb1.REMARKS',
					'DATE'=>'tb1.DATE'
					);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Revised'=>'-1','Cancelled'=>'-2','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}

if(!count($arr)>0)					 
		$where_string .= "AND tb1.DATE = '".date('Y-m-d')."'";
//END }

$approveLevel 							= (int)getMaxApproveLevel();

$sql = "SELECT SUB_1.* FROM
			(SELECT
				CONCAT(tb1.SUB_CONTRACT_NO,' / ',tb1.SUB_CONTRACT_YEAR)	AS CONCAT_SUB_CONTRACT_NO,
				tb1.SUB_CONTRACT_NO,
				tb1.SUB_CONTRACT_YEAR,
				CONCAT(tb1.ORDER_NO,' / ',tb1.ORDER_YEAR)	AS CONCAT_ORDER_NO,
				tb1.ORDER_NO,
				tb1.ORDER_YEAR,
				if(tb1.COMPANY_TO_TYPE=0,'Internal','External') as companyType,
				if(tb1.COMPANY_TO_TYPE=0,MC.strName,MS.SUB_CONTRACTOR_NAME) as subContractor,
				tb1.REMARKS,
				tb1.DATE,
				tb1.LEVELS,
				tb1.CREATED_BY,
				tb1.CREATED_DATE,
				if(tb1.STATUS=1,'Approved',if(tb1.STATUS=0,'Rejected',if(tb1.STATUS=-1,'Revised',if(tb1.STATUS=-2,'Cancelled','Pending')))) as Status,
				sys_users.strUserName AS CREATOR,
				IFNULL((
					SELECT
					concat(sys_users.strUserName,'(',max(trn_order_sub_contract_approved_by.APPROVE_DATE),')' )
					FROM
					trn_order_sub_contract_approved_by
					Inner Join sys_users ON trn_order_sub_contract_approved_by.APPROVE_BY = sys_users.intUserId
					WHERE
					trn_order_sub_contract_approved_by.SUB_CONTRACT_NO  = tb1.SUB_CONTRACT_NO AND
					trn_order_sub_contract_approved_by.SUB_CONTRACT_YEAR =  tb1.SUB_CONTRACT_YEAR AND
					trn_order_sub_contract_approved_by.APPROVE_LEVEL = '1' AND
					trn_order_sub_contract_approved_by.STATUS =  '0'
				),IF(((SELECT
					menupermision.int1Approval 
					FROM menupermision 
					Inner Join menus ON menupermision.intMenuId = menus.intId
					WHERE
					menus.strCode = '$programCode' AND
					menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
								$approval	= "2nd_Approval";
							}
							else if($i==3){
								$approval	= "3rd_Approval";
							}
							else {
								$approval	= $i."th_Approval";
							}
							
							
						$sql .= "IFNULL((
								SELECT
								concat(sys_users.strUserName,'(',max(trn_order_sub_contract_approved_by.APPROVE_DATE),')' )
								FROM
								trn_order_sub_contract_approved_by
								Inner Join sys_users ON trn_order_sub_contract_approved_by.APPROVE_BY = sys_users.intUserId
								WHERE
								trn_order_sub_contract_approved_by.SUB_CONTRACT_NO  = tb1.SUB_CONTRACT_NO AND
								trn_order_sub_contract_approved_by.SUB_CONTRACT_YEAR =  tb1.SUB_CONTRACT_YEAR AND
								trn_order_sub_contract_approved_by.APPROVE_LEVEL =  '$i' AND
								trn_order_sub_contract_approved_by.STATUS =  '0' 
							),
							IF(
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.STATUS>1) AND (tb1.STATUS<=tb1.LEVELS) AND ((SELECT
								concat(sys_users.strUserName )
								FROM
								trn_order_sub_contract_approved_by
								Inner Join sys_users ON trn_order_sub_contract_approved_by.APPROVE_BY = sys_users.intUserId
								WHERE
								trn_order_sub_contract_approved_by.SUB_CONTRACT_NO  = tb1.SUB_CONTRACT_NO AND
								trn_order_sub_contract_approved_by.SUB_CONTRACT_YEAR =  tb1.SUB_CONTRACT_YEAR AND
								trn_order_sub_contract_approved_by.APPROVE_LEVEL =  ($i-1) AND 
								trn_order_sub_contract_approved_by.STATUS='0' )<>'')),
								
								'Approve',
								 if($i>tb1.LEVELS,'-----',''))
								
								) as `".$approval."`, "; 
									
								}
								
							$sql .= "IFNULL((
								SELECT
								concat(sys_users.strUserName,'(',max(trn_order_sub_contract_approved_by.APPROVE_DATE),')' )
								FROM
								trn_order_sub_contract_approved_by
								Inner Join sys_users ON trn_order_sub_contract_approved_by.APPROVE_BY = sys_users.intUserId
								WHERE
								trn_order_sub_contract_approved_by.SUB_CONTRACT_NO  = tb1.SUB_CONTRACT_NO AND
								trn_order_sub_contract_approved_by.SUB_CONTRACT_YEAR =  tb1.SUB_CONTRACT_YEAR AND
								trn_order_sub_contract_approved_by.APPROVE_LEVEL =  '0' AND
								trn_order_sub_contract_approved_by.STATUS =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS<=tb1.LEVELS AND 
								tb1.STATUS>1),'Reject', '')) as `Reject`,";
								
							$sql .= "IFNULL((
								SELECT
								concat(sys_users.strUserName,'(',max(trn_order_sub_contract_approved_by.APPROVE_DATE),')' )
								FROM
								trn_order_sub_contract_approved_by
								Inner Join sys_users ON trn_order_sub_contract_approved_by.APPROVE_BY = sys_users.intUserId
								WHERE
								trn_order_sub_contract_approved_by.SUB_CONTRACT_NO  = tb1.SUB_CONTRACT_NO AND
								trn_order_sub_contract_approved_by.SUB_CONTRACT_YEAR =  tb1.SUB_CONTRACT_YEAR AND
								trn_order_sub_contract_approved_by.APPROVE_LEVEL =  '-2' AND
								trn_order_sub_contract_approved_by.STATUS =  '0'
							),IF(((SELECT
								menupermision.intCancel 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND 
								tb1.STATUS=1),'Cancel', '')) as `Cancel`,";
							
							$sql .= "IFNULL((
								SELECT
								concat(sys_users.strUserName,'(',max(trn_order_sub_contract_approved_by.APPROVE_DATE),')' )
								FROM
								trn_order_sub_contract_approved_by
								Inner Join sys_users ON trn_order_sub_contract_approved_by.APPROVE_BY = sys_users.intUserId
								WHERE
								trn_order_sub_contract_approved_by.SUB_CONTRACT_NO  = tb1.SUB_CONTRACT_NO AND
								trn_order_sub_contract_approved_by.SUB_CONTRACT_YEAR =  tb1.SUB_CONTRACT_YEAR AND
								trn_order_sub_contract_approved_by.APPROVE_LEVEL =  '-1' AND
								trn_order_sub_contract_approved_by.STATUS =  '0'
							),IF(((
								SELECT
								menupermision.intRevise 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND 
								tb1.STATUS=1),'Revise', '')) as `Revise`,";
								
							$sql .= "'View' as `View`   
				
FROM trn_order_sub_contract_header as tb1 
				LEFT JOIN mst_companies MC ON MC.intId=tb1.SUB_CONTRACTOR_ID
				LEFT JOIN mst_subcontractor MS ON MS.SUB_CONTRACTOR_ID=tb1.SUB_CONTRACTOR_ID
				INNER JOIN sys_users ON tb1.CREATED_BY = sys_users.intUserId
				WHERE tb1.LOCATION_ID='$session_locationId'
					$where_string
		)  
		AS SUB_1 WHERE 1=1";
//echo $sql;
$jq = new jqgrid('',$db);	

$cols	= array();
$col	= array();

$col["title"] 				= "Status";
$col["name"] 				= "Status";
$col["width"] 				= "3"; 						
$col["align"] 				= "center";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col["stype"] 				= "select";
$str 						= ":All;Pending:Pending;Approved:Approved;Rejected:Rejected;Revised:Revised" ;
$col["searchoptions"] 		= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "subContractNo";
$col["name"] 				= "SUB_CONTRACT_NO";
$col["sortable"] 			= true; 					
$col["search"] 				= true;
$col["hidden"]  			= true;				
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "subContractYear";
$col["name"] 				= "SUB_CONTRACT_YEAR";
$col["sortable"] 			= true; 					
$col["search"] 				= true;
$col["hidden"]  			= true;					
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Serial No";
$col["name"] 				= "CONCAT_SUB_CONTRACT_NO";
$col["width"] 				= "3";
$col["align"] 				= "center";
$col["sortable"] 			= true; 					
$col["search"] 				= true; 					
$col["editable"] 			= false;
$col['link']				= '?q=871&serialNo={SUB_CONTRACT_NO}&year={SUB_CONTRACT_YEAR}';
$col["linkoptions"] 		= "target='sub_contract_po.php'";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Order No";
$col["name"] 				= "CONCAT_ORDER_NO";
$col["width"] 				= "3"; 						
$col["align"] 				= "center";
$col["sortable"] 			= true; 						
$col["search"] 				= true; 					
$col["editable"] 			= false;
$cols[] 					= $col;
$col						= NULL;

$col["title"] 				= "Company Type";
$col["name"] 				= "companyType";
$col["width"] 				= "3";
$col["align"] 				= "left";
$col["sortable"] 			= true; 					
$col["search"] 				= true; 					
$col["editable"] 			= false;
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Sub Contractor";
$col["name"] 				= "subContractor";
$col["width"] 				= "6";
$col["align"] 				= "left";
$col["sortable"] 			= true; 					
$col["search"] 				= true; 					
$col["editable"] 			= false;
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Remarks";
$col["name"] 				= "REMARKS";
$col["width"] 				= "4";
$col["align"] 				= "left";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Date";
$col["name"] 				= "DATE";
$col["width"] 				= "3";
$col["align"] 				= "center";
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "1st Approval";
$col["name"] 				= "1st_Approval";
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= '?q=907&serialNo={SUB_CONTRACT_NO}&serialYear={SUB_CONTRACT_YEAR}&Mode=Confirm';
$col["linkoptions"] 		= "target='rpt_sub_contract_po.php'";
$col['linkName']			= 'Approve';
$cols[] 					= $col;	
$col						= NULL;

for($i=2; $i<=$approveLevel; $i++)
{
	if($i==2){
		$ap		= "2nd Approval";
		$ap1	= "2nd_Approval";
	}
	else if($i==3){
		$ap		= "3rd Approval";
		$ap1	= "3rd_Approval";
	}
	else {
		$ap		= $i."th Approval";
		$ap1	= $i."th_Approval";
	}

$col["title"] 				= $ap; 
$col["name"] 				= $ap1; 
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= '?q=907&serialNo={SUB_CONTRACT_NO}&serialYear={SUB_CONTRACT_YEAR}&Mode=Confirm';
$col["linkoptions"] 		= "target='rpt_sub_contract_po.php'";
$col['linkName']			= 'Approve';
$cols[] 					= $col;	
$col						= NULL;
}

$col["title"] 				= 'Revise'; 
$col["name"] 				= 'Revise'; 
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= '?q=907&serialNo={SUB_CONTRACT_NO}&serialYear={SUB_CONTRACT_YEAR}&Mode=Revise';
$col["linkoptions"] 		= "target='rpt_sub_contract_po.php'";
$col['linkName']			= 'Revise';
$cols[] 					= $col;	
$col						= NULL;

$col["title"] 				= "Report";
$col["name"] 				= "View";
$col["width"] 				= "2";
$col["align"] 				= "center"; 
$col["sortable"]			= false;
$col["editable"] 			= false; 	
$col["search"] 				= false; 
$col['link']				= '?q=907&serialNo={SUB_CONTRACT_NO}&serialYear={SUB_CONTRACT_YEAR}';
$col["linkoptions"] 		= "target='rpt_sub_contract_po.php'";
$col['linkName']			= 'View';
$cols[] 					= $col;	
$col						= NULL;

$grid["caption"] 			= "Sub Contract PO Listing";
$grid["multiselect"] 		= false;
$grid["rowNum"] 			= 20; 
$grid["sortname"] 			= 'SUB_CONTRACT_YEAR,SUB_CONTRACT_NO'; 
$grid["sortorder"] 			= "DESC"; 
$grid["autowidth"] 			= true; 
$grid["multiselect"] 		= false; 
$grid["search"] 			= true; 
$grid["postData"] 			= array("filters" => $sarr ); 
$grid["export"] 			= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

/*$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"}
     ]
}
SEARCH_JSON;

$grid["postData"] = array("filters" => $sarr ); */

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");

echo $out;

function getMaxApproveLevel()
{
	global $db;
	$appLevel = 0;
	
	$sqlp = "SELECT
			MAX(trn_order_sub_contract_header.LEVELS) AS appLevel
			FROM trn_order_sub_contract_header";					
	
	$resultp 	= $db->RunQuery($sqlp);
	$rowp		= mysqli_fetch_array($resultp);
	
	return $rowp['appLevel'];
}
?>
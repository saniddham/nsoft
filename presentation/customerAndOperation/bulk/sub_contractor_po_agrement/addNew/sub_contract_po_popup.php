<?php
ini_set('display_errors',0);
session_start();
$backwardseperator 		= "../../../../../";
//BEGIN - INCLUDE FILES {
require_once ("{$backwardseperator}dataAccess/DBManager2.php"); //connection

include_once ("../../../../../class/tables/trn_orderdetails.php");
include_once ("../../../../../class/tables/trn_order_sub_contract_details.php");
//END 	- INCLUDE FILES }

//BEGIN - CREATE PARAMETERS {
$db										= new DBManager2();
$obj_trn_orderdetails 					= new trn_orderdetails($db);
$obj_trn_order_sub_contract_details		= new trn_order_sub_contract_details($db);
//END 	- CREATE PARAMETERS }

//BEGIN - SET PARAMETERS {
$orderNo 				= $_REQUEST['OrderNo'];
$orderYear 				= $_REQUEST['OrderYear'];
//END 	- SET PARAMETERS }

//BEGIN - {
$db->connect();

$cols	= "intSalesOrderId		AS SALES_ORDER_ID,
			strSalesOrderNo		AS SALES_ORDER_NO,
			strStyleNo			AS STYLE_NO,
			strGraphicNo		AS GRAPHIC_NO,
			intPart				AS PART_NO,
			strCombo			AS COMBO,
			intQty				AS QTY";
				
$where	= "intOrderYear = $orderYear 
			AND intOrderNo = $orderNo ";

$result = $obj_trn_orderdetails->select($cols,$join = null,$where, $order = null, $limit = null);
$db->disconnect();
//END	- }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sub Contract Sales Orders</title>
<!--<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/button.css" rel="stylesheet" type="text/css" />-->
<script src="presentation/customerAndOperation/bulk/sub_contractor_po_agrement/addNew/sub_contract_po.js" type="text/javascript"></script>
</head>

<body >
<form id="frmSubContractOrders_popup" name="frmSubContractOrders_popup" method="post" action="">
  <div align="center">
    <div class="trans_layoutL" style="width:780px">
      <div class="trans_text">Sub Contract Sales Orders</div>
      <table width="800" border="0" align="center" bgcolor="#FFFFFF">
        
          <td width="100%"><table width="100%" border="0">
              <tr>
                <td align="right"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="normalfnt">
                  <tr>
                    <td width="10%">Order No</td>
                    <td width="1%">:</td>
                    <td width="89%" id="orderNo"><?php echo $orderNo.'/'.$orderYear?></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td><table width="100%"  class="bordered" id="tblPOs" >
                    <tr class="">
                      <th width="5%" ><input type="checkbox" id="chkCheckAll"/></th>
                      <th width="23%" >Sales Order</th>
                      <th width="16%" >Style No</th>
                      <th width="14%" >Graphic No</th>
                      <th width="16%" >Part No</th>
                      <th width="16%" >Combo </th>
                      <th width="10%" >PO Qty</th>
                    </tr>
                   <?php while($row = mysqli_fetch_array($result))
				   {
					   $orderQty		= round($row['QTY'],2);
					   
					   $approvedPOQty	= getApprovedPOQty($orderNo,$orderYear,$row['SALES_ORDER_ID']);
					   $qty				= $orderQty - $approvedPOQty;
					   if($qty<=0)
					   	continue;
					   ?>
                    <tr class="normalfnt" >
                        <td class="td_check" style="text-align:center"><input type="checkbox"/></td>
                        <td class="td_salesOrderId" id="<?php echo $row['SALES_ORDER_ID']?>"><?php echo $row['SALES_ORDER_NO']?></td>
                        <td class="td_styleNo" ><?php echo $row['STYLE_NO']?></td>
                        <td class="td_graphicNo" ><?php echo $row['GRAPHIC_NO']?></td>
                        <td class="td_partNo" ><?php echo $row['PART_NO']?></td>
                        <td class="td_combo"><?php echo $row['COMBO']?></td>
                        <td class="td_qty" style="text-align:right"><?php echo number_format($qty,2)?></td>
                    </tr>
                    <?php } ?>
                  </table></td>
              </tr>
              <tr>
                <td align="center" >&nbsp;</td>
              </tr>
              <tr>
                <td align="center" height="25"><a class="button white medium" id="butAddPopupItem">Add</a><a class="button white medium" id="butClose">Close</a></td>
              </tr>
            </table></td>
      </table>
    </div>
  </div>
</form>
</body>
</html>
<?php
function getApprovedPOQty($orderNo,$orderYear,$salesOrderId)
{
	global $db;
	global $obj_trn_order_sub_contract_details;
	
	$cols		= "SUM(trn_order_sub_contract_details.QTY) AS PO_QTY";
	
	$join 		= "INNER JOIN trn_order_sub_contract_header H
					ON H.SUB_CONTRACT_NO = trn_order_sub_contract_details.SUB_CONTRACT_NO
					AND H.SUB_CONTRACT_YEAR = trn_order_sub_contract_details.SUB_CONTRACT_YEAR ";
					
	$where		= "H.STATUS = 1
					AND H.ORDER_NO = '$orderNo'
					AND H.ORDER_YEAR = '$orderYear'
					AND trn_order_sub_contract_details.SALES_ORDER_ID = '$salesOrderId' ";
	
	$db->connect();
	$result1 	= $obj_trn_order_sub_contract_details->select($cols,$join,$where, $order = null, $limit = null);	
	$db->disconnect();
	$row1		= mysqli_fetch_array($result1);
	return $row1['PO_QTY'];
}
?>
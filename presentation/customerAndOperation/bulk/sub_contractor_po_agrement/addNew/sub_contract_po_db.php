<?php
ini_set('display_errors',0);
session_start();
$backwardseperator 	= "../../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$locationId 		= $_SESSION['CompanyID'];
$companyId 			= $_SESSION['headCompanyId'];

$requestType 		= $_REQUEST['requestType'];
$programName		= 'Sub Contractor PO';
$programCode		= 'P0871';

$savedStatus		= true;
$savedMasseged		= '';
$error_sql			= '';

require_once "../../../../../dataAccess/DBManager2.php"; //connection
include_once "../../../../../class/tables/trn_order_sub_contract_header.php";
include_once "../../../../../class/tables/trn_order_sub_contract_approved_by.php";
require_once "../../../../../class/cls_commonErrorHandeling_get.php";
include_once "../../../../../class/tables/trn_orderheader.php";
include_once "../../../../../class/tables/trn_orderdetails.php";
include_once "../../../../../class/cls_commonFunctions_get.php";
include_once "../../../../../class/tables/trn_order_sub_contract_details.php";
require_once $_SESSION['ROOT_PATH']."class/customerAndOperation/bulk/sub_contractor_po_agrement/cls_sub_contractor_po_agrement_get.php";
include_once "../../../../../class/customerAndOperation/cls_customer_and_operation.php";
include_once "../../../../../class/tables/mst_subcontractor.php";
include_once "../../../../../class/tables/trn_sampleinfomations_prices_sub_contract_job.php";
include_once "../../../../../class/tables/trn_order_sub_contract_size_wise_qty.php";
include_once "../../../../../class/tables/trn_sampleinfomations_details.php";
include_once "../../../../../class/dateTime.php";

$db													= new DBManager2();
$obj_trn_order_sub_contract_header 					= new trn_order_sub_contract_header($db);
$obj_trn_order_sub_contract_approved_by 			= new trn_order_sub_contract_approved_by($db);
$obj_commonErr										= new cls_commonErrorHandeling_get($db);
$obj_trn_orderheader 								= new trn_orderheader($db);
$obj_trn_orderdetails 								= new trn_orderdetails($db);
$obj_commonFunctions_get							= new cls_commonFunctions_get($db);
$obj_trn_order_sub_contract_details					= new trn_order_sub_contract_details($db);
$obj_data_get										= new cls_sub_contractor_po_agrement_get($db);
$obj_customer_and_operation							= new cls_customer_and_operation($db);
$obj_mst_subcontractor								= new mst_subcontractor($db);
$obj_trn_sampleinfomations_prices_sub_contract_job 	= new trn_sampleinfomations_prices_sub_contract_job($db);
$obj_trn_order_sub_contract_size_wise_qty 			= new trn_order_sub_contract_size_wise_qty($db);
$obj_trn_sampleinfomations_details					= new trn_sampleinfomations_details($db);
$obj_dateTime										= new dateTimes($db);

if($requestType=='URLSave')
{
	$headerArray			= json_decode($_REQUEST['HeaderArray'],true);
	$detailArray			= json_decode($_REQUEST['DetailArray'],true);	
	
	$db->connect();//open connection.

//BEGIN - PERMISSION VALIDATE BEFORE SAVING {
	$result = validateBeforeSave($subContractNo,$subContractYear,$programCode,$userId);
	if(!$result['type'] && $savedStatus)
	{
		$savedStatus		= false;
		$savedMasseged 		= $result['msg'];
	}
//END	- PERMISSION VALIDATE BEFORE SAVING }

//BEGIN - GET INSERT SERIAL NO FROM SYS_NO TABLE {
	$result 				= $obj_commonFunctions_get->GetSystemMaxNoNew('SUB_CONTRACT_PO_NO',$companyId,'RunQuery');	
	if(!$result['type'] && $savedStatus)
	{	
		$savedStatus		= false;
		$savedMasseged 		= $result['errorMsg'];
		$error_sql			= $result['errorSql'];	
	}
	else
	{
		$subContractNo  	= $result['max_no'];
		$subContractYear  	= date('Y');
	}
//END	- GET INSERT SERIAL NO FROM SYS_NO TABLE }

//BEGIN - INSERT DATE IN SEPERATE FUNCTION {	
	$result					= insertHeader($subContractNo,$subContractYear,$headerArray,$programCode,$userId,$locationId);
	if(!$result['type'] && $savedStatus)
	{
		$savedStatus		= false;
		$savedMasseged 		= $result['msg'];
		$error_sql			= $result['sql'];	
	}
//END 	- INSERT DATE IN SEPERATE FUNCTION }

//BEGIN - INSERT DETAILS {
	$result					= insertDetails($subContractNo,$subContractYear,$headerArray,$detailArray);
	if(!$result['type'] && $savedStatus)
	{
		$savedStatus		= false;
		$savedMasseged 		= $result['msg'];
		$error_sql			= $result['sql'];	
	}
//END 	- INSERT DETAILS }

	if($savedStatus)
	{
		$db->commit();
		$response['type'] 	= "pass";
		$response['msg'] 	= "Saved successfully.";
		$response['No'] 	= $subContractNo;
		$response['Year'] 	= $subContractYear;
	}
	else
	{
		$db->rollback();
		$response['type'] 	= "fail";
		$response['msg'] 	= $savedMasseged;
		$response['sql']	= $error_sql;
	}
	echo json_encode($response);
	$db->disconnect();//close connection.
}

if($requestType=='URLUpdate')
{
	$headerArray			= json_decode($_REQUEST['HeaderArray'],true);	
	$detailArray			= json_decode($_REQUEST['DetailArray'],true);	
	
	$db->connect(); //open connection.

//BEGIN - PERMISSION VALIDATE BEFORE UPDATION {
	$result = validateBeforeSave($headerArray['SubContractNo'],$headerArray['SubContractYear'],$programCode,$userId);
	if(!$result['type'] && $savedStatus)
	{
		$savedStatus		= false;
		$savedMasseged 		= $result['msg'];
	}
//END	- PERMISSION VALIDATE BEFORE UPDATION }

//BEGIN - INSERT DATE IN SEPERATE FUNCTION {	
	$result					= updateHeader($headerArray,$programCode,$userId,$locationId);
	if(!$result['type'] && $savedStatus)
	{
		$savedStatus		= false;
		$savedMasseged 		= $result['msg'];
		$error_sql			= $result['sql'];	
	}
//END 	- INSERT DATE IN SEPERATE FUNCTION }		

//BEGIN - DELETE DETAILS {
	$result					= deleteDetails($headerArray);
	if(!$result['type'] && $savedStatus)
	{
		$savedStatus		= false;
		$savedMasseged 		= $result['msg'];
		$error_sql			= $result['sql'];	
	}
//END 	- DELETE DETAILS }

//BEGIN - INSERT DETAILS {
	$result					= insertDetails($headerArray['SubContractNo'],$headerArray['SubContractYear'],$headerArray,$detailArray);
	if(!$result['type'] && $savedStatus)
	{
		$savedStatus		= false;
		$savedMasseged 		= $result['msg'];
		$error_sql			= $result['sql'];	
	}
//END 	- INSERT DETAILS }

//BEGIN - UPDATE APPROVED BY STATUS AS 0
	$result					= updateMaxStatus($headerArray['SubContractNo'],$headerArray['SubContractYear']);
	if(!$result['type'] && $savedStatus)
	{
		$savedStatus		= false;
		$savedMasseged 		= $result['msg'];
		$error_sql			= $result['sql'];	
	}
//END 	- UPDATE APPROVED BY STATUS AS 0
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 	= "pass";
		$response['msg'] 	= "Update successfully.";
		$response['No'] 	= $headerArray['SubContractNo'];
		$response['Year'] 	= $headerArray['SubContractYear'];
	}
	else
	{
		$db->rollback();
		$response['type'] 	= "fail";
		$response['msg'] 	= $savedMasseged;
		$response['sql']	= $error_sql;
	}
	echo json_encode($response);
	$db->disconnect(); //close connection.
}

if($requestType=='URLSearchOrder')
{
	$db->connect();
	$headerArray		= json_decode($_REQUEST['HeaderArray'],true);
	
	$cols	= "CU.strName 						AS CUSTOMER_NAME,
			   PT.strDescription 				AS PAYMENT_TERM,
			   FC.strDescription 				AS CURRENCY,
			   trn_orderheader.strContactPerson	AS CONTACT_PERSON,
			   SU.strUserName					AS MARKETER,
			   CL.strName						AS CUSTOMER_LOCATION,
			   trn_orderheader.intStatus		AS STATUS";
	
	$join	= "INNER JOIN mst_customer CU
					ON CU.intId = trn_orderheader.intCustomer
			   INNER JOIN mst_financepaymentsterms PT
			   		ON PT.intId = trn_orderheader.intPaymentTerm
	           INNER JOIN mst_financecurrency FC
			   		ON FC.intId = trn_orderheader.intCurrency
			   INNER JOIN sys_users SU
			   		ON SU.intUserId = trn_orderheader.intMarketer
			   INNER JOIN mst_customer_locations_header CL
			   		ON CL.intId = trn_orderheader.intCustomerLocation";
					
	$where	= "intOrderYear = ".$headerArray['OrderYear']." 
				AND intOrderNo = ".$headerArray['OrderNo'];
	
	$result = $obj_trn_orderheader->select($cols,$join,$where, $order = null, $limit = null);	
	while($row = mysqli_fetch_array($result))
	{
		$booOrderAvailable			= true;
		$data['TYPE']				= true;
		
		$data['CUSTOMER_NAME']		= $row['CUSTOMER_NAME'];
		$data['PAYMENT_TERM']		= $row['PAYMENT_TERM'];
		$data['CURRENCY']			= $row['CURRENCY'];
		$data['CONTACT_PERSON']		= $row['CONTACT_PERSON'];
		$data['MARKETER']			= $row['MARKETER'];
		$data['CUSTOMER_LOCATION']	= $row['CUSTOMER_LOCATION'];
		
		if($row['STATUS']!='1')
		{
			$data['TYPE']			= false;
			$data['MSG']			= "'Order No' not approved in the system.";
		}
	}	
	if(!$booOrderAvailable)
	{
		$data['TYPE']				= false;
		$data['MSG']				= "'Order No' not available in the system.";
	}
	echo json_encode($data);
	$db->disconnect();
}

if($requestType=="URLAddPopupItem")
{
	$db->connect();
	
	$orderNo		= explode('/',$_REQUEST['OrderNo']);
	$salesOrder		= $_REQUEST['SalesOrder'];
	
	$cols	= "intSalesOrderId		AS SALES_ORDER_ID,
				strSalesOrderNo		AS SALES_ORDER_NO,
				strStyleNo			AS STYLE_NO,
				strGraphicNo		AS GRAPHIC_NO,
				intSampleNo			AS SAMPLE_NO,
				intSampleYear		AS SAMPLE_YEAR,
				CONCAT(intSampleNo,'/',intSampleYear)	AS CONCAT_SAMPLE_NO,
				strPrintName		AS PLACEMENT,
				intPart				AS PART_NO,
				strCombo			AS COMBO,
				intRevisionNo		AS REVISION_NO,
				intQty				AS QTY";
	
	$join	= "";
	
	$where	= "intOrderYear = $orderNo[1] 
			AND intOrderNo = $orderNo[0]
			AND intSalesOrderId IN($salesOrder) ";
	
	$result = $obj_trn_orderdetails->select($cols,$join,$where, $order = null, $limit = null);	
	while($row = mysqli_fetch_array($result))
	{
		$groundColor	= getGroundColor($row['SAMPLE_NO'],$row['SAMPLE_YEAR'],$row['PLACEMENT'],$row['COMBO'],$row['REVISION_NO']);
		$orderQty		= round($row['QTY'],2);					   
	   	$approvedPOQty	= getApprovedPOQty($orderNo[0],$orderNo[1],$row['SALES_ORDER_ID']);
	   	$qty			= $orderQty - $approvedPOQty;
		$html 		   .= createHTML($row['SALES_ORDER_ID'],$row['SALES_ORDER_NO'],$row['STYLE_NO'],$row['GRAPHIC_NO'],$row['CONCAT_SAMPLE_NO'],$row['PLACEMENT'],$row['PART_NO'],$row['COMBO'],$row['REVISION_NO'],$qty,$groundColor);
		//$html1[] = $html;
	}
	$response['html']	= $html;
	echo json_encode($response);
	$db->disconnect();
}

if($requestType=="URLloadSubContractor")
{
	$subContractType	= $_REQUEST['SubContractType'];
	
	$response['html']	= loadSubContractor($subContractType);	//load subcontractor.
	echo json_encode($response);
}

if($requestType=='URLloadPrice')
{
	$sampleNo			= explode('/',$_REQUEST['SampleNo']);
	$revisionNo			= $_REQUEST['RevisionNo'];
	$combo				= $_REQUEST['Combo']; 
	$printName			= $_REQUEST['PrintName']; 
	$jobType			= $_REQUEST['JobType']; 
	
	$response['price']	= getPrice($sampleNo,$revisionNo,$combo,$printName,$jobType);
	echo json_encode($response);
}

if($requestType=='URLSaveSizes')
{
	$headerArray			= json_decode($_REQUEST['HeaderArray'],true);
	$detailArray			= json_decode($_REQUEST['DetailArray'],true);
	
	$db->connect();

//BEGIN - DELETE DETAILS {
	$result					= deleteSizeWiseDetails($headerArray);
	if(!$result['type'] && $savedStatus)
	{
		$savedStatus		= false;
		$savedMasseged 		= $result['msg'];
		$error_sql			= $result['sql'];	
	}
//END 	- DELETE DETAILS }

//BEGIN - INSERT SIZE WISE SUB CONTRACT PO QTY.	
	$result 		= saveSizes($headerArray,$detailArray);
	if(!$result['type'] && $savedStatus)
	{
		$savedStatus		= false;
		$savedMasseged 		= $result['msg'];
		$error_sql			= $result['sql'];	
	}
//END 	- INSERT SIZE WISE SUB CONTRACT PO QTY.	

	if($savedStatus)
	{
		$db->commit();		
		$response['type'] 	= "pass";
		$response['msg'] 	= "Update successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 	= "fail";
		$response['msg'] 	= $savedMasseged;
		$response['sql']	= $error_sql;
	}
	$db->disconnect();
	echo json_encode($response);
}

if($requestType=='approve')
{
	$subContractNo		= $_REQUEST['serialNo'];
	$subContractYear	= $_REQUEST['serialYear'];
	
	$db->connect();
	
	$validateArr		= validateBeforeApprove($subContractNo,$subContractYear,$programCode,$userId); // check approve permission function
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateArr["msg"];
	}
	$validatesizeWise	= checkSizeWiseQtyAdd($subContractNo,$subContractYear); // validate whethee size wise qty add
	if($validatesizeWise['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validatesizeWise['msg'];
	}
	$validateQtyArr		= validateQty($subContractNo,$subContractYear); // validate sub contract qty
	if($validateQtyArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateQtyArr["msg"];
	}
	$resultUHSArr		= updateHeaderStatus($subContractNo,$subContractYear,''); // update header status function
	if(!$resultUHSArr['status'] && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultUHSArr['msg'];
		$error_sql		= $resultUHSArr['sql'];	
	}
	$resultAPArr		= approvedData($subContractNo,$subContractYear,$userId); // update approve by table
	if(!$resultAPArr['status'] && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultAPArr['msg'];
		$error_sql		= $resultAPArr['sql'];	
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 	= "pass";
		$response['msg'] 	= "Approved Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 	= "fail";
		$response['msg'] 	= $savedMasseged;
		$response['sql']	= $error_sql;
	}
	$db->disconnect();
	echo json_encode($response);
}
else if($requestType=='reject')
{
	$subContractNo		= $_REQUEST['serialNo'];
	$subContractYear	= $_REQUEST['serialYear'];
	
	$db->connect();
	
	$validateArr		= validateBeforeReject($subContractNo,$subContractYear,$programCode,$userId); // check reject permission function
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateArr["msg"];
	}
	$resultUHSArr		= updateHeaderStatus($subContractNo,$subContractYear,'0'); // update header status function
	if(!$resultUHSArr['status'] && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultUHSArr['msg'];
		$error_sql		= $resultUHSArr['sql'];	
	}
	$data  	= array('SUB_CONTRACT_NO'		=> $subContractNo,
					'SUB_CONTRACT_YEAR'		=> $subContractYear,
					'APPROVE_LEVEL'			=> "0",
					'APPROVE_BY'			=> $userId,
					'APPROVE_DATE'			=> $obj_dateTime->getCurruntDateTime(),
					'STATUS'				=> "0"
					);
	$resultAPArr		= $obj_trn_order_sub_contract_approved_by->insert($data);
	if(!$resultAPArr['status'] && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultAPArr['msg'];
	-	$error_sql		= $resultAPArr['sql'];	
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Rejected Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql']		= $error_sql;
	}
	$db->disconnect();
	echo json_encode($response);
}
else if($requestType=='revise')
{
	$subContractNo		= $_REQUEST['serialNo'];
	$subContractYear	= $_REQUEST['serialYear'];
	
	$db->connect();
	
	$validateArr		= validateBeforeRevise($subContractNo,$subContractYear,$programCode,$userId); // check revise permission function
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateArr["msg"];
	}
	$resultUHSArr		= updateHeaderStatus($subContractNo,$subContractYear,'-1'); // update header status function
	if(!$resultUHSArr['status'] && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultUHSArr['msg'];
		$error_sql		= $resultUHSArr['sql'];	
	}
	$data  	= array('SUB_CONTRACT_NO'		=> $subContractNo,
					'SUB_CONTRACT_YEAR'		=> $subContractYear,
					'APPROVE_LEVEL'			=> "-1",
					'APPROVE_BY'			=> $userId,
					'APPROVE_DATE'			=> $obj_dateTime->getCurruntDateTime(),
					'STATUS'				=> '0'
					);
	$resultAPArr		= $obj_trn_order_sub_contract_approved_by->insert($data);
	if(!$resultAPArr['status'] && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultAPArr['msg'];
		$error_sql		= $resultAPArr['sql'];	
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Revised Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql']		= $error_sql;
	}
	$db->disconnect();
	echo json_encode($response);
}
function validateBeforeApprove($subContractNo,$subContractYear,$programCode,$userId)
{
	global $obj_commonErr;
	global $obj_trn_order_sub_contract_header;
	
	$where	= 'SUB_CONTRACT_NO = "'.$subContractNo.'" AND SUB_CONTRACT_YEAR = "'.$subContractYear.'" ' ;
	
	$header_result	= $obj_trn_order_sub_contract_header->select('*',NULL,$where,NULL,NULL); // get header data
	$headerRow		= mysqli_fetch_array($header_result);	
	
	$validateArr 	= $obj_commonErr->get_permision_withApproval_confirm($headerRow['STATUS'],$headerRow['LEVELS'],$userId,$programCode,'RunQuery'); // get approve permission
	return  $validateArr;
}
function updateHeaderStatus($subContractNo,$subContractYear,$status)
{
	global $obj_trn_order_sub_contract_header;
	
	$where	= 'SUB_CONTRACT_NO = "'.$subContractNo.'" AND SUB_CONTRACT_YEAR = "'.$subContractYear.'" ' ;
	
	if($status=='') 
	{
		$data  		= array('STATUS' => -1);
		$resultArr	= $obj_trn_order_sub_contract_header->upgrade($data,$where); // upgrade header status
	}
	else
	{
		$data  		= array('STATUS' => $status);
		$resultArr	= $obj_trn_order_sub_contract_header->update($data,$where); // update header status
		
	}
	return  $resultArr;
}
function approvedData($subContractNo,$subContractYear,$userId)
{
	global $obj_trn_order_sub_contract_header;
	global $obj_trn_order_sub_contract_approved_by;
	global $obj_dateTime;
	
	$where	= 'SUB_CONTRACT_NO = "'.$subContractNo.'" AND SUB_CONTRACT_YEAR = "'.$subContractYear.'" ' ;
	
	$header_result	= $obj_trn_order_sub_contract_header->select('*',NULL,$where,NULL,NULL); // get header data
	$headerRow		= mysqli_fetch_array($header_result);	
	
	$approval		= $headerRow['LEVELS']+1-$headerRow['STATUS'];
	
	$data  	= array('SUB_CONTRACT_NO'		=> $subContractNo ,
					'SUB_CONTRACT_YEAR'		=> $subContractYear ,
					'APPROVE_LEVEL'			=> $approval ,
					'APPROVE_BY'			=> $userId ,
					'APPROVE_DATE'			=> $obj_dateTime->getCurruntDateTime(),
					'STATUS'				=> '0'
					);
	
	$resultArr		= $obj_trn_order_sub_contract_approved_by->insert($data); // insert data to approve by table
	return $resultArr;
}
function validateBeforeReject($subContractNo,$subContractYear,$programCode,$userId)
{
	global $obj_commonErr;
	global $obj_trn_order_sub_contract_header;
	
	$where	= 'SUB_CONTRACT_NO = "'.$subContractNo.'" AND SUB_CONTRACT_YEAR = "'.$subContractYear.'" ' ;
	
	$header_result	= $obj_trn_order_sub_contract_header->select('*',NULL,$where,NULL,NULL); // get header data
	$headerRow		= mysqli_fetch_array($header_result);
	
	$validateArr 	= $obj_commonErr->get_permision_withApproval_reject($headerRow['STATUS'],$headerRow['LEVELS'],$userId,$programCode,'RunQuery'); // get reject permission
	return  $validateArr;
	
}
function validateBeforeRevise($subContractNo,$subContractYear,$programCode,$userId)
{
	global $obj_commonErr;
	global $obj_trn_order_sub_contract_header;
	
	$where	= 'SUB_CONTRACT_NO = "'.$subContractNo.'" AND SUB_CONTRACT_YEAR = "'.$subContractYear.'" ' ;
	
	$header_result	= $obj_trn_order_sub_contract_header->select('*',NULL,$where,NULL,NULL); // get header data
	$headerRow		= mysqli_fetch_array($header_result);
	
	$validateArr 	= $obj_commonErr->get_permision_withApproval_revise($headerRow['STATUS'],$headerRow['LEVELS'],$userId,$programCode,'RunQuery'); // get revise permission
	return  $validateArr;
}

function createHTML($sales_order_id,$sales_order_no,$style_no,$graphic_no,$sampleNo,$placement,$part_no,$combo,$revisionNo,$qty,$groundColor)
{
	global $obj_data_get;
	$a			= $sales_order_id;

	$html  = "<tr>".
               "<td class=\"td_del\" style=\"text-align:center\"><img src=\"images/del.png\" class=\"cls_del\"></td>".
               "<td class=\"td_salesOrder\" id=\"".$sales_order_id."\">".$sales_order_no."</td>".
               "<td class=\"td_subContractJobType\"><select name=\"cboJob$a\" id=\"cboJob$a\" style=\"width:122px\"  class=\"validate[required] cls_jobType\">".$obj_data_get->get_contract_type_options('RunQuery')."</td>".
               "<td class=\"td_graphicNo\">".$graphic_no."</td>".
			   "<td class=\"td_sampleNo\">".$sampleNo."</td>".
               "<td class=\"td_styleNo\">".$style_no."</td>".           
               "<td class=\"td_placement\">".$placement."</td>".
               "<td class=\"td_combo\">".$combo."</td>".
               "<td class=\"td_groundColor\">".$groundColor."</td>".
               "<td class=\"td_revisionNo\">".$revisionNo."</td>".
               "<td class=\"td_qty\"><input type=\"text\" name=\"txtQty$a\" id=\"txtQty$a\" style=\"width:60px;text-align:right\" class=\"validate[min[0],max[$qty],custom[integer]]\" value=\"".$qty."\" /></td>".
               "<td class=\"td_size\" style=\"text-align:center\"><img src=\"images/add_new.png\" class=\"mouseover cls_addSize\" /></td>".
               "<td class=\"td_price\"><input type=\"text\" name=\"txtPrice$a\" id=\"txtPrice$a\" style=\"width:80px;text-align:right\" value=\"0\" class=\"validate[min[0],max[0],custom[number]]\" disabled=\"disabled\"/></td>".
               "<td class=\"td_PSD\"><input type=\"text\" name=\"txtPSD$a\" id=\"txtPSD$a\" style=\"width:80px\" value=\"".date("Y-m-d")."\" onmousedown=\"DisableRightClickEvent();\" onmouseout=\"EnableRightClickEvent();\" onkeypress=\"return ControlableKeyAccess(event);\"  onclick=\"return showCalendar(this.id, '%Y-%m-%d');\" /><input type=\"reset\" style=\"visibility:hidden;width:1px\"   onclick=\"return showCalendar(this.id, '%Y-%m-%');\" /></td>".
               "<td class=\"td_deliveryDate\"><input name=\"txtDelDate$a\" type=\"text\" id=\"txtDelDate$a\" style=\"width:80px\" value=\"".date("Y-m-d")."\" onmousedown=\"DisableRightClickEvent();\" onmouseout=\"EnableRightClickEvent();\" onkeypress=\"return ControlableKeyAccess(event);\"  onclick=\"return showCalendar(this.id, '%Y-%m-%d');\" /><input type=\"reset\" style=\"visibility:hidden;width:1px\"   onclick=\"return showCalendar(this.id, '%Y-%m-%');\" /></td>".
              "</tr>";
	return $html;
}

function insertHeader($subContractNo,$subContractYear,$headerArray,$programCode,$userId,$locationId)
{
	global $obj_trn_order_sub_contract_header;
	global $obj_commonFunctions_get;
	global $obj_dateTime;
	
	$responce['type']	= true;
	
//BEGIN - GET APPROVE LEVELS FROM SYS_APPROVELEVELS TABLE. {
	$result1 = $obj_commonFunctions_get->getApproveLevels_new1($programCode,'RunQuery');
	if(!$result1['type'])
	{
		$responce['type']	= false;
		$responce['msg']	= $result1['msg'];
	}
	else{
		$approveLevel		= $result1['ApproLevel'];
		$status				= $result1['ApproLevel']+1;
	}
//END 	- GET APPROVE LEVELS FROM SYS_APPROVELEVELS TABLE. }

	$data  	= array('SUB_CONTRACT_NO'		=> $subContractNo ,
					'SUB_CONTRACT_YEAR'		=> $subContractYear ,
					'ORDER_NO'				=> $headerArray['OrderNo'] ,
					'ORDER_YEAR'			=> $headerArray['OrderYear'] ,
					'COMPANY_TO_TYPE'		=> $headerArray['SubContractType'] ,
					'SUB_CONTRACTOR_ID'		=> $headerArray['SubContractor'] ,
					'DATE'					=> $headerArray['Date'] ,
					'DELIVERY_DATE'			=> $headerArray['DeliveryDate'] ,
					'REMARKS'				=> $headerArray['Remarks'] ,
					'LOCATION_ID'			=> $locationId ,
					'STATUS'				=> $status ,
					'LEVELS'				=> $approveLevel ,					
					'CREATED_BY'			=> $userId ,
					'CREATED_DATE'			=> $obj_dateTime->getCurruntDateTime()				
					);
	
	$result1		= $obj_trn_order_sub_contract_header->insert($data); // insert data to trn_order_sub_contract_header table
	if(!$result1['status'] && $responce['type'])
	{
		$responce['type']	= false;
		$responce['msg']	= $result1['msg'];
		$responce['sql']	= $result1['sql'];
	}
	return $responce;
}

function updateHeader($headerArray,$programCode,$userId,$locationId)
{
	global $obj_trn_order_sub_contract_header;
	global $obj_commonFunctions_get;
	global $obj_dateTime;
	
 	$responce['type']	= true;
	
//BEGIN - GET APPROVE LEVELS FROM SYS_APPROVELEVELS TABLE. {
	$result1 = $obj_commonFunctions_get->getApproveLevels_new1($programCode,'RunQuery');
	if(!$result1['type'])
	{
		$responce['type']	= false;
		$responce['msg']	= $result1['msg'];
	}
	else{
		$approveLevel		= $result1['ApproLevel'];
		$status				= $result1['ApproLevel']+1;
	}
//END 	- GET APPROVE LEVELS FROM SYS_APPROVELEVELS TABLE. }

	$data  	= array(
					'ORDER_NO'				=> $headerArray['OrderNo'] ,
					'ORDER_YEAR'			=> $headerArray['OrderYear'] ,
					'COMPANY_TO_TYPE'		=> $headerArray['SubContractType'] ,
					'SUB_CONTRACTOR_ID'		=> $headerArray['SubContractor'] ,
					'DATE'					=> $headerArray['Date'] ,
					'DELIVERY_DATE'			=> $headerArray['DeliveryDate'] ,
					'REMARKS'				=> $headerArray['Remarks'] ,
					'LOCATION_ID'			=> $locationId ,
					'STATUS'				=> $status ,
					'LEVELS'				=> $approveLevel ,				
					'MODIFIED_BY'			=> $userId ,
					'MODIFIED_DATE'			=> $obj_dateTime->getCurruntDateTime()					
					);
	$where	= 'SUB_CONTRACT_NO = "'.$headerArray['SubContractNo'].'" AND SUB_CONTRACT_YEAR = "'.$headerArray['SubContractYear'].'" ' ;
		
	$result1	= $obj_trn_order_sub_contract_header->update($data,$where);
	if(!$result1['status'])
	{
		$responce['type']	= false;
		$responce['msg']	= $result1['msg'];
		$responce['sql']	= $result1['sql'];
	}
	return $responce;
}

function deleteDetails($headerArray)
{
	global $obj_trn_order_sub_contract_details;
	$responce['type']	= true;
	$where	= 'SUB_CONTRACT_NO = "'.$headerArray['SubContractNo'].'" AND SUB_CONTRACT_YEAR = "'.$headerArray['SubContractYear'].'" ' ;
	
	$result1 = $obj_trn_order_sub_contract_details->delete($where);
	if(!$result1['status'])
	{
		$responce['type']	= false;
		$responce['msg']	= $result1['msq'];
		$responce['sql']	= $result1['sql'];
	}
	return $responce;
}

function insertDetails($subContractNo,$subContractYear,$headerArray,$detailArray)
{
	global $obj_trn_order_sub_contract_details;
	$responce['type']	= true;
	foreach($detailArray as $row)
	{
		$data  	= array('SUB_CONTRACT_NO'		=> $subContractNo ,
						'SUB_CONTRACT_YEAR'		=> $subContractYear ,
						'SALES_ORDER_ID'		=> $row['SalesOrderId'] ,
						'SUB_CONTR_JOB_ID'		=> $row['SubContractJobId'] ,
						'QTY'					=> $row['Qty'] ,
						'PRICE'					=> $row['Price'] ,
						'PDS_DATE'				=> $row['PSDDate'] ,
						'DELIVERY_DATE'			=> $row['DeliveryDate']
						);
		$result1		= $obj_trn_order_sub_contract_details->insert($data); // insert data to trn_order_sub_contract_details table		
		if(!$result1['status'])
		{
			$responce['type']	= false;
			$responce['msg']	= $result1['msg'];
			$responce['sql']	= $result1['sql'];
		}
	}
	return $responce;
}

function validateBeforeSave($subContractNo,$subContractYear,$programCode,$userId)
{
	global $obj_commonErr;
	global $obj_trn_order_sub_contract_header;
	
	$responce['type']	= true;
	
	$where	= 'SUB_CONTRACT_NO = "'.$subContractNo.'" AND SUB_CONTRACT_YEAR = "'.$subContractYear.'" ' ;
	
	$header_result	= $obj_trn_order_sub_contract_header->select('*',NULL,$where,NULL,NULL); // get header data
	$headerRow		= mysqli_fetch_array($header_result);
	
	$result1  		= $obj_commonErr->get_permision_withApproval_save($headerRow['STATUS'],$headerRow['LEVELS'],$userId,$programCode,'RunQuery'); // get revise permission
	if($result1['type']=='fail')
	{
		$responce['type']	= false;
		$responce['msg']	= $result1['msg'];
	}
	return $responce;
}
function validateQty($subContractNo,$subContractYear)
{
	global $locationId;
	global $obj_customer_and_operation;
	$checkStatus	= true;
	
	$qty_result		= $obj_customer_and_operation->validateSubContractQty($subContractNo,$subContractYear,$locationId,'RunQuery');
	while($row = mysqli_fetch_array($qty_result))
	{
		$totQty	= $row['QTY'] + $row['totSavedQty'];
		if($totQty>$row['totOrderQty'] && $checkStatus)
		{
			$checkStatus	= false;
			$data['type']	= 'fail';
			$data['msg']	= 'Sub contract Qty exceed Order Qty.<br>Sales Order No : '.$row['strSalesOrderNo'].'<br>Size : '.$row['SIZE'].'<br>Order Qty : '.$row['totOrderQty'].'<br>Sub Contract Qty : '.$totQty.'';
		}
	}
	return $data;
}

function loadSubContractor($subContractType)
{
	global $db;
	global $obj_mst_subcontractor;
	
	$db->connect();		//open connection.	
	
	$html = "<option value="."".">&nbsp;</option>";
	
	$cols	= "SUB_CONTRACTOR_ID,
	           SUB_CONTRACTOR_NAME";
			   
	$where = "STATUS = 1 AND INTER_COMPANY = $subContractType";
	$result = $obj_mst_subcontractor->select($cols,$join = null,$where, $order = "SUB_CONTRACTOR_NAME", $limit = null);	
	
	while($row = mysqli_fetch_array($result))
	{
		$html .= "<option value=\"".$row['SUB_CONTRACTOR_ID']."\">".$row['SUB_CONTRACTOR_NAME']."</option>";
	}
	$db->disconnect();	//close connection.
	return $html;	
}

function getPrice($sampleNo,$revisionNo,$combo,$printName,$jobType)
{
	global $db;
	global $obj_trn_sampleinfomations_prices_sub_contract_job;
	
	$db->connect();		//open connection.	
	$amount = 0;
	$cols	= "COALESCE(dblPrice,0) AS PRICE";
			   
	$where = "intSampleNo = '".$sampleNo[0]."'
				AND intSampleYear = '".$sampleNo[1]."'
				AND intRevisionNo = '$revisionNo'
				AND strCombo = '$combo'
				AND strPrintName = '$printName'
				AND intSubContractJobID = '$jobType'";
	$result = $obj_trn_sampleinfomations_prices_sub_contract_job->select($cols,$join = null,$where, $order = null, $limit = null);		
	while($row = mysqli_fetch_array($result))
	{
		$amount	= $row['PRICE'];
	}
	$db->disconnect();	//close connection.
	return $amount;	
}

function saveSizes($headerArray,$detailArray)
{
	global $obj_trn_order_sub_contract_size_wise_qty;
	$responce['type']	= true;
	
	foreach($detailArray as $row)
	{
		$data  	= array('SUB_CONTRACT_NO'		=> $headerArray['SubContractNo'],
						'SUB_CONTRACT_YEAR'		=> $headerArray['SubContractYear'],
						'SALES_ORDER_ID'		=> $row['SalesOrderId'],
						'JOB_TYPE'				=> $row['JobType'],
						'SIZE'					=> $row['Size'],
						'QTY'					=> $row['SubQty']
						);
		$result1		= $obj_trn_order_sub_contract_size_wise_qty->insert($data); // insert data to trn_order_sub_contract_details table		
		if(!$result1['status'] && $responce['type'])
		{
			$responce['type']	= false;
			$responce['msg']	= $result1['msg'];
			$responce['sql']	= $result1['sql'];
		}
	}
	return $responce;
}

function deleteSizeWiseDetails($headerArray)
{
	global $obj_trn_order_sub_contract_size_wise_qty;
	
	$responce['type']	= true;
	
	$where	= 'SUB_CONTRACT_NO = "'.$headerArray['SubContractNo'].'" 
			   AND SUB_CONTRACT_YEAR = "'.$headerArray['SubContractYear'].'" 
			   AND SALES_ORDER_ID = "'.$headerArray['SalesOrderId'].'"' ;
	
	$result1 = $obj_trn_order_sub_contract_size_wise_qty->delete($where);
	if(!$result1['status'])
	{
		$responce['type']	= false;
		$responce['msg']	= $result1['msg'];
		$responce['sql']	= $result1['sql'];
	}
	return $responce;
}

function getGroundColor($sampleNo,$sampleYear,$printName,$comboId,$revisionNo)
{
	global $obj_trn_sampleinfomations_details;
	
	$cols	= " DISTINCT G.strName";
			   
	$join	= "Inner Join mst_colors_ground G 
					ON trn_sampleinfomations_details.intGroundColor = G.intId";
	
	$where = "trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear'  AND
					trn_sampleinfomations_details.strPrintName =  '$printName' AND
					trn_sampleinfomations_details.strComboName =  '$comboId' AND
					trn_sampleinfomations_details.intRevNo =  '$revisionNo'";
	$result1 = $obj_trn_sampleinfomations_details->select($cols,$join,$where, $order = null, $limit = null);		
	$row	= mysqli_fetch_array($result1);
	return $row['strName'];	
}

function updateMaxStatus($subContractNo,$subContractYear)
{
	global $obj_trn_order_sub_contract_approved_by;
	$responce['type']	= true;
	
	$cols		= " MAX(STATUS) AS MAX_STATUS ";
	
	$where		= " SUB_CONTRACT_NO = '".$subContractNo."' AND
					SUB_CONTRACT_YEAR = '".$subContractYear."' ";
	
	$result 	= $obj_trn_order_sub_contract_approved_by->select($cols,$join = null,$where, $order = null, $limit = null);	
	$row		= mysqli_fetch_array($result);
	
	$maxStatus	= $row['MAX_STATUS']+1;
	
	$data  		= array('STATUS' => $maxStatus);
	
	$where		= 'SUB_CONTRACT_NO = "'.$subContractNo.'" AND SUB_CONTRACT_YEAR = "'.$subContractYear.'" AND STATUS = 0 ' ;
	$result_arr	= $obj_trn_order_sub_contract_approved_by->update($data,$where);
	if(!$result_arr['status'])
	{
		$responce['type']	= false;
		$responce['msg']	= $result_arr['msg'];
		$responce['sql']	= $result_arr['sql'];
	}
	return $responce;
}

function getApprovedPOQty($orderNo,$orderYear,$salesOrderId)
{
	global $db;
	global $obj_trn_order_sub_contract_details;
	
	$cols		= "SUM(trn_order_sub_contract_details.QTY) AS PO_QTY";
	
	$join 		= "INNER JOIN trn_order_sub_contract_header H
					ON H.SUB_CONTRACT_NO = trn_order_sub_contract_details.SUB_CONTRACT_NO
					AND H.SUB_CONTRACT_YEAR = trn_order_sub_contract_details.SUB_CONTRACT_YEAR ";
					
	$where		= "H.STATUS = 1
					AND H.ORDER_NO = '$orderNo'
					AND H.ORDER_YEAR = '$orderYear'
					AND trn_order_sub_contract_details.SALES_ORDER_ID = '$salesOrderId' ";
	
	$result1 	= $obj_trn_order_sub_contract_details->select($cols,$join,$where, $order = null, $limit = null);	
	$row1		= mysqli_fetch_array($result1);	
	return $row1['PO_QTY'];
}
function checkSizeWiseQtyAdd($subContractNo,$subContractYear)
{
	global $db;
	global $obj_trn_order_sub_contract_details;
	global $obj_trn_order_sub_contract_size_wise_qty;
	$chkStatus		= true;
	$data['type']	= 'true';
	
	$msg		= "Please enter size wise qty for below sales orders<br>";
	
	$cols		= ' trn_order_sub_contract_details.SUB_CONTRACT_NO,
					trn_order_sub_contract_details.SUB_CONTRACT_YEAR,
					trn_order_sub_contract_details.SALES_ORDER_ID,
					OD.strSalesOrderNo ';
	
	$join		= ' INNER JOIN trn_order_sub_contract_header SCH ON SCH.SUB_CONTRACT_NO=trn_order_sub_contract_details.SUB_CONTRACT_NO AND
					SCH.SUB_CONTRACT_YEAR=trn_order_sub_contract_details.SUB_CONTRACT_YEAR
					INNER JOIN trn_orderdetails OD ON OD.intOrderNo=SCH.ORDER_NO AND
					OD.intOrderYear=SCH.ORDER_YEAR AND
					OD.intSalesOrderId=trn_order_sub_contract_details.SALES_ORDER_ID ';
					
	$where		= 'SCH.SUB_CONTRACT_NO = "'.$subContractNo.'" AND SCH.SUB_CONTRACT_YEAR = "'.$subContractYear.'" ' ;
	
	$result 	= $obj_trn_order_sub_contract_details->select($cols, $join, $where, $order = null, $limit = null);
	while($row = mysqli_fetch_array($result))
	{
		$where	= ' SUB_CONTRACT_NO = "'.$row['SUB_CONTRACT_NO'].'" AND 
					SUB_CONTRACT_YEAR = "'.$row['SUB_CONTRACT_YEAR'].'" AND
					SALES_ORDER_ID = "'.$row['SALES_ORDER_ID'].'" ' ;
		
		$result1 	= $obj_trn_order_sub_contract_size_wise_qty->select('*', $join = null, $where, $order = null, $limit = null);
		//$numRows	= $db->numRows();
		$numRows	= mysqli_num_rows($result1);
		if($numRows<=0)
		{
			$chkStatus	 = false;
			$msg		.= 'Sales Order No : '.$row['strSalesOrderNo'].'<br>';
		}
	}
	if(!$chkStatus)
	{
		$data['type']	= 'fail';
		$data['msg']	= $msg ;
	}
	return $data;	
}
?>
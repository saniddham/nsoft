<?php
session_start();
$backwardseperator 		= "../../../../../";
require_once ("{$backwardseperator}dataAccess/DBManager2.php"); 						$db											= new DBManager2();
include_once "../../../../../class/tables/trn_ordersizeqty.php"; 						$obj_trn_ordersizeqty 						= new trn_ordersizeqty($db);
include_once "../../../../../class/tables/trn_order_sub_contract_size_wise_qty.php";	$obj_trn_order_sub_contract_size_wise_qty 	= new trn_order_sub_contract_size_wise_qty($db);

$subContractNo		= $_REQUEST['SubContractNo'];
$subContractYear	= $_REQUEST['SubContractYear'];
$salesOrderId		= $_REQUEST['SalesOrderId'];
$jobType			= $_REQUEST['JobType'];
$poQty				= $_REQUEST['POQty'];

$result = getPoSizeQty($subContractNo,$subContractYear,$salesOrderId);
?>
<html>
<script type="text/javascript">
var salesOrderId = '<?php echo $salesOrderId?>';
var jobType = '<?php echo $jobType?>';
</script>
</head>
<body>
<form id="frmSubContractSizeWisePopup" name="frmSubContractSizeWisePopup" method="post" action="">
  <div align="center">
    <div class="trans_layoutS">
      <table width="400" border="0">
        <tr>
          <td width="471" align="center"><table width="100%"  class="bordered"  id="tblMain" align="center" >
              <tr>
                <th width="8%" height="22" >Del</th>
                <th width="27%" >Size</th>
                <th width="33%">Order Qty</th>
                <th width="32%">Size Wise Qty</th>
              </tr>
              <?php 
			  while($row = mysqli_fetch_array($result))
			  {
				  $qty = getSavedQty($subContractNo,$subContractYear,$salesOrderId,$jobType,$row["SIZE"]);
			  ?>
              <tr>
                <td class="cls_del" style="text-align:center"><img src="images/del.png" width="15" height="15" class="delImgPopup" /></td>
                <td class="cls_size"><?php echo $row["SIZE"]?></td>
                <td class="cls_orderQty" style="text-align:right"><?php echo $row["ORDER_QTY"]?></td>
                <td class="cls_subQty"><input name="txtSizeQty<?php echo $row["SIZE"]?>" type="text" id="txtSizeQty<?php echo $row["SIZE"]?>" style="width:120px; text-align:right" value="<?php echo $qty?>"  class="validate[required,custom[number],max[<?php echo $row['ORDER_QTY'] ?>],min[0]] cls_optionQty" /></td>
              </tr>
              <?php
			  	$totalQty	+= $qty;
			  }
			  ?>
              <tr>
                <td colspan="4" align="left"  bgcolor="#FFFFFF" ><?php if($editMode==1){ ?>
                  <img src="../../../../../images/Tadd.jpg" name="butInsertRowPopup" width="78" height="24" class="mouseover" id="butInsertRowPopup" />
                  <?php } ?></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td align="right" ><table width="210" border="0" cellspacing="1" cellpadding="1" class="normalfnt">
            <tr>
              <td width="104">PO Qty</td>
              <td width="96" ><input id="txtPOWiseQty" name="txtPOWiseQty" type="text" style="width:120px; text-align:right" value="<?php echo round($poQty)?>"/></td>
            </tr>
            <tr>
              <td>Size Wise Qty</td>
              <td><input id="txtSizeWiseQty" name="txtSizeWiseQty" type="text" style="width:120px; text-align:right" value="<?php echo round($totalQty)?>"/></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td align="center" ><a id="butSave" name="butSave" class="button white medium">Update</a> <a id="butClose" name="butClose" class="button white medium">Close</a></td>
        </tr>
      </table>
    </div>
  </div>
</form>
</body>
</html>
<?php
function getPoSizeQty($subContractNo,$subContractYear,$salesOrderId)
{
	global $db;
	global $obj_trn_ordersizeqty;
	
	$db->connect();		//open connection.	
	$amount = 0;
	$cols	= "strSize 	AS SIZE,
				dblQty	AS ORDER_QTY";
	
	$join	= "INNER JOIN trn_order_sub_contract_header H
				ON H.ORDER_NO = trn_ordersizeqty.intOrderNo
				  AND H.ORDER_YEAR = trn_ordersizeqty.intOrderYear";
		   
	$where  = "H.SUB_CONTRACT_NO = $subContractNo
				AND H.SUB_CONTRACT_YEAR = $subContractYear
				AND trn_ordersizeqty.intSalesOrderId = $salesOrderId";
	$result = $obj_trn_ordersizeqty->select($cols,$join,$where, $order = null, $limit = null);
	$db->disconnect();	//close connection.
	return $result;		
}

function getSavedQty($subContractNo,$subContractYear,$salesOrderId,$jobType,$size)
{
	global $db;
	global $obj_trn_order_sub_contract_size_wise_qty;
	$amount	= 0;
	
	$db->connect();	

	$cols	= "QTY	AS QTY";
	
	$where  = "SUB_CONTRACT_NO = '$subContractNo'
				AND SUB_CONTRACT_YEAR = '$subContractYear'
				AND SALES_ORDER_ID = '$salesOrderId'
				AND JOB_TYPE = '$jobType'
				AND SIZE = '$size';";
	$result = $obj_trn_order_sub_contract_size_wise_qty->select($cols,$join = null,$where, $order = null, $limit = null);
	while($row = mysqli_fetch_array($result))
	{
		$amount = $row['QTY'];
	}
	$db->disconnect();
	return $amount;	
}
?>
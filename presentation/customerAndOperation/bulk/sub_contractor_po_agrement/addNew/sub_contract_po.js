$(document).ready(function() {	
	try
	{
		$('#frmSubContractOrder').validationEngine();
		$('#frmRptSubContractPO').validationEngine();
	}
	catch(err)
	{
	}

	$('#frmSubContractOrder #butSearch').die('click').live('click',searchOrder);
	$('#frmSubContractOrder #butAddSalesOrders').die('click').live('click',addSalesOrders);
	$('#frmSubContractOrder #butNew').die('click').live('click',clearForm);
	$('#frmSubContractOrder #butSave').die('click').live('click',save);
	$('#frmSubContractOrder #butReport').die('click').live('click',Report);
	$('#frmSubContractOrder #cboSubContractType').die('change').live('change',loadSubContractor);
	$('#frmSubContractOrder #tblMain .cls_jobType').die('change').live('change',loadPrice);
	$('#frmSubContractOrder #tblMain .cls_addSize').die('click').live('click',addSizeWiseQty);
	$('#frmSubContractOrder #tblMain .cls_del').die('click').live('click',removeRow);
	$('#frmSubContractOrder #txtExYear').die('keyup').live('keyup',validateOrder);
	$('#frmSubContractOrder #txtExOrderNo').die('keyup').live('keyup',validateOrder);
	$('#frmSubContractOrder #butConfirm').die('click').live('click',Confirm);
	$('#frmSubContractOrder #butRevise').die('click').live('click',Revise);
	
	$('#frmSubContractOrders_popup #butAddPopupItem').die('click').live('click',addPopupItem);
	$('#frmSubContractOrders_popup #chkCheckAll').die('click').live('click',selectAll);
	
	$('#frmSubContractSizeWisePopup #butSave').die('click').live('click',saveSizes);
	$('#frmSubContractSizeWisePopup #tblMain .cls_optionQty').die('keyup').live('keyup',calculateSizeWiseQty);
	
	$('#frmRptSubContractPO #butRptConfirm').die('click').live('click',ConfirmRpt);
	$('#frmRptSubContractPO #butRptReject').die('click').live('click',RejectRpt);
	$('#frmRptSubContractPO #butRptRevise').die('click').live('click',ReviseRpt);
});

var booSearched		= false;
var pub_orderNo		= "";
var pub_orderYear	= "";

function validateOrder()
{
	if(booSearched)
	{
		var val = $.prompt('This action will clear your searched order data. Are you sure you want to proceed?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
				clearForm1();
				searchOrder()
			}
			else
			{
				$('#frmSubContractOrder #txtExYear').val(pub_orderYear);
				$('#frmSubContractOrder #txtExOrderNo').val(pub_orderNo);
			}			
		}
		});
	}
}

function removeRow()
{
	$(this).parent().parent().remove();
}

function calculateSizeWiseQty()
{
	var total  = 0;
	$('#frmSubContractSizeWisePopup #tblMain .cls_optionQty').each(function() {
        total += parseFloat($(this).val());
    });
	$('#frmSubContractSizeWisePopup #txtSizeWiseQty').val(total);
}

function saveSizes()
{
	//showWaiting();
	
	if(parseFloat($('#frmSubContractSizeWisePopup #txtSizeWiseQty').val())!= parseFloat($('#frmSubContractSizeWisePopup #txtPOWiseQty').val()))
	{
		$('#frmSubContractSizeWisePopup #butSave').validationEngine('showPrompt','Sales order wise po qty should equal with size wise qty.','fail');
		var t = setTimeout("alertx('#frmSubContractSizeWisePopup #butSave')",4000);
		//hideWaiting();
		return;
	}
	
	if(!$('#frmSubContractSizeWisePopup').validationEngine('validate'))
	{
		var t = setTimeout("alertx('')",4000);
		//hideWaiting();
		return;
	}
	
	var headerArray  = "{";
		headerArray += '"SubContractNo":"'+$('#frmSubContractOrder #txtSubContractNo').val()+'",';
		headerArray += '"SubContractYear":"'+$('#frmSubContractOrder #txtSubContractYear').val()+'",';
		headerArray += '"SalesOrderId":"'+salesOrderId+'",';
		headerArray += '"JobType":"'+jobType+'"';
		headerArray += "}";
	
	var detailArray  = "[";
	$('#frmSubContractSizeWisePopup #tblMain .cls_del').each(function() {
		detailArray += "{";
		detailArray += '"Size":"'+$(this).parent().find('.cls_size').html()+'",';
		detailArray += '"SubQty":"'+$(this).parent().find('.cls_subQty').children().val()+'",';
		detailArray += '"SalesOrderId":"'+salesOrderId+'",';
		detailArray += '"JobType":"'+jobType+'"';
		detailArray += "},";
	});
	detailArray 		= detailArray.substr(0,detailArray.length-1);
	detailArray			+= "]";
	
	var url 	= "presentation/customerAndOperation/bulk/sub_contractor_po_agrement/addNew/sub_contract_po_db.php?requestType=URLSaveSizes";
	
	var httpobj = $.ajax({
		url:url,
		data:"HeaderArray="+headerArray+'&DetailArray='+detailArray,
		dataType:'json',
		async:false,
		success:function(json)
		{
			$('#frmSubContractSizeWisePopup #butSave').validationEngine('showPrompt',json.msg,json.type);
			if(json.type=='pass')
			{
				var t = setTimeout("alertx('#frmSubContractSizeWisePopup #butSave')",2000);
				disablePopup();
			}
			//hideWaiting();
		},
		error:function(xhr,status)
		{		
			$('#frmSubContractSizeWisePopup #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
			var t = setTimeout("alertx('#frmSubContractSizeWisePopup #butSave')",4000);
		}
	});
}

function loadPrice()
{
	var obj	= $(this);
	
	var url 	= "presentation/customerAndOperation/bulk/sub_contractor_po_agrement/addNew/sub_contract_po_db.php?requestType=URLloadPrice";
	var data 	= "SampleNo="+obj.parent().parent().find('.td_sampleNo').html();
		data   += "&RevisionNo="+obj.parent().parent().find('.td_revisionNo').html();
		data   += "&Combo="+obj.parent().parent().find('.td_combo').html();
		data   += "&PrintName="+obj.parent().parent().find('.td_placement').html();
		data   += "&JobType="+obj.val();
	
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	async:false,
	success:function(json)
	{
		obj.parent().parent().find('.td_price').children().val(json.price);
		obj.parent().parent().find('.td_price').children().attr('title',json.price);
		obj.parent().parent().find('.td_price').children().removeClass('validate[min[0],max[0],custom[number]]');
		obj.parent().parent().find('.td_price').children().addClass('validate[min[0]max['+json.price+'],custom[number]]');
	}
	});
}

function loadSubContractor()
{
	var url 	= "presentation/customerAndOperation/bulk/sub_contractor_po_agrement/addNew/sub_contract_po_db.php?requestType=URLloadSubContractor";
	
	var httpobj = $.ajax({
	url:url,
	data:"SubContractType="+$('#frmSubContractOrder #cboSubContractType').val(),
	dataType:'json',
	async:false,
	success:function(json)
	{
		$('#frmSubContractOrder #cboSubContractor').html(json.html);
	}
	});
}

function viewReport()
{
	var url ="?q=907&serialNo="+$('#frmSubContractOrder #txtSubContractNo').val()+"&serialYear="+$('#frmSubContractOrder #txtSubContractYear').val();
	window.open(url,'');
}

function alertx(obj)
{
	$(obj).validationEngine('hide')	;
}

function clearForm()
{
	document.location.href = '?q=871';
}

function save()
{
	showWaiting();
	if(!$('#frmSubContractOrder').validationEngine('validate'))
	{
		var t = setTimeout("alertx('#frmSubContractOrder')",4000);
		hideWaiting();
		return;
	}
	
	var headerArray  = "{";
		headerArray += '"SubContractNo":"'+$('#frmSubContractOrder #txtSubContractNo').val()+'",';
		headerArray += '"SubContractYear":"'+$('#frmSubContractOrder #txtSubContractYear').val()+'",';
		headerArray += '"OrderNo":"'+$('#frmSubContractOrder #txtExOrderNo').val()+'",';
		headerArray += '"OrderYear":"'+$('#frmSubContractOrder #txtExYear').val()+'",';
		headerArray += '"SubContractType":"'+$('#frmSubContractOrder #cboSubContractType').val()+'",';
		headerArray += '"SubContractor":"'+$('#frmSubContractOrder #cboSubContractor').val()+'",';
		headerArray += '"Date":"'+$('#frmSubContractOrder #dtDate').val()+'",';
		headerArray += '"DeliveryDate":"'+$('#frmSubContractOrder #dtDeliveryDate').val()+'",';
		headerArray += '"Remarks":"'+$('#frmSubContractOrder #txtRemarks').val()+'"';
		headerArray += "}";
	
	var detailArray  = "[";
	$('#frmSubContractOrder #tblMain .td_salesOrder').each(function() {
		detailArray += "{";
		detailArray += '"SalesOrderId":"'+$(this).attr('id')+'",';
		detailArray += '"SubContractJobId":"'+$(this).parent().find('.td_subContractJobType').children().val()+'",';
		detailArray += '"Qty":"'+$(this).parent().find('.td_qty').children().val()+'",';
		detailArray += '"Price":"'+$(this).parent().find('.td_price').children().val()+'",';
		detailArray += '"PSDDate":"'+$(this).parent().find('.td_PSD').children().val()+'",';
		detailArray += '"DeliveryDate":"'+$(this).parent().find('.td_deliveryDate').children().val()+'"';
		detailArray += "},";
	});
	detailArray 		= detailArray.substr(0,detailArray.length-1);
	detailArray			+= "]";
	
	var requestType 	= ($('#frmSubContractOrder #txtSubContractNo').val()==""?'URLSave':'URLUpdate');
	var url 	= "presentation/customerAndOperation/bulk/sub_contractor_po_agrement/addNew/sub_contract_po_db.php?requestType="+requestType;
	
	var httpobj = $.ajax({
		url:url,
		data:"HeaderArray="+headerArray+'&DetailArray='+detailArray,
		dataType:'json',
		async:false,
		success:function(json)
		{
			$('#frmSubContractOrder #butSave').validationEngine('showPrompt',json.msg,json.type);
			if(json.type=='pass')
			{
				$('#frmSubContractOrder #txtSubContractNo').val(json.No);
				$('#frmSubContractOrder #txtSubContractYear').val(json.Year);
				var t = setTimeout("alertx('#frmSubContractOrder #butSave')",4000);
				$('#frmSubContractOrder #butConfirm').show();
			}
			hideWaiting();
		},
		error:function(xhr,status)
		{		
			$('#frmSubContractOrder #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
			var t = setTimeout("alertx('#frmSubContractOrder #butSave')",4000);
		}	
	});
}

function selectAll()
{
	var mainCheck = $(this).is(':checked')? true:false;
	$('#frmSubContractOrders_popup #tblPOs .td_check').each(function() {
    	$(this).children().attr('checked',mainCheck);
    });
}

function searchOrder()
{	
	showWaiting();
	
	if($('#frmSubContractOrder #txtExYear').val()=='')
	{
		$('#frmSubContractOrder #butSearch').validationEngine('showPrompt',"Please enter 'Order Year'",'fail');
		$('#frmSubContractOrder #txtExYear').focus();
		var t = setTimeout("alertx('#frmSubContractOrder #butSearch')",2000);
		hideWaiting();
		return;
	}
	
	if($('#frmSubContractOrder #txtExOrderNo').val()=='')
	{
		$('#frmSubContractOrder #butSearch').validationEngine('showPrompt',"Please enter 'Order No'",'fail');
		$('#frmSubContractOrder #txtExOrderNo').focus();
		var t = setTimeout("alertx('#frmSubContractOrder #butSearch')",2000);
		hideWaiting();
		return;
	}

	var headerArray = "{";
						headerArray += '"OrderYear":'+$('#frmSubContractOrder #txtExYear').val()+',';
						headerArray += '"OrderNo":'+$('#frmSubContractOrder #txtExOrderNo').val()+'';
		headerArray += "}";

	var url 	= "presentation/customerAndOperation/bulk/sub_contractor_po_agrement/addNew/sub_contract_po_db.php?requestType=URLSearchOrder";
	
	var httpobj = $.ajax({
	url:url,
	data:"HeaderArray="+headerArray,
	dataType:'json',
	async:false,
	success:function(json)
	{
		if(!json.TYPE)
		{
			$('#frmSubContractOrder #butSearch').validationEngine('showPrompt',json.MSG,'fail');
			var t = setTimeout("alertx('#frmSubContractOrder #butSearch')",4000);
			hideWaiting();
			return;
		}
		else
		{
			$('#frmSubContractOrder #txtCustomer').val(json.CUSTOMER_NAME);
			$('#frmSubContractOrder #txtPayTerm').val(json.PAYMENT_TERM);
			$('#frmSubContractOrder #txtCurrency').val(json.CURRENCY);
			$('#frmSubContractOrder #txtContactPerson').val(json.CONTACT_PERSON);
			$('#frmSubContractOrder #txtMarketer').val(json.MARKETER);
			$('#frmSubContractOrder #txtCustLocation').val(json.CUSTOMER_LOCATION);
			pub_orderNo		= $('#frmSubContractOrder #txtExOrderNo').val();
			pub_orderYear	= $('#frmSubContractOrder #txtExYear').val();
			booSearched	= true;
			hideWaiting();
		}
	}
	});
	hideWaiting();
}

function addSalesOrders()
{
	$('#popupContact1').html('');
	popupWindow3('1');
	$('#popupContact1').load('presentation/customerAndOperation/bulk/sub_contractor_po_agrement/addNew/sub_contract_po_popup.php?OrderNo='+$('#frmSubContractOrder #txtExOrderNo').val()+'&OrderYear='+$('#frmSubContractOrder #txtExYear').val(),function(){
		 $('#frmSubContractOrders_popup #butClose').click(disablePopup);
		 //$('#frmSubContractOrders_popup #butAddPopupItem').on('click',addPopupItem);
	});
}

function addSizeWiseQty()
{
	$('#popupContact1').html('');
	if($('#frmSubContractOrder #txtSubContractNo').val()=="")
	{
		$(this).validationEngine('showPrompt','Please first save the PO before you add sizes.','fail');
		var t = setTimeout("alertx('$(this)')",4000);
		return;
	}
	
	popupWindow3('1');
	
	var data  = "SubContractNo="+$('#frmSubContractOrder #txtSubContractNo').val();
		data += "&SubContractYear="+$('#frmSubContractOrder #txtSubContractYear').val();
		data += "&SalesOrderId="+$(this).parent().parent().find('.td_salesOrder').attr('id');
		data += "&JobType="+$(this).parent().parent().find('.td_subContractJobType').children().val();
		data += "&POQty="+$(this).parent().parent().find('.td_qty').children().val();
		
	$('#popupContact1').load('presentation/customerAndOperation/bulk/sub_contractor_po_agrement/addNew/sub_contract_po_size_popup.php?'+data,function(){
		 $('#frmSubContractSizeWisePopup #butClose').click(disablePopup);
	});
}

function addPopupItem()
{
		var detailArray  = "";
	$('#frmSubContractOrders_popup .td_salesOrderId').each(function(index, element) {
        if($(this).parent().find('.td_check').children().is(':checked'))
		{
			detailArray += $(this).parent().find('.td_salesOrderId').attr('id');
			detailArray += ',';
		}
    });
	detailArray 	 = detailArray.substr(0,detailArray.length-1);

	var url 	= "presentation/customerAndOperation/bulk/sub_contractor_po_agrement/addNew/sub_contract_po_db.php?requestType=URLAddPopupItem";	
	var httpobj = $.ajax({
	url:url,
	data:"OrderNo="+$('#frmSubContractOrders_popup #orderNo').html()+"&SalesOrder="+detailArray,
	dataType:'json',
	async:false,
	success:function(json)
	{
		$('#frmSubContractOrder #tblMain tbody').html(json.html);
		disablePopup();
	}
	});
}
function ConfirmRpt()
{
	var val = $.prompt('Are you sure you want to Approve this Sub Contract PO ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = "presentation/customerAndOperation/bulk/sub_contractor_po_agrement/addNew/sub_contract_po_db.php"+window.location.search+'&requestType=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptSubContractPO #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx1()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptSubContractPO #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx1()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}
function RejectRpt()
{
	var val = $.prompt('Are you sure you want to Reject this Sub Contract PO ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = "presentation/customerAndOperation/bulk/sub_contractor_po_agrement/addNew/sub_contract_po_db.php"+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptSubContractPO #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx2()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptSubContractPO #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx2()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}
function ReviseRpt()
{
	var val = $.prompt('Are you sure you want to Revise this Sub Contract PO ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = "presentation/customerAndOperation/bulk/sub_contractor_po_agrement/addNew/sub_contract_po_db.php"+window.location.search+'&requestType=revise';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptSubContractPO #butRptRevise').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx3()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptSubContractPO #butRptRevise').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx3()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}
function Confirm()
{
	var url  = "?q=907&serialNo="+$('#frmSubContractOrder #txtSubContractNo').val();
		url += "&serialYear="+$('#frmSubContractOrder #txtSubContractYear').val();
		url += "&Mode=Confirm";
	window.open(url,'rpt_sub_contract_po.php');
}
function Revise()
{
	var url  = "?q=907&serialNo="+$('#frmSubContractOrder #txtSubContractNo').val();
		url += "&serialYear="+$('#frmSubContractOrder #txtSubContractYear').val();
		url += "&Mode=Revise";
	window.open(url,'rpt_sub_contract_po.php');
}
function Report()
{
	if($('#frmSubContractOrder #txtSubContractNo').val()=='')
	{
		$('#frmSubContractOrder #butReport').validationEngine('showPrompt','No Sub Contract PO No to view report','fail');
		return;
	}
	
	var url  = "?q=907&serialNo="+$('#frmSubContractOrder #txtSubContractNo').val();
		url += "&serialYear="+$('#frmSubContractOrder #txtSubContractYear').val();
	window.open(url,'rpt_sub_contract_po.php');
	
}
function alertx1()
{
	$('#frmRptSubContractPO #butRptConfirm').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmRptSubContractPO #butRptReject').validationEngine('hide')	;
}
function alertx3()
{
	$('#frmRptSubContractPO #butRptRevise').validationEngine('hide')	;
}

function clearForm1()
{
	$('#frmSubContractOrder #txtCustomer').val('');
	$('#frmSubContractOrder #txtPayTerm').val('');
	$('#frmSubContractOrder #txtCurrency').val('');
	$('#frmSubContractOrder #txtContactPerson').val('');
	$('#frmSubContractOrder #txtMarketer').val('');
	$('#frmSubContractOrder #txtCustLocation').val('');
	clearGrid();
}

function clearGrid()
{
	$("#tblMain tr:gt(0)").remove();
}
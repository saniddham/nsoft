<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$userId  				= $sessions->getUserId();

$programCode			= 'P0871';

include_once "class/cls_commonErrorHandeling_get.php";
include_once "class/tables/trn_order_sub_contract_header.php";
include_once "class/tables/trn_order_sub_contract_details.php";
include_once "class/tables/mst_sub_contract_job_types.php";
include_once "class/tables/mst_subcontractor.php";
include_once "class/tables/trn_sampleinfomations_details.php";
include_once "class/tables/trn_sampleinfomations_prices_sub_contract_job.php";

$obj_commonErr			= new cls_commonErrorHandeling_get($db);

$serialNo 				= (!isset($_REQUEST['serialNo'])?'':$_REQUEST['serialNo']);

$serialYear 			= (!isset($_REQUEST['year'])?'':$_REQUEST['year']);

$obj_trn_order_sub_contract_header		= new trn_order_sub_contract_header($db);
$obj_trn_order_sub_contract_details		= new trn_order_sub_contract_details($db);
$obj_mst_sub_contract_job_types			= new mst_sub_contract_job_types($db);
$obj_mst_subcontractor					= new mst_subcontractor($db);
$obj_trn_sampleinfomations_details		= new trn_sampleinfomations_details($db);
$obj_trn_sampleinfomations_prices_sub_contract_job 	= new trn_sampleinfomations_prices_sub_contract_job($db);

$status					= $obj_trn_order_sub_contract_header->getStatus($serialNo,$serialYear);
$apprveLevels			= $obj_trn_order_sub_contract_header->getApproveLevels($serialNo,$serialYear);


$headerArray			= getHeaderDetails($serialNo,$serialYear);
$detailArray			= getDetails($serialNo,$serialYear);

$db->connect();

$permition_arr			= $obj_commonErr->get_permision_withApproval_save($status,$apprveLevels,$userId,$programCode,'RunQuery');
$permision_save			= $permition_arr['permision'];

$permition_arr			= $obj_commonErr->get_permision_withApproval_confirm($status,$apprveLevels,$userId,$programCode,'RunQuery');
$permision_confirm		= $permition_arr['permision'];

$permition_arr			= $obj_commonErr->get_permision_withApproval_revise($status,$apprveLevels,$userId,$programCode,'RunQuery');
$permision_revise		= $permition_arr['permision'];

$db->disconnect();
?>
<title>Sub Contract Purchase Order</title>
<form id="frmSubContractOrder" name="frmSubContractOrder" method="post" action="">
  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
  </table>
  <div align="center">
    <div class="trans_layoutL">
      <div class="trans_text">Customer Purchase Order (Sub Contractor)</div>
      <table width="900" border="0" align="center" bgcolor="#FFFFFF">
        
          <td width="100%"><table width="100%" border="0">
              <tr>
                <td><table width="100%" border="0" cellspacing="1" cellpadding="2" class="normalfnt tableBorder_allRound">
                    <tr>
                      <td width="102"><span class="normalfnt">Sub Contract No</span></td>
                      <td width="60"><input name="txtSubContractYear" type="text" disabled="disabled" id="txtSubContractYear" style="width:60px" value="<?php echo $serialYear ?>" /></td>
                      <td width="285"><input name="txtSubContractNo" type="text" disabled="disabled" id="txtSubContractNo" style="width:80px"value="<?php echo $serialNo ?>" /></td>
                      <td width="163">&nbsp;</td>
                      <td width="89">Date</td>
                      <td width="180"><input name="dtDate" type="text" value="<?php echo $headerArray['DATE']==''?date("Y-m-d"):$headerArray['DATE']; ?>" class="txtbox" id="dtDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/>
                        <input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                    </tr>
                  </table></td>
              </tr>
              <tr >
                <td><table width="100%" border="0" cellspacing="1" cellpadding="1" class="normalfnt tableBorder_allRound">
                    <tr>
                      <td>Existing Order</td>
                      <td><input name="txtExYear" type="text" id="txtExYear" style="width:60px" value="<?php echo $headerArray['ORDER_YEAR']?>" class="validate[required,custom[number]]"/>
                        <input name="txtExOrderNo" type="text" id="txtExOrderNo" style="width:80px" value="<?php echo $headerArray['ORDER_NO']?>" class="validate[required,custom[number]]"/>
                        <a id="butSearch" class="button white small">Search</a></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>Delivery Date</td>
                      <td><input name="dtDeliveryDate" type="text" value="<?php echo $headerArray['DELIVERY_DATE']==''?date("Y-m-d"):$headerArray['DELIVERY_DATE']; ?>" class="txtbox" id="dtDeliveryDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                    </tr>
                    <tr>
                      <td width="12%">Customer</td>
                      <td width="25%"><input value="<?php echo $headerArray['CUSTOMER_NAME']?>" name="txtCustomer" type="text" id="txtCustomer" style="width:220px" disabled="disabled"/></td>
                      <td width="11%">Payment Term</td>
                      <td width="22%"><input value="<?php echo $headerArray['PAYMENT_TERM']?>" name="txtPayTerm" type="text" id="txtPayTerm" style="width:190px" disabled="disabled"/></td>
                      <td width="10%">Currency</td>
                      <td width="20%"><input value="<?php echo $headerArray['CURRENCY']?>" name="txtCurrency" type="text" id="txtCurrency" style="width:180px" disabled="disabled"/></td>
                    </tr>
                    <tr>
                      <td>Location</td>
                      <td><input value="<?php echo $headerArray['CUSTOMER_LOCATION']?>" name="txtCustLocation" type="text" id="txtCustLocation" style="width:220px" disabled="disabled"/></td>
                      <td>Contact Person</td>
                      <td><input value="<?php echo $headerArray['CONTACT_PERSON']?>" name="txtContactPerson" type="text" id="txtContactPerson" style="width:190px" disabled="disabled"/></td>
                      <td>Marketer</td>
                      <td><input value="<?php echo $headerArray['MARKETER']?>" name="txtMarketer" type="text" id="txtMarketer" style="width:180px" disabled="disabled"/></td>
                    </tr>
                  </table></td>
              </tr>
              <tr >
                <td><table width="100%" border="0" cellspacing="1" cellpadding="1" class="normalfnt tableBorder_allRound">
                    <tr>
                      <td width="12%">Type</td>
                      <td width="25%"><select style="width:220px" id="cboSubContractType" name="cboSubContractType" class="validate[required]">
                          <option value=""></option>
                          <option value="0" <?php echo $headerArray['COMPANY_TO_TYPE']=='0'? "selected='selected'":""?>>Internal Transfer</option>
                          <option value="1" <?php echo $headerArray['COMPANY_TO_TYPE']=='1'? "selected='selected'":""?>>Out Side Transfer</option>
                        </select></td>
                      <td width="11%">Sub Contractor</td>
                      <td width="22%"><select style="width:190px" id="cboSubContractor" name="cboSubContractor" class="validate[required]">
                          <?php echo loadSubContractor($headerArray['COMPANY_TO_TYPE'],$headerArray['SUB_CONTRACTOR_ID'])?>
                        </select></td>
                      <td width="9%">&nbsp;</td>
                      <td width="21%">&nbsp;</td>
                    </tr>
                    <tr>
                      <td>Remarks</td>
                      <td colspan="5"><textarea name="txtRemarks" id="txtRemarks" style="width:514px;height:30px"><?php echo $headerArray['REMARKS']; ?></textarea></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td align="right"><a class="button white small" id="butAddSalesOrders">Add Sales Orders</a></td>
              </tr>
              <tr>
                <td><div style="width:900px;height:300px;overflow:scroll" >
                    <table class="bordered" id="tblMain" style="width:1340px" >
                      <thead>
                        <tr>
                          <th width="1%" >Del</th>
                          <th width="10%" >Sales Order</th>
                          <th width="10%" >Contract Type</th>
                          <th width="8%" >Graphic No</th>
                          <th width="5%" >Sample  No</th>
                          <th width="8%" >Style</th>
                          <th width="10%" >Placement</th>
                          <th width="6%" >Combo</th>
                          <th width="7%" >Ground Color</th>
                          <th width="5%" >Revision No</th>
                          <th width="5%" >Qty</th>
                          <th width="4%" >Size</th>
                          <th width="4%" >Price</th>
                          <th width="8%" >PSD</th>
                          <th width="8%" >Delivery Date</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php 
					  while($row = mysqli_fetch_array($detailArray))
					  {
						  $groundColor	= getGroundColor($row['SAMPLE_NO'],$row['SAMPLE_YEAR'],$row['PLACEMENT'],$row['COMBO'],$row['REVISION_NO']);
						  createHTML($row['SALES_ORDER_ID'],$row['SALES_ORDER_NO'],$row['STYLE_NO'],$row['GRAPHIC_NO'],$row['CONCAT_SAMPLE_NO'],$row['PLACEMENT'],$row['PART_NO'],$row['COMBO'],$row['REVISION_NO'],$row['QTY'],$row['PRICE'],$row['PDS_DATE'],$row['DELIVERY_DATE'],$row['SUB_CONTR_JOB_ID'],$groundColor);
					  }
					  ?>
                      </tbody>
                    </table>
                  </div></td>
              </tr>
              <tr>
                <td align="center" class="">&nbsp;</td>
              </tr>
              <tr>
                <td align="center" class="tableBorder_allRound"><a name="butNew"  class="button white medium" id="butNew">New</a>
                  <?php if($permision_save==1){ ?>
                  <a id="butSave" name="butSave"  class="button white medium">Save</a>
                  <?php } ?>
                  <a id="butConfirm" name="butConfirm" style=" <?php if(($permision_confirm==1)){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>" class="button white medium">Approve</a><a id="butRevise" name="butRevise" style=" <?php if(($permision_revise==1)){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>" class="button white medium">Revise</a> <a id="butReport" name="butReport"  class="button white medium">Report</a> <a href="main.php"  class="button white medium">Close</a></td>
              </tr>
            </table></td>
      </table>
    </div>
  </div>
</form>
<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1"> 
</div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</body>
</html>
<?php
function getHeaderDetails($serialNo,$serialYear)
{
	global $obj_trn_order_sub_contract_header;
	global $db;
		
	$cols	= "trn_order_sub_contract_header.ORDER_NO   AS ORDER_NO,
			   trn_order_sub_contract_header.ORDER_YEAR AS ORDER_YEAR,
			   CU.strName                               AS CUSTOMER_NAME,
			   PT.strDescription                        AS PAYMENT_TERM,
			   FC.strDescription                        AS CURRENCY,
			   OH.strContactPerson         	            AS CONTACT_PERSON,
			   SU.strUserName                           AS MARKETER,
			   CL.strName                               AS CUSTOMER_LOCATION,
			   COMPANY_TO_TYPE							AS COMPANY_TO_TYPE,
			   SUB_CONTRACTOR_ID						AS SUB_CONTRACTOR_ID,
			   REMARKS									AS REMARKS,
			   DATE										AS DATE,
			   DELIVERY_DATE							AS DELIVERY_DATE ";
			   
	$join   = "INNER JOIN trn_orderheader OH
					ON OH.intOrderNo = trn_order_sub_contract_header.ORDER_NO
					  AND OH.intOrderYear = trn_order_sub_contract_header.ORDER_YEAR
				  INNER JOIN mst_customer CU
					ON CU.intId = OH.intCustomer
				  INNER JOIN mst_financepaymentsterms PT
					ON PT.intId = OH.intPaymentTerm
				  INNER JOIN mst_financecurrency FC
					ON FC.intId = OH.intCurrency
				  INNER JOIN sys_users SU
					ON SU.intUserId = OH.intMarketer
				  INNER JOIN mst_customer_locations_header CL
					ON CL.intId = OH.intCustomerLocation ";
	
	$where	= "trn_order_sub_contract_header.SUB_CONTRACT_NO = '".$serialNo."'
  				AND trn_order_sub_contract_header.SUB_CONTRACT_YEAR = '".$serialYear."' " ;
	$db->connect();
	$result = $obj_trn_order_sub_contract_header->select($cols,$join,$where, $order = null, $limit = null);	
	$db->disconnect();
	$row = mysqli_fetch_array($result);
	return $row;
}

function getDetails($serialNo,$serialYear)
{
	global $db;
	
	global $obj_trn_order_sub_contract_details;
	
	$cols	= "OD.intSalesOrderId 							AS SALES_ORDER_ID,
			   OD.strSalesOrderNo 							AS SALES_ORDER_NO,
			   OD.strStyleNo      							AS STYLE_NO,
			   OD.strGraphicNo    							AS GRAPHIC_NO,
			   OD.intSampleNo								AS SAMPLE_NO,
			   OD.intSampleYear								AS SAMPLE_YEAR,
			   CONCAT(OD.intSampleNo,'/',OD.intSampleYear) 	AS CONCAT_SAMPLE_NO,
			   OD.strPrintName    							AS PLACEMENT,
			   OD.intPart         							AS PART_NO,
			   OD.strCombo        							AS COMBO,
			   OD.intRevisionNo   							AS REVISION_NO,
			   trn_order_sub_contract_details.SUB_CONTR_JOB_ID AS SUB_CONTR_JOB_ID,
			   trn_order_sub_contract_details.QTY          	AS QTY,
			   trn_order_sub_contract_details.PRICE         AS PRICE,
			   trn_order_sub_contract_details.PDS_DATE     	AS PDS_DATE,
			   trn_order_sub_contract_details.DELIVERY_DATE AS DELIVERY_DATE ";
			   
	$join   = "INNER JOIN trn_order_sub_contract_header SH
					ON SH.SUB_CONTRACT_NO = trn_order_sub_contract_details.SUB_CONTRACT_NO
					AND SH.SUB_CONTRACT_YEAR = trn_order_sub_contract_details.SUB_CONTRACT_YEAR
			   INNER JOIN trn_orderdetails OD
			     	ON OD.intOrderNo = SH.ORDER_NO
				 	AND OD.intOrderYear = SH.ORDER_YEAR 
				 	AND OD.intSalesOrderId = trn_order_sub_contract_details.SALES_ORDER_ID ";
	
	$where	= "SH.SUB_CONTRACT_NO = '".$serialNo."'
  				AND SH.SUB_CONTRACT_YEAR = '".$serialYear."' " ;
	$db->connect();
	$result = $obj_trn_order_sub_contract_details->select($cols,$join,$where, $order = null, $limit = null);	
	$db->disconnect();
	return $result;
}

function createHTML($sales_order_id,$sales_order_no,$style_no,$graphic_no,$sampleNo,$placement,$part_no,$combo,$revisionNo,$qty,$price,$psdDate,$deliveryDate,$jobId,$groundColor)
{
	$a	   = $sales_order_id;
	$maxPrice	= getPrice($sampleNo,$revisionNo,$combo,$placement,$jobId);

	$html  = "<tr>".
               "<td class=\"td_del\" style=\"text-align:center\"><img src=\"images/del.png\"></td>".
               "<td class=\"td_salesOrder\" id=\"".$sales_order_id."\">".$sales_order_no."</td>".
               "<td class=\"td_subContractJobType\"><select name=\"cboJob$a\" id=\"cboJob$a\" style=\"width:122px\"  class=\"validate[required] cls_jobType\">".loadCombo($jobId)."</td>".
               "<td class=\"td_graphicNo\">".$graphic_no."</td>".
			   "<td class=\"td_sampleNo\">".$sampleNo."</td>".
               "<td class=\"td_styleNo\">".$style_no."</td>".           
               "<td class=\"td_placement\">".$placement."</td>".
               "<td class=\"td_combo\">".$combo."</td>".
               "<td class=\"td_groundColor\">".$groundColor."</td>".
               "<td class=\"td_revisionNo\">".$revisionNo."</td>".
               "<td class=\"td_qty\"><input type=\"text\" name=\"txtQty$a\" id=\"txtQty$a\" style=\"width:60px;text-align:right\" class=\"validate[min[0],max[$qty],custom[integer]]\" value=\"".$qty."\" /></td>".
               "<td class=\"td_size\" style=\"text-align:center\"><img src=\"images/add_new.png\" class=\"mouseover cls_addSize\" /></td>".
               "<td class=\"td_price\"><input type=\"text\" name=\"txtPrice$a\" id=\"txtPrice$a\" style=\"width:80px;text-align:right\" value=\"".$price."\" class=\"validate[min[0],max[$maxPrice],custom[number]]\" disabled=\"disabled\"/></td>".
               "<td class=\"td_PSD\"><input type=\"text\" name=\"txtPSD$a\" id=\"txtPSD$a\" style=\"width:80px\" value=\"".$psdDate."\" onmousedown=\"DisableRightClickEvent();\" onmouseout=\"EnableRightClickEvent();\" onkeypress=\"return ControlableKeyAccess(event);\"  onclick=\"return showCalendar(this.id, '%Y-%m-%d');\" /><input type=\"reset\" style=\"visibility:hidden;width:1px\"   onclick=\"return showCalendar(this.id, '%Y-%m-%');\" /></td>".
               "<td class=\"td_deliveryDate\"><input name=\"txtDelDate$a\" type=\"text\" id=\"txtDelDate$a\" style=\"width:80px\" value=\"".$deliveryDate."\" onmousedown=\"DisableRightClickEvent();\" onmouseout=\"EnableRightClickEvent();\" onkeypress=\"return ControlableKeyAccess(event);\"  onclick=\"return showCalendar(this.id, '%Y-%m-%d');\" /><input type=\"reset\" style=\"visibility:hidden;width:1px\"   onclick=\"return showCalendar(this.id, '%Y-%m-%');\" /></td>".
              "</tr>";
	echo $html;
}

function loadCombo($jobId)
{
	global $obj_mst_sub_contract_job_types;
	global $db;
	
	$cols	= "mst_sub_contract_job_types.ID,
				mst_sub_contract_job_types.NAME ";
			   
	$where	= "mst_sub_contract_job_types.STATUS = 1 ";
	
	$order  = "mst_sub_contract_job_types.NAME ASC";
	$db->connect();
	$result = $obj_mst_sub_contract_job_types->select($cols,$join = null,$where, $order = null, $limit = null);	
	$html = "<option value=\"".""."\">".""."</option>";
	while($row = mysqli_fetch_array($result))
	{
		if($row['ID']==$jobId)
			$html .= "<option value=\"".$row['ID']."\" selected=\"selected\">".$row['NAME']."</option>";
		else
			$html .= "<option value=\"".$row['ID']."\">".$row['NAME']."</option>";
	}
	$db->disconnect();
	return $html;
}

function loadSubContractor($subContractType,$subContractId)
{
	global $db;
	global $obj_mst_subcontractor;
	
	$db->connect();		//open connection.	
	
	$html = "<option value="."".">&nbsp;</option>";
	
	$cols	= "SUB_CONTRACTOR_ID,
	           SUB_CONTRACTOR_NAME";
			   
	$where = "STATUS = 1 AND INTER_COMPANY = $subContractType";
	$result = $obj_mst_subcontractor->select($cols,$join = null,$where, $order = "SUB_CONTRACTOR_NAME", $limit = null);	
	
	while($row = mysqli_fetch_array($result))
	{
		if($row['SUB_CONTRACTOR_ID'] == $subContractId)
			$html .= "<option value=\"".$row['SUB_CONTRACTOR_ID']."\" selected=\"selected\">".$row['SUB_CONTRACTOR_NAME']."</option>";
		else
			$html .= "<option value=\"".$row['SUB_CONTRACTOR_ID']."\">".$row['SUB_CONTRACTOR_NAME']."</option>";
	}
	$db->disconnect();	//close connection.
	return $html;	
}

function getGroundColor($sampleNo,$sampleYear,$printName,$comboId,$revisionNo)
{
	global $db;
	global $obj_trn_sampleinfomations_details;
	
	$cols	= " DISTINCT G.strName";
			   
	$join	= "Inner Join mst_colors_ground G 
					ON trn_sampleinfomations_details.intGroundColor = G.intId";
	
	$where = "trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear'  AND
					trn_sampleinfomations_details.strPrintName =  '$printName' AND
					trn_sampleinfomations_details.strComboName =  '$comboId' AND
					trn_sampleinfomations_details.intRevNo =  '$revisionNo'";
	$db->connect();
	$result1 = $obj_trn_sampleinfomations_details->select($cols,$join,$where, $order = null, $limit = null);
	$db->disconnect();
	$row	= mysqli_fetch_array($result1);
	return $row['strName'];	
}

function getPrice($sampleNo,$revisionNo,$combo,$printName,$jobType)
{
	global $db;
	global $obj_trn_sampleinfomations_prices_sub_contract_job;
	$sampleNo			= explode('/',$sampleNo);
	$amount = 0;
	$cols	= "COALESCE(dblPrice,0) AS PRICE";
			   
	$where = "intSampleNo = '".$sampleNo[0]."'
				AND intSampleYear = '".$sampleNo[1]."'
				AND intRevisionNo = '$revisionNo'
				AND strCombo = '$combo'
				AND strPrintName = '$printName'
				AND intSubContractJobID = '$jobType'";
	$db->connect();		//open connection.	
	$result = $obj_trn_sampleinfomations_prices_sub_contract_job->select($cols,$join = null,$where, $order = null, $limit = null);		
	$db->disconnect();	//close connection.
	while($row = mysqli_fetch_array($result))
	{
		$amount	= $row['PRICE'];
	}	
	return $amount;	
}
?>
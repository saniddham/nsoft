<?php

$thisFilePath =  $_SERVER['PHP_SELF'];
?>
<style type="text/css">

.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntWhite {
	font-family: Verdana;
	font-size: 11px;
	color: #FFF;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.sampleNo{
	color: #039; font-weight: bold; font-size: 12px; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;	
}
.part{
	color: #096CBD;
	font-weight: bold;
	font-size: 13px;
	font-family: "Comic Sans MS", cursive;
}
.tableBorder_allRound{
	
	border: 1px solid #CCCCCC;
	border-radius-bottomright:10px;
	border-radius-bottomleft:10px;
	border-radius-topright:10px;
	border-radius-topleft:10px;
}
</style>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Marketing Confirm Email</title>
<style type="text/css">
.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntWhite {
	font-family: Verdana;
	font-size: 11px;
	color: #FFF;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.sampleNo{
	color: #039; font-weight: bold; font-size: 12px; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;	
}
.part{
	color: #096CBD;
	font-weight: bold;
	font-size: 12px;
	font-family: "Comic Sans MS", cursive;
}
.tableBorder_allRound{
	
	border: 1px solid #CCCCCC;
	-moz-border-radius-bottomright:10px;
	-moz-border-radius-bottomleft:10px;
	-moz-border-radius-topright:10px;
	-moz-border-radius-topleft:10px;
}
.tblBorder{
	border-top:solid;
	border-top-width:thin;
	border-bottom:solid;
	border-bottom-width:thin;
	border-left:solid;
	border-left-width:thin;
	border-right:solid;
	border-right-width:thin;
	border-color:#dce9f9;
}
</style>
</head>

<body>
<table  width="595" border="0" cellspacing="0" cellpadding="2" class="tblBorder">
  <tr>
    <td colspan="3" class="normalfnt" style="color:#3E437D">&nbsp;</td>
  </tr>
  <tr>
    <td width="92" class="normalfnt">Dear <strong>All</strong>,</td>
    <td width="482">&nbsp;</td>
    <td width="5">&nbsp;</td>
  </tr>
   <tr>
    <td width="92"></td>
    <td width="482" class="normalfnt" style="color:#00C"><b></b></td>
    <td width="5">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Purchase Order(<?php echo $con_orderNo;?>)  has been Rejected and not taken any action within 48 hrs.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><span class="normalfnt1">Due to this reason</span> the <span class="normalfnt1">account</span> of following user <span class="normalfnt1">who raised the PO</span>, <span class="normalfnt1"> has been disabled</span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">User : <?php echo $userName;?></td>
    <td>&nbsp;</td>
  </tr>

  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
 
  <tr>
    <td height="50" colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0" class="normalfnt">
        <tr>
                                                </tr>
    </table></td>
  </tr>
     
  <tr>
    <td class="normalfnt">Thanks,</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="normalfnt"><strong>admin</strong><br />
    ...................</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="normalfnt">(This is a<strong> <span style="color:#0000FF">Nsoft</span> </strong>system generated email.)</td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>

<?php 

	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	$headCompanyId = $_SESSION["headCompanyId"];
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once "{$backwardseperator}class/customerAndOperation/cls_textile_stores.php";
	
	$requestType = $_REQUEST['requestType'];
	
	if($requestType=='getSalesOrderNo')
	{
		$orderYear 	= $_REQUEST['orderYear'];	
		$orderNo 	= $_REQUEST['orderNo'];	
		$sql = "SELECT DISTINCT
					trn_orderdetails.strSalesOrderNo,intSalesOrderId, 
					mst_part.strName as part   
				FROM trn_orderdetails 
						Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId
				WHERE
					trn_orderdetails.intOrderNo 	=  '$orderNo' AND
					trn_orderdetails.intOrderYear 	=  '$orderYear'
				order by strSalesOrderNo
				";
		$result = $db->RunQuery($sql);
		echo "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			echo "<option value=\"".$row['intSalesOrderId']."\">".$row['strSalesOrderNo']."/".$row['part']."</option>";
		}
		
	}
	if($requestType=='loadOrderNo')
	{
		$orderYear 	= $_REQUEST['orderYear'];
		$sql 		= "	SELECT
							trn_orderheader.intOrderNo
						FROM
							trn_orderheader
							Inner Join mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
						WHERE
							trn_orderheader.intOrderYear 	=  '$orderYear' 	AND
							trn_orderheader.intStatus 		=  '1' 				AND
							mst_locations.intCompanyId 		=  '$headCompanyId'
						ORDER BY
							trn_orderheader.intOrderNo DESC
						";
		$result 	= $db->RunQuery($sql);
		$orderNo	= '<option value=""></option>';
		while($row = mysqli_fetch_array($result))
			$orderNo	.= '<option value="'.$row['intOrderNo'].'">'.$row['intOrderNo'].'</option>';
			
		$response['orderNo']			= $orderNo;
		echo json_encode($response);
	}
	else if($requestType=='loadAllComboDetails')
	{
		$year				= $_REQUEST['year'];
		$graphicNo			= $_REQUEST['graphicNo'];
		$styleId			= $_REQUEST['styleId'];
		$customerPONo		= $_REQUEST['customerPONo'];
		$orderNo			= $_REQUEST['orderNo'];
		$salesOrderNo		= $_REQUEST['salesOrderNo'];
		$customerId			= $_REQUEST['customerId'];
		$locationFlag		= 0;
		$companyFlag		= 0;
		echo loadAllComboDetails($year,$graphicNo,$styleId,$customerPONo,$orderNo,$salesOrderNo,$customerId,$locationFlag,$location,$companyFlag,$company);	
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////	
function loadAllComboDetails($year,$graphicNo,$styleId,$customerPONo,$orderNo,$salesOrderNo,$customerId,$locationFlag,$location,$companyFlag,$company) 	
{
	$obj = new cls_texttile();
	echo $obj->loadAllSearchComboDetails_with_sampNo('','',$year,$graphicNo,$styleId,$customerPONo,$orderNo,$customerId,$locationFlag,$location,$companyFlag,$company);	
}
//////////////////////////////////////////////////////////////////////////////////////////////////	
?>
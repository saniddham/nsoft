<?php 

	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$locationId 	= $_SESSION['CompanyID'];
	$companyId 		= $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	$requestType = $_REQUEST['requestType'];
	
	if($requestType=='saveDetails')
	{
		$db->begin();
		
		$rollBackFlag=0;
		$plan1 		= val($_REQUEST['plan1']);
		$plan2 		= val($_REQUEST['plan2']);
		$orderNo 	= $_REQUEST['orderNo'];
		$orderYear 	= $_REQUEST['orderYear'];
		$salesNo 	= $_REQUEST['salesNo'];
		
		$sql = "SELECT
				trn_orderdetails.intQty 
			FROM trn_orderdetails 
			WHERE
				trn_orderdetails.intOrderNo 		=  '$orderNo' AND
				trn_orderdetails.intOrderYear 		=  '$orderYear' AND
				trn_orderdetails.intSalesOrderId 	=  '$salesNo'
			";
		$result = $db->RunQuery2($sql);
		$Mrow=mysqli_fetch_array($result);
		if($Mrow['intQty'] != ($plan1+$plan2)){
			$rollBackFlag=1;	
			$rollBackMsg = "Total plan Qty not tally with order Qty.";
		}
		
		
		$sql = "INSERT INTO `ware_bulkallocation_header` (`intOrderNo`, `intOrderYear`, `intSalesOrderId`, `int25PlanGarmentQty`, `int75PlanGarmentQty`, `dtmCreatedDate`, `intCreatedBy`) 
		VALUES ('$orderNo', '$orderYear', '$salesNo', '$plan1', '$plan2', now(), '$userId')";
		$result = $db->RunQuery2($sql);
		$sqlM=$sql;
		if(!$result){
		$sql = "UPDATE `ware_bulkallocation_header` SET `int25PlanGarmentQty`='$plan1', `int75PlanGarmentQty`='$plan2', `dtmModifiedDate`=now(), `intModifiedBy`='$userId' WHERE (`intOrderNo`='$orderNo') AND (`intOrderYear`='$orderYear') AND (`intSalesOrderId`='$salesNo') LIMIT 1";
	    $result = $db->RunQuery2($sql);
		$sqlM=$sql;
		}
		
		if(!$result){
			$rollBackFlag=1;
		}
		
		if($rollBackFlag==1){
			$db->rollback();	
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->commit();
			$response['type'] 		= 'pass';
			if($result2)
			$response['msg'] 		= 'Updated successfully.';
			else if($result)
			$response['msg'] 		= 'Saved successfully.';
		}
		
	echo json_encode($response);
	
	$db->commit();
	
	}
	else if($requestType=='saveAllocationDetails')
	{
		$arrData 	= json_decode($_REQUEST['arrData'],true);
		$orderNo 	= $_REQUEST['orderNo'];
		$orderYear 	= $_REQUEST['orderYear'];
		$salesNo 	= $_REQUEST['salesId'];
		$itemId 	= $_REQUEST['itemId'];
		$stage		= $_REQUEST['stage'];
		
			
		foreach($arrData as $x)
		{
			######### GET GRN DETAILS ######################
			$sql = "SELECT
						ware_stocktransactions_bulk.intCurrencyId,
						ware_stocktransactions_bulk.dtGRNDate,
						ware_stocktransactions_bulk.dblGRNRate
						FROM ware_stocktransactions_bulk
					WHERE
						ware_stocktransactions_bulk.intLocationId =  '$locationId' AND
						ware_stocktransactions_bulk.intGRNNo =  '".$x['grnNo']."' AND
						ware_stocktransactions_bulk.intGRNYear =  '".$x['grnYear']."' AND
						ware_stocktransactions_bulk.intItemId =  '$itemId'
					";
			$result = $db->RunQuery($sql);
			$row=mysqli_fetch_array($result);
			$currencyId = $row['intCurrencyId'];
			$dtGRNDate = $row['dtGRNDate'];
			$dblGRNRate = $row['dblGRNRate'];
			################################################
			$db->begin();
			
			if($x['type']=='Allo')
			{
				$xtype = 'ALLOCATE';
				$p = 1;
			}
			else
			{
				$xtype = "UNALLOCATE";	
				$p = -1;
			}
				$sql = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,`intDocumntYear`,
				`intGRNNo`,`intGRNYear`,`dtGRNDate`,`intCurrencyId`,`dblGRNRate`,`intItemId`,`dblQty`,`strType`,`intOrderNo`,
				`intOrderYear`,`intSalesOrderId`,`intUser`,`dtDate`) VALUES ('$companyId','$locationId','0','0','".$x['grnNo']."',
				'".$x['grnYear']."','$dtGRNDate','$currencyId','$dblGRNRate','$itemId','".($x['qty']*-1*$p)."',
				'$xtype','$orderNo','$orderYear','$salesNo','$userId',now())";	
			
				$result = $db->RunQuery2($sql);
			if($result)
			{
				
				$sql = "INSERT INTO `ware_stocktransactions` (`intCompanyId`,`intLocationId`,`intDocumentNo`,`intDocumntYear`,
				`intGRNNo`,`intGRNYear`,`dtGRNDate`,`intCurrencyId`,`dblGRNRate`,`intItemId`,`dblQty`,`strType`,`intOrderNo`,
				`intOrderYear`,`intSalesOrderId`,`intUser`,`dtDate`) VALUES ('$companyId','$locationId','0','0','".$x['grnNo']."',
				'".$x['grnYear']."','$dtGRNDate','$currencyId','$dblGRNRate','$itemId','".($x['qty']*$p)."',
				'$xtype','$orderNo','$orderYear','$salesNo','$userId',now())";	
				$db->RunQuery2($sql);
				
				############ update allocate details table ########################
				$sql = "SELECT
						Sum(ware_stocktransactions.dblQty) AS allocatedQty
						FROM ware_stocktransactions
						WHERE
						ware_stocktransactions.intLocationId =  '$locationId' AND
						ware_stocktransactions.intOrderNo =  '$orderNo' AND
						ware_stocktransactions.intOrderYear =  '$orderYear' AND
						ware_stocktransactions.intSalesOrderId =  '$salesNo' AND
						ware_stocktransactions.intItemId =  '$itemId' AND
						(ware_stocktransactions.strType =  'ALLOCATE' OR ware_stocktransactions.strType =  'UNALLOCATE' )
						";
				$result = $db->RunQuery2($sql);
				$row=mysqli_fetch_array($result);
				$allocatedQty = (float)$row['allocatedQty'];
				
					if($stage==1)
						$x_stage=25;
					else if($stage==2)
						$x_stage=75;
					
				$sql = "INSERT INTO `ware_bulkallocation_details` (intLocationId,`intOrderNo`,`intOrderYear`,`intSalesOrderId`,`intItemId`,`dbl{$x_stage}AllocatedQty`) VALUES ('$locationId','$orderNo','$orderYear','$salesNo','$itemId','$allocatedQty')";
				$result = $db->RunQuery2($sql);
				if(!$result)
				{
					$sql = "UPDATE `ware_bulkallocation_details` SET `dbl{$x_stage}AllocatedQty`='$allocatedQty' WHERE (`intLocationId`='$locationId') AND (`intOrderNo`='$orderNo') AND (`intOrderYear`='$orderYear') AND (`intSalesOrderId`='$salesNo') AND (`intItemId`='$itemId')  ";
				$db->RunQuery2($sql);
				}
				
				$data['itemTotalQty'] = $allocatedQty;
				$data['stage'] = $stage;
				###################################################################      
				
				$db->commit();
			}
			else
			{
				$db->rollback();	
				
			}
		}
		
		echo json_encode($data);
		
	$db->commit();
	}
?>
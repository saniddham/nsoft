var rowIndex = 0;
var pubStage = 0;
$(document).ready(function(){
	
	//$('#cboOrderYear').live('change',changeOrderYear);
//-----------------------------------------------------------------
	$("#cboOrderYear").live('change',function(){
	 $('#cboGraphicNo').val('');
	 $('#cboStyle').val('');
	 $('#cboPONo').val('');
	 $('#cboOrderNo').val('');
	 $('#cboCustomer').val('');
	 $('#cboSalesOrderNo').val('');
	 document.frmAllocation.submit();	
	});
//-----------------------------------------------------------------
	
	$('#butNew').live('click',refreshPage);
	
	$("#butClose2").live('click',function(){
		disablePopup();
	});
	
	$('#frmAllocationPopUp #butSave').live('click',savePopUpDetails);
	$('.clstxtAllocate').live('keydown',allocatekeydown)
	$('.clstxtUnAllocate').live('keydown',UNallocatekeydown)
		
		
		
		
	$('#cboOrderNo').change(function(){
		var orderYear = $('#cboOrderYear').val();
		var orderNo	  = $('#cboOrderNo').val();
		var url = "allocation-db-get.php?requestType=getSalesOrderNo&orderNo="+orderNo+"&orderYear="+orderYear;
		var obj = $.ajax({url:url,async:false});
		$('#cboSalesOrderNo').html(obj.responseText);
		
		 $('#cboPONo').val('');
		 $('#cboCustomer').val('');
		 $('#cboSalesOrderNo').val('');
		 document.frmAllocation.submit();	
	});
	
	$('#cboSalesOrderNo').change(function(){
		document.frmAllocation.submit();	
	});
//-----------------------------------------------------------------
	$(".search").live('change',loadAllCombo);
		
//-----------------------------------------------------------------
	$('#frmAllocation #butSave').click(function(){
		if(!$("#frmAllocation").validationEngine('validate'))
		{
			return;
		}
		var orderNo 	= $('#cboOrderNo').val();
		var orderYear   = $('#cboOrderYear').val();
		var salesNo		= $('#cboSalesOrderNo').val();
		
		var plan1  = $('#txt1Plan').val();
		var plan2  = $('#txt2Plan').val();
		
		var data ="requestType=saveDetails&plan1="+plan1+"&plan2="+plan2+"&orderNo="+orderNo+"&orderYear="+orderYear+"&salesNo="+salesNo;

		var url = "allocation-db-set.php";
				var obj = $.ajax({
					url:url,
					
					dataType: "json",  
					data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
					//data:'{"requestType":"addsampleInfomations"}',
					async:false,
					
					success:function(json){
							$('#frmAllocation #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							if(json.type=='pass')
							{
								var t=setTimeout("alertx()",1000);
							}
						},
					error:function(xhr,status){
							
							$('#frmAllocation #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",3000);
						}		
					});	
			//--------------
	});
	
	$('#txt1Plan').keyup(function(){
		planFirstQtyCalculate($(this).val().replace(',',''));	
		savePlanQtys();
	});
	$('#txt2Plan').keyup(function(){
		planSecondQtyCalculate2($(this).val().replace(',',''));	
		savePlanQtys();
	});
	$('#txt3Plan').keyup(function(){
		planSecondQtyCalculate3($(this).val().replace(',',''));	
		savePlanQtys();
	});
	$('.but1Allocated').live('click',view1allocatePopUp);
	$('.but2Allocated').live('click',view2allocatePopUp);
	$('.but3Allocated').live('click',view3allocatePopUp);
	
	
});

function planFirstQtyCalculate(qty)
{
	var errorFlag=0;
	
	if(qty==''){
		qty=0;
	}
	if((isNaN(qty))){
		qty=0;
		return false;
	}
	var totGarmentQty=parseFloat($('#divTotGarmQty').html().replace(',',''));
	var GarmentQty2=parseFloat(totGarmentQty)-parseFloat(qty);
	
		
	var tmpMminGarmQty=0;
	$('.clsQty').each(function(){
		var allocatedQty=parseFloat($(this).parent().parent().find('.cls1Allocated').html().replace(',',''));
		var sampConPC=parseFloat($(this).parent().parent().find('.sampConPC').html().replace(',',''));
		minGarmQty=parseInt(allocatedQty/sampConPC+0.999999);

		if(minGarmQty>tmpMminGarmQty){
			tmpMminGarmQty=minGarmQty;
		}

		var planQty = parseFloat(qty)*parseFloat($(this).attr('id').replace(',',''));
			if(allocatedQty>planQty){
				errorFlag++;
			}
	});
	
	if(qty>totGarmentQty){
		alert("Invalid Garment Qty");
		$('#txt1Plan').val(tmpMminGarmQty);
		$('#txt2Plan').val(totGarmentQty-tmpMminGarmQty);
		planFirstQtyCalculate(tmpMminGarmQty);
		return false;
	}
	
	$('.clsQty').each(function(){
		var planQty = parseFloat(qty)*parseFloat($(this).attr('id').replace(',',''));
		$(this).html(planQty.toFixed(4));
	});
	$('#txt2Plan').val(GarmentQty2);
		
	if(errorFlag!==0){
			//alert("Invalid Garment Qty. Allocated Quantities cant exceed the Planed Quantities !");
		//$('#txt1Plan').val(tmpMminGarmQty);
		//GarmentQty2=totGarmentQty-tmpMminGarmQty
		//$('#txt2Plan').val(GarmentQty2);
		//planFirstQtyCalculate(tmpMminGarmQty);
	}
	//--------75% Plan Qty---------------------------------------------
		$('.clsQty2').each(function(){ 
			var conPC=parseFloat($(this).parent().parent().find('.firstDayConPC').html().replace(',',''));
			var conPC2=parseFloat($(this).parent().parent().find('.secondDayConPC').html().replace(',',''));
			if(conPC2>0){
				conPC=conPC2;
			}
			var planQty = parseFloat(GarmentQty2)*conPC;
			$(this).html(planQty.toFixed(4));
		});
	
	
}
function planSecondQtyCalculate2(qty)
{
	if(qty=='')
		qty=0;
	var garmentActual1 = $('#txt1Actual').val();
		if(garmentActual1=='')
			garmentActual1=0;
	$('.clsQty2').each(function(){
		//alert(1);
		
		var firstActual = $(this).parent().parent().find('td:eq(4)').html().replace(',','');
		if(firstActual=='')
			firstActual=0;
		var total = parseFloat(qty)*parseFloat(firstActual)/parseFloat(garmentActual1);
		$(this).html(Math.round(total.toFixed(4)));
	});
}

function planSecondQtyCalculate3(qty)
{
	if(qty=='')
		qty=0;
	var garmentActual1 = $('#txt2Actual').val().replace(',','');
		if(garmentActual1=='')
			garmentActual1=0;
	$('.clsQty3').each(function(){
		//alert(1);
		
		var firstActual = $(this).parent().parent().find('td:eq(7)').html().replace(',','');
		if(firstActual=='')
			firstActual=0;
		var total = parseFloat(qty)*parseFloat(firstActual)/parseFloat(garmentActual1);
		$(this).html(Math.round(total.toFixed(4)));
	});
}

function view1allocatePopUp()
{
	mainpopup(1,$(this));
}
function view2allocatePopUp()
{
	mainpopup(2,$(this));
}
function view3allocatePopUp()
{
	mainpopup(3,$(this));
}
function mainpopup(para,obj)
{
	pubStage = para;
	popupWindow3('1');
	if(para==1)
	{
		var itemQty 	= obj.parent().parent().find('.clsQty').html().replace(',','');
		var stage = 1;
	}
	else if(para==2)
	{
		var firstDayActualConPC = obj.parent().parent().find('.firstDayConPC').html().replace(',','');
		var secondDayActualConPC = obj.parent().parent().find('.secondDayConPC').html().replace(',','');
		var sampleActualConPC=firstDayActualConPC;
		if(secondDayActualConPC>0){
		var sampleActualConPC=secondDayActualConPC;
		}
		var totQty 	= $('#divTotGarmQty').html().replace(',','');
		var allcatedQty25 	= obj.parent().parent().find('.cls1Allocated').html().replace(',','');
		//itemQty = parseFloat(totQty)*parseFloat(sampleActualConPC)-parseFloat(allcatedQty25);
		itemQty = parseFloat(totQty)*parseFloat(sampleActualConPC);
		var stage = 2;
	}
	else
	{
		var itemQty 	= obj.parent().parent().find('.clsQty').html().replace(',','');
		var itemQty2 	= obj.parent().parent().find('.clsQty2').html().replace(',','');
		var itemQty3 	= obj.parent().parent().find('.clsQty3').html().replace(',','');
		itemQty = parseFloat(itemQty)+parseFloat(itemQty2)+parseFloat(itemQty3);
		var stage=3;
	}
	
	var itemName 	= obj.parent().parent().find('td:eq(0)').html();
	var itemId 		= obj.parent().parent().find('td:eq(0)').attr('id');
	
	var orderNo		= $('#cboOrderNo').val();
	var orderYear	= $('#cboOrderYear').val();
	var salesId		= $('#cboSalesOrderNo').val();
	rowIndex		= obj.parent().parent().index();
	$('#popupContact1').load('poup.php?itemQty='+itemQty+'&itemName='+URLEncode(itemName)+'&itemId='+itemId+'&orderNo='+orderNo+'&orderYear='+orderYear+'&salesId='+salesId+'&rowIndex='+rowIndex+'&stage='+stage);	
}
function allocatekeydown()
{
	$(this).parent().parent().find('.clstxtUnAllocate').val('');
}

function UNallocatekeydown()
{
	$(this).parent().parent().find('.clstxtAllocate').val('');
}

function savePlanQtys(){

		if(!$("#frmAllocation").validationEngine('validate'))
		{
			return;
		}
		var orderNo 	= $('#cboOrderNo').val();
		var orderYear   = $('#cboOrderYear').val();
		var salesNo		= $('#cboSalesOrderNo').val();
		
		var plan1  = $('#txt1Plan').val();
		var plan2  = $('#txt2Plan').val();
		
		var data ="requestType=saveDetails&plan1="+plan1+"&plan2="+plan2+"&orderNo="+orderNo+"&orderYear="+orderYear+"&salesNo="+salesNo;

		var url = "allocation-db-set.php";
				var obj = $.ajax({
					url:url,
					
					dataType: "json",  
					data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
					//data:'{"requestType":"addsampleInfomations"}',
					async:false,
					
					success:function(json){
							$('#frmAllocation #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							if(json.type=='pass')
							{
								var t=setTimeout("alertx()",1000);
							}
						},
					error:function(xhr,status){
							
							$('#frmAllocation #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",3000);
						}		
					});	
			//--------------
}

function savePopUpDetails()
{
	if($("#frmAllocationPopUp").validationEngine('validate'))
	{
		//$('#frmAllocationPopUp #butSave').hide();
		var arrData = '';
		$('.clstxtAllocate').each(function(){
			var grnNo 	= $(this).parent().parent().find('td:eq(0)').html();
			var grnYear = $(this).parent().parent().find('td:eq(1)').html();
			var qty	= ($(this).val()==''?0:parseFloat($(this).val().replace(',','')));
			if(qty>0)
				arrData += ((arrData)==''?'':',')+'{"grnNo":"'+grnNo+'","grnYear":"'+grnYear+'","qty":"'+qty+'","type":"Allo"}'
		});
		
		$('.clstxtUnAllocate').each(function(){
			var grnNo 	= $(this).parent().parent().find('td:eq(0)').html();
			var grnYear = $(this).parent().parent().find('td:eq(1)').html();
			var qty	= ($(this).val()==''?0:parseFloat($(this).val().replace(',','')));
			if(qty>0)
				arrData += ((arrData)==''?'':',')+'{"grnNo":"'+grnNo+'","grnYear":"'+grnYear+'","qty":"'+qty+'","type":"UN"}'
		});
		if(arrData=='')
		{
			alert('Qty not found.');	
			return;
		}
		
		
		
		
		var url = "allocation-db-set.php?requestType=saveAllocationDetails";
			url+="&orderNo="+$('#cboOrderNo').val();
			url+="&orderYear="+$('#cboOrderYear').val();
			url+="&salesId="+$('#cboSalesOrderNo').val();
			url+="&itemId="+$('#spanItemId').html();
			url+="&stage="+pubStage;
		$.ajax({url:url,
			async:false,
			data:"arrData=["+arrData+"]",
			dataType: "json",  
			success:function(json){				
				$('#frmAllocationPopUp #butSave').validationEngine('showPrompt', 'Saved Sucessfully','pass' /*'pass'*/);
				////////////////////////////////////
				//alert(rowIndex);
				if(json.stage==1)
					$('#tblMainGrid>tbody>tr:eq('+rowIndex+')').find('.cls1Allocated').html(json.itemTotalQty);
				else if(json.stage==2)
				{
					var val =json.itemTotalQty-parseFloat($('#tblMainGrid>tbody>tr:eq('+rowIndex+')').find('.cls1Allocated').html());
					$('#tblMainGrid>tbody>tr:eq('+rowIndex+')').find('.cls2Allocated').html((val*100)/100);
				}
				else if(json.stage==3)
				{
					var val =json.itemTotalQty-parseFloat($('#tblMainGrid>tbody>tr:eq('+rowIndex+')').find('.cls1Allocated').html())-parseFloat($('#tblMainGrid>tbody>tr:eq('+rowIndex+')').find('.cls2Allocated').html());
					$('#tblMainGrid>tbody>tr:eq('+rowIndex+')').find('.cls3Allocated').html((val*100)/100);
				}
					
				var itemQty 	= $('#spanQty').html().replace(',','');
				var itemName 	= $('#spanItemName').html();
				var itemId 		= $('#spanItemId').html();
				
				var orderNo		= $('#cboOrderNo').val();
				var orderYear	= $('#cboOrderYear').val();
				var salesId		= $('#cboSalesOrderNo').val();
				$('#popupContact1').load('poup.php?itemQty='+itemQty+'&itemName='+URLEncode(itemName)+'&itemId='+itemId+'&orderNo='+orderNo+'&orderYear='+orderYear+'&salesId='+salesId);
				//alert(rowIndex);
				
				//alert(rowIndex);
	
			}
		});
	}
}

//-------------------------------------
function alertx()
{
	$('#frmAllocation #butSave').validationEngine('hide')	;
}

function changeOrderYear()
{
	var orderYear 	= $(this).val();
	var url 		= "allocation-db-get.php?requestType=loadOrderNo&orderYear="+orderYear;
	$.ajax({url:url,
			async:false,
			dataType:'json',
			success:function(json){
				$('#cboOrderNo').html(json.orderNo);
			}
	});
}

function refreshPage()
{
	document.location.href = 'allocation.php';	
}
//---------------------------------------------
function loadAllCombo()
{
	//######################
	//####### get values ###
	//######################
	var year 		= $('#cboOrderYear').val();
	var graphicNo 	= $('#cboGraphicNo').val();
	var styleId 	= $('#cboStyle').val();
	var customerPONo= $('#cboPONo').val();
	var orderNo		= $('#cboOrderNo').val();
	var customerId 	= $('#cboCustomer').val();
	var salesOrderNo 	= $('#cboSalesOrderNo').val();
	
	//######################
	//####### create url ###
	//######################
	var url		="allocation-db-get.php?requestType=loadAllComboDetails";
		url	   +="&year="+			year;	
		url	   +="&graphicNo="+		graphicNo;
		url	   +="&styleId="+		styleId;
		url	   +="&customerPONo="+	customerPONo;
		url	   +="&orderNo="+		orderNo;
		url	   +="&salesOrderNo="+		salesOrderNo;
		url	   +="&customerId="+	customerId;
	
	//######################
	//####### send data  ###
	//######################
	$.ajax({url:url,
			async:false,
			dataType:'json',
			success:function(json){
					$('#cboStyle').html(json.styleNo);
					$('#cboGraphicNo').html(json.graphicNo);
					$('#cboPONo').html(json.customerPoNo);
					$('#cboOrderNo').html(json.orderNo);
					$('#cboSalesOrderNo').html(json.salesOrderNo);
					$('#cboCustomer').html(json.customer);
			}
			});
}
//------------------------------------------------------------------------


<?php

session_start();
$backwardseperator 		= "../../../../../";
$mainPath 				= $_SESSION['mainPath'];
$sessionUserId 			= $_SESSION['userId'];
$thisFilePath 			=  $_SERVER['PHP_SELF'];
$locationId				= $_SESSION["CompanyID"];
$headCompanyId 			= $_SESSION["headCompanyId"];

include  	"{$backwardseperator}dataAccess/permisionCheck.inc";

###########################
### get loading values ####
###########################
$orderYear 	= $_REQUEST['cboOrderYear'];
$orderNo 	= $_REQUEST['cboOrderNo'];
$salesOrderId 	= $_REQUEST['cboSalesOrderNo'];
$poNo = $_REQUEST['cboPONo'];
$customer = $_REQUEST['cboCustomer'];
$x_styleNo 	= $_REQUEST['cboStyle'];
$x_graphicNo 	= $_REQUEST['cboGraphicNo'];

if($orderYear==''){
	$orderYear=date('Y');
}
   $sql = "SELECT
trn_orderheader.strCustomerPoNo,
trn_orderheader.intCustomer
FROM trn_orderheader
WHERE
trn_orderheader.intOrderNo =  '$orderNo' AND
trn_orderheader.intOrderYear =  '$orderYear'"; 
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);

$x_poNo = $row['strCustomerPoNo'];
$x_custId = $row['intCustomer'];

################ 1 ACTUAL CALCULATION ####################################
	  	$sql = "SELECT
					ware_bulkallocation_header.intFirstDayPlanGarment,
					ware_bulkallocation_header.intFirstDayActualGarment,
					ware_bulkallocation_header.int25PlanGarment,
					ware_bulkallocation_header.int25ActualGarment,
					ware_bulkallocation_header.int75PlanGarment,
					ware_bulkallocation_header.int75ActualGarment,
					ware_bulkallocation_header.intStage
				FROM `ware_bulkallocation_header`
				WHERE
					ware_bulkallocation_header.intOrderNo =  '$orderNo' AND
					ware_bulkallocation_header.intOrderYear =  '$orderYear' AND
					ware_bulkallocation_header.intSalesOrderId =  '$salesOrderId'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$plan1Qty = $row['intFirstDayPlanGarment'];
			$plan2Qty = $row['int25PlanGarment'];
			$plan3Qty = $row['int75PlanGarment'];
			
			$actual1Qty = $row['intFirstDayActualGarment'];
			$actual2Qty = $row['int25ActualGarment'];
			$actual3Qty = $row['int75ActualGarment'];
			
			$intStage = $row['intStage'];
		}
if($intStage==1)
{
	$sql = "UPDATE `ware_bulkallocation_details` AS A 
			SET `dbl1ActualQty`=(
			
			SELECT
			Sum(ware_stocktransactions.dblQty) AS issueQty
			FROM ware_stocktransactions 
			WHERE
			ware_stocktransactions.intLocationId 	=  A.intLocationId AND
			ware_stocktransactions.intOrderNo 		=  A.intOrderNo AND
			ware_stocktransactions.intOrderYear 	=  A.intOrderYear AND
			ware_stocktransactions.intSalesOrderId 	=  A.intSalesOrderId AND
			(ware_stocktransactions.intItemId=A.intItemId)  AND
			(ware_stocktransactions.strType =  'ISSUE' OR ware_stocktransactions.strType =  'RETSTORES') 
			
			) * -1
			WHERE (A.intLocationId='$locationId') AND (A.intOrderNo='$orderNo') 
			AND (A.intOrderYear='$orderYear') AND (A.intSalesOrderId='$salesOrderId') 
			";
	$db->RunQuery($sql);
	
}
if($intStage==2)
{
	$sql = "UPDATE `ware_bulkallocation_details` AS A 
			SET `dbl25ActualQty`=((
			
			SELECT
			Sum(ware_stocktransactions.dblQty) AS issueQty
			FROM ware_stocktransactions 
			WHERE
			ware_stocktransactions.intLocationId 	=  A.intLocationId AND
			ware_stocktransactions.intOrderNo 		=  A.intOrderNo AND
			ware_stocktransactions.intOrderYear 	=  A.intOrderYear AND
			ware_stocktransactions.intSalesOrderId 	=  A.intSalesOrderId AND
			(ware_stocktransactions.intItemId=A.intItemId)  AND
			(ware_stocktransactions.strType =  'ISSUE' OR ware_stocktransactions.strType =  'RETSTORES') 
			
			) * -1)-dbl1ActualQty
			WHERE (A.intLocationId='$locationId') AND (A.intOrderNo='$orderNo') 
			AND (A.intOrderYear='$orderYear') AND (A.intSalesOrderId='$salesOrderId') 
			";
	$db->RunQuery($sql);
	
}
if($intStage==3)
{
	$sql = "UPDATE `ware_bulkallocation_details` AS A 
			SET `dbl75ActualQty`=((
			
			SELECT
			Sum(ware_stocktransactions.dblQty) AS issueQty
			FROM ware_stocktransactions 
			WHERE
			ware_stocktransactions.intLocationId 	=  A.intLocationId AND
			ware_stocktransactions.intOrderNo 		=  A.intOrderNo AND
			ware_stocktransactions.intOrderYear 	=  A.intOrderYear AND
			ware_stocktransactions.intSalesOrderId 	=  A.intSalesOrderId AND
			(ware_stocktransactions.intItemId=A.intItemId)  AND
			(ware_stocktransactions.strType =  'ISSUE' OR ware_stocktransactions.strType =  'RETSTORES') 
			
			) * -1)-dbl1ActualQty-dbl25ActualQty
			WHERE (A.intLocationId='$locationId') AND (A.intOrderNo='$orderNo') 
			AND (A.intOrderYear='$orderYear') AND (A.intSalesOrderId='$salesOrderId') 
			";
	$db->RunQuery($sql);
	
}
##########################################################################
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bulk Stock Allocation</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="allocation-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>
</head>

<body>
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
<form id="frmAllocation" name="frmAllocation" method="get" action="allocation.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../../libraries/calendar/theme.css" />
<script src="../../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Bulk Stock Allocation</div>
		  <table width="103%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
    <tr><td>
<table  bgcolor="#E8FED8" class="tableBorder_allRound"  width="100%" border="0" cellpadding="0" cellspacing="0"  >
            <tr>
              <td height="22" class="normalfnt">&nbsp;Year</td>
              <td class="normalfnt"><select name="cboOrderYear" id="cboOrderYear" style="width:70px"<?php /*?> class="search"<?php */?>>
                <?php
				
					$sql = "SELECT DISTINCT
							trn_orderheader.intOrderYear
							FROM trn_orderheader
							ORDER BY
							trn_orderheader.intOrderYear DESC";
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						if(($i==0) &&($orderYear==''))
						$orderYear = $row['intOrderYear'];
						
						
						if($row['intOrderYear']==$orderYear)
						echo "<option value=\"".$row['intOrderYear']."\" selected=\"selected\">".$row['intOrderYear']."</option>";	
						else
						echo "<option value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";	
						$i++;
					}
				?>
                </select></td>
              <td class="normalfnt">Style No</td>
              <td class="normalfnt"><select name="cboStyle" id="cboStyle" style="width:130px" class="search">
                <?php
                $html1="<option value=\"\"></option>";
				$html='';
					$sql = "SELECT DISTINCT
							trn_orderdetails.strStyleNo
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
							WHERE
							trn_orderheader.intStatus =  '1'  AND 
							trn_orderdetails.intOrderYear='$orderYear'  AND trn_orderheader.intOrderNo>=0  ";
					if($orderNo>0){		
					$sql .= " AND trn_orderdetails.intOrderNo='$orderNo' ";	
					}
					$sql .= " 
							   
							ORDER BY trn_orderdetails.strStyleNo ASC";
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						$i++;
						if($row['strStyleNo']==$x_styleNo)
						$html .= "<option value=\"".$row['strStyleNo']."\" selected=\"selected\">".$row['strStyleNo']."</option>";	
						else
						$html .= "<option value=\"".$row['strStyleNo']."\">".$row['strStyleNo']."</option>";	
						if(($i==1) && ($x_styleNo=='')){
							$x_styleNoT=$row['strStyleNo'];
						}
					}
					
					if(($orderNo!='') && ($x_styleNo=='')){
					$x_styleNo=$x_styleNoT;
					}
					
					if($orderNo==''){
						$html=$html1.$html;
					}
					else if($i>=1){
						$html=$html.$html1;
					}
					else if($i==0){
						$html=$html1;
					}
					echo $html;
				?></select></td>
              <td class="normalfnt">Graphic No</td>
              <td align="right" class="normalfnt"><select name="cboGraphicNo" id="cboGraphicNo" style="width:120px" class="search">
                <?php
                $html1="<option value=\"\"></option>";
				$html='';
					$sql = "SELECT DISTINCT
							trn_orderdetails.strGraphicNo
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
							WHERE
							trn_orderheader.intStatus =  '1'   AND 
							trn_orderdetails.intOrderYear='$orderYear'  AND trn_orderheader.intOrderNo>0  ";
					if($orderNo>0){		
					$sql .= " AND trn_orderdetails.intOrderNo='$orderNo' ";	
					}
					$sql .= " 
							 
							ORDER BY trn_orderdetails.strGraphicNo ASC";
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						$i++;
						if($row['strGraphicNo']==$x_graphicNo)
						$html .= "<option value=\"".$row['strGraphicNo']."\" selected=\"selected\">".$row['strGraphicNo']."</option>";	
						else
						$html .= "<option value=\"".$row['strGraphicNo']."\">".$row['strGraphicNo']."</option>";	
						if(($i==1) && ($x_graphicNo=='')){
							$x_graphicNoT=$row['strGraphicNo'];
						}
					}
					
					if(($orderNo!='') && ($x_graphicNo=='')){
					$x_graphicNo=$x_graphicNoT;
					}
					
					if($orderNo==''){
						$html=$html1.$html;
					}
					else if($i>=1){
						$html=$html.$html1;
					}
					else if($i==0){
						$html=$html1;
					}
					echo $html;
				?></select></td> </tr>
<tr>
              <td height="22" class="normalfnt">Customer PO</td>
              <td class="normalfnt"><select name="cboPONo" id="cboPONo" style="width:130px" class="search">
                <option value=""></option>
                <?php
					$sql = "SELECT DISTINCT
							trn_orderheader.strCustomerPoNo
							FROM trn_orderheader 
							where intStatus=1   AND 
							trn_orderheader.intOrderYear='$orderYear' AND trn_orderheader.intOrderNo>0  
							AND trn_orderheader.strCustomerPoNo <> '' 
							ORDER BY
							trn_orderheader.strCustomerPoNo ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if(($row['strCustomerPoNo']==$x_poNo) && ($orderNo!=''))
						echo "<option value=\"".$row['strCustomerPoNo']."\" selected=\"selected\">".$row['strCustomerPoNo']."</option>";	
						else
						echo "<option value=\"".$row['strCustomerPoNo']."\">".$row['strCustomerPoNo']."</option>";	
					}
				?>
                </select></td>
              <td class="normalfnt">Customer</td>
              <td colspan="4" class="normalfnt"><select style="width:295px" name="cboCustomer" id="cboCustomer"   class="validate[required] search" >
                <option value=""></option>
                <?php
					$sql = "SELECT DISTINCT 
							trn_orderheader.intCustomer,
							mst_customer.strName
							FROM
							trn_orderheader
							Inner Join mst_customer ON trn_orderheader.intCustomer = mst_customer.intId 
							where  
							trn_orderheader.intOrderYear='$orderYear' ";
					if($orderNo!=''){
					$sql .= " AND trn_orderheader.intOrderNo='$orderNo' ";	
					}
					$sql .= " AND trn_orderheader.intOrderNo>0  
							ORDER BY mst_customer.strName ASC";
							$pp=$sql;
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intCustomer']==$x_custId)
						echo "<option value=\"".$row['intCustomer']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intCustomer']."\">".$row['strName']."</option>";	
					}
				?>
                </select></td>
            </tr> 
<tr>
              <td height="22" class="normalfnt">Order No</td>
              <td class="normalfnt"><select name="cboOrderNo" id="cboOrderNo" style="width:130px" >
                <option value=""></option>
                <?php

					$sql = "SELECT DISTINCT trn_orderheader.intOrderNo 
					FROM trn_orderdetails 
					Inner Join trn_orderheader 
					ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear 
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo 
					AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo 
					left Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
					where trn_orderheader.intStatus = 1 
					/*AND mst_locations.intCompanyId =  '$company'*/ ";
					if($orderYear!='')
					$sql.=" AND trn_orderheader.intOrderYear 	=  '$orderYear'  ";
					
				//	if($orderNo!=''){
					if($x_graphicNo!='')
					$sql.=" AND trn_orderdetails.strGraphicNo =  '$x_graphicNo'  ";
					if($x_styleNo!='')
					$sql.=" AND trn_orderdetails.strStyleNo 		=  '$x_styleNo'  ";
					if($x_poNo!='')
					//$sql.=" AND trn_orderheader.strCustomerPoNo =  '$x_poNo' ";
					if($salesOrderId!=''){		
					//$sql .= " AND trn_orderdetails.strSalesOrderNo =  '$salesOrderId'";		
					}
				//	}
					$sql .= "ORDER BY trn_orderheader.intOrderYear DESC, trn_orderheader.intOrderNo DESC";
					//   echo $sql;
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intOrderNo']==$orderNo)
						echo "<option value=\"".$row['intOrderNo']."\" selected=\"selected\">".$row['intOrderNo']."</option>";	
						else
						echo "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";	
					}
				?>
              </select><?php //echo $sql; ?></td>
              <td class="normalfnt">Sales Order No</td>
              <td colspan="4" class="normalfnt"><select name="cboSalesOrderNo" id="cboSalesOrderNo" style="width:130px" >
                <option value=""></option>
                <?php
				///////////////////////////
				///////////////////////////////
			    	$sql = "SELECT
							trn_orderdetails.strGraphicNo,
							trn_orderdetails.intSampleNo,
							trn_orderdetails.intSampleYear,
							trn_orderdetails.strCombo,
							trn_orderdetails.strPrintName,
							trn_orderdetails.intRevisionNo,
							trn_orderdetails.intQty,
							trn_orderdetails.dblPrice,
							trn_orderdetails.dtPSD,
							trn_orderdetails.dtDeliveryDate 
						FROM trn_orderdetails 
						WHERE
							trn_orderdetails.intOrderNo 		=  '$orderNo' AND trn_orderdetails.intOrderNo > 0 AND
							trn_orderdetails.intOrderYear 		=  '$orderYear' AND
							trn_orderdetails.intSalesOrderId 	=  '$salesOrderId'
						";
				$result = $db->RunQuery($sql);
				$Mrow=mysqli_fetch_array($result);
				///////////////////////////////
				///////////////////////////
				 	$sql = "SELECT DISTINCT
					trn_orderdetails.strSalesOrderNo,intSalesOrderId, 
					mst_part.strName as part  
					FROM trn_orderdetails 
						Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
						Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId
						WHERE  trn_orderheader.intStatus = 1 /*
							AND mst_locations.intCompanyId =  '$company'*/ ";
							 
					if($orderYear!='')
					$sql.=" AND trn_orderheader.intOrderYear 	=  '$orderYear'  ";
					if($x_graphicNo!='')
					$sql.=" AND trn_orderdetails.strGraphicNo =  '$x_graphicNo'  ";
					if($x_styleNo!='')
					$sql.=" AND trn_orderdetails.strStyleNo 		=  '$x_styleNo'  ";
					if($x_poNo!='')
					//$sql.=" AND trn_orderheader.strCustomerPoNo =  '$x_poNo' ";
					if($orderNo!=''){		
					$sql .= " AND trn_orderheader.intOrderNo =  '$orderNo'";		
					}
					$sql .= "
							ORDER BY trn_orderdetails.strSalesOrderNo ASC";
							
					//echo $sql;	
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$no = $row['strSalesOrderNo']."/".$row['part'];
						$salesId = $row['intSalesOrderId'];
						if($salesId==$salesOrderId)
							echo "<option selected=\"selected\" value=\"$salesId\">$no</option>";
						else
							echo "<option value=\"$salesId\">$no</option>";
					}
				?>
              </select><?php //echo $sql; ?></td>
            </tr>            </table></td></tr>   
            
<tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <?php
		  	#########################
			## get header details ###
			#########################
			$sql = "SELECT DISTINCT
						trn_sampleinfomations_printsize.intWidth,
						trn_sampleinfomations_printsize.intHeight,
						trn_sampleinfomations.strStyleNo,
						mst_part.strName
					FROM
						trn_sampleinfomations_printsize
						Inner Join mst_part ON mst_part.intId = trn_sampleinfomations_printsize.intPart
						Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_printsize.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_printsize.intSampleYear
					WHERE
						trn_sampleinfomations_printsize.strPrintName  =  '$x_print' AND
						trn_sampleinfomations_printsize.intSampleNo   =  '$x_sampleNo' AND
						trn_sampleinfomations_printsize.intSampleYear =  '$x_sampleYear' AND
						trn_sampleinfomations_printsize.intRevisionNo =  '$x_revNo'
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					$styleNo 		= $row['strStyleNo'];
					$print_width 	= $row['intWidth'];
					$print_height 	= $row['intHeight'];
					$partName 		= $row['strName'];
				}
				
		  ?>
          <tr>
            <td height="155"  class="normalfnt"><div style="height:155px;vertical-align:middle"  class="tableBorder_allRound">
              <table class="rounded" bgcolor="" width="92%" border="0" align="center" cellpadding="0" cellspacing="1">
                <tr>
                  <td width="25%" bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
                  <td width="23%" bgcolor="#FFFFFF" class="normalfntMid">&nbsp;</td>
                  <td width="26%" bgcolor="#FFFFFF" class="normalfntMid">&nbsp;</td>
                  <td width="26%" bgcolor="#FFFFFF" class="normalfntMid">&nbsp;</td>
                  </tr>
                <tr>
                  <td bgcolor="#D1F9FC" class="txtNumber">Sample Year</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['intSampleYear'];?></td>
                  <td bgcolor="#D1F9FC" class="txtNumber">Sample No</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['intSampleNo'];?></td>
                  </tr>
                <tr>
                  <td bgcolor="#D1F9FC" class="txtNumber">Graphic No</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['strGraphicNo'];?></td>
                  <td bgcolor="#D1F9FC" class="txtNumber">Rev No</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['intRevisionNo'];?></td>
                  </tr>
                <tr>
                  <td bgcolor="#D1F9FC" class="txtNumber">Combo</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['strCombo'];?></td>
                  <td bgcolor="#D1F9FC" class="txtNumber">Print</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['strPrintName'];?></td>
                  </tr>
                <tr>
                  <td bgcolor="#D1F9FC" class="txtNumber">Order Qty</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<div id="divTotGarmQty"><?php  echo $Mrow['intQty'];?></div></td>
                  <td bgcolor="#D1F9FC" class="txtNumber">Price</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $row['dblPrice'];?></td>
                  </tr>
                <tr>
                  <td bgcolor="#D1F9FC" class="txtNumber">PSD</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['dtPSD'];?></td>
                  <td bgcolor="#D1F9FC" class="txtNumber">Deliver Date</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['dtDeliveryDate'];?></td>
                  </tr>
                </table>
            </div></td> 
            <td height="22" class="normalfnt">&nbsp;</td><td colspan="2" rowspan="2" class="normalfntMid"><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:264px;height:155px;overflow:hidden;margin-left:10px" >
              <?php
			if($salesOrderId!='')
			{
				echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"../../../../../documents/sampleinfo/samplePictures/".$Mrow['intSampleNo']."-".$Mrow['intSampleYear']."-".$Mrow['intRevisionNo']."-".substr($Mrow['strPrintName'],6,2).".png\" />";	
			}
			
			?>
            </div></td>
            </tr>
          
          </table></td>
      </tr>      <?php 
		//	$plan2Garment = (float)$Mrow['intQty']- $actual1Qty;
			
			$garmentQty25 = round(($Mrow['intQty']/100)*25);
			$garmentQty75 = $Mrow['intQty']-round(($Mrow['intQty']/100)*25);
			
			
			$sqlg="SELECT
					ware_bulkallocation_header.int25PlanGarmentQty,
					ware_bulkallocation_header.int75PlanGarmentQty
					FROM ware_bulkallocation_header
					WHERE
					ware_bulkallocation_header.intOrderNo =  '$orderNo' AND ware_bulkallocation_header.intOrderNo > 0 AND
					ware_bulkallocation_header.intOrderYear =  '$orderYear' AND
					ware_bulkallocation_header.intSalesOrderId =  '$salesOrderId'"; 
			$resultg = $db->RunQuery($sqlg);
			$rowg = mysqli_fetch_array($resultg);
			if($rowg['int25PlanGarmentQty']>0){
				$garmentQty25=$rowg['int25PlanGarmentQty'];
				$garmentQty75=$rowg['int75PlanGarmentQty'];
			}
			
			
			
			$savedFlag25=0;
			$savedFlagFirstOrSecondDay=0;
			$savedFlag75=0;
			
			 $sqlF="SELECT
					Sum(ware_bulkallocation_details.dbl25AllocatedQty) as savedFlag25,
					Sum(ware_bulkallocation_details.dbl75AllocatedQty) as savedFlag75,
					Sum(ware_bulkallocation_details.dblFirstDayActualConPC) as savedFlagFirstOrSecondDay 
					FROM
					ware_bulkallocation_details
					WHERE
					ware_bulkallocation_details.intOrderNo =  '$orderNo' AND ware_bulkallocation_details.intOrderNo > 0 AND
					ware_bulkallocation_details.intOrderYear =  '$orderYear' AND
					ware_bulkallocation_details.intSalesOrderId =  '$salesOrderId'
					GROUP BY
					ware_bulkallocation_details.intOrderNo,
					ware_bulkallocation_details.intOrderYear,
					ware_bulkallocation_details.intSalesOrderId,
					ware_bulkallocation_details.intLocationId"; 
					$resultF = $db->RunQuery($sqlF);
					$rowF = mysqli_fetch_array($resultF);
					
					if($rowF['savedFlag25']>0)
						$savedFlag25=1;
					if($rowF['savedFlag75']>0)
						$savedFlag75=1;
					if($rowF['savedFlagFirstOrSecondDay']>0)
						$savedFlagFirstOrSecondDay=1;
	  ?>
      <tr>
        <td><!--<div style="width:900px;height:300px;overflow:scroll" >-->
          <table width="100%" class="grid" id="tblMainGrid" >
            <tr class="">
              <td height="22" colspan="2" bgcolor="#F3E2BC" class="normalfntRight" >Garment Qty</td>
              <td colspan="5" align="center" bgcolor="#CAE4FB"  ><input name="txt1Plan" type="text" <?php if($saved25==1){ ?> disabled="disabled" <?php } ?> class="<?php echo "validate[max[".$Mrow['intQty']."],min[0],custom[integer]]";?>" id="txt1Plan" style="width:90px;text-align:center" value="<?Php echo $garmentQty25; ?>" /></td>
              <td colspan="3" align="center" bgcolor="#CFF9BD" >
                <input name="txt2Plan" type="text" disabled="disabled" class="<?php echo "validate[max[".$garmentQty75."],min[0],custom[integer]]";?>" id="txt2Plan" style="width:90px;text-align:center"  value="<?Php echo $garmentQty75; ?>" />
             </td>
              
              <?php
			  	########### create 3rd maxmimum qty #########################
				$thirdMaxQty =$Mrow['intQty']- ($actual2Qty+$actual1Qty);
			  ?>
              <td colspan="2" bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
              </tr>
            <tr class="">
              <td width="23%" height="22" bgcolor="#F3E2BC" class="normalfnt" >Item</td>
              <td width="5%" height="22" bgcolor="#F3E2BC" class="normalfnt" >UOM</td>
              <td width="4%" bgcolor="#CAE4FB" class="normalfnt" >Sample ConPc</td>
              <td width="4%" bgcolor="#CAE4FB" class="normalfnt" >25%  Plan Qty</td>
              <td width="8%" bgcolor="#CAE4FB" class="normalfnt" >25% Allocated Qty</td>
              <td width="5%" bgcolor="#CAE4FB" class="normalfnt" >1st Day Actual ConPc</td>
              <td width="5%" bgcolor="#CAE4FB" class="normalfnt" >2nd Day Actual ConPc</td>
              <td width="6%" bgcolor="#CFF9BD" class="normalfnt">75% Plan Qty</td>
              <td width="8%" bgcolor="#CFF9BD" class="normalfnt">75% Allocated Qty</td>
              <td width="6%" bgcolor="#CFF9BD" class="normalfnt">75% Actual Qty</td>
              <td width="6%" bgcolor="#FFFFFF" class="normalfnt">Total Plan Qty</td>
              <td width="7%" bgcolor="#FFFFFF" class="normalfnt">Total Allocated Qty</td>
              </tr>
              <?php
			  /*$sql = "select intItem,sum(qty) as qty,strName,unitName from (
				
					
						select intItem,round((dblColorWeight/sumWeight* dblWeight )/dblNoOfPcs,10) as qty,item as strName,strCode as unitName
						from 

 (SELECT

A.intItem,
mst_item.strName AS item,
mst_units.strCode,
A.dblWeight,
trn_sampleinfomations_details_technical.dblColorWeight,


(SELECT
Sum(trn_sample_color_recipes.dblWeight) AS sumWeight
FROM
trn_sample_color_recipes
WHERE
trn_sample_color_recipes.intSampleNo = A.intSampleNo AND
trn_sample_color_recipes.intSampleYear =  A.intSampleYear AND
trn_sample_color_recipes.intRevisionNo =   A.intRevisionNo AND
trn_sample_color_recipes.strCombo =   A.strCombo AND
trn_sample_color_recipes.strPrintName =   A.strPrintName AND
trn_sample_color_recipes.intTechniqueId =   A.intTechniqueId AND
trn_sample_color_recipes.intInkTypeId =   A.intInkTypeId
GROUP BY
trn_sample_color_recipes.intSampleNo,
trn_sample_color_recipes.intSampleYear,
trn_sample_color_recipes.intRevisionNo,
trn_sample_color_recipes.strCombo,
trn_sample_color_recipes.strPrintName,
trn_sample_color_recipes.intColorId,
trn_sample_color_recipes.intInkTypeId

) as sumWeight




FROM
trn_sample_color_recipes as A
Inner Join mst_item ON A.intItem = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
Inner Join mst_units ON mst_item.intUOM = mst_units.intId
Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = A.intSampleNo 
AND trn_sampleinfomations_details_technical.intSampleYear = A.intSampleYear 
AND trn_sampleinfomations_details_technical.intRevNo = A.intRevisionNo 
AND trn_sampleinfomations_details_technical.strPrintName = A.strPrintName 
AND trn_sampleinfomations_details_technical.strComboName = A.strCombo 
AND trn_sampleinfomations_details_technical.intColorId = A.intColorId 
AND trn_sampleinfomations_details_technical.intInkTypeId = A.intInkTypeId

						WHERE
							A.intSampleNo 	=  '".$Mrow['intSampleNo']."' AND
							A.intSampleYear 	=  '".$Mrow['intSampleYear']."' AND
							A.intRevisionNo 	=  '".$Mrow['intRevisionNo']."' AND
							A.strCombo 		=  '".$Mrow['strCombo']."' AND
							A.strPrintName 	=  '".$Mrow['strPrintName']."'
							
ORDER BY
A.intInkTypeId ASC
) as y
						
							
						UNION
						
						SELECT
							trn_sample_foil_consumption.intItem,
							Sum(trn_sample_foil_consumption.dblMeters) AS qty,
							mst_item.strName,
							'Meter' as unitName
						FROM
							trn_sample_foil_consumption
							Inner Join mst_item ON mst_item.intId = trn_sample_foil_consumption.intItem
						WHERE
							trn_sample_foil_consumption.intSampleNo 		=  '".$Mrow['intSampleNo']."' AND
							trn_sample_foil_consumption.intSampleYear 		=  '".$Mrow['intSampleYear']."' AND
							trn_sample_foil_consumption.intRevisionNo 		=  '".$Mrow['intRevisionNo']."' AND
							trn_sample_foil_consumption.strCombo 			=  '".$Mrow['strCombo']."' AND
							trn_sample_foil_consumption.strPrintName 		=  '".$Mrow['strPrintName']."'
						GROUP BY
							trn_sample_foil_consumption.intItem
						
						UNION
						
						SELECT
							trn_sample_spitem_consumption.intItem,
							Sum(trn_sample_spitem_consumption.dblQty),
							mst_item.strName,
							mst_units.strName AS unitName
						FROM
							trn_sample_spitem_consumption
							Inner Join mst_item ON mst_item.intId = trn_sample_spitem_consumption.intItem
							Inner Join mst_units ON mst_units.intId = mst_item.intUOM
						WHERE
							trn_sample_spitem_consumption.intSampleNo 		=  '".$Mrow['intSampleNo']."' AND
							trn_sample_spitem_consumption.intSampleYear 	=  '".$Mrow['intSampleYear']."' AND
							trn_sample_spitem_consumption.intRevisionNo 	=  '".$Mrow['intRevisionNo']."' AND
							trn_sample_spitem_consumption.strCombo 			=  '".$Mrow['strCombo']."' AND
							trn_sample_spitem_consumption.strPrintName 		=  '".$Mrow['strPrintName']."'
						GROUP BY
							trn_sample_spitem_consumption.intItem
							
						) MAIN 
						GROUP BY intItem


						";*/
						
						

	
$sql = "select mainId,mainCat,subId,subCat, intItem,itemName ,unitId,unitName
,intBomItem
,intStoresTrnsItem 
,sum(round(dblColorWeight/sumWeight*dblWeight /dblNoOfPcs,6))as consum
,sum(round(dblColorWeight/sumWeight*dblWeight * intQty /dblNoOfPcs,6)) as totalQty 
,dblLastPrice 
from

(


SELECT
trn_orderdetails.intOrderNo,mst_item.dblLastPrice,
trn_orderdetails.intOrderYear,trn_orderdetails.strSalesOrderNo,trn_orderdetails.intSalesOrderId,trn_orderdetails.intSampleNo,trn_orderdetails.intSampleYear,trn_orderdetails.strCombo,trn_orderdetails.strPrintName,trn_orderdetails.intRevisionNo,
trn_orderdetails.intPart,
trn_orderdetails.intQty,
trn_sample_color_recipes.intItem,
trn_sample_color_recipes.dblWeight,
trn_sampleinfomations_details_technical.intInkTypeId,
trn_sampleinfomations_details_technical.dblColorWeight,
trn_sample_color_recipes.intTechniqueId,
mst_item.strName itemName,
mst_item.intBomItem,
mst_item.intStoresTrnsItem,
mst_units.strName as unitName,
mst_units.intId as unitId,
mst_units.dblNoOfPcs ,
mst_maincategory.strName AS mainCat,mst_maincategory.intId AS mainId,
mst_subcategory.strName AS subCat,mst_subcategory.intId AS subId,



(SELECT
Sum(trn_sample_color_recipes.dblWeight) AS sumWeight
FROM trn_sample_color_recipes
WHERE
trn_sample_color_recipes.intSampleNo = trn_orderdetails.intSampleNo AND
trn_sample_color_recipes.intSampleYear = trn_orderdetails.intSampleYear AND
trn_sample_color_recipes.intRevisionNo =  trn_orderdetails.intRevisionNo AND
trn_sample_color_recipes.strCombo =  trn_orderdetails.strCombo AND
trn_sample_color_recipes.strPrintName =  trn_orderdetails.strPrintName AND
trn_sample_color_recipes.intTechniqueId =  trn_sample_color_recipes.intTechniqueId AND
trn_sample_color_recipes.intInkTypeId =  trn_sampleinfomations_details_technical.intInkTypeId )  as sumWeight 


FROM
trn_orderdetails
Inner Join trn_sample_color_recipes ON trn_sample_color_recipes.intSampleNo = trn_orderdetails.intSampleNo AND trn_sample_color_recipes.intSampleYear = trn_orderdetails.intSampleYear AND trn_sample_color_recipes.intRevisionNo = trn_orderdetails.intRevisionNo AND trn_sample_color_recipes.strCombo = trn_orderdetails.strCombo AND trn_sample_color_recipes.strPrintName = trn_orderdetails.strPrintName
Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = trn_sample_color_recipes.intSampleNo AND trn_sampleinfomations_details_technical.intSampleYear = trn_sample_color_recipes.intSampleYear AND trn_sampleinfomations_details_technical.intRevNo = trn_sample_color_recipes.intRevisionNo AND trn_sampleinfomations_details_technical.strComboName = trn_sample_color_recipes.strCombo AND trn_sampleinfomations_details_technical.strPrintName = trn_sample_color_recipes.strPrintName AND trn_sampleinfomations_details_technical.intColorId = trn_sample_color_recipes.intColorId AND trn_sampleinfomations_details_technical.intInkTypeId = trn_sample_color_recipes.intInkTypeId
Inner Join mst_item ON mst_item.intId = trn_sample_color_recipes.intItem
Inner Join mst_units ON mst_item.intUOM = mst_units.intId
Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
WHERE
trn_orderdetails.intOrderNo =  '$orderNo'  AND trn_orderdetails.intOrderNo > 0 AND
trn_orderdetails.intOrderYear =  '$orderYear' 
AND trn_orderdetails.intSalesOrderId =  '$salesOrderId'
) as t 

group by

 mainId,mainCat,subId,SubCat, intItem,itemName 
 

 
 ";//echo $sql;
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{    
					
					$maincatId=$row['mainId'];
					$bomItem=$row['intBomItem'];
					$storesTrnsItem=$row['intStoresTrnsItem'];
					$subCatId=$row['subId'];
					$mainCatName=$row['mainCat'];
					$subCatName=$row['subCat'];
					$itemId=$row['intItem'];
					$itemName=$row['itemName'];
					$uom=$row['unitName'];
					$uomId=$row['unitId'];
					$consum=$row['consum'];
					$totalQty=$row['totalQty'];
					$rate=$row['dblLastPrice'];
					$dblNoOfPcs=$row['dblNoOfPcs'];//
					
					
					 $sql2 = "SELECT
							ware_bulkallocation_details.dbl25AllocatedQty,
							ware_bulkallocation_details.dbl75AllocatedQty, 
							ware_bulkallocation_details.dblFirstDayActualConPC, 
							ware_bulkallocation_details.dblSecondDayActualConPC   
							FROM ware_bulkallocation_details
							WHERE
							ware_bulkallocation_details.intLocationId =  '$locationId' AND
							ware_bulkallocation_details.intOrderNo =  '$orderNo' AND ware_bulkallocation_details.intOrderNo > 0  AND
							ware_bulkallocation_details.intOrderYear =  '$orderYear' AND
							ware_bulkallocation_details.intSalesOrderId =  '$salesOrderId' AND
							ware_bulkallocation_details.intItemId =  '".$row['intItem']."'
							";
					$result2 = $db->RunQuery($sql2);
					$row2 = mysqli_fetch_array($result2);
					
					//$uom=$row['unitName'];
					$sampleConPC=$row['qty']; //for convert to KG.
					$planQty25=$consum*$garmentQty25;
					$allocatedQty25 = $row2['dbl25AllocatedQty'];
					$allocatedQty75 = $row2['dbl75AllocatedQty'];
					$firstDayActualConPC = $row2['dblFirstDayActualConPC'];
					$secondDayActualConPC = $row2['dblSecondDayActualConPC'];
					
					$sampleActualConPC=$firstDayActualConPC;
					if($secondDayActualConPC>0){
					$sampleActualConPC=$secondDayActualConPC;
					}
					//echo $sampleActualConPC."*".$garmentQty75;
					$planQty75=val($sampleActualConPC*$garmentQty75);
					
					
					$totalAllocated3 = $row2['dbl75AllocatedQty'];
					$dbl3ActualQty = $row2['dbl75ActualQty'];
				
					if(($storesTrnsItem==1) && ($bomItem==1)){
						$plusFlag=0;
						$bgcol='#FFFFC4';
					}
					else if($bomItem==1){
						$plusFlag=1;
						$bgcol='#FFFFFF';
					}
					else{
						$plusFlag=0;
						$bgcol='#CCCCCC';
					}
					
			  ?>
              
            <tr class="" bgcolor="<?php echo $bgcol; ?>">
              <td height="22" class="normalfnt" id="<?php echo $itemId ?>" ><?php echo $itemName; ?></td>
              <td height="22" class="normalfntMid" ><?php echo $uom; ?></td>
              <td class="normalfntRight sampConPC" ><?php echo number_format($consum,6); ?></td>
              <td class="normalfntRight" ><span id="<?php echo $consum; ?>" class="clsQty"><?php echo number_format($planQty25,6); ?></span></td>
              <td align="right" class="normalfntRight" bgcolor="#FFFFCE"><span class="cls1Allocated"><?php echo number_format($allocatedQty25,6); ?></span><?php if(($savedFlagFirstOrSecondDay==0) && ($plusFlag==1) ){ ?><img class="but1Allocated mouseover"  src="../../../../../images/add.png" width="16" height="16" /><?php }?></td>
              <td class="normalfntRight firstDayConPC" ><?php echo number_format( $firstDayActualConPC,6); ?></td>
              <td class="normalfntRight secondDayConPC"><?php echo number_format($secondDayActualConPC,6); ?></td>
              <td class="normalfntRight"><span class="clsQty2"><?php echo number_format($planQty75,6); ?></span></td>
              <td align="right" class="normalfntRight"  bgcolor="#FFFFCE"><span class="cls2Allocated"><?php  echo number_format($allocatedQty75-($allocatedQty75==0?0:$allocatedQty25),6); 
			  
			  ?></span><?php if(($savedFlagFirstOrSecondDay>0)  && ($plusFlag==1)){ ?><img class="but2Allocated mouseover"  src="../../../../../images/add.png" width="16" height="16" /><?php } ?></td>
              <td class="normalfntRight"><?php echo number_format( $dbl2ActualQty,6); ?></td>
              <td class="normalfntRight" ><span id="<?php echo $sampleConPC; ?>" class="clsQty3"><?php 
			  echo number_format(($sampleConPC*$plan1Qty)+(($dbl1ActualQty/$actual1Qty)*$plan2Qty)+($dbl2ActualQty/$actual2Qty)*$plan3Qty,6); ?></span></td>
              <td class="normalfntRight"><?php echo number_format($allocatedQty25+$allocatedQty75-($allocatedQty75==0?0:$allocatedQty25)+$totalAllocated3-($totalAllocated3==0?0:$allocatedQty75),6); ?></td>
              </tr>
            <?php
				}
				
				////////////////////////// END OF INK COMSUMPTION //////////////////
				
		
            
             $sql ="SELECT
	mst_maincategory.intId AS mainId,
	mst_maincategory.strName AS mainCat,
	mst_subcategory.intId AS subId,
	mst_subcategory.strName AS subCat,
	mst_item.intId AS itemId,
	mst_item.strName AS itemName,
	mst_units.intId AS unitId,
	mst_units.strName AS unitName,
	sum(trn_sample_spitem_consumption.dblQty/dblNoOfPcs) as consum,
	mst_units.dblNoOfPcs,
	mst_item.dblLastPrice,
	mst_item.intBomItem, 
	mst_item.intStoresTrnsItem,
	mst_financecurrency.strCode AS currencyName,
	sum(trn_orderdetails.intQty * trn_sample_spitem_consumption.dblQty /dblNoOfPcs) as totalQty
FROM
	trn_orderdetails
	Inner Join trn_sample_spitem_consumption ON trn_sample_spitem_consumption.intSampleNo = trn_orderdetails.intSampleNo AND trn_sample_spitem_consumption.intSampleYear = trn_orderdetails.intSampleYear AND trn_sample_spitem_consumption.intRevisionNo = trn_orderdetails.intRevisionNo AND trn_sample_spitem_consumption.strCombo = trn_orderdetails.strCombo AND trn_sample_spitem_consumption.strPrintName = trn_orderdetails.strPrintName
	Inner Join mst_item ON mst_item.intId = trn_sample_spitem_consumption.intItem
	Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
	Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
	Inner Join mst_units ON mst_units.intId = mst_item.intUOM
	Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_item.intCurrency
WHERE
	trn_orderdetails.intOrderNo =  '$orderNo' AND trn_orderdetails.intOrderNo > 0 AND
	trn_orderdetails.intOrderYear =  '$orderYear' AND 
 	trn_orderdetails.intSalesOrderId =  '$salesOrderId' 
$para
GROUP BY
	mst_maincategory.intId,
	mst_subcategory.intId,
	mst_item.intId";
	
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{    
					
					$maincatId=$row['mainId'];
					$subCatId=$row['subId'];
					$mainCatName=$row['mainCat'];
					$subCatName=$row['subCat'];
					$itemId=$row['intItem'];
					$itemName=$row['itemName'];
					$uom=$row['unitName'];
					$uomId=$row['unitId'];
					$consum=$row['consum'];
					$totalQty=$row['totalQty'];
					$rate=$row['dblLastPrice'];
					$dblNoOfPcs=$row['dblNoOfPcs'];//
					$bomItem=$row['intBomItem'];
					$storesTrnsItem=$row['intStoresTrnsItem'];
					
					 $sql2 = "SELECT
							ware_bulkallocation_details.dbl25AllocatedQty,
							ware_bulkallocation_details.dbl75AllocatedQty, 
							ware_bulkallocation_details.dblFirstDayActualConPC, 
							ware_bulkallocation_details.dblSecondDayActualConPC   
							FROM ware_bulkallocation_details
							WHERE
							ware_bulkallocation_details.intLocationId =  '$locationId' AND
							ware_bulkallocation_details.intOrderNo =  '$orderNo' AND ware_bulkallocation_details.intOrderNo > 0 AND
							ware_bulkallocation_details.intOrderYear =  '$orderYear' AND
							ware_bulkallocation_details.intSalesOrderId =  '$salesNo' AND
							ware_bulkallocation_details.intItemId =  '".$row['intItem']."'
							";
					$result2 = $db->RunQuery($sql2);
					$row2 = mysqli_fetch_array($result2);
					
					//$uom=$row['unitName'];
					$sampleConPC=$row['qty']; //for convert to KG.
					$planQty25=$consum*$garmentQty25;
					$allocatedQty25 = $row2['dbl25AllocatedQty'];
					$allocatedQty75 = $row2['dbl75AllocatedQty'];
					$firstDayActualConPC = $row2['dblFirstDayActualConPC'];
					$secondDayActualConPC = $row2['dblSecondDayActualConPC'];
					
					$sampleActualConPC=$firstDayActualConPC;
					if($secondDayActualConPC>0){
					$sampleActualConPC=$secondDayActualConPC;
					}
					
					$planQty75=val($sampleActualConPC*$garmentQty75);
					
					
					$totalAllocated3 = $row2['dbl75AllocatedQty'];
					$dbl3ActualQty = $row2['dbl75ActualQty'];
					
					if(($storesTrnsItem==1) && ($bomItem==1)){
						$plusFlag=0;
						$bgcol='#FFFFC4';
					}
					else if($bomItem==1){
						$plusFlag=1;
						$bgcol='#FFFFFF';
					}
					else{
						$plusFlag=0;
						$bgcol='#CCCCCC';
					}
			  ?>
              
            <tr class="" bgcolor="<?php echo $bgcol; ?>">
              <td height="22" class="normalfnt" id="<?php echo $itemId ?>" ><?php echo $itemName; ?></td>
              <td height="22" class="normalfntMid" ><?php echo $uom; ?></td>
              <td class="normalfntRight sampConPC" ><?php echo number_format($consum,6); ?></td>
              <td class="normalfntRight" ><span id="<?php echo $consum; ?>" class="clsQty"><?php echo number_format($planQty25,6); ?></span></td>
              <td align="right" class="normalfntRight" ><span class="cls1Allocated"><?php echo number_format($allocatedQty25,6); ?></span><?php if(($savedFlagFirstOrSecondDay==0) && ($bomItem==1)){ ?><img class="but1Allocated mouseover"  src="../../../../../images/add.png" width="16" height="16" /><?php }?></td>
              <td class="normalfntRight firstDayConPC" ><?php echo number_format( $firstDayActualConPC,6); ?></td>
              <td class="normalfntRight secondDayConPC"><?php echo number_format($secondDayActualConPC,6); ?></td>
              <td class="normalfntRight"><span class="clsQty2"><?php echo number_format($planQty75,6); ?></span></td>
              <td align="right" class="normalfntRight" ><span class="cls2Allocated"><?php  echo number_format($allocatedQty75-($allocatedQty75==0?0:$allocatedQty25),6); 
			  
			  ?></span><?php if(($savedFlagFirstOrSecondDay>0) && ($bomItem==1)){ ?><img class="but2Allocated mouseover"  src="../../../../../images/add.png" width="16" height="16" /><?php } ?></td>
              <td class="normalfntRight"><?php echo number_format( $dbl2ActualQty,6); ?></td>
              <td class="normalfntRight" ><span id="<?php echo $sampleConPC; ?>" class="clsQty3"><?php 
			  echo number_format(($sampleConPC*$plan1Qty)+(($dbl1ActualQty/$actual1Qty)*$plan2Qty)+($dbl2ActualQty/$actual2Qty)*$plan3Qty,6); ?></span></td>
              <td class="normalfntRight"><?php echo number_format($allocatedQty25+$allocatedQty75-($allocatedQty75==0?0:$allocatedQty25)+$totalAllocated3-($totalAllocated3==0?0:$allocatedQty75),6); ?></td>
              </tr>
            <?php
				}
			$sql ="SELECT
		mst_item.intId AS itemId,
		mst_maincategory.strName AS mainCat,
		mst_subcategory.strName AS subCat,
		mst_maincategory.intId AS mainId,
		mst_subcategory.intId AS subId,
		mst_item.strName AS itemName,
		mst_units.intId,
		mst_item.intBomItem, 
		mst_item.intStoresTrnsItem,
		mst_units.strName AS unitName,
		mst_financecurrency.strCode AS currencyName,
		sum(trn_orderdetails.intQty *trn_sample_foil_consumption.dblMeters ) as totalQty,
		sum(trn_sample_foil_consumption.dblMeters) as consum,
		mst_units.dblNoOfPcs,mst_item.dblLastPrice
	FROM
		trn_orderdetails
		Inner Join trn_sample_foil_consumption ON trn_sample_foil_consumption.intSampleNo = trn_orderdetails.intSampleNo AND trn_sample_foil_consumption.intSampleYear = trn_orderdetails.intSampleYear AND trn_sample_foil_consumption.intRevisionNo = trn_orderdetails.intRevisionNo AND trn_sample_foil_consumption.strCombo = trn_orderdetails.strCombo AND trn_sample_foil_consumption.strPrintName = trn_orderdetails.strPrintName
		Inner Join mst_item ON mst_item.intId = trn_sample_foil_consumption.intItem
		Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
		Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
		Inner Join mst_units ON mst_units.intId = mst_item.intUOM
		Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_item.intCurrency
	WHERE
		trn_orderdetails.intOrderNo 	=  '$orderNo' AND
		trn_orderdetails.intOrderYear 	=  '$orderYear' AND trn_orderdetails.intOrderNo > 0  AND 
 		trn_orderdetails.intSalesOrderId =  '$salesOrderId' 
		$para
	GROUP BY
		mst_maincategory.intId,
		mst_subcategory.intId,
		mst_item.intId";
		
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{    
					
					$maincatId=$row['mainId'];
					$subCatId=$row['subId'];
					$mainCatName=$row['mainCat'];
					$subCatName=$row['subCat'];
					$itemId=$row['intItem'];
					$itemName=$row['itemName'];
					$bomItem=$row['intBomItem'];
					$storesTrnsItem=$row['intStoresTrnsItem'];
					$uom=$row['unitName'];
					$uomId=$row['unitId'];
					$consum=$row['consum'];
					$totalQty=$row['totalQty'];
					$rate=$row['dblLastPrice'];
					$dblNoOfPcs=$row['dblNoOfPcs'];//
					
					
					 $sql2 = "SELECT
							ware_bulkallocation_details.dbl25AllocatedQty,
							ware_bulkallocation_details.dbl75AllocatedQty, 
							ware_bulkallocation_details.dblFirstDayActualConPC, 
							ware_bulkallocation_details.dblSecondDayActualConPC   
							FROM ware_bulkallocation_details
							WHERE
							ware_bulkallocation_details.intLocationId =  '$locationId' AND
							ware_bulkallocation_details.intOrderNo =  '$orderNo'  AND ware_bulkallocation_details.intOrderNo > 0 AND
							ware_bulkallocation_details.intOrderYear =  '$orderYear' AND
							ware_bulkallocation_details.intSalesOrderId =  '$salesNo' AND
							ware_bulkallocation_details.intItemId =  '".$row['intItem']."'
							";
					$result2 = $db->RunQuery($sql2);
					$row2 = mysqli_fetch_array($result2);
					
					//$uom=$row['unitName'];
					$sampleConPC=$row['qty']; //for convert to KG.
					$planQty25=$consum*$garmentQty25;
					$allocatedQty25 = $row2['dbl25AllocatedQty'];
					$allocatedQty75 = $row2['dbl75AllocatedQty'];
					$firstDayActualConPC = $row2['dblFirstDayActualConPC'];
					$secondDayActualConPC = $row2['dblSecondDayActualConPC'];
					
					$sampleActualConPC=$firstDayActualConPC;
					if($secondDayActualConPC>0){
					$sampleActualConPC=$secondDayActualConPC;
					}
					
					$planQty75=val($sampleActualConPC*$garmentQty75);
					
					
					$totalAllocated3 = $row2['dbl75AllocatedQty'];
					$dbl3ActualQty = $row2['dbl75ActualQty'];
					
					if(($storesTrnsItem==1) && ($bomItem==1)){
						$plusFlag=0;
						$bgcol='#FFFFC4';
					}
					else if($bomItem==1){
						$plusFlag=1;
						$bgcol='#FFFFFF';
					}
					else{
						$plusFlag=0;
						$bgcol='#CCCCCC';
					}
			  ?>
              
            <tr class="" bgcolor="<?php echo $bgcol;?>">
              <td height="22" class="normalfnt" id="<?php echo $itemId ?>" ><?php echo $itemName; ?></td>
              <td height="22" class="normalfntMid" ><?php echo $uom; ?></td>
              <td class="normalfntRight sampConPC" ><?php echo number_format($consum,6); ?></td>
              <td class="normalfntRight" ><span id="<?php echo $consum; ?>" class="clsQty"><?php echo number_format($planQty25,6); ?></span></td>
              <td align="right" bgcolor="#FFFFCE" class="normalfntRight" ><span class="cls1Allocated"><?php echo number_format($allocatedQty25,6); ?></span><?php if(($savedFlagFirstOrSecondDay==0) && ($bomItem==1) ){ ?><img class="but1Allocated mouseover"  src="../../../../../images/add.png" width="16" height="16" /><?php }?></td>
              <td class="normalfntRight firstDayConPC" ><?php echo number_format( $firstDayActualConPC,6); ?></td>
              <td class="normalfntRight secondDayConPC"><?php echo number_format($secondDayActualConPC,6); ?></td>
              <td class="normalfntRight"><span class="clsQty2"><?php echo number_format($planQty75,6); ?></span></td>
              <td align="right" bgcolor="#FFFFCE" class="normalfntRight" ><span class="cls2Allocated"><?php  echo number_format($allocatedQty75-($allocatedQty75==0?0:$allocatedQty25),6); 
			  
			  ?></span><?php if(($savedFlagFirstOrSecondDay>0) && ($bomItem==1)){ ?><img class="but2Allocated mouseover"  src="../../../../../images/add.png" width="16" height="16" /><?php } ?></td>
              <td class="normalfntRight"><?php echo number_format( $dbl2ActualQty,6); ?></td>
              <td class="normalfntRight" ><span id="<?php echo $sampleConPC; ?>" class="clsQty3"><?php 
			  echo number_format(($sampleConPC*$plan1Qty)+(($dbl1ActualQty/$actual1Qty)*$plan2Qty)+($dbl2ActualQty/$actual2Qty)*$plan3Qty,6); ?></span></td>
              <td class="normalfntRight"><?php echo number_format($allocatedQty25+$allocatedQty75-($allocatedQty75==0?0:$allocatedQty25)+$totalAllocated3-($totalAllocated3==0?0:$allocatedQty75),6); ?></td>
              </tr>
            <?php
				}
			?>
          </table>
          <!--</div>--></td>
      </tr>
      <tr>
        <td align="center" class=""><table width="100%" border="0">
          <tr>
            <td width="28%" class="normalfnt">&nbsp;</td>
            <td width="72%" valign="top" class="normalfnt">&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><img id="butNew" class="mouseover" src="../../../../../images/Tnew.jpg" width="92" height="24" /><img id="butSave" src="../../../../../images/Tsave.jpg" width="92" height="24" class="mouseover" /><img src="../../../../../planning/img/Tclose.jpg" width="92" height="24" /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<div    style="width:500px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</body>
</html>

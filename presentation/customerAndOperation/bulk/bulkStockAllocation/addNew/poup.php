<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$locationId 	= $_SESSION['CompanyID'];
$companyId 		= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];

include $backwardseperator."dataAccess/Connector.php";

$itemQty 		= $_REQUEST['itemQty'];
$itemQty=str_replace(",", "", $itemQty) ;
$itemName 		= $_REQUEST['itemName'];
$itemId 		= $_REQUEST['itemId'];

//$stage 		= $_REQUEST['stage'];

$orderNo 		= $_REQUEST['orderNo'];
$orderYear 		= $_REQUEST['orderYear'];
$salesId 		= $_REQUEST['salesId'];
$rowIndex		= $_REQUEST['rowIndex'];
//$poNo=$poNArray[0];
//$poYear=$poNArray[1];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Items</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="allocation-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>

<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" href="../../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../../libraries/validate/template.css" type="text/css">

<script src="../../../../../libraries/validate/jquery-1.js" type="application/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="application/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="application/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>
</head>

<body>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>

    
<form id="frmAllocationPopUp" name="frmAllocationPopUp" method="post" action="">
<table width="450" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
<div class="trans_layoutS">
		  <div class="trans_text"> Stock Allocation</div>
	  <table width="498" border="0" align="center" bgcolor="#FFFFFF">
    <td width="492"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableBorder_allRound">
      <tr class="normalfnt">
        <td width="23%" bgcolor="#CFE3FC" class="normalfntMid" style="border-radius: 6px 6px 6px 6px;" >Total Qty</td>
        <td width="15%" class="normalfntsm" id="spanQty"><?php echo $itemQty; ?></td>
        <td width="12%" bgcolor="#CFE3FC" class="normalfntMid" style="border-radius: 6px 6px 6px 6px;">Item</td>
        <td width="50%" class="normalfntsm" id="spanItemName"><?php echo $itemName; ?></td>
        </tr>
    </table></td>
    </tr><tr>
      <td><table  width="100%" border="0" cellpadding="0" cellspacing="0" class="tableBorder_allRound">
        <tr class="normalfntMid" >
          <td width="11%" height="17" bgcolor="#C0DDFA" style="border-radius: 6px 0px 0px 0px;">GRN No</td>
          <td width="12%" bgcolor="#C0DDFA">GRN Year</td>
          <td width="16%" bgcolor="#C0DDFA">Allocated Qty</td>
          <td width="15%" bgcolor="#C0DDFA">Issue Qty</td>
          <td width="18%" bgcolor="#C0DDFA">Stock Balance</td>
          <td width="13%" bgcolor="#C0DDFA">Allocate</td>
          <td width="15%" bgcolor="#C0DDFA" style="border-radius: 0px 6px 0px 0px;">Un-Allocate</td>
        </tr>
        <?php
			$sql = "SELECT
						A.intLocationId,
						A.intItemId,
						A.intGRNNo,
						A.intGRNYear,
						
						
						( SELECT
						Sum(ware_stocktransactions_bulk.dblQty * -1) AS allocateQty
						FROM ware_stocktransactions_bulk
						WHERE
						ware_stocktransactions_bulk.intLocationId =  A.intLocationId AND
						ware_stocktransactions_bulk.intGRNNo =  A.intGRNNo  AND
						ware_stocktransactions_bulk.intGRNYear =  A.intGRNYear AND
						ware_stocktransactions_bulk.intItemId =  A.intItemId AND
						(ware_stocktransactions_bulk.strType =  'ALLOCATE' or ware_stocktransactions_bulk.strType =  'UNALLOCATE')AND
						ware_stocktransactions_bulk.intOrderNo = '$orderNo' and
						ware_stocktransactions_bulk.intOrderYear = '$orderYear' and
						ware_stocktransactions_bulk.intSalesOrderId = '$salesId'
						 ) as allocateQty,
						(SELECT
						Sum(ware_stocktransactions.dblQty) AS styleBalQty
						FROM ware_stocktransactions
						WHERE
						ware_stocktransactions.intLocationId =  A.intLocationId AND
						ware_stocktransactions.intGRNNo =  A.intGRNNo  AND
						ware_stocktransactions.intGRNYear =  A.intGRNYear AND
						ware_stocktransactions.intItemId =  A.intItemId AND
						ware_stocktransactions.intOrderNo =  '$orderNo' AND
						ware_stocktransactions.intOrderYear =  '$orderYear' AND
						ware_stocktransactions.intSalesOrderId =  '$salesId'
						GROUP BY
						ware_stocktransactions.intItemId
						 ) as styleBalQty,
						
						Sum( A.dblQty) AS stockBal
						FROM ware_stocktransactions_bulk as A
						WHERE
							A.intLocationId =  '$locationId' AND
							A.intItemId 	=  '$itemId'
						GROUP BY
							A.intItemId,
							A.intGRNYear,
							A.dtGRNDate
						HAVING
							stockBal>0
						";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					 $stockBalQty = $row['stockBal'];
					if($stockBalQty>($itemQty-$row['allocateQty']))
					{
						$stockBalQty=val($itemQty)-val($row['allocateQty']);	
					}
					$styleBalQty	= $row['styleBalQty'];
				//	echo $stockBalQty;
		?>
        <tr class="normalfntsm">
          <td height="24" align="center"><?php echo $row['intGRNNo'] ?></td>
          <td align="center"><?php echo $row['intGRNYear'] ?></td>
          <td align="right"><?php echo round($row['allocateQty'],4) ?></td>
          <td align="right"><?php echo round(($row['allocateQty']-$row['styleBalQty']),4); ?></td>
          <td align="right"><?php echo round($row['stockBal'],4) ?></td>
          <td align="center"><input class="<?php echo  "validate[custom[number],max[$stockBalQty],min[0]]";?> clstxtAllocate"  style="width:60px;text-align:right" type="text" name="txtAllocate" id="txtAllocate" autocomplete="off" /></td>
          <td align="center"><input class="<?php echo  "validate[custom[number],max[$styleBalQty],min[0]]";?> clstxtUnAllocate" autocomplete="off" style="width:60px;text-align:right" type="text" name="txtUnAllocate" id="txtUnAllocate" /></td>
        </tr>
        <?Php
				}
		?>
      </table></td>
    </tr>
    <tr>
      <td><span style="visibility:hidden" id="spanItemId"><?php echo $itemId; ?></span>&nbsp;</td>
    </tr>
    <tr>
      <td align="center"><img class="mouseover" id="butSave" src="../../../../../images/Tsave.jpg" width="92" height="24" /><img class="mouseover" id="butClose2" src="../../../../../images/Tclose.jpg" width="92" height="24" /></td>
    </tr>
      </table>

  </div>
  </div>
</form>

</body>
</html>

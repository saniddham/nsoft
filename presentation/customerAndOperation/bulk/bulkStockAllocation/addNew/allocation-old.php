<?php

session_start();
$backwardseperator 		= "../../../../../";
$mainPath 				= $_SESSION['mainPath'];
$sessionUserId 			= $_SESSION['userId'];
$thisFilePath 			=  $_SERVER['PHP_SELF'];
$locationId				= $_SESSION["CompanyID"];

include  	"{$backwardseperator}dataAccess/permisionCheck.inc";

###########################
### get loading values ####
###########################
$orderYear 	= $_REQUEST['cboOrderYear'];
$orderNo 	= $_REQUEST['cboOrderNo'];
$salesNo 	= $_REQUEST['cboSalesOrderNo'];

################ 1 ACTUAL CALCULATION ####################################
	  	$sql = "SELECT
					ware_bulkallocation_header.intFirstDayPlanGarment,
					ware_bulkallocation_header.intFirstDayActualGarment,
					ware_bulkallocation_header.int25PlanGarment,
					ware_bulkallocation_header.int25ActualGarment,
					ware_bulkallocation_header.int75PlanGarment,
					ware_bulkallocation_header.int75ActualGarment,
					ware_bulkallocation_header.intStage
				FROM `ware_bulkallocation_header`
				WHERE
					ware_bulkallocation_header.intOrderNo =  '$orderNo' AND
					ware_bulkallocation_header.intOrderYear =  '$orderYear' AND
					ware_bulkallocation_header.intSalesOrderId =  '$salesNo'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$plan1Qty = $row['intFirstDayPlanGarment'];
			$plan2Qty = $row['int25PlanGarment'];
			$plan3Qty = $row['int75PlanGarment'];
			
			$actual1Qty = $row['intFirstDayActualGarment'];
			$actual2Qty = $row['int25ActualGarment'];
			$actual3Qty = $row['int75ActualGarment'];
			
			$intStage = $row['intStage'];
		}
if($intStage==1)
{
	$sql = "UPDATE `ware_bulkallocation_details` AS A 
			SET `dbl1ActualQty`=(
			
			SELECT
			Sum(ware_stocktransactions.dblQty) AS issueQty
			FROM ware_stocktransactions 
			WHERE
			ware_stocktransactions.intLocationId 	=  A.intLocationId AND
			ware_stocktransactions.intOrderNo 		=  A.intOrderNo AND
			ware_stocktransactions.intOrderYear 	=  A.intOrderYear AND
			ware_stocktransactions.intSalesOrderId 	=  A.intSalesOrderId AND
			(ware_stocktransactions.intItemId=A.intItemId)  AND
			(ware_stocktransactions.strType =  'ISSUE' OR ware_stocktransactions.strType =  'RETSTORES') 
			
			) * -1
			WHERE (A.intLocationId='$locationId') AND (A.intOrderNo='$orderNo') 
			AND (A.intOrderYear='$orderYear') AND (A.intSalesOrderId='$salesNo') 
			";
	$db->RunQuery($sql);
	
}
if($intStage==2)
{
	$sql = "UPDATE `ware_bulkallocation_details` AS A 
			SET `dbl25ActualQty`=((
			
			SELECT
			Sum(ware_stocktransactions.dblQty) AS issueQty
			FROM ware_stocktransactions 
			WHERE
			ware_stocktransactions.intLocationId 	=  A.intLocationId AND
			ware_stocktransactions.intOrderNo 		=  A.intOrderNo AND
			ware_stocktransactions.intOrderYear 	=  A.intOrderYear AND
			ware_stocktransactions.intSalesOrderId 	=  A.intSalesOrderId AND
			(ware_stocktransactions.intItemId=A.intItemId)  AND
			(ware_stocktransactions.strType =  'ISSUE' OR ware_stocktransactions.strType =  'RETSTORES') 
			
			) * -1)-dbl1ActualQty
			WHERE (A.intLocationId='$locationId') AND (A.intOrderNo='$orderNo') 
			AND (A.intOrderYear='$orderYear') AND (A.intSalesOrderId='$salesNo') 
			";
	$db->RunQuery($sql);
	
}
if($intStage==3)
{
	$sql = "UPDATE `ware_bulkallocation_details` AS A 
			SET `dbl75ActualQty`=((
			
			SELECT
			Sum(ware_stocktransactions.dblQty) AS issueQty
			FROM ware_stocktransactions 
			WHERE
			ware_stocktransactions.intLocationId 	=  A.intLocationId AND
			ware_stocktransactions.intOrderNo 		=  A.intOrderNo AND
			ware_stocktransactions.intOrderYear 	=  A.intOrderYear AND
			ware_stocktransactions.intSalesOrderId 	=  A.intSalesOrderId AND
			(ware_stocktransactions.intItemId=A.intItemId)  AND
			(ware_stocktransactions.strType =  'ISSUE' OR ware_stocktransactions.strType =  'RETSTORES') 
			
			) * -1)-dbl1ActualQty-dbl25ActualQty
			WHERE (A.intLocationId='$locationId') AND (A.intOrderNo='$orderNo') 
			AND (A.intOrderYear='$orderYear') AND (A.intSalesOrderId='$salesNo') 
			";
	$db->RunQuery($sql);
	
}
##########################################################################
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bulk Stock Allocation</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="allocation-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>
</head>

<body>
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php echo $_SESSION['header']; /*include  $backwardseperator.'Header.php';*/ ?></td>
	</tr> 
<form id="frmAllocation" name="frmAllocation" method="get" action="allocation.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../../libraries/calendar/theme.css" />
<script src="../../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Bulk Stock Allocation</div>
		  <table width="103%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="22" colspan="2" class="normalfnt"><div  class="tableBorder_allRound"><table class="rounded" bgcolor="#85B5EF" width="47%" border="0" align="center" cellpadding="0" cellspacing="1">
              <tr>
                <td width="10%" align="center" bgcolor="#F3E2BC" class="normalfntMid">Order Year</td>
                <td width="14%" bgcolor="#F3E2BC" class="normalfntMid">Order No</td>
                <td width="16%" bgcolor="#F3E2BC" class="normalfntMid">Sales Order No</td>
                </tr>
              <tr>
                <td bgcolor="#FFFFFF" class="normalfntMid"><select name="cboOrderYear" id="cboOrderYear" style="width:70px">
                  <?php
				    $d = date('Y');
				  	for($d;$d>=2012;$d--)
					{
						if($d==$orderYear)
							echo "<option selected=\"selected\" id=\"$d\" >$d</option>";	
						else
							echo "<option id=\"$d\" >$d</option>";	
					}
				  ?>
                  </select></td>
                <td bgcolor="#FFFFFF" class="normalfntMid"><select name="cboOrderNo" id="cboOrderNo" style="width:120px">
                  <option value=""></option>
                  <?php
					$d = date('Y');
				   	$sql = "SELECT DISTINCT
								trn_orderheader.intOrderNo
							FROM trn_orderheader
							WHERE
								trn_orderheader.intOrderYear =  '$d' and intStatus=1
							ORDER BY
								trn_orderheader.intOrderNo ASC
					";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$no = $row['intOrderNo'];
						if($no==$orderNo)
							echo "<option selected=\"selected\" value=\"$no\">$no</option>";
						else
							echo "<option value=\"$no\">$no</option>";
					}
				?>
                  </select></td>
                <td bgcolor="#FFFFFF" class="normalfntMid"><select name="cboSalesOrderNo" id="cboSalesOrderNo" style="width:120px">
                  <option value=""></option>
                              <?php
				$sql = "SELECT
							trn_orderdetails.strGraphicNo,
							trn_orderdetails.intSampleNo,
							trn_orderdetails.intSampleYear,
							trn_orderdetails.strCombo,
							trn_orderdetails.strPrintName,
							trn_orderdetails.intRevisionNo,
							trn_orderdetails.intQty,
							trn_orderdetails.dblPrice,
							trn_orderdetails.dtPSD,
							trn_orderdetails.dtDeliveryDate
						FROM trn_orderdetails
						WHERE
							trn_orderdetails.intOrderNo 		=  '$orderNo' AND
							trn_orderdetails.intOrderYear 		=  '$orderYear' AND
							trn_orderdetails.intSalesOrderId 	=  '$salesNo'
						";
				$result = $db->RunQuery($sql);
				$Mrow=mysqli_fetch_array($result);
				
			?>
                  <?php
				   	$sql = "SELECT DISTINCT
					trn_orderdetails.strSalesOrderNo,intSalesOrderId
						FROM trn_orderdetails
						WHERE
							trn_orderdetails.intOrderNo 	=  '$orderNo' AND
							trn_orderdetails.intOrderYear 	=  '$orderYear'
						order by strSalesOrderNo
						";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$no = $row['strSalesOrderNo'];
						$salesId = $row['intSalesOrderId'];
						if($salesId==$salesNo)
							echo "<option selected=\"selected\" value=\"$salesId\">$no</option>";
						else
							echo "<option value=\"$salesId\">$no</option>";
					}
				?>
                  </select></td>
                </tr>
              </table></div></td>
            <td colspan="2" rowspan="6" class="normalfntMid"><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:264px;height:148px;overflow:hidden;margin-left:10px" >
              <?php
			if($salesNo!='')
			{
				echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"../../../../../documents/sampleinfo/samplePictures/".$Mrow['intSampleNo']."-".$Mrow['intSampleYear']."-".$Mrow['intRevisionNo']."-".substr($Mrow['strPrintName'],6,2).".png\" />";	
			}
			 ?>
              </div></td>
            <td class="normalfnt">&nbsp;</td>
            <td align="left">&nbsp;</td>
          </tr>
          <?php
		  	#########################
			## get header details ###
			#########################
			$sql = "SELECT DISTINCT
						trn_sampleinfomations_printsize.intWidth,
						trn_sampleinfomations_printsize.intHeight,
						trn_sampleinfomations.strStyleNo,
						mst_part.strName
					FROM
						trn_sampleinfomations_printsize
						Inner Join mst_part ON mst_part.intId = trn_sampleinfomations_printsize.intPart
						Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_printsize.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_printsize.intSampleYear
					WHERE
						trn_sampleinfomations_printsize.strPrintName  =  '$x_print' AND
						trn_sampleinfomations_printsize.intSampleNo   =  '$x_sampleNo' AND
						trn_sampleinfomations_printsize.intSampleYear =  '$x_sampleYear' AND
						trn_sampleinfomations_printsize.intRevisionNo =  '$x_revNo'
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					$styleNo 		= $row['strStyleNo'];
					$print_width 	= $row['intWidth'];
					$print_height 	= $row['intHeight'];
					$partName 		= $row['strName'];
				}
		  ?>
          <tr>
            <td colspan="2" rowspan="5" class="normalfnt"><div style="height:110px;vertical-align:middle"  class="tableBorder_allRound">
              <table class="rounded" bgcolor="" width="92%" border="0" align="center" cellpadding="0" cellspacing="1">
                <tr>
                  <td width="25%" bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
                  <td width="23%" bgcolor="#FFFFFF" class="normalfntMid">&nbsp;</td>
                  <td width="26%" bgcolor="#FFFFFF" class="normalfntMid">&nbsp;</td>
                  <td width="26%" bgcolor="#FFFFFF" class="normalfntMid">&nbsp;</td>
                  </tr>
                <tr>
                  <td bgcolor="#D1F9FC" class="txtNumber">Sample Year</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['intSampleYear'];?></td>
                  <td bgcolor="#D1F9FC" class="txtNumber">Sample No</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['intSampleNo'];?></td>
                  </tr>
                <tr>
                  <td bgcolor="#D1F9FC" class="txtNumber">Graphic No</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['strGraphicNo'];?></td>
                  <td bgcolor="#D1F9FC" class="txtNumber">Rev No</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['intRevisionNo'];?></td>
                </tr>
                <tr>
                  <td bgcolor="#D1F9FC" class="txtNumber">Combo</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['strCombo'];?></td>
                  <td bgcolor="#D1F9FC" class="txtNumber">Print</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['strPrintName'];?></td>
                </tr>
                <tr>
                  <td bgcolor="#D1F9FC" class="txtNumber">Order Qty</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['intQty'];?></td>
                  <td bgcolor="#D1F9FC" class="txtNumber">Price</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $row['dblPrice'];?></td>
                </tr>
                <tr>
                  <td bgcolor="#D1F9FC" class="txtNumber">PSD</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['dtPSD'];?></td>
                  <td bgcolor="#D1F9FC" class="txtNumber">Deliver Date</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['dtDeliveryDate'];?></td>
                </tr>
              </table>
            </div></td>
            <td width="16%" height="22" class="normalfnt">&nbsp;</td>
            <td width="10%" align="left">&nbsp;</td>
            </tr>
          <tr>
            <td height="19" class="normalfnt">&nbsp;</td>
            <td align="left">&nbsp;</td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td align="left">&nbsp;</td>
          </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td align="left">&nbsp;</td>
          </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td align="right">&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <?php 
			$plan2Garment = (float)$Mrow['intQty']-$actual1Qty;
	  ?>
      <tr>
        <td><!--<div style="width:900px;height:300px;overflow:scroll" >-->
          <table width="100%" class="grid" id="tblMainGrid" >
            <tr class="">
              <td height="22" colspan="2" bgcolor="#F3E2BC" class="normalfntRight" >Garment Qty</td>
              <td colspan="3" bgcolor="#CAE4FB" class="normalfntsm" >Plan
                <input class="<?php echo "validate[max[".$Mrow['intQty']."],min[0],custom[integer]]";?>" type="text" value="<?Php echo $plan1Qty; ?>" style="width:40px" name="txt1Plan" id="txt1Plan" />
                Actual
                  <input class="<?php echo "validate[max[".$Mrow['intQty']."],min[0],custom[integer]]";?>" type="text"  value="<?Php echo $actual1Qty; ?>" style="width:40px" name="txt1Actual" id="txt1Actual" /></td>
              <td colspan="3" bgcolor="#CFF9BD" class="normalfnt"><span class="normalfntsm">Plan
                  <input class="<?php echo "validate[max[".$plan2Garment."],min[0],custom[integer]]";?>" type="text"  value="<?Php echo $plan2Qty; ?>" style="width:40px" name="txt2Plan" id="txt2Plan" />
Actual
<input type="text" style="width:40px" class="<?php echo "validate[max[".$plan2Garment."],min[0],custom[integer]]";?>"  value="<?Php echo $actual2Qty; ?>" name="txt2Actual" id="txt2Actual" />
              </span></td>
              
              <?php
			  	########### create 3rd maxmimum qty #########################
				$thirdMaxQty =$Mrow['intQty']- ($actual2Qty+$actual1Qty);
			  ?>
              <td colspan="3" bgcolor="#EFCAF7" class="normalfnt"><span class="normalfntsm">Plan
                  <input type="text" class="<?php echo "validate[max[".$thirdMaxQty."],min[0],custom[integer]]";?>"  value="<?Php echo $plan3Qty; ?>" style="width:40px" name="txt3Plan" id="txt3Plan" />
Actual
<input type="text" style="width:40px" class="<?php echo "validate[max[".$thirdMaxQty."],min[0],custom[integer]]";?>"  value="<?Php echo $actual3Qty; ?>" name="txt3Actual" id="txt3Actual" />
              </span></td>
              <td colspan="3" bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
              </tr>
            <tr class="">
              <td width="23%" height="22" bgcolor="#F3E2BC" class="normalfnt" >Item</td>
              <td width="5%" height="22" bgcolor="#F3E2BC" class="normalfnt" >UOM</td>
              <td width="4%" bgcolor="#CAE4FB" class="normalfnt" >1st Day Plan Qty</td>
              <td width="8%" bgcolor="#CAE4FB" class="normalfnt" >1st Day Allocated Qty</td>
              <td width="5%" bgcolor="#CAE4FB" class="normalfnt" >1st Day Actual Qty</td>
              <td width="6%" bgcolor="#CFF9BD" class="normalfnt">25% Plan Qty</td>
              <td width="8%" bgcolor="#CFF9BD" class="normalfnt">25% Allocated Qty</td>
              <td width="6%" bgcolor="#CFF9BD" class="normalfnt">25% Actual Qty</td>
              <td width="5%" bgcolor="#EFCAF7" class="normalfnt">75% Plan Qty</td>
              <td width="7%" bgcolor="#EFCAF7" class="normalfnt">75% Allocated Qty</td>
              <td width="5%" bgcolor="#EFCAF7" class="normalfnt">75% Actual Qty</td>
              <td width="6%" bgcolor="#FFFFFF" class="normalfnt">Total Plan Qty</td>
              <td width="7%" bgcolor="#FFFFFF" class="normalfnt">Total Allocated Qty</td>
              <td width="5%" bgcolor="#FFFFFF" class="normalfnt">Total Actual Qty</td>
              </tr>
              <?php
			  	$sql = "select intItem,sum(qty) as qty,strName,unitName from (SELECT
							trn_sample_color_recipes.intItem,
							Sum(trn_sample_color_recipes.dblWeight/1000) AS qty,
							mst_item.strName,
							mst_units.strName as unitName
						FROM
							trn_sample_color_recipes
							Inner Join mst_item ON mst_item.intId = trn_sample_color_recipes.intItem
							inner join mst_units on mst_item.intUOM = mst_units.intId
						WHERE
							trn_sample_color_recipes.intSampleNo 	=  '".$Mrow['intSampleNo']."' AND
							trn_sample_color_recipes.intSampleYear 	=  '".$Mrow['intSampleYear']."' AND
							trn_sample_color_recipes.intRevisionNo 	=  '".$Mrow['intRevisionNo']."' AND
							trn_sample_color_recipes.strCombo 		=  '".$Mrow['strCombo']."' AND
							trn_sample_color_recipes.strPrintName 	=  '".$Mrow['strPrintName']."'
						GROUP BY
							trn_sample_color_recipes.intItem
							
						UNION
						
						SELECT
							trn_sample_foil_consumption.intItem,
							Sum(trn_sample_foil_consumption.dblMeters) AS qty,
							mst_item.strName,
							'Meter' as unitName
						FROM
							trn_sample_foil_consumption
							Inner Join mst_item ON mst_item.intId = trn_sample_foil_consumption.intItem
						WHERE
							trn_sample_foil_consumption.intSampleNo 		=  '".$Mrow['intSampleNo']."' AND
							trn_sample_foil_consumption.intSampleYear 		=  '".$Mrow['intSampleYear']."' AND
							trn_sample_foil_consumption.intRevisionNo 		=  '".$Mrow['intRevisionNo']."' AND
							trn_sample_foil_consumption.strCombo 			=  '".$Mrow['strCombo']."' AND
							trn_sample_foil_consumption.strPrintName 		=  '".$Mrow['strPrintName']."'
						GROUP BY
							trn_sample_foil_consumption.intItem
						
						UNION
						
						SELECT
							trn_sample_spitem_consumption.intItem,
							Sum(trn_sample_spitem_consumption.dblQty),
							mst_item.strName,
							mst_units.strName AS unitName
						FROM
							trn_sample_spitem_consumption
							Inner Join mst_item ON mst_item.intId = trn_sample_spitem_consumption.intItem
							Inner Join mst_units ON mst_units.intId = mst_item.intUOM
						WHERE
							trn_sample_spitem_consumption.intSampleNo 		=  '".$Mrow['intSampleNo']."' AND
							trn_sample_spitem_consumption.intSampleYear 	=  '".$Mrow['intSampleYear']."' AND
							trn_sample_spitem_consumption.intRevisionNo 	=  '".$Mrow['intRevisionNo']."' AND
							trn_sample_spitem_consumption.strCombo 			=  '".$Mrow['strCombo']."' AND
							trn_sample_spitem_consumption.strPrintName 		=  '".$Mrow['strPrintName']."'
						GROUP BY
							trn_sample_spitem_consumption.intItem
							
						) MAIN 
						GROUP BY intItem


						";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					 $sql2 = "SELECT
							ware_bulkallocation_details.dbl1AllocatedQty,
							ware_bulkallocation_details.dbl1ActualQty,
							ware_bulkallocation_details.dbl25AllocatedQty,
							ware_bulkallocation_details.dbl25ActualQty,
							ware_bulkallocation_details.dbl75AllocatedQty,
							ware_bulkallocation_details.dbl75ActualQty
							FROM ware_bulkallocation_details
							WHERE
							ware_bulkallocation_details.intLocationId =  '$locationId' AND
							ware_bulkallocation_details.intOrderNo =  '$orderNo' AND
							ware_bulkallocation_details.intOrderYear =  '$orderYear' AND
							ware_bulkallocation_details.intSalesOrderId =  '$salesNo' AND
							ware_bulkallocation_details.intItemId =  '".$row['intItem']."'
							";
					$result2 = $db->RunQuery($sql2);
					$row2 = mysqli_fetch_array($result2);
					$totalAllocated1 = $row2['dbl1AllocatedQty'];
					$dbl1ActualQty = $row2['dbl1ActualQty'];
					
					$totalAllocated2 = $row2['dbl25AllocatedQty'];
					$dbl2ActualQty = $row2['dbl25ActualQty'];
					
					$totalAllocated3 = $row2['dbl75AllocatedQty'];
					$dbl3ActualQty = $row2['dbl75ActualQty'];
			  ?>
              
            <tr class="">
              <td height="22" bgcolor="#FFFFFF" class="normalfnt" id="<?php echo $row['intItem'] ?>" ><?php echo $row['strName']; ?></td>
              <td height="22" bgcolor="#FFFFFF" class="normalfntMid" ><?php echo $row['unitName']; ?></td>
              <td bgcolor="#FFFFFF" class="normalfntRight" ><span id="<?php echo $row['qty']; ?>" class="clsQty"><?php echo number_format($row['qty']*$plan1Qty,2); ?></span></td>
              <td align="right" bgcolor="#FFFFCE" class="normalfntRight" ><span class="cls1Allocated"><?php echo number_format($totalAllocated1,2); ?></span><?PHP if($intStage==1 || $intStage==''){ ?><img class="but1Allocated mouseover"  src="../../../../../images/add.png" width="16" height="16" /><?php }?></td>
              <td bgcolor="#FFFFFF" class="normalfntRight" ><?php echo number_format( $dbl1ActualQty,2); ?></td>
              <td bgcolor="#FFFFFF" class="normalfntRight"><span class="clsQty2"><?php echo number_format(($dbl1ActualQty/$actual1Qty)*$plan2Qty,2); ?></span></td>
              <td align="right" bgcolor="#FFFFCE" class="normalfntRight" ><span class="cls2Allocated"><?php  echo number_format($totalAllocated2-($totalAllocated2==0?0:$totalAllocated1),2); 
			  
			  ?></span><?PHP if($intStage==2){ ?><img class="but2Allocated mouseover"  src="../../../../../images/add.png" width="16" height="16" /><?php }?></td>
              <td bgcolor="#FFFFFF" class="normalfntRight"><?php echo number_format( $dbl2ActualQty,2); ?></td>
              <td bgcolor="#FFFFFF" class="normalfntRight"><span class="clsQty3"><?php echo number_format(($dbl2ActualQty/$actual2Qty)*$plan3Qty,2); ?></span></td>
              <td align="right" bgcolor="#FFFFCE" class="normalfntRight" ><span class="cls3Allocated">
                <?php  echo number_format($totalAllocated3-($totalAllocated3==0?0:$totalAllocated2),2); 
			  
			  ?>
                </span>
                <?PHP if($intStage==3 || $intStage==2){ ?>
                <img class="but3Allocated mouseover"  src="../../../../../images/add.png" width="16" height="16" />
                <?php }?></td>
              <td bgcolor="#FFFFFF" class="normalfntRight"><?php echo number_format( $dbl3ActualQty,2); ?></td>
              <td bgcolor="#FFFFFF" class="normalfntRight" ><span id="<?php echo $row['qty']; ?>" class="clsQty3"><?php 
			  echo number_format(($row['qty']*$plan1Qty)+(($dbl1ActualQty/$actual1Qty)*$plan2Qty)+($dbl2ActualQty/$actual2Qty)*$plan3Qty,2); ?></span></td>
              <td bgcolor="#FFFFFF" class="normalfntRight"><?php echo number_format($totalAllocated1+$totalAllocated2-($totalAllocated2==0?0:$totalAllocated1)+$totalAllocated3-($totalAllocated3==0?0:$totalAllocated2),2); ?></td>
              <td bgcolor="#FFFFFF" class="normalfntRight" ><?php echo number_format( $dbl1ActualQty+$dbl2ActualQty,2); ?></td>
              </tr>
            <?php
				}
			?>
          </table>
          <!--</div>--></td>
      </tr>
      <tr>
        <td align="center" class=""><table width="100%" border="0">
          <tr>
            <td width="28%" class="normalfnt">&nbsp;</td>
            <td width="72%" valign="top" class="normalfnt">&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><img id="butNew" class="mouseover" src="../../../../../images/Tnew.jpg" width="92" height="24" /><img id="butSave" src="../../../../../images/Tsave.jpg" width="92" height="24" class="mouseover" /><img src="../../../../../planning/img/Tclose.jpg" width="92" height="24" /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<div    style="width:500px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</body>
</html>

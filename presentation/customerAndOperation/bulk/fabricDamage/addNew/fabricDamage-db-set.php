<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo 	 = $_REQUEST['serialNo'];
	$year 		 = $_REQUEST['Year'];
	$orderNo  	 = $_REQUEST['orderNo'];
	$orderYear 	 = $_REQUEST['orderYear'];
	
	$arr 		= json_decode($_REQUEST['arr'], true);

//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		if($serialNo==''){
			$serialNo 	= getNextSerialNo();
			$year = date('Y');
			$editMode=0;
		}
		else{
			$editMode=1;
		}
		//-----------delete and insert to detail table-----------------------
			$saved=0;
			$toSave=0;
			$rollBackMsg="Maximum Qty for following items are...";
			foreach($arr as $arrVal)
			{
				$place = 'Stores';
				$salesOrderNo 		= $arrVal['salesNo'];
				$salesOrderId 		= $arrVal['salesId'];
				$size 	 			= $arrVal['size'];
				$grade 	 			= $arrVal['grade'];
				$cutReturn 		 	= $arrVal['cutReturn'];
				$fDamage 		 	= $arrVal['fDamage'];
		
				if($cutReturn>0)
				{
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`intSalesOrderId`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','$location','$serialNo','$year','$orderNo','$orderYear','$salesOrderId',
					'$size','$grade','".($cutReturn*-1)."','CUTRETURN','$userId',now())";
					$result = $db->RunQuery2($sql);
					if($result)
					{
					$sql = "INSERT INTO `ware_stocktransactions_fabric_complete` (`intLocationId`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`intSalesOrderId`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$location','$serialNo','$year','$orderNo','$orderYear','$salesOrderId','$size','$grade','".($cutReturn)."','CUTRETURN','$userId',now())";
					$result = $db->RunQuery2($sql);
					}	
				}
				if($fDamage>0)
				{
					$sql = "INSERT INTO `ware_stocktransactions_fabric` (`intLocationId`,`strPlace`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`intSalesOrderId`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$place','$location','$serialNo','$year','$orderNo','$orderYear','$salesOrderId',
					'$size','$grade','".($fDamage*-1)."','FDAMAGE','$userId',now())";
					$result = $db->RunQuery2($sql);
					if($result)
					{
					$sql = "INSERT INTO `ware_stocktransactions_fabric_complete` (`intLocationId`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`intSalesOrderId`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$location','$serialNo','$year','$orderNo','$orderYear','$salesOrderId','$size','$grade','".($fDamage)."','FDAMAGE','$userId',now())";
					$result = $db->RunQuery2($sql);
					}	
				}
					
					if($result==1){
					$saved++;
					//}
				}
				$toSave++;
		}
		//echo $rollBackFlag;
		
		
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
				$response['msg'] 		= 'Updated successfully.';
			else
				$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 			= $year;
		
		
		$db->CloseConnection();		
	}
//----------------------------------------
	echo json_encode($response);
//----------------------------------------




//----------------------------------------
	function getNextSerialNo()
	{
		global $db;
		global $company;
		$sql = "SELECT
				sys_no.intFabricDamage
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$company'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextNo = $row['intFabricDamage'];
		
		$sql = "UPDATE `sys_no` SET intFabricDamage=intFabricDamage+1 WHERE (`intCompanyId`='$company')  ";
		$db->RunQuery2($sql);
		
		return $nextNo;
	}
	//-----------------------------------------------------------
function loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size)
{
		global $db;
		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				ware_stocktransactions_fabric.intLocationId =  '$location' AND
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType =  'Received' 
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//-----------------------------------------------------------
function loadOrderQty($location,$orderNo,$orderYear,$salesOrderNo,$size)
{
		global $db;
		$sql = "SELECT
				trn_ordersizeqty.dblQty
				FROM trn_ordersizeqty
				WHERE
				trn_ordersizeqty.intOrderNo =  '$orderNo' AND
				trn_ordersizeqty.intOrderYear =  '$orderYear' AND
				trn_ordersizeqty.strSalesOrderNo =  '$salesOrderNo' AND
				trn_ordersizeqty.strSize =  '$size'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//-----------------------------------------------------------
?>
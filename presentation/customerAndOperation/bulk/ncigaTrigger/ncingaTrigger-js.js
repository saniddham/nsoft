// JavaScript Document
var basePath	="presentation/customerAndOperation/bulk/ncigaTrigger/";
var measurments = [];

//load orderNos
$(document).ready(function() {

	$('#frmNcinga #cboYear').die('change').live('change', function () {
		var orderYear=$('#cboYear').val();
		loadOrderNo(orderYear);


	});

//load SalesOrderNo
	$('#frmNcinga #cboStyleNo').die('change').live('change', function () {

		$('#frmNcinga #tr_detail').css('display', 'none');
		var orderYear = $('#cboYear').val();
		var orderNo=$('#cboStyleNo').val();
		loadSalesOrderNo(orderYear,orderNo);




	});


	$('#frmNcinga #butTrigger').click(function() {

		var orderType = $('#trigOpt').val();
		var orderYear = $('#cboYear').val();
		var orderNo = $('#cboStyleNo').val();
		var salesOrderNo = $('#cboSampleNo').val();

		if (!$('#frmNcinga').validationEngine('validate')) {
			var t = setTimeout("alertx('#frmNcinga')", 2000);
			return;
		} else {


			var val = $.prompt('Are you sure you want to send order data to Ncinga ?', {
				buttons: {Ok: true, Cancel: false},
				callback: function (v, m, f) {
					if (v) {

						var url = basePath + "ncingaManualTrigger-db-set.php?requestType=sendData&orderYear=" + orderYear + "&orderNo=" + orderNo + "&salesOrderNo=" + salesOrderNo + "&orderType=" + orderType;

						var obj = $.ajax({
							url: url,
							dataType: "json",
							type: 'POST',
							data: '',
							async: false,

							success: function (json) {
								$('#frmNcinga #butTrigger').validationEngine('showPrompt', json.msg, json.type /*'pass'*/);
								if (json.type == 'pass') {
									var t = setTimeout("alertx('#frmNcinga #butTrigger')", 2000);


								}
								var t = setTimeout("alertx('#frmNcinga #butTrigger')", 4000);
							},

							error: function (xhr, status) {

								$(' #frmNcinga #butTrigger').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
								var t = setTimeout("alertx('#frmNcinga #butTrigger')", 4000);

							}


						});
					}

				}


			});
		}


	});

	$('#frmNcinga #butClear').click(function () {

		$("#frmNcinga")[0].reset();



	});


	function loadOrderNo(year)
	{


		var url = basePath+"ncinga-db-get.php?requestType=loadOrderNo&orderYear="+year;
		var obj = $.ajax({url:url,async:false});
		$('#cboStyleNo').html(obj.responseText);


	}
	function loadSalesOrderNo(year,orderNo)
	{
		var url = basePath+"ncinga-db-get.php?requestType=loadSalesOrderNo&orderYear="+year+"&orderNo="+orderNo;


		var obj = $.ajax({url:url,async:false});
		$('#cboSampleNo').html(obj.responseText);


	}

	function alertx(obj)
	{
		$(obj).validationEngine('hide')	;
	}






});


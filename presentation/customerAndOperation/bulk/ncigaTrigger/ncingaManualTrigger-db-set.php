<?php
session_start();
$mainPath 	= $_SESSION['mainPath'];
$userId 	= $_SESSION['userId'];
$companyId 	= $_SESSION['CompanyID'];

$programName='Bulk Production';
$programCode='P1279';

ini_set('max_execution_time',7000);
include "../../../../dataAccess/Connector.php";
include "../../../../dataAccess/DBManager2.php";
include "../../../../libraries/mail/mail.php";
require_once "../../../../class/cls_mail.php";
require_once "../../../../class/cls_permisions.php";
require_once "../../../../class/cls_commonFunctions_get.php";
require_once "../../../../class/warehouse/cls_warehouse_get.php";

$db = new DBManager2(1);
$db->connect();

$objMail 			= new cls_create_mail($db);
$obj_permision		= new cls_permisions($db);
$obj_commom			= new cls_commonFunctions_get($db);
$obj_warehouse_get	= new cls_warehouse_get($db);

$orderNo 	= $_REQUEST['orderNo'];
$orderYear 	= $_REQUEST['orderYear'];
$salesOrderNo=$_REQUEST['salesOrderNo'];
$response = array('type'=>'', 'msg'=>'');

    if($_REQUEST['requestType']=='sendData') {
        $orderType = $_REQUEST['orderType'];
        $record = prepareRecord($orderNo, $orderYear, $salesOrderNo, $orderType);
        $prev_token = select_token();
        $is_sent = send_details($record,$prev_token);

       if($is_sent){
           $response['type'] 		= 'pass';
            $response['msg'] 		= 'Data Submitted Successfully';
        }
       else{
           $response['type'] 		= 'Fail';
            $response['msg'] 		= "Error Submitting data";
        }
        echo json_encode($response);
    }



    function newRecord($orderNo,$orderYear,$salesOrderNo,$size,$qty,$successStatus,$nciga_code)
    {
        global $db;

        $date = date("Y-m-d");
        $user = $_SESSION['userId'];
        if($_REQUEST['orderType']==0){
            $orderType='new';
        }elseif ($_REQUEST['orderType']==1){
            $orderType='revised';
        }else{
            $orderType='cancel';
        }



        $sql_insert = "INSERT INTO `manual_ncinga_trigger` (
                        `intOrderYear`,
                        `intOrderNo`,
                        `intSalesOrderId`,
                        `strSize`,
                        `dblQty`,
                        `ncingaDeliveryStatus`,
                        `date`,
                        `user`,
                        `orderType`,
                        `nciga_code`
                     ) VALUES ('$orderYear','$orderNo','$salesOrderNo','$size','$qty','$successStatus','$date','$user','$orderType','$nciga_code')";

         $db->RunQuery($sql_insert);



    }

    function prepareRecord($orderNo, $orderYear, $salesOrderNo,$orderType)
    {
        global  $db;

        $sql_select = "SELECT
                        trn_orderheader.intReviseNo,
                        trn_orderheader.intCreator,
                        trn_orderheader.intStatus,
                        trn_orderheader.intApproveLevelStart,
                        trn_orderheader.strCustomerPoNo,
                        trn_orderheader.intLocationId,
                        trn_orderheader.strTentitiveCustomerPoNo,
                        trn_orderheader.TENTATIVE,
                        mst_locations.intCompanyId,
                        mst_locations.strName AS companyLocation,
                        trn_orderheader.intCustomer,
                        mst_customer.strName AS customer,
                        mst_customer_locations_header.strName AS customerLocation,
                        mst_companies.strName AS company,
                        trn_orderheader.strCustomerPoNo,
                        trn_orderdetails.intSalesOrderId,
                        trn_orderdetails.strTentitiveSalesOrderNo,
                        trn_orderdetails.strSalesOrderNo,
                        trn_orderdetails.intOrderNo,
                        trn_orderdetails.intOrderYear,
                        trn_orderdetails.strLineNo,
                        trn_orderheader.dtDeliveryDate AS poDeliveryDate,
                        trn_orderdetails.dtDeliveryDate AS soDeliverydate,
                        trn_orderdetails.strGraphicNo,
                        trn_orderdetails.intSampleNo,
                        trn_orderdetails.intSampleYear,
                        trn_orderdetails.strStyleNo,
                        trn_orderdetails.dtPSD,
                        trn_orderdetails.intQty AS poqty,
                        trn_orderheader.dtDate,
                        trn_orderdetails.intOrderYear,
                        trn_orderdetails.intPart,
                        mst_part.strName as partName,
                        trn_orderdetails.dblDamagePercentage,
                        trn_ordersizeqty.strSize,
                        trn_ordersizeqty.dblQty,
                        mst_brand.strName AS brand,
                        trn_sampleinfomations_details.intGroundColor,
                        mst_colors_ground.strName as groundColor,
                        mst_customer_locations.BundleTag
                            FROM
                            trn_orderheader
                            INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
                            AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
                            INNER JOIN mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
                            INNER JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
                            INNER JOIN mst_customer_locations_header ON mst_customer_locations_header.intId = trn_orderheader.intCustomerLocation
                            INNER JOIN mst_customer_locations ON mst_customer_locations.intCustomerId = mst_customer.intId
                            AND mst_customer_locations.intLocationId = mst_customer_locations_header.intId
                            INNER JOIN mst_companies ON mst_companies.intId = mst_locations.intCompanyId
                            INNER JOIN mst_part ON mst_part.intId = trn_orderdetails.intPart
                            INNER JOIN trn_ordersizeqty ON trn_ordersizeqty.intOrderNo = trn_orderdetails.intOrderNo
                            AND trn_ordersizeqty.intOrderYear = trn_orderdetails.intOrderYear
                            AND trn_ordersizeqty.intSalesOrderId = trn_orderdetails.intSalesOrderId
                            INNER JOIN trn_sampleinfomations_details ON trn_orderdetails.intSampleNo = trn_sampleinfomations_details.intSampleNo
                            AND trn_orderdetails.intSampleYear = trn_sampleinfomations_details.intSampleYear
                            AND trn_orderdetails.intRevisionNo = trn_sampleinfomations_details.intRevNo
                            AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName
                            AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
                            INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                            AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                            AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo
                            INNER JOIN mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
                            iNNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
                            WHERE
                                    trn_orderheader.intOrderNo = '$orderNo'
                                    AND trn_orderheader.intOrderYear = '$orderYear'";

                            if ($salesOrderNo != '') {
                                $sql_select .= " AND trn_orderdetails.strSalesOrderNo ='$salesOrderNo'";
                            }
                            $sql_select .= "
                                    GROUP BY
                                    trn_orderdetails.intOrderNo,
                                    trn_orderdetails.intOrderYear,
                                    trn_orderdetails.intSalesOrderId,
                                    trn_ordersizeqty.strSize ";

        $finalRecord = array();
        $result1 = $db->RunQuery($sql_select);

        while ($row = mysqli_fetch_array($result1)) {
            $value = array();
            $customer = trim($row['customer']);
            $customerLocation = trim($row['customerLocation']);
            $company = trim($row['company']);
            $companyLocation = trim($row['companyLocation']);
            $strCustomerPoNo = trim($row['strCustomerPoNo']);
            $intSalesOrderId = $row['intSalesOrderId'];
            $strSalesOrderNo = trim($row['strSalesOrderNo']);
            $intOrderNo = $row['intOrderNo'];
            $strLineNo = trim($row['strLineNo']);
            $poDeliveryDate = $row['poDeliveryDate'];
            $soDeliverydate = $row['soDeliverydate'];
            $strGraphicNo = trim($row['strGraphicNo']);
            $intSampleNo = $row['intSampleNo'];
            $intSampleYear = $row['intSampleYear'];
            $groundColor = trim($row['groundColor']);
            $strStyleNo = trim($row['strStyleNo']);
            $backgroundColor = $row['intSampleYear'];
            $strSize = trim($row['strSize']);
            $dtPSD = $row['dtPSD'];
            $dblQty = $row['dblQty'];
            $poqty  = $row['poqty'];
            $dtDate = $row['dtDate'];
            $intOrderYear = $row['intOrderYear'];
            $partName = trim($row['partName']);
            $brand = trim($row['brand']);
            $dblDamagePercentage = $row['dblDamagePercentage'];
            $bundleTag = trim($row['BundleTag']);
            $tentitive = trim($row['TENTATIVE']);
            if ($tentitive > 0){
                $strCustomerPoNo = trim($row['strTentitiveCustomerPoNo']);
                $strSalesOrderNo = trim($row['strTentitiveSalesOrderNo']);
            }
            if($_REQUEST['orderType']==0){
                $orderType='new';
            }elseif ($_REQUEST['orderType']==1){
                $orderType='revised';
            }else{
                $orderType='cancel';
            }



            //---------------assign to array---------------------
            $value["transactionId"] = $intOrderNo . "." . $intOrderYear . "." . $intSalesOrderId . "." . $strSize . "";
            $value["customer"] = "$customer";
            $value["customerLocation"] = " $customerLocation";
            $value["company"] = "$company";
            $value["companyLocation"] = "$companyLocation";
            $value["customerPO"] = "$strCustomerPoNo";
            $value["customerSO"] = "$strSalesOrderNo";
            $value["SalesOrderId"] = "$intSalesOrderId";
            $value["orderNo"] = "$intOrderNo";
            $value["lineNo"] = "$strLineNo";
            $value["poDeliveryDate"] = "$poDeliveryDate";
            $value["graphicNo"] = "$strGraphicNo";
            $value["sampleNo"] = "$intSampleYear" . "/" . "$intSampleNo";
            $value["brand"] = "$brand";
            $value["style"] = "$strStyleNo";
            $value["backGroundColor"] = "$groundColor";
            $value["size"] = "$strSize";
            $value["PSD"] = "$dtPSD";
            $value["deliveryDate"] = "$soDeliverydate";
            $value["qty"] = $dblQty;
            $value["poQty"] = $poqty;
            $value["actualTotalQty"] = $dblQty;
            $value["date"] = "$dtDate";
            $value["year"] = "$intOrderYear";
            $value["part"] = "$partName";
            $value["PDPercentage"] = " $dblDamagePercentage";
            $value["damagePercentage"] = " $dblDamagePercentage";
            $value["bundleTag"] = $bundleTag;
            $value["type"] = "order";
            $value["orderType"] = "$orderType";


            $recordList = $value;
            $recordArray = array('value' => $recordList);
            array_push($finalRecord, new ArrayObject($recordArray));
        }

        return $finalRecord;

    }

    function send_details($record, $token)
    {

        if ($token == "") { //if token null
            generate_token($record);
        }

        $configs = include('../../../../config/ncigaConfig.php');
        $url = $configs['URL'];

        //data passed with the token
        $data = array('token' => $token, 'records' => $record);
        //encode in json format
        $data_json = json_encode($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);
        $nciga_response = json_decode($response); //get nciga response
        $message_code = $nciga_response->messageCode;

        if ($message_code == 'NCN0003') { //token expired
            generate_token($record);
            update_tbl($record, $message_code);
        } else if ($message_code == 'NCN0001') { //success
            update_tbl($record, $message_code);
        }
        else if ($message_code == 'NCN0002') {//invalid token
            generate_token($record);
            update_tbl($record, $message_code);
        } else if ($message_code == 'NCN0007') {//Failed to submit data
            update_tbl($record, $message_code);
        } else if ($message_code == 'NCN0005') {//Invalid URL
            update_tbl($record, $message_code);
        } else if ($message_code == 'NCN0006') {//Internal server Error , Please contact administrator
            update_tbl($record, $message_code);
        } else if ($message_code == 'NCN0008') {//Mandatory value missing
            update_tbl($record, $message_code);
        } else if ($message_code == 'NCN0009') {//Mandatory value missing
            update_tbl($record, $message_code);
        }
        if (!$response) {
                return false;
        }
        curl_close($ch);
        return true;
    }

    function generate_token($record)
    {
        $configs = include('../../../../config/ncigaConfig.php');
        $url = $configs['URL'];
        $AUTH_USERNAME = $configs['AUTH_USERNAME'];
        $AUTH_PASSWORD = $configs['AUTH_PASSWORD'];
        $TYPE = $configs['TYPE'];
        $data = array('username' => $AUTH_USERNAME, 'password' => $AUTH_PASSWORD, 'type' => $TYPE);
        $data_json = json_encode($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);

        curl_close($ch);

        insert_tbl($response); //insert token
        $token_new = select_token(); //get newly generated token
        send_details($record, $token_new); //send details again

        if (!$response) {
            die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
        }

        curl_close($ch);
    }

    function insert_tbl($data)
    {

        global $db;
        $date = date('Y-m-d H:i:s', time());

        $jsonArray = json_decode($data, true);
        $userId = $jsonArray['userId'];
        $generatedTime = $jsonArray['generatedTime'];
        $expireDate = $jsonArray['expireDate'];
        $token = $jsonArray['token'];

        $sql_insert = "INSERT INTO
                            tbl_token_details
                            (USER_ID,
                            GENERATED_TIME,
                            EXPIRED_DATE,
                            TOKEN,
                            UPDATED_TIME
                            )
                            VALUES
                            ('$userId',
                             $generatedTime,
                             $expireDate,   
                             '$token',
                             '$date'  
                            );";

        $db->RunQuery($sql_insert);
    }

//--------------------------------------------------------
    function select_token()
    {

        global $db;
        $sql_token = "SELECT UPDATED_TIME, date(UPDATED_TIME),TOKEN
                          FROM tbl_token_details AS a
                          WHERE (UPDATED_TIME)= (
                          SELECT MAX((UPDATED_TIME))
                          FROM tbl_token_details AS b )";

        $result = $db->RunQuery($sql_token);
        $row = mysqli_fetch_array($result);
        $token = $row['TOKEN'];
        return $token;
    }

    function update_tbl($record,$message_code)
    {
        global $db;
        $nciga_code=$message_code;

        foreach ($record as $key => $val) {

            foreach ($val as $data) {

                $orderNo = $data['orderNo'];
                $year = $data['year'];
                $customerSO = $data['SalesOrderId'];
                $size = $data['size'];
                $qty = $data['qty'];
                $success_status = 0;

                 if ($message_code == 'NCN0001') {
                    $success_status = 1;
                }
                 newRecord($orderNo,$year,$customerSO,$size,$qty,$success_status,$nciga_code);


            }

        }

    }
    $db->disconnect();


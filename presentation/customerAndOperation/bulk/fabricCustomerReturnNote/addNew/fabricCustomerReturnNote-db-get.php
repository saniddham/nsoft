<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once "{$backwardseperator}class/customerAndOperation/cls_textile_stores.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_permisions.php";
	
	//---------check Permission to save recive qty more than PO qty.------------	
	$objpermisionget= new cls_permisions($db);
	$exceedPOPermision 	= $objpermisionget->boolSPermision(5);
	//------------------------------------------------------------------------
	
	$programName='Fabric Customer Return';
	$programCode='P0648';
	
	/////////// type of print load part /////////////////////
if($requestType=='loadGraphicNo')
	{
		$styleNo  = $_REQUEST['styleNo'];
		$sql = "SELECT DISTINCT
				trn_orderdetails.strGraphicNo
				FROM
				trn_orderdetails
				Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
				WHERE
				trn_orderheader.intStatus =  '1' AND
				trn_orderdetails.strStyleNo =  '$styleNo' 
				ORDER BY trn_orderdetails.strGraphicNo ASC";
		$html1 = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		$i=0;
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['strGraphicNo']."\">".$row['strGraphicNo']."</option>";
				$i++;
		}
		//if($i>1){
			$html=$html1.$html;
		//}
				$response['graphicNo'] = $html;
		
		echo json_encode($response);
	}
	//------------------------------
if($requestType=='loadCustomerPONo')
	{
		$orderYear  = $_REQUEST['orderYear'];
		$styleNo  = $_REQUEST['styleNo'];
		$graphicNo  = $_REQUEST['graphicNo'];
		$sql = "SELECT DISTINCT 
				trn_orderheader.strCustomerPoNo
				FROM
				trn_sampleinfomations
				Inner Join trn_orderdetails ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_orderdetails.intRevisionNo AND trn_sampleinfomations.strGraphicRefNo = trn_orderdetails.strGraphicNo
				Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
				WHERE 
				trn_orderheader.intStatus = '1' AND 
				trn_orderdetails.strStyleNo =  '$styleNo'  ";
				if($graphicNo!=''){
				$sql .= "AND
				trn_orderdetails.strGraphicNo =  '$graphicNo'  ";
				}
				
				$sql .= "AND 
				trn_orderheader.strCustomerPoNo <> '' 
				ORDER BY trn_orderheader.strCustomerPoNo ASC 
				";
		$html1 = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		$i=0;
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['strCustomerPoNo']."\">".$row['strCustomerPoNo']."</option>";
				$i++;
		}
		//if($i>1){
			$html=$html1.$html;
		//}
				$response['customerPoNo'] = $html;
		echo json_encode($response);
	}
	//------------------------------
	else if($requestType=='loadPoNo')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderYear  = $_REQUEST['orderYear'];
		
		$sql = "SELECT DISTINCT 
				trn_orderheader.strCustomerPoNo,
				trn_orderheader.intCustomer 
				FROM trn_orderheader 
				WHERE  
				trn_orderheader.intStatus = '1' AND 
				trn_orderheader.intOrderNo='$orderNo' 
				AND trn_orderheader.intOrderYear='$orderYear' 
				AND trn_orderheader.strCustomerPoNo!=''  
				ORDER BY 
				trn_orderheader.strCustomerPoNo ASC";

		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$response['poNo'] = $row['strCustomerPoNo'];
				$response['customer'] = $row['intCustomer'];
		}
		if($orderNoArray[0]==''){
				$response['poNo'] = '';
				$response['customer'] = '';
	
		//---order no
			 $sql = "SELECT DISTINCT 
					trn_orderheader.intOrderNo,
					trn_orderheader.intCustomer,
					trn_orderheader.intOrderYear
					FROM trn_orderheader
					Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId 
					WHERE 
					mst_locations.intCompanyId =  '$company' AND  
					trn_orderheader.intStatus = '1' AND 
					trn_orderheader.intOrderYear='$orderYear'
					ORDER BY
				    trn_orderheader.intOrderNo DESC"; 
					
			$html = "<option value=\"\"></option>";
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				$html .= "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
			}
				$response['orderNo'] = $html;
					
		}
		//---sales order no
		 $sql = "SELECT DISTINCT 
				trn_orderdetails.strSalesOrderNo
				FROM
				trn_orderdetails 
				Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
				WHERE 
				trn_orderheader.intStatus = '1' AND 
				trn_orderdetails.intOrderNo='$orderNo' 
				AND trn_orderdetails.intOrderYear='$orderYear'
				ORDER BY 
				trn_orderdetails.strSalesOrderNo ASC";

		$html1 = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		$i=0;
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['strSalesOrderNo']."\">".$row['strSalesOrderNo']."</option>";
				$i++;
		}
		//if($i>1){
			$html=$html1.$html;
		//}
				$response['salesOrderNo'] = $html;
		
		
		
		echo json_encode($response);
	}
	//------------------------------
else if($requestType=='loadOrderNo')
	{
		$orderYear  = $_REQUEST['orderYear'];
		$poNo  = $_REQUEST['poNo'];
		$customer  = $_REQUEST['customer'];
		
		$sql = "SELECT DISTINCT 
				trn_orderheader.intOrderNo,
				trn_orderheader.intCustomer,
				trn_orderheader.intOrderYear
				FROM trn_orderheader 
				Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
				WHERE 
				mst_locations.intCompanyId =  '$company' AND  
				trn_orderheader.intOrderYear='$orderYear' AND 
				trn_orderheader.intStatus='1' ";
				if($poNo!=''){
		$sql .= " AND trn_orderheader.strCustomerPoNo='$poNo'";			
				}
				if($customer!=''){
		$sql .= " AND trn_orderheader.intCustomer='$customer'";			
				}
		$sql .= " ORDER BY 
				 trn_orderheader.intOrderYear DESC, 
				 trn_orderheader.intOrderNo DESC"; 
				 
				
		$result = $db->RunQuery($sql);
		$html1 = "<option value=\"\"></option>";
		$i=0;
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
			$customer=$row['intCustomer'];
			$i++;
		}
		//if($i>1){
			$html=$html1.$html;
		//}
			$response['orderNo'] = $html;
			if($poNo==''){
				$response['customer'] = '';
			}
			else{
				$response['customer'] = $customer;
			}
		if($poNo==''){
			//	$response['orderNo'] = '';
				$response['customer'] = '';
				
			 $sql = "SELECT DISTINCT 
					trn_orderheader.strCustomerPoNo 
					FROM trn_orderheader
					WHERE 
					trn_orderheader.intStatus='1'  AND 
					trn_orderheader.intOrderYear='$orderYear' 
					ORDER BY 
					trn_orderheader.strCustomerPoNo ASC";
			$html1 = "<option value=\"\"></option>";
			$result = $db->RunQuery($sql);
			$i=0;
			while($row=mysqli_fetch_array($result))
			{
				$html .= "<option value=\"".$row['strCustomerPoNo']."\">".$row['strCustomerPoNo']."</option>";
				$i++;
			}
			//if($i>1){
				$html=$html1.$html;
			//}
				$response['PoNo'] = $html;
		}
		
		echo json_encode($response);
	}
	
	else if($requestType=='loadOrderNosToSalesOrderNos')
		{
		$year				= $_REQUEST['year'];
		$graphicNo			= $_REQUEST['graphicNo'];
		$styleId			= $_REQUEST['styleId'];
		$customerPONo		= $_REQUEST['customerPONo'];
		$salesOrderNo		= $_REQUEST['salesOrderNo'];
		$customerId			= $_REQUEST['customerId'];
		
			$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
		if($graphicNo!='')
			$para.=" AND trn_orderdetails.strGraphicNo =  '$graphicNo'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId'  ";
		if($customerPONo!='')
			$para.=" AND trn_orderheader.strCustomerPoNo =  '$customerPONo' ";
		if($salesOrderNo!='')
			$para.=" AND trn_orderdetails.strSalesOrderNo 	=  '$salesOrderNo'  ";	
		$sql = "SELECT DISTINCT
					trn_orderheader.intOrderNo 
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
					trn_orderheader.intStatus=1
					$para
					
				ORDER BY intOrderNo DESC
				";
				
				
		$result = $db->RunQuery($sql);
		$html ='';
		if($orderNo=='')
		$html1 = "<option value=\"\"></option>";
		$i=0;
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option ".($customerId==$row['intOrderNo']?'selected':'')." value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
			$i++;
		}
		//if($i>1){
			$html=$html1.$html;
		//}
			$response['orderNo'] = $html;
		
		echo json_encode($response);
	}
	
	//----------------------------------
	else if($requestType=='loadPONoAndOrderNo')
	{
		$styleNo  = $_REQUEST['styleNo'];
		$graphicNo  = $_REQUEST['graphicNo'];
		$poNo  = $_REQUEST['poNo'];
		$customer  = $_REQUEST['customer'];
		$orderNo  = $_REQUEST['orderNo'];
		$orderYear  = $_REQUEST['orderYear'];
		
		$sql = "SELECT DISTINCT
				trn_orderheader.strCustomerPoNo 
				FROM trn_orderheader  
				WHERE 
				trn_orderheader.intStatus='1' AND  
				trn_orderheader.intOrderYear='$orderYear' 
				AND trn_orderheader.strCustomerPoNo <> ''"; 
				
		if($customer!=''){
		$sql .= " AND  
				trn_orderheader.intCustomer='$customer'"; 
		}
		if($styleNo!=''){
		$sql .= " AND  
				trn_orderdetails.strStyleNo='$styleNo'";
		}
		if($graphicNo!=''){
		$sql .= " AND  
				trn_orderdetails.strGraphicNo='$graphicNo'"; 
		}
		$sql .= " ORDER BY 
				trn_orderheader.strCustomerPoNo ASC";
			//	echo $sql;
		$html1 = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		$i=0;
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['strCustomerPoNo']."\">".$row['strCustomerPoNo']."</option>";
			$i++;
		}
		//if($i>1){
			$html=$html1.$html;
		//}
				$response['poNo'] = $html;
				
		$sql = "SELECT DISTINCT 
				trn_orderheader.intOrderNo,
				trn_orderheader.intOrderYear
				FROM trn_orderheader  
				Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE 
				trn_orderheader.intOrderYear='$orderYear' 
				AND trn_orderheader.intStatus='1' "; 
		if($customer!=''){
		$sql .= " AND
				trn_orderheader.intCustomer='$customer'"; 
		}
		if($styleNo!=''){
		$sql .= " AND  
				trn_orderdetails.strStyleNo='$styleNo'";
		}
		if($graphicNo!=''){
		$sql .= " AND  
				trn_orderdetails.strGraphicNo='$graphicNo'"; 
		}
		$sql .= " ORDER BY   
				trn_orderheader.intOrderYear DESC, 
				trn_orderheader.intOrderNo DESC";
				//echo $sql;
		$html1 = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		$i=0;
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
			$i++;
		}
		//if($i>1){
			$html=$html1.$html;
		//}
			$response['orderNo'] = $html;
		
		
		echo json_encode($response);
	}

	//----------------------------------
	else if($requestType=='loadSalesLineNoPartSizesComboes')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderYear  = $_REQUEST['orderYear'];
		$styleNo  = $_REQUEST['styleNo'];
		$salesOrderNo  = $_REQUEST['salesOrderNo'];
		//-----------------		
		$sql = "SELECT DISTINCT 
				trn_orderdetails.strGraphicNo as graphicNo
				FROM
				trn_orderdetails 
				Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' 
				AND trn_orderheader.intStatus='1' "; 
		if($styleNo!=''){
		$sql .= " AND 
				trn_orderdetails.strStyleNo =  '$styleNo'"; 
		}
		if($salesOrderNo!=''){
		$sql .= " AND 
				trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'"; 
		}
		$sql .= " Order by trn_orderdetails.strGraphicNo ASC";
				
		$html1 = "<option value=\"\"></option>";
		$html = "";
		$result = $db->RunQuery($sql);
		$i=0;
		while($row=mysqli_fetch_array($result))
		{
				$i++;
				$html .= "<option value=\"".$row['graphicNo']."\">".$row['graphicNo']."</option>";
		}
		if($i==1){
			$html=$html.$html1;
		}
		else{
			$html=$html1.$html;
		}
				$response['graphicNo'] = $html;
 		//-----------------		
		$sql = "SELECT DISTINCT 
				trn_orderdetails.strLineNo
				FROM
				trn_orderdetails
				Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
				WHERE
				trn_orderheader.intStatus =  '1' AND
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear'"; 
		if($styleNo!=''){
		$sql .= " AND 
				trn_orderdetails.strStyleNo =  '$styleNo'"; 
		}
		if($salesOrderNo!=''){
		$sql .= " AND 
				trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'"; 
		}
		$sql .= " ORDER BY trn_orderdetails.strLineNo ASC";		
		
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['strLineNo']."\">".$row['strLineNo']."</option>";
		}
				$response['lineNo'] = $html;
 		//-----------------		
		$sql = "SELECT DISTINCT 
				trn_orderdetails.strSalesOrderNo as salesOrderNo
				FROM
				trn_orderdetails 
				Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' 
				AND trn_orderheader.intStatus='1' "; 
		if($styleNo!=''){
		$sql .= " AND 
				trn_orderdetails.strStyleNo =  '$styleNo'"; 
		}
		$sql .= " Order by trn_orderdetails.strSalesOrderNo ASC";
				
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['salesOrderNo']."\">".$row['salesOrderNo']."</option>";
		}
				$response['salesOrderNo'] = $html;
		//-----------------		
		$sql = "SELECT DISTINCT
				trn_orderdetails.intPart,
				mst_part.strName
				FROM
				trn_orderdetails 
				Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
				left Join mst_part ON trn_orderdetails.intPart = mst_part.intId
				WHERE  
				trn_orderheader.intStatus='1' AND 
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear'"; 
		if($styleNo!=''){
		$sql .= " AND 
				trn_orderdetails.strStyleNo =  '$styleNo'"; 
		}
		if($salesOrderNo!=''){
		$sql .= " AND 
				trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'"; 
		}
		$sql .= " Order by mst_part.strName ASC";
				 
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['intPart']."\">".$row['strName']."</option>";
		}
				$response['partNo'] = $html;
		//-----------------		
	    $sql = "SELECT DISTINCT
				trn_ordersizeqty.strSize
				FROM
				trn_orderdetails 
				Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId  
				Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
				WHERE  
				trn_orderheader.intStatus='1' AND 
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear'"; 
		if($styleNo!=''){
		$sql .= " AND 
				trn_orderdetails.strStyleNo =  '$styleNo'"; 
		}
		if($salesOrderNo!=''){
		$sql .= " AND 
				trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'"; 
		}
		$sql .= " Order by trn_ordersizeqty.strSize ASC";
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['strSize']."\">".$row['strSize']."</option>";
		}
				$response['sizes'] = $html;
		//-----------------		
		$sql="SELECT DISTINCT  
			trn_sampleinfomations_details.intGroundColor,
			mst_colors_ground.strName as bgColor 
			FROM
			trn_orderdetails
			Inner Join trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
			Inner Join trn_sampleinfomations_details ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_details.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_details.intRevNo AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
			left Join mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
			WHERE
			trn_orderdetails.intOrderNo =  '$orderNo' AND
			trn_orderdetails.intOrderYear =  '$orderYear'"; 
			if($styleNo!=''){
			$sql .= " AND 
					trn_orderdetails.strStyleNo =  '$styleNo'"; 
			}
			if($salesOrderNo!=''){
			$sql .= " AND 
					trn_orderdetails.strSalesOrderNo='$salesOrderNo'"; 
			}
			$sql .= " ORDER BY 
					mst_colors_ground.strName ASC ";
		
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['intGroundColor']."\">".$row['bgColor']."</option>";
		}
				$response['color'] = $html;
		//-----------------		
		
		echo json_encode($response);
	}
//-----------------------------------
	else if($requestType=='loadQty')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderYear  = $_REQUEST['orderYear'];
		$salesOrderNo  = $_REQUEST['salesOrderNo'];
		
		$sql = "SELECT
				Sum(trn_orderdetails.intQty) AS qty
				FROM trn_orderdetails 
				WHERE
				trn_orderdetails.intOrderNo='$orderNo' 
				AND trn_orderdetails.intOrderYear='$orderYear'  
"; 
		if($salesOrderNo!=''){
		$sql .= " AND 
				trn_orderdetails.strSalesOrderNo='$salesOrderNo'"; 
		}
		$sql .= " GROUP BY
				trn_orderdetails.intOrderNo,
				trn_orderdetails.intOrderYear"; 
		
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$response['qty'] = $row['qty'];
		
		echo json_encode($response);
	}
//--------------------------------------------------------------------
	else if($requestType=='loadPartDetails')
	{
		$orderYear  = $_REQUEST['orderYear'];
		$orderNo  = $_REQUEST['orderNo'];
		$styleNo  = $_REQUEST['styleNo'];
		$salesOrderNo  = $_REQUEST['salesOrderId'];
		$lineNo  = $_REQUEST['lineNo'];
		$part  = $_REQUEST['part'];
		$size  = $_REQUEST['size'];
		$color  = $_REQUEST['color'];
		$dispNo  = $_REQUEST['dispNo'];
		$dispYear  = $_REQUEST['dispYear'];
		
		
		$sql="SELECT DISTINCT 
			trn_orderdetails.intSalesOrderId,
			trn_orderdetails.strSalesOrderNo,
			trn_orderdetails.intPart,
			mst_part.strName as part,
			trn_sampleinfomations_details.intGroundColor,
			mst_colors_ground.strName as bgColor,
			trn_orderdetails.strLineNo,
			trn_ordersizeqty.strSize,
			trn_ordersizeqty.dblQty 
			FROM
			trn_orderdetails
			Inner Join trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
			Inner Join trn_sampleinfomations_details ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_details.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_details.intRevNo AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName 
			Inner Join ware_fabricdispatchheader ON trn_orderdetails.intOrderNo = ware_fabricdispatchheader.intOrderNo AND trn_orderdetails.intOrderYear = ware_fabricdispatchheader.intOrderYear
			left Join mst_part ON trn_orderdetails.intPart = mst_part.intId
			left Join mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
			Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
			WHERE
			trn_orderdetails.intOrderNo =  '$orderNo' AND
			trn_orderdetails.intOrderYear =  '$orderYear' AND 
			ware_fabricdispatchheader.intStatus='1' AND 
			ware_fabricdispatchheader.intBulkDispatchNo =  '$dispNo' AND 
			ware_fabricdispatchheader.intBulkDispatchNoYear =  '$dispYear' 
			 "; 
			if($styleNo!=''){
			$sql .= " AND 
					trn_orderdetails.strStyleNo='$styleNo'"; 
			}
			if($salesOrderNo!=''){
			$sql .= " AND 
					trn_orderdetails.strSalesOrderNo='$salesOrderNo'"; 
			}
			if($lineNo!=''){
			$sql .= " AND 
					trn_orderdetails.strLineNo='$lineNo'"; 
			}
			if($part!=''){
			$sql .= " AND 
					trn_orderdetails.intPart='$part'"; 
			}
			if($size!=''){
			$sql .= " AND 
					trn_ordersizeqty.strSize='$size'"; 
			}
			if($color!=''){
			$sql .= " AND 
					trn_sampleinfomations_details.intGroundColor='$color'"; 
			}
			$sql .= " ORDER BY 
					trn_orderdetails.strSalesOrderNo ASC, 
					mst_part.strName ASC, 
					mst_colors_ground.strName ASC, 
					trn_orderdetails.strLineNo ASC,
					trn_ordersizeqty.strSize ASC ";

		//	echo $sql;
			
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$data['salesOrderId'] 	= $row['intSalesOrderId'];
			$data['salesOrderNo'] 	= $row['strSalesOrderNo'];
			$data['partId'] 	= $row['intPart'];
			$data['part'] 	= $row['part'];
			$data['groundColorId'] = $row['intGroundColor'];
			$data['groundColor'] = $row['bgColor'];
			$data['lineNo'] 	= $row['strLineNo'];
			$data['size'] = $row['strSize'];
			$data['qty'] = round($row['dblQty'],4);
			$data['FDQty'] = round(loadDamageReturnedQty($location,$orderNo,$orderYear,$row['intSalesOrderId'],$row['strSize']));
			$data['RecvQty'] = round(loadReceivedQty($location,$orderNo,$orderYear,$row['intSalesOrderId'],$row['strSize']));
			$data['confQty'] = round(loadNonstckConfQty($location,$orderNo,$orderYear,$row['intSalesOrderId'],$row['strSize']));
			$data['ExcessQty'] = round(loadExcessQty($orderNo,$orderYear,$row['intSalesOrderId'],$row['intPart'],$row['strSize']));
			$data['exceedPOPermision'] = $exceedPOPermision;
			
			$arrCombo[] = $data;
		}
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);
	}
	else if($requestType=='loadAllComboDetails')
	{
		$year				= $_REQUEST['year'];
		$graphicNo			= $_REQUEST['graphicNo'];
		$styleId			= $_REQUEST['styleId'];
		$customerPONo		= $_REQUEST['customerPONo'];
		$orderNo			= $_REQUEST['orderNo'];
		$salesOrderNo		= $_REQUEST['salesOrderNo'];
		$customerId			= $_REQUEST['customerId'];
		$locationFlag		= 0;
		$companyFlag		= 1;
		echo loadAllComboDetails($year,$graphicNo,$styleId,$customerPONo,$orderNo,$salesOrderNo,$customerId,$locationFlag,$location,$companyFlag,$company);	
	}
	else if($requestType=='loadSizes')
	{
		$orderYear  = $_REQUEST['orderYear'];
		$orderNo  = $_REQUEST['orderNo'];
		$salesOrderNo  = $_REQUEST['salesOrderId'];
		
		    $sql = "SELECT
				trn_ordersizeqty.strSize,
				trn_ordersizeqty.dblQty, 
				trn_orderdetails.dblOverCutPercentage 
				FROM trn_orderdetails 
				Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
				WHERE
				trn_ordersizeqty.intOrderNo =  '$orderNo' AND
				trn_ordersizeqty.intOrderYear =  '$orderYear' AND
				trn_ordersizeqty.intSalesOrderId =  '$salesOrderNo'";

		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$data['fabRcvQty']=loadReceivedQty($location,$orderNo,$orderYear,$salesOrderNo,$row['strSize']);
				$data['size'] = $row['strSize'];
				$data['orderQty'] = $row['dblQty'];
				$data['overCutPercentage'] = $row['dblOverCutPercentage'];
			$arrCombo[] = $data;
		}
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);
	}
	
	
	else if($requestType=='loadSizeCombo')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderYear  = $_REQUEST['orderYear'];
		$salesOrderNo  = $_REQUEST['salesOrderNo'];
		//-----------------		
				$sql = "SELECT DISTINCT
						trn_ordersizeqty.strSize
						FROM
						trn_orderdetails
						Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId  
						Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
						WHERE 
						trn_orderheader.intStatus = '1' AND 
						trn_orderdetails.intOrderNo =  '$orderNo' AND
						trn_orderdetails.intOrderYear =  '$orderYear'";
						if($salesOrderNo!=''){
						$sql.= " AND 
						trn_orderdetails.strSalesOrderNo =  '$salesOrderNo' ";
						}
						 
						$sql.= " ORDER BY trn_ordersizeqty.strSize ASC"; 
				$html = "<option value=\"\"></option>";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
						$html .= "<option value=\"".$row['strSize']."\">".$row['strSize']."</option>";
				}
				$response['combo'] = $html;
		echo json_encode($response);
	}
	else if($requestType=='loadOrderQty')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderYear  = $_REQUEST['orderYear'];
		$salesOrderNo  = $_REQUEST['salesOrderNo'];
		//-----------------		
				$sql = "SELECT
						trn_orderdetails.intQty
						FROM trn_orderdetails
						WHERE
						trn_orderdetails.intOrderNo =  '$orderNo' AND
						trn_orderdetails.intOrderYear =  '$orderYear' AND
						trn_orderdetails.intSalesOrderId =  '$salesOrderNo'"; 
						
				$html = "<option value=\"\"></option>";
				$result = $db->RunQuery($sql);
				$row=mysqli_fetch_array($result);
				$response['orderQty'] = $row['intQty'];
		echo json_encode($response);
	}
	
	else if($requestType=='loadDispatchNos')
	{
		$dispatchYear  = $_REQUEST['dispatchYear'];
		$orderNo  = $_REQUEST['orderNo'];
		$orderYear  = $_REQUEST['orderYear'];
		//-----------------		
		$sql = "SELECT
				ware_fabricdispatchheader.intBulkDispatchNo
				FROM ware_fabricdispatchheader
				WHERE
				ware_fabricdispatchheader.intBulkDispatchNoYear =  '$dispatchYear' AND
				ware_fabricdispatchheader.intStatus =  '1' ";
		if($orderYear!='')
		$sql.=" AND ware_fabricdispatchheader.intOrderYear 	=  '$orderYear'  ";
		if($orderNo!='')
		$sql.=" AND ware_fabricdispatchheader.intOrderNo 	=  '$orderNo'  ";
		$sql.=" ORDER BY
				ware_fabricdispatchheader.intBulkDispatchNo DESC"; 
						
				$html = "<option value=\"\"></option>";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
						$html .= "<option value=\"".$row['intBulkDispatchNo']."\">".$row['intBulkDispatchNo']."</option>";
				}
				$response['combo'] = $html;
		echo json_encode($response);
	}
	
	else if($requestType=='loadReasonCombo')
	{
		$sql = "SELECT DISTINCT 
				mst_reasons.intId, 
				mst_reasons.strReason
				FROM
				mst_reasons
				WHERE
				mst_reasons.intStatus =  '1'   
				ORDER BY mst_reasons.strReason ASC";
				
		//$html = "<option value=\"0\"></option>";
		$val=0;
				$html = "<option value=\"".$val."\"></option>";
		
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['intId']."\">".$row['strReason']."</option>";
		}
		$response['combo'] = $html;
		echo json_encode($response);
	}
//--------------------------------------------------------------


	//-----------------------------------------------------------
function loadDamageReturnedQty($location,$orderNo,$orderYear,$salesOrderId,$size)
{
		global $db;
		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty*-1) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				/*ware_stocktransactions_fabric.intLocationId =  '$location' AND*/
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType in(  'Dispatched_F' ,'Dispatched_P')
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}

	//-----------------------------------------------------------
	function loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size)
{
		global $db;
	  	$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				/*ware_stocktransactions_fabric.intLocationId =  '$location' AND*/
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType =  'CustReturned' 
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//--------------------------------------------------------------
	function loadNonstckConfQty($location,$orderNo,$orderYear,$salesOrderId,$size)
{
		global $db;
	  	$sql = "SELECT
				Sum(ware_fabricreceiveddetails.dblQty) as dblQty 
				FROM
				ware_fabricreceivedheader
				Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
				WHERE
				ware_fabricreceivedheader.intStatus >  1 AND
				ware_fabricreceivedheader.intStatus <=  ware_fabricreceivedheader.intApproveLevels AND
				ware_fabricreceivedheader.intOrderNo =  '$orderNo' AND
				ware_fabricreceivedheader.intOrderYear =  '$orderYear' AND
				ware_fabricreceiveddetails.intSalesOrderId =  '$salesOrderId' AND
				ware_fabricreceiveddetails.strSize =  '$size'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//--------------------------------------------------------------
	function loadExcessQty($orderNo,$orderYear,$salesOrderId,$partId,$size)
{
		global $db;
	       	$sql = "SELECT
				trn_ordersizeqty.dblQty,
				trn_orderdetails.dblOverCutPercentage,
				trn_orderdetails.dblDamagePercentage
				FROM trn_orderdetails 
				Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_orderdetails.intSalesOrderId =  '$salesOrderId' AND
				trn_orderdetails.intPart =  '$partId' AND 
				trn_ordersizeqty.strSize =  '$size' ";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		//$value=$rows['dblQty']*($rows['dblOverCutPercentage']+$rows['dblDamagePercentage'])/100;
		$value=$rows['dblQty']*($rows['dblOverCutPercentage']+1)/100;
		return val($value);
}

function loadAllComboDetails($year,$graphicNo,$styleId,$customerPONo,$orderNo,$salesOrderNo,$customerId,$locationFlag,$location,$companyFlag,$company) 	
{
	$obj = new cls_texttile();
	echo $obj->loadAllSearchComboDetails($year,$graphicNo,$styleId,$customerPONo,$orderNo,$customerId,$locationFlag,$location,$companyFlag,$company);	
}
//------------------------------------------------------------------------------------
?>
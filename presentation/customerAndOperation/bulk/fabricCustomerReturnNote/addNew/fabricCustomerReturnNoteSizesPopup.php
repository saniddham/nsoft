<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];

include $backwardseperator."dataAccess/Connector.php";

$programName='Fabric Customer Return';
$programCode='P0648';

$orderNo  = $_REQUEST['orderNo'];
$orderYear  = $_REQUEST['orderYear'];
$serialNo  = $_REQUEST['serialNo'];
$year  = $_REQUEST['orderYear'];
$styleNo  = $_REQUEST['styleNo'];
$graphicNo  = $_REQUEST['graphicNo'];
$custPONo  = $_REQUEST['custPONo'];
$salesOrderNo  = $_REQUEST['salesOrderNo'];

   $sql = "SELECT DISTINCT 
trn_orderdetails.intSalesOrderId
FROM trn_orderdetails
WHERE
trn_orderdetails.intOrderNo =  '$orderNo' AND
trn_orderdetails.intOrderYear =  '$orderYear'"; 
if($salesOrderNo!=''){
$sql = " AND
trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'";
}

//echo $sql;
$result = $db->RunQuery($sql);

$salesOrderIdString='';
$i=0;
while($row=mysqli_fetch_array($result))
{
	if($i==0){
	$salesOrderIdString = $row['intSalesOrderId'];
	}
	else{
	$salesOrderIdString .= ", ". $row['intSalesOrderId'];
	}
	$i++;
}
//echo $salesOrderIdString;
$editMode=loadEditMode($programCode,$intStatus,$savedStat,$intUser);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Items</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="tblItemsPopup-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>


</head>

<body>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>

    
<form id="frmFabricReceivedNoteSizesPopup" name="frmFabricReceivedNoteSizesPopup" method="post" action="">
<table width="400" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutD" style="width:400">
		  <div class="trans_text"> Add Sizes</div>
		  <table width="400" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="400" border="0">
      <tr>
        <td><table width="400" border="0" cellpadding="0" cellspacing="0" class="tableBorder">
<tr>
            <td width="1%" height="22" class="normalfnt">&nbsp;</td>
            <td width="3%" class="normalfnt">&nbsp;</td>
            <td width="22%" class="normalfnt">Year</td>
            <td width="3%" class="normalfnt">: </td>
            <td width="19%" class="normalfnt"><div id="itemMain"><?php echo $year  ;
 ?></div></td>
            <td width="3%" align="right">&nbsp;</td>
            <td width="3%" align="right"><div id="itemIdMain" style="display:none"><?php echo $itemId; ?></div></td>
            <td width="20%"  class="normalfnt">Style No </td>
            <td width="3%" class="normalfnt">: </td>
            <td width="21%" class="normalfnt"><div id="itemMain"><?php echo $styleNo;
 ?></div></td>
            <td width="1%" class="normalfnt">&nbsp;</td>
            <td width="1%" class="normalfnt">&nbsp;</td>
            </tr>  
            <tr id="rw1">
            <td height="22" class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Graphic No</td>
            <td class="normalfnt">: </td>
            <td class="normalfnt"><div id="balToInvoice"><?php echo $graphicNo;
 ?></div></td>
            <td class="normalfnt"></td>
            <td align="right">&nbsp;</td>
            <td class="normalfnt">Customer PO</td>
            <td class="normalfnt">: </td>
            <td class="normalfnt"><div id="itemMain"><?php echo $custPONo;

 ?></div></td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            </tr>   
<tr id="rw1">
            <td height="23" class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Order No</td>
            <td class="normalfnt">: </td>
            <td class="normalfnt"><?php echo $orderNo  ;
 ?></td>
            <td class="normalfnt"></td>
            <td  align="right">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt"><div id="itemMain"></div></td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            </tr>
            <tr id="rw1">
            <td height="22" class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Sales Order No</td>
            <td class="normalfnt">: </td>
            <td class="normalfnt"><select name="cboSalesOrderNoP" style="width:120px" id="cboSalesOrderNoP" class="">
			<?php
    $sql = "SELECT DISTINCT trn_orderdetails.intSalesOrderId,
							trn_orderdetails.strSalesOrderNo,
							trn_sampleinfomations_details.intGroundColor,
							mst_colors_ground.strName as bgColor,
							mst_part.strName
							FROM
							trn_orderdetails
							Inner Join trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
							Inner Join trn_sampleinfomations_details ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_details.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_details.intRevNo AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
							left Join mst_part ON trn_orderdetails.intPart = mst_part.intId
							left Join mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
							WHERE
							trn_orderdetails.intOrderNo =  '$orderNo' AND
							trn_orderdetails.intOrderYear =  '$orderYear' ";
						if($salesOrderNo!=''){	
						$sql.= " AND
							trn_orderdetails.strSalesOrderNo =  '$salesOrderNo' ";
						}
							
						$sql .= " ORDER BY trn_orderdetails.strSalesOrderNo ASC, mst_part.strName ASC";
                             
                    $html = "<option value=\"\"></option>";
                    $result = $db->RunQuery($sql);
                    while($row=mysqli_fetch_array($result))
                    {
                            $html .= "<option value=\"".$row['intSalesOrderId']."\">".$row['strSalesOrderNo']."/".$row['strName']."/".$row['bgColor']."</option>";
                    }
					echo $html;
            ?>  
            </select></td>
            <td class="normalfnt"></td>
            <td align="right">&nbsp;</td>
            <td class="normalfnt">Order Qty</td>
            <td class="normalfnt">:</td>
            <td class="normalfnt"><div id="divOrderQty"></div></td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            </tr>          
</table></td>
      </tr>
      <tr>
        <td align="center"><div style="width:380px;height:300px;overflow:scroll;" >
          <table width="100%" class="grid" id="tblSizesPopup" align="center" >
            <tr class="gridHeader">
              <td width="11%" height="22" >Delele</td>
              <td width="43%" >Size</td>
              <td width="46%">Qty</td>
              <td width="46%">Received Qty</td>
              </tr>
              <?php 
				 	 $sql = "SELECT
							trn_ordersizeqty.strSize,
							trn_ordersizeqty.dblQty
							FROM trn_ordersizeqty
							WHERE
							trn_ordersizeqty.intOrderNo =  '$orderNo' AND
							trn_ordersizeqty.intOrderYear =  '$year' AND
							trn_ordersizeqty.intSalesOrderId =  '$salesOrderNo'";
					$result = $db->RunQuery($sql);
					$r=0;
					$rcvQty=0;
					while($row=mysqli_fetch_array($result))
					{
						$r++;
						$rcvQty=getRcvQty($orderNo,$year,$salesOrderNo,$row['strSize']);
						if($rcvQty==''){
							$rcvQty=0;
						}
						
			  ?>
            <tr class="normalfnt">
              <td align="center" bgcolor="#FFFFFF"><?php if($rcvQty==0){  ?><img src="../../../../../images/del.png" width="15" height="15" class="delImgPopup" /><?php } ?></td>
              <td align="center" bgcolor="#FFFFFF" id=""><input name="txtSize" type="text" id="txtSize" style="width:60px; text-align:center" value="<?php echo $row['strSize']?>" class="size" /></td>
              <td align="center" bgcolor="#FFFFFF" id=""><input  name="txtSizeQty" type="text" id="txtSizeQty" style="width:60px; text-align:right" value="<?php echo $row['dblQty']?>"  class="validate[required,custom[number],max[<?php echo $Qty ?>],min[<?php echo $rcvQty ?>]] sizeQty" /></td>
              <td align="center" bgcolor="#FFFFFF" id=""><?php echo $rcvQty; ?></td>
              </tr>
              <?php 
					}
					if($r==0){
			  ?>
            <tr class="normalfnt">
              <td align="center" bgcolor="#FFFFFF"><?php if($rcvQty==0){  ?><img src="../../../../../images/del.png" width="15" height="15" class="delImgPopup" /><?php } ?></td>
              <td align="center" bgcolor="#FFFFFF" id=""><input name="txtSize" type="text" id="txtSize" style="width:60px; text-align:center" value="" class="size" /></td>
              <td align="center" bgcolor="#FFFFFF" id=""><input  name="txtSizeQty" type="text" id="txtSizeQty" style="width:60px;  text-align:right" value="0"  class="validate[required,custom[number],max[<?php echo $Qty ?>],min[<?php echo $rcvQty ?>]] sizeQty" /></td>
              <td align="right" bgcolor="#FFFFFF" id=""><?php echo $rcvQty; ?></td>
              </tr>
              <?php
					}
			  ?>
          <tr class="dataRow">
            <td colspan="4" align="left"  bgcolor="#FFFFFF" ><img src="../../../../../images/Tadd.jpg" name="butInsertRowPopup" width="78" height="24" class="mouseover" id="butInsertRowPopup" /></td>
            </tr>
            </table>
          </div></td>
      </tr>
        <td align="center" class="tableBorder_allRound"><img src="../../../../../images/Tsave.jpg" id="butSizeSave" name="butSizeSave" width="92" height="24"  alt="save sizes" class="mouseover" /><img src="../../../../../images/delete.png" width="92" height="24"  alt="add" id="butDelete" name="butDelete" class="mouseover" style="display:none"/><img src="../../../../../planning/img/Tclose.jpg" width="92" height="24" id="butClose2" name="butClose2" class="mouseover"  /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>

</body>
</html>
<?php
	//--------------------------------------------------------------
function loadSizeDetails($orderNo,$orderYear,$salesOrderIdString) 
{
		global $db;
		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				ware_stocktransactions_fabric.intLocationId =  '$location' AND
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType =  'Received'
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
	
}
	//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}

	//--------------------------------------------------------------
?>
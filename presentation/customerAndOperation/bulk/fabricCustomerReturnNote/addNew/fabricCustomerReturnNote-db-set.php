<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	require_once $_SESSION['ROOT_PATH']."class/cls_permisions.php";
	
	//---------check Permission to save recive qty more than PO qty.------------	
	$objpermisionget= new cls_permisions($db);
	$exceedPOPermision 	= $objpermisionget->boolSPermision(5);
	//------------------------------------------------------------------------
	
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo 	 = $_REQUEST['serialNo'];
	$year 		 = $_REQUEST['Year'];
/*	$style  = $_REQUEST['style'];
	$graphicNo 	 = $_REQUEST['graphicNo'];
	$cusromerPO 	 = $_REQUEST['cusromerPO'];
*/	$orderNo 	 = $_REQUEST['orderNo'];
	$orderYear 	 = $_REQUEST['orderYear'];
	$dispNo 	 = $_REQUEST['dispNo'];
	$dispYear 	 = $_REQUEST['dispYear'];
	$AODNo 	 = $_REQUEST['AODNo'];
	$AODDate 	 = $_REQUEST['AODDate'];
	$remarks 	 = $_REQUEST['remarks'];
	//echo $_REQUEST['arr'];
	$arr 		= json_decode($_REQUEST['arr'], true);
	
	$programName='Fabric Customer Return';
	$programCode='P0648';

	$ApproveLevels = (int)getApproveLevel($programName);
	$maxStatus = $ApproveLevels+1;

//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		$savableFlag=1;
        $editMode=0;
		
		if($serialNo==''){
			$serialNo 	= getNextSerialNo();
			$year = date('Y');
			$savedStatus=$maxStatus;
			$savedLevels=$ApproveLevels;
		}
		else{
			$editMode=1;
			$savableFlag=getSaveStatus($serialNo,$year);//check wether already confirmed
			$sql = "SELECT ware_fabriccustreturnheader.intStatus, 
			ware_fabriccustreturnheader.intApproveLevels 
			FROM ware_fabriccustreturnheader 
			WHERE
			ware_fabriccustreturnheader.intFabricCustReturnNo =  '$serialNo' AND
			ware_fabriccustreturnheader.intFabricCustReturnYear =  '$year'";

			$result = $db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			$savedStatus=$row['intStatus'];
			$savedLevels=$row['intApproveLevels'];
		}
		
		    $editPermission=loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
		
			$orderStatus=getOrderStatus($orderNo,$orderYear);//check wether already confirmed
			//--------------------------
			$companyFlag=getPOCompanyValidation('FRN',$location,$company,$orderNo,$orderYear);//check wether already confirmed
			//--------------------------
			$locationFlag=getLocationValidation('FRN',$location,$company,$serialNo,$year);//check wether already confirmed
			//--------------------------
		
		 /*if($companyFlag==1){
			 $rollBackFlag=1;
			 $rollBackMsg="Invalid Save Company";
		 }
		 else */if(( $locationFlag==1) && ($editMode==1)){
			 $rollBackFlag=1;
			 $rollBackMsg="Invalid Edit location";
		 }
		 else if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This Received No is already confirmed.cant edit.";
		 }
		 else if($editPermission==0){
			 $rollBackFlag=1;
			 $rollBackMsg="No Permission to save.";
		 }
		 else if($orderStatus!=1){
			 $rollBackFlag=1;
			 $rollBackMsg="This Order is revised or pending.";
		 }
		else if($editMode==1){//edit existing grn
		$sql = "UPDATE `ware_fabriccustreturnheader` SET intStatus ='$maxStatus', 
												       intApproveLevels ='$ApproveLevels', 
													  intOrderNo ='$orderNo', 
													  intOrderYear ='$orderYear', 
													  intDispatchNo ='$dispNo', 
													  intDispatchYear ='$dispYear', 
													  strAODNo ='$AODNo', 
													  strremarks ='$remarks', 
													  dtmdate ='$AODDate', 
													  intModifiedBy ='$userId', 
													  dtmModifiedDate =now(), 
													  intCompanyId ='$location'  
				WHERE (`intFabricCustReturnNo`='$serialNo') AND (`intFabricCustReturnYear`='$year')";
		$result = $db->RunQuery2($sql);
		}
		else{
			//$sql = "DELETE FROM `ware_fabriccustreturnheader` WHERE (`intFabricCustReturnNo`='$serialNo') AND (`intFabricCustReturnYear`='$year')";
			//$result1 = $db->RunQuery2($sql);

			$sql = "INSERT INTO `ware_fabriccustreturnheader` (`intFabricCustReturnNo`,`intFabricCustReturnYear`,intOrderNo,intOrderYear,intDispatchNo,intDispatchYear,strAODNo,strRemarks,intStatus,intApproveLevels,dtmdate,dtmCreateDate,intCteatedBy,intCompanyId) 
			VALUES ('$serialNo','$year','$orderNo','$orderYear','$dispNo','$dispYear','$AODNo','$remarks','$maxStatus','$ApproveLevels','$AODDate',now(),'$userId','$location')";
			$result = $db->RunQuery2($sql);
		}
		//-----------delete and insert to detail table-----------------------
		if(($result) && ($rollBackFlag!=1)){
			$sql = "DELETE FROM `ware_fabriccustreturndetails` WHERE (`intFabricCustReturnNo`='$serialNo') AND (`intFabricCustReturnYear`='$year')";
			$result2 = $db->RunQuery2($sql);
			$saved=0;
			$toSave=0;
			$rollBackMsg="Maximum Qty for followings...";
			foreach($arr as $arrVal)
			{
				$cutNo = $arrVal['cutNo'];
				$salesOrderId 	 = $arrVal['salesOrderId'];
				$salesOrderNo	 = $arrVal['salesOrderNo'];
				$partId 	 = $arrVal['partId'];
				$part 	 = $arrVal['part'];
				$bgColorId 		 = $arrVal['bgColorId'];
				$bgColor 	 = $arrVal['bgColor'];
				$line 	 = $arrVal['line'];
				$size 		 = $arrVal['size'];
				$qty 		 = round($arrVal['qty'],4);
				$reason 		 = $arrVal['reason'];
				//$place = 'Stores';
				//$type 	 = 'Received';
		
			 	$soStatus		=loadSalesOrderStatus($orderNo,$orderYear,$salesOrderId);
				if($soStatus== -10){
					$rollBackFlag=1;
					$rollBackMsg ="Sales order is already closed";
				}
			 
			 	$fdQty=loadDamageReturnedQty($location,$orderNo,$orderYear,$salesOrderId,$size);
			 	$receivedQty=loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size);
			 	$nonstckConfQty=loadNonstckConfQty($location,$orderNo,$orderYear,$salesOrderId,$size);
				$orderQty=loadOrderQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade);
				$excessQty=loadExcessQty($orderNo,$orderYear,$salesOrderId,$partId,$size);
				
				$recvQtyArr[$orderNo][$orderYear][$salesOrderId][$size] +=$qty;
				$orderQtyArr[$orderNo][$orderYear][$salesOrderId][$size] =$orderQty+$excessQty;
				
				
				$maxRcvQty=$orderQty-$receivedQty-$nonstckConfQty+$excessQty+$fdQty;
				
/*				if($exceedPOPermision==''){//no permision to exceed po qty
					if($recvQtyArr[$orderNo][$orderYear][$salesOrderId][$size]>$orderQtyArr[$orderNo][$orderYear][$salesOrderId][$size]){
						$rollBackFlag=1;
						$rollBackMsg .="<br> "."(".$part."-".$salesOrderNo."-".$size.") =".$maxRcvQty;
					}
					//------check maximum FR Qty--------------------
					else if($qty>$maxRcvQty){
						$rollBackFlag=1;
						$rollBackMsg .="<br> ".$cutNo."-".$part."-".$salesOrderNo."-".$size." =".$maxRcvQty;
					}
				}
*/
				//----------------------------
				if($rollBackFlag!=1){
					
					$sql = "INSERT INTO `ware_fabriccustreturndetails` (`intFabricCustReturnNo`,`intFabricCustReturnYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`intGroundColor`,`strLineNo`,`strSize`,`dblQty`,`intReason`) 
					VALUES ('$serialNo','$year','$cutNo','$salesOrderId','$partId','$bgColorId','$line','$size','$qty','$reason')";
					$result = $db->RunQuery2($sql);
					if($result==1){
					$saved++;
					}
				}
				$toSave++;
		}
		}
		//echo $rollBackFlag;
			//	print_r($recvQtyArr);
			//	print_r($orderQtyArr);
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$confirmationMode=getConfirmationMode($programCode,$userId,$ApproveLevels,$maxStatus);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
	elseif($requestType=='savSizes'){
		
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
/*			$orderNo 	 = $_REQUEST['orderNo'];
			$orderYear 	 = $_REQUEST['orderYear'];	
			$salesOrderId 	 = $_REQUEST['salesOrderId'];
	
			$sql = "DELETE FROM `trn_ordersizeqty` WHERE (`intOrderNo`='$orderNo') AND (`intOrderYear`='$orderYear') AND (`intSalesOrderId`='$salesOrderId')";
			$result2 = $db->RunQuery2($sql);
			$saved=0;
			$toSave=0;
			$rollBackMsg="Minimum Qty for followings...";
			foreach($arr as $arrVal)
			{
				$size 		 = $arrVal['size'];
				$qty 		 = round($arrVal['qty'],4);
				
			 	$receivedQty=loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size);
				$percentage=loadCutPercentage($orderNo,$orderYear,$salesOrderId);
				$minOrderQty=(($received+0.5)*100)/(100+$percentage);
				$minOrderQty=round($minOrderQty);

				//------check maximum FR Qty--------------------
				if($qty<$minOrderQty){
					$rollBackFlag=1;
					$rollBackMsg .="<br> ".$cutNo."-".$part."-".$salesOrderNo."-".$size." =".$minOrderQty;
				}

				//----------------------------
				if($rollBackFlag!=1){
					
					$sql = "INSERT INTO `trn_ordersizeqty` (`intOrderNo`,`intOrderYear`,`intSalesOrderId`,`strSize`,`dblQty`) 
					VALUES ('$orderNo','$orderYear','$salesOrderId','$size','$qty')";
					$result = $db->RunQuery2($sql);
					if($result==1){
					$saved++;
					}
				}
				$toSave++;
		}
		//echo $rollBackFlag;
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$confirmationMode=getConfirmationMode($programCode,$userId,$ApproveLevels,$maxStatus);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
*/		
		$db->CloseConnection();		
		
	}
	//----------------------------------------
	echo json_encode($response);
//----------------------------------------




//----------------------------------------
	function getNextSerialNo()
	{
		global $db;
		global $location;
		$sql = "SELECT
				sys_no.intFabricCustReturnNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$location'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextNo = $row['intFabricCustReturnNo'];
		
		$sql = "UPDATE `sys_no` SET intFabricCustReturnNo=intFabricCustReturnNo+1 WHERE (`intCompanyId`='$location')  ";
		$db->RunQuery2($sql);
		
		return $nextNo;
	}
	//-----------------------------------------------------------
	
	
	//-----------------------------------------------------------
function loadDamageReturnedQty($location,$orderNo,$orderYear,$salesOrderId,$size)
{
		global $db;
		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty*-1) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				/*ware_stocktransactions_fabric.intLocationId =  '$location' AND*/
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType =  'Dispatched_F' 
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}

	//-----------------------------------------------------------
function loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size)
{
		global $db;
		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				ware_stocktransactions_fabric.intLocationId =  '$location' AND
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType =  'Received' 
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//--------------------------------------------------------------
	function loadNonstckConfQty($location,$orderNo,$orderYear,$salesOrderId,$size)
{
		global $db;
	  	$sql = "SELECT
				Sum(ware_fabriccustreturndetails.dblQty) as dblQty 
				FROM
				ware_fabriccustreturnheader
				Inner Join ware_fabriccustreturndetails ON ware_fabriccustreturnheader.intFabricCustReturnNo = ware_fabriccustreturndetails.intFabricCustReturnNo AND ware_fabriccustreturnheader.intFabricCustReturnYear = ware_fabriccustreturndetails.intFabricCustReturnYear
				WHERE
				ware_fabriccustreturnheader.intStatus >  1 AND
				ware_fabriccustreturnheader.intStatus <=  ware_fabriccustreturnheader.intApproveLevels AND
				ware_fabriccustreturnheader.intOrderNo =  '$orderNo' AND
				ware_fabriccustreturnheader.intOrderYear =  '$orderYear' AND
				ware_fabriccustreturndetails.intSalesOrderId =  '$salesOrderId' AND
				ware_fabriccustreturndetails.strSize =  '$size'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//--------------------------------------------------------------
function loadOrderQty($location,$orderNo,$orderYear,$salesOrderNo,$size,$grade)
{
		global $db;
		  $sql = "SELECT
				trn_ordersizeqty.dblQty
				FROM trn_ordersizeqty
				WHERE
				trn_ordersizeqty.intOrderNo =  '$orderNo' AND
				trn_ordersizeqty.intOrderYear =  '$orderYear' AND
				trn_ordersizeqty.intSalesOrderId =  '$salesOrderNo' AND
				trn_ordersizeqty.strSize =  '$size'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//-----------------------------------------------------------
	function loadExcessQty($orderNo,$orderYear,$salesOrderId,$partId,$size)
{
		global $db;
	        	$sql = "SELECT
				trn_ordersizeqty.dblQty,
				trn_orderdetails.dblOverCutPercentage,
				trn_orderdetails.dblDamagePercentage
				FROM trn_orderdetails 
				Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_orderdetails.intSalesOrderId =  '$salesOrderId' AND
				trn_orderdetails.intPart =  '$partId' AND 
				trn_ordersizeqty.strSize =  '$size' ";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		$value=$rows['dblQty']*($rows['dblOverCutPercentage']+1)/100;
		return val($value);
}
//-----------------------------------------------------------
	function loadCutPercentage($orderNo,$orderYear,$salesOrderId)
{
		global $db;
	        	$sql = "SELECT
				trn_orderdetails.dblOverCutPercentage 
				FROM trn_orderdetails 
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_orderdetails.intSalesOrderId =  '$salesOrderId' ";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblOverCutPercentage']);
}
//-----------------------------------------------------------
function getConfirmationMode($programCode,$intUser,$approveLevels,$status){
		global $db;
		$k=$approveLevels+2-$status;
		$sqlp = "SELECT 
		menupermision.intEdit, 
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		if($rowp['int'.$k.'Approval']==1) 
		 $confirmatonMode=1; 
		else
		 $confirmatonMode=0; 

		return $confirmatonMode;
	}
//-----------------------------------------------------------
	function getSaveStatus($serialNo,$year)
	{
		global $db;
		$sql = "SELECT ware_fabriccustreturnheader.intStatus, ware_fabriccustreturnheader.intApproveLevels FROM ware_fabriccustreturnheader WHERE (`intFabricCustReturnNo`='$serialNo') AND (`intFabricCustReturnYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			
		return $editableFlag;
	}
//-----------------------------------------------------------
	function getOrderStatus($serialNo,$year)
	{
		global $db;
		$sql = "SELECT trn_orderheader.intStatus FROM trn_orderheader WHERE (`intOrderNo`='$serialNo') AND (`intOrderYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
 			
		return $status;
	}
//-----------------------------------------------------------
	function getPOCompanyValidation($type,$location,$company,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
		$sql = "SELECT
					mst_locations.intCompanyId
					FROM
					trn_orderheader
					Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
					WHERE
					trn_orderheader.intOrderNo =  '$serialNo' AND
					trn_orderheader.intOrderYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			if($company == $row['intCompanyId'])
			$flag=0;
 			
		return $flag;

}
//---------------------------------------------------
	function getLocationValidation($type,$location,$company,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
	  	$sql = "SELECT
					ware_fabriccustreturnheader.intCompanyId
					FROM
					ware_fabriccustreturnheader
					Inner Join mst_locations ON ware_fabriccustreturnheader.intCompanyId = mst_locations.intId
					WHERE
					ware_fabriccustreturnheader.intFabricCustReturnNo =  '$serialNo' AND
					ware_fabriccustreturnheader.intFabricCustReturnYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			if($location == $row['intCompanyId'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}

function loadSalesOrderStatus($orderNo,$orderYear,$salesOrderId){

	global $db;
	
	$sql 	= "SELECT
				trn_orderdetails.`STATUS`
				FROM `trn_orderdetails`
				WHERE
				trn_orderdetails.intOrderNo = '$orderNo' AND
				trn_orderdetails.intOrderYear = '$orderYear' AND
				trn_orderdetails.intSalesOrderId = '$salesOrderId'
				 ";
	
	$result = $db->RunQuery2($sql);
	$row	= mysqli_fetch_array($result);
	
	return $row['STATUS'];
	
}

//--------------------------------------------------------

?>

 
<?php 
//////////////////////////////////////////////
//Create By:H.B.G Korala
//note://if final confirmation raised,insert into stocktransaction table.
/////////////////////////////////////////////

	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];

	include "{$backwardseperator}dataAccess/Connector.php";
	
	$programName='Fabric Customer Return';
	$programCode='P0648';
	$fabRcvApproveLevel = (int)getApproveLevel($programName);
	
	/////////// parameters /////////////////////////////

	
	$serialNo = $_REQUEST['serialNo'];
	$year = $_REQUEST['year'];
	
	if($_REQUEST['status']=='approve')
	{
		$sql = "UPDATE `ware_fabriccustreturnheader` SET `intStatus`=intStatus-1 WHERE (intFabricCustReturnNo='$serialNo') AND (`intFabricCustReturnYear`='$year') and intStatus not in(0,1)";
		$result = $db->RunQuery($sql);
		
		if($result){
			$sql = "SELECT ware_fabriccustreturnheader.intStatus, 
			ware_fabriccustreturnheader.intApproveLevels , 
			ware_fabriccustreturnheader.intCompanyId as location,
			mst_locations.intCompanyId as company 
			FROM
			ware_fabriccustreturnheader
			Inner Join mst_locations ON ware_fabriccustreturnheader.intCompanyId = mst_locations.intId
			WHERE (intFabricCustReturnNo='$serialNo') AND (`intFabricCustReturnYear`='$year')";
			$results = $db->RunQuery($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			$company = $row['company'];
			$location = $row['location'];
			
			$approveLevel=$savedLevels+1-$status;
			$sqlI = "INSERT INTO `ware_fabriccustreturnheader_approvedby` (`intFabricCustReturnNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate) 
				VALUES ('$serialNo','$year','$approveLevel','$userId',now())";
			$resultI = $db->RunQuery($sqlI);
		//---------------------------------------------------------------------------
		}
		
	}
	else if($_REQUEST['status']=='reject')
	{
		//$grnApproveLevel = (int)getApproveLevel($programName);
			$sql = "SELECT ware_fabriccustreturnheader.intStatus, 
			ware_fabriccustreturnheader.intApproveLevels 
			are_fabricreceivedheader.intCompanyId as location,
			mst_locations.intCompanyId as company 
			FROM
			ware_fabriccustreturnheader
			Inner Join mst_locations ON ware_fabriccustreturnheader.intCompanyId = mst_locations.intId
			WHERE (intFabricCustReturnNo='$serialNo') AND (`intFabricCustReturnYear`='$year')";
			$results = $db->RunQuery($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			$company = $row['company'];
			$location = $row['location'];
		
		$sql = "UPDATE `ware_fabriccustreturnheader` SET `intStatus`=0 WHERE (intFabricCustReturnNo='$serialNo') AND (`intFabricCustReturnYear`='$year')";
		$result = $db->RunQuery($sql);
		
		$sql = "DELETE FROM `ware_fabriccustreturnheader_approvedby` WHERE (`intFabricCustReturnNo`='$serialNo') AND (`intYear`='$year')";
		$result2 = $db->RunQuery($sql);
		
		$sqlI = "INSERT INTO `ware_fabriccustreturnheader_approvedby` (`intFabricCustReturnNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate) 
			VALUES ('$serialNo','$year','0','$userId',now())";
		$resultI = $db->RunQuery($sqlI);
		//-----------------------------------------------------------------------------
	}
//--------------------------------------------------------------
?>
<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
	session_start();
	$backwardseperator = "../../../../../";
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	require_once $_SESSION['ROOT_PATH']."class/cls_permisions.php";
	
	//---------check Permission to save recive qty more than PO qty.------------	
	$objpermisionget= new cls_permisions($db);
	$exceedPOPermision 	= $objpermisionget->boolSPermision(5);
	//------------------------------------------------------------------------
	$programName='Fabric Customer Return';
	$programCode='P0648';
	$fabRcvApproveLevel = (int)getApproveLevel($programName);
	

 if($requestType=='getValidation')
	{
		$errorFlg=0;
		$serialNo  = $_REQUEST['serialNo'];
		$serialNoArray=explode("/",$serialNo);
		
		$orderStatus=getOrderStatus($serialNoArray[0],$serialNoArray[1]);//check wether already confirmed
	
		$sql = "SELECT
		ware_fabriccustreturnheader.intStatus, 
		ware_fabriccustreturnheader.intApproveLevels  
		FROM 
		ware_fabriccustreturnheader 
		WHERE
		ware_fabriccustreturnheader.intFabricCustReturnNo =  '$serialNoArray[0]' AND
		ware_fabriccustreturnheader.intFabricCustReturnYear =  '$serialNoArray[1]'";

		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$row=mysqli_fetch_array($result);
	
	
		$confirmatonMode=loadConfirmatonMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);
		
		if($orderStatus!=1){
			 $errorFlg=1;
			 $msg="This Order is revised or pending.";
		 }
		else if($row['intStatus']==1){// 
			$errorFlg = 1;
			$msg ="Final confirmation of this Received No is already raised"; 
		}
		else if($row['intStatus']==0){// 
			$errorFlg = 1;
			$msg ="This Received No is rejected"; 
		}
		else if($confirmatonMode==0){// 
			$errorFlg = 1;
			$msg ="No Permission to Approve"; 
		}
		
		$result_so=loadDetails($serialNoArray[0],$serialNoArray[1]);
		while($row_so=mysqli_fetch_array($result_so))
		{
			if($row_so['STATUS']== -10){
				$errorFlg=1;
				$msg ="Sales order ".$row_so['strSalesOrderNo']." is already closed";
			}
		}
		
		
		
		if($errorFlg==1){
			$response['status'] = 'fail';
			$response['msg'] 	= $msg;
		}
		else{
			$response['status'] 	= 'pass';
			$response['msg'] 		= '';
		}
			
			echo json_encode($response);
		}
//--------------------------------------------
else if($requestType=='validateRejecton')
	{
		$errorFlg=0;
		$serialNo  = $_REQUEST['serialNo'];
		$serialNoArray=explode("/",$serialNo);
		
		$sql = "SELECT
		ware_fabriccustreturnheader.intStatus, 
		ware_fabriccustreturnheader.intApproveLevels 
		FROM ware_fabriccustreturnheader
		WHERE
		ware_fabriccustreturnheader.intFabricCustReturnNo =  '$serialNoArray[0]' AND
		ware_fabriccustreturnheader.intFabricCustReturnYear =  '$serialNoArray[1]'";

		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$msg =""; 
		$row=mysqli_fetch_array($result);
		
		$rejectionMode=loadRejectionMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);
		
		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this Received No is already raised.So cant reject"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This Received No is already Rejected"; 
		}
		else if($rejectionMode==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="No Permission to reject this Received No"; 
		}
		else{//confirmation has been raised
			$errorFlg = 0;
			
		}

	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);

	}
	
	

	//-----------------------------------------------------------
//--------------------------------------------------------------
function loadOrderQty($location,$orderNo,$orderYear,$salesOrderNo,$size,$grade)
{
		global $db;
		  $sql = "SELECT
				trn_ordersizeqty.dblQty
				FROM trn_ordersizeqty
				WHERE
				trn_ordersizeqty.intOrderNo =  '$orderNo' AND
				trn_ordersizeqty.intOrderYear =  '$orderYear' AND
				trn_ordersizeqty.intSalesOrderId =  '$salesOrderNo' AND
				trn_ordersizeqty.strSize =  '$size'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//--------------------------------------------------------------
	function loadExcessQty($orderNo,$orderYear,$salesOrderId,$partId,$size)
{
		global $db;
	       	$sql = "SELECT
				trn_ordersizeqty.dblQty,
				trn_orderdetails.dblOverCutPercentage,
				trn_orderdetails.dblDamagePercentage
				FROM trn_orderdetails 
				Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_orderdetails.intSalesOrderId =  '$salesOrderId' AND
				trn_orderdetails.intPart =  '$partId' AND 
				trn_ordersizeqty.strSize =  '$size' ";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		$value=$rows['dblQty']*($rows['dblOverCutPercentage']+1)/100;
		return val($value);
}
//-----------------------------------------------------------
	function getOrderStatus($serialNo,$year)
	{
		global $db;
	  	$sql = "SELECT
trn_orderheader.intStatus
FROM
ware_fabriccustreturnheader
Inner Join trn_orderheader ON ware_fabriccustreturnheader.intOrderNo = trn_orderheader.intOrderNo AND ware_fabriccustreturnheader.intOrderYear = trn_orderheader.intOrderYear
WHERE
ware_fabriccustreturnheader.intFabricCustReturnNo =  '$serialNo' AND
ware_fabriccustreturnheader.intFabricCustReturnYear =  '$year'";
			$results = $db->RunQuery($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
 			
		return $status;
	}
//-----------------------------------------------------------
	function getLocationValidation($type,$location,$company,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
		$sql = "SELECT
					mst_locations.intCompanyId
					FROM
					trn_orderheader
					Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
					WHERE
					trn_orderheader.intOrderNo =  '$serialNo' AND
					trn_orderheader.intOrderYear =  '$year'";
			$results = $db->RunQuery($sql);
			$row = mysqli_fetch_array($results);
			if($company == $row['intCompanyId'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
function loadDetails($serialNo,$year){
		global $db; 
	  	$sql = "SELECT
				trn_orderdetails.strSalesOrderNo, 
				trn_orderdetails.intQty as orderQty, 
				ware_fabriccustreturndetails.strCutNo,
				mst_part.strName as part,
				mst_colors_ground.strName as bgcolor,
				ware_fabriccustreturndetails.strLineNo,
				ware_fabriccustreturndetails.strSize,
				ware_fabriccustreturndetails.dblQty,
				ware_fabriccustreturndetails.intSalesOrderId,
				ware_fabriccustreturndetails.intPart,
				ware_fabriccustreturndetails.intGroundColor ,
				ware_fabriccustreturndetails.intReason , 
				trn_orderdetails.STATUS 
				FROM
				trn_orderdetails
				Inner Join ware_fabriccustreturnheader ON ware_fabriccustreturnheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabriccustreturnheader.intOrderYear = trn_orderdetails.intOrderYear
				Inner Join ware_fabriccustreturndetails ON ware_fabriccustreturnheader.intFabricCustReturnNo = ware_fabriccustreturndetails.intFabricCustReturnNo AND ware_fabriccustreturnheader.intFabricCustReturnYear = ware_fabriccustreturndetails.intFabricCustReturnYear AND ware_fabriccustreturndetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
				left Join mst_part ON ware_fabriccustreturndetails.intPart = mst_part.intId
				left Join mst_colors_ground ON ware_fabriccustreturndetails.intGroundColor = mst_colors_ground.intId
				WHERE
				ware_fabriccustreturnheader.intFabricCustReturnNo =  '$serialNo' AND
				ware_fabriccustreturnheader.intFabricCustReturnYear =  '$year' 
				ORDER BY 
				ware_fabriccustreturndetails.strCutNo ASC, 
				trn_orderdetails.strSalesOrderNo ASC, 
				mst_part.strName ASC, 
				mst_colors_ground.strName ASC, 
				ware_fabriccustreturndetails.strLineNo ASC,
				ware_fabriccustreturndetails.strSize ASC 
				";

			return $result = $db->RunQuery($sql);
	}

//--------------------------------------------------------

?>

<?php 
	session_start();
	$companyId  = $_SESSION["CompanyID"];
	$mainUrl	= $_SESSION['MAIN_URL'];

	$sql	= " SELECT
				(
				SELECT MAX(AP.dtApprovedDate)
				FROM trn_orderheader_approvedby AS AP
				WHERE
				AP.intOrderNo=trn_orderheader_approvedby.intOrderNo AND
				AP.intYear=trn_orderheader_approvedby.intYear
				) AS LAST_REC_DATE,
				
				 	(
				SELECT TIME(MAX(AP.dtApprovedDate))
				FROM trn_orderheader_approvedby AS AP
				WHERE
				AP.intOrderNo=trn_orderheader_approvedby.intOrderNo AND
				AP.intYear=trn_orderheader_approvedby.intYear
				) AS LAST_REC_TIME,
				 
				(
				SELECT MAX(AP1.dtApprovedDate) AS MX
				FROM trn_orderheader_approvedby AS AP1
				WHERE
				AP1.intOrderNo=trn_orderheader_approvedby.intOrderNo AND
				AP1.intYear=trn_orderheader_approvedby.intYear
				AND AP1.dtApprovedDate < LAST_REC_DATE
				ORDER BY AP1.dtApprovedDate DESC LIMIT 1
				) AS PRE_REC_DATE,
				
				
				(
				SELECT  
				AP2.intApproveLevelNo
				FROM trn_orderheader_approvedby AS AP2
				WHERE
				AP2.intOrderNo=trn_orderheader_approvedby.intOrderNo AND
				AP2.intYear=trn_orderheader_approvedby.intYear
				AND AP2.dtApprovedDate = PRE_REC_DATE
				ORDER BY AP2.dtApprovedDate DESC LIMIT 1
				) AS PRE_APP_LEVEL,
				
				
				trn_orderheader_approvedby.intApproveLevelNo,
				trn_orderheader_approvedby.intOrderNo,
				trn_orderheader_approvedby.intYear,
				trn_orderheader_approvedby.intApproveUser,
				trn_orderheader_approvedby.dtApprovedDate,
				trn_orderheader_approvedby.intStatus
				FROM `trn_orderheader_approvedby`
				HAVING (intApproveLevelNo=0 /*OR (intApproveLevelNo =1 AND PRE_APP_LEVEL =0)*/)
				AND (LAST_REC_DATE= dtApprovedDate) 
				AND DATE(LAST_REC_DATE) >= '2015-08-12'
				";
	$result	= $db->RunQuery($sql);
	while($row	= mysqli_fetch_array($result))
	{
		$orderNo		= $row['intOrderNo'];
		$orderYear		= $row['intYear'];
		$con_orderNo	= $orderNo.'/'.$orderYear;
		$orderArr		= NULL;
		$rejectDate		= strtotime($row['LAST_REC_DATE']);
		$currentDate	= strtotime(date('Y-m-d H:i:s'));
		$diffInHrs		= (($currentDate - $rejectDate)/3600);
		$flag_weekend	=0;
		
		//-----------------------------------------------------------------------
		$start = date_create($row['LAST_REC_DATE']);
		$end 	= date_create(date('Y-m-d H:i:s'));
		
		// otherwise the  end date is excluded (bug?)
		$end->modify('+1 day');
		$interval = $end->diff($start);
		// total days
		$days = $interval->days;
		// create an iterateable period of date (P1D equates to 1 day)
		$period = new DatePeriod($start, new DateInterval('P1D'), $end);
		// best stored as array, so you can add more than one
		//$holidays = array('2012-09-07');
		
		foreach($period as $dt) {
			$curr = $dt->format('D');
		
			// for the updated question
			//if (in_array($dt->format('Y-m-d'), $holidays)) {
			//   $days--;
			//}
		
			// substract if Saturday or Sunday
			if ($curr == 'Sat' || $curr == 'Sun') {
				$flag_weekend=1;
			}
		}		  		
		//--------------------------------------------------------------------------
		
		if($flag_weekend==1)
			$hrs_margin = 72;
		else
			$hrs_margin = 48; 
		
		if($diffInHrs > $hrs_margin)
		{
			$sql_user		= " SELECT
								GROUP_CONCAT(DISTINCT(trn_orderheader.intCreator)) AS userId,
								sys_users.strUserName,
								trn_orderheader.intLocationId
								FROM
								trn_orderheader
								INNER JOIN sys_users ON trn_orderheader.intCreator = sys_users.intUserId
								WHERE
								trn_orderheader.intOrderNo = '$orderNo' AND
								trn_orderheader.intOrderYear = '$orderYear' 
								AND trn_orderheader.intCreator <> 56
								";
									
			$result_user	= $db->RunQuery($sql_user);
			$row_user	= mysqli_fetch_array($result_user);
			$poUserId	= $row_user['userId'];
			$location	= $row_user['intLocationId'];
			
			$sql_emailEventUser	=  "SELECT
									sys_mail_eventusers.intUserId
									FROM
									sys_mail_eventusers
									WHERE
									sys_mail_eventusers.intMailEventId = '1020' 
									AND 
									sys_mail_eventusers.intCompanyId = '$location'
									";
			$resultMailEvent	= $db->RunQuery($sql_emailEventUser);
			$mailEventUserId	= '';
			while($row_eventUser	= mysqli_fetch_array($resultMailEvent))
			{
				$mailEventUserId    .= $row_eventUser['intUserId'].',';	
			}
			$trimUserId		= rtrim($mailEventUserId,',');
			
			$userName		 = $row_user['strUserName'];
			$strEmailAddress = $obj_comm->getEmailListAll($poUserId);
			
			$url			 = $mainUrl."?q=896&orderNo=$orderNo&orderYear=$orderYear&approveMode=1";
			ob_start();
		
			include "poUserInactiveTemplate.php";	
		
			$body = ob_get_clean();
	
			echo $body;
	
			$FROM_NAME			= "NSOFT - SCREENLINE HOLDINGS";
			$FROM_EMAIL			= "";
			 
			$nowDate = date('Y-m-d');
			$mailHeader= "INACTIVE PO RAISED USER";
			//Original
		
			$mail_TO	= $strEmailAddress.','.$obj_comm->getEmailListAll($trimUserId);
			$mail_CC	= '';
			$mail_BCC	= '';
		
			insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
			
			$sql_inactive	 = "UPDATE sys_users 
								SET
								intStatus = '0' 
								
								WHERE
								intUserId = '$poUserId' ";
			$result_inactive = $db->RunQuery($sql_inactive);
			
		}
		
	}
?>
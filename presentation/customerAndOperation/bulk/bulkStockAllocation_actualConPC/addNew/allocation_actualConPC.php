<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$mainPath 				= $_SESSION['mainPath'];
$sessionUserId 			= $_SESSION['userId'];
$thisFilePath 			=  $_SERVER['PHP_SELF'];
$locationId				= $_SESSION["CompanyID"];
$company				= $_SESSION['headCompanyId'];

###########################
### get loading values ####
###########################
$orderYear 	= $_REQUEST['cboOrderYear'];
$orderNo 	= $_REQUEST['cboOrderNo'];
$salesOrderId 	= $_REQUEST['cboSalesOrderNo'];
$poNo = $_REQUEST['cboPONo'];
$customer = $_REQUEST['cboCustomer'];
$x_styleNo 	= $_REQUEST['cboStyle'];
$x_graphicNo 	= $_REQUEST['cboGraphicNo'];

if($orderNo !=''){
	$sql	="SELECT
	trn_orderdetails.strSalesOrderNo,
	trn_orderdetails.intSalesOrderId,
	trn_orderdetails.strLineNo,
	trn_orderdetails.strStyleNo,
	trn_orderdetails.strGraphicNo,
	trn_orderdetails.intOrderNo,
	trn_orderdetails.intOrderYear,
	trn_orderheader.strCustomerPoNo,
trn_orderheader.intCustomer 
	FROM
	trn_orderdetails 
	INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear

	WHERE
	trn_orderdetails.intOrderNo = '$orderNo' AND
	trn_orderdetails.intOrderYear = '$orderYear' ";
	if($salesOrderId != ''){
	$sql .="  AND
	trn_orderdetails.intSalesOrderId = '$salesOrderId' ";
	if($x_styleNo != ''){
	$sql .="  AND
	trn_orderdetails.strStyleNo = '$x_styleNo' ";
	}
	if($x_graphicNo != ''){
	$sql .="  AND
	trn_orderdetails.strGraphicNo = '$x_graphicNo' ";
	}
	}
	
	
	//echo $sql;
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	$x_styleNo 		= $row['strStyleNo'];
	$x_graphicNo 	= $row['strGraphicNo'];
	$poNo 			= $_REQUEST['strCustomerPoNo'];
	$customer 		= $_REQUEST['intCustomer'];

}

if($orderYear==''){
	$orderYear=date('Y');
}
   $sql = "SELECT
trn_orderheader.strCustomerPoNo,
trn_orderheader.intCustomer
FROM trn_orderheader
WHERE
trn_orderheader.intOrderNo =  '$orderNo' AND 
trn_orderheader.intOrderNo > 0 AND 
trn_orderheader.intOrderYear =  '$orderYear'"; 
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);

$x_poNo = $row['strCustomerPoNo'];
$x_custId = $row['intCustomer'];


################ 1 ACTUAL CALCULATION ####################################
	  	$sql = "SELECT
					ware_bulkallocation_header.intFirstDayPlanGarment,
					ware_bulkallocation_header.intFirstDayActualGarment,
					ware_bulkallocation_header.int25PlanGarment,
					ware_bulkallocation_header.int25ActualGarment,
					ware_bulkallocation_header.int75PlanGarment,
					ware_bulkallocation_header.int75ActualGarment,
					ware_bulkallocation_header.intStage
				FROM `ware_bulkallocation_header`
				WHERE
					ware_bulkallocation_header.intOrderNo =  '$orderNo' AND 
					ware_bulkallocation_header.intOrderYear =  '$orderYear' AND
					ware_bulkallocation_header.intSalesOrderId =  '$salesOrderId'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$plan1Qty = $row['intFirstDayPlanGarment'];
			$plan2Qty = $row['int25PlanGarment'];
			$plan3Qty = $row['int75PlanGarment'];
			
			$actual1Qty = $row['intFirstDayActualGarment'];
			$actual2Qty = $row['int25ActualGarment'];
			$actual3Qty = $row['int75ActualGarment'];
			
			$intStage = $row['intStage'];
		}
if($intStage==1)
{
	$sql = "UPDATE `ware_bulkallocation_details` AS A 
			SET `dbl1ActualQty`=(
			
			SELECT
			Sum(ware_stocktransactions.dblQty) AS issueQty
			FROM ware_stocktransactions 
			WHERE
			ware_stocktransactions.intLocationId 	=  A.intLocationId AND
			ware_stocktransactions.intOrderNo 		=  A.intOrderNo AND
			ware_stocktransactions.intOrderYear 	=  A.intOrderYear AND
			ware_stocktransactions.intSalesOrderId 	=  A.intSalesOrderId AND
			(ware_stocktransactions.intItemId=A.intItemId)  AND
			(ware_stocktransactions.strType =  'ISSUE' OR ware_stocktransactions.strType =  'RETSTORES') 
			
			) * -1
			WHERE (A.intLocationId='$locationId') AND (A.intOrderNo='$orderNo') 
			AND (A.intOrderYear='$orderYear') AND (A.intSalesOrderId='$salesOrderId') ";
	$db->RunQuery($sql);
	
}
if($intStage==2)
{
	$sql = "UPDATE `ware_bulkallocation_details` AS A 
			SET `dbl25ActualQty`=((
			
			SELECT
			Sum(ware_stocktransactions.dblQty) AS issueQty
			FROM ware_stocktransactions 
			WHERE
			ware_stocktransactions.intLocationId 	=  A.intLocationId AND
			ware_stocktransactions.intOrderNo 		=  A.intOrderNo AND
			ware_stocktransactions.intOrderYear 	=  A.intOrderYear AND
			ware_stocktransactions.intSalesOrderId 	=  A.intSalesOrderId AND
			(ware_stocktransactions.intItemId=A.intItemId)  AND
			(ware_stocktransactions.strType =  'ISSUE' OR ware_stocktransactions.strType =  'RETSTORES') 
			
			) * -1)-dbl1ActualQty
			WHERE (A.intLocationId='$locationId') AND (A.intOrderNo='$orderNo') 
			AND (A.intOrderYear='$orderYear') AND (A.intSalesOrderId='$salesOrderId') 
			";
	$db->RunQuery($sql);
	
}
if($intStage==3)
{
	$sql = "UPDATE `ware_bulkallocation_details` AS A 
			SET `dbl75ActualQty`=((
			
			SELECT
			Sum(ware_stocktransactions.dblQty) AS issueQty
			FROM ware_stocktransactions 
			WHERE
			ware_stocktransactions.intLocationId 	=  A.intLocationId AND
			ware_stocktransactions.intOrderNo 		=  A.intOrderNo AND
			ware_stocktransactions.intOrderYear 	=  A.intOrderYear AND
			ware_stocktransactions.intSalesOrderId 	=  A.intSalesOrderId AND
			(ware_stocktransactions.intItemId=A.intItemId)  AND
			(ware_stocktransactions.strType =  'ISSUE' OR ware_stocktransactions.strType =  'RETSTORES') 
			
			) * -1)-dbl1ActualQty-dbl25ActualQty
			WHERE (A.intLocationId='$locationId') AND (A.intOrderNo='$orderNo') 
			AND (A.intOrderYear='$orderYear') AND (A.intSalesOrderId='$salesOrderId') 
			";
	$db->RunQuery($sql);
	
}
##########################################################################
?>
 <!-- <script type="text/javascript" src="presentation/customerAndOperation/bulk/bulkStockAllocation_actualConPC/addNew/allocation_actualConPC-js.js"></script>
-->
<script>
//global variables
var rowIndex = 0;
var pubStage = 0;
var graphic  = '';
var style	 = '';
</script>

 
<form id="frmActualConPC" name="frmActualConPC" method="post">
 <div align="center">
		<div class="trans_layoutL" style="width:800">
		  <div class="trans_text">First Day/Second Day Actual Consumptions</div>
		  <table width="80%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
    <tr><td><table  bgcolor="#E8FED8" class="tableBorder_allRound"  width="100%" border="0" cellpadding="0" cellspacing="0"  >
            <tr>
              <td height="22" class="normalfnt">&nbsp;Year</td>
              <td class="normalfnt"><select name="cboOrderYear" id="cboOrderYear" style="width:70px"<?php /*?> class="search"<?php */?>>
                <?php
				
					$sql = "SELECT DISTINCT
							trn_orderheader.intOrderYear
							FROM trn_orderheader
							ORDER BY
							trn_orderheader.intOrderYear DESC";
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						if(($i==0) &&($orderYear==''))
						$orderYear = $row['intOrderYear'];
						
						
						if($row['intOrderYear']==$orderYear)
						echo "<option value=\"".$row['intOrderYear']."\" selected=\"selected\">".$row['intOrderYear']."</option>";	
						else
						echo "<option value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";	
						$i++;
					}
				?>
                </select></td>
              <td class="normalfnt">Style No</td>
              <td class="normalfnt"><select name="cboStyle" id="cboStyle" style="width:130px" class="search">
                <?php
                $html1="<option value=\"\"></option>";
				$html='';
					$sql = "SELECT DISTINCT
							trn_orderdetails.strStyleNo
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
							left Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
							WHERE
							trn_orderheader.intStatus =  '1'   AND 
							trn_orderdetails.intOrderYear='$orderYear'  AND trn_orderheader.intOrderNo>=0  ";
					if($orderNo>0){		
					$sql .= " AND trn_orderdetails.intOrderNo='$orderNo' ";	
					}
					$sql .= " 
							   
							ORDER BY trn_orderdetails.strStyleNo ASC";
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						$i++;
						if($row['strStyleNo']==$x_styleNo)
						$html .= "<option value=\"".$row['strStyleNo']."\" selected=\"selected\">".$row['strStyleNo']."</option>";	
						else
						$html .= "<option value=\"".$row['strStyleNo']."\">".$row['strStyleNo']."</option>";	
						if(($i==1) && ($x_styleNo=='')){
							$x_styleNoT=$row['strStyleNo'];
						}
					}
					
					if(($orderNo!='') && ($x_styleNo=='')){
					$x_styleNo=$x_styleNoT;
					}
					
					if($orderNo==''){
						$html=$html1.$html;
					}
					else if($i>=1){
						$html=$html.$html1;
					}
					else if($i==0){
						$html=$html1;
					}
					echo $html;
				?></select></td>
              <td class="normalfnt">Graphic No</td>
              <td align="right" class="normalfnt"><select name="cboGraphicNo" id="cboGraphicNo" style="width:120px" class="search">
                <?php
                $html1="<option value=\"\"></option>";
				$html='';
					$sql = "SELECT DISTINCT
							trn_orderdetails.strGraphicNo
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
							left Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId

							WHERE
							trn_orderheader.intStatus =  '1'    AND 
							trn_orderdetails.intOrderYear='$orderYear'  AND trn_orderheader.intOrderNo>0  ";
					if($orderNo>0){		
					$sql .= " AND trn_orderdetails.intOrderNo='$orderNo' ";	
					}
					$sql .= " 
							 
							ORDER BY trn_orderdetails.strGraphicNo ASC";
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						$i++;
						if($row['strGraphicNo']==$x_graphicNo)
						$html .= "<option value=\"".$row['strGraphicNo']."\" selected=\"selected\">".$row['strGraphicNo']."</option>";	
						else
						$html .= "<option value=\"".$row['strGraphicNo']."\">".$row['strGraphicNo']."</option>";	
						if(($i==1) && ($x_graphicNo=='')){
							$x_graphicNoT=$row['strGraphicNo'];
						}
					}
					
					if(($orderNo!='') && ($x_graphicNo=='')){
					$x_graphicNo=$x_graphicNoT;
					}
					
					if($orderNo==''){
						$html=$html1.$html;
					}
					else if($i>=1){
						$html=$html.$html1;
					}
					else if($i==0){
						$html=$html1;
					}
					echo $html;
				?></select></td> </tr>
<tr>
              <td height="22" class="normalfnt">Customer PO</td>
              <td class="normalfnt"><select name="cboPONo" id="cboPONo" style="width:130px" class="search">
                <option value=""></option>
                <?php
					$sql = "SELECT DISTINCT
							trn_orderheader.strCustomerPoNo
							FROM trn_orderheader  
							left Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId

							where trn_orderheader.intStatus=1    AND 
							trn_orderheader.intOrderYear='$orderYear' AND trn_orderheader.intOrderNo>0  
							AND trn_orderheader.strCustomerPoNo <> '' 
							ORDER BY
							trn_orderheader.strCustomerPoNo ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if(($row['strCustomerPoNo']==$x_poNo) && ($orderNo!=''))
						echo "<option value=\"".$row['strCustomerPoNo']."\" selected=\"selected\">".$row['strCustomerPoNo']."</option>";	
						else
						echo "<option value=\"".$row['strCustomerPoNo']."\">".$row['strCustomerPoNo']."</option>";	
					}
				?>
                </select><?php //echo $sql; ?></td>
              <td class="normalfnt">Customer</td>
              <td colspan="4" class="normalfnt"><select style="width:295px" name="cboCustomer" id="cboCustomer"   class="validate[required] search" >
                <option value=""></option>
                <?php
					$sql = "SELECT DISTINCT 
							trn_orderheader.intCustomer,
							mst_customer.strName
							FROM
							trn_orderheader
							Inner Join mst_customer ON trn_orderheader.intCustomer = mst_customer.intId  
							left Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
							where  
							trn_orderheader.intOrderYear='$orderYear'  ";
					if($orderNo!=''){
					$sql .= " AND trn_orderheader.intOrderNo='$orderNo' ";	
					}
					$sql .= " AND trn_orderheader.intOrderNo>0  
							ORDER BY mst_customer.strName ASC";
							$pp=$sql;
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intCustomer']==$x_custId)
						echo "<option value=\"".$row['intCustomer']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intCustomer']."\">".$row['strName']."</option>";	
					}
				?>
                </select></td>
            </tr> 
<tr>
              <td height="22" class="normalfnt">Order No</td>
              <td class="normalfnt"><select name="cboOrderNo" id="cboOrderNo" style="width:130px" >
                <option value=""></option>
                <?php

					$sql = "SELECT DISTINCT trn_orderheader.intOrderNo 
					FROM trn_orderdetails 
					Inner Join trn_orderheader 
					ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear 
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo 
					AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo 
					left Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
					where trn_orderheader.intStatus = 1 
					/* (comment - to view all )AND mst_locations.intCompanyId =  '$company' */ ";
					if($orderYear!='')
					$sql.=" AND trn_orderheader.intOrderYear 	=  '$orderYear'  ";
					
				//	if($orderNo!=''){
					if($x_graphicNo!='')
					$sql.=" AND trn_orderdetails.strGraphicNo =  '$x_graphicNo'  ";
					if($x_styleNo!='')
					$sql.=" AND trn_orderdetails.strStyleNo 		=  '$x_styleNo'  ";
					if($x_poNo!='')
					//$sql.=" AND trn_orderheader.strCustomerPoNo =  '$x_poNo' ";
					if($salesOrderId!=''){		
					//$sql .= " AND trn_orderdetails.strSalesOrderNo =  '$salesOrderId'";		
					}
				//	}
					$sql .= "ORDER BY trn_orderheader.intOrderYear DESC, trn_orderheader.intOrderNo DESC";
					//   echo $sql;
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intOrderNo']==$orderNo)
						echo "<option value=\"".$row['intOrderNo']."\" selected=\"selected\">".$row['intOrderNo']."</option>";	
						else
						echo "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";	
					}
				?>
              </select><?php // echo $sql; ?></td>
              <td class="normalfnt">Sales Order No</td>
              <td colspan="4" class="normalfnt"><select name="cboSalesOrderNo" id="cboSalesOrderNo" style="width:130px" >
                <option value=""></option>
                <?php
				///////////////////////////
				///////////////////////////////
			    	$sql = "SELECT
							trn_orderdetails.strGraphicNo,
							trn_orderdetails.intSampleNo,
							trn_orderdetails.intSampleYear,
							trn_orderdetails.strCombo,
							trn_orderdetails.strPrintName,
							trn_orderdetails.intRevisionNo,
							trn_orderdetails.intQty,
							trn_orderdetails.dblPrice,
							trn_orderdetails.dtPSD,
							trn_orderdetails.dtDeliveryDate 
						FROM trn_orderdetails 
						WHERE
							trn_orderdetails.intOrderNo 		=  '$orderNo' AND
							trn_orderdetails.intOrderYear 		=  '$orderYear' AND
							trn_orderdetails.intSalesOrderId 	=  '$salesOrderId'
						";
				$result = $db->RunQuery($sql);
				$Mrow=mysqli_fetch_array($result);
				///////////////////////////////
				///////////////////////////
				 	$sql = "SELECT DISTINCT
					trn_orderdetails.strSalesOrderNo,intSalesOrderId, 
					mst_part.strName as part  
					FROM trn_orderdetails 
						Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
						Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
						left Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId

						WHERE  trn_orderheader.intStatus = 1 
							/* AND mst_locations.intCompanyId =  '$company' */ ";
							 
					if($orderYear!='')
					$sql.=" AND trn_orderheader.intOrderYear 	=  '$orderYear'  ";
					if($x_graphicNo!='')
					$sql.=" AND trn_orderdetails.strGraphicNo =  '$x_graphicNo'  ";
					if($x_styleNo!='')
					$sql.=" AND trn_orderdetails.strStyleNo 		=  '$x_styleNo'  ";
					if($x_poNo!='')
					//$sql.=" AND trn_orderheader.strCustomerPoNo =  '$x_poNo' ";
					if($orderNo!=''){		
					$sql .= " AND trn_orderheader.intOrderNo =  '$orderNo'";		
					}
					$sql .= "
							ORDER BY trn_orderdetails.strSalesOrderNo ASC";
							
					//echo $sql;	
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$no = $row['strSalesOrderNo']."/".$row['part'];
						$salesId = $row['intSalesOrderId'];
						if($salesId==$salesOrderId)
							echo "<option selected=\"selected\" value=\"$salesId\">$no</option>";
						else
							echo "<option value=\"$salesId\">$no</option>";
					}
				?>
              </select><?php  //echo $sql; ?></td>
            </tr>            </table></td></tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <?php
		  	#########################
			## get header details ###
			#########################
			$sql = "SELECT DISTINCT
						trn_sampleinfomations_printsize.intWidth,
						trn_sampleinfomations_printsize.intHeight,
						trn_sampleinfomations.strStyleNo,
						mst_part.strName
					FROM
						trn_sampleinfomations_printsize
						Inner Join mst_part ON mst_part.intId = trn_sampleinfomations_printsize.intPart
						Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_printsize.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_printsize.intSampleYear
					WHERE
						trn_sampleinfomations_printsize.strPrintName  =  '$x_print' AND
						trn_sampleinfomations_printsize.intSampleNo   =  '$x_sampleNo' AND
						trn_sampleinfomations_printsize.intSampleYear =  '$x_sampleYear' AND
						trn_sampleinfomations_printsize.intRevisionNo =  '$x_revNo'
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					$styleNo 		= $row['strStyleNo'];
					$print_width 	= $row['intWidth'];
					$print_height 	= $row['intHeight'];
					$partName 		= $row['strName'];
				}
				
		  ?>
          <tr>
            <td height="155"  class="normalfnt"><div style="height:155px;vertical-align:middle"  class="tableBorder_allRound">
              <table class="rounded" bgcolor="" width="92%" border="0" align="center" cellpadding="0" cellspacing="1">
                <tr>
                  <td width="25%" bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
                  <td width="23%" bgcolor="#FFFFFF" class="normalfntMid">&nbsp;</td>
                  <td width="26%" bgcolor="#FFFFFF" class="normalfntMid">&nbsp;</td>
                  <td width="26%" bgcolor="#FFFFFF" class="normalfntMid">&nbsp;</td>
                  </tr>
                <tr>
                  <td bgcolor="#D1F9FC" class="txtNumber">Sample Year</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['intSampleYear'];?></td>
                  <td bgcolor="#D1F9FC" class="txtNumber">Sample No</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['intSampleNo'];?></td>
                  </tr>
                <tr>
                  <td bgcolor="#D1F9FC" class="txtNumber">Graphic No</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['strGraphicNo'];?></td>
                  <td bgcolor="#D1F9FC" class="txtNumber">Rev No</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['intRevisionNo'];?></td>
                  </tr>
                <tr>
                  <td bgcolor="#D1F9FC" class="txtNumber">Combo</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['strCombo'];?></td>
                  <td bgcolor="#D1F9FC" class="txtNumber">Print</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['strPrintName'];?></td>
                  </tr>
                <tr>
                  <td bgcolor="#D1F9FC" class="txtNumber">Order Qty</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<div id="divTotGarmQty"><?php  echo $Mrow['intQty'];?></div></td>
                  <td bgcolor="#D1F9FC" class="txtNumber">Price</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $row['dblPrice'];?></td>
                  </tr>
                <tr>
                  <td bgcolor="#D1F9FC" class="txtNumber">PSD</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['dtPSD'];?></td>
                  <td bgcolor="#D1F9FC" class="txtNumber">Deliver Date</td>
                  <td bgcolor="#FFFFFF" class="normalfntsm">:<?php  echo $Mrow['dtDeliveryDate'];?></td>
                  </tr>
                </table>
            </div></td> 
            <td height="22" class="normalfnt">&nbsp;</td><td colspan="2" rowspan="2" class="normalfntMid"><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:264px;height:155px;overflow:hidden;margin-left:10px" >
              <?php
			if($salesOrderId!='')
			{
				echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"documents/sampleinfo/samplePictures/".$Mrow['intSampleNo']."-".$Mrow['intSampleYear']."-".$Mrow['intRevisionNo']."-".substr($Mrow['strPrintName'],6,2).".png\" />";	
			}
			
			?>
            </div></td>
            </tr>
          
          </table></td>
      </tr>
      <?php 
		//	$plan2Garment = (float)$Mrow['intQty']- $actual1Qty;
			
			$garmentQty25 = round(($Mrow['intQty']/100)*25);
			$garmentQty75 = $Mrow['intQty']-round(($Mrow['intQty']/100)*25);
			
			
			$sqlg="SELECT
					ware_bulkallocation_header.int25PlanGarmentQty,
					ware_bulkallocation_header.int75PlanGarmentQty
					FROM ware_bulkallocation_header
					WHERE
					ware_bulkallocation_header.intOrderNo =  '$orderNo' AND
					ware_bulkallocation_header.intOrderYear =  '$orderYear' AND
					ware_bulkallocation_header.intSalesOrderId =  '$salesOrderId'"; 
			$resultg = $db->RunQuery($sqlg);
			$rowg = mysqli_fetch_array($resultg);
			if($rowg['int25PlanGarmentQty']>0){
				$garmentQty25=$rowg['int25PlanGarmentQty'];
				$garmentQty75=$rowg['int75PlanGarmentQty'];
			}

			$savedFlag25=0;
			$savedFlagFirstOrSecondDay=0;
			$savedFlag75=0;
			
			 $sqlF="SELECT
					Sum(ware_bulkallocation_details.dbl25AllocatedQty) as savedFlag25,
					Sum(ware_bulkallocation_details.dbl75AllocatedQty) as savedFlag75,
					Sum(ware_bulkallocation_details.dblFirstDayActualConPC) as savedFlagFirstOrSecondDay 
					FROM
					ware_bulkallocation_details
					WHERE
					ware_bulkallocation_details.intOrderNo =  '$orderNo' AND
					ware_bulkallocation_details.intOrderYear =  '$orderYear' AND
					ware_bulkallocation_details.intSalesOrderId =  '$salesOrderId'
					GROUP BY
					ware_bulkallocation_details.intOrderNo,
					ware_bulkallocation_details.intOrderYear,
					ware_bulkallocation_details.intSalesOrderId,
					ware_bulkallocation_details.intLocationId"; 
					$resultF = $db->RunQuery($sqlF);
					$rowF = mysqli_fetch_array($resultF);
					
					if($rowF['savedFlag25']>0)
						$savedFlag25=1;
					if($rowF['savedFlag75']>0)
						$savedFlag75=1;
					if($rowF['savedFlagFirstOrSecondDay']>0)
						$savedFlagFirstOrSecondDay=1;
	  ?>
      <tr>
        <td><!--<div style="width:900px;height:300px;overflow:scroll" >-->
          <table width="100%" class="grid" id="tblMainGrid" >
            
            <tr class="">
              <td width="37%" height="22" bgcolor="#F3E2BC" class="normalfnt" >Item</td>
              <td width="15%" height="22" bgcolor="#F3E2BC" class="normalfnt" >UOM</td>
              <td width="23%" bgcolor="#CAE4FB" class="normalfnt" >1st Day Actual ConPC</td>
              <td width="25%" bgcolor="#CAE4FB" class="normalfnt" >2nd Day Actual ConPC</td>
              </tr>
              <?php
			  	$sql = "select intItem,sum(qty) as qty,strName,unitName,intBomItem,intStoresTrnsItem from (SELECT
							trn_sample_color_recipes.intItem,
							Sum(trn_sample_color_recipes.dblWeight/1000) AS qty,
							mst_item.strName,
							mst_item.intBomItem,
							mst_item.intStoresTrnsItem,
							
							mst_units.strName as unitName
						FROM
							trn_sample_color_recipes
							/*INNER JOIN trn_sample_color_recipes_approve ON trn_sample_color_recipes.intSampleNo = trn_sample_color_recipes_approve.intSampleNo AND trn_sample_color_recipes.intSampleYear = trn_sample_color_recipes_approve.intSampleYear AND trn_sample_color_recipes.intRevisionNo = trn_sample_color_recipes_approve.intRevisionNo AND trn_sample_color_recipes.strCombo = trn_sample_color_recipes_approve.strCombo AND trn_sample_color_recipes.strPrintName = trn_sample_color_recipes_approve.strPrintName
							*/Inner Join mst_item ON mst_item.intId = trn_sample_color_recipes.intItem
							inner join mst_units on mst_item.intUOM = mst_units.intId
						WHERE 
							/*trn_sample_color_recipes_approve.intApproved = 1 AND */
							trn_sample_color_recipes.intSampleNo 	=  '".$Mrow['intSampleNo']."' AND
							trn_sample_color_recipes.intSampleYear 	=  '".$Mrow['intSampleYear']."' AND
							trn_sample_color_recipes.intRevisionNo 	=  '".$Mrow['intRevisionNo']."' AND
							trn_sample_color_recipes.strCombo 		=  '".$Mrow['strCombo']."' AND
							trn_sample_color_recipes.strPrintName 	=  '".$Mrow['strPrintName']."'
						GROUP BY
							trn_sample_color_recipes.intItem
							
						UNION
						
						SELECT
							trn_sample_foil_consumption.intItem,
							Sum(trn_sample_foil_consumption.dblMeters) AS qty,
							mst_item.strName,
							mst_item.intBomItem,
							mst_item.intStoresTrnsItem,
							
							'Meter' as unitName
						FROM
							trn_sample_foil_consumption
							Inner Join mst_item ON mst_item.intId = trn_sample_foil_consumption.intItem
						WHERE
							trn_sample_foil_consumption.intSampleNo 		=  '".$Mrow['intSampleNo']."' AND
							trn_sample_foil_consumption.intSampleYear 		=  '".$Mrow['intSampleYear']."' AND
							trn_sample_foil_consumption.intRevisionNo 		=  '".$Mrow['intRevisionNo']."' AND
							trn_sample_foil_consumption.strCombo 			=  '".$Mrow['strCombo']."' AND
							trn_sample_foil_consumption.strPrintName 		=  '".$Mrow['strPrintName']."'
						GROUP BY
							trn_sample_foil_consumption.intItem
						
						UNION
						
						SELECT
							trn_sample_spitem_consumption.intItem,
							Sum(trn_sample_spitem_consumption.dblQty) AS qty,
							mst_item.strName,
							mst_item.intBomItem,
							mst_item.intStoresTrnsItem,
							
							mst_units.strName AS unitName
						FROM
							trn_sample_spitem_consumption
							Inner Join mst_item ON mst_item.intId = trn_sample_spitem_consumption.intItem
							Inner Join mst_units ON mst_units.intId = mst_item.intUOM
						WHERE
							trn_sample_spitem_consumption.intSampleNo 		=  '".$Mrow['intSampleNo']."' AND
							trn_sample_spitem_consumption.intSampleYear 	=  '".$Mrow['intSampleYear']."' AND
							trn_sample_spitem_consumption.intRevisionNo 	=  '".$Mrow['intRevisionNo']."' AND
							trn_sample_spitem_consumption.strCombo 			=  '".$Mrow['strCombo']."' AND
							trn_sample_spitem_consumption.strPrintName 		=  '".$Mrow['strPrintName']."'
						GROUP BY
							trn_sample_spitem_consumption.intItem
							
						) MAIN 
						GROUP BY intItem


						";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{  
					$item=  $row['intItem'];
					$bomItem=  $row['intBomItem'];
					$storesTrnsItem=  $row['intStoresTrnsItem'];
					$disable='';
					if($bomItem==1 && $storesTrnsItem==1){
						$disable='disabled="disabled"';
					}
					 
					  $sql2 = "SELECT
							ware_bulkallocation_details.dbl25AllocatedQty,
							ware_bulkallocation_details.dbl75AllocatedQty, 
							round(ware_bulkallocation_details.dblFirstDayActualConPC,6) as dblFirstDayActualConPC,  
							round(ware_bulkallocation_details.dblSecondDayActualConPC,6) as dblSecondDayActualConPC  
							FROM ware_bulkallocation_details
							WHERE
							/*ware_bulkallocation_details.intLocationId =  '$locationId' AND */
							ware_bulkallocation_details.intOrderNo =  '$orderNo' AND
							ware_bulkallocation_details.intOrderYear =  '$orderYear' AND
							ware_bulkallocation_details.intSalesOrderId =  '$salesOrderId' AND
							ware_bulkallocation_details.intItemId =  '".$row['intItem']."'
							";
					$result2 = $db->RunQuery($sql2);
					$row2 = mysqli_fetch_array($result2);
					
					$uom=$row['unitName'];
					$sampleConPC=$row['qty'];
					$planQty25=$sampleConPC*$garmentQty25;
					$allocatedQty25 = $row2['dbl25AllocatedQty'];
					$allocatedQty75 = $row2['dbl75AllocatedQty'];
					$firstDayActualConPC = $row2['dblFirstDayActualConPC'];
					$secondDayActualConPC = $row2['dblSecondDayActualConPC'];
					
					$sampleActualConPC=$firstDayActualConPC;
					if($secondDayActualConPC>0){
					$sampleActualConPC=$secondDayActualConPC;
					}
					
					$minimumComPC=$allocatedQty75/$Mrow['intQty'];
					
					$minimumComPC1=0;
					$minimumComPC2=0;
					if($firstDayActualConPC>0){
						$minimumComPC1=$minimumComPC; 
					}
					if($secondDayActualConPC>0){
						$minimumComPC2=$minimumComPC;
					}
					//-----------------------------------
					 $sqlC="SELECT
							ware_sub_stocktransactions_bulk.intOrderNo,
							ware_sub_stocktransactions_bulk.intOrderYear,
							trn_inktype_production_allocation.dblOrderQty,
							ware_sub_stocktransactions_bulk.intColorId,
							ware_sub_stocktransactions_bulk.intTechnique,
							ware_sub_stocktransactions_bulk.intInkType,
							trn_inktype_production_allocation.dblConsumption as dblInkConsump, 
							ware_sub_stocktransactions_bulk.intItemId,
							(ware_sub_stocktransactions_bulk.dblQty*-1) as dblItemConsump,
							(
							select sum(tb1.dblQty*-1) from 
							ware_sub_stocktransactions_bulk as tb1
							where 
							ware_sub_stocktransactions_bulk.intOrderNo=tb1.intOrderNo AND 
							ware_sub_stocktransactions_bulk.intOrderYear=tb1.intOrderYear AND 
							ware_sub_stocktransactions_bulk.intColorId=tb1.intColorId AND 
							ware_sub_stocktransactions_bulk.intTechnique=tb1.intTechnique AND 
							ware_sub_stocktransactions_bulk.intInkType=tb1.intInkType AND 
							tb1.intSavedOrder =1 
							) as dblItemSumConsump,
							trn_inktype_production_allocation.dtmDate 
							
							FROM
							trn_inktype_production_allocation
							INNER JOIN ware_sub_stocktransactions_bulk ON 
							trn_inktype_production_allocation.intOrderYear = ware_sub_stocktransactions_bulk.intOrderYear 
							AND trn_inktype_production_allocation.intOrderNo = ware_sub_stocktransactions_bulk.intOrderNo 
							AND trn_inktype_production_allocation.intColorId = ware_sub_stocktransactions_bulk.intColorId 
							AND trn_inktype_production_allocation.intTechniqueId = ware_sub_stocktransactions_bulk.intTechnique 
							AND trn_inktype_production_allocation.intInkTypeId = ware_sub_stocktransactions_bulk.intInkType
							WHERE
							ware_sub_stocktransactions_bulk.intSavedOrder IN ('1','2') 
							AND trn_inktype_production_allocation.intOrderNo = '$orderNo' 
							AND trn_inktype_production_allocation.intOrderYear = '$orderYear' 
							AND ware_sub_stocktransactions_bulk.intItemId = '$item' 
							GROUP BY  
							ware_sub_stocktransactions_bulk.intItemId,date(trn_inktype_production_allocation.dtmDate)
							ORDER BY
							date(trn_inktype_production_allocation.dtmDate) ASC limit 2";
					$resultC = $db->RunQuery($sqlC);
					$i=0;
					while($rowC=mysqli_fetch_array($resultC))
					{  
						if($i==0){
						$firstDayActualConPC=round(($rowC['dblItemConsump']/$rowC['dblItemSumConsump'])*($rowC['dblInkConsump']/$rowC['dblOrderQty']),6);
						}
						else if($i==1){
						$secondDayActualConPC=round(($rowC['dblItemConsump']/$rowC['dblItemSumConsump'])*($rowC['dblInkConsump']/$rowC['dblOrderQty']),6);
						}
						
						$i++;
					}
					//-----------------------------------
			  ?>
              
            <tr class="">
              <td height="22" bgcolor="#FFFFFF" class="normalfnt itemId" id="<?php echo $row['intItem'] ?>" ><?php echo $row['strName']; ?></td>
              <td height="22" bgcolor="#FFFFFF" class="normalfntMid" ><?php echo $uom; ?></td>
              <td bgcolor="#FFFFFF" class="normalfntRight firstDayConPC" ><input name="txtFirstDay" <?php //echo $disable; ?> type="text" <?php if($saved25==1){ ?> disabled="disabled" <?php } ?> class="<?php echo "validate[min[$minimumComPC1],custom[number]]";?> firstDay" id="txt1Plan" style="width:90px;text-align:center" value="<?php echo number_format(round($firstDayActualConPC,6),6); ?>" /> </td>
              <td bgcolor="#FFFFFF" class="normalfntRight secondDayConPC"><input name="txtFirstDay" <?php //echo $disable; ?> type="text" <?php if($saved25==1){ ?> disabled="disabled" <?php } ?> class="<?php echo "validate[min[$minimumComPC2],custom[number]]";?> secondDay" id="txt1Plan" style="width:90px;text-align:center" value="<?php echo number_format(round($secondDayActualConPC,6),6); ?>" /></td>
              </tr>
            <?php
				}
			?>
          </table>
          <!--</div>--></td>
      </tr>
      <tr>
        <td align="center" class=""><table width="100%" border="0">
          <tr>
            <td width="28%" class="normalfnt">&nbsp;</td>
            <td width="72%" valign="top" class="normalfnt">&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><img id="butNew" class="mouseover" src="images/Tnew.jpg" width="92" height="24" /><img id="butSave" src="images/Tsave.jpg" width="92" height="24" class="mouseover" /><img src="images/Tclose.jpg" width="92" height="24" /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<div    style="width:500px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
 
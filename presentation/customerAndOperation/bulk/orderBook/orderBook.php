<?php //(define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>

<?php
include_once("class/tables/mst_customer.php");             $obj_mstCustomer = new mst_customer($db);
include_once("class/tables/trn_orderheader.php");          $trn_orderheader = new trn_orderheader($db);




?>
<head>
<title>Order Book</title>
<script type="text/javascript" src="presentation/customerAndOperation/bulk/orderBook/orderBook_js.js"></script>
</head>
<body >
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<form id="frmOrderBook" name="frmOrderBook" >
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Order Book</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
          <tr>
    <td>
    <table width="100%" border="0">
    	<tr>
        <td width="100%" bgcolor="#66CCFF" ><strong>Customer Information</strong></td>
   		</tr>
    
 
      <tr>
        <td>
       <table width="100%" border="0" class="">
            
          <tr>
      
          <tr>
            <td class="normalfnt"><strong>Customer &nbsp; &nbsp; &nbsp; &nbsp;</strong></td>
            <td class="normalfnt"><select class="validate[required]" name="cboCustomer" id="cboCustomer" style="width:150px"  <?php if($recvdQty>0){ ?> disabled="disabled"<?php } ?>>
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_customer.intId,
							mst_customer.strCode,
							mst_customer.strName
							FROM mst_customer
							WHERE
							mst_customer.intStatus =  '1' order by strName
							";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$intCustomer)
							echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
						else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
                  </select> </td>
            <td>&nbsp;</td>
            <td class="normalfnt"><strong>Order Year</strong></td>
            <td class="normalfnt"><select class="validate[required]" name="cboOrderYear" id="cboOrderYear" style="width:150px">
            <option value=""></option>
             <?php
					$sql = "SELECT
								trn_orderheader.intOrderYear,
								trn_orderheader.intCustomer,
								trn_orderheader.intOrderNo
						    FROM
								trn_orderheader
								
							GROUP BY 
								intOrderYear


							";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intOrderNo']==$intOrderNo)
							echo "<option selected=\"selected\" value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";	
						else
							echo "<option value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";	
					}
				?>
            
            
            
            
            </select></td>
          </tr>
          <tr>
            <td class="normalfnt"><strong>Order reference number </strong></td>
            <td class="normalfnt"><select class="validate[required]" name="cboOrderRefNo" id="cboOrderRefNo" style="width:150px" >
            </select></td>
            <td>&nbsp;</td>
            <td class="normalfnt"><strong>Sales order number</strong></td>
            <td><span class="normalfnt">
              <select class="validate[required]" name="cboSalesOrderNo" id="cboSalesOrderNo" style="width:150px">
              </select>
              </span></td>
          </tr>
           <tr>
            <td class="normalfnt"><strong>Customer purchase order number</strong></td>
            <td class="normalfnt"><input id="txtCustomerPO" name="txtCustomerPO" style="width:150px" disabled="disabled"  /></td>
            <td>&nbsp;</td>
            <td class="normalfnt"><strong>Delivery location</strong></td>
            <td class="normalfnt"><input id="txtDeliveryLocation" name="txtDeliveryLocation" style="width:150px"  disabled="disabled" /></td>
          </tr>
          
        </table></td>
      </tr>

    
      <tr>
        <td>
     <table width="100%" border="0">
    	<tr>
        <td width="100%" bgcolor="#66CCFF"><strong>Technical Information</strong></td>
   		</tr>
    
 
      <tr>
        <td>
       <table width="100%" border="0" class="">
       
		 <tr>
            <td class="normalfnt"><span class="normalfnt" ><strong>Number of panels pasted at once</strong></span></td>
            <td class="normalfnt"><input id="txtGraphic" name="txtGraphic" style="width:150px" disabled="disabled"  /></td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="normalfnt"><strong>Final sample revision number</strong></td>
            <td class="normalfnt"><input id="txtSampRev" name="txtSampRev" style="width:150px" disabled="disabled"  /></td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="normalfnt"><strong>Body colour</strong></td>
            <td class="normalfnt"><input id="txtBodyCol" name="txtBodyCol" style="width:150px" disabled="disabled"  /></td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="normalfnt"></td>
            <td class="normalfnt"></td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="normalfnt" colspan="5">
            	 <table style="width:100%" id="tblColors">
                      <tr bgcolor="#66CCFF">
                        <td><strong>Color</strong></td>
                        <td><strong>Technique</strong></td>
                        <td><strong>Ink Type</strong></td>
                        <td><strong>Size</strong></td>
                      </tr>
				</table>
            </td>
          </tr>
           <tr>
            <td class="normalfnt"></td>
            <td class="normalfnt"></td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
           <tr>
            <td class="normalfnt" colspan="5">
            	<table style="width:100%">
                  <tr  bgcolor="#66CCFF">
                    <td colspan="3"><strong>Production process &nbsp; &nbsp; &nbsp;</strong></td>
                    
                  </tr>
                  <tr>
                    <td>1</td>
                    <td class="normalfnt"><input id="txtGraphic" name="txtGraphic" style="width:150px" disabled="disabled" value="Board Printing" /></td
                  ></tr>
                   <tr>
                    <td>2</td>
                    <td class="normalfnt"><input id="txtGraphic" name="txtGraphic" style="width:150px" disabled="disabled"  /></td
                  ></tr>
                   <tr>
                    <td>3</td>
                    <td class="normalfnt"><input id="txtGraphic" name="txtGraphic" style="width:150px" disabled="disabled"  /></td
                  ></tr>
                   <tr>
                    <td>4</td>
                    <td class="normalfnt"><input id="txtGraphic" name="txtGraphic" style="width:150px" disabled="disabled"  /></td
                  ></tr>
                </table></td>
            <td class="normalfnt"></td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          
        </table></td>
      </tr>

    </table>
    </td></tr>
    
    <tr><td>
    <table width="100%" border="0">
    	<tr>
        <td width="100%" bgcolor="#66CCFF"><strong>Planning Information</strong></td>
   		</tr>
    
 
      <tr>
        <td>
       <table width="100%" border="0" class="">
        <tr>
            <td class="normalfnt"><strong>Lead time (Days)</strong></td>
            <td class="normalfnt"><input id="txtGraphic" name="txtGraphic" style="width:150px" disabled="disabled"  /></td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="normalfnt"><strong>Style number</strong></td>
            <td class="normalfnt"><input id="txtStyle" name="txtStyle" style="width:150px" disabled="disabled"  /></td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="normalfnt"><strong>Order quantity</strong></td>
            <td class="normalfnt"><input id="txtOrderQty" name="txtOrderQty" style="width:150px" disabled="disabled"  /></td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          
          <tr>
            <td class="normalfnt"><strong>Production start day (PSD)</strong></td>
            <td class="normalfnt"><input id="txtPsd" name="txtPsd" style="width:150px" disabled="disabled"  /></td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="normalfnt"><strong>Daily requirement</strong></td>
            <td class="normalfnt"><input id="txtGraphic" name="txtGraphic" style="width:150px" disabled="disabled"  /></td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="normalfnt"><strong>Marketer</strong></td>
            <td class="normalfnt"><input id="txtMarketer" name="txtMarketer" style="width:150px" disabled="disabled"  /></td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td class="normalfnt"><strong>Unit Price</strong></td>
           <td class="normalfnt"><input id="txtUnitPrice" name="txtUnitPrice" style="width:150px" disabled="disabled"  /></td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          
        </table></td>
      </tr>

    </table>
    </td></tr>
    </table></td></tr>
    
   </table>

  </div>
  
  </div>
</form>



</body>


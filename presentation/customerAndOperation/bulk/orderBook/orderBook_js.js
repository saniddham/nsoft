var basepath		= 'presentation/customerAndOperation/bulk/orderBook';
var menuID	 = 1196;

$(document).ready(function() {
	$('#frmOrderBook #cboOrderYear').die('change').live('change',loadOrderRefNo);
	$('#frmOrderBook #cboCustomer').die('change').live('change',loadOrderRefNo);
	$('#frmOrderBook #cboOrderRefNo').die('change').live('change',loadSalesOrder);
	$('#frmOrderBook #cboSalesOrderNo').die('change').live('change',loadOtherDetails);
});	
	


function loadOrderRefNo()
{
	var cboOrderYear			= $('#frmOrderBook #cboOrderYear').val(); 
	var customer				= $('#frmOrderBook #cboCustomer').val(); 
	//alert(cboOrderYear);
	//var cboOrderRefNo		 	= $('#frmOrderBook #cboOrderRefNo').val();
	var url 			= "controller.php?q="+menuID+"&requestType=loadOrderNo&OrderYear="+cboOrderYear+"&customer="+customer;
	var httpobj = $.ajax({
			url:url,
			dataType:'json',
			async:false,
			success:function(json){
				$('#frmOrderBook #cboOrderRefNo').html(json.option_order_nos);
			}
		});	
}

function loadSalesOrder()
{
	var cboOrderYear			= $('#frmOrderBook #cboOrderYear').val(); 
	var orderNo					= $('#frmOrderBook #cboOrderRefNo').val(); 

	//alert(cboOrderYear);
	//var cboOrderRefNo		 	= $('#frmOrderBook #cboOrderRefNo').val();
	var url 			= "controller.php?q="+menuID+"&requestType=loadSalesOrder&orderYear="+cboOrderYear+"&orderNo="+orderNo;
	var httpobj = $.ajax({
			url:url,
			dataType:'json',
			async:false,
			success:function(json){
				$('#frmOrderBook #txtCustomerPO').val(json.option_custPO);		
				$('#frmOrderBook #cboSalesOrderNo').html(json.option_so_order_nos);		
				$('#frmOrderBook #txtDeliveryLocation').val(json.custLocation);		
				$('#frmOrderBook #txtMarketer').val(json.marketer);	
			}
		});	
}

function loadOtherDetails()
{
	var cboOrderYear			= $('#frmOrderBook #cboOrderYear').val(); 
	var orderNo					= $('#frmOrderBook #cboOrderRefNo').val(); 
	var so						= $('#frmOrderBook #cboSalesOrderNo').val(); 
	//alert(cboOrderYear);
	//var cboOrderRefNo		 	= $('#frmOrderBook #cboOrderRefNo').val();
	var url 			= "controller.php?q="+menuID+"&requestType=loadOtherDetails&orderYear="+cboOrderYear+"&orderNo="+orderNo+"&so="+so;
	var httpobj = $.ajax({
			url:url,
			dataType:'json',
			async:false,
			success:function(json){
				$('#frmOrderBook #txtSampRev').val(json.final_samp_revision);		
				$('#frmOrderBook #txtBodyCol').val(json.body_colour);		
				$('#frmOrderBook #txtStyle').val(json.style);	
				$('#frmOrderBook #txtOrderQty').val(json.orderQty);	
				$('#frmOrderBook #txtPsd').val(json.psd);	
				//$('#frmOrderBook #txtDeliveryLocation').val(json.deliveryDt);	
				$('#frmOrderBook #txtUnitPrice').val(json.unitPrice);	
				$('#frmOrderBook #tblColors').html('');	
				$('#frmOrderBook #tblColors').html($('#frmOrderBook #tblColors').html()+json.rows_color);	
			}
		});	
}


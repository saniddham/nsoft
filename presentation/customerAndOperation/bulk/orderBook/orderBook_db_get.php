<?php 

include_once("class/tables/trn_orderheader.php");          $trn_orderheader = new trn_orderheader($db);

$requestType	= $_REQUEST['requestType'];


if($requestType == 'loadOrderNo'){
	
	$OrderYear				= $_REQUEST['OrderYear'];
	$customer				= $_REQUEST['customer'];
	
	$sql= "SELECT
			 trn_orderheader.intOrderNo
		   FROM
			 trn_orderheader
		   WHERE
			 trn_orderheader.intOrderYear='".$OrderYear."' and trn_orderheader.intCustomer='".$customer."'";
			$result = $db->RunQuery($sql);
			//echo $db->getSql();
			$html		= "<option value=\"\"></option>";
			while($rows = mysqli_fetch_array($result))
			{
				$html 	.= "<option value=\"".$rows['intOrderNo']."\">".$rows['intOrderNo']."</option>";
			}
				
			$response['option_order_nos']	=$html;
 			echo json_encode($response);
			//echo "dsfsd";
			//echo $result;
}
else if($requestType == 'loadSalesOrder'){
	
	$OrderYear				= $_REQUEST['orderYear'];
	$orderNo				= $_REQUEST['orderNo'];
	
	$sql	= "SELECT
			 trn_orderheader.strCustomerPoNo ,
			 mst_customer_locations_header.strName as custLocation ,
			 sys_users.strFullName as marketer 
		   FROM
			 trn_orderheader
			 INNER JOIN mst_customer_locations ON trn_orderheader.intCustomerLocation = mst_customer_locations.intLocationId
INNER JOIN mst_customer_locations_header ON mst_customer_locations.intLocationId = mst_customer_locations_header.intId
		     INNER JOIN sys_users ON trn_orderheader.intMarketer = sys_users.intUserId
		   WHERE
			 trn_orderheader.intOrderYear='".$OrderYear."' and trn_orderheader.intOrderNo='".$orderNo."'";
	$result = $db->RunQuery($sql);
	$rows = mysqli_fetch_array($result);
	$response['option_custPO']	=$rows['strCustomerPoNo'];
	$response['custLocation']	=$rows['custLocation'];
	$response['marketer']		=$rows['marketer'];

	$sql= "SELECT 
			trn_orderdetails.intSalesOrderId,
			trn_orderdetails.strSalesOrderNo,
			trn_orderdetails.strPrintName,
			trn_orderdetails.strCombo,
			mst_part.strName,
			concat(trn_orderdetails.strSalesOrderNo,'/',trn_orderdetails.strPrintName,'/',trn_orderdetails.strCombo,'/',mst_part.strName) as so 
			FROM
			trn_orderdetails
			INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
			WHERE
			trn_orderdetails.intOrderNo = '$orderNo' AND
			trn_orderdetails.intOrderYear = '$OrderYear'";
			$result = $db->RunQuery($sql);
			//echo $db->getSql();
			$html		= "<option value=\"\"></option>";
			while($rows = mysqli_fetch_array($result))
			{
				$html 	.= "<option value=\"".$rows['intSalesOrderId']."\">".$rows['so']."</option>";
			}
				
			$response['option_so_order_nos']	=$html;
 			echo json_encode($response);
			//echo "dsfsd";
			//echo $result;
	}
else if($requestType == 'loadOtherDetails'){
	
	$OrderYear				= $_REQUEST['orderYear'];
	$orderNo				= $_REQUEST['orderNo'];
	$so						= $_REQUEST['so'];
	
	$sql	= "SELECT
				trn_orderdetails.strSalesOrderNo,
				trn_orderdetails.strPrintName,
				trn_orderdetails.strCombo,
				mst_part.strName,
				trn_orderdetails.intSalesOrderId,
				trn_orderdetails.strGraphicNo,
				trn_orderdetails.strStyleNo,
				trn_orderdetails.intSampleNo,
				trn_orderdetails.intSampleYear,
				trn_orderdetails.intRevisionNo,
				trn_orderdetails.intQty,
				trn_orderdetails.dblPrice,
				trn_orderdetails.dtPSD,
				trn_orderdetails.dtDeliveryDate,
				mst_colors_ground.strName as ground_color 
				FROM
				trn_orderdetails
				INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
				INNER JOIN trn_sampleinfomations_details ON trn_orderdetails.intSampleNo = trn_sampleinfomations_details.intSampleNo AND trn_orderdetails.intSampleYear = trn_sampleinfomations_details.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations_details.intRevNo AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
INNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
				WHERE
				trn_orderdetails.intOrderNo = '$orderNo' AND
				trn_orderdetails.intOrderYear = '$OrderYear' AND
				trn_orderdetails.intSalesOrderId = '$so'";
	$result = $db->RunQuery($sql);
	$rows = mysqli_fetch_array($result);
	$response['final_samp_revision']	=$rows['intSampleNo'].'/'.$rows['intSampleYear'].'/'.$rows['intRevisionNo'];
	$response['body_colour']			=$rows['ground_color'];
	$response['style']					=$rows['strStyleNo'];
	$response['orderQty']				=$rows['intQty'];
	$response['psd']					=$rows['dtPSD'];
	$response['deliveryDt']				=$rows['dtDeliveryDate'];
	$response['unitPrice']				=$rows['dblPrice'];
	$response['rows_color']				=loadColors($orderNo,$OrderYear,$so);

	echo json_encode($response);
			//echo "dsfsd";
			//echo $result;
	}
	
	function loadColors($orderNo,$OrderYear,$so){
		global $db;
		
		$sql	="SELECT
				mst_colors.strName AS color,
				mst_techniques.strName AS technique,
				mst_inktypes.strName AS inkType,
				trn_ordersizeqty.strSize
				FROM
				trn_orderdetails
				INNER JOIN trn_sample_color_recipes ON trn_orderdetails.intSampleNo = trn_sample_color_recipes.intSampleNo AND trn_orderdetails.intSampleYear = trn_sample_color_recipes.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sample_color_recipes.intRevisionNo AND trn_orderdetails.strCombo = trn_sample_color_recipes.strCombo AND trn_orderdetails.strPrintName = trn_sample_color_recipes.strPrintName
				INNER JOIN mst_colors ON trn_sample_color_recipes.intColorId = mst_colors.intId
				INNER JOIN mst_techniques ON trn_sample_color_recipes.intTechniqueId = mst_techniques.intId
				INNER JOIN mst_inktypes ON trn_sample_color_recipes.intInkTypeId = mst_inktypes.intId
				INNER JOIN trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
				WHERE
				trn_orderdetails.intOrderNo = '$orderNo' AND
				trn_orderdetails.intOrderYear = '$OrderYear'  AND
				trn_orderdetails.intSalesOrderId = '$so' 
				group by  
				mst_colors.strName,
				mst_techniques.strName ,
				mst_inktypes.strName ,
				trn_ordersizeqty.strSize 
				order by 
				mst_colors.strName ASC,
				mst_techniques.strName ASC,
				mst_inktypes.strName ASC,
				trn_ordersizeqty.strSize ASC
				";
				$cells	 ='<tr bgcolor="#66CCFF">';
                $cells	.="<td><strong>Color</strong></td>";
                $cells	.="<td><strong>Technique</strong></td>";
                $cells	.="<td><strong>Ink Type</strong></td>";
                $cells	.="<td><strong>Size</strong></td>";
				$cells	.="</tr>";
			$result = $db->RunQuery($sql);
			while($rows = mysqli_fetch_array($result))
			{
				$cells	.="<tr>";
				$cells	.="<td>".$rows['color']."</td>";
				$cells	.="<td>".$rows['technique']."</td>";
				$cells	.="<td>".$rows['inkType']."</td>";
				$cells	.="<td>".$rows['strSize']."</td>";
				$cells	.="</tr>";
			}
		
		return $cells;
}
	
	

?>

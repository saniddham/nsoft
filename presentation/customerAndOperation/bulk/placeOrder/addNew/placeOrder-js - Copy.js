var basepath		= 'presentation/customerAndOperation/bulk/placeOrder/addNew/';	
var msgAlert	='';
var errsplit	=0;


$(document).ready(function() {	

	$('#frmPlaceOrders').validationEngine();
	
	calculateTotalVal();
	
	$('#frmPlaceOrders #txtCustomerPO').focus();
	
	$('#tblPOs #butSizeCopy').die('click').live('click',copySizeWiseQty);
	
	/*$("#cboCustomer").die('change').live('change',function(){
			//loadSubCategory($(this).val(),this);
                        checkCustomer($(this).val());
                      //  alert("hdh");
	}); */
	
	$("#cboPoType").die('change').live('change',function(){
			if($(this).val()==1 && $('#frmPlaceOrders #cboCompany').val()==1 && ($('#frmPlaceOrders #cboCustomer').val()==28 || $('#frmPlaceOrders #cboCustomer').val()==105))
				$('#divInstant').css('display', '');
			else{
				$('#divInstant').css('display', 'none');
				$('#chkInstant').attr('checked', false);
			}
	}); 
	
	
	$("#frmPlaceOrders #cboCompany").die('change').live('change',function(){
			if($(this).val()==1 && $('#frmPlaceOrders #cboPoType').val()==1 && ($('#frmPlaceOrders #cboCustomer').val()==28 || $('#frmPlaceOrders #cboCustomer').val()==105))
				$('#divInstant').css('display', '');
			else{
				$('#divInstant').css('display', 'none');
				$('#chkInstant').attr('checked', false);
			}
	}); 

	
	$('.delImg').die('click').live('click',function(){
		
			var obj 		=this
			var so			=$(this).parent().parent().find('#SO').html();
			var parentSo	=$(this).parent().parent().find('#parentSO').html();
			var childQty	=$(this).parent().parent().find('.clsQty').val();
			var childCustPO	=$(this).parent().parent().find('#txtCustoPOsplited').val();
			var childFlag	=0;	
			var orderNo		=$('#txtOrderNo').val();
			var year		=$('#txtYear').val();
			var rowCount1 	= document.getElementById('tblPOs').rows.length;
			
			var url 		= basepath+"placeOrder-db-get.php?requestType=getChildFlag&orderNo="+orderNo+"&year="+year+"&so="+so;
			var httpobj 	= $.ajax({url:url,async:false})
			childFlag=httpobj.responseText;
			
			$('.clsQty').each(function(){
				//alert(so+'=='+$(this).parent().parent().find('#parentSO').html());
				if(so==$(this).parent().parent().find('#parentSO').html())
					childFlag=1;
			});
			
			//alert(rowCount1);
			if(rowCount1<=3)
			{
				alert("Can't delete.Atleast one sales order should be exists in a bulk order");
				return false;	
			}
			else if(childFlag==1 && so !='' && $('#cboPoType').val()==1){
				alert("Can't delete.Split sales orders exists");
				return false;	
			}
		
			//add deleted child qty to parent
			var iniDummyQty	=0;
			var dummyQty	=0;
			$('.clsQty').each(function(){
				//alert(so+'=='+$(this).parent().parent().find('#parentSO').html());
				if(parentSo==$(this).parent().parent().find('#SO').html()){
					if(childCustPO==$(this).parent().parent().find('#txtCustoPOsplited').val())
						iniDummyQty	= $(this).val();
					else
						iniDummyQty = $(this).parent().parent().find('#dummyQty').html();
					
					dummyQty	= $(this).val();
					//alert(iniDummyQty+'/'+dummyQty+'/'+childQty);
					if(parseFloat(iniDummyQty) < parseFloat(dummyQty)+parseFloat(childQty) )
						$(this).val(parseFloat(iniDummyQty));
					else
						$(this).val(parseFloat(dummyQty)+parseFloat(childQty));
						
					if($(this).val() >0 )
						$(this).attr('disabled',false);
				}
				
 			});
		
			if(childFlag==1 && $('#cboPoType').val()==1)
				alert('Deleted split order qty was added to the parent sales order.');
	
			if($(obj).parent().parent().parent().find('tr').length>3)
			{
				$(this).parent().parent().remove();
				$('#tblPOs >tbody >tr:eq(1)').find('.clsCopy').html('&nbsp;');
			}
			
			
			var viewSave_parent	=1;
			$('#tblPOs >tbody >tr').not(':last').not(':first').each(function(){
				//alert($(this).find('#txtCustoPOsplited').val()+'='+$('#txtCustomerPO').val());
				if($('#txtCustomerPO').val()!=$(this).find('#txtCustoPOsplited').val()){
					viewSave_parent	=0;
				}
			});
 			if(viewSave_parent==1){
				$('.cls_insertRow').show();
				$('#butSave').show();
				$('#butConfirm').show();
				$('#butSaveChild').hide();
			}
		});

	$('.delImgPopup').die('click').live('click',function(){
		
		var rowCount1 = document.getElementById('tblSizesPopup').rows.length;
		if(rowCount1>3)
		{
			$(this).parent().parent().remove();
		}
		
		
	});
 
	$('#butInsertRowPopup').die('click').live('click',function(){
		var rowCount = document.getElementById('tblSizesPopup').rows.length;
		document.getElementById('tblSizesPopup').insertRow(rowCount-1);
		rowCount = document.getElementById('tblSizesPopup').rows.length;
		document.getElementById('tblSizesPopup').rows[rowCount-2].innerHTML = document.getElementById('tblSizesPopup').rows[rowCount-3].innerHTML;
		document.getElementById('tblSizesPopup').rows[rowCount-2].cells[0].innerHTML = "<img src='images/del.png' width='15' height='15' class='delImgPopup' />";
		document.getElementById('tblSizesPopup').rows[rowCount-2].cells[2].childNodes[0].className = "validate[required,custom[number],min[0]] sizeQty";
		document.getElementById('tblSizesPopup').rows[rowCount-2].cells[1].childNodes[0].value='';
		document.getElementById('tblSizesPopup').rows[rowCount-2].cells[2].childNodes[0].value=0;
		document.getElementById('tblSizesPopup').rows[rowCount-2].cells[3].innerHTML=0;
 	});

	$('#frmPlaceOrderPopup .sizeQty').die('keyup').live('keyup',function(){
		calculateTotalBalQty();
	});

  $('#frmPlaceOrders #tblPOs #cboRevision').die('change').live('change',function(){
	  var sampleNo	= $(this).parent().parent().find('#cboSampNo1').val();
	  var sampleYear= $(this).parent().parent().find('#cboYear').val();
	  var revId 	= ($(this).val());
	  var printName	= URLEncode($(this).parent().parent().find('#cboPart').val());
	  var comboId	= URLEncode($(this).parent().parent().find('#cboCombo').val());
	  var ob = $(this);

	  $(this).parent().parent().find('#txtPrice').val(0);
	  $(this).parent().parent().find('#txtGroundColor').val('');
	  
	  if(revId!=''){
		//price
	  	var url = basepath+"placeOrder-db-get.php?requestType=getPrice&sampleNo="+sampleNo+"&sampleYear="+sampleYear+"&revId="+revId+"&printName="+printName+"&comboId="+comboId;
	  	var obj =  $.ajax({url:url,async:false});
	  	ob.parent().parent().find('#txtPrice').val(obj.responseText);
	  	//color
	  	var url2 = basepath+"placeOrder-db-get.php?requestType=getColor&sampleNo="+sampleNo+"&sampleYear="+sampleYear+"&revId="+revId+"&printName="+printName+"&comboId="+comboId;
	  	loadColor(url2,ob);
		//technique group
	  	var url = basepath+"placeOrder-db-get.php?requestType=getTechGroup&sampleNo="+sampleNo+"&sampleYear="+sampleYear+"&revId="+revId+"&printName="+printName+"&comboId="+comboId;
	  	var obj2 =  $.ajax({url:url,async:false});
		ob.parent().parent().find('#cboTechGrp').val(obj2.responseText);
	  }
	  calRowAmount(ob.parent().parent());
  });
  
var prevSupplier = '';
$('#frmPlaceOrders #cboCustomer').die('change').live('change',function(){
	
	checkCustomer($(this).val());
	
	if($('#frmPlaceOrders #cboPoType').val()==1 && $('#frmPlaceOrders #cboCompany').val()==1 && ($('#frmPlaceOrders #cboCustomer').val()==28 || $('#frmPlaceOrders #cboCustomer').val()==105))
		$('#divInstant').css('display', '');
	else{
		$('#divInstant').css('display', 'none');
		$('#chkInstant').attr('checked', false);
	}
  
	var customerId = $('#cboCustomer').val();
	
	var url 		= basepath+"placeOrder-db-get.php?requestType=loadCustLocations&customerId="+customerId;
	var httpobj 	= $.ajax({url:url,async:false})
	document.getElementById('cboLocation').innerHTML=httpobj.responseText;
	
});

$('#frmPlaceOrders #tblPOs #butInsertRow').die('click').live('click',function(){		
	var rowCount = document.getElementById('tblPOs').rows.length;
	document.getElementById('tblPOs').insertRow(rowCount-1);
	rowCount = document.getElementById('tblPOs').rows.length;
	document.getElementById('tblPOs').rows[rowCount-2].innerHTML = document.getElementById('tblPOs').rows[rowCount-3].innerHTML;
	document.getElementById('tblPOs').rows[rowCount-2].className="normalfnt";
	document.getElementById('tblPOs').rows[rowCount-2].id = 'dataRow';
	//document.getElementById('tblPOs').rows[rowCount-3].className="normalfnt";
	//document.getElementById('tblPOs').rows[rowCount-3].id = 'dataRow';
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') #SO').html('');
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') #parentSO').html('');
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') .splitOrder').html('');
	$('#tblPOs >tbody >tr:eq(2)').find('#txtQty').removeClass() ;
	$('#tblPOs >tbody >tr:eq(2)').find('#txtQty').addClass('validate[min[0],custom[integer]] calculateValue clsQty') ;
	
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-2)+') .psDate').attr('id','txtPSD'+($(this).parent().parent().index()-1));
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-2)+') .delDate').attr('id','txtDelDate'+($(this).parent().parent().index()-1));	
	
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') #txtLineNo').attr('disabled',false);
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') #txtSheduleNo').attr('disabled',false);
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') #cboStyle').attr('disabled',false);
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') #cboYear').attr('disabled',false);
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') #cboSampNo1').attr('disabled',false);
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') #txtStyleNo').attr('disabled',false);
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') #cboCombo').attr('disabled',false);
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') .clsCopy').html('&nbsp;');
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') #cboRevision').attr('disabled',false);
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') #txtValue').attr('disabled',false);
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') #txtOverCutPer').attr('disabled',false);
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') #txtDamagePer').attr('disabled',false);
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') #txtPSD1').attr('disabled',false);
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') #txtDelDate1').attr('disabled',false);
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') #cboPart').attr('disabled',false);
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') #txtValue').attr('disabled',true);
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') #txtQty').attr('disabled',false);
	
	$(this).parent().parent().parent().find('tr:eq('+(rowCount-2)+') #txtCustoPOsplited').val($('#txtCustomerPO').val());
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') #txtCustoPOsplited').attr('disabled',true);
 	$(this).parent().parent().parent().find('tr:eq('+(rowCount-2)+') .clsSplit').html('');
 	$(this).parent().parent().parent().find('tr:eq('+(rowCount-2)+') #rawType').html('3');

});


$('#frmPlaceOrders #tblPOs #butSplitRow').die('click').live('click',function(){	

	var obj	=this;
 	$('#childFlag').html(1);
 	var rowCount = document.getElementById('tblPOs').rows.length;
	document.getElementById('tblPOs').insertRow(rowCount-1);
	rowCount = document.getElementById('tblPOs').rows.length;
	document.getElementById('tblPOs').rows[rowCount-2].innerHTML = 	($(obj).parent().parent().html());
;
	document.getElementById('tblPOs').rows[rowCount-2].className="normalfnt splitted_row";
	document.getElementById('tblPOs').rows[rowCount-2].id = 'dataRow';
	document.getElementById('tblPOs').rows[rowCount-2].title = 'split row';
	
	$('#tblPOs >tbody >tr:eq(2)').find('#txtQty').removeClass() ;
	$('#tblPOs >tbody >tr:eq(2)').find('#txtQty').addClass('validate[min[0],custom[integer]] calculateValue clsQty') ;
	
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-2)+') .psDate').attr('id','txtPSD'+($(this).parent().parent().index()-1));
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-2)+') .delDate').attr('id','txtDelDate'+($(this).parent().parent().index()-1));
	
 	$(this).parent().parent().parent().find('tr:eq('+(rowCount-2)+') .splitOrder').html($('#template_split').html());
 	$(this).parent().parent().parent().find('tr:eq('+(rowCount-2)+') #txtCustoPOsplited').attr('disabled',false);
 	$(this).parent().parent().parent().find('tr:eq('+(rowCount-2)+') #txtQty').attr('disabled',false);
	$(this).parent().parent().parent().find('tr:eq('+(rowCount-2)+') #txtCustoPOsplited').val('');
 	$(this).parent().parent().parent().find('tr:eq('+(rowCount-2)+') .clsSplit').html('');
 	$(this).parent().parent().parent().find('tr:eq('+(rowCount-2)+') .cls_size').html('');
 	$(this).parent().parent().parent().find('tr:eq('+(rowCount-2)+') .clsCopy').html('');
 	$(this).parent().parent().parent().find('tr:eq('+(rowCount-2)+') #SO').html('');
 	$(this).parent().parent().parent().find('tr:eq('+(rowCount-2)+') #txtQty').val(0);
 	$(this).parent().parent().parent().find('tr:eq('+(rowCount-2)+') #txtValue').val(0);
	
	$(this).parent().parent().parent().find('tr:eq('+(rowCount-2)+')').css('background-color', '#fadbd8');
	$(this).parent().parent().css('background-color', '#BAD7F3');
	$(this).parent().parent().parent().find('tr:eq('+(rowCount-2)+') #rawType').html(2);
	var PSO	= $(this).parent().parent().find('#SO').html();
	$(this).parent().parent().parent().find('tr:eq('+(rowCount-2)+') #parentSO').html(PSO);
	
	//alert($(obj).parent().parent().html());
	//$('.cls_insertRow').css('display', 'none');
	$('#butSave').css('display', 'none');
	$('#butSaveChild').show();
	$('#butConfirm').css('display', 'none');

});

$('#frmPlaceOrders .selcStyle').die('change').live('change',function() {		
	var style 	= 	$(this).val();
	var row		=	this.parentNode.parentNode.rowIndex;
	var year 	= 	$(this).parent().parent().find('#cboYear').val();
	
	$(this).parent().parent().find('#txtStyleNo').val('');
	$(this).parent().parent().find('#cboSampNo1').val('');
	$(this).parent().parent().find('.cboPart').val('');
	$(this).parent().parent().find('.cboCombo').val('');
	$(this).parent().parent().find('.cboRevision').val('');
	$(this).parent().parent().find('.groundColor').val('');
	$(this).parent().parent().find('.clsQty').val(0);
	$(this).parent().parent().find('.clsPrice').val(0);
	$(this).parent().parent().find('#txtValue').val(0);
	
	if(style!=''){
		var url 	= 	basepath+"placeOrder-db-get.php?requestType=loadSampleNos&style="+URLEncode(style)+"&year="+year;
		var httpobj = 	$.ajax({url:url,async:false})
		$(this).parent().parent().find('#cboSampNo1').html(httpobj.responseText);
	}
		
});


$('#frmPlaceOrders .cboSplitNo').die('change').live('change',function() {		
	var orderNo 	= $(this).val();
	var orderYear	= $(this).parent().parent().find('.cboSplitYear').val();
	var obj			= this;
 	
	if(orderNo!=''){
		var url 	= 	basepath+"placeOrder-db-get.php?requestType=loadSplitCustomerPO&orderNo="+orderNo+"&orderYear="+orderYear;
		var httpobj = 	$.ajax({url:url,async:false})
		$(obj).parent().parent().find('#txtCustoPOsplited').val(httpobj.responseText);
		$(obj).parent().parent().find('#txtCustoPOsplited').prop( "disabled", true );
	}
	else
		$(obj).parent().parent().find('#txtCustoPOsplited').prop( "disabled", false );
		
});

	
 //------------------------------------------------------
	$('#frmPlaceOrders .cboYear').die('change').live('change',function() {		
		var year 		= 	$(this).val();
		var row			=	this.parentNode.parentNode.rowIndex;
		var style 		= 	$(this).parent().parent().find('.selcStyle').val();
		var url 		= 	basepath+"placeOrder-db-get.php?requestType=loadSampleNos&style="+URLEncode(style)+"&year="+year;
		var httpobj 	= 	$.ajax({url:url,async:false});		
		$(this).parent().parent().find('#cboSampNo1').html(httpobj.responseText);
		
	    var customerId 	= $('#cboCustomer').val();
		var url 		= 	basepath+"placeOrder-db-get.php?requestType=loadStyles&year="+year+"&customerId="+customerId;
		var httpobj 	= 	$.ajax({url:url,async:false})		
		$(this).parent().parent().find('#cboStyle').html(httpobj.responseText);
		
		$(this).parent().parent().find('#txtStyleNo').val('');
		$(this).parent().parent().find('#cboSampNo1').val('');
		$(this).parent().parent().find('.cboPart').val('');
		$(this).parent().parent().find('.cboCombo').val('');
		$(this).parent().parent().find('.cboRevision').val('');
		$(this).parent().parent().find('.groundColor').val('');
		$(this).parent().parent().find('.clsQty').val(0);
		$(this).parent().parent().find('.clsPrice').val(0);
		$(this).parent().parent().find('#txtValue').val(0);
		
	});
 //------------------------------------------------------
	$('.allGrapicNos').die('click').live('click',loadAllGraphicNos);
 //------------------------------------------------------
	$('#frmPlaceOrders .cboSampNo1').die('change').live('change',function(){
		var sampNo = $(this).val();
		var row=this.parentNode.parentNode.rowIndex;
		//var style = document.getElementById('tblPOs').rows[row].cells[2	].childNodes[0].value;
		var year = $(this).parent().parent().find('#cboYear').val();

		$(this).parent().parent().find('#txtStyleNo').val('');
		$(this).parent().parent().find('.cboPart').val('');
		$(this).parent().parent().find('.cboCombo').val('');
		$(this).parent().parent().find('.cboRevision').val('');
		$(this).parent().parent().find('.groundColor').val('');
		$(this).parent().parent().find('.clsQty').val(0);
		$(this).parent().parent().find('.clsPrice').val(0);
		$(this).parent().parent().find('#txtValue').val(0);
		
		if(sampNo!=''){
		var url = basepath+"placeOrder-db-get.php?requestType=loadParts&year="+year+"&sampNo="+sampNo;
		var obj = $(this);
		$.ajax({url:url,async:false,dataType:'json',success:function(json){
			obj.parent().parent().find('.cboPart').html(json.parts);
			obj.parent().parent().find('#txtStyleNo').val(json.styleNo);
		}})
		}		
	});	
	
	$('#frmPlaceOrders .cboPart').die('change').live('change',function(){
		var printName = $(this).val();
		var sampleNo = $(this).parent().parent().find('#cboSampNo1').val();
		var sampleYear = $(this).parent().parent().find('#cboYear').val();

		$(this).parent().parent().find('.cboCombo').val('');
		$(this).parent().parent().find('.cboRevision').val('');
		$(this).parent().parent().find('.groundColor').val('');
		//$(this).parent().parent().find('.clsQty').val(0);
		$(this).parent().parent().find('.clsPrice').val(0);
		$(this).parent().parent().find('#txtValue').val(0);
		
		if(printName!=''){
			var url 		= basepath+"placeOrder-db-get.php?requestType=loadCombo&printName="+URLEncode(printName)+"&sampleNo="+sampleNo+"&sampleYear="+sampleYear;
			var httpobj 	= $.ajax({url:url,async:false})
			$(this).parent().parent().find('.cboCombo').html(httpobj.responseText);
		}
	})
	
 //------------------------------------------------------
	$('#frmPlaceOrders .cboCombo').die('change').live('change',function(){
		var printName = $(this).parent().parent().find('#cboPart').val();
		var sampleNo = $(this).parent().parent().find('#cboSampNo1').val();
		var sampleYear = $(this).parent().parent().find('#cboYear').val();
		var placement = $(this).parent().parent().find('#cboPart').val();
		var combo = $(this).parent().parent().find('#cboCombo').val();
		var revNo = $(this).parent().parent().find('#cboRevision').val();
	
		$(this).parent().parent().find('.cboRevision').val('');
		$(this).parent().parent().find('.groundColor').val('');
		//$(this).parent().parent().find('.clsQty').val(0);
		$(this).parent().parent().find('.clsPrice').val(0);
		$(this).parent().parent().find('#txtValue').val(0);
		
		if(combo!=''){
			var url 		= basepath+"placeOrder-db-get.php?requestType=loadRevNo&printName="+URLEncode(printName)+"&sampleNo="+sampleNo+"&sampleYear="+sampleYear+"&placement="+placement+"&combo="+URLEncode(combo)+"&revNo="+revNo;
			var httpobj 	= $.ajax({url:url,async:false})
			$(this).parent().parent().find('.cboRevision').html(httpobj.responseText);
		}
		
	});
 //------------------------------------------------------------------
	$('.calculateValue').die('keyup').live('keyup',function(){
		calRowAmount($(this).parent().parent());
		if(($('#cboPoType').val()==1) && ($(this).parent().parent().find('#parentSO').html()!='')){
			setBalanceOfParent($(this).parent().parent());
		}
		calculateTotalVal();
	});
	
 //------------------------------------------------------------------
	function calRowAmount(tr)
	{
		if(tr.find('#txtQty').val()=='' || tr.find('#txtQty').val()=='NaN')
			tr.find('#txtQty').val(0);
		var qty 	= tr.find('#txtQty').val();qty=(qty==''?0:parseFloat(qty));
		var price 	= tr.find('#txtPrice').val();price=(price==''?0:parseFloat(price));
		var num=qty*price;
		val=Math.round(num*Math.pow(10,2))/Math.pow(10,2)
		tr.find('#txtValue').val(val);
	}
        
        
        
     function checkCustomer(customerId){
    
   //alert(customerId);
    var url = basepath+"placeOrder-db-get.php?requestType=checkCustomer&customerId="+customerId;
    var  httpobj = $.ajax({
            url:url,
            async:false,
            type: "post",
           dataType:"json",
    success: function(json)
		 {
                       
	var type= (json.type);
        if(type=="fail"){
            
            alert("Customer is Blocked for Customer PO");
        }
                    
  }
    
});
}

	function setBalanceOfParent(tr)
	{
		var parent_so 	= tr.find('#parentSO').html(); 
		var used_split	=0;
		var dummy		=0;
		
		$('.clsQty').each(function(){
			if(tr.find('#parentSO').html()==$(this).parent().parent().find('#parentSO').html())
				used_split += parseFloat($(this).val());
			
		});
		
		$('.clsQty').each(function(){
			if(tr.find('#parentSO').html()==$(this).parent().parent().find('#SO').html()){
				dummy	= parseFloat($(this).parent().parent().find('#dummyQty').html()); 
				if(dummy <= used_split ){
					$(this).parent().parent().find('.clsQty').val(0);
					$(this).parent().parent().find('#txtValue').val(0);
					$(this).parent().parent().find('.clsQty').prop( "disabled", true );
				}
				else{
					$(this).parent().parent().find('.clsQty').val(dummy-used_split);
					$(this).parent().parent().find('#txtValue').val((dummy-used_split)*$(this).parent().parent().find('#txtPrice').val());
				}
			}
		});

		//alert(used_split);
		//alert(dummy);
		//var price 	= tr.find('#txtPrice').val();price=(price==''?0:parseFloat(price));
		//var num=qty*price;
		//val=Math.round(num*Math.pow(10,2))/Math.pow(10,2)
		//tr.find('#txtValue').val(val);
	}
//-----------------------------------------------------------------
	$('.clsAddSizes').die('click').live('click',function(){
		var arrBody  	= "";
		var row				=this.parentNode.parentNode.rowIndex;
		var salesOrderNo	= $(this).parent().parent().attr('id');
		var SYear			= $(this).parent().parent().find("#cboYear :selected").attr('id');
		var sampleNo		= $(this).parent().parent().find("#cboSampNo1 :selected").val(); 
		var Revision		= $(this).parent().parent().find("#cboRevision :selected").val();
		var cboprint 		= $(this).parent().parent().find("#cboPart :selected").val();
		var parts 			= cboprint.split('/');
		var Print 			= parts[1];
		var print_P			= URLEncode(Print);
		var combo 			= $(this).parent().parent().find("#cboCombo :selected").val(); 
		
		
		
		if(salesOrderNo=='')
		{
			salesOrderNo=$(this).parent().parent().index();	
		}
		var Qty =  $(this).parent().parent().find('#txtQty').val();
		var orderNo=$('#txtOrderNo').val();
		var year=$('#txtYear').val();

		if(orderNo==''){
			alert("Please Save Shedules before add Sizes");
			return false;	
		}
		popupWindow3('1');

		$('#popupContact1').load(basepath+'placeOrderSizesPopup.php?salesOrderNo='+salesOrderNo+'&orderNo='+orderNo+'&year='+year+'&Qty='+Qty+'&SYear='+SYear+'&sampleNo='+sampleNo+'&Revision='+Revision+'&combo='+combo+'&print_P='+print_P,function(){
				$('#butAdd').click(addClickedRows);
					$('#butClose1').click(disablePopup);
				
			});	
	});
//------------------------------------------------------------------
	$('#frmPlaceOrders #butSave').die('click').live('click',function(){
		saveParent('');
	});

//SAVE CHILD PO OF DUMMY PARENT PO
	$('#frmPlaceOrders #butSaveChild').die('click').live('click',function(){
		if ($('#frmPlaceOrders').validationEngine('validate'))   
		{ 
			saveSplit();
		   if(errsplit==0){
   				saveParent1(msgAlert);
		   }
		//$('#frmPlaceOrders #butSave').die('click').live('click');
		//save('');
		}
	});
  
 					 
 

  $('#frmPlaceOrders #butSaveChild11').click(function(){
	var requestType = '';
	if ($('#frmPlaceOrders').validationEngine('validate'))   
    { 
		showWaiting();
		
		//check order qty
		var boolInvalidQty = 0;
		$('.clsQty').each(function(){
			if($(this).val()=='' || $(this).val()<=0)
			{
				boolInvalidQty = 1;
				$(this).css('background-color','red');
				
			}	
		});
		//check price
		var boolInvalidPrice = 0;
		$('.clsPrice').each(function(){
			if($(this).val()=='' || $(this).val()<=0)
			{
				boolInvalidPrice = 1;
				$(this).css('background-color','red');
				
			}	
		});
		if(boolInvalidQty==1 || boolInvalidPrice==1)
		{
			alert('Invalid Qty or Price.');
			hideWaiting();
			return;
		}
		var instant	=0;
		//if(($("#chkInstant").is(":checked")))
		//	instant=1;
		///////////////////
		var data = "requestType=save";
		
			data+="&parentSerialNo	=" +	$('#txtOrderNo').val();
			data+="&parentSerialYear=" +		$('#txtYear').val();
			data+="&customerPO		=" +	URLEncode($('#txtCustomerPO').val());
			data+="&customer		="	+	$('#cboCustomer').val();
			data+="&currency		="			+	$('#cboCurrency').val();
			data+="&payTerm			="			+	$('#cboPayTerm').val();
			data+="&poCompany		="	+	$('#frmPlaceOrders #cboCompany').val();
			data+="&poLocation		="	+	$('#cboCompLocation').val();
			data+="&location		="	+	$('#cboLocation').val();
			data+="&remarks			="	+	URLEncode($('#txtRemarks').val());
			data+="&delDateH		="	+	$('#txtDelDate').val();
			data+="&dtDate			="	+	$('#dtDate').val();
			data+="&contPerson		="	+	URLEncode($('#txtContactPerson').val());
			data+="&marketer		="	+	$('#cboMarketer').val();
			data+="&payMode			="	+	$('#cboPaymentMode').val();
			data+="&poType			="	+	$('#cboPoType').val();
			data+="&techniqueType	="	+	0;
			data+="&instant="	+	instant;		
		
			//////////////////// saving main grid part //////////////////////////////////////////
			//var rowCount = $('#tblMain >tbody >tr').length;
			var rowCount = document.getElementById('tblPOs').rows.length;
			var row = 0;
			
			var arr="[";
			var salesIds='';
			$('#tblPOs >tbody >tr').not(':last').not(':first').each(function(){
				if($('#txtCustomerPO').val()==$(this).find('#txtCustoPOsplited').val()){
				
					
					 arr += "{";
					var salesOrderId	= 	$(this).attr('id');
					//alert(salesOrderId);
					var salesOrderNo 	= 	$(this).find('#txtSheduleNo').val();
					var parentSoId	 	= 	parseFloat($(this).find('#parentSO').html());
					var graphicNo 		= 	$(this).find('#cboStyle').val();
					var styleNo 		= 	$(this).find('#txtStyleNo').val();
					var sampleYear	 	= 	$(this).find('#cboYear').val();
					var sampleNo	 	= 	$(this).find('#cboSampNo1').val();
					var printName		= 	$(this).find('#cboPart').val();
					var combo		 	= 	$(this).find('#cboCombo').val();
					var revisionNo 		= 	$(this).find('#cboRevision').val();
					var qty			 	= 	$(this).find('#txtQty').val();
					var price		 	= 	$(this).find('#txtPrice').val();
					var overCutPer	 	= 	$(this).find('#txtOverCutPer').val();
					var damagePer	 	= 	$(this).find('#txtDamagePer').val();
					var psDate		 	= 	$(this).find('.psDate').val();
					var delDate		 	= 	$(this).find('.delDate').val();
					var revNo		 	= 	$(this).find('#cboRevision').val();
					var lineNo		 	= 	$(this).find('#txtLineNo').val();
					var techGroup	 	= 	$(this).find('#cboTechGrp').val();
					
					var childQty	=0;
					$('.clsQty').each(function(){
						//alert(so+'=='+$(this).parent().parent().find('#parentSO').html());
						if(salesOrderId==$(this).parent().parent().find('#parentSO').html() && $(this).parent().parent().find('#txtCustoPOsplited').html() == $('#txtCustomerPO').val() )
							childQty +=childQty;
					});
				
					if(qty*price<=0)
					{
						hideWaiting();
						alert('Invalid Qty');
						
						return;
						//return 0;
					}/// end if
					//alert(1);
					//alert(salesOrderId);
					salesIds +=salesOrderId +',' 	;
					arr += '"salesOrderId":"'+			salesOrderId +'",' 	;
					arr += '"parentSoId":"'+			parentSoId +'",' 	;
					arr += '"salesOrderNo":"'+			URLEncode(salesOrderNo) +'",' 	;
					arr += '"graphicNo":"'+				URLEncode(graphicNo) +'",' 	;
					arr += '"styleNo":"'+				URLEncode(styleNo) +'",' 	;
					arr += '"sampleYear":"'+			sampleYear +'",' 	;
					arr += '"sampleNo":"'+				sampleNo +'",' 		;
					arr += '"printName":"'+				URLEncode(printName) +'",' 	;
					arr += '"combo":"'+					URLEncode(combo) +'",' 		;
					arr += '"revisionNo":"'+			revisionNo +'",' 	;
					arr += '"qty":"'+					qty +'",' 			;
					arr += '"price":"'+					price +'",' 		;
					arr += '"overCutPer":"'+			overCutPer +'",' 	;
					arr += '"damagePer":"'+				damagePer +'",' 	;
					arr += '"psDate":"'+				psDate +'",' 		;
					arr += '"revNo":"'+					revNo +'",' 		;
					arr += '"lineNo":"'+				lineNo +'",' 		;
					arr += '"delDate":"'+				delDate +'",' 		;
					arr += '"techGroup":"'+				techGroup +'",' 		;
					arr += '"childQty":"'+				childQty +'"' 		;
					arr +=  '},';
					//alert(salesIds);
			
				}
			});
			arr = arr.substr(0,arr.length-1);
			
				salesIds = salesIds.substr(0,salesIds.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr+'&salesIds='+salesIds;
		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basepath+"placeOrder-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json", 
			type:'POST', 
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmPlaceOrders #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//hideWaiting();
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						//loadCombo_frmSampleInfomations();
									hideWaiting();

						var t=setTimeout("alertx()",1000);
						$('#txtOrderNo').val(json.serialNo);
						$('#txtYear').val(json.year);
						window.location.href = '?q=427&orderNo='+json.serialNo+'&orderYear='+json.year;						//return;
					}
				},
			error:function(xhr,status){
						hideWaiting();
					$('#frmPlaceOrders #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
	
						hideWaiting();
   });
  
  
   $('#frmPlaceOrders #butNew').click(function(){
		document.location.href ='?q=427';
   });
	
///---------------------------	
  $('#frmPlaceOrders #butSendToApprove').click(function(){
	var requestType = '';
	if ($('#frmPlaceOrders').validationEngine('validate'))   
    { 
		var serialNo= $('#txtOrderNo').val();
		var year= $('#txtYear').val();
		var rowCount = document.getElementById('tblPOs').rows.length;
		var row = 0;
			
		if(serialNo==''){
			alert("There is no PO No to send for approval");
			return false;
		}
		else if(rowCount==2){
			alert("There is no Details to send for approval");
			return false;
		}
		showWaiting();

		var data = "requestType=sendToApproval";
		
			data+="&serialNo=" +	$('#txtOrderNo').val();
			data+="&year=" +	$('#txtYear').val();
			data+="&customerPO=" +	$('#txtCustomerPO').val();
		var url = basepath+"placeOrder-db-set.php";
     	var obj = $.ajax({
			url:url,			
			dataType: "json", 
			type:'POST', 
			data:data,
			async:false,			
			success:function(json){
					$('#frmPlaceOrders #butSendToApprove').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						hideWaiting();
						var t=setTimeout("alertx()",1000);
					}
				},
			error:function(xhr,status){
					 hideWaiting();
					 $('#frmPlaceOrders #butSendToApprove').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});	
		hideWaiting();
	}
  });

$('#frmPurchaseRequisitionNote #butNew').click(function(){
	$('#frmPurchaseRequisitionNote').get(0).reset();
	clearRows();
	$('#frmPurchaseRequisitionNote #cboDispatchTo').removeAttr('disabled');
	$('#frmPurchaseRequisitionNote #cboDispatchTo').val('');
	$('#frmPurchaseRequisitionNote #cboDispatchTo').focus();
	$('#frmPurchaseRequisitionNote #txtDispNo').val('');
	$('#frmPurchaseRequisitionNote #txtDispYear').val('');
	var currentTime = new Date();
	var month 		= currentTime.getMonth()+1 ;
	var day 		= currentTime.getDate();
	var year 		= currentTime.getFullYear();
	
	if(day<10)
		day		= '0'+day;
		
	if(month<10)
		month	= '0'+month;
		
	d=year+'-'+month+'-'+day;
	
	$('#frmPurchaseRequisitionNote #dtDate').val(d);
});

$('#frmPlaceOrders #butCopyPO').click(function() {
	$('#txtOrderNo').val(''); 
	$('#txtYear').val('');
	
	var currentTime = new Date();
	var month 		= currentTime.getMonth()+1 ;
	var day 		= currentTime.getDate();
	var year 		= currentTime.getFullYear();
	var	d			= year+'-'+month+'-'+day;
	$('#dtDate').val(d);
	$('#dtDate').prop( "disabled", false );
	
	$('#cboPoType').html('<option value="0">Other</option><option value="1" selected="selected">Dummy Order</option>');
	$('#cboPoType').val(0);
	$('#cboPoType').prop( "disabled", false );
	$('#childOrders').html('');
	$('#dummyOrders').html('');
	$('#cboPoType').val(0);
	$('#chkInstant').attr('checked', false);
	$('#divInstant').css('display', 'none');
	var errFlag=0;
	
	$('.cboRevision').each(function(){

		   var sampleNo		= $(this).parent().parent().find('#cboSampNo1').val();
		  var sampleYear	= $(this).parent().parent().find('#cboYear').val();
		  var revId 		= ($(this).val());
		  var printName		= $(this).parent().parent().find('#cboPart').val();
		  var comboId		= $(this).parent().parent().find('#cboCombo').val();
			$(this).parent().parent().attr('id','');
			$(this).parent().parent().find('#SO').html('');
		  
		  var url = basepath+"placeOrder-db-get.php?requestType=getPrice&sampleNo="+sampleNo+"&sampleYear="+sampleYear+"&revId="+revId+"&printName="+printName+"&comboId="+URLEncode(comboId);
		  var obj =  $.ajax({url:url,async:false});
		  $(this).parent().parent().find('#txtPrice').val(obj.responseText);
		  calRowAmount($(this).parent().parent());
		  if(obj.responseText !='' && obj.responseText >0)
		  	errFlag=errFlag+0;
		  else
		 	errFlag=1;  

	});
	  if(errFlag==0){
		 $("#tblPOs").find("th:nth-child(" + 2 + "), td:nth-child(" + 2 + ")").hide();
		 $("#tblPOs").find("th:nth-child(" + 3 + "), td:nth-child(" + 3 + ")").hide();
		 $("#tblPOs").find("th:nth-child(" + 4 + "), td:nth-child(" + 4 + ")").hide();
		//$('td:nth-child(2),th:ntr-child(2)').hide();
		//$('td:nth-child(2),th:ntr-child(3)').hide(); 
		//$('td:nth-child(2),th:ntr-child(4)').hide();
		alert('coppied successfully.')
	  }
	  else
		alert('failed to copy');  
	
	
	$('#butSave').show();	
});

$('#frmPlaceOrders #butReport').click(function() {
	
	if($('#txtPrnNo').val()!='')
		window.open('?q=896&orderNo='+$('#txtOrderNo').val()+'&orderYear='+$('#txtYear').val());	
	else
		alert("There is no PO No to view");
});

$('#frmPlaceOrders #butConfirm').click(function() {
	
	if($('#txtPrnNo').val()!='')
		window.open('?q=896&orderNo='+$('#txtOrderNo').val()+'&orderYear='+$('#txtYear').val()+'&approveMode='+1);	
	elsef
		alert("There is no PO No to confirm");
});

$('#frmPurchaseRequisitionNote #butNew').click(function() {
	
	$('#frmPurchaseRequisitionNote').get(0).reset();
	clearRows();
	$('#frmPurchaseRequisitionNote #cboDepartment').val('');
	$('#frmPurchaseRequisitionNote #cboDepartment').focus();
	$('#frmPurchaseRequisitionNote #txtPrnNo').val('');
	$('#frmPurchaseRequisitionNote #txtYear').val('');
	$('#frmPurchaseRequisitionNote #txtRemarks').val('');
	document.getElementById('chkInternal').checked=false;
	var currentTime = new Date();
	var month 		= currentTime.getMonth()+1 ;
	var day 		= currentTime.getDate();
	var year 		= currentTime.getFullYear();
	
	if(day<10)
		day			= '0'+day;
	
	if(month<10)
		month		= '0'+month;
		
	d=year+'-'+month+'-'+day;
	
	$('#frmPurchaseRequisitionNote #dtPrnDate').val(d);
	$('#frmPurchaseRequisitionNote #dtRequiredDate').val(d);
});

$('.clsChangeStyle').die('click').live('click',function(){
	popupWindow3('1');
	$('#popupContact1').load(basepath+'changeStyle.php',function(){

	});
});
	
$('#frmPlaceOrders #cboCompany').change(function(){
	var company 	= $('#frmPlaceOrders #cboCompany').val();
	var url 		= basepath+"placeOrder-db-get.php?requestType=loadLocations&company="+company;
	var httpobj 	= $.ajax({url:url,async:false})
	$('#frmPlaceOrders #cboCompLocation').html(httpobj.responseText);	
	});
});

function loadColor(url,ob){
	//alert(url);
	  var obj2 =  $.ajax({
		  		  url:url,
	 			  dataType: "json",  
				  async:false,
		success:function(json){
	 		 ob.parent().parent().find('#txtGroundColor').val(json.colorName);
	 		 ob.parent().parent().find('#txtBrand').val(json.brand);
			}
	  });
}

function addClickedRows1()
{
	var rowCount = document.getElementById('tblItemsPopup').rows.length;
	
	for(var i=1;i<rowCount;i++)
	{
		if((document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked==true) && (document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled==false)){
			var categoryID	= document.getElementById('tblItemsPopup').rows[i].cells[1].id;
			var category	= document.getElementById('tblItemsPopup').rows[i].cells[1].innerHTML;
			var subCatID	= document.getElementById('tblItemsPopup').rows[i].cells[2].id;
			var subCat		= document.getElementById('tblItemsPopup').rows[i].cells[2].innerHTML;
			var itemID		= document.getElementById('tblItemsPopup').rows[i].cells[3].id;
			var itemCode	= document.getElementById('tblItemsPopup').rows[i].cells[3].innerHTML;
			var idemDesc	= document.getElementById('tblItemsPopup').rows[i].cells[4].innerHTML;
					
			var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="images/del.png" width="15" height="15" /></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+categoryID+'">'+category+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatID+'">'+subCat+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemID+'">'+itemCode+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemID+'">'+idemDesc+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemID+'" ><input  id="'+itemID+'" class="validate[required,custom[integer],max[1000000]]" style="width:80px;text-align:center" type="text" value="0"/></td>';

			add_new_row('#frmPurchaseRequisitionNote #tblItems',content);

		$('.clsValidateBalQty').keyup(function(){
			var input=$(this).val();
			var balQty=$(this).closest('td').attr('id');
			if((input>balQty) || (input<0)){
			alert("Invalid Qty");
				$(this).val(balQty);
				return;
			}

		});
		}
	}	
	disablePopup();
}
//-------------------------------------
function alertx()
{
	//$('#frmPurchaseRequisitionNote #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmPurchaseRequisitionNote #butDelete').validationEngine('hide')	;
}

function closePopUp(){
	
}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblItems').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblItems').deleteRow(1);
			
	}
}
//------------------------------------------------------------
function checkAlreadySelected(){
	var rowCount = document.getElementById('tblItems').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			var itemNo = 	document.getElementById('tblItems').rows[i].cells[3].id;
		
			var rowCount1 = document.getElementById('tblItemsPopup').rows.length;
			for(var j=1;j<rowCount1;j++)
			{
				var itemNoP = 	document.getElementById('tblItemsPopup').rows[i].cells[3].id;
				
				if(itemNo==itemNoP){
					document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked=true;
					document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled=true;
				}
			}
	}
}
//---------------------------------------------------------------------
function addClickedRows(){
	
	if ($('#frmPlaceOrderPopup').validationEngine('validate'))   
    { 
		
	///VALIDATE ///
	var totalQty = 0;
	$('.sizeQty').each(function(){
		totalQty += parseFloat($(this).val());
		//alert(parseFloat($(this).val()));
	});
	
	var excess=parseFloat($('#excessQty').html());
	if(excess==''){
		excess=0;
	}
	//alert($('#txtsalesOrderNo2').val());excessQty
	//if(parseFloat($('#txtsalesOrderNo2').val())+excess < totalQty)
	if(parseFloat($('#txtsalesOrderNo2').val()) != totalQty)
	{
		alert('Total Qty not tally with order Qty');	
		return;
	}
		var data = "requestType=saveSizes";
		
			data+="&serialNo=" +		$('#txtOrderNo').val();
			data+="&year=" +			$('#txtYear').val();
			data+="&txtSYear=" +			$('#txtSYear').val();
			data+="&txtsampleNo=" +			$('#txtsampleNo').val();
			data+="&txtRevision=" +			$('#txtRevision').val();
			data+="&txtcboprint=" +			$('#txtcboprint').val();
			data+="&txtcombo1=" +			$('#txtcombo1').val();
			data+="&intSalesOrderId=" +	$('#spanSalesOrderNo').html();
			
			
			//////////////////// saving main grid part //////////////////////////////////////////
			//var rowCount = $('#tblMain >tbody >tr').length;
			var rowCount = document.getElementById('tblSizesPopup').rows.length;
			var row = 0;
			
			var arr="[";
			
			for(var i=1;i<rowCount-1;i++)
			{
					var size = 	document.getElementById('tblSizesPopup').rows[i].cells[1].childNodes[0].value;
					size	 = size.trim();
					var qty  = 	document.getElementById('tblSizesPopup').rows[i].cells[2].childNodes[0].value;
					
					if(qty>0){

					 arr += "{";
						arr += '"size":"'+		size  +'",' ;
						arr += '"qty":"'+		qty +'"' ;
						arr +=  '},';
						
					}
			}
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basepath+"placeOrder-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			type:'POST', 
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmPlaceOrderPopup #butAdd').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertx()",2000);
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmPlaceOrderPopup #butAdd').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
}
//------------------------------------------------------------------------------
function calculateTotalVal(){
/*	var rowCount = document.getElementById('tblPOs').rows.length;
	var row = 0;
	var totQty=0;
	var totValue=0;
	
	for(var i=1;i<rowCount-1;i++)
	{
		var qty= 	document.getElementById('tblPOs').rows[i].cells[8].childNodes[0].value;
		var price = 	document.getElementById('tblPOs').rows[i].cells[9].childNodes[0].value;
		totQty+=parseFloat(qty);
		totValue+=parseFloat(qty)*parseFloat(price);
	}
	$('#txtTotQty').val(totQty);
	$('#txtTotVal').val(totValue);*/
	var totalQty 		= 0;
	var totalValue 		= 0;
	var totalActual		= 0;
	var childQty		= parseFloat(getChildQty());
	
	$('.clsQty').each(function(){
		if((($('#cboPoType').val() == 1) && ($(this).parent().parent().find('#txtCustoPOsplited').val()==$('#frmPlaceOrders #txtCustomerPO').val())) || ($('#cboPoType').val() != 1))
			totalQty +=parseFloat($(this).val());
			
			//alert(parseFloat($(this).val()));
			totalActual	 +=parseFloat($(this).val());		
 	});
	
	$('.clsPrice').each(function(){
		if((($('#cboPoType').val() == 1) && ($(this).parent().parent().find('#txtCustoPOsplited').val()==$('#frmPlaceOrders #txtCustomerPO').val())) || ($('#cboPoType').val() != 1))
			totalValue +=parseFloat($(this).val()*parseFloat($(this).parent().parent().find('.clsQty').val()));
 	});
	
	totalQty=Math.round(totalQty*Math.pow(10,2))/Math.pow(10,2);
	totalValue=Math.round(totalValue*Math.pow(10,2))/Math.pow(10,2)
	
	$('#txtTotQty').val(totalQty);
	$('#txtTotVal').val(totalValue);
	
	if(($('#childFlag').html() !=1) && ($('#cboPoType').val() ==1)){
		$('#txtDummyQty').val(totalQty);
	}
	
	if(isNaN(totalActual))
		totalActual	=0;
	if(isNaN(childQty))
		childQty	=0;
		
	$('#txtActualTotQty').val(totalActual+childQty);
}
//------------------------------------------------------------

function loadAllGraphicNos()
{
	var url 	= 	basepath+"placeOrder-db-get.php?requestType=loadAllGraphicNos";
	var httpobj = 	$.ajax({url:url,async:false})
	var combo = httpobj.responseText;
	
	$('#tblPOs >tbody >tr').not(':last').not(':first').each(function(){
			var graphicNo= 	$(this).find('#cboStyle').val();
			$(this).find('#cboStyle').html(combo);
			$(this).find('#cboStyle').val(graphicNo);
	});	
}

//---------------------------------------------
function calculateTotalBalQty(){
	
	var totalQty 		= 0;
	$('.sizeQty').each(function(){
		totalQty +=parseFloat($(this).val());
	});
	
	var totOrder=parseFloat($('#txtsalesOrderNo2').val());
	$('#txtBalOrderQty').val(totOrder-totalQty);
}
//----------------------------------------------------------------
function removeClass(ele,cls) {
if (hasClass(ele,cls)) {
var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
ele.className=ele.className.replace(reg,' ');
}
}

function hasClass(ele,cls) {
return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
}
 
function addClass(ele,cls) {
if (!this.hasClass(ele,cls)) ele.className += " "+cls;
}
 //---------------------------------------
function copySizeWiseQty()
{
	var rowId	= $(this).parent().attr('id');
	var trid		= rowId - 1;
	
	var orderNo 	= $('#frmPlaceOrders #txtOrderNo').val();
	var orderYear 	= $('#frmPlaceOrders #txtYear').val();
	
	var btCoppy_obj	= $(this);
  
	if(orderNo=='' || orderYear=='')
	{
	//	$(this).validationEngine('showPrompt','Please save customer PO before copy size wise qty.','fail' /*'pass'*/);
		btCoppy_obj.validationEngine('showPrompt', 'Pleases save customer PO before copy size wise qty.','fail');
		return;
	}
	
 	var salesOrderIdFrm	= $('#tblPOs tr:eq('+trid+')').attr('id');
	var salesOrderIdTo	= $(this).parent().attr('id');
	var obj				= this;
	
	var url 			= basepath+"placeOrder-db-set.php?requestType=copySizeWiseQty";
    var obj = $.ajax({
					url:url,
					dataType: "json",  
					data: 'orderNo='+orderNo+'&orderYear='+orderYear+'&salesOrderIdFrm='+salesOrderIdFrm+'&salesOrderIdTo='+salesOrderIdTo,
					async:false,
					success:function(json){
							btCoppy_obj.validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							if(json.type=='pass')
							{
								var t=setTimeout("alertxCopy()",1000);
								return;
							}
					},
					error:function(xhr,status){
							
							btCoppy_obj.validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertxCopy()",3000);
					}		
				});
}
function alertxCopy()
{
	$('#frmPlaceOrders #tblPOs .clsCopy').validationEngine('hide')	;
}

function getChildQty(){
	
	var data='';
			data+="&orderNo	=" +	$('#txtOrderNo').val();
			data+="&year=" +		$('#txtYear').val();

	var url 	= 	basepath+"placeOrder-db-get.php?requestType=loadChildQtys"+data;
	var httpobj = 	$.ajax({url:url,async:false})
	return httpobj.responseText;
}

function saveParent1(childMsg){
	
  if(childMsg!='')
   alert(childMsg);

  	var requestType = '';
	 
 		showWaiting();
		
		//check order qty
		var boolInvalidQty = 0;
		$('.clsQty').each(function(){
			if($(this).val()=='' || $(this).val()<=0)
			{
				boolInvalidQty = 1;
				$(this).css('background-color','red');
				
			}	
		});
		//check price
		var boolInvalidPrice = 0;
		$('.clsPrice').each(function(){
			if($(this).val()=='' || $(this).val()<=0)
			{
				boolInvalidPrice = 1;
				$(this).css('background-color','red');
				
			}	
		});
		if(boolInvalidQty==0 && boolInvalidPrice==1)
		{
			alert('Invalid Qty or Price.');
			hideWaiting();
			return;
		}
	
		var instant	=0;
		if(($("#chkInstant").is(":checked")))
			instant=1;
		///////////////////
		var data = "requestType=save";
		
			data+="&serialNo=" +	$('#txtOrderNo').val();
			data+="&year=" +		$('#txtYear').val();
			data+="&customerPO=" +	URLEncode($('#txtCustomerPO').val());
			data+="&customer="	+	$('#cboCustomer').val();
			data+="&currency="			+	$('#cboCurrency').val();
			data+="&payTerm="			+	$('#cboPayTerm').val();
			data+="&poCompany="	+	$('#frmPlaceOrders #cboCompany').val();
			data+="&poLocation="	+	$('#cboCompLocation').val();
			data+="&location="	+	$('#cboLocation').val();
			data+="&remarks="	+	URLEncode($('#txtRemarks').val());
			data+="&delDateH="	+	$('#txtDelDate').val();
			data+="&dtDate="	+	$('#dtDate').val();
			data+="&contPerson="	+	URLEncode($('#txtContactPerson').val());
			data+="&marketer="	+	$('#cboMarketer').val();
			data+="&payMode="	+	$('#cboPaymentMode').val();
			data+="&poType="	+	$('#cboPoType').val();
			data+="&techniqueType="	+	0;
			data+="&instant="	+	instant;
	
			//////////////////// saving main grid part //////////////////////////////////////////
			//var rowCount = $('#tblMain >tbody >tr').length;
			var rowCount = document.getElementById('tblPOs').rows.length;
			var row = 0;
			
			var arr="[";
			var salesIds='';
			$('#tblPOs >tbody >tr').not(':last').not(':first').each(function(){
				//if(($('#txtCustomerPO').val()==$(this).find('#txtCustoPOsplited').val()) || ($('#cboPoType').val()!=1)){
				
					
					 arr += "{";
					var salesOrderId	= 	$(this).attr('id');
					var parentSoId	 	= 	($(this).find('#parentSO').html());
					//alert(salesOrderId);
					var salesOrderNo 	= 	$(this).find('#txtSheduleNo').val();
					var graphicNo 		= 	$(this).find('#cboStyle').val();
					var styleNo 		= 	$(this).find('#txtStyleNo').val();
					var sampleYear	 	= 	$(this).find('#cboYear').val();
					var sampleNo	 	= 	$(this).find('#cboSampNo1').val();
					var printName		= 	$(this).find('#cboPart').val();
					var combo		 	= 	$(this).find('#cboCombo').val();
					var revisionNo 		= 	$(this).find('#cboRevision').val();
					var qty			 	= 	$(this).find('#txtQty').val();
					var price		 	= 	$(this).find('#txtPrice').val();
					var overCutPer	 	= 	$(this).find('#txtOverCutPer').val();
					var damagePer	 	= 	$(this).find('#txtDamagePer').val();
					var psDate		 	= 	$(this).find('.psDate').val();
					var delDate		 	= 	$(this).find('.delDate').val();
					var revNo		 	= 	$(this).find('#cboRevision').val();
					var lineNo		 	= 	$(this).find('#txtLineNo').val();
					var techGroup	 	= 	$(this).find('#cboTechGrp').val();
					
					var childQty	=0;
					$('.clsQty').each(function(){
						//alert($('#txtCustomerPO').val()+'=='+$(this).parent().parent().find('#txtCustoPOsplited').val());
						if(salesOrderId==$(this).parent().parent().find('#parentSO').html() && $(this).parent().parent().find('#txtCustoPOsplited').val() == $('#txtCustomerPO').val() ){
							childQty +=$(this).val();
							//alert(childQty);
						}
					});
				
					if(qty > 0 && qty*price<=0)
					{
						hideWaiting();
						alert('Invalid Qty');
						
						return;
						//return 0;
					}/// end if
					//alert(1);
					//alert(salesOrderId);
					salesIds +=salesOrderId +',' 	;
					arr += '"salesOrderId":"'+			salesOrderId +'",' 	;
					arr += '"parentSoId":"'+			parentSoId +'",' 	;
					arr += '"salesOrderNo":"'+			URLEncode(salesOrderNo) +'",' 	;
					arr += '"graphicNo":"'+				URLEncode(graphicNo) +'",' 	;
					arr += '"styleNo":"'+				URLEncode(styleNo) +'",' 	;
					arr += '"sampleYear":"'+			sampleYear +'",' 	;
					arr += '"sampleNo":"'+				sampleNo +'",' 		;
					arr += '"printName":"'+				URLEncode(printName) +'",' 	;
					arr += '"combo":"'+					URLEncode(combo) +'",' 		;
					arr += '"revisionNo":"'+			revisionNo +'",' 	;
					arr += '"qty":"'+					qty +'",' 			;
					arr += '"price":"'+					price +'",' 		;
					arr += '"overCutPer":"'+			overCutPer +'",' 	;
					arr += '"damagePer":"'+				damagePer +'",' 	;
					arr += '"psDate":"'+				psDate +'",' 		;
					arr += '"revNo":"'+					revNo +'",' 		;
					arr += '"lineNo":"'+				lineNo +'",' 		;
					arr += '"delDate":"'+				delDate +'",' 		;
					arr += '"techGroup":"'+				techGroup +'",' 		;
					arr += '"childQty":"'+				childQty +'"' 		;
					arr +=  '},';
					//alert(salesIds);
			
			//	}
			});
			arr = arr.substr(0,arr.length-1);
			
				salesIds = salesIds.substr(0,salesIds.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr+'&salesIds='+salesIds;
		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basepath+"placeOrder-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json", 
			type:'POST', 
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmPlaceOrders #butSave').validationEngine('showPrompt', (childMsg+' '+json.msg),json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//hideWaiting();
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						//loadCombo_frmSampleInfomations();
						hideWaiting();

						var t=setTimeout("alertx()",1000);
						$('#txtOrderNo').val(json.serialNo);
						$('#txtYear').val(json.year);
						alert('The dummy order '+json.serialNo+'/'+json.year+' is '+json.msg);
						window.location.href = '?q=427&orderNo='+json.serialNo+'&orderYear='+json.year;						//return;
					}
				},
			error:function(xhr,status){
						hideWaiting();
					$('#frmPlaceOrders #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	
						hideWaiting();
 	
}


function saveParent(childMsg){
	
  if(childMsg!='')
   alert(childMsg);

  	var requestType = '';
	if ($('#frmPlaceOrders').validationEngine('validate'))   
    { 
		//alert('ok');
		showWaiting();
		
		//check order qty
		var boolInvalidQty = 0;
		$('.clsQty').each(function(){
			if($(this).val()=='' || $(this).val()<=0)
			{
				boolInvalidQty = 1;
				$(this).css('background-color','red');
				
			}	
		});
		//check price
		var boolInvalidPrice = 0;
		$('.clsPrice').each(function(){
			if($(this).val()=='' || $(this).val()<=0)
			{
				boolInvalidPrice = 1;
				$(this).css('background-color','red');
				
			}	
		});
		if(boolInvalidQty==0 && boolInvalidPrice==1)
		{
			alert('Invalid Qty or Price.');
			hideWaiting();
			return;
		}
		///////////////////
		var instant	=0;
		if(($("#chkInstant").is(":checked")))
			instant=1;
			
			
		var data = "requestType=save";
		
			data+="&serialNo=" +	$('#txtOrderNo').val();
			data+="&year=" +		$('#txtYear').val();
			data+="&customerPO=" +	URLEncode($('#txtCustomerPO').val());
			data+="&customer="	+	$('#cboCustomer').val();
			data+="&currency="			+	$('#cboCurrency').val();
			data+="&payTerm="			+	$('#cboPayTerm').val();
			data+="&poCompany="	+	$('#frmPlaceOrders #cboCompany').val();
			data+="&poLocation="	+	$('#cboCompLocation').val();
			data+="&location="	+	$('#cboLocation').val();
			data+="&remarks="	+	URLEncode($('#txtRemarks').val());
			data+="&delDateH="	+	$('#txtDelDate').val();
			data+="&dtDate="	+	$('#dtDate').val();
			data+="&contPerson="	+	URLEncode($('#txtContactPerson').val());
			data+="&marketer="	+	$('#cboMarketer').val();
			data+="&payMode="	+	$('#cboPaymentMode').val();
			data+="&poType="	+	$('#cboPoType').val();
			data+="&techniqueType="	+	0;
			data+="&instant="	+	instant;
		
			//////////////////// saving main grid part //////////////////////////////////////////
			//var rowCount = $('#tblMain >tbody >tr').length;
                      //  alert(data);
			var rowCount = document.getElementById('tblPOs').rows.length;
			var row = 0;
			
			var arr="[";
			var salesIds='';
			$('#tblPOs >tbody >tr').not(':last').not(':first').each(function(){
				//if(($('#txtCustomerPO').val()==$(this).find('#txtCustoPOsplited').val()) || ($('#cboPoType').val()!=1)){
				
					
					 arr += "{";
					//var salesOrderId	= 	$(this).attr('id');
					if(!$(this).attr('id'))
					var salesOrderId	= 	'';
					else
					var salesOrderId	= 	$(this).attr('id');
				
					var parentSoId	 	= 	($(this).find('#parentSO').html());
					//alert(salesOrderId);
					var salesOrderNo 	= 	$(this).find('#txtSheduleNo').val();
					var graphicNo 		= 	$(this).find('#cboStyle').val();
					var styleNo 		= 	$(this).find('#txtStyleNo').val();
					var sampleYear	 	= 	$(this).find('#cboYear').val();
					var sampleNo	 	= 	$(this).find('#cboSampNo1').val();
					var printName		= 	$(this).find('#cboPart').val();
					var combo		 	= 	$(this).find('#cboCombo').val();
					var revisionNo 		= 	$(this).find('#cboRevision').val();
					var ItemCode 		= 	$(this).find('#txtItemCode').val();
					var qty			 	= 	$(this).find('#txtQty').val();
					var price		 	= 	$(this).find('#txtPrice').val();
					var overCutPer	 	= 	$(this).find('#txtOverCutPer').val();
					var damagePer	 	= 	$(this).find('#txtDamagePer').val();
					var psDate		 	= 	$(this).find('.psDate').val();
					var delDate		 	= 	$(this).find('.delDate').val();
					var revNo		 	= 	$(this).find('#cboRevision').val();
					var lineNo		 	= 	$(this).find('#txtLineNo').val();
					var techGroup	 	= 	$(this).find('#cboTechGrp').val();
					
					if(qty > 0 && qty*price<=0)
					{
						hideWaiting();
						alert('Invalid Qty');
						
						return;
						//return 0;
					}/// end if
					//alert(1);
					//alert(salesOrderId);
					salesIds +=salesOrderId +',' 	;
					arr += '"salesOrderId":"'+			salesOrderId +'",' 	;
					arr += '"parentSoId":"'+			parentSoId +'",' 	;
					arr += '"salesOrderNo":"'+			URLEncode(salesOrderNo) +'",' 	;
					arr += '"graphicNo":"'+				URLEncode(graphicNo) +'",' 	;
					arr += '"styleNo":"'+				URLEncode(styleNo) +'",' 	;
					arr += '"sampleYear":"'+			sampleYear +'",' 	;
					arr += '"sampleNo":"'+				sampleNo +'",' 		;
					arr += '"printName":"'+				URLEncode(printName) +'",' 	;
					arr += '"combo":"'+					URLEncode(combo) +'",' 		;
					arr += '"revisionNo":"'+			revisionNo +'",' 	;
					arr += '"qty":"'+					qty +'",' 			;
					arr += '"price":"'+					price +'",' 		;
					arr += '"overCutPer":"'+			overCutPer +'",' 	;
					arr += '"damagePer":"'+				damagePer +'",' 	;
					arr += '"psDate":"'+				psDate +'",' 		;
					arr += '"revNo":"'+					revNo +'",' 		;
					arr += '"lineNo":"'+				lineNo +'",' 		;
					arr += '"delDate":"'+				delDate +'",' 		;
					arr += '"techGroup":"'+				techGroup +'"' 		;
					arr +=  '},';
					//alert(salesIds);
			
			//	}
			});
			arr = arr.substr(0,arr.length-1);
			
				salesIds = salesIds.substr(0,salesIds.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr+'&salesIds='+salesIds;
		///////////////////////////// save main infomations /////////////////////////////////////////
	//	var url = basepath+"placeOrder-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json", 
			type:'POST', 
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmPlaceOrders #butSave').validationEngine('showPrompt', (childMsg+' '+json.msg),json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//hideWaiting();
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						//loadCombo_frmSampleInfomations();
									hideWaiting();

						var t=setTimeout("alertx()",1000);
						$('#txtOrderNo').val(json.serialNo);
						$('#txtYear').val(json.year);
						alert('The order '+json.serialNo+'/'+json.year+' is '+json.msg);
						window.location.href = '?q=427&orderNo='+json.serialNo+'&orderYear='+json.year;						//return;
					}
					else{
 						$('#tblPOs >tbody >tr').not(':last').not(':first').each(function(){
							var salesOrderId	= 	$(this).attr('id');
							if(json.err_so==salesOrderId && json.err_so >0)
								($(this).find('#txtQty').val(json.err_qty));
						});
					}
				},
			error:function(xhr,status){
						hideWaiting();
					$('#frmPlaceOrders #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
	
						hideWaiting();
   ;
	
}

function saveSplit(){
  	errsplit	=0;
	msgAlert	='';
 		
	var requestType = '';
		showWaiting();
		
		//check order qty
		var boolInvalidQty = 0;
		$('.clsQty').each(function(){
			if($(this).val()=='' || $(this).val()<=0)
			{
				boolInvalidQty = 1;
				$(this).css('background-color','red');
				
			}	
		});
		//check price
		var boolInvalidPrice = 0;
		$('.clsPrice').each(function(){
			if($(this).val()=='' || $(this).val()<=0)
			{
				boolInvalidPrice = 1;
				$(this).css('background-color','red');
				
			}	
		});
		if(boolInvalidQty==0 && boolInvalidPrice==1)
		{
			alert('Invalid Qty or Price.');
			hideWaiting();
			return;
		}
			var instant	=0;
		//if(($("#chkInstant").is(":checked")))
		//	instant=1;
		///////////////////
		var data = "requestType=saveSplittedSalesOrder";
		
			data+="&serialNo=" +	$('#txtOrderNo').val();
			data+="&year=" +		$('#txtYear').val();
			data+="&customer="	+	$('#cboCustomer').val();
			data+="&currency="			+	$('#cboCurrency').val();
			data+="&payTerm="			+	$('#cboPayTerm').val();
			data+="&poCompany="	+	$('#frmPlaceOrders #cboCompany').val();
			data+="&poLocation="	+	$('#cboCompLocation').val();
			data+="&location="	+	$('#cboLocation').val();
			data+="&remarks="	+	URLEncode($('#txtRemarks').val());
			data+="&delDateH="	+	$('#txtDelDate').val();
			data+="&dtDate="	+	$('#dtDate').val();
			data+="&contPerson="	+	URLEncode($('#txtContactPerson').val());
			data+="&marketer="	+	$('#cboMarketer').val();
			data+="&payMode="	+	$('#cboPaymentMode').val();
			data+="&poType="	+	$('#cboPoType').val();
			data+="&techniqueType="	+	0;
			data+="&instant="	+	instant;
	
			var rowCount = document.getElementById('tblPOs').rows.length;
			var row = 0;
			
			var arr="[";
			var salesIds='';
			$('#tblPOs >tbody >tr').not(':last').not(':first').each(function(){
				if($('#txtCustomerPO').val()!=$(this).find('#txtCustoPOsplited').val()){
				
					
					 arr += "{";
					var salesOrderId	= 	$(this).attr('id');
					var parentSoId	 	= 	parseFloat($(this).find('#parentSO').html());
					var save_to_year 	= 	$(this).find('#cboSplitYear').val();
					var save_to_no	 	= 	$(this).find('#cboSplitNo').val();
					//alert(salesOrderId);
					var salesOrderNo 	= 	$(this).find('#txtSheduleNo').val();
					var customerPO	 	= 	$(this).find('#txtCustoPOsplited').val();
					var graphicNo 		= 	$(this).find('#cboStyle').val();
					var styleNo 		= 	$(this).find('#txtStyleNo').val();
					var sampleYear	 	= 	$(this).find('#cboYear').val();
					var sampleNo	 	= 	$(this).find('#cboSampNo1').val();
					var printName		= 	$(this).find('#cboPart').val();
					var combo		 	= 	$(this).find('#cboCombo').val();
					var revisionNo 		= 	$(this).find('#cboRevision').val();
					var qty			 	= 	$(this).find('#txtQty').val();
					var price		 	= 	$(this).find('#txtPrice').val();
					var overCutPer	 	= 	$(this).find('#txtOverCutPer').val();
					var damagePer	 	= 	$(this).find('#txtDamagePer').val();
					var psDate		 	= 	$(this).find('.psDate').val();
					var delDate		 	= 	$(this).find('.delDate').val();
					var revNo		 	= 	$(this).find('#cboRevision').val();
					var lineNo		 	= 	$(this).find('#txtLineNo').val();
					var techGroup	 	= 	$(this).find('#cboTechGrp').val();
					
					if(qty > 0 && qty*price<=0)
					{
						hideWaiting();
						alert('Invalid Qty');
						
						return;
						//return 0;
					}/// end if
					//alert(1);
					//alert(salesOrderId);
					salesIds +=salesOrderId +',' 	;
					arr += '"salesOrderId":"'+			salesOrderId +'",' 	;
					arr += '"parentSoId":"'+			parentSoId +'",' 	;
					arr += '"save_to_year":"'+			save_to_year +'",' 	;
					arr += '"save_to_no":"'+			save_to_no +'",' 	;
					arr += '"salesOrderNo":"'+			URLEncode(salesOrderNo) +'",' 	;
					arr += '"customerPO":"'+			URLEncode(customerPO) +'",' 	;
					arr += '"graphicNo":"'+				URLEncode(graphicNo) +'",' 	;
					arr += '"styleNo":"'+				URLEncode(styleNo) +'",' 	;
					arr += '"sampleYear":"'+			sampleYear +'",' 	;
					arr += '"sampleNo":"'+				sampleNo +'",' 		;
					arr += '"printName":"'+				URLEncode(printName) +'",' 	;
					arr += '"combo":"'+					URLEncode(combo) +'",' 		;
					arr += '"revisionNo":"'+			revisionNo +'",' 	;
					arr += '"qty":"'+					qty +'",' 			;
					arr += '"price":"'+					price +'",' 		;
					arr += '"overCutPer":"'+			overCutPer +'",' 	;
					arr += '"damagePer":"'+				damagePer +'",' 	;
					arr += '"psDate":"'+				psDate +'",' 		;
					arr += '"revNo":"'+					revNo +'",' 		;
					arr += '"lineNo":"'+				lineNo +'",' 		;
					arr += '"delDate":"'+				delDate +'",' 		;
					arr += '"techGroup":"'+				techGroup +'"' 		;
					arr +=  '},';
					//alert(salesIds);
			
				}
			});
			arr = arr.substr(0,arr.length-1);
			
				salesIds = salesIds.substr(0,salesIds.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr+'&salesIds='+salesIds;
		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basepath+"placeOrder-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json", 
			type:'POST', 
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					if(json.type=='pass')
					{
					//$('#frmPlaceOrders #butSaveChild').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						//hideWaiting();
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						//loadCombo_frmSampleInfomations();
						//hideWaiting();
						$('#childOrders').show();
						
						if(json.c==1)
							$('#childOrders').html(json.child);
						else
							$('#childOrders').html(json.child);
							
						$('#tblPOs >tbody >tr').not(':last').not(':first').each(function(){
							if($('#txtCustomerPO').val()!=$(this).find('#txtCustoPOsplited').val()){
								$(this).remove();
							}
						});
						
						

						//$('.cls_insertRow').show();
						$('#butSave').show();
						$('#butSaveChild').hide();
						$('#cboPoType').prop( "disabled", true );
						msgAlert	= json.msg
 						//save(json.msg);
						//window.location.href = '?q=427&orderNo='+json.serialNo+'&orderYear='+json.year;						//return;
					errsplit	=0;
					}
					else {
						errsplit	=1;
						$('#frmPlaceOrders #butSaveChild').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					}
				},
			error:function(xhr,status){
						hideWaiting();
					$('#frmPlaceOrders #butSaveChild').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
 	
	//alert(errsplit);
				hideWaiting();
	
}
var basepath	= 'presentation/customerAndOperation/bulk/placeOrder/addNew/';

$(document).ready(function() {
  $('#butComplete').click(function() {  
	var val = $.prompt('Are you sure you want to Complete this Orders ?',{
	buttons: { Ok: true, Cancel: false },
	callback: function(v,m,f){
	if(v)
	{  
		var requestType = '';
		showWaiting();
		
		var data = "requestType=orderComplete";
			
			var arr="[";
			$('.cbox').not(':first').each(function(){
				if($(this).attr('checked')){
					var poNo=$(this).parent().parent().find('td:eq(1)').html();
					var poYear=$(this).parent().parent().find('td:eq(2)').html();
					
					arr += "{";
					arr += '"poNo":"'+				poNo +'",' 		;
					arr += '"poYear":"'+			poYear +'"' 		;
					arr +=  '},';
				}
			});
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		var url = basepath+"orderCompletion-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json", 
			type:'POST', 
			data:data,
			async:false,			
			success:function(json){
					if(json.type=='pass')
					{
						alert("Completed Sucessfully");
						hideWaiting();
						var t=setTimeout("alertx()",1000);
						window.location.href = '?q=647';						//return;
					}
				},
			error:function(xhr,status){
						hideWaiting();
					var t=setTimeout("alertx()",3000);
				}		
			});
	
						hideWaiting();
						
	}
	}
	});
   });
   
});	

function alertx()
{
	$('#frmPurchaseRequisitionNote #butSave').validationEngine('hide')	;
}
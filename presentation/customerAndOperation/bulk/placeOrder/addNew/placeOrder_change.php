<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php 
$location 		= $sessions->getLocationId();
$intUser  		= $sessions->getUserId();

require_once "class/cls_permisions.php";		$objpermisionget		= new cls_permisions($db);

$exceedPOPermision 	= $objpermisionget->boolSPermision(5);

$programCode	= 'P0645';

$orderNo 		= (!isset($_REQUEST["orderNo"])?'':$_REQUEST["orderNo"]);
$orderYear 		= (!isset($_REQUEST["orderYear"])?'':$_REQUEST["orderYear"]);
$loginnedLocationId 	= $_SESSION['CompanyID'];
$x_tentitive = 0;

$sql = "SELECT 	mst_locations.intCompanyId
		FROM mst_locations WHERE mst_locations.intId='$loginnedLocationId'";

$result 	 = $db->RunQuery($sql);
$errorFlg	 = 0;
$row		 = mysqli_fetch_array($result);
$loginMainCompany 	 = $row['intCompanyId'];

if($orderYear==''){
	$orderYear=date('Y');
}

   $sql = "SELECT
trn_orderheader.strCustomerPoNo ,intCustomer,intCustomerLocation, TENTATIVE
FROM trn_orderheader INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
WHERE
trn_orderheader.intOrderNo =  '$orderNo' AND
trn_orderheader.intOrderYear =  '$orderYear' AND mst_locations.intCompanyId = '$loginMainCompany'";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);

$x_poNo 	= $row['strCustomerPoNo'];
$x_cusId 	= $row['intCustomer'];
$x_cusLocId 	= $row['intCustomerLocation'];
$x_tentitive = $row['TENTATIVE'];

$editMode=loadEditMode($programCode,$intUser);
?>

<title>Change Order Information</title>

<form id="frmChangeOrder" name="frmChangeOrder" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL" style="width:600">
		  <div class="trans_text">Change Order Information</div>
		  <table width="576" border="0" align="center" bgcolor="#FFFFFF">
    <td width="570"><table width="100%" border="0">
    <tr><td><table width="100%">
      <tr>
        <td>
          <table  bgcolor="#E8FED8" class="tableBorder_allRound"  width="100%" border="0" cellpadding="0" cellspacing="0"  > 
  <tr>
  <td width="1%">&nbsp;</td>
    <td width="8%" height="22" class="normalfnt">Year</td>
    <td width="17%" class="normalfnt"><input  type="text" value="<?php echo $orderYear; ?>" style="width:60px;text-align:center" name="txtOrderYear" id="txtOrderYear" /></td>
    <td width="11%" class="normalfnt">Order No</td>
    <td width="35%" class="normalfnt"><input type="text" value="<?php echo $orderNo; ?>" style="width:100px;text-align:center" name="txtOrderNo" id="txtOrderNo" /></td>
    <td width="28%" class="normalfnt"><img src="images/Tview.jpg" width="92" height="24" id="butSearch" name="butSearch"  class="mouseover"/></td>
  </tr>            </table><table style="margin-top:5px"  bgcolor="" class="tableBorder_allRound"  width="100%" border="0" cellpadding="0" cellspacing="0"  >
    <tr>
      <td class="normalfnt">&nbsp;</td>
      <td height="22" class="normalfnt">&nbsp;</td>
      <td class="normalfnt">&nbsp;</td>
      <td class="normalfnt">&nbsp;</td>
      <td class="normalfnt">&nbsp;</td>
    <tr>
      <td width="1%" class="normalfnt">&nbsp;</td>
      <td width="19%" height="22" class="normalfnt">Customer PO No</td>
      <td width="24%" class="normalfnt"><input name="txtCustomerPO" type="text" <?php if ($x_tentitive == 2){ ?>disabled<?php } ?> class="validate[required , maxSize[50]] normalfnt"  id="txtCustomerPO" style="width:103px;text-align:left" value="<?php echo htmlentities($x_poNo); ?>"/></td>
      <td width="22%" class="normalfnt">Customer Location</td>
      <td width="34%" class="normalfnt"><select class="validate[required]" name="cboLocation" id="cboLocation" style="width:150px">
        <?php
				
					$sql = "SELECT
								mst_customer_locations.intLocationId,
								mst_customer_locations_header.strName
							FROM
								mst_customer_locations
								Inner Join mst_customer_locations_header ON mst_customer_locations.intLocationId = mst_customer_locations_header.intId
							WHERE
								mst_customer_locations.intCustomerId =  '$x_cusId' order by strName";
					$result = $db->RunQuery($sql);
					echo  "<option value=\"\"></option>";
					while($row=mysqli_fetch_array($result))
					{
						if($row['intLocationId']==$x_cusLocId)
							echo "<option  selected=\"selected\" value=\"".$row['intLocationId']."\">".$row['strName']."</option>";
						else
							echo "<option value=\"".$row['intLocationId']."\">".$row['strName']."</option>";
					}	
				
			?>
      </select></td>
      
      
      <tr>
        <td class="normalfnt">&nbsp;</td>
        <td height="22" class="normalfnt">&nbsp;</td>
        <td class="normalfnt">&nbsp;</td>
        <td class="normalfnt">&nbsp;</td>
        <td class="normalfnt">&nbsp;</td>
      </table>
          </td>
      </tr>
      </table></td></tr>
      <tr>
        <td><div style="width:560px;height:150px;overflow:scroll" class="tableBorder_allRound" >
          <table width="540" class="grid" id="tblMain" >
            <tr class="gridHeader">
              <td width="22%" >Sales Order ID</td>
              <td width="78%" >Sales Order No</td>
              </tr>
              <?php
				$result=loadDetails($orderNo,$orderYear);
				while($row=mysqli_fetch_array($result))
				{
					$salesOrderId=$row['intSalesOrderId'];
					$salesOrderNo=$row['strSalesOrderNo'];
					
			  ?>
<tr class="normalfnt">
  <td align="center" bgcolor="#FFFFFF" id="<?php echo $salesOrderId; ?>" class="salesOrderId"><?php echo $salesOrderId; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $salesOrderNo; ?>" ><input name="salesOrderNo" type="text" class="validate[required] normalfnt salesOrderNo"  id="salesOrderNo" style="width:183px;text-align:left" value="<?php echo $salesOrderNo ?>"/></td>
            </tr>       
             <?php
				}
			  ?>  
         </table>
        </div></td></tr>
      </tr>
<?php

?>
      <tr>
        <td align="center" class="tableBorder_allRound"><img src="images/Tnew.jpg" width="92" height="24" id="butNew" name="butNew"  class="mouseover"/>
          <?php if($editMode==1)
		  { 
		  ?><img src="images/Tsave.jpg" width="92" height="24" id="butSave" name="butSave"  class="mouseover"/><?php 
		  
		  } ?><a href="main.php"><img src="images/Tclose.jpg" width="92" height="24"  class="mouseover"/></a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<div style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
<?php
//--------------------------------------------------------------
function loadDetails($orderNo,$orderYear){
		global $db; 
	  	$sql = "SELECT
				trn_orderdetails.intSalesOrderId, 
				trn_orderdetails.strSalesOrderNo   
				FROM
				trn_orderdetails
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' 
				ORDER BY 
				trn_orderdetails.intSalesOrderId ASC 
				";

			return $result = $db->RunQuery($sql);
	}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
 	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
				 $editMode=1;
		 }
			 
	return $editMode;
}

?>

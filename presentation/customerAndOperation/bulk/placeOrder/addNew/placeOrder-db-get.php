<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	///
	/////////// type of print load part /////////////////////
	if($requestType=='loadCustLocations')
	{
		$customerId 	= $_REQUEST['customerId'];
		$sql = "SELECT
					mst_customer_locations.intLocationId,
					mst_customer_locations_header.strName
				FROM
					mst_customer_locations
					Inner Join mst_customer_locations_header ON mst_customer_locations.intLocationId = mst_customer_locations_header.intId
				WHERE
					mst_customer_locations.intCustomerId =  '$customerId'";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intLocationId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
        
        
        elseif ($requestType=='checkCustomer') {
            $customerId = $_REQUEST['customerId'];
           //echo $customerId;
            $sql_block="select ACTIVE_BULK from mst_customer where mst_customer.intId= $customerId";
           // echo $sql_block;
                        $result_block=$db->RunQuery($sql_block);
                        $row_block=mysqli_fetch_array($result_block);
                       // echo $result_block;
                          $bulk=$row_block['ACTIVE_BULK'];
                          //echo $sample; 
                          if( $bulk==1){
                             $response['type']          = "fail";
                            }
                            else{
                              $response['type']          = "pass";
                            }
            echo json_encode($response);
        }
	else if($requestType=='loadLocations')
	{
		$company  	= $_REQUEST['company'];
		$serialNo	= $_REQUEST['serialNo'];
		$year  		= $_REQUEST['year'];
		$sql = "SELECT
				mst_locations.intId,
				mst_locations.strName
				FROM mst_locations 
				LEFT JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId
				WHERE 
				mst_locations.intStatus = '1' AND 
				mst_locations.intCompanyId =  '$company' AND
				(mst_plant.MAIN_CLUSTER_ID <> 1 OR (mst_plant.MAIN_CLUSTER_ID=1 AND mst_locations.HEAD_OFFICE_FLAG<>1))
				
				union 

				SELECT
								mst_locations.intId,
								mst_locations.strName
								FROM 
								trn_orderheader 
								INNER JOIN mst_locations on  trn_orderheader.intLocationId=mst_locations.intId
								WHERE 
								trn_orderheader.intOrderNo = '$serialNo' and 
								trn_orderheader.intOrderYear = '$year' and
								mst_locations.intStatus = '1'   ";
		
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadStyles')
	{ 
		$year 	= $_REQUEST['year'];
		if($year==''){
		$year=date('Y');
		}
		$customerId 	= $_REQUEST['customerId'];
		$sql = "SELECT DISTINCT
				trn_sampleinfomations.strGraphicRefNo
				FROM
				trn_sampleinfomations
				left JOIN trn_sampleinfomations_combo_print_approvedby ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_combo_print_approvedby.SAMPLE_NO AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_combo_print_approvedby.SAMPLE_YEAR AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_combo_print_approvedby.REVISION
				WHERE
				(trn_sampleinfomations.intStatus = 1 OR trn_sampleinfomations_combo_print_approvedby.`STATUS` = 1) AND 
				trn_sampleinfomations.intSampleYear = '$year'   
				
				GROUP BY
				trn_sampleinfomations.strGraphicRefNo
				order by strGraphicRefNo
				";
		/*
		 AND 
				trn_sampleinfomations.intCustomer =  '$customerId'
				
				REMOVE ABOW LINE BY ROSHAN (2014-01-02). 
		*/
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strGraphicRefNo']."\">".$row['strGraphicRefNo']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadAllGraphicNos')
	{
		$sql = "SELECT DISTINCT
				trn_sampleinfomations.strGraphicRefNo
				FROM
				trn_sampleinfomations
				LEFT JOIN trn_sampleinfomations_combo_print_approvedby ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_combo_print_approvedby.SAMPLE_NO AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_combo_print_approvedby.SAMPLE_YEAR AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_combo_print_approvedby.REVISION
				WHERE
				(trn_sampleinfomations.intStatus = 1   OR 
				trn_sampleinfomations_combo_print_approvedby.`STATUS` = 1 )
				GROUP BY
				trn_sampleinfomations.strGraphicRefNo
				order by strGraphicRefNo
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strGraphicRefNo']."\">".$row['strGraphicRefNo']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadSampleNos')
	{
		$style 	= $_REQUEST['style'];
		$year 	= $_REQUEST['year'];
		$sql = "SELECT DISTINCT
				trn_sampleinfomations.intSampleNo
				FROM
				trn_sampleinfomations
				LEFT JOIN trn_sampleinfomations_combo_print_approvedby ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_combo_print_approvedby.SAMPLE_NO AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_combo_print_approvedby.SAMPLE_YEAR AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_combo_print_approvedby.REVISION
				WHERE
				(trn_sampleinfomations.intStatus = 1 OR
				trn_sampleinfomations_combo_print_approvedby.`STATUS` = 1 ) AND
				trn_sampleinfomations.intSampleYear = '$year'   AND
				trn_sampleinfomations.strGraphicRefNo =  '$style'  
				GROUP BY
				trn_sampleinfomations.intSampleNo
				order by intSampleNo
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intSampleNo']."\">".$row['intSampleNo']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadParts')
	{
		$sampleYear 		= $_REQUEST['year'];
		$sampNo 			= $_REQUEST['sampNo'];
		$maxRevNo			= get_max_revision_no($sampNo,$sampleYear);
		
		$sql = "SELECT DISTINCT
					mst_part.strName,mst_part.intId as partId,
					trn_sampleinfomations_printsize.strPrintName
					FROM
					trn_sampleinfomations_printsize
					INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_printsize.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_sampleinfomations_printsize.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_sampleinfomations_printsize.intRevisionNo = trn_sampleinfomations.intRevisionNo
					INNER JOIN mst_part ON mst_part.intId = trn_sampleinfomations_printsize.intPart
					LEFT JOIN trn_sampleinfomations_combo_print_approvedby ON trn_sampleinfomations_printsize.intSampleNo = trn_sampleinfomations_combo_print_approvedby.SAMPLE_NO AND trn_sampleinfomations_printsize.intSampleYear = trn_sampleinfomations_combo_print_approvedby.SAMPLE_YEAR AND trn_sampleinfomations_printsize.intRevisionNo = trn_sampleinfomations_combo_print_approvedby.REVISION AND trn_sampleinfomations_printsize.strPrintName = trn_sampleinfomations_combo_print_approvedby.PRINT 
					WHERE
					(trn_sampleinfomations.intStatus = 1 OR
					trn_sampleinfomations_combo_print_approvedby.`STATUS` = 1 ) AND
					trn_sampleinfomations_printsize.intSampleNo = '$sampNo' AND
					trn_sampleinfomations_printsize.intSampleYear = '$sampleYear' AND
					trn_sampleinfomations_printsize.intRevisionNo = '$maxRevNo' 
					GROUP BY
					trn_sampleinfomations_printsize.strPrintName,
					trn_sampleinfomations_printsize.intPart
					ORDER BY
					mst_part.strName ASC
				";
		$result = $db->RunQuery($sql);
		$x= "<option value=\"\"></option>";	
		while($row=mysqli_fetch_array($result))
		{
			$x.= "<option value=\"".$row['partId'].'/'.$row['strPrintName']."\">".$row['strName'].' - '.$row['strPrintName']."</option>";	
		}
		$response['parts'] = $x;
		////////////////////////////////////////////////////////////
		$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strComboName
				FROM
				trn_sampleinfomations_details
				INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo
				LEFT JOIN trn_sampleinfomations_combo_print_approvedby ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_combo_print_approvedby.SAMPLE_NO AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_combo_print_approvedby.SAMPLE_YEAR AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_combo_print_approvedby.REVISION AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_combo_print_approvedby.COMBO 
AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_combo_print_approvedby.PRINT

				WHERE
				(trn_sampleinfomations.intStatus = 1 OR
				trn_sampleinfomations_combo_print_approvedby.`STATUS` = 1 ) AND
				trn_sampleinfomations_details.intSampleNo = '$sampNo' AND
				trn_sampleinfomations_details.intSampleYear = '$sampleYear' AND
				trn_sampleinfomations_details.intRevNo = '$maxRevNo' 
				GROUP BY
				trn_sampleinfomations_details.strComboName
				";
		$result = $db->RunQuery($sql);
		$y= "<option value=\"\"></option>";	
		while($row=mysqli_fetch_array($result))
		{
			$y.= "<option value=\"".$row['strComboName']."\">".$row['strComboName']."</option>";	
		}
		$response['combos'] = $y;
/*		////////////////////////////////////////////////////////////
		$sql = "SELECT DISTINCT
				trn_sampleinfomations_details.intGroundColor,
				mst_colors_ground.strName
				FROM
					trn_sampleinfomations_details 
					Inner Join mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
				WHERE
					trn_sampleinfomations_details.intSampleNo =  '$sampNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear'
				";
		$result = $db->RunQuery($sql);
		$g= "<option value=\"\"></option>";	
		while($row=mysqli_fetch_array($result))
		{
			$g.= "<option value=\"".$row['intGroundColor']."\">".$row['strName']."</option>";	
		}
		$response['gColors'] = $g;
*/		////////////////////////////////////////////////////////////
		$sql = "SELECT DISTINCT
					trn_sampleinfomations.intRevisionNo,strStyleNo
				FROM
				trn_sampleinfomations
				INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_details.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_details.intRevNo
				LEFT JOIN trn_sampleinfomations_combo_print_approvedby ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_combo_print_approvedby.SAMPLE_NO AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_combo_print_approvedby.SAMPLE_YEAR AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_combo_print_approvedby.REVISION
AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_combo_print_approvedby.COMBO 
AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_combo_print_approvedby.PRINT
				WHERE
				(trn_sampleinfomations.intStatus = 1 OR
				trn_sampleinfomations_combo_print_approvedby.`STATUS` = 1 ) AND
				trn_sampleinfomations.intSampleNo = '$sampNo' AND
				trn_sampleinfomations.intSampleYear = '$sampleYear' AND
				trn_sampleinfomations.intRevisionNo = '$maxRevNo'  
				group by trn_sampleinfomations.intRevisionNo,trn_sampleinfomations.strStyleNo

				";
		$result = $db->RunQuery($sql);
		$y= "<option value=\"\"></option>";	
		while($row=mysqli_fetch_array($result))
		{
			$styleNo = $row['strStyleNo'];
			$y.= "<option value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";	
		}
		$response['revision'] 	= $y;
		$response['styleNo'] 	= $styleNo;
		////////////////////////////////////////////////////////////
		
		echo json_encode($response);
	}
	else if($requestType=='loadGroundColors')
	{
		$sampleYear 		= $_REQUEST['sampleYear'];
		$sampNo 			= $_REQUEST['sampleNo'];
		$combo 			= $_REQUEST['combo'];
		$revNo 			= $_REQUEST['revNo'];
		
		$printName	= explode('/',$_REQUEST['printName']);
		$printName = $printName[1];
		$part = $printName[0];
	
		$sql = "SELECT DISTINCT
				trn_sampleinfomations_details.intGroundColor,
				mst_colors_ground.strName
				FROM
				trn_sampleinfomations
				INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_details.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_details.intRevNo
				LEFT JOIN trn_sampleinfomations_combo_print_approvedby ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_combo_print_approvedby.SAMPLE_NO AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_combo_print_approvedby.SAMPLE_YEAR AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_combo_print_approvedby.REVISION AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_combo_print_approvedby.COMBO 
AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_combo_print_approvedby.PRINT

				WHERE
				(trn_sampleinfomations.intStatus = 1 OR
				trn_sampleinfomations_combo_print_approvedby.`STATUS` = 1 ) AND
				trn_sampleinfomations_details.intSampleNo =  '$sampNo' AND
				trn_sampleinfomations_details.intSampleYear =  '$sampleYear'  AND
				trn_sampleinfomations_details.strPrintName =  '$printName' AND
				trn_sampleinfomations_details.strComboName =  '$combo' AND
				trn_sampleinfomations_details.intRevNo =  '$revNo'  
				group by 
				trn_sampleinfomations_details.intGroundColor,
				mst_colors_ground.strName
					
					
					
				";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		 $g= $row['strName'];	
		if($revNo=='') 
		$g='';	
			
		echo $g; 
		//$response['gColors'] = $g;
		
		//echo json_encode($response);
	}
	elseif($requestType=='loadApprovedCombo')
	{
		$printName 	= $_REQUEST['printName'];
		$sampleNo  	= $_REQUEST['sampleNo'];
		$sampleYear = $_REQUEST['sampleYear'];
		
		$sql = "SELECT DISTINCT
					trn_sampleinfomations_customer_approved.strCombo,
					trn_sampleinfomations_customer_approved.intRevisionNo
				FROM trn_sampleinfomations_customer_approved
				WHERE
					trn_sampleinfomations_customer_approved.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_customer_approved.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_customer_approved.strPrintName =  '$printName'
				";
		$result = $db->RunQuery($sql);
		$x = false;
		while($row=mysqli_fetch_array($result))
		{
			$x = true;
			$response['combo'] 	= $row['strCombo'];
			$response['revNo'] 	= '<span style="color:#0033FF" >'.$row['intRevisionNo'].'</span>';
		}
		
		if(!$x)
		{
			$response['combo'] 	= '';
			$response['revNo'] 	= '<span style="color:#009900" >Pending Approval</span>';
			$response['sql']    = $sql;
		}
		echo json_encode($response);
	}
	else if($requestType=='loadCombos')
	{
		$style 	= $_REQUEST['style'];
		$year 	= $_REQUEST['year'];
		$sampNo 	= $_REQUEST['sampNo'];
		$maxRevNo	= get_max_revision_no($sampNo,$year);
 		
		$sql = "SELECT DISTINCT
				trn_sampleinfomations_combos.strComboName
				FROM
				trn_sampleinfomations_combos
				INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_combos.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_sampleinfomations_combos.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_sampleinfomations_combos.intRevisionNo = trn_sampleinfomations.intRevisionNo
				LEFT JOIN trn_sampleinfomations_combo_print_approvedby ON trn_sampleinfomations_combos.intSampleNo = trn_sampleinfomations_combo_print_approvedby.SAMPLE_NO AND trn_sampleinfomations_combos.intSampleYear = trn_sampleinfomations_combo_print_approvedby.SAMPLE_YEAR AND trn_sampleinfomations_combos.intRevisionNo = trn_sampleinfomations_combo_print_approvedby.REVISION AND trn_sampleinfomations_combos.strComboName = trn_sampleinfomations_combo_print_approvedby.COMBO 

				WHERE
				(trn_sampleinfomations.intStatus = 1 OR
				trn_sampleinfomations_combo_print_approvedby.`STATUS` = 1 ) AND
				trn_sampleinfomations_combos.intSampleNo = '$sampNo' AND
				trn_sampleinfomations_combos.intSampleYear = '$year' AND
				trn_sampleinfomations_combos.intRevisionNo = '$maxRevNo' 
				GROUP BY 
				trn_sampleinfomations_combos.strComboName 
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strComboName']."\">".$row['strComboName']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadPlacement')
	{
		$style 	= $_REQUEST['style'];
		$year 	= $_REQUEST['year'];
		$sampNo 	= $_REQUEST['sampNo'];
		$maxRevNo	= get_max_revision_no($sampNo,$year);
		
		$combo 	= $_REQUEST['combo'];
		 $sql = "SELECT
 distinct trn_sampleinfomations_printsize.intPart,
mst_part.strName
FROM
trn_sampleinfomations_combos
Inner Join trn_sampleinfomations_printsize ON trn_sampleinfomations_combos.intSampleNo = trn_sampleinfomations_printsize.intSampleNo AND trn_sampleinfomations_combos.intSampleYear = trn_sampleinfomations_printsize.intSampleYear AND trn_sampleinfomations_combos.intRevisionNo = trn_sampleinfomations_printsize.intRevisionNo
Inner Join mst_part ON trn_sampleinfomations_printsize.intPart = mst_part.intId
WHERE
trn_sampleinfomations_combos.intSampleNo =  '$sampNo' AND
trn_sampleinfomations_combos.intSampleYear =  '$year' AND
trn_sampleinfomations_combos.strComboName =  '$combo' AND
trn_sampleinfomations_combos.intRevisionNo =  '$maxRevNo'
";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intPart']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadPlacement111')
	{
		$style 	= $_REQUEST['style'];
		$year 	= $_REQUEST['year'];
		$sampNo 	= $_REQUEST['sampNo'];
		$combo 	= $_REQUEST['combo'];
		 $sql = "SELECT
 distinct trn_sampleinfomations_printsize.intPart,
mst_part.strName
FROM
trn_sampleinfomations_combos
Inner Join trn_sampleinfomations_printsize ON trn_sampleinfomations_combos.intSampleNo = trn_sampleinfomations_printsize.intSampleNo AND trn_sampleinfomations_combos.intSampleYear = trn_sampleinfomations_printsize.intSampleYear AND trn_sampleinfomations_combos.intRevisionNo = trn_sampleinfomations_printsize.intRevisionNo
Inner Join mst_part ON trn_sampleinfomations_printsize.intPart = mst_part.intId
WHERE
trn_sampleinfomations_combos.intSampleNo =  '$sampNo' AND
trn_sampleinfomations_combos.intSampleYear =  '$year' AND
trn_sampleinfomations_combos.strComboName =  '$combo'
";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intPart']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadCombo')
	{
		$sampNo  = $_REQUEST['sampleNo'];
		$year  = $_REQUEST['sampleYear'];
		$printNameArr	= explode('/',$_REQUEST['printName']);
		$printName = $printNameArr[1];
		$part = $printNameArr[0];
		$maxRevNo	= get_max_revision_no($sampNo,$year);

		
		    $sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strComboName
				FROM
					trn_sampleinfomations_details
					INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo
					LEFT JOIN trn_sampleinfomations_combo_print_approvedby ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_combo_print_approvedby.SAMPLE_NO AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_combo_print_approvedby.SAMPLE_YEAR AND 
					trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_combo_print_approvedby.REVISION AND  trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_combo_print_approvedby.PRINT  AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_combo_print_approvedby.COMBO
				
				WHERE
					(trn_sampleinfomations.intStatus = 1 OR
					trn_sampleinfomations_combo_print_approvedby.`STATUS` = 1 ) AND
					trn_sampleinfomations_details.intSampleNo =  '$sampNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$year' AND 
					trn_sampleinfomations_details.strPrintName='$printName'  AND 
 					trn_sampleinfomations_details.intRevNo='$maxRevNo'   
				GROUP BY trn_sampleinfomations_details.strComboName";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strComboName']."\">".$row['strComboName']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadRevNo')
	{
		$sampleNo  = $_REQUEST['sampleNo'];
		$sampleYear  = $_REQUEST['sampleYear'];
		$printNameArr	= explode('/',$_REQUEST['printName']);
		$printName = $printNameArr[1];
		$part = $printNameArr[0];
		$combo  = $_REQUEST['combo'];
		$maxRevNo	= get_max_revision_no($sampleNo,$sampleYear);
		
		   $sql = "SELECT DISTINCT
					trn_sampleinfomations.intRevisionNo,strStyleNo
				FROM trn_sampleinfomations
					INNER JOIN trn_sampleinfomations_details 
					ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo 
					AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_details.intSampleYear 
					AND trn_sampleinfomations.intRevisionNo=trn_sampleinfomations_details.intRevNo 
					LEFT JOIN trn_sampleinfomations_combo_print_approvedby ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_combo_print_approvedby.SAMPLE_NO AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_combo_print_approvedby.SAMPLE_YEAR AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_combo_print_approvedby.REVISION AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_combo_print_approvedby.COMBO AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_combo_print_approvedby.PRINT
				WHERE
					(trn_sampleinfomations.intStatus = 1 OR
					trn_sampleinfomations_combo_print_approvedby.`STATUS` = 1 ) AND
					trn_sampleinfomations.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations.intSampleYear =  '$sampleYear' AND 
					trn_sampleinfomations_details.strPrintName =  '$printName' AND
					trn_sampleinfomations_details.strComboName =  '$combo' AND 
 					trn_sampleinfomations_details.intRevNo =  '$maxRevNo'  
GROUP BY trn_sampleinfomations.intRevisionNo,trn_sampleinfomations.strStyleNo
					
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
		}
		echo $html;
	}
	else if($requestType=='getPrice')
	{
		$sampleNo  	= $_REQUEST['sampleNo'];
		$sampleYear	= $_REQUEST['sampleYear'];
		$revId		= $_REQUEST['revId'];
		$printName	= explode('/',$_REQUEST['printName']);
		$printName = $printName[1];
		$comboId	= $_REQUEST['comboId'];	
		
		$sql = "SELECT
					trn_sampleinfomations_prices.dblPrice, 
					costing_sample_routing_process.COST,
					costing_sample_header.QTY_PRICES
					FROM trn_sampleinfomations_prices
					LEFT JOIN costing_sample_routing_process ON
					costing_sample_routing_process.SAMPLE_NO=trn_sampleinfomations_prices.intSampleNo AND costing_sample_routing_process.SAMPLE_YEAR=trn_sampleinfomations_prices.intSampleYear
					AND costing_sample_routing_process.REVISION=trn_sampleinfomations_prices.intRevisionNo AND costing_sample_routing_process.COMBO=trn_sampleinfomations_prices.strCombo
					AND costing_sample_routing_process.PRINT=trn_sampleinfomations_prices.strPrintName AND costing_sample_routing_process.PROCESS_ID=14
					LEFT JOIN  costing_sample_header ON 
                    costing_sample_header.SAMPLE_NO = trn_sampleinfomations_prices.intSampleNo AND
                    costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations_prices.intSampleYear AND 
                    costing_sample_header.REVISION = trn_sampleinfomations_prices.intRevisionNo AND 
                    costing_sample_header.COMBO = trn_sampleinfomations_prices.strCombo AND 
                    costing_sample_header.PRINT = trn_sampleinfomations_prices.strPrintName
				WHERE
					trn_sampleinfomations_prices.intSampleNo 		=  '$sampleNo' AND
					trn_sampleinfomations_prices.intSampleYear 		=  '$sampleYear' AND
					trn_sampleinfomations_prices.intRevisionNo 		=  '$revId' AND
					trn_sampleinfomations_prices.strCombo 			=  '$comboId' AND
					trn_sampleinfomations_prices.strPrintName 		=  '$printName'
				";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
        $prices['qtyPricesEnabled'] = 0;
		if($row['COST'] != null && $row['COST'] > 0){
            $prices['pressingPrice']= $row['COST'];
        } else {
            $prices['pressingPrice'] = 0;
        }
        $priceHtml = "<option selected value=\"".$row['dblPrice']."\">".$row['dblPrice']."</option>";
		if ($row['QTY_PRICES'] == 1){
            $sql = "SELECT LOWER_MARGIN, UPPER_MARGIN, PRICE FROM costing_sample_qty_wise WHERE costing_sample_qty_wise.SAMPLE_NO='$sampleNo' AND 
					costing_sample_qty_wise.SAMPLE_YEAR 		=  '$sampleYear' AND 
					costing_sample_qty_wise.REVISION 		=  '$revId' AND 
					costing_sample_qty_wise.COMBO 			=  '$comboId' AND 
					costing_sample_qty_wise.PRINT 		=  '$printName'
				";

            $priceHtml = "<option value=\"\"></option>";
            $result = $db->RunQuery($sql);
            while($row=mysqli_fetch_array($result))
            {
            if ($row['UPPER_MARGIN']==-2){
                $row['UPPER_MARGIN'] = 'INF';
            }
            $range = "  (".$row['LOWER_MARGIN']."-".$row['UPPER_MARGIN'].")";
            $priceHtml .= "<option value=\"".$row['PRICE']."\" title=\"".$range."\">".$row['PRICE']."</option>";
            }
            $prices['qtyPricesEnabled'] = 1;
        }
        $prices['dblPrice'] = $priceHtml;
		echo json_encode($prices);
	}
	else if($requestType=='getTechGroup')
	{
		$sampleNo  	= $_REQUEST['sampleNo'];
		$sampleYear	= $_REQUEST['sampleYear'];
		$revId		= $_REQUEST['revId'];
		$printName	= explode('/',$_REQUEST['printName']);
		$printName = $printName[1];
		$comboId	= $_REQUEST['comboId'];	
		
		$sql = "SELECT
				mst_technique_groups.PRIORITY_LEVEL,
				mst_technique_groups.TECHNIQUE_GROUP_NAME,
				mst_technique_groups.TECHNIQUE_GROUP_ID
				FROM
				trn_sampleinfomations_details
				INNER JOIN mst_techniques ON trn_sampleinfomations_details.intTechniqueId = mst_techniques.intId
				INNER JOIN mst_technique_groups ON mst_techniques.intGroupId = mst_technique_groups.TECHNIQUE_GROUP_ID
				WHERE
				trn_sampleinfomations_details.intSampleNo = '$sampleNo' AND
				trn_sampleinfomations_details.intSampleYear = '$sampleYear' AND
				trn_sampleinfomations_details.intRevNo = '$revId' AND
				trn_sampleinfomations_details.strPrintName = '$printName' AND
				trn_sampleinfomations_details.strComboName = '$comboId'
				ORDER BY 
				mst_technique_groups.PRIORITY_LEVEL asc
				limit 1
				";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		echo $row['TECHNIQUE_GROUP_ID'];
	}
	else if($requestType=='getColor')
	{
		$sampleNo  	= $_REQUEST['sampleNo'];
		$sampleYear	= $_REQUEST['sampleYear'];
		$revId		= $_REQUEST['revId'];
		$printName	= explode('/',$_REQUEST['printName']);
		$printName = $printName[1];
		$comboId	= $_REQUEST['comboId'];	
		
		$sql_brand	= " SELECT
						mst_brand.strName,
						mst_brand.intId
						FROM
						trn_sampleinfomations
						INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
						WHERE
						trn_sampleinfomations.intSampleNo = '$sampleNo' AND
						trn_sampleinfomations.intSampleYear = '$sampleYear' AND
						trn_sampleinfomations.intRevisionNo = '$revId'";
		$result_brand = $db->RunQuery($sql_brand);
		$row_brand	  = mysqli_fetch_array($result_brand);
		$brand		  = $row_brand['strName'];
		
		 $sql = "SELECT DISTINCT
				trn_sampleinfomations_details.intGroundColor,
				mst_colors_ground.strName
				FROM
					trn_sampleinfomations_details 
					Inner Join mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
				WHERE
					trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear'  AND
					trn_sampleinfomations_details.strPrintName =  '$printName' AND
					trn_sampleinfomations_details.strComboName =  '$comboId' AND
					trn_sampleinfomations_details.intRevNo =  '$revId' ";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$color	= $row['strName'];
		
		$response['colorName']	= $color;
		$response['brand']		= $brand;

		echo json_encode($response);
	}
	else if($requestType=='loadChildQtys')
	{
		$orderNo  	= $_REQUEST['orderNo'];
		$year  		= $_REQUEST['year'];
		$sql = "SELECT
		Sum(trn_orderdetails.intQty) AS actualQty
		FROM
		trn_orderdetails
		INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
		WHERE
		 (
		trn_orderheader.DUMMY_PO_NO = '$orderNo' AND
		trn_orderheader.DUMMY_PO_YEAR = '$year') ";
		$result = $db->RunQuery($sql);
		$i=0;
		$row=mysqli_fetch_array($result);
		echo $row['actualQty'];
	}
	else if($requestType=='loadSplitCustomerPO')
	{
		$orderNo  	= $_REQUEST['orderNo'];
		$year  		= $_REQUEST['orderYear'];
		$sql = "SELECT
				trn_orderheader.intOrderNo,
				trn_orderheader.intOrderYear,
				trn_orderheader.strCustomerPoNo
				FROM `trn_orderheader`
				WHERE
				trn_orderheader.intOrderNo = '$orderNo' AND
				trn_orderheader.intOrderYear = '$year' ";
		$result = $db->RunQuery($sql);
		$i=0;
		$row=mysqli_fetch_array($result);
		echo $row['strCustomerPoNo'];
	}
	else if($requestType=='getChildFlag')
	{
		$orderNo  	= $_REQUEST['orderNo'];
		$year  		= $_REQUEST['year'];
		$so  		= $_REQUEST['so'];
		$sql = "SELECT
		trn_orderheader.intOrderNo 
		FROM
		trn_orderdetails
		INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
		WHERE
		 (
		trn_orderheader.DUMMY_PO_NO = '$orderNo' AND
		trn_orderheader.DUMMY_PO_YEAR = '$year' AND 
		trn_orderdetails.DUMMY_SO_ID = '$so' AND 
		trn_orderheader.intStatus !=-2) ";
		$result = $db->RunQuery($sql);
		$i=0;
		$row=mysqli_fetch_array($result);
		if($row['intOrderNo']>0)
			echo 1;
		else
			echo 0;;
	}
	

function get_max_revision_no($sampNo,$sampleYear){
    global $db;
    $sql = 	"SELECT
					Max(trn_sampleinfomations.intRevisionNo) as maxRevNo 
					FROM `trn_sampleinfomations`
					left JOIN trn_sampleinfomations_combo_print_approvedby ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_combo_print_approvedby.SAMPLE_NO AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_combo_print_approvedby.SAMPLE_YEAR AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_combo_print_approvedby.REVISION
					WHERE
					(trn_sampleinfomations.intStatus = 1 OR
					trn_sampleinfomations_combo_print_approvedby.`STATUS` = 1 ) AND
					trn_sampleinfomations.intSampleNo =  '$sampNo' AND
					trn_sampleinfomations.intSampleYear =  '$sampleYear' ";
    $result = $db->RunQuery($sql);
    $row=mysqli_fetch_array($result);

    return $row['maxRevNo'];
}
?>
var basepath	= 'presentation/customerAndOperation/bulk/placeOrder/addNew/';
			
$(document).ready(function() {
 	$("#frmChangeOrder").validationEngine();
	
	$('#frmChangeOrder #butNew').click(function(){
		document.location.href = '?q=645';
	});

	$("#cboOrderNo").live('change',function(){
		submitForm();
	});
	$('#butSearch').live('click',searchData);
	
	function searchData()
	{
		window.location.href = '?q=645&orderNo='+$('#txtOrderNo').val()+'&orderYear='+$('#txtOrderYear').val();
	}
//-----------------------------------------------------------------
	$("#cboOrderYear").live('change',function(){
		$('#txtCustomerPO').val('');
		clearGrid();
		loadOrderNos();
	});
	
//-----------------------------------------------------------------


  ///save button click event
  $('#frmChangeOrder #butSave').click(function(){
	//$('#frmChangeOrder').submit();
	var requestType = '';
	if ($('#frmChangeOrder').validationEngine('validate'))   
    { 
		showWaiting();
		var data = "requestType=save";
		
			data+="&orderNo=" +	$('#txtOrderNo').val();
			data+="&orderYear=" +		$('#txtOrderYear').val();
			data+="&customerPO=" +	URLEncode($('#txtCustomerPO').val());
			data+="&customerLocId=" +	URLEncode($('#cboLocation').val());
			
			//////////////////// saving main grid part //////////////////////////////////////////
			//var rowCount = $('#tblMain >tbody >tr').length;
			var rowCount = document.getElementById('tblMain').rows.length;
			if(rowCount<=1){
				alert("No Sales order nos");
				return false;
			}
			
			var arr="[";
			var salesOrderId='';
			var salesOrderNo='';
			$('#tblMain >tbody >tr').not(':first').each(function(){
					 
					arr += "{";
						 salesOrderId	= 	$(this).find('.salesOrderId').attr('id');
						 salesOrderNo 	= 	$(this).find('.salesOrderNo').val();
					
					arr += '"salesOrderId":"'+			salesOrderId +'",' 	;
					arr += '"salesOrderNo":"'+			URLEncode(salesOrderNo) +'"' 	;
					arr +=  '},';
			});
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			data+="&arr=" +	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basepath+"placeOrder_change-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json", 
			type:'POST', 
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			async:false,
			
			success:function(json){
					$('#frmChangeOrder #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						hideWaiting();
						var t=setTimeout("alertx()",1000);
						window.location.href = '?q=645&orderNo='+$('#txtOrderNo').val()+'&orderYear='+$('#txtOrderYear').val();						//return;
					}
				},
			error:function(xhr,status){
						hideWaiting();
					$('#frmChangeOrder #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
	}
	
						hideWaiting();
   });
   
   
   
   });

	



function alertx()
{
	$('#frmChangeOrder #butSave').validationEngine('hide')	;
}
//------------------------------------------------
function submitForm(){
	window.location.href = "?q=645&orderNo="+$('#cboOrderNo').val()
						+'&orderYear='+$('#cboOrderYear').val()
}
//----------------------------------------------- 
function loadOrderNos()
{
	var year 		= $('#cboOrderYear').val();
	var url		= basepath+"placeOrder_change-db-get.php?requestType=loadOrderNos";
		url	   +="&year="+			year;	
	$.ajax({url:url,
			async:false,
			dataType:'json',
			success:function(json){
					$('#cboOrderNo').html(json.orderNo);
			}
			});
}
//----------------------------------------------- 
function clearGrid(){
	var rowCount = document.getElementById('tblMain').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMain').deleteRow(1);
			
	}
}
<?php 
//ini_set('display_errors',1);


	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$locationId	= $companyId;

	
	require_once $_SESSION['ROOT_PATH']."class/cls_mail.php";
	require_once $_SESSION['ROOT_PATH']."class/customerAndOperation/cls_textile_stores.php";
	include "{$backwardseperator}dataAccess/Connector.php";
	
	$objMail = new cls_create_mail($db);
	$objtexttile = new cls_texttile();
	
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo 		= $_REQUEST['serialNo'];
	$year 			= $_REQUEST['year'];
	$serialNo_dummy	= $_REQUEST['serialNo'];
	$year_dummy		= $_REQUEST['year'];

	$customerPO 	= trim($_REQUEST['customerPO']);
	$customer 		= $_REQUEST['customer'];
	$currency 	    = $_REQUEST['currency'];
	$payTerm 	    = $_REQUEST['payTerm'];
	$poCompany 		= $_REQUEST['poCompany'];
	$poLocation 	= $_REQUEST['poLocation'];
	$locationId		= $poLocation;
	$location 		= $_REQUEST['location'];
	$remarks 		= $_REQUEST['remarks'];
	$delDateH 		= $_REQUEST['delDateH'];
	$dtDate 		= $_REQUEST['dtDate'];
	$contPerson 	= $_REQUEST['contPerson'];
	$marketer 		= $_REQUEST['marketer'];
	$salesIds		= $_REQUEST['salesIds'];
	$payMode		= $_REQUEST['payMode'];
	$techniqueType	= $_REQUEST['techniqueType'];
	$poType			= $_REQUEST['poType'];
	$instant		= $_REQUEST['instant'];

	$arr 		= json_decode($_REQUEST['arr'], true);
	
$programName='Place Order';
$programCode='P0427';

$ApproveLevels = (int)getApproveLevel($programName);
$apLevel = $ApproveLevels+1;
	
	
//------------save---------------------------	
	if($requestType=='save')
	{
		$db->begin();
		$error = false;
		$savableFlag=1;
	    $rollBackFlag=0;

		
		
		if($serialNo==''){
			$serialNo 	= getNextOrderNo($poLocation);
			$year = date('Y');
			$editMode=0;
			$savedStatus=$apLevel;
			$savedLevels=$ApproveLevels;
		}
		else{
			$editMode=1;
			$savableFlag=getSaveStatus($serialNo,$year);//check wether already confirmed
			$sql = "SELECT 
			trn_orderheader.DUMMY_PO_NO, 
			trn_orderheader.DUMMY_PO_YEAR, 
			trn_orderheader.intCreator, 
			trn_orderheader.intStatus, 
			trn_orderheader.intApproveLevelStart,
			trn_orderheader.PO_TYPE,
			trn_orderheader.INSTANT_ORDER 
			FROM trn_orderheader 
			WHERE
			trn_orderheader.intOrderNo =  '$serialNo' AND
			trn_orderheader.intOrderYear =  '$year'";

			$result 		=$db->RunQuery2($sql);
			$row			=mysqli_fetch_array($result);
			$savedStatus	=$row['intStatus'];
			$savedLevels	=$row['intApproveLevelStart'];
			$createdUser	=$row['intCreator'];
			$dummy_no		=$row['DUMMY_PO_NO'];
			$dummy_year		=$row['DUMMY_PO_YEAR'];
			$saved_po_type 	=$row['PO_TYPE'];
		}
		
		
		$editPermission=loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
		//$locationFlag=getLocationValidation('CustomerPO',$locationId,$serialNo,$year);
		$locationFlag=0;
		$duplicatePoCustPO=getDuplicatePoCustPO($customerPO,$customer,$serialNo,$year);
		//--------------------------
		
		
		 if($userId!=$createdUser && $editMode==1){
			 $rollBackFlag=1;
			 $rollBackMsg="It can be edited only by created users";
		 }
		 else if(( $locationFlag==1) && ($editMode==1)){
			 $rollBackFlag=1;
			 $rollBackMsg="Invalid Edit Location";
		 }
		 else if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This PO No is already confirmed.  You cannot edit further.";
		 }
		 else if($editPermission==0){
			 $rollBackFlag=1;
			 $rollBackMsg="No Permission to save.";
		 }
		 else if($duplicatePoCustPO==1){
			 $rollBackFlag=1;
			 $rollBackMsg="This Customer PO No is already exists for this selected customer.";
		 }
		 else if($instant==1 && $customer!='28' && $customer!='105'){
			 $rollBackFlag=1;
			 $rollBackMsg="Can't raise instant orders for this customer.";

		 }
		 else if($instant==1 && $poCompany!='1'){
			 $rollBackFlag=1;
			 $rollBackMsg="Can't raise instant orders for this company.";
		 }
		else if(get_mrn_flag($serialNo,$year)==1 && $saved_po_type != $poType && $editMode==1){
			 $rollBackFlag=1;
			 $rollBackMsg="Can't change order type for MRN raised orders. If you need to change, clear the existing MRN qts";
		}
		else if(get_mrn_flag($serialNo,$year)==1 && $editMode==1 && (($saved_po_type != $poType) || ($saved_po_type == $poType && $poType==1 && $instant!=1) )){
			 $rollBackFlag=1;
			 $rollBackMsg="This pre costing has MRN notes. So can't change it as non-instant . If you need to change, clear the existing MRN qts";
		}
		
		else if($editMode==1){
		$sql = "UPDATE `trn_orderheader` SET intModifyer='$userId',
									  intCustomer ='$customer', 
									  intCustomerLocation ='$location', 
									  strCustomerPoNo ='$customerPO', 
		 							  intPaymentTerm ='$payTerm',
									  intStatus ='$apLevel',
									  intApproveLevelStart='$ApproveLevels', 
									  dtmModifyDate =now(), 
									  dtDate ='$dtDate', 
									  dtDeliveryDate ='$delDateH', 
									  strContactPerson ='$contPerson', 
									  intMarketer ='$marketer', 
									  strRemark = '$remarks', 
									  intLocationId = '$locationId',
									  LC_STATUS = '$payMode',
									  TECHNIQUE_TYPE = '$techniqueType' ,
									  PO_TYPE = '$poType'  ,
									  INSTANT_ORDER = '$instant' 
				WHERE (`intOrderNo`='$serialNo') AND (`intOrderYear`='$year')";
		$result = $db->RunQuery2($sql);
		$maxAppByStatus=(int)getMaxAppByStatus($serialNo,$year)+1;
		$sql = "UPDATE `trn_orderheader_approvedby` SET intStatus ='$maxAppByStatus' 
				WHERE (`intOrderNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
               // $sql_block="select ACTIVE_BULK from mst_customer where mst_customer.intId= $customer";	
                       // echo $sql_block;
                       // $result_block=$db->RunQuery2($sql_block);
                       // $row_block=mysqli_fetch_array($result_block);
                            
                       // $BULK=$row_block['ACTIVE_BULK'];
                       // if($BULK!=1){
		$result1 = $db->RunQuery2($sql);
                        if(!$result){$error = true;	
                        
                       // }
                        
                        }
                       // else{
                             
             // $rollBackFlag=1;
              //$rollBackMsg="can not update because customer is blocked";
                     //   }
                        
                        
		}
		else{
			$sql = "DELETE FROM `trn_orderheader` WHERE 
					(`intOrderNo`='$serialNo') AND (`intOrderYear`='$year')";
			$result1 = $db->RunQuery2($sql);
			
			$sql = "INSERT INTO `trn_orderheader` (`intOrderNo`,`intOrderYear`,`strCustomerPoNo`,intPaymentTerm,dtDate,dtDeliveryDate,intCustomer,intCustomerLocation,intCurrency,strContactPerson,strRemark,intMarketer,intStatus,intCreator,dtmCreateDate,intApproveLevelStart,intLocationId,LC_STATUS,TECHNIQUE_TYPE,PO_TYPE,INSTANT_ORDER) 
					VALUES ('$serialNo','$year','$customerPO','$payTerm','$dtDate','$delDateH','$customer','$location','$currency','$contPerson','$remarks','$marketer','$apLevel','$userId',now(),$ApproveLevels,'$locationId','$payMode','$techniqueType','$poType','$instant')";
			$sql_block="select ACTIVE_BULK from mst_customer where mst_customer.intId= $customer";	
                       // echo $sql_block;
                        $result_block=$db->RunQuery2($sql_block);
                        $row_block=mysqli_fetch_array($result_block);
                            
                        $BULK=$row_block['ACTIVE_BULK'];
                        if($BULK!=1){
                        $result = $db->RunQuery2($sql);
                        
			
			if(!$result){$error = true;	}
                        }
        //$sample=0;              
         else { 
             
             
              $rollBackFlag=1;
              $rollBackMsg="can not save because customer is blocked";
             
        }
		}
		//-----------delete and insert to detail table-----------------------
		
		
		
		if(($result) && ($rollBackFlag!=1)){
			
			if($poType==2){//update deleting qty
				 $sql_dq = "select (intQty) as qty,DUMMY_SO_ID   FROM `trn_orderdetails` WHERE (`intOrderNo`='$serialNo') AND (`intOrderYear`='$year') AND  intSalesOrderId not in($salesIds)";
				$res_dq 		= $db->RunQuery2($sql_dq);
				while($row_dq=mysqli_fetch_array($res_dq))
				{
					$qt_d	= $row_dq['qty'];
					if($deleted_qty[$row_dq['DUMMY_SO_ID']]=='')
						$deleted_qty[$row_dq['DUMMY_SO_ID']]=0;
					if($qt_d=='')
						$qt_d=0;
					
					$deleted_qty[$row_dq['DUMMY_SO_ID']]	+= $qt_d;	
				}
			}
			
			$sql = "DELETE FROM `trn_orderdetails` WHERE (`intOrderNo`='$serialNo') AND (`intOrderYear`='$year') AND  intSalesOrderId not in($salesIds)";
			$result2 = $db->RunQuery2($sql);
		
			$toSave				=0;
			$saved				=0;
			$salesId 			=0;
			$nextSalesOrderId 	= getNextSalesOrderId($serialNo ,$year);
			$nextSalesOrderId ;
			$records			=0;
			$err_qty_flag		=0;
			foreach($arr as $arrVal)
			{
				$records++;
				$parentSoId 	   = $arrVal['parentSoId'];
				if($parentSoId=='')
				$parentSoId		   = 'NULL';
				$salesOrderId 	   = $arrVal['salesOrderId'];
				$salesOrderNo 	   = trim($arrVal['salesOrderNo']);
				$graphicNo 		   = trim($arrVal['graphicNo']);
				$styleNo 		   = trim($arrVal['styleNo']);
				$sampleNo 		   = $arrVal['sampleNo'];
				$sampleYear 	   = $arrVal['sampleYear'];
				$combo 		       = $arrVal['combo'];
				$printString   	   = explode('/',$arrVal['printName']);
				$printName	   	   = $printString[1];
				$partId		       = $printString[0];
				$qty 		       = $arrVal['qty'];
				$price 			   = $arrVal['price'];
				$overCutPercentage = (float)($arrVal['overCutPer']);
				$damagePercentage  = (float)($arrVal['damagePer']);
				$PSD 		       = $arrVal['psDate'];
				$deliveryDate 	   = $arrVal['delDate'];
				$revNo 	   		   = $arrVal['revNo'];
				$ItemCode 	   	   = $arrVal['ItemCodeP'];
				$lineNo 	   	   = $arrVal['lineNo'];
				$techGroup 	   	   = $arrVal['techGroup'];
				$childQty 	   	   = $arrVal['childQty'];
				$grading 	   	   = $arrVal['grading'];
				
				
				//if($poType==2)
				//$deleted_qty[$parentSoId] =- $qty;	


				$child_flag	= getChildFlag($serialNo ,$year,$salesOrderId);
				$child_flag_order	= getChildFlag_order($serialNo ,$year,$salesOrderId);
				if($child_flag_order==0 && ($poType == 1))
				$dummy_qty	=		$qty;
				else 
				$dummy_qty	=		0;
			
				$cons_fag		= checkForSpecialTechniqueConsumption($sampleNo,$sampleYear,$revNo,$combo,$printName);
				$app_fag		= getApprovedSataus($sampleNo,$sampleYear,$revNo,$combo,$printName);
				$split_deleted 	= get_deleted_split($serialNo ,$year,$salesOrderId);
				$flag_new_revision = get_new_revision_status($sampleNo,$sampleYear,$revNo);
				$dummyErr				= getDummyQtyMatchingFlag($dummy_no ,$dummy_year,$serialNo,$year,$salesOrderId,$qty);
				$editPermission_dummy	=get_dummy_edit_mode($dummy_no,$dummy_year);
				$dummyErr	= getDummyQtyBalance($serialNo ,$year,$salesOrderId,$qty,$childQty);
				
				$flag_fab_arrived		=getFabArrivedFlag($serialNo ,$year,$salesOrderId);
				
				$resp_dummy 	= get_dummy_details($serialNo,$year,$salesOrderId);
				if($dummy_no > 0 && $resp_dummy['INSTANT_ORDER']==1){
					$sampleNo_d		= $resp_dummy['sampleNo_d'];
					$sampleYear_d	= $resp_dummy['sampleYear_d'];
					$revNo_d		= $resp_dummy['revNo_d'];
					$combo_d		= $resp_dummy['combo_d'];
					$printName_d	= $resp_dummy['printName_d'];
					if(($sampleNo!=$sampleNo_d) || ($sampleYear!=$sampleYear_d) || ($revNo!=$revNo_d) || ($combo!=$combo_d) || ($printName!=$printName_d)){
					$rollBackFlag	=	1;
					$rollBackMsg	=	"Details of ".$salesOrderNo.'/'.$sampleNo.'/'.$sampleYear.'/'.$revNo.'/'.$combo.'/'.$printName." should be equal to " .$sampleNo_d.'/'.$sampleYear_d.'/'.$revNo_d.'/'.$combo_d.'/'.$printName_d;
					}
				}
				
				if($cons_fag==0){
					$rollBackFlag	=	1;
					$rollBackMsg	=	"Please enter consumptions for special techniques ".$sampleNo.'/'.$sampleYear.'/'.$revNo.'/'.$combo.'/'.$printName;
				}
 				else if($app_fag==0){
					$rollBackFlag	=	1;
					$rollBackMsg	=	"Please approve consumptions for special techniques".$sampleNo.'/'.$sampleYear.'/'.$revNo.'/'.$combo.'/'.$printName;
				}
 				else if($poType==2 && ($editPermission_dummy!=1 && $dummyErr['err']==1)){
					//if($editPermission_dummy!=1 && $dummyErr['err']==1){
						$rollBackFlag	=	1;
						$rollBackMsg	=	"Please revise the dummy PO to change Qtys";
					//}
				/*	$dummyErr	= getDummyQtyMatchingFlag($dummy_no ,$dummy_year,$salesOrderId,$qty);
					if($dummyErr['err']==1){
						$rollBackFlag	=	1;
						$rollBackMsg	=	"Qty balance of dummy order ".$dummy_no.'/'.$dummy_year.'('.$salesOrderNo.'/'.$printName.'/'.$combo.') should be '.$dummyErr['qty'];
					}*/
				}
 				//else if($poType==1 && $editMode==1 /*&& $child_flag ==1*/){
 				/*else if($poType==1 && $editMode==1 && $parentSoId =='NULL' && ($split_deleted==0 || $split_deleted==''))
				{
					//do nothing
				}*/
 				else if($poType==1 && $editMode==1 && $parentSoId =='NULL' && ($dummyErr['err']==1)){
					//if($dummyErr['err']==1){
						$rollBackFlag	=	1;
						$rollBackMsg	=	"Qty balance of dummy order ".$serialNo.'/'.$year.'('.$salesOrderNo.'/'.$printName.'/'.$combo.') should be '.$dummyErr['qty'];
						$err_qty_flag	=1;
						$err_qty_adj_so = $salesOrderId;
						$err_qty_adj	= $dummyErr['qty'];
					//}
				}
				else if($flag_fab_arrived ==0 && $flag_new_revision==1 && $editMode==0){
					$rollBackFlag	=	1;
					$rollBackMsg	=	"There is a new revision for  ".$sampleNo.'/'.$sampleYear.'('.'/'.$printName.'/'.$combo.')';
				}
				
				########################################################
				#### patch : validate sizewiseqty vs order qty     #####
			/*	$sizewiseQty = getSieWiseQty($serialNo,$year,$salesOrderId);
				if($sizewiseQty!=$qty && $sizewiseQty>0){
					$db->RunQuery2('Rollback');
					$response['type'] 		= 'fail';
					$response['msg'] 		= "Invalid sive wise qty.(Row $records)";
					$response['q'] 			= '';
					echo json_encode($response);
					return;
				}*/
				########################################################
			
				if($arrCount[$styleNo]=='')
					$arrCount[$styleNo]=0;
				$arrCount[$styleNo]++;
			
 				/*if($arrCount[$styleNo]>1 && $arrPSD[$styleNo] != $PSD){
					$rollBackFlag	=	1;
					$rollBackMsg	=	"Please enter same PSD dates for same Style";
				}*/
				$arrPSD[$styleNo]  = $PSD;
				
				//15/03/2013-------------------------
			    //$excessQty=loadExcessQty($serialNo,$year,$salesOrderId);
				$toleratePercentage	= $objtexttile->getNewTolerencePercentage2($company,$qty);
				$excessQty=ceil(loadSalesOrderWiseExcessQty($serialNo,$year,$salesOrderId,$toleratePercentage));
				///////////////////////////
				
				if($salesOrderId!='dataRow' && $salesOrderId!='')
				{
						$rcvSQty=getSalesOrderWiseRcvQty($serialNo,$year,$salesOrderId);
						if(($qty+$excessQty)<$rcvSQty){
							//$rollBackFlag=1;	
							//$rollBackMsg= 'Order Quantities cannot be less than Recieved Quantities.</br>'.'maximum Qty ":'.($qty+$excessQty).'  </br>Received Qty :'.$rcvSQty;
						}
							
					if($poType != 2)
						$parentSoId='NULL';
						
					$sql = "UPDATE `trn_orderdetails` SET `strSalesOrderNo`='$salesOrderNo',`strLineNo`='$lineNo',`strStyleNo`='$styleNo',`strGraphicNo`='$graphicNo',`intSampleNo`='$sampleNo',`intSampleYear`='$sampleYear',`strCombo`='$combo',`strPrintName`='$printName',`intRevisionNo`='$revNo',`intPart`='$partId',`intQty`='$qty',`dblToleratePercentage`='$toleratePercentage',
					`dblPrice`='$price',`dblOverCutPercentage`='$overCutPercentage',`dblDamagePercentage`='$damagePercentage',
					`dtPSD`='$PSD',`dtDeliveryDate`='$deliveryDate',`TECHNIQUE_GROUP_ID`='$techGroup', DUMMY_SO_ID =$parentSoId, ITEM_CODE_FROM_CUSTOMER ='$ItemCode',IS_GRADING = $grading";
					if($child_flag_order==0 && ($poType == 1)){
						$split_deleted = get_deleted_split($serialNo ,$year,$salesOrderId);
						if($split_deleted=='' || $split_deleted ==0){ 
							$sql .=",`DUMMY_SO_INITIAL_QTY`='$qty' ";
						}
							$sql .=",`TEMP_DELETED_SPLIT`='0' ";
					 }
					// if($poType!=1)
					//	$sql .=",`TEMP_DELETED_SPLIT`='0' ";

					$sql .= " WHERE (`intOrderNo`='$serialNo') 
					AND (`intOrderYear`='$year') AND (`intSalesOrderId`='$salesOrderId')  ";
				$result3 = $db->RunQuery2($sql);
				if(!$result3){$error = true;}
				$toSave++;
				if($result3==1){
				$saved++;
					}
				}
				else
				{	
					$salesOrderId = ++$nextSalesOrderId;
					if($poType != 2)
						$parentSoId='NULL';
					$sql = "INSERT INTO `trn_orderdetails` (`intOrderNo`,`intOrderYear`,intSalesOrderId,`strSalesOrderNo`,`strGraphicNo`,strStyleNo,`intSampleNo`,`intSampleYear`,`strCombo`,`strPrintName`,`intQty`,`dblToleratePercentage`,`dblPrice`,`dblOverCutPercentage`,`dblDamagePercentage`,`dtPSD`,`dtDeliveryDate`,intRevisionNo,strLineNo,intPart,TECHNIQUE_GROUP_ID,DUMMY_SO_INITIAL_QTY,DUMMY_SO_ID,ITEM_CODE_FROM_CUSTOMER,IS_GRADING) 
				VALUES ('$serialNo','$year','".$salesOrderId."','$salesOrderNo','$graphicNo','$styleNo','$sampleNo','$sampleYear','$combo','$printName','$qty','$toleratePercentage','$price','$overCutPercentage','$damagePercentage','$PSD','$deliveryDate','$revNo','$lineNo','$partId','$techGroup','$dummy_qty',$parentSoId,'$ItemCode',$grading)";
				$result3 = $db->RunQuery2($sql);
				if(!$result3){$error = true;}
				$toSave++;
				if($result3==1){
				$saved++;
					}
				}
			}
		}
		
		//update parent
		if($poType==2){
			foreach ($deleted_qty as $key => $value){
				
				$sql_u		="UPDATE `trn_orderdetails` SET `TEMP_DELETED_SPLIT`='$value' WHERE (`intOrderNo`='$dummy_no') AND (`intOrderYear`='$dummy_year') AND (`intSalesOrderId`='$key')";
				$res_u	 	= $db->RunQuery2($sql_u);

			}
			
			
		}
		
		
		
		if($rollBackFlag==1){
 			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
			if($err_qty_flag==1){
				$response['err_so']		= $err_qty_adj_so;
				$response['err_qty']	= $err_qty_adj;
			}
		}
		else if(($result) && ($toSave==$saved) && ($saved>0) ){
			$confirmationMode=loadConfirmatonMode($programCode,$userId,$ApproveLevels,$apLevel);
			//sendEmails($serialNo,$year,$programCode,$ApproveLevels,$apLevel,$objMail,$editMode);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
		//	$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		
		$db->commit();
		//echo json_encode($response);
	}
	
	else if($requestType=='saveSplittedSalesOrder'){
	
		$db->begin();
		$error = false;
		$savableFlag=1;
	    $rollBackFlag=0;
		
			
			if(!$result){$error = true;	}

			$toSave				=0;
			$saved				=0;
			$salesId 			=0;
			$nextSalesOrderId 	= getNextSalesOrderId($serialNo ,$year);
			$nextSalesOrderId ;
			$records			=0;
			$temp_cust_po		=$customerPO;
			
			$sql = "SELECT 
			trn_orderheader.intCreator, 
			trn_orderheader.intStatus, 
			trn_orderheader.intApproveLevelStart ,
			trn_orderheader.intLocationId 
			FROM trn_orderheader 
			WHERE
			trn_orderheader.intOrderNo =  '$serialNo_dummy' AND
			trn_orderheader.intOrderYear =  '$year_dummy'";

			$result = $db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			$savedStatus_d=$row['intStatus'];
			$savedLevels_d=$row['intApproveLevelStart'];
			$createdUser_d=$row['intCreator'];
			$poLocation=$row['intLocationId'];
			$editPermission_d=loadEditMode($programCode,$savedStatus_d,$savedLevels_d,$userId);
		
			 if($userId!=$createdUser_d && $editPermission_d==1){
				 $rollBackFlag=1;
				 $rollBackMsg="It can be edited only by created users";
			 }
		
			$sql_block="select ACTIVE_BULK from mst_customer where mst_customer.intId= $customer";	
			$result_block=$db->RunQuery2($sql_block);
			$row_block=mysqli_fetch_array($result_block);
			$BULK=$row_block['ACTIVE_BULK'];
			if($BULK==1){
				$rollBackFlag=1;
				$rollBackMsg="can not save split because customer is blocked";
			}
	
		
			function sortArr ($x, $y) {
				return strcasecmp($x['customerPO'], $y['customerPO']);
			}
			usort($arr, 'sortArr');	
			
			$order_string		='';
			$c_gen				=0;
			foreach($arr as $arrVal)
			{
				//----save header------------------------------------
				if($temp_cust_po != $arrVal['customerPO']){
					$c_gen++;
					if($arrVal['save_to_no']==''){
						$serialNo 			= getNextOrderNo($poLocation);
						$year				= date('Y');
					}
					$customerPO			= trim($arrVal['customerPO']);
					
					$duplicatePoCustPO		=	getDuplicatePoCustPO($customerPO,$customer,$serialNo,$year);
					 if($duplicatePoCustPO==1 && $arrVal['save_to_no']==''){
						 $rollBackFlag=1;
						 $rollBackMsg="This Customer PO No is already exists for this selected customer.";
					 }
					$save_to_status_flag	=	getStatusOfSaveToOrder($arrVal['save_to_no'],$arrVal['save_to_year']);
					 if($save_to_status_flag==0 && $arrVal['save_to_no']!=''){
						 $rollBackFlag=1;
						 $rollBackMsg="The split order ".$arrVal['save_to_no']."/".$arrVal['save_to_year']." is not pending";
					 }
				
		
					if($arrVal['save_to_no']==''){//save for a new customer order no
						$sql = "INSERT INTO `trn_orderheader` (`intOrderNo`,`intOrderYear`,`strCustomerPoNo`,intPaymentTerm,dtDate,dtDeliveryDate,intCustomer,intCustomerLocation,intCurrency,strContactPerson,strRemark,intMarketer,intStatus,intCreator,dtmCreateDate,intApproveLevelStart,intLocationId,LC_STATUS,TECHNIQUE_TYPE,PO_TYPE,DUMMY_PO_NO,DUMMY_PO_YEAR) 
						VALUES ('$serialNo','$year','$customerPO','$payTerm',date(now()),'$delDateH','$customer','$location','$currency','$contPerson','$remarks','$marketer','$apLevel','$userId',now(),$ApproveLevels,'$locationId','$payMode','$techniqueType','2','$serialNo_dummy','$year_dummy')";
						$result3 = $db->RunQuery2($sql);
						$order_string		.=$serialNo.'/'.$year.', ';
					}
					else
						$order_string_updated		.=$arrVal['save_to_no'].'/'.$arrVal['save_to_year'].', ';
					 
				}
				$temp_cust_po = $arrVal['customerPO'];
				//---------------------------------------------------
				$records++;
				$parentSoId 	   = $arrVal['parentSoId'];
				$salesOrderId 	   = $arrVal['salesOrderId'];
				$salesOrderNo 	   = $arrVal['salesOrderNo'];
				$graphicNo 		   = trim($arrVal['graphicNo']);
				$styleNo 		   = trim($arrVal['styleNo']);
				$sampleNo 		   = $arrVal['sampleNo'];
				$sampleYear 	   = $arrVal['sampleYear'];
				$combo 		       = $arrVal['combo'];
				$printString   	   = explode('/',$arrVal['printName']);
				$printName	   	   = $printString[1];
				$partId		       = $printString[0];
				$qty 		       = $arrVal['qty'];
				$price 			   = $arrVal['price'];
				$overCutPercentage = (float)($arrVal['overCutPer']);
				$damagePercentage  = (float)($arrVal['damagePer']);
				$PSD 		       = $arrVal['psDate'];
				$deliveryDate 	   = $arrVal['delDate'];
				$revNo 	   		   = $arrVal['revNo'];
				$ItemCode 	   	   = $arrVal['ItemCodeP'];
				$lineNo 	   	   = $arrVal['lineNo'];
				$techGroup 	   	   = $arrVal['techGroup'];
				$grading 	   	   = $arrVal['grading'];
			
			
			/*	$cons_fag	= checkForSpecialTechniqueConsumption($sampleNo,$sampleYear,$revNo,$combo,$printName);
				$app_fag	= getApprovedSataus($sampleNo,$sampleYear,$revNo,$combo,$printName);
				if($cons_fag==0){
					$rollBackFlag	=	1;
					$rollBackMsg	=	"Please enter consumptions for special techniques";
				}
 				else if($app_fag==0){
					$rollBackFlag	=	1;
					$rollBackMsg	=	"Please approve consumptions for special techniques";
				}*/
				########################################################
				#### patch : validate sizewiseqty vs order qty     #####
			/*	$sizewiseQty = getSieWiseQty($serialNo,$year,$salesOrderId);
				if($sizewiseQty!=$qty && $sizewiseQty>0){
					$db->RunQuery2('Rollback');
					$response['type'] 		= 'fail';
					$response['msg'] 		= "Invalid sive wise qty.(Row $records)";
					$response['q'] 			= '';
					echo json_encode($response);
					return;
				}*/
				########################################################
			
				if($arrCount[$styleNo]=='')
					$arrCount[$styleNo]=0;
				$arrCount[$styleNo]++;
			
 				if($arrCount[$styleNo]>1 && $arrPSD[$styleNo] != $PSD){
					$rollBackFlag	=	1;
					$rollBackMsg	=	"Please enter same PSD dates for same Style";
				}
				$arrPSD[$styleNo]  = $PSD;
				
				//15/03/2013-------------------------
			    //$excessQty=loadExcessQty($serialNo,$year,$salesOrderId);
				$toleratePercentage	= $objtexttile->getNewTolerencePercentage2($company,$qty);
				$excessQty=ceil(loadSalesOrderWiseExcessQty($serialNo,$year,$salesOrderId,$toleratePercentage));
				///////////////////////////
				if($arrVal['save_to_no']!=''){
					$salesOrderId	=	getMaxSalesOrder($arrVal['save_to_no'],$arrVal['save_to_year']);
					$sql = "INSERT INTO `trn_orderdetails` (`intOrderNo`,`intOrderYear`,intSalesOrderId,`strSalesOrderNo`,`strGraphicNo`,strStyleNo,`intSampleNo`,`intSampleYear`,`strCombo`,`strPrintName`,`intQty`,`dblToleratePercentage`,`dblPrice`,`dblOverCutPercentage`,`dblDamagePercentage`,`dtPSD`,`dtDeliveryDate`,intRevisionNo,strLineNo,intPart,TECHNIQUE_GROUP_ID,DUMMY_SO_ID,ITEM_CODE_FROM_CUSTOMER,IS_GRADING) 
				VALUES ('".$arrVal['save_to_no']."','".$arrVal['save_to_year']."','".$salesOrderId."','$salesOrderNo','$graphicNo','$styleNo','$sampleNo','$sampleYear','$combo','$printName','$qty','$toleratePercentage','$price','$overCutPercentage','$damagePercentage','$PSD','$deliveryDate','$revNo','$lineNo','$partId','$techGroup','$parentSoId','$ItemCode',$grading)";
				}
				else{
					$salesOrderId	=	getMaxSalesOrder($serialNo,$year);
					$sql = "INSERT INTO `trn_orderdetails` (`intOrderNo`,`intOrderYear`,intSalesOrderId,`strSalesOrderNo`,`strGraphicNo`,strStyleNo,`intSampleNo`,`intSampleYear`,`strCombo`,`strPrintName`,`intQty`,`dblToleratePercentage`,`dblPrice`,`dblOverCutPercentage`,`dblDamagePercentage`,`dtPSD`,`dtDeliveryDate`,intRevisionNo,strLineNo,intPart,TECHNIQUE_GROUP_ID,DUMMY_SO_ID,ITEM_CODE_FROM_CUSTOMER,IS_GRADING) 
				VALUES ('$serialNo','$year','".$salesOrderId."','$salesOrderNo','$graphicNo','$styleNo','$sampleNo','$sampleYear','$combo','$printName','$qty','$toleratePercentage','$price','$overCutPercentage','$damagePercentage','$PSD','$deliveryDate','$revNo','$lineNo','$partId','$techGroup','$parentSoId','$ItemCode',$grading)";
				}
				$result3 = $db->RunQuery2($sql);
				if(!$result3){$error = true;}
				$toSave++;
				if($result3==1){
				$saved++;
					}
				}
 	
		
			//get splitted orders---------------------
			
			$sql	="SELECT
						trn_orderheader.intOrderNo,
						trn_orderheader.intOrderYear,
						trn_orderheader.strCustomerPoNo 
						FROM `trn_orderheader`
						WHERE
						trn_orderheader.PO_TYPE = 2 AND
						trn_orderheader.DUMMY_PO_NO = '$serialNo_dummy' AND
						trn_orderheader.DUMMY_PO_YEAR = '$year_dummy'
						";
			$result = $db->RunQuery2($sql);
			$c	=0;
			$order_string_link	='';
			while($row=mysqli_fetch_array($result))
			{
				$c++;
				$childNo 			= $row['intOrderNo'];
				$childYear 			= $row['intOrderYear'];
				$childCPO 			= $row['strCustomerPoNo'];
				$order_string_link	.='<a href="?q=427&orderNo='.$childNo.'&orderYear='.$childYear.'" target="placeOrder.php">'.$childYear.'/'.$childNo.'('.$childCPO.')</a>'.', ';
			}
			$order_string_link		=rtrim($order_string_link, ", ");
			if($c==1)
				$order_string_link		='Split order is '.$order_string_link;
			else if($c>1)
				$order_string_link		='Split orders are '.$order_string_link;
			//----------------------------------------
		
		
		if($rollBackFlag==1){
 			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if($c==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'There is no seperate split orders to save';
		}
		else if($saved==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'There is no new seperate split orders to save';
		}
		else if(($toSave==$saved) && ($saved>0) ){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($c_gen==1){
				if($order_string!='')
					$response['msg'] 		= 'New split order '.rtrim($order_string, ", ").' was generated successfully.';
				if($order_string_updated!='')
					$response['msg'] 		= ' The split order '.rtrim($order_string_updated, ", ").' was updated successfully.';
			}
			else{
				if($order_string!='')
					$response['msg'] 		= 'New split orders '.rtrim($order_string, ", ").' were generated successfully.';
				if($order_string_updated!='')
					$response['msg'] 		= ' The split orders '.rtrim($order_string_updated, ", ").' were updated successfully.';
			}
			$response['serialNo'] 		= $serialNo_dummy;
			$response['year'] 			= $year_dummy;
			$response['child'] 			= rtrim($order_string_link, ", ");
			$response['c'] 				= $c;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		
		$db->commit();
		
	}
	
	if($requestType=='copySizeWiseQty')
	{
		$salesOrderIdFrm	= $_REQUEST['salesOrderIdFrm'];
		$salesOrderIdTo		= $_REQUEST['salesOrderIdTo'];
		$orderNo			= $_REQUEST['orderNo'];
		$orderYear			= $_REQUEST['orderYear'];
		
		$savedStatus    	= true;
		$errorMessage		= '';
		$errorSql			= ''; 
		
		$db->begin();
		$sql = "SELECT strSize,dblQty FROM trn_ordersizeqty
				WHERE 
				intOrderNo = '$orderNo' AND
				intOrderYear = '$orderYear' AND
				intSalesOrderId = '$salesOrderIdFrm' ";
				
		$result = $db->RunQuery2($sql);
		if(mysqli_num_rows($result)<=0 && $savedStatus)
		{
			$savedStatus 	= false;
			$errorMessage 	= 'Please enter size wise qty for first sales order before copy.';
		}
		
		$sqlIns = " INSERT INTO trn_ordersizeqty 
					(
					intOrderNo, 
					intOrderYear, 
					intSalesOrderId, 
					strSize, 
					dblQty,
                                        maxRetryCount,
                                        retryCount,
                                        orderType
					)
					(
					SELECT intOrderNo,
					intOrderYear,
					$salesOrderIdTo,
					strSize,
					dblQty,
                                        maxRetryCount,
                                        retryCount,
                                        0
					FROM trn_ordersizeqty
					WHERE 
					intOrderNo = '$orderNo' AND
					intOrderYear = '$orderYear' AND
					intSalesOrderId = '$salesOrderIdFrm'
					)";
		//echo $sqlIns;
		$resultIns = $db->RunQuery2($sqlIns);
		if(!$resultIns)
		{
			$savedStatus 	= false;
			$errorMessage 	= $db->errormsg;
			$errorSql		= $sqlIns;
		}
		
		if($savedStatus)
		{
			$db->commit();
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Successfully copied from the data row '.$salesOrderIdFrm;
		}
		else
		{
			$db->rollback();
			$response['type'] 		= 'fail';
			$response['msg'] 		= $errorMessage;
			$response['q'] 			= $errorSql;
		}
	}
	if($requestType=='sendToApproval')
	{
		$db->begin();
		$emailFlag=0;
		$sql = "SELECT
		trn_orderheader.intStatus, 
		trn_orderheader.intApproveLevelStart, 
		Count(trn_orderdetails.strSalesOrderNo) as count , 
		trn_orderheader.strCustomerPoNo , 
		mst_locations.intCompanyId 
		FROM trn_orderheader 
		Left Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear 
		Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
		WHERE
		trn_orderheader.intOrderNo =  '$serialNo' AND
		trn_orderheader.intOrderYear =  '$year'";

		$result = $db->RunQuery2($sql);
		$rollBackFlag=0;
		$rollBackMsg ='';
		$row=mysqli_fetch_array($result);
		$customerPO=$row['strCustomerPoNo'];
		
		if($row['count']==0){// 
			$rollBackFlag = 1;
			$rollBackMsg ="Details not saved to Approve"; 
		}
/*		else if($row['intStatus']==1){// 
			$rollBackFlag = 1;
			$rollBackMsg ="Final confirmation of this PO is already raised"; 
		}
*/		else if($row['intStatus']<=$row['intApproveLevelStart']){// 
			$rollBackFlag = 1;
			$rollBackMsg ="First confirmation of this PO is already raised"; 
		}
		else if($row['intStatus']==0){// 
			$rollBackFlag = 1;
			$rollBackMsg ="This PO is rejected"; 
		}
		else if(!checkSizes($serialNo,$year)){
				$rollBackMsg ="Size wise qty is not found."; 
				$rollBackFlag = 1;
		}
		else{
			$emailFlag=1;
			$rollBackFlag = 0;
			$rollBackMsg ='';
			//sendEmails($serialNo,$year,$customerPO,$programCode,$row['intApproveLevelStart'],$row['intStatus'],$row['intCompanyId'],$objMail);
		}
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($emailFlag)){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Sent sucessfully.';
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
			
			
	
	
	}
	else if($requestType=='saveSizes')
	{
		$db->begin();
		$serialNo 		= $_POST['serialNo'];
		$year 			= $_POST['year'];
		$salesOrderNo 	        = $_POST['intSalesOrderId'];
		//$SYear 			= $_POST['txtSYear'];
		//$sampleNo 		= $_POST['txtsampleNo'];
		//$Revision 		= $_POST['txtRevision'];
		//$combo 			= $_POST['txtcombo1'];
		//$print_P 		= $_POST['txtcboprint'];
		$arr 			= json_decode($_POST['arr'], true);
		
		if($salesOrderNo=='dataRow'){
                   
			$errorFlag=1;	
			$msg='Please save before add sizes';
		}
		
		if($errorFlag !=1)
                {
		
                $finalRecord = array();    
                $sql_select = " SELECT
                                trn_ordersizeqty.intOrderNo,
                                trn_ordersizeqty.intOrderYear,
                                trn_ordersizeqty.intSalesOrderId,
                                trn_ordersizeqty.strSize,
                                trn_ordersizeqty.dblQty,
                                trn_ordersizeqty.ncingaDeliveryStatus
                                FROM
                                `trn_ordersizeqty`
                                WHERE
                                (`intOrderNo` = '$serialNo')
                                AND (`intOrderYear` = '$year')
                                AND (`intSalesOrderId` = '$salesOrderNo')";
                
                //echo $sql_select;
                $result_select = $db->RunQuery2($sql_select);
                $bool_type = 0;
                
                while($row_select 	= mysqli_fetch_array($result_select))
                {
                    $value   = array();
                    $orderNo = $row_select['intOrderNo'];
                    $orderYear = $row_select['intOrderYear'];
                    $salesOrderId = $row_select['intSalesOrderId'];
                    $size = $row_select['strSize'];
                    $qty = $row_select['dblQty'];
                    $deliverToNcinga = $row_select['ncingaDeliveryStatus'];
                    
                    $value["OrderNo"] =  "$orderNo";
                    $value["OrderYear"] = "$orderYear";
                    $value["SalesOrderId"] = "$salesOrderId"; 
                    $value["Size"] = "$size";
                    $value["Qty"] = "$qty";
                    $value["DeliverToNcinga"] = "$deliverToNcinga";
                    
                    array_push($finalRecord,$value);
                  
                    
                }
            
              //  print_r($finalRecord);
               
		$sql = "DELETE FROM `trn_ordersizeqty` WHERE (`intOrderNo`='$serialNo') AND (`intOrderYear`='$year') AND (`intSalesOrderId`='$salesOrderNo')";
		
                $result2 = $db->RunQuery2($sql);
		
		$toSave=0;
		$saved=0;
		$errorFlag=0;
               // print_r($finalRecord);
                
                        
		foreach($arr as $arrVal)
		{               
                                
                         	$size 	   = strtoupper($arrVal['size']);
				$grade 	   = $arrVal['grade'];
				$qty 	   = $arrVal['qty'];
                                $bool_type = 0;
                           //  $i=0;   
                        foreach ($finalRecord as $key => $val) 
                        {
                            //$i=0;

                        $OrderNo         =  $val['OrderNo'] ;
                        $OrderYear       =  $val['OrderYear'] ;
                        $SalesOrderId    =  $val['SalesOrderId'] ;
                        $Size            =  $val['Size'] ;
                        $Qty             =  $val['Qty'] ;
                        $DeliverToNcinga             =  $val['DeliverToNcinga'] ; 
                    
                                       
                            if($val['Size']== $size && $val['Qty']!=$qty&&$DeliverToNcinga ==1)
                            { 
                                echo "ncinga";
                                unset($finalRecord[$key]);
                                $bool_type = 1; //updated
                                
                            }
                            else if($val['Size']== $size && $val['Qty']==$qty&&$DeliverToNcinga ==1)
                            { 
                                echo "ncinga same";
                                unset($finalRecord[$key]);
                                $bool_type = 1; //updated
                            }
                            else if($val['Size']== $size && $val['Qty']!=$qty&&$DeliverToNcinga ==0)
                            {
                                echo "no";
                                unset($finalRecord[$key]);
                                $bool_type = 0;//new
                            }
                            else if($val['Size']== $size && $val['Qty']==$qty&&$DeliverToNcinga ==0)
                            {
                                echo "yes";
                                unset($finalRecord[$key]);
                                $bool_type = 0;//new
                            }
                           
                            // $i++;              
                         }  //inner loop  
                        
                   
				$isgraded  = checkfordataInGrading($serialNo,$year,$salesOrderNo); 
				if($isgraded==1)
				{ 
					$issizeequal  = getsizeingrading($serialNo,$year,$salesOrderNo,$size);
					if($issizeequal!=1)
					{
						$errorFlag	=1;	
						$msg		='Sizes not found in Grading are not allowed to save';
					}
				}
				$salesOrderQty=getSalesOrderQty($serialNo,$year,$salesOrderNo);
				$rcvQty=getRcvQty($serialNo,$year,$salesOrderNo,$size);
				$fDQty  = loadDamageReturnedQty($companyId,$serialNo,$year,$salesOrderNo);
				
				$toleratePercentage	= $objtexttile->getSavedTolerencePercentage2($serialNo,$year,$salesOrderNo);
				$excessQty=(loadSalesOrderWiseExcessQty($serialNo,$year,$salesOrderNo,$toleratePercentage));
				
				///////////////////////////
				$maxQty=ceil(($qty+($excessQty/$salesOrderQty)*$qty+$fDQty));
				if($maxQty<$rcvQty){
					$errorFlag=1;	
					$msg='Size Order Quantity for size '.$size.' cannot be less than '.ceil($rcvQty-($excessQty/$salesOrderQty)*$qty-$fDQty);
				}
			
			$sql = "INSERT INTO `trn_ordersizeqty` (`intOrderNo`,`intOrderYear`,`intSalesOrderId`,strSize,dblQty,maxRetryCount,retryCount,orderType) 
					VALUES ('$serialNo','$year','$salesOrderNo','$size','$qty',3,0,$bool_type)";
                      // echo $sql;
			$result = $db->RunQuery2($sql);
                        
                         // echo $bool_insert = 1; 
			$toSave++;
			if($result==1){
			$saved++;
                        
			}
                
                }//outer foreach
               // echo ($finalRecord['DeliverToNcinga']);
                $recordList = array();
                $finalArray = array();
                
              //$finalRecord
               // print_r($finalRecord);
                if(!empty($finalRecord))
                {    // print_r($finalRecord);
                    foreach ($finalRecord as $key => $val) 
                        {
                        $strorderno         =  $val['OrderNo'];
                        $strorderyear       =  $val['OrderYear'];
                        $intsalesOrderid    =  $val['SalesOrderId'];
                        $strsize            =  $val['Size'];
                        $intqty             =  $val['Qty']; 
                        $intdeliverToNcinga    =  $val['DeliverToNcinga'];
                       
                        if($intdeliverToNcinga == 1) //if already deliverd to ncinga
                        {
                                    $sql_select = "SELECT
                    trn_orderheader.intReviseNo,
                    trn_orderheader.intCreator,
                    trn_orderheader.intStatus,
                    trn_orderheader.intApproveLevelStart,
                    trn_orderheader.strCustomerPoNo,
                    trn_orderheader.intLocationId,
                    mst_locations.intCompanyId,
                    mst_locations.strName AS companyLocation,
                    trn_orderheader.intCustomer,
                    mst_customer.strName AS customer,
                    mst_customer_locations_header.strName AS customerLocation,
                    mst_companies.strName AS company,
                    trn_orderheader.strCustomerPoNo,
                    trn_orderdetails.intSalesOrderId,
                    trn_orderdetails.strSalesOrderNo,
                    trn_orderdetails.intOrderNo,
                    trn_orderdetails.intOrderYear,
                    trn_orderdetails.strLineNo,
                    trn_orderheader.dtDeliveryDate AS poDeliveryDate,
                    trn_orderdetails.dtDeliveryDate AS soDeliverydate,
                    trn_orderdetails.strGraphicNo,
                    trn_orderdetails.intSampleNo,
                    trn_orderdetails.intSampleYear,
                    trn_orderdetails.strStyleNo,
                    trn_orderdetails.dtPSD,
                    trn_orderdetails.intQty AS poqty,
                    trn_orderheader.dtDate,
                    trn_orderdetails.intOrderYear,
                    trn_orderdetails.intPart,
                    mst_part.strName as partName,
                    trn_orderdetails.dblDamagePercentage,
                    trn_ordersizeqty.strSize,
                    trn_ordersizeqty.dblQty,
                    if((trn_ordersizeqty.orderType=0),'new','revised') as orderType,
                    mst_brand.strName AS brand,
                    trn_sampleinfomations_details.intGroundColor,
                    mst_colors_ground.strName as groundColor,
                    mst_customer_locations.BundleTag
                    FROM
                    trn_orderheader
                    INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
                    AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
                    INNER JOIN mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
                    INNER JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
                    INNER JOIN mst_customer_locations_header ON mst_customer_locations_header.intId = trn_orderheader.intCustomerLocation
                    INNER JOIN mst_customer_locations ON mst_customer_locations.intCustomerId = mst_customer.intId
                    AND mst_customer_locations.intLocationId = mst_customer_locations_header.intId
                    INNER JOIN mst_companies ON mst_companies.intId = mst_locations.intCompanyId
                    INNER JOIN mst_part ON mst_part.intId = trn_orderdetails.intPart
                    INNER JOIN trn_ordersizeqty ON trn_ordersizeqty.intOrderNo = trn_orderdetails.intOrderNo
                    AND trn_ordersizeqty.intOrderYear = trn_orderdetails.intOrderYear
                    AND trn_ordersizeqty.intSalesOrderId = trn_orderdetails.intSalesOrderId
                    INNER JOIN trn_sampleinfomations_details ON trn_orderdetails.intSampleNo = trn_sampleinfomations_details.intSampleNo
                    AND trn_orderdetails.intSampleYear = trn_sampleinfomations_details.intSampleYear
                    AND trn_orderdetails.intRevisionNo = trn_sampleinfomations_details.intRevNo
                    AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName
                    AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
                    INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                    AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                    AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo
                    INNER JOIN mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
                    iNNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
                    WHERE
                    trn_orderheader.intOrderNo = '$strorderno'
                    AND trn_orderheader.intOrderYear = '$strorderyear'
                    AND trn_orderdetails.intSalesOrderId = $intsalesOrderid    
                    -- AND  trn_ordersizeqty.strSize = '$strsize'   

                    GROUP BY
                    trn_orderdetails.intOrderNo,
                    trn_orderdetails.intOrderYear,
                    trn_orderdetails.intSalesOrderId,
                    trn_ordersizeqty.strSize
                    LIMIT 1";
         //echo   $sql_select;               
                    $result1 = $db->RunQuery2( $sql_select);  
                    while ($row = mysqli_fetch_array($result1))
                             {

                                $value = array();

                                $customer = $row['customer'];
                                $customerLocation = $row['customerLocation'];
                                $company = $row['company'];
                                $companyLocation = $row['companyLocation'];
                                $strCustomerPoNo = $row['strCustomerPoNo'];
                                $intSalesOrderId = $row['intSalesOrderId'];
                                $strSalesOrderNo = $row['strSalesOrderNo'];
                                $intOrderNo = $row['intOrderNo'];
                                $strLineNo = $row['strLineNo'];
                                $poDeliveryDate = $row['poDeliveryDate'];
                                $soDeliverydate = $row['soDeliverydate'];
                                $strGraphicNo = $row['strGraphicNo'];
                                $intSampleNo = $row['intSampleNo'];
                                $intSampleYear = $row['intSampleYear'];
                                $groundColor = $row['groundColor'];
                                $strStyleNo = $row['strStyleNo'];
                                $backgroundColor = $row['intSampleYear'];
                                $strSize = $strsize;
                                $dtPSD = $row['dtPSD'];
                                $dblQty = $intqty;
                                $poqty  = $row['poqty'];
                                $dtDate = $row['dtDate'];
                                $intOrderYear = $row['intOrderYear'];
                                $partName = $row['partName'];
                                $brand = $row['brand'];
                                $dblDamagePercentage = $row['dblDamagePercentage'];
                                $bundleTag = $row['BundleTag'];
                                $orderType = "cancel";
                
                
                                //---------------assign to array--------------------- 
                                $value["transactionId"] = $intOrderNo . "." . $intOrderYear . "." . $intSalesOrderId . "." . $strSize . "";
                                $value["customer"] =  "$customer";
                                $value["customerLocation"] = " $customerLocation";
                                $value["company"] = "$company"; 
                                $value["companyLocation"] = "$companyLocation";
                                $value["customerPO"] = "$strCustomerPoNo";
                                $value["customerSO"] = "$strSalesOrderNo";
                                $value["SalesOrderId"] = "$intSalesOrderId";
                                $value["orderNo"] = "$intOrderNo";
                                $value["lineNo"] = "$strLineNo";
                                $value["poDeliveryDate"] = "$poDeliveryDate"; 
                                $value["graphicNo"] = "$strGraphicNo";
                                $value["sampleNo"] = "$intSampleYear"."/"."$intSampleNo";
                                $value["brand"] = "$brand";
                                $value["style"] = "$strStyleNo";
                                $value["backGroundColor"] = "$groundColor";
                                $value["size"] = "$strSize";
                                $value["PSD"] = "$dtPSD";
                                $value["deliveryDate"] = "$soDeliverydate";
                                $value["qty"] = $dblQty;
                                $value["poQty"] =$poqty;
                                $value["actualTotalQty"] =$dblQty;
                                $value["date"] = "$dtDate";
                                $value["year"] = "$intOrderYear";
                                $value["part"] = "$partName";
                                $value["PDPercentage"] = " $dblDamagePercentage";
                                $value["damagePercentage"] = " $dblDamagePercentage";
                                $value["bundleTag"] = $bundleTag;
                                $value["type"] = "order";
                                $value["orderType"] = "$orderType";
                
                
                                $recordList = $value;
                                $recordArray = array('value' => $recordList);
                                array_push($finalArray,$recordArray);

                            }  //while 
                        }//if deliverd to ncinga  
                        }//foreach
                    $token = select_token();
                    send_detais($finalArray, $token);
                }//if not empty finalrecord if condition
                
              }
		
					//$db->rollback();

		if($errorFlag==1){
			$response['type'] 		= 'fail';
			$response['msg'] 		= $msg;
			$db->rollback();
		}
		else if($toSave==$saved && $saved >0){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Sizes Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		$db->commit();
	}

//----------------------------------------
	echo json_encode($response);
//-----------------------------------------------
function send_detais($record, $token) {

    if ($token == "") { //if token null
        generate_token($record);
    }

    $configs = include('../../../../../config/ncigaConfig.php');
    $url = $configs['URL'];
    $data = array('token' => $token, 'records' => $record);
    $data_json = json_encode($data);
    //print_r($data_json);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($ch);
    $nciga_response = json_decode($response); //get nciga response
  // print_r($nciga_response);
    //$message_code = $nciga_response->messageCode;

    
    if (!$response) {

        die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    }

    curl_close($ch);
}
//------------------------------------------------------------
function generate_token($record) {


    $configs = include('../../../../../config/ncigaConfig.php');
    $url = $configs['URL'];
    $AUTH_USERNAME = $configs['AUTH_USERNAME'];
    $AUTH_PASSWORD = $configs['AUTH_PASSWORD'];
    $TYPE = $configs['TYPE'];
    $data = array('username' => $AUTH_USERNAME, 'password' => $AUTH_PASSWORD, 'type' => $TYPE);
    $data_json = json_encode($data);
  //print_r($data_json);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($ch);
    //print_r($response);
    curl_close($ch);

    insert_tbl($response); //insert token 
    $token_new = select_token(); //get newly generated token
    send_detais($record, $token_new); //send details again

    if (!$response) {
        die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    }

    curl_close($ch);
}
//-------------------------------------------------------
function insert_tbl($data) {

    global $db;
    $date = date('Y-m-d H:i:s', time());

    $jsonArray = json_decode($data, true);
    $userId = $jsonArray['userId'];
    $generatedTime = $jsonArray['generatedTime'];
    $expireDate = $jsonArray['expireDate'];
    $token = $jsonArray['token'];

    $sql_insert = "
                            INSERT INTO
                            tbl_token_details
                            (USER_ID,
                            GENERATED_TIME,
                            EXPIRED_DATE,
                            TOKEN,
                            UPDATED_TIME
                            )
                            VALUES
                            ('$userId',
                             $generatedTime,
                             $expireDate,   
                             '$token',
                             '$date'  
                            );";

    $db->RunQuery2($sql_insert);
}

//--------------------------------------------------------
function select_token() {

    global $db;
    $sql_token = "SELECT UPDATED_TIME, date(UPDATED_TIME),TOKEN
                          FROM tbl_token_details AS a
                          WHERE (UPDATED_TIME)= (
                          SELECT MAX((UPDATED_TIME))
                          FROM tbl_token_details AS b )";

    $result = $db->RunQuery2($sql_token);
    $row = mysqli_fetch_array($result);
    $token = $row['TOKEN'];
    return $token;
}
	
//------------------------------------------------
function getsizeingrading($serialNo,$year,$salesOrderNo,$size)
{ 	global $db;

	$sql_get_sample = 
					"select * from trn_orderdetails
					WHERE
					trn_orderdetails.intOrderNo =   $serialNo AND
					trn_orderdetails.intOrderYear =  $year AND
					trn_orderdetails.intSalesOrderId = $salesOrderNo";
					$result_smple = $db->RunQuery2($sql_get_sample);
					while($row_sample 	= mysqli_fetch_array($result_smple))
					{
								$sample_no 		= $row_sample['intSampleNo'];
								$sample_yr 		= $row_sample['intSampleYear'];
								$revision	 	= $row_sample['intRevisionNo'];
								$combo 			= $row_sample['strCombo'];
								$print 			= $row_sample['strPrintName'];



								$sql = "SELECT * FROM
										trn_sampleinfomations_gradings
										WHERE
										trn_sampleinfomations_gradings.SAMPLE_NO =  $sample_no 
										AND trn_sampleinfomations_gradings.SAMPLE_YEAR = $sample_yr 
										AND trn_sampleinfomations_gradings.REVISION_NO = $revision
										AND trn_sampleinfomations_gradings.COMBO = '$combo' 
										AND trn_sampleinfomations_gradings.PRINT = '$print'
										AND trn_sampleinfomations_gradings.SIZE  = '$size'";	
										//echo $sql;
										$result = $db->RunQuery2($sql);
										$row 	= mysqli_fetch_array($result);
										 if(strtolower($row['SIZE'])==strtolower($size)) 
												return 1;
					}
}
//-----------------------------------------
function checkfordataInGrading($serialNo,$year,$salesOrderNo)
{
	 global $db;
	 
	 $sql_get_sample = 
					"select * from trn_orderdetails
					WHERE
					trn_orderdetails.intOrderNo =   $serialNo AND
					trn_orderdetails.intOrderYear =  $year AND
					trn_orderdetails.intSalesOrderId = $salesOrderNo";
					$result_smple = $db->RunQuery2($sql_get_sample);
					while($row_sample 	= mysqli_fetch_array($result_smple))
					{
								$sample_no 		= $row_sample['intSampleNo'];
								$sample_yr 		= $row_sample['intSampleYear'];
								$revision	 	= $row_sample['intRevisionNo'];
								$combo 			= $row_sample['strCombo'];
								$print 			= $row_sample['strPrintName'];

						  $sql = "SELECT * FROM
									trn_sampleinfomations_gradings
									WHERE
									trn_sampleinfomations_gradings.SAMPLE_NO = $sample_no  
									AND trn_sampleinfomations_gradings.SAMPLE_YEAR =$sample_yr
									AND trn_sampleinfomations_gradings.REVISION_NO = $revision
									AND trn_sampleinfomations_gradings.COMBO = '$combo' 
									AND trn_sampleinfomations_gradings.PRINT = '$print'
									";	
									//echo $sql;
									$result = $db->RunQuery2($sql);
									$row 	= mysqli_fetch_array($result);
									if ($row['SAMPLE_NO']!="")
									{ 
										return 1;
									}
					}
}
//-----------------------------------------
function getNextOrderNo($poLocation)
	{
		global $db;
		global $companyId;
		//global $poLocation;
		
		$sql_h = "SELECT
				mst_companies.strName,
				mst_locations.strName,
				mst_locations.HEAD_OFFICE_FLAG,
				mst_locations.intId AS loc,
				mst_companies.intId AS comp,
				(select Loc.intId from mst_locations as Loc where Loc.intCompanyId=mst_locations.intCompanyId and Loc.HEAD_OFFICE_FLAG=1 limit 1) as ho
				FROM
				mst_locations
				INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_companies.intStatus = 1 AND
				mst_locations.intStatus = 1 AND
				mst_locations.intId = '$poLocation'
				ORDER BY
				mst_companies.strName ASC,
				mst_locations.strName ASC
				";	
		$result_h 		= $db->RunQuery2($sql_h);
		$row_h 			= mysqli_fetch_array($result_h);
		$ho_location 	= $row_h['ho'];
		
		
		$sql = "SELECT
				sys_no.intOrderNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$ho_location'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextOrderNo = $row['intOrderNo'];
		
		$sql = "UPDATE `sys_no` SET intOrderNo=intOrderNo+1 WHERE (`intCompanyId`='$ho_location')  ";
		$db->RunQuery2($sql);
		
		return $nextOrderNo;
	}

//---------------------------------------------	
	function getNextOrderNo_1()
	{
		global $db;
		global $companyId;
		$sql = "SELECT
				sys_no.intOrderNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$ho_location'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextOrderNo = $row['intOrderNo'];
		
		$sql = "UPDATE `sys_no` SET intOrderNo=intOrderNo+1 WHERE (`intCompanyId`='$ho_location')  ";
		$db->RunQuery2($sql);
		
		return $nextOrderNo;
	}
//-------------------------------------------------------------------------
function getSalesOrderQty($orderNo,$year,$salesOrderNo){
	
	global $db;
	         	$sql = "SELECT
			trn_orderdetails.intQty,
			trn_orderdetails.dblOverCutPercentage,
			trn_orderdetails.dblDamagePercentage
			FROM trn_orderdetails 
			WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$year' AND
				trn_orderdetails.intSalesOrderId =  '$salesOrderNo'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		$value=$rows['intQty'];
		return (float)($value);
}
	
//-------------------------------------------------------------------------
function getRcvQty($orderNo,$year,$salesOrderNo,$size){
	global $db;
	 $sql1 = "SELECT
Sum(ware_fabricreceiveddetails.dblQty) AS RcvQty
FROM
ware_fabricreceivedheader
Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
WHERE
ware_fabricreceivedheader.intOrderNo =  '$orderNo' AND
ware_fabricreceivedheader.intOrderYear =  '$year' AND
ware_fabricreceivedheader.intStatus =  '1' AND
ware_fabricreceiveddetails.intSalesOrderId =  '$salesOrderNo' AND
ware_fabricreceiveddetails.strSize =  '$size'
GROUP BY
ware_fabricreceivedheader.intOrderNo,
ware_fabricreceivedheader.intOrderYear,
ware_fabricreceiveddetails.intSalesOrderId,
ware_fabricreceiveddetails.strSize";	
	$result1 = $db->RunQuery2($sql1);
	$row1=mysqli_fetch_array($result1);
	$RcvQty=$row1['RcvQty'];
	return $RcvQty;
	
}
//---------------------------------------	
function getNextSalesOrderId($orderNo,$orderYear)
{
	global $db;
	$sql = "SELECT
				Max(trn_orderdetails.intSalesOrderId) as maxNo
			FROM trn_orderdetails
			WHERE
				trn_orderdetails.intOrderNo 	=  '$orderNo' AND
				trn_orderdetails.intOrderYear 	=  '$orderYear'
			";
	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
	return $row['maxNo'];
}
//--------------------------------------------------------
function getSalesOrderWiseRcvQty($orderNo,$orderYear,$salesOrderId){
	global $db;
	
	 $sqlp = "SELECT
IFNULL(Sum(ware_fabricreceiveddetails.dblQty),0) AS qty
FROM
ware_fabricreceivedheader
Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
WHERE
ware_fabricreceivedheader.intStatus =  '1' AND
ware_fabricreceivedheader.intOrderNo =  '$orderNo' AND
ware_fabricreceivedheader.intOrderYear =  '$orderYear' AND
ware_fabricreceiveddetails.intSalesOrderId =  '$salesOrderId'";	
					
	$resultp = $db->RunQuery2($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	$rcv= $rowp['qty'];
	
	
	$sql = "SELECT
			IFNULL(sum(ware_stocktransactions_fabric.dblQty*-1),0) as dblQty 
			FROM ware_stocktransactions_fabric
			WHERE
 			ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
			ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
			ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
 			ware_stocktransactions_fabric.strType in(  'Dispatched_F' ,'Dispatched_P', 'Dispatched_CUT_RET')
			GROUP BY
			ware_stocktransactions_fabric.intOrderNo,
			ware_stocktransactions_fabric.intOrderYear,
			ware_stocktransactions_fabric.intSalesOrderId,
			ware_stocktransactions_fabric.strSize";

	$result = $db->RunQuery2($sql);
	$rows = mysqli_fetch_array($result);
	$retQty =$rows['dblQty'];
	
	$val=$rcv-$retQty;
	
	return $val;
}
//-----------------------------------------------------------
function loadSalesOrderWiseExcessQty($orderNo,$orderYear,$salesOrderId,$excessFactor)
{
		global $db;
	         	$sql = "SELECT
			trn_orderdetails.intQty,
			trn_orderdetails.dblOverCutPercentage,
			trn_orderdetails.dblDamagePercentage
			FROM trn_orderdetails 
			WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_orderdetails.intSalesOrderId =  '$salesOrderId'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		$value=$rows['intQty']*($rows['dblOverCutPercentage']+$excessFactor)/100;
		return (float)($value);
}
//--------------------------------------------------
function loadSizeWiseExcessQty($orderNo,$orderYear,$salesOrderId,$size,$excessFactor){
	global $db;
	        	$sql1 = "SELECT
				IFNULL(trn_ordersizeqty.dblQty,0) as dblQty,
				IFNULL(trn_orderdetails.dblOverCutPercentage,0) as dblOverCutPercentage,
				IFNULL(trn_orderdetails.dblDamagePercentage,0) as dblDamagePercentage 
				FROM trn_orderdetails 
				Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_orderdetails.intSalesOrderId =  '$salesOrderId' AND
				trn_ordersizeqty.strSize =  '$size' 
				GROUP BY
				trn_ordersizeqty.intOrderNo,
				trn_ordersizeqty.intOrderYear,
				trn_ordersizeqty.intSalesOrderId,
				trn_ordersizeqty.strSize";
					
	$result1 = $db->RunQuery2($sql1);
	$row1=mysqli_fetch_array($result1);
	$value=$row1['dblQty']*($row1['dblOverCutPercentage']+$excessFactor)/100;
	return $value;
	
}
//----------------------------------------------------
function checkSizes($orderNo,$orderYear)
{
	global $db;
	$sql = "SELECT
				trn_orderdetails.intOrderNo,
				trn_orderdetails.intOrderYear,
				trn_orderdetails.strSalesOrderNo,
				trn_ordersizeqty.strSize,
				trn_ordersizeqty.dblQty
			FROM
			trn_orderdetails
				Left Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_ordersizeqty.intSalesOrderId = trn_orderdetails.intSalesOrderId
			WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_ordersizeqty.strSize IS NULL 
			";
	$result = $db->RunQuery2($sql);
	$rows=mysqli_num_rows($result);
	if($rows>0)
		return false;
	else
		return true;
}
//-----------------------------------------------------------
	function getSaveStatus($serialNo,$year)
	{
		global $db;
	  	  $sql = "SELECT trn_orderheader.intStatus, trn_orderheader.intApproveLevelStart FROM trn_orderheader WHERE (`intOrderNo`='$serialNo') AND (`intOrderYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($status==-1) || ($row['intStatus']==($row['intApproveLevelStart']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			//echo $editableFlag;
			
		return $editableFlag;
	}
//-----------------------------------------------------------
	function getLocationValidation($type,$location,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
		$sql = "SELECT
					trn_orderheader.intLocationId
					FROM
					trn_orderheader
					Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
					WHERE
					trn_orderheader.intOrderNo =  '$serialNo' AND
					trn_orderheader.intOrderYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			if($location == $row['intLocationId'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function loadConfirmatonMode-------------------

function loadConfirmatonMode($programCode,$intUser,$savedStat,$intStatus){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery2($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0) || ($intStatus==-1)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function load loadEditMode---------------------
function getDuplicatePoCustPO($customerPO,$customer,$serialNo,$year){
	global $db;
	$serial=$serialNo.'/'.$year;
	//echo $savedStat;
	
	$customerPO = trim($customerPO);
	$flag=0;
	$sqlp = "SELECT
		trn_orderheader.intOrderNo
		FROM trn_orderheader
		WHERE
		trn_orderheader.intCustomer =  '$customer' AND
		trim(trn_orderheader.strCustomerPoNo) =  '$customerPO' AND
		concat(trn_orderheader.intOrderNo,'/',trn_orderheader.intOrderYear) <>  '$serial' AND 
		trn_orderheader.intStatus <>  '-2' AND 
		trn_orderheader.strCustomerPoNo <>  ''
		";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
	 if($rowp['intOrderNo']!=''){
		 $flag=1;
	 }
			 
	return $flag;
}
//--------------------------------------------------------
function sendEmails($serialNo,$year,$customerPO,$programCode,$savedStat,$intStatus,$companyId,$objMail){
	global $db;
				
		$k=$savedStat+2-$intStatus;
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail
			FROM menupermision 
			Inner Join menus ON menupermision.intMenuId = menus.intId 
			Inner Join sys_users ON menupermision.intUserId = sys_users.intUserId
			Inner Join mst_locations_user ON sys_users.intUserId = mst_locations_user.intUserId
			Inner Join mst_locations ON mst_locations_user.intLocationId = mst_locations.intId
			WHERE
			menus.strCode =  '$programCode' AND 
			menupermision.int".$k."Approval='1'  AND 
			mst_locations.intCompanyId='$companyId'  
			GROUP BY
			sys_users.intUserId	";	
				
		 $result = $db->RunQuery2($sql);
		 
		while($row=mysqli_fetch_array($result))
		{
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="CUSTOMER PO FOR APPROVAL ($serialNo/$year)"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST['no'] 		= $serialNo;
			$_REQUEST['year'] 		= $year;
			$_REQUEST['customerPO'] 		= $customerPO;
			$objMail->send_Response_Mail2('mail_placeOrder.php',$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
		} 
}

//------------------------------function get MaxAppByStatus---------------------
function getMaxAppByStatus($serialNo,$year){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	  $sql = "SELECT
				max(trn_orderheader_approvedby.intStatus) as status 
				FROM
				trn_orderheader_approvedby
				WHERE
				trn_orderheader_approvedby.intOrderNo =  '$serialNo' AND
				trn_orderheader_approvedby.intYear =  '$year'";
				
		$resultm = $db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm['status'];

}
//-----------------------------------------------------------
function loadDamageReturnedQty($location,$orderNo,$orderYear,$salesOrderId)
{
		global $db;
		$sql = "SELECT
				IFNULL(sum(ware_stocktransactions_fabric.dblQty*-1),0) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				/*ware_stocktransactions_fabric.intLocationId =  '$location' AND*/
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strType in(  'Dispatched_F' ,'Dispatched_P', 'Dispatched_CUT_RET')
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return (float)($rows['dblQty']);
}
function getSieWiseQty($serialNo,$year,$salesOrderId){
	global $db;
		$sql = "SELECT
					sum(trn_ordersizeqty.dblQty) as sizeWiseTotal
					FROM `trn_ordersizeqty`
				WHERE
					trn_ordersizeqty.intOrderNo = '$serialNo' AND
					trn_ordersizeqty.intOrderYear = '$year' AND
					trn_ordersizeqty.intSalesOrderId = '$salesOrderId'
				";
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return (float)($rows['sizeWiseTotal']);
}

function checkForSpecialTechniqueConsumption($sampleNo,$sampleYear,$revNo,$combo,$printName){
		global $db;
		$to_save	=0;
		$saved		=0;
	
		$sp_cosn_aaproved_date = get_sp_cosn_aaproved_date($sampleNo,$sampleYear,$revNo,$combo,$printName);
		
		$sqlm = "SELECT DISTINCT
				trn_sampleinfomations_details.intColorId,
				mst_techniques.strName AS TECHNIQUE,
				trn_sampleinfomations_details.intTechniqueId,
				trn_sampleinfomations_details.intItem,
				mst_item.strName as item,
				mst_item.foil_width,
				mst_item.foil_height, 
				trn_sampleinfomations_details.intItem,
				(size_w+0.5) as size_w,
				(size_h+0.5) as size_h,
				trn_sampleinfomations_details.dblQty as qty,
				0 as dblMeters, 
				'' as strCuttingSide ,
				'new' as status,
				
				trn_sampleinfomations_details.strPrintName, 
				trn_sampleinfomations_details.intItem,(size_w+0.5) as size_w,(size_h+0.5) as size_h,
				mst_item.strName  ,'new' as status  
				FROM
				trn_sampleinfomations_details
				Right Join mst_item ON trn_sampleinfomations_details.intItem = mst_item.intId 
				INNER JOIN mst_techniques ON trn_sampleinfomations_details.intTechniqueId=mst_techniques.intId
				WHERE
				trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
				trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
				trn_sampleinfomations_details.strComboName 		=  '$combo' AND 
				trn_sampleinfomations_details.intRevNo =  '$revNo' AND
				trn_sampleinfomations_details.strPrintName =  '$printName'"; 
				if($sp_cosn_aaproved_date!='' && $sp_cosn_aaproved_date < '2017-08-29'){
				$sqlm .=" AND 
				mst_techniques.intRoll =  '1'  AND CHANGE_CONPC_AFTER_SAMPLE =1 ";
				}
				else{
				$sqlm .=" AND mst_item.ROLL_FORM =  '1' AND CHANGE_CONPC_AFTER_SAMPLE =1  AND 
				mst_item.intUOM <> 6 ";
				}

			$sqlm .=" ORDER BY
				mst_item.strName ASC
			";
			$result_tech = $db->RunQuery2($sqlm);
			while($rowm=mysqli_fetch_array($result_tech))
			{
				$to_save++;
				$intColorId		= $rowm['intColorId'];
				$technique 		= $rowm['intTechniqueId'];
				$techniqueName 	= $rowm['TECHNIQUE'];
				$foilItem 		= $rowm['intItem'];
				$foilItemName 	= $rowm['item']." (".$rowm['foil_width']."/".$rowm['foil_height']." )";

				 	$sqlm_saved = "SELECT 
						mst_techniques.strName AS TECHNIQUE,
						trn_sample_foil_consumption.intTechniqueId,
						trn_sample_foil_consumption.intItem,
						mst_item.strName as item,
						mst_item.foil_width,
						mst_item.foil_height, 
						trn_sample_foil_consumption.dblWidth as size_w,
						trn_sample_foil_consumption.dbHeight as size_h,
						trn_sample_foil_consumption.dblFoilQty as qty,
						trn_sample_foil_consumption.dblMeters, 
						trn_sample_foil_consumption.strCuttingSide ,
						'saved' as status
						FROM trn_sample_foil_consumption  
						INNER JOIN mst_techniques ON trn_sample_foil_consumption.intTechniqueId = mst_techniques.intId
						INNER Join mst_item ON trn_sample_foil_consumption.intItem = mst_item.intId 
						WHERE
						trn_sample_foil_consumption.intSampleNo =  '$sampleNo' AND
						trn_sample_foil_consumption.intSampleYear =  '$sampleYear' AND
						trn_sample_foil_consumption.intRevisionNo =  '$revNo' AND
						trn_sample_foil_consumption.strCombo =  '$combo' AND
						trn_sample_foil_consumption.strPrintName =  '$printName'AND 
						trn_sample_foil_consumption.intColorId =  '$intColorId'AND 
						trn_sample_foil_consumption.intTechniqueId =  '$technique'AND 
						trn_sample_foil_consumption.intItem =  '$foilItem' ";
						if($sp_cosn_aaproved_date!='' && $sp_cosn_aaproved_date < '2017-08-29'){
						$sqlm_saved .=" AND 
						mst_techniques.intRoll =  '1' AND CHANGE_CONPC_AFTER_SAMPLE =1 ";
						}
						else{
						$sqlm_saved .=" AND mst_item.ROLL_FORM =  '1' AND CHANGE_CONPC_AFTER_SAMPLE =1  AND 
						mst_item.intUOM <> 6 ";
						}
						$sqlm_saved .=" AND trn_sample_foil_consumption.dblMeters > 0 ";	
				
						$flag1=0;
						$result_saved = $db->RunQuery2($sqlm_saved);
						while($row_saved=mysqli_fetch_array($result_saved))
						{
							$flag1=1;
							$saved++;
						}
						
						if($flag1==0){
							
							$sqlm_saved = "SELECT
							mst_techniques.strName AS TECHNIQUE,
							trn_sample_spitem_consumption.intTechniqueId,
							mst_item.strName as item,
							mst_item.foil_width,
							mst_item.foil_height, 
							trn_sample_spitem_consumption.intTechniqueId,
							trn_sample_spitem_consumption.intItem,
							trn_sample_spitem_consumption.dblQty
							FROM trn_sample_spitem_consumption
							INNER JOIN mst_techniques ON trn_sample_spitem_consumption.intTechniqueId = mst_techniques.intId
							INNER Join mst_item ON trn_sample_spitem_consumption.intItem = mst_item.intId 
							WHERE
							trn_sample_spitem_consumption.intSampleNo =  '$sampleNo' AND
							trn_sample_spitem_consumption.intSampleYear =  '$sampleYear' AND
							trn_sample_spitem_consumption.intRevisionNo =  '$revNo' AND
							trn_sample_spitem_consumption.strCombo =  '$combo' AND
							trn_sample_spitem_consumption.strPrintName =  '$printName' AND 
							trn_sample_spitem_consumption.intColorId =  '$intColorId' AND 
							trn_sample_spitem_consumption.intTechniqueId =  '$technique'AND 
							trn_sample_spitem_consumption.intItem =  '$foilItem' ";

							if($sp_cosn_aaproved_date!='' && $sp_cosn_aaproved_date < '2017-08-29'){
							$sqlm_saved .= " AND mst_techniques.intRoll =  '1' AND CHANGE_CONPC_AFTER_SAMPLE =1 ";
							}
							else{
							$sqlm_saved .= " AND mst_item.ROLL_FORM <>  '1' AND CHANGE_CONPC_AFTER_SAMPLE =1  AND mst_item.intUOM <> 6 ";
							}
							$sqlm_saved .= " and trn_sample_spitem_consumption.dblQty > 0 ";


							$result_saved = $db->RunQuery2($sqlm_saved);
							//$flag1=0;
							while($row_saved=mysqli_fetch_array($result_saved))
							{
							$flag1=1;
							$saved++;
							$technique 		= $row_saved['intTechniqueId'];
							}							
						}
				
						if($flag1==0){
							$colorReceipt	=getRecieptAvailable($sampleNo,$sampleYear,$revNo,$combo,$printName,$intColorId,$technique,$foilItem);
							if($colorReceipt === 1){
								$saved++;
							}
						}
				
			}
			
		$sqlm = "SELECT
										trn_sampleinfomations_details.intColorId,
										mst_techniques.strName AS TECHNIQUE,
										trn_sampleinfomations_details.intTechniqueId,
										mst_item.strName as item,
										trn_sampleinfomations_details.intItem,
										trn_sampleinfomations_details.dblQty
								FROM
								trn_sampleinfomations_details
								Right Join mst_item ON trn_sampleinfomations_details.intItem = mst_item.intId 
								INNER JOIN mst_techniques ON trn_sampleinfomations_details.intTechniqueId=mst_techniques.intId
								WHERE
								trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
								trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
								trn_sampleinfomations_details.strComboName 		=  '$combo' AND 
								trn_sampleinfomations_details.intRevNo =  '$revNo' AND
								trn_sampleinfomations_details.strPrintName =  '$printName' ";
								if($sp_cosn_aaproved_date!='' && $sp_cosn_aaproved_date < '2017-08-29'){

								$sqlm .=" AND 
								mst_techniques.intRoll <>  '1' AND CHANGE_CONPC_AFTER_SAMPLE =1 ";
								}
								else{
								$sqlm .=" AND mst_item.ROLL_FORM <>  '1' AND CHANGE_CONPC_AFTER_SAMPLE =1  AND 
								mst_item.intUOM <> 6 ";
								}
			
							$sqlm .="ORDER BY
								mst_item.strName ASC
			";
			$result_tech = $db->RunQuery2($sqlm);
			while($rowm=mysqli_fetch_array($result_tech))
			{
				$to_save++;
				$intColorId 	= $rowm['intColorId'];
				$technique 		= $rowm['intTechniqueId'];
				$techniqueName 	= $rowm['TECHNIQUE'];
				$foilItem 		= $rowm['intItem'];
				$foilItemName 	= $rowm['item'];

				 	$sqlm_saved = "SELECT
										mst_techniques.strName AS TECHNIQUE,
										trn_sample_spitem_consumption.intTechniqueId,
										mst_item.strName as item,
										mst_item.foil_width,
										mst_item.foil_height, 
										trn_sample_spitem_consumption.intTechniqueId,
										trn_sample_spitem_consumption.intItem,
										trn_sample_spitem_consumption.dblQty
										FROM trn_sample_spitem_consumption
										INNER JOIN mst_techniques ON trn_sample_spitem_consumption.intTechniqueId = mst_techniques.intId
										INNER Join mst_item ON trn_sample_spitem_consumption.intItem = mst_item.intId 
										WHERE
										trn_sample_spitem_consumption.intSampleNo =  '$sampleNo' AND
										trn_sample_spitem_consumption.intSampleYear =  '$sampleYear' AND
										trn_sample_spitem_consumption.intRevisionNo =  '$revNo' AND
										trn_sample_spitem_consumption.strCombo =  '$combo' AND
										trn_sample_spitem_consumption.strPrintName =  '$printName' AND 
										trn_sample_spitem_consumption.intColorId =  '$intColorId' AND 
										trn_sample_spitem_consumption.intTechniqueId =  '$technique'AND 
										trn_sample_spitem_consumption.intItem =  '$foilItem' ";
										
										if($sp_cosn_aaproved_date!='' && $sp_cosn_aaproved_date < '2017-08-29'){
										$sqlm_saved .= " AND mst_techniques.intRoll <>  '1' AND CHANGE_CONPC_AFTER_SAMPLE =1 ";
										}
										else{
										$sqlm_saved .= " AND mst_item.ROLL_FORM <>  '1' AND CHANGE_CONPC_AFTER_SAMPLE =1  AND mst_item.intUOM <> 6 ";
										}
										$sqlm_saved .= " and trn_sample_spitem_consumption.dblQty > 0 ";
										 	
			
						$result_saved = $db->RunQuery2($sqlm_saved);
						$flag2=0;
						while($row_saved=mysqli_fetch_array($result_saved))
						{
							$flag2=1;
							$saved++;
							$technique 		= $row_saved['intTechniqueId'];
						}
						if($flag2==0){
							$sqlm_saved = "SELECT 
								mst_techniques.strName AS TECHNIQUE,
								trn_sample_foil_consumption.intTechniqueId,
								trn_sample_foil_consumption.intItem,
								mst_item.strName as item,
								mst_item.foil_width,
								mst_item.foil_height, 
								trn_sample_foil_consumption.dblWidth as size_w,
								trn_sample_foil_consumption.dbHeight as size_h,
								trn_sample_foil_consumption.dblFoilQty as qty,
								trn_sample_foil_consumption.dblMeters, 
								trn_sample_foil_consumption.strCuttingSide ,
								'saved' as status
								FROM trn_sample_foil_consumption  
								INNER JOIN mst_techniques ON trn_sample_foil_consumption.intTechniqueId = mst_techniques.intId
								INNER Join mst_item ON trn_sample_foil_consumption.intItem = mst_item.intId 
								WHERE
								trn_sample_foil_consumption.intSampleNo =  '$sampleNo' AND
								trn_sample_foil_consumption.intSampleYear =  '$sampleYear' AND
								trn_sample_foil_consumption.intRevisionNo =  '$revNo' AND
								trn_sample_foil_consumption.strCombo =  '$combo' AND
								trn_sample_foil_consumption.strPrintName =  '$printName'AND 
								trn_sample_foil_consumption.intColorId =  '$intColorId'AND 
								trn_sample_foil_consumption.intTechniqueId =  '$technique'AND 
								trn_sample_foil_consumption.intItem =  '$foilItem' ";
								if($sp_cosn_aaproved_date!='' && $sp_cosn_aaproved_date < '2017-08-29'){
								$sqlm_saved .=" AND 
								mst_techniques.intRoll <>  '1' AND CHANGE_CONPC_AFTER_SAMPLE =1 ";
								}
								else{
								$sqlm_saved .=" AND mst_item.ROLL_FORM =  '1' AND CHANGE_CONPC_AFTER_SAMPLE =1  AND 
								mst_item.intUOM <> 6 ";
								}
								$sqlm_saved .=" AND trn_sample_foil_consumption.dblMeters > 0 ";	

								//$flag2=0;
								$result_saved = $db->RunQuery2($sqlm_saved);
								while($row_saved=mysqli_fetch_array($result_saved))
								{
									$flag2=1;
									$saved++;
								}							
						}
				
						
						if($flag2==0){
							$colorReceipt	=getRecieptAvailable($sampleNo,$sampleYear,$revNo,$combo,$printName,$intColorId,$technique,$foilItem);
							if($colorReceipt === 1){
								$saved++;
							}
						}
			}
			
		if($saved == $to_save || $to_save ==0)
			return 1;
		else
			return 0;	
			
}

 	
function getApprovedSataus($sampleNo,$sampleYear,$revNo,$combo,$printName){
		
		global $db;
		
		$flag_app=1;
 		
		$sqlm = "SELECT DISTINCT
				
				mst_techniques.strName AS TECHNIQUE,
				trn_sampleinfomations_details.intTechniqueId,
				trn_sampleinfomations_details.intItem,
				mst_item.strName as item,
				mst_item.foil_width,
				mst_item.foil_height, 
				trn_sampleinfomations_details.intItem,
				(size_w+0.5) as size_w,
				(size_h+0.5) as size_h,
				trn_sampleinfomations_details.dblQty as qty,
				0 as dblMeters, 
				'' as strCuttingSide ,
				'new' as status,
				
				trn_sampleinfomations_details.strPrintName, 
				trn_sampleinfomations_details.intItem,(size_w+0.5) as size_w,(size_h+0.5) as size_h,
				mst_item.strName  ,'new' as status  
				FROM
				trn_sampleinfomations_details
				Right Join mst_item ON trn_sampleinfomations_details.intItem = mst_item.intId 
				INNER JOIN mst_techniques ON trn_sampleinfomations_details.intTechniqueId=mst_techniques.intId
				WHERE
				trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
				trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
				trn_sampleinfomations_details.strComboName 		=  '$combo' AND 
				trn_sampleinfomations_details.intRevNo =  '$revNo' AND
				trn_sampleinfomations_details.strPrintName =  '$printName' /*AND 
				mst_techniques.intRoll =  '1'  AND CHANGE_CONPC_AFTER_SAMPLE =1*/
				AND mst_item.ROLL_FORM =  '1' AND CHANGE_CONPC_AFTER_SAMPLE =1  AND 
								mst_item.intUOM <> 6

			ORDER BY
				mst_item.strName ASC
			";
			$result_tech = $db->RunQuery2($sqlm);
			while($rowm=mysqli_fetch_array($result_tech))
			{
	
				$sql	="SELECT
							trn_sample_special_technical_header.`STATUS`
							FROM `trn_sample_special_technical_header`
							WHERE
							trn_sample_special_technical_header.SAMPLE_NO = '$sampleNo' AND
							trn_sample_special_technical_header.SAMPLE_YEAR = '$sampleYear' AND
							trn_sample_special_technical_header.REVISION_NO = '$revNo' AND
							trn_sample_special_technical_header.COMBO = '$combo' AND
							trn_sample_special_technical_header.PRINT = '$printName'
							";
					
				$result = $db->RunQuery2($sql);
				$row	=mysqli_fetch_array($result);
				
				if($row['STATUS']!=1)
					$flag_app=0;
			}
			
		$sqlm = "SELECT
										mst_techniques.strName AS TECHNIQUE,
										trn_sampleinfomations_details.intTechniqueId,
										mst_item.strName as item,
										trn_sampleinfomations_details.intItem,
										trn_sampleinfomations_details.dblQty
								FROM
								trn_sampleinfomations_details
								Right Join mst_item ON trn_sampleinfomations_details.intItem = mst_item.intId 
								INNER JOIN mst_techniques ON trn_sampleinfomations_details.intTechniqueId=mst_techniques.intId
								WHERE
								trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
								trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
								trn_sampleinfomations_details.strComboName 		=  '$combo' AND 
								trn_sampleinfomations_details.intRevNo =  '$revNo' AND
								trn_sampleinfomations_details.strPrintName =  '$printName' /*AND 
								mst_techniques.intRoll <>  '1' AND CHANGE_CONPC_AFTER_SAMPLE =1*/
								AND mst_item.ROLL_FORM <>  '1' AND CHANGE_CONPC_AFTER_SAMPLE =1  AND 
								mst_item.intUOM <> 6
			
							ORDER BY
								mst_item.strName ASC
			";
			$result_tech = $db->RunQuery2($sqlm);
			while($rowm=mysqli_fetch_array($result_tech))
			{
	
				$sql	="SELECT
							trn_sample_special_technical_header.`STATUS`
							FROM `trn_sample_special_technical_header`
							WHERE
							trn_sample_special_technical_header.SAMPLE_NO = '$sampleNo' AND
							trn_sample_special_technical_header.SAMPLE_YEAR = '$sampleYear' AND
							trn_sample_special_technical_header.REVISION_NO = '$revNo' AND
							trn_sample_special_technical_header.COMBO = '$combo' AND
							trn_sample_special_technical_header.PRINT = '$printName'
							";
					
				$result = $db->RunQuery2($sql);
				$row	=mysqli_fetch_array($result);
				
				if($row['STATUS']!=1)
					$flag_app=0;
			}
			
			return $flag_app;
			
}

function getChildFlag($orderNo,$orderYear,$salesOrderId){
	
	global $db;
	$child_count	=0;
 	$sql	= "SELECT
				1 as flag
				FROM
				trn_orderheader
				INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE 
				trn_orderheader.intStatus <> '-2' AND
				trn_orderheader.DUMMY_PO_NO = '$orderNo' AND
				trn_orderheader.DUMMY_PO_YEAR = '$orderYear' AND
				trn_orderdetails.DUMMY_SO_ID = '$salesOrderId' limit 1
				";				
	$result 	= $db->RunQuery2($sql);
	$row		= mysqli_fetch_array($result);
	$child_count= $row['flag'];
	return $child_count;
	
}

function getChildFlag_order($orderNo,$orderYear,$salesOrderId){
	
	global $db;
	$child_count	=0;
 	$sql	= "SELECT
				1 as flag
				FROM
				trn_orderheader
				INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderheader.DUMMY_PO_NO = '$orderNo' AND
				trn_orderheader.DUMMY_PO_YEAR = '$orderYear' AND
				trn_orderdetails.DUMMY_SO_ID = '$salesOrderId'  
				  limit 1
				";				
	$result 	= $db->RunQuery2($sql);
	$row		= mysqli_fetch_array($result);
	$child_count= $row['flag'];
	return $child_count;
	
}

function getDummyQtyMatchingFlag($orderNo,$orderYear,$childNo,$childYear,$salesOrderId,$qty){

	global $db;
	
	$qty_child_saved	=0;
	$errflag			=0;
	$max_qty			=0;
	
	/* max_qty	= ini_dummy -( tot_child but not this sales order)*/
 	
	//----------------------------------------------------
	/*
 	$sql_d	= "SELECT
				trn_orderheader.intStatus,
				trn_orderheader.intApproveLevelStart 
				FROM
				trn_orderheader
				INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderheader.intOrderNo = '$orderNo' AND
				trn_orderheader.intOrderYear = '$orderYear' AND
				trn_orderdetails.intSalesOrderId = '$salesOrderId'  
				";				
	$result_d 	= $db->RunQuery2($sql_d);
	$row_d		= mysqli_fetch_array($result_d);
	$initial_dummy		= $row_d['DUMMY_SO_INITIAL_QTY']; */
	
	//-----split for different order--------------------
	$sql1	= "SELECT
				trn_orderdetails.DUMMY_SO_ID 
				FROM
				trn_orderheader
				INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderheader.DUMMY_PO_NO = '$orderNo' AND
				trn_orderheader.DUMMY_PO_YEAR = '$orderYear' AND
				trn_orderdetails.intSalesOrderId = '$salesOrderId'  
				";				
	$result 	= $db->RunQuery2($sql1);
	$row		= mysqli_fetch_array($result);
	$dummy_so	= $row['DUMMY_SO_ID'];
	//-----split within same order--------------------
	$sql2	= "SELECT
				ifnull(sum(intQty),0) as qty
				FROM
				trn_orderheader
				INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderheader.intOrderNo = '$orderNo' AND
				trn_orderheader.intOrderYear = '$orderYear' AND
				trn_orderdetails.DUMMY_SO_ID = '$dummy_so'  
				";				
	$result 			= $db->RunQuery2($sql2);
	$row				= mysqli_fetch_array($result);
	$qty_child_saved	= $row['qty'];
	
	$sql3	= "SELECT
				ifnull(sum(intQty),0) as qty
				FROM
				trn_orderheader
				INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderheader.DUMMY_PO_NO = '$orderNo' AND
				trn_orderheader.DUMMY_PO_YEAR = '$orderYear' AND 
				trn_orderdetails.DUMMY_SO_ID = '$dummy_so'  
				";				
	$result 			= $db->RunQuery2($sql3);
	$row				= mysqli_fetch_array($result);
	$qty_child_saved	+= $row['qty'];
	
	$sql4	= "SELECT
				ifnull(sum(intQty),0) as qty
				FROM
				trn_orderheader
				INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderheader.intOrderNo = '$childNo' AND
				trn_orderheader.intOrderYear = '$childYear' AND 
				trn_orderdetails.DUMMY_SO_ID = '$dummy_so'  
				";				
	$result 			= $db->RunQuery2($sql4);
	$row				= mysqli_fetch_array($result);
	$qty_child_saved	-= $row['qty'];
	
	
	
	$sql_d	= "SELECT
				ifnull(sum(intQty),0) as dummyBalance ,
				ifnull(trn_orderdetails.DUMMY_SO_INITIAL_QTY,0) as DUMMY_SO_INITIAL_QTY 
				FROM
				trn_orderheader
				INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderheader.intOrderNo = '$orderNo' AND
				trn_orderheader.intOrderYear = '$orderYear' AND
				trn_orderdetails.intSalesOrderId = '$dummy_so'  
				";				
	$result_d 	= $db->RunQuery2($sql_d);
	$row_d		= mysqli_fetch_array($result_d);
 	
	if(($row_d['DUMMY_SO_INITIAL_QTY']!= ($qty+$qty_child_saved+$row_d['dummyBalance'])) && $row_d['dummyBalance'] >0 ){
		$errflag	=1;
	/*	$max_qty	= $row_d['DUMMY_SO_INITIAL_QTY']-$qty_child_saved;
		if($max_qty <0 )
			$max_qty	=0;*/
	}
	
	$arrDummy['err']=$errflag;
	//$arrDummy['qty']=$max_qty/*."(".$row_d['DUMMY_SO_INITIAL_QTY'].'-'.$qty_child_saved.")"*//*.'/'.$sql_d*//*.' != '.$row_d['DUMMY_SO_INITIAL_QTY'].'-'.$qty*/;
	
	return $arrDummy;
	
}

function getDummyQtyBalance($orderNo,$orderYear,$salesOrderId,$qty,$childQty){

	global $db;
	$errflag		=0;
	$max_qty		=0;

 	$sql_d	= "SELECT
				trn_orderheader.PO_TYPE, 
				trn_orderdetails.DUMMY_SO_INITIAL_QTY
				FROM
				trn_orderheader
				INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderheader.intOrderNo = '$orderNo' AND
				trn_orderheader.intOrderYear = '$orderYear' AND
				trn_orderdetails.intSalesOrderId = '$salesOrderId'  
				";				
	$result_d 			= $db->RunQuery2($sql_d);
	$row_d				= mysqli_fetch_array($result_d);
	$initial_dummy		= $row_d['DUMMY_SO_INITIAL_QTY'];
	$po_type			= $row_d['PO_TYPE'];
	
	$sql	= "SELECT
				ifnull(sum(intQty),0) as qty 
				FROM
				trn_orderheader
				INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderheader.DUMMY_PO_NO = '$orderNo' AND
				trn_orderheader.DUMMY_PO_YEAR = '$orderYear' AND
				trn_orderdetails.DUMMY_SO_ID = '$salesOrderId' AND 
				 trn_orderheader.intStatus <> -2
				";				
	$result 	= $db->RunQuery2($sql);
	$row		= mysqli_fetch_array($result);
	$used_other	= $row['qty'];


	$sql	= "SELECT
				ifnull(sum(intQty),0) as qty 
				FROM
				trn_orderheader
				INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderheader.intOrderNo = '$orderNo' AND
				trn_orderheader.intOrderYear = '$orderYear' AND
				trn_orderdetails.DUMMY_SO_ID = '$salesOrderId'  AND 
				 trn_orderheader.intStatus <> -2 
				";				
	$result 	= $db->RunQuery2($sql);
	$row		= mysqli_fetch_array($result);
	$used_same_order	= $row['qty'];
	//$used_same_order	= $childQty;

	$sql	= "SELECT
				ifnull(sum(TEMP_DELETED_SPLIT),0) as qty 
				FROM
				trn_orderheader
				INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderheader.intOrderNo = '$orderNo' AND
				trn_orderheader.intOrderYear = '$orderYear' AND
				trn_orderdetails.intSalesOrderId = '$salesOrderId' 
				";				
	$result 	= $db->RunQuery2($sql);
	$row		= mysqli_fetch_array($result);
	$deleted_split	= $row['qty'];
	
	//if($deleted_split>0)
	$bal	= $initial_dummy - ($used_other+$used_same_order+$childQty);
	//if($used_other==0 || $used_other=='')
	
	//$bal	= $initial_dummy - ($used_other+$used_same_order+$deleted_split);
	if($bal < 0)
		$bal	=0;
	/*if($deleted_split > 0 && $qty < $initial_dummy ){
		$errflag		=1;
		$max_qty		=$initial_dummy-$childQty-$used_other;
	}
	else if(($used_other==0 || $used_other=='') && ($childQty==0 || $childQty==''))
		$errflag		=0;
	else if(($used_other==0 || $used_other=='') && ($childQty>0)){
		$errflag		=1;
		echo $max_qty		=$initial_dummy-$childQty;
	}
	else if($bal != $qty){
		$errflag		=1;
		$max_qty		=$bal;
	}*/
	if($po_type==1 && $initial_dummy==$bal){
		//no qty restriction
	}
	else if($po_type==1 && $initial_dummy>$bal){
		if($bal != $qty){
			$errflag		=1;
			$max_qty		=$bal;
		} 
	}
	/*else if($bal > $qty){
		$errflag		=1;
		$max_qty		=$bal;
	}*/ 
	if($initial_dummy==0 || $initial_dummy=='')
		$errflag		=0;
	
	$arrDummy['err']=$errflag;
	$arrDummy['qty']=$max_qty/*.'/'.$sql_d*//*.' != '.$row_d['DUMMY_SO_INITIAL_QTY'].'-'.$qty*/;
	$arrDummy['desc']=$initial_dummy.' - ('.$used_other.'+'.$used_same_order.')';
	
	return $arrDummy;
	
}

function get_dummy_edit_mode($dummy_no,$dummy_year){
global $programCode;
global $db;
global $userId;
			$sql = "SELECT 
			trn_orderheader.intCreator, 
			trn_orderheader.intStatus, 
			trn_orderheader.intApproveLevelStart 
			FROM trn_orderheader 
			WHERE
			trn_orderheader.intOrderNo =  '$dummy_no' AND
			trn_orderheader.intOrderYear =  '$dummy_year'";

			$result = $db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			$savedStatus=$row['intStatus'];
			$savedLevels=$row['intApproveLevelStart'];
			$createdUser=$row['intCreator'];
  		
		$editPermission=loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
		
		return $editPermission;
	
}
function getStatusOfSaveToOrder($save_to_no,$save_to_year){
	
	global $programCode;
	global $db;
	global $userId;

	$sql = "SELECT 
			trn_orderheader.intCreator, 
			trn_orderheader.intStatus, 
			trn_orderheader.intApproveLevelStart 
			FROM trn_orderheader 
			WHERE
			trn_orderheader.intOrderNo =  '$save_to_no' AND
			trn_orderheader.intOrderYear =  '$save_to_year'";

	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
	if(($row['intApproveLevelStart']+1)==$row['intStatus'] || $row['intStatus']==0)
		return 1;
	else
		return 0;
 	
}
function getMaxSalesOrder($no,$year){
	
	global $programCode;
	global $db;
	global $userId;

	$sql = "SELECT
			(max(trn_orderdetails.intSalesOrderId)+1) as so 
			FROM `trn_orderdetails`
			WHERE
			trn_orderdetails.intOrderNo = '$no' AND
			trn_orderdetails.intOrderYear = '$year'";

	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
	if($row['so'] > 0)
		return $row['so'];
	else
		return 1;
 	
}
function get_deleted_split($serialNo ,$year,$salesOrderId){
	
	global $programCode;
	global $db;
	global $userId;

	$sql = "SELECT
			trn_orderdetails.TEMP_DELETED_SPLIT   
			FROM `trn_orderdetails`
			WHERE
			trn_orderdetails.intOrderNo = '$serialNo' AND
			trn_orderdetails.intOrderYear = '$year' AND  
			trn_orderdetails.intSalesOrderId = '$salesOrderId' ";

	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
 		return $row['TEMP_DELETED_SPLIT'];
 	
}

function get_new_revision_status($sampleNo,$sampleYear,$revNo){
	
	global $db;

	$sql = "SELECT
			trn_sampleinfomations.intRevisionNo
			FROM `trn_sampleinfomations`
			WHERE
			trn_sampleinfomations.intSampleNo = '$sampleNo' AND
			trn_sampleinfomations.intSampleYear = '$sampleYear' AND
			trn_sampleinfomations.intRevisionNo > '$revNo'
			 ";

	$result = $db->RunQuery2($sql);
	$row	=mysqli_fetch_array($result);
	if($row['intRevisionNo'] > $revNo && $row['intRevisionNo'] > 0)
		return 1;
	else
		return 0;
	
}

function get_mrn_flag($serialNo,$year){

	global $db;
	$sql = "SELECT
			ware_mrndetails.intMrnNo,
			Sum(ware_mrndetails.dblQty) mrnQty
			FROM `ware_mrndetails` 
			INNER JOIN ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
			WHERE
			ware_mrndetails.intOrderNo = '$serialNo' AND
			ware_mrndetails.intOrderYear = '$year' /* AND 	ware_mrnheader.intStatus >= 1 */";

	$result = $db->RunQuery2($sql);
	$row	=mysqli_fetch_array($result);
	if($row['mrnQty'] > 0)
		return 1;
	else
		return 0;
	
}

function getRecieptAvailable($sampleNo,$sampleYear,$revisionNo,$combo,$printName,$colorID,$intTechnicleID,$ItemID){

    global $db;

    $sql	="SELECT
					trn_sample_color_recipes.intSampleNo
					FROM
					trn_sample_color_recipes
					WHERE
					trn_sample_color_recipes.intSampleNo = '$sampleNo' AND
					trn_sample_color_recipes.intSampleYear = '$sampleYear' AND
					trn_sample_color_recipes.intRevisionNo = '$revisionNo' AND
					trn_sample_color_recipes.strCombo = '$combo' AND
					trn_sample_color_recipes.strPrintName = '$printName' AND
					trn_sample_color_recipes.intColorId = '$colorID' AND
					trn_sample_color_recipes.intTechniqueId = '$intTechnicleID' AND
					trn_sample_color_recipes.intItem = '$ItemID'
					";

    $result 	= $db->RunQuery2($sql);
    $row		= mysqli_fetch_array($result);
    $status_cp		= $row['intSampleNo'];

    if($status_cp){
        return 1;
    }
    else
        return 0;

}

function get_sp_cosn_aaproved_date($sampleNo,$sampleYear,$revNo,$combo,$printName){
		
		global $db;
		$sql = "SELECT 	date(APPROVED_DATE) as date 
				FROM 
				trn_sample_special_technical_header_approved_by 
				WHERE 
				APPROVE_LEVEL =2 AND 
				STATUS =0 AND 
				SAMPLE_NO = '$sampleNo' AND
				SAMPLE_YEAR = '$sampleYear' AND
				REVISION_NO = '$revNo' AND
				COMBO = '$combo' AND
				PRINT = '$printName' ";
		
		$result 	= $db->RunQuery2($sql);
		$row		= mysqli_fetch_array($result);
		
		return $date	= $row['date'];

	
}

function getFabArrivedFlag($serialNo ,$year,$salesOrderId){
	global $db;
	$sql	="
			SELECT
			ware_fabricreceivedheader.intFabricReceivedNo
			FROM
			ware_fabricreceivedheader
			INNER JOIN ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
			WHERE
			ware_fabricreceivedheader.intOrderNo = '$serialNo' AND
			ware_fabricreceivedheader.intOrderYear = '$year' AND
			ware_fabricreceiveddetails.intSalesOrderId = '$salesOrderId' AND 
			ware_fabricreceivedheader.intStatus =1
		";
	
		$result 	= $db->RunQuery2($sql);
		$row		= mysqli_fetch_array($result);
		
		if($row['intFabricReceivedNo'] >0 && $row['intFabricReceivedNo']!='')
			return 1;
		else
			return 0;
}

function get_dummy_details($serialNo,$year,$salesOrderId){
	global $db;

	$sql		="SELECT
dummy.intSampleNo as sampleNo_d,
dummy.intSampleYear as sampleYear_d,
dummy.intRevisionNo as revNo_d,
dummy.strCombo as combo_d,
dummy.strPrintName as printName_d,
dummy_header.INSTANT_ORDER 
FROM
trn_orderdetails
INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN trn_orderdetails AS dummy ON trn_orderheader.DUMMY_PO_NO = dummy.intOrderNo AND trn_orderheader.DUMMY_PO_YEAR = dummy.intOrderYear AND trn_orderdetails.DUMMY_SO_ID = dummy.intSalesOrderId
INNER JOIN trn_orderheader AS dummy_header ON dummy.intOrderNo = dummy_header.intOrderNo AND dummy.intOrderYear = dummy_header.intOrderYear
WHERE
trn_orderdetails.intOrderNo 		= '$serialNo' AND
trn_orderdetails.intOrderYear 		= '$year' AND
trn_orderdetails.intSalesOrderId 	= '$salesOrderId'
";

	$result 	= $db->RunQuery2($sql);
	$row		= mysqli_fetch_array($result);
	return $row;
}

function save_data($finalRecord)
                {
                    echo "hdg";
                    
                } 

?>



<?php
session_start();
ini_set('display_errors',0);
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
 
$thisFilePath =  $_SERVER['PHP_SELF'];
$company 	= $_SESSION['headCompanyId'];

$intUser  = $_SESSION["userId"];
$programName='Place Order';
$programCode='P0427';

include $backwardseperator."dataAccess/Connector.php";
require_once $_SESSION['ROOT_PATH']."class/customerAndOperation/cls_textile_stores.php";

$objtexttile = new cls_texttile();
//$excessFactor 	= $objtexttile->getFRNexcessFactor($company);

$salesOrderNo=$_GET["salesOrderNo"];
$orderNo=$_GET["orderNo"];
$year=$_GET["year"];
$Qty=$_GET["Qty"];


$SYear  			= $_REQUEST['SYear'];
$sampleNo  			= $_REQUEST['sampleNo'];
$Revision  			= $_REQUEST['Revision'];
$cboprint  			= $_REQUEST['print_P'];
$combo1  			= $_REQUEST['combo'];


if(($orderNo=='')&&($year=='')){
	$savedStat = (int)getApproveLevel($programName);
	$intStatus=$savedStat+1;
}
else{
$result = loadHeader($orderNo,$year);
while($row=mysqli_fetch_array($result))
{
		$savedStat=$row['intApproveLevelStart'];
		$intStatus=$row['intStatus'];
}
}

//------------
 $sql = "SELECT
		sum(trn_ordersizeqty.dblQty) as QtySaved
		FROM trn_orderdetails 
		Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
		WHERE
		trn_ordersizeqty.intOrderNo =  '$orderNo' AND
		trn_ordersizeqty.intOrderYear =  '$year' AND
		trn_ordersizeqty.intSalesOrderId =  '$salesOrderNo'";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$balQty=$Qty-$row['QtySaved'];
//---------------

//--------select from grading tbl---------------

$sql_select = "SELECT * FROM
trn_sampleinfomations_gradings
WHERE
trn_sampleinfomations_gradings.SAMPLE_NO =  $sampleNo 
AND trn_sampleinfomations_gradings.SAMPLE_YEAR = $SYear 
AND trn_sampleinfomations_gradings.REVISION_NO = $Revision
AND trn_sampleinfomations_gradings.COMBO = '$combo1' 
AND trn_sampleinfomations_gradings.PRINT = '$cboprint'";

//echo $sql_select;

$result 		= 	$db->RunQuery($sql_select);
$size_grading   = 	$row['SIZE'];


//----------------------------------------
$toleratePercentage	= $objtexttile->getSavedTolerencePercentage($orderNo,$year,$salesOrderNo);
$excessQty=ceil(loadSalesOrderWiseExcessQty($orderNo,$year,$salesOrderNo,$toleratePercentage));


 $editMode=loadEditMode($programCode,$intStatus,$savedStat,$intUser);

?>
<title>SAMPLE DISPATCH</title>

</head>

<body>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>

    
<form id="frmPlaceOrderPopup" name="frmPlaceOrderPopup" method="post" action="">
<table width="450" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutS">
		  <div class="trans_text"> SAMPLE SIZES</div>
		  <table width="483" border="0" align="center" bgcolor="#FFFFFF">
    <td width="477"><table width="477" border="0">
      <tr>
        <td width="471"><table width="474" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="13%" height="22" class="normalfnt" align="center">Total Qty</td>
            <td width="39%" align="center"><input name="txtsalesOrderNo2" id="txtsalesOrderNo2" type="text" value="<?php echo $Qty ?>" style="text-align:center; width:80px" disabled="disabled"/><div id="excessQty" style="display:none"><?php echo $excessQty ?></div></td>
            <td width="9%" class="normalfnt" align="center">Bal Qty</td>
            <td width="16%" class="normalfnt" align="center"><input name="txtBalOrderQty" id="txtBalOrderQty" type="text" value="<?php echo $balQty ?>" style="text-align:center; width:80px" disabled="disabled"/></td>
            <td width="23%" ><input name="txtsalesOrderNo" id="txtsalesOrderNo" type="text" value="<?php echo $salesOrderNo ?>" style="display:none"/><span id="spanSalesOrderNo" style="visibility:hidden"><?php echo $salesOrderNo; ?></span></td>
            
            
            </tr>
          </table></td>
      </tr>
      <tr>
        <td align="center"><div style="width:400px;height:300px;overflow:scroll" align="center" class="tableBorder_allRound" >
          <table width="100%"  class="bordered"  id="tblSizesPopup" align="center" >
            <tr class="">
              <th width="11%" height="22" >Delele</th>
              <th width="43%" >Size</th>
              <th width="46%">Qty</th>
              <th width="46%">Received Qty</th>
              </tr>
              <?php 
				 	 $sql = "SELECT
							trn_ordersizeqty.strSize,
							trn_orderdetails.dblOverCutPercentage , 
							trn_ordersizeqty.dblQty
							FROM trn_orderdetails 
							Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
							WHERE
							trn_ordersizeqty.intOrderNo =  '$orderNo' AND
							trn_ordersizeqty.intOrderYear =  '$year' AND
							trn_ordersizeqty.intSalesOrderId =  '$salesOrderNo'";
					$result = $db->RunQuery($sql);
					$r=0;
					$rcvQty=0;
					while($row=mysqli_fetch_array($result))
					  {
						$r++;
						$rcvQty=getRcvQty($orderNo,$year,$salesOrderNo,$row['strSize']);
						if($rcvQty==''){
							$rcvQty=0;
						}
						
						$toleratePercentage	= $objtexttile->getSavedTolerencePercentage($orderNo,$year,$salesOrderNo);
						$excessQty=ceil(loadSizeWiseExcessQty($orderNo,$year,$salesOrderNo,$row['strSize'],$toleratePercentage));
						$minQty=($rcvQty-$excessQty);
						$minQty=ceil($minQty);//01-03-2013
						
			  ?>
            <tr class="normalfnt">
              <td align="center" bgcolor="#FFFFFF"><?php if($editMode==1){ ?><?php if($rcvQty==0){  ?><img src="images/del.png" width="15" height="15" class="delImgPopup" /><?php } ?><?php } ?></td>
              
                  
              <td align="center" bgcolor="#FFFFFF" id=""><input name="txtSize" type="text" id="txtSize" style="width:60px; text-align:center" value="<?php echo $row['strSize']?>" class="" /></td>
            
              <td align="center" bgcolor="#FFFFFF" id=""><input  name="txtSizeQty" type="text" id="txtSizeQty" style="width:60px; text-align:right" value="<?php echo $row['dblQty']?>"  class="validate[required,custom[number],max[<?php echo $Qty ?>],min[<?php echo 0;//$row['dblQty'] ?>]] sizeQty" /></td>
              <td align="center" bgcolor="#FFFFFF" id=""><?php echo $rcvQty; ?></td>
              </tr>
              <?php 
					}
					if($r==0)
					{
						
			$sql_select = "SELECT * FROM
			trn_sampleinfomations_gradings
			WHERE
			trn_sampleinfomations_gradings.SAMPLE_NO =  $sampleNo 
			AND trn_sampleinfomations_gradings.SAMPLE_YEAR = $SYear 
			AND trn_sampleinfomations_gradings.REVISION_NO = $Revision
			AND trn_sampleinfomations_gradings.COMBO = '$combo1' 
			AND trn_sampleinfomations_gradings.PRINT = '$cboprint'";

			$result 			= 	$db->RunQuery($sql_select);
			while($row			=	mysqli_fetch_array($result))
			{
				$size_grading   = $row['SIZE'];

			  ?>
            <tr class="normalfnt">
              <td align="center" bgcolor="#FFFFFF"><?php if($editMode==1){ ?><?php if($rcvQty==0){  ?><img src="images/del.png" width="15" height="15" class="delImgPopup" /><?php } ?><?php } ?></td>
              
                 <td align="center" bgcolor="#FFFFFF" id=""><input name="txtSize" type="text" id="txtSize" style="width:60px; text-align:center" value="<?php echo $size_grading?>" class="" /></td>
               
              <td align="center" bgcolor="#FFFFFF" id=""><input  name="txtSizeQty" type="text" id="txtSizeQty" style="width:60px;  text-align:right" value="0"  class="validate[required,custom[number],max[<?php echo $Qty ?>],min[<?php echo $rcvQty ?>]] sizeQty" /></td>
              <td align="right" bgcolor="#FFFFFF" id=""><?php echo $rcvQty; ?></td>
              </tr>
              <?php
			} // while
					if($size_grading=="")
					{
					
					?>
                    
                     <tr class="normalfnt">
              <td align="center" bgcolor="#FFFFFF"><?php if($editMode==1){ ?><?php if($rcvQty==0){  ?><img src="images/del.png" width="15" height="15" class="delImgPopup" /><?php } ?><?php } ?></td>
             
              <td align="center" bgcolor="#FFFFFF" id=""><input name="txtSize" type="text" id="txtSize" style="width:60px; text-align:center" value="" class="" /></td>
             
              <td align="center" bgcolor="#FFFFFF" id=""><input  name="txtSizeQty" type="text" id="txtSizeQty" style="width:60px;  text-align:right" value="0"  class="validate[required,custom[number],max[<?php echo $Qty ?>],min[<?php echo $rcvQty ?>]] sizeQty" /></td>
              <td align="right" bgcolor="#FFFFFF" id=""><?php echo $rcvQty; ?></td>
              </tr>
                    <?php
					}
					} //if
			  ?>
          <tr class="dataRow">
            <td colspan="4" align="left"  bgcolor="#FFFFFF" ><?php if($editMode==1){ ?><img src="images/Tadd.jpg" name="butInsertRowPopup" width="78" height="24" class="mouseover" id="butInsertRowPopup" /><?php } ?></td>
            </tr>
            </table>
          </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><?php if($editMode==1){ ?><img src="images/Tsave.jpg" width="92" height="24"  alt="add" id="butAdd" name="butAdd" class="mouseover"/><?php } ?><img src="images/Tclose.jpg" width="92" height="24" id="butClose1" name="butClose1" class="mouseover"  /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>

</body>
</html>
<?php
//-------------------------------------------------------------------------
function getRcvQty($orderNo,$year,$salesOrderNo,$size){
	global $db;
	 $sql1 = "SELECT
Sum(ware_fabricreceiveddetails.dblQty) AS RcvQty
FROM
ware_fabricreceivedheader
Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
WHERE
ware_fabricreceivedheader.intOrderNo =  '$orderNo' AND
ware_fabricreceivedheader.intOrderYear =  '$year' AND
ware_fabricreceivedheader.intStatus =  '1' AND
ware_fabricreceiveddetails.intSalesOrderId =  '$salesOrderNo' AND
ware_fabricreceiveddetails.strSize =  '$size'
GROUP BY
ware_fabricreceivedheader.intOrderNo,
ware_fabricreceivedheader.intOrderYear,
ware_fabricreceiveddetails.intSalesOrderId,
ware_fabricreceiveddetails.strSize";	
	$result1 = $db->RunQuery($sql1);
	$row1=mysqli_fetch_array($result1);
	$RcvQty=$row1['RcvQty'];
	
	//added by roshan 2014-Jan-06
	$sql1 = "SELECT
			ifnull(Sum(ware_stocktransactions_fabric.dblQty),0) as dispatchDamages
			FROM `ware_stocktransactions_fabric`
			WHERE
			ware_stocktransactions_fabric.intOrderNo 		= '$orderNo' AND
			ware_stocktransactions_fabric.intOrderYear 		= '$year' AND
			ware_stocktransactions_fabric.intSalesOrderId 	= '$salesOrderNo' AND
			ware_stocktransactions_fabric.strSize 			= '$size' AND
			ware_stocktransactions_fabric.strType IN ('Dispatched_P','Dispatched_F','Dispatched_CUT_RET')
			";
	$result1 	= 	$db->RunQuery($sql1);
	$row1		=	mysqli_fetch_array($result1);
	$RcvQty		=	$RcvQty+$row1['dispatchDamages'];
	
	return $RcvQty;
}
//----------------------------------------------------------------------
function loadSalesOrderWiseExcessQty($orderNo,$orderYear,$salesOrderId,$excessFactor)
{
		global $db;
	         	$sql = "SELECT
			trn_orderdetails.intQty,
			trn_orderdetails.dblOverCutPercentage,
			trn_orderdetails.dblDamagePercentage
			FROM trn_orderdetails 
			WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_orderdetails.intSalesOrderId =  '$salesOrderId'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		$value=$rows['intQty']*($rows['dblOverCutPercentage']+$excessFactor)/100;
		return val($value);
}

//------------------------------------------------------------------------
	function loadSizeWiseExcessQty($orderNo,$orderYear,$salesOrderId,$size,$excessFactor)
{
		global $db;
	       	$sql = "SELECT
				trn_ordersizeqty.dblQty,
				trn_orderdetails.dblOverCutPercentage,
				trn_orderdetails.dblDamagePercentage
				FROM trn_orderdetails 
				Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_orderdetails.intSalesOrderId =  '$salesOrderId' AND
				trn_ordersizeqty.strSize =  '$size' ";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		$value=$rows['dblQty']*($rows['dblOverCutPercentage']+$excessFactor)/100;
		return val($value);
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function load Header---------------------
function loadHeader($serialNo,$year)
{
	global $db;
		   $sql = "SELECT
					trn_orderheader.strCustomerPoNo,
					trn_orderheader.dtDate,
					trn_orderheader.dtDeliveryDate,
					mst_customer.strName AS strCustomer,
					mst_customer_locations_header.strName AS strCustomerLocations,
					mst_financecurrency.strCode AS currencyCode,
					trn_orderheader.strContactPerson,
					trn_orderheader.strRemark,
					A.strUserName AS marketer,
					trn_orderheader.intStatus,
					B.strUserName AS creator,intApproveLevelStart,intCustomer,intCurrency,intCustomerLocation,intMarketer
				FROM
					trn_orderheader
					Inner Join mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
					Inner Join mst_customer_locations_header ON mst_customer_locations_header.intId = trn_orderheader.intCustomerLocation
					Inner Join mst_financecurrency ON mst_financecurrency.intId = trn_orderheader.intCurrency
					Inner Join sys_users AS A ON A.intUserId = trn_orderheader.intMarketer
					Inner Join sys_users AS B ON B.intUserId = trn_orderheader.intCreator
				WHERE
					trn_orderheader.intOrderNo 		=  '$serialNo' AND
					trn_orderheader.intOrderYear 	=  '$year'";
				 $result = $db->RunQuery($sql);
			 return $result;
}
//--------------------------------------------------------------
?>

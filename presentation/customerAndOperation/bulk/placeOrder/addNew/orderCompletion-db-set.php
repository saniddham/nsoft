<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	$locationId	= $companyId;
	//$path_rpt	= $_SESSION['MAIN_URL'];

	
	require_once "{$backwardseperator}class/cls_mail.php";
	include "{$backwardseperator}dataAccess/Connector.php";
	$objMail = new cls_create_mail($db);
	
	$response = array('type'=>'', 'msg'=>'');

	$requestType 	= $_REQUEST['requestType'];
	$arr 		= json_decode($_REQUEST['arr'], true);
	
	$programName='Place Order';
	$programCode='P0427';
	
	$ApproveLevels = (int)getApproveLevel($programName);
	$apLevel = $ApproveLevels+1;
//------------save---------------------------	
	if($requestType=='orderComplete')
	{
		$db->begin();
		$error = false;
		$savableFlag=1;
	    $rollBackFlag=0;

			foreach($arr as $arrVal)
			{
				$poNo 	   = $arrVal['poNo'];
				$poYear    = $arrVal['poYear'];
				///------
				$sql = "SELECT  
						intStatus ,
						strCustomerPoNo 
						FROM
						trn_orderheader   
						WHERE (`intOrderNo`='$poNo') 
						AND (`intOrderYear`='$poYear') ";
				$result = $db->RunQuery2($sql);
				$row=mysqli_fetch_array($result);
				$status= $row['intStatus'];	
				$customerPO= $row['strCustomerPoNo'];	
				///------
				$sql = "UPDATE `trn_orderheader` SET `intStatus`='-10',`intStatusOld`='$status' WHERE (`intOrderNo`='$poNo') 
					AND (`intOrderYear`='$poYear') ";
				$result3 = $db->RunQuery2($sql);
				if(!$result3){$error = true;}
				$toSave++;
				if($result3==1){
				$saved++;
				 $sql = "INSERT INTO `trn_orderheader_approvedby` (`intOrderNo`,`intYear`,`intApproveLevelNo`,`intApproveUser`,`dtApprovedDate`,intStatus) VALUES ('$poNo','$poYear','-10','$userId',now(),0)";
				 $result = $db->RunQuery2($sql);
				}
				
				
		// newly added by Krishantha 2017-9-18
		// checking pending MRNs for system close
		
		$sqlMrnPending = "SELECT ware_mrnheader.intStatus, ware_mrndetails.intMrnYear,ware_mrnheader.intMrnNo FROM `trn_orderheader` 
		JOIN 
		ware_mrndetails ON trn_orderheader.intOrderNo = ware_mrndetails.intOrderNo AND
		 trn_orderheader.intOrderYear = ware_mrndetails.intOrderYear 
		JOIN 
		ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND
		 ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
		WHERE ware_mrndetails.intOrderYear = '$poYear' AND ware_mrndetails.intOrderNo = '$poNo' AND ware_mrnheader.intStatus != 1 AND ware_mrnheader.intStatus >= 0 ";
		
		$resultData = $db->RunQuery2($sqlMrnPending);
		
		//updating MRN details
		while($row = mysqli_fetch_array($resultData)){
			
			$mrnYear = $row['intMrnYear'];
			$mrnNo = $row['intMrnNo'];
			
		// if pending MRN Available need to update as -2 (system closed)
		
			$sqlUpdate = "UPDATE ware_mrnheader 
				SET
				intStatus = '-2' 
				WHERE
				intMrnNo = '$mrnNo' AND 
				intMrnYear = '$mrnYear'";

				$resultsOfupdate = $db->RunQuery2($sqlUpdate);
			
		}

			// end of newly added part	
				
				
				
		}
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			sendCompletedEmailToUser($poNo,$poYear,$customerPO,$objMail,$mainPath,$root_path);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Completed successfully.';
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		
		$db->commit();
		echo json_encode($response);
	}
	
//--------------------------------------------------------
function sendCompletedEmailToUser($serialNo,$year,$customerPO,$objMail,$mainPat,$root_path){
	global $db;
	global $userId;
	//global $path_rpt;
		    $sql = "SELECT
			trn_orderheader.intCreator,
			trn_orderheader.intMarketer,
			trn_orderheader.strCustomerPoNo, 
			finance_customer_invoice_header.CREATED_BY 
			FROM 
			trn_orderheader
			LEFT JOIN finance_customer_invoice_header ON trn_orderheader.intOrderNo = finance_customer_invoice_header.ORDER_NO AND trn_orderheader.intOrderYear = finance_customer_invoice_header.ORDER_YEAR
			WHERE
			trn_orderheader.intOrderNo =  '$serialNo' AND
			trn_orderheader.intOrderYear =  '$year'";
			$result = $db->RunQuery2($sql);
 			$row=mysqli_fetch_array($result);
			$created_user	= $row['intCreator'];
			
			$creator_details	=	getSysUserDetails($row['intCreator']);
			$marketer_details	=	getSysUserDetails($row['intMarketer']);
			$invoicer_details	=	getSysUserDetails($row['CREATED_BY']);
			$customerPO = $row['strCustomerPoNo'];
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="COMPLETED CUSTOMER PO ('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='CUSTOMER PO';
			$_REQUEST['field1']='Order No';
			$_REQUEST['field2']='Order Year';
			$_REQUEST['field3']='Customer PO';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']=$customerPO;
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="COMPLETED ORDER ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Completed this";
			$_REQUEST['statement2']="to view this";
			//$_REQUEST['link']=base64_encode($mainPath."presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder.php?orderNo=$serialNo&orderYear=$year&approveMode=1"); 
			$_REQUEST['link']=base64_encode($_SESSION['MAIN_URL']."?q=896&orderNo=$serialNo&orderYear=$year"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			//$objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
			if($userId != $created_user){
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$creator_details['email'],$creator_details['fulll_name']);
			}
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$marketer_details['email'],$marketer_details['fulll_name']);
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$invoicer_details['email'],$invoicer_details['fulll_name']);
}

//--------------------------------------------------------
function getSysUserDetails($userId){
	global $db;
	

	  $sql = "SELECT
				sys_users.strFullName as fulll_name,
				sys_users.strEmail as email 
				FROM 
				sys_users
				WHERE sys_users.intUserId ='$userId'";
				
		$resultm = $db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm;

}
//--------------------------------------------------------
	
?>


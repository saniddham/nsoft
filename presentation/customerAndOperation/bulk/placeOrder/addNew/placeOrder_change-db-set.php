<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	
	$programName='Change Order Informations';
	$programCode='P0645';
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$orderNo 			= $_REQUEST['orderNo'];
	$orderYear			= trim($_REQUEST['orderYear']);
	$customerPO			= trim($_REQUEST['customerPO']);
	$customerLocId			= trim($_REQUEST['customerLocId']);
	
	$arr 		= json_decode($_REQUEST['arr'], true);
	/////////// location insert part /////////////////////
	
	$db->begin();
	if($requestType=='save')
	{
		$toSave=0;
		$saved=0;
		$rollBackFlag=0;
		
		$editMode=loadEditMode($programCode,$userId);
		$row_orderH	= getOrderHeader($orderNo,$orderYear);
		$duplicatePoCustPO=getDuplicatePoCustPO($customerPO,$row_orderH['intCustomer'],$orderNo,$orderYear);
		
		if($editMode==0){
			$rollBackMsg="No Permission to edit";
			$rollBackFlag=1;
		}
		else if($duplicatePoCustPO==1){
			$rollBackFlag=1;
			$rollBackMsg="This Customer PO No is already exists for Customer '".$row_orderH['customerName']."'";
		}
		
		$sql = "UPDATE `trn_orderheader` SET strCustomerPoNo ='$customerPO'  , intCustomerLocation = '$customerLocId'
				WHERE (`intOrderNo`='$orderNo') AND (`intOrderYear`='$orderYear') ";
		$result1 = $db->RunQuery2($sql);
				$toSave++;
				if($result1==1) 
				$saved++;
		
		foreach($arr as $arrVal)
		{
			$salesOrderId 	   = $arrVal['salesOrderId'];
			$salesOrderNo 	   = $arrVal['salesOrderNo'];
			
			$sql = "UPDATE `trn_orderdetails` SET `strSalesOrderNo`='$salesOrderNo'   WHERE (`intOrderNo`='$orderNo') 
			AND (`intOrderYear`='$orderYear') AND (`intSalesOrderId`='$salesOrderId')";
		    $result3 = $db->RunQuery2($sql);
		
			$toSave++;
			if($result3==1) 
			$saved++;
		}
		
		if($rollBackFlag==1){
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
		}
		else if($toSave==$saved){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	$db->commit();
	
	echo json_encode($response);



////////////////////////////////////////////	
function loadEditMode($programCode,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
 	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
				 $editMode=1;
		 }
			 
	return $editMode;
}

function getDuplicatePoCustPO($customerPO,$customer,$serialNo,$year){
	global $db;
	$serial=$serialNo.'/'.$year;
	//echo $savedStat;
	
	$customerPO = trim($customerPO);
	$flag=0;
	$sqlp = "SELECT
		trn_orderheader.intOrderNo
		FROM trn_orderheader
		WHERE
		trn_orderheader.intCustomer =  '$customer' AND
		trim(trn_orderheader.strCustomerPoNo) =  '$customerPO' AND
		concat(trn_orderheader.intOrderNo,'/',trn_orderheader.intOrderYear) <>  '$serial' AND 
		trn_orderheader.intStatus <>  '-2' AND 
		trn_orderheader.strCustomerPoNo <>  ''
		";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
	 if($rowp['intOrderNo']!=''){
		 $flag=1;
	 }
			 
	return $flag;
}

function getOrderHeader($serialNo,$year){
	global $db;

	$sql = "SELECT 
	trn_orderheader.intCustomer ,
	mst_customer.strName as customerName 
	FROM trn_orderheader 
	INNER JOIN mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
	WHERE
	trn_orderheader.intOrderNo =  '$serialNo' AND
	trn_orderheader.intOrderYear =  '$year'";

	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
 	
	return $row;
}

	
?>
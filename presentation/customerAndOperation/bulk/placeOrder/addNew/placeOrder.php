<?php (define('UNLOCKPAGE', true) ? die('<<< Access denied >>>') : ''); ?>
<?php

/****** NOTE **********************************************************************************
 * //2012
 * //order status status   final approved=1;  cancelled=-1;   completed=-10;   rejected=0.
 *
 * //2016
 * -- There is a facility to raise so wise split if it is a dummy order
 * -- pre costing =1, split =2, other =0
 * -- When you raise the first split, so wise initial dummy qty will be backup and
 * dummy qty will be automatically deducted (remain dummy qty = initial dummy qty - split qty)
 * -- So wise initial dummy qty < = so wise bal qty in dummy + tot(so wise split qty)
 * -- new splits can ammend to existing split orders by selecting the order no (at dummy order)
 ***********************************************************************************************/

$companyId = $sessions->getLocationId();
$company = $sessions->getCompanyId();
$intUser = $sessions->getUserId();

$programName = 'Place Order';
$programCode = 'P0427';

require_once "class/customerAndOperation/cls_textile_stores.php";
$objtexttile = new cls_texttile();
require_once "class/tables/sys_approvelevels.php";
$obj_sys_approvelevels = new sys_approvelevels($db);

$orderNo = (!isset($_REQUEST["orderNo"]) ? '' : $_REQUEST["orderNo"]);
$orderYear = (!isset($_REQUEST["orderYear"]) ? '' : $_REQUEST["orderYear"]);



$childFlag = getChildFlag($orderNo, $orderYear);
$actualTotQty = getActualTotalQty($orderNo, $orderYear);

if (($orderNo == '') && ($orderYear == '')) {
    $obj_sys_approvelevels->set($programCode);
    $savedStat = (int)$obj_sys_approvelevels->getintApprovalLevel();
    //$savedStat 				= (int)getApproveLevel($programName);
    $intStatus = $savedStat + 1;
    $strCustomerPoNo = '';
    $dtDate = '';
    $dtDeliveryDate = '';
    $strCustomer = '';
    $strCustomerLocations = '';
    $currencyCode = '';
    $strContactPerson = '';
    $strRemark = '';
    $marketer = '';
    $creator = '';
    $intApproveLevelStart = '';
    $intCustomer = '';
    $intCustomerLocation = '';
    $intCurrency = '';
    $intMarketer = '';
    $tentative = 0;
    $customerId = 0;

} else {
    $result = loadHeader($orderNo, $orderYear);
    while ($row = mysqli_fetch_array($result)) {
        $DUMMY_PO_NO = $row['DUMMY_PO_NO'];
        $DUMMY_PO_YEAR = $row['DUMMY_PO_YEAR'];
        $strCustomerPoNo = $row['strCustomerPoNo'];
        $dtDate = $row['dtDate'];
        $dtDeliveryDate = $row['dtDeliveryDate'];
        $strCustomer = $row['strCustomer'];
        $customerId = $row['customerId'];
        $strCustomerLocations = $row['strCustomerLocations'];
        $currencyCode = $row['currencyCode'];
        $paymentTerm = $row['intPaymentTerm'];
        $strContactPerson = $row['strContactPerson'];
        $strRemark = $row['strRemark'];
        $marketer = $row['marketer'];
        $creator = $row['creator'];
        $intApproveLevelStart = $row['intApproveLevelStart'] - 1;
        $intCustomer = $row['intCustomer'];
        $intCustomerLocation = $row['intCustomerLocation'];
        $poLocation = $row['intLocationId'];
        $poCompany = $row['intCompanyId'];
        $intCurrency = $row['intCurrency'];
        $intMarketer = $row['intMarketer'];
        $paymentMode = $row['LC_STATUS'];
        $poTechnique = $row['TECHNIQUE_TYPE'];

        $savedStat = $row['intApproveLevelStart'];
        $intStatus = $row['intStatus'];
        $blocked = $row['intBlocked'];
        $intReviseNo = $row['intReviseNo'];
        $poType = $row['PO_TYPE'];
        $instant = $row['INSTANT_ORDER'];
        $tentative = $row['TENTATIVE'];

        $recvdQty = $row['recvdQty'];
        if ($recvdQty == '') {
            $recvdQty = 0;
        }
        $dummy_link = '<a href="?q=427&orderNo=' . $DUMMY_PO_NO . '&orderYear=' . $DUMMY_PO_YEAR . '" target="placeOrder.php">' . $DUMMY_PO_YEAR . '/' . $DUMMY_PO_NO . '</a>';
    }
}
$tentativeMode = false;
if ($tentative ==2){
    $tentativeMode = true;
}
//get splitted orders---------------------
$order_string_link = '';

$sql = "SELECT
			trn_orderheader.intOrderNo,
			trn_orderheader.intOrderYear,
			trn_orderheader.strCustomerPoNo  
			FROM `trn_orderheader`
			WHERE
			trn_orderheader.PO_TYPE = 2 AND
			trn_orderheader.DUMMY_PO_NO = '$orderNo' AND
			trn_orderheader.DUMMY_PO_YEAR = '$orderYear' 
			and trn_orderheader.intStatus <> -2 
			";
$result = $db->RunQuery($sql);
$c = 0;
while ($row = mysqli_fetch_array($result)) {
    $c++;
    $childNo = $row['intOrderNo'];
    $childYear = $row['intOrderYear'];
    $childCPO = $row['strCustomerPoNo'];
    $order_string_link .= '<a href="?q=427&orderNo=' . $childNo . '&orderYear=' . $childYear . '" target="placeOrder.php">' . $childYear . '/' . $childNo . '(' . $childCPO . ')</a>' . ', ';
}
$order_string_link = rtrim($order_string_link, ", ");
if ($c == 1)
    $order_string_link = 'Split order is ' . $order_string_link;
else if ($c > 1)
    $order_string_link = 'Split orders are ' . $order_string_link;
//----------------------------------------


$orderDisableFlag = getOrderWiseFieldsDisableFlag($orderNo, $orderYear);
if ($orderDisableFlag > 0) {//if po is revised
    $disable = 'disabled="disabled"';
} else {
    $disable = '';
}

$editMode = loadEditMode($programCode, $intStatus, $savedStat, $intUser);
$confirmatonMode = loadConfirmatonMode($programCode, $intStatus, $savedStat, $intUser);
//echo $editMode.'=1 and '.$intStatus.'!=0 and '.$poType.'==1';
if ($editMode == 1 and $intStatus != 0 and $poType == 1)//revised and a dummy
    $dummy_splitable = 1;
else
    $dummy_splitable = 0;

//$allow_pre_costing = get_pre_costing_allowed_or_not($company);
?>
<script type="application/javascript">
    var intStatus = '<?php echo $intStatus; ?>';

</script>
<title>CUSTOMER PO</title>

<?php
$sql = "SELECT
				mst_locations.intBlocked
				FROM mst_locations
				WHERE
				mst_locations.intId =  '$companyId' ";

$result = $db->RunQuery($sql);
$row = mysqli_fetch_array($result);
if ($row['intBlocked'] == '1' && $orderNo == '') {
    $str = "This Location has been Blocked.Can't raise POs.";
    $maskClass = "maskShow";
    ?>
    <!--<div id="divMask" class="<?php echo $maskClass ?> mask"> <?php echo $str; ?></div>-->
    <?php return false;
}
?>

<style type="text/css">

    .fixHeader thead tr {
        display: block;
    }

    .fixHeader tbody {
        display: block;
        overflow: auto;
    }
</style>
<script>
    function  getId(name, element) {
        alert("alla");
        return name + element.closest('tr').rowIndex;
    }
</script>
<form id="frmPlaceOrders" name="frmPlaceOrders" method="post" action="">
    <div align="center">
        <div class="trans_layoutL">
            <div class="trans_text">Customer Purchase Order</div>

            <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
                <td>
                    <table width="100%" border="0">
                        <tr>
                            <td>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0"
                                       class="tableBorder_allRound">
                                    <tr height="22">
                                        <td width="10%" class="normalfnt">Order No</td>
                                        <td colspan="3">
                                            <table width="97%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="40%"><input name="txtOrderNo" type="text"
                                                                           disabled="disabled" id="txtOrderNo"
                                                                           style="width:60px"
                                                                           value="<?php echo $orderNo ?>"/><input
                                                                name="txtYear" type="text" disabled="disabled"
                                                                id="txtYear" style="width:40px"
                                                                value="<?php echo $orderYear ?>"/></td>
                                                    <td width="5%" class="normalfnt">&nbsp;</td>
                                                    <td width="19%" class="normalfnt">&nbsp;</td>
                                                    <td width="36%">&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="11%" class="normalfnt">&nbsp;Date</td>
                                        <td width="16%" class="normalfnt"><input name="dtDate" type="text" value="<?php
                                            if ($dtDate != '')
                                                echo $dtDate;
                                            else
                                                echo date("Y-m-d"); ?>" class="validate[required]" id="dtDate" style="width:120px;"
                                                                                 onKeyPress="return ControlableKeyAccess(event);"
                                                                                 onclick="return showCalendar(this.id, '%Y-%m-%d');"
                                                                                 disabled="disabled"/><input
                                                    type="reset" value="" class="txtbox" style="visibility:hidden;"
                                                    onclick="return showCalendar(this.id, '%Y-%m-%');"/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0"
                                       class="tableBorder_allRound">
                                    <tr height="22">
                                        <td class="normalfnt">Company <span class="compulsoryRed">*</span></td>
                                        <td colspan="3">
                                            <table width="97%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="40%"><select name="cboCompany" id="cboCompany"
                                                                            style="width:220px"
                                                                            class="txtText validate[required]" <?php if ($recvdQty > 0) { ?> disabled="disabled"<?php } ?> >
                                                            <option value=""></option>
                                                            <?php
                                                            $sql = "SELECT
                                                                    mst_companies.intId,
                                                                    mst_companies.strName
                                                                    FROM mst_companies
                                                                    WHERE
                                                                    mst_companies.intStatus =  '1'";
                                                            $result = $db->RunQuery($sql);
                                                            while ($row = mysqli_fetch_array($result)) {
                                                                if ($row['intId'] == $poCompany)
                                                                    echo "<option value=\"" . $row['intId'] . "\" selected=\"selected\">" . $row['strName'] . "</option>";
                                                                else
                                                                    echo "<option value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                                            }
                                                            ?>
                                                        </select></td>
                                                    <td width="6%" class="normalfnt">&nbsp;</td>
                                                    <td width="19%" class="normalfnt">Location <span
                                                                class="compulsoryRed">*</span></td>
                                                    <td width="35%" align="left"><select name="cboCompLocation"
                                                                                         id="cboCompLocation"
                                                                                         style="width:190px"
                                                                                         class="txtText validate[required]" <?php if ($recvdQty > 0) { ?> disabled="disabled"<?php } ?> >
                                                            <option value=""></option>
                                                            <?php
                                                            $sql = "SELECT
                                                                    mst_locations.intId,
                                                                    mst_locations.strName
                                                                    FROM mst_locations
                                                                    WHERE
                                                                    mst_locations.intCompanyId =  '$poCompany' AND  
                                                                    mst_locations.intStatus =  '1'";

                                                            $result = $db->RunQuery($sql);
                                                            while ($row = mysqli_fetch_array($result)) {
                                                                if ($row['intId'] == $poLocation)
                                                                    echo "<option value=\"" . $row['intId'] . "\" selected=\"selected\">" . $row['strName'] . "</option>";
                                                                else
                                                                    echo "<option value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                                            }
                                                            ?>
                                                        </select></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="normalfnt">Delivery Date <span
                                                    class="compulsoryRed">*</span></td>
                                        <td class="normalfnt"><input name="txtDelDate2" type="text" value="<?php
                                            if ($dtDeliveryDate != '')
                                                echo $dtDeliveryDate;
                                            else
                                                echo ""; ?>" class="dateType" id="txtDelDate"
                                                                     style="width:120px;"
                                                                     onKeyPress="return ControlableKeyAccess(event);"
                                                                     onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input
                                                    type="reset" value="" class="txtbox" style="visibility:hidden;"
                                                    onclick="return showCalendar(this.id, '%Y-%m-%');"/></td>
                                    </tr>
                                    <tr height="22">
                                        <td width="10%" class="normalfnt">Customer PO #</td>
                                        <td colspan="3">
                                            <table width="97%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="46%"><input
                                                                value="<?php echo htmlentities($strCustomerPoNo); ?>"
                                                                name="txtCustomerPO" <?php if ($tentativeMode){ ?>disabled<?php } ?> type="text" id="txtCustomerPO"
                                                                style="width:220px"
                                                                class="validate[maxSize[50]] normalfnt"/></td>
                                                    <td width="19%" class="normalfnt">Currency <span
                                                                class="compulsoryRed">*</span></td>
                                                    <td width="35%"><select class="validate[required]"
                                                                            name="cboCurrency" id="cboCurrency"
                                                                            style="width:190px" <?php echo $disable ?>>
                                                            <!--<option value=""></option>-->
                                                            <?php
                                                            $sql = "SELECT
                                                                    mst_financecurrency.intId,
                                                                    mst_financecurrency.strCode
                                                                    FROM mst_financecurrency where intId=1";
                                                            $result = $db->RunQuery($sql);
                                                            while ($row = mysqli_fetch_array($result)) {
                                                                if ($row['intId'] == $intCurrency)
                                                                    echo "<option selected=\"selected\" value=\"" . $row['intId'] . "\">" . $row['strCode'] . "</option>";
                                                                else
                                                                    echo "<option value=\"" . $row['intId'] . "\">" . $row['strCode'] . "</option>";
                                                            }
                                                            ?>
                                                        </select></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="11%"><span class="normalfnt">Contact Person</span></td>
                                        <td width="16%"><input value="<?php echo $strContactPerson; ?>"
                                                               name="txtContactPerson" type="text" id="txtContactPerson"
                                                               style="width:120px"/></td>
                                    </tr>
                                    <tr height="22">
                                        <td class="normalfnt">Customer <span class="compulsoryRed">*</span></td>
                                        <td colspan="3">
                                            <table width="97%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="41%"><select class="validate[required]"
                                                                            name="cboCustomer" id="cboCustomer"
                                                                            style="width:220px" <?php if ($recvdQty > 0) { ?> disabled="disabled"<?php } ?>>
                                                            <option value=""></option>
                                                            <?php
                                                            $sql = "SELECT
                                                                    mst_customer.intId,
                                                                    mst_customer.strCode,
                                                                    mst_customer.strName
                                                                    FROM mst_customer
                                                                    WHERE
                                                                    mst_customer.intStatus =  '1' order by strName
                                                                    ";
                                                            $result = $db->RunQuery($sql);
                                                            while ($row = mysqli_fetch_array($result)) {
                                                                if ($row['intId'] == $intCustomer)
                                                                    echo "<option selected=\"selected\" value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                                                else
                                                                    echo "<option value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                                            }
                                                            ?>
                                                        </select></td>
                                                    <td width="5%" class="normalfntMid"><img src="images/down1.png"
                                                                                             width="25" height="22"
                                                                                             alt="Load all Graphic nos"
                                                                                             id="imgLoadAllGrapicNos"
                                                                                             name="imgLoadAllGrapicNos"
                                                                                             class="allGrapicNos mouseover"
                                                                                             title="load all graphic nos"
                                                                                             style="display:none"/></td>
                                                    <td width="19%" class="normalfnt">Payment Term <span
                                                                class="compulsoryRed">*</span></td>
                                                    <td width="35%"><select name="cboPayTerm" style="width:190px"
                                                                            id="cboPayTerm" class="validate[required]">
                                                            <option value=""></option>
                                                            <?php
                                                            $sql = "SELECT
                                                                    mst_financepaymentsterms.intId,
                                                                    mst_financepaymentsterms.strName,
                                                                    mst_financepaymentsterms.strDescription
                                                                    FROM mst_financepaymentsterms";
                                                            $result = $db->RunQuery($sql);
                                                            while ($row = mysqli_fetch_array($result)) {
                                                                if ($row['intId'] == $paymentTerm)
                                                                    echo "<option value=\"" . $row['intId'] . "\" selected=\"selected\">" . $row['strName'] . " days" . "</option>";
                                                                else
                                                                    echo "<option value=\"" . $row['intId'] . "\">" . $row['strName'] . " days" . "</option>";
                                                            }
                                                            ?>
                                                        </select></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="normalfnt">Payment Mode <span class="compulsoryRed">*</span></td>
                                        <td><select id="cboPaymentMode" name="cboPaymentMode" style="width:120px"
                                                    class="validate[required]">
                                                <option value=""></option>
                                                <?php
                                                $sql = "SELECT
                                                        PM.intId,
                                                        PM.strName,
                                                        PM.strDescription
                                                        FROM mst_financepaymentsmethods PM
                                                        WHERE PM.intStatus = 1
                                                        ORDER BY PM.strName";
                                                $result = $db->RunQuery($sql);
                                                while ($row = mysqli_fetch_array($result)) {
                                                    if ($row['intId'] == $paymentMode)
                                                        echo "<option value=\"" . $row['intId'] . "\" selected=\"selected\">" . $row['strName'] . "</option>";
                                                    else
                                                        echo "<option value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                                }
                                                ?>
                                            </select></td>
                                    </tr>
                                    <tr height="22">
                                        <td class="normalfnt">Location <span class="compulsoryRed">*</span></td>
                                        <td colspan="3">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="45%"><select class="validate[required]"
                                                                            name="cboLocation" id="cboLocation"
                                                                            style="width:220px" <?php echo $disable ?>>
                                                            <?php
                                                            if ($orderNo != '') {
                                                                $sql = "SELECT
                                                                        mst_customer_locations.intLocationId,
                                                                        mst_customer_locations_header.strName
                                                                    FROM
                                                                        mst_customer_locations
                                                                        Inner Join mst_customer_locations_header ON mst_customer_locations.intLocationId = mst_customer_locations_header.intId
                                                                    WHERE
                                                                        mst_customer_locations.intCustomerId =  '$intCustomer' order by strName";
                                                                $result = $db->RunQuery($sql);
                                                                echo "<option value=\"\"></option>";
                                                                while ($row = mysqli_fetch_array($result)) {
                                                                    if ($row['intLocationId'] == $intCustomerLocation)
                                                                        echo "<option  selected=\"selected\" value=\"" . $row['intLocationId'] . "\">" . $row['strName'] . "</option>";
                                                                    else
                                                                        echo "<option value=\"" . $row['intLocationId'] . "\">" . $row['strName'] . "</option>";
                                                                }
                                                            }
                                                            ?>
                                                        </select></td>
                                                    <td width="18%" class="normalfnt">Marketer <span
                                                                class="compulsoryRed">*</span></td>
                                                    <td width="37%"><select class="validate[required]"
                                                                            name="cboMarketer" id="cboMarketer"
                                                                            style="width:190px">
                                                            <option value=""></option>
                                                            <?php
                                                            $sql = "SELECT
                                                                        mst_marketer.intUserId,
                                                                        sys_users.strFullName
                                                                    FROM
                                                                        mst_marketer
                                                                    Inner Join sys_users ON mst_marketer.intUserId = sys_users.intUserID
                                                                    WHERE 
                                                                    mst_marketer.STATUS = 1
                                                                    ";
                                                            $result = $db->RunQuery($sql);
                                                            while ($row = mysqli_fetch_array($result)) {
                                                                if ($row['intUserId'] == $intMarketer)
                                                                    echo "<option selected=\"selected\" value=\"" . $row['intUserId'] . "\">" . $row['strFullName'] . "</option>";
                                                                else
                                                                    echo "<option value=\"" . $row['intUserId'] . "\">" . $row['strFullName'] . "</option>";
                                                            }
                                                            ?>
                                                        </select></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="normalfnt">Po Type</td>
                                        <td><select id="cboPoType"
                                                    name="cboPoType" <?php if ($c > 0 or $DUMMY_PO_NO > 0){ ?>
                                                    disabled="disabled" <?php } ?>style="width:120px"
                                                    class="validate[required]">
                                                <option value="" <?php if (!$poType) {
                                                    echo 'selected="selected"';
                                                } ?>></option>
                                                <option value="0" <?php if ($poType == '0') {
                                                    echo 'selected="selected"';
                                                } ?>>Other (Actual orders without pre costing)
                                                </option>
                                                <option value="1" <?php if ($poType == 1) {
                                                    echo 'selected="selected"';
                                                } ?>>Pre Cosing Order (Dummy)
                                                </option>
                                                <?php if ($poType == 2) { ?>
                                                    <option value="2" <?php if ($poType == 2) {
                                                        echo 'selected="selected"';
                                                    } ?>>Split Order (Costing order split from Pre costing order)
                                                    </option>
                                                <?php } ?>
                                            </select></td>
                                    </tr>
                                    <tr height="22">
                                        <td class="normalfnt">Remark</td>
                                        <td colspan="2"><textarea name="txtRemarks" id="txtRemarks" cols="46"
                                                                  rows="2" maxlength="250"><?php echo $strRemark; ?></textarea></td>
                                        <td width="15%" class="normalfnt" valign="middle" colspan="2" nowrap>
                                            <div nowrap
                                                 <?php if ($poType != 1 || $poCompany != 1){ ?>style="display:none"<?php } ?>
                                                 id="divInstant">
                                                <input id="chkInstant" name="chkInstant"
                                                       type="checkbox" <?php if ($instant == 1 && $dtDate < '2018-10-15') { ?> checked <?php } else { ?> disabled="disabled" <?php } ?> >
                                                Production start without PO
                                            </div>
                                            <input id="chkTentative" name="chkTentative " type="checkbox" <?php if ($orderNo == '' || $poType ==1) { ?> disabled <?php } ?>  <?php if ($tentativeMode) { ?> checked <?php } ?>    onchange="generateDefaultNames()">
                                            Tentative
                                        </td>
                                        <td class="normalfnt" nowrap>
                                            <span class="normalfnt">
              <?php if ($orderNo != ''){
              echo 'Received Qty' ?><b><?php echo ' : ' . $recvdQty;
                                                    } ?>
            </b></span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" class="normalfnt"><b>
                                                <div id="childOrders"
                                                     style="background-color:#fadbd8 ; <?php if ($poType != 1) { ?>display:none <?php } ?>"><?php echo $order_string_link; ?></div>
                                            </b><b>
                                                <div id="dummyOrders"
                                                     style="background-color:#BAD7F3 ; <?php if ($poType != 2) { ?>display:none <?php } ?>">
                                                    Parent Dummy Order is <?php echo $dummy_link; ?></div>
                                            </b></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <div style="width:900px;height:300px;overflow:scroll">
                                    <table width="2500" height="194" class="bordered" id="tblPOs">
                                        <tr class="">
                                            <th width="24">Del</th>
                                            <th width="40"
                                                <?php if ($dummy_splitable == 0){ ?>style="display:none" <?php } ?>>
                                                Split
                                            </th>
                                            <th width="40"
                                                <?php if ($dummy_splitable == 0){ ?>style="display:none" <?php } ?>>Save&nbsp;To
                                                Existing&nbsp;split
                                            </th>
                                            <th width="40"
                                                <?php if ($dummy_splitable == 0){ ?>style="display:none" <?php } ?>>
                                                Customer PO
                                            </th>
                                            <th width="40">Line No</th>
                                            <th width="63">Material Code</th>
                                            <th width="70">Sales Order No</th>
                                            <th width="100">Graphic No</th>
                                            <th width="105">Sample No</th>
                                            <th width="100">Style</th>
                                            <th width="100">Placement</th>
                                            <th width="75">Print Combo</th>
                                            <th width="63">Revision No</th>
                                            <th width="45">Brand</th>
                                            <th width="100">Ground Color</th>
                                            <th width="27">Size</th>
                                            <th width="35">Copy</th>
                                            <th width="60">Panel Qty</th>
                                            <th width="60">Price</th>
                                            <th width="90">Value</th>
                                            <th width="60">Over Cut %</th>
                                            <th width="60">Damage %</th>
                                            <th width="186">Grading</th>
                                            <th width="90">PSD</th>
                                            <th width="186">Delivery Date</th>
                                            <th width="186">Technique Group</th>
                                            <th width="40">Split Pressing</th>

                                        </tr>
                                        <?php

                                        $detailsCount = getRecordCount($orderNo, $orderYear);
                                        if ($orderNo == '' || $detailsCount <= 0)
                                        {

                                            ?>
                                            <tr class="normalfnt" id="dataRow">
                                                <td height="49" align="center" bgcolor="#FFFFFF"><img
                                                            src="images/del.png" width="15" height="15" class="delImg"/>
                                                    <div id="rawType" style="display:none">1</div>
                                                    <div id="parentSO" style="display:none"></div>
                                                    <div id="SO" style="display:none"></div>
                                                </td>
                                                <td <?php if ($dummy_splitable == 0){ ?>style="display:none" <?php } ?>
                                                    align="center" bgcolor="#FFFFFF" class="clsSplit" id=""></td>
                                                <td <?php if ($dummy_splitable == 0){ ?>style="display:none" <?php } ?>
                                                    bgcolor="#FFFFFF" align="center">&nbsp;
                                                </td>
                                                <td <?php if ($dummy_splitable == 0){ ?>style="display:none" <?php } ?>
                                                    bgcolor="#FFFFFF" align="center"></td>
                                                <td bgcolor="#FFFFFF" align="center"><input class="validate[required]"
                                                                                            value="10" name="txtLineNo"
                                                                                            type="text" id="txtLineNo"
                                                                                            style="width:40px;text-align:center"/>
                                                </td>
                                                <td align="center" bgcolor="#FFFFFF"><input id="txtItemCode"
                                                                                            name="txtItemCode"
                                                                                            type="text"
                                                                                            style="width:100px"
                                                                                            class="ItemCode"/></td>
                                                <td bgcolor="#FFFFFF"><input class="validate[required]" type="text"
                                                                             name="txtSheduleNo" id="txtSheduleNo"
                                                                             style="width:70px"/></td>
                                                <td bgcolor="#FFFFFF"><select name="cboStyle" id="cboStyle"
                                                                              style="width:100px"
                                                                              class="selcStyle validate[required]">
                                                        <option value=""></option>
                                                        <?php
                                                        $d = date('Y');
                                                        $sql = "SELECT DISTINCT
                                                        trn_sampleinfomations.strGraphicRefNo
                                                        FROM trn_sampleinfomations
                                                        WHERE  trn_sampleinfomations.intSampleYear = $d  
                                                        order by strGraphicRefNo";
                                                        $result = $db->RunQuery($sql);
                                                        while ($row = mysqli_fetch_array($result)) {
                                                            echo "<option value=\"" . $row['strGraphicRefNo'] . "\">" . $row['strGraphicRefNo'] . "</option>";
                                                        }
                                                        ?>
                                                    </select></td>
                                                <td bgcolor="#FFFFFF" nowrap="nowrap"><select name="cboYear"
                                                                                              id="cboYear"
                                                                                              style="width:55px"
                                                                                              class="cboYear">
                                                        <?php $d = date('Y');
                                                        for ($d; $d >= 2012; $d--) {
                                                            echo "<option id=\"$d\" >$d</option>";
                                                        } ?>
                                                    </select><select name="cboSampNo" id="cboSampNo1"
                                                                     class="cboSampNo1 validate[required]"
                                                                     style="width:105px">
                                                        <option></option>
                                                    </select></td>
                                                <td align="center" bgcolor="#FFFFFF"><input name="txtStyleNo"
                                                                                            type="text" id="txtStyleNo"
                                                                                            style="width:100px"/></td>
                                                <td bgcolor="#FFFFFF"><select name="cboPart" id="cboPart"
                                                                              style="width:100px"
                                                                              class="cboPart validate[required]">
                                                    </select></td>
                                                <td bgcolor="#FFFFFF"><select name="cboCombo" id="cboCombo"
                                                                              style="width:75px"
                                                                              class="cboCombo validate[required]">
                                                    </select></td>
                                                <td align="center" bgcolor="#FFFFFF"><select name="cboRevision"
                                                                                             id="cboRevision"
                                                                                             style="width:45px"
                                                                                             class="cboRevision validate[required]">
                                                    </select></td>

                                                <td align="center" bgcolor="#FFFFFF"><input id="txtBrand"
                                                                                            name="txtBrand" type="text"
                                                                                            style="width:100px"
                                                                                            class="brand"
                                                                                            disabled="disabled"/></td>
                                                <td align="center" bgcolor="#FFFFFF"><input name="txtGroundColor"
                                                                                            type="text"
                                                                                            id="txtGroundColor"
                                                                                            style="width:100px"
                                                                                            class="groundColor"
                                                                                            disabled="disabled"/></td>
                                                <td align="center" bgcolor="#FFFFFF" class="cls_size"><img
                                                            src="images/add_new.png" width="15" height="15"
                                                            class="clsAddSizes mouseover"/></td>
                                                <td align="center" bgcolor="#FFFFFF" class="clsCopy">&nbsp;</td>

                                                <td bgcolor="#FFFFFF"><input name="txtQty" type="text" id="txtQty"
                                                                             style="width:60px; text-align:right"
                                                                             value="0"
                                                                             class="validate[min[0],custom[integer]] calculateValue clsQty"/>
                                                    <div id="dummyQty"
                                                         style="display:none"><?php echo $row2['DUMMY_SO_INITIAL_QTY']; ?></div>
                                                </td>
                                                <td width="100" nowrap="nowrap"><select name="txtPrice" id="txtPrice"  style="width:50px"  class="calculateValue validate[required, min[0]] normalfntRight clsPrice">

                                                    </select></td>
<!--                                                <td bgcolor="#FFFFFF"><input name="txtPrice" type="text" id="txtPrice"-->
<!--                                                                             disabled="disabled" align="right"-->
<!--                                                                             style="width:60px;color:#06F; text-align:right"-->
<!--                                                                             value=""-->
<!--                                                                             class="calculateValue validate[required, min[0]] normalfntRight clsPrice"/>-->
<!--                                                </td>-->
                                                <td bgcolor="#FFFFFF"><input name="txtValue" type="text" id="txtValue"
                                                                             disabled="disabled" align="right"
                                                                             style="width:90px;color:#06F; text-align:right"
                                                                             value=""
                                                                             class="calculateValue validate[min[0]] normalfntRight"/>
                                                </td>
                                                <td bgcolor="#FFFFFF"><input name="txtOverCutPer" type="text"
                                                                             id="txtOverCutPer"
                                                                             style="width:60px; text-align:right"/></td>
                                                <td bgcolor="#FFFFFF"><input name="txtDamagePer" type="text"
                                                                             id="txtDamagePer"
                                                                             style="width:60px; text-align:right"/></td>
                                                <td bgcolor="#FFFFFF" width="100">
                                                    <input type="checkbox" name="grading" class="chek" id="grading"
                                                           value="1"> yes<br>
                                                    <input type="checkbox" name="grading" class="chek" id="grading"
                                                           value="0" checked> no
                                                </td>
                                                <td bgcolor="#FFFFFF" nowrap="nowrap" width="150"><input name="txtPSD"
                                                                                                         type="text"
                                                                                                         class="psDate dateType"
                                                                                                         id="txtPSD1"
                                                                                                         style="width:80px"
                                                                                                         value=""
                                                                                                         onKeyPress="return ControlableKeyAccess(event);"
                                                                                                         onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input
                                                            type="reset" style="visibility:hidden;width:1px"
                                                            onclick="return showCalendar(this.id, '%Y-%m-%');"/>
                                                    <input name="oldtxtPSD" id="oldtxtPSD" style="display: none;"  value=""/>
                                                </td>
                                                <td bgcolor="#FFFFFF" width="100" nowrap="nowrap"><input
                                                            name="txtDelDate" type="text" class="delDate dateType"
                                                            id="txtDelDate1" style="width:80px"
                                                            value=""
                                                            onKeyPress="return ControlableKeyAccess(event);"
                                                            onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input
                                                            type="reset" value="" style="visibility:hidden;width:1px"
                                                            onclick="return showCalendar(this.id, '%Y-%m-%');"/><input name="oldtxtDelDate" id="oldtxtDelDate" style="display: none;"  value="">
                                                </td>
                                                <td bgcolor="#FFFFFF" width="100" nowrap="nowrap"><select disabled="disabled"
                                                                                          name="cboTechGrp"
                                                                                          id="cboTechGrp"
                                                                                          style="width:200px"
                                                                                          class="cboTechGrp validate[required]">
                                                        <?php
                                                        $sql1 = "SELECT
                                                                mst_technique_groups.TECHNIQUE_GROUP_ID,
                                                                mst_technique_groups.TECHNIQUE_GROUP_NAME
                                                                FROM `mst_technique_groups`
                                                                WHERE
                                                                mst_technique_groups.`STATUS` = 1
                                                                ORDER BY
                                                                mst_technique_groups.TECHNIQUE_GROUP_NAME ASC
                                                                ";
                                                        echo "<option value=\"\" selected=\"selected\"></option>";
                                                        $result1 = $db->RunQuery($sql1);
                                                        while ($row1 = mysqli_fetch_array($result1)) {
                                                            echo "<option value=\"" . $row1['TECHNIQUE_GROUP_ID'] . "\">" . $row1['TECHNIQUE_GROUP_NAME'] . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </td>
                                                <td bgcolor="#FFFFFF" width="40"><input type="checkbox" disabled name="splitPressing" id="splitPressing"
                                                                                                                                      value="0" unchecked>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        else ///////////// load details from database /////////////
                                        {
                                        $sql2 = "	SELECT
                                            trn_orderdetails.strSalesOrderNo,intSalesOrderId,
                                            trn_orderdetails.DUMMY_SO_ID,
                                            trn_orderdetails.strGraphicNo,
                                            trn_orderdetails.intSampleNo,
                                            trn_orderdetails.intSampleYear,
                                            trn_orderdetails.strCombo,
                                            trn_orderdetails.strLineNo,
                                            trn_orderdetails.strStyleNo,
                                            trn_orderdetails.strPrintName,
                                            trn_orderdetails.intRevisionNo,
                                            trn_orderdetails.intPart,
                                            trn_orderdetails.intQty,
                                            trn_orderdetails.dblPrice,
                                            trn_orderdetails.dblOverCutPercentage,
                                            trn_orderdetails.dblDamagePercentage,
                                            trn_orderdetails.dtPSD,
                                            trn_orderdetails.dtDeliveryDate,
                                            trn_orderdetails.TECHNIQUE_GROUP_ID ,
                                            trn_orderdetails.ITEM_CODE_FROM_CUSTOMER,
                                            trn_orderdetails.IS_GRADING,
                                            trn_orderdetails.SO_TYPE,
                                            trn_orderdetails.splitedPrice,
                                            IFNULL(DUMMY_SO_INITIAL_QTY,0) AS DUMMY_SO_INITIAL_QTY 
                                        FROM trn_orderdetails
                                        WHERE
                                            trn_orderdetails.intOrderNo 	=  '$orderNo' AND
                                            trn_orderdetails.intOrderYear 	=  '$orderYear'
                                        ORDER BY
                                            trn_orderdetails.SO_TYPE, trn_orderdetails.intSalesOrderId ASC
                                        ";
                                        $result2 = $db->RunQuery($sql2);
                                        $r = 0;
                                        $count = 0;
                                        while ($row2 = mysqli_fetch_array($result2))
                                        {
                                            $row2['dblPrice'] = $row2['dblPrice'] + $row2['splitedPrice'];
                                            if ($row2['SO_TYPE'] == -1){
                            ?>
                                        <tr hidden class="normalfnt cls_tr" id="<?php echo $row2['intSalesOrderId']; ?>"
                                            bgcolor="#FFFFFF">
                                            <td>
                                                <input type="checkbox" hidden  name="splitPressing" id="splitPressing"
                                                       value="-1" unchecked>
                                            </td>

                                        </tr>
                                            <?php
                                            } else {
                                                    $dummyQty += $row2['DUMMY_SO_INITIAL_QTY'];
                                                    $rcvQty = getRcvQty($orderNo, $orderYear, $row2['intSalesOrderId']);
                                                    $fDQty = loadDamageReturnedQty($companyId, $orderNo, $orderYear, $row2['intSalesOrderId']);
                                                    //15/03/2013-------------------------
                                                    //$excessQty=loadExcessQty($orderNo,$orderYear,$row2['intSalesOrderId']);
                                                    $toleratePercentage = $objtexttile->getNewTolerencePercentage($company, $row2['intQty']);
                                                    $excessQty = ceil(loadSalesOrderWiseExcessQty($orderNo, $orderYear, $row2['intSalesOrderId'], $toleratePercentage));
                                                    ///////////////////////////
                                                    $groundColor = getGroundColor($row2['intSampleNo'], $row2['intSampleYear'], $row2['strPrintName'], $row2['strCombo'], $row2['intRevisionNo']);
                                                    $r++;
                                                    $editFlag = getSalesWiseFieldsDisableFlag($orderNo, $orderYear, $row2['intSalesOrderId']);
                                                    $disable = '';
                                                    $disableFlag = 0;
                                                    if ($editFlag > 0) {//if po is revised
                                                        $disable = 'disabled="disabled"';
                                                        $disableFlag = 1;
                                                    } else if($row2['SO_TYPE'] == 2){
                                                        $pressingSoId = getPressingSalesOrder($orderNo, $orderYear, $row2['intSalesOrderId']);
                                                        $editFlag = getSalesWiseFieldsDisableFlag($orderNo, $orderYear, $pressingSoId);
                                                        if ($editFlag > 0) {
                                                            $disable = 'disabled="disabled"';
                                                            $disableFlag = 1;
                                                        }
                                                    }
                                                    $flag_child_exist = getFlagChild($orderNo, $orderYear, $row2['intSalesOrderId']);

                                                    $maxRevNo = get_max_revision_no($row2['intSampleNo'], $row2['intSampleYear']);
                                                    ?>
                                                    <tr class="normalfnt cls_tr" id="<?php echo $row2['intSalesOrderId']; ?>"
                                                        bgcolor="#FFFFFF">
                                                        <td height="42" align="center"><img src="images/del.png" width="15"
                                                                                            height="15" class="delImg"/>
                                                            <div id="rawType" style="display:none">1</div>
                                                            <div id="parentSO"
                                                                 style="display:none"><?php echo $row2['DUMMY_SO_ID']; ?></div>
                                                            <div id="SO"
                                                                 style="display:none"><?php echo $row2['intSalesOrderId']; ?></div>
                                                        </td>
                                                        <td <?php if ($dummy_splitable == 0){ ?>style="display:none" <?php } ?>
                                                            align="center" class="clsSplit"
                                                            id=""><?php if ($row2['DUMMY_SO_ID'] == '') {
                                                                echo '<a class="button green small" id="butSplitRow" name="butSplitRow">Splitt</a>';
                                                            } ?></td>
                                                        <td <?php if ($dummy_splitable == 0){ ?>style="display:none" <?php } ?>
                                                            align="center" nowrap="nowrap" class="splitOrder"></td>
                                                        <td <?php if ($dummy_splitable == 0){ ?>style="display:none" <?php } ?>
                                                            align="center"><input class="validate[required]"
                                                                                  value="<?php echo htmlentities($strCustomerPoNo); ?>"
                                                                                  name="txtCustoPOsplited" type="text"
                                                                                  id="txtCustoPOsplited" disabled="disabled"
                                                                                  style="width:60px;text-align:center"/></td>
                                                        <td align="center"><input class="validate[required]"
                                                                                  value="<?php echo $row2['strLineNo']; ?>"
                                                                                  name="txtLineNo" type="text" id="txtLineNo"
                                                                                  style="width:40px;text-align:center" <?php echo $disable ?>/>
                                                        </td>
                                                        <td align="center" bgcolor="#FFFFFF"><input id="txtItemCode"
                                                                                                    name="txtItemCode" type="text"
                                                                                                    style="width:100px"
                                                                                                    class="ItemCode"
                                                                                                    value="<?php echo $row2['ITEM_CODE_FROM_CUSTOMER']; ?>"/>
                                                        </td>
                                                        <td><input value="<?php echo $row2['strSalesOrderNo']; ?>"
                                                                   class="validate[required]" type="text" name="txtSheduleNo"
                                                                   id="txtSheduleNo" style="width:70px" <?php echo $disable ?>/>
                                                        </td>
                                                        <td><select name="cboStyle" id="cboStyle" style="width:100px"
                                                                    class="selcStyle validate[required]" <?php echo $disable ?>>
                                                                <option value=""></option>
                                                                <?php

                                                                $sql1 = " SELECT DISTINCT
                                                                          trn_sampleinfomations.strGraphicRefNo
                                                                        FROM trn_sampleinfomations
                                                                        WHERE  trn_sampleinfomations.intSampleYear = '" . $row2['intSampleYear'] . "' 
                                                                        ORDER BY strGraphicRefNo";
                                                                $result1 = $db->RunQuery($sql1);
                                                                $foundGraphicNo = false;
                                                                while ($row1 = mysqli_fetch_array($result1)) {
                                                                    if (trim($row2['strGraphicNo']) === trim($row1['strGraphicRefNo'])) {
                                                                        echo "<option selected=\"selected\" value=\"" . (string)$row1['strGraphicRefNo'] . "\">" . (string)$row1['strGraphicRefNo'] . "</option>";
                                                                        $foundGraphicNo = true;
                                                                    } else
                                                                        echo "<option value=\"" . $row1['strGraphicRefNo'] . "\">" . $row1['strGraphicRefNo'] . "</option>";
                                                                }
                                                                if (!$foundGraphicNo) {
                                                                    //echo "<option selected=\"selected\" value=\"".$row2['strGraphicNo']."\">".$row2['strGraphicNo']."</option>";
                                                                }
                                                                ?>
                                                            </select></td>
                                                        <td nowrap="nowrap"><select name="cboYear" id="cboYear" style="width:55px"
                                                                                    class="cboYear" <?php echo $disable ?>>
                                                                <?php
                                                                $d = date('Y');
                                                                for ($d; $d >= 2012; $d--) {
                                                                    if ($row2['intSampleYear'] == $d)
                                                                        echo "<option selected=\"selected\" id=\"$d\" >$d</option>";
                                                                    else
                                                                        echo "<option id=\"$d\" >$d</option>";
                                                                }
                                                                ?>
                                                            </select><select name="cboSampNo" id="cboSampNo1"
                                                                             class="cboSampNo1 validate[required]"
                                                                             style="width:105px" <?php echo $disable ?>>
                                                                <option value=""></option>
                                                                <?php
                                                                #############################
                                                                ######## SAMPLE NO ##########
                                                                #############################
                                                                $sql1 = "SELECT DISTINCT
                                                                            trn_sampleinfomations.intSampleNo
                                                                        FROM trn_sampleinfomations
                                                                        WHERE
                                                                            trn_sampleinfomations.intSampleYear =  '" . $row2['intSampleYear'] . "' AND
                                                                            trn_sampleinfomations.strGraphicRefNo =  '" . $row2['strGraphicNo'] . "'
                                                                        ORDER BY
                                                                            trn_sampleinfomations.intSampleNo DESC
                                                                        ";
                                                                $result1 = $db->RunQuery($sql1);
                                                                while ($row1 = mysqli_fetch_array($result1)) {
                                                                    if ($row2['intSampleNo'] == $row1['intSampleNo'])
                                                                        echo "<option selected=\"selected\" value=\"" . $row1['intSampleNo'] . "\">" . $row1['intSampleNo'] . "</option>";
                                                                    else
                                                                        echo "<option value=\"" . $row1['intSampleNo'] . "\">" . $row1['intSampleNo'] . "</option>";
                                                                }
                                                                ?>
                                                            </select><?php //echo $row2['intSampleNo'];
                                                            ?></td>
                                                        <td align="center"><input value="<?php echo $row2['strStyleNo']; ?>"
                                                                                  name="txtStyleNo" type="text" id="txtStyleNo"
                                                                                  style="width:100px" <?php echo $disable ?> /></td>
                                                        <td><select name="cboPart" id="cboPart" style="width:100px"
                                                                    class="cboPart validate[required]" <?php echo $disable ?>>
                                                                <option value=""></option>
                                                                <?php
                                                                #############################
                                                                ######## PART ###############
                                                                #############################
                                                                $sql1 = "SELECT DISTINCT
                                                                            mst_part.strName ,mst_part.intId as partId,
                                                                            trn_sampleinfomations_printsize.strPrintName
                                                                        FROM
                                                                        trn_sampleinfomations_printsize
                                                                            Inner Join mst_part ON mst_part.intId = trn_sampleinfomations_printsize.intPart
                                                                        WHERE
                                                                            trn_sampleinfomations_printsize.intSampleNo 	=   '" . $row2['intSampleNo'] . "' AND
                                                                            trn_sampleinfomations_printsize.intSampleYear 	=   '" . $row2['intSampleYear'] . "' ";
                                                                if ($disableFlag == 0) {
                                                                    $sql1 .= "AND trn_sampleinfomations_printsize.intRevisionNo='$maxRevNo'";
                                                                }
                                                                $sql1 .= "ORDER BY
                                                                            mst_part.strName ASC
                                                                        ";
                                                                $result1 = $db->RunQuery($sql1);

                                                                if ($disableFlag == 0) {
                                                                    //echo  "<option value=\"\"></option>";
                                                                }
                                                                while ($row1 = mysqli_fetch_array($result1)) {
                                                                    if ($row2['strPrintName'] == $row1['strPrintName'] && $row1['partId'] == $row2['intPart'])
                                                                        echo "<option selected=\"selected\" value=\"" . $row1['partId'] . '/' . $row1['strPrintName'] . "\">" . $row1['strName'] . ' - ' . $row1['strPrintName'] . "</option>";
                                                                    else
                                                                        echo "<option value=\"" . $row1['partId'] . '/' . $row1['strPrintName'] . "\">" . $row1['strName'] . ' - ' . $row1['strPrintName'] . "</option>";
                                                                }

                                                                ?>
                                                            </select></td>
                                                        <td><select name="cboCombo" id="cboCombo" style="width:75px"
                                                                    class="cboCombo validate[required]" <?php echo $disable ?>>
                                                                <?php

                                                                #############################
                                                                ######## COMBO ##############
                                                                #############################
                                                                $sql1 = "SELECT DISTINCT
                                                                            trn_sampleinfomations_details.strComboName
                                                                            FROM
                                                                                trn_sampleinfomations_details 
                                                                              
                                                                            WHERE
                                                                            trn_sampleinfomations_details.intSampleNo =  '" . $row2['intSampleNo'] . "' AND
                                                                            trn_sampleinfomations_details.intSampleYear =  '" . $row2['intSampleYear'] . "' ";
                                                                if ($disableFlag == 0) {
                                                                    $sql1 .= "AND trn_sampleinfomations_details.intRevNo='$maxRevNo'";
                                                                }
                                                                $sql1 .= "ORDER BY strComboName
                                
                                        ";
                                                                if ($disableFlag == 0) {
                                                                    echo "<option value=\"\"></option>";
                                                                }
                                                                $result1 = $db->RunQuery($sql1);
                                                                while ($row1 = mysqli_fetch_array($result1)) {
                                                                    if ($row2['strCombo'] == $row1['strComboName'])
                                                                        echo "<option selected=\"selected\" value=\"" . $row1['strComboName'] . "\">" . $row1['strComboName'] . "</option>";
                                                                    else
                                                                        echo "<option value=\"" . $row1['strComboName'] . "\">" . $row1['strComboName'] . "</option>";
                                                                }

                                                                ?>
                                                            </select></td>
                                                        <?php
                                                        if (($row2['intRevisionNo'] != $maxRevNo) && ($disableFlag == 0)) {
                                                            $groundColor = '';
                                                        }
                                                        ?>
                                                        <td align="center"><select name="cboRevision" id="cboRevision"
                                                                                   style="width:45px"
                                                                                   class="cboRevision validate[required]" <?php echo $disable ?>>
                                                                <?php

                                                                #############################
                                                                ######## REVISION ##############
                                                                #############################
                                                                $sql1 = "SELECT DISTINCT
                                                                    trn_sampleinfomations.intRevisionNo
                                                                FROM trn_sampleinfomations
                                                                INNER JOIN trn_sampleinfomations_details 
                                                                ON 
                                                                    trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo AND 
                                                                    trn_sampleinfomations.intSampleYear = trn_sampleinfomations_details.intSampleYear AND 
                                                                    trn_sampleinfomations.intRevisionNo=trn_sampleinfomations_details.intRevNo 
                                                
                                                                WHERE 
                                                                    trn_sampleinfomations.intSampleNo = '" . $row2['intSampleNo'] . "' AND
                                                                    trn_sampleinfomations.intSampleYear = '" . $row2['intSampleYear'] . "' ";
                                                                if ($disableFlag == 0) {
                                                                    $sql1 .= "AND trn_sampleinfomations.intRevisionNo='$maxRevNo'";
                                                                }

                                                                if ($disableFlag == 0) {
                                                                    echo "<option value=\"\"></option>";
                                                                }
                                                                $result1 = $db->RunQuery($sql1);
                                                                while ($row1 = mysqli_fetch_array($result1)) {
                                                                    if ($row2['intRevisionNo'] == $row1['intRevisionNo'])
                                                                        echo "<option selected=\"selected\" value=\"" . $row1['intRevisionNo'] . "\">" . $row1['intRevisionNo'] . "</option>";
                                                                    else
                                                                        echo "<option value=\"" . $row1['intRevisionNo'] . "\">" . $row1['intRevisionNo'] . "</option>";
                                                                }
                                                                $resultQtyPricesEnabledFlag = 1;
                                                                $qtyPricesEnabledQuery = "SELECT costing_sample_header.QTY_PRICES, costing_sample_header.APPROVED_PRICE 
                                                                FROM costing_sample_header WHERE costing_sample_header.SAMPLE_NO = '" . $row2['intSampleNo'] . "' AND
                                                                    costing_sample_header.SAMPLE_YEAR = '" . $row2['intSampleYear'] . "' AND 
                                                                    costing_sample_header.REVISION = '" . $row2['intRevisionNo'] . "' AND
                                                                    costing_sample_header.COMBO = '" . $row2['strCombo'] . "' AND
                                                                    costing_sample_header.PRINT = '" . $row2['strPrintName'] . "' ";

                                                                $resultQtyPricesEnabled = $db->RunQuery($qtyPricesEnabledQuery);
                                                                $resultQtyPricesEnabledRow = mysqli_fetch_array($resultQtyPricesEnabled);
                                                                $resultQtyPricesEnabledFlag = $resultQtyPricesEnabledRow['QTY_PRICES'];
                                                                ?>
                                                            </select></td>

                                                        <td align="center"><input id="txtBrand" name="txtBrand"
                                                                                  value="<?php echo getBrand($row2['intSampleNo'], $row2['intSampleYear'], $row1['intRevisionNo']); ?>"
                                                                                  type="text" style="width:100px"
                                                                                  class="groundColor" disabled="disabled"/></td>
                                                        <td align="center"><input name="txtGroundColor" type="text"
                                                                                  id="txtGroundColor" style="width:100px"
                                                                                  value="<?php echo $groundColor; ?>"
                                                                                  class="groundColor" disabled="disabled"/></td>
                                                        <td align="center" class="cls_size"><img src="images/add_new.png" width="15"
                                                                                                 height="15"
                                                                                                 class="clsAddSizes mouseover"/>
                                                        </td>
                                                        <td align="center" class="clsCopy"
                                                            id="<?php echo $row2['intSalesOrderId']; ?>"><?php echo($count > 0 ? '<a class="button green small" id="butSizeCopy" name="butSizeCopy">Copy</a>' : ''); ?></td>
                                                        <td id="<?php echo($rcvQty . "-" . $excessQty . "-" . $fDQty) ?>"><input
                                                                    name="txtQty" type="text"
                                                                    id="txtQty" <?php if ($resultQtyPricesEnabledFlag == 1) { ?><?php echo $disable ?><?php } ?>
                                                                    style="width:60px; text-align:right" value="<?php
                                                            echo $row2['intQty']; ?>"
                                                                    class="validate[min[<?php echo '0'; /*($rcvQty-$excessQty-$fDQty)*/ ?>],custom[integer]] calculateValue clsQty"/>
                                                            <div id="dummyQty"
                                                                 style="display:none"><?php echo $row2['intQty']; ?></div>
                                                        </td>
                                                        <td width="100" nowrap="nowrap">
                                                            <select name="txtPrice" id="txtPrice"  style="width:50px"  class="calculateValue validate[required, min[0]] normalfntRight clsPrice" <?php echo $disable ?>>

                                                                <?php
                                                                echo "<option selected=\"selected\" value=\"" . $row2['dblPrice'] . "\">" . $row2['dblPrice'] . "</option>";
                                                                #############################
                                                                ######## REVISION ##############
                                                                #############################
                                                                if ($resultQtyPricesEnabledFlag == 1){
                                                                    $qtyPricesQuery = "SELECT LOWER_MARGIN, UPPER_MARGIN, PRICE FROM costing_sample_qty_wise WHERE costing_sample_qty_wise.SAMPLE_NO ='" . $row2['intSampleNo'] . "' AND 
                                                                                        costing_sample_qty_wise.SAMPLE_YEAR 		=  '" . $row2['intSampleYear'] . "' AND 
                                                                                        costing_sample_qty_wise.REVISION 		=  '" . $row2['intRevisionNo'] . "' AND 
                                                                                        costing_sample_qty_wise.COMBO 			=  '" . $row2['strCombo'] . "' AND 
                                                                                        costing_sample_qty_wise.PRINT 		=  '" . $row2['strPrintName'] . "'
                                                                                    ";
                                                                    $qtyPriceRsults = $db->RunQuery($qtyPricesQuery);
                                                                    while ($qtyRow = mysqli_fetch_array($qtyPriceRsults)) {
                                                                        if ($qtyRow['UPPER_MARGIN']==-2){
                                                                            $qtyRow['UPPER_MARGIN'] = 'INF';
                                                                        }
                                                                        $range = "  (".$qtyRow['LOWER_MARGIN']."-".$qtyRow['UPPER_MARGIN'].")";
                                                                        echo "<option value=\"" . $qtyRow['PRICE'] . "\" title=\"".$range."\">" . $qtyRow['PRICE'] . "</option>";
                                                                    }
                                                                } else if ($resultQtyPricesEnabledRow['APPROVED_PRICE'] != $row2['dblPrice']) {
                                                                    ?>
                                                                    <option value="<?php echo ($resultQtyPricesEnabledRow['APPROVED_PRICE']); ?>"><?php echo ($resultQtyPricesEnabledRow['APPROVED_PRICE']); ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </td>
<!--                                                        <td><input disabled="disabled" name="txtPrice" type="text" id="txtPrice"-->
<!--                                                                   style="width:60px; text-align:right"-->
<!--                                                                   value="--><?php //echo ($row2['dblPrice']); ?><!--"-->
<!--                                                                   class="calculateValue validate[required, min[0]] clsPrice"/></td>-->
                                                        <td><input name="txtValue" type="text" id="txtValue" disabled="disabled"
                                                                   align="right" style="width:90px;color:#06F; text-align:right"
                                                                   value="<?php echo number_format($row2['intQty']*$row2['dblPrice'], 2) ?>"
                                                                   class="calculateValue validate[min[0]] normalfntRight" <?php echo $disable ?>/>
                                                        </td>
                                                        <td><input value="<?php echo $row2['dblOverCutPercentage']; ?>"
                                                                   name="txtOverCutPer" type="text" id="txtOverCutPer"
                                                                   style="width:60px; text-align:right" <?php echo $disable ?>/>
                                                        </td>
                                                        <td><input value="<?php echo $row2['dblDamagePercentage']; ?>"
                                                                   name="txtDamagePer" type="text" id="txtDamagePer"
                                                                   style="width:60px; text-align:right" <?php echo $disable ?>/>
                                                        </td>

                                                        <td>
                                                            <?php
                                                            if ($row2['IS_GRADING'] == 1) {
                                                                ?>
                                                                <input type="checkbox" name="grading" id="grading" class="chek"
                                                                       value="1" checked> yes<br>
                                                                <input type="checkbox" name="grading" id="grading" class="chek"
                                                                       value="0"> no
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <input type="checkbox" name="grading" id="grading" class="chek"
                                                                       value="1"> yes<br>
                                                                <input type="checkbox" name="grading" id="grading" class="chek"
                                                                       value="0" checked> no
                                                                <?php
                                                            }
                                                            ?>
                                                        </td>
                                                        <td width="100" nowrap="nowrap"><input name= "txtPSD<?php echo $r; ?>"
                                                                                               id = "<?php  if ($row2['dtPSD'] != '') {?> txtPSD<?php echo $r; }?>"
                                                                                               type="text" class="<?php if($disableFlag == 0) echo "psDate dateType"; else echo "psDate"; ?>" style="width:80px" <?php echo $disable ?>
                                                                                               value="<?php
                                                                                               if ($row2['dtPSD'] != '')
                                                                                                   echo $row2['dtPSD'];
                                                                                               else
                                                                                                   echo "";
                                                                                               ?>"
                                                                                               onKeyPress="return ControlableKeyAccess(event);"
                                                                                               onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input
                                                                    type="reset" value="" style="visibility:hidden;width:1px"
                                                                    onclick="return showCalendar(this.id, '%Y-%m-%d');"/>
                                                            <input name="oldtxtPSD" id="oldtxtPSD"<?php echo $r; ?> style="display: none;"  value="<?php
                                                            if ($row2['dtPSD'] != '')
                                                                echo $row2['dtPSD'];
                                                            else
                                                                echo "";
                                                            ?>"/>
                                                        </td>
                                                        <td nowrap="nowrap" width="100"><input name="txtDelDate<?php echo $r; ?>"
                                                                                               id = "<?php  if ($row2['dtDeliveryDate'] != '') {?> txtDelDate<?php echo $r; }?>"
                                                                                               type="text"
                                                                                               class="<?php if($disableFlag == 0) echo "delDate dateType"; else echo "delDate"; ?>"
                                                                <?php echo $disable ?>
                                                                                               value="<?php
                                                                                               if ($row2['dtDeliveryDate'] != '')
                                                                                                   echo $row2['dtDeliveryDate'];
                                                                                               else
                                                                                                   echo "";
                                                                                               ?>"
                                                                                               onKeyPress="return ControlableKeyAccess(event);"
                                                                                               onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input
                                                                    type="reset" value="" style="visibility:hidden;width:1px"
                                                                    onclick="return showCalendar(this.id, '%Y-%m-%');"/>
                                                            <input name="oldtxtDelDate" id ="oldtxtDelDate"<?php echo $r; ?> style="display: none;"  value="<?php
                                                            if ($row2['dtDeliveryDate'] != '')
                                                                echo $row2['dtDeliveryDate'];
                                                            else
                                                                echo "";
                                                            ?>"/>
                                                        </td>
                                                        <td width="100" nowrap="nowrap"><select name="cboTechGrp"
                                                                                                disabled="disabled" id="cboTechGrp"
                                                                                                style="width:200px"
                                                                                                class="cboTechGrp validate[required]">
                                                                <?php
                                                                $sql1 = "SELECT
                                                                        mst_technique_groups.TECHNIQUE_GROUP_ID,
                                                                        mst_technique_groups.TECHNIQUE_GROUP_NAME
                                                                        FROM `mst_technique_groups`
                                                                        WHERE
                                                                        mst_technique_groups.`STATUS` = 1
                                                                        ORDER BY
                                                                        mst_technique_groups.TECHNIQUE_GROUP_NAME ASC
                                                                        ";
                                                                echo "<option value=\"0\"></option>";
                                                                $result1 = $db->RunQuery($sql1);
                                                                while ($row1 = mysqli_fetch_array($result1)) {
                                                                    if ($row2['TECHNIQUE_GROUP_ID'] == $row1['TECHNIQUE_GROUP_ID'])
                                                                        echo "<option selected=\"selected\" value=\"" . $row1['TECHNIQUE_GROUP_ID'] . "\">" . $row1['TECHNIQUE_GROUP_NAME'] . "</option>";
                                                                    else
                                                                        echo "<option value=\"" . $row1['TECHNIQUE_GROUP_ID'] . "\">" . $row1['TECHNIQUE_GROUP_NAME'] . "</option>";
                                                                }
                                                                ?>
                                                            </select></td>

                                                        <td bgcolor="#FFFFFF" width="40">

                                                            <?php
                                                            if ($poType != 1 && $row2['SO_TYPE'] == 0){
                                                                $row2['SO_TYPE'] = getSamplePressingStatus($row2['intSampleNo'],$row2['intSampleYear'],$row2['intRevisionNo'],$row2['strCombo'],$row2['strPrintName']);
                                                            }
                                                            ?>

                                                            <?php if ( $row2['SO_TYPE']==2 && $poType !=1) { ?><input type="checkbox"  name="splitPressing" id="splitPressing" <?php echo $disable ?>
                                                                                                                                                              value="2" checked> <?php }  ?>
                                                            <?php if ( $row2['SO_TYPE']==1 && $poType !=1) { ?><input type="checkbox"  name="splitPressing" id="splitPressing"
                                                                                                       value="1" <?php echo $disable ?> unchecked> <?php }  ?>
                                                            <?php if ( $row2['SO_TYPE']==0 || $poType ==1) { ?><input type="checkbox"  name="splitPressing" id="splitPressing"
                                                                                                       value="0" unchecked disabled> <?php }  ?>
                                                        </td>
                                                        <?php
                                                        $count++;
                                            }
                                         }
                               }
                               ?>
                                        </tr>
                                        <div id="newRow"></div>
                                        <tr class="dataRow">
                                            <td colspan="21" align="left" bgcolor="#FFFFFF"
                                                class="cls_insertRow"><?php if ($poType != 2) { ?><a id="butInsertRow"
                                                                                                     class="button white meedium"
                                                                                                     title="Click here to save order"
                                                                                                     name="butInsertRow">Add</a><?php }  ?>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="">
                                <table width="100%" border="0">
                                    <tr>
                                        <td width="56%" rowspan="4">
                                            <iframe id="iframeFiles"
                                                    src="presentation/customerAndOperation/bulk/placeOrder/addNew/filesUpload.php?txtFolder=<?php echo "$orderNo-$orderYear" . '&editMode=' . $editMode; ?>"
                                                    name="iframeFiles"
                                                    style="width:400px;height:175px;border:none"></iframe>
                                        </td>
                                        <td width="15%" class="normalfnt">Total Qty</td>
                                        <td width="10%"><input name="txtTotQty" type="text" disabled="disabled"
                                                               class="txtText" id="txtTotQty" style="width:90px"
                                                               value="0"/></td>
                                        <td width="9%" class="normalfnt">Total Value</td>
                                        <td width="10%"><input name="txtTotVal" type="text" disabled="disabled"
                                                               class="txtText" id="txtTotVal" style="width:90px"
                                                               value="0"/></td>
                                    </tr>
                                    <tr>
                                        <td class="normalfnt">Initial Dummy Qty</td>
                                        <td><input name="txtDummyQty" type="text" disabled="disabled" class=""
                                                   id="txtDummyQty" style="width:90px" value="<?php echo $dummyQty ?>"/>
                                        </td>
                                        <td class="normalfnt">
                                            <div id="childFlag" style="display:none"><?php echo $childFlag; ?></div>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="normalfnt">Actual Total Qty</td>
                                        <td><input name="txtActualTotQty" type="text" disabled="disabled" class=""
                                                   id="txtActualTotQty" style="width:90px"
                                                   value="<?php echo $actualTotQty ?>"/></td>
                                        <td class="normalfnt">
                                            <div id="childFlag" style="display:none"><?php echo $childFlag; ?></div>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="tableBorder_allRound"><a id="butNew" class="button white meedium"
                                                                               title="Click here to save order"
                                                                               name="butNew">New</a>
                                <a id="butSave" class="button white meedium" title="Click here to save order"
                                   name="butNew" <?php if ($editMode == 0) { ?> style="display:none"<?php } ?>>Save</a>
                                <a id="butSaveChild" class="button white meedium" title="Click here to save split row"
                                   style="display:none" name="butSaveChild">Save Split Orders</a>
                                <a id="butConfirm" class="button white meedium" title="Click here to confirm"
                                   style=" <?php if (($intStatus > 1) && ($confirmatonMode == 1) && ($orderNo != '')) {
                                       echo 'display:yes';
                                   } else { ?><?php echo 'display:none';
                                   } ?>  " name="butConfirm">Confirm</a>
                                <a id="butReport" class="button white meedium" title="Click here to view"
                                   name="butReport">Report</a>
                                <a id="butCopyPO" class="button white meedium" title="Click here to save order"
                                   name="butCopyPO">Copy PO</a>
                                <a href="main.php" class="button white meedium">Close</a></td>
                        </tr>
                    </table>
                </td>
                </tr>
            </table>
        </div>
    </div>
    <div style="display:none" id="template_split">
        <select name="cboSplitYear" id="cboSplitYear" style="width:60px" class="cboSplitYear">
            <?php
            $d = date('Y');
            echo "<option id=\"\" ></option>";
            for ($d; $d >= 2012; $d--) {
                /*	if($row2['intSampleYear']==$d)
                        echo "<option selected=\"selected\" id=\"$d\" >$d</option>";
                    else */
                echo "<option id=\"$d\" >$d</option>";
            }
            ?>
        </select><select name="cboSplitNo" id="cboSplitNo" class="cboSplitNo" style="width:75px">
            <option value=""></option>
            <?php
            #############################
            ######## SAMPLE NO ##########
            #############################
            $sql1 = "SELECT
							trn_orderheader.intOrderNo,
							trn_orderheader.intOrderYear
							FROM `trn_orderheader`
							WHERE
							trn_orderheader.DUMMY_PO_NO = '$orderNo' AND
							trn_orderheader.DUMMY_PO_YEAR = '$orderYear'
							";
            $result1 = $db->RunQuery($sql1);
            while ($row1 = mysqli_fetch_array($result1)) {
                /*if($row2['intOrderNo']==$row1['intOrderNo'])
                    echo "<option selected=\"selected\" value=\"".$row1['intOrderNo']."\">".$row1['intOrderNo']."</option>";
                else */
                echo "<option value=\"" . $row1['intOrderNo'] . "\">" . $row1['intOrderNo'] . "</option>";
            }
            ?>
        </select></div>
</form>
<div style="width:900px; position: absolute;display:none;z-index:100" id="popupContact1"></div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
<?php

function getSamplePressingStatus($sampleNo, $sampleYear, $revId, $comboId, $printName)
{
    global $db;

    $sql = "SELECT
					costing_sample_routing_process.COST
					FROM trn_sampleinfomations_prices
					LEFT JOIN costing_sample_routing_process ON
					costing_sample_routing_process.SAMPLE_NO=trn_sampleinfomations_prices.intSampleNo AND costing_sample_routing_process.SAMPLE_YEAR=trn_sampleinfomations_prices.intSampleYear
					AND costing_sample_routing_process.REVISION=trn_sampleinfomations_prices.intRevisionNo AND costing_sample_routing_process.COMBO=trn_sampleinfomations_prices.strCombo
					AND costing_sample_routing_process.PRINT=trn_sampleinfomations_prices.strPrintName AND costing_sample_routing_process.PROCESS_ID=14
				WHERE
					trn_sampleinfomations_prices.intSampleNo 		=  '$sampleNo' AND
					trn_sampleinfomations_prices.intSampleYear 		=  '$sampleYear' AND
					trn_sampleinfomations_prices.intRevisionNo 		=  '$revId' AND
					trn_sampleinfomations_prices.strCombo 			=  '$comboId' AND
					trn_sampleinfomations_prices.strPrintName 		=  '$printName'
				";
    $result = $db->RunQuery($sql);
    $row=mysqli_fetch_array($result);
    if($row['COST'] != null && $row['COST'] > 0){
        return 1;
    } else {
        return 0;
    }
}


function getRecordCount($orderNo, $orderYear)
{
    global $db;
    $sql = "SELECT
				trn_orderdetails.strSalesOrderNo
			FROM trn_orderdetails
			WHERE
				trn_orderdetails.intOrderNo 	=  '$orderNo' AND
				trn_orderdetails.intOrderYear 	=  '$orderYear'
			";
    $result = $db->RunQuery($sql);
    return mysqli_num_rows($result);
}

//------------------------------function load Header---------------------
function getBrand($sampleNo, $sampleYear, $revNo)
{
    global $db;
    $sql = "SELECT
				mst_brand.strName,
				mst_brand.intId
				FROM
				trn_sampleinfomations
				INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
				WHERE
				trn_sampleinfomations.intSampleNo = '$sampleNo' AND
				trn_sampleinfomations.intSampleYear = '$sampleYear' AND
				trn_sampleinfomations.intRevisionNo = '$revNo'";
    $result_brand = $db->RunQuery($sql);
    $row_brand = mysqli_fetch_array($result_brand);
    $brand = $row_brand['strName'];
    return $brand;
}

function loadHeader($serialNo, $year)
{
    global $db;
    $sql = "SELECT 
		   			tb1.DUMMY_PO_NO,
					tb1.DUMMY_PO_YEAR,
					tb1.strCustomerPoNo,
					tb1.intPaymentTerm, 
					tb1.dtDate,
					tb1.dtDeliveryDate,
					tb1.TENTATIVE,
					mst_customer.strName AS strCustomer,
					mst_customer.intId AS customerId,
					mst_customer_locations_header.strName AS strCustomerLocations,
					mst_financecurrency.strCode AS currencyCode,
					tb1.strContactPerson,
					tb1.strRemark,
					A.strUserName AS marketer,
					tb1.intStatus,
					mst_locations.intBlocked, 
					tb1.intReviseNo, 
					mst_locations.intCompanyId,
					tb1.intLocationId,
					B.strUserName AS creator,
					intApproveLevelStart,
					intCustomer,
					intCurrency,
					intCustomerLocation,
					intMarketer,
					LC_STATUS,
					TECHNIQUE_TYPE,
					PO_TYPE, 
					INSTANT_ORDER,
					(SELECT
					Sum(ware_fabricreceiveddetails.dblQty)
					FROM
					ware_fabricreceivedheader
					Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
					WHERE  ware_fabricreceivedheader.intStatus=1
					AND ware_fabricreceivedheader.intOrderNo = tb1.intOrderNo AND ware_fabricreceivedheader.intOrderYear = tb1.intOrderYear
					) as recvdQty 
				FROM
					trn_orderheader as tb1 
					Inner Join mst_customer ON mst_customer.intId = tb1.intCustomer
					Inner Join mst_customer_locations_header ON mst_customer_locations_header.intId = tb1.intCustomerLocation
					Inner Join mst_financecurrency ON mst_financecurrency.intId = tb1.intCurrency
					Inner Join sys_users AS A ON A.intUserId = tb1.intMarketer
					Inner Join sys_users AS B ON B.intUserId = tb1.intCreator 
					Inner Join mst_locations ON tb1.intLocationId = mst_locations.intId 
				WHERE
					tb1.intOrderNo 		=  '$serialNo' AND
					tb1.intOrderYear 	=  '$year'";
    //	echo $sql;
    $result = $db->RunQuery($sql);
    return $result;
}

//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode, $intStatus, $savedStat, $intUser)
{
    global $db;

    //echo $savedStat;
    $editMode = 0;
    $sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";

    $resultp = $db->RunQuery($sqlp);
    $rowp = mysqli_fetch_array($resultp);

    if ($rowp['intEdit'] == 1) {
        if ($intStatus == ($savedStat + 1) || ($intStatus == 0) || ($intStatus == -1)) {
            $editMode = 1;
        }
    }

    return $editMode;
}

//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode, $intStatus, $savedStat, $intUser)
{
    global $db;

    $confirmatonMode = 0;
    $k = $savedStat + 2 - $intStatus;
    $sqlp = "SELECT
		menupermision.int" . $k . "Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";

    $resultp = $db->RunQuery($sqlp);
    $rowp = mysqli_fetch_array($resultp);

    if ($rowp['int' . $k . 'Approval'] == 1) {
        if ($intStatus != 1) {
            $confirmatonMode = 1;
        }
    }

    return $confirmatonMode;
}

//--------------------------------------------------------
function getRcvQty($orderNo, $orderYear, $salesOrderId)
{
    global $db;

    $sqlp = "SELECT
IFNULL(Sum(ware_fabricreceiveddetails.dblQty),0) AS qty
FROM
ware_fabricreceivedheader
Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
WHERE
ware_fabricreceivedheader.intStatus =  '1' AND
ware_fabricreceivedheader.intOrderNo =  '$orderNo' AND
ware_fabricreceivedheader.intOrderYear =  '$orderYear' AND
ware_fabricreceiveddetails.intSalesOrderId =  '$salesOrderId'";

    $resultp = $db->RunQuery($sqlp);
    $rowp = mysqli_fetch_array($resultp);

    return $rowp['qty'];
}

//-----------------------------------------------------------
function getSalesWiseFieldsDisableFlag($orderNo, $orderYear, $salesOrderId)
{
    global $db;

    $sqlp = "SELECT
Sum(IFNULL(ware_fabricreceiveddetails.dblQty,0)) AS qty
FROM
ware_fabricreceivedheader
Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
WHERE
ware_fabricreceivedheader.intStatus >  '0' AND
ware_fabricreceivedheader.intOrderNo =  '$orderNo' AND
ware_fabricreceivedheader.intOrderYear =  '$orderYear' AND
ware_fabricreceiveddetails.intSalesOrderId =  '$salesOrderId'";

    $resultp = $db->RunQuery($sqlp);
    $rowp = mysqli_fetch_array($resultp);

    return $rowp['qty'];
}


function getPressingSalesOrder($orderNo, $orderYear, $salesOrderId)
{
    global $db;

    $sqlp = "SELECT intSalesOrderId 
    FROM
    trn_orderdetails
    WHERE
    trn_orderdetails.intOrderNo =  '$orderNo' AND
    trn_orderdetails.intOrderYear =  '$orderYear' AND
    trn_orderdetails.linkedSoId =  '$salesOrderId'";

    $resultp = $db->RunQuery($sqlp);
    $rowp = mysqli_fetch_array($resultp);

    return $rowp['intSalesOrderId'];
}

//-----------------------------------------------------------
function getOrderWiseFieldsDisableFlag($orderNo, $orderYear)
{
    global $db;

    $sqlp = "SELECT
Sum(IFNULL(ware_fabricreceiveddetails.dblQty,0)) AS qty
FROM
ware_fabricreceivedheader
Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
WHERE
ware_fabricreceivedheader.intStatus >  '0' AND
ware_fabricreceivedheader.intOrderNo =  '$orderNo' AND
ware_fabricreceivedheader.intOrderYear =  '$orderYear'";

    $resultp = $db->RunQuery($sqlp);
    $rowp = mysqli_fetch_array($resultp);

    return $rowp['qty'];
}

//-----------------------------------------------------------
function loadSalesOrderWiseExcessQty($orderNo, $orderYear, $salesOrderId, $excessFactor)
{
    global $db;
    $sql = "SELECT
			trn_orderdetails.intQty,
			trn_orderdetails.dblOverCutPercentage,
			trn_orderdetails.dblDamagePercentage
			FROM trn_orderdetails 
			WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_orderdetails.intSalesOrderId =  '$salesOrderId'";

    $result = $db->RunQuery($sql);
    $rows = mysqli_fetch_array($result);
    $value = $rows['intQty'] * ($rows['dblOverCutPercentage'] + $excessFactor) / 100;
    return (float)$value;
}

//-----------------------------------------------------------
function getGroundColor($sampNo, $sampleYear, $printName, $combo, $revNo)
{
    //intSampleNo,intSampleYear,intRevNo,strPrintName,strComboName,intColorId
    global $db;
    $sql = "SELECT DISTINCT
				trn_sampleinfomations_details.intGroundColor,
				mst_colors_ground.strName
				FROM
					trn_sampleinfomations_details 
					Inner Join mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
				WHERE
					trn_sampleinfomations_details.intSampleNo =  '$sampNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear'  AND
					trn_sampleinfomations_details.strPrintName =  '$printName' AND
					trn_sampleinfomations_details.strComboName =  '$combo' AND
					trn_sampleinfomations_details.intRevNo =  '$revNo'
					
					
					
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $g = $row['strName'];
    if ($revNo == '')
        $g = '';
    return $g;

}

//-----------------------------------------------------------
function loadDamageReturnedQty($location, $orderNo, $orderYear, $salesOrderId)
{
    global $db;
    $sql = "SELECT
				IFNULL(sum(ware_stocktransactions_fabric.dblQty*-1),0) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				/*ware_stocktransactions_fabric.intLocationId =  '$location' AND*/
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strType in(  'Dispatched_F' ,'Dispatched_P', 'Dispatched_CUT_RET')
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId";

    $result = $db->RunQuery($sql);
    $rows = mysqli_fetch_array($result);
    return (float)($rows['dblQty']);
}

//--------------------------------------------------------------------
function get_max_revision_no($sampNo, $sampleYear)
{
    global $db;
    $sql = "SELECT Max(trn_sampleinfomations.intRevisionNo) as maxRevNo 
					FROM `trn_sampleinfomations`
                                         left JOIN trn_sampleinfomations_combo_print_approvedby ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_combo_print_approvedby.SAMPLE_NO AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_combo_print_approvedby.SAMPLE_YEAR AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_combo_print_approvedby.REVISION
					WHERE
					(trn_sampleinfomations.intStatus = 1 OR
					trn_sampleinfomations_combo_print_approvedby.`STATUS` = 1 ) AND 
                                        trn_sampleinfomations.intSampleNo =  '$sampNo' AND
					trn_sampleinfomations.intSampleYear =  '$sampleYear' ";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);

    return $row['maxRevNo'];
}

//------------------------------------------------------------------
function getChildFlag($orderNo, $orderYear)
{

    global $db;
    $child_count = 0;
    $sql = "SELECT
				1 as flag
				FROM `trn_orderheader`
				WHERE
				trn_orderheader.DUMMY_PO_NO = '$orderNo' AND
				trn_orderheader.DUMMY_PO_YEAR = '$orderYear'
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $child_count = $row['flag'];
    return $child_count;

}

function getActualTotalQty($orderNo, $orderYear)
{

    global $db;
    $actualQty = 0;
    $sql = "SELECT
				Sum(trn_orderdetails.intQty) AS actualQty
				FROM
				trn_orderdetails
				INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
				WHERE
				(trn_orderheader.intOrderNo = '$orderNo' AND
				trn_orderheader.intOrderYear = '$orderYear') OR (
				trn_orderheader.DUMMY_PO_NO = '$orderNo' AND
				trn_orderheader.DUMMY_PO_YEAR = '$orderYear') ";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $actualQty = $row['actualQty'];
    return $actualQty;

}

function getFlagChild($orderNo, $orderYear, $salesOrderId)
{

    global $db;
    $flag = 0;

    $sql = "SELECT
				trn_orderheader.intOrderNo,
				trn_orderheader.intOrderYear
				FROM
				trn_orderheader
				INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderheader.DUMMY_PO_NO = '$orderNo' AND
				trn_orderheader.DUMMY_PO_YEAR = '$orderYear' AND
				trn_orderdetails.DUMMY_SO_ID = '$salesOrderId'
				limit 1 ";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $no = $row['intOrderNo'];

    if ($no > 0) {
        $flag = 1;
    }

    return $flag;

}


?>

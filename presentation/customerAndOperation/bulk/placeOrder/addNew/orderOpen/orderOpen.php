<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "libraries/jqgrid2/inc/jqgrid_dist.php";

$company 		= $sessions->getCompanyId();
$intUser  		= $sessions->getUserId();

$approveLevel 	= (int)getMaxApproveLevel();
$programCode	= 'P0427';


$sql = "select * from(SELECT DISTINCT 
							if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected','Pending')) as Status,
							tb1.intOrderNo as `Order_No`,
							tb1.intOrderYear as `Order_Year`,
							tb1.dtmModifyDate as `Completed_Date`,
							
							IFNULL((SELECT
								Sum(trn_orderdetails.intQty) 
								FROM trn_orderdetails
								WHERE
								trn_orderdetails.intOrderNo = tb1.intOrderNo AND
								trn_orderdetails.intOrderYear =  tb1.intOrderYear
							),0) as Order_Qty, 
					
							((IFNULL((SELECT
								SUM(ware_fabricdispatchdetails.dblSampleQty+
								ware_fabricdispatchdetails.dblGoodQty+
								ware_fabricdispatchdetails.dblEmbroideryQty+
								ware_fabricdispatchdetails.dblPDammageQty+
								ware_fabricdispatchdetails.dblFdammageQty+
								ware_fabricdispatchdetails.dblCutRetQty) 
								FROM
								ware_fabricdispatchheader
								Inner Join ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear
								WHERE
								ware_fabricdispatchheader.intStatus =  '1' AND
								ware_fabricdispatchheader.intOrderNo =   tb1.intOrderNo AND
								ware_fabricdispatchheader.intOrderYear =  tb1.intOrderYear 
							),0))) as Dispatched_Qty, 
							  
							'View' as `View`   
						FROM
							trn_orderheader as tb1
							Inner Join mst_customer ON tb1.intCustomer = mst_customer.intId
							Inner Join sys_users ON tb1.intCreator = sys_users.intUserId 
							Inner Join mst_locations ON tb1.intLocationId = mst_locations.intId 
							WHERE
							mst_locations.intCompanyId =  '$company' AND tb1.intStatus='-10'   
							)  as t where 1=1
						";
				//	 echo	$sql;
$col = array();

//ORDER NO
$col["title"] 	= "Order No"; // caption of column
$col["name"] = "Order_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
$col["class"] 	= "order";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist

$cols[] = $col;	$col=NULL;
$reportLink  = "?q=896&orderNo={Order_No}&orderYear={Order_Year}";

//ORDER YEAR
$col["title"] = "Order year"; // caption of column
$col["name"] = "Order_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//Order Qty
$col["title"] = "Order Qty"; // caption of column
$col["name"] = "Order_Qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//Order Qty
$col["title"] = "Dispatch Qty"; // caption of column
$col["name"] = "Dispatched_Qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Order Qty
$col["title"] = "Completed Date"; // caption of column
$col["name"] = "Completed_Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='rptBulkOrder.php'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Open Customer place Order";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'Order_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= true; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
	
) 
);




$out = $jq->render("list1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Open Customer PO</title>
<?php include 'include/listing.html'?>
<!--<script type="text/javascript" src="presentation/customerAndOperation/bulk/placeOrder/addNew/orderCompletion-js.js"></script>-->
<script type="text/javascript" src="libraries/javascript/jquery-impromptu.js"></script>
<script type="text/javascript" src="libraries/javascript/jquery-impromptu.min.js"></script>
</head>

<body>	
  <table width="100%" border="0">
    <tr>
      <td><div align="center" style="margin:10px"><?php echo $out?></div></td>
    </tr>
    <tr>
      <td align="center" class="tableBorder_allRound"><a id="butOpen" class="button white meedium" title="Click here to save order" name="butOpen" >OPEN ORDER</a></td>
    </tr>
  </table>
</body>
</html>
<?php
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(trn_orderheader.intApproveLevelStart) AS appLevel
			FROM trn_orderheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
?>
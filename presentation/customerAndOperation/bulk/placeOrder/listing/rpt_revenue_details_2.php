<?php
date_default_timezone_set('Asia/Kolkata');
//ini_set('allow_url_include',1);
//ini_set('display_errors',1);
session_start();

$backwardseperator 	= "../../../../../";
$thisFilePath 		=  $_SERVER['PHP_SELF'];
//include_once $backwardseperator."dataAccess/permisionCheck2.inc";
include_once $backwardseperator."dataAccess/Connector.php";

//include_once "../../../../../libraries/jqdrid/inc/jqgrid_dist.php";
include_once "../../../../../libraries/jqgrid2/inc/jqgrid_dist.php";

$company 	= $_SESSION['headCompanyId'];
$locationId = $_SESSION['CompanyID'];

$intUser  = $_SESSION["userId"];

$toCurrency=1;
//---------------------------------------------------------------------
	  	   $sql1 = "SELECT intId,strName FROM 
			mst_locations 
			where intStatus = 1
			ORDER BY
			mst_locations.strName ASC
			";
			$result1 = $db->RunQuery($sql1);
			$str='';
			//$str1 .= ":All;" ;
			$j=0;
			while($row=mysqli_fetch_array($result1))
			{
				if($j==0){
				$inilocId=$row['intId'];
				$inilocation=$row['strName'];
				}
				$locId=$row['intId'];
				$location=$row['strName'];
				$str .= $location.":".$location.";" ;
				$j++;
			}
			//$str = substr($str1,0,-1);
			$str .= ":All" ;
//---------------------------------------------------------------------

$sql = "select * from(SELECT
 
mst_locations.intId,
mst_locations.strName as Location,
mst_plant.intPlantId,
mst_plant.strPlantName as plant,
tb1.intDocumentNo as DispNo,
tb1.intDocumentYear as DispYear,
date(tb1.trns_Date) as date,
concat(tb1.intOrderYear,'/',tb1.intOrderNo) as orderNo,
(select  group_concat(strGraphicNo separator ', ') from  (select distinct strGraphicNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
tb.intOrderNo =  tb1.intOrderNo AND
tb.intOrderYear =  tb1.intOrderYear
) as strGraphicNo, 
mst_financecurrency.strCode as currency,
round((tb1.Qty*-1),2) AS Qty ,
round((tb1.amount),2) AS ammount ,
concat(mst_locations.strName,'/',mst_plant.strPlantName) as orderBy


 FROM `trns_fabric_dispatch_sales_order_wise` as tb1 
LEFT JOIN mst_locations ON mst_locations.intId = tb1.intLocationId
left JOIN mst_plant ON mst_plant.intPlantId = mst_locations.intPlant
/*left JOIN mst_financeexchangerate ON $toCurrency = mst_financeexchangerate.intCurrencyId 
AND tb1.orderDate = mst_financeexchangerate.dtmDate 
AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId */
LEFT JOIN mst_financecurrency ON mst_financecurrency.intId=$toCurrency 
)  as t where 1=1
						";
				//	 echo 	$sql;
$col = array();

$col["title"] 	= "Location"; // caption of column
$col["name"] 	= "Location"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "5";
$col["stype"] 	= "select";
$str = $str ;
$col["editoptions"] 	=  array("value"=> $str);
$col["align"] 	= "center";

$cols[] = $col;	$col=NULL;

//SALES ORDER NO
$col["title"] = "Plant"; // caption of column
$col["name"] = "plant"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//SALES ORDER NO
$col["title"] = "Date"; // caption of column
$col["name"] = "date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//SALES ORDER NO
$col["title"] = "Order No"; // caption of column
$col["name"] = "orderNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//SALES ORDER NO
$col["title"] = "Graphic No"; // caption of column
$col["name"] = "strGraphicNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//ORDER YEAR
$col["title"] = "Dispatch year"; // caption of column
$col["name"] = "DispYear"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$col['link']	= "../../fabricDispatchNote/listing/rptFabricDispatchNote.php?serialNo={DispNo}&year={DispYear}";	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

//ORDER NO
$col["title"] 	= "Dispatch No"; // caption of column
$col["name"] 	= "DispNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//searchOper
$col["align"] 	= "center";
$col['link']	= "../../fabricDispatchNote/listing/rptFabricDispatchNote.php?serialNo={DispNo}&year={DispYear}";	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

$cols[] = $col;	$col=NULL;



//ORDER YEAR
$col["title"] = "Good Qty"; // caption of column
$col["name"] = "Qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

$col["title"] = "Currency"; // caption of column
$col["name"] = "currency"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//CUSTOMER PO NO
$col["title"] = "Amount"; // caption of column
$col["name"] = "ammount"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Revenue Details Report";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'orderBy'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Location","op":"eq","data":"$inilocId"}
     ]
}
SEARCH_JSON;


			//	$inilocId=$row['intId'];
			//	$inilocation=$row['strName'];


$grid["search"] = true; 
$grid["postData"] = array("filters" => $sarr ); 

// export XLS file
// export to excel parameters - range could be "all" or "filtered"
$grid["export"] = array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"Revenue Details Report");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
$grid["export"]["range"] = "filtered"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);




$out = $jq->render("list1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Revenue Details Report</title>

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />


</head>

<body>	
<form id="frmlisting" name="frmlisting" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
	<link rel="stylesheet" type="text/css" media="screen" href="../../../../../libraries/jqdrid/js/themes/smoothness/jquery-ui.custom.css"></link>	
	<link rel="stylesheet" type="text/css" media="screen" href="../../../../../libraries/jqdrid/js/jqgrid/css/ui.jqgrid.css"></link>	
	
	<script src="../../../../../libraries/jqdrid/js/jquery.min.js" type="text/javascript"></script>
	<script src="../../../../../libraries/jqdrid/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script src="../../../../../libraries/jqdrid/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="../../../../../libraries/jqdrid/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
	<script src="../../../../../libraries/javascript/script.js" type="text/javascript"></script>
	
    <td><table width="100%" border="0">
      <tr>
        <td>
        <div align="center" style="margin:10px">
        
			<?php echo $out?>
        </div>
        </td>
      </tr>

    </table></td>
    </tr>


</form>
</body>
</html>
<?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(trn_orderheader.intApproveLevelStart) AS appLevel
			FROM trn_orderheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
?>


<?php 
ini_set('display_errors',0);
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
 	
	$programName='Place Order';
	$programCode='P0427';
	
	//ini_set('display_errors',1);
	include "{$backwardseperator}dataAccess/Connector.php";
	include "../../../../../libraries/mail/mail.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_mail.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_permisions.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
	require_once $_SESSION['ROOT_PATH']."class/warehouse/cls_warehouse_get.php";
	
	$objMail 			= new cls_create_mail($db);
	$obj_permision		= new cls_permisions($db);
	$obj_commom			= new cls_commonFunctions_get($db);
	$obj_warehouse_get	= new cls_warehouse_get($db);
	//$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	//$requestType 			= $_REQUEST['requestType'];
	
	$orderNo 	= $_REQUEST['orderNo'];
	$orderYear 	= $_REQUEST['orderYear'];
	
	if($_REQUEST['status']=='approve')
	{
		$db->begin();
		
		$errorFlag	=0;
		
		
		
		$sql = "UPDATE `trn_orderheader` SET `intStatus`=intStatus-1 WHERE (`intOrderNo`='$orderNo') AND (`intOrderYear`='$orderYear') and intStatus not in(0,1)";
		$result = $db->RunQuery2($sql);
	
		if($result){
			 $sql = "	SELECT 
							trn_orderheader.intReviseNo,
							trn_orderheader.intCreator,
							trn_orderheader.intStatus,
							trn_orderheader.intApproveLevelStart,
							trn_orderheader.strCustomerPoNo , 
							trn_orderheader.intLocationId,  
							mst_locations.intCompanyId 
						FROM trn_orderheader 
						Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
						WHERE
							trn_orderheader.intOrderNo =  '$orderNo' AND
							trn_orderheader.intOrderYear =  '$orderYear'
						";
			$result2 = $db->RunQuery2($sql);
			$row2=mysqli_fetch_array($result2);
			$createdUser = $row2['intCreator'];
			$status = $row2['intStatus'];
			$savedLevels = $row2['intApproveLevelStart'];
			$customerPO= $row2['strCustomerPoNo'];
			$trnsLocation	= $row2['intLocationId'];
			$bulk_rev	= $row2['intReviseNo'];
			
			$approveLevel=$savedLevels+1-$status;
		
			//created user can't raise the 2nd approval
			if($approveLevel>=2 && $createdUser ==$userId){
				$errorFlag	=	1;
				$msg	=	"Approval ".$approveLevel." is denied for created user";
			}
		
			 /////////////////// insert approval table ////////////////////
			 $sql = "INSERT INTO `trn_orderheader_approvedby` (`intOrderNo`,`intYear`,`intApproveLevelNo`,`intApproveUser`,`dtApprovedDate`,intStatus) VALUES ('$orderNo','$orderYear','$approveLevel','$userId',now(),0)";
			 $result = $db->RunQuery2($sql);
			 //////////////////////////////////////////////////////////////

			if($status !=1){
			//sendConfirmEmails($orderNo,$orderYear,$customerPO,$programCode,$row2['intApproveLevelStart'],$row2['intStatus'],$row2['intCompanyId'],$objMail);
			}
			 
			if($status ==1)
			{
				//if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
				sendFinlConfirmedEmailToUser($orderNo,$orderYear,$customerPO,$objMail,$mainPath,$root_path);
				//include "sample_color_items_autoAllocation.php";
				//}
			}



			
			if($status == $savedLevels){
			
				$app_fag	= getSpecialTechApprovedSataus($orderNo,$orderYear);
				
				if($app_fag==0){
					$errorFlag	=	1;
					$msg	=	"Please approve consumptions for special techniques";
				}
			
				$response_prn	=generatePRN($orderNo,$orderYear,$bulk_rev);

				if($response_prn['type']=='fail'){
					$msg	=$response_prn['msg']." ".$response_prn['q'];
					$errorFlag	=1;
				}
			}
 			
		}

		if($errorFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $msg;
			$db->rollback();
		}
		else if($errorFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Approved successfully.';
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}

		$db->commit();
		//if($_REQUEST['status']==)
		echo json_encode($response);
		
		
	}
	else if($_REQUEST['status']=='reject')
	{
		$db->begin();
	
		$errorFlag		=0;
		$arrHeader		= json_decode($_REQUEST['arrHeader'], true);
		$reject_reason	= $obj_commom->replace($arrHeader['reason']);

		$sql = "UPDATE `trn_orderheader` SET `intStatus`=0,REJECT_REASON = '$reject_reason' WHERE (`intOrderNo`='$orderNo') AND (`intOrderYear`='$orderYear') ";
		$result = $db->RunQuery2($sql);
		
		cancelPRNandPOs($orderNo,$orderYear,'Rejected');
		//$errorFlag		=1;
		//$sql = "DELETE FROM `trn_orderheader_approvedby` WHERE (`intOrderNo`='$orderNo') AND (`intYear`='$orderYear')";
		//$result2 = $db->RunQuery($sql);
		
		$sqlI = "INSERT INTO `trn_orderheader_approvedby` (`intOrderNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$orderNo','$orderYear','0','$userId',now(),0)";
		$resultI = $db->RunQuery2($sqlI);
		
				 $sql = "	SELECT 
								trn_orderheader.intCreator, 
								trn_orderheader.intStatus,
								trn_orderheader.intApproveLevelStart,
								trn_orderheader.strCustomerPoNo ,
		 						trn_orderheader.intReviseNo 
							FROM trn_orderheader
							WHERE
								trn_orderheader.intOrderNo =  '$orderNo' AND
								trn_orderheader.intOrderYear =  '$orderYear'
							";
				$result2 = $db->RunQuery2($sql);
				$row2=mysqli_fetch_array($result2);
				$createdUser = $row['intCreator'];
				$status = $row2['intStatus'];
				$savedLevels = $row2['intApproveLevelStart'];
				$customerPO= $row2['strCustomerPoNo'];
				$bulk_rev= $row2['intReviseNo'];
			if($status ==0)
			{
				save_history($orderNo,$orderYear,$bulk_rev);
				//if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
				sendRejecedEmailToUser($orderNo,$orderYear,$customerPO,$objMail,$mainPath,$root_path);
				//}
			}
	
		if($errorFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $msg;
			$db->rollback();
		}
		else if($errorFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Rejected successfully.';
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		$db->commit();
		//if($_REQUEST['status']==)
		echo json_encode($response);
		
	}
	
	
	else if($_REQUEST['status']=='revisePO')
	{

		$db->begin();

		$PONo  = $_REQUEST['PONo'];
		$PONoArray=explode("/",$PONo);
		$orderNo=$PONoArray[0];
		$orderYear=$PONoArray[1];
		
		 $sql = "	SELECT  
		 				trn_orderheader.intReviseNo,
						trn_orderheader.intCreator, 
						trn_orderheader.intStatus,
						trn_orderheader.intApproveLevelStart,
						trn_orderheader.strCustomerPoNo  
					FROM trn_orderheader
					WHERE
						trn_orderheader.intOrderNo =  '$orderNo' AND
						trn_orderheader.intOrderYear =  '$orderYear'
					";
		$result2 		= $db->RunQuery2($sql);
		$row2			= mysqli_fetch_array($result2);
		$createdUser 	= $row2['intCreator'];
		$status 		= $row2['intStatus'];
		$savedLevels 	= $row2['intApproveLevelStart'];
		$customerPO		= $row2['strCustomerPoNo'];
		$bulk_rev		= $row2['intReviseNo'];
		
		
	  	$sql = "UPDATE `trn_orderheader` SET `intStatus`=-1 WHERE (`intOrderNo`='$orderNo') AND (`intOrderYear`='$orderYear')  ";
		$result=$db->RunQuery2($sql);
		
		cancelPRNandPOs($orderNo,$orderYear,'Revised');
		save_history($orderNo,$orderYear,$bulk_rev);
	
	
		//$sql = "DELETE FROM `trn_orderheader_approvedby` WHERE (`intOrderNo`='$orderNo') AND (`intYear`='$orderYear')";
		//$resultD=$db->RunQuery($sql);
		$maxAppByStatus=(int)getMaxAppByStatus($orderNo,$orderYear)+1;
		$sql = "UPDATE `trn_orderheader_approvedby` SET intStatus ='$maxAppByStatus' 
				WHERE (`intOrderNo`='$orderNo') AND (`intYear`='$orderYear') AND (`intStatus`='0')";
		$result1 = $db->RunQuery2($sql);
		
		
		$sqlI = "INSERT INTO `trn_orderheader_approvedby` (`intOrderNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$orderNo','$orderYear','-1','$userId',now(),0)";
		$resultI = $db->RunQuery2($sqlI);
		
		if($result){
			//if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
			sendRevisedEmailToUser($orderNo,$orderYear,$customerPO,$objMail,$mainPath,$root_path);
			//}
		}
				
		if($errorFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $msg;
			$db->rollback();
		}
		else if($errorFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'PO is revised successfully.';
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		$db->commit();
		//if($_REQUEST['status']==)
		echo json_encode($response);
		////////////////////////////////////////////////////////
			
	}
	
//-------	----------------------------------------------------------------------
	function checkSizes($orderNo,$orderYear)
	{
		global $db;
		$sql = "SELECT
					trn_orderdetails.intOrderNo,
					trn_orderdetails.intOrderYear,
					trn_orderdetails.strSalesOrderNo,
					trn_ordersizeqty.strSize,
					trn_ordersizeqty.dblQty
				FROM
				trn_orderdetails
					Left Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_ordersizeqty.intSalesOrderId = trn_orderdetails.intSalesOrderId
				WHERE
					trn_orderdetails.intOrderNo =  '$orderNo' AND
					trn_orderdetails.intOrderYear =  '$orderYear' AND
					trn_ordersizeqty.strSize IS NULL 
				";
		$result = $db->RunQuery($sql);
		$rows=mysqli_num_rows($result);
		if($rows>0)
			return false;
		else
			return true;
	}
//--------------------------------------------------------
function sendConfirmEmails($serialNo,$year,$customerPO,$programCode,$savedStat,$intStatus,$companyId,$objMail){
	global $db;
				
		$k=$savedStat+2-$intStatus;
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail
			FROM menupermision 
			Inner Join menus ON menupermision.intMenuId = menus.intId 
			Inner Join sys_users ON menupermision.intUserId = sys_users.intUserId
			Inner Join mst_locations_user ON sys_users.intUserId = mst_locations_user.intUserId
			Inner Join mst_locations ON mst_locations_user.intLocationId = mst_locations.intId
			WHERE
			menus.strCode =  '$programCode' AND 
			menupermision.int".$k."Approval='1'  AND 
			mst_locations.intCompanyId='$companyId'  
			GROUP BY
			sys_users.intUserId	";	
				
		 $result = $db->RunQuery($sql);
		 
		while($row=mysqli_fetch_array($result))
		{
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="CUSTOMER PO FOR APPROVAL ($serialNo/$year)"; 
			 
			//send mails ////
 			$requestArray = NULL;
			
			$requestArray['program']='CUSTOMER PO';
			$requestArray['field1']='Order No';
			$requestArray['field2']='Order Year';
			$requestArray['field3']='customerPO';
			$requestArray['field4']='';
			$requestArray['field5']='';
			$requestArray['value1']=$serialNo;
			$requestArray['value2']=$year;
			$requestArray['value3']=$customerPO;
			$requestArray['value4']='';
			$requestArray['value5']='';
			
			$requestArray['subject']= $header;
			
			$requestArray['statement1']="This Customer PO is Approved";
			$requestArray['statement2']="to view this";
			//$_REQUEST['link']=urlencode(base64_encode($mainPath."presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder.php?orderNo=$serialNo&orderYear=$year&approveMode=1"));
			$requestArray['link']=base64_encode($_SESSION['MAIN_URL']."?q=896&orderNo=$serialNo&orderYear=$year&approveMode=1"); 

			$objMail->send_Response_Mail('mail_approval_template.php',$requestArray,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
		} 
}
//--------------------------------------------------------
function sendFinlConfirmedEmailToUser($serialNo,$year,$customerPO,$objMail,$mainPat,$root_path){
	global $db;
	global $userId;
				
		    $sql = "SELECT
			trn_orderheader.intCreator,
			trn_orderheader.intMarketer,
			finance_customer_invoice_header.CREATED_BY 
			FROM 
			trn_orderheader
			LEFT JOIN finance_customer_invoice_header ON trn_orderheader.intOrderNo = finance_customer_invoice_header.ORDER_NO AND trn_orderheader.intOrderYear = finance_customer_invoice_header.ORDER_YEAR
			WHERE
			trn_orderheader.intOrderNo =  '$serialNo' AND
			trn_orderheader.intOrderYear =  '$year'";
			$result = $db->RunQuery2($sql);
 			$row=mysqli_fetch_array($result);
			$created_user	= $row['intCreator'];
			
			$creator_details	=	getSysUserDetails2($row['intCreator']);
			$marketer_details	=	getSysUserDetails2($row['intMarketer']);
			$invoicer_details	=	getSysUserDetails2($row['CREATED_BY']);
			
			$enterUserName 		= $row['strFullName'];
			$enterUserEmail 	= $row['strEmail'];
			 
			$header="CONFIRMED CUSTOMER PO ('$serialNo'/'$year')"; 
			 
			//send mails ////
 			$requestArray = NULL;
			
			$requestArray['program']='CUSTOMER PO';
			$requestArray['field1']='Order No';
			$requestArray['field2']='Order Year';
			$requestArray['field3']='Customer PO';
			$requestArray['field4']='';
			$requestArray['field5']='';
			$requestArray['value1']=$serialNo;
			$requestArray['value2']=$year;
			$requestArray['value3']=$customerPO;
			$requestArray['value4']='';
			$requestArray['value5']='';
			
			$requestArray['subject']="CONFIRMED CUSTOMER PO ('$serialNo'/'$year')";
			
			$requestArray['statement1']="Approved this";
			$requestArray['statement2']="to view this";
			//$_REQUEST['link']=base64_encode($mainPath."presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder.php?orderNo=$serialNo&orderYear=$year&approveMode=1"); 
			$_REQUEST['link']=base64_encode($_SESSION['MAIN_URL']."?q=896&orderNo=$serialNo&orderYear=$year"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			//$objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
			if($userId != $created_user){
			$objMail->send_Response_Mail2($path,$requestArray,$_SESSION["systemUserName"],$_SESSION["email"],$header,$creator_details['email'],$creator_details['fulll_name']);
			}
			$objMail->send_Response_Mail2($path,$requestArray,$_SESSION["systemUserName"],$_SESSION["email"],$header,$marketer_details['email'],$marketer_details['fulll_name']);
			$objMail->send_Response_Mail2($path,$requestArray,$_SESSION["systemUserName"],$_SESSION["email"],$header,$invoicer_details['email'],$invoicer_details['fulll_name']);
			

}
//--------------------------------------------------------
function sendRejecedEmailToUser($serialNo,$year,$customerPO,$objMail,$mainPat,$root_path){
	global $db;
	global $userId;
				
		    $sql = "SELECT
			trn_orderheader.intCreator,
			trn_orderheader.intMarketer,
			trn_orderheader.REJECT_REASON,
			finance_customer_invoice_header.CREATED_BY 
			FROM 
			trn_orderheader
			LEFT JOIN finance_customer_invoice_header ON trn_orderheader.intOrderNo = finance_customer_invoice_header.ORDER_NO AND trn_orderheader.intOrderYear = finance_customer_invoice_header.ORDER_YEAR
			WHERE
			trn_orderheader.intOrderNo =  '$serialNo' AND
			trn_orderheader.intOrderYear =  '$year'";
			$result = $db->RunQuery2($sql);
 			$row=mysqli_fetch_array($result);
			$created_user	= $row['intCreator'];
			
			$creator_details	=	getSysUserDetails2($row['intCreator']);
			$marketer_details	=	getSysUserDetails2($row['intMarketer']);
			$invoicer_details	=	getSysUserDetails2($row['CREATED_BY']);

			
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="REJECTED CUSTOMER PO ('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='CUSTOMER PO';
			$_REQUEST['field1']='Order No';
			$_REQUEST['field2']='Order Year';
			$_REQUEST['field3']='Customer PO';
			$_REQUEST['field4']='Rejected Reason';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']=$customerPO;
			$_REQUEST['value4']=$row['REJECT_REASON'];
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="REJECTED CUSTOMER PO ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Rejected this";
			$_REQUEST['statement2']="to view this";
			//$_REQUEST['link']=base64_encode($mainPath."presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder.php?orderNo=$serialNo&orderYear=$year&approveMode=1"); 
			$_REQUEST['link']=base64_encode($_SESSION['MAIN_URL']."?q=896&orderNo=$serialNo&orderYear=$year"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
 			//$objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
			if($userId != $created_user){
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$creator_details['email'],$creator_details['fulll_name']);
			}
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$marketer_details['email'],$marketer_details['fulll_name']);
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$invoicer_details['email'],$invoicer_details['fulll_name']);
}
//--------------------------------------------------------
function sendRevisedEmailToUser($serialNo,$year,$customerPO,$objMail,$mainPat,$root_path){
	global $db;
	global $userId;
	
		    $sql = "SELECT
			trn_orderheader.intCreator,
			trn_orderheader.intMarketer,
			trn_orderheader.strCustomerPoNo, 
			finance_customer_invoice_header.CREATED_BY 
			FROM 
			trn_orderheader
			LEFT JOIN finance_customer_invoice_header ON trn_orderheader.intOrderNo = finance_customer_invoice_header.ORDER_NO AND trn_orderheader.intOrderYear = finance_customer_invoice_header.ORDER_YEAR
			WHERE
			trn_orderheader.intOrderNo =  '$serialNo' AND
			trn_orderheader.intOrderYear =  '$year'";
			$result = $db->RunQuery2($sql);
 			$row=mysqli_fetch_array($result);
			$created_user	= $row['intCreator'];
			
			$creator_details	=	getSysUserDetails2($row['intCreator']);
			$marketer_details	=	getSysUserDetails2($row['intMarketer']);
			$invoicer_details	=	getSysUserDetails2($row['CREATED_BY']);
			$customerPO = $row['strCustomerPoNo'];
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="REVISED CUSTOMER PO ('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='CUSTOMER PO';
			$_REQUEST['field1']='Order No';
			$_REQUEST['field2']='Order Year';
			$_REQUEST['field3']='Customer PO';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']=$customerPO;
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="REVISED CUSTOMER PO ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Revised this";
			$_REQUEST['statement2']="to view this";
			//$_REQUEST['link']=base64_encode($mainPath."presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder.php?orderNo=$serialNo&orderYear=$year&approveMode=1"); 
			$_REQUEST['link']=base64_encode($_SESSION['MAIN_URL']."?q=896&orderNo=$serialNo&orderYear=$year"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			//$objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
			if($userId != $created_user){
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$creator_details['email'],$creator_details['fulll_name']);
			}
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$marketer_details['email'],$marketer_details['fulll_name']);
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$invoicer_details['email'],$invoicer_details['fulll_name']);

}
//------------------------------function get MaxAppByStatus---------------------
function getMaxAppByStatus($serialNo,$year){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	  $sql = "SELECT
				max(trn_orderheader_approvedby.intStatus) as status 
				FROM
				trn_orderheader_approvedby
				WHERE
				trn_orderheader_approvedby.intOrderNo =  '$serialNo' AND
				trn_orderheader_approvedby.intYear =  '$year'";
				
		$resultm = $db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm['status'];

}
//--------------------------------------------------------
function getSysUserDetails($userId){
	global $db;
	

	  $sql = "SELECT
				sys_users.strFullName as fulll_name,
				sys_users.strEmail as email 
				FROM 
				sys_users
				WHERE sys_users.intUserId ='$userId'";
				
		$resultm = $db->RunQuery($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm;

}
//-------------------------------------------------------
function getSysUserDetails2($userId){
	global $db;
	

	  $sql = "SELECT
				sys_users.strFullName as fulll_name,
				sys_users.strEmail as email 
				FROM 
				sys_users
				WHERE sys_users.intUserId ='$userId'";
				
		$resultm = $db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm;

}
//--------------------------------------------------------
function generatePRN($orderNo,$orderYear,$bulk_rev){
	
	$prn_no	=	generateSerial();
	$year 	=	date('Y');
	$response	=saveHeader($prn_no,$year,$orderNo,$orderYear,$bulk_rev);
	if($response['type'] =='pass')
	$response	=saveDetails($prn_no,$year,$orderNo,$orderYear);
	
 	//else{
	//$response['saved']=0;
	//$response			=$response_h;
	//}
 	generatePRNemail($prn_no,$year,$orderNo,$orderYear,$response['qty']);
 	return $response;
}

function generateSerial(){
	
	global $db;
	global $companyId;
	$sql = "SELECT
			sys_no.intPRNno
			FROM sys_no
			WHERE
			sys_no.intCompanyId =  '$companyId'
			";	
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	$no = $row['intPRNno'];
	
	$sql = "UPDATE `sys_no` SET intPRNno=intPRNno+1 WHERE (`intCompanyId`='$companyId')  ";
	$db->RunQuery2($sql);
	
	return $no;
}

function saveHeader($prn_no,$year,$orderNo,$orderYear,$bulk_rev){
	
	$ApproveLevels=(int)getApproveLevel2('Purchase Request Note');
	$status		  =1;
	global $db;
	global	$userId;
	global  $companyId;
	
	$psd	=getMinPSDdate($orderNo,$orderYear);
	
	$sql	= " INSERT INTO trn_prnheader (
				intPrnNo,
				intYear,
				intDepartment,
				intInternalUsage,
				dtmRequiredDate,
				dtmPrnDate,
				strRemarks,
				intStatus,
				intApproveLevels,
				intUser,
				intCompany,
				intModifiedBy,
				dtmModifiedDate,
				intOrderNo,
				intOrderYear,
				intPORaised,
				intHideInGrpPrnReport,
				PO_TYPE,
				BULK_REVISION_NO,
				SYSTEM_GENERATED 
				) VALUES 
				(
				'$prn_no',
				'$year',
				2,
				0,
				'$psd',
				DATE(NOW()),
				'System Generated PRN',
				'$status',
				'$ApproveLevels',
				'$userId',
				'$companyId',
				NULL,
				NULL,
				'$orderNo',
				'$orderYear',
				0,
				NULL,
				1,
				'$bulk_rev',
				1
				) ";
	$res	=$db->RunQuery2($sql);
	if(!$res){
		$response['type'] 		= 'fail';
		$response['msg'] 		= $db->errormsg;
		$response['sql'] 		= $sql;
	}
	else{
		$response['type'] 		= 'pass';
		$response['msg'] 		= '';
		$response['sql'] 		= '';
	}
 	return $response;
}

function saveDetails($prn_no,$year,$orderNo,$orderYear){
	
	global $db;
	$saved			=0;
	$saved_p		=0;
	$saved_prn_qty	=0;
	
	//CHECK FOR TECHNIQUES WHICH DO NOT USE RM
	$non_rm_flag	=getNonRMTechniquesFlag($orderNo,$orderYear);	
	//-----------------------------------------
	$result_foil_update	= update_unwanted_rm_qty_to_zero($orderNo,$orderYear);//update unwanted rm

	$sql	= "SELECT
				trn_orderdetails.intSalesOrderId,
				trn_orderdetails.strSalesOrderNo,
				trn_orderdetails.intQty,
				trn_orderdetails.dblToleratePercentage,
				trn_orderdetails.dblOverCutPercentage,
				trn_orderdetails.dblDamagePercentage,
				trn_orderdetails.intSampleNo,
				trn_orderdetails.intSampleYear,
				trn_orderdetails.intRevisionNo,
				trn_orderdetails.strPrintName,
				trn_orderdetails.strCombo,
				trn_orderdetails.intPart,
				trn_orderdetails.dtPSD,
				DATEDIFF(date(trn_orderheader.dtmCreateDate),date('2017-05-18')) AS DiffDate
				FROM `trn_orderdetails`
				INNER JOIN trn_orderheader on 
trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo and trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
				WHERE
				trn_orderdetails.intOrderNo 	= '$orderNo' AND
				trn_orderdetails.intOrderYear 	= '$orderYear'";
	$result = $db->RunQuery2($sql);
	while($row=mysqli_fetch_array($result)){
		$sales_order	=	$row['intSalesOrderId'];
		$sales_order_no	=	$row['strSalesOrderNo'];
		$sampleNo		=	$row['intSampleNo'];
		$sampleYear		=	$row['intSampleYear'];
		$revision		=	$row['intRevisionNo'];
		$combo			=	$row['strCombo'];
		$print			=	$row['strPrintName'];
		$part			=	$row['intPart'];
		
		$orderQty		=	$row['intQty'];
		$overCut		=	$row['dblOverCutPercentage'];
		$tollerance		=	$row['dblToleratePercentage'];
		$dammage		=	$row['dblDamagePercentage'];
		$psd			=	$row['dtPSD'];
		
		if($row['DiffDate']< 0)
		$qty			= 	$orderQty+$orderQty*($overCut+$tollerance+$dammage)/100;
		else
		$qty			= 	$orderQty+$orderQty*($overCut+$dammage)/100;
		
		$qty			=	ceil($qty);
		$response_q	=update_bulk_po_qtys($prn_no,$year,$orderQty,$qty);
		if(!$response_q['res']){
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['sql'] 		= $response_d['q'];
		}
		
	
		//----------foil-----------------------------------------
		$result_foil	= foil_item_sampleCosumptionForSampleNo_sql($sampleNo,$sampleYear,$revision,$combo,$print,$part);
		while($row_foil	=mysqli_fetch_array($result_foil)){
				$item			=	$row_foil['intItem'];
				$conPC			=	$row_foil['CONSUMPTION'];
				$to_producrd	=	$qty;
				$Qty			= 	$qty*$conPC;
				
				$response_d	=save_po_prn_details($orderNo,$orderYear,$item,$Qty);
				if(!$response_d['res']){
					$response['type'] 		= 'fail';
					$response['msg'] 		= $db->errormsg;
					$response['sql'] 		= $response_d['q'];
				}
				else
				$saved_p++;
				$response_d	=save_po_prn_details_so_wise($orderNo,$orderYear,$sales_order,$item,$Qty,$conPC,$to_producrd);
				if(!$response_d['res']){
					$response['type'] 		= 'fail';
					$response['msg'] 		= $db->errormsg;
					$response['sql'] 		= $response_d['q'];
				}
				else
				$saved_p++;
				
		}
		//----------special RM-----------------------------------
		$result_sp	= special_item_sampleCosumptionForSampleNo_sql($sampleNo,$sampleYear,$revision,$combo,$print,$part);
		while($row_sp	=mysqli_fetch_array($result_sp)){
				$item	=	$row_sp['intItem'];
				$conPC	=	$row_sp['CONSUMPTION'];
				$to_producrd	=	$qty;
				$Qty	= 	$qty*$conPC;

				$response_d	=save_po_prn_details($orderNo,$orderYear,$item,$Qty);
				if(!$response_d['res']){
					$response['type'] 		= 'fail';
					$response['msg'] 		= $db->errormsg;
					$response['sql'] 		= $response_d['q'];
				}
				else
				$saved_p++;
				$response_d	=save_po_prn_details_so_wise($orderNo,$orderYear,$sales_order,$item,$Qty,$conPC,$to_producrd);
				if(!$response_d['res']){
					$response['type'] 		= 'fail';
					$response['msg'] 		= $db->errormsg;
					$response['sql'] 		= $response_d['q'];
				}
				else
				$saved_p++;

		}
		//----------ink------------------------------------------
		$result_ink	= ink_item_sampleCosumptionForSampleNo_sql($sampleNo,$sampleYear,$revision,$combo,$print,$part);
		while($row_ink	=	mysqli_fetch_array($result_ink)){
				$item	=	$row_ink['intItem'];
				$conPC	=	$row_ink['CONSUMPTION'];
				$to_producrd	=	$qty;
				$Qty	= 	$qty*$conPC;

				$response_d	=save_po_prn_details($orderNo,$orderYear,$item,$Qty);
				if(!$response_d['res']){
					$response['type'] 		= 'fail';
					$response['msg'] 		= $db->errormsg;
					$response['sql'] 		= $response_d['q'];
				}
				else
				$saved_p++;
				$response_d	=save_po_prn_details_so_wise($orderNo,$orderYear,$sales_order,$item,$Qty,$conPC,$to_producrd);
				if(!$response_d['res']){
					$response['type'] 		= 'fail';
					$response['msg'] 		= $db->errormsg;
					$response['sql'] 		= $response_d['q'];
				}
				else
				$saved_p++;

		}
		
	//Non-direct RM-----------------------------------------------------------------
		$result_sp	= non_direct_item_sampleCosumptionForSampleNo_sql($sampleNo,$sampleYear,$revision);
		while($row_sp	=mysqli_fetch_array($result_sp)){
				$item	=	$row_sp['intItem'];
				$conPC	=	$row_sp['CONSUMPTION'];
				$to_producrd	=	$qty;
				$Qty	= 	$qty*$conPC;

				$response_d	=save_po_prn_details($orderNo,$orderYear,$item,$Qty);
				if(!$response_d['res']){
					$response['type'] 		= 'fail';
					$response['msg'] 		= $db->errormsg;
					$response['sql'] 		= $response_d['q'];
				}
				else
				$saved_p++;
				$response_d	=save_po_prn_details_so_wise($orderNo,$orderYear,$sales_order,$item,$Qty,$conPC,$to_producrd);
				if(!$response_d['res']){
					$response['type'] 		= 'fail';
					$response['msg'] 		= $db->errormsg;
					$response['sql'] 		= $response_d['q'];
				}
				else
				$saved_p++;

		}
	//End -Non-direct RM------------------------------------------------------------
	
	}//end of order detail's while loop
	
	//GENERATE PRN NOTE-------------------------------------------------------------
	
	$psd	= getMinimumPSDdate($orderNo,$orderYear);
	 $sql	= "SELECT
				PO_PR.ITEM,
				PO_PR.REQUIRED,
				PO_PR.PRN_QTY,
				PO_PR.PURCHASED_QTY,
				PO_PR.PRN_QTY_USED_OTHERS,
				PO_PR.PRN_QTY_BALANCED_FROM_DUMMY,
				(REQUIRED-((IF(PRN_QTY<0,0,PRN_QTY))-PRN_QTY_USED_OTHERS)-PRN_QTY_BALANCED_FROM_DUMMY) AS bal_to_prn1,
				(REQUIRED-((PURCHASED_QTY)-PRN_QTY_USED_OTHERS)-PRN_QTY_BALANCED_FROM_DUMMY) AS bal_to_prn2,
				(SELECT
				  if(((pdd.PRN_QTY)+pdd.PRN_QTY_BALANCED_FROM_DUMMY-(pdd.REQUIRED+pdd.PRN_QTY_USED_OTHERS))<0,0,((pdd.PRN_QTY)+pdd.PRN_QTY_BALANCED_FROM_DUMMY-(pdd.REQUIRED+pdd.PRN_QTY_USED_OTHERS))) usabal_from_dummy 
				FROM
				trn_po_prn_details as pdd
				INNER JOIN trn_orderheader ON pdd.ORDER_NO = trn_orderheader.DUMMY_PO_NO 
				AND pdd.ORDER_YEAR = trn_orderheader.DUMMY_PO_YEAR
				INNER JOIN trn_po_prn_details AS pd ON trn_orderheader.intOrderNo = pd.ORDER_NO 
				AND trn_orderheader.intOrderYear = pd.ORDER_YEAR AND pdd.ITEM = pd.ITEM
				WHERE
				pd.ORDER_NO = PO_PR.ORDER_NO AND
				pd.ORDER_YEAR = PO_PR.ORDER_YEAR AND
				pd.ITEM = PO_PR.ITEM ) usabal_from_dummy1,
				(SELECT
				  if(((pdd.PURCHASED_QTY)+pdd.PRN_QTY_BALANCED_FROM_DUMMY-(pdd.REQUIRED+pdd.PRN_QTY_USED_OTHERS))<0,0,((pdd.PURCHASED_QTY)+pdd.PRN_QTY_BALANCED_FROM_DUMMY-(pdd.REQUIRED+pdd.PRN_QTY_USED_OTHERS))) usabal_from_dummy 
				FROM
				trn_po_prn_details as pdd
				INNER JOIN trn_orderheader ON pdd.ORDER_NO = trn_orderheader.DUMMY_PO_NO 
				AND pdd.ORDER_YEAR = trn_orderheader.DUMMY_PO_YEAR
				INNER JOIN trn_po_prn_details AS pd ON trn_orderheader.intOrderNo = pd.ORDER_NO 
				AND trn_orderheader.intOrderYear = pd.ORDER_YEAR AND pdd.ITEM = pd.ITEM
				WHERE
				pd.ORDER_NO = PO_PR.ORDER_NO AND
				pd.ORDER_YEAR = PO_PR.ORDER_YEAR AND
				pd.ITEM = PO_PR.ITEM ) usabal_from_dummy2
 FROM
				trn_po_prn_details AS PO_PR
				WHERE 
				PO_PR.ORDER_NO = '$orderNo' AND
				PO_PR.ORDER_YEAR = '$orderYear'  
				GROUP BY
				PO_PR.ITEM
				";
	$result = $db->RunQuery2($sql);
	while($row=mysqli_fetch_array($result)){
		$item					=	$row['ITEM'];
		$bulkRequired			=	($row['REQUIRED']==''?0:$row['REQUIRED']);
		$usabal_from_dummy		=	max($row['usabal_from_dummy1'],$row['usabal_from_dummy2']);	
		$prn_used_from_dummy	=	(($usabal_from_dummy=='' || $usabal_from_dummy) <0 ?0:$usabal_from_dummy);
		$prn_used_from_dummy	= 	min($bulkRequired,$prn_used_from_dummy);
		$prn_used_from_dummy	=	round($prn_used_from_dummy,8);
		$bal_to_prn				=	min($row['bal_to_prn1'],$row['bal_to_prn2']);
		$prnQty					=	($bal_to_prn-$prn_used_from_dummy);
		$prnQty					=	($prnQty==''?0:$prnQty);
		$prnQty					=	($prnQty < 0?0:$prnQty);
		$prnQty					=	round($prnQty,8);
		
		$response_u		=update_po_prn_details($orderNo,$orderYear,$item,$bulkRequired,$prnQty,$prn_used_from_dummy);	
		if(!$response_u['res']){
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['sql'] 		= $response_u['q'];
		}
	
		if($prnQty > 0){
			$response_p		=insertPRNDetail($orderNo,$orderYear,$prn_no,$year,'NULL',$sales_order_no,$item,$bulkRequired,$prnQty,$prn_used_from_dummy,0,$psd);	
			if(!$response_p['res']){
				$response['type'] 		= 'fail';
				$response['msg'] 		= $db->errormsg;
				$response['sql'] 			= $response_p['q'];
			}
		}
		if($response_p['res'])
		 $saved++;
		 $saved_prn_qty	+= $prnQty;;
	}
	//----------------------------------------------------------------
	
	
	if($saved_p==0 && $non_rm_flag==0){
			$response['type'] 		= 'fail';
			$response['msg'] 		= "No RM to save";
			$response['sql'] 		= $response['sql'];
			$response['qty'] 		=0;
	}
/*	else if($saved==0){
			$response['type'] 		= 'fail';
			$response['msg'] 		= "No RM to save";
			$response['sql'] 		= '';
			$response['qty'] 		=0;
	}*/
	else{
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Saved successfully";
			$response['sql'] 		= '';
			$response['qty'] 		= $saved_prn_qty;
	}
	
	
 	
	return $response;
	
}




function ink_item_sampleCosumptionForSampleNo_sql($sampleNo,$sampleYear,$revision,$combo,$print,$part){
		global $db;
		$sql	= " select  
					intItem,
					ROUND(SUM(dblColorWeight/sumWeight*dblWeight /dblNoOfPcs),9) as CONSUMPTION 
					 from (  SELECT 
					 SCR.intItem,
					SCR.dblWeight,
					trn_sampleinfomations_details_technical.dblColorWeight ,
					mst_units.dblNoOfPcs ,
					
					 (SELECT Sum(trn_sample_color_recipes.dblWeight) AS sumWeight 
					FROM trn_sample_color_recipes 
					WHERE trn_sample_color_recipes.intSampleNo = SCR.intSampleNo 
					AND trn_sample_color_recipes.intSampleYear = SCR.intSampleYear 
					AND trn_sample_color_recipes.intRevisionNo = SCR.intRevisionNo 
					AND trn_sample_color_recipes.strCombo = SCR.strCombo 
					AND trn_sample_color_recipes.strPrintName = SCR.strPrintName 
					AND trn_sample_color_recipes.intTechniqueId = SCR.intTechniqueId 
					AND trn_sample_color_recipes.intInkTypeId = SCR.intInkTypeId 
					AND trn_sample_color_recipes.intColorId = SCR.intColorId 
					) as sumWeight 
					FROM  trn_sample_color_recipes  as SCR 
					INNER JOIN trn_sampleinfomations_details AS SD ON SCR.intSampleNo = SD.intSampleNo AND SCR.intSampleYear = SD.intSampleYear AND SCR.intRevisionNo = SD.intRevNo AND SCR.strCombo = SD.strComboName AND SCR.strPrintName = SD.strPrintName AND SCR.intColorId = SD.intColorId AND SCR.intTechniqueId = SD.intTechniqueId
					Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = SCR.intSampleNo 
					AND trn_sampleinfomations_details_technical.intSampleYear = SCR.intSampleYear 
					AND trn_sampleinfomations_details_technical.intRevNo = SCR.intRevisionNo 
					AND trn_sampleinfomations_details_technical.strComboName = SCR.strCombo 
					AND trn_sampleinfomations_details_technical.strPrintName = SCR.strPrintName 
					AND trn_sampleinfomations_details_technical.intColorId = SCR.intColorId 
					AND trn_sampleinfomations_details_technical.intInkTypeId = SCR.intInkTypeId 
					Inner Join mst_item ON mst_item.intId = SCR.intItem 
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
					Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory 
					Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory 
					WHERE SCR.intSampleNo='$sampleNo' 
					AND SCR.intSampleYear='$sampleYear' 
					AND SCR.intRevisionNo='$revision' 
					AND SCR.strCombo='$combo' 
					AND SCR.strPrintName='$print' 
					group by 
SCR.intTechniqueId 
					,SCR.intInkTypeId 
					, SCR.intColorId 
,SCR.intItem
					 ) as t 
group by intItem

		";
		$result = $db->RunQuery2($sql);
		return $result;
		
	}

function foil_item_sampleCosumptionForSampleNo_sql($sampleNo,$sampleYear,$revision,$combo,$print,$part){
	
	global $db;
		$sql	="
					SELECT 
					mst_item.intId as intItem,
					sum(trn_sample_foil_consumption.dblMeters) as CONSUMPTION
					FROM
					trn_sample_foil_consumption  
					Inner Join mst_item ON mst_item.intId = trn_sample_foil_consumption.intItem
					Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
					Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
					Inner Join mst_units ON mst_units.intId = mst_item.intUOM
					Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_item.intCurrency
					WHERE
					trn_sample_foil_consumption.intSampleNo = '$sampleNo' AND
					trn_sample_foil_consumption.intSampleYear = '$sampleYear' AND
					trn_sample_foil_consumption.intRevisionNo = '$revision' AND
					trn_sample_foil_consumption.strCombo = '$combo' AND
					trn_sample_foil_consumption.strPrintName = '$print' AND 
					trn_sample_foil_consumption.dblMeters > 0
					GROUP BY
					mst_maincategory.intId,
					mst_subcategory.intId,
					mst_item.intId				";
		
		$result = $db->RunQuery2($sql);
		return $result;
		
}

function update_unwanted_rm_qty_to_zero($orderNo,$orderYear){
	
	global $db;
	
	$sql	="SELECT
			trn_po_prn_details_sales_order.ORDER_NO,
			trn_po_prn_details_sales_order.ORDER_YEAR,
			trn_po_prn_details_sales_order.SALES_ORDER,
			trn_po_prn_details_sales_order.ITEM,
			trn_orderdetails.intSampleNo,
			trn_orderdetails.intSampleYear,
			trn_orderdetails.intRevisionNo,
			trn_orderdetails.strCombo,
			trn_orderdetails.strPrintName,
			trn_orderdetails.intPart
			FROM
			trn_po_prn_details_sales_order
			INNER JOIN trn_orderdetails ON trn_po_prn_details_sales_order.ORDER_NO = trn_orderdetails.intOrderNo AND trn_po_prn_details_sales_order.ORDER_YEAR = trn_orderdetails.intOrderYear AND trn_po_prn_details_sales_order.SALES_ORDER = trn_orderdetails.intSalesOrderId

			WHERE
			trn_po_prn_details_sales_order.ORDER_NO = '$orderNo' AND
			trn_po_prn_details_sales_order.ORDER_YEAR = '$orderYear'    
			";
	$result = $db->RunQuery2($sql);
	while($row	=mysqli_fetch_array($result)){
		$intSampleNo			= $row['intSampleNo'];
		$intSampleYear			= $row['intSampleYear'];
		$intRevisionNo			= $row['intRevisionNo'];
		$strCombo				= $row['strCombo'];
		$strPrintName			= $row['strPrintName'];
		$intPart				= $row['intPart'];
		$item					= $row['ITEM'];
		$available	= check_availablity($intSampleNo,$intSampleYear,$intRevisionNo,$strCombo,$strPrintName,$intPart,$item);	
		if(!$available){
			$sql	="UPDATE `trn_po_prn_details_sales_order` SET `REQUIRED`='0' WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item')";
		$result = $db->RunQuery2($sql);
		}
	}
		
}

function special_item_sampleCosumptionForSampleNo_sql($sampleNo,$sampleYear,$revision,$combo,$print,$part){
	global $db;
		
		$sql	= 
				"
				SELECT
				mst_item.intId as intItem,
				sum(trn_sample_spitem_consumption.dblQty/dblNoOfPcs) as CONSUMPTION
				FROM
				trn_sample_spitem_consumption 
				Inner Join mst_item ON mst_item.intId = trn_sample_spitem_consumption.intItem
				Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
				Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
				Inner Join mst_units ON mst_units.intId = mst_item.intUOM
				Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_item.intCurrency
				WHERE
				trn_sample_spitem_consumption.intSampleNo = '$sampleNo' AND
				trn_sample_spitem_consumption.intSampleYear = '$sampleYear' AND
				trn_sample_spitem_consumption.intRevisionNo = '$revision' AND
				trn_sample_spitem_consumption.strCombo = '$combo' AND
				trn_sample_spitem_consumption.strPrintName = '$print' AND 
				trn_sample_spitem_consumption.dblQty > 0 
				GROUP BY
				mst_maincategory.intId,
				mst_subcategory.intId,
				mst_item.intId";
		
		$result = $db->RunQuery2($sql);
		return $result;
	 
	}	
	
function non_direct_item_sampleCosumptionForSampleNo_sql($sampleNo,$sampleYear,$revision){
	
	global $db;
		$sql	="
					SELECT 
					trn_sample_non_direct_rm_consumption.ITEM as intItem,
					sum(trn_sample_non_direct_rm_consumption.CONSUMPTION) as CONSUMPTION
					FROM
					trn_sample_non_direct_rm_consumption  
 					WHERE
					trn_sample_non_direct_rm_consumption.SAMPLE_NO = '$sampleNo' AND
					trn_sample_non_direct_rm_consumption.SAMPLE_YEAR = '$sampleYear' AND
					trn_sample_non_direct_rm_consumption.REVISION_NO = '$revision' 
					group by 
					trn_sample_non_direct_rm_consumption.ITEM ";
		
		$result = $db->RunQuery2($sql);
		return $result;
		
}

	
function insertPRNDetail($orderNo,$orderYear,$prn_no,$year,$sales_order_id,$sales_order_no,$item,$bulkRequired,$prnQty,$prn_used_from_dummy,$poQty,$psd){
	
	global $db;
	
 	  $sql	="INSERT INTO trn_prndetails (
				intPrnNo,
				intYear,
				intItem,
				intOrderNo,
				intOrderYear,
				SALES_ORDER_ID,
				strSalesOrderNo,
				BULK_REQUIRED,
				QTY_FROM_DUMMY,
				dblPrnQty,
				dblPoQty,
				REQUIRED_DATE
				)
				VALUES
				(
				'$prn_no',
				'$year',
				'$item',
				'$orderNo',
				'$orderYear',
				$sales_order_id,
				'$sales_order_no',
				'$bulkRequired',
				'$prn_used_from_dummy',
				'$prnQty',
				'$poQty',
				'$psd'
				)";
 	$result = $db->RunQuery2($sql);
	$response['res'] 		= $result;
	$response['msg'] 		= $db->errormsg;
	$response['q'] 			= $sql;
 	return $response;
}

function  getApprovedPurchasedQty($orderNo,$orderYear,$item){
	
	global $db;
	
	$sql	="SELECT
				sum(trn_podetails.dblQty) as poQty
				FROM
				trn_podetails
				INNER JOIN trn_poheader ON trn_podetails.intPONo = trn_poheader.intPONo AND trn_podetails.intPOYear = trn_poheader.intPOYear
				INNER JOIN trn_prndetails ON trn_podetails.intPrnNo = trn_prndetails.intPrnNo AND trn_podetails.intPrnYear = trn_prndetails.intYear AND trn_podetails.intItem = trn_prndetails.intItem
				WHERE
				trn_poheader.intStatus = 1 AND
				trn_prndetails.intOrderNo = '$orderNo' AND
				trn_prndetails.intOrderYear = '$orderYear' AND
				trn_podetails.intItem = '$item'";

		$result		=	$db->RunQuery2($sql);
		$row		=	mysqli_fetch_array($result);
		$qty		=	$row['poQty'];	
		
		return $qty;
}

function generatePRNemail($prn_no,$year,$orderNo,$orderYear,$prn_saved_qty){
	//ini_set('display_errors',1);
	global $objMail;
	global $companyId;
	global $db;
	global $obj_commom;
	$content2	='';
	
	$prn_no_generated	=$prn_no;
	$prn_year_generated	=$year;

	 $sql = "SELECT
			trn_orderheader.DUMMY_PO_NO ,
			trn_orderheader.DUMMY_PO_YEAR ,
			trn_orderheader.intOrderNo,
			trn_orderheader.intOrderYear
			FROM
			trn_orderheader  
			WHERE
			trn_orderheader.intOrderNo = '$orderNo' AND
			trn_orderheader.intOrderYear = '$orderYear'
			";
	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
	$o_n	= $row['intOrderNo'].'/'.$row['intOrderYear'];
	if($row['DUMMY_PO_YEAR'] > 0)
	$d_n	= $row['DUMMY_PO_NO']."/".$row['DUMMY_PO_YEAR'];
	else 
	$d_n	= '';


	 $sql = "SELECT
			GROUP_CONCAT(distinct trn_orderdetails.strGraphicNo) as str_strGraphicNo ,
			trn_orderheader.PO_TYPE
			FROM
			trn_orderdetails
			INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
			WHERE
			trn_orderdetails.intOrderNo = '$orderNo' AND
			trn_orderdetails.intOrderYear = '$orderYear'
			";
	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
	$graphic	= $row['str_strGraphicNo'];
	$flag_dummy	= $row['PO_TYPE'];
	if($flag_dummy==1)
		$str_pre_costing	=" - Pre Costing of ERP no ".$o_n;
	else if($flag_dummy==2)
		$str_pre_costing	=" - Split Costing of Pre Costing ERP no ".$d_n;
	else
		$str_pre_costing	=" - Costing of ERP no ".$o_n;
		
		/*
		$link=$_SESSION['MAIN_URL']."?q=917&prnNo=$prn_no&year=$year";
		$statement	="Click here to view system generated PRN for ".$str_pre_costing." <a  href= ".$link.">".$prn_no."/".$year." Graphics(".$graphic." )</a>";*/
	//////////////////////////////APPROVED/////////////////////////////	
		$sql	="SELECT
				trn_prnheader.intPrnNo as no,
				trn_prnheader.intYear as year ,
				trn_prnheader.BULK_PO_QTY,
				trn_prnheader.BULK_PO_QTY_RCV_ABLE 
				FROM `trn_prnheader`
				WHERE
				trn_prnheader.intOrderNo = '$orderNo' AND
				trn_prnheader.intOrderYear = '$orderYear' AND
				trn_prnheader.intStatus = 1
				GROUP BY
				trn_prnheader.intPrnNo,
				trn_prnheader.intYear
				";
		$result = $db->RunQuery2($sql);
		$statement	='';
		$content1	='';
		$i	=0;
		while($row=mysqli_fetch_array($result)){
			$i++;
			$year				= $row['year'];
			$prn_no				= $row['no'];
			$bulk_qty			= $row['BULK_PO_QTY'];
			$bulk_po_rcv_able	= $row['BULK_PO_QTY_RCV_ABLE'];
			$link				=$_SESSION['MAIN_URL']."?q=917&prnNo=$prn_no_generated&year=$prn_year_generated";
 			$content1	.="<tr><td>&nbsp;</td><td class='normalfnt'><a  href= ".$link.">$prn_no_generated/$prn_year_generated</a></td>	<td>&nbsp;</td></tr>";
 			$content1	.="<tr><td>&nbsp;</td><td class='normalfnt'>Customer Order Qty/Customer Order Receivable Qty</td><td>".$bulk_qty."/".$bulk_po_rcv_able."</td></tr>";
		}
		if($i>0){
			$statement	.= "<tr><td>&nbsp;</td><td class='normalfnt'><b>Click Following links to view Approved PRN</b></td>	<td>&nbsp;</td></tr>".$content1."<tr><td>&nbsp;</td><td class='normalfnt'></td>	<td>&nbsp;</td></tr>";
		}
		
	///////////////////////////////CANCELLED///////////////////////
		$sql	="SELECT
				trn_prnheader.intPrnNo as no,
				trn_prnheader.intYear as year,
				trn_prnheader.BULK_PO_QTY,
				trn_prnheader.BULK_PO_QTY_RCV_ABLE 
				FROM `trn_prnheader`
				WHERE
				trn_prnheader.SYSTEM_CANCELLATION = 1 AND
				trn_prnheader.intOrderNo = '$orderNo' AND
				trn_prnheader.intOrderYear = '$orderYear' AND
				trn_prnheader.intStatus = -10
				GROUP BY
				trn_prnheader.intPrnNo,
				trn_prnheader.intYear
				";
		$result = $db->RunQuery2($sql);
		$content_c	='';
		$content_c1	='';
		$i	=0;
		while($row=mysqli_fetch_array($result)){
			$i++;
			$year				= $row['year'];
			$prn_no				= $row['no'];
			$bulk_qty			= $row['BULK_PO_QTY'];
			$bulk_po_rcv_able	= $row['BULK_PO_QTY_RCV_ABLE'];
			$link				=$_SESSION['MAIN_URL']."?q=917&prnNo=$prn_no&year=$year";
 			$content_c1	.="<tr><td>&nbsp;</td><td class='normalfnt'><a  href= ".$link.">$prn_no/$year</a></td>	<td>&nbsp;</td></tr>";
 			$content_c1	.="<tr><td>&nbsp;</td><td class='normalfnt'>Customer Order Qty/Customer Order Receivable Qty</td><td>".$bulk_qty."/".$bulk_po_rcv_able."</td></tr>";
		}
		if($i>0){
			$content_c	.= "<tr><td>&nbsp;</td><td class='normalfnt'><b>Click Following links to view auto Cancelled PRN</b></td>	<td>&nbsp;</td></tr>".$content_c1."<tr><td>&nbsp;</td><td class='normalfnt'></td>	<td>&nbsp;</td></tr>";
		}
	
		if($statement != ''){//approved
			$content	="<tr><td>&nbsp;</td>
			<td colspan='4' class='normalfnt'>$statement</td>
			</tr>";
		}
		if($content_c !=''){//cancelled
			$content	.="<tr><td>&nbsp;</td>
			<td colspan='4' class='normalfnt'>$content_c</td>
			</tr>";
		}
 	
	$sql	="SELECT
			trn_po_prn_details.ITEM,
			trn_po_prn_details.REQUIRED,
			trn_po_prn_details.PRN_QTY_USED_OTHERS,
			trn_po_prn_details.PURCHASED_QTY,
			(IFNULL(trn_po_prn_details.REQUIRED,0) + IFNULL(PRN_QTY_USED_OTHERS,0)) AS NEW_PRN_QTY,
			mst_item.strName,
			group_concat(trn_podetails.intPONo,'/',trn_podetails.intPOYear) as sup_po,
			group_concat(trn_prndetails.intPrnNo,'/',trn_prndetails.intYear) as prn
			FROM
			trn_po_prn_details
			INNER JOIN mst_item ON trn_po_prn_details.ITEM = mst_item.intId
			INNER JOIN trn_prndetails ON trn_po_prn_details.ORDER_NO = trn_prndetails.intOrderNo AND trn_po_prn_details.ORDER_YEAR = trn_prndetails.intOrderYear AND trn_po_prn_details.ITEM = trn_prndetails.intItem
			INNER JOIN trn_podetails ON trn_prndetails.intPrnNo = trn_podetails.intPrnNo AND trn_prndetails.intYear = trn_podetails.intPrnYear AND trn_prndetails.intItem = trn_podetails.intItem
			INNER JOIN trn_poheader ON trn_podetails.intPONo = trn_poheader.intPONo AND trn_podetails.intPOYear = trn_poheader.intPOYear
			WHERE
			trn_po_prn_details.ORDER_NO = '$orderNo' AND
			trn_po_prn_details.ORDER_YEAR = '$orderYear' AND 
			(IFNULL(trn_po_prn_details.REQUIRED,0) +IFNULL(PRN_QTY_USED_OTHERS,0)) < IFNULL(trn_po_prn_details.PURCHASED_QTY,0) AND
trn_poheader.intStatus = 1 
group by mst_item.strName";
	$result = $db->RunQuery2($sql);
		$content2	.="<tr>
		<td class='normalfnt' colspan='6'><b>PRN qtys of following items have been reduced.If it is possible, please revise the supplier po for new PRN quantities</b></td>
		</tr>
		<tr><td colspan='6'><table  width='595' border='0' cellspacing='0' cellpadding='2' class='tblBorder'>
		<td>Supplier PO</td>
		<td>PRN</td>
		<td>Item</td>
		<td>Already PO</td>
		<td>Required PRN Qty</td></tr>";
		$i=0;
	while($row=mysqli_fetch_array($result)){
		$i++;
		$sup_po_no	= $row['sup_po'];
		$prn_no		= $row['prn'];
		$item		= $row['strName'];
		$new_prn	= $row['NEW_PRN_QTY'];
		$old_po		= $row['PURCHASED_QTY'];
		$content2	.="
		<tr><td class='normalfnt'>".$sup_po_no."</td>
		<td class='normalfnt'>".$prn_no."</td>
		<td class='normalfnt'>$item</td>
		<td class='normalfnt'>$old_po</td>
		<td class='normalfnt'>$new_prn</td>
		</tr>";
	}
	$content2	.='</table></td></tr>';
	if($i==0)
		$content2	='';

		$nowDate 		= date('Y-m-d');
		$mailHeader		= "SYSTEM GENERATED PRN (".$prn_no."/".$year.") ".$str_pre_costing." - Graphics(".$graphic." )";
		$FROM_NAME		= "NSOFT";
		$FROM_EMAIL		= '';
		
		$sql	="SELECT
				sys_mail_eventusers.intUserId ,
				sys_users.strUserName 
				FROM
				sys_mail_eventusers
				INNER JOIN sys_users ON sys_mail_eventusers.intUserId = sys_users.intUserId
				WHERE
				sys_mail_eventusers.intMailEventId = 1031 AND
				sys_mail_eventusers.intCompanyId = '$companyId' AND
				sys_users.intStatus = 1
				GROUP BY
				sys_mail_eventusers.intUserId";
	$result = $db->RunQuery2($sql);
	while($row=mysqli_fetch_array($result)){

		$receiver_name	=$row['strUserName'];
		$mail_TO		= $obj_commom->getEmailList2($row['intUserId']);
		
		ob_start();
			include "rptBulkOrder-db-prn_mail_template.php";
 		$body		= ob_get_clean();
		
		//$objMail->insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
		insertTable2($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,'','');
		
	}
	
}


function cancelPRNandPOs($orderNo,$orderYear,$action){
	
	
	global $db;
	
	 $sql = "SELECT
			trn_orderheader.DUMMY_PO_NO ,
			trn_orderheader.DUMMY_PO_YEAR ,
			trn_orderheader.intOrderNo,
			trn_orderheader.intOrderYear
			FROM
			trn_orderheader  
			WHERE
			trn_orderheader.intOrderNo = '$orderNo' AND
			trn_orderheader.intOrderYear = '$orderYear'
			";
	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
	$o_n	= $row['intOrderNo'].'/'.$row['intOrderYear'];
	if($row['DUMMY_PO_YEAR'] > 0)
	$d_n	= $row['DUMMY_PO_NO']."/".$row['DUMMY_PO_YEAR'];
	else 
	$d_n	= '';


	 $sql = "SELECT
			GROUP_CONCAT(distinct trn_orderdetails.strGraphicNo) as str_strGraphicNo ,
			trn_orderheader.PO_TYPE
			FROM
			trn_orderdetails
			INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
			WHERE
			trn_orderdetails.intOrderNo = '$orderNo' AND
			trn_orderdetails.intOrderYear = '$orderYear'
			";
	$result 	= $db->RunQuery2($sql);
	$row		=mysqli_fetch_array($result);
	$graphic	= $row['str_strGraphicNo'];
	$flag_dummy	= $row['PO_TYPE'];
	
	if($flag_dummy==1)
		$str_pre_costing	=" - Pre Costing of ERP no ".$o_n;
	else if($flag_dummy==2)
		$str_pre_costing	=" - Split Costing of Pre Costing ERP no ".$d_n;
	else
		$str_pre_costing	=" - Costing of ERP no ".$o_n;
	
		$sql_p	=	"select 
				trn_po_prn_details.ORDER_NO,
				trn_po_prn_details.ORDER_YEAR,
				trn_po_prn_details.ITEM,
				trn_po_prn_details.REQUIRED,
				trn_po_prn_details.PRN_QTY,
				trn_po_prn_details.PURCHASED_QTY,
				trn_po_prn_details.PRN_QTY_USED_OTHERS,
				trn_po_prn_details.PRN_QTY_BALANCED_FROM_DUMMY from `trn_po_prn_details` WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear')";
		$result_p = 	$db->RunQuery2($sql_p);
		while($row_p=	mysqli_fetch_array($result_p)){
			
			$orderNo		=$row_p['ORDER_NO'];
			$orderYear		=$row_p['ORDER_YEAR'];
			$item			=$row_p['ITEM'];
			
	
			$sql_s	="select PRN_QTY_BALANCED_FROM_DUMMY from `trn_po_prn_details` WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`ITEM`='$item')";
			$result_s = $db->RunQuery2($sql_s);
			$rows=mysqli_fetch_array($result_s);
			$used_dummy	= $rows['PRN_QTY_BALANCED_FROM_DUMMY'];
			
			$sql_c	="UPDATE `trn_po_prn_details` SET `REQUIRED`=0 , PRN_QTY_BALANCED_FROM_DUMMY =0 WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`ITEM`='$item')";
			$result_c = $db->RunQuery2($sql_c);
			
			$sql_c_s	="UPDATE `trn_po_prn_details_sales_order` SET `REQUIRED`='0' WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear')  AND (`ITEM`='$item')";
			$result_c_s = $db->RunQuery2($sql_c_s);
		
		
		
			$sql_d	="update 
					trn_po_prn_details
					INNER JOIN trn_orderheader ON trn_po_prn_details.ORDER_NO = trn_orderheader.DUMMY_PO_NO 
					AND trn_po_prn_details.ORDER_YEAR = trn_orderheader.DUMMY_PO_YEAR
					INNER JOIN trn_po_prn_details AS pd ON trn_orderheader.intOrderNo = pd.ORDER_NO 
					AND trn_orderheader.intOrderYear = pd.ORDER_YEAR AND trn_po_prn_details.ITEM = pd.ITEM
					set trn_po_prn_details.PRN_QTY_USED_OTHERS = IFNULL(trn_po_prn_details.PRN_QTY_USED_OTHERS,0)-'$used_dummy'
					WHERE
					pd.ORDER_NO = '$orderNo' AND
					pd.ORDER_YEAR = '$orderYear' 
					 AND (pd.`ITEM`='$item') ";
		
			$result_d = $db->RunQuery2($sql_d);
			
		}
		
		//----------------------------------------------------------------------
		/* $sql = "select 
		 trn_prndetails.intOrderNo,
		 trn_prndetails.intOrderYear,
		 trn_prndetails.intItem ,
		 trn_prndetails.dblPrnQty
		 from 
		 trn_prnheader
left JOIN trn_prndetails ON trn_prnheader.intPrnNo = trn_prndetails.intPrnNo AND trn_prnheader.intYear = trn_prndetails.intYear 
left JOIN trn_podetails ON trn_podetails.intPrnNo = trn_prndetails.intPrnNo AND trn_podetails.intPrnYear = trn_prndetails.intYear 
left JOIN trn_poheader ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear 
WHERE (trn_poheader.intStatus >1 or trn_poheader.intStatus =0 or trn_poheader.intStatus is null ) and trn_prnheader.`intStatus`<>1 and (trn_prnheader.`intOrderNo`='$orderNo') AND (trn_prnheader.`intOrderYear`='$orderYear') ";*/

		//-----------------------------------------------------------------------
	/*	 $sql = "select 
		 trn_prndetails.intOrderNo,
		 trn_prndetails.intOrderYear,
		 trn_prndetails.intItem ,
		 trn_prndetails.dblPrnQty
		 from 
		 trn_prnheader
left JOIN trn_prndetails ON trn_prnheader.intPrnNo = trn_prndetails.intPrnNo AND trn_prnheader.intYear = trn_prndetails.intYear 
left JOIN trn_podetails ON trn_podetails.intPrnNo = trn_prndetails.intPrnNo AND trn_podetails.intPrnYear = trn_prndetails.intYear 
left JOIN trn_poheader ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear 
WHERE (trn_poheader.intStatus >1 or trn_poheader.intStatus =0 or trn_poheader.intStatus is null ) and trn_prnheader.`intStatus`=1 and (trn_prnheader.`intOrderNo`='$orderNo') AND (trn_prnheader.`intOrderYear`='$orderYear') ";
		$result = $db->RunQuery2($sql);
		while($row=mysqli_fetch_array($result)){
		
			$item	=$row['intItem'];
			$prnQty	=$row['dblPrnQty'];*/

		/*	$sql_s	="select PRN_QTY_BALANCED_FROM_DUMMY from `trn_po_prn_details` WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`ITEM`='$item')";
			$result_s = $db->RunQuery2($sql_s);
			$rows=mysqli_fetch_array($result_s);
			$used_dummy	= $rows['PRN_QTY_BALANCED_FROM_DUMMY'];*/
			
			/*$sql_c	="UPDATE trn_po_prn_details SET  PRN_QTY =PRN_QTY-'$prnQty'  WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`ITEM`='$item')";
			$result_c = $db->RunQuery2($sql_c);*/
			
		/*	$sql_d	="update 
					trn_po_prn_details
					INNER JOIN trn_orderheader ON trn_po_prn_details.ORDER_NO = trn_orderheader.DUMMY_PO_NO 
					AND trn_po_prn_details.ORDER_YEAR = trn_orderheader.DUMMY_PO_YEAR
					INNER JOIN trn_po_prn_details AS pd ON trn_orderheader.intOrderNo = pd.ORDER_NO 
					AND trn_orderheader.intOrderYear = pd.ORDER_YEAR AND trn_po_prn_details.ITEM = pd.ITEM
					set trn_po_prn_details.PRN_QTY_USED_OTHERS = IFNULL(trn_po_prn_details.PRN_QTY_USED_OTHERS,0)-'$used_dummy'
					WHERE
					pd.ORDER_NO = '$orderNo' AND
					pd.ORDER_YEAR = '$orderYear' 
					 AND (pd.`ITEM`='$item') ";
		
			$result_d = $db->RunQuery2($sql_d); */
	//	}



	
		 $sql = "UPDATE trn_prnheader
left JOIN trn_prndetails ON trn_prnheader.intPrnNo = trn_prndetails.intPrnNo AND trn_prnheader.intYear = trn_prndetails.intYear 
left JOIN trn_podetails ON trn_podetails.intPrnNo = trn_prndetails.intPrnNo AND trn_podetails.intPrnYear = trn_prndetails.intYear 
left JOIN trn_poheader ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
 SET trn_prnheader.`intStatus`=-10,trn_prnheader.`SYSTEM_CANCELLATION`=1  WHERE (trn_poheader.intStatus >1 or trn_poheader.intStatus =0 or trn_poheader.intStatus is null ) and (trn_prnheader.`intOrderNo`='$orderNo') AND (trn_prnheader.`intOrderYear`='$orderYear') ";
		$result = $db->RunQuery2($sql);

		$sql = "UPDATE trn_podetails
INNER JOIN trn_poheader ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
INNER JOIN trn_prndetails ON trn_podetails.intPrnNo = trn_prndetails.intPrnNo AND trn_podetails.intPrnYear = trn_prndetails.intYear 
SET trn_podetails.`CLEARED_QTY`=trn_podetails.`dblQty`, trn_podetails.`dblQty`=0 ,trn_podetails.CLEARED_REASON = '$action'  WHERE 
trn_prndetails.intOrderNo = '$orderNo' AND
trn_prndetails.intOrderYear = '$orderYear' and 
(trn_poheader.intStatus >1 or trn_poheader.intStatus =0)";
		$result = $db->RunQuery2($sql);
		
//-----------------------------------------------------------
		$sql	=	"select 
					trn_po_prn_details.ITEM,
					(select sum(trn_prndetails.dblPrnQty)
					from 
					trn_prnheader
					left JOIN trn_prndetails ON trn_prnheader.intPrnNo = trn_prndetails.intPrnNo 
					AND trn_prnheader.intYear = trn_prndetails.intYear 
					where  trn_prnheader.`intStatus`=1 and (trn_prnheader.`intOrderNo`=trn_po_prn_details.ORDER_NO) AND (trn_prnheader.`intOrderYear`=trn_po_prn_details.ORDER_YEAR) AND (trn_po_prn_details.ITEM=trn_prndetails.intItem) ) as prnQty
					from 
				   `trn_po_prn_details` WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear')";
		$result = $db->RunQuery2($sql);
		while($row=mysqli_fetch_array($result)){
		
			$item	=$row['ITEM'];
			$prnQty	=$row['prnQty'];
			if($prnQty=='')
				$prnQty=0;
			
			$sql_c	="UPDATE trn_po_prn_details SET  PRN_QTY ='$prnQty'  WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`ITEM`='$item')";
			$result_c = $db->RunQuery2($sql_c);
		}

//---------------------------------------------------------	
	
	
		generatePRNPOcancelledEmail($orderNo,$orderYear,$action);
}


function generatePRNPOcancelledEmail($orderNo,$orderYear,$action){
	
	global $objMail;
	global $companyId;
	global $db;
	global $obj_commom;
	
	$content	='';
	
	global $db;
	
	 $sql = "SELECT
			trn_orderheader.DUMMY_PO_NO ,
			trn_orderheader.DUMMY_PO_YEAR ,
			trn_orderheader.intOrderNo,
			trn_orderheader.intOrderYear
			FROM
			trn_orderheader  
			WHERE
			trn_orderheader.intOrderNo = '$orderNo' AND
			trn_orderheader.intOrderYear = '$orderYear'
			";
	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
	$o_n	= $row['intOrderNo'].'/'.$row['intOrderYear'];
	if($row['DUMMY_PO_YEAR'] > 0)
	$d_n	= $row['DUMMY_PO_NO']."/".$row['DUMMY_PO_YEAR'];
	else 
	$d_n	= '';

	 $sql = "SELECT
			GROUP_CONCAT(distinct trn_orderdetails.strGraphicNo) as str_strGraphicNo ,
			trn_orderheader.PO_TYPE
			FROM
			trn_orderdetails
			INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
			WHERE
			trn_orderdetails.intOrderNo = '$orderNo' AND
			trn_orderdetails.intOrderYear = '$orderYear'
			";
	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
	$graphic	= $row['str_strGraphicNo'];
	$flag_dummy	= $row['PO_TYPE'];
	if($flag_dummy==1)
		$str_pre_costing	=" - Pre Costing of ERP no ".$o_n;
	else if($flag_dummy==2)
		$str_pre_costing	=" - Split Costing of Pre Costing ERP no ".$d_n;
	else
		$str_pre_costing	=" - Costing of ERP no ".$o_n;
	
	
		//ob_start();
		
		$sql	="SELECT
				trn_prnheader.intPrnNo as no,
				trn_prnheader.intYear as year 
				FROM `trn_prnheader`
				WHERE
				trn_prnheader.SYSTEM_CANCELLATION = 1 AND
				trn_prnheader.intOrderNo = '$orderNo' AND
				trn_prnheader.intOrderYear = '$orderYear' AND
				trn_prnheader.intStatus = -10
				GROUP BY
				trn_prnheader.intPrnNo,
				trn_prnheader.intYear
				";
		$result = $db->RunQuery2($sql);
		$content1	='';
		$i	=0;
		while($row=mysqli_fetch_array($result)){
			$i++;
			$year	= $row['year'];
			$prn_no	= $row['no'];
			$link=$_SESSION['MAIN_URL']."?q=917&prnNo=$prn_no&year=$year";
 			$content1	.="<tr><td>&nbsp;</td><td class='normalfnt'><a  href= ".$link.">$prn_no/$year</a></td>	<td>&nbsp;</td></tr>";
		}
		if($i>0){
			$content	.= "<tr><td>&nbsp;</td><td class='normalfnt'><b>Click Following links to view auto Cancelled PRN</b></td>	<td>&nbsp;</td></tr>".$content1."<tr><td>&nbsp;</td><td class='normalfnt'></td>	<td>&nbsp;</td></tr>";
		}



		$sql	="SELECT
				trn_prnheader.intPrnNo as no,
				trn_prnheader.intYear as year 
				FROM `trn_prnheader`
				WHERE
				trn_prnheader.intOrderNo = '$orderNo' AND
				trn_prnheader.intOrderYear = '$orderYear' AND
				trn_prnheader.intStatus = 1
				GROUP BY
				trn_prnheader.intPrnNo,
				trn_prnheader.intYear
				";
		$result = $db->RunQuery2($sql);
		$content1	='';
		$i	=0;
		while($row=mysqli_fetch_array($result)){
			$i++;
			$year	= $row['year'];
			$prn_no	= $row['no'];
			$link=$_SESSION['MAIN_URL']."?q=917&prnNo=$prn_no&year=$year";
 			$content1	.="<tr><td>&nbsp;</td><td class='normalfnt'><a  href= ".$link.">$prn_no/$year</a></td>	<td>&nbsp;</td></tr>";
		}
		if($i>0){
			$content	.= "<tr><td>&nbsp;</td><td class='normalfnt'><b>Click Following links to view Approved PRN</b></td>	<td>&nbsp;</td></tr>".$content1."<tr><td>&nbsp;</td><td class='normalfnt'></td>	<td>&nbsp;</td></tr>";
		}


		$sql	="SELECT
				trn_podetails.intPONo AS `no`,
				trn_podetails.intPOYear AS `year`,
				mst_item.strName,
				trn_podetails.CLEARED_QTY
				FROM
				trn_poheader
				INNER JOIN trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
				INNER JOIN trn_prnheader ON trn_podetails.intPrnNo = trn_prnheader.intPrnNo AND trn_podetails.intPrnYear = trn_prnheader.intYear
INNER JOIN mst_item ON trn_podetails.intItem = mst_item.intId
				WHERE 
				trn_prnheader.intOrderNo = '$orderNo' AND 
				trn_prnheader.intOrderYear ='$orderYear' AND 
				trn_podetails.CLEARED_QTY > 0
				/*trn_poheader.SYSTEM_CANCELLATION = 1 AND
				trn_poheader.intStatus = -10 */
				/*GROUP BY
				trn_podetails.intPONo,
				trn_podetails.intPOYear */";
		$result = $db->RunQuery2($sql);
		$content1	='';
		$i	=0;
		$temp_order	='';
		while($row=mysqli_fetch_array($result)){
			$i++;
			$year			= $row['year'];
			$order_no		= $row['no'];
			$item			= $row['strName'];
			$cleared_qty	= $row['CLEARED_QTY'];
			$link=$_SESSION['MAIN_URL']."?q=934&poNo=$order_no&year=$year";
			if($order_no.'/'.$year != $temp_order){
 			$content1	.="<tr><td>&nbsp;</td><td class='normalfnt'><a  href= ".$link.">$order_no/$year</a></td>	<td>&nbsp;</td></tr>";
			}
 			$content1	.="<tr><td>&nbsp;</td><td class='normalfnt'>Item ".$item." : Qty = ".$cleared_qty."</td>	<td>&nbsp;</td></tr>";
		$temp_order	=$order_no.'/'.$year;
		}
		if($i>0){
			$content	.= "<tr><td>&nbsp;</td><td class='normalfnt'><b>Click Following links to view auto cleared Supplier PO due to the Customer PO action : ".$action."</b></td>	<td>&nbsp;</td></tr>".$content1."<tr><td>&nbsp;</td><td class='normalfnt'></td>	<td>&nbsp;</td></tr>";
		}



		$sql	="SELECT
				trn_podetails.intPONo as no ,
				trn_podetails.intPOYear as year 
				FROM
				trn_poheader
				INNER JOIN trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
				INNER JOIN trn_prnheader ON trn_podetails.intPrnNo = trn_prnheader.intPrnNo AND trn_podetails.intPrnYear = trn_prnheader.intYear
				WHERE 
				trn_prnheader.intOrderNo = '$orderNo' AND 
				trn_prnheader.intOrderYear ='$orderYear' AND 
				trn_poheader.intStatus = 1
				GROUP BY
				trn_podetails.intPONo,
				trn_podetails.intPOYear";
		$result = $db->RunQuery2($sql);
		$content1	='';
		$i	=0;
		while($row=mysqli_fetch_array($result)){
			$i++;
			$year	= $row['year'];
			$order_no	= $row['no'];
			$link=$_SESSION['MAIN_URL']."?q=934&poNo=$order_no&year=$year";
 			$content1	.="<tr><td>&nbsp;</td><td class='normalfnt'><a  href= ".$link.">$order_no/$year</a></td>	<td>&nbsp;</td></tr>";
		}
		if($i>0){
			$content	.= "<tr><td>&nbsp;</td><td class='normalfnt'><b>If it is possible please revise or cancel following supplier pos since the bulk order is ".$action.". Click Following links to view Approved Supplier PO</b></td>	<td>&nbsp;</td></tr>".$content1."<tr><td>&nbsp;</td><td class='normalfnt'></td>	<td>&nbsp;</td></tr>";
		}
	
	
		//include_once "rptBulkOrder-db-prn_mail_template.php";
		
 		//$body 			= ob_get_clean();
 		//---------
		
		$nowDate 		= date('Y-m-d');
 		
		//$mailHeader		= "SYSTEM CANCELLED PRN AND QTY CLEARED SUPPLIER PO".$str_pre_costing." - Graphics(".$graphic." )";
		$mailHeader		= "SYSTEM CANCELLED AND QTY CLEARED PRN (".$prn_no."/".$year.") ".$str_pre_costing." - Graphics(".$graphic." )";
		
		$FROM_NAME		= "NSOFT";
		$FROM_EMAIL		= '';
		
		$sql	="SELECT
				sys_mail_eventusers.intUserId ,
				sys_users.strUserName 
				FROM
				sys_mail_eventusers
				INNER JOIN sys_users ON sys_mail_eventusers.intUserId = sys_users.intUserId
				WHERE
				sys_mail_eventusers.intMailEventId = 1031 AND
				sys_mail_eventusers.intCompanyId = '$companyId' AND
				sys_users.intStatus = 1
				GROUP BY
				sys_mail_eventusers.intUserId";
	$result = $db->RunQuery2($sql);
	while($row=mysqli_fetch_array($result)){
		
		$receiver_name	=$row['strUserName'];
		$mail_TO		= $obj_commom->getEmailList2($row['intUserId']);
		$FROM_NAME		= 'NSOFT';
		
		ob_start();
			include "rptBulkOrder-db-prn_mail_template.php";
 		$body		= ob_get_clean();
		
		//$objMail->insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
		insertTable2($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,'','');
	}
}

function getMinPSDdate($orderNo,$orderYear){
	
	global $db;
	
	$sql	= "SELECT
				min(trn_orderdetails.dtPSD) as psd 
				FROM `trn_orderdetails`
				WHERE
				trn_orderdetails.intOrderNo 	= '$orderNo' AND
				trn_orderdetails.intOrderYear 	= '$orderYear' limit 1";
	$result			= $db->RunQuery2($sql);
	$row			= mysqli_fetch_array($result);
	$psd	= $row['psd'];
	return $psd;
	
}

function getSpecialTechApprovedSataus($orderNo,$orderYear){

	global $db;
	$cons_fag=1;


	$sql	= "SELECT
				(trn_orderdetails.intSampleNo) as sampleNo ,
				(trn_orderdetails.intSampleYear) as sampleYear, 
				(trn_orderdetails.strCombo) as combo ,
				(trn_orderdetails.strPrintName) as print ,
				(trn_orderdetails.intRevisionNo) as revNo 
				FROM `trn_orderdetails`
				WHERE
				trn_orderdetails.intOrderNo 	= '$orderNo' AND
				trn_orderdetails.intOrderYear 	= '$orderYear' ";
				
	$result		= $db->RunQuery2($sql);
	$cons_fag_temp=1;
	while($row	= mysqli_fetch_array($result)){
				$sampleNo 		   = $row['sampleNo'];
				$sampleYear 	   = $row['sampleYear'];
				$combo 		       = $row['combo'];
				$printName	   	   = $row['print'];
				$revNo 	   		   = $row['revNo'];
				
				$cons_fag_temp	= getApprovedSataus($sampleNo,$sampleYear,$revNo,$combo,$printName);	
				if($cons_fag_temp!=1){
					$cons_fag	=0;
				}
					
	}
	
	return $cons_fag;
	
}

function getApprovedSataus($sampleNo,$sampleYear,$revNo,$combo,$printName){
		
		global $db;
		
		$flag_app=1;
 		
		$sqlm = "SELECT DISTINCT
				
				mst_techniques.strName AS TECHNIQUE,
				trn_sampleinfomations_details.intTechniqueId,
				trn_sampleinfomations_details.intItem,
				mst_item.strName as item,
				mst_item.foil_width,
				mst_item.foil_height, 
				trn_sampleinfomations_details.intItem,
				(size_w+0.5) as size_w,
				(size_h+0.5) as size_h,
				trn_sampleinfomations_details.dblQty as qty,
				0 as dblMeters, 
				'' as strCuttingSide ,
				'new' as status,
				
				trn_sampleinfomations_details.strPrintName, 
				trn_sampleinfomations_details.intItem,(size_w+0.5) as size_w,(size_h+0.5) as size_h,
				mst_item.strName  ,'new' as status  
				FROM
				trn_sampleinfomations_details
				Right Join mst_item ON trn_sampleinfomations_details.intItem = mst_item.intId 
				INNER JOIN mst_techniques ON trn_sampleinfomations_details.intTechniqueId=mst_techniques.intId
				WHERE
				trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
				trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
				trn_sampleinfomations_details.strComboName 		=  '$combo' AND 
				trn_sampleinfomations_details.intRevNo =  '$revNo' AND
				trn_sampleinfomations_details.strPrintName =  '$printName' /*AND 
				mst_techniques.intRoll =  '1'  AND CHANGE_CONPC_AFTER_SAMPLE =1*/
				AND mst_item.ROLL_FORM =  '1' AND CHANGE_CONPC_AFTER_SAMPLE =1  AND 
				mst_item.intUOM <> 6

			ORDER BY
				mst_item.strName ASC
			";
			$result_tech = $db->RunQuery2($sqlm);
			while($rowm=mysqli_fetch_array($result_tech))
			{
	
				$sql	="SELECT
							trn_sample_special_technical_header.`STATUS`
							FROM `trn_sample_special_technical_header`
							WHERE
							trn_sample_special_technical_header.SAMPLE_NO = '$sampleNo' AND
							trn_sample_special_technical_header.SAMPLE_YEAR = '$sampleYear' AND
							trn_sample_special_technical_header.REVISION_NO = '$revNo' AND
							trn_sample_special_technical_header.COMBO = '$combo' AND
							trn_sample_special_technical_header.PRINT = '$printName'
							";
					
				$result = $db->RunQuery2($sql);
				$row	=mysqli_fetch_array($result);
				
				if($row['STATUS']!=1)
					$flag_app=0;
			}
			
		$sqlm = "SELECT
										mst_techniques.strName AS TECHNIQUE,
										trn_sampleinfomations_details.intTechniqueId,
										mst_item.strName as item,
										trn_sampleinfomations_details.intItem,
										trn_sampleinfomations_details.dblQty
								FROM
								trn_sampleinfomations_details
								Right Join mst_item ON trn_sampleinfomations_details.intItem = mst_item.intId 
								INNER JOIN mst_techniques ON trn_sampleinfomations_details.intTechniqueId=mst_techniques.intId
								WHERE
								trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
								trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
								trn_sampleinfomations_details.strComboName 		=  '$combo' AND 
								trn_sampleinfomations_details.intRevNo =  '$revNo' AND
								trn_sampleinfomations_details.strPrintName =  '$printName' /*AND 
								mst_techniques.intRoll <>  '1' AND CHANGE_CONPC_AFTER_SAMPLE =1*/
								AND mst_item.ROLL_FORM =  '1' AND CHANGE_CONPC_AFTER_SAMPLE =1  AND 
								mst_item.intUOM <> 6
			
							ORDER BY
								mst_item.strName ASC
			";
			$result_tech = $db->RunQuery2($sqlm);
			while($rowm=mysqli_fetch_array($result_tech))
			{
	
				$sql	="SELECT
							trn_sample_special_technical_header.`STATUS`
							FROM `trn_sample_special_technical_header`
							WHERE
							trn_sample_special_technical_header.SAMPLE_NO = '$sampleNo' AND
							trn_sample_special_technical_header.SAMPLE_YEAR = '$sampleYear' AND
							trn_sample_special_technical_header.REVISION_NO = '$revNo' AND
							trn_sample_special_technical_header.COMBO = '$combo' AND
							trn_sample_special_technical_header.PRINT = '$printName'
							";
					
				$result = $db->RunQuery2($sql);
				$row	=mysqli_fetch_array($result);
				
				if($row['STATUS']!=1)
					$flag_app=0;
			}
			
			return $flag_app;
			
}

function save_po_prn_details($orderNo,$orderYear,$item,$Qty){

	global $db;
	
	$sql	="SELECT
			trn_po_prn_details.ORDER_NO,
			trn_po_prn_details.ORDER_YEAR,
			trn_po_prn_details.ITEM
			FROM
			trn_po_prn_details
			WHERE
			trn_po_prn_details.ORDER_NO = '$orderNo' AND
			trn_po_prn_details.ORDER_YEAR = '$orderYear' AND
			trn_po_prn_details.ITEM = '$item'
			";
	$result = $db->RunQuery2($sql);
	$row	=mysqli_fetch_array($result);
	
	if($row['ORDER_NO'] > 0){
		$sql	="UPDATE `trn_po_prn_details` SET `REQUIRED`=`REQUIRED`+'$Qty' WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`ITEM`='$item')";
	}
	else{
		$sql	="INSERT INTO `trn_po_prn_details` (`ORDER_NO`, `ORDER_YEAR`, `ITEM`, `REQUIRED`) VALUES ('$orderNo', '$orderYear', '$item', '$Qty')";
	}
	$result = $db->RunQuery2($sql);

	$response['res'] 		= $result;
	$response['msg'] 		= $db->errormsg;
	$response['q'] 			= $sql;
 	return $response;
}

function save_po_prn_details_so_wise($orderNo,$orderYear,$sales_order_id,$item,$Qty,$conPC,$to_producrd){

	global $db;
	
	$sql	="SELECT
			trn_po_prn_details_sales_order.ORDER_NO,
			trn_po_prn_details_sales_order.ORDER_YEAR,
			trn_po_prn_details_sales_order.ITEM
			FROM
			trn_po_prn_details_sales_order
			WHERE
			trn_po_prn_details_sales_order.ORDER_NO = '$orderNo' AND
			trn_po_prn_details_sales_order.ORDER_YEAR = '$orderYear' AND 
			trn_po_prn_details_sales_order.SALES_ORDER = '$sales_order_id' AND 
			trn_po_prn_details_sales_order.ITEM = '$item'
			";
	$result = $db->RunQuery2($sql);
	$row	=mysqli_fetch_array($result);
	
	if($row['ORDER_NO'] > 0){
		$sql	="UPDATE `trn_po_prn_details_sales_order` SET `REQUIRED`='$Qty', `CONS_PC`='$conPC', `PRODUCTION_QTY`='$to_producrd' WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$sales_order_id') AND (`ITEM`='$item')";
	}
	else{
		$sql	="INSERT INTO `trn_po_prn_details_sales_order` (`ORDER_NO`, `ORDER_YEAR`, `SALES_ORDER`, `ITEM`, `REQUIRED`,`CONS_PC`,`PRODUCTION_QTY`) VALUES ('$orderNo', '$orderYear', '$sales_order_id', '$item', '$Qty', '$conPC', '$to_producrd')";
	}
	$result = $db->RunQuery2($sql);
	$response['res'] 		= $result;
	$response['msg'] 		= $db->errormsg;
	$response['q'] 			= $sql;
 	return $response;
}

function getMinimumPSDdate($orderNo,$orderYear){
	
	global $db;
	
	$sql	="SELECT
				Min(trn_orderdetails.dtPSD) AS psd
				FROM `trn_orderdetails`
				WHERE
				trn_orderdetails.intOrderNo = '$orderNo' AND
				trn_orderdetails.intOrderYear = '$orderYear'
				";
	$result = $db->RunQuery2($sql);
	$row	=mysqli_fetch_array($result);
	
	return $row['psd'];
	
}

function update_po_prn_details($orderNo,$orderYear,$item,$bulkRequired,$prnQty,$prn_used_from_dummy){
	
	global $db;
	
	$sql	="UPDATE `trn_po_prn_details` SET 
	`PRN_QTY`=`PRN_QTY`+'$prnQty',
	`PRN_QTY_BALANCED_FROM_DUMMY`=`PRN_QTY_BALANCED_FROM_DUMMY`+'$prn_used_from_dummy'  
	WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`ITEM`='$item')";
	$result = $db->RunQuery2($sql);
	
	if($result){
		$sql_d	="update 
				trn_po_prn_details
				INNER JOIN trn_orderheader ON trn_po_prn_details.ORDER_NO = trn_orderheader.DUMMY_PO_NO 
				AND trn_po_prn_details.ORDER_YEAR = trn_orderheader.DUMMY_PO_YEAR
				INNER JOIN trn_po_prn_details AS pd ON trn_orderheader.intOrderNo = pd.ORDER_NO 
				AND trn_orderheader.intOrderYear = pd.ORDER_YEAR AND trn_po_prn_details.ITEM = pd.ITEM
				set trn_po_prn_details.PRN_QTY_USED_OTHERS = IFNULL(trn_po_prn_details.PRN_QTY_USED_OTHERS,0)+'$prn_used_from_dummy'
				WHERE
				pd.ORDER_NO = '$orderNo' AND
				pd.ORDER_YEAR = '$orderYear' 
				 AND (pd.`ITEM`='$item') ";
	
		$result_d = $db->RunQuery2($sql_d);
	}
	
	$response['res'] 		= $result;
	$response['msg'] 		= $db->errormsg;
	$response['q'] 			= $sql;
 	return $response;
}

function update_bulk_po_qtys($prn_no,$year,$orderQty,$to_producrd){

	global $db;
		$sql	="UPDATE `trn_prnheader` SET `BULK_PO_QTY`= `BULK_PO_QTY`+'$orderQty', `BULK_PO_QTY_RCV_ABLE`= `BULK_PO_QTY_RCV_ABLE`+'$to_producrd' WHERE (`intPrnNo`='$prn_no') AND (`intYear`='$year')";
	$result = $db->RunQuery2($sql);

	$response['res'] 		= $result;
	$response['msg'] 		= $db->errormsg;
	$response['q'] 			= $sql;
 	return $response;
	
	
}
function save_history($orderNo,$orderYear,$bulk_rev){

global $db;
		//save in revision history////////////////
		//------header-----------------------------
		$sql_h		= "insert into trn_orderheader_revision_history (
						REVISION_NO,
						intOrderNo,
						intOrderYear,
						strCustomerPoNo,
						dtDate,
						dtDeliveryDate,
						intCustomer,
						intCustomerLocation,
						intCurrency,
						intPaymentTerm,
						strContactPerson,
						strRemark,
						REJECT_REASON,
						intMarketer,
						intStatus,
						intCreator,
						dtmCreateDate,
						intModifyer,
						dtmModifyDate,
						intApproveLevelStart,
						intLocationId,
						intAllocatedStatus,
						intReviseNo,
						intStatusOld,
						PAYMENT_RECEIVE_COMPLETED_FLAG,
						LC_STATUS,
						TECHNIQUE_TYPE)
						(
						SELECT 
						intReviseNo AS REV,
						intOrderNo,
						intOrderYear,
						strCustomerPoNo,
						dtDate,
						dtDeliveryDate,
						intCustomer,
						intCustomerLocation,
						intCurrency,
						intPaymentTerm,
						strContactPerson,
						strRemark,
						REJECT_REASON,
						intMarketer,
						intStatus,
						intCreator,
						dtmCreateDate,
						intModifyer,
						dtmModifyDate,
						intApproveLevelStart,
						intLocationId,
						intAllocatedStatus,
						intReviseNo,
						intStatusOld,
						PAYMENT_RECEIVE_COMPLETED_FLAG,
						LC_STATUS,
						TECHNIQUE_TYPE 
						FROM 
						trn_orderheader 
						WHERE  
						trn_orderheader.intOrderNo = '$orderNo' AND trn_orderheader.intOrderYear ='$orderYear'
						)";
		$result_h 	= $db->RunQuery2($sql_h);
		//------detail-----------------------------
		$sql_d		= "insert into trn_orderdetails_revision_history (
						REVISION_NO,
						intOrderNo,
						intOrderYear,
						strSalesOrderNo,
						intSalesOrderId,
						strLineNo,
						strStyleNo,
						strGraphicNo,
						intSampleNo,
						intSampleYear,
						strCombo,
						strPrintName,
						intRevisionNo,
						intPart,
						intQty,
						dblPrice,
						dblToleratePercentage,
						dblOverCutPercentage,
						dblDamagePercentage,
						dtPSD,
						dtDeliveryDate,
						intViewInSalesProjection,
						TECHNIQUE_GROUP_ID,
						`STATUS` )
						(
						SELECT 
						$bulk_rev,
						intOrderNo,
						intOrderYear,
						strSalesOrderNo,
						intSalesOrderId,
						strLineNo,
						strStyleNo,
						strGraphicNo,
						intSampleNo,
						intSampleYear,
						strCombo,
						strPrintName,
						intRevisionNo,
						intPart,
						intQty,
						dblPrice,
						dblToleratePercentage,
						dblOverCutPercentage,
						dblDamagePercentage,
						dtPSD,
						dtDeliveryDate,
						intViewInSalesProjection,
						TECHNIQUE_GROUP_ID,
						`STATUS`  
						FROM 
						trn_orderdetails 
						WHERE  
						trn_orderdetails.intOrderNo = '$orderNo' AND trn_orderdetails.intOrderYear ='$orderYear'
						)";
		$result_d 	= $db->RunQuery2($sql_d);
		//------size wise--------------------------
		$sql_s		= "insert into trn_ordersizeqty_revision_history (
						REVISION_NO,
						intOrderNo,
						intOrderYear,
						intSalesOrderId,
						strSize,
						dblQty
						)
						(
						SELECT 
						$bulk_rev,
						intOrderNo,
						intOrderYear,
						intSalesOrderId,
						strSize,
						dblQty
 						FROM 
						trn_ordersizeqty 
						WHERE  
						trn_ordersizeqty.intOrderNo = '$orderNo' AND trn_ordersizeqty.intOrderYear ='$orderYear'
						)";
		$result_s 	= $db->RunQuery2($sql_s);
		/////////////////////////////////////////

	  	$sql = "UPDATE `trn_orderheader` SET `intReviseNo`=`intReviseNo`+1 WHERE (`intOrderNo`='$orderNo') AND (`intOrderYear`='$orderYear')  ";
		$result=$db->RunQuery2($sql);
	
}

function getNonRMTechniquesFlag($orderNo,$orderYear){
	global $db;
	$sql	="SELECT
			trn_orderdetails.intOrderNo,
			trn_orderdetails.intOrderYear
			FROM
			trn_orderdetails
			INNER JOIN trn_sampleinfomations_details ON trn_orderdetails.intSampleNo = trn_sampleinfomations_details.intSampleNo AND trn_orderdetails.intSampleYear = trn_sampleinfomations_details.intSampleYear AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName AND trn_sampleinfomations_details.intRevNo = trn_orderdetails.intRevisionNo
			INNER JOIN mst_techniques ON trn_sampleinfomations_details.intTechniqueId = mst_techniques.intId
			WHERE
			mst_techniques.RM_USING = 1 AND
			trn_orderdetails.intOrderNo = '$orderNo' AND
			trn_orderdetails.intOrderYear = '$orderYear'

				";
	$result = $db->RunQuery2($sql);
	$rows=mysqli_num_rows($result);
	if($rows>0)
		return 0;
	else
		return 1;
}

function check_availablity($sampleNo,$sampleYear,$revision,$combo,$print,$intPart,$item){
	
	$available = false;
	global $db;
	
		$sql	="
					SELECT 
					mst_item.intId as intItem,
					sum(trn_sample_foil_consumption.dblMeters) as CONSUMPTION
					FROM
					trn_sample_foil_consumption  
					Inner Join mst_item ON mst_item.intId = trn_sample_foil_consumption.intItem
					Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
					Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
					Inner Join mst_units ON mst_units.intId = mst_item.intUOM
					Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_item.intCurrency
					WHERE
					trn_sample_foil_consumption.intSampleNo = '$sampleNo' AND
					trn_sample_foil_consumption.intSampleYear = '$sampleYear' AND
					trn_sample_foil_consumption.intRevisionNo = '$revision' AND
					trn_sample_foil_consumption.strCombo = '$combo' AND
					trn_sample_foil_consumption.strPrintName = '$print' AND 
					trn_sample_foil_consumption.intItem ='$item' AND 
					trn_sample_foil_consumption.dblMeters > 0
					GROUP BY
					mst_maincategory.intId,
					mst_subcategory.intId,
					mst_item.intId				";
		
		$result = $db->RunQuery2($sql);
		if($result)
		$available = true;
	

		$sql	="
					SELECT 
					trn_sample_non_direct_rm_consumption.ITEM as intItem,
					sum(trn_sample_non_direct_rm_consumption.CONSUMPTION) as CONSUMPTION
					FROM
					trn_sample_non_direct_rm_consumption  
 					WHERE
					trn_sample_non_direct_rm_consumption.SAMPLE_NO = '$sampleNo' AND
					trn_sample_non_direct_rm_consumption.SAMPLE_YEAR = '$sampleYear' AND
					trn_sample_non_direct_rm_consumption.REVISION_NO = '$revision'  AND
					trn_sample_non_direct_rm_consumption.ITEM = '$item' AND 
					trn_sample_non_direct_rm_consumption.CONSUMPTION > 0
					group by 
					trn_sample_non_direct_rm_consumption.ITEM ";
		
		$result1 = $db->RunQuery2($sql);
		if($result1)
		$available = true;
		
		$sql	= 
				"
				SELECT
				mst_item.intId as intItem,
				sum(trn_sample_spitem_consumption.dblQty/dblNoOfPcs) as CONSUMPTION
				FROM
				trn_sample_spitem_consumption 
				Inner Join mst_item ON mst_item.intId = trn_sample_spitem_consumption.intItem
				Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
				Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
				Inner Join mst_units ON mst_units.intId = mst_item.intUOM
				Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_item.intCurrency
				WHERE
				trn_sample_spitem_consumption.intSampleNo = '$sampleNo' AND
				trn_sample_spitem_consumption.intSampleYear = '$sampleYear' AND
				trn_sample_spitem_consumption.intRevisionNo = '$revision' AND
				trn_sample_spitem_consumption.strCombo = '$combo' AND
				trn_sample_spitem_consumption.strPrintName = '$print'  AND 
				mst_item.intId = '$item' AND 
				trn_sample_spitem_consumption.dblQty > 0
				GROUP BY
				mst_maincategory.intId,
				mst_subcategory.intId,
				mst_item.intId";
		
		$result2 = $db->RunQuery2($sql);
		if($result2)
		$available = true;
	
		$sql	= " select  
					intItem,
					ROUND(SUM(dblColorWeight/sumWeight*dblWeight /dblNoOfPcs),9) as CONSUMPTION 
					 from (  SELECT 
					 SCR.intItem,
					SCR.dblWeight,
					trn_sampleinfomations_details_technical.dblColorWeight ,
					mst_units.dblNoOfPcs ,
					
					 (SELECT Sum(trn_sample_color_recipes.dblWeight) AS sumWeight 
					FROM trn_sample_color_recipes 
					WHERE trn_sample_color_recipes.intSampleNo = SCR.intSampleNo 
					AND trn_sample_color_recipes.intSampleYear = SCR.intSampleYear 
					AND trn_sample_color_recipes.intRevisionNo = SCR.intRevisionNo 
					AND trn_sample_color_recipes.strCombo = SCR.strCombo 
					AND trn_sample_color_recipes.strPrintName = SCR.strPrintName 
					AND trn_sample_color_recipes.intTechniqueId = SCR.intTechniqueId 
					AND trn_sample_color_recipes.intInkTypeId = SCR.intInkTypeId 
					AND trn_sample_color_recipes.intColorId = SCR.intColorId 
					) as sumWeight 
					FROM  trn_sample_color_recipes  as SCR 
					Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = SCR.intSampleNo 
					AND trn_sampleinfomations_details_technical.intSampleYear = SCR.intSampleYear 
					AND trn_sampleinfomations_details_technical.intRevNo = SCR.intRevisionNo 
					AND trn_sampleinfomations_details_technical.strComboName = SCR.strCombo 
					AND trn_sampleinfomations_details_technical.strPrintName = SCR.strPrintName 
					AND trn_sampleinfomations_details_technical.intColorId = SCR.intColorId 
					AND trn_sampleinfomations_details_technical.intInkTypeId = SCR.intInkTypeId 
					Inner Join mst_item ON mst_item.intId = SCR.intItem 
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
					Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory 
					Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory 
					WHERE SCR.intSampleNo='$sampleNo' 
					AND SCR.intSampleYear='$sampleYear' 
					AND SCR.intRevisionNo='$revision' 
					AND SCR.strCombo='$combo' 
					AND SCR.strPrintName='$print' 
					AND SCR.intItem='$item' AND 
					dblColorWeight > 0
					group by 
SCR.intTechniqueId 
					,SCR.intInkTypeId 
					, SCR.intColorId 
,SCR.intItem
					 ) as t 
group by intItem

		";
		$result3 = $db->RunQuery2($sql);
		if($result3)
		$available = true;
		
		return $available;
}

?>







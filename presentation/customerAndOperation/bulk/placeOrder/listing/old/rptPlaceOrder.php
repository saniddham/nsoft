<?php
session_start();
$backwardseperator = "../../../../../";
$companyId = $_SESSION['CompanyID'];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";

$poNo = $_REQUEST['orderNo'];
$year = $_REQUEST['year'];
$approveMode = $_REQUEST['approveMode'];

		$sql = "SELECT
trn_orderheader.intOrderNo,
trn_orderheader.intOrderYear,
trn_orderheader.strCustomerPoNo,
trn_orderheader.dtDate,
mst_customer.strName,
trn_orderheader.intStatus,
trn_orderheader.intCreator,
useraccounts.UserName,
trn_orderheader.dtmCreateDate 
FROM
trn_orderheader
Inner Join mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
Inner Join useraccounts ON trn_orderheader.intCreator = useraccounts.intUserID
WHERE
trn_orderheader.intOrderNo =  '$poNo' AND
trn_orderheader.intOrderYear =  '$year'";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$customer = $row['strName'];
					$customerPO = $row['strCustomerPoNo'];
					$poDate = $row['dtmCreateDate'];
					$delDate = $row['dtDate'];
					$intStatus = $row['intStatus'];
					$poBy = $row['UserName'];
				 }
				 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Purchase Order Report</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>

<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<script type="application/javascript" src="rptPlaceOrder-js.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:229px;
	top:171px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
$placeOrderApproveLevel = (int)getApproveLevel('Place Order');
 $placeOrderApproveLevel=$placeOrderApproveLevel+1;
 if($intStatus==$placeOrderApproveLevel)//pending
{
?>
<div id="apDiv1"><img src="../../../../../images/pending.png"  /></div>
<?php
}
?>
<form id="frmCustomerPurchaseOrderReport" name="frmCustomerPurchaseOrderReport" method="post" action="rptPlaceOrder.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>
<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>CUSTOMER PURCHASE ORDER</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
    <?php
	if($intStatus>1)
	{
		
	?>
    <?php if($approveMode==1) { ?>
    <img src="../../../../../images/approve.png" align="middle" class="noPrint" id="imgApprove" />
    <img src="../../../../../images/reject.png" align="middle" class="noPrint" id="imgReject" />
    <?php
	}
	}
	?>
    </td>
  </tr>
  <tr>
  <?php
	$savedStat = (int)getApproveLevel('Place Order');
 	if($intStatus==1)
	{
	?>
   <td colspan="9" class="APPROVE" style="color:#F00">CONFIRMED</td>
   <?PHP
	}
	else if($intStatus==0)
	{
   ?>
   <td colspan="9" class="APPROVE" style="color:#F00">REJECTED</td>
   <?php
	}
	else
	{
   ?>
   <td width="10%" colspan="9" class="APPROVE">PENDING</td>
   <?php
	}
   ?>
   <?php
   //-----------------
	  $sqlc = "SELECT
			trn_orderheader_approvedby.intApproveUser,
			trn_orderheader_approvedby.dtApprovedDate,
			useraccounts.UserName,
			trn_orderheader_approvedby.intApproveLevelNo
			FROM
			trn_orderheader_approvedby
			Inner Join useraccounts ON trn_orderheader_approvedby.intApproveUser = useraccounts.intUserID
			WHERE
			trn_orderheader_approvedby.intOrderNo =  '$poNo' AND
			trn_orderheader_approvedby.intYear =  '$year' AND
			trn_orderheader_approvedby.intApproveLevelNo =  '$intStatus'
";
	 $resultc = $db->RunQuery($sqlc);
	 $rowc=mysqli_fetch_array($resultc);
	 $rejectBy=$rowc['UserName'];
	 $rejectDate=$rowc['dtApprovedDate'];

   //-----------------
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="9%"><span class="normalfnt"><strong>PO No</strong></span></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="5%"><span class="normalfnt"><?php echo $poNo ?>/<?php echo $year ?></span></td>
    <td width="13%" class="normalfnt"><strong>Delivery Date</strong></td>
    <td width="3%" align="center" valign="middle"><strong>:</strong></td>
    <td width="20%"><span class="normalfnt"><?php echo $delDate ?></span></td>
    <td width="14%"></td>
  <td width="15%"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Customer</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $customer ?></span></td>
    <td><span class="normalfnt"><strong>PO Date</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $poDate ?></span></td>
    <td class="normalfnt"><?php if($intStatus==0){?><strong>Rejected Date</strong><?php } ?></td>
    <td><?php if($intStatus==0){?><strong>:</strong>&nbsp;<span class="normalfnt"><?php echo $rejectDate ?></span><?php } ?></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Customer PO</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $customerPO ?></span></td>
    <td><span class="normalfnt"><strong> PO By</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $poBy ?></span></td>
    <td><span class="normalfnt"><?php if($intStatus==0){?><strong>Rejected By</strong><?php }?></span></td>
    <td><?php if($intStatus==0){?><strong>:</strong>&nbsp;<span class="normalfnt"><?php echo $rejectBy ?></span><?php } ?></td>
  </tr>
  
  
  
  <tr>
    <td colspan="9">&nbsp;</td>
    <td width="1%">&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="1000" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="gridHeader">
              <td width="9%"  height="22">Sales Order No</td>
              <td width="9%" >Style</td>
              <td width="8%" >Sample No</td>
              <td width="8%" >Graphic No</td>
              <td width="5%" >Combo</td>
              <td width="8%" >Placement</td>
              <td width="5%" >Qty</td>
              <td width="5%" >Price</td>
              <td width="6%" >Value</td>
              <td width="10%" >Over Cut %</td>
              <td width="8%" >Damange %</td>
              <td width="7%" >PSD</td>
              <td width="12%" >Delivery Date</td>
              </tr>
              <?php 
	  	 $sql1 = "SELECT
trn_orderdetails.strSalesOrderNo,
trn_orderdetails.strStyleNo,
trn_orderdetails.strGraphicNo,
trn_orderdetails.intSampleNo,
trn_orderdetails.intSampleYear,
trn_orderdetails.strCombo,
mst_part.strName,
trn_orderdetails.intQty,
trn_orderdetails.dblPrice,
trn_orderdetails.dblOverCutPercentage,
trn_orderdetails.dblDamagePercentage,
trn_orderdetails.dtPSD,
trn_orderdetails.dtDeliveryDate
FROM
trn_orderdetails
Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId
WHERE
trn_orderdetails.intOrderNo =  '$poNo' AND
trn_orderdetails.intOrderYear =  '$year'

";
			$result1 = $db->RunQuery($sql1);
			while($row=mysqli_fetch_array($result1))
			{
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" >&nbsp;<?php echo $row['strSalesOrderNo'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strStyleNo']?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['intSampleNo'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strGraphicNo'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['strCombo'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strName'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['intQty']?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblPrice'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['intQty']* $row['dblPrice']; ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblOverCutPercentage'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['dblDamagePercentage'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['dtPSD']?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dtDeliveryDate'] ?>&nbsp;</td>
              </tr>
      <?php        
			}
	  ?>
            
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<?php  
 	if($intStatus!=0)
	{

			  $placeOrderApproveLevel = (int)getApproveLevel('Place Order');
				for($i=$placeOrderApproveLevel; $i>=1; $i--)
				{
					 $sqlc = "SELECT
							trn_orderheader_approvedby.intApproveUser,
							trn_orderheader_approvedby.dtApprovedDate,
							useraccounts.UserName,
							trn_orderheader_approvedby.intApproveLevelNo
							FROM
							trn_orderheader_approvedby
							Inner Join useraccounts ON trn_orderheader_approvedby.intApproveUser = useraccounts.intUserID
							WHERE
							trn_orderheader_approvedby.intOrderNo =  '$poNo' AND
							trn_orderheader_approvedby.intYear =  '$year' AND
							trn_orderheader_approvedby.intApproveLevelNo =  '$i'
";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
						if($placeOrderApproveLevel==$i)
						$desc="1st ";
						else if($placeOrderApproveLevel==$i+1)
						$desc="2nd ";
						else if($placeOrderApproveLevel==$i+2)
						$desc="3rd ";
						else
						$desc=$placeOrderApproveLevel+1-$i."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
				?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Appoved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
            </tr>
<?php
			}
	}
?>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
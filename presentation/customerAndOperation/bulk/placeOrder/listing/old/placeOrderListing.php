<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$intUser  = $_SESSION["userId"];
$thisFilePath =  $_SERVER['PHP_SELF'];
var_dump("this is main test");

//$formName		  	= 'frmItem';
include  	"{$backwardseperator}dataAccess/permisionCheck.inc";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Purchase Order Listing</title>

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="rptPurchaseRequisitionNote-js.js"></script>
<script type="application/javascript" src="grid-js.js"></script>

</head>

<body>
<form id="frmPRNlisting" name="frmPRNlisting" method="get" action="placeOrderListing.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<?php
$stat='';
$stat = $_GET['cboPOStatus'];


$placeOrderApproveLevel = (int)getApproveLevel('Place Order')+1;
?>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">CUSTOMER PURCHASE ORDER
		    <select style="width:100px" name="cboPOStatus" id="cboPOStatus" onchange="submit();">
             <option <?php if($stat==$placeOrderApproveLevel){ ?> selected="selected" <?php } ?>value="<?php echo $placeOrderApproveLevel ?>">Pending</option>
              <option <?php if($stat==1){ ?> selected="selected" <?php } ?>value="1">Confirmed</option>
              <option <?php if($stat==0){ ?> selected="selected" <?php } ?>value="0">Rejected</option>
            <option  <?php if($stat==''){ ?> selected="selected" <?php } ?> value="">All</option>
	        </select>
LISTING</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableBorder_allRound">
          <tr>
            <td width="10%" height="22" class="normalfnt">&nbsp;</td>
            <td width="22%">&nbsp;</td>
            <td width="17%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="11%" class="normalfnt">&nbsp;</td>
            <td width="15%">&nbsp;</td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="grid" >
            <tr class="gridHeader">
              <td width="11%" height="21" >Order No</td>
              <td width="21%" >Customer</td>
              <td width="10%" >Customer PO</td>
              <td width="13%">Delivery Date</td>
              <td width="16%">User</td>
              <?php
			  $placeOrderApproveLevel = (int)getApproveLevel('Place Order');
				for($i=$placeOrderApproveLevel; $i>=1; $i--)
				{
					if($i==$placeOrderApproveLevel)
					$ap="1st Approval";
					else if($i==$placeOrderApproveLevel-1)
					$ap="2nd Approval";
					else if($i==$placeOrderApproveLevel-2)
					$ap="3rd Approval";
					else
					$ap=$i."th Approval";
					
					
					
			  ?>
              <td width="18%"><?php echo $ap ?></td>
              <?php
				}
			  ?>
              <td width="11%"> Report</td>
              </tr>
                 <?php
				 $placeOrderApproveLevel=$placeOrderApproveLevel+1;
				 if($stat=='')//all
				 $sqlCon="";
				 else if($stat==0)//rej
				 $sqlCon="where trn_orderheader.intStatus=0";
				 else if($stat==$placeOrderApproveLevel)//pending
				 $sqlCon="where trn_orderheader.intStatus=$placeOrderApproveLevel";
				 else if($stat==1)//conf
				 $sqlCon="where trn_orderheader.intStatus<$placeOrderApproveLevel and trn_orderheader.intStatus>0";
				 
	 	 		 $sql = "SELECT
							trn_orderheader.intOrderNo,
							trn_orderheader.intOrderYear,
							trn_orderheader.strCustomerPoNo,
							trn_orderheader.dtDate,
							mst_customer.strName,
							trn_orderheader.intStatus,
							trn_orderheader.intCreator,
							sys_users.strUserName
						FROM
							trn_orderheader
							Inner Join mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
							Inner Join sys_users ON trn_orderheader.intCreator = sys_users.intUserId ".$sqlCon;
//echo $sql;
                 var_dump("this is my sql");
                 var_dump($sql);
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					 $orderNo=$row['intOrderNo'];
					 $year=$row['intOrderYear'];
					 $status=$row['intStatus'];
	  			 ?>
            <tr class="normalfnt">
              <?php
				$savedStat = (int)getApproveLevel('Place Order');
				$editMode=0;
				if($row['intStatus']==0)
				$editMode=1;
				else if($savedStat+1==$row['intStatus'])
				$editMode=1;
				//user can edit only saved and rejected prns ...... 
			  ?>
              <td align="center" bgcolor="#FFFFFF"><?php if($editMode==1){ ?><a href="../addNew/placeOrder.php?orderNo=<?php echo $row['intOrderNo']?>&year=<?php echo $row['intOrderYear']?>" target="_blank"> <?php } ?><?php echo $row['intOrderNo']."/".$row['intOrderYear'];?><?php if($editMode==1){ ?></a><?php } ?></td>
              <td bgcolor="#FFFFFF"><?php echo $row['strName']?></td>
              <td bgcolor="#FFFFFF"><?php echo $row['strCustomerPoNo']?></td>
              <td bgcolor="#FFFFFF"><?php echo $row['dtDate']?></td>
              <td bgcolor="#FFFFFF"><?php echo $row['strUserName']?></td>
              <?php
			  $placeOrderApproveLevel = (int)getApproveLevel('Place Order');
			  $tempUser="";
				for($i=$placeOrderApproveLevel; $i>=1; $i--)
				{
					 $sqlc = "SELECT
							trn_orderheader_approvedby.intApproveUser,
							trn_orderheader_approvedby.dtApprovedDate,
							useraccounts.UserName,
							trn_orderheader_approvedby.intApproveLevelNo
							FROM
							trn_orderheader_approvedby
							Inner Join useraccounts ON trn_orderheader_approvedby.intApproveUser = useraccounts.intUserID
							WHERE
							trn_orderheader_approvedby.intOrderNo =  '$orderNo' AND
							trn_orderheader_approvedby.intYear =  '$year' AND
							trn_orderheader_approvedby.intApproveLevelNo =  '$i'
";
					$placeOrderApproveLevel = (int)getApproveLevel('Place Order');
					
					$resultc = $db->RunQuery($sqlc);
					$rowc=mysqli_fetch_array($resultc);

					 $sqlp = "SELECT
								menupermision.int1Approval,
								menupermision.int2Approval,
								menupermision.int3Approval,
								menupermision.int4Approval,
								menupermision.int5Approval
							FROM menupermision
							WHERE
								menupermision.intMenuId =  '41' AND
								menupermision.intUserId =  '$intUser'";	
							
					 $resultp = $db->RunQuery($sqlp);
					 $rowp=mysqli_fetch_array($resultp);
					 $k=$placeOrderApproveLevel+1-$i;
					// echo $j=$k;
						$app=0; 
						//echo $rowp['int'.$k.'Approval'];
					if($status==0)
						$app=0; 
					else if($rowp['int'.$k.'Approval']==1){
					 if($k==1)
						$app=1; 
					 else if($tempUser!='')
						$app=1; 
					}

				?>

              <td bgcolor="#FFFFFF"><?php echo $rowc['UserName']?><?php if($rowc['UserName']==''){ ?><?php if($app==1){?><a href="rptPlaceOrder.php?orderNo=<?php echo $row['intOrderNo']?>&year=<?php echo $row['intOrderYear']?>&approveMode=1" target="_blank"><span class="normalfntBlue"><strong>Approve</strong></span></a><?php } ?> <?php }else{ echo "(".$rowc['dtApprovedDate'].")";  } ?></td>
              <?php
$tempUser=	$rowc['UserName'];		  
				}
				?>
              
              <td bgcolor="#FFFFFF"><a href="rptPlaceOrder.php?orderNo=<?php echo $row['intOrderNo']?>&year=<?php echo $row['intOrderYear']?>&approveMode=0" target="_blank"><img src="../../../../../images/view.png" width="91" height="19" /></a></td>
              </tr>
              	<?php 
        		 } 
       			 ?>
            </table>
          </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><img src="../../../../../planning/img/Tclose.jpg" width="92" height="24" /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>

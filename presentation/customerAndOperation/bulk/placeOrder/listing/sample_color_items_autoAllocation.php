<?php 
// ini_set('display_errors',1);
/*
if(cumulative cunsumption > 0)
consumption						=> cumulative consumption
if(cumulative cunsumption < 0)
consumption						=> sample consumption

Maximum Allocatable Qty	= (sales order Qty - actual production raised Qty)*(consumption)*presentage - (style stock balance for item)

if(Maximum Allocatable Qty > 0 ) => - from bulk stock and + to style stock
if(Maximum Allocatable Qty < 0 ) =>  + to bulk stock and - from style stock

*/

	$userId 					= $_SESSION['userId'];
	$company 					= $_SESSION['headCompanyId'];
	//$location 					= $_SESSION['CompanyID'];
 	$location 					= $trnsLocation;
	
	$toSavedQty					= 0;
	$transSavedQty				= 0;
	
	$allocateFlag			  	= 1;
	$sampAlloPercentage  	  	= 25;
	$fullAlloPercentage  	  	= 1;
	$extraAlloPercentageFoil  	= 10;
	$extraAlloPercentageSpecial = 10;
	$extraAlloPercentageInkItm  = 10;
	
	$row						= $obj_permision->get_auto_allocation_permision($company);
	$allocateFlag			  	= $row['AUTO_ALLOCATION_PERMISION'];
	$fullAllocateFlag		  	= $row['FULL_AUTO_ALLOCATION_PERMISION'];
	
	$row						= $obj_commom->get_auto_allocation_percentages($company,'RunQuery');
	$sampAlloPercentage  	  	= $row['SAMPLE_ALLOCATION_%'];
	$fullAlloPercentage  	  	= $row['FULL_ALLOCATION_%'];
	$extraAlloPercentageFoil  	= $row['FOIL_EXTRA_ALLOCATION_%'];
	$extraAlloPercentageSpecial = $row['SP_RM_EXTRA_ALLOCATION_%'];
	$extraAlloPercentageInkItm  = $row['INK_ITEM_EXTRA_ALLOCATION_%'];
	
	
	if(($allocateFlag==1) && ($fullAllocateFlag==1)){
		$percentage_foil	= $fullAlloPercentage+$fullAlloPercentage*$extraAlloPercentageFoil/100;
		$percentage_sp_rm	= $fullAlloPercentage+$fullAlloPercentage*$extraAlloPercentageSpecial/100;
		$percentage_ink		= $fullAlloPercentage+$fullAlloPercentage*$extraAlloPercentageInkItm/100;
	}
	else if(($allocateFlag==1) && ($fullAllocateFlag==0)){
		$percentage_foil	= $sampAlloPercentage+$sampAlloPercentage*$extraAlloPercentageFoil/100;
		$percentage_sp_rm	= $sampAlloPercentage+$sampAlloPercentage*$extraAlloPercentageSpecial/100;
		$percentage_ink		= $sampAlloPercentage+$sampAlloPercentage*$extraAlloPercentageInkItm/100;
	}
	else{
		$percentage_foil	= 0;
		$percentage_sp_rm	= 0;
		$percentage_ink		= 0;
	}	
	
	
	

		$sql_O = "SELECT 
					trn_orderdetails.intSalesOrderId,
					trn_orderdetails.strGraphicNo,
					trn_orderdetails.intSampleNo,
					trn_orderdetails.intSampleYear,
					trn_orderdetails.strCombo,
					trn_orderdetails.strPrintName,
					trn_orderdetails.intRevisionNo,
					round(trn_orderdetails.intQty+trn_orderdetails.intQty*trn_orderdetails.dblDamagePercentage/100) as intQty,
					trn_orderdetails.dblPrice,
					trn_orderdetails.dtPSD,
					trn_orderdetails.dtDeliveryDate 
				FROM trn_orderdetails 
				WHERE
					trn_orderdetails.intOrderNo 		=  '$orderNo' AND
					trn_orderdetails.intOrderYear 		=  '$orderYear' 
				GROUP BY
					trn_orderdetails.intOrderNo,
					trn_orderdetails.intOrderYear,
					trn_orderdetails.intSalesOrderId
					";
		$result_O = $db->RunQuery($sql_O);
		while($row=mysqli_fetch_array($result_O)){
			$salesOrder	=	$row['intSalesOrderId'];
			$sampleNo	=	$row['intSampleNo'];			
			$sampleYear	=	$row['intSampleYear'];			
			$revision	=	$row['intRevisionNo'];			
			$combo		=	$row['strCombo'];			
			$printName	=	$row['strPrintName'];			
			$qty		=	$row['intQty'];	
						
     $sql_items="select 
				t.type,
				t.intSampleNo,
				t.intSampleYear,
				t.intRevisionNo ,
				t.strCombo ,
				t.strPrintName ,
				 t.intId ,
				 sum(qty) as consum
				
				from (
				
				(
					SELECT     
					1 as type, 
					sc.intSampleNo, 
					sc.intSampleYear, 
					sc.intRevisionNo, 
					sc.strCombo, 
					sc.strPrintName,
					 mst_item.intId,
					 (sum(round(dblColorWeight/((SELECT
					Sum(trn_sample_color_recipes.dblWeight)  
					FROM trn_sample_color_recipes
					WHERE
					trn_sample_color_recipes.intSampleNo = sc.intSampleNo AND
					trn_sample_color_recipes.intSampleYear = sc.intSampleYear AND
					trn_sample_color_recipes.intRevisionNo =  sc.intRevisionNo AND
					trn_sample_color_recipes.strCombo =  sc.strCombo AND
					trn_sample_color_recipes.strPrintName =  sc.strPrintName AND
					trn_sample_color_recipes.intTechniqueId =  sc.intTechniqueId AND
					trn_sample_color_recipes.intInkTypeId =  trn_sampleinfomations_details_technical.intInkTypeId AND
					trn_sample_color_recipes.intColorId =  trn_sampleinfomations_details_technical.intColorId 
					 ))*dblWeight /dblNoOfPcs,6))*($qty*$percentage_ink/100)) as qty 
					FROM
					trn_sample_color_recipes as sc
					Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = sc.intSampleNo 
					AND trn_sampleinfomations_details_technical.intSampleYear = sc.intSampleYear 
					AND trn_sampleinfomations_details_technical.intRevNo = sc.intRevisionNo 
					AND trn_sampleinfomations_details_technical.strComboName = sc.strCombo 
					AND trn_sampleinfomations_details_technical.strPrintName = sc.strPrintName 
					AND trn_sampleinfomations_details_technical.intColorId = sc.intColorId 
					AND trn_sampleinfomations_details_technical.intInkTypeId = sc.intInkTypeId
					Inner Join mst_item ON mst_item.intId = sc.intItem
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId
					group by 
					sc.intSampleNo, 
					sc.intSampleYear, 
					sc.intRevisionNo, 
					sc.strCombo, 
					sc.strPrintName ,
					 mst_item.intId 
			)
			UNION	
			(
				SELECT  
						2 as type, 
				trn_sample_spitem_consumption.intSampleNo, 
				trn_sample_spitem_consumption.intSampleYear, 
				trn_sample_spitem_consumption.intRevisionNo, 
				trn_sample_spitem_consumption.strCombo, 
				trn_sample_spitem_consumption.strPrintName,
				 mst_item.intId, 
					   sum(trn_sample_spitem_consumption.dblQty/dblNoOfPcs*($qty*$percentage_sp_rm/100)) as qty 
					   FROM
					trn_sample_spitem_consumption 
						Inner Join mst_item ON mst_item.intId = trn_sample_spitem_consumption.intItem
					Inner Join mst_units ON mst_units.intId = mst_item.intUOM
				group by 
				trn_sample_spitem_consumption.intSampleNo, 
				trn_sample_spitem_consumption.intSampleYear, 
				trn_sample_spitem_consumption.intRevisionNo, 
				trn_sample_spitem_consumption.strCombo, 
				trn_sample_spitem_consumption.strPrintName ,
				 mst_item.intId 
			)
			UNION 
			(
				SELECT    
						3 as type, 
				trn_sample_foil_consumption.intSampleNo, 
				trn_sample_foil_consumption.intSampleYear, 
				trn_sample_foil_consumption.intRevisionNo, 
				trn_sample_foil_consumption.strCombo, 
				trn_sample_foil_consumption.strPrintName,
				 mst_item.intId, 
						sum(trn_sample_foil_consumption.dblMeters*($qty*$percentage_foil/100)) as qty 
					FROM 
						trn_sample_foil_consumption  
						Inner Join mst_item ON mst_item.intId = trn_sample_foil_consumption.intItem
						Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
						Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
						Inner Join mst_units ON mst_units.intId = mst_item.intUOM
				group by 
				trn_sample_foil_consumption.intSampleNo, 
				trn_sample_foil_consumption.intSampleYear, 
				trn_sample_foil_consumption.intRevisionNo, 
				trn_sample_foil_consumption.strCombo, 
				trn_sample_foil_consumption.strPrintName ,
				 mst_item.intId 
			)
				) as t
				WHERE 
				t.intSampleNo='$sampleNo' AND 
				t.intSampleYear='$sampleYear' AND 
				t.intRevisionNo='$revision' AND 
				t.strCombo='$combo' AND 
				t.strPrintName='$printName'  
				GROUP BY 
				t.type,
				t.intSampleNo,
				t.intSampleYear,
				t.intRevisionNo ,
				t.strCombo ,
				t.strPrintName,
				 t.intId;
				 "; 
				 
				 
			//echo $sql_items;			
			$result_items = $db->RunQuery($sql_items);
			$sOrder=1;
			while($row_items=mysqli_fetch_array($result_items)){
				
 				$item		= $row_items['intId'];
				//$Qty		= $row_items['Qty'];
				$item_type	= $row_items['type'];
				
				$details		= $obj_warehouse_get->maximum_order_item_allocatable_details($orderNo,$orderYear,$salesOrder,$item,$item_type,$percentage_foil,$percentage_ink,$percentage_sp_rm,$executionType);
				$Qty_to_save	= ($obj_commom->ceil_to_decimal_places($details['tot_qty_for_allocate'],2)) - ($obj_commom->ceil_to_decimal_places($details['style_stock_bal'],2));
				$Qty			= $Qty_to_save;
				//--------IF NEW CUMULATIVE CONSUMPTION IS GREATER THAN PREVIOUS------------------------------------
				if($Qty_to_save > 0){
					//Substract from bulk stock
					$type		= '25_ITEM_ALLOC';
					$resultG = getGrnWiseStockBalance_bulk($location,$item);
					while($rowG=mysqli_fetch_array($resultG)){
							$stockBal = $rowG['stockBal'];
							$Qty 	  = $obj_commom->ceil_to_decimal_places($Qty,2);
							if(($Qty>0) && ($stockBal>0)){
								if($stockBal>=$Qty){
								$saveQty=$Qty;
								//$Qty=0;
								}
								else if($Qty>$stockBal){
								$saveQty=$stockBal;
								//$Qty=$Qty-$saveQty;
								}
								$grnNo=$rowG['intGRNNo'];
								$grnYear=$rowG['intGRNYear'];
								$grnDate=$rowG['dtGRNDate'];
								$grnRate=$rowG['dblGRNRate'];	
								$currency=loadCurrency($grnNo,$grnYear,$item);
								//$saveQty=round($saveQty+0.04,1);
								$Qty=$Qty-$saveQty;
							
								if($saveQty>0){	
									//
									 $sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intOrderNo,intOrderYear,intSalesOrderId,intUser,dtDate,PROGRAM) 
										VALUES ('$company','$location','0','0','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','-$saveQty','$type','$orderNo','$orderYear','$salesOrder','$userId',now(),'CUSTOMER PO')";
									$resultI = $db->RunQuery($sqlI);
									
									// 
									$sql = "INSERT INTO `ware_stocktransactions` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intOrderNo,intOrderYear,intSalesOrderId,intUser,dtDate,PROGRAM) 
											VALUES ('$company','$location','0','0','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','$saveQty','$type','$orderNo','$orderYear','$salesOrder','$userId',now(),'CUSTOMER PO')";
									$result 	= $db->RunQuery($sql);
	
									if($resultI){
										$transSavedQty+=$saveQty;
									}
									if((!$resultI) && ($rollBackFlag!=1)){
										$rollBackFlag=1;
										$sqlM=$sqlI;
										$rollBackMsg = "Ink Item Allocation Error!";
									}
								}
							}
					}
				}
				//--------IF NEW CUMULATIVE CONSUMPTION IS LESS THAN PREVIOUS------------------------------------
				else if($Qty_to_save < 0){
					//Substract from bulk stock
					$type	 = 'UNALLOCATE';
					$Qty	 = $Qty_to_save*(-1);
					$resultG = $obj_warehouse_get->get_grn_wise_style_stock($location,$orderNo,$orderYear,$salesOrder,$item,'RunQuery');
					while($rowG=mysqli_fetch_array($resultG)){
							$stockBal = $rowG['stockBal'];
							$Qty 	  = $obj_commom->ceil_to_decimal_places($Qty,2);
							if(($Qty>0) && ($stockBal>0)){
								if($stockBal>=$Qty){
								$saveQty=$Qty;
								//$Qty=0;
								}
								else if($Qty>$stockBal){
								$saveQty=$stockBal;
								//$Qty=$Qty-$saveQty;
								}
								$grnNo=$rowG['intGRNNo'];
								$grnYear=$rowG['intGRNYear'];
								$grnDate=$rowG['dtGRNDate'];
								$grnRate=$rowG['dblGRNRate'];	
								$currency=loadCurrency($grnNo,$grnYear,$item);
								//$saveQty=round($saveQty+0.04,1);
								$Qty=$Qty-$saveQty;
							
								if($saveQty>0){	
									//
									 $sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intOrderNo,intOrderYear,intSalesOrderId,intUser,dtDate,PROGRAM) 
										VALUES ('$company','$location','0','0','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','$saveQty','$type','$orderNo','$orderYear','$salesOrder','$userId',now(),'CUSTOMER PO')";
									$resultI = $db->RunQuery($sqlI);
									
									// 
									$sql = "INSERT INTO `ware_stocktransactions` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intOrderNo,intOrderYear,intSalesOrderId,intUser,dtDate,PROGRAM) 
											VALUES ('$company','$location','0','0','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','-$saveQty','$type','$orderNo','$orderYear','$salesOrder','$userId',now(),'CUSTOMER PO')";
									$result 	= $db->RunQuery($sql);
	
									if($resultI){
										$transSavedQty+=$saveQty;
									}
									if((!$resultI) && ($rollBackFlag!=1)){
										$rollBackFlag=1;
										$sqlM=$sqlI;
										$rollBackMsg = "Ink Item Allocation Error!";
									}
								}
							}
					}
				}
				//-------------------------------------------------------------				
				

				if($result){
					$transSavedQty+=$saveQty;
				}
				if((!$result) && ($rollBackFlag!=1)){
					$rollBackFlag=1;
					$sqlM=$sqlI;
					$rollBackMsg = "Ink Item Allocation Error!";
				}
 				
				$sOrder++;
			}
		}
			
//--------------------------------------------------------
function get_color_room_id($location){
	
	global $db;
	
	$sql	= "SELECT
				mst_substores.intId
				FROM `mst_substores`
				WHERE `intLocation` = '$location' AND `intColorRoom` = '1'";
	
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	$id		= $row['intId'];
	
	return $id;
}
//--------------------------------------------------------------
function getGrnWiseStockBalance_bulk($location,$item)
{
	global $db;
	 $sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) AS stockBal,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear,
			ware_stocktransactions_bulk.dtGRNDate,
			ware_stocktransactions_bulk.dblGRNRate 
			FROM ware_stocktransactions_bulk 
			WHERE
			ware_stocktransactions_bulk.intItemId =  '$item' AND
			ware_stocktransactions_bulk.intLocationId =  '$location'
			GROUP BY
			ware_stocktransactions_bulk.intItemId,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear, 
			ware_stocktransactions_bulk.dblGRNRate 
			ORDER BY
			ware_stocktransactions_bulk.dtGRNDate ASC";

	$result = $db->RunQuery($sql);
	return $result;	
}
//--------------------------------------------------------------
function loadCurrency($grnNo,$grnYear,$item)
{
	global $db;
	
	if(($grnNo!=0) && ($grnYear!=0)){
	  $sql = "SELECT
			trn_poheader.intCurrency as currency 
			FROM
			ware_grnheader
			Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
			WHERE
			ware_grnheader.intGrnNo =  '$grnNo' AND
			ware_grnheader.intGrnYear =  '$grnYear'";
	}
	else{
	$sql = "SELECT
			mst_item.intCurrency as currency 
			FROM mst_item
			WHERE
			mst_item.intId =  '$item'";
	}

	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);	
	return $row['currency'];;	
}
//--------------------------------------------------------
?>





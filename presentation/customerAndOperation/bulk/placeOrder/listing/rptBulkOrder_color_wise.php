<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>

<?php
ini_set('display_errors',0);
$companyId = $_SESSION['headCompanyId'];
$locationId = $_SESSION['CompanyID'];

$userId 	= $_SESSION['userId'];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];

$programName='Place Order';
$programCode='P0427';


$orderNo 			= $_REQUEST['orderNo'];
$orderYear			= $_REQUEST['orderYear'];
$styleNo 			= $_REQUEST['styleNo'];
$graphicNo			= $_REQUEST['graphicNo'];
$salesOrderNo 		= $_REQUEST['salesOrderNo'];
$combo			    = $_REQUEST['combo'];

$approveMode 		= $_REQUEST['approveMode'];
$revise = $_REQUEST['revise'];


		$sql = "SELECT
					trn_orderheader.strCustomerPoNo,
					trn_orderheader.dtDate,
					trn_orderheader.dtDeliveryDate,
					mst_customer.strName AS strCustomer,
					mst_customer_locations_header.strName AS strCustomerLocations,
					mst_financecurrency.strCode AS currencyCode,
					trn_orderheader.strContactPerson,
					trn_orderheader.strRemark,
					A.strUserName AS marketer,
					trn_orderheader.intStatus,
					trn_orderheader.intReviseNo, 
					B.strUserName AS creator,
					intApproveLevelStart,
					mst_locations.intCompanyId,
					trn_orderheader.intCreator
				FROM
					trn_orderheader
					Inner Join mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
					Inner Join mst_customer_locations_header ON mst_customer_locations_header.intId = trn_orderheader.intCustomerLocation
					Inner Join mst_financecurrency ON mst_financecurrency.intId = trn_orderheader.intCurrency
					Inner Join sys_users AS A ON A.intUserId = trn_orderheader.intMarketer
					Inner Join sys_users AS B ON B.intUserId = trn_orderheader.intCreator
					Inner Join mst_locations on mst_locations.intId = trn_orderheader.intLocationId
				WHERE
					trn_orderheader.intOrderNo 		=  '$orderNo' AND
					trn_orderheader.intOrderYear 	=  '$orderYear'
					";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$intCreateCompanyId		= $row['intCompanyId'];
		$intCreateUserId		= $row['intCreator'];
		$strCustomerPoNo 		= $row['strCustomerPoNo'];
		$dtDate 				= $row['dtDate'];
		$dtDeliveryDate 		= $row['dtDeliveryDate'];
		$strCustomer 			= $row['strCustomer'];
		$strCustomerLocations 	= $row['strCustomerLocations'];
		$currencyCode 			= $row['currencyCode'];
		$strContactPerson		= $row['strContactPerson'];
		$strRemark 				= $row['strRemark'];
		$marketer				= $row['marketer'];
		$intStatus				= $row['intStatus'];
		$creator				= $row['creator'];
		$intApproveLevelStart	= $row['intApproveLevelStart']-1;
		$savedLevels				= $row['intApproveLevelStart'];
		$reviseNo = $row['intReviseNo'];

$confirmationMode=loadConfirmatonMode($programCode,$intStatus,$savedLevels,$userId);
$rejectionMode=loadRejectionMode($programCode,$intStatus,$savedLevels,$userId);
$reviseMode=loadReviseMode($programCode,$intStatus,$savedLevels,$userId);

?>
<head>
<script type="text/javascript" src="presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder-js.js"></script>

<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position: absolute;
	left: 239px;
	top: 120px;
	width: 619px;
	height: 235px;
	z-index: 1;
}
.APPROVE {
	font-size: 16px;
	font-weight: bold;
	color: #00C;
}
</style>
</head>

<body>
<?php
  $placeOrderApproveLevel=$placeOrderApproveLevel+1;
 $approveLevel = $savedLevels;
 
if($intStatus==$placeOrderApproveLevel)//pending
{
?>
<div id="apDiv1"><img src="images/pending.png" width="624"  /></div>
<?php
}
?>
<form id="frmSamplePlaceOrderReport" name="frmSamplePlaceOrderReport" method="post" action="rptBulkOrder.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td colspan="3"><div id="divPoNo" style="display:none"><?php echo $orderNo ?>/<?php echo $orderYear ?></div></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>CUSTOMER PURCHASE ORDER REPORT - COLOR WISE</strong><strong></strong> </div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="10" align="center" bgcolor="#FFFFFF">
    <?php if(($approveMode==1) && ($intStatus>1)){
			if($confirmationMode==1) { 
			?>
			<img src="images/approve.png" align="middle" class="mouseover noPrint" id="imgApprove" /><?php
			}
			if(($rejectionMode==1) && ($intStatus>1)){
			?><img src="images/reject.png" align="middle" class="mouseover noPrint" id="imgReject" /><?php
                }
            }
  else if(($revise==1) && ($reviseMode==1)){?><img src="images/revise_new.png" align="middle" class="butRevise" id="butRevise"><?php } ?>	
    </td>
  </tr>
  <tr>
  <?php
	$savedStat = $intApproveLevelStart;
 	if($intStatus==1)
	{
	?>
   <td colspan="9" align="center" class="APPROVE" style="color:#090">CONFIRMED</td>
   <?PHP
	}
	else if($intStatus==0)
	{
   ?>
   <td colspan="9" align="center" class="APPROVE" style="color:#F00">REJECTED</td>
   <?php
	}
	else
	{
   ?>
   <td width="9%" colspan="9" align="center" class="APPROVE">PENDING</td>
   <?php
	}
   ?>
   <?php
   //-----------------
	  $sqlc = "SELECT
			trn_sampleorderheader_approvedby.intApproveUser,
			trn_sampleorderheader_approvedby.dtApprovedDate,
			useraccounts.UserName,
			trn_sampleorderheader_approvedby.intApproveLevelNo
	
			FROM
			trn_orderheader_approvedby
			Inner Join useraccounts ON trn_sampleorderheader_approvedby.intApproveUser = useraccounts.intUserID
			
			WHERE
			trn_sampleorderheader_approvedby.intSampleOrderNo =  '$poNo' AND
			trn_sampleorderheader_approvedby.intSampleOrderYear =  '$year' AND
			trn_sampleorderheader_approvedby.intApproveLevelNo =  '$intStatus'
";
	 $resultc = $db->RunQuery($sqlc);
	 $rowc=mysqli_fetch_array($resultc);
	 $rejectBy=$rowc['UserName'];
	 $rejectDate=$rowc['dtApprovedDate'];

   //-----------------
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="9%" class="normalfnt">Order No</td>
    <td width="1%" align="center" valign="middle">:</td>
    <td width="17%"><span class="normalfnt"><?php echo $orderNo ?>/<?php echo $orderYear ?></span></td>
    <td width="10%" class="normalfnt">Date</td>
    <td width="1%" align="center" valign="middle">:</td>
    <td width="18%"><span class="normalfnt"><?php echo $dtDate ?></span></td>
    <td width="15%" class="normalfnt">&nbsp;</td>
  <td width="10%" class="normalfnt">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Customer</td>
    <td align="center" valign="middle">:</td>
    <td><span class="normalfnt"><?php echo $strCustomer ?></span></td>
    <td width="10%" class="normalfnt">Delivery Date</td>
    <td width="1%" align="center" valign="middle">:</td>
    <td width="18%"><span class="normalfnt"><?php echo $dtDeliveryDate ?></span></td>
    
    <td class="normalfnt"><?php if($intStatus==0){?>
      Rejected Date      <?php } ?></td>
    <td><?php if($intStatus==0){?><strong>:</strong>&nbsp;<span class="normalfnt"><?php echo $rejectDate ?></span><?php } ?></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Cus PO No</td>
    <td align="center" valign="middle">:</td>
    <td class="normalfnt"><?php echo $strCustomerPoNo; ?></td>
    <td><span class="normalfnt">Marketer</span></td>
    <td align="center" valign="middle">:</td>
    <td><span class="normalfnt"><?php echo $marketer;?></span></td>
    <td><span class="normalfnt"><?php if($intStatus==0){?>
      Rejected By
      <?php }?></span></td>
    <td><?php if($intStatus==0){?><strong>:</strong>&nbsp;<span class="normalfnt"><?php echo $rejectBy ?></span><?php } ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">:</td>
    <td class="normalfnt">&nbsp;</td>
    <td class="normalfnt">PO Revise No</td>
    <td align="center" valign="middle">:</td>
    <td class="normalfnt"><?php echo $reviseNo; ?></td>
    <td colspan="2" class="normalfnt">&nbsp;</td>
    </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td colspan="7" class="normalfnt">
          <table width="806" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="gridHeader">
              <td width="8%"  height="22">Sales Order No</td>
              <td width="12%" >Graphic No</td>
              <td width="9%" >Style No</td>
              <td width="7%" >Combo</td>
              <td width="7%" >Qty </td>
              <td width="7%" >Color</td>
              <td width="9%" >Consumption </td>
              </tr>
            <?php 
			$tmpSalesId='';
			$tmpGraphic='';
			$tmpCombo='';
			
	  	 $sql1 = "	SELECT
tb1.strSalesOrderNo,
tb1.strGraphicNo,
tb1.strStyleNo,
tb1.strCombo,
trn_sampleinfomations_details_technical.intColorId,
 mst_colors.strName as color,
tb1.intQty,

(SELECT sum(trn_orderdetails.intQty) from trn_orderdetails 
WHERE
trn_orderdetails.intOrderNo 	=  tb1.intOrderNo AND
trn_orderdetails.intOrderYear 	=  tb1.intOrderYear AND 
trn_orderdetails.strSalesOrderNo 	=  tb1.strSalesOrderNo AND
trn_orderdetails.strGraphicNo 	=  tb1.strGraphicNo AND 
trn_orderdetails.strStyleNo 	=  tb1.strStyleNo AND 
trn_orderdetails.strCombo 	=  tb1.strCombo  
 ) as intQty , 
 
sum((trn_sampleinfomations_details_technical.dblColorWeight*tb1.intQty)) as colWeight  
FROM
trn_orderdetails as tb1 
Inner Join trn_sampleinfomations_details_technical ON tb1.intSampleNo = trn_sampleinfomations_details_technical.intSampleNo AND tb1.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear AND tb1.strCombo = trn_sampleinfomations_details_technical.strComboName AND tb1.strPrintName = trn_sampleinfomations_details_technical.strPrintName AND tb1.intRevisionNo = trn_sampleinfomations_details_technical.intRevNo
left Join mst_colors ON trn_sampleinfomations_details_technical.intColorId = mst_colors.intId  
WHERE
						tb1.intOrderNo 	=  '$orderNo' AND
						tb1.intOrderYear 	=  '$orderYear' ";
			if($styleNo!='')
				$sql1 .=" AND tb1.strStyleNo 	=  '$styleNo' ";
			if($graphicNo!='')
				$sql1 .=" AND tb1.strGraphicNo 	=  '$graphicNo' ";
			if($salesOrderNo!='')
				$sql1 .=" AND tb1.strSalesOrderNo 	=  '$salesOrderNo' ";
			if($combo!='')
				$sql1 .=" AND tb1.strCombo 	=  '$combo' ";  
				
				$sql1 .=" GROUP BY 
						tb1.strSalesOrderNo,
						tb1.strGraphicNo,
						tb1.strStyleNo,
						tb1.strCombo,
						trn_sampleinfomations_details_technical.intColorId 
				";
			 	//  echo $sql1;
						
			$result1 = $db->RunQuery($sql1);
			while($row=mysqli_fetch_array($result1))
			{
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
            <?php
			if(($tmpSalesId!=$row['strSalesOrderNo']) || ($tmpStyleNo!=$row['strStyleNo'])  || ($tmpGraphic!=$row['strGraphicNo']) || ($tmpCombo!=$row['strCombo']) ){
			?>
              <td class="normalfnt" >&nbsp;<?php echo $row['strSalesOrderNo'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strGraphicNo'] ?>&nbsp;</td>
              <td class="normalfnt" ><?php echo $row['strStyleNo'] ?></td>
              <td >&nbsp;<?php echo $row['strCombo'] ?>&nbsp;</td>
              <td class="normalfntRight" ><?php echo $row['intQty'] ?>&nbsp;</td>
            <?php
			}
			else{
			?>
             <td class="normalfnt" ></td>
             <td class="normalfnt" ></td>
             <td width="6%" class="normalfnt" ></td>
             <td width="7%" class="normalfnt" ></td>
             <td width="7%" class="normalfnt" ></td>
              <?php
			}
			?>
              <td width="10%" class="normalfntMid" ><?php echo $row['color'] ?></td>
              <td width="11%" class="normalfntRight" ><?php echo number_format($row['colWeight'],4) ?></td>
              
              
              
              </tr>
            <?php   
			$tmpSalesId=$row['strSalesOrderNo'];
			$tmpGraphic=$row['strGraphicNo'];
			$tmpStyleNo=$row['strStyleNo'];
			$tmpCombo=$row['strCombo'];
			}
	  ?>
            
            </table>
        </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>

<tr>
                <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr height="40" >
                      <td align="center" class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></td>
                      <td align="center" class="normalfntMid">&nbsp;</td>
</tr>                </table>
        
</form>
</body>
</html>
<?php
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//-------------------------------------------------------------------------
function getRcvQty($orderNo,$year,$salesOrderNo,$size){
	global $db;
	 $sql1 = "SELECT
Sum(ware_fabricreceiveddetails.dblQty) AS RcvQty
FROM
ware_fabricreceivedheader
Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
WHERE
ware_fabricreceivedheader.intOrderNo =  '$orderNo' AND
ware_fabricreceivedheader.intOrderYear =  '$year' AND
ware_fabricreceivedheader.intStatus =  '1' AND
ware_fabricreceiveddetails.intSalesOrderId =  '$salesOrderNo' AND
ware_fabricreceiveddetails.strSize =  '$size'
GROUP BY
ware_fabricreceivedheader.intOrderNo,
ware_fabricreceivedheader.intOrderYear,
ware_fabricreceiveddetails.intSalesOrderId,
ware_fabricreceiveddetails.strSize";	
	$result1 = $db->RunQuery($sql1);
	$row1=mysqli_fetch_array($result1);
	$RcvQty=$row1['RcvQty'];
	return $RcvQty;
}
//------------------------------function loadReviseMode-------------------
function loadReviseMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$reviseMode=0;
	$sqlp = "SELECT
		menupermision.intRevise 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intRevise']==1){
	 if(($intStatus<=$savedStat) && ($intStatus!=0)){
	 $reviseMode=1;
	 }
	}
	 
	return $reviseMode;
}
//-----------------------------------------------------------
	function getGroundColor($sampNo,$sampleYear,$printName,$combo,$revNo)
{
		global $db;
		$sql = "SELECT DISTINCT
				trn_sampleinfomations_details.intGroundColor,
				mst_colors_ground.strName
				FROM
					trn_sampleinfomations_details 
					Inner Join mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
				WHERE
					trn_sampleinfomations_details.intSampleNo =  '$sampNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear'  AND
					trn_sampleinfomations_details.strPrintName =  '$printName' AND
					trn_sampleinfomations_details.strComboName =  '$combo' AND
					trn_sampleinfomations_details.intRevNo =  '$revNo'
					
					
					
				";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$g= $row['strName'];	
		if($revNo=='') 
		$g='';	
		return $g;	
}
//-----------------------------------------------------------
?>	

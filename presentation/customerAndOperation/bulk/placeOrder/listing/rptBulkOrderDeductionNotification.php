<?php (define('UNLOCKPAGE', true) ? die('<<< Access denied >>>') : ''); ?>
<?php
$companyId = $sessions->getCompanyId();
$userId = $sessions->getUserId();
$locationId = $sessions->getLocationId();
$backwardseperator 	= "../../../../../";
include $backwardseperator."dataAccess/Connector.php";

$orderNo 			= $_REQUEST['orderNo'];
$orderYear			= $_REQUEST['orderYear'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Order Quantity / Consumption Deduction Notification</title>
    <script type="text/javascript" src="presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder-js.js?n=1"></script>
    <link rel="stylesheet" type="text/css" href="../../../libraries/calendar/theme.css"/>
    <script src="../libraries/calendar/calendar.js" type="text/javascript"></script>
    <script src="../libraries/calendar/calendar-en.js" type="text/javascript"></script>
    <script src="../libraries/calendar/runCalender.js" type="text/javascript"></script>
    <style>
        .break {
            page-break-before: always;
        }

        @media print {
            .noPrint {
                display: none;
            }
        }

        #apDiv1 {
            position: absolute;
            left: 237px;
            top: 175px;
            width: 619px;
            height: 235px;
            z-index: 1;
        }

        .APPROVE {
            font-size: 16px;
            font-weight: bold;
            color: #00C;
        }
    </style>
</head>
<body>
<form id="frmDeduction" name="frmDeduction" method="post" action="">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80"
valign="top"><?php include 'presentation/customerAndOperation/reports/reportHeader.php' ?></td>
<td width="20%"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF"><strong>Order Quantity / Consumption Deduction Notification</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>&nbsp;

</tr>
<tr>
<tr>

<td style=""> 
<b>Order No :
<?php echo $orderYear ?>/<?php echo $orderNo  ?>
</b>
</td>
</tr>
<td>
<table width="100%">
<tr>
<td colspan="7" class="normalfnt">
<table width="806" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">

<tr class="normalfnt" >
<th>Item</th>
<th>Previous Requested</th>
<th>Current Requested</th>
<th>Sup PO</th>
<th>GRN</th>
<th>MRN</th>
</tr>


<?php
$result 	=	get_details($orderNo,$orderYear);
while ($row	= 	mysqli_fetch_array($result))
{
	
?>

<tr>
<td <?php if($row['current_qty']=="" || $row['current_qty']==0){?> style="color:red;" <?php }?>> <?php echo $row['strName']; ?></td>
<td <?php if($row['current_qty']=="" || $row['current_qty']==0){?> style="color:red;" <?php }?>> <?php echo $row['previous_qty']; ?></td>
<td <?php if($row['current_qty']=="" || $row['current_qty']==0){?> style="color:red;" <?php }?>><?php echo $row['current_qty']; ?></td>
<td <?php if($row['current_qty']=="" || $row['current_qty']==0){?> style="color:red;" <?php }?>><?php echo $row['sup_po']; ?></td>
<td <?php if($row['current_qty']=="" || $row['current_qty']==0){?> style="color:red;" <?php }?>><?php echo $row['grn_no']; ?></td>
<td <?php if($row['current_qty']=="" || $row['current_qty']==0){?> style="color:red;" <?php }?>><?php echo $row['mrn_no']; ?></td>
</tr>

<?php
}
?>	

</tr>
</table>
</td>
<td width="6%">&nbsp;</td>
</tr>
</table>
</td>
</tr>
<tr>
</form>
</body>
</html>

<?php

function get_details($orderNo,$orderYear)
{
	global $db;
	
	$sql_select_revision =
							"SELECT
							trn_orderheader.intReviseNo
							FROM
							trn_orderheader
							WHERE
							trn_orderheader.intOrderNo = $orderNo
							AND
							trn_orderheader.intOrderYear = $orderYear ";
							//echo $sql_select_revision;
							$result	 	= $db->RunQuery($sql_select_revision);
							$row		= mysqli_fetch_array($result);
							$revision	= $row['intReviseNo'];
	
	
				$sql_select =
							"select 
							trn_po_prn_details_history.ITEM,
							mst_item.strName,
							trn_po_prn_details_history.REQUIRED as previous_qty,
							(select (trn_po_prn_details.REQUIRED)
							FROM
							trn_po_prn_details
							WHERE
							trn_po_prn_details.ORDER_NO = trn_po_prn_details_history.ORDER_NO
							AND
							trn_po_prn_details.ORDER_YEAR = trn_po_prn_details_history.ORDER_YEAR
							AND
							trn_po_prn_details.ITEM = trn_po_prn_details_history.ITEM
							) as current_qty,
							
							(select group_concat(concat(ware_mrnheader.intMrnNo,'/',ware_mrnheader.intMrnYear))
							FROM
							ware_mrnheader
							INNER JOIN
							ware_mrndetails
							ON
							ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND
							ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
							
							INNER JOIN
							trn_orderheader as t ON
							t.intOrderNo = ware_mrndetails.intOrderNo AND
							t.intOrderYear = ware_mrndetails.intOrderYear 
							
							WHERE
							t.intOrderNo = trn_orderheader.intOrderNo AND
							t.intOrderYear = trn_orderheader.intOrderYear
							) as mrn_no,
							
							(select GROUP_CONCAT(concat(ware_grnheader.intGrnNo,'/',ware_grnheader.intGrnYear))
							FROM
							ware_grnheader
							INNER JOIN
							ware_grndetails
							ON
							ware_grndetails.intGrnNo= ware_grnheader.intGrnNo AND
							ware_grndetails.intGrnYear = ware_grnheader.intGrnYear
							
							INNER JOIN
							trn_orderheader as t ON
							t.intOrderNo = ware_grndetails.intGrnNo AND
							t.intOrderYear = ware_grndetails.intGrnYear 
							
							WHERE
							t.intOrderNo = trn_orderheader.intOrderNo AND
							t.intOrderYear = trn_orderheader.intOrderYear
							) as grn_no,
							
							(
							SELECT
							GROUP_CONCAT(
							DISTINCT concat(
							trn_podetails.intPONo,
							'/',
							trn_podetails.intPOYear
							)
							)
							FROM
							trn_podetails
							INNER JOIN trn_prndetails ON trn_podetails.intPrnNo = trn_prndetails.intPrnNo
							AND trn_podetails.intPOYear = trn_prndetails.intYear 
							AND
							trn_podetails.intItem = trn_prndetails.intItem
							INNER JOIN trn_orderheader as t1 ON
							trn_prndetails.intOrderNo = t1.intOrderNo
							AND
							trn_prndetails.intOrderYear = t1.intOrderYear
							
							WHERE
							trn_prndetails.intOrderNo = trn_orderheader.intOrderNo
							AND trn_prndetails.intOrderYear = trn_orderheader.intOrderYear
							AND trn_prndetails.intItem = trn_po_prn_details_history.ITEM
							) AS sup_po
							
							FROM
							trn_po_prn_details_history
							
							INNER JOIN
							trn_orderheader
							ON
							trn_po_prn_details_history.ORDER_NO = trn_orderheader.intOrderNo AND
							trn_po_prn_details_history.ORDER_YEAR = trn_orderheader.intOrderYear
							
							INNER JOIN
							mst_item
							ON
							mst_item.intId = trn_po_prn_details_history.ITEM
							
							WHERE
							
							trn_orderheader.intOrderNo = $orderNo AND
							trn_orderheader.intOrderYear
							= $orderYear 
							AND
							trn_po_prn_details_history.REVISION = $revision - 1";
 
							//echo $sql_select;
							$result			= $db->RunQuery($sql_select);
							return $result;
}

?>


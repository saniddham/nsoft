<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "libraries/jqgrid2/inc/jqgrid_dist.php";

$location 		= $sessions->getLocationId();
$intUser  		= $sessions->getUserId();

//$programCode	= 'P0991';

/*################################################################
################## new part ####################################
################################################################
///// create where part ///////////////////
$arr =  json_decode($_REQUEST['filters'],true);
$arr = $arr['rules'];
$where_string = '';
$where_array = array(
				'company'=>'mst_locations.strName',
				);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Revised'=>'-1','Cancelled'=>'-2','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}

if(!count($arr)>0)					 
		$where_string .= "AND DATE(tb1.dtmCreateDate) = '".date('Y-m-d')."'";

################## end code ####################################*/



$company 	= $_SESSION['headCompanyId'];
$locationId = $_SESSION['CompanyID'];

$intUser  = $_SESSION["userId"];

$toCurrency=1;
//---------------------------------------------------------------------
	  	   $sql1 = "SELECT intId,strName FROM 
			mst_locations
			ORDER BY
			mst_locations.strName ASC
			";
			$result1 = $db->RunQuery($sql1);
			$str='';
			//$str1 .= ":All;" ;
			$j=0;
			while($row=mysqli_fetch_array($result1))
			{
				if($j==0){
				$inilocId=$row['intId'];
				$inilocation=$row['strName'];
				}
				$locId=$row['intId'];
				$location=$row['strName'];
				$str .= $location.":".$location.";" ;
				$j++;
			}
			//$str = substr($str1,0,-1);
			$str .= ":All" ;
//---------------------------------------------------------------------

$sql = "select * from(SELECT
mst_locations.intId,
mst_locations.strName as company,
mst_plant.intPlantId,
mst_plant.strPlantName as plant,
tb1.intBulkDispatchNo as DispNo,
tb1.intBulkDispatchNoYear as DispYear,
date(ware_fabricdispatchheader_approvedby.dtApprovedDate) as date,

concat(trn_orderheader.intOrderYear,'/',trn_orderheader.intOrderNo) as orderNo,

(select  group_concat(strGraphicNo separator ', ') from  (select distinct strGraphicNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
tb.intOrderNo =  tb1.intOrderNo AND
tb.intOrderYear =  tb1.intOrderYear
) as strGraphicNo, 
 
round((select Sum(ware_fabricdispatchdetails.dblGoodQty) FROM ware_fabricdispatchdetails 
WHERE ware_fabricdispatchdetails.intBulkDispatchNo=tb1.intBulkDispatchNo 
AND ware_fabricdispatchdetails.intBulkDispatchNoYear=tb1.intBulkDispatchNoYear 
 
 ),2) as Qty,

mst_financecurrency.strCode as currency,

round(((select Sum(ware_fabricdispatchdetails.dblGoodQty*trn_orderdetails.dblPrice) 
FROM ware_fabricdispatchdetails 
INNER JOIN ware_fabricdispatchheader on 
ware_fabricdispatchdetails.intBulkDispatchNo=ware_fabricdispatchheader.intBulkDispatchNo 
AND ware_fabricdispatchdetails.intBulkDispatchNoYear=ware_fabricdispatchheader.intBulkDispatchNoYear 
left JOIN mst_locations ON ware_fabricdispatchheader.intCompanyId = mst_locations.intId
left JOIN trn_orderheader ON ware_fabricdispatchheader.intOrderNo = trn_orderheader.intOrderNo 
AND ware_fabricdispatchheader.intOrderYear = trn_orderheader.intOrderYear
left JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear 
AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
left JOIN mst_financeexchangerate ON trn_orderheader.intCurrency = mst_financeexchangerate.intCurrencyId 
AND date(trn_orderheader.dtDate) = mst_financeexchangerate.dtmDate AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId
WHERE ware_fabricdispatchdetails.intBulkDispatchNo=tb1.intBulkDispatchNo 
AND ware_fabricdispatchdetails.intBulkDispatchNoYear=tb1.intBulkDispatchNoYear 
 )),2) as ammount,  

concat(mst_locations.strName,'/',mst_plant.strPlantName) as orderBy
FROM
ware_fabricdispatchheader  as tb1 
INNER JOIN ware_fabricdispatchheader_approvedby 
ON ware_fabricdispatchheader_approvedby.intBulkDispatchNo=tb1.intBulkDispatchNo 
AND ware_fabricdispatchheader_approvedby.intYear=tb1.intBulkDispatchNoYear 
AND ware_fabricdispatchheader_approvedby.intApproveLevelNo = tb1.intApproveLevels 
LEFT JOIN mst_locations ON mst_locations.intId = tb1.intCompanyId
INNER JOIN mst_plant ON mst_plant.intPlantId = mst_locations.intPlant
left JOIN trn_orderheader ON tb1.intOrderNo = trn_orderheader.intOrderNo 
AND tb1.intOrderYear = trn_orderheader.intOrderYear
left JOIN mst_financeexchangerate ON $toCurrency = mst_financeexchangerate.intCurrencyId 
AND date(trn_orderheader.dtDate) = mst_financeexchangerate.dtmDate 
AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId
LEFT JOIN mst_financecurrency ON mst_financecurrency.intId=mst_financeexchangerate.intCurrencyId 

WHERE
tb1.intStatus = 1 $where_string $limit)  as t where 1=1
						";
				//	 echo 	$sql;
$col = array();

$col["title"] 	= "Location"; // caption of column
$col["name"] 	= "company"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "5";
$col["stype"] 	= "select";
$str = $str ;
$col["editoptions"] 	=  array("value"=> $str);
$col["align"] 	= "center";

$cols[] = $col;	$col=NULL;

//SALES ORDER NO
$col["title"] = "Plant"; // caption of column
$col["name"] = "plant"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//SALES ORDER NO
$col["title"] = "Date"; // caption of column
$col["name"] = "date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//SALES ORDER NO
$col["title"] = "Order No"; // caption of column
$col["name"] = "orderNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//SALES ORDER NO
$col["title"] = "Graphic No"; // caption of column
$col["name"] = "strGraphicNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//ORDER YEAR
$col["title"] = "Dispatch year"; // caption of column
$col["name"] = "DispYear"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$col['link']	= "fabricDispatchNote/listing/rptFabricDispatchNote.php?serialNo={DispNo}&year={DispYear}";	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

//ORDER NO
$col["title"] 	= "Dispatch No"; // caption of column
$col["name"] 	= "DispNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//searchOper
$col["align"] 	= "center";
$col['link']	= "fabricDispatchNote/listing/rptFabricDispatchNote.php?serialNo={DispNo}&year={DispYear}";	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

$cols[] = $col;	$col=NULL;



//ORDER YEAR
$col["title"] = "Good Qty"; // caption of column
$col["name"] = "Qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;


$jq = new jqgrid('',$db);

$grid["caption"] 		= "Revenue Details Report";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'orderBy'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Location","op":"eq","data":"$inilocId"}
     ]
}
SEARCH_JSON;


			//	$inilocId=$row['intId'];
			//	$inilocation=$row['strName'];


$grid["search"] = true; 
$grid["postData"] = array("filters" => $sarr ); 

// export XLS file
// export to excel parameters - range could be "all" or "filtered"
$grid["export"] = array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"Revenue Details Report");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
$grid["export"]["range"] = "filtered"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);




$out = $jq->render("list1");
?>
<title>Revenue Details Report</title>
<?Php
echo $out ;


//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(trn_orderheader.intApproveLevelStart) AS appLevel
			FROM trn_orderheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
?>


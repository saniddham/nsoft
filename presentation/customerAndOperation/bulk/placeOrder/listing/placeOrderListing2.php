<?php
date_default_timezone_set('Asia/Kolkata');
//ini_set('allow_url_include',1);
//ini_set('display_errors',1);
session_start();

$backwardseperator 	= "../../../../../";
$thisFilePath 		=  $_SERVER['PHP_SELF'];
include_once $backwardseperator."dataAccess/Connector.php";

//include_once "../../../../../libraries/jqdrid/inc/jqgrid_dist.php";
include_once "../../../../../libraries/jqgrid2/inc/jqgrid_dist.php";

$company 	= $_SESSION['headCompanyId'];


$approveLevel = (int)getMaxApproveLevel();
$programName='Place Order';
$programCode='P0427';
$intUser  = $_SESSION["userId"];

//---------------------------------------------------------------------
   $sql1 = "SELECT intId,strName FROM 
	mst_companies
	ORDER BY
	mst_companies.strName ASC
	";
	$result1 = $db->RunQuery($sql1);
	$str1='';
	//$str1 .= ":All;" ;
	$j=0;
	while($row=mysqli_fetch_array($result1))
	{
		if($company==$row['intId']){
		$inilocId=$row['intId'];
		$inilocation=$row['strName'];
		}
		else{
		$locId=$row['intId'];
		$location=$row['strName'];
		$str1 .= $location.":".$location.";" ;
		}
		$j++;
	}
	
	//$str1 = substr($str1,0,-1);
	$str2=$inilocation.":".$inilocation.";" ;
	$str=$str2.$str1;
//	$str = substr($str,0,-1);
//	  $str = $str1;
	$str .= ":All" ;
//---------------------------------------------------------------------

$sql = "select * from all_order_list  where 1=1
						";
					 	//echo $sql;
$col = array();

$col["title"] 	= "Company"; // caption of column
$col["name"] 	= "company"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "8";
$col["stype"] 	= "select";
$str = $str ;
$col["editoptions"] 	=  array("value"=> $str);
$col["align"] 	= "center";

$cols[] = $col;	$col=NULL;

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = "Pending:Pending;Approved:Approved;Rejected:Rejected;Completed:Completed;Cancel:Cancel;:All" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist

$cols[] = $col;	$col=NULL;
//ORDER NO
$col["title"] 	= "Order No"; // caption of column
$col["name"] 	= "Order_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= "../addNew/placeOrder.php?orderNo={Order_No}&orderYear={Order_Year}";	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

$reportLink  = "rptBulkOrder.php?orderNo={Order_No}&orderYear={Order_Year}";
$reportLinkApprove  = "rptBulkOrder.php?orderNo={Order_No}&orderYear={Order_Year}&approveMode=1";

$cols[] = $col;	$col=NULL;


//ORDER YEAR
$col["title"] = "Order year"; // caption of column
$col["name"] = "Order_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//SALES ORDER NO
$col["title"] = "Sales Order No"; // caption of column
$col["name"] = "salesNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//ORDER YEAR
$col["title"] = "Style No"; // caption of column
$col["name"] = "styleNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

$col["title"] = "Grahpic No"; // caption of column
$col["name"] = "graphicNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//CUSTOMER PO NO
$col["title"] = "Customer PO No"; // caption of column
$col["name"] = "Customer_PO_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Customer"; // caption of column
$col["name"] = "Customer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER LOCATIONS
$col["title"] = "Customer Location"; // caption of column
$col["name"] = "Location_Name"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//DATE
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//DELIVERY DATE
$col["title"] = "Delivery Date"; // caption of column
$col["name"] = "Delivery_Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Order Qty
$col["title"] = "Order Qty"; // caption of column
$col["name"] = "Order_Qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//Order Qty
$col["title"] = "Received Qty"; // caption of column
$col["name"] = "recievedQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Customer place Order Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'Order_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"},{"field":"Company","op":"eq","data":"$inilocation"}
     ]
	 
}
SEARCH_JSON;


$grid["search"] = true; 
$grid["postData"] = array("filters" => $sarr ); 



// export XLS file
// export to excel parameters - range could be "all" or "filtered"
$grid["export"] = array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);




$out = $jq->render("list1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer PO Listing</title>

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />


</head>

<body>	
<form id="frmlisting" name="frmlisting" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
	<link rel="stylesheet" type="text/css" media="screen" href="../../../../../libraries/jqdrid/js/themes/smoothness/jquery-ui.custom.css"></link>	
	<link rel="stylesheet" type="text/css" media="screen" href="../../../../../libraries/jqdrid/js/jqgrid/css/ui.jqgrid.css"></link>	
	
	<script src="../../../../../libraries/jqdrid/js/jquery.min.js" type="text/javascript"></script>
	<script src="../../../../../libraries/jqdrid/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script src="../../../../../libraries/jqdrid/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="../../../../../libraries/jqdrid/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
	<script src="../../../../../libraries/javascript/script.js" type="text/javascript"></script>
	
    <td><table width="100%" border="0">
      <tr>
        <td>
        <div align="center" style="margin:10px">
        
			<?php echo $out?>
        </div>
        </td>
      </tr>

    </table></td>
    </tr>


</form>
</body>
</html>
<?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(trn_orderheader.intApproveLevelStart) AS appLevel
			FROM trn_orderheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
?>


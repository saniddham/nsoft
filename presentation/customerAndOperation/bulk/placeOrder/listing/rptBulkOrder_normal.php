<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId 		= $sessions->getCompanyId();
$userId 		= $sessions->getUserId();

$programCode	= 'P0427';

require_once "class/cls_permisions.php";				$objpermisionget			= new cls_permisions($db);
require_once "class/tables/sys_approvelevels.php";		$obj_sys_approvelevels		= new sys_approvelevels($db);
	
$permi_price_view 	= $objpermisionget->boolSPermision(8);
$str_price_view 	= 'hidden';

if($permi_price_view)
	$str_price_view = '';
	
$orderNo 			= $_REQUEST['orderNo'];
$orderYear			= $_REQUEST['orderYear'];
$approveMode 		= (!isset($_REQUEST["approveMode"])?'':$_REQUEST["approveMode"]);
$revise 			= (!isset($_REQUEST["revise"])?'':$_REQUEST["revise"]);

$ifSumDiff			= check_for_previous_PRI($orderNo,$orderYear);
$ifitemdiff			= check_for_previous_PRI_for_item($orderNo,$orderYear);

?>


<?php


$sql = "SELECT
					trn_orderheader.strCustomerPoNo,
					mst_financepaymentsterms.strName as paymentTerm, 
					trn_orderheader.dtDate,
					trn_orderheader.dtDeliveryDate,
					mst_customer.strName AS strCustomer,
					mst_customer_locations_header.strName AS strCustomerLocations,
					mst_financecurrency.strCode AS currencyCode,
					mst_financecurrency.strSymbol,
					trn_orderheader.strContactPerson,
					trn_orderheader.REJECT_REASON,
					trn_orderheader.strRemark,
					A.strUserName AS marketer,
					trn_orderheader.intStatus,
					trn_orderheader.intReviseNo, 
					B.strUserName AS creator,
					intApproveLevelStart,
					mst_locations.intCompanyId,
					mst_locations.strName as productionLocation,
					trn_orderheader.intCreator,
					trn_orderheader.intLocationId,
					if(mst_customer_po_techniques.ID >0 , mst_customer_po_techniques.NAME , 'None') as Technique,
					trn_orderheader.INSTANT_ORDER 
				FROM
					trn_orderheader
					Inner Join mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
					Inner Join mst_customer_locations_header ON mst_customer_locations_header.intId = trn_orderheader.intCustomerLocation
					Inner Join mst_financecurrency ON mst_financecurrency.intId = trn_orderheader.intCurrency
					Inner Join sys_users AS A ON A.intUserId = trn_orderheader.intMarketer
					Inner Join sys_users AS B ON B.intUserId = trn_orderheader.intCreator
					Inner Join mst_locations on mst_locations.intId = trn_orderheader.intLocationId
					left join mst_financepaymentsterms on mst_financepaymentsterms.intId=trn_orderheader.intPaymentTerm
					left join mst_customer_po_techniques on mst_customer_po_techniques.ID=trn_orderheader.TECHNIQUE_TYPE
				WHERE
					trn_orderheader.intOrderNo 		=  '$orderNo' AND
					trn_orderheader.intOrderYear 	=  '$orderYear'
					";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$intCreateCompanyId		= $row['intCompanyId'];
		$intCreateUserId		= $row['intCreator'];
		$strCustomerPoNo 		= $row['strCustomerPoNo'];
		$dtDate 				= $row['dtDate'];
		$locationId				= $row['intLocationId'];
		$productionLocation     = $row['productionLocation'];
		
		$dtDeliveryDate 		= $row['dtDeliveryDate'];
		$strCustomer 			= $row['strCustomer'];
		$strCustomerLocations 	= $row['strCustomerLocations'];
		$currencyCode 			= $row['currencyCode'];
		$currencySymbol			= $row['strSymbol'];
		$paymentTerm 			= $row['paymentTerm']." days";
		$strContactPerson		= $row['strContactPerson'];
		$rejectReason 			= $row['REJECT_REASON'];
		$strRemark 				= $row['strRemark'];
		$marketer				= $row['marketer'];
		$intStatus				= $row['intStatus'];
		$creator				= $row['creator'];
		$intApproveLevelStart	= $row['intApproveLevelStart']-1;
		$savedLevels			= $row['intApproveLevelStart'];
		$reviseNo 				= $row['intReviseNo'];
		$technique 				= $row['Technique'];
		$instant				= $row['INSTANT_ORDER'];

$confirmationMode=loadConfirmatonMode($programCode,$intStatus,$savedLevels,$userId);
$rejectionMode=loadRejectionMode($programCode,$intStatus,$savedLevels,$userId);
$reviseMode=loadReviseMode($programCode,$intStatus,$savedLevels,$userId);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Purchase Order Report</title>
<script type="text/javascript" src="presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder-js.js?n=1"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:237px;
	top:175px;
	width:619px;
	height:235px;
	z-index:1;
}
.APPROVE {
	font-size: 16px;
	font-weight: bold;
	color: #00C;
}
</style>
</head>

<body>
<?php
/*BEGIN - OLD FUNCTIONS {
$placeOrderApproveLevel = (int)getApproveLevel('Place Order');
$placeOrderApproveLevel=$placeOrderApproveLevel+1;
$approveLevel = $savedLevels;
END } */

$obj_sys_approvelevels->set($programCode);
$placeOrderApproveLevel	= (int)$obj_sys_approvelevels->getintApprovalLevel();
$placeOrderApproveLevel	= $placeOrderApproveLevel+1;
$approveLevel			= $savedLevels;
 
if(($intStatus > 1) && ($intStatus <=$placeOrderApproveLevel))//pending
{
?>
<div id="apDiv1" style="display:none"><img src="images/pending.png" width="624"  /></div>
<?php
}
if($intStatus==-2)//cancel
{
?>
<div id="apDiv1" style="display:none"><img src="images/cancelled.png" width="624"  /></div>
<?php
}
?>
<form id="frmSamplePlaceOrderReport" name="frmSamplePlaceOrderReport" method="post" action="rptBulkOrder.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>
<tr>
<td colspan="3"><div id="divPoNo" style="display:none"><?php echo $orderNo ?>/<?php echo $orderYear ?></div></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>CUSTOMER PURCHASE ORDER</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="10" align="center" bgcolor="#FFFFFF">
    <?php 
	if(($approveMode==1) && ($confirmationMode==1) && ($intStatus>1) && ($intStatus <= ($savedLevels+1))){//approve button
 			?>
			<img src="images/approve.png" align="middle" class="mouseover noPrint" id="imgApprove" /><?php
 	}
	if((($approveMode==1) && ($confirmationMode==1) && ($intStatus>1) && ($intStatus <= ($savedLevels+1)))
	 || ($intStatus==$savedLevels) && ($intCreateUserId==$userId))
	{//approve button
			?><img src="images/reject.png" align="middle" class="mouseover noPrint" id="imgReject" /><?php
  	}
  else if(($revise==1) && ($reviseMode==1) && ($intCreateUserId!=$userId)){?><img src="images/revise_new.png" align="middle" class="butRevise" id="butRevise"><?php } ?>	
    <div style="display:none"><?php echo "((".$confirmationMode."==1) && ((".$savedLevels."+1)==".$intStatus.") && (".$intCreateUserId."==".$userId.")) || ((".$confirmationMode."==1) && (".$savedLevels.">= ".$intStatus.") && (".$intCreateUserId."!=".$userId."))"; ?></div></td>
  </tr>
  <tr>
  <?php
	$savedStat = $intApproveLevelStart;
 	if($intStatus==1)
	{
	?>
   <td colspan="9" align="center" class="APPROVE" style="color:#090;font-size:30px">CONFIRMED</td>
   <?PHP
	}
	else if($intStatus==0)
	{
   ?>
   <td colspan="9" align="center" class="APPROVE" style="color:#F00;font-size:30px">REJECTED</td>
   <?php
	}
	else if($intStatus==-10)
	{
   ?>
   <td colspan="9" align="center" class="APPROVE" style="color:#49C8C6;font-size:30px">COMPLETED</td>
   <?php
	}
	else if($intStatus==-1)
	{
   ?>
   <td colspan="9" align="center" class="APPROVE" style="color:#F00;font-size:30px">REVISED</td>
   <?php
	}
	else if($intStatus==-2)
	{
   ?>
   <td colspan="9" align="center" class="APPROVE" style="color:#F00;font-size:30px">CANCELLED</td>
   <?php
	}
	  else
	{
   ?>
   <td width="9%" colspan="9" align="center" class="APPROVE" style="color:#00F;font-size:30px" >PENDING</td>
   <?php
	}
   ?>
   <?php
   //-----------------
	  $sqlc = "SELECT
			trn_orderheader_approvedby.intApproveUser,
			trn_orderheader_approvedby.dtApprovedDate,
			sys_users.strUserName,
			trn_orderheader_approvedby.intApproveLevelNo
	
			FROM
			trn_orderheader_approvedby
			INNER JOIN sys_users ON trn_orderheader_approvedby.intApproveUser = sys_users.intUserId
			
			WHERE
			trn_orderheader_approvedby.intOrderNo =  '$orderNo' AND
			trn_orderheader_approvedby.intYear =  '$orderYear' AND
			trn_orderheader_approvedby.intApproveLevelNo =  '$intStatus'
";
	 $resultc = $db->RunQuery($sqlc);
	 $rowc=mysqli_fetch_array($resultc);
	 $rejectBy=$rowc['strUserName'];
	 $rejectDate=$rowc['dtApprovedDate'];

   //-----------------
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="9%" class="normalfnt">Order No</td>
    <td width="1%" align="center" valign="middle">:</td>
    <td width="17%"><span class="normalfnt"><?php echo $orderNo ?>/<?php echo $orderYear ?></span></td>
    <td width="10%" class="normalfnt">Date</td>
    <td width="1%" align="center" valign="middle">:</td>
    <td width="18%"><span class="normalfnt"><?php echo $dtDate ?></span></td>
    <td width="15%" class="normalfnt">Attention To</td>
  <td width="10%" class="normalfnt"><strong>:</strong>&nbsp;<?php echo $strContactPerson; ?></td>
  <?php
  if($ifSumDiff==1 || $ifitemdiff==1)
	{
	?>
	<td style="color:red;" >Attention</td>
	<td> <strong>:</strong>&nbsp;<a href="?q=1266&orderNo= <?php echo $orderNo?>&orderYear=<?php echo $orderYear?>" style="color:red;">check</a></td>

	<?php
	}
	?>
  
 
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Customer</td>
    <td align="center" valign="middle">:</td>
    <td><span class="normalfnt"><?php echo $strCustomer ?></span></td>
    <td width="10%" class="normalfnt">Delivery Date</td>
    <td width="1%" align="center" valign="middle">:</td>
    <td width="18%"><span class="normalfnt"><?php echo $dtDeliveryDate ?></span></td>
    
    <td colspan="2" rowspan="2" class="normalfnt" valign="top"><span <?php echo($intStatus==0?'':'style="display:none"'); ?>><fieldset class="bordered normalfnt" style="width:93%;border-color:#F00">
  	<legend style="color:#F00">Reject Reason:</legend><?php echo $rejectReason; ?></fieldset></span></td>
    </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Cus PO No</td>
    <td align="center" valign="middle">:</td>
    <td class="normalfnt"><?php echo $strCustomerPoNo; ?></td>
    <td class="normalfnt">Customer Location</td>
    <td align="center" valign="middle">:</td>
    <td class="normalfnt"><?php echo $strCustomerLocations; ?></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Currency</td>
    <td align="center" valign="middle">:</td>
    <td class="normalfnt"><?php echo $currencyCode; ?></td>
    <td class="normalfnt">PO Revise No</td>
    <td align="center" valign="middle">:</td>
    <td class="normalfnt"><?php echo $reviseNo; ?></td>
    <td><span class="normalfnt">Marketer</span></td>
    <td align="center" valign="middle">:</td>
    <td><span class="normalfnt"><?php echo $marketer;?></span></td>
    </tr>
<tr>
    <td>&nbsp;</td>
    <td class="normalfnt" valign="top">Payment Term</td>
    <td align="center" valign="top">:</td>
    <td class="normalfnt" valign="top"><?php echo $paymentTerm; ?></td>
    <td class="normalfnt" valign="top">Start Production Without PO</td>
    <td align="center" valign="top">:</td>
    <td class="normalfnt" valign="top"><?php if($instant=='1'){echo 'Yes';} else {echo 'No';} ?></td>
    <td class="normalfnt">Production Location</td>
    <td align="center" valign="middle">:</td>
    <td class="normalfnt"><?php echo $productionLocation; ?></td>
    </tr>
<tr>
  <td>&nbsp;</td>
  <td colspan="6" valign="top" class="normalfnt"><a href="?q=1275&orderNo= <?php echo $orderNo?>" id="butAddEmailsDisp" target="new" class="button red meedium" title="Click here to save order" name="butNew" >Add Dispatch Emails Recipients</a></td>
  <td colspan="2" class="normalfnt" valign="top">&nbsp;</td>
  <td class="normalfnt" valign="top">&nbsp;</td>
  <td align="center" valign="top">&nbsp;</td>
  <td class="normalfnt" valign="top">&nbsp;</td>
</tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td colspan="7" class="normalfnt">
          <table width="806" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="">
              <th width="8%">Line No</th>
               <th width="8%" >Material Code</th>
              <th width="8%"  height="22">Sales Order No</th>
              <th width="12%" >Graphic No</th>
              <th width="9%" >Style No</th>
              <th width="9%" >Sample No</th>
              <th width="10%" >Placement</th>
              <th width="10%" >Part</th>
              <th width="7%" >Print Combo</th>
              <th width="8%" >Ground Color</th>
              <th width="8%" >Rev No</th>
              <th width="8%" >Brand</th>
              <th width="8%" >Image</th>
              <th width="7%" >Technique Group</th>
              <th width="7%" >Qty </th>
              <th width="6%" >Size</th>
              <th width="6%" >Qty</th>
              <th width="6%" >Received Qty</th>
              <th width="6%" >Price</th>
              <th width="6%" >Value</th>
              <th width="7%" >Over Cut %</th>
              <th width="6%" >Damage %</th>
              <th width="7%" >PSD</th>
              <th width="7%" >Delivery Date</th>
              <th width="7%" >HS Pressing</th>
              <th width="7%" >Grading</th>
              </tr>
            <?php 
			$tmpSalesId		= '';
			$tmpGraphic		= '';
			$tmpSampleNo	= '';
			$tmpSampleYear	= '';
			$tmpPlacement	= '';
			$tmpCombo		= '';
			$tmpRevNo		= '';
			$totalSizeQty	= 0;
			$totalAmount	= 0;
			
	  	 $sql1 = "	SELECT 
		 				trn_orderdetails.strLineNo,
						trn_orderdetails.strSalesOrderNo,
						trn_orderdetails.intSalesOrderId, 
						trn_orderdetails.strGraphicNo,
						trn_orderdetails.strStyleNo,
						trn_orderdetails.intSampleNo,
						trn_orderdetails.intSampleYear,
						trn_orderdetails.strCombo,
						trn_orderdetails.strPrintName,
						trn_orderdetails.intQty,
						trn_orderdetails.dblPrice,
						trn_orderdetails.splitedPrice,
						trn_orderdetails.dblOverCutPercentage,
						trn_orderdetails.dblDamagePercentage,
						trn_orderdetails.intRevisionNo,
						trn_orderdetails.dtPSD,
						trn_orderdetails.dtDeliveryDate, 
						trn_orderdetails.ITEM_CODE_FROM_CUSTOMER,
						trn_ordersizeqty.strSize,
						trn_ordersizeqty.dblQty as sizeQty ,
						mst_part.strName as partName , 
						mst_technique_groups.TECHNIQUE_GROUP_NAME AS STECHNIQUE_GRP,
						trn_orderdetails.SO_TYPE,
						trn_orderdetails.IS_GRADING
						FROM
						trn_orderdetails
						left Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId 
						left Join mst_part on mst_part.intId = trn_orderdetails.intPart 
						LEFT JOIN mst_technique_groups ON trn_orderdetails.TECHNIQUE_GROUP_ID= mst_technique_groups.TECHNIQUE_GROUP_ID 
						WHERE
						trn_orderdetails.intOrderNo 	=  '$orderNo' AND
						trn_orderdetails.intOrderYear 	=  '$orderYear' AND 
						trn_orderdetails.SO_TYPE > -1
					";
			$result1 = $db->RunQuery($sql1);
			while($row=mysqli_fetch_array($result1))
			{
						$rcvQty=getRcvQty($orderNo,$orderYear,$row['intSalesOrderId'],$row['strSize']);
						$groundColor=getGroundColor($row['intSampleNo'],$row['intSampleYear'],$row['strPrintName'],$row['strCombo'],$row['intRevisionNo']);
						if($rcvQty==''){
							$rcvQty=0;
						}
                        $row['dblPrice'] = $row['dblPrice'] + $row['splitedPrice'];
                       	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
            <?php
			if(($tmpSalesId!=$row['intSalesOrderId']) || ($tmpGraphic!=$row['strGraphicNo']) || ($tmpSampleNo!=$row['intSampleNo']) || ($tmpSampleYear!=$row['intSampleYear']) ||($tmpPlacement!=$row['strPrintName']) || ($tmpCombo!=$row['strCombo']) || ($tmpRevNo!=$row['intRevisionNo'])
){
			?>
              <td class="normalfnt" ><?php echo $row['strLineNo'] ?></td>
               <td class="normalfntMid" >&nbsp;<?php echo $row['ITEM_CODE_FROM_CUSTOMER'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strSalesOrderNo'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strGraphicNo'] ?>&nbsp;</td>
              <td class="normalfnt" ><?php echo $row['strStyleNo'] ?></td>
              <td class="normalfntRight" ><?php echo $row['intSampleYear'].'/'. $row['intSampleNo'] ?></td>
              <td class="normalfntMid" ><?php echo $row['strPrintName'] ?></td>
               <td class="normalfntMid" ><?php echo $row['partName'] ?></td>
              <td >&nbsp;<?php echo $row['strCombo'] ?>&nbsp;</td>
              <td class="normalfntMid" ><?php echo $groundColor ?></td>
              <td class="normalfntMid" >&nbsp;<?php echo $row['intRevisionNo'] ?>&nbsp;</td>
               <td class="normalfntMid" >&nbsp;<?php echo getBrand($row['intSampleNo'],$row['intSampleYear'],$row['intRevisionNo']); ?>&nbsp;</td>
               <td class="normalfntMid" ><img style="width:100px;height:100px" src="<?php echo "documents/sampleinfo/samplePictures/".$row['intSampleNo']."-".$row['intSampleYear']."-".$row['intRevisionNo']."-".substr($row['strPrintName'],6,2).'.png' ?>" /></td>
              <td class="normalfnt"><?php echo $row['STECHNIQUE_GRP'] ?>&nbsp;</td>
              <td class="normalfntRight" style="font-weight:bold" ><?php echo $row['intQty'] ?>&nbsp;</td>
            <?php
			}
			else{
			?>
              <td class="normalfnt" >&nbsp;&nbsp;</td>
              <td class="normalfnt" >&nbsp;&nbsp;</td>
              <td class="normalfnt" >&nbsp;&nbsp;</td>
              <td class="normalfnt" >&nbsp;&nbsp;</td>
              <td class="normalfntRight" ></td>
              <td class="normalfntMid" ></td>
              <td >&nbsp;&nbsp;</td>
              <td class="normalfntMid" >&nbsp;&nbsp;</td>
              <td class="normalfntRight" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;</td>
               <td class="normalfntRight" >&nbsp;</td>
               <td class="normalfntRight" >&nbsp;</td>
               <td class="normalfntRight" >&nbsp;</td>
               <td class="normalfntRight" >&nbsp;</td>
            <?php
			}
			?>
              <td class="normalfntMid" ><?php echo $row['strSize'] ?></td>
              <td class="normalfntRight" ><?php echo $row['sizeQty'];$totalSizeQty +=$row['sizeQty']; ?></td>
              <td class="normalfntRight" ><?php echo $rcvQty ?></td>
              
              
            <?php
			if(($tmpSalesId!=$row['intSalesOrderId']) || ($tmpGraphic!=$row['strGraphicNo']) || ($tmpSampleNo!=$row['intSampleNo']) || ($tmpSampleYear!=$row['intSampleYear']) ||($tmpPlacement!=$row['strPrintName']) || ($tmpCombo!=$row['strCombo']) || ($tmpRevNo!=$row['intRevisionNo'])
){
			?>
              <td class="normalfntRight" style="visibility:<?php echo $str_price_view; ?>" ><?php echo $row['dblPrice'] ?>&nbsp;</td>
              <td class="normalfntRight" style="visibility:<?php echo $str_price_view; ?>" ><?php echo $row['intQty']*$row['dblPrice'];$totalAmount += $row['intQty']*$row['dblPrice']; ?>&nbsp;</td>
              <td class="normalfntMid" ><?php echo $row['dblOverCutPercentage'] ?></td>
              <td class="normalfntMid" ><?php echo $row['dblDamagePercentage'] ?></td>
              <td class="normalfnt" ><?php echo $row['dtPSD'] ?></td>
              <td class="normalfnt" ><?php echo $row['dtDeliveryDate'] ?></td>
                <?php
                if ($row['SO_TYPE'] ==2){
                    echo '<td class="normalfnt" >YES</td>';
                } else {
                    echo '<td class="normalfnt" >NO</td>';
                }
                if ($row['IS_GRADING'] ==1){
                    echo '<td class="normalfnt" >YES</td>';
                } else {
                    echo '<td class="normalfnt" >NO</td>';
                }

                ?>
            <?php
			}
			else{
			?>
              <td class="normalfntRight" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;</td>
              <td class="normalfntMid" ></td>
              <td class="normalfntMid" ></td>
              <td class="normalfnt" ></td>
              <td class="normalfnt" ></td>
              <td class="normalfnt" ></td>
              <td class="normalfnt" ></td>
              <td class="normalfnt" ></td>
            <?php
			}
			?>
              
              </tr>
            <?php   
			
			
			
			
			$tmpSalesId=$row['intSalesOrderId'];
			$tmpGraphic=$row['strGraphicNo'];
			$tmpSampleNo=$row['intSampleNo'];
			$tmpSampleYear=$row['intSampleYear'];
			$tmpPlacement=$row['strPrintName'];
			$tmpCombo=$row['strCombo'];
			$tmpRevNo=$row['intRevisionNo'];
			}
	  ?>
            <tr><td colspan="38">
            <table width="806" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            	<tr>
                	<th width="20%">Total Panels :</th>
                    <th><?php echo $totalSizeQty; ?></th>
                    <th width="20%">&nbsp;</th>
                    <th width="20%">Total Amount :</th>
                    <th align="right"><?php echo $currencySymbol.' '.$totalAmount ?></th>
                    <th width="20%">&nbsp;</th>
               
                </tr>
            </table>
            </td></tr>
            </table>
        </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>

<tr>
                <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="56%" height="23" bgcolor="" class="normalfnt"><b>Enter By</b> - <?php echo $creator;?></td>
                    <td width="38%" bgcolor="" class="normalfnt">&nbsp;</td>
                    <td width="6%" class="normalfnt">&nbsp;</td>
                  </tr>
<?php  
					 $sqlc = "SELECT
							trn_orderheader_approvedby.intApproveUser,
							trn_orderheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							trn_orderheader_approvedby.intApproveLevelNo
							FROM
							trn_orderheader_approvedby
							Inner Join sys_users ON trn_orderheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							trn_orderheader_approvedby.intOrderNo =  '$orderNo' AND
							trn_orderheader_approvedby.intYear =  '$orderYear'      order by dtApprovedDate asc
";
					 $resultc = $db->RunQuery($sqlc);
					while($rowc=mysqli_fetch_array($resultc)){
						if($rowc['intApproveLevelNo']==1)
						$desc="1st Approved By ";
						else if($rowc['intApproveLevelNo']==2)
						$desc="2nd Approved By ";
						else if($rowc['intApproveLevelNo']==3)
						$desc="3rd Approved By ";
						else
						$desc=$rowc['intApproveLevelNo']."th Approved By ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
					 
					 if($rowc['intApproveLevelNo']>0){
						 $flag=1;
				?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?>  - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
            </tr>
<?php
	//}
	//else{			
					}
					 if($rowc['intApproveLevelNo']==0){
						 $flag=2;
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt" style="color:#FF8040"><strong> Rejected By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
					 }
					 if($rowc['intApproveLevelNo']==-1){
						 $flag=3;
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt" style="color:#CCCC66"><strong> Revised By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
					 }
					 if($rowc['intApproveLevelNo']==-10){
						 $flag=4;
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt" style="color:#00CC66"><strong> Completed By - </strong></span><span class="normalfnt" style="color:#FF5E5E"><strong><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></strong></span></td>
            </tr>
<?php
					 }
					 
					  if($rowc['intApproveLevelNo']==-2){
						 $flag=4;
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt" style="color:#FF0000;font-size:12px"><strong><b>Cancel By - </b></strong></span><span class="normalfnt" style="color:#FF5E5E"><strong><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></strong></span></td>
            </tr>
<?php
					 }
					 
					 
		 if($rowc['intApproveLevelNo']>0){
			 $finalSavedLevel=$rowc['intApproveLevelNo'];
		 }
	 }//end of while
	
	
	//echo $flag;
	if($flag>1){
		$finalSavedLevel=0;
	}
	
	if(($flag<=1) || ($intStatus>0)){
	for($j=$finalSavedLevel+1; $j<=$savedLevels; $j++)
	{ 
		if($j==1)
		$desc="1st ";
		else if($j==2)
		$desc="2nd ";
		else if($j==3)
		$desc="3rd ";
		else
		$desc=$j."th ";
		$desc2='---------------------------------';
	?>
        <tr>
            <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
        </tr>
<?php
	}
	}
?>

<tr><td height="40" ></td></tr>
<tr height="40" >
<?Php 
	$url  = "presentation/sendToApproval.php";		// * file name
	$url .= "?status=$intStatus";										// * set recent status
	$url .= "&approveLevels=$approveLevel";								// * set approve levels in order header
	$url .= "&programCode=$programCode";								// * program code (ex:P10001)
	$url .= "&program=CUSTOMER PO";									// * program name (ex:Purchase Order)
	$url .= "&companyId=$companyId";
	$url .= "&createUserId=$intCreateUserId";							 
	$url .= "&field1=Order No";												 
	$url .= "&field2=Order Year";	
	$url .= "&value1=$orderNo";												 
	$url .= "&value2=$orderYear";	
	$url .= "&field3=Customer PO";												 
	$url .= "&value3=$strCustomerPoNo";	
	
	$url .= "&subject=CUSTOMER PO FOR APPROVAL ('$orderNo'/'$orderYear')";	
	
	$url .= "&statement1=Please Approve this";	
	$url .= "&statement2=to Approve this";	
	
	//$_REQUEST['link']=base64_encode($_SESSION['MAIN_URL']."?q=896&orderNo=$serialNo&orderYear=$year&approveMode=1"); 
	$url .= "&link=".base64_encode($_SESSION['MAIN_URL']."?q=896&orderNo=$orderNo&orderYear=$orderYear&approveMode=1"); 
	//$url .= "&link=".urlencode(base64_encode("presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder.php?orderNo=$orderNo&orderYear=$orderYear&approveMode=1"));
/*edit mode for the doc*/
if($savedLevels +1 == $intStatus){
    $editMode = 1;
}else {
    $editMode = 0;
}
?>
  <td align="center" style="vertical-align:top" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:150px;border:none"  ></iframe></td>
  <td align="center" class="normalfntMid"><iframe  id="iframeFiles" src="presentation/customerAndOperation/bulk/placeOrder/addNew/filesUpload.php?txtFolder=<?php echo "$orderNo-$orderYear".'&editMode='.$editMode; ?>" name="iframeFiles" style="width:400px;height:175px;border:none;" class="noPrint"  ></iframe></td>
</tr>
<tr height="40" >
  <td align="center" class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></td>
  <td align="center" class="normalfntMid">&nbsp;</td>
</tr>
                </table> 
                
   </td></tr></table></div>     
</form>
</body>
</html>
<?php
//-----------------------------------if sum difference----------------------------------------
function check_for_previous_PRI($orderNo,$orderYear)
{ 
		global $db;
	 $sql_select_revision =
							"SELECT
							trn_orderheader.intReviseNo
							FROM
							trn_orderheader
							WHERE
							trn_orderheader.intOrderNo = $orderNo
							AND
							trn_orderheader.intOrderYear = $orderYear ";
							//echo $sql_select_revision;
							$result	 	= $db->RunQuery($sql_select_revision);
							$row		= mysqli_fetch_array($result);
							$revision	= $row['intReviseNo'];
							
		$select_sum_previous			=
							"SELECT SUM(trn_po_prn_details_history.REQUIRED) as sum_pri
							FROM
							trn_po_prn_details_history
							WHERE
							trn_po_prn_details_history.ORDER_NO = $orderNo
							AND
							trn_po_prn_details_history.ORDER_YEAR = $orderYear
							AND
							trn_po_prn_details_history.REVISION = $revision - 1";	
							
							$result_sum_previous	= $db->RunQuery($select_sum_previous);
							$row					= mysqli_fetch_array($result_sum_previous);
					       	$sum_pri_previous		= $row['sum_pri'];	

		$select_sum_current			=
							"SELECT SUM(trn_po_prn_details.REQUIRED) as sum_pri
							FROM
							trn_po_prn_details
							WHERE
							trn_po_prn_details.ORDER_NO = $orderNo
							AND
							trn_po_prn_details.ORDER_YEAR = $orderYear";
							 	
							$result_sum_current	= $db->RunQuery($select_sum_current);
							$row					= mysqli_fetch_array($result_sum_current);
							$sum_pri_current		= $row['sum_pri'];	
							
		
							if(($sum_pri_current < $sum_pri_previous))
								{   
									return 1;
				
								}
}

//------------------------------------------------------------------------

function check_for_previous_PRI_for_item($orderNo,$orderYear)
{       
        global $db;
		
		$sql_select_revision =
							"SELECT
							trn_orderheader.intReviseNo
							FROM
							trn_orderheader
							WHERE
							trn_orderheader.intOrderNo = $orderNo
							AND
							trn_orderheader.intOrderYear = $orderYear ";
							//echo $sql_select_revision;
							$result	 	= $db->RunQuery($sql_select_revision);
							$row		= mysqli_fetch_array($result);
							$revision	= $row['intReviseNo'];
		
		$item_count_current = 
							"SELECT COUNT(trn_po_prn_details.ITEM) as count
							FROM
							trn_po_prn_details
							WHERE
							trn_po_prn_details.ORDER_NO = $orderNo
							AND
							trn_po_prn_details.ORDER_YEAR = $orderYear";
							//echo $item_count_current;
							$result_count			= $db->RunQuery($item_count_current);
							$row					= mysqli_fetch_array($result_count);
							$count_current_item		= $row['count'];
		
		
		$item_count_previous = 
							"SELECT *
							FROM
							trn_po_prn_details_history
							WHERE
							trn_po_prn_details_history.ORDER_NO = $orderNo
							AND
							trn_po_prn_details_history.ORDER_YEAR = $orderYear
							AND
							trn_po_prn_details_history.REVISION = $revision - 1";
							
							$result_count			= $db->RunQuery($item_count_previous);
							while ($row					= mysqli_fetch_array($result_count))
							{
								$previous_item	= $row['ITEM'];
								$item_count_current = 
									"SELECT *
									FROM
									trn_po_prn_details
									WHERE
									trn_po_prn_details.ORDER_NO = $orderNo
									AND
									trn_po_prn_details.ORDER_YEAR = $orderYear
									AND
									trn_po_prn_details.ITEM 	=	$previous_item ";
									 
									$result_count			= $db->RunQuery($item_count_current);
									$row					= mysqli_fetch_array($result_count);
									$current_item			= 	$row['ITEM'];
									$current_qty			=	$row['REQUIRED'];
									
									if($current_item!=""&& $current_qty==0)
											{ 
												return 1;
											}
		
		
							}
		
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//-------------------------------------------------------------------------
function getRcvQty($orderNo,$year,$salesOrderNo,$size){
	global $db;
	 $sql1 = "SELECT
Sum(ware_fabricreceiveddetails.dblQty) AS RcvQty
FROM
ware_fabricreceivedheader
Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
WHERE
ware_fabricreceivedheader.intOrderNo =  '$orderNo' AND
ware_fabricreceivedheader.intOrderYear =  '$year' AND
ware_fabricreceivedheader.intStatus =  '1' AND
ware_fabricreceiveddetails.intSalesOrderId =  '$salesOrderNo' AND
ware_fabricreceiveddetails.strSize =  '$size'
GROUP BY
ware_fabricreceivedheader.intOrderNo,
ware_fabricreceivedheader.intOrderYear,
ware_fabricreceiveddetails.intSalesOrderId,
ware_fabricreceiveddetails.strSize";	
	$result1 = $db->RunQuery($sql1);
	$row1=mysqli_fetch_array($result1);
	$RcvQty=$row1['RcvQty'];
	return $RcvQty;
}
//------------------------------function loadReviseMode-------------------
function loadReviseMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$reviseMode=0;
	$sqlp = "SELECT
		menupermision.intRevise 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intRevise']==1){
	 if(($intStatus<=$savedStat) && ($intStatus!=0)){
	 $reviseMode=1;
	 }
	}
	 
	return $reviseMode;
}
//-----------------------------------------------------------
	function getGroundColor($sampNo,$sampleYear,$printName,$combo,$revNo)
{
		global $db;
		$sql = "SELECT DISTINCT
				trn_sampleinfomations_details.intGroundColor,
				mst_colors_ground.strName
				FROM
					trn_sampleinfomations_details 
					Inner Join mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
				WHERE
					trn_sampleinfomations_details.intSampleNo =  '$sampNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear'  AND
					trn_sampleinfomations_details.strPrintName =  '$printName' AND
					trn_sampleinfomations_details.strComboName =  '$combo' AND
					trn_sampleinfomations_details.intRevNo =  '$revNo'
					
					
					
				";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$g= $row['strName'];	
		if($revNo=='') 
		$g='';	
		return $g;	
}
function getBrand($sampleNo,$sampleYear,$revNo)
{
	global $db;
	$sql	= 	"SELECT
				mst_brand.strName,
				mst_brand.intId
				FROM
				trn_sampleinfomations
				INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
				WHERE
				trn_sampleinfomations.intSampleNo = '$sampleNo' AND
				trn_sampleinfomations.intSampleYear = '$sampleYear' AND
				trn_sampleinfomations.intRevisionNo = '$revNo'";
	$result_brand = $db->RunQuery($sql);
	$row_brand	  = mysqli_fetch_array($result_brand);
	$brand		  = $row_brand['strName'];
	return $brand;
}
//-----------------------------------------------------------
?>	

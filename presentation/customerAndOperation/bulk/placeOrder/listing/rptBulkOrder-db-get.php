<?php
//ini_set('display_errors',1);
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
    $loginnedLocationId 	= $_SESSION['CompanyID'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once $_SESSION['ROOT_PATH']."class/customerAndOperation/cls_textile_stores.php";
	require_once $_SESSION['ROOT_PATH']."class/customerAndOperation/bulk/LC/cls_lc_get.php";
	
	$objtexttile 	= new cls_texttile();
	$obj_lc_get		= new cls_lc_get($db);
	$programName='Place Order';
	$programCode='P0427';
	$approveLevel = (int)getApproveLevel($programName);

$sql = "SELECT 	mst_locations.intCompanyId
		FROM mst_locations WHERE mst_locations.intId='$loginnedLocationId'";

$result 	 = $db->RunQuery($sql);
$errorFlg	 = 0;
$row		 = mysqli_fetch_array($result);
$loginMainCompany 	 = $row['intCompanyId'];
	

 if($requestType=='getValidation')
	{
		$PONo  = $_REQUEST['PONo'];
		$PONoArray=explode("/",$PONo);
		$PONo=$PONoArray[0];
		$PONoYear=$PONoArray[1];

 		///////////////////////
		$sql = "SELECT 
		trn_orderheader.intStatus, 
		trn_orderheader.intApproveLevelStart, 
		trn_orderheader.strCustomerPoNo, 
		mst_locations.intCompanyId,
		Count(trn_orderdetails.strSalesOrderNo) as count, 
		trn_orderheader.intLocationId ,
		trn_orderheader.PO_TYPE,
		trn_orderheader.dtmCreateDate 
		FROM trn_orderheader 
		Left Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
		LEFT JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
		WHERE
		trn_orderheader.intOrderNo =  '$PONo' AND
		trn_orderheader.intOrderYear =  '$PONoYear'";

		$result 	 = $db->RunQuery($sql);
		$errorFlg	 = 0;
		$row		 = mysqli_fetch_array($result);
		$companyId 	 = $row['intLocationId'];
		$poRaisedMainCompany= $row['intCompanyId'];
		$ctratedTime = $row['dtmCreateDate'];
		$confirmatonMode=loadConfirmatonMode($programCode,$row['intStatus'],$row['intApproveLevelStart'],$userId);
		$colorRecipyFlag=loadColorRecipyFlag($PONo,$PONoYear);
		$nextAppLevel=$row['intApproveLevelStart']+2-$row['intStatus'];
		$poType		= $row['PO_TYPE'];
		
		if($row['count']==0){// 
			$errorFlg = 1;
			$msg ="No Details to Approve"; 
		}
		else if(($row['strCustomerPoNo']=='') && ($row['intStatus']<=$row['intApproveLevelStart'])){// 
			$errorFlg = 1;
			$msg ="Please enter the Customer PO No, Before confirm this."; 
		}
		else if($row['intStatus']==1){// 
			$errorFlg = 1;
			$msg ="Final confirmation of this PO is already raised"; 
		}

        else if($poRaisedMainCompany!=$loginMainCompany){//
            $errorFlg = 1;
            $msg ="You cannot approve POs raised from any other company";
        }
/*		else if(($nextAppLevel==2) && ($colorRecipyFlag==0)){// 
			$errorFlg = 1;
			$msg ="Pls add Items for color recepies"; 
		}
*/		else if($row['intStatus']==0){// 
			$errorFlg = 1;
			$msg ="This PO is rejected"; 
		}
		else if($confirmatonMode==0){// 
			$errorFlg = 1;
			$msg ="No Permission to Approve"; 
		}
		else if((!checkSizes($PONo,$PONoYear)) && ($row['intStatus']<=$row['intApproveLevelStart'])){
				$msg ="Size wise quantites should be updated."; 
				$errorFlg = 1;
		}
		else if((checkSizesWiseQuantitiesRecordes($PONo,$PONoYear) !='' && ($row['intStatus']<=$row['intApproveLevelStart']))){
				$msg ="Total size wise quantities should be equal to the relevent sales order's quantities for following orders, </br> ".checkSizesWiseQuantitiesRecordes($PONo,$PONoYear); 
				$errorFlg = 1;
		}
		else{
			$sql2  = "	SELECT
							trn_orderdetails.strSalesOrderNo,intSampleNo, 
							trn_orderdetails.strSalesOrderNo,intSampleYear, 
							trn_orderdetails.strSalesOrderNo,intRevisionNo, 
							trn_orderdetails.strSalesOrderNo,intSalesOrderId, 
							trn_orderdetails.strSalesOrderNo,strCombo, 
							trn_orderdetails.strSalesOrderNo,strPrintName, 
							trn_orderdetails.intQty 
						FROM trn_orderdetails
						WHERE
							trn_orderdetails.intOrderNo 	=  '$PONo' AND
							trn_orderdetails.intOrderYear 	=  '$PONoYear' AND trn_orderdetails.SO_TYPE > -1
						ORDER BY
							trn_orderdetails.intSalesOrderId ASC
						";
			$result2 = $db->RunQuery($sql2);
			$r=0;
			while($row2 = mysqli_fetch_array($result2))
			{ 
				$sampleRoomStatus	= getSampleRoomStatus($row2['intSampleNo'],$row2['intSampleYear'],$row2['intRevisionNo'],$row2['strCombo'],$row2['strPrintName']);
				$rcvSQty=getSalesOrderWiseRcvQty($PONo,$PONoYear,$row2['intSalesOrderId']);
				$fDQty  = loadDamageReturnedQty($companyId,$PONo,$PONoYear,$row2['intSalesOrderId']);
				$salesNo = $row2['strSalesOrderNo'];
				$salesId = $row2['intSalesOrderId'];
				//$excessQty=loadExcessQty($PONo,$PONoYear,$row2['intSalesOrderId']);
				$toleratePercentage	= $objtexttile->getSavedTolerencePercentage($PONo,$PONoYear,$row2['intSalesOrderId']);
				$excessQty=ceil(loadSalesOrderWiseExcessQty($PONo,$PONoYear,$row2['intSalesOrderId'],$toleratePercentage));
				
				$flag_new_revision = get_new_revision_status($row2['intSampleNo'],$row2['intSampleYear'],$row2['intRevisionNo']);
				$flag_fab_arrived	=getFabArrivedFlag($PONo,$PONoYear,$row2['intSalesOrderId']);
				
				if($flag_fab_arrived ==0 && $flag_new_revision==1 && $ctratedTime > '2017-08-01 12:00:00'){
					$errorFlg	=	1;
					$msg	=	"There is a new revision for  ".$row2['intSampleNo'].'/'.$row2['intSampleYear'].'('.'/'.$row2['strPrintName'].'/'.$row2['strCombo'].')';
				}
				
				if($sampleRoomStatus!=1){
					$errorFlg		=	1;
					$msg			=	"Sample room approval should be raised for sample (".$row2['intSampleYear']."/".$row2['intSampleNo']."/".$row2['intRevisionNo']."/".$row2['strCombo']."/".$row2['strPrintName'].")";
				}
	
				$child_flag	= getChildFlag($PONo ,$PONoYear,$salesId);
 				if($poType==1 && $child_flag ==1){
					$dummyErr	= getDummyQtyBalance($PONo ,$PONoYear,$salesId,$row2['intQty']);
					if($dummyErr['err']==1){
						$errorFlg		=	1;
						$msg			=	"Qty balance of dummy order ".$PONo.'/'.$PONoYear.'('.$salesId.'/'.$printName.'/'.$combo.') should be '.$dummyErr['qty'];
					}
				}				
				
				if($rcvSQty>$row2['intQty']+$excessQty+$fDQty +5){
						// 5 added by roshan for small quntities tolerance
						$msg= 'order Quantities cannot be less than Recieved Quantities without excess.'."Sales order No : $salesNo , Sales Id :$salesId </br> 
						Order Qty 	: ".$row2['intQty']."
						ExQty 		: $excessQty
						FD Qty		: $fDQty	
						Rcv Qty		: $rcvSQty
						
						
						";
						//$errorFlg = 1;
				}
			}
		}
		

	   ///////////////////////
 	   
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}

//---------------------------
else if($requestType=='validateRejecton')
	{
		$PONo  = $_REQUEST['PONo'];
		$PONoArray=explode("/",$PONo);
		$PONo=$PONoArray[0];
		$PONoYear=$PONoArray[1];
		///////////////////////
		$sql = "SELECT 
		trn_orderheader.intCreator, 
		trn_orderheader.intStatus, 
		trn_orderheader.intApproveLevelStart,
		mst_locations.intCompanyId 
		FROM trn_orderheader
		LEFT JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
		WHERE
		trn_orderheader.intOrderNo =  '$PONo' AND
		trn_orderheader.intOrderYear =  '$PONoYear'";

		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$msg =""; 
		$row=mysqli_fetch_array($result);
		$creator	= $row['intCreator'];
		$status		= $row['intStatus'];
		$levels		= $row['intApproveLevelStart'];
        $poRaisedMainCompany= $row['intCompanyId'];
		
		$rejectionMode=loadRejectionMode($programCode,$row['intStatus'],$row['intApproveLevelStart'],$userId);
		
		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this PO is already raised.So cant reject"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This PO is already Rejected"; 
		}
        else if($poRaisedMainCompany!=$loginMainCompany){//
            $errorFlg = 1;
            $msg ="You cannot reject POs raised from any other company";
        }
		else if($rejectionMode==0){//no confirmation has been raised
			if($creator==$userId && $status == $levels){
				$errorFlg = 0;
				$msg =""; 
			}
			else{
				$errorFlg = 1;
				$msg ="No Permission to reject this PO"; 
			}
		}
		else{//confirmation has been raised
			$errorFlg = 0;
			
		}

	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
}
//---------------------------
else if($requestType=='validateRevise')
	{
		$PONo  = $_REQUEST['PONo'];
		$PONoArray=explode("/",$PONo);
		$PONo=$PONoArray[0];
		$PONoYear=$PONoArray[1];
		///////////////////////
		$sql = "	SELECT
								trn_orderheader.intStatus, 
								trn_orderheader.intApproveLevelStart,
								mst_locations.intCompanyId, 
								LC_STATUS
							FROM trn_orderheader LEFT JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
							WHERE
								trn_orderheader.intOrderNo 		=  '$PONo' AND
								trn_orderheader.intOrderYear 	=  '$PONoYear' 
							";
		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$msg =""; 
		$row=mysqli_fetch_array($result);
		
		$reviseMode		= loadReviseMode($programCode,$row['intStatus'],$row['intApproveLevelStart'],$userId);
		$arr_lcStatus	= $obj_lc_get->get_lc_header_for_order($PONo,$PONoYear,'RunQuery');
		$lcStatus		= $arr_lcStatus['STATUS'];
        $poRaisedMainCompany= $row['intCompanyId'];
		
		if($row['intStatus']==0){// 
			$errorFlg = 1;
			$msg ="This PO is rejected.Can't revise"; 
		}
		else if($reviseMode==0){// 
			$errorFlg = 1;
			$msg ="No Permission to revise this PO"; 
		}

        else if($poRaisedMainCompany!=$loginMainCompany){//
            $errorFlg = 1;
            $msg ="You cannot revise POs raised from any other company";
        }
		//elseif($row["LC_STATUS"] && $lcStatus != '0' || $lcStatus != '-2')
		elseif($row["LC_STATUS"]==6 && $lcStatus != '')
		{
			$errorFlg = 1;
			$msg = "Sorry you cannot revise this PO,Active LC available for this PO."; 
		}
		else{// 
			$errorFlg = 0;			
		}
		
		
//$errorFlg = 1;
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
}

//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//----------------------------------------------------
function checkSizes($orderNo,$orderYear)
{
	global $db;
	$sql = "SELECT
				trn_orderdetails.intOrderNo,
				trn_orderdetails.intOrderYear,
				trn_orderdetails.strSalesOrderNo,
				trn_ordersizeqty.strSize,
				trn_ordersizeqty.dblQty
			FROM
			trn_orderdetails
				Left Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_ordersizeqty.intSalesOrderId = trn_orderdetails.intSalesOrderId
			WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				(trn_orderdetails.intQty > 0 and trn_ordersizeqty.strSize IS NULL) 
			";
	$result = $db->RunQuery($sql);
	$rows=mysqli_num_rows($result);
	if($rows>0)
		return false;
	else
		return true;
}
//------------------------------function loadConfirmatonMode-------------------
function loadReviseMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$reviseMode=0;
	$sqlp = "SELECT
		menupermision.intRevise 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intRevise']==1){
	 if(($intStatus<=$savedStat) && ($intStatus!=0)){
	 $reviseMode=1;
	 }
	}
	 
	return $reviseMode;
}

//--------------------------------------------------------
function getSalesOrderWiseRcvQty($orderNo,$orderYear,$salesOrderId){
	global $db;
	
	 $sqlp = "SELECT
Sum(ware_fabricreceiveddetails.dblQty) AS qty
FROM
ware_fabricreceivedheader
Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
WHERE
ware_fabricreceivedheader.intStatus =  '1' AND
ware_fabricreceivedheader.intOrderNo =  '$orderNo' AND
ware_fabricreceivedheader.intOrderYear =  '$orderYear' AND
ware_fabricreceiveddetails.intSalesOrderId =  '$salesOrderId'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	return $rowp['qty'];
}
//-----------------------------------------------------------
	function loadExcessQty($orderNo,$orderYear,$salesOrderId)
{
		global $db;
	         	$sql = "SELECT
				sum(trn_ordersizeqty.dblQty*trn_orderdetails.dblOverCutPercentage+trn_orderdetails.dblDamagePercentage)/100 as qty,
				trn_orderdetails.dblOverCutPercentage,
				trn_orderdetails.dblDamagePercentage
				FROM trn_orderdetails 
				Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_orderdetails.intSalesOrderId =  '$salesOrderId'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		$value=$rows['qty'];
		return val($value);
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	     $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//--------------------------------------------------------
function loadColorRecipyFlag($poNo,$year){
	
	global $db;
	
	 $sql = "SELECT
Count(trn_sample_color_recipes.intColorId) as count 
FROM
trn_orderdetails
Inner Join trn_sample_color_recipes ON trn_orderdetails.intSampleNo = trn_sample_color_recipes.intSampleNo AND trn_orderdetails.intSampleYear = trn_sample_color_recipes.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sample_color_recipes.intRevisionNo AND trn_orderdetails.strCombo = trn_sample_color_recipes.strCombo AND trn_orderdetails.strPrintName = trn_sample_color_recipes.strPrintName
WHERE
trn_orderdetails.intOrderNo =  '$poNo' AND
trn_orderdetails.intOrderYear =  '$year' ";
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		$value=$rows['count'];
		
		if($value>0){
			$value=1;
		}
		
	return $value;
}
//-----------------------------------------------------------
function loadDamageReturnedQty($location,$orderNo,$orderYear,$salesOrderId)
{
		global $db;
		$sql = "SELECT
				IFNULL(sum(ware_stocktransactions_fabric.dblQty*-1),0) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				/*ware_stocktransactions_fabric.intLocationId =  '$location' AND*/
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strType in(  'Dispatched_F' ,'Dispatched_P', 'Dispatched_CUT_RET')
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//--------------------------------------------------------------------
function loadSalesOrderWiseExcessQty($orderNo,$orderYear,$salesOrderId,$excessFactor)
{
		global $db;
	         	$sql = "SELECT
			trn_orderdetails.intQty,
			trn_orderdetails.dblOverCutPercentage,
			trn_orderdetails.dblDamagePercentage
			FROM trn_orderdetails 
			WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_orderdetails.intSalesOrderId =  '$salesOrderId'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		$value=$rows['intQty']*($rows['dblOverCutPercentage']+$excessFactor)/100;
		return val($value);
}
//------------------------------------------------------------------------------
function checkSizesWiseQuantitiesRecordes($orderNo,$orderYear){
	
	global $db;
	$sql = "SELECT
				trn_orderdetails.intOrderNo,
				trn_orderdetails.intOrderYear,
				trn_orderdetails.strSalesOrderNo,
				trn_orderdetails.intSalesOrderId ,
				trn_orderdetails.intQty 
 			FROM
			trn_orderdetails
 			WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear'";
	
	$result = $db->RunQuery($sql);
	$recordes = '';
	while($row=mysqli_fetch_array($result))
	{
		$saleOrderId	=	$row['intSalesOrderId'];
		$orderQty		=	$row['intQty'];
		$sizeQty		= getSieWiseQty($orderNo,$orderYear,$saleOrderId);
		if(round($orderQty) != round($sizeQty) ){
			$recordes .= $orderNo.'/'.$orderYear.'/'.$row['strSalesOrderNo'].'</br>';
		}
	
	}
	
	return $recordes;
	
}
function getSieWiseQty($serialNo,$year,$salesOrderId){
	global $db;
	$sql = "SELECT
			sum(trn_ordersizeqty.dblQty) as sizeWiseTotal
			FROM `trn_ordersizeqty`
			WHERE
			trn_ordersizeqty.intOrderNo = '$serialNo' AND
			trn_ordersizeqty.intOrderYear = '$year' AND
			trn_ordersizeqty.intSalesOrderId = '$salesOrderId'
		";
	$result = $db->RunQuery($sql);
	$rows = mysqli_fetch_array($result);
	return (float)($rows['sizeWiseTotal']);
}

function getDummyQtyBalance($orderNo,$orderYear,$salesOrderId,$qty){

	global $db;
	$errflag		=0;
	$max_qty		=0;

 	$sql_d	= "SELECT
				trn_orderdetails.DUMMY_SO_INITIAL_QTY
				FROM
				trn_orderheader
				INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderheader.intOrderNo = '$orderNo' AND
				trn_orderheader.intOrderYear = '$orderYear' AND
				trn_orderdetails.intSalesOrderId = '$salesOrderId'  
				";				
	$result_d 	= $db->RunQuery($sql_d);
	$row_d		= mysqli_fetch_array($result_d);
	$initial_dummy		= $row_d['DUMMY_SO_INITIAL_QTY'];
	
	$sql	= "SELECT
				ifnull(sum(intQty),0) as qty 
				FROM
				trn_orderheader
				INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderheader.DUMMY_PO_NO = '$orderNo' AND
				trn_orderheader.DUMMY_PO_YEAR = '$orderYear' AND
				trn_orderdetails.DUMMY_SO_ID = '$salesOrderId'   AND 
				 trn_orderheader.intStatus <> -2
				";				
	$result 	= $db->RunQuery($sql);
	$row		= mysqli_fetch_array($result);
	$used_other	= $row['qty'];


	$sql	= "SELECT
				ifnull(sum(intQty),0) as qty 
				FROM
				trn_orderheader
				INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderheader.intOrderNo = '$orderNo' AND
				trn_orderheader.intOrderYear = '$orderYear' AND
				trn_orderdetails.DUMMY_SO_ID = '$salesOrderId'  AND 
				 trn_orderheader.intStatus <> -2 
				";				
	$result 	= $db->RunQuery($sql);
	$row		= mysqli_fetch_array($result);
	$used_same_order	= $row['qty'];

	
	$bal	= $initial_dummy - ($used_other+$used_same_order);
	if($bal < 0)
		$bal	=0;
	
	if($bal != $qty){
		$errflag		=1;
		$max_qty		=$bal;
	}
		
 	
	$arrDummy['err']=$errflag;
	$arrDummy['qty']=$max_qty/*.'/'.$sql_d*//*.' != '.$row_d['DUMMY_SO_INITIAL_QTY'].'-'.$qty*/;
	
	return $arrDummy;
	
}

function getChildFlag($orderNo,$orderYear,$salesOrderId){
	
	global $db;
	$child_count	=0;
 	$sql	= "SELECT
				1 as flag
				FROM
				trn_orderheader
				INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderheader.DUMMY_PO_NO = '$orderNo' AND
				trn_orderheader.DUMMY_PO_YEAR = '$orderYear' AND
				trn_orderdetails.DUMMY_SO_ID = '$salesOrderId' limit 1
				";				
	$result 	= $db->RunQuery($sql);
	$row		= mysqli_fetch_array($result);
	$child_count= $row['flag'];
	return $child_count;
	
}

function getSampleRoomStatus($intSampleNo,$intSampleYear,$intRevisionNo,$combo,$print){
	
	global $db;
	$sql = "SELECT  
			trn_sampleinfomations.intSampleRoomStatus,
			trn_sampleinfomations_combo_print_approvedby.`STATUS`
			FROM trn_sampleinfomations 
			left JOIN trn_sampleinfomations_combo_print_approvedby ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_combo_print_approvedby.SAMPLE_NO 
			AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_combo_print_approvedby.SAMPLE_YEAR 
			AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_combo_print_approvedby.REVISION
			AND trn_sampleinfomations_combo_print_approvedby.COMBO = '$combo'
			AND trn_sampleinfomations_combo_print_approvedby.PRINT = '$print'
			WHERE 
			trn_sampleinfomations.intSampleNo =  '$intSampleNo' AND
			trn_sampleinfomations.intSampleYear =  '$intSampleYear' AND
			trn_sampleinfomations.intRevisionNo =  '$intRevisionNo' 
			";
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	if($row['intSampleRoomStatus']==1 || $row['STATUS']==1){
		return 1;
	}
	else
		return 0;
	
  	
}
function get_new_revision_status($sampleNo,$sampleYear,$revNo){
	
	global $db;

	$sql = "SELECT
			trn_sampleinfomations.intRevisionNo
			FROM `trn_sampleinfomations`
			WHERE
			trn_sampleinfomations.intSampleNo = '$sampleNo' AND
			trn_sampleinfomations.intSampleYear = '$sampleYear' AND
			trn_sampleinfomations.intRevisionNo > '$revNo'
			 ";

	$result = $db->RunQuery($sql);
	$row	=mysqli_fetch_array($result);
	if($row['intRevisionNo'] > $revNo && $row['intRevisionNo'] > 0)
		return 1;
	else
		return 0;
	
}

function getFabArrivedFlag($serialNo ,$year,$salesOrderId){
	global $db;
	$sql	="
			SELECT
			ware_fabricreceivedheader.intFabricReceivedNo
			FROM
			ware_fabricreceivedheader
			INNER JOIN ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
			WHERE
			ware_fabricreceivedheader.intOrderNo = '$serialNo' AND
			ware_fabricreceivedheader.intOrderYear = '$year' AND
			ware_fabricreceiveddetails.intSalesOrderId = '$salesOrderId' AND 
			ware_fabricreceivedheader.intStatus =1
		";
	
		$result 	= $db->RunQuery($sql);
		$row		= mysqli_fetch_array($result);
		
		if($row['intFabricReceivedNo'] >0 && $row['intFabricReceivedNo']!='')
			return 1;
		else
			return 0;
}




?>



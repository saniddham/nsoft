<?php


//ini_set('display_errors',1);
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$userId = $_SESSION['userId'];
$companyId = $_SESSION['CompanyID'];

$programName = 'Place Order';
$programCode = 'P0427';

//ini_set('display_errors',1);
include "{$backwardseperator}dataAccess/Connector.php";
include "../../../../../libraries/mail/mail.php";
require_once $_SESSION['ROOT_PATH'] . "class/cls_mail.php";
require_once $_SESSION['ROOT_PATH'] . "class/cls_permisions.php";
require_once $_SESSION['ROOT_PATH'] . "class/cls_commonFunctions_get.php";
require_once $_SESSION['ROOT_PATH'] . "class/warehouse/cls_warehouse_get.php";
require_once $_SESSION['ROOT_PATH'] . "class/common/azureDBconnection.php";

$objMail = new cls_create_mail($db);
$obj_permision = new cls_permisions($db);
$obj_commom = new cls_commonFunctions_get($db);
$obj_warehouse_get = new cls_warehouse_get($db);
$objAzure = new cls_azureDBconnection();
//$response = array('type'=>'', 'msg'=>'');

/////////// parameters /////////////////////////////
//$requestType 			= $_REQUEST['requestType'];

$orderNo = $_REQUEST['orderNo'];
$orderYear = $_REQUEST['orderYear'];
$samplePriceQuantity = array();

if ($_REQUEST['status'] == 'approve') {
    $db->begin();

    $errorFlag = 0;

    $grading_result = get_grading($orderNo, $orderYear);
    while ($row = mysqli_fetch_array($grading_result)) {
        $grading = $row['IS_GRADING'];
        $sales = $row['intSalesOrderId'];
        if ($grading == 1) {

            $grading_bulk = get_grading_bulk($orderNo, $orderYear, $sales);
            $grading_sample = get_grading_sample($orderNo, $orderYear, $sales);
            $ismatched = is_matched($orderNo, $orderYear, $sales);
            if ($grading_bulk == 1 || $grading_sample == 1) {
                $errorFlag = 1;
                $msg = 'Gradings should be added before approve the Customer PO';

            } else if ($grading_bulk != 1 && $grading_sample != 1 && $ismatched == 1) {
                $errorFlag = 1;
                $msg = 'Gradings are not matched';

            }
        }
    }


    $sql = "UPDATE `trn_orderheader` SET `intStatus`=intStatus-1 WHERE (`intOrderNo`='$orderNo') AND (`intOrderYear`='$orderYear') and intStatus not in(0,1)";

    $result = $db->RunQuery2($sql);

    if ($result) {
        $sql = "	SELECT 
							trn_orderheader.intReviseNo,
							trn_orderheader.intCreator,
							trn_orderheader.intStatus,
							trn_orderheader.PO_TYPE,
							trn_orderheader.intApproveLevelStart,
							trn_orderheader.strCustomerPoNo , 
							trn_orderheader.intLocationId,  
							mst_locations.intCompanyId 
						FROM trn_orderheader 
						Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
						WHERE
							trn_orderheader.intOrderNo =  '$orderNo' AND
							trn_orderheader.intOrderYear =  '$orderYear'
						";
        // echo $sql;
        $result2 = $db->RunQuery2($sql);
        $row2 = mysqli_fetch_array($result2);
        $createdUser = $row2['intCreator'];
        $status = $row2['intStatus'];
        $savedLevels = $row2['intApproveLevelStart'];
        $customerPO = $row2['strCustomerPoNo'];
        $trnsLocation = $row2['intLocationId'];
        $bulk_rev = $row2['intReviseNo'];
        $po_type = $row2['PO_TYPE'];

        $approveLevel = $savedLevels + 1 - $status;


        //created user can't raise the 2nd approval
        if ($approveLevel >= 2 && $createdUser == $userId) {
            $errorFlag = 1;
            $msg = "Approval " . $approveLevel . " is denied for created user";
        } else if ($approveLevel < 2){
            $sqlForSampleInfo = "SELECT intQty, dblPrice,splitedPrice, intSampleNo, intSampleYear, strCombo, strPrintName, intRevisionNo,DUMMY_SO_INITIAL_QTY ,QTY_PRICES , APPROVED_PRICE, costing_sample_header.STATUS, SO_TYPE, intSalesOrderId FROM 
                    trn_orderdetails INNER JOIN costing_sample_header ON costing_sample_header.SAMPLE_NO=trn_orderdetails.intSampleNo  AND 
                    costing_sample_header.SAMPLE_YEAR=trn_orderdetails.intSampleYear AND costing_sample_header.REVISION = trn_orderdetails.intRevisionNo AND
                    costing_sample_header.COMBO=trn_orderdetails.strCombo AND  costing_sample_header.PRINT=trn_orderdetails.strPrintName  WHERE trn_orderdetails.intOrderNo =  '$orderNo' AND
					trn_orderdetails.intOrderYear =  '$orderYear' AND trn_orderdetails.SO_TYPE > -1";
            $sampleResult = $db->RunQuery2($sqlForSampleInfo);
            while ($row = mysqli_fetch_array($sampleResult)) {
                $sampleYear = $row["intSampleYear"];
                $sampleNo = $row["intSampleNo"];
                $revNo = $row["intRevisionNo"];
                $combo = $row["strCombo"];
                $printName = $row["strPrintName"];
                $qtyWiseCostingQty = $row["intQty"];
                $qtyWiseCostingPrice = $row["dblPrice"] + $row["splitedPrice"];
                $salesOrderId = $row["intSalesOrderId"];
                $splitSoDummySample = false;
                $rcvSQty=getSalesOrderWiseRcvQty($orderNo,$orderYear,$salesOrderId);
                if ($row["SO_TYPE"] == 2){
                    $pressingSoId = getLinkedSoId($serialNo, $year, $salesOrderId);
                    $rcvSQty +=getSalesOrderWiseRcvQty($orderNo,$orderYear,$pressingSoId);
                }
                if ($po_type ==1){
                    $qtyWiseCostingQty = $row["DUMMY_SO_INITIAL_QTY"];
                } else if ($po_type == 2) {
                    $sqlForDummySampleInfo = "SELECT SO.intSampleNo as sampleNo, SO.intSampleYear as sampleYear, SO.strCombo as combo, SO.strPrintName as print, SO.intRevisionNo as revNo,SO.dblPrice as soPrice,SO.splitedPrice as soSplitedPrice,
                    DUMMY.intSampleNo as dummySampleNo, DUMMY.intSampleYear as dummySampleYear, DUMMY.strCombo as dummyCombo, DUMMY.strPrintName as dummyPrint, DUMMY.intRevisionNo as dummyRevNo,DUMMY.dblPrice as soPriceD,DUMMY.splitedPrice as soSplitedPriceD, CSH.STATUS FROM trn_orderdetails SO INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo=SO.intOrderNo AND 
                    trn_orderheader.intOrderYear=SO.intOrderYear INNER JOIN trn_orderdetails DUMMY ON SO.DUMMY_SO_ID=DUMMY.intSalesOrderId AND trn_orderheader.DUMMY_PO_NO=DUMMY.intOrderNo AND 
                    trn_orderheader.DUMMY_PO_YEAR=DUMMY.intOrderYear AND SO.DUMMY_SO_ID=DUMMY.intSalesOrderId LEFT JOIN costing_sample_header CSH on CSH.SAMPLE_NO = SO.intSampleNo AND CSH.SAMPLE_YEAR = SO.intSampleYear AND CSH.REVISION = SO.intRevisionNo AND CSH.COMBO = SO.strCombo AND CSH.PRINT = SO.strPrintName 
                     WHERE SO.intOrderNo =  '$orderNo' AND
                    SO.intOrderYear =  '$orderYear' AND SO.SO_TYPE > -1 AND SO.intSalesOrderId = '$salesOrderId'";
                    $dummySampleResult = $db->RunQuery2($sqlForDummySampleInfo);
                    while ($dummyRow = mysqli_fetch_array($dummySampleResult)) {
                                $sampleYear = $dummyRow["sampleYear"];
                                $sampleNo = $dummyRow["sampleNo"];
                                $revNo = $dummyRow["revNo"];
                                $combo = $dummyRow["combo"];
                                $printName = $dummyRow["print"];
                                $price = $dummyRow["soPrice"] + $dummyRow["soSplitedPrice"] ;
                                $sampleYearD = $dummyRow["dummySampleYear"];
                                $sampleNoD = $dummyRow["dummySampleNo"];
                                $revNoD = $dummyRow["dummyRevNo"];
                                $comboD = $dummyRow["dummyCombo"];
                                $printNameD = $dummyRow["dummyPrint"];
                                $priceD = $dummyRow["soSplitedPriceD"] + $dummyRow["soPriceD"] ;

                                if ($sampleYear == $sampleYearD && $sampleNo == $sampleNoD && $revNo == $revNoD && $combo == $comboD && $printName == $printNameD){
                                    $splitSoDummySample = true;
                                    if (round($price,3) != round($priceD,3)){
                                        $errorFlag =1;
                                        $msg = "Split Orders should have same price as dummy";
                                    }
                                }
                            }
                 }

                if ($errorFlag ==1 ){
                    break;
                }

                if ($row["STATUS"] != 1){
                    $errorFlag =1;
                    $msg = "Please Approve the Sample Price for ".$sampleNo."/".$sampleYear."$combo"."$printName";
                    break;
                } else if ($row["QTY_PRICES"] != 1 && round($qtyWiseCostingPrice,3) != round($row["APPROVED_PRICE"],3)){
                    $errorFlag =1;
                    $msg = "Costing approved price has changed in ".$sampleNo."/".$sampleYear."$combo"."$printName";
                    break;
                } else if ($row["QTY_PRICES"] == 1 && $rcvSQty <=0 && !$splitSoDummySample){
                    if (isset($samplePriceQuantity[$sampleYear][$sampleNo][$revNo][$combo][$printName]["quantity"])){
                        $samplePriceQuantity[$sampleYear][$sampleNo][$revNo][$combo][$printName]["quantity"] += $qtyWiseCostingQty;
                    } else {
                        $samplePriceQuantity[$sampleYear][$sampleNo][$revNo][$combo][$printName]["quantity"] = $qtyWiseCostingQty;
                    }

                    if (isset($samplePriceQuantity[$sampleYear][$sampleNo][$revNo][$combo][$printName]["price"])){
                        if ($qtyWiseCostingPrice != $samplePriceQuantity[$sampleYear][$sampleNo][$revNo][$combo][$printName]["price"]){
                            $errorFlag = 1;
                            $msg = "Prices should be same for ".$sampleNo."/".$sampleYear."$combo"."$printName";
                            break;
                        }
                    } else {
                        $samplePriceQuantity[$sampleYear][$sampleNo][$revNo][$combo][$printName]["price"] = $qtyWiseCostingPrice;
                    }
                }
            }
            foreach ($samplePriceQuantity as $year => $sampleEntries){
                foreach ($sampleEntries as $sample => $revEntries){
                    foreach ($revEntries as $revision => $comboEntries){
                        foreach ($comboEntries as $combo => $printEntries){
                            foreach ($printEntries as $print => $printEntry){
                                    $priceValidationStatus = qty_price_validate($sample, $year, $revision, $combo, $print, $printEntry['price'], $printEntry['quantity']);
                                    $soPriceIsValid =  $priceValidationStatus["qty_price_matched"];
                                    if (!$soPriceIsValid){
                                        $errorFlag = 1;
                                        $msg = $priceValidationStatus["message"];
                                        break;
                                    }
                            }
                        }
                    }
                }
            }

        }


        /////////////////// insert approval table ////////////////////
        $sql = "INSERT INTO `trn_orderheader_approvedby` (`intOrderNo`,`intYear`,`intApproveLevelNo`,`intApproveUser`,`dtApprovedDate`,intStatus) VALUES ('$orderNo','$orderYear','$approveLevel','$userId',now(),0)";
        $result = $db->RunQuery2($sql);
        //////////////////////////////////////////////////////////////

        if ($status != 1) {
            //sendConfirmEmails($orderNo,$orderYear,$customerPO,$programCode,$row2['intApproveLevelStart'],$row2['intStatus'],$row2['intCompanyId'],$objMail);
        }
        //-------------------token select nciga-----------------

        $token = select_token();

        //----------nciga-------------------------------------------------------------------
        $recordList = array();
        $finalRecord = array();
        if ($status == 1)//nciga devlopment final approved
        {
            $sql_select = "SELECT
	trn_orderheader.intReviseNo,
	trn_orderheader.intCreator,
	trn_orderheader.intStatus,
	trn_orderheader.intApproveLevelStart,
	trn_orderheader.strCustomerPoNo,
	trn_orderheader.intLocationId,
	trn_orderheader.strTentitiveCustomerPoNo,
	trn_orderheader.TENTATIVE,
	mst_locations.intCompanyId,
	mst_locations.strName AS companyLocation,
	trn_orderheader.intCustomer,
	mst_customer.strName AS customer,
	mst_customer_locations_header.strName AS customerLocation,
	mst_companies.strName AS company,
	trn_orderheader.strCustomerPoNo,
	trn_orderdetails.strTentitiveSalesOrderNo,
	trn_orderdetails.intSalesOrderId,
	trn_orderdetails.strSalesOrderNo,
	trn_orderdetails.intOrderNo,
	trn_orderdetails.intOrderYear,
	trn_orderdetails.strLineNo,
	trn_orderheader.dtDeliveryDate AS poDeliveryDate,
	trn_orderdetails.dtDeliveryDate AS soDeliverydate,
	trn_orderdetails.strGraphicNo,
	trn_orderdetails.intSampleNo,
	trn_orderdetails.intSampleYear,
	trn_orderdetails.strStyleNo,
	trn_orderdetails.dtPSD,
	trn_orderdetails.intQty AS poqty,
	trn_orderheader.dtDate,
	trn_orderdetails.intOrderYear,
	trn_orderdetails.intPart,
	mst_part.strName as partName,
	trn_orderdetails.dblDamagePercentage,
	trn_ordersizeqty.strSize,
	trn_ordersizeqty.dblQty,
        if((trn_ordersizeqty.orderType=0),'new','revised') as orderType,
	mst_brand.strName AS brand,
        trn_sampleinfomations_details.intGroundColor,
        mst_colors_ground.strName as groundColor,
        mst_customer_locations.BundleTag
        FROM
        trn_orderheader
        INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
        AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
        INNER JOIN mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
        INNER JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
        INNER JOIN mst_customer_locations_header ON mst_customer_locations_header.intId = trn_orderheader.intCustomerLocation
        INNER JOIN mst_customer_locations ON mst_customer_locations.intCustomerId = mst_customer.intId
        AND mst_customer_locations.intLocationId = mst_customer_locations_header.intId
        INNER JOIN mst_companies ON mst_companies.intId = mst_locations.intCompanyId
        INNER JOIN mst_part ON mst_part.intId = trn_orderdetails.intPart
        INNER JOIN trn_ordersizeqty ON trn_ordersizeqty.intOrderNo = trn_orderdetails.intOrderNo
        AND trn_ordersizeqty.intOrderYear = trn_orderdetails.intOrderYear
        AND trn_ordersizeqty.intSalesOrderId = trn_orderdetails.intSalesOrderId
        INNER JOIN trn_sampleinfomations_details ON trn_orderdetails.intSampleNo = trn_sampleinfomations_details.intSampleNo
        AND trn_orderdetails.intSampleYear = trn_sampleinfomations_details.intSampleYear
        AND trn_orderdetails.intRevisionNo = trn_sampleinfomations_details.intRevNo
        AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName
        AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
        INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
        AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
        AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo
        INNER JOIN mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
        iNNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
        WHERE
                trn_orderheader.intOrderNo = '$orderNo'
                AND trn_orderheader.intOrderYear = '$orderYear'
                AND trn_ordersizeqty.ncingaDeliveryStatus!=1    
                GROUP BY
                trn_orderdetails.intOrderNo,
                trn_orderdetails.intOrderYear,
                trn_orderdetails.intSalesOrderId,
                trn_ordersizeqty.strSize ";
            //echo   $sql_select;
            $result1 = $db->RunQuery2($sql_select);
            while ($row = mysqli_fetch_array($result1)) {

                $value = array();

                $customer = trim($row['customer']);
                $customerLocation = trim($row['customerLocation']);
                $company = trim($row['company']);
                $companyLocation = trim($row['companyLocation']);
                $strCustomerPoNo = trim($row['strCustomerPoNo']);
                $intSalesOrderId = $row['intSalesOrderId'];
                $strSalesOrderNo = trim($row['strSalesOrderNo']);
                $intOrderNo = $row['intOrderNo'];
                $strLineNo = trim($row['strLineNo']);
                $poDeliveryDate = $row['poDeliveryDate'];
                $soDeliverydate = $row['soDeliverydate'];
                $strGraphicNo = trim($row['strGraphicNo']);
                $intSampleNo = $row['intSampleNo'];
                $intSampleYear = $row['intSampleYear'];
                $groundColor = trim($row['groundColor']);
                $strStyleNo = trim($row['strStyleNo']);
                $backgroundColor = $row['intSampleYear'];
                $strSize = trim($row['strSize']);
                $dtPSD = $row['dtPSD'];
                $dblQty = $row['dblQty'];
                $poqty = $row['poqty'];
                $dtDate = $row['dtDate'];
                $intOrderYear = $row['intOrderYear'];
                $partName = trim($row['partName']);
                $brand = trim($row['brand']);
                $dblDamagePercentage = $row['dblDamagePercentage'];
                $bundleTag = trim($row['BundleTag']);
                $orderType = trim($row['orderType']);
                $tentitive = trim($row['TENTATIVE']);
                if ($tentitive > 0) {
                    $strCustomerPoNo = trim($row['strTentitiveCustomerPoNo']);
                    $strSalesOrderNo = trim($row['strTentitiveSalesOrderNo']);
                }

                //---------------assign to array---------------------
                $value["transactionId"] = $intOrderNo . "." . $intOrderYear . "." . $intSalesOrderId . "." . $strSize . "";
                $value["customer"] = "$customer";
                $value["customerLocation"] = " $customerLocation";
                $value["company"] = "$company";
                $value["companyLocation"] = "$companyLocation";
                $value["customerPO"] = "$strCustomerPoNo";
                $value["customerSO"] = "$strSalesOrderNo";
                $value["SalesOrderId"] = "$intSalesOrderId";
                $value["orderNo"] = "$intOrderNo";
                $value["lineNo"] = "$strLineNo";
                $value["poDeliveryDate"] = "$poDeliveryDate";
                $value["graphicNo"] = "$strGraphicNo";
                $value["sampleNo"] = "$intSampleYear" . "/" . "$intSampleNo";
                $value["brand"] = "$brand";
                $value["style"] = "$strStyleNo";
                $value["backGroundColor"] = "$groundColor";
                $value["size"] = "$strSize";
                $value["PSD"] = "$dtPSD";
                $value["deliveryDate"] = "$soDeliverydate";
                $value["qty"] = $dblQty;
                $value["poQty"] = $poqty;
                $value["actualTotalQty"] = $dblQty;
                $value["date"] = "$dtDate";
                $value["year"] = "$intOrderYear";
                $value["part"] = "$partName";
                $value["PDPercentage"] = " $dblDamagePercentage";
                $value["damagePercentage"] = " $dblDamagePercentage";
                $value["bundleTag"] = $bundleTag;
                $value["type"] = "order";
                $value["orderType"] = "$orderType";


                $recordList = $value;
                $recordArray = array('value' => $recordList);
                array_push($finalRecord, new ArrayObject($recordArray));

            }

        }
        //------------------------------------------------------------------

        if ($status == 1) {
            //if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
            sendFinlConfirmedEmailToUser($orderNo, $orderYear, $customerPO, $objMail, $mainPath, $root_path);
            //include "sample_color_items_autoAllocation.php";
            //}
        }

        if ($status == $savedLevels) {

            $app_fag = getSpecialTechApprovedSataus($orderNo, $orderYear);

            if ($app_fag == 0) {
                $errorFlag = 1;
                $msg = "Please approve consumptions for special techniques";
            }

            $response_prn = generatePRN($orderNo, $orderYear, $bulk_rev);

            if ($response_prn['type'] == 'fail') {
                $msg = $response_prn['msg'] . " " . $response_prn['q'];
                $errorFlag = 1;
            }
        }

    }

    if ($errorFlag == 1) {
        $db->RunQuery2('Rollback');
        $response['type'] = 'fail';
        $response['msg'] = $msg;
        $db->rollback();
    } else if ($errorFlag == 0) {
        if ($status == 1 && ($_SESSION['headCompanyId'] == 1) && $po_type != 1) {

            $revised_date = getRevisedDate($orderNo, $orderYear, $bulk_rev, $db);
            $transaction_string = ($revised_date != '') ? "SO_EditFinish" : "SO_New";
            updateAzureTable($orderNo, $orderYear, $db, $transaction_string, $revised_date);
        }
        $db->RunQuery2('Commit');
        $response['type'] = 'pass';
        $response['msg'] = 'Approved successfully.';
        if ($status == 1) {
            send_detais($finalRecord, $token);

        }//send details nciga

    } else {
        $db->RunQuery2('Rollback');
        $response['type'] = 'fail';
        $response['msg'] = $db->errormsg;
        $response['q'] = $sql;
    }

    $db->commit();
    //if($_REQUEST['status']==)
    echo json_encode($response);


} else if ($_REQUEST['status'] == 'reject') {
    $db->begin();

    save_pri_history($orderNo, $orderYear, $bulk_rev); // suvini ticket - 2268

    $errorFlag = 0;
    $arrHeader = json_decode($_REQUEST['arrHeader'], true);
    $reject_reason = $obj_commom->replace($arrHeader['reason']);

    $sql = "UPDATE `trn_orderheader` SET `intStatus`=0,REJECT_REASON = '$reject_reason' WHERE (`intOrderNo`='$orderNo') AND (`intOrderYear`='$orderYear') ";
    $result = $db->RunQuery2($sql);

    cancelPRNandPOs($orderNo, $orderYear, 'Rejected');
    //$errorFlag		=1;
    //$sql = "DELETE FROM `trn_orderheader_approvedby` WHERE (`intOrderNo`='$orderNo') AND (`intYear`='$orderYear')";
    //$result2 = $db->RunQuery($sql);

    $sqlI = "INSERT INTO `trn_orderheader_approvedby` (`intOrderNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$orderNo','$orderYear','0','$userId',now(),0)";
    $resultI = $db->RunQuery2($sqlI);

    $sql = "	SELECT 
								trn_orderheader.intCreator, 
								trn_orderheader.intStatus,
								trn_orderheader.intApproveLevelStart,
								trn_orderheader.strCustomerPoNo ,
								trn_orderheader.PO_TYPE,
		 						trn_orderheader.intReviseNo 
							FROM trn_orderheader
							WHERE
								trn_orderheader.intOrderNo =  '$orderNo' AND
								trn_orderheader.intOrderYear =  '$orderYear'
							";
    $result2 = $db->RunQuery2($sql);
    $row2 = mysqli_fetch_array($result2);
    $createdUser = $row['intCreator'];
    $status = $row2['intStatus'];
    $savedLevels = $row2['intApproveLevelStart'];
    $customerPO = $row2['strCustomerPoNo'];
    $bulk_rev = $row2['intReviseNo'];
    $po_type = $row2['PO_TYPE'];

    if ($status == 0) {
        save_history($orderNo, $orderYear, $bulk_rev);

        //if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
        sendRejecedEmailToUser($orderNo, $orderYear, $customerPO, $objMail, $mainPath, $root_path);
        //}
    }


    if ($errorFlag == 1) {
        $db->RunQuery2('Rollback');
        $response['type'] = 'fail';
        $response['msg'] = $msg;
        $db->rollback();
    } else if ($errorFlag == 0) {
        $db->RunQuery2('Commit');
        $response['type'] = 'pass';
        $response['msg'] = 'Rejected successfully.';
    } else {
        $db->RunQuery2('Rollback');
        $response['type'] = 'fail';
        $response['msg'] = $db->errormsg;
        $response['q'] = $sql;
    }
    $db->commit();
    //if($_REQUEST['status']==)
    echo json_encode($response);

} else if ($_REQUEST['status'] == 'revisePO') {

    $db->begin();
    save_pri_history($orderNo, $orderYear); // suvini ticket - 2268

    $PONo = $_REQUEST['PONo'];
    $PONoArray = explode("/", $PONo);
    $orderNo = $PONoArray[0];
    $orderYear = $PONoArray[1];

    $sql = "	SELECT  
		 				trn_orderheader.intReviseNo,
						trn_orderheader.intCreator, 
						trn_orderheader.intStatus,
						trn_orderheader.intApproveLevelStart,
						trn_orderheader.strCustomerPoNo,
						trn_orderheader.PO_TYPE  
					FROM trn_orderheader
					WHERE
						trn_orderheader.intOrderNo =  '$orderNo' AND
						trn_orderheader.intOrderYear =  '$orderYear'
					";
    $result2 = $db->RunQuery2($sql);
    $row2 = mysqli_fetch_array($result2);
    $createdUser = $row2['intCreator'];
    $status = $row2['intStatus'];
    $savedLevels = $row2['intApproveLevelStart'];
    $customerPO = $row2['strCustomerPoNo'];
    $bulk_rev = $row2['intReviseNo'];
    $po_type = $row2['PO_TYPE'];


    $sql = "UPDATE `trn_orderheader` SET `intStatus`=-1 WHERE (`intOrderNo`='$orderNo') AND (`intOrderYear`='$orderYear')  ";
    $result = $db->RunQuery2($sql);

    cancelPRNandPOs($orderNo, $orderYear, 'Revised');
    save_history($orderNo, $orderYear, $bulk_rev);

    //$sql = "DELETE FROM `trn_orderheader_approvedby` WHERE (`intOrderNo`='$orderNo') AND (`intYear`='$orderYear')";
    //$resultD=$db->RunQuery($sql);
    $maxAppByStatus = (int)getMaxAppByStatus($orderNo, $orderYear) + 1;
    $sql = "UPDATE `trn_orderheader_approvedby` SET intStatus ='$maxAppByStatus' 
				WHERE (`intOrderNo`='$orderNo') AND (`intYear`='$orderYear') AND (`intStatus`='0')";
    $result1 = $db->RunQuery2($sql);


    $sqlI = "INSERT INTO `trn_orderheader_approvedby` (`intOrderNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$orderNo','$orderYear','-1','$userId',now(),0)";
    $resultI = $db->RunQuery2($sqlI);


    //------------send details to ncinga-----------------------------------------------------------------------------
    $recordList = array();
    $finalRecord = array();
    $sql_select = "SELECT
	trn_orderheader.intReviseNo,
	trn_orderheader.intCreator,
	trn_orderheader.intStatus,
	trn_orderheader.intApproveLevelStart,
	trn_orderheader.strCustomerPoNo,
	trn_orderheader.intLocationId,
	trn_orderheader.strTentitiveCustomerPoNo,
	trn_orderheader.TENTATIVE,
	mst_locations.intCompanyId,
	mst_locations.strName AS companyLocation,
	trn_orderheader.intCustomer,
	mst_customer.strName AS customer,
	mst_customer_locations_header.strName AS customerLocation,
	mst_companies.strName AS company,
	trn_orderheader.strCustomerPoNo,
	trn_orderdetails.intSalesOrderId,
	trn_orderdetails.strTentitiveSalesOrderNo,
	trn_orderdetails.strSalesOrderNo,
	trn_orderdetails.intOrderNo,
	trn_orderdetails.intOrderYear,
	trn_orderdetails.strLineNo,
	trn_orderheader.dtDeliveryDate AS poDeliveryDate,
	trn_orderdetails.dtDeliveryDate AS soDeliverydate,
	trn_orderdetails.strGraphicNo,
	trn_orderdetails.intSampleNo,
	trn_orderdetails.intSampleYear,
	trn_orderdetails.strStyleNo,
	trn_orderdetails.dtPSD,
	trn_orderdetails.intQty AS poqty,
	trn_orderheader.dtDate,
	trn_orderdetails.intOrderYear,
	trn_orderdetails.intPart,
	mst_part.strName as partName,
	trn_orderdetails.dblDamagePercentage,
	trn_ordersizeqty.strSize,
	trn_ordersizeqty.dblQty,
        -- if((trn_ordersizeqty.orderType=0),'new','revised') as orderType,
	mst_brand.strName AS brand,
        trn_sampleinfomations_details.intGroundColor,
        mst_colors_ground.strName as groundColor,
        mst_customer_locations.BundleTag
        FROM
        trn_orderheader
        INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
        AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
        INNER JOIN mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
        INNER JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
        INNER JOIN mst_customer_locations_header ON mst_customer_locations_header.intId = trn_orderheader.intCustomerLocation
        INNER JOIN mst_customer_locations ON mst_customer_locations.intCustomerId = mst_customer.intId
        AND mst_customer_locations.intLocationId = mst_customer_locations_header.intId
        INNER JOIN mst_companies ON mst_companies.intId = mst_locations.intCompanyId
        INNER JOIN mst_part ON mst_part.intId = trn_orderdetails.intPart
        INNER JOIN trn_ordersizeqty ON trn_ordersizeqty.intOrderNo = trn_orderdetails.intOrderNo
        AND trn_ordersizeqty.intOrderYear = trn_orderdetails.intOrderYear
        AND trn_ordersizeqty.intSalesOrderId = trn_orderdetails.intSalesOrderId
        INNER JOIN trn_sampleinfomations_details ON trn_orderdetails.intSampleNo = trn_sampleinfomations_details.intSampleNo
        AND trn_orderdetails.intSampleYear = trn_sampleinfomations_details.intSampleYear
        AND trn_orderdetails.intRevisionNo = trn_sampleinfomations_details.intRevNo
        AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName
        AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
        INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
        AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
        AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo
        INNER JOIN mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
        iNNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
        WHERE
                trn_orderheader.intOrderNo = '$orderNo'
                AND trn_orderheader.intOrderYear = '$orderYear'
                -- AND trn_ordersizeqty.ncingaDeliveryStatus!=1    
                GROUP BY
                trn_orderdetails.intOrderNo,
                trn_orderdetails.intOrderYear,
                trn_orderdetails.intSalesOrderId,
                trn_ordersizeqty.strSize ";
    //echo   $sql_select;
    $result1 = $db->RunQuery2($sql_select);
    while ($row = mysqli_fetch_array($result1)) {

        $value = array();

        $customer = trim($row['customer']);
        $customerLocation = trim($row['customerLocation']);
        $company = trim($row['company']);
        $companyLocation = trim($row['companyLocation']);
        $strCustomerPoNo = trim($row['strCustomerPoNo']);
        $intSalesOrderId = $row['intSalesOrderId'];
        $strSalesOrderNo = trim($row['strSalesOrderNo']);
        $intOrderNo = $row['intOrderNo'];
        $strLineNo = trim($row['strLineNo']);
        $poDeliveryDate = $row['poDeliveryDate'];
        $soDeliverydate = $row['soDeliverydate'];
        $strGraphicNo = trim($row['strGraphicNo']);
        $intSampleNo = $row['intSampleNo'];
        $intSampleYear = $row['intSampleYear'];
        $groundColor = trim($row['groundColor']);
        $strStyleNo = trim($row['strStyleNo']);
        $backgroundColor = $row['intSampleYear'];
        $strSize = trim($row['strSize']);
        $dtPSD = $row['dtPSD'];
        $dblQty = $row['dblQty'];
        $poqty = $row['poqty'];
        $dtDate = $row['dtDate'];
        $intOrderYear = $row['intOrderYear'];
        $partName = trim($row['partName']);
        $brand = trim($row['brand']);
        $dblDamagePercentage = $row['dblDamagePercentage'];
        $bundleTag = trim($row['BundleTag']);
        $orderType = trim($row['orderType']);
        $tentitive = trim($row['TENTATIVE']);
        if ($tentitive > 0) {
            $strCustomerPoNo = trim($row['strTentitiveCustomerPoNo']);
            $strSalesOrderNo = trim($row['strTentitiveSalesOrderNo']);
        }


        //---------------assign to array---------------------
        $value["transactionId"] = $intOrderNo . "." . $intOrderYear . "." . $intSalesOrderId . "." . $strSize . "";
        $value["customer"] = "$customer";
        $value["customerLocation"] = " $customerLocation";
        $value["company"] = "$company";
        $value["companyLocation"] = "$companyLocation";
        $value["customerPO"] = "$strCustomerPoNo";
        $value["customerSO"] = "$strSalesOrderNo";
        $value["SalesOrderId"] = "$intSalesOrderId";
        $value["orderNo"] = "$intOrderNo";
        $value["lineNo"] = "$strLineNo";
        $value["poDeliveryDate"] = "$poDeliveryDate";
        $value["graphicNo"] = "$strGraphicNo";
        $value["sampleNo"] = "$intSampleYear" . "/" . "$intSampleNo";
        $value["brand"] = "$brand";
        $value["style"] = "$strStyleNo";
        $value["backGroundColor"] = "$groundColor";
        $value["size"] = "$strSize";
        $value["PSD"] = "$dtPSD";
        $value["deliveryDate"] = "$soDeliverydate";
        $value["qty"] = $dblQty;
        $value["poQty"] = $poqty;
        $value["actualTotalQty"] = $dblQty;
        $value["date"] = "$dtDate";
        $value["year"] = "$intOrderYear";
        $value["part"] = "$partName";
        $value["PDPercentage"] = " $dblDamagePercentage";
        $value["damagePercentage"] = " $dblDamagePercentage";
        $value["bundleTag"] = $bundleTag;
        $value["type"] = "order";
        $value["orderType"] = "deleted";


        $recordList = $value;
        $recordArray = array('value' => $recordList);
        array_push($finalRecord, new ArrayObject($recordArray));
        //print_r($finalRecord);

    }

    //------------------------------------------------------------------------------------------------------------------
    if ($result) {
        //if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
        sendRevisedEmailToUser($orderNo, $orderYear, $customerPO, $objMail, $mainPath, $root_path);
        //}
    }

    if ($errorFlag == 0 && $_SESSION['headCompanyId'] == 1 && $po_type != 1 && $status==1)  {
        updateAzureTable($orderNo, $orderYear, $db, "SO_EditStart", '');
    }

    if ($errorFlag == 1) {
        $db->RunQuery2('Rollback');
        $response['type'] = 'fail';
        $response['msg'] = $msg;
        $db->rollback();
    } else if ($errorFlag == 0) {
        $db->RunQuery2('Commit');
        $response['type'] = 'pass';
        $response['msg'] = 'PO is revised successfully.';

        // send_detais($finalRecord, $token); //send deleted details to ncinga

    } else {
        $db->RunQuery2('Rollback');
        $response['type'] = 'fail';
        $response['msg'] = $db->errormsg;
        $response['q'] = $sql;
    }
    $db->commit();
    //if($_REQUEST['status']==)
    echo json_encode($response);
    ////////////////////////////////////////////////////////

}

//-------	----------------------------------------------------------------------
function checkSizes($orderNo, $orderYear)
{
    global $db;
    $sql = "SELECT
					trn_orderdetails.intOrderNo,
					trn_orderdetails.intOrderYear,
					trn_orderdetails.strSalesOrderNo,
					trn_ordersizeqty.strSize,
					trn_ordersizeqty.dblQty
				FROM
				trn_orderdetails
					Left Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_ordersizeqty.intSalesOrderId = trn_orderdetails.intSalesOrderId
				WHERE
					trn_orderdetails.intOrderNo =  '$orderNo' AND
					trn_orderdetails.intOrderYear =  '$orderYear' AND
					trn_ordersizeqty.strSize IS NULL 
				";
    $result = $db->RunQuery($sql);
    $rows = mysqli_num_rows($result);
    if ($rows > 0)
        return false;
    else
        return true;
}

//--------------------------------------------------------
function sendConfirmEmails($serialNo, $year, $customerPO, $programCode, $savedStat, $intStatus, $companyId, $objMail)
{
    global $db;

    $k = $savedStat + 2 - $intStatus;
    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail
			FROM menupermision 
			Inner Join menus ON menupermision.intMenuId = menus.intId 
			Inner Join sys_users ON menupermision.intUserId = sys_users.intUserId
			Inner Join mst_locations_user ON sys_users.intUserId = mst_locations_user.intUserId
			Inner Join mst_locations ON mst_locations_user.intLocationId = mst_locations.intId
			WHERE
			menus.strCode =  '$programCode' AND 
			menupermision.int" . $k . "Approval='1'  AND 
			mst_locations.intCompanyId='$companyId'  
			GROUP BY
			sys_users.intUserId	";

    $result = $db->RunQuery($sql);

    while ($row = mysqli_fetch_array($result)) {
        $enterUserName = $row['strFullName'];
        $enterUserEmail = $row['strEmail'];

        $header = "CUSTOMER PO FOR APPROVAL ($serialNo/$year)";

        //send mails ////
        $requestArray = NULL;

        $requestArray['program'] = 'CUSTOMER PO';
        $requestArray['field1'] = 'Order No';
        $requestArray['field2'] = 'Order Year';
        $requestArray['field3'] = 'customerPO';
        $requestArray['field4'] = '';
        $requestArray['field5'] = '';
        $requestArray['value1'] = $serialNo;
        $requestArray['value2'] = $year;
        $requestArray['value3'] = $customerPO;
        $requestArray['value4'] = '';
        $requestArray['value5'] = '';

        $requestArray['subject'] = $header;

        $requestArray['statement1'] = "This Customer PO is Approved";
        $requestArray['statement2'] = "to view this";
        //$_REQUEST['link']=urlencode(base64_encode($mainPath."presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder.php?orderNo=$serialNo&orderYear=$year&approveMode=1"));
        $requestArray['link'] = base64_encode($_SESSION['MAIN_URL'] . "?q=896&orderNo=$serialNo&orderYear=$year&approveMode=1");

        $objMail->send_Response_Mail('mail_approval_template.php', $requestArray, $_SESSION["systemUserName"], $_SESSION["email"], $header, $enterUserEmail, $enterUserName);
    }
}

//--------------------------------------------------------
function sendFinlConfirmedEmailToUser($serialNo, $year, $customerPO, $objMail, $mainPat, $root_path)
{
    global $db;
    global $userId;

    $sql = "SELECT
			trn_orderheader.intCreator,
			trn_orderheader.intMarketer,
			finance_customer_invoice_header.CREATED_BY 
			FROM 
			trn_orderheader
			LEFT JOIN finance_customer_invoice_header ON trn_orderheader.intOrderNo = finance_customer_invoice_header.ORDER_NO AND trn_orderheader.intOrderYear = finance_customer_invoice_header.ORDER_YEAR
			WHERE
			trn_orderheader.intOrderNo =  '$serialNo' AND
			trn_orderheader.intOrderYear =  '$year'";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $created_user = $row['intCreator'];

    $creator_details = getSysUserDetails2($row['intCreator']);
    $marketer_details = getSysUserDetails2($row['intMarketer']);
    $invoicer_details = getSysUserDetails2($row['CREATED_BY']);

    $enterUserName = $row['strFullName'];
    $enterUserEmail = $row['strEmail'];

    $header = "CONFIRMED CUSTOMER PO ('$serialNo'/'$year')";

    //send mails ////
    $requestArray = NULL;

    $requestArray['program'] = 'CUSTOMER PO';
    $requestArray['field1'] = 'Order No';
    $requestArray['field2'] = 'Order Year';
    $requestArray['field3'] = 'Customer PO';
    $requestArray['field4'] = '';
    $requestArray['field5'] = '';
    $requestArray['value1'] = $serialNo;
    $requestArray['value2'] = $year;
    $requestArray['value3'] = $customerPO;
    $requestArray['value4'] = '';
    $requestArray['value5'] = '';

    $requestArray['subject'] = "CONFIRMED CUSTOMER PO ('$serialNo'/'$year')";

    $requestArray['statement1'] = "Approved this";
    $requestArray['statement2'] = "to view this";
    //$_REQUEST['link']=base64_encode($mainPath."presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder.php?orderNo=$serialNo&orderYear=$year&approveMode=1");
    $_REQUEST['link'] = base64_encode($_SESSION['MAIN_URL'] . "?q=896&orderNo=$serialNo&orderYear=$year");
    //$path=$mainPath.'presentation/mail_approval_template.php';
    $path = $root_path . "presentation/mail_approval_template.php";
    //$objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
    if ($userId != $created_user) {
        $objMail->send_Response_Mail2($path, $requestArray, $_SESSION["systemUserName"], $_SESSION["email"], $header, $creator_details['email'], $creator_details['fulll_name']);
    }
    $objMail->send_Response_Mail2($path, $requestArray, $_SESSION["systemUserName"], $_SESSION["email"], $header, $marketer_details['email'], $marketer_details['fulll_name']);
    $objMail->send_Response_Mail2($path, $requestArray, $_SESSION["systemUserName"], $_SESSION["email"], $header, $invoicer_details['email'], $invoicer_details['fulll_name']);


}

//--------------------------------------------------------
function sendRejecedEmailToUser($serialNo, $year, $customerPO, $objMail, $mainPat, $root_path)
{
    global $db;
    global $userId;

    $sql = "SELECT
			trn_orderheader.intCreator,
			trn_orderheader.intMarketer,
			trn_orderheader.REJECT_REASON,
			finance_customer_invoice_header.CREATED_BY 
			FROM 
			trn_orderheader
			LEFT JOIN finance_customer_invoice_header ON trn_orderheader.intOrderNo = finance_customer_invoice_header.ORDER_NO AND trn_orderheader.intOrderYear = finance_customer_invoice_header.ORDER_YEAR
			WHERE
			trn_orderheader.intOrderNo =  '$serialNo' AND
			trn_orderheader.intOrderYear =  '$year'";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $created_user = $row['intCreator'];

    $creator_details = getSysUserDetails2($row['intCreator']);
    $marketer_details = getSysUserDetails2($row['intMarketer']);
    $invoicer_details = getSysUserDetails2($row['CREATED_BY']);


    $enterUserName = $row['strFullName'];
    $enterUserEmail = $row['strEmail'];

    $header = "REJECTED CUSTOMER PO ('$serialNo'/'$year')";

    //send mails ////
    $_REQUEST = NULL;
    $_REQUEST = NULL;

    $_REQUEST['program'] = 'CUSTOMER PO';
    $_REQUEST['field1'] = 'Order No';
    $_REQUEST['field2'] = 'Order Year';
    $_REQUEST['field3'] = 'Customer PO';
    $_REQUEST['field4'] = 'Rejected Reason';
    $_REQUEST['field5'] = '';
    $_REQUEST['value1'] = $serialNo;
    $_REQUEST['value2'] = $year;
    $_REQUEST['value3'] = $customerPO;
    $_REQUEST['value4'] = $row['REJECT_REASON'];
    $_REQUEST['value5'] = '';

    $_REQUEST['subject'] = "REJECTED CUSTOMER PO ('$serialNo'/'$year')";

    $_REQUEST['statement1'] = "Rejected this";
    $_REQUEST['statement2'] = "to view this";
    //$_REQUEST['link']=base64_encode($mainPath."presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder.php?orderNo=$serialNo&orderYear=$year&approveMode=1");
    $_REQUEST['link'] = base64_encode($_SESSION['MAIN_URL'] . "?q=896&orderNo=$serialNo&orderYear=$year");
    //$path=$mainPath.'presentation/mail_approval_template.php';
    $path = $root_path . "presentation/mail_approval_template.php";
    //$objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
    if ($userId != $created_user) {
        $objMail->send_Response_Mail2($path, $_REQUEST, $_SESSION["systemUserName"], $_SESSION["email"], $header, $creator_details['email'], $creator_details['fulll_name']);
    }
    $objMail->send_Response_Mail2($path, $_REQUEST, $_SESSION["systemUserName"], $_SESSION["email"], $header, $marketer_details['email'], $marketer_details['fulll_name']);
    $objMail->send_Response_Mail2($path, $_REQUEST, $_SESSION["systemUserName"], $_SESSION["email"], $header, $invoicer_details['email'], $invoicer_details['fulll_name']);
}

//--------------------------------------------------------
function sendRevisedEmailToUser($serialNo, $year, $customerPO, $objMail, $mainPat, $root_path)
{
    global $db;
    global $userId;

    $sql = "SELECT
			trn_orderheader.intCreator,
			trn_orderheader.intMarketer,
			trn_orderheader.strCustomerPoNo, 
			finance_customer_invoice_header.CREATED_BY 
			FROM 
			trn_orderheader
			LEFT JOIN finance_customer_invoice_header ON trn_orderheader.intOrderNo = finance_customer_invoice_header.ORDER_NO AND trn_orderheader.intOrderYear = finance_customer_invoice_header.ORDER_YEAR
			WHERE
			trn_orderheader.intOrderNo =  '$serialNo' AND
			trn_orderheader.intOrderYear =  '$year'";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $created_user = $row['intCreator'];

    $creator_details = getSysUserDetails2($row['intCreator']);
    $marketer_details = getSysUserDetails2($row['intMarketer']);
    $invoicer_details = getSysUserDetails2($row['CREATED_BY']);
    $customerPO = $row['strCustomerPoNo'];
    $enterUserName = $row['strFullName'];
    $enterUserEmail = $row['strEmail'];

    $header = "REVISED CUSTOMER PO ('$serialNo'/'$year')";

    //send mails ////
    $_REQUEST = NULL;
    $_REQUEST = NULL;

    $_REQUEST['program'] = 'CUSTOMER PO';
    $_REQUEST['field1'] = 'Order No';
    $_REQUEST['field2'] = 'Order Year';
    $_REQUEST['field3'] = 'Customer PO';
    $_REQUEST['field4'] = '';
    $_REQUEST['field5'] = '';
    $_REQUEST['value1'] = $serialNo;
    $_REQUEST['value2'] = $year;
    $_REQUEST['value3'] = $customerPO;
    $_REQUEST['value4'] = '';
    $_REQUEST['value5'] = '';

    $_REQUEST['subject'] = "REVISED CUSTOMER PO ('$serialNo'/'$year')";

    $_REQUEST['statement1'] = "Revised this";
    $_REQUEST['statement2'] = "to view this";
    //$_REQUEST['link']=base64_encode($mainPath."presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder.php?orderNo=$serialNo&orderYear=$year&approveMode=1");
    $_REQUEST['link'] = base64_encode($_SESSION['MAIN_URL'] . "?q=896&orderNo=$serialNo&orderYear=$year");
    //$path=$mainPath.'presentation/mail_approval_template.php';
    $path = $root_path . "presentation/mail_approval_template.php";
    //$objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
    if ($userId != $created_user) {
        $objMail->send_Response_Mail2($path, $_REQUEST, $_SESSION["systemUserName"], $_SESSION["email"], $header, $creator_details['email'], $creator_details['fulll_name']);
    }
    $objMail->send_Response_Mail2($path, $_REQUEST, $_SESSION["systemUserName"], $_SESSION["email"], $header, $marketer_details['email'], $marketer_details['fulll_name']);
    $objMail->send_Response_Mail2($path, $_REQUEST, $_SESSION["systemUserName"], $_SESSION["email"], $header, $invoicer_details['email'], $invoicer_details['fulll_name']);

}

//------------------------------function get MaxAppByStatus---------------------
function getMaxAppByStatus($serialNo, $year)
{
    global $db;

    //echo $savedStat;
    $editMode = 0;
    $sql = "SELECT
				max(trn_orderheader_approvedby.intStatus) as status 
				FROM
				trn_orderheader_approvedby
				WHERE
				trn_orderheader_approvedby.intOrderNo =  '$serialNo' AND
				trn_orderheader_approvedby.intYear =  '$year'";

    $resultm = $db->RunQuery2($sql);
    $rowm = mysqli_fetch_array($resultm);

    return $rowm['status'];

}

//--------------------------------------------------------
function getSysUserDetails($userId)
{
    global $db;


    $sql = "SELECT
				sys_users.strFullName as fulll_name,
				sys_users.strEmail as email 
				FROM 
				sys_users
				WHERE sys_users.intUserId ='$userId'";

    $resultm = $db->RunQuery($sql);
    $rowm = mysqli_fetch_array($resultm);

    return $rowm;

}

//-------------------------------------------------------
function getSysUserDetails2($userId)
{
    global $db;


    $sql = "SELECT
				sys_users.strFullName as fulll_name,
				sys_users.strEmail as email 
				FROM 
				sys_users
				WHERE sys_users.intUserId ='$userId'";

    $resultm = $db->RunQuery2($sql);
    $rowm = mysqli_fetch_array($resultm);

    return $rowm;

}

//--------------------------------------------------------
function generatePRN($orderNo, $orderYear, $bulk_rev)
{

    $prn_no = generateSerial();
    $year = date('Y');
    $response = saveHeader($prn_no, $year, $orderNo, $orderYear, $bulk_rev);
    if ($response['type'] == 'pass')
        $response = saveDetails($prn_no, $year, $orderNo, $orderYear);

    //else{
    //$response['saved']=0;
    //$response			=$response_h;
    //}
    generatePRNemail($prn_no, $year, $orderNo, $orderYear, $response['qty']);
    return $response;
}

function generateSerial()
{

    global $db;
    global $companyId;
    $sql = "SELECT
			sys_no.intPRNno
			FROM sys_no
			WHERE
			sys_no.intCompanyId =  '$companyId'
			";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $no = $row['intPRNno'];

    $sql = "UPDATE `sys_no` SET intPRNno=intPRNno+1 WHERE (`intCompanyId`='$companyId')  ";
    $db->RunQuery2($sql);

    return $no;
}

function saveHeader($prn_no, $year, $orderNo, $orderYear, $bulk_rev)
{

    $ApproveLevels = (int)getApproveLevel2('Purchase Request Note');
    $status = 1;
    global $db;
    global $userId;
    global $companyId;

    $psd = getMinPSDdate($orderNo, $orderYear);

    $sql = " INSERT INTO trn_prnheader (
				intPrnNo,
				intYear,
				intDepartment,
				intInternalUsage,
				dtmRequiredDate,
				dtmPrnDate,
				strRemarks,
				intStatus,
				intApproveLevels,
				intUser,
				intCompany,
				intModifiedBy,
				dtmModifiedDate,
				intOrderNo,
				intOrderYear,
				intPORaised,
				intHideInGrpPrnReport,
				PO_TYPE,
				BULK_REVISION_NO,
				SYSTEM_GENERATED 
				) VALUES 
				(
				'$prn_no',
				'$year',
				2,
				0,
				'$psd',
				DATE(NOW()),
				'System Generated PRN',
				'$status',
				'$ApproveLevels',
				'$userId',
				'$companyId',
				NULL,
				NULL,
				'$orderNo',
				'$orderYear',
				0,
				NULL,
				1,
				'$bulk_rev',
				1
				) ";
    $res = $db->RunQuery2($sql);
    if (!$res) {
        $response['type'] = 'fail';
        $response['msg'] = $db->errormsg;
        $response['sql'] = $sql;
    } else {
        $response['type'] = 'pass';
        $response['msg'] = '';
        $response['sql'] = '';
    }
    return $response;
}

function saveDetails($prn_no, $year, $orderNo, $orderYear)
{

    global $db;
    $saved = 0;
    $saved_p = 0;
    $saved_prn_qty = 0;

    //CHECK FOR TECHNIQUES WHICH DO NOT USE RM
    $non_rm_flag = getNonRMTechniquesFlag($orderNo, $orderYear);
    //-----------------------------------------
    $result_foil_update = update_unwanted_rm_qty_to_zero($orderNo, $orderYear);//update unwanted rm

    $sql = "SELECT
				trn_orderdetails.intSalesOrderId,
				trn_orderdetails.strSalesOrderNo,
				trn_orderdetails.intQty,
				trn_orderdetails.dblToleratePercentage,
				trn_orderdetails.dblOverCutPercentage,
				trn_orderdetails.dblDamagePercentage,
				trn_orderdetails.intSampleNo,
				trn_orderdetails.intSampleYear,
				trn_orderdetails.intRevisionNo,
				trn_orderdetails.strPrintName,
				trn_orderdetails.strCombo,
				trn_orderdetails.intPart,
				trn_orderdetails.dtPSD,
				trn_orderdetails.IS_GRADING,
				mst_locations.intCompanyId,
				DATEDIFF(date(trn_orderheader.dtmCreateDate),date('2017-05-18')) AS DiffDate
				FROM `trn_orderdetails`
				INNER JOIN trn_orderheader on 
trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo and trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
                INNER JOIN  mst_locations on mst_locations.intId = trn_orderheader.intLocationId 
				WHERE
				trn_orderdetails.intOrderNo 	= '$orderNo' AND
				trn_orderdetails.intOrderYear 	= '$orderYear' AND trn_orderdetails.SO_TYPE > -1";
    $result = $db->RunQuery2($sql);
    while ($row = mysqli_fetch_array($result)) {
        $sales_order = $row['intSalesOrderId'];
        $sales_order_no = $row['strSalesOrderNo'];
        $sampleNo = $row['intSampleNo'];
        $sampleYear = $row['intSampleYear'];
        $revision = $row['intRevisionNo'];
        $combo = $row['strCombo'];
        $print = $row['strPrintName'];
        $part = $row['intPart'];
        $grading_selected = $row['IS_GRADING'];
        $orderQty = $row['intQty'];
        $overCut = $row['dblOverCutPercentage'];
        $tollerance = $row['dblToleratePercentage'];
        $dammage = $row['dblDamagePercentage'];
        $psd = $row['dtPSD'];
        if ($row['intCompanyId'] == 43 || $row['intCompanyId'] == 81){
            $overCut = 0;
        }

        if ($row['DiffDate'] < 0)
            $qty = $orderQty + $orderQty * ($overCut + $tollerance + $dammage) / 100;
        else
            $qty = $orderQty + $orderQty * ($overCut + $dammage) / 100;

        $qty = ceil($qty);
        $response_q = update_bulk_po_qtys($prn_no, $year, $orderQty, $qty);
        if (!$response_q['res']) {
            $response['type'] = 'fail';
            $response['msg'] = $db->errormsg;
            $response['sql'] = $response_d['q'];
        }


        //----------foil-----------------------------------------
        $result_foil = foil_item_sampleCosumptionForSampleNo_sql($sampleNo, $sampleYear, $revision, $combo, $print, $part);
        while ($row_foil = mysqli_fetch_array($result_foil)) {
            $item = $row_foil['intItem'];
            $conPC_sample = $row_foil['CONSUMPTION'];
            $conPC = get_average_consumption($orderNo, $orderYear, $item, $sales_order, $conPC_sample, $grading_selected);
            $to_producrd = $qty;
            if ($conPC == 0 || $conPC == "") {
                $conPC = $conPC_sample;
            }
            $Qty = $qty * $conPC;

            $response_d = save_po_prn_details($orderNo, $orderYear, $item, $Qty);
            if (!$response_d['res']) {
                $response['type'] = 'fail';
                $response['msg'] = $db->errormsg;
                $response['sql'] = $response_d['q'];
            } else
                $saved_p++;
            $response_d = save_po_prn_details_so_wise($orderNo, $orderYear, $sales_order, $item, $Qty, $conPC, $to_producrd);
            if (!$response_d['res']) {
                $response['type'] = 'fail';
                $response['msg'] = $db->errormsg;
                $response['sql'] = $response_d['q'];
            } else
                $saved_p++;

        }
        //----------special RM-----------------------------------
        $result_sp = special_item_sampleCosumptionForSampleNo_sql($sampleNo, $sampleYear, $revision, $combo, $print, $part);
        while ($row_sp = mysqli_fetch_array($result_sp)) {
            $item = $row_sp['intItem'];
            $conPC_sample = $row_sp['CONSUMPTION'];
            $conPC = get_average_consumption($orderNo, $orderYear, $item, $sales_order, $conPC_sample, $grading_selected);
            $to_producrd = $qty;
            if ($conPC == 0 || $conPC == "") {
                $conPC = $conPC_sample;
            }
            $Qty = $qty * $conPC;

            $response_d = save_po_prn_details($orderNo, $orderYear, $item, $Qty);
            if (!$response_d['res']) {
                $response['type'] = 'fail';
                $response['msg'] = $db->errormsg;
                $response['sql'] = $response_d['q'];
            } else
                $saved_p++;
            $response_d = save_po_prn_details_so_wise($orderNo, $orderYear, $sales_order, $item, $Qty, $conPC, $to_producrd);
            if (!$response_d['res']) {
                $response['type'] = 'fail';
                $response['msg'] = $db->errormsg;
                $response['sql'] = $response_d['q'];
            } else
                $saved_p++;

        }
        //----------ink------------------------------------------
        $result_ink = ink_item_sampleCosumptionForSampleNo_sql($sampleNo, $sampleYear, $revision, $combo, $print, $part);
        while ($row_ink = mysqli_fetch_array($result_ink)) {
            $item = $row_ink['intItem'];
            $conPC_sample = $row_ink['CONSUMPTION'];
            $conPC = get_average_consumption($orderNo, $orderYear, $item, $sales_order, $conPC_sample, $grading_selected);
            $to_producrd = $qty;
            if ($conPC == 0 || $conPC == "") {
                $conPC = $conPC_sample;
            }
            $Qty = $qty * $conPC;

            $response_d = save_po_prn_details($orderNo, $orderYear, $item, $Qty);
            if (!$response_d['res']) {
                $response['type'] = 'fail';
                $response['msg'] = $db->errormsg;
                $response['sql'] = $response_d['q'];
            } else
                $saved_p++;
            $response_d = save_po_prn_details_so_wise($orderNo, $orderYear, $sales_order, $item, $Qty, $conPC, $to_producrd);
            if (!$response_d['res']) {
                $response['type'] = 'fail';
                $response['msg'] = $db->errormsg;
                $response['sql'] = $response_d['q'];
            } else
                $saved_p++;

        }

        //Non-direct RM-----------------------------------------------------------------
        $result_sp = non_direct_item_sampleCosumptionForSampleNo_sql($sampleNo, $sampleYear, $revision);
        while ($row_sp = mysqli_fetch_array($result_sp)) {
            $item = $row_sp['intItem'];
            $conPC_sample = $row_sp['CONSUMPTION'];
            $conPC = get_average_consumption($orderNo, $orderYear, $item, $sales_order, $conPC_sample, $grading_selected);
            $to_producrd = $qty;
            if ($conPC == 0 || $conPC == "") {
                $conPC = $conPC_sample;
            }
            $Qty = $qty * $conPC;

            $response_d = save_po_prn_details($orderNo, $orderYear, $item, $Qty);
            if (!$response_d['res']) {
                $response['type'] = 'fail';
                $response['msg'] = $db->errormsg;
                $response['sql'] = $response_d['q'];
            } else
                $saved_p++;
            $response_d = save_po_prn_details_so_wise($orderNo, $orderYear, $sales_order, $item, $Qty, $conPC, $to_producrd);
            if (!$response_d['res']) {
                $response['type'] = 'fail';
                $response['msg'] = $db->errormsg;
                $response['sql'] = $response_d['q'];
            } else
                $saved_p++;

        }
        //End -Non-direct RM------------------------------------------------------------

    }//end of order detail's while loop

    //GENERATE PRN NOTE-------------------------------------------------------------

    $psd = getMinimumPSDdate($orderNo, $orderYear);
    $sql = "SELECT
				PO_PR.ITEM,
				PO_PR.REQUIRED,
				PO_PR.PRN_QTY,
				PO_PR.PURCHASED_QTY,
				PO_PR.PRN_QTY_USED_OTHERS,
				PO_PR.PRN_QTY_BALANCED_FROM_DUMMY,
				(REQUIRED-((IF(PRN_QTY<0,0,PRN_QTY))-PRN_QTY_USED_OTHERS)-PRN_QTY_BALANCED_FROM_DUMMY) AS bal_to_prn1,
				(REQUIRED-((PURCHASED_QTY)-PRN_QTY_USED_OTHERS)-PRN_QTY_BALANCED_FROM_DUMMY) AS bal_to_prn2,
				(SELECT
				  if(((pdd.PRN_QTY)+pdd.PRN_QTY_BALANCED_FROM_DUMMY-(pdd.REQUIRED+pdd.PRN_QTY_USED_OTHERS))<0,0,((pdd.PRN_QTY)+pdd.PRN_QTY_BALANCED_FROM_DUMMY-(pdd.REQUIRED+pdd.PRN_QTY_USED_OTHERS))) usabal_from_dummy 
				FROM
				trn_po_prn_details as pdd
				INNER JOIN trn_orderheader ON pdd.ORDER_NO = trn_orderheader.DUMMY_PO_NO 
				AND pdd.ORDER_YEAR = trn_orderheader.DUMMY_PO_YEAR
				INNER JOIN trn_po_prn_details AS pd ON trn_orderheader.intOrderNo = pd.ORDER_NO 
				AND trn_orderheader.intOrderYear = pd.ORDER_YEAR AND pdd.ITEM = pd.ITEM
				WHERE
				pd.ORDER_NO = PO_PR.ORDER_NO AND
				pd.ORDER_YEAR = PO_PR.ORDER_YEAR AND
				pd.ITEM = PO_PR.ITEM ) usabal_from_dummy1,
				(SELECT
				  if(((pdd.PURCHASED_QTY)+pdd.PRN_QTY_BALANCED_FROM_DUMMY-(pdd.REQUIRED+pdd.PRN_QTY_USED_OTHERS))<0,0,((pdd.PURCHASED_QTY)+pdd.PRN_QTY_BALANCED_FROM_DUMMY-(pdd.REQUIRED+pdd.PRN_QTY_USED_OTHERS))) usabal_from_dummy 
				FROM
				trn_po_prn_details as pdd
				INNER JOIN trn_orderheader ON pdd.ORDER_NO = trn_orderheader.DUMMY_PO_NO 
				AND pdd.ORDER_YEAR = trn_orderheader.DUMMY_PO_YEAR
				INNER JOIN trn_po_prn_details AS pd ON trn_orderheader.intOrderNo = pd.ORDER_NO 
				AND trn_orderheader.intOrderYear = pd.ORDER_YEAR AND pdd.ITEM = pd.ITEM
				WHERE
				pd.ORDER_NO = PO_PR.ORDER_NO AND
				pd.ORDER_YEAR = PO_PR.ORDER_YEAR AND
				pd.ITEM = PO_PR.ITEM ) usabal_from_dummy2
 FROM
				trn_po_prn_details AS PO_PR
				WHERE 
				PO_PR.ORDER_NO = '$orderNo' AND
				PO_PR.ORDER_YEAR = '$orderYear'  
				GROUP BY
				PO_PR.ITEM
				";
    $result = $db->RunQuery2($sql);
    while ($row = mysqli_fetch_array($result)) {
        $item = $row['ITEM'];
        $bulkRequired = ($row['REQUIRED'] == '' ? 0 : $row['REQUIRED']);
        $usabal_from_dummy = max($row['usabal_from_dummy1'], $row['usabal_from_dummy2']);
        $prn_used_from_dummy = (($usabal_from_dummy == '' || $usabal_from_dummy) < 0 ? 0 : $usabal_from_dummy);
        $prn_used_from_dummy = min($bulkRequired, $prn_used_from_dummy);
        $prn_used_from_dummy = round($prn_used_from_dummy, 8);
        $bal_to_prn = min($row['bal_to_prn1'], $row['bal_to_prn2']);
        $prnQty = ($bal_to_prn - $prn_used_from_dummy);
        $prnQty = ($prnQty == '' ? 0 : $prnQty);
        $prnQty = ($prnQty < 0 ? 0 : $prnQty);
        $prnQty = round($prnQty, 8);

        $response_u = update_po_prn_details($orderNo, $orderYear, $item, $bulkRequired, $prnQty, $prn_used_from_dummy);
        if (!$response_u['res']) {
            $response['type'] = 'fail';
            $response['msg'] = $db->errormsg;
            $response['sql'] = $response_u['q'];
        }

        if ($prnQty > 0) {
            $response_p = insertPRNDetail($orderNo, $orderYear, $prn_no, $year, 'NULL', $sales_order_no, $item, $bulkRequired, $prnQty, $prn_used_from_dummy, 0, $psd);
            if (!$response_p['res']) {
                $response['type'] = 'fail';
                $response['msg'] = $db->errormsg;
                $response['sql'] = $response_p['q'];
            }
        }
        if ($response_p['res'])
            $saved++;
        $saved_prn_qty += $prnQty;;
    }
    //----------------------------------------------------------------


    if ($saved_p == 0 && $non_rm_flag == 0) {
        $response['type'] = 'fail';
        $response['msg'] = "No RM to save";
        $response['sql'] = $response['sql'];
        $response['qty'] = 0;
    } /*	else if($saved==0){
			$response['type'] 		= 'fail';
			$response['msg'] 		= "No RM to save";
			$response['sql'] 		= '';
			$response['qty'] 		=0;
	}*/
    else {
        $response['type'] = 'pass';
        $response['msg'] = "Saved successfully";
        $response['sql'] = '';
        $response['qty'] = $saved_prn_qty;
    }


    return $response;

}


function ink_item_sampleCosumptionForSampleNo_sql($sampleNo, $sampleYear, $revision, $combo, $print, $part)
{
    global $db;
    $sql = " select  
					intItem,
					ROUND(SUM(dblColorWeight/sumWeight*dblWeight /dblNoOfPcs),9) as CONSUMPTION 
					 from (  SELECT 
					 SCR.intItem,
					SCR.dblWeight,
					trn_sampleinfomations_details_technical.dblColorWeight ,
					mst_units.dblNoOfPcs ,
					
					 (SELECT Sum(trn_sample_color_recipes.dblWeight) AS sumWeight 
					FROM trn_sample_color_recipes 
					WHERE trn_sample_color_recipes.intSampleNo = SCR.intSampleNo 
					AND trn_sample_color_recipes.intSampleYear = SCR.intSampleYear 
					AND trn_sample_color_recipes.intRevisionNo = SCR.intRevisionNo 
					AND trn_sample_color_recipes.strCombo = SCR.strCombo 
					AND trn_sample_color_recipes.strPrintName = SCR.strPrintName 
					AND trn_sample_color_recipes.intTechniqueId = SCR.intTechniqueId 
					AND trn_sample_color_recipes.intInkTypeId = SCR.intInkTypeId 
					AND trn_sample_color_recipes.intColorId = SCR.intColorId 
					) as sumWeight 
					FROM  trn_sample_color_recipes  as SCR 
					INNER JOIN trn_sampleinfomations_details AS SD ON SCR.intSampleNo = SD.intSampleNo AND SCR.intSampleYear = SD.intSampleYear AND SCR.intRevisionNo = SD.intRevNo AND SCR.strCombo = SD.strComboName AND SCR.strPrintName = SD.strPrintName AND SCR.intColorId = SD.intColorId AND SCR.intTechniqueId = SD.intTechniqueId
					Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = SCR.intSampleNo 
					AND trn_sampleinfomations_details_technical.intSampleYear = SCR.intSampleYear 
					AND trn_sampleinfomations_details_technical.intRevNo = SCR.intRevisionNo 
					AND trn_sampleinfomations_details_technical.strComboName = SCR.strCombo 
					AND trn_sampleinfomations_details_technical.strPrintName = SCR.strPrintName 
					AND trn_sampleinfomations_details_technical.intColorId = SCR.intColorId 
					AND trn_sampleinfomations_details_technical.intInkTypeId = SCR.intInkTypeId 
					Inner Join mst_item ON mst_item.intId = SCR.intItem 
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
					Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory 
					Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory 
					WHERE SCR.intSampleNo='$sampleNo' 
					AND SCR.intSampleYear='$sampleYear' 
					AND SCR.intRevisionNo='$revision' 
					AND SCR.strCombo='$combo' 
					AND SCR.strPrintName='$print' 
					group by 
SCR.intTechniqueId 
					,SCR.intInkTypeId 
					, SCR.intColorId 
,SCR.intItem
					 ) as t 
group by intItem

		";
    $result = $db->RunQuery2($sql);
    return $result;

}

function foil_item_sampleCosumptionForSampleNo_sql($sampleNo, $sampleYear, $revision, $combo, $print, $part)
{

    global $db;
    $sql = "
					SELECT 
					mst_item.intId as intItem,
					sum(trn_sample_foil_consumption.dblMeters) as CONSUMPTION
					FROM
					trn_sample_foil_consumption  
					Inner Join mst_item ON mst_item.intId = trn_sample_foil_consumption.intItem
					Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
					Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
					Inner Join mst_units ON mst_units.intId = mst_item.intUOM
					Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_item.intCurrency
					WHERE
					trn_sample_foil_consumption.intSampleNo = '$sampleNo' AND
					trn_sample_foil_consumption.intSampleYear = '$sampleYear' AND
					trn_sample_foil_consumption.intRevisionNo = '$revision' AND
					trn_sample_foil_consumption.strCombo = '$combo' AND
					trn_sample_foil_consumption.strPrintName = '$print' AND 
					trn_sample_foil_consumption.dblMeters > 0
					GROUP BY
					mst_maincategory.intId,
					mst_subcategory.intId,
					mst_item.intId				";
    //echo $sql;
    $result = $db->RunQuery2($sql);
    return $result;

}

function update_unwanted_rm_qty_to_zero($orderNo, $orderYear)
{

    global $db;

    $sql = "SELECT
			trn_po_prn_details_sales_order.ORDER_NO,
			trn_po_prn_details_sales_order.ORDER_YEAR,
			trn_po_prn_details_sales_order.SALES_ORDER,
			trn_po_prn_details_sales_order.ITEM,
			trn_orderdetails.intSampleNo,
			trn_orderdetails.intSampleYear,
			trn_orderdetails.intRevisionNo,
			trn_orderdetails.strCombo,
			trn_orderdetails.strPrintName,
			trn_orderdetails.intPart
			FROM
			trn_po_prn_details_sales_order
			INNER JOIN trn_orderdetails ON trn_po_prn_details_sales_order.ORDER_NO = trn_orderdetails.intOrderNo AND trn_po_prn_details_sales_order.ORDER_YEAR = trn_orderdetails.intOrderYear AND trn_po_prn_details_sales_order.SALES_ORDER = trn_orderdetails.intSalesOrderId

			WHERE
			trn_po_prn_details_sales_order.ORDER_NO = '$orderNo' AND
			trn_po_prn_details_sales_order.ORDER_YEAR = '$orderYear'    
			";
    $result = $db->RunQuery2($sql);
    while ($row = mysqli_fetch_array($result)) {
        $intSampleNo = $row['intSampleNo'];
        $intSampleYear = $row['intSampleYear'];
        $intRevisionNo = $row['intRevisionNo'];
        $strCombo = $row['strCombo'];
        $strPrintName = $row['strPrintName'];
        $intPart = $row['intPart'];
        $item = $row['ITEM'];
        $available = check_availablity($intSampleNo, $intSampleYear, $intRevisionNo, $strCombo, $strPrintName, $intPart, $item);
        if (!$available) {
            $sql = "UPDATE `trn_po_prn_details_sales_order` SET `REQUIRED`='0' WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item')";
            $result = $db->RunQuery2($sql);
        }
    }

}

function special_item_sampleCosumptionForSampleNo_sql($sampleNo, $sampleYear, $revision, $combo, $print, $part)
{
    global $db;

    $sql =
        "
				SELECT
				mst_item.intId as intItem,
				sum(trn_sample_spitem_consumption.dblQty/dblNoOfPcs) as CONSUMPTION
				FROM
				trn_sample_spitem_consumption 
				Inner Join mst_item ON mst_item.intId = trn_sample_spitem_consumption.intItem
				Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
				Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
				Inner Join mst_units ON mst_units.intId = mst_item.intUOM
				Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_item.intCurrency
				WHERE
				trn_sample_spitem_consumption.intSampleNo = '$sampleNo' AND
				trn_sample_spitem_consumption.intSampleYear = '$sampleYear' AND
				trn_sample_spitem_consumption.intRevisionNo = '$revision' AND
				trn_sample_spitem_consumption.strCombo = '$combo' AND
				trn_sample_spitem_consumption.strPrintName = '$print' AND 
				trn_sample_spitem_consumption.dblQty > 0 
				GROUP BY
				mst_maincategory.intId,
				mst_subcategory.intId,
				mst_item.intId";

    $result = $db->RunQuery2($sql);
    return $result;

}

function non_direct_item_sampleCosumptionForSampleNo_sql($sampleNo, $sampleYear, $revision)
{

    global $db;
    $sql = "
					SELECT 
					trn_sample_non_direct_rm_consumption.ITEM as intItem,
					sum(trn_sample_non_direct_rm_consumption.CONSUMPTION) as CONSUMPTION
					FROM
					trn_sample_non_direct_rm_consumption  
 					WHERE
					trn_sample_non_direct_rm_consumption.SAMPLE_NO = '$sampleNo' AND
					trn_sample_non_direct_rm_consumption.SAMPLE_YEAR = '$sampleYear' AND
					trn_sample_non_direct_rm_consumption.REVISION_NO = '$revision' 
					group by 
					trn_sample_non_direct_rm_consumption.ITEM ";

    $result = $db->RunQuery2($sql);
    return $result;

}


function insertPRNDetail($orderNo, $orderYear, $prn_no, $year, $sales_order_id, $sales_order_no, $item, $bulkRequired, $prnQty, $prn_used_from_dummy, $poQty, $psd)
{

    global $db;

    $sql = "INSERT INTO trn_prndetails (
				intPrnNo,
				intYear,
				intItem,
				intOrderNo,
				intOrderYear,
				SALES_ORDER_ID,
				strSalesOrderNo,
				BULK_REQUIRED,
				QTY_FROM_DUMMY,
				dblPrnQty,
				dblPoQty,
				REQUIRED_DATE
				)
				VALUES
				(
				'$prn_no',
				'$year',
				'$item',
				'$orderNo',
				'$orderYear',
				$sales_order_id,
				'$sales_order_no',
				'$bulkRequired',
				'$prn_used_from_dummy',
				'$prnQty',
				'$poQty',
				'$psd'
				)";
    $result = $db->RunQuery2($sql);
    $response['res'] = $result;
    $response['msg'] = $db->errormsg;
    $response['q'] = $sql;
    return $response;
}

function getApprovedPurchasedQty($orderNo, $orderYear, $item)
{

    global $db;

    $sql = "SELECT
				sum(trn_podetails.dblQty) as poQty
				FROM
				trn_podetails
				INNER JOIN trn_poheader ON trn_podetails.intPONo = trn_poheader.intPONo AND trn_podetails.intPOYear = trn_poheader.intPOYear
				INNER JOIN trn_prndetails ON trn_podetails.intPrnNo = trn_prndetails.intPrnNo AND trn_podetails.intPrnYear = trn_prndetails.intYear AND trn_podetails.intItem = trn_prndetails.intItem
				WHERE
				trn_poheader.intStatus = 1 AND
				trn_prndetails.intOrderNo = '$orderNo' AND
				trn_prndetails.intOrderYear = '$orderYear' AND
				trn_podetails.intItem = '$item'";

    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $qty = $row['poQty'];

    return $qty;
}

function generatePRNemail($prn_no, $year, $orderNo, $orderYear, $prn_saved_qty)
{
    //ini_set('display_errors',1);
    global $objMail;
    global $companyId;
    global $db;
    global $obj_commom;
    $content2 = '';

    $prn_no_generated = $prn_no;
    $prn_year_generated = $year;

    $sql = "SELECT
			trn_orderheader.DUMMY_PO_NO ,
			trn_orderheader.DUMMY_PO_YEAR ,
			trn_orderheader.intOrderNo,
			trn_orderheader.intOrderYear
			FROM
			trn_orderheader  
			WHERE
			trn_orderheader.intOrderNo = '$orderNo' AND
			trn_orderheader.intOrderYear = '$orderYear'
			";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $o_n = $row['intOrderNo'] . '/' . $row['intOrderYear'];
    if ($row['DUMMY_PO_YEAR'] > 0)
        $d_n = $row['DUMMY_PO_NO'] . "/" . $row['DUMMY_PO_YEAR'];
    else
        $d_n = '';


    $sql = "SELECT
			GROUP_CONCAT(distinct trn_orderdetails.strGraphicNo) as str_strGraphicNo ,
			trn_orderheader.PO_TYPE
			FROM
			trn_orderdetails
			INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
			WHERE
			trn_orderdetails.intOrderNo = '$orderNo' AND
			trn_orderdetails.intOrderYear = '$orderYear' AND trn_orderdetails.SO_TYPE > -1
			";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $graphic = $row['str_strGraphicNo'];
    $flag_dummy = $row['PO_TYPE'];
    if ($flag_dummy == 1)
        $str_pre_costing = " - Pre Costing of ERP no " . $o_n;
    else if ($flag_dummy == 2)
        $str_pre_costing = " - Split Costing of Pre Costing ERP no " . $d_n;
    else
        $str_pre_costing = " - Costing of ERP no " . $o_n;

    /*
		$link=$_SESSION['MAIN_URL']."?q=917&prnNo=$prn_no&year=$year";
		$statement	="Click here to view system generated PRN for ".$str_pre_costing." <a  href= ".$link.">".$prn_no."/".$year." Graphics(".$graphic." )</a>";*/
    //////////////////////////////APPROVED/////////////////////////////
    $sql = "SELECT
				trn_prnheader.intPrnNo as no,
				trn_prnheader.intYear as year ,
				trn_prnheader.BULK_PO_QTY,
				trn_prnheader.BULK_PO_QTY_RCV_ABLE 
				FROM `trn_prnheader`
				WHERE
				trn_prnheader.intOrderNo = '$orderNo' AND
				trn_prnheader.intOrderYear = '$orderYear' AND
				trn_prnheader.intStatus = 1
				GROUP BY
				trn_prnheader.intPrnNo,
				trn_prnheader.intYear
				";
    $result = $db->RunQuery2($sql);
    $statement = '';
    $content1 = '';
    $i = 0;
    while ($row = mysqli_fetch_array($result)) {
        $i++;
        $year = $row['year'];
        $prn_no = $row['no'];
        $bulk_qty = $row['BULK_PO_QTY'];
        $bulk_po_rcv_able = $row['BULK_PO_QTY_RCV_ABLE'];
        $link = $_SESSION['MAIN_URL'] . "?q=917&prnNo=$prn_no&year=$year";
        $content1 .= "<tr><td>&nbsp;</td><td class='normalfnt'><a  href= " . $link . ">$prn_no/$year</a></td>	<td>&nbsp;</td></tr>";
        $content1 .= "<tr><td>&nbsp;</td><td class='normalfnt'>Customer Order Qty/Customer Order Receivable Qty</td><td>" . $bulk_qty . "/" . $bulk_po_rcv_able . "</td></tr>";
    }
    if ($i > 0) {
        $statement .= "<tr><td>&nbsp;</td><td class='normalfnt'><b>Click Following links to view Approved PRN</b></td>	<td>&nbsp;</td></tr>" . $content1 . "<tr><td>&nbsp;</td><td class='normalfnt'></td>	<td>&nbsp;</td></tr>";
    }

    ///////////////////////////////CANCELLED///////////////////////
    $sql = "SELECT
				trn_prnheader.intPrnNo as no,
				trn_prnheader.intYear as year,
				trn_prnheader.BULK_PO_QTY,
				trn_prnheader.BULK_PO_QTY_RCV_ABLE 
				FROM `trn_prnheader`
				WHERE
				trn_prnheader.SYSTEM_CANCELLATION = 1 AND
				trn_prnheader.intOrderNo = '$orderNo' AND
				trn_prnheader.intOrderYear = '$orderYear' AND
				trn_prnheader.intStatus = -10
				GROUP BY
				trn_prnheader.intPrnNo,
				trn_prnheader.intYear
				";
    $result = $db->RunQuery2($sql);
    $content_c = '';
    $content_c1 = '';
    $i = 0;
    while ($row = mysqli_fetch_array($result)) {
        $i++;
        $year = $row['year'];
        $prn_no = $row['no'];
        $bulk_qty = $row['BULK_PO_QTY'];
        $bulk_po_rcv_able = $row['BULK_PO_QTY_RCV_ABLE'];
        $link = $_SESSION['MAIN_URL'] . "?q=917&prnNo=$prn_no&year=$year";
        $content_c1 .= "<tr><td>&nbsp;</td><td class='normalfnt'><a  href= " . $link . ">$prn_no/$year</a></td>	<td>&nbsp;</td></tr>";
        $content_c1 .= "<tr><td>&nbsp;</td><td class='normalfnt'>Customer Order Qty/Customer Order Receivable Qty</td><td>" . $bulk_qty . "/" . $bulk_po_rcv_able . "</td></tr>";
    }
    if ($i > 0) {
        $content_c .= "<tr><td>&nbsp;</td><td class='normalfnt'><b>Click Following links to view auto Cancelled PRN</b></td>	<td>&nbsp;</td></tr>" . $content_c1 . "<tr><td>&nbsp;</td><td class='normalfnt'></td>	<td>&nbsp;</td></tr>";
    }

    if ($statement != '') {//approved
        $content = "<tr><td>&nbsp;</td>
			<td colspan='4' class='normalfnt'>$statement</td>
			</tr>";
    }
    if ($content_c != '') {//cancelled
        $content .= "<tr><td>&nbsp;</td>
			<td colspan='4' class='normalfnt'>$content_c</td>
			</tr>";
    }

    $sql = "SELECT
			trn_po_prn_details.ITEM,
			trn_po_prn_details.REQUIRED,
			trn_po_prn_details.PRN_QTY_USED_OTHERS,
			trn_po_prn_details.PURCHASED_QTY,
			trn_po_prn_details.PRN_QTY AS PRN_QTY,
			(IFNULL(trn_po_prn_details.REQUIRED,0) + IFNULL(PRN_QTY_USED_OTHERS,0)) AS NEW_PRN_QTY,
			mst_item.strName,
			group_concat(trn_podetails.intPONo,'/',trn_podetails.intPOYear) as sup_po,
			group_concat(trn_prndetails.intPrnNo,'/',trn_prndetails.intYear) as prn
			FROM
			trn_po_prn_details
			INNER JOIN mst_item ON trn_po_prn_details.ITEM = mst_item.intId
			INNER JOIN trn_prndetails ON trn_po_prn_details.ORDER_NO = trn_prndetails.intOrderNo AND trn_po_prn_details.ORDER_YEAR = trn_prndetails.intOrderYear AND trn_po_prn_details.ITEM = trn_prndetails.intItem
			INNER JOIN trn_podetails ON trn_prndetails.intPrnNo = trn_podetails.intPrnNo AND trn_prndetails.intYear = trn_podetails.intPrnYear AND trn_prndetails.intItem = trn_podetails.intItem
			INNER JOIN trn_poheader ON trn_podetails.intPONo = trn_poheader.intPONo AND trn_podetails.intPOYear = trn_poheader.intPOYear
			WHERE
			trn_po_prn_details.ORDER_NO = '$orderNo' AND
			trn_po_prn_details.ORDER_YEAR = '$orderYear' AND 
			(IFNULL(trn_po_prn_details.REQUIRED,0) +IFNULL(PRN_QTY_USED_OTHERS,0)) < IFNULL(trn_po_prn_details.PURCHASED_QTY,0) AND
trn_poheader.intStatus = 1 
group by mst_item.strName";
    $result = $db->RunQuery2($sql);
    $content2 .= "<tr>
		<td class='normalfnt' colspan='6'><b>PRN qtys of following items have been reduced.If it is possible, please revise the supplier po for new PRN quantities</b></td>
		</tr>
		<tr><td colspan='6'><table  width='595' border='0' cellspacing='0' cellpadding='2' class='tblBorder'>
		<td>Supplier PO</td>
		<td>PRN</td>
		<td>Item</td>
		<td>Already PO</td>
		<td>Production Required</td>
		<td>Active PRN Qty</td>
		<td>Active PRN Qty-must be Cleared</td></tr>";
    $i = 0;
    while ($row = mysqli_fetch_array($result)) {
        $i++;
        $sup_po_no = $row['sup_po'];
        $prn_no = $row['prn'];
        $item = $row['strName'];
        $new_prn = $row['NEW_PRN_QTY'];
        $old_po = $row['PURCHASED_QTY'];
        $prn_qty = round($row['PRN_QTY'], 6);
        $bal_to_prn_qty = round($row['PRN_QTY'] - $row['REQUIRED'], 6);
        $bgcolor = (round(($row['PRN_QTY'] - $row['REQUIRED']), 6) <= 0) ? '#5DD087' : '#F2898B';
        $content2 .= "
		<tr><td class='normalfnt'>" . $sup_po_no . "</td>
		<td class='normalfnt'>" . $prn_no . "</td>
		<td class='normalfnt'>" . $item . "</td>
		<td class='normalfnt'>" . $old_po . "</td>
		<td class='normalfnt' bgcolor='#5DD087'>" . $new_prn . "</td>
		<td class='normalfnt' bgcolor='$bgcolor'>" . $prn_qty . "</td>
		<td class='normalfnt' bgcolor='$bgcolor'>" . $bal_to_prn_qty . "</td>
		</tr>";
    }
    $content2 .= '</table></td></tr>';
    if ($i == 0)
        $content2 = '';

    $nowDate = date('Y-m-d');
    $mailHeader = "SYSTEM GENERATED PRN (" . $prn_no . "/" . $year . ") " . $str_pre_costing . " - Graphics(" . $graphic . " )";
    $FROM_NAME = "NSOFT";
    $FROM_EMAIL = '';

    $sql = "SELECT
				sys_mail_eventusers.intUserId ,
				sys_users.strUserName 
				FROM
				sys_mail_eventusers
				INNER JOIN sys_users ON sys_mail_eventusers.intUserId = sys_users.intUserId
				WHERE
				sys_mail_eventusers.intMailEventId = 1031 AND
				sys_mail_eventusers.intCompanyId = '$companyId' AND
				sys_users.intStatus = 1
				GROUP BY
				sys_mail_eventusers.intUserId";
    $result = $db->RunQuery2($sql);
    while ($row = mysqli_fetch_array($result)) {

        $receiver_name = $row['strUserName'];
        $mail_TO = $obj_commom->getEmailList2($row['intUserId']);

        ob_start();
        include "rptBulkOrder-db-prn_mail_template.php";
        $body = ob_get_clean();

        //$objMail->insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
        insertTable2($FROM_EMAIL, $FROM_NAME, $mail_TO, $mailHeader, $body, '', '');

    }

}


function cancelPRNandPOs($orderNo, $orderYear, $action)
{


    global $db;

    $sql = "SELECT
			trn_orderheader.DUMMY_PO_NO ,
			trn_orderheader.DUMMY_PO_YEAR ,
			trn_orderheader.intOrderNo,
			trn_orderheader.intOrderYear
			FROM
			trn_orderheader  
			WHERE
			trn_orderheader.intOrderNo = '$orderNo' AND
			trn_orderheader.intOrderYear = '$orderYear'
			";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $o_n = $row['intOrderNo'] . '/' . $row['intOrderYear'];
    if ($row['DUMMY_PO_YEAR'] > 0)
        $d_n = $row['DUMMY_PO_NO'] . "/" . $row['DUMMY_PO_YEAR'];
    else
        $d_n = '';


    $sql = "SELECT
			GROUP_CONCAT(distinct trn_orderdetails.strGraphicNo) as str_strGraphicNo ,
			trn_orderheader.PO_TYPE
			FROM
			trn_orderdetails
			INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
			WHERE
			trn_orderdetails.intOrderNo = '$orderNo' AND
			trn_orderdetails.intOrderYear = '$orderYear'
			";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $graphic = $row['str_strGraphicNo'];
    $flag_dummy = $row['PO_TYPE'];

    if ($flag_dummy == 1)
        $str_pre_costing = " - Pre Costing of ERP no " . $o_n;
    else if ($flag_dummy == 2)
        $str_pre_costing = " - Split Costing of Pre Costing ERP no " . $d_n;
    else
        $str_pre_costing = " - Costing of ERP no " . $o_n;

    $sql_p = "select 
				trn_po_prn_details.ORDER_NO,
				trn_po_prn_details.ORDER_YEAR,
				trn_po_prn_details.ITEM,
				trn_po_prn_details.REQUIRED,
				trn_po_prn_details.PRN_QTY,
				trn_po_prn_details.PURCHASED_QTY,
				trn_po_prn_details.PRN_QTY_USED_OTHERS,
				trn_po_prn_details.PRN_QTY_BALANCED_FROM_DUMMY from `trn_po_prn_details` WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear')";
    $result_p = $db->RunQuery2($sql_p);
    while ($row_p = mysqli_fetch_array($result_p)) {

        $orderNo = $row_p['ORDER_NO'];
        $orderYear = $row_p['ORDER_YEAR'];
        $item = $row_p['ITEM'];


        $sql_s = "select PRN_QTY_BALANCED_FROM_DUMMY from `trn_po_prn_details` WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`ITEM`='$item')";
        $result_s = $db->RunQuery2($sql_s);
        $rows = mysqli_fetch_array($result_s);
        $used_dummy = $rows['PRN_QTY_BALANCED_FROM_DUMMY'];

        $sql_c = "UPDATE `trn_po_prn_details` SET `REQUIRED`=0 , PRN_QTY_BALANCED_FROM_DUMMY =0 WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`ITEM`='$item')";
        $result_c = $db->RunQuery2($sql_c);

        $sql_c_s = "UPDATE `trn_po_prn_details_sales_order` SET `REQUIRED`='0' WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear')  AND (`ITEM`='$item')";
        $result_c_s = $db->RunQuery2($sql_c_s);


        $sql_d = "update 
					trn_po_prn_details
					INNER JOIN trn_orderheader ON trn_po_prn_details.ORDER_NO = trn_orderheader.DUMMY_PO_NO 
					AND trn_po_prn_details.ORDER_YEAR = trn_orderheader.DUMMY_PO_YEAR
					INNER JOIN trn_po_prn_details AS pd ON trn_orderheader.intOrderNo = pd.ORDER_NO 
					AND trn_orderheader.intOrderYear = pd.ORDER_YEAR AND trn_po_prn_details.ITEM = pd.ITEM
					set trn_po_prn_details.PRN_QTY_USED_OTHERS = IFNULL(trn_po_prn_details.PRN_QTY_USED_OTHERS,0)-'$used_dummy'
					WHERE
					pd.ORDER_NO = '$orderNo' AND
					pd.ORDER_YEAR = '$orderYear' 
					 AND (pd.`ITEM`='$item') ";

        $result_d = $db->RunQuery2($sql_d);

    }

    //----------------------------------------------------------------------
    /* $sql = "select
		 trn_prndetails.intOrderNo,
		 trn_prndetails.intOrderYear,
		 trn_prndetails.intItem ,
		 trn_prndetails.dblPrnQty
		 from
		 trn_prnheader
left JOIN trn_prndetails ON trn_prnheader.intPrnNo = trn_prndetails.intPrnNo AND trn_prnheader.intYear = trn_prndetails.intYear
left JOIN trn_podetails ON trn_podetails.intPrnNo = trn_prndetails.intPrnNo AND trn_podetails.intPrnYear = trn_prndetails.intYear
left JOIN trn_poheader ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
WHERE (trn_poheader.intStatus >1 or trn_poheader.intStatus =0 or trn_poheader.intStatus is null ) and trn_prnheader.`intStatus`<>1 and (trn_prnheader.`intOrderNo`='$orderNo') AND (trn_prnheader.`intOrderYear`='$orderYear') ";*/

    //-----------------------------------------------------------------------
    /*	 $sql = "select
		 trn_prndetails.intOrderNo,
		 trn_prndetails.intOrderYear,
		 trn_prndetails.intItem ,
		 trn_prndetails.dblPrnQty
		 from
		 trn_prnheader
left JOIN trn_prndetails ON trn_prnheader.intPrnNo = trn_prndetails.intPrnNo AND trn_prnheader.intYear = trn_prndetails.intYear
left JOIN trn_podetails ON trn_podetails.intPrnNo = trn_prndetails.intPrnNo AND trn_podetails.intPrnYear = trn_prndetails.intYear
left JOIN trn_poheader ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
WHERE (trn_poheader.intStatus >1 or trn_poheader.intStatus =0 or trn_poheader.intStatus is null ) and trn_prnheader.`intStatus`=1 and (trn_prnheader.`intOrderNo`='$orderNo') AND (trn_prnheader.`intOrderYear`='$orderYear') ";
		$result = $db->RunQuery2($sql);
		while($row=mysqli_fetch_array($result)){

			$item	=$row['intItem'];
			$prnQty	=$row['dblPrnQty'];*/

    /*	$sql_s	="select PRN_QTY_BALANCED_FROM_DUMMY from `trn_po_prn_details` WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`ITEM`='$item')";
			$result_s = $db->RunQuery2($sql_s);
			$rows=mysqli_fetch_array($result_s);
			$used_dummy	= $rows['PRN_QTY_BALANCED_FROM_DUMMY'];*/

    /*$sql_c	="UPDATE trn_po_prn_details SET  PRN_QTY =PRN_QTY-'$prnQty'  WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`ITEM`='$item')";
			$result_c = $db->RunQuery2($sql_c);*/

    /*	$sql_d	="update
					trn_po_prn_details
					INNER JOIN trn_orderheader ON trn_po_prn_details.ORDER_NO = trn_orderheader.DUMMY_PO_NO
					AND trn_po_prn_details.ORDER_YEAR = trn_orderheader.DUMMY_PO_YEAR
					INNER JOIN trn_po_prn_details AS pd ON trn_orderheader.intOrderNo = pd.ORDER_NO
					AND trn_orderheader.intOrderYear = pd.ORDER_YEAR AND trn_po_prn_details.ITEM = pd.ITEM
					set trn_po_prn_details.PRN_QTY_USED_OTHERS = IFNULL(trn_po_prn_details.PRN_QTY_USED_OTHERS,0)-'$used_dummy'
					WHERE
					pd.ORDER_NO = '$orderNo' AND
					pd.ORDER_YEAR = '$orderYear'
					 AND (pd.`ITEM`='$item') ";

			$result_d = $db->RunQuery2($sql_d); */
    //	}


    $sql = "UPDATE trn_prnheader
left JOIN trn_prndetails ON trn_prnheader.intPrnNo = trn_prndetails.intPrnNo AND trn_prnheader.intYear = trn_prndetails.intYear 
left JOIN trn_podetails ON trn_podetails.intPrnNo = trn_prndetails.intPrnNo AND trn_podetails.intPrnYear = trn_prndetails.intYear 
left JOIN trn_poheader ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
 SET trn_prnheader.`intStatus`=-10,trn_prnheader.`SYSTEM_CANCELLATION`=1  WHERE (trn_poheader.intStatus >1 or trn_poheader.intStatus =0 or trn_poheader.intStatus is null ) and (trn_prnheader.`intOrderNo`='$orderNo') AND (trn_prnheader.`intOrderYear`='$orderYear') ";
    $result = $db->RunQuery2($sql);

    $sql = "UPDATE trn_podetails
INNER JOIN trn_poheader ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
INNER JOIN trn_prndetails ON trn_podetails.intPrnNo = trn_prndetails.intPrnNo AND trn_podetails.intPrnYear = trn_prndetails.intYear 
SET trn_podetails.`CLEARED_QTY`=trn_podetails.`dblQty`, trn_podetails.`dblQty`=0 ,trn_podetails.CLEARED_REASON = '$action'  WHERE 
trn_prndetails.intOrderNo = '$orderNo' AND
trn_prndetails.intOrderYear = '$orderYear' and 
(trn_poheader.intStatus >1 or trn_poheader.intStatus =0)";
    $result = $db->RunQuery2($sql);

//-----------------------------------------------------------
    $sql = "select 
					trn_po_prn_details.ITEM,
					(select sum(trn_prndetails.dblPrnQty)
					from 
					trn_prnheader
					left JOIN trn_prndetails ON trn_prnheader.intPrnNo = trn_prndetails.intPrnNo 
					AND trn_prnheader.intYear = trn_prndetails.intYear 
					where  trn_prnheader.`intStatus`=1 and (trn_prnheader.`intOrderNo`=trn_po_prn_details.ORDER_NO) AND (trn_prnheader.`intOrderYear`=trn_po_prn_details.ORDER_YEAR) AND (trn_po_prn_details.ITEM=trn_prndetails.intItem) ) as prnQty
					from 
				   `trn_po_prn_details` WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear')";
    $result = $db->RunQuery2($sql);
    while ($row = mysqli_fetch_array($result)) {

        $item = $row['ITEM'];
        $prnQty = $row['prnQty'];
        if ($prnQty == '')
            $prnQty = 0;

        $sql_c = "UPDATE trn_po_prn_details SET  PRN_QTY ='$prnQty'  WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`ITEM`='$item')";
        $result_c = $db->RunQuery2($sql_c);
    }

//---------------------------------------------------------


    generatePRNPOcancelledEmail($orderNo, $orderYear, $action);
}


function generatePRNPOcancelledEmail($orderNo, $orderYear, $action)
{

    global $objMail;
    global $companyId;
    global $db;
    global $obj_commom;

    $content = '';

    global $db;

    $sql = "SELECT
			trn_orderheader.DUMMY_PO_NO ,
			trn_orderheader.DUMMY_PO_YEAR ,
			trn_orderheader.intOrderNo,
			trn_orderheader.intOrderYear
			FROM
			trn_orderheader  
			WHERE
			trn_orderheader.intOrderNo = '$orderNo' AND
			trn_orderheader.intOrderYear = '$orderYear'
			";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $o_n = $row['intOrderNo'] . '/' . $row['intOrderYear'];
    if ($row['DUMMY_PO_YEAR'] > 0)
        $d_n = $row['DUMMY_PO_NO'] . "/" . $row['DUMMY_PO_YEAR'];
    else
        $d_n = '';

    $sql = "SELECT
			GROUP_CONCAT(distinct trn_orderdetails.strGraphicNo) as str_strGraphicNo ,
			trn_orderheader.PO_TYPE
			FROM
			trn_orderdetails
			INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
			WHERE
			trn_orderdetails.intOrderNo = '$orderNo' AND
			trn_orderdetails.intOrderYear = '$orderYear'
			";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $graphic = $row['str_strGraphicNo'];
    $flag_dummy = $row['PO_TYPE'];
    if ($flag_dummy == 1)
        $str_pre_costing = " - Pre Costing of ERP no " . $o_n;
    else if ($flag_dummy == 2)
        $str_pre_costing = " - Split Costing of Pre Costing ERP no " . $d_n;
    else
        $str_pre_costing = " - Costing of ERP no " . $o_n;


    //ob_start();

    $sql = "SELECT
				trn_prnheader.intPrnNo as no,
				trn_prnheader.intYear as year 
				FROM `trn_prnheader`
				WHERE
				trn_prnheader.SYSTEM_CANCELLATION = 1 AND
				trn_prnheader.intOrderNo = '$orderNo' AND
				trn_prnheader.intOrderYear = '$orderYear' AND
				trn_prnheader.intStatus = -10
				GROUP BY
				trn_prnheader.intPrnNo,
				trn_prnheader.intYear
				";
    $result = $db->RunQuery2($sql);
    $content1 = '';
    $i = 0;
    while ($row = mysqli_fetch_array($result)) {
        $i++;
        $year = $row['year'];
        $prn_no = $row['no'];
        $link = $_SESSION['MAIN_URL'] . "?q=917&prnNo=$prn_no&year=$year";
        $content1 .= "<tr><td>&nbsp;</td><td class='normalfnt'><a  href= " . $link . ">$prn_no/$year</a></td>	<td>&nbsp;</td></tr>";
    }
    if ($i > 0) {
        $content .= "<tr><td>&nbsp;</td><td class='normalfnt'><b>Click Following links to view auto Cancelled PRN</b></td>	<td>&nbsp;</td></tr>" . $content1 . "<tr><td>&nbsp;</td><td class='normalfnt'></td>	<td>&nbsp;</td></tr>";
    }


    $sql = "SELECT
				trn_prnheader.intPrnNo as no,
				trn_prnheader.intYear as year 
				FROM `trn_prnheader`
				WHERE
				trn_prnheader.intOrderNo = '$orderNo' AND
				trn_prnheader.intOrderYear = '$orderYear' AND
				trn_prnheader.intStatus = 1
				GROUP BY
				trn_prnheader.intPrnNo,
				trn_prnheader.intYear
				";
    $result = $db->RunQuery2($sql);
    $content1 = '';
    $i = 0;
    while ($row = mysqli_fetch_array($result)) {
        $i++;
        $year = $row['year'];
        $prn_no = $row['no'];
        $link = $_SESSION['MAIN_URL'] . "?q=917&prnNo=$prn_no&year=$year";
        $content1 .= "<tr><td>&nbsp;</td><td class='normalfnt'><a  href= " . $link . ">$prn_no/$year</a></td>	<td>&nbsp;</td></tr>";
    }
    if ($i > 0) {
        $content .= "<tr><td>&nbsp;</td><td class='normalfnt'><b>Click Following links to view Approved PRN</b></td>	<td>&nbsp;</td></tr>" . $content1 . "<tr><td>&nbsp;</td><td class='normalfnt'></td>	<td>&nbsp;</td></tr>";
    }


    $sql = "SELECT
				trn_podetails.intPONo AS `no`,
				trn_podetails.intPOYear AS `year`,
				mst_item.strName,
				trn_podetails.CLEARED_QTY
				FROM
				trn_poheader
				INNER JOIN trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
				INNER JOIN trn_prnheader ON trn_podetails.intPrnNo = trn_prnheader.intPrnNo AND trn_podetails.intPrnYear = trn_prnheader.intYear
INNER JOIN mst_item ON trn_podetails.intItem = mst_item.intId
				WHERE 
				trn_prnheader.intOrderNo = '$orderNo' AND 
				trn_prnheader.intOrderYear ='$orderYear' AND 
				trn_podetails.CLEARED_QTY > 0
				/*trn_poheader.SYSTEM_CANCELLATION = 1 AND
				trn_poheader.intStatus = -10 */
				/*GROUP BY
				trn_podetails.intPONo,
				trn_podetails.intPOYear */";
    $result = $db->RunQuery2($sql);
    $content1 = '';
    $i = 0;
    $temp_order = '';
    while ($row = mysqli_fetch_array($result)) {
        $i++;
        $year = $row['year'];
        $order_no = $row['no'];
        $item = $row['strName'];
        $cleared_qty = $row['CLEARED_QTY'];
        $link = $_SESSION['MAIN_URL'] . "?q=934&poNo=$order_no&year=$year";
        if ($order_no . '/' . $year != $temp_order) {
            $content1 .= "<tr><td>&nbsp;</td><td class='normalfnt'><a  href= " . $link . ">$order_no/$year</a></td>	<td>&nbsp;</td></tr>";
        }
        $content1 .= "<tr><td>&nbsp;</td><td class='normalfnt'>Item " . $item . " : Qty = " . $cleared_qty . "</td>	<td>&nbsp;</td></tr>";
        $temp_order = $order_no . '/' . $year;
    }
    if ($i > 0) {
        $content .= "<tr><td>&nbsp;</td><td class='normalfnt'><b>Click Following links to view auto cleared Supplier PO due to the Customer PO action : " . $action . "</b></td>	<td>&nbsp;</td></tr>" . $content1 . "<tr><td>&nbsp;</td><td class='normalfnt'></td>	<td>&nbsp;</td></tr>";
    }


    $sql = "SELECT
				trn_podetails.intPONo as no ,
				trn_podetails.intPOYear as year 
				FROM
				trn_poheader
				INNER JOIN trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
				INNER JOIN trn_prnheader ON trn_podetails.intPrnNo = trn_prnheader.intPrnNo AND trn_podetails.intPrnYear = trn_prnheader.intYear
				WHERE 
				trn_prnheader.intOrderNo = '$orderNo' AND 
				trn_prnheader.intOrderYear ='$orderYear' AND 
				trn_poheader.intStatus = 1
				GROUP BY
				trn_podetails.intPONo,
				trn_podetails.intPOYear";
    $result = $db->RunQuery2($sql);
    $content1 = '';
    $i = 0;
    while ($row = mysqli_fetch_array($result)) {
        $i++;
        $year = $row['year'];
        $order_no = $row['no'];
        $link = $_SESSION['MAIN_URL'] . "?q=934&poNo=$order_no&year=$year";
        $content1 .= "<tr><td>&nbsp;</td><td class='normalfnt'><a  href= " . $link . ">$order_no/$year</a></td>	<td>&nbsp;</td></tr>";
    }
    if ($i > 0) {
        $content .= "<tr><td>&nbsp;</td><td class='normalfnt'><b>If it is possible please revise or cancel following supplier pos since the bulk order is " . $action . ". Click Following links to view Approved Supplier PO</b></td>	<td>&nbsp;</td></tr>" . $content1 . "<tr><td>&nbsp;</td><td class='normalfnt'></td>	<td>&nbsp;</td></tr>";
    }


    //include_once "rptBulkOrder-db-prn_mail_template.php";

    //$body 			= ob_get_clean();
    //---------

    $nowDate = date('Y-m-d');

    //$mailHeader		= "SYSTEM CANCELLED PRN AND QTY CLEARED SUPPLIER PO".$str_pre_costing." - Graphics(".$graphic." )";
    $mailHeader = "SYSTEM CANCELLED AND QTY CLEARED PRN (" . $prn_no . "/" . $year . ") " . $str_pre_costing . " - Graphics(" . $graphic . " )";

    $FROM_NAME = "NSOFT";
    $FROM_EMAIL = '';

    $sql = "SELECT
				sys_mail_eventusers.intUserId ,
				sys_users.strUserName 
				FROM
				sys_mail_eventusers
				INNER JOIN sys_users ON sys_mail_eventusers.intUserId = sys_users.intUserId
				WHERE
				sys_mail_eventusers.intMailEventId = 1031 AND
				sys_mail_eventusers.intCompanyId = '$companyId' AND
				sys_users.intStatus = 1
				GROUP BY
				sys_mail_eventusers.intUserId";
    $result = $db->RunQuery2($sql);
    while ($row = mysqli_fetch_array($result)) {

        $receiver_name = $row['strUserName'];
        $mail_TO = $obj_commom->getEmailList2($row['intUserId']);
        $FROM_NAME = 'NSOFT';

        ob_start();
        include "rptBulkOrder-db-prn_mail_template.php";
        $body = ob_get_clean();

        //$objMail->insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
        insertTable2($FROM_EMAIL, $FROM_NAME, $mail_TO, $mailHeader, $body, '', '');
    }
}

function getMinPSDdate($orderNo, $orderYear)
{

    global $db;

    $sql = "SELECT
				min(trn_orderdetails.dtPSD) as psd 
				FROM `trn_orderdetails`
				WHERE
				trn_orderdetails.intOrderNo 	= '$orderNo' AND
				trn_orderdetails.intOrderYear 	= '$orderYear' limit 1";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $psd = $row['psd'];
    return $psd;

}

function getSpecialTechApprovedSataus($orderNo, $orderYear)
{

    global $db;
    $cons_fag = 1;


    $sql = "SELECT
				(trn_orderdetails.intSampleNo) as sampleNo ,
				(trn_orderdetails.intSampleYear) as sampleYear, 
				(trn_orderdetails.strCombo) as combo ,
				(trn_orderdetails.strPrintName) as print ,
				(trn_orderdetails.intRevisionNo) as revNo 
				FROM `trn_orderdetails`
				WHERE
				trn_orderdetails.intOrderNo 	= '$orderNo' AND
				trn_orderdetails.intOrderYear 	= '$orderYear'  AND trn_orderdetails.SO_TYPE > -1 ";

    $result = $db->RunQuery2($sql);
    $cons_fag_temp = 1;
    while ($row = mysqli_fetch_array($result)) {
        $sampleNo = $row['sampleNo'];
        $sampleYear = $row['sampleYear'];
        $combo = $row['combo'];
        $printName = $row['print'];
        $revNo = $row['revNo'];

        $cons_fag_temp = getApprovedSataus($sampleNo, $sampleYear, $revNo, $combo, $printName);
        if ($cons_fag_temp != 1) {
            $cons_fag = 0;
        }

    }

    return $cons_fag;

}

function getApprovedSataus($sampleNo, $sampleYear, $revNo, $combo, $printName)
{

    global $db;

    $flag_app = 1;

    $sqlm = "SELECT DISTINCT
				
				mst_techniques.strName AS TECHNIQUE,
				trn_sampleinfomations_details.intTechniqueId,
				trn_sampleinfomations_details.intItem,
				mst_item.strName as item,
				mst_item.foil_width,
				mst_item.foil_height, 
				trn_sampleinfomations_details.intItem,
				(size_w+0.5) as size_w,
				(size_h+0.5) as size_h,
				trn_sampleinfomations_details.dblQty as qty,
				0 as dblMeters, 
				'' as strCuttingSide ,
				'new' as status,
				
				trn_sampleinfomations_details.strPrintName, 
				trn_sampleinfomations_details.intItem,(size_w+0.5) as size_w,(size_h+0.5) as size_h,
				mst_item.strName  ,'new' as status  
				FROM
				trn_sampleinfomations_details
				Right Join mst_item ON trn_sampleinfomations_details.intItem = mst_item.intId 
				INNER JOIN mst_techniques ON trn_sampleinfomations_details.intTechniqueId=mst_techniques.intId
				WHERE
				trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
				trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
				trn_sampleinfomations_details.strComboName 		=  '$combo' AND 
				trn_sampleinfomations_details.intRevNo =  '$revNo' AND
				trn_sampleinfomations_details.strPrintName =  '$printName' /*AND 
				mst_techniques.intRoll =  '1'  AND CHANGE_CONPC_AFTER_SAMPLE =1*/
				AND mst_item.ROLL_FORM =  '1' AND CHANGE_CONPC_AFTER_SAMPLE =1  AND 
				mst_item.intUOM <> 6

			ORDER BY
				mst_item.strName ASC
			";
    $result_tech = $db->RunQuery2($sqlm);
    while ($rowm = mysqli_fetch_array($result_tech)) {

        $sql = "SELECT
							trn_sample_special_technical_header.`STATUS`
							FROM `trn_sample_special_technical_header`
							WHERE
							trn_sample_special_technical_header.SAMPLE_NO = '$sampleNo' AND
							trn_sample_special_technical_header.SAMPLE_YEAR = '$sampleYear' AND
							trn_sample_special_technical_header.REVISION_NO = '$revNo' AND
							trn_sample_special_technical_header.COMBO = '$combo' AND
							trn_sample_special_technical_header.PRINT = '$printName'
							";

        $result = $db->RunQuery2($sql);
        $row = mysqli_fetch_array($result);

        if ($row['STATUS'] != 1)
            $flag_app = 0;
    }

    $sqlm = "SELECT
										mst_techniques.strName AS TECHNIQUE,
										trn_sampleinfomations_details.intTechniqueId,
										mst_item.strName as item,
										trn_sampleinfomations_details.intItem,
										trn_sampleinfomations_details.dblQty
								FROM
								trn_sampleinfomations_details
								Right Join mst_item ON trn_sampleinfomations_details.intItem = mst_item.intId 
								INNER JOIN mst_techniques ON trn_sampleinfomations_details.intTechniqueId=mst_techniques.intId
								WHERE
								trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
								trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
								trn_sampleinfomations_details.strComboName 		=  '$combo' AND 
								trn_sampleinfomations_details.intRevNo =  '$revNo' AND
								trn_sampleinfomations_details.strPrintName =  '$printName' /*AND 
								mst_techniques.intRoll <>  '1' AND CHANGE_CONPC_AFTER_SAMPLE =1*/
								AND mst_item.ROLL_FORM =  '1' AND CHANGE_CONPC_AFTER_SAMPLE =1  AND 
								mst_item.intUOM <> 6
			
							ORDER BY
								mst_item.strName ASC
			";
    $result_tech = $db->RunQuery2($sqlm);
    while ($rowm = mysqli_fetch_array($result_tech)) {

        $sql = "SELECT
							trn_sample_special_technical_header.`STATUS`
							FROM `trn_sample_special_technical_header`
							WHERE
							trn_sample_special_technical_header.SAMPLE_NO = '$sampleNo' AND
							trn_sample_special_technical_header.SAMPLE_YEAR = '$sampleYear' AND
							trn_sample_special_technical_header.REVISION_NO = '$revNo' AND
							trn_sample_special_technical_header.COMBO = '$combo' AND
							trn_sample_special_technical_header.PRINT = '$printName'
							";

        $result = $db->RunQuery2($sql);
        $row = mysqli_fetch_array($result);

        if ($row['STATUS'] != 1)
            $flag_app = 0;
    }

    return $flag_app;

}

function save_po_prn_details($orderNo, $orderYear, $item, $Qty)
{

    global $db;

    $sql = "SELECT
			trn_po_prn_details.ORDER_NO,
			trn_po_prn_details.ORDER_YEAR,
			trn_po_prn_details.ITEM
			FROM
			trn_po_prn_details
			WHERE
			trn_po_prn_details.ORDER_NO = '$orderNo' AND
			trn_po_prn_details.ORDER_YEAR = '$orderYear' AND
			trn_po_prn_details.ITEM = '$item'
			";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);

    if ($row['ORDER_NO'] > 0) {
        $sql = "UPDATE `trn_po_prn_details` SET `REQUIRED`=`REQUIRED`+'$Qty' WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`ITEM`='$item')";
    } else {
        $sql = "INSERT INTO `trn_po_prn_details` (`ORDER_NO`, `ORDER_YEAR`, `ITEM`, `REQUIRED`) VALUES ('$orderNo', '$orderYear', '$item', '$Qty')";
    }
    $result = $db->RunQuery2($sql);

    $response['res'] = $result;
    $response['msg'] = $db->errormsg;
    $response['q'] = $sql;
    return $response;
}

function save_po_prn_details_so_wise($orderNo, $orderYear, $sales_order_id, $item, $Qty, $conPC, $to_producrd)
{

    global $db;

    $sql = "SELECT
			trn_po_prn_details_sales_order.ORDER_NO,
			trn_po_prn_details_sales_order.ORDER_YEAR,
			trn_po_prn_details_sales_order.ITEM
			FROM
			trn_po_prn_details_sales_order
			WHERE
			trn_po_prn_details_sales_order.ORDER_NO = '$orderNo' AND
			trn_po_prn_details_sales_order.ORDER_YEAR = '$orderYear' AND 
			trn_po_prn_details_sales_order.SALES_ORDER = '$sales_order_id' AND 
			trn_po_prn_details_sales_order.ITEM = '$item'
			";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);

    if ($row['ORDER_NO'] > 0) {
        $sql = "UPDATE `trn_po_prn_details_sales_order` SET `REQUIRED`='$Qty', `CONS_PC`='$conPC', `PRODUCTION_QTY`='$to_producrd' WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$sales_order_id') AND (`ITEM`='$item')";
    } else {
        $sql = "INSERT INTO `trn_po_prn_details_sales_order` (`ORDER_NO`, `ORDER_YEAR`, `SALES_ORDER`, `ITEM`, `REQUIRED`,`CONS_PC`,`PRODUCTION_QTY`) VALUES ('$orderNo', '$orderYear', '$sales_order_id', '$item', '$Qty', '$conPC', '$to_producrd')";
    }
    $result = $db->RunQuery2($sql);
    $response['res'] = $result;
    $response['msg'] = $db->errormsg;
    $response['q'] = $sql;
    return $response;
}

function getMinimumPSDdate($orderNo, $orderYear)
{

    global $db;

    $sql = "SELECT
				Min(trn_orderdetails.dtPSD) AS psd
				FROM `trn_orderdetails`
				WHERE
				trn_orderdetails.intOrderNo = '$orderNo' AND
				trn_orderdetails.intOrderYear = '$orderYear'
				";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);

    return $row['psd'];

}

function update_po_prn_details($orderNo, $orderYear, $item, $bulkRequired, $prnQty, $prn_used_from_dummy)
{

    global $db;

    $sql = "UPDATE `trn_po_prn_details` SET 
	`PRN_QTY`=`PRN_QTY`+'$prnQty',
	`PRN_QTY_BALANCED_FROM_DUMMY`=`PRN_QTY_BALANCED_FROM_DUMMY`+'$prn_used_from_dummy'  
	WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`ITEM`='$item')";
    $result = $db->RunQuery2($sql);

    if ($result) {
        $sql_d = "update 
				trn_po_prn_details
				INNER JOIN trn_orderheader ON trn_po_prn_details.ORDER_NO = trn_orderheader.DUMMY_PO_NO 
				AND trn_po_prn_details.ORDER_YEAR = trn_orderheader.DUMMY_PO_YEAR
				INNER JOIN trn_po_prn_details AS pd ON trn_orderheader.intOrderNo = pd.ORDER_NO 
				AND trn_orderheader.intOrderYear = pd.ORDER_YEAR AND trn_po_prn_details.ITEM = pd.ITEM
				set trn_po_prn_details.PRN_QTY_USED_OTHERS = IFNULL(trn_po_prn_details.PRN_QTY_USED_OTHERS,0)+'$prn_used_from_dummy'
				WHERE
				pd.ORDER_NO = '$orderNo' AND
				pd.ORDER_YEAR = '$orderYear' 
				 AND (pd.`ITEM`='$item') ";

        $result_d = $db->RunQuery2($sql_d);
    }

    $response['res'] = $result;
    $response['msg'] = $db->errormsg;
    $response['q'] = $sql;
    return $response;
}

function update_bulk_po_qtys($prn_no, $year, $orderQty, $to_producrd)
{

    global $db;
    $sql = "UPDATE `trn_prnheader` SET `BULK_PO_QTY`= `BULK_PO_QTY`+'$orderQty', `BULK_PO_QTY_RCV_ABLE`= `BULK_PO_QTY_RCV_ABLE`+'$to_producrd' WHERE (`intPrnNo`='$prn_no') AND (`intYear`='$year')";
    $result = $db->RunQuery2($sql);

    $response['res'] = $result;
    $response['msg'] = $db->errormsg;
    $response['q'] = $sql;
    return $response;


}

function save_history($orderNo, $orderYear, $bulk_rev)
{

    global $db;
    //save in revision history////////////////
    //------header-----------------------------
    $sql_h = "insert into trn_orderheader_revision_history (
						REVISION_NO,
						intOrderNo,
						intOrderYear,
						strCustomerPoNo,
						dtDate,
						dtDeliveryDate,
						intCustomer,
						intCustomerLocation,
						intCurrency,
						intPaymentTerm,
						strContactPerson,
						strRemark,
						REJECT_REASON,
						intMarketer,
						intStatus,
						intCreator,
						dtmCreateDate,
						intModifyer,
						dtmModifyDate,
						intApproveLevelStart,
						intLocationId,
						intAllocatedStatus,
						intReviseNo,
						intStatusOld,
						PAYMENT_RECEIVE_COMPLETED_FLAG,
						LC_STATUS,
						TECHNIQUE_TYPE)
						(
						SELECT 
						intReviseNo AS REV,
						intOrderNo,
						intOrderYear,
						strCustomerPoNo,
						dtDate,
						dtDeliveryDate,
						intCustomer,
						intCustomerLocation,
						intCurrency,
						intPaymentTerm,
						strContactPerson,
						strRemark,
						REJECT_REASON,
						intMarketer,
						intStatus,
						intCreator,
						dtmCreateDate,
						intModifyer,
						dtmModifyDate,
						intApproveLevelStart,
						intLocationId,
						intAllocatedStatus,
						intReviseNo,
						intStatusOld,
						PAYMENT_RECEIVE_COMPLETED_FLAG,
						LC_STATUS,
						TECHNIQUE_TYPE 
						FROM 
						trn_orderheader 
						WHERE  
						trn_orderheader.intOrderNo = '$orderNo' AND trn_orderheader.intOrderYear ='$orderYear'
						)";
    $result_h = $db->RunQuery2($sql_h);
    //------detail-----------------------------
    $sql_d = "insert into trn_orderdetails_revision_history (
						REVISION_NO,
						intOrderNo,
						intOrderYear,
						strSalesOrderNo,
						intSalesOrderId,
						strLineNo,
						strStyleNo,
						strGraphicNo,
						intSampleNo,
						intSampleYear,
						strCombo,
						strPrintName,
						intRevisionNo,
						intPart,
						intQty,
						dblPrice,
						dblToleratePercentage,
						dblOverCutPercentage,
						dblDamagePercentage,
						dtPSD,
						dtDeliveryDate,
						intViewInSalesProjection,
						TECHNIQUE_GROUP_ID,
						`STATUS` )
						(
						SELECT 
						$bulk_rev,
						intOrderNo,
						intOrderYear,
						strSalesOrderNo,
						intSalesOrderId,
						strLineNo,
						strStyleNo,
						strGraphicNo,
						intSampleNo,
						intSampleYear,
						strCombo,
						strPrintName,
						intRevisionNo,
						intPart,
						intQty,
						dblPrice,
						dblToleratePercentage,
						dblOverCutPercentage,
						dblDamagePercentage,
						dtPSD,
						dtDeliveryDate,
						intViewInSalesProjection,
						TECHNIQUE_GROUP_ID,
						`STATUS`  
						FROM 
						trn_orderdetails 
						WHERE  
						trn_orderdetails.intOrderNo = '$orderNo' AND trn_orderdetails.intOrderYear ='$orderYear'
						)";
    $result_d = $db->RunQuery2($sql_d);
    //------size wise--------------------------
    $sql_s = "insert into trn_ordersizeqty_revision_history (
						REVISION_NO,
						intOrderNo,
						intOrderYear,
						intSalesOrderId,
						strSize,
						dblQty
						)
						(
						SELECT 
						$bulk_rev,
						intOrderNo,
						intOrderYear,
						intSalesOrderId,
						strSize,
						dblQty
 						FROM 
						trn_ordersizeqty 
						WHERE  
						trn_ordersizeqty.intOrderNo = '$orderNo' AND trn_ordersizeqty.intOrderYear ='$orderYear'
						)";
    $result_s = $db->RunQuery2($sql_s);
    /////////////////////////////////////////

    $sql = "UPDATE `trn_orderheader` SET `intReviseNo`=`intReviseNo`+1 WHERE (`intOrderNo`='$orderNo') AND (`intOrderYear`='$orderYear')  ";
    $result = $db->RunQuery2($sql);

}

function save_pri_history($orderNo, $orderYear)
{
    global $db;

    $sql_po_prn_details = "INSERT INTO trn_po_prn_details_history
						(
						ORDER_NO,
						ORDER_YEAR,
						ITEM,
						REQUIRED,
						PRN_QTY,
						PURCHASED_QTY,
						PRN_QTY_USED_OTHERS,
						PRN_QTY_BALANCED_FROM_DUMMY,
						REVISION,
						REVISED_BY,
						REVISED_TIME
						)
						(
						select 
						ORDER_NO,
						ORDER_YEAR,
						ITEM,
						REQUIRED,
						PRN_QTY,
						PURCHASED_QTY,
						PRN_QTY_USED_OTHERS,
						PRN_QTY_BALANCED_FROM_DUMMY,
						intReviseNo,
						intModifyer,
						dtmModifyDate
						FROM
						trn_po_prn_details
						INNER JOIN
						trn_orderheader
						ON trn_orderheader.intOrderNo = trn_po_prn_details.ORDER_NO AND
						trn_orderheader.intOrderYear = trn_po_prn_details.ORDER_YEAR
						WHERE
						trn_po_prn_details.ORDER_NO = $orderNo
						AND
						trn_po_prn_details.ORDER_YEAR = $orderYear
						
						)
						";
    //echo $sql_po_prn_details;
    $result_h = $db->RunQuery2($sql_po_prn_details);

    //------------------------ insert in po_prn_sales order--------------------
    $sql_po_prn_saleorder = "
					INSERT INTO trn_po_prn_details_sales_order_history
					(
					ORDER_NO,
					ORDER_YEAR,
					SALES_ORDER,
					ITEM,
					CONS_PC,
					PRODUCTION_QTY,
					REQUIRED,
					PRN_QTY,
					MRN_TOT_QTY,
					MRN_EXTRA_QTY,
					ISSUED_QTY,
					RETURNED_QTY,
					GP_OUT_FROM_HO,
					GP_IN_TO_HO,
					REVISION,
					REVISED_BY,
					REVISED_TIME
					)
					(
					select 
					ORDER_NO,
					ORDER_YEAR,
					SALES_ORDER,
					ITEM,
					CONS_PC,
					PRODUCTION_QTY,
					REQUIRED,
					PRN_QTY,
					MRN_TOT_QTY,
					MRN_EXTRA_QTY,
					ISSUED_QTY,
					RETURNED_QTY,
					GP_OUT_FROM_HO,
					GP_IN_TO_HO,
					intReviseNo,
					intModifyer,
					dtmModifyDate
					
					FROM
					trn_po_prn_details_sales_order
					INNER JOIN
					trn_orderheader
					ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO AND
					trn_orderheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
					WHERE
					trn_po_prn_details_sales_order.ORDER_NO = $orderNo
					AND
					trn_po_prn_details_sales_order.ORDER_YEAR = $orderYear
					
					)
						";
    //echo $sql_po_prn_saleorder;
    $result_h = $db->RunQuery2($sql_po_prn_saleorder);

}

function getNonRMTechniquesFlag($orderNo, $orderYear)
{
    global $db;
    $sql = "SELECT
			trn_orderdetails.intOrderNo,
			trn_orderdetails.intOrderYear
			FROM
			trn_orderdetails
			INNER JOIN trn_sampleinfomations_details ON trn_orderdetails.intSampleNo = trn_sampleinfomations_details.intSampleNo AND trn_orderdetails.intSampleYear = trn_sampleinfomations_details.intSampleYear AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName AND trn_sampleinfomations_details.intRevNo = trn_orderdetails.intRevisionNo
			INNER JOIN mst_techniques ON trn_sampleinfomations_details.intTechniqueId = mst_techniques.intId
			WHERE
			mst_techniques.RM_USING = 1 AND
			trn_orderdetails.intOrderNo = '$orderNo' AND
			trn_orderdetails.intOrderYear = '$orderYear'

				";
    $result = $db->RunQuery2($sql);
    $rows = mysqli_num_rows($result);
    if ($rows > 0)
        return 0;
    else
        return 1;
}

function check_availablity($sampleNo, $sampleYear, $revision, $combo, $print, $intPart, $item)
{

    $available = false;
    global $db;

    $sql = "
					SELECT 
					mst_item.intId as intItem,
					sum(trn_sample_foil_consumption.dblMeters) as CONSUMPTION
					FROM
					trn_sample_foil_consumption  
					Inner Join mst_item ON mst_item.intId = trn_sample_foil_consumption.intItem
					Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
					Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
					Inner Join mst_units ON mst_units.intId = mst_item.intUOM
					Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_item.intCurrency
					WHERE
					trn_sample_foil_consumption.intSampleNo = '$sampleNo' AND
					trn_sample_foil_consumption.intSampleYear = '$sampleYear' AND
					trn_sample_foil_consumption.intRevisionNo = '$revision' AND
					trn_sample_foil_consumption.strCombo = '$combo' AND
					trn_sample_foil_consumption.strPrintName = '$print' AND 
					trn_sample_foil_consumption.intItem ='$item' AND 
					trn_sample_foil_consumption.dblMeters > 0
					GROUP BY
					mst_maincategory.intId,
					mst_subcategory.intId,
					mst_item.intId				";

    $result = $db->RunQuery2($sql);
    if ($result)
        $available = true;


    $sql = "
					SELECT 
					trn_sample_non_direct_rm_consumption.ITEM as intItem,
					sum(trn_sample_non_direct_rm_consumption.CONSUMPTION) as CONSUMPTION
					FROM
					trn_sample_non_direct_rm_consumption  
 					WHERE
					trn_sample_non_direct_rm_consumption.SAMPLE_NO = '$sampleNo' AND
					trn_sample_non_direct_rm_consumption.SAMPLE_YEAR = '$sampleYear' AND
					trn_sample_non_direct_rm_consumption.REVISION_NO = '$revision'  AND
					trn_sample_non_direct_rm_consumption.ITEM = '$item' AND 
					trn_sample_non_direct_rm_consumption.CONSUMPTION > 0
					group by 
					trn_sample_non_direct_rm_consumption.ITEM ";

    $result1 = $db->RunQuery2($sql);
    if ($result1)
        $available = true;

    $sql =
        "
				SELECT
				mst_item.intId as intItem,
				sum(trn_sample_spitem_consumption.dblQty/dblNoOfPcs) as CONSUMPTION
				FROM
				trn_sample_spitem_consumption 
				Inner Join mst_item ON mst_item.intId = trn_sample_spitem_consumption.intItem
				Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
				Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
				Inner Join mst_units ON mst_units.intId = mst_item.intUOM
				Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_item.intCurrency
				WHERE
				trn_sample_spitem_consumption.intSampleNo = '$sampleNo' AND
				trn_sample_spitem_consumption.intSampleYear = '$sampleYear' AND
				trn_sample_spitem_consumption.intRevisionNo = '$revision' AND
				trn_sample_spitem_consumption.strCombo = '$combo' AND
				trn_sample_spitem_consumption.strPrintName = '$print'  AND 
				mst_item.intId = '$item' AND 
				trn_sample_spitem_consumption.dblQty > 0
				GROUP BY
				mst_maincategory.intId,
				mst_subcategory.intId,
				mst_item.intId";

    $result2 = $db->RunQuery2($sql);
    if ($result2)
        $available = true;

    $sql = " select  
					intItem,
					ROUND(SUM(dblColorWeight/sumWeight*dblWeight /dblNoOfPcs),9) as CONSUMPTION 
					 from (  SELECT 
					 SCR.intItem,
					SCR.dblWeight,
					trn_sampleinfomations_details_technical.dblColorWeight ,
					mst_units.dblNoOfPcs ,
					
					 (SELECT Sum(trn_sample_color_recipes.dblWeight) AS sumWeight 
					FROM trn_sample_color_recipes 
					WHERE trn_sample_color_recipes.intSampleNo = SCR.intSampleNo 
					AND trn_sample_color_recipes.intSampleYear = SCR.intSampleYear 
					AND trn_sample_color_recipes.intRevisionNo = SCR.intRevisionNo 
					AND trn_sample_color_recipes.strCombo = SCR.strCombo 
					AND trn_sample_color_recipes.strPrintName = SCR.strPrintName 
					AND trn_sample_color_recipes.intTechniqueId = SCR.intTechniqueId 
					AND trn_sample_color_recipes.intInkTypeId = SCR.intInkTypeId 
					AND trn_sample_color_recipes.intColorId = SCR.intColorId 
					) as sumWeight 
					FROM  trn_sample_color_recipes  as SCR 
					Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = SCR.intSampleNo 
					AND trn_sampleinfomations_details_technical.intSampleYear = SCR.intSampleYear 
					AND trn_sampleinfomations_details_technical.intRevNo = SCR.intRevisionNo 
					AND trn_sampleinfomations_details_technical.strComboName = SCR.strCombo 
					AND trn_sampleinfomations_details_technical.strPrintName = SCR.strPrintName 
					AND trn_sampleinfomations_details_technical.intColorId = SCR.intColorId 
					AND trn_sampleinfomations_details_technical.intInkTypeId = SCR.intInkTypeId 
					Inner Join mst_item ON mst_item.intId = SCR.intItem 
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
					Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory 
					Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory 
					WHERE SCR.intSampleNo='$sampleNo' 
					AND SCR.intSampleYear='$sampleYear' 
					AND SCR.intRevisionNo='$revision' 
					AND SCR.strCombo='$combo' 
					AND SCR.strPrintName='$print' 
					AND SCR.intItem='$item' AND 
					dblColorWeight > 0
					group by 
SCR.intTechniqueId 
					,SCR.intInkTypeId 
					, SCR.intColorId 
,SCR.intItem
					 ) as t 
group by intItem

		";
    $result3 = $db->RunQuery2($sql);
    if ($result3)
        $available = true;

    return $available;
}

function getSalesOrderWiseRcvQty($orderNo,$orderYear,$salesOrderId){
    global $db;

    $sqlp = "SELECT
IFNULL(Sum(ware_fabricreceiveddetails.dblQty),0) AS qty
FROM
ware_fabricreceivedheader
Inner Join ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
WHERE
ware_fabricreceivedheader.intStatus =  '1' AND
ware_fabricreceivedheader.intOrderNo =  '$orderNo' AND
ware_fabricreceivedheader.intOrderYear =  '$orderYear' AND
ware_fabricreceiveddetails.intSalesOrderId =  '$salesOrderId'";

    $resultp = $db->RunQuery2($sqlp);
    $rowp=mysqli_fetch_array($resultp);

    $rcv= $rowp['qty'];


    $sql = "SELECT
			IFNULL(sum(ware_stocktransactions_fabric.dblQty*-1),0) as dblQty 
			FROM ware_stocktransactions_fabric
			WHERE
 			ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
			ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
			ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
 			ware_stocktransactions_fabric.strType in(  'Dispatched_F' ,'Dispatched_P', 'Dispatched_CUT_RET')
			GROUP BY
			ware_stocktransactions_fabric.intOrderNo,
			ware_stocktransactions_fabric.intOrderYear,
			ware_stocktransactions_fabric.intSalesOrderId,
			ware_stocktransactions_fabric.strSize";

    $result = $db->RunQuery2($sql);
    $rows = mysqli_fetch_array($result);
    $retQty =$rows['dblQty'];

    $val=$rcv-$retQty;

    return $val;
}

function get_average_consumption($orderNo, $orderYear, $item, $sales_order, $conPC_samp, $grading_selected)
{
    global $db;
    $data = get_size_wise_details($orderNo, $orderYear, $item, $sales_order);
    while ($row = mysqli_fetch_array($data)) {
        $order_qty = $row['dblQty'];
        $size = $row['strSize'];
        $sample_no = $row['intSampleNo'];
        $sample_year = $row['intSampleYear'];
        $combo = $row['strCombo'];
        $print = $row['strPrintName'];
        $rev = $row['intRevisionNo'];
        $cons_sample = $conPC_samp;
        $cons = get_grading_cons($orderNo, $orderYear, $item, $sales_order, $size, $sample_no, $sample_year, $combo, $print, $rev);


        $pri_size += $cons * $order_qty;
        $tot_qty += $order_qty;

    }

    $conPC = $pri_size / $tot_qty;
    if ($conPC == "" || $conPC == 0 || $conPC == 'null' || $grading_selected == 0) {
        $conPC = $cons_sample;
    }


    return $conPC;
}

function get_size_wise_details($orderNo, $orderYear, $item, $sales_order)
{
    global $db;

    $sql_select = "select 
		trn_ordersizeqty.dblQty,
		trn_ordersizeqty.strSize,
		trn_orderdetails.intSampleNo,
		trn_orderdetails.intSampleYear,
		trn_orderdetails.strCombo,
		trn_orderdetails.intPart,
		trn_orderdetails.strPrintName,
		trn_orderdetails.intRevisionNo
		
		FROM
		trn_ordersizeqty
		INNER JOIN
		trn_orderdetails
		ON
		trn_ordersizeqty.intOrderNo = trn_orderdetails.intOrderNo AND
		trn_ordersizeqty.intOrderYear = trn_orderdetails.intOrderYear AND
		trn_ordersizeqty.intSalesOrderId = trn_orderdetails.intSalesOrderId
		WHERE
		trn_ordersizeqty.intOrderNo = $orderNo
		AND
		trn_ordersizeqty.intOrderYear = $orderYear
		AND
		trn_ordersizeqty.intSalesOrderId = $sales_order ";
    //echo $sql_select;
    $result = $db->RunQuery2($sql_select);
    return $result;

}

function get_grading_cons($orderNo, $orderYear, $item, $sales_order, $size, $sample_no, $sample_year, $combo, $print, $rev)
{
    global $db;

    $sql_get_grading = "SELECT
					
					sum(TB1.EDITED_CONSUMPTION) as EDITED_CONSUMPTION
					FROM
					
					(SELECT
					
					trn_sampleinfomations_grading_roll_items.SAMPLE_NO,
					trn_sampleinfomations_grading_roll_items.SAMPLE_YEAR,
					trn_sampleinfomations_grading_roll_items.REVISION,
					trn_sampleinfomations_grading_roll_items.ITEM,
					trn_sampleinfomations_grading_roll_items.COMBO,
					trn_sampleinfomations_grading_roll_items.PRINT,
					trn_sampleinfomations_grading_roll_items.SIZE,
					
					trn_sampleinfomations_grading_roll_items.EDITED_CONSUMPTION
					FROM
					trn_sampleinfomations_grading_roll_items
					
					-- GROUP BY
					
					-- trn_sampleinfomations_grading_roll_items.SAMPLE_NO,
					-- trn_sampleinfomations_grading_roll_items.SAMPLE_YEAR,
					-- trn_sampleinfomations_grading_roll_items.REVISION,
					-- trn_sampleinfomations_grading_roll_items.ITEM
					
					
					UNION ALL
					
					SELECT
					trn_sampleinfomations_grading_ink_items.SAMPLE_NO,
					trn_sampleinfomations_grading_ink_items.SAMPLE_YEAR,
					trn_sampleinfomations_grading_ink_items.REVISION,
					trn_sampleinfomations_grading_ink_items.ITEM,
					trn_sampleinfomations_grading_ink_items.COMBO,
					trn_sampleinfomations_grading_ink_items.PRINT,
					trn_sampleinfomations_grading_ink_items.SIZE,
					trn_sampleinfomations_grading_ink_items.EDITED_CONSUMPTION
					
					FROM
					
					trn_sampleinfomations_grading_ink_items
					
					-- GROUP BY
					
					-- trn_sampleinfomations_grading_ink_items.SAMPLE_NO,
					-- trn_sampleinfomations_grading_ink_items.SAMPLE_YEAR,
					-- trn_sampleinfomations_grading_ink_items.REVISION,
					-- trn_sampleinfomations_grading_ink_items.ITEM
					
					UNION ALL
					
					SELECT
					
					trn_sampleinfomations_grading_non_direct_items.SAMPLE_NO,
					trn_sampleinfomations_grading_non_direct_items.SAMPLE_YEAR,
					trn_sampleinfomations_grading_non_direct_items.REVISION,
					trn_sampleinfomations_grading_non_direct_items.ITEM,
					trn_sampleinfomations_grading_non_direct_items.COMBO,
					trn_sampleinfomations_grading_non_direct_items.PRINT,
					trn_sampleinfomations_grading_non_direct_items.SIZE,
					trn_sampleinfomations_grading_non_direct_items.EDITED_CONSUMPTION
					
					FROM
					trn_sampleinfomations_grading_non_direct_items
					
					-- GROUP BY
					-- trn_sampleinfomations_grading_non_direct_items.SAMPLE_NO,
					-- trn_sampleinfomations_grading_non_direct_items.SAMPLE_YEAR,
					-- trn_sampleinfomations_grading_non_direct_items.REVISION,
					-- trn_sampleinfomations_grading_non_direct_items.ITEM
					
					
					UNION ALL
					
					SELECT
					trn_sampleinfomations_grading_non_roll_items.SAMPLE_NO,
					trn_sampleinfomations_grading_non_roll_items.SAMPLE_YEAR,
					trn_sampleinfomations_grading_non_roll_items.REVISION,
					trn_sampleinfomations_grading_non_roll_items.ITEM,
					trn_sampleinfomations_grading_non_roll_items.COMBO,
					trn_sampleinfomations_grading_non_roll_items.PRINT,
					trn_sampleinfomations_grading_non_roll_items.SIZE,
					trn_sampleinfomations_grading_non_roll_items.EDITED_CONSUMPTION
					
					FROM
					trn_sampleinfomations_grading_non_roll_items
					
					-- GROUP BY
					-- trn_sampleinfomations_grading_non_roll_items.SAMPLE_NO,
					-- trn_sampleinfomations_grading_non_roll_items.SAMPLE_YEAR,
					-- trn_sampleinfomations_grading_non_roll_items.REVISION,
					-- trn_sampleinfomations_grading_non_roll_items.ITEM
					)
					AS TB1
					
					
					WHERE
					TB1.SAMPLE_NO = $sample_no 
					AND
					TB1.SAMPLE_YEAR = $sample_year
					AND
					TB1.ITEM = $item
					AND
					TB1.REVISION = $rev
					AND
					TB1.COMBO = '$combo'
					AND
					TB1.PRINT = '$print' 
					AND
					TB1.SIZE = '$size'";
    //echo $sql_get_grading;
    $result = $db->RunQuery2($sql_get_grading);
    $row = mysqli_fetch_array($result);
    $cons = $row['EDITED_CONSUMPTION'];
    return $cons;
}

function get_sample_data($orderNo, $orderYear)
{
    global $db;
    $sql1 = "	SELECT 
		 				trn_orderdetails.strLineNo,
						trn_orderdetails.strSalesOrderNo,
						trn_orderdetails.intSalesOrderId, 
						trn_orderdetails.strGraphicNo,
						trn_orderdetails.strStyleNo,
						trn_orderdetails.intSampleNo,
						trn_orderdetails.intSampleYear,
						trn_orderdetails.strCombo,
						trn_orderdetails.strPrintName,
						trn_orderdetails.intQty,
						trn_orderdetails.dblPrice,
						trn_orderdetails.dblOverCutPercentage,
						trn_orderdetails.dblDamagePercentage,
						trn_orderdetails.intRevisionNo,
						trn_orderdetails.dtPSD,
						trn_orderdetails.dtDeliveryDate, 
						trn_orderdetails.ITEM_CODE_FROM_CUSTOMER,
						trn_ordersizeqty.strSize,
						trn_ordersizeqty.dblQty as sizeQty ,
						mst_part.strName as partName , 
						mst_technique_groups.TECHNIQUE_GROUP_NAME AS STECHNIQUE_GRP
						FROM
						trn_orderdetails
						left Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId 
						left Join mst_part on mst_part.intId = trn_orderdetails.intPart 
						LEFT JOIN mst_technique_groups ON trn_orderdetails.TECHNIQUE_GROUP_ID= mst_technique_groups.TECHNIQUE_GROUP_ID 
						WHERE
						trn_orderdetails.intOrderNo 	=  '$orderNo' AND
						trn_orderdetails.intOrderYear 	=  '$orderYear'
					";
    $result1 = $db->RunQuery2($sql1);
    return $result1;
}

function checkfordataInGrading($sampleYear, $sampleNo, $revNo, $combo, $printName, $size)
{
    global $db;
    $sql = "SELECT * FROM
			trn_sampleinfomations_gradings
			WHERE
			trn_sampleinfomations_gradings.SAMPLE_NO = $sampleNo 
			AND trn_sampleinfomations_gradings.SAMPLE_YEAR = $sampleYear 
			AND trn_sampleinfomations_gradings.REVISION_NO = $revNo
			AND trn_sampleinfomations_gradings.COMBO = '$combo' 
			AND trn_sampleinfomations_gradings.PRINT = '$printName'
			";
    //	echo $sql;
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    if ($row['SAMPLE_NO'] != "") {
        return 1;
    }

}

function getsizeingrading($sampleYear, $sampleNo, $revNo, $combo, $printName, $size)
{
    global $db;
    $sql = "SELECT * FROM
			trn_sampleinfomations_gradings
			WHERE
			trn_sampleinfomations_gradings.SAMPLE_NO =  $sampleNo 
			AND trn_sampleinfomations_gradings.SAMPLE_YEAR = $sampleYear 
			AND trn_sampleinfomations_gradings.REVISION_NO = $revNo
			AND trn_sampleinfomations_gradings.COMBO = '$combo' 
			AND trn_sampleinfomations_gradings.PRINT = '$printName'
			AND trn_sampleinfomations_gradings.SIZE  = '$size'";
    //echo $sql;
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    if (strtolower($row['SIZE']) != strtolower($size) || $row['SIZE'] == "")
        return 1;

}

function get_grading($orderNo, $orderYear)
{
    global $db;
    $sql_select = "
					SELECT
					*,
					trn_orderdetails.IS_GRADING
					FROM
					trn_orderdetails
					WHERE
					trn_orderdetails.intOrderNo =  $orderNo
					AND
					trn_orderdetails.intOrderYear = $orderYear ";
    //echo $sql_select;
    $result = $db->RunQuery2($sql_select);
    return $result;

}

function get_grading_bulk($orderNo, $orderYear, $sales)
{
    global $db;
    $sql1 = "	SELECT 
		 				trn_orderdetails.strLineNo,
						trn_orderdetails.strSalesOrderNo,
						trn_orderdetails.intSalesOrderId, 
						trn_orderdetails.strGraphicNo,
						trn_orderdetails.strStyleNo,
						trn_orderdetails.intSampleNo,
						trn_orderdetails.intSampleYear,
						trn_orderdetails.strCombo,
						trn_orderdetails.strPrintName,
						trn_orderdetails.intQty,
						trn_orderdetails.dblPrice,
						trn_orderdetails.dblOverCutPercentage,
						trn_orderdetails.dblDamagePercentage,
						trn_orderdetails.intRevisionNo,
						trn_orderdetails.dtPSD,
						trn_orderdetails.dtDeliveryDate, 
						trn_orderdetails.ITEM_CODE_FROM_CUSTOMER,
						trn_ordersizeqty.strSize,
						trn_ordersizeqty.dblQty as sizeQty ,
						mst_part.strName as partName , 
						mst_technique_groups.TECHNIQUE_GROUP_NAME AS STECHNIQUE_GRP
						FROM
						trn_orderdetails
						left Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId 
						left Join mst_part on mst_part.intId = trn_orderdetails.intPart 
						LEFT JOIN mst_technique_groups ON trn_orderdetails.TECHNIQUE_GROUP_ID= mst_technique_groups.TECHNIQUE_GROUP_ID 
						WHERE
						trn_orderdetails.intOrderNo 	=  '$orderNo' AND
						trn_orderdetails.intOrderYear 	=  $orderYear and
						trn_orderdetails.intSalesOrderId  =  $sales
					";
    //echo $sql1;
    $result1 = $db->RunQuery2($sql1);
    $row = mysqli_fetch_array($result1);
    if ($row['strSize'] == "")
        return 1;

}


function get_grading_sample($orderNo, $orderYear, $sales)
{
    global $db;
    $sql1 = "	SELECT 
		 				trn_orderdetails.strLineNo,
						trn_orderdetails.strSalesOrderNo,
						trn_orderdetails.intSalesOrderId, 
						trn_orderdetails.strGraphicNo,
						trn_orderdetails.strStyleNo,
						trn_orderdetails.intSampleNo,
						trn_orderdetails.intSampleYear,
						trn_orderdetails.strCombo,
						trn_orderdetails.strPrintName,
						trn_orderdetails.intQty,
						trn_orderdetails.dblPrice,
						trn_orderdetails.dblOverCutPercentage,
						trn_orderdetails.dblDamagePercentage,
						trn_orderdetails.intRevisionNo,
						trn_orderdetails.dtPSD,
						trn_orderdetails.dtDeliveryDate, 
						trn_orderdetails.ITEM_CODE_FROM_CUSTOMER,
						trn_ordersizeqty.strSize,
						trn_ordersizeqty.dblQty as sizeQty ,
						mst_part.strName as partName , 
						mst_technique_groups.TECHNIQUE_GROUP_NAME AS STECHNIQUE_GRP
						FROM
						trn_orderdetails
						left Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId 
						left Join mst_part on mst_part.intId = trn_orderdetails.intPart 
						LEFT JOIN mst_technique_groups ON trn_orderdetails.TECHNIQUE_GROUP_ID= mst_technique_groups.TECHNIQUE_GROUP_ID 
						WHERE
						trn_orderdetails.intOrderNo 	=  '$orderNo' AND
						trn_orderdetails.intOrderYear 	=  $orderYear AND
						trn_orderdetails.intSalesOrderId  =  $sales";
    //echo $sql1;
    $result1 = $db->RunQuery2($sql1);
    while ($row = mysqli_fetch_array($result1)) {

        $intSampleNo = $row['intSampleNo'];
        $intSampleYear = $row['intSampleYear'];
        $strCombo = $row['strCombo'];
        $strPrintName = $row['strPrintName'];
        $intRevisionNo = $row['intRevisionNo'];

        $sql = "SELECT * FROM
							trn_sampleinfomations_gradings
							WHERE
							trn_sampleinfomations_gradings.SAMPLE_NO 		= 	$intSampleNo	
							AND trn_sampleinfomations_gradings.SAMPLE_YEAR 	=	$intSampleYear	 
							AND trn_sampleinfomations_gradings.REVISION_NO 	= 	$intRevisionNo
							AND trn_sampleinfomations_gradings.COMBO 		= 	'$strCombo' 
							AND trn_sampleinfomations_gradings.PRINT 		= 	'$strPrintName'
							";
        //echo $sql;
        $result = $db->RunQuery2($sql);
        $row = mysqli_fetch_array($result);
        if ($row['SIZE'] == "")
            return 1;
    }
}

function is_matched($orderNo, $orderYear, $sales)
{
    global $db;

    $sql1 = "	SELECT 
		 				trn_orderdetails.strLineNo,
						trn_orderdetails.strSalesOrderNo,
						trn_orderdetails.intSalesOrderId, 
						trn_orderdetails.strGraphicNo,
						trn_orderdetails.strStyleNo,
						trn_orderdetails.intSampleNo,
						trn_orderdetails.intSampleYear,
						trn_orderdetails.strCombo,
						trn_orderdetails.strPrintName,
						trn_orderdetails.intQty,
						trn_orderdetails.dblPrice,
						trn_orderdetails.dblOverCutPercentage,
						trn_orderdetails.dblDamagePercentage,
						trn_orderdetails.intRevisionNo,
						trn_orderdetails.dtPSD,
						trn_orderdetails.dtDeliveryDate, 
						trn_orderdetails.ITEM_CODE_FROM_CUSTOMER,
						trn_ordersizeqty.strSize,
						trn_ordersizeqty.dblQty as sizeQty ,
						mst_part.strName as partName , 
						mst_technique_groups.TECHNIQUE_GROUP_NAME AS STECHNIQUE_GRP
						FROM
						trn_orderdetails
						left Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId 
						left Join mst_part on mst_part.intId = trn_orderdetails.intPart 
						LEFT JOIN mst_technique_groups ON trn_orderdetails.TECHNIQUE_GROUP_ID= mst_technique_groups.TECHNIQUE_GROUP_ID 
						WHERE
						trn_orderdetails.intOrderNo 	=  '$orderNo' AND
						trn_orderdetails.intOrderYear 	=  $orderYear and
						trn_orderdetails.intSalesOrderId  =  $sales
					";
    //echo $sql1;
    $result1 = $db->RunQuery2($sql1);
    while ($row = mysqli_fetch_array($result1)) {
        $intSampleNo = $row['intSampleNo'];
        $intSampleYear = $row['intSampleYear'];
        $strCombo = $row['strCombo'];
        $strPrintName = $row['strPrintName'];
        $intRevisionNo = $row['intRevisionNo'];
        $size = $row['strSize'];

        $sql = "SELECT * FROM
							trn_sampleinfomations_gradings
							WHERE
							trn_sampleinfomations_gradings.SAMPLE_NO 		= 	$intSampleNo	
							AND trn_sampleinfomations_gradings.SAMPLE_YEAR 	=	$intSampleYear	 
							AND trn_sampleinfomations_gradings.REVISION_NO 	= 	$intRevisionNo
							AND trn_sampleinfomations_gradings.COMBO 		= 	'$strCombo' 
							AND trn_sampleinfomations_gradings.PRINT 		= 	'$strPrintName'
							AND trn_sampleinfomations_gradings.SIZE 		 = 	'$size'
							";
        //echo $sql;
        $result = $db->RunQuery2($sql);
        $row_r = mysqli_fetch_array($result);

        if (strtolower($row_r['SIZE']) != strtolower($size) || $row_r['SIZE'] == "") {
            return 1;
        }
    }
}

function send_detais($record, $token)
{

    if ($token == "") { //if token null
        generate_token($record);
    }

    $configs = include('../../../../../config/ncigaConfig.php');
    $url = $configs['URL'];
    //data passed with the token
    $data = array('token' => $token, 'records' => $record);
    //encode in json format
    $data_json = json_encode($data);
    // print_r($data_json);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    //
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($ch);
    $nciga_response = json_decode($response); //get nciga response
    //print_r($nciga_response);
    $message_code = "NORES";
    $message_code = $nciga_response->messageCode;

    if ($message_code == 'NCN0003') { //token expired
        generate_token($record);
        update_tbl($record, $message_code);
    } else if ($message_code == 'NCN0001') {//success
        update_tbl($record, $message_code);
    } else if ($message_code == 'NCN0002') {//invalid token
        generate_token($record);
        update_tbl($record, $message_code);
    } else if ($message_code == 'NCN0007') {//Failed to submit data
        update_tbl($record, $message_code);
        send_detais($record, $token);
    } else if ($message_code == 'NCN0005') {//Invalid URL
        update_tbl($record, $message_code);
        send_detais($record, $token);
    } else if ($message_code == 'NCN0006') {//Internal server Error , Please contact administrator
        update_tbl($record, $message_code);
        send_detais($record, $token);
    } else if ($message_code == 'NCN0008') {//Mandatory value missing
        update_tbl($record, $message_code);
        //send_detais($record, $token);
    } else if ($message_code == 'NCN0009') {//Mandatory value missing
        update_tbl($record, $message_code);
        //send_detais($record, $token);
    }


    if (!$response) {

        die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    }
    curl_close($ch);
}

function generate_token($record)
{
    $configs = include('../../../../../config/ncigaConfig.php');
    $url = $configs['URL'];
    $AUTH_USERNAME = $configs['AUTH_USERNAME'];
    $AUTH_PASSWORD = $configs['AUTH_PASSWORD'];
    $TYPE = $configs['TYPE'];
    $data = array('username' => $AUTH_USERNAME, 'password' => $AUTH_PASSWORD, 'type' => $TYPE);
    $data_json = json_encode($data);
    //print_r($data_json);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    //
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($ch);

    curl_close($ch);

    insert_tbl($response); //insert token
    $token_new = select_token(); //get newly generated token
    send_detais($record, $token_new); //send details again

    if (!$response) {
        die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    }
    curl_close($ch);
}

function insert_tbl($data)
{
    global $db;
    $date = date('Y-m-d H:i:s', time());

    $jsonArray = json_decode($data, true);
    $userId = $jsonArray['userId'];
    $generatedTime = $jsonArray['generatedTime'];
    $expireDate = $jsonArray['expireDate'];
    $token = $jsonArray['token'];

    $sql_insert = "
                            INSERT INTO
                            tbl_token_details
                            (USER_ID,
                            GENERATED_TIME,
                            EXPIRED_DATE,
                            TOKEN,
                            UPDATED_TIME
                            )
                            VALUES
                            ('$userId',
                             $generatedTime,
                             $expireDate,   
                             '$token',
                             '$date'  
                            );";

    $db->RunQuery2($sql_insert);
}

//--------------------------------------------------------
function select_token()
{
    global $db;
    $sql_token = "SELECT UPDATED_TIME, date(UPDATED_TIME),TOKEN
                          FROM tbl_token_details AS a
                          WHERE (UPDATED_TIME)= (
                          SELECT MAX((UPDATED_TIME))
                          FROM tbl_token_details AS b )";

    $result = $db->RunQuery2($sql_token);
    $row = mysqli_fetch_array($result);
    $token = $row['TOKEN'];
    return $token;
}

function update_tbl($record, $message_code)
{
    global $db;

    foreach ($record as $key => $val) {

        foreach ($val as $data) {

            $orderNo = $data['orderNo'];
            $year = $data['year'];
            $customerSO = $data['SalesOrderId'];
            $size = $data['size'];

            $sql_retryCount = "SELECT trn_ordersizeqty.retryCount,
                                trn_ordersizeqty.maxRetryCount
                                FROM
                                trn_ordersizeqty
                                WHERE
                                trn_ordersizeqty.intOrderNo = $orderNo
                                AND trn_ordersizeqty.intOrderYear = $year
                                AND trn_ordersizeqty.intSalesOrderId = $customerSO
                                AND  trn_ordersizeqty.strSize = '$size'";

            $result = $db->RunQuery2($sql_retryCount);
            $row = mysqli_fetch_array($result);
            $retryCount = $row['retryCount'];
            $maxRetryCount = $row['maxRetryCount'];

            $sql_update = "  UPDATE
                                trn_ordersizeqty";
            if ($message_code == 'NCN0001') {
                $sql_update .= " SET
                                    trn_ordersizeqty.ncingaDeliveryStatus = 1";
            } else {
                $sql_update .= " SET
                                    
                                    trn_ordersizeqty.retryCount = $retryCount+1";
            }
            $sql_update .= ", nciga_code = '$message_code'";
            $sql_update .= " WHERE
                                    trn_ordersizeqty.intOrderNo = $orderNo
                                    AND trn_ordersizeqty.intOrderYear = $year
                                    AND trn_ordersizeqty.intSalesOrderId = $customerSO
                                    AND  trn_ordersizeqty.strSize = '$size'
                                    AND trn_ordersizeqty.retryCount   < $maxRetryCount";

            $db->RunQuery2($sql_update);
        }
    }
}

function updateAzureTable($orderNo, $orderYear, $db, $transactionType, $revisedDate)
{

    global $objAzure;
    $environment = $objAzure->getEnvironment();
    $headerTable = 'SalesHeader' . $environment;
    $lineTable = 'SalesLine' . $environment;
    $azure_connection = $objAzure->connectAzureDB();

    $sql_header = "SELECT
			trn_orderheader.intReviseNo,
			trn_orderheader.intMarketer AS marketer,
			trn_orderheader.intStatus,
			trn_orderheader.strRemark,
			trn_orderheader.intApproveLevelStart,
			trn_orderheader.strCustomerPoNo,
			trn_orderheader.intLocationId,
			trn_orderheader.intCustomer,
			trn_orderheader.intPaymentTerm,
			trn_orderheader.dtmCreateDate AS order_date,
			mst_customer.strName AS customer,
			mst_customer.strCode AS customerCode,
			trn_orderheader.strCustomerPoNo,
			mst_customer_locations_header.strName AS customer_location,
			mst_financecurrency.strCode AS curr_code,
			marketer.strUserName,
			(
							SELECT
								CAST(
									MAX(
										ware_stocktransactions_fabric.dtDate
									) AS DATE
								)
							FROM
								ware_stocktransactions_fabric
							WHERE
								ware_stocktransactions_fabric.intOrderNo = trn_orderheader.intOrderNo
							AND ware_stocktransactions_fabric.intOrderYear = trn_orderheader.intOrderYear
							AND ware_stocktransactions_fabric.strType LIKE '%Dispatched%' 
							GROUP BY
								ware_stocktransactions_fabric.intOrderNo,
								ware_stocktransactions_fabric.intOrderYear
			) AS lastDispatchDate
			FROM
			trn_orderheader
			INNER JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
			INNER JOIN mst_financecurrency ON mst_financecurrency.intId = trn_orderheader.intCurrency
			INNER JOIN sys_users AS marketer ON marketer.intUserId = trn_orderheader.intMarketer
			INNER JOIN trn_orderheader_approvedby ON trn_orderheader_approvedby.intOrderNo=trn_orderheader.intOrderNo
			AND trn_orderheader_approvedby.intYear=trn_orderheader.intOrderYear AND trn_orderheader_approvedby.intApproveLevelNo=3
			LEFT JOIN mst_customer_locations_header ON trn_orderheader.intCustomerLocation = mst_customer_locations_header.intId
			WHERE
			trn_orderheader.intOrderNo = '$orderNo'
			AND trn_orderheader.intOrderYear = '$orderYear'                         
			GROUP BY
			trn_orderheader.intOrderNo,
			trn_orderheader.intOrderYear";

    $result_header = $db->RunQuery2($sql_header);
    while ($row_header = mysqli_fetch_array($result_header)) {
        $poString = trim($orderNo) . '/' . trim($orderYear);
        $customer = trim($row_header['customer']);
        $cus_location = trim($row_header['customer_location']);
        $customerCode = $row_header['customerCode'];
        $strCustomerPoNo = trim($row_header['strCustomerPoNo']);
        $strRemark = trim($row_header['strRemark']);
        $strRemark_sql = $db->escapeString($strRemark);
        $orderDate = $row_header['order_date'];
        $curr_code = ($row_header['curr_code'] == 'EURO') ? "Eur" : ($row_header['curr_code'] == 'LKR' ? "" : $row_header['curr_code']);
        $payment_term = $row_header['intPaymentTerm'];
        $marketer = $row_header['marketer'];
        $location = $row_header['intLocationId'];
        $revise_no = $row_header['intReviseNo'];
        $disp_date = $row_header['lastDispatchDate'];
        $successHeader = 0;
        $i = 0;
        if ($transactionType == 'SO_EditStart') {
            $revisedDate = date('Y-m-d H:i:s');
        }
        $postingDate = date('Y-m-d H:i:s');

        $sql_select = "SELECT
						OD.intSalesOrderId,
						OD.strSalesOrderNo,
						OD.intOrderNo,
						OD.intOrderYear,
						OD.strLineNo,
						OD.SO_TYPE,
						OD.splitedPrice,
						OD.dtDeliveryDate AS soDeliverydate,
						OD.dtPSD AS PSD,
						OD.strGraphicNo,
						OD.intSampleNo,
						OD.intSampleYear,
						OD.strStyleNo,
						OD.strPrintName,
						OD.dblOverCutPercentage,
						OD.dblDamagePercentage,
						OD.strCombo,
						OD.TECHNIQUE_GROUP_ID, 
						(
						  select TECHNIQUE_GROUP_NAME from mst_technique_groups where mst_technique_groups.TECHNIQUE_GROUP_ID = OD.TECHNIQUE_GROUP_ID
						)AS technique,
						OD.dtPSD,
						OD.intQty AS poqty,
						OD.dblPrice AS price,
						OD.intOrderYear,
						OD.intPart,
						mst_part.strName AS partName,
						OD.dblDamagePercentage,
						mst_brand.strName AS brand,
						trn_sampleinfomations_details.intGroundColor,
						mst_colors_ground.strName AS groundColor,
						(
							SELECT
								sum(
									finance_customer_invoice_details.QTY
								)
							FROM
								finance_customer_invoice_details
							INNER JOIN finance_customer_invoice_header ON finance_customer_invoice_header.SERIAL_NO = finance_customer_invoice_details.SERIAL_NO
							AND finance_customer_invoice_header.SERIAL_NO = finance_customer_invoice_details.SERIAL_NO
							AND finance_customer_invoice_header. STATUS = 1
							WHERE
								finance_customer_invoice_header.ORDER_NO = OD.intOrderNo
							AND finance_customer_invoice_header.ORDER_YEAR = OD.intOrderYear
							AND finance_customer_invoice_details.SALES_ORDER_ID = OD.intSalesOrderId
						) AS invoiced_qty,
						(
							SELECT
								IFNULL(
									SUM(
										CEIL(
											trn_ordersizeqty.dblQty * (
												trn_orderdetails.dblOverCutPercentage + trn_orderdetails.dblDamagePercentage
											) / 100
										)
									),
									0
								) AS excessQty
							FROM
								trn_orderdetails
							INNER JOIN trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo
							AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear
							AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
							WHERE
								trn_orderdetails.intOrderNo = OD.intOrderNo
							AND trn_orderdetails.intOrderYear = OD.intOrderYear
							AND trn_orderdetails.intSalesOrderId = OD.intSalesOrderId
						) AS excessQty
					FROM
						trn_orderdetails OD
					INNER JOIN mst_part ON mst_part.intId = OD.intPart
					INNER JOIN trn_sampleinfomations_details ON OD.intSampleNo = trn_sampleinfomations_details.intSampleNo
					AND OD.intSampleYear = trn_sampleinfomations_details.intSampleYear
					AND OD.intRevisionNo = trn_sampleinfomations_details.intRevNo
					AND OD.strCombo = trn_sampleinfomations_details.strComboName
					AND OD.strPrintName = trn_sampleinfomations_details.strPrintName
					INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
					AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
					AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo
					INNER JOIN mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
					INNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
					WHERE
						OD.intOrderNo = '$orderNo'
					AND OD.intOrderYear = '$orderYear' AND OD.SO_TYPE > -1 
					GROUP BY
						OD.intOrderNo,
						OD.intOrderYear,
						OD.intSalesOrderId";

        $result1 = $db->RunQuery2($sql_select);
        while ($row = mysqli_fetch_array($result1)) {
            $intSalesOrderId = $row['intSalesOrderId'];
            $strSalesOrderNo = trim($row['strSalesOrderNo']);
            $line_no = intval($row['strLineNo']);
            $strGraphicNo = trim($row['strGraphicNo']);
            $strCombo = trim($row['strCombo']);
            $intSampleNo = $row['intSampleNo'];
            $intSampleYear = $row['intSampleYear'];
            $groundColor = trim($row['groundColor']);
            $strStyleNo = trim($row['strStyleNo']);
            $strPrintName = trim($row['strPrintName']);
            $partName = trim($row['partName']);
            $brand = trim($row['brand']);
            $sampleNo = $intSampleNo . '/' . $intSampleYear;
            $psd = $row['PSD'];
            $technique = $row['technique'];
            if (strpos($technique, 'Plotter') !== false) {
                $technique = 'Plotter/Lazer cut';
            }
            $successDetails = 0;
            $poqty = $row['poqty'];
            $invoiced_qty = $row['invoiced_qty'];
            $totalQty = $poqty + $row['excessQty'] + 2 - $invoiced_qty;
            $amount = round($row['price'], 5);
            if ($row['SO_TYPE'] == 2) {
                $amount = round($row['price'] + $row['splitedPrice'], 5);
            }
            if ($totalQty > 0) {
                $i++;
                $sql_azure_details = "INSERT into $lineTable (Transaction_type, Order_No, Document_Type, Sales_Order_No, Line_No, Sales_Order_Name, Item_Code, Description, Item_Category, Item_SubCategory, Quantity, Original_Quantity, Amount, VAT_Prod_Posting_Group, Technique_Revenue_Code, Graphic_No, Style_No, [Print], Combo, Part, Brand, Sample_No, Production_Start_Date, Revised_Count) VALUES ('$transactionType','$poString', 'Order','$intSalesOrderId','$line_no', '$strSalesOrderNo', 'PRINT','PRINT','PRINT','PRINT','$totalQty','$poqty','$amount', 'VAT', '$technique', '$strGraphicNo','$strStyleNo','$strPrintName','$strCombo','$partName','$brand','$sampleNo','$psd','$revise_no')";
                if ($azure_connection) {
                    $getResults = $objAzure->runQuery($azure_connection, $sql_azure_details);
                    if ($getResults != FALSE) {
                        $successDetails = 1;
                    }
                }
                $sql_details = "INSERT into trn_financemodule_salesline (Transaction_type, Order_No, Sales_Order_No, Line_No, Sales_Order_Name, Item_Code, Description, Item_Category, Item_SubCategory, Quantity, Original_Quantity, Amount, VAT_Prod_Posting_Group, Technique_Revenue_Code, Graphic_No, Style_No, Print, Combo, Part, Brand, Sample_No, Production_Start_Date, Revised_Count, deliveryStatus, deliveryDate) VALUES ('$transactionType','$poString','$intSalesOrderId','$line_no', '$strSalesOrderNo', 'PRINT','PRINT','PRINT','PRINT','$totalQty','$poqty','$amount', 'VAT', '$technique', '$strGraphicNo','$strStyleNo','$strPrintName','$strCombo','$partName','$brand','$sampleNo','$psd','$revise_no','$successDetails',NOW())";
                $result_details = $db->RunQuery2($sql_details);
            }
        }

        $sql_azure_header = "INSERT into $headerTable (Transaction_type, Order_No, Document_Type, Customer_Code, Customer_Name, Posting_Date, Order_Date, Currency_Code, Revised_Date, Narration, Payment_Terms, Customer_PO_Number, Sales_Person, Location_Code, No_of_Lines, Revised_Count, Last_Dispatched_Date, Customer_Location) VALUES ('$transactionType','$poString','Order','$customerCode','$customer', '$postingDate', '$orderDate','$curr_code','$revisedDate','$strRemark','$payment_term','$strCustomerPoNo', '$marketer', '$location', '$i', '$revise_no', '$disp_date', '$cus_location')";
        if ($azure_connection) {
            $getResults = $objAzure->runQuery($azure_connection, $sql_azure_header);
            if ($getResults != FALSE) {
                $successHeader = 1;
            }
        }

        $sql = "INSERT into trn_financemodule_salesheader (Transaction_type, Order_No, Document_Type, Customer_Code, Customer_Name, Posting_Date, Order_Date, Currency_Code, Revised_Date, Narration, Payment_Terms, Customer_PO_Number, Sales_Person, Location_Code, No_of_Lines, Revised_Count, Last_Dispatched_Date, Customer_Location, deliveryStatus, deliveryDate ) VALUES ('$transactionType','$poString','Order','$customerCode','$customer', '$postingDate', '$orderDate','$curr_code','$revisedDate','$strRemark_sql','$payment_term','$strCustomerPoNo', '$marketer', '$location', '$i', '$revise_no', '$disp_date', '$cus_location', '$successHeader', NOW())";
        $result_header = $db->RunQuery2($sql);

    }
}

function getRevisedDate($poNo, $poYear, $revision_No, $db)
{
    $poString = $poNo . '/' . $poYear;
    $revised_date = '';
    $sql = "SELECT MAX(Revised_Date) AS Revised_Date FROM `trn_financemodule_salesheader` WHERE `Transaction_Type` = 'SO_EditStart' AND `Order_No` = '$poString' AND `Revised_Count` <= '$revision_No'";
    $result = $db->RunQuery2($sql);
    while ($row = mysqli_fetch_array($result)) {
        $revised_date = $row['Revised_Date'];
    }
    return $revised_date;
}

function getLinkedSoId($orderNo, $year,$soId){

    global $db;

    $sql = "SELECT 
			trn_orderdetails.intSalesOrderId 
			FROM trn_orderdetails 
			WHERE
			trn_orderdetails.intOrderNo =  '$orderNo' AND
			trn_orderdetails.intOrderYear =  '$year' AND 
			trn_orderdetails.linkedSoId =  '$soId'
			";
    $result = $db->RunQuery2($sql);
    $row=mysqli_fetch_array($result);

    return $row['intSalesOrderId'];
}

function qty_price_validate($sampleNo,$sampleYear,$revNo,$combo,$printName,$price, $soQty){

    global $db;
    $sql = "SELECT 	LOWER_MARGIN, UPPER_MARGIN 
		FROM 
		costing_sample_qty_wise 
		WHERE  
		SAMPLE_NO = '$sampleNo' AND
		SAMPLE_YEAR = '$sampleYear' AND
		REVISION = '$revNo' AND
		COMBO = '$combo' AND
		PRINT = '$printName' AND 
		PRICE = '$price' ";

    $result 	= $db->RunQuery2($sql);
    $row		= mysqli_fetch_array($result);
    if ($row['LOWER_MARGIN'] <= $soQty && ($soQty <= $row['UPPER_MARGIN'] || $row['UPPER_MARGIN']==-2)){
        $validationStatus["qty_price_matched"]=true;
    } else {
        $validationStatus["qty_price_matched"]=false;
        $validationStatus["message"]="So Price is not within the qty break down price for ".$sampleNo."/".$sampleYear."/".$revNo."/".$printName."/".$combo;
    }
    return $validationStatus;

}


?>







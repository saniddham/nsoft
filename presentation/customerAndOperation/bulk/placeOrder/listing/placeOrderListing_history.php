<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php

include_once "libraries/jqgrid2/inc/jqgrid_dist.php";

$company 		= $sessions->getCompanyId();

$approveLevel 	= (int)getMaxApproveLevel();
$programCode	= 'P0427';
$intUser  		= $_SESSION["userId"];

//------------------------------------------
$sql1 = "SELECT intId,strName FROM
			mst_companies
			ORDER BY
			mst_companies.strName ASC";
$result1 = $db->RunQuery($sql1);

$str1   = "" ;
//$str1   = ":All;" ;
$j		= 0;
while($row=mysqli_fetch_array($result1))
{
	if($company	== $row['intId']){
		$inilocId		= $row['intId'];
		$inilocation	= $row['strName'];
	}
	else{
		$locId			= $row['intId'];
		$location		= $row['strName'];
		$str1 		   .= $location.":".$location.";" ;
	}
	$j++;
}
$str2	= $inilocation.":".$inilocation.";" ;
$str	= $str2.$str1;
$str .= ":All" ;
//---------------------------------------------------------------------

//BEGIN - ADD DEFAULT WHERE STRING WHEN FORM LOADING {
$arr =  json_decode($_REQUEST['filters'],true);

$arr = $arr['rules'];

$where_string = '';
$where_array = array(
	'company'=>'mst_companies.strName',
	'Order_No'=>"tb1.intOrderNo",
	'Order_Year'=>"tb1.intOrderYear",
	'Customer'=>"mst_customer.strName",
	'Location_Name'=>"mst_customer_locations_header.strName",
	'Marketer'=>"sys_users.strFullName",
);

$arr_status = array('Approved'=>'1','Rejected'=>'0','Revised'=>'-1','Cancelled'=>'-2','Completed'=>'-10','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}

if(!count($arr)>0){
	$where_string .= "AND tb1.dtDate = '".date('Y-m-d')."'";
	$where_string .= "AND mst_companies.strName = '$inilocation'";
}
//END }



$sql = "select * from(SELECT DISTINCT
							mst_companies.strName as company,
							tb1.intOrderNo as `Order_No`,
							tb1.intOrderYear as `Order_Year`,
							tb1.strCustomerPoNo as `Customer_PO_No`,
							tb1.REVISION_NO as RevisionNO,
							mst_customer.strName as `Customer`,
							sys_users.strFullName as Marketer,";

$sql .= "
							'View' as `View`
						FROM
							trn_orderheader_revision_history as tb1
							Inner Join mst_customer ON tb1.intCustomer = mst_customer.intId
							Inner Join sys_users ON tb1.intMarketer = sys_users.intUserId
							Inner Join mst_locations ON tb1.intLocationId = mst_locations.intId
							INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
							INNER JOIN mst_customer_locations_header ON mst_customer_locations_header.intId = tb1.intCustomerLocation
							left join mst_customer_po_techniques on mst_customer_po_techniques.ID=tb1.TECHNIQUE_TYPE
							WHERE 1=1
								$where_string
							)  as t where 1=1
						";
//echo $sql;
$col = array();

$col["title"] 	= "Company"; // caption of column
$col["name"] 	= "company"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] 	= "8";
$col["stype"] 	= "select";
$str = $str ;
$col["editoptions"] 	=  array("value"=> $str);
$col["align"] 	= "center";

$cols[] = $col;	$col=NULL;

//ORDER NO
$col["title"] 	= "Order No"; // caption of column
$col["name"] 	= "Order_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] 	= "3";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= "?q=427&orderNo={Order_No}&orderYear={Order_Year}";
$col["linkoptions"] = "target='placeOrder.php'"; // extra params with <a> tag

$reportLink  		= "?q=1176&orderNo={Order_No}&orderYear={Order_Year}&revisionNo={RevisionNO}";
$reportLinkApprove  = "?q=1176&orderNo={Order_No}&orderYear={Order_Year}&approveMode=1";

$cols[] = $col;	$col=NULL;


//ORDER YEAR
$col["title"] = "Order year"; // caption of column
$col["name"] = "Order_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//CUSTOMER PO NO
$col["title"] = "Customer PO No"; // caption of column
$col["name"] = "Customer_PO_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "4";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Customer"; // caption of column
$col["name"] = "Customer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Markter
$col["title"] = "Marketer"; // caption of column
$col["name"] = "Marketer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//REVISE
$col["title"] = "Revision No"; // caption of column
$col["name"] = "RevisionNO"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "2";
$col["search"] = false;
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//VIEW
$col["title"] = "View"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "2";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='rptBulkOrder_history.php'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Place Order History Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'Order_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


$sarr = <<< SEARCH_JSON
{
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"},{"field":"Company","op":"eq","data":"$inilocation"}
     ]

}
SEARCH_JSON;

$grid["search"] = true;

// export PDF file
// export to excel parameters
$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(
		"add"=>false, // allow/disallow add
		"edit"=>false, // allow/disallow edit
		"delete"=>false, // allow/disallow delete
		"rowactions"=>false, // show/hide row wise edit/del/save option
		"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
		"export"=>true
	)
);
$out = $jq->render("list1");
?>
<title>Customer PO Listing</title>
<?php echo $out?>

<?php
function getMaxApproveLevel()
{
	global $db;

	$appLevel=0;
	$sqlp = "SELECT
			Max(trn_orderheader.intApproveLevelStart) AS appLevel
			FROM trn_orderheader";

	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);

	return $rowp['appLevel'];
}
?>


var basepath	= 'presentation/customerAndOperation/bulk/placeOrder/listing/';

$(document).ready(function() {
	
	//------------
	$('#frmSamplePlaceOrderReport #imgApprove').click(function(){
		//showWaiting();
	var val = $.prompt('Are you sure you want to approve this Bulk PO ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
						if(validate()==0){ 
							var url = basepath+"rptBulkOrder-db-set.php"+window.location.search+'&status=approve';
							/*var obj = $.ajax({url:url,async:false});
							window.location.href = window.location.href;
							window.opener.location.reload();//reload listing page*/
     						var obj = $.ajax({
										url:url,
										
										dataType: "json", 
										type:'POST', 
										data:'',//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
										//data:'{"requestType":"addsampleInfomations"}',
										async:false,
										
										success:function(json){
												$('#frmSamplePlaceOrderReport #imgApprove').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												if(json.type=='pass')
												{
													//hideWaiting();
													//$('#frmSampleInfomations').get(0).reset();
													//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
													//loadCombo_frmSampleInfomations();
													hideWaiting();
							
													var t=setTimeout("alertx()",1000);
													//window.location.href = window.location.href;
													//window.opener.location.reload();//reload listing page*/
												}
											},
										error:function(xhr,status){
													hideWaiting();
												$('#frmSamplePlaceOrderReport #imgApprove').validationEngine('showPrompt', errormsg(xhr.status),'fail');
												var t=setTimeout("alertx()",3000);
												//function (xhr, status){errormsg(status)}
											}		
										});
						}
					}
				}});
		//hideWaiting();
	});
	//------------
	$('#frmSamplePlaceOrderReport #imgReject').click(function(){
		//showWaiting();
	var rejectState = {
			state0: {
				html:'<label>Are you sure you want to reject this Bulk PO ?</label></br><label>Enter the Reason : <textarea name="txtReason" id="txtReason" cols="50" rows="3"></textarea></label><br />',
				buttons: { Ok: true, Cancel: false },
				persistent: true,
				submit:function(v,m,f){
					if(v)
					{	
						if(validateRejecton()==0)
						{
							var reason 	= $('#txtReason').val();
							
							var arrHeader = "{";
							arrHeader += '"reason":'+URLEncode_json(reason)+'';
							arrHeader += "}";
								
							var url 	= basepath+"rptBulkOrder-db-set.php"+window.location.search+'&status=reject';
							//var obj 	= $.ajax({url:url,async:false});
							var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'POST',
											data:window.location.search+'&status=reject&arrHeader='+arrHeader,
											async:false
											});
							window.location.href = window.location.href;
							window.opener.location=window.opener.location;
						}
					}
				}
			}
		};
		$.prompt(rejectState);
		
		//hideWaiting();
		
		/*var rejectState = {
			state0: {
				html:'<label>Are you sure you want to reject this Bulk PO ?</label></br><label>Enter the Reason : <textarea name="txtReason" id="txtReason" cols="50" rows="3"></textarea></label><br />',
				buttons: { Ok: true, Cancel: false },
				persistent: false,
				submit:function(e,v,m,f){
					if(v)
					{	
						if(validateRejecton()==0)
						{
							var reason 	= $('#txtReason').val();
							
							var arrHeader = "{";
							arrHeader += '"reason":'+URLEncode_json(reason)+'';
							arrHeader += "}";
								
							var url 	= basepath+"rptBulkOrder-db-set.php"+window.location.search+'&status=reject';
							//var obj 	= $.ajax({url:url,async:false});
							var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'POST',
											data:window.location.search+'&status=reject&arrHeader='+arrHeader,
											async:false
											});
							window.location.href = window.location.href;
							window.opener.location=window.opener.location;
						}
					}
				}
			}
		};
$.prompt(rejectState);*/
		
		
	});
	//------------
	
	$('#frmSamplePlaceOrderReport .butRevise').live('click',function(){
	    var PONo = document.getElementById('divPoNo').innerHTML;
		if(validateRevise(PONo)==0){ 
			var val = $.prompt('Are you sure you want to revise this Bulk PO ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
						var url = basepath+"rptBulkOrder-db-set.php?PONo="+PONo+"&status=revisePO";
						var obj = $.ajax({url:url,async:false});
						//window.location.href = window.location.href;
					}
				}});
		}
	});
	//------------
});
//---------------------------------------------------
function validate(){
		var ret=0;	
	    var PONo = document.getElementById('divPoNo').innerHTML;
		var url 		= basepath+"rptBulkOrder-db-get.php?requestType=getValidation";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"PONo="+PONo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",5000);
					  ret= 1;
					}
				}
			});
			
			return ret;
}
//---------------------------------------------------
function validateRejecton(){
		var ret=0;	
	    var PONo = document.getElementById('divPoNo').innerHTML;
		var url 		= basepath+"rptBulkOrder-db-get.php?requestType=validateRejecton";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"PONo="+PONo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgReject').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertxRej()",3000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//---------------------------------------------------
function validateRevise(PONo){
		var ret=0;	
		var url 		= basepath+"rptBulkOrder-db-get.php?requestType=validateRevise";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"PONo="+PONo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#butRevise').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertxR()",3000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//---------------------------------------------------
function alertx()
{
	$('#frmSamplePlaceOrderReport #imgApprove').validationEngine('hide')	;
}
function alertxRej()
{
	$('#frmSamplePlaceOrderReport #imgApprove').validationEngine('hide')	;
}
function alertxR()
{
	$('#frmlisting #butRevise').validationEngine('hide')	;
}
<?php
ini_set('display_errors',0);
session_start();
$backwardseperator 	= "../../../../../";
include $backwardseperator."dataAccess/Connector.php";

$orderNo 			= $_REQUEST['orderNo'];
$orderYear			= $_REQUEST['orderYear'];



?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Order Quantity / Consumption Deduction Notification</title>
        
       
    </head>

    <body>
<form id="frmOrderStockMovementReport" name="frmOrderStockMovementReport" method="post" action="">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80"
valign="top"><?php include 'presentation/procurement/reports/reportHeader.php' ?></td>
<td width="20%"></td>
</tr>
<tr>
<td colspan="3">
<div id="divPoNo" style="display:none"><?php echo $year ?>/<?php echo $orderNo ?></div>
</td>
</tr>
</table>
<div align="center">

<div style="background-color:#FFF"><strong>Order Quantity / Consumption Deduction Notification </strong><strong></strong>
</div>

<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
<td>
<table width="100%">
<tr>
<td colspan="10" align="center" bgcolor="#FFFFFF">
</td>
</tr>
<tr>
<td width="1%"><span class="normalfnt"><strong>Order No:</strong></span></td>
<td width="9%" class="normalfnt"><strong><?php  echo $year. "/" ?></strong>
	
<strong><?php  echo $orderNo; ?>
</strong></td>
</tr>

</table>
</td>
</tr>
<tr>
<td>
<table width="100%">
<tr>
<td colspan="20">
<?php 
?>
<table width="100%" class="bordered" id="tblMainGrid" cellspacing="0"
cellpadding="0">

<tr>
<th>Item</th>
<th>Previous Requested</th>
<th>Current Requested</th>
<th>Sup PO</th>
<th>GRN</th>
<th>MRN</th>
</tr>


<tbody>
<?php
$result 	=	get_details($orderNo,$orderYear);
while ($row	= 	mysqli_fetch_array($result))
{
?>			
<tr>
<td <?php if($row['current_qty']==""){?> style="color:red;" <?php }?>> <?php echo $row['strName']; ?></td>
<td> <?php echo $row['previous_qty']; ?></td>
<td><?php echo $row['current_qty']; ?></td>
<td><?php echo $row['sup_po']; ?></td>
<td><?php echo $row['grn_no']; ?></td>
<td ><?php echo $row['mrn_no']; ?></td>
</tr>

<?php
}
?>	
</tbody>











                                         

</table>



	
	


</table>



</tr></table>		




		
</td>
</tr>

</table>

</div>
</form>
</body>
</html>
<?php



function get_details($orderNo,$orderYear)
{
	global $db;
	
	$sql_select_revision =
							"SELECT
							trn_orderheader.intReviseNo
							FROM
							trn_orderheader
							WHERE
							trn_orderheader.intOrderNo = $orderNo
							AND
							trn_orderheader.intOrderYear = $orderYear ";
							//echo $sql_select_revision;
							$result	 	= $db->RunQuery($sql_select_revision);
							$row		= mysqli_fetch_array($result);
							$revision	= $row['intReviseNo'];
	
	
				$sql_select =
							"select 
							trn_po_prn_details_history.ITEM,
							mst_item.strName,
							trn_po_prn_details_history.REQUIRED as previous_qty,
							(select (trn_po_prn_details.REQUIRED)
							FROM
							trn_po_prn_details
							WHERE
							trn_po_prn_details.ORDER_NO = trn_po_prn_details_history.ORDER_NO
							AND
							trn_po_prn_details.ORDER_YEAR = trn_po_prn_details_history.ORDER_YEAR
							AND
							trn_po_prn_details.ITEM = trn_po_prn_details_history.ITEM
							) as current_qty,
							
							(select group_concat(concat(ware_mrnheader.intMrnNo,'/',ware_mrnheader.intMrnYear))
							FROM
							ware_mrnheader
							INNER JOIN
							ware_mrndetails
							ON
							ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND
							ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
							
							INNER JOIN
							trn_orderheader as t ON
							t.intOrderNo = ware_mrndetails.intOrderNo AND
							t.intOrderYear = ware_mrndetails.intOrderYear 
							
							WHERE
							t.intOrderNo = trn_orderheader.intOrderNo AND
							t.intOrderYear = trn_orderheader.intOrderYear
							) as mrn_no,
							
							(select GROUP_CONCAT(concat(ware_grnheader.intGrnNo,'/',ware_grnheader.intGrnYear))
							FROM
							ware_grnheader
							INNER JOIN
							ware_grndetails
							ON
							ware_grndetails.intGrnNo= ware_grnheader.intGrnNo AND
							ware_grndetails.intGrnYear = ware_grnheader.intGrnYear
							
							INNER JOIN
							trn_orderheader as t ON
							t.intOrderNo = ware_grndetails.intGrnNo AND
							t.intOrderYear = ware_grndetails.intGrnYear 
							
							WHERE
							t.intOrderNo = trn_orderheader.intOrderNo AND
							t.intOrderYear = trn_orderheader.intOrderYear
							) as grn_no,
							
							(select 
							GROUP_CONCAT(DISTINCT concat(trn_podetails.intPONo,'/',trn_podetails.intPOYear))
							FROM
							trn_podetails
							INNER JOIN
							trn_prndetails
							ON
							trn_podetails.intPrnNo = trn_prndetails.intPrnNo AND
							trn_podetails.intPOYear   = trn_prndetails.intYear
							WHERE
							trn_prndetails.intOrderNo = trn_orderheader.intOrderNo AND
							trn_prndetails.intOrderYear = trn_orderheader.intOrderYear) as sup_po
							
							FROM
							trn_po_prn_details_history
							
							INNER JOIN
							trn_orderheader
							ON
							trn_po_prn_details_history.ORDER_NO = trn_orderheader.intOrderNo AND
							trn_po_prn_details_history.ORDER_YEAR = trn_orderheader.intOrderYear
							
							INNER JOIN
							mst_item
							ON
							mst_item.intId = trn_po_prn_details_history.ITEM
							
							WHERE
							
							trn_orderheader.intOrderNo = $orderNo AND
							trn_orderheader.intOrderYear
							= $orderYear 
							AND
							trn_po_prn_details_history.REVISION = $revision - 1";
 
							//echo $sql_select;
							$result			= $db->RunQuery($sql_select);
							return $result;
}

?>


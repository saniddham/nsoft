var basepath	= 'presentation/customerAndOperation/bulk/placeOrder/customer_orderwise_emails/';

$(document).ready(function(){	
	
	//if(){
	loadData1();
	//}

	$("#frmCustomerWiseEmail").validationEngine();
	$("#frmCustomerWiseEmail #cboCustomer").die('change').live('change',function()
	{
			loadOrderYear($(this).val(),this);
	});
	
	$("#frmCustomerWiseEmail #cboYear").die('change').live('change',function()
	{
			loadOrderNumber($(this).val(),this);
	});
	
	$('.clsEmail').bind('keypress', function(e) {
    //if(e.keyCode==13){
        // Enter pressed... do anything here...
		//alert("jdjdh")
   // }
});
	$('#cboOrdernumber').die('change').live('change',loadData);
	$('#butSave').die('click').live('click',saveData);
	$('#butNew').die('click').live('click',clearAll);
});

//-------------------------function------------------------------------

function loadOrderYear(customer,obj)
{
	var url 	= basepath+"customer_orderwise_emails-db-get.php";
	var data  	= "requestType=loadYear&customer="+customer;
	
	var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST', 
			data:data,
			async:false,
			success:function(json)
			{ 
				$('#frmCustomerWiseEmail #cboYear').html(json.response);
				
			}
			});
}

function loadOrderNumber(year,obj)
{
	
	var customer	=	$("#cboCustomer option:selected").val();
	var url 		= 	basepath+"customer_orderwise_emails-db-get.php";
	var data  		= 	"requestType=loadOrder&customer="+customer+"&year="+year;
	
	var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST', 
			data:data,
			async:false,
			success:function(json)
			{ 
				$('#frmCustomerWiseEmail #cboOrdernumber').html(json.response);
			}
			});
}

function loadData1()
{
	//alert('11');
	loadData();
}

function loadData()
{
	//alert('22');
	var customer 	= $("#cboCustomer option:selected").val();
	var year 		= $("#cboYear option:selected").val();
	var no 			= $("#cboOrdernumber option:selected").val();
	
	$("#tblMain tr:gt(0)").remove();
	
	if(no=='')
	{
		return;
	}
	var url 	= basepath+"customer_orderwise_emails-db-get.php?requestType=loadData";
	var httpobj = $.ajax({
		url:url,
		type:'POST',
		dataType:'json',
		data:"orderNo="+no+"&customer="+customer+"&year="+year,
		async:false,
		success:function(json){
			
			var lengthDetail   = json.arrDetailData.length;
			var arrDetailData  = json.arrDetailData;
			
			for(var i=0;i<lengthDetail;i++)
			{
				var order		= arrDetailData[i]['Order'];
				var TOemail		= arrDetailData[i]['TOemail'];	
				var CCemail		= arrDetailData[i]['CCemail'];
				var BCCemail	= arrDetailData[i]['BCCemail'];
				var	Type		= arrDetailData[i]['Type'];
				
				if(order!="")
				{
					if(Type==1)
					{
				var content='<tr class="normalfnt" id='+1+'><td class="type" width="4%">Marketing</td>';
				
				content+='<td align="center" ><textarea name="txtEmail" id="txtEmail" class="clsEmail validate[maxSize[250]]" cols="20" rows="2" onkeypress="myFunction(event)" >'+TOemail	+'</textarea></td>';
				content+='<td align="center" ><textarea name="txtCCEmail" id="txtCCEmail" class="clsCCEmail validate[maxSize[250]]" cols="20" rows="2" onkeypress="myFunction(event)">'+CCemail+'</textarea></td>';
				content+='<td align="center" ><textarea name="txtCCEmail" id="txtCCEmail" class="clsBCCEmail validate[maxSize[250]]" cols="20" rows="2" onkeypress="myFunction(event)">'+BCCemail+'</textarea></td></tr>';
				
				$('#frmCustomerWiseEmail #tblMain').append(content)
					}
					if(Type==2)
					{
				
				var content='<tr class="normalfnt" id='+2+'><td class="type" width="4%">Planning</td>';
				
				content+='<td align="center" ><textarea name="txtEmail" id="txtEmail" class="clsEmail validate[maxSize[250]]" cols="20" rows="2" onkeypress="myFunction(event)">'+TOemail	+'</textarea></td>';
				content+='<td align="center" ><textarea name="txtCCEmail" id="txtCCEmail" class="clsCCEmail validate[maxSize[250]]" cols="20" rows="2" onkeypress="myFunction(event)">'+CCemail+'</textarea></td>';
				content+='<td align="center" ><textarea name="txtCCEmail" id="txtCCEmail" class="clsBCCEmail validate[maxSize[250]]" cols="20" rows="2" onkeypress="myFunction(event)">'+BCCemail+'</textarea></td></tr>';
				
				$('#frmCustomerWiseEmail #tblMain').append(content)
					}
			}
			else
			{
				var content='<tr class="normalfnt" id='+1+'><td class="type" width="4%">Marketing</td>';
				
				content+='<td align="center" ><textarea name="txtEmail" id="txtEmail" class="clsEmail validate[maxSize[250]]" cols="20" rows="2" onkeypress="myFunction(event)"></textarea></td>';
				content+='<td align="center" ><textarea name="txtCCEmail" id="txtCCEmail" class="clsCCEmail validate[maxSize[250]]" cols="20" rows="2" onkeypress="myFunction(event)"></textarea></td>';
				content+='<td align="center" ><textarea name="txtCCEmail" id="txtCCEmail" class="clsBCCEmail validate[maxSize[250]]" cols="20" rows="2" onkeypress="myFunction(event)"></textarea></td></tr>';
				
				$('#frmCustomerWiseEmail #tblMain').append(content)
				
				var content='<tr class="normalfnt" id='+2+'><td class="type" width="4%">Planning</td>';
				
				content+='<td align="center" ><textarea name="txtEmail" id="txtEmail" class="clsEmail validate[maxSize[250]]" cols="20" rows="2" onkeypress="myFunction(event)"></textarea></td>';
				content+='<td align="center" ><textarea name="txtCCEmail" id="txtCCEmail" class="clsCCEmail validate[maxSize[250]]" cols="20" rows="2" onkeypress="myFunction(event)"></textarea></td>';
				content+='<td align="center" ><textarea name="txtCCEmail" id="txtCCEmail" class="clsBCCEmail validate[maxSize[250]]" cols="20" rows="2" onkeypress="myFunction(event)"></textarea></td></tr>';
				
				$('#frmCustomerWiseEmail #tblMain').append(content)
			}
			}
		}
	});
	
}

function saveData()
{
	var customer 	= $("#cboCustomer option:selected").val();
	var year 		= $("#cboYear option:selected").val();
	var order 		= $("#cboOrdernumber option:selected").val();
	var chkStatus = true;
	var value="[ ";
	$('.clsEmail').each(function(){

		toemail 	= $(this).val();
		ccEmail 	= $(this).parent().parent().find('.clsCCEmail').val();
		bccEmail 	= $(this).parent().parent().find('.clsBCCEmail').val();
		type 		= $(this).parent().parent().attr('id');
		
		value +='{"type":"'+type+'","toemail":'+URLEncode_json(toemail)+',"ccemail":'+URLEncode_json(ccEmail)+',"bccEmail":'+URLEncode_json(bccEmail)+'},' ;
		
	});
	
	value = value.substr(0,value.length-1);
	value += " ]";
	
	
	var url = basepath+"customer_orderwise_emails-db-set.php?requestType=saveData";
	$.ajax({
			url:url,
			type:'post',
			async:false,
			dataType:'json',
			data:"&year="+year+"&order="+order+"&emailDetails="+value,
	success:function(json){
			$('#butSave').validationEngine('showPrompt', json.msg,json.type);
			if(json.type=='pass')
			{
				var t=setTimeout("alertx()",3000);
				return;
			}
		},
		error:function(){
			
			$('#frmCustomerWiseEmail #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
			var t=setTimeout("alertx()",3000);	
			return;					
		}
	});
	
}

/*function myFunction(evt)
{
 var x =  evt.key;

 if( evt.key === ";")
  { 
   	alert("avoid semicolon in textbox")  
	//return false;		
   }	
	return false;
}*/

function myFunction(e) {
        var theEvent = e || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        var regex = /[^;]+$/;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault) {
                theEvent.preventDefault();
            }
        }
    }

function clearAll()
{
	$("#tblMain tr:gt(0)").remove();
	$('#cboCustomer').val('');
	$('#cboYear').html('<option></option>');
	$('#cboOrdernumber').html('<option></option>');
}

function alertx()
{
	$('#frmCustomerWiseEmail #butSave').validationEngine('hide')	;
}
<?php 
session_start();
//ini_set('display_errors',1);
$backwardseperator 	= "../../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 	 		= $_SESSION['userId'];
$requestType 		= $_REQUEST['requestType'];
//$emailDetails 		= json_decode($_REQUEST['emailDetails'], true);
$locationId	  		= $_SESSION["CompanyID"];

include "{$backwardseperator}dataAccess/Connector.php";

$sql = "SELECT 	mst_locations.intCompanyId
		FROM mst_locations WHERE mst_locations.intId='$locationId'";

$result 	 = $db->RunQuery($sql);
$row		 = mysqli_fetch_array($result);
$loginMainCompany 	 = $row['intCompanyId'];

if($requestType=='saveData')
{
	$year 				= $_REQUEST['year'];
	$order 				= $_REQUEST['order'];
	$emailDetailsq 		= json_decode($_REQUEST['emailDetails'], true);
	
	$rollBackFlag 	= 0;
	$saved			= 0;
	$save			= 0;

    $sql = "SELECT mst_locations.intCompanyId
            FROM trn_orderheader INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
            WHERE
            trn_orderheader.intOrderNo =  '$order' AND
            trn_orderheader.intOrderYear =  '$year'";

    $result 	 = $db->RunQuery($sql);
    $row		 = mysqli_fetch_array($result);
    $orderMainCompany 	 = $row['intCompanyId'];
    if ($orderMainCompany == $loginMainCompany){
        foreach($emailDetailsq as $arrDetails)
        {
            $toemails 			= $arrDetails['toemail'];
            $toemail_sort 		= explode(',', $toemails);
            sort($toemail_sort);
            $toemail			= implode(',', $toemail_sort);

            $ccemails	 		= $arrDetails['ccemail'];
            $ccemail_sort 		= explode(',', $ccemails);
            sort($ccemail_sort);
            $ccemail			= implode(',', $ccemail_sort);

            $bccEmails 			= $arrDetails['bccEmail'];
            $bccEmail_sort 		= explode(',', $bccEmails);
            sort($bccEmail_sort);
            $bccEmail			= implode(',', $bccEmail_sort);

            $type 				= $arrDetails['type'];

            $sql_select =
                "SELECT
				trn_orderheader_dispatch_emails.ORDER_NO,
				trn_orderheader_dispatch_emails.ORDER_YEAR,
				trn_orderheader_dispatch_emails.strBCC_EMAIL,
				trn_orderheader_dispatch_emails.strCC_EMAIL,
				trn_orderheader_dispatch_emails.strTO_EMAIL,
				trn_orderheader_dispatch_emails.TYPE
				FROM
				trn_orderheader_dispatch_emails
				WHERE
				trn_orderheader_dispatch_emails.ORDER_NO = $order AND
				trn_orderheader_dispatch_emails.ORDER_YEAR = $year AND
				trn_orderheader_dispatch_emails.TYPE = $type";

            $result_q 	= 	$db->RunQuery($sql_select);
            $row_q		=	mysqli_fetch_array($result_q);

            if($row_q['ORDER_NO']!="")
            {


                $sql_update = "UPDATE trn_orderheader_dispatch_emails 
					SET
					strTO_EMAIL = '$toemail' ,
					strCC_EMAIL = '$ccemail' , 
					strBCC_EMAIL = '$bccEmail'
					WHERE
					trn_orderheader_dispatch_emails.ORDER_NO = $order AND
					trn_orderheader_dispatch_emails.ORDER_YEAR = $year AND
					trn_orderheader_dispatch_emails.TYPE = $type ";
                //echo $sql_update;
                $result_update = $db->RunQuery($sql_update);
                if($result_update)
                {
                    $response['type'] 		= 'pass';
                    $response['msg'] 		= 'Updated successfully.';
                }
            }
            else
            {
                $sql_insert = "
				INSERT INTO 
				trn_orderheader_dispatch_emails
				(ORDER_NO,
				ORDER_YEAR,
				TYPE,
				strTO_EMAIL,
				strCC_EMAIL,
				strBCC_EMAIL
				)
				VALUES 
				($order,
				$year,
				$type,
				'$toemail',
				'$ccemail',
				'$bccEmail')";

                $result_insert = $db->RunQuery($sql_insert);
                if($result_insert)
                {
                    $response['type'] 		= 'pass';
                    $response['msg'] 		= 'saved successfully.';
                }
            }

        }//for
    } else {
        {
            $response['type'] 		= 'error';
            $response['msg'] 		= 'You cannot update POs from other company logins.';
        }
    }


	echo json_encode($response);
}
?>
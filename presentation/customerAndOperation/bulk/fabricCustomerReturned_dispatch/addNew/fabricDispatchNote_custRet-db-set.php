<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo 	 = $_REQUEST['serialNo'];
	$year 		 = $_REQUEST['Year'];
/*	$style  = $_REQUEST['style'];
	$graphicNo 	 = $_REQUEST['graphicNo'];
	$cusromerPO 	 = $_REQUEST['cusromerPO'];
*/	$custLocation 	 = $_REQUEST['custLocation'];
	$orderNo 	 = $_REQUEST['orderNo'];
	$orderYear 	 = $_REQUEST['orderYear'];
//	$AODNo 	 = $_REQUEST['AODNo'];
	$dispatchDate 	 = $_REQUEST['dispatchDate'];
	$remarks 	 = $_REQUEST['remarks'];
	//echo $_REQUEST['arr'];
	$arr 		= json_decode($_REQUEST['arr'], true);
	
	$programName='Customer Returned Dispatch';
	$programCode='P0650';

	$ApproveLevels = (int)getApproveLevel($programName);
	$maxStatus = $ApproveLevels+1;

//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		$savableFlag=1;
	    $rollBackFlag=0;
		
		if($serialNo==''){
			$serialNo 	= getNextSerialNo();
			$year = date('Y');
			$editMode=0;
			$savedStatus=$maxStatus;
			$savedLevels=$ApproveLevels;
		}
		else{
			$editMode=1;
			$savableFlag=getSaveStatus($serialNo,$year);//check wether already confirmed
			$sql = "SELECT
			ware_fabriccustreturndispatchheader.intStatus, 
			ware_fabriccustreturndispatchheader.intApproveLevels 
			FROM ware_fabriccustreturndispatchheader 
			WHERE
			ware_fabriccustreturndispatchheader.intBulkDispatchNo =  '$serialNo' AND
			ware_fabriccustreturndispatchheader.intBulkDispatchNoYear =  '$year'";

			$result = $db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			$savedStatus=$row['intStatus'];
			$savedLevels=$row['intApproveLevels'];
		}
		    $editPermission=loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
		//--------------------------
		$locationFlag=getLocationValidation('fabDispatch',$location,$serialNo,$year);
		//--------------------------
		
		 if(( $locationFlag==1) && ($editMode==1)){
			 $rollBackFlag=1;
			 $rollBackMsg="Invalid Edit Location";
		 }
		 else if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This Dispatch No is already confirmed.cant edit.";
		 }
		 else if($editPermission==0){
			 $rollBackFlag=1;
			 $rollBackMsg="No Permission to save.";
		 }
		else if($editMode==1){
		$sql = "UPDATE `ware_fabriccustreturndispatchheader` SET intStatus ='$maxStatus', 
												       intApproveLevels ='$ApproveLevels', 
													  intCustLocation ='$custLocation', 
													  intOrderNo ='$orderNo', 
													  intOrderYear ='$orderYear', 
													  strremarks ='$remarks', 
													  dtmdate ='$dispatchDate', 
													  intModifiedBy ='$userId', 
													  dtmModifiedDate =now() 
				WHERE (`intBulkDispatchNo`='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
		$result = $db->RunQuery2($sql);
		}
		else{
			//$sql = "DELETE FROM `ware_fabriccustreturndispatchheader` WHERE (`intBulkDispatchNo`='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
			//$result1 = $db->RunQuery2($sql);

			$sql = "INSERT INTO `ware_fabriccustreturndispatchheader` (`intBulkDispatchNo`,`intBulkDispatchNoYear`,intCustLocation,intOrderNo,intOrderYear,strRemarks,intStatus,intApproveLevels,dtmdate,dtmCreateDate,intCteatedBy,intCompanyId) 
					VALUES ('$serialNo','$year','$custLocation','$orderNo','$orderYear','$remarks','$maxStatus','$ApproveLevels','$dispatchDate',now(),'$userId','$location')";
			$result = $db->RunQuery2($sql);
		}
		//-----------delete and insert to detail table-----------------------
		if(($result) && ($rollBackFlag!=1)){
			$sql = "DELETE FROM `ware_fabriccustreturndispatchdetails` WHERE (`intBulkDispatchNo`='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
			$result2 = $db->RunQuery2($sql);
			$saved=0;
			$toSave=0;
			$rollBackMsg="Maximum Qty for followings...";
			foreach($arr as $arrVal)
			{
				$cutNo = $arrVal['cutNo'];
				$salesOrderId 	 = $arrVal['salesOrderId'];
				$salesOrderNo	 = $arrVal['salesOrderNo'];
				$partId 	 = $arrVal['partId'];
				$part 	 = $arrVal['part'];
				$bgColorId 		 = $arrVal['bgColorId'];
				$bgColor 	 = $arrVal['bgColor'];
				$line 	 = $arrVal['line'];
				$size 		 = $arrVal['size'];
				$sqty 		 = round($arrVal['sqty'],4);
				$gqty 		 = round($arrVal['gqty'],4);
				$eqty 		 = round($arrVal['eqty'],4);
				$pqty 		 = round($arrVal['pqty'],4);
				$fqty 		 = round($arrVal['fqty'],4);
				$remarks 		 = $arrVal['remarks'];
				$tot=val($sqty)+val($gsqty)+val($eqty)+val($pqty)+val($fqty);
				//$place = 'Stores';
				//$type 	 = 'Received';
			
			 	$soStatus		=loadSalesOrderStatus($orderNo,$orderYear,$salesOrderId);
				if($soStatus== -10){
					$rollBackFlag=1;
					$rollBackMsg ="Sales order is already closed";
				}
			
				$balQty=loadBalQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$partId,$size);
				$nonStkConfQty=0;
				$orderQty=loadOrderQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade);
				$maxQty=$balQty-$nonStkConfQty;

				//------check maximum FR Qty--------------------
				if($tot>$maxQty){
					$rollBackFlag=1;
					$rollBackMsg .="<br> ".$cutNo."-".$part."-".$salesOrderNo."-".$size." =".$maxQty;
				}

				//----------------------------
				if($rollBackFlag!=1){
					
					$sql = "INSERT INTO `ware_fabriccustreturndispatchdetails` (`intBulkDispatchNo`,`intBulkDispatchNoYear`,`strCutNo`,`intSalesOrderId`,`intPart`,`intGroundColor`,`strLineNo`,`strSize`,`dblSampleQty`,`dblGoodQty`,`dblEmbroideryQty`,`dblPDammageQty`,`dblFdammageQty`,`strRemarks`) 
					VALUES ('$serialNo','$year','$cutNo','$salesOrderId','$partId','$bgColorId','$line','$size','$sqty','$gqty','$eqty','$pqty','$fqty','$remarks')";
					$result = $db->RunQuery2($sql);
					if($result==1){
					$saved++;
					}
				}
				$toSave++;
		}
		}
		//echo $rollBackFlag;
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$confirmationMode=getConfirmationMode($programCode,$userId,$ApproveLevels,$maxStatus);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
//----------------------------------------
	echo json_encode($response);
//----------------------------------------




//----------------------------------------
	function getNextSerialNo()
	{
		global $db;
		global $location;
		$sql = "SELECT
				sys_no.intFabricCustReturnDispNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$location'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextNo = $row['intFabricCustReturnDispNo'];
		
		$sql = "UPDATE `sys_no` SET intFabricCustReturnDispNo=intFabricCustReturnDispNo+1 WHERE (`intCompanyId`='$location')  ";
		$db->RunQuery2($sql);
		
		return $nextNo;
	}
//-----------------------------------------------------------
function loadOrderQty($location,$orderNo,$orderYear,$salesOrderNo,$size,$grade)
{
		global $db;
		  $sql = "SELECT
				trn_ordersizeqty.dblQty
				FROM trn_ordersizeqty
				WHERE
				trn_ordersizeqty.intOrderNo =  '$orderNo' AND
				trn_ordersizeqty.intOrderYear =  '$orderYear' AND
				trn_ordersizeqty.intSalesOrderId =  '$salesOrderNo' AND
				trn_ordersizeqty.strSize =  '$size'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
	//--------------------------------------------------------------
	function loadBalQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$intPart,$size){
		global $db;
		    $sql = "SELECT
				sum(ware_fabriccustreturndetails.dblQty-IFNULL(ware_fabriccustreturndetails.dblDispatchedQty,0)) as qty , 
				(SELECT 
				sum(IFNULL(ware_fabriccustreturndispatchdetails.dblSampleQty,0)+IFNULL(ware_fabriccustreturndispatchdetails.dblGoodQty,0)+IFNULL(ware_fabriccustreturndispatchdetails.dblEmbroideryQty,0)+IFNULL(ware_fabriccustreturndispatchdetails.dblPDammageQty,0)+IFNULL(ware_fabriccustreturndispatchdetails.dblFdammageQty,0)) 
				FROM
				ware_fabriccustreturndispatchdetails
				Inner Join ware_fabriccustreturndispatchheader ON ware_fabriccustreturndispatchdetails.intBulkDispatchNo = ware_fabriccustreturndispatchheader.intBulkDispatchNo AND ware_fabriccustreturndispatchdetails.intBulkDispatchNoYear = ware_fabriccustreturndispatchheader.intBulkDispatchNoYear
				WHERE
				ware_fabriccustreturndispatchheader.intStatus =  '1' AND 
				ware_fabriccustreturndispatchheader.intOrderNo =  ware_fabriccustreturnheader.intOrderNo AND
				ware_fabriccustreturndispatchheader.intOrderYear =  ware_fabriccustreturnheader.intOrderYear AND
				ware_fabriccustreturndispatchdetails.strCutNo =  ware_fabriccustreturndetails.strCutNo AND
				ware_fabriccustreturndispatchdetails.intSalesOrderId =  ware_fabriccustreturndetails.intSalesOrderId AND
				ware_fabriccustreturndispatchdetails.strSize =  ware_fabriccustreturndetails.strSize AND
				ware_fabriccustreturndispatchdetails.intPart =  ware_fabriccustreturndetails.intPart ) as dispQty  
				
				FROM
				ware_fabriccustreturndetails
				Inner Join ware_fabriccustreturnheader ON ware_fabriccustreturndetails.intFabricCustReturnNo = ware_fabriccustreturnheader.intFabricCustReturnNo AND ware_fabriccustreturndetails.intFabricCustReturnYear = ware_fabriccustreturnheader.intFabricCustReturnYear
				WHERE
				ware_fabriccustreturnheader.intOrderNo =  '$orderNo' AND
				ware_fabriccustreturnheader.intOrderYear =  '$orderYear' AND
				ware_fabriccustreturndetails.strCutNo =  '$cutNo' AND
				ware_fabriccustreturndetails.intSalesOrderId =  '$salesOrderId' AND
				ware_fabriccustreturndetails.strSize =  '$size' AND
				ware_fabriccustreturndetails.intPart =  '$intPart'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['qty']-$rows['dispQty']);
	}
//-----------------------------------------------------------
function getConfirmationMode($programCode,$intUser,$approveLevels,$status){
		global $db;
		$k=$approveLevels+2-$status;
		$sqlp = "SELECT 
		menupermision.intEdit, 
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		if($rowp['int'.$k.'Approval']==1) 
		 $confirmatonMode=1; 
		else
		 $confirmatonMode=0; 

		return $confirmatonMode;
	}
	//--------------------------------------------------------------
	function loadNonStockConfQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$intPart,$size){
		global $db;
		   $sql = "SELECT
					sum(ware_fabriccustreturndispatchdetails.dblSampleQty+
					ware_fabriccustreturndispatchdetails.dblGoodQty+
					ware_fabriccustreturndispatchdetails.dblEmbroideryQty+
					ware_fabriccustreturndispatchdetails.dblPDammageQty+
					ware_fabriccustreturndispatchdetails.dblFdammageQty) as qty
					FROM
					ware_fabriccustreturndispatchheader
					Inner Join ware_fabriccustreturndispatchdetails ON ware_fabriccustreturndispatchheader.intBulkDispatchNo = ware_fabriccustreturndispatchdetails.intBulkDispatchNo AND ware_fabriccustreturndispatchheader.intBulkDispatchNoYear = ware_fabriccustreturndispatchdetails.intBulkDispatchNoYear
					WHERE
					ware_fabriccustreturndispatchheader.intOrderNo =  '$orderNo' AND
					ware_fabriccustreturndispatchheader.intOrderYear =  '$orderYear' AND
					ware_fabriccustreturndispatchdetails.intSalesOrderId =  '$salesOrderId' AND
					ware_fabriccustreturndispatchdetails.strCutNo =  '$cutNo' AND
					ware_fabriccustreturndispatchdetails.strSize =  '$size' AND
					ware_fabriccustreturndispatchdetails.intPart =  '$intPart' AND
					ware_fabriccustreturndispatchheader.intStatus >  '1' AND
					ware_fabriccustreturndispatchheader.intApproveLevels >= ware_fabriccustreturndispatchheader.intStatus AND
					ware_fabriccustreturndispatchheader.intCompanyId =  '$location'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
	//	echo val($rows['qty']);
		return val($rows['qty']);
	}
//-----------------------------------------------------------
	function getSaveStatus($serialNo,$year)
	{
		global $db;
		$sql = "SELECT ware_fabriccustreturndispatchheader.intStatus, ware_fabriccustreturndispatchheader.intApproveLevels FROM ware_fabriccustreturndispatchheader WHERE (`intBulkDispatchNo`='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			
		return $editableFlag;
	}
//-----------------------------------------------------------
	function getLocationValidation($type,$location,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
	 	$sql = "SELECT
					ware_fabriccustreturndispatchheader.intCompanyId
					FROM
					ware_fabriccustreturndispatchheader
					Inner Join mst_locations ON ware_fabriccustreturndispatchheader.intCompanyId = mst_locations.intId
					WHERE
					ware_fabriccustreturndispatchheader.intBulkDispatchNo =  '$serialNo' AND
					ware_fabriccustreturndispatchheader.intBulkDispatchNoYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			if($location == $row['intCompanyId'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function load loadEditMode----
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}

function loadSalesOrderStatus($orderNo,$orderYear,$salesOrderId){

	global $db;
	
	$sql 	= "SELECT
				trn_orderdetails.`STATUS`
				FROM `trn_orderdetails`
				WHERE
				trn_orderdetails.intOrderNo = '$orderNo' AND
				trn_orderdetails.intOrderYear = '$orderYear' AND
				trn_orderdetails.intSalesOrderId = '$salesOrderId'
				 ";
	
	$result = $db->RunQuery2($sql);
	$row	= mysqli_fetch_array($result);
	
	return $row['STATUS'];
	
}
//--------------------------------------------------------

?>

<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];

include $backwardseperator."dataAccess/Connector.php";

$orderNo  = $_REQUEST['orderNo'];
$orderYear  = $_REQUEST['orderYear'];
$serialNo  = $_REQUEST['serialNo'];
$year  = $_REQUEST['orderYear'];
$styleNo  = $_REQUEST['styleNo'];
$graphicNo  = $_REQUEST['graphicNo'];
$custPONo  = $_REQUEST['custPONo'];
$salesOrderNo  = $_REQUEST['salesOrderNo'];

$x_salesOrderNo='';
?>

<form id="frmFabricRecvNotePopup" name="frmFabricRecvNotePopup" method="post" action="">
<table width="500" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutD" style="width:720px">
		  <div class="trans_text">  Allocation</div>
		  <table width="650" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="600" border="0">
      <tr>
        <td><table width="650" border="0" cellpadding="0" cellspacing="0" class="tableBorder">
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Year</td>
            <td class="normalfnt">: </td>
            <td class="normalfnt"><div id="itemMain4"><?php echo $year  ;
 ?></div></td>
            <td align="right">&nbsp;</td>
            <td align="right"><div id="itemIdMain" style="display:none"><?php echo $itemId; ?></div></td>
            <td class="normalfnt">Order No</td>
            <td class="normalfnt">: </td>
            <td class="normalfnt"><div id="itemMain4"><?php echo $orderNo  ;
 ?></div></td>
            <td></td>
          </tr>
          <tr id="rw3">
            <td height="22" class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Customer PO</td>
            <td class="normalfnt">: </td>
            <td class="normalfnt"><div id="balToInvoice"><?php echo $custPONo;

 ?></div></td>
            <td class="normalfnt"></td>
            <td align="right">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt"><div id="itemMain3"></div></td>
            <td></td>
          </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Style No</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt"><select name="cboStyleP" style="width:120px" id="cboStyleP" class="searchP">
              <?php
					 $sql = "SELECT DISTINCT 
							trn_orderdetails.strStyleNo
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
							WHERE 
							trn_orderheader.intStatus = '1' AND 
							trn_orderdetails.intOrderNo='$orderNo' 
							AND trn_orderdetails.intOrderYear='$orderYear'
							ORDER BY 
							trn_orderdetails.strSalesOrderNo ASC";
			
					$html = "<option value=\"\"></option>";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{		
						 
							$html .= "<option value=\"".$row['strStyleNo']."\">".$row['strStyleNo']."</option>";
 					}
					echo $html;
				?>
            </select></td>
            <td class="normalfnt"></td>
            <td align="right">&nbsp;</td>
            <td class="normalfnt">Graphic</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt"><select name="cboGraphicP" style="width:120px" id="cboGraphicP" class="searchP">
              <?php
					 $sql = "SELECT DISTINCT 
							trn_orderdetails.strGraphicNo
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
							WHERE 
							trn_orderheader.intStatus = '1' AND 
							trn_orderdetails.intOrderNo='$orderNo' 
							AND trn_orderdetails.intOrderYear='$orderYear'
							ORDER BY 
							trn_orderdetails.strGraphicNo ASC";
			
					$html = "<option value=\"\"></option>";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{		
						 
							$html .= "<option value=\"".$row['strGraphicNo']."\">".$row['strGraphicNo']."</option>";
 					}
					echo $html;
				?>
            </select></td>
            <td></td>
          </tr>
          <tr id="rw2">
            <td height="22" class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Sales Order No</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt"><select name="cboSalesOrderNoP" style="width:120px" id="cboSalesOrderNoP" class="searchP">
              <?php
					 $sql = "SELECT DISTINCT 
							trn_orderdetails.strSalesOrderNo
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
							WHERE 
							trn_orderheader.intStatus = '1' AND 
							trn_orderdetails.intOrderNo='$orderNo' 
							AND trn_orderdetails.intOrderYear='$orderYear'
							ORDER BY 
							trn_orderdetails.strSalesOrderNo ASC";
			
					$html = "<option value=\"\"></option>";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{		
						if($row['strSalesOrderNo']==$salesOrderNo){
							$html .= "<option value=\"".$row['strSalesOrderNo']."\" selected=\"selected\">".$row['strSalesOrderNo']."</option>";
						}
						else{
							$html .= "<option value=\"".$row['strSalesOrderNo']."\">".$row['strSalesOrderNo']."</option>";
						}
					}
					echo $html;
				?>
            </select></td>
            <td class="normalfnt"></td>
            <td align="right">&nbsp;</td>
            <td class="normalfnt">Line No</td>
            <td class="normalfnt">&nbsp;</td>
            <td class="normalfnt"><div id="itemMain2">
              <select name="cboLineNo" style="width:120px" id="cboLineNo" class="">
                <?php
		$sql = "SELECT DISTINCT 
				trn_orderdetails.strLineNo
				FROM
				trn_orderdetails
				Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
				WHERE 
				trn_orderheader.intStatus = '1' AND 
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear'  ";
				if($salesOrderNo!=''){
				$sql .= " AND trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'";
				}
				$sql .= " 
				ORDER BY trn_orderdetails.strLineNo ASC"; 
				
					$html = "<option value=\"\"></option>";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{		
						$html .= "<option value=\"".$row['strLineNo']."\">".$row['strLineNo']."</option>";
					}
					echo $html;
				?>
              </select>
            </div></td>
            <td></td>
          </tr>  
            <tr id="rw1">
              <td width="3%" height="22" class="normalfnt">&nbsp;</td>
              <td width="15%" class="normalfnt">Part No</td>
              <td width="1%" class="normalfnt">: </td>
              <td width="27%" class="normalfnt"><select name="cboPartNo" style="width:120px" id="cboPartNo" class="">
                <?php
                    $sql = "SELECT DISTINCT
                            trn_orderdetails.intPart,
                            mst_part.strName
                            FROM
                            trn_orderdetails
                            Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
							WHERE
							trn_orderheader.intStatus =  '1' AND 
                            trn_orderdetails.intOrderNo =  '$orderNo' AND
                            trn_orderdetails.intOrderYear =  '$orderYear'   ";
							if($salesOrderNo!=''){
							$sql .= " AND trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'";
							}
							$sql .= " 
							ORDER BY mst_part.strName asc"; 
                             
                    $html = "<option value=\"\"></option>";
                    $result = $db->RunQuery($sql);
                    while($row=mysqli_fetch_array($result))
                    {
                            $html .= "<option value=\"".$row['intPart']."\">".$row['strName']."</option>";
                    }
					echo $html;
            ?>  
                </select></td>
              <td width="1%" class="normalfnt"></td>
              <td width="1%" align="right">&nbsp;</td>
              <td width="15%" class="normalfnt">Size</td>
              <td width="2%" class="normalfnt">&nbsp;</td>
              <td width="27%" class="normalfnt"><div id="itemMain">
                <select name="cboSizes" style="width:120px" id="cboSizes" class="">
                  <?php
				$sql = "SELECT DISTINCT
						trn_ordersizeqty.strSize
						FROM
						trn_orderdetails
						Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId 
						Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
						WHERE
						trn_orderheader.intStatus =  '1' AND 
						trn_orderdetails.intOrderNo =  '$orderNo' AND
						trn_orderdetails.intOrderYear =  '$orderYear'   ";
						if($salesOrderNo!=''){
						$sql .= " AND trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'";
						}
						$sql .= " 
						ORDER BY trn_ordersizeqty.strSize ASC"; 
				$html = "<option value=\"\"></option>";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
						$html .= "<option value=\"".$row['strSize']."\">".$row['strSize']."</option>";
				}
				echo $html;
			   ?>
                  </select>
                </div></td>
              <td width="8%"></td>
            </tr><tr id="rw1">
            <td width="3%" height="22" class="normalfnt">&nbsp;</td>
            <td width="15%" class="normalfnt">Cut No</td>
            <td width="1%" class="normalfnt">: </td>
            <td width="27%" class="normalfnt"><select name="cboCutNo" style="width:120px" id="cboCutNo" class="">
               <?php
				$sql = "SELECT DISTINCT   
						ware_fabriccustreturndetails.strCutNo
						FROM
						ware_fabriccustreturnheader
						Inner Join ware_fabriccustreturndetails ON ware_fabriccustreturnheader.intFabricCustReturnNo = ware_fabriccustreturndetails.intFabricCustReturnNo AND ware_fabriccustreturnheader.intFabricCustReturnYear = ware_fabriccustreturndetails.intFabricCustReturnYear 
						Inner Join trn_orderdetails ON ware_fabriccustreturnheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabriccustreturnheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_fabriccustreturndetails.intSalesOrderId = trn_orderdetails.intSalesOrderId AND ware_fabriccustreturndetails.strLineNo = trn_orderdetails.strLineNo AND ware_fabriccustreturndetails.intPart = trn_orderdetails.intPart
						WHERE
						ware_fabriccustreturnheader.intOrderNo =  '$orderNo' AND
						ware_fabriccustreturnheader.intOrderYear =  '$orderYear'   ";
						if($salesOrderNo!=''){
						$sql .= " AND trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'";
						}
						$sql .= " 
						ORDER BY ware_fabriccustreturndetails.strCutNo ASC"; 
				$html = "<option value=\"\"></option>";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
						$html .= "<option value=\"".$row['strCutNo']."\">".$row['strCutNo']."</option>";
				}
				echo $html;
			   ?>
            </select></td>
            <td width="1%" class="normalfnt"></td>
            <td width="1%" align="right">&nbsp;</td>
            <td width="15%" class="normalfnt">Color</td>
            <td width="2%" class="normalfntRight"></td>
            <td width="27%" class="normalfntLeft"><select name="cboColor" style="width:120px" id="cboColor" class="">
              <?php
		$sql="SELECT DISTINCT  
			trn_sampleinfomations_details.intGroundColor,
			mst_colors_ground.strName as bgColor 
			FROM
			trn_orderdetails
			Inner Join trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
			Inner Join trn_sampleinfomations_details ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_details.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_details.intRevNo AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
			left Join mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
			WHERE
			trn_orderdetails.intOrderNo =  '$orderNo' AND
			trn_orderdetails.intOrderYear =  '$orderYear'"; 
			if($salesOrderNo!=''){
			$sql .= " AND 
					trn_orderdetails.strSalesOrderNo='$salesOrderNo'"; 
			}
			$sql .= " ORDER BY 
					mst_colors_ground.strName ASC ";
				$html = "<option value=\"\"></option>";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
						$html .= "<option value=\"".$row['intGroundColor']."\">".$row['bgColor']."</option>";
				}
				echo $html;
			   ?>
            </select></td>
            <td><span class="normalfntLeft"><img src="images/smallSearch.png" width="24" height="24" class="mouseover" id="imgSearchItems" /></span></td>
            </tr>          </table></td>
      </tr>
      <?php //echo $pp
	  ?>
      <tr>
        <td><div style="width:700px;height:300px;overflow:scroll" >
          <table width="679" class="grid" id="tblPopup" >
                        <tr class="gridHeader">
              <td width="4%" ><input type="checkbox" name="chkAll" id="chkAll" /></td>
              <td width="7%" >Cut No</td>
              <td width="16%" >Sales Order No</td>
              <td width="9%" >Part</td>
              <td width="21%" >Back Ground Color</td>
              <td width="12%">Line No</td>
              <td width="9%">Size</td>
              <td width="13%">Qty</td>
              <td width="13%"> Bal Disp Qty</td>
              </tr>

            </table>
          </div></td>
      </tr>
        <td align="center" class="tableBorder_allRound"><img src="images/Tadd.jpg" width="92" height="24"  alt="add" id="butAdd" name="butAdd" class="mouseover"/><img src="images/delete.png" width="92" height="24"  alt="add" id="butDelete" name="butDelete" class="mouseover" style="display:none"/><img src="images/Tclose.jpg" width="92" height="24" id="butClose1" name="butClose1" class="mouseover"  /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
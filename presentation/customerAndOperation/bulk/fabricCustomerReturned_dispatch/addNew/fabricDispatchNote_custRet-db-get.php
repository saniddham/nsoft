<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once "{$backwardseperator}class/customerAndOperation/cls_textile_stores.php";
	
	$programName='Customer Returned Dispatch';
	$programCode='P0650';
	
	/////////// type of print load part /////////////////////
if($requestType=='loadGraphicNo')
	{
		$styleNo  = $_REQUEST['styleNo'];
		$sql = "SELECT DISTINCT
				trn_orderdetails.strGraphicNo
				FROM
				trn_orderdetails
				Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
				WHERE
				trn_orderheader.intStatus =  '1' AND
				trn_orderdetails.strStyleNo =  '$styleNo' 
				ORDER BY trn_orderdetails.strGraphicNo ASC";
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['strGraphicNo']."\">".$row['strGraphicNo']."</option>";
		}
				$response['graphicNo'] = $html;
		
		echo json_encode($response);
	}
	//------------------------------
if($requestType=='loadCustomerPONo')
	{
		$orderYear  = $_REQUEST['orderYear'];
		$styleNo  = $_REQUEST['styleNo'];
		$graphicNo  = $_REQUEST['graphicNo'];
		$sql = "SELECT DISTINCT 
				trn_orderheader.strCustomerPoNo
				FROM
				trn_sampleinfomations
				Inner Join trn_orderdetails ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_orderdetails.intRevisionNo AND trn_sampleinfomations.strGraphicRefNo = trn_orderdetails.strGraphicNo
				Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
				WHERE 
				trn_orderheader.intStatus = '1' AND 
				trn_orderdetails.strStyleNo =  '$styleNo'  ";
				if($graphicNo!=''){
				$sql .= "AND
				trn_orderdetails.strGraphicNo =  '$graphicNo'  ";
				}
				
				$sql .= "AND 
				trn_orderheader.strCustomerPoNo <> '' 
				ORDER BY trn_orderheader.strCustomerPoNo ASC 
				";
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['strCustomerPoNo']."\">".$row['strCustomerPoNo']."</option>";
		}
				$response['customerPoNo'] = $html;
		echo json_encode($response);
	}
	//------------------------------
	else if($requestType=='loadPoNo')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderYear  = $_REQUEST['orderYear'];
		
		$sql = "SELECT DISTINCT 
				trn_orderheader.strCustomerPoNo,
				trn_orderheader.intCustomer 
				FROM trn_orderheader 
				WHERE
				trn_orderheader.intStatus =  '1' AND
				trn_orderheader.intOrderNo='$orderNo' 
				AND trn_orderheader.intOrderYear='$orderYear' 
				AND trn_orderheader.strCustomerPoNo!=''  
				ORDER BY 
				trn_orderheader.strCustomerPoNo ASC";

		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$response['poNo'] = $row['strCustomerPoNo'];
				$response['customer'] = $row['intCustomer'];
		}
		if($orderNoArray[0]==''){
				$response['poNo'] = '';
				$response['customer'] = '';
	
		//---order no
			 $sql = "SELECT DISTINCT 
					trn_orderheader.intOrderNo,
					trn_orderheader.intCustomer,
					trn_orderheader.intOrderYear
					FROM trn_orderheader
					WHERE
					trn_orderheader.intStatus =  '1' AND
					trn_orderheader.intOrderYear='$orderYear'
					ORDER BY
				    trn_orderheader.intOrderNo DESC"; 
					
			$html = "<option value=\"\"></option>";
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				$html .= "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
			}
				$response['orderNo'] = $html;
					
		}
		//---sales order no
		 $sql = "SELECT DISTINCT 
				trn_orderdetails.strSalesOrderNo
				FROM
				trn_orderdetails
				Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
				WHERE
				trn_orderheader.intStatus =  '1' AND
				trn_orderdetails.intOrderNo='$orderNo' 
				AND trn_orderdetails.intOrderYear='$orderYear'
				ORDER BY 
				trn_orderdetails.strSalesOrderNo ASC";

		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['strSalesOrderNo']."\">".$row['strSalesOrderNo']."</option>";
		}
				$response['salesOrderNo'] = $html;
		
		
		
		echo json_encode($response);
	}
	//------------------------------
else if($requestType=='loadOrderNo')
	{
		$orderYear  = $_REQUEST['orderYear'];
		$poNo  = $_REQUEST['poNo'];
		$customer  = $_REQUEST['customer'];
		
		$sql = "SELECT DISTINCT 
				trn_orderheader.intOrderNo,
				trn_orderheader.intCustomer,
				trn_orderheader.intOrderYear
				FROM trn_orderheader
				WHERE
				trn_orderheader.intOrderYear='$orderYear' AND 
				trn_orderheader.intStatus='1' ";
				if($poNo!=''){
		$sql .= " AND trn_orderheader.strCustomerPoNo='$poNo'";			
				}
				if($customer!=''){
		$sql .= " AND trn_orderheader.intCustomer='$customer'";			
				}
		$sql .= " ORDER BY 
				 trn_orderheader.intOrderYear DESC, 
				 trn_orderheader.intOrderNo DESC"; 
				 
				
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
			$customer=$row['intCustomer'];
		}
			$response['orderNo'] = $html;
			if($poNo==''){
				$response['customer'] = '';
			}
			else{
				$response['customer'] = $customer;
			}
		if($poNo==''){
			//	$response['orderNo'] = '';
				$response['customer'] = '';
				
			 $sql = "SELECT DISTINCT 
					trn_orderheader.strCustomerPoNo 
					FROM trn_orderheader
					WHERE
					trn_orderheader.intStatus =  '1' AND
					trn_orderheader.intOrderYear='$orderYear' 
					ORDER BY 
					trn_orderheader.strCustomerPoNo ASC";
			$html = "<option value=\"\"></option>";
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				$html .= "<option value=\"".$row['strCustomerPoNo']."\">".$row['strCustomerPoNo']."</option>";
			}
				$response['PoNo'] = $html;
		}
		
		echo json_encode($response);
	}
	//----------------------------------
	else if($requestType=='loadOrderNosToSalesOrderNos')
		{
		$year				= $_REQUEST['year'];
		$graphicNo			= $_REQUEST['graphicNo'];
		$styleId			= $_REQUEST['styleId'];
		$customerPONo		= $_REQUEST['customerPONo'];
		$salesOrderNo		= $_REQUEST['salesOrderNo'];
		$customerId			= $_REQUEST['customerId'];
		
			$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
		if($graphicNo!='')
			$para.=" AND trn_orderdetails.strGraphicNo =  '$graphicNo'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId'  ";
		if($customerPONo!='')
			$para.=" AND trn_orderheader.strCustomerPoNo =  '$customerPONo' ";
		if($salesOrderNo!='')
			$para.=" AND trn_orderdetails.strSalesOrderNo 	=  '$salesOrderNo'  ";	
		$sql = "SELECT DISTINCT
					trn_orderheader.intOrderNo 
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
					trn_orderheader.intStatus=1
					$para
					
				ORDER BY intOrderNo DESC
				";
				
		$result = $db->RunQuery($sql);
		$html ='';
		if($orderNo=='')
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option ".($customerId==$row['intOrderNo']?'selected':'')." value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
		}
			$response['orderNo'] = $html;
		
		echo json_encode($response);
	}
	
	//----------------------------------
	else if($requestType=='loadPONoAndOrderNo')
	{
		$styleNo  = $_REQUEST['styleNo'];
		$graphicNo  = $_REQUEST['graphicNo'];
		$poNo  = $_REQUEST['poNo'];
		$customer  = $_REQUEST['customer'];
		$orderNo  = $_REQUEST['orderNo'];
		$orderYear  = $_REQUEST['orderYear'];
		
		$sql = "SELECT DISTINCT
				trn_orderheader.strCustomerPoNo 
				FROM trn_orderheader  
				WHERE  
				trn_orderheader.intStatus =  '1' AND
				trn_orderheader.intOrderYear='$orderYear' 
				AND trn_orderheader.strCustomerPoNo <> ''"; 
				
		if($customer!=''){
		$sql .= " AND  
				trn_orderheader.intCustomer='$customer'"; 
		}
		if($styleNo!=''){
		$sql .= " AND  
				trn_orderdetails.strStyleNo='$styleNo'";
		}
		if($graphicNo!=''){
		$sql .= " AND  
				trn_orderdetails.strGraphicNo='$graphicNo'"; 
		}
		$sql .= " ORDER BY 
				trn_orderheader.strCustomerPoNo ASC";
			//	echo $sql;
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['strCustomerPoNo']."\">".$row['strCustomerPoNo']."</option>";
		}
				$response['poNo'] = $html;
				
		$sql = "SELECT DISTINCT 
				trn_orderheader.intOrderNo,
				trn_orderheader.intOrderYear
				FROM trn_orderheader  
				Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE  
				trn_orderheader.intStatus =  '1' AND
				trn_orderheader.intOrderYear='$orderYear'"; 
		if($customer!=''){
		$sql .= " AND
				trn_orderheader.intCustomer='$customer'"; 
		}
		if($styleNo!=''){
		$sql .= " AND  
				trn_orderdetails.strStyleNo='$styleNo'";
		}
		if($graphicNo!=''){
		$sql .= " AND  
				trn_orderdetails.strGraphicNo='$graphicNo'"; 
		}
		$sql .= " ORDER BY   
				trn_orderheader.intOrderYear DESC, 
				trn_orderheader.intOrderNo DESC";
				//echo $sql;
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
		}
				$response['orderNo'] = $html;
		
		
		echo json_encode($response);
	}

	//----------------------------------
	else if($requestType=='loadSalesLineNoPartSizesComboes')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderYear  = $_REQUEST['orderYear'];
		$styleNo  = $_REQUEST['styleNo'];
		$salesOrderNo  = $_REQUEST['salesOrderNo'];
		//-----------------		
		$sql = "SELECT DISTINCT 
				trn_orderdetails.strGraphicNo as graphicNo
				FROM
				trn_orderdetails 
				Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' 
				AND trn_orderheader.intStatus='1' "; 
		if($styleNo!=''){
		$sql .= " AND 
				trn_orderdetails.strStyleNo =  '$styleNo'"; 
		}
		if($salesOrderNo!=''){
		$sql .= " AND 
				trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'"; 
		}
		$sql .= " Order by trn_orderdetails.strGraphicNo ASC";
				
		$html1 = "<option value=\"\"></option>";
		$html = "";
		$result = $db->RunQuery($sql);
		$i=0;
		while($row=mysqli_fetch_array($result))
		{
				$i++;
				$html .= "<option value=\"".$row['graphicNo']."\">".$row['graphicNo']."</option>";
		}
		if($i==1){
			$html=$html.$html1;
		}
		else{
			$html=$html1.$html;
		}
				$response['graphicNo'] = $html;
 		//-----------------		
		$sql = "SELECT DISTINCT 
				trn_orderdetails.strLineNo
				FROM
				trn_orderdetails
				Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
				WHERE
				trn_orderheader.intStatus =  '1' AND
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear'"; 
		if($styleNo!=''){
		$sql .= " AND 
				trn_orderdetails.strStyleNo =  '$styleNo'"; 
		}
		if($salesOrderNo!=''){
		$sql .= " AND 
				trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'"; 
		}
		$sql .= " ORDER BY trn_orderdetails.strLineNo ASC";		
		
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['strLineNo']."\">".$row['strLineNo']."</option>";
		}
				$response['lineNo'] = $html;
 		//-----------------		
		$sql = "SELECT DISTINCT 
				trn_orderdetails.strSalesOrderNo as salesOrderNo
				FROM
				trn_orderdetails 
				Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' 
				AND trn_orderheader.intStatus='1' "; 
		if($styleNo!=''){
		$sql .= " AND 
				trn_orderdetails.strStyleNo =  '$styleNo'"; 
		}
		$sql .= " Order by trn_orderdetails.strSalesOrderNo ASC";
				
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['salesOrderNo']."\">".$row['salesOrderNo']."</option>";
		}
				$response['salesOrderNo'] = $html;
		//-----------------		
		$sql = "SELECT DISTINCT
				trn_orderdetails.intPart,
				mst_part.strName
				FROM
				trn_orderdetails
				left Join mst_part ON trn_orderdetails.intPart = mst_part.intId
				Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
				WHERE
				trn_orderheader.intStatus =  '1' AND
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear'"; 
		if($styleNo!=''){
		$sql .= " AND 
				trn_orderdetails.strStyleNo =  '$styleNo'"; 
		}
		if($salesOrderNo!=''){
		$sql .= " AND 
				trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'"; 
		}
		$sql .= " ORDER BY mst_part.strName ASC"; 
				 
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['intPart']."\">".$row['strName']."</option>";
		}
				$response['partNo'] = $html;
		//-----------------		
	    $sql = "SELECT DISTINCT
				trn_ordersizeqty.strSize
				FROM
				trn_orderdetails
				Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId 
				Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
				WHERE
				trn_orderheader.intStatus =  '1' AND
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear'"; 
		if($styleNo!=''){
		$sql .= " AND 
				trn_orderdetails.strStyleNo =  '$styleNo'"; 
		}
		if($salesOrderNo!=''){
		$sql .= " AND 
				trn_orderdetails.strSalesOrderNo =  '$salesOrderNo'"; 
		}
		$sql .= " ORDER BY trn_ordersizeqty.strSize ASC"; 
		
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['strSize']."\">".$row['strSize']."</option>";
		}
				$response['sizes'] = $html;
		//-----------------		
		$sql="SELECT DISTINCT  
			trn_sampleinfomations_details.intGroundColor,
			mst_colors_ground.strName as bgColor 
			FROM
			trn_orderdetails
			Inner Join trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
			Inner Join trn_sampleinfomations_details ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_details.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_details.intRevNo AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
			left Join mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
			WHERE
			trn_orderdetails.intOrderNo =  '$orderNo' AND
			trn_orderdetails.intOrderYear =  '$orderYear'"; 
			if($styleNo!=''){
			$sql .= " AND 
					trn_orderdetails.strStyleNo =  '$styleNo'"; 
			}
			if($salesOrderNo!=''){
			$sql .= " AND 
					trn_orderdetails.strSalesOrderNo='$salesOrderNo'"; 
			}
			$sql .= " ORDER BY 
					mst_colors_ground.strName ASC ";
		
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$html .= "<option value=\"".$row['intGroundColor']."\">".$row['bgColor']."</option>";
		}
				$response['color'] = $html;
		//-----------------		
		echo json_encode($response);
	}
//-----------------------------------
	else if($requestType=='loadQty')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderYear  = $_REQUEST['orderYear'];
		$salesOrderNo  = $_REQUEST['salesOrderNo'];
		
		$sql = "SELECT
				Sum(trn_orderdetails.intQty) AS qty
				FROM trn_orderdetails 
				WHERE
				trn_orderdetails.intOrderNo='$orderNo' 
				AND trn_orderdetails.intOrderYear='$orderYear'  
"; 
		if($salesOrderNo!=''){
		$sql .= " AND 
				trn_orderdetails.strSalesOrderNo='$salesOrderNo'"; 
		}
		$sql .= " GROUP BY
				trn_orderdetails.intOrderNo,
				trn_orderdetails.intOrderYear"; 
		
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$response['qty'] = $row['qty'];
		
		echo json_encode($response);
	}
//--------------------------------------------------------------------
	else if($requestType=='loadPartDetails')
	{
		$orderYear  = $_REQUEST['orderYear'];
		$orderNo  = $_REQUEST['orderNo'];
		$styleNo  = $_REQUEST['styleNo'];
		$salesOrderNo  = $_REQUEST['salesOrderId'];
		$lineNo  = $_REQUEST['lineNo'];
		$part  = $_REQUEST['part'];
		$cutNo  = $_REQUEST['cutNo'];
		$size  = $_REQUEST['size'];
		$color  = $_REQUEST['color'];
		
		$sql="SELECT DISTINCT  
			ware_fabriccustreturndetails.strCutNo, 
			ware_fabriccustreturndetails.intSalesOrderId,
			trn_orderdetails.strSalesOrderNo,
			ware_fabriccustreturndetails.intPart,
			mst_part.strName as part,
			ware_fabriccustreturndetails.intGroundColor,
			mst_colors_ground.strName as bgColor,
			ware_fabriccustreturndetails.strLineNo,
			ware_fabriccustreturndetails.strSize,
			ware_fabriccustreturndetails.dblQty 
			FROM
			ware_fabriccustreturnheader
			Inner Join trn_orderdetails ON ware_fabriccustreturnheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabriccustreturnheader.intOrderYear = trn_orderdetails.intOrderYear 
			Inner Join ware_fabriccustreturndetails ON ware_fabriccustreturnheader.intFabricCustReturnNo = ware_fabriccustreturndetails.intFabricCustReturnNo AND ware_fabriccustreturnheader.intFabricCustReturnYear = ware_fabriccustreturndetails.intFabricCustReturnYear AND ware_fabriccustreturndetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
			Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId
			Inner Join mst_colors_ground ON ware_fabriccustreturndetails.intGroundColor = mst_colors_ground.intId
			WHERE
			ware_fabriccustreturnheader.intOrderNo =  '$orderNo' AND
			ware_fabriccustreturnheader.intOrderYear =  '$orderYear' AND 
			ware_fabriccustreturnheader.intStatus='1' 
			"; 
			if($styleNo!=''){
			$sql .= " AND 
					trn_orderdetails.strStyleNo='$styleNo'"; 
			}
			if($salesOrderNo!=''){
			$sql .= " AND 
					trn_orderdetails.strSalesOrderNo='$salesOrderNo'"; 
			}
			if($lineNo!=''){
			$sql .= " AND 
					ware_fabriccustreturndetails.strLineNo='$lineNo'"; 
			}
			if($part!=''){
			$sql .= " AND 
					ware_fabriccustreturndetails.intPart='$part'"; 
			}
			if($cutNo!=''){
			$sql .= " AND 
					ware_fabriccustreturndetails.strCutNo='$cutNo'"; 
			}
			if($size!=''){
			$sql .= " AND 
					ware_fabriccustreturndetails.strSize='$size'"; 
			}
			if($color!=''){
			$sql .= " AND 
					ware_fabriccustreturndetails.intGroundColor='$color'"; 
			}
			
			$sql .= " GROUP BY 
						ware_fabriccustreturnheader.intOrderNo , 
						ware_fabriccustreturnheader.intOrderYear , 
						trn_orderdetails.strSalesOrderNo , 
						ware_fabriccustreturndetails.strLineNo , 
						ware_fabriccustreturndetails.intPart , 
						ware_fabriccustreturndetails.strCutNo , 
						ware_fabriccustreturndetails.intSalesOrderId,
						ware_fabriccustreturndetails.strSize  
			";
			
			$sql .= " ORDER BY   
			ware_fabriccustreturndetails.strCutNo ASC,
					trn_orderdetails.strSalesOrderNo ASC, 
					mst_part.strName ASC, 
					mst_colors_ground.strName ASC, 
					ware_fabriccustreturndetails.strLineNo ASC,
					ware_fabriccustreturndetails.strSize ASC ";
		//	echo $sql;
			
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$data['cutNo'] 	= $row['strCutNo'];
			$data['salesOrderId'] 	= $row['intSalesOrderId'];
			$data['salesOrderNo'] 	= $row['strSalesOrderNo'];
			$data['partId'] 	= $row['intPart'];
			$data['part'] 	= $row['part'];
			$data['groundColorId'] = $row['intGroundColor'];
			$data['groundColor'] = $row['bgColor'];
			$data['lineNo'] 	= $row['strLineNo'];
			$data['size'] = $row['strSize'];
			//$data['qty'] = $row['dblQty'];
			$data['qty'] = round(loadBalQty($location,$orderNo,$orderYear,$row['strCutNo'],$row['intSalesOrderId'],$row['intPart'],$row['strSize']));
			$data['nonStkConfQty'] = 0;
			$arrCombo[] = $data;
		}
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);
	}
	else if($requestType=='loadAllComboDetails')
	{
		$year				= $_REQUEST['year'];
		$graphicNo			= $_REQUEST['graphicNo'];
		$styleId			= $_REQUEST['styleId'];
		$customerPONo		= $_REQUEST['customerPONo'];
		$orderNo			= $_REQUEST['orderNo'];
		$customerId			= $_REQUEST['customerId'];
		$locationFlag		= 0;
		$companyFlag		= 1;
		echo loadAllComboDetails($year,$graphicNo,$styleId,$customerPONo,$orderNo,$customerId,$locationFlag,$location,$companyFlag,$company);	
	}
	//--------------------------------------------------------------
	function loadBalQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$intPart,$size){
		global $db;
		    $sql = "SELECT
				sum(ware_fabriccustreturndetails.dblQty-IFNULL(ware_fabriccustreturndetails.dblDispatchedQty,0)) as qty , 
				(SELECT 
				sum(IFNULL(ware_fabriccustreturndispatchdetails.dblSampleQty,0)+IFNULL(ware_fabriccustreturndispatchdetails.dblGoodQty,0)+IFNULL(ware_fabriccustreturndispatchdetails.dblEmbroideryQty,0)+IFNULL(ware_fabriccustreturndispatchdetails.dblPDammageQty,0)+IFNULL(ware_fabriccustreturndispatchdetails.dblFdammageQty,0)) 
				FROM
				ware_fabriccustreturndispatchdetails
				Inner Join ware_fabriccustreturndispatchheader ON ware_fabriccustreturndispatchdetails.intBulkDispatchNo = ware_fabriccustreturndispatchheader.intBulkDispatchNo AND ware_fabriccustreturndispatchdetails.intBulkDispatchNoYear = ware_fabriccustreturndispatchheader.intBulkDispatchNoYear
				WHERE
				ware_fabriccustreturndispatchheader.intStatus =  '1' AND 
				ware_fabriccustreturndispatchheader.intOrderNo =  ware_fabriccustreturnheader.intOrderNo AND
				ware_fabriccustreturndispatchheader.intOrderYear =  ware_fabriccustreturnheader.intOrderYear AND
				ware_fabriccustreturndispatchdetails.strCutNo =  ware_fabriccustreturndetails.strCutNo AND
				ware_fabriccustreturndispatchdetails.intSalesOrderId =  ware_fabriccustreturndetails.intSalesOrderId AND
				ware_fabriccustreturndispatchdetails.strSize =  ware_fabriccustreturndetails.strSize AND
				ware_fabriccustreturndispatchdetails.intPart =  ware_fabriccustreturndetails.intPart ) as dispQty  
				
				FROM
				ware_fabriccustreturndetails
				Inner Join ware_fabriccustreturnheader ON ware_fabriccustreturndetails.intFabricCustReturnNo = ware_fabriccustreturnheader.intFabricCustReturnNo AND ware_fabriccustreturndetails.intFabricCustReturnYear = ware_fabriccustreturnheader.intFabricCustReturnYear
				WHERE 
				ware_fabriccustreturnheader.intStatus = 1 AND 
				ware_fabriccustreturnheader.intOrderNo =  '$orderNo' AND
				ware_fabriccustreturnheader.intOrderYear =  '$orderYear' AND
				ware_fabriccustreturndetails.strCutNo =  '$cutNo' AND
				ware_fabriccustreturndetails.intSalesOrderId =  '$salesOrderId' AND
				ware_fabriccustreturndetails.strSize =  '$size' AND
				ware_fabriccustreturndetails.intPart =  '$intPart'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['qty']-$rows['dispQty']);
	}
//------------------------------------------------------------------------------------------------
function loadAllComboDetails($year,$graphicNo,$styleId,$customerPONo,$orderNo,$customerId,$locationFlag,$location,$companyFlag,$company) 	
{
	$obj = new cls_texttile();
	echo $obj->loadAllSearchComboDetails($year,$graphicNo,$styleId,$customerPONo,$orderNo,$customerId,$locationFlag,$location,$companyFlag,$company);	
}
	//--------------------------------------------------------------
function loadNonStockConfQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$intPart,$size){
	global $db;
	   $sql = "SELECT
				sum(ware_fabriccustreturndispatchdetails.dblSampleQty+
				ware_fabriccustreturndispatchdetails.dblGoodQty+
				ware_fabriccustreturndispatchdetails.dblEmbroideryQty+
				ware_fabriccustreturndispatchdetails.dblPDammageQty+
				ware_fabriccustreturndispatchdetails.dblFdammageQty) as qty
				FROM
				ware_fabriccustreturndispatchheader
				Inner Join ware_fabriccustreturndispatchdetails ON ware_fabriccustreturndispatchheader.intBulkDispatchNo = ware_fabriccustreturndispatchdetails.intBulkDispatchNo AND ware_fabriccustreturndispatchheader.intBulkDispatchNoYear = ware_fabriccustreturndispatchdetails.intBulkDispatchNoYear
				WHERE
				ware_fabriccustreturndispatchheader.intOrderNo =  '$orderNo' AND
				ware_fabriccustreturndispatchheader.intOrderYear =  '$orderYear' AND
				ware_fabriccustreturndispatchdetails.intSalesOrderId =  '$salesOrderId' AND
				ware_fabriccustreturndispatchdetails.strCutNo =  '$cutNo' AND
				ware_fabriccustreturndispatchdetails.strSize =  '$size' AND
				ware_fabriccustreturndispatchdetails.intPart =  '$intPart' AND
				ware_fabriccustreturndispatchheader.intStatus >  '1' AND
				ware_fabriccustreturndispatchheader.intApproveLevels >= ware_fabriccustreturndispatchheader.intStatus AND
				ware_fabriccustreturndispatchheader.intCompanyId =  '$location'";

	$result = $db->RunQuery($sql);
	$rows = mysqli_fetch_array($result);
//	echo val($rows['qty']);
	return val($rows['qty']);
}
//-----------------------------------------------------------
?>
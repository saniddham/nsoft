<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
	session_start();
	$backwardseperator = "../../../../../";
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";

	$programName='Dispatch Note';
	$programCode='P0470';
	$fabRcvApproveLevel = (int)getApproveLevel($programName);
	

 if($requestType=='getValidation')
	{
		$serialNo  = $_REQUEST['serialNo'];
		$serialNoArray=explode("/",$serialNo);
		
		$sql = "SELECT 
				ware_fabriccustreturndispatchheader.intOrderNo,
				ware_fabriccustreturndispatchheader.intStatus,
				ware_fabriccustreturndispatchheader.intApproveLevels,
				ware_fabriccustreturndispatchheader.intOrderYear, 
				trn_orderdetails.strSalesOrderNo,
				trn_orderdetails.intSalesOrderId, 
				ware_fabriccustreturndispatchdetails.strCutNo,
				ware_fabriccustreturndispatchdetails.intPart, 
				mst_part.strName as part,
				ware_fabriccustreturndispatchdetails.intGroundColor, 
				mst_colors_ground.strName as bgcolor,
				ware_fabriccustreturndispatchdetails.strLineNo,
				ware_fabriccustreturndispatchdetails.strSize,
				ware_fabriccustreturndispatchdetails.dblSampleQty,
				ware_fabriccustreturndispatchdetails.dblGoodQty,
				ware_fabriccustreturndispatchdetails.dblEmbroideryQty,
				ware_fabriccustreturndispatchdetails.dblPDammageQty,
				ware_fabriccustreturndispatchdetails.dblFDammageQty ,
				trn_orderdetails.STATUS 
				FROM
				trn_orderdetails
				Inner Join ware_fabriccustreturndispatchheader ON ware_fabriccustreturndispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabriccustreturndispatchheader.intOrderYear = trn_orderdetails.intOrderYear
				Inner Join ware_fabriccustreturndispatchdetails ON ware_fabriccustreturndispatchheader.intBulkDispatchNo = ware_fabriccustreturndispatchdetails.intBulkDispatchNo AND ware_fabriccustreturndispatchheader.intBulkDispatchNoYear = ware_fabriccustreturndispatchdetails.intBulkDispatchNoYear AND ware_fabriccustreturndispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
				Inner Join mst_part ON ware_fabriccustreturndispatchdetails.intPart = mst_part.intId
				Inner Join mst_colors_ground ON ware_fabriccustreturndispatchdetails.intGroundColor = mst_colors_ground.intId
				WHERE
				ware_fabriccustreturndispatchheader.intBulkDispatchNo =  '$serialNoArray[0]' AND
				ware_fabriccustreturndispatchheader.intBulkDispatchNoYear =  '$serialNoArray[1]' ";
				
		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$msg1 =""; 
		$msg ="Maximum Qtys for items"; 
		while($row=mysqli_fetch_array($result))
		{
				$status=$row['intStatus'];
				$orderNo=$row['intOrderNo'];
				$orderYear=$row['intOrderYear'];
				$salesOrderNo=$row['strSalesOrderNo'];
				$salesOrderId=$row['intSalesOrderId'];
				$cutNo=$row['strCutNo'];
				$partId=$row['intPart'];
				$part=$row['part'];
				$size=$row['strSize'];
				$tot=val($row['dblSampleQty'])+val($row['dblGoodQty'])+val($row['dblEmbroideryQty'])+val($row['dblPDammageQty'])+val($row['dblFDammageQty']);
			
				$balQty=loadBalQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$partId,$size);
				$nonStkConfQty=0;
				$orderQty=loadOrderQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade);
				$maxQty=$balQty-$nonStkConfQty;
				if($row['intStatus']>1 && ($row['intStatus']<=($row['intApproveLevels']))){
				$maxQty=$balQty-$nonStkConfQty+$tot;
				}
				
				//echo $tot."pp".$maxQty;

				//------check maximum FR Qty--------------------
				$confirmatonMode=loadConfirmatonMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);
			
				if($row['STATUS']== -10){
					$errorFlg=1;
					$msg ="Sales order ".$row['strSalesOrderNo']." is already closed";
				}
				else if($status==0){
					$errorFlg=1;
					$msg ="This Dispatch No is Rejected.";
				}
				else if($status==1){
					$errorFlg=1;
					$msg ="Final confirmation of this Dispatch No is already raised";
				}
				else if($confirmatonMode==0){// 
					$errorFlg = 1;
					$msg ="No Permission to Approve"; 
				}
				else if($tot>$maxQty){
					$errorFlg=1;
					$msg .="<br> ".$cutNo."-".$part."-".$salesOrderNo."-".$size." =".$maxQty;
				}
		}//end of while
	
	if($msg1!=''){
		$msg=$msg1;
	}
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}

else if($requestType=='validateRejecton')
	{
		$errorFlg=0;
		$serialNo  = $_REQUEST['serialNo'];
		$serialNoArray=explode("/",$serialNo);
		
		$sql = "SELECT
		ware_fabriccustreturndispatchheader.intStatus, 
		ware_fabriccustreturndispatchheader.intApproveLevels 
		FROM ware_fabriccustreturndispatchheader
		WHERE
		ware_fabriccustreturndispatchheader.intBulkDispatchNo =  '$serialNoArray[0]' AND
		ware_fabriccustreturndispatchheader.intBulkDispatchNoYear =  '$serialNoArray[1]'";

		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$msg =""; 
		$row=mysqli_fetch_array($result);
		
		$rejectionMode=loadRejectionMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);
		
		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this Dispatch No is already raised.So cant reject"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This Dispatch No is already Rejected"; 
		}
		else if($rejectionMode==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="No Permission to reject this Dispatch No"; 
		}
		else{//confirmation has been raised
			$errorFlg = 0;
			
		}

	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);

	}

	

	//-----------------------------------------------------------
function loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size,$grade)
{
		global $db;
		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				ware_stocktransactions_fabric.intLocationId =  '$location' AND
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType =  'Received' 
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//-----------------------------------------------------------
function loadOrderQty($location,$orderNo,$orderYear,$salesOrderNo,$size,$grade)
{
		global $db;
		  $sql = "SELECT
				trn_ordersizeqty.dblQty
				FROM trn_ordersizeqty
				WHERE
				trn_ordersizeqty.intOrderNo =  '$orderNo' AND
				trn_ordersizeqty.intOrderYear =  '$orderYear' AND
				trn_ordersizeqty.intSalesOrderId =  '$salesOrderNo' AND
				trn_ordersizeqty.strSize =  '$size'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
	//--------------------------------------------------------------
	function loadBalQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$intPart,$size){
		global $db;
		    $sql = "SELECT
				sum(ware_fabriccustreturndetails.dblQty-IFNULL(ware_fabriccustreturndetails.dblDispatchedQty,0)) as qty , 
				(SELECT 
				sum(IFNULL(ware_fabriccustreturndispatchdetails.dblSampleQty,0)+IFNULL(ware_fabriccustreturndispatchdetails.dblGoodQty,0)+IFNULL(ware_fabriccustreturndispatchdetails.dblEmbroideryQty,0)+IFNULL(ware_fabriccustreturndispatchdetails.dblPDammageQty,0)+IFNULL(ware_fabriccustreturndispatchdetails.dblFdammageQty,0)) 
				FROM
				ware_fabriccustreturndispatchdetails
				Inner Join ware_fabriccustreturndispatchheader ON ware_fabriccustreturndispatchdetails.intBulkDispatchNo = ware_fabriccustreturndispatchheader.intBulkDispatchNo AND ware_fabriccustreturndispatchdetails.intBulkDispatchNoYear = ware_fabriccustreturndispatchheader.intBulkDispatchNoYear
				WHERE
				ware_fabriccustreturndispatchheader.intStatus =  '1' AND 
				ware_fabriccustreturndispatchheader.intOrderNo =  ware_fabriccustreturnheader.intOrderNo AND
				ware_fabriccustreturndispatchheader.intOrderYear =  ware_fabriccustreturnheader.intOrderYear AND
				ware_fabriccustreturndispatchdetails.strCutNo =  ware_fabriccustreturndetails.strCutNo AND
				ware_fabriccustreturndispatchdetails.intSalesOrderId =  ware_fabriccustreturndetails.intSalesOrderId AND
				ware_fabriccustreturndispatchdetails.strSize =  ware_fabriccustreturndetails.strSize AND
				ware_fabriccustreturndispatchdetails.intPart =  ware_fabriccustreturndetails.intPart ) as dispQty  
				
				FROM
				ware_fabriccustreturndetails
				Inner Join ware_fabriccustreturnheader ON ware_fabriccustreturndetails.intFabricCustReturnNo = ware_fabriccustreturnheader.intFabricCustReturnNo AND ware_fabriccustreturndetails.intFabricCustReturnYear = ware_fabriccustreturnheader.intFabricCustReturnYear
				WHERE
				ware_fabriccustreturnheader.intOrderNo =  '$orderNo' AND
				ware_fabriccustreturnheader.intOrderYear =  '$orderYear' AND
				ware_fabriccustreturndetails.strCutNo =  '$cutNo' AND
				ware_fabriccustreturndetails.intSalesOrderId =  '$salesOrderId' AND
				ware_fabriccustreturndetails.strSize =  '$size' AND
				ware_fabriccustreturndetails.intPart =  '$intPart'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
	//	echo val($rows['qty']);
		return val($rows['qty']-$rows['dispQty']);
	}
	//--------------------------------------------------------------
	function loadNonStockConfQty($location,$orderNo,$orderYear,$cutNo,$salesOrderId,$intPart,$size){
		global $db;
		   $sql = "SELECT
					sum(ware_fabriccustreturndispatchdetails.dblSampleQty+
					ware_fabriccustreturndispatchdetails.dblGoodQty+
					ware_fabriccustreturndispatchdetails.dblEmbroideryQty+
					ware_fabriccustreturndispatchdetails.dblPDammageQty+
					ware_fabriccustreturndispatchdetails.dblFdammageQty) as qty
					FROM
					ware_fabriccustreturndispatchheader
					Inner Join ware_fabriccustreturndispatchdetails ON ware_fabriccustreturndispatchheader.intBulkDispatchNo = ware_fabriccustreturndispatchdetails.intBulkDispatchNo AND ware_fabriccustreturndispatchheader.intBulkDispatchNoYear = ware_fabriccustreturndispatchdetails.intBulkDispatchNoYear
					WHERE
					ware_fabriccustreturndispatchheader.intOrderNo =  '$orderNo' AND
					ware_fabriccustreturndispatchheader.intOrderYear =  '$orderYear' AND
					ware_fabriccustreturndispatchdetails.intSalesOrderId =  '$salesOrderId' AND
					ware_fabriccustreturndispatchdetails.strCutNo =  '$cutNo' AND
					ware_fabriccustreturndispatchdetails.strSize =  '$size' AND
					ware_fabriccustreturndispatchdetails.intPart =  '$intPart' AND
					ware_fabriccustreturndispatchheader.intStatus >  '1' AND
					ware_fabriccustreturndispatchheader.intApproveLevels >= ware_fabriccustreturndispatchheader.intStatus AND
					ware_fabriccustreturndispatchheader.intCompanyId =  '$location'";
	
		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
	//	echo val($rows['qty']);
		return val($rows['qty']);
	}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}

//--------------------------------------------------------

?>

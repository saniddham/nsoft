<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "libraries/jqgrid2/inc/jqgrid_dist.php";

$location 		= $sessions->getLocationId();
$company 		= $sessions->getCompanyId();
$intUser  		= $sessions->getUserId();

$programCode	= 'P0650';

$approveLevel 	= (int)getMaxApproveLevel();

//BEGIN - ADD DEFAULT WHERE STRING WHEN FORM LOADING {
$arr =  json_decode($_REQUEST['filters'],true);

$arr = $arr['rules'];

$where_string = '';
$where_array = array(
					'Status'=>'tb1.intStatus',
					'Dispatch_No'=>"tb1.intBulkDispatchNo",
					'Dispatch_Year'=>"tb1.intBulkDispatchNoYear",
					'strStyleNo'=>"(select  group_concat(strStyleNo separator ', ') from  (select distinct strStyleNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb2.intOrderNo AND
							tb.intOrderYear =  tb2.intOrderYear)",
					'strGraphicNo'=>"(select  group_concat(strGraphicNo separator ', ') from  (select distinct strGraphicNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb2.intOrderNo AND
							tb.intOrderYear =  tb2.intOrderYear)",
					'Order_No'=>"concat(tb1.intOrderNo,'/',tb1.intOrderYear)",
					'Date'=>"tb1.dtmdate"
					);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}

if(!count($arr)>0)					 
		$where_string .= "AND tb1.dtmdate = '".date('Y-m-d')."'";
//END }

$sql = "select * from(SELECT DISTINCT 
							if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected','Pending')) as Status,
							tb1.intBulkDispatchNo as `Dispatch_No`,
							tb1.intBulkDispatchNoYear as `Dispatch_Year`,
							tb1.dtmdate	as `Date`,
							
							(select  group_concat(strStyleNo separator ', ') from  (select distinct strStyleNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb2.intOrderNo AND
							tb.intOrderYear =  tb2.intOrderYear
							) as strStyleNo,
							
							(select  group_concat(strGraphicNo separator ', ') from  (select distinct strGraphicNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb2.intOrderNo AND
							tb.intOrderYear =  tb2.intOrderYear

							) as strGraphicNo, 
							
							IFNULL((SELECT
								Sum(ware_fabriccustreturndispatchdetails.dblFdammageQty)
								FROM
								ware_fabriccustreturndispatchdetails 
								WHERE
								ware_fabriccustreturndispatchdetails.intBulkDispatchNo  =   tb1.intBulkDispatchNo AND
								ware_fabriccustreturndispatchdetails.intBulkDispatchNoYear =  tb1.intBulkDispatchNoYear

							),0) as dispatchQty, 
							
							sys_users.strUserName as User,
							tb1.intApproveLevels,
							tb1.intStatus,
							concat(tb1.intOrderNo,'/',tb1.intOrderYear) as Order_No,  
							IFNULL((
                                      SELECT
								concat(sys_users.strUserName,'(',max(ware_fabriccustreturndispatchheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_fabriccustreturndispatchheader_approvedby
								Inner Join sys_users ON ware_fabriccustreturndispatchheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_fabriccustreturndispatchheader_approvedby.intBulkDispatchNo  = tb1.intBulkDispatchNo AND
								ware_fabriccustreturndispatchheader_approvedby.intYear =  tb1.intBulkDispatchNoYear AND
								ware_fabriccustreturndispatchheader_approvedby.intApproveLevelNo =  '1'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.intStatus>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_fabriccustreturndispatchheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_fabriccustreturndispatchheader_approvedby
								Inner Join sys_users ON ware_fabriccustreturndispatchheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_fabriccustreturndispatchheader_approvedby.intBulkDispatchNo  = tb1.intBulkDispatchNo AND
								ware_fabriccustreturndispatchheader_approvedby.intYear =  tb1.intBulkDispatchNoYear AND
								ware_fabriccustreturndispatchheader_approvedby.intApproveLevelNo =  '$i'
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.intStatus>1) AND (tb1.intStatus<=tb1.intApproveLevels) AND ((SELECT
								max(ware_fabriccustreturndispatchheader_approvedby.dtApprovedDate)
								FROM
								ware_fabriccustreturndispatchheader_approvedby
								Inner Join sys_users ON ware_fabriccustreturndispatchheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_fabriccustreturndispatchheader_approvedby.intBulkDispatchNo  = tb1.intBulkDispatchNo AND
								ware_fabriccustreturndispatchheader_approvedby.intYear =  tb1.intBulkDispatchNoYear AND
								ware_fabriccustreturndispatchheader_approvedby.intApproveLevelNo =  ($i-1))<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*false part*/
								 if($i>tb1.intApproveLevels,'-----',''))
								
								/*end IFNULL false part*/
								) as `".$approval."`, "; 
									
								}
								
						$sql .= " 'View' as `View`   
						FROM
							ware_fabriccustreturndispatchheader as tb1 
						Inner Join trn_orderdetails as tb2 ON tb1.intOrderNo = tb2.intOrderNo AND tb1.intOrderYear = tb2.intOrderYear
						Inner Join sys_users ON tb1.intCteatedBy = sys_users.intUserId 
						WHERE 1=1
							$where_string						
							)  as t where 1=1
						";
					      	//echo $sql;
$col = array();

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist

$cols[] = $col;	$col=NULL;
//Fabric Receive No
$col["title"] 	= "Dispatch No"; // caption of column
$col["name"] 	= "Dispatch_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= "?q=651&serialNo={Dispatch_No}&year={Dispatch_Year}";	 
$col["linkoptions"] = "target='fabricDispatchNote_custRet.php'"; // extra params with <a> tag

$reportLink  = "?q=898&serialNo={Dispatch_No}&year={Dispatch_Year}";
$reportLinkApprove  = "?q=898&serialNo={Dispatch_No}&year={Dispatch_Year}&approveMode=1";

$cols[] = $col;	$col=NULL;


//Fabric Receive Year
$col["title"] = "Dispatch Year"; // caption of column
$col["name"] = "Dispatch_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//CUSTOMER PO NO
$col["title"] = "Style No"; // caption of column
$col["name"] = "strStyleNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Graphic No"; // caption of column
$col["name"] = "strGraphicNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Order No"; // caption of column
$col["name"] = "Order_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//CUSTOMER
$col["title"] = "Disp Qty"; // caption of column
$col["name"] = "dispatchQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;


//DATE
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//FIRST APPROVAL
$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;

$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='rptFabricDispatchNote_custRet.php'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='rptFabricDispatchNote_custRet.php'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}


//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='rptFabricDispatchNote_custRet.php'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Customer Returned Fabric OUT Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'Dispatch_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>

			<?php echo $out?>
<?php
//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(ware_fabriccustreturndispatchheader.intApproveLevels) AS appLevel
			FROM ware_fabriccustreturndispatchheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
?>

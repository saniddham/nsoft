<?php 
//////////////////////////////////////////////
//Create By:H.B.G Korala
//note://if final confirmation raised,insert into stocktransaction table.
/////////////////////////////////////////////

	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];

	include "{$backwardseperator}dataAccess/Connector.php";
	
	$programName='Fabric Received Note';
	$programCode='P0045';
	$fabRcvApproveLevel = (int)getApproveLevel($programName);
	
	/////////// parameters /////////////////////////////

	
	$serialNo = $_REQUEST['serialNo'];
	$year = $_REQUEST['year'];
	
	if($_REQUEST['status']=='approve')
	{
		$sql = "UPDATE `ware_fabriccustreturndispatchheader` SET `intStatus`=intStatus-1 WHERE (intBulkDispatchNo='$serialNo') AND (`intBulkDispatchNoYear`='$year') and intStatus not in(0,1)";
		$result = $db->RunQuery($sql);
		
		if($result){
			$sql = "SELECT ware_fabriccustreturndispatchheader.intStatus, 
			ware_fabriccustreturndispatchheader.intApproveLevels,  
			ware_fabriccustreturndispatchheader.intCompanyId AS location, 
			mst_locations.intCompanyId AS company 
			FROM 
			ware_fabriccustreturndispatchheader 
			Inner Join mst_locations ON ware_fabriccustreturndispatchheader.intCompanyId = mst_locations.intId
			WHERE (intBulkDispatchNo='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
			$results = $db->RunQuery($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			$company = $row['company'];
			$location = $row['location'];
			
			$approveLevel=$savedLevels+1-$status;
			$sqlI = "INSERT INTO `ware_fabriccustreturndispatchheader_approvedby` (`intBulkDispatchNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate) 
				VALUES ('$serialNo','$year','$approveLevel','$userId',now())";
			$resultI = $db->RunQuery($sqlI);

		if($status==1){//if final confirmation raised,insert to stock
			$sql1 = "SELECT
				ware_fabriccustreturndispatchheader.intOrderNo,
				ware_fabriccustreturndispatchheader.intOrderYear,
				ware_fabriccustreturndispatchdetails.strCutNo,
				ware_fabriccustreturndispatchdetails.intSalesOrderId,
				ware_fabriccustreturndispatchdetails.intPart, 
				ware_fabriccustreturndispatchdetails.strSize,
				IFNULL(ware_fabriccustreturndispatchdetails.dblSampleQty,0) as dblSampleQty,
				IFNULL(ware_fabriccustreturndispatchdetails.dblGoodQty,0) as dblGoodQty,
				IFNULL(ware_fabriccustreturndispatchdetails.dblEmbroideryQty,0) as dblEmbroideryQty,
				IFNULL(ware_fabriccustreturndispatchdetails.dblPDammageQty,0) as dblPDammageQty,
				IFNULL(ware_fabriccustreturndispatchdetails.dblFDammageQty,0) as dblFDammageQty 
				FROM
				ware_fabriccustreturndispatchdetails
				Inner Join ware_fabriccustreturndispatchheader ON ware_fabriccustreturndispatchdetails.intBulkDispatchNo = ware_fabriccustreturndispatchheader.intBulkDispatchNo AND ware_fabriccustreturndispatchdetails.intBulkDispatchNoYear = ware_fabriccustreturndispatchheader.intBulkDispatchNoYear
				WHERE
				ware_fabriccustreturndispatchdetails.intBulkDispatchNo =  '$serialNo' AND
				ware_fabriccustreturndispatchdetails.intBulkDispatchNoYear =  '$year'";
				$result1 = $db->RunQuery($sql1);
				$toSave=0;
				while($row=mysqli_fetch_array($result1))
				{
					$orderNo=$row['intOrderNo'];
					$orderYear=$row['intOrderYear'];
					$cutNo=$row['strCutNo'];
					$part=$row['intPart'];
					$salesOrderId=$row['intSalesOrderId'];
					$size=$row['strSize'];
					$sampleQty=round($row['dblSampleQty'],4);
					$goodQty=round($row['dblGoodQty'],4);
					$embroideryQty=round($row['dblEmbroideryQty'],4);
					$pDammageQty=round($row['dblPDammageQty'],4);
					$fDammageQty=round($row['dblFDammageQty'],4);
					$toSave=$sampleQty+$goodQty+$embroideryQty+$pDammageQty+$fDammageQty;
					
					$sql2="	select 
					ware_fabriccustreturnheader.intFabricCustReturnNo,
					ware_fabriccustreturnheader.intFabricCustReturnYear
					FROM
					ware_fabriccustreturndetails
					Inner Join ware_fabriccustreturnheader ON ware_fabriccustreturndetails.intFabricCustReturnNo = ware_fabriccustreturnheader.intFabricCustReturnNo AND ware_fabriccustreturndetails.intFabricCustReturnYear = ware_fabriccustreturnheader.intFabricCustReturnYear
					WHERE 
					ware_fabriccustreturnheader.intStatus = 1 AND 
					ware_fabriccustreturnheader.intOrderNo =  '$orderNo' AND
					ware_fabriccustreturnheader.intOrderYear =  '$orderYear' AND
					ware_fabriccustreturndetails.strCutNo =  '$cutNo' AND
					ware_fabriccustreturndetails.intSalesOrderId =  '$salesOrderId' AND
					ware_fabriccustreturndetails.strSize =  '$size' AND
					ware_fabriccustreturndetails.intPart =  '$intPart'";
					$result2 = $db->RunQuery($sql2);
					while($row2=mysqli_fetch_array($result2))
					{
/*						$sql3 = "update ware_fabriccustreturnheader  
						Inner Join ware_fabriccustreturndetails ON ware_fabriccustreturndetails.intFabricCustReturnNo = ware_fabriccustreturnheader.intFabricCustReturnNo AND ware_fabriccustreturndetails.intFabricCustReturnYear = ware_fabriccustreturnheader.intFabricCustReturnYear
						set dblDispatchedQty = dblDispatchedQty+
						if((dblQty-IFNULL(dblDispatchedQty,0))<$toSave,(dblQty-IFNULL(dblDispatchedQty,0)),$toSave)  
						
						WHERE
						ware_fabriccustreturnheader.intStatus = 1 AND 
						ware_fabriccustreturnheader.intOrderNo =  '$orderNo' AND
						ware_fabriccustreturnheader.intOrderYear =  '$orderYear' AND
						ware_fabriccustreturndetails.strCutNo =  '$cutNo' AND
						ware_fabriccustreturndetails.intSalesOrderId =  '$salesOrderId' AND
						ware_fabriccustreturndetails.strSize =  '$size' AND
						ware_fabriccustreturndetails.intPart =  '$intPart'";
						$result3 = $db->RunQuery($sql3);
*/						
					}
				}
		}

		//---------------------------------------------------------------------------
		}
		
	}
	else if($_REQUEST['status']=='reject')
	{
		//$grnApproveLevel = (int)getApproveLevel($programName);
		$sql = "SELECT ware_fabriccustreturndispatchheader.intStatus,ware_fabriccustreturndispatchheader.intApproveLevels FROM ware_gatepassheader WHERE  (intBulkDispatchNo='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
		$results = $db->RunQuery($sql);
		$row = mysqli_fetch_array($results);
		$status = $row['intStatus'];
		$savedLevels = $row['intApproveLevels'];
		
		$sql = "UPDATE `ware_fabriccustreturndispatchheader` SET `intStatus`=0 WHERE (intBulkDispatchNo='$serialNo') AND (`intBulkDispatchNoYear`='$year')";
		$result = $db->RunQuery($sql);
		
		$sql = "DELETE FROM `ware_fabriccustreturndispatchheader_approvedby` WHERE (`intBulkDispatchNo`='$serialNo') AND (`intYear`='$year')";
		$result2 = $db->RunQuery($sql);
		
		$sqlI = "INSERT INTO `ware_fabriccustreturndispatchheader_approvedby` (`intBulkDispatchNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate) 
			VALUES ('$serialNo','$year','0','$userId',now())";
		$resultI = $db->RunQuery($sqlI);
		
		//--- 
	}
//--------------------------------------------------------------
?>
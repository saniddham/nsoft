<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>

<?php
//CLASSSE----------------
	require_once	$_SESSION['ROOT_PATH']."class/tables/trn_sales_order_open_requisition_header.php";	            
														$trn_sales_order_open_requisition_header	= new trn_sales_order_open_requisition_header($db);
	require_once	$_SESSION['ROOT_PATH']."class/tables/trn_sales_order_open_header.php";	            
														$trn_sales_order_open_header				= new trn_sales_order_open_header($db);
	include_once 	$_SESSION['ROOT_PATH']."class/tables/mst_locations.php";	$mst_locations								= new mst_locations($db);
	include_once 	$_SESSION['ROOT_PATH']."class/tables/menupermision.php";	$menupermision 								= new menupermision($db);
	
//LOAD INITIAL DATA-------------
	$reqYearCombo	= $trn_sales_order_open_requisition_header->getAllApprovedRequisitionsYears('');
	$date			= date("Y-m-d");
	$programCode	= 'P1160';
	
//LOAD PERMISSIONS
	$menupermision->set($programCode,$sessions->getUserId());
	$mst_locations->set($sessions->getLocationId());
	if(isset($_REQUEST['serialNo']) && isset($_REQUEST['serialYear']))
	{	
		$trn_sales_order_open_header->set($_REQUEST['serialNo'],$_REQUEST['serialYear']);
		$status				= $trn_sales_order_open_header->getSTATUS();
		$levels				= $trn_sales_order_open_header->getAPPROVE_LEVELS();	
		$butSave			= $menupermision->checkMenuPermision('Edit',$status,$levels);
		$butApprove			= $menupermision->checkMenuPermision('Approve',$status,$levels);
	}
	else
	{
		$butSave			= $menupermision->checkMenuPermision('Edit','null','null');
		$butApprove			= $menupermision->checkMenuPermision('Approve','null','null');
	}
	
//------------------------- 
?>

<title>Open Sales Orders</title>
<style type="text/css">
.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<body>
<form id="frmOpenSalesOrder" name="frmOpenSalesOrder" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL" style="width:1000">
		  <div class="trans_text">Open Sales Orders</div>
		  <table width="700" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
    <tr><td><table width="100%">
      <tr>
        <td><fieldset class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td width="8%" class="normalfnt">Serial No</td>
  <td width="21%" class="normalfnt"><input  id="txtSerialNo" class="normalfnt" style="width:70px;text-align:right" type="text" value="<?php echo $_REQUEST["serialNo"] ?>" readonly/><input  id="txtYear" class="normalfnt" style="width:40px;text-align:right" type="text" value="<?php echo $_REQUEST["serialYear"] ?>" readonly/></td>
  <td width="1%" class="normalfnt">&nbsp;</td>
  <td width="18%" class="normalfnt">&nbsp;</td>
  <td width="8%" class="normalfnt">&nbsp;</td>
  <td width="12%" class="normalfnt">&nbsp;</td>
    <td width="10%" class="normalfntRight">&nbsp;Date</td>
    <td width="22%" class="normalfntRight"><input name="dtDate" type="text" value="<?php echo $date; ?>" class="txtbox" id="dtDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
</tr>        </table></fieldset></td>
      </tr>
    </table></td></tr>
    <tr><td><table width="100%">
<tr>
        <td><table  bgcolor="#E8FED8" class="tableBorder_allRound"  width="100%" border="0" cellpadding="0" cellspacing="0"  >
            <tr height="25">
              <td width="11%" class="normalfnt"><span class="reqisition">Requisition No</span>&nbsp;</td>
              <td width="23%" class="normalfnt"><span class="reqisition">
                <select name="cboRequisitionYear" id="cboRequisitionYear" style="width:70px" >
                <?php echo $reqYearCombo; ?>
                </select><select name="cboRequisitionNo" id="cboRequisitionNo" style="width:130px" >
                </select>
              </span></td>
              <td width="11%" class="normalfnt"><div class="reqisition"></div></td>
              <td width="23%" class="normalfnt"><div class="reqisition"></div></td>
              <td width="18%" class="normalfntRight" colspan="2"></td>
              <td width="14%" align="right" class="normalfntLeft"></td> </tr>  
              <tr height="25">
              <td width="11%" class="normalfnt">Order&nbsp;No</td>
              <td width="23%" class="normalfnt"><select name="cboOrderYear" id="cboOrderYear" style="width:70px" disabled="disabled" >
                </select><select name="cboOrderNo" id="cboOrderNo" style="width:130px" disabled="disabled" >
                  <option value=""></option>
                </select></td>
              <td width="6%" class="normalfnt">&nbsp;</td>
              <td width="11%" class="normalfntRight" align="right">Customer PO</td>
              <td class="normalfntLeft" colspan="2" align="left">&nbsp;<input  id="txtCustomerPO" class="normalfnt" style="width:100px;text-align:right" type="text" value="" readonly/></td>
              <td width="32%" align="right" class="normalfntLeft">&nbsp;</td> </tr>              </table></td>
      </tr>
    </table></td></tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="2500" height=""  class="bordered" id="tblSalesOrders" >
            <tr class="">
              <th>Open</th>
              <th >Status</th>
              <th >Sales Order No </th>
              <th >Graphic No</th>
              <th >Sample No</th>
              <th >Style</th>
              <th >Combo</th>
              <th >Part</th>
              <th >Print</th>
              <th >Revision</th>
              <th >Order Qty</th>
              <th >Price</th>
              <th >Value</th>
              <th >Technique Group</th> 
              </tr>
            </table>
        </div></td></tr>
     <tr>
        <td align="center" class="">&nbsp;</td>
      </tr>       </tr>
      <tr>
        <td align="center" class="tableBorder_allRound">
        <img src="images/Tnew.jpg" width="92" height="24" id="butNew" name="butNew"  class="mouseover"/>
        <img src="images/Tsave.jpg" width="92" height="24" id="butSave" name="butSave" <?php if($butSave['type'] != 1){ ?> style="display:none" <?php } ?>  class="mouseover"/>
        <img class="mouseover" src="images/Tconfirm.jpg" width="92" height="24"id="butConfirm" name="butConfirm" <?php if($butApprove['type'] != 1){ ?> style="display:none" <?php } ?> />
        <img src="images/Treport.jpg" width="92" height="24" id="butReport" name="butReport"  class="mouseover"/><a href="main.php"><img src="images/Tclose.jpg" width="92" height="24"  class="mouseover"/></a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<div style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</body>
</html>

 
	loadSavedDetails($('#frmOpenSalesOrder #txtSerialNo').val(),$('#frmOpenSalesOrder #txtYear').val());
 
$(document).ready(function(){	

	
	$("#frmOpenSalesOrder #cboRequisitionYear").die('change').live('change',function(){
		loadRequisitionNos();
	});
	$("#frmOpenSalesOrder #cboRequisitionNo").die('change').live('change',function(){
		loadOrders();
		loadCustomerPO();
		loadSalesOrderDetails();
	});
	$("#frmOpenSalesOrder #butSave").die('click').live('click',function(){
		saveMain();
	});
	$("#frmOpenSalesOrder #butNew").die('click').live('click',function(){
		loadNewPage();
	});
	$("#frmOpenSalesOrder #butConfirm").die('click').live('click',function(){
		viewReport('Confirm');
	});
	$("#frmOpenSalesOrder #butReport").die('click').live('click',function(){
		viewReport('');
	});
	$("#frmOpenOrdersRpt #butRptConfirm").die('click').live('click',function(){
		approve();
	});
	$("#frmOpenOrdersRpt #butRptReject").die('click').live('click',function(){
		reject();
	});
	
});

function loadRequisitionNos(){
	
	var reqYear 	= $('#frmOpenSalesOrder #cboRequisitionYear').val();
	
	if(reqYear==''){
		$('#frmOpenSalesOrder #cboRequisitionNo').html('');
		$('#frmOpenSalesOrder #cboOrderYear').val('');
		$('#frmOpenSalesOrder #cboOrderNo').html('');
		return;
	}
	var url   		= "controller.php?q=1160&requestType=loadRequisitionNos";
	var data		= "reqYear="+reqYear;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmOpenSalesOrder #cboRequisitionNo').html(json.reqCombo);
			}
	});	
}

function loadOrders(){
	
	var reqYear 	= $('#frmOpenSalesOrder #cboRequisitionYear').val();
	var reqNo	 	= $('#frmOpenSalesOrder #cboRequisitionNo').val();
	
	if(reqYear==''){
		$('#frmOpenSalesOrder #cboRequisitionYear').val('');
		$('#frmOpenSalesOrder #cboRequisitionNo').html('');
		$('#frmOpenSalesOrder #cboOrderYear').html('');
		$('#frmOpenSalesOrder #cboOrderNo').html('');
		return;
	}
	if(reqNo==''){
		$('#frmOpenSalesOrder #cboRequisitionNo').val('');
		$('#frmOpenSalesOrder #cboOrderYear').html('');
		$('#frmOpenSalesOrder #cboOrderNo').html('');
		return;
	}
	var url   		= "controller.php?q=1160&requestType=loadOrders";
	var data		= "reqYear="+reqYear;
		data		+= "&reqNo="+reqNo;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmOpenSalesOrder #cboOrderYear').html(json.orderYearCombo);
				$('#frmOpenSalesOrder #cboOrderNo').html(json.orderNoCombo);
			}
	});	
}

function loadCustomerPO(){
	
	var orderYear 	= $('#frmOpenSalesOrder #cboOrderYear').val();
	var orderNo 	= $('#frmOpenSalesOrder #cboOrderNo').val();
	
	var url   		= "controller.php?q=1160&requestType=loadCustomerPO";
	var data		= "orderYear="+orderYear+"&orderNo="+orderNo;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmOpenSalesOrder #txtCustomerPO').val(json.customerPO);
			}
	});	
}

function loadSalesOrderDetails(){
	
	var reqYear 	= $('#frmOpenSalesOrder #cboRequisitionYear').val();
	var reqNo	 	= $('#frmOpenSalesOrder #cboRequisitionNo').val();
	
	if(reqYear==''){
		$('#frmOpenSalesOrder #cboRequisitionYear').val('');
		$('#frmOpenSalesOrder #cboRequisitionNo').html('');
		$('#frmOpenSalesOrder #cboOrderYear').html('');
		$('#frmOpenSalesOrder #cboOrderNo').html('');
		return;
	}
	if(reqNo==''){
		$('#frmOpenSalesOrder #cboRequisitionNo').val('');
		$('#frmOpenSalesOrder #cboOrderYear').html('');
		$('#frmOpenSalesOrder #cboOrderNo').html('');
	}
	
	var url   		= "controller.php?q=1160&requestType=loadSalesOrderDetails";
	var data		= "reqYear="+reqYear;
		data		+= "&reqNo="+reqNo;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmOpenSalesOrder #tblSalesOrders').html(json.tableHtml);
			}
	});	
}

function loadSavedDetails(serialNo,serialYear){
	
	$('#frmOpenSalesOrder #txtSerialNo').val(serialNo);
	$('#frmOpenSalesOrder #txtYear').val(serialYear);
	
	loadHeader(serialNo,serialYear);
	loadDetails(serialNo,serialYear);
	
}

function loadHeader(serialNo,serialYear){

	var url   		= "controller.php?q=1160&requestType=loadHeader";
	var data		= "serialNo="+serialNo;
		data		+= "&serialYear="+serialYear;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmOpenSalesOrder #cboRequisitionYear').html(json.reqYearCombo);
				$('#frmOpenSalesOrder #cboRequisitionNo').html(json.reqNoCombo);
				$('#frmOpenSalesOrder #cboOrderNo').html(json.orderNoCombo);
				$('#frmOpenSalesOrder #cboOrderYear').html(json.orderYearCombo);
				$('#frmOpenSalesOrder #dtDate').val(json.date);
				$('#frmOpenSalesOrder #txtCustomerPO').val(json.customerPO);
			}
	});	
	
}


function loadDetails(serialNo,serialYear){
	
	var url   		= "controller.php?q=1160&requestType=loadSalesOrderDetails";
	var data		= "serialNo="+serialNo;
		data		+= "&serialYear="+serialYear;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmOpenSalesOrder #tblSalesOrders').html(json.tableHtml);
			}
	});	
}

function saveMain(){
	
	var serialNo	=$('#frmOpenSalesOrder #txtSerialNo').val();
	var serialYear	= $('#frmOpenSalesOrder #txtYear').val();

	if(serialNo=='' && serialYear=='')
		save();
	else
		update();
	
}

function save(){
	
	showWaiting();
	
	var chkStatus		= false;
	var arrDetails		= "";
	var numberValidate 	= 0;
	
	var reqNo		= $('#frmOpenSalesOrder #cboRequisitionNo').val();
	var reqYear		= $('#frmOpenSalesOrder #cboRequisitionYear').val();
	var orderNo		= $('#frmOpenSalesOrder #cboOrderNo').val();
	var orderYear	= $('#frmOpenSalesOrder #cboOrderYear').val();

	var data = "requestType=save";
	
	var arrHeader 	= "{";
							arrHeader += '"reqNo":"'+reqNo+'",' ;
							arrHeader += '"reqYear":"'+reqYear+'",' ;
							arrHeader += '"orderNo":"'+orderNo+'",' ;
							arrHeader += '"orderYear":'+orderYear+'';
							
		arrHeader  += "}";
		
	$('#frmOpenSalesOrder #tblSalesOrders .open:checked').each(function(){
		var salesOrder		= $(this).parent().parent().find('.salesOrder').attr('id');
			arrDetails += "{";
			arrDetails += '"orderNo":"'+ orderNo +'",' ;
			arrDetails += '"orderYear":"'+ orderYear +'",' ;
			arrDetails += '"salesOrder":'+salesOrder+'';
			arrDetails += "},";

	});
	
	arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
	var arrDetails	= '['+arrDetails+']';
	
	data		   +="&arrHeader="+arrHeader;
	data		   +="&arrDetails="+arrDetails;

	var url   = "controller.php?q=1160";	
	$.ajax({
			url:url,
			dataType:'json',
			type:'post',
			data:data,
			async:false,
			success:function(json){
				$('#frmOpenSalesOrder #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				
				if(json.type=='pass')
				{
					hideWaiting();
					$('#frmOpenSalesOrder #txtSerialNo').val(json.serialNo);
					$('#frmOpenSalesOrder #txtYear').val(json.serialYear);
					if(json.savePerm==1){
						$('#frmOpenSalesOrder #butSave').show();
					}
					else{
						$('#frmOpenSalesOrder #butSave').hide();
					}
					if(json.approvePerm==1){
						$('#frmOpenSalesOrder #butConfirm').show();
					}
					else{
						$('#frmOpenSalesOrder #butConfirm').hide();
					}
					return;
				}
				else
				{
					hideWaiting();
				}
			},
			error:function(xhr,status)
			{
				$('#frmOpenSalesOrder #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				hideWaiting();
				return;;
			}
	});
	hideWaiting();	
}

function update(){

	showWaiting();
	
	var chkStatus		= false;
	var arrDetails		= "";
	var numberValidate 	= 0;
	
	var serialNo	= $('#frmOpenSalesOrder #txtSerialNo').val();
	var serialYear	= $('#frmOpenSalesOrder #txtYear').val();
	var reqNo		= $('#frmOpenSalesOrder #cboRequisitionNo').val();
	var reqYear		= $('#frmOpenSalesOrder #cboRequisitionYear').val();
	var orderNo		= $('#frmOpenSalesOrder #cboOrderNo').val();
	var orderYear	= $('#frmOpenSalesOrder #cboOrderYear').val();

	var data = "requestType=update";
	
	var arrHeader 	= "{";
							arrHeader += '"serialNo":"'+serialNo+'",' ;
							arrHeader += '"serialYear":"'+serialYear+'",' ;
							arrHeader += '"reqNo":"'+reqNo+'",' ;
							arrHeader += '"reqYear":"'+reqYear+'",' ;
							arrHeader += '"orderNo":"'+orderNo+'",' ;
							arrHeader += '"orderYear":'+orderYear+'';
							
		arrHeader  += "}";
		
	$('#frmOpenSalesOrder #tblSalesOrders .open:checked').each(function(){
		var salesOrder	= $(this).parent().parent().find('.salesOrder').attr('id');
			arrDetails += "{";
			arrDetails += '"orderNo":"'+ orderNo +'",' ;
			arrDetails += '"orderYear":"'+ orderYear +'",' ;
			arrDetails += '"salesOrder":'+salesOrder+'';
			arrDetails += "},";

	});
	
	arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
	var arrDetails	= '['+arrDetails+']';
	
	data		   +="&arrHeader="+arrHeader;
	data		   +="&arrDetails="+arrDetails;

	var url   = "controller.php?q=1160";	
	$.ajax({
			url:url,
			dataType:'json',
			type:'post',
			data:data,
			async:false,
			success:function(json){
				$('#frmOpenSalesOrder #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				
				if(json.type=='pass')
				{
					hideWaiting();
					$('#frmOpenSalesOrder #txtSerialNo').val(json.serialNo);
					$('#frmOpenSalesOrder #txtYear').val(json.serialYear);
					if(json.savePerm==1){
						$('#frmOpenSalesOrder #butSave').show();
					}
					else{
						$('#frmOpenSalesOrder #butSave').hide();
					}
					if(json.approvePerm==1){
						$('#frmOpenSalesOrder #butConfirm').show();
					}
					else{
						$('#frmOpenSalesOrder #butConfirm').hide();
					}
					return;
				}
				else
				{
					hideWaiting();
				}
			},
			error:function(xhr,status)
			{
				$('#frmOpenSalesOrder #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				hideWaiting();
				return;;
			}
	});
	hideWaiting();	
	
}

function viewReport(type){
	
	if($('#frmOpenSalesOrder #txtSerialNo').val()=="") 
		return;
	var url = "?q=1162&serialNo="+$('#frmOpenSalesOrder #txtSerialNo').val()+"&serialYear="+$('#frmOpenSalesOrder #txtYear').val()+"&mode="+type;
	window.open(url,'openSalesOrders-report.php');
	
	
}

function approve()
{
	var val = $.prompt('Are you sure you want to approve this Sales Order Opening ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
			showWaiting();
			var url = "controller.php?q=1160&"+window.location.search+'&requestType=approve';
			var obj = $.ajax({
				url:url,
				type:'post',
				dataType: "json",  
				data:'',
				async:false,						
				success:function(json){
					$('#frmOpenOrdersRpt #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertR1()",1000);
						window.location.href = window.location.href;
						window.opener.location.reload();
						return;
					}
					 hideWaiting();
				},
				error:function(xhr,status){						
						$('#frmOpenOrdersRpt #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertR1()",3000);
						 hideWaiting();
				}		
			});	
		}			  
	}});
}

function reject()
{
	var val = $.prompt('Are you sure you want to reject this Sales Order Opening ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = "controller.php?q=1160&"+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,						
						success:function(json){
								$('#frmOpenOrdersRpt #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertR2()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmOpenOrdersRpt #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertR2()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}

function loadNewPage()
{
	window.location.href = "?q=1167";
}

function alertR1()
{
	$('#frmOpenOrdersRpt #butRptConfirm').validationEngine('hide');
}
function alertR2()
{
	$('#frmOpenOrdersRpt #butRptReject').validationEngine('hide');
}
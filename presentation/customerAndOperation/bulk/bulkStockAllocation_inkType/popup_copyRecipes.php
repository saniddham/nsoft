<?php
	session_start();
	$backwardseperator 		= "../../../../";
	$mainPath 				= $_SESSION['mainPath'];
	$sessionUserId 			= $_SESSION['userId'];
	$thisFilePath 			=  $_SERVER['PHP_SELF'];
	$locationId				= $_SESSION["CompanyID"];

	include  	"{$backwardseperator}dataAccess/Connector.php";
	
	$sampleNo  		= $_REQUEST['sampleNo'];
	$sampleYear		= $_REQUEST['sampleYear'];
	$revNo			= $_REQUEST['revNo'];
	$combo			= $_REQUEST['combo'];
	$printName		= $_REQUEST['printName'];
	
	$colorId		= $_REQUEST['colorId'];
	$techniqueId	= $_REQUEST['techniqueId'];
	$inkTypeId		= $_REQUEST['inkTypeId'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="css.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table bgcolor="#000000" width="1010" height="702" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">
      <table width="673" border="0" cellspacing="0" cellpadding="0">
        <tr style="visibility:hidden">
          <td width="391" height="20" align="left" valign="middle">&nbsp;</td>
          <td width="282" height="20" align="left" valign="middle"><input  value="<?Php echo $_REQUEST['sampleNo']; ?>"  type="text" name="txtOrderNo" id="txtOrderNo" />
          <input  value="<?Php echo $_REQUEST['sampleYear']; ?>"  type="text" name="txtSampleYear" id="txtSampleYear" /></td>
        </tr>
        <tr>
          <td height="67" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td align="center" style="color:white">&nbsp;</td>
              <td style="font-size:28px">From</td>
              <td align="center" style="color:white">&nbsp;</td>
              <td valign="bottom"></td>
            </tr>
            <tr>
              <td width="34%" align="center" style="color:white">Revision No</td>
              <td width="34%" align="center" style="color:white">Combo</td>
              <td width="34%" align="center" style="color:white">Print</td>
              <td width="44%" valign="bottom"></td>
            </tr>
            <tr>
              <td><select name="cboRevisionNo" size="1" multiple="multiple" id="cboRevisionNo1" style="height:200px;width:200px">
                <?php
				 echo $sql = "SELECT
					trn_sampleinfomations.intRevisionNo
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations.intSampleNo =  '$sampleNo'
				ORDER BY
					trn_sampleinfomations.intRevisionNo ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($revNo==$row['intRevisionNo'])
						echo "<option selected=\"selected\" value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
					else
						echo "<option value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
				}
				  ?>
              </select></td>
              <td><select name="cboCombo" size="1" multiple="multiple" id="cboCombo1"  style="height:200px;width:200px">
                <?php
			if($revNo!='')
			{
				echo $sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strComboName
					FROM trn_sampleinfomations_details
				WHERE
					trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear' and trn_sampleinfomations_details.intRevNo='$revNo'
				ORDER BY
					trn_sampleinfomations_details.strComboName ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($combo==$row['strComboName'])
						echo "<option selected=\"selected\" value=\"".$row['strComboName']."\">".$row['strComboName']."</option>";
					else
						echo "<option value=\"".$row['strComboName']."\">".$row['strComboName']."</option>";
				}
			}
				  ?>
              </select></td>
              <td><select name="cboPrint" size="1" multiple="multiple" id="cboPrint1" style="height:200px;width:200px">
                <?php
			
				$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strPrintName
					FROM trn_sampleinfomations_details
				WHERE
					trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_details.strComboName 		=  '$combo' and trn_sampleinfomations_details.intRevNo='$revNo'
				ORDER BY
					trn_sampleinfomations_details.strPrintName ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($printName==$row['strPrintName'])
						echo "<option selected=\"selected\" value=\"".$row['strPrintName']."\">".$row['strPrintName']."</option>";
					else
						echo "<option value=\"".$row['strPrintName']."\">".$row['strPrintName']."</option>";
				}
				  ?>
              </select></td>
              <td valign="bottom"></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td valign="bottom"></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td style="font-size:28px">To</td>
              <td>&nbsp;</td>
              <td valign="bottom"></td>
            </tr>
            <tr>
              <td align="center" style="color:white">Revision No</td>
              <td align="center" style="color:white">Combo</td>
              <td align="center" style="color:white">Print</td>
              <td valign="bottom"></td>
            </tr>
            <tr>
              <td><select name="cboRevisionNo2" size="1" multiple="multiple" id="cboRevisionNo2" style="height:200px;width:200px">
                <?php
				 echo $sql = "SELECT
					trn_sampleinfomations.intRevisionNo
				FROM trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations.intSampleNo =  '$sampleNo'
				ORDER BY
					trn_sampleinfomations.intRevisionNo ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($revNo==$row['intRevisionNo'])
						echo "<option selected=\"selected\" value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
					else
						echo "<option value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
				}
				  ?>
                </select></td>
              <td><select name="cboCombo2" size="1" multiple="multiple" id="cboCombo2"  style="height:200px;width:200px">
                <?php
			if($revNo!='')
			{
				echo $sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strComboName
					FROM trn_sampleinfomations_details
				WHERE
					trn_sampleinfomations_details.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear' and trn_sampleinfomations_details.intRevNo='$revNo'
				ORDER BY
					trn_sampleinfomations_details.strComboName ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($combo==$row['strComboName'])
						echo "<option selected=\"selected\" value=\"".$row['strComboName']."\">".$row['strComboName']."</option>";
					else
						echo "<option value=\"".$row['strComboName']."\">".$row['strComboName']."</option>";
				}
			}
				  ?>
                </select></td>
              <td><select name="cboPrint2" size="1" multiple="multiple" id="cboPrint2" style="height:200px;width:200px">
                <?php
			
				$sql = "SELECT DISTINCT
					trn_sampleinfomations_details.strPrintName
					FROM trn_sampleinfomations_details
				WHERE
					trn_sampleinfomations_details.intSampleNo 	=  '$sampleNo' AND
					trn_sampleinfomations_details.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_details.strComboName 		=  '$combo' and trn_sampleinfomations_details.intRevNo='$revNo'
				ORDER BY
					trn_sampleinfomations_details.strPrintName ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($printName==$row['strPrintName'])
						echo "<option selected=\"selected\" value=\"".$row['strPrintName']."\">".$row['strPrintName']."</option>";
					else
						echo "<option value=\"".$row['strPrintName']."\">".$row['strPrintName']."</option>";
				}
				  ?>
                </select></td>
              <td valign="bottom"></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td valign="bottom"></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td valign="bottom"></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><div id="butAddItemToGrid3" style="width:50px;height:35px" class="button green" >COPY</div></td>
              <td>&nbsp;</td>
              <td valign="bottom"></td>
            </tr>
          </table></td>
        </tr>
      </table>    </td>
  </tr>
  <tr>
    <td height="345"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="3"><div style="width:100%;height:100px;color:#FFF"><div id="msg" align="center" style="width:100%;height:30px;color:#3F6"></div>
        </div></td>
      </tr>
      <tr>
        
        <td colspan="3" align="center"><div style="height:28px" align="right" id="butClose_popup" class="button pink" >CLOSE</div></td>
        
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="19">&nbsp;</td>
  </tr>
</table>
</body>
</html>
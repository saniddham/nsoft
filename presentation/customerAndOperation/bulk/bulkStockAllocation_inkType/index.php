<?php
	session_start();
 	
	$backwardseperator 		=	"../../../../";
	$mainPath 				=	$_SESSION['mainPath'];
	$sessionUserId 			=	$_SESSION['userId'];
	$thisFilePath 			=	$_SERVER['PHP_SELF'];
	$locationId				=	$_SESSION["CompanyID"];

	include  	"{$backwardseperator}dataAccess/permisionCheck_touch.inc";
	
	$graphicNo			=	$_REQUEST['graphicNo'];
	$orderNo  			=	$_REQUEST['orderNo'];
	$orderYear			=	$_REQUEST['orderYear'];
	$graphicNoSelected	=	$_REQUEST['graphicNoSelected'];
	$salesOrderId		=	$_REQUEST['salesOrderId'];
	$qty				=	$_REQUEST['qty'];
	
	$sql    		=	loadHeader($graphicNoSelected,$orderNo,$orderYear);
	$result 		=	$db->RunQuery($sql);
	while($row	=	mysqli_fetch_array($result))
	{
		$salesOrderQty 	=	$row['orderQty'];	
    	$SalesOrderNo 	=	$row['strSalesOrderNo']."/".$row['strName'];	
		$sampleNo  		=	$row['intSampleNo'];
		$sampleYear		=	$row['intSampleYear'];
		$revNo			=	$row['intRevisionNo'];
		$combo			=	$row['strCombo'];
		$printName		=	$row['strPrintName'];
	}
	
	
	$mode = 'first';
	if(isset($graphicNo) && isset($graphicNoSelected) && isset($orderNo) && isset($orderYear))
	{
		$mode = 'last';
	}
	else if(isset($graphicNo) && isset($graphicNoSelected) && isset($orderYear))
	{
		$mode = "third";	
	}
	else if(isset($graphicNo))
	{
		$mode = "second";	
	}
	
	//$mode='third';
	//echo  $mode;
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Color Room Item Allocations</title>

	<link href="css.css" rel="stylesheet" />
	<!-- jQuery & jQuery UI + theme (required) -->
	<link href="other/jquery-ui.css" rel="stylesheet">
	<script src="other/jquery.js"></script>
	<script src="other/jquery-ui.min.js"></script>

	<!-- keyboard widget css & script (required) -->
	<link href="css/keyboard.css" rel="stylesheet">
	<script src="js/jquery.keyboard.js"></script>

	<!-- keyboard extensions (optional) -->
	<script src="js/jquery.mousewheel.js"></script>
	<script src="js/jquery.keyboard.extension-typing.js"></script>
	<script src="js/jquery.keyboard.extension-autocomplete.js"></script>

	<!-- demo only -->
	<link href="demo/demo.css" rel="stylesheet">
	<script src="demo/demo.js"></script>
	<script src="other/jquery.jatt.min.js"></script> <!-- tooltips -->

	<!-- theme switcher -->
	<script src="other/themeswitchertool.htm"></script>

	<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
    <script type="application/javascript" src="index.js"></script>
	<script>
		$(function(){
			$('#keyboard').keyboard();
		});
		var projectName = '/<?php echo $_SESSION["projectName"]; ?>' ;
	</script>
  
    <script type="application/javascript" src="bulkStockAllocation_inkType-js.js"></script>
</head>

<body style="background-color:#483D8B">
<table width="1024" height="199" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="135" height="48" class="label" >&nbsp;ORDER YEAR</td>
      <td width="149"><select name="cboOrderYear" class="down"   id="cboOrderYear" >
      	<?php
				    $d = date('Y');
					if($orderYear=='')
						$orderYear = $d;
				  	for($d;$d>=2012;$d--)
					{
						if($d==$orderYear)
							echo "<option selected=\"selected\" id=\"$d\" >$d</option>";	
						else
							echo "<option id=\"$d\" >$d</option>";	
					}
		?>
      </select></td>
      <td width="136" class="label">GRAPHIC NO</td>
      <td width="179"><input type="text" style="height:50px;margin-top:5px; width:155px" class="txtGraphicNo" id="num"   role="textbox" value="<?php echo $graphicNo; ?>" /></td>
      <td width="5" class="label"> </td>
      <td width="62"> </td>
      <td width="110">&nbsp;<a id="butSearch" class="orange button search" style="height:35px;margin-top:5px">Search</a></td>
      <td>&nbsp;<a id="butSapmle" href="../../sample/colorRecipes/touchScreen/index.php" class="white button" style="height:35px;margin-top:5px">SAMPLE</a></td>
      <td align="right"><a id="butSearch" href="logout.php" class="pink button logout" style="height:35px;margin-top:5px; width:65px">LOG OUT</a></td> 
      <td width="1"></td>
    </tr>
	<?php
    if(($mode=='second') ||($mode=='third'))
    {
    ?>
    <tr>
      <td height="17" colspan="10">
      <table width="100%">
      <tr>
      <td>
      <table style="margin-top:150px" align="center" width="150" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center">Graphic Nos List</td>
        </tr>
        <tr style="margin-top:250px">
          <td align="center" valign="middle"><select name="cboGraphicNo" size="1" multiple="multiple" id="cboGraphicNo" style="height:200px;width:300px">
            <?php
				    $sql = "SELECT DISTINCT
							trn_orderdetails.strGraphicNo
							FROM `trn_orderdetails`
							WHERE
							trn_orderdetails.intOrderYear = '$orderYear' AND
							trn_orderdetails.strGraphicNo LIKE '%$graphicNo%'
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($graphicNoSelected==$row['strGraphicNo'])
						echo "<option selected=\"selected\" value=\"".$row['strGraphicNo']."\">".$row['strGraphicNo']."</option>";
					else
						echo "<option value=\"".$row['strGraphicNo']."\">".$row['strGraphicNo']."</option>";
				}
				  ?>
          </select></td>
        </tr>
      </table>
      </td>
      <td width="15%"> </td>
      <td align="center" valign="middle">
      <?php
      if($mode=='third'){
	  ?>
      <table style="margin-top:150px" align="center" width="150" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center">Order Nos List</td>
        </tr>
        <tr style="margin-top:250px">
          <td><select name="cboOrderNo" size="1" multiple="multiple" id="cboOrderNo" style="height:200px;width:300px">
            <?php
				    $sql = "SELECT DISTINCT
						trn_orderdetails.intOrderNo,
						trn_orderdetails.intOrderYear
						FROM `trn_orderdetails`
						WHERE
						trn_orderdetails.strGraphicNo = '$graphicNoSelected' AND
						trn_orderdetails.intOrderYear = '$orderYear'
						ORDER BY
						trn_orderdetails.intOrderYear ASC,
						trn_orderdetails.intOrderNo ASC
				";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($orderNo==$row['intOrderNo'])
						echo "<option selected=\"selected\" value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
					else
						echo "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
				}
						echo "<option value=\"All\">All</option>";
				  ?>
          </select></td>
        </tr>
      </table>
      <?php
	  }
	  ?>
      </td>
      </tr>
      </table>
      </td>
    </tr>
    <?PHp
		die();
		}
	?>
    
    
    <?php
	if($mode=='last'){
  	?>
    
    <tr>
      <td height="99" colspan="9" align="left" valign="bottom"><table align="left" width="43%" border="0" cellspacing="0" cellpadding="0" >
            <tr>
              <td width="22%" >Graphic No</td>
              <td width="1%" >:</td>
              <td width="35%" id="txtSampNo1"><?php echo $graphicNoSelected; ?></td>
                  <td width="42%" rowspan="5" class="normalfnt"><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:184px;height:108px;overflow:hidden" >
            <?php
			
			if($sampleYear!='' && $sampleNo!='' && $revNo!='')
			{
				echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"../../../../documents/sampleinfo/samplePictures/$sampleNo-$sampleYear-$revNo-".substr($printName,6,1).".png\" />";	
			}
			 ?>
          </div></td>
              </tr></table></td>
    </tr>
    
  <tr style="margin-top:250px">
      <td colspan="9"><table class="tableBorder_allRound"  style="background:#FFF;margin-top:5px" width="100%" border="0" cellspacing="1" cellpadding="0" id="tblParent">
        <tr style="color:#FF0">
          <td class="tableBorder_allRound" width="9%" align="center" bgcolor="#333333">COLOR</td>
          <td class="tableBorder_allRound" width="18%" align="center" bgcolor="#333333">TECHNIQUE</td>
          <td class="tableBorder_allRound" width="19%" align="center" bgcolor="#333333">INK TYPE</td>
          <td class="tableBorder_allRound" width="7%" align="center" bgcolor="#333333">CON PC</td>
           <td class="tableBorder_allRound" width="10%" align="center" bgcolor="#333333">QTY</td>
          <td class="tableBorder_allRound" width="6%" align="center" bgcolor="#333333">ITEM</td>
          <td class="tableBorder_allRound" width="10%" align="center" bgcolor="#333333">ORDERS LIST</td>
          <td class="tableBorder_allRound" width="9%" align="center" bgcolor="#333333">STOCK QTY</td>
          <td class="tableBorder_allRound" width="12%" align="center" bgcolor="#333333" style="display:none">Orders List</td>
       
        </tr>
        <?Php
	 $sql = "SELECT    
						intColorId,
						colorName,
						intInkType,
						inkTypeName,
						intTechnique,
						technique,
						itemList,
						weightList,
						dblColorWeight ,
						GROUP_CONCAT(ordersList) as ordersList 
 				FROM (SELECT	 
								trn_sampleinfomations_details_technical.intNoOfShots, 
								trn_sampleinfomations_details_technical.dblColorWeight, 
								trn_sampleinfomations_details_technical.intColorId, 
								trn_sampleinfomations_details_technical.intInkTypeId as intInkType, 
								mst_inktypes.strName AS inkTypeName,  
								mst_colors.strName AS colorName, 
								trn_sampleinfomations_details.intTechniqueId as intTechnique,  
								mst_techniques.strName AS technique, 
								GROUP_CONCAT(trn_sample_color_recipes.intItem ORDER BY trn_sample_color_recipes.intItem ASC) as itemList,
								GROUP_CONCAT(trn_sample_color_recipes.dblWeight ORDER BY trn_sample_color_recipes.intItem ASC) as weightList,
								GROUP_CONCAT(DISTINCT intOrderYear,'/',intOrderNo,'/',intSalesOrderId) as ordersList,
								trn_sample_color_recipes.dblWeight  
							FROM 
							trn_sampleinfomations_details_technical 
							INNER JOIN mst_inktypes 
								ON mst_inktypes.intId = trn_sampleinfomations_details_technical.intInkTypeId  
							INNER JOIN mst_colors 
								ON mst_colors.intId = trn_sampleinfomations_details_technical.intColorId  
							INNER JOIN trn_sampleinfomations_details 
								ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_details_technical.intSampleNo  
									AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear  
									AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_details_technical.intRevNo  
									AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_details_technical.strPrintName  
									AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_details_technical.strComboName  
									AND trn_sampleinfomations_details.intColorId = trn_sampleinfomations_details_technical.intColorId  
							INNER JOIN mst_techniques 
								ON mst_techniques.intId = trn_sampleinfomations_details.intTechniqueId  
							INNER JOIN trn_sample_color_recipes 
								ON trn_sampleinfomations_details_technical.intSampleNo = trn_sample_color_recipes.intSampleNo  
									AND trn_sampleinfomations_details_technical.intSampleYear = trn_sample_color_recipes.intSampleYear  
									AND trn_sampleinfomations_details_technical.intRevNo = trn_sample_color_recipes.intRevisionNo  
									AND trn_sampleinfomations_details_technical.strComboName = trn_sample_color_recipes.strCombo  
									AND trn_sampleinfomations_details_technical.strPrintName = trn_sample_color_recipes.strPrintName  
									AND trn_sampleinfomations_details_technical.intColorId = trn_sample_color_recipes.intColorId  
									AND trn_sampleinfomations_details_technical.intInkTypeId = trn_sample_color_recipes.intInkTypeId  
						
							LEFT JOIN trn_sample_color_recipes_approve 
							ON trn_sample_color_recipes.intSampleNo = trn_sample_color_recipes_approve.intSampleNo 
							AND trn_sample_color_recipes.intSampleYear = trn_sample_color_recipes_approve.intSampleYear 
							AND trn_sample_color_recipes.intRevisionNo = trn_sample_color_recipes_approve.intRevisionNo 
							AND trn_sample_color_recipes.strCombo = trn_sample_color_recipes_approve.strCombo 
							AND trn_sample_color_recipes.strPrintName = trn_sample_color_recipes_approve.strPrintName
						
							INNER JOIN trn_orderdetails 
								ON trn_sampleinfomations_details_technical.intSampleNo = trn_orderdetails.intSampleNo  
									AND trn_sampleinfomations_details_technical.intSampleYear = trn_orderdetails.intSampleYear  
									AND trn_sampleinfomations_details_technical.intRevNo = trn_orderdetails.intRevisionNo  
									AND trn_sampleinfomations_details_technical.strPrintName = trn_orderdetails.strPrintName  
									AND trn_sampleinfomations_details_technical.strComboName = trn_orderdetails.strCombo    
							WHERE  
								trn_orderdetails.strGraphicNo = '$graphicNoSelected' AND 
								trn_orderdetails.intOrderYear = '$orderYear' AND 
								trn_sample_color_recipes_approve.intApproved = '1'";
								
								
								if(($orderNo!='') && (($orderNo!='All'))){
									$sql.=" 
									AND
									trn_orderdetails.intOrderNo = '$orderNo' ";
								}
							$sql .="
							GROUP BY 
									trn_sampleinfomations_details_technical.intColorId, trn_sampleinfomations_details_technical.intInkTypeId,  
									trn_sample_color_recipes.intTechniqueId,
									trn_orderdetails.intOrderNo, trn_orderdetails.intOrderYear, trn_orderdetails.intSalesOrderId 
							ORDER BY 
									trn_orderdetails.strGraphicNo ASC, 
									trn_sample_color_recipes.intColorId ASC, 
									trn_sample_color_recipes.intInkTypeId ASC,
									trn_sample_color_recipes.intTechniqueId ASC,
 									trn_sample_color_recipes.intItem ASC 
									 )as tb1 
						 GROUP BY intColorId, colorName, intInkType, inkTypeName, intTechnique, technique, itemList, weightList  
						";
				  //  echo $sql;		
 				$result = $db->RunQuery($sql);
				$i=0;
				$j=0;
				while($row=mysqli_fetch_array($result))
				{
					
 					$ordersList		=	$row['ordersList'];
 					$colorId		=	$row['intColorId'];
					$colorName		=	$row['colorName'];
					$techniqueId	=	$row['intTechnique'];
					$techniqueName		=	$row['technique'];
					$inkTypeId		=	$row['intInkType'];
					$inkTypeName	=	$row['inkTypeName'];
					$conPc			=	$row['dblColorWeight'];
					$item			=	$row['intItem'];
					$weight			=	$row['dblWeight'];
					$expConsump		=	$qty*$conPc;
					$Qty 			=	$qty*$conPc;
  					
					$h++;
 					$stockBal		=	getOrderWiseInkStockBal($locationId,$ordersList,$colorId,$techniqueId,$inkTypeId);		
					
						?>
                            <tr>
                                  <td bgcolor="#333333" id="<?Php echo $colorId; ?>"><?php echo $colorName ?></td>
                                  <td bgcolor="#333333" id="<?php echo $techniqueId; ?>"><?php echo $techniqueName ?></td>
                                  <td bgcolor="#333333" id="<?php echo $inkTypeId; ?>"><?php echo $inkTypeName ?></td>
                                  <td align="right" bgcolor="#333333" class="normalfntMid" id="<?php echo $conPc; ?>"><?php echo $conPc ?></td>
                                   <td align="center" bgcolor="#333333" class="normalfntMid"><input id="txtProdQty" class="hex txtQty ui-keyboard-input ui-widget-content ui-corner-all" type="text" value="<?php echo $Qty; ?>" role="textbox" aria-haspopup="true" style="height:30px;margin-top:5px; width:95px; alignment-adjust:before-edge; text-align:right"></td> 
                                   <td style="height:20px"  bgcolor="#333333" class="normalfntMid"><a  id="butAdd" class="green button " style="height:16px;margin:2px 2px 2px 2px;">Add</a></td>
                                   <td align="right" bgcolor="#333333" class="normalfntRight" id="orderList"><a  id="butView" class="green button " style="height:16px;margin:2px 2px 2px 2px;">View</a></td>
                                  <td align="right" bgcolor="#333333" class="normalfntRight" id="stockBal"><?php echo $stockBal; ?></td>
                                  <td bgcolor="#333333" id="<?Php echo $ordersList; ?>" style="display:none"><?php echo $ordersList ?></td>
                             </tr>
						<?php
                        }
  				
				
					
 		?>
        <?php
 						
   			//	}
		?>
    </table></td>
    </tr>
        <?php
				}
		?>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td width="125">&nbsp;</td>
      <td width="122">&nbsp;</td>
    </tr>
  </table>
			
		</div>
		<p>
		  
</p>
	
</body>
</html>
<?php
function loadHeader($graphicNoSelected,$orderNo,$orderYear){
	
	$sql="SELECT 
			trn_orderdetails.strSalesOrderNo,
			mst_part.strName,
			trn_orderdetails.intSampleNo,
			trn_orderdetails.intSampleYear,
			trn_orderdetails.strCombo,
			trn_orderdetails.strPrintName,
			trn_orderdetails.intRevisionNo,
			sum(trn_orderdetails.intQty) as orderQty 
			FROM `trn_orderdetails`
			INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
			WHERE
			trn_orderdetails.strGraphicNo = '$graphicNoSelected' AND
			trn_orderdetails.intOrderYear = '$orderYear' ";
			if($orderNo!=''){
			$sql.=" AND
			trn_orderdetails.intOrderNo = '$orderNo'
			";
			}
			
			return  $sql;
 }

function getOrderWiseInkStockBal($locationId,$ordersList,$colorId,$techniqueId,$inkTypeId){
		global $db;
	
	    	    $sql1="select IFNULL(sum(IFNULL(qty,0)),0) as stkQty from(SELECT
			 (IFNULL(ware_sub_color_stocktransactions_bulk.dblQty,0)) as qty ,
			concat(intOrderYear,'/',intOrderNo,'/',intSalesOrderId) as orders
			FROM
			ware_sub_color_stocktransactions_bulk
			WHERE
			ware_sub_color_stocktransactions_bulk.intLocationId = '$locationId' AND
			ware_sub_color_stocktransactions_bulk.intColorId = '$colorId' AND
			ware_sub_color_stocktransactions_bulk.intTechnique = '$techniqueId' AND
			ware_sub_color_stocktransactions_bulk.intInkType = '$inkTypeId'  
 			HAVING orders IN ('$ordersList')) as tb1";
	
 
	$result1 = $db->RunQuery($sql1);
	$row1=mysqli_fetch_array($result1);
	$sumQty=$row1['stkQty'];
	return $sumQty;
	
}


?>

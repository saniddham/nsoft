// JavaScript Document

var ordersList;
 var colorId;
var techniqueId;
var inkTypeId;
var inkRow;

$(document).ready(function(){
	$('#butSearch').live('click',searchDetails);
	$('#butAdd').live('click',viewPopup);//viewPopup_comments
	$('#butView').live('click',viewOrdersPopup);//viewPopup_comments
	$('#butViewDetails').live('click',viewPopup_comments);//viewPopup_comments
	//$('#cboItemTypeMain').live('change',loadItems);
	//$('#butClose_popup').live('click','hidePopup');
	$('#cboItemTypeMain').live('change',loadSubCategory);
	$('#cboItemTypeMain2').live('change',loadSubCategory2);
	
	$('#cboItemTypeSub').live('change',loadItems);
	$('#butSearchItem').live('click',searchItems);
	$('#butSearchItem2').live('click',searchItems2);
	
	$('#butAddItemToGrid').live('click',addItemToGrid);
	$('#butAddItemToGrid2').live('click',addItemToGrid2);
	$('#butAddItemToGrid3').live('click',addItemToGrid3);
	
	$('#butSave').live('click',saveDetails);
	$('#butCal').live('click',cal);
	
	$('#removeRow').live('click',removeRow);
	$('#butAddUnderCoat').live('click',addUndercoat);
	$('.txtShots').live('blur',updateShots);
	$('#butAddAntiMigration').live('click',butAddAntiMigration);
	//butViewAddItem
	$('#butViewAddItem').live('click',butViewAddItem);
	$('#butViewCopyItem').live('click',butViewCopyItem);
	
	
	
	
});

function butViewCopyItem()
{
	showWaiting();
	colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
	techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
	inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
	
	$('#divBackgroundImg').css('left','10px');
	$('#divBackgroundImg').css('top','10px');
	
	var url = "popup_copyRecipes.php";
		url +="?sampleYear="+$('#cboOrderYear').val();
		url +="&sampleNo="+$('.txtGraphicNo').val();
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
		url +="&colorId="+colorId;
		url +="&techniqueId="+techniqueId;
		url +="&inkTypeId="+inkTypeId;
	//alert(url);
	$('#divBackgroundImg').load(url,function(){
		$('#butClose_popup').click(function(){
			hideWaiting();
		});	
		$('#txtSearchItem').keyboard();
		//$('#txtQty').addClass('hex txtGraphicNo ui-keyboard-input ui-widget-content ui-corner-all');
		$('#txtQty').keyboard();
		
		
	});
}
function butViewAddItem()
{
	showWaiting();
	colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
	techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
	inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
	
	$('#divBackgroundImg').css('left','10px');
	$('#divBackgroundImg').css('top','10px');
	
	var url = "popup_addItems.php";
		url +="?sampleYear="+$('#cboOrderYear').val();
		url +="&sampleNo="+$('.txtGraphicNo').val();
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
		url +="&colorId="+colorId;
		url +="&techniqueId="+techniqueId;
		url +="&inkTypeId="+inkTypeId;
	//alert(url);
	$('#divBackgroundImg').load(url,function(){
		$('#butClose_popup').click(function(){
			hideWaiting();
		});	
		$('#txtSearchItem').keyboard();
		//$('#txtQty').addClass('hex txtOrderNo ui-keyboard-input ui-widget-content ui-corner-all');
		$('#txtQty').keyboard();
		
		
	});
}
function butAddAntiMigration()
{
	var url = "bulkStockAllocation_inkType-db-set.php?requestType=butAddAntiMigration";
		url +="&sampleYear="+$('#cboOrderYear').val();
		url +="&sampleNo="+$('.txtOrderNo').val();
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
	$.ajax({url:url,async:false});	
	document.location.href = document.location.href;
}
function updateShots()
{
	colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
	techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
	inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
	
	var url = "bulkStockAllocation_inkType-db-set.php?requestType=saveShots";
		url +="&sampleYear="+$('#cboOrderYear').val();
		url +="&sampleNo="+$('.txtOrderNo').val();
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
		url +="&colorId="+colorId;
		url +="&techniqueId="+techniqueId;
		url +="&inkTypeId="+inkTypeId;
		url +="&shots="+$(this).val();
	$.ajax({url:url,async:false});
}
function addUndercoat()
{
	showWaiting();
	colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
	techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
	inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
	
	$('#divBackgroundImg').css('left','10px');
	$('#divBackgroundImg').css('top','10px');
	
	var url 	= "bulkStockAllocation_inkType-db-set.php?requestType=addNewUndercoat";
		url +="&sampleYear="+$('#cboOrderYear').val();
		url +="&sampleNo="+$('.txtOrderNo').val();
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
		url +="&colorId="+colorId;
		url +="&techniqueId="+techniqueId;
		url +="&inkTypeId="+inkTypeId;
		
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				document.location.href = document.location.href;
	}
	});		
}


function removeRow()
{
	var id 				= $(this).parent().attr('id');
	var sampleNo 		= $('#txtSampNo').text();
	var sampleYear 		= $('#txtSampleYear').text();
	var revNo 			= $('#txtRevNo').text();
	var combo 			= $('#txtCombo').text();
	var printName 		= $('#txtPrintName').text();
	var obj				= this;
	
	var url = "bulkStockAllocation_inkType-db-set.php?requestType=RemoveItem";
		$.ajax({
		url:url,
		async:false,
		dataType:'json',
		data:"&id="+id+"&ordersList="+ordersList+"&colorId="+colorId+"&techniqueId="+techniqueId+"&inkTypeId="+inkTypeId,
		success:function(json){
					
			if(json.type=='fail')
			{
				$('#msg').html(json.msg);	
			}
			else
			{
				$('#msg').html('');
				$(obj).parent().remove();
			}
		}
	});	
		
}
function cal()
{
	var y =$(this).parent().parent().find('.txtQty').val();
	var x = $(this).parent().parent().find('.txtQty2').val();
	
	if(y=='')
		y = 0;
	if(x=='')
		x=0;
	var stock=parseFloat($(this).parent().parent().find('.txtQty6').val())-parseFloat(y);
	$(this).parent().parent().find('.txtQty2').val(parseFloat(x)+parseFloat(y));
	$(this).parent().parent().find('.txtQty').val('');
	$(this).parent().parent().find('.txtQty6').val(stock);
	$(this).parent().parent().find('.txtQty5').val(parseFloat($(this).parent().parent().find('.txtQty5').val())+parseFloat(y));
	

}
function addItemToGrid()
{
	//alert(1);
 	var itemName 		= $('#cboItemList option:selected').text();
	var id 				= $('#cboItemList').val();
	var stockBal		= 0;
	var sampleNo 		= $('#txtSampNo').text();
	var sampleYear 		= $('#txtSampleYear').text();
	var revNo 			= $('#txtRevNo').text();
	var combo 			= $('#txtCombo').text();
	var printName 		= $('#txtPrintName').text();
 	
	var saveItemStatus  = true;
	if(id == null)
		id = '';
	
	if(id!='')
	{
		var url = "bulkStockAllocation_inkType-db-get.php?requestType=getStockBalance&id="+id+"&ordersList="+ordersList;
			$.ajax({
			url:url,
			async:false,
			dataType:'json',
			success:function(json){
						
					stockBal =json.stockBal;
					extraQty =json.extraQty;
			}
		});		  

		var url = "bulkStockAllocation_inkType-db-set.php?requestType=saveItem";
			$.ajax({
			url:url,
			async:false,
			dataType:'json',
			data:"&id="+id+"&ordersList="+ordersList+"&colorId="+colorId+"&techniqueId="+techniqueId+"&inkTypeId="+inkTypeId,
			success:function(json){
						
				if(json.type=='fail')
				{
					$('#msg').html(json.msg);
					saveItemStatus = false;
				}
				else
				{
					$('#msg').html('');
				}
			}
		});	
	
		if(saveItemStatus)
		{	
			$('#tblPopUpGrid').append('<tr id="'+id+'"><td>&nbsp;</td><td id="removeRow" align="center" bgcolor="#CCCCCC" style="color:#F00">DEL</td><td style="color:black" bgcolor="#CCCCCC">'+itemName+'</td> <td width="8%" bgcolor="#CCCCCC"><input disabled="disabled" value="0" style="width:90px;height:50px; text-align:right" class="txtQty1" type="text" name="txtQty1" id="txtQty1" /></td><td width="7%" bgcolor="#CCCCCC"><input disabled="disabled" value="0" style="width:90px;height:50px; text-align:right" class="txtQty4" type="text" name="txtQty4" id="txtQty4" /></td><td width="7%" bgcolor="#CCCCCC"><input disabled="disabled" value="0" style="width:90px;height:50px; text-align:right" class="txtQty5" type="text" name="txtQty5" id="txtQty5" /></td><td width="10%" bgcolor="#CCCCCC"><input disabled="disabled" value="0" style="width:90px;height:50px; text-align:right" class="txtQty2" type="text" name="txtQty2" id="txtQty2" /></td><td width="10%" bgcolor="#CCCCCC"><input value="" style="width:90px;height:50px; text-align:right" class="hex txtQty ui-keyboard-input ui-widget-content ui-corner-all" type="text"  id="txtQty"  aria-haspopup="true" /></td><td width="15%" bgcolor="#CCCCCC"><div id="butCal" style="width:40px;height:35px" class="button green" >+</div></td><td width="6%" bgcolor="#CCCCCC"><input disabled="disabled" value="'+stockBal+'" style="width:90px;height:50px; text-align:right" class="txtQty6" type="text" name="txtQty6" id="txtQty6" /></td><td width="6%" bgcolor="#CCCCCC"><input disabled="disabled" value="'+extraQty+'" style="width:90px;height:50px; text-align:right" class="txtQty7" type="text" name="txtQty7" id="txtQty7" /></td></tr>');
			$('.txtQty').keyboard();
		}
	}
}
function addItemToGrid2()
{
	var id 			= $('#cboItemList').val();
	var url = "bulkStockAllocation_inkType-db-set.php?requestType=addNewItem2&mainId="+$('#cboItemTypeMain2').val()+'&subId='+$('#cboItemTypeSub2').val()+'&itemId='+id;
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				if(json.status==1)
					$('#msg').html('SAVED SUCCESSFULLY.');
				else
					$('#msg').html('');
	}
	});
}

function addItemToGrid3()
{
	var sampleNo		= $('#txtOrderNo').val();
	var sampleYear		= $('#txtSampleYear').val();
	
	var rev1 			= $('#cboRevisionNo1').val();
	var combo1 			= $('#cboCombo1').val();
	var print1 			= $('#cboPrint1').val();
	
	var rev2 			= $('#cboRevisionNo2').val();
	var combo2 			= $('#cboCombo2').val();
	var print2 			= $('#cboPrint2').val();
	
	var url 		 = "bulkStockAllocation_inkType-db-set.php?requestType=copyRecipes";
		url			+= "&sampleNo="+sampleNo;
		url			+= "&sampleYear="+sampleYear;
		
		url			+= "&rev1="+rev1;
		url			+= "&rev2="+rev2;
		url			+= "&combo1="+combo1;
		url			+= "&combo2="+combo2;
		url			+= "&print1="+print1;
		url			+= "&print2="+print2;
		
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				if(json.status==1)
					$('#msg').html(json.msg);
				else
					$('#msg').html('');
	}
	});
}

function searchItems()
{
	var url = "bulkStockAllocation_inkType-db-get.php?requestType=searchItems&mainId="+$('#cboItemTypeMain').val()+'&subId='+$('#cboItemTypeSub').val()+'&like='+$('#txtSearchItem').val();
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				
				$('#cboItemList').html(json.itemList);
	}
	});		
}

function searchItems2()
{
	var url = "bulkStockAllocation_inkType-db-get.php?requestType=searchItems2&mainId="+$('#cboItemTypeMain2').val()+'&subId='+$('#cboItemTypeSub2').val()+'&like='+$('#txtSearchItem').val();
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				$('#cboItemList').html(json.itemList);
	}
	});		
}

function loadItems()
{
	$('#txtSearchItem').val('');
	var url = "bulkStockAllocation_inkType-db-get.php?requestType=loadItems&mainId="+$('#cboItemTypeMain').val()+'&subId='+$('#cboItemTypeSub').val();
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				$('#cboItemList').html(json.itemList);
	}
	});	
}
function loadSubCategory()
{
	var url = "bulkStockAllocation_inkType-db-get.php?requestType=loadSubCategoryList&mainId="+$('#cboItemTypeMain').val();
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				$('#cboItemTypeSub').html(json.subList);
				$('#cboItemList').html(json.itemList);
	}
	});	
}

function loadSubCategory2()
{
	var url = "bulkStockAllocation_inkType-db-get.php?requestType=loadSubCategoryList&mainId="+$('#cboItemTypeMain2').val();
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
				$('#cboItemTypeSub2').html(json.subList);
				
	}
	});	
}

function searchDetails()
{
	var orderYear 	= $('#cboOrderYear').val();
	var orderNo 	= $('#num').val();
	document.location.href = 'index.php?orderYear='+orderYear+'&orderNo='+orderNo+'&qty='+$('#txtQty').val();	

}

/*function hidePopup()
{
	alert(1);
}*/

function viewPopup_comments()
{
	showWaiting();
	colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
	techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
	inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
	
	$('#divBackgroundImg').css('left','10px');
	$('#divBackgroundImg').css('top','10px');
	
	var url = "popup_comments.php";
		url +="?sampleYear="+$('#cboOrderYear').val();
		url +="&sampleNo="+$('.txtGraphicNo').val();
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
		url +="&colorId="+colorId;
		url +="&techniqueId="+techniqueId;
		url +="&inkTypeId="+inkTypeId;
	//alert(url);
	$('#divBackgroundImg').load(url,function(){
		$('#butClose_popup').click(function(){
			hideWaiting();
		});	
		$('#txtSearchItem').keyboard();
		//$('#txtQty').addClass('hex txtGraphicNo ui-keyboard-input ui-widget-content ui-corner-all');
		$('#txtQty').keyboard();
		
		
	});
}

function viewPopup()
{
	showWaiting();
	
	ordersList = $(this).parent().parent().find('td:eq(8)').attr('id');
 	colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
	techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
	inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
	qty = $(this).parent().parent().find('#txtProdQty').val();
	inkRow = $(this).parent().parent().closest('tr')[0].sectionRowIndex;
 	
	$('#divBackgroundImg').css('left','10px');
	$('#divBackgroundImg').css('top','10px');
	
	var url = "popup.php";
		url +="?ordersList="+ordersList;
 		url +="&qty="+qty;
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
		url +="&colorId="+colorId;
		url +="&techniqueId="+techniqueId;
		url +="&inkTypeId="+inkTypeId;
	//alert(url);
	$('#divBackgroundImg').load(url,function(){
		$('#butClose_popup').click(function(){
			hideWaiting();
		});	
		$('#txtSearchItem').keyboard();
		//$('#txtQty').addClass('hex txtGraphicNo ui-keyboard-input ui-widget-content ui-corner-all');
		$('#txtQty').keyboard();
		$('.txtQty').keyboard();
		
	});
}

function viewOrdersPopup(){

	showWaiting();
	
	ordersList = $(this).parent().parent().find('td:eq(8)').attr('id');
 	colorId = $(this).parent().parent().find('td:eq(0)').attr('id');
	techniqueId = $(this).parent().parent().find('td:eq(1)').attr('id');
	inkTypeId = $(this).parent().parent().find('td:eq(2)').attr('id');
	qty = $(this).parent().parent().find('#txtProdQty').val();
	inkRow = $(this).parent().parent().closest('tr')[0].sectionRowIndex;
 	
	$('#divBackgroundImg').css('left','10px');
	$('#divBackgroundImg').css('top','10px');
	
	var url = "popupOrderList.php";
		url +="?ordersList="+ordersList;
 		url +="&qty="+qty;
		url +="&revNo="+$('#txtRevNo').text();
		url +="&combo="+URLEncode($('#txtCombo').text());
		url +="&printName="+URLEncode($('#txtPrintName').text());
		url +="&colorId="+colorId;
		url +="&techniqueId="+techniqueId;
		url +="&inkTypeId="+inkTypeId;
	//alert(url);
	$('#divBackgroundImg').load(url,function(){
		$('#butClose_popup').click(function(){
			hideWaiting();
		});	
		$('#txtSearchItem').keyboard();
		//$('#txtQty').addClass('hex txtGraphicNo ui-keyboard-input ui-widget-content ui-corner-all');
		$('#txtQty').keyboard();
		$('.txtQty').keyboard();
		
	});
	
}

function saveDetails(){
	//alert(colorId);
	var arrRecipe = '';
	var data='';
	var i =0;
	
	var orderNo 		= 	$('.txtGraphicNo').val();
	var orderYear 		= 	$('#cboOrderYear').val();
	var salesOrderId = $('#txtSalesOrderID').text();
	var revisionNo 		= 	$('#txtRevNo').text();
	var combo 			=	$('#txtCombo').text();
	var printName 		=	$('#txtPrintName').text();
	var errFlag=1;
	var tot=0;
	var weight=0;
	$('#tblPopUpGrid >tbody>tr').each(function(){
		//var itemId 		= $(this).val();
		
		//if(itemId!='')
		//{
			//var colorId 	= colorId
			//var techniqueId = technique
			//var inkTypeId 	= $(this).parent().parent().parent().parent().parent().parent().find('td:eq(3)').attr('id');
			 weight 		= $(this).find('.txtQty2').val();
			var itemId		= $(this).attr('id');
			if((weight!='') &&(weight!=0))
			{
			arrRecipe +=(i++ == 0?'':',' )+'{"color":"'+colorId+'","techId":"'+techniqueId+'","inkTypeId":"'+inkTypeId+'","weight":"'+weight+'","orderNo":"'+orderNo+'","orderYear":"'+orderYear+'","salesOrderId":"'+salesOrderId+'","revisionNo":"'+revisionNo+'","combo":"'+combo+'","printName":"'+printName+'","itemId":"'+itemId+'"}';
			errFlag=0;
			}
		//}
		tot+=parseFloat(weight);
	});

	document.getElementById('tblParent').rows[inkRow].cells[4].childNodes[0].value=(tot);
	var inkStock=parseFloat(document.getElementById('tblParent').rows[inkRow].cells[7].innerHTML);
	document.getElementById('tblParent').rows[inkRow].cells[7].innerHTML=inkStock+(tot);

		if(errFlag==1){
			alert("Please add WEIGHT to save.");
			return false;
		}

		data  = 'ordersList='+ ordersList;
		data += '&arrRecipe=['+ arrRecipe +']';
		
		
		var url 	= "bulkStockAllocation_inkType-db-set.php?requestType=saveDetails";
		$.ajax({
			url:url,
			data:data,
			async:false,
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
				$('#msg').html(json.msg);
					if(json.type=='pass'){
						$('#butSave').hide();
					}
				},
			error:function(xhr,status){
					
					
				}		
			});
			//hideWaiting();
   }
	

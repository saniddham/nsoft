<?php
	session_start();
 	include "../../../../dataAccess/Connector.php";
	$requestType = $_REQUEST['requestType'];
	$userId		 = $_SESSION["userId"];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	
	
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	if($requestType=='saveDetails')
	{
		$toSave=0;
		$saved=0;
		$ordersList		= $_REQUEST['ordersList'];
		$arrRecipe 		= json_decode($_REQUEST['arrRecipe'], true);
 		
		$totOrderQty =getTotOrderQty($ordersList);
  			
		$arrOrders =explode( ',', $ordersList );
		
		
		$tot=0;
		$totSavedQty = 0;
		for($h=0;$h<count($arrOrders);$h++){
 
 			$string= $arrOrders[$h];
			$arrOrderDetails =explode( '/', $string);
			$orderNo=$arrOrderDetails[1];
			$orderYear=$arrOrderDetails[0];
			$salesOrderId=$arrOrderDetails[2];
 		
			$orderQty =getOrderQty($arrOrders[$h]);
 		
			$revNo 			= $arrRecipe[0]['revisionNo'];
			$combo 			= $arrRecipe[0]['combo'];
			$printName 		= $arrRecipe[0]['printName'];
			$weight 		= $arrRecipe[0]['weight'];
			
			$color 			= $arrRecipe[0]['color'];
			$techId 		= $arrRecipe[0]['techId'];
			$inkTypeId 		= $arrRecipe[0]['inkTypeId'];
			
			$item 		= $arrRecipe[0]['itemId'];
			
			//-------------------------------------
			$sqlM="SELECT
			(IFNULL(max(ware_sub_color_stocktransactions_bulk.intSavedOrder),0)+1)as saveNo 
			FROM `ware_sub_color_stocktransactions_bulk`
			WHERE
			ware_sub_color_stocktransactions_bulk.intOrderNo = '$orderNo' AND
			ware_sub_color_stocktransactions_bulk.intOrderYear = '$orderYear'	";
			$resultM = $db->RunQuery2($sqlM);
			$rowM=mysqli_fetch_array($resultM);
			$saveNo=$rowM['saveNo'];
			//-------------------------------------
		
		$SavedQty=0;
 		foreach($arrRecipe as $x)
		{
			$itemId 		= $x['itemId'];
			$Qty 		= $x['weight'];
			$Qty1 		= $x['weight'];
			$itemWiseSavedqty=0;
			$totOrderQty=round(($orderQty/$totOrderQty)*$x['weight'],4);
			$tot +=$Qty;

			//-----------------
		$resultG = getGrnWiseColorRoom_stockBal($orderNo,$orderYear,$salesOrderId,$location,$itemId);
		while($rowG=mysqli_fetch_array($resultG)){
				$subStores=$rowG['intSubStores'];
				
				if(($Qty>0) && (val($rowG['stockBal'])>0)){
					if($Qty<=val($rowG['stockBal'])){
					$saveQty=$Qty;
					$Qty=0;
					}
					else if($Qty>val($rowG['stockBal'])){
					$saveQty=val($rowG['stockBal']);
					$Qty=$Qty-$saveQty;
					}
					$grnNo=$rowG['intGRNNo'];
					$grnYear=$rowG['intGRNYear'];
					$grnDate=$rowG['dtGRNDate'];
					$grnRate=$rowG['dblGRNRate'];	
					$currency=loadCurrency($grnNo,$grnYear,$item);
					$saveQty=round($saveQty,4);		
					if($saveQty>0){	
					
					$toSave++;
					
					$sqlI = "INSERT INTO `ware_sub_stocktransactions_bulk` (intSavedOrder,`intCompanyId`,`intLocationId`,`intSubStores`,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,`intOrderNo`,`intOrderYear`,`intSalesOrderId`,`intColorId`,`intTechnique`,`intInkType`,intItemId,dblQty,strType,intUser,dtDate) 
						VALUES ('$saveNo','$company','$location','$subStores','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$orderNo','$orderYear','$salesOrderId','".$x['color']."','".$x['techId']."','".$x['inkTypeId']."','$itemId','-$saveQty','INK_ALLOC','$userId',now())";
					$resultI = $db->RunQuery2($sqlI);
					if($resultI){
						$saved+=$resultI;
						$itemWiseSavedqty +=$saveQty; 
						$SavedQty+=$saveQty;
						$totSavedQty+=$saveQty;
					}
					else{
						$rollBackFlag=1;
						$sqlM=$sqlI;
					}
					if(((!$resultI) && ($rollBackFlag!=1))  ){
						$rollBackFlag=1;
						$sqlM=$sqlI;
						$rollBackMsg = "Saving error!";
					}
					
					}
				}
		}//end of while
 		if(round($Qty1,4) > round($itemWiseSavedqty,4)){
				$rollBackFlag=1;
				$itemName=getItemName($itemId);
				$rollBackMsg = "No stock balance to save for item - ".$itemName/*.'-'.$Qty1.'-'.$itemWiseSavedqty*/;
		}
			//-----------------
			//$saved+=$result;
		}//end of for each
		$toSave++;
		$sql = "INSERT INTO `ware_sub_color_stocktransactions_bulk` 
		(`intSavedOrder`,`intCompanyId`,`intLocationId`,`intSubStores`,`intColorId`,`intTechnique`,`intInkType`,`dblQty`,`strType`,
			`intOrderNo`,`intOrderYear`,`intSalesOrderId`,`intUser`,`dtDate`) 
		VALUES ('".$saveNo."','".$company."','".$location."','".$subStores."','".$x['color']."','".$x['techId']."','".$x['inkTypeId']."','".$SavedQty."','INK_ALLOC','".$orderNo."','".$orderYear."','".$salesOrderId."','".$_SESSION["userId"]."',now())";
		$result 	= $db->RunQuery2($sql);
		if($result){
			$saved+=$result;
		}
		else{
			$sqlM=$sql; 
		}
		
			}
 			
 		if(round($totSavedQty,4)!=round($tot,4)){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg/*.$totSavedQty.'--'.$tot*/;
			$response['q'] 			= '';
		}
		else if(($toSave==$saved) && ($totSavedQty>0) &&($rollBackFlag!=1)){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM."/".$toSave."==".$saved;
		}
			
	}
	else if($requestType=="saveItem")
	{
		$rollBackFlag	= 0;
		$itemId 		= $_REQUEST['id'];
		//$graphicNoSelected 		= $_REQUEST['graphicNoSelected'];
 		$ordersList 		= $_REQUEST['ordersList'];
 		$arrOrders =explode( ',', $ordersList );
		//get only one order(bcs all orders samples are same)
		$salesDetails= $arrOrders[0];
		$arrOrderDetails =explode( '/', $salesDetails);
		$orderNo=$arrOrderDetails[1];
		$orderYear=$arrOrderDetails[0];
		$salesOrderId=$arrOrderDetails[2];
		
		$sqlM="SELECT
				trn_orderdetails.intSampleNo,
				trn_orderdetails.intSampleYear,
				trn_orderdetails.intRevisionNo,
				trn_orderdetails.strCombo,
				trn_orderdetails.strPrintName
				FROM `trn_orderdetails`
				WHERE
				trn_orderdetails.intOrderNo = '$orderNo' AND
				trn_orderdetails.intOrderYear = '$orderYear' AND
				trn_orderdetails.intSalesOrderId = '$salesOrderId'";
		$resultM = $db->RunQuery2($sqlM);
		$row=mysqli_fetch_array($resultM);
 	
		$sampleNo 		= trim($row['intSampleNo']);
		$sampleYear 	= trim($row['intSampleYear']);
		$revNo 			= trim($row['intRevisionNo']);
		$combo 			= trim($row['strCombo']);
		$printName 		= trim($row['strPrintName']);
		$colorId 		= $_REQUEST['colorId'];
		$techniqueId 	= $_REQUEST['techniqueId'];
		$inkTypeId 		= $_REQUEST['inkTypeId'];
		
		$sql = "SELECT * FROM trn_sample_color_recipes
				WHERE intSampleNo='$sampleNo' AND intSampleYear='$sampleYear' AND
				intRevisionNo='$revNo' AND strCombo='$combo' AND strPrintName='$printName' AND 
				intColorId='$colorId' AND intTechniqueId='$techniqueId' AND 
				intInkTypeId='$inkTypeId' AND intItem='$itemId' ";
		$result = $db->RunQuery2($sql);
		if(mysqli_num_rows($result)>0)
		{
			$rollBackFlag	= 1;
			$rollBackMsg	= "selected Item already exist.";
		}
		else
		{
			$sqlIns = "INSERT INTO trn_sample_color_recipes 
						(
						intSampleNo, intSampleYear, intRevisionNo, 
						strCombo, strPrintName, intColorId, 
						intTechniqueId, intInkTypeId, intItem, 
						intBulkItem,intAddedByColorRoom, dblWeight, intUser, 
						dtDate
						)
						VALUES
						(
						'$sampleNo', '$sampleYear', '$revNo', 
						'$combo', '$printName', '$colorId', 
						'$techniqueId', '$inkTypeId', '$itemId', 
						1,1, 0, '$userId', 
						now()
						); ";
			$resultIns = $db->RunQuery2($sqlIns);
			if(!$resultIns)
			{
				$rollBackFlag	= 1;
				$rollBackMsg	= $db->errormsg;
			}
		}
		
 		if($rollBackFlag==1)
		{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
		}
		if($rollBackFlag==0)
		{
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
		}
	}
	else if($requestType=='RemoveItem')
	{
		$rollBackFlag	= 0;
		$itemId 		= $_REQUEST['id'];

 		$ordersList 		= $_REQUEST['ordersList'];
 		$arrOrders =explode( ',', $ordersList );
		//get only one order(bcs all orders samples are same)
		$salesDetails= $arrOrders[0];
		$arrOrderDetails =explode( '/', $salesDetails);
		$orderNo=$arrOrderDetails[1];
		$orderYear=$arrOrderDetails[0];
		$salesOrderId=$arrOrderDetails[2];
		
		$sqlM="SELECT
				trn_orderdetails.intSampleNo,
				trn_orderdetails.intSampleYear,
				trn_orderdetails.intRevisionNo,
				trn_orderdetails.strCombo,
				trn_orderdetails.strPrintName
				FROM `trn_orderdetails`
				WHERE
				trn_orderdetails.intOrderNo = '$orderNo' AND
				trn_orderdetails.intOrderYear = '$orderYear' AND
				trn_orderdetails.intSalesOrderId = '$salesOrderId'";
		$resultM = $db->RunQuery2($sqlM);
		$row=mysqli_fetch_array($resultM);
 	
		$sampleNo 		= trim($row['intSampleNo']);
		$sampleYear 	= trim($row['intSampleYear']);
		$revNo 			= trim($row['intRevisionNo']);
		$combo 			= trim($row['strCombo']);
		$printName 		= trim($row['strPrintName']);
	
		$colorId 		= $_REQUEST['colorId'];
		$techniqueId 	= $_REQUEST['techniqueId'];
		$inkTypeId 		= $_REQUEST['inkTypeId'];
		
		$sql = "DELETE FROM trn_sample_color_recipes 
				WHERE
				intSampleNo = '$sampleNo' AND 
				intSampleYear = '$sampleYear' AND 
				intRevisionNo = '$revNo' AND 
				strCombo = '$combo' AND 
				strPrintName = '$printName' AND 
				intColorId = '$colorId' AND 
				intTechniqueId = '$techniqueId' AND 
				intInkTypeId = '$inkTypeId' AND 
				intItem = '$itemId' ;";
		$result = $db->RunQuery2($sql);
		if(!$result)
		{
			$rollBackFlag	= 1;
			$rollBackMsg	= $db->errormsg;
		}
		if($rollBackFlag==1)
		{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
		}
		if($rollBackFlag==0)
		{
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['q'] 			= $sql;
		}
	}
	$db->CloseConnection();		
	echo json_encode($response);

	//--------------------------------------------------------------
	function getGrnWiseColorRoom_stockBal($orderNo,$orderYear,$salesOrderId,$location,$item)
	{
		global $db;
	       $sql = "SELECT
			Sum(ware_sub_stocktransactions_bulk.dblQty) AS stockBal,
			ware_sub_stocktransactions_bulk.intSubStores,
			ware_sub_stocktransactions_bulk.intGRNNo,
			ware_sub_stocktransactions_bulk.intGRNYear,
			ware_sub_stocktransactions_bulk.dtGRNDate,
			ware_sub_stocktransactions_bulk.dblGRNRate
			FROM ware_sub_stocktransactions_bulk 
			INNER JOIN mst_substores ON ware_sub_stocktransactions_bulk.intSubStores = mst_substores.intId
			WHERE 
			ware_sub_stocktransactions_bulk.intOrderNo 		=  '$orderNo' AND
			ware_sub_stocktransactions_bulk.intOrderYear 	=  '$orderYear' AND
			ware_sub_stocktransactions_bulk.intSalesOrderId =  '$salesOrderId' AND
			ware_sub_stocktransactions_bulk.intItemId 		=  '$item' AND
			ware_sub_stocktransactions_bulk.intLocationId 	=  '$location' AND 
			mst_substores.intColorRoom = '1' 
			GROUP BY
			ware_sub_stocktransactions_bulk.intItemId,
			ware_sub_stocktransactions_bulk.intGRNNo,
			ware_sub_stocktransactions_bulk.intGRNYear, 
			ware_sub_stocktransactions_bulk.dblGRNRate 
			ORDER BY
			ware_sub_stocktransactions_bulk.dtGRNDate ASC";

	$result = $db->RunQuery2($sql);
	return $result;	
	}
function loadCurrency($grnNo,$grnYear,$item)
{
	global $db;
	
	if(($grnNo!=0) && ($grnYear!=0)){
	  $sql = "SELECT
			trn_poheader.intCurrency as currency 
			FROM
			ware_grnheader
			Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
			WHERE
			ware_grnheader.intGrnNo =  '$grnNo' AND
			ware_grnheader.intGrnYear =  '$grnYear'";
	}
	else{
	$sql = "SELECT
			mst_item.intCurrency as currency 
			FROM mst_item
			WHERE
			mst_item.intId =  '$item'";
	}

	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
	return $row['currency'];;	
}
	//--------------------------------------------------------------
	function getTotOrderQty($ordersList)
	{
		global $db;
		
		$arrOrders =explode( ',', $ordersList );

		$sql = "select sum(intQty) as qty from (
				SELECT
				trn_orderdetails.intQty,
				concat(trn_orderdetails.intOrderYear,'/',trn_orderdetails.intOrderNo,'/',trn_orderdetails.intSalesOrderId) as orderDt 
				FROM `trn_orderdetails`) as tb1 WHERE  FIND_IN_SET (orderDt,'$ordersList')

				";
		
		$result = $db->RunQuery2($sql);
		$row=mysqli_fetch_array($result);
		$qty = $row['qty'];
		return $qty;
	}
	//--------------------------------------------------------------
	function getOrderQty($order)
	{
		
		$arrOrderDetails =explode( '/', $order);
		$orderNo=$arrOrderDetails[1];
		$orderYear=$arrOrderDetails[0];
		$salesOrderId=$arrOrderDetails[2];
		
		global $db;
		$sql = "SELECT
				trn_orderdetails.intQty
				FROM `trn_orderdetails`
				WHERE
				trn_orderdetails.intOrderNo = '$orderNo' AND
				trn_orderdetails.intOrderYear = '$orderYear' AND
				trn_orderdetails.intSalesOrderId = '$salesOrderId'";
		
		$result = $db->RunQuery2($sql);
		$row=mysqli_fetch_array($result);
		$qty = $row['intQty'];
		return $qty;
	}
//--------------------------------------------------------------
function getItemName($itemId){
	
		global $db;
		$sql = "SELECT
				mst_item.strName
				FROM `mst_item`
				WHERE
				mst_item.intId = '$itemId'
				";
		
		$result = $db->RunQuery2($sql);
		$row=mysqli_fetch_array($result);
		$name = $row['strName'];
		return $name;
}

 
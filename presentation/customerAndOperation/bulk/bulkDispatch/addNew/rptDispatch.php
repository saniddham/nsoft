<?php
session_start();
$backwardseperator = "../../../../../";
$companyId = $_SESSION['CompanyID'];
$locationId = $companyId;
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";

$dispatchNo 			= $_REQUEST['dispatchNo'];
$dispatchYear 			= $_REQUEST['dispatchYear'];

		$sql = "SELECT
		sys_users.strFullName AS enterUser,
		date(ware_stocktransactions_fabric_complete.dtDate) AS disDate,
		mst_customer.strName as customerName,
		mst_customer_locations_header.strName as locationNameC,
		mst_customer.strContactPerson
		FROM
		ware_stocktransactions_fabric_complete
		Inner Join sys_users ON sys_users.intUserId = ware_stocktransactions_fabric_complete.intUser
		Inner Join trn_orderheader ON trn_orderheader.intOrderNo = ware_stocktransactions_fabric_complete.intOrderNo AND trn_orderheader.intOrderYear = ware_stocktransactions_fabric_complete.intOrderYear
		Inner Join mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
		Inner Join mst_customer_locations_header ON mst_customer_locations_header.intId = trn_orderheader.intCustomerLocation
		WHERE
		ware_stocktransactions_fabric_complete.intDocumentNo =  '$dispatchNo' AND
		ware_stocktransactions_fabric_complete.intDocumentYear =  '$dispatchYear' AND
		ware_stocktransactions_fabric_complete.strType IN(  'COMPLETE-DISPATCH','FDAMAGE-DISPATCH','PDAMAGE-DISPATCH','CUTRETURN-DISPATCH')					
					";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$enterUser 		= $row['enterUser'];
					$disDate 		= $row['disDate'];
					$customerName 	= $row['customerName'];
					$locationNameC 	= $row['locationNameC'];
					$strContactPerson 	= $row['strContactPerson'];
				 }
				 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sample Dispatch Report</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src=".../../../../../libraries/jquery/jquery-ui.js"></script>

<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<script type="application/javascript" src="rptSampleDispatch-js.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:223px;
	top:272px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 16px;
	font-weight: bold;
	font-family: "Arial Black", Gadget, sans-serif;
	color: #36F;
}
</style>
</head>

<body>
<form id="frmDispatchReport" name="frmDispatchReport" method="post" action="rptDispatch.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>
<tr>
<td colspan="3"></td>
</tr>
</table>

<div align="center">
<div style="background-color:#FFF" ><strong>SAMPLE </strong><strong>DISPATCH</strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="9" align="center" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
  <tr>
  

  </tr>
  <tr>
    <td width="5%">&nbsp;</td>
    <td width="14%"><span class="normalfnt">Dispatch No</span></td>
    <td width="2%" align="center" valign="middle">:</td>
    <td width="20%"><span class="normalfnt"><?php echo $dispatchNo ?>/<?php echo $dispatchYear ?></span></td>
    <td width="15%">&nbsp;</td>
    <td width="7%" align="center" valign="middle">&nbsp;</td>
    <td width="13%"><span class="normalfnt">Dispatch Date</span></td>
    <td width="2%" align="center" valign="middle">:</td>
    <td width="22%"><span class="normalfnt"><?php echo $disDate ?></span></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Customer</td>
    <td align="center" valign="middle">:</td>
    <td><span class="normalfnt"><?php echo $customerName ?></span></td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td><span class="normalfnt">Dispatch By</span></td>
    <td align="center" valign="middle">:</td>
    <td><span class="normalfnt"><?php echo $enterUser ?></span></td>
    </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Location</td>
    <td align="center" valign="middle">:</td>
    <td><span class="normalfnt"><?php echo $locationNameC ?></span></td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td class="normalfnt">Attention To</td>
    <td align="center" valign="middle">:</td>
    <td><span class="normalfnt"><?php echo $strContactPerson ?></span></td>
    </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt"><table width="100%" class="grid" id="tblDispatch" >
          <tr class="gridHeader">
            <td width="13%" height="22" >Sales Order No</td>
            <td width="10%" >Size</td>
            <td width="10%" >Cut No</td>
            <td width="17%">Fabric Part</td>
            <td width="9%">Good Qty</td>
            <td width="8%">Fabric Damage</td>
            <td width="9%">Production Damage</td>
            <td width="9%">Cut Return</td>
            <td width="9%">Dispatch Qty</td>
            <td width="6%"> Bag No</td>
          </tr>
          <?php

		  $sql = "SELECT
						trn_orderdetails.strSalesOrderNo,
						ware_stocktransactions_fabric_complete.strSize,
						ware_stocktransactions_fabric_complete.intGrade as cutNo,
						mst_part.strName AS partName,
						(
							SELECT
								Sum(ware_stocktransactions_fabric_complete.dblQty *-1) AS goodQty
							FROM
								ware_stocktransactions_fabric_complete AS A
							WHERE
								A.intDocumentNo 	=  ware_stocktransactions_fabric_complete.intDocumentNo AND
								A.intDocumentYear 	=  ware_stocktransactions_fabric_complete.intDocumentYear AND
								A.intOrderNo 		=  ware_stocktransactions_fabric_complete.intOrderNo AND
								A.intOrderYear 		=  ware_stocktransactions_fabric_complete.intOrderYear AND
								A.intSalesOrderId  	=  ware_stocktransactions_fabric_complete.intSalesOrderId AND
								A.strSize 			=  ware_stocktransactions_fabric_complete.strSize AND
								A.intGrade 			=  ware_stocktransactions_fabric_complete.intGrade AND
								A.strType 			=  'COMPLETE-DISPATCH'
						
						)as goodQty,
						(
							SELECT
								Sum(ware_stocktransactions_fabric_complete.dblQty *-1) 
							FROM
								ware_stocktransactions_fabric_complete AS A
							WHERE
								A.intDocumentNo 	=  ware_stocktransactions_fabric_complete.intDocumentNo AND
								A.intDocumentYear 	=  ware_stocktransactions_fabric_complete.intDocumentYear AND
								A.intOrderNo 		=  ware_stocktransactions_fabric_complete.intOrderNo AND
								A.intOrderYear 		=  ware_stocktransactions_fabric_complete.intOrderYear AND
								A.intSalesOrderId  	=  ware_stocktransactions_fabric_complete.intSalesOrderId AND
								A.strSize 			=  ware_stocktransactions_fabric_complete.strSize AND
								A.intGrade 			=  ware_stocktransactions_fabric_complete.intGrade AND
								A.strType 			=  'FDAMAGE-DISPATCH'
						
						)as fdamageQty,
						(
							SELECT
								Sum(ware_stocktransactions_fabric_complete.dblQty *-1) 
							FROM
								ware_stocktransactions_fabric_complete AS A
							WHERE
								A.intDocumentNo 	=  ware_stocktransactions_fabric_complete.intDocumentNo AND
								A.intDocumentYear 	=  ware_stocktransactions_fabric_complete.intDocumentYear AND
								A.intOrderNo 		=  ware_stocktransactions_fabric_complete.intOrderNo AND
								A.intOrderYear 		=  ware_stocktransactions_fabric_complete.intOrderYear AND
								A.intSalesOrderId  	=  ware_stocktransactions_fabric_complete.intSalesOrderId AND
								A.strSize 			=  ware_stocktransactions_fabric_complete.strSize AND
								A.intGrade 			=  ware_stocktransactions_fabric_complete.intGrade AND
								A.strType 			=  'PDAMAGE-DISPATCH'
						
						)as pdamageQty,
						(
							SELECT
								Sum(ware_stocktransactions_fabric_complete.dblQty *-1) 
							FROM
								ware_stocktransactions_fabric_complete AS A
							WHERE
								A.intDocumentNo 	=  ware_stocktransactions_fabric_complete.intDocumentNo AND
								A.intDocumentYear 	=  ware_stocktransactions_fabric_complete.intDocumentYear AND
								A.intOrderNo 		=  ware_stocktransactions_fabric_complete.intOrderNo AND
								A.intOrderYear 		=  ware_stocktransactions_fabric_complete.intOrderYear AND
								A.intSalesOrderId  	=  ware_stocktransactions_fabric_complete.intSalesOrderId AND
								A.strSize 			=  ware_stocktransactions_fabric_complete.strSize AND
								A.intGrade 			=  ware_stocktransactions_fabric_complete.intGrade AND
								A.strType 			=  'CUTRETURN-DISPATCH'
						
						)as cutreturnQty
					FROM
						ware_stocktransactions_fabric_complete
						Inner Join trn_orderdetails ON trn_orderdetails.intOrderNo = ware_stocktransactions_fabric_complete.intOrderNo AND trn_orderdetails.intOrderYear = ware_stocktransactions_fabric_complete.intOrderYear AND trn_orderdetails.intSalesOrderId = ware_stocktransactions_fabric_complete.intSalesOrderId
						Inner Join trn_sampleinfomations_printsize ON trn_sampleinfomations_printsize.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations_printsize.intSampleYear = trn_orderdetails.intSampleYear AND trn_sampleinfomations_printsize.strPrintName = trn_orderdetails.strPrintName AND trn_sampleinfomations_printsize.intRevisionNo = trn_orderdetails.intRevisionNo
						Inner Join mst_part ON mst_part.intId = trn_sampleinfomations_printsize.intPart
					WHERE
						ware_stocktransactions_fabric_complete.intDocumentNo =  '$dispatchNo' AND
						ware_stocktransactions_fabric_complete.intDocumentYear =  '$dispatchYear'
					group by 
						ware_stocktransactions_fabric_complete.intDocumentNo ,
						ware_stocktransactions_fabric_complete.intDocumentYear ,
						ware_stocktransactions_fabric_complete.intOrderNo ,
						ware_stocktransactions_fabric_complete.intOrderYear ,
						ware_stocktransactions_fabric_complete.intSalesOrderId ,
						ware_stocktransactions_fabric_complete.strSize ,
						ware_stocktransactions_fabric_complete.intGrade  
					";

					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
				  ?>
          <tr class="normalfnt" >
            <td align="center" bgcolor="#FFFFFF"><?php echo $row['strSalesOrderNo']; ?></td>
            <td align="center" bgcolor="#FFFFFF" ><?php echo $row['strSize'] ?></td>
            <td align="center" bgcolor="#FFFFFF" ><?php echo $row['cutNo'] ?></td>
            <td align="center" bgcolor="#FFFFFF" ><?php echo $row['partName'] ?></td>
            <td align="center" bgcolor="#FFFFFF" ><?php echo $row['goodQty'] ?></td>
            <td align="center" bgcolor="#FFFFFF" ><?php echo $row['fdamageQty'] ?></td>
            <td align="center" bgcolor="#FFFFFF" ><?php echo $row['pdamageQty'] ?></td>
            <td align="center" bgcolor="#FFFFFF" ><?php echo $row['cutreturnQty'] ?></td>
            <td align="center" bgcolor="#FFFFFF" ><?php echo $row['strSize'] ?></td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <?php
					}
				  ?>
        </table></td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
            <tr>
              <td bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
<?php  
 	if($intStatus!=0)
	{

			  $prnApproveLevel = $intApproveLevelStart-1;
				for($i=$prnApproveLevel; $i>=1; $i--)
				{
					 $sqlc = "SELECT
							trn_sampledispatchheader_approvedby.intApproveUser,
							trn_sampledispatchheader_approvedby.dtApprovedDate,
							sys_users.strUserName,
							trn_sampledispatchheader_approvedby.intApproveLevelNo
							FROM
							trn_sampledispatchheader_approvedby
							Inner Join sys_users ON trn_sampledispatchheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							trn_sampledispatchheader_approvedby.intDispatchNo =  '$dispatchNo' AND
							trn_sampledispatchheader_approvedby.intDispatchYear =  '$dispatchYear' AND
							trn_sampledispatchheader_approvedby.intApproveLevelNo =  '$i'
";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
						if($prnApproveLevel==$i)
						$desc="1st ";
						else if($prnApproveLevel==$i+1)
						$desc="2nd ";
						else if($prnApproveLevel==$i+2)
						$desc="3rd ";
						else
						$desc=$prnApproveLevel+1-$i."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['strUserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['strUserName']=='')
					 $desc2='---------------------------------';
				?>

            <tr>
                <td bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
<?php
			}
	}
?>

<tr height="40">
  <td align="center" class="normalfntMid">Printed Date: <?php echo date("Y/m/d") ?></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
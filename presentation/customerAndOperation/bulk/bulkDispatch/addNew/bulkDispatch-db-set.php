<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo 	 = $_REQUEST['serialNo'];
	$year 		 = $_REQUEST['Year'];
	$orderNo  	 = $_REQUEST['orderNo'];
	$orderYear 	 = $_REQUEST['orderYear'];
	$locationTo  = $location;
	
	$arr 		= json_decode($_REQUEST['arr'], true);

//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		if($serialNo==''){
			$serialNo 	= getNextSerialNo();
			$year = date('Y');
			$editMode=0;
		}
		else{
			$editMode=1;
		}
		//-----------delete and insert to detail table-----------------------
			$saved=0;
			$toSave=0;
			//$rollBackMsg="Maximum Qty for following items are...";
			foreach($arr as $arrVal)
			{
				//$place = 'Production';
				$salesOrderNo 		= $arrVal['salesNo'];
				$salesOrderId 		= $arrVal['salesId'];
				$size 	 			= $arrVal['size'];
				$grade 	 			= $arrVal['grade'];
				$goodQty 		 	= $arrVal['goodQty'];
				$fdQty 		 		= $arrVal['fdQty'];
				$pdQty 		 		= $arrVal['pdQty'];
				$cutReturnQty 		= $arrVal['cutReturnQty'];
		
				if($goodQty>0)
				{	
					$sql = "INSERT INTO `ware_stocktransactions_fabric_complete` (`intLocationId`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`intSalesOrderId`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$locationTo','$serialNo','$year','$orderNo','$orderYear','$salesOrderId','$size','$grade','".($goodQty*-1)."','COMPLETE-DISPATCH','$userId',now())";
					$result = $db->RunQuery2($sql);
				}
				if($fdQty>0)
				{	
					$sql = "INSERT INTO `ware_stocktransactions_fabric_complete` (`intLocationId`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`intSalesOrderId`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$locationTo','$serialNo','$year','$orderNo','$orderYear','$salesOrderId','$size','$grade','".($fdQty*-1)."','FDAMAGE-DISPATCH','$userId',now())";
					$result = $db->RunQuery2($sql);
				}
				if($pdQty>0)
				{	
					$sql = "INSERT INTO `ware_stocktransactions_fabric_complete` (`intLocationId`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`intSalesOrderId`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$locationTo','$serialNo','$year','$orderNo','$orderYear','$salesOrderId','$size','$grade','".($pdQty*-1)."','PDAMAGE-DISPATCH','$userId',now())";
					$result = $db->RunQuery2($sql);
				}
				if($cutReturnQty>0)
				{	
					$sql = "INSERT INTO `ware_stocktransactions_fabric_complete` (`intLocationId`,`intToLocationId`,`intDocumentNo`,`intDocumentYear`,`intOrderNo`,`intOrderYear`,`intSalesOrderId`,`strSize`,`intGrade`,`dblQty`,`strType`,`intUser`,`dtDate`) 
					VALUES ('$location','$locationTo','$serialNo','$year','$orderNo','$orderYear','$salesOrderId','$size','$grade','".($cutReturnQty*-1)."','CUTRETURN-DISPATCH','$userId',now())";
					$result = $db->RunQuery2($sql);
				}
				
		}
		//echo $rollBackFlag;
		
		
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
				$response['msg'] 		= 'Updated successfully.';
			else
				$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 			= $year;
		
		
		$db->CloseConnection();		
	}
//----------------------------------------
	echo json_encode($response);
//----------------------------------------




//----------------------------------------
	function getNextSerialNo()
	{
		global $db;
		global $company;
		$sql = "SELECT
				sys_no.intBulkDispatchNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$company'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextNo = $row['intBulkDispatchNo'];
		
		$sql = "UPDATE `sys_no` SET intBulkDispatchNo=intBulkDispatchNo+1 WHERE (`intCompanyId`='$company')  ";
		$db->RunQuery2($sql);
		
		return $nextNo;
	}
	//-----------------------------------------------------------
function loadReceivedQty($location,$orderNo,$orderYear,$salesOrderId,$size)
{
		global $db;
		$sql = "SELECT
				sum(ware_stocktransactions_fabric.dblQty) as dblQty 
				FROM ware_stocktransactions_fabric
				WHERE
				ware_stocktransactions_fabric.intLocationId =  '$location' AND
				ware_stocktransactions_fabric.intOrderNo =  '$orderNo' AND
				ware_stocktransactions_fabric.intOrderYear =  '$orderYear' AND
				ware_stocktransactions_fabric.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions_fabric.strSize =  '$size' AND
				ware_stocktransactions_fabric.strType =  'Received' 
				GROUP BY
				ware_stocktransactions_fabric.intOrderNo,
				ware_stocktransactions_fabric.intOrderYear,
				ware_stocktransactions_fabric.intSalesOrderId,
				ware_stocktransactions_fabric.strSize";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//-----------------------------------------------------------
function loadOrderQty($location,$orderNo,$orderYear,$salesOrderNo,$size)
{
		global $db;
		$sql = "SELECT
				trn_ordersizeqty.dblQty
				FROM trn_ordersizeqty
				WHERE
				trn_ordersizeqty.intOrderNo =  '$orderNo' AND
				trn_ordersizeqty.intOrderYear =  '$orderYear' AND
				trn_ordersizeqty.strSalesOrderNo =  '$salesOrderNo' AND
				trn_ordersizeqty.strSize =  '$size'";
	
		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['dblQty']);
}
//-----------------------------------------------------------
?>
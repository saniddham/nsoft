var basepath	= 'presentation/customerAndOperation/bulk/cancleOrder/';
var saveStatus 	= false;

$(document).ready(function(){	
	
	$("#frmOrderCancle").validationEngine();
	$('#butView').live('click',showDetails);
	$('#txtOrderYear').live('focus',hideDetails);
	$('#txtOrderNo').live('focus',hideDetails);butNew
	$('#butNew').live('click',clearAll);
	$('#butCancel').live('click',cancelOrder);
 	$('#butRevise').live('click',revise);

});
function showDetails()
{
	if($('#frmOrderCancle').validationEngine('validate'))
	{
		var orderYear = $('#txtOrderYear').val();
		var orderNo	  = $('#txtOrderNo').val();

		var url = basepath+"cancelOrder-db-get.php?requestType=loadData";
		var obj = $.ajax({
			url:url,
			dataType: "json",  
			data:"&orderYear="+orderYear+'&orderNo='+orderNo,
			async:false,
			success:function(json){
				//console.log(json); return false;
					if(json.type=='pass')
					{
						$('.clsOrderDate').html(json.orderDate);
						$('.clsOrderQty').html(json.orderQty);
						$('.clsEnterBy').html(json.enterBy);
						$('.clsCurrency').html(json.currency);
						$('.clsCustomer').html(json.customer);
						$('.clsPONo').html(json.customerPONo);
						$('.clsLocation').html(json.enterLoc);
						$('.clsFabInQty').html(json.fabInQty);
						$('.clsInvoiceVal').html(json.invValue);
						$('.clsDispatchQty').html(json.dispQty);
						$('.clsPayRecived').html(json.paymentRecive);
						$('.list_mrn').html(json.list_mrn);
                        $('.list_Issue').html(json.list_Issue);
                        $('.list_grn').html(json.list_Grn);

						
 					
						if(json.status==0)
						{
							$('.clsStatus').html('Rejected');
							$('.clsStatus').css('color','#F00');
							
						}
						else if(json.status==1)
						{
							$('.clsStatus').html('Approved');
							$('.clsStatus').css('color','#00CD00');
							if(json.revise_permision==1)
							$('.clsStatus').html('Approved <a class="button white medium" id="butRevise" name="butRevise">Revise</a>');
						}
						else if(json.status==-12)
						{
							$('.clsStatus').html('Revised');
							$('.clsStatus').css('color','#EE799F');
							$('#frmOrderCancle #butRevise').hide();
							
						}
						else if(json.status==-2)
						{
							$('.clsStatus').html('Canceled');
							$('.clsStatus').css('color','#EE799F');
							$('#frmOrderCancle #butCancel').hide();
							
						}
						else if(json.status==-10)
						{
							$('.clsStatus').html('Completed');
							$('.clsStatus').css('color','#8B3E2F');
							$('#frmOrderCancle #butCancel').hide();
							
						}
						else
						{
							$('.clsStatus').html('Pending');
							$('.clsStatus').css('color','#3A5FCD');
							
						}
						if(json.dispQty!=0 || json.invValue!=0 || json.paymentRecive!=0 || json.fabInQty!=0)
						{
							$('#frmOrderCancle #butCancel').hide();
						}
						$("#divOrderDetails").slideDown("slow");
						saveStatus = true;
					}
					else
					{
						$('#frmOrderCancle #butView').validationEngine('showPrompt','Invalid Order No / Order Year.','fail');
						var t=setTimeout("alertx()",3000);
						$("#divOrderDetails").slideUp("slow");
						saveStatus = false;
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmOrderCancle #butView').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					$("#divOrderDetails").slideUp("slow");
					saveStatus = false;
					return;
				}		
		});
		
	}	
 
}
function cancelOrder()
{
	if($('#frmOrderCancle').validationEngine('validate'))
	{
		if(!saveStatus)
		{
			$('#frmOrderCancle #butCancel').validationEngine('showPrompt','Please view the Order.','fail');
			var t=setTimeout("alertx2()",3000);
			return;
		}
		else
		{
			var val = $.prompt('Are you sure you want to cancel "'+$('#txtOrderYear').val()+'/'+$('#txtOrderNo').val()+'" ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
					if(v)
					{
						var url = basepath+"cancelOrder-db-set.php";
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							data:'requestType=cancelOrder&orderYear='+$('#txtOrderYear').val()+'&orderNo='+$('#txtOrderNo').val(),
							async:false,
							success:function(json){
								
								$('#frmOrderCancle #butCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								
								if(json.type=='pass')
								{
									var t=setTimeout("alertx2()",3000);
									var t=setTimeout("clearAll()",3000);
									return;
								}
								var t=setTimeout("alertx2()",3000);	
								return;
							},
							error:function(xhr,status){
								
								$('#frmOrderCancle #butCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx2()",3000);
								return;
							}		
								 
						});
					}
				}
		 	});

		}
	}
}
function hideDetails()
{
	$("#divOrderDetails").slideUp("slow");
	$('#frmOrderCancle #butCancel').show();
	saveStatus = false;
}
function clearAll()
{
	$('#divOrderDetails').slideUp("slow");
	$('#txtOrderYear').val('');
	$('#txtOrderNo').val('');
	saveStatus = false;
}
function alertx()
{
	$('#frmOrderCancle #butView').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmOrderCancle #butCancel').validationEngine('hide')	;
}
function alertx3()
{
	$('#frmOrderCancle #butRevise').validationEngine('hide')	;
}

function revise(){
 	
		var orderYear = $('#txtOrderYear').val();
		var orderNo	  = $('#txtOrderNo').val();
		
		if(orderYear=='' || orderNo==''){
			alert('Please select Order No and Order Year');
			return false;
		}
		
		var url = basepath+"cancelOrder-db-get.php?requestType=revisePO";
			var val = $.prompt('Are you sure you want to revise "'+$('#txtOrderYear').val()+'/'+$('#txtOrderNo').val()+'" ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
						var url = basepath+"cancelOrder-db-set.php?orderYear="+orderYear+"&orderNo="+orderNo+"&requestType=revisePO";
						var obj = $.ajax({url:url,async:false});
						showDetails();
					}
				}});
 }

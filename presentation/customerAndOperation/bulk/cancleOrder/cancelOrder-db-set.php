<?php
ini_set('display_errors',0); 
session_start();
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 	 		= $_SESSION['userId'];
$locationId	  		= $_SESSION["CompanyID"];
$companyId 			= $_SESSION['CompanyID'];
$requestType 		= $_REQUEST['requestType'];

include_once "{$backwardseperator}dataAccess/Connector.php";
include "../../../../libraries/mail/mail.php";
require_once $_SESSION['ROOT_PATH']."class/cls_mail.php";
require_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
require_once $_SESSION['ROOT_PATH']."class/common/azureDBconnection.php";


$objMail 		= new cls_create_mail($db);
$obj_commom		= new cls_commonFunctions_get($db);
$objAzure = new cls_azureDBconnection();

if($requestType=='cancelOrder')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	$rollBackFlag 	= 1;
	$orderNo 		= $_REQUEST['orderNo'];
	$orderYear 		= $_REQUEST['orderYear'];
	
	$sql = "SELECT 
			OH.intCreator, 
			OH.intOrderNo, 
			OH.intOrderYear,
			OH.PO_TYPE,
			OH.intStatus,
			(SELECT IFNULL(SUM(dblQty),0) FROM ware_stocktransactions_fabric STF WHERE STF.intOrderYear=OH.intOrderYear AND STF.intOrderNo=OH.intOrderNo AND STF.strType='Received' ) AS fabInQty,
			(SELECT IFNULL(ABS(SUM(dblQty)),0) FROM ware_stocktransactions_fabric STF WHERE STF.intOrderYear=OH.intOrderYear AND STF.intOrderNo=OH.intOrderNo AND STF.strType='Dispatched_G' ) AS dispatchQty,
			IFNULL((SELECT SUM(CSD.dblQty*CSD.dblUnitPrice) FROM fin_customer_salesinvoice_details CSD
			INNER JOIN fin_customer_salesinvoice_header CSH  ON CSH.intInvoiceNo=CSD.intInvoiceNo AND CSH.intAccPeriodId=CSD.intAccPeriodId AND
			CSH.intLocationId=CSD.intLocationId AND CSH.intCompanyId=CSD.intCompanyId AND CSH.strReferenceNo=CSD.strReferenceNo
			WHERE CSH.strPoNo=OH.strCustomerPoNo ),0) AS invoiceValue,
			IFNULL((SELECT SUM(dblPayAmount) FROM fin_customer_receivedpayments_main_details CRPMD
			INNER JOIN fin_customer_receivedpayments_header CRPH  ON CRPH.intReceiptNo=CRPMD.intReceiptNo AND 
			CRPH.intAccPeriodId=CRPMD.intAccPeriodId AND
			CRPH.intLocationId=CRPMD.intLocationId AND 
			CRPH.intCompanyId=CRPMD.intCompanyId AND 
			CRPH.strReferenceNo=CRPMD.strReferenceNo
			INNER JOIN fin_customer_salesinvoice_header CSH ON CSH.strReferenceNo=CRPMD.strDocNo
			WHERE CSH.strPoNo=OH.strCustomerPoNo AND CRPH.intDeleteStatus=0),0) AS paymentRecive
			FROM trn_orderheader OH
			WHERE OH.intOrderYear='$orderYear' AND
			OH.intOrderNo='$orderNo' ";
	
	$result 		= $db->RunQuery2($sql);
	$row 			= mysqli_fetch_array($result);
	$createdUser 	= $row['intCreator'];
	$po_type        = $row['PO_TYPE'];
	$po_status      = $row['intStatus'];
	
	if($row['dispatchQty']>0)
	{
		$rollBackFlag 	= 1;
		$rollBackMsg	= 'Cancellation failed.'.'</br>'.'DispatchQty Qty = '.$row['dispatchQty'];
	}
	/*if($issuedQty>0)
	{
		$rollBackFlag 	= 1;
		$rollBackMsg	= 'Cancellation failed.'.'</br>'.'Please return the issued Qty.';
	}
	else if($mrnQty>0)
	{
		$rollBackFlag 	= 1;
		$rollBackMsg	= 'Cancellation failed.'.'</br>'.'Please clear the MRN Qty.';
	}*/
	else if($row['invoiceValue']>0)
	{
		$rollBackFlag 	= 1;
		$rollBackMsg	= 'Cancellation failed.'.'</br>'.'Invoice value = '.$row['invoiceValue'];
	}
	else if($row['paymentRecive']>0)
	{
		$rollBackFlag 	= 1;
		$rollBackMsg	= 'Cancellation failed.'.'</br>'.'Payment Recieved = '.$row['paymentRecive'];
	}
	else
	{
		$rollBackFlag 	= 0;
	}
	
	if($rollBackFlag==0)
	{
		
		cancelPRNandPOs($orderNo,$orderYear,'Cancelled');
		$sql = "UPDATE trn_orderheader SET
				intStatus = '-2' , 
				intModifyer = '$userId' , 
				dtmModifyDate = now() 
				WHERE
				intOrderNo = '$orderNo' AND intOrderYear = '$orderYear' ;";
		$result = $db->RunQuery2($sql);
		if($result)
		{
			$sql = "INSERT INTO trn_orderheader_approvedby 
					(intOrderNo, 
					intYear, 
					intApproveLevelNo, 
					intApproveUser, 
					dtApprovedDate, 
					intStatus
					)
					VALUES
					('$orderNo', 
					'$orderYear', 
					'-2', 
					'$userId', 
					now(), 
					'0'
					);";
			$result = $db->RunQuery2($sql);
			if($result)
			{
				$rollBackFlag 	= 0;
				$rollBackMsg	= 'Cancelled sucessfully.';
			}
			else
			{
				$rollBackFlag 	= 1;
				$rollBackMsg	= $db->errormsg;	
			}
		}
		else
		{
			$rollBackFlag 	= 1;
			$rollBackMsg	= $db->errormsg;
		}
	}
	if($rollBackFlag==1)
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
	}
	else
	{
		//if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
		sendCanceledEmailToUser($orderNo,$orderYear,$customerPO,$objMail,$mainPath,$root_path);
        if($status == 0 && $_SESSION['headCompanyId'] == 1 && $po_type != 1 && $po_status ==1){
            updateAzureTable($orderNo,$orderYear,$db,"SO_Cancel");
        }
		//}
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		$response['msg'] 		= $rollBackMsg;
	}
	
	$db->CloseConnection();
	echo json_encode($response);
}

if($requestType=='revisePO')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	
	$orderNo  	= $_REQUEST['orderNo'];
	$orderYear  = $_REQUEST['orderYear'];
	
	 $sql = "	SELECT 
					trn_orderheader.intCreator, 
					trn_orderheader.intStatus,
					trn_orderheader.intApproveLevelStart,
					trn_orderheader.strCustomerPoNo  
				FROM trn_orderheader
				WHERE
					trn_orderheader.intOrderNo =  '$orderNo' AND
					trn_orderheader.intOrderYear =  '$orderYear'
				";
	$result2 = $db->RunQuery2($sql);
	$row2=mysqli_fetch_array($result2);
	$createdUser = $row['intCreator'];
	$status = $row2['intStatus'];
	$savedLevels = $row2['intApproveLevelStart'];
	$customerPO= $row2['strCustomerPoNo'];
	
		cancelPRNandPOs($orderNo,$orderYear,'Revised');
	
	$sql = "UPDATE `trn_orderheader` SET `intStatus`=`intApproveLevelStart`+1, `intReviseNo`=`intReviseNo`+1 WHERE (`intOrderNo`='$orderNo') AND (`intOrderYear`='$orderYear')  ";
	$result=$db->RunQuery2($sql);
	//$sql = "DELETE FROM `trn_orderheader_approvedby` WHERE (`intOrderNo`='$orderNo') AND (`intYear`='$orderYear')";
	//$resultD=$db->RunQuery($sql);
	$maxAppByStatus=(int)getMaxAppByStatus($orderNo,$orderYear)+1;
	$sql = "UPDATE `trn_orderheader_approvedby` SET intStatus ='$maxAppByStatus' 
			WHERE (`intOrderNo`='$orderNo') AND (`intYear`='$orderYear') AND (`intStatus`='0')";
	$result1 = $db->RunQuery2($sql);
	
	
	$sqlI = "INSERT INTO `trn_orderheader_approvedby` (`intOrderNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
		VALUES ('$orderNo','$orderYear','-1','$userId',now(),0)";
	$resultI = $db->RunQuery2($sqlI);
	
	if($result){
		//if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
		sendRevisedEmailToUser($orderNo,$orderYear,$customerPO,$objMail,$mainPath,$root_path);
        updateAzureTable($orderNo,$orderYear,$db,"SO_EditStart");
		//}
		$db->RunQuery2('Commit');
		$response['msg'] = "PO revised successfully.";	
		$response['type'] = 'pass';
	}
	else{
		$db->RunQuery2('Rollback');
		$response['msg'] = "PO revised Fialed.";	
		$response['type'] = 'fail';
	}
	$db->CloseConnection();
	echo json_encode($response);
	////////////////////////////////////////////////////////
		
}






//////////////////////////////
function getMaxAppByStatus($serialNo,$year){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	  $sql = "SELECT
				max(trn_orderheader_approvedby.intStatus) as status 
				FROM
				trn_orderheader_approvedby
				WHERE
				trn_orderheader_approvedby.intOrderNo =  '$serialNo' AND
				trn_orderheader_approvedby.intYear =  '$year'";
				
		$resultm = $db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm['status'];

}
//--------------------------------------------------------
function sendRevisedEmailToUser($serialNo,$year,$customerPO,$objMail,$mainPat,$root_path){
	global $db;
	global $userId;
		    $sql = "SELECT
			trn_orderheader.intCreator,
			trn_orderheader.intMarketer,
			trn_orderheader.strCustomerPoNo, 
			finance_customer_invoice_header.CREATED_BY 
			FROM 
			trn_orderheader
			LEFT JOIN finance_customer_invoice_header ON trn_orderheader.intOrderNo = finance_customer_invoice_header.ORDER_NO AND trn_orderheader.intOrderYear = finance_customer_invoice_header.ORDER_YEAR
			WHERE
			trn_orderheader.intOrderNo =  '$serialNo' AND
			trn_orderheader.intOrderYear =  '$year'";
			$result = $db->RunQuery2($sql);
 			$row=mysqli_fetch_array($result);
			$created_user	= $row['intCreator'];
			
			$creator_details	=	getSysUserDetails($row['intCreator']);
			$marketer_details	=	getSysUserDetails($row['intMarketer']);
			$invoicer_details	=	getSysUserDetails($row['CREATED_BY']);
			$customerPO = $row['strCustomerPoNo'];
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="REVISED CUSTOMER PO ('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='CUSTOMER PO';
			$_REQUEST['field1']='Order No';
			$_REQUEST['field2']='Order Year';
			$_REQUEST['field3']='Customer PO';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']=$customerPO;
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="REVISED CUSTOMER PO ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Revised this";
			$_REQUEST['statement2']="to view this";
			//$_REQUEST['link']=base64_encode($mainPath."presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder.php?orderNo=$serialNo&orderYear=$year&approveMode=1"); 
			$_REQUEST['link']=base64_encode($_SESSION['MAIN_URL']."?q=896&orderNo=$serialNo&orderYear=$year"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			//$objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
			if($userId != $created_user){
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$creator_details['email'],$creator_details['fulll_name']);
			}
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$marketer_details['email'],$marketer_details['fulll_name']);
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$invoicer_details['email'],$invoicer_details['fulll_name']);
}

//---------------------------------------------------------------
function sendCanceledEmailToUser($serialNo,$year,$customerPO,$objMail,$mainPat,$root_path){
	global $db;
	global $userId;
	
		    $sql = "SELECT
			trn_orderheader.intCreator,
			trn_orderheader.intMarketer,
			trn_orderheader.strCustomerPoNo, 
			finance_customer_invoice_header.CREATED_BY 
			FROM 
			trn_orderheader
			LEFT JOIN finance_customer_invoice_header ON trn_orderheader.intOrderNo = finance_customer_invoice_header.ORDER_NO AND trn_orderheader.intOrderYear = finance_customer_invoice_header.ORDER_YEAR
			WHERE
			trn_orderheader.intOrderNo =  '$serialNo' AND
			trn_orderheader.intOrderYear =  '$year'";
			$result = $db->RunQuery2($sql);
 			$row=mysqli_fetch_array($result);
			$created_user	= $row['intCreator'];
			
			$creator_details	=	getSysUserDetails($row['intCreator']);
			$marketer_details	=	getSysUserDetails($row['intMarketer']);
			$invoicer_details	=	getSysUserDetails($row['CREATED_BY']);
			$customerPO 		= $row['strCustomerPoNo'];
			$enterUserName 		= $row['strFullName'];
			$enterUserEmail 	= $row['strEmail'];
			 
			$header="CANCELED CUSTOMER PO ('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='CUSTOMER PO';
			$_REQUEST['field1']='Order No';
			$_REQUEST['field2']='Order Year';
			$_REQUEST['field3']='Customer PO';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']=$customerPO;
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="CANCELED CUSTOMER PO ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Canceled this";
			$_REQUEST['statement2']="to view this";
			//$_REQUEST['link']=base64_encode($mainPath."presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder.php?orderNo=$serialNo&orderYear=$year&approveMode=1"); 
			$_REQUEST['link']=base64_encode($_SESSION['MAIN_URL']."?q=896&orderNo=$serialNo&orderYear=$year"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			//$objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
			if($userId != $created_user){
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$creator_details['email'],$creator_details['fulll_name']);
			}
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$marketer_details['email'],$marketer_details['fulll_name']);
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$invoicer_details['email'],$invoicer_details['fulll_name']);
}
//--------------------------------------------------------
function getSysUserDetails($userId){
	global $db;
	

	  $sql = "SELECT
				sys_users.strFullName as fulll_name,
				sys_users.strEmail as email 
				FROM 
				sys_users
				WHERE sys_users.intUserId ='$userId'";
				
		$resultm = $db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm;

}
//--------------------------------------------------------
function cancelPRNandPOs($orderNo,$orderYear,$action){
	
	
	global $db;
	
	 $sql = "SELECT
			GROUP_CONCAT(distinct trn_orderdetails.strGraphicNo) as str_strGraphicNo ,
			trn_orderheader.PO_TYPE
			FROM
			trn_orderdetails
			INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
			WHERE
			trn_orderdetails.intOrderNo = '$orderNo' AND
			trn_orderdetails.intOrderYear = '$orderYear'
			";
	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
	$graphic	= $row['str_strGraphicNo'];
	$flag_dummy	= $row['PO_TYPE'];
	if($flag_dummy==1)
		$str_pre_costing	="Pre Costing";
	else
		$str_pre_costing	="Costing";
	
		$sql_p	=	"select 
				trn_po_prn_details.ORDER_NO,
				trn_po_prn_details.ORDER_YEAR,
				trn_po_prn_details.ITEM,
				trn_po_prn_details.REQUIRED,
				trn_po_prn_details.PRN_QTY,
				trn_po_prn_details.PURCHASED_QTY,
				trn_po_prn_details.PRN_QTY_USED_OTHERS,
				trn_po_prn_details.PRN_QTY_BALANCED_FROM_DUMMY from `trn_po_prn_details` WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear')";
		$result_p = 	$db->RunQuery2($sql_p);
		while($row_p=	mysqli_fetch_array($result_p)){
			
			$orderNo		=$row_p['ORDER_NO'];
			$orderYear		=$row_p['ORDER_YEAR'];
			$item			=$row_p['ITEM'];
			
	
			$sql_s	="select PRN_QTY_BALANCED_FROM_DUMMY from `trn_po_prn_details` WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`ITEM`='$item')";
			$result_s = $db->RunQuery2($sql_s);
			$rows=mysqli_fetch_array($result_s);
			$used_dummy	= $rows['PRN_QTY_BALANCED_FROM_DUMMY'];
			
			$sql_c	="UPDATE `trn_po_prn_details` SET `REQUIRED`=0 , PRN_QTY_BALANCED_FROM_DUMMY =0 WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`ITEM`='$item')";
			$result_c = $db->RunQuery2($sql_c);
			
			
			$sql_c_s	="UPDATE `trn_po_prn_details_sales_order` SET `REQUIRED`='0' WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear')  AND (`ITEM`='$item')";
			$result_c_s = $db->RunQuery2($sql_c_s);
		
			$sql_d	="update 
					trn_po_prn_details
					INNER JOIN trn_orderheader ON trn_po_prn_details.ORDER_NO = trn_orderheader.DUMMY_PO_NO 
					AND trn_po_prn_details.ORDER_YEAR = trn_orderheader.DUMMY_PO_YEAR
					INNER JOIN trn_po_prn_details AS pd ON trn_orderheader.intOrderNo = pd.ORDER_NO 
					AND trn_orderheader.intOrderYear = pd.ORDER_YEAR AND trn_po_prn_details.ITEM = pd.ITEM
					set trn_po_prn_details.PRN_QTY_USED_OTHERS = IFNULL(trn_po_prn_details.PRN_QTY_USED_OTHERS,0)-'$used_dummy'
					WHERE
					pd.ORDER_NO = '$orderNo' AND
					pd.ORDER_YEAR = '$orderYear' 
					 AND (pd.`ITEM`='$item') ";
		
			$result_d = $db->RunQuery2($sql_d);
			
		}
		
		//----------------------------------------------------------------------
		/* $sql = "select 
		 trn_prndetails.intOrderNo,
		 trn_prndetails.intOrderYear,
		 trn_prndetails.intItem ,
		 trn_prndetails.dblPrnQty
		 from 
		 trn_prnheader
left JOIN trn_prndetails ON trn_prnheader.intPrnNo = trn_prndetails.intPrnNo AND trn_prnheader.intYear = trn_prndetails.intYear 
left JOIN trn_podetails ON trn_podetails.intPrnNo = trn_prndetails.intPrnNo AND trn_podetails.intPrnYear = trn_prndetails.intYear 
left JOIN trn_poheader ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear 
WHERE (trn_poheader.intStatus >1 or trn_poheader.intStatus =0 or trn_poheader.intStatus is null ) and trn_prnheader.`intStatus`<>1 and (trn_prnheader.`intOrderNo`='$orderNo') AND (trn_prnheader.`intOrderYear`='$orderYear') ";*/

		//-----------------------------------------------------------------------
	/*	 $sql = "select 
		 trn_prndetails.intOrderNo,
		 trn_prndetails.intOrderYear,
		 trn_prndetails.intItem ,
		 trn_prndetails.dblPrnQty
		 from 
		 trn_prnheader
left JOIN trn_prndetails ON trn_prnheader.intPrnNo = trn_prndetails.intPrnNo AND trn_prnheader.intYear = trn_prndetails.intYear 
left JOIN trn_podetails ON trn_podetails.intPrnNo = trn_prndetails.intPrnNo AND trn_podetails.intPrnYear = trn_prndetails.intYear 
left JOIN trn_poheader ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear 
WHERE (trn_poheader.intStatus >1 or trn_poheader.intStatus =0 or trn_poheader.intStatus is null ) and trn_prnheader.`intStatus`=1 and (trn_prnheader.`intOrderNo`='$orderNo') AND (trn_prnheader.`intOrderYear`='$orderYear') ";
		$result = $db->RunQuery2($sql);
		while($row=mysqli_fetch_array($result)){
		
			$item	=$row['intItem'];
			$prnQty	=$row['dblPrnQty'];*/

		/*	$sql_s	="select PRN_QTY_BALANCED_FROM_DUMMY from `trn_po_prn_details` WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`ITEM`='$item')";
			$result_s = $db->RunQuery2($sql_s);
			$rows=mysqli_fetch_array($result_s);
			$used_dummy	= $rows['PRN_QTY_BALANCED_FROM_DUMMY'];*/
			
			/*$sql_c	="UPDATE trn_po_prn_details SET  PRN_QTY =PRN_QTY-'$prnQty'  WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`ITEM`='$item')";
			$result_c = $db->RunQuery2($sql_c);*/
			
		/*	$sql_d	="update 
					trn_po_prn_details
					INNER JOIN trn_orderheader ON trn_po_prn_details.ORDER_NO = trn_orderheader.DUMMY_PO_NO 
					AND trn_po_prn_details.ORDER_YEAR = trn_orderheader.DUMMY_PO_YEAR
					INNER JOIN trn_po_prn_details AS pd ON trn_orderheader.intOrderNo = pd.ORDER_NO 
					AND trn_orderheader.intOrderYear = pd.ORDER_YEAR AND trn_po_prn_details.ITEM = pd.ITEM
					set trn_po_prn_details.PRN_QTY_USED_OTHERS = IFNULL(trn_po_prn_details.PRN_QTY_USED_OTHERS,0)-'$used_dummy'
					WHERE
					pd.ORDER_NO = '$orderNo' AND
					pd.ORDER_YEAR = '$orderYear' 
					 AND (pd.`ITEM`='$item') ";
		
			$result_d = $db->RunQuery2($sql_d); */
	//	}



	
		 $sql = "UPDATE trn_prnheader
left JOIN trn_prndetails ON trn_prnheader.intPrnNo = trn_prndetails.intPrnNo AND trn_prnheader.intYear = trn_prndetails.intYear 
left JOIN trn_podetails ON trn_podetails.intPrnNo = trn_prndetails.intPrnNo AND trn_podetails.intPrnYear = trn_prndetails.intYear 
left JOIN trn_poheader ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
 SET trn_prnheader.`intStatus`=-10,trn_prnheader.`SYSTEM_CANCELLATION`=1  WHERE (trn_poheader.intStatus >1 or trn_poheader.intStatus =0 or trn_poheader.intStatus is null ) and (trn_prnheader.`intOrderNo`='$orderNo') AND (trn_prnheader.`intOrderYear`='$orderYear') ";
		$result = $db->RunQuery2($sql);

		$sql = "UPDATE trn_podetails
INNER JOIN trn_poheader ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
INNER JOIN trn_prndetails ON trn_podetails.intPrnNo = trn_prndetails.intPrnNo AND trn_podetails.intPrnYear = trn_prndetails.intYear 
SET trn_podetails.`CLEARED_QTY`=trn_podetails.`dblQty`, trn_podetails.`dblQty`=0 ,trn_podetails.CLEARED_REASON = '$action'  WHERE 
trn_prndetails.intOrderNo = '$orderNo' AND
trn_prndetails.intOrderYear = '$orderYear' and 
(trn_poheader.intStatus >1 or trn_poheader.intStatus =0)";
		$result = $db->RunQuery2($sql);
		
//-----------------------------------------------------------
		$sql	=	"select 
					trn_po_prn_details.ITEM,
					(select sum(trn_prndetails.dblPrnQty)
					from 
					trn_prnheader
					left JOIN trn_prndetails ON trn_prnheader.intPrnNo = trn_prndetails.intPrnNo 
					AND trn_prnheader.intYear = trn_prndetails.intYear 
					where  trn_prnheader.`intStatus`=1 and (trn_prnheader.`intOrderNo`=trn_po_prn_details.ORDER_NO) AND (trn_prnheader.`intOrderYear`=trn_po_prn_details.ORDER_YEAR) AND (trn_po_prn_details.ITEM=trn_prndetails.intItem) ) as prnQty
					from 
				   `trn_po_prn_details` WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear')";
		$result = $db->RunQuery2($sql);
		while($row=mysqli_fetch_array($result)){
		
			$item	=$row['ITEM'];
			$prnQty	=$row['prnQty'];
			if($prnQty=='')
				$prnQty=0;
			
			$sql_c	="UPDATE trn_po_prn_details SET  PRN_QTY ='$prnQty'  WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`ITEM`='$item')";
			$result_c = $db->RunQuery2($sql_c);
		}

//---------------------------------------------------------	
	
	
		generatePRNPOcancelledEmail($orderNo,$orderYear,$action);
}


function generatePRNPOcancelledEmail($orderNo,$orderYear,$action){
	
	global $objMail;
	global $companyId;
	global $db;
	global $obj_commom;
	
	$content	='';
	
	global $db;
	
	 $sql = "SELECT
			GROUP_CONCAT(distinct trn_orderdetails.strGraphicNo) as str_strGraphicNo ,
			trn_orderheader.PO_TYPE
			FROM
			trn_orderdetails
			INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
			WHERE
			trn_orderdetails.intOrderNo = '$orderNo' AND
			trn_orderdetails.intOrderYear = '$orderYear'
			";
	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
	$graphic	= $row['str_strGraphicNo'];
	$flag_dummy	= $row['PO_TYPE'];
	if($flag_dummy==1)
		$str_pre_costing	=" - Pre Costing";
	else
		$str_pre_costing	=" - Costing";
	
	
		//ob_start();
		
		$sql	="SELECT
				trn_prnheader.intPrnNo as no,
				trn_prnheader.intYear as year 
				FROM `trn_prnheader`
				WHERE
				trn_prnheader.SYSTEM_CANCELLATION = 1 AND
				trn_prnheader.intOrderNo = '$orderNo' AND
				trn_prnheader.intOrderYear = '$orderYear' AND
				trn_prnheader.intStatus = -10
				GROUP BY
				trn_prnheader.intPrnNo,
				trn_prnheader.intYear
				";
		$result = $db->RunQuery2($sql);
		$content1	='';
		$i	=0;
		while($row=mysqli_fetch_array($result)){
			$i++;
			$year	= $row['year'];
			$prn_no	= $row['no'];
			$link=$_SESSION['MAIN_URL']."?q=917&prnNo=$prn_no&year=$year";
 			$content1	.="<tr><td>&nbsp;</td><td class='normalfnt'><a  href= ".$link.">$prn_no/$year</a></td>	<td>&nbsp;</td></tr>";
		}
		if($i>0){
			$content	.= "<tr><td>&nbsp;</td><td class='normalfnt'><b>Click Following links to view auto Cancelled PRN</b></td>	<td>&nbsp;</td></tr>".$content1."<tr><td>&nbsp;</td><td class='normalfnt'></td>	<td>&nbsp;</td></tr>";
		}



		$sql	="SELECT
				trn_prnheader.intPrnNo as no,
				trn_prnheader.intYear as year 
				FROM `trn_prnheader`
				WHERE
				trn_prnheader.intOrderNo = '$orderNo' AND
				trn_prnheader.intOrderYear = '$orderYear' AND
				trn_prnheader.intStatus = 1
				GROUP BY
				trn_prnheader.intPrnNo,
				trn_prnheader.intYear
				";
		$result = $db->RunQuery2($sql);
		$content1	='';
		$i	=0;
		while($row=mysqli_fetch_array($result)){
			$i++;
			$year	= $row['year'];
			$prn_no	= $row['no'];
			$link=$_SESSION['MAIN_URL']."?q=917&prnNo=$prn_no&year=$year";
 			$content1	.="<tr><td>&nbsp;</td><td class='normalfnt'><a  href= ".$link.">$prn_no/$year</a></td>	<td>&nbsp;</td></tr>";
		}
		if($i>0){
			$content	.= "<tr><td>&nbsp;</td><td class='normalfnt'><b>Click Following links to view Approved PRN</b></td>	<td>&nbsp;</td></tr>".$content1."<tr><td>&nbsp;</td><td class='normalfnt'></td>	<td>&nbsp;</td></tr>";
		}


		$sql	="SELECT
				trn_podetails.intPONo AS `no`,
				trn_podetails.intPOYear AS `year`,
				mst_item.strName,
				trn_podetails.CLEARED_QTY
				FROM
				trn_poheader
				INNER JOIN trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
				INNER JOIN trn_prnheader ON trn_podetails.intPrnNo = trn_prnheader.intPrnNo AND trn_podetails.intPrnYear = trn_prnheader.intYear
INNER JOIN mst_item ON trn_podetails.intItem = mst_item.intId
				WHERE 
				trn_prnheader.intOrderNo = '$orderNo' AND 
				trn_prnheader.intOrderYear ='$orderYear' AND 
				trn_podetails.CLEARED_QTY > 0
				/*trn_poheader.SYSTEM_CANCELLATION = 1 AND
				trn_poheader.intStatus = -10 */
				/*GROUP BY
				trn_podetails.intPONo,
				trn_podetails.intPOYear */";
		$result = $db->RunQuery2($sql);
		$content1	='';
		$i	=0;
		$temp_order	='';
		while($row=mysqli_fetch_array($result)){
			$i++;
			$year			= $row['year'];
			$order_no		= $row['no'];
			$item			= $row['strName'];
			$cleared_qty	= $row['CLEARED_QTY'];
			$link=$_SESSION['MAIN_URL']."?q=934&poNo=$order_no&year=$year";
			if($order_no.'/'.$year != $temp_order){
 			$content1	.="<tr><td>&nbsp;</td><td class='normalfnt'><a  href= ".$link.">$order_no/$year</a></td>	<td>&nbsp;</td></tr>";
			}
 			$content1	.="<tr><td>&nbsp;</td><td class='normalfnt'>Item ".$item." : Qty = ".$cleared_qty."</td>	<td>&nbsp;</td></tr>";
		$temp_order	=$order_no.'/'.$year;
		}
		if($i>0){
			$content	.= "<tr><td>&nbsp;</td><td class='normalfnt'><b>Click Following links to view auto cleared Supplier PO due to the Customer PO action : ".$action."</b></td>	<td>&nbsp;</td></tr>".$content1."<tr><td>&nbsp;</td><td class='normalfnt'></td>	<td>&nbsp;</td></tr>";
		}



		$sql	="SELECT
				trn_podetails.intPONo as no ,
				trn_podetails.intPOYear as year 
				FROM
				trn_poheader
				INNER JOIN trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
				INNER JOIN trn_prnheader ON trn_podetails.intPrnNo = trn_prnheader.intPrnNo AND trn_podetails.intPrnYear = trn_prnheader.intYear
				WHERE 
				trn_prnheader.intOrderNo = '$orderNo' AND 
				trn_prnheader.intOrderYear ='$orderYear' AND 
				trn_poheader.intStatus = 1
				GROUP BY
				trn_podetails.intPONo,
				trn_podetails.intPOYear";
		$result = $db->RunQuery2($sql);
		$content1	='';
		$i	=0;
		while($row=mysqli_fetch_array($result)){
			$i++;
			$year	= $row['year'];
			$order_no	= $row['no'];
			$link=$_SESSION['MAIN_URL']."?q=934&poNo=$order_no&year=$year";
 			$content1	.="<tr><td>&nbsp;</td><td class='normalfnt'><a  href= ".$link.">$order_no/$year</a></td>	<td>&nbsp;</td></tr>";
		}
		if($i>0){
			$content	.= "<tr><td>&nbsp;</td><td class='normalfnt'><b>If it is possible please revise or cancel following supplier pos since the bulk order is ".$action.". Click Following links to view Approved Supplier PO</b></td>	<td>&nbsp;</td></tr>".$content1."<tr><td>&nbsp;</td><td class='normalfnt'></td>	<td>&nbsp;</td></tr>";
		}
	
	
		//include_once "rptBulkOrder-db-prn_mail_template.php";
		
 		//$body 			= ob_get_clean();
 		//---------
		
		$nowDate 		= date('Y-m-d');
 		
		$mailHeader		= "SYSTEM CANCELLED PRN AND QTY CLEARED SUPPLIER PO".$str_pre_costing." - Graphics(".$graphic." )";
		
		$FROM_NAME		= "NSOFT";
		$FROM_EMAIL		= '';
		
		$sql	="SELECT
				sys_mail_eventusers.intUserId ,
				sys_users.strUserName 
				FROM
				sys_mail_eventusers
				INNER JOIN sys_users ON sys_mail_eventusers.intUserId = sys_users.intUserId
				WHERE
				sys_mail_eventusers.intMailEventId = 1031 AND
				sys_mail_eventusers.intCompanyId = '$companyId' AND
				sys_users.intStatus = 1
				GROUP BY
				sys_mail_eventusers.intUserId";
	$result = $db->RunQuery2($sql);
	while($row=mysqli_fetch_array($result)){
		
		$receiver_name	=$row['strUserName'];
		$mail_TO		= $obj_commom->getEmailList2($row['intUserId']);
		$FROM_NAME		= 'NSOFT';
		
		ob_start();
			include "rptBulkOrder-db-prn_mail_template.php";
 		$body		= ob_get_clean();
		
		//$objMail->insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);
		insertTable2($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,'','');
	}
}

function updateAzureTable($orderNo,$orderYear,$db,$transactionType)
{

    global $objAzure;
    $environment = $objAzure->getEnvironment();
    $headerTable = 'SalesHeader'.$environment;
    $lineTable = 'SalesLine'.$environment;

    $azure_connection = $objAzure->connectAzureDB();
    $sql_header = "SELECT
			trn_orderheader.intReviseNo,
			trn_orderheader.intMarketer AS marketer,
			trn_orderheader.intStatus,
			trn_orderheader.strRemark,
			trn_orderheader.intApproveLevelStart,
			trn_orderheader.strCustomerPoNo,
			trn_orderheader.intLocationId,
			trn_orderheader.intCustomer,
			trn_orderheader.intPaymentTerm,
			trn_orderheader.dtmCreateDate AS order_date,
			mst_customer.strName AS customer,
			mst_customer.strCode AS customerCode,
			trn_orderheader.strCustomerPoNo,
			mst_customer_locations_header.strName AS customer_location,
			mst_financecurrency.strCode AS curr_code,
			marketer.strUserName,
			(
							SELECT
								CAST(
									MAX(
										ware_stocktransactions_fabric.dtDate
									) AS DATE
								)
							FROM
								ware_stocktransactions_fabric
							WHERE
								ware_stocktransactions_fabric.intOrderNo = trn_orderheader.intOrderNo
							AND ware_stocktransactions_fabric.intOrderYear = trn_orderheader.intOrderYear
							AND ware_stocktransactions_fabric.strType LIKE '%Dispatched%' 
							GROUP BY
								ware_stocktransactions_fabric.intOrderNo,
								ware_stocktransactions_fabric.intOrderYear
			) AS lastDispatchDate
			FROM
			trn_orderheader
			INNER JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
			INNER JOIN mst_financecurrency ON mst_financecurrency.intId = trn_orderheader.intCurrency
			INNER JOIN sys_users AS marketer ON marketer.intUserId = trn_orderheader.intMarketer
			INNER JOIN trn_orderheader_approvedby ON trn_orderheader.intOrderNo = trn_orderheader_approvedby.intOrderNo
            AND trn_orderheader.intOrderYear = trn_orderheader_approvedby.intYear
            AND trn_orderheader_approvedby.intApproveLevelNo = 3
			LEFT JOIN mst_customer_locations_header ON trn_orderheader.intCustomerLocation = mst_customer_locations_header.intId
			WHERE
			trn_orderheader.intOrderNo = '$orderNo'
			AND trn_orderheader.intOrderYear = '$orderYear'                         
			GROUP BY
			trn_orderheader.intOrderNo,
			trn_orderheader.intOrderYear";

    $result_header = $db->RunQuery2($sql_header);
    while ($row_header = mysqli_fetch_array($result_header)) {
        $poString = trim($orderNo).'/'.trim($orderYear);
        $customer = trim($row_header['customer']);
        $customerCode = $row_header['customerCode'];
        $cus_location = trim($row_header['customer_location']);
        $strCustomerPoNo = trim($row_header['strCustomerPoNo']);
        $strRemark = $row_header['strRemark'];
        $strRemark_sql = $db->escapeString($strRemark);
        $orderDate = $row_header['order_date'];
        $curr_code = $row_header['curr_code'];
        $payment_term = $row_header['intPaymentTerm'];
        $marketer = $row_header['marketer'];
        $location = $row_header['intLocationId'];
        $revise_no = $row_header['intReviseNo'];
        $disp_date = $row_header['lastDispatchDate'];
        $successHeader = 0;
        $i = 0;
        if($transactionType == 'SO_EditStart'){
            $revisedDate = date('Y-m-d');
        }
        $postingDate = date('Y-m-d H:i:s');

        $sql_select = "SELECT
			OD.intSalesOrderId,
			OD.strSalesOrderNo,
			OD.intOrderNo,
			OD.intOrderYear,
			OD.strLineNo,
			OD.dtDeliveryDate AS soDeliverydate,
			OD.dtPSD AS PSD,			
			OD.strGraphicNo,
			OD.intSampleNo,
			OD.intSampleYear,
			OD.strStyleNo,
			OD.dblOverCutPercentage,
			OD.dblDamagePercentage,
			OD.strPrintName,
			OD.strCombo,
			OD.TECHNIQUE_GROUP_ID, 
			(
			  select TECHNIQUE_GROUP_NAME from mst_technique_groups where mst_technique_groups.TECHNIQUE_GROUP_ID = OD.TECHNIQUE_GROUP_ID
			)AS technique,
			OD.dtPSD,
			OD.intQty AS poqty,
			OD.dblPrice AS price,
			OD.intOrderYear,
			OD.intPart,
			mst_part.strName as partName,
			OD.dblDamagePercentage,
			mst_brand.strName AS brand,
			trn_sampleinfomations_details.intGroundColor,
			mst_colors_ground.strName as groundColor,
			(
				SELECT
					sum(finance_customer_invoice_details.QTY)
				FROM
					finance_customer_invoice_details
				INNER JOIN finance_customer_invoice_header ON finance_customer_invoice_header.SERIAL_NO = finance_customer_invoice_details.SERIAL_NO
				AND finance_customer_invoice_header.SERIAL_NO = finance_customer_invoice_details.SERIAL_NO
				AND finance_customer_invoice_header.STATUS = 1
				WHERE
					finance_customer_invoice_header.ORDER_NO = OD.intOrderNo
				AND finance_customer_invoice_header.ORDER_YEAR = OD.intOrderYear
				AND finance_customer_invoice_details.SALES_ORDER_ID = OD.intSalesOrderId
			) AS invoiced_qty,
			(
            SELECT
                IFNULL(
                    SUM(
                        CEIL(
                            trn_ordersizeqty.dblQty * (
                                trn_orderdetails.dblOverCutPercentage + trn_orderdetails.dblDamagePercentage
                            ) / 100
                        )
                    ),
                    0
                ) AS excessQty
            FROM
                trn_orderdetails
            INNER JOIN trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo
            AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear
            AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
            WHERE
                trn_orderdetails.intOrderNo = OD.intOrderNo
            AND trn_orderdetails.intOrderYear = OD.intOrderYear
            AND trn_orderdetails.intSalesOrderId = OD.intSalesOrderId
        ) AS excessQty				
			FROM
			trn_orderdetails OD
			INNER JOIN mst_part ON mst_part.intId = OD.intPart
			INNER JOIN trn_sampleinfomations_details ON OD.intSampleNo = trn_sampleinfomations_details.intSampleNo
			AND OD.intSampleYear = trn_sampleinfomations_details.intSampleYear
			AND OD.intRevisionNo = trn_sampleinfomations_details.intRevNo
			AND OD.strCombo = trn_sampleinfomations_details.strComboName
			AND OD.strPrintName = trn_sampleinfomations_details.strPrintName
			INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
			AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
			AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo
			INNER JOIN mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
			INNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
			WHERE
			OD.intOrderNo = '$orderNo'
			AND OD.intOrderYear = '$orderYear'
			GROUP BY
			OD.intOrderNo,
			OD.intOrderYear,
			OD.intSalesOrderId";

        $result1 = $db->RunQuery2($sql_select);
        while ($row = mysqli_fetch_array($result1)) {
            $intSalesOrderId = $row['intSalesOrderId'];
            $strSalesOrderNo = trim($row['strSalesOrderNo']);
            $line_no = intval($row['strLineNo']);
            $strGraphicNo = trim($row['strGraphicNo']);
            $strCombo = trim($row['strCombo']);
            $intSampleNo = $row['intSampleNo'];
            $intSampleYear = $row['intSampleYear'];
            $groundColor = trim($row['groundColor']);
            $strStyleNo = trim($row['strStyleNo']);
            $strPrintName = trim($row['strPrintName']);
            $partName = trim($row['partName']);
            $brand = trim($row['brand']);
            $technique = $row['technique'];
            if (strpos($technique, 'Plotter') !== false) {
                $technique='Plotter/Lazer cut';
            }
            $sampleNo = $intSampleNo.'/'.$intSampleYear;
            $psd = $row['PSD'];
            $successDetails = 0;
            $poqty = $row['poqty'];
            $invoiced_qty = $row['invoiced_qty'];
            $totalQty = $poqty + $row['excessQty'] + 2 - $invoiced_qty;
            $amount = round( $row['price'], 5);

            if($totalQty > 0) {

                $i++;

                $sql_azure_details = "INSERT into $lineTable (Transaction_type, Order_No, Document_Type, Sales_Order_No, Line_No, Sales_Order_Name, Item_Code, Description, Item_Category, Item_SubCategory, Quantity, Original_Quantity, Amount, VAT_Prod_Posting_Group, Technique_Revenue_Code, Graphic_No, Style_No, [Print], Combo, Part, Brand, Sample_No, Production_Start_Date, Revised_Count) VALUES ('$transactionType','$poString', 'Order','$intSalesOrderId','$line_no', '$strSalesOrderNo', 'PRINT','PRINT','PRINT','PRINT','$totalQty','$poqty','$amount', 'VAT', '$technique', '$strGraphicNo','$strStyleNo','$strPrintName','$strCombo','$partName','$brand','$sampleNo','$psd', '$revise_no')";
                if ($azure_connection) {
                    $getResults = $objAzure->runQuery($azure_connection, $sql_azure_details);
                    if ($getResults != FALSE) {
                        $successDetails = 1;
                    }
                }
                $sql_details = "INSERT into trn_financemodule_salesline (Transaction_type, Order_No, Sales_Order_No, Line_No, Sales_Order_Name, Item_Code, Description, Item_Category, Item_SubCategory, Quantity, Original_Quantity, Amount, VAT_Prod_Posting_Group, Technique_Revenue_Code, Graphic_No, Style_No, Print, Combo, Part, Brand, Sample_No, Production_Start_Date, Revised_Count, deliveryStatus, deliveryDate) VALUES ('$transactionType','$poString','$intSalesOrderId','$line_no', '$strSalesOrderNo', 'PRINT','PRINT','PRINT','PRINT','$totalQty','$poqty','$amount', 'VAT', '$technique', '$strGraphicNo','$strStyleNo','$strPrintName','$strCombo','$partName','$brand','$sampleNo','$psd','$revise_no','$successDetails',NOW())";
                $result_details = $db->RunQuery2($sql_details);
            }
        }



        $sql_azure_header = "INSERT into $headerTable (Transaction_type, Order_No, Document_Type, Customer_Code, Customer_Name, Posting_Date, Order_Date, Currency_Code, Revised_Date, Narration, Payment_Terms, Customer_PO_Number, Sales_Person, Location_Code, No_of_Lines, Revised_Count, Last_Dispatched_Date, Customer_Location) VALUES ('$transactionType','$poString','Order','$customerCode','$customer', '$postingDate', '$orderDate','$curr_code','$revisedDate','$strRemark','$payment_term','$strCustomerPoNo', '$marketer', '$location', '$i','$revise_no','$disp_date','$cus_location')";
        if($azure_connection) {
            $getResults = $objAzure->runQuery($azure_connection, $sql_azure_header);
            if($getResults != FALSE){
                $successHeader = 1;
            }
        }
        $sql = "INSERT into trn_financemodule_salesheader (Transaction_type, Order_No, Document_Type, Customer_Code, Customer_Name, Posting_Date, Order_Date, Currency_Code, Revised_Date, Narration, Payment_Terms, Customer_PO_Number, Sales_Person, Location_Code, No_of_Lines, Revised_Count, Last_Dispatched_Date, Customer_Location, deliveryStatus, deliveryDate ) VALUES ('$transactionType','$poString','Order','$customerCode','$customer', '$postingDate', '$orderDate','$curr_code','$revisedDate','$strRemark_sql','$payment_term','$strCustomerPoNo', '$marketer', '$location', '$i', '$revise_no', '$disp_date', '$cus_location', '$successHeader', NOW())";
        $result_header = $db->RunQuery2($sql);

    }


}

?>
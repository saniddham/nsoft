<?php
session_start();
//ini_set('display_errors', 'off');
 //   error_reporting(E_ALL);
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 	 		= $_SESSION['userId'];
$locationId	  		= $_SESSION["CompanyID"];
$requestType 		= $_REQUEST['requestType'];
$programCode		='P0427';


include_once "{$backwardseperator}dataAccess/Connector.php";
require_once $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";

$obj_commonErr		= new cls_commonErrorHandeling_get($db);


$sql = "SELECT 	mst_locations.intCompanyId
		FROM mst_locations WHERE mst_locations.intId='$locationId'";

$result 	 = $db->RunQuery($sql);
$errorFlg	 = 0;
$row		 = mysqli_fetch_array($result);
$loginMainCompany 	 = $row['intCompanyId'];

if($requestType=='loadData')
{
    $orderYear 	= $_REQUEST['orderYear'];
    $orderNo 	= $_REQUEST['orderNo'];

    $sql = "SELECT OH.intOrderNo,
			OH.intOrderYear,
			OH.dtDate,
			(SELECT IFNULL(SUM(intQty),0) FROM trn_orderdetails OD WHERE OD.intOrderNo=OH.intOrderNo AND OD.intOrderYear=OH.intOrderYear) AS orderQty,
			(SELECT IFNULL(SUM(dblQty),0) FROM ware_stocktransactions_fabric STF WHERE STF.intOrderYear=OH.intOrderYear AND STF.intOrderNo=OH.intOrderNo AND STF.strType='Received' ) AS fabInQty,
			(SELECT IFNULL(ABS(SUM(dblQty)),0) FROM ware_stocktransactions_fabric STF WHERE STF.intOrderYear=OH.intOrderYear AND STF.intOrderNo=OH.intOrderNo AND STF.strType='Dispatched_G' ) AS dispatchQty,
			IFNULL((SELECT SUM(CSD.dblQty*CSD.dblUnitPrice) FROM fin_customer_salesinvoice_details CSD
			INNER JOIN fin_customer_salesinvoice_header CSH  ON CSH.intInvoiceNo=CSD.intInvoiceNo AND CSH.intAccPeriodId=CSD.intAccPeriodId AND
			CSH.intLocationId=CSD.intLocationId AND CSH.intCompanyId=CSD.intCompanyId AND CSH.strReferenceNo=CSD.strReferenceNo
			WHERE CSH.strPoNo=OH.strCustomerPoNo ),0) AS invoiceValue,
			IFNULL((SELECT SUM(dblPayAmount) FROM fin_customer_receivedpayments_main_details CRPMD
			INNER JOIN fin_customer_receivedpayments_header CRPH  ON CRPH.intReceiptNo=CRPMD.intReceiptNo AND 
			CRPH.intAccPeriodId=CRPMD.intAccPeriodId AND
			CRPH.intLocationId=CRPMD.intLocationId AND 
			CRPH.intCompanyId=CRPMD.intCompanyId AND 
			CRPH.strReferenceNo=CRPMD.strReferenceNo
			INNER JOIN fin_customer_salesinvoice_header CSH ON CSH.strReferenceNo=CRPMD.strDocNo
			WHERE CSH.strPoNo=OH.strCustomerPoNo AND CRPH.intDeleteStatus=0),0) AS paymentRecive,
			SU.strUserName, 
			OH.intStatus,
			OH.intApproveLevelStart,
			OH.strCustomerPoNo,
			CONCAT(MC.strName,'-',ML.strName) AS enterLocation,
			MCU.strName AS customer,
			FC.strCode AS currency
			FROM trn_orderheader OH
			INNER JOIN sys_users SU ON SU.intUserId=OH.intCreator
			INNER JOIN mst_locations ML ON ML.intId=OH.intLocationId
			INNER JOIN mst_companies MC ON ML.intCompanyId=MC.intId
			INNER JOIN mst_customer MCU ON MCU.intId=OH.intCustomer
			INNER JOIN mst_financecurrency FC ON FC.intId=OH.intCurrency
			WHERE OH.intOrderYear='$orderYear' AND
			OH.intOrderNo='$orderNo' AND MC.intId = '$loginMainCompany'";

	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	if(mysqli_num_rows($result)>0)
		$response['type'] 		= 'pass';
	else
		$response['type'] 		= 'fail';

	$revisePermition	=$obj_commonErr->get_permision_withApproval_revise($row['intStatus'],$row['intApproveLevelStart'],$userId,$programCode,'RunQuery');
	$reviseMode			=$revisePermition['permision'];

	$list_mrn	='';

	$result_mrn	= getApprovedandPendingMrns($orderNo,$orderYear);
 	while($row_m = mysqli_fetch_array($result_mrn)){
	$url		= $_SESSION['MAIN_URL'];
	$mrn_no 	= $row_m['mrn_no'];
	$mrn_year 	= $row_m['year'];
 	$list_mrn 	.='(<a href='.$url.'index.php?q=229&mrnNo='.$mrn_no.'&year='.$mrn_year.' target="new">'.$mrn_no.'</a>)';
 	}

    $list_Issue = '';
    $result_issuedNotes= getApprovedandPendingIssueNotes($orderNo,$orderYear);
    while($row_m = mysqli_fetch_array($result_issuedNotes)){
        $url		= $_SESSION['MAIN_URL'];
        $IssueNo	= $row_m['IssueNo'];
        $Issue_year 	= $row_m['Year'];
        $list_Issue 	.='(<a href='.$url.'index.php?q=231&issueNo='.$IssueNo.'&year='.$Issue_year.' target="new">'.$IssueNo.'</a>)';
    }

    //?q=231&issueNo=85000132&year=2016"


    //?q=224&grnNo=212013&y
    /**
     * GRN
     */
    $result_grn	= getApprovedandPendingGrns($orderNo,$orderYear);
    while($row_m = mysqli_fetch_array($result_grn)){

        $url		= $_SESSION['MAIN_URL'];
        $Grn_no 	= $row_m['grnNo'];
        $Grn_year 	= $row_m['Year'];
        $list_Grn 	.='(<a href='.$url.'index.php?q=224&grnNo='.$Grn_no.'&year='.$Grn_year.' target="new">'.$Grn_no.'</a>)';
    }


    $response['list_Grn'] 		= $list_Grn;
    $response['list_Issue'] 	= $list_Issue;
	$response['list_mrn'] 		= $list_mrn;
	$response['orderDate'] 		= $row['dtDate'];
	$response['orderQty'] 		= $row['orderQty'];
	$response['enterBy'] 		= $row['strUserName'];
	$response['status'] 		= $row['intStatus'];
	$response['customerPONo'] 	= $row['strCustomerPoNo'];
	$response['enterLoc'] 		= $row['enterLocation'];
	$response['customer'] 		= $row['customer'];
	$response['currency'] 		= $row['currency'];
	$response['fabInQty'] 		= $row['fabInQty'];
	$response['dispQty'] 		= $row['dispatchQty'];
	$response['invValue'] 		= $row['invoiceValue'];
	$response['paymentRecive'] 	= $row['paymentRecive'];
	$response['revise_permision'] 	= $reviseMode;

	echo json_encode($response);



}






function getApprovedandPendingMrns($orderNo,$orderYear){

	global $db;
	$sql	= "SELECT DISTINCT
	ware_mrndetails.intMrnNo AS mrn_no,
	ware_mrndetails.intMrnYear AS year,
	ware_mrnheader.intStatus
FROM
	ware_mrndetails
INNER JOIN ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo
AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
WHERE ware_mrnheader.intStatus = 1 AND ware_mrnheader.intStatus NOT IN(0,-2)
AND ware_mrndetails.intOrderNo = '$orderNo' AND ware_mrndetails.intOrderYear = '$orderYear'
 ";
	$result = $db->RunQuery($sql);
	return $result;
}



/*
 * AUTHOR : HASITHA CHARAKA
 * 2018-02-21
 */
function getApprovedandPendingGrns($orderNo,$orderYear){

    global $db;
    $sql	= "SELECT
DISTINCT ware_grnheader.intGrnNo AS grnNo,
ware_grnheader.intGrnYear AS Year,
trn_prndetails.intOrderYear,
trn_prndetails.intOrderNo
FROM
ware_grnheader
INNER JOIN trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
INNER JOIN ware_grndetails ON ware_grndetails.intGrnNo = ware_grnheader.intGrnNo AND ware_grndetails.intGrnYear = ware_grnheader.intGrnYear
INNER JOIN trn_podetails ON trn_podetails.intPONo = trn_poheader.intPONo AND trn_podetails.intPOYear = trn_poheader.intPOYear
INNER JOIN trn_prnheader ON trn_prnheader.intPrnNo = trn_podetails.intPrnNo AND trn_prnheader.intYear = trn_podetails.intPrnYear
INNER JOIN trn_prndetails ON trn_prndetails.intPrnNo = trn_prnheader.intPrnNo AND trn_prndetails.intYear = trn_prnheader.intYear
WHERE ware_grnheader.intStatus = 1 AND   ware_grnheader.intStatus <> 0 AND trn_prndetails.intOrderYear = '$orderYear' AND  trn_prndetails.intOrderNo = '$orderNo'
";

    $result = $db->RunQuery($sql);
    return $result;


}


function getApprovedandPendingIssueNotes($orderNo,$orderYear){

    global $db;
    $sql	= "SELECT
	DISTINCT ware_issuedetails.intIssueNo AS IssueNo ,
	ware_issuedetails.intIssueYear AS Year,
	ware_issueheader.intStatus
FROM
	ware_issuedetails
INNER JOIN ware_issueheader ON ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo
AND ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear

WHERE
 ware_issueheader.intStatus = 1 AND  ware_issueheader.intStatus != 0
AND
ware_issuedetails.intOrderYear = '$orderYear' AND ware_issuedetails.strOrderNo = '$orderNo'";
    $result = $db->RunQuery($sql);
    return $result;


}

?>
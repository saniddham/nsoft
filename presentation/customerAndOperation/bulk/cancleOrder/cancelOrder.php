<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId	  		= $sessions->getLocationId();
?>
<head>
<title>Order Cancellation</title>
</head>

<body>
<form id="frmOrderCancle" name="frmOrderCancle" autocomplete="off">
<div align="center">
    <div class="trans_layoutD">
    <div class="trans_text">Order Cancellation</div>
    <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <tr>
    	<td><table width="100%" border="0" align="center">
        <tr>
        	<td width="16%" class="normalfnt">Order Year</td>
        	<td width="23%" class="normalfnt"><input type="text" name="txtOrderYear" id="txtOrderYear" style="width:100px" class="validate[required]" /></td>
        	<td width="16%" class="normalfnt">Order No</td>
        	<td width="25%" class="normalfnt"><input type="text" name="txtOrderNo" id="txtOrderNo" style="width:120px" class="validate[required]" /></td>
        	<td width="20%" class="normalfnt" style="text-align:right"><img src="images/Tview.jpg" alt="View" name="butView" width="92" height="24" border="0"  class="mouseover" id="butView" tabindex="9"/></td>
        </tr>
        <tr>
          <td class="normalfnt">&nbsp;</td>
          <td class="normalfnt">&nbsp;</td>
          <td class="normalfnt">&nbsp;</td>
          <td class="normalfnt">&nbsp;</td>
          <td class="normalfnt">&nbsp;</td>
        </tr>
        </table>
        </td>
    </tr>
    <tr>
        <td>
        <div id="divOrderDetails" class="clsDivOrderDetails" style="width:590px;display:none">
            <table width="100%" border="0" align="center" class="tableBorder">
            <tr>
            <td align="center" bgcolor="#E4E4E4" class="tableBorder"><b>
            <div class="clsStatus" style="color:#090;text-align:center"></div></b></td>
            </tr>
            <tr>
            <td>
            <table width="100%" border="0" align="center" cellspacing="5">
            <tr>
            	<td width="16%" class="normalfnt">Order Date</td>
            	<td width="1%" class="normalfnt">:</td>
                <td width="43%" style="font-weight:bold" class="normalfnt clsOrderDate"></td>
                <td width="18%" class="normalfnt">Order Qty</td>
                <td width="1%" class="normalfnt">:</td>
                <td width="21%" style="font-weight:bold" class="normalfnt clsOrderQty"></td>
            </tr>
            <tr>
            	<td class="normalfnt">Enter By</td>
            	<td class="normalfnt">:</td>
                <td style="font-weight:bold" class="normalfnt clsEnterBy"></td>
                <td class="normalfnt">Currency</td>
                <td class="normalfnt">:</td>
                <td style="font-weight:bold" class="normalfnt clsCurrency">&nbsp;</td>
            </tr>
            <tr>
            	<td class="normalfnt">Customer</td>
            	<td class="normalfnt">:</td>
                <td style="font-weight:bold" class="normalfnt clsCustomer">&nbsp;</td>
                <td class="normalfnt">Customer PONo</td>
                <td class="normalfnt">:</td>
                <td style="font-weight:bold" class="normalfnt clsPONo">&nbsp;</td>
            </tr>
            <tr>
            	<td class="normalfnt">Enter Location</td>
            	<td class="normalfnt">:</td>
                <td colspan="2" style="font-weight:bold" class="normalfnt clsLocation">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
            </tr>
          	</table>
            </td>
            </tr>
            </table>
            <table width="100%" border="0" align="center">
            <tr>
            <td></td>
            </tr>
            </table>
            <table width="100%" border="0" align="center" class="tableBorder">
            <tr>
                <td>
                    <table width="100%" border="0" align="center" cellspacing="2">
                    <tr>
                        <td width="16%" class="normalfnt">Fabric In Qty</td>
                        <td width="1%" class="normalfnt">:</td>
                        <td width="43%" style="color:#F00" class="normalfnt clsFabInQty"></td>
                        <td width="18%" class="normalfnt">Invoice Value</td>
                        <td width="1%" class="normalfnt">:</td>
                        <td width="21%" style="color:#F00" class="normalfnt clsInvoiceVal">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="normalfnt">Dispatch Qty</td>
                        <td class="normalfnt">:</td>
                        <td style="color:#F00" class="normalfnt clsDispatchQty">&nbsp;</td>
                        <td class="normalfnt">Payment Recived</td>
                        <td class="normalfnt">:</td>
                        <td style="color:#F00" class="normalfnt clsPayRecived">&nbsp;</td>
                    </tr>
                    </table>
                </td>
            </tr>
            </table>
            <table width="100%" border="0" align="center" class="tableBorder">
            <tr>
                <td>
                    <table width="100%" border="0" align="center" cellspacing="2">
                    <tr>
                        <td width="20%" class="normalfnt">Approved MRN</td>
                        <td width="2%" class="normalfnt">:</td>
                        <td class="normalfnt list_mrn">
                        <?php
							echo $list_mrn;
						?>
                        </td>
                      </tr>
                    <tr>
                        <td class="normalfnt">Approved Issue notes</td>
                        <td class="normalfnt">:</td>
                        <td class="normalfnt list_Issue">
<!--                            <a href="--><?php //echo $_SESSION['MAIN_URL'] ?><!--index.php?q=231&issueNo=85000132&year=2016" target="new">no</a></td>-->
                      </tr>
                    </table>
                </td>
            </tr>
            </table>
            <table width="100%" border="0" align="center" class="tableBorder">
            <tr>
                <td>
                    <table width="100%" border="0" align="center" cellspacing="2">
                    <tr>
                        <td width="20%" class="normalfnt">Approved GRN</td>
                        <td width="2%" class="normalfnt">:</td>
                        <td class="normalfnt list_grn">
                        <?php
							echo $list_grn;
						?>
                        </td>
                      </tr>
                    <tr>
                        <td class="normalfnt"></td>
                        <td class="normalfnt"></td>
                        <td class="normalfnt"></td>
                      </tr>
                    </table>
                </td>
            </tr>
            </table>
             </div>
        </td>
    </tr>
    </table>
    <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <tr>
    	<td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
    <tr>
    	<td width="100%" align="center" valign="top" bgcolor=""><a class="button white medium" id="butNew" name="butNew">&nbsp;&nbsp;New&nbsp;&nbsp;</a><a class="button white medium" id="butCancel" name="butCancel">Cancel</a><a href="main.php" class="button white medium" id="butClose" name="butClose">&nbsp;Close&nbsp;</a></td>
    </tr>
    </table></td>
    </tr>
    </table>
    </div>
</div>
</form>
</body>
</html>
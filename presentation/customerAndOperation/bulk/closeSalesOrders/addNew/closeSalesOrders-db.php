<?php 

	session_start();
	ini_set('display_errors',0);
	
try
{
	$mainPath 			= $_SESSION['mainPath'];
	$userId 			= $_SESSION['userId'];
	$location 			= $_SESSION['CompanyID'];
	$company 			= $_SESSION['headCompanyId'];
	$programCode		= 'P1167';
	
	require_once 		"class/cls_commonFunctions_get.php";					$objcomfunc 		= new 	            cls_commonFunctions_get($db);
	require_once 		"class/tables/trn_sales_order_close_details.php";	            
	           																	$trn_sales_order_close_details 		= new trn_sales_order_close_details($db);
	require_once 		"class/tables/trn_sales_order_close_header.php";	            
	           																	$trn_sales_order_close_header 		= new trn_sales_order_close_header($db);
	require_once 		"class/tables/mst_part.php";							$mst_part 		= new mst_part($db);
	require_once 		"class/tables/mst_technique_groups.php";				$mst_technique_groups 		= new mst_technique_groups($db);
require_once 			"class/tables/menupermision.php";						$menupermision	= new menupermision($db);
require_once 			"class/tables/trn_orderdetails.php";					$trn_orderdetails	= new trn_orderdetails($db);
require_once 			"class/tables/trn_sales_order_close_approvedby.php";	$trn_sales_order_close_approvedby	= new trn_sales_order_close_approvedby($db);
require_once 			"class/dateTime.php";									$dateTimes	= new dateTimes($db);
require_once 			"class/tables/trn_orderheader.php";						$trn_orderheader	= new trn_orderheader($db);
require_once 			"class/tables/trn_orderdetails.php";					$trn_orderdetails	= new trn_orderdetails($db);
require_once 			"class/tables/sys_no.php";								$sys_no	= new sys_no($db);
require_once 			"class/tables/sys_approvelevels.php";					$sys_approvelevels	= new sys_approvelevels($db);


	//--------------------------------------------------

	$response 			= array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 		= $_REQUEST['requestType'];
	//$programName		='Dispatch Note';

//------------save---------------------------	
	if($requestType=='loadOrderNos')
	{
		$orderYear 					= $_REQUEST['orderYear'];
		$pending_reqCombo			= $trn_orderdetails->getOpenedOrderNos($orderYear,'');
		$response['orderNoCombo']	= $pending_reqCombo;
		
	}
	else if($requestType=='loadCustomerPO')
	{
		$orderYear 					= $_REQUEST['orderYear'];
		$orderNo 					= $_REQUEST['orderNo'];
		$trn_orderheader->set($orderNo,$orderYear);
		$customerPO					= $trn_orderheader->getstrCustomerPoNo();
		$response['customerPO']		= $customerPO;
		
	}
	else if($requestType=='loadSalesOrderDetails')
	{
		$orderYear 				= $_REQUEST['orderYear'];
		$orderNo				= $_REQUEST['orderNo'];
		$serialYear 			= $_REQUEST['serialYear'];
		$serialNo 				= $_REQUEST['serialNo'];
		
		$result					= $trn_orderdetails->getOpenedOrderDetails($orderYear,$orderNo,$serialNo,$serialYear);
		$tableHtml				='';
		ob_start();
		?>
        <tr class="">
          <th nowrap="nowrap">Close</th>
          <th nowrap="nowrap" >Status</th>
          <th nowrap="nowrap" >Sales Order No </th>
          <th nowrap="nowrap" >Graphic No</th>
          <th nowrap="nowrap" >Sample No</th>
          <th nowrap="nowrap" >Style</th>
          <th nowrap="nowrap" >Combo</th>
          <th nowrap="nowrap" >Part</th>
          <th nowrap="nowrap" >Print</th>
          <th nowrap="nowrap" >Revision</th>
          <th nowrap="nowrap">Order Qty</th>
          <th nowrap="nowrap" >Price</th>
          <th nowrap="nowrap" >Value</th>
          <th nowrap="nowrap" >Technique Group</th> 
          </tr>
        <?php
		while($row=mysqli_fetch_array($result))
		{
			$mst_part->set($row['intPart']);
			$part		= $mst_part->getstrName();
			$mst_technique_groups->set($row['TECHNIQUE_GROUP_ID']);
			$techGrp	= $mst_technique_groups->getTECHNIQUE_GROUP_NAME();
			if($row['SAVED_STATUS']==1)
				$chk_saved = ' checked="checked"';
			else
				$chk_saved = '';
		?>	
            <tr class="normalfnt" id="dataRow" >
                <td height="49" align="center" bgcolor="#FFFFFF" nowrap="nowrap"><input id="chkClose" type="checkbox" class="close" <?php echo $chk_saved; ?>></td>
                <td bgcolor="#FFFFFF" align="center" nowrap="nowrap"><?php echo $row['STATUS']; ?></td>
                <td bgcolor="#FFFFFF" align="center" nowrap="nowrap" id="<?php echo $row['intSalesOrderId']; ?>" class="salesOrder"><?php echo $row['strSalesOrderNo']; ?></td>
                <td bgcolor="#FFFFFF" align="center" nowrap="nowrap"><?php echo $row['strGraphicNo']; ?></td>
                <td bgcolor="#FFFFFF" align="center" nowrap="nowrap"><?php echo $row['intSampleYear']."/".$row['intSampleNo']; ?></td>
                <td bgcolor="#FFFFFF" align="center" nowrap="nowrap"><?php echo $row['strStyleNo']; ?></td>
                <td bgcolor="#FFFFFF" align="center" nowrap="nowrap"><?php echo $row['strCombo']; ?></td>
                <td bgcolor="#FFFFFF" align="center" nowrap="nowrap"><?php echo $part; ?></td>
                <td bgcolor="#FFFFFF" align="center" nowrap="nowrap"><?php echo $row['strPrintName']; ?></td>
                <td bgcolor="#FFFFFF" align="center" nowrap="nowrap"><?php echo $row['intRevisionNo']; ?></td>
                <td bgcolor="#FFFFFF" align="right" nowrap="nowrap"><?php echo $row['intQty']; ?></td>
                <td bgcolor="#FFFFFF" align="right" nowrap="nowrap"><?php echo $row['dblPrice']; ?></td>
                <td bgcolor="#FFFFFF" align="right" nowrap="nowrap"><?php echo $row['intQty']*$row['dblPrice']; ?></td>
                <td bgcolor="#FFFFFF" align="center" nowrap="nowrap"><?php echo $techGrp; ?></td>
              </tr>		<?php
        }
 		$tableHtml	.= ob_get_clean();
		$response['tableHtml']	= $tableHtml;
	}
	else if($requestType=='save')
	{
		$db->connect();$db->begin();//close connection.
	
		//check user permision to save 
		//get sys max no
		//update max status
		//check valid status of order
		//save header data
		//save detail data
		//check valid status of sales order 
		$arrHeader		= json_decode($_REQUEST['arrHeader'],true);
		$orderNo		= $arrHeader['orderNo'];
		$orderYear		= $arrHeader['orderYear'];
		
		$editMode		= false;
		$arrDetails 	= json_decode($_REQUEST['arrDetails'],true);
		
		//check save permission	
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Edit','','');
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);

		//get sys max no
		$serialNo		= $sys_no->getSerialNoAndUpdateSysNo('CLOSE_ORDER_NO',$sessions->getLocationId());
		$serialYear		= $dateTimes->getCurruntYear();
		
		//check order status
		$trn_orderheader->set($orderNo,$orderYear);
		if(!$trn_orderheader->getintStatus())
			throw new Exception('This is not an approved order');
		
		//get and update sys_approvelevels values
		$sys_approvelevels->set($programCode);
		$approveLevels	= $sys_approvelevels->getintApprovalLevel();
		$status			= $approveLevels+1; 
	
		// save header data
		$result_arr		= $trn_sales_order_close_header->insertRec($serialNo,$serialYear,$orderNo,$orderYear,$dateTimes->getCurruntDateTime(),$sessions->getUserId(),$dateTimes->getCurruntDateTime(),$sessions->getUserId(),$approveLevels,$status,$sessions->getLocationId());
		
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);

		//check button permissions
		$trn_sales_order_close_header->set($serialNo,$serialYear);
		$status				= $trn_sales_order_close_header->getSTATUS();
		$approveLevels		= $trn_sales_order_close_header->getAPPROVE_LEVELS();
		
		$menupermision->set($programCode,$sessions->getUserId());
		$approvePermArr		= $menupermision->checkMenuPermision('Approve',$status,$approveLevels);
		$savePermArr		= $menupermision->checkMenuPermision('Edit',$status,$approveLevels);
		
		$saved	=0;
		foreach($arrDetails as $array_loop)
		{
			$saved++;
			$orderNo		= $array_loop['orderNo'];
			$orderYear		= $array_loop['orderYear'];
			$salesOrder		= $array_loop['salesOrder'];
			
			//check sales order status
			$trn_orderdetails->set($orderNo,$orderYear,$salesOrder);
			if($trn_orderdetails->getSTATUS()== -10)
				throw new Exception('This sales order is already closed');
				
			$trn_sales_order_close_details->insertRec($serialNo,$serialYear,$orderNo,$orderYear,$salesOrder);
		
		}
		if($saved==0)
			throw new Exception('Detail saving error');
		
		$db->commit();
		
		$response['type'] 			= "pass";
		$response['msg'] 			= "Saved Successfully.";
			
		$response['serialNo']		= $serialNo;
		$response['serialYear']		= $serialYear;
		$response['status']			= $status;
		$response['approvePerm']	= $approvePermArr['type'];
		$response['savePerm']		= $savePermArr['type'];

	}
	else if($requestType=='update')
	{
		$db->connect();$db->begin();//close connection.
		//check user permision to save 
		//check valid status of order
		//update header data
		//delete detail data
		//save detail data
		//check valid status of sales order 
		
		$arrHeader		= json_decode($_REQUEST['arrHeader'],true);
		$serialNo		= $arrHeader['serialNo'];
		$serialYear		= $arrHeader['serialYear'];
		$orderNo		= $arrHeader['orderNo'];
		$orderYear		= $arrHeader['orderYear'];
		
		$editMode		= false;
		$arrDetails 	= json_decode($_REQUEST['arrDetails'],true);
		
		// set trn_sales_order_close_header values
		$trn_sales_order_close_header->set($serialNo,$serialYear);
	
		//check location with saved location when edit
		if($trn_sales_order_close_header->getLOCATION_ID()!=$sessions->getLocationId())
			throw new Exception("This is not the saved location");
			
		//check valid status of header table
		$status			= $trn_sales_order_close_header->getSTATUS();
		$approveLevels	= $trn_sales_order_close_header->getAPPROVE_LEVELS();
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Edit',$status,$approveLevels);
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);
		
		// set sys_approvelevels values
		$sys_approvelevels->set($programCode);
		$approveLevels		= $sys_approvelevels->getintApprovalLevel();
		$status				= $approveLevels+1;
	
		//update header data
		$trn_sales_order_close_header->setLOCATION_ID($sessions->getLocationId());
		$trn_sales_order_close_header->setSTATUS($status);
		$trn_sales_order_close_header->setAPPROVE_LEVELS($approveLevels);
		$trn_sales_order_close_header->setMODIFIED_DATE(date("Y-m-d H:i:s"));
		$trn_sales_order_close_header->setMODIFIED_BY($_SESSION['userId']);
		$result_arr	= $trn_sales_order_close_header->commit();
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
		
		//update max status in approve by table
		$trn_sales_order_close_approvedby->updateMaxStatus($serialNo,$serialYear);
		
		//delete issue to production detail data
		$result_arr = $trn_sales_order_close_details->delete(" CLOSE_NO = $serialNo AND CLOSE_YEAR = $serialYear ");
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
	
		//check status for final approval
		$trn_sales_order_close_header->set($serialNo,$serialYear);
		$status				= $trn_sales_order_close_header->getSTATUS();
		$approveLevels		= $trn_sales_order_close_header->getAPPROVE_LEVELS();
		$menupermision->set($programCode,$sessions->getUserId());
		$approvePermArr		= $menupermision->checkMenuPermision('Approve',$status,$approveLevels);
		$savePermArr		= $menupermision->checkMenuPermision('Edit',$status,$approveLevels);
		
		$saved	=0;
		foreach($arrDetails as $array_loop)
		{
			$saved++;
			$orderNo		= $array_loop['orderNo'];
			$orderYear		= $array_loop['orderYear'];
			$salesOrder		= $array_loop['salesOrder'];
			
			//check sales order status
			$trn_orderdetails->set($orderNo,$orderYear,$salesOrder);
			if($trn_orderdetails->getSTATUS()== -10)
				throw new Exception('This sales order is already closed');
				
			$trn_sales_order_close_details->insertRec($serialNo,$serialYear,$orderNo,$orderYear,$salesOrder);
		
		}
		if($saved==0)
			throw new Exception('Detail saving error');
		
		$db->commit();
		$response['msg'] 			= "Updated Successfully.";
		$response['type'] 			= "pass";
		$response['serialNo']		= $serialNo;
		$response['serialYear']		= $serialYear;
		$response['status']			= $status;
		$response['approvePerm']	= $approvePermArr['type'];
		$response['savePerm']		= $savePermArr['type'];
	
	}
	
	else if($requestType=='loadHeader')
	{
		
 		$db->connect();$db->begin();//open connection.
		$serialNo 					= $_REQUEST['serialNo'];
		$serialYear					= $_REQUEST['serialYear'];
		
		$date						= date("Y-m-d");
		
		if($serialNo >0 && $serialYear > 0){
			$trn_sales_order_close_header->set($serialNo,$serialYear);
			$orderNo					= $trn_sales_order_close_header->getORDER_NO();
			$orderYear					= $trn_sales_order_close_header->getORDER_YEAR();
			$date						= substr($trn_sales_order_close_header->getCREATED_DATE(),0,10);
		}
		$trn_orderheader->set($orderNo,$orderYear);
		$customerPO					= $trn_orderheader->getstrCustomerPoNo();
		
		$response['orderYearCombo']	= $trn_orderdetails->getOpenedOrderYears($orderYear);
		$response['orderNoCombo']	= $trn_orderdetails->getOpenedOrderNos($orderYear,$orderNo);
		$response['date']			= $date;
		$response['customerPO']		= $customerPO;

	}
	else if($requestType=="approve")
	{
		// check approval permission
		// check status
		// check save location and is production location
		// check qty with stock
		
		$db->connect();$db->begin();//open connection.
		
		
		$serialNo			= $_REQUEST['serialNo'];
		$serialYear			= $_REQUEST['serialYear'];
		
		// set trn_sales_order_close_header table
		$trn_sales_order_close_header->set($serialNo,$serialYear);
		$status				= $trn_sales_order_close_header->getSTATUS();
		$approveLevels		= $trn_sales_order_close_header->getAPPROVE_LEVELS();
		
		//check location with saved location when edit
		if($trn_sales_order_close_header->getLOCATION_ID()!=$sessions->getLocationId())
			throw new Exception("This is not the saved location");
				
		//check permission
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Approve',$status,$approveLevels);
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);
			
		// update header status function
		$where		= 'CLOSE_NO = "'.$serialNo.'" AND CLOSE_YEAR = "'.$serialYear.'" ' ;
		$data  		= array('STATUS' => '-1');
		
		$resultArr	= $trn_sales_order_close_header->upgrade($data,$where); // upgrade header status
		if(!$resultArr['status'])
			throw new Exception($resultArr['msg']);

		 // update approve by table
		$trn_sales_order_close_header->set($serialNo,$serialYear);
		$status		= $trn_sales_order_close_header->getSTATUS();
		$level		= $trn_sales_order_close_header->getAPPROVE_LEVELS();
		$approval	= $level+1-$status;
		
		$result_arr	= $trn_sales_order_close_approvedby->insertRec($serialNo,$serialYear,$approval,$sessions->getUserId(),$dateTimes->getCurruntDateTime(),0);
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
		
		$result	   = $trn_sales_order_close_details->getSavedDetails($serialNo,$serialYear);
		while($row = mysqli_fetch_array($result))
		{
			//$db->connect();$db->begin();//open connection.
			$trn_orderdetails->set($row['intOrderNo'],$row['intOrderYear'],$row['intSalesOrderId']);
			
			if($trn_orderdetails->getSTATUS()==-10)
				throw new Exception($trn_orderdetails->getstrSalesOrderNo().' is not a opened sales order');
			
			if($status==1)
			 {
				$where		= 'intOrderNo = "'.$row['intOrderNo'].'" AND intOrderYear = "'.$row['intOrderYear'].'" AND intSalesOrderId = "'.$row['intSalesOrderId'].'" ' ;
				$data  		= array('STATUS' => '-10');
				
				$resultArr	= $trn_orderdetails->update($data,$where); // upgrade header status
				if(!$resultArr['status'])
					throw new Exception($resultArr['msg']);
			}
	
		}
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Approved Successfully.";
	}
	else if($requestType=="reject")
	{
		$db->connect();$db->begin();//open connection.
		
			
		//requested parameters
		$serialNo			= $_REQUEST['serialNo'];
		$serialYear			= $_REQUEST['serialYear'];
		// set trn_sales_order_close_header table
		$trn_sales_order_close_header->set($serialNo,$serialYear);
		$status				= $trn_sales_order_close_header->getSTATUS();
		$approveLevels		= $trn_sales_order_close_header->getAPPROVE_LEVELS();
		
		//check location with saved location when edit
		if($trn_sales_order_close_header->getLOCATION_ID()!=$sessions->getLocationId())
			throw new Exception("This is not the saved location");
		
		//check permission
		$menupermision->set($programCode,$sessions->getUserId());
		$permissionArr	= $menupermision->checkMenuPermision('Reject',$status,$approveLevels);
		
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);
		
		// update header status function
		$trn_sales_order_close_header->setSTATUS(0);
		$result_arr	= $trn_sales_order_close_header->commit();
		
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
			
		// update approve by table
		$result_arr	= $trn_sales_order_close_approvedby->insertRec($serialNo,$serialYear,0,$sessions->getUserId(),$dateTimes->getCurruntDateTime(),0);
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
		
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Rejected Successfully.";
	}

}
catch(Exception $e)
{
	$db->rollback();
	$response['msg'] 	=  $e->getMessage();;
	$response['error'] 	=  $error_handler->jTraceEx($e);
	$response['type'] 	=  'fail';
	$response['sql']	=  $db->getSql();
//	$db->disconnect();
}
$db->disconnect();
echo json_encode($response);



?>

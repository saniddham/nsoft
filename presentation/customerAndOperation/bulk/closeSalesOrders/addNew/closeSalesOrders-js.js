	
	loadSavedDetails($('#frmCloseSalesOrder #txtSerialNo').val(),$('#frmCloseSalesOrder #txtYear').val());

$(document).ready(function(){	


	$("#frmCloseSalesOrder #cboOrderYear").die('change').live('change',function(){
		loadOrderNos();
	});
	$("#frmCloseSalesOrder #cboOrderNo").die('change').live('change',function(){
		loadCustomerPO();
		loadSalesOrderDetails();
	});
	$("#frmCloseSalesOrder #butSave").die('click').live('click',function(){
		saveMain();
	});
	$("#frmCloseSalesOrder #butNew").die('click').live('click',function(){
		loadNewPage();
	});
	$("#frmCloseSalesOrder #butConfirm").die('click').live('click',function(){
		viewReport('Confirm');
	});
	$("#frmCloseSalesOrder #butReport").die('click').live('click',function(){
		viewReport('');
	});
	$("#frmCloseOrdersRpt #butRptConfirm").die('click').live('click',function(){
		approve();
	});
	$("#frmCloseOrdersRpt #butRptReject").die('click').live('click',function(){
		reject();
	});
	
});

function loadOrderNos(){
	
	var orderYear 	= $('#frmCloseSalesOrder #cboOrderYear').val();
	
	if(orderYear==''){
		$('#frmCloseSalesOrder #cboOrderYear').val('');
		$('#frmCloseSalesOrder #cboOrderNo').html('');
		return;
	}
	var url   		= "controller.php?q=1167&requestType=loadOrderNos";
	var data		= "orderYear="+orderYear;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmCloseSalesOrder #cboOrderNo').html(json.orderNoCombo);
			}
	});	
}

function loadCustomerPO(){
	
	var orderYear 	= $('#frmCloseSalesOrder #cboOrderYear').val();
	var orderNo 	= $('#frmCloseSalesOrder #cboOrderNo').val();
	
	var url   		= "controller.php?q=1167&requestType=loadCustomerPO";
	var data		= "orderYear="+orderYear+"&orderNo="+orderNo;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmCloseSalesOrder #txtCustomerPO').val(json.customerPO);
			}
	});	
}

function loadOrders(){
	
	var orderYear 	= $('#frmCloseSalesOrder #cboOrderYear').val();
	var orderNo	 	= $('#frmCloseSalesOrder #cboOrderNo').val();
	
	if(orderYear==''){
		$('#frmCloseSalesOrder #cboOrderYear').val('');
		$('#frmCloseSalesOrder #cboOrderNo').val('');
		return;
	}
	if(orderNo==''){
		$('#frmCloseSalesOrder #cboOrderYear').val('');
		$('#frmCloseSalesOrder #cboOrderNo').val('');
		return;
	}
	var url   		= "controller.php?q=1167&requestType=loadOrders";
	var data		= "orderYear="+orderYear;
		data		+= "&orderNo="+orderNo;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmCloseSalesOrder #cboOrderYear').html(json.orderYearCombo);
				$('#frmCloseSalesOrder #cboOrderNo').html(json.orderNoCombo);
			}
	});	
}

function loadSalesOrderDetails(){
	
	var orderYear 	= $('#frmCloseSalesOrder #cboOrderYear').val();
	var orderNo	 	= $('#frmCloseSalesOrder #cboOrderNo').val();
	
	if(orderYear==''){
		$('#frmCloseSalesOrder #cboOrderYear').val('');
		$('#frmCloseSalesOrder #cboOrderNo').val('');
		return;
	}
	if(orderNo==''){
		$('#frmCloseSalesOrder #cboOrderYear').val('');
		$('#frmCloseSalesOrder #cboOrderNo').val('');
		return;
	}
	
	var url   		= "controller.php?q=1167&requestType=loadSalesOrderDetails";
	var data		= "orderYear="+orderYear;
		data		+= "&orderNo="+orderNo;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmCloseSalesOrder #tblSalesOrders').html(json.tableHtml);
			}
	});	
}

function loadSavedDetails(serialNo,serialYear){
	
	$('#frmCloseSalesOrder #txtSerialNo').val(serialNo);
	$('#frmCloseSalesOrder #txtYear').val(serialYear);
	
	loadHeader(serialNo,serialYear);
	loadDetails(serialNo,serialYear);
	
}

function loadHeader(serialNo,serialYear){

	var url   		= "controller.php?q=1167&requestType=loadHeader";
	var data		= "serialNo="+serialNo;
		data		+= "&serialYear="+serialYear;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmCloseSalesOrder #cboOrderNo').html(json.orderNoCombo);
				$('#frmCloseSalesOrder #cboOrderYear').html(json.orderYearCombo);
				$('#frmCloseSalesOrder #dtDate').val(json.date);
				$('#frmCloseSalesOrder #txtCustomerPO').val(json.customerPO);
			}
	});	
	
}


function loadDetails(serialNo,serialYear){
	
	var url   		= "controller.php?q=1167&requestType=loadSalesOrderDetails";
	var data		= "serialNo="+serialNo;
		data		+= "&serialYear="+serialYear;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmCloseSalesOrder #tblSalesOrders').html(json.tableHtml);
			}
	});	
}

function saveMain(){
	
	var serialNo	=$('#frmCloseSalesOrder #txtSerialNo').val();
	var serialYear	= $('#frmCloseSalesOrder #txtYear').val();

	if(serialNo=='' && serialYear=='')
		save();
	else
		update();
	
}

function save(){
	
	showWaiting();
	
	var chkStatus		= false;
	var arrDetails		= "";
	var numberValidate 	= 0;
	
	var orderNo		= $('#frmCloseSalesOrder #cboOrderNo').val();
	var orderYear	= $('#frmCloseSalesOrder #cboOrderYear').val();

	var data = "requestType=save";
	
	var arrHeader 	= "{";
							arrHeader += '"orderNo":"'+orderNo+'",' ;
							arrHeader += '"orderYear":'+orderYear+'';
							
		arrHeader  += "}";
		
	$('#frmCloseSalesOrder #tblSalesOrders .close:checked').each(function(){
		var salesOrder		= $(this).parent().parent().find('.salesOrder').attr('id');
			arrDetails += "{";
			arrDetails += '"orderNo":"'+ orderNo +'",' ;
			arrDetails += '"orderYear":"'+ orderYear +'",' ;
			arrDetails += '"salesOrder":'+salesOrder+'';
			arrDetails += "},";

	});
	
	arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
	var arrDetails	= '['+arrDetails+']';
	
	data		   +="&arrHeader="+arrHeader;
	data		   +="&arrDetails="+arrDetails;

	var url   = "controller.php?q=1167";	
	$.ajax({
			url:url,
			dataType:'json',
			type:'post',
			data:data,
			async:false,
			success:function(json){
				$('#frmCloseSalesOrder #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				
				if(json.type=='pass')
				{
					hideWaiting();
					$('#frmCloseSalesOrder #txtSerialNo').val(json.serialNo);
					$('#frmCloseSalesOrder #txtYear').val(json.serialYear);
					if(json.savePerm==1){
						$('#frmCloseSalesOrder #butSave').show();
					}
					else{
						$('#frmCloseSalesOrder #butSave').hide();
					}
					if(json.approvePerm==1){
						$('#frmCloseSalesOrder #butConfirm').show();
					}
					else{
						$('#frmCloseSalesOrder #butConfirm').hide();
					}
					
					return;
				}
				else
				{
					hideWaiting();
				}
			},
			error:function(xhr,status)
			{
				$('#frmCloseSalesOrder #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				hideWaiting();
				return;;
			}
	});
	hideWaiting();	
}

function update(){

	showWaiting();
	
	var chkStatus		= false;
	var arrDetails		= "";
	var numberValidate 	= 0;
	
	var serialNo	= $('#frmCloseSalesOrder #txtSerialNo').val();
	var serialYear	= $('#frmCloseSalesOrder #txtYear').val();
	var orderNo		= $('#frmCloseSalesOrder #cboOrderNo').val();
	var orderYear	= $('#frmCloseSalesOrder #cboOrderYear').val();

	var data = "requestType=update";
	
	var arrHeader 	= "{";
							arrHeader += '"serialNo":"'+serialNo+'",' ;
							arrHeader += '"serialYear":"'+serialYear+'",' ;
							arrHeader += '"orderNo":"'+orderNo+'",' ;
							arrHeader += '"orderYear":'+orderYear+'';
							
		arrHeader  += "}";
		
	$('#frmCloseSalesOrder #tblSalesOrders .close:checked').each(function(){
		var salesOrder	= $(this).parent().parent().find('.salesOrder').attr('id');
			arrDetails += "{";
			arrDetails += '"orderNo":"'+ orderNo +'",' ;
			arrDetails += '"orderYear":"'+ orderYear +'",' ;
			arrDetails += '"salesOrder":'+salesOrder+'';
			arrDetails += "},";

	});
	
	arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
	var arrDetails	= '['+arrDetails+']';
	
	data		   +="&arrHeader="+arrHeader;
	data		   +="&arrDetails="+arrDetails;

	var url   = "controller.php?q=1167";	
	$.ajax({
			url:url,
			dataType:'json',
			type:'post',
			data:data,
			async:false,
			success:function(json){
				$('#frmCloseSalesOrder #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				
				if(json.type=='pass')
				{
					hideWaiting();
					
					$('#frmCloseSalesOrder #txtSerialNo').val(json.serialNo);
					$('#frmCloseSalesOrder #txtYear').val(json.serialYear);
					if(json.savePerm==1){
						$('#frmCloseSalesOrder #butSave').show();
					}
					else{
						$('#frmCloseSalesOrder #butSave').hide();
					}
					if(json.approvePerm==1){
						$('#frmCloseSalesOrder #butConfirm').show();
					}
					else{
						$('#frmCloseSalesOrder #butConfirm').hide();
					}
					return;
				}
				else
				{
					hideWaiting();
				}
			},
			error:function(xhr,status)
			{
				$('#frmCloseSalesOrder #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				hideWaiting();
				return;;
			}
	});
	hideWaiting();	
	
}

function viewReport(type){
	
	if($('#frmCloseSalesOrder #txtSerialNo').val()=="") 
		return;
	var url = "?q=1168&serialNo="+$('#frmCloseSalesOrder #txtSerialNo').val()+"&serialYear="+$('#frmCloseSalesOrder #txtYear').val()+"&mode="+type;
	window.open(url,'closeSalesOrders-report.php');
	
	
}

function approve()
{
	var val = $.prompt('Are you sure you want to approve this Sales Order Closing ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
			showWaiting();
			var url = "controller.php?q=1167&"+window.location.search+'&requestType=approve';
			var obj = $.ajax({
				url:url,
				type:'post',
				dataType: "json",  
				data:'',
				async:false,						
				success:function(json){
					$('#frmCloseOrdersRpt #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertR1()",1000);
						window.location.href = window.location.href;
						window.opener.location.reload();
						return;
					}
					 hideWaiting();
				},
				error:function(xhr,status){						
						$('#frmCloseOrdersRpt #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertR1()",3000);
						 hideWaiting();
				}		
			});	
		}			  
	}});
}

function reject()
{
	var val = $.prompt('Are you sure you want to reject this Sales Order Closing ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = "controller.php?q=1167&"+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,						
						success:function(json){
								$('#frmCloseOrdersRpt #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertR2()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmCloseOrdersRpt #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertR2()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}

function loadNewPage(){
	
	window.location.href = "?q=1167";
	
}


function alertR1()
{
	$('#frmCloseOrdersRpt #butRptConfirm').validationEngine('hide');
}
function alertR2()
{
	$('#frmOpenOrdersRpt #butRptReject').validationEngine('hide');
}
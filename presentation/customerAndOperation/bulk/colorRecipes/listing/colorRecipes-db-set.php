<?php 

	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];

	include "{$backwardseperator}dataAccess/Connector.php";
	
	$requestType 			= $_REQUEST['requestType'];
    $sampleNo 				= $_REQUEST['sampleNo'];
    $sampleYear 			= $_REQUEST['sampleyear'];
    $revNo 					= $_REQUEST['revNo'];
    $combo 					= $_REQUEST['combo'];
    $printName 				= $_REQUEST['print'];
    $recipeRevision 		= $_REQUEST['bulkRev'];

    if($requestType=='approveRecipe')
    {
        $toSave=0;
        $saved=0;
        $db->begin();

        $errFlag=0;

        $sql 	= "SELECT STATUS, approvalLvl
		FROM trn_sample_color_recipes_bulk_header 
		WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName' and recipeRevision='$recipeRevision'";
        $result = $db->RunQuery2($sql);
        $row	=mysqli_fetch_array($result);
        $headerStatus = $row['STATUS'];
        $initialApprovalLvls = $row['approvalLvl'];
        $currentApprovalLvl = $initialApprovalLvls + 2 - $headerStatus;
        $userPermission = 1; // get form methos

        if ($userPermission ==1){
            $toSave++;
            $sql 		= "UPDATE `trn_sample_color_recipes_bulk_header` SET `STATUS` = STATUS-1 WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName' and recipeRevision='$recipeRevision'";
            $result_header	=$db->RunQuery2($sql);
            if ($result_header){
                $saved++;
            }
            $approvCount = 0;
            if ($currentApprovalLvl ==1){
                $sql = "SELECT MAX(intStatus) as revCount FROM trn_sample_color_recipes_bulk_approve  WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName' and recipeRevision='$recipeRevision' and intApproveLevelNo = $currentApprovalLvl";
                $result = $db->RunQuery2($sql);
                $row	=mysqli_fetch_array($result);
                $approvCount = $row['revCount'] + 1;
            } else {
                $queryAppLvl = $currentApprovalLvl-1;
                $sql = "SELECT MAX(intStatus) as revCount FROM trn_sample_color_recipes_bulk_approve  WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName' and recipeRevision='$recipeRevision' and intApproveLevelNo = $queryAppLvl";
                $result = $db->RunQuery2($sql);
                $row	=mysqli_fetch_array($result);
                $approvCount = $row['revCount'];
            }
            $toSave++;
            $sql = "INSERT INTO `trn_sample_color_recipes_bulk_approve` 
            (`intSampleNo`,`intSampleYear`,`intRevisionNo`,`strCombo`,`strPrintName`,`recipeRevision`,`intApproveLevelNo`,`dtmdateApproved`,`intUser`,`intStatus`) 
            VALUES ('$sampleNo','$sampleYear','$revNo','$combo','$printName', '$recipeRevision','$currentApprovalLvl',now(),'$userId','$approvCount')";
            $result 	= $db->RunQuery2($sql);
            if ($result){
                $saved++;
            }
        }

        if(($toSave==$saved)){
            $db->RunQuery2('Commit');
            $response['type'] 		= 'pass';
            $response['revision']	= 'pass';
            $response['msg'] 		= 'Saved successfully.';
        } else{
            $db->RunQuery2('Rollback');
            $response['type'] 		= 'fail';
            $response['msg'] 		= $db->errormsg;
            $response['q'] 			= $sql;
        }
        $db->commit();
        echo json_encode($response);
    }

    else if($requestType=='rejectRecipe')
    {
        $toSave=0;
        $saved=0;
        $db->begin();

        $errFlag=0;

        $sql 	= "SELECT STATUS, approvalLvl
		FROM trn_sample_color_recipes_bulk_header 
		WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName' and recipeRevision='$recipeRevision'";
        $result = $db->RunQuery2($sql);
        $row	=mysqli_fetch_array($result);
        $headerStatus = $row['STATUS'];
        $initialApprovalLvls = $row['approvalLvl'];
        $currentApprovalLvl = $initialApprovalLvls + 2 - $headerStatus;
        $userPermission = 1; // get form methos

        if ($userPermission ==1){
            $rejectStatus = -1;
            $toSave++;
            $sql 		= "UPDATE `trn_sample_color_recipes_bulk_header` SET `STATUS` = '$rejectStatus' WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName' and recipeRevision='$recipeRevision'";
            $result_header	=$db->RunQuery2($sql);
            if ($result_header){
                $saved++;
            }
            $approvCount = 0;
            $queryAppLvl = $currentApprovalLvl-1;
            $sql = "SELECT MAX(intStatus) as revCount FROM trn_sample_color_recipes_bulk_approve  WHERE intSampleNo='$sampleNo' and intSampleYear='$sampleYear' and intRevisionNo='$revNo' and strCombo='$combo' and strPrintName='$printName' and recipeRevision='$recipeRevision' and intApproveLevelNo = $queryAppLvl";
            $result = $db->RunQuery2($sql);
            $row	=mysqli_fetch_array($result);
            $approvCount = $row['revCount'];

            $toSave++;
            $rejectStatus++;
            $sql = "INSERT INTO `trn_sample_color_recipes_bulk_approve` 
            (`intSampleNo`,`intSampleYear`,`intRevisionNo`,`strCombo`,`strPrintName`,`recipeRevision`,`intApproveLevelNo`,`dtmdateApproved`,`intUser`,`intStatus`) 
            VALUES ('$sampleNo','$sampleYear','$revNo','$combo','$printName', '$recipeRevision','$rejectStatus',now(),'$userId','$approvCount')";
            $result 	= $db->RunQuery2($sql);
            if ($result){
                $saved++;
            }
        }

        if(($toSave==$saved)){
            $db->RunQuery2('Commit');
            $response['type'] 		= 'pass';
            $response['revision']	= 'pass';
            $response['msg'] 		= 'Reject successfully.';
        } else{
            $db->RunQuery2('Rollback');
            $response['type'] 		= 'fail';
            $response['msg'] 		= $db->errormsg;
            $response['q'] 			= $sql;
        }
        $db->commit();
        echo json_encode($response);
    }
?>
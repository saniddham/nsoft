<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId 				= $_SESSION['CompanyID'];
$userId 				= $_SESSION['userId'];
$locationId 			= $companyId;
$programCode			= 'P1298';

include_once  "class/cls_commonErrorHandeling_get.php";

$obj_commonErrHandle 	= new cls_commonErrorHandeling_get($db);

$sampleNo 				= $_REQUEST['sampleNo'];
$sampleYear 			= $_REQUEST['sampleyear'];
$revNo 					= $_REQUEST['revNo'];
$combo 					= $_REQUEST['combo'];
$print 					= $_REQUEST['print'];
$bulkRev 				= $_REQUEST['bulkRev'];
$approveMode 			= $_REQUEST['approveMode'];
$mode 			    	= $_REQUEST['mode'];

$sql = "SELECT alphabatic FROM mst_number_mapping WHERE intNumber= '$bulkRev'";
$result 			= $db->RunQuery($sql);
$number_array		= mysqli_fetch_array($result);
$bulkRevName		= $number_array['alphabatic'];
if($approveMode=='confirm')
    $mode = 'Confirm';
else if($approveMode=='cancle')
    $mode = 'Cancel';

$sql = "SELECT trn_sample_color_recipes_bulk_header.STATUS,trn_sample_color_recipes_bulk_header.createdDate,
        trn_sample_color_recipes_bulk_header.approvalLvl,trn_sample_color_recipes_bulk_header.createdBy, 
        trn_sample_color_recipes_bulk_header.createdDate,sys_users.strUserName FROM	trn_sample_color_recipes_bulk_header
        LEFT JOIN sys_users on sys_users.intUserId=trn_sample_color_recipes_bulk_header.createdBy  WHERE
        trn_sample_color_recipes_bulk_header.intSampleNo 		= '$sampleNo' 	AND
        trn_sample_color_recipes_bulk_header.intSampleYear 	= '$sampleYear'	AND
        trn_sample_color_recipes_bulk_header.intRevisionNo 		= '$revNo' 		AND
        trn_sample_color_recipes_bulk_header.strCombo 			= '$combo' 		AND
        trn_sample_color_recipes_bulk_header.strPrintName 			= '$print' AND 
        trn_sample_color_recipes_bulk_header.recipeRevision 			= '$bulkRev'
        ";
//echo $sql;
$result 			= $db->RunQuery($sql);
$header_array		= mysqli_fetch_array($result);
$createdDate			= $header_array['createdDate'];
$recipeStatus			= $header_array['STATUS'];
$recipeApprovalLvl		= $header_array['approvalLvl'];
$createdTime            = $header_array['createdDate'];
$createdBy              = $header_array['strUserName'];

$flag_saved	=1;
//if($header_array['SAMPLE_NO']=='')
//    $flag_saved	=0;

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_reject($recipeStatus,$recipeApprovalLvl,$userId,$programCode,'RunQuery');
$permision_reject			= $permition_arr['permision'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_confirm($recipeStatus,$recipeApprovalLvl,$userId,$programCode,'RunQuery');
$permision_confirm			= $permition_arr['permision'];

?><head>
<!--    <title>Bulk Recipe Report</title>-->
    <link rel="stylesheet" type="text/css" href="css/button.css"/>
    <link rel="stylesheet" type="text/css" href="css/promt.css"/>
    <script type="application/javascript" src="presentation/customerAndOperation/bulk/colorRecipes/addNew/colorRecipes-js.js"></script>
    <style>
        .break { page-break-before: always; }

        @media print {
            .noPrint
            {
                display:none;
            }
        }
        #apDiv1 {
            position: absolute;
            left: 300px;
            top: 170px;
            width: 650px;
            height: 322px;
            z-index: 1;
        }
        .APPROVE {
            font-size: 18px;
            font-weight: bold;
        }
    </style>
</head>

<form id="frmSampleCostSheetRpt" name="frmSampleCostSheetRpt" method="post" action="">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
        </tr>
        <tr>
            <td width="20%"></td>
            <td width="60%" height="80" valign="top"><?php if($flag_saved==1){ include 'reportHeader.php';} ?></td>
            <td width="20%"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
    </table>

    <table width="88%" border="0" align="center" bgcolor="#FFFFFF" >
        <tr><tr>
            <?php
            if($recipeStatus==1 || $bulkRev == 0){?><td class="APPROVE" style="color:#00CC66" >CONFIRMED</td><?PHP }
            else if($recipeStatus==-1) {?> <td class="APPROVE" style="color:#FF8040">REJECTED</td><?php }
            else if($recipeStatus< 4 && $recipeStatus > 1)	{?> <td class="APPROVE" style="color:#FF8040">PENDING</td><?php }
            else 									{?> <td class="APPROVE" style="color:#00F">NOT SAVED</td><?php }
            ?>
        </tr> </tr>
        <tr><td align="left">
                    <?php if($mode==2 && $recipeStatus > 1  && ($recipeStatus - $recipeApprovalLvl < 1) && $permision_confirm){ ?><a id="butRptApprove" class="button white medium">Approve Recipe</a>&nbsp;&nbsp;<?php } ?>
                    <?php if($mode==2 && $recipeStatus > 1 && ($recipeStatus - $recipeApprovalLvl < 1) && $permision_reject){ ?><a id="butRptReject" class="button white medium">Reject Recipe</a>&nbsp;&nbsp;<?php } ?>
            </td>
        </tr>

        <tr>
            <td align="center" colspan="6"><strong>Bulk Recipe Report</strong></td>
        </tr>

        <tr><td colspan="6"><table width="100%" class="bordered">
                    <tr  nowrap="nowrap" class="headerLoad">
                        <th width="8%" class="normalfnt">Sample Year</th>
                        <td width="7%" id="txtSmapleYear"><?php echo $sampleYear; ?></td>
                        <th width="6%" class="normalfnt">Sample No</th>
                        <td width="11%" id="txtsampleNo"><?php echo $sampleNo; ?></td>
                        <th width="7%" class="normalfnt">Revision No</th>
                        <td width="6%" id="txtrevNo"><?php echo $revNo; ?></td>
                        <th width="6%" class="normalfnt">Combo</th>
                        <td width="12%" id="txtcombo"><?php echo $combo; ?></td>
                        <th width="4%">Print</th>
                        <td width="12%" id="txtprint"><?php echo $print; ?></td>
                        <th width="9%" class="normalfnt">Bulk Recipe No</th>
                        <td width="9%" hidden id="bulkRecipeNo"><?php echo $bulkRev; ?></td>
                        <td width="9%" id="bulkRecipeName"><?php echo $bulkRevName; ?></td>
                    </tr>
                    </table></td></tr>

         <tr>
                <td colspan="6">
                        <table width="100%" class="bordered" >
                            <tr>
                                <th height="17" colspan="10"  class="normalfntMid"><strong>Inks</strong></th>
                            </tr>
                            <tr>
                                <th width="2%" height="22">&nbsp;</th>
                                <th width="11%">Color</th>
                                <th width="19%">Technique</th>
                                <th width="16%">Ink Type</th>
                                <th width="13%">Shots</th>
                                <th width="29%">Item</th>
                                <th width="19%">Item Ratio</th>
                            </tr>
                            <?php

                            $result_recipe =	get_details_array($sampleNo,$sampleYear,$revNo,$combo,$print,$bulkRev);
                            $table='trn_sample_color_recipes_bulk';
                            if ($bulkRev == 0 ){
                                $table = "trn_sample_color_recipes";
                            }
                            while ($row = mysqli_fetch_array($result_recipe)) {
                                $showExcelReportStatus = 1;
                                $detailArrayIndex = 0;
                                $colorName =  $row['colorName'];
                                $technique =  $row['technique'];
                                $intkTypeName =  $row['inkTypeName'];
                                $intNumOfShots =  $row['intNumOfShots'];

                                $sql2 = "SELECT
                                                        tsb.intItem,
                                                        tsb.dblWeight,
                                                        mst_item.strName AS itemName							
                                                        FROM $table as tsb
                                                        INNER JOIN mst_item ON mst_item.intId = tsb.intItem
                                                        WHERE
                                                        tsb.intSampleNo =  '$sampleNo' AND
                                                        tsb.intSampleYear =  '$sampleYear' AND
                                                        tsb.intRevisionNo =  '$revNo' AND
                                                        tsb.strCombo =  '$combo' AND
                                                        tsb.strPrintName =  '$print' AND ";
                                if ($bulkRev != 0){
                                    $sql2.=                "tsb.bulkRevNumber = '$bulkRev' AND ";
                                }
                                $sql2.=                "tsb.intColorId =  '" . $row['intColorId'] . "' AND
                                                        tsb.intInkTypeId='" . $row['intInkType'] . "' AND
                                                        tsb.intTechniqueId='" . $row['intTechnique'] . "'
                                                        ";
                                $result2 = $db->RunQuery($sql2);
                                $recCount = mysqli_num_rows($result2);
                                $weightIndex = 0;
                                $entry = 0;
                                if ($recCount > 0) {
                                    while ($row2 = mysqli_fetch_array($result2)) {
                                        $entry++;
                                        $weightDetails = array();
                                        $details = array();
                                        $itemId = $row2['intItem'];
                                        $weight = $row2['dblWeight'];
                                        $itemName = $row2['itemName'];
                                        $details['itemId'] = $itemId;
                                        $details['itemName'] = $itemName;
                                        $details['weight'] = $weight;
                                        $detailList['records'][$intkTypeName][$technique][$colorName][$detailArrayIndex]= $details;
                                        ?>


                                        <tr class="normalfnt dataRow">
                                            <?php
                                            if ($entry == 1) {
                                                ?>
                                                <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
                                                <td bgcolor="#FFFFFF"
                                                    id="<?Php echo $row['intColorId']; ?>"><?php echo $row['colorName'] ?></td>
                                                <td bgcolor="#FFFFFF"
                                                    id="<?php echo $row['intTechnique']; ?>"><?php echo $row['technique'] ?></td>
                                                <td bgcolor="#FFFFFF"
                                                    id="<?php echo $row['intInkType']; ?>"><?php echo $row['inkTypeName'] ?></td>
                                                <td bgcolor="#FFFFFF"
                                                    class="normalfntMid"><?php echo $intNumOfShots ?></td>
                                                <?php
                                            } else {
                                                ?>
                                                <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
                                                <td bgcolor="#FFFFFF"></td>
                                                <td bgcolor="#FFFFFF"></td>
                                                <td bgcolor="#FFFFFF"></td>
                                                <td bgcolor="#FFFFFF"></td>
                                                <?php
                                            }
                                            ?>


                                            <td bgcolor="#FFFFFF" id="cboItem"
                                                name="cboItem"
                                                class="normalfntMid"><?php echo $itemName; ?></td>
                                            <td bgcolor="#FFFFFF" class="normalfntRight"
                                                name="txtWeight" id="txtWeight"
                                                value="<?php echo $weight; ?>"><?php echo $weight; ?></td>
                                        </tr>


                                        <?php
                                        $detailArrayIndex++;
                                    }
                                }
                                ?>
                                <?php
                            }
                            ?>
                        </table>
                    </td>
            </tr>


        <?php
        if ($bulkRev != 0){
            $creator			   = $createdBy;
            $createdDate		   =$createdTime;
            $header_array['STATUS']=$status;
            $header_array['LEVELS']=$levels;
            $sqlA = "SELECT
								sys_users.strUserName as UserName,
								tbl.dtmdateApproved AS dtApprovedDate,
								tbl.intApproveLevelNo AS intApproveLevelNo 
								FROM
								trn_sample_color_recipes_bulk_approve AS tbl
								INNER JOIN sys_users ON tbl.intUser = sys_users.intUserId
								WHERE
								tbl.intSampleNo ='$sampleNo' AND
								tbl.intSampleYear ='$sampleYear' AND
								tbl.intRevisionNo ='$revNo' AND
								tbl.strCombo ='$combo' AND
								tbl.strPrintName = '$print' AND	
								tbl.recipeRevision = '$bulkRev' 
								order by tbl.dtmdateApproved ASC
		  
				  		";

            $resultA = $db->RunQuery($sqlA);

            include "presentation/report_approvedBy_details.php";
        }

        ?>

    </table>

</form>

<div style="width:900px; position:absolute; display:none;z-index:100;"  id="popupContact1"></div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
<?php
function get_details_array($sampleNo,$sampleYear,$revNo,$combo,$print,$bulkRev){
    global $db;
    $sql = "";
    if ($bulkRev == 0){
        $sql = "SELECT
						trn_sampleinfomations_details_technical.intNoOfShots as intNumOfShots,
						trn_sampleinfomations_details_technical.intColorId,
						trn_sampleinfomations_details_technical.intInkTypeId as intInkType,
						mst_inktypes.strName AS inkTypeName,
						mst_colors.strName AS colorName,
						trn_sampleinfomations_details.intTechniqueId as intTechnique,
						mst_techniques.strName AS technique
					FROM
						trn_sampleinfomations_details_technical
						Inner Join mst_inktypes ON mst_inktypes.intId = trn_sampleinfomations_details_technical.intInkTypeId
						Inner Join mst_colors ON mst_colors.intId = trn_sampleinfomations_details_technical.intColorId
						Inner Join trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_details_technical.intSampleNo AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_details_technical.intRevNo AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_details_technical.strPrintName AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_details_technical.strComboName AND trn_sampleinfomations_details.intColorId = trn_sampleinfomations_details_technical.intColorId
						Inner Join mst_techniques ON mst_techniques.intId = trn_sampleinfomations_details.intTechniqueId						
					WHERE
						trn_sampleinfomations_details_technical.intSampleNo =  '$sampleNo' AND
						trn_sampleinfomations_details_technical.intSampleYear =  '$sampleYear' AND
						trn_sampleinfomations_details_technical.intRevNo =  '$revNo' AND
						trn_sampleinfomations_details_technical.strPrintName =  '$print' AND
						trn_sampleinfomations_details_technical.strComboName =  '$combo' 
						group by 
						trn_sampleinfomations_details_technical.intColorId,
						trn_sampleinfomations_details.intTechniqueId,
						trn_sampleinfomations_details_technical.intInkTypeId
				";
    } else {
        $sql = "SELECT
                trn_sample_color_recipes_bulk.intNumOfShots,
                trn_sample_color_recipes_bulk.intColorId,
                trn_sample_color_recipes_bulk.intInkTypeId as intInkType,
                mst_inktypes.strName AS inkTypeName,
                mst_colors.strName AS colorName,
                trn_sample_color_recipes_bulk.intTechniqueId as intTechnique,
                mst_techniques.strName AS technique
            FROM
                trn_sample_color_recipes_bulk
                Inner Join mst_inktypes ON mst_inktypes.intId = trn_sample_color_recipes_bulk.intInkTypeId
                Inner Join mst_colors ON mst_colors.intId = trn_sample_color_recipes_bulk.intColorId
                Inner Join mst_techniques ON mst_techniques.intId = trn_sample_color_recipes_bulk.intTechniqueId						
            WHERE
                trn_sample_color_recipes_bulk.intSampleNo =  '$sampleNo' AND
                trn_sample_color_recipes_bulk.intSampleYear =  '$sampleYear' AND
                trn_sample_color_recipes_bulk.intRevisionNo =  '$revNo' AND
                trn_sample_color_recipes_bulk.strPrintName =  '$print' AND
                trn_sample_color_recipes_bulk.strCombo =  '$combo' AND 
                trn_sample_color_recipes_bulk.bulkRevNumber =  '$bulkRev' 
                group by 
                trn_sample_color_recipes_bulk.intColorId,
                trn_sample_color_recipes_bulk.intTechniqueId,
                trn_sample_color_recipes_bulk.intInkTypeId
        ";
    }
    $result_recipe = $db->RunQuery($sql);
    return $result_recipe;
}
?>

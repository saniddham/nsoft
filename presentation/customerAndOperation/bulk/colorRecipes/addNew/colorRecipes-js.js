// JavaScript Document
var basepath	= 'presentation/customerAndOperation/bulk/colorRecipes/listing/';
var basePath	="presentation/customerAndOperation/bulk/colorRecipes/addNew/";

 
$(document).ready(function() {

    $('#frmColorRecipies #cboYear').die('change').live('change', function () {
        loadGraphicNo();
    });

    $('#frmColorRecipies #cboGraphicNo').die('change').live('change', function () {
        loadSampleNos();
    });

    // $('#frmColorRecipies #cboSalesOrderId').die('change').live('change', function () {
    //     window.location.href = "?q=1282&orderYear=" + $('#cboYear').val() + "&orderNo=" + $('#cboOrderNo').val() + "&salesOrderId=" + $('#cboSalesOrderId').val();
    // });

    $('#frmColorRecipies #cboSampleNo').die('change').live('change',function(){
        var intSampleNo = $('#cboSampleNo').val();
        var intSampleYear = $('#cboYear').val();
        var graphicNo = $('#cboGraphicNo').val()
        loadRevisionNo(intSampleNo,intSampleYear,graphicNo);
    });

    $('#frmColorRecipies #cboRevisionNo').die('click').live('change',function(){
        loadCombo();
    });

    $('#frmColorRecipies #cboCombo').die('change').live('change',function(){
        loadPrint();
    });

    $('#frmColorRecipies #cboPrint').die('change').live('change',function(){
        loadRecipeRevision();

    });

    $('#cboRecipeRevision').change(function(){
        var sampleNo= $('#cboSampleNo').val();
        var sampleYear= $('#cboYear').val();
        window.location.href = "?q=1282&graphicNo="+URLEncode($('#cboGraphicNo').val())
            +'&sampleNo='+ sampleNo
            +'&sampleYear='+ sampleYear
            +'&revNo='+$('#cboRevisionNo').val()
            +'&combo='+URLEncode($('#cboCombo').val())
            +'&printName='+URLEncode($('#cboPrint').val())
            +'&recipeRevision='+$('#cboRecipeRevision').val();
    });

    // $('#butReport').die('click').live('click', viewExcelReport);

    $('#butReport').click(function() {
        var intSampleNo = $('#cboSampleNo').val();
        var intSampleYear = $('#cboYear').val();

        if($('#txtPrnNo').val()!='')
            window.open('?q=1298&mode=1&sampleNo='+intSampleNo+'&sampleyear='+intSampleYear+'&revNo='+$('#cboRevisionNo').val() + '&combo='+ $('#cboCombo').val() + '&print='+$('#cboPrint').val() + '&bulkRev='+$('#cboRecipeRevision').val());
        else
            alert("There is no Sample No to view");
    });

    $('#butApprove').click(function() {
        var intSampleNo = $('#cboSampleNo').val();
        var intSampleYear = $('#cboYear').val();

        if($('#txtPrnNo').val()!='')
            window.open('?q=1298&mode=2&sampleNo='+intSampleNo+'&sampleyear='+intSampleYear+'&revNo='+$('#cboRevisionNo').val() + '&combo='+ $('#cboCombo').val() + '&print='+$('#cboPrint').val() + '&bulkRev='+$('#cboRecipeRevision').val());
        else
            alert("There is no Sample No to approve");
    });

    $('#butRptReject').click(function(){
        var sampleNo 		= 	$('#txtsampleNo').text();
        var sampleYear 		= 	$('#txtSmapleYear').text();
        var revisionNo 		= 	$('#txtrevNo').text();
        var combo 			=	$('#txtcombo').text();
        var printName 		=	$('#txtprint').text();
        var bulkRecipe 		=	$('#bulkRecipeNo').text();
        var rejectState = {
            state0: {
                html:'<label>Are you sure you want to reject this Bulk Recipe ?</label></br><label>Enter the Reason : <textarea name="txtReason" id="txtReason" cols="50" rows="3"></textarea></label><br />',
                buttons: { Ok: true, Cancel: false },
                persistent: true,
                submit:function(v,m,f){
                    if(v)
                    {
                        var reason 	= $('#txtReason').val();

                        var arrHeader = "{";
                        arrHeader += '"reason":'+URLEncode_json(reason)+'';
                        arrHeader += "}";

                        var url = basepath+"colorRecipes-db-set.php?requestType=rejectRecipe&sampleNo="+sampleNo+ "&sampleyear="+sampleYear+"&revNo="+revisionNo+"&combo="+URLEncode(combo)+"&print="+ URLEncode(printName)+"&bulkRev="+ bulkRecipe;                        //var obj 	= $.ajax({url:url,async:false});
                        var httpobj = $.ajax({
                            url:url,
                            dataType:'json',
                            type:'POST',
                            data:'',
                            async:false
                        });
                        window.location.href = window.location.href;
                        window.opener.location=window.opener.location;

                    }
                }
            }
        };
        $.prompt(rejectState);
    });

    $('#butRptApprove').click(function(){

        var sampleNo 		= 	$('#txtsampleNo').text();
        var sampleYear 		= 	$('#txtSmapleYear').text();
        var revisionNo 		= 	$('#txtrevNo').text();
        var combo 			=	$('#txtcombo').text();
        var printName 		=	$('#txtprint').text();
        var bulkRecipe 		=	$('#bulkRecipeNo').text();

        var val = $.prompt('Are you sure you want to approve this Color Recipe ?',{
            buttons: { Ok: true, Cancel: false },
            callback: function(v,m,f){
                if(v)
                {
                        var url = basepath+"colorRecipes-db-set.php?requestType=approveRecipe&sampleNo="+sampleNo+ "&sampleyear="+sampleYear+"&revNo="+revisionNo+"&combo="+URLEncode(combo)+"&print="+ URLEncode(printName)+"&bulkRev="+ bulkRecipe;
                        var obj = $.ajax({
                            url:url,

                            dataType: "json",
                            type:'POST',
                            data:'',//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
                            //data:'{"requestType":"addsampleInfomations"}',
                            async:false,

                            success:function(json){
                                $('#butRptApprove').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                                if(json.type=='pass')
                                {
                                    hideWaiting();
                                    var t=setTimeout("alertx()",1000);
                                    window.location.href = window.location.href;
                                    window.opener.location=window.opener.location;
                                }
                            },
                            error:function(xhr,status){
                                hideWaiting();
                                $('#butRptApprove').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                                var t=setTimeout("alertx()",3000);
                                //function (xhr, status){errormsg(status)}
                            }
                        });
                }
            }});
        //hideWaiting();
    });
});

function loadGraphicNo() {
    var url = basePath+"colorRecipes-db-get.php?requestType=loadGraphicNo&orderYear="+$('#cboYear').val();
    var obj = $.ajax({url:url,async:false});
    $('#cboGraphicNo').html(obj.responseText);
}

function loadSampleNos() {
    var url = basePath+"colorRecipes-db-get.php?requestType=loadSamples&sampleYear="+$('#cboYear').val()+"&graphic="+$('#cboGraphicNo').val();
    var obj = $.ajax({url:url,async:false});
    $('#cboSampleNo').html(obj.responseText);
}


function loadRevisionNo(sampleNo,sampleYear,graphicNo)
{
    var url = basePath+"colorRecipes-db-get.php?requestType=loadRevisionNo&sampleNo="+sampleNo+"&sampleYear="+sampleYear;
    var httpobj = $.ajax({
        url:url,
        dataType:'json',
        type:'GET',
        data:"",
        async:false,
        success:function(json){
            $('.price_shots').val(json.cost);
            $('#cboRevisionNo').html(json.revision)
            if (graphicNo == "" || graphicNo== null){
                $('#cboGraphicNo').html(json.graphic);
            }
            $('#cboCombo').html('');
            $('#cboPrint').html('');
        }
    });

}

function loadCombo()
{
    var intSampleNo = $('#cboSampleNo').val();
    var intSampleYear = $('#cboYear').val()
    var url = basePath+"colorRecipes-db-get.php?requestType=loadCombo&sampleNo="+URLEncode(intSampleNo)+"&sampleYear="+intSampleYear+"&revNo="+$('#cboRevisionNo').val();
    var obj = $.ajax({url:url,async:false});
    $('#cboCombo').html(obj.responseText);
    $('#cboPrint').html('');
}

function loadPrint()
{
    var intSampleNo = $('#cboSampleNo').val();
    var intSampleYear = $('#cboYear').val();
    var url = basePath+"colorRecipes-db-get.php?requestType=loadPrint&sampleNo="+URLEncode(intSampleNo)+"&sampleYear="+intSampleYear+"&revNo="+$('#cboRevisionNo').val()+"&combo="+URLEncode($('#cboCombo').val());
    var obj = $.ajax({url:url,async:false});
    $('#cboPrint').html(obj.responseText);
}

function  loadRecipeRevision() {
    var intSampleNo = $('#cboSampleNo').val();
    var intSampleYear = $('#cboYear').val();
    var revision = $('#cboRevisionNo').val();
    var combo = URLEncode($('#cboCombo').val());
    var print = URLEncode($('#cboPrint').val());
    var url = basePath+"colorRecipes-db-get.php?requestType=loadRecipeRevision&sampleNo="+URLEncode(intSampleNo)+"&sampleYear="+intSampleYear+"&revNo="+revision+"&combo="+combo+"&print="+$('#cboPrint').val();
    var obj = $.ajax({url:url,async:false});
    $('#cboRecipeRevision').html(obj.responseText);

}

function viewExcelReport(){

    var url =basePath + 'colorRecipesExcel.php';
    window.open(url);
}
	

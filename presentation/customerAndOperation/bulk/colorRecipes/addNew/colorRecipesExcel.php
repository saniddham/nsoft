<?php
session_start();
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=color_recipes_report.xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");

$dataArray = $_SESSION['COLOR_RECIPE_DATA'];
$subArray = $dataArray['records'];


?>
<div align="center">
    <div class="trans_layoutL">
        <div class="trans_text"><strong>COLOR RECIPES</strong></div>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr></tr>
            <tr>
                <td height="22" colspan="10" class="normalfnt">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="bordered">
                        <tr>
                            <th width="10%" class="normalfnt">Order Year</th>
                            <th width="10%" class="normalfnt">Order No</th>
                            <th width="10%" class="normalfnt">SalesOrder No</th>
                            <th width="18%" class="normalfnt">Graphic No</th>
                            <th width="10%" class="normalfnt">Sample No</th>
                            <th width="8%" class="normalfnt">Revision No</th>
                            <th width="11%" class="normalfnt">Combo</th>
                            <th width="9%" class="normalfnt">Print</th>
                            <th width="4%" class="normalfnt">Recipe Revision</th>
                            <th width="20%" class="normalfnt">Style No</th>
                            <th width="10%" class="normalfnt">Graphic Placement</th>
                            <th width="8%" class="normalfnt">Graphic Width(Inch)</th>
                            <th width="8%" class="normalfnt">Graphic Height(Inch)</th>
                        </tr>
                        <tr>
                            <td align="center"><?php echo $dataArray['orderYear']; ?></td>
                            <td align="center"><?php echo $dataArray['orderNo']; ?></td>
                            <td align="center"><?php echo $dataArray['salesOrderNo']; ?></td>
                            <td align="center"><?php echo $dataArray['graphicNo']; ?></td>
                            <td align="center"><?php echo $dataArray['sampleNo']; ?></td>
                            <td align="center"><?php echo $dataArray['revNo']; ?></td>
                            <td align="center"><?php echo $dataArray['combo']; ?></td>
                            <td align="center"><?php echo $dataArray['print']; ?></td>
                            <td align="center"><?php echo $dataArray['recipeRevision']; ?></td>
                            <td align="center"><?php echo $dataArray['styleNo']; ?></td>
                            <td align="center"><?php echo $dataArray['partName']; ?></td>
                            <td align="center"><?php echo $dataArray['print_width']; ?></td>
                            <td align="center"><?php echo $dataArray['print_height']; ?></td>
                        </tr>
                        <tr></tr>
                    </table>
            <tr></tr>
            <tr>
                <td colspan="10">
                    <table width="100%" class="bordered" border="1" align="center">
                        <tr>
                            <th width="11%">Color</th>
                            <th width="19%">Technique</th>
                            <th width="16%">Ink Type</th>
                            <th width="13%">Shots</th>
                            <th width="10%">Weight(g)</th>
                            <th width="29%">Item/Weight(g)</th>
                        </tr>
                        <?php

                        foreach ($subArray as $inkType => $inkEntries){
                            foreach ($inkEntries as $technique => $techEntries){
                                foreach ($techEntries as $color => $colorEntries){
                                    foreach ($colorEntries["records"] as $entry){
                                        ?>
                                        <tr>
                                            <td align="center"><?php echo $inkType; ?></td>
                                            <td align="center"><?php echo $technique; ?></td>
                                            <td align="center"><?php echo $color; ?></td>
                                            <td align="center"><?php echo $color; ?></td>
                                            <td align="center"><?php echo $entry['weight']; ?></td>
                                            <td align="center"><?php echo $entry['itemName']; ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                            }
                        }
//                        for ($i = 0; $i < sizeof($subArray); $i++) {
//                            ?>
<!--                            <tr>-->
<!--                                <td align="center">--><?php //echo $subArray[$i]['color']; ?><!--</td>-->
<!--                                <td align="center">--><?php //echo $subArray[$i]['technique']; ?><!--</td>-->
<!--                                <td align="center">--><?php //echo $subArray[$i]['inkType']; ?><!--</td>-->
<!--                                <td align="center">--><?php //echo $subArray[$i]['shots']; ?><!--</td>-->
<!--                                <td align="center">--><?php //echo $subArray[$i]['weight']; ?><!--</td>-->
<!--                                <td align="center">-->
<!--                                    <table width="287" bgcolor="#FFFFFF" class="normalfnt" cellpadding="0"-->
<!--                                           cellspacing="1">-->
<!--                                        --><?php
//                                        $weightDetails = $subArray[$i]['weightInfo'];
//                                        for ($j = 0; $j < sizeof($weightDetails); $j++) {
//                                            ?>
<!--                                            <tr>-->
<!--                                                <td>--><?php //echo $weightDetails[$j]['itemName']; ?><!--</td>-->
<!--                                                <td>--><?php //echo $weightDetails[$j]['weight']; ?><!--</td>-->
<!--                                            </tr>-->
<!--                                            --><?php
//                                        }
//                                        ?>
<!--                                    </table>-->
<!--                                </td>-->
<!--                            </tr>-->
<!--                            --><?php
//                        }
                        ?>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</div>








<?php (define('UNLOCKPAGE', true) ? die('<<< Access denied >>>') : ''); ?>
<?php
$locationId = $sessions->getLocationId();


###########################
### get loading values ####
###########################
$x_graphicNo = $_REQUEST['graphicNo'];
$x_sampleNo = $_REQUEST['sampleNo'];
$x_sampleYear = $_REQUEST['sampleYear'];
$x_revNo = $_REQUEST['revNo'];
$x_combo = $_REQUEST['combo'];
$x_print = $_REQUEST['printName'];
$x_recipeRev = $_REQUEST['recipeRevision'];
if ($x_sampleNo && $x_sampleYear && $x_revNo != '' && $x_combo !='' && $x_print != '' && $x_recipeRev != ''){
    $sql = "SELECT STATUS FROM `trn_sample_color_recipes_bulk_header` WHERE `intSampleNo` = '$x_sampleNo' 
            AND `intSampleYear` = '$x_sampleYear' AND `intRevisionNo` = '$x_revNo' AND `strCombo` = '$x_combo' 
            AND `strPrintName`='$x_print' AND `recipeRevision`='$x_recipeRev'";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $recipe_status = $row['STATUS'];
}


$showExcelReportStatus = 0;
if ($x_sampleYear == '') {
    $x_sampleYear = date('Y');
}



$detailList = array();
$detailList['sampleYear'] = $x_sampleYear;
$detailList['sampleNo'] = $x_sampleNo . '/' . $x_sampleYear;
$detailList['revNo'] = $x_revNo;
$detailList['combo'] = $x_combo;
$detailList['print'] = $x_print;
$detailList['graphicNo'] = $x_graphicNo;
$detailList['recipeRevision'] = $x_recipeRev;


//------------------------------------
if ($x_sampleNo) {
    $sql = "SELECT
					trn_sampleinfomations.intStatus,
					trn_sampleinfomations.intTechnicalStatus
					FROM `trn_sampleinfomations`
					WHERE
					trn_sampleinfomations.intSampleNo = '$x_sampleNo' AND
					trn_sampleinfomations.intSampleYear = '$x_sampleYear' AND
					trn_sampleinfomations.intRevisionNo = '$x_revNo'
					";
    $results = $db->RunQuery($sql);
    $row = mysqli_fetch_array($results);
    $sampStatus = $row['intStatus'];
    $techStatus = $row['intTechnicalStatus'];
}


?>
<head>
    <title>Color Recipes</title>
</head>

<body>
<form id="frmColorRecipies" name="frmColorRecipies" method="post">
    <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    </table>
    <div align="center">
        <div class="trans_layoutL">
            <div class="trans_text">COLOR RECIPES</div>
            <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
                <td>
                    <table width="100%" border="0">
                        <tr>
                            <td>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td height="22" colspan="7" class="normalfnt">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0"
                                                   class="bordered">
                                                <tr>
                                                    <th width="8%" height="19" class="normalfnt">Sample Year</th>
<!--                                                    <th width="8%" class="normalfnt">Order No</th>-->
<!--                                                    <th width="8%" class="normalfnt">Sales Order No</th>-->
                                                    <th width="18%" class="normalfnt">Graphic No</th>
                                                    <th width="13%" class="normalfnt">Sample No</th>
                                                    <th width="6%" class="normalfnt">Revision No</th>
                                                    <th width="11%" class="normalfnt">Combo</th>
                                                    <th width="9%" class="normalfnt">Print</th>
                                                    <th width="6%" class="normalfnt">Recipe Revision
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><select name="cboYear" id="cboYear" style="width:70px">
                                                            <?php
                                                            $d = date('Y');
                                                            for ($d; $d >= 2012; $d--) {
                                                                if ($d == $x_sampleYear)
                                                                    echo "<option selected=\"selected\" id=\"$d\" >$d</option>";
                                                                else
                                                                    echo "<option id=\"$d\" >$d</option>";
                                                            }
                                                            if ($x_sampleYear != '') {
                                                                $d = $x_sampleYear;
                                                            } else {
                                                                $d = date('Y');
                                                            }

                                                            ?>
                                                        </select></td>

                                                    <td><select name="cboGraphicNo" id="cboGraphicNo" style="width:160px">
                                                            <?php
                                                            //$d = date('Y');
                                                            $sql = "SELECT
                                                            distinct strGraphicRefNo 
                                                        FROM
                                                            trn_sampleinfomations
                                                        WHERE
                                                            trn_sampleinfomations.intSampleYear = '$x_sampleYear' AND intStatus = 1
                                                        ORDER BY
                                                            strGraphicRefNo ASC";
                                                            $result = $db->RunQuery($sql);
                                                            if (mysqli_num_rows($result) > 1) {
                                                                echo " <option value=\"\"></option>";
                                                            }
                                                            while ($row = mysqli_fetch_array($result)) {
                                                                $strGraphicNo = $row['strGraphicRefNo'];
                                                                if ($x_graphicNo == $strGraphicNo)
                                                                    echo "<option selected=\"selected\" value=\"$strGraphicNo\">$strGraphicNo</option>";
                                                                else
                                                                    echo "<option value=\"$strGraphicNo\">$strGraphicNo</option>";
                                                            }
                                                            ?>
                                                        </select></td>
                                                    <td><select name="cboSampleNo" id="cboSampleNo" style="width:120px">
                                                            <?php
                                                            //$d = date('Y');
                                                            $sql = "SELECT
                                                            distinct intSampleNo 
                                                        FROM
                                                            trn_sampleinfomations
                                                        WHERE
                                                            trn_sampleinfomations.intSampleYear = '$x_sampleYear' AND intStatus = 1
                                                        ORDER BY
                                                            intSampleNo ASC";
                                                            $result = $db->RunQuery($sql);
                                                            if (mysqli_num_rows($result) > 1) {
                                                                echo " <option value=\"\"></option>";
                                                            }
                                                            while ($row = mysqli_fetch_array($result)) {
                                                                $sampleNo = $row['intSampleNo'];
                                                                if ($x_sampleNo == $sampleNo)
                                                                    echo "<option selected=\"selected\" value=\"$sampleNo\">$sampleNo</option>";
                                                                else
                                                                    echo "<option value=\"$sampleNo\">$sampleNo</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                    </td>
                                                    <td><select name="cboRevisionNo" id="cboRevisionNo"
                                                                style="width:60px">
                                                            <option value=""></option>
                                                            <?php
                                                            $sql = "SELECT
                                                                        trn_sampleinfomations.intRevisionNo
                                                                    FROM trn_sampleinfomations
                                                                    WHERE
                                                                        trn_sampleinfomations.intSampleYear =  '$x_sampleYear' AND
                                                                        trn_sampleinfomations.intSampleNo =  '$x_sampleNo' 
                                                                        AND trn_sampleinfomations.intTechnicalStatus =1 
                                                                    ORDER BY
                                                                        trn_sampleinfomations.intRevisionNo ASC
                                                                    ";
                                                            $result = $db->RunQuery($sql);
                                                            while ($row = mysqli_fetch_array($result)) {
                                                                if ($x_revNo == $row['intRevisionNo'])
                                                                    echo "<option selected=\"selected\" value=\"" . $row['intRevisionNo'] . "\">" . $row['intRevisionNo'] . "</option>";
                                                                else
                                                                    echo "<option value=\"" . $row['intRevisionNo'] . "\">" . $row['intRevisionNo'] . "</option>";
                                                            }
                                                            ?>
                                                        </select></td>
                                                    <td><select name="cboCombo" id="cboCombo" style="width:100px">
                                                            <option value=""></option>
                                                            <?php
                                                            $sql = "SELECT DISTINCT
                                                                        trn_sampleinfomations_details.strComboName
                                                                        FROM trn_sampleinfomations_details
                                                                    WHERE
                                                                        trn_sampleinfomations_details.intSampleNo =  '$x_sampleNo' AND
                                                                        trn_sampleinfomations_details.intSampleYear =  '$x_sampleYear' 
                                                                    ORDER BY
                                                                        trn_sampleinfomations_details.strComboName ASC
                                                                    ";
                                                            $result = $db->RunQuery($sql);
                                                            while ($row = mysqli_fetch_array($result)) {
                                                                if ($x_combo == $row['strComboName'])
                                                                    echo "<option selected=\"selected\" value=\"" . $row['strComboName'] . "\">" . $row['strComboName'] . "</option>";
                                                                else
                                                                    echo "<option value=\"" . $row['strComboName'] . "\">" . $row['strComboName'] . "</option>";
                                                            }
                                                            ?>
                                                        </select></td>
                                                    <td><select name="cboPrint" id="cboPrint" style="width:100px">
                                                            <option value=""></option>
                                                            <?php
                                                            $sql = "SELECT DISTINCT
                                                                        trn_sampleinfomations_details.strPrintName
                                                                        FROM trn_sampleinfomations_details
                                                                    WHERE
                                                                        trn_sampleinfomations_details.intSampleNo 	=  '$x_sampleNo' AND
                                                                        trn_sampleinfomations_details.intSampleYear =  '$x_sampleYear' AND
                                                                        trn_sampleinfomations_details.strComboName 		=  '$x_combo'
                                                                    ORDER BY
                                                                        trn_sampleinfomations_details.strPrintName ASC
                                                                    ";
                                                            $result = $db->RunQuery($sql);
                                                            while ($row = mysqli_fetch_array($result)) {
                                                                if ($x_print == $row['strPrintName'])
                                                                    echo "<option selected=\"selected\" value=\"" . $row['strPrintName'] . "\">" . $row['strPrintName'] . "</option>";
                                                                else
                                                                    echo "<option value=\"" . $row['strPrintName'] . "\">" . $row['strPrintName'] . "</option>";
                                                            }
                                                            ?>
                                                        </select></td>
                                                    <td><select name="cboRecipeRevision" id="cboRecipeRevision"
                                                                style="width:80px">
                                                            <?php
                                                            //$d = date('Y');
                                                            $sql = "SELECT  
                                                            intColorId FROM  trn_sample_color_recipes 
                                                        WHERE
                                                            trn_sample_color_recipes.intSampleNo =  '$x_sampleNo' AND
                                                            trn_sample_color_recipes.intSampleYear =  '$x_sampleYear' AND
                                                            trn_sample_color_recipes.intRevisionNo =  '$x_revNo' AND
                                                            trn_sample_color_recipes.strCombo =  '$x_combo' AND
                                                            trn_sample_color_recipes.strPrintName =  '$x_print'  ";
                                                            $result = $db->RunQuery($sql);
                                                            echo " <option value=\"\"></option>";

                                                            if(mysqli_num_rows($result) == 0){
                                                                echo "<option value=\"\">No recipes found</option>";
                                                            }
                                                            else {

                                                                if ($x_recipeRev == 0)
                                                                    echo "<option selected=\"selected\" value=\"" . "0" . "\">" . "A" . "</option>";
                                                                else
                                                                    echo "<option value=\"" . "0" . "\">" . "A" . "</option>";
                                                            }

                                                            $sql = "SELECT  
                                                            recipeRevision, alphabatic FROM trn_sample_color_recipes_bulk_header left join mst_number_mapping ON 
                                                            mst_number_mapping.intNumber=trn_sample_color_recipes_bulk_header.recipeRevision
                                                        WHERE
                                                            trn_sample_color_recipes_bulk_header.intSampleNo =  '$x_sampleNo' AND
                                                            trn_sample_color_recipes_bulk_header.intSampleYear =  '$x_sampleYear' AND
                                                            trn_sample_color_recipes_bulk_header.intRevisionNo =  '$x_revNo' AND
                                                            trn_sample_color_recipes_bulk_header.strCombo =  '$x_combo' AND
                                                            trn_sample_color_recipes_bulk_header.strPrintName =  '$x_print' 
                                                            ORDER BY
                                                            trn_sample_color_recipes_bulk_header.recipeRevision ASC";
                                                            $result = $db->RunQuery($sql);
                                                            while ($row = mysqli_fetch_array($result)) {
                                                                $recipeRev = $row['recipeRevision'];
                                                                $recipeRevName = $row['alphabatic'];
                                                                if ($x_recipeRev == $recipeRev)
                                                                    echo "<option selected=\"selected\" value=\"$recipeRev\">$recipeRevName</option>";
                                                                else
                                                                    echo "<option value=\"$recipeRev\">$recipeRevName</option>";
                                                            }
                                                            ?>
                                                        </select></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="22" class="normalfnt">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td colspan="2" class="normalfntMid"><strong>Graphic</strong></td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td align="left">&nbsp;</td>
                                    </tr>
                                    <?php
                                    #########################
                                    ## get header details ###
                                    #########################
                                    $sql = "SELECT DISTINCT
                                            trn_sampleinfomations_printsize.intWidth,
                                            trn_sampleinfomations_printsize.intHeight,
                                            trn_sampleinfomations.strStyleNo,
                                            mst_part.strName
                                        FROM
                                            trn_sampleinfomations_printsize
                                            Inner Join mst_part ON mst_part.intId = trn_sampleinfomations_printsize.intPart
                                            Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_printsize.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_printsize.intSampleYear
                                        WHERE
                                            trn_sampleinfomations_printsize.strPrintName  =  '$x_print' AND
                                            trn_sampleinfomations_printsize.intSampleNo   =  '$x_sampleNo' AND
                                            trn_sampleinfomations_printsize.intSampleYear =  '$x_sampleYear' AND
                                            trn_sampleinfomations_printsize.intRevisionNo =  '$x_revNo'
                                    ";
                                    $result = $db->RunQuery($sql);
                                    while ($row = mysqli_fetch_array($result)) {
                                        $styleNo = $row['strStyleNo'];
                                        $print_width = $row['intWidth'];
                                        $print_height = $row['intHeight'];
                                        $partName = $row['strName'];
                                    }
                                    $detailList['styleNo'] = $styleNo;
                                    $detailList['print_width'] = $print_width;
                                    $detailList['print_height'] = $print_height;
                                    $detailList['partName'] = $partName;
                                    ?>
                                    <tr>
                                        <td width="13%" height="22" class="normalfnt">Style No</td>
                                        <td width="21%" class="normalfnt">: <?php echo $styleNo; ?></td>
                                        <td colspan="2" rowspan="5" class="normalfntMid">
                                            <div id="divPicture" class="tableBorder_allRound divPicture" align="center"
                                                 contenteditable="true"
                                                 style="width:264px;height:148px;overflow:hidden">
                                                <?php
                                                if ($x_sampleNo != '') {
                                                    echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"documents/sampleinfo/samplePictures/$x_sampleNo-$x_sampleYear-$x_revNo-" . substr($x_print, 6, 1) . ".png\" />";
                                                }
                                                ?>
                                            </div>
                                        </td>
                                        <td width="6%" class="normalfnt">&nbsp;</td>
                                        <td width="11%" class="normalfnt">&nbsp;</td>
                                        <td width="18%" align="left">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="19" class="normalfnt">Graphic Placement</td>
                                        <td class="normalfnt">: <?php echo $partName; ?></td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td align="left">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="22" class="normalfnt">Graphic Size <span class="normalfntGrey">(Inch)</span>
                                        </td>
                                        <td>
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr class="normalfnt">
                                                    <td width="15%" align="center"><input disabled="disabled"
                                                                                          value="<?php echo $print_width; ?>"
                                                                                          type="text"
                                                                                          name="txtPrintWidth"
                                                                                          id="txtPrintWidth"
                                                                                          style="width:30px"/></td>
                                                    <td width="18%">W</td>
                                                    <td width="21%" align="center"><input disabled="disabled"
                                                                                          value="<?php echo $print_height; ?>"
                                                                                          type="text"
                                                                                          name="txtPrintHeight"
                                                                                          id="txtPrintHeight"
                                                                                          style="width:30px"/></td>
                                                    <td width="46%">H</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td align="left">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="22" class="normalfnt">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td align="left">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="22" class="normalfnt">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td align="right"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="width:900px;height:500px;overflow:scroll">
                                    <table width="100%" class="bordered">
                                        <tr>
                                            <th width="2%" height="22">&nbsp;</th>
                                            <th width="11%">Color</th>
                                            <th width="19%">Technique</th>
                                            <th width="16%">Ink Type</th>
                                            <th width="13%">Shots</th>
                                            <th width="29%">Item</th>
                                            <th width="19%">Weight Ratio</th>
                                        </tr>
                                        <?php
                                        $detailList['records'] = array();
                                        if ($x_recipeRev != '' && $x_recipeRev == 0) {
                                            $sql = "SELECT
						trn_sampleinfomations_details_technical.intNoOfShots,
						trn_sampleinfomations_details_technical.intColorId,
						trn_sampleinfomations_details_technical.intInkTypeId as intInkType,
						mst_inktypes.strName AS inkTypeName,
						mst_colors.strName AS colorName,
						trn_sampleinfomations_details.intTechniqueId as intTechnique,
						mst_techniques.strName AS technique
					FROM
						trn_sampleinfomations_details_technical
						Inner Join mst_inktypes ON mst_inktypes.intId = trn_sampleinfomations_details_technical.intInkTypeId
						Inner Join mst_colors ON mst_colors.intId = trn_sampleinfomations_details_technical.intColorId
						Inner Join trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations_details_technical.intSampleNo AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations_details_technical.intRevNo AND trn_sampleinfomations_details.strPrintName = trn_sampleinfomations_details_technical.strPrintName AND trn_sampleinfomations_details.strComboName = trn_sampleinfomations_details_technical.strComboName AND trn_sampleinfomations_details.intColorId = trn_sampleinfomations_details_technical.intColorId
						Inner Join mst_techniques ON mst_techniques.intId = trn_sampleinfomations_details.intTechniqueId						
					WHERE
						trn_sampleinfomations_details_technical.intSampleNo =  '$x_sampleNo' AND
						trn_sampleinfomations_details_technical.intSampleYear =  '$x_sampleYear' AND
						trn_sampleinfomations_details_technical.intRevNo =  '$x_revNo' AND
						trn_sampleinfomations_details_technical.strPrintName =  '$x_print' AND
						trn_sampleinfomations_details_technical.strComboName =  '$x_combo' 
						group by 
						trn_sampleinfomations_details_technical.intColorId,
						trn_sampleinfomations_details.intTechniqueId,
						trn_sampleinfomations_details_technical.intInkTypeId
				";
                                            $result = $db->RunQuery($sql);
                                            $detailArrayIndex = 0;
                                            while ($row = mysqli_fetch_array($result)) {
                                                $showExcelReportStatus = 1;
                                                $details = array();
                                                $details['color'] = $row['colorName'];
                                                $details['technique'] = $row['technique'];
                                                $details['inkType'] = $row['inkTypeName'];
                                                $details['shots'] = $row['intNoOfShots'];
                                                $details['weight'] = $row['dblColorWeight'];

                                                $sql2 = "SELECT
							trn_sample_color_recipes.intItem,
							trn_sample_color_recipes.dblWeight,
							mst_item.strName AS itemName							
					FROM trn_sample_color_recipes
					INNER JOIN mst_item ON mst_item.intId = trn_sample_color_recipes.intItem
					WHERE
					trn_sample_color_recipes.intSampleNo =  '$x_sampleNo' AND
					trn_sample_color_recipes.intSampleYear =  '$x_sampleYear' AND
					trn_sample_color_recipes.intRevisionNo =  '$x_revNo' AND
					trn_sample_color_recipes.strCombo =  '$x_combo' AND
					trn_sample_color_recipes.strPrintName =  '$x_print' AND
					trn_sample_color_recipes.intColorId =  '" . $row['intColorId'] . "' AND
					trn_sample_color_recipes.intInkTypeId='" . $row['intInkType'] . "' AND
					trn_sample_color_recipes.intTechniqueId='" . $row['intTechnique'] . "'
					";
                                                //echo $sql2;
                                                $result2 = $db->RunQuery($sql2);
                                                $recCount = mysqli_num_rows($result2);
                                                $weightIndex = 0;
                                                $entry = 0;
                                                if ($recCount > 0) {
                                                    while ($row2 = mysqli_fetch_array($result2)) {
                                                        $entry++;
                                                        $weightDetails = array();
                                                        $itemId = $row2['intItem'];
                                                        $weight = $row2['dblWeight'];
                                                        $itemName = $row2['itemName'];
                                                        $weightDetails['itemId'] = $itemId;
                                                        $weightDetails['itemName'] = $itemName;
                                                        $weightDetails['weight'] = $weight;
                                                        ?>


                                                        <tr class="normalfnt dataRow">
                                                            <?php
                                                            if ($entry == 1) {
                                                                ?>
                                                                <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
                                                                <td bgcolor="#FFFFFF"
                                                                    id="<?Php echo $row['intColorId']; ?>"><?php echo $row['colorName'] ?></td>
                                                                <td bgcolor="#FFFFFF"
                                                                    id="<?php echo $row['intTechnique']; ?>"><?php echo $row['technique'] ?></td>
                                                                <td bgcolor="#FFFFFF"
                                                                    id="<?php echo $row['intInkType']; ?>"><?php echo $row['inkTypeName'] ?></td>
                                                                <td bgcolor="#FFFFFF"
                                                                    class="normalfntMid"><?php echo $row['intNoOfShots'] ?></td>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
                                                                <td bgcolor="#FFFFFF"
                                                                    id="<?Php echo $row['intColorId']; ?>"></td>
                                                                <td bgcolor="#FFFFFF"
                                                                    id="<?php echo $row['intTechnique']; ?>"></td>
                                                                <td bgcolor="#FFFFFF"
                                                                    id="<?php echo $row['intInkType']; ?>"></td>
                                                                <td bgcolor="#FFFFFF"
                                                                    class="normalfntMid"><?php echo $row['intNoOfShots'] ?></td>
                                                                <?php
                                                            }
                                                            ?>


                                                            <td bgcolor="#FFFFFF" id="cboItem"
                                                                name="cboItem"
                                                                class="normalfntMid"><?php echo $itemName; ?></td>
                                                            <td bgcolor="#FFFFFF" class="normalfntRight"
                                                                name="txtWeight" id="txtWeight"
                                                                value="<?php echo $weight; ?>"><?php echo $weight; ?></td>
                                                        </tr>


                                                        <?php
                                                        $details['weightInfo'][$weightIndex] = $weightDetails;
                                                        $weightIndex++;
                                                    }
                                                }
                                                ?>
                                                <?php
                                                $detailList['records'][$detailArrayIndex] = $details;
                                                $detailArrayIndex++;
                                            }
                                        } else if ($x_recipeRev != '' && $x_recipeRev > 0) {
                                            $sql = "SELECT
                                                    trn_sample_color_recipes_bulk.intNumOfShots,
                                                    trn_sample_color_recipes_bulk.intColorId,
                                                    trn_sample_color_recipes_bulk.intInkTypeId as intInkType,
                                                    mst_inktypes.strName AS inkTypeName,
                                                    mst_colors.strName AS colorName,
                                                    trn_sample_color_recipes_bulk.intTechniqueId as intTechnique,
                                                    mst_techniques.strName AS technique
                                                FROM
                                                    trn_sample_color_recipes_bulk
                                                    Inner Join mst_inktypes ON mst_inktypes.intId = trn_sample_color_recipes_bulk.intInkTypeId
                                                    Inner Join mst_colors ON mst_colors.intId = trn_sample_color_recipes_bulk.intColorId
                                                    Inner Join mst_techniques ON mst_techniques.intId = trn_sample_color_recipes_bulk.intTechniqueId						
                                                WHERE
                                                    trn_sample_color_recipes_bulk.intSampleNo =  '$x_sampleNo' AND
                                                    trn_sample_color_recipes_bulk.intSampleYear =  '$x_sampleYear' AND
                                                    trn_sample_color_recipes_bulk.intRevisionNo =  '$x_revNo' AND
                                                    trn_sample_color_recipes_bulk.strPrintName =  '$x_print' AND
                                                    trn_sample_color_recipes_bulk.strCombo =  '$x_combo' AND 
                                                    trn_sample_color_recipes_bulk.bulkRevNumber =  '$x_recipeRev' 
                                                    group by 
                                                    trn_sample_color_recipes_bulk.intColorId,
                                                    trn_sample_color_recipes_bulk.intTechniqueId,
                                                    trn_sample_color_recipes_bulk.intInkTypeId
                                            ";
                                            $result = $db->RunQuery($sql);
                                            while ($row = mysqli_fetch_array($result)) {
                                                $showExcelReportStatus = 1;
                                                $detailArrayIndex = 0;
                                                $details = array();
                                                $colorName =  $row['colorName'];
                                                $technique =  $row['technique'];
                                                $intkTypeName =  $row['inkTypeName'];
                                                $intNumOfShots =  $row['intNumOfShots'];

                                                $sql2 = "SELECT
                                                        trn_sample_color_recipes_bulk.intItem,
                                                        trn_sample_color_recipes_bulk.dblWeight,
                                                        mst_item.strName AS itemName							
                                                        FROM trn_sample_color_recipes_bulk
                                                        INNER JOIN mst_item ON mst_item.intId = trn_sample_color_recipes_bulk.intItem
                                                        WHERE
                                                        trn_sample_color_recipes_bulk.intSampleNo =  '$x_sampleNo' AND
                                                        trn_sample_color_recipes_bulk.intSampleYear =  '$x_sampleYear' AND
                                                        trn_sample_color_recipes_bulk.intRevisionNo =  '$x_revNo' AND
                                                        trn_sample_color_recipes_bulk.strCombo =  '$x_combo' AND
                                                        trn_sample_color_recipes_bulk.strPrintName =  '$x_print' AND
                                                        trn_sample_color_recipes_bulk.bulkRevNumber = '$x_recipeRev' AND
                                                        trn_sample_color_recipes_bulk.intColorId =  '" . $row['intColorId'] . "' AND
                                                        trn_sample_color_recipes_bulk.intInkTypeId='" . $row['intInkType'] . "' AND
                                                        trn_sample_color_recipes_bulk.intTechniqueId='" . $row['intTechnique'] . "'
                                                        ";
                                                $result2 = $db->RunQuery($sql2);
                                                $recCount = mysqli_num_rows($result2);
                                                $weightIndex = 0;
                                                $entry = 0;
                                                if ($recCount > 0) {
                                                    while ($row2 = mysqli_fetch_array($result2)) {
                                                        $entry++;
                                                        $weightDetails = array();
                                                        $details = array();
                                                        $itemId = $row2['intItem'];
                                                        $weight = $row2['dblWeight'];
                                                        $itemName = $row2['itemName'];
                                                        $details['itemId'] = $itemId;
                                                        $details['itemName'] = $itemName;
                                                        $details['weight'] = $weight;
                                                        $detailList['records'][$intkTypeName][$technique][$colorName][$detailArrayIndex]= $details;
                                                        ?>


                                                        <tr class="normalfnt dataRow">
                                                            <?php
                                                            if ($entry == 1) {
                                                                ?>
                                                                <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
                                                                <td bgcolor="#FFFFFF"
                                                                    id="<?Php echo $row['intColorId']; ?>"><?php echo $row['colorName'] ?></td>
                                                                <td bgcolor="#FFFFFF"
                                                                    id="<?php echo $row['intTechnique']; ?>"><?php echo $row['technique'] ?></td>
                                                                <td bgcolor="#FFFFFF"
                                                                    id="<?php echo $row['intInkType']; ?>"><?php echo $row['inkTypeName'] ?></td>
                                                                <td bgcolor="#FFFFFF"
                                                                    class="normalfntMid"><?php echo $intNumOfShots ?></td>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
                                                                <td bgcolor="#FFFFFF"></td>
                                                                <td bgcolor="#FFFFFF"></td>
                                                                <td bgcolor="#FFFFFF"></td>
                                                                <td bgcolor="#FFFFFF"></td>
                                                                <?php
                                                            }
                                                            ?>


                                                            <td bgcolor="#FFFFFF" id="cboItem"
                                                                name="cboItem"
                                                                class="normalfntMid"><?php echo $itemName; ?></td>
                                                            <td bgcolor="#FFFFFF" class="normalfntRight"
                                                                name="txtWeight" id="txtWeight"
                                                                value="<?php echo $weight; ?>"><?php echo $weight; ?></td>
                                                        </tr>


                                                        <?php
                                                        $detailArrayIndex++;
                                                    }
                                                }
                                                ?>
                                                <?php
                                            }
                                        }
                                        $_SESSION['COLOR_RECIPE_DATA'] = $detailList;
                                        ?>
                                    </table>
                                </div>
                            </td>
                        <tr>
                            <td align="center" class="" nowrap="nowrap">
                                <a href="?q=1282" id="butNew" class="button green large" style="" name="butNew" on> New </a>&nbsp;&nbsp;&nbsp;

                                <?php
                                if($recipe_status > 1 && $recipe_status < 4){?>
                                    <a id="butApprove" class="button green large" style="" name="butApprove" on> Approve </a>&nbsp;&nbsp;&nbsp;
                                <?php } ?>
                                <a id="butReport" class="button green large" style="" name="butReport" on> Report </a>&nbsp;&nbsp;&nbsp
                            </td>
                        </tr>
                    </table>
                </td>
                </tr>
            </table>

        </div>
    </div>

</form>
</body>
var basepath = 'presentation/customerAndOperation/NcingaDeliveryReports/';

$(document).ready(function () {
   

   
    $('#tblOrderDetailsHead').die('click').live('click', function () {
        headShowHide('tblOrderDetailsHead', 'tblOrderDetailsBody');

    });

    $('#tblOrderDetailsBody thead').die('click').live('click', function () {

        bodyShowHide('tblOrderDetailsHead', 'tblOrderDetailsBody');

    });
    //-----------------------------------
    $('#tblFabricDetailsHead').die('click').live('click', function () {
        headShowHide('tblFabricDetailsHead', 'tblFbricDetailsBody');

    });

    $('#tblFbricDetailsBody thead').die('click').live('click', function () {

        bodyShowHide('tblFabricDetailsHead', 'tblFbricDetailsBody');

    });
    //------------------------------------------------------------

    ///////////////////////Order Wise Fabric In - Out///////////////////////////////
    //$("#frmOrderWiseFabInOut").validationEngine();
   
    
/////////////////////////////////////
    //-------------------------------------------- 
////////////////STYLE STOCK MOVEMENT/////////////////
    //-------------------------------------------- 
    
////////////////END OF STYLE STOCK MOVEMENT/////////////////
    //-------------------------------------------- 
///////////////ORDER NO WISE FABRIC IN OUT/////////////////
    //-------------------------------------------- 
    $('#frmOrderWiseFabInOut #butReports').die('click').live('click', function () {
        if ($('#frmOrderWiseFabInOut').validationEngine('validate'))
        {
            var url = '?q=948';
            url += '&orderNo=' + $('#frmOrderWiseFabInOut #cboOrderNo').val();
            url += '&orderYear=' + $('#frmOrderWiseFabInOut #cboOrderYear').val();
            url += '&styleNo=' + $('#frmOrderWiseFabInOut #cboStyle').val();

            if ($('#frmOrderWiseFabInOut #chkColor').attr('checked'))
                url += '&option=1';
            else if ($('#frmOrderWiseFabInOut #chkCutNo').attr('checked'))
                url += '&option=2';
            else if ($('#frmOrderWiseFabInOut #chkSize').attr('checked'))
                url += '&option=3';
            else if ($('#frmOrderWiseFabInOut #chkPart').attr('checked'))
                url += '&option=4';
            else if ($('#frmOrderWiseFabInOut #chkSalesOrder').attr('checked'))
                url += '&option=5';
            else if ($('#frmOrderWiseFabInOut #chkSalesOrderSize').attr('checked')) {
                url = '';
                url += '?q=944';
                url += '&orderNo=' + $('#frmOrderWiseFabInOut #cboOrderNo').val();
                url += '&orderYear=' + $('#frmOrderWiseFabInOut #cboOrderYear').val();
                url += '&styleNo=' + $('#frmOrderWiseFabInOut #cboStyle').val();
                url += '&option=6';
            }
            window.open(url, 'rptOrderNoWiseFabricInOut.php');
        }
    });
////////////////END OF ORDER NO WISE FABRIC IN OUT//////////////////

///////////////CUSTOMER WISE FABRIC IN OUT///////////////////////// 

    $("#frmCustomerWiseFabInOut #butReports").die('click').live('click', function () {
        if ($('#frmCustomerWiseFabInOut').validationEngine('validate'))
        {
            var url = '?q=942';
            url += '&customer=' + $('#frmCustomerWiseFabInOut #cboCustomer').val();
            url += '&fromDate=' + $('#frmCustomerWiseFabInOut #txtFromDate').val();
            url += '&toDate=' + $('#frmCustomerWiseFabInOut #txtToDate').val();
            window.open(url);
        }
    });

/////////////////////////FABRIC CHECK STATUS///////////////////////// 

    $("#frmFabricCheckStatus #butReports").die('click').live('click', function () {
        if ($('#frmFabricCheckStatus').validationEngine('validate'))
        {
            var url = '?q=935';

            window.open(url);
        }
    });

/////////////////////////KPI REPORTS///////////////////////// 

    $('#frmKPI #cboCustomer').die('change').live('change', function () {
        loadSampleNos();
        loadGraphicNos();
    });

    $('#frmKPI #cboSampYear').die('change').live('change', function () {
        loadSampleNos();
        loadGraphicNos();
    });

    $('#frmKPI #cboSampNo').die('change').live('change', function () {
        loadGraphicNos();
    });

    $('#frmKPI #cboGraphicNo').die('change').live('change', function () {
        //loadSampleNos();
        //loadSampleYear();
    });

    $("#frmKPI #butNew").die('click').live('click', function () {
        $('#frmKPI #cboCustomer').val('');
        $('#frmKPI #cboSampNo').val('');
        $('#frmKPI #cboSampYear').val('');
        $('#frmKPI #cboGraphicNo').val('');
        $('#frmKPI #txtFromDate10').val('');
        $('#frmKPI #txtToDate10').val('');
        loadSampleNos();
        loadGraphicNos();
    });


    $("#frmKPI #butReports").die('click').live('click', function () {
        if ($('#frmKPI').validationEngine('validate'))
        {
            var url = '?q=886';
            url += '&customer=' + $('#frmKPI #cboCustomer').val();
            url += '&sampNo=' + $('#frmKPI #cboSampNo').val();
            url += '&sampYear=' + $('#frmKPI #cboSampYear').val();
            url += '&graphicNo=' + $('#frmKPI #cboGraphicNo').val();
            url += '&fromDate=' + $('#frmKPI #txtFromDate10').val();
            url += '&toDate=' + $('#frmKPI #txtToDate10').val();
            window.open(url);
        }
    });







    $('#frmCustomerWiseFabInOut #chkDate').die('click').live('click', function () {
        var val = $(this).attr('checked');
        if (!val)
        {
            $('#frmCustomerWiseFabInOut #txtFromDate').val('');
            $('#frmCustomerWiseFabInOut #txtFromDate').attr('disabled', 'true');
            $('#frmCustomerWiseFabInOut #txtToDate').val('');
            $('#frmCustomerWiseFabInOut #txtToDate').attr('disabled', 'true');
        } else
        {
            $('#frmCustomerWiseFabInOut #txtFromDate').removeAttr('disabled');
            $('#frmCustomerWiseFabInOut #txtToDate').removeAttr('disabled');
        }
    });
//////////////END OF CUSTOMER WISE FABRIC IN OUT////////////////////////////

    $('#frmKPI #chkDate').die('click').live('click', function () {
        var val = $(this).attr('checked');
        if (!val)
        {
            $('#frmKPI #txtFromDate10').val('');
            $('#frmKPI #txtFromDate10').attr('disabled', 'true');
            $('#frmKPI #txtToDate10').val('');
            $('#frmKPI #txtToDate10').attr('disabled', 'true');
        } else
        {
            $('#frmKPI #txtFromDate10').removeAttr('disabled');
            $('#frmKPI #txtToDate10').removeAttr('disabled');
        }
    });

//--------------------------------------------	  
    $('#frmCustomerDispatch #chkDate').die('click').live('click', function () {
        var val = $(this).attr('checked');
        if (!val)
        {
            $('#frmCustomerDispatch #txtFromDate11').val('');
            $('#frmCustomerDispatch #txtFromDate11').attr('disabled', 'true');
            $('#frmCustomerDispatch #txtToDate11').val('');
            $('#frmCustomerDispatch #txtToDate11').attr('disabled', 'true');
            $('#frmCustomerDispatch #txtFH').val('');
            $('#frmCustomerDispatch #txtFH').attr('disabled', 'true');
            $('#frmCustomerDispatch #txtFM').val('');
            $('#frmCustomerDispatch #txtFM').attr('disabled', 'true');
            //$('#frmCustomerDispatch #txtFS').val('');  
            //$('#frmCustomerDispatch #txtFS').attr('disabled','true');
            $('#frmCustomerDispatch #txtTH').val('');
            $('#frmCustomerDispatch #txtTH').attr('disabled', 'true');
            $('#frmCustomerDispatch #txtTM').val('');
            $('#frmCustomerDispatch #txtTM').attr('disabled', 'true');
            //$('#frmCustomerDispatch #txtTS').val('');  
            //$('#frmCustomerDispatch #txtTS').attr('disabled','true');
        } else
        {
            var printDate = formatDate();

            $('#frmCustomerDispatch #txtFromDate11').removeAttr('disabled');
            $('#frmCustomerDispatch #txtFromDate11').val(printDate);
            $('#frmCustomerDispatch #txtToDate11').removeAttr('disabled');
            $('#frmCustomerDispatch #txtToDate11').val(printDate);
            $('#frmCustomerDispatch #txtFH').removeAttr('disabled');
            $('#frmCustomerDispatch #txtFM').removeAttr('disabled');
            //$('#frmCustomerDispatch #txtFS').removeAttr('disabled');
            $('#frmCustomerDispatch #txtTH').removeAttr('disabled');
            $('#frmCustomerDispatch #txtTM').removeAttr('disabled');
            //$('#frmCustomerDispatch #txtTS').removeAttr('disabled');
        }
    });

    $("#frmCustomerDispatch #butReports").die('click').live('click', function () {
        if ($('#frmCustomerDispatch').validationEngine('validate'))
        {
            var url = '?q=937';
            url += '&location=' + $('#frmCustomerDispatch #cboLocation').val();
            url += '&dtFrom=' + $('#frmCustomerDispatch #txtFromDate11').val();
            url += '&dtTo=' + $('#frmCustomerDispatch #txtToDate11').val();
            url += '&dtFH=' + $('#frmCustomerDispatch #txtFH').val();
            url += '&dtFM=' + $('#frmCustomerDispatch #txtFM').val();
            url += '&dtTH=' + $('#frmCustomerDispatch #txtTH').val();
            url += '&dtTM=' + $('#frmCustomerDispatch #txtTM').val();
            window.open(url);
        }
    });

    //-------------------------------------------- 
    $('#frmSampleHitRate #cboSampleYear').change(function () {
        var customer = $('#frmSampleHitRate #cboCustomer').val();
        var year = $('#frmSampleHitRate #cboSampleYear').val();
        var graphic = $('#frmSampleHitRate #cboGraphic').val();
        var url = "controller.php?q=61&requestType=loadSampleNos&sampYear=" + year + "&graphic=" + graphic + "&customer=" + customer;
        ;
        var httpobj = $.ajax({url: url, async: false})
        $('#frmSampleHitRate #cboSampleNo').html(httpobj.responseText);

    });
    //-------------------------------------------- 
    $('#frmSampleHitRate #cboGraphic').change(function () {
        $('#frmSampleHitRate #cboSampleYear').val('');
        $('#frmSampleHitRate #cboSampleNo').val('');
    });
    //-------------------------------------------- 
    $('#frmSampleHitRate #cboCustomer').change(function () {
        var customer = $('#frmSampleHitRate #cboCustomer').val();
        var url = "controller.php?q=61&requestType=loadGraphicNos&customer=" + customer;
        var httpobj = $.ajax({url: url, async: false})
        $('#frmSampleHitRate #cboGraphic').html(httpobj.responseText);
        $('#frmSampleHitRate #cboSampleYear').val('');
        $('#frmSampleHitRate #cboSampleNo').val('');
    });
    //-------------------------------------------- 
    $('#frmSampleHitRate #butNew').click(function () {
        $('#frmSampleHitRate #cboSampleYear').val('');
        $('#frmSampleHitRate #cboSampleNo').val('');
        $('#frmSampleHitRate #cboCustomer').val('');
        $('#frmSampleHitRate #cboMarketer').val('');
        $('#frmSampleHitRate #cboGraphic').val('');
        $('#frmSampleHitRate #dtSampleDate').val('');

    });

    //-------------------------------------------- 
//--------------------------------------------
});






////////////////////////////////////////////
function ViewReport(type, formId)
{
    if ($('#' + formId).validationEngine('validate')) {
        var url = "report.php?type=" + type + "&" + $('#' + formId).serialize();
        window.open(url)
    }
}


///////////////////////////////////	
function downloadFile() {
    var date = $('#frmCompanyStockBalance #dtDateToS').val();
    window.open('excellReports/companyStockBalance/companyStockBalance-xlsx.php?company=' + $('#frmCompanyStockBalance #cboCompany').val() + '&date=' + date + '&location=' + $('#frmCompanyStockBalance #cboLocation').val() + '&currency=' + $('#frmCompanyStockBalance #cboCurrency').val());

}

////////////////////////////////////
function headShowHide(headId, bodyId) {

    $('#tblFabricMovementBody').hide('slow');
    $('#tblOrderWiseFabInOutBody').hide('slow');
    $('#tblCustomerWiseFabInOutBody').hide('slow');
    $('#tblKPIBody').hide('slow');
    $('#tblFabricCheckStatusBody').hide('slow');
    $('#tblCustomerDispatchBody').hide('slow');


    $('#tblFabricMovementHead').show('slow');
    $('#tblOrderWiseFabInOutHead').show('slow');
    $('#tblCustomerWiseFabInOutHead').show('slow');
    $('#tblKPIHead').show('slow');
    $('#tblFabricCheckStatusHead').show('slow');
    $('#tblCustomerDispatchHead').show('slow');



    $('#' + headId).hide('slow');
    $('#' + bodyId).show('slow');

}
//------------------------------------
function bodyShowHide(headId, bodyId) {
    $('#' + headId).show('slow');
    $('#' + bodyId).hide('slow');
}
////////////////////////////////////////

///////////////////////Order Wise Fabric In - Out///////////////////////////////
//------------------------------------------------------------------------
function submitForm() {
    window.location.href = "orderNoWiseFabricInOut/orderNoWiseFabricInOut.php?orderNo=" + $('#cboOrderNo').val()
            + '&salesOrderNo=' + $('#frmOrderWiseFabInOut #cboSalesOrderNo').val()
            + '&poNo=' + $('#frmOrderWiseFabInOut #cboPONo').val()
            + '&customer=' + $('#frmOrderWiseFabInOut #cboCustomer').val()
            + '&orderYear=' + $('#frmOrderWiseFabInOut #cboOrderYear').val()
}


function ViewReport_excel(type, formId, format)
{

    
   if (type == 'orderDetail') {
        var dateFrom = $('#frmOrderDetails #dailyOrderDateFrom').val();
        var dateTo   = $('#frmOrderDetails #dailyOrdersDateTo').val();
        
        
        var url = "controller.php?q=1277&requestType=loadOrderDetails&dateFrom="+dateFrom+"&dateTo="+dateTo;
        url += "&" + $('#' + formId).serialize();
        /*	var httpobj 	= $.ajax({url:url,async:false})
         alert(httpobj.responseText);
         return httpobj.responseText;*/

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'post',
            data: '',
            async: false,
            success: function (json) {
                $('#frmOrderDetails #butReports').validationEngine('showPrompt', json.msg, json.type /*'pass'*/);
                if (json.type == 'pass')
                {
                    var url = "presentation/customerAndOperation/NcingaDeliveryReports/rptDailyOrderDetails.php?type=" + type + "&report_type=" + format + "&" + $('#' + formId).serialize()+"&dateFrom="+dateFrom+"&dateTo="+dateTo;
                    window.open(url)
                    return;
                } else
                {
                    hideWaiting();
                }
            },
            error: function (xhr, status)
            {
                $('#frmOrderDetails #butReports').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
                var t = setTimeout("alertx()", 2000);
                hideWaiting();
                return;
                ;
            }
        });
        $('#frmOrderDetails')[0].reset();
    } else if (type == 'fabricDetail') {
        
        var dateFrom = $('#frmFabricDetails #dailyFabricDateFrom').val();
        var dateTo   = $('#frmFabricDetails #dailyFabricDateTo').val();
        
        var url = "controller.php?q=1277&requestType=loadFabricDetails&dateFrom="+dateFrom+"&dateTo="+dateTo;
        url += "&" + $('#' + formId).serialize();
        /*	var httpobj 	= $.ajax({url:url,async:false})
         alert(httpobj.responseText);
         return httpobj.responseText;*/

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'post',
            data: '',
            async: false,
            success: function (json) {
                $('#frmFabricDetails #butReports').validationEngine('showPrompt', json.msg, json.type /*'pass'*/);
                if (json.type == 'pass')
                {
                    var url = "presentation/customerAndOperation/NcingaDeliveryReports/rptDailyFabricReceivedDetails.php?type=" + type + "&report_type=" + format + "&" + $('#' + formId).serialize()+"&dateFrom="+dateFrom+"&dateTo="+dateTo;
                    window.open(url)
                    return;
                } else
                {
                    hideWaiting();
                }
            },
            error: function (xhr, status)
            {
                $('#frmFabricDetails #butReports').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
                var t = setTimeout("alertx()", 2000);
                hideWaiting();
                return;
                ;
            }
        });
         $('#frmFabricDetails')[0].reset();
    }
    var t = setTimeout("alertx()", 2000);

}

function alertx()
{
   
    $('#frmFabricDetails #butReports').validationEngine('hide');
    $('#frmOrderDetails #butReports').validationEngine('hide');
}

function formatDate() {
    var d = new Date(),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

///////////////////////////////////////////////////////////
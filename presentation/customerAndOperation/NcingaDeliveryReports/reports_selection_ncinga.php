<?php (define('UNLOCKPAGE', true) ? die('<<< Access denied >>>') : ''); ?>
<?php
$location = $sessions->getLocationId();
$company = $sessions->getCompanyId();
$intUser = $sessions->getUserId();

include_once "class/cls_commonErrorHandeling_get.php";

$obj_common_err = new cls_commonErrorHandeling_get($db);

$year = (!isset($_SESSION["Year"]) ? '' : $_SESSION["Year"]);
$month = (!isset($_SESSION["Month"]) ? '' : $_SESSION["Month"]);
?>
<title>Daily - Reports</title>
<!--<form id="frmPayrollReport" name="frmPayrollReport" autocomplete="off">
--><div align="center">
    <div class="trans_layoutL" style="width:700px">
        <div class="trans_text">Daily Reports</div>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">

            <!--daily order approved details -->    
            <tr>
                <td><table align="left" width="600" class="main_header_table" id="tblOrderDetailsHead" cellpadding="2" cellspacing="0">
                        <tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Daily Approved Order Details -(excel).</u></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td><form name="frmOrderDetails" id="frmOrderDetails" autocomplete="off">
                        <table width="700" align="left" border="0" id="tblOrderDetailsBody" class="main_table" cellspacing="0" style="display:none;">
                            <thead>
                                <tr>
                                    <td colspan="7" style="text-align:left">Daily Approved Order Details -(excel)</td>
                                </tr>
                            </thead>
                            <tbody>
                               <tr>
                                <td width="200" align="left"><span class="normalfnt">Date From</span></td>
                                <td width="443" align="left"><input name="txtFromDate" type="text" class="validate[required] txtbox" id="dailyOrderDateFrom" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);" onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value="" class="txtbox" style="visibility:hidden;width:2px;" onclick="return showCalendar(this.id, '%Y-%m-%');"/></td>
                            </tr>
                            <tr>
                            <td width="200" align="left"><span class="normalfnt">Date To</span></td>
                            <td width="443" align="left"><input name="txtToDate" type="text"
                                                                class="validate[required] txtbox" id="dailyOrdersDateTo" style="width:98px;"
                                                                onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();"
                                                                onkeypress="return ControlableKeyAccess(event);"
                                                                onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value="" class="txtbox" style="visibility:hidden;width:2px;"
                                                                                                                            onclick="return showCalendar(this.id, '%Y-%m-%');"/></td>
                            </tr>
                                <tr>
                                    <td colspan="7" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                            <tr>
                                                <td width="100%" align="center" bgcolor="">

                                                    <img  style="display:inline" border="0" src="images/Treport_exel.png" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport_excel('orderDetail', 'frmOrderDetails', 'excel')"/>
                                                    <a href="main.php">
                                                        <img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </tbody>
                        </table></form></td>
            </tr>

            <!--end of daily order approved details -->   
            <!--daily fabric received approved details -->
            <tr>
                <td><table align="left" width="600" class="main_header_table" id="tblFabricDetailsHead" cellpadding="2" cellspacing="0">
                        <tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Daily Fabric received Details -(excel).</u></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td><form name="frmFabricDetails" id="frmFabricDetails" autocomplete="off">
                        <table width="700" align="left" border="0" id="tblFbricDetailsBody" class="main_table" cellspacing="0" style="display:none;">
                            <thead>
                                <tr>
                                    <td colspan="7" style="text-align:left">Daily Fabric Received Details -(excel)</td>
                                </tr>
                            </thead>
                            <tbody>

                             <tr>
                                <td width="200" align="left"><span class="normalfnt">Date From</span></td>
                                <td width="443" align="left"><input name="txtFromDate" type="text" class="validate[required] txtbox" id="dailyFabricDateFrom" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);" onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value="" class="txtbox" style="visibility:hidden;width:2px;" onclick="return showCalendar(this.id, '%Y-%m-%');"/></td>
                            </tr>
                            <tr>
                            <td width="200" align="left"><span class="normalfnt">Date To</span></td>
                            <td width="443" align="left"><input name="txtToDate" type="text"
                                                                class="validate[required] txtbox" id="dailyFabricDateTo" style="width:98px;"
                                                                onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();"
                                                                onkeypress="return ControlableKeyAccess(event);"
                                                                onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value="" class="txtbox" style="visibility:hidden;width:2px;"
                                                                                                                            onclick="return showCalendar(this.id, '%Y-%m-%');"/></td>
                            </tr>        


                                <tr>
                                    <td colspan="7" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                            <tr>
                                                <td width="100%" align="center" bgcolor="">

                                                    <img  style="display:inline" border="0" src="images/Treport_exel.png" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport_excel('fabricDetail', 'frmFabricDetails', 'excel')"/>
                                                    <a href="main.php">
                                                        <img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </tbody>
                        </table></form></td>
            </tr>


        </table>
    </div>
</div>
<!--</form>
-->
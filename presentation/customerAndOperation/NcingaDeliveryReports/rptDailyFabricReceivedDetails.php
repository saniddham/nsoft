<?php
session_start();

include "../../../dataAccess/DBManager2.php";
include "../../../class/cls_dateTime.php";

$db = new DBManager2(1);
$cls_dateTime = new cls_dateTime($db);
$date = $cls_dateTime->getCurruntDate();

$db->connect();

$type       = $_REQUEST['type'];
$dateFrom   = $_REQUEST['dateFrom'];
$dateTo     = $_REQUEST['dateTo'];

$sql = getDailyFabricreceivedDetailsquery($dateFrom,$dateTo);
$result = $db->RunQuery($sql);

$fileName = "Daily Fabric Received  Details Report " . $date . ".xls";
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=\"" . $fileName . "\"");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <th width="20%">customerLocation</th>
        <th width="10%">plantLocation</th>
        <th width="10%"> lineNo</th>
        <th width="10%"> fabricReceivedNoteId</th>
        <th width="10%">customer </th>
        <th width="10%">brand </th>
        <th width="10%" >customerPO </th>
        <th width="10%" >customerSO </th>
        <th width="10%">orderNo </th>
        <th width="10%">style </th>
        <th width="10%">graphicNo </th>
        <th width="10%">backGroundColor </th>
        <th width="10%">size </th>
        <th width="10%">qty </th>
        <th width="10%">PSD</th>
        <th width="10%">deliveryDate </th>
        <th width="10%">year </th>
        <th width="10%">part </th>
        <th width="10%">cutNo </th>
        <th width="10%">PDPercentage</th>
        <th width="10%">overCutPercentage</th>
        <th width="10%">ncingaDeliveryStatus</th>

    </tr>
    <?php
    foreach ($result as $row) {
        //while($row = mysqli_fetch_array($result))
        ?>
        <tr>
            <td class="normalfnt" style="text-align: center"><?php echo $row['customerLocation']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['plantLocation']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['lineNo']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['fabricReceivedNoteId']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['customer']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo getBrand($row['intSampleNo'], $row['intSampleYear'], $row['intRevisionNo']); ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['customerPO']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['customerSO']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['orderNo']; ?></td>
            <td class="normalfnt" style="text-align: right"><?php echo $row['style']; ?></td>

            <td class="normalfnt" style="text-align: center"><?php echo $row['graphicNo']; ?></td>

            <td class="normalfnt" style="text-align: center"><?php echo $row['backGroundColor']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['size']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['qty']; ?></td>

            <td class="normalfnt" style="text-align: right"><?php echo ($row['PSD']); ?></td>
            <td class="normalfnt" style="text-align: right"><?php echo ($row['deliveryDate']); ?></td>
            <td class="normalfnt" style="text-align: right"><?php echo ($row['YEAR']); ?></td>
            <td class="normalfnt" style="text-align: right"><?php echo ($row['part']); ?></td>
            <td class="normalfnt" style="text-align: right"><?php echo ($row['cutNo']); ?></td>

            <td class="normalfnt" style="text-align: center"><?php echo $row['PDPercentage']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['overCutPercentage']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['ncingaDeliveryStatus']; ?></td>


        </tr>
        <?php
    }
    return;

    function getDailyFabricreceivedDetailsquery($dateFrom,$dateTo) {

        $sql = "SELECT
	mst_customer_locations_header.strName AS customerLocation,
	mst_locations.strName AS plantLocation,
	ware_fabricreceiveddetails.strLineNo AS lineNo,
	ware_fabricreceiveddetails.intFabricReceivedNo AS fabricReceivedNoteId,
	mst_customer.strName AS customer,
        -- mst_brand.strName AS brand,
	trn_orderheader.strCustomerPoNo AS customerPO,
	ware_fabricreceivedheader.intOrderNo AS orderNo,
	ware_fabricreceivedheader.intOrderYear AS YEAR,
	ware_fabricreceiveddetails.strCutNo AS cutNo,
	ware_fabricreceiveddetails.intSalesOrderId AS customerSO,
	mst_part.strName AS part,
	ware_fabricreceiveddetails.strSize AS size,
	ware_fabricreceiveddetails.dblQty AS qty,
	ware_fabricreceivedheader.strGraphicNo AS graphicNo,
	ware_fabricreceivedheader.strStyleNo AS style,
	mst_colors_ground.strName AS backGroundColor,
	trn_orderdetails.dtPSD AS PSD,
	trn_orderdetails.dtDeliveryDate AS deliveryDate,
	trn_orderdetails.dblDamagePercentage AS PDPercentage,
	trn_orderdetails.dblOverCutPercentage AS overCutPercentage,
        trn_orderdetails.intSampleNo,
        trn_orderdetails.intSampleYear,
        trn_orderdetails.intRevisionNo,
        ware_fabricreceiveddetails.ncingaDeliveryStatus
FROM
	trn_orderdetails
/*INNER JOIN trn_sampleinfomations_details ON trn_orderdetails.intSampleNo = trn_sampleinfomations_details.intSampleNo
AND trn_orderdetails.intSampleYear = trn_sampleinfomations_details.intSampleYear
AND trn_orderdetails.intRevisionNo = trn_sampleinfomations_details.intRevNo
AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName
AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo*/
-- INNER JOIN mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN ware_fabricreceivedheader ON ware_fabricreceivedheader.intOrderNo = trn_orderdetails.intOrderNo
AND ware_fabricreceivedheader.intOrderYear = trn_orderdetails.intOrderYear
INNER JOIN ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo
AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
AND ware_fabricreceiveddetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
INNER JOIN ware_fabricreceivedheader_approvedby ON ware_fabricreceivedheader_approvedby.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo
AND ware_fabricreceivedheader_approvedby.intYear = ware_fabricreceiveddetails.intFabricReceivedYear
INNER JOIN mst_part ON ware_fabricreceiveddetails.intPart = mst_part.intId
INNER JOIN mst_colors_ground ON ware_fabricreceiveddetails.intGroundColor = mst_colors_ground.intId
Inner Join mst_locations ON ware_fabricreceivedheader.intCompanyId = mst_locations.intId
INNER JOIN mst_customer_locations_header ON mst_customer_locations_header.intId = trn_orderheader.intCustomerLocation
INNER JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
WHERE
	date(
		ware_fabricreceivedheader_approvedby.dtApprovedDate
	) >= '$dateFrom' 
        
AND 
date(
		ware_fabricreceivedheader_approvedby.dtApprovedDate
	) <= '$dateTo' 
order by fabricReceivedNoteId";


        return $sql;
    }

    function getBrand($sampleNo, $sampleYear, $revNo) {
        global $db;
       
        $sql = "SELECT
				mst_brand.strName,
				mst_brand.intId
				FROM
				trn_sampleinfomations
				INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
				WHERE
				trn_sampleinfomations.intSampleNo = '$sampleNo' AND
				trn_sampleinfomations.intSampleYear = '$sampleYear' AND
				trn_sampleinfomations.intRevisionNo = '$revNo'";
        $result_brand = $db->RunQuery($sql);
        $row_brand = mysqli_fetch_array($result_brand);
        $brand = $row_brand['strName'];
        return $brand;
    }

    $db->disconnect();

<?php
session_start();

//ini_set('display_errors',1);
include "../../../dataAccess/DBManager2.php";
include "../../../class/cls_dateTime.php";

$db = new DBManager2(1);
$cls_dateTime = new cls_dateTime($db);
$date = $cls_dateTime->getCurruntDate();

$db->connect();

$type       = $_REQUEST['type'];
$dateFrom   = $_REQUEST['dateFrom'];
$dateTo     = $_REQUEST['dateTo'];


$sql = getDailyOrderDetailsquery($dateFrom,$dateTo);
$result = $db->RunQuery($sql);

$fileName = "Daily Approved Order Details Report " . $date . ".xls";
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=\"" . $fileName . "\"");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
header("Pragma: public");
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <th width="20%">customer</th>
        <th width="10%">customerLocation</th>
        <th width="10%"> company</th>
        <th width="10%"> companyLocation</th>
        <th width="10%">brand </th>
        <th width="10%">customerPO </th>
        <th width="10%" >customerSO </th>
        <th width="10%" >bundleTag</th>
        <th width="10%" >orderNo </th>
        <th width="10%">lineNo </th>
        <th width="10%">poDeliveryDate </th>
        <th width="10%">graphicNo </th>
        <th width="10%">sampleNo </th>
        <th width="10%">style </th>
        <th width="10%">backGroundColor </th>
        <th width="10%">size</th>
        <th width="10%">PSD </th>
        <th width="10%">deliveryDate </th>
        <th width="10%">qty </th>
        <th width="10%">poQty </th>
        <th width="10%">actualTotalQty</th>
        <th width="10%">date</th>
        <th width="10%">year</th>
        <th width="10%">part</th>
        <th width="10%">PDPercentage </th>
        <th width="10%">damagePercentage </th>
        <th width="10%">ncingaDeliveryStatus</th>
    </tr>
    <?php
    foreach ($result as $row) {
        echo $row['YEAR'];
        //while($row = mysqli_fetch_array($result))
        ?>
        <tr>
            <td class="normalfnt" style="text-align: center"><?php echo $row['customer']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['customerLocation']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['company']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['companyLocation']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['brand']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['customerPO']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['customerSO']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['bundleTag']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['orderNo']; ?></td>
            <td class="normalfnt" style="text-align: right"><?php echo $row['lineNo']; ?></td>

            <td class="normalfnt" style="text-align: center"><?php echo $row['poDeliveryDate']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['graphicNo']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['sampleNo']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['style']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['backGroundColor']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['size']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['PSD']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['deliveryDate']; ?></td>
            <td class="normalfnt" style="text-align: right"><?php echo $row['qty']; ?></td>

            <td class="normalfnt" style="text-align: center"><?php echo $row['poQty']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['actualTotalQty']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['date']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['orderYear']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['part']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['PDPercentage']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['damagePercentage']; ?></td>
            <td class="normalfnt" style="text-align: center"><?php echo $row['ncingaDeliveryStatus']; ?></td>
        </tr>
        <?php
    }
    return;

    function getDailyOrderDetailsquery($dateFrom,$dateTo) {

        $sql = "SELECT
	mst_customer.strName AS customer,
	mst_customer_locations_header.strName AS customerLocation,
	mst_companies.strName AS company,
	mst_locations.strName AS companyLocation,
	mst_brand.strName AS brand,
	trn_orderheader.strCustomerPoNo AS customerPO,
	trn_orderdetails.intSalesOrderId AS customerSO,
        mst_customer_locations.BundleTag AS bundleTag,
	trn_orderdetails.intOrderNo AS orderNo,
	trn_orderdetails.strLineNo AS lineNo,
	trn_orderheader.dtDeliveryDate AS poDeliveryDate,
	trn_orderdetails.strGraphicNo AS graphicNo,
	trn_orderdetails.intSampleNo AS sampleNo,
	trn_orderdetails.strStyleNo AS style,
	mst_colors_ground.strName AS backGroundColor,
	trn_ordersizeqty.strSize AS size,
	trn_orderdetails.dtPSD AS PSD,
	trn_orderdetails.dtDeliveryDate AS deliveryDate,
	trn_ordersizeqty.dblQty AS qty,
	trn_orderdetails.intQty AS poQty,
	0 AS actualTotalQty,
	trn_orderheader.dtDate AS date,
	trn_orderdetails.intOrderYear AS orderYear,
	mst_part.strName AS part,
	trn_orderdetails.dblDamagePercentage AS PDPercentage,
	trn_orderdetails.dblDamagePercentage AS damagePercentage,
	trn_ordersizeqty.ncingaDeliveryStatus
FROM
	trn_orderheader
INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
INNER JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
INNER JOIN mst_customer_locations_header ON mst_customer_locations_header.intId = trn_orderheader.intCustomerLocation
INNER JOIN mst_customer_locations ON mst_customer_locations.intCustomerId = mst_customer.intId
AND mst_customer_locations.intLocationId = mst_customer_locations_header.intId
INNER JOIN mst_companies ON mst_companies.intId = mst_locations.intCompanyId
INNER JOIN mst_part ON mst_part.intId = trn_orderdetails.intPart
INNER JOIN trn_ordersizeqty ON trn_ordersizeqty.intOrderNo = trn_orderdetails.intOrderNo
AND trn_ordersizeqty.intOrderYear = trn_orderdetails.intOrderYear
AND trn_ordersizeqty.intSalesOrderId = trn_orderdetails.intSalesOrderId
INNER JOIN trn_sampleinfomations_details ON trn_orderdetails.intSampleNo = trn_sampleinfomations_details.intSampleNo
AND trn_orderdetails.intSampleYear = trn_sampleinfomations_details.intSampleYear
AND trn_orderdetails.intRevisionNo = trn_sampleinfomations_details.intRevNo
AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName
AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo
INNER JOIN mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
INNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
INNER JOIN trn_orderheader_approvedby ON trn_orderheader.intOrderNo = trn_orderheader_approvedby.intOrderNo
AND trn_orderheader_approvedby.intYear = trn_orderheader.intOrderYear

WHERE
	trn_orderheader.intStatus = 1
AND date(
	trn_orderheader_approvedby.dtApprovedDate) >= '$dateFrom' 
AND date(
	trn_orderheader_approvedby.dtApprovedDate) <= '$dateTo'
AND trn_orderheader_approvedby.intApproveLevelNo = 3
AND trn_orderheader_approvedby.intStatus = 0
GROUP BY
	trn_orderdetails.intOrderNo,
	trn_orderdetails.intOrderYear,
	trn_orderdetails.intSalesOrderId,
	trn_ordersizeqty.strSize ";

//echo $sql;
        return  $sql;
    }

    $db->disconnect();
    
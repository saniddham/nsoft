<?php
session_start();
$requestType = $_REQUEST['requestType'];
//$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$userId = $_SESSION['userId'];
$location = $_SESSION['CompanyID'];
$company = $_SESSION['headCompanyId'];
//include "{$backwardseperator}dataAccess/Connector.php";

 if ($requestType == 'loadOrderDetails') {
    $db->connect();
    $dateFrom = $_REQUEST['dateFrom'];
    $dateTo   = $_REQUEST['dateTo'];

    $sql = "SELECT
	mst_customer.strName AS customer,
	mst_customer_locations_header.strName AS customerLocation,
	mst_companies.strName AS company,
	mst_locations.strName AS companyLocation,
	mst_brand.strName AS brand,
	trn_orderheader.strCustomerPoNo AS customerPO,
	trn_orderdetails.intSalesOrderId AS customerSO,
	trn_orderdetails.intOrderNo AS orderNo,
	trn_orderdetails.strLineNo AS lineNo,
	trn_orderheader.dtDeliveryDate AS poDeliveryDate,
	trn_orderdetails.strGraphicNo AS graphicNo,
	trn_orderdetails.intSampleNo AS sampleNo,
	trn_orderdetails.strStyleNo AS style,
	mst_colors_ground.strName AS backGroundColor,
	trn_ordersizeqty.strSize AS size,
	trn_orderdetails.dtPSD AS PSD,
	trn_orderdetails.dtDeliveryDate AS deliveryDate,
	trn_ordersizeqty.dblQty AS qty,
	trn_orderdetails.intQty AS poQty,
	0 AS actualTotalQty,
	trn_orderheader.dtDate AS date,
	trn_orderdetails.intOrderYear AS orderYear,
	mst_part.strName AS part,
	trn_orderdetails.dblDamagePercentage AS PDPercentage,
	trn_orderdetails.dblDamagePercentage AS damagePercentage,
	trn_ordersizeqty.ncingaDeliveryStatus
FROM
	trn_orderheader
INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
INNER JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
INNER JOIN mst_customer_locations_header ON mst_customer_locations_header.intId = trn_orderheader.intCustomerLocation
INNER JOIN mst_companies ON mst_companies.intId = mst_locations.intCompanyId
INNER JOIN mst_part ON mst_part.intId = trn_orderdetails.intPart
INNER JOIN trn_ordersizeqty ON trn_ordersizeqty.intOrderNo = trn_orderdetails.intOrderNo
AND trn_ordersizeqty.intOrderYear = trn_orderdetails.intOrderYear
AND trn_ordersizeqty.intSalesOrderId = trn_orderdetails.intSalesOrderId
INNER JOIN trn_sampleinfomations_details ON trn_orderdetails.intSampleNo = trn_sampleinfomations_details.intSampleNo
AND trn_orderdetails.intSampleYear = trn_sampleinfomations_details.intSampleYear
AND trn_orderdetails.intRevisionNo = trn_sampleinfomations_details.intRevNo
AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName
AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo
INNER JOIN mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
INNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
INNER JOIN trn_orderheader_approvedby ON trn_orderheader.intOrderNo = trn_orderheader_approvedby.intOrderNo
AND trn_orderheader_approvedby.intYear = trn_orderheader.intOrderYear
WHERE
	trn_orderheader.intStatus = 1
AND date(
	trn_orderheader_approvedby.dtApprovedDate) >= '$dateFrom' 
AND date(
	trn_orderheader_approvedby.dtApprovedDate) <= '$dateTo'
AND trn_orderheader_approvedby.intApproveLevelNo = 3
AND trn_orderheader_approvedby.intStatus = 0
GROUP BY
	trn_orderdetails.intOrderNo,
	trn_orderdetails.intOrderYear,
	trn_orderdetails.intSalesOrderId,
	trn_ordersizeqty.strSize ";

//echo $sql;
    $result = $db->RunQuery($sql);

    $r = $db->numRows();

    if ($r <= 0) {
        $resp['type'] = 'fail';
        $resp['msg'] = 'No data to export';
    } else {
        $resp['type'] = 'pass';
        $resp['msg'] = '';
    }

    echo json_encode($resp);
    $db->disconnect();
} 
else if ($requestType == 'loadFabricDetails') {
    $db->connect();
    $dateFrom = $_REQUEST['dateFrom'];
    $dateTo   = $_REQUEST['dateTo'];

    $sql = "SELECT
	mst_customer_locations_header.strName AS customerLocation,
	mst_locations.strName AS plantLocation,
	ware_fabricreceiveddetails.strLineNo AS lineNo,
	ware_fabricreceiveddetails.intFabricReceivedNo AS fabricReceivedNoteId,
	mst_customer.strName AS customer,
        -- mst_brand.strName AS brand,
	trn_orderheader.strCustomerPoNo AS customerPO,
	ware_fabricreceivedheader.intOrderNo AS orderNo,
	ware_fabricreceivedheader.intOrderYear AS YEAR,
	ware_fabricreceiveddetails.strCutNo AS cutNo,
	ware_fabricreceiveddetails.intSalesOrderId AS customerSO,
	mst_part.strName AS part,
	ware_fabricreceiveddetails.strSize AS size,
	ware_fabricreceiveddetails.dblQty AS qty,
	ware_fabricreceivedheader.strGraphicNo AS graphicNo,
	ware_fabricreceivedheader.strStyleNo AS style,
	mst_colors_ground.strName AS backGroundColor,
	trn_orderdetails.dtPSD AS PSD,
	trn_orderdetails.dtDeliveryDate AS deliveryDate,
	trn_orderdetails.dblDamagePercentage AS PDPercentage,
	trn_orderdetails.dblOverCutPercentage AS overCutPercentage,
        trn_orderdetails.intSampleNo,
        trn_orderdetails.intSampleYear,
        trn_orderdetails.intRevisionNo,
        ware_fabricreceiveddetails.ncingaDeliveryStatus
FROM
	trn_orderdetails
/*INNER JOIN trn_sampleinfomations_details ON trn_orderdetails.intSampleNo = trn_sampleinfomations_details.intSampleNo
AND trn_orderdetails.intSampleYear = trn_sampleinfomations_details.intSampleYear
AND trn_orderdetails.intRevisionNo = trn_sampleinfomations_details.intRevNo
AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName
AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo*/
-- INNER JOIN mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN ware_fabricreceivedheader ON ware_fabricreceivedheader.intOrderNo = trn_orderdetails.intOrderNo
AND ware_fabricreceivedheader.intOrderYear = trn_orderdetails.intOrderYear
INNER JOIN ware_fabricreceiveddetails ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo
AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear
AND ware_fabricreceiveddetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
INNER JOIN ware_fabricreceivedheader_approvedby ON ware_fabricreceivedheader_approvedby.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo
AND ware_fabricreceivedheader_approvedby.intYear = ware_fabricreceiveddetails.intFabricReceivedYear
INNER JOIN mst_part ON ware_fabricreceiveddetails.intPart = mst_part.intId
INNER JOIN mst_colors_ground ON ware_fabricreceiveddetails.intGroundColor = mst_colors_ground.intId
Inner Join mst_locations ON ware_fabricreceivedheader.intCompanyId = mst_locations.intId
INNER JOIN mst_customer_locations_header ON mst_customer_locations_header.intId = trn_orderheader.intCustomerLocation
INNER JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
WHERE
	date(
		ware_fabricreceivedheader_approvedby.dtApprovedDate
	) >= '$dateFrom' 
        
AND 
date(
		ware_fabricreceivedheader_approvedby.dtApprovedDate
	) <= '$dateTo'  ";

//echo $sql;
    $result = $db->RunQuery($sql);

    $r = $db->numRows();

    if ($r <= 0) {
        $resp['type'] = 'fail';
        $resp['msg'] = 'No data to export';
    } else {
        $resp['type'] = 'pass';
        $resp['msg'] = '';
    }

    echo json_encode($resp);
    $db->disconnect();
}
?>

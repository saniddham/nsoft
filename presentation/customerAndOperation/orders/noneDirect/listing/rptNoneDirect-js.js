//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////


// JavaScript Document

$(document).ready(function() {
//	$('#frmlisting').validationEngine();
	
	$('#frmReport #imgApprove').click(function(){
	var val = $.prompt('Are you sure you want to approve?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
						if(validateQuantities()==0){
							var url = "rptNoneDirect-db-set.php"+window.location.search+'&status=approve';
							var obj = $.ajax({url:url,async:false});
						 	window.location.href = window.location.href;
						 	window.opener.location.reload();//reload listing page
						}
					}
				}});
	});
	
	$('#frmReport #imgReject').click(function(){
	var val = $.prompt('Are you sure you want to reject?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										if(validateRejecton()==0){
											var url = "rptNoneDirect-db-set.php"+window.location.search+'&status=reject';
											var obj = $.ajax({url:url,async:false});
											window.location.href = window.location.href;
											window.opener.location.reload();//reload listing page
										}
									}
								}});
	});
	
	
});
//---------------------------------------------------
function validateQuantities(){
		var ret=0;	
	    var projectNo = document.getElementById('divProjectNo').innerHTML;
	    var year = document.getElementById('divYear').innerHTML;
		var url 		= "rptNoneDirect-db-get.php?requestType=getValidation";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"projectId="+projectNo+"&year="+year,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",3000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//---------------------------------------------------
function validateRejecton(){
		var ret=0;	
	    var projectNo = document.getElementById('divProjectNo').innerHTML;
	    var year = document.getElementById('divYear').innerHTML;
		var url 		= "rptNoneDirect-db-get.php?requestType=validateRejecton";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"projectId="+projectNo+"&year="+year,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",3000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//-----------------------------------------------------
function alertx()
{
	$('#frmReport #imgApprove').validationEngine('hide')	;
}
//------------------------------------------------
function alertx1()
{
//	$('#frmlisting .butRevise').validationEngine('hide')	;
}
//--------------------------------------------
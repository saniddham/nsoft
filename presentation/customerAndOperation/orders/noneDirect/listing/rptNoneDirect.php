<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////

session_start();
$backwardseperator = "../../../../../";
$companyId = $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";

$serialNo = $_REQUEST['projectId'];
$year = $_REQUEST['year'];

$approveMode = $_REQUEST['approveMode'];

/*$projectId ='100001';
$subProjectId=1;
$approveMode = 1;
*/
$programName='None-Direct';
$programCode='P0609';
$issueApproveLevel = (int)getApproveLevel($programName);

/*$gatePassNo = '100000';
$year = '2012';
$approveMode=1;
*/
       $sql = "SELECT distinct 
				trn_orderheader.intOrderNo,
				trn_orderheader.intOrderYear,
				trn_orderheader.intCRNId, 
				trn_orderheader.intCustomer,
				trn_orderheader.strOrderCode,
				trn_orderheader.strOrderName,
				trn_orderheader.strProjectAddress,
				trn_orderheader.intCurrency, 
				mst_financecurrency.strCode as currency, 
				trn_orderheader.intStatus,
				trn_orderheader.intApproveLevelStart,
				trn_orderheader.intCreator,
				trn_orderheader.dtmCreateDate,
				trn_orderheader.intModifyer,
				trn_orderheader.dtmModifyDate , 
				trn_orderheader.intLocationId , 
				sys_users.strUserName 
				FROM
				trn_orderheader 
				Inner Join mst_financecurrency ON trn_orderheader.intCurrency = mst_financecurrency.intId 
				Inner Join sys_users ON trn_orderheader.intCreator = sys_users.intUserId
				WHERE
trn_orderheader.intOrderNo =  '$serialNo'  AND 
trn_orderheader.intOrderYear =  '$year'
";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$code = $row['strOrderCode'];
					$project = $row['strOrderName'];
					$user = $row['strUserName']; 
					$date=substr($row['dtmCreateDate'],0,10);
					$locationId = $row['intLocationId'];//this locationId use in report header(reportHeader.php)--------------------
					$intStatus = $row['intStatus'];
					$savedLevels = $row['intApproveLevelStart'];
					$intCreateCompanyId		= $row['intCompanyId'];
					$intCreateUserId		= $row['intCreator'];
					$strAddress		= $row['strProjectAddress'];
					$currency = $row['currency'];
					
				 }
				 
$confirmationMode=loadConfirmatonMode($programCode,$intStatus,$savedLevels,$intUser);
$rejectionMode=loadRejectionMode($programCode,$intStatus,$savedLevels,$intUser);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Project Report</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="rptNoneDirect-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:273px;
	top:170px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="../../../../../images/pending.png"  /></div>
<?php
}
?>
<form id="frmReport" name="frmReport" method="post" action="rptNoneDirect.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>PROJECTS REPORT</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
    <?php
	if($intStatus>1)
	{
	?>
    <?php if($approveMode==1){
			if($confirmationMode==1) { 
			?>
			<img src="../../../../../images/approve.png" align="middle" class="mouseover noPrint" id="imgApprove" />
			<?php
			}
			if($rejectionMode==1) { 
			?>
            <img src="../../../../../images/reject.png" align="middle" class="mouseover noPrint" id="imgReject" />
                <?php
                }
            }
	}
	?>
    </td>
  </tr>
  <tr>
  <?php
 	if($intStatus==1)
	{
	?>
   <td colspan="9" class="APPROVE" >CONFIRMED</td>
   <?PHP
	}
	else if($intStatus==0)
	{
   ?>
   <td colspan="9" class="APPROVE" style="color:#F00">REJECTED</td>
   <?php
	}
	else
	{
   ?>
   <td width="10%" colspan="9" class="APPROVE">PENDING</td>
   <?php
	}
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="11%"><span class="normalfnt"><strong>Project Code</strong></span></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    
    <td width="26%"><span class="normalfnt"><?php echo $code ?></span></td>
    <td width="8%" class="normalfnt"><strong>Project</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="28%"><span class="normalfnt"><?php echo $serialNo."/".$year; ?></span></td>
    <td width="1%"><div id="divProjectNo" style="display:none"><?php echo $serialNo ?></div><div id="divYear" style="display:none"><?php echo $year ?></div></td>
  <td width="3%"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Created By</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $user  ?></span></td>
    <td><span class="normalfnt"><strong>Address</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $strAddress ?></span></td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Date</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $date  ?></span></td>
    <td class="normalfnt"><strong>Currency</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $currency  ?></span></td>
    <td>&nbsp;</td>
  </tr>
  
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="gridHeader">
              <td width="20%" >Sub Project ID</td>
              <td width="19%" >Code</td>
              <td width="44%" >Description</td>
              <td width="17%" >Estimate Cost</td>
              </tr>
              <?php 
	  	   $sql1 = "SELECT
				trn_orderdetails.strSalesOrderNo,
				trn_orderdetails.intSalesOrderId,
				trn_orderdetails.strSubProjectDesc,
				trn_orderdetails.dblPrice 
				FROM trn_orderdetails
				WHERE
				trn_orderdetails.intOrderNo =  '$serialNo' AND 
				trn_orderdetails.intOrderYear =  '$year'
				ORDER BY
				trn_orderdetails.intOrderNo ASC, 
				trn_orderdetails.intOrderYear ASC  ";
			$result1 = $db->RunQuery($sql1);
			$totQty=0;
			$totAmmount=0;
			while($row=mysqli_fetch_array($result1))
			{
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" >&nbsp;<?php echo $row['intSalesOrderId'];?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strSalesOrderNo'];?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strSubProjectDesc'];?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblPrice'] ?>&nbsp;</td>
              </tr>
      <?php 
			$totamount+=$row['dblPrice'];
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $totamount ?>&nbsp;</td>
              </tr>
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<?php  
 	if($intStatus!=0)
	{

				for($i=1; $i<=$savedLevels; $i++)
				{
					   $sqlc = "SELECT
							trn_orderheader_approvedby.intApproveUser,
							trn_orderheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName, 
							trn_orderheader_approvedby.intApproveLevelNo
							FROM
							trn_orderheader_approvedby
							Inner Join sys_users ON trn_orderheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							trn_orderheader_approvedby.intOrderNo =  '$serialNo' AND 
							trn_orderheader_approvedby.intYear =  '$year' AND
							trn_orderheader_approvedby.intApproveLevelNo =  '$i'  order by intApproveLevelNo asc
";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
						if($i==1)
						$desc="1st ";
						else if($i==2)
						$desc="2nd ";
						else if($i==3)
						$desc="3rd ";
						else
						$desc=$i."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
				?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
            </tr>
<?php
			}
	}
	else{
					 $sqlc = "SELECT
							trn_orderheader_approvedby.intApproveUser,
							trn_orderheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							trn_orderheader_approvedby.intApproveLevelNo
							FROM
							trn_orderheader_approvedby
							Inner Join sys_users ON trn_orderheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							trn_orderheader_approvedby.intOrderNo =  '$serialNo' AND  
							trn_orderheader_approvedby.intYear =  '$year' AND
							trn_orderheader_approvedby.intApproveLevelNo =  '0'";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Rejected By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
	}
?>

<tr height="120">
  <td align="center" class="normalfntMid"></td>
</tr>

<tr height="90" >
<?Php 
$field1="Project";
$field2="Year";
	$url  = "{$backwardseperator}presentation/sendToApproval.php";		// * file name
	$url .= "?status=$intStatus";										// * set recent status
	$url .= "&approveLevels=$savedLevels";								// * set approve levels in order header
	$url .= "&programCode=$programCode";								// * program code (ex:P10001)
	$url .= "&companyId=$company";									// * created company id
	$url .= "&programName=PROJECT";									// * program name (ex:Purchase Order)
	$url .= "&val1=$serialNo";	
	$url .= "&val2=$year";	
	$url .= "&fieldName1=$field1";	
	$url .= "&fieldName2=$field2";	
	$url .= "&createUserId=$intCreateUserId";							// * doc year
	$url .= "&link=".urlencode(base64_encode($mainPath."presentation/customerAndOperation/orders/noneDirect/listing/rptNoneDirect.php?projectId=$serialNo&approveMode=1"));
?>
  <td align="center" style="vertical-align:top" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:150px;border:none"  ></iframe></td>
</tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//-------------------------------------------------------------------------
?>	

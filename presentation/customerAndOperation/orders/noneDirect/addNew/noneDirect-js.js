function functionList(serialNo,year)
{
//	loadSearchCombo();
	//loadCRNCombo();
	loadCustomerCombo();
	loadCurrencyCombo();
	
	if(serialNo!='')
	{
		$('#frmOrders #txtOrderNo').val(serialNo);
		$('#frmOrders #txtYear').val(year);
		loadProjectDetails();
		
		//$('#frmOrders #cboSearch').change();
	}
	viewButtons();
}		
$(document).ready(function() {
  		$("#frmOrders").validationEngine();
		$('#frmOrders #txtCode').focus();
  //permision for add 
  
  if(intAddx)
  {
 	$('#frmOrders #butNew').show();
	$('#frmOrders #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmOrders #butSave').show();
	//$('#frmOrders #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmOrders #butDelete').show();
	//$('#frmOrders #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	//$('#frmOrders #cboSearch').removeAttr('disabled');
  }
  
   $('#frmOrders #cboSalesType').live('change',function(){
	//	window.location.href = "non_direct_sales.php";
	if($('#frmOrders #cboSalesType').val()=='')
		window.location.href = "noneDirect.php";
	else if($('#frmOrders #cboSalesType').val()==1)
		window.location.href = "direct_sales.php";
	else if($('#frmOrders #cboSalesType').val()==2)
		window.location.href = "non_direct_sales.php";
   });
 //-------------------------------------------------------
   $('#frmOrders #cboCustomer').live('change',function(){
	   
/*		var data="cboCustomer=" +	$('#cboCustomer').val();
		var url 		= "noneDirect-db.php?requestType=loadCustomerAddress";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data: data,
			async:false,
			success:function(json){
					document.getElementById("txtAddress").innerHTML=json.arrCombo;
			}
		});
*/   });
 //------------------------------------------------------- 
	$('.delImg').live('click',function(){
			if($(this).parent().parent().parent().find('tr').length>3)
			$(this).parent().parent().remove();
		});
  
 //------------------------------------------------------- 
$('#butInsertRow').live('click',function(){
		
	var rowCount = document.getElementById('tblSubProjects').rows.length;
	document.getElementById('tblSubProjects').insertRow(rowCount-1);
	rowCount = document.getElementById('tblSubProjects').rows.length;
	document.getElementById('tblSubProjects').rows[rowCount-2].innerHTML = document.getElementById('tblSubProjects').rows[rowCount-3].innerHTML;
	document.getElementById('tblSubProjects').rows[rowCount-2].className="normalfnt";
	document.getElementById('tblSubProjects').rows[rowCount-2].id = 'dataRow';
	//$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-2)+') .proID').html(($(this).parent().parent().index()-2));
	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') .proID').html(parseFloat(($(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-2)+') .proID').html()))+1);
	
$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') .subCode').val('');
$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') .subDesc').val('');
$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-1)+') .subCost').val(0);
	
//	alert($(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-2)+') .proID').html());
	
/*	$(this).parent().parent().parent().find('tr:eq('+($(this).parent().parent().index()-2)+') .delDate').attr('id','txtDelDate'+($(this).parent().parent().index()-1));*/	
	});
 //-------------------------------------------------------
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

  ///save button click event
  $('#frmOrders #butSave').click(function(){
	//$('#frmOrders').submit();
	var requestType = '';
	if ($('#frmOrders').validationEngine('validate'))   
    { 
	showWaiting();

		var data = "requestType=save";
			data+="&serialNo=" +	$('#txtOrderNo').val();
			data+="&year=" +	$('#txtYear').val();
			data+="&cboCRN=" +	$('#cboCRN').val();
			data+="&date=" +	$('#dtDate').val();
			data+="&cboCustomer=" +	$('#cboCustomer').val();
			data+="&txtCode=" +		URLEncode($('#txtCode').val());
			data+="&txtName="	+		URLEncode($('#txtName').val());
			data+="&txtAddress="	+	URLEncode($('#txtAddress').val());
			data+="&chkActive="	+	$('#chkActive').val();
			data+="&currency="	+	URLEncode($('#cboCurrency').val());
			
			//////////////////// saving main grid part //////////////////////////////////////////
			//var rowCount = $('#tblMain >tbody >tr').length;
			var rowCount = document.getElementById('tblSubProjects').rows.length;
			var row = 0;
			
			var arr="[";
			var salesIds='';
			$('#tblSubProjects >tbody >tr').not(':last').not(':first').each(function(){
					 
					 arr += "{";
					 
					var subProjId 	= 	parseFloat($(this).find('.proID').html());
					var subCode 		= 	$(this).find('.subCode').val();
					var subDesc 		= 	$(this).find('.subDesc').val();
					var subEstCost	 	= 	$(this).find('.subCost').val();
					
					
					if((subCode=='') || (subDesc=='') || (subEstCost==''))
					{
						hideWaiting();
						alert('Please Enter Data');
						
						return;
						//return 0;
					}/// end if
					//alert(1);
					arr += '"subProjId":"'+	 subProjId +'",' 		;
					arr += '"subCode":"'+   	subCode +'",' 		;
					arr += '"subDesc":"'+	 subDesc +'",' 		;
					arr += '"subEstCost":"'+ 	subEstCost +'"' 		;
					arr +=  '},';
			});
			arr = arr.substr(0,arr.length-1);
			
			arr += " ]";
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = "noneDirect-db.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			data:data,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmOrders #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						/*window.location.href = "projects.php";*/
						//loadSearchCombo();
						var t=setTimeout("alertx()",1000);
						$('#frmOrders #txtOrderNo').val(json.serialNo);
						$('#frmOrders #txtYear').val(json.year);
						//loadProjectDetails();
						viewButtons();
						
						if(json.confirmationMode==1){
							$('#butConfirm').show();
						}
						else{
							$('#butConfirm').hide();
						}
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmOrders #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
		hideWaiting();	
	}
   });
   
   /////////////////////////////////////////////////////
   //// load project details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmOrders #cboSearch').click(function(){
	   $('#frmOrders').validationEngine('hide');
   });
   
    $('#frmOrders #cboSearch').change(function(){
		loadProjectDetails();
	});
	
    $('#frmOrders #cboCRN').change(function(){
		if($('#frmOrders #cboCRN').val()!=''){
		//$('#frmOrders #cboCustomer').attr('disabled',true);
		}
		else{
		//$('#frmOrders #cboCustomer').attr('disabled',false);
		}
		//loadCustomer();
	});
	
	$('#frmOrders #butNew').click(function(){
		window.location.href = "noneDirect.php";
		$('#frmOrders').get(0).reset();
		loadSearchCombo();
		$('#frmOrders #txtCode').focus();
	});
    $('#frmOrders #butDelete').click(function(){
		if($('#frmOrders #cboSearch').val()=='')
		{
			$('#frmOrders #butDelete').validationEngine('showPrompt', 'Please select project.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmOrders #txtOrderNo').val()+'/'+$('#frmOrders #txtYear').val()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										showWaiting();
										var url = "noneDirect-db.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&serialNo='+$('#frmOrders #txtOrderNo').val()+'&year'+$('#frmOrders #txtYear').val(),
											async:false,
											success:function(json){
												
												$('#frmOrders #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													window.location.href = "noneDirect.php";
													//loadSearchCombo();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
										hideWaiting();	
									}
				}
		 	});
			
		}
	});
});



//----------------------------------------------------
function loadSearchCombo(){
	
		var url 		= "noneDirect-db.php?requestType=loadSearchCombo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"",
			async:false,
			success:function(json){
					document.getElementById("cboSearch").innerHTML=json.arrCombo;
			}
		});
	
}
//----------------------------------------------------
function loadCRNCombo(){
		var url 		= "noneDirect-db.php?requestType=loadCRNCombo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"",
			async:false,
			success:function(json){
				//	document.getElementById("cboCRN").innerHTML=json.arrCombo;
			}
		});
}
//----------------------------------------------------
function loadCustomerCombo(){
		var url 		= "noneDirect-db.php?requestType=loadCustomerCombo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"",
			async:false,
			success:function(json){
					document.getElementById("cboCustomer").innerHTML=json.arrCombo;
			}
		});
}
//----------------------------------------------------
function loadCurrencyCombo(){
		var url 		= "noneDirect-db.php?requestType=loadCurrencyCombo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"",
			async:false,
			success:function(json){
					document.getElementById("cboCurrency").innerHTML=json.arrCombo;
			}
		});
}
//----------------------------------------------------
function alertx()
{
	$('#frmOrders #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmOrders #butDelete').validationEngine('hide')	;
}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function setInitialRows(id,code,desc,cost,flag){
	if(flag==1){
		var rowCount = document.getElementById('tblSubProjects').rows.length;
				for(var i=1;i<rowCount;i++)
				{
						document.getElementById('tblSubProjects').deleteRow(1);
						
				}
	}
			var content='';

			content='<tr class="normalfnt" >';
			content+='<td align="center" bgcolor="#FFFFFF"><img src="../../../../../images/del.png" width="15" height="15" class="delImg" /></td>'
			content+='<td bgcolor="#FFFFFF" align="center"  style="width:40px" class="proID">'+id+'</td>';
			content+='<td bgcolor="#FFFFFF"  style="width:70px"><input class="validate[required, max[50]] subCode" type="text" name="txtSubCode" id="txtSubCode"  style="width:150px" value="'+code+'"/></td>';
		  content+='<td bgcolor="#FFFFFF"><input name="txtSubDesc" type="text" id="txtSubDesc" style="width:310px"  class="validate[required, max[250]]  subDesc" value="'+desc+'"/></td>';
		 content+='<td bgcolor="#FFFFFF" align="center"><input name="txtEstCost" type="text" id="txtEstCost" style="width:120px" class="validate[required,min[0],custom[number]]  subCost" value="'+cost+'"/></td>';
		 content+=' </tr>';
			//alert(arrData[i]['strCode']);
		add_new_row('#frmOrders #tblSubProjects',content);
		
}

function setLastRow(){
	
			var content='';
			content+='<tr class="dataRow">';
            content+='<td colspan="6" align="left"  bgcolor="#FFFFFF" ><img src="../../../../../images/Tadd.jpg" name="butInsertRow" width="78" height="24" class="mouseover" id="butInsertRow" /></td>';
           content+='</tr>';
		
		add_new_row('#frmOrders #tblSubProjects',content);
}

function loadProjectDetails(){
	
		$('#frmOrders').validationEngine('hide');
		//showWaiting();
		var url = "noneDirect-db.php";
		if($('#frmOrders #txtOrderNo').val()=='')
		{ 
			setInitialRows(1,'','','',1);
			setLastRow();
			$('#frmOrders').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadHeader&serialNo='+$('#frmOrders #txtOrderNo').val()+'&year='+$('#frmOrders #txtYear').val(),
			async:false,
			success:function(json){
					var arrData = json.arrData;
					//json  = eval('('+json+')');
					var i=0;
				//	alert(arrData[i]['strCode']);
				//	$('#frmOrders #txtID').val(arrData[i]['intProjectId']);
					$('#frmOrders #txtCode').val(arrData[i]['strOrderCode']);
					$('#frmOrders #cboCRN').val(arrData[i]['intCrnId']);
					$('#frmOrders #cboCustomer').val(arrData[i]['intCustomer']);
					$('#frmOrders #txtName').val(arrData[i]['strOrderName']);
					$('#frmOrders #txtAddress').val(arrData[i]['strProjectAddress']);
					$('#frmOrders #cboCurrency').val(arrData[i]['intCurrency']);
					if(arrData[i]['intStatus'])
						$('#frmOrders #chkActive').attr('checked',true);
					else
						$('#frmOrders #chkActive').removeAttr('checked');
			}
	});
	//////////// end of load header /////////////////
		var url = "noneDirect-db.php";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&serialNo='+$('#frmOrders #txtOrderNo').val()+'&year='+$('#frmOrders #txtYear').val(),
			async:false,
			success:function(json){
					var arrData = json.arrData;
					
					var rowCount = document.getElementById('tblSubProjects').rows.length;

					var content='';
					var k=0;
					setInitialRows(1,'','',0,1);
					setLastRow();
					for(var i=0; i<arrData.length; i++){
						if(k==0){
							for(var j=1;j<rowCount;j++)
							{
								rowCount = document.getElementById('tblSubProjects').rows.length;
								document.getElementById('tblSubProjects').deleteRow(1);
								//document.getElementById('tblSubProjects').deleteRow(2);
									
							}
							k++;
						} 
						
						setInitialRows(arrData[i]['intSalesOrderId'],arrData[i]['strSalesOrderNo'],arrData[i]['strSubProjectDesc'],arrData[i]['dblPrice'],0)
					
					
					}//end of for
					
					setLastRow();
			}
	});
	//////////// end of load details /////////////////
		//hideWaiting();	
	
}

//--------------------------------------
function viewButtons(){
	var serialNo = $('#txtOrderNo').val();
	var year = $('#txtYear').val();
	var url 		= "noneDirect-db.php?requestType=getViewButtonsPermisions&serialNo="+serialNo+"&year="+year;
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:"",
		async:false,
		success:function(json){
				if((json.confirmPermision==1)){
					$('#butConfirm').show();
				}
				else{
					$('#butConfirm').hide();
				}
				if((json.editPermision==1)&&(json.editableFlag==1)){
					$('#butSave').show();
				}
				else{
					$('#butSave').hide();
				}
		}
	});
}

//----------------------------------	
$('#butConfirm').live('click',function(){
	if($('#txtOrderNo').val()!=''){
		window.open('../listing/rptNoneDirect.php?projectId='+$('#txtOrderNo').val()+'&year='+$('#txtYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no Order No to confirm");
	}
});
//-----------------------------------------------------

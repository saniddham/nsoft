<?php
// ini_set('display_errors',1);//echo $_SESSION['ROOT_PATH'];

///////////////////////////////
// H B G KORALA   ////////////
//////////////////////////////
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$requestType 	= $_REQUEST['requestType'];
	$programName='None-Direct';
	$programCode='P0609';
	
	include "{$backwardseperator}dataAccess/Connector.php";
	
	require_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
 	require_once $_SESSION['ROOT_PATH']."class/customerAndOperation/orders/noneDirect/cls_noneDirect_get.php";
	require_once $_SESSION['ROOT_PATH']."class/customerAndOperation/orders/noneDirect/cls_noneDirect_set.php";
	
	$objcomfunget= new cls_commonFunctions_get($db);
	$objnonedirectget= new cls_noneDirect_get($db);
	$objnonedirectset= new cls_noneDirect_set($db);
	
	
	
	$db->begin();
	if($requestType=='loadSearchCombo')
	{ 
		$response['arrCombo'] 	= $objnonedirectget->getProjects_options($company);
		echo json_encode($response);
	}
	else if($requestType=='loadCRNCombo')
	{
		$response['arrCombo'] 	= $objnonedirectget->getCRNNos_options($company);
		echo json_encode($response);
	}
	else if($requestType=='loadCustomerCombo')
	{
		$response['arrCombo'] 	= $objnonedirectget->getCustomers_options();
		echo json_encode($response);
	}
	else if($requestType=='loadCustomerAddress')
	{
		$response['arrCombo'] 	= $objnonedirectget->getCustomerAddress($_REQUEST['cboCustomer']);
		echo json_encode($response);
	}
	else if($requestType=='getViewButtonsPermisions')
	{
		 $val=$objnonedirectget->getHeader($_REQUEST['serialNo'],$_REQUEST['year']);
		$response['confirmPermision'] 	= $objcomfunget->getPermission($programCode,$userId,$val[0]['intApproveLevels'],$val[0]['intStatus']);
		$response['editableFlag'] 	= $objnonedirectget->getEditableFlag($_REQUEST['serialNo'],$_REQUEST['year']);
		$response['editPermision'] 	= $objcomfunget->getEditPermission($programCode,$userId,$val[0]['intApproveLevels'],$val[0]['intStatus']);
		echo json_encode($response);
	}
	else if($requestType=='loadCurrencyCombo')
	{
		$response['arrCombo'] 	= $objnonedirectget->getCurrency_options();
		echo json_encode($response);
	}
	else if($requestType=='loadHeader')
	{
		$response['arrData'] 	= $objnonedirectget->getHeader($_REQUEST['serialNo'],$_REQUEST['year']);
		echo json_encode($response);
	}
	else if($requestType=='loadDetails')
	{
		$response['arrData'] 	= $objnonedirectget->getDetails($_REQUEST['serialNo'],$_REQUEST['year']);
		echo json_encode($response);
	}
	
	else if($requestType=='save')
	{
		
			$rollBackFlag=0;
			$rollBackMsg =''; 
			$sqlString='';
			
			$serialNo 	 = $_REQUEST['serialNo'];
			$year 	 = $_REQUEST['year'];
			
			$crnId 	 = $_REQUEST['cboCRN'];
			$customer 	 = $_REQUEST['cboCustomer'];
			$code 	 = $_REQUEST['txtCode'];
			$name 	 = $_REQUEST['txtName'];
			$address 	 = $_REQUEST['txtAddress'];
			$date 	 = $_REQUEST['date'];
			$currency 	 = $_REQUEST['currency'];
			
			$appLevels = $objcomfunget->getApproveLevels($programName);
			$status=$appLevels+1;
			$appLevelsSO = $objcomfunget->getApproveLevels('Place Order');
			$statusSO=$appLevels+1;
			$editableFlag= $objnonedirectget->getEditableFlag($serach);
			$editPermision= $objcomfunget->getEditPermission($programCode,$userId,$appLevels,$status);
			$allreadySavedFlag = $objnonedirectget->getAllreadySavedFlag($serialNo,$year);
			////////////////////////
			if(($allreadySavedFlag==1) && ($editableFlag==0)){
				$rollBackFlag=1;
				$rollBackMsg ='This Order is already confirmed.Cant edit'; 
			}
			else if($editPermision==0){
				$rollBackFlag=1;
				$rollBackMsg ='No Permision to edit'; 
			}
			
			if($rollBackFlag==0){
			//save header 
				if($allreadySavedFlag==0){
				//$response['arrData'] = $objnonedirectset->insert_projects($crnId,$customer,$currency,$code,$name,$address,$appLevels,$userId,$date,$company,$location);
				$response['arrData'] = $objnonedirectset->insert_salesOrder_header($crnId,$customer,$currency,$code,$name,$address,$appLevelsSO,$userId,$date,$company,$location);//29-01-2013
				}
				else{
				//$response['arrData'] = $objnonedirectset->update_projects($serialNo,$year,$crnId,$customer,$currency,$code,$name,$address,$appLevels,$userId,$date,$company,$location);
				$response['arrData'] = $objnonedirectset->update_salesOrder_header($serialNo,$year,$crnId,$customer,$currency,$code,$name,$address,$appLevelsSO,$userId,$date,$company,$location);//29-01-2013
				}
				
				$serialNo=$response['arrData']['serialNo'];
				$year=$response['arrData']['year'];
				$savedHeader=$response['arrData']['result'];
				$sqlString.=$response['arrData']['q'];
				$msg.=$response['arrData']['msg'];
				if($savedHeader!=1){
					$rollBackFlag=1;
					$rollBackMsg="Header saving Error"."</br>".$msg;	
				}
		
			}//end if($rollBackFlag==0)
 
 					
			//save details 
				if($rollBackFlag==0){
					$arr 		= json_decode($_REQUEST['arr'], true);
					$toSave=0;
					$saved=0;
					$sqlString='';
					$msg='';
					
					$response['arrData'] 	= $objnonedirectset->delete_salesOrder_details($serialNo,$year);//29-01-2013
					//$response['arrData'] 	= $objnonedirectset->delete_projects_details($serialNo,$year);
					$deleted=$response['arrData']['result'];
					if($deleted!=1){
						$rollBackFlag=1;
						$rollBackMsg="Details saving Error"."</br>".$msg;	
					}
					
				if($rollBackFlag==0){
					foreach($arr as $arrVal)
					{
						$toSave++;
						$subProjId 	   = $arrVal['subProjId'];
						$subCode 	   = $arrVal['subCode'];
						$subDesc 		   = $arrVal['subDesc'];
						$subEstCost 		   = $arrVal['subEstCost'];
		
						if($rollBackFlag==0){
						$response['arrData'] = $objnonedirectset->insert_salesOrder_details($serialNo,$year,$subProjId,$code,$subCode,$subDesc,$subEstCost);//29-01-2013
						//$response['arrData'] = $objnonedirectset->insert_projects_details($serialNo,$year,$subProjId,$subCode,$subDesc,$subEstCost);
						$saved=$response['arrData']['result'];
						$sqlString.=$response['arrData']['q'];
						$msg.=$response['arrData']['msg'];
						if($saved!=1){
							$rollBackFlag=1;
							$rollBackMsg="Details saving Error"."</br>".$msg;	
						}
					
					}
				
				}
			}
		}//end if($rollBackFlag==0)
				
		//////////Response////////////////	
		if($rollBackFlag==1){
			$db->rollback();
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlString;
		}			
		else if(($rollBackFlag==0)){
			$confirmationMode = $objcomfunget->getPermission($programCode,$userId,$appLevels,$status);
			$db->commit();
			$response['type'] 		= 'pass';
			if($allreadySavedFlag==0) 
			$response['msg'] 		= 'Saved successfully.';
			else 
			$response['msg'] 		= 'Updated successfully.';
			$response['serialNo'] = $serialNo;
			$response['year'] = $year;
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->rollback();	
			$response['type'] 		= 'fail';
			//$response['msg'] 		= $db->errormsg;
			if(($msg=='') && $sqlString!='' )
			$msg="Error In Query</br>".$sqlString;
			$response['msg'] 		= $msg;
			$response['q'] 			= $sqlString;
		}
		echo  json_encode($response);
	}
	
	
	
	
	else if($requestType=='delete')
	{
			$serialNo 	 = $_REQUEST['serialNo'];
			$year 	 = $_REQUEST['year'];
	
			$response['arrData'] 	= $objnonedirectset->delete_salesOrders($serialNo,$year);//29-01-2013
			//$response['arrData'] 	= $objnonedirectset->delete_projects($serialNo,$year);
			$deleted=$response['arrData']['result'];
			$sqlString.=$response['arrData']['q'];
			$msg.=$response['arrData']['msg'];
	
			$response['arrData'] 	= $objnonedirectset->delete_salesOrder_details($serialNo,$year);//29-01-2013
			//$response['arrData'] 	= $objnonedirectset->delete_projects_details($serialNo,$year);
			$deleted=$response['arrData']['result'];
			$sqlString.=$response['arrData']['q'];
			$msg.=$response['arrData']['msg'];
				
						
			if($rollBackFlag==1){
				$db->rollback();
				$response['type'] 		= 'fail';
				$response['msg'] 		= $rollBackMsg;
				$response['q'] 			= '';
			}			
			else if(($deleted==1)){
				$db->commit();
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Deleted successfully.';
			}
			else{
				$db->rollback();
				$response['type'] 		= 'fail';
				if(($msg=='') && $sqlString!='' )
				$msg="Error In Query</br>".$sqlString;
				$response['msg'] 		= $msg;
				$response['q'] 			= $sqlString;
			}
			echo  json_encode($response);
	}
	
	
	$db->commit();
	
?>
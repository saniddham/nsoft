<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";

	$programName='Direct';
	$programCode='P0607';
	$issueApproveLevel = (int)getApproveLevel($programName);
	

 if($requestType=='getValidation')
	{
		$orderNo  = $_REQUEST['orderId'];
		$year  = $_REQUEST['year'];
		
		///////////////////////
		$sql = "SELECT
		trn_orders_direct_header.intStatus,
		trn_orders_direct_header.intApproveLevels
		FROM trn_orders_direct_header
		WHERE
		trn_orders_direct_header.intOrderId =  '$orderNo' AND
		trn_orders_direct_header.intYear =  '$year'
		";

		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$row=mysqli_fetch_array($result);
		$status = $row['intStatus'];
		$approveLevels = $row['intApproveLevels'];
		
		$confirmatonMode=loadConfirmatonMode($programCode,$status,$approveLevels,$userId);
		
		if($row['intStatus']==1){// 
			$errorFlg = 1;
			$msg ="Final confirmation of this Order is already raised"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This Order is Rejected"; 
		}
		else if($confirmatonMode==0){// 
			$errorFlg = 1;
			$msg ="No Permission to Approve"; 
		}
		else{//confirmation has been raised
			$errorFlg = 0;
		}
		////////////////////////
	
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}
//---------------------------
else if($requestType=='validateRejecton')
	{
		$orderNo  = $_REQUEST['orderId'];
		$year  = $_REQUEST['year'];
		
		///////////////////////
		$sql = "SELECT
		trn_orders_direct_header.intStatus,
		trn_orders_direct_header.intApproveLevels
		FROM trn_orders_direct_header
		WHERE
		trn_orders_direct_header.intOrderId =  '$orderNo' AND
		trn_orders_direct_header.intYear =  '$year'
		";

		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$msg =""; 
		$row=mysqli_fetch_array($result);
		
		$rejectionMode=loadRejectionMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);
		
		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation is already raised.So cant reject"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Already Rejected"; 
		}
		else if($rejectionMode==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="No Permission to reject"; 
		}
		else{//confirmation has been raised
			$errorFlg = 0;
			
		}

	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}

//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	 $sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}

//--------------------------------------------------------

?>

<?php
date_default_timezone_set('Asia/Kolkata');
//ini_set('allow_url_include',1);
// ini_set('display_errors',1);
session_start();

$backwardseperator 	= "../../../../../";
$thisFilePath 		=  $_SERVER['PHP_SELF'];
include_once $backwardseperator."dataAccess/permisionCheck2.inc";

include_once "../../../../../libraries/jqdrid/inc/jqgrid_dist.php";

$company 	= $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];

$programName='Direct';
$programCode='P0607';
$intUser  = $_SESSION["userId"];

$approveLevel = (int)getMaxApproveLevel();

$sql = "select * from(SELECT DISTINCT 
							if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected','Pending')) as Status,
							tb1.dtmCreateDate as date ,
							sys_users.strUserName as user,
							tb1.intOrderId as orderId,
							tb1.intYear as year,

							IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(trn_orders_direct_header_approvedby.dtApprovedDate),')' )
								FROM
								trn_orders_direct_header_approvedby
								Inner Join sys_users ON trn_orders_direct_header_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								trn_orders_direct_header_approvedby.intOrderId  = tb1.intOrderId AND
								trn_orders_direct_header_approvedby.intApproveLevelNo =  '1'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.intStatus>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(trn_orders_direct_header_approvedby.dtApprovedDate),')' )
								FROM
								trn_orders_direct_header_approvedby
								Inner Join sys_users ON trn_orders_direct_header_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								trn_orders_direct_header_approvedby.intOrderId  = tb1.intOrderId AND
								trn_orders_direct_header_approvedby.intApproveLevelNo =  '$i'
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.intStatus>1) AND (tb1.intStatus<=tb1.intApproveLevels) AND ((SELECT
								max(trn_orders_direct_header_approvedby.dtApprovedDate) 
								FROM
								trn_orders_direct_header_approvedby
								Inner Join sys_users ON trn_orders_direct_header_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								trn_orders_direct_header_approvedby.intOrderId  = tb1.intOrderId AND
								trn_orders_direct_header_approvedby.intApproveLevelNo =  ($i-1))<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*fase part*/
								 if($i>tb1.intApproveLevels,'-----',''))
								
								/*end IFNULL false part*/
								) as `".$approval."`, "; 
									
								}
							
							
								
							$sql .= "'View' as `View`   
						FROM
							trn_orders_direct_header as tb1 
							Inner Join trn_orders_direct_header ON tb1.intOrderId = trn_orders_direct_header.intOrderId
							Inner Join sys_users ON tb1.intCreator = sys_users.intUserId
							WHERE
							tb1.intCompanyId =  '$company' 
							)  as t where 1=1
						";
					   	     $sql;

						 
$col = array();

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;	$col=NULL;

//PO Year
$col["title"] = "Order No"; // caption of column
$col["name"] = "orderId"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$col['link']	= "../addNew/direct.php?orderId={orderId}&year={year}&savedSatatus={Status}";	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;


//PO Year
$col["title"] = "Year"; // caption of column
$col["name"] = "year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
//$col['link']	= "../addNew/projects.php?orderId={orderId}&year={year}&savedSatatus={Status}";	 
//$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;


$reportLink  = "rptDirect.php?orderId={orderId}&year={year}";
$reportLinkApprove  = "rptDirect.php?orderId={orderId}&year={year}&approveMode=1";



//User
$col["title"] = "User"; // caption of column
$col["name"] = "user"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;




//FIRST APPROVAL
$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}


//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Orders (Direct) Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'orderId'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);




$out = $jq->render("list1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Orders (Direct) Listing</title>

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />


</head>

<body>	
<form id="frmlisting" name="frmlisting" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
	<link rel="stylesheet" type="text/css" media="screen" href="../../../../../libraries/jqdrid/js/themes/smoothness/jquery-ui.custom.css"></link>	
	<link rel="stylesheet" type="text/css" media="screen" href="../../../../../libraries/jqdrid/js/jqgrid/css/ui.jqgrid.css"></link>	
	
	<script src="../../../../../libraries/jqdrid/js/jquery.min.js" type="text/javascript"></script>
	<script src="../../../../../libraries/jqdrid/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script src="../../../../../libraries/jqdrid/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="../../../../../libraries/jqdrid/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
	<script src="../../../../../libraries/javascript/script.js" type="text/javascript"></script>
	<script type="application/javascript" src="rptBom-js.js"></script>
    
    <td><table width="100%" border="0">
      <tr>
        <td>
        <div align="center" style="margin:10px">
        
			<?php echo $out?>
        </div>
        </td>
      </tr>

    </table></td>
    </tr>


</form>
</body>
</html>
<?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(trn_orders_direct_header.intApproveLevels) AS appLevel
			FROM trn_orders_direct_header";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
//------------------------------function load User Department---------------------
function getUserDepartment($intUser){
	global $db;

	$sqlp = "SELECT
			sys_users.intDepartmentId
			FROM
			sys_users
			WHERE
			sys_users.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['intDepartmentId'];

}
?>


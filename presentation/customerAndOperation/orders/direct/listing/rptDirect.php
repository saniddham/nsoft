<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////

session_start();
$backwardseperator = "../../../../../";
$companyId = $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";

$serialNo = $_REQUEST['orderId'];
$year = $_REQUEST['year'];

$approveMode = $_REQUEST['approveMode'];

/*$projectId ='100001';
$subProjectId=1;
$approveMode = 1;
*/
$programName='Direct';
$programCode='P0607';
$issueApproveLevel = (int)getApproveLevel($programName);

/*$gatePassNo = '100000';
$year = '2012';
$approveMode=1;
*/
    $sql = "SELECT
trn_orders_direct_header.intCurrencyId,
trn_orders_direct_header.strAddress,
trn_orders_direct_header.intStatus,
trn_orders_direct_header.intApproveLevels,
trn_orders_direct_header.intCreator,
sys_users.strUserName,
trn_orders_direct_header.dtmCreateDate,
trn_orders_direct_header.intLocationId,
trn_orders_direct_header.intCompanyId, 
mst_financecurrency.strCode as currency 
FROM
trn_orders_direct_header
Inner Join sys_users ON trn_orders_direct_header.intCreator = sys_users.intUserId 
Inner Join mst_financecurrency ON trn_orders_direct_header.intCurrencyId = mst_financecurrency.intId 
WHERE 
trn_orders_direct_header.intOrderId =  '$serialNo'  AND 
trn_orders_direct_header.intYear =  '$year'
";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$currency = $row['currency'];
					$user = $row['strUserName']; 
					$date=substr($row['dtmCreateDate'],0,10);
					$locationId = $row['intLocationId'];//this locationId use in report header(reportHeader.php)--------------------
					$intStatus = $row['intStatus'];
					$savedLevels = $row['intApproveLevels'];
					$intCreateCompanyId		= $row['intCompanyId'];
					$intCreateUserId		= $row['intCreator'];
					$strAddress		= $row['strAddress'];
					
				 }
				 
$confirmationMode=loadConfirmatonMode($programCode,$intStatus,$savedLevels,$intUser);
$rejectionMode=loadRejectionMode($programCode,$intStatus,$savedLevels,$intUser);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sales Order Report - Direct</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="rptDirect-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:277px;
	top:169px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="../../../../../images/pending.png"  /></div>
<?php
}
?>
<form id="frmReport" name="frmReport" method="post" action="rptNoneDirect.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>PROJECTS REPORT</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
    <?php
	if($intStatus>1)
	{
	?>
    <?php if($approveMode==1){
			if($confirmationMode==1) { 
			?>
			<img src="../../../../../images/approve.png" align="middle" class="mouseover noPrint" id="imgApprove" />
			<?php
			}
			if($rejectionMode==1) { 
			?>
            <img src="../../../../../images/reject.png" align="middle" class="mouseover noPrint" id="imgReject" />
                <?php
                }
            }
	}
	?>
    </td>
  </tr>
  <tr>
  <?php
 	if($intStatus==1)
	{
	?>
   <td colspan="9" class="APPROVE" >CONFIRMED</td>
   <?PHP
	}
	else if($intStatus==0)
	{
   ?>
   <td colspan="9" class="APPROVE" style="color:#F00">REJECTED</td>
   <?php
	}
	else
	{
   ?>
   <td width="10%" colspan="9" class="APPROVE">PENDING</td>
   <?php
	}
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="11%"><span class="normalfnt"><strong>Order No</strong></span></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    
    <td width="26%"><span class="normalfnt"><?php echo $serialNo."/".$year; ?></span></td>
    <td width="8%" class="normalfnt"><strong>Currency</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="28%"><span class="normalfnt"><?php echo $currency; ?></span></td>
    <td width="1%"><div id="divProjectNo" style="display:none"><?php echo $serialNo ?></div><div id="divYear" style="display:none"><?php echo $year ?></div></td>
  <td width="3%"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Created By</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $user  ?></span></td>
    <td><span class="normalfnt"><strong>Address</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $strAddress ?></span></td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Date</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $date  ?></span></td>
<td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="gridHeader">
              <td width="15%" >Main Category</td>
              <td width="16%" >Sub Category</td>
              <td width="35%" >Item</td>
              <td width="10%" >Qty</td>
              <td width="12%" >Buying Price</td>
              <td width="12%" >Selling Price</td>
              </tr>
              <?php 
	  	   $sql1 = "SELECT
mst_maincategory.strName as mainCat, 
mst_subcategory.strName as subCat, 
mst_item.strName as item,
trn_orders_direct_details.dblQty,
trn_orders_direct_details.dblSellingPrice,
trn_orders_direct_details.dblBuyingPrice
FROM
trn_orders_direct_details
Inner Join mst_item ON trn_orders_direct_details.intItemId = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
WHERE
trn_orders_direct_details.intOrderId =  '$serialNo' AND
trn_orders_direct_details.intYear =  '$year'
ORDER BY
mst_item.strName ASC
";
			$result1 = $db->RunQuery($sql1);
			$totQty=0;
			$totAmmount=0;
			while($row=mysqli_fetch_array($result1))
			{
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" >&nbsp;<?php echo $row['mainCat'];?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['subCat'];?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['item'];?>&nbsp;</td>
              <td class="normalfntRight" ><?php echo $row['dblQty'] ?></td>
              <td class="normalfntRight" ><?php echo $row['dblBuyingPrice'] ?></td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblSellingPrice'] ?>&nbsp;</td>
              </tr>
      <?php 
			$totamountBuy+=$row['dblBuyingPrice']*$row['dblQty'];
			$totamountSel+=$row['dblSellingPrice']*$row['dblQty'];
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php //echo $totamountBuy ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php //echo $totamountSel ?>&nbsp;</td>
              </tr>
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<?php  
 	if($intStatus!=0)
	{

				for($i=1; $i<=$savedLevels; $i++)
				{
					   $sqlc = "SELECT
							trn_orders_direct_header_approvedby.intApproveUser,
							trn_orders_direct_header_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName, 
							trn_orders_direct_header_approvedby.intApproveLevelNo
							FROM
							trn_orders_direct_header_approvedby
							Inner Join sys_users ON trn_orders_direct_header_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							trn_orders_direct_header_approvedby.intOrderId =  '$serialNo' AND 
							trn_orders_direct_header_approvedby.intYear =  '$year' AND
							trn_orders_direct_header_approvedby.intApproveLevelNo =  '$i'  order by intApproveLevelNo asc
";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
						if($i==1)
						$desc="1st ";
						else if($i==2)
						$desc="2nd ";
						else if($i==3)
						$desc="3rd ";
						else
						$desc=$i."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
				?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
            </tr>
<?php
			}
	}
	else{
					 $sqlc = "SELECT
							trn_orders_direct_header_approvedby.intApproveUser,
							trn_orders_direct_header_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							trn_orders_direct_header_approvedby.intApproveLevelNo
							FROM
							trn_orders_direct_header_approvedby
							Inner Join sys_users ON trn_orders_direct_header_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							trn_orders_direct_header_approvedby.intOrderId =  '$serialNo' AND  
							trn_orders_direct_header_approvedby.intYear =  '$year' AND
							trn_orders_direct_header_approvedby.intApproveLevelNo =  '0'";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Rejected By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
	}
?>

<tr height="120">
  <td align="center" class="normalfntMid"></td>
</tr>

<tr height="90" >
<?Php 
$field1="Order No";
$field2="Year";
	$url  = "{$backwardseperator}presentation/sendToApproval.php";		// * file name
	$url .= "?status=$intStatus";										// * set recent status
	$url .= "&approveLevels=$savedLevels";								// * set approve levels in order header
	$url .= "&programCode=$programCode";								// * program code (ex:P10001)
	$url .= "&companyId=$company";									// * created company id
	$url .= "&programName=SALES ORDER (DIRECT)";									// * program name (ex:Purchase Order)
	$url .= "&val1=$serialNo";	
	$url .= "&val2=$year";	
	$url .= "&fieldName1=$field1";	
	$url .= "&fieldName2=$field2";	
	$url .= "&createUserId=$intCreateUserId";							// * doc year
	$url .= "&link=".urlencode(base64_encode($mainPath."presentation/customerAndOperation/orders/direct/listing/rptDirect.php?orderId=$serialNo&approveMode=1"));
?>
  <td align="center" style="vertical-align:top" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:150px;border:none"  ></iframe></td>
</tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//-------------------------------------------------------------------------
?>	

<?php 

	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	
	$programName='Direct';
	$programCode='P0607';
	
	//ini_set('display_errors',1);
	include "{$backwardseperator}dataAccess/Connector.php";
	include "{$backwardseperator}libraries/mail/mail.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_mail.php";
	
	$objMail = new cls_create_mail($db);
	/////////// parameters /////////////////////////////
	//$requestType 			= $_REQUEST['requestType'];
	
		$orderNo  = $_REQUEST['orderId'];
		$year  = $_REQUEST['year'];
	
	if($_REQUEST['status']=='approve')
	{
		$sql = "UPDATE `trn_orders_direct_header` SET `intStatus`=intStatus-1 WHERE (`intOrderId`='$orderNo') AND (`intYear`='$year') and intStatus not in(0,1)";
		$result = $db->RunQuery($sql);
		
		if($result){
			   $sql = "	SELECT
						trn_orders_direct_header.intStatus,
						trn_orders_direct_header.intApproveLevels 
						FROM trn_orders_direct_header  
						WHERE (trn_orders_direct_header.`intOrderId`='$orderNo') 
						AND (trn_orders_direct_header.`intYear`='$year')
						";
			$result2 = $db->RunQuery($sql);
			$row2=mysqli_fetch_array($result2);
			$status = $row2['intStatus'];
			$savedLevels = $row2['intApproveLevels'];
			
			$approveLevel=$savedLevels+1-$status;
			 /////////////////// insert approval table ////////////////////
			   $sql = "INSERT INTO `trn_orders_direct_header_approvedby` (`intOrderId`,`intYear`,`intApproveLevelNo`,`intApproveUser`,`dtApprovedDate`) VALUES ('$orderNo','$year','$approveLevel','$userId',now())";
			 $result = $db->RunQuery($sql);
			 //////////////////////////////////////////////////////////////
			if($status ==1)
			{
				sendFinlConfirmedEmailToUser($orderNo,$year,$objMail);
				//generateAutoMRN($orderNo,$year); do this laterrrrrrrrrrrrrrr
			}
		}
		
		
	}
	else if($_REQUEST['status']=='reject')
	{
		
		$sql = "UPDATE `trn_orders_direct_header` SET `intStatus`=0 WHERE (`intOrderId`='$orderNo') AND (`intYear`='$year') ";
		$result = $db->RunQuery($sql);
		
		$sql = "DELETE FROM `trn_orders_direct_header_approvedby` WHERE (`intOrderId`='$orderNo') AND (`intYear`='$year') ";
		$result2 = $db->RunQuery($sql);
		
		$sqlI = "INSERT INTO `trn_orders_direct_header_approvedby` (`intOrderId`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate) 
			VALUES ('$orderNo','$year','0','$userId',now())";
		$resultI = $db->RunQuery($sqlI);
		
				 $sql = "	SELECT
							trn_orders_direct_header.intStatus,
							trn_orders_direct_header.intApproveLevels 
							FROM trn_orders_direct_header  
							WHERE
							trn_orders_direct_header.intOrderId =  '$orderNo' AND
							trn_orders_direct_header.intYear =  '$year'
							";
				$result2 = $db->RunQuery($sql);
				$row2=mysqli_fetch_array($result2);
				$status = $row2['intStatus'];
			if($status ==0)
			{
				sendRejecedEmailToUser($orderNo,$year,$objMail);
			}
		
		
	}
	

//--------------------------------------------------------
function sendFinlConfirmedEmailToUser($serialNo,$year,$objMail){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail  
			FROM 
			trn_orders_direct_header
			Inner Join sys_users ON trn_orders_direct_header.intCreator = sys_users.intUserId 
			WHERE (trn_orders_direct_header.`intOrderId`='$serialNo') AND (trn_orders_direct_header.`intYear`='$year')";

				
		 $result = $db->RunQuery($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
				$header="FINAL APPROVAL RAISED FOR BOM (Order- '".$serialNo."'/ Year- '".$year."')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST['no'] 		= $serialNo;
			$_REQUEST['year'] 		= $year;
			$objMail->send_Response_Mail('mail_final_approved_template.php',$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
function sendRejecedEmailToUser($serialNo,$year,$objMail){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail  
			FROM 
			trn_orders_direct_header
			Inner Join sys_users ON trn_orders_direct_header.intCreator = sys_users.intUserId 
			WHERE (trn_orders_direct_header.`intOrderId`='$serialNo') AND (trn_orders_direct_header.`intYear`='$year')";

				
		 $result = $db->RunQuery($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="REJECTED THE BOM (Project- '".$serialNo."'/ Year- '".$year."')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST['no'] 		= $serialNo;
			$_REQUEST['year'] 		= $year;
			$_REQUEST['customerPO'] 		= $customerPO;
			$objMail->send_Response_Mail('mail_rejected_template.php',$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
function generateAutoMRN($orderNo,$year){
	global $db;
				$sql1 = "SELECT
						trn_orders_direct_header.intCurrencyId,
						trn_orders_direct_header.intCreator,
						trn_orders_direct_header.dtmCreateDate
						trn_orders_direct_header.intLocationId,
						trn_orders_direct_header.intCompanyId,
						trn_orders_direct_details.intItemId,
						trn_orders_direct_details.dblQty,
						trn_orders_direct_details.dblSellingPrice,
						trn_orders_direct_details.dblBuyingPrice
						FROM
						trn_orders_direct_header
						Inner Join trn_orders_direct_details ON trn_orders_direct_header.intOrderId = trn_orders_direct_details.intOrderId AND trn_orders_direct_header.intYear = trn_orders_direct_details.intYear
						WHERE
						trn_orders_direct_header.intOrderId =  '$orderNo' AND
						trn_orders_direct_header.intYear =  '$year'";
				$result1 = $db->RunQuery($sql1);
				$k=0;
				while($row=mysqli_fetch_array($result1))
				{ 
					$item=$row['intItemId'];
					$Qty=round($row['dblQty'],4);
					$price=$row['dblUnitPrice'];
					$date=$row['dtmCreateDate'];
					$user=$row['dtmCreateDate'];
					$location=$row['intLocationId'];
					$company=$row['intCompanyId'];
				
				 //--------------
					if($k==0){//1.insert in to mrn header
					$program='MRN';
					$ApproveLevels = (int)getApproveLevel($program);
					//$status = $ApproveLevels+1;
					$status = 1;
					$serialNo 	= getNextSerialNo();
					$year = date('Y');
					$userDepartment = getUserDepartment();
					$remarks='Auto Generated MRN for Direct Sales Order';
					
					$sqlI = "INSERT INTO `ware_mrnheader` (`intMrnNo`,`intMrnYear`,`intDepartment`,intStatus,intApproveLevels,datdate,strRemarks,dtmCreateDate,intUser,intCompanyId) 
						VALUES ('$serialNo','$year','$userDepartment','$status','$ApproveLevels','$date','$remarks','$date','$user','$location')";
					$resultI = $db->RunQuery($sqlI);
					}
				//----------------------	
					//2.insert in to mrn approved by table?????????????
			
			    //--------------	
					//3.insert in to mrn  details
					$sqlI = "INSERT INTO `ware_mrndetails` (`intMrnNo`,`intMrnYear`,`intOrderNo`,intOrderYear,strStyleNo,intItemId,dblQty,dblIssudQty,dblMRNClearQty) 
						VALUES ('$company','$location','$grnNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','$Qty','GRN','$userId',now())";
					$resultI = $db->RunQuery($sqlI);
					
					
					$k++;
				}
	
}
?>







<?php
///////////////////////////////
// H B G KORALA   ////////////
//////////////////////////////
session_start();
$backwardseperator = "../../../../../";
$thisFilePath =  $_SERVER['PHP_SELF'];
$company 	= $_SESSION['headCompanyId'];

include  "{$backwardseperator}dataAccess/permisionCheck.inc";
$serialNo = $_REQUEST['orderId'];
$year = $_REQUEST['year'];

//require_once $_SESSION['ROOT_PATH']."class/cls_permisions.php";
require_once "{$backwardseperator}class/cls_permisions.php";

//---------check Permission to save recive qty more than PO qty.------------
$objpermisionget= new cls_permisions($db);
$permisionEnableCR 	= $objpermisionget->getPermisionCompanyEnableCR($company);
//------------------------------------------------------------------------
//echo $_SESSION['ROOT_PATH'];
?>
<script type="application/javascript" >
var cId = '<?php echo $cId ?>';
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sales Orders - Direct</title>

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="direct-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" href="../../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../../libraries/validate/template.css" type="text/css">

</head>

<body onLoad="functionList('<?php echo $serialNo; ?>','<?php echo $year; ?>');">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<script src="../../../../../libraries/validate//jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<form id="frmOrders" name="frmOrders" method="post" action="direct-db.php"  autocomplete="off">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>

<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Sales Orders - Direct</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0" align="center">
      <tr>
        <td width="100%"><table width="100%" border="0" >   
        
<tr>
  <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableBorder_allRound">
    <tr>
      <td width="1%" height="22" class="normalfnt"></td>
      <td width="13%" height="22" class="normalfnt">Order No</td>
      <td width="28%"><input name="txtOrderNo" type="text" disabled="disabled"id="txtOrderNo" style="width:60px"value="<?php echo $orderNo ?>" class="normalfnt" /><input name="txtYear" type="text" disabled="disabled"id="txtYear" style="width:40px" value="<?php echo $orderYear ?>"  class="normalfnt"/></td>
      <td width="1%">&nbsp;</td>
      <td width="36%">&nbsp;</td>
      <td width="5%" class="normalfnt">Date</td>
      <td width="16%"><input name="dtDate" type="text" value="<?php 
			if($dtDate!='')
				echo $dtDate;
			 else
				echo date("Y-m-d"); ?>" class="txtbox" id="dtDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
      </tr>
    </table></td>
</tr>   
             <tr>
            <td height="47" ><table width="100%" border="0" class="">
                
                             
<tr <?php if($permisionEnableCR!=1){?>style="display:none"  <?php } ?>
>
                <td width="3" class="normalfnt">&nbsp;</td>
                <td width="116" class="normalfnt">CRN No</td>
              <td><input name="cboCRN" type="text" class="validate[required,maxSize[10]] text-input" id="cboCRN" style="width:140px" /></td>
              
                          <td class="normalfnt">Currency&nbsp;<span class="compulsoryRed">*</span></td>
                          <td><select name="cboCurrency" class="txtbox validate[required]" id="cboCurrency" style="width:80px">
                            <option value=""></option>
                          </select></td>
              </tr>    
                        <tr>
                          <td class="normalfnt">&nbsp;</td>
                          <td class="normalfnt">Customer&nbsp;<span class="compulsoryRed">*</span></td>
                          <td width="452"><select name="cboCustomer" class="txtbox validate[required]" id="cboCustomer" style="width:300px">
                            <option value=""></option>
                          </select></td>
                          <td width="77" class="normalfnt">&nbsp;</td>
                          <td width="236">&nbsp;</td>
                        </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Address</td>
                <td colspan="3"><textarea name="txtAddress" style="width:360px"  rows="2" class="txtbox" id="txtAddress"></textarea></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Active</td>
                <td><input type="checkbox" name="chkActive" id="chkActive" checked="checked"/></td>
                <td class="normalfnt">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td colspan="4">&nbsp;</td>
              </tr>
            <tr>
                <td colspan="5"><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%"  class="grid" id="tblOrderItems" >
            <tr class="gridHeader">
              <td width="25" height="22" >Del</td>
              <td width="151" >Main Category</td>
              <td width="145" >Sub Category </td>
              <td width="271" >Item</td>
              <td width="37" >UOM</td>
              <td width="69" >Qty</td>
              <td width="86" >Selling Price</td>
              <td width="80" >Buying Price</td>
              </tr>
            <tr class="normalfnt" >
              <td align="center" bgcolor="#FFFFFF"><img src="../../../../../images/del.png" width="15" height="15" class="delImg" /></td>
              <td bgcolor="#FFFFFF" align="center"  style="width:40px" class="proID"><select name="cboMainCategory" style="width:140px" id="cboMainCategory" class="mainCat">
                <option value=""></option>
              </select></td>
              <td bgcolor="#FFFFFF"  style="width:70px"><select name="cboSubCategory" style="width:140px" id="cboSubCategory" class="subCat">
                <option value=""></option>
              </select></td>
              <td bgcolor="#FFFFFF"><select name="cboItem" style="width:260px" id="cboItem" class="item">
                  <option value=""></option>
                </select>
             </td>
              <td bgcolor="#FFFFFF" align="center" class="uom">&nbsp;</td>
              <td bgcolor="#FFFFFF" align="center"><input name="txtEstCost3" type="text" id="txtEstCost3" style="width:80px; text-align:right" class="validate[required,min[0],custom[number]]  qty"/></td>
              <td bgcolor="#FFFFFF" align="center"><input name="txtEstCost2" type="text" id="txtEstCost2" style="width:80px; text-align:right" class="validate[required,min[0],custom[number]]  sellingPrice"/></td>
              <td bgcolor="#FFFFFF" align="center"><input name="txtEstCost" type="text" id="txtEstCost" style="width:80px; text-align:right" class="validate[required,min[0],custom[number]]  buyingPrice"/></td>
              </tr>

          <tr class="dataRow">
            <td colspan="9" align="left"  bgcolor="#FFFFFF" ><img src="../../../../../images/Tadd.jpg" name="butInsertRow" width="78" height="24" class="mouseover" id="butInsertRow" /></td>
            </tr>
          </table>
        </div></td>
              </tr>              </table></td>
            </tr>
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img style="display:none" class="mouseover" src="../../../../../images/Tconfirm.jpg" width="92" height="24" id="butConfirm" name="butConfirm" /><img style="display:none" border="0" src="../../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../../main.php"><img  src="../../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
</form>
</body>
</html>

<?php
 // ini_set('display_errors',1);//echo $_SESSION['ROOT_PATH'];

///////////////////////////////
// H B G KORALA   ////////////
//////////////////////////////
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$requestType 	= $_REQUEST['requestType'];
	$programName='Direct';
	$programCode='P0607';
	
	include "{$backwardseperator}dataAccess/Connector.php";
	
	require_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
 	require_once $_SESSION['ROOT_PATH']."class/customerAndOperation/orders/direct/cls_direct_get.php";
	require_once $_SESSION['ROOT_PATH']."class/customerAndOperation/orders/direct/cls_direct_set.php";
	
	$objcomfunget= new cls_commonFunctions_get($db);
	$objdirectget= new cls_direct_get($db);
	$objdirectset= new cls_direct_set($db);
	
	
	
	$db->begin();
	if($requestType=='loadSearchCombo')
	{ 
		$response['arrCombo'] 	= $objdirectget->getProjects_options($company);
		echo json_encode($response);
	}
	else if($requestType=='loadCRNCombo')
	{
		$response['arrCombo'] 	= $objdirectget->getCRNNos_options($company);
		echo json_encode($response);
	}
	else if($requestType=='loadCustomerCombo')
	{
		$response['arrCombo'] 	= $objdirectget->getCustomers_options();
		echo json_encode($response);
	}
	else if($requestType=='loadCurrencyCombo')
	{
		$response['arrCombo'] 	= $objdirectget->getCurrency_options();
		echo json_encode($response);
	}
	else if($requestType=='getViewButtonsPermisions')
	{
		 $val=$objdirectget->getHeader($_REQUEST['serialNo'],$_REQUEST['year']);
		$response['confirmPermision'] 	= $objcomfunget->getPermission($programCode,$userId,$val[0]['intApproveLevels'],$val[0]['intStatus']);
		$response['editableFlag'] 	= $objdirectget->getEditableFlag($_REQUEST['serialNo'],$_REQUEST['year']);
		$response['editPermision'] 	= $objcomfunget->getEditPermission($programCode,$userId,$val[0]['intApproveLevels'],$val[0]['intStatus']);
		echo json_encode($response);
	}
	else if($requestType=='loadMainCatCombo')
	{
		$response['arrCombo']  	= $objdirectget->getMainCat_options();
		echo json_encode($response);
	}
	else if($requestType=='loadSubCategoriesCombo')
	{
		$response['arrCombo'] 	= $objdirectget->getSubCategories_options($_REQUEST['mainCat']);
		echo json_encode($response);
	}
	else if($requestType=='loadItemCombo')
	{
		$response['arrCombo'] 	= $objdirectget->getItems_options($_REQUEST['mainCat'],$_REQUEST['subCat']);
		echo json_encode($response);
	}
	else if($requestType=='loadItemDetails')
	{
		$response	= $objdirectget->getItem_details($_REQUEST['itemId']);
		echo json_encode($response);
	}
	else if($requestType=='loadHeader')
	{
		$response['arrData'] 	= $objdirectget->getHeader($_REQUEST['serialNo'],$_REQUEST['year']);
		echo json_encode($response);
	}
	else if($requestType=='loadDetails')
	{
		$response['arrData'] 	= $objdirectget->getDetails($_REQUEST['serialNo'],$_REQUEST['year']);
		echo json_encode($response);
	}
	
	else if($requestType=='save')
	{
		
			$rollBackFlag=0;
			$rollBackMsg =''; 
			$sqlString='';
			
			$serialNo 	 = $_REQUEST['serialNo'];
			$year 	 = $_REQUEST['year'];
			
			$crnId 	 = $_REQUEST['cboCRN'];
			$customer 	 = $_REQUEST['cboCustomer'];
			$currency 	 = $_REQUEST['currency'];
			$address 	 = $_REQUEST['txtAddress'];
			$date 	 = $_REQUEST['date'];
			
			$appLevels = $objcomfunget->getApproveLevels($programName);
			$status=$appLevels+1;
			$editableFlag= $objdirectget->getEditableFlag($serach);
			$editPermision= $objcomfunget->getEditPermission($programCode,$userId,$appLevels,$status);
			$allreadySavedFlag = $objdirectget->getAllreadySavedFlag($serialNo,$year);
			////////////////////////
			if(($allreadySavedFlag==1) && ($editableFlag==0)){
				$rollBackFlag=1;
				$rollBackMsg ='This Order is already confirmed.Cant edit'; 
			}
			else if($editPermision==0){
				$rollBackFlag=1;
				$rollBackMsg ='No Permision to edit'; 
			}
			
			if($rollBackFlag==0){
			//save header 
				if($allreadySavedFlag==0){
				$response['arrData'] = $objdirectset->insert_orders($crnId,$customer,$currency,$address,$appLevels,$userId,$date,$company,$location);
				}
				else{
				$response['arrData'] = $objdirectset->update_orders($serialNo,$year,$crnId,$customer,$currency,$address,$appLevels,$userId,$date,$company,$location);
				}
				
				$serialNo=$response['arrData']['serialNo'];
				$year=$response['arrData']['year'];
				$savedHeader=$response['arrData']['result'];
				$sqlString.=$response['arrData']['q'];
				$msg.=$response['arrData']['msg'];
				if($savedHeader!=1){
					$rollBackFlag=1;
					$rollBackMsg="Header saving Error"."</br>".$msg;	
				}
		
			}//end if($rollBackFlag==0)
 
 					
			//save details 
				if($rollBackFlag==0){
					$arr 		= json_decode($_REQUEST['arr'], true);
					$toSave=0;
					$saved=0;
					$sqlString='';
					$msg='';
					
					$response['arrData'] 	= $objdirectset->delete_orders_details($serialNo,$year);
					$deleted=$response['arrData']['result'];
					if($deleted!=1){
						$rollBackFlag=1;
						$rollBackMsg="Details saving Error"."</br>".$msg;	
					}
					
				if($rollBackFlag==0){
					foreach($arr as $arrVal)
					{
						$toSave++;
						$itemId 	   = $arrVal['itemId'];
						$qty 	   = $arrVal['qty'];
						$sellingPrice 		   = $arrVal['sellingPrice'];
						$buyingPrice 		   = $arrVal['buyingPrice'];
		
						if($rollBackFlag==0){
						$response['arrData'] = $objdirectset->insert_orders_details($serialNo,$year,$itemId,$qty,$sellingPrice,$buyingPrice);
						
						$saved=$response['arrData']['result'];
						$sqlString.=$response['arrData']['q'];
						$msg.=$response['arrData']['msg'];
						if($saved!=1){
							$rollBackFlag=1;
							$rollBackMsg="Details saving Error"."</br>".$msg;	
						}
					
					}
				
				}
			}
		}//end if($rollBackFlag==0)
				
		//////////Response////////////////	
		if($rollBackFlag==1){
			$db->rollback();
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlString;
		}			
		else if(($rollBackFlag==0)){
			$confirmationMode = $objcomfunget->getPermission($programCode,$userId,$appLevels,$status);
			$db->commit();
			$response['type'] 		= 'pass';
			if($allreadySavedFlag==0) 
			$response['msg'] 		= 'Saved successfully.';
			else 
			$response['msg'] 		= 'Updated successfully.';
			$response['serialNo'] = $serialNo;
			$response['year'] = $year;
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->rollback();	
			$response['type'] 		= 'fail';
			//$response['msg'] 		= $db->errormsg;
			if(($msg=='') && $sqlString!='' )
			$msg="Error In Query</br>".$sqlString;
			$response['msg'] 		= $msg;
			$response['q'] 			= $sqlString;
		}
		echo  json_encode($response);
	}
	
	
	
	
	else if($requestType=='delete')
	{
			$serialNo 	 = $_REQUEST['serialNo'];
			$year 	 = $_REQUEST['year'];
	
			$response['arrData'] 	= $objdirectset->delete_orders($serialNo,$year);
			$deleted=$response['arrData']['result'];
			$sqlString.=$response['arrData']['q'];
			$msg.=$response['arrData']['msg'];
			
			$response['arrData'] 	= $objdirectset->delete_orders_details($serialNo,$year);
			$deleted=$response['arrData']['result'];
			$sqlString.=$response['arrData']['q'];
			$msg.=$response['arrData']['msg'];
				
						
			if($rollBackFlag==1){
				$db->rollback();
				$response['type'] 		= 'fail';
				$response['msg'] 		= $rollBackMsg;
				$response['q'] 			= '';
			}			
			else if(($deleted==1)){
				$db->commit();
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Deleted successfully.';
			}
			else{
				$db->rollback();
				$response['type'] 		= 'fail';
				if(($msg=='') && $sqlString!='' )
				$msg="Error In Query</br>".$sqlString;
				$response['msg'] 		= $msg;
				$response['q'] 			= $sqlString;
			}
			echo  json_encode($response);
	}
	
	
	$db->commit();
	
?>
function functionList(serialNo,year)
{
//	loadSearchCombo();
	//loadCRNCombo();
	loadCustomerCombo();
	loadCurrencyCombo();
	loadMainCatCombo();
	
	if(serialNo!='')
	{
		$('#frmOrders #txtOrderNo').val(serialNo);
		$('#frmOrders #txtYear').val(year);
		loadOrderDetails();
		
		//$('#frmOrders #cboSearch').change();
	}
	viewButtons();
}		
$(document).ready(function() {
  		$("#frmOrders").validationEngine();
		$('#frmOrders #txtCode').focus();
  //permision for add 
  
  if(intAddx)
  {
 	$('#frmOrders #butNew').show();
	$('#frmOrders #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmOrders #butSave').show();
	//$('#frmOrders #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmOrders #butDelete').show();
	//$('#frmOrders #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	//$('#frmOrders #cboSearch').removeAttr('disabled');
  }
  
   $('#frmOrders #cboSalesType').live('change',function(){
	//	window.location.href = "non_direct_sales.php";
	if($('#frmOrders #cboSalesType').val()=='')
		window.location.href = "projects.php";
	else if($('#frmOrders #cboSalesType').val()==1)
		window.location.href = "direct_sales.php";
	else if($('#frmOrders #cboSalesType').val()==2)
		window.location.href = "non_direct_sales.php";
   });
  
 //------------------------------------------------------- 
		$('.delImg').live('click',function(){
			if($(this).parent().parent().parent().find('tr').length>3)
			$(this).parent().parent().remove();
		});
//------------------------------------------------------------  
		$(".mainCat").live('change',function(){
			loadSubCategories(this);
		});
		//--------
		$(".subCat").live('change',function(){
			loadItems(this);
		});
		//--------
		$(".item").live('change',function(){
			loadItemDetails(this);
		});
 //------------------------------------------------------- 
$('#butInsertRow').live('click',function(){
		
	var rowCount = document.getElementById('tblOrderItems').rows.length;
	document.getElementById('tblOrderItems').insertRow(rowCount-1);
	rowCount = document.getElementById('tblOrderItems').rows.length;
	document.getElementById('tblOrderItems').rows[rowCount-2].innerHTML = document.getElementById('tblOrderItems').rows[rowCount-3].innerHTML;
	document.getElementById('tblOrderItems').rows[rowCount-2].className="normalfnt";
	document.getElementById('tblOrderItems').rows[rowCount-2].cells[4].innerHTML='';
	});
 //-------------------------------------------------------
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

  ///save button click event
  $('#frmOrders #butSave').click(function(){
	//$('#frmOrders').submit();
	var requestType = '';
	if ($('#frmOrders').validationEngine('validate'))   
    { 
	showWaiting();

		var data = "requestType=save";
			data+="&serialNo=" +	$('#txtOrderNo').val();
			data+="&year=" +	$('#txtYear').val();
			data+="&cboCRN=" +	$('#cboCRN').val();
			data+="&date=" +	$('#dtDate').val();
			data+="&cboCustomer=" +	$('#cboCustomer').val();
			data+="&txtAddress="	+	URLEncode($('#txtAddress').val());
			data+="&currency="	+	URLEncode($('#cboCurrency').val());
			data+="&chkActive="	+	$('#chkActive').val();
			
			//////////////////// saving main grid part //////////////////////////////////////////
			//var rowCount = $('#tblMain >tbody >tr').length;
			var rowCount = document.getElementById('tblOrderItems').rows.length;
			var row = 0;
			
			var arr="[";
			var salesIds='';
			$('#tblOrderItems >tbody >tr').not(':last').not(':first').each(function(){
					 
					 arr += "{";
					 
					var itemId 		= 	$(this).find('.item').val();
					var qty	 	= 	$(this).find('.qty').val();
					var sellingPrice	 	= 	$(this).find('.sellingPrice').val();
					var buyingPrice	 	= 	$(this).find('.buyingPrice').val();
					
					
					if((itemId=='') || (qty=='') || (sellingPrice=='') || (buyingPrice==''))
					{
						hideWaiting();
						alert('Please Enter Data');
						
						return;
						//return 0;
					}/// end if
					//alert(1);
					arr += '"itemId":"'+	 itemId +'",' 		;
					arr += '"qty":"'+   	qty +'",' 		;
					arr += '"sellingPrice":"'+	 sellingPrice +'",' 		;
					arr += '"buyingPrice":"'+ 	buyingPrice +'"' 		;
					arr +=  '},';
			});
			arr = arr.substr(0,arr.length-1);
			
			arr += " ]";
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = "direct-db.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			data:data,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmOrders #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						/*window.location.href = "projects.php";*/
						//loadSearchCombo();
						var t=setTimeout("alertx()",1000);
						$('#frmOrders #txtOrderNo').val(json.serialNo);
						$('#frmOrders #txtYear').val(json.year);
						viewButtons();
						
						if(json.confirmationMode==1){
							$('#butConfirm').show();
						}
						else{
							$('#butConfirm').hide();
						}
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmOrders #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
		hideWaiting();	
	}
   });
   
   /////////////////////////////////////////////////////
   //// load project details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmOrders #cboSearch').click(function(){
	   $('#frmOrders').validationEngine('hide');
   });
   
    $('#frmOrders #cboSearch').change(function(){
		loadOrderDetails();
	});
	
    $('#frmOrders #cboCRN').change(function(){
		if($('#frmOrders #cboCRN').val()!=''){
		//$('#frmOrders #cboCustomer').attr('disabled',true);
		}
		else{
		//$('#frmOrders #cboCustomer').attr('disabled',false);
		}
		//loadCustomer();
	});
	
	$('#frmOrders #butNew').click(function(){
		window.location.href = "projects.php";
		$('#frmOrders').get(0).reset();
		loadSearchCombo();
		$('#frmOrders #txtCode').focus();
	});
    $('#frmOrders #butDelete').click(function(){
		if($('#frmOrders #cboSearch').val()=='')
		{
			$('#frmOrders #butDelete').validationEngine('showPrompt', 'Please select project.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmOrders #txtOrderNo').val()+'/'+$('#frmOrders #txtYear').val()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										showWaiting();
										var url = "direct-db.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&serialNo='+$('#frmOrders #txtOrderNo').val()+'&year'+$('#frmOrders #txtYear').val(),
											async:false,
											success:function(json){
												
												$('#frmOrders #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													window.location.href = "projects.php";
													//loadSearchCombo();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
										hideWaiting();	
									}
				}
		 	});
			
		}
	});
});



//----------------------------------------------------
function loadSearchCombo(){
	
		var url 		= "direct-db.php?requestType=loadSearchCombo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"",
			async:false,
			success:function(json){
					document.getElementById("cboSearch").innerHTML=json.arrCombo;
			}
		});
	
}
//----------------------------------------------------
function loadCRNCombo(){
		var url 		= "direct-db.php?requestType=loadCRNCombo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"",
			async:false,
			success:function(json){
				//	document.getElementById("cboCRN").innerHTML=json.arrCombo;
			}
		});
}
//----------------------------------------------------
function loadCustomerCombo(){
		var url 		= "direct-db.php?requestType=loadCustomerCombo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"",
			async:false,
			success:function(json){
					document.getElementById("cboCustomer").innerHTML=json.arrCombo;
			}
		});
}
//----------------------------------------------------
function loadCurrencyCombo(){
		var url 		= "direct-db.php?requestType=loadCurrencyCombo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"",
			async:false,
			success:function(json){
					document.getElementById("cboCurrency").innerHTML=json.arrCombo;
			}
		});
}
//----------------------------------------------------

function alertx()
{
	$('#frmOrders #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmOrders #butDelete').validationEngine('hide')	;
}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function setInitialRows(id,code,desc,cost,flag){
	if(flag==1){
		var rowCount = document.getElementById('tblOrderItems').rows.length;
				for(var i=1;i<rowCount;i++)
				{
						document.getElementById('tblOrderItems').deleteRow(1);
						
				}
	}
			var content='';

			content='<tr class="normalfnt" >';
			content+='<td align="center" bgcolor="#FFFFFF"><img src="../../../../images/del.png" width="15" height="15" class="delImg" /></td>'
			content+='<td bgcolor="#FFFFFF" align="center"  style="width:40px" class="proID"><select name="cboMainCategory" style="width:140px" id="cboMainCategory" class="mainCat"><option value=""></option></select></td>';
			content+='<td bgcolor="#FFFFFF"  style="width:70px"><input class="validate[required, max[50]] subCode" type="text" name="txtSubCode" id="txtSubCode"  style="width:150px" value="'+code+'"/></td>';
		  content+='<td bgcolor="#FFFFFF"><input name="txtSubDesc" type="text" id="txtSubDesc" style="width:310px"  class="validate[required, max[250]]  subDesc" value="'+desc+'"/></td>';
		 content+='<td bgcolor="#FFFFFF" align="center"><input name="txtEstCost" type="text" id="txtEstCost" style="width:120px" class="validate[required,min[0],custom[number]]  subCost" value="'+cost+'"/></td>';
		 content+='<td bgcolor="#FFFFFF" align="center"><input name="txtEstCost" type="text" id="txtEstCost" style="width:120px" class="validate[required,min[0],custom[number]]  subCost" value="'+cost+'"/></td>';
		 content+='<td bgcolor="#FFFFFF" align="center"><input name="txtEstCost" type="text" id="txtEstCost" style="width:120px" class="validate[required,min[0],custom[number]]  subCost" value="'+cost+'"/></td>';
		 content+=' </tr>';
			//alert(arrData[i]['strCode']);
		add_new_row('#frmOrders #tblOrderItems',content);
		
}

function setLastRow(){
	
			var content='';
			content+='<tr class="dataRow">';
            content+='<td colspan="8" align="left"  bgcolor="#FFFFFF" ><img src="../../../../../images/Tadd.jpg" name="butInsertRow" width="78" height="24" class="mouseover" id="butInsertRow" /></td>';
           content+='</tr>';
		
		add_new_row('#frmOrders #tblOrderItems',content);
}

function loadOrderDetails(){
	
			clearHeader();
			clearRows();
			loadHeaderData();
			loadGridData();
			viewButtons();
}


function loadHeaderData(){
		$('#frmOrders').validationEngine('hide');
		//showWaiting();
		var url = "direct-db.php";
		if($('#frmOrders #txtOrderNo').val()=='')
		{ 
			setInitialRows(1,'','','',1);
			setLastRow();
			$('#frmOrders').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadHeader&serialNo='+$('#frmOrders #txtOrderNo').val()+'&year='+$('#frmOrders #txtYear').val(),
			async:false,
			success:function(json){
					var arrData = json.arrData;
					//json  = eval('('+json+')');
					var i=0;
				//	alert(arrData[i]['strCode']);
				//	$('#frmOrders #txtID').val(arrData[i]['intProjectId']);
					$('#frmOrders #cboCurrency').val(arrData[i]['intCurrencyId']);
					$('#frmOrders #cboCRN').val(arrData[i]['intCrnId']);
					$('#frmOrders #cboCustomer').val(arrData[i]['intCustomer']);
					$('#frmOrders #txtAddress').val(arrData[i]['strAddress']);
					if(arrData[i]['intStatus'])
						$('#frmOrders #chkActive').attr('checked',true);
					else
						$('#frmOrders #chkActive').removeAttr('checked');
			}
	});
}

function loadGridData(){
	//////////// end of load header /////////////////
		var url = "direct-db.php";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&serialNo='+$('#frmOrders #txtOrderNo').val()+'&year='+$('#frmOrders #txtYear').val(),
			async:false,
			success:function(json){
				var arrData = json.arrData;
				for(var i=0; i<arrData.length; i++){
				var mainCat=arrData[i]['intMainCategory'];
				var subCat=arrData[i]['intSubCategory'];
				var itemId=arrData[i]['intItemId'];
				var qty=arrData[i]['dblQty'];
				var uom=arrData[i]['uom'];
				var sellingPrice=arrData[i]['dblSellingPrice'];
				var dblBuyingPrice=arrData[i]['dblBuyingPrice'];
				
				if(i>0){
					$('#butInsertRow').click();
				}
				$('#tblOrderItems >tbody >tr:last').prev().find(".mainCat").val(mainCat);
				$('#tblOrderItems >tbody >tr:last').prev().find(".mainCat").change();
				$('#tblOrderItems >tbody >tr:last').prev().find(".subCat").val(subCat);
				$('#tblOrderItems >tbody >tr:last').prev().find(".subCat").change();
				$('#tblOrderItems >tbody >tr:last').prev().find(".item").val(itemId);
				$('#tblOrderItems >tbody >tr:last').prev().find(".item").change();
				$('#tblOrderItems >tbody >tr:last').prev().find(".uom").html(uom);
				$('#tblOrderItems >tbody >tr:last').prev().find(".qty").val(qty);
				$('#tblOrderItems >tbody >tr:last').prev().find(".sellingPrice").val(sellingPrice);
				$('#tblOrderItems >tbody >tr:last').prev().find(".buyingPrice").val(dblBuyingPrice);
			}
		}
	});
	//////////// end of load details /////////////////
	//hideWaiting();	
}

//----------------------------------------------
function clearHeader(){
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		$('#frmOrders #dtDate').val(d);
		$('#frmOrders #cboCurrency').val('');
		$('#frmOrders #cboCRN').val('');
		$('#frmOrders #cboCustomer').val('');
		$('#frmOrders #chkActive').attr('checked',true);
}
//----------------------------------------------
function clearRows(){
	
	var rowCount = document.getElementById('tblOrderItems').rows.length;
	
	for(var i=2;i<rowCount-1;i++)
	{
		document.getElementById('tblOrderItems').deleteRow(1);
	}
	
	$('#tblOrderItems >tbody >tr').not(':last').not(':first').each(function(){
		
	$(this).parent().parent().find(".mainCat").val('');
	$(this).parent().parent().find(".subCat").html('');
	$(this).parent().parent().find(".item").html('');
	$(this).parent().parent().find(".uom").html('');
	$(this).parent().parent().find(".qty").val('');
	$(this).parent().parent().find(".sellingPrice ").val('');
	$(this).parent().parent().find(".buyingPrice").val('');
	});
		
}


//----------------------------------------------------
function loadMainCatCombo(){
		var url 		= "direct-db.php?requestType=loadMainCatCombo";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"",
			async:false,
			success:function(json){
					document.getElementById("cboMainCategory").innerHTML=json.arrCombo;
			}
		});
	
}
//----------------------------------------------------
function loadSubCategories(obj){
	var mainCat = $(obj).parent().parent().find('.mainCat').val();
	var row=obj.parentNode.parentNode.rowIndex;
	
		var url 		= "direct-db.php?requestType=loadSubCategoriesCombo&mainCat="+mainCat;
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"",
			async:false,
			success:function(json){
			document.getElementById('tblOrderItems').rows[row].cells[2].childNodes[0].innerHTML=json.arrCombo;
			//alert($(obj).parent().parent().find('#frmOrders .cboSubCategory').text());
			//document.getElementById('tblOrderItems').rows[row].cells[2].childNodes[0].innerHTML=httpobj.responseText;
			//alert(json.arrCombo);
			}
		});
	
}
//----------------------------------------------
function loadItems(obj){
	var mainCat = $(obj).parent().parent().find('.mainCat').val();
	var subCat = $(obj).parent().parent().find('.subCat').val();
	var url 		= "direct-db.php?requestType=loadItemCombo&mainCat="+mainCat+"&subCat="+subCat;
	//var httpobj 	= $.ajax({url:url,async:false});
	
	var row=obj.parentNode.parentNode.rowIndex;
	//$(obj).parent().parent().find('#cboItem').text(httpobj.responseText);
//	document.getElementById('tblOrderItems').rows[row].cells[3].childNodes[0].innerHTML=httpobj.responseText;
	
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"",
			async:false,
			success:function(json){
			document.getElementById('tblOrderItems').rows[row].cells[3].childNodes[0].innerHTML=json.arrCombo;
			//alert($(obj).parent().parent().find('#frmOrders .cboSubCategory').text());
			//document.getElementById('tblOrderItems').rows[row].cells[2].childNodes[0].innerHTML=httpobj.responseText;
			//alert(json.arrCombo);
			}
		});
	
	
}
//-----------------------------------------------
function loadItemDetails(obj){
	var itemId = $(obj).parent().parent().find('.item').val();
	var url 		= "direct-db.php?requestType=loadItemDetails&itemId="+itemId;
/*	var httpobj 	= $.ajax({url:url,async:false});
	$(obj).parent().parent().find('.uom').text(httpobj.responseText);
*/	
		var url = "direct-db.php?requestType=loadItemDetails";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"itemId="+itemId,
			async:false,
			success:function(json){

					$(obj).parent().parent().find('.uom').attr('id',(json.uomId));
					$(obj).parent().parent().find('.uom').text(json.uom);
					$(obj).parent().parent().find('.qtyCellP').attr('id',(json.stockQty));
					$(obj).parent().parent().find('.stkQtyP').html(json.stockQty);
					$(obj).parent().parent().find('.stkQtyP').attr('id',(json.stockQty));
			}
		});
	
	
	
}
//--------------------------------------
function viewButtons(){
	var serialNo = $('#txtOrderNo').val();
	var year = $('#txtYear').val();
	var url 		= "direct-db.php?requestType=getViewButtonsPermisions&serialNo="+serialNo+"&year="+year;
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:"",
		async:false,
		success:function(json){
				if((json.confirmPermision==1)){
					$('#butConfirm').show();
				}
				else{
					$('#butConfirm').hide();
				}
				if((json.editPermision==1)&&(json.editableFlag==1)){
					$('#butSave').show();
				}
				else{
					$('#butSave').hide();
				}
		}
	});
}
//--------------------------------------

//----------------------------------	
$('#butConfirm').live('click',function(){
	if($('#txtOrderNo').val()!=''){
		window.open('../listing/rptDirect.php?orderId='+$('#txtOrderNo').val()+'&year='+$('#txtYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no Order No to confirm");
	}
});
//-----------------------------------------------------

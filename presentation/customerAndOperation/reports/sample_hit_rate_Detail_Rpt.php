<?php (define('UNLOCKPAGE', true) ? die('<<< Access denied >>>') : ''); ?>
<?php
$companyId = $sessions->getCompanyId();
$userId = $sessions->getUserId();
$locationId = $sessions->getLocationId();
$programCode = 'P0427';

//Get Dates
$dateFrom  = $_REQUEST['dateFrom'];
$dateTo    = $_REQUEST['dateTo'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Development Hit Rate - Detail Report</title>
    <script type="text/javascript" src="presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder-js.js?n=1"></script>
    <link rel="stylesheet" type="text/css" href="../../../libraries/calendar/theme.css"/>
    <script src="../libraries/calendar/calendar.js" type="text/javascript"></script>
    <script src="../libraries/calendar/calendar-en.js" type="text/javascript"></script>
    <script src="../libraries/calendar/runCalender.js" type="text/javascript"></script>
    <style>
        .break {
            page-break-before: always;
        }

        @media print {
            .noPrint {
                display: none;
            }
        }

        #apDiv1 {
            position: absolute;
            left: 237px;
            top: 175px;
            width: 619px;
            height: 235px;
            z-index: 1;
        }

        .APPROVE {
            font-size: 16px;
            font-weight: bold;
            color: #00C;
        }
    </style>
</head>
<body>
<form id="frmSamplePlaceOrderReport" name="frmSamplePlaceOrderReport" method="post" action="rptBulkOrder.php">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td width="20%"></td>
            <td width="60%" height="80"
                valign="top"><?php include 'presentation/customerAndOperation/reports/reportHeader.php' ?></td>
            <td width="20%"></td>
        </tr>
    </table>
    <div align="center">
        <div style="background-color:#FFF"><strong>Development Hit Rate - Detail Report</strong><strong></strong></div>
        <table width="900" border="0" align="center" bgcolor="#FFFFFF">
            <tr>&nbsp;
                <td style="font-weight: bold">
                    Sample Date From -:&nbsp;<?php echo $dateFrom; ?> &nbsp;&nbsp;&nbsp;To -:&nbsp;<?php echo $dateTo; ?>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td colspan="7" class="normalfnt">
                                <table width="806" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">

                                    <tr class="normalfnt" >
                                        <thead>
                                        <th width="10%" height="22">Marketer</th>
                                        <th width="13%">Customer</th>
                                        <th width="10%">Brand</th>
                                         <th width="12%">Sample Order</th>
                                         <th width="12%">Sample Order Created on</th>
                                         <th width="11%">Sample Category</th>
                                        <th width="10%">Sample NO</th>
                                         <th width="10%">Dispatch No</th>
                                          <th width="10%">Dispatch Date</th>
                                        <th width="11%">Graphics </th>
                                        <th width="7%">Technique </th>
                                        <th width="4%">Heat Transfer </th>
                                        <th width="7%">Combos </th>
                                        <th width="4%">Parts </th>
                                        <th width="11%">Bulk Po Received</th>
                                        </thead>
                                    </tr>
										<?php
											$sql = "SELECT 
												TB1.Marketer AS marketer,
												TB1.Customer AS customer,
												TB1.Brand AS brand,
												TB1.sampleOrderDate,
												TB1.intSampleOrderNo,
												TB1.intSampleOrderYear,
												TB1.sampleCategory,
												TB1.strGraphicRefNo,
												TB1.intSampleNo,
												TB1.intSampleYear,
												CONCAT(TB1.intSampleOrderNo,'/',TB1.intSampleOrderYear) AS sampleOrderNoAndYear,
												CONCAT(TB1.intSampleNo,'/',TB1.intSampleYear) AS sampleNoAndYear,
												
												
												(SELECT  GROUP_CONCAT( distinct trn_sampledispatchdetails.intDispatchNo)
from trn_sampledispatchdetails
INNER JOIN trn_sampledispatchheader on trn_sampledispatchdetails.intDispatchNo = trn_sampledispatchheader.intDispatchNo AND
trn_sampledispatchdetails.intDispatchYear = trn_sampledispatchheader.intDispatchYear
INNER JOIN trn_sampleinfomations_details AS sd1 
ON trn_sampledispatchdetails.intSampleNo = sd1.intSampleNo AND 
trn_sampledispatchdetails.intSampleYear = sd1.intSampleYear AND 
trn_sampledispatchdetails.intRevisionNo = sd1.intRevNo 
where trn_sampledispatchdetails.SAMPLE_ORDER_NO=TB1.intSampleOrderNo 
AND trn_sampledispatchdetails.SAMPLE_ORDER_YEAR=TB1.intSampleOrderYear

 )as dispatchN0,
(SELECT GROUP_CONCAT(distinct trn_sampleorderdetails.strCombo) from trn_sampleorderdetails INNER JOIN trn_sampleorderheader on trn_sampleorderheader.intSampleOrderNo = trn_sampleorderdetails.intSampleOrderNo AND trn_sampleorderheader.intSampleOrderYear = trn_sampleorderdetails.intSampleOrderYear where trn_sampleorderheader.intSampleOrderNo=TB1.intSampleOrderNo AND trn_sampleorderheader.intSampleOrderYear=TB1.intSampleOrderYear AND trn_sampleorderheader.intStatus=1 ) combos,
(SELECT GROUP_CONCAT(distinct mst_part.strName) from trn_sampleorderdetails INNER JOIN trn_sampleorderheader on trn_sampleorderheader.intSampleOrderNo = trn_sampleorderdetails.intSampleOrderNo AND trn_sampleorderheader.intSampleOrderYear = trn_sampleorderdetails.intSampleOrderYear INNER JOIN mst_part on mst_part.intId = trn_sampleorderdetails.intPart where trn_sampleorderheader.intSampleOrderNo=TB1.intSampleOrderNo AND trn_sampleorderheader.intSampleOrderYear=TB1.intSampleOrderYear AND trn_sampleorderheader.intStatus=1 ) as parts,
(SELECT  GROUP_CONCAT(distinct trn_sampledispatchheader.dtSaved)
from trn_sampledispatchheader
INNER JOIN trn_sampledispatchdetails on trn_sampledispatchheader.intDispatchNo = trn_sampledispatchdetails.intDispatchNo AND 
trn_sampledispatchheader.intDispatchYear = trn_sampledispatchdetails.intDispatchYear
where trn_sampledispatchdetails.SAMPLE_ORDER_NO=TB1.intSampleOrderNo 
AND trn_sampledispatchdetails.SAMPLE_ORDER_YEAR=TB1.intSampleOrderYear
AND
trn_sampledispatchheader.intStatus=1 ) dispatchDate,

												(select CONCAT(soh.intSampleOrderNo,'/',soh.intSampleOrderYear) from trn_sampleorderheader AS soh
													INNER JOIN trn_sampleinfomations AS si ON soh.intSampleNo = si.intSampleNo AND soh.intSampleYear = si.intSampleYear
												where TB1.intSampleNo=soh.intSampleNo AND TB1.intSampleYear=soh.intSampleYear AND TB1.strGraphicRefNo=si.strGraphicRefNo  
												order by soh.dtmCreateDate asc limit 1) as first_order,
												(SELECT
													GROUP_CONCAT(distinct mst_techniques.strName) AS tech
													FROM
													trn_sampleorderheader AS soh1
													INNER JOIN trn_sampleinfomations_details AS sd1 ON soh1.intSampleNo = sd1.intSampleNo AND soh1.intSampleYear = sd1.intSampleYear AND soh1.intRevNo = sd1.intRevNo
													INNER JOIN mst_techniques ON sd1.intTechniqueId = mst_techniques.intId
													where
													soh1.intSampleOrderNo=TB1.intSampleOrderNo AND
													soh1.intSampleOrderYear=TB1.intSampleOrderYear) as technique,
													
													(SELECT heatTransfer FROM trn_sampleorderheader AS soh1 INNER JOIN trn_sampleinfomations_details 
													AS sd1 ON soh1.intSampleNo = sd1.intSampleNo AND soh1.intSampleYear = sd1.intSampleYear AND 
													soh1.intRevNo = sd1.intRevNo INNER JOIN mst_techniques ON sd1.intTechniqueId = mst_techniques.intId where soh1.intSampleOrderNo=TB1.intSampleOrderNo 
													AND soh1.intSampleOrderYear=TB1.intSampleOrderYear  LIMIT 1) as heat_transfer, 
													
												 	(SELECT
													COUNT(DISTINCT CONCAT(sd1.intColorId,'/',sd1.intTechniqueId))
													FROM
													trn_sampleorderheader AS soh1
													INNER JOIN trn_sampleinfomations_details AS sd1 ON soh1.intSampleNo = sd1.intSampleNo AND soh1.intSampleYear = sd1.intSampleYear AND soh1.intRevNo = sd1.intRevNo
													where
													soh1.intSampleOrderNo=TB1.intSampleOrderNo AND
													soh1.intSampleOrderYear=TB1.intSampleOrderYear
													) as num_of_colors,
													
													(SELECT
													group_concat(DISTINCT CONCAT(sd1.intColorId,'/',sd1.intTechniqueId))
													FROM
													trn_sampleorderheader AS soh1
													INNER JOIN trn_sampleinfomations_details AS sd1 ON soh1.intSampleNo = sd1.intSampleNo AND soh1.intSampleYear = sd1.intSampleYear AND soh1.intRevNo = sd1.intRevNo
													where
													soh1.intSampleOrderNo=TB1.intSampleOrderNo AND
													soh1.intSampleOrderYear=TB1.intSampleOrderYear
													) as colors,
													
													(SELECT
													sum(trn_sampleinfomations_details_technical.intNoOfShots)
													FROM
													trn_sampleorderheader AS soh1
													INNER JOIN trn_sampleinfomations_details_technical ON soh1.intSampleNo = trn_sampleinfomations_details_technical.intSampleNo AND soh1.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear AND soh1.intRevNo = trn_sampleinfomations_details_technical.intRevNo
													where
													soh1.intSampleOrderNo=TB1.intSampleOrderNo AND
													soh1.intSampleOrderYear=TB1.intSampleOrderYear) as num_of_shots, 
													
													SUM(ActualCount) AS Hit,
													TB1.dispatchN0,
													TB1.dispatchDate 
													
													FROM
													(
													SELECT
													
														sampleInfomationFilter.dispatchN0,
														sampleInfomationFilter.dispatchDate,

														sys_users.strUserName AS Marketer,
														mst_customer.strName AS Customer,
														mst_brand.strName AS Brand,
														sampleInfomationFilter.strGraphicRefNo,
														sampleInfomationFilter.sampleOrderDate,
														sampleInfomationFilter.intSampleOrderNo,
														sampleInfomationFilter.intSampleOrderYear,
														sampleInfomationFilter.sampleCategory,
														sampleInfomationFilter.intSampleNo,
                                                        sampleInfomationFilter.intSampleYear,
														(
															SELECT SUM(

															IF (
																(
																	(
																		trn_orderdetails.intSalesOrderId > 0
																	)
																	AND (
																		trn_orderheader.intStatus <= trn_orderheader.intApproveLevelStart
																		AND trn_orderheader.intStatus >= 1
																		OR trn_orderheader.intStatus in (-10,-1)
																	)
																),
																1,
																0
															)
															)
															FROM
																trn_sampleinfomations_details
															INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
															AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
															AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo
															LEFT JOIN trn_orderdetails ON trn_orderdetails.intSampleNo = trn_sampleinfomations_details.intSampleNo
															AND trn_orderdetails.intSampleYear = trn_sampleinfomations_details.intSampleYear
															AND trn_orderdetails.intRevisionNo = trn_sampleinfomations_details.intRevNo
															AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName
															AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
															LEFT JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo
															AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
															WHERE
																trn_sampleinfomations.intMarketer = sampleInfomationFilter.intMarketer
															AND trn_sampleinfomations.intCustomer = sampleInfomationFilter.intCustomer
															AND trn_sampleinfomations.intBrand = sampleInfomationFilter.intBrand
															AND trn_sampleinfomations.strGraphicRefNo = sampleInfomationFilter.strGraphicRefNo
															AND trn_sampleinfomations.intSampleNo = sampleInfomationFilter.intSampleNo 
															AND trn_sampleinfomations.intSampleYear = sampleInfomationFilter.intSampleYear
														) AS ActualCount
													FROM
														(
															SELECT distinct
																intCustomer,
																intBrand,
																strGraphicRefNo,
																trn_sampleorderheader.dtmCreateDate,
																trn_sampleinfomations.intMarketer,
																trn_sampleinfomations.intSampleNo,
																trn_sampleinfomations.intSampleYear,
																trn_sampleinfomations.intRevisionNo,
																trn_sampleorderdetails.strCombo,
																(trn_sampleorderheader.dtmCreateDate) as sampleOrderDate,
																trn_sampleorderheader.intSampleOrderNo,
																trn_sampleorderheader.intSampleOrderYear,
																mst_sample_category.NAME AS sampleCategory ,
 												
												(SELECT  GROUP_CONCAT( distinct trn_sampledispatchdetails.intDispatchNo)
from trn_sampledispatchdetails
INNER JOIN trn_sampledispatchheader on trn_sampledispatchdetails.intDispatchNo = trn_sampledispatchheader.intDispatchNo AND
trn_sampledispatchdetails.intDispatchYear = trn_sampledispatchheader.intDispatchYear
 
where trn_sampledispatchdetails.SAMPLE_ORDER_NO=trn_sampleorderheader.intSampleOrderNo 
AND trn_sampledispatchdetails.SAMPLE_ORDER_YEAR=trn_sampleorderheader.intSampleOrderYear

 )as dispatchN0,
(SELECT  GROUP_CONCAT(distinct trn_sampledispatchheader.dtSaved)
from trn_sampledispatchheader
INNER JOIN trn_sampledispatchdetails on trn_sampledispatchheader.intDispatchNo = trn_sampledispatchdetails.intDispatchNo AND 
trn_sampledispatchheader.intDispatchYear = trn_sampledispatchdetails.intDispatchYear
where trn_sampledispatchdetails.SAMPLE_ORDER_NO=trn_sampleorderheader.intSampleOrderNo 
AND trn_sampledispatchdetails.SAMPLE_ORDER_YEAR=trn_sampleorderheader.intSampleOrderYear
AND
trn_sampledispatchheader.intStatus=1 ) dispatchDate 
															FROM
																trn_sampleinfomations
															INNER JOIN trn_sampleorderheader ON trn_sampleorderheader.intSampleNo = trn_sampleinfomations.intSampleNo
															AND trn_sampleorderheader.intSampleYear = trn_sampleinfomations.intSampleYear
															AND trn_sampleorderheader.intRevNo = trn_sampleinfomations.intRevisionNo
															LEFT JOIN mst_sample_category ON mst_sample_category.ID = trn_sampleorderheader.`SAMPLE CATEGORY`
															INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
															AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
															AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo
															INNER JOIN trn_sampleorderdetails ON trn_sampleorderdetails.strCombo = trn_sampleinfomations_details.strComboName
															AND trn_sampleorderdetails.intSampleOrderNo = trn_sampleorderheader.intSampleOrderNo
															AND trn_sampleorderdetails.intSampleOrderYear = trn_sampleorderheader.intSampleOrderYear
															WHERE
																date(
																	trn_sampleorderheader.dtmCreateDate
																) BETWEEN ('$dateFrom')
															AND ('$dateTo')
															AND trn_sampleorderheader.intStatus = '1'
														) AS sampleInfomationFilter
													INNER JOIN sys_users ON sys_users.intUserId = sampleInfomationFilter.intMarketer
													INNER JOIN mst_customer ON mst_customer.intId = sampleInfomationFilter.intCustomer
													INNER JOIN mst_brand ON mst_brand.intId = sampleInfomationFilter.intBrand
													GROUP BY
														Marketer,
														Customer,
														Brand,
														sampleInfomationFilter.strGraphicRefNo,
														sampleInfomationFilter.intSampleOrderNo,
														sampleInfomationFilter.intSampleOrderYear,
														sampleInfomationFilter.intSampleNo,
														sampleInfomationFilter.intSampleYear
													ORDER BY
														Marketer,
														Customer,
														Brand,
														sampleInfomationFilter.strGraphicRefNo,
														sampleInfomationFilter.intSampleOrderNo,
														sampleInfomationFilter.intSampleOrderYear,
														sampleInfomationFilter.intSampleNo,
														sampleInfomationFilter.intSampleYear
												) AS TB1
											GROUP BY
												TB1.Marketer,
												TB1.Customer,
												TB1.Brand,
												TB1.strGraphicRefNo,
												TB1.intSampleOrderNo,
												TB1.intSampleOrderYear,
												TB1.intSampleNo,
												TB1.intSampleYear";
//											echo $sql;
                        $result = $db->RunQuery($sql);
                            while ( $row = mysqli_fetch_array($result)) {
                            $intSampleOrderYear   	= $row['sampleOrderNoAndYear'];
                            $Graphics 				= $row['strGraphicRefNo'];
                            $bulk_Po_Received   	= $row['Hit'];
                            $sampleNoAndYear    	= $row['sampleNoAndYear'];
			   				$sampleCategory     	= $row['sampleCategory'];
							$sampleOrderDate     	= $row['sampleOrderDate'];
							$first_samp_order     	= $row['first_order'];
							$technique	     		= $row['technique'];
							$dispatchN0     		= $row['dispatchN0'];
							$dispatchDate	     	= $row['dispatchDate'];
							$heatTransfer           = $row['heat_transfer'];
                            $combos	     	        = $row['combos'];
                            $parts                 = $row['parts'];
							if ($heatTransfer == 1){
                                $heatTransfer = 'Yes';
                            } else {
                                $heatTransfer = 'No';
                            }
							
								
							if($first_samp_order != $intSampleOrderYear)
								$bgCol	="#d3d3d3";
							else
								$bgCol	="#70C18B";
                            ?>
                        <tr class="normalfnt" bgcolor="<?php echo $bgCol; ?>">
                            <td class="normalfnt" NOWRAP style="text-align: center">&nbsp;
                            <?php echo $row['marketer']; ?>&nbsp;</td>
                            <td class="normalfnt" style="text-align: center">&nbsp;
                            <?php echo $row['customer']; ?>&nbsp;</td>
                            <td class="normalfnt" style="text-align: center">&nbsp;<?php echo $row['brand']; ?></td>
                           <td class="normalfntRight" style="text-align: center">&nbsp;<?php echo $intSampleOrderYear; ?></td>
                           <td class="normalfntMid"><span class="normalfntRight" style="text-align: center"><?php echo $sampleOrderDate; ?></span></td>
                            <td class="normalfntMid">&nbsp;<?php echo $sampleCategory; ?></td>
			    			<td class="normalfntRight" style="text-align: center">&nbsp;<?php echo  $sampleNoAndYear; ?></td>
                            <td class="normalfntRight" style="text-align: center">&nbsp;<?php echo $dispatchN0; ?></td>
                            <td class="normalfntRight" style="text-align: center">&nbsp;<?php echo $dispatchDate; ?></td>
                             <td class="normalfntMid">&nbsp;<?php echo $Graphics; ?></td>
                            <td class="normalfntMid">&nbsp;<?php echo $technique; ?></td>
                            <td class="normalfntMid">&nbsp;<?php echo $heatTransfer; ?></td>
                            <td class="normalfntMid">&nbsp;<?php echo $combos; ?></td>
                            <td class="normalfntMid">&nbsp;<?php echo $parts; ?></td>
                            
                            <td class="normalfntMid">&nbsp;
                            <?php 
                            	if ($bulk_Po_Received>0) {
                            		echo "Yes";
                            	}
                            	else{
                            		echo "No";
                            	}

                              ?></td>
                            <?php }?>

                        </tr>
                        <tr style="height: 40px">
                             <td colspan="24"></td>
                        </tr>
                                </table>
                            </td>
                            <td width="6%">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
</form>
</body>
</html>


<?php (define('UNLOCKPAGE', true) ? die('<<< Access denied >>>') : ''); ?>
<?php
$companyId = $sessions->getCompanyId();
$userId = $sessions->getUserId();
$locationId = $sessions->getLocationId();
$programCode = 'P1278';


$dateFrom = $_REQUEST['dateFrom'];
$dateTo = $_REQUEST['dateTo'];
$firstCheck = $_REQUEST['firstCheck'];
$secondCheck = $_REQUEST['secondCheck'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>DUMMY PO DETAIL REPORT</title>
        <script type="text/javascript"
        src="presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder-js.js?n=1"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/excellentexport@2.1.0/dist/excellentexport.min.js"></script>
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css"/>
        <style>
            .break {
                page-break-before: always;
            }

            @media print {
                .noPrint {
                    display: none;
                }
            }

            #apDiv1 {
                position: absolute;
                left: 237px;
                top: 175px;
                width: 619px;
                height: 235px;
                z-index: 1;
            }

            .APPROVE {
                font-size: 16px;
                font-weight: bold;
                color: #00C;
            }

            .foo {
                float: left;
                width: 20px;
                height: 20px;
                margin: 1px;
                border: 1px solid rgba(0, 0, 0, .2);
            }

            .blue {
                background: #71ede4;
            }
            .red {
                background: #FF99CC;
            }
            div.dataTables_length {
                padding-left: 2em;
            }
            div.dataTables_length,
            div.dataTables_filter {
                padding-top: 0.55em;
            }
            div.dt-buttons{
                position:relative !important;
                float:left !important;
            }
            #tblMainGrid_paginate{
                position: absolute;
                top: -87px;
                right: 837px;
                width: 436px;
                height: 100px;
            }

            .type_2 {
                background-color:  #FF99CC !important;
            }

            .stock {
                background-color:  #71ede4 !important;
            }

            .dataTables_paginate {
                margin-top: 15px;
                position: absolute;
                text-align: right;
                left: 30%;
            }
        </style>


        <script src="libraries/dataTables/jquery.dataTables.min.js"></script>
        <script src="libraries/dataTables/dataTables.buttons.min.js"></script>
        <script src="libraries/dataTables/buttons.flash.min.js"></script>
        <script src="libraries/dataTables/jszip.min.js"></script>
        <script src="libraries/dataTables/pdfmake.min.js"></script>
        <script src="libraries/dataTables/vfs_fonts.js"></script>
        <script src="libraries/dataTables/buttons.html5.min.js"></script>
        <script src="libraries/dataTables/buttons.print.min.js "></script>

        <link rel="stylesheet" type="text/css" href="libraries/dataTables/css/jquery.dataTables.min.css">
            <link rel="stylesheet" type="text/css" href="libraries/dataTables/css/buttons.dataTables.min.css">

                <script type="text/javascript">

                    $(document).ready(function () {

                        var table = $('#tblMainGrid').DataTable({

                            "bFilter": false,
                            pageLength: 50,
                            responsive: true,
                            "scrollY": 600,
                            "scrollX": true,
                            dom: 'Bfrtip',
                            buttons: [{
                                    extend: 'print',
                                    text: 'Print',
                                    exportOptions: {
                                        columns: ':not(.no-print)'
                                    },
                                    footer: true,
                                    autoPrint: false
                                }, {

                                    extend: 'pdfHtml5',
                                    messageTop: 'DUMMY PO DETAIL REPORT',
                                    orientation: 'landscape',
                                    pageSize: 'LEGAL',
                                    text: 'PDF',

                                }, {

                                    extend: 'excel',
                                    messageTop: 'DUMMY PO DETAIL REPORT',
                                    text: 'Excel',

                                },
                                {

                                    extend: 'csv',
                                    text: 'CSV',
                                    customize: function (csv) {

                                        return '\t DUMMY PO DETAIL REPORT\n' + csv;
                                    },
                                    exportOptions: {
                                        columns: ':not(.no-print)'
                                    }


                                }],
                            "createdRow": function (row, data, dataIndex) {
                                // alert(data[0]);

                                if (data[12].replace(/[\$,]/g, '') * 1 < data[15]) {

                                    $(row).find('td:eq(12)').addClass('stock');
                                }


                            },
                            //invisible type column on table
                            "columnDefs": [
                                {
                                    "targets": 0,
                                    //"visible": false,
                                    "searchable": false
                                }
                            ]
                                    // table.column( 0 ).visible( false );
                        });


                    });
                </script>




                </head>

                <body>
                    <form id="frmDummyDetails" name="frmDummyDetails" method="post">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td colspan="3"></td>
                            </tr>
                            <tr>
                                <td width="20%"></td>
                                <td width="60%" height="80"
                                    valign="top"><?php include 'presentation/procurement/reports/reportHeader.php' ?></td>
                                <td width="20%"></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <strong> Dummy PO Created Date From -:</strong>&nbsp;<strong><?php echo $dateFrom; ?></strong> &nbsp;&nbsp;&nbsp;<strong>Created Date To -:</strong>&nbsp;<strong><?php echo $dateTo; ?></strong>


                                </td>

                            </tr>
                            <td width="14%">&nbsp;</td>
                                <td width="24%">&nbsp;</td>
                        </table>
                        <div align="center" sstyle="font-weight: bold"><strong>
<?php if ($firstCheck == 1 && $secondCheck == 0) { ?>
                                REPORT OF DUMMY POs HAVING PANEL QTY GREATER THAN ZERO <?php } else if ($secondCheck == 2 && $firstCheck == 0) {
    ?>
                                REPORT OF DUMMY POs NOT HAVING ACTUAL POs
                            <?php } else if($secondCheck == 2 && $firstCheck == 1){ ?>
                                DUMMY PO DETAIL REPORT
                            <?php } ?>


                        </strong><strong></strong></div>

                <table width="900" border="0" align="center" bgcolor="#FFFFFF">
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td colspan="10" align="center" bgcolor="#FFFFFF">
                                    </td>
                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td colspan="20">
                                    </td>
                                </tr>
                                <tr height="50">
                                    <td></td>
                                </tr> 

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br/>
                            <table width="100%">
                                <tr>
                                    <td colspan="20">


                                        <div>
                                            <table width="100%" class="table table-striped table-bordered bordered" id="tblMainGrid"  cellspacing="0"
                                                   cellpadding="0">
                                                <thead>
                                                    <tr class="">

                                                        <th class="tbheader"><div>Dummy Order</div></th>
                                                        <th class="tbheader"><div>Dummy Order Year</div></th>
                                                        <th class="tbheader"><div>Customer PO</div></th>
                                                        <th  class="tbheader"><div>Brand</div></th>
                                                        <th class="tbheader"><div>Customer</div></th>
                                                        <th class="tbheader"><div>Customer Location</div></th>
                                                        <th class="tbheader"><div>Marketer</div></th>
                                                        <th class="tbheader"><div>Created User</div></th>
                                                        <th class="tbheader"><div>Contact Person</div>                </th>
                                                        <th class="tbheader"><div>Remaining Dummy Qty</div></th>
                                                        <th class="tbheader"><div>Initial Dummy Qty</div></th>
                                                        <th class="tbheader"><div>Split Qty</div></th>
                                                        <th class="tbheader"><div>Created Date</div></th>
                                                        <th  class="tbheader"><div>1st Approval</div></th>
                                                        <th class="tbheader"><div>2nd Approval</div></th>
                                                        <th class="tbheader"><div>3rd Approval</div></th>
                                                        <th class="tbheader"><div>Status</div></th>

                                                    </tr>

                                                </thead>
                                                <tbody>
<?php
$sql = getReportData($dateFrom, $dateTo, $firstCheck, $secondCheck);
$result_tech = $db->RunQuery($sql);
while ($row = mysqli_fetch_array($result_tech)) {
    ?>
                                                        <tr>

                                                            <td> 
                                                        <?php echo $row['dummyOrder']; ?></td>
                                                            <td>
                                                        <?php echo $row['dummyOrderYear']; ?></td>

                                                            <td>
                                                                <?php echo $row['customerPo']; ?></td>
                                                            <td>
                                                                <?php echo getBrand($row['sample'], $row['sample_year'], $row['rev']); ?></td>
                                                            <td>
    <?php echo $row['customer']; ?></td>
                                                            <td>
                                                                <?php echo $row['customerLocation']; ?></td>
                                                            <td>
                                                                <?php echo $row['marketer']; ?></td>

                                                            <td>
                                                                <?php echo $row['createdUser']; ?></td>
                                                            <td>
                                                                <?php echo $row['contactPerson']; ?></td>
                                                            <td>
    <?php echo $row['panelQty']; ?></td>
                                                            <td>
                                                                <?php echo $row['initial_dummy_qty']; ?></td>
                                                            
                                                             <td>
                                                                <?php echo $row['initial_dummy_qty'] - $row['panelQty']; ?></td>
                                                            <td>
                                                                <?php echo $row['createdDate']; ?></td>

                                                            <td>
                                                                <?php
                                                                if ($row['firstApprovedDate'] == 0)
                                                                    echo "";
                                                                else
                                                                    echo $row['firstApprovedDate']  
                                                                    ?></td>

                                                             <td>
                                                                <?php
                                                                if ($row['secondApproved'] == 0)
                                                                    echo "";
                                                                else
                                                                    echo $row['secondApproved']
                                                                    ?></td>

                                                            <td>
                                                                <?php
                                                                if ($row['thirdApproved'] == 0)
                                                                    echo "";
                                                                else
                                                                    echo $row['thirdApproved']
                                                                    ?></td>

                                                            <td >
                                                                <?php echo $row['dummyPoStatus']; ?></td>

                                                        </tr>


<?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>    

                            </table>
                        </td>
                        <td width="6%"></td>
                    </tr>
                </table>
                </div>
            </form>
        </body>
        </html>
<?php

function getReportData($dateFrom, $dateTo, $firstCheck, $secondCheck) {


    $sql = "SELECT
	dummyPo.intOrderNo AS dummyOrder,
	dummyPo.intOrderYear AS dummyOrderYear,
	dummyPo.strCustomerPoNo AS customerPo,
	dummyPo.customer AS customer,
	dummyPo.Marketer AS marketer,
	
	dummyPo.panel_qty AS panelQty,
        dummyPo.initial_dummy_qty,
	dummyPo.created_user AS createdUser,
	dummyPo.dtmCreateDate AS createdDate,
	dummyPo.ApproLevelOne AS firstApprovedDate,
	dummyPo.customer_location AS customerLocation,
	dummyPo.strContactPerson AS contactPerson,
        dummyPo.ApproLevelTwo AS secondApproved,
	dummyPo.ApproLevelThree AS thirdApproved,
	

IF (
	dummyPo.dummmy_status = 1,
	'Approved',

IF (
	dummyPo.dummmy_status = 0,
	'Rejected',

IF (
	dummyPo.dummmy_status =- 10,
	'Completed',

IF (
	dummyPo.dummmy_status =- 2,
	'Cancelled',

IF (
	dummyPo.dummmy_status =- 1,
	'Revised',
	'Pending'
)
)
)
)
) AS dummyPoStatus,
 actualPo.actual_status,
 actualPo.dummy_po,
 dummyPo.sample,
 dummyPo.sample_year,
 dummyPo.rev
FROM
	(
		SELECT DISTINCT
			trn_orderheader.intOrderNo,
			trn_orderheader.intOrderYear,
			trn_orderheader.strCustomerPoNo,
			trn_orderheader.intCustomer,
			trn_orderheader.intMarketer,
			trn_orderheader.intCreator,
			trn_orderheader.dtmCreateDate,
			trn_orderheader.intCustomerLocation,
			trn_orderheader.strContactPerson,
			mst_customer.strName AS customer,
			mst_customer_locations_header.strName AS customer_location,
			marketer.strUserName AS Marketer,
			creator.strUserName AS created_user,
			trn_orderdetails.intSampleNo as sample,
			trn_orderdetails.intSampleYear as sample_year,
			trn_orderdetails.intRevisionNo as rev,
			sum(trn_orderdetails.intQty) AS panel_qty,
                        trn_orderheader.intStatus AS dummmy_status,
                        sum(trn_orderdetails.DUMMY_SO_INITIAL_QTY) as initial_dummy_qty,
			(
				SELECT
					trn_orderheader_approvedby.dtApprovedDate
				FROM
					trn_orderheader_approvedby
				WHERE
					trn_orderheader_approvedby.intOrderNo = trn_orderheader.intOrderNo
				AND trn_orderheader_approvedby.intYear = trn_orderheader.intOrderYear
				AND trn_orderheader_approvedby.intApproveLevelNo = 1
                                AND trn_orderheader_approvedby.intStatus = 0
			) AS ApproLevelOne,
			(
				SELECT
					trn_orderheader_approvedby.dtApprovedDate
				FROM
					trn_orderheader_approvedby
				WHERE
					trn_orderheader_approvedby.intOrderNo = trn_orderheader.intOrderNo
				AND trn_orderheader_approvedby.intYear = trn_orderheader.intOrderYear
				AND trn_orderheader_approvedby.intApproveLevelNo = 2
                                AND trn_orderheader_approvedby.intStatus = 0
			) AS ApproLevelTwo,
			(
				SELECT
					trn_orderheader_approvedby.dtApprovedDate
				FROM
					trn_orderheader_approvedby
				WHERE
					trn_orderheader_approvedby.intOrderNo = trn_orderheader.intOrderNo
				AND trn_orderheader_approvedby.intYear = trn_orderheader.intOrderYear
				AND trn_orderheader_approvedby.intApproveLevelNo = 3
                                AND trn_orderheader_approvedby.intStatus = 0
			) AS ApproLevelThree
		FROM
			trn_orderheader
		INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
		AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
		INNER JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
		INNER JOIN mst_customer_locations_header ON mst_customer_locations_header.intId = trn_orderheader.intCustomerLocation
		INNER JOIN sys_users AS marketer ON marketer.intUserId = trn_orderheader.intMarketer
		INNER JOIN sys_users AS creator ON creator.intUserId = trn_orderheader.intCreator
		
		WHERE
			date(
				trn_orderheader.dtmCreateDate
			)  BETWEEN '$dateFrom' AND '$dateTo'
		AND trn_orderheader.PO_TYPE = 1
               
		GROUP BY
			trn_orderdetails.intOrderNo,
			trn_orderdetails.intOrderYear
	) AS dummyPo
LEFT JOIN (
	SELECT
		trn_orderheader.intOrderNo AS actual_order,
		trn_orderheader.DUMMY_PO_NO AS dummy_po,
		trn_orderheader.intStatus AS actual_status
	FROM
		trn_orderheader
	WHERE
		trn_orderheader.PO_TYPE = 2
	AND trn_orderheader.DUMMY_PO_NO IN (
		SELECT
			trn_orderheader.intOrderNo
		FROM
			trn_orderheader
		WHERE
			date(
				trn_orderheader.dtmCreateDate
			)  BETWEEN '$dateFrom' AND '$dateTo'
		AND trn_orderheader.PO_TYPE = 1

	)
        -- AND trn_orderheader.intStatus = 4
) AS actualPo ON dummyPo.intOrderNo = actualPo.dummy_po
GROUP BY
dummyPo.intOrderNo,
dummyPo.intOrderYear
HAVING";
    if ($firstCheck == 1 && $secondCheck==0 ) {
        $sql .= " dummyPo.panel_qty >0";
    } else if ($secondCheck == 2 && $firstCheck==0) {
        $sql .= " actualPo.dummy_po IS NULL OR actualPo.actual_status = 4";
    } else if($firstCheck == 1 && $secondCheck == 2){
        $sql .= " actualPo.dummy_po IS NULL OR actualPo.actual_status = 4
OR dummyPo.panel_qty >0";
    }
    //echo $sql;
    return $sql;
}

function getBrand($sampleNo, $sampleYear, $revNo) {
    global $db;

    $sql = "SELECT
				mst_brand.strName,
				mst_brand.intId
				FROM
				trn_sampleinfomations
				INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
				WHERE
				trn_sampleinfomations.intSampleNo = '$sampleNo' AND
				trn_sampleinfomations.intSampleYear = '$sampleYear' AND
				trn_sampleinfomations.intRevisionNo = '$revNo'";
    $result_brand = $db->RunQuery($sql);
    $row_brand = mysqli_fetch_array($result_brand);
    $brand = $row_brand['strName'];
    return $brand;
}
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
//include  "{$backwardseperator}dataAccess/permisionCheck.inc";
//include   "reports.js";
//BEGIN - CLASS OBJECTS {
//$obj_getData  = new clsGetData;
//END - CLASS OBJECTS   }
$userId         = $_SESSION['userId'];
$year           = $_SESSION["Year"];
$month          = $_SESSION["Month"];
?>
    <title>nsoft | Procument - Reports</title>
    <script type="text/javascript" src="presentation/customerAndOperation/reports/reports.js"></script>
    
    <div align="center">
    <div class="trans_layoutL" style="width:600px">
        <div class="trans_text">Marketing Reports</div>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <!--BEGIN - PO Summery-->
            <tr>
                <td><table align="left" width="600" class="main_header_table" id="tblGraphicWiseHead" cellpadding="2" cellspacing="0">
                        <tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Sample Hit Rate Grapic Wise</u></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td><form name="frmGraphicWise" id="frmGraphicWise" autocomplete="off">
                        <table width="600" align="left" border="0" id="tblGraphicWiseBody" class="main_table" cellspacing="0" style="display:none;">
                            <thead>
                            <tr>
                                <td colspan="4" style="text-align:left">Sample Hit Rate Grapic Wise<img src="images/close_small.png" align="right" class="mouseover"/>
                                </td>
                            </tr>
                            </thead>
                            <tbody align="center">
                            <tr>
                                <td width="200" align="left"><span class="normalfnt">Date From</span></td>
                                <td width="443" align="left"><input name="txtFromDate" type="text" class="validate[required] txtbox" id="grphicwiseDateFrom" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);" onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value="" class="txtbox" style="visibility:hidden;width:2px;" onclick="return showCalendar(this.id, '%Y-%m-%');"/></td>
                            </tr>
                            <tr>
                            <td width="200" align="left"><span class="normalfnt">Date To</span></td>
                            <td width="443" align="left"><input name="txtToDate" type="text"
                                                                class="validate[required] txtbox" id="grphicwiseDateTo" style="width:98px;"
                                                                onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();"
                                                                onkeypress="return ControlableKeyAccess(event);"
                                                                onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value="" class="txtbox" style="visibility:hidden;width:2px;"
                                                                                                                            onclick="return showCalendar(this.id, '%Y-%m-%');"/></td>
                            </tr>
                            <tr>
                                <td class="normalfnt">&nbsp;</td>
                                <td >&nbsp;</td>
                                <td >&nbsp;</td>
                                <td >&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                        <tr>
                                            <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReportGraphic('frmGraphicWise')"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table></form></td>
            </tr>
            <tr>
                <td><table align="left" width="600" class="main_header_table" id="tblComboWiseHead" cellpadding="2" cellspacing="0">
                        <tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Sample Hit Rate Combo Wise</u></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td><form name="frmComboWise" id="frmComboWise" autocomplete="off">
                        <table width="600" align="left" border="0" id="tblComboWiseBody" class="main_table" cellspacing="0" style="display:none;">
                            <thead>
                            <tr>
                                <td colspan="4" style="text-align:left">Sample Hit Rate Combo Wise<img src="images/close_small.png" align="right" class="mouseover"/>
                                </td>
                            </tr>
                            </thead>
                            <tbody align="center">
                            <tr>
                                <td width="200" align="left"><span class="normalfnt">Date From</span></td>
                                <td width="443" align="left"><input name="txtFromDate" type="text"
                                                                    class="validate[required] txtbox" id="combowiseDateFrom" style="width:98px;"
                                                                    onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();"
                                                                    onkeypress="return ControlableKeyAccess(event);"
                                                                    onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value="" class="txtbox" style="visibility:hidden;width:2px;"
                                                                                                                                onclick="return showCalendar(this.id, '%Y-%m-%');"/></td>
                            </tr>
                            <tr>
                                <td width="200" align="left"><span class="normalfnt">Date To</span></td>
                                <td width="443" align="left"><input name="txtToDate" type="text"
                                                                    class="validate[required] txtbox" id="combowiseDateTo" style="width:98px;"
                                                                    onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();"
                                                                    onkeypress="return ControlableKeyAccess(event);"
                                                                    onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value="" class="txtbox" style="visibility:hidden;width:2px;"
                                  /tr>
                            <tr>
                                <td class="normalfnt">&nbsp;</td>
                                <td >&nbsp;</td>
                                <td >&nbsp;</td>
                                <td >&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                        <tr>
                                            <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReportComboWise('frmComboWise')"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table></form></td>
            </tr>


         <tr>
                <td><table align="left" width="600" class="main_header_table" id="tblDetailHead" cellpadding="2" cellspacing="0">
                        <tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Sample Hit Rate Detail Report</u></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td><form name="frmDetail" id="frmDetail" autocomplete="off">
                        <table width="600" align="left" border="0" id="tblDetailBody" class="main_table" cellspacing="0" style="display:none;">
                            <thead>
                            <tr>
                                <td colspan="4" style="text-align:left">Sample Hit Rate Detail Report<img src="images/close_small.png" align="right" class="mouseover"/>
                                </td>
                            </tr>
                            </thead>
                            <tbody align="center">
                            <tr>
                                <td width="200" align="left"><span class="normalfnt">Date From</span></td>
                                <td width="443" align="left"><input name="txtFromDate" type="text"
                                                                    class="validate[required] txtbox" id="DetailDateFrom" style="width:98px;"
                                                                    onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();"
                                                                    onkeypress="return ControlableKeyAccess(event);"
                                                                    onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value="" class="txtbox" style="visibility:hidden;width:2px;"
                                                                                                                                onclick="return showCalendar(this.id, '%Y-%m-%');"/></td>
                            </tr>
                            <tr>
                                <td width="200" align="left"><span class="normalfnt">Date To</span></td>
                                <td width="443" align="left"><input name="txtToDate" type="text"
                                                                    class="validate[required] txtbox" id="DetailDateTo" style="width:98px;"
                                                                    onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();"
                                                                    onkeypress="return ControlableKeyAccess(event);"
                                                                    onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value="" class="txtbox" style="visibility:hidden;width:2px;"
                                  /tr>
                            <tr>
                                <td class="normalfnt">&nbsp;</td>
                                <td >&nbsp;</td>
                                <td >&nbsp;</td>
                                <td >&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                        <tr>
                                            <td width="100%" align="center" bgcolor=""><img style="display:inline" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReportDetail('frmDetail')"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table></form></td>
            </tr>
            
              <tr>
                <td><table align="left" width="600" class="main_header_table" id="tblDummyPoDetailHead" cellpadding="2" cellspacing="0">
                        <tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Dummy PO Detail Report</u></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td><form name="frmDummyPo" id="frmDummyPo" autocomplete="off">
                        <table width="600" align="left" border="0" id="tblDummyPoDetailBody" class="main_table" cellspacing="0" style="display:none;">
                            <thead>
                            <tr>
                                <td colspan="4" style="text-align:left">Dummy PO Details<img src="images/close_small.png" align="right" class="mouseover"/>
                                </td>
                            </tr>
                            </thead>
                            <tbody align="center">
                            <tr>
                                <td width="200" align="left"><span class="normalfnt">Date From</span></td>
                                <td width="443" align="left"><input name="txtFromDate" type="text" class="validate[required] txtbox" id="DummyPODateFrom"
                                                                    style="width:98px;" onmousedown="DisableRightClickEvent();" 
                                                                    onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);" 
                                                                    onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value="" c
                                                                    lass="txtbox" style="visibility:hidden;width:2px;" 
                                                                    onclick="return showCalendar(this.id, '%Y-%m-%');"/></td>
                            </tr>
                             <tr>
                                <td width="200" align="left"><span class="normalfnt">Date To</span></td>
                                <td width="443" align="left"><input name="txtToDate" type="text" class="validate[required] txtbox" id="DummyPODateTo"
                                                                    style="width:98px;" onmousedown="DisableRightClickEvent();" 
                                                                    onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);" 
                                                                    onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value="" c
                                                                    lass="txtbox" style="visibility:hidden;width:2px;" 
                                                                    onclick="return showCalendar(this.id, '%Y-%m-%');"/></td>
                            </tr>
                            
                           
                           
                             <tr>

                                <td width="22.5%" height="27" class="normalfnt"><input type="checkbox" name="check1" id="check1" value="1"> Partly Split Dummy POs</td>
                                <td width="22.5%">
                                    <input type="checkbox" name="check2" id="check2" value="2"> Unsplit Dummy POs
                                </td>
                                <td width="14%">&nbsp;</td>
                                <td width="24%">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="normalfnt">&nbsp;</td>
                                <td >&nbsp;</td>
                                <td >&nbsp;</td>
                                <td >&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center" class="normalfntMid"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                        <tr>
                                            <td width="100%" align="center" bgcolor="">
                                                <img style="display:inline" border="0" 
                                                     src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  
                                                     class="mouseover" id="butNew" tabindex="28"/>
                                                <img  style="display:inline" border="0" 
                                                      src="images/Treport.jpg" alt="Save" 
                                                      name="butReports" class="mouseover" 
                                                      id="butReports" tabindex="24" 
                                                      onClick="ViewReportDummyOrderDetails('frmDummyPo')"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table></form></td>
            </tr>

        </table>
    </div>
</div>
<?php (define('UNLOCKPAGE', true) ? die('<<< Access denied >>>') : ''); ?>
<?php
$companyId = $sessions->getCompanyId();
$userId = $sessions->getUserId();
$locationId = $sessions->getLocationId();
$programCode = 'P0427';

//Get Dates
$dateFrom  = $_REQUEST['dateFrom'];
$dateTo    = $_REQUEST['dateTo'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Development Hit Rate Report - Combo wise</title>
    <script type="text/javascript" src="presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder-js.js?n=1"></script>
    <style>
        .break {
            page-break-before: always;
        }

        @media print {
            .noPrint {
                display: none;
            }
        }

        #apDiv1 {
            position: absolute;
            left: 237px;
            top: 175px;
            width: 619px;
            height: 235px;
            z-index: 1;
        }

        .APPROVE {
            font-size: 16px;
            font-weight: bold;
            color: #00C;
        }
    </style>
</head>
<body>
<form id="frmSamplePlaceOrderReport" name="frmSamplePlaceOrderReport" method="post" action="rptBulkOrder.php">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td width="20%"></td>
            <td width="60%" height="80"
                valign="top"><?php include 'presentation/customerAndOperation/reports/reportHeader.php' ?></td>
            <td width="20%"></td>
        </tr>
    </table>
    <div align="center">
        <div style="background-color:#FFF"><strong>Development Hit Rate Report - Combo wise</strong><strong></strong></div>
        <table width="900" border="0" align="center" bgcolor="#FFFFFF">
            <tr>&nbsp;
                <td style="font-weight: bold">
                    Sample Date From -:&nbsp;<?php echo $dateFrom; ?> &nbsp;&nbsp;&nbsp;To -:&nbsp;<?php echo $dateTo; ?>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td colspan="7" class="normalfnt">
                                <table width="806" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
                                <tr class="normalfnt">
                                        <th width="8%" height="22">Marketer</th>
                                        <th width="12%">Customer</th>
                                        <th width="9%">Brand</th>
                                        <th width="9%">Combos Developed</th>
                                        <th width="10%">Actual Order Received</th>
                                        <th width="10%">Hit %</th>
                                </tr>
										<?php
                                        $sql = "SELECT
                                            TB1.Marketer AS marketer,
                                            TB1.Customer AS customer,
                                            TB1.Brand AS brand,
                                            count(TB1.strCombo) AS Developed_Count,
                                            SUM(ActualCount) AS Hit
                                        FROM
                                            (
                                                SELECT
                                                    sys_users.strUserName AS Marketer,
                                                    mst_customer.strName AS Customer,
                                                    mst_brand.strName AS Brand,
                                                    sampleInfomationFilter.strGraphicRefNo,
                                                    sampleInfomationFilter.strCombo,
                                                    (
                                                        SELECT
        
                                                        IF (
                                                            ((trn_orderdetails.intSalesOrderId >0 ) AND (trn_orderheader.intStatus <=  trn_orderheader.intApproveLevelStart AND trn_orderheader.intStatus >= 1 OR trn_orderheader.intStatus = -10)),
                                                            1,
                                                            0
                                                        )
                                                        FROM
                                                            trn_sampleinfomations_details
                                                        INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                                                        AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                                                        AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo
                                                        LEFT JOIN trn_orderdetails ON trn_orderdetails.intSampleNo = trn_sampleinfomations_details.intSampleNo
                                                        AND trn_orderdetails.intSampleYear = trn_sampleinfomations_details.intSampleYear
                                                        AND trn_orderdetails.intRevisionNo = trn_sampleinfomations_details.intRevNo
                                                        AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName
                                                        AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName
                                                        LEFT JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo 
                                                        AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
                                                        WHERE
                                                            trn_sampleinfomations.intMarketer = sampleInfomationFilter.intMarketer
                                                        AND trn_sampleinfomations.intCustomer = sampleInfomationFilter.intCustomer
                                                        AND trn_sampleinfomations.intBrand = sampleInfomationFilter.intBrand
                                                        AND trn_sampleinfomations.strGraphicRefNo = sampleInfomationFilter.strGraphicRefNo
                                                        AND trn_sampleinfomations.intSampleNo = sampleInfomationFilter.intSampleNo
                                                        -- AND trn_sampleinfomations.intRevisionNo = sampleInfomationFilter.intRevisionNo
                                                        AND trn_sampleinfomations.intSampleYear = sampleInfomationFilter.intSampleYear
                                                        AND trn_sampleinfomations_details.strComboName = sampleInfomationFilter.strCombo
														ORDER BY trn_orderdetails.intSalesOrderId DESC
                                                        LIMIT 1
                                                    ) AS ActualCount
                                                FROM
                                                    (
                                                        SELECT
                                                            intCustomer,
                                                            intBrand,
                                                            strGraphicRefNo,
                                                            trn_sampleorderheader.dtmCreateDate,
                                                            trn_sampleinfomations.intMarketer,
                                                            trn_sampleinfomations.intSampleNo,
                                                            trn_sampleinfomations.intSampleYear,
                                                            trn_sampleinfomations.intRevisionNo,
                                                            trn_sampleorderdetails.strCombo
                                                        FROM
                                                            trn_sampleinfomations
                                                        INNER JOIN trn_sampleorderheader ON trn_sampleorderheader.intSampleNo = trn_sampleinfomations.intSampleNo
                                                        AND trn_sampleorderheader.intSampleYear = trn_sampleinfomations.intSampleYear
                                                        AND trn_sampleorderheader.intRevNo = trn_sampleinfomations.intRevisionNo
                                                        INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                                                        AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                                                        AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo
                                                        INNER JOIN trn_sampleorderdetails ON trn_sampleorderdetails.strCombo = trn_sampleinfomations_details.strComboName
                                                        AND trn_sampleorderdetails.intSampleOrderNo = trn_sampleorderheader.intSampleOrderNo
                                                        AND trn_sampleorderdetails.intSampleOrderYear = trn_sampleorderheader.intSampleOrderYear
                                                        WHERE
                                                            date(trn_sampleorderheader.dtmCreateDate) BETWEEN ('$dateFrom') AND ('$dateTo')
                                                            AND trn_sampleorderheader.intStatus = '1'
                                                    ) AS sampleInfomationFilter
                                                INNER JOIN sys_users ON sys_users.intUserId = sampleInfomationFilter.intMarketer
                                                INNER JOIN mst_customer ON mst_customer.intId = sampleInfomationFilter.intCustomer
                                                INNER JOIN mst_brand ON mst_brand.intId = sampleInfomationFilter.intBrand
                                                GROUP BY
                                                    Marketer,
                                                    Customer,
                                                    Brand,
                                                    sampleInfomationFilter.strGraphicRefNo,
                                                    sampleInfomationFilter.strCombo
                                                ORDER BY
                                                    Marketer,
                                                    Customer,
                                                    Brand,
                                                    sampleInfomationFilter.strGraphicRefNo,
                                                    sampleInfomationFilter.strCombo
                                            ) AS TB1
                                        GROUP BY
                                            TB1.Marketer,
                                            TB1.Customer,
                                            TB1.Brand
                                    ";
                            $result = $db->RunQuery($sql);

                            while ( $row = mysqli_fetch_array($result)) {
                            $developedCount = $row['Developed_Count'];
                            $hitCount       = $row['Hit'];
                            $hitPresentage  = ($hitCount/$developedCount)*100;
                            ?>
                                <tr class="normalfnt" bgcolor="#d3d3d3">
                                    <td class="normalfnt" NOWRAP style="text-align: center">&nbsp;<?php echo $row['marketer']; ?>&nbsp;</td>
                                    <td class="normalfnt" style="text-align: center">&nbsp;
                                    <?php echo $row['customer']; ?>&nbsp;</td>
                                    <td class="normalfnt" style="text-align: center">&nbsp;
                                    <?php echo $row['brand']; ?></td>
                                    <td class="normalfntRight" style="text-align: center">&nbsp;
                                    <?php echo $developedCount; ?></td>
                                    <td class="normalfntMid">&nbsp;
                                    <?php echo $hitCount; ?></td>
                                    <td class="normalfntMid">
                                        <?php echo number_format((float)$hitPresentage, 2, '.', '');?>
                                    </td>
                                </tr>
                            <?php } ?>   
                                <tr style="height: 40px">
                                    <td colspan="20"></td>
                                </tr>
                                </table>
                            </td>
                            <td width="6%">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
</form>
</body>
</html>


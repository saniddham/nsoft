var basePath	="presentation/customerAndOperation/reports/";

$(document).ready(function(){
	//------------------------
	$('#tblGraphicWiseHead').click(function(){

		headShowHideGraphic('tblGraphicWiseHead','tblGraphicWiseBody');
		bodyShowHideCombo('tblComboWiseHead','tblComboWiseBody');
		bodyShowHideDetail('tblDetailHead','tblDetailBody');
                bodyShowHideDummyDetail('tblDummyPoDetailHead','tblDummyPoDetailBody');

	});
        
        

    $('#tblComboWiseHead').click(function(){
		
		headShowHideCombo('tblComboWiseHead','tblComboWiseBody');
		bodyShowHideGraphic('tblGraphicWiseHead','tblGraphicWiseBody');
		bodyShowHideDetail('tblDetailHead','tblDetailBody');
                bodyShowHideDummyDetail('tblDummyPoDetailHead','tblDummyPoDetailBody');
	});
	//detail
	$('#tblDetailHead').click(function(){

		headShowHideDetail('tblDetailHead','tblDetailBody');
		bodyShowHideGraphic('tblGraphicWiseHead','tblGraphicWiseBody');
		bodyShowHideCombo('tblComboWiseHead','tblComboWiseBody');
                bodyShowHideDummyDetail('tblDummyPoDetailHead','tblDummyPoDetailBody');
	});
        
        $('#tblDummyPoDetailHead').click(function(){
                
                headShowHideDummyDetail('tblDummyPoDetailHead','tblDummyPoDetailBody');
		bodyShowHideGraphic('tblGraphicWiseHead','tblGraphicWiseBody');
		bodyShowHideCombo('tblComboWiseHead','tblComboWiseBody');
                bodyShowHideDetail('tblDetailHead','tblDetailBody');
                
	});
        
        //-----------------------------------------------------

	$('#frmGraphicWise thead').click(function(){

		bodyShowHideGraphic('tblGraphicWiseHead','tblGraphicWiseBody');

	});

	$('#frmComboWise thead').click(function(){

		bodyShowHideCombo('tblComboWiseHead','tblComboWiseBody');

	});
	//detail
	$('#frmDetailHead thead').click(function(){

		bodyShowHideDetail('tblDetailHead','tblDetailBody');

	});
        $('#frmDummyPo thead').click(function(){

		bodyShowHideDummyDetail('tblDummyPoDetailHead','tblDummyPoDetailBody');

	});
//-----------------------------------------------------------------------------------------------
	$('#tblGraphicWiseBody thead').click(function(){

		bodyShowHideGraphic('tblGraphicWiseHead','tblGraphicWiseBody');

	});
	$('#tblComboWiseBody thead').click(function(){

		bodyShowHideCombo('tblComboWiseHead','tblComboWiseBody');

	});

	//detil
	$('#tblDetailBody thead').click(function(){

		bodyShowHideDetail('tblDetailHead','tblDetailBody');

	});
        
        $('#tblDummyPoDetailBody thead').click(function(){

		bodyShowHideDummyDetail('tblDummyPoDetailHead','tblDummyPoDetailBody');

	});
	//-------------------------------------------------------------------
	$('#frmGraphicWise #butNew').click(function(){
		$('#frmGraphicWise')[0].reset();
	});

	$('#frmComboWise #butNew').click(function(){
		$('#frmComboWise')[0].reset();
	});
	//detil
	$('#frmDetail #butNew').click(function(){
		$('#frmDetail')[0].reset();
	});

});

function ViewReportGraphic(formId)
{
	//var reportId	="927";
	if ($('#'+formId).validationEngine('validate'))  {
		var dateFrom = $('#frmGraphicWise #grphicwiseDateFrom').val();
		var dateTo   = $('#frmGraphicWise #grphicwiseDateTo').val();
	var url = "?q=1182&dateFrom="+dateFrom+"&dateTo="+dateTo;
	if(dateFrom && dateTo != ""){
	window.open(url)	
		}
	}
	$('#frmGraphicWise')[0].reset();
}

function ViewReportComboWise(formId)
{
	//var reportId	="927";
	if ($('#'+formId).validationEngine('validate'))  {
		var dateFrom = $('#frmComboWise #combowiseDateFrom').val();
		var dateTo   = $('#frmComboWise #combowiseDateTo').val();
	var url = "?q=1181&dateFrom="+dateFrom+"&dateTo="+dateTo;
	if(dateFrom && dateTo != ""){
	window.open(url)	
		}
	}
	$('#frmComboWise')[0].reset();
}

//detil

function ViewReportDetail(formId)
{
   
	//var reportId	="927";
        //alert("hi")
	if ($('#'+formId).validationEngine('validate'))  {
		var dateFrom = $('#frmDetail #DetailDateFrom').val();
               
		var dateTo   = $('#frmDetail #DetailDateTo').val();
	var url = "?q=1184&dateFrom="+dateFrom+"&dateTo="+dateTo;
       
	if(dateFrom && dateTo != ""){
	window.open(url)	
		}
	}
	$('#frmDetail')[0].reset();
}

function ViewReportDummyOrderDetails(formId)
{
	
	if ($('#'+formId).validationEngine('validate'))  {
		var dateFrom    = $('#frmDummyPo #DummyPODateFrom').val();
		var dateTo      = $('#frmDummyPo #DummyPODateTo').val();
               var firstCheck  	= ($("#frmDummyPo #check1").is(':checked') ) ? 1 : 0;
               
               var secondCheck  = ($("#frmDummyPo #check2").is(':checked') ) ? 2 : 0;
             
               
	var url = "?q=1278&dateFrom="+dateFrom+"&dateTo="+dateTo+"&firstCheck="+firstCheck+"&secondCheck="+secondCheck;
       // alert(url)
	if(dateFrom && dateTo != ""){
	window.open(url)	
		}
	}
	
	$('#frmDummyPo')[0].reset();
}


function headShowHideGraphic(headId,bodyId){
		 $('#'+headId).hide('slow');
		 $('#'+bodyId).toggle();

}
function headShowHideCombo(headId,bodyId){
		 $('#'+headId).hide('slow');
		 $('#'+bodyId).toggle();

}
//dedail
function headShowHideDetail(headId,bodyId){
		 $('#'+headId).hide('slow');
		 $('#'+bodyId).toggle();

}
function headShowHideDummyDetail(headId,bodyId){
		 $('#'+headId).hide('slow');
		 $('#'+bodyId).toggle();

}

function bodyShowHideGraphic(headId,bodyId){

		$('#'+headId).show('slow');
		$('#'+bodyId).hide('slow');
}
function bodyShowHideCombo(headId,bodyId){

		$('#'+headId).show('slow');
		$('#'+bodyId).hide('slow');
}
//detil
function bodyShowHideDetail(headId,bodyId){

		$('#'+headId).show('slow');
		$('#'+bodyId).hide('slow');
}

function bodyShowHideDummyDetail(headId,bodyId){

		$('#'+headId).show('slow');
		$('#'+bodyId).hide('slow');
}

<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	
	/////////// type of print load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
					intId,
					strName
				FROM mst_techniques
				order by strName
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		$sql = "SELECT 	strName,
						intGroupId,
						strRemark,
						intStatus,intRoll,
					mst_techniques.CHANGE_CONPC_AFTER_SAMPLE,
					mst_techniques.RM_USING	
				FROM mst_techniques
				WHERE
					intId =  '$id'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['name'] 						= $row['strName'];
			$response['id'] 						= $id;
			$response['groupId'] 					= $row['intGroupId'];
			$response['remark'] 					= $row['strRemark'];
			$response['status'] 					= ($row['intStatus']?true:false);
			$response['roll'] 						= ($row['intRoll']?true:false);
			$response['CHANGE_CONPC_AFTER_SAMPLE']  = ($row['CHANGE_CONPC_AFTER_SAMPLE']?true:false);
			$response['RM_USING']					= ($row['RM_USING']?true:false);
	
		}
		echo json_encode($response);
	}
	else if($requestType == 'loadTechniqueGrid')
	{
		$techGroupId	= $_REQUEST['techGroupId'];
		$html			= '';
		
		$sqlTech		= " SELECT
							mst_technique_groups.TECHNIQUE_GROUP_NAME
							FROM
							mst_technique_groups
							WHERE
							mst_technique_groups.TECHNIQUE_GROUP_ID = '$techGroupId'";
		$result_tech	= $db->RunQuery($sqlTech);
		$rowTech		= mysqli_fetch_array($result_tech);
		$groupName		= $rowTech['TECHNIQUE_GROUP_NAME'];
		$htmlHead		= "Techniques of ".$groupName;	
		$sql			= " SELECT
							mst_techniques.intId,
							mst_techniques.strName
							FROM
							mst_techniques
							WHERE
							mst_techniques.intStatus <> -2 AND 
							mst_techniques.intGroupId = '$techGroupId'";

		$result = $db->RunQuery($sql);
		if(mysqli_num_rows($result)>0)
		{
			while($row=mysqli_fetch_array($result))
			{
				$html	.= "<tr id=\"".$row['intId']."\"><td>".$row['strName']."</td></tr>";
			}
		}
              
		$response['headerDtl']	= $htmlHead;
		$response['gridDtl']	= $html;
		echo json_encode($response);				
	}
	
?>
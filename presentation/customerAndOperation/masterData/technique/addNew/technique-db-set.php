<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$mainUrl	= $_SESSION['MAIN_URL'];
	
	include "{$backwardseperator}dataAccess/Connector.php";
	include	"../../../../../class/cls_commonFunctions_get.php";
	include	"../../../../../class/cls_commonErrorHandeling_get.php";
	include_once "../../../../../libraries/email_service/mailPoolTable.php";
	$response = array('type'=>'', 'msg'=>'');
	
	$obj_common				= new cls_commonFunctions_get($db);
	$obj_common_error		= new cls_commonErrorHandeling_get($db);
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];//
	$id 			= $_REQUEST['cboSearch'];
	$name			= trim($_REQUEST['txtName']);
	$remark			= trim($_REQUEST['txtRemark']);
	$intRoll		= ($_REQUEST['chkFoil']?1:0);
	$spTech			= (isset($_REQUEST['chkSpecial'])?1:0);
	$rmUsing = (isset($_REQUEST['chkUserRM']) ? 1 : 0);

	$techGroup		= $_REQUEST['cboTechniqueGroup'];	
	$savedStatus	= true;
	$savedMasseged	= '';
	$programCode	= 'P1131';
	
	$approveLevels 	= $obj_common->get_approve_levels($programCode,'RunQuery');
	$status			= $approveLevels+1;
	/////////// type of print insert part /////////////////////
	if($requestType=='add')
	{
		$db->begin();
		$permissionResult		= $obj_common_error->get_permision_withApproval_save($status,$approveLevels,$userId,$programCode,'RunQuery2');	
		if($savedStatus && $permissionResult['type'] == 'fail')
		{
			$savedStatus		= false;
			$savedMasseged	 	= $permissionResult['msg'];
				
		}
				
		$sql 					= "INSERT INTO `mst_techniques` 				(`strName`,`intGroupId`,`strRemark`,`intCreator`,dtmCreateDate,intRoll,intStatus,intApproveLevels,CHANGE_CONPC_AFTER_SAMPLE,RM_USING) 
						VALUES ('$name','$techGroup','$remark','$userId',now(),$intRoll,'$status','$approveLevels',$spTech,$rmUsing)";
		$result = $db->RunQuery($sql);
		
		if($savedStatus && $result){
			$db->commit();
			$response['techId']		= $db->insertId;
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$db->rollback();			
			$response['type'] 		= 'fail';
			$response['msg'] 		= $savedMasseged;
			$response['q'] 			= $sql;
		}
	}
	/////////// type of print update part /////////////////////
	else if($requestType=='edit')
	{
		$db->begin();
		$sqlStatus				= " SELECT
									mst_techniques.intStatus,
									mst_techniques.intApproveLevels,
									mst_techniques.intGroupId
									FROM
									mst_techniques
									WHERE
									mst_techniques.intId = '$id'";
		$resultStatus			= $db->RunQuery2($sqlStatus);
		$rowStatus				= mysqli_fetch_array($resultStatus);
		$techGrpId				= $rowStatus['intGroupId'];
				
		$permissionResult		= $obj_common_error->get_permision_withApproval_save($rowStatus['intStatus'],$rowStatus['intApproveLevels'],$userId,$programCode,'RunQuery2');	
		if($savedStatus && $permissionResult['type'] == 'fail')
		{
			$savedStatus		= false;
			$savedMasseged	 	= $permissionResult['msg'];
				
		}
		
		$sql = "UPDATE `mst_techniques` SET 	strName		='$name',
												intGroupId	='$techGroup',
												strRemark	='$remark',
												intStatus	='$status',
												intApproveLevels	='$approveLevels',
												intModifyer	='$userId',
												intRoll 	= $intRoll,
												CHANGE_CONPC_AFTER_SAMPLE 	= $spTech,
												RM_USING 	= $rmUsing
				WHERE (`intId`='$id')";
		$result = $db->RunQuery2($sql);
		if($savedStatus && !$result)
		{
			$savedStatus			= false;
			$savedMasseged			= $db->errormsg;	
		}
		
		$approvedByUpdate			= updateMaxStatus($id,'RunQuery2');
		if($savedStatus && !$approvedByUpdate)
		{
			$savedStatus			= false;
			$savedMasseged			= $db->errormsg;	
		}		
		
		if($savedStatus){
			$db->commit();
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$db->rollback();
			$response['type'] 		= 'fail';
			$response['msg'] 		= $savedMasseged;
			$response['q'] 			= $sql;
		}
	}
	/////////// type of print delete part /////////////////////
	else if($requestType=='delete')
	{
		$db->begin();
		$sqlStatus				= " SELECT
									mst_techniques.intStatus,
									mst_techniques.intApproveLevels,
									mst_techniques.intGroupId
									FROM
									mst_techniques
									WHERE
									mst_techniques.intId = '$id'";
		$resultStatus			= $db->RunQuery2($sqlStatus);
		$rowStatus				= mysqli_fetch_array($resultStatus);
		$techGrpId				= $rowStatus['intGroupId'];
		$tech_status			= $rowStatus['intStatus'];
		
		if($savedStatus && $tech_status != ($rowStatus['intApproveLevels']+1))
		{
			$savedStatus	= false;
			$savedMasseged	= "This technique is not in pending stage. You can't delete this";
		}
		if($savedStatus)
		{
			$sql_insert				= " INSERT INTO mst_technique_history 
										(TECHNIQUE_ID, 
										TECHNIQUE_GROUP_ID, 
										MODIFIED_BY, 
										MODIFIED_DATE, 
										CHANGE_STATUS
										)
										VALUES
										('$id', 
										'$techGrpId', 
										'$userId', 
										NOW(), 
										'2'
										);";
			$result_insert			= $db->RunQuery2($sql_insert);
			if($savedStatus && !$result_insert)
			{
				$savedStatus	= false;
				$savedMasseged	= $db->errormsg;
			}
		}
		if($savedStatus)
		{
			$sql = "UPDATE mst_techniques 
					SET
					intStatus = '-2' 	
					WHERE
					intId = '$id' ";
			$result = $db->RunQuery2($sql);
			if($savedStatus && !$result)
			{
				$savedStatus	= false;
				$savedMasseged	= $db->errormsg;	
			}
		}
		if($savedStatus){
			$db->commit();
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
			sendTechniqueMail($id,$userId,$mainUrl,'RunQuery');
		}
		else{
			$db->rollback();
			$response['type'] 		= 'fail';
			$response['msg'] 		= $savedMasseged;
			$response['q'] 			= $sql;
		}
	}
	echo json_encode($response);
	
	function updateMaxStatus($id,$executeType)
	{
		global $db;
		
		$sql_max	= "SELECT COALESCE(MAX(STATUS),0)+1 AS MAX_APPROVE_NO 
					   FROM mst_technique_approved_by 
					   WHERE TECHNIQUE_ID = '$id'";
		$result_max	= $db->$executeType($sql_max);
		$row		= mysqli_fetch_array($result_max);
		$maxStatus	= $row['MAX_APPROVE_NO'];
		
		$sql		= " UPDATE mst_technique_approved_by 
						SET
						STATUS = '$maxStatus'
						WHERE
						TECHNIQUE_ID = '$id' AND STATUS = 0";
		
		$result		= $db->$executeType($sql);
		return $result;	
	}


function sendTechniqueMail($techniqueId,$userId,$mainUrl,$executeType)
{
	global $db;
	global $obj_common;
	$chngStatus	= '';
	$techAddUser = '';
	
	$sql_emailEventUser	=  "SELECT
							sys_mail_eventusers.intUserId
							FROM
							sys_mail_eventusers
							WHERE
							sys_mail_eventusers.intMailEventId = '1021' 
							/*AND 
							sys_mail_eventusers.intCompanyId = '$companyId'*/
							";
	$resultMailEvent	= $db->$executeType($sql_emailEventUser);
	$mailEventUserId	= '';
	while($row_eventUser	= mysqli_fetch_array($resultMailEvent))
	{
		$mailEventUserId    .= $row_eventUser['intUserId'].',';	
	}
	$trimUserId		= rtrim($mailEventUserId,',');
		
	$sql		= 	  " SELECT
						mst_techniques.intId,
						mst_techniques.strName,
						mst_technique_history.HISTORY_ID,
						mst_technique_history.TECHNIQUE_ID,
						mst_technique_history.TECHNIQUE_GROUP_ID,
						mst_technique_history.MODIFIED_BY,
						MAX(mst_technique_history.MODIFIED_DATE) AS LAST_MODIFY,
						mst_technique_history.CHANGE_STATUS
						FROM
						mst_techniques
						INNER JOIN mst_technique_history ON mst_techniques.intId = mst_technique_history.TECHNIQUE_ID
						WHERE
						mst_techniques.intId = '$techniqueId'
						ORDER BY
						mst_technique_history.MODIFIED_DATE DESC"	;
				
	$result		= $db->$executeType($sql);
	$row		= mysqli_fetch_array($result);
	if($row['CHANGE_STATUS'] == 1)
		$chngStatus	= 'Edited';
	else if($row['CHANGE_STATUS'] == 2)
		$chngStatus	= 'Deleted';
	else if($row['CHANGE_STATUS'] == '')
		$chngStatus	= 'Added';	

	$techniqueName		= $row['strName'];				
	$url				= $mainUrl."?q=1130&techniqueId=$techniqueId";
	ob_start();
	
	include "../listing/technique_mail_template.php";	

	$body = ob_get_clean();

	//echo $body;

	$FROM_NAME			= "NSOFT - SCREENLINE HOLDINGS";
	$FROM_EMAIL			= "";
	 
	$nowDate = date('Y-m-d');
	
	if($chngStatus == 'Added')
		$mailHeader= "NEW TECHNIQUE ALERT";
	else if($chngStatus == 'Edited')
		$mailHeader= "TECHNIQUE UPDATE ALERT";
	else if($chngStatus == 'Deleted')
		$mailHeader= "TECHNIQUE DELETE ALERT";
	//Original

	$mail_TO	= $obj_common->getEmailList($userId).','.$obj_common->getEmailList($trimUserId);
	$mail_CC	= '';
	$mail_BCC	= '';
	
	insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);

			
	
}
?>
<?php  (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php  
include	"class/cls_commonErrorHandeling_get.php";

$obj_commonErr	= new cls_commonErrorHandeling_get($db);

$intUser  		= $_SESSION["userId"];
$techniqueId	= $_REQUEST['techniqueId'];
$programCode	= 'P1131';

$sql_head		= " SELECT
					mst_techniques.intId,
					mst_techniques.strName,
					mst_techniques.intGroupId,
					mst_techniques.strRemark,
					mst_techniques.intRoll,
					mst_techniques.intStatus,
					mst_techniques.intApproveLevels,
					mst_techniques.intCreator,
					mst_techniques.CHANGE_CONPC_AFTER_SAMPLE,
					mst_techniques.RM_USING					
					FROM
					mst_techniques
					WHERE
					mst_techniques.intId = '$techniqueId'
					";
					
$result_head	= $db->RunQuery($sql_head);
$row_head		= mysqli_fetch_array($result_head);

$permition_arr		= $obj_commonErr->get_permision_withApproval_save($row_head['intStatus'],$row_head['intApproveLevels'],$intUser,$programCode,'RunQuery');
$permision_save		= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_revise($row_head['intStatus'],$row_head['intApproveLevels'],$intUser,$programCode,'RunQuery');
$permision_revise	= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($row_head['intStatus'],$row_head['intApproveLevels'],$intUser,$programCode,'RunQuery');
$permision_confirm	= $permition_arr['permision'];
	
?>
<title>Technique</title>
<form id="frmTechnique" name="frmTypeOfPrint" method="post" action="technique-db-set.php" autocomplete="off">
<div align="center">
		<div class="trans_layoutD">
		  <div class="trans_text">Technique</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="585" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Technique</td>
                <td colspan="3">
                <select name="cboSearch" class="txtbox" id="cboSearch" style="width:250px" tabindex="1">
                   <option value=""></option>
                 <?php  $sql = "SELECT
									intId,
									strName
								FROM mst_techniques 
								WHERE
								mst_techniques.intStatus <> -2
								order by strName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									if($techniqueId == $row['intId'])
										echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";
									else
										echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?> 
                  </select></td>
              </tr>
              <tr>
                <td width="115" class="normalfnt">&nbsp;</td>
                <td width="119" class="normalfnt">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Name&nbsp;<span class="compulsoryRed">*</span></td>
                <td colspan="3"><input name="txtName" type="text" class="validate[required,maxSize[50]]" id="txtName" style="width:250px" maxlength="50" value="<?php echo $row_head['strName']?>" tabindex="2"/><input name="txtTechId" value="<?php echo $techniqueId?>" id="txtTechId" type="text" style="display:none"/></td>
              </tr>
               <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Technique Group&nbsp;<span class="compulsoryRed">*</span></td>
                <td colspan="3"><select name="cboTechniqueGroup" class="txtbox" id="cboTechniqueGroup" style="width:250px" tabindex="1">
                   <option value=""></option>
                 <?php  $sql = "SELECT
								mst_technique_groups.TECHNIQUE_GROUP_ID,
								mst_technique_groups.TECHNIQUE_GROUP_NAME
								FROM
								mst_technique_groups
								WHERE
								mst_technique_groups.`STATUS` = 1
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									if($row_head['intGroupId'] == $row['TECHNIQUE_GROUP_ID'])
										echo "<option selected=\"selected\" value=\"".$row['TECHNIQUE_GROUP_ID']."\">".$row['TECHNIQUE_GROUP_NAME']."</option>";	
									else
										echo "<option value=\"".$row['TECHNIQUE_GROUP_ID']."\">".$row['TECHNIQUE_GROUP_NAME']."</option>";
								}
                   ?> 
                  </select></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Description</td>
                <td><textarea name="txtRemark" style="width:250px" class="validate[maxSize[250]]"  rows="2" id="txtRemark"  tabindex="3"><?php echo $row_head['strRemark']?></textarea></td>
                <td class="normalfnt">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr  style="visibility: hidden">
                <td display=none; class="normalfnt">&nbsp;</td>
                <td  class="normalfnt">Roll</td>
                <td><input type ="text" <?php echo($row_head['intRoll'] == 1?"checked=\"checked\"":'');?> name="chkFoil" id="chkFoil"  tabindex="4"/></td>
                <td class="normalfnt">&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Special Technique</td>
                <td><input type="checkbox" <?php echo($row_head['CHANGE_CONPC_AFTER_SAMPLE'] == 1?"checked=\"checked\"":'');?> name="chkSpecial" id="chkSpecial"  tabindex="4"/></td>
                <td class="normalfnt">&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Use RM</td>
                <td><input type="checkbox" <?php echo($row_head['RM_USING'] == 1?"checked=\"checked\"":'');?> name="chkUserRM" id="chkUserRM"  tabindex="4"/></td>
                <td class="normalfnt">&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td colspan="2" class="normalfnt"><table class="bordered" id="tblMain" style="width:380px;">
               <th id="techniqueHeader" class="normalfnt"></th>
               <tbody id="tbodyDtl"></tbody>
               </table></td>
                
                <td colspan="2"></td>
                </tr>
              </table></td>
            </tr>
            
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
              <td align="center" ><a class="button white medium" id="butNew" name="butNew">New</a><?php echo($permision_save==1?'<a class="button white medium" id="butSave" name="butSave">Save</a>':'');?>
              <?php echo($form_permision['delete']==1?'<a class="button white medium" id="butDelete" name="butDelete">Delete</a>':'');?><a class="button white medium" id="butConfirm" name="butConfirm" <?php echo($permision_confirm!=1?'style="display:none"':''); ?> >Approve</a><a class="button white medium" id="butRevise" name="butRevise" <?php echo($permision_revise!=1?'style="display:none"':''); ?> >Revise</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a>
              
              </td>
              </tr>
            </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
</form>
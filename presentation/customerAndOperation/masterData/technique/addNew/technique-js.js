var basepath	= 'presentation/customerAndOperation/masterData/technique/addNew/';
var menuID		= '';
var reportId	= 1130;

$(document).ready(function() {	
	$("#frmTechnique").validationEngine();
	$('#frmTechnique #txtName').focus();
  	$('#frmTechnique #butSave').die('click').live('click',save);
    $('#frmTechnique #cboSearch').die('change').live('change',searchData);	
	$('#frmTechnique #butNew').die('click').live('click',clearForm);	
    $('#frmTechnique #butDelete').die('click').live('click',deleteData);
	$('#frmTechnique #cboTechniqueGroup').die('change').live('change',function(){loadTechniqueGrid('')});
	$('#frmTechnique #butConfirm').die('click').live('click',approveRpt);
	$('#frmTechnique #butRevise').die('click').live('click',ReviseRpt);
});

function save()
{	
	if(!$('#frmTechnique').validationEngine('validate'))
	{
		var t = setTimeout("alertx('#frmTechnique')",2000);
		return;
	}
	
	showWaiting();
	
	if($("#frmTechnique #cboSearch").val()=='')
		requestType = 'add';
	else
		requestType = 'edit';
	
	var url = basepath+"technique-db-set.php";
	var obj = $.ajax({
		url:url,
		dataType: "json",
		type:'post', 
		data:$("#frmTechnique").serialize()+'&requestType='+requestType,
		async:false,			
		success:function(json){
				$('#frmTechnique #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				if(json.type=='pass')
				{
					$('#frmTechnique #txtTechId').val(json.techId);
					var t=setTimeout("alertx('#frmTechnique #butSave')",2000);
					hideWaiting();
					clearForm();
					return;
				}
				hideWaiting();
				var t = setTimeout("alertx('#frmInkType #butSave')",4000);
			},
		error:function(xhr,status){				
				$('#frmTechnique #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				var t = setTimeout("alertx('#frmInkType #butSave')",4000);
			}		
	});
}

function deleteData()
{	
	if($('#frmTechnique #cboSearch').val()=='')
	{
		$('#frmTechnique #butDelete').validationEngine('showPrompt', 'Please select Type.', 'fail');
		var t=setTimeout("alertx('#frmTechnique #butDelete')",2000);
		return;	
	}
	
	var val = $.prompt('Are you sure you want to delete "'+$('#frmTechnique #cboSearch option:selected').text()+'" ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
				var url = basepath+"technique-db-set.php";
				var httpobj = $.ajax({
					url:url,
					dataType:'json',
					type:'post',
					data:'requestType=delete&cboSearch='+$('#frmTechnique #cboSearch').val(),
					async:false,
					success:function(json){						
						$('#frmTechnique #butDelete').validationEngine('showPrompt', json.msg,json.type);						
						if(json.type=='pass')
						{
							clearForm();							
							var t = setTimeout("alertx('#frmTechnique #butDelete')",2000);return;
						}	
						var t = setTimeout("alertx('#frmTechnique #butDelete')",3000);
					},
					error:function(xhr,status){							
						$('#frmTechnique #butDelete').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t = setTimeout("alertx('#frmTechnique #butDelete')",4000);
					} 
				});
			}
		}
	});		
}
function urlApprove()
{

	var val = $.prompt('Are you sure you want to approve this Invoice ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "controller.php?q="+menuID+window.location.search+'&requestType=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptPettyCashInv #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertxR1()",10000);
									window.location.href = window.location.href;
									if(window.opener.location.pathname=='/nsoft/presentation/finance_new/petty_cash/petty_cash_invoice/petty_cash_invoice_listing.php')
									{
										window.opener.location.reload();//reload listing page
									}
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptPettyCashInv #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertxR1()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}
function loadTechniqueGrid(groupId)
{
	if(groupId != '') 
		var techGroupId	= groupId;
	else
		var techGroupId = $('#frmTechnique #cboTechniqueGroup').val();
	
	var url = basepath+"technique-db-get.php";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		type:'post',
		data:'requestType=loadTechniqueGrid&techGroupId='+techGroupId,
		async:false,
		success:function(json){
			$('#frmTechnique #tblMain').show();
			$('#tblMain #tbodyDtl').html(json.gridDtl);
			$('#tblMain #techniqueHeader').html(json.headerDtl);
		}
	});
}

function searchData()
{	
	$('#frmTechnique').validationEngine('hide');
	
	if($('#frmTechnique #cboSearch').val()=='')
	{
		clearForm();
		return;
	}
	
	var url = basepath+"technique-db-get.php";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		type:'post',
		data:'requestType=loadDetails&id='+$(this).val(),
		async:false,
		success:function(json){
				$('#frmTechnique #txtName').val(json.name);
				$('#frmTechnique #txtTechId').val(json.id);
				$('#frmTechnique #cboTechniqueGroup').val(json.groupId);
				$('#frmTechnique #txtRemark').val(json.remark);
				$('#frmTechnique #chkActive').attr('checked',json.status);
				$('#frmTechnique #chkFoil').attr('checked',json.roll);
				$('#frmTechnique #chkSpecial').attr('checked',json.CHANGE_CONPC_AFTER_SAMPLE);
				$('#frmTechnique #chkUserRM').attr('checked',json.RM_USING);
				loadTechniqueGrid(json.groupId);
		}
	});
}

function clearForm()
{	
	var t = setTimeout("alertx('#frmTechnique')",2000);	
	showWaiting();
	loadCombo_frmTechnique();
	$('#frmTechnique #cboSearch').val('');
	$('#frmTechnique #txtName').val('');
	$('#frmTechnique #txtRemark').val('');
	$('#frmTechnique #cboTechniqueGroup').val('');
	$('#frmTechnique #chkFoil').prop('checked',false);
	$('#frmTechnique #chkActive').prop('checked',true);
	$('#frmTechnique #txtName').focus();
	$('#frmTechnique #tblMain').hide();
	hideWaiting();	
}

function loadCombo_frmTechnique()
{
	var url 	= basepath+"technique-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmTechnique #cboSearch').html(httpobj.responseText);
}
function approveRpt()
{
	if($('#frmTechnique #txtTechId').val()!=''){
		window.open('?q='+reportId+'&techniqueId='+$('#frmTechnique #txtTechId').val()+'&approveMode=Confirm');	
	}
	else{
		alert("There is no Technique to confirm");
	}
}
function ReviseRpt()
{
	if($('#frmTechnique #txtTechId').val()!=''){
		window.open('?q='+reportId+'&techniqueId='+$('#frmTechnique #txtTechId').val()+'&approveMode=Revise');	
	}
	else{
		alert("There is no Technique No to confirm");
	}	
}
function alertx(obj)
{
	$(obj).validationEngine('hide')	;
}
<?php
(define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');
ini_set('display_errors',1);
include	"class/cls_commonErrorHandeling_get.php";

 $obj_commonErr	= new cls_commonErrorHandeling_get($db);


$intUser  		= $_SESSION["userId"];
$locationId		= $_SESSION["CompanyID"];
$companyId		= $_SESSION["headCompanyId"];

$techniqueId 	= $_REQUEST['techniqueId'];
$mode			= $_REQUEST['approveMode'];
$programName	='Purchase Order';
$programCode	='P1131';

/*$poNo = '100003';
$year = '2012';
$approveMode=1;
*/
  $sql = "SELECT
		mst_techniques.strName,
		mst_techniques.strRemark,
		IF(mst_techniques.intRoll=1,'Yes','No') AS roll,
		mst_technique_groups.TECHNIQUE_GROUP_NAME,
		mst_techniques.intStatus AS STATUS,
		mst_techniques.intApproveLevels AS LEVELS,
		mst_techniques.intCreator,
		sys_users.strUserName AS CREATOR, 
		mst_techniques.dtmCreateDate AS CREATED_DATE,
		if(CHANGE_CONPC_AFTER_SAMPLE=1,'Yes','No') as CHANGE_CONPC_AFTER_SAMPLE,
		if(RM_USING=1,'Yes','No') as RM_USING 
		FROM
		mst_techniques
		INNER JOIN sys_users ON mst_techniques.intCreator = sys_users.intUserId
		LEFT JOIN mst_technique_groups ON mst_techniques.intGroupId = mst_technique_groups.TECHNIQUE_GROUP_ID
		WHERE
		mst_techniques.intId = '$techniqueId' ";
		
	 $result = $db->RunQuery($sql);
	 $header_array		=mysqli_fetch_array($result);
	 $technique 		= $header_array['strName'];
	 $remark			= $header_array['strRemark'];
	 $techGroupName		= $header_array['TECHNIQUE_GROUP_NAME'];
	 $roll				= $header_array['roll'];
	 $sp_tech			= $header_array['CHANGE_CONPC_AFTER_SAMPLE'];
	 $rm_use			= $header_array['RM_USING'];
	 
	 
	 $permition_arr		= $obj_commonErr->get_permision_withApproval_revise($header_array['STATUS'],$header_array['LEVELS'],$intUser,$programCode,'RunQuery');
	$permision_revise	= $permition_arr['permision'];
	$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$intUser,$programCode,'RunQuery');
	$permision_confirm	= $permition_arr['permision'];
				 
?>
<head>
<title>Techniques Report</title>
<script type="text/javascript" src="presentation/customerAndOperation/masterData/technique/listing/rpt_techniques_js.js?n=1"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position: absolute;
	left: 329px;
	top: 199px;
	width: 554px;
	height: 222px;
	z-index: 1;
}
#apDiv2 {
	position: absolute;
	left: 449px;
	top: 179px;
	width: 357px;
	height: 183px;
	z-index: 1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
	
?>
<div id="apDiv1"><img width="413" height="219" src="images/pending.png"  /></div>
<?php
}
 
?>

<form id="frmRptTechnique" name="frmRptTechnique" method="post">
<div style="width:700px;padding-left:300px">
	<table width="100%" align="center">
		<tr>
			<td colspan="3"><?php include 'reportHeader.php'?></td>
		</tr>
		<tr>
		  <td colspan="3" style="text-align:center">&nbsp;</td></tr>
		<tr>
		<td colspan="3" style="text-align:center"><strong>TECNIQUES REPORT</strong></td>
		</tr>
		<?php
			include "presentation/report_approve_status_and_buttons.php"
		 ?>
		<tr>
			<td colspan="3">
				<table width="129%" border="0">
					<tr class="normalfnt" height="21">
						<td width="17%"><b>Technique</b></td>
						<td width="1%">:</td>
						<td width="30%"><?php echo $technique?></td>
						<td width="20%"><b>Technique Group</b></td>
						<td width="2%">:</td>
						<td width="30%"><?php echo $techGroupName ?></td>
					</tr>
					<tr class="normalfnt" height="21">
					  <td><b>Special Technique</b></td>
						<td width="1%">:</td>
						<td width="30%"><?php echo $sp_tech?></td>
						<td width="20%"><b>RM Using</b></td>
						<td width="2%">:</td>
						<td width="30%"><?php echo $rm_use ?></td>
					</tr>
					<tr class="normalfnt">

						<td width="17%"><b>Remark</b></td>
						<td width="1%">:</td>
						<td width="30%"><?php echo $remark?></td>
					</tr>
					<tr class="normalfnt" height="100">
						<td width="17%"></td>
						<td width="1%"></td>
						<td width="30%"></td>
					</tr>
				 </table>
			</td>
		</tr>
		<tr>
		<td height="20">
		<?php
				$creator		= $header_array['CREATOR'];
				$createdDate	= $header_array['CREATED_DATE'];
				$sqlA = "SELECT 	
						APPROVE_LEVEL AS intApproveLevelNo, 
						STATUS, 
						APPROVED_BY,
						sys_users.strUserName as UserName, 
						APPROVED_DATE AS dtApprovedDate

						FROM 
						mst_technique_approved_by 
						INNER JOIN sys_users ON mst_technique_approved_by.APPROVED_BY = sys_users.intUserId
						WHERE
						TECHNIQUE_ID = '$techniqueId'
						order by APPROVED_DATE asc";

				$resultA 		= $db->RunQuery($sqlA);
				include "presentation/report_approvedBy_details.php"
		?>
		</td>
		</tr>
		<tr height="20"><td></td></tr>
	</table>
</div>
</form> 
</body>
</html>

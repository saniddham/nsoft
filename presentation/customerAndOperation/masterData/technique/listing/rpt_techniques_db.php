<?php
session_start();
$backwardseperator 		= "../../../../../";
$mainPath 				= $_SESSION['mainPath'];
$userId 				= $_SESSION['userId'];
$companyId 				= $_SESSION['CompanyID'];
$SRdb					= $_SESSION["SRDatabese"] ;
$mainUrl				= $_SESSION['MAIN_URL'];

$requestType 			= $_REQUEST['requestType'];
//$programName			= 'Sample Dispatch';
$programCode			= 'P1131';

include_once "{$backwardseperator}dataAccess/Connector.php";
include_once "../../../../../class/cls_commonFunctions_get.php";
include_once "../../../../../class/cls_commonErrorHandeling_get.php";
include_once "../../../../../libraries/email_service/mailPoolTable.php";

$obj_common				= new cls_commonFunctions_get($db);
$obj_errorHandeling 	= new cls_commonErrorHandeling_get($db);

$savedStatus			= true;
$finalApprove			= false;
$savedMasseged 			= '';
$error_sql				= '';

if($requestType=='approve')
{
	$techniqueId		= $_REQUEST["techniqueId"];
	
	$db->begin();
	
	$validateArr		= validateBeforeApprove($techniqueId,$programCode,$userId);
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateArr["msg"];
	}
	$resultUHSArr		= updateHeaderStatus($techniqueId,'');
	if($resultUHSArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultUHSArr["savedMassege"];
		$error_sql		= $resultUHSArr['error_sql'];	
	}

	$resultAPArr		= approvedData($techniqueId,$userId);
	if($resultAPArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultAPArr["savedMassege"];
		$error_sql		= $resultAPArr['error_sql'];	
	}
	$rowS					= loadHeaderData($techniqueId,'RunQuery2');
	
	if($savedStatus)
	{
		
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg']		= "Approved Successfully.";
		if($rowS['intStatus'] == 1) 
		{
			sendTechniqueMail($techniqueId,$userId,$mainUrl,'RunQuery');
		}
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql']		= $error_sql;
	}
	echo json_encode($response);	
}
else if($requestType=='revise')
{
	$techniqueId			= $_REQUEST["techniqueId"];
	
	$db->begin();
	
	$validateArr		= validateBeforeRevise($techniqueId,$programCode,$userId);
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateArr["msg"];
	}
	$resultHistory		= insertToHistoryTable($techniqueId,$userId);
	if($resultHistory['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultHistory["savedMassege"];
		$error_sql		= $resultHistory['error_sql'];	
	}
	
	$resultUHSArr		= updateHeaderStatus($techniqueId,'-1');
	if($resultUHSArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultUHSArr["savedMassege"];
		$error_sql		= $resultUHSArr['error_sql'];	
	}
	
	$resultAPArr		= approved_by_insert($techniqueId,$userId,'-1');
	if($resultAPArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultAPArr["savedMassege"];
		$error_sql		= $resultAPArr['error_sql'];	
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Revised Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql']		= $error_sql;
	}
	echo json_encode($response);
}	
function validateBeforeApprove($techniqueId,$programCode,$userId)
{
	global $obj_errorHandeling;
	
	$header_arr			= loadHeaderData($techniqueId,'RunQuery2');
	$validateArr 		= $obj_errorHandeling->get_permision_withApproval_confirm($header_arr['intStatus'],$header_arr['intApproveLevels'],$userId,$programCode,'RunQuery2');
	
	return  $validateArr;
}
function loadHeaderData($techniqueId,$executionType)
{
	global $db;
	
	$sql = "SELECT
			mst_techniques.intId,
			mst_techniques.strName,
			mst_techniques.intGroupId,
			mst_techniques.strRemark,
			mst_techniques.intRoll,
			mst_techniques.intStatus,
			mst_techniques.intApproveLevels,
			mst_techniques.intCreator,
			mst_techniques.dtmCreateDate,
			mst_techniques.intModifyer,
			mst_techniques.dtmModifyDate
			FROM
			mst_techniques
			WHERE
			mst_techniques.intId = '$techniqueId'";
	
	$result = $db->$executionType($sql);
	$row	= mysqli_fetch_array($result);
	
	return $row;
}
function updateHeaderStatus($techniqueId,$status)
{
	global $db;
	
	if($status=='') 
		$para 	= 'intStatus-1';
	else
		$para 	= $status;
		
	$sql = "UPDATE mst_techniques 
			SET
			intStatus = $para
			WHERE
			mst_techniques.intId = '$techniqueId'";
	
	$result = $db->RunQuery2($sql);
	if(!$result)
	{
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= $db->errormsg;
		$data['error_sql']		= $sql;
	}
	return $data;
}


function approvedData($techniqueId,$userId)
{
	global $finalApprove;
	
	$header_arr 	= loadHeaderData($techniqueId,'RunQuery2');
	if($header_arr['intStatus']==1)
		$finalApprove = true;
	
	$approval		= $header_arr['intApproveLevels']+1-$header_arr['intStatus'];
	$resultArr		= approved_by_insert($techniqueId,$userId,$approval);
	
	return $resultArr;
}
function approved_by_insert($techniqueId,$userId,$approval)
{
	global $db;
	
	$sql = " INSERT INTO mst_technique_approved_by 
			(TECHNIQUE_ID, 
			APPROVE_LEVEL, 
			STATUS, 
			APPROVED_BY, 
			APPROVED_DATE
			)
			VALUES
			('$techniqueId', 
			'$approval', 
			0, 
			'$userId', 
			NOW()
			)";
		
	$result = $db->RunQuery2($sql);
	if(!$result)
	{
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= $db->errormsg;
		$data['error_sql']		= $sql;
	}
	return $data;
}
function validateBeforeRevise($techniqueId,$programCode,$userId)
{
	global $obj_errorHandeling;
	
	$header_arr			= loadHeaderData($techniqueId,'RunQuery2');
	$validateArr 		= $obj_errorHandeling->get_permision_withApproval_revise($header_arr['intStatus'],$header_arr['intApproveLevels'],$userId,$programCode,'RunQuery2');
	
	return  $validateArr;
}

function insertToHistoryTable($techniqueId,$userId)
{
	global $db;

	$header_arr			= loadHeaderData($techniqueId,'RunQuery2');
	$techGrpId			= $header_arr['intGroupId'];	
	$sql_insert			= " INSERT INTO mst_technique_history 
							(TECHNIQUE_ID, 
							TECHNIQUE_GROUP_ID, 
							MODIFIED_BY, 
							MODIFIED_DATE, 
							CHANGE_STATUS
							)
							VALUES
							('$techniqueId', 
							'$techGrpId', 
							'$userId', 
							NOW(), 
							'1'
							)";
		$result_insert	= $db->RunQuery2($sql_insert);
		if(!$result_insert)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $db->errormsg;
			$data['error_sql']		= $sql;
		}
		
		return $data;
			
}

function sendTechniqueMail($techniqueId,$userId,$mainUrl,$executeType)
{
	global $db;
	global $obj_common;
	
	$chngStatus	= '';
	$techAddUser = '';
	$mailEventUserId	= '';
	
	$sql_emailEventUser	=  "SELECT
							sys_mail_eventusers.intUserId
							FROM
							sys_mail_eventusers
							WHERE
							sys_mail_eventusers.intMailEventId = '1021' 
							/*AND 
							sys_mail_eventusers.intCompanyId = '$companyId'*/
							";
	$resultMailEvent	= $db->$executeType($sql_emailEventUser);
	
	while($row_eventUser	= mysqli_fetch_array($resultMailEvent))
	{
		$mailEventUserId    .= $row_eventUser['intUserId'].',';	
	}
	$trimUserId		= rtrim($mailEventUserId,',');
	$sqlTech_user	= " SELECT
						mst_techniques.intCreator,
						mst_techniques.intModifyer
						FROM
						mst_techniques
						WHERE
						mst_techniques.intId = '$techniqueId'";
	$resultTech_usr	=  $db->$executeType($sqlTech_user);
	$rowTech_user	= mysqli_fetch_array($resultTech_usr);
	if($rowTech_user['intModifyer'] == '')
		$techAddUser	= $rowTech_user['intCreator'];
	else
		$techAddUser	= $rowTech_user['intModifyer'];
		
	$sql		= 	  " SELECT
						mst_techniques.intId,
						mst_techniques.strName,
						mst_technique_history.HISTORY_ID,
						mst_technique_history.TECHNIQUE_ID,
						mst_technique_history.TECHNIQUE_GROUP_ID,
						mst_technique_history.MODIFIED_BY,
						MAX(mst_technique_history.MODIFIED_DATE) AS LAST_MODIFY,
						mst_technique_history.CHANGE_STATUS
						FROM
						mst_techniques
						INNER JOIN mst_technique_history ON mst_techniques.intId = mst_technique_history.TECHNIQUE_ID
						WHERE
						mst_techniques.intId = '$techniqueId'
						ORDER BY
						mst_technique_history.MODIFIED_DATE DESC"	;
				
	$result		= $db->$executeType($sql);
	$row		= mysqli_fetch_array($result);
	if($row['CHANGE_STATUS'] == 1)
		$chngStatus	= 'Edited';
	else if($row['CHANGE_STATUS'] == 2)
		$chngStatus	= 'Delete';
	else if($row['CHANGE_STATUS'] == '')
		$chngStatus	= 'Added';	
	
	$techniqueName		= $row['strName'];					
	$url				= $mainUrl."?q=1130&techniqueId=$techniqueId";
	ob_start();
	include "technique_mail_template.php";	

	$body = ob_get_clean();

	//echo $body;

	$FROM_NAME			= "NSOFT - SCREENLINE HOLDINGS";
	$FROM_EMAIL			= "";
	 
	$nowDate = date('Y-m-d');
	
	if($chngStatus == 'Added')
		$mailHeader= "NEW TECHNIQUE ALERT";
	else if($chngStatus == 'Edited')
		$mailHeader= "TECHNIQUE UPDATE ALERT";
	//Original

	$mail_TO	= $obj_common->getEmailList($techAddUser).','.$obj_common->getEmailList($userId).','.$obj_common->getEmailList($trimUserId);
	$mail_CC	= '';
	$mail_BCC	= '';
	
	insertTable($FROM_EMAIL,$FROM_NAME,$mail_TO,$mailHeader,$body,$mail_CC,$mail_BCC);

			
	
}
?>
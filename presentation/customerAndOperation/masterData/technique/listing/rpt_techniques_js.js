var basePath	= "presentation/customerAndOperation/masterData/technique/listing/";

$(document).ready(function() {

	$('#frmRptTechnique #butRptConfirm').die('click').live('click',ConfirmRpt);
	$('#frmRptTechnique #butRptRevise').die('click').live('click',ReviseRpt);
 
 });
 
 
 
 function ConfirmRpt(){
	 
	 
	var val = $.prompt('Are you sure you want to approve this Technique ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"rpt_techniques_db.php"+window.location.search+'&requestType=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptTechnique #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									//var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptTechnique #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								//var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
					   hideWaiting();
				}});
  }
  
  
  function ReviseRpt(){
	  
	var val = $.prompt('Are you sure you want to revise this Technique ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
				showWaiting();
				var url = basePath+"rpt_techniques_db.php"+window.location.search+'&requestType=revise';
				var obj = $.ajax({
					url:url,
					type:'post',
					dataType: "json",  
					data:'',
					async:false,
					
					success:function(json){
							$('#frmRptTechnique #butRptRevise').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							if(json.type=='pass')
							{
								//var t=setTimeout("alertx()",1000);
								window.location.href = window.location.href;
								window.opener.location.reload();//reload listing page
								return;
							}
						},
					error:function(xhr,status){
							
							$('#frmRptTechnique #butRptRevise').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							//var t=setTimeout("alertx()",3000);
						}		
					});
				////////////
					}
					hideWaiting();
			}});
	  
  }

 


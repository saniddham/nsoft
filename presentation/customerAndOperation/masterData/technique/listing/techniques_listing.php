<?php
require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$intUser  			= $_SESSION["userId"];
$programCode		= "P1131";
$reportID			= 1130;
$formID				= 1131;

$approveLevel = (int)getMaxApproveLevel();
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];

$where_string = '';
$where_array = array(
				'Status'=>'MT.intStatus',
				'TECHNIQUE_ID'=>"CONCAT(MT.intId)",	
				'DATE'=>'MT.DATE'
					);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2');
if($arr)
{
	foreach($arr as $k=>$v)
	{
		if($v['field']=='Status')
		{
			if($arr_status[$v['data']]==2)
				$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
			else
				$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
		}
		else if($where_array[$v['field']])
			$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
	}
}
if(!count($arr)>0)					 
	$where_string .= "AND MT.DATE = '".date('Y-m-d')."'";

$sql = "SELECT SUB_1.* FROM
			(SELECT
			  MT.intId																AS TECHNIQUE_ID,
			  MT.strName															AS TECHNIQUE_NAME,
			  MT.intGroupId															AS GROUP_ID, 
			  DATE(MT.dtmCreateDate)												AS DATE,
			  MT.strRemark															AS REMARK,
			  if(MT.CHANGE_CONPC_AFTER_SAMPLE,'YES','NO') 							AS SP_TECH,
			  if(MT.RM_USING,'YES','NO') 							AS RM_USE,
			  IF(MT.intStatus=1,'Approved',IF(MT.intStatus=-2,'Deleted',IF(MT.intStatus=-1,'Revised','Pending'))) AS Status,
			  (SELECT
			  MTG.TECHNIQUE_GROUP_NAME
			  FROM
			  mst_technique_groups MTG
			  WHERE MT.intGroupId = MTG.TECHNIQUE_GROUP_ID)							AS GROUP_NAME, ";
				  
		$sql .= "IFNULL((
				SELECT
				concat(U.strUserName,'(',max(MTA.APPROVED_DATE),')' )
				FROM
				mst_technique_approved_by MTA	
				Inner Join sys_users U
					ON MTA.APPROVED_BY = U.intUserId
				WHERE
				MTA.TECHNIQUE_ID  = MT.intId AND
				MTA.APPROVE_LEVEL =  '1' AND
				MTA.STATUS =  '0'
				),IF(((SELECT
				MP.int1Approval 
				FROM menupermision MP
				Inner Join menus M ON MP.intMenuId = M.intId
				WHERE
				M.strCode = '$programCode' AND
				MP.intUserId =  '$intUser')=1 AND MT.intStatus>1),'Approve', '')) as `1st_Approval`, ";
				
	for($i=2; $i<=$approveLevel; $i++){		
		if($i==2){
			$approval	= "2nd_Approval";
		}
		else if($i==3){
			$approval	= "3rd_Approval";
		}
		else {
			$approval	= $i."th_Approval";
		}
		
		
		$sql .= "IFNULL(
		(
		SELECT
		concat(U.strUserName,'(',max(MTA.APPROVED_DATE),')' )
		FROM
		mst_technique_approved_by MTA
		Inner Join sys_users U ON MTA.APPROVED_BY = U.intUserId
		WHERE
		MTA.TECHNIQUE_ID  = MT.intId AND
		MTA.APPROVE_LEVEL =  '$i' AND
		MTA.STATUS =  '0' 
		),
		IF(
		((SELECT
		MP.int".$i."Approval 
		FROM menupermision MP
		Inner Join menus M 
			ON MP.intMenuId = M.intId
		WHERE
			M.strCode = '$programCode' AND
			MP.intUserId =  '$intUser')=1 AND (MT.intStatus>1) AND (MT.intStatus<=MT.intApproveLevels) AND ((SELECT
		concat(U.strUserName )
		FROM
		mst_technique_approved_by MTA
		Inner Join sys_users U ON MTA.APPROVED_BY = U.intUserId
		WHERE
		MTA.TECHNIQUE_ID = MT.intId AND
		MTA.APPROVE_LEVEL = ($i-1) AND 
		MTA.STATUS = '0' )<>'')),		
		'Approve',
		if($i>MT.intApproveLevels,'-----',''))
		
		) as `".$approval."`, "; 
		
		}
	
	
	$sql .= "IFNULL((SELECT
			concat(sys_users.strUserName,'(',max(mst_technique_approved_by.APPROVED_DATE),')' )
			FROM
			mst_technique_approved_by
			Inner Join sys_users ON mst_technique_approved_by.APPROVED_BY = sys_users.intUserId
			WHERE
			mst_technique_approved_by.TECHNIQUE_ID  = MT.intId AND
			mst_technique_approved_by.APPROVE_LEVEL =  '-1' AND
			mst_technique_approved_by.STATUS =  '0'
			),IF(((SELECT
			menupermision.intRevise 
			FROM menupermision 
			Inner Join menus ON menupermision.intMenuId = menus.intId
			WHERE
			menus.strCode = '$programCode' AND
			menupermision.intUserId =  '$intUser')=1 AND 
			MT.intStatus=1),'Revise', '')) as `Revise`,";
					
				  
				  
	$sql .= " 'View'											AS VIEW,
			  'Report'											AS REPORT
			  FROM mst_techniques MT
			  			  
		)  
		AS SUB_1 WHERE 1=1";
//echo $sql;

$jq		= new jqgrid('',$db);
$col	= array();

$col["title"] 			= "Status";
$col["name"] 			= "Status";
$col["width"] 			= "2"; 						// not specifying width will expand to fill space
$col["align"] 			= "left";
$col["sortable"] 		= true; 						// this column is not sortable
$col["search"] 			= true; 						// this column is not searchable
$col["editable"] 		= false;
$col["stype"] 			= "select";
$str 					= "Pending:Pending;Approved:Approved;Deleted:Deleted;Revised:Revised;:All";  // all row, blank row, then all db values. ; is separator
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	
$col					= NULL;

$col["title"]			= "Serial No";
$col["name"]			= "TECHNIQUE_ID";
$col["width"]			= "1";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$col["hidden"]			= true;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Technique";
$col["name"]			= "TECHNIQUE_NAME";
$col["width"]			= "3";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$col["link"]			= '?q='.$formID.'&techniqueId={TECHNIQUE_ID}';
$col["linkoptions"]		= "target='gate_pass_return.php'";
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Technique Group";
$col["name"]			= "GROUP_NAME";
$col["width"]			= "3";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Special Technique";
$col["name"]			= "SP_TECH";
$col["width"]			= "3";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "RM Using";
$col["name"]			= "RM_USE";
$col["width"]			= "3";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Remark";
$col["name"]			= "REMARK";
$col["width"]			= "2";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$cols[]			    	= $col;
$col					= NULL;

//BEGIN - FIRST APPROVAL {
$col["title"] 			= "1st Approval";
$col["name"] 			= "1st_Approval";
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '?q='.$reportID.'&techniqueId={TECHNIQUE_ID}&approveMode=Confirm';
$col["linkoptions"] 	= "target='rptGatepassReturned.php'";
$col['linkName']		= 'Approve';
$cols[] 				= $col;	
$col					= NULL;
//END	- FIRST APPROVEL }

//BEGIN - SECOND & MORE APPROVAL {
for($i=2; $i<=$approveLevel; $i++)
{
	if($i==2){
		$ap		= "2nd Approval";
		$ap1	= "2nd_Approval";
	}
	else if($i==3){
		$ap		= "3rd Approval";
		$ap1	= "3rd_Approval";
	}
	else {
		$ap		= $i."th Approval";
		$ap1	= $i."th_Approval";
	}

$col["title"] 			= $ap; // caption of column
$col["name"] 			= $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '?q='.$reportID.'&techniqueId={TECHNIQUE_ID}&approveMode=Confirm';
$col["linkoptions"] 	= "target='rptGatepassReturned.php'";
$col['linkName']		= 'Approve';
$cols[] 				= $col;	
$col					= NULL;
}

$col["title"] 			= "Revise"; // caption of column
$col["name"] 			= "Revise"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '?q='.$reportID.'&techniqueId={TECHNIQUE_ID}&approveMode=Revise';
$col["linkoptions"] 	= "target='rptGatepassReturned.php'";
$col['linkName']		= 'Revise';
$cols[] 				= $col;	
$col					= NULL;
$d	=date('Y-m-d');
$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"},{"field":"DATE","op":"eq","data":"$d"}
     ]

}
SEARCH_JSON;

$grid["postData"] = array("filters" => $sarr ); 

$grid["caption"]		= "Technique List";
$grid["multiselect"]	= false;
$grid["rowNum"]			= 20;
/*$grid["sortname"]		= "GATEPASS_NO";
$grid["sortorder"]		= "DESC";*/
$grid["autowidth"]		= true;
$grid["multiselect"]	= false;
$grid["search"]			= true;
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$jq->set_options($grid);
$jq->select_command=$sql;
$jq->set_columns($cols);
$jq->set_actions(array(
	"add"=>false,
	"edit"=>false,
	"delete"=>false,	
	"rowactions"=>false,
	"search"=>"advance",
	"export"=>true));
	
$out	= $jq->render('list1');

?>
<title>Technique List</title>

<form id="returnListing" name="returnListing" action="" method="post">
           	<div align="center" style="margin:10px;">
                	<?php echo $out ?>
                </div>
</form>

<?php
function getMaxApproveLevel()
{
	global $db;
	$appLevel = 0;
	
	$sqlp = "SELECT
			MAX(mst_technique_approved_by.APPROVE_LEVEL) AS appLevel
			FROM
			mst_technique_approved_by
			";					
				
	$resultp 	= $db->RunQuery($sqlp);
	$rowp		= mysqli_fetch_array($resultp);			 
	return $rowp['appLevel'];
}
?>

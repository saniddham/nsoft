<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];
	$code			= trim($_REQUEST['txtCode']);
	$name			= trim($_REQUEST['txtName']);
	$remark			= trim($_REQUEST['txtRemark']);
	$intStatus		= ($_REQUEST['chkActive']?1:0);	
	
	/////////// location insert part /////////////////////
	if($requestType=='add')
	{
		
		$sql_c 		= "SELECT count(mst_colors.strName) as cnt,
						group_concat(mst_colors.strName) as colors 
						FROM 
						mst_colors
						WHERE
						REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE('$name',' ',''),'.',''),'-',''),'/',''),')',''),'|',''),'+',''),'_',''),'&',''),'%',''),',',''),'*',''),'=',''),':',''),';',''),'`',''),'~','') 
						= 
						REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(mst_colors.strName,' ',''),'.',''),'-',''),'/',''),')',''),'|',''),'+',''),'_',''),'&',''),'%',''),',',''),'*',''),'=',''),':',''),';',''),'`',''),'~','') 
						order by mst_colors.strName asc
						";
		$result_c 		=$db->RunQuery($sql_c);
		$row_c			=mysqli_fetch_array($result_c);
		//print_r($row_c);
		//die();
		$num_of_saved	=$row_c['cnt'];
		$cols_saved		=$row_c['colors'];
		
		if($num_of_saved==0){
			$sql = "INSERT INTO `mst_colors` (`strCode`,`strName`,`strRemark`,`intCreator`,dtmCreateDate,intStatus) 
					VALUES ('$code','$name','$remark','$userId',now(),'$intStatus')";
			$result = $db->RunQuery($sql);
			if($result){
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Saved successfully.';
			}
		}
		else{
			if($num_of_saved>0){
				$response['type'] 		= 'fail';
				$response['msg'] 		= "The colour, '".$cols_saved."' is already existing.";
				$response['q'] 			= $sql;
			}
			else{
				$response['type'] 		= 'fail';
				$response['msg'] 		= $db->errormsg;
				$response['q'] 			= $sql;
			}
		}
	}
	/////////// location update part /////////////////////
	else if($requestType=='edit')
	{
		$sql = "UPDATE `mst_colors` SET 	strCode		='$code',
											strName		='$name',
											strRemark	='$remark',
											intStatus	='$intStatus',
											intModifyer	='$userId'
				WHERE (`intId`='$id')";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// location delete part /////////////////////
	else if($requestType=='delete')
	{
		$sql = "DELETE FROM `mst_colors` WHERE (`intId`='$id')  ";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
?>
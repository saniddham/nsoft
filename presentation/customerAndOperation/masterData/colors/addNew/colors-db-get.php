<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	
	/////////// location load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
					intId,
					strName
				FROM mst_colors
				order by strName
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		$sql = "SELECT
					strCode,
					strName,
					strRemark,
					intStatus
				FROM mst_colors
				WHERE
					intId =  '$id'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['code'] 	= $row['strCode'];
			$response['name'] 	= $row['strName'];
			$response['remark'] = $row['strRemark'];
			$response['status'] = ($row['intStatus']?true:false);
		}
		echo json_encode($response);
	}
	elseif($requestType=='URLgetNextCode')
	{
		$sql = "SELECT MAX(intId) AS colorId FROM mst_colors order by colorId DESC";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['colorId'] = $row['colorId'] + 1000;
		}

		echo json_encode($response);
	}
?>
var basepath	= 'presentation/customerAndOperation/masterData/marketer/';
// JavaScript Document
$(document).ready(function() {
	
	//$("#frmProductionAllocation").validationEngine();
	setRemoveRow();
	$('#butAdd').die('click').live('click',function(){
		
		popupWindow3('1');
		$('#popupContact1').html('');
		$('#popupContact1').load(basepath+'popup.php',function(){
			
			$('#frmPopUp #popButAdd').click(function(){
				//addEmailsToGrid(mainRowId,typeRowId,cellId);
				$('.chkUsers:checked').each(function(){
					var fromId = $(this).val();
					var found = false;
					$('.clsMarketer').each(function(){
						if(fromId ==$(this).parent().attr('id'))
						{
							found = true;
						}	
					});
					if(found==false)
					{
						var markMangCombo=getMarketerManagersCombo();
						//var clusterCombo=getClusterCombo();
						document.getElementById('tblMain').insertRow(document.getElementById('tblMain').rows.length);
						document.getElementById('tblMain').rows[document.getElementById('tblMain').rows.length-1].innerHTML = "				<td bgcolor=\"#FFFFFF\" class=\"normalfnt\">"+$(this).parent().parent().find('td').eq(1).html()+"</td>"+
						"<td bgcolor=\"#FFFFFF\" class=\"clsMarketer normalfnt\">"+markMangCombo+"</td>"+
						"<td bgcolor=\"#FFFFFF\" class=\"normalfnt\" style=\"text-align:center\"><input name=\"chkOther\" id =\"chkOther\" type=\"checkbox\" /></td>"+
						"<td bgcolor=\"#FFFFFF\" class=\"normalfnt\" style=\"text-align:center\"><input name=\"chkStatus\" id =\"chkStatus\" type=\"checkbox\" checked=\"checked\"/></td>"+
						"<td bgcolor=\"#FFFFFF\" class=\"normalfnt\" style=\"text-align:center\"><img src=\"images/add_new.png\" width=\"15\" height=\"15\" class=\"clsAddSizes mouseover\"/></td>";
						document.getElementById('tblMain').rows[document.getElementById('tblMain').rows.length-1].id  = fromId;	
						setRemoveRow();	
					}
				});
				disablePopup();
				return;
				
			});
			
			$('#frmPopUp #butClose1').click(disablePopup);
			
		});	
		
	});
	$('#butSave').die('click').live('click',function(){
	if($('#frmMarketer').validationEngine('validate')){
		var value="[ ";
		$('.clsMarketer').each(function(){
			
			var userId	= $(this).parent().attr('id');
			var manager	= $(this).parent().find("#cboMarketerManager").val();
			var status	= ($(this).parent().find("#chkStatus").attr('checked')?1:0);
			var other	= ($(this).parent().find('#chkOther').attr('checked')?1:0);
			
			if(manager==''){
				alert("Please enter marketing manager");
				return false;
			}
			value +='{"marketerId":"'+userId+'","manager":"'+manager+'","status":"'+status+'","other":"'+other+'"},' ;	
			//value +='{"manager":"'+manager+'"},' ;	
		})
		value = value.substr(0,value.length-1);
		value += " ]";
		
		var url = basepath+"marketer-db-set.php?requestType=saveData";
					$.ajax({
							url:url,
							async:false,
							dataType:'json',
							type:'post',
							data:'&userDetails='+value,
						success:function(json){
							$('#frmMarketer #butSave').validationEngine('showPrompt', json.msg,json.type );
							if(json.type=='pass')
							{
								var t=setTimeout("alertx()",1000);
								//setTimeout("window.location.reload();",1000)
								return;
							}
						},
						error:function(xhr,status){
							
							$('#frmMarketer #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",1000);
							return;
						}
					});
		}
	});
	
	$('#Save').die('click').live('click',function() //suvini
	{
		
			var marketerId	= $(".marketer").attr('id');
			
			if ($("#cluster1").is(':checked') ) 
			{
			var cluster1 = $('#cluster1').val();
			
			}
			
			if ($("#cluster2").is(':checked') ) 
			{
			var cluster2 = $('#cluster2').val();
			
			}
			
			if ($("#cluster3").is(':checked') ) 
			{
			var cluster3 = $('#cluster3').val();
			
			}
	
	
	
	 	var url = basepath+"marketer-db-set.php?requestType=saveData_cluster&marketerId="+marketerId+"&cluster1="+cluster1+"&cluster2="+cluster2+"&cluster3="+cluster3;
		
				$.ajax({
							url:url,
							async:false,
							dataType:'json',
							type:'post',
							//data:'&userDetails='+value,
							success:function(json){
							$('#Save').validationEngine('showPrompt', json.msg,json.type );
							if(json.type=='pass')
							{
								var t=setTimeout("alertx()",1000);
								//setTimeout("window.location.reload();",1000)
								return;
							}
						},
						error:function(xhr,status){
							
							$('#Save').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",1000);
							return;
						}
						
					});
	});
	
	$('#butClose1').die('click').live('click',disablePopup); 	//suvini
	//----------------------------------------------------------------- 
	$('.clsAddSizes').die('click').live('click',function()
	{
		var row=this.parentNode.parentNode.rowIndex;
		var markter_id= $(this).parent().parent().attr('id');
		
		popupWindow3('1');

		$('#popupContact1').load(basepath+'marketer_cluster_assign_Popup.php?markter_id='+markter_id,function(){
				$('#butAdd').click(addClickedRows);
				$('#butClose1').click(disablePopup);
				
			});	
			
			
	});
	
	
	
//------------------------------------------------------------------
	
	
	
	
	
 });

function setRemoveRow()
{
	$('.removeRow').click(function(){
		 $(this).parent().parent().remove();
		 var marketerId = $(this).parent().parent().attr('id');
		  var url = basepath+"marketer-db-set.php?requestType=deleteData";
				$.ajax({
						url:url,
						async:false,
						dataType:'json',
						type:'post',
						data:'&marketerId='+marketerId,
					success:function(json){
						$('#frmMarketer .removeRow').validationEngine('showPrompt', json.msg,json.type );
						if(json.type=='pass')
						{
							var t=setTimeout("alertD()",1000);
							return;
						}
					},
					error:function(xhr,status){
						
						$('#frmMarketer .removeRow').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertD()",1000);
						return;
					}
				});
	});	
}
function alertx()
{
	$('#frmMarketer #butSave').validationEngine('hide')	;
}
function alertD()
{
	$('#frmMarketer .removeRow').validationEngine('hide')	;
}
function getMarketerManagersCombo(){
	var url 	= basepath+"marketer-db-get.php?requestType=loadMarketerManagersCombo";
	var httpobj = $.ajax({url:url,async:false})
	return httpobj.responseText;
}
function getClusterCombo(){
	var url 	= basepath+"marketer-db-get.php?requestType=loadClusterCombo";
	var httpobj = $.ajax({url:url,async:false})
	return httpobj.responseText;
}


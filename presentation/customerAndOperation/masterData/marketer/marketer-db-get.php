<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	
	/////////// location load part /////////////////////
	if($requestType=='loadMarketerManagersCombo')
	{
					$sqlm = "SELECT
							mst_marketer_manager.intMarketingManagerId,
							mst_marketer_manager.strName
							FROM `mst_marketer_manager`
							WHERE
							mst_marketer_manager.intStatus = 1 
							";
					$resultm = $db->RunQuery($sqlm);
					$html = '<select style="width:100%" name="cboMarketerManager" id="cboMarketerManager"   class="validate[required]">';
					$html .= "<option value=\"\"></option>";
					while($rowm=mysqli_fetch_array($resultm))
					{
						$html.= "<option value=\"".$rowm['intMarketingManagerId']."\">".$rowm['strName']."</option>";	
					}
					$html.= "</select>";
		echo $html;
	}
	else if($requestType=='loadClusterCombo')
	{
					$sqlm = "SELECT
 							sys_report_cluster_types.CLUSTER_TYPE,
							sys_report_cluster_types.`STATUS`,
							sys_report_cluster_types.`TO`,
							sys_report_cluster_types.CC,
							sys_report_cluster_types.BCC,
							sys_report_cluster_types.`NO`
							FROM `sys_report_cluster_types`
							WHERE
							sys_report_cluster_types.REPORT_TYPE = 'SALES_PROJECTION' AND
							sys_report_cluster_types.`STATUS` = 1
							ORDER BY
							sys_report_cluster_types.CLUSTER_TYPE ASC 
							";
					$resultm = $db->RunQuery($sqlm);
					$html = '<select style="width:100%" name="cboCluster" id="cboCluster"   class="validate[required]">';
					$html .= "<option value=\"\"></option>";
					while($rowm=mysqli_fetch_array($resultm))
					{
						$html.= "<option value=\"".$rowm['NO']."\">".$rowm['CLUSTER_TYPE']."</option>";	
					}
					$html.= "</select>";
		echo $html;
	}
?>
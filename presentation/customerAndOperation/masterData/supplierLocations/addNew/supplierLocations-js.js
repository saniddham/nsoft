var basePath	= "presentation/customerAndOperation/masterData/supplierLocations/addNew/";
			
$(document).ready(function() {
  		$("#frmSupplierLocations").validationEngine();
		$('#frmSupplierLocations #txtName').focus();
  //permision for add 
  /*if(intAddx)
  {
 	$('#frmSupplierLocations #butNew').show();
	$('#frmSupplierLocations #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmSupplierLocations #butSave').show();
	$('#frmSupplierLocations #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmSupplierLocations #butDelete').show();
	$('#frmSupplierLocations #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmSupplierLocations #cboSearch').removeAttr('disabled');
  }*/
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

  ///save button click event
  $('#frmSupplierLocations #butSave').die('click').live('click',function(){
	//$('#frmSupplierLocations').submit();
	var requestType = '';
	if ($('#frmSupplierLocations').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		showWaiting();
		if($("#frmSupplierLocations #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = basePath+"supplierLocations-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			type:'POST',
			data:$("#frmSupplierLocations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmSupplierLocations #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmSupplierLocations').get(0).reset();
						loadCombo_frmSupplierLocations();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmSupplierLocations #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
		hideWaiting();
	}
   });
   
   /////////////////////////////////////////////////////
   //// load supplier locations details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmSupplierLocations #cboSearch').die('click').live('click',function(){
	   $('#frmSupplierLocations').validationEngine('hide');
   });
    $('#frmSupplierLocations #cboSearch').die('change').live('change',function(){
		$('#frmSupplierLocations').validationEngine('hide');
		var url = basePath+"supplierLocations-db-get.php";
		if($('#frmSupplierLocations #cboSearch').val()=='')
		{
			$('#frmSupplierLocations').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmSupplierLocations #txtName').val(json.name);
					$('#frmSupplierLocations #txtRemark').val(json.remark);
					$('#frmSupplierLocations #chkActive').attr('checked',json.status);
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmSupplierLocations #butNew').die('click').live('click',function(){
		$('#frmSupplierLocations').get(0).reset();
		loadCombo_frmSupplierLocations();
		$('#frmSupplierLocations #txtName').focus();
	});
    $('#frmSupplierLocations #butDelete').die('click').live('click',function(){
		if($('#frmSupplierLocations #cboSearch').val()=='')
		{
			$('#frmSupplierLocations #butDelete').validationEngine('showPrompt', 'Please select Locations.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmSupplierLocations #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basePath+"supplierLocations-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'POST',
											data:'requestType=delete&cboSearch='+$('#frmSupplierLocations #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmSupplierLocations #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmSupplierLocations').get(0).reset();
													loadCombo_frmSupplierLocations();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});


function loadCombo_frmSupplierLocations()
{
	var url 	= basePath+"supplierLocations-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmSupplierLocations #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmSupplierLocations #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmSupplierLocations #butDelete').validationEngine('hide')	;
}

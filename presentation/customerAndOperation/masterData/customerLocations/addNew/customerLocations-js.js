var basepath	= 'presentation/customerAndOperation/masterData/customerLocations/addNew/';
			
$(document).ready(function() {
  		$("#frmCustomerLocations").validationEngine();
		$('#frmCustomerLocations #txtName').focus();

  ///save button click event
  $('#frmCustomerLocations #butSave').click(function(){
	//$('#frmCustomerLocations').submit();
	var requestType = '';
	if ($('#frmCustomerLocations').validationEngine('validate'))   
    { 
		showWaiting();
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmCustomerLocations #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = basepath+"customerLocations-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			type:'POST',
			data:$("#frmCustomerLocations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmCustomerLocations #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmCustomerLocations').get(0).reset();
						loadCombo_frmCustomerLocations();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmCustomerLocations #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
		hideWaiting();
	}
   });
   
   /////////////////////////////////////////////////////
   //// load customer locations details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmCustomerLocations #cboSearch').click(function(){
	   $('#frmCustomerLocations').validationEngine('hide');
   });
    $('#frmCustomerLocations #cboSearch').change(function(){
		$('#frmCustomerLocations').validationEngine('hide');
		var url = basepath+"customerLocations-db-get.php";
		if($('#frmCustomerLocations #cboSearch').val()=='')
		{
			$('#frmCustomerLocations').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmCustomerLocations #txtName').val(json.name);
					$('#frmCustomerLocations #txtAddress').val(json.address);
					$('#frmCustomerLocations #txtRemark').val(json.remark);
					$('#frmCustomerLocations #chkActive').attr('checked',json.status);
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmCustomerLocations #butNew').click(function(){
		$('#frmCustomerLocations').get(0).reset();
		loadCombo_frmCustomerLocations();
		$('#frmCustomerLocations #txtName').focus();
	});
    $('#frmCustomerLocations #butDelete').click(function(){
		if($('#frmCustomerLocations #cboSearch').val()=='')
		{
			$('#frmCustomerLocations #butDelete').validationEngine('showPrompt', 'Please select Locations.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmCustomerLocations #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basepath+"customerLocations-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'post',
											data:'requestType=delete&cboSearch='+$('#frmCustomerLocations #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmCustomerLocations #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmCustomerLocations').get(0).reset();
													loadCombo_frmCustomerLocations();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});


function loadCombo_frmCustomerLocations()
{
	var url 	= basepath+"customerLocations-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmCustomerLocations #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmCustomerLocations #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmCustomerLocations #butDelete').validationEngine('hide')	;
}

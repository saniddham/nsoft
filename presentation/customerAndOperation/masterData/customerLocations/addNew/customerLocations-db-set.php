<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];
    $address		= trim($_REQUEST['txtAddress']);
	$name			= trim($_REQUEST['txtName']);
	$remark			= trim($_REQUEST['txtRemark']);
	$intStatus		= ($_REQUEST['chkActive']?1:0);	
	
	/////////// customer locations insert part /////////////////////
	if($requestType=='add')
	{
		$sql = "INSERT INTO `mst_customer_locations_header` (`strName`,`ADDRESS`,`strRemark`,`intCreator`,dtmCreateDate,intStatus) 
				VALUES ('$name','$address','$remark','$userId',now(),'$intStatus')";
		$result = $db->RunQuery($sql);
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	/////////// customer locations update part /////////////////////
	else if($requestType=='edit')
	{
		$sql = "UPDATE `mst_customer_locations_header` SET 	strName		='$name',
												ADDRESS	='$address',
												strRemark	='$remark',
												intStatus	='$intStatus',
												intModifyer	='$userId'
				WHERE (`intId`='$id')";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// customer locations delete part /////////////////////
	else if($requestType=='delete')
	{
		$sql = "DELETE FROM `mst_customer_locations_header` WHERE (`intId`='$id')  ";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
?>
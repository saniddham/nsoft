<?php 
session_start();
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 	 		= $_SESSION['userId'];
$requestType 		= $_REQUEST['requestType'];

include "{$backwardseperator}dataAccess/Connector.php";

if($requestType=='loadData')
{
	$customer = $_REQUEST['customer'];
	$sql = "SELECT  distinct mst_brand.strName as brandName, mst_brand.intId as brandId FROM mst_customer_brand LEFT JOIN mst_brand ON mst_brand.intId =   mst_customer_brand.intBrandId WHERE mst_customer_brand.intCustomerId='$customer'";
    $brands = "<option value=\"\"></option>";
    $c_locations = "<option value=\"\"></option>";
    $result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
        $brands .= "<option value=\"".$row['brandId']."\">".$row['brandName']."</option>";
	}

    $sql = "SELECT distinct mst_customer_locations_header.strName as locationName, mst_customer_locations_header.intId as locationId FROM mst_customer_locations_header LEFT JOIN mst_customer_locations ON mst_customer_locations.intLocationId =   mst_customer_locations_header.intId WHERE mst_customer_locations.intCustomerId ='$customer'";
    $result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
    {
        $c_locations .= "<option value=\"".$row['locationId']."\">".$row['locationName']."</option>";
    }

	$response['brands']  = $brands;
    $response['locations']  = $c_locations;
	echo json_encode($response);
} else if ($requestType=='loadEmails') {
        $customer = $_REQUEST['customer'];
        $brand = $_REQUEST['brand'];
        $location = $_REQUEST['location'];

        $response['email'] = "";
        $response['ccEmail'] = "";
        $sql = "SELECT EMAIL, EMAIL_CC,SEND_PLANT_ASWELL FROM mst_customer_brand_wise_emails WHERE  CUSTOMER_ID='$customer' AND LOCATION_ID = '$location' AND BRAND_ID='$brand'";
        $result = $db->RunQuery($sql);
        while($row=mysqli_fetch_array($result))
        {
            $response['email']	 		= $row['EMAIL'];
            $response['ccEmail'] 	 	= $row['EMAIL_CC'];
            $response['sendPlant']      = $row['SEND_PLANT_ASWELL'];
        }
        echo json_encode($response);
}
?>
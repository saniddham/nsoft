var basepath	= 'presentation/customerAndOperation/masterData/customerBrandEmail/';

$(document).ready(function(){	
	$("#frmCustomerLocationEmail").validationEngine();
	$('#cboCustomer').die('change').live('change',loadData);
    $('#cboBrand').die('change').live('change',loadEmails);
    $('#cboLocation').die('change').live('change',loadEmails);
	$('#butSave').die('click').live('click',saveData);
	$('#butNew').die('click').live('click',clearAll);
});

function loadData()
{
    $('#txtEmail').val("");
    $('#txtCCEmail').val("");
	if($(this).val()=='')
	{
		return;
	}
	var url 	= basepath+"customerBrandEmail-db-get.php?requestType=loadData";
	var httpobj = $.ajax({
		url:url,
		type:'POST',
		dataType:'json',
		data:"customer="+$(this).val(),
		async:false,
		success:function(json){
			var brands   = json.brands;
			var locations  = json.locations;
            $('#cboLocation')
                .empty()
                .append(locations);
            $('#cboBrand')
                .empty()
                .append(brands);
		}
	});
	
}

function loadEmails()
{
    var brand = $('#cboBrand').val();
    var location = $('#cboLocation').val();
    var customer = $('#cboCustomer').val();

    if(brand!='' && location!='')
    {
        var url 	= basepath+"customerBrandEmail-db-get.php?requestType=loadEmails";
        var httpobj = $.ajax({
            url:url,
            type:'POST',
            dataType:'json',
            data:"&customer="+customer + "&brand="+ brand + "&location=" + location,
            async:false,
            success:function(json){
                var email		= json.email;
                var ccEmail		= json.ccEmail;
                var plantMail		= json.sendPlant;
                $('#txtEmail').val(email);
                $('#txtCCEmail').val(ccEmail);
                if (plantMail == 1){
                    $("#chkPlantMail").prop("checked", true);
				} else {
                    $("#chkPlantMail").prop("checked", false);
				}

            }
        });
    }


}
function saveData()
{
	var customerId = $('#cboCustomer').val();
    var brand = $('#cboBrand').val();
    var location = $('#cboLocation').val();
    var emails = $('#txtEmail').val();
    var ccEmails = $('#txtCCEmail').val();
    var checkStatus=$("#chkPlantMail").is(":checked");
    var  checkFlag = 0;
    if (checkStatus){
        checkFlag = 1;
	}
	
	if(customerId=='' || brand == '' || location=='')
	{
		$(this).validationEngine('showPrompt', 'Please select a Customer, Brand and Location.','fail');
		var t=setTimeout("alertx()",3000);	
		return;
	}

	var value="[ ";
		value +='{"email":'+URLEncode_json(emails)+',"ccEmail":'+URLEncode_json(ccEmails)+',"checkFlag":' + checkFlag + ' }' ;
	value += " ]";
	var url = basepath+"customerBrandEmail-db-set.php?requestType=saveData";
	$.ajax({
			url:url,
			type:'post',
			async:false,
			dataType:'json',
			data:"&customerId="+customerId+ "&brand="+brand + "&location=" + location +"&emailDetails="+value,
	success:function(json){
			$('#butSave').validationEngine('showPrompt', json.msg,json.type);
			if(json.type=='pass')
			{
				var t=setTimeout("alertx()",3000);
				return;
			}
		},
		error:function(){
			
			$('#frmCustomerLocationEmail #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
			var t=setTimeout("alertx()",3000);	
			return;					
		}
	});
	
}
function clearAll()
{
	// $("#tblMain tr:gt(0)").remove();
	$('#cboCustomer').val('');
    $('#cboBrand').val('');
    $('#cboLocation').val('');
    $('#txtEmail').val('');
    $('#txtCCEmail').val('');
    $("#chkPlantMail").prop("checked", false);
}
function add_new_row(table,rowcontent)
{
	if ($(table).length>0)
	{
		if ($(table+' > tbody').length==0) $(table).append('<tbody />');
		($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
	}
}
function alertx()
{
	$('#frmCustomerLocationEmail #butSave').validationEngine('hide')	;
}
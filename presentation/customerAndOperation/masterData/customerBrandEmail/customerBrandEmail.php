<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
require_once "class/cls_permisions.php";

$objpermisionget			= new cls_permisions($db);
 
$indiaCustomerViewMode		= $objpermisionget->boolSPermision(14);
$srilankaCustomerViewMode	= $objpermisionget->boolSPermision(15);
$otherCustomerViewMode		= $objpermisionget->boolSPermision(16);

//101 - India
//204 - Sri lanka 
$inList		= '';

if($indiaCustomerViewMode==1){
	$inList = '101';
}
else{
	$notInList = '101';
}
if($srilankaCustomerViewMode==1){
	if($inList=='')
	$inList .= '204';
	else
	$inList .= ',204';
}
else{ 
	if($notInList=='')
	$notInList .= '204';
	else
	$notInList .= ',204';
}
 
 if($notInList == ''){
	 $notInList = "'*'";
 }
  
if($otherCustomerViewMode==1){
	$para = "AND (intCountryId NOT IN ($notInList))";
}
else{
	$para = "AND (intCountryId IN ($inList))";
}

?>
<title>Customer Brands Email</title>
<form id="frmCustomerLocationEmail" name="frmCustomerLocationEmail" method="post" action="" autocomplete="off">
<div align="center">
		<div class="trans_layoutD" style="width:700px">
		  <div class="trans_text">Customer Brands Email</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
          <tr>
    <td><table width="100%" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
              <tr>
                
                <td class="normalfnt">Customer</td>
                <td colspan="2">
                <select name="cboCustomer" class="txtbox" id="cboCustomer" style="width:150px" tabindex="1">
                   <option value="" style="width:150px"  style="width:150px" ></option>
                 <?php   $sql = "SELECT intId,strName FROM mst_customer WHERE intStatus=1 $para  ORDER BY strName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option style=\"width:150px\"  value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?> 
                  </select></td>

                  <td class="normalfnt">Location</td>
                  <td colspan="2">
                      <select name="cboLocation" class="txtbox" id="cboLocation" style="width:150px" style="width:270px" tabindex="1">
                          <option value=""></option>
                          <?php   $sql = "SELECT intId,strName FROM mst_customer_locations_header WHERE intStatus=1 ORDER BY strName
								";
                          $result = $db->RunQuery($sql);
                          while($row=mysqli_fetch_array($result))
                          {
                              echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
                          }
                          ?>
                      </select></td>


              </tr>
              <tr>

                  <td class="normalfnt">Brand</td>
                  <td colspan="2">
                      <select name="cboBrand" class="txtbox" id="cboBrand" style="width:150px" tabindex="1">
                          <option value=""></option>
                          <?php   $sql = "SELECT intId,strName FROM mst_brand WHERE intStatus=1 ORDER BY strName
								";
                          $result = $db->RunQuery($sql);
                          while($row=mysqli_fetch_array($result))
                          {
                              echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
                          }
                          ?>
                      </select></td>
                  <td class="normalfnt"></td>
                  <td class="normalfnt">
                      <input id="chkPlantMail" name="chkPlantMail"
                             type="checkbox">
                      Send The Mail To Customer Plant
                  </td>
              </tr>
              <tr>
               
                <td colspan="4" class="normalfnt">
          <table width="450" class="bordered" id="tblMain" >
          <thead>
            <tr>
              <th width="35%" height="22" >Email</th>
              <th width="35%" >CC Email</th>
            </tr>
          </thead>
          <tbody>
          <tr>
              <td width="35%" height="22" ><textarea name="txtEmail" id="txtEmail" class="clsEmail validate[maxSize[250]]" cols="30" rows="2" ></textarea></td>
              <td width="35%" ><textarea name="txtCCEmail" id="txtCCEmail" class="clsCCEmail validate[maxSize[250]]" cols="30" rows="2" ></textarea></td>
          </tr>
          </tbody>
          </table>
          </td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
    <tr>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" ><img border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img <?php echo $form_permision['add']?'':'style="display:none"'?> border="0" src="images/Tsave.jpg" alt="Save" name="butSave" width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
    </tr>
  </table>
	</div>
  </div>
</form>
<?php
ini_set('display_errors',0);
session_start();
$backwardseperator = "../../../../";
$mainPath 	= $_SESSION['mainPath'];
$userId 	= $_SESSION['userId'];
include "{$backwardseperator}dataAccess/Connector.php";
$response = array('type'=>'', 'msg'=>'');

/////////// parameters /////////////////////////////
$requestType 	= $_REQUEST['requestType'];
$id 			= $_REQUEST['cboSearch'];

$section        =$_REQUEST['cboTypeOfPrint'];
$user           =$_SESSION['userId'];
$code			= trim($_REQUEST['txtCode']);
$name			= trim($_REQUEST['txtName']);;
$remark			= trim($_REQUEST['txtRemark']);
$intStatus		= ($_REQUEST['chkActive']?1:0);
$company        =$_REQUEST['cboCompany'];
$location       =$_REQUEST['cboLocation'];

/////////// module insert part /////////////////////
if($requestType=='add')
{
    $sql="INSERT INTO `mst_module` (`intSection`,`strCode`,`strName`,`intCompany`,`intLocation`,`strRemark`,`intStatus`,`intCreator`,`dtmCreateDate`) 
				VALUES ('$section','$code','$name','$company','$location','$remark',$intStatus,'$userId',now())";

    $result = $db->RunQuery($sql);

    if($result){
        $response['type'] 		= 'pass';
        $response['msg'] 		= 'Saved successfully.';
    }
    else{
        $response['type'] 		= 'fail';
        $response['msg'] 		= $db->errormsg;
        $response['q'] 			= $sql;
    }
}
/////////// module update part /////////////////////
else if($requestType=='edit')
{
    $sql = "UPDATE `nsoft`.`mst_module`
            SET `intSection` = '$section',
             `strCode` = '$code',
             `strName` = '$name',
            `intCompany` = '$company',
             `intLocation` = '$location',
             `strRemark` = '$remark',
             `intStatus` = '$intStatus',
             `intModifyer` = '$user',
             `dtmModifyDate` = now()
            WHERE
                (`intId` = '$id')";

    $result = $db->RunQuery($sql);
    if(($result)){
        $response['type'] 		= 'pass';
        $response['msg'] 		= 'Updated successfully.';
    }
    else{
        $response['type'] 		= 'fail';
        $response['msg'] 		= $db->errormsg;
        $response['q'] 			=$sql;
    }
}

/////////// module delete part /////////////////////
else if($requestType=='delete')
{
    $sql = "DELETE FROM `mst_module` WHERE (`intId`='$id')  ";
    $result = $db->RunQuery($sql);
    if(($result)){
        $response['type'] 		= 'pass';
        $response['msg'] 		= 'Deleted successfully.';
    }
    else{
        $response['type'] 		= 'fail';
        $response['msg'] 		= $db->errormsg;
        $response['q'] 			=$sql;
    }
}
echo json_encode($response);
?>
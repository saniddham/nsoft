<?php
session_start();
$backwardseperator = "../../../../";
$mainPath 	= $_SESSION['mainPath'];
$userId 	= $_SESSION['userId'];
$requestType 	= $_REQUEST['requestType'];
include "{$backwardseperator}dataAccess/Connector.php";


/////////// location load part /////////////////////
if($requestType=='loadCombo')
{
    $sql = "SELECT
					intId,
					strName
				FROM mst_module 
				order by strName
				";
    $result = $db->RunQuery($sql);
    $html = "<option value=\"\"></option>";
    while($row=mysqli_fetch_array($result))
    {
        $html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
    }
    echo $html;
}
else if($requestType=='loadDetails')
{
    $id  = $_REQUEST['id'];
    $sql = "SELECT
	mst_module.strCode,
	mst_module.strName as module,
	mst_section.strName as section,
	mst_module.strRemark,
	mst_module.intStatus,
	mst_companies.strName as company,
	mst_locations.strName as location
FROM
	mst_module INNER JOIN mst_section ON mst_section.intId=mst_module.intSection 
	INNER  JOIN mst_companies ON  mst_companies.intId=mst_module.intCompany
	INNER  JOIN mst_locations ON  mst_locations.intId=mst_module.intLocation
	WHERE mst_module.intId='$id'";


    $result = $db->RunQuery($sql);
    while($row=mysqli_fetch_array($result))
    {
        $response['code'] 				= $row['strCode'];
        $response['name'] 				= $row['module'];
        $response['section'] 		    = $row['section'];
        $response['remark'] 			= $row['strRemark'];
        $response['company']            =$row['company'];
        $response['location']           =$row['location'];
        $response['status'] 			= ($row['intStatus']?true:false);

    }

    echo json_encode($response);
}
elseif($requestType=='URLgetNextCode')
{
    $sql = "SELECT MAX(intId) AS tId FROM mst_module order by tId DESC";
    $result = $db->RunQuery($sql);
    while($row=mysqli_fetch_array($result))
    {
        $response['NextId'] 				= $row['tId'] + 1000;
    }
    echo json_encode($response);
}

?>
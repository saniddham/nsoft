$(document).ready(function() {
    getNextCode();
    $("#frmProductionModules").validationEngine();
    $('#frmProductionModules #txtName').focus();
    $('#frmProductionModules #butSave').die('click').live('click',save);
    $('#frmProductionModules #butNew').die('click').live('click',clearForm);
    $('#frmProductionModules #cboSearch').die('change').live('change',searchCombo);
    $('#frmProductionModules #butDelete').die('click').live('click',deleteData);
});

function getNextCode()
{
    if($('#frmProductionModules #txtCode').val()!='')
        return;

    var url 	= "presentation/customerAndOperation/masterData/productionModules/productionModules-db-get.php";
    var data	= "requestType=URLgetNextCode";

    $.ajax({
        url:url,
        data:data,
        dataType: "json",
        async:false,
        success:function(json){
            $('#frmProductionModules #txtCode').val('M'+json.NextId);
        }
    });
}

function save()
{
    if(!$('#frmProductionModules').validationEngine('validate'))
    {
        var t = setTimeout("alertx('#frmProductionModules')",2000);
        return;
    }

    showWaiting();

    if($("#frmProductionModules #cboSearch").val()=='')
        requestType = 'add';
    else
        requestType = 'edit';

    var url = "presentation/customerAndOperation/masterData/productionModules/productionModules-db-set.php";
    var obj = $.ajax({
        url:url,
        dataType: "json",
        type: 'post',
        data:$("#frmProductionModules").serialize()+'&requestType='+requestType,
        async:false,
        success:function(json){
            $('#frmProductionModules #butSave').validationEngine('showPrompt', json.msg,json.type);
            if(json.type=='pass')
            {
                var t = setTimeout("alertx('#frmProductionModules #butSave')",2000);
                hideWaiting();
                clearForm();
                return;
            }
            hideWaiting();
            var t = setTimeout("alertx('#frmProductionModules #butSave')",4000);
        },
        error:function(xhr,status){
            hideWaiting();
            $('#frmProductionModules #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
            var t=setTimeout("alertx('#frmProductionModules #butSave')",4000);
        }
    });
}

function deleteData()
{
    if($('#frmProductionModules #cboSearch').val()=='')
    {
        $('#frmProductionModules #butDelete').validationEngine('showPrompt', 'Please select Production Category.', 'fail');
        var t=setTimeout("alertx('#frmProductionModules')",2000);
        return;
    }

    var val = $.prompt('Are you sure you want to delete "'+$('#frmProductionModules #cboSearch option:selected').text()+'" ?',{
        buttons: { Ok: true, Cancel: false },
        callback: function(v,m,f){
            if(v)
            {
                var url = "presentation/customerAndOperation/masterData/productionModules/productionModules-db-set.php";
                var httpobj = $.ajax({
                    url:url,
                    dataType:'json',
                    type: 'post',
                    data:'requestType=delete&cboSearch='+$('#frmProductionModules #cboSearch').val(),
                    async:false,
                    success:function(json){
                        $('#frmProductionModules #butDelete').validationEngine('showPrompt', json.msg,json.type);
                        if(json.type=='pass')
                        {
                            var t=setTimeout("alertx('#frmProductionModules #butDelete')",2000);
                            clearForm();
                            return;
                        }
                        var t=setTimeout("alertx('#frmProductionModules #butDelete')",4000);
                    },
                    error:function(xhr,status){
                        $('#frmProductionModules #butDelete').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                        var t=setTimeout("alertx('#frmProductionModules #butDelete')",4000);
                    }
                });
            }
        }
    });
}

function clearForm()
{
    var t = setTimeout("alertx('#frmProductionModules')",2000);
    showWaiting();
    loadCombo_frmProductionModules();
    $('#frmProductionModules #cboSearch').val('');
    $('#frmProductionModules #cboSection').val('');
    $('#frmProductionModules #txtCode').val('');
    $('#frmProductionModules #txtName').val('');
    $('#frmProductionModules #cbocompany').val('');
    $('#frmProductionModules #cboLocation').val('');
    $('#frmProductionModules #chkActive').prop('checked',true);
    getNextCode();
    $('#frmProductionModules #txtName').focus();
    hideWaiting();
}

function searchCombo()
{
    $('#frmProductionModules').validationEngine('hide');

    if($('#frmProductionModules #cboSearch').val()=='')
    {

        clearForm();
        return;
    }

    var url = "presentation/customerAndOperation/masterData/productionModules/productionModules-db-get.php";

    var httpobj = $.ajax({
        url:url,
        dataType:'json',
        data:'requestType=loadDetails&id='+$(this).val(),
        async:false,
        success:function(json){

            $('#frmProductionModules #txtCode').val(json.code);
            $('#frmProductionModules #txtName').val(json.name);
            $('#frmProductionModules #cboSection').val(json.section);
            $('#frmProductionModules #cboCompany').val(json.company);
            $('#frmProductionModules #cboLocation').val(json.location);
            $('#frmProductionModules #txtRemark').val(json.remark);
            $('#frmProductionModules #chkActive').attr('checked',json.status);
        }
    });
}

function loadCombo_frmProductionModules()
{
    var url 	= "presentation/customerAndOperation/masterData/productionModules/productionModules-db-get.php?requestType=loadCombo";
    var httpobj = $.ajax({url:url,async:false})
    $('#frmProductionModules #cboSearch').html(httpobj.responseText);

}

function alertx(obj)
{
    $(obj).validationEngine('hide')	;
}
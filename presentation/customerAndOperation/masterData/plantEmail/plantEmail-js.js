var basepath	= 'presentation/customerAndOperation/masterData/plantEmail/';

$(document).ready(function(){	
	$("#frmPlantEmail").validationEngine();	
	$('#butSave').die('click').live('click',saveData);
});

function saveData()
{
	var value="[ ";
	$('.clsEmail').each(function(){

		plantId  	= $(this).parent().parent().attr('id');
		email 		= $(this).val();
		ccEmail 	= $(this).parent().parent().find('.clsCCEmail').val();
		
		value +='{"plantId":"'+plantId+'","email":"'+email+'","ccEmail":"'+ccEmail+'"},' ;	
	});
	
	value = value.substr(0,value.length-1);
	value += " ]";
	
	var url = basepath+"plantEmail-db-set.php?requestType=saveData";
	$.ajax({
			url:url,
			async:false,
			dataType:'json',
			type:'post',
			data:"&emailDetails="+value,
	success:function(json){
			$('#butSave').validationEngine('showPrompt', json.msg,json.type);
			if(json.type=='pass')
			{
				var t=setTimeout("alertx()",3000);
				return;
			}
		},
		error:function(){
			
			$('#frmPlantEmail #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
			return;					
		}
	});
}
function alertx()
{
	$('#frmPlantEmail #butSave').validationEngine('hide')	;
}
<?php 
session_start();
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 	 		= $_SESSION['userId'];
$requestType 		= $_REQUEST['requestType'];
$emailDetails 		= json_decode($_REQUEST['emailDetails'], true);

include "{$backwardseperator}dataAccess/Connector.php";

if($requestType=='saveData')
{
	$rollBackFlag 	= 0;
	
	foreach($emailDetails as $arrDetails)
	{
		$email 		= $arrDetails['email'];
		$ccEmail 	= $arrDetails['ccEmail'];
		$plantId 	= $arrDetails['plantId'];
		
		$sql = "SELECT * FROM mst_plant_email WHERE intPlantId='$plantId' ";
		$result = $db->RunQuery($sql);
		if(mysqli_num_rows($result)==0)
		{
			$sqlFinal = "INSERT INTO mst_plant_email 
						(
						intPlantId, 
						strEmailAddress, 
						strCCEmailAddress, 
						intCreator, 
						dtmCreateDate
						)
						VALUES
						(
						'$plantId', 
						'$email', 
						'$ccEmail', 
						'$userId', 
						now()
						);";
			$resultFinal = $db->RunQuery($sqlFinal);
			if(!$resultFinal)
			{
				$rollBackFlag = 1;
				$rollBackMsg  = $db->errormsg;
			}
		}
		else
		{
			$sqlFinal = " UPDATE mst_plant_email 
							SET
							strEmailAddress = '$email' , 
							strCCEmailAddress = '$ccEmail' , 
							intModifyer = '$userId' , 
							dtmModifyDate = NOW()
							WHERE
							intPlantId = '$plantId' ";
			$resultFinal = $db->RunQuery($sqlFinal);
			if(!$resultFinal)
			{
				$rollBackFlag = 1;
				$rollBackMsg  = $db->errormsg;
			}
		}
	}
	if($rollBackFlag==0)
	{
		$response['type'] 		= 'pass';
		$response['msg'] 		= 'Saved successfully.';
	}
	else
	{
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
	}
	echo json_encode($response);
}

?>
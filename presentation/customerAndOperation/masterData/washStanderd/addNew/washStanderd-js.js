var basepath	= 'presentation/customerAndOperation/masterData/washStanderd/addNew/';

$(document).ready(function() {
	getNextCode();
	$("#frmWashStanderd").validationEngine();
	$('#frmWashStanderd #txtCode').focus();
	$('#frmWashStanderd #butSave').die('click').live('click',saveData);
	$('#frmWashStanderd #butNew').die('click').live('click',clearForm);

    $('#frmWashStanderd #cboSearch').die('change').live('change',function()
	{
		$('#frmWashStanderd').validationEngine('hide');
		
		if($('#frmWashStanderd #cboSearch').val()=='')
		{
			$('#frmWashStanderd').get(0).reset();
			return;	
		}
		
		var url = basepath+"washStanderd-db-get.php";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'post',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
				$('#frmWashStanderd #txtCode').val(json.code);
				$('#frmWashStanderd #txtName').val(json.name);
				$('#frmWashStanderd #txtRemark').val(json.remark);
				$('#frmWashStanderd #chkActive').prop('checked',json.status);
			}
		});
	});
	
    $('#frmWashStanderd #butDelete').die('click').live('click',function(){
		if($('#frmWashStanderd #cboSearch').val()=='')
		{
			$('#frmWashStanderd #butDelete').validationEngine('showPrompt', 'Please select Wash Standerd.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmWashStanderd #cboSearch option:selected').text()+'" ?',{
						buttons: { Ok: true, Cancel: false },
						callback: function(v,m,f){
							if(v)
							{
								var url = basepath+"washStanderd-db-set.php";
								var httpobj = $.ajax({
									url:url,
									dataType:'json',
									type:'post',
									data:'requestType=delete&cboSearch='+$('#frmWashStanderd #cboSearch').val(),
									async:false,
									success:function(json){										
										$('#frmWashStanderd #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);										
										if(json.type=='pass')
										{											
											var t=setTimeout("alertDelete()",2000);
											clearForm();
											return;
										}	
										var t=setTimeout("alertDelete()",3000);
									}	 
							});
						}
				}
		 	});
			
		}
	});
});

function getNextCode()
{
	if($('#frmWashStanderd #txtCode').val()!='')
		return;
		
	var url 	= "presentation/customerAndOperation/masterData/washStanderd/addNew/washStanderd-db-get.php";
	var data	= "requestType=URLgetNextCode";

	$.ajax({
		url:url,
		data:data,
		dataType: "json",  
		type:'post',
		async:false,			
		success:function(json){
			$('#frmWashStanderd #txtCode').val('WS'+json.NextId);
		}
	});	
}

function saveData()
{		
	if ($('#frmWashStanderd').validationEngine('validate'))   
	{			 
		if($("#frmWashStanderd #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = basepath+"washStanderd-db-set.php";
		var obj = $.ajax({
			url:url,
			dataType: "json",  
			data:$("#frmWashStanderd").serialize()+'&requestType='+requestType,
			async:false,			
			success:function(json){
					$('#frmWashStanderd #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{							
						var t=setTimeout("alertx()",2000);
						clearForm();
						return;
					}
					var t=setTimeout("alertx()",4000);
				},
			error:function(xhr,status){
					
					$('#frmWashStanderd #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",4000);
				}		
		});
	}	
}

function clearForm()
{
	var t = setTimeout("alertx('#frmWashStanderd')",2000);
	showWaiting();
	$('#frmWashStanderd').get(0).reset();
	loadCombo_frmWashStanderd();
	getNextCode();
	$('#frmWashStanderd #txtCode').focus();
	hideWaiting();
}

function loadCombo_frmWashStanderd()
{
	var url 	= basepath+"washStanderd-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmWashStanderd #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmWashStanderd #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmWashStanderd #butDelete').validationEngine('hide')	;
}

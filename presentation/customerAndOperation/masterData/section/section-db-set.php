<?php 
	session_start();
        $backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];
	$name			= trim($_REQUEST['txtName']);
	$remark			= trim($_REQUEST['txtRemark']);
	$intStatus		= ($_REQUEST['chkActive']?1:0);	
	
	/////////// section insert part /////////////////////
	if($requestType=='add')
	{
		$sql = "INSERT INTO `mst_section` (`strName`,`strRemark`,`intCreator`,dtmCreateDate,intStatus) 
				VALUES ('$name','$remark','$userId',now(),'$intStatus')";
		$result = $db->RunQuery($sql);
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	/////////// section update part /////////////////////
	else if($requestType=='edit')
	{
		$sql = "UPDATE `mst_section` SET 	strName		='$name',
												strRemark	='$remark',
												intStatus	='$intStatus',
												intModifyer	='$userId'
				WHERE (`intId`='$id')";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// section delete part /////////////////////
	else if($requestType=='delete')
	{
		$sql_select	=  "SELECT
						trn_bulkdailyproduction.intBulkProductionNo,
						trn_bulkdailyproduction.intBulkProductionYear
						FROM
						trn_bulkdailyproduction
						WHERE
						trn_bulkdailyproduction.intSection = '$id'";
						
		$result_select	= $db->RunQuery($sql);
		if(mysqli_num_rows($result)>=1)
		{
			$sql = "UPDATE `mst_section` SET intStatus	='-1' 
				WHERE (`intId`='$id')";
			$result = $db->RunQuery($sql);
			if(($result)){
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Deleted successfully.';
			}
			else{
				$response['type'] 		= 'fail';
				$response['msg'] 		= $db->errormsg;
				$response['q'] 			=$sql;
			}
		}
		else 
		{
			$response['type'] 		= 'fail';
			$response['msg'] 		= 'This Section already used in Bulk Daily Production.';	
		}	
	}
	echo json_encode($response);
?>
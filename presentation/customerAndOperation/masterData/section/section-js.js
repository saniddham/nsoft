/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var basepath	= 'presentation/customerAndOperation/masterData/section/';
			
$(document).ready(function() {
  		$("#frmBulkSection").validationEngine();
		$('#frmBulkSection #txtName').focus();

  ///save button click event
  $('#frmBulkSection #butSave').click(function(){
	//$('#frmBulkSection').submit();
	var requestType = '';
	if ($('#frmBulkSection').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmBulkSection #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = basepath+"section-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json", 
			type:'POST', 
			data:$("#frmBulkSection").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmBulkSection #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmBulkSection').get(0).reset();
						loadCombo_frmSection();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmBulkSection #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load customer brands details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmBulkSection #cboSearch').click(function(){
	   $('#frmBulkSection').validationEngine('hide');
   });
    $('#frmBulkSection #cboSearch').change(function(){
		$('#frmBulkSection').validationEngine('hide');
		var url = basepath+"section-db-get.php";
		if($('#frmBulkSection #cboSearch').val()=='')
		{
			$('#frmBulkSection').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'post',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmBulkSection #txtName').val(json.name);
					$('#frmBulkSection #txtRemark').val(json.remark);
					$('#frmBulkSection #chkActive').attr('checked',json.status);
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmBulkSection #butNew').click(function(){
		$('#frmBulkSection').get(0).reset();
		loadCombo_frmSection();
		$('#frmBulkSection #txtName').focus();
	});
    $('#frmBulkSection #butDelete').click(function(){
		if($('#frmBulkSection #cboSearch').val()=='')
		{
			$('#frmBulkSection #butDelete').validationEngine('showPrompt', 'Please select section.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmBulkSection #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basepath+"section-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'post',
											data:'requestType=delete&cboSearch='+$('#frmBulkSection #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmBulkSection #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmBulkSection').get(0).reset();
													loadCombo_frmSection();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});


function loadCombo_frmSection()
{
	var url 	= basepath+"section-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmBulkSection #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmBulkSection #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmBulkSection #butDelete').validationEngine('hide')	;
}

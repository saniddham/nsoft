var basePath	= "presentation/customerAndOperation/masterData/typeOfSupplier/addNew/";
			
$(document).ready(function() {
  		$("#frmTypeOfSupplier").validationEngine();
		$('#frmTypeOfSupplier #txtCode').focus();
  //permision for add 
  /*if(intAddx)
  {
 	$('#frmTypeOfSupplier #butNew').show();
	$('#frmTypeOfSupplier #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmTypeOfSupplier #butSave').show();
	$('#frmTypeOfSupplier #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmTypeOfSupplier #butDelete').show();
	$('#frmTypeOfSupplier #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmTypeOfSupplier #cboSearch').removeAttr('disabled');
  }*/
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

  ///save button click event
  $('#frmTypeOfSupplier #butSave').die('click').live('click',function(){
	//$('#frmTypeOfSupplier').submit();
	var requestType = '';
	if ($('#frmTypeOfSupplier').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		showWaiting();
		if($("#frmTypeOfSupplier #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = basePath+"typeOfSupplier-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",
			type:'post',  
			data:$("#frmTypeOfSupplier").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmTypeOfSupplier #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmTypeOfSupplier').get(0).reset();
						loadCombo_frmTypeOfSupplier();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmTypeOfSupplier #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
		hideWaiting();
	}
   });
   
   /////////////////////////////////////////////////////
   //// load type of supplier details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmTypeOfSupplier #cboSearch').die('click').live('click',function(){
	   $('#frmTypeOfSupplier').validationEngine('hide');
   });
    $('#frmTypeOfSupplier #cboSearch').die('change').live('change',function(){
		//alert('ok');
		$('#frmTypeOfSupplier').validationEngine('hide');
		var url = basePath+"typeOfSupplier-db-get.php";
		if($('#frmTypeOfSupplier #cboSearch').val()=='')
		{
			$('#frmTypeOfSupplier').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmTypeOfSupplier #txtCode').val(json.code);
					$('#frmTypeOfSupplier #txtName').val(json.name);
					$('#frmTypeOfSupplier #txtRemark').val(json.remark);
					$('#frmTypeOfSupplier #chkActive').attr('checked',json.status);
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmTypeOfSupplier #butNew').die('click').live('click',function(){
		$('#frmTypeOfSupplier').get(0).reset();
		loadCombo_frmTypeOfSupplier();
		$('#frmTypeOfSupplier #txtCode').focus();
	});
    $('#frmTypeOfSupplier #butDelete').die('click').live('click',function(){
		if($('#frmTypeOfSupplier #cboSearch').val()=='')
		{
			$('#frmTypeOfSupplier #butDelete').validationEngine('showPrompt', 'Please select Type.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmTypeOfSupplier #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basePath+"typeOfSupplier-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'POST',
											data:'requestType=delete&cboSearch='+$('#frmTypeOfSupplier #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmTypeOfSupplier #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmTypeOfSupplier').get(0).reset();
													loadCombo_frmTypeOfSupplier();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});


function loadCombo_frmTypeOfSupplier()
{
	var url 	= basePath+"typeOfSupplier-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmTypeOfSupplier #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmTypeOfSupplier #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmTypeOfSupplier #butDelete').validationEngine('hide')	;
}

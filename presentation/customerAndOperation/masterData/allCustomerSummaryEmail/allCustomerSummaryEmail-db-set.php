<?php 
session_start();
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 	 		= $_SESSION['userId'];
$requestType 		= $_REQUEST['requestType'];
$locationId	  		= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$userDetails 		= json_decode($_REQUEST['userDetails'], true);

include "{$backwardseperator}dataAccess/Connector.php";

if($requestType=='saveData')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	
	$rollBackFlag 	= 0;
	
	$sqlDel = "DELETE FROM mst_allcustomer_summary_email ";
	$resultDel = $db->RunQuery2($sqlDel);
	if(!$resultDel)
	{
		$rollBackFlag = 1;
		$rollBackMsg  = $db->errormsg;
	}
	else
	{
		foreach($userDetails as $arrDetails)
		{
			$userId = $arrDetails['userId'];
			
			$sql = "INSERT INTO mst_allcustomer_summary_email 
					(
					intUserId, 
					intCompanyId
					)
					VALUES
					(
					'$userId', 
					'$locationId'
					);";
			$result = $db->RunQuery2($sql);
			if(!$result)
			{
				$rollBackFlag = 1;
				$rollBackMsg  = $db->errormsg;
			}
		}
	}
	if($rollBackFlag==1)
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
	}
	else
	{
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		$response['msg'] 		= 'Saved successfully.';
	}
	$db->CloseConnection();	
	echo json_encode($response);	
}
?>
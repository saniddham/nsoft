var basepath	= 'presentation/customerAndOperation/masterData/allCustomerSummaryEmail/';
// JavaScript Document
$(document).ready(function(){	
	$("#frmMailConfig").validationEngine();	
	$('#butInsert').die('click').live('click',setToAddNew);
	$('#butSave').die('click').live('click',saveData);
	$('.removeRow').die('click').live('click',setRemoveRow);
});
function setToAddNew()
{
	popupWindow3('1');
	$('#popupContact1').html('');
	$('#popupContact1').load(basepath+'emailUserPopup.php',function(){
		
		$('#frmPopUp #butAdd').die('click').live('click',function(){
		
			$('.chkUsers:checked').each(function(){
				var fromId = $(this).parent().parent().attr('id');
				var found  = false;
				
				$('#frmMailConfig #tblMain .removeRow').each(function(){
					if(fromId ==$(this).parent().parent().attr('id'))
					{
						found = true;
					}	
				});
				if(found==false)
				{
					
					document.getElementById('tblMain').insertRow(document.getElementById('tblMain').rows.length);
					document.getElementById('tblMain').rows[document.getElementById('tblMain').rows.length-1].innerHTML = "<td align=\"center\" >"+
					"<img class=\"mouseover removeRow\" src=\"images/del.png\" /></td>"+
   		 			"<td align=\"left\" >"+$(this).parent().parent().find('td').eq(1).html()+"</td>"+
    				"<td align=\"left\">"+$(this).parent().parent().find('td').eq(2).html()+"</td>";
					document.getElementById('tblMain').rows[document.getElementById('tblMain').rows.length-1].id  = fromId;
					setRemoveRow();
					
				}
			});
			disablePopup();
			
		});
		
		$('#frmPopUp #butClose1').die('click').live('click',disablePopup);
		
	});		
	
}
function setRemoveRow()
{
	$(this).parent().parent().remove();
}
function saveData()
{
	var chkStatus = true;
	var value="[ ";
	$('.removeRow').each(function(){

		userId  	= $(this).parent().parent().attr('id');
		
		value +='{"userId":"'+userId+'" },' ;
		chkStatus = false;	
	});
	
	value = value.substr(0,value.length-1);
	value += " ]";
	
	if(chkStatus)
	{
		$(this).validationEngine('showPrompt', 'No data to save.','fail');	
		return;
	}
	
	var url = basepath+"allCustomerSummaryEmail-db-set.php?requestType=saveData";
	$.ajax({
			url:url,
			async:false,
			dataType:'json',
			type:'post',
			data:"&userDetails="+value,
	success:function(json){
			$('#butSave').validationEngine('showPrompt', json.msg,json.type);
			if(json.type=='pass')
			{
				var t=setTimeout("alertx()",3000);
				return;
			}
		},
		error:function(){
			
			$('#frmMailConfig #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');	
			return;					
		}
	});
}
function alertx()
{
	$('#frmMailConfig #butSave').validationEngine('hide')	;
}
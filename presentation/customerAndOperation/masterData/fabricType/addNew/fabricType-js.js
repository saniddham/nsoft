var basepath = 'presentation/customerAndOperation/masterData/fabricType/addNew/';
			
$(document).ready(function() {
		loadNewCode();
  		$("#frmFabricType").validationEngine();
		$('#frmFabricType #txtCode').focus();

  ///save button click event
  $('#frmFabricType #butSave').click(function(){
	//$('#frmFabricType').submit();
	var requestType = '';
	if ($('#frmFabricType').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmFabricType #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = basepath+"fabricType-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json", 
			type:'POST', 
			data:$("#frmFabricType").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmFabricType #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmFabricType').get(0).reset();
						loadCombo_frmPrintMode();
						loadNewCode();
						var t=setTimeout("alertx()",1000);
						$('#txtCode').val(json.nextNo);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmFabricType #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load location details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmFabricType #cboSearch').click(function(){
	   $('#frmFabricType').validationEngine('hide');
   });
    $('#frmFabricType #cboSearch').change(function(){
		$('#frmFabricType').validationEngine('hide');
		var url = basepath+"fabricType-db-get.php";
		if($('#frmFabricType #cboSearch').val()=='')
		{
			$('#frmFabricType').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmFabricType #txtCode').val(json.code);
					$('#frmFabricType #txtName').val(json.name);
					$('#frmFabricType #txtRemark').val(json.remark);
					$('#frmFabricType #chkActive').attr('checked',json.status);
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmFabricType #butNew').click(function(){
		$('#frmFabricType').get(0).reset();
		loadCombo_frmPrintMode();
		loadNewCode();
		$('#frmFabricType #txtCode').focus();
	});
    $('#frmFabricType #butDelete').click(function(){
		if($('#frmFabricType #cboSearch').val()=='')
		{
			$('#frmFabricType #butDelete').validationEngine('showPrompt', 'Please select fabricType.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmFabricType #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basepath+"fabricType-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'POST',
											data:'requestType=delete&cboSearch='+$('#frmFabricType #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmFabricType #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmFabricType').get(0).reset();
													loadCombo_frmPrintMode();
													loadNewCode();
													var t=setTimeout("alertDelete()",1000);return;
													$('#txtCode').val(json.nextNo);
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});


function loadCombo_frmPrintMode()
{
	var url 	= basepath+"fabricType-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmFabricType #cboSearch').html(httpobj.responseText);
}
function loadNewCode()
{ 
	var url 	= basepath+"fabricType-db-get.php?requestType=loadNewCode";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmFabricType #txtCode').val(httpobj.responseText);
}


function alertx()
{
	$('#frmFabricType #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmFabricType #butDelete').validationEngine('hide')	;
}

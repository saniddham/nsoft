<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];
	$code			= trim($_REQUEST['txtCode']);
	$name			= trim($_REQUEST['txtName']);
	$remark			= trim($_REQUEST['txtRemark']);
	$intStatus		= ($_REQUEST['chkActive']?1:0);	
	
	/////////// location insert part /////////////////////
	if($requestType=='add')
	{
		$sql = "INSERT INTO `mst_fabrictype` (`strCode`,`strName`,`strRemark`,`intCreator`,dtmCreateDate,intStatus) 
				VALUES ('$code','$name','$remark','$userId',now(),'$intStatus')";
		$result = $db->RunQuery($sql);
		/////////////////////////// get next code /////////////////////////
		$sql = "SELECT MAX(intId) AS id FROM mst_fabrictype";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$nextCode = 'FT'.($row['id'] + 1000);
		}
		///////////////////////////////////////////////////////////////////
		if($result){
			$response['type'] 		= 'pass';
			$response['nextNo'] 	= $nextCode;
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	/////////// location update part /////////////////////
	else if($requestType=='edit')
	{
		$sql = "UPDATE `mst_fabrictype` SET 	strCode		='$code',
											strName		='$name',
											strRemark	='$remark',
											intStatus	='$intStatus',
											intModifyer	='$userId'
				WHERE (`intId`='$id')";
		$result = $db->RunQuery($sql);
		
		/////////////////////////// get next code /////////////////////////
		$sql = "SELECT MAX(intId) AS id FROM mst_fabrictype";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$nextCode = 'FT'.($row['id'] + 1000);
		}
		///////////////////////////////////////////////////////////////////
		if(($result)){
			$response['type'] 		= 'pass';
			$response['nextNo'] 	= $nextCode;
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// location delete part /////////////////////
	else if($requestType=='delete')
	{
		$sql = "DELETE FROM `mst_fabrictype` WHERE (`intId`='$id')  ";
		$result = $db->RunQuery($sql);
		
		/////////////////////////// get next code /////////////////////////
		$sql = "SELECT MAX(intId) AS id FROM mst_fabrictype";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$nextCode = 'FT'.($row['id'] + 1000);
		}
		///////////////////////////////////////////////////////////////////
		
		if(($result)){
			$response['type'] 		= 'pass';
			$response['nextNo'] 	= $nextCode;
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
?>
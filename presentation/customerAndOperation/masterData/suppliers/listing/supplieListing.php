<?php
(define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');
require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";

$reportId 		= 890;	
$programCode	='P0078';
$intUser		=$_SESSION["userId"];
$approveLevel 	= GetMaxApproveLevel();

///// create where part ///////////////////
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = "";
$where_array = array(
				'STATUS'=>'SU.intStatus',
				'ID'=>'SU.intId',
				'CODE'=>'SU.strCode',
				'NAME'=>'SU.strName',
				'PHONE_NO'=>'SU.strPhoneNo',
				'MOBILE_NO'=>'SU.strMobileNo',
				'COUNTRY_NAME'=>'CO.strCountryName'
				);

$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2','Cancelled'=>'-2','Revised'=>'-1');
if($arr)
{
	foreach($arr as $k=>$v)
	{
		if($v['field']=='STATUS')
		{
			if($arr_status[$v['data']]==2)
				$where_string .= "AND ".$where_array[$v['field']]." > 1";
			else
				$where_string .= "AND ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
		}
		else if($where_array[$v['field']])
			$where_string .= "AND ".$where_array[$v['field']]." like '%".$v['data']."%' ";
	}
}
/*if(!count($arr)>0)					 
	$filter = "AND tb1.dtmPODate = '".date('Y-m-d')."'";
*/

$sql = "SELECT SUB_1.* FROM
		(SELECT
		  	SU.intId      						AS ID,
		  	SU.strCode 							AS CODE,
		  	SU.strName 							AS NAME,
		 	CONCAT(SU.strAddress,' ',strCity) 	AS ADDRESS,
			CO.intCountryID						AS COUNTRY_ID,
			CO.strCountryName 					AS COUNTRY_NAME,
			ST.intId							AS TYPE_ID,
			ST.strName							AS TYPE_NAME,
			SU.strContactPerson					AS CONTACT_PERSON,
			CU.intId							AS CURRENCY_ID,
			CU.strCode							AS CURRENCY_CODE,
			SU.strPhoneNo						AS PHONE_NO,
			SU.strMobileNo						AS MOBILE_NO,
			SU.strFaxNo							AS FAX_NO,
			SU.strEmail 						AS EMAIL,
			SU.strWebSite						AS WEBSITE,
			
			
			if(SU.intStatus=1,'Approved',if(SU.intStatus=0,'Rejected',if(SU.intStatus=-10,'Completed',if(SU.intStatus=-2,'Cancelled',if(SU.intStatus=-1,'Revised','Pending'))))) as STATUS,
			IFNULL((
			SELECT
			concat(U.strUserName,'(',max(IUA.dtApprovedDate),')' )
			FROM
			mst_supplier_approvedby IUA
			Inner Join sys_users U 
				ON IUA.intApproveUser = U.intUserId
			WHERE
				IUA.intSupplierId  = SU.intId AND
				IUA.intApproveLevelNo =  '1' AND 
				IUA.intStatus = '0'
				),IF(((SELECT
				MP.int1Approval 
			FROM menupermision MP
			Inner Join menus M 
				ON MP.intMenuId = M.intId
			WHERE
				M.strCode = '$programCode' AND
				MP.intUserId =  '$intUser') = 1 
				AND SU.intStatus > 1),'Approve', '')) AS `1st_Approval`, ";
				
for($i=2; $i<=$approveLevel; $i++)
{							
	if($i==2){
		$approval	= "2nd_Approval";
	}
	else if($i==3){
		$approval	= "3rd_Approval";
	}
	else {
		$approval	= $i."th_Approval";
	}
	
	
$sql .= "IFNULL(
		(
		SELECT
		concat(U.strUserName,'(',max(IUA.dtApprovedDate),')' )
		FROM
		mst_supplier_approvedby IUA
		Inner Join sys_users U
			ON IUA.intApproveUser = U.intUserId
		WHERE
			IUA.intSupplierId  = SU.intId AND
			IUA.intApproveLevelNo =  '$i' AND 
			IUA.intStatus = '0'
		),
		IF(
		((SELECT
			MP.int".$i."Approval 
			FROM menupermision MP
			Inner Join menus M
				ON MP.intMenuId = M.intId
			WHERE
				M.strCode = '$programCode' 
				AND MP.intUserId =  '$intUser')=1 
				AND (SU.intStatus > 1) 
				AND (SU.intStatus <= SU.intApproveLevel) 
				AND ((SELECT
					max(IUA.dtApprovedDate) 
					FROM
					mst_supplier_approvedby IUA
					Inner Join sys_users U
						ON IUA.intApproveUser = U.intUserId
					WHERE
					IUA.intSupplierId  = SU.intId AND
					IUA.intApproveLevelNo =  ($i-1) AND 
					IUA.intStatus='0')<>'')),		
			'Approve',
			 if($i>SU.intApproveLevel,'-----',''))		
			) as `".$approval."`, "; 
	}				
	
							$sql .= "IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(mst_supplier_approvedby.dtApprovedDate),')' )
								FROM
								mst_supplier_approvedby
								Inner Join sys_users ON mst_supplier_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								mst_supplier_approvedby.intSupplierId  = SU.intId AND
 								mst_supplier_approvedby.intApproveLevelNo =  '0' AND
								mst_supplier_approvedby.intStatus =  '0'
							),IF(((SELECT
								menupermision.intReject 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND SU.intStatus<=SU.intApproveLevel AND 
								SU.intStatus>1),'Reject', '')) as `Reject`,";
								
							$sql .= "IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(mst_supplier_approvedby.dtApprovedDate),')' )
								FROM
								mst_supplier_approvedby
								Inner Join sys_users ON mst_supplier_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								mst_supplier_approvedby.intSupplierId  = SU.intId AND
 								mst_supplier_approvedby.intApproveLevelNo =  '-2' AND
								mst_supplier_approvedby.intStatus =  '0'
							),IF(((SELECT
								menupermision.intCancel 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND 
								SU.intStatus=1),'Cancel', '')) as `Cancel`,";
	
							$sql .= "IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(mst_supplier_approvedby.dtApprovedDate),')' )
								FROM
								mst_supplier_approvedby
								Inner Join sys_users ON mst_supplier_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								mst_supplier_approvedby.intSupplierId  = SU.intId AND
 								mst_supplier_approvedby.intApproveLevelNo =  '-1' AND
								mst_supplier_approvedby.intStatus =  '0'
							),IF(((SELECT
								menupermision.intRevise 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND 
								SU.intStatus=1),'Revise', '')) as `Revise`,";
								
							$sql .= "'View' as `View`, ";  
			
			$sql .= "(SELECT GROUP_CONCAT(L.strName) FROM mst_supplier_locations SL 
			INNER JOIN mst_supplier_locations_header L ON L.intId = SL.intLocationId
			WHERE SL.intSupplierId = SU.intId
			)									AS SUPPLIER_LOCATION
			
		FROM mst_supplier SU
		INNER JOIN mst_country CO
			ON CO.intCountryID = SU.intCountryId
		LEFT JOIN mst_typeofsupplier ST
			ON ST.intId = SU.intTypeId
		LEFT JOIN mst_financecurrency CU
			ON CU.intId = SU.intCurrencyId
			WHERE 1=1 
			$where_string
		)  AS SUB_1 WHERE 1=1 ";
		//  echo $sql;

$jq = new jqgrid('',$db);	
$col = array();
$reportLink  		= "presentation/customerAndOperation/masterData/suppliers/addNew/rptSupplier.php?";

$col["title"] 			= "Status";
$col["name"] 			= "STATUS";
$col["width"] 			= "4";
$col["align"] 			= "center";
$col["sortable"] 		= false; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["stype"] 			= "select";
$str 					= "Pending:Pending;Approved:Approved;Rejected:Rejected;Cancelled:Cancelled;Revised:Revised;:All" ;
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "ID";
$col["name"] 			= "ID";
$col["width"] 			= "1";
$col['link']			= '?q=78&id={ID}';
$col["linkoptions"] 	= "target='_blank'";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Code";
$col["name"] 			= "CODE";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Name";
$col["name"] 			= "NAME";
$col["width"] 			= "6";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Address";
$col["name"] 			= "ADDRESS";
$col["width"] 			= "4";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Type";
$col["name"] 			= "TYPE_NAME";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["dbname"] 			= "TYPE_ID";
$client_lookup 			= $jq->get_dropdown_values("SELECT intId AS k,strName AS v FROM mst_typeofsupplier WHERE intStatus = 1 order by strName");
$col["stype"] 			= "select";
$str 					= ":;".$client_lookup;
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Country";
$col["name"] 			= "COUNTRY_NAME";
$col["width"] 			= "3";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["dbname"] 			= "COUNTRY_ID";
$client_lookup 			= $jq->get_dropdown_values("SELECT intCountryID AS k,strCountryName AS v FROM mst_country");
$col["stype"] 			= "select";
$str 					= ":;".$client_lookup;
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Currency";
$col["name"] 			= "CURRENCY_CODE";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["dbname"] 			= "CURRENCY_ID";
$client_lookup 			= $jq->get_dropdown_values("SELECT intId AS k,strCode AS v FROM mst_financecurrency ORDER BY strCode");
$col["stype"] 			= "select";
$str 					= ":;".$client_lookup;
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Phone No";
$col["name"] 			= "PHONE_NO";
$col["width"] 			= "3";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Mobile No";
$col["name"] 			= "MOBILE_NO";
$col["width"] 			= "3";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Fax No";
$col["name"] 			= "FAX_NO";
$col["width"] 			= "3";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Email";
$col["name"] 			= "EMAIL";
$col["width"] 			= "3";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Web Site";
$col["name"] 			= "WEBSITE";
$col["width"] 			= "3";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Contact Person";
$col["name"] 			= "CONTACT_PERSON";
$col["width"] 			= "3";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Locations";
$col["name"] 			= "SUPPLIER_LOCATION";
$col["width"] 			= "3";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;


 
$col["title"] 			= "1st Approval";
$col["name"] 			= "1st_Approval";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col['link']			= '?q='.$reportId.'&supplierId={ID}&mode=Confirm';
$col["linkoptions"] 	= "target='_blank'";
$col["linkName"] 		= "Approve";
$col["sortable"] 		= false; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

for($i=2;$i<=$approveLevel;$i++)
{
if($i==2){
	$ap		= "2nd Approval";
	$ap1	= "2nd_Approval";
}
else if($i==3){
	$ap		= "3rd Approval";
	$ap1	= "3rd_Approval";
}
else{
	$ap		= $i."th Approval";
	$ap1	= $i."th_Approval";
}

$col["title"] 			= $ap;
$col["name"] 			= $ap1;
$col["width"] 			= "3";
$col["align"] 			= "center";
$col['link']			= "#";
$col['link']			= '?q='.$reportId.'&supplierId={ID}&mode=Confirm';
$col["linkoptions"] 	= "target='_blank'";
$col["linkName"] 		= "Approve";
$col["sortable"] 		= false; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;
}

 
/*$col["title"] 			= 'Reject'; // caption of column
$col["name"] 			= 'Reject'; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '../addNew/rptSupplier.php?supplierId={ID}&mode=Reject';
$col["linkoptions"] 	= "target='../addNew/rptSupplier.php'";
$col['linkName']		= 'Reject';
$cols[] 				= $col;	
$col					=NULL;
*/
$col["title"] 			= 'Cancel'; // caption of column
$col["name"] 			= 'Cancel'; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '?q='.$reportId.'&supplierId={ID}&mode=Cancel';
$col["linkoptions"] 	= "target='_blank'";
$col['linkName']		= 'Cancel';
$cols[] 				= $col;	
$col					=NULL;

$col["title"] 			= 'Revise'; // caption of column
$col["name"] 			= 'Revise'; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "4";
$col["search"]			= false;
$col["align"] 			= "center";
$col['link']			= '?q='.$reportId.'&supplierId={ID}&mode=Revise';
$col["linkoptions"] 	= "target='_blank'";
$col['linkName']		= 'Revise';
$cols[] 				= $col;	
$col					=NULL;

//VIEW
$col["title"] 			= "Report"; // caption of column
$col["name"] 			= "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "2";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '?q='.$reportId.'&supplierId={ID}';
$col["linkoptions"] 	= "target='_blank'";
$cols[] 				= $col;	
$col					=NULL;


$grid["caption"] 		= "Supplier Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'NAME'; // by default sort grid by this field
$grid["sortorder"] 		= "ASC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"Supplier Listing");
$grid["export"]["range"]= "filtered";

$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"STATUS","op":"eq","data":"Pending"}
     ]
	 
}
SEARCH_JSON;

$grid["search"] = true; 
$grid["postData"] = array("filters" => $sarr ); 

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Supplier Listing</title>
<?php 
		include "include/listing.html";
	?>
</head>
<body>	
<form id="frmlisting" name="frmlisting" method="post" action="">
    
        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
       
</form>
</body>

<?php
function GetMaxApproveLevel()
{
	global $db;	
	$appLevel	= 0;
	
	$sql = "SELECT
			COALESCE(Max(mst_supplier.intApproveLevel),0) AS appLevel
			FROM mst_supplier";	
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);	
	return $row['appLevel'];
}
?>
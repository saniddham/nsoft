<?php
(define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');
//include_once $backwardseperator."libraries/jqdrid/inc/jqgrid_dist.php";
require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";

$company 	= $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];

//$programName='Material Issued Note';
//$programCode='P0231';
$intUser  = $_SESSION["userId"];

//$userDepartment=getUserDepartment($intUser);
//$approveLevel = (int)getMaxApproveLevel();

$sql = "select 

t.sortBy,
t.latestName, 
t.intAutoId,
t.intId,
t.strCode,
t.supplierName,
t.intTypeId,
t.strAddress,
t.strContactPerson,
t.strPrintOnCheque,
t.strCity,
t.strCountryName,
t.currency,
t.strPhoneNo,
t.strMobileNo,
t.strFaxNo,
t.strEmail,
t.strWebSite,
t.shipment_method,
t.strIcCode,
t.strVatNo,
t.strSVatNo,
t.strRegistrationNo,
t.strInvoiceType,
t.strAccNo,
t.payment_term,
t.payment_method,
t.intCreditLimit,
t.chart_of_account,
t.strLeadTime,
t.strBlocked,
t.intRank,
t.`Status`,
t.intCreator,
t.dtmCreateDate,
t.interCompOrNot,
t.company,
t.location,
t.dtmDateUpdateOrDelete,
t.UserUpdateOrDelete,
t.strType


 from(select 
mst_supplier.strName as latestName, 
tb2.sortBy,
tb2.intAutoId,
tb2.intId,
tb2.strCode,
tb2.supplierName,
tb2.intTypeId,
tb2.strAddress,
tb2.strContactPerson,
tb2.strPrintOnCheque,
tb2.strCity,
tb2.strCountryName,
tb2.currency,
tb2.strPhoneNo,
tb2.strMobileNo,
tb2.strFaxNo,
tb2.strEmail,
tb2.strWebSite,
tb2.shipment_method,
tb2.strIcCode,
tb2.strVatNo,
tb2.strSVatNo,
tb2.strRegistrationNo,
tb2.strInvoiceType,
tb2.strAccNo,
tb2.payment_term,
tb2.payment_method,
tb2.intCreditLimit,
tb2.chart_of_account,
tb2.strLeadTime,
tb2.strBlocked,
tb2.intRank,
tb2.`Status`,
tb2.intCreator,
tb2.dtmCreateDate,
tb2.interCompOrNot,
tb2.company,
tb2.location,
tb2.dtmDateUpdateOrDelete,
tb2.UserUpdateOrDelete,
tb2.strType
 from ((SELECT 
concat(history_mst_supplier.intId,'/',dtmDateUpdateOrDelete) as sortBy,
history_mst_supplier.intAutoId,
history_mst_supplier.intId,
history_mst_supplier.strCode,
history_mst_supplier.strName as supplierName,
history_mst_supplier.intTypeId,
history_mst_supplier.strAddress,
history_mst_supplier.strContactPerson,
history_mst_supplier.strPrintOnCheque,
history_mst_supplier.strCity,
mst_country.strCountryName,
mst_financecurrency.strCode as currency,
history_mst_supplier.strPhoneNo,
history_mst_supplier.strMobileNo,
history_mst_supplier.strFaxNo,
history_mst_supplier.strEmail,
history_mst_supplier.strWebSite,
mst_shipmentmethod.strName as shipment_method,
history_mst_supplier.strIcCode,
history_mst_supplier.strVatNo,
history_mst_supplier.strSVatNo,
history_mst_supplier.strRegistrationNo,
history_mst_supplier.strInvoiceType,
history_mst_supplier.strAccNo,
mst_financepaymentsterms.strName as payment_term,
mst_financepaymentsmethods.strName as payment_method,
history_mst_supplier.intCreditLimit,
mst_financechartofaccounts.strName as chart_of_account,
history_mst_supplier.strLeadTime,
history_mst_supplier.strBlocked,
history_mst_supplier.intRank,
if(history_mst_supplier.strType='DELETE','DELETED',if(history_mst_supplier.strType='UPDATE','UPDATED','LATEST')) AS `Status`,
history_mst_supplier.intCreator,
history_mst_supplier.dtmCreateDate,
if(history_mst_supplier.intInterCompany=1,'Yes','No') as interCompOrNot,
if(history_mst_supplier.intInterCompany=1,mst_companies.strName,'') as company,
if(history_mst_supplier.intInterCompany=1,mst_locations.strName,'') as location,
history_mst_supplier.dtmDateUpdateOrDelete,
sys_users.strUserName as UserUpdateOrDelete,
history_mst_supplier.strType
FROM
history_mst_supplier
left Join mst_country ON history_mst_supplier.intCountryId = mst_country.intCountryID
left Join mst_financecurrency ON history_mst_supplier.intCurrencyId = mst_financecurrency.intId 
left Join mst_shipmentmethod ON history_mst_supplier.intShipmentId = mst_shipmentmethod.intId
left Join mst_financepaymentsterms ON history_mst_supplier.intPaymentsTermsId = mst_financepaymentsterms.intId
left Join mst_financepaymentsmethods ON history_mst_supplier.intPaymentsMethodsId = mst_financepaymentsmethods.intId
left Join mst_financechartofaccounts ON history_mst_supplier.intChartOfAccountId = mst_financechartofaccounts.intId
left Join mst_companies ON history_mst_supplier.intCompanyId = mst_companies.intId
left Join mst_locations ON history_mst_supplier.intInterLocation = mst_locations.intId 
left Join sys_users ON history_mst_supplier.intUserUpdateOrDelete = sys_users.intUserId
 )

UNION 

(SELECT 
concat(mst_supplier.intId,'/','9999-99-99 99:99:99') as sortBy,
0 as intAutoId,
mst_supplier.intId,
mst_supplier.strCode,
mst_supplier.strName as supplierName,
mst_supplier.intTypeId,
mst_supplier.strAddress,
mst_supplier.strContactPerson,
mst_supplier.strPrintOnCheque,
mst_supplier.strCity,
mst_country.strCountryName,
mst_financecurrency.strCode as currency,
mst_supplier.strPhoneNo,
mst_supplier.strMobileNo,
mst_supplier.strFaxNo,
mst_supplier.strEmail,
mst_supplier.strWebSite,
mst_shipmentmethod.strName as shipment_method,
mst_supplier.strIcCode,
mst_supplier.strVatNo,
mst_supplier.strSVatNo,
mst_supplier.strRegistrationNo,
mst_supplier.strInvoiceType,
mst_supplier.strAccNo,
mst_financepaymentsterms.strName as payment_term,
mst_financepaymentsmethods.strName as payment_method,
mst_supplier.intCreditLimit,
mst_financechartofaccounts.strName as chart_of_account,
mst_supplier.strLeadTime,
mst_supplier.strBlocked,
mst_supplier.intRank,
'LATEST' AS `Status`,
mst_supplier.intCreator,
mst_supplier.dtmCreateDate,
if(mst_supplier.intInterCompany=1,'Yes','No') as interCompOrNot,
if(mst_supplier.intInterCompany=1,mst_companies.strName,'') as company,
if(mst_supplier.intInterCompany=1,mst_locations.strName,'') as location,
'' as dtmDateUpdateOrDelete,
'' as UserUpdateOrDelete,
'LATEST' as strType
FROM  
mst_supplier  
left Join mst_country ON mst_supplier.intCountryId = mst_country.intCountryID
left Join mst_financecurrency ON mst_supplier.intCurrencyId = mst_financecurrency.intId 
left Join mst_shipmentmethod ON mst_supplier.intShipmentId = mst_shipmentmethod.intId
left Join mst_financepaymentsterms ON mst_supplier.intPaymentsTermsId = mst_financepaymentsterms.intId
left Join mst_financepaymentsmethods ON mst_supplier.intPaymentsMethodsId = mst_financepaymentsmethods.intId
left Join mst_financechartofaccounts ON mst_supplier.intChartOfAccountId = mst_financechartofaccounts.intId
left Join mst_companies ON mst_supplier.intCompanyId = mst_companies.intId
left Join mst_locations ON mst_supplier.intInterLocation = mst_locations.intId )
 ) as tb2  inner join mst_supplier on mst_supplier.intId=tb2.intId 
order by sortBy desc 
) as t where 1=1
						";
					  	  //   echo $sql;
						 
$col = array();

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
//edittype
$col["stype"] 	= "select";
$str = ":All;DELETED:Deleted;UPDATED:Updated;LATEST:Latest" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;	$col=NULL;

//PO No
$col["title"] 	= "Supplier Code"; // caption of column
$col["name"] 	= "strCode"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
//$col['link']	= "../addNew/issue.php?issueNo={Issue_No}&year={Issue_Year}";	 
//$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

//$reportLink  = "rptIssue.php?issueNo={Issue_No}&year={Issue_Year}";
//$reportLinkApprove  = "rptIssue.php?issueNo={Issue_No}&year={Issue_Year}&approveMode=1";

$cols[] = $col;	$col=NULL;



//PO Year
$col["title"] = "Supplier Name"; // caption of column
$col["name"] = "supplierName"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//Supplier
$col["title"] = "Country"; // caption of column
$col["name"] = "strCountryName"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//User
$col["title"] = "Phone"; // caption of column
$col["name"] = "strPhoneNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//VIEW
$col["title"] = "Mobile"; // caption of column
$col["name"] = "strMobileNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//VIEW
$col["title"] = "Inter Company"; // caption of column
$col["name"] = "interCompOrNot"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//VIEW
$col["title"] = "Company"; // caption of column
$col["name"] = "company"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//VIEW
$col["title"] = "Location"; // caption of column
$col["name"] = "location"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//VIEW
$col["title"] = "User (Update/Delete)"; // caption of column
$col["name"] = "UserUpdateOrDelete"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//VIEW
$col["title"] = "Date (Update/Delete)"; // caption of column
$col["name"] = "dtmDateUpdateOrDelete"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

$col = array();
$col["title"] = "Closed";
$col["name"] = "intId";
$col["width"] = "50";
$col["editable"] = true;
$col["edittype"] = "checkbox"; // render as checkbox
$col["editoptions"] = array("value"=>"Yes:No"); // with these values "checked_value:unchecked_value"
$cols[] = $col;


/*//VIEW
$col["title"] = "Inter Company"; // caption of column
$col["name"] = "strMobileNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;


*/




$jq 	= new jqgrid('',$db);	
$col 	= array();
$cols 	= array();

$grid["caption"] 		= "Supplier Modification Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'sortBy'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


$grid["grouping"] = true; // 
$grid["groupingView"] = array();
$grid["groupingView"]["groupField"] = array("intId"); // specify column name to group listing
$grid["groupingView"]["groupColumnShow"] = array(false); // either show grouped column in list or not (default: true)
/*$grid["groupingView"]["groupText"] = array("<b>Supplier Records(s) - {0} - {1}</b>"); // {0} is grouped value, {1} is count in group
*/
$grid["groupingView"]["groupText"] = array("<b>Supplier Records(s) - {1}</b>"); // {0} is grouped value, {1} is count in group
$grid["groupingView"]["groupOrder"] = array("asc"); // show group in asc or desc order
$grid["groupingView"]["groupDataSorted"] = array(true); // show sorted data within group
$grid["groupingView"]["groupSummary"] = array(true); // work with summaryType, summaryTpl, see column: $col["name"] = "total";
$grid["groupingView"]["groupCollapse"] = false; // Turn true to show group collapse (default: false) 
$grid["groupingView"]["showSummaryOnHide"] = true; // show summary row even if group collapsed (hide) 


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);




$out = $jq->render("list1");

include "include/listing.html";

?>
<form id="frmlisting" name="frmlisting" method="post" >
    <div align="center" style="margin:10px">
        <?php echo $out?>
    </div>
</form>
<?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(ware_issueheader.intApproveLevels) AS appLevel
			FROM ware_issueheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
//------------------------------function load User Department---------------------
function getUserDepartment($intUser){
	global $db;

	$sqlp = "SELECT
			sys_users.intDepartmentId
			FROM
			sys_users
			WHERE
			sys_users.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['intDepartmentId'];

}
?>


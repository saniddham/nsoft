<?php
(define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');
//include "include/javascript.html";

$supId = $_REQUEST['id'];
$newCode=getMaxCode();
$obsoleteCurrency = array("BDT","Franc","INR");

?>
<script type="application/javascript" >
var supId = '<?php echo $supId ?>';
</script>
<title>Supplier Details</title>

<!--<body onLoad="functionList();">-->
<form id="frmSupplier" name="frmSupplier" method="post" autocomplete="off">
<div align="center">
		<div class="trans_layoutD">
		  <div class="trans_text">Supplier Details</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="585" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Supplier</td>
                <td width="442" colspan="3">
                <select <?Php if(!($form_permision['edit'] || $form_permision['delete'])){echo 'disabled';}  ?> name="cboSearch" id="cboSearch"  style="width:100%" tabindex="1">
                 <option value=""></option>
                 <?php  $sql = "SELECT
								mst_supplier.intId,
								mst_supplier.strName
								FROM mst_supplier
								order by strName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?> 
                </select></td>
              </tr>
              <tr>
                <td width="1" class="normalfnt">&nbsp;</td>
                <td width="116" class="normalfnt">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td colspan="4" class="normalfnt"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="tableBorder">
                    <span class="normalfnt"><strong>General</strong></span></td>
                    </tr>
                  <tr class="">
                    <td width="21%" class="normalfnt">Supplier Code&nbsp;<span class="compulsoryRed">*</span></td>
                    <td width="32%"><input name="txtCode" type="text" class="validate[required,maxSize[10]]" id="txtCode" style="width:140px" maxlength="10" tabindex="2" value="<?php echo $newCode ; ?>"/></td>
                    <td width="18%" class="normalfnt">Type&nbsp;<span class="compulsoryRed">*</span></td>
                    <td width="29%"><select name="cboType" id="cboType" style="width:158px"  tabindex="3"  class="txtbox validate[required]">
                      <option value=""></option>
                      <?php  $sql = "SELECT
						intId,
						strName
						FROM mst_typeofsupplier
						WHERE
							intStatus = 1
						order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
        				?>
                    </select></td>
                    </tr>
                  <tr class="">
                    <td class="normalfnt">Supplier Name&nbsp;<span class="compulsoryRed">*</span></td>
                    <td colspan="3"><input name="txtName" class="validate[required,maxSize[50]]" type="text" id="txtName" style="width:100%" maxlength="50" tabindex="4"/></td>
                    </tr>
                  <tr class="">
                    <td class="normalfnt">Address</td>
                    <td colspan="3"><textarea name="txtAddress" style="width:100%"  rows="2" id="txtAddress" class="validate[maxSize[250]]" tabindex="5"></textarea></td>
                    </tr>
<tr class="">
                    <td class="normalfnt">City</td>
                    <td><input name="txtCity" type="text" id="txtCity" style="width:140px" class="validate[maxSize[200]]" maxlength="200" tabindex="8"/></td>
                    <td class="normalfnt">Country <span class="compulsoryRed">*</span></td>
                    <td><select name="cboCountry" id="cboCountry" style="width:158px" class="validate[required]" tabindex="9">
                      <option value=""></option>
                      <?php  $sql = "SELECT
									mst_country.intCountryID,
									mst_country.strCountryName
								FROM mst_country
								WHERE
									intStatus = 1
									order by strCountryName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intCountryID']."\">".$row['strCountryName']."</option>";
								}
                   ?>
                    </select></td>
                    </tr>                  <tr class="">
                    <td class="normalfnt">Contact Person</td>
                    <td colspan="3">
                    <textarea name="txtContact" style="width:100%; height:20px"  rows="2" id="txtContact" class="validate[maxSize[250]]" tabindex="6"></textarea></td>
                    </tr>
                  <tr class="">
                    <td class="normalfnt">Print On Cheque</td>
                    <td colspan="3">
                    <input name="txtPrintCheque" type="text" id="txtPrintCheque" style="width:100%" class="validate[maxSize[250]]" maxlength="250" tabindex="7"></td>
                    </tr>
                  
                  <tr class="">
                    <td class="normalfnt"> Currency <span class="compulsoryRed">*</span></td>
                    <td><select name="cboCurrency"  id="cboCurrency" style="width:140px" class="validate[required]" tabindex="10">
                      <option value=""></option>
                      <?php  $sql = "SELECT
						intId,
						strCode
						FROM mst_financecurrency
						WHERE
							intStatus = 1
						order by strCode
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
                            if ($_SESSION["headCompanyId"] == '1') {
                                if (!in_array($row['strCode'], $obsoleteCurrency)) {
                                    echo "<option value=\"" . $row['intId'] . "\">" . $row['strCode'] . "</option>";
                                }
                            } else {
                                echo "<option value=\"" . $row['intId'] . "\">" . $row['strCode'] . "</option>";
                            }
						}
        				?>
                    </select></td>
                    <td class="normalfnt">Color Code</td>
                   <td><select name="cboColorCode" id="cboColorCode" style="width:158px">
                    <option value=""></option>
                   <option value="B2F7D0" style="background-color:#B2F7D0">B2F7D0</option>
                   <option value="D9E2F7" style="background-color:#D9E2F7">D9E2F7</option>
                   <option value="FCECE3" style="background-color:#FCECE3">FCECE3</option>
                   <option value="EAEDEF" style="background-color:#EAEDEF">EAEDEF</option>
                   </select></td>
                  </tr>
                  </table></td>
              </tr>
              <tr>
              	<td>&nbsp;</td>
                <td colspan="5"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="tableBorder">
                    <span class="normalfnt"><strong>Communication</strong></span></td>
                  </tr>
                  <tr>
                    <td width="21%"><span class="normalfnt">Phone No</span></td>
                    <td width="31%"><input name="txtPhone" type="text" id="txtPhone" style="width:140px"  tabindex="11" class="validate[maxSize[100]]" maxlength="100"/></td>
                    <td width="19%"><span class="normalfnt">Mobile No</span></td>
                    <td width="29%"><input name="txtMobile" type="text" id="txtMobile" style="width:158px"  tabindex="12" class="validate[maxSize[100]]" maxlength="100"/></td>
                  </tr>
                  <tr>
                    <td><span class="normalfnt">Fax</span></td>
                    <td><input name="txtFax" type="text" id="txtFax" style="width:140px" tabindex="13"  class="validate[maxSize[100]]" maxlength="100"/></td>
                    <td><span class="normalfnt">E-Mail</span></td>
                    <td>
              <input name="txtEMail" type="text" id="txtEMail" class="validate[custom[email]]" style="width:158px" tabindex="14" /></td>
                  </tr>
                  <tr>
                    <td><span class="normalfnt">Website</span></td>
                    <td colspan="3">
                   <input name="txtWeb" class="validate[custom[url]]" type="text"  id="txtWeb" style="width:100%"  tabindex="15"/></td>
                    </tr>
                  <tr>
                    <td><span class="normalfnt">Shipment Method&nbsp;<span class="compulsoryRed">*</span></span></td>
                    <td><select name="cboShipmentMethod" id="cboShipmentMethod" style="width:140px" tabindex="16" class="validate[required]" >
                      <option value=""></option>
                      <?php  $sql = "SELECT
						intId,
						strName
						FROM mst_shipmentmethod
						WHERE
							intStatus = 1
						order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
        				?>
                    </select></td>
                    <td><span class="normalfnt">IC Partner Code</span></td>
                    <td>
              <input name="txtIcCode" type="text" id="txtIcCode" style="width:158px" tabindex="17"  class="validate[maxSize[50]]" maxlength="50"></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td rowspan="3" class="normalfnt">&nbsp;</td>
                <td colspan="4" class="normalfnt"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="tableBorder">
                    <span class="normalfnt"><strong>Legal Information</strong></span></td>
                    </tr>
                  <tr>
                    <td width="21%" class="normalfnt">VAT Reg No. <span class="compulsoryRed">*</span></td>
                    <td width="31%"><input name="txtVatNo" class="validate[required,maxSize[100]]" type="text" id="txtVatNo" style="width:140px" tabindex="18" maxlength="100" /></td>
                    <td width="19%" class="normalfnt">SVAT No. <span class="compulsoryRed">*</span></td>
                    <td width="29%"><input name="txtSvatNo" class="validate[required,maxSize[100]]" type="text" id="txtSvatNo" style="width:158px" tabindex="19" maxlength="100" /></td>
                    </tr>
                  <tr>
                    <td class="normalfnt">Business Reg No</td>
                    <td><input name="txtRegNo" type="text" id="txtRegNo" style="width:140px" tabindex="20"   class="validate[maxSize[100]]" maxlength="100"/></td>
                    <td class="normalfnt">Invoice Type <span class="compulsoryRed">*</span></td>
                    <td><select name="cboInvoiceType" id="cboInvoiceType" style="width:100%" class="validate[required]">
                      <option value=""></option>
                      <?php  $sql = "SELECT
						intId,
						strInvoiceType
						FROM mst_invoicetype
						WHERE
							intStatus = 1
						order by strInvoiceType
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strInvoiceType']."</option>";
						}
        				?>
                    </select></td>
                    </tr>
                  <tr>
                    <td class="normalfnt">Finance Type</td>
                    <td colspan="3" class="normalfnt"><select name="cboFinanceType" id="cboFinanceType" style="width:100%">
                        <option value=""></option>
                        <?php
                                    	$sql = "SELECT FINANCE_TYPE_ID,FINANCE_TYPE_NAME
                                    			FROM finance_mst_account_type
                                    			WHERE STATUS=1
                                    			ORDER BY FINANCE_TYPE_NAME";
                                    	$result = $db->RunQuery($sql);
										while($row=mysqli_fetch_array($result))
										{
											echo "<option value=\"".$row["FINANCE_TYPE_ID"]."\">".$row["FINANCE_TYPE_NAME"]."</option>";
										}
                                    ?>
                      </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Main Type</td>
                    <td colspan="3" class="normalfnt"><select name="cboMainType" id="cboMainType" style="width:100%">
                      </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Sub Type</td>
                    <td colspan="3" class="normalfnt"><select name="cboSubType" id="cboSubType" style="width:100%">
                      </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Chart Of Account</td>
                    <td colspan="3" class="normalfnt"><input name="txtChartOfAccount" maxlength="255" class="" type="text" id="txtChartOfAccount" style="width:100%" /><input  name="txtChartOfAccountDB" class="" type="text" id="txtChartOfAccountDB" style="width:100px;display:none" /></td>
                  </tr>
                  </table></td>
              </tr>
              <tr>
                <td colspan="4" class="normalfnt"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="tableBorder">
                    <span class="normalfnt"><strong>Payments</strong></span></td>
                  </tr>
                  <tr>
                    <td width="21%" class="normalfnt">Accounts No.    </td>
                    <td width="29%">
                  <input name="txtAccNo" type="text" id="txtAccNo" style="width:140px" tabindex="24"  class="validate[maxSize[100]]" maxlength="100"/></td>
                    <td width="21%" class="normalfnt">Payments Terms&nbsp;<span class="compulsoryRed">*</span></td>
                    <td width="29%"><select name="cboPaymentsTerms" id="cboPaymentsTerms" style="width:158px" tabindex="25"  class="validate[required]">
                      <option value=""></option>
                      <?php  $sql = "SELECT
						intId,
						strName
						FROM mst_financepaymentsterms
						WHERE
							intStatus = 0
						order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']." days"."</option>";
						}
        				?>
                    </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Credit Limit</td>
                    <td><input name="txtCreditLimit" type="text" id="txtCreditLimit" style="width:140px" tabindex="26" class="validate[custom[number],min[0]]" value="0"  /></td>
                    <td class="normalfnt">Payments Method&nbsp;<span class="compulsoryRed">*</span></td>
                    <td><select name="cboPaymentsMethods" id="cboPaymentsMethods" style="width:158px" tabindex="27"  class="validate[required]" >
                      <option value=""></option>
                      <?php  $sql = "SELECT
						intId,
						strName
						FROM mst_financepaymentsmethods
						WHERE
							intStatus = 1
						order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
        				?>
                    </select></td>
                  </tr>
                  <tr style="display:none">
                    <td class="normalfnt">Ledger Accounts</td>
                    <td colspan="2"><select name="cboChartOfAcc" id="cboChartOfAcc" style="width:158px" tabindex="28" >
                      <option value=""></option>
                      <?php  $sql = "SELECT
						intId,
						strCode,
						strName
						FROM mst_financechartofaccounts
						WHERE
						intStatus = '1' AND  intFinancialTypeId = '18' AND strType = 'Posting'
						order by strCode
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";
						}
        			?>
                    </select></td>
                    <td>&nbsp;</td>
                  </tr>
                </table>
                  <table width="100%" border="0" class="tableBorder">
                    <tr>
                      <td width="100%" colspan="4" align="left" bgcolor="#E4E4E4" class="tableBorder">
                      <span class="normalfnt"><strong>Receiving</strong></span></td>
                    </tr>
                    <tr>
                      <td colspan="4" align="left">
                        <table width="100%">
                          <tr>
                            <td width="46%"><img id="butLocations" src="images/add_new.png" width="15" height="15" /><div style="width:250px;height:250px;overflow:scroll" ><table id="tblLocations" width="100%" border="0" cellpadding="0" cellspacing="1" class="bordered">
                              <thead>
                                <tr>
                                  <th width="8%" height="19" align="center" bgcolor="#FAD163" class="normalfntMid"></th>
                                  <th width="92%" bgcolor="#FAD163" class="normalfntMid">Location</th>
                                  </tr>
                                </thead>
                              <tbody>
                                <?php
	 	 		 $sql = "SELECT
				 intId,
				 strName
				 FROM mst_supplier_locations_header
				 WHERE
				 intStatus = '1' 
				 ORDER BY strName ASC";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
	  			 ?>
                                <tr class="normalfnt">
                                  <td align="center" bgcolor="#FFFFFF" class="normalfntMid">
                                    <input type="checkbox" class="locations" name="loc<?php echo $row['intId'];?>" id="loc<?php echo $row['intId'];?>" value="<?php echo $row['intId'];?>" /></td>
                                  <td bgcolor="#FFFFFF"><?php echo $row['strName'];?></td>
                                  </tr>
                                <?php 
        		 } 
       			 ?>
                                </tbody>
                              </table></div></td>
                            <td width="4%"></td>
                            <td width="21%"  class="normalfnt" valign="bottom">Lead Time </td>
                            <td width="29%" valign="bottom"><input name="txtLeadTime" type="text" id="txtLeadTime" style="width:158px" tabindex="29"  class="validate[maxSize[100]]" maxlength="100"/></td>
                            </tr>
                          </table>
                        </td>
                    </tr>
                    </table></td>
                </tr>
              <tr>
                <td colspan="4" class="normalfnt"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="tableBorder">
                    <span class="normalfnt"><strong>Other</strong></span></td>
                    </tr>
                  <tr>
                    <td width="20%" class="normalfnt">Blocked</td>
                    <td width="30%" class="normalfnt">
                    <select name="cboBlocked" class="txtbox" id="cboBlocked" style="width:140px" tabindex="30" >
                      <option>All</option>
                      <option>None</option>
                      <option>Payments</option>
                      </select></td>
                    <td width="18%">&nbsp;</td>
                    <td width="32%">&nbsp;</td>
                    </tr>
                  <tr class="" style="display:none">
                    <td class="normalfnt">Rank</td>
                    <td colspan="3">
  <input name="txtRank" type="text" id="txtRank" style="width:20px" class="validate[custom[integer],min[0],max[10],maxSize[2]]" tabindex="31" maxlength="3"/>
  <span class="normalfnt" style="color:#CCC;font-size:9px">1 to 10. 1=Super ,10=Blocked</span></td>
                  </tr>
                  <tr class="">
                    <td class="normalfnt">Inter Company</td>
                    <td colspan="3"><input type="checkbox" name="chkInterComp" id="chkInterComp"  tabindex="33"/></td>
                  </tr> 
                    <tr class="">
                    <td class="normalfnt">Company</td>
                    <td ><select name="cboInterCompany" class="txtbox" id="cboInterCompany" style="width:140px" disabled="disabled"  tabindex="34">
                      <option value=""></option>
                      <?php  $sql = "SELECT
									mst_companies.intId,
									mst_companies.strName,
									mst_companies.strCode
									FROM mst_companies
									WHERE
									mst_companies.intStatus =  '1'
									ORDER BY 
									strCode, strName ASC 
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";
						}
        			?>
                      </select></td>
                    <td class="normalfnt">Location</td>
                    <td ><select name="cboInterLocation" tabindex="35" class="txtbox" id="cboInterLocation" style="width:158px" disabled="disabled" >
                      <option value=""></option>
                      <?php  $sql = "SELECT
									mst_locations.strName,
									mst_locations.intId,
									mst_locations.strCode
									FROM mst_locations
									WHERE
									mst_locations.intStatus =  '1'
									ORDER BY 
									strCode, strName ASC 
									";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";
						}
        			?>
                      </select></td>
                    </tr>                    
                  </table></td>
              </tr>
              </table></td>
            </tr>
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor="">
                    <a class="button white medium" alt="New" name="butNew" width="92" height="24" id="butNew" tabindex="28">New</a>
                    <?php
                if(($form_permision['add']||$form_permision['edit']) and $intStatus != 1)
				{
				?>
                    <a class="button white medium"  style="display:inline"   alt="Save" name="butSave"width="92" height="24"   id="butSave" tabindex="24">Save</a><?php } 
				
				?>
                    <a class="button white medium"  style=" <?php if($intStatus>1){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>"   alt="Approve" name="butConfirm"width="92" height="24"   id="butConfirm" tabindex="30">Approve</a>
                    <a class="button white medium"  style="display:none"   alt="Report" name="butReport"width="92" height="24"   id="butReport" tabindex="31">Report</a>
                    <a class="button white medium" href="main.php"  alt="Close" name="butClose" width="92" height="24" border="0"   id="butClose" tabindex="27">Close</a></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
  <!-- this is supplier location popup window -->
	<div style="position: absolute;display:none;z-index:100"  id="popupContact1">
    <iframe onload="loadLocations();"   id="iframeMain1" name="iframeMain1" src="header_db.php?q=349&iframe=1&clearCash=1" style="width:800px;height:400px;border:0;overflow:hidden">
    </iframe>
    </div>
    
	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</form>

<?php
function getMaxCode()
{
	global $db;
	$sql = "SELECT
			mst_supplier.intId,
			mst_supplier.strCode
			FROM mst_supplier
			ORDER BY
			mst_supplier.intId DESC 
			limit 1
		";
		//die($sql);
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	$code=$row['strCode'];
	
	$number= preg_match("[^0-9]", "",$code);
	$stringLength= strlen($code)- strlen($number);
	$string=substr($code,0,$stringLength);
	
	$numberLength= strlen($number);
	if($numberLength==0){
		$numberLength=1;
	}
	
	$newNumber= str_pad($row['intId']+1, $numberLength, "0", STR_PAD_LEFT); 
	$newCode=$string.$newNumber;

	return $newCode;
}

?>


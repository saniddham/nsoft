<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include_once "{$backwardseperator}dataAccess/Connector.php";
	include_once  ("../../../../../class/cls_commonErrorHandeling_get.php");
	include_once  ("../../../../../class/cls_commonFunctions_get.php");
 	
	$obj_commonErr					= new cls_commonErrorHandeling_get($db);
	$cls_commonFunctions_get		= new cls_commonFunctions_get($db);
	$progrmCode						= 'P0078';
 	$session_userId					= $_SESSION["userId"];

	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 		= $_REQUEST['requestType'];
	$id 				= $_REQUEST['cboSearch'];
	$code				= trim($_REQUEST['txtCode']);
	$name				= trim($_REQUEST['txtName']);
	$type				= ($_REQUEST['cboType']==''?'NULL':$_REQUEST['cboType']);
	$address			= $cls_commonFunctions_get->replace($_REQUEST['txtAddress']);
	$contact			= $_REQUEST['txtContact'];
	$chequename			= $_REQUEST['txtPrintCheque'];
	$city				= trim($_REQUEST['txtCity']);
	$country			= ($_REQUEST['cboCountry']==''?'NULL':$_REQUEST['cboCountry']);
	$currency			= ($_REQUEST['cboCurrency']==''?'NULL':$_REQUEST['cboCurrency']);
	$phone				= $_REQUEST['txtPhone'];
	$mobile				= $_REQUEST['txtMobile'];
	$fax				= $_REQUEST['txtFax'];
	$email				= trim($_REQUEST['txtEMail']);
	$web				= $_REQUEST['txtWeb'];
	$shipment 			= ($_REQUEST['cboShipmentMethod']==''?'NULL':$_REQUEST['cboShipmentMethod']);
	$partnercode		= $_REQUEST['txtIcCode'];
	$vatNo				= $_REQUEST['txtVatNo'];
	$sVatNo				= $_REQUEST['txtSvatNo'];
	$regNo				= $_REQUEST['txtRegNo'];
	$invoType			= $_REQUEST['cboInvoiceType'];
	$subType			= ($_REQUEST['cboSubType']==''?'NULL':$_REQUEST['cboSubType']);
	$chartOfAccName		= $_REQUEST['txtChartOfAccount'];
	$accNo				= $_REQUEST['txtAccNo'];
	$paymentsterms 		= ($_REQUEST['cboPaymentsTerms']==''?'NULL':$_REQUEST['cboPaymentsTerms']);
	$paymentsmethods 	= ($_REQUEST['cboPaymentsMethods']==''?'NULL':$_REQUEST['cboPaymentsMethods']);
	$creditlimit 		= val($_REQUEST['txtCreditLimit']);
	$chartofaccount		= ($_REQUEST['cboChartOfAcc']==''?'NULL':$_REQUEST['cboChartOfAcc']);
	$leadtime			= $_REQUEST['txtLeadTime'];
	$blocked 			= $_REQUEST['cboBlocked'];
	$rank 				= val($_REQUEST['txtRank']);
	$intStatus			= ($_REQUEST['chkActive']?1:0);
	$interCompany 		= ($_REQUEST['chkInterComp']?1:0);
	$companyId 			= val($_REQUEST['cboInterCompany']);
	$locationId 		= val($_REQUEST['cboInterLocation']);	
	$colorCode 			= $_REQUEST['cboColorCode'];

	$approve_levels		= GetApproveLevel1($progrmCode);
	$intStatus			= $approve_levels+1;
	
	if($companyId==''){
		$companyId=0;
	}
	if($locationId==''){
		$locationId=0;
	}
	//-----------------------------------------------
		$locations = json_decode($_REQUEST['location'], true);
	//-----------------------------------------------
	///////// supplier insert part /////////////////////
	if($requestType=='add')
	{
 		$header_arr		= headerArray($id);
 		$permision_arr	= $obj_commonErr->get_permision_withApproval_save($header_arr['status'],$header_arr['levels'],$session_userId,$progrmCode,'RunQuery');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
			echo json_encode($response);
			return;
		}
	
		$sql 	= "INSERT INTO `mst_supplier` (`strCode`,`strName`,`intTypeId`,`strAddress`,`strContactPerson`,`strPrintOnCheque`,`strCity`,`intCountryId`,`intCurrencyId`,`strPhoneNo`,`strMobileNo`,`strFaxNo`,`strEmail`,`strWebSite`,`intShipmentId`,`strIcCode`,`strVatNo`,`strSVatNo`,`strRegistrationNo`,`strInvoiceType`,`strAccNo`,`intPaymentsTermsId`,`intPaymentsMethodsId`,`intCreditLimit`,`intChartOfAccountId`,`strLeadTime`,`strBlocked`,`intRank`,`intStatus`,`intApproveLevel`,`intCreator`,`dtmCreateDate`,`intInterCompany`,`intInterLocation`,`intCompanyId`,`strColorCode`,SUB_TYPE,CHART_OF_ACCOUNT) 
					VALUES ('$code','$name',$type,'$address','$contact','$chequename','$city',$country,$currency,'$phone','$mobile','$fax','$email','$web',$shipment,'$partnercode','$vatNo','$sVatNo','$regNo','$invoType','$accNo',$paymentsterms,$paymentsmethods,'$creditlimit',$chartofaccount,'$leadtime','$blocked','$rank','$intStatus','$approve_levels','$userId',now(),'$interCompany','$locationId','$companyId','$colorCode',$subType,'$chartOfAccName')";	
				
		$result1 	= $db->RunQuery($sql);
		if($result1)
		$supId		=	$db->insertId;
		
		
		if($supId){
		if(count($locations) != 0)
		{
		foreach($locations as $locat)
		{
			$locId	= $locat['locationId'];
			$sql	= "INSERT INTO `mst_supplier_locations`
						(`intLocationId`,`intSupplierId`,`intCreator`,dtmCreateDate) 
						VALUES 
						('$locId','$supId','$userId',now())";
			$result = $db->RunQuery($sql);
		}
		}
		}
		
		if($result || $supId){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
			UpdateApprovebyStatus($supId);
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	/////////// supplier update part /////////////////////
	else if($requestType=='edit')
	{
 		$header_arr		= headerArray($id);
 		$permision_arr	= $obj_commonErr->get_permision_withApproval_save($header_arr['status'],$header_arr['levels'],$session_userId,$progrmCode,'RunQuery');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
			echo json_encode($response);
			return;
		}
		
		$sql = "DELETE FROM `mst_supplier_locations` WHERE (`intSupplierId`='$id')";
		$db->RunQuery($sql);
				
		foreach($locations as $locat)
		{
			$locId	= $locat['locationId'];
			$sql	= "INSERT INTO `mst_supplier_locations`
						(`intLocationId`,`intSupplierId`,`intCreator`,dtmCreateDate) 
						VALUES 
						('$locId','$id','$userId',now())";
			$db->RunQuery($sql);
		}
		
		$sql = "UPDATE `mst_supplier` SET 	strCode					='$code',
											strName					= '$name',
											intTypeId				= $type,
											strAddress				= '$address',
											strContactPerson		= '$contact',
											strPrintOnCheque		= '$chequename',
											strCity					= '$city',
											intCountryId			= $country,
											intCurrencyId			= $currency,
											strPhoneNo				= '$phone',
											strMobileNo				= '$mobile',
											strFaxNo				= '$fax',
											strEmail				= '$email',
											strWebSite				= '$web',
											intShipmentId			= $shipment,
											strIcCode				= '$partnercode',
											strVatNo				= '$vatNo',
											strSVatNo				= '$sVatNo',
											strRegistrationNo		= '$regNo',
											strInvoiceType			= '$invoType',
											strAccNo				= '$accNo',
											intPaymentsTermsId		= $paymentsterms,
											intPaymentsMethodsId	= $paymentsmethods,
											intCreditLimit			= '$creditlimit',
											intChartOfAccountId		= $chartofaccount,
											strLeadTime				= '$leadtime',
											strBlocked				= '$blocked',
											intRank					= '$rank',
											intStatus				= '$intStatus',
											intApproveLevel			= '$approve_levels',
											intModifyer				= '$userId',
											intInterCompany			= '$interCompany',
											intInterLocation		= '$locationId',
											intCompanyId			= '$companyId',
											strColorCode			= '$colorCode',
											SUB_TYPE				= $subType,
											CHART_OF_ACCOUNT		= '$chartOfAccName' 
 				WHERE (`intId`='$id')";
		$result = $db->RunQuery($sql);
		
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
			UpdateApprovebyStatus($id);
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}

	else if($requestType=='approve')
	{	
		$rollBack=0;
 		$supplierId			= $_REQUEST["supplierId"];
		$chartOfAccName		= $_REQUEST["chartOfAccName"];
 		$subType			= ($_REQUEST['subType']==''?'NULL':$_REQUEST['subType']);
		$savedStatus 		= true;
		$errorMsg			= '';
		
 		$header_arr			= headerArray($supplierId);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_arr['status'],$header_arr['levels'],$session_userId,$progrmCode,'RunQuery');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
			echo json_encode($response);
			return;
		}
 		else if(($header_arr['status']==2) && (($chartOfAccName=='') || ($subType==NULL))){
			$rollBack			=	1;
			$response['msg'] 	=	'Please select sub type and Chart of account name';	
			$response['type'] 	=	'fail';
			echo json_encode($response);
			return;
		}
 
		$db->begin();
		
		$approve_level	= GetMaxApproveLevel($supplierId);
 		$sql 			= "UPDATE mst_supplier SET intStatus = intStatus - 1,SUB_TYPE=$subType ,CHART_OF_ACCOUNT='$chartOfAccName' WHERE intId = '$supplierId' ";
		$result 		= $db->RunQuery2($sql);
		if(!$result && $savedStatus){
			$savedStatus 	= false;
			$errorMsg		= $db->errormsg;
			$sqlE=$sql;
		}
		
 		if($header_arr['status']==2 && !CheckIsAvailable($supplierId))
		{
 			$maxCode	=getMaxCode($subType);
			$sql 		= "INSERT INTO finance_mst_chartofaccount 
							(CHART_OF_ACCOUNT_CODE, 
							CHART_OF_ACCOUNT_NAME, 
							SUB_TYPE_ID, 
							CATEGORY_TYPE, 
							CATEGORY_ID,
							STATUS,
							CREATED_BY,
							CREATED_DATE)
							VALUES
							('$maxCode', 
							'$chartOfAccName', 
							'$subType', 
							'S', 
							'$supplierId', 
							'1', 
							'$userId', 
							now());";
			$result 	= $db->RunQuery2($sql);
			$accountId=$db->insertId;
 
			if(!$result && $savedStatus){
				$savedStatus 	= false;
				$errorMsg		= $db->errormsg;
				$sqlE			=$sql;
			}
				
			$sql 	= "SELECT mst_companies.intId FROM `mst_companies` WHERE mst_companies.intStatus = 1";
			$result = $db->RunQuery2($sql);
			while($row = mysqli_fetch_array($result))
			{
				$companyId	=	$row['intId'];
				$sqlI 		=	"INSERT INTO finance_mst_chartofaccount_company 
								(COMPANY_ID, 
								CHART_OF_ACCOUNT_ID)
								VALUES
								('$companyId', 
								'$accountId');";
				$resultI 	=	$db->RunQuery2($sqlI);
				if(!$resultI && $savedStatus){
					$savedStatus 	=	false;
					$errorMsg		= $db->errormsg;
					$sqlE			=	$sqlI;
				}
			}
		}
		
	 	$sql = "INSERT INTO mst_supplier_approvedby 
				(intSupplierId, 
				intApproveLevelNo, 
				intApproveUser, 
				dtApprovedDate, 
				intStatus)
				VALUES
				('$supplierId', 
				'$approve_level', 
				'$userId', 
				now(), 
				'0');";
		$result = $db->RunQuery2($sql);
		if(!$result && $savedStatus){
			$savedStatus 	= false;
			$errorMsg		= $db->errormsg;
			$sqlE			= $sql;
		}
			
		if($rollBack==1){
			$response['msg'] 	= $msg;	
			$response['type'] 	= 'fail';
			$db->rollback();
		}
		else if($savedStatus){
			$response['msg'] 	= "Approved successfully.";	
			$response['type'] 	= 'pass';

            if($_SESSION['headCompanyId'] == 1 && isFinalApprovalRaised($supplierId)) {
            	sendDetailsToFinanceModule($supplierId);
            }
			$db->commit();
		}
		else{
			$response['msg'] 	= $errorMsg;	
			$response['type'] 	= 'fail';
			$response['sql'] 	= $sqlE;
			$db->rollback();
		}
	}
	else if($requestType=='revise')
	{	
		$supplierId		= $_REQUEST["supplierId"];
		$savedStatus 	= true;

 		$header_arr			= headerArray($supplierId);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_revise($header_arr['status'],$header_arr['levels'],$session_userId,$progrmCode,'RunQuery');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
			echo json_encode($response);
			return;
		}
	
		$db->begin();
		
		$sql 	= "UPDATE mst_supplier SET intStatus = -1 WHERE intId = '$supplierId' ";
		$result = $db->RunQuery2($sql);
		if(!$result)
			$savedStatus 	= false;
		
		$sql = "INSERT INTO mst_supplier_approvedby 
				(intSupplierId, 
				intApproveLevelNo, 
				intApproveUser, 
				dtApprovedDate, 
				intStatus)
				VALUES
				('$supplierId', 
				'-1', 
				'$userId', 
				now(), 
				'0');";
		$result = $db->RunQuery2($sql);
		if(!$result)
			$savedStatus 	= false;
		
		if($savedStatus){
			$response['msg']	= "Revised successfully.";	
			$response['type']	= 'pass';
			$db->commit();
		}
		else{
			$response['msg']	= "Revision failed.";	
			$response['type']	= 'fail';
			$db->rollback();
		}
	}
	else if($requestType=='cancel')
	{	
		$supplierId		= $_REQUEST["supplierId"];
		$savedStatus 	= true;

 		$header_arr			= headerArray($supplierId);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_cancel($header_arr['status'],$header_arr['levels'],$session_userId,$progrmCode,'RunQuery');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
			echo json_encode($response);
			return;
		}
	
		$db->begin();
		
		$sql 	= "UPDATE mst_supplier SET intStatus = -2 WHERE intId = '$supplierId' ";
		$result = $db->RunQuery2($sql);
		if(!$result)
			$savedStatus 	= false;
		
		$sql = "INSERT INTO mst_supplier_approvedby 
				(intSupplierId, 
				intApproveLevelNo, 
				intApproveUser, 
				dtApprovedDate, 
				intStatus)
				VALUES
				('$supplierId', 
				'-2', 
				'$userId', 
				now(), 
				'0');";
		$result = $db->RunQuery2($sql);
		if(!$result)
			$savedStatus 	= false;
		
		if($savedStatus){
			$response['msg']	= "Cancelled successfully.";	
			$response['type']	= 'pass';
			$db->commit();
		}
		else{
			$response['msg']	= "Cancellation failed.";	
			$response['type']	= 'fail';
			$db->rollback();
		}
	}
	else if($requestType=='reject')
	{	
		$supplierId		= $_REQUEST["supplierId"];
		$savedStatus 	= true;

 		$header_arr			= headerArray($supplierId);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_reject($header_arr['status'],$header_arr['levels'],$session_userId,$progrmCode,'RunQuery');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
			echo json_encode($response);
			return;
		}
	
		$db->begin();
		
		$sql 	= "UPDATE mst_supplier SET intStatus = 0 WHERE intId = '$supplierId' ";
		$result = $db->RunQuery2($sql);
		if(!$result){
			$savedStatus 	= false;
			$sqlE			= $sql;
		}
	
	 	$sql = "INSERT INTO mst_supplier_approvedby 
				(intSupplierId, 
				intApproveLevelNo, 
				intApproveUser, 
				dtApprovedDate, 
				intStatus)
				VALUES
				('$supplierId', 
				'0', 
				'$userId', 
				now(), 
				'0');";
		$result = $db->RunQuery2($sql);
		if(!$result){
			$savedStatus 	= false;
			$sqlE			= $sql;
		}
		
		if($savedStatus){
			$response['msg']	= "Rejected successfully.";	
			$response['type']	= 'pass';
			$db->commit();
		}
		else{
			$response['msg']	= "Rejection failed.";	
			$response['type']	= 'fail';
			$response['sql']	= $sqlE;
			$db->rollback();
		}
	}
	
	/////////// supplier delete part /////////////////////
	else if($requestType=='delete')
	{	
 		$header_arr			= headerArray($id);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_delete($header_arr['status'],$header_arr['levels'],$session_userId,$progrmCode,'RunQuery');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
			echo json_encode($response);
			return;
		}
		
		$sql = "DELETE FROM `mst_supplier` WHERE (`intId`='$id')  ";
		$result = $db->RunQuery($sql);
		if(($result)){
			
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
	

	
function GetMaxApproveLevel($supplierId)
{
	global $db;	
	$appLevel	= 0;
	
	$sql 	= "SELECT COALESCE(MAX(intApproveLevelNo),0) AS  MAX_NO
				FROM mst_supplier_approvedby 
				WHERE intSupplierId = $supplierId AND intStatus = 0";	
	$result = $db->RunQuery2($sql);
	$row 	= mysqli_fetch_array($result);
	return $row['MAX_NO']+1;
}
function headerArray($id)
{
	global $db;
	$status	= 0;
	$sql 	= "SELECT COALESCE(intStatus,0)  AS status, intApproveLevel as levels FROM mst_supplier WHERE intId = $id";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row;
}
function GetApproveLevel1($proCode)
{
	global $db;
	$approvalLevel	= 0;
	$sql 	= "SELECT intApprovalLevel FROM sys_approvelevels WHERE strCode = '$proCode'";
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{	
		$approvalLevel	=  $row['intApprovalLevel'];
	}
	return $approvalLevel;
}

function UpdateApprovebyStatus($supplier_id)
{
	global $db;
	$max_status	= GetMaxStatus($supplier_id);
	$sql 		= "UPDATE mst_supplier_approvedby 
					SET
						intStatus = $max_status	
					WHERE
						intSupplierId = '$supplier_id' AND intStatus = '0' ;";
	$result 	= $db->RunQuery($sql);
	
}

function GetMaxStatus($supplier_id)
{
	global $db;
	
	$sql 	= "SELECT MAX(intStatus) MAX_STATUS FROM mst_supplier_approvedby WHERE intSupplierId = '$supplier_id'";
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{	
		$max_status	=  $row['MAX_STATUS']+1;
	}
	return $max_status;
}

function getMaxCode($subType)
{
	global $db;
	
	//$sql 	= "SELECT LPAD((MAX(IFNULL(CHART_OF_ACCOUNT_CODE,0))+1),3,0) MAX_CODE FROM finance_mst_chartofaccount WHERE SUB_TYPE_ID = '$subType'";
	$sql 	= "SELECT LPAD(IFNULL((MAX(CAST(CHART_OF_ACCOUNT_CODE AS UNSIGNED))),0)+1,3,0) AS MAX_CODE FROM finance_mst_chartofaccount WHERE SUB_TYPE_ID = '$subType'";
	$result = $db->RunQuery2($sql);
	while($row = mysqli_fetch_array($result))
	{	
		$max_status	=  $row['MAX_CODE']+1;
	}
	return $max_status;
}

function CheckIsAvailable($supplierId)
{
	global $db;
	$sql = "select count(*) AS COUNT FROM finance_mst_chartofaccount where CATEGORY_ID = '$supplierId' AND CATEGORY_TYPE = 'S'";	
	$result = $db->RunQuery2($sql);
	$row	= mysqli_fetch_array($result);
	if($row["COUNT"]>0)
		return true;
	else
		return false;
}

function sendDetailsToFinanceModule($supplierId){
    $configs = include('../../../../../config/zillionConfig.php');
    $url = $configs['URL'].'IntVendor';
    $supplier_details = getSupplierDetails($supplierId);
    $sup_no = $supplier_details['SUPPLIER_CODE'];
    $data_json = createJsonObject($supplier_details);
    $auth_header = "Authorization: ".$configs["TYPE"].' '.base64_encode($configs['AUTH_USERNAME'].':'.$configs['AUTH_PASSWORD']);
    $header_arr = array("Content-Type: application/json", $auth_header,"If-Match: *");
    if(!hasAlreadySent($url,$sup_no,$header_arr)) {
        $header_arr = array("Content-Type: application/json", $auth_header);
        $responseArray = callAPI($url, 'POST', $header_arr, $data_json);
        $updating = 0;
    }
    else{
        $updateUrl = $url."(No='$sup_no')";
        $responseArray = callAPI($updateUrl,"PUT",$header_arr,$data_json);
        $updating = 1;
    }
    $successStatus = 0;
    $response = $responseArray['response'];
    $httpcode = $responseArray['code'];
    if($httpcode == '201' || $httpcode == '204'){
        $successStatus = 1; //delivered
    }

    insertToTransactionTable($supplier_details,$supplierId,$successStatus,$updating);

}

function getSupplierDetails($supplierId){
    global $db;
    $sql = "SELECT 
			SU.strCode								AS SUPPLIER_CODE,
			SU.strName								AS SUPPLIER_NAME,
			SU.strAddress                           AS ADDRESS,
			SU_TYPE.strName							AS TYPE,
			SU.strCity                              AS CITY,
			CURR.intId								AS CURRENCY_ID,
			CURR.strCode							AS CURRENCY_CODE,
			IFNULL(
			SU.strPhoneNo,SU.strMobileNo
			) AS PHONE_NO,
			SU.strEmail								   AS EMAIL,
			SU.strVatNo                               AS VAT_NO,
			SU.strSVatNo                               AS SVAT_NO			                  
            FROM
                mst_supplier SU
            INNER JOIN mst_financecurrency CURR	ON CURR.intId = SU.intCurrencyId
            LEFT JOIN mst_typeofsupplier SU_TYPE ON SU_TYPE.intId = SU.intTypeId
            WHERE
                SU.intId = '$supplierId' AND SU.intStatus = '1'";
    $result = $db->RunQuery2($sql);
    return mysqli_fetch_array($result);

}

function createJsonObject($row){
    $vatCode = (is_numeric($row['SVAT_NO']) && $row['SVAT_NO'] != '0')?"SVAT":"VAT";
    $vat_no = $row['VAT_NO'];
    $svat_no = (is_numeric($row['SVAT_NO']))?$row['SVAT_NO']:'';
    $currency = ($row['CURRENCY_CODE'] == 'EURO')?"Eur":($row['CURRENCY_CODE'] == 'LKR'?"":$row['CURRENCY_CODE']);
    $address = preg_replace("/[\n\r]/","",$row['ADDRESS']);
    $splitAddress = explode(',',$address);
    $address1 = "";
    $address2 = "";
    for($i=0;$i<count($splitAddress);$i++){
        if($i<=count($splitAddress)/2) {
            $address1 = $address1 . ',' . trim($splitAddress[$i]);
        }
        else{
            $address2 = $address2.','.trim($splitAddress[$i]);
        }
    }
    $address1 = ltrim($address1, ',');
    $address2 = ltrim($address2, ',');
    if($row['TYPE'] == 'IMPORT' )
			$type = 'FOREIGN';
	else if($row['TYPE'] == 'INTER COMPANY' || $row['TYPE'] == 'LOCAL' || $row['TYPE'] == 'ON LOAN')
		$type = 'SUPPLY';
	else $type = $row['TYPE'];
    $data = array('No' => $row['SUPPLIER_CODE'],
        'Name' => $row['SUPPLIER_NAME'],
        'Address' => $address1,
        'Address_2'=>$address2,
        'City'=> $row['CITY'],
        'E_Mail'=>$row['EMAIL'],
        'Phone_No'=>$row['PHONE_NO'],
        'VAT_Bus_Posting_Group'=>$vatCode,
        'VAT_Registration_No'=>$vat_no,
        'SVAT_No'=>$svat_no,
        'Currency_Code'=>$currency,
        'Vendor_Posting_Group'=>$type
    );
    return json_encode($data);
}

function insertToTransactionTable($row, $supplierId, $deliveryStatus,$updating)
{
	global $db;
    $vatCode = (is_numeric($row['SVAT_NO']) && $row['SVAT_NO'] != '0')?"SVAT":"VAT";
    $vat_no = $row['VAT_NO'];
    $svat_no = (is_numeric($row['SVAT_NO']))?$row['SVAT_NO']:'';
    // empty for LKR
    $currency = ($row['CURRENCY_CODE'] == 'EURO')?"Eur":($row['CURRENCY_CODE'] == 'LKR'?"":$row['CURRENCY_CODE']);
    $address = preg_replace("/[\n\r]/", "", $row['ADDRESS']);
    if($row['TYPE'] == 'IMPORT' )
        $type = 'FOREIGN';
    else if($row['TYPE'] == 'INTER COMPANY' || $row['TYPE'] == 'LOCAL' || $row['TYPE'] == 'ON LOAN')
        $type = 'SUPPLY';
    else $type = $row['TYPE'];
    $sql = "INSERT INTO `trn_financemodule_supplier` (
                            `supplierId`,
                            `strCode`,
                            `strName`,
                            `strAddress`,
                            `strCity`,
                            `strEmail`,
                            `strPhoneNo`,
                            `vatBustGroup`,
                            `strVatNo`,
                            `strSVatNo`,
                            `currencyCode`,
                            `vendorPostingGroup`,
                            `deliveryDate`,
                            `deliveryStatus`,
                            `isUpdating`
                        )
                        VALUES
                            ('$supplierId','".$row['SUPPLIER_CODE']."','".$row['SUPPLIER_NAME']."','$address','".$row['CITY']."','".$row['EMAIL']."','".$row['PHONE_NO']."','$vatCode','$vat_no','$svat_no', '$currency','$type', NOW(),'$deliveryStatus','$updating')";


    $result = $db->RunQuery2($sql);

}

function hasAlreadySent($url,$supplierCode,$headerArray){
    $getUrl = $url."(No='$supplierCode')";
    $responseArray = callAPI($getUrl,'GET',$headerArray,null);
    $xml = simplexml_load_string($responseArray['response']);
    return ($xml->count())>0;
}

function callAPI($url,$method,$headerArray,$data){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);
    if($method == 'POST'){
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    else if($method == 'PUT'){
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $responseArray = array();
    $response = curl_exec($ch);
    $responseArray['response'] = $response;
    $responseArray['code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    return $responseArray;
}

function isFinalApprovalRaised($supplierId){
    global $db;
    $sql 		= "SELECT intStatus FROM mst_supplier WHERE intId = $supplierId AND intStatus = '1'";
    $result 	= $db->RunQuery2($sql);
    return (mysqli_num_rows($result)>0);
}



?>
 

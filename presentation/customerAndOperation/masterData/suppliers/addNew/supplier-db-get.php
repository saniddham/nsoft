<?php
//ini_set('display_errors',1);
	session_start();
	$backwardseperator = "../../../../../";
	include_once "{$backwardseperator}dataAccess/Connector.php";
 	include_once  ("../../../../../class/cls_commonErrorHandeling_get.php");
 	
 	$obj_commonErr	= new cls_commonErrorHandeling_get($db);
	
	$progrmCode		='P0078';
	$mainPath 		= $_SESSION['mainPath'];
	$userId 		= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
 
	/////////// supplier load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql	= "SELECT
					intId,
					strName
					FROM mst_supplier
					order by strName
					";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	
	if($requestType=='loadNewCode')
	{
	$sql = "SELECT
			mst_supplier.intId,
			mst_supplier.strCode
			FROM mst_supplier
			ORDER BY
			mst_supplier.intId DESC 
			limit 1
		";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$code=$row['strCode'];
		
		$number= preg_match("[^0-9]", "",$code);
		$stringLength= strlen($code)- strlen($number);
		$string=substr($code,0,$stringLength);
		
		$numberLength= strlen($number);
		if($numberLength==0){
			$numberLength=1;
		}
		
		$newNumber= str_pad($row['intId']+1, $numberLength, "0", STR_PAD_LEFT); 
		$newCode=$string.$newNumber;
		echo  $newCode;
	}
	
	else if($requestType=='loadLocations')
	{
		$company  = $_REQUEST['company'];
		$sql = "SELECT
				mst_locations.strName,
				mst_locations.intId,
				mst_locations.strCode
				FROM mst_locations
				WHERE
				mst_locations.intStatus =  '1' AND
				mst_locations.intCompanyId =  '$company'
				ORDER BY
				strCode, strName ASC 
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadMainType')
	{
		$financeType = $_REQUEST['financeType'];
		
		$response['mainTypeCombo']	= getMainTypeCombo($financeType);
		echo json_encode($response);
	}
	else if($requestType=='loadSubType')
	{
		$mainType = $_REQUEST['mainType'];
		
		$response['subTypeCombo']	= getSubTypeCombo($mainType);
		echo json_encode($response);
	}
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		
		//------------------------------
		$sql = "SELECT intLocationId
				FROM mst_supplier_locations
				WHERE
					intSupplierId =  '$id'";
		$result = $db->RunQuery($sql);
	
		$arrlocat;
		while($row=mysqli_fetch_array($result))
		{
			$val['locateId'] 	= $row['intLocationId'];
			$arrlocat[] = $val;
		}
		
		$response['locatVal'] = $arrlocat;
		//-------------------------------
		
		$sql = "SELECT
					strCode,
					strName,
					intTypeId,
					strAddress,
					strContactPerson,
					strPrintOnCheque,
					strCity,
					intCountryId,
					intCurrencyId,
					strPhoneNo,
					strMobileNo,
					strFaxNo,
					strEmail,
					strWebSite,
					intShipmentId,
					strIcCode,
					strVatNo,
					strSVatNo,
					strRegistrationNo,
					strInvoiceType,
					strAccNo,
					intPaymentsTermsId,
					intPaymentsMethodsId,
					IFNULL(intCreditLimit,0) as intCreditLimit,
					intChartOfAccountId,
					strLeadTime,
					strBlocked,
					intRank,
					intInterCompany,
					intInterLocation,
					intCompanyId,
					strColorCode,
					mst_supplier.intStatus,
					mst_supplier.intApproveLevel,
					mst_supplier.SUB_TYPE,
					CHART_OF_ACCOUNT,
					finance_mst_account_sub_type.MAIN_TYPE_ID ,
					finance_mst_account_main_type.FINANCE_TYPE_ID  
				FROM mst_supplier 
				LEFT JOIN finance_mst_account_sub_type ON mst_supplier.SUB_TYPE=finance_mst_account_sub_type.SUB_TYPE_ID
				LEFT JOIN finance_mst_account_main_type ON finance_mst_account_sub_type.MAIN_TYPE_ID=finance_mst_account_main_type.MAIN_TYPE_ID 
				WHERE
					intId =  '$id'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['code'] 			= $row['strCode'];
			$response['name'] 			= $row['strName'];
			$response['type'] 			= $row['intTypeId'];
			$response['address'] 		= $row['strAddress'];
			$response['contact'] 		= $row['strContactPerson'];
			$response['chequename'] 	= $row['strPrintOnCheque'];
			$response['city'] 			= $row['strCity'];
			$response['country'] 		= $row['intCountryId'];
			$response['currency'] 		= $row['intCurrencyId'];
			$response['phone'] 			= $row['strPhoneNo'];
			$response['mobile'] 		= $row['strMobileNo'];
			$response['fax'] 			= $row['strFaxNo'];
			$response['email'] 			= $row['strEmail'];
			$response['web'] 			= $row['strWebSite'];
			$response['shipment'] 		= $row['intShipmentId'];
			$response['partnercode'] 	= $row['strIcCode'];
			$response['vatNo'] 			= $row['strVatNo'];
			$response['sVatNo'] 		= $row['strSVatNo'];
			$response['regNo'] 			= $row['strRegistrationNo'];
			$response['invoType'] 		= $row['strInvoiceType'];
			$response['accNo'] 			= $row['strAccNo'];
			$response['payterms'] 		= $row['intPaymentsTermsId'];
			$response['paymethods'] 	= $row['intPaymentsMethodsId'];
			$response['creditlimit'] 	= $row['intCreditLimit'];
			$response['chartofaccount'] = $row['intChartOfAccountId'];
			$response['leadtime'] 		= $row['strLeadTime'];
			$response['blocked'] 		= $row['strBlocked'];
			$response['rank'] 			= $row['intRank'];
			$response['status'] 		= ($row['intStatus']?true:false);
			$response['subType'] 		= $row['SUB_TYPE'];
			$response['chartOfAccName'] = $row['CHART_OF_ACCOUNT'];
			$response['mainType'] 		= $row['MAIN_TYPE_ID'];
			$response['financeType'] 	= $row['FINANCE_TYPE_ID'];
			$response['interCompany'] 	= $row['intInterCompany'];
			$response['locationId'] 	= $row['intInterLocation'];
			$response['companyId'] 		= $row['intCompanyId'];
			$response['ColorCode'] 		= $row['strColorCode'];
			$permision_arr				= $obj_commonErr->get_permision_withApproval_save($row['intStatus'],$row['intApproveLevel'],$userId,$progrmCode,'RunQuery');
 			$permision_save				= $permision_arr['permision'];	
			$response['permision_save']	= $permision_save;
 			$permision_arr				= $obj_commonErr->get_permision_withApproval_confirm($row['intStatus'],$row['intApproveLevel'],$userId,$progrmCode,'RunQuery');
 			$permision_approve			= $permision_arr['permision'];	
			$response['permision_approve']	= $permision_approve;
		}
		//print_r($response);
		echo json_encode($response);
	}

//--------------------------
function getMainTypeCombo($financeType)
{
	global $db;
	
	$sql = "SELECT MAIN_TYPE_ID,MAIN_TYPE_NAME
			FROM finance_mst_account_main_type
			WHERE FINANCE_TYPE_ID='$financeType' AND
			STATUS='1'
			ORDER BY MAIN_TYPE_NAME";
		//	die($sql);
	$result = $db->RunQuery($sql);
	$html 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$html.="<option value=\"".$row['MAIN_TYPE_ID']."\">".$row['MAIN_TYPE_NAME']."</option>";
	}
	return $html;
}
function getSubTypeCombo($mainType)
{
	global $db;
	
	$sql = "SELECT SUB_TYPE_ID,SUB_TYPE_NAME
			FROM finance_mst_account_sub_type
			WHERE MAIN_TYPE_ID='$mainType' AND
			STATUS='1'
			ORDER BY SUB_TYPE_NAME ";
	$result = $db->RunQuery($sql);
	$html 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$html.="<option value=\"".$row['SUB_TYPE_ID']."\">".$row['SUB_TYPE_NAME']."</option>";
	}
	return $html;
}

	
?>
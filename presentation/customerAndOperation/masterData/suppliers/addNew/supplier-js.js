var basePath = 'presentation/customerAndOperation/masterData/suppliers/addNew/';
var arrLocate = [];
var reportId = 890;			

var invoType = "";
function functionList()
{
    if(supId!='')
    {
        $('#frmSupplier #cboSearch').val(supId);
       loadSearchDetails();
    }
}
$(document).ready(function() {
	functionList();
	$('#frmSupplier #cboFinanceType').die('change').live('change',function(){LoadMainType('frmSupplier');});
	$('#frmSupplier #cboMainType').die('change').live('change',function(){LoadSubType('frmSupplier');});
	$('#frmSupplier #cboSubType').die('change').live('change',function(){loadChartOfAccDefault('frmSupplier');});
	$('#frmSupplier #txtName').die('keyup').live('keyup',function(){loadChartOfAccDefault('frmSupplier');});
	
	$('#frmRptSupplier #cboFinanceType').die('change').live('change',function(){LoadMainType('frmRptSupplier');});
	$('#frmRptSupplier #cboMainType').die('change').live('change',function(){LoadSubType('frmRptSupplier');});
 	
	$('#frmSupplier #butConfirm').die('click').live('click',ViewConfirmRpt);
	$('#frmSupplier #butReport').die('click').live('click',ViewRpt);

	$('#frmRptSupplier #butRptConfirm').die('click').live('click',ConfirmRpt);
	$('#frmRptSupplier #butRptReject').die('click').live('click',RejectRpt);
	$('#frmRptSupplier #butRptCancel').die('click').live('click',CancelRpt);
	$('#frmRptSupplier #butRptRevise').die('click').live('click',ReviseRpt);
	
  		$("#frmSupplier").validationEngine();
		$('#frmSupplier #txtCode').focus();
  //permision for add 
 /* if(intAddx)
  {
 	$('#frmSupplier #butNew').show();
	$('#frmSupplier #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmSupplier #butSave').show();
	$('#frmSupplier #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmSupplier #butDelete').show();
	$('#frmSupplier #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmSupplier #cboSearch').removeAttr('disabled');
  }*/
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

	$('#frmSupplier #txtCreditLimit').die('keyup').live('keyup',function(e){
			if($(this).val()=="" || isNaN($(this).val())){
				$(this).val(0);
				$(this).select();
			}
 	});
	
	//-------------Hemanthi-----26/06/2012------------
  $('#frmSupplier #chkInterComp').die('click').live('click',function(){
		if(document.getElementById('chkInterComp').checked==true){ 
		$('#frmSupplier #cboInterCompany').removeAttr('disabled');
		}
		else{
		$('#frmSupplier #cboInterCompany').attr('disabled', true);
		$('#frmSupplier #cboInterLocation').attr('disabled', true);
		$('#frmSupplier #cboInterCompany').val('');
		$('#frmSupplier #cboInterLocation').val('');
		}
  });
	//-------------Hemanthi-----26/06/2012------------
  $('#frmSupplier #cboInterCompany').die('change').live('change',function(){
	    var company= $('#frmSupplier #cboInterCompany').val();
		var url 	= basePath+"supplier-db-get.php?requestType=loadLocations"+"&company="+company;
		var httpobj = $.ajax({url:url,async:false})
		$('#frmSupplier #cboInterLocation').html(httpobj.responseText);
		$('#frmSupplier #cboInterLocation').removeAttr('disabled');
  });
	//-----------------------------------------------
  ///save button click event
  $('#frmSupplier #butSave').die('click').live('click',function(){
	
		if(document.getElementById('chkInterComp').checked==true){ 
			if($('#frmSupplier #cboInterCompany').val()==''){
			alert("Please select Company");
			return false;
			}
			else if($('#frmSupplier #cboInterLocation').val()==''){
			alert("Please select Location");
			return false;
			}
		}
		
	var locationValue="[ ";
		$('.locations:checked').each(function(){
			locationValue += '{ "locationId":"'+$(this).val()+'"},';
		});
		locationValue = locationValue.substr(0,locationValue.length-1);
		locationValue += " ]";
		
	var requestType = '';
	if ($('#frmSupplier').validationEngine('validate'))   
    { 
		showWaiting();
	
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmSupplier #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = basePath+"supplier-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",
			type:'POST',  
		data:$("#frmSupplier").serialize()+'&requestType='+requestType+'&location='+locationValue/*+'&invoType='+invoType*/,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmSupplier #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmSupplier').get(0).reset();
						loadCombo_frmSupplier();
						loadNewCode();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmSupplier #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
		hideWaiting();
	}
						$('#frmSupplier #cboInterCompany').attr('disabled',true);
   });
   
   /////////////////////////////////////////////////////
   //// load supplier details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmSupplier #cboSearch').die('click').live('click',function(){
	   $('#frmSupplier').validationEngine('hide');
   });
   
    $('#frmSupplier #cboSearch').die('change').live('change',loadSearchDetails);
		
	//////////// end of load details /////////////////
	

	
	$('#frmSupplier #butNew').die('click').live('click',function(){
		$('#frmSupplier').get(0).reset();
		loadCombo_frmSupplier();
		loadNewCode();
		$('#frmSupplier #txtCode').focus();
		$('#frmSupplier #butSave').show();
		/*if(intEditx)
		{
			$('#frmSupplier #butSave').show();
			$('#frmSupplier #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
		}
		
		//permision for delete
		if(intDeletex)
		{
			$('#frmSupplier #butDelete').show();
			$('#frmSupplier #cboSearch').removeAttr('disabled');
		}
		
		//permision for view
		if(intViewx)
		{
			$('#frmSupplier #cboSearch').removeAttr('disabled');
		}*/
		
	});
    $('#frmSupplier #butDelete').die('click').live('click',function(){
		if($('#frmSupplier #cboSearch').val()=='')
		{
			$('#frmSupplier #butDelete').validationEngine('showPrompt', 'Please select Supplier.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmSupplier #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basePath+"supplier-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'POST',
											data:'requestType=delete&cboSearch='+$('#frmSupplier #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmSupplier #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmSupplier').get(0).reset();
													loadCombo_frmSupplier();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
		$('.invTpe').die('click').live('click',function(){
		invoType = $(this).val();	
	});
});


function loadCombo_frmSupplier()
{
	var url 	= basePath+"supplier-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmSupplier #cboSearch').html(httpobj.responseText);
}

function loadNewCode()
{ 
	var url 	= basePath+"supplier-db-get.php?requestType=loadNewCode";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmSupplier #txtCode').val(httpobj.responseText);
}

function alertx()
{
	$('#frmSupplier #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmSupplier #butDelete').validationEngine('hide')	;
}

function popupLocations()
{
	popupWindow('1');	
}

function loadLocations()
{
	$("#butLocations").die('click').live('click',popupLocations);
	//$.getScript('presentation/customerAndOperation/masterData/supplierLocations/addNew/supplierLocations-js.js');
}

function closePopUp()
{
	arrLocate = [];
	
	var i = 0;

		$('.locations:checked').each(function(){
			arrLocate[i++]=$(this).val();
		});

	$("#tblLocations").load(basePath+'table-location.php',function(){
	for(var j=0;j<=arrLocate.length-1;j++)
	{
		$('#frmSupplier #loc'+arrLocate[j]).attr('checked',true);					
	}
	});
}

function LoadMainType(formId)
{
  	$('#'+formId+' #cboMainType').html('');
	$('#'+formId+' #cboSubType').html('');
	$('#'+formId+' #cboChartOfAccount').html('');
	
	if($('#'+formId+' #cboFinanceType').val()=='')
		return;
	var url = basePath+"supplier-db-get.php?requestType=loadMainType";
	var obj = $.ajax({
		url:url,
		dataType: "json",  
		type:'POST',
		data:"&financeType="+$('#'+formId+' #cboFinanceType').val(),
		async:false,
		success:function(json){
			
			$('#'+formId+' #cboMainType').html(json.mainTypeCombo);			
		},
		error:function(xhr,status){
				
		}		
	});
}
function LoadSubType(formId)
{
	$('#'+formId+' #cboSubType').html('');
	$('#'+formId+' #cboChartOfAccount').html('');
	
	if($('#'+formId+' #cboMainType').val()=='')
		return;
		
	var url = basePath+"supplier-db-get.php?requestType=loadSubType";
	var obj = $.ajax({
		url:url,
		dataType: "json",
		type:'POST',  
		data:"&mainType="+$('#'+formId+' #cboMainType').val(),
		async:false,
		success:function(json){
			
			$('#'+formId+' #cboSubType').html(json.subTypeCombo);			
		},
		error:function(xhr,status){
				
		}		
	});
}
function loadChartOfAccDefault(formId)
{
 	$('#'+formId+' #cboChartOfAccount').html('');
	
	if(($('#'+formId+' #cboSubType').val()=='') || ($('#'+formId+' #cboSubType').val()==null)){
 		return false;
	}
	else if($('#'+formId+' #txtChartOfAccountDB').val() == ''){	
		$('#'+formId+' #txtChartOfAccount').val($('#'+formId+' #txtName').val());
	}
}

function ViewConfirmRpt()
{
	if($('#frmSupplier #cboSearch').val()=="")
	{
		alert("No details available to confirm.");
		return;
	}
 	var url  = "?q="+reportId+"&supplierId="+$('#frmSupplier #cboSearch').val()+"&mode=Confirm";
 	window.open(url,'rptSupplier.php');
}

function ViewRpt()
{
	if($('#frmSupplier #cboSearch').val()=="")
	{
		alert("No details available to view report.");
		return;
	}
 	var url  = "?q="+reportId+"&supplierId="+$('#frmSupplier #cboSearch').val()+"&mode=";
	window.open(url,'rptSupplier.php');
}

 function ConfirmRpt(){
	 
	var chartOfAccName	=$('#frmRptSupplier #txtChartOfAccount').val();
	var subType			=$('#frmRptSupplier #cboSubType').val();

	var val = $.prompt('Are you sure you want to approve this Supplier ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"supplier-db-set.php"+window.location.search+'&requestType=approve&subType='+subType+'&chartOfAccName='+URLEncode(chartOfAccName);
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptSupplier #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptSupplier #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
					   hideWaiting();
				}});
  }
  
  
  function RejectRpt(){
	  
	var val = $.prompt('Are you sure you want to reject this Supplier ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
				showWaiting();
				var url = basePath+"supplier-db-set.php"+window.location.search+'&requestType=reject';
				var obj = $.ajax({
					url:url,
					type:'post',
					dataType: "json",  
					data:'',
					async:false,
					
					success:function(json){
							$('#frmRptSupplier #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							if(json.type=='pass')
							{
								var t=setTimeout("alertx()",1000);
								window.location.href = window.location.href;
								window.opener.location.reload();//reload listing page
								return;
							}
						},
					error:function(xhr,status){
							
							$('#frmRptSupplier #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",3000);
						}		
					});
				////////////
					}
					hideWaiting();
			}});
	  
  }

 
function CancelRpt(){

	var val = $.prompt('Are you sure you want to cancel this Supplier ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
				if(v)
				{
					showWaiting();
					var url = basePath+"supplier-db-set.php"+window.location.search+'&requestType=cancel';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptSupplier #butRptCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptSupplier #butRptCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
						hideWaiting();
 				}});
	
}

function ReviseRpt(){

	var val = $.prompt('Are you sure you want to revise this Supplier ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
				if(v)
				{
					showWaiting();
					var url = basePath+"supplier-db-set.php"+window.location.search+'&requestType=revise';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptSupplier #butRptRevise').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptSupplier #butRptRevise').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
						hideWaiting();
 				}});
	
}

function loadSearchDetails(){
	var supID	= $('#frmSupplier #cboSearch').val();
	//alert(supID);
		
	$('#frmSupplier').validationEngine('hide');
		var url = basePath+"supplier-db-get.php";
		if($('#frmSupplier #cboSearch').val()=='')
		{
			$('#frmSupplier').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+supID,
			async:false,
			success:function(json){
				//json  = eval('('+json+')');
					$('#frmSupplier #txtCode').val(json.code);
					$('#frmSupplier #txtName').val(json.name);
					$('#frmSupplier #cboType').val(json.type);
					$('#frmSupplier #txtAddress').val(json.address);
					$('#frmSupplier #txtContact').val(json.contact);
					$('#frmSupplier #txtPrintCheque').val(json.chequename);
					$('#frmSupplier #txtCity').val(json.city);
					$('#frmSupplier #cboCountry').val(json.country);
					$('#frmSupplier #cboCurrency').val(json.currency);
					$('#frmSupplier #txtPhone').val(json.phone);
					$('#frmSupplier #txtMobile').val(json.mobile);
					$('#frmSupplier #txtFax').val(json.fax);
					$('#frmSupplier #txtEMail').val(json.email);
					$('#frmSupplier #txtWeb').val(json.web);
					$('#frmSupplier #cboShipmentMethod').val(json.shipment);
					$('#frmSupplier #txtIcCode').val(json.partnercode);
					$('#frmSupplier #txtVatNo').val(json.vatNo);
					$('#frmSupplier #txtSvatNo').val(json.sVatNo);
					$('#frmSupplier #txtRegNo').val(json.regNo);
					$('#frmSupplier #cboInvoiceType').val(json.invoType);
 					invoType 			= json.invoType;
						
					var subType 		= json.subType;
					var mainType		= json.mainType;
					var financeType		= json.financeType
					var chartOfAccName	= json.chartOfAccName;
					$('#frmSupplier #cboFinanceType').val(financeType);
					LoadMainType('frmSupplier');
					$('#frmSupplier #cboMainType').val(mainType);
					LoadSubType('frmSupplier');	
					$('#frmSupplier #cboSubType').val(subType);
					$('#frmSupplier #txtChartOfAccount').val(chartOfAccName);
					$('#frmSupplier #txtChartOfAccountDB').val(chartOfAccName);

					$('#frmSupplier #txtAccNo').val(json.accNo);
					$('#frmSupplier #cboPaymentsTerms').val(json.payterms);
					$('#frmSupplier #cboPaymentsMethods').val(json.paymethods);
					$('#frmSupplier #txtCreditLimit').val(json.creditlimit);
					$('#frmSupplier #cboChartOfAcc').val(json.chartofaccount);
					$('#frmSupplier #txtLeadTime').val(json.leadtime);
					$('#frmSupplier #cboBlocked').val(json.blocked);
					$('#frmSupplier #txtRank').val(json.rank);
					$('#frmSupplier #cboColorCode').val(json.ColorCode);
 
					if(json.interCompany==1){
						$('#frmSupplier #chkInterComp').attr('checked',true);
						$('#frmSupplier #cboInterCompany').val(json.companyId);
						$('#frmSupplier #cboInterCompany').removeAttr('disabled');
						$('#frmSupplier #cboInterLocation').val(json.locationId);
						$('#frmSupplier #cboInterLocation').removeAttr('disabled');
					}
					else
					{
						$('#frmSupplier #chkInterComp').removeAttr('checked');
						$('#frmSupplier #cboInterCompany').val('');
						$('#frmSupplier #cboInterCompany').attr('disabled',true);
						$('#frmSupplier #cboInterLocation').val('');
						$('#frmSupplier #cboInterLocation').attr('disabled',true);
					}
						
					var savePermision = json.permision_save;
					var approvePermision = json.permision_approve;
					if(savePermision==1){
						$('#frmSupplier #butSave').show();
					}
					else{
						$('#frmSupplier #butSave').hide();
					}
					if(approvePermision==1){
						$('#frmSupplier #butConfirm').show();
					}
					else{
						$('#frmSupplier #butConfirm').hide();
					}
					
 					if($('#frmSupplier #cboSearch').val()!=''){
						$('#frmSupplier #butReport').show();
					}
					else{
						$('#frmSupplier #butReport').hide();
					}

				//--------------------------------------------------
				var locId = "";
				if(json.locatVal!=null){
				$('.locations:checkbox').removeAttr('checked');	
				for(var j=0;j<=json.locatVal.length-1;j++)
				{
					locId = json.locatVal[j].locateId;
					$('#frmSupplier #loc'+locId).attr('checked',true);					
				}}
				else
				{
					$('.locations:checkbox').removeAttr('checked');
				}
				//--------------------------------------------------
			}
	});
}
var basepath	= 'presentation/customerAndOperation/masterData/groundColors/addNew/';
			
$(document).ready(function() {
	$("#frmColors").validationEngine();
	$('#frmColors #txtCode').focus();

  ///save button click event
  $('#frmColors #butSave').click(function(){
	//$('#frmColors').submit();
	var requestType = '';
	if ($('#frmColors').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmColors #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = basepath+"colors-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			data:$("#frmColors").serialize()+'&requestType='+requestType,
			type:'post',
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmColors #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmColors').get(0).reset();
						loadCombo_frmColors();
						loadNewCode();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmColors #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load location details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmColors #cboSearch').click(function(){
	   $('#frmColors').validationEngine('hide');
   });
    $('#frmColors #cboSearch').change(function(){
		$('#frmColors').validationEngine('hide');
		var url = basepath+"colors-db-get.php";
		if($('#frmColors #cboSearch').val()=='')
		{
			$('#frmColors').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'post',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmColors #txtCode').val(json.code);
					$('#frmColors #txtName').val(json.name);
					$('#frmColors #txtRemark').val(json.remark);
					$('#frmColors #chkActive').attr('checked',json.status);
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmColors #butNew').click(function(){
		$('#frmColors').get(0).reset();
		loadNewCode();
		loadCombo_frmColors();
		$('#frmColors #txtCode').focus();
	});
    $('#frmColors #butDelete').click(function(){
		if($('#frmColors #cboSearch').val()=='')
		{
			$('#frmColors #butDelete').validationEngine('showPrompt', 'Please select Wash Standerd.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmColors #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basepath+"colors-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'post',
											data:'requestType=delete&cboSearch='+$('#frmColors #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmColors #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmColors').get(0).reset();
													loadCombo_frmColors();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});


function loadCombo_frmColors()
{
	var url 	= basepath+"colors-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmColors #cboSearch').html(httpobj.responseText);
}

function loadNewCode()
{ 
	var url 	= basepath+"colors-db-get.php?requestType=loadNewCode";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmColors #txtCode').val(httpobj.responseText);
}

function alertx()
{
	$('#frmColors #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmColors #butDelete').validationEngine('hide')	;
}

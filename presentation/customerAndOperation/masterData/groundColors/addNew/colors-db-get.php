<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	
	/////////// location load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
					intId,
					strName
				FROM mst_colors_ground
				order by strName
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	
	if($requestType=='loadNewCode')
	{
		$sql = "SELECT
				mst_colors_ground.intId,
				mst_colors_ground.strCode
				FROM mst_colors_ground
				ORDER BY
				mst_colors_ground.intId DESC 
				limit 1
			";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$code=$row['strCode'];
		
		$number= preg_match("[^0-9]", "",$code);
		$stringLength= strlen($code)- strlen($number);
		$string=substr($code,0,$stringLength);
		
		$numberLength= strlen($number);
		if($numberLength==0){
			$numberLength=1;
		}
		
		$newNumber= str_pad($row['intId']+1, $numberLength, "0", STR_PAD_LEFT); 
		$newCode=$string.$newNumber;
		echo  $newCode;
	}
	
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		$sql = "SELECT
					strCode,
					strName,
					strRemark,
					intStatus
				FROM mst_colors_ground
				WHERE
					intId =  '$id'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['code'] 	= $row['strCode'];
			$response['name'] 	= $row['strName'];
			$response['remark'] = $row['strRemark'];
			$response['status'] = ($row['intStatus']?true:false);
		}
		echo json_encode($response);
	}
	
	
?>
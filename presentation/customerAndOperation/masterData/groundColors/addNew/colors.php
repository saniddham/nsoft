<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$newCode=getMaxCode();
?>
<title>Ground Colors</title>
<form id="frmColors" name="frmColors" method="post" autocomplete="off"  >
<div align="center">
		<div class="trans_layoutD">
		  <div class="trans_text">Ground Colors</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="585" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
              <tr>
                <td width="46" class="normalfnt">&nbsp;</td>
                <td width="158" class="normalfnt">Colors</td>
                <td colspan="2">
                  <select name="cboSearch" class="txtbox" id="cboSearch"  style="width:250px;" <?php echo ($form_permision['edit']?'':'disabled="disabled"')?>>
                  <option value=""></option>
                 <?php  $sql = "SELECT
									intId,
									strName
								FROM mst_colors_ground
								order by strName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?> </select>				</td>                
                </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td>&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Color Code&nbsp;<span class="compulsoryRed">*</span></td>
                <td width="142"><input  name="txtCode" type="text" class="validate[required,maxSize[10]]" id="txtCode" style="width:140px"  tabindex="1" value="<?php echo $newCode ; ?>"/></td>
                <td width="209" class="normalfnt">&nbsp;</td>
                </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Color Name&nbsp;<span class="compulsoryRed">*</span></td>
                <td colspan="2"><input name="txtName" type="text" class="validate[required,maxSize[50]]" id="txtName" style="width:250px" tabindex="2" maxlength="50" /></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Remarks</td>
                <td colspan="2">
                  <textarea name="txtRemark" style="width:250px" class="validate[maxSize[250]]"  rows="2" id="txtRemark"  tabindex="3"></textarea>				</td>
              </tr>
              <tr>
                <td rowspan="2" class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Active</td>
                <td><input type="checkbox" name="chkActive" id="chkActive" checked="checked" tabindex="4"/></td>
                <td class="normalfnt">&nbsp;</td>
                </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td></td>
                </tr>
              </table></td>
            </tr>
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><img border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img <?php echo $form_permision['add']?'':'style="display:none"'?> border="0" src="images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img <?php echo $form_permision['delete']?'':'style="display:none"'?> border="0" src="images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
</form>

<?php
function getMaxCode()
{
	global $db;
	$sql = "SELECT
			mst_colors_ground.intId,
			mst_colors_ground.strCode
			FROM mst_colors_ground
			ORDER BY
			mst_colors_ground.intId DESC 
			limit 1
		";
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	$code=$row['strCode'];
	
	$number= preg_match("[^0-9]", "",$code);
	$stringLength= strlen($code)- strlen($number);
	$string=substr($code,0,$stringLength);
	
	$numberLength= strlen($number);
	if($numberLength==0){
		$numberLength=1;
	}
	
	$newNumber= str_pad($row['intId']+1, $numberLength, "0", STR_PAD_LEFT); 
	$newCode=$string.$newNumber;

	return $newCode;
}

?>

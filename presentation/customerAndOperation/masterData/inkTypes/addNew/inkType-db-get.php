<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	
	/////////// location load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
					intId,
					strName
				FROM mst_inktypes
				order by strName
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		$sql = "SELECT
					strCode,
					strName,
					strRemark,
					INK_TYPE_CATEGORY,
					intTechniqueId,
					intType,
					intStatus
				FROM mst_inktypes
				WHERE
					intId =  '$id'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['code'] 				= $row['strCode'];
			$response['name'] 				= $row['strName'];
			$response['typeOfPrint'] 		= $row['intTechniqueId'];
			$response['inkType_grp'] 		= $row['INK_TYPE_CATEGORY'];
			$response['intType'] 			= $row['intType'];
			$response['remark'] 			= $row['strRemark'];
			$response['status'] 			= ($row['intStatus']?true:false);
		}
		echo json_encode($response);
	}
	elseif($requestType=='URLgetNextCode')
	{
		$sql = "SELECT MAX(intId) AS tId FROM mst_inktypes order by tId DESC";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['NextId'] 				= $row['tId'] + 1000;
		}
		echo json_encode($response);
	}
	
?>
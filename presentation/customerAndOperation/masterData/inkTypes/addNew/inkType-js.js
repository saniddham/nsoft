$(document).ready(function() {
	getNextCode();
	$("#frmInkType").validationEngine();
	$('#frmInkType #txtName').focus();
	$('#frmInkType #butSave').die('click').live('click',save);
	$('#frmInkType #butNew').die('click').live('click',clearForm);  
    $('#frmInkType #cboSearch').die('change').live('change',searchCombo);
 	$('#frmInkType #butDelete').die('click').live('click',deleteData);
});

function getNextCode()
{
	if($('#frmInkType #txtCode').val()!='')
		return;
		
	var url 	= "presentation/customerAndOperation/masterData/inkTypes/addNew/inkType-db-get.php";
	var data	= "requestType=URLgetNextCode";

	$.ajax({
		url:url,
		data:data,
		dataType: "json",  
		async:false,			
		success:function(json){
			$('#frmInkType #txtCode').val('TE'+json.NextId);
		}
	});	
}

function save()
{	
	if(!$('#frmInkType').validationEngine('validate'))
	{
		var t = setTimeout("alertx('#frmInkType')",2000);
    	return;
	}
	
	showWaiting();
	
	if($("#frmInkType #cboSearch").val()=='')
		requestType = 'add';
	else
		requestType = 'edit';
	
	var url = "presentation/customerAndOperation/masterData/inkTypes/addNew/inkType-db-set.php";
	var obj = $.ajax({
		url:url,
		dataType: "json",  
		type: 'post',
		data:$("#frmInkType").serialize()+'&requestType='+requestType,
		async:false,			
		success:function(json){
				$('#frmInkType #butSave').validationEngine('showPrompt', json.msg,json.type);
				if(json.type=='pass')
				{
					var t = setTimeout("alertx('#frmInkType #butSave')",2000);
					hideWaiting();
					clearForm();
					return;
				}
				hideWaiting();
				var t = setTimeout("alertx('#frmInkType #butSave')",4000);
			},
		error:function(xhr,status){
				hideWaiting();
				$('#frmInkType #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				var t=setTimeout("alertx('#frmInkType #butSave')",4000);
			}		
	});
}

function deleteData()
{
	if($('#frmInkType #cboSearch').val()=='')
	{
		$('#frmInkType #butDelete').validationEngine('showPrompt', 'Please select inkType.', 'fail');
		var t=setTimeout("alertx('#frmInkType')",2000);	
		return;
	}
	
	var val = $.prompt('Are you sure you want to delete "'+$('#frmInkType #cboSearch option:selected').text()+'" ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
				var url = "presentation/customerAndOperation/masterData/inkTypes/addNew/inkType-db-set.php";
				var httpobj = $.ajax({
					url:url,
					dataType:'json',
					type: 'post',
					data:'requestType=delete&cboSearch='+$('#frmInkType #cboSearch').val(),
					async:false,
					success:function(json){							
						$('#frmInkType #butDelete').validationEngine('showPrompt', json.msg,json.type);							
						if(json.type=='pass')
						{
							var t=setTimeout("alertx('#frmInkType #butDelete')",2000);
							clearForm();
							return;
						}	
						var t=setTimeout("alertx('#frmInkType #butDelete')",4000);
					},
					error:function(xhr,status){							
						$('#frmInkType #butDelete').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertx('#frmInkType #butDelete')",4000);
					} 
				});
			}
		}
	});		
}

function clearForm()
{	
	var t = setTimeout("alertx('#frmInkType')",2000);
	showWaiting();
	loadCombo_frmInkType();	
	$('#frmInkType #cboSearch').val('');
	$('#frmInkType #txtCode').val('');
	$('#frmInkType #txtName').val('');
	$('#frmInkType #cboTypeOfPrint').val('');
	$('#frmInkType #txtRemark').val('');
	$('#frmInkType #cboType').val('null');
	$('#frmInkType #chkActive').prop('checked',true);
	getNextCode();
	$('#frmInkType #txtName').focus();
	hideWaiting();
}

function searchCombo()
{	
	$('#frmInkType').validationEngine('hide');
	
	if($('#frmInkType #cboSearch').val()=='')
	{
		clearForm();
		return;	
	}
	
	var url = "presentation/customerAndOperation/masterData/inkTypes/addNew/inkType-db-get.php";
	
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:'requestType=loadDetails&id='+$(this).val(),
		async:false,
		success:function(json){
				$('#frmInkType #txtCode').val(json.code);
				$('#frmInkType #txtName').val(json.name);
				$('#frmInkType #cboInkTypeGroup').val(json.inkType_grp);
				$('#frmInkType #cboTypeOfPrint').val(json.typeOfPrint);
				$('#frmInkType #cboType').val(json.intType);
				$('#frmInkType #txtRemark').val(json.remark);
				$('#frmInkType #chkActive').attr('checked',json.status);
		}
	});
}

function loadCombo_frmInkType()
{
	var url 	= "presentation/customerAndOperation/masterData/inkTypes/addNew/inkType-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmInkType #cboSearch').html(httpobj.responseText);
}

function alertx(obj)
{
	$(obj).validationEngine('hide')	;
}
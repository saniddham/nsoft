<?php 
ini_set('display_errors',0);
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	include_once "{$backwardseperator}libraries/mail/mail.php";		
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];
	$typeOfPrintId	= $_REQUEST['cboTypeOfPrint'];
	$typeGroup		= $_REQUEST['cboInkTypeGroup'];
	$intType 		= $_REQUEST['cboType'];
	$code			= trim($_REQUEST['txtCode']);
	$name			= trim($_REQUEST['txtName']);
	$remark			= trim($_REQUEST['txtRemark']);
	//$intStatus		= ($_REQUEST['chkActive']?1:0);	
	$intStatus		= ($_REQUEST['chkActive']?2:0);	//there is a approve level for costing team
	/////////// location insert part /////////////////////
	if($requestType=='add')
	{
		/*$sql = "INSERT INTO `mst_inktypes` (`strCode`,`strName`,intTechniqueId,intType,INK_TYPE_CATEGORY,`strRemark`,`intCreator`,dtmCreateDate,intStatus) 
				VALUES ('$code','$name','$typeGroup','$typeOfPrintId',$intType,'$remark','$userId',now(),'$intStatus')"; */
		$sql = "INSERT INTO `mst_inktypes` (`strCode`,`strName`,intTechniqueId,intType,`strRemark`,`intCreator`,dtmCreateDate,intStatus,INK_TYPE_CATEGORY) 
				VALUES ('$code','$name','$typeOfPrintId',$intType,'$remark','$userId',now(),'$intStatus',$typeGroup)";
		$result = $db->RunQuery($sql);
	
		////////////////////////////////////
 			$ink_type_name	= $name;
			$sql = "SELECT
					  U.strEmail		AS USER_EMAIL,
					  U.strFullName		AS RECEIVER_NAME
					FROM sys_mail_events ME
					  INNER JOIN sys_mail_eventusers MEU
						ON MEU.intMailEventId = ME.intMailIventId
					  INNER JOIN sys_users U
						ON MEU.intUserId = U.intUserId
					WHERE ME.strMailEventName = 'Send emai to approve new Ink types'";

			$result_e	= $db->RunQuery($sql);
 		
		$mailHeader	= "New Ink type has been added to the system";
		while($row = mysqli_fetch_array($result_e))
		{
			$to_user	= $row['RECEIVER_NAME'];
			$from_user	= $_SESSION["systemUserName"];
			$body			= '';
			ob_start();
			include 'mail_ink_type_for_approval.php';
			$body 			= ob_get_clean();
		
			sendMessage($_SESSION["email"],$_SESSION["systemUserName"],$row['USER_EMAIL'],$mailHeader,$body,'','');
		}
		///////////////////////////////////
	
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	/////////// location update part /////////////////////
	else if($requestType=='edit')
	{
		$sql = "UPDATE `mst_inktypes` SET 	strCode		= '$code',
											strName		= '$name',
											intTechniqueId ='$typeOfPrintId',
											INK_TYPE_CATEGORY ='$typeGroup',
											intType		= $intType,
											strRemark	= '$remark',
											intStatus	= '$intStatus',
											intModifyer	= '$userId'
				WHERE (`intId`='$id')";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// location delete part /////////////////////
	else if($requestType=='delete')
	{
		$sql = "DELETE FROM `mst_inktypes` WHERE (`intId`='$id')  ";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
?>
<?php
//$backwardseperator = "../../../../../";
//include  "{$backwardseperator}dataAccess/Connector.php";

/*
$no = $_REQUEST['no'];
$year = $_REQUEST['year'];
$revNo = $_REQUEST['revNo'];
*/

$no 		= $sampleNo;
$year 		= $sampleYear;
$revNo 		= $revision;


//echo $_REQUEST['x'].'- x';
$mainPath = $_SESSION['mainPath'];
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ink Type Added email</title>
<style type="text/css">
.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntWhite {
	font-family: Verdana;
	font-size: 11px;
	color: #FFF;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.sampleNo{
	color: #039; font-weight: bold; font-size: 12px; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;	
}
.part{
	color: #096CBD;
	font-weight: bold;
	font-size: 13px;
	font-family: "Comic Sans MS", cursive;
}
.tableBorder_allRound{
	
	border: 1px solid #CCCCCC;
	-moz-border-radius-bottomright:10px;
	-moz-border-radius-bottomleft:10px;
	-moz-border-radius-topright:10px;
	-moz-border-radius-topleft:10px;
}
</style>
</head>

<body>
<table  width="595" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" class="normalfnt" style="color:#3E437D">&nbsp;</td>
  </tr>
  <tr>
    <td width="151" class="normalfnt">Dear <strong><?php echo $to_user; ?></strong>,</td>
    <td width="440">&nbsp;</td>
    <td width="4">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">I have added the new ink type : <?php echo $ink_type_name ?></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Please approve it.</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="normalfnt">Thanks,</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="normalfnt"><strong><?php echo $from_user ; ?> . </strong><br />
    ...................</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="normalfnt">(This is a<strong> <span style="color:#0000CC">NSOFT</span> </strong>system generated email.)</td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
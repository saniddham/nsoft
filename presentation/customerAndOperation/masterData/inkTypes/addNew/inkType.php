<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<title>Ink Types</title>
<form id="frmInkType" name="frmInkType" method="post" autocomplete="off"  >
<div align="center">
		<div class="trans_layoutD">
		  <div class="trans_text">Ink Types</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="585" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
              <tr>
                <td width="46" class="normalfnt">&nbsp;</td>
                <td width="158" class="normalfnt">Ink Types</td>
                <td colspan="2">
                  <select name="cboSearch" class="txtbox" id="cboSearch"  style="width:250px;" <?php echo ($form_permision['edit']?'':'disabled="disabled"')?> tabindex="1">
                  <option value=""></option>
                 <?php  $sql = "SELECT
									intId,
									strName
								FROM mst_inktypes
								order by strName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?> </select>				</td>                
                </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td>&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                </tr>
<tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Costing Group <span class="compulsoryRed">*</span></td>
                <td colspan="2"><select   name="cboInkTypeGroup" class="validate[required] txtbox" id="cboInkTypeGroup"  style="width:250px;" tabindex="4" >
                  <option value=""></option>
                  <?php  $sql = "SELECT
									ID,
									DESCRIPTION
								FROM mst_ink_type_category
								order by DESCRIPTION
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['ID']."\">".$row['DESCRIPTION']."</option>";
								}
                   ?>
                </select></td>
              </tr>              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Ink Types Code&nbsp;<span class="compulsoryRed">*</span></td>
                <td width="142"><input  name="txtCode" type="text" class="validate[required,maxSize[10]]" id="txtCode" style="width:140px"  tabindex="2"/></td>
                <td width="209" class="normalfnt">&nbsp;</td>
                </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Ink Types Name&nbsp;<span class="compulsoryRed">*</span></td>
                <td colspan="2"><input name="txtName" type="text" class="validate[required,maxSize[90]]" id="txtName" style="width:250px; height:30px" tabindex="3" maxlength="90" /></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Technique <span class="compulsoryRed">*</span></td>
                <td colspan="2"><select   name="cboTypeOfPrint" class="validate[required] txtbox" id="cboTypeOfPrint"  style="width:250px;" tabindex="4" >
                  <option value=""></option>
                  <?php  $sql = "SELECT
									intId,
									strName
								FROM mst_techniques
								order by strName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?>
                </select></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Remarks</td>
                <td colspan="2">
                  <textarea name="txtRemark" style="width:250px" class="validate[maxSize[250]]"  rows="2" id="txtRemark"  tabindex="5"></textarea>				</td>
              </tr>
              <tr>
                <td rowspan="3" class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Category Type</td>
                <td colspan="2"><select   name="cboType" class="txtbox" id="cboType"  style="width:250px;" tabindex="6">
                  <option value="null"></option>
                  <option value="1">Weight</option>
                  <option value="2">Size</option>
                  <option value="3">Special RM</option>
                 
                </select></td>
                </tr>
              <tr>
                <td class="normalfnt">Active</td>
                <td><input type="checkbox" name="chkActive" id="chkActive" checked="checked" tabindex="7"/></td>
                <td class="normalfnt">&nbsp;</td>
                </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td></td>
                </tr>
              </table></td>
            </tr>
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor="">
                <img border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="10"/>
                <img <?php echo $form_permision['add']?'':'style="display:none"'?> border="0" src="images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="8"/>
                <img <?php echo $form_permision['delete']?'':'style="display:none"'?> border="0" src="images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="9"/>
                <a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="11"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
</form>
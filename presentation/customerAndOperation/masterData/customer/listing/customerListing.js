var basePath	="presentation/customerAndOperation/masterData/customer/listing/";


$(document).each(function(index, element) {
    $('.butApprove').live('click',Approve);
	$('.butDeActive').live('click',DeActive);
});

function Approve()
{
	var x = confirm("Are you sure want to 'Approve Selected Customer'.");
	if(!x)return;
	
	var url 	 = basePath+'customer-db-set.php?requestType=Approve';
		url 	+= '&CustomerId='+$(this).parent().parent().find('td:eq(0)').html();
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
			if(json.type=='pass')
				document.location.href  = document.location.href;
		}
	});
}

function DeActive()
{
	var x = confirm("Are you sure want to 'Deactivate Selected Customer'.");
	if(!x)return;
	
	var url 	 = basePath+'customer-db-set.php?requestType=DeActive';
		url 	+= '&CustomerId='+$(this).parent().parent().find('td:eq(0)').html();
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
			if(json.type=='pass')
				document.location.href  = document.location.href;
		}
	});
}
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
require_once("libraries/jqgrid2/inc/jqgrid_dist.php");

$intUser		= $_SESSION["userId"];
$programCode	= 'P0019';
$approveLevel 	= GetMaxApproveLevel();

$sql = "SELECT SUB_1.* FROM
		(SELECT 
			CU.intId 								AS ID,
			CU.strCode								AS CUSTOMER_CODE,
			CU.strName								AS CUSTOMER_NAME,
			CONCAT(CU.strAddress,' ',CU.strCity) 	AS ADDRESS,
			CU.intTypeId							AS MARKETER_ID,
			TM.strName								AS MARKETER_NAME,
			CU.intCountryId							AS COUNTRY_ID,
			CO.strCountryName						AS COUNTRY_NAME,
			CURR.intId								AS CURRENCY_ID,
			CURR.strCode							AS CURRENCY_CODE,
			CU.strPhoneNo							AS PHONE_NO,
			CU.strMobileNo							AS MOBILE_NO,
			CU.strFaxNo 							AS FAX_NO,
			CU.strEmail								AS EMAIL,
			CU.strWebSite							AS WEBSITE,
			
			if(CU.intStatus=1,'Approved',if(CU.intStatus=0,'Rejected',if(CU.intStatus=-10,'Completed',if(CU.intStatus=-2,'Cancelled',if(CU.intStatus=-1,'Revised','Pending'))))) as STATUS,
			IFNULL((
			SELECT
			concat(U.strUserName,'(',max(IUA.dtApprovedDate),')' )
			FROM
			mst_customer_approvedby IUA
			Inner Join sys_users U 
				ON IUA.intApproveUser = U.intUserId
			WHERE
				IUA.intCustomerId  = CU.intId AND
				IUA.intApproveLevelNo =  '1' AND 
				IUA.intStatus = '0'
				),IF(((SELECT
				MP.int1Approval 
			FROM menupermision MP
			Inner Join menus M 
				ON MP.intMenuId = M.intId
			WHERE
				M.strCode = '$programCode' AND
				MP.intUserId =  '$intUser') = 1 
				AND CU.intStatus > 1),'Approve', '')) AS `1st_Approval`, ";
				
for($i=2; $i<=$approveLevel; $i++)
{							
	if($i==2){
		$approval	= "2nd_Approval";
	}
	else if($i==3){
		$approval	= "3rd_Approval";
	}
	else {
		$approval	= $i."th_Approval";
	}
	
	
$sql .= "IFNULL(
		(
		SELECT
		concat(U.strUserName,'(',max(IUA.dtApprovedDate),')' )
		FROM
		mst_customer_approvedby IUA
		Inner Join sys_users U
			ON IUA.intApproveUser = U.intUserId
		WHERE
			IUA.intCustomerId  = CU.intId AND
			IUA.intApproveLevelNo =  '$i' AND 
			IUA.intStatus = '0'
		),
		IF(
		((SELECT
			MP.int".$i."Approval 
			FROM menupermision MP
			Inner Join menus M
				ON MP.intMenuId = M.intId
			WHERE
				M.strCode = '$programCode' 
				AND MP.intUserId =  '$intUser')=1 
				AND (CU.intStatus > 1) 
				AND (CU.intStatus <= CU.intApproveLevel) 
				AND ((SELECT
					max(IUA.dtApprovedDate) 
					FROM
					mst_customer_approvedby IUA
					Inner Join sys_users U
						ON IUA.intApproveUser = U.intUserId
					WHERE
					IUA.intCustomerId  = CU.intId AND
					IUA.intApproveLevelNo =  ($i-1) AND 
					IUA.intStatus='0')<>'')),		
			'Approve',
			 if($i>CU.intApproveLevel,'-----',''))		
			) as `".$approval."`, "; 
	}				
	
							$sql .= "IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(mst_customer_approvedby.dtApprovedDate),')' )
								FROM
								mst_customer_approvedby
								Inner Join sys_users ON mst_customer_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								mst_customer_approvedby.intCustomerId  = CU.intId AND
 								mst_customer_approvedby.intApproveLevelNo =  '0' AND
								mst_customer_approvedby.intStatus =  '0'
							),IF(((SELECT
								menupermision.intReject 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND CU.intStatus<=CU.intApproveLevel AND 
								CU.intStatus>1),'Reject', '')) as `Reject`,";
								
							$sql .= "IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(mst_customer_approvedby.dtApprovedDate),')' )
								FROM
								mst_customer_approvedby
								Inner Join sys_users ON mst_customer_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								mst_customer_approvedby.intCustomerId  = CU.intId AND
 								mst_customer_approvedby.intApproveLevelNo =  '-2' AND
								mst_customer_approvedby.intStatus =  '0'
							),IF(((SELECT
								menupermision.intCancel 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND 
								CU.intStatus=1),'Cancel', '')) as `Cancel`,";
								
							$sql .= "IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(mst_customer_approvedby.dtApprovedDate),')' )
								FROM
								mst_customer_approvedby
								Inner Join sys_users ON mst_customer_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								mst_customer_approvedby.intCustomerId  = CU.intId AND
 								mst_customer_approvedby.intApproveLevelNo =  '-1' AND
								mst_customer_approvedby.intStatus =  '0'
							),IF(((SELECT
								menupermision.intRevise 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND 
								CU.intStatus=1),'Revise', '')) as `Revise`,";
						
							$sql .= "'View' as `View`, ";  
	
					
$sql .=	"CU.strContactPerson						AS CONTACT_PERSON,
		IF(CU.intStatus=1,'Active','In-active')		AS ACT_STATUS
		FROM mst_customer CU
		LEFT JOIN mst_typeofmarketer TM
			ON TM.intId = CU.intTypeId
		LEFT JOIN mst_country CO
			ON CO.intCountryID = CU.intCountryId
		INNER JOIN mst_financecurrency CURR
			ON CURR.intId = CU.intCurrencyId
		ORDER BY CU.strName)  AS SUB_1 WHERE 1=1 ";

$jq = new jqgrid('',$db);	
$col 	= array();
$cols 	= array();
 
$col["title"] 			= "Status";
$col["name"] 			= "STATUS";
$col["width"] 			= "4";
$col["align"] 			= "center";
$col["sortable"] 		= false; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col["stype"] 			= "select";
$str 					= "Pending:Pending;Approved:Approved;Rejected:Rejected;Cancelled:Cancelled;Revised:Revised;:All" ;
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "ID";
$col["name"] 			= "ID";
$col["width"] 			= "1";
$col['link']			= '?q=24&id={ID}';
$col["linkoptions"] 	= "target='customer.php'";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Code";
$col["name"] 			= "CUSTOMER_CODE";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Name";
$col["name"] 			= "CUSTOMER_NAME";
$col["width"] 			= "6";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Address";
$col["name"] 			= "ADDRESS";
$col["width"] 			= "4";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Type";
$col["name"] 			= "MARKETER_NAME";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
//$col["dbname"] 			= "MARKETER_ID";
//$client_lookup 			= $jq->get_dropdown_values("SELECT intId AS k,strName AS v FROM mst_typeofmarketer WHERE intStatus = 1 order by strName");
//$col["stype"] 			= "select";
//$str 					= ":;".$client_lookup;
//$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Country";
$col["name"] 			= "COUNTRY_NAME";
$col["width"] 			= "3";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
//$col["dbname"] 			= "COUNTRY_ID";
//$client_lookup 			= $jq->get_dropdown_values("SELECT intCountryID AS k,strCountryName AS v FROM mst_country");
//$col["stype"] 			= "select";
//$str 						= ":;".$client_lookup;
//$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Currency";
$col["name"] 			= "CURRENCY_CODE";
$col["width"] 			= "2";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
//$col["dbname"] 			= "CURRENCY_ID";
//$client_lookup 			= $jq->get_dropdown_values("SELECT intId AS k,strCode AS v FROM mst_financecurrency ORDER BY strCode");
//$col["stype"] 			= "select";
//$str 					= ":;".$client_lookup;
//$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Phone No";
$col["name"] 			= "PHONE_NO";
$col["width"] 			= "3";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Mobile No";
$col["name"] 			= "MOBILE_NO";
$col["width"] 			= "3";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Fax No";
$col["name"] 			= "FAX_NO";
$col["width"] 			= "3";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Email";
$col["name"] 			= "EMAIL";
$col["width"] 			= "3";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Web Site";
$col["name"] 			= "WEBSITE";
$col["width"] 			= "3";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Contact Person";
$col["name"] 			= "CONTACT_PERSON";
$col["width"] 			= "3";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "1st Approval";
$col["name"] 			= "1st_Approval";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col['link']			= '?q=910&customerId={ID}&mode=Confirm';
$col["linkoptions"] 	= "target='rptCustomer.php'";
$col["linkName"] 		= "Approve";
$col["sortable"] 		= false; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

for($i=2;$i<=$approveLevel;$i++)
{
if($i==2){
	$ap		= "2nd Approval";
	$ap1	= "2nd_Approval";
}
else if($i==3){
	$ap		= "3rd Approval";
	$ap1	= "3rd_Approval";
}
else{
	$ap		= $i."th Approval";
	$ap1	= $i."th_Approval";
}

$col["title"] 			= $ap;
$col["name"] 			= $ap1;
$col["width"] 			= "3";
$col["align"] 			= "center";
$col['link']			= "#";
$col['link']			= '?q=910&customerId={ID}&mode=Confirm';
$col["linkoptions"] 	= "target='rptCustomer.php'";
$col["linkName"] 		= "Approve";
$col["sortable"] 		= false; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;
}


/*$col["title"] 		= 'Reject'; // caption of column
$col["name"] 			= 'Reject'; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '../addNew/rptCustomer.php?customerId={ID}&mode=Reject';
$col["linkoptions"] 	= "target='../addNew/rptCustomer.php'";
$col['linkName']		= 'Reject';
$cols[] 				= $col;	
$col					=NULL;
*/

$col["title"] 			= 'Cancel'; // caption of column
$col["name"] 			= 'Cancel'; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "4";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '?q=910&customerId={ID}&mode=Cancel';
$col["linkoptions"] 	= "target='rptCustomer.php'";
$col['linkName']		= 'Cancel';
$cols[] 				= $col;	
$col					=NULL;

$col["title"] 			= 'Revise'; // caption of column
$col["name"] 			= 'Revise'; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "4";
$col["search"]			= false;
$col["align"] 			= "center";
$col['link']			= '?q=910&customerId={ID}&mode=Revise';
$col["linkoptions"] 	= "target='rptCustomer.php'";
$col['linkName']		= 'Revise';
$cols[] 				= $col;	
$col					=NULL;

//VIEW
$col["title"] 			= "Report"; // caption of column
$col["name"] 			= "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "2";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= '?q=910&customerId={ID}';
$col["linkoptions"] 	= "target='rptCustomer.php'";
$cols[] 				= $col;	
$col					=NULL;

$grid["caption"] 		= "Customer Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'CUSTOMER_NAME'; // by default sort grid by this field
$grid["sortorder"] 		= "ASC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"}
     ]
	 
}
SEARCH_JSON;


$grid["search"] 	= true; 
$grid["postData"] 	= array("filters" => $sarr ); 

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Customer Listing</title>
<?php echo $out?>
<?php
function GetMaxApproveLevel()
{
	global $db;	
	$appLevel	= 0;
	
	$sql = "SELECT
			COALESCE(Max(mst_customer.intApproveLevel),0) AS appLevel
			FROM mst_customer";	
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	return $row['appLevel'];
}
?>
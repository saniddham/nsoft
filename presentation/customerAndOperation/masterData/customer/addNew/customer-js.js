var basePath	="presentation/customerAndOperation/masterData/customer/addNew/";
var reportId	="";

var arrLocate = [];
var arrBrand = [];

var invoType = "";
function functionList()
{
    if(cusId!='')
    {
        $('#frmCustomer #cboSearch').val(cusId);
        $('#frmCustomer #cboSearch').change();
    }
}

$(document).ready(function() {
  	
	$('#frmCustomer #cboFinanceType').die('change').live('change',function(){LoadMainType('frmCustomer');});
	$('#frmCustomer #cboMainType').die('change').live('change',function(){LoadSubType('frmCustomer');});
	$('#frmCustomer #cboSubType').die('change').live('change',function(){loadChartOfAccDefault('frmCustomer');});
	$('#frmCustomer #txtName').die('keyup').live('keyup',function(){loadChartOfAccDefault('frmCustomer');});
	
	$('#frmRptCustomer #cboFinanceType').die('change').live('change',function(){LoadMainType('frmRptCustomer');});
	$('#frmRptCustomer #cboMainType').die('change').live('change',function(){LoadSubType('frmRptCustomer');});
	
	$('#frmCustomer #butConfirm').die('click').live('click',ViewConfirmRpt);
	$('#frmCustomer #butReport').die('click').live('click',ViewRpt);
 	
	$('#frmRptCustomer #butRptConfirm').die('click').live('click',ConfirmRpt);
	$('#frmRptCustomer #butRptReject').die('click').live('click',RejectRpt);
	$('#frmRptCustomer #butRptCancel').die('click').live('click',CancelRpt);
	$('#frmRptCustomer #butRptRevise').die('click').live('click',ReviseRpt);
	
	$("#frmCustomer").validationEngine();
	$('#frmCustomer #txtCode').live();
        $("#frmCustomer #butBundleTag").click(function(){
            var cus_Id = $('#cboSearch').val();
            loadPopup(cus_Id);
	});

	$('#frmCustomer #txtCreditLimit').die('keyup').live('keyup',function(e){
			if($(this).val()=="" || isNaN($(this).val())){
				$(this).val(0);
				$(this).select();
			}
 	});

  ///save button click event
  $('#frmCustomer #butSave').die('click').live('click',function(){
	
	var brandValue="[ ";
		$('.brandname:checked').each(function(){
			brandValue += '{ "brandId":"'+$(this).val()+'"},';
		});
		brandValue = brandValue.substr(0,brandValue.length-1);
		brandValue += " ]";
		
	var locationValue="[ ";
		$('.locations:checked').each(function(){
			locationValue += '{ "locationId":"'+$(this).val()+'"},';
		});
		locationValue = locationValue.substr(0,locationValue.length-1);
		locationValue += " ]";
		
	var requestType = '';
	if ($('#frmCustomer').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmCustomer #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = basePath+"customer-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json", 
		data:$("#frmCustomer").serialize()+'&requestType='+requestType+'&brand='+brandValue+'&location='+locationValue/*+'&invoType='+invoType*/,
			//data:'{"requestType":"addLocation"}',
			async:false,
			type:'POST',
			success:function(json){
					$('#frmCustomer #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmCustomer').get(0).reset();
						loadCombo_frmCustomer();
						var t=setTimeout("alertx()",1000);
						$('#txtCode').val(json.nxtNo);
						//location.reload(true);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmCustomer #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load customer details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmCustomer #cboSearch').die('change').live('change',function(){
	   $('#frmCustomer').validationEngine('hide');
   });
    $('#frmCustomer #cboSearch').die('change').live('change',function(){
		$('#frmCustomer').validationEngine('hide');
		var url = "presentation/customerAndOperation/masterData/customer/addNew/customer-db-get.php";
		if($('#frmCustomer #cboSearch').val()=='')
		{
			$('#frmCustomer').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					
					$('#frmCustomer #txtCode').val(json.code);
					$('#frmCustomer #txtName').val(json.name);
					$('#frmCustomer #cboType').val(json.type);
					$('#frmCustomer #txtAddress').val(json.address);
					$('#frmCustomer #txtContact').val(json.contact);
					$('#frmCustomer #txtCity').val(json.city);
					$('#frmCustomer #cboCountry').val(json.country);
					$('#frmCustomer #cboCurrency').val(json.currency);
					$('#frmCustomer #txtPhone').val(json.phone);
                	$('#frmCustomer #emailMethod').val(json.emailMethod);
					
					$('#frmCustomer #txtMobile').val(json.mobile);
					$('#frmCustomer #txtFax').val(json.fax);
					$('#frmCustomer #txtEMail').val(json.email);
					$('#frmCustomer #txtWeb').val(json.web);
					$('#frmCustomer #cboShipmentMethod').val(json.shipment);
					$('#frmCustomer #txtVatNo').val(json.vatNo);
					$('#frmCustomer #txtSvatNo').val(json.sVatNo);
					$('#frmCustomer #txtRegNo').val(json.regNo);
					$('#frmCustomer #cboInvoiceType').val(json.invoType);
 					invoType 			= json.invoType;
						
					var subType 		= json.subType;
					var mainType		= json.mainType;
					var financeType		= json.financeType
					var chartOfAccName	= json.chartOfAccName;
					$('#frmCustomer #cboFinanceType').val(financeType);
					LoadMainType('frmCustomer');
					$('#frmCustomer #cboMainType').val(mainType);
					LoadSubType('frmCustomer');	
					$('#frmCustomer #cboSubType').val(subType);
					$('#frmCustomer #txtChartOfAccount').val(chartOfAccName);
					$('#frmCustomer #txtChartOfAccountDB').val(chartOfAccName);
				
					$('#frmCustomer #cboPaymentsTerms').val(json.payterms);
					$('#frmCustomer #cboPaymentsMethods').val(json.paymethods);
					$('#frmCustomer #txtCreditLimit').val(json.creditlimit);
					$('#frmCustomer #cboChartOfAcc').val(json.chartofaccount);
					$('#frmCustomer #cboBlocked').val(json.blocked);
					$('#frmCustomer #txtRank').val(json.rank);
					var savePermision = json.permision_save;
					var approvePermision = json.permision_approve;
					if(savePermision==1){
						$('#frmCustomer #butSave').show();
					}
					else{
						$('#frmCustomer #butSave').hide();
					}
					
					if(approvePermision==1){
						$('#frmCustomer #butConfirm').show();
					}
					else{
						$('#frmCustomer #butConfirm').hide();
					}
					
 					if($('#frmCustomer #cboSearch').val()!=''){
						$('#frmCustomer #butReport').show();
					}
					else{
						$('#frmCustomer #butReport').hide();
					}
				//alert(json.locatVal.length);
				//--------------------------------------------------
				var brnId = "";
				if(json.brnVal!=null){
				$('.brandname:checkbox').removeAttr('checked');	
				//alert(1);
				for(var i=0;i<=json.brnVal.length-1;i++)
				{
					
					brnId = json.brnVal[i].brandId;
					$('#frmCustomer #brn'+brnId).prop('checked',true);					
				}}
				else
				{
					$('.brandname:checkbox').removeAttr('checked');	
				}
				//--------------------------------------------------

				//--------------------------------------------------
				var locId = "";
				if(json.locatVal!=null){
				$('.locations:checkbox').removeAttr('checked');	
				for(var j=0;j<=json.locatVal.length-1;j++)
				{
					locId = json.locatVal[j].locateId;
					$('#frmCustomer #loc'+locId).prop('checked',true);					
				}}
				else
				{
					$('.locations:checkbox').removeAttr('checked');
				}
				//--------------------------------------------------
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmCustomer #butNew').die('click').live('click',function(){
		window.location.href	= '?q=24';
		loadCombo_frmCustomer();
		$('#frmCustomer #txtCode').focus();
 	});
	
    $('#frmCustomer #butDelete').die('click').live('click',function(){
		if($('#frmCustomer #cboSearch').val()=='')
		{
			$('#frmCustomer #butDelete').validationEngine('showPrompt', 'Please select Customer.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmCustomer #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "presentation/customerAndOperation/masterData/customer/addNew/customer-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type: 'post',
											data:'requestType=delete&cboSearch='+$('#frmCustomer #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmCustomer #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmCustomer').get(0).reset();
													loadCombo_frmCustomer();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
 });


function loadCombo_frmCustomer()
{
	var url 	= "presentation/customerAndOperation/masterData/customer/addNew/customer-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmCustomer #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmCustomer #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmCustomer #butDelete').validationEngine('hide')	;
}

function popupLocations()
{
	popupWindow('1');	
}

function popupBrands()
{
	popupWindow('2');	
}

function loadLocations()
{
	$("#butLocations").die('click').live('click',popupLocations);
}

function loadBrands()
{
	$("#butBrandName").die('click').live('click',popupBrands);
}

function closePopUp()
{
	arrLocate = [];
	arrBrand = [];
	
	var i = 0;
	var k = 0;

		$('.locations:checked').each(function(){
			arrLocate[i++]=$(this).val();
		});
		$('.brandname:checked').each(function(){
			arrBrand[k++]=$(this).val();
		});
	
	$("#tblLocations").load(basePath+'table-location.php',function(){
	for(var j=0;j<=arrLocate.length-1;j++)
	{
		$('#frmCustomer #loc'+arrLocate[j]).prop('checked',true);					
	}
	});
	
	$("#tblBrands").load(basePath+'table-brand.php',function(){
	for(var l=0;l<=arrBrand.length-1;l++)
	{
		$('#frmCustomer #brn'+arrBrand[l]).prop('checked',true);					
	}
	});
}
function LoadMainType(formId)
{
 	$('#'+formId+' #cboMainType').html('');
	$('#'+formId+' #cboSubType').html('');
	$('#'+formId+' #cboChartOfAccount').html('');
	
	if($('#'+formId+' #cboFinanceType').val()=='')
		return;
		
	var url = basePath+"customer-db-get.php?requestType=loadMainType";
	var obj = $.ajax({
		url:url,
		dataType: "json",  
		data:"&financeType="+$('#'+formId+' #cboFinanceType').val(),
		async:false,
		success:function(json){
			
			$('#'+formId+' #cboMainType').html(json.mainTypeCombo);			
		},
		error:function(xhr,status){
				
		}		
	});
}
function LoadSubType(formId)
{
	$('#'+formId+' #cboSubType').html('');
	$('#'+formId+' #cboChartOfAccount').html('');
	
	if($('#'+formId+' #cboMainType').val()=='')
		return;
 	var url = basePath+"customer-db-get.php?requestType=loadSubType";
	var obj = $.ajax({
		url:url,
		dataType: "json",  
		data:"&mainType="+$('#'+formId+' #cboMainType').val(),
		async:false,
		success:function(json){
			
			$('#'+formId+' #cboSubType').html(json.subTypeCombo);			
		},
		error:function(xhr,status){
				
		}		
	});
}
function loadChartOfAccDefault(formId)
{
 	$('#'+formId+' #cboChartOfAccount').html('');
	
	if(($('#'+formId+' #cboSubType').val()=='') || ($('#'+formId+' #cboSubType').val()==null)){
 		return false;
	}
	else if($('#'+formId+' #txtChartOfAccountDB').val() == ''){	
		$('#'+formId+' #txtChartOfAccount').val($('#'+formId+' #txtName').val());
	}
}

function ViewConfirmRpt()
{
	if($('#frmCustomer #cboSearch').val()=="")
	{
		alert("No details available to confirm.");
		return;
	}
 	var url  = "?q=910&customerId="+$('#frmCustomer #cboSearch').val()+"&mode=Confirm";
 	window.open(url,'rptCustomer.php');
}

function ViewRpt()
{
	if($('#frmCustomer #cboSearch').val()=="")
	{
		alert("No details available to view report.");
		return;
	}
 	var url  = "?q=910&customerId="+$('#frmCustomer #cboSearch').val()+"&mode=";
	window.open(url,'rptCustomer.php');
}

 function ConfirmRpt(){
	 
	var chartOfAccName	=$('#frmRptCustomer #txtChartOfAccount').val();
	var subType			=$('#frmRptCustomer #cboSubType').val();

	var val = $.prompt('Are you sure you want to approve this Customer ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = basePath+"customer-db-set.php"+window.location.search+'&requestType=approve&subType='+subType+'&chartOfAccName='+chartOfAccName;
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptCustomer #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptCustomer #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
					   hideWaiting();
				}});
  }
  
  
  function RejectRpt(){
	  
	var val = $.prompt('Are you sure you want to reject this Customer ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
				showWaiting();
				var url = basePath+"customer-db-set.php"+window.location.search+'&requestType=reject';
				var obj = $.ajax({
					url:url,
					type:'post',
					dataType: "json",  
					data:'',
					async:false,
					
					success:function(json){
							$('#frmRptCustomer #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							if(json.type=='pass')
							{
								var t=setTimeout("alertx()",1000);
								window.location.href = window.location.href;
								window.opener.location.reload();//reload listing page
								return;
							}
						},
					error:function(xhr,status){
							
							$('#frmRptCustomer #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",3000);
						}		
					});
				////////////
					}
					hideWaiting();
			}});
	  
  }

 
function CancelRpt(){

	var val = $.prompt('Are you sure you want to cancel this Customer ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
				if(v)
				{
					showWaiting();
					var url = basePath+"customer-db-set.php"+window.location.search+'&requestType=cancel';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptCustomer #butRptCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptCustomer #butRptCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
						hideWaiting();
 				}});
	
}

function ReviseRpt(){

	var val = $.prompt('Are you sure you want to revise this Customer ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
				if(v)
				{
					showWaiting();
					var url = basePath+"customer-db-set.php"+window.location.search+'&requestType=revise';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptCustomer #butRptRevise').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptCustomer #butRptRevise').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
						hideWaiting();
 				}});
	
}
//bundle tag
function loadPopup(cusId)
{ 
    popupWindow('1');
    $('#popupContact1').load(basePath+'bundleTagPopup.php?cusId='+cusId,function(){
        
     $('#cboTag').change(function(){
       
                        var cusLocation = $('#cboTag').val();
                        
                        var url 		= basePath+"customer-db-get.php?requestType=loadBundletag&cusLocation="+cusLocation+'&cus='+cusId;
                        var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								
							 $("#txtTag").val(json.BundleTag);
								
							}
								
						});
           });   
           
       $('#butSaveB').die('click').live('click',function(){
            var cusLocation  =  $('#cboTag').val();
            var bundleTag    =  $("#txtTag").val();
            
            var url 	     =  basePath+"customer-db-set.php?requestType=saveBundletag&cusLocation="+cusLocation+'&bundleTag='+bundleTag+'&cus='+cusId;
            var obj = $.ajax({
                            url:url,
                            type:'post',
                            dataType: "json",  
                            data:'',
                            async:false,
						
                            success:function(json){
                                     $('#butSaveB').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);   
                                     //$("#txtTag").val(json.BundleTag);

                                    }
								
                                });
           
       });     
    
    
});

}
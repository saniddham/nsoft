<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once  ("class/cls_commonErrorHandeling_get.php");

$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$progrmCode			='P0019';

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$locationId 		= $session_locationId;

$customerId			= $_REQUEST["customerId"];
$mode				= $_REQUEST["mode"];
if($mode==''){
 $disabled			='disabled=disabled';
}
  
$header_array 		= get_ReportHeaderDetails_array($customerId);
$brand_result		= get_ReportGridDetails_brand_result($customerId);
$location_result	= get_ReportGridDetails_location_result($customerId);
 
$permision_arr		= $obj_commonErr->get_permision_withApproval_save($header_array['status'],$header_array['levels'],$session_userId,$progrmCode,'RunQuery');
$permision_save		= $permision_arr['permision'];	
$permision_arr		= $obj_commonErr->get_permision_withApproval_cancel($header_array['status'],$header_array['levels'],$session_userId,$progrmCode,'RunQuery');
$permision_cancel	= $permision_arr['permision'];	
$permision_arr		= $obj_commonErr->get_permision_withApproval_reject($header_array['status'],$header_array['levels'],$session_userId,$progrmCode,'RunQuery');
$permision_reject	= $permision_arr['permision'];	
$permision_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_array['status'],$header_array['levels'],$session_userId,$progrmCode,'RunQuery');
$permision_confirm	= $permision_arr['permision'];	
$permision_arr		= $obj_commonErr->get_permision_withApproval_revise($header_array['status'],$header_array['levels'],$session_userId,$progrmCode,'RunQuery');
$permision_revise	= $permision_arr['permision'];	
 
if(!isset($_REQUEST["SerialNo"]))
	$invoiceDate	= date("Y-m-d");
else
	$invoiceDate	= $header_array["INVOICED_DATE"];
?>
<head>
<title>Customer Report</title>
 <script type="application/javascript" src="presentation/customerAndOperation/masterData/customer/addNew/customer-js.js"></script>
  <style type="text/css">
.apDiv1 {
	position: absolute;
	left: 335px;
	top: 154px;
	width: auto;
	height: auto;
	z-index: 0;
	opacity: 0.1;
}
  </style>
</head>

<body>
<div id="partPay" class="apDiv1 <?php echo $header_array["status"]=='10'?'maskShow':'maskHide'?>"><img src="images/cancelled.png"  /></div>
<form id="frmRptCustomer" name="frmRptCustomer" method="post"  autocomplete="off">
  <table width="900" align="center">
    <tr>
      <td><?php include 'reportHeader.php'?></td>
    </tr>
    <tr>
      <td style="text-align:center" class="reportHeader">Customer Report</td>
    </tr>
    <tr>
    <td align="center" bgcolor="#EAEAFF">
		<?php if(($mode=='Confirm')&&($permision_confirm==1)){?>
            <a class="button white medium" id="butRptConfirm">Approve</a>
        <?php
        }
/*        if(($mode=='Reject')&&($permision_reject==1)){?>
*/        if(($mode=='Confirm')&&($permision_reject==1)){?>
            <a class="button white medium" id="butRptReject">Reject</a>
        <?php
        }
        if(($mode=='Cancel')&&($permision_cancel==1)){?>
            <a class="button white medium" id="butRptCancel">Cancel</a>
        <?php
        }
        if(($mode=='Revise')&&($permision_revise==1)){?>
            <a class="button white medium" id="butRptRevise">Revise</a>
        <?php
        }
        ?>
      </td>
  </tr> 
<tr>
  <?php
 	if($header_array["status"]==1){?><td class="APPROVE" >CONFIRMED</td><?PHP }
	else if($header_array["status"]==0)		{?> <td class="APPROVE" style="color:#FF8040">REJECTED</td><?php }
	else if($header_array["status"]==-1)	{?> <td class="APPROVE" style="color:#FF8040">REVISED</td><?php }
	else if($header_array["status"]==-2)	{?> <td class="APPROVE" style="color:#FF0000">CANCELLED</td><?php }
	else if($header_array["status"]==-10)	{?> <td class="APPROVE" style="color:#00CC66">COMPLETED</td><?php }
	else 									{?> <td class="APPROVE">PENDING</td><?php }
   ?>
  </tr>    <tr>
      <td><table width="100%" border="0" class="normalfnt">
          <tr>
            <td width="12%">Customer</td>
            <td width="1%">:</td>
            <td width="22%"><?php echo $header_array["strName"]?><input name="txtName" class="" type="text" id="txtName" style="width:300px; display:none" maxlength="50" value="<?php echo $header_array["strName"]?>"/></td>
            <td width="12%">Code</td>
            <td width="1%">:</td>
            <td width="20%"><?php echo $header_array["strCode"]?></td>
            <td width="11%">Type</td>
            <td width="1%">:</td>
            <td width="20%"><?php echo $header_array["type"]?></td>
          </tr>
          <tr>
            <td>Address</td>
            <td>:</td>
            <td><?php echo $header_array["strAddress"]?></td>
            <td>Contact Person</td>
            <td>:</td>
            <td><?php echo $header_array["strContactPerson"]?></td>
            <td>City</td>
            <td>:</td>
            <td><?php echo $header_array["strCity"]?></td>
          </tr>
<tr>
            <td>Currency</td>
            <td>:</td>
            <td><?php echo $header_array["currency"]?></td>
            <td>Phone No</td>
            <td>:</td>
            <td><?php echo $header_array["strPhoneNo"]?></td>
            <td>Mobile</td>
            <td>:</td>
            <td><?php echo $header_array["strMobileNo"]?></td>
          </tr><tr>
            <td>Fax</td>
            <td>:</td>
            <td><?php echo $header_array["strFaxNo"]?></td>
            <td>Email</td>
            <td>:</td>
            <td><?php echo $header_array["strEmail"]?></td>
            <td>Web Site</td>
            <td>:</td>
            <td><?php echo $header_array["strWebSite"]?></td>
          </tr><tr>
            <td>Shipment Method</td>
            <td>:</td>
            <td><?php echo $header_array["shipment_method"]?></td>
            <td>Payment Term</td>
            <td>:</td>
            <td><?php echo $header_array["paymentTerm"]?></td>
            <td>Country</td>
            <td>:</td>
            <td><?php echo $header_array["strCountryName"]?></td>
          </tr><tr>
            <td>Payment Method</td>
            <td>:</td>
            <td><?php echo $header_array["paymentMethod"]?></td>
            <td>Credit Limit</td>
            <td>:</td>
            <td><?php echo $header_array["intCreditLimit"]?></td>
            <td>&nbsp;</td>
            <td></td>
            <td>&nbsp;</td>
          </tr>
            
        </table></td>
    </tr>
    <tr>
      <td class="normalfnt"></td>
    </tr>
    <tr>
      <td class="normalfnt" valign="top"><table width="100%">
      <tr>
      <td width="45%" valign="top"><table width="50%" border="0" class="rptBordered" id="tblMain2">
          <thead>
            <tr>
              <th width="10%">Brands</th>
            </tr>
          </thead>
          <tbody>
            <?php
 while($row = mysqli_fetch_array($brand_result))
{ 
?>
            <tr>
              <td style="text-align:center"><?php echo ($row["strName"])?></td>
            </tr>
            <?php
 }
?>
            </tbody>
        </table></td>
        <td width="10%"></td>
      <td width="45%" valign="top"><table width="50%" border="0" class="rptBordered" id="tblMain">
          <thead>
            <tr>
              <th width="3%">Customer Locations</th>
              </tr>
          </thead>
          <tbody>
            <?php
 while($row = mysqli_fetch_array($location_result))
{ 
?>
            <tr>
              <td style="text-align:center"><?php echo ($row["strName"])?></td>
              </tr>
            <?php
 }
?>
            </tbody>
        </table></td>
      </tr></table></td>
    </tr>
    <tr>
      <td class="normalfnt"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="6" align="left" bgcolor="#E4E4E4" class="tableBorder">
                    <span class="normalfnt"><strong>Legal Information</strong></span></td>
          </tr>
                  <tr>
                    <td width="17%" class="normalfnt">VAT Reg No.</td>
                    <td width="1%" class="normalfnt">:</td>
                    <td width="50%"><?php echo $header_array["strVatNo"]?></td>
                    <td width="10%" class="normalfnt">SVAT No.</td>
                    <td width="1%">:</td>
                    <td width="21%"><?php echo $header_array["strSVatNo"]?></td>
          </tr>
                  <tr>
                    <td class="normalfnt">Business Reg No</td>
                    <td class="normalfnt">:</td>
                    <td><?php echo $header_array["strRegistrationNo"]?></td>
          </tr>
                  <tr>
                    <td class="normalfnt">Finance Type</td>
                    <td class="normalfnt">&nbsp;</td>
                    <td class="normalfnt"><select name="cboFinanceType" id="cboFinanceType" style="width:300px" <?php echo $disabled; ?>>
                        <option value=""></option>
                        <?php
                                    	$sql = "SELECT FINANCE_TYPE_ID,FINANCE_TYPE_NAME
                                    			FROM finance_mst_account_type
                                    			WHERE STATUS=1
                                    			ORDER BY FINANCE_TYPE_NAME";
                                    	$result = $db->RunQuery($sql);
										while($row=mysqli_fetch_array($result))
										{
											echo "<option value=\"".$row["FINANCE_TYPE_ID"]."\">".$row["FINANCE_TYPE_NAME"]."</option>";
										}
                                    ?>
                      </select></td>
                    <td class="normalfnt">&nbsp;</td>
                    <td class="normalfnt">&nbsp;</td>
                    <td class="normalfnt">&nbsp;</td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Main Type</td>
                    <td class="normalfnt">&nbsp;</td>
                    <td class="normalfnt"><select name="cboMainType" id="cboMainType" style="width:300px" <?php echo $disabled; ?>>
                      </select></td>
                    <td class="normalfnt">&nbsp;</td>
                    <td class="normalfnt">&nbsp;</td>
                    <td class="normalfnt">&nbsp;</td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Sub Type</td>
                    <td class="normalfnt">&nbsp;</td>
                    <td class="normalfnt"><select name="cboSubType" id="cboSubType" style="width:300px" <?php echo $disabled; ?>>
                      </select></td>
                    <td class="normalfnt">&nbsp;</td>
                    <td class="normalfnt">&nbsp;</td>
                    <td class="normalfnt">&nbsp;</td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Chart Of Account</td>
                    <td class="normalfnt">&nbsp;</td>
                    <td class="normalfnt"><input name="txtChartOfAccount" maxlength="255" class="" type="text" id="txtChartOfAccount" style="width:300px" <?php echo $disabled; ?> /><input  name="txtChartOfAccountDB" class="" type="text" id="txtChartOfAccountDB" style="width:100px;display:none" /></td>
                    <td class="normalfnt">&nbsp;</td>
                    <td class="normalfnt">&nbsp;</td>
                    <td class="normalfnt">&nbsp;</td>
                  </tr>
      </table></td>
    </tr>
    <tr>
      <td ></td>
    </tr>
     <tr>
      <td ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="98%" height="23" bgcolor="" class="normalfnt"><b>Entered By</b> - <?php echo $header_array['creator'];?></td>
                    <td width="1%" bgcolor="" class="normalfnt">&nbsp;</td>
                    <td width="1%" class="normalfnt">&nbsp;</td>
                  </tr>
<?php  
					 $sqlc = "SELECT
							mst_customer_approvedby.intApproveUser,
							mst_customer_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							mst_customer_approvedby.intApproveLevelNo
							FROM
							mst_customer_approvedby
							Inner Join sys_users ON mst_customer_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							mst_customer_approvedby.intCustomerId =  '$customerId'       order by dtApprovedDate asc
";
					 $resultc = $db->RunQuery($sqlc);
			while($rowc=mysqli_fetch_array($resultc)){
					if($rowc['intApproveLevelNo']==-10){
					$desc="Completed By ";
					$col ="#00CC66";
					}
					else if($rowc['intApproveLevelNo']==-2){
					$desc="Cancelled By ";
					$col ="#FF0000";
					}
					else if($rowc['intApproveLevelNo']==-1){
					$desc="Revised By ";
					$col ="#FF8040";
					}
					else if($rowc['intApproveLevelNo']==0){
					$desc="Rejected By ";
					$col ="#FF8040";
					}
					else if($rowc['intApproveLevelNo']==1){
					$desc="1st Approved By ";
					$col ="#000000";
					}
					else if($rowc['intApproveLevelNo']==2){
					$desc="2nd Approved By ";
					$col ="#000000";
					}
					else if($rowc['intApproveLevelNo']==3){
					$desc="3rd Approved By ";
					$col ="#000000";
					}
					else{
					$desc=$rowc['intApproveLevelNo']."th Approved By ";
					$col ="#000000";
					}
					//  $desc=$ap.$desc;
					$desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					if($rowc['UserName']=='')
					$desc2='---------------------------------';
					
					//$desc=str_pad($desc,25," ",STR_PAD_RIGHT);
					//$desc=str_replace(" ","&nbsp;",$desc);
					 ?>
                <tr>
                    <td bgcolor="#FFFFFF"><span class="normalfnt" style="color:<?php echo $col; ?>"><strong><?php echo $desc; ?>- </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
                </tr>
					<?php			 
					$finalPrintedApproval=$rowc['intApproveLevelNo'];
 			 }//end of while
			 
			$intStatus=$header_array['status'];
			$appLevels=$header_array['levels'];
			if(($finalPrintedApproval<$appLevels) && ($finalPrintedApproval>=0) && ($intStatus>0)){
			for($j=$finalPrintedApproval+1; $j<=$appLevels; $j++)
			{ 
				if($j==1)
				$desc="1st Approved By ";
				else if($j==2)
				$desc="2nd Approved By ";
				else if($j==3)
				$desc="3rd Approved By ";
				else
				$desc=$j."th Approved By ";
				$desc2='---------------------------------';
				
 				//$desc=str_pad($desc,25," ",STR_PAD_RIGHT);
				//$desc=str_replace(" ","&nbsp;",$desc);
			?>
				<tr>
					<td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc;?>- </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
				</tr>
		<?php
			}
			}
 ?>

<tr><td height="40" ></td></tr>
 
<tr height="40" >
  <td align="center" class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></td>
  <td align="center" class="normalfntMid">&nbsp;</td>
</tr>
       </table></td>
    </tr>
  </table>
</form>
 
<script type="text/javascript" language="javascript">
 		var financeType='<?php echo $header_array['FINANCE_TYPE_ID']; ?>';
		var mainType='<?php echo $header_array['MAIN_TYPE_ID']; ?>';
		var subType='<?php echo $header_array['SUB_TYPE']; ?>';
		var chartOfAccName='<?php echo $header_array['CHART_OF_ACCOUNT']; ?>';
  
		$('#frmRptCustomer #cboFinanceType').val(financeType);
		LoadMainType('frmRptCustomer');
		$('#frmRptCustomer #cboMainType').val(mainType);
		LoadSubType('frmRptCustomer');	
		$('#frmRptCustomer #cboSubType').val(subType);
		$('#frmRptCustomer #txtChartOfAccount').val(chartOfAccName);
		
		$('#frmRptCustomer #cboFinanceType').live('change',function(){LoadMainType('frmRptCustomer');});
		$('#frmRptCustomer #cboMainType').live('change',function(){LoadSubType('frmRptCustomer');});
		
   </script>

</body>
</html>
<?php
function get_ReportHeaderDetails_array($customerId){
	global $db;
	  $sql = "SELECT
			mst_customer.strCode,
			mst_customer.strName,
			mst_typeofmarketer.strName AS type,
			mst_customer.strAddress,
			mst_customer.strContactPerson,
			mst_customer.strCity,
			mst_financecurrency.strCode AS currency,
			mst_customer.strPhoneNo,
			mst_customer.strMobileNo,
			mst_customer.strFaxNo,
			mst_customer.strEmail,
			mst_customer.strWebSite,
			mst_shipmentmethod.strName AS shipment_method,
			mst_customer.strVatNo,
			mst_customer.strSVatNo,
			mst_customer.strRegistrationNo,
			mst_invoicetype.strInvoiceType,
			mst_financepaymentsterms.strName AS paymentTerm,
			mst_country.strCountryName,
			mst_financepaymentsmethods.strName AS paymentMethod,
			mst_customer.intCreditLimit,
			mst_customer.intChartOfAccountId,
			mst_customer.SUB_TYPE,
			CHART_OF_ACCOUNT,
			finance_mst_account_sub_type.MAIN_TYPE_ID ,
			finance_mst_account_main_type.FINANCE_TYPE_ID,
			mst_customer.intStatus as status,
			mst_customer.intApproveLevel as levels ,
			sys_users.strUserName as creator 
 			FROM
			mst_customer
			LEFT JOIN mst_typeofmarketer ON mst_customer.intTypeId = mst_typeofmarketer.intId
			LEFT JOIN mst_country ON mst_customer.intCountryId = mst_country.intCountryID
			LEFT JOIN mst_financecurrency ON mst_customer.intCurrencyId = mst_financecurrency.intId
			LEFT JOIN mst_shipmentmethod ON mst_customer.intShipmentId = mst_shipmentmethod.intId
			LEFT JOIN mst_invoicetype ON mst_customer.strInvoiceType = mst_invoicetype.intId
			LEFT JOIN mst_financepaymentsterms ON mst_customer.intPaymentsTermsId = mst_financepaymentsterms.intId
			LEFT JOIN mst_financepaymentsmethods ON mst_customer.intPaymentsMethodsId = mst_financepaymentsmethods.intId
			LEFT JOIN finance_mst_account_sub_type ON mst_customer.SUB_TYPE=finance_mst_account_sub_type.SUB_TYPE_ID
			LEFT JOIN finance_mst_account_main_type ON finance_mst_account_sub_type.MAIN_TYPE_ID=finance_mst_account_main_type.MAIN_TYPE_ID 
			LEFT JOIN sys_users ON mst_customer.intCreator = sys_users.intUserId
			WHERE
			mst_customer.intId = '$customerId' ";
	$result = $db->RunQuery($sql);
 	$row=mysqli_fetch_array($result);
	return $row;
	
}

function get_ReportGridDetails_brand_result($customerId){
	global $db;
	$sql = "SELECT
			mst_brand.strName
			FROM
			mst_customer_brand
			INNER JOIN mst_brand ON mst_customer_brand.intBrandId = mst_brand.intId
			WHERE
			mst_customer_brand.intCustomerId = '$customerId'
			ORDER BY
			mst_brand.strName ASC
			 ";
	$result = $db->RunQuery($sql);
	return $result;
}

function get_ReportGridDetails_location_result($customerId){
	global $db;
		$sql = "SELECT
				mst_customer_locations_header.strName
				FROM
				mst_customer_locations
				INNER JOIN mst_customer_locations_header ON mst_customer_locations.intLocationId = mst_customer_locations_header.intId
				WHERE
				mst_customer_locations.intCustomerId = '$customerId'
				ORDER BY
				mst_customer_locations_header.strName ASC";
	$result = $db->RunQuery($sql);
	return $result;
}

?>

<?php
 	session_start();
	$backwardseperator = "../../../../../";
	include_once "{$backwardseperator}dataAccess/Connector.php";
	include_once  ("../../../../../class/cls_commonErrorHandeling_get.php");
 	
 	$obj_commonErr	= new cls_commonErrorHandeling_get($db);
	
 	$progrmCode		='P0019';
 	$mainPath 		= $_SESSION['mainPath'];
	$userId 		= $_SESSION['userId'];
	$requestType	= $_REQUEST['requestType'];
 	
	/////////// customer load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql 	= "SELECT
					intId,
					strName
					FROM mst_customer
					order by strName
					";
		$result = $db->RunQuery($sql);
		$html 	= "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadMainType')
	{
		$financeType 				= $_REQUEST['financeType'];
 		$response['mainTypeCombo']	= getMainTypeCombo($financeType);
		echo json_encode($response);
	}
	else if($requestType=='loadSubType')
	{
		$mainType 					= $_REQUEST['mainType'];
 		$response['subTypeCombo']	= getSubTypeCombo($mainType);
		echo json_encode($response);
	}
 	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		
 		$sql = "SELECT intBrandId
				FROM mst_customer_brand
				WHERE
					intCustomerId =  '$id'";
		$result = $db->RunQuery($sql);
	
		$arrBrn;
		while($row=mysqli_fetch_array($result))
		{
			$val1['brandId'] 	= $row['intBrandId'];
			$arrBrn[] = $val1;
		}
		
		$response['brnVal'] = $arrBrn;
		//------------------------------
		
		//------------------------------
		$sql = "SELECT intLocationId
				FROM mst_customer_locations
				WHERE
					intCustomerId =  '$id'";
		$result = $db->RunQuery($sql);
	
		$arrlocat;
		while($row=mysqli_fetch_array($result))
		{
			$val2['locateId'] 	= $row['intLocationId'];
			$arrlocat[] = $val2;
		}
		
		$response['locatVal'] = $arrlocat;
		//-------------------------------
		
		$sql = "SELECT
					strCode,
					strName,
					intTypeId,
					strAddress,
					strContactPerson,
					strCity,
					intCountryId,
					intCurrencyId,
					strPhoneNo,
					strMobileNo,
					strFaxNo,
					strEmail,
					strWebSite,
					intShipmentId,
					strVatNo,
					strSVatNo,
					strRegistrationNo,
					strInvoiceType,
					intPaymentsTermsId,
					intPaymentsMethodsId,
					IFNULL(intCreditLimit,0) as intCreditLimit,
					intChartOfAccountId,
					strBlocked,
					intRank,
					mst_customer.intStatus,
					mst_customer.intApproveLevel,
					mst_customer.SUB_TYPE,
					mst_customer.intBrandWiseEmails,
					CHART_OF_ACCOUNT,
					finance_mst_account_sub_type.MAIN_TYPE_ID ,
					finance_mst_account_main_type.FINANCE_TYPE_ID  
				FROM mst_customer 
				LEFT JOIN finance_mst_account_sub_type ON mst_customer.SUB_TYPE=finance_mst_account_sub_type.SUB_TYPE_ID
				LEFT JOIN finance_mst_account_main_type ON finance_mst_account_sub_type.MAIN_TYPE_ID=finance_mst_account_main_type.MAIN_TYPE_ID 
				WHERE
					intId =  '$id'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['code'] 			= $row['strCode'];
			$response['name'] 			= $row['strName'];
			$response['type'] 			= $row['intTypeId'];
			$response['address'] 		= $row['strAddress'];
			$response['contact'] 		= $row['strContactPerson'];
			$response['city'] 			= $row['strCity'];
			$response['country'] 		= $row['intCountryId'];
			$response['currency'] 		= $row['intCurrencyId'];
			$response['phone'] 			= $row['strPhoneNo'];
			$response['mobile'] 		= $row['strMobileNo'];
			$response['fax'] 			= $row['strFaxNo'];
			$response['email'] 			= $row['strEmail'];
			$response['web'] 			= $row['strWebSite'];
			$response['shipment'] 		= $row['intShipmentId'];
			$response['vatNo'] 			= $row['strVatNo'];
			$response['sVatNo'] 		= $row['strSVatNo'];
			$response['regNo'] 			= $row['strRegistrationNo'];
			$response['invoType'] 		= $row['strInvoiceType'];
			$response['payterms'] 		= $row['intPaymentsTermsId'];
			$response['paymethods'] 	= $row['intPaymentsMethodsId'];
			$response['creditlimit'] 	= $row['intCreditLimit'];
			$response['chartofaccount'] = $row['intChartOfAccountId'];
			$response['blocked'] 		= $row['strBlocked'];
			$response['rank'] 			= $row['intRank'];
			$response['status'] 		= ($row['intStatus']?true:false);
			$response['subType'] 		= $row['SUB_TYPE'];
			$response['chartOfAccName'] = $row['CHART_OF_ACCOUNT'];
			$response['mainType'] 		= $row['MAIN_TYPE_ID'];
			$response['financeType'] 	= $row['FINANCE_TYPE_ID'];
 			$permision_arr				= $obj_commonErr->get_permision_withApproval_save($row['intStatus'],$row['intApproveLevel'],$userId,$progrmCode,'RunQuery');
 			$permision_save				= $permision_arr['permision'];	
			$response['permision_save']	= $permision_save;
 			$permision_arr				= $obj_commonErr->get_permision_withApproval_confirm($row['intStatus'],$row['intApproveLevel'],$userId,$progrmCode,'RunQuery');
 			$permision_approve			= $permision_arr['permision'];	
			$response['permision_approve']	= $permision_approve;
            $response['emailMethod']	= $row['intBrandWiseEmails'];;
		}
		echo json_encode($response);
	}
        
                else if($requestType=='loadBundletag')
        {
                $cusLocation  = $_REQUEST['cusLocation'];
                $cus          = $_REQUEST['cus'];

                $sql = "select mst_customer_locations.BundleTag
                        FROM
                        mst_customer_locations
                        WHERE
                        mst_customer_locations.intCustomerId = $cus 
                        AND
                        mst_customer_locations.intLocationId = $cusLocation";

                $result     =   $db->RunQuery($sql);
                $row        =   mysqli_fetch_array($result);
                $response['BundleTag']  = $row['BundleTag'];

                echo json_encode($response);

        }



//--------------------------
function getMainTypeCombo($financeType)
{
	global $db;
	
	$sql = "SELECT MAIN_TYPE_ID,MAIN_TYPE_NAME
			FROM finance_mst_account_main_type
			WHERE FINANCE_TYPE_ID='$financeType' AND
			STATUS='1'
			ORDER BY MAIN_TYPE_NAME";
	$result = $db->RunQuery($sql);
	$html 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$html.="<option value=\"".$row['MAIN_TYPE_ID']."\">".$row['MAIN_TYPE_NAME']."</option>";
	}
	return $html;
}
function getSubTypeCombo($mainType)
{
	global $db;
	
	$sql = "SELECT SUB_TYPE_ID,SUB_TYPE_NAME
			FROM finance_mst_account_sub_type
			WHERE MAIN_TYPE_ID='$mainType' AND
			STATUS='1'
			ORDER BY SUB_TYPE_NAME ";
	$result = $db->RunQuery($sql);
	$html 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$html.="<option value=\"".$row['SUB_TYPE_ID']."\">".$row['SUB_TYPE_NAME']."</option>";
	}
	return $html;
}

?>
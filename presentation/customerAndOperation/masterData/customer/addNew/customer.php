<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$progrmCode			= 'P0019';
$session_userId		= $_SESSION["userId"];
$obsoleteCurrency = array("BDT","Franc","INR");
 
$cusId				= '';
$sql 				= "SELECT MAX(intId)+1 AS cusId FROM mst_customer order by cusId DESC";
$result 			= $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$cutomerCode 	= 'CU'.($row['cusId'] + 1000);
}
$cusId 				= (!isset($_REQUEST['id'])?'':$_REQUEST['id']);

?>
<script type="application/javascript" >
var cusId = '<?php echo $cusId ?>';
</script>
<head>
<title>Customer Details</title>
</head>
<body onLoad="functionList();">
<form id="frmCustomer" name="frmCustomer" method="post" >
<div align="center">
		<div class="trans_layoutD">
		  <div class="trans_text">Customer Details</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="585" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
              <tr>
                <td height="25" class="normalfnt">&nbsp;</td>
                <td align="left" class="normalfnt">Customer</td>
                <td width="428" colspan="3">
                <select name="cboSearch" class="txtbox" id="cboSearch"  style="width:100%" <?php echo ($form_permision['edit']?'':'disabled="disabled"')?>>
                <option value=""></option>
                 <?php  $sql = "SELECT
								mst_customer.intId,
								mst_customer.strName
								FROM mst_customer
								order by strName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?> 
                </select>
                  </td>
              </tr>
              <tr>
                <td width="1" class="normalfnt">&nbsp;</td>
                <td width="130" class="normalfnt">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td colspan="4" class="normalfnt"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="tableBorder">
                    <span class="normalfnt"><strong>General</strong></span></td>
                    </tr>
                  <tr class="">
                    <td width="23%" class="normalfnt">Customer Code&nbsp;<span class="compulsoryRed">*</span></td>
                    <td width="35%">
                   	<input value="<?php echo $cutomerCode; ?>" name="txtCode" type="text" class="validate[required,maxSize[10]]" id="txtCode" style="width:140px" />
                    </td>
                    <td width="13%" class="normalfnt">Type&nbsp;<span class="compulsoryRed">*</span></td>
                    <td width="29%">
                    <select name="cboType" id="cboType" style="width:158px" class="txtbox validate[required]" >
                       <option value=""></option>
                      <?php  $sql = "SELECT
						intId,
						strName
						FROM mst_typeofmarketer
						WHERE
							intStatus = 1
						order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
        				?>
                    </select></td>
                    </tr>
                  <tr class="">
                    <td height="30" class="normalfnt">Customer Name <span class="compulsoryRed">*</span></td>
                    <td colspan="3">
                    <input name="txtName" class="validate[required,maxSize[50]]" type="text" id="txtName" style="width:100%" maxlength="50"/></td>
                    </tr>
                  <tr class="">
                    <td class="normalfnt">Address</td>
                    <td colspan="3">
                 <textarea name="txtAddress" style="width:100%"  rows="2" id="txtAddress"></textarea></td>
                    </tr>
                  
                  <tr class="">
                    <td class="normalfnt">City</td>
                    <td><input name="txtCity" type="text" id="txtCity" style="width:140px"/></td>
                    <td class="normalfnt">Country <span class="compulsoryRed">*</span></td>
                    <td><select name="cboCountry" id="cboCountry" style="width:158px" class="validate[required]">
                      <option value=""></option>
                      <?php  $sql = "SELECT
									mst_country.intCountryID,
									mst_country.strCountryName
								FROM mst_country
								WHERE
									intStatus = 1
									order by strCountryName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intCountryID']."\">".$row['strCountryName']."</option>";
								}
                   ?>
                    </select></td>
                  </tr>
                <tr class="">
                    <td class="normalfnt">Contact Person</td>
                    <td colspan="3">
                <textarea name="txtContact" style="width:100%; height:20px"   id="txtContact" ></textarea></td>
                    </tr>                  
                    <tr class="">
                    <td class="normalfnt"> Currency <span class="compulsoryRed">*</span></td>
                    <td><select name="cboCurrency" class="validate[required]" id="cboCurrency" style="width:140px">
                      <option value=""></option>
                      <?php  $sql = "SELECT
						intId,
						strCode
						FROM mst_financecurrency
						WHERE
							intStatus = 1
						order by strCode
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result)) {
                            if ($_SESSION["headCompanyId"] == '1') {
                                if (!in_array($row['strCode'], $obsoleteCurrency)) {
                                    echo "<option value=\"" . $row['intId'] . "\">" . $row['strCode'] . "</option>";
                                }
                            } else {
                                echo "<option value=\"" . $row['intId'] . "\">" . $row['strCode'] . "</option>";
                            }
                        }
        				?>
                    </select></td>
                   
                    <td class="normalfnt" >Bundle Tag</td>
                    <td style="width:140px" ><img id="butBundleTag" src="images/add_new.png" width="15" height="15" /></td>
                    
                  </tr>
<tr class="">
  <td colspan="4" class="normalfnt">
    <table width="100%">
      <tr>
        <td width="45%" valign="top"><img id="butBrandName" src="images/add_new.png" width="15" height="15" /><div style="width:250px;height:250px;overflow:scroll" >
          <table id="tblBrands" class="bordered" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
            <thead>
              <tr>
                <th width="8%" height="19" align="center" bgcolor="#FAD163" class="normalfntMid"></th>
                <th width="92%" bgcolor="#FAD163" class="normalfntMid">Brand Name</th>
                </tr>
              </thead>
            <tbody>
              <?php
	 	 		 $sql = "SELECT
				 intId,
				 strName
				 FROM mst_brand
				 WHERE
				 intStatus = '1'
				 ORDER BY strName";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
	  			 ?>
              <tr class="normalfnt">
                <td align="center" bgcolor="#FFFFFF" class="normalfntMid">
                  <input type="checkbox" class="brandname" name="brn<?php echo $row['intId'];?>" id="brn<?php echo $row['intId'];?>" value="<?php echo $row['intId'];?>" />
                  </td>
                <td bgcolor="#FFFFFF"><?php echo $row['strName'];?></td>
                </tr>
              <?php 
        		 } 
       			 ?>
              </tbody>
            </table></div></td>
        <td width="10%">&nbsp;</td>
        <td width="45%" valign="top" ><img id="butLocations" src="images/add_new.png" width="15" height="15" /><div style="width:250px;height:250px;overflow:scroll" ><table id="tblLocations" class="bordered" width="100%" border="0" cellpadding="0" cellspacing="1" >
          <thead>
            <tr>
              <th width="8%" height="19" align="center" bgcolor="#FAD163" class="normalfntMid"></th>
              <th width="92%" bgcolor="#FAD163" class="normalfntMid">Location</th>
              </tr>
            </thead>
          <tbody>
            <?php
	 	 		 $sql = "SELECT
				 intId,
				 strName
				 FROM mst_customer_locations_header
				 WHERE
				 intStatus = '1'
				 ORDER BY strName";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
	  			 ?>
            <tr class="normalfnt">
              <td align="center" bgcolor="#FFFFFF" class="normalfntMid">
                <input type="checkbox" class="locations" name="loc<?php echo $row['intId'];?>" id="loc<?php echo $row['intId'];?>" value="<?php echo $row['intId'];?>" /></td>
              <td bgcolor="#FFFFFF"><?php echo $row['strName'];?></td>
              </tr>
            <?php 
        		 } 
       			 ?>
            </tbody>
          </table></div></td>
        </tr>
      </table>
    </td>
</tr>                  
</table></td>
              </tr>
              <tr>
              	<td>&nbsp;</td>
                <td colspan="5"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="tableBorder">
                    <span class="normalfnt"><strong>Communication</strong></span></td>
                  </tr>
                  <tr>
                    <td width="23%"><span class="normalfnt">Phone No</span></td>
                    <td width="30%">
                    <input name="txtPhone" type="text" id="txtPhone" style="width:140px" /></td>
                    <td width="18%"><span class="normalfnt">Mobile No</span></td>
                    <td width="29%">
                    <input name="txtMobile" type="text" id="txtMobile" style="width:158px" /></td>
                  </tr>
                  <tr>
                    <td><span class="normalfnt">Fax</span></td>
                    <td>
                    <input name="txtFax" type="text" id="txtFax" style="width:140px" /></td>
                    <td><span class="normalfnt">E-Mail</span></td>
                    <td>
              <input name="txtEMail" type="text" id="txtEMail"  style="width:158px" /></td>
                  </tr>
                  <tr>
                    <td><span class="normalfnt">Website</span></td>
                    <td colspan="3">
                    <input name="txtWeb" class="validate[custom[url]]" type="text" id="txtWeb" style="width:100%" /></td>
                    </tr>
                  <tr>
                    <td><span class="normalfnt">Shipment Method  <span class="compulsoryRed">*</span></span></td>
                    <td>
                 	<select name="cboShipmentMethod" id="cboShipmentMethod" style="width:140px"  class="validate[required]" >
                    <option value=""></option>
                      <?php  $sql = "SELECT
						intId,
						strName
						FROM mst_shipmentmethod
						WHERE
							intStatus = 1
						order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
        				?>
                    </select></td>
                      <td class="normalfnt">Dispatched Email Method</td>
                      <td>&nbsp;
                          <select name="emailMethod" id="emailMethod" style="width:140px" >
                              <option value="0"> Location Wise </option>
                              <option value="1">Brand Wise</option>
                              <option value="2">All</option>
                          </select>
                      </td>
                    </tr>
                </table></td>
              </tr>
              <tr>
                <td rowspan="3" class="normalfnt">&nbsp;</td>
                <td colspan="4" class="normalfnt"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="tableBorder">
                    <span class="normalfnt"><strong>Legal Information</strong></span></td>
                    </tr>
                  <tr>
                    <td width="23%" class="normalfnt">VAT Reg No. </td>
                    <td width="30%">
                    <input name="txtVatNo" type="text" id="txtVatNo" style="width:140px" /></td>
                    <td width="18%" class="normalfnt">SVAT No. </td>
                    <td width="29%">
                    <input name="txtSvatNo" type="text" id="txtSvatNo" style="width:158px" /></td>
                    </tr>
                  <tr>
                    <td class="normalfnt">Business Reg No</td>
                    <td>
                    <input name="txtRegNo" type="text" id="txtRegNo" style="width:140px" /></td>
                    </tr>
                  <tr>
                    <td class="normalfnt">Finance Type</td>
                    <td colspan="3" class="normalfnt"><select name="cboFinanceType" id="cboFinanceType" style="width:100%">
                        <option value=""></option>
                        <?php
                                    	$sql = "SELECT FINANCE_TYPE_ID,FINANCE_TYPE_NAME
                                    			FROM finance_mst_account_type
                                    			WHERE STATUS=1
                                    			ORDER BY FINANCE_TYPE_NAME";
                                    	$result = $db->RunQuery($sql);
										while($row=mysqli_fetch_array($result))
										{
											echo "<option value=\"".$row["FINANCE_TYPE_ID"]."\">".$row["FINANCE_TYPE_NAME"]."</option>";
										}
                                    ?>
                      </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Main Type</td>
                    <td colspan="3" class="normalfnt"><select name="cboMainType" id="cboMainType" style="width:100%">
                      </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Sub Type</td>
                    <td colspan="3" class="normalfnt"><select name="cboSubType" id="cboSubType" style="width:100%">
                      </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Chart Of Account</td>
                    <td colspan="3" class="normalfnt"><input name="txtChartOfAccount" class="" type="text" id="txtChartOfAccount" style="width:100%" maxlength="255" /><input  name="txtChartOfAccountDB" class="" type="text" id="txtChartOfAccountDB" style="width:100px;display:none" /></td>
                  </tr>
                  </table></td>
              </tr>
              <tr>
                <td colspan="4" class="normalfnt"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="tableBorder">
                    <span class="normalfnt"><strong>Payments</strong></span></td>
                  </tr>
                  <tr>
                    <td width="23%" class="normalfnt">Payments Terms  <span class="compulsoryRed">*</span></td>
                    <td width="26%">
                   <select name="cboPaymentsTerms" id="cboPaymentsTerms" style="width:140px"   class="validate[required]">
                    <option value=""></option>
                      <?php  $sql = "SELECT
						intId,
						strName
						FROM mst_financepaymentsterms
						WHERE
							intStatus = 0
						order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']." days"."</option>";
						}
        				?>
                    </select></td>
                    <td width="22%" class="normalfnt">Payments Methods  <span class="compulsoryRed">*</span></td>
                    <td width="29%">
                 <select name="cboPaymentsMethods" id="cboPaymentsMethods" style="width:100%"   class="validate[required]" >
                    <option value=""></option>
                      <?php  $sql = "SELECT
						intId,
						strName
						FROM mst_financepaymentsmethods
						WHERE
							intStatus = 1
						order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
        				?>
                    </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Credit Limit</td>
                    <td>
                    <input name="txtCreditLimit" type="text" id="txtCreditLimit" style="width:140px" value="0" /></td>
                    <td class="normalfnt" style="display:none">Ledger Accounts</td>
                    <td style="display:none">
                    <select name="cboChartOfAcc" id="cboChartOfAcc" style="width:1100%" >
                    <option value=""></option>
                 	<?php  $sql = "SELECT
						intId,
						strCode,
						strName
						FROM mst_financechartofaccounts
						WHERE
						intStatus = '1' AND  intFinancialTypeId = '10' AND strType = 'Posting'
						order by strCode
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";
						}
        			?>
                    </select></td>
                  </tr>
                </table></td>
                </tr>
              <tr>
                <td colspan="4" class="normalfnt"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="tableBorder">
                     <span class="normalfnt"><strong>Other</strong></span></td>
                    </tr>
                  <tr>
                    <td width="23%" class="normalfnt">Blocked</td>
                    <td width="27%">
                    <select name="cboBlocked" id="cboBlocked" style="width:140px" >
                      <option>None</option>
                      <option>Delivery</option>
                      <option>Invoice</option>
                      <option>All</option>
                    </select></td>
                    <td width="21%">&nbsp;</td>
                    <td width="29%">&nbsp;</td>
                    </tr>
                  <tr class="" style="display:none">
                    <td class="normalfnt">Rank</td>
                    <td colspan="3">
 <input name="txtRank" type="text" id="txtRank" style="width:20px" class="validate[custom[integer],min[0],max[10]]"/>
                      <span class="normalfnt" style="color:#CCC;font-size:9px">1 to 10. 1=Super ,10=Blocked</span></td>
                    </tr>
                   </table></td>
              </tr>
              </table></td>
            </tr>
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center">
                <a class="button white medium" alt="New" name="butNew" width="92" height="24" id="butNew" tabindex="28">New</a>
                <a class="button white medium" <?php echo $form_permision['add']?'':'style="display:none"'?>  alt="Save" name="butSave"width="92" height="24"   id="butSave" tabindex="29">Save</a>
                <a class="button white medium"  style="display:none"   alt="Approve" name="butConfirm"width="92" height="24"   id="butConfirm" tabindex="30">Approve</a>
                <a class="button white medium"  style="display:none"   alt="Report" name="butReport"width="92" height="24"   id="butReport" tabindex="31">Report</a>
                 <a class="button white medium" href="main.php"  alt="Close" name="butClose" width="92" height="24" border="0"   id="butClose" tabindex="32">Close</a>
               </td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
  
  <!-- this is customer brand popup window -->
	<div style="position: absolute;display:none;z-index:100"  id="popupContact2">
    <iframe onload="loadBrands();"   id="iframeMain2" name="iframeMain2" src="header_db.php?requestType=URLGetHeader&q=345&iframe=1&clearCash=1" style="width:800px;height:400px;border:0;overflow:hidden">
    </iframe>
    </div>
  
      	<!-- this is customer location popup window -->
	<div style="position: absolute;display:none;z-index:100"  id="popupContact1">
    <iframe onload="loadLocations();"   id="iframeMain1" name="iframeMain1" src="header_db.php?requestType=URLGetHeader&q=344&iframe=1&clearCash=1" style="width:800px;height:400px;border:0;overflow:hidden">
    </iframe>
    </div>
    
	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
    
</form>
</body>
</html>


<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include_once "{$backwardseperator}dataAccess/Connector.php";
	include_once  ("../../../../../class/cls_commonFunctions_get.php");
	include_once  ("../../../../../class/cls_commonErrorHandeling_get.php");

	$obj_common			= new cls_commonFunctions_get($db);
	$obj_commonErr		= new cls_commonErrorHandeling_get($db);
	$progrmCode			='P0019';
 	$session_userId		= $_SESSION["userId"];

	$response = array('type'=>'', 'msg'=>'');

 	$requestType 		= $_REQUEST['requestType'];
	$id 				= $_REQUEST['cboSearch'];
	$code				= trim($_REQUEST['txtCode']);
	$name				= trim($_REQUEST['txtName']);
	$type				= ($_REQUEST['cboType']==''?'NULL':$_REQUEST['cboType']);
	$address			= $_REQUEST['txtAddress'];
	$contact			= $_REQUEST['txtContact'];
	$city				= trim($_REQUEST['txtCity']);
	$country			= ($_REQUEST['cboCountry']==''?'NULL':$_REQUEST['cboCountry']);
	$currency			= ($_REQUEST['cboCurrency']==''?'NULL':$_REQUEST['cboCurrency']);
	$phone				= $_REQUEST['txtPhone'];
	$mobile				= $_REQUEST['txtMobile'];
	$fax				= $_REQUEST['txtFax'];
	$email				= trim($_REQUEST['txtEMail']);
	$web				= $_REQUEST['txtWeb'];
	$shipment 			= ($_REQUEST['cboShipmentMethod']==''?'NULL':$_REQUEST['cboShipmentMethod']);
	$vatNo				= $_REQUEST['txtVatNo'];
	$sVatNo				= $_REQUEST['txtSvatNo'];
	$regNo				= $_REQUEST['txtRegNo'];
    $BundleTag			= $_REQUEST['hello'];
    $emailMethod		= $_REQUEST['emailMethod'];
        
	$invoType			= $_REQUEST['cboInvoiceType'];
	$subType			= ($_REQUEST['cboSubType']==''?'NULL':$_REQUEST['cboSubType']);
	$chartOfAccName		= $_REQUEST['txtChartOfAccount'];
	$paymentsterms 		= ($_REQUEST['cboPaymentsTerms']==''?'NULL':$_REQUEST['cboPaymentsTerms']);
	$paymentsmethods 	= ($_REQUEST['cboPaymentsMethods']==''?'NULL':$_REQUEST['cboPaymentsMethods']);
	$creditlimit 		= val($_REQUEST['txtCreditLimit']);
	$chartofaccount		= ($_REQUEST['cboChartOfAcc']==''?'NULL':$_REQUEST['cboChartOfAcc']);
	$blocked 			= $_REQUEST['cboBlocked'];
	$rank 				= val($_REQUEST['txtRank']);
  	$brands 			= json_decode($_REQUEST['brand'], true);
	$locations 			= json_decode($_REQUEST['location'], true);

    $COMMERCIAL_INVOICE = 1;
    $TAX_INVOICE = 2;
    $SVAT_INVOICE = 3;
    $invoType = 1;

	$approve_levels		= GetApproveLevel1('P0019');
	$intStatus			= $approve_levels+1;
	if ($sVatNo != ""){
        $invoType = $SVAT_INVOICE;
    } else if ($vatNo != ""){
        $invoType = $TAX_INVOICE;
    }

	if($requestType=='add')
	{
  		$header_arr			= headerArray($id);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_save($header_arr['status'],$header_arr['levels'],$session_userId,$progrmCode,'RunQuery');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];
			$response['type'] 	=	'fail';
			echo json_encode($response);
			return;
		}

		$sql	= "INSERT INTO `mst_customer`(`strCode`,`strName`,`intTypeId`,`strAddress`,`strContactPerson`,`strCity`,`intCountryId`,`intCurrencyId`,`strPhoneNo`,`strMobileNo`,`strFaxNo`,`strEmail`,`strWebSite`,`intShipmentId`,`strVatNo`,`strSVatNo`,`strRegistrationNo`,`strInvoiceType`,`intPaymentsTermsId`,`intPaymentsMethodsId`,`intCreditLimit`,`intChartOfAccountId`,`strBlocked`,`intRank`,`intStatus`,`intApproveLevel`,`intCreator`,`dtmCreateDate`,SUB_TYPE,CHART_OF_ACCOUNT) 
				VALUES ('$code','$name',$type,'$address','$contact','$city',$country,$currency,'$phone','$mobile','$fax','$email','$web',$shipment,'$vatNo','$sVatNo','$regNo','$invoType',$paymentsterms,$paymentsmethods,'$creditlimit',$chartofaccount,'$blocked','$rank','$intStatus','$approve_levels','$userId',now(),$subType,'$chartOfAccName')";
 		$cusId	= $db->autoInsertNo($sql);

 		if(count($brands) != 0)
		{
			foreach($brands as $brand)
			{
				$brnId	= $brand['brandId'];
				$sql	= "INSERT INTO `mst_customer_brand`
							(`intBrandId`,`intCustomerId`,`intCreator`,dtmCreateDate) 
							VALUES 
							('$brnId','$cusId','$userId',now())";
				$result = $db->RunQuery($sql);
			}
		}
		if(count($locations) != 0)
		{
			foreach($locations as $locat)
			{   // echo $cusId;
				$locId 	= $locat['locationId'];

				$sql 	= "INSERT INTO `mst_customer_locations`
						(`intLocationId`,`intCustomerId`,`intCreator`,dtmCreateDate) 
						VALUES 
						('$locId','$cusId','$userId',now())";
				$result = $db->RunQuery($sql);

                                $sql_bundleTag =
                                       "SELECT
                                        CONCAT(p.sub_cus_name,' ',p.sub_loc_name) as bundleTag
                                        FROM
                                        (

                                        SELECT
                                                SUBSTR(t.cus_Name, 1, 7) AS sub_cus_name,
                                                SUBSTR(t.cus_loction, 1, 9) AS sub_loc_name,
                                                t.intLocationId,
                                                t.cus_id
                                        FROM
                                                (
                                                        SELECT
                                        mst_customer.intId AS cus_id,
                                        mst_customer.strName AS cus_Name,
                                        mst_customer_locations.intLocationId,
                                        mst_customer_locations_header.strName AS cus_loction
                                        FROM
                                                mst_customer_locations
                                        INNER JOIN mst_customer_locations_header ON mst_customer_locations.intLocationId = mst_customer_locations_header.intId
                                        INNER JOIN mst_customer ON mst_customer.intId = mst_customer_locations.intCustomerId
                                        WHERE mst_customer.intId = $cusId
                                        AND mst_customer_locations.intLocationId = $locId
                                ) AS t ) as p

                                        ORDER BY p.intLocationId";
                                       // echo $sql_bundleTag;
                                        $result__bundleTag		=   $db->RunQuery($sql_bundleTag);
                                        $row__bundleTag                 =   mysqli_fetch_array($result__bundleTag);
                                        $bundleTag                      =   $row__bundleTag ['bundleTag'];

                                        $sql_update = "update mst_customer_locations
                                                       SET mst_customer_locations.BundleTag = '$bundleTag'  
                                                       WHERE
                                                       mst_customer_locations.intCustomerId = $cusId AND
                                                       mst_customer_locations.intLocationId = $locId";
                                        //echo $sql_update;
                                        $db->RunQuery($sql_update);
			}
		}

		/////////////////////// get max no ////////////////////
		$sql		= "SELECT MAX(intId)+1 AS cusId FROM mst_customer order by cusId DESC";
		$result		= $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$cutomerCode = 'CU'.($row['cusId'] + 1000);
		}
		/////////////////////////////////////////////////////////
		if($rollBack==1){
			$response['msg'] 		= $msg;
			$response['type'] 		= 'fail';
			$db->rollback();
		}
		else if($result || $cusId){
			$response['type'] 		= 'pass';
			$response['nxtNo'] 		= $cutomerCode;
			$response['msg'] 		= 'Saved successfully.';
			UpdateApprovebyStatus($cusId);
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
/*		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $resp['msg'];
 		}
*/ 	}
	/////////// customer update part /////////////////////
	else if($requestType=='edit')
	{
 		$header_arr		= headerArray($id);
              
 		$permision_arr	= $obj_commonErr->get_permision_withApproval_save($header_arr['status'],$header_arr['levels'],$session_userId,$progrmCode,'RunQuery');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];
			$response['type'] 	=	'fail';
			echo json_encode($response);
			return;
		}

		$sql 			= "DELETE FROM `mst_customer_brand` WHERE (`intCustomerId`='$id')";
		$db->RunQuery($sql);
		$chkLocations = '';
		foreach($locations as $locDel)
		{
			$chkLocations.= $locDel['locationId'].',';

		}

		$chkLocations = substr($chkLocations,0,(strlen($chkLocations)-1));

		//comment by roshan (2013-08-14) we are going to allocate location part set new form
		//$sql = "DELETE FROM `mst_customer_locations` WHERE intCustomerId='$id' and intLocationId NOT IN ($chkLocations) ";
		//$db->RunQuery($sql);

		foreach($brands as $brand)
		{
			$brnId	= $brand['brandId'];
			$sql	= "INSERT INTO `mst_customer_brand`
					(`intBrandId`,`intCustomerId`,`intCreator`,dtmCreateDate) 
					VALUES 
					('$brnId','$id','$userId',now())";
			$db->RunQuery($sql);
		}

		foreach($locations as $locat)
		{
			$locId		= $locat['locationId'];

			$sqlChk		= "SELECT * FROM mst_customer_locations WHERE intLocationId='$locId' AND intCustomerId='$id' ";
                        //echo $sqlChk;
			$resultChk	= $db->RunQuery($sqlChk);


			if(mysqli_num_rows($resultChk)==0)

			{
				$sql	= "INSERT INTO `mst_customer_locations`
							(`intLocationId`,`intCustomerId`,`intCreator`,dtmCreateDate) 
							VALUES 
							('$locId','$id','$userId',now())";
                                //echo $sql;
				$db->RunQuery($sql);

                                $sql_bundleTag =

                                       "SELECT
                                        CONCAT(p.sub_cus_name,' ',p.sub_loc_name) as bundleTag
                                        FROM
                                        (

                                        SELECT
                                                SUBSTR(t.cus_Name, 1, 7) AS sub_cus_name,
                                                SUBSTR(t.cus_loction, 1, 9) AS sub_loc_name,
                                                t.intLocationId,
                                                t.cus_id
                                        FROM
                                                (
                                                        SELECT
                                        mst_customer.intId AS cus_id,
                                        mst_customer.strName AS cus_Name,
                                        mst_customer_locations.intLocationId,
                                        mst_customer_locations_header.strName AS cus_loction
                                        FROM
                                                mst_customer_locations
                                        INNER JOIN mst_customer_locations_header ON mst_customer_locations.intLocationId = mst_customer_locations_header.intId
                                        INNER JOIN mst_customer ON mst_customer.intId = mst_customer_locations.intCustomerId
                                        WHERE mst_customer.intId = $id
                                        AND mst_customer_locations.intLocationId = $locId
                                ) AS t ) as p

                                        ORDER BY p.intLocationId";
                                      // echo $sql_bundleTag;
                                        $result__bundleTag		=   $db->RunQuery($sql_bundleTag);
                                        $row__bundleTag                 =   mysqli_fetch_array($result__bundleTag);
                                        $bundleTag                      =   $row__bundleTag ['bundleTag'];

                                        $sql_update = "update mst_customer_locations
                                                       SET mst_customer_locations.BundleTag = '$bundleTag'  
                                                       WHERE
                                                       mst_customer_locations.intCustomerId = $id AND
                                                       mst_customer_locations.intLocationId = $bundleTagLoc";
                                       //echo $sql_update;
                                        $db->RunQuery($sql_update);


			}
		}


		 $sql = "UPDATE `mst_customer` SET 	strCode					='$code',
											strName					= '$name',
											intTypeId				= $type,
											strAddress				= '$address',
											strContactPerson		= '$contact',
											strCity					= '$city',
											intCountryId			= $country,
											intCurrencyId			= $currency,
											strPhoneNo				= '$phone',
											strMobileNo				= '$mobile',
											strFaxNo				= '$fax',
											strEmail				= '$email',
											strWebSite				= '$web',
											intShipmentId			= $shipment,
											strVatNo				= '$vatNo',
											strSVatNo				= '$sVatNo',
											strRegistrationNo		= '$regNo',
											strInvoiceType			= '$invoType',
											intPaymentsTermsId		= $paymentsterms,
											intPaymentsMethodsId	= $paymentsmethods,
											intCreditLimit			= '$creditlimit',
											intChartOfAccountId		= $chartofaccount,
											strBlocked				= '$blocked',
											intRank					= '$rank',
											intStatus				= '$intStatus',
											intApproveLevel			= '$approve_levels',
											intModifyer				= '$userId',
											SUB_TYPE				= $subType,
											CHART_OF_ACCOUNT		= '$chartOfAccName' 
				WHERE (`intId`='$id')";
		$result = $db->RunQuery($sql);

 		/////////////////////// get max no ////////////////////
		$sql 	= "SELECT MAX(intId)+1 AS cusId FROM mst_customer order by cusId DESC";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$cutomerCode = 'CU'.($row['cusId'] + 1000);
		}
		/////////////////////////////////////////////////////////

 		if(isset($rollBack) && $rollBack==1){
			$response['msg'] 		= $msg;
			$response['type'] 		= 'fail';
			$db->rollback();
		}
		else if(($result)){
			$response['type'] 		= 'pass';
			$response['nxtNo'] 		= $cutomerCode;
			$response['msg'] 		= 'Updated successfully.';
			UpdateApprovebyStatus($id);
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	else if($requestType=='approve')
	{
		$rollBack=0;
 		$customerId			= $_REQUEST["customerId"];
		$chartOfAccName		= $_REQUEST["chartOfAccName"];
 		$subType			= ($_REQUEST['subType']==''?'NULL':$_REQUEST['subType']);
		$savedStatus 		= true;

 		$header_arr			= headerArray($customerId);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_arr['status'],$header_arr['levels'],$session_userId,$progrmCode,'RunQuery');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];
			$response['type'] 	=	'fail';
			echo json_encode($response);
			return;
		}
 		else if(($header_arr['status']==2) && (($chartOfAccName=='') || ($subType==NULL))){
			$rollBack			=	1;
			$response['msg'] 	=	'Please select sub type and Chart of account name';
			$response['type'] 	=	'fail';
			echo json_encode($response);
			return;
		}

		$db->begin();

		$approve_level	= GetMaxApproveLevel($customerId);
 		$sql 			= "UPDATE mst_customer SET intStatus = intStatus - 1,SUB_TYPE=$subType ,CHART_OF_ACCOUNT='$chartOfAccName' WHERE intId = '$customerId' ";
		$result = $db->RunQuery2($sql);
		if(!$result){
			$savedStatus 	= false;
			$sqlE=$sql;
		}

 		if($header_arr['status']==2 && !CheckIsAvailable($customerId))
		{
 			$maxCode=getMaxCode($subType);
			$sql 	= "INSERT INTO finance_mst_chartofaccount 
					(CHART_OF_ACCOUNT_CODE, 
					CHART_OF_ACCOUNT_NAME, 
					SUB_TYPE_ID, 
					CATEGORY_TYPE, 
					CATEGORY_ID,
					STATUS,
					CREATED_BY,
					CREATED_DATE)
					VALUES
					('$maxCode', 
					'$chartOfAccName', 
					'$subType', 
					'C', 
					'$customerId', 
					'1', 
					'$userId', 
					now());";
				$result = $db->RunQuery2($sql);
				$accountId=$db->insertId;

			if(!$result){
				$savedStatus 	= false;
				$sqlE			=$sql;
			}

			$sql 	= "SELECT mst_companies.intId FROM `mst_companies` WHERE mst_companies.intStatus = 1";
			$result = $db->RunQuery2($sql);
			while($row = mysqli_fetch_array($result))
			{
				$companyId	=	$row['intId'];
				$sqlI 		=	"INSERT INTO finance_mst_chartofaccount_company 
								(COMPANY_ID, 
								CHART_OF_ACCOUNT_ID)
								VALUES
								('$companyId', 
								'$accountId');";
				$resultI 	=	$db->RunQuery2($sqlI);
				if(!$resultI){
					$savedStatus 	=	false;
					$sqlE			=	$sqlI;
				}
			}
 		}
		if($header_arr['status']==2 && CheckIsAvailable($customerId))
		{
			$maxCode=getMaxCode($subType);

			$sql = "UPDATE finance_mst_chartofaccount
					SET 
					CHART_OF_ACCOUNT_CODE = '$maxCode',
					CHART_OF_ACCOUNT_NAME = '$chartOfAccName',
					SUB_TYPE_ID = '$subType',
					LAST_MODIFY_BY = '$userId',
					LAST_MODIFY_DATE = now()
					WHERE
					CATEGORY_ID = '$customerId' AND 
					CATEGORY_TYPE = 'C'
					 ";

			$result = $db->RunQuery2($sql);
			if(!$result)
			{
				$msg			= $db->errormsg;
				$savedStatus 	= false;
				$sqlE			= $sql;
			}

		}

	 	$sql = "INSERT INTO mst_customer_approvedby 
				(intCustomerId, 
				intApproveLevelNo, 
				intApproveUser, 
				dtApprovedDate, 
				intStatus)
				VALUES
				('$customerId', 
				'$approve_level', 
				'$userId', 
				now(), 
				'0');";
		$result = $db->RunQuery2($sql);
		if(!$result){
			$savedStatus 	= false;
			$sqlE			= $sql;
		}


		if($rollBack==1){
			$response['msg']	= $msg;
			$response['type']	= 'fail';
			$response['sql']	= $sqlE;
			$db->rollback();
		}
		else if($savedStatus){
			$response['msg']	= "Approved successfully.";
			$response['type']	= 'pass';
			if($_SESSION['CompanyID'] == 2 && isFinalApprovalRaised($customerId)) {
			    sendDetailsToFinanceModule($customerId);
            }
			$db->commit();
		}
		else{
			$response['msg']	= "Approved failed.";
			$response['type']	= 'fail';
			$response['sql']	= $sqlE;
			$db->rollback();
		}
	}
	else if($requestType=='revise')
	{
		$customerId			= $_REQUEST["customerId"];
		$savedStatus 		= true;

 		$header_arr			= headerArray($customerId);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_revise($header_arr['status'],$header_arr['levels'],$session_userId,$progrmCode,'RunQuery');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];
			$response['type'] 	=	'fail';
			echo json_encode($response);
			return;
		}

		$db->begin();

		$sql = "UPDATE mst_customer SET intStatus = -1 WHERE intId = '$customerId' ";
		$result = $db->RunQuery2($sql);
		if(!$result)
			$savedStatus 	= false;

		$sql = "INSERT INTO mst_customer_approvedby 
				(intCustomerId, 
				intApproveLevelNo, 
				intApproveUser, 
				dtApprovedDate, 
				intStatus)
				VALUES
				('$customerId', 
				'-1', 
				'$userId', 
				now(), 
				'0');";
		$result = $db->RunQuery2($sql);
		if(!$result)
			$savedStatus 	= false;

		if($savedStatus){
			$response['msg']	= "Revised successfully.";
			$response['type']	= 'pass';
			$db->commit();
		}
		else{
			$response['msg']	= "Revision failed.";
			$response['type']	= 'fail';
			$db->rollback();
		}
	}
	else if($requestType=='cancel')
	{
		$customerId		= $_REQUEST["customerId"];
		$savedStatus 	= true;

 		$header_arr			= headerArray($customerId);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_cancel($header_arr['status'],$header_arr['levels'],$session_userId,$progrmCode,'RunQuery');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];
			$response['type'] 	=	'fail';
			echo json_encode($response);
			return;
		}

		$db->begin();

		$sql = "UPDATE mst_customer SET intStatus = -2 WHERE intId = '$customerId' ";
		$result = $db->RunQuery2($sql);
		if(!$result)
			$savedStatus 	= false;

		$sql = "INSERT INTO mst_customer_approvedby 
				(intCustomerId, 
				intApproveLevelNo, 
				intApproveUser, 
				dtApprovedDate, 
				intStatus)
				VALUES
				('$customerId', 
				'-2', 
				'$userId', 
				now(), 
				'0');";
		$result = $db->RunQuery2($sql);
		if(!$result)
			$savedStatus 	= false;

		if($savedStatus){
			$response['msg']	= "Cancelled successfully.";
			$response['type']	= 'pass';
			$db->commit();
		}
		else{
			$response['msg']	= "Cancellation failed.";
			$response['type']	= 'fail';
			$db->rollback();
		}
	}
	else if($requestType=='reject')
	{
		$customerId		= $_REQUEST["customerId"];
		$savedStatus 	= true;

 		$header_arr			= headerArray($customerId);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_reject($header_arr['status'],$header_arr['levels'],$session_userId,$progrmCode,'RunQuery');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];
			$response['type'] 	=	'fail';
			echo json_encode($response);
			return;
		}


		$db->begin();

		$sql 	= "UPDATE mst_customer SET intStatus = 0 WHERE intId = '$customerId' ";
		$result = $db->RunQuery2($sql);
		if(!$result)
			$savedStatus 	= false;

		$sql = "INSERT INTO mst_customer_approvedby 
				(intCustomerId, 
				intApproveLevelNo, 
				intApproveUser, 
				dtApprovedDate, 
				intStatus)
				VALUES
				('$customerId', 
				'0', 
				'$userId', 
				now(), 
				'0');";
		$result = $db->RunQuery2($sql);
		if(!$result)
			$savedStatus 	= false;

		if($savedStatus){
			$response['msg']	= "Rejected successfully.";
			$response['type']	= 'pass';
			$db->commit();
		}
		else{
			$response['msg']	= "Rejection failed.";
			$response['type']	= 'fail';
			$db->rollback();
		}
              
	}

        
        else if($requestType=='saveBundletag')
        { 
           
            $cusLocation        = $_REQUEST["cusLocation"];
            $bundleTag		= $_REQUEST["bundleTag"];
            $cus		= $_REQUEST["cus"];
            
            $sql = "update mst_customer_locations
                    SET mst_customer_locations.BundleTag = '$bundleTag'  
                    WHERE
                    mst_customer_locations.intCustomerId =  $cus AND
                    mst_customer_locations.intLocationId = $cusLocation";
            //echo  $sql;
             $db->begin(); 
             $result = $db->RunQuery($sql);
            
        }
	

	/////////// customer delete part /////////////////////
	/*else if($requestType=='delete')
	{
 		$header_arr			= headerArray($id);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_delete($header_arr['status'],$header_arr['levels'],$session_userId,$progrmCode,'RunQuery');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];
			$response['type'] 	=	'fail';
			echo json_encode($response);
			return;
		}

		$sql = "DELETE FROM `mst_customer` WHERE (`intId`='$id')  ";
		$result = $db->RunQuery($sql);
		if(($result)){

			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}*/

	echo json_encode($response);

function GetMaxApproveLevel($customerId)
{
	global $db;
 	$appLevel	= 0;
 	$sql 		= "SELECT COALESCE(MAX(intApproveLevelNo),0) AS  MAX_NO
				FROM mst_customer_approvedby 
				WHERE intCustomerId = $customerId AND intStatus = 0";
	$result 	= $db->RunQuery2($sql);
	$row 		= mysqli_fetch_array($result);
	return $row['MAX_NO']+1;
}

function CheckStatus($customerId)
{
	global $db;
	$status	= 0;
	$sql 	= "SELECT COALESCE(intStatus,0) AS STATUS FROM mst_customer WHERE intId = $customerId";
	$result = $db->RunQuery($sql);
	while($row 	= mysqli_fetch_array($result))
	{
		$status	=  $row['STATUS'];
	}
	return $status;
}

function headerArray($customerId)
{
	global $db;
	$status	= 0;
	$sql 	= "SELECT COALESCE(intStatus,0)  AS status, intApproveLevel as levels FROM mst_customer WHERE intId = $customerId";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row;
}

function GetApproveLevel1($proCode)
{
	global $db;
	$approvalLevel	= 0;
	$sql 			= "SELECT intApprovalLevel FROM sys_approvelevels WHERE strCode = '$proCode'";
	$result 		= $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$approvalLevel	=  $row['intApprovalLevel'];
	}
	return $approvalLevel;
}

function UpdateApprovebyStatus($customer_id)
{
	global $db;
	$max_status	= GetMaxStatus($customer_id);
	$sql 		= "UPDATE mst_customer_approvedby 
					SET
						intStatus = $max_status	
					WHERE
						intCustomerId = '$customer_id' AND intStatus = '0' ;";
	$result 	= $db->RunQuery($sql);
 }

function GetMaxStatus($customer_id)
{
	global $db;

	$sql 	= "SELECT MAX(intStatus) MAX_STATUS FROM mst_customer_approvedby WHERE intCustomerId = '$customer_id'";
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$max_status	=  $row['MAX_STATUS']+1;
	}
	return $max_status;
}

function getMaxCode($subType)
{
	global $db;

	//$sql 	= "SELECT LPAD((MAX(IFNULL(CHART_OF_ACCOUNT_CODE,0))+1),3,0) MAX_CODE FROM finance_mst_chartofaccount WHERE SUB_TYPE_ID = '$subType'";
	$sql 	= "SELECT IFNULL(IF (CHAR_LENGTH(MAX(CAST(CHART_OF_ACCOUNT_CODE AS UNSIGNED))) < 3,LPAD(MAX(CAST(CHART_OF_ACCOUNT_CODE AS UNSIGNED)) + 1,3,0),MAX(CAST(CHART_OF_ACCOUNT_CODE AS UNSIGNED)) + 1),LPAD(1, 3, 0)) AS MAX_CODE
                FROM
                    finance_mst_chartofaccount
                WHERE
                    SUB_TYPE_ID = '$subType'";
	$result = $db->RunQuery2($sql);
	while($row = mysqli_fetch_array($result))
	{
		$max_status	=  $row['MAX_CODE']+1;
	}
	return $max_status;
}

function CheckIsAvailable($customerId)
{
	global $db;
	$sql = "select count(*) AS COUNT FROM finance_mst_chartofaccount where CATEGORY_ID = '$customerId' AND CATEGORY_TYPE = 'C'";
	$result = $db->RunQuery2($sql);
	$row	= mysqli_fetch_array($result);
	if($row["COUNT"]>0)
		return true;
	else
		return false;
}

function sendDetailsToFinanceModule($customerId){
    $configs = include('../../../../../config/zillionConfig.php');
    $url = $configs['URL'].'IntCustomer';
    $customer_details = getCustomerDetails($customerId);
    $data_json = createJsonObject($customer_details);
    $data = json_decode($data_json,true);
    $cus_no = $data['No'];
    $auth_header = "Authorization: ".$configs["TYPE"].' '.base64_encode($configs['AUTH_USERNAME'].':'.$configs['AUTH_PASSWORD']);
    $header_arr = array("Content-Type: application/json", $auth_header,"If-Match: *");
    if(!hasAlreadySent($url,$cus_no,$header_arr)) {
        $header_arr = array("Content-Type: application/json", $auth_header);
        $responseArray = callAPI($url, 'POST', $header_arr, $data_json);
        $updating = 0;
    }
    else{
        $updateUrl = $url."(No='$cus_no')";
        $responseArray = callAPI($updateUrl,"PUT",$header_arr,$data_json);
        $updating = 1;
    }
    $successStatus = 0;
    $response = $responseArray['response'];

    $httpcode = $responseArray['code'];
    if($httpcode == '201' || $httpcode == '204'){
        $successStatus = 1; //delivered
    }

    insertToTransactionTable($customer_details,$customerId,$successStatus,$updating);

}

function getCustomerDetails($customerId){
    global $db;
    $sql = "SELECT 
			CU.strCode								AS CUSTOMER_CODE,
			CU.strName								AS CUSTOMER_NAME,
			CU.strAddress                           AS ADDRESS,
			CU.strCity                              AS CITY,
			CURR.intId								AS CURRENCY_ID,
			CURR.strCode							AS CURRENCY_CODE,
			IFNULL(
			CU.strPhoneNo,CU.strMobileNo
			) AS PHONE_NO,
			CU.strEmail								   AS EMAIL,
			CU.strVatNo                               AS VAT_NO,
			CU.strSVatNo                               AS SVAT_NO			                  
            FROM
                mst_customer CU
            INNER JOIN mst_financecurrency CURR	ON CURR.intId = CU.intCurrencyId
            WHERE
                CU.intId = '$customerId' AND CU.intStatus = '1'";
    $result = $db->RunQuery2($sql);
    return mysqli_fetch_array($result);

}

function createJsonObject($row){
    $vatCode = (is_numeric($row['SVAT_NO']) && $row['SVAT_NO'] != '0')?"SVAT":"VAT";
    $currency = ($row['CURRENCY_CODE'] == 'EURO')?"Eur":($row['CURRENCY_CODE'] == 'LKR'?"":$row['CURRENCY_CODE']);
    $address = preg_replace("/[\n\r]/",",",$row['ADDRESS']);
    $splitAddress = explode(',',$address);
    $address1 = "";
    $address2 = "";
    for($i=0;$i<count($splitAddress)/2;$i++){
        $address1 = $address1.','.$splitAddress[$i];
    }
    for($i=count($splitAddress)/2;$i<count($splitAddress);$i++){
        $address2 = $address2.','.$splitAddress[$i];
    }
    $address1 = ltrim($address1, ',');
    $address2 = ltrim($address2, ',');
    $data = array('No' => $row['CUSTOMER_CODE'],
        'Name' => $row['CUSTOMER_NAME'],
        'Address' => $address1,
        'Address_2'=>$address2,
        'City'=> $row['CITY'],
        'E_Mail'=>$row['EMAIL'],
        'Phone_No'=>$row['PHONE_NO'],
        'VAT_Bus_Posting_Group'=>$vatCode,
        'VAT_Registration_No'=>$row['VAT_NO'],
        'SVAT_No'=>$row['SVAT_NO'],
        'Currency_Code'=>$currency,
        'Customer_Posting_Group'=>"DEBTORS"
    );
    return json_encode($data);
}

function isFinalApprovalRaised($customerId){
    global $db;
    $sql 		= "SELECT intApproveLevelNo
				FROM mst_customer_approvedby 
				WHERE intCustomerId = $customerId AND intApproveLevelNo = '2' AND intStatus = 0";
    $result 	= $db->RunQuery2($sql);
    return (mysqli_num_rows($result)>0);
}

function insertToTransactionTable($row, $customerId, $deliveryStatus,$updating)
{
        global $db;
       $vatCode = (is_numeric($row['SVAT_NO']) && $row['SVAT_NO'] != '0')?"SVAT":"VAT";
        // empty for LKR
        $currency = ($row['CURRENCY_CODE'] == 'EURO')?"Eur":($row['CURRENCY_CODE'] == 'LKR'?"":$row['CURRENCY_CODE']);
        $address = preg_replace("/[\n\r]/", ",", $row['ADDRESS']);
        $sql = "INSERT INTO `trn_financemodule_customer` (
                            `customerId`,
                            `strCode`,
                            `strName`,
                            `strAddress`,
                            `strCity`,
                            `strEmail`,
                            `strPhoneNo`,
                            `vatBustGroup`,
                            `strVatNo`,
                            `strSVatNo`,
                            `currencyCode`,
                            `cusPostingGroup`,
                            `deliveryDate`,
                            `deliveryStatus`,
                            `isUpdating`
                        )
                        VALUES
                            ('$customerId','".$row['CUSTOMER_CODE']."','".$row['CUSTOMER_NAME']."','$address','".$row['CUSTOMER_CITY']."','".$row['EMAIL']."','".$row['PHONE_NO']."','$vatCode','".$row['VAT_NO']."','".$row['SVAT_NO']."', '$currency','DEBTORS', NOW(),'$deliveryStatus','$updating')";


    $result = $db->RunQuery2($sql);

}

function hasAlreadySent($url,$customerCode,$headerArray){
    $getUrl = $url."(No='$customerCode')";
    $responseArray = callAPI($getUrl,'GET',$headerArray,null);
    $xml = simplexml_load_string($responseArray['response']);
    return ($xml->count())>0;
}

function callAPI($url,$method,$headerArray,$data){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);
    if($method == 'POST'){
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    else if($method == 'PUT'){
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $responseArray = array();
    $response = curl_exec($ch);
    if(!$response){
        return;
        //die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    }
    $responseArray['response'] = $response;
    $responseArray['code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    return $responseArray;
}
?>
var basepath	= 'presentation/customerAndOperation/masterData/printMode/addNew/';
function functionList()
{
	if(prnCode!='')
	{
		$('#frmPrintMode #txtCode').val('PM'+prnCode);
	}
}		
		
$(document).ready(function() {
	getNextCode();
	$("#frmPrintMode").validationEngine();
	$('#frmPrintMode #txtCode').focus();
	$('#frmPrintMode #butSave').die('click').live('click',save);
	$('#frmPrintMode #cboSearch').die('change').live('change',searchData);
	$('#frmPrintMode #butNew').die('click').live('click',clearForm);
	$('#frmPrintMode #butDelete').die('click').live('click',deleteData);
});

function getNextCode()
{
	if($('#frmPrintMode #txtCode').val()!='')
		return;
		
	var url 	= "presentation/customerAndOperation/masterData/printMode/addNew/printMode-db-get.php";
	var data	= "requestType=URLgetNextCode";

	$.ajax({
		url:url,
		data:data,
		dataType: "json",  
		type:'post',
		async:false,			
		success:function(json){
			$('#frmPrintMode #txtCode').val('PM'+json.NextId);
		}
	});	
}

function save()
{
	if(!$('#frmPrintMode').validationEngine('validate'))   
    {
		var t = setTimeout("alertx('#frmPrintMode')",2000);
		return; 
	}
	
	showWaiting();

	if($("#frmPrintMode #cboSearch").val()=='')
		requestType = 'add';
	else
		requestType = 'edit';
	
	var url = basepath+"printMode-db-set.php";
	var obj = $.ajax({
		url:url,
		dataType: "json",  
		type:'post',
		data:$("#frmPrintMode").serialize()+'&requestType='+requestType,
		async:false,		
		success:function(json){
				$('#frmPrintMode #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				if(json.type=='pass')
				{
					var t = setTimeout("alertx('#frmPrintMode #butSave')",2000);
					hideWaiting();
					clearForm();
					return;
				}
				hideWaiting();
				var t = setTimeout("alertx('#frmPrintMode #butSave')",44000);
			},
		error:function(xhr,status){				
				$('#frmPrintMode #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				var t=setTimeout("alertx('#frmPrintMode #butSave')",4000);
				hideWaiting();
			}		
	});
}

function searchData()
{	
	$('#frmPrintMode').validationEngine('hide');	
	
	if($('#frmPrintMode #cboSearch').val()=='')
	{
		clearForm();
		return;	
	}
	
	var url = basepath+"printMode-db-get.php";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		type:'post',
		data:'requestType=loadDetails&id='+$(this).val(),
		async:false,
		success:function(json){
			$('#frmPrintMode #txtCode').val(json.code);
			$('#frmPrintMode #txtName').val(json.name);
			$('#frmPrintMode #txtRemark').val(json.remark);
			$('#frmPrintMode #chkActive').prop('checked',json.status);
		}
	});
}

function clearForm()
{
	var t = setTimeout("alertx('#frmPrintMode')",2000);
	showWaiting();
	loadCombo_frmPrintMode();
	$('#frmPrintMode #cboSearch').val('');
	$('#frmPrintMode #txtCode').val('');
	$('#frmPrintMode #txtName').val('');
	$('#frmPrintMode #txtRemark').val('');
	$('#frmPrintMode #chkActive').prop('checked',true);
	getNextCode();
	$('#frmPrintMode #txtName').focus();
	hideWaiting();
}

function deleteData()
{	
	if($('#frmPrintMode #cboSearch').val()=='')
	{
		$('#frmPrintMode #butDelete').validationEngine('showPrompt','Please select printMode.','fail');
		var t=setTimeout("alertx('#frmPrintMode #butDelete')",2000);	
		return;		
	}
	
	var val = $.prompt('Are you sure you want to delete "'+$('#frmPrintMode #cboSearch option:selected').text()+'" ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
				var url = basepath+"printMode-db-set.php";
				var httpobj = $.ajax({
					url:url,
					dataType:'json',
					type:'post',
					data:'requestType=delete&cboSearch='+$('#frmPrintMode #cboSearch').val(),
					async:false,
					success:function(json){						
						$('#frmPrintMode #butDelete').validationEngine('showPrompt', json.msg,json.type);						
						if(json.type=='pass')
						{
							var t=setTimeout("alertx('#frmPrintMode #butDelete')",2000);
							clearForm();	
							return;
						}	
						var t=setTimeout("alertx('#frmPrintMode #butDelete')",4000);	
					},
					error:function(xhr,status){							
						$('#frmPrintMode #butDelete').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertx('#frmPrintMode #butDelete')",4000);
					} 
				});
			}
		}
	});
}

function loadCombo_frmPrintMode()
{
	var url 	= basepath+"printMode-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmPrintMode #cboSearch').html(httpobj.responseText);
}

function alertx(obj)
{
	$(obj).validationEngine('hide')	;
}
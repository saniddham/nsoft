var basepath	= 'presentation/customerAndOperation/masterData/customerBrands/addNew/';
			
$(document).ready(function() {
  		$("#frmCustomerBrands").validationEngine();
		$('#frmCustomerBrands #txtName').focus();

  ///save button click event
  $('#frmCustomerBrands #butSave').click(function(){
	//$('#frmCustomerBrands').submit();
	var requestType = '';
	if ($('#frmCustomerBrands').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmCustomerBrands #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = basepath+"customerBrands-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json", 
			type:'POST', 
			data:$("#frmCustomerBrands").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmCustomerBrands #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmCustomerBrands').get(0).reset();
						loadCombo_frmCustomerBrands();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmCustomerBrands #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load customer brands details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmCustomerBrands #cboSearch').click(function(){
	   $('#frmCustomerBrands').validationEngine('hide');
   });
    $('#frmCustomerBrands #cboSearch').change(function(){
		$('#frmCustomerBrands').validationEngine('hide');
		var url = basepath+"customerBrands-db-get.php";
		if($('#frmCustomerBrands #cboSearch').val()=='')
		{
			$('#frmCustomerBrands').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'post',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmCustomerBrands #txtName').val(json.name);
					$('#frmCustomerBrands #txtRemark').val(json.remark);
					$('#frmCustomerBrands #chkActive').attr('checked',json.status);
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmCustomerBrands #butNew').click(function(){
		$('#frmCustomerBrands').get(0).reset();
		loadCombo_frmCustomerBrands();
		$('#frmCustomerBrands #txtName').focus();
	});
    $('#frmCustomerBrands #butDelete').click(function(){
		if($('#frmCustomerBrands #cboSearch').val()=='')
		{
			$('#frmCustomerBrands #butDelete').validationEngine('showPrompt', 'Please select Brands.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmCustomerBrands #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basepath+"customerBrands-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'post',
											data:'requestType=delete&cboSearch='+$('#frmCustomerBrands #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmCustomerBrands #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmCustomerBrands').get(0).reset();
													loadCombo_frmCustomerBrands();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});


function loadCombo_frmCustomerBrands()
{
	var url 	= basepath+"customerBrands-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmCustomerBrands #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmCustomerBrands #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmCustomerBrands #butDelete').validationEngine('hide')	;
}

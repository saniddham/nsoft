<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];
	$name			= trim($_REQUEST['txtName']);
	$remark			= trim($_REQUEST['txtRemark']);
	$intStatus		= ($_REQUEST['chkActive']?1:0);	
	
	/////////// customer brands insert part /////////////////////
	if($requestType=='add')
	{
		$sql = "INSERT INTO `mst_brand` (`strName`,`strRemark`,`intCreator`,dtmCreateDate,intStatus) 
				VALUES ('$name','$remark','$userId',now(),'$intStatus')";
		$result = $db->RunQuery($sql);
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	/////////// customer brands update part /////////////////////
	else if($requestType=='edit')
	{
		$sql = "UPDATE `mst_brand` SET 	strName		='$name',
												strRemark	='$remark',
												intStatus	='$intStatus',
												intModifyer	='$userId'
				WHERE (`intId`='$id')";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// customer brands delete part /////////////////////
	else if($requestType=='delete')
	{
		$sql_select	=  "SELECT
						trn_sampleinfomations.intSampleNo,
						trn_sampleinfomations.intSampleYear
						FROM
						trn_sampleinfomations
						WHERE
						trn_sampleinfomations.intBrand = '$id'";
						
		$result_select	= $db->RunQuery($sql);
		if(mysqli_num_rows($result)>=1)
		{
			$sql = "UPDATE `mst_brand` SET intStatus	='-1' 
				WHERE (`intId`='$id')";
			$result = $db->RunQuery($sql);
			if(($result)){
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Deleted successfully.';
			}
			else{
				$response['type'] 		= 'fail';
				$response['msg'] 		= $db->errormsg;
				$response['q'] 			=$sql;
			}
		}
		else 
		{
			$response['type'] 		= 'fail';
			$response['msg'] 		= 'This Brand alredy used in sample information sheet.';	
		}	
	}
	echo json_encode($response);
?>
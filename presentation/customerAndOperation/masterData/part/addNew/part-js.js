var basepath	= 'presentation/customerAndOperation/masterData/part/addNew/';

function functionList()
{
	if(partCode!='')
	{
		$('#frmPart #txtCode').val('PT'+partCode);
	}
}			
$(document).ready(function() {
	getNextCode();	
	$("#frmPart").validationEngine();
	$('#frmPart #txtCode').focus();

  ///save button click event
  $('#frmPart #butSave').click(function(){
	//$('#frmPart').submit();
	var requestType = '';
	if ($('#frmPart').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmPart #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = basepath+"part-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",
			type:'post',  
			data:$("#frmPart").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmPart #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmPart').get(0).reset();
						loadCombo_frmPart();
						getNextCode();
						var t=setTimeout("alertx()",1000);
						location.reload(true);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmPart #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load location details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmPart #cboSearch').click(function(){
	   $('#frmPart').validationEngine('hide');
   });
    $('#frmPart #cboSearch').change(function(){
		$('#frmPart').validationEngine('hide');
		var url = basepath+"part-db-get.php";
		if($('#frmPart #cboSearch').val()=='')
		{
			$('#frmPart').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'post',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmPart #txtCode').val(json.code);
					$('#frmPart #txtName').val(json.name);
					$('#frmPart #txtRemark').val(json.remark);
					$('#frmPart #chkActive').attr('checked',json.status);
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmPart #butNew').click(function(){
		$('#frmPart').get(0).reset();
		loadCombo_frmPart();
		getNextCode();
		$('#frmPart #txtName').focus();
	});
    $('#frmPart #butDelete').click(function(){
		if($('#frmPart #cboSearch').val()=='')
		{
			$('#frmPart #butDelete').validationEngine('showPrompt', 'Please select part.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmPart #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basepath+"part-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'post',
											data:'requestType=delete&cboSearch='+$('#frmPart #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmPart #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmPart').get(0).reset();
													loadCombo_frmPart();
													getNextCode();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});

function getNextCode()
{
	if($('#frmPart #txtCode').val()!='')
		return;

	var url 	= "presentation/customerAndOperation/masterData/part/addNew/part-db-get.php";
	var data	= "requestType=URLgetNextCode";

	$.ajax({
		url:url,
		data:data,
		dataType: "json",  
		type:'POST',
		async:false,			
		success:function(json){
			$('#frmPart #txtCode').val('PT'+json.NextId);
		}
	});	
}


function loadCombo_frmPart()
{
	var url 	= basepath+"part-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmPart #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmPart #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmPart #butDelete').validationEngine('hide')	;
}

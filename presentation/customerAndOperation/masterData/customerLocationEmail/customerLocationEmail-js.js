var basepath	= 'presentation/customerAndOperation/masterData/customerLocationEmail/';

$(document).ready(function(){	
	$("#frmCustomerLocationEmail").validationEngine();
	$('#cboCustomer').die('change').live('change',loadData);
	$('#butSave').die('click').live('click',saveData);
	$('#butNew').die('click').live('click',clearAll);
});

function loadData()
{
	$("#tblMain tr:gt(0)").remove();
	
	if($(this).val()=='')
	{
		return;
	}
	var url 	= basepath+"customerLocationEmail-db-get.php?requestType=loadData";
	var httpobj = $.ajax({
		url:url,
		type:'POST',
		dataType:'json',
		data:"customer="+$(this).val(),
		async:false,
		success:function(json){
			
			var lengthDetail   = json.arrDetailData.length;
			var arrDetailData  = json.arrDetailData;
			
			for(var i=0;i<lengthDetail;i++)
			{
				var locationId	= arrDetailData[i]['LocationId'];	
				var location	= arrDetailData[i]['Location'];
				var email		= arrDetailData[i]['Email'];
				var ccEmail		= arrDetailData[i]['CCEmail'];
				
				var content='<tr class="normalfnt" id='+locationId+'><td>'+location+'</td>';
				content+='<td align="center" ><textarea name="txtEmail" id="txtEmail" class="clsEmail validate[maxSize[250]]" cols="30" rows="2" >'+email+'</textarea></td>';
				content+='<td align="center" ><textarea name="txtCCEmail" id="txtCCEmail" class="clsCCEmail validate[maxSize[250]]" cols="30" rows="2" >'+ccEmail+'</textarea></td></tr>';
				
				add_new_row('#frmCustomerLocationEmail #tblMain',content);
			}
		}
	});
	
}
function saveData()
{
	var customerId = $('#cboCustomer').val();
	
	if(customerId=='')
	{
		$(this).validationEngine('showPrompt', 'Please select a Customer.','fail');
		var t=setTimeout("alertx()",3000);	
		return;
	}
	var chkStatus = true;
	var value="[ ";
	$('.clsEmail').each(function(){

		locationId  = $(this).parent().parent().attr('id');
		email 		= $(this).val();
		ccEmail 	= $(this).parent().parent().find('.clsCCEmail').val();
		value +='{"locationId":"'+locationId+'","email":'+URLEncode_json(email)+',"ccEmail":'+URLEncode_json(ccEmail)+'},' ;
		chkStatus = false;	
	});
	
	value = value.substr(0,value.length-1);
	value += " ]";
	
	if(chkStatus)
	{
		$(this).validationEngine('showPrompt', 'No data to save.','fail');
		var t=setTimeout("alertx()",3000);	
		return;
	}
	
	var url = basepath+"customerLocationEmail-db-set.php?requestType=saveData";
	$.ajax({
			url:url,
			type:'post',
			async:false,
			dataType:'json',
			data:"&customerId="+customerId+"&emailDetails="+value,
	success:function(json){
			$('#butSave').validationEngine('showPrompt', json.msg,json.type);
			if(json.type=='pass')
			{
				var t=setTimeout("alertx()",3000);
				return;
			}
		},
		error:function(){
			
			$('#frmCustomerLocationEmail #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
			var t=setTimeout("alertx()",3000);	
			return;					
		}
	});
	
}
function clearAll()
{
	$("#tblMain tr:gt(0)").remove();
	$('#cboCustomer').val('');
}
function add_new_row(table,rowcontent)
{
	if ($(table).length>0)
	{
		if ($(table+' > tbody').length==0) $(table).append('<tbody />');
		($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
	}
}
function alertx()
{
	$('#frmCustomerLocationEmail #butSave').validationEngine('hide')	;
}
var basepath	= 'presentation/customerAndOperation/masterData/shipmentMethod/addNew/';
			
$(document).ready(function() {
  		$("#frmShipmentMethod").validationEngine();
		$('#frmShipmentMethod #txtName').focus();

  ///save button click event
  $('#frmShipmentMethod #butSave').click(function(){
	//$('#frmShipmentMethod').submit();
	var requestType = '';
	if ($('#frmShipmentMethod').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmShipmentMethod #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = basepath+"shipmentMethod-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",
			type:'post',  
			data:$("#frmShipmentMethod").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmShipmentMethod #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmShipmentMethod').get(0).reset();
						loadCombo_frmShipmentMethod();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmShipmentMethod #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load shipment method details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmShipmentMethod #cboSearch').click(function(){
	   $('#frmShipmentMethod').validationEngine('hide');
   });
    $('#frmShipmentMethod #cboSearch').change(function(){
		$('#frmShipmentMethod').validationEngine('hide');
		var url = basepath+"shipmentMethod-db-get.php";
		if($('#frmShipmentMethod #cboSearch').val()=='')
		{
			$('#frmShipmentMethod').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'post',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmShipmentMethod #txtName').val(json.name);
					$('#frmShipmentMethod #txtRemark').val(json.remark);
					$('#frmShipmentMethod #chkActive').attr('checked',json.status);
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmShipmentMethod #butNew').click(function(){
		$('#frmShipmentMethod').get(0).reset();
		loadCombo_frmShipmentMethod();
		$('#frmShipmentMethod #txtName').focus();
	});
    $('#frmShipmentMethod #butDelete').click(function(){
		if($('#frmShipmentMethod #cboSearch').val()=='')
		{
			$('#frmShipmentMethod #butDelete').validationEngine('showPrompt', 'Please select Method.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmShipmentMethod #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basepath+"shipmentMethod-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'post',
											data:'requestType=delete&cboSearch='+$('#frmShipmentMethod #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmShipmentMethod #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmShipmentMethod').get(0).reset();
													loadCombo_frmShipmentMethod();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});


function loadCombo_frmShipmentMethod()
{
	var url 	= basepath+"shipmentMethod-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmShipmentMethod #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmShipmentMethod #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmShipmentMethod #butDelete').validationEngine('hide')	;
}

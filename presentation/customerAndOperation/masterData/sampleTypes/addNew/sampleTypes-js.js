var basepath	= 'presentation/customerAndOperation/masterData/sampleTypes/addNew/';
	
$(document).ready(function() {
  		$("#frmSampleTypes").validationEngine();
		$('#frmSampleTypes #txtName').focus();

  ///save button click event
  $('#frmSampleTypes #butSave').click(function(){
	//$('#frmSampleTypes').submit();
	var requestType = '';
	if ($('#frmSampleTypes').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmSampleTypes #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = basepath+"sampleTypes-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json", 
			type:'POST', 
			data:$("#frmSampleTypes").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmSampleTypes #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmSampleTypes').get(0).reset();
						loadCombo_frmSampleTypes();
						var t=setTimeout("alertx()",1000);
						//location.reload(true);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmSampleTypes #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load location details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmSampleTypes #cboSearch').click(function(){
	   $('#frmSampleTypes').validationEngine('hide');
   });
    $('#frmSampleTypes #cboSearch').change(function(){
		$('#frmSampleTypes').validationEngine('hide');
		var url = basepath+"sampleTypes-db-get.php";
		if($('#frmSampleTypes #cboSearch').val()=='')
		{
			$('#frmSampleTypes').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					//$('#frmSampleTypes #txtCode').val(json.code);
					$('#frmSampleTypes #txtName').val(json.name);
					$('#frmSampleTypes #txtRemark').val(json.remark);
					$('#frmSampleTypes #chkActive').attr('checked',json.status);
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmSampleTypes #butNew').click(function(){
		$('#frmSampleTypes').get(0).reset();
		//location.reload(true);
		loadCombo_frmSampleTypes();
		$('#frmSampleTypes #txtName').focus();
	});
    $('#frmSampleTypes #butDelete').click(function(){
		if($('#frmSampleTypes #cboSearch').val()=='')
		{
			$('#frmSampleTypes #butDelete').validationEngine('showPrompt', 'Please select Wash Standerd.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmSampleTypes #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basepath+"sampleTypes-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'POST',
											data:'requestType=delete&cboSearch='+$('#frmSampleTypes #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmSampleTypes #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmSampleTypes').get(0).reset();
													loadCombo_frmSampleTypes();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});


function loadCombo_frmSampleTypes()
{
	var url 	= basepath+"sampleTypes-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmSampleTypes #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmSampleTypes #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmSampleTypes #butDelete').validationEngine('hide')	;
}

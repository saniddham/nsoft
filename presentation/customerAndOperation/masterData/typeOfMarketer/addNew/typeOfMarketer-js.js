$(document).ready(function() {
	getNextCode();
	$("#frmTypeOfMarketer").validationEngine();
	$('#frmTypeOfMarketer #txtName').focus();
	$('#frmTypeOfMarketer #butSave').die('click').live('click',save);
  	$('#frmTypeOfMarketer #cboSearch').die('change').live('change',searchCombo);	
	$('#frmTypeOfMarketer #butNew').die('click').live('click',clearForm);	
	$('#frmTypeOfMarketer #butDelete').die('click').live('click',deleteData);
});

function save()
{
	
	if(!$('#frmTypeOfMarketer').validationEngine('validate'))
	{
		var t = setTimeout("alertx('#frmTypeOfMarketer')",2000);
    	return;
	}
	
	showWaiting();
	
	if($("#frmTypeOfMarketer #cboSearch").val()=='')
		requestType = 'add';
	else
		requestType = 'edit';
	
	var url = "presentation/customerAndOperation/masterData/typeOfMarketer/addNew/typeOfMarketer-db-set.php";
	var obj = $.ajax({
		url:url,
		dataType: "json",  
		type:'post',
		data:$("#frmTypeOfMarketer").serialize()+'&requestType='+requestType,
		async:false,		
		success:function(json){
				$('#frmTypeOfMarketer #butSave').validationEngine('showPrompt', json.msg,json.type);
				if(json.type=='pass')
				{
					var t = setTimeout("alertx('#frmTypeOfMarketer #butSave')",2000);
					hideWaiting();
					clearForm();
					return;
				}
				hideWaiting();
				var t=setTimeout("alertx('#frmTypeOfMarketer #butSave')",4000);
			},
		error:function(xhr,status){		
				hideWaiting();		
				$('#frmTypeOfMarketer #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				var t=setTimeout("alertx('#frmTypeOfMarketer #butSave')",4000);
			}		
	});
}

function searchCombo()
{	
	alertx('#frmTypeOfMarketer');
	var url = "presentation/customerAndOperation/masterData/typeOfMarketer/addNew/typeOfMarketer-db-get.php";
	if($('#frmTypeOfMarketer #cboSearch').val()=='')
	{
		$('#frmTypeOfMarketer').get(0).reset();
		return;	
	}
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		type:'post',
		data:'requestType=loadDetails&id='+$(this).val(),
		async:false,
		success:function(json){
			$('#frmTypeOfMarketer #txtCode').val(json.code);
			$('#frmTypeOfMarketer #txtName').val(json.name);
			$('#frmTypeOfMarketer #txtRemark').val(json.remark);
			$('#frmTypeOfMarketer #chkActive').attr('checked',json.status);
		}
	});
}

function deleteData()
{	
	if($('#frmTypeOfMarketer #cboSearch').val()=='')
	{
		$('#frmTypeOfMarketer #butDelete').validationEngine('showPrompt', 'Please select Type.', 'fail');
		var t = setTimeout("alertx('#frmTypeOfMarketer #butDelete')",1000);	
		return;
	}
		var val = $.prompt('Are you sure you want to delete "'+$('#frmTypeOfMarketer #cboSearch option:selected').text()+'" ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
				if(v)
				{
					var url = "presentation/customerAndOperation/masterData/typeOfMarketer/addNew/typeOfMarketer-db-set.php";
					var httpobj = $.ajax({
						url:url,
						dataType:'json',
						type:'post',
						data:'requestType=delete&cboSearch='+$('#frmTypeOfMarketer #cboSearch').val(),
						async:false,
						success:function(json){							
							$('#frmTypeOfMarketer #butDelete').validationEngine('showPrompt', json.msg,json.type);							
							if(json.type=='pass')
							{								
								var t=setTimeout("alertx('#frmTypeOfMarketer #butDelete')",1000);
								clearForm();
								return;;
							}	
							var t=setTimeout("alertx('#frmTypeOfMarketer #butDelete')",4000);
						},
						error:function(xhr,status){				
							$('#frmTypeOfMarketer #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx('#frmTypeOfMarketer #butDelete')",4000);
						}	 
					});
				}
			}
		});
}

function getNextCode()
{
	if($('#frmTypeOfMarketer #txtCode').val()!='')
		return;
		
	var url 	= "presentation/customerAndOperation/masterData/typeOfMarketer/addNew/typeOfMarketer-db-get.php";
	var data	= "requestType=URLgetNextCode";
	var a		= 0;
	$.ajax({
		url:url,
		data:data,
		dataType: "json",
		type:'post',  
		async:false,			
		success:function(json){
			$('#frmTypeOfMarketer #txtCode').val('MK'+json.NextId);
		}
	});	
}

function clearForm()
{	
	var t = setTimeout("alertx('#frmTypeOfMarketer')",1000);
	showWaiting();
	$('#frmTypeOfMarketer #cboSearch').val('');
	$('#frmTypeOfMarketer #txtCode').val('');
	$('#frmTypeOfMarketer #txtName').val('');
	$('#frmTypeOfMarketer #txtRemark').val('');
	$('#frmTypeOfMarketer #chkActive').prop('checked',true);
	loadCombo_frmTypeOfMarketer();
	getNextCode();
	$('#frmTypeOfMarketer #txtName').focus();	
	hideWaiting();
}

function loadCombo_frmTypeOfMarketer()
{
	var url 	= "presentation/customerAndOperation/masterData/typeOfMarketer/addNew/typeOfMarketer-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmTypeOfMarketer #cboSearch').html(httpobj.responseText);
}

function alertx(obj)
{
	$(obj).validationEngine('hide')	;
}
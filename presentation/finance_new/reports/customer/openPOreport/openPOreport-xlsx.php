<?php
session_start();
ini_set('max_execution_time',600000);
$backwardseperator 	= "../../../../../";

require_once "{$backwardseperator}dataAccess/Connector.php";
require_once '../../../../../libraries/excel/Classes/PHPExcel.php';
require_once '../../../../../libraries/excel/Classes/PHPExcel/IOFactory.php';

$customer		= $_REQUEST["customer"]; 
$cusLocation	= $_REQUEST["cusLocation"];
$PONo			= $_REQUEST["PONo"];
$orderYear		= $_REQUEST["orderYear"];
$orderNo		= $_REQUEST["orderNo"];
$type			= $_REQUEST["type"];
$company		= $_REQUEST["company"];
$paymentCom		= $_REQUEST["paymentCom"];
$wsql 			= '';
$hsql 			= '';
$i				= 3;

error_reporting(E_ALL);

$file = 'openPO-excel.xls'; //LOAD TEMPARARY FILE

if (!file_exists($file))
	exit("Can't find $fileName ");

$objPHPExcel 	= PHPExcel_IOFactory::load($file);

$styleArray = array(
       'borders' => array(
             'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
             ),
       ),
);


if($customer!='')
	$wsql .="AND OH.intCustomer = '$customer' ";
if($cusLocation!='')
	$wsql .="AND OH.intCustomerLocation = '$cusLocation' ";
if($PONo!='')
	$wsql .="AND OH.strCustomerPoNo LIKE '%$PONo%' ";
if($orderYear!='')
	$wsql .="AND OH.intOrderYear = '$orderYear' ";
if($orderNo!='')
	$wsql .="AND OH.intOrderNo LIKE '%$orderNo%' ";
if($company!='')
	$wsql .="AND ML.intCompanyId = '$company' ";

switch($type)
{
	case 1 :
		$hsql .="AND IN_QTY = 0 ";
	break;
	
	case 2 :
		$hsql .="AND DISPATCH_QTY = 0 ";
	break;
	
	case 3 :
		$hsql .="AND INVOICE_QTY = 0 ";
	break;
}
		
$sql = "SELECT 
		CU.strName AS CUSTOMER_NAME,
		CL.strName AS CUSTOMER_LOCATION,
		OH.strCustomerPoNo AS CUSTOMER_PONO,
		OH.dtDate as PO_DATE,
		ROUND(SUM(OD.intQty * OD.dblPrice),0) AS ORDER_AMOUNT,
		CONCAT(OH.intOrderYear,' - ',OH.intOrderNo) AS CON_ORDER_NO,
		OH.intOrderYear AS ORDER_YEAR,
		OH.intOrderNo AS ORDER_NO,
		ML.intCompanyId AS COMPANY_ID,
		
		SUBSTRING((SELECT GROUP_CONCAT(DISTINCT CSIH.INVOICE_NO)
		FROM finance_customer_invoice_header CSIH  
		WHERE CSIH.ORDER_NO = OH.intOrderNo
		AND CSIH.ORDER_YEAR = OH.intOrderYear
		AND CSIH.STATUS = 1),1,20) AS INVOICE_NO,
		SUM(OD.intQty) AS ORDER_QTY,
		
		ROUND(COALESCE((SELECT SUM(FRD.dblQty)
		FROM ware_fabricreceiveddetails FRD 
		INNER JOIN ware_fabricreceivedheader FRH
		ON FRH.intFabricReceivedNo = FRD.intFabricReceivedNo 
		AND FRH.intFabricReceivedYear = FRD.intFabricReceivedYear
		WHERE FRH.intOrderNo = OH.intOrderNo 
		AND FRH.intOrderYear = OH.intOrderYear 
		AND FRH.intStatus = 1),0)) AS IN_QTY,
		
		ROUND(COALESCE((SELECT SUM(FDD.dblGoodQty)
		FROM ware_fabricdispatchdetails FDD 
		INNER JOIN ware_fabricdispatchheader FDH 
		ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
		AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
		WHERE FDH.intOrderNo = OH.intOrderNo 
		AND FDH.intOrderYear = OH.intOrderYear 
		AND FDH.intStatus = 1),0)) AS DISPATCH_QTY,
		
		ROUND(COALESCE((SELECT SUM(FCID.QTY)
		FROM finance_customer_invoice_details FCID  
		INNER JOIN finance_customer_invoice_header FCIH ON FCIH.SERIAL_NO=FCID.SERIAL_NO
		AND FCIH.SERIAL_YEAR=FCID.SERIAL_YEAR
		WHERE FCIH.ORDER_NO = OH.intOrderNo
		AND FCIH.ORDER_YEAR = OH.intOrderYear
		AND FCIH.STATUS = 1),0)) AS INVOICE_QTY
		
		FROM trn_orderheader OH
		INNER JOIN trn_orderdetails OD ON OD.intOrderNo = OH.intOrderNo AND OD.intOrderYear = OH.intOrderYear
		INNER JOIN mst_customer CU ON CU.intId = OH.intCustomer
		INNER JOIN mst_locations ML ON ML.intId=OH.intLocationId
		LEFT JOIN mst_customer_locations_header CL ON CL.intId = OH.intCustomerLocation
		WHERE 1 = 1 
		$wsql
		AND OH.PAYMENT_RECEIVE_COMPLETED_FLAG = $paymentCom
		AND OH.intStatus <> '-2'
		GROUP BY OH.intOrderYear,OH.intOrderNo
		HAVING 1=1 
		$hsql ";

$result = $db->RunQuery($sql);

while($row=mysqli_fetch_array($result))
{
	$customer   	= $row["CUSTOMER_NAME"];
	$cusLocation  	= $row["CUSTOMER_LOCATION"];
	$PONo   		= $row["CUSTOMER_PONO"];
	$PODate   		= $row["PO_DATE"];
	$POAmount  		= $row["ORDER_AMOUNT"];
	$orderNo   		= $row["CON_ORDER_NO"];
	$invoiceNo 		= $row["INVOICE_NO"];
	$orderQty   	= $row["ORDER_QTY"];
	$inQty   		= $row["IN_QTY"];
	$dispatchQty 	= $row["DISPATCH_QTY"]; 
	$invoiceQty 	= $row["INVOICE_QTY"]; 
	
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$i,$customer);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,$i,$cusLocation);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,$i,$PONo);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,$i,$POAmount);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$i,$PODate);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,$i,$orderNo);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$i,$invoiceNo);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$i,$orderQty);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8,$i,$inQty);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9,$i,$dispatchQty);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10,$i,$invoiceQty);
	$objPHPExcel->getActiveSheet()->getStyle("A$i:k$i")->applyFromArray($styleArray);
	
	$i++;
}

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$file.'"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output'); 
echo 'done';
exit;
	
?>
// JavaScript Document
$(document).ready(function(){

	$("#frmOpenPORpt #butReport").live('click',excelReport);
	$("#frmOpenPORpt #cboCustomer").live('click',loadCusLocation);
	$("#frmOpenPORpt #butNew").live('click',clearAll);
});
function excelReport()
{
	var customer 	 	= $('#frmOpenPORpt #cboCustomer').val();
	var cusLocation 	= $('#frmOpenPORpt #cboCusLocation').val();
	var PONo  	 		= $('#frmOpenPORpt #txtPONo').val();
	var orderYear  	 	= $('#frmOpenPORpt #txtOrderNo').val();
	var orderNo  	 	= $('#frmOpenPORpt #txtOrderYear').val();
	var type  	 		= $('#frmOpenPORpt #cboType').val();
	var company  	 	= $('#frmOpenPORpt #cboPOCompany').val();
	var paymentCom  	= ($('#frmOpenPORpt #chkPaymentComplete').attr('checked')?1:0);

	window.open('openPOreport-xlsx.php?customer='+customer+'&cusLocation='+cusLocation+'&PONo='+PONo+'&orderYear='+orderYear+'&orderNo='+orderNo+'&type='+type+'&company='+company+'&paymentCom='+paymentCom);	
}
function loadCusLocation()
{
	var url				= "rptOpenPO_db.php?requestType=loadCusLocation&customerId="+$(this).val();
	var httpObj 		= $.ajax({url:url,type:'POST',async:false})
	
	$('#frmOpenPORpt #cboCusLocation').html(httpObj.responseText);
}
function clearAll()
{
	$('#frmOpenPORpt')[0].reset();
}
<?php
session_start();
$backwardseperator 	= "../../../../../";
$thisFilePath 		=  $_SERVER['PHP_SELF'];

include  "{$backwardseperator}dataAccess/permisionCheck.inc";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Open PO Report</title>
</head>

<link rel="stylesheet" type="text/css" href="../../../../../css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="../../../../../css/button.css"/>
<link rel="stylesheet" type="text/css" href="../../../../../css/promt.css"/>

<body>
<form id="frmOpenPORpt" name="frmOpenPORpt" autocomplete="off">
<table width="100%" border="0" align="center">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
        <script type="application/javascript" src="rptOpenPO_js.js"></script>
	</tr> 
</table>
<div align="center">
	<div class="trans_layoutS">
	<div class="trans_text">Open PO Report</div>
    <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    	<tr>
        	<td>
            <table width="100%" border="0" align="center">
            	<tr class="normalfnt">
                	<td width="4%">&nbsp;</td>
                    <td width="33%">Customer</td>
                    <td width="63%"><select style="width:300px;" name="cboCustomer" id="cboCustomer" >
                   	<option value=""></option>
                    <?php
						$sql = "SELECT DISTINCT
								CU.intId 		AS CUSTOMER_ID,
								CU.strName 		AS CUSTOMER_NAME
								FROM trn_orderheader OH 
								INNER JOIN mst_customer CU ON OH.intCustomer = CU.intId
								WHERE OH.intStatus = 1
								ORDER BY CU.strName ";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=".$row["CUSTOMER_ID"].">".$row["CUSTOMER_NAME"]."</option>";
						}
					?>
                    </select></td>
                </tr>
            	<tr class="normalfnt">
            	  <td>&nbsp;</td>
            	  <td>Customer Location</td>
            	  <td><select style="width:300px;" name="cboCusLocation" id="cboCusLocation" >
                  <option value=""></option>
					<?php
						$sql = "SELECT DISTINCT CLH.intId,CLH.strName
								FROM mst_customer_locations_header CLH
								INNER JOIN mst_customer_locations CL ON CL.intLocationId=CLH.intId
								WHERE 1=1
								ORDER BY CLH.strName";
						$result = $db->RunQuery($sql);
						
                    while($row = mysqli_fetch_array($result))
                    {
						echo "<option value=\"".$row["intId"]."\" >".$row["strName"]."</option>";
                    }
                    ?>
                    </select></td>
          	  </tr>
            	<tr class="normalfnt">
            	  <td>&nbsp;</td>
            	  <td>PO No</td>
            	  <td><input type="text" name="txtPONo" id="txtPONo" style="width:140px;" /></td>
          	  </tr>
            	<tr class="normalfnt">
            	  <td>&nbsp;</td>
            	  <td>Order Year/No</td>
            	  <td><input type="text" name="txtOrderNo" id="txtOrderNo" style="width:50px;" />&nbsp;<input type="text" name="txtOrderYear" id="txtOrderYear" style="width:85px;" /></td>
          	  </tr>
            	<tr class="normalfnt">
            	  <td>&nbsp;</td>
            	  <td>Type</td>
            	  <td><select style="width:140px;" name="cboType" id="cboType" >
                  <option value=""></option>
                  <option value="1">In Qty = 0</option>
                  <option value="2">Dispatch Qty = 0</option>
                  <option value="3">Invoice Qty = 0</option>
          	    </select></td>
          	  </tr>
            	<tr class="normalfnt">
            	  <td>&nbsp;</td>
            	  <td>Company</td>
            	  <td><select style="width:300px;" name="cboPOCompany" id="cboPOCompany" >
            	    <option value=""></option>
            	    <?php
						$sql = "SELECT intId,strName
								FROM mst_companies
								WHERE intStatus = 1
								ORDER BY strName";
						$result = $db->RunQuery($sql);
						
                    while($row = mysqli_fetch_array($result))
                    {
						echo "<option value=\"".$row["intId"]."\" >".$row["strName"]."</option>";
                    }
                    ?>
          	    </select></td>
          	  </tr>
            	<tr class="normalfnt">
            	  <td>&nbsp;</td>
            	  <td>Payment Complete</td>
            	  <td><input type="checkbox" name="chkPaymentComplete" id="chkPaymentComplete" /></td>
          	  </tr>
            </table>
            </td>
        </tr>
        <tr>
            <td height="32">
            	<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                    <td align="center" ><a class="button white medium" id="butNew">New</a><a class="button white medium" id="butReport">Report</a><a href="../../../../main.php" class="button white medium" id="butSave">Close</a>
                    </td>
                </tr>
            </table>
            </td>
        </tr>
    </table>
    </div>
</div>       
</form>
</body>
</html>
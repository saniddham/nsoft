<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId 			= $_SESSION['headCompanyId'];
$location 			= $_SESSION['CompanyID'];
$intUser 			= $_SESSION["userId"];
$deci				= 2;
$OrderComYear		= date('Y')-2 ;

$invMenuId			= 701;
$advSetMenuId		= 722;
$advMenuId			= 702;
$crdNtMenuId		= 704;
$dbtNtMenuId		= 758;
$payRcvMenuId		= 706;

if(!isset($_POST["cboOrder"])){
	$cboOrder		= 1;
	$sqlLimit		= 0;
}else{
	$cboOrder		= $_REQUEST["cboOrder"];
	$sqlLimit		= "";
}
	
$cboInvoice			= (!isset($_REQUEST["cboInvoice"])?'':$_REQUEST["cboInvoice"]);
$cboCreditPeriod	= (!isset($_REQUEST["cboCreditPeriod"])?'':$_REQUEST["cboCreditPeriod"]);
$cboCustomer		= (!isset($_REQUEST["cboCustomer"])?'':$_REQUEST["cboCustomer"]);
$txtCustomerPONo	= (!isset($_REQUEST["txtCustomerPONo"])?'':$_REQUEST["txtCustomerPONo"]);
$chkDateStatus		= (!isset($_REQUEST["chkDateFilter"])?0:1);
$txtDateFrom		= (!isset($_REQUEST["txtDateFrom"])?'':$_REQUEST["txtDateFrom"]);
$txtDateTo			= (!isset($_REQUEST["txtDateTo"])?'':$_REQUEST["txtDateTo"]);
//END 	- SET PARAMETERS	}

//BEGIN - INCLUDE FILES {
//include  	"{$backwardseperator}dataAccess/Connector.php";
//END 	- INCLUDE FILES }

$lastPaymentRange		= GetLastPaymentRange();
$payment_term_array 	= GetPaymentTerm();
$payment_range 			= GetPaymentTerm_Range($cboCreditPeriod);

$payment_range_array	= explode('-',$payment_range);
?>
<title>Customer Invoice Aging Report</title>

<script type="text/javascript">
function Reload()
{
	document.frmCustomerInvoiceAgingReport.submit();	
}
function checkDateVal(obj)
{
	if($(obj).is(':checked'))
	{
		$('#frmCustomerInvoiceAgingReport #txtDateFrom').attr('disabled',false);
		$('#frmCustomerInvoiceAgingReport #txtDateTo').attr('disabled',false);
	}
	else
	{
		$('#frmCustomerInvoiceAgingReport #txtDateFrom').attr('disabled',true);
		$('#frmCustomerInvoiceAgingReport #txtDateTo').attr('disabled',true);
		$('#frmCustomerInvoiceAgingReport #txtDateFrom').val('');
		$('#frmCustomerInvoiceAgingReport #txtDateTo').val('');
	}
	
}

</script>

<style type="text/css">
#progress {
 width: 100px;   
 border: 1px solid black;
 position: relative;
 padding: 1px;
}

#percent {
 position: absolute;   
 left: 50%;
}

#bar {
 height: 15px;
 background-color: #0C0;
}
</style>

<form id="frmCustomerInvoiceAgingReport" name="frmCustomerInvoiceAgingReport" method="post">
  <div align="center">
  <div><strong>Customer Invoice Aging Report</strong></div>
<table width="1100" border="0" align="center">
 <tr>
   <td><table width="1341" border="0" class="normalfnt">
     <tr>
       <td width="204" style="text-align:center">Customer</td>
       <td width="204" style="text-align:center">Order</td>
       <td width="204" style="text-align:center">Invoice</td>
       <td width="204" style="text-align:center">Credit Period</td>
       <td width="147" style="text-align:center">Customer PONo</td>
        <td width="29" style="text-align:center">&nbsp;</td>
        <td width="128" style="text-align:left">Dispatch Date from</td>
        <td width="127" style="text-align:left">Dispatch Date to</td>
       <td width="69" rowspan="2" valign="bottom" style="text-align:center"><a class="button white medium" onClick="Reload();">Search</a></td>
       </tr>
     <tr>
       <td style="text-align:center"><select style="width:200px" name="cboCustomer" id="cboCustomer" >
       <option value="">&nbsp;</option>
            <?php 
				$sql = "SELECT DISTINCT
							CU.intId 		AS CUSTOMER_ID,
							CU.strName 		AS CUSTOMER_NAME
						FROM trn_orderheader OH 
						INNER JOIN mst_customer CU ON OH.intCustomer = CU.intId
						WHERE OH.intStatus = 1
						ORDER BY CU.strName";
				$result = $db->RunQuery($sql);
				while($row = mysqli_fetch_array($result))
				{
					if($cboCustomer==$row["CUSTOMER_ID"])
            			echo "<option value=".$row["CUSTOMER_ID"]." selected=\"selected\">".$row["CUSTOMER_NAME"]."</option>";
					else
						echo "<option value=".$row["CUSTOMER_ID"].">".$row["CUSTOMER_NAME"]."</option>";
           		} 
		   ?>
       </select></td>
       <td style="text-align:center"><select style="width:200px" name="cboOrder" id="cboOrder" >
       		<option value="" <?php echo ($cboOrder==''?'selected=selected':'')?>>ALL</option>
       		<option value="1" <?php echo ($cboOrder=='1'?'selected=selected':'')?>>Order Completed</option>
            <option value="2" <?php echo ($cboOrder=='2'?'selected=selected':'')?>>Pending</option>
         </select></td>
       <td style="text-align:center"><select style="width:200px" name="cboInvoice" id="cboInvoice" >
       		<option value="" <?php echo ($cboInvoice==''?'selected=selected':'')?>>ALL</option>
       	 	<option value="1" <?php echo ($cboInvoice=='1'?'selected=selected':'')?>>Balance To Invoice</option>
            <option value="2" <?php echo ($cboInvoice=='2'?'selected=selected':'')?>>Balance Payment Received</option>
         </select></td>
       <td style="text-align:center"><select style="width:200px" name="cboCreditPeriod" id="cboCreditPeriod" >
       		<option value="" <?php echo ($cboCreditPeriod==''?'selected=selected':'')?>>All</option>
            <?php 
				$sql = "SELECT intId,strName FROM mst_financepaymentsterms WHERE strName <> 0 ORDER BY strName ";
				$result = $db->RunQuery($sql);
				while($row = mysqli_fetch_array($result))
				{
					if($cboCreditPeriod==$row["intId"])
            			echo "<option value=".$row["intId"]." selected=\"selected\">".$row["strName"]."</option>";
					else
						echo "<option value=".$row["intId"].">".$row["strName"]."</option>";
           		} 
		   ?>
           <option value="More" <?php echo ($cboCreditPeriod=='More'?'selected=selected':'')?>>More</option>
         </select></td>
       <td style="text-align:center"><input type="text" style="width:100%" name="txtCustomerPONo" id="txtCustomerPONo" value="<?php echo $txtCustomerPONo?>"/></td>
       <td style="text-align:right"><input name="chkDateFilter" id="chkDateFilter" type="checkbox" onclick="checkDateVal(this);" <?php echo($chkDateStatus==1?'checked="checked"':''); ?>/></td>
                    <td style="text-align:left"><input name="txtDateFrom" type="text" value="<?php echo $txtDateFrom; ?>" id="txtDateFrom" style="width:98px;" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo($chkDateStatus==0?'disabled="disabled"':'');?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                    <td style="text-align:left"><input name="txtDateTo" type="text" value="<?php echo $txtDateTo; ?>" id="txtDateTo" style="width:98px;" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo($chkDateStatus==0?'disabled="disabled"':'');?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
       </tr>
   </table></td>
 </tr>
 <tr>
  <td><table  border="0" cellpadding="0" cellspacing="0" class="bordered" width="100%" bgcolor="#FFFFFF">
    <thead>
      <tr valign="bottom">
        <th>Customer Name</th>
          <th>Marketer</th>
        <th>Customer Location</th>
        <th>Production Plant</th>
        <th>Dispatched Location</th>
        <th>PONo</th>
        <th>Order No</th>
        <th>Order Date</th>
        <th>PSD Date</th>
        <th>Sample No</th>
        <th>Technique Group</th>
        <th>Invoice No</th>
        <th>Graphic No's</th>
        <th>Order Qty</th>
        <th>Dispatch Qty</th>
        <th>Dispatch Prog</th>
        <th>Rate</th>
        <th>Dispatched Amount</th>
        <th>Invoiced Amount</th>
        <th>Advanced Not Settle</th>
        <th>Advanced Amount</th>
        <th>Credit Amount</th>
        <th>Debit Amount</th>
        <th>Excess Received</th>
        <th>Payment Received Amount</th>
        <th>Balance To Invoice</th>
        <th>Balance To Payment Received</th>
        <th>Last Dispatch Date</th>
        <th>Credit Term</th>
        <?php for($i=0;$i<count($payment_term_array);$i++){?>
        <th style="width:40px"><?php echo $payment_term_array[$i]?></th>
        <?php }?>
        </tr>
    </thead>
     <tbody>
<?php
$result = GetMainDetails($cboOrder,$cboInvoice,$cboCreditPeriod,$cboCustomer,$txtCustomerPONo,$chkDateStatus,$txtDateFrom,$txtDateTo);
while($row = mysqli_fetch_array($result))
{
	$progress 			= (round((round($row["DISPATCH_QTY"])/round($row["ORDER_QTY"]))*100,2)>100?100:round((round($row["DISPATCH_QTY"])/round($row["ORDER_QTY"]))*100,2));
?>
      <tr>
        <td nowrap="nowrap" title="Customer Name" ><?php echo $row["CUSTOMER_NAME"]?></td>
        <td nowrap="nowrap" title="Marketer" ><?php echo $row["MARKETER"]?></td>
        <td nowrap="nowrap" title="customer Location"><?php echo $row["CUSTOMER_LOCATION"]?></td>
        <td nowrap="nowrap" title="dispatch plants"><?php echo $row["DISP_PLANTS"]?></td>
        <td nowrap="nowrap" title="dispatch plants"><?php echo $row["DISP_LOCATIONS"]?></td>
        <td nowrap="nowrap" title="Customer PO"><?php echo $row["CUSTOMER_PONO"]?></td>
        <td nowrap="nowrap" title="Order No"><?php echo $row["ORDER_NO"]?></td>
        <td nowrap="nowrap" title="Order Date"><?php echo $row["ORDER_DATE"]?></td>
        <td nowrap="nowrap" title="PSD date"><?php echo $row["PSD_DATE"]?></td>
        <td nowrap="nowrap" title="Sample"><?php echo $row["SAMPLE_NO"]?></td>
        <td nowrap="nowrap" title="Technique group"><?php echo $row["TECH_GROUP"]?></td>
        <td nowrap="nowrap" title="Invoice No"><?php echo $row["INVOICE_NO"]?></td>
        <td nowrap="nowrap" title="Graphic No's"><?php echo $row["GRAPHIC_NO"]?></td>
        <td nowrap="nowrap" style="text-align:right" title="Order Qty"><?php echo number_format($row["ORDER_QTY"])?></td>
        <td nowrap="nowrap" style="text-align:right" title="Dispatch Qty"><?php echo number_format($row["DISPATCH_QTY"])?></td>
        <td nowrap="nowrap" title="Dispatch Prog"><div id="progress"><span id="percent"><?php echo $progress?>%</span><div id="bar" style="width:<?php echo $progress?>px"></div></div></td>
        <td nowrap="nowrap" style="text-align:right" title="Rate"><?php echo number_format($row["ORDER_RATE"],4)?></td>
        <td nowrap="nowrap" style="text-align:right" title="Dispatched Amount"><?php echo number_format($row["DISPATCH_AMOUNT"],$deci)?></td>
        <td nowrap="nowrap" style="text-align:right" title="Invoiced Amount"><?php echo (round($row["BALANCE_TO_INVOICE"],$deci)>0 ? "<a href=\"?q=".$invMenuId."&CustomerPO=".base64_encode($row["CUSTOMER_PONO"])."&CustomerId=".$row["CUSTOMER_ID"]."\" target=\"invoice.php\">".number_format($row["INVOICE_AMOUNT"],$deci)."</a>":number_format($row["INVOICE_AMOUNT"],$deci))?></td>        
        <td nowrap="nowrap" style="text-align:right" title="Advanced Not Settle"><?php echo (round($row["ADVANCE_NOT_SETTLED"],$deci)>0 ? "<a href=\"?q=".$advSetMenuId."&cboCustomer=".urlencode($row["CUSTOMER_ID"])."&cboCurrency=".$row["CURRENCY_ID"]."\" target=\"advance_settlement.php\">".number_format($row["ADVANCE_NOT_SETTLED"],$deci)."</a>":number_format($row["ADVANCE_NOT_SETTLED"],$deci))?></td>
        <td nowrap="nowrap" style="text-align:right" title="Advanced Amount"><?php echo (round($row["BALANCE_TO_PAYMENT_RECEIVED"],$deci)>0 ? "<a href=\"?q=".$advMenuId."&CustomerId=".urlencode($row["CUSTOMER_ID"])."&CurrencyId=".$row["CURRENCY_ID"]."\" target=\"advance_settlement.php\">".number_format($row["ADVANCE_SETTLED"],$deci)."</a>":number_format($row["ADVANCE_SETTLED"],$deci))?></td>
        <td nowrap="nowrap" style="text-align:right" title="Credit Amount"><?php echo (round($row["BALANCE_TO_PAYMENT_RECEIVED"],$deci)>0 ? "<a href=\"?q=".$crdNtMenuId."&CustomerId=".urlencode($row["CUSTOMER_ID"])."&CurrencyId=".$row["CURRENCY_ID"]."\" target=\"payment_receive.php\">".number_format($row["CREDIT_NOTE_AMOUNT"],$deci)."</a>":number_format($row["CREDIT_NOTE_AMOUNT"],$deci))?></td>
        <td nowrap="nowrap" style="text-align:right" title="Debit Amount"><?php echo (round($row["BALANCE_TO_PAYMENT_RECEIVED"],$deci)>0 ? "<a href=\"?q=".$dbtNtMenuId."&CustomerId=".urlencode($row["CUSTOMER_ID"])."&CurrencyId=".$row["CURRENCY_ID"]."\" target=\"payment_receive.php\">".number_format($row["DEBIT_NOTE_AMOUNT"],$deci)."</a>":number_format($row["DEBIT_NOTE_AMOUNT"],$deci))?></td>
        <td nowrap="nowrap" style="text-align:right" title="Excess Received Amount"><?php echo number_format($row["EX_RECEIVED_AMOUNT"],$deci)?></td>
        <td nowrap="nowrap" style="text-align:right" title="Payment Received Amount"><?php echo (round($row["BALANCE_TO_PAYMENT_RECEIVED"],$deci)>0 ? "<a href=\"?q=".$payRcvMenuId."&CustomerId=".urlencode($row["CUSTOMER_ID"])."&CurrencyId=".$row["CURRENCY_ID"]."\" target=\"payment_receive.php\">".number_format($row["PAYMENT_RECEIVED_AMOUNT"],$deci)."</a>":number_format($row["PAYMENT_RECEIVED_AMOUNT"],$deci))?></td>
        <td nowrap="nowrap" style="text-align:right" title="Balance To Invoice"><?php echo number_format($row["BALANCE_TO_INVOICE"],$deci)?></td>
        <td nowrap="nowrap" style="text-align:right" title="Balance To Payment Received"><?php echo number_format($row["BALANCE_TO_PAYMENT_RECEIVED"],$deci)?></td>
        <td nowrap="nowrap" style="text-align:right" title="Last Dispatch Date"><?php echo $row["DISPATCH_DATE"]?></td>
        <td nowrap="nowrap" style="text-align:right" title="Credit Term"><?php echo $row["PAYMENT_TERM"]?></td>
		<?php  $start = 0;
		if($row["PAYMENT_TERM"]<$row["DIFF_DATES"])
			$agingCSS = "color:#F00;font-weight:bold";
		else
			$agingCSS = "";
			
		for($i=0;$i<count($payment_term_array);$i++){
		$diff_date 	= $row["DIFF_DATES"];
		$aging		= 0;		
		
				if($start <= $diff_date && $payment_term_array[$i] > $diff_date)
				{
					$aging = round($row["BALANCE_TO_PAYMENT_RECEIVED"],$deci);					
				}
				else 
					$aging = 0;
			if($agingCSS=="")
				$aging_array_normal[$payment_term_array[$i]] += $aging;	
			else
				$aging_array_due[$payment_term_array[$i]] += $aging;	
				
			$start = $payment_term_array[$i];	
			?>
        <td nowrap="nowrap" style="text-align:right;<?php echo $agingCSS?>" title="<?php echo $payment_term_array[$i]?>"><?php echo $aging==0?'&nbsp;':number_format($aging,$deci)?></td>
        <?php }?>
       </tr>
<?php
$tot_order_qty						+= round($row["ORDER_QTY"]);
$tot_dispatch_qty					+= round($row["DISPATCH_QTY"]);
$tot_dispatched_amount 				+= round($row["DISPATCH_AMOUNT"],$deci);
$tot_invoiced_amount 				+= round($row["INVOICE_AMOUNT"],$deci);
$tot_ex_received_amount				+= round($row["EX_RECEIVED_AMOUNT"],$deci);
$tot_payment_received_amount		+= round($row["PAYMENT_RECEIVED_AMOUNT"],$deci);
$tot_credit_note_amount				+= round($row["CREDIT_NOTE_AMOUNT"],$deci);
$tot_debit_note_amount				+= round($row["DEBIT_NOTE_AMOUNT"],$deci);
$tot_advance_not_settled		    += round($row["ADVANCE_NOT_SETTLED"],$deci);
$tot_advance_settled				+= round($row["ADVANCE_SETTLED"],$deci);
$tot_balance_to_invoice				+= round($row["BALANCE_TO_INVOICE"],$deci);
$tot_balance_to_payment_received	+= round($row["BALANCE_TO_PAYMENT_RECEIVED"],$deci);
}
?>
</tbody>
<tfoot>
	<tr style="font-weight:bold;text-align:right">
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap"><?php echo number_format($tot_order_qty)?></td>
        <td nowrap="nowrap"><?php echo number_format($tot_dispatch_qty)?></td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap"><?php echo number_format($tot_dispatched_amount,$deci)?></td>
        <td nowrap="nowrap"><?php echo number_format($tot_invoiced_amount,$deci)?></td>
        <td nowrap="nowrap"><?php echo number_format($tot_advance_not_settled,$deci)?></td>
        <td nowrap="nowrap"><?php echo number_format($tot_advance_settled,$deci)?></td>
        <td nowrap="nowrap"><?php echo number_format($tot_credit_note_amount,$deci)?></td>
        <td nowrap="nowrap"><?php echo number_format($tot_debit_note_amount,$deci)?></td>
        <td nowrap="nowrap"><?php echo number_format($tot_ex_received_amount,$deci)?></td>	
        <td nowrap="nowrap"><?php echo number_format($tot_payment_received_amount,$deci)?></td>
        <td nowrap="nowrap"><?php echo number_format($tot_balance_to_invoice,$deci)?></td>
        <td nowrap="nowrap"><?php echo number_format($tot_balance_to_payment_received,$deci)?></td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
    	<?php for($i=0;$i<count($payment_term_array);$i++){?>
        <td nowrap="nowrap"><?php echo number_format($aging_array_normal[$payment_term_array[$i]])?></td>
        <?php } ?>
      </tr>
      <tr style="color:#F00;font-weight:bold;text-align:right">
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <?php for($i=0;$i<count($payment_term_array);$i++){?>
        <td nowrap="nowrap"><?php echo number_format($aging_array_due[$payment_term_array[$i]])?></td>
        <?php } ?>
      </tr>
  </tfoot>  
  </table></td>
</tr>
            <tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>
</form>
<script type="text/javascript">
$(document).ready(function()
{
	$('.bordered tbody tr').click(function(){
		if($(this).attr('style')!='background-color:#82FF82')
		{
			$('.bordered tbody tr').attr('style','background-color:#FFFFFF');  
			   $(this).attr('style','background-color:#82FF82');     
		}                                             
	})
});
</script>
<?php
function GetMainDetails($cboOrder,$cboInvoice,$cboCreditPeriod,$cboCustomer,$txtCustomerPONo,$chkDateStatus,$txtDateFrom,$txtDateTo)
{
	global $db;
	global $deci;
	global $OrderComYear;
	global $payment_range_array;
	global $lastPaymentRange;
	global $companyId;
	global $sqlLimit;
	
	if($cboCustomer!="")
		$para1 .= "AND CU.intId =  '$cboCustomer'";
	
	
	$sql = "SELECT SUB_1.*,
				ROUND((DISPATCH_AMOUNT),$deci) - (INVOICE_AMOUNT)			AS BALANCE_TO_INVOICE,
				ROUND((DISPATCH_AMOUNT),$deci) - PAYMENT_RECEIVED_AMOUNT - ADVANCE_SETTLED - CREDIT_NOTE_AMOUNT AS BALANCE_TO_PAYMENT_RECEIVED
				
				
			/*	ROUND((DISPATCH_QTY * ORDER_RATE),$deci)							AS DISPATCH_AMOUNT,
				ROUND((DISPATCH_QTY * ORDER_RATE),$deci) - (INVOICE_AMOUNT)			AS BALANCE_TO_INVOICE,
				ROUND((DISPATCH_QTY * ORDER_RATE),$deci) - PAYMENT_RECEIVED_AMOUNT - ADVANCE_SETTLED - CREDIT_NOTE_AMOUNT AS BALANCE_TO_PAYMENT_RECEIVED */
			FROM
			(SELECT
			  OH.intCurrency													AS CURRENCY_ID,
			  CU.intId															AS CUSTOMER_ID,
			  CU.strName														AS CUSTOMER_NAME,
			  CL.strName														AS CUSTOMER_LOCATION,
			  P.strPlantName													AS PRODUCTION_PLANT,
			  P.strPlantName													AS PRODUCTION_LOCATION,
			  OH.dtDate 														AS ORDER_DATE,
			  (SELECT DATE(dtmdate) 
			  FROM ware_fabricreceivedheader AS FRH 
			  WHERE FRH.intOrderNo = OH.intOrderNo 
			  	AND FRH.intOrderYear = OH.intOrderYear 
				AND FRH.intStatus=1 
				order by FRH.dtmdate ASC LIMIT 1)								AS PSD_DATE,
			  CONCAT(OH.intOrderYear,' - ',OH.intOrderNo) 						AS ORDER_NO,
			  GROUP_CONCAT(DISTINCT OD.intSampleYear,' - ',OD.intSampleNo)		AS SAMPLE_NO,
			  (GROUP_CONCAT(DISTINCT mst_technique_groups.TECHNIQUE_GROUP_NAME)) AS TECH_GROUP,
			  OH.strCustomerPoNo 												AS CUSTOMER_PONO,
			  SUM(OD.intQty)          											AS ORDER_QTY,
			  ROUND(SUM(OD.intQty * OD.dblPrice),0) 							AS ORDER_AMOUNT,
			  ROUND(SUM(OD.intQty * OD.dblPrice)/SUM(OD.intQty),4) 				AS ORDER_RATE,			  
			  SUBSTRING(GROUP_CONCAT(DISTINCT OD.strGraphicNo),1,20)			AS GRAPHIC_NO,
			  FPT.strName        												AS PAYMENT_TERM,
			  
			  ROUND(COALESCE((SELECT SUM(FDD.dblGoodQty)
			  FROM ware_fabricdispatchdetails FDD 
			  INNER JOIN ware_fabricdispatchheader FDH 
				ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
				AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
			  WHERE FDH.intOrderNo = OH.intOrderNo 
			  	AND FDH.intOrderYear = OH.intOrderYear 
				AND FDH.intStatus = 1),0))										AS DISPATCH_QTY,
			  
			  ROUND(COALESCE((SELECT
				Sum(FDD.dblGoodQty*OD1.dblPrice)
				FROM
				ware_fabricdispatchdetails AS FDD
				INNER JOIN ware_fabricdispatchheader AS FDH ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
				INNER JOIN trn_orderdetails AS OD1 ON FDH.intOrderNo = OD1.intOrderNo AND FDH.intOrderYear = OD1.intOrderYear AND FDD.intSalesOrderId = OD1.intSalesOrderId
				WHERE
				FDH.intOrderNo = OH.intOrderNo AND
				FDH.intOrderYear = OH.intOrderYear AND
				FDH.intStatus = 1
				),0),2)										AS DISPATCH_AMOUNT,
			
			
			  SUBSTRING((SELECT GROUP_CONCAT(DISTINCT CSIH.INVOICE_NO)
			   FROM finance_customer_invoice_header CSIH  
			   WHERE CSIH.ORDER_NO = OH.intOrderNo
			   AND CSIH.ORDER_YEAR = OH.intOrderYear
			   AND CSIH.STATUS = 1),1,20)										AS INVOICE_NO,
   
				ROUND(COALESCE((SELECT SUM(VALUE) 
				FROM finance_customer_transaction FCT
				WHERE FCT.ORDER_NO = OH.intOrderNo
				AND FCT.ORDER_YEAR = OH.intOrderYear
				AND DOCUMENT_TYPE = 'INVOICE'),0),$deci)						AS INVOICE_AMOUNT,
				
				ROUND(COALESCE((SELECT SUM(VALUE) * -1
				FROM finance_customer_transaction FCT
				WHERE FCT.CUSTOMER_ID = OH.intCustomer
				AND FCT.CURRENCY_ID = OH.intCurrency
				AND FCT.INVOICE_NO IS NULL
				AND FCT.INVOICE_YEAR IS NULL
				AND DOCUMENT_TYPE = 'ADVANCE'),0),$deci)						AS ADVANCE_NOT_SETTLED,
				
				ROUND(COALESCE((SELECT SUM(VALUE) * -1
				FROM finance_customer_transaction FCT
				WHERE FCT.ORDER_NO = OH.intOrderNo
				AND FCT.ORDER_YEAR = OH.intOrderYear
				AND FCT.INVOICE_NO IS NOT NULL
				AND FCT.INVOICE_YEAR IS NOT NULL
				AND DOCUMENT_TYPE = 'ADVANCE'),0),$deci)						AS ADVANCE_SETTLED,
				
				ROUND(COALESCE((SELECT SUM(VALUE) * -1
				FROM finance_customer_transaction FCT
				WHERE FCT.ORDER_NO = OH.intOrderNo
				AND FCT.ORDER_YEAR = OH.intOrderYear
				AND FCT.DOCUMENT_TYPE = 'CREDIT'),0),$deci)						AS CREDIT_NOTE_AMOUNT,
				
				/*ROUND(COALESCE((SELECT SUM(VALUE)
				FROM finance_customer_transaction FCT
				WHERE FCT.ORDER_NO = OH.intOrderNo
				AND FCT.ORDER_YEAR = OH.intOrderYear
				AND FCT.DOCUMENT_TYPE = 'DEBIT'),0),$deci)						AS DEBIT_NOTE_AMOUNT,*/
				
				0																AS  DEBIT_NOTE_AMOUNT,
				
				ROUND(COALESCE((SELECT SUM(VALUE) * -1
				FROM finance_customer_transaction FCT
				WHERE FCT.ORDER_NO = OH.intOrderNo
				AND FCT.ORDER_YEAR = OH.intOrderYear
				AND (FCT.DOCUMENT_TYPE = 'PAYRECEIVE' OR FCT.DOCUMENT_TYPE = 'EXRECEIVE')),0),$deci)			AS PAYMENT_RECEIVED_AMOUNT,			
			  
				ROUND(COALESCE((SELECT SUM(VALUE) * -1
				FROM finance_customer_transaction FCT
				WHERE FCT.ORDER_NO = OH.intOrderNo
				AND FCT.ORDER_YEAR = OH.intOrderYear
				AND (FCT.DOCUMENT_TYPE = 'EXRECEIVE')),0),$deci)					AS EX_RECEIVED_AMOUNT,

			  (SELECT sys_users.strUserName FROM sys_users WHERE sys_users.intUserId = OH.intMarketer) AS MARKETER,
			   
			   (SELECT
				 DATE(FDHA.dtApprovedDate)
			   FROM ware_fabricdispatchdetails FDD
				 INNER JOIN ware_fabricdispatchheader FDH
				   ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
					 AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
				 INNER JOIN ware_fabricdispatchheader_approvedby FDHA
				   ON FDHA.intBulkDispatchNo = FDH.intBulkDispatchNo
					 AND FDHA.intYear = FDH.intBulkDispatchNoYear	 
			   WHERE FDH.intOrderNo = OH.intOrderNo
				   AND FDH.intOrderYear = OH.intOrderYear
				  -- AND FDD.intSalesOrderId = OD.intSalesOrderId
				   AND FDH.intStatus = 1 
			   ORDER BY DATE(FDHA.dtApprovedDate) DESC LIMIT 1)							AS DISPATCH_DATE,
			    
			   DATEDIFF(NOW(),(SELECT
				 DATE(FDH.dtmdate)
			   FROM ware_fabricdispatchdetails FDD
				 INNER JOIN ware_fabricdispatchheader FDH
				   ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
					 AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
			   WHERE FDH.intOrderNo = OH.intOrderNo
				   AND FDH.intOrderYear = OH.intOrderYear
				   AND FDD.intSalesOrderId = OD.intSalesOrderId
				   AND FDH.intStatus = 1 
			   ORDER BY DATE(FDH.dtmdate) DESC LIMIT 1)) 						AS DIFF_DATES , 
			   
			   ( SELECT
					GROUP_CONCAT(DISTINCT(mst_plant.strPlantName)) as plants
					FROM
					ware_fabricdispatchheader as FDH
					INNER JOIN mst_locations ON FDH.intCompanyId = mst_locations.intId
					INNER JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId 
					WHERE FDH.intOrderNo = OH.intOrderNo
				   AND FDH.intOrderYear = OH.intOrderYear
				   AND FDH.intStatus = 1 ) AS DISP_PLANTS,
				   
                ( SELECT
                GROUP_CONCAT(DISTINCT(mst_locations.strName)) as locations
                FROM
                ware_fabricdispatchheader as FDH
                INNER JOIN mst_locations ON FDH.intCompanyId = mst_locations.intId
                WHERE FDH.intOrderNo = OH.intOrderNo
               AND FDH.intOrderYear = OH.intOrderYear
               AND FDH.intStatus = 1 ) AS DISP_LOCATIONS
   
			FROM trn_orderheader OH
			INNER JOIN trn_orderdetails OD
				ON OD.intOrderNo = OH.intOrderNo
				AND OD.intOrderYear = OH.intOrderYear
			LEFT JOIN mst_technique_groups ON OD.TECHNIQUE_GROUP_ID = mst_technique_groups.TECHNIQUE_GROUP_ID
			INNER JOIN mst_customer CU
				ON CU.intId = OH.intCustomer
			INNER JOIN mst_financepaymentsterms FPT
    			ON FPT.intId = OH.intPaymentTerm
			INNER JOIN mst_locations L 
				  ON L.intId = OH.intLocationId
			INNER JOIN mst_plant P 
				  ON P.intPlantId = L.intPlant
			LEFT JOIN mst_customer_locations_header CL
				ON CL.intId = OH.intCustomerLocation
			 WHERE 1 = 1 
			 	AND OH.PAYMENT_RECEIVE_COMPLETED_FLAG = 0
			 	AND OH.intStatus <> '-2'
				AND OH.intOrderYear > $OrderComYear
				AND OH.intStatus <> '-10' 
				-- AND OH.intCustomer NOT IN (181)
			 	AND L.intCompanyId = '$companyId' 
				AND OH.strCustomerPoNo LIKE '%$txtCustomerPONo%' 
				$para1
			 $sqlLimit ";
				  		
	$sql .= "GROUP BY OH.intOrderYear,OH.intOrderNo
			 HAVING 1=1 ";
	
if($cboOrder=='1')
	$sql .= "AND round((round(DISPATCH_QTY)/round(ORDER_QTY))*100,2) >= 100 ";

if($cboOrder=='2')
	$sql .= "AND round((round(DISPATCH_QTY)/round(ORDER_QTY))*100,2) < 100 ";	
			 
if($cboCreditPeriod!="" && $cboCreditPeriod!="More")
	$sql .= "AND DIFF_DATES BETWEEN $payment_range_array[0] AND $payment_range_array[1] ";

if($cboCreditPeriod=="More")
	$sql .= "AND DIFF_DATES > $lastPaymentRange ";

if($cboCustomer!="")
	$sql .= "AND CUSTOMER_ID = $cboCustomer ";

if($chkDateStatus==1)
{
	if($txtDateFrom!='')
		$sql .= "AND DISPATCH_DATE >= '$txtDateFrom' ";
	
	if($txtDateTo!='')
		$sql .= "AND DISPATCH_DATE <= '$txtDateTo' ";
}	

	$sql .= "ORDER BY OH.intOrderYear,OH.intOrderNo) AS SUB_1
	HAVING 1=1 ";
	
if($cboInvoice == '1')
	$sql .= "AND BALANCE_TO_INVOICE > 0 ";

if($cboInvoice == '2')
    $sql .= "AND BALANCE_TO_PAYMENT_RECEIVED > 0 ";

	return $db->RunQuery($sql);
}

function GetPaymentTerm()
{
	global $db;
	$i = 0;
	$sql = "SELECT DISTINCT
			   strName
			FROM mst_financepaymentsterms
			WHERE strName <> 0 
			ORDER BY strName";
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$new_array[$i++] = $row["strName"];
	}
		$new_array[$i++] = "More";
	return $new_array;
}

function GetPaymentTerm_Range($cboCreditPeriod)
{
	global $db;
	
	$sql = "SELECT
			  strRange
			FROM mst_financepaymentsterms
			WHERE  intId = '$cboCreditPeriod'";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	return $row["strRange"];
}

function GetLastPaymentRange()
{
	global $db;
	$sql = "SELECT DISTINCT
			   strName	AS LAST
			FROM mst_financepaymentsterms
			ORDER BY strName DESC
			LIMIT 1";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	return $row["LAST"];
}
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
ini_set('max_execution_time',6000000);

include_once "class/tables/trn_orderheader.php";						$trn_orderheader			= new trn_orderheader($db);
include_once "class/tables/mst_customer.php";							$mst_customer				= new mst_customer($db);
include_once "class/tables/mst_plant.php";								$mst_plant					= new mst_plant($db);
include_once "class/tables/mst_brand.php";								$mst_brand					= new mst_brand($db);

$companyId			= $sessions->getCompanyId();
$deci				= 2;
$OrderComYear		= date('Y')-2 ;

if(!isset($_POST["cboOrder"]))
{
	$cboOrder		= 1;
	$sqlLimit		= 0;
}
else
{
	$cboOrder		= $_POST["cboOrder"];
	$sqlLimit		= "";
}

$cboCustomer		= (!isset($_REQUEST["cboCustomer"])?'':$_REQUEST["cboCustomer"]);
$txtCustomerPONo	= (!isset($_REQUEST["txtCustomerPONo"])?'':$_REQUEST["txtCustomerPONo"]);
$chkDateStatus		= (!isset($_REQUEST["chkDateFilter"])?0:1);
$txtDateFrom		= (!isset($_REQUEST["txtDateFrom"])?'':$_REQUEST["txtDateFrom"]);
$txtDateTo			= (!isset($_REQUEST["txtDateTo"])?'':$_REQUEST["txtDateTo"]);

?>
<title>Customer Invoice Aging Report</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
function ReloadSO()
{
	document.frmCustomerInvoiceAgingSOWiseReport.submit();	
}
function checkDateVal(obj)
{
	if($(obj).is(':checked'))
	{
		$('#frmCustomerInvoiceAgingSOWiseReport #txtDateFrom').attr('disabled',false);
		$('#frmCustomerInvoiceAgingSOWiseReport #txtDateTo').attr('disabled',false);
	}
	else
	{
		$('#frmCustomerInvoiceAgingSOWiseReport #txtDateFrom').attr('disabled',true);
		$('#frmCustomerInvoiceAgingSOWiseReport #txtDateTo').attr('disabled',true);
		$('#frmCustomerInvoiceAgingSOWiseReport #txtDateFrom').val('');
		$('#frmCustomerInvoiceAgingSOWiseReport #txtDateTo').val('');
	}
	
}
</script>

<style type="text/css">
#progress {
 width: 100px;   
 border: 1px solid black;
 position: relative;
 padding: 1px;
}

#percent {
 position: absolute;   
 left: 50%;
}

#bar {
 height: 15px;
 background-color: #0C0;
}
</style>

<form id="frmCustomerInvoiceAgingSOWiseReport" name="frmCustomerInvoiceAgingSOWiseReport" method="post">
<div align="center">
<div><strong>Customer Invoice Aging Report (Sales Order Wise)</strong></div>
	<table width="100%" border="0" align="center">
    <tr>
    	<td>
        	<table width="100%" border="0" class="normalfnt">
            	<tr>
                    <td width="218" style="text-align:center">Customer</td>
                    <td width="220" style="text-align:center">Order</td>
                    <td width="208" style="text-align:center">Customer PONo</td>
                    <td width="20" style="text-align:center">&nbsp;</td>
                    <td width="126" style="text-align:left">Dispatch Date from</td>
                    <td width="131" style="text-align:left">Dispatch Date to</td>
                    <td width="347" rowspan="2" valign="bottom" style="text-align:left"><a class="button white medium" onClick="ReloadSO();">Search</a></td>
                </tr>
                <tr>
                    <td style="text-align:left"><select style="width:200px" name="cboCustomer" id="cboCustomer" >
                    <?php
                    	echo $trn_orderheader->getCustomerCombo($cboCustomer,'trn_orderheader.intStatus = 1')	;
                    ?>
                    </select>
                    </td>
                    <td style="text-align:left"><select style="width:200px" name="cboOrder" id="cboOrder" >
                    	<option value="" <?php echo ($cboOrder==''?'selected=selected':'')?>>ALL</option>
                    	<option value="1" <?php echo ($cboOrder=='1'?'selected=selected':'')?>>Order Completed</option>
                    	<option value="2" <?php echo ($cboOrder=='2'?'selected=selected':'')?>>Pending</option>
                    </select>
                    </td>
                    <td style="text-align:left"><input type="text" style="width:80%" name="txtCustomerPONo" id="txtCustomerPONo" value="<?php echo $txtCustomerPONo?>"/></td>
                    <td style="text-align:center"><input name="chkDateFilter" id="chkDateFilter" type="checkbox" onclick="checkDateVal(this);" <?php echo($chkDateStatus==1?'checked="checked"':''); ?>/></td>
                    <td style="text-align:left"><input name="txtDateFrom" type="text" value="<?php echo $txtDateFrom; ?>" id="txtDateFrom" style="width:98px;" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo($chkDateStatus==0?'disabled="disabled"':'');?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                    <td style="text-align:left"><input name="txtDateTo" type="text" value="<?php echo $txtDateTo; ?>" id="txtDateTo" style="width:98px;" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo($chkDateStatus==0?'disabled="disabled"':'');?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td>
        	<table  border="0" cellpadding="0" cellspacing="0" class="bordered" width="100%" bgcolor="#FFFFFF">
            <thead>
                <tr valign="top">
                    <th>Customer Name</th>
                    <th>Customer Location</th>
                    <th>Production Plant</th>
                    <th>PONo</th>
                    <th>Order No</th>
                    <th>Sales Order</th>
                    <th>Brand</th>
                    <th>Order Date</th>
                    <th>PSD Date</th>
                    <th>Invoice No</th>
                    <th>Graphic No's</th>
                    <th>Order Qty</th>
                    <th>Dispatch Qty</th>
                    <th>Dispatch Prog</th>
                    <th>Rate</th>
                    <th>Dispatched Amount</th>
                    <th>Invoiced Amount</th>
                    <th>Last Dispatch Date</th>
            	</tr>
            </thead>
            <tbody>
            <?php
			$result			= getDetailData($cboOrder,$cboCustomer,$txtCustomerPONo,$chkDateStatus,$txtDateFrom,$txtDateTo);
			while($row = mysqli_fetch_array($result))
			{
				$progress 	= (round((round($row["DISPATCH_QTY"])/round($row["ORDER_QTY"]))*100,2)>100?100:round((round($row["DISPATCH_QTY"])/round($row["ORDER_QTY"]))*100,2));
				
				$mst_customer->set($row['CUSTOMER_ID']);
				$mst_plant->set($row['PLANTID']);
				$mst_brand->set($row['intBrand']);
				
				$customerName	= $mst_customer->getstrName();
				$plantName		= $mst_plant->getstrPlantName();
				$brandName		= $mst_brand->getstrName();
			?>	
                <tr>
                    <td nowrap="nowrap" title="Customer Name" style="text-align:left"><?php echo $customerName;?></td>
                    <td nowrap="nowrap" title="Customer Location" style="text-align:left"><?php echo $row['CUSTOMER_LOCATION'];?></td>
                    <td nowrap="nowrap" title="Production Plant" style="text-align:left"><?php echo $row['DISP_PLANTS'];?></td>
                   	<td nowrap="nowrap" title="Customer PO No" style="text-align:left"><?php echo $row['CUSTOMER_PONO'];?></td>
         			<td nowrap="nowrap" title="Order No" style="text-align:center"><?php echo $row['ORDER_NO'];?></td>
                    <td nowrap="nowrap" title="Sales Order No" style="text-align:left"><?php echo $row['SALESORDER'];?></td>
                    <td nowrap="nowrap" title="Order date" style="text-align:center"><span style="text-align:left"><?php echo $brandName;?></span></td>
                    <td nowrap="nowrap" title="Order date" style="text-align:center"><?php echo $row['ORDER_DATE'];?></td>
                    <td nowrap="nowrap" title="Order date" style="text-align:center"><?php echo $row['PSD_DATE'];?></td>
                    <td nowrap="nowrap" title="Invoice No" style="text-align:left"><?php echo $row['INVOICE_NO'];?></td>
                    <td nowrap="nowrap" title="Graphic No" style="text-align:left"><?php echo $row['GRAPHIC_NO'];?></td>
                    <td nowrap="nowrap" title="Order Qty" style="text-align:right"><?php echo $row['ORDER_QTY'];?></td>
                    <td nowrap="nowrap" title="Dispatched Qty" style="text-align:right"><?php echo $row['DISPATCH_QTY'];?></td>
                    <td nowrap="nowrap" title="Dispatched Prog"><div id="progress"><span id="percent"><?php echo $progress?>%</span><div id="bar" style="width:<?php echo $progress?>px"></div></div></td>
                    <td nowrap="nowrap" title="Order Rate" style="text-align:right"><?php echo $row['ORDER_RATE'];?></td>
                    <td nowrap="nowrap" title="Dispatched Amount" style="text-align:right"><?php echo $row['DISPATCH_AMOUNT'];?></td>
                    <td nowrap="nowrap" title="Invoiced Amount" style="text-align:right"><?php echo $row['INVOICE_AMOUNT'];?></td>
                    <td nowrap="nowrap" title="Last Dispatched Date" style="text-align:center"><?php echo $row['DISPATCH_DATE'];?></td>
                </tr>
                <?php
				
				$tot_order_qty						+= round($row["ORDER_QTY"]);
				$tot_dispatch_qty					+= round($row["DISPATCH_QTY"]);
				$tot_dispatched_amount 				+= round($row["DISPATCH_AMOUNT"],$deci);
				$tot_invoiced_amount 				+= round($row["INVOICE_AMOUNT"],$deci);
			}
			?>
            </tbody>
            <tfoot>
            	<tr style="font-weight:bold;text-align:right">
                    <td nowrap="nowrap" title="Customer Name" style="text-align:left">&nbsp;</td>
                    <td nowrap="nowrap" title="Customer Location" style="text-align:left">&nbsp;</td>
                    <td nowrap="nowrap" title="Production Plant" style="text-align:left">&nbsp;</td>
                   	<td nowrap="nowrap" title="Customer PO No" style="text-align:left">&nbsp;</td>
         			<td nowrap="nowrap" title="Order No" style="text-align:center">&nbsp;</td>
                    <td nowrap="nowrap" title="Sales Order No" style="text-align:left">&nbsp;</td>
                    <td nowrap="nowrap" title="Order date" style="text-align:center">&nbsp;</td>
                    <td nowrap="nowrap" title="Order date" style="text-align:center">&nbsp;</td>
                    <td nowrap="nowrap" title="Order date" style="text-align:center">&nbsp;</td>
                    <td nowrap="nowrap" title="Invoice No" style="text-align:left">&nbsp;</td>
                    <td nowrap="nowrap" title="Graphic No" style="text-align:left">&nbsp;</td>
                    <td nowrap="nowrap" title="Order Qty" style="text-align:right"><?php echo $tot_order_qty;?></td>
                    <td nowrap="nowrap" title="Dispatched Qty" style="text-align:right"><?php echo $tot_dispatch_qty;?></td>
                    <td nowrap="nowrap" title="Dispatched Prog">&nbsp;</td>
                    <td nowrap="nowrap" title="Order Rate" style="text-align:right">&nbsp;</td>
                    <td nowrap="nowrap" title="Dispatched Amount" style="text-align:right"><?php echo $tot_dispatched_amount;?></td>
                    <td nowrap="nowrap" title="Invoiced Amount" style="text-align:right"><?php echo $tot_invoiced_amount;?></td>
                    <td nowrap="nowrap" title="Last Dispatched Date" style="text-align:center">&nbsp;</td>
                </tr>	
            </tfoot>
            </table>
        </td>
    </tr>
    <tr height="40">
    	<td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
    </tr>
    </table> 
</div>
</form>
<?php
function getDetailData($cboOrder,$cboCustomer,$txtCustomerPONo,$chkDateStatus,$txtDateFrom,$txtDateTo)
{
	global $db;
	global $companyId;
	global $sqlLimit;
	global $deci;
	global $OrderComYear;
	
	if($cboCustomer!="")
		$para .= "AND OH.intCustomer = '$cboCustomer' ";
	
	if($txtCustomerPONo!="")
		$para .= "AND OH.strCustomerPoNo LIKE '%$txtCustomerPONo%' ";
	
	$sql = "SELECT 
			OH.intCurrency										AS CURRENCY_ID,
			OH.intCustomer										AS CUSTOMER_ID,
			CL.strName											AS CUSTOMER_LOCATION,
			OH.dtDate 											AS ORDER_DATE,
			OD.dtPSD											AS PSD_DATE,
			CONCAT(OH.intOrderYear,' - ',OH.intOrderNo) 		AS ORDER_NO,
			OH.strCustomerPoNo 									AS CUSTOMER_PONO,
			OD.strSalesOrderNo									AS SALESORDER,
			OD.intSalesOrderId									AS SALESORDERID,
			L.intPlant											AS PLANTID,						
			OD.intQty        									AS ORDER_QTY,
			ROUND((OD.intQty * OD.dblPrice),0) 					AS ORDER_AMOUNT,
			ROUND((OD.intQty * OD.dblPrice)/(OD.intQty),4) 		AS ORDER_RATE,
			OD.strGraphicNo										AS GRAPHIC_NO,
			
			(SELECT
			DATE(FDH.dtmdate)
			FROM ware_fabricdispatchdetails FDD
			INNER JOIN ware_fabricdispatchheader FDH
			ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
			AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
			WHERE FDH.intOrderNo = OH.intOrderNo
			AND FDH.intOrderYear = OH.intOrderYear
			AND FDD.intSalesOrderId = OD.intSalesOrderId
			AND FDH.intStatus = 1 
			ORDER BY DATE(FDH.dtmdate) DESC LIMIT 1)							AS DISPATCH_DATE,
			
			ROUND(COALESCE((SELECT SUM(FDD.dblGoodQty)
			FROM ware_fabricdispatchdetails FDD 
			INNER JOIN ware_fabricdispatchheader FDH 
			ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
			AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
			WHERE FDH.intOrderNo = OH.intOrderNo 
			AND FDH.intOrderYear = OH.intOrderYear
			AND FDD.intSalesOrderId = OD.intSalesOrderId 
			AND FDH.intStatus = 1),0))											AS DISPATCH_QTY,
			
			ROUND(COALESCE((SELECT
			SUM(FDD.dblGoodQty*OD.dblPrice)
			FROM
			ware_fabricdispatchdetails AS FDD
			INNER JOIN ware_fabricdispatchheader AS FDH ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
			/*INNER JOIN trn_orderdetails AS OD1 ON FDH.intOrderNo = OD1.intOrderNo AND FDH.intOrderYear = OD1.intOrderYear AND FDD.intSalesOrderId = OD1.intSalesOrderId and OD.intSalesOrderId = OD1.intSalesOrderId */
			WHERE
			FDH.intOrderNo = OH.intOrderNo AND
			FDH.intOrderYear = OH.intOrderYear AND
			FDD.intSalesOrderId = OD.intSalesOrderId AND 
			FDH.intStatus = 1
			),0),$deci)																AS DISPATCH_AMOUNT,
					
			(SELECT DISTINCT GROUP_CONCAT(CSIH.INVOICE_NO)
			FROM finance_customer_invoice_header CSIH  
			INNER JOIN finance_customer_invoice_details CSID ON CSIH.SERIAL_NO = CSID.SERIAL_NO AND CSIH.SERIAL_YEAR = CSID.SERIAL_YEAR
			WHERE CSIH.ORDER_NO = OH.intOrderNo
			AND CSIH.ORDER_YEAR = OH.intOrderYear
			AND CSID.SALES_ORDER_ID = OD.intSalesOrderId
			AND CSIH.STATUS = 1) 												AS INVOICE_NO,
					
			ROUND(COALESCE((SELECT SUM(CSID.VALUE+CSID.TAX_VALUE)
			FROM finance_customer_invoice_header CSIH  
			INNER JOIN finance_customer_invoice_details CSID ON CSIH.SERIAL_NO = CSID.SERIAL_NO AND CSIH.SERIAL_YEAR = CSID.SERIAL_YEAR
			WHERE CSIH.ORDER_NO = OH.intOrderNo
			AND CSIH.ORDER_YEAR = OH.intOrderYear
			AND CSID.SALES_ORDER_ID = OD.intSalesOrderId
			AND CSIH.STATUS = 1),0),$deci)											AS INVOICE_AMOUNT ,
			trn_sampleinfomations.intBrand,
			  ( SELECT
					GROUP_CONCAT(DISTINCT(mst_plant.strPlantName)) as plants
					FROM
					ware_fabricdispatchheader as FDH
					INNER JOIN ware_fabricdispatchdetails AS FDD ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					INNER JOIN mst_locations ON FDH.intCompanyId = mst_locations.intId
					INNER JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId 
					WHERE FDH.intOrderNo = OH.intOrderNo
				   AND FDH.intOrderYear = OH.intOrderYear
				   AND FDD.intSalesOrderId = OD.intSalesOrderId
				   AND FDH.intStatus = 1 ) AS DISP_PLANTS 
			FROM
			trn_orderheader OH
			INNER JOIN trn_orderdetails OD ON OD.intOrderNo = OH.intOrderNo AND OH.intOrderYear = OD.intOrderYear
			INNER JOIN trn_sampleinfomations ON OD.intSampleNo = trn_sampleinfomations.intSampleNo AND OD.intSampleYear = trn_sampleinfomations.intSampleYear AND OD.intRevisionNo = trn_sampleinfomations.intRevisionNo
			INNER JOIN mst_locations L ON L.intId = OH.intLocationId
			LEFT JOIN mst_customer_locations_header CL ON CL.intId = OH.intCustomerLocation
			WHERE 1 = 1 
			AND OH.PAYMENT_RECEIVE_COMPLETED_FLAG = 0
			AND OH.intStatus <> '-2'
			/*AND OH.intStatus <> '-10'*/ //commented as per Manoj's request 2017-01-24
			AND L.intCompanyId = '$companyId' 
			AND OH.intOrderYear > $OrderComYear
			$para
			$sqlLimit
			GROUP BY OH.intOrderNo,OH.intOrderYear,OD.intSalesOrderId
			HAVING 1=1 ";
	
	if($cboOrder=='1')
		$sql .= "AND round((round(DISPATCH_QTY)/round(ORDER_QTY))*100,2) >= 100 ";
	
	if($cboOrder=='2')
		$sql .= "AND round((round(DISPATCH_QTY)/round(ORDER_QTY))*100,2) < 100 ";	
	
	if($chkDateStatus==1)
	{
		if($txtDateFrom!='')
			$sql .= "AND DISPATCH_DATE >= '$txtDateFrom' ";
		
		if($txtDateTo!='')
			$sql .= "AND DISPATCH_DATE <= '$txtDateTo' ";
	}
	//echo $sql;
	
	return $db->RunQuery($sql);
}
?>
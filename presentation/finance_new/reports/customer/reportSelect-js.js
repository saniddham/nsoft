var basePath	= "presentation/finance_new/reports/customer/";
var pub_Id		= 0;

$(document).ready(function(){
	pub_Id		= 0;
//BEGIN - GL REPORTS {
	$('#tblGLReportHeader').die('click').live('click',function(){
		headShowHide('GLReportHeader','divGLReportDetail');
	});
	$('#tblGLReportDetail thead').die('click').live('click',function(){	
		bodyShowHide('divGLReportDetail','GLReportHeader');	
	});
//END 	- GL REPORTS }

//BEGIN - OpenPO REPORTS {
	$('#tblOpenPOHeader').die('click').live('click',function(){
		headShowHide('OpenPOHeader','divOpenPODetail');
	});
	$('#tblOpenPODetail thead').die('click').live('click',function(){	
		bodyShowHide('divOpenPODetail','OpenPOHeader');	
	});
//END 	- OpenPO REPORTS }


$('#butReport').die('click').live('click',ViewReport);
$("#frmOpenPODetail #cboCustomerPO").die('change').live('change',loadCusLocation);
$("#frmOpenPODetail #butReportPO").die('click').live('click',excelReport);
$("#frmOpenPODetail #butNewPO").die('click').live('click',clearAll);

});
function headShowHide(headerId,bodyDivId)
{
	$('#divGLReportDetail').slideUp(1000);
	$('#divOpenPODetail').slideUp(1000);
	$('#divIncomeStatement').slideUp(1000);
	$('#divBalanceSheet').slideUp(1000);
	
	$('#GLReportHeader').fadeIn(1500);
	$('#OpenPOHeader').fadeIn(1500);
	$('#IncomeStatement').fadeIn(1500);
	$('#BalanceSheet').fadeIn(1500);
	
	if(headerId!='' && bodyDivId!='')
	{
		$('#'+headerId).fadeOut(1000);
		$('#'+bodyDivId).slideDown(1000);
	}
}

function bodyShowHide(bodyDivId,headerId)
{
	$('#'+bodyDivId).slideUp(1000);
	$('#'+headerId).fadeIn(1500);
}

function ViewReport()
{
	var reportName = $('#cboType').val();
	var url  = basePath+reportName+'?';
	    url += "CustId="+$('#cboCustomer').val();
	window.open(url,reportName);
}
function loadCusLocation()
{
	var url				= basePath+"openPOreport/rptOpenPO_db.php?requestType=loadCusLocation&customerId="+$(this).val();
	var httpObj 		= $.ajax({url:url,type:'POST',async:false})
	
	$('#frmOpenPODetail #cboCusLocation').html(httpObj.responseText);
}
function excelReport()
{
	var customer 	 	= $('#frmOpenPODetail #cboCustomerPO').val();
	var cusLocation 	= $('#frmOpenPODetail #cboCusLocation').val();
	var PONo  	 		= $('#frmOpenPODetail #txtPONo').val();
	var orderYear  	 	= $('#frmOpenPODetail #txtOrderNo').val();
	var orderNo  	 	= $('#frmOpenPODetail #txtOrderYear').val();
	var type  	 		= $('#frmOpenPODetail #cboType').val();
	var company  	 	= $('#frmOpenPODetail #cboPOCompany').val();
	var paymentCom  	= ($('#frmOpenPODetail #chkPaymentComplete').attr('checked')?1:0);

	window.open(basePath+'openPOreport/openPOreport-xlsx.php?customer='+customer+'&cusLocation='+cusLocation+'&PONo='+PONo+'&orderYear='+orderYear+'&orderNo='+orderNo+'&type='+type+'&company='+company+'&paymentCom='+paymentCom);	
}
function clearAll()
{
	$('#frmOpenPODetail')[0].reset();
}


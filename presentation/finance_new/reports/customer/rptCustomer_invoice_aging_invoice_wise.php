<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
ini_set('max_execution_time',6000000);

include_once "class/tables/trn_orderheader.php";						$trn_orderheader			= new trn_orderheader($db);
include_once "class/tables/mst_customer.php";							$mst_customer				= new mst_customer($db);
include_once "class/tables/mst_plant.php";								$mst_plant					= new mst_plant($db);
include_once "class/tables/mst_brand.php";								$mst_brand					= new mst_brand($db);

$companyId			= $sessions->getCompanyId();
$deci				= 2;
//$OrderComYear		= date('Y')-2 ;
$OrderComYear		= date('Y')-3 ;//2017-01-26 dinusha view 2 yrs before data

if(!isset($_POST["cboOrder"]))
{
	$cboOrder		= 1;
	$sqlLimit		= 0;
}
else
{
	$cboOrder		= $_POST["cboOrder"];
	$sqlLimit		= "";
}

$cboCustomer		= (!isset($_REQUEST["cboCustomer"])?'':$_REQUEST["cboCustomer"]);
$txtCustomerPONo	= (!isset($_REQUEST["txtCustomerPONo"])?'':$_REQUEST["txtCustomerPONo"]);
$chkDateStatus		= (!isset($_REQUEST["chkDateFilter"])?0:1);
$txtDateFrom		= (!isset($_REQUEST["txtDateFrom"])?'':$_REQUEST["txtDateFrom"]);
$txtDateTo			= (!isset($_REQUEST["txtDateTo"])?'':$_REQUEST["txtDateTo"]);
$RdoDateCheck		= (!isset($_REQUEST["rdoDateCheck"])?'':$_REQUEST["rdoDateCheck"]);

?>
<title>Customer Invoice Aging Report</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
function ReloadSO()
{
	document.frmCustomerInvoiceAgingINVWiseReport.submit();	
}
function checkDateVal(obj)
{
	if($(obj).is(':checked'))
	{
		$('#frmCustomerInvoiceAgingINVWiseReport #txtDateFrom').attr('disabled',false);
		$('#frmCustomerInvoiceAgingINVWiseReport #txtDateTo').attr('disabled',false);
	}
	else
	{
		$('#frmCustomerInvoiceAgingINVWiseReport #txtDateFrom').attr('disabled',true);
		$('#frmCustomerInvoiceAgingINVWiseReport #txtDateTo').attr('disabled',true);
		$('#frmCustomerInvoiceAgingINVWiseReport #txtDateFrom').val('');
		$('#frmCustomerInvoiceAgingINVWiseReport #txtDateTo').val('');
	}
	
}
</script>
<style type="text/css">
#progress {
 width: 100px;   
 border: 1px solid black;
 position: relative;
 padding: 1px;
}

#percent {
 position: absolute;   
 left: 50%;
}

#bar {
 height: 15px;
 background-color: #0C0;
}
</style>
<form id="frmCustomerInvoiceAgingINVWiseReport" name="frmCustomerInvoiceAgingINVWiseReport" method="post">
<div align="center">
<div><strong>Customer Invoice Aging Report (Invoice Wise)</strong></div>
	<table width="100%" border="0" align="center">
    <tr>
    	<td>
        	<table width="100%" border="0" class="normalfnt">
            	<tr>
                    <td width="210" style="text-align:center">Customer</td>
                    <td width="211" style="text-align:center">Order</td>
                    <td width="181" style="text-align:center">Customer PONo</td>
                    <td width="20" style="text-align:center">&nbsp;</td>
                    <td width="126" style="text-align:left">Date from</td>
                    <td width="126" style="text-align:left">Date to</td>
                    <td width="96" style="text-align:left">&nbsp;</td>
                    <td width="98" style="text-align:left">&nbsp;</td>
                    <td width="194" rowspan="2" valign="bottom" style="text-align:left"><a class="button white medium" onClick="ReloadSO();">Search</a></td>
                </tr>
                <tr>
                    <td style="text-align:left"><select style="width:200px" name="cboCustomer" id="cboCustomer" >
                    <?php
                    	echo $trn_orderheader->getCustomerCombo($cboCustomer,'trn_orderheader.intStatus = 1')	;
                    ?>
                    </select>
                    </td>
                    <td style="text-align:left"><select style="width:200px" name="cboOrder" id="cboOrder" >
                    	<option value="" <?php echo ($cboOrder==''?'selected=selected':'')?>>ALL</option>
                    	<option value="1" <?php echo ($cboOrder=='1'?'selected=selected':'')?>>Order Completed</option>
                    	<option value="2" <?php echo ($cboOrder=='2'?'selected=selected':'')?>>Pending</option>
                    </select>
                    </td>
                    <td style="text-align:left"><input type="text" style="width:80%" name="txtCustomerPONo" id="txtCustomerPONo" value="<?php echo $txtCustomerPONo?>"/></td>
                    <td style="text-align:center"><input name="chkDateFilter" id="chkDateFilter" type="checkbox" onclick="checkDateVal(this);" <?php echo($chkDateStatus==1?'checked="checked"':''); ?>/></td>
                    <td style="text-align:left"><input name="txtDateFrom" type="text" value="<?php echo $txtDateFrom; ?>" id="txtDateFrom" style="width:98px;" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo($chkDateStatus==0?'disabled="disabled"':'');?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                    <td style="text-align:left"><input name="txtDateTo" type="text" value="<?php echo $txtDateTo; ?>" id="txtDateTo" style="width:98px;" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php echo($chkDateStatus==0?'disabled="disabled"':'');?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"  onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                    <td width="96" style="text-align:left" valign="middle"><input id="rdoDispatched" name="rdoDateCheck" value="Dispatched" type="radio" <?php echo($RdoDateCheck=='Dispatched'?'checked="checked"':'');?> />&nbsp;Dispatched</td>
                    <td width="98" style="text-align:left"><input id="rdoInvoiced" name="rdoDateCheck" value="Invoiced" type="radio" <?php echo($RdoDateCheck=='Invoiced'?'checked="checked"':'');?> />&nbsp;Invoiced</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td>
        	<table  border="0" cellpadding="0" cellspacing="0" class="bordered" width="100%" bgcolor="#FFFFFF">
            <thead>
                <tr valign="top">
                    <th>Customer Name</th>
                    <th>Customer Location</th>
                    <th>Production Plant</th>
                    <th>PONo</th>
                    <th>Marketer</th>
                    <th>Order No</th>
                    <th>Sales Order</th>
                    <th>Brand</th>
                    <th>Technique Group</th>
                    <th>Order Date</th>
                    <th>PSD DATE</th>
                    <th>Invoice No</th>
                    <th>Graphic No's</th>
                    <th>Order Qty</th>
                    <th>Dispatch Qty</th>
                    <th>Dispatch Prog</th>
                    <th>Rate</th>
                    <th>Dispatched Amount</th>
                    <th>Invoiced Amount</th>
                    <th>Invoiced Date</th>
                    <th>Last Dispatch Date</th>
            	</tr>
            </thead>
            <tbody>
            <?php
			$tot_order_qty				= 0;
			$tot_dispatch_qty			= 0;		
			$tot_dispatched_amount 		= 0;		
			$tot_invoiced_amount 		= 0;
			$sel_sql=getDetailData_for_sql($cboOrder,$cboCustomer,$txtCustomerPONo,$chkDateStatus,$txtDateFrom,$txtDateTo,$RdoDateCheck);	
			$result			= getDetailData($cboOrder,$cboCustomer,$txtCustomerPONo,$chkDateStatus,$txtDateFrom,$txtDateTo,$RdoDateCheck);
			if(mysqli_num_rows($result)>0)
			{
				while($row = mysqli_fetch_array($result))
				{
					$progress 	= (round((round($row["DISPATCH_QTY"])/round($row["ORDER_QTY"]))*100,2)>100?100:round((round($row["DISPATCH_QTY"])/round($row["ORDER_QTY"]))*100,2));
					
					$salesOrderLen	= strlen($row['SALESORDER']);
					$salesOrder		= substr($row['SALESORDER'],0,25);
					
					$mst_customer->set($row['CUSTOMER_ID']);
					$mst_plant->set($row['PLANTID']);
					$mst_brand->set($row['intBrand']);
					
					$customerName	= $mst_customer->getstrName();
					$plantName		= $mst_plant->getstrPlantName();
					$brandName		= $mst_brand->getstrName();
					
					
				?>	
					<tr>
						<td nowrap="nowrap" title="Customer Name" style="text-align:left"><?php echo $customerName;?></td>
						<td nowrap="nowrap" title="Customer Location" style="text-align:left"><?php echo $row['CUSTOMER_LOCATION'];?></td>
						<td nowrap="nowrap" title="Production Plant" style="text-align:left"><?php echo $row['DISP_PLANTS'];?></td>
						<td nowrap="nowrap" title="Customer PO No" style="text-align:left"><?php echo $row['CUSTOMERPONO'];?></td>
						<td nowrap="nowrap" title="Marketer" style="text-align:left"><?php echo $row['MARKETER'];?></td>
						<td nowrap="nowrap" title="Order No" style="text-align:center"><?php echo $row['ORDER_NO'];?></td>
						<td nowrap="nowrap" title="Sales Order No" style="text-align:left"><?php echo ($salesOrderLen>25?$salesOrder.'...':$row['SALESORDER']);?></td>
						<td nowrap="nowrap" title="Brand" style="text-align:center"><span style="text-align:left"><?php echo $brandName;?></span></td>
                        <td nowrap="nowrap" title="Technique Group" style="text-align:left"><?php echo $row['technique_group'];?></td>
						<td nowrap="nowrap" title="Order date" style="text-align:center"><?php echo $row['ORDER_DATE'];?></td>
						<td nowrap="nowrap" title="Order date" style="text-align:center"><?php echo $row['PSD_DATE'];?></td>
						<td nowrap="nowrap" title="Invoice No" style="text-align:left"><?php echo $row['INVOICE_NO'];?></td>
						<td nowrap="nowrap" title="Graphic No" style="text-align:left"><?php echo $row['GRAPHIC_NO'];?></td>
						<td nowrap="nowrap" title="Order Qty" style="text-align:right"><?php echo $row['ORDER_QTY'];?></td>
						<td nowrap="nowrap" title="Dispatched Qty" style="text-align:right"><?php echo $row['DISPATCH_QTY'];?></td>
						<td nowrap="nowrap" title="Dispatched Prog"><div id="progress"><span id="percent"><?php echo $progress?>%</span><div id="bar" style="width:<?php echo $progress?>px"></div></div></td>
						<td nowrap="nowrap" title="Order Rate" style="text-align:right"><?php echo $row['ORDER_RATE'];?></td>
						<td nowrap="nowrap" title="Dispatched Amount" style="text-align:right"><?php echo $row['DISPATCH_AMOUNT'];?></td>
						<td nowrap="nowrap" title="Invoiced Amount" style="text-align:right"><?php echo $row['INVOICE_AMOUNT'];?></td>
						<td nowrap="nowrap" title="Invoiced Date" style="text-align:center"><?php echo $row['INVOICED_DATE'];?></td>
						<td nowrap="nowrap" title="Last Dispatched Date" style="text-align:center"><?php echo $row['DISPATCH_DATE'];?></td>
					</tr>
					<?php
					
					$tot_order_qty						+= round($row["ORDER_QTY"]);
					$tot_dispatch_qty					+= round($row["DISPATCH_QTY"]);
					$tot_dispatched_amount 				+= round($row["DISPATCH_AMOUNT"],$deci);
					$tot_invoiced_amount 				+= round($row["INVOICE_AMOUNT"],$deci);
				}
			}
			?>
            </tbody>
            <tfoot>
            	<tr style="font-weight:bold;text-align:right">
                    <td nowrap="nowrap" title="Customer Name" style="text-align:left">&nbsp;</td>
                    <td nowrap="nowrap" title="Customer Location" style="text-align:left">&nbsp;</td>
                    <td nowrap="nowrap" title="Production Plant" style="text-align:left">&nbsp;</td>
                   	<td nowrap="nowrap" title="Customer PO No" style="text-align:left">&nbsp;</td>
                   	<td nowrap="nowrap" title="Marketer" style="text-align:center">&nbsp;</td>
         			<td nowrap="nowrap" title="Order No" style="text-align:center">&nbsp;</td>
                    <td nowrap="nowrap" title="Sales Order No" style="text-align:left">&nbsp;</td>
                    <td nowrap="nowrap" title="Brand" style="text-align:center">&nbsp;</td>
                     <td nowrap="nowrap" title="Technique Group" style="text-align:center">&nbsp;</td>
                    <td nowrap="nowrap" title="Order date" style="text-align:center">&nbsp;</td>
                    <td nowrap="nowrap" title="Order date" style="text-align:center">&nbsp;</td>
                    <td nowrap="nowrap" title="Invoice No" style="text-align:left">&nbsp;</td>
                    <td nowrap="nowrap" title="Graphic No" style="text-align:left">&nbsp;</td>
                    <td nowrap="nowrap" title="Order Qty" style="text-align:right"><?php echo $tot_order_qty;?></td>
                    <td nowrap="nowrap" title="Dispatched Qty" style="text-align:right"><?php echo $tot_dispatch_qty;?></td>
                    <td nowrap="nowrap" title="Dispatched Prog">&nbsp;</td>
                    <td nowrap="nowrap" title="Order Rate" style="text-align:right">&nbsp;</td>
                    <td nowrap="nowrap" title="Dispatched Amount" style="text-align:right"><?php echo $tot_dispatched_amount;?></td>
                    <td nowrap="nowrap" title="Invoiced Amount" style="text-align:right"><?php echo $tot_invoiced_amount;?></td>
                    <td nowrap="nowrap" title="Last Dispatched Date" style="text-align:center">&nbsp;</td>
                    <td nowrap="nowrap" title="Last Dispatched Date" style="text-align:center">&nbsp;</td>
                </tr>	
            </tfoot>
            </table>
        </td>
    </tr>
    <tr style="display:none"><td><?php echo $sel_sql; ?></td></tr>
    <tr height="40">
    	<td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
    </tr>
    </table> 
</div>
</form>
<?php
function getDetailData($cboOrder,$cboCustomer,$txtCustomerPONo,$chkDateStatus,$txtDateFrom,$txtDateTo,$RdoDateCheck)
{
	global $db;
	global $companyId;
	global $sqlLimit;
	global $deci;
	global $OrderComYear;
	
	if($cboCustomer!="")
		$para .= "AND OH.intCustomer = '$cboCustomer' ";
	
	if($txtCustomerPONo!="")
		$para .= "AND OH.strCustomerPoNo LIKE '%$txtCustomerPONo%' ";
	
	$sql = "SELECT 
			OH.intCurrency						AS CURRENCY_ID,
			OH.intCustomer						AS CUSTOMER_ID,
			CL.strName							AS CUSTOMER_LOCATION,
			SU.strUserName						AS MARKETER , 
			OH.dtDate 							AS ORDER_DATE,
			CONCAT(OH.intOrderYear,' - ',OH.intOrderNo) 		AS ORDER_NO,
			GROUP_CONCAT(DISTINCT OD.strSalesOrderNo) 	AS SALESORDER,
			GROUP_CONCAT(DISTINCT OD.dtPSD) 	AS PSD_DATE,
			(
			SELECT GROUP_CONCAT(DISTINCT TECHNIQUE_GROUP_NAME)
			FROM mst_technique_groups
			WHERE
			TECHNIQUE_GROUP_ID = OD.TECHNIQUE_GROUP_ID
			) AS technique_group,
			OH.strCustomerPoNo					AS CUSTOMERPONO,
			L.intPlant							AS PLANTID,
			SUM(OD.intQty)        				AS ORDER_QTY,
			ROUND(SUM((OD.intQty * OD.dblPrice)),0) 		AS ORDER_AMOUNT,
			ROUND(SUM((OD.intQty * OD.dblPrice))/SUM((OD.intQty)),4) AS ORDER_RATE,
			OD.strGraphicNo						AS GRAPHIC_NO,
			FCIH.INVOICED_DATE					AS INVOICED_DATE,
			
				(SELECT
				DATE(FDH.dtmdate)
				FROM ware_fabricdispatchdetails FDD
				INNER JOIN ware_fabricdispatchheader FDH
				ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
				AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
				WHERE FDH.intOrderNo = FCIH.ORDER_NO
				AND FDH.intOrderYear = FCIH.ORDER_YEAR
				AND FDD.intSalesOrderId = FCID.SALES_ORDER_ID
				AND FDH.intStatus = 1 
				ORDER BY DATE(FDH.dtmdate) DESC LIMIT 1)							AS DISPATCH_DATE,
			
			ROUND(SUM(COALESCE((SELECT SUM(FDD.dblGoodQty)
						  FROM ware_fabricdispatchdetails FDD 
						  INNER JOIN ware_fabricdispatchheader FDH 
							ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
							AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
						  WHERE FDH.intOrderNo = FCIH.ORDER_NO
							AND FDH.intOrderYear = FCIH.ORDER_YEAR
							AND FDD.intSalesOrderId = FCID.SALES_ORDER_ID 
							AND FDH.intStatus = 1),$deci)))		AS DISPATCH_QTY,
			ROUND(SUM(COALESCE((SELECT
							SUM(FDD.dblGoodQty*OD1.dblPrice)
							FROM
							ware_fabricdispatchdetails AS FDD
							INNER JOIN ware_fabricdispatchheader AS FDH ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
							INNER JOIN trn_orderdetails AS OD1 ON FDH.intOrderNo = OD1.intOrderNo AND FDH.intOrderYear = OD1.intOrderYear AND FDD.intSalesOrderId = OD1.intSalesOrderId
							WHERE
							FDH.intOrderNo = FCIH.ORDER_NO AND
							FDH.intOrderYear = FCIH.ORDER_YEAR AND
							OD1.intSalesOrderId = FCID.SALES_ORDER_ID AND
							FDH.intStatus = 1
							),0)),$deci)					AS DISPATCH_AMOUNT,
							
							CONCAT(FCIH.SERIAL_YEAR,'-',FCIH.SERIAL_NO) AS INVOICE_NO,
							
							ROUND(COALESCE(SUM(FCID.VALUE+FCID.TAX_VALUE),$deci),2)	AS INVOICE_AMOUNT ,
			trn_sampleinfomations.intBrand,
			( SELECT
					GROUP_CONCAT(DISTINCT(mst_plant.strPlantName)) as plants
					FROM
					ware_fabricdispatchheader as FDH
					INNER JOIN ware_fabricdispatchdetails AS FDD ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					INNER JOIN mst_locations ON FDH.intCompanyId = mst_locations.intId
					INNER JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId 
					WHERE FDH.intOrderNo = FCIH.ORDER_NO
				   AND FDH.intOrderYear = FCIH.ORDER_YEAR
				   AND FDD.intSalesOrderId = OD.intSalesOrderId
				   AND FDH.intStatus = 1 ) AS DISP_PLANTS 
			FROM
			finance_customer_invoice_header FCIH
			INNER JOIN finance_customer_invoice_details FCID ON FCID.SERIAL_NO = FCIH.SERIAL_NO AND FCID.SERIAL_YEAR = FCIH.SERIAL_YEAR
			INNER JOIN (select * from trn_orderheader where trn_orderheader.PAYMENT_RECEIVE_COMPLETED_FLAG = 0
			AND trn_orderheader.intStatus <> '-2'  AND trn_orderheader.intOrderYear > '$OrderComYear') OH ON OH.intOrderNo = FCIH.ORDER_NO AND OH.intOrderYear = FCIH.ORDER_YEAR
			INNER JOIN trn_orderdetails OD ON OD.intOrderNo = FCIH.ORDER_NO AND OD.intOrderYear = FCIH.ORDER_YEAR AND OD.intSalesOrderId = FCID.SALES_ORDER_ID
			INNER JOIN trn_sampleinfomations ON OD.intSampleNo = trn_sampleinfomations.intSampleNo AND OD.intSampleYear = trn_sampleinfomations.intSampleYear AND OD.intRevisionNo = trn_sampleinfomations.intRevisionNo
			INNER JOIN mst_locations L ON L.intId = OH.intLocationId
			LEFT JOIN mst_customer_locations_header CL ON CL.intId = OH.intCustomerLocation
			INNER JOIN sys_users AS SU ON SU.intUserId = OH.intMarketer 
			INNER JOIN mst_customer MC ON MC.intId = OH.intCustomer
			WHERE 1 = 1 
			AND OH.PAYMENT_RECEIVE_COMPLETED_FLAG = 0
			AND OH.intStatus <> '-2'
			-- AND OH.intStatus <> '-10' /*requst to remove this by dinusha 2016-03-10*/
			AND FCIH.STATUS = 1
			AND L.intCompanyId = '$companyId' 
			AND OH.intOrderYear > $OrderComYear
			$para
			$sqlLimit
			GROUP BY FCIH.SERIAL_NO,FCIH.SERIAL_YEAR
			HAVING 1=1 ";
	
	if($cboOrder=='1')
		$sql .= "AND round((round(DISPATCH_QTY)/round(ORDER_QTY))*100,2) >= 100 ";
	
	if($cboOrder=='2')
		$sql .= "AND round((round(DISPATCH_QTY)/round(ORDER_QTY))*100,2) < 100 ";	
	
	if($chkDateStatus==1)
	{
		if($RdoDateCheck=='Dispatched')
		{
			if($txtDateFrom!='')
				$sql .= "AND DISPATCH_DATE >= '$txtDateFrom' ";
		
			if($txtDateTo!='')
				$sql .= "AND DISPATCH_DATE <= '$txtDateTo' ";
		}
		if($RdoDateCheck=='Invoiced')
		{
			if($txtDateFrom!='')
				$sql .= "AND INVOICED_DATE >= '$txtDateFrom' ";
		
			if($txtDateTo!='')
				$sql .= "AND INVOICED_DATE <= '$txtDateTo' ";
		}
		
	}
	
	$sql .= "ORDER BY MC.strName,OH.strCustomerPoNo ";
	return $db->RunQuery($sql);
}

function getDetailData_for_sql($cboOrder,$cboCustomer,$txtCustomerPONo,$chkDateStatus,$txtDateFrom,$txtDateTo,$RdoDateCheck)
{
	global $db;
	global $companyId;
	global $sqlLimit;
	global $deci;
	global $OrderComYear;
	
	if($cboCustomer!="")
		$para .= "AND OH.intCustomer = '$cboCustomer' ";
	
	if($txtCustomerPONo!="")
		$para .= "AND OH.strCustomerPoNo LIKE '%$txtCustomerPONo%' ";
	
	$sql = "SELECT 
			OH.intCurrency						AS CURRENCY_ID,
			OH.intCustomer						AS CUSTOMER_ID,
			CL.strName							AS CUSTOMER_LOCATION,
			SU.strUserName						AS MARKETER , 
			OH.dtDate 							AS ORDER_DATE,
			CONCAT(OH.intOrderYear,' - ',OH.intOrderNo) 		AS ORDER_NO,
			GROUP_CONCAT(DISTINCT OD.strSalesOrderNo) 	AS SALESORDER,
			(
			SELECT GROUP_CONCAT(DISTINCT TECHNIQUE_GROUP_NAME)
			FROM mst_technique_groups
			WHERE
			TECHNIQUE_GROUP_ID = OD.TECHNIQUE_GROUP_ID
			) AS technique_group,
			OH.strCustomerPoNo					AS CUSTOMERPONO,
			L.intPlant							AS PLANTID,
			SUM(OD.intQty)        				AS ORDER_QTY,
			ROUND(SUM((OD.intQty * OD.dblPrice)),0) 		AS ORDER_AMOUNT,
			ROUND(SUM((OD.intQty * OD.dblPrice))/SUM((OD.intQty)),4) AS ORDER_RATE,
			OD.strGraphicNo						AS GRAPHIC_NO,
			FCIH.INVOICED_DATE					AS INVOICED_DATE,
			
				(SELECT
				DATE(FDH.dtmdate)
				FROM ware_fabricdispatchdetails FDD
				INNER JOIN ware_fabricdispatchheader FDH
				ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
				AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
				WHERE FDH.intOrderNo = FCIH.ORDER_NO
				AND FDH.intOrderYear = FCIH.ORDER_YEAR
				AND FDD.intSalesOrderId = FCID.SALES_ORDER_ID
				AND FDH.intStatus = 1 
				ORDER BY DATE(FDH.dtmdate) DESC LIMIT 1)							AS DISPATCH_DATE,
			
			ROUND(SUM(COALESCE((SELECT SUM(FDD.dblGoodQty)
						  FROM ware_fabricdispatchdetails FDD 
						  INNER JOIN ware_fabricdispatchheader FDH 
							ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
							AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
						  WHERE FDH.intOrderNo = FCIH.ORDER_NO
							AND FDH.intOrderYear = FCIH.ORDER_YEAR
							AND FDD.intSalesOrderId = FCID.SALES_ORDER_ID 
							AND FDH.intStatus = 1),$deci)))		AS DISPATCH_QTY,
			ROUND(SUM(COALESCE((SELECT
							SUM(FDD.dblGoodQty*OD1.dblPrice)
							FROM
							ware_fabricdispatchdetails AS FDD
							INNER JOIN ware_fabricdispatchheader AS FDH ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
							INNER JOIN trn_orderdetails AS OD1 ON FDH.intOrderNo = OD1.intOrderNo AND FDH.intOrderYear = OD1.intOrderYear AND FDD.intSalesOrderId = OD1.intSalesOrderId
							WHERE
							FDH.intOrderNo = FCIH.ORDER_NO AND
							FDH.intOrderYear = FCIH.ORDER_YEAR AND
							OD1.intSalesOrderId = FCID.SALES_ORDER_ID AND
							FDH.intStatus = 1
							),0)),$deci)					AS DISPATCH_AMOUNT,
							
							CONCAT(FCIH.SERIAL_YEAR,'-',FCIH.SERIAL_NO) AS INVOICE_NO,
							
							ROUND(COALESCE(SUM(FCID.VALUE+FCID.TAX_VALUE),$deci),2)	AS INVOICE_AMOUNT ,
			trn_sampleinfomations.intBrand,
			( SELECT
					GROUP_CONCAT(DISTINCT(mst_plant.strPlantName)) as plants
					FROM
					ware_fabricdispatchheader as FDH
					INNER JOIN ware_fabricdispatchdetails AS FDD ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					INNER JOIN mst_locations ON FDH.intCompanyId = mst_locations.intId
					INNER JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId 
					WHERE FDH.intOrderNo = FCIH.ORDER_NO
				   AND FDH.intOrderYear = FCIH.ORDER_YEAR
				   AND FDD.intSalesOrderId = OD.intSalesOrderId
				   AND FDH.intStatus = 1 ) AS DISP_PLANTS 
			FROM
			finance_customer_invoice_header FCIH
			INNER JOIN finance_customer_invoice_details FCID ON FCID.SERIAL_NO = FCIH.SERIAL_NO AND FCID.SERIAL_YEAR = FCIH.SERIAL_YEAR
			INNER JOIN trn_orderheader OH ON OH.intOrderNo = FCIH.ORDER_NO AND OH.intOrderYear = FCIH.ORDER_YEAR
			INNER JOIN trn_orderdetails OD ON OD.intOrderNo = FCIH.ORDER_NO AND OD.intOrderYear = FCIH.ORDER_YEAR AND OD.intSalesOrderId = FCID.SALES_ORDER_ID
			INNER JOIN trn_sampleinfomations ON OD.intSampleNo = trn_sampleinfomations.intSampleNo AND OD.intSampleYear = trn_sampleinfomations.intSampleYear AND OD.intRevisionNo = trn_sampleinfomations.intRevisionNo
			INNER JOIN mst_locations L ON L.intId = OH.intLocationId
			LEFT JOIN mst_customer_locations_header CL ON CL.intId = OH.intCustomerLocation
			INNER JOIN sys_users AS SU ON SU.intUserId = OH.intMarketer 
			INNER JOIN mst_customer MC ON MC.intId = OH.intCustomer
			WHERE 1 = 1 
			AND OH.PAYMENT_RECEIVE_COMPLETED_FLAG = 0
			AND OH.intStatus <> '-2'
			-- AND OH.intStatus <> '-10' /*requst to remove this by dinusha 2016-03-10*/
			AND FCIH.STATUS = 1
			AND L.intCompanyId = '$companyId' 
			AND OH.intOrderYear > $OrderComYear
			$para
			$sqlLimit
			GROUP BY FCIH.SERIAL_NO,FCIH.SERIAL_YEAR
			HAVING 1=1 ";
	
	if($cboOrder=='1')
		$sql .= "AND round((round(DISPATCH_QTY)/round(ORDER_QTY))*100,2) >= 100 ";
	
	if($cboOrder=='2')
		$sql .= "AND round((round(DISPATCH_QTY)/round(ORDER_QTY))*100,2) < 100 ";	
	
	if($chkDateStatus==1)
	{
		if($RdoDateCheck=='Dispatched')
		{
			if($txtDateFrom!='')
				$sql .= "AND DISPATCH_DATE >= '$txtDateFrom' ";
		
			if($txtDateTo!='')
				$sql .= "AND DISPATCH_DATE <= '$txtDateTo' ";
		}
		if($RdoDateCheck=='Invoiced')
		{
			if($txtDateFrom!='')
				$sql .= "AND INVOICED_DATE >= '$txtDateFrom' ";
		
			if($txtDateTo!='')
				$sql .= "AND INVOICED_DATE <= '$txtDateTo' ";
		}
		
	}
	
	$sql .= "ORDER BY MC.strName,OH.strCustomerPoNo ";
	return $sql;
 }

?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId 				= $_SESSION["headCompanyId"];
$locationId				= $_SESSION["CompanyID"];
$target_currencyId		= $_REQUEST["CurrencyId"];
$accountTypeId			= $_REQUEST["FinanceType"];
$accountMainTypeId		= $_REQUEST["MainType"];
$accountSubTypeId		= $_REQUEST["SubType"];
$accountId				= $_REQUEST["ChartOfAccount"];
$dateFrom				= $_REQUEST["DateFrom"];
$dateTo					= $_REQUEST["DateTo"];
$target_currencyName	= GetTargetCurrencyName($target_currencyId);
?>

<title>Trial Balance Report</title>

<table width="800" border="0" align="center">
  <tr>
    <td align="center"><?php include 'reportHeader.php'?></td>
  </tr>
  <tr>
    <td class="reportHeader" align="center">Trial Balance [<?php echo $target_currencyName?>]</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" class="rptBordered">
        <thead>
          <tr>
            <th width="12%" rowspan="2">Ledger Code</th>
            <th width="68%" rowspan="2">Ledger Account</th>
            <th colspan="2">[<?php echo $target_currencyName?>]</th>
          </tr>
          <tr>
            <th width="10%">Dr</th>
            <th width="10%">Cr</th>
          </tr>
        </thead>
        <tbody>
          <?php $result = GetDetails($target_currencyId,$accountTypeId,$accountMainTypeId,$accountSubTypeId,$accountId,$companyId,$dateFrom,$dateTo);
		while($row = mysqli_fetch_array($result))
		{
			if($row["TRANS"]>0)
				$dedit	= $row["TRANS"];
			else
				$creit	= $row["TRANS"];
?>
          <tr>
            <td style="text-align:center"><?php echo $row["CHART_OF_ACCOUNT_CODE"]?></td>
            <td><?php echo $row["CHART_OF_ACCOUNT_NAME"]?></td>
            <td style="text-align:right"><?php echo number_format(abs($dedit),2)?></td>
            <td style="text-align:right"><?php echo number_format(abs($creit),2)?></td>
          </tr>
          <?php
		  $total_dr	+= round($dedit,2);
		  $total_cr	+= round($creit,2);
		  $dedit	= 0;
		  $creit	= 0;
		}
?>
        </tbody>
        <tfoot>
          <tr style="font-weight:bold">
            <td>&nbsp;</td>
            <td>TOTAL</td>
            <td style="text-align:right;border-top-style:groove;border-bottom-style:double"><?php echo number_format(abs($total_dr),2)?></td>
            <td style="text-align:right;border-top-style:groove;border-bottom-style:double"><?php echo number_format(abs($total_cr),2)?></td>
          </tr>
        </tfoot>
      </table></td>
  </tr>
</table>
<?php
function GetDetails($target_currencyId,$accountTypeId,$accountMainTypeId,$accountSubTypeId,$accountId,$companyId,$dateFrom,$dateTo)
{
	global $db;

/*	$sql = "SELECT
	          CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE) AS CHART_OF_ACCOUNT_CODE,
			  FCOA.CHART_OF_ACCOUNT_CODE			AS CHART_OF_ACCOUNT_CODE1,
			  FCOA.CHART_OF_ACCOUNT_NAME			AS CHART_OF_ACCOUNT_NAME,
			  IF(TRANSACTION_TYPE='D',AMOUNT,'')	AS DR,
			  IF(TRANSACTION_TYPE='C',AMOUNT,'') 	AS CR  			  
			FROM finance_transaction FT
			  INNER JOIN finance_mst_chartofaccount FCOA			  
				ON FCOA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID
			  INNER JOIN finance_mst_account_sub_type FMST 
			    ON FMST.SUB_TYPE_ID = FCOA.SUB_TYPE_ID
			  INNER JOIN finance_mst_account_main_type FMMT 
			    ON FMMT.MAIN_TYPE_ID = FMST.MAIN_TYPE_ID
			  INNER JOIN finance_mst_account_type FMT 
			    ON FMT.FINANCE_TYPE_ID = FMMT.FINANCE_TYPE_ID";*/
	
	if($accountTypeId != "")
		$sql_where .= "AND FMMT.FINANCE_TYPE_ID = $accountTypeId ";
	if($accountMainTypeId != "")
		$sql_where .= "AND FMST.MAIN_TYPE_ID = $accountMainTypeId ";
	if($accountSubTypeId != "")
		$sql_where .= "AND FCOA.SUB_TYPE_ID = $accountSubTypeId ";
	if($accountId != "")
		$sql_where .= "AND FT.CHART_OF_ACCOUNT_ID = $accountId ";
				
	$sql = "SELECT 
				SUB_T.CHART_OF_ACCOUNT_ID,
				SUB_T.CHART_OF_ACCOUNT_CODE,
				SUB_T.CHART_OF_ACCOUNT_NAME,
				SUB_T.DEBIT,
				SUB_T.CREDIT,
				SUB_T.DEBIT-SUB_T.CREDIT AS TRANS
			FROM
			(SELECT DISTINCT
			  FT.CHART_OF_ACCOUNT_ID,
			   CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE) AS CHART_OF_ACCOUNT_CODE,
			   FCOA.CHART_OF_ACCOUNT_NAME			AS CHART_OF_ACCOUNT_NAME,
			  -- ROUND(COALESCE((SELECT SUM(AMOUNT) FROM finance_transaction SUB_FT WHERE SUB_FT.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID AND TRANSACTION_TYPE = 'D'),0),2) AS DEBIT,
			  -- ROUND(COALESCE((SELECT SUM(AMOUNT) FROM finance_transaction SUB_FT WHERE SUB_FT.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID AND TRANSACTION_TYPE = 'C'),0),2) AS CREDIT
			  ROUND(COALESCE((SELECT SUM((AMOUNT/(SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(SUB_FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = FT.COMPANY_ID)) * (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = SUB_FT.CURRENCY_ID AND DATE(SUB_FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = FT.COMPANY_ID)) FROM finance_transaction SUB_FT WHERE SUB_FT.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID AND TRANSACTION_TYPE = 'D'),0),2) AS DEBIT,
			  ROUND(COALESCE((SELECT SUM((AMOUNT/(SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(SUB_FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = FT.COMPANY_ID)) * (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = SUB_FT.CURRENCY_ID AND DATE(SUB_FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = FT.COMPANY_ID)) FROM finance_transaction SUB_FT WHERE SUB_FT.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID AND TRANSACTION_TYPE = 'C'),0),2) AS CREDIT
			FROM finance_transaction FT
			INNER JOIN finance_mst_chartofaccount FCOA			  
			  ON FCOA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID
			INNER JOIN finance_mst_account_sub_type FMST 
			  ON FMST.SUB_TYPE_ID = FCOA.SUB_TYPE_ID
			INNER JOIN finance_mst_account_main_type FMMT 
			  ON FMMT.MAIN_TYPE_ID = FMST.MAIN_TYPE_ID
			INNER JOIN finance_mst_account_type FMT 
			  ON FMT.FINANCE_TYPE_ID = FMMT.FINANCE_TYPE_ID
			WHERE FT.COMPANY_ID = $companyId
				AND DATE(FT.LAST_MODIFIED_DATE) BETWEEN '$dateFrom' AND '$dateTo'
			  $sql_where
			)AS SUB_T";
	return $db->RunQuery($sql);
}

function GetTargetCurrencyName($target_currencyId)
{
	global $db;
	
	$sql = "SELECT strCode AS CODE FROM mst_financecurrency WHERE intId = $target_currencyId";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["CODE"];
}
?>

<?php
session_start();
$backwardseperator 	= "../../../../";
require_once "{$backwardseperator}dataAccess/Connector.php";
require_once "../../../../class/masterData/exchange_rate/cls_exchange_rate_get.php";
require_once '../../../../libraries/excel/Classes/PHPExcel.php';
require_once '../../../../libraries/excel/Classes/PHPExcel/IOFactory.php';

$session_company 	= $_SESSION['headCompanyId'];
$custId				= $_REQUEST["CustId"];
$termArray			= SetPaymentTermArray(); 
$currency			= 1;
error_reporting(E_ALL);

$file = 'rptCustomerOutstanding_customize_details.xls'; //LOAD TEMPARARY FILE

//if (!file_exists($file))
	//exit("Can't find $fileName");

$headerCSS = array('font' =>array('color' =>array('rgb' => '000000'),'bold' => true,),
					'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb'=>'E3E9F9'),),'alignment' => array('wrap'=> true,'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER),);
					 
$objPHPExcel 		= new PHPExcel();
$objExchangeRateGet	= new cls_exchange_rate_get($db);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,1,"Sceenline Company PVT LTD")->getStyle("A1:A1")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,2,"Debtor's Outstanding As at ".date('Y-M-d'))->getStyle("A2:A2")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,3,"All Amounts in USD")->getStyle("A3:A3")->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A5:Y5")->applyFromArray($headerCSS);
$x = 0;
$y = 5;
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"Company Name");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"Invoice No");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"Invoice Date");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"Due Date");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"Days");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"Customer");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"PONo");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"Style No");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"PO Date");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"Relavent Merchant");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"PO Qty");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"Average Rate");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"PO Value");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"Invoice Qty");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"Invoice Value");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"Received Value");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"Last Received Date");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"Advance Settled");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"Credit Note");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"Balance");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,"Current");
for($i=0;$i<count($termArray);$i++)
{
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,$termArray[$i]["RANGE"].' Days');
}
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,'More Days');

	$result = GetDetails($custId);
	//$objPHPExcel->getActiveSheet()->insertNewRowBefore(5,mysqli_num_rows($result)-1);
	while($row=mysqli_fetch_array($result))
	{
		$currCus			= $row["CUSTOMER_ID"];
		$x = 0;
		$sourceCurRate 		= $objExchangeRateGet->GetAllValues($row['CURRENCY_ID'],2,$row['INVOICE_DATE'],$session_company,'RunQuery');
		$targetCurRate  	= $objExchangeRateGet->GetAllValues($currency,2,$row['INVOICE_DATE'],$session_company,'RunQuery');
		$sourceCurRate		= $sourceCurRate["AVERAGE_RATE"];
		$targetCurRate		= $targetCurRate["AVERAGE_RATE"];
		
		if($currCus != $preCus && $booFirst)
		{
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,++$y,"Advance Amount");
			$advanceAmount	= GetAdvanceAmount($preCus);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$y,$advanceAmount);
		}
		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,++$y,$row["COMPANY_NAME"]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,$row["INVOICE_NO"]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,$row["INVOICE_DATE"]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,$row["DUE_DATE"]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,$row["DATE_DIFF"]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,$row["CUSTOMER_NAME"]);		
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,$row["CUSTOMER_PONO"]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,$row["STYLE_NO"]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,$row["ORDER_DATE"]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,$row["CONTACT_PERSON"]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,$row["ORDER_QTY"]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,$row["RATE"]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,round(($row["ORDER_VALUE"]/$targetCurRate)*$sourceCurRate,2));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,$row["INVOICE_QTY"]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,round(($row["INVOICE_VALUE"]/$targetCurRate)*$sourceCurRate,2));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,round(($row["RECEIVED_AMOUNT"]/$targetCurRate)*$sourceCurRate,2));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,$row["LAST_RECEIVED_DATE"]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,round(($row["ADVANCE_SETTLED"]/$targetCurRate)*$sourceCurRate,2));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,round(($row["CREDIT_NOTE_AMOUNT"]/$targetCurRate)*$sourceCurRate,2));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,round(($row["BALANCE"]/$targetCurRate)*$sourceCurRate,2));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,round(($row["CURRENT"]/$targetCurRate)*$sourceCurRate,2));
		
		for($i=0;$i<count($termArray);$i++)
		{
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,round(($row["DUE_".$termArray[$i]["NAME"]]/$targetCurRate)*$sourceCurRate,2));
		}
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($x++,$y,round(($row["DUE_MORE"]/$targetCurRate)*$sourceCurRate,2));
		
		$preCus			= $row["CUSTOMER_ID"];
		$booFirst		= true;		
	}
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,++$y,"Advance Amount");
		$advanceAmount	= GetAdvanceAmount($preCus);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$y,$advanceAmount);

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$file.'"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output'); 
echo 'done';
exit;

function GetDetails($custId)
{
	global $db;
	global $termArray;
	
	if($custId != "")
	$para = "AND CU.intId = $custId ";
	
	$sql = "SELECT
 T1.CURRENCY_ID,
 T1.COMPANY_NAME,
 T1.CUSTOMER_ID,
 CONCAT(T1.CUSTOMER_NAME,'-',T1.CUSTOMER_LOCATION_NAME)	AS CUSTOMER_NAME,
 T1.INVOICE_NO,
 T1.INVOICE_DATE,
 T1.PAYMENT_TERM,
 T1.DUE_DATE,
 T1.DATE_DIFF,
 T1.CUSTOMER_PONO,
 T1.STYLE_NO,
 T1.CONTACT_PERSON,
 T1.ORDER_QTY,
 T1.ORDER_VALUE,
 T1.ORDER_DATE,
 T1.RATE,
 T1.INVOICE_QTY,
 T1.INVOICE_VALUE,
 T1.ADVANCE_SETTLED,
 T1.CREDIT_NOTE_AMOUNT,
 T1.DEBIT_NOTE_AMOUNT,
 T1.RECEIVED_AMOUNT,
 T1.LAST_RECEIVED_DATE,
 (INVOICE_VALUE - ADVANCE_SETTLED - CREDIT_NOTE_AMOUNT - RECEIVED_AMOUNT + DEBIT_NOTE_AMOUNT)	AS BALANCE,
 
 ROUND(IF((DATE_DIFF < 0),((INVOICE_VALUE - ADVANCE_SETTLED - CREDIT_NOTE_AMOUNT - RECEIVED_AMOUNT + DEBIT_NOTE_AMOUNT)),0),2) AS CURRENT, ";
 
 for($i=0;$i<count($termArray);$i++)
 {
	 $a 	= explode('-',$termArray[$i]["RANGE"]);
	 $sql .= "ROUND(IF((DATE_DIFF >= ".$a[0]."  && DATE_DIFF <= ".$a[1]."),((INVOICE_VALUE - ADVANCE_SETTLED - CREDIT_NOTE_AMOUNT - RECEIVED_AMOUNT + DEBIT_NOTE_AMOUNT)),0),2) AS DUE_".$termArray[$i]["NAME"].",";
 }
 	$last	= $a[1]+1;
 	 $sql .= "ROUND(IF((DATE_DIFF >=".$last."),((INVOICE_VALUE - ADVANCE_SETTLED - CREDIT_NOTE_AMOUNT - RECEIVED_AMOUNT + DEBIT_NOTE_AMOUNT)),0),2) AS DUE_MORE ";
	 
	$sql .= "FROM(
	SELECT
	FCIH.CURRENCY_ID															AS CURRENCY_ID,
	CO.strName         															AS COMPANY_NAME,
	CU.intId         															AS CUSTOMER_ID,
	CU.strName         															AS CUSTOMER_NAME,
	CLH.strName            														AS CUSTOMER_LOCATION_NAME,
	FCIH.INVOICE_NO    															AS INVOICE_NO,
	FCIH.INVOICED_DATE 															AS INVOICE_DATE,
	FPT.strName       	 														AS PAYMENT_TERM,
	DATE_ADD(FCIH.INVOICED_DATE, INTERVAL FPT.strName DAY) 						AS DUE_DATE,
	DATEDIFF(NOW(),DATE_ADD(FCIH.INVOICED_DATE, INTERVAL FPT.strName DAY)) 		AS DATE_DIFF,
	OH.strCustomerPoNo															AS CUSTOMER_PONO,
	OH.strContactPerson															AS CONTACT_PERSON,
	ROUND(SUM(FCID.QTY)) 														AS INVOICE_QTY,
	ROUND(SUM(FCID.VALUE),2) 													AS INVOICE_VALUE,
	OH.dtDate																	AS ORDER_DATE,
	
	(SELECT AVG(OD.dblPrice)
	FROM trn_orderdetails OD
	WHERE OD.intOrderNo = OH.intOrderNo
		AND OD.intOrderYear = OH.intOrderYear)									AS RATE,
	
	(SELECT SUM(OD.intQty)
	FROM trn_orderdetails OD
	WHERE OD.intOrderNo = OH.intOrderNo
		AND OD.intOrderYear = OH.intOrderYear)									AS ORDER_QTY,
		
	(SELECT SUM(OD.intQty * OD.dblPrice)
	FROM trn_orderdetails OD
	WHERE OD.intOrderNo = OH.intOrderNo
		AND OD.intOrderYear = OH.intOrderYear)									AS ORDER_VALUE,
		
	(SELECT GROUP_CONCAT(OD.strStyleNo SEPARATOR ' / ')
	FROM trn_orderdetails OD
	WHERE OD.intOrderNo = OH.intOrderNo
		AND OD.intOrderYear = OH.intOrderYear)									AS STYLE_NO,
	
	ROUND(COALESCE((SELECT SUM(VALUE) * -1
	FROM finance_customer_transaction FCT
	WHERE FCT.INVOICE_NO = FCIH.SERIAL_NO 
	AND FCT.INVOICE_YEAR = FCIH.SERIAL_YEAR
	AND DOCUMENT_TYPE = 'ADVANCE'),0),2)										AS ADVANCE_SETTLED,

	ROUND(COALESCE((SELECT SUM(VALUE) * -1
	FROM finance_customer_transaction FCT
	WHERE FCT.INVOICE_NO = FCIH.SERIAL_NO 
	AND FCT.INVOICE_YEAR = FCIH.SERIAL_YEAR
	AND FCT.DOCUMENT_TYPE = 'CREDIT'),0),2)										AS CREDIT_NOTE_AMOUNT,	

	ROUND(COALESCE((SELECT SUM(VALUE)
	FROM finance_customer_transaction FCT
	WHERE FCT.INVOICE_NO = FCIH.SERIAL_NO 
	AND FCT.INVOICE_YEAR = FCIH.SERIAL_YEAR
	AND FCT.DOCUMENT_TYPE = 'DEBIT'),0),2)										AS DEBIT_NOTE_AMOUNT,

	ROUND(COALESCE((SELECT SUM(PAY_AMOUNT) 
	FROM finance_customer_pay_receive_details FCPRD 
	WHERE FCPRD.INVOICE_NO = FCIH.SERIAL_NO 
	AND FCPRD.INVOICE_YEAR = FCIH.SERIAL_YEAR ),0),2) 							AS RECEIVED_AMOUNT,
	
	(SELECT FCPRH.RECEIPT_DATE
	FROM finance_customer_pay_receive_details FCPRD 
	INNER JOIN finance_customer_pay_receive_header FCPRH
	  ON FCPRH.RECEIPT_NO = FCPRD.RECEIPT_NO
	  AND FCPRH.RECEIPT_YEAR = FCPRD.RECEIPT_YEAR
	WHERE FCPRD.INVOICE_NO = FCIH.SERIAL_NO 
	  AND FCPRD.INVOICE_YEAR = FCIH.SERIAL_YEAR 
	ORDER BY FCPRH.RECEIPT_DATE DESC 
	LIMIT 1) 																	AS LAST_RECEIVED_DATE

	FROM finance_customer_invoice_header FCIH
	INNER JOIN finance_customer_invoice_details FCID
	ON FCID.SERIAL_NO = FCIH.SERIAL_NO
	  AND FCID.SERIAL_YEAR = FCIH.SERIAL_YEAR
	INNER JOIN mst_customer CU
	  ON CU.intId = FCIH.CUSTOMER_ID
	INNER JOIN mst_companies CO
	  ON CO.intId = FCIH.COMPANY_ID
	INNER JOIN trn_orderheader OH
	  ON OH.intOrderNo = FCIH.ORDER_NO
	  AND OH.intOrderYear = FCIH.ORDER_YEAR
	INNER JOIN mst_customer_locations_header CLH
      ON CLH.intId = OH.intCustomerLocation 
	INNER JOIN mst_financepaymentsterms FPT
	  ON FPT.intId = OH.intPaymentTerm
	WHERE 1=1
	  $para
	GROUP BY FCIH.SERIAL_NO,FCIH.SERIAL_YEAR
	) AS T1
	ORDER BY T1.CUSTOMER_NAME";
	//die($sql);
	return $db->RunQuery($sql);
}

function SetPaymentTermArray()
{
	global $db;
	$array	= array();
	$i		= 0;
	$sql = "SELECT strName,strRange FROM mst_financepaymentsterms WHERE strName <> 0 ORDER BY strName";
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$array[$i]["NAME"] = $row["strName"];
		$array[$i]["RANGE"] = $row["strRange"];
		$i++;
	}
	return $array;
}

function GetAdvanceAmount($preCus)
{
	global $db;
	global $objExchangeRateGet;
	global $currency;
	global $session_company;
	
	$totalAmount	= 0;
	
	$sql = "SELECT FCT.CUSTOMER_ID,FCT.CURRENCY_ID,FCT.TRANSACTION_DATE_TIME,ABS(SUM(VALUE)) AS VALUE
			FROM finance_customer_transaction FCT
			WHERE FCT.CUSTOMER_ID = $preCus
			AND FCT.INVOICE_NO IS NULL
			AND FCT.INVOICE_YEAR IS NULL
			AND DOCUMENT_TYPE = 'ADVANCE'			
			GROUP BY FCT.CUSTOMER_ID,FCT.CURRENCY_ID,FCT.TRANSACTION_DATE_TIME";
	$result1 = $db->RunQuery($sql);
	while($row1 = mysqli_fetch_array($result1))
	{
		$sourceCurRate 		= $objExchangeRateGet->GetAllValues($row1['CURRENCY_ID'],2,$row1['TRANSACTION_DATE_TIME'],$session_company,'RunQuery');
		$targetCurRate  	= $objExchangeRateGet->GetAllValues($currency,2,$row1['TRANSACTION_DATE_TIME'],$session_company,'RunQuery');
		$sourceCurRate		= $sourceCurRate["AVERAGE_RATE"];
		$targetCurRate		= $targetCurRate["AVERAGE_RATE"];
		$totalAmount	   += round((($row1["VALUE"]/$targetCurRate)*$sourceCurRate),2);
	}
	return $totalAmount;
}
?>
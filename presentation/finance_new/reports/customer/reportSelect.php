<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId	  		= $_SESSION["CompanyID"];

//include "include/javascript.html";
?>

<title>Finance - Reports</title>

<!--<script type="text/javascript" src="presentation/finance_new/reports/customer/reportSelect-js.js"></script>-->

<div align="center">
  <div class="trans_layoutD">
    <div class="trans_text">Customer Reports</div>
    <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
      <tr id="GLReportHeader">
        <td><table align="left" width="100%" class="main_header_table" id="tblGLReportHeader" cellpadding="0" cellspacing="0">
            <tr>
              <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Outstanding Reports.</u></td>
            </tr>
          </table></td>
      </tr>
      
      <!-- ----------GL Details ------------------------- -->
      <tr>
        <td><form id="frmGLReportDetail" name="frmGLReportDetail">
            <div id="divGLReportDetail" class="clsdivGLReportDetail" style="width:100%;display:none">
              <table align="left" width="100%" class="main_table" id="tblGLReportDetail" border="0" cellpadding="0" cellspacing="0">
                <thead>
                  <tr>
                    <td colspan="7" style="text-align:left">Outstanding Reports Selection Criteria</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td width="21%" class="normalfnt" height="">Report Type</td>
                    <td width="79%" colspan="3" class="normalfnt"><select id="cboType" name="cboType" style="width:150px" class="validate[required]">
					<option value="rptCustomerOutstanding_details.php">Detail Report</option>
                    <option value="rptCustomerOutstanding_summary.php">Summary Report</option>
                     <option value="rptCustomerOutstanding_customize_details.php">Customize Report</option>
                    </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Customer</td>
                    <td colspan="3" class="normalfnt"><select name="cboCustomer" id="cboCustomer" style="width:320px">
                        <option value=""></option>
					<?php
                        $sql = "SELECT DISTINCT
                                  CU.intId   AS CUSTOMER_ID,
                                  CU.strName AS CUSTOMER_NAME
                                FROM finance_customer_invoice_header IH
                                  INNER JOIN mst_customer CU
                                    ON CU.intId = IH.CUSTOMER_ID
                                ORDER BY CU.strName";
                        $result = $db->RunQuery($sql);
                        while($row=mysqli_fetch_array($result))
                        {
                            echo "<option value=\"".$row["CUSTOMER_ID"]."\">".$row["CUSTOMER_NAME"]."</option>";
                        }
                    ?>
                      </select></td>
                  </tr>
                  <tr>
                    <td colspan="4" class="normalfnt"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                        <tr>
                          <td width="100%" style="text-align:center"><a class="button white medium" id="butReport" name="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
                        </tr>
                      </table></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </form></td>
      </tr>
      <!-- ----------------------------------------------------------- -->
      <tr id="OpenPOHeader">
        <td><table align="left" width="100%" class="main_header_table" id="tblOpenPOHeader" cellpadding="0" cellspacing="0">
            <tr>
              <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Open PO Reports.</u></td>
            </tr>
          </table></td>
      </tr>
      
      <!-- ----------Open PO Details ------------------------- -->
      <tr>
      <td><form id="frmOpenPODetail" name="frmOpenPODetail">
            <div id="divOpenPODetail" class="clsdivOpenPODetail" style="width:100%;display:none">
            <table align="left" width="100%" class="main_table" id="tblOpenPODetail" border="0" cellpadding="0" cellspacing="0">
            <thead>
                  <tr>
                    <td colspan="7" style="text-align:left">Open PO Reports Selection Criteria</td>
                  </tr>
            </thead>
           	<tbody>
            <tr>
        	
                	<td width="33%">Customer</td>
                    <td width="63%"><select style="width:300px;" name="cboCustomerPO" id="cboCustomerPO" >
                   	<option value=""></option>
                    <?php
						$sql = "SELECT DISTINCT
								CU.intId 		AS CUSTOMER_ID,
								CU.strName 		AS CUSTOMER_NAME
								FROM trn_orderheader OH 
								INNER JOIN mst_customer CU ON OH.intCustomer = CU.intId
								WHERE OH.intStatus = 1
								ORDER BY CU.strName ";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=".$row["CUSTOMER_ID"].">".$row["CUSTOMER_NAME"]."</option>";
						}
					?>
                    </select></td>
                </tr>
            	<tr class="normalfnt">
            	  <td>Customer Location</td>
            	  <td><select style="width:300px;" name="cboCusLocation" id="cboCusLocation" >
                  <option value=""></option>
					<?php
						$sql = "SELECT DISTINCT CLH.intId,CLH.strName
								FROM mst_customer_locations_header CLH
								INNER JOIN mst_customer_locations CL ON CL.intLocationId=CLH.intId
								WHERE 1=1
								ORDER BY CLH.strName";
						$result = $db->RunQuery($sql);
						
                    while($row = mysqli_fetch_array($result))
                    {
						echo "<option value=\"".$row["intId"]."\" >".$row["strName"]."</option>";
                    }
                    ?>
                    </select></td>
          	  </tr>
            	<tr class="normalfnt">
            	  <td>PO No</td>
            	  <td><input type="text" name="txtPONo" id="txtPONo" style="width:140px;" /></td>
          	  </tr>
            	<tr class="normalfnt">
            	  <td>Order Year/No</td>
            	  <td><input type="text" name="txtOrderNo" id="txtOrderNo" style="width:50px;" />&nbsp;<input type="text" name="txtOrderYear" id="txtOrderYear" style="width:85px;" /></td>
          	  </tr>
            	<tr class="normalfnt">
            	  <td>Type</td>
            	  <td><select style="width:140px;" name="cboType" id="cboType" >
                  <option value=""></option>
                  <option value="1">In Qty = 0</option>
                  <option value="2">Dispatch Qty(Good Qty) = 0</option>
                  <option value="3">Invoice Qty = 0</option>
          	    </select></td>
          	  </tr>
            	<tr class="normalfnt">
            	  <td>Company</td>
            	  <td><select style="width:300px;" name="cboPOCompany" id="cboPOCompany" >
            	    <option value=""></option>
            	    <?php
						$sql = "SELECT intId,strName
								FROM mst_companies
								WHERE intStatus = 1
								ORDER BY strName";
						$result = $db->RunQuery($sql);
						
                    while($row = mysqli_fetch_array($result))
                    {
						echo "<option value=\"".$row["intId"]."\" >".$row["strName"]."</option>";
                    }
                    ?>
          	    </select></td>
          	  </tr>
            	<tr class="normalfnt">
            	  <td>Payment Complete</td>
            	  <td><input type="checkbox" name="chkPaymentComplete" id="chkPaymentComplete" /></td>
          	  </tr>
            
        <tr>
            <td colspan="4">
            	<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" >
                <tr>
                    <td align="center" ><a class="button white medium" id="butNewPO">New</a><a class="button white medium" id="butReportPO">Report</a><a href="main.php" class="button white medium" id="butClose">Close</a>
                    </td>
                </tr>
            </table>
            </td>
        </tr>
        </tbody>
    </table>
            </div>
           </form>
      </td>
      </tr>
       <!-- ----------------------------------------------------------- -->
    </table>
  </div>
</div>
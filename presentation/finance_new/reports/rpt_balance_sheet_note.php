<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
ini_set('max_execution_time',60000);
$gross_profit_loss		= 0;
$companyId 				= $_SESSION["headCompanyId"];
$locationId				= $_SESSION["CompanyID"];
$target_currencyId		= $_REQUEST["CurrencyId"];
$dateFrom				= $_REQUEST["DateFrom"];
$dateTo					= $_REQUEST["DateTo"];
$before_array			= json_decode($_REQUEST["Before_Array"],true);
$after_array			= json_decode($_REQUEST["After_Array"],true);
$bgColor_pre			= "#FCF2F3";
$bgColor_after			= "#EFFFF3";

$incStatReportId		= 961;
$GLSumReportId			= 968;
?>
<head>
<title>Balance Sheet Note</title>
</head>

<body>
<table width="800" border="0" align="center">
  <tr>
    <td align="center"><?php include 'reportHeader.php'?></td>
  </tr>
  <tr>
    <td class="reportHeader" align="center">Balance Sheet [<?php echo GetCurrencyCode($target_currencyId);?>]</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" class="rptBordered">
        <thead>
          <tr>
            <th width="12%">Chart Of Account Code</th>
            <th width="68%">Chart Of Account Name</th>
            <?php foreach($before_array as $before_loop){?>
            <th width="10"><?php echo $before_loop["DATE_FROM"]."<br/>".$before_loop["DATE_TO"]?></th>
            <?php } ?>
            <th width="10%"><?php echo $dateFrom."<br/>".$dateTo?></th>
            <?php foreach($after_array as $after_loop){?>
            <th width="10" ><?php echo $after_loop["DATE_FROM"]."<br/>".$after_loop["DATE_TO"]?></th>
            <?php } ?>
          </tr>
        </thead>
        <tbody>
          <!--BEGIN - GROSS PROFIT/(LOSS) { -->
          <?php $result = GrossProfit_part01('1,2,3,4,5');
		while($row = mysqli_fetch_array($result))
		{
			if($row["CHART_OF_ACCOUNT_NAME"]=='Retained Earnings'){
				$amount = GetReturnEarnings($dateFrom,$dateTo);
				$url	= $row["CHART_OF_ACCOUNT_NAME"];
			}elseif($row["CHART_OF_ACCOUNT_NAME"]=='Profit/(Loss) for the Period'){
				$amount = GetProfitLoss($dateFrom,$dateTo);
				$url	= $row["CHART_OF_ACCOUNT_NAME"];
				$url 	= "<a href=\"?q=".$incStatReportId."&DateFrom=".$dateFrom."&DateTo=".$dateTo."&CurrencyId=".$target_currencyId."\" target=\"rpt_income_statement.php\">".$row["CHART_OF_ACCOUNT_NAME"]."</a>";
			}else{
				$amount = round($row["AMOUNT"],2);
				$url 	= "<a href=\"?q=".$GLSumReportId."&fromDate=".$dateFrom."&toDate=".$dateTo."&currency=".$target_currencyId."&financeType=".$row["FINANCE_TYPE_ID"]."&mainCateId=".$row["MAIN_TYPE_ID"]."&subCatId=".$row["SUB_TYPE_ID"]."&chartOfAccId=".$row["CHART_OF_ACCOUNT_ID"]."\" target=\"rptGeneralLedgerSummary.php\">".$row["CHART_OF_ACCOUNT_NAME"]."</a>";
			}
?>
          <?php if(!isset($main_type) || $main_type!=$row["SUB_TYPE_NAME"]){
				
		?>
        <?php if(isset($main_type)){ ?>
          <tr>
            <td style="text-align:center">&nbsp;</td>
            <td><b>TOTAL</b></td>
            <?php $loop = 0; foreach($before_array as $before_loop){?>
            <td style="text-align:right;border-top-style:groove;border-bottom-style:double;background-color:<?php echo $bgColor_pre?>"><b><?php echo number_format($total_current_assets1[$loop++],2)?></b></td>
            <?php $total_current_assets1=array();} ?>
            <td style="text-align:right;border-top-style:groove;border-bottom-style:double"><b><?php echo number_format($total,2)?></b></td>
            <?php $loop = 0; foreach($after_array as $after_loop){?>
            <td style="text-align:right;border-top-style:groove;border-bottom-style:double;background-color:<?php echo $bgColor_after?>"><b><?php echo number_format($total_current_assets2[$loop++],2)?></b></td>
            <?php $total_current_assets2=array();} ?>
          </tr>
          <tr>
          	<td colspan="3">&nbsp;</td>
          </tr>
          <?php } ?>
          <tr bgcolor="#ECECFF">
            <td style="text-align:center"><?php echo $row["SUB_TYPE_CODE"]?></td>
            <td><a name="<?php echo $row["SUB_TYPE_NAME"]?>"><?php echo $row["SUB_TYPE_NAME"]?></a></td>
            <?php foreach($before_array as $before_loop){?>
            <td >&nbsp;</td>
            <?php } ?>
            <td>&nbsp;</td>
            <?php foreach($after_array as $after_loop){?>
            <td >&nbsp;</td>
            <?php } ?>
          </tr>
          <?php $total = 0;
		  		$main_type = $row["SUB_TYPE_NAME"];
		  } ?>
          <tr>
            <td style="text-align:center"><?php echo $row["CHART_OF_ACCOUNT_CODE"]?></td>
            <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $url?></td>
            <?php 
			$loop = 0; 
			foreach($before_array as $before_loop)
			{
				if($row["CHART_OF_ACCOUNT_NAME"]=='Retained Earnings')
					$periodAmount = GetReturnEarnings($before_loop["DATE_FROM"],$before_loop["DATE_TO"]);
				elseif($row["CHART_OF_ACCOUNT_NAME"]=='Profit/(Loss) for the Period')
					$periodAmount = GetProfitLoss($before_loop["DATE_FROM"],$before_loop["DATE_TO"]);
				else
					$periodAmount = GetOtherAmount($before_loop["DATE_FROM"],$before_loop["DATE_TO"],$row["CHART_OF_ACCOUNT_ID"]);				
				
				$total_current_assets1[$loop++]	+= round($periodAmount,2);
			?>
            <td style="text-align:right;background-color:<?php echo $bgColor_pre?>"><?php echo number_format($periodAmount,2) ?></td>
            <?php 
			} 
			?>
            <td style="text-align:right"><?php echo number_format($amount,2)?></td>
            <?php 
			$loop = 0; 
			foreach($after_array as $after_loop)
			{
				if($row["CHART_OF_ACCOUNT_NAME"]=='Retained Earnings')
					$periodAmount = GetReturnEarnings($after_loop["DATE_FROM"],$after_loop["DATE_TO"]);
				elseif($row["CHART_OF_ACCOUNT_NAME"]=='Profit/(Loss) for the Period')
					$periodAmount = GetProfitLoss($after_loop["DATE_FROM"],$after_loop["DATE_TO"]);
				else
					$periodAmount = GetOtherAmount($after_loop["DATE_FROM"],$after_loop["DATE_TO"],$row["CHART_OF_ACCOUNT_ID"]);
				
				$total_current_assets2[$loop++]	+= round($periodAmount,2);
			?>
            <td style="text-align:right;background-color:<?php echo $bgColor_after?>"><?php echo number_format($periodAmount,2) ?></td>
            <?php 
			} 
			?>
          </tr>
          <?php
		  $total += round($amount,2);
		}
?>
		   <tr>
            <td style="text-align:center">&nbsp;</td>
            <td><b>TOTAL</b></td>
            <?php $loop = 0; foreach($before_array as $before_loop){?>
            <td style="text-align:right;border-top-style:groove;border-bottom-style:double;background-color:<?php echo $bgColor_pre?>"><b><?php echo number_format($total_current_assets1[$loop++],2)?></b></td>
            <?php } ?>
            <td style="text-align:right;border-top-style:groove;border-bottom-style:double"><b><?php echo number_format($total,2)?></b></td>
            <?php $loop = 0; foreach($after_array as $after_loop){?>
            <td style="text-align:right;border-top-style:groove;border-bottom-style:double;background-color:<?php echo $bgColor_after?>"><b><?php echo number_format($total_current_assets2[$loop++],2)?></b></td>
            <?php } ?>
          </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
<?php
function GrossProfit_part01($para)
{
	global $db;
	global $companyId;
	global $target_currencyId;
	global $dateFrom;
	global $dateTo;

	$sql = "SELECT 
			  FINANCE_TYPE_ID,
			  FINANCE_TYPE_NAME,
			  MAIN_TYPE_ID,
			  MAIN_TYPE_NAME,
			  SUB_TYPE_ID																	AS SUB_TYPE_ID,
			  CONCAT(FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE) 						AS SUB_TYPE_CODE,
			  SUB_TYPE_NAME																	AS SUB_TYPE_NAME,
			  CHART_OF_ACCOUNT_ID															AS CHART_OF_ACCOUNT_ID,
			  CONCAT(FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE,CHART_OF_ACCOUNT_CODE) 	AS CHART_OF_ACCOUNT_CODE,
			  CHART_OF_ACCOUNT_NAME 														AS CHART_OF_ACCOUNT_NAME,
			  SUM(IF(TRANSACTION_ADD_TYPE='D',DEBIT-CREDIT,CREDIT-DEBIT))					AS AMOUNT
			FROM 
			(SELECT 
			  TRANSACTION_ADD_TYPE,
			  FINANCE_TYPE_ID,
			  FINANCE_TYPE_CODE,
			  FINANCE_TYPE_NAME,
			  MAIN_TYPE_ID,
			  MAIN_TYPE_CODE,
			  MAIN_TYPE_NAME,
			  SUB_TYPE_ID,
			  SUB_TYPE_CODE,
			  SUB_TYPE_NAME,
			  CHART_OF_ACCOUNT_ID,
			  CHART_OF_ACCOUNT_CODE,
			  CHART_OF_ACCOUNT_NAME,
			  DEBIT,
			  CREDIT
			FROM 
			(SELECT
				T.TRANSACTION_ADD_TYPE,
				T.FINANCE_TYPE_ID,
				T.FINANCE_TYPE_CODE,
				T.FINANCE_TYPE_NAME,
				MT.MAIN_TYPE_ID,
				MT.MAIN_TYPE_CODE,
				MT.MAIN_TYPE_NAME,
				ST.SUB_TYPE_ID,
				ST.SUB_TYPE_CODE,
				ST.SUB_TYPE_NAME,
				COA.CHART_OF_ACCOUNT_ID,
				COA.CHART_OF_ACCOUNT_CODE,
				COA.CHART_OF_ACCOUNT_NAME,
				
				(SELECT SUM(IF(TRANSACTION_TYPE='D',COALESCE(FT.AMOUNT,0)/((SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER 
						WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1))*((SELECT SUB_ER.dblExcAvgRate 
						FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID 
						AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1)),0)) FROM finance_transaction FT  
						WHERE COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID AND FT.COMPANY_ID = $companyId AND DATE(FT.LAST_MODIFIED_DATE) <= '$dateTo') AS DEBIT,
						
				(SELECT SUM(IF(TRANSACTION_TYPE='C',COALESCE(FT.AMOUNT,0)/((SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER 
				WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1))*((SELECT SUB_ER.dblExcAvgRate 
				FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID 
				AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1)),0)) FROM finance_transaction FT  
				WHERE COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID AND FT.COMPANY_ID = $companyId AND DATE(FT.LAST_MODIFIED_DATE) <= '$dateTo') AS CREDIT
			FROM finance_mst_account_type T
			INNER JOIN finance_mst_account_main_type MT ON T.FINANCE_TYPE_ID = MT.FINANCE_TYPE_ID
			INNER JOIN finance_mst_account_sub_type ST ON MT.MAIN_TYPE_ID = ST.MAIN_TYPE_ID
			INNER JOIN finance_mst_chartofaccount COA ON ST.SUB_TYPE_ID = COA.SUB_TYPE_ID
			/*LEFT JOIN finance_transaction FT ON COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID*/
			WHERE MT.MAIN_TYPE_ID IN ($para)
			/*AND (FT.COMPANY_ID = $companyId	OR FT.COMPANY_ID IS NULL)
			AND (DATE(FT.LAST_MODIFIED_DATE) <= '$dateTo' OR DATE(FT.LAST_MODIFIED_DATE) IS NULL)*/
			)AS SUB_1) AS SUB_2
			GROUP BY FINANCE_TYPE_ID,MAIN_TYPE_ID,SUB_TYPE_ID,CHART_OF_ACCOUNT_ID
			ORDER BY FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE,CHART_OF_ACCOUNT_CODE";
			
	return $db->RunQuery($sql);
}

function GetReturnEarnings($dateFrom,$dateTo)
{
	$gross_profit_loss1 = GetReturnEarningsAmount('6,7',$dateFrom,$dateTo);
	$gross_profit_loss2 = GetReturnEarningsAmount('8,9',$dateFrom,$dateTo);
	return $gross_profit_loss1 - $gross_profit_loss2;
}

function GetProfitLoss($dateFrom,$dateTo)
{
	$gross_profit_loss1 = GetProfitLossAmount('6,7',$dateFrom,$dateTo);
	$gross_profit_loss2 = GetProfitLossAmount('8,9',$dateFrom,$dateTo);
	return $gross_profit_loss1 - $gross_profit_loss2;
}

function GetReturnEarningsAmount($para,$dateFrom,$dateTo)
{
	global $db;
	global $companyId;
	global $target_currencyId;
	//global $dateFrom;
	//global $dateTo;
	
	/*$sql = "SELECT 
				T.FINANCE_TYPE_NAME												AS FINANCE_TYPE_NAME,
				MT.MAIN_TYPE_NAME												AS MAIN_TYPE_NAME,
				ST.SUB_TYPE_NAME												AS SUB_TYPE_NAME,
				CONCAT(T.FINANCE_TYPE_CODE,MT.MAIN_TYPE_CODE,ST.SUB_TYPE_CODE) 	AS SUB_TYPE_CODE,
				ROUND(COALESCE(SUM(FT.AMOUNT),0),2) 							AS AMOUNT1,
				ROUND(COALESCE(SUM(FT.AMOUNT / (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId) * (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId)),0),2) AS AMOUNT
			FROM finance_mst_account_main_type MT 
			INNER JOIN finance_mst_account_type T ON T.FINANCE_TYPE_ID = MT.FINANCE_TYPE_ID
			INNER JOIN finance_mst_account_sub_type ST ON ST.MAIN_TYPE_ID = MT.MAIN_TYPE_ID
			INNER JOIN finance_mst_chartofaccount COA ON COA.SUB_TYPE_ID = ST.SUB_TYPE_ID
			LEFT JOIN finance_transaction FT ON FT.CHART_OF_ACCOUNT_ID = COA.CHART_OF_ACCOUNT_ID
			WHERE MT.MAIN_TYPE_ID IN ($para)
				AND (DATE(FT.LAST_MODIFIED_DATE) < '$dateFrom' OR DATE(FT.LAST_MODIFIED_DATE) IS NULL)
			GROUP BY ST.MAIN_TYPE_ID,ST.SUB_TYPE_ID";*/
			
	$sql = "SELECT 
			FINANCE_TYPE_NAME,
			MAIN_TYPE_NAME,
			CONCAT(FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE) AS SUB_TYPE_CODE,
			SUB_TYPE_NAME,
			
			SUM(IF(TRANSACTION_ADD_TYPE='D',DEBIT-CREDIT,CREDIT-DEBIT))AS AMOUNT
			FROM 
			(SELECT 
				TRANSACTION_ADD_TYPE,
				FINANCE_TYPE_ID,
				FINANCE_TYPE_CODE,
				FINANCE_TYPE_NAME,
				MAIN_TYPE_ID,
				MAIN_TYPE_CODE,
				MAIN_TYPE_NAME,
				SUB_TYPE_ID,
				SUB_TYPE_CODE,
				SUB_TYPE_NAME,
				DEBIT,
				CREDIT
			FROM 
				(SELECT
					T.TRANSACTION_ADD_TYPE,
					T.FINANCE_TYPE_ID,
					T.FINANCE_TYPE_CODE,
					T.FINANCE_TYPE_NAME,
					MT.MAIN_TYPE_ID,
					MT.MAIN_TYPE_CODE,
					MT.MAIN_TYPE_NAME,
					ST.SUB_TYPE_ID,
					ST.SUB_TYPE_CODE,
					ST.SUB_TYPE_NAME,
					COA.CHART_OF_ACCOUNT_ID,
					COA.CHART_OF_ACCOUNT_CODE,
					COA.CHART_OF_ACCOUNT_NAME,
					
					(SELECT SUM(IF(TRANSACTION_TYPE='D',COALESCE(FT.AMOUNT,0)/((SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER 
					WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1))*((SELECT SUB_ER.dblExcAvgRate 
					FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID 
					AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1)),0)) FROM finance_transaction FT  
					WHERE COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID AND FT.COMPANY_ID = $companyId AND DATE(FT.LAST_MODIFIED_DATE) <= '$dateTo') AS DEBIT,
					
					(SELECT SUM(IF(TRANSACTION_TYPE='C',COALESCE(FT.AMOUNT,0)/((SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER 
					WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1))*((SELECT SUB_ER.dblExcAvgRate 
					FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID 
					AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1)),0)) FROM finance_transaction FT  
					WHERE COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID AND FT.COMPANY_ID = $companyId AND DATE(FT.LAST_MODIFIED_DATE) <= '$dateTo') AS CREDIT 	
								
				FROM finance_mst_account_type T
				INNER JOIN finance_mst_account_main_type MT ON T.FINANCE_TYPE_ID = MT.FINANCE_TYPE_ID
				INNER JOIN finance_mst_account_sub_type ST ON MT.MAIN_TYPE_ID = ST.MAIN_TYPE_ID
				INNER JOIN finance_mst_chartofaccount COA ON ST.SUB_TYPE_ID = COA.SUB_TYPE_ID
				/*LEFT JOIN finance_transaction FT ON COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID*/
				WHERE MT.MAIN_TYPE_ID IN ($para)
					/*AND (FT.COMPANY_ID = $companyId	OR FT.COMPANY_ID IS NULL)
				AND (DATE(FT.LAST_MODIFIED_DATE) < '$dateFrom' OR DATE(FT.LAST_MODIFIED_DATE) IS NULL)*/
				)AS SUB_1) AS SUB_2
				GROUP BY FINANCE_TYPE_ID,MAIN_TYPE_ID,SUB_TYPE_ID
				ORDER BY FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE";
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$gross_profit_loss += round($row["AMOUNT"],2);
	}
	return $gross_profit_loss;
}

function GetProfitLossAmount($para,$dateFrom,$dateTo)
{
	global $db;
	global $companyId;
	global $target_currencyId;
	//global $dateFrom;
	//global $dateTo;

	$sql = "SELECT 
			FINANCE_TYPE_NAME,
			MAIN_TYPE_NAME,
			CONCAT(FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE) AS SUB_TYPE_CODE,
			SUB_TYPE_NAME,
			
			SUM(IF(TRANSACTION_ADD_TYPE='D',DEBIT-CREDIT,CREDIT-DEBIT))AS AMOUNT
			FROM 
			(SELECT 
				TRANSACTION_ADD_TYPE,
				FINANCE_TYPE_ID,
				FINANCE_TYPE_CODE,
				FINANCE_TYPE_NAME,
				MAIN_TYPE_ID,
				MAIN_TYPE_CODE,
				MAIN_TYPE_NAME,
				SUB_TYPE_ID,
				SUB_TYPE_CODE,
				SUB_TYPE_NAME,
				DEBIT,
				CREDIT
				FROM 
				(SELECT
					T.TRANSACTION_ADD_TYPE,
					T.FINANCE_TYPE_ID,
					T.FINANCE_TYPE_CODE,
					T.FINANCE_TYPE_NAME,
					MT.MAIN_TYPE_ID,
					MT.MAIN_TYPE_CODE,
					MT.MAIN_TYPE_NAME,
					ST.SUB_TYPE_ID,
					ST.SUB_TYPE_CODE,
					ST.SUB_TYPE_NAME,
					COA.CHART_OF_ACCOUNT_ID,
					COA.CHART_OF_ACCOUNT_CODE,
					COA.CHART_OF_ACCOUNT_NAME,
					
					(SELECT SUM(IF(TRANSACTION_TYPE='D',COALESCE(FT.AMOUNT,0)/((SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER 
					WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1))*((SELECT SUB_ER.dblExcAvgRate 
					FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID 
					AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1)),0)) FROM finance_transaction FT  
					WHERE COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID AND FT.COMPANY_ID = $companyId AND DATE(FT.LAST_MODIFIED_DATE) BETWEEN '$dateFrom' AND '$dateTo') AS DEBIT,
					
					(SELECT SUM(IF(TRANSACTION_TYPE='C',COALESCE(FT.AMOUNT,0)/((SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER 
					WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1))*((SELECT SUB_ER.dblExcAvgRate 
					FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID 
					AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1)),0)) FROM finance_transaction FT  
					WHERE COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID AND FT.COMPANY_ID = $companyId AND DATE(FT.LAST_MODIFIED_DATE) BETWEEN '$dateFrom' AND '$dateTo') AS CREDIT 
					
				FROM finance_mst_account_type T
				INNER JOIN finance_mst_account_main_type MT ON T.FINANCE_TYPE_ID = MT.FINANCE_TYPE_ID
				INNER JOIN finance_mst_account_sub_type ST ON MT.MAIN_TYPE_ID = ST.MAIN_TYPE_ID
				INNER JOIN finance_mst_chartofaccount COA ON ST.SUB_TYPE_ID = COA.SUB_TYPE_ID
				/*LEFT JOIN finance_transaction FT ON COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID*/
				WHERE MT.MAIN_TYPE_ID IN ($para)
				/*AND (FT.COMPANY_ID = $companyId	OR FT.COMPANY_ID IS NULL)
				AND (DATE(FT.LAST_MODIFIED_DATE) BETWEEN '$dateFrom' AND '$dateTo' OR DATE(FT.LAST_MODIFIED_DATE) IS NULL)*/
				)AS SUB_1) AS SUB_2
				GROUP BY FINANCE_TYPE_ID,MAIN_TYPE_ID,SUB_TYPE_ID
				ORDER BY FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE";
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$gross_profit_loss += round($row["AMOUNT"],2);
	}
	return $gross_profit_loss;
}

function GetOtherAmount($dateFrom,$dateTo,$para)
{
	global $db;
	global $companyId;
	global $target_currencyId;
	
	$sql = "SELECT 
				COALESCE(SUM(IF(TRANSACTION_ADD_TYPE='D',DEBIT-CREDIT,CREDIT-DEBIT)),0)AS AMOUNT
				FROM 
				(SELECT 
			  		TRANSACTION_ADD_TYPE,
					FINANCE_TYPE_ID,
					FINANCE_TYPE_CODE,
			  		FINANCE_TYPE_NAME,
					MAIN_TYPE_ID,
					MAIN_TYPE_CODE,
			  		MAIN_TYPE_NAME,
			  		SUB_TYPE_ID,
			  		SUB_TYPE_CODE,
			  		SUB_TYPE_NAME,
					CHART_OF_ACCOUNT_ID,
					DEBIT,
			  		CREDIT
					FROM 
					(SELECT
						T.TRANSACTION_ADD_TYPE,
						T.FINANCE_TYPE_ID,
						T.FINANCE_TYPE_CODE,
						T.FINANCE_TYPE_NAME,
						MT.MAIN_TYPE_ID,
						MT.MAIN_TYPE_CODE,
						MT.MAIN_TYPE_NAME,
						ST.SUB_TYPE_ID,
						ST.SUB_TYPE_CODE,
						ST.SUB_TYPE_NAME,
						COA.CHART_OF_ACCOUNT_ID,
						COA.CHART_OF_ACCOUNT_CODE,
						COA.CHART_OF_ACCOUNT_NAME,
						
						(SELECT SUM(IF(TRANSACTION_TYPE='D',COALESCE(FT.AMOUNT,0)/((SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER 
					WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1))*((SELECT SUB_ER.dblExcAvgRate 
					FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID 
					AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1)),0)) FROM finance_transaction FT  
					WHERE COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID AND FT.COMPANY_ID = $companyId AND DATE(FT.LAST_MODIFIED_DATE) <= '$dateTo') AS DEBIT,

					(SELECT SUM(IF(TRANSACTION_TYPE='C',COALESCE(FT.AMOUNT,0)/((SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER 
					WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1))*((SELECT SUB_ER.dblExcAvgRate 
					FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID 
					AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1)),0)) FROM finance_transaction FT  
					WHERE COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID AND FT.COMPANY_ID = $companyId AND DATE(FT.LAST_MODIFIED_DATE) <= '$dateTo') AS CREDIT
					
					FROM finance_mst_account_type T
					INNER JOIN finance_mst_account_main_type MT ON T.FINANCE_TYPE_ID = MT.FINANCE_TYPE_ID
					INNER JOIN finance_mst_account_sub_type ST ON MT.MAIN_TYPE_ID = ST.MAIN_TYPE_ID
					INNER JOIN finance_mst_chartofaccount COA ON ST.SUB_TYPE_ID = COA.SUB_TYPE_ID
					/*INNER JOIN finance_transaction FT ON COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID*/
					WHERE COA.CHART_OF_ACCOUNT_ID IN ($para)
						/*AND (FT.COMPANY_ID = $companyId	OR FT.COMPANY_ID IS NULL)
						AND DATE(FT.LAST_MODIFIED_DATE) <= '$dateTo' */
					)AS SUB_1) AS SUB_2
					GROUP BY FINANCE_TYPE_ID,MAIN_TYPE_ID,SUB_TYPE_ID,CHART_OF_ACCOUNT_ID
					ORDER BY FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["AMOUNT"];
}

function GetCurrencyCode($target_currencyId)
{
	global $db;
	
	$sql = "SELECT strCode FROM mst_financecurrency WHERE intId = $target_currencyId";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	return $row["strCode"];
}
?>
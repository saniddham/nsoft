<?php
session_start();

//BEGIN - INCLUDE FILES {
$backwardseperator 	= "../../../../../";
require_once ("{$backwardseperator}dataAccess/Connector.php");
require_once ("../../../../../libraries/fpdf/fpdf.php");
//END	- }

$border		= 0;
$border1	= 1;
class PDF extends FPDF
{
	private $db;
	
	function EmptyRows($count,$loop)
	{
		for($i=1;$i<=$count;$i++)
		{
			$this->Ln();
			$this->SetFont('Times','',10);$this->Cell(15,5,++$loop.'.',1,0,'R');
			$this->SetFont('Times','',10);$this->Cell(50,5,'',1,0,'C');
			$this->SetFont('Times','',10);$this->Cell(40,5,'',1,0,'C');
			$this->SetFont('Times','',10);$this->Cell(20,5,'',1,0,'C');
			$this->SetFont('Times','',10);$this->Cell(30,5,'',1,0,'C');
			$this->SetFont('Times','',10);$this->Cell(35,5,'',1,0,'C');
			$this->SetFont('Times','',10);$this->Cell(40,5,'',1,0,'C');
			$this->SetFont('Times','',10);$this->Cell(45,5,'',1,0,'C');
		}
	}
	
	function Footer()
	{
		$this->SetY(-5);
		$this->SetFont('Times','I',8);
		$this->Cell(0,2,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}
	
	function ReportHeader()
	{
		$this->SetFont('Times','',10);$this->Cell(10,5,'',0,0,'L');
		$this->SetFont('Times','',10);$this->Cell(30,5,'',0,0,'L');
		$this->SetFont('Times','',10);$this->Cell(55,5,'',0,0,'L');
		$this->SetFont('Times','',10);$this->Cell(55,5,'',0,0,'L');
		$this->SetFont('Times','B',11);$this->Cell(0,5,'FORM: SVAT 05 ( b )',0,1,'R');
		$this->SetFont('Times','B',12);$this->Cell(0,5,'Goods  / Services Declaration  - Supplymentary Form',0,1,'C');
		$this->SetFont('Times','B',12);$this->Cell(0,10,'Suspended VAT Credit Notes',0,1,'C');
	}
	
	function CreateTableHeader()
	{
		$this->SetFont('Times','',10);$this->Cell(15,5,'Serial No','LTR',0,'C');
		$this->SetFont('Times','',10);$this->Cell(50,5,'SVAT Credit','LTR',0,'C');
		$this->SetFont('Times','',10);$this->Cell(40,5,'Relevant SVAT','LTR',0,'C');
		$this->SetFont('Times','',10);$this->Cell(20,5,'Date of','LTR',0,'C');
		$this->SetFont('Times','',10);$this->Cell(30,5,'Value of SVAT','LTR',0,'C');
		$this->SetFont('Times','',10);$this->Cell(35,5,'Suspended VAT','LTR',0,'C');
		$this->SetFont('Times','',10);$this->Cell(40,5,'Credit Voucher No.','LTR',0,'C');
		$this->SetFont('Times','',10);$this->Cell(45,5,'Credit Voucher No.','LTR',0,'C');
		$this->Ln();
		
		$this->SetFont('Times','',10);$this->Cell(15,5,'','LBR',0,'C');
		$this->SetFont('Times','',10);$this->Cell(50,5,'Note No','LBR',0,'C');
		$this->SetFont('Times','',10);$this->Cell(40,5,'Invoice No.','LBR',0,'C');
		$this->SetFont('Times','',10);$this->Cell(20,5,'Supply','LBR',0,'C');
		$this->SetFont('Times','',10);$this->Cell(30,5,'Credit Note (RS)','LBR',0,'C');
		$this->SetFont('Times','',10);$this->Cell(35,5,'Amount (RS)','LBR',0,'C');
		$this->SetFont('Times','',10);$this->Cell(40,5,'previously obtained','LBR',0,'C');
		$this->SetFont('Times','',10);$this->Cell(45,5,'issued for the current month','LBR',0,'C');
	}
}

$pdf 					= new PDF('L','mm','A4');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->ReportHeader();



$result = GetDetails();
$rowCount	= mysqli_num_rows($result);



	if($rowCount<20){	
		$pdf->CreateTableHeader();	
		while($row = mysqli_fetch_array($result))
		{			
			$pdf->Ln();
			$pdf->SetFont('Times','',10);$pdf->Cell(15,5,++$loop.'.',1,0,'R');
			$pdf->SetFont('Times','',10);$pdf->Cell(50,5,$row["INVOICE_NO"],1,0,'L');
			$pdf->SetFont('Times','',10);$pdf->Cell(40,5,$row["PURCHASE_DATE"],1,0,'C');
			$pdf->SetFont('Times','',10);$pdf->Cell(20,5,$row["VALUE"],1,0,'R');
			$pdf->SetFont('Times','',10);$pdf->Cell(30,5,$row["SVAT_VALUE"],1,0,'R');
			$pdf->SetFont('Times','',10);$pdf->Cell(35,5,'',1,0,'C');
			$pdf->SetFont('Times','',10);$pdf->Cell(40,5,'',1,0,'C');
			$pdf->SetFont('Times','',10);$pdf->Cell(45,5,'',1,0,'C');
		}
		$pdf->EmptyRows(20-$rowCount,$loop);
	}		
	else
	{
		$x = 0;
		$pdf->CreateTableHeader();
		while($row = mysqli_fetch_array($result))
		{
			if($x==35)
			{
				$x = 0;
				$pdf->AddPage();
				$pdf->CreateTableHeader();
			}			
			$pdf->Ln();
			$pdf->SetFont('Times','',10);$pdf->Cell(15,6,++$loop.'.',1,0,'R');
			$pdf->SetFont('Times','',10);$pdf->Cell(50,6,$row["INVOICE_NO"],1,0,'L');
			$pdf->SetFont('Times','',10);$pdf->Cell(40,6,$row["PURCHASE_DATE"],1,0,'C');
			$pdf->SetFont('Times','',10);$pdf->Cell(35,6,$row["VALUE"],1,0,'R');
			$pdf->SetFont('Times','',10);$pdf->Cell(30,6,$row["SVAT_VALUE"],1,0,'R');
			$pdf->SetFont('Times','',10);$pdf->Cell(35,6,'',1,0,'C');
			$pdf->SetFont('Times','',10);$pdf->Cell(40,6,'',1,0,'C');
			$pdf->SetFont('Times','',10);$pdf->Cell(45,6,'',1,0,'C');		
			$x++;	
		}
	}

	$pdf->Ln();
	$pdf->SetFont('Times','',10);$pdf->Cell(125,6,'Total :',1,0,'R');
	$pdf->SetFont('Times','',10);$pdf->Cell(30,6,'',1,0,'C');
	$pdf->SetFont('Times','',10);$pdf->Cell(35,6,'',1,0,'C');
	$pdf->SetFont('Times','',10);$pdf->Cell(40,6,'',1,0,'C');
	$pdf->SetFont('Times','',10);$pdf->Cell(45,6,'',1,0,'C');
	
	$pdf->Ln(25);	
	$pdf->SetFont('Times','',10);$pdf->Cell(85,6,'..............................................................',$border,0,'L');
	$pdf->SetFont('Times','',10);$pdf->Cell(10,6,'',$border,0,'L');
	$pdf->SetFont('Times','',10);$pdf->Cell(120,6,'',$border,0,'L');
	$pdf->SetFont('Times','',10);$pdf->Cell(60,6,'..............................................................',$border,0,'L');
	
	$pdf->Ln();
	$pdf->SetFont('Times','',10);$pdf->Cell(85,6,'Signature of the Supplier & the Seal',$border,0,'L');
	$pdf->SetFont('Times','',10);$pdf->Cell(10,6,'',$border,0,'L');
	$pdf->SetFont('Times','',10);$pdf->Cell(120,6,'',$border,0,'L');
	$pdf->SetFont('Times','',10);$pdf->Cell(60,6,'Signature of the Purchaser & the  Seal',$border,0,'L');
	
	$pdf->Ln();
	$pdf->SetFont('Times','',10);$pdf->Cell(85,6,'Date : ',$border,0,'L');
	$pdf->SetFont('Times','',10);$pdf->Cell(10,6,'',$border,0,'L');
	$pdf->SetFont('Times','',10);$pdf->Cell(120,6,'',$border,0,'L');
	$pdf->SetFont('Times','',10);$pdf->Cell(60,6,'Date : ',$border,0,'L');

$pdf->Output();

function GetDetails()
{
	global $db;
	
	$sql = "SELECT
			  PIH.INVOICE_NO    		AS INVOICE_NO,
			  PIH.PURCHASE_DATE 		AS PURCHASE_DATE,
			  SUM(PID.QTY * UNIT_PRICE) AS VALUE,
			  SUM(AMOUNT)       		AS SVAT_VALUE
			FROM finance_transaction T
			  INNER JOIN finance_supplier_purchaseinvoice_header PIH
				ON PIH.PURCHASE_INVOICE_NO = T.DOCUMENT_NO
				  AND PIH.PURCHASE_INVOICE_YEAR = T.DOCUMENT_YEAR
			  INNER JOIN finance_supplier_purchaseinvoice_details PID
				ON PID.PURCHASE_INVOICE_NO = PIH.PURCHASE_INVOICE_NO
				  AND PID.PURCHASE_INVOICE_YEAR = PIH.PURCHASE_INVOICE_YEAR
			WHERE CHART_OF_ACCOUNT_ID = 97
				AND DOCUMENT_TYPE = 'INVOICE'
			GROUP BY T.DOCUMENT_YEAR,T.DOCUMENT_NO";
			
	$sql1 = "SELECT
			  T.INVOICE_NO    		AS INVOICE_NO,
			  T.LAST_MODIFIED_BY 		AS PURCHASE_DATE,
			  '1' AS VALUE,
			  AMOUNT    		AS SVAT_VALUE
			FROM finance_transaction T";
	return $db->RunQuery($sql);
	
}
?>
<?php
session_start();

//BEGIN - INCLUDE FILES {
$backwardseperator 	= "../../../../../";
require_once ("{$backwardseperator}dataAccess/Connector.php");
require_once ("../../../../../libraries/fpdf/fpdf.php");
//END	- }

$border		= 0;
$border1	= 1;
class PDF extends FPDF
{
	private $db;
}

$pdf 					= new PDF('P','mm','A4');
$pdf->AddPage();
$pdf->SetXY(10,15);
$pdf->Cell(0,256,'',1,0);
$pdf->SetXY(10,5);
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(30,5,'Reference No',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(55,5,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(55,5,'',$border,0,'L');
$pdf->SetFont('Times','B',11);$pdf->Cell(40,5,'FORM : SVAT 04',$border,1,'R');
$pdf->SetX(45);
$pdf->SetFont('Times','',10);$pdf->Cell(55,5,'(Office Use Only)',$border,1,'C');
$pdf->SetFont('Times','',11);$pdf->Cell(0,10,'Goods / Services Declaration under SVATS ',$border1,1,'C');

$pdf->SetFont('Times','B',10);$pdf->Cell(95,8,'FROM',$border,0,'C');
$pdf->SetFont('Times','B',10);$pdf->Cell(95,8,'TO',$border,0,'C');
$pdf->Ln();

$pdf->SetFont('Times','B',10);$pdf->Cell(5,10,'1.',$border,0,'L');
$pdf->SetFont('Times','B',10);$pdf->Cell(30,10,'Period',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(55,10,'22',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'C');
$pdf->SetFont('Times','',10);$pdf->Cell(30,10,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(55,10,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'L');
$pdf->Ln();

$pdf->SetFont('Times','B',10);$pdf->Cell(5,10,'2.',$border,0,'C');
$pdf->SetFont('Times','B',10);$pdf->Cell(30,10,'Supplier',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(55,10,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'L');
$pdf->SetFont('Times','B',10);$pdf->Cell(5,10,'3.',$border,0,'C');
$pdf->SetFont('Times','B',10);$pdf->Cell(30,10,'Purchaser',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(55,10,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'L');
$pdf->Ln();

$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'C');
$pdf->SetFont('Times','B',10);$pdf->Cell(30,10,'SVAT No.',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(55,10,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'C');
$pdf->SetFont('Times','B',10);$pdf->Cell(30,10,'SVAT No.',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(55,10,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'L');
$pdf->Ln(10);

$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'C');
$pdf->SetFont('Times','B',10);$pdf->Cell(30,10,'VAT No.',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(55,10,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'C');
$pdf->SetFont('Times','B',10);$pdf->Cell(30,10,'VAT No.',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(55,10,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'L');
$pdf->Ln(12);

$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'C');
$pdf->SetFont('Times','B',10);$pdf->Cell(30,10,'Name',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(55,10,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'C');
$pdf->SetFont('Times','B',10);$pdf->Cell(30,10,'Name',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(55,10,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'L');
$pdf->Ln(12);

$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'C');
$pdf->SetFont('Times','B',10);$pdf->Cell(30,10,'Address',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(55,10,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'C');
$pdf->SetFont('Times','B',10);$pdf->Cell(30,10,'Address',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(55,10,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'L');
$pdf->Ln(12);

$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'C');
$pdf->SetFont('Times','B',10);$pdf->Cell(30,10,'Email Address',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(55,10,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'C');
$pdf->SetFont('Times','B',10);$pdf->Cell(30,10,'Email Address',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(55,10,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,10,'',$border,0,'L');
$pdf->Ln(12);

$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'4.',$border,0,'C');
$pdf->SetFont('Times','',10);$pdf->Cell(125,5,'Total Value of invoices issued under SVATS (FORM  SVAT 05)',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(30,5,'SLRS',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(25,5,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->Ln(6);

$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'5.',$border,0,'C');
$pdf->SetFont('Times','',10);$pdf->Cell(125,5,'Total Value of NFE invoices issued under SVATS ( FORM  SVAT 07 (c)).',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(30,5,'SLRS',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(25,5,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->Ln(6);

$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'6.',$border,0,'C');
$pdf->SetFont('Times','',10);$pdf->Cell(125,5,'Total Value of SVAT Debit Notes issued under SVAT ( FORM  SVAT 05 (a)).',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(30,5,'SLRS',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(25,5,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->Ln(6);

$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'7.',$border,0,'C');
$pdf->SetFont('Times','',10);$pdf->Cell(125,5,'Total Value of SVAT Credit Notes issued under SVAT ( FORM  SVAT 05 (b)).',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(30,5,'SLRS',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(25,5,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->Ln(6);

$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'8.',$border,0,'C');
$pdf->SetFont('Times','',10);$pdf->Cell(125,5,'Adjusted Value of Supplies made under SVAT ( 4 + 5 + 6 - 7 )',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(30,5,'SLRS',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(25,5,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->Ln(6);

$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'9.',$border,0,'C');
$pdf->SetFont('Times','',10);$pdf->Cell(125,5,'Suspended VAT for the above Period.',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(30,5,'SLRS',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(25,5,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->Ln(6);

$pdf->SetFont('Times','B',10);$pdf->Cell(0,10,'Credit voucher is to be writen according to item 8 and 9 above above',$border,0,'C');
$pdf->Ln(10);

$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'10.',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(30,5,'No. of Invoices',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(60,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'10.',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(30,5,'No. of pages',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(60,5,'',$border,0,'L');
$pdf->Ln(6);

$pdf->SetFont('Times','',10);$pdf->Cell(35,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(30,5,'SVAT05',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(25,5,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(35,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(30,5,'SVAT05',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(25,5,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->Ln(6);

$pdf->SetFont('Times','',10);$pdf->Cell(35,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(30,5,'SVAT07 (c)',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(25,5,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(35,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(30,5,'SVAT07 (c)',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(25,5,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->Ln(6);

$pdf->SetFont('Times','',10);$pdf->Cell(35,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(30,5,'SVAT05 (a)',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(25,5,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(35,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(30,5,'SVAT05 (a)',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(25,5,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->Ln(6);

$pdf->SetFont('Times','',10);$pdf->Cell(35,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(30,5,'SVAT05 (b)',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(25,5,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(35,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(30,5,'SVAT05 (b)',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(25,5,'',$border1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->Ln(6);

$pdf->SetFont('Times','B',10);$pdf->Cell(5,10,'12.',$border,0,'L');
$pdf->SetFont('Times','B',10);$pdf->Cell(90,10,'Declaration of the Supplier',$border,0,'L');
$pdf->SetFont('Times','B',10);$pdf->Cell(5,10,'13.',$border,0,'L');
$pdf->SetFont('Times','B',10);$pdf->Cell(90,10,'Declaration of the Purchaser',$border,0,'L');
$pdf->Ln(10);

$pdf->SetFont('Times','',8);$pdf->Cell(5,10,'',$border,0,'L');
$pdf->SetFont('Times','',8);$pdf->MultiCell(90,4,'     To the best of my knowledge I / We hereby declare that the above goods/services were sold by me /us  only for the purpose of export and / or to be  usedin a specified project and /or to be used  during the project implimentation period of a project approved under section 22(7) of the VAT Act and / or for indirect export. Also, value of NFE supplies were made on behalf of specific foreign buyers  in order to utilise them for export to same buyers.',$border,'L','');
$pdf->SetXY(105,$pdf->GetY()-28);
$pdf->SetFont('Times','',8);$pdf->Cell(5,10,'',$border,0,'L');
$pdf->SetFont('Times','',8);$pdf->MultiCell(90,4.7,'     I / We hereby declare that the above purchases were made by me /us  only for the purpose of export and / or to be  used in a specified project and /or to be used  during the project implimentation period of a project approved under section 22(7) of the VAT Act and / or for indirect export. Also, value of NFE supplies received on behalf of specific foreign buyers  and utilise to export for same buyers.',$border,'L','');
$pdf->Ln(4);


$pdf->SetFont('Times','',8);$pdf->Cell(5,10,'',$border,0,'L');
$pdf->SetFont('Times','',8);$pdf->MultiCell(90,4,'     Further , in the event of any failure to comply with the guideline issued by the Commissioner General , I /We  am / are aware that I /We , am/are  lable to VAT.',$border,'L','');
$pdf->SetXY(105,$pdf->GetY()-12);
$pdf->SetFont('Times','',8);$pdf->Cell(5,10,'',$border,0,'L');
$pdf->SetFont('Times','',8);$pdf->MultiCell(90,4,'     Further , in the event of any failure to comply with the guideline issued by the Commissioner General , I /We  am / are aware that I /We , am/are  lable to VAT.',$border,'L','');
$pdf->Ln(20);

$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(90,5,'Signature of the Supplier & the Seal',$border,0,'C');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(90,5,'Signature of the Purchaser & the Seal',$border,0,'C');
$pdf->Ln();

$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(30,5,'Date',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(60,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(30,5,'Date',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(60,5,'',$border,0,'L');

$pdf->SetXY(10,188);$pdf->Cell(95,88,'',1,0,'L');
$pdf->SetXY(105,188);$pdf->Cell(95,88,'',1,0,'L');
$pdf->Output();
?>
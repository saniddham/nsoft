<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
ini_set('display_errors',0);
ini_set('max_execution_time',300);

//include "include/javascript.html";

$companyId 			= $_SESSION['headCompanyId'];
$location 			= $_SESSION['CompanyID'];
$intUser 			= $_SESSION["userId"];
$deci				= 2;
$currency			= 1;//convert all amount to USD

$invMenuId			= 676;
$advPayMenuId		= 683;
$advSettleMenuId	= 778;
$payMenuId			= 691;

if($_SERVER['REQUEST_METHOD'] == "POST")
{

	$cboSupplier		= $_REQUEST['cboSupplier'];
	$cboOrder			= $_REQUEST['cboOrder'];
	$cboInvoice			= $_REQUEST['cboInvoice'];
	$cboCreditPeriod	= $_REQUEST['cboCreditPeriod'];
	$txtPONo			= $_REQUEST['txtPONo'];
	$txtInvoiceNo		= $_REQUEST['txtInvoiceNo'];
	$currency			= $_REQUEST['cboCurrency'];
	$cboCurrencyType	= $_REQUEST['cboCurrencyType'];
	
	$grnFromDate		= $_REQUEST['fromDate'];
	$grnToDate			= $_REQUEST['toDate'];
}
if(!isset($_POST["cboSupplier"]))
	$sqlLimit		= 0;
else
	$sqlLimit		= "";

$lastPaymentRange		= GetLastPaymentRange();
$payment_term_array 	= GetPaymentTerm();
$payment_range 			= GetPaymentTerm_Range($cboCreditPeriod);

$payment_range_array	= explode('-',$payment_range);
?>
<title>Supplier Invoice Aging Report</title>

<script type="text/javascript">
function Reload()
{
	document.frmSupplierInvoiceAgingReport.submit();	
}
</script>


<style type="text/css">
#progress {
 width: 100px;   
 border: 1px solid black;
 position: relative;
 padding: 1px;
}

#percent {
 position: absolute;   
 left: 50%;
}

#bar {
 height: 15px;
 background-color: #0C0;
}
</style>
<form id="frmSupplierInvoiceAgingReport" name="frmSupplierInvoiceAgingReport" method="post">
  <div align="center">
  <div><strong>Supplier Invoice Aging Report</strong><strong></strong></div>
<table width="1200" border="0" align="center">
 <tr>
   <td><table width="1750" border="0" class="normalfnt">
     <tr>
       <td width="257" style="text-align:center">Supplier</td>
       <td width="257" style="text-align:center">Purchase Order</td>
       <td width="268" style="text-align:center">Invoice</td>
       <td width="261" style="text-align:center">Credit Period</td>
       <td width="261" style="text-align:center">PO No</td>
       <td width="261" style="text-align:center">Invoice No</td>
       <td width="250" style="text-align:center">Currency</td>
       <td width="200" style="text-align:center">Type</td>
       <td  width="450"  style="text-align:center">GRN-FROM</td>
       <td  width="450" style="text-align:center">GRN-TO</td>
       <td style="text-align:center"></td>
     </tr>
     <tr>
     <td style="text-align:center"><select style="width:250px" name="cboSupplier" id="cboSupplier" >
     <option value=""></option>
       	<?php
			$sql = "SELECT DISTINCT PO.intSupplier,MS.strName
					FROM trn_poheader PO
					INNER JOIN mst_supplier MS ON PO.intSupplier=MS.intId
					WHERE PO.intStatus = 1 
					ORDER BY MS.strName ";
					
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				if($row['intSupplier']==$cboSupplier)
					echo "<option value=\"".$row['intSupplier']."\" selected=\"selected\">".$row['strName']."</option>";
				else
					echo "<option value=\"".$row['intSupplier']."\" >".$row['strName']."</option>";
			}
		?>
         </select></td>
       <td style="text-align:center"><select style="width:170px" name="cboOrder" id="cboOrder" >
       		<option value="" <?php echo ($cboOrder==''?'selected=selected':'')?>>ALL</option>
       		<option value="1" <?php echo ($cboOrder=='1'?'selected=selected':'')?>>GRN Completed</option>
            <option value="2" <?php echo ($cboOrder=='2'?'selected=selected':'')?>>Pending</option>
         </select></td>
       <td style="text-align:center"><select style="width:170px" name="cboInvoice" id="cboInvoice" >
       		<option value="" <?php echo ($cboInvoice==''?'selected=selected':'')?>>ALL</option>
       	 	<option value="1" <?php echo ($cboInvoice=='1'?'selected=selected':'')?>>Balance To Invoice</option>
            <option value="2" <?php echo ($cboInvoice=='2'?'selected=selected':'')?>>Balance Payment</option>
         </select></td>
       <td style="text-align:center"><select style="width:170px" name="cboCreditPeriod" id="cboCreditPeriod" >
       		<option value="" <?php echo ($cboCreditPeriod==''?'selected=selected':'')?>>All</option>
            <?php 
				$sql = "SELECT intId,strName FROM mst_financepaymentsterms WHERE strName <> 0 ORDER BY strName ";
				$result = $db->RunQuery($sql);
				while($row = mysqli_fetch_array($result))
				{
					if($cboCreditPeriod==$row["intId"])
            			echo "<option value=".$row["intId"]." selected=\"selected\">".$row["strName"]."</option>";
					else
						echo "<option value=".$row["intId"].">".$row["strName"]."</option>";
           		} 
		   ?>
           <option value="More" <?php echo ($cboCreditPeriod=='More'?'selected=selected':'')?>>More</option>
         </select></td>
         <td style="text-align:center"><input name="txtPONo" id="txtPONo" type="text" style="width:170px" value="<?php echo $txtPONo; ?>" /></td>
         <td style="text-align:center"><input name="txtInvoiceNo" id="txtInvoiceNo" type="text" style="width:170px" value="<?php echo $txtInvoiceNo; ?>" /></td>
         <td style="text-align:center"><select style="width:100px" name="cboCurrency" id="cboCurrency" >
           <?php 
				$sql = "SELECT intId,strCode
						FROM mst_financecurrency
						WHERE intStatus = 1
						ORDER BY strCode ";
				$result = $db->RunQuery($sql);
				while($row = mysqli_fetch_array($result))
				{
					if($currency==$row["intId"])
            			echo "<option value=".$row["intId"]." selected=\"selected\">".$row["strCode"]."</option>";
					else
						echo "<option value=".$row["intId"].">".$row["strCode"]."</option>";
           		} 
		   ?>
         </select></td>
         <td style="text-align:center"><select style="width:100px" name="cboCurrencyType" id="cboCurrencyType" >
         <option value="1" <?php echo ($cboCurrencyType=='1'?'selected=selected':'')?>>Convert</option>
         <option value="2" <?php echo ($cboCurrencyType=='2'?'selected=selected':'')?>>Filter</option>
         </select></td>
         <td ><input name="fromDate" type="text" value="<?Php echo $grnFromDate; ?>"  class="txtbox" id="fromDate" style="width:80px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
         <td ><input name="toDate" type="text" value="<?Php echo $grnToDate; ?>"  class="txtbox" id="toDate" style="width:80px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
         <td style="text-align:center"><a class="button white medium" id="butSearch" name="butSearch" onClick="Reload();" >Search</a></td>
     </tr>
   </table></td>
 </tr>
 <tr>
  <td><table  border="0" cellpadding="0" cellspacing="0" class="bordered" width="100%" bgcolor="#FFFFFF">
    <thead>
    <tr valign="bottom">
        <th>Supplier Name</th>
        <th>PONo</th>
        <th>PO Crrency</th>
        <th>GRN No</th>
        <th>PO Date</th>
        <th>Invoice No</th>
        <th>Invoice Date</th>
        <th>Pur Invoice No</th>
        <th>PO Value</th>
        <th>GRN Value</th>
        <th>Tot GRN Value</th>
        <th>GRN Prog</th>
        <th>Invoiced Amount</th>
        <th>Advanced Not Settle</th>
        <th>Advanced Amount</th>
        <th>Debit Amount</th>
        <th>Credit Amount</th>
        <th>Paid Amount</th>
        <th>Balance To Invoice</th>
        <th>Balance To Payment</th>
        <th>Last GRN Date</th>
        <th>Credit Term</th>
        <?php for($i=0;$i<count($payment_term_array);$i++){?>
        <th style="width:40px"><?php echo $payment_term_array[$i]?></th>
        <?php }?>
        </tr>
    </thead>
     <tbody>
<?php
$result = GetMainDetails($cboSupplier,$cboOrder,$cboInvoice,$cboCreditPeriod,$txtPONo,$txtInvoiceNo,$grnFromDate,$grnToDate,$currency,$cboCurrencyType);
while($row = mysqli_fetch_array($result))
{
	$savedCurRate 		= getCurrencyRate($row['CURRENCY'],$row['PODATE']);
	$targetCurRate  	= getCurrencyRate($currency,$row['PODATE']);
	$totGrnVal			= ($row["TOTGRNVALUE"]/$targetCurRate)*$savedCurRate;
	$POVal				= ($row["POVALUE"]/$targetCurRate)*$savedCurRate;
	$grnVal				= ($row["GRNVALUE"]/$targetCurRate)*$savedCurRate;
	$balanceToInvoice	= round(($row["BALANCE_TO_INVOICE"]/$targetCurRate)*$savedCurRate,$deci);
	$balanceToPaid		= round(($row["BALANCE_TO_PAYMENT"]/$targetCurRate)*$savedCurRate,$deci);
	$invoiceAmount		= ($row["INVOICEAMOUNT"]/$targetCurRate)*$savedCurRate;
	$totRecieveAmt		= ($row["totRecievedAmount"]/$targetCurRate)*$savedCurRate;
	$POAdvance			= ($row["POADVANCED"]/$targetCurRate)*$savedCurRate;
	$invoiceAdvanced	= ($row["INVOICEADVANCED"]/$targetCurRate)*$savedCurRate;
	$debitAmount		= ($row["DEBITAMOUNT"]/$targetCurRate)*$savedCurRate;
	$creditAmount		= ($row["CREDITAMOUNT"]/$targetCurRate)*$savedCurRate;
	$paidAmount			= ($row["PAIDAMOUNT"]/$targetCurRate)*$savedCurRate;	
	
	$progress = (round((round($totGrnVal)/round($POVal))*100,2)>100?100:round((round($totGrnVal)/round($POVal))*100,2));
	$finalGRNValue = round($grnVal,$deci);
	$finalGRNTOTVal = round($totGrnVal,$deci);
	
	//$balanceToInvoice 	= round($row['BALANCE_TO_INVOICE'],$deci);
	//$balanceToPaid 		= round($row['BALANCE_TO_PAYMENT'],$deci);
?>
      <tr>
          <td nowrap="nowrap" title="Supplier Name"><?php echo $row["SUPPLIERNAME"]?></td>
          <td nowrap="nowrap" title="PONo"><?php echo $row["PONO"];?></td>
          <td nowrap="nowrap" style="text-align:center" title="POCurrency"><?php echo $row["PO_CURRENCY_CODE"];?></td>
          <td nowrap="nowrap" title="GRN No"><?php echo $row["GRNNO"];?></td>
          <td nowrap="nowrap" title="PO Date"><?php echo $row["PODATE"];?></td>
          <td nowrap="nowrap" title="Invoice No"><?php echo $row["INVOICE"];?></td>
          <td nowrap="nowrap" title="Invoice No"><?php echo $row['INVOICE_DATE'];?></td>
          <td nowrap="nowrap" title="Purchase Invoice No"><?php echo $row["PUR_INVOICE_NO"];?></td>
          <td nowrap="nowrap" style="text-align:right" title="PO Value"><?php echo number_format($POVal,$deci)?></td>
          <td nowrap="nowrap" style="text-align:right" title="GRN Value"><?php echo number_format($finalGRNValue,$deci)?></td>
          <td nowrap="nowrap" style="text-align:right" title="Total GRN Value"><?php echo number_format($finalGRNTOTVal,$deci)?></td>
          <td nowrap="nowrap" title="GRN Prog"><div id="progress"><span id="percent"><?php echo $progress?>%</span><div id="bar" style="width:<?php echo $progress?>px"></div></div></td>
          <td nowrap="nowrap" style="text-align:right" title="Invoiced Amount"><?php echo ($finalGRNValue>$invoiceAmount?"<a href=\"?q=".$invMenuId."&invoiceNoAL=".urlencode($row["INVOICE"])."\" target=\"purchaseInvoice.php\">".number_format($invoiceAmount,$deci)."</a>":number_format($invoiceAmount,$deci))?></td>
          <td nowrap="nowrap" style="text-align:right" title="PO Advanced Amount"><?php echo ((round($POVal,$deci)-round($totRecieveAmt,$deci))>0?"<a href=\"?q=".$advPayMenuId."&supplierIdAL=".$row["SUPPLIERID"]."&PONoAL=".$row["PONO_FOR_AUTOLOAD"]."\" target=\"advancePayment.php\">".number_format($POAdvance,$deci)."</a>":number_format($POAdvance,$deci))?></td>
          <td nowrap="nowrap" style="text-align:right" title="Invoice Advanced Amount"><?php echo ((round($invoiceAmount,$deci)>0 && round($balanceToInvoice,$deci)>0)?"<a href=\"?q=".$advSettleMenuId."&supplierId=".$row["SUPPLIERID"]."&currencyId=".$row["CURRENCY"]."\" target=\"advanceSettlement.php\">".number_format($invoiceAdvanced,$deci)."</a>":number_format($invoiceAdvanced,$deci))?></td>
          <td nowrap="nowrap" style="text-align:right" title="Debit Amount"><?php echo ((round($invoiceAmount,$deci)>0 && round($balanceToInvoice,$deci)>0)?"<a href=\"?q=709&SupplierId=".$row["SUPPLIERID"]."&CurrencyId=".$row["CURRENCY"]."&InvoiceNo=".$row["PUR_INVOICE_NO"]."\" target=\"debit_note.php\">".number_format($debitAmount,$deci)."</a>":number_format($debitAmount,$deci))?></td>
          <td nowrap="nowrap" style="text-align:right" title="Credit Amount"><?php echo (round($invoiceAmount,$deci)>0?"<a href=\"?q=1084&SupplierId=".$row["SUPPLIERID"]."&CurrencyId=".$row["CURRENCY"]."&InvoiceNo=".$row["PUR_INVOICE_NO"]."\" target=\"credit_note.php\">".number_format($creditAmount,$deci)."</a>":number_format($creditAmount,$deci))?></td>
          <td nowrap="nowrap" style="text-align:right" title="Paid Amount"><?php echo ((round($invoiceAmount,$deci)>0 && round($balanceToPaid,$deci)>0)?"<a href=\"?q=".$payMenuId."&supplierIdAL=".$row["SUPPLIERID"]."&currencyIdAL=".$row["CURRENCY"]."\" target=\"supplierPayment.php\">".number_format($paidAmount,$deci)."</a>":number_format($paidAmount,$deci))?></td>
          <td nowrap="nowrap" style="text-align:right" title="Balance To Invoice"><?php echo number_format($balanceToInvoice,$deci)?></td>
          <td nowrap="nowrap" style="text-align:right" title="Balance To Payment"><?php echo number_format($balanceToPaid,$deci)?></td>
          <td nowrap="nowrap" style="text-align:right" title="Last GRN Date"><?php echo $row["GRNDATE"]?></td>
          <td nowrap="nowrap" style="text-align:right" title="Credit Term"><?php echo $row["CREDITTERM"]?></td>
          <?php  $start = 0;
          if($row["CREDITTERM"]<$row["DIFF_DATES"])
              $agingCSS = "color:#F00;font-weight:bold";
          else
              $agingCSS = "";
			
          for($i=0;$i<count($payment_term_array);$i++){
              $diff_date 	= $row["DIFF_DATES"];
              $aging		= 0;
		
              if($start <= $diff_date && $payment_term_array[$i] >= $diff_date)
              {
                  $aging = $balanceToPaid;
              }
				else 
					$aging = 0;
			if($agingCSS=="")
				$aging_array_normal[$payment_term_array[$i]] += $aging;	
			else
				$aging_array_due[$payment_term_array[$i]] += $aging;	
				
			$start = $payment_term_array[$i];	
			?>
        <td nowrap="nowrap" style="text-align:right;<?php echo $agingCSS?>" title="<?php echo $payment_term_array[$i]?>"><?php echo $aging==0?'&nbsp;':number_format($aging,$deci)?></td>
        <?php }?>
       </tr>
<?php
$tot_PO_Value						+= round($POVal,$deci);
$tot_GRN_Value						+= round($finalGRNValue,$deci);
$tot_invoiced_amount 				+= round($invoiceAmount,$deci);
$tot_payment_amount					+= round($paidAmount,$deci);
$tot_debit_note_amount				+= round($debitAmount,$deci);
$tot_credit_note_amount				+= round($creditAmount,$deci);
$tot_balance_to_invoice				+= round($balanceToInvoice,$deci);
$tot_balance_to_payment				+= round($balanceToPaid,$deci);
}
?>
	<tr>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap" style="text-align:right">&nbsp;</td>
        <td nowrap="nowrap" style="text-align:right"><b><?php echo number_format($tot_GRN_Value,$deci)?></b></td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap" style="text-align:right"><b><?php echo number_format($tot_invoiced_amount,$deci)?></b></td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap" style="text-align:right"><b><?php echo number_format($tot_debit_note_amount,$deci)?></b></td>
        <td nowrap="nowrap" style="text-align:right"><b><?php echo number_format($tot_credit_note_amount,$deci)?></b></td>
        <td nowrap="nowrap" style="text-align:right"><b><?php echo number_format($tot_payment_amount,$deci)?></b></td>
        <td nowrap="nowrap" style="text-align:right"><b><?php echo number_format($tot_balance_to_invoice,$deci)?></b></td>
        <td nowrap="nowrap" style="text-align:right"><b><?php echo number_format($tot_balance_to_payment,$deci)?></b></td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
    	<?php for($i=0;$i<count($payment_term_array);$i++){?>
        <td nowrap="nowrap" style="text-align:right"><b><?php echo number_format(round($aging_array_normal[$payment_term_array[$i]],$deci),$deci)?></b></td>
        <?php } ?>
      </tr>
      <tr>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
         <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <?php for($i=0;$i<count($payment_term_array);$i++){?>
        <td nowrap="nowrap" style="text-align:right;color:#F00"><b><?php echo number_format(round($aging_array_due[$payment_term_array[$i]],$deci),$deci)?></b></td>
        <?php } ?>
      </tr>
    </tbody>
  </table></td>
</tr>
            <tr>
<tr height="40">
  <td align="left" class="normalfnt" style="font-style:italic;">Printed Date: <?php echo date("Y/m/d") ?></td>
</tr>
</table>
</div>
</form>
<script type="text/javascript">
$(document).ready(function()
{
	$('.bordered tbody tr').die('click').live('click',function(){
		if($(this).attr('style')!='background-color:#82FF82')
		{
			$('.bordered tbody tr').attr('style','background-color:#FFFFFF');  
			   $(this).attr('style','background-color:#82FF82');     
		}                                             
	})
});
</script>
<?php
function GetMainDetails($cboSupplier,$cboOrder,$cboInvoice,$cboCreditPeriod,$txtPONo,$txtInvoiceNo,$grnFromDate,$grnToDate,$currency,$cboCurrencyType)
{
	global $companyId;
	global $db;
	global $deci;
	global $payment_range_array;
	global $lastPaymentRange;
	global $sqlLimit;
	
	$sql = "SELECT
			SUB_1.*,
			(GRNVALUE-INVOICEAMOUNT)							 		AS BALANCE_TO_INVOICE,
			((GRNVALUE)-PAIDAMOUNT-INVOICEADVANCED-DEBITAMOUNT+CREDITAMOUNT) 		AS BALANCE_TO_PAYMENT
			FROM
				(SELECT
				MS.strName                 								AS SUPPLIERNAME,
				POH.intSupplier            								AS SUPPLIERID,
				CONCAT(POH.intPOYear,' - ',POH.intPONo) 				AS PONO,
				CONCAT(POH.intPONo,'/',POH.intPOYear) 					AS PONO_FOR_AUTOLOAD,
				CONCAT(GH.intGrnYear,' - ',GH.intGrnNo) 				AS GRNNO,
				DATE(POH.dtmPODate)        								AS PODATE,
				GH.strInvoiceNo            								AS INVOICE,
				GH.SUPPLIER_INVOICE_DATE            					AS INVOICE_DATE,
				POH.intCurrency											AS CURRENCY,
				MC.strCode												AS PO_CURRENCY_CODE,
				
				SUBSTRING((SELECT GROUP_CONCAT(DISTINCT CONCAT(SPIH.PURCHASE_INVOICE_YEAR,'-',SPIH.PURCHASE_INVOICE_NO))
				FROM finance_supplier_purchaseinvoice_header SPIH  
				WHERE SPIH.INVOICE_NO = GH.strInvoiceNo
					AND SPIH.STATUS<>'-2'),1,34)						AS PUR_INVOICE_NO,
				
				ROUND(SUM((POD.dblUnitPrice*POD.dblQty)*(100-POD.dblDiscount)/100),$deci)+ROUND(SUM(POD.dblTaxAmmount),$deci) AS POVALUE,
				
				COALESCE((SELECT ROUND(SUM((GD.dblGrnQty - GD.dblRetunSupplierQty) * GD.dblGrnRate),$deci)+ROUND(SUM((PD.dblTaxAmmount/(PD.dblUnitPrice*PD.dblQty*(100-PD.dblDiscount)/100))*((GD.dblGrnQty - GD.dblRetunSupplierQty) * GD.dblGrnRate)),$deci)
				FROM ware_grndetails GD
				INNER JOIN ware_grnheader GRH 
					ON GRH.intGrnNo = GD.intGrnNo 
					AND GRH.intGrnYear = GD.intGrnYear
				INNER JOIN (SELECT * FROM trn_podetails GROUP BY trn_podetails.intPONo,trn_podetails.intPOYear,trn_podetails.intItem ) PD 
					ON PD.intPONo = GRH.intPoNo 
					AND PD.intPOYear = GRH.intPoYear 
					AND GD.intItemId = PD.intItem
				WHERE GD.intGrnNo = GH.intGrnNo
					AND GD.intGrnYear = GH.intGrnYear
					AND GRH.intStatus = 1
				GROUP BY GD.intGrnNo,GD.intGrnYear),0) 					AS GRNVALUE,
				
				COALESCE((SELECT ROUND(SUM((GD.dblGrnQty - GD.dblRetunSupplierQty)*GD.dblGrnRate),$deci)+ROUND(SUM(POD.dblTaxAmmount),$deci)
				FROM ware_grndetails GD
				INNER JOIN ware_grnheader GRH 
					ON GRH.intGrnNo = GD.intGrnNo 
					AND GRH.intGrnYear = GD.intGrnYear
				WHERE GRH.intPoNo = POH.intPONo
					AND GRH.intPoYear = POH.intPOYear
					AND GRH.intStatus = 1),0) 				AS TOTGRNVALUE,
				
				COALESCE((SELECT ROUND(SUM(ST.VALUE),$deci)
				FROM finance_supplier_purchaseinvoice_header SPIH
				INNER JOIN finance_supplier_transaction ST 
					ON ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO 
					AND ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR
				WHERE ST.PO_NO = POH.intPONo
					AND ST.PO_YEAR = POH.intPOYear
					AND SPIH.INVOICE_NO = GH.strInvoiceNo
					AND ST.DOCUMENT_TYPE = 'INVOICE'),0) 				AS INVOICEAMOUNT,
				
				COALESCE((SELECT ROUND(SUM((ST.VALUE)*-1),$deci)
				FROM finance_supplier_transaction ST
				WHERE ST.PO_NO = POH.intPONo
					AND ST.PO_YEAR = POH.intPOYear
					AND ST.PURCHASE_INVOICE_NO IS NULL
					AND ST.PURCHASE_INVOICE_YEAR IS NULL
					AND ST.DOCUMENT_TYPE = 'ADVANCE'),0) 							AS POADVANCED,
				
				COALESCE((SELECT ROUND(SUM((ST.VALUE)*-1),$deci)
				FROM finance_supplier_purchaseinvoice_header SPIH
				INNER JOIN finance_supplier_transaction ST 
					ON ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO 
					AND ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR
				WHERE SPIH.INVOICE_NO = GH.strInvoiceNo
					AND ST.PURCHASE_INVOICE_NO = SPIH.PURCHASE_INVOICE_NO
					AND ST.PURCHASE_INVOICE_YEAR = SPIH.PURCHASE_INVOICE_YEAR
					AND ST.DOCUMENT_TYPE = 'ADVANCE'),0) 							AS INVOICEADVANCED,
				
				COALESCE((SELECT ROUND(SUM((ST.VALUE)*-1),$deci)
				FROM finance_supplier_purchaseinvoice_header SPIH
				INNER JOIN finance_supplier_transaction ST ON ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
				ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR
				WHERE ST.PO_NO = POH.intPONo
				AND ST.PO_YEAR = POH.intPOYear
				AND SPIH.INVOICE_NO = GH.strInvoiceNo
				AND ST.PURCHASE_INVOICE_NO = SPIH.PURCHASE_INVOICE_NO
				AND ST.PURCHASE_INVOICE_YEAR = SPIH.PURCHASE_INVOICE_YEAR
				AND ST.DOCUMENT_TYPE = 'DEBIT'),0) 									AS DEBITAMOUNT,
				
				COALESCE((SELECT ROUND(SUM((ST.VALUE)),$deci)
				FROM finance_supplier_purchaseinvoice_header SPIH
				INNER JOIN finance_supplier_transaction ST ON ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
				ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR
				WHERE ST.PO_NO = POH.intPONo
				AND ST.PO_YEAR = POH.intPOYear
				AND SPIH.INVOICE_NO = GH.strInvoiceNo
				AND ST.PURCHASE_INVOICE_NO = SPIH.PURCHASE_INVOICE_NO
				AND ST.PURCHASE_INVOICE_YEAR = SPIH.PURCHASE_INVOICE_YEAR
				AND ST.DOCUMENT_TYPE = 'CREDIT'),0) 									AS CREDITAMOUNT,
				
				COALESCE((SELECT ROUND(SUM((ST.VALUE)*-1),$deci)
				FROM finance_supplier_purchaseinvoice_header SPIH
				INNER JOIN finance_supplier_transaction ST 
					ON ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO 
					AND ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR
				WHERE ST.PO_NO = POH.intPONo
					AND ST.PO_YEAR = POH.intPOYear
					AND SPIH.INVOICE_NO = GH.strInvoiceNo
					AND ST.PURCHASE_INVOICE_NO = SPIH.PURCHASE_INVOICE_NO
					AND ST.PURCHASE_INVOICE_YEAR = SPIH.PURCHASE_INVOICE_YEAR
					AND ST.DOCUMENT_TYPE = 'PAYMENT'),0) 							AS PAIDAMOUNT,
				
				COALESCE((SELECT ROUND(SUM((ST.VALUE)*-1),2)
				FROM finance_supplier_transaction ST
				WHERE PO_NO=POH.intPONo AND
					PO_YEAR = POH.intPOYear 
					AND DOCUMENT_TYPE!='INVOICE'),0) 								AS totRecievedAmount,
				
				GH.datdate                 											AS GRNDATE,
				DATEDIFF(NOW(),GH.datdate) 											AS DIFF_DATES,
				FPT.strName                											AS CREDITTERM
				
				FROM trn_poheader POH
				INNER JOIN trn_podetails POD 
					ON POH.intPONo = POD.intPONo 
					AND POH.intPOYear = POD.intPOYear
				LEFT JOIN ware_grnheader GH 
					ON GH.intPoNo = POH.intPONo 
					AND GH.intPoYear = POH.intPOYear
				INNER JOIN mst_supplier MS 
					ON MS.intId = POH.intSupplier
				INNER JOIN mst_financepaymentsterms FPT 
					ON FPT.intId = POH.intPaymentTerm
				INNER JOIN mst_locations ML 
					ON ML.intId=POH.intCompany
				INNER JOIN mst_financecurrency MC 
					ON MC.intId=POH.intCurrency
				WHERE POH.intStatus = 1 
					AND PAYMENT_COMPLETED_FLAG = 0 
					AND GH.intStatus = 1 
					AND ML.intCompanyId = '$companyId' 
				 $sqlLimit ";
if($txtPONo!='') 
	$sql .= "AND POH.intPONo = '$txtPONo' ";
if($txtInvoiceNo!='') 
	$sql .= "AND GH.strInvoiceNo = '$txtInvoiceNo' ";
if($cboSupplier!='') 
	$sql .= "AND POH.intSupplier = '$cboSupplier' ";
				
$sql .= "GROUP BY POH.intPONo,POH.intPOYear,GH.strInvoiceNo
		 HAVING 1=1 ";
			 
if($cboCreditPeriod!="" && $cboCreditPeriod!="More")
	$sql .= "AND DIFF_DATES BETWEEN $payment_range_array[0] AND $payment_range_array[1] ";

if($cboCreditPeriod=="More")
	$sql .= "AND DIFF_DATES > $lastPaymentRange ";

if($cboCurrencyType==2)
	$sql .= "AND POH.intCurrency = $currency ";
	
if($grnFromDate!='')
	$sql .=" AND GH.datdate   >='$grnFromDate' ";
	
if($grnToDate!='')
	$sql .=" AND GH.datdate   <='$grnToDate' ";

	$sql .= "ORDER BY POH.intPOYear,POH.intPONo,GH.strInvoiceNo) AS SUB_1
	HAVING 1=1 ";
	
if($cboInvoice == '1')
	$sql .= "AND BALANCE_TO_INVOICE > 0 ";

if($cboInvoice == '2')
	$sql .= "AND BALANCE_TO_PAYMENT > 0 ";

if($cboOrder=='1')
	$sql .= "AND ROUND((ROUND(TOTGRNVALUE)/ROUND(POVALUE))*100,$deci) >= 100  ";

if($cboOrder=='2')
	$sql .= "AND ROUND((ROUND(TOTGRNVALUE)/ROUND(POVALUE))*100,$deci) < 100 ";	
	
	

	return $db->RunQuery($sql);
}

function GetPaymentTerm()
{
	global $db;
	$i = 0;
	$sql = "SELECT DISTINCT
			   strName
			FROM mst_financepaymentsterms
			WHERE strName <> 0 
			ORDER BY strName";
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$new_array[$i++] = $row["strName"];
	}
		$new_array[$i++] = "More";
	return $new_array;
}

function GetPaymentTerm_Range($cboCreditPeriod)
{
	global $db;
	
	$sql = "SELECT
			  strRange
			FROM mst_financepaymentsterms
			WHERE  intId = '$cboCreditPeriod'";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	return $row["strRange"];
}

function GetLastPaymentRange()
{
	global $db;
	$sql = "SELECT DISTINCT
			   strName	AS LAST
			FROM mst_financepaymentsterms
			ORDER BY strName DESC
			LIMIT 1";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	return $row["LAST"];
}
function getCurrencyRate($currency,$date)
{
	global $db;
	global $companyId;
	
	$sql = "SELECT dblExcAvgRate
			FROM mst_financeexchangerate
			WHERE intCurrencyId='$currency' AND
			dtmDate='$date' AND
			intCompanyId='$companyId' ";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);

	return $row['dblExcAvgRate'];
}
?>
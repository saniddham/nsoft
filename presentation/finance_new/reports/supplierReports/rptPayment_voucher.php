<?php
session_start();
$backwardseperator 	= "../../../../";
include "../../../../dataAccess/Connector.php";
include "../../../../class/finance/cls_convert_amount_to_word.php";
include "../../../../class/finance/reports/supplierReports/cls_paymentvoucher_get.php";

$obj_AmtName	   	= new Cls_Convert_Amount_To_Word($db);
$obj_payVoucher_get	= new Cls_PaymentVoucher_Get($db);

$mainPath 			= $_SESSION['mainPath'];
$locationId			= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$thisFilePath 		= $_SERVER['PHP_SELF'];

$paymentNo			= $_REQUEST['serialNo'];
$paymentYear		= $_REQUEST['serialYear'];
$payType			= $_REQUEST['type'];

$header_arry		= $obj_payVoucher_get->getHeaderData($paymentNo,$paymentYear,$payType);
$result_Details		= $obj_payVoucher_get->getDetailData($paymentNo,$paymentYear,$payType);

$voucherNo			= $header_arry['voucherYear'].'/'.$header_arry['voucherMonth'].'/'.$paymentNo.'/'.$header_arry['strCostCenterCode'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Payment Voucher</title>
<link rel="stylesheet" type="text/css" href="../../../../css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="../../../../css/button.css"/>
<link rel="stylesheet" type="text/css" href="../../../../css/promt.css"/>

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">

<style>
#apDiv1 {
	position: absolute;
	left: 400px;
	top: 140px;
	width: 650px;
	height: 322px;
	z-index: 1;
}
</style>

</head>
<body>
<?php
if($header_arry['STATUS']==-2)//pending
{
?>
	<div id="apDiv1"><img src="../../../../images/cancelled.png" style="opacity:0.2" /></div>
<?php
}
?>
<form id="frmPaymentVoucher" name="frmPaymentVoucher" method="post">
    <table width="800" align="center">
    	<tr>
        	<td><?php include '../../../../reportHeader.php' ?></td>
        </tr>
        <tr>
        	<td style="text-align:center">&nbsp;</td>
        </tr>
        <tr>
        	<td style="text-align:center"><strong>PAYMENT VOUCHER - <?php echo $header_arry['CHART_OF_ACCOUNT_NAME']; ?></strong></td>
     	</tr>
        <tr>
        	<td>
            	<table width="100%" border="0" class="normalfnt">
                <tr>
                    <td width="15%">&nbsp;</td>
                    <td width="1%">&nbsp;</td>
                    <td width="59%">&nbsp;</td>
                    <td width="11%">&nbsp;</td>
                    <td width="1%">&nbsp;</td>
                    <td width="13%">&nbsp;</td>
                </tr>
                <tr>
                    <td><b>Voucher No</b></td>
                    <td>:</td>
                    <td><b><?php echo $voucherNo; ?></b></td>
                    <td>Date</td>
                    <td>:</td>
                    <td style="text-align:right"><?php echo date("d-M-Y", strtotime($header_arry["paymentDate"])) ?></td>
                </tr>
                </table>
            </td>
        </tr>
        <tr>
        	<td>
            <table width="100%" border="0" class="rptBordered" id="tblMain">
            	<thead>
                    <tr>
                        <th> Particulars</th>
                        <th width="15%">Amount (LKR)</th>
                    </tr>
                </thead>
                <tr class="normalfnt">
                	<td style="text-align:left"><b>Account  :</b>&nbsp;<?php echo $header_arry['supplier']; ?></td>
                	<td style="text-align:right">&nbsp;</td>
                </tr>
                <?php
				$totAmountLocal = 0;
				$totAmountAct 	= 0;
				$detailRow 		= "";
				
				while($row=mysqli_fetch_array($result_Details))
				{
					$savedCurrencyRate 	= $obj_payVoucher_get->getExchangeRate($header_arry['CURRENCY_ID'],$header_arry['paymentDate']);
					$amountLocal 		= round($savedCurrencyRate,2)*$row['amonut'];
					$totAmountLocal	   += $amountLocal;
					$totAmountAct	   += $row['amonut'];
                   
				    $detailRow .= '<tr class="normalfnt">
									<td style="text-align:left">'.$row['invoiceNo'].'</td>
									<td style="text-align:right">'.number_format($row['amonut'],2).'</td>
									<td style="text-align:center">'.($row['GRNNo']==""?"&nbsp;":$row['GRNNo']).'</td>
									<td style="text-align:center">'.($row['PONo']==""?"&nbsp;":$row['PONo']).'</td>
								  </tr>';
				}
				?>
                <tr class="normalfnt">
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="31%" style="text-align:center"><strong><?php echo number_format($totAmountAct,2)?>&nbsp;<?php echo $header_arry['currency'] ?>&nbsp;@&nbsp;Rs.&nbsp;<?php echo number_format($savedCurrencyRate,2)?></strong></td>
                                <td width="25%" style="text-align:center"><strong>Amount&nbsp;(<?php echo $header_arry['currency'] ?>)</strong></td>
                                <td width="22%" style="text-align:center"><strong>GRN No</strong></td>
                                <td width="22%" style="text-align:center"><strong>PO No</strong></td>
                            </tr>
                            <?php echo $detailRow; ?>
                        </table>
                    </td>
                    <td valign="top" style="text-align:right"><?php echo number_format($totAmountLocal,2);?></td>
                </tr>
                <tr class="normalfnt">
                    <td >&nbsp;</td>
                    <td >&nbsp;</td>
                </tr>
                <tr class="normalfnt">
                    <td style="text-align:left"><b>Through&nbsp;&nbsp;&nbsp;:</b>&nbsp;</td>
                    <td style="text-align:right">&nbsp;</td>
                </tr>
                <tr class="normalfnt">
                  <td style="text-align:left">&nbsp;&nbsp;<?php echo $header_arry["CHART_OF_ACCOUNT_NAME"]; ?></td>
                  <td style="text-align:right">&nbsp;</td>
                </tr>
                <tr class="normalfnt">
                    <td style="text-align:left"><b>Narration&nbsp;:</b>&nbsp;</td>
                    <td style="text-align:right">&nbsp;</td>
                </tr>
                 <tr class="normalfnt">
                   <td >&nbsp;&nbsp;<span style="text-align:left"><?php echo $header_arry["REMARKS"]; ?></span></td>
                   <td >&nbsp;</td>
                 </tr>
                 <tr class="normalfnt">
                    <td >&nbsp;</td>
                    <td >&nbsp;</td>
                </tr>
                 <tr class="normalfnt">
                    <td >&nbsp;</td>
                    <td >&nbsp;</td>
                </tr>
                <tr class="normalfnt">
                    <td style="text-align:left"><b>Amount (in words)&nbsp;:</b></td>
                    <td style="text-align:right">&nbsp;</td>
                </tr>
                <tr class="normalfnt">
                    <td style="text-align:left"><?php echo $obj_AmtName->Convert_Amount($totAmountLocal,'LKR'); ?></td>
                    <td style="text-align:right">&nbsp;</td>
                </tr>
                <tr>
                    <td align="right"><b>Total :</b></td>
                    <td width="15%" style="text-align:right"><b><?php echo number_format($totAmountLocal,2);?></b></td>
                </tr>
            </table>
      <tr>
            	<td class="normalfnt">&nbsp;</td>
            </tr>
            <tr>
            	<td class="normalfnt">&nbsp;</td>
            </tr> 
            <tr>
                <td>
                    <table width="100%" border="0" class="normalfnt">
                        <tr>
                          <td style="text-align:left;border-bottom:dotted">&nbsp;</td>
                          <td style="text-align:left">&nbsp;</td>
                          <td style="text-align:right">&nbsp;</td>
                          <td style="text-align:right;border-bottom:dotted">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="14%" style="text-align:center">Checked By</td>
                            <td width="9%" style="text-align:left">&nbsp;</td>
                            <td width="64%" style="text-align:right">&nbsp;</td>
                            <td width="13%" style="text-align:center">Authorized By</td>
                        </tr>
                        <tr>
                            <td width="14%">&nbsp;</td>
                            <td width="9%">&nbsp;</td>
                            <td width="64%">&nbsp;</td>
                            <td width="13%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="14%">&nbsp;</td>
                            <td width="9%">&nbsp;</td>
                            <td width="64%">&nbsp;</td>
                            <td width="13%">&nbsp;</td>
                        </tr>
                        <tr>
                          <td style="text-align:center;border-bottom:dotted"><b><?php echo $obj_payVoucher_get->getPreparedBy($header_arry['CREATED_BY']); ?></b></td>
                          <td style="text-align:left">&nbsp;</td>
                          <td style="text-align:right">&nbsp;</td>
                          <td style="text-align:right;border-bottom:dotted">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="14%" style="text-align:center">Prepared By</td>
                            <td width="9%" style="text-align:left">&nbsp;</td>
                            <td width="64%" style="text-align:right">&nbsp;</td>
                            <td width="13%" style="text-align:center">Received By</td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            
            </td>
        </tr>
    </table>
</form>
</body>
</html>
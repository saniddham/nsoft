<?php
session_start();
$session_companyId	= $_SESSION['headCompanyId'];
//BEGIN - INCLUDE FILES {
$backwardseperator 	= "../../../../";
require_once ("{$backwardseperator}dataAccess/Connector.php");
require_once ("../../../../libraries/fpdf/fpdf.php");
//END	- }

$date	= strtoupper(date('M').''.date('y'));
$date_array	= str_split($date);
$border		= 0;
$border1	= 1;
class PDF extends FPDF
{
	private $db;
}

$pdf 					= new PDF('P','mm','A4');

$company_array	= GetCompanyDetails($session_companyId);
$result	= GetDetails($session_companyId);
while($row = mysqli_fetch_array($result))
{
$pdf->AddPage();
$pdf->Cell(0,260,'',1,0);
$pdf->SetXY(10,10);
$pdf->SetFont('Times','B',9);$pdf->Cell(0,4,'11A',0,1,'R');
$pdf->SetFont('Times','',8);$pdf->Cell(0,5,'PART 1 : Sec(I) - GAZETTE EXTRAORDINARY OF THE DEMOCRATIC SOCIALIST REPUBLIC OF SRI LANKA - 11.11.1998',0,1,'C');
$pdf->Cell(0,5,'','B',0,'C');

$pdf->Ln(7);
$pdf->SetFont('Times','',9);$pdf->Cell(0,5,'1.',0,1,'L');
$pdf->Cell(5,5,'',0,0,'L');
$pdf->SetFont('Times','',9);$pdf->Cell(25,5,'Month & Year',0,0,'L');
$pdf->Cell(5,5,$date_array[0],1,0,'L');$pdf->Cell(5,5,$date_array[1],1,0,'L');$pdf->Cell(5,5,$date_array[2],1,0,'L');$pdf->Cell(5,5,$date_array[3],1,0,'L');$pdf->Cell(5,5,$date_array[4],1,0,'L');
$pdf->SetFont('Times','B',11);$pdf->Cell(95,5,'GOODS RECEIVED NOTE',0,0,'C');
$pdf->SetFont('Times','',8);$pdf->Cell(40,5,'Annex II C',0,0,'L');

$pdf->Ln(10);
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'2.','T',0,'L');
$pdf->SetFont('Times','B',10);$pdf->Cell(100,5,'Name and Address of Indirect Exporter & TIN ','T',0,'L');
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'3.','LT',0,'L');
$pdf->SetFont('Times','B',10);$pdf->Cell(37,5,'Assessment No.','T',0,'L');
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'4.','LT',0,'L');
$pdf->SetFont('Times','B',10);$pdf->Cell(38,5,'GRN Ref No.','T',0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(100,5,$company_array["COMPANY_NAME"],$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','L',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(37,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','L',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(38,5,'',$border,0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(100,5,$company_array["COMPANY_ADDRESS"],$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','LB',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(37,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','L',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(38,5,'',$border,0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(100,5,$company_array["COMPANY_CITY"],$border,0,'L');
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'5.','L',0,'L');
$pdf->SetFont('Times','B',10);$pdf->Cell(37,5,'Approval No.','T',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','L',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(38,5,'','0',0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(100,5,$company_array["COMPANY_COUNTRY"],$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','L',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(37,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','L',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(38,5,'',0,0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(100,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','L',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(37,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','L',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(38,5,'',0,0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'6.','T',0,'L');
$pdf->SetFont('Times','B',10);$pdf->Cell(100,5,'Name and Address of Final Exporter & TIN','T',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','LT',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(37,5,'','T',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','T',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(38,5,'','T',0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(100,5,$row["CUSTOMER_NAME"],$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','L',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(37,5,'Remarks :',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(38,5,'',$border,0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(100,5,$row["CUSTOMER_ADDRESS"],$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','L',0,'L');
$pdf->SetFont('Times','',8);$pdf->Cell(37,5,'Raw material usage and value of duty and other levies to be credited.',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(38,5,'',$border,0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(100,5,$row["CUSTOMER_CITY"],$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','L',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(37,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(38,5,'',$border,0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(100,5,$row["CUSTOMER_COUNTRY"],$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','L',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(37,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(38,5,'',$border,0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(100,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','L',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(37,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(38,5,'',$border,0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(100,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','L',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(37,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',$border,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(38,5,'',$border,0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'7.','T',0,'L');
$pdf->SetFont('Times','B',9);$pdf->Cell(100,5,'Description of Goods With Specification','T',0,'C');
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'8.','LT',0,'L');
$pdf->SetFont('Times','B',9);$pdf->Cell(20,5,'H S Code','T',0,'C');
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'9.','LT',0,'L');
$pdf->SetFont('Times','B',9);$pdf->Cell(15,5,'Unit of ','T',0,'C');
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'10.','LT',0,'L');
$pdf->SetFont('Times','B',9);$pdf->Cell(15,5,'Quantity','T',0,'C');
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'11.','LT',0,'L');
$pdf->SetFont('Times','B',9);$pdf->Cell(15,5,'Value','T',0,'C');

$pdf->Ln();
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'',0,0,'L');
$pdf->SetFont('Times','B',9);$pdf->Cell(100,5,'',0,0,'L');
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'','LB',0,'L');
$pdf->SetFont('Times','B',9);$pdf->Cell(20,5,'','B',0,'L');
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'','LB',0,'L');
$pdf->SetFont('Times','B',9);$pdf->Cell(15,5,'Measure','B',0,'C');
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'','LB',0,'L');
$pdf->SetFont('Times','B',9);$pdf->Cell(15,5,'','B',0,'L');
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'','LB',0,'L');
$pdf->SetFont('Times','B',9);$pdf->Cell(15,5,'(US$)','B',0,'C');

$pdf->Ln();
$pdf->SetFont('Times','',8);$pdf->Cell(5,5,'1.',1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(100,5,'Screen prinitng',1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'999906','B',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(20,5,'','B',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','LB',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(15,5,'','B',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','LB',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(15,5,'','B',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(20,5,number_format($row["FINAL_VALUE"],2),'LB',0,'R');

for($i=2;$i<=5;$i++)
{
$pdf->Ln();
$pdf->SetFont('Times','',8);$pdf->Cell(5,5,$i.'.',1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(100,5,'',1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','B',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(20,5,'','B',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','LB',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(15,5,'','B',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','LB',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(15,5,'','B',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','LB',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(15,5,'','B',0,'L');
}

$pdf->Ln();
$pdf->SetFont('Times','',10);$pdf->Cell(130,5,'','B',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(20,5,'Totals','B',0,'R');
$pdf->SetFont('Times','',10);$pdf->Cell(20,5,'',1,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(20,5,number_format($row["FINAL_VALUE"],2),1,0,'R');

$pdf->Ln();
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'12.',0,0,'L');
$pdf->SetFont('Times','B',10);$pdf->Cell(20,5,'Unit of',0,0,'C');
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'13.','L',0,'L');
$pdf->SetFont('Times','B',10);$pdf->Cell(20,5,'Quantity',0,0,'C');
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'14.','L',0,'L');
$pdf->SetFont('Times','B',10);$pdf->Cell(95,5,'Bond / TIEP / Approval No',0,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(40,5,'','L',0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',0,0,'L');
$pdf->SetFont('Times','B',10);$pdf->Cell(20,5,'Measure',0,0,'C');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','L',0,'L');
$pdf->Cell(20,5,'',0,0,'L');
$pdf->Cell(5,5,'',1,0,'L');
$pdf->Cell(5,5,'',1,0,'L');
$pdf->Cell(10,5,'',1,0,'L');
$pdf->Cell(5,5,'',1,0,'L');
$pdf->Cell(5,5,'',1,0,'L');
$pdf->Cell(5,5,'',1,0,'L');
$pdf->Cell(5,5,'',1,0,'L');
$pdf->Cell(5,5,'',1,0,'L');
$pdf->Cell(5,5,'',1,0,'L');
$pdf->Cell(5,5,'',1,0,'L');
$pdf->Cell(5,5,'',1,0,'L');
$pdf->Cell(10,5,'',1,0,'L');
$pdf->Cell(15,5,'',1,0,'L');
$pdf->Cell(15,5,'',1,0,'L');
$pdf->Cell(40,5,'','L',0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',9);$pdf->Cell(10,5,'1','TR',0,'R');
$pdf->SetFont('Times','',10);$pdf->Cell(15,5,'','T',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','LT',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(20,5,'','T',0,'L');
$pdf->SetFont('Times','',7);$pdf->Cell(100,5,'We ................................................. (Name of Exporter) certify that we have purchased and received ','LR',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(40,5,'','T',0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',9);$pdf->Cell(10,5,'2','TR',0,'R');
$pdf->SetFont('Times','',10);$pdf->Cell(15,5,'','T',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','LT',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(20,5,'','T',0,'L');
$pdf->SetFont('Times','',7);$pdf->Cell(100,5,'into our factory/bonded warehouse at ........................ (address) on ......................... (date) the locally','LR',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(40,5,'',0,0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',9);$pdf->Cell(10,5,'3','TR',0,'R');
$pdf->SetFont('Times','',10);$pdf->Cell(15,5,'','T',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','LT',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(20,5,'','T',0,'L');
$pdf->SetFont('Times','',7);$pdf->Cell(100,5,'produced goods fully described in the GRN. ** We undertake that the locally  produced  goods will','LR',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(40,5,'',0,0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',9);$pdf->Cell(10,5,'4','TR',0,'R');
$pdf->SetFont('Times','',10);$pdf->Cell(15,5,'','T',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','LT',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(20,5,'','T',0,'L');
$pdf->SetFont('Times','',7);$pdf->Cell(100,5,'be used by us exclusively for export. We hereby authorize the customs to duly debit out','LR',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(40,5,'',0,0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',9);$pdf->Cell(10,5,'5','TR',0,'R');
$pdf->SetFont('Times','',10);$pdf->Cell(15,5,'','T',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','LT',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(20,5,'','T',0,'L');
$pdf->SetFont('Times','',7);$pdf->Cell(100,5,'stock/bank guarantee registers with these particulars on or before* .../..../.... (date). No claim will','LR',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(40,5,'',0,0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'','TB',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(20,5,'','TB',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'','LB',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(20,5,'','B',0,'L');
$pdf->SetFont('Times','',7);$pdf->Cell(100,5,'be made for duty rebate in respect of goods supplied herein.','LRB',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(40,5,'','B',0,'L');

/*$pdf->Ln();
$pdf->SetFont('Times','',9);$pdf->Cell(0,5,'',1,0,'L');*/

$pdf->Ln();
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'15.',0,0,'L');
$pdf->SetFont('Times','B',10);$pdf->Cell(80,5,'Accepted above in good order',0,0,'L');
$pdf->SetFont('Times','',9);$pdf->Cell(5,5,'16.','L',0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(100,5,'',0,0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',8);$pdf->Cell(5,5,'',0,0,'L');
$pdf->SetFont('Times','',8);$pdf->Cell(80,5,'Name of Company : ..............................................................................',0,0,'L');
$pdf->SetFont('Times','',8);$pdf->Cell(5,5,'','L',0,'L');
$pdf->SetFont('Times','',8);$pdf->Cell(100,5,'Name of Authorized Signatory : Dulip Gayan Ranchagoda',0,0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',8);$pdf->Cell(5,5,'',0,0,'L');
$pdf->SetFont('Times','',8);$pdf->Cell(80,5,'Name of Signatory : ......................... Designation : ..............................',0,0,'L');
$pdf->SetFont('Times','',8);$pdf->Cell(5,5,'','L',0,'L');
$pdf->SetFont('Times','',8);$pdf->Cell(100,5,'Designation : Accountant',0,0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',8);$pdf->Cell(5,5,'',0,0,'L');
$pdf->SetFont('Times','',8);$pdf->Cell(80,5,'Signature : .............................................................................................',0,0,'L');
$pdf->SetFont('Times','',8);$pdf->Cell(5,5,'','L',0,'L');
$pdf->SetFont('Times','',8);$pdf->Cell(100,5,'Signature : .........................................................................................................................',0,0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',8);$pdf->Cell(5,5,'','B',0,'L');
$pdf->SetFont('Times','',8);$pdf->Cell(80,5,'Date : .....................................................................................................','B',0,'L');
$pdf->SetFont('Times','',8);$pdf->Cell(5,5,'','LB',0,'L');
$pdf->SetFont('Times','',8);$pdf->Cell(100,5,'Date : .................................................................................................................................','B',0,'L');

$pdf->Ln(10);
$pdf->SetFont('Times','',10);$pdf->Cell(5,5,'',0,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(80,5,'Dark cage for the use of final exporter.',0,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(105,5,'',0,0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',10);$pdf->Cell(10,5,'',0,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(85,5,'* Not more than 30 days from the date of purchase',0,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(105,5,'',0,0,'L');

$pdf->Ln();
$pdf->SetFont('Times','',10);$pdf->Cell(10,5,'',0,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(85,5,'**  Not applicable for suppliers under rule',0,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(105,5,'',0,0,'L');

$pdf->Ln(10);
$code_WH_array	= str_split($company_array["WH_CODE"]);
$pdf->SetFont('Times','',10);$pdf->Cell(5,8,'',0,0,'L');
$pdf->SetFont('Times','',10);$pdf->Cell(30,8,'WH Code',0,0,'C');
$pdf->Cell(8,8,$code_WH_array[0],1,0,'C');
$pdf->Cell(8,8,$code_WH_array[1],1,0,'C');
$pdf->Cell(8,8,$code_WH_array[2],1,0,'C');
$pdf->Cell(8,8,$code_WH_array[3],1,0,'C');
$pdf->Cell(8,8,$code_WH_array[4],1,0,'C');
$pdf->Cell(8,8,$code_WH_array[5],1,0,'C');
$pdf->Cell(8,8,$code_WH_array[6],1,0,'C');
$pdf->Cell(8,8,$code_WH_array[7],1,0,'C');
$pdf->Cell(8,8,$code_WH_array[8],1,0,'C');
$pdf->Cell(8,8,$code_WH_array[9],1,0,'C');
$pdf->Cell(8,8,$code_WH_array[10],1,0,'C');
$pdf->Cell(8,8,$code_WH_array[11],1,0,'C');
$pdf->Cell(8,8,$code_WH_array[12],1,0,'C');
$pdf->Cell(8,8,$code_WH_array[13],1,0,'C');
$pdf->Cell(8,8,$code_WH_array[14],1,0,'C');
$pdf->Cell(8,8,$code_WH_array[15],1,0,'C');

$pdf->Ln(12);
$pdf->Cell(5,8,'',0,0,'L');
$pdf->Cell(79,8,'',0,0,'L');
$pdf->Cell(15,8,'CPC1',0,0,'L');
$pdf->Cell(8,8,'',1,0,'L');
$pdf->Cell(8,8,'',1,0,'L');
$pdf->Cell(8,8,'',1,0,'L');
$pdf->Cell(8,8,'',1,0,'L');
$pdf->Cell(8,8,'',1,0,'L');
$pdf->Cell(8,8,'',1,0,'L');
$pdf->Cell(8,8,'',1,0,'L');
$pdf->Cell(8,8,'',1,0,'L');

$pdf->Ln();
$pdf->Cell(5,8,'',0,0,'L');
$pdf->Cell(79,8,'12 - 228',0,0,'L');
$pdf->Cell(15,8,'CPC2',0,0,'L');
$pdf->Cell(8,8,'',1,0,'L');
$pdf->Cell(8,8,'',1,0,'L');
$pdf->Cell(8,8,'',1,0,'L');
$pdf->Cell(8,8,'',1,0,'L');
$pdf->Cell(8,8,'',1,0,'L');
$pdf->Cell(8,8,'',1,0,'L');
$pdf->Cell(8,8,'',1,0,'L');
$pdf->Cell(8,8,'',1,0,'L');
}
$pdf->Output();


function GetDetails($companyId)
{
	global $db;
	
	/*$sql = "SELECT
			  C.strName    								AS CUSTOMER_NAME,
			  C.strAddress 								AS CUSTOMER_ADDRESS,
			  C.strCity   								AS CUSTOMER_CITY,
			  CO1.strCountryName  						AS CUSTOMER_COUNTRY,
			  COM.strName								AS COMPANY_NAME,
			  LO.strAddress								AS COMPANY_ADDRESS,
			  LO.strCity								AS COMPANY_CITY,
			  LO.strDistrict							AS COMPANY_COUNTRY,
			  COM.strWHCode								AS WH_CODE,
			  ROUND(COALESCE(SUM(VALUE),0),2) 			AS INVOICE_VALUE
			FROM finance_customer_transaction CT
			  INNER JOIN trn_orderheader OH ON OH.intOrderNo = CT.ORDER_NO AND OH.intOrderYear = CT.ORDER_YEAR
			  INNER JOIN mst_customer C
				ON C.intId = CT.CUSTOMER_ID
			  INNER JOIN mst_country CO1 ON CO1.intCountryID = C.intCountryId 
			  INNER JOIN mst_locations LO ON LO.intId = OH.intLocationId  
  			  INNER JOIN mst_companies COM ON COM.intId = LO.intCompanyId  
			WHERE DOCUMENT_TYPE = 'INVOICE'
			GROUP BY CT.CUSTOMER_ID
			ORDER BY C.strName";*/
	
	$sql = "SELECT 
				CUSTOMER_ID,
				CUSTOMER_NAME,
				CUSTOMER_ADDRESS,
				CUSTOMER_CITY,
				CUSTOMER_COUNTRY,
				SUM(INVOICE_VALUE) - SUM(CREDIT_VALUE) AS FINAL_VALUE
			FROM 
			(SELECT
			  C.intId						AS CUSTOMER_ID,				
			  C.strName          					AS CUSTOMER_NAME,
			  C.strAddress       					AS CUSTOMER_ADDRESS,
			  C.strCity          					AS CUSTOMER_CITY,
			  CO1.strCountryName 					AS CUSTOMER_COUNTRY,
			  
			  (SELECT
				 ROUND(COALESCE(SUM(VALUE),0),2)
			   FROM finance_customer_transaction CT_SUB1
			   WHERE CT.DOCUMENT_NO = CT_SUB1.DOCUMENT_NO
				   AND CT.DOCUMENT_YEAR = CT_SUB1.DOCUMENT_YEAR
				   AND CT_SUB1.DOCUMENT_TYPE = 'INVOICE') 		AS INVOICE_VALUE,
			  (SELECT
				 ABS(ROUND(COALESCE(SUM(VALUE),0),2))
			   FROM finance_customer_transaction CT_SUB1
			   WHERE CT.DOCUMENT_NO = CT_SUB1.DOCUMENT_NO
				   AND CT.DOCUMENT_YEAR = CT_SUB1.DOCUMENT_YEAR
				   AND CT_SUB1.DOCUMENT_TYPE = 'CREDIT') 		AS CREDIT_VALUE
			FROM finance_customer_transaction CT
			  INNER JOIN mst_customer C
				ON C.intId = CT.CUSTOMER_ID
			  INNER JOIN mst_country CO1
				ON CO1.intCountryID = C.intCountryId
			WHERE CT.COMPANY_ID = $companyId
				AND DATE(CT.TRANSACTION_DATE_TIME) BETWEEN '2013-11-01'
				AND '2013-11-30'
				) AS T1
				GROUP BY CUSTOMER_ID
				HAVING (SUM(INVOICE_VALUE) - SUM(CREDIT_VALUE)) > 0";
	return $db->RunQuery($sql);
}

function GetCompanyDetails($companyId)
{
	global $db;
	
	$sql = "SELECT
				C.strName					AS COMPANY_NAME,
				L.strName 					AS LOCATION_NAME,
				L.strAddress				AS COMPANY_ADDRESS,
				L.strCity					AS COMPANY_CITY,
				CO.strCountryName			AS COMPANY_COUNTRY,	
				C.strWHCode				AS WH_CODE
				FROM
				mst_locations L
				INNER JOIN mst_companies C ON L.intCompanyId = C.intId
				INNER JOIN mst_country CO ON C.intCountryId = CO.intCountryID
				WHERE
				C.intId =  $companyId
				AND L.HEAD_OFFICE_FLAG = 1";
		$result = $db->RunQuery($sql);
		return mysqli_fetch_array($result);
}
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
	$locationId			= $_SESSION["CompanyID"];
	$companyId			= $_SESSION["headCompanyId"];
	
	$fromDate			= $_REQUEST['fromDate'];
	$toDate				= $_REQUEST['toDate'];
	$currency			= $_REQUEST['currency'];;
	$GLAccId			= $_REQUEST['chartOfAccId'];
	
	$payDue_Array		=array(30,60,90,120,'more');
	
	$data['headerArr'] 	= GetHeaderDetails($currency,$GLAccId);
	
?>
<head>
<title>General Ledger - Aging Report</title>
</head>
<body>
<form id="frmGeneralLedgerAging" name="frmGeneralLedgerAging" method="post">
	<table width="1000" align="center">
        <tr>
        	<td><?php include 'reportHeader.php'; ?></td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
        </tr>
        <tr>
        	<td class="reportHeader" style="text-align:center"><strong>General Ledger - Aging Report (<?php echo $data['headerArr']['CURName']; ?>)</strong></td>
        </tr>
        <tr>
            <td class="normalfnt" style="text-align:center">
                <table align="center" width="300" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="64" class="normalfnt"><strong>From&nbsp;&nbsp;:</strong></td>
                        <td width="92" class="normalfnt"><?php echo $fromDate; ?></td>
                        <td width="45" class="normalfnt"><strong>To&nbsp;&nbsp;:</strong></td>
                        <td width="99" class="normalfnt"><?php echo $toDate; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        	<td style="text-align:center">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0">
                    <tr class="normalfnt">
                        <td width="12%">Ledger Account</td>
                        <td width="1%" style="text-align:center">:</td>
                        <td width="45%"><?php echo $data['headerArr']['CAName']; ?></td>
                        <td width="12%">&nbsp;</td>
                        <td width="1%" style="text-align:center">&nbsp;</td>
                        <td width="29%">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        	<td>
            	<table width="100%" class="rptBordered" id="tblMain" border="0">
                    <thead>
                        <tr>
                            <th width="12%">Date</th>
                            <th width="17%">Reference No.</th>
                            <th width="15%">Overdue Days</th>
                            <th width="14%">Due On</th>
                            <th width="18%">Opening Amount (<?php echo $data['headerArr']['CURName']; ?>)</th>
                            <th width="16%">Pending Amount (<?php echo $data['headerArr']['CURName']; ?>)</th>
							<?php 
							$startVal = 0;
							for($i=0;$i<count($payDue_Array);$i++)
							{
								if($payDue_Array[$i]!='more')
									$payDays = $startVal.'-'.$payDue_Array[$i];
								else
									$payDays = $payDue_Array[$i];
								
								$startVal = $payDue_Array[$i]+1;
							?>
                            	<th nowrap="nowrap"><?php echo $payDays;?></th>
                            <?php 
							}
							?>
                        </tr>
                    </thead>
					<?php
					$totOpeningAmt = 0;
					$totPendingAmt = 0;
					
                    $sql = "(SELECT DATE(LAST_MODIFIED_DATE) AS dtDate,
							CURRENCY_ID,
                            INVOICE_NO,
                            INVOICE_YEAR,
                            DOCUMENT_NO,
                            DOCUMENT_YEAR,
                            DOCUMENT_TYPE,
                            TRANSACTION_TYPE,
                            TRANSACTION_CATEGORY,
                            CURRENCY_ID,
                            ROUND(AMOUNT,2) AS amount,
                            CHART_OF_ACCOUNT_ID,
							'' AS orderId
                            FROM finance_transaction
                            WHERE 
                            CHART_OF_ACCOUNT_ID='$GLAccId' AND
                            DOCUMENT_TYPE = 'INVOICE' AND
                            DATE(LAST_MODIFIED_DATE)<='$toDate' AND
							COMPANY_ID='$companyId'
                            ORDER BY LAST_MODIFIED_DATE                             
							)
							UNION 
							(
							SELECT DATE(LAST_MODIFIED_DATE) AS dtDate,
							CURRENCY_ID,
							INVOICE_NO,
							INVOICE_YEAR,
							DOCUMENT_NO,
							DOCUMENT_YEAR,
							DOCUMENT_TYPE,
							TRANSACTION_TYPE,
							TRANSACTION_CATEGORY,
							CURRENCY_ID,
							ROUND(AMOUNT,2) AS amount,
							CHART_OF_ACCOUNT_ID,
							'' AS orderId
							FROM finance_transaction
							WHERE 
							CHART_OF_ACCOUNT_ID='$GLAccId' AND
							DOCUMENT_TYPE = 'ADVANCE' AND  
							INVOICE_NO IS NULL AND
							INVOICE_YEAR IS NULL AND
							DATE(LAST_MODIFIED_DATE)<='$toDate' AND
							COMPANY_ID='$companyId'
							ORDER BY LAST_MODIFIED_DATE                          
							)
							UNION 
							(
							SELECT DATE(LAST_MODIFIED_DATE) AS dtDate,
							CURRENCY_ID,
							INVOICE_NO,
							INVOICE_YEAR,
							DOCUMENT_NO,
							DOCUMENT_YEAR,
							DOCUMENT_TYPE,
							finance_transaction.TRANSACTION_TYPE,
							TRANSACTION_CATEGORY,
							CURRENCY_ID,
							ROUND(finance_journal_entry_details.AMOUNT,2) AS amount,
							finance_transaction.CHART_OF_ACCOUNT_ID,
							finance_journal_entry_details.ORDER_ID AS orderId
							FROM finance_transaction
							INNER JOIN finance_journal_entry_details ON finance_journal_entry_details.JOURNAL_ENTRY_NO=finance_transaction.DOCUMENT_NO AND
							finance_journal_entry_details.JOURNAL_ENTRY_YEAR=finance_transaction.DOCUMENT_YEAR AND 
							finance_journal_entry_details.CHART_OF_ACCOUNT_ID=finance_transaction.CHART_OF_ACCOUNT_ID
							WHERE 
							finance_transaction.CHART_OF_ACCOUNT_ID='$GLAccId' AND
							finance_transaction.DOCUMENT_TYPE = 'JOURNAL_ENTRY' AND  
							finance_journal_entry_details.SETTELEMENT_NO IS NULL AND
							finance_journal_entry_details.SETTELEMENT_YEAR IS NULL AND
							DATE(LAST_MODIFIED_DATE)<='$toDate' AND
							finance_transaction.COMPANY_ID='$companyId'
							GROUP BY finance_journal_entry_details.JOURNAL_ENTRY_YEAR, finance_journal_entry_details.JOURNAL_ENTRY_NO,
							finance_journal_entry_details.ORDER_ID
							ORDER BY LAST_MODIFIED_DATE                          
							) 
							UNION
							(
							SELECT DATE(LAST_MODIFIED_DATE) AS dtDate,
							CURRENCY_ID,
							INVOICE_NO,
							INVOICE_YEAR,
							DOCUMENT_NO,
							DOCUMENT_YEAR,
							DOCUMENT_TYPE,
							TRANSACTION_TYPE,
							TRANSACTION_CATEGORY,
							CURRENCY_ID,
							ROUND(AMOUNT,2) AS amount,
							CHART_OF_ACCOUNT_ID,
							'' AS orderId
							FROM finance_transaction
							WHERE 
							CHART_OF_ACCOUNT_ID='$GLAccId' AND
							DOCUMENT_TYPE = 'DEBIT' AND  
							DATE(LAST_MODIFIED_DATE)<='$toDate' AND
							COMPANY_ID='$companyId'
							ORDER BY LAST_MODIFIED_DATE
							)";
  					
                    $result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$rwAmount = $row['amount'];
						
						/*if($row['TRANSACTION_CATEGORY']=='SU')
						{
							switch($row['TRANSACTION_TYPE'])
							{
								case 'D':
								$amount = $row['amount']*-1;
								break;
								case 'C':
								$amount = $row['amount'];
								break;
							}
						}
						else
						{
							switch($row['TRANSACTION_TYPE'])
							{
								case 'D':
								$amount = $row['amount'];
								break;
								case 'C':
								$amount = $row['amount']*-1;
								break;
							}
						}*/
						$savedCurRate 	= getCurrencyRate($row['CURRENCY_ID'],$row['dtDate']);
						$targetCurRate  = getCurrencyRate($currency,$row['dtDate']);
						$amount 		= ($rwAmount/$targetCurRate)*$savedCurRate;
						
						$dtdate			= $row['dtDate'];
						$currDate		= date('Y-m-d');
						$referenceNo	= getReferenceNo($row['DOCUMENT_NO'],$row['DOCUMENT_YEAR'],$row['DOCUMENT_TYPE'],$row['TRANSACTION_CATEGORY'],$row['CHART_OF_ACCOUNT_ID'],$row['orderId']);
						$invoiceRemarks	= getInvoiceRemarks($row['TRANSACTION_CATEGORY'],$row['DOCUMENT_TYPE'],$row['DOCUMENT_NO'],$row['DOCUMENT_YEAR']);
						$pendingAmount	= getPendingAmount($row['DOCUMENT_NO'],$row['DOCUMENT_YEAR'],$row['DOCUMENT_TYPE'],$row['TRANSACTION_CATEGORY'],$row['orderId'],$rwAmount);
						$paymentTerm	= getPaymentTerm($row['DOCUMENT_NO'],$row['DOCUMENT_YEAR'],$row['DOCUMENT_TYPE'],$row['TRANSACTION_CATEGORY']);
				
						$pendingAmount	= ($pendingAmount/$targetCurRate)*$savedCurRate;
						
						$totOpeningAmt +=$amount;
						$totPendingAmt +=$pendingAmount;
						
						if($paymentTerm=='')
							$dueOn = '&nbsp;';
						else
							$dueOn = date('Y-m-d', strtotime($dtdate. ' + '.$paymentTerm.' days'));
							
						$start 	= strtotime($dtdate);
						$end 	= strtotime($currDate);
						
						$days_between = ceil(abs($end - $start) / 86400);
					?>
                        <tr class="normalfnt">
                            <td style="text-align:center"><?php echo $row['dtDate']; ?></td>
                            <td style="text-align:left"><?php echo ($invoiceRemarks!=''?$invoiceRemarks.' / '.$referenceNo:$referenceNo); ?></td>
                            <td style="text-align:center"><?php echo $days_between; ?></td>
                            <td style="text-align:center"><?php echo $dueOn; ?></td>
                            <td style="text-align:right"><?php echo number_format($amount,2); ?></td>
                            <td style="text-align:right"><?php echo ($pendingAmount==0?number_format(abs($pendingAmount),2):number_format($pendingAmount,2)); ?></td>
                            <?php
							$start 	= 0;
                            for($i=0;$i<count($payDue_Array);$i++)
							{
								$penAmt = 0;
								
								if($start<=$days_between && $payDue_Array[$i]=='more')
								{
									$penAmt = $pendingAmount;
								}
								elseif($start<=$days_between && $payDue_Array[$i]>=$days_between)
								{
									$penAmt = $pendingAmount;
								}
								else
								{
									$penAmt = 0;
								}
								$payDue_ArrayTot[$payDue_Array[$i]] += $penAmt;
								$start = $payDue_Array[$i]+1;
								
							?>
                            	<td nowrap="nowrap" style="text-align:right"><?php echo ($penAmt==0?'&nbsp;':number_format($penAmt,2)); ?></td>
                            <?php
							}
							?>
                        </tr>
                    <?php
					}
                    ?>
                    <tr class="normalfnt">
                        <td colspan="3" style="text-align:center">&nbsp;</td>
                        <td style="text-align:left"><strong>Total</strong></td>
                        <td style="text-align:right"><b><?php echo number_format($totOpeningAmt,2); ?></b></td>
                        <td style="text-align:right"><b><?php echo ($totPendingAmt==0?number_format(abs($totPendingAmt),2):number_format($totPendingAmt,2)); ?></b></td>
                        <?php 
						for($i=0;$i<count($payDue_Array);$i++)
						{
						?>
        					<td nowrap="nowrap" style="text-align:right;"><b><?php echo ($payDue_ArrayTot[$payDue_Array[$i]]==0?'&nbsp;':number_format($payDue_ArrayTot[$payDue_Array[$i]],2)) ?></b></td>                   
       				 	<?php 
						} 
						?>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
<?php
function GetHeaderDetails($currency,$GLAccId)
{
	global $db;
	
	$sqlCUR = " SELECT strCode
				FROM mst_financecurrency
				WHERE intId='$currency' ";
	
	$resultCUR 	= $db->RunQuery($sqlCUR);
	$rowCUR 	= mysqli_fetch_array($resultCUR);
	
	$sqlCA  = " SELECT CHART_OF_ACCOUNT_NAME
				FROM finance_mst_chartofaccount
				WHERE CHART_OF_ACCOUNT_ID='$GLAccId' ";
	
	$resultCA 	= $db->RunQuery($sqlCA);
	$rowCA		= mysqli_fetch_array($resultCA);
	
	$data['CAName']  = $rowCA['CHART_OF_ACCOUNT_NAME'];
	$data['CURName'] = $rowCUR['strCode'];
	
	return $data;
}
function getReferenceNo($docNo,$docYear,$docType,$transCat,$chartOfAccId,$orderId)
{
	global $db;
	
	if($docType=='ADVANCE')
	{
		switch($transCat)
		{
			case 'SU':
			$referenceNo = getReferenceNoAdvData('finance_supplier_advancepayment_header','PO_NO','PO_YEAR','ADVANCE_PAYMENT_NO','ADVANCE_PAYMENT_YEAR',$docNo,$docYear);
			break;
			case 'CU':
			$referenceNo = getReferenceNoAdvData('finance_customer_advance_header','ORDER_NO','ORDER_YEAR','ADVANCE_NO','ADVANCE_YEAR',$docNo,$docYear);
			break;
		}
	}
	if($docType=='INVOICE')
	{
		switch($transCat)
		{
			case 'SU':
			$referenceNo = getReferenceNoInvData('finance_supplier_purchaseinvoice_header','PURCHASE_INVOICE_NO','PURCHASE_INVOICE_YEAR',$docNo,$docYear);
			break;
			case 'CU':
			$referenceNo = getReferenceNoInvData('finance_customer_invoice_header','SERIAL_NO','SERIAL_YEAR',$docNo,$docYear);
			break;
		}
	}
	if($docType=='DEBIT')
	{
		switch($transCat)
		{
			//case 'SU':
			//$referenceNo = getReferenceNoInvData('finance_supplier_purchaseinvoice_header','PURCHASE_INVOICE_NO','PURCHASE_INVOICE_YEAR',$docNo,$docYear);
			//break;
			case 'CU':
			$referenceNo = getReferenceNoDebData('finance_customer_debit_note_header','DEBIT_NO','DEBIT_YEAR',$docNo,$docYear);
			break;
		}
	}
	if($docType=='JOURNAL_ENTRY')
	{
		$referenceNo = getJournalReferenceNo($docNo,$docYear,$chartOfAccId,$orderId);
	}
	return $referenceNo;
}
function getInvoiceRemarks($transactionCat,$docType,$docNo,$docYear)
{
	global $db;
	$invoiceRemarks = '';
	
	if($transactionCat=='CU')
	{
		if($docType=='INVOICE')
		{
			$sql = "SELECT REMARKS
					FROM finance_customer_invoice_header
					WHERE finance_customer_invoice_header.SERIAL_NO='$docNo' AND finance_customer_invoice_header.SERIAL_YEAR='$docYear'";
			$result = $db->RunQuery($sql);
			$row = mysqli_fetch_array($result);
			
			$invoiceRemarks = $row['REMARKS'];
		}
		
	}
	return $invoiceRemarks;
}
function getReferenceNoAdvData($tableName,$refNo,$refYear,$docNoFeild,$docYearFeild,$docNo,$docYear)
{
	global $db;
	
	$sql = "SELECT CONCAT(APH.$docNoFeild,'/',APH.$docYearFeild) AS referenceNo
			FROM $tableName AS APH
			WHERE $docNoFeild='$docNo' AND
			$docYearFeild='$docYear' ";
	
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	
	return $row['referenceNo'];
}
function getReferenceNoInvData($tableName,$docNoFeild,$docYearFeild,$docNo,$docYear)
{
	global $db;
	
	$sql = "SELECT IH.INVOICE_NO AS referenceNo
			FROM $tableName AS IH
			WHERE $docNoFeild='$docNo' AND
			$docYearFeild='$docYear' ";
	
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	
	return $row['referenceNo'];
}
function getReferenceNoDebData($tableName,$docNoFeild,$docYearFeild,$docNo,$docYear)
{
	global $db;
	
	$sql = "SELECT CONCAT(DNH.$docNoFeild,'/',DNH.$docYearFeild) AS referenceNo
			FROM $tableName AS DNH
			WHERE $docNoFeild='$docNo' AND
			$docYearFeild='$docYear' ";
	
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	
	return $row['referenceNo'];
}
function getPendingAmount($docNo,$docYear,$docType,$transCat,$orderId,$rwAmount)
{
	global $db;
	
	if($docType=='ADVANCE')
	{
		switch($transCat)
		{
			case 'SU':
			$pendingAmt = getPendingAmtAdvData('finance_supplier_transaction','PURCHASE_INVOICE_NO','PURCHASE_INVOICE_YEAR',$docNo,$docYear);
			break;
			case 'CU':
			$pendingAmt = getPendingAmtAdvData('finance_customer_transaction','INVOICE_NO','INVOICE_YEAR',$docNo,$docYear);
			break;
		}
	}
	if($docType=='INVOICE')
	{
		switch($transCat)
		{
			case 'SU':
			$pendingAmt = getPendingAmtInvData('finance_supplier_transaction','PURCHASE_INVOICE_NO','PURCHASE_INVOICE_YEAR',$docNo,$docYear);
			break;
			case 'CU':
			$pendingAmt = getPendingAmtInvData('finance_customer_transaction','INVOICE_NO','INVOICE_YEAR',$docNo,$docYear);
			break;
		}
	}
	if($docType=='DEBIT')
	{
		switch($transCat)
		{
			//case 'SU':
			//$pendingAmt = getPendingAmtInvData('finance_supplier_transaction','PURCHASE_INVOICE_NO','PURCHASE_INVOICE_YEAR',$docNo,$docYear);
			//break;
			case 'CU':
			$pendingAmt = getPendingAmtDebData('finance_customer_transaction','DEBIT_NO','DEBIT_YEAR',$docNo,$docYear);
			break;
		}
	}
	if($docType=='JOURNAL_ENTRY')
	{
		$pendingAmt = getJournalPendingAmt($docNo,$docYear,$orderId,$rwAmount);
		
	}
	return $pendingAmt;
}
function getPendingAmtAdvData($tableName,$invNoFeild,$invYearFeild,$docNo,$docYear)
{
	global $db;
	global $companyId;
	
	$sql = "SELECT round(SUM(FT.VALUE),2) AS pendingAmt
			FROM $tableName FT
			WHERE 
			FT.DOCUMENT_NO='$docNo' AND
			FT.DOCUMENT_YEAR='$docYear' AND
			FT.DOCUMENT_TYPE='ADVANCE' AND
			FT.$invNoFeild IS NULL AND
			FT.$invYearFeild IS NULL AND
			COMPANY_ID='$companyId' ";
	
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	
	return $row['pendingAmt'];
}
function getPendingAmtInvData($tableName,$invNoFeild,$invYearFeild,$docNo,$docYear)
{
	global $db;
	global $companyId;
	
	$sql = "SELECT round(SUM(FT.VALUE),2) AS pendingAmt
			FROM $tableName FT
			WHERE 
			FT.$invNoFeild='$docNo' AND
			FT.$invYearFeild='$docYear' AND
			COMPANY_ID='$companyId' ";
	
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	
	return $row['pendingAmt'];
}
function getPendingAmtDebData($tableName,$debitNoFeild,$debitYearFeild,$docNo,$docYear)
{
	global $db;
	global $companyId;
	
	$sql = "SELECT round(SUM(FT.VALUE),2) AS pendingAmt
			FROM $tableName FT
			WHERE 
			FT.$debitNoFeild='$docNo' AND
			FT.$debitYearFeild='$docYear' AND
			COMPANY_ID='$companyId' ";
	
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	
	return $row['pendingAmt'];
}
function getPaymentTerm($docNo,$docYear,$docType,$transCat)
{
	global $db;
	
	if($docType=='INVOICE')
	{
		switch($transCat)
		{
			case 'SU':
			$payTerm = getPayTerm('finance_supplier_transaction','PO_NO','PO_YEAR','PURCHASE_INVOICE_NO','PURCHASE_INVOICE_YEAR',$docNo,$docYear,'trn_poheader','intPONo','intPOYear');
			break;
			case 'CU':
			$payTerm = getPayTerm('finance_customer_transaction','ORDER_NO','ORDER_YEAR','INVOICE_NO','INVOICE_YEAR',$docNo,$docYear,'trn_orderheader','intOrderNo','intOrderYear');
			break;
		}
	}
	else
		$payTerm = '';
	
	return $payTerm;
}
function getPayTerm($tableName,$orderNoFld,$orderYearFld,$invNoFld,$invYearFld,$docNo,$docYear,$jtable,$jtablepara1,$jtablepara2)
{
	global $db;
	global $companyId;
	
	$sql = "SELECT DISTINCT mst_financepaymentsterms.strName
			FROM $tableName FT
			INNER JOIN $jtable ON $jtable.$jtablepara1=FT.$orderNoFld AND $jtable.$jtablepara2=FT.$orderYearFld
			INNER JOIN mst_financepaymentsterms ON mst_financepaymentsterms.intId=$jtable.intPaymentTerm
			WHERE $invNoFld='$docNo' AND
			$invYearFld='$docYear' AND
			DOCUMENT_TYPE='INVOICE' AND
			COMPANY_ID='$companyId' ";
	
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	
	return $row['strName'];		
}
function getCurrencyRate($currency,$date)
{
	global $db;
	global $companyId;
	
	$sql = "SELECT dblExcAvgRate
			FROM mst_financeexchangerate
			WHERE intCurrencyId='$currency' AND
			dtmDate='$date' AND
			intCompanyId='$companyId' ";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);

	return $row['dblExcAvgRate'];
}
function getJournalReferenceNo($docNo,$docYear,$chartOfAccId,$orderId)
{
	global $db;
	
	$sql = "SELECT CONCAT(remarks,'-',JOURNAL_ENTRY_NO,'/',JOURNAL_ENTRY_YEAR) AS referenceNo
			FROM
			(SELECT JED1.JOURNAL_ENTRY_NO,
			JED1.JOURNAL_ENTRY_YEAR,
			(SELECT REMARKS 
			FROM finance_journal_entry_details JED2
			WHERE JED2.JOURNAL_ENTRY_NO=JED1.JOURNAL_ENTRY_NO AND
			JED2.JOURNAL_ENTRY_YEAR=JED1.JOURNAL_ENTRY_YEAR AND
			JED2.CHART_OF_ACCOUNT_ID=JED1.CHART_OF_ACCOUNT_ID AND
			JED2.ORDER_ID=JED1.ORDER_ID
			) AS remarks
			FROM finance_journal_entry_details JED1
			WHERE JED1.JOURNAL_ENTRY_NO='$docNo' AND
			JED1.JOURNAL_ENTRY_YEAR='$docYear' AND
			JED1.CHART_OF_ACCOUNT_ID='$chartOfAccId' AND
			JED1.ORDER_ID ='$orderId'
			) AS TBL_1";
	
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	
	return $row['referenceNo'];
	
}
function getJournalPendingAmt($docNo,$docYear,$orderId,$rwAmount)
{
	global $db;
	
	$sql = "SELECT ROUND(SUM(BAL_AMOUNT),2) AS pendingAmt
			FROM finance_journal_entry_details
			WHERE JOURNAL_ENTRY_NO='$docNo' AND
			JOURNAL_ENTRY_YEAR='$docYear' AND
			ORDER_ID='$orderId' ";

	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	
	$pendingAmt = $row['pendingAmt'];
	return $pendingAmt;
}
?>
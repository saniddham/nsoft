var pub_Id				= 0;
var basePath			= "presentation/finance_new/reports/";
var GLSumReportId		= 968;
var GLDetReportId		= 966;
var GLLogReportId		= 964;
var cusTrailBalReportId	= 962;
var incStatReportId		= 961;
var incStatCSWReportId	= 958;
var balSheetReportId	= 956;
var svat4ReportId		= 955;
var svat5ReportId		= 952;
var svat5bReportId		= 950;
var trnLogMenuId		= 888;

$(document).ready(function(){
	pub_Id	= 0;
	$('#frmNonMovingOrders').validationEngine();
//BEGIN - GL REPORTS {
	$('#tblGLReportHeader').die('click').live('click',function(){
		headShowHide('GLReportHeader','divGLReportDetail');
	});
	$('#tblGLReportDetail thead').die('click').live('click',function(){	
		bodyShowHide('divGLReportDetail','GLReportHeader');	
	});
//END 	- GL REPORTS }

//BEGIN - TRAIL BALANCE REPORT {	
	$('#tblTrailBalanceHeader').die('click').live('click',function(){
		headShowHide('TrailBalanceHeader','divTrailBalanceHeader');
	});
	$('#tblTrailBalanceDetial thead').die('click').live('click',function(){	
		bodyShowHide('divTrailBalanceHeader','TrailBalanceHeader');	
	});
//END 	- TRAIL BALANCE REPORT }

//BEGIN - INCOME STATEMENT REPORT { 
	$('#tblIncomeStatement').die('click').live('click',function(){
		headShowHide('IncomeStatement','divIncomeStatement');
	});
	$('#tblIncomeStatementHeader thead').die('click').live('click',function(){	
		bodyShowHide('divIncomeStatement','IncomeStatement');	
	});
//END 	- INCOME STATEMENT REPORT }

//BEGIN - BALANCE SHEET REPORT { 
	$('#tblBalanceSheet').die('click').live('click',function(){
		headShowHide('BalanceSheet','divBalanceSheet');
	});
	$('#tblBalanceSheetHeader thead').die('click').live('click',function(){	
		bodyShowHide('divBalanceSheet','BalanceSheet');	
	});
//END 	- BALANCE SHEET REPORT }

//BEGIN - BALANCE SHEET REPORT { 
	$('#tblSVATReports').die('click').live('click',function(){
		headShowHide('SVATReports','divSVATReports');
	});
	$('#tblSVATReportsDetail thead').die('click').live('click',function(){	
		bodyShowHide('divSVATReports','SVATReports');	
	});
//END 	- BALANCE SHEET REPORT }

//BEGIN - BALANCE SHEET REPORT { 
	$('#tblGLMapping').die('click').live('click',function(){
		headShowHide('tr_GLMapping','div_GLMapping');
	});
	$('#tblGLMappingDetails thead').die('click').live('click',function(){	
		bodyShowHide('div_GLMapping','tr_GLMapping');	
	});
	
//END 	- BALANCE SHEET REPORT }
	$('#tblNonMovingOrders').die('click').live('click',function(){	
		bodyShowHide('tr_nonMovingOrders','div_nonMovingOrders');	
	});
	
	$('#tblNonMovingOrdersDetail thead').die('click').live('click',function(){	
		bodyShowHide('div_nonMovingOrders','tr_nonMovingOrders');	
	});
	

//BEGIN - BALANCE SHEET REPORT { 
	$('#tblStatementCashFlow').die('click').live('click',function(){
		headShowHide('tr_statementCashFlow','div_StatementCashFlow');
	});
	$('#tblStatementCashFlowDetails thead').die('click').live('click',function(){	
		bodyShowHide('div_StatementCashFlow','tr_statementCashFlow');	
	});
//END 	- BALANCE SHEET REPORT }

//BEGIN - TRANSACTION REPORT { 
	$('#tblTransactionLog').die('click').live('click',function(){
		headShowHide('','');
		transactionLogReport();
	});
//END 	- TRANSACTION REPORT }

	$('#frmGLReportDetail #cboFinanceType').die('change').live('change',function(){LoadMainType('frmGLReportDetail');});
	$('#frmGLReportDetail #cboMainType').die('change').live('change',function(){LoadSubType('frmGLReportDetail');});
	$('#frmGLReportDetail #cboSubType').die('change').live('change',function(){loadChartOfAcc('frmGLReportDetail');});
	$('#frmGLReportDetail #butSummary').die('click').live('click',function(){GLSummaryReport('summary');});
	$('#frmGLReportDetail #butDetail').die('click').live('click',function(){GLSummaryReport('detail');});
	
	$('#frmTrailBalanceHeader #cboFinanceType').die('change').live('change',function(){LoadMainType('frmTrailBalanceHeader');});
	$('#frmTrailBalanceHeader #cboMainType').die('change').live('change',function(){LoadSubType('frmTrailBalanceHeader');});
	$('#frmTrailBalanceHeader #cboSubType').die('change').live('change',function(){loadChartOfAcc('frmTrailBalanceHeader');});
	$('#frmTrailBalanceHeader #butReport').die('click').live('click',function(){TrialBalanceReport();});
	
	$('#frmIncomeStatement #butReport').die('click').live('click',function(){IncomeStatementReport();});
	$('#frmBalanceSheet #butReport').die('click').live('click',function(){BalanceSheetReport();});
	$('#frmIncomeStatement #cboReportType').die('change').live('change',loadCostCenter);
	$('#frmSVATReports #butReport').die('click').live('click',LoadSVATReport);
	$('#frmGLMapping #butReport').die('click').live('click',viewGLMappingReport);
	$('#frmStatementCashFlow #butReport').die('click').live('click',viewCashFlowStatementReport);
	$('#frmNonMovingOrders #butReport').die('click').live('click',viewNonMovingOrderReport);
	
	$('.previous').die('click').live('click',AddPreviousButton);
	$('.after').die('click').live('click',AddNextButton);
	$('.cls_but_remove').die('click').live('click',RemoveButton);
	$('#frmGLMapping #cboMainCategory').die('change').live('change',function(){loadSubCategory('frmGLMapping',$(this));});
	$('#frmGLMapping #butNew').die('click').live('click',function(){clearForm('frmGLMapping');});
	$('#frmNonMovingOrders #butNew').die('click').live('click',function(){clearForm('frmNonMovingOrders');});
});

function clearForm(formId)
{
	$('#'+formId)[0].reset();
}

function headShowHide(headerId,bodyDivId)
{
	$('#divGLReportDetail').slideUp(1000);
	$('#divTrailBalanceHeader').slideUp(1000);
	$('#divIncomeStatement').slideUp(1000);
	$('#divBalanceSheet').slideUp(1000);
	$('#divSVATReports').slideUp(1000);
	
	$('#GLReportHeader').fadeIn(1500);
	$('#TrailBalanceHeader').fadeIn(1500);
	$('#IncomeStatement').fadeIn(1500);
	$('#BalanceSheet').fadeIn(1500);
	$('#SVATReports').fadeIn(1500);
	
	if(headerId!='' && bodyDivId!='')
	{
		$('#'+headerId).fadeOut(1000);
		$('#'+bodyDivId).slideDown(1000);
	}
}
function bodyShowHide(bodyDivId,headerId)
{
	$('#'+bodyDivId).slideUp(1000);
	$('#'+headerId).fadeIn(1500);
}
function LoadMainType(formId)
{
	$('#'+formId+' #cboMainType').html('');
	$('#'+formId+' #cboSubType').html('');
	$('#'+formId+' #cboChartOfAccount').html('');
	
	if($('#'+formId+' #cboFinanceType').val()=='')
		return;
		
	var url = basePath+"reportSelect-db-get.php?requestType=loadMainType";
	var obj = $.ajax({
		url:url,
		dataType: "json",
		type:'POST',  
		data:"&financeType="+$('#'+formId+' #cboFinanceType').val(),
		async:false,
		success:function(json){
			
			$('#'+formId+' #cboMainType').html(json.mainTypeCombo);			
		},
		error:function(xhr,status){
				
		}		
	});
}
function LoadSubType(formId)
{
	$('#'+formId+' #cboSubType').html('');
	$('#'+formId+' #cboChartOfAccount').html('');
	
	if($('#'+formId+' #cboMainType').val()=='')
		return;
		
	var url = basePath+"reportSelect-db-get.php?requestType=loadSubType";
	var obj = $.ajax({
		url:url,
		dataType: "json",
		type:'POST',  
		data:"&mainType="+$('#'+formId+' #cboMainType').val(),
		async:false,
		success:function(json){
			
			$('#'+formId+' #cboSubType').html(json.subTypeCombo);			
		},
		error:function(xhr,status){
				
		}		
	});
}
function loadChartOfAcc(formId)
{
	$('#'+formId+' #cboChartOfAccount').html('');
	
	if($('#'+formId+' #cboSubType')=='')
		return;
		
	var url = basePath+"reportSelect-db-get.php?requestType=loadChartOfAcc";
	var obj = $.ajax({
		url:url,
		dataType: "json",
		type:'POST',  
		data:"&subType="+$('#'+formId+' #cboSubType').val(),
		async:false,
		success:function(json){
			
			$('#'+formId+' #cboChartOfAccount').html(json.chartOfAccCombo);			
		},
		error:function(xhr,status){
				
		}		
	});
}

function loadSubCategory(formId,obj)
{
	$('#'+formId+' #cboSubCategory').html('');
	
	if($(obj).val()=='')
		return;
		
	var url = basePath+"reportSelect-db-get.php?requestType=URLLoadSubCategory";
	var obj = $.ajax({
		url:url,
		dataType: "json",
		type:'POST',  
		data:"MainCatId="+$(obj).val(),
		async:false,
		success:function(json){			
			$('#'+formId+' #cboSubCategory').html(json.html);			
		},
		error:function(xhr,status){
				
		}		
	});
}
function GLSummaryReport(type)
{
	var currencyId 		= $('#cboCurrency').val();
	var financeType 	= $('#cboFinanceType').val();
	var mainType 		= $('#cboMainType').val();
	var subType 		= $('#cboSubType').val();
	var chartOfAccId 	= $('#cboChartOfAccount').val();
	var fromDate 		= $('#dtFromDate').val();
	var toDate 			= $('#dtToDate').val();
	
	var difference = (Date.parse(toDate) - Date.parse(fromDate)) / (86400000 * 7);
	if(difference<0)
	{
		$('#frmGLReportDetail #butSummary').validationEngine('showPrompt','* Invalid Date Range','fail');
		var t=setTimeout("alertx()",2000);	
		return;
	}
	if(type=='summary')
	{
		var url  	= "?q="+GLSumReportId;
		var rptName = "rptGeneralLedgerSummary.php";
	}
	else
	{
		var url  = "?q="+GLDetReportId;
		var rptName = "rptGeneralLedgerDetail.php";
	}
	
		url += "&fromDate="+fromDate;
		url += "&toDate="+toDate;
		url += "&currency="+currencyId;
		url += "&financeType="+financeType;
		url += "&mainCateId="+mainType;
		url += "&subCatId="+subType;
		url += "&chartOfAccId="+chartOfAccId;
		
	window.open(url,rptName);
}
function transactionLogReport()
{
	var url  = "?q="+trnLogMenuId;
	window.open(url,'rptGeneralLedgerLog.php');
}
function alertx()
{
	$('#frmGLReportDetail #butSummary').validationEngine('hide')	;
}

function TrialBalanceReport()
{
	var url  = "?q="+cusTrailBalReportId;
		url += "&CurrencyId="+$('#frmTrailBalanceHeader #cboCurrency').val();
		url += "&FinanceType="+$('#frmTrailBalanceHeader #cboFinanceType').val();
		url += "&MainType="+$('#frmTrailBalanceHeader #cboMainType').val();
		url += "&SubType="+$('#frmTrailBalanceHeader #cboSubType').val();
		url += "&ChartOfAccount="+$('#frmTrailBalanceHeader #cboChartOfAccount').val();
		url += "&DateFrom="+$('#frmTrailBalanceHeader #dtFromDateTrialBal').val();
		url += "&DateTo="+$('#frmTrailBalanceHeader #dtToDateTrialBal').val();
	window.open(url,'rptCustomer_trial_balance.php');
}

function IncomeStatementReport()
{
	var rptName = $('#frmIncomeStatement #cboReportType').val();
	
	if(rptName=="rpt_income_statement.php")
		var url  = "?q="+incStatReportId;
			
	if(rptName=="rpt_income_statement_cswise.php")
		var url  = "?q="+incStatCSWReportId;
	
		url += "&CurrencyId="+$('#frmIncomeStatement #cboCurrency').val();
		url += "&DateFrom="+$('#frmIncomeStatement #dtFromDateInCome').val();
		url += "&DateTo="+$('#frmIncomeStatement #dtToDateInCome').val();
		url += "&costCenter="+$('#frmIncomeStatement #cboCostCenter').val();
	window.open(url,rptName);
}

function BalanceSheetReport()
{
	var before_array	= "[";
	$('.cls_tr_previous').each(function(){
			before_array += "{";
			before_array += '"DATE_FROM":"'+$(this).find('.cls_td_from').children().val()+'",';
			before_array += '"DATE_TO":"'+$(this).find('.cls_td_to').children().val()+'"';
			before_array +=  '},';
	});

	before_array 		 = before_array.substr(0,before_array.length-1);
	before_array 		+= " ]";
	
	var after_array	= "[";
	$('.cls_tr_next').each(function(){
		after_array += "{";
		after_array += '"DATE_FROM":"'+$(this).find('.cls_td_from').children().val()+'",';
		after_array += '"DATE_TO":"'+$(this).find('.cls_td_to').children().val()+'"';
		after_array +=  '},';
	});

	after_array 		 = after_array.substr(0,after_array.length-1);
	after_array 		+= " ]";
	
	var url  = "?q="+balSheetReportId;
		url += "&CurrencyId="+$('#frmBalanceSheet #cboCurrency').val();
		url += "&DateFrom="+$('#frmBalanceSheet #dtFromDateBS').val();
		url += "&DateTo="+$('#frmBalanceSheet #dtToDateBS').val();
		url += "&Before_Array="+before_array;
		url += "&After_Array="+after_array;
	window.open(url,'rpt_balance_sheet.php');
}

function AddPreviousButton()
{	
	var html = "<tr class=\"cls_tr_previous\">"+
				  "<td class=\"normalfnt\">Previous Date</td>"+
				  "<td width=\"21%\" class=\"normalfnt cls_td_from\"><input type=\"text\" onclick=\"return showCalendar(this.id, '%Y-%m-%d');\" onkeypress=\"return ControlableKeyAccess(event);\" onmouseout=\"EnableRightClickEvent();\" onmousedown=\"DisableRightClickEvent();\" style=\"width:90px;\" id=\"dtFromDateBS"+pub_Id+"\" class=\"txtbox\" value=\""+pub_currDate+"\" name=\"dtFromDateBS"+pub_Id+"\"><input type=\"reset\" onclick=\"return showCalendar(this.id, '%Y-%m-%');\" style=\"visibility:hidden;\" class=\"txtbox\" value=\"\"></td>"+
				  "<td width=\"6%\" class=\"normalfnt\">To</td>"+
				  "<td width=\"21%\" class=\"normalfnt cls_td_to\"><input type=\"text\" onclick=\"return showCalendar(this.id, '%Y-%m-%d');\" onkeypress=\"return ControlableKeyAccess(event);\" onmouseout=\"EnableRightClickEvent();\" onmousedown=\"DisableRightClickEvent();\" style=\"width:90px;\" id=\"dtToDateBS"+pub_Id+"\" class=\"txtbox\" value=\""+pub_currDate+"\" name=\"dtToDateBS"+pub_Id+"\"><input type=\"reset\" onclick=\"return showCalendar(this.id, '%Y-%m-%');\" style=\"visibility:hidden;\" class=\"txtbox\" value=\"\"></td>"+
				  "<td width=\"26%\" class=\"normalfnt\"><a class=\"button red small cls_but_remove\">Remove</a></td>"+
				"</tr>";
	$(this).parent().parent().before(html);
	pub_Id++;
}

function AddNextButton()
{
	var html = "<tr class=\"cls_tr_next\">"+
				  "<td class=\"normalfnt\">Next Date</td>"+
				  "<td width=\"21%\" class=\"normalfnt cls_td_from\"><input type=\"text\" onclick=\"return showCalendar(this.id, '%Y-%m-%d');\" onkeypress=\"return ControlableKeyAccess(event);\" onmouseout=\"EnableRightClickEvent();\" onmousedown=\"DisableRightClickEvent();\" style=\"width:90px;\" id=\"dtFromDateBS"+pub_Id+"\" class=\"txtbox\" value=\""+pub_currDate+"\" name=\"dtFromDateBS"+pub_Id+"\"><input type=\"reset\" onclick=\"return showCalendar(this.id, '%Y-%m-%');\" style=\"visibility:hidden;\" class=\"txtbox\" value=\"\"></td>"+
				  "<td width=\"6%\" class=\"normalfnt\">To</td>"+
				  "<td width=\"21%\" class=\"normalfnt cls_td_to\"><input type=\"text\" onclick=\"return showCalendar(this.id, '%Y-%m-%d');\" onkeypress=\"return ControlableKeyAccess(event);\" onmouseout=\"EnableRightClickEvent();\" onmousedown=\"DisableRightClickEvent();\" style=\"width:90px;\" id=\"dtToDateBS"+pub_Id+"\" class=\"txtbox\" value=\""+pub_currDate+"\" name=\"dtToDateBS"+pub_Id+"\"><input type=\"reset\" onclick=\"return showCalendar(this.id, '%Y-%m-%');\" style=\"visibility:hidden;\" class=\"txtbox\" value=\"\"></td>"+
				  "<td width=\"26%\" class=\"normalfnt\"><a class=\"button red small cls_but_remove\">Remove</a></td>"+
				"</tr>";
	$(this).parent().parent().after(html);
	pub_Id++;
	//$(this).parent().parent().after("<tr>"+$(this).parent().parent().html()+"</tr>");
}

function RemoveButton()
{
	$(this).parent().parent().remove();
}
function loadCostCenter()
{
	
	if($(this).val()=='rpt_income_statement_cswise.php')
		$('#frmIncomeStatement #tr_costCenter').show();
	else
		$('#frmIncomeStatement #tr_costCenter').hide();
}

function LoadSVATReport()
{
	var reportName = $('#frmSVATReports #cboReportType').val();
	if(reportName==3)
		return;
	
	var url	= "presentation/finance_new/reports/supplierReports/svat/"+reportName;
	
	window.open(url,reportName);
}

function viewGLMappingReport()
{
	var url	 = "presentation/finance_new/reports/gl_mapping_report_xls.php?";
		url += "MainCat="+$('#frmGLMapping #cboMainCategory').val();
		url += "&SubCat="+$('#frmGLMapping #cboSubCategory').val();
		url += "&GL="+$('#frmGLMapping #cboChartOfAccount').val();
	
	window.open(url,'gl_mapping_report_xls.php');
}

function viewCashFlowStatementReport()
{
	var url	 = "?q=1106";
		url += "&Date="+$('#frmStatementCashFlow #dtCFToDate').val();
		url += "&Currency="+$('#frmStatementCashFlow #cboCurrency').val();
	
	window.open(url,'cash_flow_statement_report.php');
}
function viewNonMovingOrderReport()
{
	if($('#frmNonMovingOrders').validationEngine('validate'))
	{
	var url	 = "?q=1138";
		url += "&DateFrom="+$('#frmNonMovingOrders #dtFromDateNM').val();
		url += "&DateTo="+$('#frmNonMovingOrders #dtToDateNM').val();
		url += "&Cluster="+$('#frmNonMovingOrders #cboMainCluster').val();
	
	window.open(url,'rptNonMovingOrderDetails.php');
	}
}
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$backwardseperator = "../../../";
//BEGIN - INCLUDE FILES {
include_once ("{$backwardseperator}dataAccess/Connector.php");
//END 	- INCLUDE FILES }

$companyId 				= $_SESSION["headCompanyId"];
$locationId				= $_SESSION["CompanyID"];
$target_currencyId		= $_REQUEST["CurrencyId"];
$dateFrom				= $_REQUEST["DateFrom"];
$dateTo					= $_REQUEST["DateTo"];
$costCenter				= $_REQUEST["costCenter"];
$gross_profit_loss		= 0;

$para					= "CurrencyId=$target_currencyId&DateFrom=$dateFrom&DateTo=$dateTo&costCenter=$costCenter";

if($costCenter=='')
	$coscenterPara = '';
else
	$coscenterPara = " AND (FT.LOCATION_ID = $costCenter OR FT.LOCATION_ID IS NULL) ";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Income Statement</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="800" border="0" align="center">
  <tr>
    <td align="center"><?php include '../../../reportHeader.php'?></td>
  </tr>
  <tr>
    <td class="reportHeader" align="center">Income Statement</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" class="rptBordered">
        <thead>
          <tr>
            <th width="12%">Finance Type</th>
            <th width="68%">Finance Sub Type</th>
            <th width="10%">Amount</th>
          </tr>
        </thead>
        <tbody>
          <!--BEGIN - GROSS PROFIT/(LOSS) { -->
          <?php $result = GrossProfit_part01('6');
		while($row = mysqli_fetch_array($result))
		{
?>
          <?php if(!isset($main_type) || $main_type!=$row["MAIN_TYPE_NAME"]){
				$main_type = $row["MAIN_TYPE_NAME"];
		?>
          <tr bgcolor="#ECECFF">
            <td><?php echo $row["FINANCE_TYPE_NAME"]?></td>
            <td><?php echo $row["MAIN_TYPE_NAME"]?></td>
            <td>&nbsp;</td>
          </tr>
          <?php } ?>
          <tr>
            <td style="text-align:center"><?php echo $row["SUB_TYPE_CODE"]?></td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;<a target="rpt_income_statement_note_cswise.php" href="rpt_income_statement_note_cswise.php?<?php echo $para?>#<?php echo $row["SUB_TYPE_NAME"]?>"><?php echo $row["SUB_TYPE_NAME"]?></a></td>
            <td style="text-align:right"><?php echo number_format($row["AMOUNT"],2)?></td>
          </tr>
          <?php
		  $gross_profit_loss += round($row["AMOUNT"],2);
		}
?>
          <?php $result = GrossProfit_part01('8');
		while($row = mysqli_fetch_array($result))
		{
?>
          <?php if(!isset($main_type) || $main_type!=$row["MAIN_TYPE_NAME"]){
				$main_type = $row["MAIN_TYPE_NAME"];
		?>
          <tr bgcolor="#ECECFF">
            <td><?php echo $row["FINANCE_TYPE_NAME"]?></td>
            <td><?php echo $row["MAIN_TYPE_NAME"]?></td>
            <td>&nbsp;</td>
          </tr>
          <?php } ?>
          <tr>
            <td style="text-align:center"><?php echo $row["SUB_TYPE_CODE"]?></td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;<a target="rpt_income_statement_note_cswise.php" href="rpt_income_statement_note_cswise.php?<?php echo $para?>#<?php echo $row["SUB_TYPE_NAME"]?>"><?php echo $row["SUB_TYPE_NAME"]?></a></td>
            <td style="text-align:right"><?php echo number_format($row["AMOUNT"],2)?></td>
          </tr>
          <?php
		  $gross_profit_loss -= round($row["AMOUNT"],2);
		}
?>
          <tr style="font-weight:bold">
            <td>&nbsp;</td>
            <td>GROSS PROFIT/(LOSS)</td>
            <td style="text-align:right;border-top-style:groove;"><?php echo number_format($gross_profit_loss,2)?></td>
          </tr>
          
          <!--END - GROSS PROFIT/(LOSS) } --> 
          
          <!--BEGIN - OPERATING PORFIT/(LOSS) { -->
          <?php $result = GrossProfit_part01('7');
		while($row = mysqli_fetch_array($result))
		{
?>
          <?php if(!isset($main_type) || $main_type!=$row["MAIN_TYPE_NAME"]){
				$main_type = $row["MAIN_TYPE_NAME"];
		?>
          <tr bgcolor="#ECECFF">
            <td><?php echo $row["FINANCE_TYPE_NAME"]?></td>
            <td><?php echo $row["MAIN_TYPE_NAME"]?></td>

            <td>&nbsp;</td>
          </tr>
          <?php } ?>
          <tr>
            <td style="text-align:center"><?php echo $row["SUB_TYPE_CODE"]?></td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;<a target="rpt_income_statement_note_cswise.php" href="rpt_income_statement_note_cswise.php?<?php echo $para?>#<?php echo $row["SUB_TYPE_NAME"]?>"><?php echo $row["SUB_TYPE_NAME"]?></a></td>
            <td style="text-align:right"><?php echo number_format($row["AMOUNT"],2)?></td>
          </tr>
          <?php
		  $gross_profit_loss += round($row["AMOUNT"],2);
		}
?>
          <?php $result = GrossProfit_part02('39,40,41,42');
		while($row = mysqli_fetch_array($result))
		{
?>
          <?php if(!isset($main_type) || $main_type!=$row["MAIN_TYPE_NAME"]){
				$main_type = $row["MAIN_TYPE_NAME"];
		?>
          <tr bgcolor="#ECECFF">
            <td><?php echo $row["FINANCE_TYPE_NAME"]?></td>
            <td><?php echo $row["MAIN_TYPE_NAME"]?></td>
            <td>&nbsp;</td>
          </tr>
          <?php } ?>
          <tr>
            <td style="text-align:center"><?php echo $row["SUB_TYPE_CODE"]?></td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;<a target="rpt_income_statement_note_cswise.php" href="rpt_income_statement_note_cswise.php?<?php echo $para?>#<?php echo $row["SUB_TYPE_NAME"]?>"><?php echo $row["SUB_TYPE_NAME"]?></a></td>
            <td style="text-align:right"><?php echo number_format($row["AMOUNT"],2)?></td>
          </tr>
          <?php
		  $gross_profit_loss -= round($row["AMOUNT"],2);
		}
?>
          <!--END   - OPERATING PORFIT/(LOSS) } -->
          
          <tr style="font-weight:bold">
            <td>&nbsp;</td>
            <td>OPERATING PORFIT/(LOSS)</td>
            <td style="text-align:right;border-top-style:groove;"><?php echo number_format($gross_profit_loss,2)?></td>
          </tr>
          
          <!--BEGIN - PROFIT/(LOSS) BEFORE TAX { -->
          <?php $result = GrossProfit_part02('43');
		while($row = mysqli_fetch_array($result))
		{
?>
          <tr>
            <td style="text-align:center"><?php echo $row["SUB_TYPE_CODE"]?></td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;<a target="rpt_income_statement_note_cswise.php" href="rpt_income_statement_note_cswise.php?<?php echo $para?>#<?php echo $row["SUB_TYPE_NAME"]?>"><?php echo $row["SUB_TYPE_NAME"]?></a></td>
            <td style="text-align:right"><?php echo number_format($row["AMOUNT"],2)?></td>
          </tr>
          <?php
		  $gross_profit_loss -= round($row["AMOUNT"],2);
		}
?>
          <tr style="font-weight:bold">
            <td>&nbsp;</td>
            <td>PROFIT/(LOSS) BEFORE TAX</td>
            <td style="text-align:right;border-top-style:groove;"><?php echo number_format($gross_profit_loss,2)?></td>
          </tr>
          <!--END   - PROFIT/(LOSS) BEFORE TAX } --> 
          
          <!--BEGIN - NET PROFIT/(LOSS) } -->
          <?php $result = GrossProfit_part02('44');
		while($row = mysqli_fetch_array($result))
		{
?>
          <tr>
            <td style="text-align:center"><?php echo $row["SUB_TYPE_CODE"]?></td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;<a target="rpt_income_statement_note_cswise.php" href="rpt_income_statement_note_cswise.php?<?php echo $para?>#<?php echo $row["SUB_TYPE_NAME"]?>"><?php echo $row["SUB_TYPE_NAME"]?></a></td>
            <td style="text-align:right"><?php echo number_format($row["AMOUNT"],2)?></td>
          </tr>
          <?php
		  $gross_profit_loss -= round($row["AMOUNT"],2);
		}
?>
        </tbody>
        <tfoot>
          <tr style="font-weight:bold">
            <td>&nbsp;</td>
            <td>NET PROFIT/(LOSS)</td>
            <td style="text-align:right;border-top-style:groove;border-bottom-style:double"><?php echo number_format($gross_profit_loss,2)?></td>
          </tr>
        </tfoot>
        <!--END   - NET PROFIT/(LOSS) } -->
      </table></td>
  </tr>
</table>
</body>
</html>
<?php
function GrossProfit_part01($para)
{
	global $db;
	global $companyId;
	global $target_currencyId;
	global $dateFrom;
	global $dateTo;
	global $coscenterPara;
	
	/*	$sql = "SELECT 
				T.FINANCE_TYPE_NAME												AS FINANCE_TYPE_NAME,
				MT.MAIN_TYPE_NAME												AS MAIN_TYPE_NAME,
				ST.SUB_TYPE_NAME												AS SUB_TYPE_NAME,
				CONCAT(T.FINANCE_TYPE_CODE,MT.MAIN_TYPE_CODE,ST.SUB_TYPE_CODE) 	AS SUB_TYPE_CODE,
				ROUND(COALESCE(SUM(FT.AMOUNT),0),2) 							AS AMOUNT1,
				ROUND(COALESCE(SUM(FT.AMOUNT / (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId) * (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId)),0),2) AS AMOUNT
			FROM finance_mst_account_main_type MT 
			INNER JOIN finance_mst_account_type T ON T.FINANCE_TYPE_ID = MT.FINANCE_TYPE_ID
			INNER JOIN finance_mst_account_sub_type ST ON ST.MAIN_TYPE_ID = MT.MAIN_TYPE_ID
			INNER JOIN finance_mst_chartofaccount COA ON COA.SUB_TYPE_ID = ST.SUB_TYPE_ID
			LEFT JOIN finance_transaction FT ON FT.CHART_OF_ACCOUNT_ID = COA.CHART_OF_ACCOUNT_ID
			WHERE MT.MAIN_TYPE_ID IN ($para)
				AND (DATE(FT.LAST_MODIFIED_DATE) BETWEEN '$dateFrom' AND '$dateTo' OR DATE(FT.LAST_MODIFIED_DATE) IS NULL)
			GROUP BY ST.MAIN_TYPE_ID,ST.SUB_TYPE_ID";
	return $db->RunQuery($sql);*/
	
	$sql = "SELECT 
			FINANCE_TYPE_NAME,
			MAIN_TYPE_NAME,
			CONCAT(FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE) AS SUB_TYPE_CODE,
			SUB_TYPE_NAME,
			
			SUM(IF(TRANSACTION_ADD_TYPE='D',DEBIT-CREDIT,CREDIT-DEBIT))AS AMOUNT
			FROM 
			(SELECT 
				TRANSACTION_ADD_TYPE,
				FINANCE_TYPE_ID,
				FINANCE_TYPE_CODE,
				FINANCE_TYPE_NAME,
				MAIN_TYPE_ID,
				MAIN_TYPE_CODE,
				MAIN_TYPE_NAME,
				SUB_TYPE_ID,
				SUB_TYPE_CODE,
				SUB_TYPE_NAME,
				IF(TRANSACTION_TYPE='D',AMOUNT,0) AS DEBIT,
				IF(TRANSACTION_TYPE='C',AMOUNT,0) AS CREDIT
			FROM 
				(SELECT
					T.TRANSACTION_ADD_TYPE,
					T.FINANCE_TYPE_ID,
					T.FINANCE_TYPE_CODE,
					T.FINANCE_TYPE_NAME,
					MT.MAIN_TYPE_ID,
					MT.MAIN_TYPE_CODE,
					MT.MAIN_TYPE_NAME,
					ST.SUB_TYPE_ID,
					ST.SUB_TYPE_CODE,
					ST.SUB_TYPE_NAME,
					COA.CHART_OF_ACCOUNT_ID,
					COA.CHART_OF_ACCOUNT_CODE,
					COA.CHART_OF_ACCOUNT_NAME,
					FT.CURRENCY_ID,
					FT.TRANSACTION_TYPE,
					ROUND(COALESCE(FT.AMOUNT / (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId) * (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId),0),2) AS AMOUNT
				FROM finance_mst_account_type T
				INNER JOIN finance_mst_account_main_type MT ON T.FINANCE_TYPE_ID = MT.FINANCE_TYPE_ID
				INNER JOIN finance_mst_account_sub_type ST ON MT.MAIN_TYPE_ID = ST.MAIN_TYPE_ID
				INNER JOIN finance_mst_chartofaccount COA ON ST.SUB_TYPE_ID = COA.SUB_TYPE_ID
				LEFT JOIN finance_transaction FT ON COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID
				WHERE MT.MAIN_TYPE_ID IN ($para)
				$coscenterPara
				AND (DATE(FT.LAST_MODIFIED_DATE) BETWEEN '$dateFrom' AND '$dateTo' OR DATE(FT.LAST_MODIFIED_DATE) IS NULL))
				AS SUB_1) AS SUB_2
				GROUP BY FINANCE_TYPE_ID,MAIN_TYPE_ID,SUB_TYPE_ID
				ORDER BY FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE";
		return $db->RunQuery($sql);
}

function GrossProfit_part02($para)
{
	global $db;
	global $companyId;
	global $target_currencyId;
	global $dateFrom;
	global $dateTo;
	global $coscenterPara;
	
	/*$sql = "SELECT 
				T.FINANCE_TYPE_NAME												AS FINANCE_TYPE_NAME,
				MT.MAIN_TYPE_NAME												AS MAIN_TYPE_NAME,
				ST.SUB_TYPE_NAME												AS SUB_TYPE_NAME,
				CONCAT(T.FINANCE_TYPE_CODE,MT.MAIN_TYPE_CODE,ST.SUB_TYPE_CODE) 	AS SUB_TYPE_CODE,
				ROUND(COALESCE(SUM(FT.AMOUNT),0),2) 							AS AMOUNT1,
				ROUND(COALESCE(SUM(FT.AMOUNT / (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId) * (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId)),0),2) AS AMOUNT
			FROM finance_mst_account_main_type MT 
			INNER JOIN finance_mst_account_type T ON T.FINANCE_TYPE_ID = MT.FINANCE_TYPE_ID
			INNER JOIN finance_mst_account_sub_type ST ON ST.MAIN_TYPE_ID = MT.MAIN_TYPE_ID
			INNER JOIN finance_mst_chartofaccount COA ON COA.SUB_TYPE_ID = ST.SUB_TYPE_ID
			LEFT JOIN finance_transaction FT ON FT.CHART_OF_ACCOUNT_ID = COA.CHART_OF_ACCOUNT_ID
			WHERE ST.SUB_TYPE_ID IN ($para)
				AND (DATE(FT.LAST_MODIFIED_DATE) BETWEEN '$dateFrom' AND '$dateTo' OR DATE(FT.LAST_MODIFIED_DATE) IS NULL)
			GROUP BY ST.MAIN_TYPE_ID,ST.SUB_TYPE_ID";
	return $db->RunQuery($sql);*/
	
	$sql = "SELECT 
			FINANCE_TYPE_NAME,
			MAIN_TYPE_NAME,
			CONCAT(FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE) AS SUB_TYPE_CODE,
			SUB_TYPE_NAME,
			
			SUM(IF(TRANSACTION_ADD_TYPE='D',DEBIT-CREDIT,CREDIT-DEBIT))AS AMOUNT
			FROM 
			(SELECT 
				TRANSACTION_ADD_TYPE,
				FINANCE_TYPE_ID,
				FINANCE_TYPE_CODE,
				FINANCE_TYPE_NAME,
				MAIN_TYPE_ID,
				MAIN_TYPE_CODE,
				MAIN_TYPE_NAME,
				SUB_TYPE_ID,
				SUB_TYPE_CODE,
				SUB_TYPE_NAME,
				IF(TRANSACTION_TYPE='D',AMOUNT,0) AS DEBIT,
				IF(TRANSACTION_TYPE='C',AMOUNT,0) AS CREDIT
			FROM 
				(SELECT
					T.TRANSACTION_ADD_TYPE,
					T.FINANCE_TYPE_ID,
					T.FINANCE_TYPE_CODE,
					T.FINANCE_TYPE_NAME,
					MT.MAIN_TYPE_ID,
					MT.MAIN_TYPE_CODE,
					MT.MAIN_TYPE_NAME,
					ST.SUB_TYPE_ID,
					ST.SUB_TYPE_CODE,
					ST.SUB_TYPE_NAME,
					COA.CHART_OF_ACCOUNT_ID,
					COA.CHART_OF_ACCOUNT_CODE,
					COA.CHART_OF_ACCOUNT_NAME,
					FT.CURRENCY_ID,
					FT.TRANSACTION_TYPE,
					ROUND(COALESCE(FT.AMOUNT / (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId) * (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId),0),2) AS AMOUNT
				FROM finance_mst_account_type T
				INNER JOIN finance_mst_account_main_type MT ON T.FINANCE_TYPE_ID = MT.FINANCE_TYPE_ID
				INNER JOIN finance_mst_account_sub_type ST ON MT.MAIN_TYPE_ID = ST.MAIN_TYPE_ID
				INNER JOIN finance_mst_chartofaccount COA ON ST.SUB_TYPE_ID = COA.SUB_TYPE_ID
				LEFT JOIN finance_transaction FT ON COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID
				WHERE ST.SUB_TYPE_ID IN ($para)
				$coscenterPara
				AND (DATE(FT.LAST_MODIFIED_DATE) BETWEEN '$dateFrom' AND '$dateTo' OR DATE(FT.LAST_MODIFIED_DATE) IS NULL))
				AS SUB_1) AS SUB_2
				GROUP BY FINANCE_TYPE_ID,MAIN_TYPE_ID,SUB_TYPE_ID
				ORDER BY FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE";
				//die($sql);
		return $db->RunQuery($sql);
}
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
######################################################################################################################
## INCLUDE FILES																									##
require_once "class/tables/finance_mst_transaction_type.php"; 					$finance_mst_transaction_type 					= new finance_mst_transaction_type($db);
require_once "class/tables/finance_cashflow_statement_cpanal_category.php"; 	$finance_cashflow_statement_cpanal_category 	= new finance_cashflow_statement_cpanal_category($db);
require_once "class/tables/finance_cashflow_statement_cpanal.php"; 				$finance_cashflow_statement_cpanal 				= new finance_cashflow_statement_cpanal($db);
require_once "class/tables/finance_transaction.php"; 							$finance_transaction 							= new finance_transaction($db);
require_once "class/tables/mst_financecurrency.php"; 							$mst_financecurrency							= new mst_financecurrency($db);
require_once "class/tables/mst_companies.php";									$mst_companies									= new mst_companies($db);																								

######################################################################################################################

######################################################################################################################
## ASSIGN PARAMETERS																								##
$dateTo					= $_REQUEST['Date'];
$convertCurrencyId		= $_REQUEST['Currency'];
$mst_financecurrency->set($convertCurrencyId);
$mst_companies->set($sessions->getCompanyId());
######################################################################################################################
?>

<head>
<title>STATEMENT OF CASHFLOW</title>
</head>

<body>
<table width="900" border="0" align="center">
  <tr>
    <td class="reportHeader" align="center" style="font-size:24px"><?php echo strtoupper($mst_companies->getstrName())?></td>
  </tr>
  <tr>
    <td class="reportHeader" align="center" style="font-size:14px">STATEMENT OF CASH FLOW - <span style="font-size:14px">Year Ended <?php echo date("jS F Y", strtotime($_REQUEST['Date']));?></span></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" class="rptBordered">
        <thead>
          <tr>
            <th width="20">No</th>
            <th width="500">Statement Item</th>
            <th width="10">Amount <br/>[<?php echo $mst_financecurrency->getstrCode()?>]</th>
          </tr>
        </thead>
        <tbody>
          <?php 
	  	$i = 0;
	  	$result = $finance_cashflow_statement_cpanal_category->getDetails();
	  	while($row = mysqli_fetch_array($result))
		{
			$i++;
	  ?>
          <tr style="font-weight:bold">
            <td style="text-align:left"><?php echo $i?>.</td>
            <td><u><?php echo $row["REPORT_CATEGORY_NAME"]?></u></td>
            <td >&nbsp;</td>
          </tr>
          <?php
		  	$x 			= 0;
			$totAmount	= 0;
			$result1 = $finance_cashflow_statement_cpanal->getDetails($row["REPORT_CATEGORY_ID"]);
			while($row1 = mysqli_fetch_array($result1))
			{
				$x++;
				$finance_mst_transaction_type->set($row1['TRANSACTION_PROGRAM_ID']);
				$amount = $finance_transaction->getTotAmountForSelectedCriteria($sessions->getCompanyId(),$row1['CHART_OF_ACCOUNT_ID'],$finance_mst_transaction_type->getDOCUMENT_TYPE(),$dateFrom=null,$dateTo,$convertCurrencyId);
		?>
          <tr>
            <td style="text-align:left"><?php echo $i.'.'.$x?></td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row1["REPORT_FIELD_NAME"]?></td>
            <td style="text-align:right"><?php echo number_format($amount,2)?></td>
          </tr>
          <?php
		  	$totAmount	+= round($amount,2);
			}
		?>
          <tr style="font-weight:bold">
            <td>&nbsp;</td>
            <td >Net <?php echo $row["REPORT_CATEGORY_NAME"]?></td>
            <td style="text-align:right"><?php echo number_format($totAmount,2)?></td>
          </tr>
          <tr>
            <td colspan="3">&nbsp;</td>
          </tr>
          <?php
		  $grandTotAmount	+= $totAmount;
		}
		?>
        <tr style="font-weight:bold">
            <td>&nbsp;</td>
            <td style="text-align:right">GRAND TOTAL</td>
            <td style="text-align:right"><?php echo number_format($grandTotAmount,2)?></td>
          </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
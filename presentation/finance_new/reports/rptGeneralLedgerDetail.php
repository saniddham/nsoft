<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
	$locationId			= $_SESSION["CompanyID"];
	$companyId			= $_SESSION["headCompanyId"];
	
	$fromDate			= $_REQUEST['fromDate'];
	$toDate				= $_REQUEST['toDate'];
	$currency			= $_REQUEST['currency'];
	$finaceType			= ($_REQUEST['financeType']=='null'?'':$_REQUEST['financeType']);
	$mainCatId			= ($_REQUEST['mainCateId']=='null'?'':$_REQUEST['mainCateId']);
	$subCatId			= ($_REQUEST['subCatId']=='null'?'':$_REQUEST['subCatId']);
	$GLAccId			= ($_REQUEST['chartOfAccId']=='null'?'':$_REQUEST['chartOfAccId']);
	
	$baseCurrency		= getBaseCurrency($companyId);
	$data['headerArr'] 	= GetHeaderDetails($currency,$finaceType,$mainCatId,$subCatId,$GLAccId);
	$detail_result 		= GetGridDetails($finaceType,$mainCatId,$subCatId,$GLAccId,$fromDate,$toDate);

	$openingBal			= getOpeningBalance($finaceType,$mainCatId,$subCatId,$GLAccId,$fromDate,$currency);
	$closingBal			= 0;
	
	$invReportId			= 893;
	$advPayReportId			= 953;	
	$supPayReportId			= 943;
	$supDebitReportId		= 1082;
	$supCreditReportId		= 1086;	
	$cusInvReportId			= 894;
	$cusAdvReportId			= 899;
	$cusPayRecvReportId		= 930;
	$cusDbtNoteReportId		= 928;
	$cusCrdNoteReportId		= 913;
	$cusPayRcvSetlReportId	= 933;
	$otrBillInvReportId		= 979;
	$otrBillPayReportId		= 946;
	$otrBillRcvInvReportId	= 1087;
	$otrBillRcvPayReportId	= 1093;
	$jurnlEntrReportId		= 986;
	$bnkPayReportId			= 987;
?>
<head>
    <title>General Ledger - Detail</title>
</head>
<body>
<form id="frmGeneralLedgerSummary" name="frmGeneralLedgerSummary" method="post">
<div align="center">
  <table width="1800" align="center">
    <tr>
      <td><?php include 'reportHeader.php'; ?></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="reportHeader" style="text-align:center"><strong>Ledger Details - Details (General Ledger <?php echo $data['headerArr']['CURName']; ?>)</strong></td>
    </tr>
    <tr>
      <td class="normalfnt" style="text-align:center"><table align="center" width="300" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="64" class="normalfnt"><strong>From&nbsp;&nbsp;:</strong></td>
            <td width="92" class="normalfnt"><?php echo $fromDate; ?></td>
            <td width="45" class="normalfnt"><strong>To&nbsp;&nbsp;:</strong></td>
            <td width="99" class="normalfnt"><?php echo $toDate; ?></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="reportHeader" style="text-align:center">&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" border="0">
        <tr class="normalfnt">
          <td width="16%">Finance Type</td>
          <td width="1%" style="text-align:center">:</td>
          <td width="32%"><?php echo $data['headerArr']['FTName']; ?></td>
          <td width="17%">Main Category</td>
           <td width="2%" style="text-align:center">:</td>
            <td width="32%"><?php echo $data['headerArr']['MCName']; ?></td>
        </tr>
        <tr class="normalfnt">
          <td>Sub Category</td>
          <td style="text-align:center">:</td>
          <td><?php echo $data['headerArr']['SCName']; ?></td>
          <td>Chart Of Account</td>
           <td style="text-align:center">:</td>
            <td><?php echo $data['headerArr']['CAName']; ?></td>
        </tr>
      </table></td>
    </tr>
    <tr>
    	<td><table width="100%" border="0">
    	  <tr class="normalfnt">
    	    <td width="80%">&nbsp;</td>
    	    <td width="11%">Opening Balance</td>
    	    <td width="1%">:</td>
    	    <td width="8%" style="text-align:right"><?php echo number_format($openingBal,2); ?></td>
  	      </tr>
  	  </table></td>
    </tr>
    <tr>
        <td class="reportHeader" style="text-align:center">
            <table width="100%" class="rptBordered" id="tblMain" border="0">
            <thead>
                <tr>
                    <th width="2%">&nbsp;</th>
                    <th width="8%">Chart of Account</th>
                    <th width="7%">Transaction Type</th>
                    <th width="5%">Date</th>
                    <th width="6%">Invoice No</th>
                    <th width="6%">Document No</th>
                    <th width="7%">Ledger Category</th>
                    <th width="8%">Remarks</th>
                    <th width="5%">Payment Method</th>
                    <th width="5%">Cost Center</th>
                    <th width="6%">Reference No</th>
                    <th width="11%">Account Name</th>
                    <th width="6%">Currency</th>
                    <th width="5%">Currency Rate</th>
                    <th width="4%">Debit Amt</th>
                    <th width="4%">Credit Amt</th>
                    <th width="5%">Closing Balance</th>
                </tr>
       		 </thead>
			<?php
			$loop=0;
				while($row=mysqli_fetch_array($detail_result))
				{
					$rowChk             = false;
					$savedCurrencyRate 	= getCurrencyRate($row['CURRENCY_ID'],$row['dtmDate']);
					//$targetCurrencyRate = getCurrencyRate($currency,$row['dtmDate']);
					$accountArr			= getAccountName($row['CHART_OF_ACCOUNT_ID'],$row['TRANSACTION_TYPE'],$row['TRANSACTION_CATEGORY'],$row['DOC_NO'],$row['DOC_YEAR'],$row['DOCUMENT_TYPE'],$row['ftAmount']);
					
					$invoiceNo			= getInvoiceNo($row['TRANSACTION_CATEGORY'],$row['DOCUMENT_TYPE'],$row['DOC_NO'],$row['DOC_YEAR']);					
					$amount 			= $row['amount'];
					
					$calculatePlusType  = getCalculateType($row['FINANCE_TYPE_ID']);
					if($calculatePlusType==$row['TRANSACTION_TYPE'])
					{
						$closingBal = $openingBal+$amount;
					}
					else
					{
						$closingBal = $openingBal-$amount;
					}

						$data['detailArr'] = getOtherDetails($row['DOCUMENT_TYPE'],$row['TRANSACTION_CATEGORY'],$row['DOC_NO'],$row['DOC_YEAR']);
			?>
            		<tr>
           			<td class="normalfnt" style="text-align:center"><?php echo ++$i; ?>.</td>
                    <td class="normalfnt" style="text-align:left"><?php echo $row['CHART_OF_ACCOUNT_NAME']; ?></td>
                    <td class="normalfnt" style="text-align:left"><?php echo $row['TYPE_NAME']; ?></td>
                    <td class="normalfnt" style="text-align:center"><?php echo $row['dtmDate']; ?></td>
                    <td class="normalfnt" style="text-align:left"><?php echo ($invoiceNo==''?'&nbsp;':$invoiceNo); ?></td>
                    <td class="normalfnt" style="text-align:center"><?php echo ((getLink($row['DOCUMENT_TYPE'],$row['TRANSACTION_CATEGORY'],$row['DOC_NO'],$row['DOC_YEAR']))==''?$row['documentNo']:'<a href="'.getLink($row['DOCUMENT_TYPE'],$row['TRANSACTION_CATEGORY'],$row['DOC_NO'],$row['DOC_YEAR']).'" target="'.++$loop.'">'.$row["documentNo"].'</a>'); ?></td>
                    <td class="normalfnt" style="text-align:left"><?php echo $row['SUB_TYPE_NAME']; ?></td>
                    <td class="normalfnt" style="text-align:left"><?php echo ($data['detailArr']['remarks']==''?'&nbsp;':$data['detailArr']['remarks']); ?></td>
                    <td class="normalfnt" style="text-align:left"><?php echo ($data['detailArr']['payMethod']==''?'&nbsp;':$data['detailArr']['payMethod']); ?></td>
                    <td class="normalfnt" style="text-align:left"><?php echo ($data['detailArr']['costCenter']==''?'&nbsp;':$data['detailArr']['costCenter']); ?></td>
                    <td class="normalfnt" style="text-align:left"><?php echo ($row['BANK_REFERENCE_NO']==''?'&nbsp;':$row['BANK_REFERENCE_NO']); ?></td>
                    <td class="normalfnt" style="text-align:left"><?php echo ($accountArr['accName']==''?'&nbsp;':$accountArr['accName']); ?></td>
                    <td class="normalfnt" style="text-align:left"><?php echo $row['CURENNCY']; ?></td>
                    <td class="normalfnt" style="text-align:right"><?php echo $savedCurrencyRate; ?></td>
                    <td class="normalfnt" style="text-align:right"><?php echo ($row['TRANSACTION_TYPE']=='D'?number_format(abs($amount),2):'&nbsp;'); ?></td>
                    <td class="normalfnt" style="text-align:right"><?php echo ($row['TRANSACTION_TYPE']=='C'?number_format(abs($amount),2):'&nbsp;'); ?></td>
                    <td class="normalfnt" style="text-align:right"><?php echo number_format($closingBal,2); ?></td>
			<?php
					$openingBal = $closingBal;
								
				}
            ?>
            </tr>
            </table>
      </td>
    </tr>
  </table>
  </div>
</form>
</body>
<?php
function GetGridDetails($finaceType,$mainCatId,$subCatId,$GLAccId,$fromDate,$toDate)
{
	global $db;
	global $companyId;
	global $currency;
	global $baseCurrency;
	
	$WHSql = '';
	if($fromDate!='' && $toDate!='')
		$WHSql.="AND DATE(FT.LAST_MODIFIED_DATE) BETWEEN ('$fromDate') AND ('$toDate') ";		
	if($finaceType!='')
		$WHSql.="AND AT.FINANCE_TYPE_ID='$finaceType' ";
	if($mainCatId!='')
		$WHSql.="AND MT.MAIN_TYPE_ID='$mainCatId' ";
	if($subCatId!='')
		$WHSql.="AND ST.SUB_TYPE_ID='$subCatId' ";
	if($GLAccId!='')
		$WHSql.="AND COA.CHART_OF_ACCOUNT_ID='$GLAccId' ";
	if($baseCurrency!=$currency)
		$WHsql.="AND FT.DOCUMENT_TYPE NOT IN ('GAIN_LOSS') ";
	
	$sql ="SELECT T1.*,SUM(lastAmonut) AS amount FROM(SELECT
			FT.CHART_OF_ACCOUNT_ID,
			COA.CHART_OF_ACCOUNT_NAME,
			DATE(FT.LAST_MODIFIED_DATE) AS dtmDate,
			CONCAT(FT.INVOICE_YEAR,'-',FT.INVOICE_NO) AS invoiceNo,
			PIH.INVOICE_NO,
			CONCAT(FT.DOCUMENT_YEAR,'-',FT.DOCUMENT_NO) AS documentNo,
			
			ROUND(COALESCE(FT.AMOUNT / (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId ='$currency' AND 
  			DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId) * (SELECT SUB_ER.dblExcAvgRate FROM 
 			mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID AND 
			DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId),0),2) AS lastAmonut,
			
			FT.AMOUNT as ftAmount,
			FT.INVOICE_NO as ftInvNo,
			FT.INVOICE_YEAR as ftInvYear,
			FT.DOCUMENT_NO,
			FT.DOCUMENT_YEAR,
			FT.DOCUMENT_NO as DOC_NO,
			FT.DOCUMENT_YEAR as DOC_YEAR,
			FT.TRANSACTION_TYPE,
			FT.DOCUMENT_TYPE,
			FT.TRANSACTION_CATEGORY,
  			FMTT.TYPE_NAME,
			FT.CURRENCY_ID,
			FT.BANK_REFERENCE_NO,
			ST.SUB_TYPE_NAME,
			AT.FINANCE_TYPE_ID,
			FT.LAST_MODIFIED_DATE,
			FC.strCode AS CURENNCY,
			FT.SERIAL_ID
			FROM finance_transaction FT
			INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=FT.CHART_OF_ACCOUNT_ID
			INNER JOIN finance_mst_account_sub_type ST ON ST.SUB_TYPE_ID=COA.SUB_TYPE_ID
			INNER JOIN finance_mst_account_main_type MT ON MT.MAIN_TYPE_ID=ST.MAIN_TYPE_ID
			INNER JOIN finance_mst_account_type AT ON AT.FINANCE_TYPE_ID=MT.FINANCE_TYPE_ID
			LEFT JOIN finance_mst_transaction_type FMTT ON FMTT.DOCUMENT_TYPE=FT.DOCUMENT_TYPE AND FMTT.TRANSACTION_CATEGORY=FT.TRANSACTION_CATEGORY
			INNER JOIN mst_financecurrency FC ON FC.intId=FT.CURRENCY_ID
			LEFT JOIN finance_supplier_purchaseinvoice_header PIH ON PIH.PURCHASE_INVOICE_NO=FT.INVOICE_NO AND PIH.PURCHASE_INVOICE_YEAR=FT.INVOICE_YEAR
			WHERE FT.COMPANY_ID='$companyId' 
			$WHSql)AS T1
			GROUP BY CHART_OF_ACCOUNT_ID,DATE(LAST_MODIFIED_DATE),ftInvNo,
			ftInvYear,DOCUMENT_NO,DOCUMENT_YEAR,TRANSACTION_TYPE,TRANSACTION_CATEGORY,DOCUMENT_TYPE
			ORDER BY LAST_MODIFIED_DATE,SERIAL_ID ";
	//echo $sql;
	$result = $db->RunQuery($sql);
	return $result;
}
function GetHeaderDetails($currency,$finaceType,$mainCatId,$subCatId,$GLAccId)
{
	global $db;
	$sqlCUR = "SELECT strCode
				FROM mst_financecurrency
				WHERE intId='$currency' ";
	$resultCUR = $db->RunQuery($sqlCUR);
	$rowCUR = mysqli_fetch_array($resultCUR);
	
	$sqlFT = "SELECT FINANCE_TYPE_NAME
				FROM finance_mst_account_type
				WHERE FINANCE_TYPE_ID='$finaceType' ";
	
	$resultFT = $db->RunQuery($sqlFT);
	$rowFT = mysqli_fetch_array($resultFT);
	
	$sqlSC = "SELECT SUB_TYPE_NAME
				FROM finance_mst_account_sub_type
				WHERE SUB_TYPE_ID='$subCatId' ";
	
	$resultSC = $db->RunQuery($sqlSC);
	$rowSC = mysqli_fetch_array($resultSC);
	
	$sqlMC = "SELECT MAIN_TYPE_NAME
				FROM finance_mst_account_main_type
				WHERE MAIN_TYPE_ID='$mainCatId' ";
	
	$resultMC = $db->RunQuery($sqlMC);
	$rowMC = mysqli_fetch_array($resultMC);
	
	$sqlCA = "SELECT CHART_OF_ACCOUNT_NAME
				FROM finance_mst_chartofaccount
				WHERE CHART_OF_ACCOUNT_ID='$GLAccId' ";
	
	$resultCA = $db->RunQuery($sqlCA);
	$rowCA = mysqli_fetch_array($resultCA);
	
	$data['FTName']  = $rowFT['FINANCE_TYPE_NAME'];
	$data['SCName']  = $rowSC['SUB_TYPE_NAME'];
	$data['MCName']  = $rowMC['MAIN_TYPE_NAME'];
	$data['CAName']  = $rowCA['CHART_OF_ACCOUNT_NAME'];
	$data['CURName'] = $rowCUR['strCode'];
	
	return $data;
	
}
function getCurrencyRate($currency,$date)
{
	global $db;
	global $companyId;
	
	$sql = "SELECT dblExcAvgRate
			FROM mst_financeexchangerate
			WHERE intCurrencyId='$currency' AND
			dtmDate='$date' AND
			intCompanyId='$companyId' ";

	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	
	return $row['dblExcAvgRate'];
}
function getOpeningBalance($finaceType,$mainCatId,$subCatId,$GLAccId,$fromDate,$currency)
{
	global $db;
	global $companyId;
	global $baseCurrency;
	
	$sql ="SELECT ROUND(SUM((amount/targetExchangeRate)*savedExchangeRate),2) AS AMOUNT,
			transactionType
			FROM(
			SELECT FT.AMOUNT AS amount,
			FT.TRANSACTION_TYPE AS transactionType,
			(SELECT dblExcAvgRate 
			FROM mst_financeexchangerate
			WHERE mst_financeexchangerate.intCurrencyId=FT.CURRENCY_ID AND
			mst_financeexchangerate.intCompanyId='$companyId' AND
			mst_financeexchangerate.dtmDate=DATE(FT.LAST_MODIFIED_DATE)) AS savedExchangeRate,
			(SELECT dblExcAvgRate 
			FROM mst_financeexchangerate
			WHERE mst_financeexchangerate.intCurrencyId='$currency' AND
			mst_financeexchangerate.intCompanyId='$companyId' AND
			mst_financeexchangerate.dtmDate=DATE(FT.LAST_MODIFIED_DATE)) AS targetExchangeRate
			FROM finance_transaction FT
			INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=FT.CHART_OF_ACCOUNT_ID
			INNER JOIN finance_mst_account_sub_type ST ON ST.SUB_TYPE_ID=COA.SUB_TYPE_ID
			INNER JOIN finance_mst_account_main_type MT ON MT.MAIN_TYPE_ID=ST.MAIN_TYPE_ID
			INNER JOIN finance_mst_account_type AT ON AT.FINANCE_TYPE_ID=MT.FINANCE_TYPE_ID
			WHERE FT.COMPANY_ID='$companyId' AND DATE(FT.LAST_MODIFIED_DATE)<'$fromDate' ";
			
	if($finaceType!='')
		$sql.="AND AT.FINANCE_TYPE_ID='$finaceType' ";
	if($mainCatId!='')
		$sql.="AND MT.MAIN_TYPE_ID='$mainCatId' ";
	if($subCatId!='')
		$sql.="AND ST.SUB_TYPE_ID='$subCatId' ";
	if($GLAccId!='')
		$sql.="AND COA.CHART_OF_ACCOUNT_ID='$GLAccId' ";
	if($baseCurrency!=$currency)
		$sql.="AND FT.DOCUMENT_TYPE NOT IN ('GAIN_LOSS') ";
	
	$sql.="GROUP BY FT.CHART_OF_ACCOUNT_ID,DATE(FT.LAST_MODIFIED_DATE),FT.INVOICE_NO,
			FT.INVOICE_YEAR,FT.DOCUMENT_NO,FT.DOCUMENT_YEAR,FT.TRANSACTION_TYPE,FT.TRANSACTION_CATEGORY
			ORDER BY FT.CHART_OF_ACCOUNT_ID,FT.INVOICE_YEAR,FT.INVOICE_NO ) AS SUB_1 ";
	$sql.="GROUP BY transactionType ";
	//echo $sql;
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		if($row['transactionType']=='C')
			$creditAmount = $row['AMOUNT'];
		else
			$debitAmount = $row['AMOUNT'];
	}
	$openingBal = $debitAmount-$creditAmount;
	
	return ($openingBal==''?0:$openingBal);
}
function getOtherDetails($documentType,$transactionType,$docNo,$docYear)
{
	global $db;
	
	if($transactionType=='SU')
	{
		switch($documentType)
		{
			case 'INVOICE':
				$data = getDocumentDetails('finance_supplier_purchaseinvoice_header','finance_supplier_purchaseinvoice_details','PURCHASE_INVOICE_NO','PURCHASE_INVOICE_YEAR','','COST_CENTER',$docNo,$docYear);
			break;
			case 'ADVANCE':
				$data = getDocumentDetails('finance_supplier_advancepayment_header','finance_supplier_advancepayment_details','ADVANCE_PAYMENT_NO','ADVANCE_PAYMENT_YEAR','PAY_METHOD','COST_CENTER',$docNo,$docYear);
			break;
			case 'PAYMENT':
				$data = getDocumentDetails('finance_supplier_payment_header','finance_supplier_payment_gl','PAYMENT_NO','PAYMENT_YEAR','PAY_METHOD','COST_CENTER',$docNo,$docYear);
			break;
			case 'DEBIT':
				$data = getDocumentDetails('finance_supplier_debitnote_header','finance_supplier_debitnote_details','DEBIT_NO','DEBIT_YEAR','','COST_CENTER_ID',$docNo,$docYear);
			break;
			case 'CREDIT':
				$data = getDocumentDetails('finance_supplier_creditnote_header','finance_supplier_creditnote_details','CREDIT_NO','CREDIT_YEAR','','COST_CENTER_ID',$docNo,$docYear);
			break;
		}
	}
	else if($transactionType=='CU')
	{
		switch($documentType)
		{
			case 'INVOICE':
				$data = getDocumentDetails('finance_customer_invoice_header','finance_customer_invoice_details','SERIAL_NO','SERIAL_YEAR','','COST_CENTER',$docNo,$docYear);
			break;
			case 'ADVANCE':
				$data = getDocumentDetails('finance_customer_advance_header','finance_customer_advance_details','ADVANCE_NO','ADVANCE_YEAR','PAYMENT_METHOD','COST_CENTER_ID',$docNo,$docYear);
			break;
			case 'PAYRECEIVE':
				$data = getDocumentDetails('finance_customer_pay_receive_header','finance_customer_pay_receive_gl','RECEIPT_NO','RECEIPT_YEAR','PAYMENT_MODE','COST_CENTER_ID',$docNo,$docYear);
			break;
			case 'DEBIT':
				$data = getDocumentDetails('finance_customer_debit_note_header','finance_customer_debit_note_detail','DEBIT_NO','DEBIT_YEAR','','COST_CENTER_ID',$docNo,$docYear);
			break;
			case 'CREDIT':
				$data = getDocumentDetails('finance_customer_credit_note_header','finance_customer_credit_note_detail','CREDIT_NO','CREDIT_YEAR','','COST_CENTER_ID',$docNo,$docYear);
			break;
		}
	}
	return $data;	
}
function getDocumentDetails($mainTable,$detailTable,$serialNo,$serialYear,$payMethod,$costCenter,$docNo,$docYear)
{
	global $db;
	global $companyId;
	
	$sql = "SELECT MT.REMARKS";
	
	if($payMethod!='')
		$sql.=",FPM.strName AS payMethod";
	
	$sql.=",FD.strCode as costCenter
			FROM $mainTable MT ";
	if($payMethod!='')
		$sql.="INNER JOIN mst_financepaymentsmethods FPM ON FPM.intId=$payMethod ";
	
	$sql.="	INNER JOIN $detailTable MTD ON MTD.$serialNo=MT.$serialNo AND 
			MTD.$serialYear=MT.$serialYear
			INNER JOIN mst_financedimension FD ON FD.intId=MTD.$costCenter
			WHERE MT.$serialNo='$docNo' AND
			MT.$serialYear='$docYear' AND
			MT.COMPANY_ID='$companyId' ";

	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$data['remarks'] 	= $row['REMARKS'];
		if($payMethod!='')
			$data['payMethod'] 	= $row['payMethod'];
		else
			$data['payMethod'] 	= '';
		$data['costCenter'] = $row['costCenter'];
	}
	return $data;
}
function getCalculateType($financeId)
{
	global $db;
	
	$sql = "SELECT TRANSACTION_ADD_TYPE
			FROM finance_mst_account_type
			WHERE FINANCE_TYPE_ID='$financeId' AND
			STATUS='1'";
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	
	return $row['TRANSACTION_ADD_TYPE'];
}
function getAccountName($chartOfAccId,$transacType,$transacCat,$docNo,$docYear,$docType,$ftAmount)
{
	global $db;
	
	$sqlChk = "SELECT * FROM 
				finance_mst_chartofaccount_invoice_type
				WHERE INVOICE_TYPE_ID='3' AND
				CHART_OF_ACCOUNT_ID='$chartOfAccId' AND
				TRANSACTION_CATEGORY='$transacCat' ";
	$resultChk = $db->RunQuery($sqlChk);
	$count = mysqli_num_rows($resultChk);
	if($count>0)
	{
		$sql = "SELECT CHART_OF_ACCOUNT_NAME,COA.CHART_OF_ACCOUNT_ID
				FROM finance_mst_chartofaccount COA
				INNER JOIN finance_mst_chartofaccount_invoice_type COAIT ON COAIT.CHART_OF_ACCOUNT_ID=COA.CHART_OF_ACCOUNT_ID
				WHERE COAIT.INVOICE_TYPE_ID='3' AND
				TRANSACTION_CATEGORY='$transacCat' AND
				TRANSACTION_TYPE<>'$transacType' AND
				COAIT.CHART_OF_ACCOUNT_ID<>'$chartOfAccId' ";
		$result = $db->RunQuery($sql);
	}
	else if($count<=0 && ($docType=='ISSUE' || $docType=='RETSTORES'))
	{
		$sql = "SELECT CHART_OF_ACCOUNT_NAME,COA.CHART_OF_ACCOUNT_ID
				FROM finance_transaction FT
				INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=FT.CHART_OF_ACCOUNT_ID
				WHERE FT.DOCUMENT_NO='$docNo' AND
				FT.DOCUMENT_YEAR='$docYear' AND
				FT.TRANSACTION_CATEGORY='$transacCat' AND
				FT.DOCUMENT_TYPE='$docType' AND
				FT.TRANSACTION_TYPE <>'$transacType' AND
				FT.AMOUNT='$ftAmount' ";
	}
	else
	{
		$sql = "SELECT CHART_OF_ACCOUNT_NAME,COA.CHART_OF_ACCOUNT_ID
				FROM finance_transaction FT
				INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=FT.CHART_OF_ACCOUNT_ID
				WHERE FT.DOCUMENT_NO='$docNo' AND
				FT.DOCUMENT_YEAR='$docYear' AND
				FT.TRANSACTION_CATEGORY='$transacCat' AND
				FT.DOCUMENT_TYPE='$docType' AND
				FT.TRANSACTION_TYPE <>'$transacType'
				ORDER BY AMOUNT DESC
				LIMIT 0,1";
	}
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	
	$data['accName']	= $row['CHART_OF_ACCOUNT_NAME'];
	$data['GLId']		= $row['CHART_OF_ACCOUNT_ID'];
	
	return $data;
}
function getInvoiceNo($transactionCat,$docType,$docNo,$docYear)
{
	global $db;
	$invoiceNo = '';
	if($transactionCat=='SU')
	{
		if($docType=='INVOICE')
		{
			$sql = "SELECT INVOICE_NO as invoiceNo
					FROM finance_supplier_purchaseinvoice_header
					WHERE PURCHASE_INVOICE_NO='$docNo' AND PURCHASE_INVOICE_YEAR='$docYear' ";
			$result = $db->RunQuery($sql);
			$row = mysqli_fetch_array($result);
			
			$invoiceNo = $row['invoiceNo'];
		}
		
	}
	else
	{
		if($docType=='INVOICE')
		{
			$sql = "SELECT INVOICE_NO AS invoiceNo
					FROM finance_customer_invoice_header
					WHERE finance_customer_invoice_header.SERIAL_NO='$docNo' AND finance_customer_invoice_header.SERIAL_YEAR='$docYear'";
			$result = $db->RunQuery($sql);
			$row = mysqli_fetch_array($result);
			
			$invoiceNo = $row['invoiceNo'];
		}
		
	}
	return $invoiceNo;
}
function getLink($docType,$transacCat,$docNo,$docYear)
{
	global $invReportId;
	global $advPayReportId;	
	global $supPayReportId;	
	global $supDebitReportId;
	global $supCreditReportId;
	global $cusInvReportId;
	global $cusAdvReportId;
	global $cusPayRecvReportId;
	global $cusDbtNoteReportId;
	global $cusCrdNoteReportId;
	global $cusPayRcvSetlReportId;
	global $otrBillInvReportId;
	global $otrBillPayReportId;
	global $otrBillRcvInvReportId;
	global $otrBillRcvPayReportId;
	global $jurnlEntrReportId;
	global $bnkPayReportId;
	
	if($transacCat=='SU')
	{
		switch($docType)
		{
			case 'INVOICE':
				$link = "?q=".$invReportId."&purInvoiceNo=$docNo&purInvoiceYear=$docYear";
			break;
			case 'ADVANCE':
				$link = "?q=".$advPayReportId."&advancePayNo=$docNo&advancePayYear=$docYear";
			break;
			case 'PAYMENT':
				$link = "?q=".$supPayReportId."&paymentNo=$docNo&paymentYear=$docYear";
			break;
			case 'DEBIT':
				$link = "?q=".$supDebitReportId."&debitNo=$docNo&debitYear=$docYear";
			break;
			case 'CREDIT':
				$link = "?q=".$supCreditReportId."&CreditNo=$docNo&CreditYear=$docYear";
			break;
		}
	}
	else if($transacCat=='CU')
	{
		switch($docType)
		{
			case 'INVOICE':
				$link = "?q=".$cusInvReportId."&SerialNo=$docNo&SerialYear=$docYear";
			break;
			case 'ADVANCE':
				$link = "?q=".$cusAdvReportId."&AdvanceNo=$docNo&AdvanceYear=$docYear";
			break;
			case 'PAYRECEIVE':
				$link = "?q=".$cusPayRecvReportId."&SerialNo=$docNo&SerialYear=$docYear";
			break;
			case 'DEBIT':
				$link = "?q=".$cusDbtNoteReportId."&debitNoteNo=$docNo&debitNoteYear=$docYear";
			break;
			case 'CREDIT':
				$link = "?q=".$cusCrdNoteReportId."&SerialNo=$docNo&SerialYear=$docYear";
			break;
			case 'INV_SETTLEMENT':
				$link = "?q=".$cusPayRcvSetlReportId."&SerialNo=$docNo&SerialYear=$docYear";
			break;
		}
	}
	else if($transacCat=='OP')
	{
		switch($docType)
		{
			case 'BILLINVOICE':
				$link = "?q=".$otrBillInvReportId."&billInvoiceNo=$docNo&billInvoiceYear=$docYear";
			break;
			case 'BILLPAYMENT':
				$link = "?q=".$otrBillPayReportId."&paymentNo=$docNo&paymentYear=$docYear";
			break;
		}
	}
	else if($transacCat=='OR')
	{
		switch($docType)
		{
			case 'BILLINVOICE':
				$link = "?q=".$otrBillRcvInvReportId."&invoiceNo=$docNo&invoiceYear=$docYear";
			break;
			case 'BILLPAYMENT':
				$link = "?q=".$otrBillRcvPayReportId."&receiveNo=$docNo&receiveYear=$docYear";
			break;
		}
	}
	else if($transacCat=='JE')
	{
		switch($docType)
		{
			case 'JOURNAL_ENTRY':
				$link = "?q=".$jurnlEntrReportId."&SerialNo=$docNo&SerialYear=$docYear";
			break;
		}
	}
	else
	{
		$link = '';
	}
	return $link;
}
function getBaseCurrency($companyId)
{
	global $db;
	
	$sql 	= "SELECT intBaseCurrencyId FROM mst_companies WHERE intId='$companyId' ";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	
	return $row['intBaseCurrencyId'];
	
}
?>

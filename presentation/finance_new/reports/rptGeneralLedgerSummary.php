<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
	$locationId			= $_SESSION["CompanyID"];
	$companyId			= $_SESSION["headCompanyId"];
	
	$fromDate			= $_REQUEST['fromDate'];
	$toDate				= $_REQUEST['toDate'];
	$currency			= $_REQUEST['currency'];
	$finaceType			= ($_REQUEST['financeType']=='null'?'':$_REQUEST['financeType']);
	$mainCatId			= ($_REQUEST['mainCateId']=='null'?'':$_REQUEST['mainCateId']);
	$subCatId			= ($_REQUEST['subCatId']=='null'?'':$_REQUEST['subCatId']);
	$GLAccId			= ($_REQUEST['chartOfAccId']=='null'?'':$_REQUEST['chartOfAccId']);
	
	$baseCurrency		= getBaseCurrency($companyId);
	$data['headerArr'] 	= GetHeaderDetails($currency,$finaceType,$mainCatId,$subCatId,$GLAccId);
	$detail_result 		= GetGridDetails($finaceType,$mainCatId,$subCatId,$GLAccId,$fromDate,$toDate,$currency);
	
	$openingBal			= getOpeningBalance($finaceType,$mainCatId,$subCatId,$GLAccId,$fromDate,$currency);
	$closingBal			= 0;
	
	$invReportId			= 893;
	$advPayReportId			= 953;	
	$supPayReportId			= 943;
	$supDebitReportId		= 1082;
	$supCreditReportId		= 1086;
	$cusInvReportId			= 943;
	$cusAdvReportId			= 899;
	$cusPayRecvReportId		= 930;
	$cusDbtNoteReportId		= 928;
	$cusCrdNoteReportId		= 913;
	$cusPayRcvSetlReportId	= 933;
	$otrBillInvReportId		= 979;
	$otrBillPayReportId		= 946;
	$otrBillRcvInvReportId	= 1087;
	$otrBillRcvPayReportId	= 1093;
	$jurnlEntrReportId		= 986;
	$bnkPayReportId			= 987;
	$glAgingReportId		= 926;
?>
<head>
    <title>General Ledger - Summary</title>
</head>
<body>
<form id="frmGeneralLedgerSummary" name="frmGeneralLedgerSummary" method="post">
  <table width="1100" align="center">
    <tr>
      <td><?php include 'reportHeader.php'; ?></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="reportHeader" style="text-align:center"><strong>Ledger Details - Summary (General Ledger <?php echo $data['headerArr']['CURName']; ?>)</strong></td>
    </tr>
    <tr>
      <td class="normalfnt" style="text-align:center"><table align="center" width="300" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="64" class="normalfnt"><strong>From&nbsp;&nbsp;:</strong></td>
            <td width="92" class="normalfnt"><?php echo $fromDate; ?></td>
            <td width="45" class="normalfnt"><strong>To&nbsp;&nbsp;:</strong></td>
            <td width="99" class="normalfnt"><?php echo $toDate; ?></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td class="reportHeader" style="text-align:center">&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" border="0">
        <tr class="normalfnt">
          <td width="16%">Finance Type</td>
          <td width="1%" style="text-align:center">:</td>
          <td width="32%"><?php echo $data['headerArr']['FTName']; ?></td>
          <td width="17%">Main Category</td>
           <td width="2%" style="text-align:center">:</td>
            <td width="32%"><?php echo $data['headerArr']['MCName']; ?></td>
        </tr>
        <tr class="normalfnt">
          <td>Sub Category</td>
          <td style="text-align:center">:</td>
          <td><?php echo $data['headerArr']['SCName']; ?></td>
          <td>Chart Of Account</td>
           <td style="text-align:center">:</td>
            <td><a href="?q=<?php echo $glAgingReportId; ?>&fromDate=<?php echo $fromDate; ?>&toDate=<?php echo $toDate; ?>&currency=<?php echo $currency; ?>&chartOfAccId=<?php echo $GLAccId; ?>" target="rptGeneralLedgerAging.php"><?php echo $data['headerArr']['CAName']; ?></a></td>
        </tr>
      </table></td>
    </tr>
    <tr>
    	<td><table width="100%" border="0">
    	  <tr class="normalfnt">
    	    <td width="80%">&nbsp;</td>
    	    <td width="11%">Opening Balance</td>
    	    <td width="1%">:</td>
    	    <td width="8%" style="text-align:right"><?php echo number_format($openingBal,2); ?></td>
  	      </tr>
  	  </table></td>
    </tr>
    <tr>
        <td class="reportHeader" style="text-align:center">
            <table width="100%" class="rptBordered" id="tblMain" border="0">
            <thead>
                <tr>
                    <th width="3%">&nbsp;</th>
                    <th width="19%">Chart of Account</th>
                    <th width="8%">Date</th>
                    <th width="10%">Invoice No</th>
                    <th width="10%">Document No</th>
                    <th width="11%">Reference No</th>
                    <th width="15%">Account Name</th>
                    <th width="8%">Debit Amt</th>
                    <th width="8%">Credit Amt</th>
                    <th width="8%">Closing Balance</th>
                </tr>
       		 </thead>
			<?php
				$loop = 0;
				while($row=mysqli_fetch_array($detail_result))
				{
					$rowChk         = false;
					$accountArr		= getAccountName($row['CHART_OF_ACCOUNT_ID'],$row['TRANSACTION_TYPE'],$row['TRANSACTION_CATEGORY'],$row['DOC_NO'],$row['DOC_YEAR'],$row['DOCUMENT_TYPE'],$row['ftAmount']);
					
					//$savedCurrencyRate 	= getCurrencyRate($row['CURRENCY_ID'],$row['dtmDate']);
					$invoiceNo			= getInvoiceNo($row['TRANSACTION_CATEGORY'],$row['DOCUMENT_TYPE'],$row['DOC_NO'],$row['DOC_YEAR']);
					//$targetCurrencyRate = getCurrencyRate($currency,$row['dtmDate']);
					//$rwAmount 			= $row['amount'];
					//$amount 			= ($rwAmount/$targetCurrencyRate)*$savedCurrencyRate;
					$amount 			= $row['amount'];
					
					$calculatePlusType  = getCalculateType($row['FINANCE_TYPE_ID']);
					
					if($calculatePlusType==$row['TRANSACTION_TYPE'])
					{
						$closingBal = $openingBal+$amount;
					}
					else
					{
						$closingBal = $openingBal-$amount;
					}
			?>
                    <tr>
                        <td class="normalfnt" style="text-align:center"><?php echo ++$i; ?>.</td>
                        <td class="normalfnt" style="text-align:left"><?php echo $row['CHART_OF_ACCOUNT_NAME']; ?></td>
                        <td class="normalfnt" style="text-align:center"><?php echo $row['dtmDate']; ?></td>
                        <td class="normalfnt" style="text-align:left"><?php echo ($invoiceNo==''?'&nbsp;':$invoiceNo); ?></td>
                        <td class="normalfnt" style="text-align:center"><?php echo ((getLink($row['DOCUMENT_TYPE'],$row['TRANSACTION_CATEGORY'],$row['DOC_NO'],$row['DOC_YEAR']))==''?$row['documentNo']:'<a href="'.getLink($row['DOCUMENT_TYPE'],$row['TRANSACTION_CATEGORY'],$row['DOC_NO'],$row['DOC_YEAR']).'" target="'.++$loop.'">'.$row["documentNo"].'</a>'); ?></td>
                        <td class="normalfnt" style="text-align:left"><?php echo ($row['BANK_REFERENCE_NO']==''?'&nbsp;':$row['BANK_REFERENCE_NO']); ?></td>
                        <td class="normalfnt" style="text-align:left"><?php echo ($accountArr['accName']==''?'&nbsp;':$accountArr['accName']); ?></td>
                        <td class="normalfnt" style="text-align:right"><?php echo ($row['TRANSACTION_TYPE']=='D'?number_format(abs($amount),2):'&nbsp;'); ?></td>
                        <td class="normalfnt" style="text-align:right"><?php echo ($row['TRANSACTION_TYPE']=='C'?number_format(abs($amount),2):'&nbsp;'); ?></td>
                        <td class="normalfnt" style="text-align:right"><?php echo number_format($closingBal,2); ?></td>
                    </tr>
			<?php
					$openingBal = $closingBal;
					
				}
            ?>
            </table>
      </td>
    </tr>
  </table>
</form>
</body>
</html>
<?php
function GetGridDetails($finaceType,$mainCatId,$subCatId,$GLAccId,$fromDate,$toDate)
{
	global $db;
	global $companyId;
	global $currency;
	global $baseCurrency;
	
	/*$sql ="SELECT FT.CHART_OF_ACCOUNT_ID,
			COA.CHART_OF_ACCOUNT_NAME,
			DATE(FT.LAST_MODIFIED_DATE) AS dtmDate,
			CONCAT(FT.INVOICE_YEAR,'-',FT.INVOICE_NO) AS invoiceNo,
			CONCAT(FT.DOCUMENT_YEAR,'-',FT.DOCUMENT_NO) AS documentNo,
			SUM(FT.AMOUNT) AS amount,
			FT.TRANSACTION_TYPE,
			FT.TRANSACTION_CATEGORY,
			FT.DOCUMENT_NO as DOC_NO,
			FT.DOCUMENT_YEAR as DOC_YEAR,
			FT.DOCUMENT_TYPE,
			FT.CURRENCY_ID,
			AT.FINANCE_TYPE_ID,
			FT.BANK_REFERENCE_NO
			FROM finance_transaction FT
			INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=FT.CHART_OF_ACCOUNT_ID
			INNER JOIN finance_mst_account_sub_type ST ON ST.SUB_TYPE_ID=COA.SUB_TYPE_ID
			INNER JOIN finance_mst_account_main_type MT ON MT.MAIN_TYPE_ID=ST.MAIN_TYPE_ID
			INNER JOIN finance_mst_account_type AT ON AT.FINANCE_TYPE_ID=MT.FINANCE_TYPE_ID
			WHERE FT.COMPANY_ID='$companyId' ";
	if($fromDate!='' && $toDate!='')
		$sql.="AND DATE(FT.LAST_MODIFIED_DATE) BETWEEN ('$fromDate') AND ('$toDate') ";		
	if($finaceType!='')
		$sql.="AND AT.FINANCE_TYPE_ID='$finaceType' ";
	if($mainCatId!='')
		$sql.="AND MT.MAIN_TYPE_ID='$mainCatId' ";
	if($subCatId!='')
		$sql.="AND ST.SUB_TYPE_ID='$subCatId' ";
	if($GLAccId!='')
		$sql.="AND COA.CHART_OF_ACCOUNT_ID='$GLAccId' "
	
	$sql.=" GROUP BY FT.CHART_OF_ACCOUNT_ID,DATE(FT.LAST_MODIFIED_DATE),FT.INVOICE_NO,
			FT.INVOICE_YEAR,FT.DOCUMENT_NO,FT.DOCUMENT_YEAR,FT.TRANSACTION_TYPE,FT.TRANSACTION_CATEGORY
			ORDER BY FT.LAST_MODIFIED_DATE ";*/
	$WHsql = '';
	if($fromDate!='' && $toDate!='')
		$WHsql.="AND DATE(FT.LAST_MODIFIED_DATE) BETWEEN ('$fromDate') AND ('$toDate') ";		
	if($finaceType!='')
		$WHsql.="AND AT.FINANCE_TYPE_ID='$finaceType' ";
	if($mainCatId!='')
		$WHsql.="AND MT.MAIN_TYPE_ID='$mainCatId' ";
	if($subCatId!='')
		$WHsql.="AND ST.SUB_TYPE_ID='$subCatId' ";
	if($GLAccId!='')
		$WHsql.="AND COA.CHART_OF_ACCOUNT_ID='$GLAccId' ";
	if($baseCurrency!=$currency)
		$WHsql.="AND FT.DOCUMENT_TYPE NOT IN ('GAIN_LOSS') ";
		
	$sql =" SELECT T1.*,SUM(lastAmonut) AS amount FROM(SELECT
			FT.CHART_OF_ACCOUNT_ID,
			COA.CHART_OF_ACCOUNT_NAME,
			DATE(FT.LAST_MODIFIED_DATE) AS dtmDate,
			CONCAT(FT.INVOICE_YEAR,'-',FT.INVOICE_NO) AS invoiceNo,
			CONCAT(FT.DOCUMENT_YEAR,'-',FT.DOCUMENT_NO) AS documentNo,
			
			ROUND(COALESCE(FT.AMOUNT / (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = $currency AND 
  			DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId) * (SELECT SUB_ER.dblExcAvgRate FROM 
 			mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID AND 
			DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId),0),2) AS lastAmonut,
			
			FT.AMOUNT as ftAmount,
			FT.INVOICE_NO,
			FT.INVOICE_YEAR,
			FT.DOCUMENT_NO,
			FT.DOCUMENT_YEAR,
			FT.TRANSACTION_TYPE,
			FT.TRANSACTION_CATEGORY,
			FT.DOCUMENT_NO            AS DOC_NO,
			FT.DOCUMENT_YEAR          AS DOC_YEAR,
			FT.DOCUMENT_TYPE,
			FT.CURRENCY_ID,
			AT.FINANCE_TYPE_ID,
			FT.BANK_REFERENCE_NO,
			FT.LAST_MODIFIED_DATE,
			FT.SERIAL_ID
			
			FROM finance_transaction FT
			INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=FT.CHART_OF_ACCOUNT_ID
			INNER JOIN finance_mst_account_sub_type ST ON ST.SUB_TYPE_ID=COA.SUB_TYPE_ID
			INNER JOIN finance_mst_account_main_type MT ON MT.MAIN_TYPE_ID=ST.MAIN_TYPE_ID
			INNER JOIN finance_mst_account_type AT ON AT.FINANCE_TYPE_ID=MT.FINANCE_TYPE_ID
			WHERE FT.COMPANY_ID='$companyId' 
			$WHsql)AS T1
			GROUP BY CHART_OF_ACCOUNT_ID,DATE(LAST_MODIFIED_DATE),INVOICE_NO, INVOICE_YEAR,DOCUMENT_NO,
			DOCUMENT_YEAR,TRANSACTION_TYPE,TRANSACTION_CATEGORY,DOCUMENT_TYPE
			ORDER BY LAST_MODIFIED_DATE,SERIAL_ID 
			limit 2";
	
	$result = $db->RunQuery($sql);
	return $result;
}
function GetHeaderDetails($currency,$finaceType,$mainCatId,$subCatId,$GLAccId)
{
	global $db;
	$sqlCUR = "SELECT strCode
				FROM mst_financecurrency
				WHERE intId='$currency' ";
	$resultCUR = $db->RunQuery($sqlCUR);
	$rowCUR = mysqli_fetch_array($resultCUR);
	
	$sqlFT = "SELECT FINANCE_TYPE_NAME
				FROM finance_mst_account_type
				WHERE FINANCE_TYPE_ID='$finaceType' ";
	
	$resultFT = $db->RunQuery($sqlFT);
	$rowFT = mysqli_fetch_array($resultFT);
	
	$sqlSC = "SELECT SUB_TYPE_NAME
				FROM finance_mst_account_sub_type
				WHERE SUB_TYPE_ID='$subCatId' ";
	
	$resultSC = $db->RunQuery($sqlSC);
	$rowSC = mysqli_fetch_array($resultSC);
	
	$sqlMC = "SELECT MAIN_TYPE_NAME
				FROM finance_mst_account_main_type
				WHERE MAIN_TYPE_ID='$mainCatId' ";
	
	$resultMC = $db->RunQuery($sqlMC);
	$rowMC = mysqli_fetch_array($resultMC);
	
	$sqlCA = "SELECT CHART_OF_ACCOUNT_NAME
				FROM finance_mst_chartofaccount
				WHERE CHART_OF_ACCOUNT_ID='$GLAccId' ";
	
	$resultCA = $db->RunQuery($sqlCA);
	$rowCA = mysqli_fetch_array($resultCA);
	
	$data['FTName']  = $rowFT['FINANCE_TYPE_NAME'];
	$data['SCName']  = $rowSC['SUB_TYPE_NAME'];
	$data['MCName']  = $rowMC['MAIN_TYPE_NAME'];
	$data['CAName']  = $rowCA['CHART_OF_ACCOUNT_NAME'];
	$data['CURName'] = $rowCUR['strCode'];
	
	return $data;
	
}
function getCurrencyRate($currency,$date)
{
	global $db;
	global $companyId;
	
	$sql = "SELECT dblExcAvgRate
			FROM mst_financeexchangerate
			WHERE intCurrencyId='$currency' AND
			dtmDate='$date' AND
			intCompanyId='$companyId' ";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	
	return $row['dblExcAvgRate'];
}
function getOpeningBalance($finaceType,$mainCatId,$subCatId,$GLAccId,$fromDate,$currency)
{
	global $db;
	global $companyId;
	global $baseCurrency;
	
	$sql ="SELECT ROUND(SUM((amount/targetExchangeRate)*savedExchangeRate),2) AS AMOUNT,
			transactionType
			FROM(
			SELECT FT.AMOUNT AS amount,
			FT.TRANSACTION_TYPE AS transactionType,
			(SELECT dblExcAvgRate 
			FROM mst_financeexchangerate
			WHERE mst_financeexchangerate.intCurrencyId=FT.CURRENCY_ID AND
			mst_financeexchangerate.intCompanyId='$companyId' AND
			mst_financeexchangerate.dtmDate=DATE(FT.LAST_MODIFIED_DATE)) AS savedExchangeRate,
			(SELECT dblExcAvgRate 
			FROM mst_financeexchangerate
			WHERE mst_financeexchangerate.intCurrencyId='$currency' AND
			mst_financeexchangerate.intCompanyId='$companyId' AND
			mst_financeexchangerate.dtmDate=DATE(FT.LAST_MODIFIED_DATE)) AS targetExchangeRate
			FROM finance_transaction FT
			INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=FT.CHART_OF_ACCOUNT_ID
			INNER JOIN finance_mst_account_sub_type ST ON ST.SUB_TYPE_ID=COA.SUB_TYPE_ID
			INNER JOIN finance_mst_account_main_type MT ON MT.MAIN_TYPE_ID=ST.MAIN_TYPE_ID
			INNER JOIN finance_mst_account_type AT ON AT.FINANCE_TYPE_ID=MT.FINANCE_TYPE_ID
			WHERE FT.COMPANY_ID='$companyId' AND DATE(FT.LAST_MODIFIED_DATE)<'$fromDate' ";
			
	if($finaceType!='')
		$sql.="AND AT.FINANCE_TYPE_ID='$finaceType' ";
	if($mainCatId!='')
		$sql.="AND MT.MAIN_TYPE_ID='$mainCatId' ";
	if($subCatId!='')
		$sql.="AND ST.SUB_TYPE_ID='$subCatId' ";
	if($GLAccId!='')
		$sql.="AND COA.CHART_OF_ACCOUNT_ID='$GLAccId' ";
	if($baseCurrency!=$currency)
		$sql.="AND FT.DOCUMENT_TYPE NOT IN ('GAIN_LOSS') ";
	
	$sql.="ORDER BY FT.CHART_OF_ACCOUNT_ID,FT.INVOICE_YEAR,FT.INVOICE_NO ) AS SUB_1 ";
	$sql.="GROUP BY transactionType ";
	//echo $sql;
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		if($row['transactionType']=='C')
			$creditAmount = $row['AMOUNT'];
		else
			$debitAmount = $row['AMOUNT'];
	}
	$calculatePlusType  = getCalculateType($finaceType);
	
	if($calculatePlusType=='C')
		$openingBal = $creditAmount-$debitAmount;
	else
		$openingBal = $debitAmount-$creditAmount;
	
	return ($openingBal==''?0:$openingBal);
}
function getCalculateType($financeId)
{
	global $db;
	
	$sql = "SELECT TRANSACTION_ADD_TYPE
			FROM finance_mst_account_type
			WHERE FINANCE_TYPE_ID='$financeId' AND
			STATUS='1'";
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	
	return $row['TRANSACTION_ADD_TYPE'];
}
function getAccountName($chartOfAccId,$transacType,$transacCat,$docNo,$docYear,$docType,$ftAmount)
{
	global $db;
	
	$sqlChk = "SELECT * FROM 
				finance_mst_chartofaccount_invoice_type
				WHERE INVOICE_TYPE_ID='3' AND
				CHART_OF_ACCOUNT_ID='$chartOfAccId' AND
				TRANSACTION_CATEGORY='$transacCat' ";
	$resultChk = $db->RunQuery($sqlChk);
	$count = mysqli_num_rows($resultChk);
	if($count>0)
	{
		$sql = "SELECT CHART_OF_ACCOUNT_NAME,COA.CHART_OF_ACCOUNT_ID
				FROM finance_mst_chartofaccount COA
				INNER JOIN finance_mst_chartofaccount_invoice_type COAIT ON COAIT.CHART_OF_ACCOUNT_ID=COA.CHART_OF_ACCOUNT_ID
				WHERE COAIT.INVOICE_TYPE_ID='3' AND
				TRANSACTION_CATEGORY='$transacCat' AND
				TRANSACTION_TYPE<>'$transacType' AND
				COAIT.CHART_OF_ACCOUNT_ID<>'$chartOfAccId' ";
		$result = $db->RunQuery($sql);
	}
	else if($count<=0 && ($docType=='ISSUE' || $docType=='RETSTORES'))
	{
		$sql = "SELECT CHART_OF_ACCOUNT_NAME,COA.CHART_OF_ACCOUNT_ID
				FROM finance_transaction FT
				INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=FT.CHART_OF_ACCOUNT_ID
				WHERE FT.DOCUMENT_NO='$docNo' AND
				FT.DOCUMENT_YEAR='$docYear' AND
				FT.TRANSACTION_CATEGORY='$transacCat' AND
				FT.DOCUMENT_TYPE='$docType' AND
				FT.TRANSACTION_TYPE <>'$transacType' AND
				FT.AMOUNT='$ftAmount' ";
	}
	else
	{
		$sql = "SELECT CHART_OF_ACCOUNT_NAME,COA.CHART_OF_ACCOUNT_ID
				FROM finance_transaction FT
				INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=FT.CHART_OF_ACCOUNT_ID
				WHERE FT.DOCUMENT_NO='$docNo' AND
				FT.DOCUMENT_YEAR='$docYear' AND
				FT.TRANSACTION_CATEGORY='$transacCat' AND
				FT.DOCUMENT_TYPE='$docType' AND
				FT.TRANSACTION_TYPE <>'$transacType'
				ORDER BY AMOUNT DESC
				LIMIT 0,1";
	}
	
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	
	$data['accName']	= $row['CHART_OF_ACCOUNT_NAME'];
	$data['GLId']		= $row['CHART_OF_ACCOUNT_ID'];
	
	return $data;
}
function getInvoiceNo($transactionCat,$docType,$docNo,$docYear)
{
	global $db;
	$invoiceNo = '';
	if($transactionCat=='SU')
	{
		if($docType=='INVOICE')
		{
			$sql = "SELECT INVOICE_NO as invoiceNo
					FROM finance_supplier_purchaseinvoice_header
					WHERE PURCHASE_INVOICE_NO='$docNo' AND PURCHASE_INVOICE_YEAR='$docYear' ";
			$result = $db->RunQuery($sql);
			$row = mysqli_fetch_array($result);
			
			$invoiceNo = $row['invoiceNo'];
		}
		
	}
	else if($transactionCat=='CU')
	{
		if($docType=='INVOICE')
		{
			$sql = "SELECT INVOICE_NO AS invoiceNo
					FROM finance_customer_invoice_header
					WHERE finance_customer_invoice_header.SERIAL_NO='$docNo' AND finance_customer_invoice_header.SERIAL_YEAR='$docYear'";
			$result = $db->RunQuery($sql);
			$row = mysqli_fetch_array($result);
			
			$invoiceNo = $row['invoiceNo'];
		}
		
	}
	else if($transactionCat=='OP')
	{
		if($docType=='BILLINVOICE')
		{
			$sql = "SELECT REFERENCE_NO AS invoiceNo
					FROM finance_other_payable_bill_header
					WHERE finance_other_payable_bill_header.BILL_INVOICE_NO='$docNo' AND finance_other_payable_bill_header.BILL_INVOICE_YEAR='$docYear'";
			$result = $db->RunQuery($sql);
			$row = mysqli_fetch_array($result);
			
			$invoiceNo = $row['invoiceNo'];
		}
		
	}
	else if($transactionCat=='OR')
	{
		if($docType=='BILLINVOICE')
		{
			$sql = "SELECT REFERENCE_NO AS invoiceNo
					FROM finance_other_receivable_invoice_header
					WHERE finance_other_receivable_invoice_header.BILL_INVOICE_NO='$docNo' AND 	
					finance_other_receivable_invoice_header.BILL_INVOICE_YEAR='$docYear'";
			$result = $db->RunQuery($sql);
			$row = mysqli_fetch_array($result);
			
			$invoiceNo = $row['invoiceNo'];
		}
		
	}
	return $invoiceNo;
}
function getLink($docType,$transacCat,$docNo,$docYear)
{
	global $invReportId;
	global $advPayReportId;	
	global $supPayReportId;	
	global $supDebitReportId;
	global $supCreditReportId;
	global $cusInvReportId;
	global $cusAdvReportId;
	global $cusPayRecvReportId;
	global $cusDbtNoteReportId;
	global $cusCrdNoteReportId;
	global $cusPayRcvSetlReportId;
	global $otrBillInvReportId;
	global $otrBillPayReportId;
	global $otrBillRcvInvReportId;
	global $otrBillRcvPayReportId;
	global $jurnlEntrReportId;
	global $bnkPayReportId;
	
	if($transacCat=='SU')
	{
		switch($docType)
		{
			case 'INVOICE':
				$link = "?q=".$invReportId."&purInvoiceNo=$docNo&purInvoiceYear=$docYear";
			break;
			case 'ADVANCE':
				$link = "?q=".$advPayReportId."&advancePayNo=$docNo&advancePayYear=$docYear";
			break;
			case 'PAYMENT':
				$link = "?q=".$supPayReportId."&paymentNo=$docNo&paymentYear=$docYear";
			break;
			case 'DEBIT':
				$link = "?q=".$supDebitReportId."&debitNo=$docNo&debitYear=$docYear";
			break;
			case 'CREDIT':
				$link = "?q=".$supCreditReportId."&CreditNo=$docNo&CreditYear=$docYear";
			break;
		}
	}
	else if($transacCat=='CU')
	{
		switch($docType)
		{
			case 'INVOICE':
				$link = "?q=".$cusInvReportId."&SerialNo=$docNo&SerialYear=$docYear";
			break;
			case 'ADVANCE':
				$link = "?q=".$cusAdvReportId."&AdvanceNo=$docNo&AdvanceYear=$docYear";
			break;
			case 'PAYRECEIVE':
				$link = "?q=".$cusPayRecvReportId."&SerialNo=$docNo&SerialYear=$docYear";
			break;
			case 'DEBIT':
				$link = "?q=".$cusDbtNoteReportId."&debitNoteNo=$docNo&debitNoteYear=$docYear";
			break;
			case 'CREDIT':
				$link = "?q=".$cusCrdNoteReportId."&SerialNo=$docNo&SerialYear=$docYear";
			break;
			case 'INV_SETTLEMENT':
				$link = "?q=".$cusPayRcvSetlReportId."&SerialNo=$docNo&SerialYear=$docYear";
			break;
		}
	}
	else if($transacCat=='OP')
	{
		switch($docType)
		{
			case 'BILLINVOICE':
				$link = "?q=".$otrBillInvReportId."&billInvoiceNo=$docNo&billInvoiceYear=$docYear";
			break;
			case 'BILLPAYMENT':
				$link = "?q=".$otrBillPayReportId."&paymentNo=$docNo&paymentYear=$docYear";
			break;
		}
	}
	else if($transacCat=='OR')
	{
		switch($docType)
		{
			case 'BILLINVOICE':
				$link = "?q=".$otrBillRcvInvReportId."&invoiceNo=$docNo&invoiceYear=$docYear";
			break;
			case 'BILLPAYMENT':
				$link = "?q=".$otrBillRcvPayReportId."&receiveNo=$docNo&receiveYear=$docYear";
			break;
		}
	}
	else if($transacCat=='JE')
	{
		switch($docType)
		{
			case 'JOURNAL_ENTRY':
				$link = "?q=".$jurnlEntrReportId."&SerialNo=$docNo&SerialYear=$docYear";
			break;
		}
	}
	else if($transacCat=='BP')
	{
		switch($docType)
		{
			case 'BANK_PAYMENT':
				$link = "?q=".$bnkPayReportId."&SerialNo=$docNo&SerialYear=$docYear";
			break;
		}
	}
	else
	{
		$link = '';
	}
	return $link;
}
function getBaseCurrency($companyId)
{
	global $db;
	
	$sql 	= "SELECT intBaseCurrencyId FROM mst_companies WHERE intId='$companyId' ";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	
	return $row['intBaseCurrencyId'];
	
}
?>
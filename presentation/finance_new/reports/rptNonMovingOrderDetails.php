<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
ini_set('max_execution_time',6000000);

//$backwardseperator = "../../../../";
include_once "class/tables/trn_orderheader.php";						$trn_orderheader			= new trn_orderheader($db);
include_once "class/tables/mst_customer.php";							$mst_customer				= new mst_customer($db);
include_once "class/tables/mst_plant.php";								$mst_plant					= new mst_plant($db);
include_once "class/tables/mst_brand.php";								$mst_brand					= new mst_brand($db);

$programCode	= 'P01138';
$userId			= $_SESSION['userId'];
$sqlp 		= "SELECT
				menupermision.intView  
				FROM menupermision 
				Inner Join menus ON menupermision.intMenuId = menus.intId
				WHERE
				menus.strCode =  '$programCode' AND
				menupermision.intUserId =  '$userId'";	
			
$resultp 	= $db->RunQuery($sqlp);
$rowp		= mysqli_fetch_array($resultp);
	 
if($rowp['intView'] != 1)
{
	include 'dataAccess/permissionDenied.php';
	exit();	
}

$clusterId			= $_REQUEST['Cluster'];
$dateTo				= $_REQUEST['DateTo'];
$datefrom			= $_REQUEST['DateFrom'];
$deci				= 2;
$OrderComYear		= date('Y')-2 ;
$reportId			= 9;


?>
<?php 
	 $sql	=  "SELECT
				sys_main_clusters.`NAME`,
				sys_main_clusters.ID
				FROM
				sys_main_clusters
				WHERE
				sys_main_clusters.ID = '$clusterId'
				";
				
	$resultC	= $db->RunQuery($sql);
	$rowCluster	= mysqli_fetch_array($resultC);
	$cluster   = $rowCluster['NAME'];
		
?>
<style type="text/css">
#progress {
 width: 100px;   
 border: 1px solid black;
 position: relative;
 padding: 1px;
}

#percent {
 position: absolute;   
 left: 50%;
}

#bar {
 height: 15px;
 background-color: #0C0;
}
</style>
<title>Non Moving Order Report</title>
<form id="frmCustomerInvoiceAgingSOWiseReport" name="frmCustomerInvoiceAgingSOWiseReport" method="post">
<div align="center">
<div><strong>Non Moving Order Details - <?php echo $cluster?></strong></div>
	<table width="100%" border="0" align="center">
    
    <tr>
    	<td>
        	<table  border="0" cellpadding="0" cellspacing="0" class="bordered" width="100%" bgcolor="#FFFFFF">
            <thead>
                <tr valign="top">
                    <th>Customer Name</th>
                    <th>Customer Location</th>
                    <th>Production Plant</th>
                    <th>PONo</th>
                    <th>Order No</th>
                    <th>Sales Order</th>
                    <th>Brand</th>
                    <th>Order Date</th>
                    <th>Invoice No</th>
                    <th>Graphic No's</th>
                    <th>Order Qty</th>
                    <th>Dispatch Qty</th>
                    <th>Dispatch Prog</th>
                    <th>Rate</th>
                    <th>Dispatched Amount</th>
                    <th>Invoiced Amount</th>
                    <th>Last Dispatch Date</th>
                    <th>Idle Period</th>
            	</tr>
            </thead>
            <tbody>
            <?php
			$result			= getDetailData($clusterId,$datefrom,$dateTo);
			while($row = mysqli_fetch_array($result))
			{
				$progress 	= (round((round($row["DISPATCH_QTY"])/round($row["ORDER_QTY"]))*100,2)>100?100:round((round($row["DISPATCH_QTY"])/round($row["ORDER_QTY"]))*100,2));
				
				$mst_customer->set($row['CUSTOMER_ID']);
				$mst_plant->set($row['PLANTID']);
				$mst_brand->set($row['intBrand']);
				
				$customerName	= $mst_customer->getstrName();
				$plantName		= $mst_plant->getstrPlantName();
				$brandName		= $mst_brand->getstrName();
							
				 	
			?>	
                <tr>
                    <td nowrap="nowrap" title="Customer Name" style="text-align:left"><?php echo $customerName;?></td>
                    <td nowrap="nowrap" title="Customer Location" style="text-align:left"><?php echo $row['CUSTOMER_LOCATION'];?></td>
                    <td nowrap="nowrap" title="Production Plant" style="text-align:left"><?php echo $row['DISP_PLANTS'];?></td>
                   	<td nowrap="nowrap" title="Customer PO No" style="text-align:left"><?php echo $row['CUSTOMER_PONO'];?></td>
         			<td nowrap="nowrap" title="Order No" style="text-align:center"><?php echo $row['ORDER_NO'];?></td>
                    <td nowrap="nowrap" title="Sales Order No" style="text-align:left"><?php echo $row['SALESORDER'];?></td>
                    <td nowrap="nowrap" title="Order date" style="text-align:center"><span style="text-align:left"><?php echo $brandName;?></span></td>
                    <td nowrap="nowrap" title="Order date" style="text-align:center"><?php echo $row['ORDER_DATE'];?></td>
                    <td nowrap="nowrap" title="Invoice No" style="text-align:left"><?php echo $row['INVOICE_NO'];?></td>
                    <td nowrap="nowrap" title="Graphic No" style="text-align:left"><?php echo $row['GRAPHIC_NO'];?></td>
                    <td nowrap="nowrap" title="Order Qty" style="text-align:right"><?php echo $row['ORDER_QTY'];?></td>
                    <td nowrap="nowrap" title="Dispatched Qty" style="text-align:right"><?php echo $row['DISPATCH_QTY'];?></td>
                    <td nowrap="nowrap" title="Dispatched Prog"><div id="progress"><span id="percent"><?php echo $progress?>%</span><div id="bar" style="width:<?php echo $progress?>px"></div></div></td>
                    <td nowrap="nowrap" title="Order Rate" style="text-align:right"><?php echo $row['ORDER_RATE'];?></td>
                    <td nowrap="nowrap" title="Dispatched Amount" style="text-align:right"><?php echo $row['DISPATCH_AMOUNT'];?></td>
                    <td nowrap="nowrap" title="Invoiced Amount" style="text-align:right"><?php echo $row['INVOICE_AMOUNT'];?></td>
                    <td nowrap="nowrap" title="Last Dispatched Date" style="text-align:center"><?php echo $row['DISPATCH_DATE'];?></td>
                    <td nowrap="nowrap" title="Last Dispatched Date" style="text-align:center"><?php echo $row['IDLE_PERIOD'];?></td>
                </tr>
                <?php
				
				$tot_order_qty						+= round($row["ORDER_QTY"]);
				$tot_dispatch_qty					+= round($row["DISPATCH_QTY"]);
				$tot_dispatched_amount 				+= round($row["DISPATCH_AMOUNT"],$deci);
				$tot_invoiced_amount 				+= round($row["INVOICE_AMOUNT"],$deci);
			}
			?>
            </tbody>
            <tfoot>
            	<tr style="font-weight:bold;text-align:right">
                    <td nowrap="nowrap" title="Customer Name" style="text-align:left">&nbsp;</td>
                    <td nowrap="nowrap" title="Customer Location" style="text-align:left">&nbsp;</td>
                    <td nowrap="nowrap" title="Production Plant" style="text-align:left">&nbsp;</td>
                   	<td nowrap="nowrap" title="Customer PO No" style="text-align:left">&nbsp;</td>
         			<td nowrap="nowrap" title="Order No" style="text-align:center">&nbsp;</td>
                    <td nowrap="nowrap" title="Sales Order No" style="text-align:left">&nbsp;</td>
                    <td nowrap="nowrap" title="Order date" style="text-align:center">&nbsp;</td>
                    <td nowrap="nowrap" title="Order date" style="text-align:center">&nbsp;</td>
                    <td nowrap="nowrap" title="Invoice No" style="text-align:left">&nbsp;</td>
                    <td nowrap="nowrap" title="Graphic No" style="text-align:left">&nbsp;</td>
                    <td nowrap="nowrap" title="Order Qty" style="text-align:right">&nbsp;</td>
                    <td nowrap="nowrap" title="Dispatched Qty" style="text-align:right">&nbsp;</td>
                    <td nowrap="nowrap" title="Dispatched Prog">&nbsp;</td>
                    <td nowrap="nowrap" title="Order Rate" style="text-align:right">&nbsp;</td>
                    <td nowrap="nowrap" title="Dispatched Amount" style="text-align:right">&nbsp;</td>
                    <td nowrap="nowrap" title="Invoiced Amount" style="text-align:right">&nbsp;</td>
                    <td nowrap="nowrap" title="Last Dispatched Date" style="text-align:center">&nbsp;</td>
                </tr>	
            </tfoot>
            </table>
        </td>
    </tr>
    <tr height="40">
    	<td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
    </tr>
    </table> 
</div>
</form>
<?php
function getDetailData($clusterId,$datefrom,$dateTo)
{
	global $db;
	global $companyId;
	global $deci;
	
	if($datefrom !='' && $dateTo!='')
	$para = " AND OH.dtDate<='$dateTo' AND OH.dtDate >= '$datefrom' ";
	else
	$para = " AND OH.dtDate >= '2015-04-01'";
		
	$sql =  "SELECT 
			OH.intCurrency										AS CURRENCY_ID,
			OH.intCustomer										AS CUSTOMER_ID,
			CL.strName											AS CUSTOMER_LOCATION,
			OH.dtDate 											AS ORDER_DATE,
			CONCAT(OH.intOrderYear,' - ',OH.intOrderNo) 		AS ORDER_NO,
			OH.strCustomerPoNo 									AS CUSTOMER_PONO,
			OD.strSalesOrderNo									AS SALESORDER,
			OD.intSalesOrderId									AS SALESORDERID,
			L.intPlant											AS PLANTID,						
			OD.intQty        									AS ORDER_QTY,
			ROUND((OD.intQty * OD.dblPrice),0) 					AS ORDER_AMOUNT,
			ROUND((OD.intQty * OD.dblPrice)/(OD.intQty),4) 		AS ORDER_RATE,
			OD.strGraphicNo										AS GRAPHIC_NO,
			
			(SELECT
			DATE(FDH.dtmdate)
			FROM ware_fabricdispatchdetails FDD
			INNER JOIN ware_fabricdispatchheader FDH
			ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
			AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
			WHERE FDH.intOrderNo = OH.intOrderNo
			AND FDH.intOrderYear = OH.intOrderYear
			AND FDD.intSalesOrderId = OD.intSalesOrderId
			AND FDH.intStatus = 1 
			ORDER BY DATE(FDH.dtmdate) DESC LIMIT 1)							AS DISPATCH_DATE,

			DATEDIFF(DATE(NOW()),(SELECT
			DATE(FDH.dtmdate)
			FROM ware_fabricdispatchdetails FDD
			INNER JOIN ware_fabricdispatchheader FDH
			ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
			AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
			WHERE FDH.intOrderNo = OH.intOrderNo
			AND FDH.intOrderYear = OH.intOrderYear
			AND FDD.intSalesOrderId = OD.intSalesOrderId
			AND FDH.intStatus = 1 
			ORDER BY DATE(FDH.dtmdate) DESC LIMIT 1)) AS IDLE_PERIOD,

			(SELECT
			FDH.intStatus
			FROM ware_fabricdispatchdetails FDD
			INNER JOIN ware_fabricdispatchheader FDH
			ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
			AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
			WHERE FDH.intOrderNo = OH.intOrderNo
			AND FDH.intOrderYear = OH.intOrderYear
			AND FDD.intSalesOrderId = OD.intSalesOrderId
			AND FDH.intStatus = 1 
			ORDER BY DATE(FDH.dtmdate) DESC LIMIT 1)							AS DISPATCH_STATUS,
			
			ROUND(COALESCE((SELECT SUM(FDD.dblGoodQty)
			FROM ware_fabricdispatchdetails FDD 
			INNER JOIN ware_fabricdispatchheader FDH 
			ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
			AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
			WHERE FDH.intOrderNo = OH.intOrderNo 
			AND FDH.intOrderYear = OH.intOrderYear
			AND FDD.intSalesOrderId = OD.intSalesOrderId 
			AND FDH.intStatus = 1),0))											AS DISPATCH_QTY,
			
			ROUND(COALESCE((SELECT
			SUM(FDD.dblGoodQty*OD1.dblPrice)
			FROM
			ware_fabricdispatchdetails AS FDD
			INNER JOIN ware_fabricdispatchheader AS FDH ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
			INNER JOIN trn_orderdetails AS OD1 ON FDH.intOrderNo = OD1.intOrderNo AND FDH.intOrderYear = OD1.intOrderYear AND FDD.intSalesOrderId = OD1.intSalesOrderId
			WHERE
			FDH.intOrderNo = OH.intOrderNo AND
			FDH.intOrderYear = OH.intOrderYear AND
			FDD.intSalesOrderId = OD.intSalesOrderId AND 
			FDH.intStatus = 1
			),0),$deci)																AS DISPATCH_AMOUNT,
					
			(SELECT DISTINCT GROUP_CONCAT(CSIH.INVOICE_NO)
			FROM finance_customer_invoice_header CSIH  
			INNER JOIN finance_customer_invoice_details CSID ON CSIH.SERIAL_NO = CSID.SERIAL_NO AND CSIH.SERIAL_YEAR = CSID.SERIAL_YEAR
			WHERE CSIH.ORDER_NO = OH.intOrderNo
			AND CSIH.ORDER_YEAR = OH.intOrderYear
			AND CSID.SALES_ORDER_ID = OD.intSalesOrderId
			AND CSIH.STATUS = 1) 												AS INVOICE_NO,
					
			ROUND(COALESCE((SELECT SUM(CSID.VALUE+CSID.TAX_VALUE)
			FROM finance_customer_invoice_header CSIH  
			INNER JOIN finance_customer_invoice_details CSID ON CSIH.SERIAL_NO = CSID.SERIAL_NO AND CSIH.SERIAL_YEAR = CSID.SERIAL_YEAR
			WHERE CSIH.ORDER_NO = OH.intOrderNo
			AND CSIH.ORDER_YEAR = OH.intOrderYear
			AND CSID.SALES_ORDER_ID = OD.intSalesOrderId
			AND CSIH.STATUS = 1),0),$deci)											AS INVOICE_AMOUNT ,
			trn_sampleinfomations.intBrand,
		  	(SELECT
				GROUP_CONCAT(DISTINCT(mst_plant.strPlantName)) as plants
				FROM
				ware_fabricdispatchheader as FDH
				INNER JOIN ware_fabricdispatchdetails AS FDD ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
				INNER JOIN mst_locations ON FDH.intCompanyId = mst_locations.intId
				INNER JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId 
				WHERE FDH.intOrderNo = OH.intOrderNo
			   AND FDH.intOrderYear = OH.intOrderYear
			   AND FDD.intSalesOrderId = OD.intSalesOrderId
			   AND FDH.intStatus = 1 ) AS DISP_PLANTS 
			FROM
			trn_orderheader OH
			INNER JOIN trn_orderdetails OD ON OD.intOrderNo = OH.intOrderNo AND OH.intOrderYear = OD.intOrderYear
			INNER JOIN trn_sampleinfomations ON OD.intSampleNo = trn_sampleinfomations.intSampleNo AND OD.intSampleYear = trn_sampleinfomations.intSampleYear AND OD.intRevisionNo = trn_sampleinfomations.intRevisionNo
			INNER JOIN mst_locations L ON L.intId = OH.intLocationId 
			INNER JOIN mst_plant P ON P.intPlantId = L.intPlant 
			LEFT JOIN mst_customer_locations_header CL ON CL.intId = OH.intCustomerLocation
			WHERE 
			P.MAIN_CLUSTER_ID = '$clusterId'	
			AND OH.intStatus = 1 
			$para
			GROUP BY OH.intOrderNo,OH.intOrderYear,OD.intSalesOrderId
			HAVING ORDER_QTY > DISPATCH_QTY AND (DISPATCH_DATE IS NOT NULL AND DATEDIFF(DATE(NOW()),DISPATCH_DATE) >= 14)";
	return $db->RunQuery($sql);
}
?>
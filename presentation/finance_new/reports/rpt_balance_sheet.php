<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
ini_set('max_execution_time',60000);

include_once ("class/masterData/companies/cls_companies_get.php");


$obj_company_get		= new cls_companies_get($db);

$gross_profit_loss		= 0;
$companyId 				= $_SESSION["headCompanyId"];
$locationId				= $_SESSION["CompanyID"];
$target_currencyId		= $_REQUEST["CurrencyId"];
$dateFrom				= $_REQUEST["DateFrom"];
$dateTo					= $_REQUEST["DateTo"];
$before_array			= json_decode($_REQUEST["Before_Array"],true);
$after_array			= json_decode($_REQUEST["After_Array"],true);
$para					= "&CurrencyId=$target_currencyId&DateFrom=$dateFrom&DateTo=$dateTo&Before_Array=".urlencode($_REQUEST["Before_Array"])."&After_Array=".urlencode($_REQUEST["After_Array"]);
$bgColor_pre			= "#FCF2F3";
$bgColor_after			= "#EFFFF3";
$company_array			= $obj_company_get->GetCompanyReportHeader($locationId);

$blnShNoteReportId		= 929;

 
?>
<head>
<title>Balance Sheet</title>
</head>

<body>
<table width="900" border="0" align="center">
  <tr>
    <td align="center"><?php include 'reportHeader.php'?></td>
  </tr>
  <tr>
    <td class="reportHeader" align="center">Balance Sheet [<?php echo GetCurrencyCode($target_currencyId);?>]</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" class="rptBordered">
        <thead>
          <tr>
            <th width="20">Finance Type</th>
            <th width="500">Finance Sub Type</th>
            <?php foreach($before_array as $before_loop){?>
            <th width="10"><?php echo $before_loop["DATE_FROM"]."<br/>".$before_loop["DATE_TO"]?></th>
            <?php } ?>
            <th width="10"><?php echo $dateFrom."<br/>".$dateTo?></th>
            <?php foreach($after_array as $after_loop){?>
            <th width="10" ><?php echo $after_loop["DATE_FROM"]."<br/>".$after_loop["DATE_TO"]?></th>
            <?php } ?>
          </tr>
        </thead>
        <tbody>
          <!--BEGIN - TOTAL NON CURRENT ASSETS { -->
          <?php $result = GrossProfit_part01('1');
		while($row = mysqli_fetch_array($result))
		{
?>
          <?php if(!isset($main_type) || $main_type!=$row["MAIN_TYPE_NAME"]){
				$main_type = $row["MAIN_TYPE_NAME"];
		?>
          <tr bgcolor="#ECECFF">
            <td><?php echo $row["FINANCE_TYPE_NAME"]?></td>
            <td><?php echo $row["MAIN_TYPE_NAME"]?></td>
            <?php foreach($before_array as $before_loop){?>
            <td >&nbsp;</td>
            <?php } ?>
            <td>&nbsp;</td>
            <?php foreach($after_array as $after_loop){?>
            <td >&nbsp;</td>
            <?php } ?>
          </tr>
          <?php } ?>
          <tr>
            <td style="text-align:center"><?php echo $row["SUB_TYPE_CODE"]?></td>
            <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;<a target="rpt_balance_sheet_note.php" href="?q=<?php echo $blnShNoteReportId.$para?>#<?php echo $row["SUB_TYPE_NAME"]?>"><?php echo $row["SUB_TYPE_NAME"]?></a></td>
            <?php 
			$loop = 0; 
			foreach($before_array as $before_loop)
			{
				$periodAmount = GetOtherAmount($before_loop["DATE_FROM"],$before_loop["DATE_TO"],$row["SUB_TYPE_ID"]);
				$total_non_current_assets1[$loop]	+= round($periodAmount,2);
				$total_assets1[$loop]				+= round($periodAmount,2);
				$loop++;
			?>
            <td style="text-align:right;background-color:<?php echo $bgColor_pre?>"><?php echo number_format($periodAmount,2) ?></td>
            <?php 
			} 
			?>
            <td style="text-align:right"><?php echo number_format($row["AMOUNT"],2)?></td>
            <?php 
			$loop = 0; 
			foreach($after_array as $after_loop)
			{
				$periodAmount = GetOtherAmount($after_loop["DATE_FROM"],$after_loop["DATE_TO"],$row["SUB_TYPE_ID"]);
				$total_non_current_assets2[$loop]	+= round($periodAmount,2);
				$total_assets2[$loop]				+= round($periodAmount,2);
				$loop++;
			?>
            <td style="text-align:right;background-color:<?php echo $bgColor_after?>"><?php echo number_format($periodAmount,2) ?></td>
            <?php 
			} 
			?>
          </tr>
          <?php
		  $total_non_current_assets += round($row["AMOUNT"],2);
		}
?>
          <tr style="font-weight:bold">
            <td>&nbsp;</td>
            <td>TOTAL NON CURRENT ASSETS</td>
            <?php $loop = 0; foreach($before_array as $before_loop){?>
            <td style="text-align:right;border-top-style:groove;background-color:<?php echo $bgColor_pre?>"><?php echo number_format($total_non_current_assets1[$loop++],2)?></td>
            <?php } ?>
            <td style="text-align:right;border-top-style:groove;"><?php echo number_format($total_non_current_assets,2)?></td>
            <?php $loop = 0; foreach($after_array as $after_loop){?>
            <td style="text-align:right;border-top-style:groove;background-color:<?php echo $bgColor_after?>"><?php echo number_format($total_non_current_assets2[$loop++],2)?></td>
            <?php } ?>
          </tr>
          
          <!--END - TOTAL NON CURRENT ASSETS } --> 
          
          <!--BEGIN - TOTAL CURRENT ASSETS { -->
          <?php $result = GrossProfit_part01('2');
		while($row = mysqli_fetch_array($result))
		{
?>
          <?php if(!isset($main_type) || $main_type!=$row["MAIN_TYPE_NAME"]){
				$main_type = $row["MAIN_TYPE_NAME"];
		?>
          <tr bgcolor="#ECECFF">
            <td><?php echo $row["FINANCE_TYPE_NAME"]?></td>
            <td><?php echo $row["MAIN_TYPE_NAME"]?></td>
            <?php foreach($before_array as $before_loop){?>
            <td >&nbsp;</td>
            <?php } ?>
            <td>&nbsp;</td>
            <?php foreach($after_array as $after_loop){?>
            <td >&nbsp;</td>
            <?php } ?>
          </tr>
          <?php } ?>
          <tr>
            <td style="text-align:center"><?php echo $row["SUB_TYPE_CODE"]?></td>
            <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;<a target="rpt_balance_sheet_note.php" href="?q=<?php echo $blnShNoteReportId.$para?>#<?php echo $row["SUB_TYPE_NAME"]?>"><?php echo $row["SUB_TYPE_NAME"]?></a></td>
            <?php 
			$loop = 0; 
			foreach($before_array as $before_loop)
			{
				$periodAmount = GetOtherAmount($before_loop["DATE_FROM"],$before_loop["DATE_TO"],$row["SUB_TYPE_ID"]);
				$total_current_assets1[$loop]	+= round($periodAmount,2);
				$total_assets1[$loop]			+= round($periodAmount,2);
				$loop++;
			?>
            <td style="text-align:right;background-color:<?php echo $bgColor_pre?>"><?php echo number_format($periodAmount,2) ?></td>
            <?php 
			} 
			?>
            <td style="text-align:right"><?php echo number_format($row["AMOUNT"],2)?></td>
            <?php 
			$loop = 0; 
			foreach($after_array as $after_loop)
			{
				$periodAmount = GetOtherAmount($after_loop["DATE_FROM"],$after_loop["DATE_TO"],$row["SUB_TYPE_ID"]);
				$total_current_assets2[$loop]	+= round($periodAmount,2);
				$total_assets2[$loop]			+= round($periodAmount,2);
				$loop++;
			?>
            <td style="text-align:right;background-color:<?php echo $bgColor_after?>"><?php echo number_format($periodAmount,2) ?></td>
            <?php 
			} 
			?>
          </tr>
          <?php
		  $total_current_assets += round($row["AMOUNT"],2);
		}
		
?>
          <!--END - TOTAL CURRENT ASSETS } -->
          
          <tr style="font-weight:bold">
            <td>&nbsp;</td>
            <td>TOTAL CURRENT ASSETS</td>
            <?php $loop = 0; foreach($before_array as $before_loop){?>
            <td style="text-align:right;border-top-style:groove;background-color:<?php echo $bgColor_pre?>"><?php echo number_format($total_current_assets1[$loop++],2)?></td>
            <?php } ?>
            <td style="text-align:right;border-top-style:groove;"><?php echo number_format($total_current_assets,2)?></td>
            <?php $loop = 0; foreach($after_array as $after_loop){?>
            <td style="text-align:right;border-top-style:groove;background-color:<?php echo $bgColor_after?>"><?php echo number_format($total_current_assets2[$loop++],2)?></td>
            <?php } ?>
          </tr>
          <?php
		  $total_assets	= round($total_current_assets,2)  + round($total_non_current_assets,2);
		  ?>
          <tr style="font-weight:bold">
            <td>&nbsp;</td>
            <td>TOTAL ASSETS</td>
            <?php $loop = 0; foreach($before_array as $before_loop){?>
            <td style="text-align:right;border-top-style:groove;background-color:<?php echo $bgColor_pre?>"><?php echo number_format($total_assets1[$loop++],2)?></td>
            <?php } ?>
            <td style="text-align:right;border-top-style:groove;"><?php echo number_format($total_assets,2)?></td>
            <?php $loop = 0; foreach($after_array as $after_loop){?>
            <td style="text-align:right;border-top-style:groove;background-color:<?php echo $bgColor_after?>"><?php echo number_format($total_assets2[$loop++],2)?></td>
            <?php } ?>
          </tr>
          
          <!--BEGIN - TOTAL NON CURRENT LIABILITIES { -->
          <?php $result = GrossProfit_part01('3');
		while($row = mysqli_fetch_array($result))
		{
?>
          <?php if(!isset($main_type) || $main_type!=$row["MAIN_TYPE_NAME"]){
				$main_type = $row["MAIN_TYPE_NAME"];
		?>
          <tr bgcolor="#ECECFF">
            <td><?php echo $row["FINANCE_TYPE_NAME"]?></td>
            <td><?php echo $row["MAIN_TYPE_NAME"]?></td>
            <?php foreach($before_array as $before_loop){?>
            <td >&nbsp;</td>
            <?php } ?>
            <td>&nbsp;</td>
            <?php foreach($after_array as $after_loop){?>
            <td >&nbsp;</td>
            <?php } ?>
          </tr>
          <?php } ?>
          <tr>
            <td style="text-align:center"><?php echo $row["SUB_TYPE_CODE"]?></td>
            <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;<a target="rpt_balance_sheet_note.php" href="?q=<?php echo $blnShNoteReportId.$para?>#<?php echo $row["SUB_TYPE_NAME"]?>"><?php echo $row["SUB_TYPE_NAME"]?></a></td>
            <?php 
			$loop = 0; 
			foreach($before_array as $before_loop)
			{
				$periodAmount = GetOtherAmount($before_loop["DATE_FROM"],$before_loop["DATE_TO"],$row["SUB_TYPE_ID"]);
				$total_non_current_liabilities1[$loop]	+= round($periodAmount,2);
				$total_liabilities1[$loop]				+= round($periodAmount,2);
				$loop++;
			?>
            <td style="text-align:right;background-color:<?php echo $bgColor_pre?>"><?php echo number_format($periodAmount,2) ?></td>
            <?php 
			} 
			?>
            <td style="text-align:right"><?php echo number_format($row["AMOUNT"],2)?></td>
            <?php 
			$loop = 0; 
			foreach($after_array as $after_loop)
			{
				$periodAmount = GetOtherAmount($after_loop["DATE_FROM"],$after_loop["DATE_TO"],$row["SUB_TYPE_ID"]);
				$total_non_current_liabilities2[$loop]	+= round($periodAmount,2);
				$total_liabilities2[$loop]				+= round($periodAmount,2);
				$loop++;
			?>
            <td style="text-align:right;background-color:<?php echo $bgColor_after?>"><?php echo number_format($periodAmount,2) ?></td>
            <?php 
			} 
			?>
          </tr>
          <?php
		  $total_non_current_liabilities += round($row["AMOUNT"],2);
		}
?>
          <tr style="font-weight:bold">
            <td>&nbsp;</td>
            <td>TOTAL NON CURRENT LIABILITIES</td>
            <?php $loop = 0; foreach($before_array as $before_loop){?>
            <td style="text-align:right;border-top-style:groove;background-color:<?php echo $bgColor_pre?>"><?php echo number_format($total_non_current_liabilities1[$loop++],2)?></td>
            <?php } ?>
            <td style="text-align:right;border-top-style:groove;"><?php echo number_format($total_non_current_liabilities,2)?></td>
            <?php $loop = 0; foreach($after_array as $after_loop){?>
            <td style="text-align:right;border-top-style:groove;background-color:<?php echo $bgColor_after?>"><?php echo number_format($total_non_current_liabilities2[$loop++],2)?></td>
            <?php } ?>
          </tr>
          <!--END   - TOTAL NON CURRENT LIABILITIES } --> 
          
          <!--BEGIN - TOTAL CURRENT LIABILITIES { -->
          <?php $result = GrossProfit_part01('4');
		while($row = mysqli_fetch_array($result))
		{
?>
          <?php if(!isset($main_type) || $main_type!=$row["MAIN_TYPE_NAME"]){
				$main_type = $row["MAIN_TYPE_NAME"];
		?>
          <tr bgcolor="#ECECFF">
            <td><?php echo $row["FINANCE_TYPE_NAME"]?></td>
            <td><?php echo $row["MAIN_TYPE_NAME"]?></td>
            <?php foreach($before_array as $before_loop){?>
            <td>&nbsp;</td>
            <?php } ?>
            <td>&nbsp;</td>
            <?php foreach($after_array as $after_loop){?>
            <td>&nbsp;</td>
            <?php } ?>
          </tr>
          <?php } ?>
          <tr>
            <td style="text-align:center"><?php echo $row["SUB_TYPE_CODE"]?></td>
            <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;<a target="rpt_balance_sheet_note.php" href="?q=<?php echo $blnShNoteReportId.$para?>#<?php echo $row["SUB_TYPE_NAME"]?>"><?php echo $row["SUB_TYPE_NAME"]?></a></td>
            <?php 
			$loop = 0; 
			foreach($before_array as $before_loop)
			{
				$periodAmount = GetOtherAmount($before_loop["DATE_FROM"],$before_loop["DATE_TO"],$row["SUB_TYPE_ID"]);
				$total_current_liabilities1[$loop]	+= round($periodAmount,2);
				$total_liabilities1[$loop]			+= round($periodAmount,2);
				$loop++;
			?>
            <td style="text-align:right;background-color:<?php echo $bgColor_pre?>"><?php echo number_format($periodAmount,2) ?></td>
            <?php 
			} 
			?>
            <td style="text-align:right"><?php echo number_format($row["AMOUNT"],2)?></td>
            <?php 
			$loop = 0; 
			foreach($after_array as $after_loop)
			{
				$periodAmount = GetOtherAmount($after_loop["DATE_FROM"],$after_loop["DATE_TO"],$row["SUB_TYPE_ID"]);
				$total_current_liabilities2[$loop]	+= round($periodAmount,2);
				$total_liabilities2[$loop]			+= round($periodAmount,2);
				$loop++;
			?>
            <td style="text-align:right;background-color:<?php echo $bgColor_after?>"><?php echo number_format($periodAmount,2) ?></td>
            <?php 
			} 
			?>
          </tr>
          <?php
		  $total_current_liabilities += round($row["AMOUNT"],2);
		}
?>
          <tr style="font-weight:bold">
            <td>&nbsp;</td>
            <td>TOTAL CURRENT LIABILITIES</td>
            <?php $loop = 0; foreach($before_array as $before_loop){?>
            <td style="text-align:right;border-top-style:groove;background-color:<?php echo $bgColor_pre?>"><?php echo number_format($total_current_liabilities1[$loop++],2)?></td>
            <?php } ?>
            <td style="text-align:right;border-top-style:groove;"><?php echo number_format($total_current_liabilities,2)?></td>
            <?php $loop = 0; foreach($after_array as $after_loop){?>
            <td style="text-align:right;border-top-style:groove;background-color:<?php echo $bgColor_after?>"><?php echo number_format($total_current_liabilities2[$loop++],2)?></td>
            <?php } ?>
          </tr>
          <!--END   - TOTAL CURRENT LIABILITIES } -->
          <?php
		  $total_liabilities = round($total_non_current_liabilities,2) + round($total_current_liabilities,2);
		  ?>
          <tr style="font-weight:bold">
            <td>&nbsp;</td>
            <td>TOTAL LIABILITIES</td>
            <?php $loop = 0; foreach($before_array as $before_loop){?>
            <td style="text-align:right;border-top-style:groove;background-color:<?php echo $bgColor_pre?>"><?php echo number_format($total_liabilities1[$loop++],2)?></td>
            <?php } ?>
            <td style="text-align:right;border-top-style:groove;"><?php echo number_format($total_liabilities,2)?></td>
            <?php $loop = 0; foreach($after_array as $after_loop){?>
            <td style="text-align:right;border-top-style:groove;background-color:<?php echo $bgColor_after?>"><?php echo number_format($total_liabilities2[$loop++],2)?></td>
            <?php } ?>
          </tr>
          <?php
		  $net_assets = round($total_assets,2) - round($total_liabilities,2);
		  ?>
          <tr style="font-weight:bold">
            <td>&nbsp;</td>
            <td>NET ASSETS</td>
            <?php $loop = 0; foreach($before_array as $before_loop){?>
            <td style="text-align:right;border-top-style:groove;border-bottom-style:double;background-color:<?php echo $bgColor_pre?>"><?php echo number_format($total_assets1[$loop] - $total_liabilities1[$loop],2)?></td>
            <?php $loop++;} ?>
            <td style="text-align:right;border-top-style:groove;border-bottom-style:double"><?php echo number_format($net_assets,2)?></td>
            <?php $loop = 0; foreach($after_array as $after_loop){?>
            <td style="text-align:right;border-top-style:groove;border-bottom-style:double;background-color:<?php echo $bgColor_after?>"><?php echo number_format($total_assets2[$loop] - $total_liabilities2[$loop],2)?></td>
            <?php $loop++;} ?>
          </tr>
          
          <!--BEGIN - TOTAL EQUITY { -->
          <?php $result = GrossProfit_part01('5');
		while($row = mysqli_fetch_array($result))
		{			
			if($row["SUB_TYPE_NAME"]=='RETAINED EARNINGS')
				$amount = GetReturnEarnings($dateFrom,$dateTo);
			else
				$amount = round($row["AMOUNT"],2);
?>
          <?php if(!isset($main_type) || $main_type!=$row["MAIN_TYPE_NAME"]){
				$main_type = $row["MAIN_TYPE_NAME"];
		?>
          <tr bgcolor="#ECECFF">
            <td><?php echo $row["FINANCE_TYPE_NAME"]?></td>
            <td><?php echo $row["MAIN_TYPE_NAME"]?></td>
            <?php foreach($before_array as $before_loop){?>
            <td>&nbsp;</td>
            <?php } ?>
            <td>&nbsp;</td>
            <?php foreach($after_array as $after_loop){?>
            <td>&nbsp;</td>
            <?php } ?>
          </tr>
          <?php } ?>
          <tr>
            <td style="text-align:center"><?php echo $row["SUB_TYPE_CODE"]?></td>
            <td nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;<a target="rpt_balance_sheet_note.php" href="?q=<?php echo $blnShNoteReportId.$para?>#<?php echo $row["SUB_TYPE_NAME"]?>"><?php echo $row["SUB_TYPE_NAME"]?></a></td>
            <?php 
			$loop = 0; 
			foreach($before_array as $before_loop)
			{
				if($row["SUB_TYPE_NAME"]=='RETAINED EARNINGS')
					$periodAmount = GetReturnEarnings($before_loop["DATE_FROM"],$before_loop["DATE_TO"]);
				else
					$periodAmount = GetOtherAmount($before_loop["DATE_FROM"],$before_loop["DATE_TO"],$row["SUB_TYPE_ID"]);				
				
				$total_equity1[$loop++]	+= round($periodAmount,2);
			?>
            <td style="text-align:right;background-color:<?php echo $bgColor_pre?>"><?php echo number_format($periodAmount,2) ?></td>
            <?php 
			} 
			?>
            <td style="text-align:right"><?php echo number_format($amount,2)?></td>
            <?php 
			$loop = 0; 
			foreach($after_array as $after_loop)
			{
				if($row["SUB_TYPE_NAME"]=='RETAINED EARNINGS')
					$periodAmount = GetReturnEarnings($after_loop["DATE_FROM"],$after_loop["DATE_TO"]);
				else
					$periodAmount = GetOtherAmount($after_loop["DATE_FROM"],$after_loop["DATE_TO"],$row["SUB_TYPE_ID"]);				
				
				$total_equity2[$loop++]	+= round($periodAmount,2);
			?>
            <td style="text-align:right;background-color:<?php echo $bgColor_after?>"><?php echo number_format($periodAmount,2) ?></td>
            <?php 
			} 
			?>
          </tr>
          <?php
		  $total_equity += round($amount,2);
		}
?>
          <tr style="font-weight:bold">
            <td>&nbsp;</td>
            <td>TOTAL EQUITY</td>
            <?php $loop = 0; foreach($before_array as $before_loop){?>
             <td style="text-align:right;border-top-style:groove;border-bottom-style:double;background-color:<?php echo $bgColor_pre?>"><?php echo number_format($total_equity1[$loop++],2)?></td>
            <?php } ?>
            <td style="text-align:right;border-top-style:groove;border-bottom-style:double;"><?php echo number_format($total_equity,2)?></td>
            <?php $loop = 0; foreach($after_array as $after_loop){?>
             <td style="text-align:right;border-top-style:groove;border-bottom-style:double;background-color:<?php echo $bgColor_after?>"><?php echo number_format($total_equity2[$loop++],2)?></td>
            <?php } ?>
          </tr>
          <!--END   - TOTAL EQUITY } -->
      </table></td>
  </tr>
</table>
</body>
</html>
<?php
function GrossProfit_part01($para)
{
	global $db;
	global $companyId;
	global $target_currencyId;
	global $dateFrom;
	global $dateTo;
	global $company_array;

	//if($target_currencyId!=$company_array["BASE_CURRENCY_ID"])
		//$where_sql	= "AND (FT.DOCUMENT_TYPE NOT IN ('GAIN_LOSS') OR FT.DOCUMENT_TYPE IS NULL) ";

	$sql = "SELECT 
				FINANCE_TYPE_NAME,
				MAIN_TYPE_NAME,
				SUB_TYPE_ID,
				CONCAT(FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE) AS SUB_TYPE_CODE,
				SUB_TYPE_NAME,
				
				SUM(IF(TRANSACTION_ADD_TYPE='D',DEBIT-CREDIT,CREDIT-DEBIT))AS AMOUNT
				FROM 
				(SELECT 
			  		TRANSACTION_ADD_TYPE,
					FINANCE_TYPE_ID,
					FINANCE_TYPE_CODE,
			  		FINANCE_TYPE_NAME,
					MAIN_TYPE_ID,
					MAIN_TYPE_CODE,
			  		MAIN_TYPE_NAME,
			  		SUB_TYPE_ID,
			  		SUB_TYPE_CODE,
			  		SUB_TYPE_NAME,
			  		DEBIT,
			  		CREDIT
				FROM 
					(SELECT
						T.TRANSACTION_ADD_TYPE,
						T.FINANCE_TYPE_ID,
						T.FINANCE_TYPE_CODE,
						T.FINANCE_TYPE_NAME,
						MT.MAIN_TYPE_ID,
						MT.MAIN_TYPE_CODE,
						MT.MAIN_TYPE_NAME,
						ST.SUB_TYPE_ID,
						ST.SUB_TYPE_CODE,
						ST.SUB_TYPE_NAME,
						COA.CHART_OF_ACCOUNT_ID,
						COA.CHART_OF_ACCOUNT_CODE,
						COA.CHART_OF_ACCOUNT_NAME,
						
						(SELECT SUM(IF(TRANSACTION_TYPE='D',COALESCE(FT.AMOUNT,0)/((SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER 
						WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1))*((SELECT SUB_ER.dblExcAvgRate 
						FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID 
						AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1)),0)) FROM finance_transaction FT  
						WHERE COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID AND FT.COMPANY_ID = $companyId AND DATE(FT.LAST_MODIFIED_DATE) <= '$dateTo') AS DEBIT,

						(SELECT SUM(IF(TRANSACTION_TYPE='C',COALESCE(FT.AMOUNT,0)/((SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER 
						WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1))*((SELECT SUB_ER.dblExcAvgRate 
						FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID 
						AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1)),0)) FROM finance_transaction FT  
						WHERE COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID AND FT.COMPANY_ID = $companyId AND DATE(FT.LAST_MODIFIED_DATE) <= '$dateTo') AS CREDIT 						
					
						/*ROUND(COALESCE(FT.AMOUNT / (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1) * (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1),0),2) AS AMOUNT*/
					FROM finance_mst_account_type T
					INNER JOIN finance_mst_account_main_type MT ON T.FINANCE_TYPE_ID = MT.FINANCE_TYPE_ID
					INNER JOIN finance_mst_account_sub_type ST ON MT.MAIN_TYPE_ID = ST.MAIN_TYPE_ID
					INNER JOIN finance_mst_chartofaccount COA ON ST.SUB_TYPE_ID = COA.SUB_TYPE_ID
					/*LEFT JOIN finance_transaction FT ON COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID*/
					WHERE MT.MAIN_TYPE_ID IN ($para)
					/*AND (FT.COMPANY_ID = $companyId	OR FT.COMPANY_ID IS NULL)
					AND (DATE(FT.LAST_MODIFIED_DATE) <= '$dateTo' OR DATE(FT.LAST_MODIFIED_DATE) IS NULL)*/
					$where_sql					
					)AS SUB_1) AS SUB_2
					GROUP BY FINANCE_TYPE_ID,MAIN_TYPE_ID,SUB_TYPE_ID
					ORDER BY FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE";
			return $db->RunQuery($sql);
}

function GetReturnEarnings($dateFrom,$dateTo)
{
	$gross_profit_loss1 = round(GetReturnEarningsAmount('6,7',$dateFrom,$dateTo),2);
	$gross_profit_loss2 = round(GetReturnEarningsAmount('8,9',$dateFrom,$dateTo),2);
	$gross_profit_loss3 = round(GrossProfit_part02('6,7',$dateFrom,$dateTo),2);
	$gross_profit_loss4 = round(GrossProfit_part02('8,9',$dateFrom,$dateTo),2);
	$gross_profit_loss5 = round(GetOtherAmount($dateFrom,$dateTo,32),2);
	return ($gross_profit_loss1 - $gross_profit_loss2) + ($gross_profit_loss3 - $gross_profit_loss4) + $gross_profit_loss5;
}

function GetReturnEarningsAmount($para,$dateFrom,$dateTo)
{
	global $db;
	global $companyId;
	global $target_currencyId;

	$sql = "SELECT 
			FINANCE_TYPE_NAME,
			MAIN_TYPE_NAME,
			CONCAT(FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE) AS SUB_TYPE_CODE,
			SUB_TYPE_NAME,
			
			SUM(IF(TRANSACTION_ADD_TYPE='D',DEBIT-CREDIT,CREDIT-DEBIT))AS AMOUNT
			FROM 
			(SELECT 
				TRANSACTION_ADD_TYPE,
				FINANCE_TYPE_ID,
				FINANCE_TYPE_CODE,
				FINANCE_TYPE_NAME,
				MAIN_TYPE_ID,
				MAIN_TYPE_CODE,
				MAIN_TYPE_NAME,
				SUB_TYPE_ID,
				SUB_TYPE_CODE,
				SUB_TYPE_NAME,
				DEBIT,
				CREDIT
			FROM 
				(SELECT
					T.TRANSACTION_ADD_TYPE,
					T.FINANCE_TYPE_ID,
					T.FINANCE_TYPE_CODE,
					T.FINANCE_TYPE_NAME,
					MT.MAIN_TYPE_ID,
					MT.MAIN_TYPE_CODE,
					MT.MAIN_TYPE_NAME,
					ST.SUB_TYPE_ID,
					ST.SUB_TYPE_CODE,
					ST.SUB_TYPE_NAME,
					COA.CHART_OF_ACCOUNT_ID,
					COA.CHART_OF_ACCOUNT_CODE,
					COA.CHART_OF_ACCOUNT_NAME,
 					
					(SELECT SUM(IF(TRANSACTION_TYPE='D',COALESCE(FT.AMOUNT,0)/((SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER 
					WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1))*((SELECT SUB_ER.dblExcAvgRate 
					FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID 
					AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1)),0)) FROM finance_transaction FT  
					WHERE COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID AND FT.COMPANY_ID = $companyId AND DATE(FT.LAST_MODIFIED_DATE) <= '$dateTo') AS DEBIT,

					(SELECT SUM(IF(TRANSACTION_TYPE='C',COALESCE(FT.AMOUNT,0)/((SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER 
					WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1))*((SELECT SUB_ER.dblExcAvgRate 
					FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID 
					AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1)),0)) FROM finance_transaction FT  
					WHERE COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID AND FT.COMPANY_ID = $companyId AND DATE(FT.LAST_MODIFIED_DATE) <= '$dateTo') AS CREDIT 						
		
					/*
					ROUND(COALESCE(FT.AMOUNT / (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1) * (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1),0),2) AS AMOUNT
					*/
				FROM finance_mst_account_type T
				INNER JOIN finance_mst_account_main_type MT ON T.FINANCE_TYPE_ID = MT.FINANCE_TYPE_ID
				INNER JOIN finance_mst_account_sub_type ST ON MT.MAIN_TYPE_ID = ST.MAIN_TYPE_ID
				INNER JOIN finance_mst_chartofaccount COA ON ST.SUB_TYPE_ID = COA.SUB_TYPE_ID
				/*LEFT JOIN finance_transaction FT ON COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID*/
				WHERE MT.MAIN_TYPE_ID IN ($para)
					/*AND (FT.COMPANY_ID = $companyId	OR FT.COMPANY_ID IS NULL)
					AND (DATE(FT.LAST_MODIFIED_DATE) < '$dateFrom' OR DATE(FT.LAST_MODIFIED_DATE) IS NULL)*/ )
				AS SUB_1) AS SUB_2
				GROUP BY FINANCE_TYPE_ID,MAIN_TYPE_ID,SUB_TYPE_ID
				ORDER BY FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE";
				
 	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$gross_profit_loss += round($row["AMOUNT"],2);
	}
	return round($gross_profit_loss,2);
}

function GrossProfit_part02($para,$dateFrom,$dateTo)
{
	global $db;
	global $companyId;
	global $target_currencyId;

	$sql = "SELECT 
			FINANCE_TYPE_NAME,
			MAIN_TYPE_NAME,
			CONCAT(FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE) AS SUB_TYPE_CODE,
			SUB_TYPE_NAME,
			
			SUM(IF(TRANSACTION_ADD_TYPE='D',DEBIT-CREDIT,CREDIT-DEBIT))AS AMOUNT
			FROM 
			(SELECT 
				TRANSACTION_ADD_TYPE,
				FINANCE_TYPE_ID,
				FINANCE_TYPE_CODE,
				FINANCE_TYPE_NAME,
				MAIN_TYPE_ID,
				MAIN_TYPE_CODE,
				MAIN_TYPE_NAME,
				SUB_TYPE_ID,
				SUB_TYPE_CODE,
				SUB_TYPE_NAME,
				DEBIT,
				CREDIT
			FROM 
				(SELECT
					T.TRANSACTION_ADD_TYPE,
					T.FINANCE_TYPE_ID,
					T.FINANCE_TYPE_CODE,
					T.FINANCE_TYPE_NAME,
					MT.MAIN_TYPE_ID,
					MT.MAIN_TYPE_CODE,
					MT.MAIN_TYPE_NAME,
					ST.SUB_TYPE_ID,
					ST.SUB_TYPE_CODE,
					ST.SUB_TYPE_NAME,
					COA.CHART_OF_ACCOUNT_ID,
					COA.CHART_OF_ACCOUNT_CODE,
					COA.CHART_OF_ACCOUNT_NAME,
					/*FT.CURRENCY_ID,
					FT.TRANSACTION_TYPE,*/
					
					(SELECT SUM(IF(TRANSACTION_TYPE='D',COALESCE(FT.AMOUNT,0)/((SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER 
					WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1))*((SELECT SUB_ER.dblExcAvgRate 
					FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID 
					AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1)),0)) FROM finance_transaction FT  
					WHERE COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID AND FT.COMPANY_ID = $companyId AND DATE(FT.LAST_MODIFIED_DATE) BETWEEN '$dateFrom' AND '$dateTo') AS DEBIT,

					(SELECT SUM(IF(TRANSACTION_TYPE='C',COALESCE(FT.AMOUNT,0)/((SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER 
					WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1))*((SELECT SUB_ER.dblExcAvgRate 
					FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID 
					AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1)),0)) FROM finance_transaction FT  
					WHERE COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID AND FT.COMPANY_ID = $companyId AND DATE(FT.LAST_MODIFIED_DATE) BETWEEN '$dateFrom' AND '$dateTo') AS CREDIT 						
		
					/*
					ROUND(COALESCE(FT.AMOUNT / (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1) * (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1),0),2) AS AMOUNT
					*/
				FROM finance_mst_account_type T
				INNER JOIN finance_mst_account_main_type MT ON T.FINANCE_TYPE_ID = MT.FINANCE_TYPE_ID
				INNER JOIN finance_mst_account_sub_type ST ON MT.MAIN_TYPE_ID = ST.MAIN_TYPE_ID
				INNER JOIN finance_mst_chartofaccount COA ON ST.SUB_TYPE_ID = COA.SUB_TYPE_ID
				/*LEFT JOIN finance_transaction FT ON COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID*/
				WHERE MT.MAIN_TYPE_ID IN ($para)
				/*AND (FT.COMPANY_ID = $companyId	OR FT.COMPANY_ID IS NULL)
				AND (DATE(FT.LAST_MODIFIED_DATE) BETWEEN '$dateFrom' AND '$dateTo' OR DATE(FT.LAST_MODIFIED_DATE) IS NULL) */ )
				AS SUB_1) AS SUB_2
				GROUP BY FINANCE_TYPE_ID,MAIN_TYPE_ID,SUB_TYPE_ID
				ORDER BY FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE";
 	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$gross_profit_loss += round($row["AMOUNT"],2);
	}
	return round($gross_profit_loss,2);
}

function GetOtherAmount($dateFrom,$dateTo,$para)
{
	global $db;
	global $companyId;
	global $target_currencyId;
	
	$sql = "SELECT 
				FINANCE_TYPE_NAME,
				MAIN_TYPE_NAME,
				SUB_TYPE_ID,
				CONCAT(FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE) AS SUB_TYPE_CODE,
				SUB_TYPE_NAME,
				
				COALESCE(SUM(IF(TRANSACTION_ADD_TYPE='D',DEBIT-CREDIT,CREDIT-DEBIT)),0)AS AMOUNT
				FROM 
				(SELECT 
			  		TRANSACTION_ADD_TYPE,
					FINANCE_TYPE_ID,
					FINANCE_TYPE_CODE,
			  		FINANCE_TYPE_NAME,
					MAIN_TYPE_ID,
					MAIN_TYPE_CODE,
			  		MAIN_TYPE_NAME,
			  		SUB_TYPE_ID,
			  		SUB_TYPE_CODE,
			  		SUB_TYPE_NAME,
			  		DEBIT,
			  		CREDIT
				FROM 
					(SELECT
						T.TRANSACTION_ADD_TYPE,
						T.FINANCE_TYPE_ID,
						T.FINANCE_TYPE_CODE,
						T.FINANCE_TYPE_NAME,
						MT.MAIN_TYPE_ID,
						MT.MAIN_TYPE_CODE,
						MT.MAIN_TYPE_NAME,
						ST.SUB_TYPE_ID,
						ST.SUB_TYPE_CODE,
						ST.SUB_TYPE_NAME,
						COA.CHART_OF_ACCOUNT_ID,
						COA.CHART_OF_ACCOUNT_CODE,
						COA.CHART_OF_ACCOUNT_NAME,
						/*FT.CURRENCY_ID,
						FT.TRANSACTION_TYPE,*/
						
					(SELECT SUM(IF(TRANSACTION_TYPE='D',COALESCE(FT.AMOUNT,0)/((SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER 
					WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1))*((SELECT SUB_ER.dblExcAvgRate 
					FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID 
					AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1)),0)) FROM finance_transaction FT  
					WHERE COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID AND FT.COMPANY_ID = $companyId AND DATE(FT.LAST_MODIFIED_DATE) <= '$dateTo') AS DEBIT,

					(SELECT SUM(IF(TRANSACTION_TYPE='C',COALESCE(FT.AMOUNT,0)/((SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER 
					WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1))*((SELECT SUB_ER.dblExcAvgRate 
					FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID 
					AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1)),0)) FROM finance_transaction FT  
					WHERE COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID AND FT.COMPANY_ID = $companyId AND DATE(FT.LAST_MODIFIED_DATE) <= '$dateTo') AS CREDIT 						
						
						/*
						ROUND(COALESCE(FT.AMOUNT / (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1) * (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId LIMIT 1),0),2) AS AMOUNT
						*/
						
					FROM finance_mst_account_type T
					INNER JOIN finance_mst_account_main_type MT ON T.FINANCE_TYPE_ID = MT.FINANCE_TYPE_ID
					INNER JOIN finance_mst_account_sub_type ST ON MT.MAIN_TYPE_ID = ST.MAIN_TYPE_ID
					INNER JOIN finance_mst_chartofaccount COA ON ST.SUB_TYPE_ID = COA.SUB_TYPE_ID
					/*INNER JOIN finance_transaction FT ON COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID*/
					WHERE COA.SUB_TYPE_ID IN ($para)
					/*AND (FT.COMPANY_ID = $companyId	OR FT.COMPANY_ID IS NULL)
					AND DATE(FT.LAST_MODIFIED_DATE) <= '$dateTo' */ )
					AS SUB_1) AS SUB_2
					GROUP BY FINANCE_TYPE_ID,MAIN_TYPE_ID,SUB_TYPE_ID
					ORDER BY FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE";
 	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return round($row["AMOUNT"],2);
}

function GetCurrencyCode($target_currencyId)
{
	global $db;
	
	$sql = "SELECT strCode FROM mst_financecurrency WHERE intId = $target_currencyId";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	return $row["strCode"];
}
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId			= $_SESSION["headCompanyId"];
require_once("libraries/jqgrid2/inc/jqgrid_dist.php");

// {
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'CHART_OF_ACCOUNT_NAME'=>'FT.CHART_OF_ACCOUNT_ID',
				'TYPE_NAME'=>'FTT.TYPE_ID',
				'DOCUMENT_NO'=>'FT.DOCUMENT_NO',
				'DOCUMENT_YEAR'=>'FT.DOCUMENT_YEAR',
				'INVOICE_NO'=>'FT.INVOICE_NO',
				'INVOICE_YEAR'=>'FT.INVOICE_YEAR',
				'currency'=>'FT.CURRENCY_ID',
				'NAME'=>'FT.TRANSACTION_CATEGORY',
				'addBy'=>'SU.strUserName',
				'LAST_MODIFIED_DATE'=>'FT.LAST_MODIFIED_DATE'
				);
				
foreach($arr as $k=>$v)
{
	$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND DATE(FT.LAST_MODIFIED_DATE) = '".date('Y-m-d')."'";
	
// }


$sql = "SELECT SUB_1.* FROM
		(
		SELECT  FT.CHART_OF_ACCOUNT_ID,
		COA.CHART_OF_ACCOUNT_NAME,
		FT.DOCUMENT_NO,
		FT.DOCUMENT_YEAR,
		FT.DOCUMENT_TYPE,
		FTT.TYPE_ID,
		FTT.TYPE_NAME,
		FT.INVOICE_NO,
		FT.INVOICE_YEAR,
		FT.AMOUNT,
		FT.TRANSACTION_TYPE,
		FT.CURRENCY_ID,
		FC.strCode AS currency,
		FT.BANK_REFERENCE_NO,
		FT.TRANSACTION_CATEGORY_ID,
		FT.TRANSACTION_CATEGORY,
		IF(FT.TRANSACTION_CATEGORY='CU',(SELECT
		mst_customer.strName
		FROM mst_customer
		WHERE
		mst_customer.intId = FT.TRANSACTION_CATEGORY_ID),(SELECT
		mst_supplier.strName
		FROM mst_supplier
		WHERE
		mst_supplier.intId = FT.TRANSACTION_CATEGORY_ID)) AS NAME,
		IF(FT.TRANSACTION_TYPE='C',FT.AMOUNT,'') AS credit,
		IF(FT.TRANSACTION_TYPE='D',FT.AMOUNT,'') AS debit,
		SU.strUserName as addBy,
		FT.LAST_MODIFIED_DATE
		FROM finance_transaction FT
		INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=FT.CHART_OF_ACCOUNT_ID
		INNER JOIN finance_mst_transaction_type FTT ON FTT.DOCUMENT_TYPE=FT.DOCUMENT_TYPE AND 
		FTT.TRANSACTION_CATEGORY=FT.TRANSACTION_CATEGORY
		INNER JOIN sys_users SU ON SU.intUserId=FT.LAST_MODIFIED_BY
		LEFT JOIN mst_financecurrency FC ON FC.intId=FT.CURRENCY_ID
		WHERE FT.COMPANY_ID='$companyId'
		$where_string
		)  
		AS SUB_1 WHERE 1=1";

$jq 	= new jqgrid('',$db);	
$col 	= array();
$cols 	= array();

$col["title"] 			= "Chart Of Account";
$col["name"] 			= "CHART_OF_ACCOUNT_NAME";
$col["dbname"] 			= "CHART_OF_ACCOUNT_ID";
$col["width"] 			= "8";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 
$Account_lookup 		= $jq->get_dropdown_values("SELECT FCOA.CHART_OF_ACCOUNT_ID AS k,
							CONCAT(CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE),'-',
							FCOA.CHART_OF_ACCOUNT_NAME) AS v
							FROM finance_mst_chartofaccount FCOA
							INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
							INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
							INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
							INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID
							WHERE FCOAC.COMPANY_ID='$companyId' ORDER BY v");
$col["stype"]			= "select";
$str 					= ":All;".$Account_lookup;
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$col["editoptions"] 	= array("value"=> $Account_lookup); // with these values "key:value;key:value;key:value"
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Type";
$col["name"] 			= "TYPE_NAME";
$col["dbname"] 			= "TYPE_ID";
$col["width"] 			= "5";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$type_lookup 			= $jq->get_dropdown_values("SELECT DISTINCT TYPE_ID AS k, TYPE_NAME AS v FROM finance_mst_transaction_type ORDER BY TYPE_NAME");
$col["stype"]			= "select";
$str 					= ":All;".$type_lookup;
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$col["editoptions"] 	= array("value"=> $type_lookup); // with these values "key:value;key:value;key:value"
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Document No";
$col["name"] 			= "DOCUMENT_NO";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Document Year";
$col["name"] 			= "DOCUMENT_YEAR";
$col["width"] 			= "4";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Invoice No";
$col["name"] 			= "INVOICE_NO";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Invoice Year";
$col["name"] 			= "INVOICE_YEAR";
$col["width"] 			= "3";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Currency";
$col["name"] 			= "currency";
$col["dbname"] 			= "CURRENCY_ID";
$col["width"] 			= "3";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$currency_lookup 		= $jq->get_dropdown_values("SELECT DISTINCT intId AS k, strCode AS v FROM mst_financecurrency WHERE intStatus=1 order by strCode");
$col["stype"]			= "select";
$str 					= ":All;".$currency_lookup;
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$col["editoptions"] 	= array("value"=> $currency_lookup); // with these values "key:value;key:value;key:value"
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Credit";
$col["name"] 			= "credit";
$col["width"] 			= "3";
$col["align"] 			= "right";
$col["sortable"] 		= true; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Debit";
$col["name"] 			= "debit";
$col["width"] 			= "3";
$col["align"] 			= "right";
$col["sortable"] 		= true; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;



$col["title"] 			= "Name";
$col["name"] 			= "NAME";
$col["dbname"] 			= "TRANSACTION_CATEGORY";
$col["width"] 			= "7";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 
$col["stype"] 			= "select";
$str 					= ":All;CU:Customer;SU:Supplier" ;
$col["editoptions"] 	=  array("value"=> $str);				
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Add By";
$col["name"] 			= "addBy";
$col["width"] 			= "3";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Date";
$col["name"] 			= "LAST_MODIFIED_DATE";
$col["width"] 			= "4";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "General Ledger Log";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'LAST_MODIFIED_DATE'; // by default sort grid by this field
$grid["sortorder"] 		= "ASC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
echo $out;
<?php
session_start();

//BEGIN - SESSION {
$session_locationId	= $_SESSION["CompanyID"];
$session_companyId	= $_SESSION["headCompanyId"];
//END	- SESSION }

//BEGIN - INCLUDE FILES {
$backwardseperator 	= "../../../";
include_once ("{$backwardseperator}dataAccess/Connector.php");
include_once ("../../../libraries/fpdf/fpdf.php");
include_once ("../../../class/finance/reports/cls_rpt_paymentvoucher_get.php");
include_once ("../../../class/finance/reports/cls_rpt_paymentvoucher_set.php");
include_once ("../../../class/masterData/companies/cls_companies_get.php");
include_once ("../../../class/masterData/exchange_rate/cls_exchange_rate_get.php");
include_once ("../../../class/masterData/currency/cls_currency_get.php");

include_once ("../../../class/finance/cls_convert_amount_to_word.php");
//END	- INCLUDE FILES }

//BEGIN	- CREATE OBJECTS {
$obj_company_get			= new cls_companies_get($db);
$obj_exchange_rate_get		= new cls_exchange_rate_get($db);
$obj_currency_get			= new cls_currency_get($db);
$obj_payVoucher_get			= new Cls_Rpt_Payment_Voucher_Get($db);
$obj_payVoucher_set			= new Cls_Rpt_Payment_Voucher_Set($db);
$obj_convert_amount_to_word	= new Cls_Convert_Amount_To_Word($db);
//END	- CREATE OBJECTS }
//BEGIN - PARAMETERS {
//$periodArray = array();
$serialNo		= $_REQUEST["SerialNo"];
$serialYear		= $_REQUEST["SerialYear"];
$type			= $_REQUEST["Type"];
$border			= 0;
$font			= 'Times';
//END	- PARAMETERS }
class PDF extends FPDF
{
	function Header()
	{
		global $result_reportHeader;
		global $title;
		global $session_companyId;
		global $font;
		$h = 5;
		
		$this->Image('../../../images/screenline.jpg',8,9);
		$this->SetFont($font,'B',14);$this->Cell(0,$h,$result_reportHeader["COMPANY_NAME"],'0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,$result_reportHeader["LOCATION_ADDRESS"].' '.$result_reportHeader["LOCATION_STREET"].' '.$result_reportHeader["LOCATION_CITY"].' '.$result_reportHeader["COMPANY_COUNTRY"].'.','0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,"Tel : ".$result_reportHeader["LOCATION_PHONE"].'  '."Fax : ".$result_reportHeader["LOCATION_FAX"],'0',1,'C');
		$this->SetFont($font,'',10);$this->Cell(0,$h,"E-mail : ".$result_reportHeader["LOCATION_EMAIL"].'  '."Web : ".$result_reportHeader["LOCATION_WEB"],'0',1,'C');
		$this->SetFont($font,'B',12);$this->Cell(0,10,$title,'0',1,'C');		
	}
	
	function SetPageWaterMark($status,$printStatus)
	{
		//Put the watermark
		switch($status)
		{
			case 0:
				$msg	= "REJECTED VOUCHER";
				break;
			case 1:
				$msg	= ($printStatus==1?"DUPLICATE":"");
				break;
			case $status>1 :
				$msg	= "PENDING VOUCHER";
				break;
			case '-2':
				$msg	= "CANCELED VOUCHER";
				break;
			case '-1':
				$msg	= "REVISED VOUCHER";
				break;
			default:
				$msg	= "";
				break;
		}
		$this->SetFont('Times','B',50);
		$this->SetTextColor(255,192,203);
		$this->RotatedText(35,190,$msg,45);
		$this->SetTextColor(0,0,0);
	}
	
	function RotatedText($x, $y, $txt, $angle)
	{
		//Text rotated around its origin
		$this->Rotate($angle,$x,$y);
		$this->Text($x,$y,$txt);
		$this->Rotate(0);
	}
		
	
	
	function ReportHeader($voucherNo,$header_array)
	{
		global $border;
		$h = 5;
		$this->SetFont($font,'',11);$this->Cell(25,$h,'Voucher No',$border,0,'L');
		$this->SetFont($font,'',11);$this->Cell(4,$h,':',$border,0,'L');
		$this->SetFont($font,'',11);$this->Cell(95,$h,$voucherNo,$border,0,'L');
		$this->SetFont($font,'',11);$this->Cell(25,$h,'Date',$border,0,'L');
		$this->SetFont($font,'',11);$this->Cell(4,$h,':',$border,0,'L');
		$this->SetFont($font,'',11);$this->Cell(85,$h,$header_array["paymentDate"],$border,1,'L');
		
		$this->SetFont($font,'',11);$this->Cell(25,$h,'Cheque No',$border,0,'L');
		$this->SetFont($font,'',11);$this->Cell(4,$h,':',$border,0,'L');
		$this->SetFont($font,'',11);$this->Cell(95,$h,$header_array["CHEQUE_NO"],$border,0,'L');
	}
	
	function CreateTable($header_array,$detail_array,$rowCount,$fieldCount,$exRate_array,$baseCurr_array,$obj_convert_amount_to_word)
	{
		$count 			= 1;
		$booFirst		= true;
		$tot_value		= round($header_array["TOTAL_AMOUNT"]*$exRate_array["AVERAGE_RATE"],2);
		
		$this->Table_header1($header_array,$baseCurr_array);
		$this->Table_header($header_array,$exRate_array,$baseCurr_array,$tot_value);
		while($row=mysqli_fetch_array($detail_array))
		{	
			//print_r($row);

			$this->Table_Body($row,$count);	
			if($count == 30){
				$this->AddPage();
				$this->Table_header($header_array,$exRate_array,$baseCurr_array,$tot_value);
				$count = 0;	
				$booFirst = false;		
			}			
			$count++;			
		}
		$this->Table_Body_Validate($count);	
		$this->Table_footer($header_array,$tot_value,$obj_convert_amount_to_word,$baseCurr_array);
	}
	
	function Table_header1($header_array,$baseCurr_array)
	{
		$this->SetY(50);
		$h = 180;
		$this->SetFont('Times','B',10);
		$this->Cell(165,10,"Particulars",'1',0,'C');
		$this->Cell(35,10,"Amount [".$baseCurr_array["CODE"].']','1',1,'C');
		$this->Cell(25,10,"Account",'BTL',0,'L');
		$this->Cell(4,10,":",'BT',0,'L');
		$this->SetFont('Times','',10);$this->Cell(136,10,$header_array['supplier'],'TBR',0,'L');
		$this->Cell(35,10,"",'1',0,'C');
		$this->Ln();
	}
	
	function Table_header($header_array,$exRate_array,$baseCurr_array,$tot_value)
	{
		$this->SetY(70);
		$h = 10;
		$this->SetFont('Times','B',10);
		$this->Cell(50,$h,number_format($header_array["TOTAL_AMOUNT"],2).' '.$header_array['currency'].' @ '.$baseCurr_array["SYMBOL"].' '.$exRate_array["AVERAGE_RATE"],'1',0,'C');
		$this->Cell(25,$h,"Amount [".$header_array["currency"].']','1',0,'C');
		$this->Cell(20,$h,"Debit",'1',0,'C');
		$this->Cell(20,$h,"Credit",'1',0,'C');
		$this->Cell(25,$h,"GRN No",'1',0,'C');
		$this->Cell(25,$h,"PO No",'1',0,'C');
		$this->Cell(35,$h,number_format($tot_value,2),'1',0,'R');
		$this->Ln();
		
	}
	
	function Table_Body($row,$count)
	{	
		$h	= 5;
		$this->SetFont('Times','',8);
		$this->Cell(50,$h,$row["invoiceNo"],'1',0,'L');
		$this->SetFont('Times','',10);
		$this->Cell(25,$h,number_format($row["amonut"],2),'1',0,'R');
		$this->Cell(20,$h,number_format($row["DEBIT_AMOUNT"],2),'1',0,'R');
		$this->Cell(20,$h,number_format($row["CREDIT_AMOUNT"],2),'1',0,'R');
		$this->Cell(25,$h,$row["GRNNo"],'1',0,'C');
		$this->Cell(25,$h,$row["PONo"],'1',0,'C');
		$this->Cell(35,$h,"",'1',0,'C');
		$this->Ln();
	}
	
	function Table_Body_Validate($count)
	{
		$h1	= 0;
		for($i=$count;$i<=18;$i++)
		{
			$h1 += 5;
		}
		$this->SetFont('Times','',7);
		$this->Cell(165,$h1,"",'1',0,'C');
		$this->Cell(35,$h1,"",'1',0,'C');
		$this->Ln();
	}
	
	function Table_footer($header_array,$tot_value,$obj_convert_amount_to_word,$baseCurr_array)
	{
		$this->SetY($this->GetY());
		$h = 180;		
		$this->SetFont('Times','B',10);$this->Cell(25,10,"Through",'LBT',0,'L');
		$this->Cell(4,10,":",'TB',0,'L');
		$this->SetFont('Times','',10);$this->Cell(136,10,"",'BTR',0,'C');
		$this->Cell(35,10,"",'1',0,'C');
		$this->Ln();
		
		$this->Cell(4,10,"",'LBT',0,'L');
		$this->Cell(161,10,$header_array["CHART_OF_ACCOUNT_NAME"],'TB',0,'L');
		$this->Cell(35,10,"",'1',0,'C');
		$this->Ln();
		
		$this->SetFont('Times','B',10);$this->Cell(25,10,"Narration",'LBT',0,'L');
		$this->Cell(4,10,":",'TB',0,'L');
		$this->SetFont('Times','',10);$this->Cell(136,10,"",'BTR',0,'C');
		$this->Cell(35,10,"",'1',0,'C');
		$this->Ln();
		
		$this->SetFont('Times','',7);$this->Cell(4,10,"",'LBT',0,'L');
		$this->Cell(161,10,$header_array["REMARKS"],'TB',0,'L');
		$this->Cell(35,10,"",'1',0,'C');
		$this->Ln();
		
		$this->SetFont('Times','B',10);$this->Cell(35,10,"Amount (In Words)",'L',0,'L');
		$this->SetFont('Times','',10);$this->Cell(130,10,"",'0',0,'C');
		$this->Cell(35,10,"",'1',0,'C');
		$this->Ln();
		
		$this->SetFont('Times','',7);$this->Cell(165,10,$obj_convert_amount_to_word->Convert_Amount($tot_value,$baseCurr_array["CODE"]),'1',0,'L');
		$this->Cell(35,10,"",'1',0,'L');
		$this->Ln();
		
		$this->SetFont('Times','B',10);$this->Cell(165,10,'Total','1',0,'R');
		$this->Cell(35,10,number_format($tot_value,2),'1',0,'R');
		$this->Ln();
	}
	
	function Footer()
	{
		global $obj_company_get;
		global $session_companyId;
		global $header_array;
		
		$this->SetXY(140,-30);
		$this->SetFont('Times','',8);
		$this->Cell(50,5,'Checked By',0,0,'C');

		$this->SetXY(140,-33);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetXY(20,-30);
		$this->SetFont('Times','',8);
		$this->Cell(50,5,'Authorized By',0,0,'C');

		$this->SetXY(20,-33);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetXY(140,-15);
		$this->SetFont('Times','',8);
		$this->Cell(50,5,'Received By',0,0,'C');

		$this->SetXY(140,-18);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetXY(20,-15);
		$this->SetFont('Times','',8);
		$this->Cell(50,5,'Prepared By',0,0,'C');

		$this->SetXY(20,-18);
		$this->Cell(50,5,'..........................................',0,0,'C');
		
		$this->SetXY(20,-19);
		$this->Cell(50,5,$header_array["CREATED_BY_NAME"],0,0,'C');

		$this->SetY(-5);
		$this->SetFont('Times','I',8);
		$this->Cell(0,2,'Page '.$this->PageNo().'/{nb}',0,0,'R');
	}
}

$pdf 					= new PDF('P','mm','Letter');
$result_reportHeader	= $obj_company_get->GetCompanyReportHeader($session_locationId);
$header_array			= $obj_payVoucher_get->HeaderData_pdf($serialNo,$serialYear,$type,'RunQuery');
//print_r($header_array);

$detail_array			= $obj_payVoucher_get->DetailData_pdf($serialNo,$serialYear,$type,'RunQuery');
 $title					= "PAYMENT VOUCHER - ".$header_array['CHART_OF_ACCOUNT_NAME'];
//$voucherNo				= $header_array['voucherYear'].'/'.$header_array['voucherMonth'].'/'.$serialNo.'/'.$header_array['strCostCenterCode'];
$voucherNo				= $header_array['voucherNo'];
$exRate_array			= $obj_exchange_rate_get->GetAllValues($header_array['CURRENCY_ID'],$deci=2,$header_array['paymentDate'],$header_array['COMPANY_ID'],'RunQuery');
$baseCurr_array			= $obj_currency_get->GetAllValues($result_reportHeader["BASE_CURRENCY_ID"],'RunQuery');
$rowCount 				= mysqli_num_rows($detail_array);
$fieldCount 			= mysqli_num_fields($detail_array);

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetPageWaterMark($header_array["STATUS"],$header_array["PRINT_STATUS"]);
$pdf->ReportHeader($voucherNo,$header_array);
$pdf->CreateTable($header_array,$detail_array,$rowCount,$fieldCount,$exRate_array,$baseCurr_array,$obj_convert_amount_to_word);

$obj_payVoucher_set->UpdateStatus($serialNo,$serialYear,$type);

$pdf->Output('rpt_suspended_tax_invoice_pdf1.pdf','I');

function GetMainHeaderDetails()
{
	global $db;

	$sql = "SELECT 
			CU.strName				AS CUSTOMER_NAME,
			CU.strAddress			AS ADDRESS,
			CU.strCity				AS CITY,
			CO.strCountryName		AS COUNTRY_NAME,
			CU.strContactPerson		AS CONTACT_PERSON,
			CU.strVatNo				AS VAT_NO,
			CU.strSVatNo			AS SVAT_NO,
			FCIH.INVOICE_NO			AS INVOICE_NO,
			FCIH.INVOICED_DATE		AS INVOICED_DATE
			FROM 	
			finance_customer_invoice_header FCIH
			INNER JOIN mst_customer CU ON CU.intId = FCIH.CUSTOMER_ID
			LEFT JOIN mst_country CO ON CO.intCountryID = CU.intCountryId
			WHERE FCIH.SERIAL_NO 	= '100000'
			AND SERIAL_YEAR 		= '2013'";	
	$result = $db->RunQuery($sql);
	return mysqli_fetch_array($result);
}
?>
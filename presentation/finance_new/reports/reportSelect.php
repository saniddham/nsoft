<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId	  		= $_SESSION["CompanyID"];
$userId				= $_SESSION['userId'];

require_once "class/tables/mst_maincategory.php";				$mst_maincategory 			= new mst_maincategory($db);
require_once "class/tables/mst_subcategory.php";				$mst_subcategory			= new mst_subcategory($db);
require_once "class/tables/finance_mst_chartofaccount.php"; 	$finance_mst_chartofaccount	= new finance_mst_chartofaccount($db);
require_once "class/tables/mst_financecurrency.php"; 			$mst_financecurrency		= new mst_financecurrency($db);
require_once "class/tables/mst_companies.php";					$mst_companies				= new mst_companies($db);																								
require_once "class/tables/sys_main_clusters.php";				$sys_main_clusters				= new sys_main_clusters($db);																								
$mst_companies->set($sessions->getCompanyId());
?>

<title>Finance - Reports</title>

<!--<script type="text/javascript" src="presentation/finance_new/reports/reportSelect-js.js"></script>-->

<script type="text/javascript">
var pub_currDate	= '<?php echo date('Y-m-d')?>';
</script>
<div align="center">
  <div class="trans_layoutD">
    <div class="trans_text">Finance Reports</div>
    <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
      <tr id="GLReportHeader">
        <td><table align="left" width="100%" class="main_header_table" id="tblGLReportHeader" cellpadding="0" cellspacing="0">
            <tr>
              <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>General Ledger Reports.</u></td>
            </tr>
          </table></td>
      </tr>
      
      <!-- ----------GL Details ------------------------- -->
      <tr>
        <td><form id="frmGLReportDetail" name="frmGLReportDetail">
            <div id="divGLReportDetail" class="clsdivGLReportDetail" style="width:100%;display:none">
              <table align="left" width="100%" class="main_table" id="tblGLReportDetail" border="0" cellpadding="0" cellspacing="0">
                <thead>
                  <tr>
                    <td colspan="7" style="text-align:left">General Ledger Selection Criteria</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td width="21%" class="normalfnt" height="">Currency</td>
                    <td colspan="3" class="normalfnt"><select id="cboCurrency" name="cboCurrency" style="width:120px" class="validate[required]">
                        <?php 
										$sql="SELECT
												mst_financecurrency.intId,
												mst_financecurrency.strCode
												FROM mst_financecurrency
												WHERE
												mst_financecurrency.intStatus =  '1'
												ORDER BY
												mst_financecurrency.intId";
                                   		$result=$db->RunQuery($sql);
										while($row=mysqli_fetch_array($result))
										{
										echo "<option value=\"".$row["intId"]."\">".$row["strCode"]."</option>";
										}
									?>
                      </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Finance Type</td>
                    <td colspan="3" class="normalfnt"><select name="cboFinanceType" id="cboFinanceType" style="width:120px">
                        <option value=""></option>
                        <?php
                                    	$sql = "SELECT FINANCE_TYPE_ID,FINANCE_TYPE_NAME
                                    			FROM finance_mst_account_type
                                    			WHERE STATUS=1
                                    			ORDER BY FINANCE_TYPE_NAME";
                                    	$result = $db->RunQuery($sql);
										while($row=mysqli_fetch_array($result))
										{
											echo "<option value=\"".$row["FINANCE_TYPE_ID"]."\">".$row["FINANCE_TYPE_NAME"]."</option>";
										}
                                    ?>
                      </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Main Type</td>
                    <td colspan="3" class="normalfnt"><select name="cboMainType" id="cboMainType" style="width:250px">
                      </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Sub Type</td>
                    <td colspan="3" class="normalfnt"><select name="cboSubType" id="cboSubType" style="width:250px">
                      </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Chart Of Account</td>
                    <td colspan="3" class="normalfnt"><select name="cboChartOfAccount" id="cboChartOfAccount" style="width:250px">
                      </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Date</td>
                    <td width="21%" class="normalfnt"><input name="dtFromDate" type="text" value="<?php   echo date("Y-m-d"); ?>" class="txtbox" id="dtFromDate" style="width:90px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                    <td width="6%" class="normalfnt">To</td>
                    <td width="52%" class="normalfnt"><input name="dtToDate" type="text" value="<?php  echo date("Y-m-d"); ?>" class="txtbox" id="dtToDate" style="width:90px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                  </tr>
                  <tr>
                    <td colspan="4" class="normalfnt"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                        <tr>
                          <td width="100%" style="text-align:center"><a class="button white medium" id="butDetail" name="butDetail">Detail</a><a class="button white medium" id="butSummary" name="butSummary">Summary</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
                        </tr>
                      </table></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </form></td>
      </tr>
      
      <!--BEGIN - TRAIL BALANCE SELECTION CRITERIA-->
      <tr id="TrailBalanceHeader">
        <td><table align="left" width="100%" class="main_header_table" id="tblTrailBalanceHeader" cellpadding="0" cellspacing="0">
            <tr>
              <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Trail Balance.</u></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><form id="frmTrailBalanceHeader" name="frmTrailBalanceHeader">
            <div id="divTrailBalanceHeader" class="clsTrailBalanceHeader" style="width:100%;display:none">
              <table align="left" width="100%" class="main_table" id="tblTrailBalanceDetial" border="0" cellpadding="0" cellspacing="0">
                <thead>
                  <tr>
                    <td colspan="7" style="text-align:left">Trail Balance Selection Criteria</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td width="21%" class="normalfnt" height="">Currency</td>
                    <td colspan="3" class="normalfnt"><select id="cboCurrency" name="cboCurrency" style="width:120px" class="validate[required]">
                        <?php 
										$sql="SELECT
												mst_financecurrency.intId,
												mst_financecurrency.strCode
												FROM mst_financecurrency
												WHERE
												mst_financecurrency.intStatus =  '1'
												ORDER BY
												mst_financecurrency.intId";
                                   		$result=$db->RunQuery($sql);
										while($row=mysqli_fetch_array($result))
										{
										echo "<option value=\"".$row["intId"]."\">".$row["strCode"]."</option>";
										}
									?>
                      </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Finance Type</td>
                    <td colspan="3" class="normalfnt"><select name="cboFinanceType" id="cboFinanceType" style="width:120px">
                        <option value="">&nbsp;</option>
                        <?php
                                    	$sql = "SELECT FINANCE_TYPE_ID,FINANCE_TYPE_NAME
                                    			FROM finance_mst_account_type
                                    			WHERE STATUS=1
                                    			ORDER BY FINANCE_TYPE_NAME";
                                    	$result = $db->RunQuery($sql);
										while($row=mysqli_fetch_array($result))
										{
											echo "<option value=\"".$row["FINANCE_TYPE_ID"]."\">".$row["FINANCE_TYPE_NAME"]."</option>";
										}
                                    ?>
                      </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Main Type</td>
                    <td colspan="3" class="normalfnt"><select name="cboMainType" id="cboMainType" style="width:250px">
                        <option value="">&nbsp;</option>
                      </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Sub Type</td>
                    <td colspan="3" class="normalfnt"><select name="cboSubType" id="cboSubType" style="width:250px">
                        <option value="">&nbsp;</option>
                      </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Chart Of Account</td>
                    <td colspan="3" class="normalfnt"><select name="cboChartOfAccount" id="cboChartOfAccount" style="width:250px">
                        <option value="">&nbsp;</option>
                      </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Date</td>
                    <td width="21%" class="normalfnt"><input name="dtFromDateTrialBal" type="text" value="<?php   echo date("Y-m-d"); ?>" class="txtbox" id="dtFromDateTrialBal" style="width:90px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                    <td width="6%" class="normalfnt">To</td>
                    <td width="52%" class="normalfnt"><input name="dtToDateInComeTrialBal" type="text" value="<?php  echo date("Y-m-d"); ?>" class="txtbox" id="dtToDateTrialBal" style="width:90px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                  </tr>
                  <tr>
                    <td colspan="4" class="normalfnt"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                        <tr>
                          <td width="100%" style="text-align:center"><a class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butReport" name="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
                        </tr>
                      </table></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </form></td>
      </tr>
      <!--END - TRAIL BALANCE SELECTION CRITERIA--> 
      
      <!--BEGIN - INCOME STATEMENT SELECTION CRITERIA { -->
      <tr id="IncomeStatement">
        <td><table align="left" width="100%" class="main_header_table" id="tblIncomeStatement" cellpadding="0" cellspacing="0">
            <tr>
              <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Income Statement.</u></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><form id="frmIncomeStatement" name="frmIncomeStatement">
            <div id="divIncomeStatement" class="clsIncomeStatement" style="width:100%;display:none">
              <table align="left" width="100%" class="main_table" id="tblIncomeStatementHeader" border="0" cellpadding="0" cellspacing="0">
                <thead>
                  <tr>
                    <td colspan="7" style="text-align:left">Income Statement Selection Criteria</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td width="21%" class="normalfnt" height="">Currency</td>
                    <td colspan="3" class="normalfnt"><select id="cboCurrency" name="cboCurrency" style="width:120px" class="validate[required]">
                        <?php 
										$sql="SELECT
												mst_financecurrency.intId,
												mst_financecurrency.strCode
												FROM mst_financecurrency
												WHERE
												mst_financecurrency.intStatus =  '1'
												ORDER BY
												mst_financecurrency.intId";
                                   		$result=$db->RunQuery($sql);
										while($row=mysqli_fetch_array($result))
										{
										echo "<option value=\"".$row["intId"]."\">".$row["strCode"]."</option>";
										}
									?>
                      </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt" height="">Report Type</td>
                    <td colspan="3" class="normalfnt"><select name="cboReportType" id="cboReportType" style="width:160px">
                    <option value="rpt_income_statement.php" selected="selected">Company Wise</option>
                    <option value="rpt_income_statement_cswise.php">Cost Center Wise</option>
                    </select></td>
                  </tr>
                  <tr id="tr_costCenter" style="display:none">
                    <td class="normalfnt" height="">Cost Center</td>
                    <td colspan="3" class="normalfnt"><select name="cboCostCenter" id="cboCostCenter" style="width:160px">
                    <option value=""></option>
                    <?php
						$sql="SELECT intLocation,strName
								FROM mst_financedimension
								WHERE intStatus=1
								ORDER BY strName";
						$result=$db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
						echo "<option value=\"".$row["intLocation"]."\">".$row["strName"]."</option>";
						}
					?>
                    </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Date</td>
                    <td width="21%" class="normalfnt"><input name="dtFromDateInCome" type="text" value="<?php   echo date("Y-m-d"); ?>" class="txtbox" id="dtFromDateInCome" style="width:90px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                    <td width="6%" class="normalfnt">To</td>
                    <td width="52%" class="normalfnt"><input name="dtToDateInCome" type="text" value="<?php  echo date("Y-m-d"); ?>" class="txtbox" id="dtToDateInCome" style="width:90px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                  </tr>
                  <tr>
                    <td colspan="4" class="normalfnt"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                        <tr>
                          <td width="100%" style="text-align:center"><a class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butReport" name="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
                        </tr>
                      </table></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </form></td>
      </tr>
      <!--END - INCOME STATEMENT SELECTION CRITERIA }--> 
      
      <!--BEGIN - BALANCE SHEET SELECTION CRITERIA { -->
      <tr id="BalanceSheet">
        <td><table align="left" width="100%" class="main_header_table" id="tblBalanceSheet" cellpadding="0" cellspacing="0">
            <tr>
              <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Balance Sheet.</u></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><form id="frmBalanceSheet" name="frmBalanceSheet">
            <div id="divBalanceSheet" class="clsBalanceSheet" style="width:100%;display:none">
              <table align="left" width="100%" class="main_table" id="tblBalanceSheetHeader" border="0" cellpadding="0" cellspacing="0">
                <thead>
                  <tr>
                    <td colspan="8" style="text-align:left">Balance Sheet Selection Criteria</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td width="21%" class="normalfnt" height="">Currency</td>
                    <td colspan="4" class="normalfnt"><select id="cboCurrency" name="cboCurrency" style="width:120px" class="validate[required]">
                        <?php 
										$sql="SELECT
												mst_financecurrency.intId,
												mst_financecurrency.strCode
												FROM mst_financecurrency
												WHERE
												mst_financecurrency.intStatus =  '1'
												ORDER BY
												mst_financecurrency.intId";
                                   		$result=$db->RunQuery($sql);
										while($row=mysqli_fetch_array($result))
										{
										echo "<option value=\"".$row["intId"]."\">".$row["strCode"]."</option>";
										}
									?>
                      </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Date</td>
                    <td width="21%" class="normalfnt"><input name="dtFromDateBS" type="text" value="<?php   echo date("Y-m-d"); ?>" class="txtbox" id="dtFromDateBS" style="width:90px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                    <td width="6%" class="normalfnt">To</td>
                    <td width="26%" class="normalfnt"><input name="dtToDateBS" type="text" value="<?php  echo date("Y-m-d"); ?>" class="txtbox" id="dtToDateBS" style="width:90px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                    <td width="26%" class="normalfnt"><a class="button green small previous">Previous</a><a class="button green small after">&nbsp;&nbsp;&nbsp;&nbsp;Next&nbsp;&nbsp;&nbsp;&nbsp;</a></td>
                  </tr>
                  <tr>
                    <td colspan="5" class="normalfnt"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                        <tr>
                          <td width="100%" style="text-align:center"><a class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butReport" name="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
                        </tr>
                      </table></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </form></td>
      </tr>
      <!--END - BALANCE SHEET SELECTION CRITERIA }-->
      

      
      <!--BEGIN - SVAT REPORT CRITERIA { -->
      <tr id="SVATReports">
        <td><table align="left" width="100%" class="main_header_table" id="tblSVATReports" cellpadding="0" cellspacing="0">
            <tr>
              <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>SVAT Reports.</u></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><form id="frmSVATReports" name="frmSVATReports">
            <div id="divSVATReports" class="clsSVATReports" style="width:100%;display:none">
              <table width="100%" border="0" cellpadding="1" align="center" class="main_table" id="tblSVATReportsDetail">
              <thead>
                  <tr>
                    <td colspan="8" style="text-align:left">SVAT Reports Selection Criteria</td>
                  </tr>
                </thead>
                <tr>
                  <td class="normalfnt" style="text-align:center"><input name="chkDate" id="chkDate" type="checkbox" checked="checked" onClick="checkDate()" /></td>
                  <td colspan="2" class="normalfnt">Invoice Date From</td>
                  <td width="160"><span class="normalfnt">
                    <input name="txtFromDate" type="text" tabindex="7" class="txtbox" id="txtFromDate" style="width:90px"  onmousedown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onFocus="return showCalendar(this.id, '%Y-%m-%d');" value="<?php echo date('Y-m-d');?>" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input name="reset" type="reset"  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%d');" value="" />
                  </span></td>
                  <td width="18"><span class="normalfnt">To</span></td>
                  <td width="140" class="normalfnt"><input name="txtToDate" type="text" tabindex="7" class="txtbox" id="txtToDate" style="width:90px"  onmousedown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onFocus="return showCalendar(this.id, '%Y-%m-%d');" value="<?php echo date('Y-m-d');?>" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input name="resetto" type="reset"  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%d');" value="" /></td>
                </tr>
                <tr>
                  <td width="20" class="normalfnt">&nbsp;</td>
                  <td colspan="2" class="normalfnt">Currency / Rate<span class="compulsoryRed"> *</span></td>
                  <td colspan="2" class="normalfnt"><select name="cboCurrency2" id="cboCurrency2" style="width:165px" disabled="disabled">
                    <option value="2" selected="selected">SLR</option>
                    <!-- <?php
		  $sql = "select intCurID,strCurrency from currencytypes order by intCurID;";
		  $result = $db->RunQuery($sql);
		  while($row = mysqli_fetch_array($result))
		  {
		  		echo "<option value=\"". $row["intCurID"] ."\">" . $row["strCurrency"] ."</option>" ;
		  }		  
		  ?>-->
                  </select></td>
                  <td class="normalfnt"><label for="textfield"></label>
                    <input type="text" name="txtCurrencyRate" id="txtCurrencyRate" style="width:90px" /></td>
                </tr>
                <tr>
                  <td class="normalfnt">&nbsp;</td>
                  <td colspan="2" class="normalfnt">Report Type<span class="compulsoryRed"></span></td>
                  <td colspan="2" class="normalfnt"><select name="cboReportType" id="cboReportType" style="width:165px">
                    <option value="3" selected="selected">All</option>
                    <option value="rpt_svat_04.php">SVAT 04</option>
                    <option value="rpt_svat_05.php">SVAT 05</option>
                    <option value="rpt_svat_05_b.php">SVAT 05(b)</option> 
                    <option value="rpt_svat_07">SVAT 07</option>
                    <option value="rpt_svat_07_a.php">SVAT 07(a)</option>
                    <option value="rpt_svat_07_b.php">SVAT 07(b)</option>
                    <option value="rpt_svat_07_c.php">SVAT 07(c)(NFE)</option>
                  </select></td>
                  <td class="normalfnt">&nbsp;</td>
                </tr>
                <tr>
                  <td height="22" colspan="6" class="normalfnt"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                        <tr>
                          <td width="100%" style="text-align:center"><a class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butReport" name="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
                        </tr>
                      </table></td>
                </tr>
              </table>
            </div>
          </form></td>
      </tr>
      <!--END - SVAT REPORT CRITERIA }-->
      
            <!--BEGIN - TRANSACTION LOG CRITERIA {-->
       <tr id="TransactionLog">
        <td><table align="left" width="100%" class="main_header_table" id="tblTransactionLog" cellpadding="0" cellspacing="0">
            <tr>
              <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Transaction Log.</u></td>
            </tr>
          </table></td>
      </tr>
      <!--BEGIN - TRANSACTION LOG CRITERIA }-->
      
      <!--BEGIN - GL MAPPING LIST {-->
      <tr id="tr_GLMapping">
        <td><table align="left" width="100%" class="main_header_table" id="tblGLMapping" cellpadding="0" cellspacing="0">
            <tr>
              <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>GL Accounts Mapping List</u></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><form id="frmGLMapping" name="frmGLMapping">
            <div id="div_GLMapping" class="cls_GLMapping" style="width:100%;display:none">
              <table width="100%" border="0" cellpadding="1" align="center" class="main_table" id="tblGLMappingDetails">
              <thead>
                  <tr>
                    <td colspan="6" style="text-align:left">GL Accounts Mapping Selection Criteria</td>
                  </tr>
                </thead>
                <tr>
                  <td colspan="2" class="normalfnt">Main Category</td>
                  <td width="321"><select name="cboMainCategory" id="cboMainCategory"  style="width:250px">
				  <?php echo $mst_maincategory->getCombo();?>
                  </select></td>
                  <td width="140" class="normalfnt">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" class="normalfnt">Sub Category</td>
                  <td class="normalfnt"><select name="cboSubCategory" id="cboSubCategory" style="width:250px">
                   <option value="">&nbsp;</option>
                    <?php //echo $mst_subcategory->getCombo();?>
                  </select></td>
                  <td class="normalfnt">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" class="normalfnt">Chart Of Account</td>
                  <td class="normalfnt"><select name="cboChartOfAccount" id="cboChartOfAccount" style="width:250px">
                  <option value="">&nbsp;</option>
                    <?php echo $finance_mst_chartofaccount->getFilterdAccountCombo($sessions->getCompanyId(),'N');?>
                  </select></td>
                  <td class="normalfnt">&nbsp;</td>
                </tr>
                <tr>
                  <td height="22" colspan="4" class="normalfnt"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                        <tr>
                          <td width="100%" style="text-align:center"><a class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butReport" name="butReport">Excel Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
                        </tr>
                      </table></td>
                </tr>
              </table>
            </div>
          </form></td>
      </tr>      
      <!--END - GL MAPPING LIST }-->
      
       <!--BEGIN - GL MAPPING LIST {-->
      <tr id="tr_statementCashFlow">
        <td><table align="left" width="100%" class="main_header_table" id="tblStatementCashFlow" cellpadding="0" cellspacing="0">
            <tr>
              <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Cash Flow Statement</u></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><form id="frmStatementCashFlow" name="frmStatementCashFlow">
            <div id="div_StatementCashFlow" class="cls_StatementCashFlow" style="width:100%;display:none">
              <table width="100%" border="0" cellpadding="1" align="center" class="main_table" id="tblStatementCashFlowDetails">
              <thead>
                  <tr>
                    <td colspan="6" style="text-align:left">Cash Flow Statement</td>
                  </tr>
                </thead>
                <tr>
                  <td colspan="2" class="normalfnt">Currency</td>
                  <td><select name="cboCurrency" id="cboCurrency" style="width:94px">
                  <?php 
				  echo $mst_financecurrency->getCombo($mst_companies->getintBaseCurrencyId());
				  ?>
                  </select></td>
                  <td class="normalfnt">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" class="normalfnt">To Date</td>
                  <td width="321"><input type="text" onclick="return showCalendar(this.id, '%Y-%m-%d');" onkeypress="return ControlableKeyAccess(event);" onmouseout="EnableRightClickEvent();" onmousedown="DisableRightClickEvent();" style="width:90px;" id="dtCFToDate" class="txtbox" value="<?php echo date('Y-m-d')?>" name="dtCFToDate"><input type="reset" onclick="return showCalendar(this.id, '%Y-%m-%');" style="visibility:hidden;" class="txtbox" value=""></td>
                  <td width="140" class="normalfnt">&nbsp;</td>
                </tr>
                <tr>
                  <td height="22" colspan="4" class="normalfnt"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                        <tr>
                          <td width="100%" style="text-align:center"><a class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butReport" name="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
                        </tr>
                      </table></td>
                </tr>
              </table>
            </div>
          </form></td>
      </tr>
      
      <!--END - GL MAPPING LIST }-->
      <?php
		if(loadViewMode('P01138',$userId)==1){
		?>

      <tr id="tr_nonMovingOrders">
        <td><table align="left" width="100%" class="main_header_table" id="tblNonMovingOrders" cellpadding="0" cellspacing="0">
            <tr>
              <td width="544"><img src="images/report_go.png" class="mouseover" />&nbsp;<u>Dispatch Raised Non Moving Orders</u></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><form id="frmNonMovingOrders" name="frmNonMovingOrders">
            <div id="div_nonMovingOrders" class="cls_nonMovingOrders" style="width:100%;display:none">
              <table width="100%" border="0" cellpadding="1" align="center" class="main_table" id="tblNonMovingOrdersDetail">
              <thead>
                  <tr>
                    <td colspan="6" style="text-align:left">Non Moving Orders</td>
                  </tr>
                </thead>
                <tr>
                    <td width="21%" class="normalfnt" height="">Main Cluster</td>
                    <td colspan="4" class="normalfnt"><select id="cboMainCluster" name="cboMainCluster" style="width:120px" class="validate[required]">
                       <?php echo $sys_main_clusters->getCombo();
									?>
                      </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">From</td>
                    <td width="21%" class="normalfnt"><input name="dtFromDateNM" type="text" value="" class="txtbox" id="dtFromDateNM" style="width:90px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                    <td width="6%" class="normalfnt">To</td>
                    <td width="26%" class="normalfnt"><input name="dtToDateNM" type="text" value="" class="txtbox" id="dtToDateNM" style="width:90px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                    
                  </tr>
                <tr>
                  <td height="22" colspan="4" class="normalfnt"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                        <tr>
                          <td width="100%" style="text-align:center"><a class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butReport" name="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
                        </tr>
                      </table></td>
                </tr>
              </table>
            </div>
          </form></td>
      </tr>
      <?php } ?>
    </table>
  </div>
</div>
<?php
function loadViewMode($programCode,$intUser){
	global $db;
	
	$editMode	=0;
	$sqlp 		= "SELECT
					menupermision.intView  
					FROM menupermision 
					Inner Join menus ON menupermision.intMenuId = menus.intId
					WHERE
					menus.strCode =  '$programCode' AND
					menupermision.intUserId =  '$intUser'";	
				
	$resultp 	= $db->RunQuery($sqlp);
	$rowp		= mysqli_fetch_array($resultp);
		 
	return $rowp['intView'];
}
?>
<?php
ini_set('display_errors',0);
session_start();
#BEGIN - INCLUDE FILE {
require_once '../../../dataAccess/DBManager2.php';	$db				= new DBManager2();
require_once '../../../libraries/excel/Classes/PHPExcel.php';
require_once '../../../libraries/excel/Classes/PHPExcel/IOFactory.php';
#END 	- INCLUDE FILE }

$mainCatId	= $_REQUEST["MainCat"];
$subCatId	= $_REQUEST["SubCat"];
$gl			= $_REQUEST["GL"];

error_reporting(E_ALL);
$db->connect();
$file = 'gl_mapping_report_xls.xls'; //LOAD TEMPARARY FILE

if (!file_exists($file))
	exit("Can't find $fileName");

$objPHPExcel = PHPExcel_IOFactory::load($file);
$i = 2;

if($mainCatId!="")
	$para	.= "AND I.intMainCategory = $mainCatId ";
if($subCatId!="")
	$para	.= "AND I.intSubCategory = $subCatId ";
if($gl!="")
	$para	.= "AND FCOA.CHART_OF_ACCOUNT_ID = $gl ";
	
	  $sql = "SELECT
				  MC.strName                 AS MAIN_CATEGORY_NAME,
				  SC.strName                 AS SUB_CATEGORY_NAME,
				  SC.strCode                 AS SUB_CATEGORY_CODE,
				  I.strName                  AS ITEM_NAME,
				  FAT.FINANCE_TYPE_NAME      AS FINANCE_TYPE_NAME,
				  FMT.MAIN_TYPE_NAME         AS MAIN_TYPE_NAME,
				  FST.SUB_TYPE_NAME          AS SUB_TYPE_NAME,
				  FCOA.CHART_OF_ACCOUNT_NAME AS CHART_OF_ACCOUNT_NAME
				FROM mst_item I
				  INNER JOIN finance_mst_chartofaccount_subcategory FSC
					ON FSC.SUBCATEGORY_ID = I.intSubCategory
				  INNER JOIN mst_maincategory MC
					ON MC.intId = I.intMainCategory
				  INNER JOIN mst_subcategory SC
					ON SC.intId = I.intSubCategory
				  INNER JOIN finance_mst_chartofaccount FCOA
					ON FCOA.CHART_OF_ACCOUNT_ID = FSC.CHART_OF_ACCOUNT_ID
				  INNER JOIN finance_mst_account_sub_type FST
					ON FST.SUB_TYPE_ID = FCOA.SUB_TYPE_ID
				  INNER JOIN finance_mst_account_main_type FMT 
					ON FMT.MAIN_TYPE_ID = FST.MAIN_TYPE_ID
				  INNER JOIN finance_mst_account_type FAT
					ON FAT.FINANCE_TYPE_ID = FMT.FINANCE_TYPE_ID
				  WHERE 1 = 1
				  	$para";
	$result = $db->RunQuery($sql);
	$booAvailable	= false;
	$objPHPExcel->getActiveSheet()->insertNewRowBefore($i+1,count($result)-1);	
	foreach($result as $row)
	{
		$booAvailable	= true;
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$i,$row['MAIN_CATEGORY_NAME']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,$i,$row["SUB_CATEGORY_NAME"]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,$i,$row["SUB_CATEGORY_CODE"]);
		$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(3,$i,$row["ITEM_NAME"]);
		$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(4,$i,$row["FINANCE_TYPE_NAME"]);
		$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(5,$i,$row["MAIN_TYPE_NAME"]);
		$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(6,$i,$row["SUB_TYPE_NAME"]);
		$objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(7,$i,$row["CHART_OF_ACCOUNT_NAME"]);
		$i++;
	}
	
if(!$booAvailable)
{
	echo "<script type=\"text/javascript\">alert('Sorry! No details available to view.');window.close();</script>";
	die('Sorry! No report details appear to view.');
}

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$file.'"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output'); 
echo 'done';
exit;
?>
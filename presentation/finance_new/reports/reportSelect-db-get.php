<?php 
session_start();
$backwardseperator 	= "../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$locationId	  		= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$requestType 		= $_REQUEST['requestType'];

require_once '../../../dataAccess/DBManager2.php';			$db				= new DBManager2();
require_once "../../../class/tables/mst_subcategory.php"; 	$mst_subcategory	= new mst_subcategory($db);
##################################
## OPEN CONNECTION				##
##################################
$db->connect();

if($requestType=='loadMainType')
{
	$financeType = $_REQUEST['financeType'];
	
	$response['mainTypeCombo']	= getMainTypeCombo($financeType);
	echo json_encode($response);
}
if($requestType=='loadSubType')
{
	$mainType = $_REQUEST['mainType'];
	
	$response['subTypeCombo']	= getSubTypeCombo($mainType);
	echo json_encode($response);
}
if($requestType=='loadChartOfAcc')
{
	$subType = $_REQUEST['subType'];
	
	$response['chartOfAccCombo']	= getChartOfAccCombo($subType);
	echo json_encode($response);
}
if($requestType=='URLLoadSubCategory')
{
	$mainCatId = $_REQUEST['MainCatId'];

	$response['html']	= $mst_subcategory->getCombo('',"intMainCategory = $mainCatId");
	echo json_encode($response);
}
##################################
## CLOSE CONNECTION				##
##################################
	$db->disconnect();

function getMainTypeCombo($financeType)
{
	global $db;
	
	$sql = "SELECT MAIN_TYPE_ID,MAIN_TYPE_NAME
			FROM finance_mst_account_main_type
			WHERE FINANCE_TYPE_ID='$financeType' AND
			STATUS='1'
			ORDER BY MAIN_TYPE_NAME";
	$result = $db->RunQuery($sql);
	$html 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$html.="<option value=\"".$row['MAIN_TYPE_ID']."\">".$row['MAIN_TYPE_NAME']."</option>";
	}
	return $html;
}
function getSubTypeCombo($mainType)
{
	global $db;
	
	$sql = "SELECT SUB_TYPE_ID,SUB_TYPE_NAME
			FROM finance_mst_account_sub_type
			WHERE MAIN_TYPE_ID='$mainType' AND
			STATUS='1'
			ORDER BY SUB_TYPE_NAME ";
	$result = $db->RunQuery($sql);
	$html 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$html.="<option value=\"".$row['SUB_TYPE_ID']."\">".$row['SUB_TYPE_NAME']."</option>";
	}
	return $html;
}
function getChartOfAccCombo($subType)
{
	global $db;
	global $companyId;
	
	$sql = "SELECT FCOA.CHART_OF_ACCOUNT_ID,
		CONCAT(FCOA.CHART_OF_ACCOUNT_NAME,'-',CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE)) AS GLAccount
		FROM finance_mst_chartofaccount FCOA
		INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
		INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
		INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
		INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID
		WHERE FCOA.SUB_TYPE_ID='$subType' AND 
		FCOAC.COMPANY_ID='$companyId'
		ORDER BY FCOA.CHART_OF_ACCOUNT_NAME ";
	$result = $db->RunQuery($sql);
	$html 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$html.="<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['GLAccount']."</option>";
	}
	return $html;
}
?>
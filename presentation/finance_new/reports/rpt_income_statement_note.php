<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$gross_profit_loss		= 0;
$companyId 				= $_SESSION["headCompanyId"];
$locationId				= $_SESSION["CompanyID"];
$target_currencyId		= $_REQUEST["CurrencyId"];
$dateFrom				= $_REQUEST["DateFrom"];
$dateTo					= $_REQUEST["DateTo"];

$GLSumReportId			= 968;
?>
<head>
<title>Income Statement Note</title>
</head>

<body>
<table width="800" border="0" align="center">
  <tr>
    <td align="center"><?php include 'reportHeader.php'?></td>
  </tr>
  <tr>
    <td class="reportHeader" align="center">Income Statement Note</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" class="rptBordered">
        <thead>
          <tr>
            <th width="12%">Chart Of Account Code</th>
            <th width="68%">Chart Of Account Name</th>
            <th width="10%">Amount</th>
          </tr>
        </thead>
        <tbody>
          <!--BEGIN - GROSS PROFIT/(LOSS) { -->
          <?php $result = GrossProfit_part01('6,8,7,9');
		while($row = mysqli_fetch_array($result))
		{
?>
          <?php if(!isset($main_type) || $main_type!=$row["SUB_TYPE_NAME"]){
				
		?>
        <?php if(isset($main_type)){ ?>
          <tr>
            <td style="text-align:center">&nbsp;</td>
            <td><b>TOTAL</b></td>
            <td style="text-align:right;border-top-style:groove;border-bottom-style:double"><b><?php echo number_format($total,2)?></b></td>
          </tr>
          <tr>
          	<td colspan="3">&nbsp;</td>
          </tr>
          <?php } ?>
          <tr bgcolor="#ECECFF">
            <td style="text-align:center"><?php echo $row["SUB_TYPE_CODE"]?></td>
            <td><a name="<?php echo $row["SUB_TYPE_NAME"]?>"><?php echo $row["SUB_TYPE_NAME"]?></a></td>
            <td>&nbsp;</td>
          </tr>
          <?php $total = 0;
		  		$main_type = $row["SUB_TYPE_NAME"];
		  } ?>
          <tr>
            <td style="text-align:center"><?php echo $row["CHART_OF_ACCOUNT_CODE"]?></td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;<a href="?q=<?php echo $GLSumReportId; ?>&fromDate=<?php echo $dateFrom?>&toDate=<?php echo $dateTo?>&currency=<?php echo $target_currencyId?>&financeType=<?php echo $row["FINANCE_TYPE_ID"]?>&mainCateId=<?php echo $row["MAIN_TYPE_ID"]?>&subCatId=<?php echo $row["SUB_TYPE_ID"]?>&chartOfAccId=<?php echo $row["CHART_OF_ACCOUNT_ID"]?>" target="rptGeneralLedgerSummary.php"><?php echo $row["CHART_OF_ACCOUNT_NAME"]?></a></td>
            <td style="text-align:right"><?php echo number_format($row["AMOUNT"],2)?></td>
          </tr>
          <?php
		  $total += round($row["AMOUNT"],2);
		}
?>
		   <tr>
            <td style="text-align:center">&nbsp;</td>
            <td><b>TOTAL</b></td>
            <td style="text-align:right;border-top-style:groove;border-bottom-style:double"><b><?php echo number_format($total,2)?></b></td>
          </tr>
      </table></td>
  </tr>
</table>
</body>
</html>
<?php
function GrossProfit_part01($para)
{
	global $db;
	global $companyId;
	global $target_currencyId;
	global $dateFrom;
	global $dateTo;
	
	/*$sql = "SELECT 
				T.FINANCE_TYPE_NAME												AS FINANCE_TYPE_NAME,
				MT.MAIN_TYPE_NAME												AS MAIN_TYPE_NAME,
				CONCAT(T.FINANCE_TYPE_CODE,MT.MAIN_TYPE_CODE,ST.SUB_TYPE_CODE) 	AS SUB_TYPE_CODE,
				ST.SUB_TYPE_NAME												AS SUB_TYPE_NAME,				
				COA.CHART_OF_ACCOUNT_NAME										AS CHART_OF_ACCOUNT_NAME,
				CONCAT(T.FINANCE_TYPE_CODE,MT.MAIN_TYPE_CODE,ST.SUB_TYPE_CODE,COA.CHART_OF_ACCOUNT_CODE) 	AS CHART_OF_ACCOUNT_CODE,
				ROUND(COALESCE(SUM(FT.AMOUNT),0),2) 							AS AMOUNT1,
				ROUND(COALESCE(SUM(FT.AMOUNT / (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId) * (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId)),0),2) AS AMOUNT
			FROM finance_mst_account_main_type MT 
			INNER JOIN finance_mst_account_type T ON T.FINANCE_TYPE_ID = MT.FINANCE_TYPE_ID
			INNER JOIN finance_mst_account_sub_type ST ON ST.MAIN_TYPE_ID = MT.MAIN_TYPE_ID
			INNER JOIN finance_mst_chartofaccount COA ON COA.SUB_TYPE_ID = ST.SUB_TYPE_ID
			LEFT JOIN finance_transaction FT ON FT.CHART_OF_ACCOUNT_ID = COA.CHART_OF_ACCOUNT_ID
			WHERE MT.MAIN_TYPE_ID IN ($para)
				AND (DATE(FT.LAST_MODIFIED_DATE) BETWEEN '$dateFrom' AND '$dateTo' OR DATE(FT.LAST_MODIFIED_DATE) IS NULL)
			GROUP BY ST.MAIN_TYPE_ID,ST.SUB_TYPE_ID,COA.CHART_OF_ACCOUNT_ID";
	return $db->RunQuery($sql);*/
	
	$sql = "SELECT 
			  FINANCE_TYPE_ID,
			  FINANCE_TYPE_NAME,
			  MAIN_TYPE_ID,
			  MAIN_TYPE_NAME,
			  SUB_TYPE_ID																	AS SUB_TYPE_ID,
			  CONCAT(FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE) 						AS SUB_TYPE_CODE,
			  SUB_TYPE_NAME																	AS SUB_TYPE_NAME,
			  CHART_OF_ACCOUNT_ID															AS CHART_OF_ACCOUNT_ID,
			  CONCAT(FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE,CHART_OF_ACCOUNT_CODE) 	AS CHART_OF_ACCOUNT_CODE,
			  CHART_OF_ACCOUNT_NAME 														AS CHART_OF_ACCOUNT_NAME,
			  SUM(IF(TRANSACTION_ADD_TYPE='D',DEBIT-CREDIT,CREDIT-DEBIT))					AS AMOUNT
			FROM 
			(SELECT 
			  TRANSACTION_ADD_TYPE,
			  FINANCE_TYPE_ID,
			  FINANCE_TYPE_CODE,
			  FINANCE_TYPE_NAME,
			  MAIN_TYPE_ID,
			  MAIN_TYPE_CODE,
			  MAIN_TYPE_NAME,
			  SUB_TYPE_ID,
			  SUB_TYPE_CODE,
			  SUB_TYPE_NAME,
			  CHART_OF_ACCOUNT_ID,
			  CHART_OF_ACCOUNT_CODE,
			  CHART_OF_ACCOUNT_NAME,
			  IF(TRANSACTION_TYPE='D',AMOUNT,0) AS DEBIT,
			  IF(TRANSACTION_TYPE='C',AMOUNT,0) AS CREDIT
			FROM 
			(SELECT
				T.TRANSACTION_ADD_TYPE,
				T.FINANCE_TYPE_ID,
				T.FINANCE_TYPE_CODE,
				T.FINANCE_TYPE_NAME,
				MT.MAIN_TYPE_ID,
				MT.MAIN_TYPE_CODE,
				MT.MAIN_TYPE_NAME,
				ST.SUB_TYPE_ID,
				ST.SUB_TYPE_CODE,
				ST.SUB_TYPE_NAME,
				COA.CHART_OF_ACCOUNT_ID,
				COA.CHART_OF_ACCOUNT_CODE,
				COA.CHART_OF_ACCOUNT_NAME,
				FT.CURRENCY_ID,
				FT.TRANSACTION_TYPE,
				ROUND(COALESCE(FT.AMOUNT / (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = $target_currencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId) * (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId),0),2) AS AMOUNT
			FROM finance_mst_account_type T
			INNER JOIN finance_mst_account_main_type MT ON T.FINANCE_TYPE_ID = MT.FINANCE_TYPE_ID
			INNER JOIN finance_mst_account_sub_type ST ON MT.MAIN_TYPE_ID = ST.MAIN_TYPE_ID
			INNER JOIN finance_mst_chartofaccount COA ON ST.SUB_TYPE_ID = COA.SUB_TYPE_ID
			LEFT JOIN finance_transaction FT ON COA.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID
			WHERE MT.MAIN_TYPE_ID IN ($para)
			AND (FT.COMPANY_ID = $companyId	OR FT.COMPANY_ID IS NULL)
			AND (DATE(FT.LAST_MODIFIED_DATE) BETWEEN '$dateFrom' AND '$dateTo' OR DATE(FT.LAST_MODIFIED_DATE) IS NULL))
			AS SUB_1) AS SUB_2
			GROUP BY FINANCE_TYPE_ID,MAIN_TYPE_ID,SUB_TYPE_ID,CHART_OF_ACCOUNT_ID
			ORDER BY FINANCE_TYPE_CODE,MAIN_TYPE_CODE,SUB_TYPE_CODE,CHART_OF_ACCOUNT_CODE";
		return $db->RunQuery($sql);
}
?>
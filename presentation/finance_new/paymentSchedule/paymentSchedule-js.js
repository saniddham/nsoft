// JavaScript Document
var basePath	= "presentation/finance_new/paymentSchedule/";
var reportId	= 978;
var menuId		= 761;
$(document).ready(function(){
	
	$("#frmPaymentSchedule").validationEngine();
	
	/*if(intAddx)
	{
		$('#frmPaymentSchedule #butNew').show();
		
		if(status==-2)
			$('#frmPaymentSchedule #butSave').hide();
		else
			$('#frmPaymentSchedule #butSave').show();
	}
	//permision for edit 
	if(intEditx)
	{
		if(status==-2)
			$('#frmPaymentSchedule #butSave').hide();
		else
			$('#frmPaymentSchedule #butSave').show();
	}*/
	
	$('#frmPaymentSchedule #butInsertRow').die('click').live('click',addNewRow);
	$('#frmPaymentSchedule .removeRow').die('click').live('click',deleteRow);
	$('#frmPaymentSchedule .clsPayDate').die('blur').live('blur',function(){addValidateClass(this,'Date')});
	$('#frmPaymentSchedule .clsAmount').die('blur').live('blur',function(){addValidateClass(this,'Amount')});
	$('#frmPaymentSchedule #butSave').die('click').live('click',saveData);
	$('#frmPaymentSchedule .clsAmount').die('keyup').live('keyup',calculateTotal);
	$('#frmPaymentSchedule #butNew').die('click').live('click',clearAll);
	$('#frmPaymentSchedule #cboCreditAccount').die('change').live('change',loadSettleCombo);
	$('#frmPaymentSchedule #cboSettleAccount').die('change').live('change',loadBalAmount);
	
	$('#frmPaymentSchedule #butConfirm').die('click').live('click',Confirm);
	$('#frmPaymentSchedule #butCancel').die('click').live('click',Cancel);
	$('#frmPaymentSchedule #butReport').die('click').live('click',Report);
	
	
});
function addNewRow()
{
	var lastDateIdArr 	= $('#frmPaymentSchedule #tblMain tbody tr:last').find('.clsPayDate').attr('id').split('~');
	var lastDateId		= parseFloat(lastDateIdArr[1]);
	$('#frmPaymentSchedule #tblMain tbody tr:last').after("<tr>"+$('#frmPaymentSchedule #tblMain .cls_tr_firstRow').html()+"</tr>");
	$('#frmPaymentSchedule #tblMain tbody tr:last').find('.clsPayDate').attr('id','txtPayDate~'+(lastDateId+1));
	$('#frmPaymentSchedule #tblMain tbody tr:last').find('.clsPayDate').val('');
	$('#frmPaymentSchedule #tblMain tbody tr:last').find('.clsPayDate').removeAttr('disabled');
	$('#frmPaymentSchedule #tblMain tbody tr:last').find('.clsAmount').val('');
	$('#frmPaymentSchedule #tblMain tbody tr:last').find('.clsAmount').removeAttr('disabled');
	$('#frmPaymentSchedule #tblMain tbody tr:last').find('.clsStatus').html('&nbsp;');
	$('#frmPaymentSchedule #tblMain tbody tr:last').find('.clsProcessedDate').html('&nbsp;');
	
	if($('#frmPaymentSchedule #tblMain tbody tr:last').find('.clsStatus').attr('id')==1)
	{
		$('#frmPaymentSchedule #tblMain tbody tr:last').find('.clsDelete').html('<img src="images/del.png" class="removeRow mouseover"/>');
	}
	$('#frmPaymentSchedule #tblMain tbody tr:last').find('.clsStatus').attr('id',0);
}
function deleteRow()
{
	var delRowCount = parseInt(document.getElementById('tblMain').rows.length);
	
	if(delRowCount>4)
		$(this).parent().parent().remove();
	
	if(parseInt(document.getElementById('tblMain').rows.length)==4)
		$('#frmPaymentSchedule #tblMain tbody tr:last').addClass('cls_tr_firstRow');
}
function addValidateClass(obj,type)
{
	switch(type)
	{
		case 'Date' :
			if($(obj).val()!='')
				$(obj).parent().parent().find('.clsAmount').addClass('validate[required,custom[number]]');
			else
				$(obj).parent().parent().find('.clsAmount').removeClass('validate[required,custom[number]]');
		case 'Amount' :
			if($(obj).val()!='')
				$(obj).parent().parent().find('.clsPayDate').addClass('validate[required]');
			else
				$(obj).parent().parent().find('.clsPayDate').removeClass('validate[required]');
	}
}
function calculateTotal()
{
	var totalValue		= 0;
	
	$('#frmPaymentSchedule #tblMain .clsAmount').each(function(){
	
			totalValue += parseFloat(($(this).val()==''?0:$(this).val()));
	});
	
	totalValue	 = RoundNumber(totalValue,2);
	
	$('#frmPaymentSchedule #tblMain .cls_td_totAmount').html(totalValue);	
}
function saveData()
{
	if(!IsProcessMonthLocked_date($('#frmPaymentSchedule #txtDate').val()))
		return;
		
	showWaiting();
	var scheduleNo		= $('#frmPaymentSchedule #txtScheduleNo').val();
	var scheduleYear	= $('#frmPaymentSchedule #txtScheduleYear').val();
	var date			= $('#frmPaymentSchedule #txtDate').val();
	var currency		= $('#frmPaymentSchedule #cboCurrency').val();
	var creditGL		= $('#frmPaymentSchedule #cboCreditAccount').val();
	var settleGL		= $('#frmPaymentSchedule #cboSettleAccount').val();
	var debitGL			= $('#frmPaymentSchedule #cboDebitAccount').val();
	var remarks			= $('#frmPaymentSchedule #txtRemarks').val();
	var balAmount		= parseFloat($('#frmPaymentSchedule #txtBalanceAmount').val());
	var totalAmonut		= parseFloat($('#frmPaymentSchedule #tblMain .cls_td_totAmount').html());
	
	if($('#frmPaymentSchedule').validationEngine('validate'))
	{
		if(totalAmonut!=balAmount)
		{
			$('#frmPaymentSchedule #tblMain .cls_td_totAmount').validationEngine('showPrompt','Total Amount must equal to Balance Amount.','fail');
			hideWaiting();	
			return;
		}
		
		var data = "requestType=saveData";
		var arrHeader = "{";
							arrHeader += '"scheduleNo":"'+scheduleNo+'",' ;
							arrHeader += '"scheduleYear":"'+scheduleYear+'",' ;
							arrHeader += '"date":"'+date+'",' ;
							arrHeader += '"currency":"'+currency+'",' ;
							arrHeader += '"remarks":'+URLEncode_json(remarks)+',';
							arrHeader += '"creditGL":"'+creditGL+'",' ;
							arrHeader += '"settleGL":"'+settleGL+'",' ;
							arrHeader += '"totalAmonut":"'+totalAmonut+'",' ;
							arrHeader += '"debitGL":"'+debitGL+'"' ;

			arrHeader += "}";
		
		var chkStatus	= false;
		var chkDtStatus	= true;
		var arrDetails	= "";
		
		$('#frmPaymentSchedule .clsPayDate').each(function(){
			
			var payDate 	= $(this).val();	
			var amount	 	= $(this).parent().parent().find('.clsAmount').val();
			
			if(payDate!='' && amount!='')
			{
				if($(this).parent().parent().find('.clsStatus').attr('id')==0 && payDate<curDate)
				{
					$(this).validationEngine('showPrompt','Date must greater than current Date.','fail');
					chkDtStatus = false;
					hideWaiting();	
					return false;
				}
				chkStatus	= true;
				arrDetails += "{";
				arrDetails += '"payDate":"'+ payDate +'",' ;
				arrDetails += '"amount":"'+ amount +'"' ;
				arrDetails += "},";
			}
		});
		if(!chkDtStatus)
			return;
		
		if(!chkStatus)
		{
			$(this).validationEngine('showPrompt','No records to save.','fail');
			hideWaiting();	
			return;
		}
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		
		var arrHeader	= arrHeader;
		var arrDetails	= '['+arrDetails+']';
		data+="&arrHeader="+arrHeader+"&arrDetails="+arrDetails;
		
		var url = basePath+"paymentSchedule-db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmPaymentSchedule #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t = setTimeout("alertx()",3000);
						$('#frmPaymentSchedule #txtScheduleNo').val(json.scheduleNo);
						$('#frmPaymentSchedule #txtScheduleYear').val(json.scheduleYear);
						$('#frmPaymentSchedule #butConfirm').show();
						hideWaiting();
						return;
					}
					else
					{
						hideWaiting();
					}
				},
				error:function(xhr,status){
						
						$('#frmPaymentSchedule #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
				}		
		});
	}
	else
	{
		hideWaiting();
	}
}
function loadSettleCombo()
{
 	var url 	= basePath+"paymentSchedule-db.php?requestType=getSettleCombo";
	var data 	= "account="+$(this).val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmPaymentSchedule #cboSettleAccount').html(json.combo);
			}
	});		
}
function loadBalAmount()
{
 	var url 	= basePath+"paymentSchedule-db.php?requestType=loadBalAmount";
	var data 	= "settleAccount="+$(this).val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				var balAmount = (json.balAmount==null?0:json.balAmount);
				$('#frmPaymentSchedule #txtBalanceAmount').val(RoundNumber(balAmount,2));
			}
	});		
}
function Confirm()
{
	var url  = "?q="+reportId+"&scheduleNo="+$('#frmPaymentSchedule #txtScheduleNo').val();
	    url += "&scheduleYear="+$('#frmPaymentSchedule #txtScheduleYear').val();
	    url += "&mode=Confirm";
	window.open(url,'rptPaymentSchedule.php');
}
function Cancel()
{
	var url  =  "?q="+reportId+"&scheduleNo="+$('#frmPaymentSchedule #txtScheduleNo').val();
	    url += "&scheduleYear="+$('#frmPaymentSchedule #txtScheduleYear').val();
	    url += "&mode=Cancel";
	window.open(url,'rptPaymentSchedule.php');
}
function Report()
{
	if($('#frmPaymentSchedule #txtScheduleNo').val()=='')
	{
		$('#frmPaymentSchedule #butReport').validationEngine('showPrompt','No schedule no to view Report','fail');
		return;	
	}
	var url  =  "?q="+reportId+"&scheduleNo="+$('#frmPaymentSchedule #txtScheduleNo').val();
	    url += "&scheduleYear="+$('#frmPaymentSchedule #txtScheduleYear').val();
	window.open(url,'rptPaymentSchedule.php');
}
function clearAll()
{
	window.location.href = '?q='+menuId;
}
function alertx()
{
	$('#frmPaymentSchedule #butSave').validationEngine('hide')	;
}
<?php 
 session_start();
$backwardseperator 		= "../../../";
$userId 				= $_SESSION['userId'];
$locationId	  			= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];

$requestType 			= $_REQUEST['requestType'];
$programName			= 'Payment Schedule Approval';
$programCode			= 'P0761';

include_once "../../../dataAccess/Connector.php";
include_once "../../../class/finance/paymentSchedule/cls_schedule_set.php";
include_once "../../../class/finance/paymentSchedule/cls_schedule_get.php";

$obj_schedule_set		= new Cls_Schedule_Set($db);
$obj_schedule_get		= new Cls_Schedule_Get($db);

if($requestType=='saveData')
{
	$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
	$arrDetails 		= json_decode($_REQUEST['arrDetails'],true);
	
	echo $obj_schedule_set->save($arrHeader,$arrDetails,$programCode,$programName);
}
else if($requestType=='approve')
{
	$scheduleNo			= $_REQUEST["scheduleNo"];
	$scheduleYear		= $_REQUEST["scheduleYear"];
	
	$response			= $obj_schedule_get->validateBeforeApprove($scheduleNo,$scheduleYear,$programCode);
	
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	echo $obj_schedule_set->approve($scheduleNo,$scheduleYear);
}
else if($requestType=='reject')
{
	$scheduleNo			= $_REQUEST["scheduleNo"];
	$scheduleYear		= $_REQUEST["scheduleYear"];
	
	$response			= $obj_schedule_get->validateBeforeReject($scheduleNo,$scheduleYear,$programCode);
	 
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	echo $obj_schedule_set->reject($scheduleNo,$scheduleYear);
}
else if($requestType=='cancel')
{
	$scheduleNo			= $_REQUEST["scheduleNo"];
	$scheduleYear		= $_REQUEST["scheduleYear"];
	
	$response			= $obj_schedule_get->validateBeforeCancel($scheduleNo,$scheduleYear,$programCode);
	 
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	echo $obj_schedule_set->cancel($scheduleNo,$scheduleYear);
}
else if($requestType=='revise')
{
	$scheduleNo			= $_REQUEST["scheduleNo"];
	$scheduleYear		= $_REQUEST["scheduleYear"];
	
	$response			= $obj_schedule_get->validateBeforeRevise($scheduleNo,$scheduleYear,$programCode);
	 
	if($response["type"]=='fail')
	{
		echo json_encode($response);
		return;
	}
	echo $obj_schedule_set->revise($scheduleNo,$scheduleYear);
}
else if($requestType=='getSettleCombo')
{
	$account			= $_REQUEST['account'];
	$response['combo'] 	= $obj_schedule_get->getJECombo('','','',$account);

 	echo json_encode($response);
}
else if($requestType=='loadBalAmount')
{
	$settleAccount			= $_REQUEST['settleAccount'];
	$settelementArr			= explode("/", $settleAccount);
	$settelementYear		= $settelementArr[0];  
	$settelementNo			= $settelementArr[1];  
	$settelementOrder		= $settelementArr[2];
	
	$response['balAmount'] 	= $obj_schedule_get->getJESettlementBalance($settelementYear,$settelementNo,$settelementOrder);

 	echo json_encode($response);
}

?>
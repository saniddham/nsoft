<?php
//ini_set('display_errors',1);
session_start();
$session_userId	= 2;
include_once "../../../dataAccess/LoginDBManager.php";
//include_once "../../../dataAccess/DBManager.php";

$db 			= new LoginDBManager();
$currentDate	= date('Y-m-d');
$NewDate 		= date ("Y-m-d", strtotime("-1 day", strtotime($currentDate)));

	
	//$db->begin();
	$payResult	= getPaymentScheduleDetails($NewDate);
	while($row = mysqli_fetch_array($payResult))
	{
		if($row['AMOUNT']<=$row['BAL_AMOUNT'])
		{
			$result = ProcessTransaction($row['CREDIT_ACCOUNT_ID'],$row['AMOUNT'],$row['SCHEDULE_NO'],$row['SCHEDULE_YEAR'],'PAY_SCHEDULE','C','PS','null',$row['CURRENCY_ID'],'null','null',$row['COMPANY_ID'],$row['LOCATION_ID'],$session_userId,$NewDate);
	
			if($result["Saved_Status"])
				$result = ProcessTransaction($row['DEBIT_ACCOUNT_ID'],$row['AMOUNT'],$row['SCHEDULE_NO'],$row['SCHEDULE_YEAR'],'PAY_SCHEDULE','D','PS','null',$row['CURRENCY_ID'],'null','null',$row['COMPANY_ID'],$row['LOCATION_ID'],$session_userId,$NewDate);
	
			if($result["Saved_Status"])
				$result = UpdateDetails($NewDate);
			
			if($result["Saved_Status"])
				$result = UpdateJEBalAmount($row['JOURNAL_ENTRY_NO'],$row['JOURNAL_ENTRY_YEAR'],$row['ORDER_ID'],$row['AMOUNT']);
		}
	}
	
	if($result["Saved_Status"])
	{
		//$db->commit();
		$response['type'] 			= "pass";
		$response['msg'] 			= "Processed successfully.";
	}
	else
	{
		//$db->rollback();
		$response['type'] 			= "Processed fail.";
		$response['msg'] 			= $result["Error_Msg"];
		$response['ErrorSql'] 		= $Error_Sql;
	}
	print_r($response);

function getPaymentScheduleDetails($NewDate)
{
	global $db;
	
	$sql = "SELECT 
			SH.CREDIT_ACCOUNT_ID,
			SH.DEBIT_ACCOUNT_ID,
			SD.AMOUNT,
			SH.SCHEDULE_NO,
			SH.SCHEDULE_YEAR,
			SH.CURRENCY_ID,
			SH.LOCATION_ID,
			SH.COMPANY_ID,
			JD.BAL_AMOUNT,
			JD.JOURNAL_ENTRY_NO,
			JD.JOURNAL_ENTRY_YEAR,
			JD.ORDER_ID
			FROM finance_payment_schedule_header SH
			INNER JOIN finance_payment_schedule_details SD
			ON SD.SCHEDULE_NO = SH.SCHEDULE_NO AND SD.SCHEDULE_YEAR = SH.SCHEDULE_YEAR
			INNER JOIN finance_journal_entry_details JD ON JD.JOURNAL_ENTRY_NO=SH.SETTLEMENT_NO AND 
			JD.JOURNAL_ENTRY_YEAR=SH.SETTLEMENT_YEAR AND JD.ORDER_ID=SH.SETTLEMENT_ORDER
			WHERE SH.STATUS = 1 AND 
			SD.PAY_DATE = '$NewDate' ";

	return $db->RunQuery($sql);
}
function ProcessTransaction($accountId,$amount,$docNo,$docYear,$docType,$transacType,$transacCat,$transCatId,$currency,$bankRefNo,$remarks,$company,$location,$session_userId,$NewDate)
{
	global $db;
	
	$sql = "INSERT INTO finance_transaction 
			(
			CHART_OF_ACCOUNT_ID, 
			AMOUNT, 
			DOCUMENT_NO, 
			DOCUMENT_YEAR, 
			DOCUMENT_TYPE, 
			TRANSACTION_TYPE, 
			TRANSACTION_CATEGORY, 
			TRANSACTION_CATEGORY_ID, 
			CURRENCY_ID, 
			BANK_REFERENCE_NO, 
			REMARKS, 
			LOCATION_ID, 
			COMPANY_ID, 
			LAST_MODIFIED_BY, 
			LAST_MODIFIED_DATE
			)
			VALUES
			(
			'$accountId', 
			'$amount', 
			'$docNo', 
			'$docYear', 
			'$docType', 
			'$transacType', 
			'$transacCat', 
			$transCatId, 
			'$currency', 
			$bankRefNo, 
			$remarks, 
			'$location', 
			'$company', 
			'$session_userId', 
			'$NewDate'
			)";

	$result = $db->RunQuery($sql);
	if(!$result){
			$array["Saved_Status"]	= false;
			$array["Error_Msg"]		= $db->errormsg;
			$array["Error_Sql"]		= $sql;
	}
	else
	{
		$array["Saved_Status"]	= true;
	}
	return $array;
}

function UpdateDetails($NewDate)
{
	global $db;
	
	$sql = "UPDATE finance_payment_schedule_details SD
			  INNER JOIN finance_payment_schedule_header SH
				ON SD.SCHEDULE_NO = SH.SCHEDULE_NO
				  AND SD.SCHEDULE_YEAR = SH.SCHEDULE_YEAR
				SET SD.STATUS = 1,
				SD.PROCESS_DATE = NOW()
			WHERE SD.PAY_DATE = '$NewDate'
				AND SH.STATUS = 1";
	$result = $db->RunQuery($sql);
	if(!$result){
			$array["Saved_Status"]	= false;
			$array["Error_Msg"]		= $db->errormsg;
			$array["Error_Sql"]		= $sql;
	}
	else
	{
		$array["Saved_Status"]	= true;
	}
	return $array;
}
function UpdateJEBalAmount($JENo,$JEYear,$orderId,$amount)
{
	global $db;
	
	$sql = "UPDATE finance_journal_entry_details 
			SET
			BAL_AMOUNT = BAL_AMOUNT - $amount
			WHERE
			JOURNAL_ENTRY_NO = '$JENo' AND 
			JOURNAL_ENTRY_YEAR = '$JEYear' AND 
			ORDER_ID = '$orderId' ";
			
	$result = $db->RunQuery($sql);
	if(!$result){
			$array["Saved_Status"]	= false;
			$array["Error_Msg"]		= $db->errormsg;
			$array["Error_Sql"]		= $sql;
	}
	else
	{
		$array["Saved_Status"]	= true;
	}
	return $array;
}
?>
<?php
session_start();
ini_set('display_errors',0);
$backwardseperator 		= "../../../../";
$userId 				= $_SESSION['userId'];
$locationId				= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];
$HR_DB 					= $_SESSION['HRDatabase'];
$main_DB 				= $_SESSION['Database'];

$requestType 			= $_REQUEST['requestType'];
$programCode			= 'P0866';
$programName			= 'Petty Cash Request';
$savedMasseged			= "";
$error_sql				= "";
$savedStatus			= true;
$editMode				= false;

include_once "{$backwardseperator}dataAccess/Connector.php";
include_once "../../../../class/cls_commonFunctions_get.php";
require_once "../../../../class/cls_commonErrorHandeling_get.php";
include "../../../../class/finance/petty_cash/petty_cash_requisition/petty_cash_requisition_get.php";
include "../../../../class/finance/petty_cash/petty_cash_requisition/petty_cash_requisition_set.php";

$obj_petty_cash_requisition_set	= new cls_petty_cash_requisition_set($db);
$obj_petty_cash_requisition_get = new cls_petty_cash_requisition_get($db);
$obj_common						= new cls_commonFunctions_get($db);
$obj_commonErr					= new cls_commonErrorHandeling_get($db);

if($requestType == 'saveData')
{

	$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
	$arrDetail			= json_decode($_REQUEST['arrDetail'],true);
	//print_r($arrDetail);
	$db->begin();
	
	$requestNo			= $arrHeader['requestNo'];
	$requestYear		= $arrHeader['requestYear'];
	$locationID			= $arrHeader['locationID'];
	$remark				= $obj_common->replace($arrHeader["remark"]);
	$total				= $arrHeader['total'];	
	$date				= date('Y-m-d');
	//die($requestNo);
	$validateArr		= validateBeforeSave($requestNo,$requestYear,$programCode,$userId);
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateArr["msg"];
	}
	if($requestNo =='' && $requestYear =='')
	{
		$sysNo_arry 		= $obj_common->GetSystemMaxNo('PETTY_CASH_REQUISITION',$locationId);
		
		if($sysNo_arry["rollBackFlag"]==1 && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $sysNo_arry["msg"];
			$error_sql		= $sysNo_arry["q"];	
		}
		
		$requestNo			= $sysNo_arry["max_no"];
		$requestYear		= date('Y');
		$approveLevels 		= $obj_common->getApproveLevels($programName);
		$status				= $approveLevels+1;
		
		$headerArr				= $obj_petty_cash_requisition_set->saveHeader($requestNo,$requestYear,$total,$remark,$date,$status,$approveLevels,$companyId,$locationId,$userId,'RunQuery2');
		if($headerArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $headerArr['savedMassege'];
			$error_sql		= $headerArr['error_sql'];	
		}
		foreach($arrDetail as $arrDetail)
		{
			$invoiceNo		= $arrDetail["invoiceNo"];
			$invoiceYear	= $arrDetail["invoiceYear"];
			$itemID			= $arrDetail["itemID"];
			$itemNameArr	= $obj_petty_cash_requisition_get->getItemName($itemID,'RunQuery2');
			//die($itemNameArr['NAME']);
			$itemArr		= $obj_petty_cash_requisition_get->getItemStatus($invoiceNo,$invoiceYear,$locationId,'RunQuery2');
			$row		    = mysqli_fetch_array($itemArr);
			if($row['REQUEST_STATUS'] == 1)
			{
				$savedStatus	= false;
				$savedMasseged	= "Alredy request petty cash for ".$itemNameArr['NAME'];
				$error_sql		= '';
			}
			
			$detailArr		= $obj_petty_cash_requisition_set->saveDetails($requestNo,$requestYear,$invoiceNo,$invoiceYear,'RunQuery2');
			if($detailArr['savedStatus'] == 'fail' && $savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $detailArr['savedMassege'];
				$error_sql		= $detailArr['error_sql'];	
			}
			
		}
	}
	else
	{
		$header_array 		= $obj_petty_cash_requisition_get->get_header($requestNo,$requestYear,'RunQuery2');
		$status				= $header_array['STATUS'];
		$approveLevels		= $header_array['LEVELS'];
		$approveLevels 		= $obj_common->getApproveLevels($programName);
		$status				= $approveLevels+1;
		
		$updateArr		= $obj_petty_cash_requisition_set->updateHeader($requestNo,$requestYear,$total,$remark,$userId,$status,$approveLevels,'RunQuery2');
		if($updateArr['savedStatus'] == 'fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $updateArr['savedMassege'];
			$error_sql		= $updateArr['error_sql'];	
		}
		$resultUHArr		= updateMaxStatus($requestNo,$requestYear);
		if($resultUHArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultUHArr['savedMassege'];
			$error_sql		= $resultUHArr['error_sql'];	
		}
		$deleteArr		= $obj_petty_cash_requisition_set->deleteDetails($requestNo,$requestYear,'RunQuery2');
		if($deleteArr['savedStatus'] == 'fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $deleteArr['savedMassege'];
			$error_sql		= $deleteArr['error_sql'];	
		}
		foreach($arrDetail as $arrDetail)
		{
			$invoiceNo		= $arrDetail["invoiceNo"];
			$invoiceYear	= $arrDetail["invoiceYear"];
			$itemID			= $arrDetail["itemID"];
			$itemNameArr	= $obj_petty_cash_requisition_get->getItemName($itemID,'RunQuery2');
			$itemArr		= $obj_petty_cash_requisition_get->getItemStatus($invoiceNo,$invoiceYear,$itemID,'RunQuery2');
			$row		    = mysqli_fetch_array($itemArr);
			if($row['REQUEST_STATUS'] == 1)
			{
				$savedStatus	= false;
				$savedMasseged	= "Can't Update.Alredy request petty cash for ".$itemNameArr['NAME'];
				$error_sql		= '';
			}
			
			$saveArr	= $obj_petty_cash_requisition_set->saveDetails($requestNo,$requestYear,$invoiceNo,$invoiceYear,'RunQuery2');
			if($saveArr['savedStatus'] == 'fail' && $savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $saveArr['savedMassege'];
				$error_sql		= $saveArr['error_sql'];	
			}
			
		}
		$editMode		= true;
	}
	
	$resultArr	= $obj_common->validateDuplicateSerialNoWithSysNo($requestNo,'PETTY_CASH_REQUISITION',$locationId);
	if($resultArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged	= $resultArr['msg'];
	}
	
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 			= "pass";
		if($editMode)
			$response['msg'] 		= "Updated Successfully.";
		else
		$response['msg'] 		= "Saved Successfully.";
		$response['requestNo']		= $requestNo;
		$response['requestYear']	= $requestYear;
	}
	else
	{
		$db->rollback();
		$response['type'] 			= "fail";
		$response['msg'] 			= $savedMasseged;
		$response['sql']			= $error_sql;
	}
	echo json_encode($response);
}
else if($requestType=='approve')
{
	$requestNo				= $_REQUEST['requestNo'];
	$requestYear			= $_REQUEST['requestYear'];
	
	$db->begin();
	
	$header_array			= $obj_petty_cash_requisition_get->get_header($requestNo,$requestYear,'RunQuery2');
	$itemResult				= $obj_petty_cash_requisition_get->getInvoiceItems($requestNo,$requestYear,'RunQuery2');
	$permition_arr			= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['APPROVE_LEVELS'],$userId,$programCode,'RunQuery2');
	$permision_confirm		= $permition_arr['permision']; 
	
	if($permition_arr['type']=='fail' && $savedStatus)
	{
		$savedStatus		= false;
		$savedMasseged	 	= $permition_arr['msg'];	
	}
	while($row1 = mysqli_fetch_array($itemResult))
	{				
		$status_array	= $obj_petty_cash_requisition_get->getItemStatus($row1['INVOICE_NO'],$row1['INVOICE_YEAR'],$locationId,'RunQuery2');
		$row		    = mysqli_fetch_array($status_array);
		if($row['REQUEST_STATUS']==1 && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= 'Already request petty cash for '.$row1['NAME'];	
		}	
	}		

	$status			= $header_array['STATUS']-1;
	$resultUHSArr	= $obj_petty_cash_requisition_set->update_header_status($requestNo,$requestYear,$status,'RunQuery2');
	if($resultUHSArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged	= $resultUHSArr['msg'];
		$error_sql		= $resultUHSArr['sql'];
	}	
	
	$header_array		= $obj_petty_cash_requisition_get->get_header($requestNo,$requestYear,'RunQuery2');
	$levels				= $header_array['APPROVE_LEVELS']+1-$header_array['STATUS'];
	$resultUABArr		= $obj_petty_cash_requisition_set->approved_by_insert($requestNo,$requestYear,$userId,$levels,'RunQuery2');
	if($resultUABArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged	= $resultUABArr['msg'];
		$error_sql		= $resultUABArr['sql'];
	}
	
	
	if($header_array['STATUS']==1)
	{
		$itemResult	= $obj_petty_cash_requisition_get->getInvoiceItems($requestNo,$requestYear,'RunQuery2');
		while($row = mysqli_fetch_array($itemResult))
		{
			$updateInvArr = $obj_petty_cash_requisition_set->updateInvoice($row['INVOICE_NO'],$row['INVOICE_YEAR'],$locationId,1,'RunQuery2');
			if($updateInvArr['savedStatus']=='fail' && $savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $updateInvArr['savedMassege'];
				$error_sql		= $updateInvArr['error_sql'];	
			}
		}
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Approved Successfully.";

	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql']		= $error_sql;
	}
	echo json_encode($response);		

}
else if($requestType=='reject'){
		
		$rollBack_flag 			= false;
		$rollBack_msg			= '';
		$rollBack_sql			= '';
		
		$requestNo						= $_REQUEST['requestNo'];
		$requestYear					= $_REQUEST['requestYear'];
				
		$db->begin();
	
		$header_array			= $obj_petty_cash_requisition_get->get_header($requestNo,$requestYear,'RunQuery2');
		$permition_arr			= $obj_commonErr->get_permision_withApproval_reject($header_array['STATUS'],$header_array['APPROVE_LEVELS'],$userId,$programCode,'RunQuery2');
		$permision_confirm		= $permition_arr['permision']; 
		//--------------------------
		//$permition_arr['type'];
		if($permition_arr['type']  == 'fail'){
			$rollBack_flag		=	true;
			$rollBack_msg	 	=	$permition_arr['msg'];	
			$rollBack_sql		=	'';
 		}
		
		if($rollBack_flag != true){
		//1. update header status
			$status				= 0;
			$response_1			= $obj_petty_cash_requisition_set->update_header_status($requestNo,$requestYear,$status,'RunQuery2');
			if($response_1['type']	 == 'fail'){
				$rollBack_flag	=true;
				$rollBack_msg	=$response_1['msg'];
				$rollBack_sql	=$response_1['sql'];
			}
		}
			
		if($rollBack_flag != true){
		//2. approve by insert
			if($rollBack_flag != 1){
				$response_2		= $obj_petty_cash_requisition_set->approved_by_insert($requestNo,$requestYear,$userId,0,'RunQuery2');
				if($response_2['type']	 == 'fail'){
					$rollBack_flag		=true;
					$rollBack_msg		.=$response_2['msg'];
					$rollBack_sql		.=$response_2['sql'];
				}
			}
		}
		if($rollBack_flag==true){
			$db->rollback();
			$response['type']	= 'fail';
			$response['msg']	= $rollBack_msg;
			$response['q']		= $rollBack_sql;
		}
		else if($toSave==$saved){
			$header_array		= $obj_petty_cash_requisition_get->get_header($requestNo,$requestYear,'RunQuery2');
			$db->commit();
			$response['type']	= 'pass';
			if($header_array['STATUS']==0)
			$response['msg']	= 'Rejected successfully.';
			else
			$response['msg']	= 'Rejection Failed.';
		}
		else{
			$db->rollback();
			$response['type']	= 'fail';
			$response['msg']	= $db->errormsg;
			$response['q']		= $sql;
		}
	
		$db->CloseConnection();		
		echo json_encode($response);
	
	}
else if($requestType == 'loadItems')
{
	$toDate  	= $_REQUEST['toDate'];
	$fromDate  	= $_REQUEST['fromDate'];
	$itemsArr	= $obj_petty_cash_requisition_get->getInvoice($toDate,$fromDate,$locationId,'RunQuery');
	$content	= '';
	$response['gridDetail']	= '';
	while($row=mysqli_fetch_array($itemsArr))
	{
		$invoiveNo		= $row['SERIAL_NO'];
		$serialNo		= $row['CON_SERIAL_NO'];
		$invoiceYear	= $row['SERIAL_YEAR'];
		$fileName		= $row['FILE_NAME'];
        $itemName		= $row['ITEM'];    
		$item			= $row['PETTY_CASH_ITEM']; 
		$amount			= abs($row['AMOUNT']);
		$formatAmt		= number_format($amount,2);
		$folderName 	= $row['FILE_NAME'];
		$file_path_srting	=filesInDir("../../../../documents/finance/petty_cash/$folderName");
		
                	
		$content .= "<tr id=\"".$invoiveNo."\" class=\"normalfnt\"><td align=\"center\" bgcolor=\"#FFFFFF\" id=\"".$invoiceYear."\"><input type=\"checkbox\" class=\"chkInvoice\" id=\"chkInvoice\" /></td>";
		$content .="<td align=\"center\" bgcolor=\"#FFFFFF\" id=\"".$fileName."\" class=\"cls_td_invoiceNo\">".$serialNo."</td>";
									
		$content .="<td align=\"left\" bgcolor=\"#FFFFFF\" id=\"".$item."\" class=\"cls_td_item\">".$itemName."</td>";
		$content .="<td align=\"right\" bgcolor=\"#FFFFFF\" id=\"".$amount."\" class=\"cls_td_amount\">".$formatAmt."</td>";
        $content .="<td align=\"center\" bgcolor=\"#FFFFFF\" class=\"cls_td_path\" hidden=\"true\">".$file_path_srting."&nbsp;</td></tr>";
	}
	$response['gridDetail'] = $content;
	echo json_encode($response);
}

function updateMaxStatus($requestNo,$requestYear)
{
	global $obj_petty_cash_requisition_get;
	global $obj_petty_cash_requisition_set;
	
	$maxStatus		= $obj_petty_cash_requisition_get->getMaxStatus($requestNo,$requestYear);
	$resultArr		= $obj_petty_cash_requisition_set->updateApproveByStatus($requestNo,$requestYear,$maxStatus);
	
	return $resultArr;
}

function filesInDir($tdir)
{
	//die($tdir);
	global $folderName;
	$m		= 0;
	$dirs 	= scandir($tdir);
	$html	= '';
	
	foreach($dirs as $file)
	{
		
		if (($file == '.')||($file == '..'))
		{
		}
		else
		{
			
			if (! is_dir($tdir.'/'.$file))
			{
			
			$html	.= "<a target=\"_blank\" href=\"../../../../documents/finance/petty_cash/".$folderName."/$file\">$file</a>";
			}
		}
	}	
	return $html;
}

function validateBeforeSave($requestNo,$requestYear,$programCode,$userId)
{
	global $obj_commonErr;
	global $obj_petty_cash_requisition_get;
	global $main_DB;
	global $HR_DB;

	$header_arr		= $obj_petty_cash_requisition_get->get_header($requestNo,$requestYear,'RunQuery2');
	$validateArr 	= $obj_commonErr->get_permision_withApproval_save($header_arr['STATUS'],$header_arr['LEVELS'],$userId,$programCode,'RunQuery2');
	
	return  $validateArr;
}
?>
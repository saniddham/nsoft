<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId						= $_SESSION["CompanyID"];
$companyId						= $_SESSION["headCompanyId"];
$userId							= $_SESSION["userId"];
$HR_DB 							= $_SESSION['HRDatabase'];
$main_DB 						= $_SESSION['Database'];
$programCode					= 'P0866';

include_once "class/cls_commonErrorHandeling_get.php";
include 	 "class/cls_commonFunctions_get.php";
include 	 "class/finance/petty_cash/petty_cash_requisition/petty_cash_requisition_get.php";
//include 	 "include/javascript.html";

$obj_common_func				= new cls_commonFunctions_get($db);
$obj_commonErrHandle			= new cls_commonErrorHandeling_get($db);
$locationArr					= $obj_common_func->loadLocation($companyId,'RunQuery');
$obj_petty_cash_requisition_get	= new cls_petty_cash_requisition_get($db);

$requestNo						= (!isset($_REQUEST['requestNo'])?'':$_REQUEST['requestNo']);
$requestYear					= (!isset($_REQUEST['requestYear'])?'':$_REQUEST['requestYear']);

$header_arr						= $obj_petty_cash_requisition_get->get_header($requestNo,$requestYear,'RunQuery');

$detail_arr						= $obj_petty_cash_requisition_get->getDetails($requestNo,$requestYear,$main_DB,$HR_DB,'RunQuery');
$intStatus						= $header_arr['STATUS'];
$levels							= $header_arr['APPROVE_LEVELS'];

$permision_arr					= $obj_commonErrHandle->get_permision_withApproval_save($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_save					= $permision_arr['permision'];

$permision_arr					= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_confirm				= $permision_arr['permision'];

?>
<title>Petty Cash Requisition</title>

<!--<script type="text/javascript" src="presentation/finance_new/petty_cash/petty_cash_requisition/petty_cash_requsition_js.js" ></script>-->

<form id="frmPettyCashRequest" name="frmPettyCashRequest" method="post">
<div align="center">
	<div class="trans_layoutD">
	<div class="trans_text">Petty Cash Reimbursement Requisition</div>
    <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <tr class="normalfnt">
    <td><table width="100%" border="0" class="normalfnt">
        <tr >
        	<td width="16%">Requisition No</td>
            <td width="35%"><input type="text" disabled="disabled" id="txtRequisitionNo" name="txtRequisitionNo" style="width:100px" value="<?php echo $requestNo?>" /> <input type="text" disabled="disabled" id="txtRequisitionYear" name="txtRequisitionYear" style="width:50px" value="<?php echo $requestYear ?>" /></td>
            <td width="15%">Location</td>
            <td width="29%"><select id="cboLocation" disabled="disabled" name="cboLocation" style="width:150px">
            <option value=""></option>
            <?php
			while ($row = mysqli_fetch_array($locationArr))
			{
				if($locationId == $row['intId'])
					echo "<option selected=\"selected\" value=\"".$row['intId']."\">".$row['strName']."</option>";	
			}
			?>
            </select> </td>
        </tr>
        <tr>
        <td valign="top">Remark</td>
        <td><textarea id="txtRemark" name="txtRemark"><?php echo $header_arr['REMARKS']; ?></textarea></td>
        </tr>
       </table></td>
    </tr>
    <tr><td style="text-align:right"><a class="button green small" id="butAdd" name="butAdd">Add</a></td></tr>
     <tr>
        <td><table id="tblMain" width="100%" class="bordered">
        <thead>
        <tr>
        <th width="7%">Del</th>
        <th width="28%">Invoice No</th>
        <th width="25%">Item</th>
        <th width="21%">Amount</th>
        <th width="19%">Bill</th>
        </tr>
        </thead>
       	<tbody id="tblBody">
        <?php
		while($row = mysqli_fetch_array($detail_arr))
		{
		?>
        <tr class = "td_invoiceNo" id="<?php echo $row['INVOICE_NO']?>">
        <td align="center" style="text-align:center"><img class="mouseover removeRow" src="images/del.png" /></td>							        <td class="cls_td_invoice" style="text-align:center" id="<?php echo $row['INVOICE_YEAR'] ?>"><?php echo $row['CON_INVOICE_NO'] ?></td>
		<td class="cls_td_item" id="item"><?php echo $row['ITEM']?></td>
		<td class ="cls_td_amt" id="<?php echo abs($row['AMOUNT'])?>" style="text-align:right"><?php echo abs($row['AMOUNT'])?></td>					        <td style="text-align:center"><?php $folderName = $row['FILE_PATH'];filesInDir("documents/finance/petty_cash/$folderName");
        	?>&nbsp </td>
					
        </tr>
        <?php
		}?>
        </tbody>
        </table></td>
        </tr>
        <tr>
        <td><table width="100%">
        <tr>
        <td width="20%" class="normalfnt" style="font-weight:bold">Total Amount: </td>
        <td width="40%" class="normalfnt" style="font-weight:bold" ><div id="totAmt">
		<?php echo ($header_arr['TOTAL_AMOUNT']==''?0:$header_arr['TOTAL_AMOUNT']) ?></div></td>
        <td width="40%" class="normalfnt" style="font-weight:bold">&nbsp;</td>
        
        </tr>
        </table>
        </td>
         </tr>
         <tr>
          <td width="100%" align="center" ><a class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butSave" name="butSave" <?php if($permision_save!=1){ ?>style="display:none"<?php } ?>>Save</a><a class="button white medium" id="butApprove" name="butApprove" <?php if($permision_confirm!=1){ ?> style="display:none"<?php } ?>>Approve</a><a class="button white medium" id="butReport" name="butReport">Report</a><a  class="button white medium" id="butClosePop" name="butClosePop" href="main.php">Close</a></td>
        </tr>
    </table>
    </div>
    </div>
    </form>

   <div style="width:500px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>

<?php
function filesInDir($tdir)
{
	//die($tdir);
	global $folderName;
	$m		= 0;
	$dirs 	= scandir($tdir);
	$html	= '';
	
	foreach($dirs as $file)
	{
		
		if (($file == '.')||($file == '..'))
		{
		}
		else
		{
			
			if (! is_dir($tdir.'/'.$file))
			{
			
			$html	.= "<a target=\"_blank\" href=\"documents/finance/petty_cash/".$folderName."/$file\">$file</a>";
			}
		}
	}	
	echo $html;
}
?>
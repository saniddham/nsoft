<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php

	$companyId 						= $_SESSION['headCompanyId'];
	$locationId						= $_SESSION['CompanyID'];
	$session_userId					= $_SESSION["userId"];
	$intUser						= $_SESSION["userId"];
	$HR_DB 							= $_SESSION['HRDatabase'];
	$main_DB 						= $_SESSION['Database'];
	
 	require_once 			"class/cls_commonFunctions_get.php";
	require_once 			"class/cls_commonErrorHandeling_get.php";
	require_once 			"class/finance/petty_cash/petty_cash_requisition/petty_cash_requisition_get.php";

	$obj_common						= new cls_commonFunctions_get($db);
	$obj_commonErr					= new cls_commonErrorHandeling_get($db);
	$obj_petty_cash_requisition_get	= new cls_petty_cash_requisition_get($db);
	$programName					='Petty Cash Request';
	$programCode					='P0866';
	 
	$requestNo						= $_REQUEST['requestNo'];
	$requestYear					= $_REQUEST['requestYear'];
	$mode							= (!isset($_REQUEST['mode'])?'':$_REQUEST['mode']);
	 //die($requestNo);
	$header_array					= $obj_petty_cash_requisition_get->get_header($requestNo,$requestYear,'RunQuery');
 	$detailResult					= $obj_petty_cash_requisition_get->getDetails($requestNo,$requestYear,$main_DB,$HR_DB,'RunQuery');
	$permition_arr					= $obj_commonErr->get_permision_withApproval_save($header_array['STATUS'],$header_array['APPROVE_LEVELS'],$intUser,$programCode,'RunQuery');
	$permision_save					= $permition_arr['permision'];
	$permition_arr					= $obj_commonErr->get_permision_withApproval_reject($header_array['STATUS'],$header_array['APPROVE_LEVELS'],$intUser,$programCode,'RunQuery');
	$permision_reject				= $permition_arr['permision'];
	$permition_arr					= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['APPROVE_LEVELS'],$intUser,$programCode,'RunQuery');
	$permision_confirm				= $permition_arr['permision'];
	
  ?>
<head>
<title>Petty Cash Invoice Report</title>

<script type="text/javascript" src="presentation/finance_new/petty_cash/petty_cash_requisition/petty_cash_requsition_js.js"></script>

 </head>
<body>
<style type="text/css">
#apDiv1 {
	position: absolute;
	left: 468px;
	top: 130px;
	width: 650px;
	height: 322px;
	z-index: 1;
}
</style>
<?php
if($header_array['STATUS'] != 1)
{
?>
<div id="apDiv1" ><img src="images/invalid2.png" style="opacity:0.3" /></div>
<?php
}
?>

 <form id="frmRptPettyCashReq" name="frmRptPettyCashReq" method="post">
  <table width="1014" align="center">
    <tr>
      <td width="1006"><?php include 'reportHeader.php'?></td>
    </tr>
    <tr>
      <td class="reportHeader" align="center">Petty Cash  Requisition Report</td>
    </tr>
	<?php
		include "presentation/report_approve_status_and_buttons.php"
     ?>
    <tr>
        <td>
        <table width="100%" border="0" class="normalfnt">
            <tr>
            <td width="15%" height="21">Requisition No</td>
            <td width="2%">:</td>
            <td width="29%"><?php echo $header_array['CON_REQUISITION_NO']?></td>
            <td width="20%">Request Date</td>
            <td width="2%">:</td>
            <td width="32%"><?php echo $header_array['REQUEST_DATE']?></td>
            </tr>
            <tr>
                <td height="20">Location</td>
                <td>:</td>
                <td ><?php echo $header_array['LOCATION'] ?></td>
                <td>Requested By</td>
                <td>:</td>
                <td ><?php echo $header_array['USER'] ?></td>
            </tr>
            <tr>
                <td height="20">Total Amount</td>
                <td>:</td>
                <td ><?php echo $header_array['TOTAL_AMOUNT'] ?></td>
                <td>Remarks</td>
                <td>:</td>
                <td rowspan="2" valign="top" ><?php echo $header_array['REMARKS'] ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td style="font-weight:bold">&nbsp;</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td >&nbsp;</td>
            </tr>
  
		</table>
        </td>
    </tr>
    <tr>
      <td >
        <table width="100%" id="tblData" class="bordered" border="0" >
          <thead>
            <tr class="normalfnt">
              <th width="14%" >Invoice No</th>
              <th width="12%" >Invoice Date</th>
              <th width="25%" >Item</th>
              <th width="24%" >Used By</th>
              <th width="16%" >Amount</th>
              <th width="9%" >Bill</th>
            </tr>
            </thead>
            <tbody id="tblBody" >
            <?php 
				while($row = mysqli_fetch_array($detailResult))
				{
			?>
            <tr class="normalfnt">
              <td style="text-align:center" class="td_invoiceNo" id="<?php echo $row['INVOICE_NO'] ?>"><?php echo $row['CON_INVOICE_NO'] ?></td>
              <td style="text-align:center" class="td_invoiceYear" id="<?php echo $row['INVOICE_YEAR'] ?>"><?php echo $row['DATE'] ?></td>
              <td class="td_item" id="<?php echo $row['PETTY_CASH_ITEM'] ?>" ><?php echo $row['ITEM'] ?></td>
              <td ><?php echo $row['USEDBY'] ?></td>
              <td style="text-align:right" ><?php echo number_format(abs($row['AMOUNT']),2) ?></td>
              <td style="text-align:center" ><?php $folderName = $row['FILE_PATH']; filesInDir("documents/finance/petty_cash/$folderName");
        	?></td>
             </tr>
              <?php 
				}?>
           </tbody>
          </table>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>
      <?php
			$creator		= $header_array['USER'];
			$createdDate	= $header_array['CREATED_DATE'];
 			$resultA 		= $obj_petty_cash_requisition_get->get_report_approval_details_result($requestNo,$requestYear,'RunQuery');
			include "presentation/report_approvedBy_details.php"
 	?>
    </td>
    </tr>
    <tr>
    <td align="center" class="normalfntMid"><strong>Printed Date : </strong><?php echo date('Y-m-d H:i:s');?></td>
    </tr>
  </table>
</form>
</body>
</html>

<?php


function filesInDir($tdir)
{
	global $folderName;
	$m		= 0;
	$dirs 	= scandir($tdir);
	$html	= '';
	
	foreach($dirs as $file)
	{
		
		if (($file == '.')||($file == '..'))
		{
		}
		else
		{
			
			if (! is_dir($tdir.'/'.$file))
			{
			
			$html	.= "<a target=\"_blank\" href=\"documents/finance/petty_cash/".$folderName."/$file\">$file</a>";
			}
		}
	}
	echo $html;
}


?>
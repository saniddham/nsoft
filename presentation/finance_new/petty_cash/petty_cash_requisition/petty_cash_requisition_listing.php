<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once "libraries/jqgrid2/inc/jqgrid_dist.php";

$company 				= $_SESSION['headCompanyId'];
$location   			= $_SESSION['CompanyID'];
$intUser  				= $_SESSION["userId"];
$HR_DB 					= $_SESSION['HRDatabase'];
$main_DB 				= $_SESSION['Database'];

$menuId					= 866;
$reportId				= 923;

$programName			= 'Petty Cash Requitition';
$programCode			= 'P0866';

$max_approveLevel 		= approveLevel();

// {
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'STATUS'=>'H.STATUS',
				'CONCAT_REQUISITION_NO'=>"CONCAT(REQUISITION_NO,'/',REQUISITION_YEAR)",
				'REMARKS'=>'REMARKS',
				'CREATED_USER'=>'U.strUserName',
				'CREATED_DATE'=>'H.CREATED_DATE'
				);
				
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='STATUS')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND DATE(H.CREATED_DATE) = '".date('Y-m-d')."'";
	
// }

$sql					= getSql($max_approveLevel,$location,$where_string);//$obj_petty_cash_inv_get->get_listing_sql($programCode,$max_approveLevel,$intUser,$main_DB,$HR_DB,$location);
						 
$col 	= array();
$cols 	= array();

//BEGIN - STATUS {
$col["title"] 			= "Status";
$col["name"] 			= "STATUS";
$col["width"] 			= "1";
$col["stype"] 			= "select";
$str 					= ":All;Pending:Pending;Approved:Approved;Rejected:Rejected";
$col["editoptions"] 	= array("value"=> $str);
$col["align"] 			= "center";
$cols[] 				= $col;	
$col					= NULL;
//END 	- STATUS }
$col["title"] 			= "Requisition No";
$col["name"] 			= "CONCAT_REQUISITION_NO"; 
$col["width"] 			= "2";
$col["align"] 			= "center";
$col['link']			= "?q=".$menuId."&requestNo={REQUISITION_NO}&requestYear={REQUISITION_YEAR}";	 
$col["linkoptions"] 	= "target='_blank'";
$cols[] 				= $col;	
$col					= NULL;

$formLink  				= "?q=".$menuId."&requestNo={REQUISITION_NO}&requestYear={REQUISITION_YEAR}";
$reportLink  			= "?q=".$reportId."&requestNo={REQUISITION_NO}&requestYear={REQUISITION_YEAR}";
$reportLinkApprove 		= "?q=".$reportId."&requestNo={REQUISITION_NO}&requestYear={REQUISITION_YEAR}&mode=Confirm";

$col["title"]			= "Serial No";
$col["name"]			= "REQUISITION_NO";
$col["width"]			= "1";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$col["hidden"]			= true;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Serial Year";
$col["name"]			= "REQUISITION_YEAR";
$col["width"]			= "1";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$col["hidden"]			= true;
$cols[]			    	= $col;
$col					= NULL;

$col["title"] 			= "Remarks";
$col["name"] 			= "REMARKS";
$col["width"] 			= "4";
$col["align"] 			= "left";
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Amount";
$col["name"] 			= "AMOUNT";
$col["width"] 			= "2";
$col["align"] 			= "right";
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Created By";
$col["name"] 			= "CREATED_USER";
$col["width"] 			= "2";
$col["align"] 			= "left";
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Created Date";
$col["name"] 			= "CREATED_DATE"; 
$col["width"] 			= "2";
$col["align"] 			= "center";
$cols[] 				= $col;	
$col					= NULL;
 
//FIRST APPROVAL
$col["title"] 			= "1st Approval";
$col["name"] 			= "1st_Approval";
$col["width"] 			= "3";
$col["search"]			= false;
$col["align"] 			= "center";
$col['link']			= $reportLinkApprove;
$col['linkName']		= 'Approve';
$col["linkoptions"] 	= "target='_blank'";
$cols[] 				= $col;	
$col					= NULL;

for($i=2; $i<=$max_approveLevel; $i++){
	if($i==2){
	$ap		= "2nd Approval";
	$ap1	= "2nd_Approval";
	}
	else if($i==3){
	$ap		= "3rd Approval";
	$ap1	= "3rd_Approval";
	}
	else {
	$ap		= $i."th Approval";
	$ap1	= $i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] 			= $ap;
$col["name"] 			= $ap1;
$col["width"] 			= "3";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= $reportLinkApprove;
$col['linkName']		= 'Approve';
$col["linkoptions"] 	= "target = '_blank'";
$cols[] 				= $col;	
$col					= NULL;
}

$col["title"] 			= "Report";
$col["name"] 			= "VIEW";
$col["width"] 			= "1";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= $reportLink;
$col["linkoptions"]		= "target = '_blank'";
$cols[] 				= $col;	
$col					= NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Petty Cash Requisition Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20;
$grid["sortname"] 		= 'REQUEST_DATE';
$grid["sortorder"] 		= "DESC";
$grid["autowidth"] 		= true;
$grid["multiselect"] 	= false;

$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"STATUS","op":"eq","data":"Pending"}
     ]
}
SEARCH_JSON;
$grid["search"] = true; 
//$grid["postData"] = array("filters" => $sarr ); 

$jq->set_options($grid);
$jq->select_command = $sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Petty Cash Requisition Listing</title>
<?php
echo $out;

function getSql($approveLevel,$location,$where_string)
{
	global $db;
	global $programCode;
	global $intUser;
	
	$sql = "SELECT * FROM(
			SELECT
				REQUISITION_NO								AS REQUISITION_NO,
				REQUISITION_YEAR							AS REQUISITION_YEAR,
				CONCAT(REQUISITION_NO,'/',REQUISITION_YEAR)	AS CONCAT_REQUISITION_NO,
				REMARKS										AS REMARKS,
				REQUEST_DATE								AS REQUEST_DATE,
				if(H.STATUS=1,'Approved',if(H.STATUS=0,'Rejected','Pending')) AS STATUS,
				TOTAL_AMOUNT								AS AMOUNT,
				U.strUserName								AS CREATED_USER,
				H.CREATED_DATE								AS CREATED_DATE, ";
	$sql .= "IFNULL((
				SELECT
					concat(U.strUserName,'(',max(A.APPROVED_DATE),')' )
				FROM
					finance_pettycash_requisition_approvedby A
				INNER JOIN sys_users U 
					ON A.APPROVED_BY = U.intUserId
				WHERE
					A.REQUISITION_NO = H.REQUISITION_NO AND
					A.REQUISITION_YEAR = H.REQUISITION_YEAR AND
					A.APPROVE_LEVEL = '1' AND 
					A.STATUS = '0' AND 
					H.STATUS >0
				),IF(((SELECT
					MP.int1Approval 
				FROM menupermision MP
				Inner Join menus M ON MP.intMenuId = M.intId
				WHERE
					M.strCode = '$programCode' AND
					MP.intUserId =  '$intUser')=1 AND H.STATUS>1),'Approve', '')) as `1st_Approval`,";
					
	for($i=2;$i<=$approveLevel;$i++)
	{
		if($i==2){
			$approval	= "2nd_Approval";
		}
		else if($i==3){
			$approval	= "3rd_Approval";
		}
		else {
			$approval	= $i."th_Approval";
		}
			
		$sql .= "IFNULL((SELECT
					CONCAT(U.strUserName,'(',max(A.APPROVED_DATE),')' )
				FROM
					finance_pettycash_requisition_approvedby A
				INNER JOIN sys_users U
					ON A.APPROVED_BY = U.intUserId
				WHERE
					A.REQUISITION_NO = H.REQUISITION_NO AND
					A.REQUISITION_YEAR = H.REQUISITION_YEAR AND
					A.APPROVE_LEVEL = '$i' AND 
					A.STATUS = '0' 
			),
			IF(
			((SELECT
				MP.int".$i."Approval 
				FROM menupermision MP
				INNER JOIN menus M 
					ON MP.intMenuId = M.intId
				WHERE
					M.strCode = '$programCode' AND
					MP.intUserId = '$intUser')=1 AND 
					(H.STATUS>1) AND 
					(H.STATUS <= H.APPROVE_LEVELS) AND 
				((SELECT
					max(A.APPROVED_DATE) 
				FROM
				finance_pettycash_requisition_approvedby A
				INNER JOIN sys_users U 
					ON A.APPROVED_BY = U.intUserId
				WHERE
					A.REQUISITION_NO = H.REQUISITION_NO AND
					A.REQUISITION_YEAR = H.REQUISITION_YEAR AND
					A.APPROVE_LEVEL = ($i-1) AND 
					A.STATUS = '0' )<>'')),'Approve',if($i>H.APPROVE_LEVELS,'-----',''))) AS `".$approval."`, ";		
	}	
				
	$sql .="'view'										AS VIEW
			FROM 
				finance_pettycash_requisition_header H
			INNER JOIN sys_users U 
				ON U.intUserId = H.CREATED_BY
			WHERE H.LOCATION_ID = $location
			$where_string)
			AS T WHERE 1=1";
	//echo $sql;
	return $sql;
}

function approveLevel()
{
	global $db;
	
	$sql = "SELECT
				Max(H.APPROVE_LEVELS) AS LEVELS
			FROM `finance_pettycash_requisition_header` H";
	$result = $db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	return $row["LEVELS"];
}
?>
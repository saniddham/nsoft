// JavaScript Document
var basePath	= "presentation/finance_new/petty_cash/petty_cash_requisition/";
var reportId	= 923;
var menuId		= 866;

$(document).ready(function() {
	
	$("#frmPettyCashRequest").validationEngine();
	$('#frmPopUp #chkCheckAll').die('click').live('click',function(){
		CheckAll_Approve($(this).is(':checked'));
	});
	$('#frmPettyCashRequest #butAdd').die('click').live('click',setToAddNew);
	$('#frmPopUp #butAdd').die('click').live('click',addInvoice);
	$('#frmPettyCashRequest .removeRow').die('click').live('click',deleteRow);
	$('#frmPettyCashRequest #butSave').die('click').live('click',saveRequest);
	$('#frmPettyCashRequest #butApprove').die('click').live('click',Confirm);
	$('#frmPettyCashRequest #butReport').die('click').live('click',loadReport);
	$('#frmPettyCashRequest #butNew').die('click').live('click',clearAll);
	$('#frmRptPettyCashReq #butRptConfirm').die('click').live('click',urlApprove);
	$('#frmRptPettyCashReq #butRptReject').die('click').live('click',urlReject);
	$('#frmPettyCashRequest .butRptConfirmformError').die('click').live('click',alertxR1);
	$('#frmPopUp #butSearch').die('click').live('click',loadItems);
});

function setToAddNew()
{
	var loop	= 0;
	popupWindow3('1');
	var toDate		= $('#frmPettyCashRequest #toDate').val();
	var fromDate	= $('#frmPettyCashRequest #fromDate').val();
	var locationID	= $('#frmPettyCashRequest #cboLocation').val();
	//alert(aa);
	$('#popupContact1').load(basePath+"popUp_petty_cash_requisition.php?locationID="+locationID,function(){
			loadItems();		
			$('#frmPopUp #butClosePop').die('click').live('click',disablePopup);
	});
			
}	

function addInvoice()
{
	var total	= parseFloat($('#frmPettyCashRequest #totAmt').html());
	$('#frmPopUp .chkInvoice:checked').each(function(){
				
				var popRow			= $(this).parent().parent().attr('id');
				var popObj			= $(this).parent().parent();
				var file			= popObj.find('.cls_td_invoiceNo').attr('id');
				var itemID			= popObj.find('.cls_td_item').attr('id');
				var amount			= parseFloat(popObj.find('.cls_td_amount').attr('id'));
				total				= total + amount;
				totAmount			= RoundNumber_FixedDecimals(total,2);
				var invoiceYear		= $(this).parent().attr('id');
				var found 			= false;
				$('#frmPettyCashRequest #tblMain #tblBody >tr').each(function(){
					var mainObj		= $(this).attr('id');
					
					if(popRow == mainObj)
						found = true;
				});
				
				if(!found)
				{					
					var x = "<tr class =\"td_invoiceNo\" id=\""+popRow+"\">"+
					"<td align=\"center\" style=\"text-align:center\"><img class=\"mouseover removeRow\" src=\"images/del.png\" /></td>"+
					"<td class=\"cls_td_invoice\" id=\""+invoiceYear+"\">"+popObj.find('.cls_td_invoiceNo').html()+"</td>"+
					"<td class=\"cls_td_item\" id=\""+itemID+"\">"+popObj.find('.cls_td_item').html()+"</td>"+
					"<td class =\"cls_td_amt\" id=\""+amount+"\" style=\"text-align:right\">"+popObj.find('.cls_td_amount').html()+"</td>"+
					"<td style=\"text-align:center\">"+popObj.find('.cls_td_path').html()+"</td></tr>";
					
					$("#frmPettyCashRequest #tblMain #tblBody:last").append(x); 
					$('#totAmt').html(totAmount);
				}
			});
			disablePopup();
				
}
function loadItems()
{
	$("#frmPopUp #tblInvoicePopup tr:gt(1)").remove();
			
			var fromDate	= $('#frmPopUp #fromDate').val();
			var toDate		= $('#frmPopUp #toDate').val();
			
			var url 		= basePath+"petty_cash_requisition_db.php?requestType=loadItems";
			var httpobj = $.ajax({
				url:url,
				dataType:'json',
				type:'POST',
				data:"fromDate="+fromDate+"&toDate="+toDate,
				async:false,
				success:function(json){
			
					$('#frmPopUp #tblInvoicePopup #tblContent').html(json.gridDetail);	
				}
			});	
}
function CheckAll_Approve(checked)
{
$('#frmPopUp .chkInvoice').each(function(){
		if(!$(this).is(':disabled')){
			if(checked)
				$(this).attr('checked','checked');
			else
				$(this).removeAttr('checked');
		}
	});
}

function deleteRow()
{
	var obj 	= this;
	var amount1	= parseFloat($(obj).parent().parent().find('.cls_td_amt').attr('id'));
	$(obj).parent().parent().remove();
	var balance	= parseFloat($('#frmPettyCashRequest #totAmt').html()) - amount1;
	var bal		= RoundNumber_FixedDecimals(balance,2);
	$('#frmPettyCashRequest #totAmt').html(bal);
			
}
function saveRequest()
{
	showWaiting();
	var requestNo	= $('#frmPettyCashRequest #txtRequisitionNo').val();
	var requestYear	= $('#frmPettyCashRequest #txtRequisitionYear').val();
	var locationID	= $('#frmPettyCashRequest #cboLocation').val();
	var remark		= $('#frmPettyCashRequest #txtRemark').val();
	var total		= 0;	
	
	if($('#frmPettyCashRequest').validationEngine('validate'))
	{
		var data = "requestType=saveData";
		
		var arrDetail	 = "[";
		$('#frmPettyCashRequest #tblMain #tblBody >tr').each(function(){
			var invoiceNo		= $(this).attr('id');
			var invoiceYear		= $(this).find('.cls_td_invoice').attr('id');
			var amount			= parseFloat($(this).find('.cls_td_amt').attr('id'));
			var itemID			= $(this).find('.cls_td_item').attr('id');	
			var itemName		= $(this).find('.cls_td_item').html();	
			total				= total + amount;
			totAmount			= RoundNumber_FixedDecimals(total,2);
			
				arrDetail 	+= "{";
				arrDetail 	+= '"invoiceNo":"'+invoiceNo+'",' ;
				arrDetail 	+= '"invoiceYear":"'+invoiceYear+'",';
				arrDetail 	+= '"itemID":"'+itemID+'"' ;
				arrDetail 	+=  "},";
		});
		arrDetail 	 = arrDetail.substr(0,arrDetail.length-1);
		arrDetail 	+= "]";
		
		var arrHeader = "{";
							arrHeader += '"requestNo":"'+requestNo+'",' ;
							arrHeader += '"requestYear":"'+requestYear+'",' ;
							arrHeader += '"locationID":"'+locationID+'",' ;
							arrHeader += '"remark":'+URLEncode_json(remark)+',';
							arrHeader += '"total":"'+totAmount+'"' ;
 			arrHeader  += "}";
		data   +="&arrHeader="+arrHeader;
		data   +="&arrDetail="+arrDetail;
		var url = basePath+"petty_cash_requisition_db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmPettyCashRequest #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t = setTimeout("alertx()",3000);
						$('#frmPettyCashRequest #txtRequisitionNo').val(json.requestNo);
						$('#frmPettyCashRequest #txtRequisitionYear').val(json.requestYear);
						$('#frmPettyCashRequest #butApprove').show();
						
						hideWaiting();
						return;
					}
					else
					{
						hideWaiting();
					}
				},
				error:function(xhr,status)
				{
					$('#frmPettyCashRequest #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					hideWaiting();
					return;;
				}
		});
	}
}
function urlApprove()
{

	var val = $.prompt('Are you sure you want to approve this Requisition ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					/*var arrDetail	 = "[";
					$('#frmRptPettyCashReq #tblData #tblBody >tr').each(function(){
						var invoiceNo		= $(this).find('.td_invoiceNo').attr('id');
						var invoiceYear		= $(this).find('.td_invoiceYear').attr('id');
												
							arrDetail 	+= "{";
							arrDetail 	+= '"invoiceNo":"'+invoiceNo+'",' ;
							arrDetail 	+= '"invoiceYear":"'+invoiceYear+'"';
							arrDetail 	+=  '},';
					});
					arrDetail 	 = arrDetail.substr(0,arrDetail.length-1);
					arrDetail 	+= " ]";*/
					var url = basePath+"petty_cash_requisition_db.php"+window.location.search+'&requestType=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptPettyCashReq #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertxR1()",10000);
									window.location.href = window.location.href;
									window.opener.location.reload();
									
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptPettyCashReq #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertxR1()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}
function urlReject()
{
	var val = $.prompt('Are you sure you want to reject this Requisition ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = basePath+"petty_cash_requisition_db.php"+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptPettyCashReq #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertxR2()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();

									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptPettyCashReq #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertxR2()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}
function Confirm()
{
	var url  = "?q="+reportId+"&requestNo="+$('#frmPettyCashRequest #txtRequisitionNo').val();
	    url += "&requestYear="+$('#frmPettyCashRequest #txtRequisitionYear').val();
	    url += "&mode=Confirm";
	window.open(url,'rptPetty_cash_requisition.php');
}
function loadReport()
{
	if($('#frmPettyCashRequest #txtRequisitionNo').val()=='')
	{
		$('#frmPettyCashRequest #butReport').validationEngine('showPrompt','No Requisition No to view Report','fail');
		return;	
	}
	var url  = "?q="+reportId+"&requestNo="+$('#frmPettyCashRequest #txtRequisitionNo').val();
	    url += "&requestYear="+$('#frmPettyCashRequest #txtRequisitionYear').val();
	
	window.open(url,'rptPetty_cash_requisition.php');
}

function RoundNumber_FixedDecimals(num,dec)
{
	num = parseFloat(num).toFixed(parseFloat(dec)+4);
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return parseFloat(result).toFixed(dec);
}
function alertx()
{
	$('#frmPettyCashRequest #butSave').validationEngine('hide')	;
}
function alertxR1()
{
	$('#frmRptPettyCashReq #butRptConfirm').validationEngine('hide')	;
}
function alertxR2()
{
	$('#frmRptPettyCashReq #butRptReject').validationEngine('hide')	;
}

function clearAll()
{
	window.location.href = '?q='+menuId;
}

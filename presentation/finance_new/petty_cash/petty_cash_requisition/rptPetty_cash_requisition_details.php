<?php
session_start();
$backwardseperator		= "../../../../";
$companyId 				= $_SESSION['headCompanyId'];
$locationId				= $_SESSION['CompanyID'];
include_once 			"{$backwardseperator}dataAccess/Connector.php";

//BEGIN - SET PARAMETERS {
if(!isset($_REQUEST['txtDateFrom']))
{
	$dateFrom 	= date('Y-m-01');
	$dateTo		= date('Y-m-d');
}
else
{
	$dateFrom 	= $_REQUEST['txtDateFrom'];
	$dateTo		= $_REQUEST['txtDateTo'];
}
//END	- SET PARAMETERS }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Petty Cash Detail Report</title>
<link rel="stylesheet" type="text/css" href="../../../../css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css"/>
<link rel="stylesheet" type="text/css" href="../../../../css/promt.css"/>
<link rel="stylesheet" type="text/css" href="../../../../css/button.css"/>
<link rel="stylesheet" type="text/css" href="../../../../libraries/litebox/css/lightbox.css" media="screen"/>

<script src="../../../../libraries/litebox/js/prototype.js" type="text/javascript"></script>
<script src="../../../../libraries/litebox/js/scriptaculous.js?load=effects" type="text/javascript"></script>
<script src="../../../../libraries/litebox/js/lightbox.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
<script src="../../../../libraries/javascript/script.js" type="text/javascript"></script>
<script type="text/javascript">
function submit()
{
	document.frmPettycashRequisitinDetailReport.submit();
}
</script>
</head>
<body>
<form id="frmPettycashRequisitinDetailReport" name="frmPettycashRequisitinDetailReport" method="post" action="rptPetty_cash_requisition_details.php">
  <table width="100%" align="center">
    <tr>
      <td width="1006" align="center" class="reportHeader">Petty Cash  Requisition Detail Report</td>
    </tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="1" cellpadding="1" class="normalfnt">
        <tr>
          <td width="12%">Request Date From</td>
          <td width="1%">:</td>
          <td width="8%"><input name="txtDateFrom" type="text" value="<?php echo $dateFrom; ?>" class="txtbox validate[required]" id="txtDateFrom" style="width:80px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          <td width="72%">&nbsp;</td>
          <td width="7%"><a class="button white small" onclick="submit()">Search</a></td>
        </tr>
        <tr>
          <td>Request Date To</td>
          <td>:</td>
          <td><input name="txtDateTo" type="text" value="<?php echo $dateTo; ?>" class="txtbox validate[required]" id="txtDateTo" style="width:80px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td ><table width="100%" class="bordered" border="0" >
          <thead>
            <tr class="normalfnt">
              <th width="9%" >Requisition No</th>
              <th width="12%" >Invoice No</th>
              <th width="38%" >Item Description</th>
              <th width="19%" >Requested By</th>
              <th width="11%" >Invoiced Date</th>
              <th width="11%" >Amount <br/>[LKR]</th>
            </tr>
          </thead>
          <tbody>
            <?php $result = getData($dateFrom,$dateTo);
			while($row = mysqli_fetch_array($result)){?>
            <tr class="normalfnt">
              <td ><a href="../petty_cash_requisition/rptPetty_cash_requisition.php?requestNo=<?php echo $row["REQUISITION_NO"]?>&requestYear=<?php echo $row["REQUISITION_YEAR"]?>" target="rptPetty_cash_requisition.php"><?php echo $row["CONCAT_REQUISITION_NO"]?></a></td>
              <td >&nbsp;</td>
              <td >&nbsp;</td>
              <td >&nbsp;</td>
              <td >&nbsp;</td>
              <td >&nbsp;</td>
            </tr>
            <?php
				$result1 = getInvoiceData($row["REQUISITION_NO"],$row["REQUISITION_YEAR"]);
				while($row1 = mysqli_fetch_array($result1))
				{
					?>
                    <tr class="normalfnt">
                      <td >&nbsp;</td>
                      <td ><a href="../petty_cash_invoice/rpt_petty_cash_invoice.php?serialNo=<?php echo $row1["INVOICE_NO"]?>&serialYear=<?php echo $row1["INVOICE_YEAR"]?>" target="rpt_petty_cash_invoice.php"><?php echo $row1["INVOICE_NO"].'/'.$row1["INVOICE_YEAR"]?></a></td>
                      <td ><?php echo $row1["NAME"]?></td>
                      <td ><a href="<?php echo '/'.$_SESSION["HRDatabase"].'/'.$row1['PHOTO_URL']?>" rel="lightbox" title="<?php echo $row["EMPLOYEE_NAME"]?>"><?php echo $row1["INITIAL_NAME"]?></a></td>
                      <td style="text-align:center"><?php echo $row1["INVOICED_DATE"]?></td>
                      <td style="text-align:right"><?php echo number_format($row1["AMOUNT"],2)?></td>
                    </tr>
                    <?php
				}
			} 
			?>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td align="center" class="normalfntMid">&nbsp;</td>
    </tr>
    <tr>
      <td align="center" class="normalfntMid"><strong>Printed Date : </strong><?php echo date('Y-m-d H:i:s');?></td>
    </tr>
  </table>
</form>
</body>
</html>
<?php
function getData($dateFrom,$dateTo)
{
	global $db;
	
	$sql = "SELECT 
	        	CONCAT(REQUISITION_NO,'/',REQUISITION_YEAR) AS CONCAT_REQUISITION_NO,
				REQUISITION_NO								AS REQUISITION_NO,
				REQUISITION_YEAR							AS REQUISITION_YEAR
			FROM finance_pettycash_requisition_header H 
			WHERE H.STATUS = 1
				AND H.REQUEST_DATE BETWEEN '$dateFrom' AND '$dateTo' ";
	return $db->RunQuery($sql);
}

function getInvoiceData($no,$year)
{
	global $db;
	
	$sql = "SELECT 
				INVOICE_NO,
				INVOICE_YEAR,
				PI.NAME,
				I.DATE				AS INVOICED_DATE,
				ABS(I.AMOUNT) 			AS AMOUNT,
				E.strInitialName  	AS INITIAL_NAME,
				E.Photo				AS PHOTO_URL
			FROM finance_pettycash_requisition_detail D
			INNER JOIN finance_pettycash_invoice I
				ON I.SERIAL_NO = D.INVOICE_NO
			  	AND I.SERIAL_YEAR = D.INVOICE_YEAR
			INNER JOIN finance_mst_pettycash_item PI
			  ON PI.ID      = I.PETTY_CASH_ITEM
			INNER JOIN qpay.mst_employee E 
				ON E.intEmployeeId = I.REQUEST_EMPLYEE    
			WHERE D.REQUISITION_NO = '$no'
			AND D.REQUISITION_YEAR = '$year'";
	return $db->RunQuery($sql);
}
?>
<?php
session_start();
$backwardseperator 		= "../../../../";
$userId 				= $_SESSION['userId'];
$locationId	  			= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];

$requestType 			= $_REQUEST['requestType'];
$programCode			= 'P0859';
$savedMasseged			= "";
$error_sql				= "";
$savedStatus			= true;
$editMode				= false;
//die($requestType);
include_once "{$backwardseperator}dataAccess/Connector.php";
include_once "../../../../class/cls_commonFunctions_get.php";
include "../../../../class/finance/petty_cash/petty_cash_item/cls_petty_cash_item_get.php";
include "../../../../class/finance/petty_cash/petty_cash_item/cls_petty_cash_item_set.php";

$obj_petty_cash_item_set			= new cls_petty_cash_item_set($db);
$obj_petty_cash_item_get			= new cls_petty_cash_item_get($db);
$obj_comfunc_get					= new cls_commonFunctions_get($db);

$IOUCat			= $obj_petty_cash_item_get->getIOUID();

if($requestType	== 'saveData')
{
	$dataArr	= json_decode($_REQUEST['dataArr'],true);
	$searchId	= $dataArr['searchID'];
	$itemCode	= $dataArr['itemCode'];
	$itemName	= $dataArr['itemName'];
	$category	= $dataArr['category'];
	$status		= $dataArr['status'];
	
	$db->begin();
	$savePermission	= $obj_comfunc_get->Load_menupermision2($programCode,$userId,'intAdd');
	if($savePermission==0 && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged  = 'No permission to save.';
	}
	if($category == $IOUCat)
	{
		$itemRows	= $obj_petty_cash_item_get->getCategoryWiseItem($category);
		if($itemRows > 0 && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= "You can not enter another item to IOU category";

		}
		
		
	}
	if($searchId == '')
	{
		$saveArr	= $obj_petty_cash_item_set->saveItem($itemCode,$itemName,$category,$status,'RunQuery2');
		if($saveArr['savedStatus']=='fail' && ($savedStatus))
		{
			//die('ok');
			$savedStatus	= false;
			$savedMasseged	= $saveArr['savedMassege'];
			$error_sql		= $saveArr['error_sql'];
		}
	}
	else
	{
		$editMode	= true;
		$saveArr	= $obj_petty_cash_item_set->updateItem($searchId,$itemCode,$itemName,$category,$status,'RunQuery2');
		
		if($saveArr['savedStatus']=='fail' && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged	= $saveArr['savedMassege'];
			$error_sql		= $saveArr['error_sql'];
		}			
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type']	= 'pass';
		if($editMode== true)
			$response['msg']	= "Updated Successfully";
		else
			$response['msg']	= "Saved Successfully";			
	}
	else
	{
		$db->rollback();
		$response['type']	= 'fail';
		$response['msg']	= $savedMasseged;
		$response['sql']	= $error_sql;	
	}
	echo json_encode($response);
}
else if($requestType == 'loadItem')
{
	$result1	= $obj_petty_cash_item_get->loadItems('RunQuery');
	
	$html = "<option value=\"\"></option>";
	
	while($row = mysqli_fetch_array($result1))
	{
		$html .= "<option value=\"".$row["ID"]."\">".$row["NAME"]."</option>";
	}
	echo $html;
}
else if($requestType == 'searchData')
{
	$searchId				= $_REQUEST['searchID'];
	$searchData				= $obj_petty_cash_item_get->searchItem($searchId,'RunQuery');
	$response['itemCode']	= $searchData['CODE'];
	$response['itemName']	= $searchData['NAME'];
	$response['category']	= $searchData['CATEGORY_ID'];
	$response['status']		=( $searchData['STATUS']==1?true:false);
	
	echo json_encode($response);
}
else if($requestType == 'deleteItem')
{
	
	$searchId	 = $_REQUEST['searchID'];
	
	$db->begin();
	
	$deletePermission	= $obj_comfunc_get->Load_menupermision2($programCode,$userId,'intDelete');
	if($deletePermission==0 && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged  = 'No permission to delete.';
	}
	
	$dataArr		= $obj_petty_cash_item_set->deleteItem($searchId,'RunQuery2');
	if($dataArr['savedStatus']=='fail' && ($savedStatus))
	{
		$savedStatus	= false;
		$savedMasseged	= $dataArr['savedMassege'];
		$error_sql		= $dataArr['error_sql'];
	}
	
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg']		= "Deleted Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);	
}
?>

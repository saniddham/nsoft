<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$userId 			= $_SESSION['userId'];
$locationId	  		= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$programCode		= 'P0859';

include "class/cls_commonFunctions_get.php";
include "class/finance/petty_cash/petty_cash_item/cls_petty_cash_item_get.php";
//include "include/javascript.html";

$obj_petty_cash_item_get = new cls_petty_cash_item_get($db);
$obj_comFunc_get 		= new cls_commonFunctions_get($db);

$search_result			= $obj_petty_cash_item_get->loadCategory('RunQuery');
$search_item			= $obj_petty_cash_item_get->loadItems('RunQuery');

$savedMode				= $obj_comFunc_get->Load_menupermision($programCode,$userId,'intAdd');
$deleteMode				= $obj_comFunc_get->Load_menupermision($programCode,$userId,'intDelete');


?>
<title>Petty Cash Item</title>

<!--<script type="text/javascript" src="presentation/finance_new/petty_cash/petty_cash_item/petty_cash_item_js.js"></script> --> 

<form id="frmPettyCashItem" name="frmPettyCashItem" method="post">
<div align="center">
    <div class="trans_layoutS">
   		<div class="trans_text">Petty Cash Item</div>
            <table width="100%" border="0" align="center">
            	<tr>
            		<td width="62%">
                    <table width="100%" border="0" class="">
            			<tr>
            				<td class="normalfnt">&nbsp;</td>
            				<td class="normalfnt">Item</td>
            				<td width="309" colspan="2">
                            <select name="cboSearch" class="txtbox" id="cboSearch" style="width:250px" tabindex="1">
                            <option value=""></option>
                            <?php
					while($row = mysqli_fetch_array($search_item))
					{
						echo "<option value=\"".$row["ID"]."\">".$row["NAME"]."</option>";
					}
				    ?>
                                   				
                            </select></td>
            			</tr>
           				<tr>
           					<td width="56" class="normalfnt">&nbsp;</td>
            				<td width="115" class="normalfnt">&nbsp;</td>
            				<td colspan="2">&nbsp;</td>
            			</tr>
                        <tr>
                            <td class="normalfnt">&nbsp;</td>
                            <td class="normalfnt">Item Code&nbsp;<span class="compulsoryRed">*</span></td>
                            <td colspan="2"><input name="txtItemCode" type="text" class="validate[required,maxSize[50]]" id="txtItemCode" style="width:250px" maxlength="50" tabindex="2"/></td>
            			</tr>
            			<tr>
                            <td class="normalfnt">&nbsp;</td>
                            <td class="normalfnt">Item Name&nbsp;<span class="compulsoryRed">*</span></td>
                            <td colspan="2"><input name="txtItemName" type="text" class="validate[required,maxSize[50]]" id="txtItemName" style="width:250px" maxlength="50" tabindex="2"/></td>
            			</tr>
            			<tr>
            			  <td class="normalfnt">&nbsp;</td>
            			  <td class="normalfnt">Item Category&nbsp;<span class="compulsoryRed">*</span></td>
                          <td colspan="2"><select name="cboCategory" class="validate[required]" id="cboCategory" style="width:150px" tabindex="3" >
                    <option value=""></option>
                    <?php
					while($row = mysqli_fetch_array($search_result))
					{
						echo "<option value=\"".$row["ID"]."\">".$row["NAME"]."</option>";
					}
				    ?>
                          </select> <img src="images/add_new.png" alt="addItem" name="butViewCategory" class="mouseover" id="butViewCategory"  /></td>
          			  </tr>
             		  <tr>
                            <td class="normalfnt">&nbsp;</td>
                            <td class="normalfnt">Active</td>
                            <td><input type="checkbox" name="chkActive" id="chkActive" checked="checked" tabindex="5"/></td>
            			</tr>
            			<tr>
                            <td class="normalfnt">&nbsp;</td>
                            <td class="normalfnt">&nbsp;</td>
                            <td colspan="3" class="normalfnt">&nbsp;</td>
            			</tr>
            		</table>
            		</td>
            	</tr>
            	<tr>
            		<td height="34">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
            			<tr>
            				<td width="100%" align="center" bgcolor=""><a class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butSave" name="butSave" <?php echo($savedMode==1?'':'style="display:none"'); ?> >Save</a><a class="button white medium" id="butDelete" name="butDelete" <?php echo($deleteMode==1?'':'style="display:none"'); ?>>Delete</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
            			</tr>
            		</table>
                    </td>
            	</tr>
            </table>
	</div>
</div>
<div  style="position: absolute;display:none;z-index:100"  id="popupContact1">
    <?php 
	
	 ?>
    <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="header_db.php?q=1075&iframe=1&clearCash=1" style="width:650px;height:390px;border:0;overflow:hidden">
    
    </iframe>
    </div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</form>
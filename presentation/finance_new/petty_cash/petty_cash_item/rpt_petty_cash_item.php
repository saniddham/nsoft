<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include "class/finance/petty_cash/petty_cash_item/cls_petty_cash_item_get.php";

$obj_petty_cash_item_get	= new cls_petty_cash_item_get($db); 
$resultList					= $obj_petty_cash_item_get->pettyCash_item('RunQuery');

?>
<style type="text/css">
#apDiv1 {
	position: absolute;
	left: 425px;
	top: 134px;
	width: 370px;
	height: 165px;
	z-index: 1;
}
</style>
<title>Petty Cash Item List</title>
<form id="frmForm" name="frmForm">
  <table width="800" border="0" align="center">
  <tr>
  	<td>
    	<table width="100%" border="0">
        <tr>
        	<td class="reportHeader" align="center">Petty Cash Item List</td>
        </tr>
        </table>
    </td>
  </tr>
  <tr>
  	<td>
    	<table align="center" width="100%" class="rptBordered" id="tblMain" bgcolor="#FFFFFF">
        <thead>
          <tr>
            <th rowspan="2" nowrap="nowrap">No</th>
            <th rowspan="2" >Category</th>
            <th rowspan="2">Item Code</th>
            <th rowspan="2" nowrap="nowrap">Item Name</th>
            <th rowspan="2" >Status</th>
          </tr>
        </thead>
        <tbody>
        	<?php
			$rowCount	= 0;
			while($row = mysqli_fetch_array($resultList))
			{ 
			//die('ok');?>
                <tr>
                <td align="center" nowrap="nowrap" style="text-align:center" ><?php echo ++$rowCount ?>.</td>
                <td align="center" nowrap="nowrap" style="text-align:left"><?php echo $row['category']?></td>
                <td align="center" nowrap="nowrap" style="text-align:center"><?php echo $row['CODE']?></td>
                <td align="center" nowrap="nowrap" style="text-align:left"><?php echo $row['NAME']?></td>
                <td align="center" nowrap="nowrap" style="text-align:center"><?php echo ($row['STATUS']==1?'Active':'Inactive')?></td>
			<?php
			}
			?>
        </tbody>
        </table>
    </td>
  </tr>
  </table>
</form>

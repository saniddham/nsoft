var basePath	= "presentation/finance_new/petty_cash/petty_cash_item/";
$(document).ready(function(e) {
	
    $("#frmPettyCashItem").validationEngine();
	$('#frmPettyCashItem #butSave').die('click').live('click',saveItem);
	$('#frmPettyCashItem #cboSearch').die('change').live('change',searchItem);
	$('#frmPettyCashItem #butDelete').die('click').live('click',deleateItem);
	$('#frmPettyCashItem #butNew').die('click').live('click',clearAll);
	//loadItem();
	
});
function saveItem()
{
	var searchID		= $('#frmPettyCashItem #cboSearch').val();
	var itemCode		= $('#frmPettyCashItem #txtItemCode').val();
	var itemName		= $('#frmPettyCashItem #txtItemName').val();
	var category		= $('#frmPettyCashItem #cboCategory').val();	
	var status			= ($('#frmPettyCashItem #chkActive').attr('checked')?1:0);
	
	if($('#frmPettyCashItem').validationEngine('validate'))
	{
		var data		= "requestType=saveData";
		var dataArr		= "{";	
			dataArr	   += '"searchID":"'+searchID+'",';
			dataArr	   += '"itemCode":"'+itemCode+'",';
			dataArr	   += '"itemName":"'+itemName+'",';
			dataArr	   += '"category":"'+category+'",';
			dataArr	   += '"status":"'+status+'"';
			dataArr	   += "}";
			
			data	   += "&dataArr="+dataArr;
			
		var url			= basePath+"petty_cash_item_db.php";
		$.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:data,
			async:false,
			success: function(json)
			{
				$('#frmPettyCashItem #butSave').validationEngine('showPrompt',json.msg,json.type);
				if(json.type == 'pass')
				{
					$('#frmPettyCashItem').get(0).reset();
					loadItem();
					var t=setTimeout("alertx()",1000);
					return;
				}
				else
				{
					
				}
				
			},
			error:function(xhr,status){
						
						$('#frmPettyCashItem #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						return;
				}		
		});
	}
	
}

function loadMain()
{
	$("#butViewCategory").die('click').live('click',popupCategory);
}

function popupCategory()
{
	popupWindow2('1','#cboSearch','#cboCategory','#frmPettyCashItem');
}

function searchItem()
{
	var searchID	= $('#frmPettyCashItem #cboSearch').val();
	if(searchID	== '')
	{
		clearAll();
		return ;	
	}
	
	var data 		= "requestType=searchData";
	data		   += "&searchID="+searchID;
	var url	 		= basePath+"petty_cash_item_db.php";
	$.ajax({
		url:url,
		data:data,
		dataType:'json',
		type:'POST',
		async:false,
		success: function(json)
		{
			$('#frmPettyCashItem #txtItemCode').val(json.itemCode);
			$('#frmPettyCashItem #txtItemName').val(json.itemName);
			$('#frmPettyCashItem #cboCategory').val(json.category);
			$('#frmPettyCashItem #chkActive').attr('checked',json.status);		
		},
		error:function(xhr,status)
		{
		
		}	
		
		});
		
}
function deleateItem()
{
	var searchID	= $('#frmPettyCashItem #cboSearch').val();
	if(searchID	== '')
	{
		$('#frmPettyCashItem #cboSearch').validationEngine('showPrompt','Please select an Item.','fail');
		return ;	
	}
	var val = $.prompt('Are you sure you want to delete "'+$('#frmPettyCashItem #cboSearch option:selected').text()+'" ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
	var data 		= "requestType=deleteItem";
	data		   += "&searchID="+searchID;
	var url	 		= basePath+"petty_cash_item_db.php";
	$.ajax({
		url:url,
		data:data,
		dataType:'json',
		type:'POST',
		async:false,
		success: function(json)
		{
			$('#frmPettyCashItem #butDelete').validationEngine('showPrompt', json.msg,json.type);
								if(json.type=='pass')
								{
									$('#frmPettyCashItem').get(0).reset();
									loadItem();
									var t=setTimeout("Deletex()",1000);
									return;
								}	
		},
		error:function(xhr,status)
		{
			
		}
	});
	}
	}
});

}
function clearAll()
{
	$('#frmPettyCashItem').get(0).reset();
	loadItem();
	//loadSearchCombo();
}
function loadItem()
{
	//alert('ok');
	var url 	= basePath+"petty_cash_item_db.php?requestType=loadItem";
	var httpobj = $.ajax({url:url,type:'POST',async:false});
	$('#frmPettyCashItem #cboSearch').html(httpobj.responseText);	
}
function alertx()
{
	$('#frmPettyCashItem #butSave').validationEngine('hide')	;
}
function Deletex()
{
	$('#frmPettyCashItem #butDelete').validationEngine('hide')	;
}
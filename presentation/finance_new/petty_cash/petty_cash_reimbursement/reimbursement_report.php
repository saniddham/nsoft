<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$location	 		= $sessions->getLocationId();
$userId  			= $sessions->getUserId();
$companyId  		= $sessions->getCompanyId(); 
$programCode		= 'P1110';

$reimbursementNo 	= $_REQUEST['reimbursementNo'];
$reimbursementYear 	= $_REQUEST['reimbursementYear'];
$mode 				= $_REQUEST['mode'];

include_once "class/tables/menupermision.php";									$menupermision								= new menupermision($db);
include_once "class/tables/sys_approvelevels.php";								$sys_approvelevels							= new sys_approvelevels($db);
include_once "class/tables/sys_no.php";											$sys_no										= new sys_no($db);
include_once "class/dateTime.php";												$dateTimes									= new dateTimes($db);
include_once "class/tables/finance_pettycash_reimbursement.php";				$finance_pettycash_reimbursement			= new finance_pettycash_reimbursement($db);
include_once "class/tables/finance_pettycash_reimbursement_approve_by.php";		$finance_pettycash_reimbursement_approve_by	= new finance_pettycash_reimbursement_approve_by($db);
include_once "class/tables/mst_locations.php";									$mst_locations								= new mst_locations($db);
include_once "class/tables/sys_users.php";										$sys_users									= new sys_users($db);

$finance_pettycash_reimbursement->set($reimbursementNo,$reimbursementYear);
$date			= $finance_pettycash_reimbursement->getDATE();
$amount			= $finance_pettycash_reimbursement->getAMOUNT();
$reim_location	= $finance_pettycash_reimbursement->getLOCATION_ID();
$remark			= $finance_pettycash_reimbursement->getREMARKS();
$status			= $finance_pettycash_reimbursement->getSTATUS();
$approveLevel	= $finance_pettycash_reimbursement->getAPPROVE_LEVELS();
$locationId		= $finance_pettycash_reimbursement->getUSER_LOCATION_ID();

$mst_locations->set($reim_location);
$rmlocationName	= $mst_locations->getstrName();
$createCompanyId = $mst_locations->getintCompanyId();

$header_array['STATUS']	= $status;
$header_array['LEVELS'] = $approveLevel;

$menupermision->set($programCode,$userId);

$perRejArr				= $menupermision->checkMenuPermision('Reject',$status,$approveLevel);
$perApproArr			= $menupermision->checkMenuPermision('Approve',$status,$approveLevel);
$perCancelArr			= $menupermision->checkMenuPermision('Cancel',$status,$approveLevel);
$pdfPermission			= $menupermision->getintExportToPDF();

$permision_reject		= ($perRejArr['type']?1:0);
$permision_confirm		= ($perApproArr['type']?1:0);
$permision_cancel		= ($perCancelArr['type']?1:0);

?>
<head>
<title>Other Receivable - Bill Invoice Report</title>
<script type="application/javascript" src="presentation/finance_new/petty_cash/petty_cash_reimbursement/reimbursement_js.js"></script>
<style>
#apDiv1 {
	position: absolute;
	left: 276px;
	top: 143px;
	width: 650px;
	height: 322px;
	z-index: 1;
}
</style>

</head>


<body>
<div id="rpt_div_main">
<?php include 'presentation/report_ribbon_watermark.php'?>

<div id="rpt_div_table">

<form id="frmReimbursementRpt" name="frmReimbursementRpt" method="post" autocomplete="off">
<table width="900" align="center">
    <tr>
		<td colspan="2">	
				 <?php include 'report_header_latest.php'?>
			</td>
	</tr>
    
    <tr>
    	<td colspan="2" class="reportHeader" align="center">PETTY CASH REIMBURSEMENT REPORT</td>
    </tr>
    <tr>
    <td colspan="2"><table width="100%" align="center">
     <?php include "presentation/report_approve_status_and_buttons_latest.php" ?>
    </table></td>
    </tr>
    <tr>
        <td colspan="2">
            <table width="100%" border="0" class="normalfnt">
                <tr>
                	<td width="5%">&nbsp;</td>
                    <td width="21%">&nbsp;</td>
                    <td width="1%">&nbsp;</td>
                    <td width="34%">&nbsp;</td>
                    <td width="17%">&nbsp;</td>
                    <td width="1%">&nbsp;</td>
                    <td width="21%">&nbsp;</td>
                </tr>
                <tr>
                	<td width="5%">&nbsp;</td>
                    <td>Reimbursement No</td>
                    <td>:</td>
            		<td><?php echo $reimbursementNo.'/'.$reimbursementYear ?></td>
                    <td>Reimbursement Date</td>
                    <td>:</td>
                    <td><?php echo $date;?></td>
                </tr>
                <tr>
                	<td width="5%">&nbsp;</td>
                    <td>Location</td>
                    <td>:</td>
                    <td><?php echo $rmlocationName?></td>
                    <td>Amount</td>
                    <td>:</td>
                    <td><?php echo number_format($amount,2)?></td>
                </tr>
                <tr>
                	<td width="5%">&nbsp;</td>
                    <td valign="top">Remarks</td>
                    <td valign="top">:</td>
                    <td rowspan="2" valign="top"><?php echo $remark?></td>
                    <td valign="top">&nbsp;</td>
                     <td valign="top"></td>
                    <td valign="top">&nbsp;</td>
                </tr>
                <tr>
                	<td width="5%">&nbsp;</td>
                    <td height="16">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="right"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td colspan="2" class="normalfnt">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="2">
        <?php
			$db->connect();$db->begin();
			$sys_users->set($finance_pettycash_reimbursement->getCREATED_BY());
			
			$creator		= $sys_users->getstrUserName();
			$createdDate	= $finance_pettycash_reimbursement->getCREATED_DATE();
			
			$resultA		= $finance_pettycash_reimbursement_approve_by->getApprovedByData($reimbursementNo,$reimbursementYear);
			include "presentation/report_approvedBy_details.php";
			$db->disconnect();
 	?>
        </td></tr>
	<?php
    if($mode=='Confirm' && $status > 1)
    {
    ?>
    <tr>
    <?php
    $createCompanyId = $createCompanyId;
    $url  = "presentation/send_to_approval_latest.php";
    $url .= "?status=$status";										// * set recent status
    $url .= "&approveLevels=$approveLevel";								// * set approve levels in order header
    $url .= "&programCode=$programCode";								// * program code (ex:P10001)
    $url .= "&program=Finance Petty Cash Reimbursement";									// * program name (ex:Purchase Order)
    $url .= "&companyId=$createCompanyId";									// * created company id
    $url .= "&createUserId=$createdBy";
    
    $url .= "&subject=PETTY CASH REIMBURSEMENT FOR APPROVAL (".$reimbursementNo."/".$reimbursementYear.")";	
    
    $url .= "&statement1=Please Approve this";	
    $url .= "&statement2=to Approve this";	
                    // * doc year
    $url .= "&link=".urlencode(base64_encode(MAIN_URL."?q=1112&reimbursementNo=".$reimbursementNo."&reimbursementYear=".$reimbursementYear."&mode=Confirm"));
    ?>
    <td colspan="2" align="center" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:150px;border:none"></iframe></td>
    </tr>
    <?php
    }
    ?>
<tr>
  <td colspan="2" align="center" class="normalfntMid"><strong>Printed Date:  <?php echo date("Y/m/d") ?></strong></td>
</tr>
       </table>
</form></div></div>
</body>
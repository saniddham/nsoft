<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$programCode		= 'P1110';
$reportID			= 1112;
$formID				= 1110;
$location		= $sessions->getLocationId();
$intUser		= $sessions->getUserId();
$company		= $sessions->getCompanyId();
include_once "class/tables/finance_pettycash_reimbursement_approve_by.php";		$finance_pettycash_reimbursement_approve_by	= new finance_pettycash_reimbursement_approve_by($db);
require_once "libraries/jqgrid2/inc/jqgrid_dist.php";

$select				= "MAX(APPROVE_LEVELS) AS APP_LEVELS";
$db->connect();$db->begin();
$header_result		= $finance_pettycash_reimbursement_approve_by->select($select,NULL,$where=NULL,NULL,NULL);	
$header_array		= mysqli_fetch_array($header_result);
$approveLevel 		= $header_array['APP_LEVELS'];
//BEGIN - ADD DEFAULT WHERE STRING WHEN FORM LOADING {
$arr =  json_decode($_REQUEST['filters'],true);
//print_r($arr);

$arr = $arr['rules'];

$where_string = '';
$where_array = array(
					'Status'=>'ORH.STATUS',
					'CONCAT_REIMBURSEMENT_NO'=>"CONCAT(ORH.REIMBURSEMENT_NO,'/',ORH.REIMBURSEMENT_YEAR )",
					'LOCATION'=>"L.strName",
					'DATE'=>'DATE'
					);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Revised'=>'-1','Cancelled'=>'-2','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}

if(!count($arr)>0)					 
		$where_string .= "AND DATE(ORH.DATE) = '".date('Y-m-d')."'";
//END }

$sql = "SELECT SUB_1.* FROM
			(SELECT
			  CONCAT(ORH.REIMBURSEMENT_NO,'/',ORH.REIMBURSEMENT_YEAR )			AS CONCAT_REIMBURSEMENT_NO,
			  ORH.REIMBURSEMENT_NO												AS REIMBURSEMENT_NO,
			  ORH.REIMBURSEMENT_YEAR											AS REIMBURSEMENT_YEAR,
			  ORH.DATE 															AS DATE,
			  if(ORH.STATUS=1,'Approved',if(ORH.STATUS=0,'Rejected',if(ORH.STATUS='-2','Cancelled','Pending'))) as Status,
			  U.strUserName														AS SAVED_BY,
			  L.strName 														AS LOCATION,
			  ORH.REMARKS 														AS REMARK,
			  ";
				  
		$sql .= "IFNULL((
				SELECT
				concat(U.strUserName,'(',max(ORA.APPROVED_DATE),')' )
				FROM
				finance_pettycash_reimbursement_approve_by ORA
				Inner Join sys_users U
					ON ORA.APPROVED_BY = U.intUserId
				WHERE
				ORA.REIMBURSEMENT_NO  = ORH.REIMBURSEMENT_NO AND
				ORA.REIMBURSEMENT_YEAR =  ORH.REIMBURSEMENT_YEAR AND
				ORA.APPROVE_LEVELS =  '1' AND
				ORA.STATUS =  '0'
				),IF(((SELECT
				MP.int1Approval 
				FROM menupermision MP
				Inner Join menus M ON MP.intMenuId = M.intId
				WHERE
				M.strCode = '$programCode' AND
				MP.intUserId =  '$intUser')=1 AND ORH.STATUS>1),'Approve', '')) as `1st_Approval`, ";
			
	for($i=2; $i<=$approveLevel; $i++){		
		if($i==2){
			$approval	= "2nd_Approval";
		}
		else if($i==3){
			$approval	= "3rd_Approval";
		}
		else {
			$approval	= $i."th_Approval";
		}
		
		
		$sql .= "IFNULL(
		(
		SELECT
		concat(U.strUserName,'(',max(ORA.APPROVED_DATE),')' )
		FROM
		finance_pettycash_reimbursement_approve_by ORA
		Inner Join sys_users U ON ORA.APPROVED_BY = U.intUserId
		WHERE
		ORA.REIMBURSEMENT_NO  = ORH.REIMBURSEMENT_NO AND
		ORA.REIMBURSEMENT_YEAR =  ORH.REIMBURSEMENT_YEAR AND
		ORA.APPROVE_LEVELS =  '$i' AND
		ORA.STATUS = 0
		),
		IF(
		((SELECT
		MP.int".$i."Approval 
		FROM menupermision MP
		Inner Join menus M 
			ON MP.intMenuId = M.intId
		WHERE
			M.strCode = '$programCode' AND
			MP.intUserId =  '$intUser')=1 AND (ORH.STATUS>1) AND (ORH.STATUS<=ORH.APPROVE_LEVELS) AND ((SELECT
		concat(U.strUserName )
		FROM
		finance_pettycash_reimbursement_approve_by ORA
		Inner Join sys_users U ON ORA.APPROVED_BY = U.intUserId
		WHERE
		ORA.REIMBURSEMENT_NO  = ORH.REIMBURSEMENT_NO AND
		ORA.REIMBURSEMENT_YEAR =  ORH.REIMBURSEMENT_YEAR AND
		ORA.APPROVE_LEVELS =  ($i-1) AND
		ORA.STATUS = '0' )<>'')), 'Approve',
		if($i>ORH.APPROVE_LEVELS,'-----',''))
		
		) as `".$approval."`, "; 
		
		}
	$sql .= "IFNULL((SELECT
								concat(U.strUserName,'(',max(ORA.APPROVED_DATE),')' )
								FROM
								finance_pettycash_reimbursement_approve_by ORA
								Inner Join sys_users U ON ORA.APPROVED_BY = U.intUserId
								WHERE
								ORA.REIMBURSEMENT_NO  = ORH.REIMBURSEMENT_NO AND
								ORA.REIMBURSEMENT_YEAR =  ORH.REIMBURSEMENT_YEAR AND
								ORA.APPROVE_LEVELS =  '-2' AND
								ORA.STATUS =  '0'
							),IF(((SELECT
								menupermision.intCancel 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND 
								ORH.STATUS=1),'Cancel', '')) as `Cancel`,";
				  
	$sql .= " 'View'											AS VIEW,
			  'Report'											AS REPORT
			  FROM finance_pettycash_reimbursement ORH
			  INNER JOIN sys_users U ON ORH.CREATED_BY = U.intUserId
			  INNER JOIN mst_locations L ON ORH.LOCATION_ID = L.intId
	          WHERE ORH.USER_LOCATION_ID='$location'
			$where_string
			)  

		AS SUB_1 WHERE 1=1";
					  	//die($sql);
					   
$formLink  					= "?q=".$formID."&reimbursementNo={REIMBURSEMENT_NO}&reimbursementYear={REIMBURSEMENT_YEAR}";
$reportLink  				= "?q=".$reportID."&reimbursementNo={REIMBURSEMENT_NO}&reimbursementYear={REIMBURSEMENT_YEAR}";
$reportLinkApprove  		= "?q=".$reportID."&reimbursementNo={REIMBURSEMENT_NO}&reimbursementYear={REIMBURSEMENT_YEAR}&mode=Confirm";
$reportLinkCancel	  		= "?q=".$reportID."&reimbursementNo={REIMBURSEMENT_NO}&reimbursementYear={REIMBURSEMENT_YEAR}&mode=Cancel";
					   
$col  = array();
$cols = array();

//STATUS
$col["title"] 				= "Status"; // caption of column
$col["name"] 				= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "3";
//edittype
$col["stype"] 				= "select";
$str 						= ":All;Pending:Pending;Approved:Approved;Rejected:Rejected;Cancelled:Cancelled" ;
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
//searchOper
$col["align"] 				= "center";

$cols[] = $col;	$col=NULL;

$col["title"]			= "REIMBURSEMENT_NO";
$col["name"]			= "REIMBURSEMENT_NO";
$col["width"]			= "1";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$col["hidden"]			= true;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "REIMBURSEMENT_YEAR";
$col["name"]			= "REIMBURSEMENT_YEAR";
$col["width"]			= "1";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$col["hidden"]			= true;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Bill Invoice No";
$col["name"]			= "CONCAT_REIMBURSEMENT_NO";
$col["width"]			= "3";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$col["link"]			= $formLink;
$col["linkoptions"]		= "target='reimbursement.php'";
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Reference No";
$col["name"]			= "REFERENCE_NO";
$col["width"]			= "3";
$col["align"]			= "left";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Invoice Date";
$col["name"]			= "DATE";
$col["width"]			= "3";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Location";
$col["name"]			= "LOCATION";
$col["width"]			= "3";
$col["align"]			= "left";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Currency";
$col["name"]			= "CURRENCY";
$col["width"]			= "3";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Remark";
$col["name"]			= "REMARK";
$col["width"]			= "3";
$col["align"]			= "left";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$cols[]			    	= $col;
$col					= NULL;

$col["title"]			= "Saved By";
$col["name"]			= "SAVED_BY";
$col["width"]			= "3";
$col["align"]			= "center";
$col["sortable"]		= true;
$col["search"]			= true;
$col["editable"]		= false;
$cols[]			    	= $col;
$col					= NULL;

//FIRST APPROVAL
$col["title"] 				= "1st Approval"; // caption of column
$col["name"] 				= "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "5";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= $reportLinkApprove;

$col['linkName']			= 'Approve';
$col["linkoptions"] 		= "target='reimbursement_report.php'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap			="2nd Approval";
	$ap1		="2nd_Approval";
	}
	else if($i==3){
	$ap			="3rd Approval";
	$ap1		="3rd_Approval";
	}
	else {
	$ap			=$i."th Approval";
	$ap1		=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] 			= $ap; // caption of column
$col["name"] 			= $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 			= "5";
$col["search"] 			= false;
$col["align"] 			= "center";
$col['link']			= $reportLinkApprove;
$col['linkName']		= 'Approve';
$col["linkoptions"] 	= "target='reimbursement_report.php'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}

//FIRST APPROVAL
$col["title"] 				= 'Cancel'; 
$col["name"] 				= 'Cancel'; 
$col["width"] 				= "4";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= $reportLinkCancel;
$col['linkName']			= 'Cancel';
$col["linkoptions"] 		= "target='reimbursement_report.php'"; // extra params with <a> tag
$cols[] = $col;	$col		= NULL;
//VIEW
$col["title"] 				= "Report"; // caption of column
$col["name"] 				= "REPORT"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 				= "3";
$col["search"] 				= false;
$col["align"] 				= "center";
$col['link']				= $reportLink;
$col["linkoptions"] 		= "target='reimbursement_report.php'"; // extra params with <a> tag
$cols[] 					= $col;	
$col						= NULL;

//$d	=date('Y-m-d');
$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"All"}
     ]

}
SEARCH_JSON;

$jq = new jqgrid('',$db);
$grid["caption"] 		= "Petty Cash Reimbursement Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'REIMBURSEMENT_NO'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
 	<?php 
		// include "include/listing.html";
	?>
 <title>Petty Cash Reimbursement Listing</title>
 <form id="frmlisting" name="frmlisting" method="post" action="">

        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
</form>
 <?php
 
 $db->disconnect();

 ?>
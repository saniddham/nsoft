<?php 
include_once("class/tables/mst_locations.php");									$mst_locations								= new mst_locations($db);
include_once "class/tables/finance_pettycash_reimbursement.php";				$finance_pettycash_reimbursement			= new finance_pettycash_reimbursement($db);
include_once "class/dateTime.php";												$dateTimes									= new dateTimes($db);

$db->connect();

$programCode	= 'P0850';
$locationId		= $sessions->getLocationId();
$userId			= $sessions->getUserId();
$companyId		= $sessions->getCompanyId();

$reimbursementNo	= $_REQUEST['reimbursementNo'];
$reimbursementYear	= $_REQUEST['reimbursementYear'];

if($reimbursementNo != '' && $reimbursementYear != '')
{
	$finance_pettycash_reimbursement->set($reimbursementNo,$reimbursementYear);
	$date			= $finance_pettycash_reimbursement->getDATE();
	$amount			= $finance_pettycash_reimbursement->getAMOUNT();
	$reim_location	= $finance_pettycash_reimbursement->getLOCATION_ID();
	$remark			= $finance_pettycash_reimbursement->getREMARKS();
	$status			= $finance_pettycash_reimbursement->getSTATUS();
	$approveLevel	= $finance_pettycash_reimbursement->getAPPROVE_LEVELS();
	$locationId		= $finance_pettycash_reimbursement->getUSER_LOCATION_ID();	
	
	$save_perm		= $menupermision->checkMenuPermision('Edit',$status,$approveLevel);
	$appr_perm		= $menupermision->checkMenuPermision('Approve',$status,$approveLevel);
	$cancel_perm	= $menupermision->checkMenuPermision('Cancel',$status,$approveLevel);
}
else
	$save_perm		= $menupermision->checkMenuPermision('Edit','null','null');
?>
<title>Petty Cash Reimbursement</title>
<script>
var dateNow    = "<?php echo $dateTimes->getCurruntDate();?>";
</script>

<form id="frmReimbursement" name="frmReimbursement" autocomplete="off" method="post">
<div align="center">
	<div class="trans_layoutS" style="width:500px">
	<div class="trans_text">Petty Cash Reimbursement</div>
    <table width="500px" border="0" align="center" bgcolor="#FFFFFF">
    <tr class="normalfnt">
    	<td><table width="100%" border="0">
    	      <tr class="normalfnt">
    	        <td>Serial No</td>
    	        <td colspan="2"><input type="text" name="txtReimbursementNo" id="txtReimbursementNo" style="width:60px" value="<?php echo $reimbursementNo?>" disabled="disabled"/> <input type="text" name="txtReimbursementYear" id="txtReimbursementYear" style="width:40px" value="<?php echo $reimbursementYear ?>" disabled="disabled"/></td>
  	        </tr>
   			<tr>
   	        <td>Date<span class="compulsoryRed"></span></td>
    	        <td colspan="2"><input name="dtDate" type="text" value="<?php echo($reimbursementNo==''?date("Y-m-d"):$date); ?>" class="txtbox validate[required]" id="dtDate" style="width:107px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
  	        </tr>
    	      <tr class="normalfnt">
    	        <td>Amount<span class="compulsoryRed">*</span></td>
    	        <td colspan="2">
                <input type="text" name="txtAmount" id="txtAmount" class="validate[required,decimal[2],custom[number]] cls_input_number_validation" tabindex="3" value="<?php echo $amount?>" style="width:107px"/>
               </td>
  	        </tr>
    	      <tr class="normalfnt">
    	        <td>Location<span class="compulsoryRed">*</span></td>
    	        <td colspan="2"> <select name="cboLocation" id="cboLocation" class="validate[required]" style="width:200px">
                
                 <?php 
				 if($reimbursementNo == '' && $reimbursementYear == '')
				 	echo $mst_locations->getCombo(NULL,"intStatus = 1");
				else
					echo $mst_locations->getCombo($reim_location,"intStatus = 1");
						
				?>
   	            </select></td>
  	        </tr>

	   	      <tr class="normalfnt">
    	        <td valign="top">Remarks</td>
    	        <td colspan="2" valign="top"><textarea name="txtRemarks" id="txtRemarks" style="width:325px;" rows="3"><?php echo $remark; ?></textarea></td>
  	        </tr>
  	      </table></td>
  	    </tr>
    	<tr>
        <td height="32" >
        <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center" ><a href="?q=1110" class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butSave" name="butSave" style=" <?php if($save_perm['type']){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>">Save</a><a class="button white medium" id="butConfirm" name="butConfirm" style=" <?php if($appr_perm['type']){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>">Approve</a><a class="button white medium" id="butCancle" name="butCancle" style=" <?php if($cancel_perm['type']){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>">Cancel</a><a class="button white medium" id="butReport" name="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
        </tr>
        </table>
        </td>
    </tr>
    </table>
    </div>
</div>
</form>

// JavaScript Document
var menuID 		= '1110';
var reportID	= '1112';
$(document).ready(function(e) {
    $('#frmReimbursement #butSave').die('click').live('click',saveData);
	$('#frmReimbursement #butConfirm').die('click').live('click',urlApprove);
	$('#frmReimbursementRpt #butRptConfirm').die('click').live('click',approve);
	$('#frmReimbursementRpt #butRptReject').die('click').live('click',reject);
	$('#frmReimbursementRpt #butRptCancel').die('click').live('click',cancel);
	$('#frmReimbursement #butReport').die('click').live('click',urlReport);
	$('#frmReimbursement #butCancle').die('click').live('click',urlCancel);

});

function saveData()
{
	showWaiting();
	var reimbursementNo		= $('#frmReimbursement #txtReimbursementNo').val();	
	var reimbursementYear	= $('#frmReimbursement #txtReimbursementYear').val();	
	var date				= $('#frmReimbursement #dtDate').val();	
	var locationId			= $('#frmReimbursement #cboLocation').val();	
	var amount				= $('#frmReimbursement #txtAmount').val();	
	var remarks				= $('#frmReimbursement #txtRemarks').val();	
	
	var data	= "requestType=saveData";
	if($('#frmReimbursement').validationEngine('validate'))
	{
		var data = "requestType=saveData";
		var arrHeader = "{";
							arrHeader += '"reimbursementNo":"'+reimbursementNo+'",' ;
							arrHeader += '"reimbursementYear":"'+reimbursementYear+'",' ;
							arrHeader += '"date":"'+date+'",' ;
							arrHeader += '"locationId":"'+locationId+'",' ;
							arrHeader += '"amount":"'+amount+'",' ;
							arrHeader += '"remarks":'+URLEncode_json(remarks)+'' ;
			arrHeader += " }";
		
		data+="&arrHeader="+arrHeader;
		
		var url = "controller.php?q="+menuID;
			$.ajax({
					url:url,
					dataType:'json',
					type:'post',
					data:data,
					async:false,
					success:function(json){
						$('#frmReimbursement #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
						{
							$('#frmReimbursement #txtReimbursementNo').val(json.reimbursementNo);
							$('#frmReimbursement #txtReimbursementYear').val(json.reimbursementYear);
							if(json.app_perm)
								$('#frmReimbursement #butConfirm').show();
							if(json.cancel_perm)
								$('#frmReimbursement #butCancle').show();
							hideWaiting();
							return;
						}
						else
						{
							hideWaiting();
						}
					},
					error:function(xhr,status){
							
							$('#frmReimbursement #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							hideWaiting();
							return;
					}		
			});
		
	}
	else
	{
		hideWaiting();
	}
}

function approve()
{
	var val = $.prompt('Are you sure you want to approve this Reimbursement ?',{
	buttons: { Ok: true, Cancel: false },
	callback: function(v,m,f){
	if(v)
	{
		showWaiting();
		var url = "controller.php?q="+menuID+window.location.search+'&requestType=approve';
		var obj = $.ajax({
			url:url,
			type:'post',
			dataType: "json",  
			data:'',
			async:false,
			
			success:function(json){
					$('#frmReimbursementRpt #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertxR1()",10000);
						window.location.href = window.location.href;
						if(window.opener.location.pathname=='/nsoft/presentation/finance_new/petty_cash/petty_cash_invoice/petty_cash_invoice_listing.php')
						{
							window.opener.location.reload();//reload listing page
						}
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmReimbursementRpt #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertxR1()",3000);
					return;
				}		
			});
		hideWaiting();
		}
	
}});	
}

function reject()
{
	var val = $.prompt('Are you sure you want to reject this Reimbursement ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
		if(v)
		{
			showWaiting();
			var url = "controller.php?q="+menuID+window.location.search+'&requestType=reject';
			var obj = $.ajax({
				url:url,
				type:'post',
				dataType: "json",  
				data:'',
				async:false,
				
				success:function(json){
						$('#frmReimbursementRpt #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
						{
							var t=setTimeout("alertxR2()",1000);
							window.location.href = window.location.href;
							if(window.opener.location.pathname=='/nsoft/presentation/finance_new/petty_cash/petty_cash_invoice/petty_cash_invoice_listing.php')
							{
								window.opener.location.reload();//reload listing page
							}

							return;
						}
					},
				error:function(xhr,status){
						
						$('#frmReimbursementRpt #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertxR2()",3000);
						return;
					}		
				});
			hideWaiting();
			}
		
	}});
	
}
function cancel()
{
	var val = $.prompt('Are you sure you want to Cancel this invoice ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url 	 ="controller.php?q="+menuID+window.location.search+'&requestType=cancel';
					
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType:'json',  
						async:false,
						success:function(json){
					$('#frmReimbursementRpt #butRptCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//var t=setTimeout("alertx1()",1000);
						window.location.href = window.location.href;
						window.opener.location.reload();
						return;
					}
					 hideWaiting();
					},
					error:function(xhr,status){						
							$('#frmReimbursementRpt #butRptCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							//var t=setTimeout("alertx1()",3000);
							 hideWaiting();
					}		
						});
					
					}
				
			}});	
}
function urlApprove()
{
	var url  = "?q="+reportID+"&reimbursementNo="+$('#frmReimbursement #txtReimbursementNo').val();
	    url += "&reimbursementYear="+$('#frmReimbursement #txtReimbursementYear').val();
	    url += "&mode=Confirm";
	window.open(url,'rpt_petty_cash_invoice.php');	
}
function urlReport()
{
	var url  = "?q="+reportID+"&reimbursementNo="+$('#frmReimbursement #txtReimbursementNo').val();
	    url += "&reimbursementYear="+$('#frmReimbursement #txtReimbursementYear').val();
	window.open(url,'rpt_petty_cash_invoice.php');	
}
function urlCancel()
{
	var url  = "?q="+reportID+"&reimbursementNo="+$('#frmReimbursement #txtReimbursementNo').val();
	    url += "&reimbursementYear="+$('#frmReimbursement #txtReimbursementYear').val();
	    url += "&mode=Cancel";
	window.open(url,'rpt_petty_cash_invoice.php');	
}
//function clearAll()
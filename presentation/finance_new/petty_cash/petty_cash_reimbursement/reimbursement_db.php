<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php 

try{
	include_once "class/tables/menupermision.php";									$menupermision								= new menupermision($db);
	include_once "class/tables/sys_approvelevels.php";								$sys_approvelevels							= new sys_approvelevels($db);
	include_once "class/tables/sys_no.php";											$sys_no										= new sys_no($db);
	include_once "class/dateTime.php";												$dateTimes									= new dateTimes($db);
	include_once "class/tables/finance_pettycash_reimbursement.php";				$finance_pettycash_reimbursement			= new finance_pettycash_reimbursement($db);
	include_once "class/tables/finance_pettycash_reimbursement_approve_by.php";		$finance_pettycash_reimbursement_approve_by	= new finance_pettycash_reimbursement_approve_by($db);
	include_once "class/tables/finance_pettycash_transaction.php";					$finance_pettycash_transaction				= new finance_pettycash_transaction($db);
	include_once "class/tables/finance_pettycash_budget_details.php";				$finance_pettycash_budget_details					= new finance_pettycash_budget_details($db);
		
	$requestType	= $_REQUEST['requestType'];
	$programCode	= 'P1110';
	$userId			= $sessions->getUserId();
	$location		= $sessions->getLocationId();
			
	if($requestType == 'saveData')
	{
		$month 				= date("m");
		$year 				= date("Y");
		$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
		
		$db->connect();$db->begin();
		
		$reimbursementNo	= $arrHeader['reimbursementNo'];
		$reimbursementYear	= $arrHeader['reimbursementYear'];
		$date				= $arrHeader['date'];
		$locationId			= $arrHeader["locationId"];
		$amount				= $arrHeader['amount'];
		$remarks			= $arrHeader['remarks'];
		
		$editMode			= false;
			
		$menupermision->set($programCode,$userId);
	
		## get approve levels and status
		$sys_approvelevels->set($programCode);
		$approveLevel		= $sys_approvelevels->getintApprovalLevel();
		$status				= $approveLevel+1;
		$pettyBudget		= $finance_pettycash_budget_details->getBudgetAmount($month,$year,$locationId);
		############################################################
		## check whether the budget entered to current month	  ##
		############################################################
		if($pettyBudget =='' || $pettyBudget == 0)
			throw new Exception('Please enter budget before reimbursement');	
			
		############################################################
		## check whether reimburse amount exceeds budget  		  ##
		############################################################
		$pettyBal	= $finance_pettycash_transaction->getTotalPettyBal($location);
		$totAmount	= $pettyBal + $amount;
		if($totAmount > $pettyBudget)
			throw new Exception('petty cash balance can not exceeds budget amount<br>Budget Amount: '.$pettyBudget.'<br>Current Balance: '.$pettyBal);			
		
		if($reimbursementNo == '' && $reimbursementYear == '')	
		{
			####################################################
			## 1. check permission							  ##
			####################################################
			$permissionArr	= $menupermision->checkMenuPermision('Edit',NULL,NULL);
			if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);	
				
			$reimbursementNo	= $sys_no->getSerialNoAndUpdateSysNo('PETTY_CASH_REIMBURSEMENT_NO',$location);
			$reimbursementYear	= $dateTimes->getCurruntYear();	
			
			$resultHArr			= $finance_pettycash_reimbursement->insertRec($reimbursementNo,$reimbursementYear,$date,$locationId,$amount,$remarks,$status,$approveLevel,$location,$userId,$dateTimes->getCurruntDateTime(),'NULL',$dateTimes->getCurruntDateTime());
			if(!$resultHArr['status'])
				throw new Exception($resultHArr['msg']);

		}
		else
		{
			$finance_pettycash_reimbursement->set($reimbursementNo,$reimbursementYear);
			$status_saved			= $finance_pettycash_reimbursement->getSTATUS();
			$approveLevel_saved		= $finance_pettycash_reimbursement->getAPPROVE_LEVELS();
			
			$permissionArr	= $menupermision->checkMenuPermision('Edit',$status_saved,$approveLevel_saved);

			if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);
				
			####################################################
			## 2.check whether saved location			  	  ##
			####################################################
			
			$savedLocation	= $finance_pettycash_reimbursement->getUSER_LOCATION_ID();
			if($savedLocation != $location)
					throw new Exception("This is not saved location");
			
			$finance_pettycash_reimbursement->setDATE($date);
			$finance_pettycash_reimbursement->setLOCATION_ID($locationId);
			$finance_pettycash_reimbursement->setAMOUNT($amount);
			$finance_pettycash_reimbursement->setREMARKS($remarks);
			$finance_pettycash_reimbursement->setSTATUS($status);
			$finance_pettycash_reimbursement->setAPPROVE_LEVELS($approveLevel);
			$finance_pettycash_reimbursement->setMODIFIED_BY($userId);
			$finance_pettycash_reimbursement->commit();
			
			$editMode	= true;		
			
		}
		
		$finance_pettycash_reimbursement_approve_by->updateMaxStatus($reimbursementNo,$reimbursementYear);
		
		if($status == 1)
			insertToTransaction($reimbursementNo,$reimbursementYear);
		
		$appPermArr		= $menupermision->checkMenuPermision('Approve',$status,$approveLevel);
		$app_perm		= $appPermArr['type'];
		$cancelPermArr	= $menupermision->checkMenuPermision('Cancel',$status,$approveLevel);
		$cancel_perm	= $cancelPermArr['type'];

		$db->commit();
		$response['type'] 				= "pass";
		if($editMode == 1)
			$response['msg'] 			= "Updated Successfully.";
		else
			$response['msg'] 			= "Saved Successfully.";
		$response['reimbursementNo']	= $reimbursementNo;
		$response['reimbursementYear']	= $reimbursementYear;
		$response['app_perm']			= $app_perm;
		$response['cancel_perm']		= $cancel_perm;
		echo json_encode($response);
			
	}
	else if($requestType == 'approve')
	{
		$db->connect();$db->begin();	
		$reimbursementNo	= $_REQUEST['reimbursementNo'];
		$reimbursementYear	= $_REQUEST['reimbursementYear'];
		
		## get header status and approve level	
		$finance_pettycash_reimbursement->set($reimbursementNo,$reimbursementYear);
		$status				= $finance_pettycash_reimbursement->getSTATUS(); 
		$approveLevels		= $finance_pettycash_reimbursement->getAPPROVE_LEVELS();
		$amount				= $finance_pettycash_reimbursement->getAMOUNT();
		$date				= $finance_pettycash_reimbursement->getDATE();
		$reimLoc			= $finance_pettycash_reimbursement->getLOCATION_ID();
		$menupermision->set($programCode,$userId);
		
		$month 				= date("m",strtotime($date));
		$year 				= date("Y",strtotime($date));
		
		####################################################
		## 1.check Approve permission					  ##
		####################################################
		$permissionArr	= $menupermision->checkMenuPermision('Approve',$status,$approveLevels);
		if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);
		
		####################################################
		## 3.check whether saved location			  	  ##
		####################################################
		$user_location	= $finance_pettycash_reimbursement->getUSER_LOCATION_ID();
		if($user_location != $location)
				throw new Exception("This is not saved location");
				
		$pettyBudget		= $finance_pettycash_budget_details->getBudgetAmount($month,$year,$reimLoc);
		############################################################
		## check whether the budget entered to current month	  ##
		############################################################
		if($pettyBudget =='' || $pettyBudget == 0)
			throw new Exception('Please enter budget before reimbursement approval');	
			
		############################################################
		## check whether reimburse amount exceeds budget  		  ##
		############################################################
		$pettyBal	= $finance_pettycash_transaction->getTotalPettyBal($location);
		$totAmount	= $pettyBal + $amount;
		if($totAmount > $pettyBudget)
			throw new Exception('petty cash balance can not exceeds budget amount<br>Budget Amount: '.$pettyBudget.'<br>Current Balance: '.$pettyBal);		
			
				
		## Update header status				
		$where = "REIMBURSEMENT_NO = '$reimbursementNo' AND REIMBURSEMENT_YEAR= '$reimbursementYear'";
		
		$data  =array(	'STATUS'=> -1
		);
		$result_update_h		= $finance_pettycash_reimbursement->upgrade($data,$where);
		if(!$result_update_h['status']) 
			throw new Exception($result_update_h['msg']);
		
		## insert to approve by table				  	  

		$finance_pettycash_reimbursement->set($reimbursementNo,$reimbursementYear);
		$hstatus		= $finance_pettycash_reimbursement->getSTATUS();
		$aplevel		= $finance_pettycash_reimbursement->getAPPROVE_LEVELS();
		$level			= (int)$aplevel+1- (int)$hstatus; 
		$inserToApprovedBy	= $finance_pettycash_reimbursement_approve_by->insertRec($reimbursementNo,$reimbursementYear,$level,$userId,$dateTimes->getCurruntDateTime(),0);
		if(!$inserToApprovedBy['status']) 
			throw new Exception($inserToApprovedBy['msg']);
		
		if($hstatus == 1)
		{
			insertToTransaction($reimbursementNo,$reimbursementYear);
		}
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Approved Successfully.";	
		$db->disconnect();
		echo json_encode($response);
	
	}
	else if($requestType == 'reject')
	{
		$db->connect();$db->begin();
		$reimbursementNo	= $_REQUEST['reimbursementNo'];
		$reimbursementYear	= $_REQUEST['reimbursementYear'];
		
		
		$finance_pettycash_reimbursement->set($reimbursementNo,$reimbursementYear);
		$status				= $finance_pettycash_reimbursement->getSTATUS(); 
		$approveLevels		= $finance_pettycash_reimbursement->getAPPROVE_LEVELS();

		$menupermision->set($programCode,$userId);
		
		## check Reject permission	
		$permissionArr	= $menupermision->checkMenuPermision('Reject',$status,$approveLevels);
		if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);
		
		## check whether saved location	
		$saved_location	= $finance_pettycash_reimbursement->getUSER_LOCATION_ID();
		if($saved_location != $location)
				throw new Exception("This is not saved location");
						
		$finance_pettycash_reimbursement->setSTATUS(0);
		$finance_pettycash_reimbursement->commit();
		
		$inserToApprovedBy	= $finance_pettycash_reimbursement_approve_by->insertRec($reimbursementNo,$reimbursementYear,0,$userId,$dateTimes->getCurruntDateTime(),0);
		if(!$inserToApprovedBy['status']) 
			throw new Exception($inserToApprovedBy['msg']);
				
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Rejected Successfully.";
		$db->disconnect();	
		echo json_encode($response);	
	}
	else if($requestType == 'cancel')
	{
		$db->connect();$db->begin();
		$reimbursementNo	= $_REQUEST['reimbursementNo'];
		$reimbursementYear	= $_REQUEST['reimbursementYear'];
		## get header status and approve level	
		$finance_pettycash_reimbursement->set($reimbursementNo,$reimbursementYear);
		$status				= $finance_pettycash_reimbursement->getSTATUS(); 
		$approveLevels		= $finance_pettycash_reimbursement->getAPPROVE_LEVELS();
		$date				= $finance_pettycash_reimbursement->getDATE();
		$reimLocation		= $finance_pettycash_reimbursement->getLOCATION_ID();
		$month 				= date("m",strtotime($date));
		$year 				= date("Y",strtotime($date));
		

		$menupermision->set($programCode,$userId);
		
		####################################################
		## 1.check Cancel permission					  ##
		####################################################
		$permissionArr	= $menupermision->checkMenuPermision('Cancel',$status,$approveLevels);
		if(!$permissionArr['type'])
				throw new Exception($permissionArr['msg']);
		
		####################################################
		## 2.check whether saved location			  	  ##
		####################################################
		$saved_location	= $finance_pettycash_reimbursement->getUSER_LOCATION_ID();
		if($saved_location != $location)		
			throw new Exception("This is not saved location");
	
		####################################################
		## 3.update header status  					 	  ##
		####################################################
		$finance_pettycash_reimbursement->setSTATUS(-2);
		$finance_pettycash_reimbursement->commit();
		
		## update approved by table	 				

		$inserToApprovedBy	= $finance_pettycash_reimbursement_approve_by->insertRec($reimbursementNo,$reimbursementYear,-2,$userId,$dateTimes->getCurruntDateTime(),0);
		if(!$inserToApprovedBy['status']) 
			throw new Exception($inserToApprovedBy['msg']);
			
		####################################################
		## 4.rollback finance_pettycash_transaction		  ##
		#################################################### 
		$where = "DOCUMENT_NO = '$reimbursementNo' AND DOCUMENT_YEAR= '$reimbursementYear' AND DOCUMENT_TYPE='REIMBURSEMENT'";
		$deleteTransArr	= $finance_pettycash_transaction->delete($where);
		if(!$deleteTransArr['type'])
			throw new Exception($deleteTransArr['msg']);
		
		$cashBal		= $finance_pettycash_transaction->getPettyCashBalance($month,$year,$reimLocation);
		//die($cashBal);
		if($cashBal<0)
			throw new Exception('Petty cash balance can not be less than zero');
		
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "cancel Successfully.";
		echo json_encode($response);
		$db->disconnect();	
				
	}
}
catch(Exception $e)
{
	$db->rollback();
	$response['msg'] 	=  $e->getMessage();
	$response['error'] 	=  $error_handler->jTraceEx($e);
	$response['type']	=  'fail';
	$response['sql']	=  $db->getSql();
	$db->disconnect();		
	echo json_encode($response);
}

function insertToTransaction($reimbursementNo,$reimbursementYear)
{
	global $finance_pettycash_transaction;
	global $finance_pettycash_reimbursement;
	global $dateTimes;
	global $userId;
		
	$finance_pettycash_reimbursement->set($reimbursementNo,$reimbursementYear);
	$reim_amount	= $finance_pettycash_reimbursement->getAMOUNT();
	$reim_location	= $finance_pettycash_reimbursement->getLOCATION_ID();
	$reim_date		= $finance_pettycash_reimbursement->getDATE();
	$docType		= 'REIMBURSEMENT';
	$insertArr		= $finance_pettycash_transaction->insertRec($reim_location,$reimbursementNo,$reimbursementYear,$docType,'NULL','NULL','NULL','NULL','NULL',$reim_amount,$reim_amount,$reim_date,$userId,$dateTimes->getCurruntDateTime());
	if(!$insertArr['type'])
		throw new Exception($insertArr['msg']);
		
}

?>
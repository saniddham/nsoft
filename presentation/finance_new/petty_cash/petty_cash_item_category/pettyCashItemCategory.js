var menuID = 1075;
$(document).ready(function(e) {
	$('#frmPettyCahItemCategory #txtCategoryCode').focus();
   $('#frmPettyCahItemCategory #cboFinanceType').die('change').live('change',function(){loadMainType($('#frmPettyCahItemCategory #cboFinanceType').val())}); 
   $('#frmPettyCahItemCategory #cboMainType').die('change').live('change',function(){loadSubType($('#frmPettyCahItemCategory #cboMainType').val())}); 
   $('#frmPettyCahItemCategory #cboSubType').die('change').live('change',function(){loadChartOfAcc($('#frmPettyCahItemCategory #cboSubType').val())}); 
   $('#frmPettyCahItemCategory #butSave').die('click').live('click',saveData); 
   $('#frmPettyCahItemCategory #cboSearch').die('change').live('change',searchCategory); 
   $('#frmPettyCahItemCategory #butDelete').die('click').live('click',Delete);
   });

function loadCategory()
{
	var url 			= "controller.php?q="+menuID+"&requestType=loadCategory";
	var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json){
				$('#frmPettyCahItemCategory #cboSearch').html(json.cboCategory);
			}
		})	
}
function loadMainType(type)
{
	$('#frmPettyCahItemCategory #cboSubType').val('');
	$('#frmPettyCahItemCategory #cboChartOfAccount').val('');
	var financeType		= type;
	var url 			= "controller.php?q="+menuID+"&requestType=loadMainType&financeType="+financeType;
	var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json){
				$('#frmPettyCahItemCategory #cboMainType').html(json.mainTypeCombo);
			}
		})
}
function loadSubType(type)
{
	$('#frmPettyCahItemCategory #cboChartOfAccount').val('');
	var mainType 		= type;
	var url 			= "controller.php?q="+menuID+"&requestType=loadSubType&mainType="+mainType;
	var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json){
				$('#frmPettyCahItemCategory #cboSubType').html(json.subTypeCombo);
			}
		})
}
function loadChartOfAcc(type)
{
	var subType	= type;
	var url 		= "controller.php?q="+menuID+"&requestType=loadChartOfAcc&subType="+subType;
	var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json){
				$('#frmPettyCahItemCategory #cboChartOfAccount').html(json.chrtOfAccCombo);
			}
		})
}
function saveData()
{
	if(!$('#frmPettyCahItemCategory').validationEngine('validate'))   
    	return ;
		
	showWaiting();
	var data			= "requestType=saveData";
	var category		= $("#frmPettyCahItemCategory #cboSearch").val();
	var glID			= $('#frmPettyCahItemCategory #cboChartOfAccount').val();
	var categoryCode	= $('#frmPettyCahItemCategory #txtCategoryCode').val();
	var categoryName	= $('#frmPettyCahItemCategory #txtCategoryName').val();
	var status			= ($('#frmPettyCahItemCategory #chkActive').attr('checked')?1:0);
	var iouStatus		= ($('#frmPettyCahItemCategory #chkIOU').attr('checked')?1:0);

	var dataArr		= "{";	
			dataArr	   += '"category":"'+category+'",';
			dataArr	   += '"glID":"'+glID+'",';
			dataArr	   += '"categoryCode":"'+categoryCode+'",';
			dataArr	   += '"categoryName":"'+categoryName+'",';
			dataArr	   += '"status":"'+status+'",';
			dataArr	   += '"iouStatus":"'+iouStatus+'"';
			dataArr	   += "}";
	data	   += "&dataArr="+dataArr;			
	var url = "controller.php?q="+menuID;
	var obj = $.ajax({
		url:url,
		dataType: "json",
		type:'POST',
		data:data,  
		async:false,			
		success:function(json){
				$('#frmPettyCahItemCategory #butSave').validationEngine('showPrompt', json.msg,json.type);
				if(json.type=='pass')
				{
					setTimeout("alertx()",1000);
					$('#frmPettyCahItemCategory').get(0).reset();
					loadCategory();
					return;
				}
			},
		error:function(xhr,status){
				$('#frmPettyCahItemCategory #butSave').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
				hideWaiting();
				setTimeout("alertx()",1000);
			}		
		});
	hideWaiting();	
}

function searchCategory()
{
	var category	= $('#frmPettyCahItemCategory #cboSearch').val();
	if(category	== '')
	{
		//clearAll();
		return ;	
	}
	
	var data 		= "requestType=searchData";
	data		   += "&category="+category;
	var url	 		= "controller.php?q="+menuID;
	$.ajax({
		url:url,
		data:data,
		dataType:'json',
		type:'POST',
		async:false,
		success: function(json)
		{
			loadChartOfAcc(json.subType);
			loadSubType(json.mainType);
			loadMainType(json.financeType);
			$('#frmPettyCahItemCategory #cboChartOfAccount').val(json.glID);
			$('#frmPettyCahItemCategory #cboSubType').val(json.subType);
			$('#frmPettyCahItemCategory #cboMainType').val(json.mainType);
			$('#frmPettyCahItemCategory #cboFinanceType').val(json.financeType);
			$('#frmPettyCahItemCategory #txtCategoryCode').val(json.code);
			$('#frmPettyCahItemCategory #txtCategoryName').val(json.name);
			$('#frmPettyCahItemCategory #chkActive').attr('checked',json.status);
			$('#frmPettyCahItemCategory #chkIOU').attr('checked',json.iouStatus);
		},
		error:function(xhr,status)
		{
			
		}	
		
		});	
}
function Delete()
{
	var category	= $('#frmPettyCahItemCategory #cboSearch').val();
	if(category	== '')
	{
		$('#frmPettyCahItemCategory #cboSearch').validationEngine('showPrompt','Please select a Category.','fail');
		return ;	
	}
	var val = $.prompt('Are you sure you want to delete "'+$('#frmPettyCahItemCategory #cboSearch option:selected').text()+'" ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
	var data 		= "requestType=deleteCategory";
	data		   += "&category="+category;
	var url	 		= "controller.php?q="+menuID;
	$.ajax({
		url:url,
		data:data,
		dataType:'json',
		type:'POST',
		async:false,
		success: function(json)
		{
			$('#frmPettyCahItemCategory #butDelete').validationEngine('showPrompt', json.msg,json.type);
								if(json.type=='pass')
								{
									$('#frmPettyCahItemCategory').get(0).reset();
									loadCategory();
									var t=setTimeout("Deletex()",1000);
									return;
								}	
		},
		error:function(xhr,status)
		{
			
		}
	});
	}
	}
});	
}
function alertx()
{
	$('#frmPettyCahItemCategory #butSave').validationEngine('hide')	;
}
function Deletex()
{
	$('#frmPettyCahItemCategory #butDelete').validationEngine('hide')	;
}
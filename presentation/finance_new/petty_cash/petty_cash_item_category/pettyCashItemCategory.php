<?php
(define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');
if($_REQUEST['iframe'])
{
include "include/javascript.html";
include "include/css.html";
?>
<script type="text/javascript" src="presentation/finance_new/petty_cash/petty_cash_item_category/pettyCashItemCategory.js"></script>
<?php } ?>
<?php 
include "class/finance/masterData/chartOfAccount/cls_chartofaccount_get.php";
$obj_chartofaccount_get	= new Cls_ChartOfAccount_Get($db);
include_once "class/tables/finance_mst_pettycash_category.php";							$finance_mst_pettycash_category		= new finance_mst_pettycash_category($db);
$db->connect();
//$db->begin();

$programCode	= 'P1075';
$userId			= $sessions->getUserId();

$menupermision->set($programCode,$userId);
$save_perm				= $menupermision->getintEdit();
$delete_perm			= $menupermision->getintDelete();
?>
<title>Petty Cash Item Category</title>
<form id="frmPettyCahItemCategory" >
<div align="center">
		<div class="trans_layoutD" style="width:550px">
		  <div class="trans_text">Petty Cash Item Category</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
           	<tr>
            	<td>
                <table width="100%" border="0" align="center">
                	  <tr class="normalfnt">
                        	<td width="31%">Category</td>
                            <td width="69%"><select id="cboSearch" style="width:250px">
                            <?php echo $finance_mst_pettycash_category->getCombo(NULL,NULL) ?>
                            </select></td>
                        </tr>
                        <tr><td>&nbsp;</td>
                        <td>&nbsp;</td></tr>
                         <tr class="normalfnt">
                        <td>Category Code</td>
                        <td><input  name="txtCategoryCode" type="text" id="txtCategoryCode" style="width:248px;"  /></td>
                      </tr>
                      <tr class="normalfnt">
                        <td>Category Name <span class="compulsoryRed">*</span></td>
                        <td><input  name="txtCategoryName" type="text" class="validate[required]" id="txtCategoryName" style="width:248px;" /></td>
                      </tr>
                      <tr height="10"><td></td><td></td></tr>
                      <tr><td colspan="2">
                      <fieldset class="tableBorder">
                      <table width="100%" >
                      
                      <tr class="normalfnt">
                        <td>Finance Type</td>
                        <td>
                        <select  name="cboFinanceType" id="cboFinanceType"  style="width:250px;"  tabindex="6" class="cls_normal cls_clear">
						<?php
                        	echo $obj_chartofaccount_get->loadFinanceTypeCombo();
                        ?>
                        </select></td>
                      </tr>
                      <tr class="normalfnt">
                        <td>Main Type</td>
                        <td><select  name="cboMainType" id="cboMainType"  style="width:250px;"  tabindex="7" class="cls_normal cls_clear">
                          
                        </select></td>
                      </tr>
                      <tr class="normalfnt">
                        <td width="30%">Sub Type</td>
                        <td width="70%"><select  name="cboSubType" id="cboSubType"  style="width:250px;"  tabindex="8" class="cls_normal cls_clear">
                          
                        </select></td>
                      </tr>
                      <tr class="normalfnt">
                        <td>Chart Of Account <span class="compulsoryRed">*</span></td>
                        <td><select  name="cboChartOfAccount" class="validate[required] cls_normal cls_clear" id="cboChartOfAccount"  style="width:250px;"  tabindex="9" >
                          
                        </select></td>
                      </tr>
                     </table></fieldset></td></tr>
                       <tr class="normalfnt">
                       <td>IOU Status</td>
                        <td><input name="chkIOU" id="chkIOU" type="checkbox" value=""></td>
                        
                      </tr>
                      <tr class="normalfnt">
                        <td>Active </td>
                        <td><input name="chkActive" id="chkActive" type="checkbox" value=""></td>
                      </tr>
                    </table>
                </td>
         	</tr>  
            <tr>
            	<td height="34">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
            			<tr>
            				<td width="100%" align="center" bgcolor=""><a href="?q=1075" class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butSave" name="butSave" <?php echo($save_perm==1?'':'style="display:none"'); ?> >Save</a><a class="button white medium" id="butDelete" name="butDelete" <?php echo($delete_perm==1?'':'style="display:none"'); ?> >Delete</a><a href="main.php"><div class="button white medium" id="butClose">Close</div></a></td>
            			</tr>
            		</table>
            </tr>        
          </table>
        </div>
</div>
</form>
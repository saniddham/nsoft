<?php 
try{
include  "class/finance/masterData/chartOfAccount/cls_chartofaccount_get.php";			$obj_chartofaccount_get				= new Cls_ChartOfAccount_Get($db);
include_once "class/tables/menupermision.php";											$menupermision						= new menupermision($db);	
include_once "class/tables/finance_mst_pettycash_category.php";							$finance_mst_pettycash_category		= new finance_mst_pettycash_category($db);
include_once "class/tables/finance_mst_chartofaccount.php";								$finance_mst_chartofaccount			= new finance_mst_chartofaccount($db);
include_once "class/tables/finance_mst_account_main_type.php";								$finance_mst_account_main_type		= new finance_mst_account_main_type($db);
include_once "class/tables/finance_mst_account_sub_type.php";								$finance_mst_account_sub_type		= new finance_mst_account_sub_type($db);
include_once "class/tables/finance_mst_account_type.php";									$finance_mst_account_type			= new finance_mst_account_type($db);
$requestType	= $_REQUEST['requestType'];
$editMode		= false;
$location		= $sessions->getLocationId();
$userId			= $sessions->getUserId();
$company		= $sessions->getCompanyId();
$programCode	= 'P1075';

if($requestType=='loadMainType')
{
	$financeType	= $_REQUEST['financeType'];

	$response['mainTypeCombo']	= $obj_chartofaccount_get->loadMainTypeCombo($financeType);
	
}
else if($requestType == 'loadSubType')
{
	$mainType	= $_REQUEST['mainType'];

	$response['subTypeCombo']	= $obj_chartofaccount_get->loadSubTypeCombo($mainType);
			
}
else if($requestType == 'loadChartOfAcc')
{
	$subType	= $_REQUEST['subType'];

	$response['chrtOfAccCombo'] = $obj_chartofaccount_get->loadSearchCombo($subType);
			
}
else if($requestType == 'loadCategory')
{
	$response['cboCategory'] = $finance_mst_pettycash_category->getCombo(NULL,NULL);
}
else if($requestType == 'saveData')
{
	$dataArr	= json_decode($_REQUEST['dataArr'],true);
	$category	= $dataArr['category'];
	$glID		= $dataArr['glID'];
	$code		= $dataArr['categoryCode'];
	$name		= $dataArr['categoryName'];
	$status		= $dataArr['status'];
	$iouStatus	= $dataArr['iouStatus'];

	$db->begin();
	$menupermision->set($programCode,$userId);
		//check save permission
	$permissionArr	= $menupermision->checkMenuPermision('Edit',NULL,NULL);
		if(!$permissionArr['type'])
			throw new Exception($permissionArr['msg']);

	$IOU_COUNT	= $finance_mst_pettycash_category->getIOUStatus();
	
	if($category == '')
	{
		$editMode = false;
		
		if($iouStatus == 1 && $IOU_COUNT>0)
			throw new Exception('IOU category already exists');
		
		$saveData	= 	$finance_mst_pettycash_category->insertRec($glID,$code,$name,$status,$iouStatus);
			if(!$saveData['status'])
				throw new Exception($saveData['msg']);
	}
	else
	{
		$editMode = true;
		$finance_mst_pettycash_category->set($category);
		$old_iouStat	= $finance_mst_pettycash_category->getIOU_STATUS();
		if($old_iouStat != 1 && $iouStatus == 1 && $IOU_COUNT>0)
			throw new Exception('IOU category already exists');
		$data = array( 
				'GL_ID'=>$glID 
				,'CODE'=>$code 
				,'NAME'=>$name 
				,'STATUS'=>$status
				,'IOU_STATUS'=>$iouStatus 
				);
		$where	= "ID = '$category'";
		$updateData	= $finance_mst_pettycash_category->update($data,$where);
		if(!$updateData['status'])
				throw new Exception($updateData['msg']);	
	}
	
	$db->commit();
	$response['type']	= 'pass';
	if($editMode== true)
		$response['msg']	= "Updated Successfully";
	else
		$response['msg']	= "Saved Successfully";			
		
}
else if($requestType == 'searchData')
{
	$category		= $_REQUEST['category'];
	$finance_mst_pettycash_category->set($category);
	$glID			= $finance_mst_pettycash_category->getGL_ID();
	$code			= $finance_mst_pettycash_category->getCODE();
	$name			= $finance_mst_pettycash_category->getNAME();
	$status			= $finance_mst_pettycash_category->getSTATUS();
	$iouStatus		= $finance_mst_pettycash_category->getIOU_STATUS();
	
	$finance_mst_chartofaccount->set($glID);
	$subType 		= $finance_mst_chartofaccount->getSUB_TYPE_ID();
	$finance_mst_account_sub_type->set($subType);
	$mainType 		= $finance_mst_account_sub_type->getMAIN_TYPE_ID();
	$finance_mst_account_main_type->set($mainType);
	$financeType 	= $finance_mst_account_main_type->getFINANCE_TYPE_ID();
	
	$response['glID']		=  $glID;
	$response['code']		=  $code;
	$response['name']		=  $name;
	$response['status']		=  ($status==1?true:false);	
	$response['iouStatus']	=  ($iouStatus==1?true:false);	
	$response['subType']	=  $subType;
	$response['mainType']	=  $mainType;
	$response['financeType']=  $financeType;
}
else if($requestType == 'deleteCategory')
{
	$category		= $_REQUEST['category'];
	$db->begin();
	$finance_mst_pettycash_category->set($category);
	if($finance_mst_pettycash_category->getBLOCK_STATUS() == 1)
		throw new Exception('You can not delete this record');
	
	$resultDel		= $finance_mst_pettycash_category->delete("ID = '$category'");
	if(!$resultDel['status'])	
		throw new Exception($resultDel['msg']);
	else
	{	
		$db->commit();	
		$response['type']	= 'pass';
		$response['msg']	= $resultDel['msg'];
	}
}

}
catch(Exception $e){
		$db->rollback();
		$response['msg'] 	=  $e->getMessage();;
		$response['error'] 	=  $error_handler->jTraceEx($e);
		$response['type']	=  'fail';
		$response['sql']	=  $db->getSql();
	}
	
	$db->disconnect();		
	echo json_encode($response);	
?>
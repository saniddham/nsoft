<?php 
try{
	include_once("class/cls_commonFunctions_get.php");				$cls_commonFunctions_get		= new cls_commonFunctions_get($db);
	include_once("class/tables/finance_pettycash_employees.php");	$finance_pettycash_employees	= new finance_pettycash_employees($db);
	
	
	$programCode	= 'P1108';
	$locationId		= $sessions->getLocationId();
	$userId			= $sessions->getUserId();
	$companyId		= $sessions->getCompanyId();
	$HR_DB 			= $sessions->getHrDatabase();
	
	$requestType	= $_REQUEST['requestType'];
	
	
	if($requestType == 'saveEmployee')
	{
		$empID				= $_REQUEST['empID'];

		$employeeResult		= $cls_commonFunctions_get->getQpayEmployees($HR_DB,$locationId,$empID);
		$empRow				= mysqli_fetch_array($employeeResult);
		$empName			= $empRow['strInitialName']; 

		$db->connect(); $db->begin();
		
		$pcEmpName			= $finance_pettycash_employees->getEmpNameByID($empID,$HR_DB,$locationId);
		if($pcEmpName != NULL)
			throw new Exception('Already exist!');
		
		$saveEmp			= $finance_pettycash_employees->insertRec($empID);
		if(!$saveEmp['type'])
			throw new Exception($saveEmp['msg']);

		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Saved Successfully.";
		$response['empName']	= $empName;
		echo json_encode($response);

	}
	else if($requestType == 'deleteEmployee')
	{
		$empID	= $_REQUEST['empID'];
		
		$db->connect(); $db->begin();
		$deleteRslt	= $finance_pettycash_employees->delete("EMPLOYEE_ID='$empID'");	
		if(!$deleteRslt['type'])
			throw new Exception($deleteRslt['msg']);
		
		$db->commit();
		$response['type'] 		= "pass";
		echo json_encode($response);
	}
}
catch(Exception $e)
	{
		$db->rollback();
		$response['msg'] 	=  $e->getMessage();
		$response['error'] 	=  $error_handler->jTraceEx($e);
		$response['type']	=  'fail';
		$response['sql']	=  $db->getSql();
		$db->disconnect();		
		echo json_encode($response);
	}
	
?>
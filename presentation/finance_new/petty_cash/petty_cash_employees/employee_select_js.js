// JavaScript Document
var menuID	= 1108;
$(document).ready(function(e) {
    try
	{
		$('#frmPettyEmployee').validationEngine();
		$(".chosen-select").chosen({allow_single_deselect: true , no_results_text: "Oops, nothing found!"}); 
	}
	catch(err)
	{
		
	}
	$('#frmPettyEmployee #butAdd').die('click').live('click',addEmployeToGrid);
	$('#frmPettyEmployee .clsDel').die('click').live('click',deleteRow);
});

function addEmployeToGrid()
{
	showWaiting();
	var empID	= $('#frmPettyEmployee #cboEmployee').val();
	
	var url 	= "controller.php?q="+menuID+"&requestType=saveEmployee";
	var data 	= "empID="+empID;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmPettyEmployee #butAdd').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				if(json.type=='pass')
				{
					var x = "<tr id=\""+empID+"\">"+
		"<td align=\"center\" style=\"text-align:center\"><img class=\"mouseover clsDel\" src=\"images/del.png\" /></td>"+
			"<td style=\"text-align:center\">"+json.empName+"</td></tr>";	
			
					$("#frmPettyEmployee #tblMain #tbleEmpBody:last").append(x);
					hideWaiting();
					
				}
				else
				{
					hideWaiting();
				}
			},
			error:function(xhr,status){
						
						$('#frmPettyEmployee #butAdd').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
				}		
		});
			
}

function deleteRow()
{
	var obj		= $(this);
	var empID	= obj.parent().parent().attr('id');
	var url 	= "controller.php?q="+menuID+"&requestType=deleteEmployee";
	var data 	= "empID="+empID;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				if(json.type=='pass')
				{
					obj.parent().parent().remove();
				}
			}
	});
}
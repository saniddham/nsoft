<?php 
include_once("class/cls_commonFunctions_get.php");				$cls_commonFunctions_get		= new cls_commonFunctions_get($db);
include_once("class/tables/finance_pettycash_employees.php");	$finance_pettycash_employees	= new finance_pettycash_employees($db);

$programCode	= 'P1108';
$locationId		= $sessions->getLocationId();
$userId			= $sessions->getUserId();
$companyId		= $sessions->getCompanyId();
$HR_DB 			= $sessions->getHrDatabase();

$employeeResult	= $cls_commonFunctions_get->getQpayEmployees($HR_DB,$locationId);

?>
<title>Petty Cash Employees</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" /> 


<form id="frmPettyEmployee" name="frmPettyEmployee"   autocomplete="off">
<div align="center">
		<div class="trans_layoutS">
		  <div class="trans_text">Petty Cash Employees</div>
    <table width="100%" border="0" align="center">
      <tr>
        <td width="100%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">Employee</td>
                <td><select name="cboEmployee"  class="cboEmployee chosen-select validate[required]" id="cboEmployee" style="width:300px">	
                <option value=""></option>
                <?php while($row = mysqli_fetch_array($employeeResult))
				{
				?>			
                <option value="<?php echo $row['intEmployeeId'] ?>"><?php echo $row['strInitialName']?></option>
               <?php }?>
                  </select></td>
                  <td><a class="button green small" id="butAdd" name="butAdd">Add</a></td>
              </tr>
              </table></td></tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><table id="tblMain" width="100%" border="0" class="bordered">
                  <tr>
                    <th width="16%">Del</th>
                    <th width="84%">Employee</th>
                    </tr>
                    <tbody id="tbleEmpBody">
                    <?php 
						$result	= $finance_pettycash_employees->getPettyEmploye_result($HR_DB,$locationId);
						while($row = mysqli_fetch_array($result))
						{
					?>
                    	<tr id="<?php echo $row['EMPLOYEE_ID']?>">
                        <td align="center"><img border="0" src="images/del.png" alt="Save" name="butDel" class="mouseover clsDel" id="butDel" tabindex="24"/></td>
                        <td align="center"><?php echo $row['EMP_NAME']?></td>
                        </tr>
                       <?php } ?>
                    </tbody>
                </table></td>
            </tr>
        
            </table></td>
            </tr>
            <tr height="40">
         <td align="center"><a href="?q=1108" class="button white medium" id="butNew" name="butNew">New</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
         </tr>
      </table>
	</div>
  </div>
</form>

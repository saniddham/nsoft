<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php 
include_once("class/tables/mst_department.php");						$mst_department						= new mst_department($db);
include_once("class/tables/finance_mst_pettycash_category.php");		$finance_mst_pettycash_category		= new finance_mst_pettycash_category($db);
include_once("class/tables/finance_mst_pettycash_item.php");			$finance_mst_pettycash_item			= new finance_mst_pettycash_item($db);
include_once("class/tables/finance_pettycash_employees.php");			$finance_pettycash_employees		= new finance_pettycash_employees($db);
include_once "class/tables/finance_pettycash_transaction.php";			$finance_pettycash_transaction		= new finance_pettycash_transaction($db);
include_once "class/tables/finance_pettycash_invoice_header.php";		$finance_pettycash_invoice_header	= new finance_pettycash_invoice_header($db);
include_once "class/tables/finance_pettycash_invoice_detail.php";		$finance_pettycash_invoice_detail	= new finance_pettycash_invoice_detail($db);
include_once "class/dateTime.php";										$dateTimes							= new dateTimes($db);


$db->connect();

$programCode	= 'P0850';
$locationId		= $sessions->getLocationId();
$userId			= $sessions->getUserId();
$companyId		= $sessions->getCompanyId();
$HR_DB 			= $sessions->getHrDatabase();

$invoiceNo		= $_REQUEST['invoiceNo'];
$invoiceYear	= $_REQUEST['invoiceYear'];

if($invoiceNo != '' && $invoiceYear != '')
{
	$finance_pettycash_invoice_header->set($invoiceNo,$invoiceYear);
	$invoiceDate	= $finance_pettycash_invoice_header->getDATE();
	$remark			= $finance_pettycash_invoice_header->getREMARKS();
	$type			= $finance_pettycash_invoice_header->getINVOICE_TYPE();
	$IOUNo			= $finance_pettycash_invoice_header->getIOU_PARENT_NO();
	$IOUYear		= $finance_pettycash_invoice_header->getIOU_PARENT_YEAR();
	$IOUAmount		= $finance_pettycash_transaction->getIOUCashBal($IOUNo,$IOUYear);
	$settlementNo	= $IOUNo.'/'.$IOUYear.'-IOU=>'.number_format($IOUAmount);
	$status			= $finance_pettycash_invoice_header->getSTATUS();
	$approveLevel	= $finance_pettycash_invoice_header->getAPPROVE_LEVEL();
	$locationId		= $finance_pettycash_invoice_header->getLOCATION_ID();
	$createdBy		= $finance_pettycash_invoice_header->getCREATED_BY();
	
	$save_perm				= $menupermision->checkMenuPermision('Edit',$status,$approveLevel);
	$appr_perm				= $menupermision->checkMenuPermision('Approve',$status,$approveLevel);
	$cancel_perm			= $menupermision->checkMenuPermision('Cancel',$status,$approveLevel);
}
else
	$save_perm				= $menupermision->checkMenuPermision('Edit','null','null');


?>
<title>Petty Cash Invoice</title>
<script>
var dateNow    = "<?php echo $dateTimes->getCurruntDate();?>";
</script>
<form id="frmPettyCashInvoice">
<div align="center">
	<div class="trans_layoutL" style="width:1000px">
	<div class="trans_text">Petty Cash Invoice</div>
    <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
   	<tr>
   	  <td colspan="2">
    	<table width="100%">
   <tr class="normalfnt">
    <td width="20%">Invoice No</td>
    <td width="49%"><input name="txtInvoiceNo" id="txtInvoiceNo" style="width:60px" type="text" value="<?php echo $invoiceNo?>" disabled> <input name="txtInvoiceYear" id="txtInvoiceYear" style="width:40px" value="<?php echo $invoiceYear?>" type="text" disabled></td>
   <td rowspan="3" ><fieldset class="tableBorder"><table width="444"><tr><td width="20">&nbsp;</td><td width="120">&nbsp;</td>
     <td width="20">&nbsp;</td>
     <td width="100">&nbsp;</td>
     <td width="20">&nbsp;</td>
     <td width="136">&nbsp;</td>
   </tr>
   <tr><td width="20"><input name="rdoType" id="rdoType" class="clsNormal validate[required]" type="radio" value="1" <?php if($invoiceNo == '' || $type == 1 ){ ?> checked="checked"  <?php } ?>   /></td><td width="120">Normal</td>
     <td width="20"><input name="rdoType" id="rdoType" class="clsIOU validate[required]" type="radio" value="2" <?php if($invoiceNo != '' && $type == 2 ){ ?> checked="checked"  <?php } ?> /></td>
     <td width="100">IOU</td>
     <td width="20"><input name="rdoType" id="rdoType" class="clsSettlement validate[required]" type="radio" value="3" <?php if($invoiceNo != '' && $type == 3 ){ ?> checked="checked"  <?php } ?>/></td>
     <td width="136">IOU Settlement</td>
   </tr>
     <tr><td width="20">&nbsp;</td><td width="120">&nbsp;</td>
     <td width="20">&nbsp;</td>
     <td width="100">&nbsp;</td>
     <td width="20">&nbsp;</td>
     <td width="136">&nbsp;</td>
   </tr></table></fieldset>
   </tr>
  <tr class="normalfnt">
   <td >Date</td>
    <td class="clsDate"  id="<?php echo date("Y-m-d") ?>"><input name="dtDate" type="text" class="txtbox validate[required]" id="dtDate" style="width:106px;" tabindex="1"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);" value="<?php echo($invoiceNo==''?date("Y-m-d"):$invoiceDate);?>"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
     </tr>
  <tr class="normalfnt">
    <td valign="top">Remark</td>
    <td rowspan="2" valign="top"><textarea name="txtRemarks" id="txtRemarks" style="width:250px; height:60px; resize:none" tabindex="4" ></textarea></td>
     </tr>
    <tr class="normalfnt">
    <td height="32">&nbsp;</td>
    <td><table width="445" id="tblIOUNo" <?php if($type != 3){?>style="display:none" <?php } ?>><tr><td width="124">IOU No       </td>
      <td width="309"><select name="cboSettlementNo" id="cboSettlementNo" style="width:300px" tabindex="5">
<?php        
if($invoiceNo != '')
	echo $finance_pettycash_invoice_detail->getIOUDetailsCombo($IOUNo.'/'.$IOUYear);
else
	echo $finance_pettycash_invoice_detail->getIOUDetailsCombo();	
	 ?>
      </select></td></tr></table></td>
    </tr>
 
</table>
    </td>
    </tr>  
    <tr>
    <td width="59%">
    <div id="divSettlement" style=" <?php if($invoiceNo != '' && $type == 3){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>">
    <table width="100%">
    <tr><td width="51%"><table id="tbleIOUDetails" width="500" class="rptBordered"  >
  <tr>
    <th>Department</th>
    <th>Requested By</th>
    <th>IOU Amount</th>
  </tr>
  <tbody id="IOUBody">
  
 <?php  $IOUResult	 	= $finance_pettycash_invoice_detail->getIOUDetails($IOUNo,$IOUYear,NULL);
			while($row		= mysqli_fetch_array($IOUResult))
			{
				$mst_department->set($row['DEPARTMENT_ID']);
				$department	= $mst_department->getstrName();
				$empName	= $finance_pettycash_employees->getEmpNameByID($row['REQUEST_USER_ID'],$HR_DB,$locationId);

	?>			
				
				<tr><td><?php echo $department?></td>
				<td><?php echo $empName ?></td>
				<td><?php echo number_format($row['CASH_BALANCE'],2)?></td></tr>
				
		<?php 	}?>
  </tbody>
 </table></td>
    <td width="49%">&nbsp;</td>
    </tr> 
<tr><td>&nbsp;</td>
<td>&nbsp;</td></tr>

</table></div></td></tr>

    <tr>
    <td colspan="2"><div class="cls_div_bgstyle" style="overflow:scroll;overflow-x:hidden;width:auto;height:250px;"><table id="tblPettyCash" width="100%" border="0" class="bordered" style="background-color:#FFF">
    <tr>
                    	<th colspan="7" ><div style="float:right"><a class="button white small" id="butInsertRow">Add New Item</a></div></th>
                    </tr> 
  <tr>
    <th width="4%">Del</th>
    <th width="15%">Department <span class="compulsoryRed">*</span></th>
    <th width="16%">Item Category <span class="compulsoryRed">*</span></th>
    <th width="16%">Item <span class="compulsoryRed">*</span></th>
    <th width="28%">Request By <span class="compulsoryRed">*</span></th>
    <th width="10%">Bill Amount <span class="compulsoryRed">*</span></th>
    <th width="11%" id="thCashRcv" style=" <?php if($type == 3){  echo 'display:yes'; } else { echo 'display:none';} ?>" >Cash Receive</th>
  </tr>
  <?php if($invoiceNo == '' && $invoiceYear == '') {?>
  <tr class="normalfnt cls_tr_firstRow">
    <td align="center"><img border="0" src="images/del.png" alt="Save" name="butDel" class="mouseover clsDel" id="butDel" tabindex="24"/></td>
    <td><select name="cboDepartment" id="cboDepartment" class="clsDepartment validate[required]" style="width:100%"> 
       <?php
       	echo $mst_department->getCombo(NULL,"intStatus=1");
	   ?>
      </select></td>
    <td><select name="cboItemCategory" id="cboItemCategory" class="validate[required]" style="width:100%">
     <?php 
	 	echo $finance_mst_pettycash_category->getCombo(NULL,"STATUS = 1");
	 ?>
      </select></td>
    <td><select name="cboItem" id="cboItem" class="clsItem validate[required]" style="width:100%">
    <?php
		//echo $finance_mst_pettycash_item->getCombo(NULL,"STATUS = 1");
	?>
      </select></td>
    <td><select name="cboRequestedBy" id="cboRequestedBy" class="clsRequestedBy validate[required]" style="width:99%" >
       <?php
			echo $finance_pettycash_employees->getPettyEmploye(NULL,$HR_DB,$locationId);
		  ?>
      </select></td>
    <td><input type="text" name="txtAmount" id="txtAmount" style="width:98%;text-align:right" class="clsBillAmount validate[required,decimal[2],custom[number]] cls_input_number_validation"/></td>
    <td id="tdRecvAmount" style="display:none"><input type="text" name="txtRcvAmount" value="0.00" class="clsRecvAmount validate[decimal[2],custom[number]] cls_input_number_validation" id="txtRcvAmount" style="width:98%;text-align:right" /></td>
  </tr>
  <?php } 
  else {
	  $detailResult	= $finance_pettycash_invoice_detail->loadIOUDetails($invoiceNo,$invoiceYear,$HR_DB);
		while($rowD		= mysqli_fetch_array($detailResult) )
		{
  ?>
  <tr class="normalfnt cls_tr_firstRow">
    <td align="center"><img border="0" src="images/del.png" alt="Save" name="butDel" class="mouseover clsDel" id="butDel" tabindex="24"/></td>
    <td><select name="cboDepartment" id="cboDepartment" class="clsDepartment validate[required]" style="width:100%"> 
       <?php
       	echo $mst_department->getCombo($rowD['DEPARTMENT_ID'],"intStatus=1");
	   ?>
      </select></td>
    <td><select name="cboItemCategory" id="cboItemCategory" class="validate[required]" style="width:100%">
     <?php 
	 	echo $finance_mst_pettycash_category->getCombo($rowD['CATEGORY_ID'],"STATUS = 1");
	 ?>
      </select></td>
    <td><select name="cboItem" id="cboItem" class="clsItem validate[required]" style="width:100%">
    <?php
		echo $finance_mst_pettycash_item->getCombo($rowD['PETTY_CASH_ITEM_ID'],"STATUS = 1");
	?>
      </select></td>
    <td><select name="cboRequestedBy" id="cboRequestedBy" class="clsRequestedBy validate[required]" style="width:99%" >
       <?php 
			echo $finance_pettycash_employees->getPettyEmploye($rowD['REQUEST_USER_ID'],$HR_DB,$locationId);
		  ?>
      </select></td>
    <td><input type="text" name="txtAmount" id="txtAmount" style="width:98%;text-align:right" class="clsBillAmount validate[required,decimal[2],custom[number]] cls_input_number_validation" value="<?php echo abs($rowD['INVOICE_AMOUNT']) ?>"/></td>
    <td id="tdRecvAmount" style=" <?php if($type == 3){  echo 'display:yes'; } else { echo 'display:none'; }?>" ><input type="text" name="txtRcvAmount" value="<?php echo $rowD['RECEIVE_AMOUNT']?>" class="clsRecvAmount validate[decimal[2],custom[number]] cls_input_number_validation" id="txtRcvAmount" style="width:98%;text-align:right" /></td>
  </tr>
  <?php }
  } ?>
   
</table></div>
</td>
    </tr>
    <tr><td colspan="2">&nbsp;</td></tr>
     <tr class="fileUpload" style=" <?php if($invoiceNo != ''){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>">
          <td colspan="2" style="text-align:left" ><iframe id="iframeFiles" src="presentation/finance_new/petty_cash/petty_cash_invoice/file_upload.php?txtFolder=<?php echo $invoiceNo.'-'.$invoiceYear; ?>" name="iframeFiles" style="width:399px;height:175px;border:none" ></iframe></td>
          </tr>
    <tr>
    <td colspan="2"><table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
            <tr height="40">
            	<td align="center" ><a href="?q=850" class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butSave" name="butSave" style=" <?php if($save_perm['type']){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>">Save</a><a class="button white medium" id="butConfirm" name="butConfirm" style=" <?php if($appr_perm['type']){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>">Approve</a><a class="button white medium" id="butCancle" name="butCancle" style=" <?php if($cancel_perm['type']){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>">Cancel</a><a class="button white medium" id="butReport" name="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
            </tr>
            </table>
    </td>
    </tr> 
    </table>
    </div>
</div>
</form>

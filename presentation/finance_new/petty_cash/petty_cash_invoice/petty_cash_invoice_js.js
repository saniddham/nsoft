var menuID		= 850;
var reportID	= 924;
var rdo_type	= '';
//var dateNow	= $('#frmPettyCashInvoice .clsDate').attr('id');
/*if type = 1 : NORMAL
 if type = 2 : IOU
 if type = 3 : SETTLEMENT */

$(document).ready(function(e) {
    try
	{
		$('#frmPettyCashInvoice').validationEngine();
	
	}
	catch(err)
	{
		
	}
	if($('#frmPettyCashInvoice .clsNormal').is(':checked') && $('#frmPettyCashInvoice #txtInvoiceNo').val() == '')
	{	loadCategory(1); }
	//$('#frmPettyCashInvoice .clsNormal').die('click').live('click',function(){loadCategory(1)});
	//$('#frmPettyCashInvoice .clsIOU').die('click').live('click',function(){loadCategory(2)});
	$('#frmPettyCashInvoice #cboItemCategory').die('change').live('change',loadCategoryWiseItems);
	$('#frmPettyCashInvoice #butInsertRow').die('click').live('click',inserNewRow);
	$('#frmPettyCashInvoice .clsDel').die('click').live('click',deleteRow);
	$('#frmPettyCashInvoice #butSave').die('click').live('click',saveInvoice);
	//$('#frmPettyCashInvoice .clsSettlement').die('click').live('click',displayIOU);
	$('#frmPettyCashInvoice #cboSettlementNo').die('change').live('change',loadIOUTable);
	$('#frmRptPettyCashInv #butRptConfirm').die('click').live('click',urlApprove);
	$('#frmRptPettyCashInv #butRptReject').die('click').live('click',urlReject);
	$('#frmPettyCashInvoice #butConfirm').die('click').live('click',Confirm);
	$('#frmPettyCashInvoice #butReport').die('click').live('click',loadReport);
	$('#frmRptPettyCashInv #butRptConfirm_d').die('click').live('click',urlApprove_d);
	$('#frmRptPettyCashInv #butRptReject_d').die('click').live('click',urlReject_d);
	$('#frmPettyCashInvoice #rdoType').die('click').live('click',clearAll);
	$('#frmRptPettyCashInv #butRptCancel').die('click').live('click',urlCancel);
	$('#frmPettyCashInvoice #butCancle').die('click').live('click',Cancel);

	rdo_type	= $('#frmPettyCashInvoice .clsNormal').val();


});

function clearAll()
{//alert(rdo_type);
	var objj	= $(this);
	var val 	= $.prompt('You will lose every data.Are you sure you want to proceed?',{
	buttons: { Ok: true, Cancel: false },
	callback: function(v,m,f){
	if(v)
	{	
		rdo_type	= objj.attr('value');
		$("#frmPettyCashInvoice #tblPettyCash tr:gt(2)").remove();
		$('#frmPettyCashInvoice').find("input[type=text], textarea,select").val('');
		
		$('#frmPettyCashInvoice #dtDate').val(dateNow);
		if($('#frmPettyCashInvoice .clsNormal').is(':checked'))
			loadCategory(1);
		if($('#frmPettyCashInvoice .clsIOU').is(':checked'))
		{
			loadCategory(2);
			var url 	= "controller.php?q="+menuID+"&requestType=loadItems";
			var data 	= "categoryId="+$('#frmPettyCashInvoice #cboItemCategory').val();
			$.ajax({
					url:url,
					data:data,
					dataType:'json',
					type:'POST',
					async:false,
					success:function(json)
					{
						$('#frmPettyCashInvoice .clsItem').html(json.html);
					}
			});		
		}
		if($('#frmPettyCashInvoice .clsSettlement').is(':checked'))
		{displayIOU();}

	}
	else
	{
		if(rdo_type==1)
			$('#frmPettyCashInvoice .clsNormal').attr('checked',true);
		else if(rdo_type==2)
			$('#frmPettyCashInvoice .clsIOU').attr('checked',true);
		else if(rdo_type==3)
			$('#frmPettyCashInvoice .clsSettlement').attr('checked',true);
	}
	}
	});
}

function displayIOU()
{
	$('#frmPettyCashInvoice #tblIOUNo').show();
	$('#frmPettyCashInvoice #thCashRcv').show();
	$('#frmPettyCashInvoice #tdRecvAmount').show();
	loadCategory(1);
}
function loadIOUTable()
{
	var iouNo	= $('#frmPettyCashInvoice #cboSettlementNo').val();
	
	var url 	= "controller.php?q="+menuID+"&requestType=loadIOUDetails";
	var data 	= "iouNo="+iouNo;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmPettyCashInvoice #divSettlement').show();	
				$("#frmPettyCashInvoice #tblPettyCash tr:gt(2)").remove();
				
				$('#frmPettyCashInvoice #tbleIOUDetails #IOUBody').html(json.iouDetails);
				if($('#frmPettyCashInvoice #txtInvoiceNo').val() == '')
				{
					$('#frmPettyCashInvoice #cboDepartment').html(json.dept_combo);
					$('#frmPettyCashInvoice #cboRequestedBy').html(json.emp_combo);
				}
			}
	});	
}

function loadCategory(type)
{
	$('#frmPettyCashInvoice #cboItem').html('');
	if(!$('#frmPettyCashInvoice .clsSettlement').is(':checked'))
	{
		$('#frmPettyCashInvoice #tblIOUNo').hide();
		$('#frmPettyCashInvoice #divSettlement').hide();
		$('#frmPettyCashInvoice #thCashRcv').hide();
		$('#frmPettyCashInvoice #tdRecvAmount').hide();
	}
	var url 	= "controller.php?q="+menuID+"&requestType=loadCategory";
	var data 	= "type="+type;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				$('#frmPettyCashInvoice #cboItemCategory').html(json.category)
			}
	});	
		
}

function loadCategoryWiseItems()
{
	var obj	= $(this);
	var categoryId = obj.val();

	if(obj.val()=='')
	{
		obj.parent().parent().find('.clsItem').html('');
		return;
	}
		
	var url 	= "controller.php?q="+menuID+"&requestType=loadItems";
	var data 	= "categoryId="+categoryId;
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			type:'POST',
			async:false,
			success:function(json)
			{
				obj.parent().parent().find('.clsItem').html(json.html);
			}
	});	
		
}

function inserNewRow()
{
	if($('#frmPettyCashInvoice').validationEngine('validate'))
	{
		$('#frmPettyCashInvoice #tblPettyCash tbody tr:last').after("<tr>"+$('#frmPettyCashInvoice #tblPettyCash .cls_tr_firstRow').html()+"</tr>");
		$('#frmPettyCashInvoice #tblPettyCash tbody tr:last #cboDepartment').val('');
		$('#frmPettyCashInvoice #tblPettyCash tbody tr:last #cboItemCategory').val('');
		$('#frmPettyCashInvoice #tblPettyCash tbody tr:last #cboItem').val('');
		$('#frmPettyCashInvoice #tblPettyCash tbody tr:last #cboRequestedBy').val('');
		$('#frmPettyCashInvoice #tblPettyCash tbody tr:last #txtAmount').val('');
		$('#frmPettyCashInvoice #tblPettyCash tbody tr:last #txtRcvAmount').val('0.00');	
	}
}
function deleteRow()
{
	var delRowCount = parseInt(document.getElementById('tblPettyCash').rows.length);
	
	if(delRowCount>3)
		$(this).parent().parent().remove();
	
	if(parseInt(document.getElementById('tblPettyCash').rows.length)==3)
		$('#frmPettyCashInvoice #tblPettyCash tbody tr:last').addClass('cls_tr_firstRow');
	
	//CaculateTotalValue();	
}
function saveInvoice()
{
	showWaiting();
	var invoiceNo	= $('#frmPettyCashInvoice #txtInvoiceNo').val();
	var invoiceYear	= $('#frmPettyCashInvoice #txtInvoiceYear').val();
	var type;
	var IOUNo		= '';
	if( $('#frmPettyCashInvoice .clsNormal').is(':checked'))
		type 	= 1;

	else if($('#frmPettyCashInvoice .clsIOU').is(':checked'))
		type 	= 2;
	else
	{	
		type 	= 3;
		IOUNo	= $('#frmPettyCashInvoice #cboSettlementNo').val();
	}
	var date		= $('#frmPettyCashInvoice #dtDate').val();
	var remarks		= $('#frmPettyCashInvoice #txtRemarks').val();
		
	if($('#frmPettyCashInvoice').validationEngine('validate'))
	{
		var data = "requestType=saveData";
		var arrHeader = "{";
							arrHeader += '"invoiceNo":"'+invoiceNo+'",' ;
							arrHeader += '"invoiceYear":"'+invoiceYear+'",' ;
							arrHeader += '"date":"'+date+'",' ;
							arrHeader += '"remarks":'+URLEncode_json(remarks)+',';
							arrHeader += '"IOUNo":"'+IOUNo+'",' ;
							arrHeader += '"type":"'+type+'"' ;
							
			arrHeader  += "}";
		
		var arrDetails	= "";
		$('#frmPettyCashInvoice #tblPettyCash .clsBillAmount').each(function(){
			
			var departmentId	= $(this).parent().parent().find('.clsDepartment').val();
			var itemId			= $(this).parent().parent().find('.clsItem').val();
			var requestedBy		= $(this).parent().parent().find('.clsRequestedBy').val(); 
			var billAmount		= $(this).val();
			var recvAmount		= $(this).parent().parent().find('.clsRecvAmount').val(); 
			if(recvAmount == '')
				recvAmount = 0;
				
			arrDetails += "{";
			arrDetails += '"departmentId":"'+ departmentId +'",' ;
			arrDetails += '"itemId":"'+ itemId +'",' ;
			arrDetails += '"requestedBy":"'+ requestedBy +'",' ;
			arrDetails += '"billAmount":"'+ billAmount +'",' ;
			arrDetails += '"recvAmount":"'+ recvAmount +'"' ;
			arrDetails +=  '},';
		});
		
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		
		var arrHeader	= arrHeader;
		var arrDetails	= '['+arrDetails+']';
		data	   	   += "&arrHeader="+arrHeader+"&arrDetails="+arrDetails;
		
		var url = "controller.php?q="+menuID;
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmPettyCashInvoice #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t = setTimeout("alertx()",3000);
						$('#frmPettyCashInvoice #txtInvoiceNo').val(json.invoiceNo);
						$('#frmPettyCashInvoice #txtInvoiceYear').val(json.invoiceYear);
						$('#frmPettyCashInvoice #butConfirm').show();
						
						var src = "presentation/finance_new/petty_cash/petty_cash_invoice/file_upload.php?txtFolder="+json.invoiceNo+'-'+json.invoiceYear;
						
						$('#frmPettyCashInvoice #iframeFiles').attr('src',src)
						$('#frmPettyCashInvoice .fileUpload').show();
						hideWaiting();
						return;
					}
					else
					{
						hideWaiting();
					}
				},
				error:function(xhr,status){
						
						$('#frmPettyCashInvoice #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
				}		
		});
	}
	else
	{
		hideWaiting();
	}	
}

function urlApprove()
{

	var val = $.prompt('Are you sure you want to approve this Invoice ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "controller.php?q="+menuID+window.location.search+'&requestType=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptPettyCashInv #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertxR1()",10000);
									window.location.href = window.location.href;
									if(window.opener.location.pathname=='/nsoft/presentation/finance_new/petty_cash/petty_cash_invoice/petty_cash_invoice_listing.php')
									{
										window.opener.location.reload();//reload listing page
									}
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptPettyCashInv #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertxR1()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}
function urlReject()
{
	var val = $.prompt('Are you sure you want to reject this Invoice ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "controller.php?q="+menuID+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptPettyCashInv #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertxR2()",1000);
									window.location.href = window.location.href;
									if(window.opener.location.pathname=='/nsoft/presentation/finance_new/petty_cash/petty_cash_invoice/petty_cash_invoice_listing.php')
									{
										window.opener.location.reload();//reload listing page
									}

									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptPettyCashInv #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertxR2()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}

function urlApprove_d()
{

	var deptItem	=$(this).closest('tr').attr('id'); 

	var val = $.prompt('Are you sure you want to approve this Invoice ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "controller.php?q="+menuID+window.location.search+'&deptItem='+deptItem+'&requestType=approve_department';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptPettyCashInv #butRptConfirm_d').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertxD1()",10000);
									window.location.href = window.location.href;
									if(window.opener.location.pathname=='/nsoft/presentation/finance_new/petty_cash/petty_cash_invoice/petty_cash_invoice_listing.php')
									{
										window.opener.location.reload();//reload listing page
									}
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptPettyCashInv #butRptConfirm_d').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertxD1()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}
function urlReject_d()
{
	var deptItem	=$(this).closest('tr').attr('id'); 

	var val = $.prompt('Are you sure you want to reject this Invoice ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "controller.php?q="+menuID+window.location.search+'&deptItem='+deptItem+'&requestType=reject_department';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptPettyCashInv #butRptReject_d').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertxD2()",1000);
									window.location.href = window.location.href;
									if(window.opener.location.pathname=='/nsoft/presentation/finance_new/petty_cash/petty_cash_invoice/petty_cash_invoice_listing.php')
									{
										window.opener.location.reload();//reload listing page
									}

									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptPettyCashInv #butRptReject_d').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertxD2()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}
function urlCancel()
{
	var val = $.prompt('Are you sure you want to Cancel this invoice ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url 	 ="controller.php?q=850"+window.location.search+'&requestType=cancel';
					
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType:'json',  
						async:false,
						success:function(json){
					$('#frmRptPettyCashInv #butRptCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//var t=setTimeout("alertx1()",1000);
						window.location.href = window.location.href;
						window.opener.location.reload();
						return;
					}
					 hideWaiting();
					},
					error:function(xhr,status){						
							$('#frmRptPettyCashInv #butRptCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							//var t=setTimeout("alertx1()",3000);
							 hideWaiting();
					}		
						});
					
					}
				
			}});
}
function alertx()
{
	$('#frmPettyCashInvoice #butSave').validationEngine('hide')	;
}
function alertxR1()
{
	$('#frmRptPettyCashInv #butRptConfirm').validationEngine('hide')	;
}
function alertxR2()
{
	$('#frmRptPettyCashInv #butRptReject').validationEngine('hide')	;
}
function alertxD1()
{
	$('#frmRptPettyCashInv #butRptConfirm_d').validationEngine('hide')	;
}
function alertxD2()
{
	$('#frmRptPettyCashInv #butRptReject_d').validationEngine('hide')	;
}


function Confirm()
{
	var url  = "?q="+reportID+"&invoiceNo="+$('#frmPettyCashInvoice #txtInvoiceNo').val();
	    url += "&invoiceYear="+$('#frmPettyCashInvoice #txtInvoiceYear').val();
	    url += "&mode=Confirm";
	window.open(url,'rpt_petty_cash_invoice.php');
}
function loadReport()
{
	if($('#frmPettyCashInvoice #txtInvoiceNo').val()=='')
	{
		$('#frmPettyCashInvoice #butReport').validationEngine('showPrompt','No Invoice No to view Report','fail');
		return;	
	}
	var url  = "?q="+reportID+"&invoiceNo="+$('#frmPettyCashInvoice #txtInvoiceNo').val();
	    url += "&invoiceYear="+$('#frmPettyCashInvoice #txtInvoiceYear').val();
	
	window.open(url,'rpt_petty_cash_invoice.php');
}

function Cancel()
{
	var url  = "?q="+reportID+"&invoiceNo="+$('#frmPettyCashInvoice #txtInvoiceNo').val();
	    url += "&invoiceYear="+$('#frmPettyCashInvoice #txtInvoiceYear').val();
		url += "&mode=Cancel";
	window.open(url,'rpt_petty_cash_invoice.php');
}

<?php 
ini_set('display_errors',0);
include_once "class/finance/petty_cash/petty_cash_invoice/cls_petty_cash_invoice_get.php";
include_once "class/finance/petty_cash/petty_cash_item/cls_petty_cash_item_get.php";
include_once "class/cls_commonErrorHandeling_get.php";
include_once "class/cls_commonFunctions_get.php";
			 
$obj_petty_cash_invoice_get1	= new cls_petty_cash_invoice_get($db);
$obj_petty_cash_item_get1		= new cls_petty_cash_item_get($db);
$obj_commonErrHandle1			= new cls_commonErrorHandeling_get($db);
$obj_common1					= new cls_commonFunctions_get($db);
			 
			 if($_REQUEST["serialNo"] !=''){
			 	$serialNo					= $_REQUEST["serialNo"];
			 	$serialYear					= $_REQUEST["serialYear"];
			 }
			 
			 ?>
             
                <table style="width:500px" class="bordered" id="tblDepartment" >
                <thead>
                    <tr>
                    <th colspan="3" style="text-align:right"><a class="button white small" id="butInsertRow">Add New Row</a></th>
                    </tr>
                    <tr class="normalfnt">
                    <th width="9%">Del</th>
                    <th width="65%">Department <span class="compulsoryRed">*</span></th>
                    <th width="26%">Bill Amount <span class="compulsoryRed">*</span></th>
                    </tr>
                </thead>
                <tbody>
                <?php
				$TotDepAmonut	= 0;
				if($serialNo!='' && $serialYear!='')
				{
					$result	= $obj_petty_cash_invoice_get1->getDepartmentAmounts($serialNo,$serialYear,'RunQuery');
					while($row = mysqli_fetch_array($result))
					{
						$TotDepAmonut += $row['AMOUNT'];
				?>	
                		<tr class="cls_tr_firstRow">
                        	<td style="text-align:center" class="clsDelete"><img src="images/del.png" class="removeRow mouseover"/></td>	
                            <td style="text-align:center"><select name="cboDepartment" id="cboDepartment" class="clsDepartment validate[required]" style="width:100%">
                            <option value=""></option>
                            <?php
                            $result2	= $obj_common1->loadDepartment('RunQuery');
                            while($row2 = mysqli_fetch_array($result2))
                            {
								if($row['DEPARTMENT_ID']==$row2['intId'])
                                	echo "<option selected=\"selected\" value=\"".$row2['intId']."\">".$row2['strName']."</option>";
								else
									echo "<option value=\"".$row2['intId']."\">".$row2['strName']."</option>";
                            }
                            ?>
                            </select></td>
                            <td style="text-align:center"><input name="txtDepAmount" type="text" id="txtDepAmount" style="width:100%;text-align:right" class="clsDepAmount validate[required,custom[number]]" value="<?php echo $row['AMOUNT']; ?>"/></td>
                       </tr>
                		
                <?php
					}
				}
				else
				{
				?>
                	 <tr class="cls_tr_firstRow">
            			<td style="text-align:center" class="clsDelete"><img src="images/del.png" class="removeRow mouseover"/></td>
                		<td style="text-align:center"><select name="cboDepartment" id="cboDepartment" class="clsDepartment validate[required]" style="width:100%">
                        <option value=""></option>
                        <?php
						$result		= $obj_common1->loadDepartment('RunQuery');
						while($row = mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
						?>
                        </select></td>
                		<td style="text-align:center"><input name="txtDepAmount" type="text" id="txtDepAmount" style="width:100%;text-align:right" class="clsDepAmount validate[required,custom[number]]" value=""/></td>
            		</tr>
                <?php
				}
				?>
                </tbody>
                <tfoot>
                    <tr class="dataRow">
                      <td colspan="2" align="left">&nbsp;</td>
                      <td style="text-align:center">
                        <input name="txtTotDepAmonut" id="txtTotDepAmonut" class="clsTotDepAmonut" type="text" style="width:100%;text-align:right" disabled="disabled" value="<?php echo number_format($TotDepAmonut, 2, '.', ''); ?>"/></td>
                      </tr>
                    </tfoot>
                </table>

<?php
session_start();
$folderName 		= $_REQUEST['txtFolder'];
$serialArr			= explode('-',$folderName);

include_once "../../../../dataAccess/DBManager2.php";
include_once "../../../../class/tables/finance_pettycash_invoice_header.php";		
			
$db												= new DBManager2();
$finance_pettycash_invoice_header				= new finance_pettycash_invoice_header($db);
$db->connect();
$finance_pettycash_invoice_header->set($serialArr[0],$serialArr[1]);

$intStatus			= $finance_pettycash_invoice_header->getSTATUS();
$approveLevel		= $finance_pettycash_invoice_header->getAPPROVE_LEVEL();
$filePath			= "../../../../documents/finance/petty_cash/$folderName/";

if(isset($_REQUEST['butUpload']))
{
	if($intStatus<=$approveLevel)
	{
		echo "<span style=\"color:red\">Cannot upload bills after a approval raised.</span>";	
	}
	else
	{
		if (($_FILES["file"]["size"] < 102400))//100KB
		{
			if(!is_dir("../../../../documents/finance/petty_cash/$folderName/"))
			{
				mkdir("../../../../documents/finance/petty_cash/$folderName/",0700);
			}
			if ($_FILES["file"]["error"] == 0)
			{ 
				move_uploaded_file($_FILES["file"]["tmp_name"],$filePath . $_FILES["file"]["name"]);
			}
		}
		else
		{
			echo "<span style=\"color:red\">Invalid Size.( Max size = 100KB )</span>";	
		}
		
	}
}

?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Upload Files</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form id="frmUpload" name="frmUpload" method="post" action="file_upload.php" enctype="multipart/form-data">

<table class="bordered" width="355" height="48" >
<thead>
  <tr class="normalfnt">
    <th colspan="2" bgcolor="" class="normalfntMid">Upload Documents</th>
    </tr>
  <tr class="normalfnt">
    <th width="67" bgcolor="" class="normalfntMid">No</th>
    <th width="312" bgcolor="" class="normalfntMid">File Name</th>
    </tr>
</thead>
<tbody>
  <?php
  	//echo "../../../../../documents/sampleinfo/docs/$folderName";
	filesInDir($filePath);
	
				function filesInDir($tdir)
				{
					global $folderName;
					$m=0;
					$dirs = scandir($tdir);
					foreach($dirs as $file)
					{
						
						if (($file == '.')||($file == '..'))
						{
						}
						else
						{
							
							if (! is_dir($tdir.'/'.$file))
							{
								
							echo "	<tr bgcolor=\"#ffffff\" class=\"normalfnt\">
										<td>".++$m."</td>
										<td><a target=\"_blank\" href=\"../../../../documents/finance/petty_cash/".$folderName."/$file\">$file</a></td>
								 	 </tr>";
							}
						}
					}
				}
  ?>
  
  <tr>
    <td colspan="2" bgcolor="#FFFFFF"><input type="file" name="file" id="file" />
      <input <?php echo ($intStatus<=$approveLevel?'style="display:none"':'');?> type="submit" name="butUpload" id="butUpload" value="Upload" /></td>
    </tr>
  <tr>
    <td colspan="2" bgcolor="#FFFFFF"><input style="visibility:hidden" name="txtFolder" type="text" id="txtFolder" value="<?php echo $folderName; ?>" /></td>
    </tr>
  </tbody>
</table>
</form>
</body>
</html>
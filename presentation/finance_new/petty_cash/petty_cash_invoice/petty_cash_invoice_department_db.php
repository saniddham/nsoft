<?php
	session_start();
	//ini_set('display_errors',1);
	$backwardseperator 		= "../../../../";
	$mainPath 				= $_SESSION['mainPath'];
	$userId 				= $_SESSION['userId'];
	$location 				= $_SESSION['CompanyID'];
	$company 				= $_SESSION['headCompanyId'];
	$HR_DB 					= $_SESSION['HRDatabase'];
	$main_DB 				= $_SESSION['Database'];

	include_once "{$backwardseperator}dataAccess/Connector.php";
 	require_once "../../../../class/cls_commonFunctions_get.php";
	require_once "../../../../class/cls_commonErrorHandeling_get.php";
	require_once "../../../../class/finance/petty_cash/petty_cash_invoice/cls_petty_cash_invoice_get.php";
	require_once "../../../../class/finance/petty_cash/petty_cash_invoice/cls_petty_cash_invoice_set.php";
	require_once "../../../../class/finance/petty_cash/petty_cash_book/cls_petty_cash_get.php";
  	
	$obj_common				= new cls_commonFunctions_get($db);
	$obj_commonErr			= new cls_commonErrorHandeling_get($db);
	$obj_petty_cash_inv_get	= new cls_petty_cash_invoice_get($db);
	$obj_petty_cash_inv_set	= new cls_petty_cash_invoice_set($db);
	$obj_petty_cash_get		= new cls_petty_cash_get($db);
  
	$requestType			= $_REQUEST['requestType'];
 	
	$programName			='Petty Cash Invoice';
	$programCode			='P0850';

 	//---------------------------	
	
if($requestType=='approve_department'){
	
		$serialNo				= $_REQUEST['serialNo'];
		$serialYear				= $_REQUEST['serialYear'];
		$department				= $_REQUEST['department'];
	
		$db->begin();
		
		$rollBack_flag 			= false;
		$rollBack_msg			= '';
		$rollBack_sql			= '';
	
		$header_array 			= $obj_petty_cash_inv_get->get_header_department_wise($serialNo,$serialYear,$department,$main_DB,$HR_DB,'RunQuery2');
		$permition_arr			= $obj_petty_cash_inv_get->get_permision_approval_for_department_heads($serialNo,$serialYear,$department,$intUser,'RunQuery2');
		$permision_confirm		= $permition_arr['permision'];
		$permision_reject		= $permition_arr['permision'];
  		
		//VALIDATION--------------------------------------
		//1. check for approval permision
		if($permision_confirm  != 1 || $header_array['STATUS'] <=1){
			$rollBack_flag		=	true;
			$rollBack_msg	 	=	'No permision to approve';	
			$rollBack_sql		=	'';
 		}
 		
 		//SAVING--------------------------------------
		if($rollBack_flag != true){
		//3. update header status
			$status				= $header_array['STATUS']-1;
			$response_1			= $obj_petty_cash_inv_set->update_department_header_status($serialYear,$serialNo,$department,$status,'RunQuery2');
			if($response_1['type']	 == 'fail'){
				$rollBack_flag	=true;
				$rollBack_msg	=$response_1['msg'];
				$rollBack_sql	=$response_1['sql'];
			}
		}
		if($rollBack_flag != true){
		//4. approve by insert
			if($rollBack_flag != true){
				$header_array 	= $obj_petty_cash_inv_get->get_header_department_wise($serialNo,$serialYear,$department,$main_DB,$HR_DB,'RunQuery2');
				$levels			= $header_array['LEVELS']+1-$header_array['STATUS'];
				$response_2		= $obj_petty_cash_inv_set->approved_by_department_insert($serialYear,$serialNo,$department,$userId,$levels,'RunQuery2');
				if($response_2['type']	 == 'fail'){
					$rollBack_flag		=true;
					$rollBack_msg		=$response_2['msg'];
					$rollBack_sql		=$response_2['sql'];
				}
			}
		}
		
 
		if($rollBack_flag==true){
			$db->rollback();
			$response['type']	= 'fail';
			$response['msg']	= $rollBack_msg;
			$response['q']		= $rollBack_sql;
		}
		else if($header_array['STATUS'] ==1 ){
			$db->commit();
			$response['type']	= 'pass';
			$response['msg']	= 'Approval Raised successfully.';
		}
		else{
			$db->rollback();
			$response['type']	= 'fail';
			$response['msg']	= $db->errormsg;
			$response['q']		= $sql;
		}
	
		$db->CloseConnection();		
		echo json_encode($response);
	
	}
	else if($requestType=='reject_department'){
		
		$rollBack_flag 			= false;
		$rollBack_msg			= '';
		$rollBack_sql			= '';
		
		$serialNo				= $_REQUEST['serialNo'];
		$serialYear				= $_REQUEST['serialYear'];
		$department				= $_REQUEST['department'];
		
		$db->begin();
	
		$header_array			= $obj_petty_cash_inv_get->get_header_department_wise($serialNo,$serialYear,$department,$main_DB,$HR_DB,'RunQuery2');
		$permition_arr			= $obj_petty_cash_inv_get->get_permision_approval_for_department_heads($serialNo,$serialYear,$department,$intUser,'RunQuery2');
		$permision_confirm		= $permition_arr['permision']; 
		//print_r($permition_arr);
		//--------------------------
		//$permition_arr['type'];
		if($permision_confirm  != 1 || $header_array['STATUS'] <=1){
			$rollBack_flag		=	true;
			$rollBack_msg	 	=	'No permision to reject';	
			$rollBack_sql		=	'';
 		}
		
		if($rollBack_flag != true){
		//1. update header status
			$status				=0;
			$response_1			= $obj_petty_cash_inv_set->update_department_header_status($serialYear,$serialNo,$department,$status,'RunQuery2');
			if($response_1['type']	 == 'fail'){
				$rollBack_flag	=true;
				$rollBack_msg	=$response_1['msg'];
				$rollBack_sql	=$response_1['sql'];
			}
		}
			
		if($rollBack_flag != true){
		//2. approve by insert
			if($rollBack_flag != 1){
				$response_2		= $obj_petty_cash_inv_set->approved_by_department_insert($serialYear,$serialNo,$department,$userId,0,'RunQuery2');
				if($response_2['type']	 == 'fail'){
					$rollBack_flag		=true;
					$rollBack_msg		.=$response_2['msg'];
					$rollBack_sql		.=$response_2['sql'];
				}
			}
		}
		
		if($rollBack_flag==true){
			$db->rollback();
			$response['type']	= 'fail';
			$response['msg']	= $rollBack_msg;
			$response['q']		= $rollBack_sql;
		}
		else if($toSave==$saved){
			$header_array 	= $obj_petty_cash_inv_get->get_header_department_wise($serialNo,$serialYear,$department,$main_DB,$HR_DB,'RunQuery2');	
			$db->commit();
			$response['type']	= 'pass';
			if($header_array['STATUS']==0)
			$response['msg']	= 'Rejected successfully.';
			else
			$response['msg']	= 'Rejection Failed.';
		}
		else{
			$db->rollback();
			$response['type']	= 'fail';
			$response['msg']	= $db->errormsg;
			$response['q']		= $sql;
		}
	
		$db->CloseConnection();		
		echo json_encode($response);
	
	}
 ?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
	include_once "class/tables/finance_mst_pettycash_category.php";						$finance_mst_pettycash_category						= new finance_mst_pettycash_category($db);
	include_once "class/tables/finance_mst_pettycash_item.php";							$finance_mst_pettycash_item							= new finance_mst_pettycash_item($db);
	include_once "class/tables/menupermision.php";										$menupermision										= new menupermision($db);
	include_once "class/tables/finance_pettycash_invoice_approve_by.php";				$finance_pettycash_invoice_approve_by				= new finance_pettycash_invoice_approve_by($db);
	include_once "class/tables/sys_no.php";												$sys_no												= new sys_no($db);
	include_once "class/dateTime.php";													$dateTimes											= new dateTimes($db);
	include_once "class/tables/finance_pettycash_invoice_header.php";					$finance_pettycash_invoice_header					= new finance_pettycash_invoice_header($db);
	include_once "class/tables/finance_pettycash_budget_details.php";					$finance_pettycash_budget_details					= new finance_pettycash_budget_details($db);
	include_once "class/tables/finance_pettycash_transaction.php";						$finance_pettycash_transaction						= new finance_pettycash_transaction($db);
	include_once "class/finance/budget/cls_common_function_budget_get.php";				$cls_common_function_budget_get						= new cls_common_function_budget_get($db);
	include_once "class/tables/finance_pettycash_invoice_detail.php";					$finance_pettycash_invoice_detail					= new finance_pettycash_invoice_detail($db);
	include_once "class/tables/mst_financeaccountingperiod.php";						$mst_financeaccountingperiod						= new mst_financeaccountingperiod($db);
	include_once "class/tables/mst_department.php" ;									$mst_department										= new mst_department($db);
	include_once "class/tables/finance_pettycash_employees.php";						$finance_pettycash_employees						= new finance_pettycash_employees($db);
	include_once "class/tables/finance_pettycash_invoice_department_approved_by.php";	$finance_pettycash_invoice_department_approved_by	= new finance_pettycash_invoice_department_approved_by($db);
	include_once "class/tables/sys_users.php";											$sys_users											= new sys_users($db);
	include_once "class/tables/mst_department_heads.php";								$mst_department_heads						= new mst_department_heads($db);
	include_once "class/tables/mst_locations.php";										$mst_locations								= new mst_locations($db);													

	$location	 	= $sessions->getLocationId();
	$userId  		= $sessions->getUserId();
	$companyId  	= $sessions->getCompanyId(); 
	$HR_DB 			= $sessions->getHrDatabase();
	$programCode	= 'P0850';

	$invoiceNo		= $_REQUEST['invoiceNo'];
	$invoiceYear	= $_REQUEST['invoiceYear'];
	$mode			= $_REQUEST['mode'];
	
	$finance_pettycash_invoice_header->set($invoiceNo,$invoiceYear);
	$invoiceDate	= $finance_pettycash_invoice_header->getDATE();
	$remark			= $finance_pettycash_invoice_header->getREMARKS();
	$type			= $finance_pettycash_invoice_header->getINVOICE_TYPE();
	$IOUNo			= $finance_pettycash_invoice_header->getIOU_PARENT_NO();
	$IOUYear		= $finance_pettycash_invoice_header->getIOU_PARENT_YEAR();
	$IOUAmount		= $finance_pettycash_transaction->getIOUCashBal($IOUNo,$IOUYear);
	$settlementNo	= $IOUNo.'/'.$IOUYear.'-IOU=>'.number_format(abs($IOUAmount));
	$status			= $finance_pettycash_invoice_header->getSTATUS();
	$approveLevel	= $finance_pettycash_invoice_header->getAPPROVE_LEVEL();
	$locationId		= $finance_pettycash_invoice_header->getLOCATION_ID();
	$createdBy		= $finance_pettycash_invoice_header->getCREATED_BY();
	$mst_locations->set($locationId);
	$createCompanyId = $mst_locations->getintCompanyId();
		
	$header_array['STATUS']	= $status;
	$header_array['LEVELS'] = $approveLevel;
	
	$sys_users->set($createdBy);
	
	$menupermision->set($programCode,$userId);

	$perRejArr				= $menupermision->checkMenuPermision('Reject',$status,$approveLevel);
	$perApproArr			= $menupermision->checkMenuPermision('Approve',$status,$approveLevel);
	$perCancelArr			= $menupermision->checkMenuPermision('Cancel',$status,$approveLevel);

	$permision_reject		= ($perRejArr['type']?1:0);
	$permision_confirm		= ($perApproArr['type']?1:0);
	$permision_cancel		= ($perCancelArr['type']?1:0);

?>
<head>
<title>Petty Cash Invoice Report</title>

<script type="text/javascript" src="presentation/finance_new/petty_cash/petty_cash_invoice/petty_cash_invoice_js.js"></script>

</head>
<body>
<style type="text/css">
.apDiv1 {
	position:absolute;
	left:380px;
	top:100px;
	width:auto;
	height:auto;
	z-index:0;
	opacity:0.1;
}
</style>

 <div id="rpt_div_main">
    <?php include 'presentation/report_ribbon_watermark.php'?>
    <div id="rpt_div_table">
<form id="frmRptPettyCashInv" name="frmRptPettyCashInv" method="post">

  <table width="800" align="center">
    <tr>
      <td><?php include 'report_header_latest.php'?>
			</td>
    </tr>
    <tr>
      <td class="reportHeader" align="center">Petty Cash Invoice Report</td>
    </tr>
    <td colspan="2"><table width="100%" align="center">
     <?php include "presentation/report_approve_status_and_buttons_latest.php" ?>
    </table></td>
    <tr>
        <td>
        <table width="100%" border="0" class="normalfnt">
            <tr>
            <td width="18%">Invoice No</td>
            <td width="4%">:</td>
            <td width="29%"><?php echo $invoiceNo.'/'.$invoiceYear?></td>
            <td width="16%">Date</td>
            <td width="3%">:</td>
            <td width="30%"><?php echo $invoiceDate?></td>
            </tr>
            <tr>
                <td>Remarks</td>
                <td>:</td>
                <td rowspan="2" valign="top" ><?php echo $remark?></td>
               <?php 
			   if($type == 3)
			   echo "<td>IOU No</td>
                <td>:</td>
                <td >".$settlementNo."</td>"; ?>
            </tr>
  
		</table>
        </td>
    </tr>
    <tr>
   <td align="center"><table id="tbleDeptWiseItems" width="100%" border="0" class="normalfnt"><tr>
    <td width="80%" valign="top">
    <table width="100%" class="bordered" border="0" >
        <thead>
            <tr>
            	<th width="85" >Department</th>
                <th width="118" >Item Category</th>
                <th width="72" >Item</th>
				<th width="99" >Requested BY</th>
           		<th width="85" >Bill Amount</th>
                <th width="91" >Cash Receive</th>
           		<th width="164" style="color:#00C">Department Approval</th>
                <th width="56" >Status</th>
            </tr>
        </thead>
        <tbody>
        <?php 
		$detailResult	= $finance_pettycash_invoice_detail->loadIOUDetails($invoiceNo,$invoiceYear,$HR_DB);
		while($rowD		= mysqli_fetch_array($detailResult) )
		{
			$permition_arr_d		= $mst_department_heads->getDepartmentWiseApprovePermission($rowD['DEPARTMENT_ID'],$rowD['PETTY_CASH_ITEM_ID'],$userId,$location);
			$permision_confirm		= $permition_arr_d['permision'];
			$permision_reject		= $permition_arr_d['permision'];
			if($rowD['DEPT_APPROVE_STATUS'] <=1 ){
			$permision_confirm		= 0;
			$permision_reject		= 0;
			}
		?>
            	<tr class="normalfnt clsDepartment" id="<?php echo $rowD['DEPARTMENT_ID'].'/'.$rowD['PETTY_CASH_ITEM_ID']; ?>">
                	<td style="text-align:left"  nowrap="nowrap"><?php echo $rowD['DEPT_NAME']?></td>
                    <td style="text-align:left" nowrap="nowrap"><?php echo $rowD['CATEGORY']?></td>
                    <td style="text-align:left" nowrap="nowrap"><?php echo $rowD['ITEM']?></td>
                    <td style="text-align:left" nowrap="nowrap"><?php echo $rowD['REQ_USER']?></td>
                    <td style="text-align:right" nowrap="nowrap"><?php echo number_format(abs($rowD['INVOICE_AMOUNT']),2); ?></td>
                    <td style="text-align:right" nowrap="nowrap"><?php echo number_format($rowD['RECEIVE_AMOUNT'],2); ?></td>
                     <?php
					include "presentation/finance_new/petty_cash/petty_cash_invoice/report_approve_status_and_buttons_department_wise.php"
					?>
                </tr>
          <?php } ?>     
        </tbody>
		</table> 
     </td>
    </tr>
<tr>
<tr><td>&nbsp;</td></tr>
<td>  
        <table style="width:350px" class="bordered" border="0" >
        <thead>
            <tr class="normalfnt">
            <th colspan="2">Attached Documents</th>
            </tr>
            <tr class="normalfnt">
            <th >No</th>
            <th >File Name</th>
            </tr>
        </thead>
        <tbody>
		<?php
        	$folderName = $invoiceNo."-".$invoiceYear;
        	filesInDir("documents/finance/petty_cash/$folderName");
        ?>	
        </tbody>
        </table>
     </td></tr>   
    </table></td>
    </tr>
    <tr>
    	<td></td>
    </tr>
    <tr>
    	<td align="center">
        </td>
    </tr>
    <tr>
    <td>
    <?php
			$creator		= $sys_users->getstrUserName();
			$createdDate	= $finance_pettycash_invoice_header->getCREATED_DATE();
 			$resultA 		= $finance_pettycash_invoice_approve_by->getApprovedByData($invoiceNo,$invoiceYear);
			include "presentation/report_approvedBy_details.php"
 	?>
    </td>
    </tr>
      <?php
    if($mode=='Confirm' && $status > 1)
    {
    ?>
    <tr>
    <?php
    $createCompanyId = $createCompanyId;
    $url  = "presentation/send_to_approval_latest.php";
    $url .= "?status=$status";										// * set recent status
    $url .= "&approveLevels=$approveLevel";								// * set approve levels in order header
    $url .= "&programCode=$programCode";								// * program code (ex:P10001)
    $url .= "&program=Finance Petty Cash Invoice";									// * program name (ex:Purchase Order)
    $url .= "&companyId=$createCompanyId";									// * created company id
    $url .= "&createUserId=$createdBy";
    
    $url .= "&subject=PETTY CASH INVOICE FOR APPROVAL (".$invoiceNo."/".$invoiceYear.")";	
    
    $url .= "&statement1=Please Approve this";	
    $url .= "&statement2=to Approve this";	
                    // * doc year
    $url .= "&link=".urlencode(base64_encode(MAIN_URL."?q=924&invoiceNo=".$invoiceNo."&invoiceYear=".$invoiceYear."&mode=Confirm"));
    ?>
    <td colspan="2" align="center" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:150px;border:none"></iframe></td>
    </tr>
    <?php
    }
    ?>
    <tr>
    <td align="center" class="normalfntMid"><strong>Printed Date : </strong><?php echo date('Y-m-d H:i:s');?></td>
    </tr>
  </table>
  
</form>
</div>
  </div>
</body>
</html>

<?php


function filesInDir($tdir)
{
	global $folderName;
	$m		= 0;
	$dirs 	= scandir($tdir);
	$html	= '';
	
	foreach($dirs as $file)
	{
		
		if (($file == '.')||($file == '..'))
		{
		}
		else
		{
			
			if (! is_dir($tdir.'/'.$file))
			{
			
			$html	.= "<tr bgcolor=\"#ffffff\" class=\"normalfnt\">
							<td align=\"center\">".++$m."</td>
							<td><a target=\"_blank\" href=\"documents/finance/petty_cash/".$folderName."/$file\">$file</a></td>
						</tr>";
			}
		}
	}	
	echo $html;
}


?>
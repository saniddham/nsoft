<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php 
	/*
	SAVE/EDIT VALIDATION
	1. check permission
	2. display error msg if details not available. 
	3. check saved location when edit
	4. if IOU can not enter other items
	5. check location wise petty cash budget
	6. check department wise item budget
	
	APPROVE VALIDATION
	1. check permission
	2. check saved location when approve
	3. if IOU can not enter other items
	4. check location wise petty cash budget
	5. check department wise item budget 
	 
	*/
	try{
		include_once "class/tables/finance_mst_pettycash_category.php";						$finance_mst_pettycash_category						= new finance_mst_pettycash_category($db);
		include_once "class/tables/finance_mst_pettycash_item.php";							$finance_mst_pettycash_item							= new finance_mst_pettycash_item($db);
		include_once "class/tables/menupermision.php";										$menupermision										= new menupermision($db);
		include_once "class/tables/sys_approvelevels.php";									$sys_approvelevels									= new sys_approvelevels($db);
		include_once "class/tables/sys_no.php";												$sys_no												= new sys_no($db);
		include_once "class/dateTime.php";													$dateTimes											= new dateTimes($db);
		include_once "class/tables/finance_pettycash_invoice_header.php";					$finance_pettycash_invoice_header					= new finance_pettycash_invoice_header($db);
		include_once "class/tables/finance_pettycash_budget_details.php";					$finance_pettycash_budget_details					= new finance_pettycash_budget_details($db);
		include_once "class/tables/finance_pettycash_transaction.php";						$finance_pettycash_transaction						= new finance_pettycash_transaction($db);
		include_once "class/finance/budget/cls_common_function_budget_get.php";				$cls_common_function_budget_get						= new cls_common_function_budget_get($db);
		include_once "class/tables/finance_pettycash_invoice_detail.php";					$finance_pettycash_invoice_detail					= new finance_pettycash_invoice_detail($db);
		include_once "class/tables/mst_financeaccountingperiod.php";						$mst_financeaccountingperiod						= new mst_financeaccountingperiod($db);
		include_once "class/tables/mst_department.php" ;									$mst_department										= new mst_department($db);
		include_once "class/tables/finance_pettycash_employees.php";						$finance_pettycash_employees						= new finance_pettycash_employees($db);
		include_once "class/tables/finance_pettycash_invoice_department_approved_by.php";	$finance_pettycash_invoice_department_approved_by	= new finance_pettycash_invoice_department_approved_by($db);
		include_once "class/tables/finance_pettycash_invoice_approve_by.php";				$finance_pettycash_invoice_approve_by				= new finance_pettycash_invoice_approve_by($db);
		include_once "class/tables/mst_department_heads.php";								$mst_department_heads						= new mst_department_heads($db);

		$requestType	= $_REQUEST['requestType'];
		$programCode	= 'P0850';
		$userId			= $sessions->getUserId();
		$locationId		= $sessions->getLocationId();
		$HR_DB 			= $sessions->getHrDatabase();
		$deptStatus		= 2;
		$deptAppLevel	= 1;
		$IOUCat			= $finance_mst_pettycash_category->getIOUID();
		$IOUItemRslt	= $finance_mst_pettycash_item->getPettyItem($IOUCat);
		$rowItm			= mysqli_fetch_array($IOUItemRslt);
		$IOUItem		= $rowItm['itemId'];

		if($requestType == 'loadItems')
		{
			$categoryId	= $_REQUEST['categoryId'];
			$html		= "<option value=\"\"></option>";
			$resultItm	= $finance_mst_pettycash_item->getPettyItem($categoryId);
			
			while($row = mysqli_fetch_array($resultItm))
			{
				if($row['itemId'] == $IOUItem)
					$html  .= "<option selected=\"selected\" value=\"".$row['itemId']."\">".$row['itemName']."</option>";
				else
					$html  .= "<option value=\"".$row['itemId']."\">".$row['itemName']."</option>";
			}
			
			$response['html'] = $html;
			echo json_encode($response);

		}
		else if($requestType == 'loadCategory')
		{
			$type	= $_REQUEST['type'];
			if($type == 1)
				$category	= $finance_mst_pettycash_category->getCombo(NULL,"IOU_STATUS = 0 AND STATUS = 1");
			else if($type == 2)
				$category	= $finance_mst_pettycash_category->getCombo($IOUCat,"IOU_STATUS = 1 AND STATUS = 1");
				
			$response['category']	= $category;
			echo json_encode($response);

		}
		else if($requestType=='saveData')
		{
			$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
			$arrDetails 		= json_decode($_REQUEST['arrDetails'],true);

			$db->connect();$db->begin();
			
			$invoiceNo			= $arrHeader['invoiceNo'];
			$invoiceYear		= $arrHeader['invoiceYear'];
			$date				= $arrHeader['date'];
			$firstDay 			= date("Y-m-01",strtotime($date));
			$month 				= date("m",strtotime($date));
			$year 				= date("Y",strtotime($date));
			$remarks			= $arrHeader["remarks"];
			$IOUNoArr			= $arrHeader['IOUNo'];
			$arr_IOU			= explode('/',$IOUNoArr);
			$IOUNo				= $arr_IOU[0];
			$IOUYear			= $arr_IOU[1];
			$type				= $arrHeader['type'];

			if($IOUNo =='')
			{
				$IOUNo			= 0;
				$IOUYear		= 0;
			}
						
			$editMode			= false;
			
			$menupermision->set($programCode,$userId);
		
			## get approve levels and status
			$sys_approvelevels->set($programCode);
			$approveLevel		= $sys_approvelevels->getintApprovalLevel();
			$status				= $approveLevel+1; 		

			if($invoiceNo=='' && $invoiceYear=='')
			{
				####################################################
				## 1. check permission							  ##
				####################################################
				$permissionArr	= $menupermision->checkMenuPermision('Edit',NULL,NULL);
				if(!$permissionArr['type'])
					throw new Exception($permissionArr['msg']);
				
				####################################################
				## 2.display error msg if details not available.  ##
				####################################################
				if(!$arrDetails)
					throw new Exception("No details to save");
					
				//get sys max no and update sys no table
				$invoiceNo			= $sys_no->getSerialNoAndUpdateSysNo('PETTY_CASH_INVOICE_NO',$locationId);
				$invoiceYear		= $dateTimes->getCurruntYear();	

				$resultHArr			= $finance_pettycash_invoice_header->insertRec($invoiceNo,$invoiceYear,$IOUNo,$IOUYear,$date,$type,$remarks,$locationId,$status,$approveLevel,$userId,$dateTimes->getCurruntDateTime(),'NULL',$dateTimes->getCurruntDateTime());
				if(!$resultHArr['status'])
					throw new Exception($resultHArr['msg']);
			}
			else
			{
				$finance_pettycash_invoice_header->set($invoiceNo,$invoiceYear);
				$status_saved			= $finance_pettycash_invoice_header->getSTATUS();
				$approveLevel_saved		= $finance_pettycash_invoice_header->getAPPROVE_LEVEL();
				
				$permissionArr	= $menupermision->checkMenuPermision('Edit',$status_saved,$approveLevel_saved);
	
				if(!$permissionArr['type'])
					throw new Exception($permissionArr['msg']);
					
				####################################################
				## 3.check whether saved location			  	  ##
				####################################################
				$location	= $finance_pettycash_invoice_header->getLOCATION_ID();
				if($location != $locationId)
						throw new Exception("This is not a saved location");
				
				## update header data							
				$finance_pettycash_invoice_header->set($invoiceNo,$invoiceYear);
	
				$finance_pettycash_invoice_header->setDATE($date);
				$finance_pettycash_invoice_header->setINVOICE_TYPE($type);
				$finance_pettycash_invoice_header->setREMARKS($remarks);
				$finance_pettycash_invoice_header->setIOU_PARENT_NO($IOUNo);
				$finance_pettycash_invoice_header->setIOU_PARENT_YEAR($IOUYear);
				$finance_pettycash_invoice_header->setSTATUS($status);
				$finance_pettycash_invoice_header->setAPPROVE_LEVEL($approveLevel);
				$finance_pettycash_invoice_header->setMODIFIED_BY($userId);
				$finance_pettycash_invoice_header->commit();
					
				## update approved by table
				$finance_pettycash_invoice_approve_by->updateMaxStatus($invoiceNo,$invoiceYear);
					
				## delete issue to production detail data		
				$resultDel		= $finance_pettycash_invoice_detail->delete(" PETTY_INVOICE_NO = '$invoiceNo' AND PETTY_INVOICE_YEAR = '$invoiceYear'");
				if(!$resultDel['type'])	
					throw new Exception($resultDel['type']);	

				$editMode			= true;
			}
			foreach($arrDetails as $array_loop)
			{
				$departmentId		= $array_loop['departmentId'];
				$itemId				= $array_loop['itemId'];
			 	$requestedBy		= $array_loop['requestedBy']; 
				$billAmount			= $array_loop['billAmount'];
				$recvAmount			= (!$array_loop['recvAmount']?0:$array_loop['recvAmount']);
				$ckAmount			= $billAmount + $recvAmount;
				//die($recvAmount);
				if($ckAmount<=0)
					throw new Exception('Sum of bill amount and receive amount can not be zero');
				
				//check petty cash balance
				if($type != 3)
				{
					$pettyBal	= $finance_pettycash_transaction->getTotalPettyBal($locationId);
					if($pettyBal<$billAmount)	
						throw new Exception('Cannot proceed.Petty cash balance is less than bill amount<br>Petty cash balance:'.$pettyBal);
				}
				####################################################
				## 5. check location wise petty cash budget	  	  ##
				####################################################
				$validateArr		= validateLocationBudgetExceed($month,$year,$recvAmount,$billAmount,$locationId);
				
				####################################################
				## 6. check department wise item budget		  	  ##
				####################################################
				$bugetBalArr		= getBudgetBalance($month,$year,$itemId,$billAmount,$departmentId);
				if($type == 3 )
				{
					$IOUResult	 	= $finance_pettycash_invoice_detail->getIOUDetails($IOUNo,$IOUYear,$departmentId);
					if(mysqli_num_rows($IOUResult) == NULL)
						throw new Exception('Please select correct department');
					while($rowIOU			= mysqli_fetch_array($IOUResult))
					{
						$iouAmount		= $rowIOU['INVOICE_AMOUNT'];
						if($iouAmount<$billAmount && $recvAmount > 0)
							throw new Exception('You can not enter receive amount');
					}
					while($row		= mysqli_fetch_array($IOUResult))
					{	
						if($row['REQUEST_USER_ID'] != $requestedBy)
							throw new Exception('Please select correct user');
																
					}
				}
				
				$resultSaveDetail	= $finance_pettycash_invoice_detail->insertRec($invoiceNo,$invoiceYear,$departmentId,$itemId,$requestedBy,-$billAmount,$recvAmount,-$billAmount,$deptStatus,$deptAppLevel);
				if(!$resultSaveDetail['type'])
					throw new Exception($resultSaveDetail['msg']);
					
				## update approved by table
				$finance_pettycash_invoice_department_approved_by->updateMaxStatus($invoiceNo,$invoiceYear);
				
				if($approveLevel == 0)
				{
					$docType	= '';
					if($type == 1)
						$docType = 'NORMAL';
					else if($type == 2)
						$docType = 'IOU';
					else
						$docType = 'SETTLEMENT';
					saveToTransaction($docType,$invoiceNo,$invoiceYear,$IOUNo,$IOUYear,$date,$locationId,$userId);
				}
			}
			$db->commit();
			$response['type'] 			= "pass";
			if($editMode == 1)
				$response['msg'] 		= "Updated Successfully.";
			else
				$response['msg'] 		= "Saved Successfully.";
			$response['invoiceNo']		= $invoiceNo;
			$response['invoiceYear']	= $invoiceYear;
			$response['status']			= $status;
			echo json_encode($response);
			

		}
		else if($requestType == 'loadIOUDetails')
		{
			$IOUNoArr		= $_REQUEST['iouNo'];
			$arr_IOU		= explode('/',$IOUNoArr);
			$IOUNo			= $arr_IOU[0];
			$IOUYear		= $arr_IOU[1];
			
			$content		= '';
			$htmDept		= '<option value=""></option>';
			$htmlEmp		= '<option value=""></option>';
			$IOUResult	 	= $finance_pettycash_invoice_detail->getIOUDetails($IOUNo,$IOUYear,NULL);
			while($row		= mysqli_fetch_array($IOUResult))
			{
				$mst_department->set($row['DEPARTMENT_ID']);
				$department	= $mst_department->getstrName();
				
				$empName	= $finance_pettycash_employees->getEmpNameByID($row['REQUEST_USER_ID'],$HR_DB,$locationId);
				$content   .= "<tr><td>".$department."</td>";
				$content   .= "<td>".$empName."</td>";
				$content   .= "<td>".number_format($row['CASH_BALANCE'],2)."</td></tr>";
				
				$htmDept   .= '<option value="'.$row['DEPARTMENT_ID'].'">'.$department.'</option>';
				$htmlEmp   .= '<option value="'.$row['REQUEST_USER_ID'].'">'.$empName.'</option>';	
			}
			$response['iouDetails'] = $content;
			
			$response['dept_combo']	= $htmDept;
			$response['emp_combo']	= $htmlEmp;
			echo json_encode($response);
		}
		
		else if($requestType=='approve_department')
		{
	
			$invoiceNo				= $_REQUEST['invoiceNo'];
			$invoiceYear			= $_REQUEST['invoiceYear'];
			$deptItem				= $_REQUEST['deptItem'];
			$deptItemArr			= explode('/',$deptItem);
			$departmentId			= $deptItemArr[0];
			$itemId					= $deptItemArr[1];
	
			$db->connect();$db->begin();
			
			$finance_pettycash_invoice_header->set($invoiceNo,$invoiceYear);
			$invoiceDate			= $finance_pettycash_invoice_header->getDATE();
			$firstDay 				= date("Y-m-01",strtotime($invoiceDate));
			$month 					= date("m",strtotime($invoiceDate));
			$year 					= date("Y",strtotime($invoiceDate));
			$finance_pettycash_invoice_detail->set($invoiceNo,$invoiceYear,$itemId,$departmentId);
			$deptAppStatus			= $finance_pettycash_invoice_detail->getDEPT_APPROVE_STATUS();
			$billAmount				= $finance_pettycash_invoice_detail->getINVOICE_AMOUNT();
			$permition_arr			= $mst_department_heads->getDepartmentWiseApprovePermission($departmentId,$itemId,$userId,$locationId);
			$permision_confirm		= $permition_arr['permision'];
			$permision_reject		= $permition_arr['permision'];
  		
			//VALIDATION--------------------------------------
			
			####################################################
			## 1. check for approval permision			  	  ##
			####################################################
			if($permision_confirm  != 1 || $deptAppStatus <=1)
					throw new Exception('No permission to approve');
					
			####################################################
			## 2. check department wise item budget		  	  ##
			####################################################
			$bugetBalArr		= getBudgetBalance($month,$year,$itemId,$billAmount,$departmentId);
			
			//SAVING--------------------------------------
			//3. update detail department wise status
			$where = "PETTY_INVOICE_NO = '$invoiceNo' AND PETTY_INVOICE_YEAR= '$invoiceYear' AND DEPARTMENT_ID = '$departmentId' AND PETTY_CASH_ITEM_ID = '$itemId'";
		
			$data  =array(	'DEPT_APPROVE_STATUS'=> -1
			);
			$result_update_h		= $finance_pettycash_invoice_detail->upgrade($data,$where);
			if(!$result_update_h['status']) 
				throw new Exception($result_update_h['msg']);
			
		## insert to approve by table				  	
			$finance_pettycash_invoice_detail->set($invoiceNo,$invoiceYear,$itemId,$departmentId);  
			$hstatus		= $finance_pettycash_invoice_detail->getDEPT_APPROVE_STATUS();
			$aplevel		= $finance_pettycash_invoice_detail->getDEPT_APPROVE_LEVELS();
			$level			= (int)$aplevel+1- (int)$hstatus; 
			$inserToApprovedBy	= $finance_pettycash_invoice_department_approved_by->insertRec($invoiceNo,$invoiceYear,$departmentId,$itemId,$level,$userId,$dateTimes->getCurruntDateTime(),0);
			if(!$inserToApprovedBy['status']) 
				throw new Exception($inserToApprovedBy['msg']);
		
			$db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Approved Successfully.";	
			$db->disconnect();
			echo json_encode($response);
	
	}
	else if($requestType=='reject_department'){
		
		$invoiceNo				= $_REQUEST['invoiceNo'];
		$invoiceYear			= $_REQUEST['invoiceYear'];
		$deptItem				= $_REQUEST['deptItem'];
		$deptItemArr			= explode('/',$deptItem);
		$departmentId			= $deptItemArr[0];
		$itemId					= $deptItemArr[1];

		$db->connect();$db->begin();
			
	## update detail table dept approve status		
		$finance_pettycash_invoice_detail->set($invoiceNo,$invoiceYear,$itemId,$departmentId);
		$finance_pettycash_invoice_detail->setDEPT_APPROVE_STATUS(0);
		$finance_pettycash_invoice_detail->commit();
		
	## insert to approve by table				  	
		
		$inserToApprovedBy	= $finance_pettycash_invoice_department_approved_by->insertRec($invoiceNo,$invoiceYear,$departmentId,$itemId,0,$userId,$dateTimes->getCurruntDateTime(),0);
		if(!$inserToApprovedBy['status']) 
			throw new Exception($inserToApprovedBy['msg']);
	
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Rejected Successfully.";	
		$db->disconnect();
		echo json_encode($response);
	
		}
		
		else if($requestType == 'approve')
		{
			$db->connect();$db->begin();	
			$invoiceNo				= $_REQUEST['invoiceNo'];
			$invoiceYear			= $_REQUEST['invoiceYear'];
			
			## get header status and approve level	
			$finance_pettycash_invoice_header->set($invoiceNo,$invoiceYear);
			$status				= $finance_pettycash_invoice_header->getSTATUS(); 
			$approveLevels		= $finance_pettycash_invoice_header->getAPPROVE_LEVEL();
			$type				= $finance_pettycash_invoice_header->getINVOICE_TYPE();
			$IOUNo				= $finance_pettycash_invoice_header->getIOU_PARENT_NO();
			$IOUYear			= $finance_pettycash_invoice_header->getIOU_PARENT_YEAR();
			$date				= $finance_pettycash_invoice_header->getDATE();
	
			$menupermision->set($programCode,$userId);
			
			####################################################
			## 1.check Approve permission					  ##
			####################################################
			$permissionArr	= $menupermision->checkMenuPermision('Approve',$status,$approveLevels);
			if(!$permissionArr['type'])
					throw new Exception($permissionArr['msg']);
			
			####################################################
			## 2.check whether saved location			  	  ##
			####################################################
			$location	= $finance_pettycash_invoice_header->getLOCATION_ID();
			if($location != $locationId)
					throw new Exception("This is not saved location");
			
			####################################################
			## 2.check whether department approval raised  	  ##
			####################################################
			$finance_pettycash_invoice_detail->checkDepartmentWiseApprove($invoiceNo,$invoiceYear,$HR_DB);

			$detlRsult	= $finance_pettycash_invoice_detail->loadIOUDetails($invoiceNo,$invoiceYear,$HR_DB);
			while($rowDtl	= mysqli_fetch_array($detlRsult))
			{
				if($type != 3)
				{
					$pettyBal	= $finance_pettycash_transaction->getTotalPettyBal($locationId);
					if($pettyBal< abs($rowDtl['INVOICE_AMOUNT']))	
						throw new Exception('Cannot proceed.Petty cash balance is less than bill amount');
				}
			}
			## Update header status				
			$where = "PETTY_INVOICE_NO = '$invoiceNo' AND PETTY_INVOICE_YEAR= '$invoiceYear'";
			
			$data  =array(	'STATUS'=> -1
			);
			$result_update_h		= $finance_pettycash_invoice_header->upgrade($data,$where);
			if(!$result_update_h['status']) 
				throw new Exception($result_update_h['msg']);
			
			## insert to approve by table				  	  
	
			$finance_pettycash_invoice_header->set($invoiceNo,$invoiceYear);
			$hstatus		= $finance_pettycash_invoice_header->getSTATUS();
			$aplevel		= $finance_pettycash_invoice_header->getAPPROVE_LEVEL();
			$level			= (int)$aplevel+1- (int)$hstatus; 
			$inserToApprovedBy	= $finance_pettycash_invoice_approve_by->insertRec($invoiceNo,$invoiceYear,$level,$userId,$dateTimes->getCurruntDateTime(),0);
			if(!$inserToApprovedBy['status']) 
				throw new Exception($inserToApprovedBy['msg']);
			
			if($hstatus == 1)
			{
				$docType	= '';
					if($type == 1)
						$docType = 'NORMAL';
					else if($type == 2)
						$docType = 'IOU';
					else
						$docType = 'SETTLEMENT';
					saveToTransaction($docType,$invoiceNo,$invoiceYear,$IOUNo,$IOUYear,$date,$locationId,$userId);
			}
			$db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Approved Successfully.";	
			echo json_encode($response);
			$db->disconnect();
		}
		else if($requestType == 'reject')
		{
			$db->connect();$db->begin();	
			$invoiceNo				= $_REQUEST['invoiceNo'];
			$invoiceYear			= $_REQUEST['invoiceYear'];
			
			## get header status and approve level	
			$finance_pettycash_invoice_header->set($invoiceNo,$invoiceYear);
			$status				= $finance_pettycash_invoice_header->getSTATUS(); 
			$approveLevels		= $finance_pettycash_invoice_header->getAPPROVE_LEVEL();
			$type				= $finance_pettycash_invoice_header->getINVOICE_TYPE();
			$IOUNo				= $finance_pettycash_invoice_header->getIOU_PARENT_NO();
			$IOUYear			= $finance_pettycash_invoice_header->getIOU_PARENT_YEAR();
	
			$menupermision->set($programCode,$userId);
			
			## check Reject permission	
			$permissionArr	= $menupermision->checkMenuPermision('Reject',$status,$approveLevels);
			if(!$permissionArr['type'])
					throw new Exception($permissionArr['msg']);
			
			## check whether saved location	
			$location	= $finance_pettycash_invoice_header->getLOCATION_ID();
			if($location != $locationId)
					throw new Exception("This is not saved location");
							
			$finance_pettycash_invoice_header->setSTATUS(0);
			$finance_pettycash_invoice_header->commit();
			
			$inserToApprovedBy	= $finance_pettycash_invoice_approve_by->insertRec($invoiceNo,$invoiceYear,0,$userId,$dateTimes->getCurruntDateTime(),0);
			if(!$inserToApprovedBy['status']) 
				throw new Exception($inserToApprovedBy['msg']);
					
			$db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Rejected Successfully.";
			$db->disconnect();
			echo json_encode($response);
					
		}
		
		else if($requestType == 'cancel')
		{
			$db->connect();$db->begin();
			$invoiceNo			= $_REQUEST['invoiceNo'];
			$invoiceYear		= $_REQUEST['invoiceYear'];
			
			## get header status and approve level	
			$finance_pettycash_invoice_header->set($invoiceNo,$invoiceYear);
			$status				= $finance_pettycash_invoice_header->getSTATUS(); 
			$approveLevels		= $finance_pettycash_invoice_header->getAPPROVE_LEVEL();
			$type				= $finance_pettycash_invoice_header->getINVOICE_TYPE();
			$IOUNo				= $finance_pettycash_invoice_header->getIOU_PARENT_NO();
			$IOUYear			= $finance_pettycash_invoice_header->getIOU_PARENT_YEAR();
			$requestStatus		= $finance_pettycash_invoice_header->getREQUEST_STATUS();
			
			if($requestStatus == 1)
				throw new Exception('Cannot cancel invoice.Alredy request for reimbursement');
	
			$menupermision->set($programCode,$userId);
			
			####################################################
			## 1.check Cancel permission					  ##
			####################################################
			$permissionArr	= $menupermision->checkMenuPermision('Cancel',$status,$approveLevels);
			if(!$permissionArr['type'])
					throw new Exception($permissionArr['msg']);
			
			####################################################
			## 2.check whether saved location			  	  ##
			####################################################
			$location	= $finance_pettycash_invoice_header->getLOCATION_ID();
			if($location != $locationId)
					throw new Exception("This is not saved location");	
		
			####################################################
			## 3.update header status  					 	  ##
			####################################################
			$finance_pettycash_invoice_header->setSTATUS(-2);
			$finance_pettycash_invoice_header->commit();
			
			## update approved by table	 				
	
			$inserToApprovedBy	= $finance_pettycash_invoice_approve_by->insertRec($invoiceNo,$invoiceYear,-2,$userId,$dateTimes->getCurruntDateTime(),0);
			if(!$inserToApprovedBy['status']) 
				throw new Exception($inserToApprovedBy['msg']);
				
			####################################################
			## 4.rollback finance_pettycash_transaction		  ##
			#################################################### 
	
			$docType	= '';
			if($type == 1)
				$docType = 'NORMAL';
			else if($type == 2)
				$docType = 'IOU';
			else
				$docType = 'SETTLEMENT';
				
			rollbackTransactions($docType,$invoiceNo,$invoiceYear,$IOUNo,$IOUYear,$locationId,$userId);
					
			$db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "cancel Successfully.";
			echo json_encode($response);
			$db->disconnect();	
				
		}
	
	}
	catch(Exception $e)
	{
		$db->rollback();
		$response['msg'] 	=  $e->getMessage();
		$response['error'] 	=  $error_handler->jTraceEx($e);
		$response['type']	=  'fail';
		$response['sql']	=  $db->getSql();
		$db->disconnect();		
		echo json_encode($response);
	}
	function validateLocationBudgetExceed($month,$year,$recvAmount,$billAmount,$locationId)
	{
		global $finance_pettycash_budget_details;
		global $finance_pettycash_transaction;

		$pettyBudget	= $finance_pettycash_budget_details->getBudgetAmount($month,$year,$locationId);
		$pettyBalance	= $finance_pettycash_transaction->getPettyCashBalance($month,$year,$locationId);
		$totExpences	= $pettyBalance+$recvAmount-$billAmount;
		$pettyBudget	= ($pettyBudget==''?0:$pettyBudget);

		if(abs($totExpences)>$pettyBudget)
			throw new Exception("You cannot exeed the petty cash monthly budget margin.<br> Budget Margin : ".$pettyBudget."<br> Total Expences : ".abs($totExpences)."");
		else
			return true;
	}
	function getBudgetBalance($month,$year,$itemId,$billAmount,$departmentId)
	{
		global $finance_mst_pettycash_item;
		global $cls_common_function_budget_get;
		global $mst_financeaccountingperiod;
		global $locationId;
		
		$getBugCatArr	= $finance_mst_pettycash_item->getBudgetCategoryDetail($itemId,$locationId,$departmentId);
		$numROws		= mysqli_num_rows($getBugCatArr);

		if($numROws != NULL)
		{
			$row			= mysqli_fetch_array($getBugCatArr);
			$financeYearId	= $mst_financeaccountingperiod->getFinanceYearId($year,$month);
			$balance_arr	= $cls_common_function_budget_get->load_budget_balance($locationId,$departmentId,$financeYearId,'',$row['ITEM_ID'],$month,'RunQuery');
			
			if($row['BUDGET_TYPE']=='U')
				$budget_balance	= $balance_arr['balUnits'];
			
			if($row['BUDGET_TYPE']=='A')
				$budget_balance	= $balance_arr['balAmount'];
				
			if($billAmount>$budget_balance)
				throw new Exception("You can not exceed the budget balance.<br>Item : ".$row['item']."<br>Budget Balance : ".$budget_balance."");
		}
		
		else
			return true;
	}
	function saveToTransaction($docType,$invoiceNo,$invoiceYear,$IOUNo,$IOUYear,$date,$locationId,$userId)
	{
		global $finance_pettycash_transaction;
		global $dateTimes;
		global $finance_pettycash_invoice_detail;
		global $HR_DB;

		$balAmount	= 0;
		$dtlResult			= $finance_pettycash_invoice_detail->loadIOUDetails($invoiceNo,$invoiceYear,$HR_DB);
		while($rowDtl		= mysqli_fetch_array($dtlResult))
		{
			$departmentId		= $rowDtl['DEPARTMENT_ID'];
			$reqUserId			= $rowDtl['REQUEST_USER_ID'];
			$billAmount			= abs($rowDtl['INVOICE_AMOUNT']);
			$recvAmount			= $rowDtl['RECEIVE_AMOUNT'];
			$itemId				= $rowDtl['PETTY_CASH_ITEM_ID'];
			
			$insertTrans		= $finance_pettycash_transaction->insertRec($locationId,$invoiceNo,$invoiceYear,$docType,$IOUNo,$IOUYear,$departmentId,$reqUserId,$itemId,-$billAmount,-$billAmount,$date,$userId ,$dateTimes->getCurruntDateTime());
			if(!$insertTrans['type'])
				throw new Exception($insertTrans['msg']);
				
			if($docType	== 'SETTLEMENT')
			{
				$transResult	= $finance_pettycash_invoice_detail->getIOUDetails($IOUNo,$IOUYear,$departmentId,$itemId);
				$rowTrans		= mysqli_fetch_array($transResult);
				
				//check petty cash balance
				if($billAmount > $rowTrans['CASH_BALANCE'])
				{	
					$pettyBal	= $finance_pettycash_transaction->getTotalPettyBal($locationId);
					if($pettyBal<$billAmount)	
						throw new Exception('Cannot proceed.Petty cash balance is less than bill amount');
				}

				if($recvAmount == 0 && $billAmount > $rowTrans['CASH_BALANCE'])
				{	$balAmount	= 0;
					$balDtlAmt	= 0;
				}
				else if($recvAmount == 0 && $billAmount <= $rowTrans['CASH_BALANCE'])
				{
					$balDtlAmt	= ($rowTrans['CASH_BALANCE'] - $billAmount);
					if($balDtlAmt<0)
						$balDtlAmt	= 0;
					else	
						$balDtlAmt	= -$balDtlAmt;
						
					$balAmount	= ($rowTrans['CASH_BALANCE'] - $billAmount);
					if($balAmount<0)
						$balAmount	= 0;
					else	
						$balAmount	= -$balAmount;
				}
				else
				{
					$balDtlAmt	= ($rowTrans['CASH_BALANCE'] - ($billAmount+$recvAmount));
					if($balDtlAmt<0)
						$balDtlAmt	= 0;
					else	
						$balDtlAmt	= -$balDtlAmt;
						
					$balAmount	= ($rowTrans['CASH_BALANCE'] - $billAmount);
					if($balAmount<0)
						$balAmount	= 0;
					else	
						$balAmount	= -$balAmount;
					
				}
				//echo $departmentId.'-'.$balAmount;  
				$data			= array('BALANCE_AMOUNT'=>$balDtlAmt,
										'RECEIVE_AMOUNT'=>$recvAmount,);
				$where			= "	finance_pettycash_invoice_detail.PETTY_INVOICE_NO = '$IOUNo' AND
									finance_pettycash_invoice_detail.PETTY_INVOICE_YEAR	= '$IOUYear'
									AND  finance_pettycash_invoice_detail.DEPARTMENT_ID = '$departmentId'";
				$updateTrans	= $finance_pettycash_invoice_detail->update($data,$where);
				if(!$updateTrans['type'])
					throw new Exception($updateTrans['msg']);
	
				$data			= array('CASH_BALANCE'=>$balAmount,);
				$where			= "	finance_pettycash_transaction.DOCUMENT_NO = '$IOUNo' AND
									finance_pettycash_transaction.DOCUMENT_YEAR	= '$IOUYear'
									AND  finance_pettycash_transaction.DEPARTMENT_ID = '$departmentId'";
				$updateTrans	= $finance_pettycash_transaction->update($data,$where);
				if(!$updateTrans['type'])
					throw new Exception($updateTrans['msg']);

				if($recvAmount > 0)
				{
					//$docType	= 'SE_CASH';
					$insertCHTrans	= $finance_pettycash_transaction->insertRec($locationId,$invoiceNo,$invoiceYear,'SE_CASH',$IOUNo,$IOUYear,$departmentId,$reqUserId,$itemId,$recvAmount,$recvAmount,$date,$userId,$dateTimes->getCurruntDateTime());
					if(!$insertCHTrans['type'])
						throw new Exception($insertCHTrans['msg']);
				}

			}
		}
	}
	
	function rollbackTransactions($docType,$invoiceNo,$invoiceYear,$IOUNo,$IOUYear,$locationId,$userId)
	{
		global $finance_pettycash_transaction;
		global $dateTimes;
		global $finance_pettycash_invoice_detail;
		global $HR_DB;

		$balAmount	= 0;
		$where				= "LOCATION_ID='$locationId' AND DOCUMENT_NO='$invoiceNo' AND DOCUMENT_YEAR='$invoiceYear'";
		$deleteTrans		= $finance_pettycash_transaction->delete($where);
		if(!$deleteTrans['type'])
			throw new Exception($deleteTrans['msg']);
		if($docType == 'SETTLEMENT')
		{	
			$dtlResult			= $finance_pettycash_invoice_detail->loadIOUDetails($invoiceNo,$invoiceYear,$HR_DB);
			while($rowDtl		= mysqli_fetch_array($dtlResult))
			{
				$departmentId		= $rowDtl['DEPARTMENT_ID'];
				$reqUserId			= $rowDtl['REQUEST_USER_ID'];
				$billAmount			= abs($rowDtl['INVOICE_AMOUNT']);
				$recvAmount			= $rowDtl['RECEIVE_AMOUNT'];
				$itemId				= $rowDtl['PETTY_CASH_ITEM_ID'];
				
				$dtlAmt			= $finance_pettycash_invoice_detail-> getDetailAmountsWhenCancel($IOUNo,$IOUYear,$invoiceNo,$invoiceYear);
				$invoiceAmt		= $finance_pettycash_invoice_detail->getInvoiceAmount($IOUNo,$IOUYear);
				$detailBal 		= $invoiceAmt - $dtlAmt;

				$data			= array('BALANCE_AMOUNT'=>$detailBal,);
				$where			= "	finance_pettycash_invoice_detail.PETTY_INVOICE_NO = '$IOUNo' AND
									finance_pettycash_invoice_detail.PETTY_INVOICE_YEAR	= '$IOUYear'
									AND  finance_pettycash_invoice_detail.DEPARTMENT_ID = '$departmentId'";
				$updateTrans	= $finance_pettycash_invoice_detail->update($data,$where);
				if(!$updateTrans['type'])
					throw new Exception($updateTrans['msg']);
					
				$transResult	= $finance_pettycash_transaction->getIOUDetails($IOUNo,$IOUYear,$departmentId,$itemId);
				$rowTrans		= mysqli_fetch_array($transResult);
				/*if($billAmount < $rowTrans['INVOICE_AMOUNT'])
					$balAmount		= -($rowTrans['CASH_BAL'] + $billAmount);
				else
				{
*/					$settledAmt		=$finance_pettycash_transaction->getSettleAmount($IOUNo,$IOUYear,$departmentId);
					$balAmount		= -(abs($rowTrans['INVOICE_AMOUNT'])-abs($settledAmt));
				//}
	
				$data			= array('CASH_BALANCE'=>$balAmount,);
				$where			= "	finance_pettycash_transaction.DOCUMENT_NO = '$IOUNo' AND
									finance_pettycash_transaction.DOCUMENT_YEAR	= '$IOUYear'
									AND  finance_pettycash_transaction.DEPARTMENT_ID = '$departmentId'";
				$updateTrans	= $finance_pettycash_transaction->update($data,$where);
				if(!$updateTrans['type'])
					throw new Exception($updateTrans['msg']);
				
			}	

		}
	}
?>
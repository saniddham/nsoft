<?php
	session_start();
	$backwardseperator		= "../../../../";
	$companyId 				= $_SESSION['headCompanyId'];
	$locationId				= $_SESSION['CompanyID'];
	$mainPath				= $_SESSION['mainPath'];
	$session_userId			= $_SESSION["userId"];
	$intUser				= $_SESSION["userId"];
	$thisFilePath			=  $_SERVER['PHP_SELF'];
	$HR_DB 					= $_SESSION['HRDatabase'];
	$main_DB 				= $_SESSION['Database'];
	
	include_once 			"{$backwardseperator}dataAccess/Connector.php";
 	require_once 			"../../../../class/cls_commonFunctions_get.php";
	require_once 			"../../../../class/cls_commonErrorHandeling_get.php";
	require_once 			"../../../../class/finance/petty_cash/petty_cash_invoice/cls_petty_cash_invoice_get.php";
  	
	$obj_common				= new cls_commonFunctions_get($db);
	$obj_commonErr			= new cls_commonErrorHandeling_get($db);
	$obj_petty_cash_inv_get	= new cls_petty_cash_invoice_get($db);
	
	$programName			='Petty Cash Invoice';
	$programCode			='P0850';
	 
	$serialNo				= $_REQUEST["serialNo"];
	$serialYear				= $_REQUEST["serialYear"];
	$department				= $_REQUEST["department"];
	$mode					= $_REQUEST["mode"];
	 
	$header_array1			= $obj_petty_cash_inv_get->get_header($serialNo,$serialYear,$main_DB,$HR_DB,'RunQuery');
	$header_array 			= $obj_petty_cash_inv_get->get_header_department_wise($serialNo,$serialYear,$department,$main_DB,$HR_DB,'RunQuery');
 	//print_r($header_array);
	$permition_arr			= $obj_petty_cash_inv_get->get_permision_approval_for_department_heads($serialNo,$serialYear,$department,$intUser,'RunQuery');
	$permision_confirm		= $permition_arr['permision'];
	$permision_reject		= $permition_arr['permision'];
  ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Petty Cash Invoice - Department Wise Report</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../../../css/button.css"/>

<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="petty_cash_invoice_department_js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
 </head>
<body>
<style type="text/css">
.apDiv1 {
	position:absolute;
	left:380px;
	top:100px;
	width:auto;
	height:auto;
	z-index:0;
	opacity:0.1;
}
</style>
<?php
//$header_array1["STATUS"]=3;

?>

 <form id="frmRptPettyCashInvDepWise" name="frmRptPettyCashInvDepWise" method="post" action="rpt_petty_cash_invoice_department_heads.php">
  <table width="800" align="center">
    <tr>
      <td><?php include '../../../../reportHeader.php'?></td>
    </tr>
    <tr>
      <td class="reportHeader" align="center">Petty Cash Invoice (Department Approval) Report</td>
    </tr>
	<?php
		include "../../../../presentation/report_approve_status_and_buttons.php"
     ?>
    <tr>
        <td>
        <table width="100%" border="0" class="normalfnt">
            <tr>
            <td width="15%">Serial No</td>
            <td width="2%">:</td>
            <td width="29%"><?php echo $serialYear.'/'.$serialNo; ?></td>
            <td width="20%">Date</td>
            <td width="2%">:</td>
            <td width="32%"><?php  echo $header_array1['DATE']?></td>
            </tr>
            <tr>
                <td>Category</td>
                <td>:</td>
                <td ><?php echo $header_array1['CATEGORY']; ?></td>
                <td>Requested By</td>
                <td>:</td>
                <td ><?php echo $header_array1['EMPLOYEE_NAME']; ?></td>
            </tr>
            <tr>
                <td>Item</td>
                <td>:</td>
                <td ><?php echo $header_array1['ITEM_NAME']; ?></td>
                <td>Remarks</td>
                <td>:</td>
                <td rowspan="2" valign="top" ><?php echo $header_array1['REMARKS']; ?></td>
            </tr>
          <tr>
                <td><?php if($header_array1['SETTLEMENT_NO'] > 0){ ?>Settlement No <?php }  ?></td>
                <td>:</td>
                <td ><?php if($header_array1['SETTLEMENT_NO'] > 0){ ?><?php echo $header_array1['SETTLEMENT_NO'].'/'.$header_array1['SETTLEMENT_YEAR']; ?><?php }  ?></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4">
                	<table width="100%" border="0" class="bordered">
                    <tr>
                     <th>Department</th>
                    <th>Amount</th>
                     </tr>
                    <?php
					$result	= $obj_petty_cash_inv_get->get_selectedDepartmentAmounts($serialNo,$serialYear,$department,'RunQuery');
					while($row = mysqli_fetch_array($result))
					{
 					?>
                    <tr>
                     <td><?php echo $row['strName']; ?></td>
                    <td align="right"><?php echo number_format(abs($row['AMOUNT']),2); ?></td>
                     </tr>
                    <?php
 					}
					?>
                    </table>
                </td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td style="font-weight:bold">&nbsp;</td>
                <td></td>
                <td></td>
            </tr>
  
		</table>
        </td>
    </tr>
    <tr>
    <td >
        <table style="width:450px" class="bordered" border="0" >
        <thead>
            <tr class="normalfnt">
            <th colspan="2">Attached Documents</th>
            </tr>
            <tr class="normalfnt">
            <th >No</th>
            <th >File Name</th>
            </tr>
        </thead>
        <tbody>
		<?php
        	$folderName = $serialNo."-".$serialYear;
        	filesInDir("../../../../documents/finance/petty_cash/$folderName");
        ?>	
        </tbody>
        </table>
    </td>
    </tr>
    <tr>
    	<td><table width="100%" border="0" class="normalfnt">
            <tr>
                <td width="15%">&nbsp;</td>
                <td width="2%">&nbsp;</td>
                <td width="29%">&nbsp;</td>
                <td width="20%">&nbsp;</td>
                <td width="2%">&nbsp;</td>
                <td width="32%" >&nbsp;</td>
            </tr>
  
		</table></td>
    </tr>
    <tr>
    <td>
    <?php
 			$creator		= $header_array['USER'];
			$createdDate	= $header_array['CREATED_DATE'];
 			$resultA 		= $obj_petty_cash_inv_get->get_dep_wise_report_approval_details_result($serialYear,$serialNo,$department,'RunQuery');
			include "../../../../presentation/report_approvedBy_details.php"
 	?>
    </td>
    </tr>
    <tr>
      <td align="center" class="normalfntMid">&nbsp;</td>
    </tr>
    <tr>
    <td align="center" class="normalfntMid"><strong>Printed Date : </strong><?php echo date('Y-m-d H:i:s');?></td>
    </tr>
  </table>
</form>
</body>
</html>

<?php


function filesInDir($tdir)
{
	global $folderName;
	$m		= 0;
	$dirs 	= scandir($tdir);
	$html	= '';
	
	foreach($dirs as $file)
	{
		
		if (($file == '.')||($file == '..'))
		{
		}
		else
		{
			
			if (! is_dir($tdir.'/'.$file))
			{
			
			$html	.= "<tr bgcolor=\"#ffffff\" class=\"normalfnt\">
							<td align=\"center\">".++$m."</td>
							<td><a target=\"_blank\" href=\"../../../../documents/finance/petty_cash/".$folderName."/$file\">$file</a></td>
						</tr>";
			}
		}
	}	
	echo $html;
}


?>